/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 15/07/2013 21:55:28
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;


using EntitySpaces.Interfaces;
using EntitySpaces.Core;

























































































































































































using Financial.Investidor;



















namespace Financial.Contabil
{

    [Serializable]
    abstract public class esContabPlanoCollection : esEntityCollection
    {
        public esContabPlanoCollection()
        {

        }

        protected override string GetCollectionName()
        {
            return "ContabPlanoCollection";
        }

        #region Query Logic
        protected void InitQuery(esContabPlanoQuery query)
        {
            query.OnLoadDelegate = this.OnQueryLoaded;
            query.es.Connection = ((IEntityCollection)this).Connection;
        }

        protected bool OnQueryLoaded(DataTable table)
        {
            this.PopulateCollection(table);
            return (this.RowCount > 0) ? true : false;
        }

        protected override void HookupQuery(esDynamicQuery query)
        {
            this.InitQuery(query as esContabPlanoQuery);
        }
        #endregion

        virtual public ContabPlano DetachEntity(ContabPlano entity)
        {
            return base.DetachEntity(entity) as ContabPlano;
        }

        virtual public ContabPlano AttachEntity(ContabPlano entity)
        {
            return base.AttachEntity(entity) as ContabPlano;
        }

        virtual public void Combine(ContabPlanoCollection collection)
        {
            base.Combine(collection);
        }

        new public ContabPlano this[int index]
        {
            get
            {
                return base[index] as ContabPlano;
            }
        }

        public override Type GetEntityType()
        {
            return typeof(ContabPlano);
        }
    }



    [Serializable]
    abstract public class esContabPlano : esEntity
    {
        /// <summary>
        /// Used internally by the entity's DynamicQuery mechanism.
        /// </summary>
        virtual protected esContabPlanoQuery GetDynamicQuery()
        {
            return null;
        }

        public esContabPlano()
        {

        }

        public esContabPlano(DataRow row)
            : base(row)
        {

        }

        #region LoadByPrimaryKey
        public virtual bool LoadByPrimaryKey(System.Int32 idPlano)
        {
            if (this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
                return LoadByPrimaryKeyDynamic(idPlano);
            else
                return LoadByPrimaryKeyStoredProcedure(idPlano);
        }

        /// <summary>
        /// Loads an entity by primary key
        /// </summary>
        /// <remarks>
        /// EntitySpaces requires primary keys be defined on all tables.
        /// If a table does not have a primary key set,
        /// this method will not compile.
        /// Does not support sqlAcessType. It only works with DynamicQuery
        /// </remarks>
        /// <param name="fieldsToReturn">Fields desired</param>
        public virtual bool LoadByPrimaryKey(List<esQueryItem> fieldsToReturn, System.Int32 idPlano)
        {
            esQueryItem[] fields = fieldsToReturn.ToArray();
            esContabPlanoQuery query = this.GetDynamicQuery();
            query
                .Select(fields)
                .Where(query.IdPlano == idPlano);

            return query.Load();
        }

        public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.Int32 idPlano)
        {
            if (sqlAccessType == esSqlAccessType.DynamicSQL)
                return LoadByPrimaryKeyDynamic(idPlano);
            else
                return LoadByPrimaryKeyStoredProcedure(idPlano);
        }

        private bool LoadByPrimaryKeyDynamic(System.Int32 idPlano)
        {
            esContabPlanoQuery query = this.GetDynamicQuery();
            query.Where(query.IdPlano == idPlano);
            return query.Load();
        }

        private bool LoadByPrimaryKeyStoredProcedure(System.Int32 idPlano)
        {
            esParameters parms = new esParameters();
            parms.Add("IdPlano", idPlano);
            return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
        }
        #endregion



        #region Properties


        public override void SetProperties(IDictionary values)
        {
            foreach (string propertyName in values.Keys)
            {
                this.SetProperty(propertyName, values[propertyName]);
            }
        }

        public override void SetProperty(string name, object value)
        {
            if (this.Row == null) this.AddNew();

            esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
            if (col != null)
            {
                if (value == null || value.GetType().ToString() == "System.String")
                {
                    // Use the strongly typed property
                    switch (name)
                    {
                        case "IdPlano": this.str.IdPlano = (string)value; break;
                        case "Descricao": this.str.Descricao = (string)value; break;
                        case "ValidaDigitoConta": this.str.ValidaDigitoConta = (string)value; break;
                    }
                }
                else
                {
                    switch (name)
                    {
                        case "IdPlano":

                            if (value == null || value.GetType().ToString() == "System.Int32")
                                this.IdPlano = (System.Int32?)value;
                            break;


                        default:
                            break;
                    }
                }
            }
            else if (this.Row.Table.Columns.Contains(name))
            {
                this.Row[name] = value;
            }
            else
            {
                throw new Exception("SetProperty Error: '" + name + "' not found");
            }
        }


        /// <summary>
        /// Maps to ContabPlano.IdPlano
        /// </summary>
        virtual public System.Int32? IdPlano
        {
            get
            {
                return base.GetSystemInt32(ContabPlanoMetadata.ColumnNames.IdPlano);
            }

            set
            {
                base.SetSystemInt32(ContabPlanoMetadata.ColumnNames.IdPlano, value);
            }
        }

        /// <summary>
        /// Maps to ContabPlano.Descricao
        /// </summary>
        virtual public System.String Descricao
        {
            get
            {
                return base.GetSystemString(ContabPlanoMetadata.ColumnNames.Descricao);
            }

            set
            {
                base.SetSystemString(ContabPlanoMetadata.ColumnNames.Descricao, value);
            }
        }

        /// <summary>
        /// Maps to ContabPlano.ValidaDigitoConta
        /// </summary>
        virtual public System.String ValidaDigitoConta
        {
            get
            {
                return base.GetSystemString(ContabPlanoMetadata.ColumnNames.ValidaDigitoConta);
            }

            set
            {
                base.SetSystemString(ContabPlanoMetadata.ColumnNames.ValidaDigitoConta, value);
            }
        }

        #endregion

        #region String Properties


        [BrowsableAttribute(false)]
        public esStrings str
        {
            get
            {
                if (esstrings == null)
                {
                    esstrings = new esStrings(this);
                }
                return esstrings;
            }
        }


        [Serializable]
        sealed public class esStrings
        {
            public esStrings(esContabPlano entity)
            {
                this.entity = entity;
            }


            public System.String IdPlano
            {
                get
                {
                    System.Int32? data = entity.IdPlano;
                    return (data == null) ? String.Empty : Convert.ToString(data);
                }

                set
                {
                    if (value == null || value.Length == 0) entity.IdPlano = null;
                    else entity.IdPlano = Convert.ToInt32(value);
                }
            }

            public System.String Descricao
            {
                get
                {
                    System.String data = entity.Descricao;
                    return (data == null) ? String.Empty : Convert.ToString(data);
                }

                set
                {
                    if (value == null || value.Length == 0) entity.Descricao = null;
                    else entity.Descricao = Convert.ToString(value);
                }
            }

            public System.String ValidaDigitoConta
            {
                get
                {
                    System.String data = entity.ValidaDigitoConta;
                    return (data == null) ? String.Empty : Convert.ToString(data);
                }

                set
                {
                    if (value == null || value.Length == 0) entity.ValidaDigitoConta = null;
                    else entity.ValidaDigitoConta = Convert.ToString(value);
                }
            }

            private esContabPlano entity;
        }
        #endregion

        #region Query Logic
        protected void InitQuery(esContabPlanoQuery query)
        {
            query.OnLoadDelegate = this.OnQueryLoaded;
            query.es.Connection = ((IEntity)this).Connection;
        }

        [System.Diagnostics.DebuggerNonUserCode]
        protected bool OnQueryLoaded(DataTable table)
        {
            bool dataFound = this.PopulateEntity(table);

            if (this.RowCount > 1)
            {
                throw new Exception("esContabPlano can only hold one record of data");
            }

            return dataFound;
        }
        #endregion

        [NonSerialized]
        private esStrings esstrings;
    }



    public partial class ContabPlano : esContabPlano
    {


        #region ClienteCollectionByIdPlano - Zero To Many
        /// <summary>
        /// Zero to Many
        /// Foreign Key Name - ContabPlano_Cliente_FK1
        /// </summary>

        [XmlIgnore]
        public ClienteCollection ClienteCollectionByIdPlano
        {
            get
            {
                if (this._ClienteCollectionByIdPlano == null)
                {
                    this._ClienteCollectionByIdPlano = new ClienteCollection();
                    this._ClienteCollectionByIdPlano.es.Connection.Name = this.es.Connection.Name;
                    this.SetPostSave("ClienteCollectionByIdPlano", this._ClienteCollectionByIdPlano);

                    if (this.IdPlano != null)
                    {
                        this._ClienteCollectionByIdPlano.Query.Where(this._ClienteCollectionByIdPlano.Query.IdPlano == this.IdPlano);
                        this._ClienteCollectionByIdPlano.Query.Load();

                        // Auto-hookup Foreign Keys
                        this._ClienteCollectionByIdPlano.fks.Add(ClienteMetadata.ColumnNames.IdPlano, this.IdPlano);
                    }
                }

                return this._ClienteCollectionByIdPlano;
            }

            set
            {
                if (value != null) throw new Exception("'value' Must be null");

                if (this._ClienteCollectionByIdPlano != null)
                {
                    this.RemovePostSave("ClienteCollectionByIdPlano");
                    this._ClienteCollectionByIdPlano = null;

                }
            }
        }

        private ClienteCollection _ClienteCollectionByIdPlano;
        #endregion


        #region ContabContaCollectionByIdPlano - Zero To Many
        /// <summary>
        /// Zero to Many
        /// Foreign Key Name - ContabConta_ContabPlano_FK1
        /// </summary>

        [XmlIgnore]
        public ContabContaCollection ContabContaCollectionByIdPlano
        {
            get
            {
                if (this._ContabContaCollectionByIdPlano == null)
                {
                    this._ContabContaCollectionByIdPlano = new ContabContaCollection();
                    this._ContabContaCollectionByIdPlano.es.Connection.Name = this.es.Connection.Name;
                    this.SetPostSave("ContabContaCollectionByIdPlano", this._ContabContaCollectionByIdPlano);

                    if (this.IdPlano != null)
                    {
                        this._ContabContaCollectionByIdPlano.Query.Where(this._ContabContaCollectionByIdPlano.Query.IdPlano == this.IdPlano);
                        this._ContabContaCollectionByIdPlano.Query.Load();

                        // Auto-hookup Foreign Keys
                        this._ContabContaCollectionByIdPlano.fks.Add(ContabContaMetadata.ColumnNames.IdPlano, this.IdPlano);
                    }
                }

                return this._ContabContaCollectionByIdPlano;
            }

            set
            {
                if (value != null) throw new Exception("'value' Must be null");

                if (this._ContabContaCollectionByIdPlano != null)
                {
                    this.RemovePostSave("ContabContaCollectionByIdPlano");
                    this._ContabContaCollectionByIdPlano = null;

                }
            }
        }

        private ContabContaCollection _ContabContaCollectionByIdPlano;
        #endregion


        #region ContabLancamentoCollectionByIdPlano - Zero To Many
        /// <summary>
        /// Zero to Many
        /// Foreign Key Name - ContabLancamento_ContabPlano_FK1
        /// </summary>

        [XmlIgnore]
        public ContabLancamentoCollection ContabLancamentoCollectionByIdPlano
        {
            get
            {
                if (this._ContabLancamentoCollectionByIdPlano == null)
                {
                    this._ContabLancamentoCollectionByIdPlano = new ContabLancamentoCollection();
                    this._ContabLancamentoCollectionByIdPlano.es.Connection.Name = this.es.Connection.Name;
                    this.SetPostSave("ContabLancamentoCollectionByIdPlano", this._ContabLancamentoCollectionByIdPlano);

                    if (this.IdPlano != null)
                    {
                        this._ContabLancamentoCollectionByIdPlano.Query.Where(this._ContabLancamentoCollectionByIdPlano.Query.IdPlano == this.IdPlano);
                        this._ContabLancamentoCollectionByIdPlano.Query.Load();

                        // Auto-hookup Foreign Keys
                        this._ContabLancamentoCollectionByIdPlano.fks.Add(ContabLancamentoMetadata.ColumnNames.IdPlano, this.IdPlano);
                    }
                }

                return this._ContabLancamentoCollectionByIdPlano;
            }

            set
            {
                if (value != null) throw new Exception("'value' Must be null");

                if (this._ContabLancamentoCollectionByIdPlano != null)
                {
                    this.RemovePostSave("ContabLancamentoCollectionByIdPlano");
                    this._ContabLancamentoCollectionByIdPlano = null;

                }
            }
        }

        private ContabLancamentoCollection _ContabLancamentoCollectionByIdPlano;
        #endregion


        #region ContabRoteiroCollectionByIdPlano - Zero To Many
        /// <summary>
        /// Zero to Many
        /// Foreign Key Name - ContabPlano_ContabRoteiro_FK1
        /// </summary>

        [XmlIgnore]
        public ContabRoteiroCollection ContabRoteiroCollectionByIdPlano
        {
            get
            {
                if (this._ContabRoteiroCollectionByIdPlano == null)
                {
                    this._ContabRoteiroCollectionByIdPlano = new ContabRoteiroCollection();
                    this._ContabRoteiroCollectionByIdPlano.es.Connection.Name = this.es.Connection.Name;
                    this.SetPostSave("ContabRoteiroCollectionByIdPlano", this._ContabRoteiroCollectionByIdPlano);

                    if (this.IdPlano != null)
                    {
                        this._ContabRoteiroCollectionByIdPlano.Query.Where(this._ContabRoteiroCollectionByIdPlano.Query.IdPlano == this.IdPlano);
                        this._ContabRoteiroCollectionByIdPlano.Query.Load();

                        // Auto-hookup Foreign Keys
                        this._ContabRoteiroCollectionByIdPlano.fks.Add(ContabRoteiroMetadata.ColumnNames.IdPlano, this.IdPlano);
                    }
                }

                return this._ContabRoteiroCollectionByIdPlano;
            }

            set
            {
                if (value != null) throw new Exception("'value' Must be null");

                if (this._ContabRoteiroCollectionByIdPlano != null)
                {
                    this.RemovePostSave("ContabRoteiroCollectionByIdPlano");
                    this._ContabRoteiroCollectionByIdPlano = null;

                }
            }
        }

        private ContabRoteiroCollection _ContabRoteiroCollectionByIdPlano;
        #endregion


        #region ContabSaldoCollectionByIdPlano - Zero To Many
        /// <summary>
        /// Zero to Many
        /// Foreign Key Name - ContabSaldo_ContabPlano_FK1
        /// </summary>

        [XmlIgnore]
        public ContabSaldoCollection ContabSaldoCollectionByIdPlano
        {
            get
            {
                if (this._ContabSaldoCollectionByIdPlano == null)
                {
                    this._ContabSaldoCollectionByIdPlano = new ContabSaldoCollection();
                    this._ContabSaldoCollectionByIdPlano.es.Connection.Name = this.es.Connection.Name;
                    this.SetPostSave("ContabSaldoCollectionByIdPlano", this._ContabSaldoCollectionByIdPlano);

                    if (this.IdPlano != null)
                    {
                        this._ContabSaldoCollectionByIdPlano.Query.Where(this._ContabSaldoCollectionByIdPlano.Query.IdPlano == this.IdPlano);
                        this._ContabSaldoCollectionByIdPlano.Query.Load();

                        // Auto-hookup Foreign Keys
                        this._ContabSaldoCollectionByIdPlano.fks.Add(ContabSaldoMetadata.ColumnNames.IdPlano, this.IdPlano);
                    }
                }

                return this._ContabSaldoCollectionByIdPlano;
            }

            set
            {
                if (value != null) throw new Exception("'value' Must be null");

                if (this._ContabSaldoCollectionByIdPlano != null)
                {
                    this.RemovePostSave("ContabSaldoCollectionByIdPlano");
                    this._ContabSaldoCollectionByIdPlano = null;

                }
            }
        }

        private ContabSaldoCollection _ContabSaldoCollectionByIdPlano;
        #endregion


        /// <summary>
        /// Used internally by the entity's hierarchical properties.
        /// </summary>
        protected override List<esPropertyDescriptor> GetHierarchicalProperties()
        {
            List<esPropertyDescriptor> props = new List<esPropertyDescriptor>();

            props.Add(new esPropertyDescriptor(this, "ClienteCollectionByIdPlano", typeof(ClienteCollection), new Cliente()));
            props.Add(new esPropertyDescriptor(this, "ContabContaCollectionByIdPlano", typeof(ContabContaCollection), new ContabConta()));
            props.Add(new esPropertyDescriptor(this, "ContabLancamentoCollectionByIdPlano", typeof(ContabLancamentoCollection), new ContabLancamento()));
            props.Add(new esPropertyDescriptor(this, "ContabRoteiroCollectionByIdPlano", typeof(ContabRoteiroCollection), new ContabRoteiro()));
            props.Add(new esPropertyDescriptor(this, "ContabSaldoCollectionByIdPlano", typeof(ContabSaldoCollection), new ContabSaldo()));

            return props;
        }

        /// <summary>
        /// Used internally for retrieving AutoIncrementing keys
        /// during hierarchical PreSave.
        /// </summary>
        protected override void ApplyPreSaveKeys()
        {
        }

        /// <summary>
        /// Used internally for retrieving AutoIncrementing keys
        /// during hierarchical PostSave.
        /// </summary>
        protected override void ApplyPostSaveKeys()
        {
            if (this._ClienteCollectionByIdPlano != null)
            {
                foreach (Cliente obj in this._ClienteCollectionByIdPlano)
                {
                    if (obj.es.IsAdded)
                    {
                        obj.IdPlano = this.IdPlano;
                    }
                }
            }
            if (this._ContabContaCollectionByIdPlano != null)
            {
                foreach (ContabConta obj in this._ContabContaCollectionByIdPlano)
                {
                    if (obj.es.IsAdded)
                    {
                        obj.IdPlano = this.IdPlano;
                    }
                }
            }
            if (this._ContabLancamentoCollectionByIdPlano != null)
            {
                foreach (ContabLancamento obj in this._ContabLancamentoCollectionByIdPlano)
                {
                    if (obj.es.IsAdded)
                    {
                        obj.IdPlano = this.IdPlano;
                    }
                }
            }
            if (this._ContabRoteiroCollectionByIdPlano != null)
            {
                foreach (ContabRoteiro obj in this._ContabRoteiroCollectionByIdPlano)
                {
                    if (obj.es.IsAdded)
                    {
                        obj.IdPlano = this.IdPlano;
                    }
                }
            }
            if (this._ContabSaldoCollectionByIdPlano != null)
            {
                foreach (ContabSaldo obj in this._ContabSaldoCollectionByIdPlano)
                {
                    if (obj.es.IsAdded)
                    {
                        obj.IdPlano = this.IdPlano;
                    }
                }
            }
        }

        /// <summary>
        /// Used internally for retrieving AutoIncrementing keys
        /// during hierarchical PostOneToOneSave.
        /// </summary>
        protected override void ApplyPostOneSaveKeys()
        {
        }

    }



    [Serializable]
    abstract public class esContabPlanoQuery : esDynamicQuery
    {
        override protected IMetadata Meta
        {
            get
            {
                return ContabPlanoMetadata.Meta();
            }
        }


        public esQueryItem IdPlano
        {
            get
            {
                return new esQueryItem(this, ContabPlanoMetadata.ColumnNames.IdPlano, esSystemType.Int32);
            }
        }

        public esQueryItem Descricao
        {
            get
            {
                return new esQueryItem(this, ContabPlanoMetadata.ColumnNames.Descricao, esSystemType.String);
            }
        }

        public esQueryItem ValidaDigitoConta
        {
            get
            {
                return new esQueryItem(this, ContabPlanoMetadata.ColumnNames.ValidaDigitoConta, esSystemType.String);
            }
        }
    }



    [Serializable]
    [XmlType("ContabPlanoCollection")]
    public partial class ContabPlanoCollection : esContabPlanoCollection, IEnumerable<ContabPlano>
    {
        public ContabPlanoCollection()
        {

        }

        public static implicit operator List<ContabPlano>(ContabPlanoCollection coll)
        {
            List<ContabPlano> list = new List<ContabPlano>();

            foreach (ContabPlano emp in coll)
            {
                list.Add(emp);
            }

            return list;
        }

        #region Housekeeping methods
        override protected IMetadata Meta
        {
            get
            {
                return ContabPlanoMetadata.Meta();
            }
        }



        override protected esDynamicQuery GetDynamicQuery()
        {
            if (this.query == null)
            {
                this.query = new ContabPlanoQuery();
                this.InitQuery(query);
            }
            return this.query;
        }

        override protected esEntity CreateEntityForCollection(DataRow row)
        {
            return new ContabPlano(row);
        }

        override protected esEntity CreateEntity()
        {
            return new ContabPlano();
        }


        #endregion


        [BrowsableAttribute(false)]
        public ContabPlanoQuery Query
        {
            get
            {
                if (this.query == null)
                {
                    this.query = new ContabPlanoQuery();
                    base.InitQuery(this.query);
                }

                return this.query;
            }
        }

        public void QueryReset()
        {
            this.query = null;
        }

        public bool Load(ContabPlanoQuery query)
        {
            this.query = query;
            base.InitQuery(this.query);
            return this.Query.Load();
        }

        public ContabPlano AddNew()
        {
            ContabPlano entity = base.AddNewEntity() as ContabPlano;

            return entity;
        }

        public ContabPlano FindByPrimaryKey(System.Int32 idPlano)
        {
            return base.FindByPrimaryKey(idPlano) as ContabPlano;
        }


        #region IEnumerable<ContabPlano> Members

        IEnumerator<ContabPlano> IEnumerable<ContabPlano>.GetEnumerator()
        {
            System.Collections.IEnumerable enumer = this as System.Collections.IEnumerable;
            System.Collections.IEnumerator iterator = enumer.GetEnumerator();

            while (iterator.MoveNext())
            {
                yield return iterator.Current as ContabPlano;
            }
        }

        #endregion

        private ContabPlanoQuery query;
    }


    /// <summary>
    /// Encapsulates the 'ContabPlano' table
    /// </summary>

    [Serializable]
    public partial class ContabPlano : esContabPlano
    {
        public ContabPlano()
        {

        }

        public ContabPlano(DataRow row)
            : base(row)
        {

        }

        #region Housekeeping methods
        override protected IMetadata Meta
        {
            get
            {
                return ContabPlanoMetadata.Meta();
            }
        }



        override protected esContabPlanoQuery GetDynamicQuery()
        {
            if (this.query == null)
            {
                this.query = new ContabPlanoQuery();
                this.InitQuery(query);
            }
            return this.query;
        }
        #endregion




        [BrowsableAttribute(false)]
        public ContabPlanoQuery Query
        {
            get
            {
                if (this.query == null)
                {
                    this.query = new ContabPlanoQuery();
                    base.InitQuery(this.query);
                }

                return this.query;
            }
        }

        public void QueryReset()
        {
            this.query = null;
        }


        public bool Load(ContabPlanoQuery query)
        {
            this.query = query;
            base.InitQuery(this.query);
            return this.Query.Load();
        }

        private ContabPlanoQuery query;
    }



    [Serializable]
    public partial class ContabPlanoQuery : esContabPlanoQuery
    {
        public ContabPlanoQuery()
        {

        }

        public ContabPlanoQuery(string joinAlias)
        {
            this.es.JoinAlias = joinAlias;
        }


    }



    [Serializable]
    public partial class ContabPlanoMetadata : esMetadata, IMetadata
    {
        #region Protected Constructor
        protected ContabPlanoMetadata()
        {
            _columns = new esColumnMetadataCollection();
            esColumnMetadata c;

            c = new esColumnMetadata(ContabPlanoMetadata.ColumnNames.IdPlano, 0, typeof(System.Int32), esSystemType.Int32);
            c.PropertyName = ContabPlanoMetadata.PropertyNames.IdPlano;
            c.IsInPrimaryKey = true;
            c.IsAutoIncrement = true;
            c.NumericPrecision = 10;
            _columns.Add(c);


            c = new esColumnMetadata(ContabPlanoMetadata.ColumnNames.Descricao, 1, typeof(System.String), esSystemType.String);
            c.PropertyName = ContabPlanoMetadata.PropertyNames.Descricao;
            c.CharacterMaxLength = 100;
            c.NumericPrecision = 0;
            _columns.Add(c);

            c = new esColumnMetadata(ContabPlanoMetadata.ColumnNames.ValidaDigitoConta, 2, typeof(System.String), esSystemType.String);
            c.PropertyName = ContabPlanoMetadata.PropertyNames.ValidaDigitoConta;
            c.CharacterMaxLength = 1;
            c.NumericPrecision = 0;
            c.HasDefault = true;
            c.Default = @"('N')";
            _columns.Add(c);

        }
        #endregion

        static public ContabPlanoMetadata Meta()
        {
            return meta;
        }

        public Guid DataID
        {
            get { return base._dataID; }
        }

        public bool MultiProviderMode
        {
            get { return false; }
        }

        public esColumnMetadataCollection Columns
        {
            get { return base._columns; }
        }

        #region ColumnNames
        public class ColumnNames
        {
            public const string IdPlano = "IdPlano";
            public const string Descricao = "Descricao";
            public const string ValidaDigitoConta = "ValidaDigitoConta";
        }
        #endregion

        #region PropertyNames
        public class PropertyNames
        {
            public const string IdPlano = "IdPlano";
            public const string Descricao = "Descricao";
            public const string ValidaDigitoConta = "ValidaDigitoConta";
        }
        #endregion

        public esProviderSpecificMetadata GetProviderMetadata(string mapName)
        {
            MapToMeta mapMethod = mapDelegates[mapName];

            if (mapMethod != null)
                return mapMethod(mapName);
            else
                return null;
        }

        #region MAP esDefault

        static private int RegisterDelegateesDefault()
        {
            // This is only executed once per the life of the application
            lock (typeof(ContabPlanoMetadata))
            {
                if (ContabPlanoMetadata.mapDelegates == null)
                {
                    ContabPlanoMetadata.mapDelegates = new Dictionary<string, MapToMeta>();
                }

                if (ContabPlanoMetadata.meta == null)
                {
                    ContabPlanoMetadata.meta = new ContabPlanoMetadata();
                }

                MapToMeta mapMethod = new MapToMeta(meta.esDefault);
                mapDelegates.Add("esDefault", mapMethod);
                mapMethod("esDefault");
            }
            return 0;
        }

        private esProviderSpecificMetadata esDefault(string mapName)
        {
            if (!_providerMetadataMaps.ContainsKey(mapName))
            {
                esProviderSpecificMetadata meta = new esProviderSpecificMetadata();


                meta.AddTypeMap("IdPlano", new esTypeMap("int", "System.Int32"));
                meta.AddTypeMap("Descricao", new esTypeMap("varchar", "System.String"));
                meta.AddTypeMap("ValidaDigitoConta", new esTypeMap("char", "System.String"));



                meta.Source = "ContabPlano";
                meta.Destination = "ContabPlano";

                meta.spInsert = "proc_ContabPlanoInsert";
                meta.spUpdate = "proc_ContabPlanoUpdate";
                meta.spDelete = "proc_ContabPlanoDelete";
                meta.spLoadAll = "proc_ContabPlanoLoadAll";
                meta.spLoadByPrimaryKey = "proc_ContabPlanoLoadByPrimaryKey";

                this._providerMetadataMaps["esDefault"] = meta;
            }

            return this._providerMetadataMaps["esDefault"];
        }

        #endregion

        static private ContabPlanoMetadata meta;
        static protected Dictionary<string, MapToMeta> mapDelegates;
        static private int _esDefault = RegisterDelegateesDefault();
    }
}
