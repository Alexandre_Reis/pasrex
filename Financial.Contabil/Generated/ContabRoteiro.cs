/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 15/07/2013 21:55:28
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;


using EntitySpaces.Interfaces;
using EntitySpaces.Core;





































		



			
					


		

		
		
		

		

























		






	

















				
		
		




		








		
				
				












	

















					
								
											
	










		





		




				
				








using Financial.Fundo;

				




		

		
		
		
		
		





namespace Financial.Contabil
{

	[Serializable]
	abstract public class esContabRoteiroCollection : esEntityCollection
	{
		public esContabRoteiroCollection()
		{

		}

		protected override string GetCollectionName()
		{
			return "ContabRoteiroCollection";
		}

		#region Query Logic
		protected void InitQuery(esContabRoteiroQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntityCollection)this).Connection;
		}

		protected bool OnQueryLoaded(DataTable table)
		{
			this.PopulateCollection(table);
			return (this.RowCount > 0) ? true : false;
		}
		
		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery(query as esContabRoteiroQuery);
		}
		#endregion
		
		virtual public ContabRoteiro DetachEntity(ContabRoteiro entity)
		{
			return base.DetachEntity(entity) as ContabRoteiro;
		}
		
		virtual public ContabRoteiro AttachEntity(ContabRoteiro entity)
		{
			return base.AttachEntity(entity) as ContabRoteiro;
		}
		
		virtual public void Combine(ContabRoteiroCollection collection)
		{
			base.Combine(collection);
		}
		
		new public ContabRoteiro this[int index]
		{
			get
			{
				return base[index] as ContabRoteiro;
			}
		}

		public override Type GetEntityType()
		{
			return typeof(ContabRoteiro);
		}
	}



	[Serializable]
	abstract public class esContabRoteiro : esEntity
	{
		/// <summary>
		/// Used internally by the entity's DynamicQuery mechanism.
		/// </summary>
		virtual protected esContabRoteiroQuery GetDynamicQuery()
		{
			return null;
		}

		public esContabRoteiro()
		{

		}

		public esContabRoteiro(DataRow row)
			: base(row)
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.Int32 idEvento)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idEvento);
			else
				return LoadByPrimaryKeyStoredProcedure(idEvento);
		}

		/// <summary>
		/// Loads an entity by primary key
		/// </summary>
		/// <remarks>
		/// EntitySpaces requires primary keys be defined on all tables.
		/// If a table does not have a primary key set,
		/// this method will not compile.
		/// Does not support sqlAcessType. It only works with DynamicQuery
		/// </remarks>
		/// <param name="fieldsToReturn">Fields desired</param>
		public virtual bool LoadByPrimaryKey(List<esQueryItem> fieldsToReturn, System.Int32 idEvento)
		{
			esQueryItem[] fields = fieldsToReturn.ToArray();			
			esContabRoteiroQuery query = this.GetDynamicQuery();
			query
			    .Select(fields)
			    .Where(query.IdEvento == idEvento);
		
			return query.Load();
		}
		
		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.Int32 idEvento)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idEvento);
			else
				return LoadByPrimaryKeyStoredProcedure(idEvento);
		}

		private bool LoadByPrimaryKeyDynamic(System.Int32 idEvento)
		{
			esContabRoteiroQuery query = this.GetDynamicQuery();
			query.Where(query.IdEvento == idEvento);
			return query.Load();
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.Int32 idEvento)
		{
			esParameters parms = new esParameters();
			parms.Add("IdEvento",idEvento);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		
		
		#region Properties
		
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}

		public override void SetProperty(string name, object value)
		{
			if(this.Row == null) this.AddNew();
			
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value.GetType().ToString() == "System.String")
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "IdEvento": this.str.IdEvento = (string)value; break;							
						case "ContaDebito": this.str.ContaDebito = (string)value; break;							
						case "ContaCredito": this.str.ContaCredito = (string)value; break;							
						case "Origem": this.str.Origem = (string)value; break;							
						case "Identificador": this.str.Identificador = (string)value; break;							
						case "Descricao": this.str.Descricao = (string)value; break;							
						case "IdPlano": this.str.IdPlano = (string)value; break;							
						case "IdCentroCusto": this.str.IdCentroCusto = (string)value; break;							
						case "ContaDebitoReduzida": this.str.ContaDebitoReduzida = (string)value; break;							
						case "ContaCreditoReduzida": this.str.ContaCreditoReduzida = (string)value; break;							
						case "IdContaCredito": this.str.IdContaCredito = (string)value; break;							
						case "IdContaDebito": this.str.IdContaDebito = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "IdEvento":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdEvento = (System.Int32?)value;
							break;
						
						case "Origem":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.Origem = (System.Int32?)value;
							break;
						
						case "IdPlano":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdPlano = (System.Int32?)value;
							break;
						
						case "IdCentroCusto":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdCentroCusto = (System.Int32?)value;
							break;
						
						case "IdContaCredito":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdContaCredito = (System.Int32?)value;
							break;
						
						case "IdContaDebito":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdContaDebito = (System.Int32?)value;
							break;
					

						default:
							break;
					}
				}
			}
			else if(this.Row.Table.Columns.Contains(name))
			{
				this.Row[name] = value;
			}
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}
		
		
		/// <summary>
		/// Maps to ContabRoteiro.IdEvento
		/// </summary>
		virtual public System.Int32? IdEvento
		{
			get
			{
				return base.GetSystemInt32(ContabRoteiroMetadata.ColumnNames.IdEvento);
			}
			
			set
			{
				base.SetSystemInt32(ContabRoteiroMetadata.ColumnNames.IdEvento, value);
			}
		}
		
		/// <summary>
		/// Maps to ContabRoteiro.ContaDebito
		/// </summary>
		virtual public System.String ContaDebito
		{
			get
			{
				return base.GetSystemString(ContabRoteiroMetadata.ColumnNames.ContaDebito);
			}
			
			set
			{
				base.SetSystemString(ContabRoteiroMetadata.ColumnNames.ContaDebito, value);
			}
		}
		
		/// <summary>
		/// Maps to ContabRoteiro.ContaCredito
		/// </summary>
		virtual public System.String ContaCredito
		{
			get
			{
				return base.GetSystemString(ContabRoteiroMetadata.ColumnNames.ContaCredito);
			}
			
			set
			{
				base.SetSystemString(ContabRoteiroMetadata.ColumnNames.ContaCredito, value);
			}
		}
		
		/// <summary>
		/// Maps to ContabRoteiro.Origem
		/// </summary>
		virtual public System.Int32? Origem
		{
			get
			{
				return base.GetSystemInt32(ContabRoteiroMetadata.ColumnNames.Origem);
			}
			
			set
			{
				base.SetSystemInt32(ContabRoteiroMetadata.ColumnNames.Origem, value);
			}
		}
		
		/// <summary>
		/// Maps to ContabRoteiro.Identificador
		/// </summary>
		virtual public System.String Identificador
		{
			get
			{
				return base.GetSystemString(ContabRoteiroMetadata.ColumnNames.Identificador);
			}
			
			set
			{
				base.SetSystemString(ContabRoteiroMetadata.ColumnNames.Identificador, value);
			}
		}
		
		/// <summary>
		/// Maps to ContabRoteiro.Descricao
		/// </summary>
		virtual public System.String Descricao
		{
			get
			{
				return base.GetSystemString(ContabRoteiroMetadata.ColumnNames.Descricao);
			}
			
			set
			{
				base.SetSystemString(ContabRoteiroMetadata.ColumnNames.Descricao, value);
			}
		}
		
		/// <summary>
		/// Maps to ContabRoteiro.IdPlano
		/// </summary>
		virtual public System.Int32? IdPlano
		{
			get
			{
				return base.GetSystemInt32(ContabRoteiroMetadata.ColumnNames.IdPlano);
			}
			
			set
			{
				if(base.SetSystemInt32(ContabRoteiroMetadata.ColumnNames.IdPlano, value))
				{
					this._UpToContabPlanoByIdPlano = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to ContabRoteiro.IdCentroCusto
		/// </summary>
		virtual public System.Int32? IdCentroCusto
		{
			get
			{
				return base.GetSystemInt32(ContabRoteiroMetadata.ColumnNames.IdCentroCusto);
			}
			
			set
			{
				if(base.SetSystemInt32(ContabRoteiroMetadata.ColumnNames.IdCentroCusto, value))
				{
					this._UpToContabCentroCustoByIdCentroCusto = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to ContabRoteiro.ContaDebitoReduzida
		/// </summary>
		virtual public System.String ContaDebitoReduzida
		{
			get
			{
				return base.GetSystemString(ContabRoteiroMetadata.ColumnNames.ContaDebitoReduzida);
			}
			
			set
			{
				base.SetSystemString(ContabRoteiroMetadata.ColumnNames.ContaDebitoReduzida, value);
			}
		}
		
		/// <summary>
		/// Maps to ContabRoteiro.ContaCreditoReduzida
		/// </summary>
		virtual public System.String ContaCreditoReduzida
		{
			get
			{
				return base.GetSystemString(ContabRoteiroMetadata.ColumnNames.ContaCreditoReduzida);
			}
			
			set
			{
				base.SetSystemString(ContabRoteiroMetadata.ColumnNames.ContaCreditoReduzida, value);
			}
		}
		
		/// <summary>
		/// Maps to ContabRoteiro.IdContaCredito
		/// </summary>
		virtual public System.Int32? IdContaCredito
		{
			get
			{
				return base.GetSystemInt32(ContabRoteiroMetadata.ColumnNames.IdContaCredito);
			}
			
			set
			{
				if(base.SetSystemInt32(ContabRoteiroMetadata.ColumnNames.IdContaCredito, value))
				{
					this._UpToContabContaByIdContaCredito = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to ContabRoteiro.IdContaDebito
		/// </summary>
		virtual public System.Int32? IdContaDebito
		{
			get
			{
				return base.GetSystemInt32(ContabRoteiroMetadata.ColumnNames.IdContaDebito);
			}
			
			set
			{
				if(base.SetSystemInt32(ContabRoteiroMetadata.ColumnNames.IdContaDebito, value))
				{
					this._UpToContabContaByIdContaDebito = null;
				}
			}
		}
		
		[CLSCompliant(false)]
		internal protected ContabCentroCusto _UpToContabCentroCustoByIdCentroCusto;
		[CLSCompliant(false)]
		internal protected ContabConta _UpToContabContaByIdContaCredito;
		[CLSCompliant(false)]
		internal protected ContabConta _UpToContabContaByIdContaDebito;
		[CLSCompliant(false)]
		internal protected ContabPlano _UpToContabPlanoByIdPlano;
		#endregion	

		#region String Properties


		[BrowsableAttribute( false )]
		public esStrings str
		{
			get
			{
				if (esstrings == null)
				{
					esstrings = new esStrings(this);
				}
				return esstrings;
			}
		}


		[Serializable]
		sealed public class esStrings
		{
			public esStrings(esContabRoteiro entity)
			{
				this.entity = entity;
			}
			
	
			public System.String IdEvento
			{
				get
				{
					System.Int32? data = entity.IdEvento;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdEvento = null;
					else entity.IdEvento = Convert.ToInt32(value);
				}
			}
				
			public System.String ContaDebito
			{
				get
				{
					System.String data = entity.ContaDebito;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ContaDebito = null;
					else entity.ContaDebito = Convert.ToString(value);
				}
			}
				
			public System.String ContaCredito
			{
				get
				{
					System.String data = entity.ContaCredito;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ContaCredito = null;
					else entity.ContaCredito = Convert.ToString(value);
				}
			}
				
			public System.String Origem
			{
				get
				{
					System.Int32? data = entity.Origem;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Origem = null;
					else entity.Origem = Convert.ToInt32(value);
				}
			}
				
			public System.String Identificador
			{
				get
				{
					System.String data = entity.Identificador;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Identificador = null;
					else entity.Identificador = Convert.ToString(value);
				}
			}
				
			public System.String Descricao
			{
				get
				{
					System.String data = entity.Descricao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Descricao = null;
					else entity.Descricao = Convert.ToString(value);
				}
			}
				
			public System.String IdPlano
			{
				get
				{
					System.Int32? data = entity.IdPlano;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdPlano = null;
					else entity.IdPlano = Convert.ToInt32(value);
				}
			}
				
			public System.String IdCentroCusto
			{
				get
				{
					System.Int32? data = entity.IdCentroCusto;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdCentroCusto = null;
					else entity.IdCentroCusto = Convert.ToInt32(value);
				}
			}
				
			public System.String ContaDebitoReduzida
			{
				get
				{
					System.String data = entity.ContaDebitoReduzida;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ContaDebitoReduzida = null;
					else entity.ContaDebitoReduzida = Convert.ToString(value);
				}
			}
				
			public System.String ContaCreditoReduzida
			{
				get
				{
					System.String data = entity.ContaCreditoReduzida;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ContaCreditoReduzida = null;
					else entity.ContaCreditoReduzida = Convert.ToString(value);
				}
			}
				
			public System.String IdContaCredito
			{
				get
				{
					System.Int32? data = entity.IdContaCredito;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdContaCredito = null;
					else entity.IdContaCredito = Convert.ToInt32(value);
				}
			}
				
			public System.String IdContaDebito
			{
				get
				{
					System.Int32? data = entity.IdContaDebito;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdContaDebito = null;
					else entity.IdContaDebito = Convert.ToInt32(value);
				}
			}
			

			private esContabRoteiro entity;
		}
		#endregion

		#region Query Logic
		protected void InitQuery(esContabRoteiroQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntity)this).Connection;
		}

		[System.Diagnostics.DebuggerNonUserCode]
		protected bool OnQueryLoaded(DataTable table)
		{
			bool dataFound = this.PopulateEntity(table);

			if (this.RowCount > 1)
			{
				throw new Exception("esContabRoteiro can only hold one record of data");
			}

			return dataFound;
		}
		#endregion
		
		[NonSerialized]
		private esStrings esstrings;
	}


	
	public partial class ContabRoteiro : esContabRoteiro
	{

				
		#region TabelaProvisaoCollectionByIdEventoProvisao - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - ContabRoteiro_TabelaProvisao_FK1
		/// </summary>

		[XmlIgnore]
		public TabelaProvisaoCollection TabelaProvisaoCollectionByIdEventoProvisao
		{
			get
			{
				if(this._TabelaProvisaoCollectionByIdEventoProvisao == null)
				{
					this._TabelaProvisaoCollectionByIdEventoProvisao = new TabelaProvisaoCollection();
					this._TabelaProvisaoCollectionByIdEventoProvisao.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("TabelaProvisaoCollectionByIdEventoProvisao", this._TabelaProvisaoCollectionByIdEventoProvisao);
				
					if(this.IdEvento != null)
					{
						this._TabelaProvisaoCollectionByIdEventoProvisao.Query.Where(this._TabelaProvisaoCollectionByIdEventoProvisao.Query.IdEventoProvisao == this.IdEvento);
						this._TabelaProvisaoCollectionByIdEventoProvisao.Query.Load();

						// Auto-hookup Foreign Keys
						this._TabelaProvisaoCollectionByIdEventoProvisao.fks.Add(TabelaProvisaoMetadata.ColumnNames.IdEventoProvisao, this.IdEvento);
					}
				}

				return this._TabelaProvisaoCollectionByIdEventoProvisao;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._TabelaProvisaoCollectionByIdEventoProvisao != null) 
				{ 
					this.RemovePostSave("TabelaProvisaoCollectionByIdEventoProvisao"); 
					this._TabelaProvisaoCollectionByIdEventoProvisao = null;
					
				} 
			} 			
		}

		private TabelaProvisaoCollection _TabelaProvisaoCollectionByIdEventoProvisao;
		#endregion

				
		#region TabelaProvisaoCollectionByIdEventoPagamento - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - ContabRoteiro_TabelaProvisao_FK2
		/// </summary>

		[XmlIgnore]
		public TabelaProvisaoCollection TabelaProvisaoCollectionByIdEventoPagamento
		{
			get
			{
				if(this._TabelaProvisaoCollectionByIdEventoPagamento == null)
				{
					this._TabelaProvisaoCollectionByIdEventoPagamento = new TabelaProvisaoCollection();
					this._TabelaProvisaoCollectionByIdEventoPagamento.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("TabelaProvisaoCollectionByIdEventoPagamento", this._TabelaProvisaoCollectionByIdEventoPagamento);
				
					if(this.IdEvento != null)
					{
						this._TabelaProvisaoCollectionByIdEventoPagamento.Query.Where(this._TabelaProvisaoCollectionByIdEventoPagamento.Query.IdEventoPagamento == this.IdEvento);
						this._TabelaProvisaoCollectionByIdEventoPagamento.Query.Load();

						// Auto-hookup Foreign Keys
						this._TabelaProvisaoCollectionByIdEventoPagamento.fks.Add(TabelaProvisaoMetadata.ColumnNames.IdEventoPagamento, this.IdEvento);
					}
				}

				return this._TabelaProvisaoCollectionByIdEventoPagamento;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._TabelaProvisaoCollectionByIdEventoPagamento != null) 
				{ 
					this.RemovePostSave("TabelaProvisaoCollectionByIdEventoPagamento"); 
					this._TabelaProvisaoCollectionByIdEventoPagamento = null;
					
				} 
			} 			
		}

		private TabelaProvisaoCollection _TabelaProvisaoCollectionByIdEventoPagamento;
		#endregion

				
		#region TabelaTaxaAdministracaoCollectionByIdEventoProvisao - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - ContabRoteiro_TabelaTaxaAdministracao_FK1
		/// </summary>

		[XmlIgnore]
		public TabelaTaxaAdministracaoCollection TabelaTaxaAdministracaoCollectionByIdEventoProvisao
		{
			get
			{
				if(this._TabelaTaxaAdministracaoCollectionByIdEventoProvisao == null)
				{
					this._TabelaTaxaAdministracaoCollectionByIdEventoProvisao = new TabelaTaxaAdministracaoCollection();
					this._TabelaTaxaAdministracaoCollectionByIdEventoProvisao.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("TabelaTaxaAdministracaoCollectionByIdEventoProvisao", this._TabelaTaxaAdministracaoCollectionByIdEventoProvisao);
				
					if(this.IdEvento != null)
					{
						this._TabelaTaxaAdministracaoCollectionByIdEventoProvisao.Query.Where(this._TabelaTaxaAdministracaoCollectionByIdEventoProvisao.Query.IdEventoProvisao == this.IdEvento);
						this._TabelaTaxaAdministracaoCollectionByIdEventoProvisao.Query.Load();

						// Auto-hookup Foreign Keys
						this._TabelaTaxaAdministracaoCollectionByIdEventoProvisao.fks.Add(TabelaTaxaAdministracaoMetadata.ColumnNames.IdEventoProvisao, this.IdEvento);
					}
				}

				return this._TabelaTaxaAdministracaoCollectionByIdEventoProvisao;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._TabelaTaxaAdministracaoCollectionByIdEventoProvisao != null) 
				{ 
					this.RemovePostSave("TabelaTaxaAdministracaoCollectionByIdEventoProvisao"); 
					this._TabelaTaxaAdministracaoCollectionByIdEventoProvisao = null;
					
				} 
			} 			
		}

		private TabelaTaxaAdministracaoCollection _TabelaTaxaAdministracaoCollectionByIdEventoProvisao;
		#endregion

				
		#region TabelaTaxaAdministracaoCollectionByIdEventoPagamento - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - ContabRoteiro_TabelaTaxaAdministracao_FK2
		/// </summary>

		[XmlIgnore]
		public TabelaTaxaAdministracaoCollection TabelaTaxaAdministracaoCollectionByIdEventoPagamento
		{
			get
			{
				if(this._TabelaTaxaAdministracaoCollectionByIdEventoPagamento == null)
				{
					this._TabelaTaxaAdministracaoCollectionByIdEventoPagamento = new TabelaTaxaAdministracaoCollection();
					this._TabelaTaxaAdministracaoCollectionByIdEventoPagamento.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("TabelaTaxaAdministracaoCollectionByIdEventoPagamento", this._TabelaTaxaAdministracaoCollectionByIdEventoPagamento);
				
					if(this.IdEvento != null)
					{
						this._TabelaTaxaAdministracaoCollectionByIdEventoPagamento.Query.Where(this._TabelaTaxaAdministracaoCollectionByIdEventoPagamento.Query.IdEventoPagamento == this.IdEvento);
						this._TabelaTaxaAdministracaoCollectionByIdEventoPagamento.Query.Load();

						// Auto-hookup Foreign Keys
						this._TabelaTaxaAdministracaoCollectionByIdEventoPagamento.fks.Add(TabelaTaxaAdministracaoMetadata.ColumnNames.IdEventoPagamento, this.IdEvento);
					}
				}

				return this._TabelaTaxaAdministracaoCollectionByIdEventoPagamento;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._TabelaTaxaAdministracaoCollectionByIdEventoPagamento != null) 
				{ 
					this.RemovePostSave("TabelaTaxaAdministracaoCollectionByIdEventoPagamento"); 
					this._TabelaTaxaAdministracaoCollectionByIdEventoPagamento = null;
					
				} 
			} 			
		}

		private TabelaTaxaAdministracaoCollection _TabelaTaxaAdministracaoCollectionByIdEventoPagamento;
		#endregion

				
		#region TabelaTaxaPerformanceCollectionByIdEventoProvisao - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - ContabRoteiro_TabelaTaxaPerformance_FK1
		/// </summary>

		[XmlIgnore]
		public TabelaTaxaPerformanceCollection TabelaTaxaPerformanceCollectionByIdEventoProvisao
		{
			get
			{
				if(this._TabelaTaxaPerformanceCollectionByIdEventoProvisao == null)
				{
					this._TabelaTaxaPerformanceCollectionByIdEventoProvisao = new TabelaTaxaPerformanceCollection();
					this._TabelaTaxaPerformanceCollectionByIdEventoProvisao.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("TabelaTaxaPerformanceCollectionByIdEventoProvisao", this._TabelaTaxaPerformanceCollectionByIdEventoProvisao);
				
					if(this.IdEvento != null)
					{
						this._TabelaTaxaPerformanceCollectionByIdEventoProvisao.Query.Where(this._TabelaTaxaPerformanceCollectionByIdEventoProvisao.Query.IdEventoProvisao == this.IdEvento);
						this._TabelaTaxaPerformanceCollectionByIdEventoProvisao.Query.Load();

						// Auto-hookup Foreign Keys
						this._TabelaTaxaPerformanceCollectionByIdEventoProvisao.fks.Add(TabelaTaxaPerformanceMetadata.ColumnNames.IdEventoProvisao, this.IdEvento);
					}
				}

				return this._TabelaTaxaPerformanceCollectionByIdEventoProvisao;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._TabelaTaxaPerformanceCollectionByIdEventoProvisao != null) 
				{ 
					this.RemovePostSave("TabelaTaxaPerformanceCollectionByIdEventoProvisao"); 
					this._TabelaTaxaPerformanceCollectionByIdEventoProvisao = null;
					
				} 
			} 			
		}

		private TabelaTaxaPerformanceCollection _TabelaTaxaPerformanceCollectionByIdEventoProvisao;
		#endregion

				
		#region TabelaTaxaPerformanceCollectionByIdEventoPagamento - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - ContabRoteiro_TabelaTaxaPerformance_FK2
		/// </summary>

		[XmlIgnore]
		public TabelaTaxaPerformanceCollection TabelaTaxaPerformanceCollectionByIdEventoPagamento
		{
			get
			{
				if(this._TabelaTaxaPerformanceCollectionByIdEventoPagamento == null)
				{
					this._TabelaTaxaPerformanceCollectionByIdEventoPagamento = new TabelaTaxaPerformanceCollection();
					this._TabelaTaxaPerformanceCollectionByIdEventoPagamento.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("TabelaTaxaPerformanceCollectionByIdEventoPagamento", this._TabelaTaxaPerformanceCollectionByIdEventoPagamento);
				
					if(this.IdEvento != null)
					{
						this._TabelaTaxaPerformanceCollectionByIdEventoPagamento.Query.Where(this._TabelaTaxaPerformanceCollectionByIdEventoPagamento.Query.IdEventoPagamento == this.IdEvento);
						this._TabelaTaxaPerformanceCollectionByIdEventoPagamento.Query.Load();

						// Auto-hookup Foreign Keys
						this._TabelaTaxaPerformanceCollectionByIdEventoPagamento.fks.Add(TabelaTaxaPerformanceMetadata.ColumnNames.IdEventoPagamento, this.IdEvento);
					}
				}

				return this._TabelaTaxaPerformanceCollectionByIdEventoPagamento;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._TabelaTaxaPerformanceCollectionByIdEventoPagamento != null) 
				{ 
					this.RemovePostSave("TabelaTaxaPerformanceCollectionByIdEventoPagamento"); 
					this._TabelaTaxaPerformanceCollectionByIdEventoPagamento = null;
					
				} 
			} 			
		}

		private TabelaTaxaPerformanceCollection _TabelaTaxaPerformanceCollectionByIdEventoPagamento;
		#endregion

				
		#region UpToContabCentroCustoByIdCentroCusto - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - ContabCentroCusto_ContabRoteiro_FK1
		/// </summary>

		[XmlIgnore]
		public ContabCentroCusto UpToContabCentroCustoByIdCentroCusto
		{
			get
			{
				if(this._UpToContabCentroCustoByIdCentroCusto == null
					&& IdCentroCusto != null					)
				{
					this._UpToContabCentroCustoByIdCentroCusto = new ContabCentroCusto();
					this._UpToContabCentroCustoByIdCentroCusto.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToContabCentroCustoByIdCentroCusto", this._UpToContabCentroCustoByIdCentroCusto);
					this._UpToContabCentroCustoByIdCentroCusto.Query.Where(this._UpToContabCentroCustoByIdCentroCusto.Query.IdCentroCusto == this.IdCentroCusto);
					this._UpToContabCentroCustoByIdCentroCusto.Query.Load();
				}

				return this._UpToContabCentroCustoByIdCentroCusto;
			}
			
			set
			{
				this.RemovePreSave("UpToContabCentroCustoByIdCentroCusto");
				

				if(value == null)
				{
					this.IdCentroCusto = null;
					this._UpToContabCentroCustoByIdCentroCusto = null;
				}
				else
				{
					this.IdCentroCusto = value.IdCentroCusto;
					this._UpToContabCentroCustoByIdCentroCusto = value;
					this.SetPreSave("UpToContabCentroCustoByIdCentroCusto", this._UpToContabCentroCustoByIdCentroCusto);
				}
				
			}
		}
		#endregion
		

				
		#region UpToContabContaByIdContaCredito - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - ContabRoteiro_ContabConta_FK1
		/// </summary>

		[XmlIgnore]
		public ContabConta UpToContabContaByIdContaCredito
		{
			get
			{
				if(this._UpToContabContaByIdContaCredito == null
					&& IdContaCredito != null					)
				{
					this._UpToContabContaByIdContaCredito = new ContabConta();
					this._UpToContabContaByIdContaCredito.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToContabContaByIdContaCredito", this._UpToContabContaByIdContaCredito);
					this._UpToContabContaByIdContaCredito.Query.Where(this._UpToContabContaByIdContaCredito.Query.IdConta == this.IdContaCredito);
					this._UpToContabContaByIdContaCredito.Query.Load();
				}

				return this._UpToContabContaByIdContaCredito;
			}
			
			set
			{
				this.RemovePreSave("UpToContabContaByIdContaCredito");
				

				if(value == null)
				{
					this.IdContaCredito = null;
					this._UpToContabContaByIdContaCredito = null;
				}
				else
				{
					this.IdContaCredito = value.IdConta;
					this._UpToContabContaByIdContaCredito = value;
					this.SetPreSave("UpToContabContaByIdContaCredito", this._UpToContabContaByIdContaCredito);
				}
				
			}
		}
		#endregion
		

				
		#region UpToContabContaByIdContaDebito - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - ContabRoteiro_ContabConta_FK2
		/// </summary>

		[XmlIgnore]
		public ContabConta UpToContabContaByIdContaDebito
		{
			get
			{
				if(this._UpToContabContaByIdContaDebito == null
					&& IdContaDebito != null					)
				{
					this._UpToContabContaByIdContaDebito = new ContabConta();
					this._UpToContabContaByIdContaDebito.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToContabContaByIdContaDebito", this._UpToContabContaByIdContaDebito);
					this._UpToContabContaByIdContaDebito.Query.Where(this._UpToContabContaByIdContaDebito.Query.IdConta == this.IdContaDebito);
					this._UpToContabContaByIdContaDebito.Query.Load();
				}

				return this._UpToContabContaByIdContaDebito;
			}
			
			set
			{
				this.RemovePreSave("UpToContabContaByIdContaDebito");
				

				if(value == null)
				{
					this.IdContaDebito = null;
					this._UpToContabContaByIdContaDebito = null;
				}
				else
				{
					this.IdContaDebito = value.IdConta;
					this._UpToContabContaByIdContaDebito = value;
					this.SetPreSave("UpToContabContaByIdContaDebito", this._UpToContabContaByIdContaDebito);
				}
				
			}
		}
		#endregion
		

				
		#region UpToContabPlanoByIdPlano - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - ContabPlano_ContabRoteiro_FK1
		/// </summary>

		[XmlIgnore]
		public ContabPlano UpToContabPlanoByIdPlano
		{
			get
			{
				if(this._UpToContabPlanoByIdPlano == null
					&& IdPlano != null					)
				{
					this._UpToContabPlanoByIdPlano = new ContabPlano();
					this._UpToContabPlanoByIdPlano.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToContabPlanoByIdPlano", this._UpToContabPlanoByIdPlano);
					this._UpToContabPlanoByIdPlano.Query.Where(this._UpToContabPlanoByIdPlano.Query.IdPlano == this.IdPlano);
					this._UpToContabPlanoByIdPlano.Query.Load();
				}

				return this._UpToContabPlanoByIdPlano;
			}
			
			set
			{
				this.RemovePreSave("UpToContabPlanoByIdPlano");
				

				if(value == null)
				{
					this.IdPlano = null;
					this._UpToContabPlanoByIdPlano = null;
				}
				else
				{
					this.IdPlano = value.IdPlano;
					this._UpToContabPlanoByIdPlano = value;
					this.SetPreSave("UpToContabPlanoByIdPlano", this._UpToContabPlanoByIdPlano);
				}
				
			}
		}
		#endregion
		

		
		/// <summary>
		/// Used internally by the entity's hierarchical properties.
		/// </summary>
		protected override List<esPropertyDescriptor> GetHierarchicalProperties()
		{
			List<esPropertyDescriptor> props = new List<esPropertyDescriptor>();
			
			props.Add(new esPropertyDescriptor(this, "TabelaProvisaoCollectionByIdEventoProvisao", typeof(TabelaProvisaoCollection), new TabelaProvisao()));
			props.Add(new esPropertyDescriptor(this, "TabelaProvisaoCollectionByIdEventoPagamento", typeof(TabelaProvisaoCollection), new TabelaProvisao()));
			props.Add(new esPropertyDescriptor(this, "TabelaTaxaAdministracaoCollectionByIdEventoProvisao", typeof(TabelaTaxaAdministracaoCollection), new TabelaTaxaAdministracao()));
			props.Add(new esPropertyDescriptor(this, "TabelaTaxaAdministracaoCollectionByIdEventoPagamento", typeof(TabelaTaxaAdministracaoCollection), new TabelaTaxaAdministracao()));
			props.Add(new esPropertyDescriptor(this, "TabelaTaxaPerformanceCollectionByIdEventoProvisao", typeof(TabelaTaxaPerformanceCollection), new TabelaTaxaPerformance()));
			props.Add(new esPropertyDescriptor(this, "TabelaTaxaPerformanceCollectionByIdEventoPagamento", typeof(TabelaTaxaPerformanceCollection), new TabelaTaxaPerformance()));
		
			return props;
		}	
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PreSave.
		/// </summary>
		protected override void ApplyPreSaveKeys()
		{
			if(!this.es.IsDeleted && this._UpToContabCentroCustoByIdCentroCusto != null)
			{
				this.IdCentroCusto = this._UpToContabCentroCustoByIdCentroCusto.IdCentroCusto;
			}
			if(!this.es.IsDeleted && this._UpToContabContaByIdContaCredito != null)
			{
				this.IdContaCredito = this._UpToContabContaByIdContaCredito.IdConta;
			}
			if(!this.es.IsDeleted && this._UpToContabContaByIdContaDebito != null)
			{
				this.IdContaDebito = this._UpToContabContaByIdContaDebito.IdConta;
			}
			if(!this.es.IsDeleted && this._UpToContabPlanoByIdPlano != null)
			{
				this.IdPlano = this._UpToContabPlanoByIdPlano.IdPlano;
			}
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostSave.
		/// </summary>
		protected override void ApplyPostSaveKeys()
		{
			if(this._TabelaProvisaoCollectionByIdEventoProvisao != null)
			{
				foreach(TabelaProvisao obj in this._TabelaProvisaoCollectionByIdEventoProvisao)
				{
					if(obj.es.IsAdded)
					{
						obj.IdEventoProvisao = this.IdEvento;
					}
				}
			}
			if(this._TabelaProvisaoCollectionByIdEventoPagamento != null)
			{
				foreach(TabelaProvisao obj in this._TabelaProvisaoCollectionByIdEventoPagamento)
				{
					if(obj.es.IsAdded)
					{
						obj.IdEventoPagamento = this.IdEvento;
					}
				}
			}
			if(this._TabelaTaxaAdministracaoCollectionByIdEventoProvisao != null)
			{
				foreach(TabelaTaxaAdministracao obj in this._TabelaTaxaAdministracaoCollectionByIdEventoProvisao)
				{
					if(obj.es.IsAdded)
					{
						obj.IdEventoProvisao = this.IdEvento;
					}
				}
			}
			if(this._TabelaTaxaAdministracaoCollectionByIdEventoPagamento != null)
			{
				foreach(TabelaTaxaAdministracao obj in this._TabelaTaxaAdministracaoCollectionByIdEventoPagamento)
				{
					if(obj.es.IsAdded)
					{
						obj.IdEventoPagamento = this.IdEvento;
					}
				}
			}
			if(this._TabelaTaxaPerformanceCollectionByIdEventoProvisao != null)
			{
				foreach(TabelaTaxaPerformance obj in this._TabelaTaxaPerformanceCollectionByIdEventoProvisao)
				{
					if(obj.es.IsAdded)
					{
						obj.IdEventoProvisao = this.IdEvento;
					}
				}
			}
			if(this._TabelaTaxaPerformanceCollectionByIdEventoPagamento != null)
			{
				foreach(TabelaTaxaPerformance obj in this._TabelaTaxaPerformanceCollectionByIdEventoPagamento)
				{
					if(obj.es.IsAdded)
					{
						obj.IdEventoPagamento = this.IdEvento;
					}
				}
			}
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostOneToOneSave.
		/// </summary>
		protected override void ApplyPostOneSaveKeys()
		{
		}
		
	}



	[Serializable]
	abstract public class esContabRoteiroQuery : esDynamicQuery
	{
		override protected IMetadata Meta
		{
			get
			{
				return ContabRoteiroMetadata.Meta();
			}
		}	
		

		public esQueryItem IdEvento
		{
			get
			{
				return new esQueryItem(this, ContabRoteiroMetadata.ColumnNames.IdEvento, esSystemType.Int32);
			}
		} 
		
		public esQueryItem ContaDebito
		{
			get
			{
				return new esQueryItem(this, ContabRoteiroMetadata.ColumnNames.ContaDebito, esSystemType.String);
			}
		} 
		
		public esQueryItem ContaCredito
		{
			get
			{
				return new esQueryItem(this, ContabRoteiroMetadata.ColumnNames.ContaCredito, esSystemType.String);
			}
		} 
		
		public esQueryItem Origem
		{
			get
			{
				return new esQueryItem(this, ContabRoteiroMetadata.ColumnNames.Origem, esSystemType.Int32);
			}
		} 
		
		public esQueryItem Identificador
		{
			get
			{
				return new esQueryItem(this, ContabRoteiroMetadata.ColumnNames.Identificador, esSystemType.String);
			}
		} 
		
		public esQueryItem Descricao
		{
			get
			{
				return new esQueryItem(this, ContabRoteiroMetadata.ColumnNames.Descricao, esSystemType.String);
			}
		} 
		
		public esQueryItem IdPlano
		{
			get
			{
				return new esQueryItem(this, ContabRoteiroMetadata.ColumnNames.IdPlano, esSystemType.Int32);
			}
		} 
		
		public esQueryItem IdCentroCusto
		{
			get
			{
				return new esQueryItem(this, ContabRoteiroMetadata.ColumnNames.IdCentroCusto, esSystemType.Int32);
			}
		} 
		
		public esQueryItem ContaDebitoReduzida
		{
			get
			{
				return new esQueryItem(this, ContabRoteiroMetadata.ColumnNames.ContaDebitoReduzida, esSystemType.String);
			}
		} 
		
		public esQueryItem ContaCreditoReduzida
		{
			get
			{
				return new esQueryItem(this, ContabRoteiroMetadata.ColumnNames.ContaCreditoReduzida, esSystemType.String);
			}
		} 
		
		public esQueryItem IdContaCredito
		{
			get
			{
				return new esQueryItem(this, ContabRoteiroMetadata.ColumnNames.IdContaCredito, esSystemType.Int32);
			}
		} 
		
		public esQueryItem IdContaDebito
		{
			get
			{
				return new esQueryItem(this, ContabRoteiroMetadata.ColumnNames.IdContaDebito, esSystemType.Int32);
			}
		} 
		
	}



	[Serializable]
	[XmlType("ContabRoteiroCollection")]
	public partial class ContabRoteiroCollection : esContabRoteiroCollection, IEnumerable<ContabRoteiro>
	{
		public ContabRoteiroCollection()
		{

		}
		
		public static implicit operator List<ContabRoteiro>(ContabRoteiroCollection coll)
		{
			List<ContabRoteiro> list = new List<ContabRoteiro>();
			
			foreach (ContabRoteiro emp in coll)
			{
				list.Add(emp);
			}
			
			return list;
		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return  ContabRoteiroMetadata.Meta();
			}
		}
		
		
		
		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new ContabRoteiroQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		
		override protected esEntity CreateEntityForCollection(DataRow row)
		{
			return new ContabRoteiro(row);
		}

		override protected esEntity CreateEntity()
		{
			return new ContabRoteiro();
		}
		
		
		#endregion


		[BrowsableAttribute( false )]
		public ContabRoteiroQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new ContabRoteiroQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}

		public bool Load(ContabRoteiroQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		public ContabRoteiro AddNew()
		{
			ContabRoteiro entity = base.AddNewEntity() as ContabRoteiro;
			
			return entity;
		}

		public ContabRoteiro FindByPrimaryKey(System.Int32 idEvento)
		{
			return base.FindByPrimaryKey(idEvento) as ContabRoteiro;
		}


		#region IEnumerable<ContabRoteiro> Members

		IEnumerator<ContabRoteiro> IEnumerable<ContabRoteiro>.GetEnumerator()
		{
			System.Collections.IEnumerable enumer = this as System.Collections.IEnumerable;
			System.Collections.IEnumerator iterator = enumer.GetEnumerator();

			while(iterator.MoveNext())
			{
				yield return iterator.Current as ContabRoteiro;
			}
		}

		#endregion
		
		private ContabRoteiroQuery query;
	}


	/// <summary>
	/// Encapsulates the 'ContabRoteiro' table
	/// </summary>

	[Serializable]
	public partial class ContabRoteiro : esContabRoteiro
	{
		public ContabRoteiro()
		{

		}
	
		public ContabRoteiro(DataRow row)
			: base(row)
		{

		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return ContabRoteiroMetadata.Meta();
			}
		}
		
		
		
		override protected esContabRoteiroQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new ContabRoteiroQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		#endregion
		



		[BrowsableAttribute( false )]
		public ContabRoteiroQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new ContabRoteiroQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}
		

		public bool Load(ContabRoteiroQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		private ContabRoteiroQuery query;
	}



	[Serializable]
	public partial class ContabRoteiroQuery : esContabRoteiroQuery
	{
		public ContabRoteiroQuery()
		{

		}		
		
		public ContabRoteiroQuery(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
			
	}



	[Serializable]
	public partial class ContabRoteiroMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected ContabRoteiroMetadata()
		{
			_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(ContabRoteiroMetadata.ColumnNames.IdEvento, 0, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = ContabRoteiroMetadata.PropertyNames.IdEvento;
			c.IsInPrimaryKey = true;
			c.IsAutoIncrement = true;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ContabRoteiroMetadata.ColumnNames.ContaDebito, 1, typeof(System.String), esSystemType.String);
			c.PropertyName = ContabRoteiroMetadata.PropertyNames.ContaDebito;
			c.CharacterMaxLength = 20;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ContabRoteiroMetadata.ColumnNames.ContaCredito, 2, typeof(System.String), esSystemType.String);
			c.PropertyName = ContabRoteiroMetadata.PropertyNames.ContaCredito;
			c.CharacterMaxLength = 20;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ContabRoteiroMetadata.ColumnNames.Origem, 3, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = ContabRoteiroMetadata.PropertyNames.Origem;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ContabRoteiroMetadata.ColumnNames.Identificador, 4, typeof(System.String), esSystemType.String);
			c.PropertyName = ContabRoteiroMetadata.PropertyNames.Identificador;
			c.CharacterMaxLength = 20;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ContabRoteiroMetadata.ColumnNames.Descricao, 5, typeof(System.String), esSystemType.String);
			c.PropertyName = ContabRoteiroMetadata.PropertyNames.Descricao;
			c.CharacterMaxLength = 200;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ContabRoteiroMetadata.ColumnNames.IdPlano, 6, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = ContabRoteiroMetadata.PropertyNames.IdPlano;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ContabRoteiroMetadata.ColumnNames.IdCentroCusto, 7, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = ContabRoteiroMetadata.PropertyNames.IdCentroCusto;	
			c.NumericPrecision = 10;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ContabRoteiroMetadata.ColumnNames.ContaDebitoReduzida, 8, typeof(System.String), esSystemType.String);
			c.PropertyName = ContabRoteiroMetadata.PropertyNames.ContaDebitoReduzida;
			c.CharacterMaxLength = 8;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ContabRoteiroMetadata.ColumnNames.ContaCreditoReduzida, 9, typeof(System.String), esSystemType.String);
			c.PropertyName = ContabRoteiroMetadata.PropertyNames.ContaCreditoReduzida;
			c.CharacterMaxLength = 8;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ContabRoteiroMetadata.ColumnNames.IdContaCredito, 10, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = ContabRoteiroMetadata.PropertyNames.IdContaCredito;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ContabRoteiroMetadata.ColumnNames.IdContaDebito, 11, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = ContabRoteiroMetadata.PropertyNames.IdContaDebito;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
		}
		#endregion
	
		static public ContabRoteiroMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base._dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base._columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string IdEvento = "IdEvento";
			 public const string ContaDebito = "ContaDebito";
			 public const string ContaCredito = "ContaCredito";
			 public const string Origem = "Origem";
			 public const string Identificador = "Identificador";
			 public const string Descricao = "Descricao";
			 public const string IdPlano = "IdPlano";
			 public const string IdCentroCusto = "IdCentroCusto";
			 public const string ContaDebitoReduzida = "ContaDebitoReduzida";
			 public const string ContaCreditoReduzida = "ContaCreditoReduzida";
			 public const string IdContaCredito = "IdContaCredito";
			 public const string IdContaDebito = "IdContaDebito";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string IdEvento = "IdEvento";
			 public const string ContaDebito = "ContaDebito";
			 public const string ContaCredito = "ContaCredito";
			 public const string Origem = "Origem";
			 public const string Identificador = "Identificador";
			 public const string Descricao = "Descricao";
			 public const string IdPlano = "IdPlano";
			 public const string IdCentroCusto = "IdCentroCusto";
			 public const string ContaDebitoReduzida = "ContaDebitoReduzida";
			 public const string ContaCreditoReduzida = "ContaCreditoReduzida";
			 public const string IdContaCredito = "IdContaCredito";
			 public const string IdContaDebito = "IdContaDebito";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(ContabRoteiroMetadata))
			{
				if(ContabRoteiroMetadata.mapDelegates == null)
				{
					ContabRoteiroMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (ContabRoteiroMetadata.meta == null)
				{
					ContabRoteiroMetadata.meta = new ContabRoteiroMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();
				

				meta.AddTypeMap("IdEvento", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("ContaDebito", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("ContaCredito", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("Origem", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("Identificador", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("Descricao", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("IdPlano", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("IdCentroCusto", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("ContaDebitoReduzida", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("ContaCreditoReduzida", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("IdContaCredito", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("IdContaDebito", new esTypeMap("int", "System.Int32"));			
				
				
				
				meta.Source = "ContabRoteiro";
				meta.Destination = "ContabRoteiro";
				
				meta.spInsert = "proc_ContabRoteiroInsert";				
				meta.spUpdate = "proc_ContabRoteiroUpdate";		
				meta.spDelete = "proc_ContabRoteiroDelete";
				meta.spLoadAll = "proc_ContabRoteiroLoadAll";
				meta.spLoadByPrimaryKey = "proc_ContabRoteiroLoadByPrimaryKey";
				
				this._providerMetadataMaps["esDefault"] = meta;
			}
			
			return this._providerMetadataMaps["esDefault"];
		}

		#endregion

		static private ContabRoteiroMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
