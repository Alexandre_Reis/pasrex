/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 15/07/2013 21:55:28
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;


using EntitySpaces.Interfaces;
using EntitySpaces.Core;





































		



			
					


		

		
		
		

		

























		






	

















				
		
		




		








		
				
				












	

















					
								
											
	










		





		




				
				









using Financial.Investidor;
				




		

		
		
		
		
		





namespace Financial.Contabil
{

	[Serializable]
	abstract public class esContabLancamentoCollection : esEntityCollection
	{
		public esContabLancamentoCollection()
		{

		}

		protected override string GetCollectionName()
		{
			return "ContabLancamentoCollection";
		}

		#region Query Logic
		protected void InitQuery(esContabLancamentoQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntityCollection)this).Connection;
		}

		protected bool OnQueryLoaded(DataTable table)
		{
			this.PopulateCollection(table);
			return (this.RowCount > 0) ? true : false;
		}
		
		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery(query as esContabLancamentoQuery);
		}
		#endregion
		
		virtual public ContabLancamento DetachEntity(ContabLancamento entity)
		{
			return base.DetachEntity(entity) as ContabLancamento;
		}
		
		virtual public ContabLancamento AttachEntity(ContabLancamento entity)
		{
			return base.AttachEntity(entity) as ContabLancamento;
		}
		
		virtual public void Combine(ContabLancamentoCollection collection)
		{
			base.Combine(collection);
		}
		
		new public ContabLancamento this[int index]
		{
			get
			{
				return base[index] as ContabLancamento;
			}
		}

		public override Type GetEntityType()
		{
			return typeof(ContabLancamento);
		}
	}



	[Serializable]
	abstract public class esContabLancamento : esEntity
	{
		/// <summary>
		/// Used internally by the entity's DynamicQuery mechanism.
		/// </summary>
		virtual protected esContabLancamentoQuery GetDynamicQuery()
		{
			return null;
		}

		public esContabLancamento()
		{

		}

		public esContabLancamento(DataRow row)
			: base(row)
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.Int32 idLancamento)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idLancamento);
			else
				return LoadByPrimaryKeyStoredProcedure(idLancamento);
		}

		/// <summary>
		/// Loads an entity by primary key
		/// </summary>
		/// <remarks>
		/// EntitySpaces requires primary keys be defined on all tables.
		/// If a table does not have a primary key set,
		/// this method will not compile.
		/// Does not support sqlAcessType. It only works with DynamicQuery
		/// </remarks>
		/// <param name="fieldsToReturn">Fields desired</param>
		public virtual bool LoadByPrimaryKey(List<esQueryItem> fieldsToReturn, System.Int32 idLancamento)
		{
			esQueryItem[] fields = fieldsToReturn.ToArray();			
			esContabLancamentoQuery query = this.GetDynamicQuery();
			query
			    .Select(fields)
			    .Where(query.IdLancamento == idLancamento);
		
			return query.Load();
		}
		
		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.Int32 idLancamento)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idLancamento);
			else
				return LoadByPrimaryKeyStoredProcedure(idLancamento);
		}

		private bool LoadByPrimaryKeyDynamic(System.Int32 idLancamento)
		{
			esContabLancamentoQuery query = this.GetDynamicQuery();
			query.Where(query.IdLancamento == idLancamento);
			return query.Load();
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.Int32 idLancamento)
		{
			esParameters parms = new esParameters();
			parms.Add("IdLancamento",idLancamento);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		
		
		#region Properties
		
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}

		public override void SetProperty(string name, object value)
		{
			if(this.Row == null) this.AddNew();
			
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value.GetType().ToString() == "System.String")
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "IdLancamento": this.str.IdLancamento = (string)value; break;							
						case "IdCliente": this.str.IdCliente = (string)value; break;							
						case "DataLancamento": this.str.DataLancamento = (string)value; break;							
						case "DataRegistro": this.str.DataRegistro = (string)value; break;							
						case "IdContaDebito": this.str.IdContaDebito = (string)value; break;							
						case "IdContaCredito": this.str.IdContaCredito = (string)value; break;							
						case "Valor": this.str.Valor = (string)value; break;							
						case "ContaDebito": this.str.ContaDebito = (string)value; break;							
						case "ContaCredito": this.str.ContaCredito = (string)value; break;							
						case "Origem": this.str.Origem = (string)value; break;							
						case "Identificador": this.str.Identificador = (string)value; break;							
						case "Descricao": this.str.Descricao = (string)value; break;							
						case "IdPlano": this.str.IdPlano = (string)value; break;							
						case "Fonte": this.str.Fonte = (string)value; break;							
						case "IdCentroCusto": this.str.IdCentroCusto = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "IdLancamento":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdLancamento = (System.Int32?)value;
							break;
						
						case "IdCliente":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdCliente = (System.Int32?)value;
							break;
						
						case "DataLancamento":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.DataLancamento = (System.DateTime?)value;
							break;
						
						case "DataRegistro":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.DataRegistro = (System.DateTime?)value;
							break;
						
						case "IdContaDebito":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdContaDebito = (System.Int32?)value;
							break;
						
						case "IdContaCredito":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdContaCredito = (System.Int32?)value;
							break;
						
						case "Valor":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.Valor = (System.Decimal?)value;
							break;
						
						case "Origem":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.Origem = (System.Int32?)value;
							break;
						
						case "IdPlano":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdPlano = (System.Int32?)value;
							break;
						
						case "Fonte":
						
							if (value == null || value.GetType().ToString() == "System.Byte")
								this.Fonte = (System.Byte?)value;
							break;
						
						case "IdCentroCusto":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdCentroCusto = (System.Int32?)value;
							break;
					

						default:
							break;
					}
				}
			}
			else if(this.Row.Table.Columns.Contains(name))
			{
				this.Row[name] = value;
			}
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}
		
		
		/// <summary>
		/// Maps to ContabLancamento.IdLancamento
		/// </summary>
		virtual public System.Int32? IdLancamento
		{
			get
			{
				return base.GetSystemInt32(ContabLancamentoMetadata.ColumnNames.IdLancamento);
			}
			
			set
			{
				base.SetSystemInt32(ContabLancamentoMetadata.ColumnNames.IdLancamento, value);
			}
		}
		
		/// <summary>
		/// Maps to ContabLancamento.IdCliente
		/// </summary>
		virtual public System.Int32? IdCliente
		{
			get
			{
				return base.GetSystemInt32(ContabLancamentoMetadata.ColumnNames.IdCliente);
			}
			
			set
			{
				if(base.SetSystemInt32(ContabLancamentoMetadata.ColumnNames.IdCliente, value))
				{
					this._UpToClienteByIdCliente = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to ContabLancamento.DataLancamento
		/// </summary>
		virtual public System.DateTime? DataLancamento
		{
			get
			{
				return base.GetSystemDateTime(ContabLancamentoMetadata.ColumnNames.DataLancamento);
			}
			
			set
			{
				base.SetSystemDateTime(ContabLancamentoMetadata.ColumnNames.DataLancamento, value);
			}
		}
		
		/// <summary>
		/// Maps to ContabLancamento.DataRegistro
		/// </summary>
		virtual public System.DateTime? DataRegistro
		{
			get
			{
				return base.GetSystemDateTime(ContabLancamentoMetadata.ColumnNames.DataRegistro);
			}
			
			set
			{
				base.SetSystemDateTime(ContabLancamentoMetadata.ColumnNames.DataRegistro, value);
			}
		}
		
		/// <summary>
		/// Maps to ContabLancamento.IdContaDebito
		/// </summary>
		virtual public System.Int32? IdContaDebito
		{
			get
			{
				return base.GetSystemInt32(ContabLancamentoMetadata.ColumnNames.IdContaDebito);
			}
			
			set
			{
				if(base.SetSystemInt32(ContabLancamentoMetadata.ColumnNames.IdContaDebito, value))
				{
					this._UpToContabContaByIdContaDebito = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to ContabLancamento.IdContaCredito
		/// </summary>
		virtual public System.Int32? IdContaCredito
		{
			get
			{
				return base.GetSystemInt32(ContabLancamentoMetadata.ColumnNames.IdContaCredito);
			}
			
			set
			{
				if(base.SetSystemInt32(ContabLancamentoMetadata.ColumnNames.IdContaCredito, value))
				{
					this._UpToContabContaByIdContaCredito = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to ContabLancamento.Valor
		/// </summary>
		virtual public System.Decimal? Valor
		{
			get
			{
				return base.GetSystemDecimal(ContabLancamentoMetadata.ColumnNames.Valor);
			}
			
			set
			{
				base.SetSystemDecimal(ContabLancamentoMetadata.ColumnNames.Valor, value);
			}
		}
		
		/// <summary>
		/// Maps to ContabLancamento.ContaDebito
		/// </summary>
		virtual public System.String ContaDebito
		{
			get
			{
				return base.GetSystemString(ContabLancamentoMetadata.ColumnNames.ContaDebito);
			}
			
			set
			{
				base.SetSystemString(ContabLancamentoMetadata.ColumnNames.ContaDebito, value);
			}
		}
		
		/// <summary>
		/// Maps to ContabLancamento.ContaCredito
		/// </summary>
		virtual public System.String ContaCredito
		{
			get
			{
				return base.GetSystemString(ContabLancamentoMetadata.ColumnNames.ContaCredito);
			}
			
			set
			{
				base.SetSystemString(ContabLancamentoMetadata.ColumnNames.ContaCredito, value);
			}
		}
		
		/// <summary>
		/// Maps to ContabLancamento.Origem
		/// </summary>
		virtual public System.Int32? Origem
		{
			get
			{
				return base.GetSystemInt32(ContabLancamentoMetadata.ColumnNames.Origem);
			}
			
			set
			{
				base.SetSystemInt32(ContabLancamentoMetadata.ColumnNames.Origem, value);
			}
		}
		
		/// <summary>
		/// Maps to ContabLancamento.Identificador
		/// </summary>
		virtual public System.String Identificador
		{
			get
			{
				return base.GetSystemString(ContabLancamentoMetadata.ColumnNames.Identificador);
			}
			
			set
			{
				base.SetSystemString(ContabLancamentoMetadata.ColumnNames.Identificador, value);
			}
		}
		
		/// <summary>
		/// Maps to ContabLancamento.Descricao
		/// </summary>
		virtual public System.String Descricao
		{
			get
			{
				return base.GetSystemString(ContabLancamentoMetadata.ColumnNames.Descricao);
			}
			
			set
			{
				base.SetSystemString(ContabLancamentoMetadata.ColumnNames.Descricao, value);
			}
		}
		
		/// <summary>
		/// Maps to ContabLancamento.IdPlano
		/// </summary>
		virtual public System.Int32? IdPlano
		{
			get
			{
				return base.GetSystemInt32(ContabLancamentoMetadata.ColumnNames.IdPlano);
			}
			
			set
			{
				if(base.SetSystemInt32(ContabLancamentoMetadata.ColumnNames.IdPlano, value))
				{
					this._UpToContabPlanoByIdPlano = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to ContabLancamento.Fonte
		/// </summary>
		virtual public System.Byte? Fonte
		{
			get
			{
				return base.GetSystemByte(ContabLancamentoMetadata.ColumnNames.Fonte);
			}
			
			set
			{
				base.SetSystemByte(ContabLancamentoMetadata.ColumnNames.Fonte, value);
			}
		}
		
		/// <summary>
		/// Maps to ContabLancamento.IdCentroCusto
		/// </summary>
		virtual public System.Int32? IdCentroCusto
		{
			get
			{
				return base.GetSystemInt32(ContabLancamentoMetadata.ColumnNames.IdCentroCusto);
			}
			
			set
			{
				if(base.SetSystemInt32(ContabLancamentoMetadata.ColumnNames.IdCentroCusto, value))
				{
					this._UpToContabCentroCustoByIdCentroCusto = null;
				}
			}
		}
		
		[CLSCompliant(false)]
		internal protected Cliente _UpToClienteByIdCliente;
		[CLSCompliant(false)]
		internal protected ContabCentroCusto _UpToContabCentroCustoByIdCentroCusto;
		[CLSCompliant(false)]
		internal protected ContabConta _UpToContabContaByIdContaCredito;
		[CLSCompliant(false)]
		internal protected ContabConta _UpToContabContaByIdContaDebito;
		[CLSCompliant(false)]
		internal protected ContabPlano _UpToContabPlanoByIdPlano;
		#endregion	

		#region String Properties


		[BrowsableAttribute( false )]
		public esStrings str
		{
			get
			{
				if (esstrings == null)
				{
					esstrings = new esStrings(this);
				}
				return esstrings;
			}
		}


		[Serializable]
		sealed public class esStrings
		{
			public esStrings(esContabLancamento entity)
			{
				this.entity = entity;
			}
			
	
			public System.String IdLancamento
			{
				get
				{
					System.Int32? data = entity.IdLancamento;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdLancamento = null;
					else entity.IdLancamento = Convert.ToInt32(value);
				}
			}
				
			public System.String IdCliente
			{
				get
				{
					System.Int32? data = entity.IdCliente;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdCliente = null;
					else entity.IdCliente = Convert.ToInt32(value);
				}
			}
				
			public System.String DataLancamento
			{
				get
				{
					System.DateTime? data = entity.DataLancamento;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DataLancamento = null;
					else entity.DataLancamento = Convert.ToDateTime(value);
				}
			}
				
			public System.String DataRegistro
			{
				get
				{
					System.DateTime? data = entity.DataRegistro;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DataRegistro = null;
					else entity.DataRegistro = Convert.ToDateTime(value);
				}
			}
				
			public System.String IdContaDebito
			{
				get
				{
					System.Int32? data = entity.IdContaDebito;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdContaDebito = null;
					else entity.IdContaDebito = Convert.ToInt32(value);
				}
			}
				
			public System.String IdContaCredito
			{
				get
				{
					System.Int32? data = entity.IdContaCredito;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdContaCredito = null;
					else entity.IdContaCredito = Convert.ToInt32(value);
				}
			}
				
			public System.String Valor
			{
				get
				{
					System.Decimal? data = entity.Valor;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Valor = null;
					else entity.Valor = Convert.ToDecimal(value);
				}
			}
				
			public System.String ContaDebito
			{
				get
				{
					System.String data = entity.ContaDebito;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ContaDebito = null;
					else entity.ContaDebito = Convert.ToString(value);
				}
			}
				
			public System.String ContaCredito
			{
				get
				{
					System.String data = entity.ContaCredito;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ContaCredito = null;
					else entity.ContaCredito = Convert.ToString(value);
				}
			}
				
			public System.String Origem
			{
				get
				{
					System.Int32? data = entity.Origem;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Origem = null;
					else entity.Origem = Convert.ToInt32(value);
				}
			}
				
			public System.String Identificador
			{
				get
				{
					System.String data = entity.Identificador;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Identificador = null;
					else entity.Identificador = Convert.ToString(value);
				}
			}
				
			public System.String Descricao
			{
				get
				{
					System.String data = entity.Descricao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Descricao = null;
					else entity.Descricao = Convert.ToString(value);
				}
			}
				
			public System.String IdPlano
			{
				get
				{
					System.Int32? data = entity.IdPlano;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdPlano = null;
					else entity.IdPlano = Convert.ToInt32(value);
				}
			}
				
			public System.String Fonte
			{
				get
				{
					System.Byte? data = entity.Fonte;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Fonte = null;
					else entity.Fonte = Convert.ToByte(value);
				}
			}
				
			public System.String IdCentroCusto
			{
				get
				{
					System.Int32? data = entity.IdCentroCusto;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdCentroCusto = null;
					else entity.IdCentroCusto = Convert.ToInt32(value);
				}
			}
			

			private esContabLancamento entity;
		}
		#endregion

		#region Query Logic
		protected void InitQuery(esContabLancamentoQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntity)this).Connection;
		}

		[System.Diagnostics.DebuggerNonUserCode]
		protected bool OnQueryLoaded(DataTable table)
		{
			bool dataFound = this.PopulateEntity(table);

			if (this.RowCount > 1)
			{
				throw new Exception("esContabLancamento can only hold one record of data");
			}

			return dataFound;
		}
		#endregion
		
		[NonSerialized]
		private esStrings esstrings;
	}


	
	public partial class ContabLancamento : esContabLancamento
	{

				
		#region UpToClienteByIdCliente - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - Cliente_ContabLancamento_FK1
		/// </summary>

		[XmlIgnore]
		public Cliente UpToClienteByIdCliente
		{
			get
			{
				if(this._UpToClienteByIdCliente == null
					&& IdCliente != null					)
				{
					this._UpToClienteByIdCliente = new Cliente();
					this._UpToClienteByIdCliente.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToClienteByIdCliente", this._UpToClienteByIdCliente);
					this._UpToClienteByIdCliente.Query.Where(this._UpToClienteByIdCliente.Query.IdCliente == this.IdCliente);
					this._UpToClienteByIdCliente.Query.Load();
				}

				return this._UpToClienteByIdCliente;
			}
			
			set
			{
				this.RemovePreSave("UpToClienteByIdCliente");
				

				if(value == null)
				{
					this.IdCliente = null;
					this._UpToClienteByIdCliente = null;
				}
				else
				{
					this.IdCliente = value.IdCliente;
					this._UpToClienteByIdCliente = value;
					this.SetPreSave("UpToClienteByIdCliente", this._UpToClienteByIdCliente);
				}
				
			}
		}
		#endregion
		

				
		#region UpToContabCentroCustoByIdCentroCusto - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - ContabCentroCusto_ContabLancamento_FK1
		/// </summary>

		[XmlIgnore]
		public ContabCentroCusto UpToContabCentroCustoByIdCentroCusto
		{
			get
			{
				if(this._UpToContabCentroCustoByIdCentroCusto == null
					&& IdCentroCusto != null					)
				{
					this._UpToContabCentroCustoByIdCentroCusto = new ContabCentroCusto();
					this._UpToContabCentroCustoByIdCentroCusto.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToContabCentroCustoByIdCentroCusto", this._UpToContabCentroCustoByIdCentroCusto);
					this._UpToContabCentroCustoByIdCentroCusto.Query.Where(this._UpToContabCentroCustoByIdCentroCusto.Query.IdCentroCusto == this.IdCentroCusto);
					this._UpToContabCentroCustoByIdCentroCusto.Query.Load();
				}

				return this._UpToContabCentroCustoByIdCentroCusto;
			}
			
			set
			{
				this.RemovePreSave("UpToContabCentroCustoByIdCentroCusto");
				

				if(value == null)
				{
					this.IdCentroCusto = null;
					this._UpToContabCentroCustoByIdCentroCusto = null;
				}
				else
				{
					this.IdCentroCusto = value.IdCentroCusto;
					this._UpToContabCentroCustoByIdCentroCusto = value;
					this.SetPreSave("UpToContabCentroCustoByIdCentroCusto", this._UpToContabCentroCustoByIdCentroCusto);
				}
				
			}
		}
		#endregion
		

				
		#region UpToContabContaByIdContaCredito - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - ContabLancamento_ContabConta_FK1
		/// </summary>

		[XmlIgnore]
		public ContabConta UpToContabContaByIdContaCredito
		{
			get
			{
				if(this._UpToContabContaByIdContaCredito == null
					&& IdContaCredito != null					)
				{
					this._UpToContabContaByIdContaCredito = new ContabConta();
					this._UpToContabContaByIdContaCredito.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToContabContaByIdContaCredito", this._UpToContabContaByIdContaCredito);
					this._UpToContabContaByIdContaCredito.Query.Where(this._UpToContabContaByIdContaCredito.Query.IdConta == this.IdContaCredito);
					this._UpToContabContaByIdContaCredito.Query.Load();
				}

				return this._UpToContabContaByIdContaCredito;
			}
			
			set
			{
				this.RemovePreSave("UpToContabContaByIdContaCredito");
				

				if(value == null)
				{
					this.IdContaCredito = null;
					this._UpToContabContaByIdContaCredito = null;
				}
				else
				{
					this.IdContaCredito = value.IdConta;
					this._UpToContabContaByIdContaCredito = value;
					this.SetPreSave("UpToContabContaByIdContaCredito", this._UpToContabContaByIdContaCredito);
				}
				
			}
		}
		#endregion
		

				
		#region UpToContabContaByIdContaDebito - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - ContabLancamento_ContabConta_FK2
		/// </summary>

		[XmlIgnore]
		public ContabConta UpToContabContaByIdContaDebito
		{
			get
			{
				if(this._UpToContabContaByIdContaDebito == null
					&& IdContaDebito != null					)
				{
					this._UpToContabContaByIdContaDebito = new ContabConta();
					this._UpToContabContaByIdContaDebito.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToContabContaByIdContaDebito", this._UpToContabContaByIdContaDebito);
					this._UpToContabContaByIdContaDebito.Query.Where(this._UpToContabContaByIdContaDebito.Query.IdConta == this.IdContaDebito);
					this._UpToContabContaByIdContaDebito.Query.Load();
				}

				return this._UpToContabContaByIdContaDebito;
			}
			
			set
			{
				this.RemovePreSave("UpToContabContaByIdContaDebito");
				

				if(value == null)
				{
					this.IdContaDebito = null;
					this._UpToContabContaByIdContaDebito = null;
				}
				else
				{
					this.IdContaDebito = value.IdConta;
					this._UpToContabContaByIdContaDebito = value;
					this.SetPreSave("UpToContabContaByIdContaDebito", this._UpToContabContaByIdContaDebito);
				}
				
			}
		}
		#endregion
		

				
		#region UpToContabPlanoByIdPlano - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - ContabLancamento_ContabPlano_FK1
		/// </summary>

		[XmlIgnore]
		public ContabPlano UpToContabPlanoByIdPlano
		{
			get
			{
				if(this._UpToContabPlanoByIdPlano == null
					&& IdPlano != null					)
				{
					this._UpToContabPlanoByIdPlano = new ContabPlano();
					this._UpToContabPlanoByIdPlano.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToContabPlanoByIdPlano", this._UpToContabPlanoByIdPlano);
					this._UpToContabPlanoByIdPlano.Query.Where(this._UpToContabPlanoByIdPlano.Query.IdPlano == this.IdPlano);
					this._UpToContabPlanoByIdPlano.Query.Load();
				}

				return this._UpToContabPlanoByIdPlano;
			}
			
			set
			{
				this.RemovePreSave("UpToContabPlanoByIdPlano");
				

				if(value == null)
				{
					this.IdPlano = null;
					this._UpToContabPlanoByIdPlano = null;
				}
				else
				{
					this.IdPlano = value.IdPlano;
					this._UpToContabPlanoByIdPlano = value;
					this.SetPreSave("UpToContabPlanoByIdPlano", this._UpToContabPlanoByIdPlano);
				}
				
			}
		}
		#endregion
		

		
		/// <summary>
		/// Used internally by the entity's hierarchical properties.
		/// </summary>
		protected override List<esPropertyDescriptor> GetHierarchicalProperties()
		{
			List<esPropertyDescriptor> props = new List<esPropertyDescriptor>();
			
		
			return props;
		}	
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PreSave.
		/// </summary>
		protected override void ApplyPreSaveKeys()
		{
			if(!this.es.IsDeleted && this._UpToContabCentroCustoByIdCentroCusto != null)
			{
				this.IdCentroCusto = this._UpToContabCentroCustoByIdCentroCusto.IdCentroCusto;
			}
			if(!this.es.IsDeleted && this._UpToContabContaByIdContaCredito != null)
			{
				this.IdContaCredito = this._UpToContabContaByIdContaCredito.IdConta;
			}
			if(!this.es.IsDeleted && this._UpToContabContaByIdContaDebito != null)
			{
				this.IdContaDebito = this._UpToContabContaByIdContaDebito.IdConta;
			}
			if(!this.es.IsDeleted && this._UpToContabPlanoByIdPlano != null)
			{
				this.IdPlano = this._UpToContabPlanoByIdPlano.IdPlano;
			}
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostSave.
		/// </summary>
		protected override void ApplyPostSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostOneToOneSave.
		/// </summary>
		protected override void ApplyPostOneSaveKeys()
		{
		}
		
	}



	[Serializable]
	abstract public class esContabLancamentoQuery : esDynamicQuery
	{
		override protected IMetadata Meta
		{
			get
			{
				return ContabLancamentoMetadata.Meta();
			}
		}	
		

		public esQueryItem IdLancamento
		{
			get
			{
				return new esQueryItem(this, ContabLancamentoMetadata.ColumnNames.IdLancamento, esSystemType.Int32);
			}
		} 
		
		public esQueryItem IdCliente
		{
			get
			{
				return new esQueryItem(this, ContabLancamentoMetadata.ColumnNames.IdCliente, esSystemType.Int32);
			}
		} 
		
		public esQueryItem DataLancamento
		{
			get
			{
				return new esQueryItem(this, ContabLancamentoMetadata.ColumnNames.DataLancamento, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem DataRegistro
		{
			get
			{
				return new esQueryItem(this, ContabLancamentoMetadata.ColumnNames.DataRegistro, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem IdContaDebito
		{
			get
			{
				return new esQueryItem(this, ContabLancamentoMetadata.ColumnNames.IdContaDebito, esSystemType.Int32);
			}
		} 
		
		public esQueryItem IdContaCredito
		{
			get
			{
				return new esQueryItem(this, ContabLancamentoMetadata.ColumnNames.IdContaCredito, esSystemType.Int32);
			}
		} 
		
		public esQueryItem Valor
		{
			get
			{
				return new esQueryItem(this, ContabLancamentoMetadata.ColumnNames.Valor, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem ContaDebito
		{
			get
			{
				return new esQueryItem(this, ContabLancamentoMetadata.ColumnNames.ContaDebito, esSystemType.String);
			}
		} 
		
		public esQueryItem ContaCredito
		{
			get
			{
				return new esQueryItem(this, ContabLancamentoMetadata.ColumnNames.ContaCredito, esSystemType.String);
			}
		} 
		
		public esQueryItem Origem
		{
			get
			{
				return new esQueryItem(this, ContabLancamentoMetadata.ColumnNames.Origem, esSystemType.Int32);
			}
		} 
		
		public esQueryItem Identificador
		{
			get
			{
				return new esQueryItem(this, ContabLancamentoMetadata.ColumnNames.Identificador, esSystemType.String);
			}
		} 
		
		public esQueryItem Descricao
		{
			get
			{
				return new esQueryItem(this, ContabLancamentoMetadata.ColumnNames.Descricao, esSystemType.String);
			}
		} 
		
		public esQueryItem IdPlano
		{
			get
			{
				return new esQueryItem(this, ContabLancamentoMetadata.ColumnNames.IdPlano, esSystemType.Int32);
			}
		} 
		
		public esQueryItem Fonte
		{
			get
			{
				return new esQueryItem(this, ContabLancamentoMetadata.ColumnNames.Fonte, esSystemType.Byte);
			}
		} 
		
		public esQueryItem IdCentroCusto
		{
			get
			{
				return new esQueryItem(this, ContabLancamentoMetadata.ColumnNames.IdCentroCusto, esSystemType.Int32);
			}
		} 
		
	}



	[Serializable]
	[XmlType("ContabLancamentoCollection")]
	public partial class ContabLancamentoCollection : esContabLancamentoCollection, IEnumerable<ContabLancamento>
	{
		public ContabLancamentoCollection()
		{

		}
		
		public static implicit operator List<ContabLancamento>(ContabLancamentoCollection coll)
		{
			List<ContabLancamento> list = new List<ContabLancamento>();
			
			foreach (ContabLancamento emp in coll)
			{
				list.Add(emp);
			}
			
			return list;
		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return  ContabLancamentoMetadata.Meta();
			}
		}
		
		
		
		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new ContabLancamentoQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		
		override protected esEntity CreateEntityForCollection(DataRow row)
		{
			return new ContabLancamento(row);
		}

		override protected esEntity CreateEntity()
		{
			return new ContabLancamento();
		}
		
		
		#endregion


		[BrowsableAttribute( false )]
		public ContabLancamentoQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new ContabLancamentoQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}

		public bool Load(ContabLancamentoQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		public ContabLancamento AddNew()
		{
			ContabLancamento entity = base.AddNewEntity() as ContabLancamento;
			
			return entity;
		}

		public ContabLancamento FindByPrimaryKey(System.Int32 idLancamento)
		{
			return base.FindByPrimaryKey(idLancamento) as ContabLancamento;
		}


		#region IEnumerable<ContabLancamento> Members

		IEnumerator<ContabLancamento> IEnumerable<ContabLancamento>.GetEnumerator()
		{
			System.Collections.IEnumerable enumer = this as System.Collections.IEnumerable;
			System.Collections.IEnumerator iterator = enumer.GetEnumerator();

			while(iterator.MoveNext())
			{
				yield return iterator.Current as ContabLancamento;
			}
		}

		#endregion
		
		private ContabLancamentoQuery query;
	}


	/// <summary>
	/// Encapsulates the 'ContabLancamento' table
	/// </summary>

	[Serializable]
	public partial class ContabLancamento : esContabLancamento
	{
		public ContabLancamento()
		{

		}
	
		public ContabLancamento(DataRow row)
			: base(row)
		{

		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return ContabLancamentoMetadata.Meta();
			}
		}
		
		
		
		override protected esContabLancamentoQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new ContabLancamentoQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		#endregion
		



		[BrowsableAttribute( false )]
		public ContabLancamentoQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new ContabLancamentoQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}
		

		public bool Load(ContabLancamentoQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		private ContabLancamentoQuery query;
	}



	[Serializable]
	public partial class ContabLancamentoQuery : esContabLancamentoQuery
	{
		public ContabLancamentoQuery()
		{

		}		
		
		public ContabLancamentoQuery(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
			
	}



	[Serializable]
	public partial class ContabLancamentoMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected ContabLancamentoMetadata()
		{
			_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(ContabLancamentoMetadata.ColumnNames.IdLancamento, 0, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = ContabLancamentoMetadata.PropertyNames.IdLancamento;
			c.IsInPrimaryKey = true;
			c.IsAutoIncrement = true;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ContabLancamentoMetadata.ColumnNames.IdCliente, 1, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = ContabLancamentoMetadata.PropertyNames.IdCliente;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ContabLancamentoMetadata.ColumnNames.DataLancamento, 2, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = ContabLancamentoMetadata.PropertyNames.DataLancamento;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ContabLancamentoMetadata.ColumnNames.DataRegistro, 3, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = ContabLancamentoMetadata.PropertyNames.DataRegistro;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ContabLancamentoMetadata.ColumnNames.IdContaDebito, 4, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = ContabLancamentoMetadata.PropertyNames.IdContaDebito;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ContabLancamentoMetadata.ColumnNames.IdContaCredito, 5, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = ContabLancamentoMetadata.PropertyNames.IdContaCredito;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ContabLancamentoMetadata.ColumnNames.Valor, 6, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = ContabLancamentoMetadata.PropertyNames.Valor;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ContabLancamentoMetadata.ColumnNames.ContaDebito, 7, typeof(System.String), esSystemType.String);
			c.PropertyName = ContabLancamentoMetadata.PropertyNames.ContaDebito;
			c.CharacterMaxLength = 20;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ContabLancamentoMetadata.ColumnNames.ContaCredito, 8, typeof(System.String), esSystemType.String);
			c.PropertyName = ContabLancamentoMetadata.PropertyNames.ContaCredito;
			c.CharacterMaxLength = 20;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ContabLancamentoMetadata.ColumnNames.Origem, 9, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = ContabLancamentoMetadata.PropertyNames.Origem;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ContabLancamentoMetadata.ColumnNames.Identificador, 10, typeof(System.String), esSystemType.String);
			c.PropertyName = ContabLancamentoMetadata.PropertyNames.Identificador;
			c.CharacterMaxLength = 20;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ContabLancamentoMetadata.ColumnNames.Descricao, 11, typeof(System.String), esSystemType.String);
			c.PropertyName = ContabLancamentoMetadata.PropertyNames.Descricao;
			c.CharacterMaxLength = 200;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ContabLancamentoMetadata.ColumnNames.IdPlano, 12, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = ContabLancamentoMetadata.PropertyNames.IdPlano;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ContabLancamentoMetadata.ColumnNames.Fonte, 13, typeof(System.Byte), esSystemType.Byte);
			c.PropertyName = ContabLancamentoMetadata.PropertyNames.Fonte;	
			c.NumericPrecision = 3;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ContabLancamentoMetadata.ColumnNames.IdCentroCusto, 14, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = ContabLancamentoMetadata.PropertyNames.IdCentroCusto;	
			c.NumericPrecision = 10;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
		}
		#endregion
	
		static public ContabLancamentoMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base._dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base._columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string IdLancamento = "IdLancamento";
			 public const string IdCliente = "IdCliente";
			 public const string DataLancamento = "DataLancamento";
			 public const string DataRegistro = "DataRegistro";
			 public const string IdContaDebito = "IdContaDebito";
			 public const string IdContaCredito = "IdContaCredito";
			 public const string Valor = "Valor";
			 public const string ContaDebito = "ContaDebito";
			 public const string ContaCredito = "ContaCredito";
			 public const string Origem = "Origem";
			 public const string Identificador = "Identificador";
			 public const string Descricao = "Descricao";
			 public const string IdPlano = "IdPlano";
			 public const string Fonte = "Fonte";
			 public const string IdCentroCusto = "IdCentroCusto";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string IdLancamento = "IdLancamento";
			 public const string IdCliente = "IdCliente";
			 public const string DataLancamento = "DataLancamento";
			 public const string DataRegistro = "DataRegistro";
			 public const string IdContaDebito = "IdContaDebito";
			 public const string IdContaCredito = "IdContaCredito";
			 public const string Valor = "Valor";
			 public const string ContaDebito = "ContaDebito";
			 public const string ContaCredito = "ContaCredito";
			 public const string Origem = "Origem";
			 public const string Identificador = "Identificador";
			 public const string Descricao = "Descricao";
			 public const string IdPlano = "IdPlano";
			 public const string Fonte = "Fonte";
			 public const string IdCentroCusto = "IdCentroCusto";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(ContabLancamentoMetadata))
			{
				if(ContabLancamentoMetadata.mapDelegates == null)
				{
					ContabLancamentoMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (ContabLancamentoMetadata.meta == null)
				{
					ContabLancamentoMetadata.meta = new ContabLancamentoMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();
				

				meta.AddTypeMap("IdLancamento", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("IdCliente", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("DataLancamento", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("DataRegistro", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("IdContaDebito", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("IdContaCredito", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("Valor", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("ContaDebito", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("ContaCredito", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("Origem", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("Identificador", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("Descricao", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("IdPlano", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("Fonte", new esTypeMap("tinyint", "System.Byte"));
				meta.AddTypeMap("IdCentroCusto", new esTypeMap("int", "System.Int32"));			
				
				
				
				meta.Source = "ContabLancamento";
				meta.Destination = "ContabLancamento";
				
				meta.spInsert = "proc_ContabLancamentoInsert";				
				meta.spUpdate = "proc_ContabLancamentoUpdate";		
				meta.spDelete = "proc_ContabLancamentoDelete";
				meta.spLoadAll = "proc_ContabLancamentoLoadAll";
				meta.spLoadByPrimaryKey = "proc_ContabLancamentoLoadByPrimaryKey";
				
				this._providerMetadataMaps["esDefault"] = meta;
			}
			
			return this._providerMetadataMaps["esDefault"];
		}

		#endregion

		static private ContabLancamentoMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
