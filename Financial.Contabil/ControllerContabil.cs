﻿using System.Collections.Generic;
using System;
using System.Text;
using System.Data.SqlClient;
using EntitySpaces.Interfaces;
using EntitySpaces.Core;
using Financial.Investidor;
using Financial.Contabil.Enums;
using Financial.Common;
using Financial.Util;
using Financial.Common.Enums;

namespace Financial.Contabil.Controller
{
    public class ControllerContabil
    {
        public void ExecutaProcessoEventosPeriodo(int idCliente, DateTime dataInicio, DateTime dataFim, bool zeragem)
        {
            Cliente cliente = new Cliente();
            List<esQueryItem> campos = new List<esQueryItem>();
            campos.Add(cliente.Query.IdCliente);
            campos.Add(cliente.Query.Status);
            campos.Add(cliente.Query.DataDia);
            campos.Add(cliente.Query.IsProcessando);
            campos.Add(cliente.Query.DataImplantacao);
            campos.Add(cliente.Query.ContabilDataTrava);
            
            if (cliente.LoadByPrimaryKey(campos, idCliente))
            {
                if (cliente.IsProcessando == "S")
                {
                    throw new Exception("Cliente " + cliente.IdCliente.Value.ToString() +
                                    " - " + cliente.Apelido + " está sendo processado no momento. Processo cancelado.");
                }
                

                if (!Calendario.IsDiaUtil(dataInicio, LocalFeriadoFixo.Brasil, TipoFeriado.Brasil))
                {
                    throw new Exception("Data inicial não é dia útil");
                }
                
                if (!Calendario.IsDiaUtil(dataFim, LocalFeriadoFixo.Brasil, TipoFeriado.Brasil))
                {
                    throw new Exception("Data fim não é dia útil");
                }
                                

                DateTime dataDia = cliente.DataDia.Value;
                DateTime dataImplantacao = cliente.DataImplantacao.Value;
                int status = cliente.Status.Value;

                if (dataInicio < dataImplantacao)
                {
                    throw new Exception("Data início deve ser maior ou igual que a data de implantação do cliente " + idCliente);
                }

                if (dataFim > dataDia)
                {
                    throw new Exception("Data fim deve ser menor ou igual que a data dia do cliente " + idCliente);
                }

                if (cliente.ContabilDataTrava.HasValue && dataInicio <= cliente.ContabilDataTrava.Value)
                {
                    throw new Exception("Data início deve ser maior ou igual que a data de travamento contábil (" +
                                            cliente.ContabilDataTrava.Value.ToShortDateString() + "), do cliente " + idCliente);
                }

                //Inicia o processamento mudando o status IsProcessando
                cliente.IsProcessando = "S";
                cliente.Save();

                DateTime dataAux = dataInicio;

                ContabSaldo contabSaldo = new ContabSaldo();
                contabSaldo.Query.Select(contabSaldo.Query.Data.Min());
                contabSaldo.Query.Where(contabSaldo.Query.IdCliente.Equal(idCliente));
                contabSaldo.Query.Load();
                DateTime? dataSaldoInicial = contabSaldo.Data;
                
                while (DateTime.Compare(dataFim, dataAux) >= 0)
                {
                    this.ExecutaProcessoEventos(idCliente, dataAux, true, dataSaldoInicial, zeragem);

                    dataAux = Calendario.AdicionaDiaUtil(dataAux, 1, LocalFeriadoFixo.Brasil, TipoFeriado.Brasil);                    
                }

                cliente.IsProcessando = "N";
                cliente.Save();
            }
        }

        public void ExecutaProcessoEventos(int idCliente, DateTime data)
        {
            this.ExecutaProcessoEventos(idCliente, data, false, null, false);
        }

        private void ExecutaProcessoEventos(int idCliente, DateTime data, bool processamentoPeriodo, DateTime? dataSaldoInicial, bool zeragem)
        {
            if (!processamentoPeriodo)
            {
                ContabSaldo contabSaldo = new ContabSaldo();
                contabSaldo.Query.Select(contabSaldo.Query.Data.Min());
                contabSaldo.Query.Where(contabSaldo.Query.IdCliente.Equal(idCliente));
                contabSaldo.Query.Load();
                dataSaldoInicial = contabSaldo.Data;
            }

            Cliente cliente = new Cliente();
            List<esQueryItem> campos = new List<esQueryItem>();
            campos.Add(cliente.Query.IdCliente);
            campos.Add(cliente.Query.DataDia);
            campos.Add(cliente.Query.CalculaContabil);
            campos.Add(cliente.Query.ContabilAcoes);
            campos.Add(cliente.Query.ContabilFundos);
            campos.Add(cliente.Query.ContabilBMF);
            campos.Add(cliente.Query.ContabilLiquidacao);
            campos.Add(cliente.Query.ContabilDaytrade);
            campos.Add(cliente.Query.IdPlano);
            campos.Add(cliente.Query.ApuraGanhoRV);
            cliente.LoadByPrimaryKey(campos, idCliente);

            if (cliente.CalculaContabil == "S")
            {
                ContabSaldo contabSaldo = new ContabSaldo();

                ContabRoteiroCollection contabRoteiroCollection = new ContabRoteiroCollection();
                contabRoteiroCollection.Query.Where(contabRoteiroCollection.Query.IdPlano.Equal(cliente.IdPlano.Value));
                contabRoteiroCollection.Query.Load();

                Dictionary<int, int> roteiroContabil = new Dictionary<int, int>();
                foreach (ContabRoteiro contabRoteiro in contabRoteiroCollection)
                {
                    if (!roteiroContabil.ContainsKey(contabRoteiro.Origem.Value))
                    {
                        roteiroContabil.Add(contabRoteiro.Origem.Value, contabRoteiro.IdEvento.Value);
                    }
                }

                ContabLancamentoCollection contabLancamentoCollection = new ContabLancamentoCollection();
                contabLancamentoCollection.Query.Select(contabLancamentoCollection.Query.IdLancamento);
                contabLancamentoCollection.Query.Where(contabLancamentoCollection.Query.IdCliente.Equal(idCliente),
                                                       contabLancamentoCollection.Query.DataRegistro.Equal(data),
                                                       contabLancamentoCollection.Query.Fonte.NotEqual((byte)FonteLancamentoContabil.Manual));
                contabLancamentoCollection.Query.Load();
                contabLancamentoCollection.MarkAllAsDeleted();
                contabLancamentoCollection.Save();

                bool diaUtilBolsa = Calendario.IsDiaUtil(data, (byte)LocalFeriadoFixo.Bovespa, TipoFeriado.Brasil);

                if (diaUtilBolsa)
                {
                    ContabBolsa contabBolsa = new ContabBolsa(roteiroContabil, contabRoteiroCollection);
                    contabBolsa.ExecutaProcesso(cliente, data, dataSaldoInicial);

                    ContabBMF contabBMF = new ContabBMF(roteiroContabil, contabRoteiroCollection);
                    contabBMF.ExecutaProcesso(cliente, data);
                }

                ContabRendaFixa contabRendaFixa = new ContabRendaFixa();
                contabRendaFixa.ExecutaProcesso(cliente, data);

                ContabFundo contabFundo = new ContabFundo();
                contabFundo.ExecutaProcesso(cliente, data);

                ContabSwap contabSwap = new ContabSwap(roteiroContabil, contabRoteiroCollection);
                contabSwap.ExecutaProcesso(cliente, data);

                ContabCotista contabCotista = new ContabCotista(roteiroContabil, contabRoteiroCollection);
                contabCotista.ExecutaProcesso(cliente, data);

                ContabDespesas contabDespesas = new ContabDespesas();
                contabDespesas.ExecutaProcesso(cliente, data);

                ContabLiquidacao contabLiquidacao = new ContabLiquidacao();
                contabLiquidacao.GeraEventosLiquidacao(cliente, data);

                if (!dataSaldoInicial.HasValue || dataSaldoInicial.Value < data)
                {                    
                    contabSaldo.CalculaSaldo(cliente, data);
                }

                if (diaUtilBolsa)
                {                    
                    ContabBolsa contabBolsa = new ContabBolsa(roteiroContabil, contabRoteiroCollection);

                    if (cliente.ContabilAcoes == (byte)TipoContabilAcoes.Consolidado)
                    {
                        contabBolsa.GeraAjusteContabilAcoes(cliente, data);
                    }
                    contabBolsa.GeraAjusteContabilOPCComprada(cliente, data);
                    contabBolsa.GeraAjusteContabilOPVComprada(cliente, data);
                    contabBolsa.GeraAjusteContabilOPCVendida(cliente, data);
                    contabBolsa.GeraAjusteContabilOPVVendida(cliente, data);
                }

                if (zeragem)
                {
                    contabSaldo = new ContabSaldo();
                    contabSaldo.ZeraSaldoResultados(cliente, data);
                }

            }
        }
    }
}