﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Financial.Investidor;
using Financial.Investidor.Enums;
using Financial.BMF;
using Financial.Bolsa;
using Financial.Bolsa.Enums;
using InterfaceSinacor.Properties;

namespace InterfaceSinacor
{
    public partial class Integra : Form
    {
        public Integra()
        {
            InitializeComponent();
            EntitySpaces.Interfaces.esProviderFactory.Factory = new EntitySpaces.LoaderMT.esDataProviderFactory();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (textData.Text == "")
            {
                MessageBox.Show("Data não preenchida.", "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1);
                return;
            }

            DateTime data = new DateTime(textData.Value.Year, textData.Value.Month, textData.Value.Day);

            Cursor.Current = Cursors.WaitCursor;

            ClienteCollection clienteCollection = new ClienteCollection();
            clienteCollection.Query.Select(clienteCollection.Query.IdCliente);
            clienteCollection.Query.Where(clienteCollection.Query.StatusAtivo.Equal((byte)StatusAtivoCliente.Ativo),
                                          clienteCollection.Query.Status.NotEqual((byte)StatusCliente.Divulgado),
                                          clienteCollection.Query.DataDia.Equal(data));
            clienteCollection.Query.Load();

            if (clienteCollection.Count == 0)
            {
                MessageBox.Show("Não há clientes ativos que estejam abertos nesta data.", "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1);
                Cursor.Current = Cursors.Default;
                return;
            }

            foreach (Cliente cliente in clienteCollection)
            {
                int idCliente = cliente.IdCliente.Value;

                bool bolsa = Convert.ToBoolean(Settings.Default.Bolsa);
                bool btc = Convert.ToBoolean(Settings.Default.BTC);
                bool bmf = Convert.ToBoolean(Settings.Default.BMF);
                bool cc = Convert.ToBoolean(Settings.Default.CC);

                if (bolsa)
                {
                    OrdemBolsa ordemBolsa = new OrdemBolsa();
                    string tipoMercado = TipoMercadoBolsa.MercadoVista;

                    ordemBolsa.IntegraOrdensSinacor(idCliente, data, (byte)FonteOrdemBolsa.Manual, tipoMercado);

                    LiquidacaoTermoBolsa liquidacaoTermoBolsa = new LiquidacaoTermoBolsa();
                    liquidacaoTermoBolsa.IntegraLiquidacaoTermoSinacor(idCliente, data);
                    //                    
                    new PosicaoTermoBolsa().AtualizaContratoTermoSinacor(idCliente);
                }

                if (btc)
                {
                    OperacaoEmprestimoBolsa operacaoEmprestimoBolsa = new OperacaoEmprestimoBolsa();
                    operacaoEmprestimoBolsa.IntegraOperacaoEmprestimoSinacor(idCliente, data);

                    LiquidacaoEmprestimoBolsa liquidacaoEmprestimoBolsa = new LiquidacaoEmprestimoBolsa();
                    liquidacaoEmprestimoBolsa.IntegraLiquidacaoEmprestimoSinacor(idCliente, data);
                }

                if (bmf)
                {
                    OperacaoBMF operacaoBMF = new OperacaoBMF();
                    operacaoBMF.IntegraOperacoesSinacor(idCliente, data);
                }
            }
            
            Cursor.Current = Cursors.Default;

            MessageBox.Show("Exportação realizada com sucesso.");
        }



   }
}