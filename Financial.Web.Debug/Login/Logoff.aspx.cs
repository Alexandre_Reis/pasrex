﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class login_Logoff : System.Web.UI.Page {
    protected void Page_Load(object sender, EventArgs e) {
        //Pagina LOGOFF utilizada pelo FDesk
        System.Web.Security.FormsAuthentication.SignOut();
        Response.Redirect("~/default.aspx");
    }
}
