﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Threading;
using System.Web.Configuration;
using System.Globalization;
using Financial.Web.Util;
using Financial.WebConfigConfiguration;
using System.IO;
using System.Drawing;
using Financial.Security;
using Financial.Security.Enums;

public partial class LoginInit : Financial.Web.Common.BasePage
{
    protected override void InitializeCulture()
    {
        PersonalizeCulture.InicializaCulturePersonalizada();
        base.InitializeCulture();
    }

    new protected void Page_Load(object sender, EventArgs e)
    {
        const string LOGIN_FAILURE_TEXT_DEFAULT = "Usuário ou senha inválidos. Tente novamente.";
        TextBox Username = Login1.FindControl("Username") as TextBox;
        TextBox Password = Login1.FindControl("Password") as TextBox;

        if (!Page.IsPostBack && WebConfig.AppSettings.TecladoVirtual)
        {
            ScriptKeyboard();
            Password.Attributes.Add("onkeydown", "alert('Para digitar a senha, deve ser usado o teclado virtual!'); this.value = '';");
        }

        Page.ClientScript.RegisterStartupScript(this.GetType(), "MainPageReload",
        "<script type=\"text/javascript\">" +
        "if (self.parent!=self) self.parent.location.reload(true);" +
        "</script>");

        LinkButton LoginButton = Login1.FindControl("LoginButton") as LinkButton;

        Username.Attributes.Add("onKeyPress", "javascript:if (event.keyCode == 13) __doPostBack('" + LoginButton.UniqueID + "','')");
        Password.Attributes.Add("onKeyPress", "javascript:if (event.keyCode == 13) __doPostBack('" + LoginButton.UniqueID + "','')");

        if (!Page.IsPostBack)
        {
            string failureText = ConfigurationManager.AppSettings["LoginFailureText"];
            Login1.FailureText = String.IsNullOrEmpty(failureText) ? LOGIN_FAILURE_TEXT_DEFAULT : failureText;
            this.InitializeCulture();
        }

        Response.Cache.SetCacheability(HttpCacheability.ServerAndNoCache);
        if (Context.User.Identity.IsAuthenticated)
        {
            FormsAuthentication.SignOut();
            Session.Abandon();
            Session.Clear();
        }
        this.Login1.Focus();
    }

    private void ScriptKeyboard()
    {
        HtmlGenericControl Include = new HtmlGenericControl("script");
        Include.Attributes.Add("type", "text/javascript");
        Include.Attributes.Add("src", "../jsKey/keyboard.js");
        this.Page.Header.Controls.Add(Include);
    }

    protected void btnLembrarSenha_Click(object sender, EventArgs e)
    {
        Response.Redirect("LembrarSenha.aspx");
    }

    protected void Login1_LoggedIn(object sender, EventArgs e)
    {
        TextBox Username = Login1.FindControl("Username") as TextBox;
        CheckBox RememberMe = Login1.FindControl("RememberMe") as CheckBox;
        FormsAuthentication.SetAuthCookie(Username.Text.ToString(), RememberMe.Checked);

        Usuario usuario = new Usuario();
        usuario.BuscaUsuario(Username.Text.ToString());

        if (usuario.TrocaSenha == "S")
        {
            string returnURL = Request.Params["ReturnUrl"];

            //O trocar senha precisa saber se trata-se de um user com acesso ao portal cliente ou interno
            if (String.IsNullOrEmpty(returnURL))
            {
                returnURL = "~/default.aspx";
            }
            Response.Redirect("~/Login/TrocarSenha.aspx?ReturnURL=" + returnURL);
        }

    }
}
