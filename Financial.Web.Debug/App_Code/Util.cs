﻿using System.Collections;
using Financial.Bolsa.Enums;
using System.Collections.Generic;
using Financial.BMF.Enums;
using System;
using System.Globalization;
using Financial.Util;
using Financial.Util.Enums;
using DevExpress.Web.ASPxEditors;
using System.Web.UI;
using System.ComponentModel;
using System.Text;
using EntitySpaces.Interfaces;
using EntitySpaces.Core;
using System.Threading;
using System.IO;

namespace Financial.Web.Util
{
    #region Enums do Util
    public enum ConfiguracaoCombo
    {
        Unsigned = 0,
        OpcaoBranco = 1, // Controe uma linha em Branco para o combo
    }

    public enum Relatorios
    {
        [StringValue("EnumRelatorios.ComposicaoCarteira")]
        ComposicaoCarteira = 1,

        [StringValue("EnumRelatorios.ExtratoClienteMensal")]
        ExtratoClienteMensal = 5,

        [StringValue("EnumRelatorios.FluxoCaixaSintetico")]
        FluxoCaixaSintetico = 10,

        [StringValue("EnumRelatorios.FluxoCaixaAnalitico")]
        FluxoCaixaAnalitico = 20,

        [StringValue("EnumRelatorios.MapaResultado")]
        MapaResultado = 30,

        [StringValue("EnumRelatorios.SaldoCotistaAnaliticoCodigo")]
        SaldoCotistaAnaliticoCodigo = 100,

        [StringValue("EnumRelatorios.SaldoCotistaAnaliticoNome")]
        SaldoCotistaAnaliticoNome = 101,

        [StringValue("EnumRelatorios.SaldoCotistaConsolidadoCodigo")]
        SaldoCotistaConsolidadoCodigo = 102,

        [StringValue("EnumRelatorios.SaldoCotistaConsolidadoNome")]
        SaldoCotistaConsolidadoNome = 103,

        [StringValue("EnumRelatorios.MovimentoCotista")]
        MovimentoCotista = 110,

        [StringValue("EnumRelatorios.NotasAplicacaoResgate")]
        NotasAplicacaoResgate = 115,

        [StringValue("EnumRelatorios.ExtratoContaCorrente")]
        ExtratoContaCorrente = 150,

        [StringValue("EnumRelatorios.InformeConsolidadoCarteiras")]
        InformeConsolidadoCarteiras = 200,

        [StringValue("EnumRelatorios.ResultadoEnquadramentoConsolidadoDesenquadrado")]
        ResultadoEnquadramentoConsolidadoDesenquadrado = 500,

        [StringValue("EnumRelatorios.ResultadoEnquadramentoConsolidado")]
        ResultadoEnquadramentoConsolidado = 501,

        [StringValue("EnumRelatorios.ResultadoEnquadramentoIndividual")]
        ResultadoEnquadramentoIndividual = 502
    }
    #endregion

    public static class TraducaoEnumsWeb
    {
        #region Funções privates para fazer Pesquisa dentro dos Enums
        /// <summary>
        /// Dado um codigo retorna um Enum do tipo EnumRelatorios
        /// </summary>
        /// <exception>throws ArgumentException se não existir um enum com o valor do código</exception> 
        /// <returns></returns>
        private static Relatorios SearchEnumRelatorios(int codigo)
        {
            int[] relatorioValues = (int[])Enum.GetValues(typeof(Relatorios));

            int? posicao = null;
            for (int i = 0; i < relatorioValues.Length; i++)
            {
                if (relatorioValues[i] == codigo)
                {
                    posicao = i;
                    break;
                }
            }

            if (posicao.HasValue)
            {
                string relatorioString = Enum.GetNames(typeof(Relatorios))[posicao.Value];
                // Monta o Enum de acordo com a string
                Relatorios relatorio = (Relatorios)Enum.Parse(typeof(Relatorios), relatorioString);
                return relatorio;
            }
            else
            {
                throw new ArgumentException("Enum Relatorios não possue Constante com valor " + codigo);
            }
        }


        #endregion

        public static class EnumRelatorios
        {
            /// <summary>
            /// Realiza a tradução do Enum Relatorios
            /// </summary>
            /// <param name="codigo"></param>
            /// <returns></returns>
            public static string TraduzEnum(int codigo)
            {
                Relatorios t = TraducaoEnumsWeb.SearchEnumRelatorios(codigo);
                string chave = "#" + StringEnum.GetStringValue(t);
                //return Resources.Web.ResourceManager.GetString(chave);
                return "";
            }
        }
    }

    #region Funções relativas a Carregamento de Combos
    /// <summary>
    ///  
    /// </summary>
    public class Combos
    {

        #region Combo TipoMercadoBolsa
        /// <summary>
        /// 
        /// </summary>
        /// <param name="configuracao">Parametros de Configuração do combo
        /// OpcaoBranco: carrega uma linha em branco a mais no combo
        /// </param>
        /// <returns>Retorna o texto traduzido e uma chave para ser usado no carregamento do combo</returns>
        public static List<Hashtable> carregaComboTipoMercadoBolsa(ConfiguracaoCombo configuracao)
        {
            List<Hashtable> l = new List<Hashtable>();

            // Procura no enum de Bolsa os valores para carregar no combo
            List<string> valoresEnum = TipoMercadoBolsa.Values();

            if (configuracao == ConfiguracaoCombo.OpcaoBranco)
            {
                Hashtable h = new Hashtable();
                h.Add("", "");
                l.Add(h);
            }
            // Para cada Valor do Enum Monta um HashTable textoTraduzido-Valor
            for (int i = 0; i < valoresEnum.Count; i++)
            {
                Hashtable h = new Hashtable();
                string textoTraduzido = TipoMercadoBolsa.str.RetornaTexto(valoresEnum[i]);
                // Monta o HashTable com os valores do Combo
                h.Add(valoresEnum[i], textoTraduzido);
                // Adiciona o HashTable na lista
                l.Add(h);
            }
            return l;
        }

        /// <summary>
        /// Carrega os valores do combo de acordo com o enum de TipoMercadoBolsa
        /// </summary>
        /// <returns>Retorna o texto traduzido e uma chave para ser usado no carregamento do combo</returns>
        public static List<Hashtable> carregaComboTipoMercadoBolsa()
        {
            return carregaComboTipoMercadoBolsa(ConfiguracaoCombo.Unsigned);
        }
        #endregion

        #region Combo TipoMercadoBMF
        /// <summary>
        /// 
        /// </summary>
        /// <param name="configuracao">parametros de Configuração do combo
        /// OpcaoBranco: carrega uma linha em branco a mais no combo        
        /// </param>
        /// <returns>Retorna o texto traduzido e uma chave para ser usado no carregamento do combo</returns>
        public static List<Hashtable> carregaComboTipoMercadoBMF(ConfiguracaoCombo configuracao)
        {
            List<Hashtable> l = new List<Hashtable>();

            // Procura no enum de Bolsa os valores para carregar no combo
            List<int> valoresEnum = TipoMercadoBMF.Values();

            if (configuracao == ConfiguracaoCombo.OpcaoBranco)
            {
                Hashtable h = new Hashtable();
                h.Add("", "");
                l.Add(h);
            }

            // Para cada Valor do Enum Monta um HashTable textoTraduzido-Valor
            for (int i = 0; i < valoresEnum.Count; i++)
            {
                Hashtable h = new Hashtable();
                string textoTraduzido = TipoMercadoBMF.str.RetornaTexto(valoresEnum[i]);
                // Monta o HashTable com os valores do Combo
                h.Add(Convert.ToString(valoresEnum[i]), textoTraduzido);
                // Adiciona o HashTable na lista
                l.Add(h);
            }
            return l;
        }

        /// <summary>
        /// Carrega os valores do combo de acordo com o enum de TipoMercadoBMF
        /// </summary>
        /// <returns>Retorna o texto traduzido e uma chave para ser usado no carregamento do combo</returns>
        public static List<Hashtable> carregaComboTipoMercadoBMF()
        {
            return carregaComboTipoMercadoBMF(ConfiguracaoCombo.Unsigned);
        }
        #endregion
    }
    #endregion

    #region Tratamento Diferenciado para Formatos de Data e Numeros Fora do padrão
    /// <summary>
    ///     Personaliza Formato de Datas e Numeros por Cultura que não se adequam ao Padrão
    /// </summary>
    public class PersonalizeCulture
    {
        public static void InicializaCulturePersonalizada()
        {
            CultureInfo ci = Thread.CurrentThread.CurrentCulture;
            CultureInfo culturaLocal = new CultureInfo(ci.Name);

            // Tratamento Especial para Formatos de Data e Numeros
            switch (ci.Name)
            {
                case "pt-BR":
                    culturaLocal.DateTimeFormat.ShortDatePattern = StringEnum.GetStringValue(PatternData.formato2);
                    Thread.CurrentThread.CurrentCulture = culturaLocal;
                    break;
                case "en-US":
                    culturaLocal.DateTimeFormat.ShortDatePattern = StringEnum.GetStringValue(PatternData.formato1);
                    Thread.CurrentThread.CurrentCulture = culturaLocal;
                    break;
            }
        }
    }
    #endregion

    public class ActionFromUIOutput
    {
        public int errorCode;
        public string errorMessage;
        public object json;
        public ActionFromUIOutput(int errorCode, string errorMessage, object json)
        {
            this.errorCode = errorCode;
            this.errorMessage = errorMessage;
            this.json = json;
        }
    }

    public class UtilitarioWeb : Page
    {

        /// <summary>
        /// Retorna o IP da máquina do Cliente para uma Aplicação Web
        /// </summary>
        /// <returns></returns>
        public static string GetIP(System.Web.HttpRequest p)
        {
            return p.ServerVariables["REMOTE_ADDR"];
            //return Utilitario.GetLocalIp();
            //return Request.UserHostAddress;
        }

        /// <summary>
        /// Imprime o Conteudo de um Objeto
        /// </summary>
        /// <param name="obj">Objeto que se quer Imprimir</param>
        /// <returns></returns>             
        public static string ToString(esEntity obj)
        {
            /*
            #region Teste
            StringBuilder s = new StringBuilder();
            //
            s.Append("Object=");
            int i = 0;

            Type t = obj.GetType();
            PropertyInfo[] pi = t.GetProperties();
            foreach (PropertyInfo p in pi) {
                string name = p.Name;
                object value = p.GetType();
                //
                if (i == 0) {
                    s.AppendFormat("{ ( {0}={1}", name, value);
                }
                else {
                    s.AppendFormat(", {0}={1}", name, value);
                }
                //
                i++;
            }
            s.Append(" ) }");
            return s.ToString();
            #endregion

            #region Teste2
            StringBuilder sb = new StringBuilder();
            System.Type type = obj.GetType();

            System.Reflection.PropertyInfo[] pi = type.GetProperties();
            sb.Append("\r\nProperties:");

            foreach (PropertyInfo p in pi) {

                if (!p.ToString().Contains("By")) {
                    sb.Append("\r\n  " + p.ToString() + " = \"" + p.GetValue(obj, null) + "\"");
                }
            }
            return sb.ToString();
            #endregion
            */

            List<string> modificados = obj.es.ModifiedColumns;

            StringBuilder s = new StringBuilder();
            //
            s.Append(" Object={( ");
            //int i = 0;
            foreach (esColumnMetadata col in obj.es.Meta.Columns)
            {
                s.Append(col.PropertyName + " = ");
                s.Append(obj.GetColumn(col.Name) + "; ");
            }
            s.Append(" )}");
            return s.ToString();
        }

        /// <summary>
        /// Copia uma Stream para outra
        /// </summary>
        /// <param name="source"></param>
        /// <param name="target"></param>
        public static void CopyStream(Stream source, Stream target)
        {
            byte[] buffer = new byte[0x10000];
            int bytes;
            try
            {
                while ((bytes = source.Read(buffer, 0, buffer.Length)) > 0)
                {
                    target.Write(buffer, 0, bytes);
                }
            }
            finally
            {
                target.Flush();
                // Or target.Close(); if you're done here already.
            }
        }
    }
}