using System;
using Financial.Util;

namespace Financial.Web.Enums {

    public enum TipoLingua {
        [StringValue("pt-BR")]
        Portugues = 1,

        [StringValue("en-US")]
        Ingles = 2,
    }
}