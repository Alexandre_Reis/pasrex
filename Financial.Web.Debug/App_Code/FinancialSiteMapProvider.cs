using System;
using System.Data;
using System.Collections.Generic;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Collections.Specialized;
using Financial.Security;
using Financial.Util;
using Financial.Util.ConfiguracaoSistema;
using Financial.Util.Enums;
using Financial.Security.Enums;

/// <summary>
/// Summary description for FinancialSiteMapProvider
/// </summary>
public class FinancialSiteMapProvider : XmlSiteMapProvider {
    protected ConfiguracaoCollection configColl;

    protected bool AcessoAcoesOpcoes(int idMenu) {
        bool acessa = true;

        List<int> listaMenus = new List<int>();
        listaMenus.Add(1820);
        listaMenus.Add(2100);
        listaMenus.Add(20000);

        Configuracao config = configColl.FindByPrimaryKey(ConfiguracaoRegrasNegocio.Seguranca.Menu.AcoesOpcoes);

        if (config == null)
            return false;

        if (config.ValorTexto.Trim().ToUpper() == "N") {
            if (listaMenus.Contains(idMenu)) {
                acessa = false;
            }
        }

        return acessa;
    }
    protected bool AcessoTermoAcoes(int idMenu) {
        bool acessa = true;

        List<int> listaMenus = new List<int>();
        listaMenus.Add(2300);

        Configuracao config = configColl.FindByPrimaryKey(ConfiguracaoRegrasNegocio.Seguranca.Menu.TermoAcoes);

        if (config == null)
            return false;

        if (config.ValorTexto.Trim().ToUpper() == "N") {
            if (listaMenus.Contains(idMenu)) {
                acessa = false;
            }
        }

        return acessa;
    }
    protected bool AcessoBTCAcoes(int idMenu) {
        bool acessa = true;

        List<int> listaMenus = new List<int>();
        listaMenus.Add(2400);
        listaMenus.Add(1840);
        listaMenus.Add(20140);
        listaMenus.Add(20150);

        Configuracao config = configColl.FindByPrimaryKey(ConfiguracaoRegrasNegocio.Seguranca.Menu.BTC_Aluguel);

        if (config == null)
            return false;

        if (config.ValorTexto.Trim().ToUpper() == "N") {
            if (listaMenus.Contains(idMenu)) {
                acessa = false;
            }
        }

        return acessa;
    }
    protected bool AcessoBMF(int idMenu) {
        bool acessa = true;

        List<int> listaMenus = new List<int>();
        listaMenus.Add(1830);
        listaMenus.Add(3000);
        listaMenus.Add(21000);

        Configuracao config = configColl.FindByPrimaryKey(ConfiguracaoRegrasNegocio.Seguranca.Menu.BMF);

        if (config == null)
            return false;

        if (config.ValorTexto.Trim().ToUpper() == "N") {
            if (listaMenus.Contains(idMenu)) {
                acessa = false;
            }
        }

        return acessa;
    }
    protected bool AcessoRendaFixa(int idMenu) {
        bool acessa = true;

        List<int> listaMenus = new List<int>();
        listaMenus.Add(3500);
        listaMenus.Add(22000);

        Configuracao config = configColl.FindByPrimaryKey(ConfiguracaoRegrasNegocio.Seguranca.Menu.RendaFixa);

        if (config == null)
            return false;

        if (config.ValorTexto.Trim().ToUpper() == "N") {
            if (listaMenus.Contains(idMenu)) {
                acessa = false;
            }
        }

        return acessa;
    }
    protected bool AcessoFundos(int idMenu) {
        bool acessa = true;

        List<int> listaMenus = new List<int>();
        listaMenus.Add(4800);
        listaMenus.Add(15185);
        listaMenus.Add(15190);
        listaMenus.Add(15210);
        listaMenus.Add(15220);
        listaMenus.Add(16200);
        listaMenus.Add(17500);

        Configuracao config = configColl.FindByPrimaryKey(ConfiguracaoRegrasNegocio.Seguranca.Menu.Fundos);

        if (config == null)
            return false;

        if (config.ValorTexto.Trim().ToUpper() == "N") {
            if (listaMenus.Contains(idMenu)) {
                acessa = false;
            }
        }

        return acessa;
    }
    protected bool AcessoSwap(int idMenu) {
        bool acessa = true;

        List<int> listaMenus = new List<int>();
        listaMenus.Add(4000);
        listaMenus.Add(23000);

        Configuracao config = configColl.FindByPrimaryKey(ConfiguracaoRegrasNegocio.Seguranca.Menu.Swap);

        if (config == null)
            return false;

        if (config.ValorTexto.Trim().ToUpper() == "N") {
            if (listaMenus.Contains(idMenu)) {
                acessa = false;
            }
        }

        return acessa;
    }
    protected bool AcessoGerencial(int idMenu) {
        bool acessa = true;

        List<int> listaMenus = new List<int>();
        listaMenus.Add(21500);

        Configuracao config = configColl.FindByPrimaryKey(ConfiguracaoRegrasNegocio.Seguranca.Menu.Gerencial);

        if (config.ValorTexto.Trim().ToUpper() == "N") {
            if (listaMenus.Contains(idMenu)) {
                acessa = false;
            }
        }

        return acessa;
    }
    protected bool AcessoCalculoDespesas(int idMenu) {
        bool acessa = true;

        List<int> listaMenus = new List<int>();
        listaMenus.Add(7300);
        listaMenus.Add(7400);
        listaMenus.Add(7500);
        listaMenus.Add(7600);
        listaMenus.Add(15160);
        listaMenus.Add(15170);
        listaMenus.Add(15180);
        listaMenus.Add(16120);

        Configuracao config = configColl.FindByPrimaryKey(ConfiguracaoRegrasNegocio.Seguranca.Menu.CalculoDespesas);

        if (config == null)
            return false;

        if (config.ValorTexto.Trim().ToUpper() == "N") {
            if (listaMenus.Contains(idMenu)) {
                acessa = false;
            }
        }

        return acessa;
    }
    protected bool AcessoRebates(int idMenu) {
        bool acessa = true;

        List<int> listaMenus = new List<int>();
        listaMenus.Add(9000);
        listaMenus.Add(18500);

        Configuracao config = configColl.FindByPrimaryKey(ConfiguracaoRegrasNegocio.Seguranca.Menu.Rebates);

        if (config == null)
            return false;

        if (config.ValorTexto.Trim().ToUpper() == "N") {
            if (listaMenus.Contains(idMenu)) {
                acessa = false;
            }
        }

        return acessa;
    }
    protected bool AcessoEnquadramento(int idMenu) {
        bool acessa = true;

        List<int> listaMenus = new List<int>();
        listaMenus.Add(11000);
        listaMenus.Add(24000);

        Configuracao config = configColl.FindByPrimaryKey(ConfiguracaoRegrasNegocio.Seguranca.Menu.Enquadramento);

        if (config == null)
            return false;

        if (config.ValorTexto.Trim().ToUpper() == "N") {
            if (listaMenus.Contains(idMenu)) {
                acessa = false;
            }
        }

        return acessa;
    }
    protected bool AcessoCotista(int idMenu) {
        bool acessa = true;

        List<int> listaMenus = new List<int>();
        listaMenus.Add(10000);
        listaMenus.Add(18000);
        listaMenus.Add(40160);

        Configuracao config = configColl.FindByPrimaryKey(ConfiguracaoRegrasNegocio.Seguranca.Menu.Cotista);

        if (config == null)
            return false;

        if (config.ValorTexto.Trim().ToUpper() == "N") {
            if (listaMenus.Contains(idMenu)) {
                acessa = false;
            }
        }

        return acessa;
    }
    protected bool AcessoContabil(int idMenu) {
        bool acessa = true;

        List<int> listaMenus = new List<int>();
        listaMenus.Add(13000);

        Configuracao config = configColl.FindByPrimaryKey(ConfiguracaoRegrasNegocio.Seguranca.Menu.Contabil);

        if (config == null)
            return false;

        if (config.ValorTexto.Trim().ToUpper() == "N") {
            if (listaMenus.Contains(idMenu)) {
                acessa = false;
            }
        }

        return acessa;
    }
    protected bool AcessoInformes(int idMenu) {
        bool acessa = true;

        Configuracao configClubes = configColl.FindByPrimaryKey(ConfiguracaoRegrasNegocio.Seguranca.Menu.InformeClubes);
        Configuracao configFundos = configColl.FindByPrimaryKey(ConfiguracaoRegrasNegocio.Seguranca.Menu.InformeFundos);
        Configuracao configApuracaoIR = configColl.FindByPrimaryKey(ConfiguracaoRegrasNegocio.Seguranca.Menu.ApuracaoIR);

        bool acessaInformeClubes = configClubes.ValorTexto.Trim().ToUpper() == "S";
        bool acessaInformeFundos = configFundos.ValorTexto.Trim().ToUpper() == "S";
        bool acessaInformeApuracaoIR = configApuracaoIR.ValorTexto.Trim().ToUpper() == "S";

        if (idMenu == 25100 && !acessaInformeClubes) {
            acessa = false;
        }
        else if (idMenu == 25220 && !acessaInformeClubes && !acessaInformeFundos) {
            acessa = false;
        }
        else if (idMenu == 25230 && !acessaInformeApuracaoIR) {
            acessa = false;
        }
        else if (idMenu == 25240 && !acessaInformeFundos) {
            acessa = false;
        }
        else if (idMenu == 25000 && !acessaInformeClubes && !acessaInformeFundos && !acessaInformeApuracaoIR) {
            acessa = false;
        }
        else if (idMenu == 25200 && !acessaInformeClubes && !acessaInformeFundos && !acessaInformeApuracaoIR) {
            acessa = false;
        }


        return acessa;
    }

    bool testaAcesso;
    public bool TestaAcesso {
        set { testaAcesso = value; }
    }

    public FinancialSiteMapProvider() {
        this.testaAcesso = true;
    }

    public override bool IsAccessibleToUser(HttpContext context, SiteMapNode node) {
        return IsVisible(context, node);
    }

    private bool IsVisible(HttpContext context, SiteMapNode node) {
        configColl = new ConfiguracaoCollection();
        configColl.LoadAll();

        if (this.testaAcesso) {
            string login = context.User.Identity.Name;

            int idGrupo = 0;
            Usuario usuario = new Usuario();
            if (usuario.BuscaUsuario(login)) {
                idGrupo = usuario.IdGrupo.Value;
            }

            string menuIDString = node["menuID"];
            int menuID;

            if (int.TryParse(menuIDString, out menuID)) {
                if (!TestaAcessoCliente(menuID)) {
                    return false;
                }
                else {
                    PermissaoMenu permissaoMenu = new PermissaoMenu();
                    if (permissaoMenu.LoadByPrimaryKey(idGrupo, menuID)) {
                        return permissaoMenu.PermissaoLeitura == "S";
                    }
                    else {
                        GrupoUsuario grupoUsuario = new GrupoUsuario();
                        grupoUsuario.LoadByPrimaryKey(idGrupo);

                        if (grupoUsuario.TipoPerfil.Value == (byte)TipoPerfilGrupo.Administrador && menuID >= 40000 && menuID <= 40180) {
                            return true;
                        }
                        else {
                            return false;
                        }
                    }
                }
            }

            return true;
        }
        else {
            return true;
        }
    }

    private bool TestaAcessoCliente(int menuId) {
        if (!AcessoAcoesOpcoes(menuId)) {
            return false;
        }
        else if (!AcessoTermoAcoes(menuId)) {
            return false;
        }
        else if (!AcessoBTCAcoes(menuId)) {
            return false;
        }
        else if (!AcessoBMF(menuId)) {
            return false;
        }
        else if (!AcessoRendaFixa(menuId)) {
            return false;
        }
        else if (!AcessoFundos(menuId)) {
            return false;
        }
        else if (!AcessoSwap(menuId)) {
            return false;
        }
        else if (!AcessoGerencial(menuId)) {
            return false;
        }
        else if (!AcessoCalculoDespesas(menuId)) {
            return false;
        }
        else if (!AcessoRebates(menuId)) {
            return false;
        }
        else if (!AcessoEnquadramento(menuId)) {
            return false;
        }
        else if (!AcessoCotista(menuId)) {
            return false;
        }
        else if (!AcessoContabil(menuId)) {
            return false;
        }
        else if (!AcessoInformes(menuId)) {
            return false;
        }

        return true;
    }
}