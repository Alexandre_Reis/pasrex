<%@ WebHandler Language="C#" Class="TestePerformance" %>

using System;
using System.Web;
using Financial.Bolsa;
using System.Data;
using Financial.RendaFixa;


public class TestePerformance : IHttpHandler {
    
    public void ProcessRequest (HttpContext context) {

        const int ID_CARTEIRA = 220;
        
        DateTime startDateTime = DateTime.Now;
        int numVezes = 2000;

        DateTime? dataMes = new DateTime(2012, 1, 1);
        DateTime dataReferencia = startDateTime;

        int[] idsEstrategias = new int[] { 1, 2, 3, 4, 5, 6 };

        //var
            
        
        DataTable table = new DataTable();
        for (int i = 0; i < numVezes; i++)
        {
            foreach (int idEstrategia in idsEstrategias)
            {
                PosicaoRendaFixaHistoricoQuery posicaoRendaFixaHistoricoQuery = new PosicaoRendaFixaHistoricoQuery("P");
                TituloRendaFixaQuery tituloRendaFixaQuery = new TituloRendaFixaQuery("T");

                posicaoRendaFixaHistoricoQuery.Select(posicaoRendaFixaHistoricoQuery.ValorMercado.Sum());
                posicaoRendaFixaHistoricoQuery.InnerJoin(tituloRendaFixaQuery).On(tituloRendaFixaQuery.IdTitulo == posicaoRendaFixaHistoricoQuery.IdTitulo);
                posicaoRendaFixaHistoricoQuery.Where(posicaoRendaFixaHistoricoQuery.IdCliente.Equal(ID_CARTEIRA),
                                                 posicaoRendaFixaHistoricoQuery.DataHistorico.Equal(dataReferencia),
                                                 posicaoRendaFixaHistoricoQuery.Quantidade.NotEqual(0),
                                                 tituloRendaFixaQuery.IdEstrategia.Equal(idEstrategia));

                PosicaoRendaFixaHistorico posicaoRendaFixaHistorico = new PosicaoRendaFixaHistorico();
                //posicaoRendaFixaHistorico.Load(posicaoRendaFixaHistoricoQuery);

                /*
                OperacaoBolsaQuery operacaoBolsaQuery = new OperacaoBolsaQuery("O");
                AtivoBolsaQuery ativoBolsaQuery = new AtivoBolsaQuery("A");
                operacaoBolsaQuery.Select(operacaoBolsaQuery.ValorLiquido.Sum(),
                                          operacaoBolsaQuery.Valor.Sum(),
                                          operacaoBolsaQuery.TipoOperacao,
                                          operacaoBolsaQuery.Origem);
                operacaoBolsaQuery.InnerJoin(ativoBolsaQuery).On(ativoBolsaQuery.CdAtivoBolsa == operacaoBolsaQuery.CdAtivoBolsa);
                operacaoBolsaQuery.Where(operacaoBolsaQuery.Data.GreaterThan(dataMes.Value),
                                         operacaoBolsaQuery.Data.LessThanOrEqual(dataReferencia),
                                         operacaoBolsaQuery.IdCliente.Equal(ID_CARTEIRA),
                                         ativoBolsaQuery.IdEstrategia.Equal(idEstrategia));
                operacaoBolsaQuery.GroupBy(operacaoBolsaQuery.TipoOperacao, operacaoBolsaQuery.Origem);
*/
                table = posicaoRendaFixaHistoricoQuery.LoadDataTable();
                
            }
        }
        
        DateTime endDateTime = DateTime.Now;
        
        context.Response.ContentType = "text/html";
        context.Response.Write("start:" + startDateTime.ToString() + "<br />end: " + endDateTime.ToString());
    }
 
    public bool IsReusable {
        get {
            return false;
        }
    }

}