﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Financial.Investidor;
using Financial.Investidor.Enums;
using Financial.Util;
using Financial.Web.Common;
using DevExpress.Web.ASPxPopupControl;
using DevExpress.Web.ASPxGridView;

public partial class DefaultFDesk : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        PlaceHolder ph1 = new PlaceHolder();
        this.FindControl("form1").Controls.Add(ph1);
        BrowserPopups popups = new BrowserPopups();

        ASPxPopupControl popup = popups.PopupCliente(false);

        ph1.Controls.Add(popup);

        //DiretorioBaseHelp.HRef = ConfigurationManager.AppSettings["DiretorioBaseHelp"] + "/fdesk/index.htm";
        DiretorioBaseHelp.HRef = "help/fdesk/application.htm";

        string cliente = ConfigurationManager.AppSettings["Cliente"];
        if (!string.IsNullOrEmpty(cliente))
        {
            //Checar se todos os web.configs estao corretos antes de implementar a linha abaixo:
            //Title = cliente + ": Portal do Cliente";
        }

    }

    protected void EsDSCliente_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        ClienteCollection clienteCollection = new ClienteCollection();
        clienteCollection.BuscaClientesComAcesso(HttpContext.Current.User.Identity.Name);

        e.Collection = clienteCollection;
    }
}