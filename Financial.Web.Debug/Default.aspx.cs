﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Financial.WebConfigConfiguration;
using System.IO;
using System.Collections.Specialized;
using Financial.Security;
using Financial.Security.Enums;

public partial class Default : Financial.Web.Common.BasePage
{
    private Usuario usuario = new Usuario();
    new protected void Page_Load(object sender, EventArgs e)
    {
        const string URL_FDESK = "~/DefaultFDesk.aspx";

        usuario.BuscaUsuario(HttpContext.Current.User.Identity.Name);

        //Examinar se devemos forcar usuario para FinancialDesk
        if (WebConfig.AppSettings.FinancialDesk)
        {
            if (usuario.IdGrupo.HasValue)
            {
                GrupoUsuario grupoUsuario = new GrupoUsuario();
                grupoUsuario.LoadByPrimaryKey(usuario.IdGrupo.Value);

                if (grupoUsuario.TipoPerfil.Value == (byte)TipoPerfilGrupo.Externo)
                {
                    Response.Redirect(URL_FDESK);
                    return;
                }
            }
        }


        Response.Cache.SetCacheability(HttpCacheability.ServerAndNoCache);

        textSearch.Text = textTextoSearch.Text;

        //Carrega página default
        string paginaDefault = RetornaPaginaDefault();
        homePage.Attributes["href"] = paginaDefault;

        hiddenPage.Value = paginaDefault;

        chkJanela.Attributes.Add("onclick", "iterateThroughMenu();");
    }

    private string RetornaPaginaDefault()
    {
        //Busca página default associada ao usuário, se não tiver, pega a página default do web.config
        string pagina = "";
        if (!String.IsNullOrEmpty(usuario.PaginaInicial))
        {
            pagina = usuario.PaginaInicial;
        }

        if (pagina == "")
        {
            pagina = "Home.aspx";
            //pagina = WebConfig.AppSettings.PaginaInicial;
        }

        return pagina;
    }

    protected void NavigationMenu_DataBound(object sender, EventArgs e)
    {
        DevExpress.Web.ASPxMenu.ASPxMenu menu = (DevExpress.Web.ASPxMenu.ASPxMenu)sender;
        if (menu.Items.Count == 1)
        {
            DevExpress.Web.ASPxMenu.MenuItemCollection directChildren = menu.Items[0].Items;

            int directChildrenCount = directChildren.Count;
            for (int childIndex = 0; childIndex < directChildren.Count; )
            {
                menu.Items.Add(directChildren[childIndex]);
            }

            menu.Items.RemoveAt(0);

        }
    }

    protected void callback1_Callback(object source, DevExpress.Web.ASPxCallback.CallbackEventArgs e)
    {
        if (HttpContext.Current.User.Identity.IsAuthenticated)
        {
            string pagina = Convert.ToString(Session["CurrentPage"]);
            Usuario usuario = new Usuario();
            if (usuario.BuscaUsuario(HttpContext.Current.User.Identity.Name))
            {
                usuario.PaginaInicial = pagina;
                usuario.Save();

                e.Result = "Página Inicial alterada com sucesso!" + "|" + pagina;
            }
            else
            {
                e.Result = "Ocorreu algum problema, atualização cancelada.";
            }
        }
    }

}