﻿Ext.ns('Financial', 'Financial.Login', 'Financial.Util');

Ext.onReady(function(){
        Financial.Login.updateHTML();
});

Financial.Util.outerHTML = function(node){
    return node.outerHTML || new XMLSerializer().serializeToString(node);
}

Financial.Login.buttonTexts = {
    LembrarSenha : 'Esqueci a senha',
    EnviarButton : '',
    BackButton : '',
    LoginButton : ''
};

Financial.Login.updateHTML = function(){
    var pathname = location.pathname.toLowerCase();
    var updatePanel, tokens, newHTML, prefixo='';
    var paginaLoginInit = false;
            
    if(pathname.indexOf("logininit.") != -1){
        paginaLoginInit = true;
        updatePanel = Ext.get('Login1_UpdatePanel1');
        tokens = ['UserName', 'Password', 'RememberMe', 'LoginButton', 'LembrarSenha', 'FailureText'];
        prefixo = 'Login1_';
        newHTML = Financial.Login.Template;
    }else if(pathname.indexOf("lembrarsenha.") != -1){
        updatePanel = Ext.get('UpdatePanel1');
        tokens = ['UserName', 'EnviarButton', 'BackButton'];    
        newHTML = Financial.Login.TemplateLembrarSenha;
    }else if(pathname.indexOf("trocarsenha.") != -1){
        updatePanel = Ext.get('UpdatePanel1');
        tokens = ['Password', 'PasswordConfirmation', 'LoginButton', 'FailureText'];
        newHTML = Financial.Login.TemplateAlterarSenha;
    }else{
        alert('Página não implementada');
        return;
    }
   
    for(var i=0; i < tokens.length; i++){
        var token = tokens[i];
        var element = Ext.get(prefixo + token);
        var elementHTML = Financial.Util.outerHTML(element.dom);
        var buttonText = Financial.Login.buttonTexts[token];
        if(buttonText != undefined){
            elementHTML = elementHTML.replace("{text}", buttonText);
        }
        newHTML = newHTML.replace("{" + token + "}", elementHTML);
    }
    updatePanel.dom.innerHTML = newHTML;
    
    if(paginaLoginInit){
        var failureText = Ext.get('Login1_FailureText').dom.innerHTML.replace(/^\s+|\s+$/g, "");
        if(failureText.length > 0){
            alert(failureText);
        }
    }
    
    try{
        VKI_buildKeyboardInputs();
    }
    catch(e)
    { } 
}

Financial.Login.Template = '<table width="600" height="400" cellspacing="0" cellpadding="0" align="center" class="login_default"> 	<tbody><tr><td background="../imagens/login_bg.jpg" valign="top">     <div class="logoCliente">     </div>             <div id="Container">     <div id="login_content">        <span id="labelMensagem">Seja bem vindo ao Financial Web.</span>           <br>     <span id="labelMensagem2">Para ter acesso ao sistema, por favor identifique-se.</span>     <br> <br>         <table style="border-collapse: collapse;" border="0" cellpadding="0" cellspacing="0"> 	<tbody><tr> 		<td>             <div> 			            <table style="border-collapse: collapse;" border="0" cellpadding="0" cellspacing="0">                 <tbody><tr>                     <td>                         <table border="0" cellpadding="0">                             <tbody><tr>                                 <td class="label" align="right">                                     <label for="Login1_UserName" id="Login1_UserNameLabel">Login:</label></td>                                 <td>                                     {UserName}                                                                     </td>                             </tr>                             <tr>                                 <td class="label" align="right">                                     <label for="Login1_Password" id="Login1_PasswordLabel">Senha:</label></td>                                 <td>                                     {Password}                                 </td>                                 <td>                                                                     </td>                             </tr>                             <tr>                                 <td colspan="2">                                     {RememberMe}<label for="Login1_RememberMe">Lembrar login  |</label>                                     {LembrarSenha}                                 </td>                             </tr>                             <tr>                                 <td class="failureText" colspan="2" style="color: red;" align="center">                                     {FailureText}                                 </td>                             </tr>                                                         <tr>                                                                <td colspan="2" align="right">                                     {LoginButton}                                  </td>                             </tr>                         </tbody></table>                     </td>                                    </tr>             </tbody></table>             		</div>                     </td> 	</tr> </tbody></table>            </div>         </div>     </td> 	</tr> 	</tbody> 	</table>';
Financial.Login.TemplateLembrarSenha = '<table width="600" height="400" align="center" cellpadding="0" cellspacing="0" class="login_default">                         <tr>                             <td valign="top" background="../imagens/login_bg.jpg">                                 <div class="logoCliente">                                 </div>                                    <div id="Container">                                     <div id="login_content">                                         &nbsp;<br />                                         <div> 	                                                <table>                                                     <tr>                                                         <td>                                                             <p>                                                                 <b>Esqueceu sua senha?</b></p>                                                             <p>                                                                 Informe seu login para receber sua senha no e-mail cadastradado.</p>                                                             <table border="0" cellpadding="0">                                                                 <tr>                                                                     <td align="right" class="label">                                                                         <label for="UserName" id="UserNameLabel">Email:</label></td>                                                                     <td>                                                                         {UserName}                                                                     </td>                                                                 </tr>                                                                 <tr>                                                                 </tr>                                                                 <tr>                                                                 </tr>                                                                 <tr>                                                                     <td align="right" colspan="2">                                                                         <div style="float: right;" class="linkButton linkButtonNoBorder">{EnviarButton}                                                                         {BackButton}</div>                                                                    </td>                                                                 </tr>                                                             </table>                                                         </td>                                                     </tr>                                                 </table>                                             					</div>                                     </div>                                 </div>                             </td>                         </tr>                     </table>';
Financial.Login.TemplateAlterarSenha = '<table width="600" height="400" cellspacing="0" cellpadding="0" align="center" class="login_default"> 	<tbody><tr><td background="../imagens/login_bg.jpg" valign="top">     <div class="logoCliente">     </div>             <div id="Container">     <div id="login_content">        <span id="labelMensagem">Seja bem vindo ao Financial Web.</span>           <br>     <span id="labelMensagem2">Este é o seu primeiro login. Por favor, digite uma nova senha.</span>     <br> <br>         <table style="border-collapse: collapse;" border="0" cellpadding="0" cellspacing="0"> 	<tbody><tr> 		<td>             <div> 			            <table style="border-collapse: collapse;" border="0" cellpadding="0" cellspacing="0">                 <tbody><tr>                     <td>                         <table border="0" cellpadding="0">                             <tbody><tr>                                 <td class="label" align="right">                                     <label for="Login1_Password" id="Login1_PasswordLabel">Nova senha:</label></td>                                 <td>                                     {Password}                                                                     </td>                             </tr>                             <tr>                                 <td class="label" align="right">                                     <label for="Login1_PasswordConfirmation" id="Login1_PasswordConfirmationLabel">Confirmar senha:</label></td>                                 <td>                                     {PasswordConfirmation}                                 </td>                                 <td>                                                                     </td>                             </tr>                             <tr>                                 <td colspan="2">                                     &nbsp           </td>                             </tr>                             <tr>                                 <td class="failureText" colspan="2" style="color: red;" align="center">                                     {FailureText}                                 </td>                             </tr>                                                         <tr>                                                                <td colspan="2" align="right">                                     {LoginButton}                                  </td>                             </tr>                         </tbody></table>                     </td>                                    </tr>             </tbody></table>             		</div>                     </td> 	</tr> </tbody></table>            </div>         </div>     </td> 	</tr> 	</tbody> 	</table>';