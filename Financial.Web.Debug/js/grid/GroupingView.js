Ext.namespace('FDESK.grid.GroupingView');
FDESK.grid.GroupingView = function(config) {

	FDESK.grid.GroupingView.superclass.constructor.call(this, config);

};

Ext.extend(FDESK.grid.GroupingView, Ext.grid.GroupingView, {
	initData : function(ds, cm) {
		FDESK.grid.GroupingView.superclass.initData.apply(this, arguments);
		this.ds.on('beforeload', this.onBeforeLoad, this);
		this.ds.on('load', this.collapseAllGroups, this);
	},

	onBeforeLoad : function(){
		this.expandAllGroups();
		this.grid.getSelectionModel().clearSelections(false);
	},
	
	// private
	interceptMouse : function(e) {
		var hd = e.getTarget('.x-grid-group-hd', this.mainBody);
		if (hd) {
			e.stopEvent();
			this.toggleGroup(hd.parentNode);
		} else {
			var altTitleSummary = e.getTarget('.x-grid-summary-alttitle');
			if(altTitleSummary){
				return;
			}
			
			var hdSummary = e.getTarget('.x-grid-summary-hd');
			if (hdSummary) {
				e.stopEvent();
				var gridGroupEl = Ext.fly(hdSummary).parent('.x-grid-group');
				if (!gridGroupEl.hasClass('x-grid-group-noexpander')) {
					this.toggleGroup(gridGroupEl);
				}
			}
		}
	},

	afterRender : function() {
		FDESK.grid.GroupingView.superclass.afterRender.call(this);
		this.collapseAllGroups();
	},

	toggleGroup : function(group, expanded) {
		this.grid.stopEditing(true);
		group = Ext.getDom(group);
		var gel = Ext.get(group);
		expanded = expanded !== undefined ? expanded : gel
				.hasClass('x-grid-group-collapsed');

		this.state[gel.dom.id] = expanded;

		var summary = gel.child('.x-grid3-summary-row');

		gel[expanded ? 'removeClass' : 'addClass']('x-grid-group-collapsed');
		var groupHd = gel.child('.x-grid-group-hd');
		groupHd[expanded ? 'removeClass' : 'addClass']('x-hide-display');
		summary[expanded ? 'removeClass' : 'addClass']('x-grid-summary-collapsed');
		var groupTitleEl = gel.child('.x-grid-group-title');

		var firstTd = Ext.get(summary.child('td'));
		var firstCol = Ext.get(firstTd.down('div'));

		if (!summary.dom.formatted) {
			summary.dom.groupTitle = groupTitleEl.dom.innerHTML;
			firstTd.addClass('x-grid-summary-hd');
			firstCol.addClass('x-grid-summary-alttitle');
			firstCol.insertSibling({
						tag : 'div',
						cls : 'x-grid-summary-title',
						html : summary.dom.groupTitle
					});
			firstCol.dom.innerHTML += summary.dom.groupTitle;
			summary.dom.formatted = true;
		}

		firstCol[expanded ? 'addClass' : 'removeClass']('x-grid-hide-summary');
	},

	// private
	doRender : function(cs, rs, ds, startRow, colCount, stripe) {
		if (rs.length < 1) {
			return '';
		}
		var groupField = this.getGroupField(), colIndex = this.cm
				.findColumnIndex(groupField), g;

		this.enableGrouping = !!groupField;

		if (!this.enableGrouping || this.isUpdating) {
			return Ext.grid.GroupingView.superclass.doRender.apply(this,
					arguments);
		}
		var gstyle = 'width:' + this.getTotalWidth() + ';';

		var gidPrefix = this.grid.getGridEl().id;
		var cfg = this.cm.config[colIndex];
		var groupRenderer = cfg.groupRenderer || cfg.renderer;
		var prefix = this.showGroupName
				? (cfg.groupName || cfg.header) + ': '
				: '';

		var groups = [], curGroup, i, len, gid;
		for (i = 0, len = rs.length; i < len; i++) {
			var rowIndex = startRow + i, r = rs[i], gvalue = r.data[groupField];

			g = this.getGroup(gvalue, r, groupRenderer, rowIndex, colIndex, ds);
			if (!curGroup || curGroup.group != g) {
				gid = gidPrefix + '-gp-' + groupField + '-'
						+ Ext.util.Format.htmlEncode(g);
				// if state is defined use it, however state is in terms of
				// expanded
				// so negate it, otherwise use the default.
				var isCollapsed = typeof this.state[gid] !== 'undefined'
						? !this.state[gid]
						: this.startCollapsed;
				var gcls = isCollapsed ? 'x-grid-group-collapsed' : '';
				curGroup = {
					group : g,
					gvalue : gvalue,
					text : prefix + g,
					groupId : gid,
					startRow : rowIndex,
					rs : [r],
					cls : gcls,
					style : gstyle
				};
				groups.push(curGroup);
			} else {
				curGroup.rs.push(r);
			}
			r._groupId = gid;
		}

		var buf = [];

		for (i = 0, len = groups.length; i < len; i++) {
			g = groups[i];
			var disabledGroup = this.disabledGroups
					&& this.disabledGroups.indexOf(g.gvalue) != -1;

			if (disabledGroup) {
				// g.style = 'display: none';
				g.cls = 'x-grid-group-noexpander';
			}

			this.doGroupStart(buf, g, cs, ds, colCount);
			buf[buf.length] = Ext.grid.GroupingView.superclass.doRender.call(
					this, cs, g.rs, ds, g.startRow, colCount, stripe);

			this.doGroupEnd(buf, g, cs, ds, colCount);
		}
		return buf.join('');
	}

});
