Ext.namespace('FDESK.panel.ZoomIndicadores');
FDESK.panel.ZoomIndicadores = function(config) {

	var fieldsQuadroRetorno = [{
				name : 'Ano',
				type : 'int'
			}, {
				name : 'IndiceDescricao',
				type : 'string'
			}, {
				name : 'IndiceDifDescricao',
				type : 'string'
			}, {
				name : 'ColAno',
				type : 'string',
				header : {
					width : '10%',
					cls : 'centered',
					title : 'Ano'
				},
				cell : {
					cls : 'centered',
					format : '{[values.Ano + "<br /><span class=indice-value>" + values.IndiceDescricao + "</span><br /><span class=indice-value>" + values.IndiceDifDescricao + "</span>"]}'
				}
			}];

	var periodosQuadroRetorno = [['Janeiro', 'Jan', '7%'], ['Fevereiro', 'Fev', '7%'],
			['Marco', 'Mar', '7%'], ['Abril', 'Abr', '7%'], ['Maio', 'Mai', '7%'],
			['Junho', 'Jun', '7%'], ['Julho', 'Jul', '7%'], ['Agosto', 'Ago', '7%'],
			['Setembro', 'Set', '7%'], ['Outubro', 'Out', '7%'], ['Novembro', 'Nov', '7%'],
			['Dezembro', 'Dez', '7%'], ['Ano', 'Rent.Ano', '9%']];

	for (var i = 0; i < periodosQuadroRetorno.length; i++) {
		var fieldName = periodosQuadroRetorno[i][0], title = periodosQuadroRetorno[i][1], width = periodosQuadroRetorno[i][2];

		fieldsQuadroRetorno.push({
					name : 'Rentabilidade' + fieldName,
					type : 'float'
				}, {
					name : 'RentabIndice' + fieldName,
					type : 'float'
				}, {
					name : 'RentabIndiceDif' + fieldName,
					type : 'float'
				}, {
					name : 'Col' + fieldName,
					type : 'string',
					header : {
						width : width,
						cls : 'centered',
						title : title
					},
					cell : {
						cls : 'centered',
						format : '{[FDESK.util.Util.formatSinalPercent(Ext.util.Format.number(values["Rentabilidade'
								+ fieldName
								+ '"], "0.000,00/i"), true)]}<br /><span class="indice-value">{[FDESK.util.Util.formatSinalPercent(Ext.util.Format.number(values["RentabIndice'
								+ fieldName
								+ '"], "0.000,00/i"), true)]}</span><br /><span class="indice-value">{[FDESK.util.Util.formatSinalPercent(Ext.util.Format.number(values["RentabIndiceDif'
								+ fieldName
								+ '"], "0.000,00/i"), true)]}</span>'
					}
				});
	};

	var tableQuadroRetorno = {
		width : '100%',
		ref : '../../tableQuadroRetorno',
		xtype : 'basetable',
		readerConfig : {
			fields : fieldsQuadroRetorno
		},

		data : []
	};

    var toolsGrafico = [{
		        qtip : FDESK.AS.helpQtip,
                id : 'help',
			    handler : function(){
			        FDESK.LE.openHelpWin({topic: 'module_3_2'});
			    }
		    }];
		    
	var graficoIndicadores1 = {
		xtype : 'panelgrafico',
		ref : '../../graficoIndicadores1',
		title : '&nbsp',
		chartGroup : 'MultiSeries',
		frame : true,
		tools : toolsGrafico
	};

	var graficoIndicadores2 = {
		xtype : 'panelgrafico',
		ref : '../../graficoIndicadores2',
		title : '&nbsp',
		chartGroup : 'MultiSeries',
		frame : true,
		tools : toolsGrafico
	};

	var col1Config = {
		name : 'Descricao',
		type : 'string',
		header : {
			width : '60%',
			cls : 'centered',
			title : '&nbsp'
		},
		cell : {
			format : '{[values.Descricao.replace(/(positivo|acima|maior)/i,"<span class=positivo>$1</span>").replace(/(negativo|abaixo|menor)/i,"<span class=negativo>$1</span>")]}'
		}
	};

	var tableEstatistica1 = {
		xtype : 'basetable',
		width : '100%',
		ref : '../../../tableEstatistica1',
		data : [],
		readerConfig : {
			fields : [col1Config, {
				name : 'Quantidade',
				type : 'float',
				header : {
					width : '20%',
					cls : 'centered',
					title : 'Qtd.'
				},
				cell : {
					cls : 'centered',
					format : '{[FDESK.util.Util.formatSinal(Ext.util.Format.number(values.Quantidade, "0.000/i"), true)]}'
				}
			}, {
				name : 'Percentual',
				type : 'float',
				header : {
					width : '20%',
					cls : 'centered',
					title : 'Perct.'
				},
				cell : {
					cls : 'centered',
					format : '{[FDESK.util.Util.formatSinalPercent(Ext.util.Format.number(values.Percentual, "0.000,0/i"), true)]}'
				}
			}]
		}
	};

	var tableEstatistica2 = {
		xtype : 'basetable',
		width : '100%',
		ref : '../../../tableEstatistica2',
		readerConfig : {
			fields : [col1Config, {
				name : 'Retorno',
				type : 'float',
				header : {
					width : '20%',
					cls : 'centered',
					title : 'Rentab.'
				},
				cell : {
					cls : 'centered',
					format : '{[FDESK.util.Util.formatSinalPercent(Ext.util.Format.number(values.Retorno, "0.000,00/i"), true)]}'
				}
			}, {
				name : 'Mes',
				type : 'string',
				header : {
					width : '20%',
					cls : 'centered',
					title : 'M�s'
				},
				cell : {
					cls : 'centered'
				}
			}]
		}
	};

	var tableIndicadores = {
		xtype : 'basetable',
		width : '100%',
		ref : '../../../tableIndicadores',
		readerConfig : {
			fields : [{
				name : 'VolMes',
				type : 'float',
				header : {
					width : '33%',
					cls : 'centered',
					title : 'Vol M�s'
				},
				cell : {
					cls : 'centered',
					format : '{[FDESK.util.Util.formatSinalPercent(Ext.util.Format.number(values.VolMes, "0.000,00/i"), true)]}'
				}
			}, {
				name : 'Vol6Meses',
				type : 'float',
				header : {
					width : '33%',
					cls : 'centered',
					title : 'Vol 6 Meses'
				},
				cell : {
					cls : 'centered',
					format : '{[FDESK.util.Util.formatSinalPercent(Ext.util.Format.number(values.Vol6Meses, "0.000,00/i"), true)]}'
				}

			}, {
				name : 'Vol12Meses',
				type : 'float',
				header : {
					width : '33%',
					cls : 'centered',
					title : 'Vol 12 Meses'
				},
				cell : {
					cls : 'centered',
					format : '{[FDESK.util.Util.formatSinalPercent(Ext.util.Format.number(values.Vol12Meses, "0.000,00/i"), true)]}'
				}

			}]
		}
	};

	var tplTopHeaderRetornoPeriodo = String.format(
			'<tr><th class="{0}">Per�odo</th>'
					+ '<th class="{0}" colspan="3">Rentabilidade(%)</th></tr>',
			'centered');

	var tableRetornoPeriodo = {
		xtype : 'basetable',
		ref : '../../../tableRetornoPeriodo',
		tplTopHeader : tplTopHeaderRetornoPeriodo,
		width : '100%',
		readerConfig : {
			fields : [{
						name : 'Periodo',
						type : 'string',
						header : {
							width : '40%',
							cls : 'centered',
							title : ''
						},
						cell : {
							cls : 'centered'
						}
					}, {
						name : 'RetornoCarteira',
						type : 'float',
						header : {
							width : '20%',
							cls : 'centered',
							title : 'Carteira'
						},
						cell : {
							cls : 'centered',
							format : '{[FDESK.util.Util.formatSinal(Ext.util.Format.number(values.RetornoCarteira, "0.000,00/i"), true)]}'
						}
					}, {
						name : 'RetornoBenchmark',
						type : 'float',
						header : {
							width : '20%',
							cls : 'centered',
							title : 'Indx.'
						},
						cell : {
							cls : 'centered',
							format : '{[FDESK.util.Util.formatSinal(Ext.util.Format.number(values.RetornoBenchmark, "0.000,00/i"), true)]}'
						}
					}, {
						name : 'RetornoDiferencial',
						type : 'float',
						header : {
							width : '20%',
							cls : 'centered',
							title : '% Indx.'
						},
						cell : {
							cls : 'centered',
							format : '{[FDESK.util.Util.formatSinal(Ext.util.Format.number(values.RetornoDiferencial, "0.000,00/i"), true)]}'
						}
					}]
		},
		data : [{}],

		listeners : {
			'refresh' : function(table) {
				var tableRowEl = table.getEl().query('tr')[1];
				var ths = table.getEl().query('th');
				tableRowEl.removeChild(ths[2]);
				ths[0].rowSpan = 2;
			}
		}
	};

	var panelQuadroRetorno = {
		xtype : 'panel',
		frame : true,
		items : tableQuadroRetorno,
		title : 'Quadro Retorno',
		columnWidth : .984,
		tools : [{
		        qtip : FDESK.AS.helpQtip,
		        id : 'help',
			    handler : function(){
			        FDESK.LE.openHelpWin({topic: 'module_3_1'});
			    }
		    }]
	};

	var panelRiscoRetorno = {
		xtype : 'panel',
		frame : true,
		ref : '../../panelRiscoRetorno',
		items : [tableEstatistica1, tableEstatistica2, tableIndicadores],
		title : 'Risco / Retorno',
		tools : [{
		        qtip : FDESK.AS.helpQtip,
		        id : 'help',
			    handler : function(){
			        FDESK.LE.openHelpWin({topic: 'module_3_3'});
			    }
		    }]
	};

	var panelRetornoPeriodo = {
		xtype : 'panel',
		frame : true,
		ref : '../../panelRetornoPeriodo',
		items : [tableRetornoPeriodo],
		title : 'Retornos por Per�odo',
		bodyStyle : 'background-color: #EFF5FF;',
		tools : [{
		        qtip : FDESK.AS.helpQtip,
                id : 'help',
			    handler : function(){
			        FDESK.LE.openHelpWin({topic: 'module_3_4'});
			    }
		    }]
	};

	var columnLeft = {
		defaults : {
			style : 'margin-bottom: 30px;'
		},
		items : [graficoIndicadores1, panelRiscoRetorno],
		columnWidth : .492,
		style : 'margin-right: 30px;'
	};

	var columnRight = {
		defaults : {
			style : 'margin-bottom: 30px;'
		},
		items : [graficoIndicadores2, panelRetornoPeriodo],
		columnWidth : .492
	};

	var panelColumns = {
		border : false,
		defaults : {
			border : false,
			style : 'margin-bottom: 30px;',
			bodyStyle : 'background-color: transparent'
		},
		xtype : 'panel',
		layout : 'column',
		style : 'margin: 30px 20px 0 20px;',
		bodyStyle : 'background-color: transparent',
		items : [panelQuadroRetorno, columnLeft, columnRight]
	};

	this.items = [panelColumns];

	FDESK.panel.ZoomIndicadores.superclass.constructor.call(this, config);

	this.subscribeEvent('tabindicadorestablesloaded', this.loadTables, this);
	
	this.subscribeEvent('zoomcardsresize', this.adjustHeight, this);
	this.subscribeEvent('dateperiodochanged',
							function(date) {
								this.loadGraficos({});
							}, this);
	
	
	this.subscribeEvent('indicechanged',
							function(idIndiceParam) {
								this.loadGraficos({});
							}, this);
							
	
	this.subscribeEvent('environmentupdated',
							function() {
								this.loadGraficos({});
							}, this);

	
	this.graficoIndicadores1.selectChart(0);
	this.graficoIndicadores2.selectChart(1);


};

Ext.extend(FDESK.panel.ZoomIndicadores, Ext.Panel, {
			border : false,
			
			loadGraficos : function(params){
			    this.graficoIndicadores1.updateChart(null, {});
				this.graficoIndicadores2.updateChart(null, {});
			},
			
			loadTables : function(data) {
			    
				data.quadroRetorno && this.tableQuadroRetorno.loadData(data.quadroRetorno);
				data.estatistica1 && this.tableEstatistica1.loadData(data.estatistica1);
				data.estatistica2 && this.tableEstatistica2.loadData(data.estatistica2);
				data.indicadores && this.tableIndicadores.loadData([data.indicadores]);
				data.retornoPeriodo && this.tableRetornoPeriodo.loadData(data.retornoPeriodo);

			},
			adjustHeight : function() {
				var h = Math.round(((this.ownerCt.getWidth() - 90) / 2) / 1.25);
				this.graficoIndicadores1.setHeight(h);
				this.graficoIndicadores2.setHeight(h);

				if (this.panelRetornoPeriodo.rendered) {
					this.panelRetornoPeriodo.setHeight(this.panelRiscoRetorno
							.getHeight());
				}

			}
		});
Ext.reg('zoomindicadores', FDESK.panel.ZoomIndicadores);