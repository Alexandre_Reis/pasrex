Ext.namespace('FDESK.panel.ZoomRelatorios');
FDESK.panel.ZoomRelatorios = function(config) {


    // Custom rendering Template
    var resultTpl = new Ext.XTemplate(
        '<tpl for="."><div class="x-combo-list-item level-{Level} isleaf-{IsLeaf}">{Singular}</div></tpl>'
    ); 
    
    
	this.comboRelatorio = new Ext.form.ComboBox({
				fieldLabel : 'Extrato',
				hiddenName : 'IdRelatorio',
				//ctCls : 'flex-combo',
				store : FDESK.AS.stores.relatorioInfo,
				width : 410,
				listWidth: 400,
				valueField : 'Url',
				displayField : 'Singular',
				mode : 'local',
				triggerAction : 'all',
				editable : false,
				tpl : resultTpl,
				ctCls : 'form-item-relatorio',
				allowBlank : false,
				listeners : {
				    'beforeselect' : function(combo, record, index) {
					    return (record.get('Url').length > 0);
					},
					'select' : function(combo, record, index) {
					    this.updateIframeSrc(combo.getValue());
					},
					scope : this
				}
			});
	

	this.iframe = new Ext.Component({
				autoEl : {
					tag : 'iframe',
					width : '100%',
					height: '330px',
                    style : 'border: 0px none; visibility: hidden;',
					scrolling : 'no'
				}
			});

	var panelRelatorios = {
		xtype : 'form',
		id : 'form-relatorios',
		autoHeight : true,
		width: 700,
		labelWidth: 60,
		defaults : {
			style : 'margin-bottom: 10px;'
		},
		style : 'margin: 30px auto 0 auto;',
		bodyStyle : 'padding:5px 0 0 0;',
		frame : true,
		items : [this.comboRelatorio, this.iframe], 
		title : 'Extratos - IR',
		tools : [{
		        qtip : FDESK.AS.helpQtip,
		        id : 'help',
			    handler : function(){
			        FDESK.LE.openHelpWin({topic: 'module_4'});
			    }
		    }]
	};
	
	this.items = [panelRelatorios];

	FDESK.panel.ZoomRelatorios.superclass.constructor.call(this, config);
};

Ext.extend(FDESK.panel.ZoomRelatorios, Ext.Panel, {
			cls : 'zoom-relatorios',
			getIframePanelContainer : function(){
			    return this.iframe.document && this.iframe.document.getElementById('container_small');
			},
			
			updateIframeSrc : function(url){

                var iframeDom = this.iframe.getEl().dom;

			    iframeDom.src =  url;
	            iframeDom.style.visibility = 'hidden';
	            
	            this.iframe.getEl().on('load', function(){
	                this.iframe.document = iframeDom.contentDocument || iframeDom.contentWindow.document;
                    this.iframeLoaded();
	            }, this);
	            
            },
			
			iframeLoaded : function(){
    		    var btnVisualiza = this.iframe.document.getElementById('btnVisualiza');
    		    if(btnVisualiza){
    		        btnVisualiza.style.display = 'none';
    		    }
    		    this.iframe.getEl().dom.style.visibility = 'visible';
                this.getIframePanelContainer().style.height = (this.iframe.getEl().getHeight()-10) + 'px';    		    
	        }
		});

Ext.reg('zoomrelatorios', FDESK.panel.ZoomRelatorios);