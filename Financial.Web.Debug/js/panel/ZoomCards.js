Ext.namespace('FDESK.panel.ZoomCards');
FDESK.panel.ZoomCards = function(config) {

	var datePicker = {
		xtype : 'datefield',
		format : 'j \\de F \\de Y',
		ctCls : 'flex-combo flex-datepicker',
		maxValue : FDESK.AS.user.dataCliente,
		editable : false,
		width : 210,
		hidden: FDESK.AppSettings.g_ShowCarteiraOnline === true,
		id : 'picker-data-referencia',
		value : FDESK.AS.dataReferencia,
		listeners : {
			'select' : function(datePicker, date) {
				FDESK.AS.setDataReferencia(date, null, false);
			},
			scope : this
		}
	};

	this.comboIndice = new Ext.form.ComboBox({
				hiddenName : 'IdIndice',
				hidden : true,
				id : 'combo-indice',
				ctCls : 'flex-combo',
				store :FDESK.AS.stores.indices,
				width : 210,
				valueField: 'IdIndice',
    			displayField: 'Descricao',
				mode : 'local',
				triggerAction : 'all',
				editable : false,
				value : FDESK.AS.idIndice,
				listeners : {
					'select' : function(combo, record, index) {
					    FDESK.AS.setIdIndice(combo.getValue(), null, false);
					},
					scope : this
				}
			});
			
	var buttonZoomPosicaoCarteira = {
		xtype : 'button',
		scope : this,
		ctCls : 'x-btn-flexleft',
		allowDepress : false,
		toggleGroup : 'zoomcards',
		enableToggle : true,
		pressed : true,
		handler : function() {
			this.setVisibleComboIndice(false);
			this.layout.setActiveItem(0);
		},
		text : 'CARTEIRA'
	};

	var buttonZoomPerspectiva = {
		xtype : 'button',
		scope : this,
		ctCls : FDESK.AppSettings.g_HideRelatorio ? 'x-btn-flexright' : 'x-btn-flexcenter',
		toggleGroup : 'zoomcards',
		allowDepress : false,
		enableToggle : true,
		toggleHandler : function() {
			this.setVisibleComboIndice(true);
			this.layout.setActiveItem(1);
		},
		text : 'INDICADORES'
	};
	var buttonZoomRelatorio = FDESK.AppSettings.g_HideRelatorio ? null : {
		xtype : 'button',
		scope : this,
		ctCls : 'x-btn-flexright',
		toggleGroup : 'zoomcards',
		allowDepress : false,
		enableToggle : true,
		toggleHandler : function() {
			this.setVisibleComboIndice(false);
			this.layout.setActiveItem(2);
		},
		text : 'EXTRATOS'
	};

    var buttonZoomCarteiraOnline = FDESK.AppSettings.g_ShowCarteiraOnline ? {
		xtype : 'button',
		scope : this,
		ctCls : 'x-btn-flexright',
		toggleGroup : 'zoomcards',
		allowDepress : false,
		enableToggle : true,
		toggleHandler : function() {
			this.setVisibleComboIndice(false);
			this.layout.setActiveItem(0);
		},
		text : 'CARTEIRA ONLINE'
	} : null;
	
	var buttonCollapseHeader = {
		xtype : 'button',
		scope : this,
		ctCls : 'x-btn-flexcollapse',
		enableToggle : true,
		toggleHandler : function() {
			this.publishEvent('collapsenorthpanelpressed');
		}
	};

	var panelZoomPosicaoCarteira = {
		xtype : 'zoomposicaocarteira',
		autoScroll : true
	};

	var panelZoomIndicadores = {
		xtype : 'zoomindicadores',
		width : '100%',
		ref : 'panelZoomIndicadores',
		autoScroll : true
	};

	var panelZoomRelatorios = {
		xtype : 'zoomrelatorios',
		ref : 'panelZoomRelatorios',
		hideMode : 'display',
		autoScroll: true
	};

	var panelZoomCarteiraOnline = {
		xtype : 'zoomcarteiraonline',
		ref : 'panelZoomCarteiraOnline',
		hideMode : 'display'/*,
		autoScroll : true*/
	};


	if(FDESK.AppSettings.g_ShowCarteiraOnline){
	    buttonZoomPosicaoCarteira = buttonZoomPerspectiva = buttonZoomRelatorio = null;
	    panelZoomPosicaoCarteira = panelZoomIndicadores = panelZoomRelatorios = null;
	}


	var defConfig = {
		tbar : {
			cls : 'zoomcards-toolbar',
			items : [buttonZoomPosicaoCarteira, buttonZoomPerspectiva,
					buttonZoomRelatorio, '->', '<div id="combo-indice-label">�ndice:</div>', this.comboIndice,
					(FDESK.AppSettings.g_ShowCarteiraOnline === true ? '':'Data Refer�ncia:'), datePicker, '-',
					buttonCollapseHeader].compact()
		},
		layout : 'card',
		// cls : 'zoomcards',
		ctCls : 'zoomcards-panel',
		bodyStyle : 'background-color: transparent',
		border : false,
		forceLayout : true,
		deferredRender : true,
		defaults : {
			hideMode : 'offsets',
			bodyStyle : 'background-color: transparent'
		},
		items : [panelZoomPosicaoCarteira, panelZoomIndicadores,
				panelZoomRelatorios, panelZoomCarteiraOnline].compact(),
		activeItem : 0,
		monitorResize : true
	};

	Ext.applyIf(config, defConfig);
	FDESK.panel.ZoomCards.superclass.constructor.call(this, config);

	this.on('afterlayout', this.adjustHeight, this);
	this.subscribeEvent('viewporttogglecollapse', this.adjustHeight, this);
};

Ext.extend(FDESK.panel.ZoomCards, Ext.Panel, {
			setVisibleComboIndice : function(show){
				Ext.get('combo-indice-label').setVisible(show);
				this.comboIndice.setVisible(show);
			},
			
			adjustHeight : function() {
				var headerPanel = this.ownerCt.ownerCt.headerPanel;
				this.setHeight(Ext.lib.Dom.getViewHeight()
						- (headerPanel.getHeight() || 6) - 2);
				this.publishEvent('zoomcardsresize');
			}
		});

Ext.reg('zoomcards', FDESK.panel.ZoomCards);