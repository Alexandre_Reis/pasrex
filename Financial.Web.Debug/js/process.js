﻿$(document).ready(function() {
	$("#spaceused1").progressBar({width: 240});
});

var Financial = {};
Financial.Util = {};
Financial.Util.ProgressBar = {};

Financial.Util.ProgressBar.PrepareProcess = function(config)
{   
    for(prop in config){
        Financial.Util.ProgressBar[prop] = config[prop];
    }
    var grid = Financial.Util.ProgressBar.Grid;
    grid.GetSelectedFieldValues('IdCliente;Apelido', OnGetSelectedFieldValuesDiario);    
}

function OnGetSelectedFieldValuesDiario(selectedValues)
{  
    var idClientes=[], nomeClientes=[];
    if(selectedValues.length == 0) return;
     
    for(i = 0; i < selectedValues.length; i++) 
    {
        idClientes.push(selectedValues[i][0]);
        nomeClientes.push(selectedValues[i][1]);
    }
    Financial.Util.ProgressBar.StartProcess(idClientes, nomeClientes);        
}

Financial.Util.ProgressBar.StopProcess = function() {
    Financial.Util.ProgressBar.halt = true;
    $("#client-info").html('Aguarde... O processamento será interrompido após o cliente atual ser processado');
    
}

Financial.Util.ProgressBar.StartProcess = function(idClientes, nomeClientes) 
{   
    Financial.Util.ProgressBar.IdClientes = idClientes;
    Financial.Util.ProgressBar.NomeClientes = nomeClientes;    
    
    $('#spaceused1').progressBar(0);
    progressWindow.Show();
    $('#progressWindow_btnClose').hide();
    $('#progressWindow_btnStop').show();
    
    Financial.Util.ProgressBar.halt = false;
    Financial.Util.ProgressBar.ProcessClient(0);
    return true;
}

Financial.Util.ProgressBar.ProcessClient = function(index)
{
    var idCliente = Financial.Util.ProgressBar.IdClientes[index];
    var nomeCliente = Financial.Util.ProgressBar.NomeClientes[index];
    
    $("#client-info").html('Processando: ' + nomeCliente);
  
	$.ajax({
	    url: "ControllerProcessaCliente.ashx",
	    dataType: 'json',
	    cache: false,
	    data: {idCliente : idCliente, 
	           tipoProcesso : Financial.Util.ProgressBar.TipoProcesso,
	           processaBolsa : Financial.Util.ProgressBar.ProcessaBolsa,
	           processaBMF : Financial.Util.ProgressBar.ProcessaBMF,
	           processaRendaFixa : Financial.Util.ProgressBar.ProcessaRendaFixa,
	           processaSwap : Financial.Util.ProgressBar.ProcessaSwap,
	           integraBolsa : Financial.Util.ProgressBar.IntegraBolsa,
	           integraBMF : Financial.Util.ProgressBar.IntegraBMF,
	           integraCC : Financial.Util.ProgressBar.IntegraCC,
	           dataFinal : Financial.Util.ProgressBar.DataFinal},
	    //async: false,
	    success: function(data) {
	    
	        if(data.success === false){
	            $("#client-info").html('<span class="warning">' + data.errorMessage + '</span>');
		        $('#progressWindow_btnClose').show();
                $('#progressWindow_btnStop').hide();
                return;
	        }
	    
		    $('#spaceused1').progressBar(((index + 1) / Financial.Util.ProgressBar.IdClientes.length) * 100);
	    
		    //Checar se processamos o ultimo
		    if(index == (Financial.Util.ProgressBar.IdClientes.length - 1)){
		        $("#client-info").html('Fim do Processamento');
		        $('#progressWindow_btnClose').show();
                $('#progressWindow_btnStop').hide();                
		        return;
		    }
		    
		    //Checar variavel global de interrupcao antes de continuar
            if(Financial.Util.ProgressBar.halt === true){
                progressWindow.Hide();                
                return;
            }
            
            //Encadear chamada do proximo recursivamente
            Financial.Util.ProgressBar.ProcessClient(index + 1);            
        }
    });        
}
