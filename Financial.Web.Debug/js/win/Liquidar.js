Ext.namespace('FDESK.win.Liquidar');

FDESK.win.Liquidar = function(config) {
	var tableDetalheAtivo = {
		xtype : 'basetable',
//		tableCls : 'x-table-detalhe-ativo',
		cls : 'x-table-fixedheader x-table-fixedheader2line',
		ref : 'tableDetalheAtivo',
		emptyText: FDESK.AS.tableOperacaoEmptyText,
		width : '100%',
		autoScroll : Ext.isWebKit ? true : undefined,
		height: Ext.isWebKit ? 259 : undefined,
		scrollerHeight : Ext.isWebKit ? undefined : (222 + (Ext.isIE ? 39 : 0) + 'px'),

		readerConfig : {
			fields : [{
						name : 'DataLancamento',
						type : 'string',
						header : {
							width : '15%',
							cls : 'centered',
							title : 'Data Lançamento'
						},
						cell : {
							cls : 'centered data'
						}
					},{
						name : 'DataVencimento',
						type : 'string',
						header : {
							width : '15%',
							cls : 'centered',
							title : 'Data Vencimento'
						},
						cell : {
							cls : 'centered data'
						}
					},{
						name : 'Descricao',
						type : 'string',
						header : {
							width : '50%',
							cls : 'centered',
							title : 'Descrição'
						}
					}, {
						name : 'Valor',
						type : 'float',
						header : {
							width : '20%',
							cls : 'centered',
							title : 'Valor'
						},
						cell : {
							cls : 'right-aligned last',
							format : '{$n$:number("0.000,00/i")}'
						}
					}]
		}
	};

	config.items = [tableDetalheAtivo];

	FDESK.win.Liquidar.superclass.constructor.call(this, config);
};

Ext.extend(FDESK.win.Liquidar, FDESK.win.Ativo, {
			title : 'Liquidação Proj.',

			updateForm : function(config, show) {
				this.params = {
					dateBegin : FDESK.AS.dataReferencia.format('Y-m-d'),
					dateEnd : FDESK.AS.dataReferencia.format('Y-m-d'),
					tables : this.tipoRegistro,
					date : FDESK.AS.dataReferencia.format('Y-m-d')
				};
				FDESK.win.Liquidar.superclass.updateForm.apply(this, [config,
								show, this.params]);
			},

			afterUpdateForm : function(obj) {
				var data = obj.valoresLiquidar;
				if (data) {
					
					var rec = FDESK.AS.stores.tipoAtivo.getAt(FDESK.AS.stores.tipoAtivo.find(
						'Tipo', this.tipoRegistro));

					this.headerTitle.update({
								NomeAtivo : rec.data.Plural
							});

					this.tableDetalheAtivo.store
							.loadData(data);
				}

				FDESK.win.Liquidar.superclass.afterUpdateForm.call(this);
			}
		});