Ext.namespace('FDESK.win.RendaFixa');

FDESK.win.RendaFixa = function(config) {
	config = config || {};
	config.tableDetalheAtivoConfig = {
		xtype : 'basetable',
		tableCls : 'x-table-detalhe-ativo',
		ref : 'tableDetalheAtivo',
		width : '100%',
		readerConfig : {
			fields : [{
						name : 'DataOperacao',
						type : 'string',
						header : {
//							width : '20%',
							cls : 'centered',
							title : 'Data Operacao'
						},
						cell : {
							cls : 'centered data'
						}
					}, {
						name : 'DataVencimento',
						type : 'string',
						header : {
	//						width : '20%',
							cls : 'centered',
							title : 'Data Vencimento'
						},
						cell : {
							cls : 'centered data'
						}
					}, {
						name : 'Quantidade',
						type : 'float',
						header : {
		//					width : '20%',
							cls : 'centered',
							title : 'Quantidade'
						},
						cell : {
							cls : 'centered',
							format : '{$n$:number("0.000/i")}'
						}
					}, {
						name : 'PuMercado',
						type : 'float',
						header : {
		//					width : '20%',
							cls : 'centered',
							title : 'PU Mercado'
						},
						cell : {
							cls : 'centered',
							format : '{$n$:number("0.000,00/i")}'
						}
					}, {
						name : 'PuOperacao',
						type : 'float',
						header : {
//							width : '20%',
							cls : 'centered',
							title : 'PU Operacao'
						},
						cell : {
							cls : 'centered',
							format : '{$n$:number("0.000,00/i")}'
						}
					}, {
						name : 'SaldoBruto',
						type : 'float',
						header : {
	//						width : '20%',
							cls : 'centered',
							title : 'Saldo Bruto'
						},
						cell : {
							cls : 'centered',
							format : '{$n$:number("0.000,00/i")}'
						}
					}, {
						name : 'ValorIR',
						type : 'float',
						header : {
		//					width : '20%',
							cls : 'centered',
							title : 'IR'
						},
						cell : {
							cls : 'centered',
							format : '{$n$:number("0.000,00/i")}'
						}
					}, {
						name : 'ValorIOF',
						type : 'float',
						header : {
		//					width : '20%',
							cls : 'centered',
							title : 'IOF'
						},
						cell : {
							cls : 'centered',
							format : '{$n$:number("0.000,00/i")}'
						}
					}, {
						name : 'SaldoLiquido',
						type : 'float',
						header : {
			//				width : '20%',
							cls : 'centered',
							title : 'Saldo Liquido'
						},
						cell : {
							cls : 'centered',
							format : '{$n$:number("0.000,00/i")}'
						}
					}]
		}
	};

	config.hideSeletor = true;

	FDESK.win.RendaFixa.superclass.constructor.call(this, config);

};

Ext.extend(FDESK.win.RendaFixa, FDESK.win.SeletorPeriodo, {
			title : 'Renda Fixa',
			// tables : 'RendaFixa',
			fieldDescricao : 'DescricaoTitulo',
			posicaoSinteticaMethod : 'posicaoRendaFixaSintetica',
			operacaoAnaliticaMethod : 'operacaoRendaFixaAnalitica'
		});
