Ext.namespace('FDESK.win.SeletorPeriodo');

FDESK.win.SeletorPeriodo = function(config) {
	var tableDetalheAtivo = config.tableDetalheAtivoConfig;
	var panelAtivoSintetico, panelAtivoAnalitico;

	if (this.operacaoSinteticaMethod) {
		var tplTopHeaderAtivoSintetico = String.format(
				'<tr><th class="{0}">&nbsp</th>'
						+ '<th class="{0}" colspan="3">Compra</th>'
						+ '<th class="{0}" colspan="3">Venda</th>'
						+ '<th class="{0}" colspan="3">Posi��o</th>' + '</tr>',
				'centered');

		var tableAtivoSintetico = {
			xtype : 'basetable',
			emptyText : FDESK.AS.tableOperacaoEmptyText,
			tplTopHeader : tplTopHeaderAtivoSintetico,
			cls : 'x-table-fixedheader x-table-fixedheader2line',
			scrollerHeight : 86 + (Ext.isIE ? 39 : 0) + 'px',
			ref : '../../tableAtivoSintetico',
			width : '100%',
			readerConfig : {
				fields : [{
							name : 'Data',
							type : 'string',
							header : {
								width : '10%',
								cls : 'centered',
								title : 'Data'
							},
							cell : {
								cls : 'centered data'
							}
						}, {
							name : 'QuantidadeCompra',
							type : 'int',
							header : {
								width : '10%',
								cls : 'centered',
								title : 'Qtde'
							},
							cell : {
								cls : 'right-aligned compra',
								format : '{$n$:number("0.000/i")}'
							}
						}, {
							name : 'PrecoCompra',
							type : 'float',
							header : {
								width : '10%',
								cls : 'centered',
								title : 'Pre�o'
							},
							cell : {
								cls : 'right-aligned compra',
								format : '{$n$:number("0.000,00/i")}'
							}
						}, {
							name : 'ValorCompra',
							type : 'float',
							header : {
								width : '10%',
								cls : 'centered',
								title : 'Valor'
							},
							cell : {
								cls : 'right-aligned compra',
								format : '{$n$:number("0.000,00/i")}'
							}
						}, {
							name : 'QuantidadeVenda',
							type : 'int',
							header : {
								width : '10%',
								cls : 'centered',
								title : 'Qtde'
							},
							cell : {
								cls : 'right-aligned venda',
								format : '{$n$:number("0.000/i")}'
							}
						}, {
							name : 'PrecoVenda',
							type : 'float',
							header : {
								width : '10%',
								cls : 'centered',
								title : 'Preco'
							},
							cell : {
								cls : 'right-aligned venda',
								format : '{$n$:number("0.000,00/i")}'
							}
						}, {
							name : 'ValorVenda',
							type : 'float',
							header : {
								width : '10%',
								cls : 'centered',
								title : 'Valor'
							},
							cell : {
								cls : 'right-aligned venda',
								format : '{$n$:number("0.000,00/i")}'
							}
						}, {
							name : 'QuantidadePosicao',
							type : 'int',
							header : {
								width : '10%',
								cls : 'centered',
								title : 'Qtde'
							},
							cell : {
								cls : 'right-aligned posicao',
								format : '{$n$:number("0.000/i")}'
							}
						}, {
							name : 'PrecoPosicao',
							type : 'float',
							header : {
								width : '10%',
								cls : 'centered',
								title : 'Preco'
							},
							cell : {
								cls : 'right-aligned posicao',
								format : '{$n$:number("0.000,00/i")}'
							}
						}, {
							name : 'ValorPosicao',
							type : 'float',
							header : {
								width : '10%',
								cls : 'centered',
								title : 'Valor'
							},
							cell : {
								cls : 'right-aligned posicao last',
								format : '{$n$:number("0.000,00/i")}'
							}
						}]
			}
		};

		var panelAtivoSintetico = {
			xtype : 'panel',
			title : 'Sint�tico',
			items : [tableAtivoSintetico]
		};

	}

	if (this.operacaoAnaliticaMethod) {
		var tableAtivoAnalitico = config.tableAtivoAnaliticoConfig || {
			xtype : 'basetable',
			emptyText : FDESK.AS.tableOperacaoEmptyText,
			ref : '../../tableAtivoAnalitico',
			cls : 'x-table-fixedheader',
			scrollerHeight : 110 + (Ext.isIE ? 24 : 0) + 'px',
			width : '100%',
			readerConfig : {
				fields : [{
							name : 'Data',
							type : 'string',
							header : {
								width : '20%',
								cls : 'centered',
								title : 'Data'
							},
							cell : {
								cls : 'centered'
							}
						}, {
							name : 'TipoOperacao',
							type : 'string',
							header : {
								width : '20%',
								cls : 'centered',
								title : 'Tipo'
							},
							cell : {
								cls : 'centered',
								format : '{[FDESK.AS.tipoOperacao[values["TipoOperacao"]]]}'
							}
						}, {
							name : 'Quantidade',
							type : 'int',
							header : {
								width : '20%',
								cls : 'centered',
								title : 'Qtde'
							},
							cell : {
								cls : 'right-aligned',
								format : '{$n$:number("0.000/i")}'
							}
						}, {
							name : 'Preco',
							type : 'float',
							header : {
								width : '20%',
								cls : 'centered',
								title : 'Pre�o'
							},
							cell : {
								cls : 'right-aligned',
								format : '{$n$:number("0.000,00/i")}'
							}
						}, {
							name : 'Valor',
							type : 'float',
							header : {
								width : '20%',
								cls : 'centered',
								title : 'Valor'
							},
							cell : {
								cls : 'right-aligned last',
								format : '{$n$:number("0.000,00/i")}'
							}
						}]
			}
		};

		var panelAtivoAnalitico = {
			xtype : 'panel',
			bodyStyle : 'overflow-x: hidden;',
			title : 'Anal�tico',
			items : [tableAtivoAnalitico],
			listeners : {
				'activate' : function(p) {
					// Hack maldito para IE refazer layout e aparecer scrollbar
					// no
					// body
					p.ownerCt.setActiveTab(0);
					this.removeListener('activate', arguments.callee, this);
					p.ownerCt.setActiveTab(1);
				}
			}
		};
	}

	var tabPanel = {
		ref : 'tabPanel',
		xtype : 'tabpanel',
		cls : 'tabpanel-seletor-periodo',
		deferredRender : false,
		style : config.hideSeletor === true ? 'display:none': undefined,
		activeItem : 0,
		bodyStyle : 'border-bottom: 0; border-left:0; border-right:0;',
		defaults : {
			autoScroll : Ext.isWebKit,
			autoHeight : !Ext.isWebKit,
			height: Ext.isWebKit ? 132 : undefined
		},
		items : [panelAtivoSintetico, panelAtivoAnalitico].compact()
	};

	var dateFieldCfg = {
		xtype : 'datefield',
		format : 'd - M - Y',
		editable : false,
		ctCls : 'flex-combo-small',
		//width : 116,
		value : FDESK.AS.dataReferencia,
		listeners : {
			scope : this,
			select : function(field, newDate) {
				if (!field.lastValue
						|| field.lastValue.getTime() !== newDate.getTime()) {
					field.lastValue = field.getValue();
					this.updateForm();
				}
			}
		}
	};
	this.id = Ext.id();
	this.fPeriodoInicio = new Ext.form.DateField(Ext.apply({
				id : 'startdt-' + this.id,
				name : 'startdt',
				vtype : 'daterange',
				endDateField : 'enddt-' + this.id
			}, dateFieldCfg));

	this.fPeriodoFim = new Ext.form.DateField(Ext.apply({
				id : 'enddt-' + this.id,
				name : 'enddt',
				vtype : 'daterange',
				startDateField : 'startdt-' + this.id
			}, dateFieldCfg));

	this.boxSeletorPeriodo = new Ext.Container({
				cls : 'box-seletor-periodo',
				xtype : 'container',
				items : [{
							xtype : 'box',
							autoEl : {
								tag : 'div',
								style : 'margin-right:4px;padding-top:2px;',
								html : 'Per�odo:'
							}
						}, this.fPeriodoInicio, {
							xtype : 'box',
							autoEl : {
								tag : 'div',
								style : 'margin:0 4px 0 4px;padding-top:2px;',
								html : 'a'
							}
						}, this.fPeriodoFim]
			});

	config.items = [tableDetalheAtivo, tabPanel];
	if(config.hideSeletor === true){
	    config.height = 216;
	}

	
	FDESK.win.SeletorPeriodo.superclass.constructor.call(this, config);

	this.on('afterlayout', function() {
				this.boxSeletorPeriodo.render(this.tabPanel.header
						.query('.x-tab-strip')[0]);
			}, this, {
				single : true
			});
};

Ext.extend(FDESK.win.SeletorPeriodo, FDESK.win.Ativo, {
			updateForm : function(config, show) {
				if (show) {
					// Resetar data para a �ltima carregada nas tabelas
					this.fPeriodoInicio.setValue(FDESK.AS.dataReferencia);
					this.fPeriodoFim.setValue(FDESK.AS.dataReferencia);
				};

				var params = {
					dateBegin : this.fPeriodoInicio.getValue().format('Y-m-d'),
					dateEnd : this.fPeriodoFim.getValue().format('Y-m-d'),
					tables : this.tipoRegistro,
					date : FDESK.AS.dataReferencia.format('Y-m-d')
				};

				if (this.body) {
					// Mask para atualizacao de data do seletor
					this.body.mask('Carregando...',
							'ext-el-mask-msg x-mask-loading');
				}

				FDESK.win.SeletorPeriodo.superclass.updateForm.apply(this, [
								config, show, params]);
			},
			afterUpdateForm : function(obj) {
				if (obj[this.posicaoSinteticaMethod]) {
					var data = obj[this.posicaoSinteticaMethod];
					this.headerTitle.update({
								NomeAtivo : data[this.fieldDescricao]
							});

					this.tableDetalheAtivo.loadData([data]);
				}

				if (obj[this.operacaoSinteticaMethod]) {
					this.tableAtivoSintetico
							.loadData(obj[this.operacaoSinteticaMethod]);
				}

				if (obj[this.operacaoAnaliticaMethod]) {
					this.tableAtivoAnalitico
							.loadData(obj[this.operacaoAnaliticaMethod]);
				}

				this.body.unmask();
				FDESK.win.SeletorPeriodo.superclass.afterUpdateForm.call(this);
			}
		});
