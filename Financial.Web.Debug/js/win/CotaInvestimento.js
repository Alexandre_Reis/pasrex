Ext.namespace('FDESK.win.CotaInvestimento');

FDESK.win.CotaInvestimento = function(config) {
	var tableDetalheAtivo = {
		xtype : 'basetable',
		ref : 'tableDetalheAtivo',
		width : '100%',
		tableCls : 'x-table-detalhe-ativo',
		readerConfig : {
			fields : [{
						name : 'Quantidade',
						type : 'float',
						header : {
							width : '16%',
							cls : 'centered',
							title : 'Quantidade'
						},
						cell : {
							cls : 'centered',
							format : '{$n$:number("0.000/i")}'
						}
					}, {
						name : 'ValorCota',
						type : 'float',
						header : {
							width : '16%',
							cls : 'centered',
							title : 'Valor Cota'
						},
						cell : {
							cls : 'centered',
							format : '{$n$:number("0.000,0000000/i")}'
						}
					}, {
						name : 'SaldoBruto',
						type : 'float',
						header : {
							width : '16%',
							cls : 'centered',
							title : 'Saldo Bruto'
						},
						cell : {
							cls : 'centered',
							format : '{$n$:number("0.000,00/i")}'
						}
					}, {
						name : 'ValorIR',
						type : 'float',
						header : {
							width : '16%',
							cls : 'centered',
							title : 'IR'
						},
						cell : {
							cls : 'centered',
							format : '{$n$:number("0.000,00/i")}'
						}
					}, {
						name : 'ValorIOF',
						type : 'float',
						header : {
							width : '16%',
							cls : 'centered',
							title : 'IOF'
						},
						cell : {
							cls : 'centered',
							format : '{$n$:number("0.000,00/i")}'
						}
					}, {
						name : 'SaldoLiquido',
						type : 'float',
						header : {
							width : '20%',
							cls : 'centered',
							title : 'Saldo Liquido'
						},
						cell : {
							cls : 'centered',
							format : '{$n$:number("0.000,00/i")}'
						}
					}]
		}
	};

	var tableSaldosAplics = {
		xtype : 'basetable',
		ref : '../../tableSaldosAplics',
		cls : 'x-table-fixedheader x-table-fixedheader2line',
		emptyText: FDESK.AS.tableOperacaoEmptyText,
		scrollerHeight : 108 + (Ext.isIE ? 39 : 0) + 'px',
		width : '100%',
		listeners : {
			'click' : function(dataView, index, node, e) {
				return; //quando tiver resgate tirar
				this.updateTableResgateAplics({
					pkResgateFundo : dataView.store.data.items[index].data.IdPosicao,
					dataAplicacao : dataView.store.data.items[index].data.DataAplicacao
				});
			},
			scope : this
		},
		readerConfig : {
			fields : [{
						name : 'IdPosicao',
						type : 'int'
					}, {
						name : 'DataAplicacao',
						type : 'string',
						header : {
							width : '12%',
							cls : 'centered',
							title : 'Data Aplicacao'
						},
						cell : {
							//cls : 'centered link' //quando tiver resgate
							cls : 'centered'
						}
					}, {
						name : 'CotaAplicacao',
						type : 'float',
						header : {
							width : '12%',
							cls : 'centered',
							title : 'Cota Aplicacao'
						},
						cell : {
							cls : 'right-aligned',
							format : '{$n$:number("0.000,0000000/i")}'
						}
					}, {
						name : 'ValorAplicado',
						type : 'float',
						header : {
							width : '12%',
							cls : 'centered',
							title : 'Valor Aplicado'
						},
						cell : {
							cls : 'right-aligned',
							format : '{$n$:number("0.000,00/i")}'
						}
					}, {
						name : 'Quantidade',
						type : 'float',
						header : {
							width : '12%',
							cls : 'centered',
							title : 'Qtde Cotas'
						},
						cell : {
							cls : 'right-aligned',
							format : '{$n$:number("0.000,0000000/i")}'
						}
					}, {
						name : 'SaldoBruto',
						type : 'float',
						header : {
							width : '12%',
							cls : 'centered',
							title : 'Saldo Bruto'
						},
						cell : {
							cls : 'right-aligned',
							format : '{$n$:number("0.000,00/i")}'
						}
					}, {
						name : 'ValorIR',
						type : 'float',
						header : {
							width : '12%',
							cls : 'centered',
							title : 'IR'
						},
						cell : {
							cls : 'right-aligned',
							format : '{$n$:number("0.000,00/i")}'
						}
					}, {
						name : 'ValorIOF',
						type : 'float',
						header : {
							width : '12%',
							cls : 'centered',
							title : 'IOF'
						},
						cell : {
							cls : 'right-aligned',
							format : '{$n$:number("0.000,00/i")}'
						}
					}, {
						name : 'SaldoLiquido',
						type : 'float',
						header : {
							width : '12%',
							cls : 'centered',
							title : 'Saldo Liquido'
						},
						cell : {
							cls : 'right-aligned last',
							format : '{$n$:number("0.000,00/i")}'
						}
					}]
		}
	};

	var tableResgatesAplics = {
		xtype : 'basetable',
		ref : '../../tableResgatesAplics',
		width : '100%',
		readerConfig : {
			fields : [{
						name : 'DataResgate',
						type : 'string',
						header : {
							width : '25%',
							cls : 'centered',
							title : 'Data Resgate'
						},
						cell : {
							cls : 'centered'
						}
					}, {
						name : 'Quantidade',
						type : 'float',
						header : {
							width : '25%',
							cls : 'centered',
							title : 'Qtde Resgatada'
						},
						cell : {
							cls : 'right-aligned',
							format : '{$n$:number("0.000,0000000/i")}'
						}
					}, {
						name : 'CotaResgate',
						type : 'float',
						header : {
							width : '25%',
							cls : 'centered',
							title : 'Cota Resgate'
						},
						cell : {
							cls : 'right-aligned',
							format : '{$n$:number("0.000,0000000/i")}'
						}
					}, {
						name : 'ValorResgatado',
						type : 'float',
						header : {
							width : '25%',
							cls : 'centered',
							title : 'Valor Resgatado'
						},
						cell : {
							cls : 'right-aligned',
							format : '{$n$:number("0.000,00/i")}'
						}
					}]
		}
	};

	var panelSaldosAplics = {
		xtype : 'panel',
		ref : '../panelSaldosAplics',
		title : 'Saldos Aplicacoes',
		items : [tableSaldosAplics]
	};

	var panelResgatesAplics = {
		xtype : 'panel',
		closable : true,
		listeners : {
			'beforeclose' : function() {
				this.tabPanel.hideTabStripItem(this.panelResgatesAplics);
				this.tabPanel.setActiveTab(this.panelSaldosAplics);
				return false;
			},
			scope : this
		},
		ref : '../panelResgatesAplics',
		title : 'Resgates Aplicacoes',
		items : [tableResgatesAplics]
	};

	var tabPanel = {
		ref : 'tabPanel',
		xtype : 'tabpanel',
		activeItem : 0,
		bodyStyle: 'border-bottom: 0; border-left:0; border-right:0',
		defaults : {
			autoScroll : Ext.isWebKit,
			autoHeight : !Ext.isWebKit,
			height: Ext.isWebKit ? 147 : undefined
		},
		//items : [panelSaldosAplics, panelResgatesAplics] //quando tiver resgate
		items : [panelSaldosAplics]
	};

	config.items = [tableDetalheAtivo, tabPanel];

	FDESK.win.CotaInvestimento.superclass.constructor.call(this, config);
};

Ext.extend(FDESK.win.CotaInvestimento, FDESK.win.Ativo, {
	title : 'Fundo',
	updateTableResgateAplics : function(params) {

		Ext.apply(this.params, params);
        params.idCliente = FDESK.AS.user.idCliente;

		this.panelResgatesAplics.setTitle(String.format(
				'Resgates Aplicacoes de {0}', params.dataAplicacao));

		Ext.Ajax.request({
					scope : this,
					method : 'GET',
					params : this.params,
					url : FDESK.AS.g_WebDbName + '/ashx/table.ashx',
					failure : function(response) {
						var obj = Ext.decode(response.responseText);
						FDESK.AS.errHandler(obj && obj.erros);
					},
					success : function(response) {
						var obj = Ext.decode(response.responseText);
						if (obj.operacaoResgateFundo) {
							this.tableResgatesAplics.store
									.loadData(obj.operacaoResgateFundo);
							this.tabPanel
									.unhideTabStripItem(this.panelResgatesAplics);
							this.tabPanel
									.setActiveTab(this.panelResgatesAplics);
						}
					}
				});

	},

	updateForm : function(config, show) {
		this.params = {
			dateBegin : FDESK.AS.dataReferencia.format('Y-m-d'),
			dateEnd : FDESK.AS.dataReferencia.format('Y-m-d'),
			tables : 'CotaInvestimento',
			popup : '1',
			date : FDESK.AS.dataReferencia.format('Y-m-d')
		};
		FDESK.win.CotaInvestimento.superclass.updateForm.apply(this, [config,
						show, this.params]);
	},

	afterUpdateForm : function(obj) {
		this.tabPanel.hideTabStripItem(this.panelResgatesAplics);

		if (obj.posicaoFundoSintetica) {
			this.headerTitle.update({
						NomeAtivo : obj.posicaoFundoSintetica.NomeFundo
					});

			this.tableDetalheAtivo.loadData([obj.posicaoFundoSintetica]);
		}

		this.tableSaldosAplics.loadData(obj.posicaoFundoAnalitica);

		FDESK.win.CotaInvestimento.superclass.afterUpdateForm.call(this);
	}
});