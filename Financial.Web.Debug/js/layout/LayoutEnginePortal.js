Ext.namespace('FDESK', 'FDESK.LayoutEngine');

FDESK.LayoutEngine = function() {
	var LayoutEngine = function() {
	};

	Ext.extend(LayoutEngine, Ext.util.Observable, {
				windows : [],
				showUserActions : function(){
				    if(!this.menuUserActions)
				    {
				        var actionTrocaCarteira = new Ext.Action({
                            text: 'Visualizar outra carteira',
                            handler: function(){
                                popupCliente.ShowAtElementByID(this.id);
                            }
                        });

				        var actionAlterarSenha = new Ext.Action({
                            text: 'Alterar informa��es do usu�rio',
                            handler: function(){
                                FDESK.LE.openDocInWin({tipoRegistro: 'Usuario'}, false);
                            }
                        });

				        this.menuUserActions = new Ext.menu.Menu({
				            items: [actionTrocaCarteira, actionAlterarSenha]
				        });
				    
				        this.menuUserActions.elAlignTo = Ext.get('x-info-user');
				    }
				    this.menuUserActions.show(this.menuUserActions.elAlignTo);
				},
				openHelpWin : function(config){
				    FDESK.LE.openDocInWin({tipoRegistro: 'Help', topic:config.topic}, false);
				},
				openDocInWin : function(formParams, maskBody) {
					if (maskBody) {
						Ext.getBody().mask('Carregando', 'x-mask-loading');
					}

					var tr = formParams.tipoRegistro;

					if (!this.windows['win' + tr]) {
						this.windows['win' + tr] = new FDESK.win[tr]({
									tipoRegistro : tr
								});
						if (maskBody) {
							this.windows['win' + tr].on('formready',
									function() {
										Ext.getBody().unmask();
									});
						}
					}

					this.windows['win' + tr].updateForm(formParams, true);
					return this['win' + tr];
				},

				loadTabTables : function(tableTabNames, params) {
				
					params = params || {};
					params.date = FDESK.AS.dataReferencia.format('Y-m-d');
					params.idIndice = FDESK.AS.idIndice;

					// Se tableTabNames nao especificado, carrega todas as
					// tables dos tabs
					var tables;
					
					if(FDESK.AppSettings.g_ShowCarteiraOnline === true){
                        tables = {
						    carteiraOnline : ['CarteiraOnline']
					    };
					}else{
					    tables = {
						    posicaoCarteira : ['CarteiraSintetica'],
						    indicadores : ['QuadroRetorno', 'Estatistica1',
								'Estatistica2', 'Indicadores', 'RetornoPeriodo']
					    };
					}

					var tablesToLoad = [], tabsToLoad = [];
					for (var tableTabName in tables) {
						if (!tableTabNames
								|| tableTabNames.indexOf(tableTabName) !== -1) {
							tablesToLoad = tablesToLoad
									.concat(tables[tableTabName]);
							tabsToLoad.push(tableTabName);
						}
					}

					params.tables = tablesToLoad.join('|');
					params.idCliente = FDESK.AS.user.idCliente;

					Ext.getBody().mask('Carregando...',
							'ext-el-mask-msg x-mask-loading');
							
					Ext.Ajax.request({
								scope : this,
								method : 'GET',
								params : params,
								url : FDESK.AS.g_WebDbName + '/ashx/Table.ashx',
								failure : function(response) {
									var obj = Ext.decode(response.responseText);
									FDESK.AS.errHandler(obj && obj.erros);
								},
								success : function(response) {
									var data = Ext
											.decode(response.responseText);
									for (var i = 0; i < tabsToLoad.length; i++) {
										var eventName = String.format(
												'tab{0}tablesloaded',
												tabsToLoad[i].toLowerCase());
										this.publishEvent(eventName, data);
									}
									Ext.getBody().unmask();
								}
							});
				},

				init : function() {

					Ext.BLANK_IMAGE_URL = FDESK.AS.g_PathExt
							+ "/resources/images/default/s.gif";
					Ext.SSL_SECURE_URL = FDESK.AS.g_PathExt
							+ "/resources/images/default/s.gif";

					Ext.QuickTips.init();

					var headerPanelHeight = 65;

					var panelZoomCards = {
						xtype : 'zoomcards'
					};

					var centerPanel = {
						region : 'center',
						items : [panelZoomCards]
					};

					var headerPanel = {
						region : "north",
						ref : 'headerPanel',
						height : headerPanelHeight,
						header : false,
						bodyBorder : false,
						border : false,
						cls : 'header-panel',
						contentEl : 'x-taskbar',
						collapseMode : 'mini',
						collapsible : true,
						listeners : {
							collapse : function() {
								this.publishEvent('viewporttogglecollapse');
							},
							expand : function() {
								this.publishEvent('viewporttogglecollapse');
							},
							afterrender : function() {
								this.subscribeEvent(
										'collapsenorthpanelpressed',
										this.toggleCollapse, this);
							}
						}
					};

					this.viewport = new Ext.Viewport({
								style : 'background: transparent;',
								renderTo : Ext.getBody(),
								layout : 'border',
								items : [centerPanel, headerPanel]
							});

					this.subscribeEvent('dateperiodochanged', function(
									dateParam) {
								this.loadTabTables(null);
							}, this);

					this.subscribeEvent('indicechanged',
							function(idIndice) {
								this.loadTabTables(['indicadores']);
							}, this);
							
					this.subscribeEvent('environmentupdated',
							function() {
								this.loadTabTables(null);
							}, this);		
					
					//Precisamos chamar o loadtables pois o primeiro evento environmentupdated
				    //eh disparado antes da carga deste modulo
				    this.loadTabTables(null);
					

					setTimeout(function() {
								Ext.get('loading-outer').remove();
								Ext.get('loading-mask').remove();
							}, 250);
				}
			});
	return new LayoutEngine();
}();

FDESK.LE = FDESK.LayoutEngine;