<%@ Application Language="C#" %>

<script RunAt="server">

    void Application_Start(object sender, EventArgs e) {
        EntitySpaces.Interfaces.esProviderFactory.Factory = new EntitySpaces.LoaderMT.esDataProviderFactory();
        string arquivo = Financial.WebConfigConfiguration.DiretorioAplicacao.DiretorioBaseAplicacao + "web.Config";
        log4net.Config.XmlConfigurator.Configure(new System.IO.FileInfo(arquivo));

        if (EntitySpaces.Interfaces.esConfigSettings.DefaultConnection.Provider == "EntitySpaces.SqlClientProvider") {
            EntitySpaces.Core.esUtility u = new EntitySpaces.Core.esUtility();
            string sql = " set language english ";
            u.ExecuteNonQuery(EntitySpaces.Interfaces.esQueryType.Text, sql, "");
        }
    }

    void Application_End(object sender, EventArgs e) {
        //  Code that runs on application shutdown

    }

    void Application_Error(object sender, EventArgs e) {
        // Code that runs when an unhandled error occurs
        //Response.Redirect("~/Erro/PrincipalErro.aspx");
    }

    void Session_Start(object sender, EventArgs e) {

        //string path = AppDomain.CurrentDomain.BaseDirectory;
        //path += @"Data\financial.xml";

        //Korzh.EasyQuery.DataModel model = new Korzh.EasyQuery.DataModel();
        //model.UseResourcesForOperators = true;

        //model.LoadFromFile(path);

        //Korzh.EasyQuery.Query query = new Korzh.EasyQuery.Query();
        ////query.Formats.EOL = Korzh.EasyQuery.EOLSymbol.None;
        //query.Model = model;
        ////query.Formats.AlphaAlias = true;
                
        //query.Formats.DateFormat = "yyyy-MM-dd";
        //query.Formats.DateTimeFormat = "yyyy-MM-dd";
        //query.Formats.QuoteBool = false;
        //query.Formats.QuoteTime = true;
        //query.Formats.UseSchema = false;
        //query.Formats.SqlQuote1 = '[';
        //query.Formats.SqlQuote2 = ']';
        //query.Formats.SqlSyntax = Korzh.EasyQuery.SqlSyntax.SQL2;
        //query.Formats.TimeFormat = "";
        ////
        //Session["Query"] = query;
    }

    void Session_End(object sender, EventArgs e) {
        // Code that runs when a session ends. 
        // Note: The Session_End event is raised only when the sessionstate mode
        // is set to InProc in the Web.config file. If session mode is set to StateServer 
        // or SQLServer, the event is not raised.

        //Session["Query"] = null;  
    }
       
</script>