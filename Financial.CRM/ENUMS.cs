﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Financial.CRM.Enums {
    
    public enum TipoPessoa 
    {
        Fisica = 1,
        Juridica = 2
    }

    public enum EstadoCivilPessoa
    {
        NaoAplicavel = 0,
        Casado = 1,
        Solteiro = 2,
        Viuvo = 3,
        Divorciado = 4,
        Outros = 5        
    }

    public class SexoPessoa
    {
        public string Masculino = "M";
        public string Feminino = "F";
    }

    public enum TipoTelefonePessoa {
        Residencial = 1,
        Celular = 2,
        Comercial = 3,
        Outros = 4
    }

    public static class TipoEnderecoPessoa
    {
        public static string Comercial = "C";
        public static string Residencial = "R";
    }

    public static class RecebeCorrespondenciaEnderecoPessoa
    {
        public static string Sim = "S";
        public static string Nao = "N";
    }

    public enum StatusAtivoPessoa
    {
        Sim = 1,
        Não = 2
    }

    public enum TipoDocumentoPessoa
    {
        CedulaIdentidade_CI = 1, //Cédula Identidade (CI)
        CedulaIdentidadeEstrangeiro_CIE = 2, //Cédula Identidade Estrangeiro (CIE)
        CedulaIdentidadeMilitar = 3, //Carteira Militar
        CedulaIdentidadeProfissional = 4,//Identidade Profissional (CIP)
        CedulaIdentidadeProfissionalOrgaoPublico = 5, //Cédula Identidade Profissional (Público)
        CertidaoNascimento = 6, //Certidão Nascimento
        CarteiraHabilitacao = 7, //Carteira Habilitação
        CarteiraTrabalhoProfissional = 8, //Carteira Trabalho
        IdentificaçãoPrisional = 9, //Identificação Prisional
        LaissezPasser = 10, //Laissez Passer
        Passaporte = 11, //Passaporte
        CedulaIdentidade_RG = 12, //Cédula Identidade (RG)
        DocumentoIdentidade = 13, //Documento Identidade (RI)
        CedulaIdentidadeEstrangeiro_RNE = 14, //Cédula Identidade Estrangeiro (RNE)
        InscricaoEstadual = 15, //Inscrição Estadual 
        NIRE = 16 //NIRE
    }

    public enum EmissorDocumentoPessoa
    {
        ABEXCON	= 1,
        AERON	= 2,
        ASLEGIS = 3,
        CBM	    = 4,
        CFP	    = 5,
        CGJ	    = 6,
        CMRJ	= 7,
        CNDL	= 8,
        CNE	    = 9,
        CNEN	= 10,
        CONDEBR = 11,
        CONFEV  = 12,
        CONFIPA = 13,
        CONRE	= 14,
        CONRERP = 15,
        CORE	= 16,
        CORECON = 17,
        COREM	= 18,
        COREN	= 19,
        CRA	    = 20,
        CRAS	= 21,
        CRB	    = 22,
        CRBM	= 23,
        CRC	    = 24,
        CRCPN	= 25,
        CREA	= 26,
        CRECI	= 27,
        CREF	= 28,
        CRESS	= 29,
        CRF	    = 30,
        CRFA	= 31,
        CRFTO	= 32,
        CRM	    = 33,
        CRMV	= 34,
        CRN	    = 35,
        CRO	    = 36,
        CRP	    = 37,
        CRQ	    = 38,
        CRTA	= 39,
        CRTR	= 40,
        CTMTB	= 41,
        DEF	    = 42,
        DEPCT	= 43,
        DESIPE  = 44,
        DESP	= 45,
        DETRAN  = 46,
        DFSP	= 47,
        DGPC	= 48,
        DI	    = 49,
        DPF	    = 50,
        DPGE	= 51,
        DPTC	= 52,
        DSG	    = 53,
        EAPJU	= 54,
        ESG	    = 55,
        EXERCIT = 56,
        FENAJ	= 57,
        FUNAI	= 58,
        GEJ	    = 59,
        GISI	= 60,
        IC	    = 61,
        IFP	    = 62,
        IIDAMP  = 63,
        IIDML	= 64,
        IIEP	= 65,
        IITP	= 66,
        IMLC	= 67,
        INI	    = 68,
        IPF	    = 69,
        IPT	    = 70,
        ITB	    = 71,
        JUIJ	= 72,
        LAISSEZ = 73,
        MARINHA = 74,
        MC	    = 75,
        MCT	    = 76,
        MCU	    = 77,
        MED	    = 78,
        MEFP	= 79,
        MEUF	= 80,
        MF	    = 81,
        MGUERRA = 82,
        MI	    = 83,
        MIC	    = 84,
        MJ	    = 85,
        MPAS	= 86,
        MPDFT	= 87,
        MPEMG	= 88,
        MPEMT	= 89,
        MPEPR	= 90,
        MPERJ	= 91,
        MPESP	= 92,
        MPF	    = 93,
        MPM	    = 94,
        MPT	    = 95,
        MRE	    = 96,
        MS	    = 97,
        MT	    = 98,
        MTRANSP = 99,
        OAB	    = 100,
        OMBCR	= 101,
        P_JURIC = 102,
        PASSPOR = 103,
        PCIID	= 104,
        PCRJ	= 105,
        PDFII	= 106,
        PFF	    = 107,
        PGE	    = 108,
        PGJ	    = 109,
        PIG	    = 110,
        PJEG	= 111,
        PM	    = 112,
        PMP	    = 113,
        RBR	    = 114,
        RFBR	= 115,
        RFF	    = 116,
        SADGP	= 117,
        SAPESS  = 118,
        SC	    = 119,
        SDPPES  = 120,
        SEA	    = 121,
        SECT	= 122,
        SEDPF	= 123,
        SEDS	= 124,
        SEEC	= 125,
        SEF	    = 126,
        SEJSP	= 127,
        SEJU	= 128,
        SEOMA	= 129,
        SEPC	= 130,
        SES	    = 131,
        SESP	= 132,
        SF	    = 133,
        SGPJ	= 134,
        SIC	    = 135,
        SIJ	    = 136,
        SJP	    = 137,
        SJS	    = 138,
        SJSP	= 139,
        SJTC	= 140,
        SPC	    = 141,
        SPCII	= 142,
        SPSP	= 143,
        SPTC	= 144,
        SRF	    = 145,
        SSI	    = 146,
        SSP	    = 147,
        SSPDC	= 148,
        STM	    = 149,
        TC	    = 150,
        TJ	    = 151,
        TRE	    = 152,
        TRF	    = 153,
        TRIBALC = 154,
        TRT	    = 155,
        TST	    = 156,
        SEFAZ   = 157,
        JCDE    = 158,
        JUCAP   = 159,
        JUCEA   = 160,
        JUCEAC  = 161,
        JUCEAL  = 162,
        JUCEB   = 163,
        JUCEC   = 164,
        JUCEES  = 165,
        JUCEG   = 166,
        JUCEMA  = 167,
        JUCEMAT = 168,
        JUCEMG  = 169,
        JUCEMS  = 170,
        JUCEP   = 171,
        JUCEPA  = 172,
        JUCEPAR = 173,
        JUCEPE  = 174,
        JUCEPI  = 175,
        JUCER   = 176,
        JUCERGS = 177,
        JUCERJA = 178,
        JUCERN  = 179,
        JUCERR  = 180,
        JUCESC  = 181,
        JUCESE  = 182,
        JUCESP  = 183,
        JUCETIN	= 184
    }

    public enum SituacaoLegalPessoa
    {
        Espolio = 1,
        Interdito = 2,
        Maior = 3,
        Menor = 4,
        Emancipado = 5
    }

    public enum TipoVinculoPessoa
    {
        Cotitular = 1
    }

    public enum NaturezaCapitalPessoaJuridica
    {
        Credito = 001,
        Debito = 002,
        PE_Federal = 101,
        PE_Estadual_DF = 102,
        PE_Municipal = 103,
        PL_Federal = 104,
        PL_Estadual_DF = 105,
        PL_Municipal = 106,
        PJ_Federal = 107,
        PJ_Estadual_DF = 108,
        Orgao_Autonomo_Direito_Publico = 109,
        Autarquia_Federal = 110,
        Autarquia_Estadual_DF = 111,
        Autarquia_Municipal = 112,
        Fundação_Federal = 113,
        Fundação_Estadual_DF = 114,
        Fundação_Municipal = 115,
        OP_Autonomo_Federal = 116,
        OP_Autonomo_Estadual_DF = 117,
        OP_Autonomo_Municipal = 118,
        Empresa_Publica = 201,
        SA_Fechada_Empresa_Publica = 202,
        Sociedade_de_Economia_Mista = 203,
        SA_Aberta = 204,
        SA_Fechada = 205,
        SA_Limitada = 206,
        SA_emvNome_Coletivo = 207,
        SA_em_Comandita_Simples = 208,
        SA_EM_Comandita_Por_Ações = 209,
        SM_de_Capital_e_Indústria = 210,
        SC_com_Fins_Lucrativos = 211,
        SOC_em_Conta_de_Participação = 212,
        Empresário_Individual = 213,
        Cooperativa = 214,
        Consórcio_de_Sociedades = 215,
        Grupo_de_Sociedades = 216,
        Estab_Brasil_Soc_Estrangeira = 217,
        SA_em_Garantia_Solidária = 218,
        Empr_Binacional_ARG_BRA = 219,
        Entidade_Binacional_Itaipu = 220,
        Empresa_Domiciliada_Exterior = 221,
        Clube_Fundo_de_Investimento = 222,
        SS_Pura = 223,
        SS_Limitada = 224,
        SS_em_Nome_Coletivo = 225,
        SS_em_Comandita_Simples = 226,
        Empresa_Binacional = 227,
        Fundação_Recursos_Privados = 301,
        Associação = 302,
        Cartório = 303,
        Organização_Social = 304,
        OSCIP = 305,
        Fundações_Recursos_Privado = 306,
        Serviço_Social_Autonomo = 307,
        Condomínio_Edifício = 308,
        Unidade_Executora = 309,
        Comissão_de_Conciliação_Prévia = 310,
        Entidade_Mediação_e_Arbitragem = 311,
        Partido_Político = 312,
        Entidade_Sindical = 313,
        Estab_Fundação_ou_Ass_Estr = 320,
        Fundação_Associação_Exterior = 321,
        Associação_Privada = 399,
        Empresa_Individual_Imobiliária = 401,
        Contribuinte_Individual = 408,
        Candidato_Cargo_Polit_Eletivo = 409,
        Organismo_Int_e_Outras_Inst_Extrat = 450,
        Organizacao_Int_e_Outras_Inst_Extrat = 500,
        Comissão_Polinacional = 119,
        Fundo_Público = 120,
        Associação_Pública = 121,
        Consórcio_de_Empregadores = 228,
        Consórcio_Simples = 229,
        Emp_Indiv_Resp_Ltda_Nat_Emp = 230,
        Emp_Indiv_Resp_Ltda_Nat_Sim = 231,
        Organização_Religiosa = 322,
        Comunidade_Indígena = 323,
        Fundo_Privado = 324,
        Organização_Internacional = 501,
        Repr_Diplomática_Estrangeira = 502,
        Outras_Inst_Extraterritoriais = 503
    }
}
