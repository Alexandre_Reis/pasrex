/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 18/09/2015 11:19:04
===============================================================================
*/

using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Text;
using System.Data;

using EntitySpaces.Interfaces;
using EntitySpaces.Core;

namespace Financial.CRM
{
	public partial class PessoaSuitability : esPessoaSuitability
	{
        public bool compare(PessoaSuitability pessoaSuitability)
        {
            bool retorno = (this.Perfil == pessoaSuitability.Perfil && 
                            this.Recusa == pessoaSuitability.Recusa && 
                            this.Dispensado == pessoaSuitability.Dispensado && 
                            this.Dispensa == pessoaSuitability.Dispensa &&
                            this.IdValidacao == pessoaSuitability.IdValidacao);

            return retorno;
        }
	}
}
