﻿using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Text;
using System.Data;

using EntitySpaces.Interfaces;
using EntitySpaces.Core;
using Financial.CRM.Enums;
using Financial.Util;
using Financial.Fundo;
using Financial.Investidor;
using Financial.Investidor.Enums;
using Financial.Common.Enums;
using Financial.Fundo.Enums;
using Financial.InvestidorCotista.Enums;
using Financial.Captacao.Enums;
using Financial.Common;
using Financial.Tributo.Custom;
using Financial.InvestidorCotista;
using Financial.Bolsa;
using Financial.Interfaces.Import.YMF;
using Financial.Interfaces.Sinacor;
using System.Xml;
using System.IO;
using Financial.InvestidorCotista.Perfil;
using System.Xml.Serialization;
using Financial.CRM.IntegracaoCrmService;

namespace Financial.CRM
{
    public partial class Pessoa : esPessoa
    {

        #region Pessoa Endereço
        PessoaEndereco pessoaEndereco;
        //utilizado para manter carregado o primeiro PessoaEndereco desta Pessoa. 
        //Muitas vezes esse endereço é o único, podendo ser reutilizado
        public PessoaEndereco PessoaEndereco
        {
            get
            {
                if (pessoaEndereco == null)
                {
                    pessoaEndereco = PessoaEnderecoCollectionByIdPessoa[0];
                }
                return pessoaEndereco;
            }
            set
            {
                //Nao implementado
            }
        }
        #endregion

        #region PessoaTelefone
        PessoaTelefone pessoaTelefone;

        public PessoaTelefone PessoaTelefone
        {
            get
            {
                if (pessoaTelefone == null)
                {
                    PessoaTelefoneCollection a = this.PessoaTelefoneCollectionByIdPessoa;

                    if (a.Count >= 0)
                    {
                        //
                        a.Sort = PessoaTelefoneMetadata.ColumnNames.TipoTelefone + " ASC";

                        pessoaTelefone = a[0];
                    }
                }
                return pessoaTelefone;
            }
            set
            {
                //Nao implementado
            }
        }
        #endregion

        //#region PessoaEmail
        //PessoaEmail pessoaEmail;

        //public PessoaEmail PessoaEmail {
        //    get {
        //        if (pessoaEmail == null) {

        //            PessoaEmailCollection p = this.PessoaEmailCollectionByIdPessoa;
        //            if (p.HasData) {
        //                pessoaEmail = p[0];
        //            }
        //        }
        //        return pessoaEmail;
        //    }
        //    set {
        //        //Nao implementado
        //    }
        //} 
        //#endregion

        string email = "";
        public string Email
        {
            get
            {
                PessoaEmailCollection a = this.PessoaEmailCollectionByIdPessoa;

                if (a.HasData)
                {
                    email = a[0].str.Email;
                }
                return email;
            }
        }

        string fone; /* Pega primeiro telefone disponivel na ordem Residencial, Celular, Comercial, Outros */
        public string Fone
        {
            get
            {
                if (this.PessoaTelefone == null)
                {
                    fone = null;
                }

                if (this.PessoaTelefone.Numero != null)
                {
                    fone = this.PessoaTelefone.Numero;
                }
                return fone;
            }
            set
            {
                //Nao implementado
            }
        }

        private string dddFone = ""; /* Pega primeiro telefone disponivel na ordem Residencial, Celular, Comercial, Outros */
        public string DDD_Fone
        {
            get
            {
                this.dddFone = "";
                if (this.PessoaTelefone == null)
                {
                    this.dddFone = "";
                    return this.dddFone;
                }

                if (!String.IsNullOrEmpty(this.PessoaTelefone.str.Ddd))
                {
                    if (!this.PessoaTelefone.str.Ddd.Contains("("))
                    {
                        this.dddFone += "(";
                    }

                    this.dddFone += this.PessoaTelefone.str.Ddd;

                    if (!this.PessoaTelefone.str.Ddd.Contains(")"))
                    {
                        this.dddFone += ") ";
                    }
                }


                this.dddFone += this.PessoaTelefone.str.Numero;

                return this.dddFone;
            }
        }

        string foneResidencial = "";
        public string FoneResidencial
        {
            get
            {
                PessoaTelefoneCollection a = this.PessoaTelefoneCollectionByIdPessoa;

                if (a.HasData)
                {
                    a.Filter = "" + PessoaTelefoneMetadata.ColumnNames.TipoTelefone + " = " + (int)TipoTelefonePessoa.Residencial;

                    if (a.Count > 0)
                    {
                        foneResidencial = a[0].str.Numero;
                    }
                }
                return foneResidencial;
            }
        }

        string dddFoneResidencial = "";
        public string DDD_FoneResidencial
        {
            get
            {
                PessoaTelefoneCollection a = this.PessoaTelefoneCollectionByIdPessoa;

                if (a.HasData)
                {
                    a.Filter = "" + PessoaTelefoneMetadata.ColumnNames.TipoTelefone + " = " + (int)TipoTelefonePessoa.Residencial;

                    if (a.Count > 0)
                    {
                        if (!String.IsNullOrEmpty(a[0].str.Ddd))
                        {
                            if (!a[0].str.Ddd.Contains("("))
                            {
                                dddFoneResidencial += "(";
                            }

                            dddFoneResidencial += a[0].str.Ddd;

                            if (!a[0].str.Ddd.Contains(")"))
                            {
                                dddFoneResidencial += ") ";
                            }
                        }
                        dddFoneResidencial += a[0].str.Numero;
                    }
                }
                return dddFoneResidencial;
            }
        }


        string foneComercial = "";
        public string FoneComercial
        {
            get
            {
                PessoaTelefoneCollection a = this.PessoaTelefoneCollectionByIdPessoa;

                if (a.HasData)
                {
                    a.Filter = "" + PessoaTelefoneMetadata.ColumnNames.TipoTelefone + " = " + (int)TipoTelefonePessoa.Comercial;

                    if (a.Count > 0)
                    {
                        foneComercial = a[0].str.Numero;
                    }
                }
                return foneComercial;
            }
        }



        public override string Cpfcnpj
        {
            get
            {
                return base.Tipo == (byte)TipoPessoa.Fisica
                              ? Utilitario.MascaraCPF(base.Cpfcnpj)
                              : Utilitario.MascaraCNPJ(base.Cpfcnpj);
            }
            set
            {
                base.Cpfcnpj = value;
            }
        }

        private bool _integraCrm;
        public void IntegraCrm(bool integraCrm)
        {
            _integraCrm = integraCrm;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns>bool indicando se Pessoa é Fisica</returns>
        public bool IsPessoaFisica()
        {
            return this.Tipo == (byte)TipoPessoa.Fisica;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns>bool indicando se Pessoa é Juridica</returns>
        public bool IsPessoaJuridica()
        {
            return this.Tipo == (byte)TipoPessoa.Juridica;
        }

        /// <summary>
        /// max Id pessoa + 1
        /// </summary>
        /// <returns></returns>
        public int GeraIdPessoa()
        {
            //Rotina para gerar o próximo Id de pessoa disponível
            int idPessoaVago = 1;

            Pessoa pessoa = new Pessoa();
            pessoa.Query.Select(pessoa.Query.IdPessoa.Max());
            if (pessoa.Query.Load())
            {
                if (pessoa.IdPessoa.HasValue)
                {
                    idPessoaVago = pessoa.IdPessoa.Value + 1;
                }
            }
            return idPessoaVago;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="clienteYMF"></param>
        public void ImportaClienteYMF(ClienteYMF clienteYMF)
        {
            if (!this.IdPessoa.HasValue)
            {
                //Estamos inserindo uma nova pessoa
                int idPessoa;

                /*if (int.TryParse(clienteYMF.CdCliente, out idPessoa))
                {
                    //Verificar se o numero nao colide com algum Id ja utilizado no banco
                    Pessoa pessoa = new Pessoa();
                    if (pessoa.LoadByPrimaryKey(idPessoa))
                    {
                        throw new Exception("Código de cliente YMF já se encontra em uso no Financial: " + idPessoa);
                    }
                }
                else
                {*/
                //Estamos importando um cliente YMF com codigo alfanumerico (provavelmente um fundo)
                //Vamos utilizar o primeiro IdPessoa disponivel
                idPessoa = this.GeraIdPessoa();
                //}

                this.IdPessoa = idPessoa;
                this.CodigoInterface = clienteYMF.CdCliente.Trim();

            }
            else
            {
                //Estamos fazendo update numa pessoa já inserida
            }

            this.Tipo = this.MapIcFjPessoaYMF(clienteYMF.IcFjPessoa);
            this.EstadoCivil = this.MapIdEstadoCivil(clienteYMF.IdEstadoCivil);
            this.Nome = clienteYMF.NmCliente;
            //this.Apelido = clienteYMF.NmAbreviado;
            this.Apelido = clienteYMF.NmCliente.Length > 40 ? clienteYMF.NmCliente.Substring(0, 40) : clienteYMF.NmCliente;
            this.Cpfcnpj = this.IsPessoaFisica() ? clienteYMF.NoCpf.ToString() : clienteYMF.NoCgc.ToString();
            this.Sexo = clienteYMF.IcMfSexo;
            this.DataNascimento = this.IsPessoaFisica() ? clienteYMF.DtNascimento : clienteYMF.DtConstituicao;
            this.Profissao = clienteYMF.DsProfissao;

            this.DataCadatro = clienteYMF.DtInclusao.HasValue ? clienteYMF.DtInclusao : System.DateTime.Now;
            this.DataUltimaAlteracao = clienteYMF.DtAlteracao.HasValue ? clienteYMF.DtAlteracao : this.DataCadatro;
        }

        /// <summary>
        /// De Acordo com os Codigos Sinacor
        /// Insere nova pessoa se não Existir Ou Atualiza se já existir
        /// 
        /// Deleta e Insere os endereços em PessoaEndereco
        /// Atualiza os Emails em PessoaEndereco
        /// </summary>
        /// <exception cref="Exception">Se codigo sinacor não achado ou cpf não encontrado na base sinacor</exception>
        public void ImportaPessoasSinacor(List<int> codigosSinacor)
        {

            PessoaCollection pessoaCollection = new PessoaCollection();
            PessoaDocumentoCollection pessoaDocumentoCollection = new PessoaDocumentoCollection();
            for (int i = 0; i < codigosSinacor.Count; i++)
            {

                #region TSCCLIBOL
                TscclibolCollection tscclibolCollection = new TscclibolCollection();

                // Throws Exception
                tscclibolCollection.GetTscclibol(codigosSinacor[i]);

                decimal cpfCGC = tscclibolCollection[0].CdCpfcgc.Value;
                #endregion

                #region TSCCLIGER

                TsccligerCollection collCliGer = new TsccligerCollection();
                TsccligerCollection.InfoPessoaSinacor info;
                try
                {
                    info = collCliGer.RetornaInfoPessoa(cpfCGC);
                }
                catch (Exception e1)
                {
                    throw new Exception(e1.Message + codigosSinacor[i].ToString());
                }
                #endregion

                Pessoa pessoa = new Pessoa();
                if (!pessoa.LoadByPrimaryKey(codigosSinacor[i]))
                { // Insert
                    pessoa = new Pessoa();
                }

                #region Update/Insert
                pessoa.IdPessoa = codigosSinacor[i];
                pessoa.Nome = info.Nome.Trim();

                #region Apelido
                string apelido = info.Nome;
                if (apelido.Length > 40)
                {
                    apelido = apelido.Substring(0, 40);
                }

                if (string.IsNullOrEmpty(apelido.Trim()))
                {
                    int maxlength = (int)PessoaMetadata.Meta().Columns.FindByColumnName(PessoaMetadata.ColumnNames.Apelido).CharacterMaxLength;
                    pessoa.Apelido = (pessoa.Nome.Length <= maxlength) ? pessoa.Nome : pessoa.Nome.Substring(0, maxlength - 1);
                }
                else
                {
                    pessoa.Apelido = apelido;
                }

                #endregion

                pessoa.Tipo = info.Tipo == "F" ? (byte)TipoPessoa.Fisica : (byte)TipoPessoa.Juridica;
                pessoa.DataCadatro = DateTime.Now;
                pessoa.DataUltimaAlteracao = DateTime.Now;

                #region CNPJ
                int lengthCnpj = pessoa.Tipo == (byte)TipoPessoa.Fisica ? 11 : 14;
                string cnpjNumOnly = string.Join(null, System.Text.RegularExpressions.Regex.Split(cpfCGC.ToString(), "[^\\d]")).PadLeft(lengthCnpj, '0');
                pessoa.Cpfcnpj = cnpjNumOnly;
                #endregion

                pessoa.DataNascimento = info.DataNascimentoFundo;
                pessoa.PessoaPoliticamenteExposta = info.InPoliticoExp;

                #region TscclicompCollection
                TscclicompCollection tscclicompCol = new TscclicompCollection();
                tscclicompCol.es.Connection.Name = "Sinacor";
                tscclicompCol.es.Connection.Schema = ParametrosConfiguracaoSistema.Integracoes.SchemaSinacor;
                //
                tscclicompCol.Query.Where(tscclicompCol.Query.CdCpfcgc == cpfCGC);
                tscclicompCol.Query.Load();
                #endregion

                if (tscclicompCol.Count > 0)
                {
                    pessoa.FiliacaoNomeMae = "";
                    pessoa.FiliacaoNomePai = "";
                    pessoa.UFNaturalidade = "";
                    if (!String.IsNullOrEmpty(tscclicompCol[0].NmMae))
                    {
                        pessoa.FiliacaoNomeMae = StringExt.Truncate(tscclicompCol[0].NmMae, 100);
                    }
                    if (!String.IsNullOrEmpty(tscclicompCol[0].NmPai))
                    {
                        pessoa.FiliacaoNomePai = StringExt.Truncate(tscclicompCol[0].NmPai, 100);
                    }
                    if (!String.IsNullOrEmpty(tscclicompCol[0].SgEstadoNasc))
                    {
                        pessoa.UFNaturalidade = tscclicompCol[0].SgEstadoNasc;
                    }

                    #region PessoaDocumento
                    if (!String.IsNullOrEmpty(tscclicompCol[0].CdDocIdent) &&
                        tscclicompCol[0].DtDocIdent.HasValue &&
                        !String.IsNullOrEmpty(tscclicompCol[0].CdOrgEmit))
                    {
                        // Faz o Save da pessoaDocumento
                        PessoaDocumento p1 = pessoaDocumentoCollection.AddNew();
                        p1.IdPessoa = this.IdPessoa;
                        p1.NumeroDocumento = StringExt.Truncate(tscclicompCol[0].CdDocIdent, 13);
                        p1.TipoDocumento = (byte)TipoDocumentoPessoa.CedulaIdentidade_RG;
                        p1.DataExpedicao = tscclicompCol[0].DtDocIdent;
                        p1.OrgaoEmissor = StringExt.Truncate(tscclicompCol[0].CdOrgEmit, 7);
                    }
                    //                    
                    #endregion
                }

                pessoaCollection.AttachEntity(pessoa);

                #endregion
            }

            using (esTransactionScope scope = new esTransactionScope())
            {

                pessoaCollection.Save();
                //
                pessoaDocumentoCollection.Save(); // Salva pessoa Documento
                //
                PessoaEnderecoCollection pe = new PessoaEnderecoCollection();

                // Importa Endereco da Pessoa
                pe.ImportaEnderecoSinacor(codigosSinacor);

                // Importa Email da Pessoa
                pe.ImportaEmailSinacor(codigosSinacor);

                //
                scope.Complete();
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="idEstadoCivil"></param>
        /// <returns></returns>
        private byte? MapIdEstadoCivil(int idEstadoCivil)
        {
            byte? estadoCivil;
            if (idEstadoCivil == Financial.Interfaces.Import.YMF.Enums.IdEstadoCivil.NULL)
            {
                estadoCivil = null;
            }
            else if (idEstadoCivil == Financial.Interfaces.Import.YMF.Enums.IdEstadoCivil.CASADO)
            {
                estadoCivil = (byte)EstadoCivilPessoa.Casado;
            }
            else if (idEstadoCivil == Financial.Interfaces.Import.YMF.Enums.IdEstadoCivil.DIVORCIADO)
            {
                estadoCivil = (byte)EstadoCivilPessoa.Divorciado;
            }
            else if (idEstadoCivil == Financial.Interfaces.Import.YMF.Enums.IdEstadoCivil.OUTROS)
            {
                estadoCivil = (byte)EstadoCivilPessoa.Outros;
            }
            else if (idEstadoCivil == Financial.Interfaces.Import.YMF.Enums.IdEstadoCivil.SOLTEIRO)
            {
                estadoCivil = (byte)EstadoCivilPessoa.Solteiro;
            }
            else if (idEstadoCivil == Financial.Interfaces.Import.YMF.Enums.IdEstadoCivil.VIUVO)
            {
                estadoCivil = (byte)EstadoCivilPessoa.Viuvo;
            }
            else
            {
                throw new Exception("Tipo de Estado Civil não suportado");
            }

            return estadoCivil;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="icFjPessoa"></param>
        /// <returns></returns>
        private byte MapIcFjPessoaYMF(string icFjPessoa)
        {
            byte tipo;
            if (icFjPessoa == Financial.Interfaces.Import.YMF.Enums.IcFjPessoa.FISICA)
            {
                tipo = (byte)TipoPessoa.Fisica;
            }
            else if (icFjPessoa == Financial.Interfaces.Import.YMF.Enums.IcFjPessoa.JURIDICA)
            {
                tipo = (byte)TipoPessoa.Juridica;
            }
            else
            {
                throw new Exception("Tipo de Pessoa não suportado");
            }

            return tipo;
        }

        #region Interface com CRM

        private string GeraCrmXml(Cotista cotista)
        {
            Financial.CRM.InterfaceCrm.cliente clienteCrm = new Financial.CRM.InterfaceCrm.cliente();

            foreach (PessoaEndereco endereco in PessoaEnderecoCollectionByIdPessoa)
            {
                if (endereco.TipoEndereco == "R" && string.IsNullOrEmpty(clienteCrm.endereco))
                {
                    clienteCrm.bairro = endereco.Bairro;
                    clienteCrm.cep = endereco.Cep;
                    clienteCrm.cidade = endereco.Cidade;
                    clienteCrm.complemento = endereco.Complemento;
                    clienteCrm.endereco = endereco.Endereco;
                    clienteCrm.numero = endereco.Numero;
                    clienteCrm.pais = endereco.Pais;

                }
                if (endereco.TipoEndereco == "C" && string.IsNullOrEmpty(clienteCrm.enderecoCom))
                {
                    clienteCrm.bairroCom = endereco.Bairro;
                    clienteCrm.cepCom = endereco.Cep;
                    clienteCrm.cidadeCom = endereco.Cidade;
                    clienteCrm.complementoCom = endereco.Complemento;
                    clienteCrm.enderecoCom = endereco.Endereco;
                    clienteCrm.numeroCom = endereco.Numero;
                    clienteCrm.ufCom = endereco.Uf;
                    clienteCrm.paisCom = endereco.Pais;
                }
            }
            foreach (PessoaTelefone telefone in PessoaTelefoneCollectionByIdPessoa)
            {
                if (telefone.TipoTelefone == (byte)TipoTelefonePessoa.Residencial && string.IsNullOrEmpty(clienteCrm.foneNumero))
                {
                    clienteCrm.foneDDD = telefone.Ddd;
                    clienteCrm.foneNumero = telefone.Numero;
                    clienteCrm.foneRamal = telefone.Ramal;
                }
                if (telefone.TipoTelefone == (byte)TipoTelefonePessoa.Comercial && string.IsNullOrEmpty(clienteCrm.foneNumeroCom))
                {
                    clienteCrm.foneDDDCom = telefone.Ddd;
                    clienteCrm.foneNumeroCom = telefone.Numero;
                    clienteCrm.foneRamalCom = telefone.Ramal;
                }
            }

            foreach (PessoaEmail email in PessoaEmailCollectionByIdPessoa)
            {
                if (!string.IsNullOrEmpty(email.Email))
                {
                    clienteCrm.email = this.Email;
                    break;
                }
            }

            foreach (PessoaDocumento documento in PessoaDocumentoCollectionByIdPessoa)
            {
                if (documento.TipoDocumento == (byte)TipoDocumentoPessoa.DocumentoIdentidade && string.IsNullOrEmpty(clienteCrm.numeroRG))
                {
                    clienteCrm.emissorRG = documento.OrgaoEmissor;
                    clienteCrm.numeroRG = documento.NumeroDocumento;
                    clienteCrm.emissorUF = documento.UFEmissor;
                    if (documento.DataExpedicao != null)
                    {
                        clienteCrm.dataEmissaoRG = documento.DataExpedicao.Value.ToString("dd/MM/yyyy");
                    }
                }
            }

            ContaCorrenteCollection contaCorrenteCollection = new ContaCorrenteCollection();
            ContaCorrenteQuery contaCorrenteQuery = new ContaCorrenteQuery("C");
            AgenciaQuery agenciaQuery = new AgenciaQuery("A");
            BancoQuery bancoQuery = new BancoQuery("B");

            contaCorrenteQuery.Select(contaCorrenteQuery.Numero,
                contaCorrenteQuery.DigitoConta,
                agenciaQuery.Codigo.As("Agencia"),
                agenciaQuery.DigitoAgencia.As("DigitoAgencia"),
                bancoQuery.CodigoCompensacao.As("Banco"),
                bancoQuery.CodigoExterno.As("CodigoExterno"));
            contaCorrenteQuery.InnerJoin(agenciaQuery).On(agenciaQuery.IdAgencia == contaCorrenteQuery.IdAgencia);
            contaCorrenteQuery.InnerJoin(bancoQuery).On(bancoQuery.IdBanco == contaCorrenteQuery.IdBanco);
            contaCorrenteQuery.Where(contaCorrenteQuery.IdPessoa.Equal(this.IdPessoa.Value));

            contaCorrenteCollection.Load(contaCorrenteQuery);

            List<Financial.CRM.InterfaceCrm.clienteContaBancaria> contas = new List<Financial.CRM.InterfaceCrm.clienteContaBancaria>();

            for (int i = 0; i < contaCorrenteCollection.Count; i++)
            {
                Financial.CRM.InterfaceCrm.clienteContaBancaria conta = new Financial.CRM.InterfaceCrm.clienteContaBancaria();
                conta.codAgencia = contaCorrenteCollection[i].GetColumn("Agencia").ToString();
                conta.digitoAgencia = contaCorrenteCollection[i].GetColumn("DigitoAgencia").ToString();
                conta.codExterno = contaCorrenteCollection[i].GetColumn("CodigoExterno").ToString();
                conta.Conta = contaCorrenteCollection[i].Numero;
                conta.digitoConta = contaCorrenteCollection[i].DigitoConta;
                conta.status = "ATIVO";
                contas.Add(conta);
            }

            //if (contas.Count > 0)
            //{
            //    clienteCrm.contaBancaria = contas.ToArray();
            //}

            clienteCrm.codigoCotista = cotista.IdCotista.Value;
            clienteCrm.sistema = "BRITECH";
            clienteCrm.codigoCotistaSpecified = true;
            //clienteCrm.empresaTrabalha = this.

            clienteCrm.estadoCivil = this.EstadoCivil.ToString();

            clienteCrm.nomeMae = this.FiliacaoNomeMae;
            clienteCrm.nomePai = this.FiliacaoNomePai;

            clienteCrm.politicamenteExposta = this.PessoaPoliticamenteExposta;
            clienteCrm.pessoaVinculada = this.PessoaVinculada;
            clienteCrm.profissao = this.Profissao;
            clienteCrm.sexo = this.Sexo;
            clienteCrm.cpfCnpj = this.Cpfcnpj;
            clienteCrm.nome = this.Nome;
            clienteCrm.tipoAtivo = cotista.IsAtivo.ToString();
            clienteCrm.isentoIOF = cotista.IsentoIOF;
            clienteCrm.isentoIR = cotista.IsentoIR;
            clienteCrm.tipoRenda = this.TipoRenda;
            clienteCrm.situacaoLegal = Convert.ToInt32(this.SituacaoLegal);
            //clienteCrm.valorPatrimonio = 
            //clienteCrm.valorRendaFamiliar = 

            if (this.DataNascimento != null)
            {
                clienteCrm.dataNascimento = this.DataNascimento.Value.ToString("dd/MM/yyyy");
            }
            //clienteCrm.contaBancaria
            //    clienteCrm.codExterno
            //    clienteCrm.codAgencia
            //    clienteCrm.digitoAgencia
            //    clienteCrm.digitoConta
            //    clienteCrm.numeroConta

            XmlSerializer ser = new XmlSerializer(clienteCrm.GetType());
            string result = string.Empty;

            using (MemoryStream memStm = new MemoryStream())
            {
                ser.Serialize(memStm, clienteCrm);

                memStm.Position = 0;
                result = new StreamReader(memStm).ReadToEnd();
            }

            return result;
        }

        public void IntegraCrm(string url, string email, string senha)
        {
            Cotista cotista = new Cotista();
            if (!cotista.LoadByPrimaryKey(this.IdPessoa.Value)) {
                return;
            }

            try {
                loginVO login = new loginVO();
                login.email = email;
                login.password = senha;

                PessoaSuitability pessoaSuitability = new PessoaSuitability();
                DateTime dataDeadLine = new DateTime();
                String descricao = null;
                string perfil = null;

                // dados suitability
                if (pessoaSuitability.LoadByPrimaryKey(this.IdPessoa.Value)) {
                    ControllerPerfilCotista controllerPerfilCotista = new ControllerPerfilCotista();
                    if (pessoaSuitability.UltimaAlteracao != null) {
                        dataDeadLine = controllerPerfilCotista.retornaValidade(pessoaSuitability.UltimaAlteracao.Value).Date;
                    }
                    if (pessoaSuitability != null && pessoaSuitability.Perfil != null) {
                        SuitabilityPerfilInvestidor suitabilityPerfilInvestidor = new SuitabilityPerfilInvestidor();
                        if (suitabilityPerfilInvestidor.LoadByPrimaryKey(pessoaSuitability.Perfil.Value)) {
                            descricao = suitabilityPerfilInvestidor.Perfil;
                        }
                    }
                }

                // dados alerta 
                List<emailVO> emailVoList = new List<emailVO>();
                foreach (PessoaEmail var in PessoaEmailCollectionByIdPessoa) {
                    emailVO emailvo = new emailVO();
                    emailvo.alerta = this.AlertaCadastro == "S" ? true : false;
                    emailvo.email = var.Email;
                    emailVoList.Add(emailvo);
                }

                // dados documento
                List<string> documentoList = new List<string>();
                foreach (PessoaDocumento pessoaDocumento in PessoaDocumentoCollectionByIdPessoa) {
                    string tpDocIde = "";
                    if (pessoaDocumento.TipoDocumento == (byte)TipoDocumentoPessoa.CedulaIdentidade_CI) {
                        tpDocIde = "CI";
                    }
                    else if (pessoaDocumento.TipoDocumento == (byte)TipoDocumentoPessoa.CedulaIdentidadeEstrangeiro_CIE) {
                        tpDocIde = "CIE";
                    }
                    else if (pessoaDocumento.TipoDocumento == (byte)TipoDocumentoPessoa.CedulaIdentidadeMilitar) {
                        tpDocIde = "CIM";
                    }
                    else if (pessoaDocumento.TipoDocumento == (byte)TipoDocumentoPessoa.CedulaIdentidadeProfissional) {
                        tpDocIde = "CIP";
                    }
                    else if (pessoaDocumento.TipoDocumento == (byte)TipoDocumentoPessoa.CedulaIdentidadeProfissionalOrgaoPublico) {
                        tpDocIde = "CIPF";
                    }
                    else if (pessoaDocumento.TipoDocumento == (byte)TipoDocumentoPessoa.CertidaoNascimento) {
                        tpDocIde = "CN";
                    }
                    else if (pessoaDocumento.TipoDocumento == (byte)TipoDocumentoPessoa.CarteiraHabilitacao) {
                        tpDocIde = "CNH";
                    }
                    else if (pessoaDocumento.TipoDocumento == (byte)TipoDocumentoPessoa.CarteiraTrabalhoProfissional) {
                        tpDocIde = "CTPS";
                    }

                    else if (pessoaDocumento.TipoDocumento == (byte)TipoDocumentoPessoa.LaissezPasser) {
                        tpDocIde = "LPASS";
                    }
                    else if (pessoaDocumento.TipoDocumento == (byte)TipoDocumentoPessoa.Passaporte) {
                        tpDocIde = "PASSP";
                    }
                    else if (pessoaDocumento.TipoDocumento == (byte)TipoDocumentoPessoa.CedulaIdentidade_RG) {
                        tpDocIde = "RG";
                    }
                    else if (pessoaDocumento.TipoDocumento == (byte)TipoDocumentoPessoa.DocumentoIdentidade) {
                        tpDocIde = "RI";
                    }
                    else if (pessoaDocumento.TipoDocumento == (byte)TipoDocumentoPessoa.CedulaIdentidadeEstrangeiro_RNE) {
                        tpDocIde = "RNE";
                    }

                    documentoList.Add(string.Format("{0};{1}", tpDocIde, pessoaDocumento.DataVencimento == null ? "" : pessoaDocumento.DataVencimento.Value.ToString("dd/MM/yyyy")));
                }

                string requestAtualizaCrm = GeraCrmXml(cotista);

                string msg = string.Empty;
				
                using (ClienteWebService service = new ClienteWebService()) {
                    service.Url = url;
                    if (this.DataVencimentoCadastro != null) {
                        wsResponse response = service.alertaVencimentoCadastro(login, cotista.IdCotista.Value.ToString(), cotista.IdCotista.Value.ToString(), this.Nome, emailVoList.ToArray(), this.DataVencimentoCadastro == null ? null : this.DataVencimentoCadastro.Value.ToString("dd/MM/yyyy"));
                        if (response.codigo == "-1") {
                            msg = string.Format("Alerta Vencimento Cadastro:\n {0}\n\n", response.resposta);
                        }
                    }

                    foreach (string documento in documentoList) {
                        string[] doc = documento.Split(';');
                        if (!string.IsNullOrEmpty(doc[0]) && !string.IsNullOrEmpty(doc[1])) {
                            wsResponse response2 = service.alertaVencimentoDocumento(login, cotista.IdCotista.Value.ToString(), emailVoList.ToArray(), doc[0], doc[1]);
                            if (response2.codigo == "-1") {
                                msg += string.Format("Alerta Vencimento Documento:\n {0}\n\n", response2.resposta);
                                break;
                            }
                        }
                    }

                    wsResponse response3 = service.atualizacaoCRM(requestAtualizaCrm);
                    if (response3.codigo == "-1") {
                        msg += string.Format("Atualizacao CRM:\n {0}\n\n", response3.resposta);
                    }

                    //if (!string.IsNullOrEmpty(descricao)) {
                    //    wsResponse response4 = service.revalidacaoPerfilRisco(login, dataDeadLine.ToString("dd/MM/yyyy"), cotista.IdCotista.Value.ToString(), descricao, "documentoUltimoPerfil", emailVoList.ToArray());
                    if (!string.IsNullOrEmpty(descricao))
                    {
                        CarteiraQuery carteiraQuery = new CarteiraQuery("c");
                        SuitabilityPerfilInvestidorQuery suitabilityPerfilInvestidorQuery = new SuitabilityPerfilInvestidorQuery("p");

                        string perfilRisco = string.Empty;

                        carteiraQuery.Select(carteiraQuery.PerfilRisco,
                                             suitabilityPerfilInvestidorQuery.Perfil.As("perfilRisco"));
                        carteiraQuery.InnerJoin(suitabilityPerfilInvestidorQuery).On(carteiraQuery.PerfilRisco == suitabilityPerfilInvestidorQuery.IdPerfilInvestidor);
                        carteiraQuery.Where(carteiraQuery.IdCarteira == this.IdPessoa.Value);
                        CarteiraCollection coll = new CarteiraCollection();
                        if (coll.Load(carteiraQuery) && coll.Count > 0)
                        {
                            perfilRisco = coll[0].GetColumn("perfilRisco").ToString();
                        }

                        wsResponse response4 = service.revalidacaoPerfilRisco(login,
                                                                                dataDeadLine.ToString("dd/MM/yyyy"),
                                                                                cotista.IdCotista.Value.ToString(),
                                                                                descricao,
                                                                                "documentoUltimoPerfil",
                                                                                emailVoList.ToArray(),
                                                                                this.IdPessoa.Value.ToString(),
                                                                                perfilRisco);
                        if (response4.codigo == "-1")
                        {
                            msg += string.Format("revalidacaoPerfilRisco:\n {0}\n\n", response4.resposta);
                        }
                    }

                    PosicaoCotistaQuery pQuery = new PosicaoCotistaQuery("P");
                    pQuery.Select(pQuery.IdCarteira);
                    pQuery.Where(pQuery.IdCotista.Equal(cotista.IdCotista.Value));
                    pQuery.es.Distinct = true;

                    PosicaoCotistaCollection posicaoCotistaCollection = new PosicaoCotistaCollection();
                    if (posicaoCotistaCollection.Load(pQuery))
                    {
                        CarteiraQuery carteiraQuery = new CarteiraQuery("c");
                        SuitabilityPerfilInvestidorQuery suitabilityPerfilInvestidorQuery = new SuitabilityPerfilInvestidorQuery("p");

                        foreach (PosicaoCotista posicaoCotista in posicaoCotistaCollection)
                        {
                            string perfilRisco = string.Empty;

                            carteiraQuery.Select(carteiraQuery.PerfilRisco,
                                                 suitabilityPerfilInvestidorQuery.Perfil.As("pRisco"));
                            carteiraQuery.InnerJoin(suitabilityPerfilInvestidorQuery).On(carteiraQuery.PerfilRisco == suitabilityPerfilInvestidorQuery.IdPerfilInvestidor);
                            carteiraQuery.Where(carteiraQuery.IdCarteira == posicaoCotista.IdCarteira.Value);
                            CarteiraCollection coll = new CarteiraCollection();
                            if (coll.Load(carteiraQuery) && coll.Count > 0)
                            {
                                perfilRisco = coll[0].GetColumn("pRisco").ToString();
                            }

                            wsResponse response5 =  service.integrarPerfilRiscoFundo(login,
                                                                                     cotista.IdCotista.Value.ToString(),
                                                                                     posicaoCotista.IdCarteira.Value.ToString(),
                                                                                     perfilRisco);

                            if (response5.codigo == "-1")
                            {
                                msg += string.Format("integrarPerfilRiscoFundo:\n {0}\n\n", response5.resposta);
                            }
                        }

                        if (!string.IsNullOrEmpty(msg))
                        {
                            throw new Exception(msg);
                        }
                    }
                }
            }
            catch (Exception ex) {
                throw new Exception("Ocorreu um na integraçăo com o CRM:\n" + ex.Message);
            }
        }

        #endregion
    }
}
