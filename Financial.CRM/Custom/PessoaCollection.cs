﻿/*
===============================================================================
                     EntitySpaces(TM) by EntitySpaces, LLC
                 A New 2.0 Architecture for the .NET Framework
                          http://www.entityspaces.net
===============================================================================
                       EntitySpaces Version # 1.5.0.0
                       MyGeneration Version # 1.1.5.1
                           22/2/2007 17:48:49
-------------------------------------------------------------------------------
*/


using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Text;
using System.Data;

using EntitySpaces.Interfaces;
using EntitySpaces.Core;
using Financial.Fundo;
using Financial.CRM.Enums;
using Financial.Util;

namespace Financial.CRM
{
	public partial class PessoaCollection : esPessoaCollection
	{
        public Pessoa BuscaPessoaPorCodigoInterface(string codigoInterface)
        {
            Pessoa pessoa = null;

            this.QueryReset();
            this.Query.Where(this.Query.CodigoInterface == codigoInterface);
            if (this.Load(this.Query))
            {
                pessoa = this[0];
            }

            return pessoa;

        }

        public Pessoa BuscaPessoaPorCPFCNPJ(string cpfcnpj)
        {
            Pessoa pessoa = null;

            this.QueryReset();
            this.Query.Where(this.Query.Cpfcnpj == cpfcnpj);
            if (this.Load(this.Query))
            {
                pessoa = this[0];
            }

            return pessoa;

        }

        public PessoaCollection BuscaPessoasPorCPFCNPJ(string cpfcnpj)
        {
            PessoaCollection pessoas = null;

            cpfcnpj = Utilitario.RemoveCaracteresEspeciais(cpfcnpj);

            this.QueryReset();
            this.Query.Where(this.Query.Cpfcnpj == cpfcnpj);
            if (this.Load(this.Query))
            {
                pessoas = this;
            }

            return pessoas;

        }

        public void ImportaClienteYMF(Financial.Interfaces.Import.YMF.ClienteYMF[] clientesYMF)
        {
            /*
            PessoaCollection pessoaCollectionDeletar = new PessoaCollection();
            CarteiraCollection carteiraCollectionDeletar = new CarteiraCollection();
            PessoaCollection pessoaRepository = new PessoaCollection();
            foreach (Financial.Interfaces.Import.YMF.ClienteYMF clienteYMF in clientesYMF)
            {
                //Antes de inicar a criacao de novos usuarios precisamos deletar todas as pessoas 
                //presentes no arquivo. Isso é necessário pois estamos usando um código de sequenciamento
                //para os fundos. Caso contrario toda vez que for feita a importacao, novos Ids de fundo serao
                //gerados

                Pessoa pessoa = pessoaRepository.BuscaPessoaPorCodigoInterface(clienteYMF.CdCliente);
                if (pessoa != null)
                {
                    Carteira carteira = new Carteira();
                    if(carteira.LoadByPrimaryKey(pessoa.IdPessoa.Value)){
                        carteiraCollectionDeletar.AttachEntity(carteira);
                    }
                    pessoaCollectionDeletar.AttachEntity(pessoa);
                }
            }

            //Precisamos salvar collection fora do Scope pois precisamos do banco de dados sem as pessoas
            //que serão importadas
            if (pessoaCollectionDeletar.Count > 0)
            {
                carteiraCollectionDeletar.MarkAllAsDeleted();
                carteiraCollectionDeletar.Save();

                pessoaCollectionDeletar.MarkAllAsDeleted();
                pessoaCollectionDeletar.Save();
            }*/

            foreach (Financial.Interfaces.Import.YMF.ClienteYMF clienteYMF in clientesYMF) {
                Pessoa pessoaExistente = new PessoaCollection().BuscaPessoaPorCodigoInterface(clienteYMF.CdCliente);
                Pessoa pessoa = new Pessoa();
                
                if (pessoaExistente != null) {
                    pessoa.LoadByPrimaryKey(pessoaExistente.IdPessoa.Value);
                }
                else {

                    pessoa.ImportaClienteYMF(clienteYMF);

                    //Nao podemos salvar a collection pois o IdPessoa pode ser sequencial e precisa ser armazenado no banco imediatamente
                    pessoa.Save();

                    #region PessoaDocumento
                    // Faz o Save da pessoaDocumento
                    PessoaDocumento pessoaDocumento = new PessoaDocumento();
                    pessoaDocumento.IdPessoa = pessoa.IdPessoa.Value;
                    pessoaDocumento.NumeroDocumento = clienteYMF.NrRg.Trim();
                    pessoaDocumento.TipoDocumento = (byte)TipoDocumentoPessoa.CedulaIdentidade_RG;
                    pessoaDocumento.DataExpedicao = null;
                    pessoaDocumento.OrgaoEmissor = StringExt.Truncate(clienteYMF.DsRgEmissor, 7);
                    //
                    pessoaDocumento.Save();
                    #endregion
                }

                this.AttachEntity(pessoa);
            }

        }
	}
}
