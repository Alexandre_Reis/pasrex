/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 18/09/2015 11:19:04
===============================================================================
*/

using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Text;
using System.Data;

using EntitySpaces.Interfaces;
using EntitySpaces.Core;

using Financial.Common.Enums;
using Financial.Investidor;

namespace Financial.CRM
{
	public partial class PessoaSuitabilityHistorico : esPessoaSuitabilityHistorico
	{
        /// <summary>
        /// 
        /// </summary>
        /// <param name="pessoaSuitability"></param>
        /// <param name="tipoOperacaoBanco"></param>
        //public static void createPessoaSuitabilityHistorico(PessoaSuitability pessoaSuitability, TipoOperacaoBanco tipoOperacaoBanco)
        //{
        //    PessoaSuitabilityHistorico pessoaSuitabilityHistorico = new PessoaSuitabilityHistorico();
        //    pessoaSuitabilityHistorico.DataHistorico = DateTime.Now;
        //    pessoaSuitabilityHistorico.IdPessoa = pessoaSuitability.IdPessoa;
        //    pessoaSuitabilityHistorico.IdPerfil = pessoaSuitability.Perfil;
        //    pessoaSuitabilityHistorico.Recusa = pessoaSuitability.Recusa;
        //    pessoaSuitabilityHistorico.Dispensado = pessoaSuitability.Dispensado;
        //    pessoaSuitabilityHistorico.IdDispensa = pessoaSuitability.Dispensa;
        //    pessoaSuitabilityHistorico.UltimaAlteracao = pessoaSuitability.UltimaAlteracao;
        //    pessoaSuitabilityHistorico.IdValidacao = pessoaSuitability.IdValidacao;
        //    pessoaSuitabilityHistorico.Tipo = tipoOperacaoBanco.ToString();

        //    if (pessoaSuitability.IdValidacao.HasValue)
        //    {
        //        SuitabilityValidacao suitabilityValidacao = new SuitabilityValidacao();
        //        suitabilityValidacao.LoadByPrimaryKey(pessoaSuitability.IdValidacao.Value);
        //        pessoaSuitabilityHistorico.Validacao = suitabilityValidacao.Descricao;
        //    }

        //    if (pessoaSuitability.Perfil.HasValue)
        //    {
        //        SuitabilityPerfilInvestidor suitabilityPerfilInvestidor = new SuitabilityPerfilInvestidor();
        //        suitabilityPerfilInvestidor.LoadByPrimaryKey(pessoaSuitability.Perfil.Value);
        //        pessoaSuitabilityHistorico.Perfil = suitabilityPerfilInvestidor.Perfil;
        //    }

        //    if (pessoaSuitability.Dispensa.HasValue)
        //    {
        //        SuitabilityTipoDispensa suitabilityTipoDispensa = new SuitabilityTipoDispensa();
        //        suitabilityTipoDispensa.LoadByPrimaryKey(pessoaSuitability.Dispensa.Value);
        //        pessoaSuitabilityHistorico.Dispensa = suitabilityTipoDispensa.Descricao;
        //    }

        //    pessoaSuitabilityHistorico.Save();
        //}

        /// <summary>
        /// Overload com o Usu�rio
        /// </summary>
        /// <param name="pessoaSuitability"></param>
        /// <param name="tipoOperacaoBanco"></param>
        /// <param name="login"></param>
        public static void createPessoaSuitabilityHistorico(PessoaSuitability pessoaSuitability, TipoOperacaoBanco tipoOperacaoBanco, string login) {
            PessoaSuitabilityHistorico pessoaSuitabilityHistorico = new PessoaSuitabilityHistorico();
            pessoaSuitabilityHistorico.DataHistorico = DateTime.Now;
            pessoaSuitabilityHistorico.IdPessoa = pessoaSuitability.IdPessoa;
            pessoaSuitabilityHistorico.IdPerfil = pessoaSuitability.Perfil;
            pessoaSuitabilityHistorico.Recusa = pessoaSuitability.Recusa;
            pessoaSuitabilityHistorico.Dispensado = pessoaSuitability.Dispensado;
            pessoaSuitabilityHistorico.IdDispensa = pessoaSuitability.Dispensa;
            pessoaSuitabilityHistorico.UltimaAlteracao = pessoaSuitability.UltimaAlteracao;
            pessoaSuitabilityHistorico.IdValidacao = pessoaSuitability.IdValidacao;
            pessoaSuitabilityHistorico.Tipo = tipoOperacaoBanco.ToString();
            // Salva o Login
            pessoaSuitabilityHistorico.Usuario = login;

            if (pessoaSuitability.IdValidacao.HasValue) {
                SuitabilityValidacao suitabilityValidacao = new SuitabilityValidacao();
                suitabilityValidacao.LoadByPrimaryKey(pessoaSuitability.IdValidacao.Value);
                pessoaSuitabilityHistorico.Validacao = suitabilityValidacao.Descricao;
            }

            if (pessoaSuitability.Perfil.HasValue) {
                SuitabilityPerfilInvestidor suitabilityPerfilInvestidor = new SuitabilityPerfilInvestidor();
                suitabilityPerfilInvestidor.LoadByPrimaryKey(pessoaSuitability.Perfil.Value);
                pessoaSuitabilityHistorico.Perfil = suitabilityPerfilInvestidor.Perfil;
            }

            if (pessoaSuitability.Dispensa.HasValue) {
                SuitabilityTipoDispensa suitabilityTipoDispensa = new SuitabilityTipoDispensa();
                suitabilityTipoDispensa.LoadByPrimaryKey(pessoaSuitability.Dispensa.Value);
                pessoaSuitabilityHistorico.Dispensa = suitabilityTipoDispensa.Descricao;
            }

            pessoaSuitabilityHistorico.Save();
        }
	}
}
