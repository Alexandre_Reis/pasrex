﻿/*
===============================================================================
                     EntitySpaces(TM) by EntitySpaces, LLC
                 A New 2.0 Architecture for the .NET Framework
                          http://www.entityspaces.net
===============================================================================
                       EntitySpaces Version # 2007.0.0507.0
                       MyGeneration Version # 1.2.0.6
                           16/5/2007 12:12:22
-------------------------------------------------------------------------------
*/


using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Text;
using System.Data;

using EntitySpaces.Interfaces;
using EntitySpaces.Core;

namespace Financial.CRM
{
    public partial class PessoaEndereco : esPessoaEndereco
    {
        private string enderecoCompleto;

        public string EnderecoCompleto
        {
            get
            {
                string enderecoCompleto = "";
                enderecoCompleto += this.str.Endereco + ", " + this.str.Numero + " ";
                enderecoCompleto += this.Complemento + " - " + this.Cidade + " - " + this.Uf;

                return enderecoCompleto;
            }
        }

        public bool ImportaEnderecoYMF(Financial.Interfaces.Import.YMF.EnderecoYMF enderecoYMF) {
            string codigoClienteYMF = enderecoYMF.CdCliente.Trim();
            Pessoa pessoa = new PessoaCollection().BuscaPessoaPorCodigoInterface(codigoClienteYMF);
            if (pessoa == null) {
                throw new Exception("Não foi possível encontrar a pessoa com código YMF: " + codigoClienteYMF);
            }

            this.IdPessoa = pessoa.IdPessoa;
            this.Endereco = String.IsNullOrEmpty(enderecoYMF.DsLogradouro) ? String.Empty : enderecoYMF.DsLogradouro;
            this.Numero = String.Empty;
            this.Bairro = String.IsNullOrEmpty(enderecoYMF.NmBairro) ? String.Empty : enderecoYMF.NmBairro;
            this.Cidade = String.IsNullOrEmpty(enderecoYMF.NmCidade) ? String.Empty : enderecoYMF.NmCidade;
            this.Uf = String.IsNullOrEmpty(enderecoYMF.SgUf) ? String.Empty : enderecoYMF.SgUf;
            this.Cep = String.IsNullOrEmpty(enderecoYMF.NrCep) ? String.Empty : enderecoYMF.NrCep;

            this.Pais = String.IsNullOrEmpty(enderecoYMF.DsPais) ? String.Empty : enderecoYMF.DsPais;

            if (string.IsNullOrEmpty(this.Cep.Trim())) {
                this.Cep = enderecoYMF.DsZipCode;
            }

            this.RecebeCorrespondencia = String.IsNullOrEmpty(enderecoYMF.IcSnComprovado) ? "N" : enderecoYMF.IcSnComprovado;
            //

            string fone = String.IsNullOrEmpty(enderecoYMF.NrTelefone) ? String.Empty : enderecoYMF.NrTelefone;
            string ramal = String.IsNullOrEmpty(enderecoYMF.NrRamal) ? String.Empty : enderecoYMF.NrRamal;            
            string email = String.IsNullOrEmpty(enderecoYMF.DsEmail) ? String.Empty : enderecoYMF.DsEmail;

            if (!String.IsNullOrEmpty(enderecoYMF.NrFax)) {
                fone += " fax: " + enderecoYMF.NrFax;
                fone = fone.Trim();
            }

            if ( String.IsNullOrEmpty(this.Endereco) && String.IsNullOrEmpty(this.Bairro) && String.IsNullOrEmpty(this.Cidade) &&
                 String.IsNullOrEmpty(this.Cep) && String.IsNullOrEmpty(this.Uf) && String.IsNullOrEmpty(this.Pais) &&
                 String.IsNullOrEmpty(fone) && String.IsNullOrEmpty(ramal) &&
                 String.IsNullOrEmpty(email)) {
                return false;
            }
            else {
                return true;
            }
        }
    }
}
