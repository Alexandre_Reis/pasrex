﻿/*
===============================================================================
                     EntitySpaces(TM) by EntitySpaces, LLC
                 A New 2.0 Architecture for the .NET Framework
                          http://www.entityspaces.net
===============================================================================
                       EntitySpaces Version # 2007.0.0507.0
                       MyGeneration Version # 1.2.0.6
                           16/5/2007 12:12:21
-------------------------------------------------------------------------------
*/

using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Text;
using System.Data;

using EntitySpaces.Interfaces;
using EntitySpaces.Core;
using Financial.Interfaces.Sinacor;
using Financial.Util;
using Financial.CRM.Enums;

namespace Financial.CRM {
	public partial class PessoaEnderecoCollection : esPessoaEnderecoCollection {

        public void ImportaEnderecoYMF(Financial.Interfaces.Import.YMF.EnderecoYMF[] enderecosYMF) {

            PessoaTelefoneCollection pessoaTelefoneCollection = new PessoaTelefoneCollection();
            PessoaEmailCollection pessoaEmailCollection = new PessoaEmailCollection();

            List<int> idPessoaEnderecoImportadoList = new List<int>(); 

            foreach (Financial.Interfaces.Import.YMF.EnderecoYMF enderecoYMF in enderecosYMF) {
               
                PessoaEndereco pessoaEndereco = new PessoaEndereco();
                if (pessoaEndereco.ImportaEnderecoYMF(enderecoYMF)) {
                    idPessoaEnderecoImportadoList.Add(pessoaEndereco.IdPessoa.Value);
                    
                    #region PessoaTelefone
                    string fone = String.IsNullOrEmpty(enderecoYMF.NrTelefone) ? String.Empty : enderecoYMF.NrTelefone;
                    if (!String.IsNullOrEmpty(enderecoYMF.NrFax)) {
                        fone += " fax: " + enderecoYMF.NrFax;
                        fone = fone.Trim();
                    }

                    if (!String.IsNullOrEmpty(fone)) {
                        PessoaTelefone pessoaTelefone = pessoaTelefoneCollection.AddNew();
                        //
                        pessoaTelefone.IdPessoa = pessoaEndereco.IdPessoa.Value;
                        pessoaTelefone.Ddi = "";
                        pessoaTelefone.Ddd = "";
                        string ramal = String.IsNullOrEmpty(enderecoYMF.NrRamal) ? String.Empty : enderecoYMF.NrRamal;
                        pessoaTelefone.Ramal = ramal.Length > 5 ? ramal.Substring(0, 5) : ramal;
                        pessoaTelefone.Numero = fone.Length > 20 ? fone.Substring(0,20) : fone;
                        pessoaTelefone.TipoTelefone = (byte)TipoTelefonePessoa.Residencial;
                    }
                    #endregion

                    #region PessoaEmail
                    if (!String.IsNullOrEmpty(enderecoYMF.DsEmail)) {
                        PessoaEmail pessoaEmail = pessoaEmailCollection.AddNew();
                        //
                        pessoaEmail.IdPessoa = pessoaEndereco.IdPessoa.Value;
                        pessoaEmail.Email = String.IsNullOrEmpty(enderecoYMF.DsEmail) ? String.Empty : enderecoYMF.DsEmail;
                    }                    
                    #endregion

                    // PessoaEndereco
                    this.AttachEntity(pessoaEndereco);
                } 
            }

            if (idPessoaEnderecoImportadoList.Count > 0) {
                using (esTransactionScope scope = new esTransactionScope()) {

                    #region Deleta PessoaEndereco por IdPessoa
                    PessoaEnderecoCollection pessoaEnderecoCollectionDeletar = new PessoaEnderecoCollection();

                    pessoaEnderecoCollectionDeletar.Query.Where(pessoaEnderecoCollectionDeletar.Query.IdPessoa.In(idPessoaEnderecoImportadoList));
                    pessoaEnderecoCollectionDeletar.Query.Load();
                    pessoaEnderecoCollectionDeletar.MarkAllAsDeleted();
                    pessoaEnderecoCollectionDeletar.Save();
                    #endregion

                    #region Deleta PessoaTelefone por IdPessoa
                    PessoaTelefoneCollection pessoaTelefoneCollectionDeletar = new PessoaTelefoneCollection();
                    pessoaTelefoneCollectionDeletar.Query.Where(pessoaTelefoneCollectionDeletar.Query.IdPessoa.In(idPessoaEnderecoImportadoList));
                    pessoaTelefoneCollectionDeletar.Query.Load();
                    pessoaTelefoneCollectionDeletar.MarkAllAsDeleted();
                    pessoaTelefoneCollectionDeletar.Save();
                    #endregion

                    #region Deleta PessoaEmail por IdPessoa
                    PessoaEmailCollection pessoaEmailCollectionDeletar = new PessoaEmailCollection();
                    pessoaEmailCollectionDeletar.Query.Where(pessoaEmailCollectionDeletar.Query.IdPessoa.In(idPessoaEnderecoImportadoList));
                    pessoaEmailCollectionDeletar.Query.Load();
                    pessoaEmailCollectionDeletar.MarkAllAsDeleted();
                    pessoaEmailCollectionDeletar.Save();
                    #endregion

                    #region Saves

                    this.Save(); // Salva novas PessoaEnderecos

                    // Salvar Pessoa Telefone
                    pessoaTelefoneCollection.Save();

                    // Salvar Pessoa Email
                    pessoaEmailCollection.Save();

                    #endregion

                    scope.Complete();
                }
            }
        }

        /// <summary>
        /// Insere PessoaEndereços e PessoaTelefones de acordo com os codigosSinacor
        /// Deleta do Financial antes de Inserir
        /// </summary>
        /// <param name="codigosSinacor">Lista de codigos Sinacor para importar Endereços</param>
        /// <returns></returns>
        /// <exception cref="Exception">Se codigo sinacor não achado ou cpf não encontrado na base sinacor</exception>        
        public void ImportaEnderecoSinacor(List<int> codigosSinacor) {

            for (int i = 0; i < codigosSinacor.Count; i++) {
                #region TSCCLIBOL para pegar o cpfCGC
                TscclibolCollection tscclibolCollection = new TscclibolCollection();

                // throws Exception Se codigo Sinacor não achado ou cpf não encontrado
                tscclibolCollection.GetTscclibol(codigosSinacor[i]);

                decimal cpfCGC = tscclibolCollection[0].CdCpfcgc.Value;
                #endregion

                #region TSCENDE para Inclusão de PessoaEndereco
                TscendeCollection t1 = new TscendeCollection();
                t1.es.Connection.Name = "Sinacor";
                t1.es.Connection.Schema = ParametrosConfiguracaoSistema.Integracoes.SchemaSinacor;
                //
                t1.Query.Where(t1.Query.CdCpfcgc == cpfCGC);
                //

                t1.Query.Load();

                //
                // Se tem pelo menos 1 endereço para aquela pessoa no Sinacor
                if (t1.Count >= 1) {
                    #region Deleta do Financial
                    PessoaEnderecoCollection pessoaEnderecoCollection = new PessoaEnderecoCollection();
                    //
                    pessoaEnderecoCollection.Query.Where(pessoaEnderecoCollection.Query.IdPessoa == codigosSinacor[i]);
                    pessoaEnderecoCollection.Query.Load();
                    pessoaEnderecoCollection.MarkAllAsDeleted();

                    PessoaTelefoneCollection pessoaTelefoneCollection = new PessoaTelefoneCollection();
                    //
                    pessoaTelefoneCollection.Query.Where(pessoaTelefoneCollection.Query.IdPessoa == codigosSinacor[i]);
                    pessoaTelefoneCollection.Query.Load();
                    pessoaTelefoneCollection.MarkAllAsDeleted();
                    #endregion

                    #region PessoaEndereco
                    PessoaEnderecoCollection pEnderecoInsere = new PessoaEnderecoCollection();

                    // Para cada Endereço do Sinacor Criar 1 endereço no Financial
                    for (int j = 0; j < t1.Count; j++) {
                        //              
                        PessoaEndereco pessoaEndereco = pEnderecoInsere.AddNew();
                        //

                        pessoaEndereco.IdPessoa = codigosSinacor[i];
                        pessoaEndereco.RecebeCorrespondencia = t1[j].InEndeCorr != null ? t1[j].InEndeCorr.ToUpper() : "N";

                        pessoaEndereco.Endereco = t1[j].NmLogradouro.Trim();
                        pessoaEndereco.Numero = t1[j].NrPredio.Trim();
                        if (t1[j].NmCompEnde != null) {
                            pessoaEndereco.Complemento = t1[j].NmCompEnde.Trim();
                        }

                        pessoaEndereco.Bairro = t1[j].NmBairro.Trim();
                        pessoaEndereco.Cidade = t1[j].NmCidade.Trim();

                        pessoaEndereco.Cep = t1[j].CdCep.ToString().PadLeft(5, '0') + t1[j].CdCepExt.ToString().PadLeft(3, '0');

                        pessoaEndereco.Uf = t1[j].SgEstado.ToUpper();
                        pessoaEndereco.Pais = "Brasil";                        
                    }

                    if (pEnderecoInsere.Count >= 1) {

                        if (!pEnderecoInsere.TemRecebeCorrespondencia()) {

                            // Coloca RecebeCorrespondencia no primeiro
                            pEnderecoInsere[0].RecebeCorrespondencia = "S";
                        }
                    }
                    
                    #endregion

                    #region PessoaTelefone

                    PessoaTelefoneCollection pTelefoneInsere = new PessoaTelefoneCollection();

                    // Para cada telefone do Sinacor Criar 1 telefone no Financial
                    for (int k = 0; k < t1.Count; k++) {
                        //              
                        if (!String.IsNullOrEmpty(t1[k].NrTelefone.ToString())) {
                            PessoaTelefone pessoaTelefone = pTelefoneInsere.AddNew();
                            //

                            pessoaTelefone.IdPessoa = codigosSinacor[i];
                            pessoaTelefone.Ddi = "";
                            pessoaTelefone.Ddd = t1[k].CdDddTel.HasValue ? t1[k].CdDddTel.ToString() : "";
                            pessoaTelefone.Numero = t1[k].NrTelefone.ToString();                           
                            pessoaTelefone.TipoTelefone = (byte)TipoTelefonePessoa.Residencial;
                        }
                    }
                    #endregion

                    using (esTransactionScope scope = new esTransactionScope()) {
                       
                        pessoaEnderecoCollection.Save(); // Deleta Financial
                        //
                        pEnderecoInsere.Save(); // Salva Os do Sinacor

                        if (pTelefoneInsere.Count >= 1) {
                            pessoaTelefoneCollection.Save(); // Deleta Financial
                            //
                            pTelefoneInsere.Save(); // Salva Os do Sinacor
                        }
                        //
                        scope.Complete();
                    }

                }
                #endregion
            }
        }

         /// <summary>
         /// Deleta e Insere Email da Pessoa no Financial de acordo com o Sinacor
         /// </summary>
         /// <param name="codigosSinacor"></param>
         /// <exception cref="Exception">Se codigo sinacor não achado ou cpf não encontrado na base sinacor</exception>
         public void ImportaEmailSinacor(List<int> codigosSinacor) {

             PessoaEmailCollection pessoaEmailCollection = new PessoaEmailCollection();
             
             for (int i = 0; i < codigosSinacor.Count; i++) {

                #region TSCCLIBOL para pegar o cpfCGC
                TscclibolCollection tscclibolCollection = new TscclibolCollection();

                // throws Exception Se codigo Sinacor não achado ou cpf não encontrado
                tscclibolCollection.GetTscclibol(codigosSinacor[i]);

                decimal cpfCGC = tscclibolCollection[0].CdCpfcgc.Value;
                #endregion

                #region TSCEMAIL
                TscemailCollection t2 = new TscemailCollection();
                t2.es.Connection.Name = "Sinacor";
                t2.es.Connection.Schema = ParametrosConfiguracaoSistema.Integracoes.SchemaSinacor;
                //
                t2.Query.Where(t2.Query.CdCpfcgc == cpfCGC)
                        .OrderBy(t2.Query.InPrincipal.Descending);
                //
                t2.Query.Load();

                if (t2.Count >= 1) { // Se tem Email                                                           

                    #region Deleta PessoaEmail por IdPessoa
                    PessoaEmailCollection pessoaEmailCollectionDeletar = new PessoaEmailCollection();
                    pessoaEmailCollectionDeletar.Query.Where(pessoaEmailCollectionDeletar.Query.IdPessoa == codigosSinacor[i]);

                    pessoaEmailCollectionDeletar.Query.Load();
                    pessoaEmailCollectionDeletar.MarkAllAsDeleted();                    
                    #endregion

                    for (int j = 0; j < t2.Count; j++) {                        
                        PessoaEmail pessoaEmail = pessoaEmailCollection.AddNew();
                        //
                        pessoaEmail.IdPessoa = codigosSinacor[i];
                        pessoaEmail.Email = t2[j].NmEMail.Trim();
                    }

                    using (esTransactionScope scope = new esTransactionScope()) {
                        //
                        pessoaEmailCollectionDeletar.Save();
                        //
                        pessoaEmailCollection.Save();
                        //                      
                        scope.Complete();
                    }                                                                                                                                                                                    
                }
                #endregion
            }
        }

        #region Funções Auxiliares
        /// <summary>
        /// Indica se algum elemento da collection tem RecebeCorrespondencia = 'S'
        /// Collection deve estar inicialmente carregada com dados
        /// </summary>
        /// <param name="p"></param>
        private bool TemRecebeCorrespondencia() {
            bool retorno = false;

            for (int i = 0; i < this.Count; i++) {
                if (this[i].RecebeCorrespondencia == "S") {
                    return true;
                }
            }
            return retorno;
        }

        /// <summary>
        /// Collection já ordenada por RecebeCorrespondencia - "S" e "N"
        /// </summary>
        /// <param name="p"></param>
        /// <param name="recebeCorrespondencia">S ou N</param>
        /// <returns>index do primeiro registro com RecebeCorrespondencia igual ao parametro passado
        /// -1 se não achou
        /// </returns>
        private int FindPrimeiroRecebeCorrespondencia(PessoaEnderecoCollection p, string recebeCorrespondencia) {
            int retorno = -1;

            for (int i = 0; i < p.Count; i++) {
                if (p[i].RecebeCorrespondencia == recebeCorrespondencia.ToUpper()) {
                    return i;
                }
            }
            return retorno;
        } 
        #endregion
	}
}