/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 04/09/2014 16:23:26
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;


using EntitySpaces.Interfaces;
using EntitySpaces.Core;





































		



			
					


		

		
		
		

		

























		






	

















				
		
		




		








		
				
				












	

















					
								
											
	












		





		




				
				








				




		

		
		
		
		
		





namespace Financial.CRM
{

	[Serializable]
	abstract public class esSubClasseCnaeCollection : esEntityCollection
	{
		public esSubClasseCnaeCollection()
		{

		}

		protected override string GetCollectionName()
		{
			return "SubClasseCnaeCollection";
		}

		#region Query Logic
		protected void InitQuery(esSubClasseCnaeQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntityCollection)this).Connection;
		}

		protected bool OnQueryLoaded(DataTable table)
		{
			this.PopulateCollection(table);
			return (this.RowCount > 0) ? true : false;
		}
		
		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery(query as esSubClasseCnaeQuery);
		}
		#endregion
		
		virtual public SubClasseCnae DetachEntity(SubClasseCnae entity)
		{
			return base.DetachEntity(entity) as SubClasseCnae;
		}
		
		virtual public SubClasseCnae AttachEntity(SubClasseCnae entity)
		{
			return base.AttachEntity(entity) as SubClasseCnae;
		}
		
		virtual public void Combine(SubClasseCnaeCollection collection)
		{
			base.Combine(collection);
		}
		
		new public SubClasseCnae this[int index]
		{
			get
			{
				return base[index] as SubClasseCnae;
			}
		}

		public override Type GetEntityType()
		{
			return typeof(SubClasseCnae);
		}
	}



	[Serializable]
	abstract public class esSubClasseCnae : esEntity
	{
		/// <summary>
		/// Used internally by the entity's DynamicQuery mechanism.
		/// </summary>
		virtual protected esSubClasseCnaeQuery GetDynamicQuery()
		{
			return null;
		}

		public esSubClasseCnae()
		{

		}

		public esSubClasseCnae(DataRow row)
			: base(row)
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.Int32 idSubClasseCnae)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idSubClasseCnae);
			else
				return LoadByPrimaryKeyStoredProcedure(idSubClasseCnae);
		}

		/// <summary>
		/// Loads an entity by primary key
		/// </summary>
		/// <remarks>
		/// EntitySpaces requires primary keys be defined on all tables.
		/// If a table does not have a primary key set,
		/// this method will not compile.
		/// Does not support sqlAcessType. It only works with DynamicQuery
		/// </remarks>
		/// <param name="fieldsToReturn">Fields desired</param>
		public virtual bool LoadByPrimaryKey(List<esQueryItem> fieldsToReturn, System.Int32 idSubClasseCnae)
		{
			esQueryItem[] fields = fieldsToReturn.ToArray();			
			esSubClasseCnaeQuery query = this.GetDynamicQuery();
			query
			    .Select(fields)
			    .Where(query.IdSubClasseCnae == idSubClasseCnae);
		
			return query.Load();
		}
		
		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.Int32 idSubClasseCnae)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idSubClasseCnae);
			else
				return LoadByPrimaryKeyStoredProcedure(idSubClasseCnae);
		}

		private bool LoadByPrimaryKeyDynamic(System.Int32 idSubClasseCnae)
		{
			esSubClasseCnaeQuery query = this.GetDynamicQuery();
			query.Where(query.IdSubClasseCnae == idSubClasseCnae);
			return query.Load();
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.Int32 idSubClasseCnae)
		{
			esParameters parms = new esParameters();
			parms.Add("IdSubClasseCnae",idSubClasseCnae);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		
		
		#region Properties
		
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}

		public override void SetProperty(string name, object value)
		{
			if(this.Row == null) this.AddNew();
			
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value.GetType().ToString() == "System.String")
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "IdSubClasseCnae": this.str.IdSubClasseCnae = (string)value; break;							
						case "CdDivisao": this.str.CdDivisao = (string)value; break;							
						case "CdGrupo": this.str.CdGrupo = (string)value; break;							
						case "CdClasse": this.str.CdClasse = (string)value; break;							
						case "CdSubClasse": this.str.CdSubClasse = (string)value; break;							
						case "NomeSubClasse": this.str.NomeSubClasse = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "IdSubClasseCnae":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdSubClasseCnae = (System.Int32?)value;
							break;
						
						case "CdDivisao":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.CdDivisao = (System.Int32?)value;
							break;
						
						case "CdGrupo":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.CdGrupo = (System.Int32?)value;
							break;
						
						case "CdClasse":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.CdClasse = (System.Int32?)value;
							break;
						
						case "CdSubClasse":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.CdSubClasse = (System.Int32?)value;
							break;
					

						default:
							break;
					}
				}
			}
			else if(this.Row.Table.Columns.Contains(name))
			{
				this.Row[name] = value;
			}
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}
		
		
		/// <summary>
		/// Maps to SubClasseCnae.IdSubClasseCnae
		/// </summary>
		virtual public System.Int32? IdSubClasseCnae
		{
			get
			{
				return base.GetSystemInt32(SubClasseCnaeMetadata.ColumnNames.IdSubClasseCnae);
			}
			
			set
			{
				base.SetSystemInt32(SubClasseCnaeMetadata.ColumnNames.IdSubClasseCnae, value);
			}
		}
		
		/// <summary>
		/// Maps to SubClasseCnae.CdDivisao
		/// </summary>
		virtual public System.Int32? CdDivisao
		{
			get
			{
				return base.GetSystemInt32(SubClasseCnaeMetadata.ColumnNames.CdDivisao);
			}
			
			set
			{
				base.SetSystemInt32(SubClasseCnaeMetadata.ColumnNames.CdDivisao, value);
			}
		}
		
		/// <summary>
		/// Maps to SubClasseCnae.CdGrupo
		/// </summary>
		virtual public System.Int32? CdGrupo
		{
			get
			{
				return base.GetSystemInt32(SubClasseCnaeMetadata.ColumnNames.CdGrupo);
			}
			
			set
			{
				base.SetSystemInt32(SubClasseCnaeMetadata.ColumnNames.CdGrupo, value);
			}
		}
		
		/// <summary>
		/// Maps to SubClasseCnae.CdClasse
		/// </summary>
		virtual public System.Int32? CdClasse
		{
			get
			{
				return base.GetSystemInt32(SubClasseCnaeMetadata.ColumnNames.CdClasse);
			}
			
			set
			{
				base.SetSystemInt32(SubClasseCnaeMetadata.ColumnNames.CdClasse, value);
			}
		}
		
		/// <summary>
		/// Maps to SubClasseCnae.CdSubClasse
		/// </summary>
		virtual public System.Int32? CdSubClasse
		{
			get
			{
				return base.GetSystemInt32(SubClasseCnaeMetadata.ColumnNames.CdSubClasse);
			}
			
			set
			{
				base.SetSystemInt32(SubClasseCnaeMetadata.ColumnNames.CdSubClasse, value);
			}
		}
		
		/// <summary>
		/// Maps to SubClasseCnae.NomeSubClasse
		/// </summary>
		virtual public System.String NomeSubClasse
		{
			get
			{
				return base.GetSystemString(SubClasseCnaeMetadata.ColumnNames.NomeSubClasse);
			}
			
			set
			{
				base.SetSystemString(SubClasseCnaeMetadata.ColumnNames.NomeSubClasse, value);
			}
		}
		
		#endregion	

		#region String Properties


		[BrowsableAttribute( false )]
		public esStrings str
		{
			get
			{
				if (esstrings == null)
				{
					esstrings = new esStrings(this);
				}
				return esstrings;
			}
		}


		[Serializable]
		sealed public class esStrings
		{
			public esStrings(esSubClasseCnae entity)
			{
				this.entity = entity;
			}
			
	
			public System.String IdSubClasseCnae
			{
				get
				{
					System.Int32? data = entity.IdSubClasseCnae;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdSubClasseCnae = null;
					else entity.IdSubClasseCnae = Convert.ToInt32(value);
				}
			}
				
			public System.String CdDivisao
			{
				get
				{
					System.Int32? data = entity.CdDivisao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.CdDivisao = null;
					else entity.CdDivisao = Convert.ToInt32(value);
				}
			}
				
			public System.String CdGrupo
			{
				get
				{
					System.Int32? data = entity.CdGrupo;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.CdGrupo = null;
					else entity.CdGrupo = Convert.ToInt32(value);
				}
			}
				
			public System.String CdClasse
			{
				get
				{
					System.Int32? data = entity.CdClasse;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.CdClasse = null;
					else entity.CdClasse = Convert.ToInt32(value);
				}
			}
				
			public System.String CdSubClasse
			{
				get
				{
					System.Int32? data = entity.CdSubClasse;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.CdSubClasse = null;
					else entity.CdSubClasse = Convert.ToInt32(value);
				}
			}
				
			public System.String NomeSubClasse
			{
				get
				{
					System.String data = entity.NomeSubClasse;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.NomeSubClasse = null;
					else entity.NomeSubClasse = Convert.ToString(value);
				}
			}
			

			private esSubClasseCnae entity;
		}
		#endregion

		#region Query Logic
		protected void InitQuery(esSubClasseCnaeQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntity)this).Connection;
		}

		[System.Diagnostics.DebuggerNonUserCode]
		protected bool OnQueryLoaded(DataTable table)
		{
			bool dataFound = this.PopulateEntity(table);

			if (this.RowCount > 1)
			{
				throw new Exception("esSubClasseCnae can only hold one record of data");
			}

			return dataFound;
		}
		#endregion
		
		[NonSerialized]
		private esStrings esstrings;
	}


	
	public partial class SubClasseCnae : esSubClasseCnae
	{

		
		/// <summary>
		/// Used internally by the entity's hierarchical properties.
		/// </summary>
		protected override List<esPropertyDescriptor> GetHierarchicalProperties()
		{
			List<esPropertyDescriptor> props = new List<esPropertyDescriptor>();
			
		
			return props;
		}	
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PreSave.
		/// </summary>
		protected override void ApplyPreSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostSave.
		/// </summary>
		protected override void ApplyPostSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostOneToOneSave.
		/// </summary>
		protected override void ApplyPostOneSaveKeys()
		{
		}
		
	}



	[Serializable]
	abstract public class esSubClasseCnaeQuery : esDynamicQuery
	{
		override protected IMetadata Meta
		{
			get
			{
				return SubClasseCnaeMetadata.Meta();
			}
		}	
		

		public esQueryItem IdSubClasseCnae
		{
			get
			{
				return new esQueryItem(this, SubClasseCnaeMetadata.ColumnNames.IdSubClasseCnae, esSystemType.Int32);
			}
		} 
		
		public esQueryItem CdDivisao
		{
			get
			{
				return new esQueryItem(this, SubClasseCnaeMetadata.ColumnNames.CdDivisao, esSystemType.Int32);
			}
		} 
		
		public esQueryItem CdGrupo
		{
			get
			{
				return new esQueryItem(this, SubClasseCnaeMetadata.ColumnNames.CdGrupo, esSystemType.Int32);
			}
		} 
		
		public esQueryItem CdClasse
		{
			get
			{
				return new esQueryItem(this, SubClasseCnaeMetadata.ColumnNames.CdClasse, esSystemType.Int32);
			}
		} 
		
		public esQueryItem CdSubClasse
		{
			get
			{
				return new esQueryItem(this, SubClasseCnaeMetadata.ColumnNames.CdSubClasse, esSystemType.Int32);
			}
		} 
		
		public esQueryItem NomeSubClasse
		{
			get
			{
				return new esQueryItem(this, SubClasseCnaeMetadata.ColumnNames.NomeSubClasse, esSystemType.String);
			}
		} 
		
	}



	[Serializable]
	[XmlType("SubClasseCnaeCollection")]
	public partial class SubClasseCnaeCollection : esSubClasseCnaeCollection, IEnumerable<SubClasseCnae>
	{
		public SubClasseCnaeCollection()
		{

		}
		
		public static implicit operator List<SubClasseCnae>(SubClasseCnaeCollection coll)
		{
			List<SubClasseCnae> list = new List<SubClasseCnae>();
			
			foreach (SubClasseCnae emp in coll)
			{
				list.Add(emp);
			}
			
			return list;
		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return  SubClasseCnaeMetadata.Meta();
			}
		}
		
		
		
		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new SubClasseCnaeQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		
		override protected esEntity CreateEntityForCollection(DataRow row)
		{
			return new SubClasseCnae(row);
		}

		override protected esEntity CreateEntity()
		{
			return new SubClasseCnae();
		}
		
		
		#endregion


		[BrowsableAttribute( false )]
		public SubClasseCnaeQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new SubClasseCnaeQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}

		public bool Load(SubClasseCnaeQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		public SubClasseCnae AddNew()
		{
			SubClasseCnae entity = base.AddNewEntity() as SubClasseCnae;
			
			return entity;
		}

		public SubClasseCnae FindByPrimaryKey(System.Int32 idSubClasseCnae)
		{
			return base.FindByPrimaryKey(idSubClasseCnae) as SubClasseCnae;
		}


		#region IEnumerable<SubClasseCnae> Members

		IEnumerator<SubClasseCnae> IEnumerable<SubClasseCnae>.GetEnumerator()
		{
			System.Collections.IEnumerable enumer = this as System.Collections.IEnumerable;
			System.Collections.IEnumerator iterator = enumer.GetEnumerator();

			while(iterator.MoveNext())
			{
				yield return iterator.Current as SubClasseCnae;
			}
		}

		#endregion
		
		private SubClasseCnaeQuery query;
	}


	/// <summary>
	/// Encapsulates the 'SubClasseCnae' table
	/// </summary>

	[Serializable]
	public partial class SubClasseCnae : esSubClasseCnae
	{
		public SubClasseCnae()
		{

		}
	
		public SubClasseCnae(DataRow row)
			: base(row)
		{

		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return SubClasseCnaeMetadata.Meta();
			}
		}
		
		
		
		override protected esSubClasseCnaeQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new SubClasseCnaeQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		#endregion
		



		[BrowsableAttribute( false )]
		public SubClasseCnaeQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new SubClasseCnaeQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}
		

		public bool Load(SubClasseCnaeQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		private SubClasseCnaeQuery query;
	}



	[Serializable]
	public partial class SubClasseCnaeQuery : esSubClasseCnaeQuery
	{
		public SubClasseCnaeQuery()
		{

		}		
		
		public SubClasseCnaeQuery(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
			
	}



	[Serializable]
	public partial class SubClasseCnaeMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected SubClasseCnaeMetadata()
		{
			_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(SubClasseCnaeMetadata.ColumnNames.IdSubClasseCnae, 0, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = SubClasseCnaeMetadata.PropertyNames.IdSubClasseCnae;
			c.IsInPrimaryKey = true;
			c.IsAutoIncrement = true;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(SubClasseCnaeMetadata.ColumnNames.CdDivisao, 1, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = SubClasseCnaeMetadata.PropertyNames.CdDivisao;	
			c.NumericPrecision = 10;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(SubClasseCnaeMetadata.ColumnNames.CdGrupo, 2, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = SubClasseCnaeMetadata.PropertyNames.CdGrupo;	
			c.NumericPrecision = 10;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(SubClasseCnaeMetadata.ColumnNames.CdClasse, 3, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = SubClasseCnaeMetadata.PropertyNames.CdClasse;	
			c.NumericPrecision = 10;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(SubClasseCnaeMetadata.ColumnNames.CdSubClasse, 4, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = SubClasseCnaeMetadata.PropertyNames.CdSubClasse;	
			c.NumericPrecision = 10;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(SubClasseCnaeMetadata.ColumnNames.NomeSubClasse, 5, typeof(System.String), esSystemType.String);
			c.PropertyName = SubClasseCnaeMetadata.PropertyNames.NomeSubClasse;
			c.CharacterMaxLength = 255;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
		}
		#endregion
	
		static public SubClasseCnaeMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base._dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base._columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string IdSubClasseCnae = "IdSubClasseCnae";
			 public const string CdDivisao = "CdDivisao";
			 public const string CdGrupo = "CdGrupo";
			 public const string CdClasse = "CdClasse";
			 public const string CdSubClasse = "CdSubClasse";
			 public const string NomeSubClasse = "NomeSubClasse";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string IdSubClasseCnae = "IdSubClasseCnae";
			 public const string CdDivisao = "CdDivisao";
			 public const string CdGrupo = "CdGrupo";
			 public const string CdClasse = "CdClasse";
			 public const string CdSubClasse = "CdSubClasse";
			 public const string NomeSubClasse = "NomeSubClasse";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(SubClasseCnaeMetadata))
			{
				if(SubClasseCnaeMetadata.mapDelegates == null)
				{
					SubClasseCnaeMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (SubClasseCnaeMetadata.meta == null)
				{
					SubClasseCnaeMetadata.meta = new SubClasseCnaeMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();
				

				meta.AddTypeMap("IdSubClasseCnae", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("CdDivisao", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("CdGrupo", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("CdClasse", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("CdSubClasse", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("NomeSubClasse", new esTypeMap("varchar", "System.String"));			
				
				
				
				meta.Source = "SubClasseCnae";
				meta.Destination = "SubClasseCnae";
				
				meta.spInsert = "proc_SubClasseCnaeInsert";				
				meta.spUpdate = "proc_SubClasseCnaeUpdate";		
				meta.spDelete = "proc_SubClasseCnaeDelete";
				meta.spLoadAll = "proc_SubClasseCnaeLoadAll";
				meta.spLoadByPrimaryKey = "proc_SubClasseCnaeLoadByPrimaryKey";
				
				this._providerMetadataMaps["esDefault"] = meta;
			}
			
			return this._providerMetadataMaps["esDefault"];
		}

		#endregion

		static private SubClasseCnaeMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
