/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 3/15/2016 3:55:38 PM
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;


using EntitySpaces.Interfaces;
using EntitySpaces.Core;





































		



			
					


		

		
		
		

		

























		






	

















				
		
		




		








		
				
				












	

















					
								
											
	












		





		




				
				








				




		

		
		
		
		
		





namespace Financial.CRM
{

	[Serializable]
	abstract public class esPessoaSuitabilityHistoricoCollection : esEntityCollection
	{
		public esPessoaSuitabilityHistoricoCollection()
		{

		}

		protected override string GetCollectionName()
		{
			return "PessoaSuitabilityHistoricoCollection";
		}

		#region Query Logic
		protected void InitQuery(esPessoaSuitabilityHistoricoQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntityCollection)this).Connection;
		}

		protected bool OnQueryLoaded(DataTable table)
		{
			this.PopulateCollection(table);
			return (this.RowCount > 0) ? true : false;
		}
		
		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery(query as esPessoaSuitabilityHistoricoQuery);
		}
		#endregion
		
		virtual public PessoaSuitabilityHistorico DetachEntity(PessoaSuitabilityHistorico entity)
		{
			return base.DetachEntity(entity) as PessoaSuitabilityHistorico;
		}
		
		virtual public PessoaSuitabilityHistorico AttachEntity(PessoaSuitabilityHistorico entity)
		{
			return base.AttachEntity(entity) as PessoaSuitabilityHistorico;
		}
		
		virtual public void Combine(PessoaSuitabilityHistoricoCollection collection)
		{
			base.Combine(collection);
		}
		
		new public PessoaSuitabilityHistorico this[int index]
		{
			get
			{
				return base[index] as PessoaSuitabilityHistorico;
			}
		}

		public override Type GetEntityType()
		{
			return typeof(PessoaSuitabilityHistorico);
		}
	}



	[Serializable]
	abstract public class esPessoaSuitabilityHistorico : esEntity
	{
		/// <summary>
		/// Used internally by the entity's DynamicQuery mechanism.
		/// </summary>
		virtual protected esPessoaSuitabilityHistoricoQuery GetDynamicQuery()
		{
			return null;
		}

		public esPessoaSuitabilityHistorico()
		{

		}

		public esPessoaSuitabilityHistorico(DataRow row)
			: base(row)
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.DateTime dataHistorico, System.Int32 idPessoa)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(dataHistorico, idPessoa);
			else
				return LoadByPrimaryKeyStoredProcedure(dataHistorico, idPessoa);
		}

		/// <summary>
		/// Loads an entity by primary key
		/// </summary>
		/// <remarks>
		/// EntitySpaces requires primary keys be defined on all tables.
		/// If a table does not have a primary key set,
		/// this method will not compile.
		/// Does not support sqlAcessType. It only works with DynamicQuery
		/// </remarks>
		/// <param name="fieldsToReturn">Fields desired</param>
		public virtual bool LoadByPrimaryKey(List<esQueryItem> fieldsToReturn, System.DateTime dataHistorico, System.Int32 idPessoa)
		{
			esQueryItem[] fields = fieldsToReturn.ToArray();			
			esPessoaSuitabilityHistoricoQuery query = this.GetDynamicQuery();
			query
			    .Select(fields)
			    .Where(query.DataHistorico == dataHistorico, query.IdPessoa == idPessoa);
		
			return query.Load();
		}
		
		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.DateTime dataHistorico, System.Int32 idPessoa)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(dataHistorico, idPessoa);
			else
				return LoadByPrimaryKeyStoredProcedure(dataHistorico, idPessoa);
		}

		private bool LoadByPrimaryKeyDynamic(System.DateTime dataHistorico, System.Int32 idPessoa)
		{
			esPessoaSuitabilityHistoricoQuery query = this.GetDynamicQuery();
			query.Where(query.DataHistorico == dataHistorico, query.IdPessoa == idPessoa);
			return query.Load();
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.DateTime dataHistorico, System.Int32 idPessoa)
		{
			esParameters parms = new esParameters();
			parms.Add("DataHistorico",dataHistorico);			parms.Add("IdPessoa",idPessoa);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		
		
		#region Properties
		
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}

		public override void SetProperty(string name, object value)
		{
			if(this.Row == null) this.AddNew();
			
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value.GetType().ToString() == "System.String")
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "DataHistorico": this.str.DataHistorico = (string)value; break;							
						case "IdPessoa": this.str.IdPessoa = (string)value; break;							
						case "IdValidacao": this.str.IdValidacao = (string)value; break;							
						case "Validacao": this.str.Validacao = (string)value; break;							
						case "IdPerfil": this.str.IdPerfil = (string)value; break;							
						case "Perfil": this.str.Perfil = (string)value; break;							
						case "Recusa": this.str.Recusa = (string)value; break;							
						case "Dispensado": this.str.Dispensado = (string)value; break;							
						case "IdDispensa": this.str.IdDispensa = (string)value; break;							
						case "Dispensa": this.str.Dispensa = (string)value; break;							
						case "UltimaAlteracao": this.str.UltimaAlteracao = (string)value; break;							
						case "Tipo": this.str.Tipo = (string)value; break;							
						case "Usuario": this.str.Usuario = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "DataHistorico":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.DataHistorico = (System.DateTime?)value;
							break;
						
						case "IdPessoa":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdPessoa = (System.Int32?)value;
							break;
						
						case "IdValidacao":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdValidacao = (System.Int32?)value;
							break;
						
						case "IdPerfil":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdPerfil = (System.Int32?)value;
							break;
						
						case "IdDispensa":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdDispensa = (System.Int32?)value;
							break;
						
						case "UltimaAlteracao":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.UltimaAlteracao = (System.DateTime?)value;
							break;
					

						default:
							break;
					}
				}
			}
			else if(this.Row.Table.Columns.Contains(name))
			{
				this.Row[name] = value;
			}
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}
		
		
		/// <summary>
		/// Maps to PessoaSuitabilityHistorico.DataHistorico
		/// </summary>
		virtual public System.DateTime? DataHistorico
		{
			get
			{
				return base.GetSystemDateTime(PessoaSuitabilityHistoricoMetadata.ColumnNames.DataHistorico);
			}
			
			set
			{
				base.SetSystemDateTime(PessoaSuitabilityHistoricoMetadata.ColumnNames.DataHistorico, value);
			}
		}
		
		/// <summary>
		/// Maps to PessoaSuitabilityHistorico.IdPessoa
		/// </summary>
		virtual public System.Int32? IdPessoa
		{
			get
			{
				return base.GetSystemInt32(PessoaSuitabilityHistoricoMetadata.ColumnNames.IdPessoa);
			}
			
			set
			{
				base.SetSystemInt32(PessoaSuitabilityHistoricoMetadata.ColumnNames.IdPessoa, value);
			}
		}
		
		/// <summary>
		/// Maps to PessoaSuitabilityHistorico.IdValidacao
		/// </summary>
		virtual public System.Int32? IdValidacao
		{
			get
			{
				return base.GetSystemInt32(PessoaSuitabilityHistoricoMetadata.ColumnNames.IdValidacao);
			}
			
			set
			{
				base.SetSystemInt32(PessoaSuitabilityHistoricoMetadata.ColumnNames.IdValidacao, value);
			}
		}
		
		/// <summary>
		/// Maps to PessoaSuitabilityHistorico.Validacao
		/// </summary>
		virtual public System.String Validacao
		{
			get
			{
				return base.GetSystemString(PessoaSuitabilityHistoricoMetadata.ColumnNames.Validacao);
			}
			
			set
			{
				base.SetSystemString(PessoaSuitabilityHistoricoMetadata.ColumnNames.Validacao, value);
			}
		}
		
		/// <summary>
		/// Maps to PessoaSuitabilityHistorico.IdPerfil
		/// </summary>
		virtual public System.Int32? IdPerfil
		{
			get
			{
				return base.GetSystemInt32(PessoaSuitabilityHistoricoMetadata.ColumnNames.IdPerfil);
			}
			
			set
			{
				base.SetSystemInt32(PessoaSuitabilityHistoricoMetadata.ColumnNames.IdPerfil, value);
			}
		}
		
		/// <summary>
		/// Maps to PessoaSuitabilityHistorico.Perfil
		/// </summary>
		virtual public System.String Perfil
		{
			get
			{
				return base.GetSystemString(PessoaSuitabilityHistoricoMetadata.ColumnNames.Perfil);
			}
			
			set
			{
				base.SetSystemString(PessoaSuitabilityHistoricoMetadata.ColumnNames.Perfil, value);
			}
		}
		
		/// <summary>
		/// Maps to PessoaSuitabilityHistorico.Recusa
		/// </summary>
		virtual public System.String Recusa
		{
			get
			{
				return base.GetSystemString(PessoaSuitabilityHistoricoMetadata.ColumnNames.Recusa);
			}
			
			set
			{
				base.SetSystemString(PessoaSuitabilityHistoricoMetadata.ColumnNames.Recusa, value);
			}
		}
		
		/// <summary>
		/// Maps to PessoaSuitabilityHistorico.Dispensado
		/// </summary>
		virtual public System.String Dispensado
		{
			get
			{
				return base.GetSystemString(PessoaSuitabilityHistoricoMetadata.ColumnNames.Dispensado);
			}
			
			set
			{
				base.SetSystemString(PessoaSuitabilityHistoricoMetadata.ColumnNames.Dispensado, value);
			}
		}
		
		/// <summary>
		/// Maps to PessoaSuitabilityHistorico.IdDispensa
		/// </summary>
		virtual public System.Int32? IdDispensa
		{
			get
			{
				return base.GetSystemInt32(PessoaSuitabilityHistoricoMetadata.ColumnNames.IdDispensa);
			}
			
			set
			{
				base.SetSystemInt32(PessoaSuitabilityHistoricoMetadata.ColumnNames.IdDispensa, value);
			}
		}
		
		/// <summary>
		/// Maps to PessoaSuitabilityHistorico.Dispensa
		/// </summary>
		virtual public System.String Dispensa
		{
			get
			{
				return base.GetSystemString(PessoaSuitabilityHistoricoMetadata.ColumnNames.Dispensa);
			}
			
			set
			{
				base.SetSystemString(PessoaSuitabilityHistoricoMetadata.ColumnNames.Dispensa, value);
			}
		}
		
		/// <summary>
		/// Maps to PessoaSuitabilityHistorico.UltimaAlteracao
		/// </summary>
		virtual public System.DateTime? UltimaAlteracao
		{
			get
			{
				return base.GetSystemDateTime(PessoaSuitabilityHistoricoMetadata.ColumnNames.UltimaAlteracao);
			}
			
			set
			{
				base.SetSystemDateTime(PessoaSuitabilityHistoricoMetadata.ColumnNames.UltimaAlteracao, value);
			}
		}
		
		/// <summary>
		/// Maps to PessoaSuitabilityHistorico.Tipo
		/// </summary>
		virtual public System.String Tipo
		{
			get
			{
				return base.GetSystemString(PessoaSuitabilityHistoricoMetadata.ColumnNames.Tipo);
			}
			
			set
			{
				base.SetSystemString(PessoaSuitabilityHistoricoMetadata.ColumnNames.Tipo, value);
			}
		}
		
		/// <summary>
		/// Maps to PessoaSuitabilityHistorico.Usuario
		/// </summary>
		virtual public System.String Usuario
		{
			get
			{
				return base.GetSystemString(PessoaSuitabilityHistoricoMetadata.ColumnNames.Usuario);
			}
			
			set
			{
				base.SetSystemString(PessoaSuitabilityHistoricoMetadata.ColumnNames.Usuario, value);
			}
		}
		
		#endregion	

		#region String Properties


		[BrowsableAttribute( false )]
		public esStrings str
		{
			get
			{
				if (esstrings == null)
				{
					esstrings = new esStrings(this);
				}
				return esstrings;
			}
		}


		[Serializable]
		sealed public class esStrings
		{
			public esStrings(esPessoaSuitabilityHistorico entity)
			{
				this.entity = entity;
			}
			
	
			public System.String DataHistorico
			{
				get
				{
					System.DateTime? data = entity.DataHistorico;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DataHistorico = null;
					else entity.DataHistorico = Convert.ToDateTime(value);
				}
			}
				
			public System.String IdPessoa
			{
				get
				{
					System.Int32? data = entity.IdPessoa;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdPessoa = null;
					else entity.IdPessoa = Convert.ToInt32(value);
				}
			}
				
			public System.String IdValidacao
			{
				get
				{
					System.Int32? data = entity.IdValidacao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdValidacao = null;
					else entity.IdValidacao = Convert.ToInt32(value);
				}
			}
				
			public System.String Validacao
			{
				get
				{
					System.String data = entity.Validacao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Validacao = null;
					else entity.Validacao = Convert.ToString(value);
				}
			}
				
			public System.String IdPerfil
			{
				get
				{
					System.Int32? data = entity.IdPerfil;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdPerfil = null;
					else entity.IdPerfil = Convert.ToInt32(value);
				}
			}
				
			public System.String Perfil
			{
				get
				{
					System.String data = entity.Perfil;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Perfil = null;
					else entity.Perfil = Convert.ToString(value);
				}
			}
				
			public System.String Recusa
			{
				get
				{
					System.String data = entity.Recusa;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Recusa = null;
					else entity.Recusa = Convert.ToString(value);
				}
			}
				
			public System.String Dispensado
			{
				get
				{
					System.String data = entity.Dispensado;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Dispensado = null;
					else entity.Dispensado = Convert.ToString(value);
				}
			}
				
			public System.String IdDispensa
			{
				get
				{
					System.Int32? data = entity.IdDispensa;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdDispensa = null;
					else entity.IdDispensa = Convert.ToInt32(value);
				}
			}
				
			public System.String Dispensa
			{
				get
				{
					System.String data = entity.Dispensa;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Dispensa = null;
					else entity.Dispensa = Convert.ToString(value);
				}
			}
				
			public System.String UltimaAlteracao
			{
				get
				{
					System.DateTime? data = entity.UltimaAlteracao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.UltimaAlteracao = null;
					else entity.UltimaAlteracao = Convert.ToDateTime(value);
				}
			}
				
			public System.String Tipo
			{
				get
				{
					System.String data = entity.Tipo;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Tipo = null;
					else entity.Tipo = Convert.ToString(value);
				}
			}
				
			public System.String Usuario
			{
				get
				{
					System.String data = entity.Usuario;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Usuario = null;
					else entity.Usuario = Convert.ToString(value);
				}
			}
			

			private esPessoaSuitabilityHistorico entity;
		}
		#endregion

		#region Query Logic
		protected void InitQuery(esPessoaSuitabilityHistoricoQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntity)this).Connection;
		}

		[System.Diagnostics.DebuggerNonUserCode]
		protected bool OnQueryLoaded(DataTable table)
		{
			bool dataFound = this.PopulateEntity(table);

			if (this.RowCount > 1)
			{
				throw new Exception("esPessoaSuitabilityHistorico can only hold one record of data");
			}

			return dataFound;
		}
		#endregion
		
		[NonSerialized]
		private esStrings esstrings;
	}


	
	public partial class PessoaSuitabilityHistorico : esPessoaSuitabilityHistorico
	{

		
		/// <summary>
		/// Used internally by the entity's hierarchical properties.
		/// </summary>
		protected override List<esPropertyDescriptor> GetHierarchicalProperties()
		{
			List<esPropertyDescriptor> props = new List<esPropertyDescriptor>();
			
		
			return props;
		}	
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PreSave.
		/// </summary>
		protected override void ApplyPreSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostSave.
		/// </summary>
		protected override void ApplyPostSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostOneToOneSave.
		/// </summary>
		protected override void ApplyPostOneSaveKeys()
		{
		}
		
	}



	[Serializable]
	abstract public class esPessoaSuitabilityHistoricoQuery : esDynamicQuery
	{
		override protected IMetadata Meta
		{
			get
			{
				return PessoaSuitabilityHistoricoMetadata.Meta();
			}
		}	
		

		public esQueryItem DataHistorico
		{
			get
			{
				return new esQueryItem(this, PessoaSuitabilityHistoricoMetadata.ColumnNames.DataHistorico, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem IdPessoa
		{
			get
			{
				return new esQueryItem(this, PessoaSuitabilityHistoricoMetadata.ColumnNames.IdPessoa, esSystemType.Int32);
			}
		} 
		
		public esQueryItem IdValidacao
		{
			get
			{
				return new esQueryItem(this, PessoaSuitabilityHistoricoMetadata.ColumnNames.IdValidacao, esSystemType.Int32);
			}
		} 
		
		public esQueryItem Validacao
		{
			get
			{
				return new esQueryItem(this, PessoaSuitabilityHistoricoMetadata.ColumnNames.Validacao, esSystemType.String);
			}
		} 
		
		public esQueryItem IdPerfil
		{
			get
			{
				return new esQueryItem(this, PessoaSuitabilityHistoricoMetadata.ColumnNames.IdPerfil, esSystemType.Int32);
			}
		} 
		
		public esQueryItem Perfil
		{
			get
			{
				return new esQueryItem(this, PessoaSuitabilityHistoricoMetadata.ColumnNames.Perfil, esSystemType.String);
			}
		} 
		
		public esQueryItem Recusa
		{
			get
			{
				return new esQueryItem(this, PessoaSuitabilityHistoricoMetadata.ColumnNames.Recusa, esSystemType.String);
			}
		} 
		
		public esQueryItem Dispensado
		{
			get
			{
				return new esQueryItem(this, PessoaSuitabilityHistoricoMetadata.ColumnNames.Dispensado, esSystemType.String);
			}
		} 
		
		public esQueryItem IdDispensa
		{
			get
			{
				return new esQueryItem(this, PessoaSuitabilityHistoricoMetadata.ColumnNames.IdDispensa, esSystemType.Int32);
			}
		} 
		
		public esQueryItem Dispensa
		{
			get
			{
				return new esQueryItem(this, PessoaSuitabilityHistoricoMetadata.ColumnNames.Dispensa, esSystemType.String);
			}
		} 
		
		public esQueryItem UltimaAlteracao
		{
			get
			{
				return new esQueryItem(this, PessoaSuitabilityHistoricoMetadata.ColumnNames.UltimaAlteracao, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem Tipo
		{
			get
			{
				return new esQueryItem(this, PessoaSuitabilityHistoricoMetadata.ColumnNames.Tipo, esSystemType.String);
			}
		} 
		
		public esQueryItem Usuario
		{
			get
			{
				return new esQueryItem(this, PessoaSuitabilityHistoricoMetadata.ColumnNames.Usuario, esSystemType.String);
			}
		} 
		
	}



	[Serializable]
	[XmlType("PessoaSuitabilityHistoricoCollection")]
	public partial class PessoaSuitabilityHistoricoCollection : esPessoaSuitabilityHistoricoCollection, IEnumerable<PessoaSuitabilityHistorico>
	{
		public PessoaSuitabilityHistoricoCollection()
		{

		}
		
		public static implicit operator List<PessoaSuitabilityHistorico>(PessoaSuitabilityHistoricoCollection coll)
		{
			List<PessoaSuitabilityHistorico> list = new List<PessoaSuitabilityHistorico>();
			
			foreach (PessoaSuitabilityHistorico emp in coll)
			{
				list.Add(emp);
			}
			
			return list;
		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return  PessoaSuitabilityHistoricoMetadata.Meta();
			}
		}
		
		
		
		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new PessoaSuitabilityHistoricoQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		
		override protected esEntity CreateEntityForCollection(DataRow row)
		{
			return new PessoaSuitabilityHistorico(row);
		}

		override protected esEntity CreateEntity()
		{
			return new PessoaSuitabilityHistorico();
		}
		
		
		#endregion


		[BrowsableAttribute( false )]
		public PessoaSuitabilityHistoricoQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new PessoaSuitabilityHistoricoQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}

		public bool Load(PessoaSuitabilityHistoricoQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		public PessoaSuitabilityHistorico AddNew()
		{
			PessoaSuitabilityHistorico entity = base.AddNewEntity() as PessoaSuitabilityHistorico;
			
			return entity;
		}

		public PessoaSuitabilityHistorico FindByPrimaryKey(System.DateTime dataHistorico, System.Int32 idPessoa)
		{
			return base.FindByPrimaryKey(dataHistorico, idPessoa) as PessoaSuitabilityHistorico;
		}


		#region IEnumerable<PessoaSuitabilityHistorico> Members

		IEnumerator<PessoaSuitabilityHistorico> IEnumerable<PessoaSuitabilityHistorico>.GetEnumerator()
		{
			System.Collections.IEnumerable enumer = this as System.Collections.IEnumerable;
			System.Collections.IEnumerator iterator = enumer.GetEnumerator();

			while(iterator.MoveNext())
			{
				yield return iterator.Current as PessoaSuitabilityHistorico;
			}
		}

		#endregion
		
		private PessoaSuitabilityHistoricoQuery query;
	}


	/// <summary>
	/// Encapsulates the 'PessoaSuitabilityHistorico' table
	/// </summary>

	[Serializable]
	public partial class PessoaSuitabilityHistorico : esPessoaSuitabilityHistorico
	{
		public PessoaSuitabilityHistorico()
		{

		}
	
		public PessoaSuitabilityHistorico(DataRow row)
			: base(row)
		{

		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return PessoaSuitabilityHistoricoMetadata.Meta();
			}
		}
		
		
		
		override protected esPessoaSuitabilityHistoricoQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new PessoaSuitabilityHistoricoQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		#endregion
		



		[BrowsableAttribute( false )]
		public PessoaSuitabilityHistoricoQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new PessoaSuitabilityHistoricoQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}
		

		public bool Load(PessoaSuitabilityHistoricoQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		private PessoaSuitabilityHistoricoQuery query;
	}



	[Serializable]
	public partial class PessoaSuitabilityHistoricoQuery : esPessoaSuitabilityHistoricoQuery
	{
		public PessoaSuitabilityHistoricoQuery()
		{

		}		
		
		public PessoaSuitabilityHistoricoQuery(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
			
	}



	[Serializable]
	public partial class PessoaSuitabilityHistoricoMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected PessoaSuitabilityHistoricoMetadata()
		{
			_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(PessoaSuitabilityHistoricoMetadata.ColumnNames.DataHistorico, 0, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = PessoaSuitabilityHistoricoMetadata.PropertyNames.DataHistorico;
			c.IsInPrimaryKey = true;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PessoaSuitabilityHistoricoMetadata.ColumnNames.IdPessoa, 1, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = PessoaSuitabilityHistoricoMetadata.PropertyNames.IdPessoa;
			c.IsInPrimaryKey = true;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PessoaSuitabilityHistoricoMetadata.ColumnNames.IdValidacao, 2, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = PessoaSuitabilityHistoricoMetadata.PropertyNames.IdValidacao;	
			c.NumericPrecision = 10;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PessoaSuitabilityHistoricoMetadata.ColumnNames.Validacao, 3, typeof(System.String), esSystemType.String);
			c.PropertyName = PessoaSuitabilityHistoricoMetadata.PropertyNames.Validacao;
			c.CharacterMaxLength = 100;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PessoaSuitabilityHistoricoMetadata.ColumnNames.IdPerfil, 4, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = PessoaSuitabilityHistoricoMetadata.PropertyNames.IdPerfil;	
			c.NumericPrecision = 10;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PessoaSuitabilityHistoricoMetadata.ColumnNames.Perfil, 5, typeof(System.String), esSystemType.String);
			c.PropertyName = PessoaSuitabilityHistoricoMetadata.PropertyNames.Perfil;
			c.CharacterMaxLength = 100;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PessoaSuitabilityHistoricoMetadata.ColumnNames.Recusa, 6, typeof(System.String), esSystemType.String);
			c.PropertyName = PessoaSuitabilityHistoricoMetadata.PropertyNames.Recusa;
			c.CharacterMaxLength = 1;
			c.NumericPrecision = 0;
			c.HasDefault = true;
			c.Default = @"('N')";
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PessoaSuitabilityHistoricoMetadata.ColumnNames.Dispensado, 7, typeof(System.String), esSystemType.String);
			c.PropertyName = PessoaSuitabilityHistoricoMetadata.PropertyNames.Dispensado;
			c.CharacterMaxLength = 1;
			c.NumericPrecision = 0;
			c.HasDefault = true;
			c.Default = @"('N')";
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PessoaSuitabilityHistoricoMetadata.ColumnNames.IdDispensa, 8, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = PessoaSuitabilityHistoricoMetadata.PropertyNames.IdDispensa;	
			c.NumericPrecision = 10;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PessoaSuitabilityHistoricoMetadata.ColumnNames.Dispensa, 9, typeof(System.String), esSystemType.String);
			c.PropertyName = PessoaSuitabilityHistoricoMetadata.PropertyNames.Dispensa;
			c.CharacterMaxLength = 100;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PessoaSuitabilityHistoricoMetadata.ColumnNames.UltimaAlteracao, 10, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = PessoaSuitabilityHistoricoMetadata.PropertyNames.UltimaAlteracao;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PessoaSuitabilityHistoricoMetadata.ColumnNames.Tipo, 11, typeof(System.String), esSystemType.String);
			c.PropertyName = PessoaSuitabilityHistoricoMetadata.PropertyNames.Tipo;
			c.CharacterMaxLength = 10;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PessoaSuitabilityHistoricoMetadata.ColumnNames.Usuario, 12, typeof(System.String), esSystemType.String);
			c.PropertyName = PessoaSuitabilityHistoricoMetadata.PropertyNames.Usuario;
			c.CharacterMaxLength = 100;
			c.NumericPrecision = 0;
			c.HasDefault = true;
			c.Default = @"('')";
			_columns.Add(c); 
			
				
		}
		#endregion
	
		static public PessoaSuitabilityHistoricoMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base._dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base._columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string DataHistorico = "DataHistorico";
			 public const string IdPessoa = "IdPessoa";
			 public const string IdValidacao = "IdValidacao";
			 public const string Validacao = "Validacao";
			 public const string IdPerfil = "IdPerfil";
			 public const string Perfil = "Perfil";
			 public const string Recusa = "Recusa";
			 public const string Dispensado = "Dispensado";
			 public const string IdDispensa = "IdDispensa";
			 public const string Dispensa = "Dispensa";
			 public const string UltimaAlteracao = "UltimaAlteracao";
			 public const string Tipo = "Tipo";
			 public const string Usuario = "Usuario";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string DataHistorico = "DataHistorico";
			 public const string IdPessoa = "IdPessoa";
			 public const string IdValidacao = "IdValidacao";
			 public const string Validacao = "Validacao";
			 public const string IdPerfil = "IdPerfil";
			 public const string Perfil = "Perfil";
			 public const string Recusa = "Recusa";
			 public const string Dispensado = "Dispensado";
			 public const string IdDispensa = "IdDispensa";
			 public const string Dispensa = "Dispensa";
			 public const string UltimaAlteracao = "UltimaAlteracao";
			 public const string Tipo = "Tipo";
			 public const string Usuario = "Usuario";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(PessoaSuitabilityHistoricoMetadata))
			{
				if(PessoaSuitabilityHistoricoMetadata.mapDelegates == null)
				{
					PessoaSuitabilityHistoricoMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (PessoaSuitabilityHistoricoMetadata.meta == null)
				{
					PessoaSuitabilityHistoricoMetadata.meta = new PessoaSuitabilityHistoricoMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();
				

				meta.AddTypeMap("DataHistorico", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("IdPessoa", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("IdValidacao", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("Validacao", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("IdPerfil", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("Perfil", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("Recusa", new esTypeMap("char", "System.String"));
				meta.AddTypeMap("Dispensado", new esTypeMap("char", "System.String"));
				meta.AddTypeMap("IdDispensa", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("Dispensa", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("UltimaAlteracao", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("Tipo", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("Usuario", new esTypeMap("varchar", "System.String"));			
				
				
				
				meta.Source = "PessoaSuitabilityHistorico";
				meta.Destination = "PessoaSuitabilityHistorico";
				
				meta.spInsert = "proc_PessoaSuitabilityHistoricoInsert";				
				meta.spUpdate = "proc_PessoaSuitabilityHistoricoUpdate";		
				meta.spDelete = "proc_PessoaSuitabilityHistoricoDelete";
				meta.spLoadAll = "proc_PessoaSuitabilityHistoricoLoadAll";
				meta.spLoadByPrimaryKey = "proc_PessoaSuitabilityHistoricoLoadByPrimaryKey";
				
				this._providerMetadataMaps["esDefault"] = meta;
			}
			
			return this._providerMetadataMaps["esDefault"];
		}

		#endregion

		static private PessoaSuitabilityHistoricoMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
