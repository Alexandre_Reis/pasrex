/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 04/09/2014 16:23:25
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;


using EntitySpaces.Interfaces;
using EntitySpaces.Core;





































		



			
					


		

		
		
		

		

























		






	

















				
		
		




		








		
				
				












	

















					
								
											
	












		





		




				
				








				




		

		
		
		
		
		





namespace Financial.CRM
{

	[Serializable]
	abstract public class esGrupoCnaeCollection : esEntityCollection
	{
		public esGrupoCnaeCollection()
		{

		}

		protected override string GetCollectionName()
		{
			return "GrupoCnaeCollection";
		}

		#region Query Logic
		protected void InitQuery(esGrupoCnaeQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntityCollection)this).Connection;
		}

		protected bool OnQueryLoaded(DataTable table)
		{
			this.PopulateCollection(table);
			return (this.RowCount > 0) ? true : false;
		}
		
		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery(query as esGrupoCnaeQuery);
		}
		#endregion
		
		virtual public GrupoCnae DetachEntity(GrupoCnae entity)
		{
			return base.DetachEntity(entity) as GrupoCnae;
		}
		
		virtual public GrupoCnae AttachEntity(GrupoCnae entity)
		{
			return base.AttachEntity(entity) as GrupoCnae;
		}
		
		virtual public void Combine(GrupoCnaeCollection collection)
		{
			base.Combine(collection);
		}
		
		new public GrupoCnae this[int index]
		{
			get
			{
				return base[index] as GrupoCnae;
			}
		}

		public override Type GetEntityType()
		{
			return typeof(GrupoCnae);
		}
	}



	[Serializable]
	abstract public class esGrupoCnae : esEntity
	{
		/// <summary>
		/// Used internally by the entity's DynamicQuery mechanism.
		/// </summary>
		virtual protected esGrupoCnaeQuery GetDynamicQuery()
		{
			return null;
		}

		public esGrupoCnae()
		{

		}

		public esGrupoCnae(DataRow row)
			: base(row)
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.Int32 idGrupoCnae)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idGrupoCnae);
			else
				return LoadByPrimaryKeyStoredProcedure(idGrupoCnae);
		}

		/// <summary>
		/// Loads an entity by primary key
		/// </summary>
		/// <remarks>
		/// EntitySpaces requires primary keys be defined on all tables.
		/// If a table does not have a primary key set,
		/// this method will not compile.
		/// Does not support sqlAcessType. It only works with DynamicQuery
		/// </remarks>
		/// <param name="fieldsToReturn">Fields desired</param>
		public virtual bool LoadByPrimaryKey(List<esQueryItem> fieldsToReturn, System.Int32 idGrupoCnae)
		{
			esQueryItem[] fields = fieldsToReturn.ToArray();			
			esGrupoCnaeQuery query = this.GetDynamicQuery();
			query
			    .Select(fields)
			    .Where(query.IdGrupoCnae == idGrupoCnae);
		
			return query.Load();
		}
		
		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.Int32 idGrupoCnae)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idGrupoCnae);
			else
				return LoadByPrimaryKeyStoredProcedure(idGrupoCnae);
		}

		private bool LoadByPrimaryKeyDynamic(System.Int32 idGrupoCnae)
		{
			esGrupoCnaeQuery query = this.GetDynamicQuery();
			query.Where(query.IdGrupoCnae == idGrupoCnae);
			return query.Load();
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.Int32 idGrupoCnae)
		{
			esParameters parms = new esParameters();
			parms.Add("IdGrupoCnae",idGrupoCnae);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		
		
		#region Properties
		
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}

		public override void SetProperty(string name, object value)
		{
			if(this.Row == null) this.AddNew();
			
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value.GetType().ToString() == "System.String")
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "IdGrupoCnae": this.str.IdGrupoCnae = (string)value; break;							
						case "CdDivisao": this.str.CdDivisao = (string)value; break;							
						case "CdGrupo": this.str.CdGrupo = (string)value; break;							
						case "NomeGrupo": this.str.NomeGrupo = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "IdGrupoCnae":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdGrupoCnae = (System.Int32?)value;
							break;
						
						case "CdDivisao":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.CdDivisao = (System.Int32?)value;
							break;
						
						case "CdGrupo":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.CdGrupo = (System.Int32?)value;
							break;
					

						default:
							break;
					}
				}
			}
			else if(this.Row.Table.Columns.Contains(name))
			{
				this.Row[name] = value;
			}
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}
		
		
		/// <summary>
		/// Maps to GrupoCnae.IdGrupoCnae
		/// </summary>
		virtual public System.Int32? IdGrupoCnae
		{
			get
			{
				return base.GetSystemInt32(GrupoCnaeMetadata.ColumnNames.IdGrupoCnae);
			}
			
			set
			{
				base.SetSystemInt32(GrupoCnaeMetadata.ColumnNames.IdGrupoCnae, value);
			}
		}
		
		/// <summary>
		/// Maps to GrupoCnae.CdDivisao
		/// </summary>
		virtual public System.Int32? CdDivisao
		{
			get
			{
				return base.GetSystemInt32(GrupoCnaeMetadata.ColumnNames.CdDivisao);
			}
			
			set
			{
				base.SetSystemInt32(GrupoCnaeMetadata.ColumnNames.CdDivisao, value);
			}
		}
		
		/// <summary>
		/// Maps to GrupoCnae.CdGrupo
		/// </summary>
		virtual public System.Int32? CdGrupo
		{
			get
			{
				return base.GetSystemInt32(GrupoCnaeMetadata.ColumnNames.CdGrupo);
			}
			
			set
			{
				base.SetSystemInt32(GrupoCnaeMetadata.ColumnNames.CdGrupo, value);
			}
		}
		
		/// <summary>
		/// Maps to GrupoCnae.NomeGrupo
		/// </summary>
		virtual public System.String NomeGrupo
		{
			get
			{
				return base.GetSystemString(GrupoCnaeMetadata.ColumnNames.NomeGrupo);
			}
			
			set
			{
				base.SetSystemString(GrupoCnaeMetadata.ColumnNames.NomeGrupo, value);
			}
		}
		
		#endregion	

		#region String Properties


		[BrowsableAttribute( false )]
		public esStrings str
		{
			get
			{
				if (esstrings == null)
				{
					esstrings = new esStrings(this);
				}
				return esstrings;
			}
		}


		[Serializable]
		sealed public class esStrings
		{
			public esStrings(esGrupoCnae entity)
			{
				this.entity = entity;
			}
			
	
			public System.String IdGrupoCnae
			{
				get
				{
					System.Int32? data = entity.IdGrupoCnae;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdGrupoCnae = null;
					else entity.IdGrupoCnae = Convert.ToInt32(value);
				}
			}
				
			public System.String CdDivisao
			{
				get
				{
					System.Int32? data = entity.CdDivisao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.CdDivisao = null;
					else entity.CdDivisao = Convert.ToInt32(value);
				}
			}
				
			public System.String CdGrupo
			{
				get
				{
					System.Int32? data = entity.CdGrupo;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.CdGrupo = null;
					else entity.CdGrupo = Convert.ToInt32(value);
				}
			}
				
			public System.String NomeGrupo
			{
				get
				{
					System.String data = entity.NomeGrupo;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.NomeGrupo = null;
					else entity.NomeGrupo = Convert.ToString(value);
				}
			}
			

			private esGrupoCnae entity;
		}
		#endregion

		#region Query Logic
		protected void InitQuery(esGrupoCnaeQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntity)this).Connection;
		}

		[System.Diagnostics.DebuggerNonUserCode]
		protected bool OnQueryLoaded(DataTable table)
		{
			bool dataFound = this.PopulateEntity(table);

			if (this.RowCount > 1)
			{
				throw new Exception("esGrupoCnae can only hold one record of data");
			}

			return dataFound;
		}
		#endregion
		
		[NonSerialized]
		private esStrings esstrings;
	}


	
	public partial class GrupoCnae : esGrupoCnae
	{

		
		/// <summary>
		/// Used internally by the entity's hierarchical properties.
		/// </summary>
		protected override List<esPropertyDescriptor> GetHierarchicalProperties()
		{
			List<esPropertyDescriptor> props = new List<esPropertyDescriptor>();
			
		
			return props;
		}	
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PreSave.
		/// </summary>
		protected override void ApplyPreSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostSave.
		/// </summary>
		protected override void ApplyPostSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostOneToOneSave.
		/// </summary>
		protected override void ApplyPostOneSaveKeys()
		{
		}
		
	}



	[Serializable]
	abstract public class esGrupoCnaeQuery : esDynamicQuery
	{
		override protected IMetadata Meta
		{
			get
			{
				return GrupoCnaeMetadata.Meta();
			}
		}	
		

		public esQueryItem IdGrupoCnae
		{
			get
			{
				return new esQueryItem(this, GrupoCnaeMetadata.ColumnNames.IdGrupoCnae, esSystemType.Int32);
			}
		} 
		
		public esQueryItem CdDivisao
		{
			get
			{
				return new esQueryItem(this, GrupoCnaeMetadata.ColumnNames.CdDivisao, esSystemType.Int32);
			}
		} 
		
		public esQueryItem CdGrupo
		{
			get
			{
				return new esQueryItem(this, GrupoCnaeMetadata.ColumnNames.CdGrupo, esSystemType.Int32);
			}
		} 
		
		public esQueryItem NomeGrupo
		{
			get
			{
				return new esQueryItem(this, GrupoCnaeMetadata.ColumnNames.NomeGrupo, esSystemType.String);
			}
		} 
		
	}



	[Serializable]
	[XmlType("GrupoCnaeCollection")]
	public partial class GrupoCnaeCollection : esGrupoCnaeCollection, IEnumerable<GrupoCnae>
	{
		public GrupoCnaeCollection()
		{

		}
		
		public static implicit operator List<GrupoCnae>(GrupoCnaeCollection coll)
		{
			List<GrupoCnae> list = new List<GrupoCnae>();
			
			foreach (GrupoCnae emp in coll)
			{
				list.Add(emp);
			}
			
			return list;
		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return  GrupoCnaeMetadata.Meta();
			}
		}
		
		
		
		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new GrupoCnaeQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		
		override protected esEntity CreateEntityForCollection(DataRow row)
		{
			return new GrupoCnae(row);
		}

		override protected esEntity CreateEntity()
		{
			return new GrupoCnae();
		}
		
		
		#endregion


		[BrowsableAttribute( false )]
		public GrupoCnaeQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new GrupoCnaeQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}

		public bool Load(GrupoCnaeQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		public GrupoCnae AddNew()
		{
			GrupoCnae entity = base.AddNewEntity() as GrupoCnae;
			
			return entity;
		}

		public GrupoCnae FindByPrimaryKey(System.Int32 idGrupoCnae)
		{
			return base.FindByPrimaryKey(idGrupoCnae) as GrupoCnae;
		}


		#region IEnumerable<GrupoCnae> Members

		IEnumerator<GrupoCnae> IEnumerable<GrupoCnae>.GetEnumerator()
		{
			System.Collections.IEnumerable enumer = this as System.Collections.IEnumerable;
			System.Collections.IEnumerator iterator = enumer.GetEnumerator();

			while(iterator.MoveNext())
			{
				yield return iterator.Current as GrupoCnae;
			}
		}

		#endregion
		
		private GrupoCnaeQuery query;
	}


	/// <summary>
	/// Encapsulates the 'GrupoCnae' table
	/// </summary>

	[Serializable]
	public partial class GrupoCnae : esGrupoCnae
	{
		public GrupoCnae()
		{

		}
	
		public GrupoCnae(DataRow row)
			: base(row)
		{

		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return GrupoCnaeMetadata.Meta();
			}
		}
		
		
		
		override protected esGrupoCnaeQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new GrupoCnaeQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		#endregion
		



		[BrowsableAttribute( false )]
		public GrupoCnaeQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new GrupoCnaeQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}
		

		public bool Load(GrupoCnaeQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		private GrupoCnaeQuery query;
	}



	[Serializable]
	public partial class GrupoCnaeQuery : esGrupoCnaeQuery
	{
		public GrupoCnaeQuery()
		{

		}		
		
		public GrupoCnaeQuery(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
			
	}



	[Serializable]
	public partial class GrupoCnaeMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected GrupoCnaeMetadata()
		{
			_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(GrupoCnaeMetadata.ColumnNames.IdGrupoCnae, 0, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = GrupoCnaeMetadata.PropertyNames.IdGrupoCnae;
			c.IsInPrimaryKey = true;
			c.IsAutoIncrement = true;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(GrupoCnaeMetadata.ColumnNames.CdDivisao, 1, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = GrupoCnaeMetadata.PropertyNames.CdDivisao;	
			c.NumericPrecision = 10;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(GrupoCnaeMetadata.ColumnNames.CdGrupo, 2, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = GrupoCnaeMetadata.PropertyNames.CdGrupo;	
			c.NumericPrecision = 10;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(GrupoCnaeMetadata.ColumnNames.NomeGrupo, 3, typeof(System.String), esSystemType.String);
			c.PropertyName = GrupoCnaeMetadata.PropertyNames.NomeGrupo;
			c.CharacterMaxLength = 255;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
		}
		#endregion
	
		static public GrupoCnaeMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base._dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base._columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string IdGrupoCnae = "IdGrupoCnae";
			 public const string CdDivisao = "CdDivisao";
			 public const string CdGrupo = "CdGrupo";
			 public const string NomeGrupo = "NomeGrupo";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string IdGrupoCnae = "IdGrupoCnae";
			 public const string CdDivisao = "CdDivisao";
			 public const string CdGrupo = "CdGrupo";
			 public const string NomeGrupo = "NomeGrupo";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(GrupoCnaeMetadata))
			{
				if(GrupoCnaeMetadata.mapDelegates == null)
				{
					GrupoCnaeMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (GrupoCnaeMetadata.meta == null)
				{
					GrupoCnaeMetadata.meta = new GrupoCnaeMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();
				

				meta.AddTypeMap("IdGrupoCnae", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("CdDivisao", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("CdGrupo", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("NomeGrupo", new esTypeMap("varchar", "System.String"));			
				
				
				
				meta.Source = "GrupoCnae";
				meta.Destination = "GrupoCnae";
				
				meta.spInsert = "proc_GrupoCnaeInsert";				
				meta.spUpdate = "proc_GrupoCnaeUpdate";		
				meta.spDelete = "proc_GrupoCnaeDelete";
				meta.spLoadAll = "proc_GrupoCnaeLoadAll";
				meta.spLoadByPrimaryKey = "proc_GrupoCnaeLoadByPrimaryKey";
				
				this._providerMetadataMaps["esDefault"] = meta;
			}
			
			return this._providerMetadataMaps["esDefault"];
		}

		#endregion

		static private GrupoCnaeMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
