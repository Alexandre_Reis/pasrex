/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 23/05/2014 15:02:35
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;


using EntitySpaces.Interfaces;
using EntitySpaces.Core;





































		



			
					


		

		
		
		

		

























		






	

















				
		
		




		








		
				
				












	

















					
								
											
	










		





		




				
				








				




		

		
		
		
		
		





namespace Financial.CRM
{

	[Serializable]
	abstract public class esPessoaTelefoneCollection : esEntityCollection
	{
		public esPessoaTelefoneCollection()
		{

		}

		protected override string GetCollectionName()
		{
			return "PessoaTelefoneCollection";
		}

		#region Query Logic
		protected void InitQuery(esPessoaTelefoneQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntityCollection)this).Connection;
		}

		protected bool OnQueryLoaded(DataTable table)
		{
			this.PopulateCollection(table);
			return (this.RowCount > 0) ? true : false;
		}
		
		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery(query as esPessoaTelefoneQuery);
		}
		#endregion
		
		virtual public PessoaTelefone DetachEntity(PessoaTelefone entity)
		{
			return base.DetachEntity(entity) as PessoaTelefone;
		}
		
		virtual public PessoaTelefone AttachEntity(PessoaTelefone entity)
		{
			return base.AttachEntity(entity) as PessoaTelefone;
		}
		
		virtual public void Combine(PessoaTelefoneCollection collection)
		{
			base.Combine(collection);
		}
		
		new public PessoaTelefone this[int index]
		{
			get
			{
				return base[index] as PessoaTelefone;
			}
		}

		public override Type GetEntityType()
		{
			return typeof(PessoaTelefone);
		}
	}



	[Serializable]
	abstract public class esPessoaTelefone : esEntity
	{
		/// <summary>
		/// Used internally by the entity's DynamicQuery mechanism.
		/// </summary>
		virtual protected esPessoaTelefoneQuery GetDynamicQuery()
		{
			return null;
		}

		public esPessoaTelefone()
		{

		}

		public esPessoaTelefone(DataRow row)
			: base(row)
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.Int32 idTelefone)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idTelefone);
			else
				return LoadByPrimaryKeyStoredProcedure(idTelefone);
		}

		/// <summary>
		/// Loads an entity by primary key
		/// </summary>
		/// <remarks>
		/// EntitySpaces requires primary keys be defined on all tables.
		/// If a table does not have a primary key set,
		/// this method will not compile.
		/// Does not support sqlAcessType. It only works with DynamicQuery
		/// </remarks>
		/// <param name="fieldsToReturn">Fields desired</param>
		public virtual bool LoadByPrimaryKey(List<esQueryItem> fieldsToReturn, System.Int32 idTelefone)
		{
			esQueryItem[] fields = fieldsToReturn.ToArray();			
			esPessoaTelefoneQuery query = this.GetDynamicQuery();
			query
			    .Select(fields)
			    .Where(query.IdTelefone == idTelefone);
		
			return query.Load();
		}
		
		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.Int32 idTelefone)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idTelefone);
			else
				return LoadByPrimaryKeyStoredProcedure(idTelefone);
		}

		private bool LoadByPrimaryKeyDynamic(System.Int32 idTelefone)
		{
			esPessoaTelefoneQuery query = this.GetDynamicQuery();
			query.Where(query.IdTelefone == idTelefone);
			return query.Load();
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.Int32 idTelefone)
		{
			esParameters parms = new esParameters();
			parms.Add("IdTelefone",idTelefone);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		
		
		#region Properties
		
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}

		public override void SetProperty(string name, object value)
		{
			if(this.Row == null) this.AddNew();
			
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value.GetType().ToString() == "System.String")
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "IdTelefone": this.str.IdTelefone = (string)value; break;							
						case "IdPessoa": this.str.IdPessoa = (string)value; break;							
						case "Ddi": this.str.Ddi = (string)value; break;							
						case "Ddd": this.str.Ddd = (string)value; break;							
						case "Numero": this.str.Numero = (string)value; break;							
						case "Ramal": this.str.Ramal = (string)value; break;							
						case "TipoTelefone": this.str.TipoTelefone = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "IdTelefone":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdTelefone = (System.Int32?)value;
							break;
						
						case "IdPessoa":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdPessoa = (System.Int32?)value;
							break;
						
						case "TipoTelefone":
						
							if (value == null || value.GetType().ToString() == "System.Byte")
								this.TipoTelefone = (System.Byte?)value;
							break;
					

						default:
							break;
					}
				}
			}
			else if(this.Row.Table.Columns.Contains(name))
			{
				this.Row[name] = value;
			}
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}
		
		
		/// <summary>
		/// Maps to PessoaTelefone.IdTelefone
		/// </summary>
		virtual public System.Int32? IdTelefone
		{
			get
			{
				return base.GetSystemInt32(PessoaTelefoneMetadata.ColumnNames.IdTelefone);
			}
			
			set
			{
				base.SetSystemInt32(PessoaTelefoneMetadata.ColumnNames.IdTelefone, value);
			}
		}
		
		/// <summary>
		/// Maps to PessoaTelefone.IdPessoa
		/// </summary>
		virtual public System.Int32? IdPessoa
		{
			get
			{
				return base.GetSystemInt32(PessoaTelefoneMetadata.ColumnNames.IdPessoa);
			}
			
			set
			{
				if(base.SetSystemInt32(PessoaTelefoneMetadata.ColumnNames.IdPessoa, value))
				{
					this._UpToPessoaByIdPessoa = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to PessoaTelefone.DDI
		/// </summary>
		virtual public System.String Ddi
		{
			get
			{
				return base.GetSystemString(PessoaTelefoneMetadata.ColumnNames.Ddi);
			}
			
			set
			{
				base.SetSystemString(PessoaTelefoneMetadata.ColumnNames.Ddi, value);
			}
		}
		
		/// <summary>
		/// Maps to PessoaTelefone.DDD
		/// </summary>
		virtual public System.String Ddd
		{
			get
			{
				return base.GetSystemString(PessoaTelefoneMetadata.ColumnNames.Ddd);
			}
			
			set
			{
				base.SetSystemString(PessoaTelefoneMetadata.ColumnNames.Ddd, value);
			}
		}
		
		/// <summary>
		/// Maps to PessoaTelefone.Numero
		/// </summary>
		virtual public System.String Numero
		{
			get
			{
				return base.GetSystemString(PessoaTelefoneMetadata.ColumnNames.Numero);
			}
			
			set
			{
				base.SetSystemString(PessoaTelefoneMetadata.ColumnNames.Numero, value);
			}
		}
		
		/// <summary>
		/// Maps to PessoaTelefone.Ramal
		/// </summary>
		virtual public System.String Ramal
		{
			get
			{
				return base.GetSystemString(PessoaTelefoneMetadata.ColumnNames.Ramal);
			}
			
			set
			{
				base.SetSystemString(PessoaTelefoneMetadata.ColumnNames.Ramal, value);
			}
		}
		
		/// <summary>
		/// Maps to PessoaTelefone.TipoTelefone
		/// </summary>
		virtual public System.Byte? TipoTelefone
		{
			get
			{
				return base.GetSystemByte(PessoaTelefoneMetadata.ColumnNames.TipoTelefone);
			}
			
			set
			{
				base.SetSystemByte(PessoaTelefoneMetadata.ColumnNames.TipoTelefone, value);
			}
		}
		
		[CLSCompliant(false)]
		internal protected Pessoa _UpToPessoaByIdPessoa;
		#endregion	

		#region String Properties


		[BrowsableAttribute( false )]
		public esStrings str
		{
			get
			{
				if (esstrings == null)
				{
					esstrings = new esStrings(this);
				}
				return esstrings;
			}
		}


		[Serializable]
		sealed public class esStrings
		{
			public esStrings(esPessoaTelefone entity)
			{
				this.entity = entity;
			}
			
	
			public System.String IdTelefone
			{
				get
				{
					System.Int32? data = entity.IdTelefone;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdTelefone = null;
					else entity.IdTelefone = Convert.ToInt32(value);
				}
			}
				
			public System.String IdPessoa
			{
				get
				{
					System.Int32? data = entity.IdPessoa;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdPessoa = null;
					else entity.IdPessoa = Convert.ToInt32(value);
				}
			}
				
			public System.String Ddi
			{
				get
				{
					System.String data = entity.Ddi;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Ddi = null;
					else entity.Ddi = Convert.ToString(value);
				}
			}
				
			public System.String Ddd
			{
				get
				{
					System.String data = entity.Ddd;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Ddd = null;
					else entity.Ddd = Convert.ToString(value);
				}
			}
				
			public System.String Numero
			{
				get
				{
					System.String data = entity.Numero;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Numero = null;
					else entity.Numero = Convert.ToString(value);
				}
			}
				
			public System.String Ramal
			{
				get
				{
					System.String data = entity.Ramal;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Ramal = null;
					else entity.Ramal = Convert.ToString(value);
				}
			}
				
			public System.String TipoTelefone
			{
				get
				{
					System.Byte? data = entity.TipoTelefone;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.TipoTelefone = null;
					else entity.TipoTelefone = Convert.ToByte(value);
				}
			}
			

			private esPessoaTelefone entity;
		}
		#endregion

		#region Query Logic
		protected void InitQuery(esPessoaTelefoneQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntity)this).Connection;
		}

		[System.Diagnostics.DebuggerNonUserCode]
		protected bool OnQueryLoaded(DataTable table)
		{
			bool dataFound = this.PopulateEntity(table);

			if (this.RowCount > 1)
			{
				throw new Exception("esPessoaTelefone can only hold one record of data");
			}

			return dataFound;
		}
		#endregion
		
		[NonSerialized]
		private esStrings esstrings;
	}


	
	public partial class PessoaTelefone : esPessoaTelefone
	{

				
		#region UpToPessoaByIdPessoa - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - FK_PessoaTelefone_Pessoa
		/// </summary>

		[XmlIgnore]
		public Pessoa UpToPessoaByIdPessoa
		{
			get
			{
				if(this._UpToPessoaByIdPessoa == null
					&& IdPessoa != null					)
				{
					this._UpToPessoaByIdPessoa = new Pessoa();
					this._UpToPessoaByIdPessoa.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToPessoaByIdPessoa", this._UpToPessoaByIdPessoa);
					this._UpToPessoaByIdPessoa.Query.Where(this._UpToPessoaByIdPessoa.Query.IdPessoa == this.IdPessoa);
					this._UpToPessoaByIdPessoa.Query.Load();
				}

				return this._UpToPessoaByIdPessoa;
			}
			
			set
			{
				this.RemovePreSave("UpToPessoaByIdPessoa");
				

				if(value == null)
				{
					this.IdPessoa = null;
					this._UpToPessoaByIdPessoa = null;
				}
				else
				{
					this.IdPessoa = value.IdPessoa;
					this._UpToPessoaByIdPessoa = value;
					this.SetPreSave("UpToPessoaByIdPessoa", this._UpToPessoaByIdPessoa);
				}
				
			}
		}
		#endregion
		

		
		/// <summary>
		/// Used internally by the entity's hierarchical properties.
		/// </summary>
		protected override List<esPropertyDescriptor> GetHierarchicalProperties()
		{
			List<esPropertyDescriptor> props = new List<esPropertyDescriptor>();
			
		
			return props;
		}	
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PreSave.
		/// </summary>
		protected override void ApplyPreSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostSave.
		/// </summary>
		protected override void ApplyPostSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostOneToOneSave.
		/// </summary>
		protected override void ApplyPostOneSaveKeys()
		{
		}
		
	}



	[Serializable]
	abstract public class esPessoaTelefoneQuery : esDynamicQuery
	{
		override protected IMetadata Meta
		{
			get
			{
				return PessoaTelefoneMetadata.Meta();
			}
		}	
		

		public esQueryItem IdTelefone
		{
			get
			{
				return new esQueryItem(this, PessoaTelefoneMetadata.ColumnNames.IdTelefone, esSystemType.Int32);
			}
		} 
		
		public esQueryItem IdPessoa
		{
			get
			{
				return new esQueryItem(this, PessoaTelefoneMetadata.ColumnNames.IdPessoa, esSystemType.Int32);
			}
		} 
		
		public esQueryItem Ddi
		{
			get
			{
				return new esQueryItem(this, PessoaTelefoneMetadata.ColumnNames.Ddi, esSystemType.String);
			}
		} 
		
		public esQueryItem Ddd
		{
			get
			{
				return new esQueryItem(this, PessoaTelefoneMetadata.ColumnNames.Ddd, esSystemType.String);
			}
		} 
		
		public esQueryItem Numero
		{
			get
			{
				return new esQueryItem(this, PessoaTelefoneMetadata.ColumnNames.Numero, esSystemType.String);
			}
		} 
		
		public esQueryItem Ramal
		{
			get
			{
				return new esQueryItem(this, PessoaTelefoneMetadata.ColumnNames.Ramal, esSystemType.String);
			}
		} 
		
		public esQueryItem TipoTelefone
		{
			get
			{
				return new esQueryItem(this, PessoaTelefoneMetadata.ColumnNames.TipoTelefone, esSystemType.Byte);
			}
		} 
		
	}



	[Serializable]
	[XmlType("PessoaTelefoneCollection")]
	public partial class PessoaTelefoneCollection : esPessoaTelefoneCollection, IEnumerable<PessoaTelefone>
	{
		public PessoaTelefoneCollection()
		{

		}
		
		public static implicit operator List<PessoaTelefone>(PessoaTelefoneCollection coll)
		{
			List<PessoaTelefone> list = new List<PessoaTelefone>();
			
			foreach (PessoaTelefone emp in coll)
			{
				list.Add(emp);
			}
			
			return list;
		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return  PessoaTelefoneMetadata.Meta();
			}
		}
		
		
		
		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new PessoaTelefoneQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		
		override protected esEntity CreateEntityForCollection(DataRow row)
		{
			return new PessoaTelefone(row);
		}

		override protected esEntity CreateEntity()
		{
			return new PessoaTelefone();
		}
		
		
		#endregion


		[BrowsableAttribute( false )]
		public PessoaTelefoneQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new PessoaTelefoneQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}

		public bool Load(PessoaTelefoneQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		public PessoaTelefone AddNew()
		{
			PessoaTelefone entity = base.AddNewEntity() as PessoaTelefone;
			
			return entity;
		}

		public PessoaTelefone FindByPrimaryKey(System.Int32 idTelefone)
		{
			return base.FindByPrimaryKey(idTelefone) as PessoaTelefone;
		}


		#region IEnumerable<PessoaTelefone> Members

		IEnumerator<PessoaTelefone> IEnumerable<PessoaTelefone>.GetEnumerator()
		{
			System.Collections.IEnumerable enumer = this as System.Collections.IEnumerable;
			System.Collections.IEnumerator iterator = enumer.GetEnumerator();

			while(iterator.MoveNext())
			{
				yield return iterator.Current as PessoaTelefone;
			}
		}

		#endregion
		
		private PessoaTelefoneQuery query;
	}


	/// <summary>
	/// Encapsulates the 'PessoaTelefone' table
	/// </summary>

	[Serializable]
	public partial class PessoaTelefone : esPessoaTelefone
	{
		public PessoaTelefone()
		{

		}
	
		public PessoaTelefone(DataRow row)
			: base(row)
		{

		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return PessoaTelefoneMetadata.Meta();
			}
		}
		
		
		
		override protected esPessoaTelefoneQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new PessoaTelefoneQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		#endregion
		



		[BrowsableAttribute( false )]
		public PessoaTelefoneQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new PessoaTelefoneQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}
		

		public bool Load(PessoaTelefoneQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		private PessoaTelefoneQuery query;
	}



	[Serializable]
	public partial class PessoaTelefoneQuery : esPessoaTelefoneQuery
	{
		public PessoaTelefoneQuery()
		{

		}		
		
		public PessoaTelefoneQuery(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
			
	}



	[Serializable]
	public partial class PessoaTelefoneMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected PessoaTelefoneMetadata()
		{
			_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(PessoaTelefoneMetadata.ColumnNames.IdTelefone, 0, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = PessoaTelefoneMetadata.PropertyNames.IdTelefone;
			c.IsInPrimaryKey = true;
			c.IsAutoIncrement = true;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PessoaTelefoneMetadata.ColumnNames.IdPessoa, 1, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = PessoaTelefoneMetadata.PropertyNames.IdPessoa;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PessoaTelefoneMetadata.ColumnNames.Ddi, 2, typeof(System.String), esSystemType.String);
			c.PropertyName = PessoaTelefoneMetadata.PropertyNames.Ddi;
			c.CharacterMaxLength = 5;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PessoaTelefoneMetadata.ColumnNames.Ddd, 3, typeof(System.String), esSystemType.String);
			c.PropertyName = PessoaTelefoneMetadata.PropertyNames.Ddd;
			c.CharacterMaxLength = 5;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PessoaTelefoneMetadata.ColumnNames.Numero, 4, typeof(System.String), esSystemType.String);
			c.PropertyName = PessoaTelefoneMetadata.PropertyNames.Numero;
			c.CharacterMaxLength = 20;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PessoaTelefoneMetadata.ColumnNames.Ramal, 5, typeof(System.String), esSystemType.String);
			c.PropertyName = PessoaTelefoneMetadata.PropertyNames.Ramal;
			c.CharacterMaxLength = 5;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PessoaTelefoneMetadata.ColumnNames.TipoTelefone, 6, typeof(System.Byte), esSystemType.Byte);
			c.PropertyName = PessoaTelefoneMetadata.PropertyNames.TipoTelefone;	
			c.NumericPrecision = 3;
			_columns.Add(c); 
			
				
		}
		#endregion
	
		static public PessoaTelefoneMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base._dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base._columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string IdTelefone = "IdTelefone";
			 public const string IdPessoa = "IdPessoa";
			 public const string Ddi = "DDI";
			 public const string Ddd = "DDD";
			 public const string Numero = "Numero";
			 public const string Ramal = "Ramal";
			 public const string TipoTelefone = "TipoTelefone";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string IdTelefone = "IdTelefone";
			 public const string IdPessoa = "IdPessoa";
			 public const string Ddi = "Ddi";
			 public const string Ddd = "Ddd";
			 public const string Numero = "Numero";
			 public const string Ramal = "Ramal";
			 public const string TipoTelefone = "TipoTelefone";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(PessoaTelefoneMetadata))
			{
				if(PessoaTelefoneMetadata.mapDelegates == null)
				{
					PessoaTelefoneMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (PessoaTelefoneMetadata.meta == null)
				{
					PessoaTelefoneMetadata.meta = new PessoaTelefoneMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();
				

				meta.AddTypeMap("IdTelefone", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("IdPessoa", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("DDI", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("DDD", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("Numero", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("Ramal", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("TipoTelefone", new esTypeMap("tinyint", "System.Byte"));			
				
				
				
				meta.Source = "PessoaTelefone";
				meta.Destination = "PessoaTelefone";
				
				meta.spInsert = "proc_PessoaTelefoneInsert";				
				meta.spUpdate = "proc_PessoaTelefoneUpdate";		
				meta.spDelete = "proc_PessoaTelefoneDelete";
				meta.spLoadAll = "proc_PessoaTelefoneLoadAll";
				meta.spLoadByPrimaryKey = "proc_PessoaTelefoneLoadByPrimaryKey";
				
				this._providerMetadataMaps["esDefault"] = meta;
			}
			
			return this._providerMetadataMaps["esDefault"];
		}

		#endregion

		static private PessoaTelefoneMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
