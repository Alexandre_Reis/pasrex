/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 07/01/2016 17:11:20
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;


using EntitySpaces.Interfaces;
using EntitySpaces.Core;
using Financial.Investidor;
using Financial.InvestidorCotista;
using Financial.Captacao;



namespace Financial.CRM
{

	[Serializable]
	abstract public class esPessoaCollection : esEntityCollection
	{
		public esPessoaCollection()
		{

		}

		protected override string GetCollectionName()
		{
			return "PessoaCollection";
		}

		#region Query Logic
		protected void InitQuery(esPessoaQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntityCollection)this).Connection;
		}

		protected bool OnQueryLoaded(DataTable table)
		{
			this.PopulateCollection(table);
			return (this.RowCount > 0) ? true : false;
		}
		
		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery(query as esPessoaQuery);
		}
		#endregion
		
		virtual public Pessoa DetachEntity(Pessoa entity)
		{
			return base.DetachEntity(entity) as Pessoa;
		}
		
		virtual public Pessoa AttachEntity(Pessoa entity)
		{
			return base.AttachEntity(entity) as Pessoa;
		}
		
		virtual public void Combine(PessoaCollection collection)
		{
			base.Combine(collection);
		}
		
		new public Pessoa this[int index]
		{
			get
			{
				return base[index] as Pessoa;
			}
		}

		public override Type GetEntityType()
		{
			return typeof(Pessoa);
		}
	}



	[Serializable]
	abstract public class esPessoa : esEntity
	{
		/// <summary>
		/// Used internally by the entity's DynamicQuery mechanism.
		/// </summary>
		virtual protected esPessoaQuery GetDynamicQuery()
		{
			return null;
		}

		public esPessoa()
		{

		}

		public esPessoa(DataRow row)
			: base(row)
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.Int32 idPessoa)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idPessoa);
			else
				return LoadByPrimaryKeyStoredProcedure(idPessoa);
		}

		/// <summary>
		/// Loads an entity by primary key
		/// </summary>
		/// <remarks>
		/// EntitySpaces requires primary keys be defined on all tables.
		/// If a table does not have a primary key set,
		/// this method will not compile.
		/// Does not support sqlAcessType. It only works with DynamicQuery
		/// </remarks>
		/// <param name="fieldsToReturn">Fields desired</param>
		public virtual bool LoadByPrimaryKey(List<esQueryItem> fieldsToReturn, System.Int32 idPessoa)
		{
			esQueryItem[] fields = fieldsToReturn.ToArray();			
			esPessoaQuery query = this.GetDynamicQuery();
			query
			    .Select(fields)
			    .Where(query.IdPessoa == idPessoa);
		
			return query.Load();
		}
		
		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.Int32 idPessoa)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idPessoa);
			else
				return LoadByPrimaryKeyStoredProcedure(idPessoa);
		}

		private bool LoadByPrimaryKeyDynamic(System.Int32 idPessoa)
		{
			esPessoaQuery query = this.GetDynamicQuery();
			query.Where(query.IdPessoa == idPessoa);
			return query.Load();
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.Int32 idPessoa)
		{
			esParameters parms = new esParameters();
			parms.Add("IdPessoa",idPessoa);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		
		
		#region Properties
		
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}

		public override void SetProperty(string name, object value)
		{
			if(this.Row == null) this.AddNew();
			
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value.GetType().ToString() == "System.String")
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "IdPessoa": this.str.IdPessoa = (string)value; break;							
						case "Nome": this.str.Nome = (string)value; break;							
						case "Apelido": this.str.Apelido = (string)value; break;							
						case "Tipo": this.str.Tipo = (string)value; break;							
						case "DataCadatro": this.str.DataCadatro = (string)value; break;							
						case "DataUltimaAlteracao": this.str.DataUltimaAlteracao = (string)value; break;							
						case "Cpfcnpj": this.str.Cpfcnpj = (string)value; break;							
						case "IdAgenteDistribuidor": this.str.IdAgenteDistribuidor = (string)value; break;							
						case "EstadoCivil": this.str.EstadoCivil = (string)value; break;							
						case "Sexo": this.str.Sexo = (string)value; break;							
						case "DataNascimento": this.str.DataNascimento = (string)value; break;							
						case "Profissao": this.str.Profissao = (string)value; break;							
						case "CodigoInterface": this.str.CodigoInterface = (string)value; break;							
						case "PessoaPoliticamenteExposta": this.str.PessoaPoliticamenteExposta = (string)value; break;							
						case "FiliacaoNomePai": this.str.FiliacaoNomePai = (string)value; break;							
						case "FiliacaoNomeMae": this.str.FiliacaoNomeMae = (string)value; break;							
						case "UFNaturalidade": this.str.UFNaturalidade = (string)value; break;							
						case "IdPerfilInvestidor": this.str.IdPerfilInvestidor = (string)value; break;							
						case "SituacaoLegal": this.str.SituacaoLegal = (string)value; break;							
						case "PessoaVinculada": this.str.PessoaVinculada = (string)value; break;							
						case "NaturezaJuridica": this.str.NaturezaJuridica = (string)value; break;							
						case "TipoRenda": this.str.TipoRenda = (string)value; break;							
						case "IndicadorFatca": this.str.IndicadorFatca = (string)value; break;							
						case "GiinTin": this.str.GiinTin = (string)value; break;							
						case "JustificativaGiin": this.str.JustificativaGiin = (string)value; break;							
						case "CnaeDivisao": this.str.CnaeDivisao = (string)value; break;							
						case "CnaeGrupo": this.str.CnaeGrupo = (string)value; break;							
						case "CnaeClasse": this.str.CnaeClasse = (string)value; break;							
						case "CnaeSubClasse": this.str.CnaeSubClasse = (string)value; break;							
						case "Nacionalidade": this.str.Nacionalidade = (string)value; break;
                        case "OffShore": this.str.OffShore = (string)value; break;
                        case "DataVencimentoCadastro": this.str.DataVencimentoCadastro = (string)value; break;
                        case "AlertaCadastro": this.str.AlertaCadastro = (string)value; break;
                        case "EmailAlerta": this.str.EmailAlerta = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "IdPessoa":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdPessoa = (System.Int32?)value;
							break;
						
						case "Tipo":
						
							if (value == null || value.GetType().ToString() == "System.Byte")
								this.Tipo = (System.Byte?)value;
							break;
						
						case "DataCadatro":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.DataCadatro = (System.DateTime?)value;
							break;
						
						case "DataUltimaAlteracao":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.DataUltimaAlteracao = (System.DateTime?)value;
							break;
						
						case "IdAgenteDistribuidor":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdAgenteDistribuidor = (System.Int32?)value;
							break;
						
						case "EstadoCivil":
						
							if (value == null || value.GetType().ToString() == "System.Byte")
								this.EstadoCivil = (System.Byte?)value;
							break;
						
						case "DataNascimento":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.DataNascimento = (System.DateTime?)value;
							break;
						
						case "IdPerfilInvestidor":
						
							if (value == null || value.GetType().ToString() == "System.Byte")
								this.IdPerfilInvestidor = (System.Byte?)value;
							break;
						
						case "SituacaoLegal":
						
							if (value == null || value.GetType().ToString() == "System.Byte")
								this.SituacaoLegal = (System.Byte?)value;
							break;
						
						case "NaturezaJuridica":
						
							if (value == null || value.GetType().ToString() == "System.Int16")
								this.NaturezaJuridica = (System.Int16?)value;
							break;
						
						case "CnaeDivisao":
						
							if (value == null || value.GetType().ToString() == "System.Byte")
								this.CnaeDivisao = (System.Byte?)value;
							break;
						
						case "CnaeGrupo":
						
							if (value == null || value.GetType().ToString() == "System.Byte")
								this.CnaeGrupo = (System.Byte?)value;
							break;
						
						case "CnaeClasse":
						
							if (value == null || value.GetType().ToString() == "System.Byte")
								this.CnaeClasse = (System.Byte?)value;
							break;
						
						case "CnaeSubClasse":
						
							if (value == null || value.GetType().ToString() == "System.Byte")
								this.CnaeSubClasse = (System.Byte?)value;
							break;
						
						case "Nacionalidade":
						
							if (value == null || value.GetType().ToString() == "System.Int16")
								this.Nacionalidade = (System.Int16?)value;
							break;

                        case "DataVencimentoCadastro":

                            if (value == null || value.GetType().ToString() == "System.DateTime")
                                this.DataVencimentoCadastro = (System.DateTime?)value;
                            break;

						default:
							break;
					}
				}
			}
			else if(this.Row.Table.Columns.Contains(name))
			{
				this.Row[name] = value;
			}
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}
		
		
		/// <summary>
		/// Maps to Pessoa.IdPessoa
		/// </summary>
		virtual public System.Int32? IdPessoa
		{
			get
			{
				return base.GetSystemInt32(PessoaMetadata.ColumnNames.IdPessoa);
			}
			
			set
			{
				base.SetSystemInt32(PessoaMetadata.ColumnNames.IdPessoa, value);
			}
		}
		
		/// <summary>
		/// Maps to Pessoa.Nome
		/// </summary>
		virtual public System.String Nome
		{
			get
			{
				return base.GetSystemString(PessoaMetadata.ColumnNames.Nome);
			}
			
			set
			{
				base.SetSystemString(PessoaMetadata.ColumnNames.Nome, value);
			}
		}
		
		/// <summary>
		/// Maps to Pessoa.Apelido
		/// </summary>
		virtual public System.String Apelido
		{
			get
			{
				return base.GetSystemString(PessoaMetadata.ColumnNames.Apelido);
			}
			
			set
			{
				base.SetSystemString(PessoaMetadata.ColumnNames.Apelido, value);
			}
		}
		
		/// <summary>
		/// Maps to Pessoa.Tipo
		/// </summary>
		virtual public System.Byte? Tipo
		{
			get
			{
				return base.GetSystemByte(PessoaMetadata.ColumnNames.Tipo);
			}
			
			set
			{
				base.SetSystemByte(PessoaMetadata.ColumnNames.Tipo, value);
			}
		}
		
		/// <summary>
		/// Maps to Pessoa.DataCadatro
		/// </summary>
		virtual public System.DateTime? DataCadatro
		{
			get
			{
				return base.GetSystemDateTime(PessoaMetadata.ColumnNames.DataCadatro);
			}
			
			set
			{
				base.SetSystemDateTime(PessoaMetadata.ColumnNames.DataCadatro, value);
			}
		}
		
		/// <summary>
		/// Maps to Pessoa.DataUltimaAlteracao
		/// </summary>
		virtual public System.DateTime? DataUltimaAlteracao
		{
			get
			{
				return base.GetSystemDateTime(PessoaMetadata.ColumnNames.DataUltimaAlteracao);
			}
			
			set
			{
				base.SetSystemDateTime(PessoaMetadata.ColumnNames.DataUltimaAlteracao, value);
			}
		}
		
		/// <summary>
		/// Maps to Pessoa.Cpfcnpj
		/// </summary>
		virtual public System.String Cpfcnpj
		{
			get
			{
				return base.GetSystemString(PessoaMetadata.ColumnNames.Cpfcnpj);
			}
			
			set
			{
				base.SetSystemString(PessoaMetadata.ColumnNames.Cpfcnpj, value);
			}
		}
		
		/// <summary>
		/// Maps to Pessoa.IdAgenteDistribuidor
		/// </summary>
		virtual public System.Int32? IdAgenteDistribuidor
		{
			get
			{
				return base.GetSystemInt32(PessoaMetadata.ColumnNames.IdAgenteDistribuidor);
			}
			
			set
			{
				base.SetSystemInt32(PessoaMetadata.ColumnNames.IdAgenteDistribuidor, value);
			}
		}
		
		/// <summary>
		/// Maps to Pessoa.EstadoCivil
		/// </summary>
		virtual public System.Byte? EstadoCivil
		{
			get
			{
				return base.GetSystemByte(PessoaMetadata.ColumnNames.EstadoCivil);
			}
			
			set
			{
				base.SetSystemByte(PessoaMetadata.ColumnNames.EstadoCivil, value);
			}
		}
		
		/// <summary>
		/// Maps to Pessoa.Sexo
		/// </summary>
		virtual public System.String Sexo
		{
			get
			{
				return base.GetSystemString(PessoaMetadata.ColumnNames.Sexo);
			}
			
			set
			{
				base.SetSystemString(PessoaMetadata.ColumnNames.Sexo, value);
			}
		}
		
		/// <summary>
		/// Maps to Pessoa.DataNascimento
		/// </summary>
		virtual public System.DateTime? DataNascimento
		{
			get
			{
				return base.GetSystemDateTime(PessoaMetadata.ColumnNames.DataNascimento);
			}
			
			set
			{
				base.SetSystemDateTime(PessoaMetadata.ColumnNames.DataNascimento, value);
			}
		}
		
		/// <summary>
		/// Maps to Pessoa.Profissao
		/// </summary>
		virtual public System.String Profissao
		{
			get
			{
				return base.GetSystemString(PessoaMetadata.ColumnNames.Profissao);
			}
			
			set
			{
				base.SetSystemString(PessoaMetadata.ColumnNames.Profissao, value);
			}
		}
		
		/// <summary>
		/// Maps to Pessoa.CodigoInterface
		/// </summary>
		virtual public System.String CodigoInterface
		{
			get
			{
				return base.GetSystemString(PessoaMetadata.ColumnNames.CodigoInterface);
			}
			
			set
			{
				base.SetSystemString(PessoaMetadata.ColumnNames.CodigoInterface, value);
			}
		}
		
		/// <summary>
		/// Maps to Pessoa.PessoaPoliticamenteExposta
		/// </summary>
		virtual public System.String PessoaPoliticamenteExposta
		{
			get
			{
				return base.GetSystemString(PessoaMetadata.ColumnNames.PessoaPoliticamenteExposta);
			}
			
			set
			{
				base.SetSystemString(PessoaMetadata.ColumnNames.PessoaPoliticamenteExposta, value);
			}
		}
		
		/// <summary>
		/// Maps to Pessoa.FiliacaoNomePai
		/// </summary>
		virtual public System.String FiliacaoNomePai
		{
			get
			{
				return base.GetSystemString(PessoaMetadata.ColumnNames.FiliacaoNomePai);
			}
			
			set
			{
				base.SetSystemString(PessoaMetadata.ColumnNames.FiliacaoNomePai, value);
			}
		}
		
		/// <summary>
		/// Maps to Pessoa.FiliacaoNomeMae
		/// </summary>
		virtual public System.String FiliacaoNomeMae
		{
			get
			{
				return base.GetSystemString(PessoaMetadata.ColumnNames.FiliacaoNomeMae);
			}
			
			set
			{
				base.SetSystemString(PessoaMetadata.ColumnNames.FiliacaoNomeMae, value);
			}
		}
		
		/// <summary>
		/// Maps to Pessoa.UFNaturalidade
		/// </summary>
		virtual public System.String UFNaturalidade
		{
			get
			{
				return base.GetSystemString(PessoaMetadata.ColumnNames.UFNaturalidade);
			}
			
			set
			{
				base.SetSystemString(PessoaMetadata.ColumnNames.UFNaturalidade, value);
			}
		}
		
		/// <summary>
		/// Maps to Pessoa.IdPerfilInvestidor
		/// </summary>
		virtual public System.Byte? IdPerfilInvestidor
		{
			get
			{
				return base.GetSystemByte(PessoaMetadata.ColumnNames.IdPerfilInvestidor);
			}
			
			set
			{
				if(base.SetSystemByte(PessoaMetadata.ColumnNames.IdPerfilInvestidor, value))
				{
					this._UpToPerfilInvestidorByIdPerfilInvestidor = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to Pessoa.SituacaoLegal
		/// </summary>
		virtual public System.Byte? SituacaoLegal
		{
			get
			{
				return base.GetSystemByte(PessoaMetadata.ColumnNames.SituacaoLegal);
			}
			
			set
			{
				base.SetSystemByte(PessoaMetadata.ColumnNames.SituacaoLegal, value);
			}
		}
		
		/// <summary>
		/// Maps to Pessoa.PessoaVinculada
		/// </summary>
		virtual public System.String PessoaVinculada
		{
			get
			{
				return base.GetSystemString(PessoaMetadata.ColumnNames.PessoaVinculada);
			}
			
			set
			{
				base.SetSystemString(PessoaMetadata.ColumnNames.PessoaVinculada, value);
			}
		}
		
		/// <summary>
		/// Maps to Pessoa.NaturezaJuridica
		/// </summary>
		virtual public System.Int16? NaturezaJuridica
		{
			get
			{
				return base.GetSystemInt16(PessoaMetadata.ColumnNames.NaturezaJuridica);
			}
			
			set
			{
				base.SetSystemInt16(PessoaMetadata.ColumnNames.NaturezaJuridica, value);
			}
		}
		
		/// <summary>
		/// Maps to Pessoa.TipoRenda
		/// </summary>
		virtual public System.String TipoRenda
		{
			get
			{
				return base.GetSystemString(PessoaMetadata.ColumnNames.TipoRenda);
			}
			
			set
			{
				base.SetSystemString(PessoaMetadata.ColumnNames.TipoRenda, value);
			}
		}
		
		/// <summary>
		/// Maps to Pessoa.IndicadorFatca
		/// </summary>
		virtual public System.String IndicadorFatca
		{
			get
			{
				return base.GetSystemString(PessoaMetadata.ColumnNames.IndicadorFatca);
			}
			
			set
			{
				base.SetSystemString(PessoaMetadata.ColumnNames.IndicadorFatca, value);
			}
		}
		
		/// <summary>
		/// Maps to Pessoa.GiinTin
		/// </summary>
		virtual public System.String GiinTin
		{
			get
			{
				return base.GetSystemString(PessoaMetadata.ColumnNames.GiinTin);
			}
			
			set
			{
				base.SetSystemString(PessoaMetadata.ColumnNames.GiinTin, value);
			}
		}
		
		/// <summary>
		/// Maps to Pessoa.JustificativaGiin
		/// </summary>
		virtual public System.String JustificativaGiin
		{
			get
			{
				return base.GetSystemString(PessoaMetadata.ColumnNames.JustificativaGiin);
			}
			
			set
			{
				base.SetSystemString(PessoaMetadata.ColumnNames.JustificativaGiin, value);
			}
		}
		
		/// <summary>
		/// Maps to Pessoa.CnaeDivisao
		/// </summary>
		virtual public System.Byte? CnaeDivisao
		{
			get
			{
				return base.GetSystemByte(PessoaMetadata.ColumnNames.CnaeDivisao);
			}
			
			set
			{
				base.SetSystemByte(PessoaMetadata.ColumnNames.CnaeDivisao, value);
			}
		}
		
		/// <summary>
		/// Maps to Pessoa.CnaeGrupo
		/// </summary>
		virtual public System.Byte? CnaeGrupo
		{
			get
			{
				return base.GetSystemByte(PessoaMetadata.ColumnNames.CnaeGrupo);
			}
			
			set
			{
				base.SetSystemByte(PessoaMetadata.ColumnNames.CnaeGrupo, value);
			}
		}
		
		/// <summary>
		/// Maps to Pessoa.CnaeClasse
		/// </summary>
		virtual public System.Byte? CnaeClasse
		{
			get
			{
				return base.GetSystemByte(PessoaMetadata.ColumnNames.CnaeClasse);
			}
			
			set
			{
				base.SetSystemByte(PessoaMetadata.ColumnNames.CnaeClasse, value);
			}
		}
		
		/// <summary>
		/// Maps to Pessoa.CnaeSubClasse
		/// </summary>
		virtual public System.Byte? CnaeSubClasse
		{
			get
			{
				return base.GetSystemByte(PessoaMetadata.ColumnNames.CnaeSubClasse);
			}
			
			set
			{
				base.SetSystemByte(PessoaMetadata.ColumnNames.CnaeSubClasse, value);
			}
		}
		
		/// <summary>
		/// Maps to Pessoa.Nacionalidade
		/// </summary>
		virtual public System.Int16? Nacionalidade
		{
			get
			{
				return base.GetSystemInt16(PessoaMetadata.ColumnNames.Nacionalidade);
			}
			
			set
			{
				base.SetSystemInt16(PessoaMetadata.ColumnNames.Nacionalidade, value);
			}
		}
		
		/// <summary>
		/// Maps to Pessoa.OffShore
		/// </summary>
		virtual public System.String OffShore
		{
			get
			{
				return base.GetSystemString(PessoaMetadata.ColumnNames.OffShore);
			}
			
			set
			{
				base.SetSystemString(PessoaMetadata.ColumnNames.OffShore, value);
			}
		}
		
        /// <summary>
        /// Maps to Pessoa.DataVencimentoCadastro
        /// </summary>
        virtual public System.DateTime? DataVencimentoCadastro
        {
            get
            {
                return base.GetSystemDateTime(PessoaMetadata.ColumnNames.DataVencimentoCadastro);
            }

            set
            {
                base.SetSystemDateTime(PessoaMetadata.ColumnNames.DataVencimentoCadastro, value);
            }
        }
        /// <summary>
        /// Maps to Pessoa.AlertaCadastro
        /// </summary>
        virtual public System.String AlertaCadastro
        {
            get
            {
                return base.GetSystemString(PessoaMetadata.ColumnNames.AlertaCadastro);
            }

            set
            {
                base.SetSystemString(PessoaMetadata.ColumnNames.AlertaCadastro, value);
            }
        }

        /// <summary>
        /// Maps to Pessoa.EmailAlerta
        /// </summary>
        virtual public System.String EmailAlerta
        {
            get
            {
                return base.GetSystemString(PessoaMetadata.ColumnNames.EmailAlerta);
            }

            set
            {
                base.SetSystemString(PessoaMetadata.ColumnNames.EmailAlerta, value);
            }
        }
		[CLSCompliant(false)]
		internal protected PerfilInvestidor _UpToPerfilInvestidorByIdPerfilInvestidor;
		#endregion	

		#region String Properties


		[BrowsableAttribute( false )]
		public esStrings str
		{
			get
			{
				if (esstrings == null)
				{
					esstrings = new esStrings(this);
				}
				return esstrings;
			}
		}


		[Serializable]
		sealed public class esStrings
		{
			public esStrings(esPessoa entity)
			{
				this.entity = entity;
			}
			
	
			public System.String IdPessoa
			{
				get
				{
					System.Int32? data = entity.IdPessoa;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdPessoa = null;
					else entity.IdPessoa = Convert.ToInt32(value);
				}
			}
				
			public System.String Nome
			{
				get
				{
					System.String data = entity.Nome;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Nome = null;
					else entity.Nome = Convert.ToString(value);
				}
			}
				
			public System.String Apelido
			{
				get
				{
					System.String data = entity.Apelido;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Apelido = null;
					else entity.Apelido = Convert.ToString(value);
				}
			}
				
			public System.String Tipo
			{
				get
				{
					System.Byte? data = entity.Tipo;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Tipo = null;
					else entity.Tipo = Convert.ToByte(value);
				}
			}
				
			public System.String DataCadatro
			{
				get
				{
					System.DateTime? data = entity.DataCadatro;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DataCadatro = null;
					else entity.DataCadatro = Convert.ToDateTime(value);
				}
			}
				
			public System.String DataUltimaAlteracao
			{
				get
				{
					System.DateTime? data = entity.DataUltimaAlteracao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DataUltimaAlteracao = null;
					else entity.DataUltimaAlteracao = Convert.ToDateTime(value);
				}
			}
				
			public System.String Cpfcnpj
			{
				get
				{
					System.String data = entity.Cpfcnpj;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Cpfcnpj = null;
					else entity.Cpfcnpj = Convert.ToString(value);
				}
			}
				
			public System.String IdAgenteDistribuidor
			{
				get
				{
					System.Int32? data = entity.IdAgenteDistribuidor;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdAgenteDistribuidor = null;
					else entity.IdAgenteDistribuidor = Convert.ToInt32(value);
				}
			}
				
			public System.String EstadoCivil
			{
				get
				{
					System.Byte? data = entity.EstadoCivil;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.EstadoCivil = null;
					else entity.EstadoCivil = Convert.ToByte(value);
				}
			}
				
			public System.String Sexo
			{
				get
				{
					System.String data = entity.Sexo;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Sexo = null;
					else entity.Sexo = Convert.ToString(value);
				}
			}
				
			public System.String DataNascimento
			{
				get
				{
					System.DateTime? data = entity.DataNascimento;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DataNascimento = null;
					else entity.DataNascimento = Convert.ToDateTime(value);
				}
			}
				
			public System.String Profissao
			{
				get
				{
					System.String data = entity.Profissao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Profissao = null;
					else entity.Profissao = Convert.ToString(value);
				}
			}
				
			public System.String CodigoInterface
			{
				get
				{
					System.String data = entity.CodigoInterface;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.CodigoInterface = null;
					else entity.CodigoInterface = Convert.ToString(value);
				}
			}
				
			public System.String PessoaPoliticamenteExposta
			{
				get
				{
					System.String data = entity.PessoaPoliticamenteExposta;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.PessoaPoliticamenteExposta = null;
					else entity.PessoaPoliticamenteExposta = Convert.ToString(value);
				}
			}
				
			public System.String FiliacaoNomePai
			{
				get
				{
					System.String data = entity.FiliacaoNomePai;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FiliacaoNomePai = null;
					else entity.FiliacaoNomePai = Convert.ToString(value);
				}
			}
				
			public System.String FiliacaoNomeMae
			{
				get
				{
					System.String data = entity.FiliacaoNomeMae;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FiliacaoNomeMae = null;
					else entity.FiliacaoNomeMae = Convert.ToString(value);
				}
			}
				
			public System.String UFNaturalidade
			{
				get
				{
					System.String data = entity.UFNaturalidade;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.UFNaturalidade = null;
					else entity.UFNaturalidade = Convert.ToString(value);
				}
			}
				
			public System.String IdPerfilInvestidor
			{
				get
				{
					System.Byte? data = entity.IdPerfilInvestidor;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdPerfilInvestidor = null;
					else entity.IdPerfilInvestidor = Convert.ToByte(value);
				}
			}
				
			public System.String SituacaoLegal
			{
				get
				{
					System.Byte? data = entity.SituacaoLegal;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.SituacaoLegal = null;
					else entity.SituacaoLegal = Convert.ToByte(value);
				}
			}
				
			public System.String PessoaVinculada
			{
				get
				{
					System.String data = entity.PessoaVinculada;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.PessoaVinculada = null;
					else entity.PessoaVinculada = Convert.ToString(value);
				}
			}
				
			public System.String NaturezaJuridica
			{
				get
				{
					System.Int16? data = entity.NaturezaJuridica;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.NaturezaJuridica = null;
					else entity.NaturezaJuridica = Convert.ToInt16(value);
				}
			}
				
			public System.String TipoRenda
			{
				get
				{
					System.String data = entity.TipoRenda;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.TipoRenda = null;
					else entity.TipoRenda = Convert.ToString(value);
				}
			}
				
			public System.String IndicadorFatca
			{
				get
				{
					System.String data = entity.IndicadorFatca;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IndicadorFatca = null;
					else entity.IndicadorFatca = Convert.ToString(value);
				}
			}
				
			public System.String GiinTin
			{
				get
				{
					System.String data = entity.GiinTin;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.GiinTin = null;
					else entity.GiinTin = Convert.ToString(value);
				}
			}
				
			public System.String JustificativaGiin
			{
				get
				{
					System.String data = entity.JustificativaGiin;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.JustificativaGiin = null;
					else entity.JustificativaGiin = Convert.ToString(value);
				}
			}
				
			public System.String CnaeDivisao
			{
				get
				{
					System.Byte? data = entity.CnaeDivisao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.CnaeDivisao = null;
					else entity.CnaeDivisao = Convert.ToByte(value);
				}
			}
				
			public System.String CnaeGrupo
			{
				get
				{
					System.Byte? data = entity.CnaeGrupo;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.CnaeGrupo = null;
					else entity.CnaeGrupo = Convert.ToByte(value);
				}
			}
				
			public System.String CnaeClasse
			{
				get
				{
					System.Byte? data = entity.CnaeClasse;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.CnaeClasse = null;
					else entity.CnaeClasse = Convert.ToByte(value);
				}
			}
				
			public System.String CnaeSubClasse
			{
				get
				{
					System.Byte? data = entity.CnaeSubClasse;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.CnaeSubClasse = null;
					else entity.CnaeSubClasse = Convert.ToByte(value);
				}
			}
				
			public System.String Nacionalidade
			{
				get
				{
					System.Int16? data = entity.Nacionalidade;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Nacionalidade = null;
					else entity.Nacionalidade = Convert.ToInt16(value);
				}
			}
				
			public System.String OffShore
			{
				get
				{
					System.String data = entity.OffShore;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.OffShore = null;
					else entity.OffShore = Convert.ToString(value);
				}
			}
			
            public System.String DataVencimentoCadastro
            {
                get
                {
                    System.DateTime? data = entity.DataVencimentoCadastro;
                    return (data == null) ? String.Empty : Convert.ToString(data);
                }

                set
                {
                    if (value == null || value.Length == 0) entity.DataVencimentoCadastro = null;
                    else entity.DataVencimentoCadastro = Convert.ToDateTime(value);
                }
            }
            public System.String AlertaCadastro
            {
                get
                {
                    System.String data = entity.AlertaCadastro;
                    return (data == null) ? String.Empty : Convert.ToString(data);

                }



                set
                {
                    if (value == null || value.Length == 0) entity.AlertaCadastro = null;
                    else entity.AlertaCadastro = Convert.ToString(value);


                }
            }

            public System.String EmailAlerta
            {
                get
                {
                    System.String data = entity.EmailAlerta;
                    return (data == null) ? String.Empty : Convert.ToString(data);
                }

                set
                {
                    if (value == null || value.Length == 0) entity.EmailAlerta = null;
                    else entity.EmailAlerta = Convert.ToString(value);
                }
            }

			private esPessoa entity;
		}
		#endregion

		#region Query Logic
		protected void InitQuery(esPessoaQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntity)this).Connection;
		}

		[System.Diagnostics.DebuggerNonUserCode]
		protected bool OnQueryLoaded(DataTable table)
		{
			bool dataFound = this.PopulateEntity(table);

			if (this.RowCount > 1)
			{
				throw new Exception("esPessoa can only hold one record of data");
			}

			return dataFound;
		}
		#endregion
		
		[NonSerialized]
		private esStrings esstrings;
	}


	
	public partial class Pessoa : esPessoa
	{

				
		#region ClienteCollectionByIdPessoa - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - Cliente_IdPessoa_FK1
		/// </summary>

		[XmlIgnore]
		public ClienteCollection ClienteCollectionByIdPessoa
		{
			get
			{
				if(this._ClienteCollectionByIdPessoa == null)
				{
					this._ClienteCollectionByIdPessoa = new ClienteCollection();
					this._ClienteCollectionByIdPessoa.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("ClienteCollectionByIdPessoa", this._ClienteCollectionByIdPessoa);
				
					if(this.IdPessoa != null)
					{
						this._ClienteCollectionByIdPessoa.Query.Where(this._ClienteCollectionByIdPessoa.Query.IdPessoa == this.IdPessoa);
						this._ClienteCollectionByIdPessoa.Query.Load();

						// Auto-hookup Foreign Keys
						this._ClienteCollectionByIdPessoa.fks.Add(ClienteMetadata.ColumnNames.IdPessoa, this.IdPessoa);
					}
				}

				return this._ClienteCollectionByIdPessoa;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._ClienteCollectionByIdPessoa != null) 
				{ 
					this.RemovePostSave("ClienteCollectionByIdPessoa"); 
					this._ClienteCollectionByIdPessoa = null;
					
				} 
			} 			
		}

		private ClienteCollection _ClienteCollectionByIdPessoa;
		#endregion

				
		#region ContaCorrenteCollectionByIdPessoa - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - Pessoa_ContaCorrente_FK1
		/// </summary>

		[XmlIgnore]
		public ContaCorrenteCollection ContaCorrenteCollectionByIdPessoa
		{
			get
			{
				if(this._ContaCorrenteCollectionByIdPessoa == null)
				{
					this._ContaCorrenteCollectionByIdPessoa = new ContaCorrenteCollection();
					this._ContaCorrenteCollectionByIdPessoa.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("ContaCorrenteCollectionByIdPessoa", this._ContaCorrenteCollectionByIdPessoa);
				
					if(this.IdPessoa != null)
					{
						this._ContaCorrenteCollectionByIdPessoa.Query.Where(this._ContaCorrenteCollectionByIdPessoa.Query.IdPessoa == this.IdPessoa);
						this._ContaCorrenteCollectionByIdPessoa.Query.Load();

						// Auto-hookup Foreign Keys
						this._ContaCorrenteCollectionByIdPessoa.fks.Add(ContaCorrenteMetadata.ColumnNames.IdPessoa, this.IdPessoa);
					}
				}

				return this._ContaCorrenteCollectionByIdPessoa;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._ContaCorrenteCollectionByIdPessoa != null) 
				{ 
					this.RemovePostSave("ContaCorrenteCollectionByIdPessoa"); 
					this._ContaCorrenteCollectionByIdPessoa = null;
					
				} 
			} 			
		}

		private ContaCorrenteCollection _ContaCorrenteCollectionByIdPessoa;
		#endregion

				
		#region CotistaCollectionByIdPessoa - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - Cotista_IdPessoa_FK1
		/// </summary>

		[XmlIgnore]
		public CotistaCollection CotistaCollectionByIdPessoa
		{
			get
			{
				if(this._CotistaCollectionByIdPessoa == null)
				{
					this._CotistaCollectionByIdPessoa = new CotistaCollection();
					this._CotistaCollectionByIdPessoa.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("CotistaCollectionByIdPessoa", this._CotistaCollectionByIdPessoa);
				
					if(this.IdPessoa != null)
					{
						this._CotistaCollectionByIdPessoa.Query.Where(this._CotistaCollectionByIdPessoa.Query.IdPessoa == this.IdPessoa);
						this._CotistaCollectionByIdPessoa.Query.Load();

						// Auto-hookup Foreign Keys
						this._CotistaCollectionByIdPessoa.fks.Add(CotistaMetadata.ColumnNames.IdPessoa, this.IdPessoa);
					}
				}

				return this._CotistaCollectionByIdPessoa;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._CotistaCollectionByIdPessoa != null) 
				{ 
					this.RemovePostSave("CotistaCollectionByIdPessoa"); 
					this._CotistaCollectionByIdPessoa = null;
					
				} 
			} 			
		}

		private CotistaCollection _CotistaCollectionByIdPessoa;
		#endregion

				
		#region PessoaDocumentoCollectionByIdPessoa - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - FK_PessoaDocumento_Pessoa
		/// </summary>

		[XmlIgnore]
		public PessoaDocumentoCollection PessoaDocumentoCollectionByIdPessoa
		{
			get
			{
				if(this._PessoaDocumentoCollectionByIdPessoa == null)
				{
					this._PessoaDocumentoCollectionByIdPessoa = new PessoaDocumentoCollection();
					this._PessoaDocumentoCollectionByIdPessoa.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("PessoaDocumentoCollectionByIdPessoa", this._PessoaDocumentoCollectionByIdPessoa);
				
					if(this.IdPessoa != null)
					{
						this._PessoaDocumentoCollectionByIdPessoa.Query.Where(this._PessoaDocumentoCollectionByIdPessoa.Query.IdPessoa == this.IdPessoa);
						this._PessoaDocumentoCollectionByIdPessoa.Query.Load();

						// Auto-hookup Foreign Keys
						this._PessoaDocumentoCollectionByIdPessoa.fks.Add(PessoaDocumentoMetadata.ColumnNames.IdPessoa, this.IdPessoa);
					}
				}

				return this._PessoaDocumentoCollectionByIdPessoa;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._PessoaDocumentoCollectionByIdPessoa != null) 
				{ 
					this.RemovePostSave("PessoaDocumentoCollectionByIdPessoa"); 
					this._PessoaDocumentoCollectionByIdPessoa = null;
					
				} 
			} 			
		}

		private PessoaDocumentoCollection _PessoaDocumentoCollectionByIdPessoa;
		#endregion

				
		#region PessoaEmailCollectionByIdPessoa - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - FK_PessoaEmail_Pessoa
		/// </summary>

		[XmlIgnore]
		public PessoaEmailCollection PessoaEmailCollectionByIdPessoa
		{
			get
			{
				if(this._PessoaEmailCollectionByIdPessoa == null)
				{
					this._PessoaEmailCollectionByIdPessoa = new PessoaEmailCollection();
					this._PessoaEmailCollectionByIdPessoa.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("PessoaEmailCollectionByIdPessoa", this._PessoaEmailCollectionByIdPessoa);
				
					if(this.IdPessoa != null)
					{
						this._PessoaEmailCollectionByIdPessoa.Query.Where(this._PessoaEmailCollectionByIdPessoa.Query.IdPessoa == this.IdPessoa);
						this._PessoaEmailCollectionByIdPessoa.Query.Load();

						// Auto-hookup Foreign Keys
						this._PessoaEmailCollectionByIdPessoa.fks.Add(PessoaEmailMetadata.ColumnNames.IdPessoa, this.IdPessoa);
					}
				}

				return this._PessoaEmailCollectionByIdPessoa;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._PessoaEmailCollectionByIdPessoa != null) 
				{ 
					this.RemovePostSave("PessoaEmailCollectionByIdPessoa"); 
					this._PessoaEmailCollectionByIdPessoa = null;
					
				} 
			} 			
		}

		private PessoaEmailCollection _PessoaEmailCollectionByIdPessoa;
		#endregion

				
		#region PessoaEnderecoCollectionByIdPessoa - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - Pessoa_PessoaEndereco_FK1
		/// </summary>

		[XmlIgnore]
		public PessoaEnderecoCollection PessoaEnderecoCollectionByIdPessoa
		{
			get
			{
				if(this._PessoaEnderecoCollectionByIdPessoa == null)
				{
					this._PessoaEnderecoCollectionByIdPessoa = new PessoaEnderecoCollection();
					this._PessoaEnderecoCollectionByIdPessoa.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("PessoaEnderecoCollectionByIdPessoa", this._PessoaEnderecoCollectionByIdPessoa);
				
					if(this.IdPessoa != null)
					{
						this._PessoaEnderecoCollectionByIdPessoa.Query.Where(this._PessoaEnderecoCollectionByIdPessoa.Query.IdPessoa == this.IdPessoa);
						this._PessoaEnderecoCollectionByIdPessoa.Query.Load();

						// Auto-hookup Foreign Keys
						this._PessoaEnderecoCollectionByIdPessoa.fks.Add(PessoaEnderecoMetadata.ColumnNames.IdPessoa, this.IdPessoa);
					}
				}

				return this._PessoaEnderecoCollectionByIdPessoa;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._PessoaEnderecoCollectionByIdPessoa != null) 
				{ 
					this.RemovePostSave("PessoaEnderecoCollectionByIdPessoa"); 
					this._PessoaEnderecoCollectionByIdPessoa = null;
					
				} 
			} 			
		}

		private PessoaEnderecoCollection _PessoaEnderecoCollectionByIdPessoa;
		#endregion

				
		#region PessoaTelefoneCollectionByIdPessoa - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - FK_PessoaTelefone_Pessoa
		/// </summary>

		[XmlIgnore]
		public PessoaTelefoneCollection PessoaTelefoneCollectionByIdPessoa
		{
			get
			{
				if(this._PessoaTelefoneCollectionByIdPessoa == null)
				{
					this._PessoaTelefoneCollectionByIdPessoa = new PessoaTelefoneCollection();
					this._PessoaTelefoneCollectionByIdPessoa.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("PessoaTelefoneCollectionByIdPessoa", this._PessoaTelefoneCollectionByIdPessoa);
				
					if(this.IdPessoa != null)
					{
						this._PessoaTelefoneCollectionByIdPessoa.Query.Where(this._PessoaTelefoneCollectionByIdPessoa.Query.IdPessoa == this.IdPessoa);
						this._PessoaTelefoneCollectionByIdPessoa.Query.Load();

						// Auto-hookup Foreign Keys
						this._PessoaTelefoneCollectionByIdPessoa.fks.Add(PessoaTelefoneMetadata.ColumnNames.IdPessoa, this.IdPessoa);
					}
				}

				return this._PessoaTelefoneCollectionByIdPessoa;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._PessoaTelefoneCollectionByIdPessoa != null) 
				{ 
					this.RemovePostSave("PessoaTelefoneCollectionByIdPessoa"); 
					this._PessoaTelefoneCollectionByIdPessoa = null;
					
				} 
			} 			
		}

		private PessoaTelefoneCollection _PessoaTelefoneCollectionByIdPessoa;
		#endregion

				
		#region SuitabilityRespostaCollectionByIdPessoa - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - FK_SuitabilityResposta_Pessoa
		/// </summary>

		[XmlIgnore]
		public SuitabilityRespostaCollection SuitabilityRespostaCollectionByIdPessoa
		{
			get
			{
				if(this._SuitabilityRespostaCollectionByIdPessoa == null)
				{
					this._SuitabilityRespostaCollectionByIdPessoa = new SuitabilityRespostaCollection();
					this._SuitabilityRespostaCollectionByIdPessoa.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("SuitabilityRespostaCollectionByIdPessoa", this._SuitabilityRespostaCollectionByIdPessoa);
				
					if(this.IdPessoa != null)
					{
						this._SuitabilityRespostaCollectionByIdPessoa.Query.Where(this._SuitabilityRespostaCollectionByIdPessoa.Query.IdPessoa == this.IdPessoa);
						this._SuitabilityRespostaCollectionByIdPessoa.Query.Load();

						// Auto-hookup Foreign Keys
						this._SuitabilityRespostaCollectionByIdPessoa.fks.Add(SuitabilityRespostaMetadata.ColumnNames.IdPessoa, this.IdPessoa);
					}
				}

				return this._SuitabilityRespostaCollectionByIdPessoa;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._SuitabilityRespostaCollectionByIdPessoa != null) 
				{ 
					this.RemovePostSave("SuitabilityRespostaCollectionByIdPessoa"); 
					this._SuitabilityRespostaCollectionByIdPessoa = null;
					
				} 
			} 			
		}

		private SuitabilityRespostaCollection _SuitabilityRespostaCollectionByIdPessoa;
		#endregion

				
		#region TabelaRebateOfficerCollectionByIdPessoa - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - Pessoa_TabelaRebateOfficer_FK1
		/// </summary>

		[XmlIgnore]
		public TabelaRebateOfficerCollection TabelaRebateOfficerCollectionByIdPessoa
		{
			get
			{
				if(this._TabelaRebateOfficerCollectionByIdPessoa == null)
				{
					this._TabelaRebateOfficerCollectionByIdPessoa = new TabelaRebateOfficerCollection();
					this._TabelaRebateOfficerCollectionByIdPessoa.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("TabelaRebateOfficerCollectionByIdPessoa", this._TabelaRebateOfficerCollectionByIdPessoa);
				
					if(this.IdPessoa != null)
					{
						this._TabelaRebateOfficerCollectionByIdPessoa.Query.Where(this._TabelaRebateOfficerCollectionByIdPessoa.Query.IdPessoa == this.IdPessoa);
						this._TabelaRebateOfficerCollectionByIdPessoa.Query.Load();

						// Auto-hookup Foreign Keys
						this._TabelaRebateOfficerCollectionByIdPessoa.fks.Add(TabelaRebateOfficerMetadata.ColumnNames.IdPessoa, this.IdPessoa);
					}
				}

				return this._TabelaRebateOfficerCollectionByIdPessoa;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._TabelaRebateOfficerCollectionByIdPessoa != null) 
				{ 
					this.RemovePostSave("TabelaRebateOfficerCollectionByIdPessoa"); 
					this._TabelaRebateOfficerCollectionByIdPessoa = null;
					
				} 
			} 			
		}

		private TabelaRebateOfficerCollection _TabelaRebateOfficerCollectionByIdPessoa;
		#endregion

				
		#region UpToPerfilInvestidorByIdPerfilInvestidor - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - PerfilInvestidor_Pessoa_FK1
		/// </summary>

		[XmlIgnore]
		public PerfilInvestidor UpToPerfilInvestidorByIdPerfilInvestidor
		{
			get
			{
				if(this._UpToPerfilInvestidorByIdPerfilInvestidor == null
					&& IdPerfilInvestidor != null					)
				{
					this._UpToPerfilInvestidorByIdPerfilInvestidor = new PerfilInvestidor();
					this._UpToPerfilInvestidorByIdPerfilInvestidor.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToPerfilInvestidorByIdPerfilInvestidor", this._UpToPerfilInvestidorByIdPerfilInvestidor);
					this._UpToPerfilInvestidorByIdPerfilInvestidor.Query.Where(this._UpToPerfilInvestidorByIdPerfilInvestidor.Query.IdPerfilInvestidor == this.IdPerfilInvestidor);
					this._UpToPerfilInvestidorByIdPerfilInvestidor.Query.Load();
				}

				return this._UpToPerfilInvestidorByIdPerfilInvestidor;
			}
			
			set
			{
				this.RemovePreSave("UpToPerfilInvestidorByIdPerfilInvestidor");
				

				if(value == null)
				{
					this.IdPerfilInvestidor = null;
					this._UpToPerfilInvestidorByIdPerfilInvestidor = null;
				}
				else
				{
					this.IdPerfilInvestidor = value.IdPerfilInvestidor;
					this._UpToPerfilInvestidorByIdPerfilInvestidor = value;
					this.SetPreSave("UpToPerfilInvestidorByIdPerfilInvestidor", this._UpToPerfilInvestidorByIdPerfilInvestidor);
				}
				
			}
		}
		#endregion
		

		
		/// <summary>
		/// Used internally by the entity's hierarchical properties.
		/// </summary>
		protected override List<esPropertyDescriptor> GetHierarchicalProperties()
		{
			List<esPropertyDescriptor> props = new List<esPropertyDescriptor>();
			
			props.Add(new esPropertyDescriptor(this, "ClienteCollectionByIdPessoa", typeof(ClienteCollection), new Cliente()));
			props.Add(new esPropertyDescriptor(this, "ContaCorrenteCollectionByIdPessoa", typeof(ContaCorrenteCollection), new ContaCorrente()));
			props.Add(new esPropertyDescriptor(this, "CotistaCollectionByIdPessoa", typeof(CotistaCollection), new Cotista()));
			props.Add(new esPropertyDescriptor(this, "PessoaDocumentoCollectionByIdPessoa", typeof(PessoaDocumentoCollection), new PessoaDocumento()));
			props.Add(new esPropertyDescriptor(this, "PessoaEmailCollectionByIdPessoa", typeof(PessoaEmailCollection), new PessoaEmail()));
			props.Add(new esPropertyDescriptor(this, "PessoaEnderecoCollectionByIdPessoa", typeof(PessoaEnderecoCollection), new PessoaEndereco()));
			props.Add(new esPropertyDescriptor(this, "PessoaTelefoneCollectionByIdPessoa", typeof(PessoaTelefoneCollection), new PessoaTelefone()));
			props.Add(new esPropertyDescriptor(this, "SuitabilityRespostaCollectionByIdPessoa", typeof(SuitabilityRespostaCollection), new SuitabilityResposta()));
			props.Add(new esPropertyDescriptor(this, "TabelaRebateOfficerCollectionByIdPessoa", typeof(TabelaRebateOfficerCollection), new TabelaRebateOfficer()));
		
			return props;
		}	
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PreSave.
		/// </summary>
		protected override void ApplyPreSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostSave.
		/// </summary>
		protected override void ApplyPostSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostOneToOneSave.
		/// </summary>
		protected override void ApplyPostOneSaveKeys()
		{
		}
		
	}



	[Serializable]
	abstract public class esPessoaQuery : esDynamicQuery
	{
		override protected IMetadata Meta
		{
			get
			{
				return PessoaMetadata.Meta();
			}
		}	
		

		public esQueryItem IdPessoa
		{
			get
			{
				return new esQueryItem(this, PessoaMetadata.ColumnNames.IdPessoa, esSystemType.Int32);
			}
		} 
		
		public esQueryItem Nome
		{
			get
			{
				return new esQueryItem(this, PessoaMetadata.ColumnNames.Nome, esSystemType.String);
			}
		} 
		
		public esQueryItem Apelido
		{
			get
			{
				return new esQueryItem(this, PessoaMetadata.ColumnNames.Apelido, esSystemType.String);
			}
		} 
		
		public esQueryItem Tipo
		{
			get
			{
				return new esQueryItem(this, PessoaMetadata.ColumnNames.Tipo, esSystemType.Byte);
			}
		} 
		
		public esQueryItem DataCadatro
		{
			get
			{
				return new esQueryItem(this, PessoaMetadata.ColumnNames.DataCadatro, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem DataUltimaAlteracao
		{
			get
			{
				return new esQueryItem(this, PessoaMetadata.ColumnNames.DataUltimaAlteracao, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem Cpfcnpj
		{
			get
			{
				return new esQueryItem(this, PessoaMetadata.ColumnNames.Cpfcnpj, esSystemType.String);
			}
		} 
		
		public esQueryItem IdAgenteDistribuidor
		{
			get
			{
				return new esQueryItem(this, PessoaMetadata.ColumnNames.IdAgenteDistribuidor, esSystemType.Int32);
			}
		} 
		
		public esQueryItem EstadoCivil
		{
			get
			{
				return new esQueryItem(this, PessoaMetadata.ColumnNames.EstadoCivil, esSystemType.Byte);
			}
		} 
		
		public esQueryItem Sexo
		{
			get
			{
				return new esQueryItem(this, PessoaMetadata.ColumnNames.Sexo, esSystemType.String);
			}
		} 
		
		public esQueryItem DataNascimento
		{
			get
			{
				return new esQueryItem(this, PessoaMetadata.ColumnNames.DataNascimento, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem Profissao
		{
			get
			{
				return new esQueryItem(this, PessoaMetadata.ColumnNames.Profissao, esSystemType.String);
			}
		} 
		
		public esQueryItem CodigoInterface
		{
			get
			{
				return new esQueryItem(this, PessoaMetadata.ColumnNames.CodigoInterface, esSystemType.String);
			}
		} 
		
		public esQueryItem PessoaPoliticamenteExposta
		{
			get
			{
				return new esQueryItem(this, PessoaMetadata.ColumnNames.PessoaPoliticamenteExposta, esSystemType.String);
			}
		} 
		
		public esQueryItem FiliacaoNomePai
		{
			get
			{
				return new esQueryItem(this, PessoaMetadata.ColumnNames.FiliacaoNomePai, esSystemType.String);
			}
		} 
		
		public esQueryItem FiliacaoNomeMae
		{
			get
			{
				return new esQueryItem(this, PessoaMetadata.ColumnNames.FiliacaoNomeMae, esSystemType.String);
			}
		} 
		
		public esQueryItem UFNaturalidade
		{
			get
			{
				return new esQueryItem(this, PessoaMetadata.ColumnNames.UFNaturalidade, esSystemType.String);
			}
		} 
		
		public esQueryItem IdPerfilInvestidor
		{
			get
			{
				return new esQueryItem(this, PessoaMetadata.ColumnNames.IdPerfilInvestidor, esSystemType.Byte);
			}
		} 
		
		public esQueryItem SituacaoLegal
		{
			get
			{
				return new esQueryItem(this, PessoaMetadata.ColumnNames.SituacaoLegal, esSystemType.Byte);
			}
		} 
		
		public esQueryItem PessoaVinculada
		{
			get
			{
				return new esQueryItem(this, PessoaMetadata.ColumnNames.PessoaVinculada, esSystemType.String);
			}
		} 
		
		public esQueryItem NaturezaJuridica
		{
			get
			{
				return new esQueryItem(this, PessoaMetadata.ColumnNames.NaturezaJuridica, esSystemType.Int16);
			}
		} 
		
		public esQueryItem TipoRenda
		{
			get
			{
				return new esQueryItem(this, PessoaMetadata.ColumnNames.TipoRenda, esSystemType.String);
			}
		} 
		
		public esQueryItem IndicadorFatca
		{
			get
			{
				return new esQueryItem(this, PessoaMetadata.ColumnNames.IndicadorFatca, esSystemType.String);
			}
		} 
		
		public esQueryItem GiinTin
		{
			get
			{
				return new esQueryItem(this, PessoaMetadata.ColumnNames.GiinTin, esSystemType.String);
			}
		} 
		
		public esQueryItem JustificativaGiin
		{
			get
			{
				return new esQueryItem(this, PessoaMetadata.ColumnNames.JustificativaGiin, esSystemType.String);
			}
		} 
		
		public esQueryItem CnaeDivisao
		{
			get
			{
				return new esQueryItem(this, PessoaMetadata.ColumnNames.CnaeDivisao, esSystemType.Byte);
			}
		} 
		
		public esQueryItem CnaeGrupo
		{
			get
			{
				return new esQueryItem(this, PessoaMetadata.ColumnNames.CnaeGrupo, esSystemType.Byte);
			}
		} 
		
		public esQueryItem CnaeClasse
		{
			get
			{
				return new esQueryItem(this, PessoaMetadata.ColumnNames.CnaeClasse, esSystemType.Byte);
			}
		} 
		
		public esQueryItem CnaeSubClasse
		{
			get
			{
				return new esQueryItem(this, PessoaMetadata.ColumnNames.CnaeSubClasse, esSystemType.Byte);
			}
		} 
		
		public esQueryItem Nacionalidade
		{
			get
			{
				return new esQueryItem(this, PessoaMetadata.ColumnNames.Nacionalidade, esSystemType.Int16);
			}
		} 
		
		public esQueryItem OffShore
		{
			get
			{
				return new esQueryItem(this, PessoaMetadata.ColumnNames.OffShore, esSystemType.String);
			}
		} 
		
        public esQueryItem DataVencimentoCadastro
        {
            get
            {
                return new esQueryItem(this, PessoaMetadata.ColumnNames.DataVencimentoCadastro, esSystemType.DateTime);
            }
        }
        public esQueryItem AlertaCadastro
        {
            get
            {
                return new esQueryItem(this, PessoaMetadata.ColumnNames.AlertaCadastro, esSystemType.String);
            }
        }

        public esQueryItem EmailAlerta
        {
            get
            {
                return new esQueryItem(this, PessoaMetadata.ColumnNames.EmailAlerta, esSystemType.String);
            }
        } 
	}



	[Serializable]
	[XmlType("PessoaCollection")]
	public partial class PessoaCollection : esPessoaCollection, IEnumerable<Pessoa>
	{
		public PessoaCollection()
		{

		}
		
		public static implicit operator List<Pessoa>(PessoaCollection coll)
		{
			List<Pessoa> list = new List<Pessoa>();
			
			foreach (Pessoa emp in coll)
			{
				list.Add(emp);
			}
			
			return list;
		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return  PessoaMetadata.Meta();
			}
		}
		
		
		
		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new PessoaQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		
		override protected esEntity CreateEntityForCollection(DataRow row)
		{
			return new Pessoa(row);
		}

		override protected esEntity CreateEntity()
		{
			return new Pessoa();
		}
		
		
		#endregion


		[BrowsableAttribute( false )]
		public PessoaQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new PessoaQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}

		public bool Load(PessoaQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		public Pessoa AddNew()
		{
			Pessoa entity = base.AddNewEntity() as Pessoa;
			
			return entity;
		}

		public Pessoa FindByPrimaryKey(System.Int32 idPessoa)
		{
			return base.FindByPrimaryKey(idPessoa) as Pessoa;
		}


		#region IEnumerable<Pessoa> Members

		IEnumerator<Pessoa> IEnumerable<Pessoa>.GetEnumerator()
		{
			System.Collections.IEnumerable enumer = this as System.Collections.IEnumerable;
			System.Collections.IEnumerator iterator = enumer.GetEnumerator();

			while(iterator.MoveNext())
			{
				yield return iterator.Current as Pessoa;
			}
		}

		#endregion
		
		private PessoaQuery query;
	}


	/// <summary>
	/// Encapsulates the 'Pessoa' table
	/// </summary>

	[Serializable]
	public partial class Pessoa : esPessoa
	{
		public Pessoa()
		{

		}
	
		public Pessoa(DataRow row)
			: base(row)
		{

		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return PessoaMetadata.Meta();
			}
		}
		
		
		
		override protected esPessoaQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new PessoaQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		#endregion
		



		[BrowsableAttribute( false )]
		public PessoaQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new PessoaQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}
		

		public bool Load(PessoaQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		private PessoaQuery query;
	}



	[Serializable]
	public partial class PessoaQuery : esPessoaQuery
	{
		public PessoaQuery()
		{

		}		
		
		public PessoaQuery(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
			
	}



	[Serializable]
	public partial class PessoaMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected PessoaMetadata()
		{
			_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(PessoaMetadata.ColumnNames.IdPessoa, 0, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = PessoaMetadata.PropertyNames.IdPessoa;
			c.IsInPrimaryKey = true;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PessoaMetadata.ColumnNames.Nome, 1, typeof(System.String), esSystemType.String);
			c.PropertyName = PessoaMetadata.PropertyNames.Nome;
			c.CharacterMaxLength = 255;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PessoaMetadata.ColumnNames.Apelido, 2, typeof(System.String), esSystemType.String);
			c.PropertyName = PessoaMetadata.PropertyNames.Apelido;
			c.CharacterMaxLength = 100;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PessoaMetadata.ColumnNames.Tipo, 3, typeof(System.Byte), esSystemType.Byte);
			c.PropertyName = PessoaMetadata.PropertyNames.Tipo;	
			c.NumericPrecision = 3;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PessoaMetadata.ColumnNames.DataCadatro, 4, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = PessoaMetadata.PropertyNames.DataCadatro;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PessoaMetadata.ColumnNames.DataUltimaAlteracao, 5, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = PessoaMetadata.PropertyNames.DataUltimaAlteracao;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PessoaMetadata.ColumnNames.Cpfcnpj, 6, typeof(System.String), esSystemType.String);
			c.PropertyName = PessoaMetadata.PropertyNames.Cpfcnpj;
			c.CharacterMaxLength = 30;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PessoaMetadata.ColumnNames.IdAgenteDistribuidor, 7, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = PessoaMetadata.PropertyNames.IdAgenteDistribuidor;	
			c.NumericPrecision = 10;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PessoaMetadata.ColumnNames.EstadoCivil, 8, typeof(System.Byte), esSystemType.Byte);
			c.PropertyName = PessoaMetadata.PropertyNames.EstadoCivil;	
			c.NumericPrecision = 3;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PessoaMetadata.ColumnNames.Sexo, 9, typeof(System.String), esSystemType.String);
			c.PropertyName = PessoaMetadata.PropertyNames.Sexo;
			c.CharacterMaxLength = 1;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PessoaMetadata.ColumnNames.DataNascimento, 10, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = PessoaMetadata.PropertyNames.DataNascimento;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PessoaMetadata.ColumnNames.Profissao, 11, typeof(System.String), esSystemType.String);
			c.PropertyName = PessoaMetadata.PropertyNames.Profissao;
			c.CharacterMaxLength = 50;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PessoaMetadata.ColumnNames.CodigoInterface, 12, typeof(System.String), esSystemType.String);
			c.PropertyName = PessoaMetadata.PropertyNames.CodigoInterface;
			c.CharacterMaxLength = 40;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PessoaMetadata.ColumnNames.PessoaPoliticamenteExposta, 13, typeof(System.String), esSystemType.String);
			c.PropertyName = PessoaMetadata.PropertyNames.PessoaPoliticamenteExposta;
			c.CharacterMaxLength = 1;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PessoaMetadata.ColumnNames.FiliacaoNomePai, 14, typeof(System.String), esSystemType.String);
			c.PropertyName = PessoaMetadata.PropertyNames.FiliacaoNomePai;
			c.CharacterMaxLength = 100;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PessoaMetadata.ColumnNames.FiliacaoNomeMae, 15, typeof(System.String), esSystemType.String);
			c.PropertyName = PessoaMetadata.PropertyNames.FiliacaoNomeMae;
			c.CharacterMaxLength = 100;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PessoaMetadata.ColumnNames.UFNaturalidade, 16, typeof(System.String), esSystemType.String);
			c.PropertyName = PessoaMetadata.PropertyNames.UFNaturalidade;
			c.CharacterMaxLength = 2;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PessoaMetadata.ColumnNames.IdPerfilInvestidor, 17, typeof(System.Byte), esSystemType.Byte);
			c.PropertyName = PessoaMetadata.PropertyNames.IdPerfilInvestidor;	
			c.NumericPrecision = 3;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PessoaMetadata.ColumnNames.SituacaoLegal, 18, typeof(System.Byte), esSystemType.Byte);
			c.PropertyName = PessoaMetadata.PropertyNames.SituacaoLegal;	
			c.NumericPrecision = 3;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PessoaMetadata.ColumnNames.PessoaVinculada, 19, typeof(System.String), esSystemType.String);
			c.PropertyName = PessoaMetadata.PropertyNames.PessoaVinculada;
			c.CharacterMaxLength = 1;
			c.NumericPrecision = 0;
			c.HasDefault = true;
			c.Default = @"('N')";
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PessoaMetadata.ColumnNames.NaturezaJuridica, 20, typeof(System.Int16), esSystemType.Int16);
			c.PropertyName = PessoaMetadata.PropertyNames.NaturezaJuridica;	
			c.NumericPrecision = 5;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PessoaMetadata.ColumnNames.TipoRenda, 21, typeof(System.String), esSystemType.String);
			c.PropertyName = PessoaMetadata.PropertyNames.TipoRenda;
			c.CharacterMaxLength = 1;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PessoaMetadata.ColumnNames.IndicadorFatca, 22, typeof(System.String), esSystemType.String);
			c.PropertyName = PessoaMetadata.PropertyNames.IndicadorFatca;
			c.CharacterMaxLength = 1;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PessoaMetadata.ColumnNames.GiinTin, 23, typeof(System.String), esSystemType.String);
			c.PropertyName = PessoaMetadata.PropertyNames.GiinTin;
			c.CharacterMaxLength = 16;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PessoaMetadata.ColumnNames.JustificativaGiin, 24, typeof(System.String), esSystemType.String);
			c.PropertyName = PessoaMetadata.PropertyNames.JustificativaGiin;
			c.CharacterMaxLength = 2;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PessoaMetadata.ColumnNames.CnaeDivisao, 25, typeof(System.Byte), esSystemType.Byte);
			c.PropertyName = PessoaMetadata.PropertyNames.CnaeDivisao;	
			c.NumericPrecision = 3;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PessoaMetadata.ColumnNames.CnaeGrupo, 26, typeof(System.Byte), esSystemType.Byte);
			c.PropertyName = PessoaMetadata.PropertyNames.CnaeGrupo;	
			c.NumericPrecision = 3;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PessoaMetadata.ColumnNames.CnaeClasse, 27, typeof(System.Byte), esSystemType.Byte);
			c.PropertyName = PessoaMetadata.PropertyNames.CnaeClasse;	
			c.NumericPrecision = 3;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PessoaMetadata.ColumnNames.CnaeSubClasse, 28, typeof(System.Byte), esSystemType.Byte);
			c.PropertyName = PessoaMetadata.PropertyNames.CnaeSubClasse;	
			c.NumericPrecision = 3;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PessoaMetadata.ColumnNames.Nacionalidade, 29, typeof(System.Int16), esSystemType.Int16);
			c.PropertyName = PessoaMetadata.PropertyNames.Nacionalidade;	
			c.NumericPrecision = 5;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PessoaMetadata.ColumnNames.OffShore, 30, typeof(System.String), esSystemType.String);
			c.PropertyName = PessoaMetadata.PropertyNames.OffShore;
			c.CharacterMaxLength = 1;
			c.NumericPrecision = 0;
			c.HasDefault = true;
			c.Default = @"('N')";
			_columns.Add(c); 

            c = new esColumnMetadata(PessoaMetadata.ColumnNames.DataVencimentoCadastro, 30, typeof(System.DateTime), esSystemType.DateTime);
            c.PropertyName = PessoaMetadata.PropertyNames.DataVencimentoCadastro;
            c.NumericPrecision = 0;
            c.IsNullable = true;
            _columns.Add(c);

            c = new esColumnMetadata(PessoaMetadata.ColumnNames.AlertaCadastro, 31, typeof(System.String), esSystemType.String);
            c.PropertyName = PessoaMetadata.PropertyNames.AlertaCadastro;
            c.CharacterMaxLength = 1;
            c.NumericPrecision = 0;
            c.HasDefault = true;
            c.Default = @"('N')";
            _columns.Add(c);


            c = new esColumnMetadata(PessoaMetadata.ColumnNames.EmailAlerta, 32, typeof(System.String), esSystemType.String);
            c.PropertyName = PessoaMetadata.PropertyNames.EmailAlerta;
            c.CharacterMaxLength = 200;
            c.NumericPrecision = 0;
            c.IsNullable = true;
            _columns.Add(c); 
		}
		#endregion
	
		static public PessoaMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base._dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base._columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string IdPessoa = "IdPessoa";
			 public const string Nome = "Nome";
			 public const string Apelido = "Apelido";
			 public const string Tipo = "Tipo";
			 public const string DataCadatro = "DataCadatro";
			 public const string DataUltimaAlteracao = "DataUltimaAlteracao";
			 public const string Cpfcnpj = "Cpfcnpj";
			 public const string IdAgenteDistribuidor = "IdAgenteDistribuidor";
			 public const string EstadoCivil = "EstadoCivil";
			 public const string Sexo = "Sexo";
			 public const string DataNascimento = "DataNascimento";
			 public const string Profissao = "Profissao";
			 public const string CodigoInterface = "CodigoInterface";
			 public const string PessoaPoliticamenteExposta = "PessoaPoliticamenteExposta";
			 public const string FiliacaoNomePai = "FiliacaoNomePai";
			 public const string FiliacaoNomeMae = "FiliacaoNomeMae";
			 public const string UFNaturalidade = "UFNaturalidade";
			 public const string IdPerfilInvestidor = "IdPerfilInvestidor";
			 public const string SituacaoLegal = "SituacaoLegal";
			 public const string PessoaVinculada = "PessoaVinculada";
			 public const string NaturezaJuridica = "NaturezaJuridica";
			 public const string TipoRenda = "TipoRenda";
			 public const string IndicadorFatca = "IndicadorFatca";
			 public const string GiinTin = "GiinTin";
			 public const string JustificativaGiin = "JustificativaGiin";
			 public const string CnaeDivisao = "CnaeDivisao";
			 public const string CnaeGrupo = "CnaeGrupo";
			 public const string CnaeClasse = "CnaeClasse";
			 public const string CnaeSubClasse = "CnaeSubClasse";
			 public const string Nacionalidade = "Nacionalidade";
			 public const string OffShore = "OffShore";
             public const string DataVencimentoCadastro = "DataVencimentoCadastro";
             public const string AlertaCadastro = "AlertaCadastro";
             public const string EmailAlerta = "EmailAlerta";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string IdPessoa = "IdPessoa";
			 public const string Nome = "Nome";
			 public const string Apelido = "Apelido";
			 public const string Tipo = "Tipo";
			 public const string DataCadatro = "DataCadatro";
			 public const string DataUltimaAlteracao = "DataUltimaAlteracao";
			 public const string Cpfcnpj = "Cpfcnpj";
			 public const string IdAgenteDistribuidor = "IdAgenteDistribuidor";
			 public const string EstadoCivil = "EstadoCivil";
			 public const string Sexo = "Sexo";
			 public const string DataNascimento = "DataNascimento";
			 public const string Profissao = "Profissao";
			 public const string CodigoInterface = "CodigoInterface";
			 public const string PessoaPoliticamenteExposta = "PessoaPoliticamenteExposta";
			 public const string FiliacaoNomePai = "FiliacaoNomePai";
			 public const string FiliacaoNomeMae = "FiliacaoNomeMae";
			 public const string UFNaturalidade = "UFNaturalidade";
			 public const string IdPerfilInvestidor = "IdPerfilInvestidor";
			 public const string SituacaoLegal = "SituacaoLegal";
			 public const string PessoaVinculada = "PessoaVinculada";
			 public const string NaturezaJuridica = "NaturezaJuridica";
			 public const string TipoRenda = "TipoRenda";
			 public const string IndicadorFatca = "IndicadorFatca";
			 public const string GiinTin = "GiinTin";
			 public const string JustificativaGiin = "JustificativaGiin";
			 public const string CnaeDivisao = "CnaeDivisao";
			 public const string CnaeGrupo = "CnaeGrupo";
			 public const string CnaeClasse = "CnaeClasse";
			 public const string CnaeSubClasse = "CnaeSubClasse";
			 public const string Nacionalidade = "Nacionalidade";
			 public const string OffShore = "OffShore";
             public const string DataVencimentoCadastro = "DataVencimentoCadastro";
             public const string AlertaCadastro = "AlertaCadastro";
             public const string EmailAlerta = "EmailAlerta";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(PessoaMetadata))
			{
				if(PessoaMetadata.mapDelegates == null)
				{
					PessoaMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (PessoaMetadata.meta == null)
				{
					PessoaMetadata.meta = new PessoaMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();
				

				meta.AddTypeMap("IdPessoa", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("Nome", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("Apelido", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("Tipo", new esTypeMap("tinyint", "System.Byte"));
				meta.AddTypeMap("DataCadatro", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("DataUltimaAlteracao", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("Cpfcnpj", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("IdAgenteDistribuidor", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("EstadoCivil", new esTypeMap("tinyint", "System.Byte"));
				meta.AddTypeMap("Sexo", new esTypeMap("char", "System.String"));
				meta.AddTypeMap("DataNascimento", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("Profissao", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("CodigoInterface", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("PessoaPoliticamenteExposta", new esTypeMap("char", "System.String"));
				meta.AddTypeMap("FiliacaoNomePai", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("FiliacaoNomeMae", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("UFNaturalidade", new esTypeMap("char", "System.String"));
				meta.AddTypeMap("IdPerfilInvestidor", new esTypeMap("tinyint", "System.Byte"));
				meta.AddTypeMap("SituacaoLegal", new esTypeMap("tinyint", "System.Byte"));
				meta.AddTypeMap("PessoaVinculada", new esTypeMap("char", "System.String"));
				meta.AddTypeMap("NaturezaJuridica", new esTypeMap("smallint", "System.Int16"));
				meta.AddTypeMap("TipoRenda", new esTypeMap("char", "System.String"));
				meta.AddTypeMap("IndicadorFatca", new esTypeMap("char", "System.String"));
				meta.AddTypeMap("GiinTin", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("JustificativaGiin", new esTypeMap("char", "System.String"));
				meta.AddTypeMap("CnaeDivisao", new esTypeMap("tinyint", "System.Byte"));
				meta.AddTypeMap("CnaeGrupo", new esTypeMap("tinyint", "System.Byte"));
				meta.AddTypeMap("CnaeClasse", new esTypeMap("tinyint", "System.Byte"));
				meta.AddTypeMap("CnaeSubClasse", new esTypeMap("tinyint", "System.Byte"));
				meta.AddTypeMap("Nacionalidade", new esTypeMap("smallint", "System.Int16"));
				meta.AddTypeMap("OffShore", new esTypeMap("char", "System.String"));			
                meta.AddTypeMap("DataVencimentoCadastro", new esTypeMap("datetime", "System.DateTime"));
                meta.AddTypeMap("AlertaCadastro", new esTypeMap("char", "System.String"));
                meta.AddTypeMap("EmailAlerta", new esTypeMap("varchar", "System.String"));
				
				meta.Source = "Pessoa";
				meta.Destination = "Pessoa";
				
				meta.spInsert = "proc_PessoaInsert";				
				meta.spUpdate = "proc_PessoaUpdate";		
				meta.spDelete = "proc_PessoaDelete";
				meta.spLoadAll = "proc_PessoaLoadAll";
				meta.spLoadByPrimaryKey = "proc_PessoaLoadByPrimaryKey";
				
				this._providerMetadataMaps["esDefault"] = meta;
			}
			
			return this._providerMetadataMaps["esDefault"];
		}

		#endregion

		static private PessoaMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
