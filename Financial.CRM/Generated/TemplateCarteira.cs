/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 23/05/2014 15:02:36
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;


using EntitySpaces.Interfaces;
using EntitySpaces.Core;





































		



			
					


		

		
		
		

		

























		






	











using Financial.Fundo;







				
		
		




		








		
				
				












	

















					
								
											
	










		





		




				
				








				




		

		
		
		
		
		





namespace Financial.CRM
{

	[Serializable]
	abstract public class esTemplateCarteiraCollection : esEntityCollection
	{
		public esTemplateCarteiraCollection()
		{

		}

		protected override string GetCollectionName()
		{
			return "TemplateCarteiraCollection";
		}

		#region Query Logic
		protected void InitQuery(esTemplateCarteiraQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntityCollection)this).Connection;
		}

		protected bool OnQueryLoaded(DataTable table)
		{
			this.PopulateCollection(table);
			return (this.RowCount > 0) ? true : false;
		}
		
		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery(query as esTemplateCarteiraQuery);
		}
		#endregion
		
		virtual public TemplateCarteira DetachEntity(TemplateCarteira entity)
		{
			return base.DetachEntity(entity) as TemplateCarteira;
		}
		
		virtual public TemplateCarteira AttachEntity(TemplateCarteira entity)
		{
			return base.AttachEntity(entity) as TemplateCarteira;
		}
		
		virtual public void Combine(TemplateCarteiraCollection collection)
		{
			base.Combine(collection);
		}
		
		new public TemplateCarteira this[int index]
		{
			get
			{
				return base[index] as TemplateCarteira;
			}
		}

		public override Type GetEntityType()
		{
			return typeof(TemplateCarteira);
		}
	}



	[Serializable]
	abstract public class esTemplateCarteira : esEntity
	{
		/// <summary>
		/// Used internally by the entity's DynamicQuery mechanism.
		/// </summary>
		virtual protected esTemplateCarteiraQuery GetDynamicQuery()
		{
			return null;
		}

		public esTemplateCarteira()
		{

		}

		public esTemplateCarteira(DataRow row)
			: base(row)
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.Int32 idCarteira)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idCarteira);
			else
				return LoadByPrimaryKeyStoredProcedure(idCarteira);
		}

		/// <summary>
		/// Loads an entity by primary key
		/// </summary>
		/// <remarks>
		/// EntitySpaces requires primary keys be defined on all tables.
		/// If a table does not have a primary key set,
		/// this method will not compile.
		/// Does not support sqlAcessType. It only works with DynamicQuery
		/// </remarks>
		/// <param name="fieldsToReturn">Fields desired</param>
		public virtual bool LoadByPrimaryKey(List<esQueryItem> fieldsToReturn, System.Int32 idCarteira)
		{
			esQueryItem[] fields = fieldsToReturn.ToArray();			
			esTemplateCarteiraQuery query = this.GetDynamicQuery();
			query
			    .Select(fields)
			    .Where(query.IdCarteira == idCarteira);
		
			return query.Load();
		}
		
		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.Int32 idCarteira)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idCarteira);
			else
				return LoadByPrimaryKeyStoredProcedure(idCarteira);
		}

		private bool LoadByPrimaryKeyDynamic(System.Int32 idCarteira)
		{
			esTemplateCarteiraQuery query = this.GetDynamicQuery();
			query.Where(query.IdCarteira == idCarteira);
			return query.Load();
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.Int32 idCarteira)
		{
			esParameters parms = new esParameters();
			parms.Add("IdCarteira",idCarteira);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		
		
		#region Properties
		
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}

		public override void SetProperty(string name, object value)
		{
			if(this.Row == null) this.AddNew();
			
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value.GetType().ToString() == "System.String")
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "IdCarteira": this.str.IdCarteira = (string)value; break;							
						case "Descricao": this.str.Descricao = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "IdCarteira":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdCarteira = (System.Int32?)value;
							break;
					

						default:
							break;
					}
				}
			}
			else if(this.Row.Table.Columns.Contains(name))
			{
				this.Row[name] = value;
			}
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}
		
		
		/// <summary>
		/// Maps to TemplateCarteira.IdCarteira
		/// </summary>
		virtual public System.Int32? IdCarteira
		{
			get
			{
				return base.GetSystemInt32(TemplateCarteiraMetadata.ColumnNames.IdCarteira);
			}
			
			set
			{
				base.SetSystemInt32(TemplateCarteiraMetadata.ColumnNames.IdCarteira, value);
			}
		}
		
		/// <summary>
		/// Maps to TemplateCarteira.Descricao
		/// </summary>
		virtual public System.String Descricao
		{
			get
			{
				return base.GetSystemString(TemplateCarteiraMetadata.ColumnNames.Descricao);
			}
			
			set
			{
				base.SetSystemString(TemplateCarteiraMetadata.ColumnNames.Descricao, value);
			}
		}
		
		#endregion	

		#region String Properties


		[BrowsableAttribute( false )]
		public esStrings str
		{
			get
			{
				if (esstrings == null)
				{
					esstrings = new esStrings(this);
				}
				return esstrings;
			}
		}


		[Serializable]
		sealed public class esStrings
		{
			public esStrings(esTemplateCarteira entity)
			{
				this.entity = entity;
			}
			
	
			public System.String IdCarteira
			{
				get
				{
					System.Int32? data = entity.IdCarteira;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdCarteira = null;
					else entity.IdCarteira = Convert.ToInt32(value);
				}
			}
				
			public System.String Descricao
			{
				get
				{
					System.String data = entity.Descricao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Descricao = null;
					else entity.Descricao = Convert.ToString(value);
				}
			}
			

			private esTemplateCarteira entity;
		}
		#endregion

		#region Query Logic
		protected void InitQuery(esTemplateCarteiraQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntity)this).Connection;
		}

		[System.Diagnostics.DebuggerNonUserCode]
		protected bool OnQueryLoaded(DataTable table)
		{
			bool dataFound = this.PopulateEntity(table);

			if (this.RowCount > 1)
			{
				throw new Exception("esTemplateCarteira can only hold one record of data");
			}

			return dataFound;
		}
		#endregion
		
		[NonSerialized]
		private esStrings esstrings;
	}


	
	public partial class TemplateCarteira : esTemplateCarteira
	{

		#region UpToCarteira - One To One
		/// <summary>
		/// One to One
		/// Foreign Key Name - Carteira_TemplateCarteira_FK1
		/// </summary>

		[XmlIgnore]
		public Carteira UpToCarteira
		{
			get
			{
				if(this._UpToCarteira == null
					&& IdCarteira != null					)
				{
					this._UpToCarteira = new Carteira();
					this._UpToCarteira.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToCarteira", this._UpToCarteira);
					this._UpToCarteira.Query.Where(this._UpToCarteira.Query.IdCarteira == this.IdCarteira);
					this._UpToCarteira.Query.Load();
				}

				return this._UpToCarteira;
			}
			
			set 
			{ 
				this.RemovePreSave("UpToCarteira");

				if(value == null)
				{
					this._UpToCarteira = null;
				}
				else
				{
					this._UpToCarteira = value;
					this.SetPreSave("UpToCarteira", this._UpToCarteira);
				}
				
				
			} 
		}

		private Carteira _UpToCarteira;
		#endregion

		
		/// <summary>
		/// Used internally by the entity's hierarchical properties.
		/// </summary>
		protected override List<esPropertyDescriptor> GetHierarchicalProperties()
		{
			List<esPropertyDescriptor> props = new List<esPropertyDescriptor>();
			
		
			return props;
		}	
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PreSave.
		/// </summary>
		protected override void ApplyPreSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostSave.
		/// </summary>
		protected override void ApplyPostSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostOneToOneSave.
		/// </summary>
		protected override void ApplyPostOneSaveKeys()
		{
		}
		
	}



	[Serializable]
	abstract public class esTemplateCarteiraQuery : esDynamicQuery
	{
		override protected IMetadata Meta
		{
			get
			{
				return TemplateCarteiraMetadata.Meta();
			}
		}	
		

		public esQueryItem IdCarteira
		{
			get
			{
				return new esQueryItem(this, TemplateCarteiraMetadata.ColumnNames.IdCarteira, esSystemType.Int32);
			}
		} 
		
		public esQueryItem Descricao
		{
			get
			{
				return new esQueryItem(this, TemplateCarteiraMetadata.ColumnNames.Descricao, esSystemType.String);
			}
		} 
		
	}



	[Serializable]
	[XmlType("TemplateCarteiraCollection")]
	public partial class TemplateCarteiraCollection : esTemplateCarteiraCollection, IEnumerable<TemplateCarteira>
	{
		public TemplateCarteiraCollection()
		{

		}
		
		public static implicit operator List<TemplateCarteira>(TemplateCarteiraCollection coll)
		{
			List<TemplateCarteira> list = new List<TemplateCarteira>();
			
			foreach (TemplateCarteira emp in coll)
			{
				list.Add(emp);
			}
			
			return list;
		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return  TemplateCarteiraMetadata.Meta();
			}
		}
		
		
		
		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new TemplateCarteiraQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		
		override protected esEntity CreateEntityForCollection(DataRow row)
		{
			return new TemplateCarteira(row);
		}

		override protected esEntity CreateEntity()
		{
			return new TemplateCarteira();
		}
		
		
		#endregion


		[BrowsableAttribute( false )]
		public TemplateCarteiraQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new TemplateCarteiraQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}

		public bool Load(TemplateCarteiraQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		public TemplateCarteira AddNew()
		{
			TemplateCarteira entity = base.AddNewEntity() as TemplateCarteira;
			
			return entity;
		}

		public TemplateCarteira FindByPrimaryKey(System.Int32 idCarteira)
		{
			return base.FindByPrimaryKey(idCarteira) as TemplateCarteira;
		}


		#region IEnumerable<TemplateCarteira> Members

		IEnumerator<TemplateCarteira> IEnumerable<TemplateCarteira>.GetEnumerator()
		{
			System.Collections.IEnumerable enumer = this as System.Collections.IEnumerable;
			System.Collections.IEnumerator iterator = enumer.GetEnumerator();

			while(iterator.MoveNext())
			{
				yield return iterator.Current as TemplateCarteira;
			}
		}

		#endregion
		
		private TemplateCarteiraQuery query;
	}


	/// <summary>
	/// Encapsulates the 'TemplateCarteira' table
	/// </summary>

	[Serializable]
	public partial class TemplateCarteira : esTemplateCarteira
	{
		public TemplateCarteira()
		{

		}
	
		public TemplateCarteira(DataRow row)
			: base(row)
		{

		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return TemplateCarteiraMetadata.Meta();
			}
		}
		
		
		
		override protected esTemplateCarteiraQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new TemplateCarteiraQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		#endregion
		



		[BrowsableAttribute( false )]
		public TemplateCarteiraQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new TemplateCarteiraQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}
		

		public bool Load(TemplateCarteiraQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		private TemplateCarteiraQuery query;
	}



	[Serializable]
	public partial class TemplateCarteiraQuery : esTemplateCarteiraQuery
	{
		public TemplateCarteiraQuery()
		{

		}		
		
		public TemplateCarteiraQuery(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
			
	}



	[Serializable]
	public partial class TemplateCarteiraMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected TemplateCarteiraMetadata()
		{
			_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(TemplateCarteiraMetadata.ColumnNames.IdCarteira, 0, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = TemplateCarteiraMetadata.PropertyNames.IdCarteira;
			c.IsInPrimaryKey = true;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TemplateCarteiraMetadata.ColumnNames.Descricao, 1, typeof(System.String), esSystemType.String);
			c.PropertyName = TemplateCarteiraMetadata.PropertyNames.Descricao;
			c.CharacterMaxLength = 100;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
		}
		#endregion
	
		static public TemplateCarteiraMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base._dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base._columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string IdCarteira = "IdCarteira";
			 public const string Descricao = "Descricao";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string IdCarteira = "IdCarteira";
			 public const string Descricao = "Descricao";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(TemplateCarteiraMetadata))
			{
				if(TemplateCarteiraMetadata.mapDelegates == null)
				{
					TemplateCarteiraMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (TemplateCarteiraMetadata.meta == null)
				{
					TemplateCarteiraMetadata.meta = new TemplateCarteiraMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();
				

				meta.AddTypeMap("IdCarteira", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("Descricao", new esTypeMap("varchar", "System.String"));			
				
				
				
				meta.Source = "TemplateCarteira";
				meta.Destination = "TemplateCarteira";
				
				meta.spInsert = "proc_TemplateCarteiraInsert";				
				meta.spUpdate = "proc_TemplateCarteiraUpdate";		
				meta.spDelete = "proc_TemplateCarteiraDelete";
				meta.spLoadAll = "proc_TemplateCarteiraLoadAll";
				meta.spLoadByPrimaryKey = "proc_TemplateCarteiraLoadByPrimaryKey";
				
				this._providerMetadataMaps["esDefault"] = meta;
			}
			
			return this._providerMetadataMaps["esDefault"];
		}

		#endregion

		static private TemplateCarteiraMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
