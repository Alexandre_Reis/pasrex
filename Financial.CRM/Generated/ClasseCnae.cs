/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 04/09/2014 16:23:24
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;


using EntitySpaces.Interfaces;
using EntitySpaces.Core;





































		



			
					


		

		
		
		

		

























		






	

















				
		
		




		








		
				
				












	

















					
								
											
	












		





		




				
				








				




		

		
		
		
		
		





namespace Financial.CRM
{

	[Serializable]
	abstract public class esClasseCnaeCollection : esEntityCollection
	{
		public esClasseCnaeCollection()
		{

		}

		protected override string GetCollectionName()
		{
			return "ClasseCnaeCollection";
		}

		#region Query Logic
		protected void InitQuery(esClasseCnaeQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntityCollection)this).Connection;
		}

		protected bool OnQueryLoaded(DataTable table)
		{
			this.PopulateCollection(table);
			return (this.RowCount > 0) ? true : false;
		}
		
		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery(query as esClasseCnaeQuery);
		}
		#endregion
		
		virtual public ClasseCnae DetachEntity(ClasseCnae entity)
		{
			return base.DetachEntity(entity) as ClasseCnae;
		}
		
		virtual public ClasseCnae AttachEntity(ClasseCnae entity)
		{
			return base.AttachEntity(entity) as ClasseCnae;
		}
		
		virtual public void Combine(ClasseCnaeCollection collection)
		{
			base.Combine(collection);
		}
		
		new public ClasseCnae this[int index]
		{
			get
			{
				return base[index] as ClasseCnae;
			}
		}

		public override Type GetEntityType()
		{
			return typeof(ClasseCnae);
		}
	}



	[Serializable]
	abstract public class esClasseCnae : esEntity
	{
		/// <summary>
		/// Used internally by the entity's DynamicQuery mechanism.
		/// </summary>
		virtual protected esClasseCnaeQuery GetDynamicQuery()
		{
			return null;
		}

		public esClasseCnae()
		{

		}

		public esClasseCnae(DataRow row)
			: base(row)
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.Int32 idClasseCnae)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idClasseCnae);
			else
				return LoadByPrimaryKeyStoredProcedure(idClasseCnae);
		}

		/// <summary>
		/// Loads an entity by primary key
		/// </summary>
		/// <remarks>
		/// EntitySpaces requires primary keys be defined on all tables.
		/// If a table does not have a primary key set,
		/// this method will not compile.
		/// Does not support sqlAcessType. It only works with DynamicQuery
		/// </remarks>
		/// <param name="fieldsToReturn">Fields desired</param>
		public virtual bool LoadByPrimaryKey(List<esQueryItem> fieldsToReturn, System.Int32 idClasseCnae)
		{
			esQueryItem[] fields = fieldsToReturn.ToArray();			
			esClasseCnaeQuery query = this.GetDynamicQuery();
			query
			    .Select(fields)
			    .Where(query.IdClasseCnae == idClasseCnae);
		
			return query.Load();
		}
		
		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.Int32 idClasseCnae)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idClasseCnae);
			else
				return LoadByPrimaryKeyStoredProcedure(idClasseCnae);
		}

		private bool LoadByPrimaryKeyDynamic(System.Int32 idClasseCnae)
		{
			esClasseCnaeQuery query = this.GetDynamicQuery();
			query.Where(query.IdClasseCnae == idClasseCnae);
			return query.Load();
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.Int32 idClasseCnae)
		{
			esParameters parms = new esParameters();
			parms.Add("IdClasseCnae",idClasseCnae);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		
		
		#region Properties
		
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}

		public override void SetProperty(string name, object value)
		{
			if(this.Row == null) this.AddNew();
			
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value.GetType().ToString() == "System.String")
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "IdClasseCnae": this.str.IdClasseCnae = (string)value; break;							
						case "CdDivisao": this.str.CdDivisao = (string)value; break;							
						case "CdGrupo": this.str.CdGrupo = (string)value; break;							
						case "CdClasse": this.str.CdClasse = (string)value; break;							
						case "NomeClasse": this.str.NomeClasse = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "IdClasseCnae":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdClasseCnae = (System.Int32?)value;
							break;
						
						case "CdDivisao":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.CdDivisao = (System.Int32?)value;
							break;
						
						case "CdGrupo":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.CdGrupo = (System.Int32?)value;
							break;
						
						case "CdClasse":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.CdClasse = (System.Int32?)value;
							break;
					

						default:
							break;
					}
				}
			}
			else if(this.Row.Table.Columns.Contains(name))
			{
				this.Row[name] = value;
			}
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}
		
		
		/// <summary>
		/// Maps to ClasseCnae.IdClasseCnae
		/// </summary>
		virtual public System.Int32? IdClasseCnae
		{
			get
			{
				return base.GetSystemInt32(ClasseCnaeMetadata.ColumnNames.IdClasseCnae);
			}
			
			set
			{
				base.SetSystemInt32(ClasseCnaeMetadata.ColumnNames.IdClasseCnae, value);
			}
		}
		
		/// <summary>
		/// Maps to ClasseCnae.CdDivisao
		/// </summary>
		virtual public System.Int32? CdDivisao
		{
			get
			{
				return base.GetSystemInt32(ClasseCnaeMetadata.ColumnNames.CdDivisao);
			}
			
			set
			{
				base.SetSystemInt32(ClasseCnaeMetadata.ColumnNames.CdDivisao, value);
			}
		}
		
		/// <summary>
		/// Maps to ClasseCnae.CdGrupo
		/// </summary>
		virtual public System.Int32? CdGrupo
		{
			get
			{
				return base.GetSystemInt32(ClasseCnaeMetadata.ColumnNames.CdGrupo);
			}
			
			set
			{
				base.SetSystemInt32(ClasseCnaeMetadata.ColumnNames.CdGrupo, value);
			}
		}
		
		/// <summary>
		/// Maps to ClasseCnae.CdClasse
		/// </summary>
		virtual public System.Int32? CdClasse
		{
			get
			{
				return base.GetSystemInt32(ClasseCnaeMetadata.ColumnNames.CdClasse);
			}
			
			set
			{
				base.SetSystemInt32(ClasseCnaeMetadata.ColumnNames.CdClasse, value);
			}
		}
		
		/// <summary>
		/// Maps to ClasseCnae.NomeClasse
		/// </summary>
		virtual public System.String NomeClasse
		{
			get
			{
				return base.GetSystemString(ClasseCnaeMetadata.ColumnNames.NomeClasse);
			}
			
			set
			{
				base.SetSystemString(ClasseCnaeMetadata.ColumnNames.NomeClasse, value);
			}
		}
		
		#endregion	

		#region String Properties


		[BrowsableAttribute( false )]
		public esStrings str
		{
			get
			{
				if (esstrings == null)
				{
					esstrings = new esStrings(this);
				}
				return esstrings;
			}
		}


		[Serializable]
		sealed public class esStrings
		{
			public esStrings(esClasseCnae entity)
			{
				this.entity = entity;
			}
			
	
			public System.String IdClasseCnae
			{
				get
				{
					System.Int32? data = entity.IdClasseCnae;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdClasseCnae = null;
					else entity.IdClasseCnae = Convert.ToInt32(value);
				}
			}
				
			public System.String CdDivisao
			{
				get
				{
					System.Int32? data = entity.CdDivisao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.CdDivisao = null;
					else entity.CdDivisao = Convert.ToInt32(value);
				}
			}
				
			public System.String CdGrupo
			{
				get
				{
					System.Int32? data = entity.CdGrupo;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.CdGrupo = null;
					else entity.CdGrupo = Convert.ToInt32(value);
				}
			}
				
			public System.String CdClasse
			{
				get
				{
					System.Int32? data = entity.CdClasse;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.CdClasse = null;
					else entity.CdClasse = Convert.ToInt32(value);
				}
			}
				
			public System.String NomeClasse
			{
				get
				{
					System.String data = entity.NomeClasse;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.NomeClasse = null;
					else entity.NomeClasse = Convert.ToString(value);
				}
			}
			

			private esClasseCnae entity;
		}
		#endregion

		#region Query Logic
		protected void InitQuery(esClasseCnaeQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntity)this).Connection;
		}

		[System.Diagnostics.DebuggerNonUserCode]
		protected bool OnQueryLoaded(DataTable table)
		{
			bool dataFound = this.PopulateEntity(table);

			if (this.RowCount > 1)
			{
				throw new Exception("esClasseCnae can only hold one record of data");
			}

			return dataFound;
		}
		#endregion
		
		[NonSerialized]
		private esStrings esstrings;
	}


	
	public partial class ClasseCnae : esClasseCnae
	{

		
		/// <summary>
		/// Used internally by the entity's hierarchical properties.
		/// </summary>
		protected override List<esPropertyDescriptor> GetHierarchicalProperties()
		{
			List<esPropertyDescriptor> props = new List<esPropertyDescriptor>();
			
		
			return props;
		}	
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PreSave.
		/// </summary>
		protected override void ApplyPreSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostSave.
		/// </summary>
		protected override void ApplyPostSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostOneToOneSave.
		/// </summary>
		protected override void ApplyPostOneSaveKeys()
		{
		}
		
	}



	[Serializable]
	abstract public class esClasseCnaeQuery : esDynamicQuery
	{
		override protected IMetadata Meta
		{
			get
			{
				return ClasseCnaeMetadata.Meta();
			}
		}	
		

		public esQueryItem IdClasseCnae
		{
			get
			{
				return new esQueryItem(this, ClasseCnaeMetadata.ColumnNames.IdClasseCnae, esSystemType.Int32);
			}
		} 
		
		public esQueryItem CdDivisao
		{
			get
			{
				return new esQueryItem(this, ClasseCnaeMetadata.ColumnNames.CdDivisao, esSystemType.Int32);
			}
		} 
		
		public esQueryItem CdGrupo
		{
			get
			{
				return new esQueryItem(this, ClasseCnaeMetadata.ColumnNames.CdGrupo, esSystemType.Int32);
			}
		} 
		
		public esQueryItem CdClasse
		{
			get
			{
				return new esQueryItem(this, ClasseCnaeMetadata.ColumnNames.CdClasse, esSystemType.Int32);
			}
		} 
		
		public esQueryItem NomeClasse
		{
			get
			{
				return new esQueryItem(this, ClasseCnaeMetadata.ColumnNames.NomeClasse, esSystemType.String);
			}
		} 
		
	}



	[Serializable]
	[XmlType("ClasseCnaeCollection")]
	public partial class ClasseCnaeCollection : esClasseCnaeCollection, IEnumerable<ClasseCnae>
	{
		public ClasseCnaeCollection()
		{

		}
		
		public static implicit operator List<ClasseCnae>(ClasseCnaeCollection coll)
		{
			List<ClasseCnae> list = new List<ClasseCnae>();
			
			foreach (ClasseCnae emp in coll)
			{
				list.Add(emp);
			}
			
			return list;
		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return  ClasseCnaeMetadata.Meta();
			}
		}
		
		
		
		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new ClasseCnaeQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		
		override protected esEntity CreateEntityForCollection(DataRow row)
		{
			return new ClasseCnae(row);
		}

		override protected esEntity CreateEntity()
		{
			return new ClasseCnae();
		}
		
		
		#endregion


		[BrowsableAttribute( false )]
		public ClasseCnaeQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new ClasseCnaeQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}

		public bool Load(ClasseCnaeQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		public ClasseCnae AddNew()
		{
			ClasseCnae entity = base.AddNewEntity() as ClasseCnae;
			
			return entity;
		}

		public ClasseCnae FindByPrimaryKey(System.Int32 idClasseCnae)
		{
			return base.FindByPrimaryKey(idClasseCnae) as ClasseCnae;
		}


		#region IEnumerable<ClasseCnae> Members

		IEnumerator<ClasseCnae> IEnumerable<ClasseCnae>.GetEnumerator()
		{
			System.Collections.IEnumerable enumer = this as System.Collections.IEnumerable;
			System.Collections.IEnumerator iterator = enumer.GetEnumerator();

			while(iterator.MoveNext())
			{
				yield return iterator.Current as ClasseCnae;
			}
		}

		#endregion
		
		private ClasseCnaeQuery query;
	}


	/// <summary>
	/// Encapsulates the 'ClasseCnae' table
	/// </summary>

	[Serializable]
	public partial class ClasseCnae : esClasseCnae
	{
		public ClasseCnae()
		{

		}
	
		public ClasseCnae(DataRow row)
			: base(row)
		{

		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return ClasseCnaeMetadata.Meta();
			}
		}
		
		
		
		override protected esClasseCnaeQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new ClasseCnaeQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		#endregion
		



		[BrowsableAttribute( false )]
		public ClasseCnaeQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new ClasseCnaeQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}
		

		public bool Load(ClasseCnaeQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		private ClasseCnaeQuery query;
	}



	[Serializable]
	public partial class ClasseCnaeQuery : esClasseCnaeQuery
	{
		public ClasseCnaeQuery()
		{

		}		
		
		public ClasseCnaeQuery(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
			
	}



	[Serializable]
	public partial class ClasseCnaeMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected ClasseCnaeMetadata()
		{
			_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(ClasseCnaeMetadata.ColumnNames.IdClasseCnae, 0, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = ClasseCnaeMetadata.PropertyNames.IdClasseCnae;
			c.IsInPrimaryKey = true;
			c.IsAutoIncrement = true;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ClasseCnaeMetadata.ColumnNames.CdDivisao, 1, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = ClasseCnaeMetadata.PropertyNames.CdDivisao;	
			c.NumericPrecision = 10;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ClasseCnaeMetadata.ColumnNames.CdGrupo, 2, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = ClasseCnaeMetadata.PropertyNames.CdGrupo;	
			c.NumericPrecision = 10;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ClasseCnaeMetadata.ColumnNames.CdClasse, 3, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = ClasseCnaeMetadata.PropertyNames.CdClasse;	
			c.NumericPrecision = 10;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ClasseCnaeMetadata.ColumnNames.NomeClasse, 4, typeof(System.String), esSystemType.String);
			c.PropertyName = ClasseCnaeMetadata.PropertyNames.NomeClasse;
			c.CharacterMaxLength = 255;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
		}
		#endregion
	
		static public ClasseCnaeMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base._dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base._columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string IdClasseCnae = "IdClasseCnae";
			 public const string CdDivisao = "CdDivisao";
			 public const string CdGrupo = "CdGrupo";
			 public const string CdClasse = "CdClasse";
			 public const string NomeClasse = "NomeClasse";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string IdClasseCnae = "IdClasseCnae";
			 public const string CdDivisao = "CdDivisao";
			 public const string CdGrupo = "CdGrupo";
			 public const string CdClasse = "CdClasse";
			 public const string NomeClasse = "NomeClasse";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(ClasseCnaeMetadata))
			{
				if(ClasseCnaeMetadata.mapDelegates == null)
				{
					ClasseCnaeMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (ClasseCnaeMetadata.meta == null)
				{
					ClasseCnaeMetadata.meta = new ClasseCnaeMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();
				

				meta.AddTypeMap("IdClasseCnae", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("CdDivisao", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("CdGrupo", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("CdClasse", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("NomeClasse", new esTypeMap("varchar", "System.String"));			
				
				
				
				meta.Source = "ClasseCnae";
				meta.Destination = "ClasseCnae";
				
				meta.spInsert = "proc_ClasseCnaeInsert";				
				meta.spUpdate = "proc_ClasseCnaeUpdate";		
				meta.spDelete = "proc_ClasseCnaeDelete";
				meta.spLoadAll = "proc_ClasseCnaeLoadAll";
				meta.spLoadByPrimaryKey = "proc_ClasseCnaeLoadByPrimaryKey";
				
				this._providerMetadataMaps["esDefault"] = meta;
			}
			
			return this._providerMetadataMaps["esDefault"];
		}

		#endregion

		static private ClasseCnaeMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
