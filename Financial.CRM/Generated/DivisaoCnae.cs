/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 04/09/2014 16:23:25
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;


using EntitySpaces.Interfaces;
using EntitySpaces.Core;





































		



			
					


		

		
		
		

		

























		






	

















				
		
		




		








		
				
				












	

















					
								
											
	












		





		




				
				








				




		

		
		
		
		
		





namespace Financial.CRM
{

	[Serializable]
	abstract public class esDivisaoCnaeCollection : esEntityCollection
	{
		public esDivisaoCnaeCollection()
		{

		}

		protected override string GetCollectionName()
		{
			return "DivisaoCnaeCollection";
		}

		#region Query Logic
		protected void InitQuery(esDivisaoCnaeQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntityCollection)this).Connection;
		}

		protected bool OnQueryLoaded(DataTable table)
		{
			this.PopulateCollection(table);
			return (this.RowCount > 0) ? true : false;
		}
		
		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery(query as esDivisaoCnaeQuery);
		}
		#endregion
		
		virtual public DivisaoCnae DetachEntity(DivisaoCnae entity)
		{
			return base.DetachEntity(entity) as DivisaoCnae;
		}
		
		virtual public DivisaoCnae AttachEntity(DivisaoCnae entity)
		{
			return base.AttachEntity(entity) as DivisaoCnae;
		}
		
		virtual public void Combine(DivisaoCnaeCollection collection)
		{
			base.Combine(collection);
		}
		
		new public DivisaoCnae this[int index]
		{
			get
			{
				return base[index] as DivisaoCnae;
			}
		}

		public override Type GetEntityType()
		{
			return typeof(DivisaoCnae);
		}
	}



	[Serializable]
	abstract public class esDivisaoCnae : esEntity
	{
		/// <summary>
		/// Used internally by the entity's DynamicQuery mechanism.
		/// </summary>
		virtual protected esDivisaoCnaeQuery GetDynamicQuery()
		{
			return null;
		}

		public esDivisaoCnae()
		{

		}

		public esDivisaoCnae(DataRow row)
			: base(row)
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.Int32 idDivisaoCnae)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idDivisaoCnae);
			else
				return LoadByPrimaryKeyStoredProcedure(idDivisaoCnae);
		}

		/// <summary>
		/// Loads an entity by primary key
		/// </summary>
		/// <remarks>
		/// EntitySpaces requires primary keys be defined on all tables.
		/// If a table does not have a primary key set,
		/// this method will not compile.
		/// Does not support sqlAcessType. It only works with DynamicQuery
		/// </remarks>
		/// <param name="fieldsToReturn">Fields desired</param>
		public virtual bool LoadByPrimaryKey(List<esQueryItem> fieldsToReturn, System.Int32 idDivisaoCnae)
		{
			esQueryItem[] fields = fieldsToReturn.ToArray();			
			esDivisaoCnaeQuery query = this.GetDynamicQuery();
			query
			    .Select(fields)
			    .Where(query.IdDivisaoCnae == idDivisaoCnae);
		
			return query.Load();
		}
		
		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.Int32 idDivisaoCnae)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idDivisaoCnae);
			else
				return LoadByPrimaryKeyStoredProcedure(idDivisaoCnae);
		}

		private bool LoadByPrimaryKeyDynamic(System.Int32 idDivisaoCnae)
		{
			esDivisaoCnaeQuery query = this.GetDynamicQuery();
			query.Where(query.IdDivisaoCnae == idDivisaoCnae);
			return query.Load();
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.Int32 idDivisaoCnae)
		{
			esParameters parms = new esParameters();
			parms.Add("IdDivisaoCnae",idDivisaoCnae);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		
		
		#region Properties
		
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}

		public override void SetProperty(string name, object value)
		{
			if(this.Row == null) this.AddNew();
			
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value.GetType().ToString() == "System.String")
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "IdDivisaoCnae": this.str.IdDivisaoCnae = (string)value; break;							
						case "CdDivisaoCnae": this.str.CdDivisaoCnae = (string)value; break;							
						case "CdSecao": this.str.CdSecao = (string)value; break;							
						case "NomeDivisao": this.str.NomeDivisao = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "IdDivisaoCnae":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdDivisaoCnae = (System.Int32?)value;
							break;
						
						case "CdDivisaoCnae":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.CdDivisaoCnae = (System.Int32?)value;
							break;
					

						default:
							break;
					}
				}
			}
			else if(this.Row.Table.Columns.Contains(name))
			{
				this.Row[name] = value;
			}
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}
		
		
		/// <summary>
		/// Maps to DivisaoCnae.IdDivisaoCnae
		/// </summary>
		virtual public System.Int32? IdDivisaoCnae
		{
			get
			{
				return base.GetSystemInt32(DivisaoCnaeMetadata.ColumnNames.IdDivisaoCnae);
			}
			
			set
			{
				base.SetSystemInt32(DivisaoCnaeMetadata.ColumnNames.IdDivisaoCnae, value);
			}
		}
		
		/// <summary>
		/// Maps to DivisaoCnae.CdDivisaoCnae
		/// </summary>
		virtual public System.Int32? CdDivisaoCnae
		{
			get
			{
				return base.GetSystemInt32(DivisaoCnaeMetadata.ColumnNames.CdDivisaoCnae);
			}
			
			set
			{
				base.SetSystemInt32(DivisaoCnaeMetadata.ColumnNames.CdDivisaoCnae, value);
			}
		}
		
		/// <summary>
		/// Maps to DivisaoCnae.CdSecao
		/// </summary>
		virtual public System.String CdSecao
		{
			get
			{
				return base.GetSystemString(DivisaoCnaeMetadata.ColumnNames.CdSecao);
			}
			
			set
			{
				base.SetSystemString(DivisaoCnaeMetadata.ColumnNames.CdSecao, value);
			}
		}
		
		/// <summary>
		/// Maps to DivisaoCnae.NomeDivisao
		/// </summary>
		virtual public System.String NomeDivisao
		{
			get
			{
				return base.GetSystemString(DivisaoCnaeMetadata.ColumnNames.NomeDivisao);
			}
			
			set
			{
				base.SetSystemString(DivisaoCnaeMetadata.ColumnNames.NomeDivisao, value);
			}
		}
		
		#endregion	

		#region String Properties


		[BrowsableAttribute( false )]
		public esStrings str
		{
			get
			{
				if (esstrings == null)
				{
					esstrings = new esStrings(this);
				}
				return esstrings;
			}
		}


		[Serializable]
		sealed public class esStrings
		{
			public esStrings(esDivisaoCnae entity)
			{
				this.entity = entity;
			}
			
	
			public System.String IdDivisaoCnae
			{
				get
				{
					System.Int32? data = entity.IdDivisaoCnae;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdDivisaoCnae = null;
					else entity.IdDivisaoCnae = Convert.ToInt32(value);
				}
			}
				
			public System.String CdDivisaoCnae
			{
				get
				{
					System.Int32? data = entity.CdDivisaoCnae;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.CdDivisaoCnae = null;
					else entity.CdDivisaoCnae = Convert.ToInt32(value);
				}
			}
				
			public System.String CdSecao
			{
				get
				{
					System.String data = entity.CdSecao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.CdSecao = null;
					else entity.CdSecao = Convert.ToString(value);
				}
			}
				
			public System.String NomeDivisao
			{
				get
				{
					System.String data = entity.NomeDivisao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.NomeDivisao = null;
					else entity.NomeDivisao = Convert.ToString(value);
				}
			}
			

			private esDivisaoCnae entity;
		}
		#endregion

		#region Query Logic
		protected void InitQuery(esDivisaoCnaeQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntity)this).Connection;
		}

		[System.Diagnostics.DebuggerNonUserCode]
		protected bool OnQueryLoaded(DataTable table)
		{
			bool dataFound = this.PopulateEntity(table);

			if (this.RowCount > 1)
			{
				throw new Exception("esDivisaoCnae can only hold one record of data");
			}

			return dataFound;
		}
		#endregion
		
		[NonSerialized]
		private esStrings esstrings;
	}


	
	public partial class DivisaoCnae : esDivisaoCnae
	{

		
		/// <summary>
		/// Used internally by the entity's hierarchical properties.
		/// </summary>
		protected override List<esPropertyDescriptor> GetHierarchicalProperties()
		{
			List<esPropertyDescriptor> props = new List<esPropertyDescriptor>();
			
		
			return props;
		}	
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PreSave.
		/// </summary>
		protected override void ApplyPreSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostSave.
		/// </summary>
		protected override void ApplyPostSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostOneToOneSave.
		/// </summary>
		protected override void ApplyPostOneSaveKeys()
		{
		}
		
	}



	[Serializable]
	abstract public class esDivisaoCnaeQuery : esDynamicQuery
	{
		override protected IMetadata Meta
		{
			get
			{
				return DivisaoCnaeMetadata.Meta();
			}
		}	
		

		public esQueryItem IdDivisaoCnae
		{
			get
			{
				return new esQueryItem(this, DivisaoCnaeMetadata.ColumnNames.IdDivisaoCnae, esSystemType.Int32);
			}
		} 
		
		public esQueryItem CdDivisaoCnae
		{
			get
			{
				return new esQueryItem(this, DivisaoCnaeMetadata.ColumnNames.CdDivisaoCnae, esSystemType.Int32);
			}
		} 
		
		public esQueryItem CdSecao
		{
			get
			{
				return new esQueryItem(this, DivisaoCnaeMetadata.ColumnNames.CdSecao, esSystemType.String);
			}
		} 
		
		public esQueryItem NomeDivisao
		{
			get
			{
				return new esQueryItem(this, DivisaoCnaeMetadata.ColumnNames.NomeDivisao, esSystemType.String);
			}
		} 
		
	}



	[Serializable]
	[XmlType("DivisaoCnaeCollection")]
	public partial class DivisaoCnaeCollection : esDivisaoCnaeCollection, IEnumerable<DivisaoCnae>
	{
		public DivisaoCnaeCollection()
		{

		}
		
		public static implicit operator List<DivisaoCnae>(DivisaoCnaeCollection coll)
		{
			List<DivisaoCnae> list = new List<DivisaoCnae>();
			
			foreach (DivisaoCnae emp in coll)
			{
				list.Add(emp);
			}
			
			return list;
		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return  DivisaoCnaeMetadata.Meta();
			}
		}
		
		
		
		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new DivisaoCnaeQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		
		override protected esEntity CreateEntityForCollection(DataRow row)
		{
			return new DivisaoCnae(row);
		}

		override protected esEntity CreateEntity()
		{
			return new DivisaoCnae();
		}
		
		
		#endregion


		[BrowsableAttribute( false )]
		public DivisaoCnaeQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new DivisaoCnaeQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}

		public bool Load(DivisaoCnaeQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		public DivisaoCnae AddNew()
		{
			DivisaoCnae entity = base.AddNewEntity() as DivisaoCnae;
			
			return entity;
		}

		public DivisaoCnae FindByPrimaryKey(System.Int32 idDivisaoCnae)
		{
			return base.FindByPrimaryKey(idDivisaoCnae) as DivisaoCnae;
		}


		#region IEnumerable<DivisaoCnae> Members

		IEnumerator<DivisaoCnae> IEnumerable<DivisaoCnae>.GetEnumerator()
		{
			System.Collections.IEnumerable enumer = this as System.Collections.IEnumerable;
			System.Collections.IEnumerator iterator = enumer.GetEnumerator();

			while(iterator.MoveNext())
			{
				yield return iterator.Current as DivisaoCnae;
			}
		}

		#endregion
		
		private DivisaoCnaeQuery query;
	}


	/// <summary>
	/// Encapsulates the 'DivisaoCnae' table
	/// </summary>

	[Serializable]
	public partial class DivisaoCnae : esDivisaoCnae
	{
		public DivisaoCnae()
		{

		}
	
		public DivisaoCnae(DataRow row)
			: base(row)
		{

		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return DivisaoCnaeMetadata.Meta();
			}
		}
		
		
		
		override protected esDivisaoCnaeQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new DivisaoCnaeQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		#endregion
		



		[BrowsableAttribute( false )]
		public DivisaoCnaeQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new DivisaoCnaeQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}
		

		public bool Load(DivisaoCnaeQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		private DivisaoCnaeQuery query;
	}



	[Serializable]
	public partial class DivisaoCnaeQuery : esDivisaoCnaeQuery
	{
		public DivisaoCnaeQuery()
		{

		}		
		
		public DivisaoCnaeQuery(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
			
	}



	[Serializable]
	public partial class DivisaoCnaeMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected DivisaoCnaeMetadata()
		{
			_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(DivisaoCnaeMetadata.ColumnNames.IdDivisaoCnae, 0, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = DivisaoCnaeMetadata.PropertyNames.IdDivisaoCnae;
			c.IsInPrimaryKey = true;
			c.IsAutoIncrement = true;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(DivisaoCnaeMetadata.ColumnNames.CdDivisaoCnae, 1, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = DivisaoCnaeMetadata.PropertyNames.CdDivisaoCnae;	
			c.NumericPrecision = 10;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(DivisaoCnaeMetadata.ColumnNames.CdSecao, 2, typeof(System.String), esSystemType.String);
			c.PropertyName = DivisaoCnaeMetadata.PropertyNames.CdSecao;
			c.CharacterMaxLength = 255;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(DivisaoCnaeMetadata.ColumnNames.NomeDivisao, 3, typeof(System.String), esSystemType.String);
			c.PropertyName = DivisaoCnaeMetadata.PropertyNames.NomeDivisao;
			c.CharacterMaxLength = 255;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
		}
		#endregion
	
		static public DivisaoCnaeMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base._dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base._columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string IdDivisaoCnae = "IdDivisaoCnae";
			 public const string CdDivisaoCnae = "CdDivisaoCnae";
			 public const string CdSecao = "CdSecao";
			 public const string NomeDivisao = "NomeDivisao";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string IdDivisaoCnae = "IdDivisaoCnae";
			 public const string CdDivisaoCnae = "CdDivisaoCnae";
			 public const string CdSecao = "CdSecao";
			 public const string NomeDivisao = "NomeDivisao";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(DivisaoCnaeMetadata))
			{
				if(DivisaoCnaeMetadata.mapDelegates == null)
				{
					DivisaoCnaeMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (DivisaoCnaeMetadata.meta == null)
				{
					DivisaoCnaeMetadata.meta = new DivisaoCnaeMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();
				

				meta.AddTypeMap("IdDivisaoCnae", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("CdDivisaoCnae", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("CdSecao", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("NomeDivisao", new esTypeMap("varchar", "System.String"));			
				
				
				
				meta.Source = "DivisaoCnae";
				meta.Destination = "DivisaoCnae";
				
				meta.spInsert = "proc_DivisaoCnaeInsert";				
				meta.spUpdate = "proc_DivisaoCnaeUpdate";		
				meta.spDelete = "proc_DivisaoCnaeDelete";
				meta.spLoadAll = "proc_DivisaoCnaeLoadAll";
				meta.spLoadByPrimaryKey = "proc_DivisaoCnaeLoadByPrimaryKey";
				
				this._providerMetadataMaps["esDefault"] = meta;
			}
			
			return this._providerMetadataMaps["esDefault"];
		}

		#endregion

		static private DivisaoCnaeMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
