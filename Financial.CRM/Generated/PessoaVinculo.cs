/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 23/05/2014 15:02:36
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;


using EntitySpaces.Interfaces;
using EntitySpaces.Core;





































		



			
					


		

		
		
		

		

























		






	

















				
		
		




		








		
				
				












	

















					
								
											
	










		





		




				
				








				




		

		
		
		
		
		





namespace Financial.CRM
{

	[Serializable]
	abstract public class esPessoaVinculoCollection : esEntityCollection
	{
		public esPessoaVinculoCollection()
		{

		}

		protected override string GetCollectionName()
		{
			return "PessoaVinculoCollection";
		}

		#region Query Logic
		protected void InitQuery(esPessoaVinculoQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntityCollection)this).Connection;
		}

		protected bool OnQueryLoaded(DataTable table)
		{
			this.PopulateCollection(table);
			return (this.RowCount > 0) ? true : false;
		}
		
		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery(query as esPessoaVinculoQuery);
		}
		#endregion
		
		virtual public PessoaVinculo DetachEntity(PessoaVinculo entity)
		{
			return base.DetachEntity(entity) as PessoaVinculo;
		}
		
		virtual public PessoaVinculo AttachEntity(PessoaVinculo entity)
		{
			return base.AttachEntity(entity) as PessoaVinculo;
		}
		
		virtual public void Combine(PessoaVinculoCollection collection)
		{
			base.Combine(collection);
		}
		
		new public PessoaVinculo this[int index]
		{
			get
			{
				return base[index] as PessoaVinculo;
			}
		}

		public override Type GetEntityType()
		{
			return typeof(PessoaVinculo);
		}
	}



	[Serializable]
	abstract public class esPessoaVinculo : esEntity
	{
		/// <summary>
		/// Used internally by the entity's DynamicQuery mechanism.
		/// </summary>
		virtual protected esPessoaVinculoQuery GetDynamicQuery()
		{
			return null;
		}

		public esPessoaVinculo()
		{

		}

		public esPessoaVinculo(DataRow row)
			: base(row)
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.Int32 idPessoa, System.Int32 idPessoaVinculada)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idPessoa, idPessoaVinculada);
			else
				return LoadByPrimaryKeyStoredProcedure(idPessoa, idPessoaVinculada);
		}

		/// <summary>
		/// Loads an entity by primary key
		/// </summary>
		/// <remarks>
		/// EntitySpaces requires primary keys be defined on all tables.
		/// If a table does not have a primary key set,
		/// this method will not compile.
		/// Does not support sqlAcessType. It only works with DynamicQuery
		/// </remarks>
		/// <param name="fieldsToReturn">Fields desired</param>
		public virtual bool LoadByPrimaryKey(List<esQueryItem> fieldsToReturn, System.Int32 idPessoa, System.Int32 idPessoaVinculada)
		{
			esQueryItem[] fields = fieldsToReturn.ToArray();			
			esPessoaVinculoQuery query = this.GetDynamicQuery();
			query
			    .Select(fields)
			    .Where(query.IdPessoa == idPessoa, query.IdPessoaVinculada == idPessoaVinculada);
		
			return query.Load();
		}
		
		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.Int32 idPessoa, System.Int32 idPessoaVinculada)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idPessoa, idPessoaVinculada);
			else
				return LoadByPrimaryKeyStoredProcedure(idPessoa, idPessoaVinculada);
		}

		private bool LoadByPrimaryKeyDynamic(System.Int32 idPessoa, System.Int32 idPessoaVinculada)
		{
			esPessoaVinculoQuery query = this.GetDynamicQuery();
			query.Where(query.IdPessoa == idPessoa, query.IdPessoaVinculada == idPessoaVinculada);
			return query.Load();
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.Int32 idPessoa, System.Int32 idPessoaVinculada)
		{
			esParameters parms = new esParameters();
			parms.Add("IdPessoa",idPessoa);			parms.Add("IdPessoaVinculada",idPessoaVinculada);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		
		
		#region Properties
		
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}

		public override void SetProperty(string name, object value)
		{
			if(this.Row == null) this.AddNew();
			
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value.GetType().ToString() == "System.String")
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "IdPessoa": this.str.IdPessoa = (string)value; break;							
						case "IdPessoaVinculada": this.str.IdPessoaVinculada = (string)value; break;							
						case "TipoVinculo": this.str.TipoVinculo = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "IdPessoa":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdPessoa = (System.Int32?)value;
							break;
						
						case "IdPessoaVinculada":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdPessoaVinculada = (System.Int32?)value;
							break;
						
						case "TipoVinculo":
						
							if (value == null || value.GetType().ToString() == "System.Byte")
								this.TipoVinculo = (System.Byte?)value;
							break;
					

						default:
							break;
					}
				}
			}
			else if(this.Row.Table.Columns.Contains(name))
			{
				this.Row[name] = value;
			}
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}
		
		
		/// <summary>
		/// Maps to PessoaVinculo.IdPessoa
		/// </summary>
		virtual public System.Int32? IdPessoa
		{
			get
			{
				return base.GetSystemInt32(PessoaVinculoMetadata.ColumnNames.IdPessoa);
			}
			
			set
			{
				base.SetSystemInt32(PessoaVinculoMetadata.ColumnNames.IdPessoa, value);
			}
		}
		
		/// <summary>
		/// Maps to PessoaVinculo.IdPessoaVinculada
		/// </summary>
		virtual public System.Int32? IdPessoaVinculada
		{
			get
			{
				return base.GetSystemInt32(PessoaVinculoMetadata.ColumnNames.IdPessoaVinculada);
			}
			
			set
			{
				base.SetSystemInt32(PessoaVinculoMetadata.ColumnNames.IdPessoaVinculada, value);
			}
		}
		
		/// <summary>
		/// Maps to PessoaVinculo.TipoVinculo
		/// </summary>
		virtual public System.Byte? TipoVinculo
		{
			get
			{
				return base.GetSystemByte(PessoaVinculoMetadata.ColumnNames.TipoVinculo);
			}
			
			set
			{
				base.SetSystemByte(PessoaVinculoMetadata.ColumnNames.TipoVinculo, value);
			}
		}
		
		#endregion	

		#region String Properties


		[BrowsableAttribute( false )]
		public esStrings str
		{
			get
			{
				if (esstrings == null)
				{
					esstrings = new esStrings(this);
				}
				return esstrings;
			}
		}


		[Serializable]
		sealed public class esStrings
		{
			public esStrings(esPessoaVinculo entity)
			{
				this.entity = entity;
			}
			
	
			public System.String IdPessoa
			{
				get
				{
					System.Int32? data = entity.IdPessoa;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdPessoa = null;
					else entity.IdPessoa = Convert.ToInt32(value);
				}
			}
				
			public System.String IdPessoaVinculada
			{
				get
				{
					System.Int32? data = entity.IdPessoaVinculada;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdPessoaVinculada = null;
					else entity.IdPessoaVinculada = Convert.ToInt32(value);
				}
			}
				
			public System.String TipoVinculo
			{
				get
				{
					System.Byte? data = entity.TipoVinculo;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.TipoVinculo = null;
					else entity.TipoVinculo = Convert.ToByte(value);
				}
			}
			

			private esPessoaVinculo entity;
		}
		#endregion

		#region Query Logic
		protected void InitQuery(esPessoaVinculoQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntity)this).Connection;
		}

		[System.Diagnostics.DebuggerNonUserCode]
		protected bool OnQueryLoaded(DataTable table)
		{
			bool dataFound = this.PopulateEntity(table);

			if (this.RowCount > 1)
			{
				throw new Exception("esPessoaVinculo can only hold one record of data");
			}

			return dataFound;
		}
		#endregion
		
		[NonSerialized]
		private esStrings esstrings;
	}


	
	public partial class PessoaVinculo : esPessoaVinculo
	{

		
		/// <summary>
		/// Used internally by the entity's hierarchical properties.
		/// </summary>
		protected override List<esPropertyDescriptor> GetHierarchicalProperties()
		{
			List<esPropertyDescriptor> props = new List<esPropertyDescriptor>();
			
		
			return props;
		}	
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PreSave.
		/// </summary>
		protected override void ApplyPreSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostSave.
		/// </summary>
		protected override void ApplyPostSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostOneToOneSave.
		/// </summary>
		protected override void ApplyPostOneSaveKeys()
		{
		}
		
	}



	[Serializable]
	abstract public class esPessoaVinculoQuery : esDynamicQuery
	{
		override protected IMetadata Meta
		{
			get
			{
				return PessoaVinculoMetadata.Meta();
			}
		}	
		

		public esQueryItem IdPessoa
		{
			get
			{
				return new esQueryItem(this, PessoaVinculoMetadata.ColumnNames.IdPessoa, esSystemType.Int32);
			}
		} 
		
		public esQueryItem IdPessoaVinculada
		{
			get
			{
				return new esQueryItem(this, PessoaVinculoMetadata.ColumnNames.IdPessoaVinculada, esSystemType.Int32);
			}
		} 
		
		public esQueryItem TipoVinculo
		{
			get
			{
				return new esQueryItem(this, PessoaVinculoMetadata.ColumnNames.TipoVinculo, esSystemType.Byte);
			}
		} 
		
	}



	[Serializable]
	[XmlType("PessoaVinculoCollection")]
	public partial class PessoaVinculoCollection : esPessoaVinculoCollection, IEnumerable<PessoaVinculo>
	{
		public PessoaVinculoCollection()
		{

		}
		
		public static implicit operator List<PessoaVinculo>(PessoaVinculoCollection coll)
		{
			List<PessoaVinculo> list = new List<PessoaVinculo>();
			
			foreach (PessoaVinculo emp in coll)
			{
				list.Add(emp);
			}
			
			return list;
		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return  PessoaVinculoMetadata.Meta();
			}
		}
		
		
		
		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new PessoaVinculoQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		
		override protected esEntity CreateEntityForCollection(DataRow row)
		{
			return new PessoaVinculo(row);
		}

		override protected esEntity CreateEntity()
		{
			return new PessoaVinculo();
		}
		
		
		#endregion


		[BrowsableAttribute( false )]
		public PessoaVinculoQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new PessoaVinculoQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}

		public bool Load(PessoaVinculoQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		public PessoaVinculo AddNew()
		{
			PessoaVinculo entity = base.AddNewEntity() as PessoaVinculo;
			
			return entity;
		}

		public PessoaVinculo FindByPrimaryKey(System.Int32 idPessoa, System.Int32 idPessoaVinculada)
		{
			return base.FindByPrimaryKey(idPessoa, idPessoaVinculada) as PessoaVinculo;
		}


		#region IEnumerable<PessoaVinculo> Members

		IEnumerator<PessoaVinculo> IEnumerable<PessoaVinculo>.GetEnumerator()
		{
			System.Collections.IEnumerable enumer = this as System.Collections.IEnumerable;
			System.Collections.IEnumerator iterator = enumer.GetEnumerator();

			while(iterator.MoveNext())
			{
				yield return iterator.Current as PessoaVinculo;
			}
		}

		#endregion
		
		private PessoaVinculoQuery query;
	}


	/// <summary>
	/// Encapsulates the 'PessoaVinculo' table
	/// </summary>

	[Serializable]
	public partial class PessoaVinculo : esPessoaVinculo
	{
		public PessoaVinculo()
		{

		}
	
		public PessoaVinculo(DataRow row)
			: base(row)
		{

		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return PessoaVinculoMetadata.Meta();
			}
		}
		
		
		
		override protected esPessoaVinculoQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new PessoaVinculoQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		#endregion
		



		[BrowsableAttribute( false )]
		public PessoaVinculoQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new PessoaVinculoQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}
		

		public bool Load(PessoaVinculoQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		private PessoaVinculoQuery query;
	}



	[Serializable]
	public partial class PessoaVinculoQuery : esPessoaVinculoQuery
	{
		public PessoaVinculoQuery()
		{

		}		
		
		public PessoaVinculoQuery(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
			
	}



	[Serializable]
	public partial class PessoaVinculoMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected PessoaVinculoMetadata()
		{
			_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(PessoaVinculoMetadata.ColumnNames.IdPessoa, 0, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = PessoaVinculoMetadata.PropertyNames.IdPessoa;
			c.IsInPrimaryKey = true;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PessoaVinculoMetadata.ColumnNames.IdPessoaVinculada, 1, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = PessoaVinculoMetadata.PropertyNames.IdPessoaVinculada;
			c.IsInPrimaryKey = true;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PessoaVinculoMetadata.ColumnNames.TipoVinculo, 2, typeof(System.Byte), esSystemType.Byte);
			c.PropertyName = PessoaVinculoMetadata.PropertyNames.TipoVinculo;	
			c.NumericPrecision = 3;
			_columns.Add(c); 
			
				
		}
		#endregion
	
		static public PessoaVinculoMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base._dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base._columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string IdPessoa = "IdPessoa";
			 public const string IdPessoaVinculada = "IdPessoaVinculada";
			 public const string TipoVinculo = "TipoVinculo";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string IdPessoa = "IdPessoa";
			 public const string IdPessoaVinculada = "IdPessoaVinculada";
			 public const string TipoVinculo = "TipoVinculo";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(PessoaVinculoMetadata))
			{
				if(PessoaVinculoMetadata.mapDelegates == null)
				{
					PessoaVinculoMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (PessoaVinculoMetadata.meta == null)
				{
					PessoaVinculoMetadata.meta = new PessoaVinculoMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();
				

				meta.AddTypeMap("IdPessoa", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("IdPessoaVinculada", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("TipoVinculo", new esTypeMap("tinyint", "System.Byte"));			
				
				
				
				meta.Source = "PessoaVinculo";
				meta.Destination = "PessoaVinculo";
				
				meta.spInsert = "proc_PessoaVinculoInsert";				
				meta.spUpdate = "proc_PessoaVinculoUpdate";		
				meta.spDelete = "proc_PessoaVinculoDelete";
				meta.spLoadAll = "proc_PessoaVinculoLoadAll";
				meta.spLoadByPrimaryKey = "proc_PessoaVinculoLoadByPrimaryKey";
				
				this._providerMetadataMaps["esDefault"] = meta;
			}
			
			return this._providerMetadataMaps["esDefault"];
		}

		#endregion

		static private PessoaVinculoMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
