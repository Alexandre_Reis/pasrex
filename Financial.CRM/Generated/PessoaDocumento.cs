/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 23/05/2014 15:02:35
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;


using EntitySpaces.Interfaces;
using EntitySpaces.Core;





































		



			
					


		

		
		
		

		

























		






	

















				
		
		




		








		
				
				












	

















					
								
											
	










		





		




				
				








				




		

		
		
		
		
		





namespace Financial.CRM
{

	[Serializable]
	abstract public class esPessoaDocumentoCollection : esEntityCollection
	{
		public esPessoaDocumentoCollection()
		{

		}

		protected override string GetCollectionName()
		{
			return "PessoaDocumentoCollection";
		}

		#region Query Logic
		protected void InitQuery(esPessoaDocumentoQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntityCollection)this).Connection;
		}

		protected bool OnQueryLoaded(DataTable table)
		{
			this.PopulateCollection(table);
			return (this.RowCount > 0) ? true : false;
		}
		
		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery(query as esPessoaDocumentoQuery);
		}
		#endregion
		
		virtual public PessoaDocumento DetachEntity(PessoaDocumento entity)
		{
			return base.DetachEntity(entity) as PessoaDocumento;
		}
		
		virtual public PessoaDocumento AttachEntity(PessoaDocumento entity)
		{
			return base.AttachEntity(entity) as PessoaDocumento;
		}
		
		virtual public void Combine(PessoaDocumentoCollection collection)
		{
			base.Combine(collection);
		}
		
		new public PessoaDocumento this[int index]
		{
			get
			{
				return base[index] as PessoaDocumento;
			}
		}

		public override Type GetEntityType()
		{
			return typeof(PessoaDocumento);
		}
	}



	[Serializable]
	abstract public class esPessoaDocumento : esEntity
	{
		/// <summary>
		/// Used internally by the entity's DynamicQuery mechanism.
		/// </summary>
		virtual protected esPessoaDocumentoQuery GetDynamicQuery()
		{
			return null;
		}

		public esPessoaDocumento()
		{

		}

		public esPessoaDocumento(DataRow row)
			: base(row)
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.Int32 idDocumento)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idDocumento);
			else
				return LoadByPrimaryKeyStoredProcedure(idDocumento);
		}

		/// <summary>
		/// Loads an entity by primary key
		/// </summary>
		/// <remarks>
		/// EntitySpaces requires primary keys be defined on all tables.
		/// If a table does not have a primary key set,
		/// this method will not compile.
		/// Does not support sqlAcessType. It only works with DynamicQuery
		/// </remarks>
		/// <param name="fieldsToReturn">Fields desired</param>
		public virtual bool LoadByPrimaryKey(List<esQueryItem> fieldsToReturn, System.Int32 idDocumento)
		{
			esQueryItem[] fields = fieldsToReturn.ToArray();			
			esPessoaDocumentoQuery query = this.GetDynamicQuery();
			query
			    .Select(fields)
			    .Where(query.IdDocumento == idDocumento);
		
			return query.Load();
		}
		
		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.Int32 idDocumento)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idDocumento);
			else
				return LoadByPrimaryKeyStoredProcedure(idDocumento);
		}

		private bool LoadByPrimaryKeyDynamic(System.Int32 idDocumento)
		{
			esPessoaDocumentoQuery query = this.GetDynamicQuery();
			query.Where(query.IdDocumento == idDocumento);
			return query.Load();
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.Int32 idDocumento)
		{
			esParameters parms = new esParameters();
			parms.Add("IdDocumento",idDocumento);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		
		
		#region Properties
		
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}

		public override void SetProperty(string name, object value)
		{
			if(this.Row == null) this.AddNew();
			
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value.GetType().ToString() == "System.String")
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "IdDocumento": this.str.IdDocumento = (string)value; break;							
						case "IdPessoa": this.str.IdPessoa = (string)value; break;							
						case "TipoDocumento": this.str.TipoDocumento = (string)value; break;							
						case "NumeroDocumento": this.str.NumeroDocumento = (string)value; break;							
						case "DataExpedicao": this.str.DataExpedicao = (string)value; break;							
						case "OrgaoEmissor": this.str.OrgaoEmissor = (string)value; break;							
						case "UFEmissor": this.str.UFEmissor = (string)value; break;
                        case "DataVencimento": this.str.DataVencimento = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "IdDocumento":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdDocumento = (System.Int32?)value;
							break;
						
						case "IdPessoa":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdPessoa = (System.Int32?)value;
							break;
						
						case "TipoDocumento":
						
							if (value == null || value.GetType().ToString() == "System.Byte")
								this.TipoDocumento = (System.Byte?)value;
							break;
						
						case "DataExpedicao":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.DataExpedicao = (System.DateTime?)value;
							break;

                        case "DataVencimento":

                            if (value == null || value.GetType().ToString() == "System.DateTime")
                                this.DataVencimento = (System.DateTime?)value;
                            break;

						default:
							break;
					}
				}
			}
			else if(this.Row.Table.Columns.Contains(name))
			{
				this.Row[name] = value;
			}
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}
		
		
		/// <summary>
		/// Maps to PessoaDocumento.IdDocumento
		/// </summary>
		virtual public System.Int32? IdDocumento
		{
			get
			{
				return base.GetSystemInt32(PessoaDocumentoMetadata.ColumnNames.IdDocumento);
			}
			
			set
			{
				base.SetSystemInt32(PessoaDocumentoMetadata.ColumnNames.IdDocumento, value);
			}
		}
		
		/// <summary>
		/// Maps to PessoaDocumento.IdPessoa
		/// </summary>
		virtual public System.Int32? IdPessoa
		{
			get
			{
				return base.GetSystemInt32(PessoaDocumentoMetadata.ColumnNames.IdPessoa);
			}
			
			set
			{
				if(base.SetSystemInt32(PessoaDocumentoMetadata.ColumnNames.IdPessoa, value))
				{
					this._UpToPessoaByIdPessoa = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to PessoaDocumento.TipoDocumento
		/// </summary>
		virtual public System.Byte? TipoDocumento
		{
			get
			{
				return base.GetSystemByte(PessoaDocumentoMetadata.ColumnNames.TipoDocumento);
			}
			
			set
			{
				base.SetSystemByte(PessoaDocumentoMetadata.ColumnNames.TipoDocumento, value);
			}
		}
		
		/// <summary>
		/// Maps to PessoaDocumento.NumeroDocumento
		/// </summary>
		virtual public System.String NumeroDocumento
		{
			get
			{
				return base.GetSystemString(PessoaDocumentoMetadata.ColumnNames.NumeroDocumento);
			}
			
			set
			{
				base.SetSystemString(PessoaDocumentoMetadata.ColumnNames.NumeroDocumento, value);
			}
		}
		
		/// <summary>
		/// Maps to PessoaDocumento.DataExpedicao
		/// </summary>
		virtual public System.DateTime? DataExpedicao
		{
			get
			{
				return base.GetSystemDateTime(PessoaDocumentoMetadata.ColumnNames.DataExpedicao);
			}
			
			set
			{
				base.SetSystemDateTime(PessoaDocumentoMetadata.ColumnNames.DataExpedicao, value);
			}
		}
		
		/// <summary>
		/// Maps to PessoaDocumento.OrgaoEmissor
		/// </summary>
		virtual public System.String OrgaoEmissor
		{
			get
			{
				return base.GetSystemString(PessoaDocumentoMetadata.ColumnNames.OrgaoEmissor);
			}
			
			set
			{
				base.SetSystemString(PessoaDocumentoMetadata.ColumnNames.OrgaoEmissor, value);
			}
		}
		
		/// <summary>
		/// Maps to PessoaDocumento.UFEmissor
		/// </summary>
		virtual public System.String UFEmissor
		{
			get
			{
				return base.GetSystemString(PessoaDocumentoMetadata.ColumnNames.UFEmissor);
			}
			
			set
			{
				base.SetSystemString(PessoaDocumentoMetadata.ColumnNames.UFEmissor, value);
			}
		}

        /// <summary>
        /// Maps to PessoaDocumento.DataVencimento
        /// </summary>
        virtual public System.DateTime? DataVencimento
        {
            get
            {
                return base.GetSystemDateTime(PessoaDocumentoMetadata.ColumnNames.DataVencimento);
            }

            set
            {
                base.SetSystemDateTime(PessoaDocumentoMetadata.ColumnNames.DataVencimento, value);
            }
        }

		[CLSCompliant(false)]
		internal protected Pessoa _UpToPessoaByIdPessoa;
		#endregion	

		#region String Properties


		[BrowsableAttribute( false )]
		public esStrings str
		{
			get
			{
				if (esstrings == null)
				{
					esstrings = new esStrings(this);
				}
				return esstrings;
			}
		}


		[Serializable]
		sealed public class esStrings
		{
			public esStrings(esPessoaDocumento entity)
			{
				this.entity = entity;
			}
			
	
			public System.String IdDocumento
			{
				get
				{
					System.Int32? data = entity.IdDocumento;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdDocumento = null;
					else entity.IdDocumento = Convert.ToInt32(value);
				}
			}
				
			public System.String IdPessoa
			{
				get
				{
					System.Int32? data = entity.IdPessoa;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdPessoa = null;
					else entity.IdPessoa = Convert.ToInt32(value);
				}
			}
				
			public System.String TipoDocumento
			{
				get
				{
					System.Byte? data = entity.TipoDocumento;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.TipoDocumento = null;
					else entity.TipoDocumento = Convert.ToByte(value);
				}
			}
				
			public System.String NumeroDocumento
			{
				get
				{
					System.String data = entity.NumeroDocumento;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.NumeroDocumento = null;
					else entity.NumeroDocumento = Convert.ToString(value);
				}
			}
				
			public System.String DataExpedicao
			{
				get
				{
					System.DateTime? data = entity.DataExpedicao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DataExpedicao = null;
					else entity.DataExpedicao = Convert.ToDateTime(value);
				}
			}
				
			public System.String OrgaoEmissor
			{
				get
				{
					System.String data = entity.OrgaoEmissor;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.OrgaoEmissor = null;
					else entity.OrgaoEmissor = Convert.ToString(value);
				}
			}
				
			public System.String UFEmissor
			{
				get
				{
					System.String data = entity.UFEmissor;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.UFEmissor = null;
					else entity.UFEmissor = Convert.ToString(value);
				}
			}

            public System.String DataVencimento
            {
                get
                {
                    System.DateTime? data = entity.DataVencimento;
                    return (data == null) ? String.Empty : Convert.ToString(data);
                }

                set
                {
                    if (value == null || value.Length == 0) entity.DataVencimento = null;
                    else entity.DataVencimento = Convert.ToDateTime(value);
                }
            }

			private esPessoaDocumento entity;
		}
		#endregion

		#region Query Logic
		protected void InitQuery(esPessoaDocumentoQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntity)this).Connection;
		}

		[System.Diagnostics.DebuggerNonUserCode]
		protected bool OnQueryLoaded(DataTable table)
		{
			bool dataFound = this.PopulateEntity(table);

			if (this.RowCount > 1)
			{
				throw new Exception("esPessoaDocumento can only hold one record of data");
			}

			return dataFound;
		}
		#endregion
		
		[NonSerialized]
		private esStrings esstrings;
	}


	
	public partial class PessoaDocumento : esPessoaDocumento
	{

				
		#region UpToPessoaByIdPessoa - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - FK_PessoaDocumento_Pessoa
		/// </summary>

		[XmlIgnore]
		public Pessoa UpToPessoaByIdPessoa
		{
			get
			{
				if(this._UpToPessoaByIdPessoa == null
					&& IdPessoa != null					)
				{
					this._UpToPessoaByIdPessoa = new Pessoa();
					this._UpToPessoaByIdPessoa.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToPessoaByIdPessoa", this._UpToPessoaByIdPessoa);
					this._UpToPessoaByIdPessoa.Query.Where(this._UpToPessoaByIdPessoa.Query.IdPessoa == this.IdPessoa);
					this._UpToPessoaByIdPessoa.Query.Load();
				}

				return this._UpToPessoaByIdPessoa;
			}
			
			set
			{
				this.RemovePreSave("UpToPessoaByIdPessoa");
				

				if(value == null)
				{
					this.IdPessoa = null;
					this._UpToPessoaByIdPessoa = null;
				}
				else
				{
					this.IdPessoa = value.IdPessoa;
					this._UpToPessoaByIdPessoa = value;
					this.SetPreSave("UpToPessoaByIdPessoa", this._UpToPessoaByIdPessoa);
				}
				
			}
		}
		#endregion
		

		
		/// <summary>
		/// Used internally by the entity's hierarchical properties.
		/// </summary>
		protected override List<esPropertyDescriptor> GetHierarchicalProperties()
		{
			List<esPropertyDescriptor> props = new List<esPropertyDescriptor>();
			
		
			return props;
		}	
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PreSave.
		/// </summary>
		protected override void ApplyPreSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostSave.
		/// </summary>
		protected override void ApplyPostSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostOneToOneSave.
		/// </summary>
		protected override void ApplyPostOneSaveKeys()
		{
		}
		
	}



	[Serializable]
	abstract public class esPessoaDocumentoQuery : esDynamicQuery
	{
		override protected IMetadata Meta
		{
			get
			{
				return PessoaDocumentoMetadata.Meta();
			}
		}	
		

		public esQueryItem IdDocumento
		{
			get
			{
				return new esQueryItem(this, PessoaDocumentoMetadata.ColumnNames.IdDocumento, esSystemType.Int32);
			}
		} 
		
		public esQueryItem IdPessoa
		{
			get
			{
				return new esQueryItem(this, PessoaDocumentoMetadata.ColumnNames.IdPessoa, esSystemType.Int32);
			}
		} 
		
		public esQueryItem TipoDocumento
		{
			get
			{
				return new esQueryItem(this, PessoaDocumentoMetadata.ColumnNames.TipoDocumento, esSystemType.Byte);
			}
		} 
		
		public esQueryItem NumeroDocumento
		{
			get
			{
				return new esQueryItem(this, PessoaDocumentoMetadata.ColumnNames.NumeroDocumento, esSystemType.String);
			}
		} 
		
		public esQueryItem DataExpedicao
		{
			get
			{
				return new esQueryItem(this, PessoaDocumentoMetadata.ColumnNames.DataExpedicao, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem OrgaoEmissor
		{
			get
			{
				return new esQueryItem(this, PessoaDocumentoMetadata.ColumnNames.OrgaoEmissor, esSystemType.String);
			}
		} 
		
		public esQueryItem UFEmissor
		{
			get
			{
				return new esQueryItem(this, PessoaDocumentoMetadata.ColumnNames.UFEmissor, esSystemType.String);
			}
		}

        public esQueryItem DataVencimento
        {
            get
            {
                return new esQueryItem(this, PessoaDocumentoMetadata.ColumnNames.DataVencimento, esSystemType.DateTime);
            }
        } 
	}



	[Serializable]
	[XmlType("PessoaDocumentoCollection")]
	public partial class PessoaDocumentoCollection : esPessoaDocumentoCollection, IEnumerable<PessoaDocumento>
	{
		public PessoaDocumentoCollection()
		{

		}
		
		public static implicit operator List<PessoaDocumento>(PessoaDocumentoCollection coll)
		{
			List<PessoaDocumento> list = new List<PessoaDocumento>();
			
			foreach (PessoaDocumento emp in coll)
			{
				list.Add(emp);
			}
			
			return list;
		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return  PessoaDocumentoMetadata.Meta();
			}
		}
		
		
		
		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new PessoaDocumentoQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		
		override protected esEntity CreateEntityForCollection(DataRow row)
		{
			return new PessoaDocumento(row);
		}

		override protected esEntity CreateEntity()
		{
			return new PessoaDocumento();
		}
		
		
		#endregion


		[BrowsableAttribute( false )]
		public PessoaDocumentoQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new PessoaDocumentoQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}

		public bool Load(PessoaDocumentoQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		public PessoaDocumento AddNew()
		{
			PessoaDocumento entity = base.AddNewEntity() as PessoaDocumento;
			
			return entity;
		}

		public PessoaDocumento FindByPrimaryKey(System.Int32 idDocumento)
		{
			return base.FindByPrimaryKey(idDocumento) as PessoaDocumento;
		}


		#region IEnumerable<PessoaDocumento> Members

		IEnumerator<PessoaDocumento> IEnumerable<PessoaDocumento>.GetEnumerator()
		{
			System.Collections.IEnumerable enumer = this as System.Collections.IEnumerable;
			System.Collections.IEnumerator iterator = enumer.GetEnumerator();

			while(iterator.MoveNext())
			{
				yield return iterator.Current as PessoaDocumento;
			}
		}

		#endregion
		
		private PessoaDocumentoQuery query;
	}


	/// <summary>
	/// Encapsulates the 'PessoaDocumento' table
	/// </summary>

	[Serializable]
	public partial class PessoaDocumento : esPessoaDocumento
	{
		public PessoaDocumento()
		{

		}
	
		public PessoaDocumento(DataRow row)
			: base(row)
		{

		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return PessoaDocumentoMetadata.Meta();
			}
		}
		
		
		
		override protected esPessoaDocumentoQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new PessoaDocumentoQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		#endregion
		



		[BrowsableAttribute( false )]
		public PessoaDocumentoQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new PessoaDocumentoQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}
		

		public bool Load(PessoaDocumentoQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		private PessoaDocumentoQuery query;
	}



	[Serializable]
	public partial class PessoaDocumentoQuery : esPessoaDocumentoQuery
	{
		public PessoaDocumentoQuery()
		{

		}		
		
		public PessoaDocumentoQuery(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
			
	}



	[Serializable]
	public partial class PessoaDocumentoMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected PessoaDocumentoMetadata()
		{
			_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(PessoaDocumentoMetadata.ColumnNames.IdDocumento, 0, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = PessoaDocumentoMetadata.PropertyNames.IdDocumento;
			c.IsInPrimaryKey = true;
			c.IsAutoIncrement = true;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PessoaDocumentoMetadata.ColumnNames.IdPessoa, 1, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = PessoaDocumentoMetadata.PropertyNames.IdPessoa;	
			c.NumericPrecision = 10;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PessoaDocumentoMetadata.ColumnNames.TipoDocumento, 2, typeof(System.Byte), esSystemType.Byte);
			c.PropertyName = PessoaDocumentoMetadata.PropertyNames.TipoDocumento;	
			c.NumericPrecision = 3;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PessoaDocumentoMetadata.ColumnNames.NumeroDocumento, 3, typeof(System.String), esSystemType.String);
			c.PropertyName = PessoaDocumentoMetadata.PropertyNames.NumeroDocumento;
			c.CharacterMaxLength = 13;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PessoaDocumentoMetadata.ColumnNames.DataExpedicao, 4, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = PessoaDocumentoMetadata.PropertyNames.DataExpedicao;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PessoaDocumentoMetadata.ColumnNames.OrgaoEmissor, 5, typeof(System.String), esSystemType.String);
			c.PropertyName = PessoaDocumentoMetadata.PropertyNames.OrgaoEmissor;
			c.CharacterMaxLength = 7;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PessoaDocumentoMetadata.ColumnNames.UFEmissor, 6, typeof(System.String), esSystemType.String);
			c.PropertyName = PessoaDocumentoMetadata.PropertyNames.UFEmissor;
			c.CharacterMaxLength = 2;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c);

            c = new esColumnMetadata(PessoaDocumentoMetadata.ColumnNames.DataVencimento, 7, typeof(System.DateTime), esSystemType.DateTime);
            c.PropertyName = PessoaDocumentoMetadata.PropertyNames.DataVencimento;
            c.NumericPrecision = 0;
            c.IsNullable = true;
            _columns.Add(c); 
				
		}
		#endregion
	
		static public PessoaDocumentoMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base._dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base._columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string IdDocumento = "IdDocumento";
			 public const string IdPessoa = "IdPessoa";
			 public const string TipoDocumento = "TipoDocumento";
			 public const string NumeroDocumento = "NumeroDocumento";
			 public const string DataExpedicao = "DataExpedicao";
			 public const string OrgaoEmissor = "OrgaoEmissor";
			 public const string UFEmissor = "UFEmissor";
             public const string DataVencimento = "DataVencimento";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string IdDocumento = "IdDocumento";
			 public const string IdPessoa = "IdPessoa";
			 public const string TipoDocumento = "TipoDocumento";
			 public const string NumeroDocumento = "NumeroDocumento";
			 public const string DataExpedicao = "DataExpedicao";
			 public const string OrgaoEmissor = "OrgaoEmissor";
			 public const string UFEmissor = "UFEmissor";
             public const string DataVencimento = "DataVencimento";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(PessoaDocumentoMetadata))
			{
				if(PessoaDocumentoMetadata.mapDelegates == null)
				{
					PessoaDocumentoMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (PessoaDocumentoMetadata.meta == null)
				{
					PessoaDocumentoMetadata.meta = new PessoaDocumentoMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();
				

				meta.AddTypeMap("IdDocumento", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("IdPessoa", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("TipoDocumento", new esTypeMap("tinyint", "System.Byte"));
				meta.AddTypeMap("NumeroDocumento", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("DataExpedicao", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("OrgaoEmissor", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("UFEmissor", new esTypeMap("char", "System.String"));
                meta.AddTypeMap("DataVencimento", new esTypeMap("datetime", "System.DateTime"));
				
				
				meta.Source = "PessoaDocumento";
				meta.Destination = "PessoaDocumento";
				
				meta.spInsert = "proc_PessoaDocumentoInsert";				
				meta.spUpdate = "proc_PessoaDocumentoUpdate";		
				meta.spDelete = "proc_PessoaDocumentoDelete";
				meta.spLoadAll = "proc_PessoaDocumentoLoadAll";
				meta.spLoadByPrimaryKey = "proc_PessoaDocumentoLoadByPrimaryKey";
				
				this._providerMetadataMaps["esDefault"] = meta;
			}
			
			return this._providerMetadataMaps["esDefault"];
		}

		#endregion

		static private PessoaDocumentoMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
