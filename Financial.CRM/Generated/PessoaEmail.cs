/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 23/05/2014 15:02:35
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;


using EntitySpaces.Interfaces;
using EntitySpaces.Core;





































		



			
					


		

		
		
		

		

























		






	

















				
		
		




		








		
				
				












	

















					
								
											
	










		





		




				
				








				




		

		
		
		
		
		





namespace Financial.CRM
{

	[Serializable]
	abstract public class esPessoaEmailCollection : esEntityCollection
	{
		public esPessoaEmailCollection()
		{

		}

		protected override string GetCollectionName()
		{
			return "PessoaEmailCollection";
		}

		#region Query Logic
		protected void InitQuery(esPessoaEmailQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntityCollection)this).Connection;
		}

		protected bool OnQueryLoaded(DataTable table)
		{
			this.PopulateCollection(table);
			return (this.RowCount > 0) ? true : false;
		}
		
		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery(query as esPessoaEmailQuery);
		}
		#endregion
		
		virtual public PessoaEmail DetachEntity(PessoaEmail entity)
		{
			return base.DetachEntity(entity) as PessoaEmail;
		}
		
		virtual public PessoaEmail AttachEntity(PessoaEmail entity)
		{
			return base.AttachEntity(entity) as PessoaEmail;
		}
		
		virtual public void Combine(PessoaEmailCollection collection)
		{
			base.Combine(collection);
		}
		
		new public PessoaEmail this[int index]
		{
			get
			{
				return base[index] as PessoaEmail;
			}
		}

		public override Type GetEntityType()
		{
			return typeof(PessoaEmail);
		}
	}



	[Serializable]
	abstract public class esPessoaEmail : esEntity
	{
		/// <summary>
		/// Used internally by the entity's DynamicQuery mechanism.
		/// </summary>
		virtual protected esPessoaEmailQuery GetDynamicQuery()
		{
			return null;
		}

		public esPessoaEmail()
		{

		}

		public esPessoaEmail(DataRow row)
			: base(row)
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.Int32 idEmail)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idEmail);
			else
				return LoadByPrimaryKeyStoredProcedure(idEmail);
		}

		/// <summary>
		/// Loads an entity by primary key
		/// </summary>
		/// <remarks>
		/// EntitySpaces requires primary keys be defined on all tables.
		/// If a table does not have a primary key set,
		/// this method will not compile.
		/// Does not support sqlAcessType. It only works with DynamicQuery
		/// </remarks>
		/// <param name="fieldsToReturn">Fields desired</param>
		public virtual bool LoadByPrimaryKey(List<esQueryItem> fieldsToReturn, System.Int32 idEmail)
		{
			esQueryItem[] fields = fieldsToReturn.ToArray();			
			esPessoaEmailQuery query = this.GetDynamicQuery();
			query
			    .Select(fields)
			    .Where(query.IdEmail == idEmail);
		
			return query.Load();
		}
		
		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.Int32 idEmail)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idEmail);
			else
				return LoadByPrimaryKeyStoredProcedure(idEmail);
		}

		private bool LoadByPrimaryKeyDynamic(System.Int32 idEmail)
		{
			esPessoaEmailQuery query = this.GetDynamicQuery();
			query.Where(query.IdEmail == idEmail);
			return query.Load();
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.Int32 idEmail)
		{
			esParameters parms = new esParameters();
			parms.Add("IdEmail",idEmail);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		
		
		#region Properties
		
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}

		public override void SetProperty(string name, object value)
		{
			if(this.Row == null) this.AddNew();
			
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value.GetType().ToString() == "System.String")
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "IdEmail": this.str.IdEmail = (string)value; break;							
						case "IdPessoa": this.str.IdPessoa = (string)value; break;							
						case "Email": this.str.Email = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "IdEmail":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdEmail = (System.Int32?)value;
							break;
						
						case "IdPessoa":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdPessoa = (System.Int32?)value;
							break;
					

						default:
							break;
					}
				}
			}
			else if(this.Row.Table.Columns.Contains(name))
			{
				this.Row[name] = value;
			}
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}
		
		
		/// <summary>
		/// Maps to PessoaEmail.IdEmail
		/// </summary>
		virtual public System.Int32? IdEmail
		{
			get
			{
				return base.GetSystemInt32(PessoaEmailMetadata.ColumnNames.IdEmail);
			}
			
			set
			{
				base.SetSystemInt32(PessoaEmailMetadata.ColumnNames.IdEmail, value);
			}
		}
		
		/// <summary>
		/// Maps to PessoaEmail.IdPessoa
		/// </summary>
		virtual public System.Int32? IdPessoa
		{
			get
			{
				return base.GetSystemInt32(PessoaEmailMetadata.ColumnNames.IdPessoa);
			}
			
			set
			{
				if(base.SetSystemInt32(PessoaEmailMetadata.ColumnNames.IdPessoa, value))
				{
					this._UpToPessoaByIdPessoa = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to PessoaEmail.Email
		/// </summary>
		virtual public System.String Email
		{
			get
			{
				return base.GetSystemString(PessoaEmailMetadata.ColumnNames.Email);
			}
			
			set
			{
				base.SetSystemString(PessoaEmailMetadata.ColumnNames.Email, value);
			}
		}
		
		[CLSCompliant(false)]
		internal protected Pessoa _UpToPessoaByIdPessoa;
		#endregion	

		#region String Properties


		[BrowsableAttribute( false )]
		public esStrings str
		{
			get
			{
				if (esstrings == null)
				{
					esstrings = new esStrings(this);
				}
				return esstrings;
			}
		}


		[Serializable]
		sealed public class esStrings
		{
			public esStrings(esPessoaEmail entity)
			{
				this.entity = entity;
			}
			
	
			public System.String IdEmail
			{
				get
				{
					System.Int32? data = entity.IdEmail;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdEmail = null;
					else entity.IdEmail = Convert.ToInt32(value);
				}
			}
				
			public System.String IdPessoa
			{
				get
				{
					System.Int32? data = entity.IdPessoa;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdPessoa = null;
					else entity.IdPessoa = Convert.ToInt32(value);
				}
			}
				
			public System.String Email
			{
				get
				{
					System.String data = entity.Email;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Email = null;
					else entity.Email = Convert.ToString(value);
				}
			}
			

			private esPessoaEmail entity;
		}
		#endregion

		#region Query Logic
		protected void InitQuery(esPessoaEmailQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntity)this).Connection;
		}

		[System.Diagnostics.DebuggerNonUserCode]
		protected bool OnQueryLoaded(DataTable table)
		{
			bool dataFound = this.PopulateEntity(table);

			if (this.RowCount > 1)
			{
				throw new Exception("esPessoaEmail can only hold one record of data");
			}

			return dataFound;
		}
		#endregion
		
		[NonSerialized]
		private esStrings esstrings;
	}


	
	public partial class PessoaEmail : esPessoaEmail
	{

				
		#region UpToPessoaByIdPessoa - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - FK_PessoaEmail_Pessoa
		/// </summary>

		[XmlIgnore]
		public Pessoa UpToPessoaByIdPessoa
		{
			get
			{
				if(this._UpToPessoaByIdPessoa == null
					&& IdPessoa != null					)
				{
					this._UpToPessoaByIdPessoa = new Pessoa();
					this._UpToPessoaByIdPessoa.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToPessoaByIdPessoa", this._UpToPessoaByIdPessoa);
					this._UpToPessoaByIdPessoa.Query.Where(this._UpToPessoaByIdPessoa.Query.IdPessoa == this.IdPessoa);
					this._UpToPessoaByIdPessoa.Query.Load();
				}

				return this._UpToPessoaByIdPessoa;
			}
			
			set
			{
				this.RemovePreSave("UpToPessoaByIdPessoa");
				

				if(value == null)
				{
					this.IdPessoa = null;
					this._UpToPessoaByIdPessoa = null;
				}
				else
				{
					this.IdPessoa = value.IdPessoa;
					this._UpToPessoaByIdPessoa = value;
					this.SetPreSave("UpToPessoaByIdPessoa", this._UpToPessoaByIdPessoa);
				}
				
			}
		}
		#endregion
		

		
		/// <summary>
		/// Used internally by the entity's hierarchical properties.
		/// </summary>
		protected override List<esPropertyDescriptor> GetHierarchicalProperties()
		{
			List<esPropertyDescriptor> props = new List<esPropertyDescriptor>();
			
		
			return props;
		}	
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PreSave.
		/// </summary>
		protected override void ApplyPreSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostSave.
		/// </summary>
		protected override void ApplyPostSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostOneToOneSave.
		/// </summary>
		protected override void ApplyPostOneSaveKeys()
		{
		}
		
	}



	[Serializable]
	abstract public class esPessoaEmailQuery : esDynamicQuery
	{
		override protected IMetadata Meta
		{
			get
			{
				return PessoaEmailMetadata.Meta();
			}
		}	
		

		public esQueryItem IdEmail
		{
			get
			{
				return new esQueryItem(this, PessoaEmailMetadata.ColumnNames.IdEmail, esSystemType.Int32);
			}
		} 
		
		public esQueryItem IdPessoa
		{
			get
			{
				return new esQueryItem(this, PessoaEmailMetadata.ColumnNames.IdPessoa, esSystemType.Int32);
			}
		} 
		
		public esQueryItem Email
		{
			get
			{
				return new esQueryItem(this, PessoaEmailMetadata.ColumnNames.Email, esSystemType.String);
			}
		} 
		
	}



	[Serializable]
	[XmlType("PessoaEmailCollection")]
	public partial class PessoaEmailCollection : esPessoaEmailCollection, IEnumerable<PessoaEmail>
	{
		public PessoaEmailCollection()
		{

		}
		
		public static implicit operator List<PessoaEmail>(PessoaEmailCollection coll)
		{
			List<PessoaEmail> list = new List<PessoaEmail>();
			
			foreach (PessoaEmail emp in coll)
			{
				list.Add(emp);
			}
			
			return list;
		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return  PessoaEmailMetadata.Meta();
			}
		}
		
		
		
		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new PessoaEmailQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		
		override protected esEntity CreateEntityForCollection(DataRow row)
		{
			return new PessoaEmail(row);
		}

		override protected esEntity CreateEntity()
		{
			return new PessoaEmail();
		}
		
		
		#endregion


		[BrowsableAttribute( false )]
		public PessoaEmailQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new PessoaEmailQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}

		public bool Load(PessoaEmailQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		public PessoaEmail AddNew()
		{
			PessoaEmail entity = base.AddNewEntity() as PessoaEmail;
			
			return entity;
		}

		public PessoaEmail FindByPrimaryKey(System.Int32 idEmail)
		{
			return base.FindByPrimaryKey(idEmail) as PessoaEmail;
		}


		#region IEnumerable<PessoaEmail> Members

		IEnumerator<PessoaEmail> IEnumerable<PessoaEmail>.GetEnumerator()
		{
			System.Collections.IEnumerable enumer = this as System.Collections.IEnumerable;
			System.Collections.IEnumerator iterator = enumer.GetEnumerator();

			while(iterator.MoveNext())
			{
				yield return iterator.Current as PessoaEmail;
			}
		}

		#endregion
		
		private PessoaEmailQuery query;
	}


	/// <summary>
	/// Encapsulates the 'PessoaEmail' table
	/// </summary>

	[Serializable]
	public partial class PessoaEmail : esPessoaEmail
	{
		public PessoaEmail()
		{

		}
	
		public PessoaEmail(DataRow row)
			: base(row)
		{

		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return PessoaEmailMetadata.Meta();
			}
		}
		
		
		
		override protected esPessoaEmailQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new PessoaEmailQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		#endregion
		



		[BrowsableAttribute( false )]
		public PessoaEmailQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new PessoaEmailQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}
		

		public bool Load(PessoaEmailQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		private PessoaEmailQuery query;
	}



	[Serializable]
	public partial class PessoaEmailQuery : esPessoaEmailQuery
	{
		public PessoaEmailQuery()
		{

		}		
		
		public PessoaEmailQuery(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
			
	}



	[Serializable]
	public partial class PessoaEmailMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected PessoaEmailMetadata()
		{
			_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(PessoaEmailMetadata.ColumnNames.IdEmail, 0, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = PessoaEmailMetadata.PropertyNames.IdEmail;
			c.IsInPrimaryKey = true;
			c.IsAutoIncrement = true;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PessoaEmailMetadata.ColumnNames.IdPessoa, 1, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = PessoaEmailMetadata.PropertyNames.IdPessoa;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PessoaEmailMetadata.ColumnNames.Email, 2, typeof(System.String), esSystemType.String);
			c.PropertyName = PessoaEmailMetadata.PropertyNames.Email;
			c.CharacterMaxLength = 200;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
		}
		#endregion
	
		static public PessoaEmailMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base._dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base._columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string IdEmail = "IdEmail";
			 public const string IdPessoa = "IdPessoa";
			 public const string Email = "Email";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string IdEmail = "IdEmail";
			 public const string IdPessoa = "IdPessoa";
			 public const string Email = "Email";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(PessoaEmailMetadata))
			{
				if(PessoaEmailMetadata.mapDelegates == null)
				{
					PessoaEmailMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (PessoaEmailMetadata.meta == null)
				{
					PessoaEmailMetadata.meta = new PessoaEmailMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();
				

				meta.AddTypeMap("IdEmail", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("IdPessoa", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("Email", new esTypeMap("varchar", "System.String"));			
				
				
				
				meta.Source = "PessoaEmail";
				meta.Destination = "PessoaEmail";
				
				meta.spInsert = "proc_PessoaEmailInsert";				
				meta.spUpdate = "proc_PessoaEmailUpdate";		
				meta.spDelete = "proc_PessoaEmailDelete";
				meta.spLoadAll = "proc_PessoaEmailLoadAll";
				meta.spLoadByPrimaryKey = "proc_PessoaEmailLoadByPrimaryKey";
				
				this._providerMetadataMaps["esDefault"] = meta;
			}
			
			return this._providerMetadataMaps["esDefault"];
		}

		#endregion

		static private PessoaEmailMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
