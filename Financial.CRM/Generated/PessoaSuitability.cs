/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 18/09/2015 12:58:21
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;


using EntitySpaces.Interfaces;
using EntitySpaces.Core;

using Financial.Investidor;

namespace Financial.CRM
{

	[Serializable]
	abstract public class esPessoaSuitabilityCollection : esEntityCollection
	{
		public esPessoaSuitabilityCollection()
		{

		}

		protected override string GetCollectionName()
		{
			return "PessoaSuitabilityCollection";
		}

		#region Query Logic
		protected void InitQuery(esPessoaSuitabilityQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntityCollection)this).Connection;
		}

		protected bool OnQueryLoaded(DataTable table)
		{
			this.PopulateCollection(table);
			return (this.RowCount > 0) ? true : false;
		}
		
		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery(query as esPessoaSuitabilityQuery);
		}
		#endregion
		
		virtual public PessoaSuitability DetachEntity(PessoaSuitability entity)
		{
			return base.DetachEntity(entity) as PessoaSuitability;
		}
		
		virtual public PessoaSuitability AttachEntity(PessoaSuitability entity)
		{
			return base.AttachEntity(entity) as PessoaSuitability;
		}
		
		virtual public void Combine(PessoaSuitabilityCollection collection)
		{
			base.Combine(collection);
		}
		
		new public PessoaSuitability this[int index]
		{
			get
			{
				return base[index] as PessoaSuitability;
			}
		}

		public override Type GetEntityType()
		{
			return typeof(PessoaSuitability);
		}
	}



	[Serializable]
	abstract public class esPessoaSuitability : esEntity
	{
		/// <summary>
		/// Used internally by the entity's DynamicQuery mechanism.
		/// </summary>
		virtual protected esPessoaSuitabilityQuery GetDynamicQuery()
		{
			return null;
		}

		public esPessoaSuitability()
		{

		}

		public esPessoaSuitability(DataRow row)
			: base(row)
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.Int32 idPessoa)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idPessoa);
			else
				return LoadByPrimaryKeyStoredProcedure(idPessoa);
		}

		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.Int32 idPessoa)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idPessoa);
			else
				return LoadByPrimaryKeyStoredProcedure(idPessoa);
		}

		private bool LoadByPrimaryKeyDynamic(System.Int32 idPessoa)
		{
			esPessoaSuitabilityQuery query = this.GetDynamicQuery();
			query.Where(query.IdPessoa == idPessoa);
			return query.Load();
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.Int32 idPessoa)
		{
			esParameters parms = new esParameters();
			parms.Add("IdPessoa",idPessoa);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		
		
		#region Properties
		
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}

		public override void SetProperty(string name, object value)
		{
			if(this.Row == null) this.AddNew();
			
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value.GetType().ToString() == "System.String")
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "IdPessoa": this.str.IdPessoa = (string)value; break;							
						case "IdValidacao": this.str.IdValidacao = (string)value; break;							
						case "Perfil": this.str.Perfil = (string)value; break;							
						case "Recusa": this.str.Recusa = (string)value; break;							
						case "Dispensado": this.str.Dispensado = (string)value; break;							
						case "Dispensa": this.str.Dispensa = (string)value; break;							
						case "UltimaAlteracao": this.str.UltimaAlteracao = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "IdPessoa":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdPessoa = (System.Int32?)value;
							break;
						
						case "IdValidacao":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdValidacao = (System.Int32?)value;
							break;
						
						case "Perfil":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.Perfil = (System.Int32?)value;
							break;
						
						case "Dispensa":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.Dispensa = (System.Int32?)value;
							break;
						
						case "UltimaAlteracao":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.UltimaAlteracao = (System.DateTime?)value;
							break;
					

						default:
							break;
					}
				}
			}
			else if(this.Row.Table.Columns.Contains(name))
			{
				this.Row[name] = value;
			}
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}
		
		
		/// <summary>
		/// Maps to PessoaSuitability.IdPessoa
		/// </summary>
		virtual public System.Int32? IdPessoa
		{
			get
			{
				return base.GetSystemInt32(PessoaSuitabilityMetadata.ColumnNames.IdPessoa);
			}
			
			set
			{
				base.SetSystemInt32(PessoaSuitabilityMetadata.ColumnNames.IdPessoa, value);
			}
		}
		
		/// <summary>
		/// Maps to PessoaSuitability.IdValidacao
		/// </summary>
		virtual public System.Int32? IdValidacao
		{
			get
			{
				return base.GetSystemInt32(PessoaSuitabilityMetadata.ColumnNames.IdValidacao);
			}
			
			set
			{
				if(base.SetSystemInt32(PessoaSuitabilityMetadata.ColumnNames.IdValidacao, value))
				{
					this._UpToSuitabilityValidacaoByIdValidacao = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to PessoaSuitability.Perfil
		/// </summary>
		virtual public System.Int32? Perfil
		{
			get
			{
				return base.GetSystemInt32(PessoaSuitabilityMetadata.ColumnNames.Perfil);
			}
			
			set
			{
				if(base.SetSystemInt32(PessoaSuitabilityMetadata.ColumnNames.Perfil, value))
				{
					this._UpToSuitabilityPerfilInvestidorByPerfil = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to PessoaSuitability.Recusa
		/// </summary>
		virtual public System.String Recusa
		{
			get
			{
				return base.GetSystemString(PessoaSuitabilityMetadata.ColumnNames.Recusa);
			}
			
			set
			{
				base.SetSystemString(PessoaSuitabilityMetadata.ColumnNames.Recusa, value);
			}
		}
		
		/// <summary>
		/// Maps to PessoaSuitability.Dispensado
		/// </summary>
		virtual public System.String Dispensado
		{
			get
			{
				return base.GetSystemString(PessoaSuitabilityMetadata.ColumnNames.Dispensado);
			}
			
			set
			{
				base.SetSystemString(PessoaSuitabilityMetadata.ColumnNames.Dispensado, value);
			}
		}
		
		/// <summary>
		/// Maps to PessoaSuitability.Dispensa
		/// </summary>
		virtual public System.Int32? Dispensa
		{
			get
			{
				return base.GetSystemInt32(PessoaSuitabilityMetadata.ColumnNames.Dispensa);
			}
			
			set
			{
				if(base.SetSystemInt32(PessoaSuitabilityMetadata.ColumnNames.Dispensa, value))
				{
					this._UpToSuitabilityTipoDispensaByDispensa = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to PessoaSuitability.UltimaAlteracao
		/// </summary>
		virtual public System.DateTime? UltimaAlteracao
		{
			get
			{
				return base.GetSystemDateTime(PessoaSuitabilityMetadata.ColumnNames.UltimaAlteracao);
			}
			
			set
			{
				base.SetSystemDateTime(PessoaSuitabilityMetadata.ColumnNames.UltimaAlteracao, value);
			}
		}
		
		[CLSCompliant(false)]
		internal protected SuitabilityPerfilInvestidor _UpToSuitabilityPerfilInvestidorByPerfil;
		[CLSCompliant(false)]
		internal protected SuitabilityTipoDispensa _UpToSuitabilityTipoDispensaByDispensa;
		[CLSCompliant(false)]
		internal protected SuitabilityValidacao _UpToSuitabilityValidacaoByIdValidacao;
		#endregion	

		#region String Properties


		[BrowsableAttribute( false )]
		public esStrings str
		{
			get
			{
				if (esstrings == null)
				{
					esstrings = new esStrings(this);
				}
				return esstrings;
			}
		}


		[Serializable]
		sealed public class esStrings
		{
			public esStrings(esPessoaSuitability entity)
			{
				this.entity = entity;
			}
			
	
			public System.String IdPessoa
			{
				get
				{
					System.Int32? data = entity.IdPessoa;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdPessoa = null;
					else entity.IdPessoa = Convert.ToInt32(value);
				}
			}
				
			public System.String IdValidacao
			{
				get
				{
					System.Int32? data = entity.IdValidacao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdValidacao = null;
					else entity.IdValidacao = Convert.ToInt32(value);
				}
			}
				
			public System.String Perfil
			{
				get
				{
					System.Int32? data = entity.Perfil;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Perfil = null;
					else entity.Perfil = Convert.ToInt32(value);
				}
			}
				
			public System.String Recusa
			{
				get
				{
					System.String data = entity.Recusa;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Recusa = null;
					else entity.Recusa = Convert.ToString(value);
				}
			}
				
			public System.String Dispensado
			{
				get
				{
					System.String data = entity.Dispensado;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Dispensado = null;
					else entity.Dispensado = Convert.ToString(value);
				}
			}
				
			public System.String Dispensa
			{
				get
				{
					System.Int32? data = entity.Dispensa;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Dispensa = null;
					else entity.Dispensa = Convert.ToInt32(value);
				}
			}
				
			public System.String UltimaAlteracao
			{
				get
				{
					System.DateTime? data = entity.UltimaAlteracao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.UltimaAlteracao = null;
					else entity.UltimaAlteracao = Convert.ToDateTime(value);
				}
			}
			

			private esPessoaSuitability entity;
		}
		#endregion

		#region Query Logic
		protected void InitQuery(esPessoaSuitabilityQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntity)this).Connection;
		}

		[System.Diagnostics.DebuggerNonUserCode]
		protected bool OnQueryLoaded(DataTable table)
		{
			bool dataFound = this.PopulateEntity(table);

			if (this.RowCount > 1)
			{
				throw new Exception("esPessoaSuitability can only hold one record of data");
			}

			return dataFound;
		}
		#endregion
		
		[NonSerialized]
		private esStrings esstrings;
	}


	
	public partial class PessoaSuitability : esPessoaSuitability
	{

		#region UpToPessoa - One To One
		/// <summary>
		/// One to One
		/// Foreign Key Name - PessoaSuitability_Pessoa_FK
		/// </summary>

		[XmlIgnore]
		public Pessoa UpToPessoa
		{
			get
			{
				if(this._UpToPessoa == null
					&& IdPessoa != null					)
				{
					this._UpToPessoa = new Pessoa();
					this._UpToPessoa.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToPessoa", this._UpToPessoa);
					this._UpToPessoa.Query.Where(this._UpToPessoa.Query.IdPessoa == this.IdPessoa);
					this._UpToPessoa.Query.Load();
				}

				return this._UpToPessoa;
			}
			
			set 
			{ 
				this.RemovePreSave("UpToPessoa");

				if(value == null)
				{
					this._UpToPessoa = null;
				}
				else
				{
					this._UpToPessoa = value;
					this.SetPreSave("UpToPessoa", this._UpToPessoa);
				}
				
				
			} 
		}

		private Pessoa _UpToPessoa;
		#endregion

				
		#region UpToSuitabilityPerfilInvestidorByPerfil - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - PessoaSuitability_PerfilInvestidor_FK
		/// </summary>

		[XmlIgnore]
		public SuitabilityPerfilInvestidor UpToSuitabilityPerfilInvestidorByPerfil
		{
			get
			{
				if(this._UpToSuitabilityPerfilInvestidorByPerfil == null
					&& Perfil != null					)
				{
					this._UpToSuitabilityPerfilInvestidorByPerfil = new SuitabilityPerfilInvestidor();
					this._UpToSuitabilityPerfilInvestidorByPerfil.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToSuitabilityPerfilInvestidorByPerfil", this._UpToSuitabilityPerfilInvestidorByPerfil);
					this._UpToSuitabilityPerfilInvestidorByPerfil.Query.Where(this._UpToSuitabilityPerfilInvestidorByPerfil.Query.IdPerfilInvestidor == this.Perfil);
					this._UpToSuitabilityPerfilInvestidorByPerfil.Query.Load();
				}

				return this._UpToSuitabilityPerfilInvestidorByPerfil;
			}
			
			set
			{
				this.RemovePreSave("UpToSuitabilityPerfilInvestidorByPerfil");
				

				if(value == null)
				{
					this.Perfil = null;
					this._UpToSuitabilityPerfilInvestidorByPerfil = null;
				}
				else
				{
					this.Perfil = value.IdPerfilInvestidor;
					this._UpToSuitabilityPerfilInvestidorByPerfil = value;
					this.SetPreSave("UpToSuitabilityPerfilInvestidorByPerfil", this._UpToSuitabilityPerfilInvestidorByPerfil);
				}
				
			}
		}
		#endregion
		

				
		#region UpToSuitabilityTipoDispensaByDispensa - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - PessoaSuitability_TipoDispensa_FK
		/// </summary>

		[XmlIgnore]
		public SuitabilityTipoDispensa UpToSuitabilityTipoDispensaByDispensa
		{
			get
			{
				if(this._UpToSuitabilityTipoDispensaByDispensa == null
					&& Dispensa != null					)
				{
					this._UpToSuitabilityTipoDispensaByDispensa = new SuitabilityTipoDispensa();
					this._UpToSuitabilityTipoDispensaByDispensa.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToSuitabilityTipoDispensaByDispensa", this._UpToSuitabilityTipoDispensaByDispensa);
					this._UpToSuitabilityTipoDispensaByDispensa.Query.Where(this._UpToSuitabilityTipoDispensaByDispensa.Query.IdDispensa == this.Dispensa);
					this._UpToSuitabilityTipoDispensaByDispensa.Query.Load();
				}

				return this._UpToSuitabilityTipoDispensaByDispensa;
			}
			
			set
			{
				this.RemovePreSave("UpToSuitabilityTipoDispensaByDispensa");
				

				if(value == null)
				{
					this.Dispensa = null;
					this._UpToSuitabilityTipoDispensaByDispensa = null;
				}
				else
				{
					this.Dispensa = value.IdDispensa;
					this._UpToSuitabilityTipoDispensaByDispensa = value;
					this.SetPreSave("UpToSuitabilityTipoDispensaByDispensa", this._UpToSuitabilityTipoDispensaByDispensa);
				}
				
			}
		}
		#endregion
		

				
		#region UpToSuitabilityValidacaoByIdValidacao - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - PessoaSuitability_Validacao_FK
		/// </summary>

		[XmlIgnore]
		public SuitabilityValidacao UpToSuitabilityValidacaoByIdValidacao
		{
			get
			{
				if(this._UpToSuitabilityValidacaoByIdValidacao == null
					&& IdValidacao != null					)
				{
					this._UpToSuitabilityValidacaoByIdValidacao = new SuitabilityValidacao();
					this._UpToSuitabilityValidacaoByIdValidacao.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToSuitabilityValidacaoByIdValidacao", this._UpToSuitabilityValidacaoByIdValidacao);
					this._UpToSuitabilityValidacaoByIdValidacao.Query.Where(this._UpToSuitabilityValidacaoByIdValidacao.Query.IdValidacao == this.IdValidacao);
					this._UpToSuitabilityValidacaoByIdValidacao.Query.Load();
				}

				return this._UpToSuitabilityValidacaoByIdValidacao;
			}
			
			set
			{
				this.RemovePreSave("UpToSuitabilityValidacaoByIdValidacao");
				

				if(value == null)
				{
					this.IdValidacao = null;
					this._UpToSuitabilityValidacaoByIdValidacao = null;
				}
				else
				{
					this.IdValidacao = value.IdValidacao;
					this._UpToSuitabilityValidacaoByIdValidacao = value;
					this.SetPreSave("UpToSuitabilityValidacaoByIdValidacao", this._UpToSuitabilityValidacaoByIdValidacao);
				}
				
			}
		}
		#endregion
		

		
		/// <summary>
		/// Used internally by the entity's hierarchical properties.
		/// </summary>
		protected override List<esPropertyDescriptor> GetHierarchicalProperties()
		{
			List<esPropertyDescriptor> props = new List<esPropertyDescriptor>();
			
		
			return props;
		}	
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PreSave.
		/// </summary>
		protected override void ApplyPreSaveKeys()
		{
			if(!this.es.IsDeleted && this._UpToSuitabilityPerfilInvestidorByPerfil != null)
			{
				this.Perfil = this._UpToSuitabilityPerfilInvestidorByPerfil.IdPerfilInvestidor;
			}
			if(!this.es.IsDeleted && this._UpToSuitabilityTipoDispensaByDispensa != null)
			{
				this.Dispensa = this._UpToSuitabilityTipoDispensaByDispensa.IdDispensa;
			}
			if(!this.es.IsDeleted && this._UpToSuitabilityValidacaoByIdValidacao != null)
			{
				this.IdValidacao = this._UpToSuitabilityValidacaoByIdValidacao.IdValidacao;
			}
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostSave.
		/// </summary>
		protected override void ApplyPostSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostOneToOneSave.
		/// </summary>
		protected override void ApplyPostOneSaveKeys()
		{
		}
		
	}



	[Serializable]
	abstract public class esPessoaSuitabilityQuery : esDynamicQuery
	{
		override protected IMetadata Meta
		{
			get
			{
				return PessoaSuitabilityMetadata.Meta();
			}
		}	
		

		public esQueryItem IdPessoa
		{
			get
			{
				return new esQueryItem(this, PessoaSuitabilityMetadata.ColumnNames.IdPessoa, esSystemType.Int32);
			}
		} 
		
		public esQueryItem IdValidacao
		{
			get
			{
				return new esQueryItem(this, PessoaSuitabilityMetadata.ColumnNames.IdValidacao, esSystemType.Int32);
			}
		} 
		
		public esQueryItem Perfil
		{
			get
			{
				return new esQueryItem(this, PessoaSuitabilityMetadata.ColumnNames.Perfil, esSystemType.Int32);
			}
		} 
		
		public esQueryItem Recusa
		{
			get
			{
				return new esQueryItem(this, PessoaSuitabilityMetadata.ColumnNames.Recusa, esSystemType.String);
			}
		} 
		
		public esQueryItem Dispensado
		{
			get
			{
				return new esQueryItem(this, PessoaSuitabilityMetadata.ColumnNames.Dispensado, esSystemType.String);
			}
		} 
		
		public esQueryItem Dispensa
		{
			get
			{
				return new esQueryItem(this, PessoaSuitabilityMetadata.ColumnNames.Dispensa, esSystemType.Int32);
			}
		} 
		
		public esQueryItem UltimaAlteracao
		{
			get
			{
				return new esQueryItem(this, PessoaSuitabilityMetadata.ColumnNames.UltimaAlteracao, esSystemType.DateTime);
			}
		} 
		
	}



	[Serializable]
	[XmlType("PessoaSuitabilityCollection")]
	public partial class PessoaSuitabilityCollection : esPessoaSuitabilityCollection, IEnumerable<PessoaSuitability>
	{
		public PessoaSuitabilityCollection()
		{

		}
		
		public static implicit operator List<PessoaSuitability>(PessoaSuitabilityCollection coll)
		{
			List<PessoaSuitability> list = new List<PessoaSuitability>();
			
			foreach (PessoaSuitability emp in coll)
			{
				list.Add(emp);
			}
			
			return list;
		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return  PessoaSuitabilityMetadata.Meta();
			}
		}
		
		
		
		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new PessoaSuitabilityQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		
		override protected esEntity CreateEntityForCollection(DataRow row)
		{
			return new PessoaSuitability(row);
		}

		override protected esEntity CreateEntity()
		{
			return new PessoaSuitability();
		}
		
		
		#endregion


		[BrowsableAttribute( false )]
		public PessoaSuitabilityQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new PessoaSuitabilityQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}

		public bool Load(PessoaSuitabilityQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		public PessoaSuitability AddNew()
		{
			PessoaSuitability entity = base.AddNewEntity() as PessoaSuitability;
			
			return entity;
		}

		public PessoaSuitability FindByPrimaryKey(System.Int32 idPessoa)
		{
			return base.FindByPrimaryKey(idPessoa) as PessoaSuitability;
		}


		#region IEnumerable<PessoaSuitability> Members

		IEnumerator<PessoaSuitability> IEnumerable<PessoaSuitability>.GetEnumerator()
		{
			System.Collections.IEnumerable enumer = this as System.Collections.IEnumerable;
			System.Collections.IEnumerator iterator = enumer.GetEnumerator();

			while(iterator.MoveNext())
			{
				yield return iterator.Current as PessoaSuitability;
			}
		}

		#endregion
		
		private PessoaSuitabilityQuery query;
	}


	/// <summary>
	/// Encapsulates the 'PessoaSuitability' table
	/// </summary>

	[Serializable]
	public partial class PessoaSuitability : esPessoaSuitability
	{
		public PessoaSuitability()
		{

		}
	
		public PessoaSuitability(DataRow row)
			: base(row)
		{

		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return PessoaSuitabilityMetadata.Meta();
			}
		}
		
		
		
		override protected esPessoaSuitabilityQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new PessoaSuitabilityQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		#endregion
		



		[BrowsableAttribute( false )]
		public PessoaSuitabilityQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new PessoaSuitabilityQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}
		

		public bool Load(PessoaSuitabilityQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		private PessoaSuitabilityQuery query;
	}



	[Serializable]
	public partial class PessoaSuitabilityQuery : esPessoaSuitabilityQuery
	{
		public PessoaSuitabilityQuery()
		{

		}		
		
		public PessoaSuitabilityQuery(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
			
	}



	[Serializable]
	public partial class PessoaSuitabilityMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected PessoaSuitabilityMetadata()
		{
			_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(PessoaSuitabilityMetadata.ColumnNames.IdPessoa, 0, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = PessoaSuitabilityMetadata.PropertyNames.IdPessoa;
			c.IsInPrimaryKey = true;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PessoaSuitabilityMetadata.ColumnNames.IdValidacao, 1, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = PessoaSuitabilityMetadata.PropertyNames.IdValidacao;	
			c.NumericPrecision = 10;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PessoaSuitabilityMetadata.ColumnNames.Perfil, 2, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = PessoaSuitabilityMetadata.PropertyNames.Perfil;	
			c.NumericPrecision = 10;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PessoaSuitabilityMetadata.ColumnNames.Recusa, 3, typeof(System.String), esSystemType.String);
			c.PropertyName = PessoaSuitabilityMetadata.PropertyNames.Recusa;
			c.CharacterMaxLength = 1;
			c.NumericPrecision = 0;
			c.HasDefault = true;
			c.Default = @"('N')";
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PessoaSuitabilityMetadata.ColumnNames.Dispensado, 4, typeof(System.String), esSystemType.String);
			c.PropertyName = PessoaSuitabilityMetadata.PropertyNames.Dispensado;
			c.CharacterMaxLength = 1;
			c.NumericPrecision = 0;
			c.HasDefault = true;
			c.Default = @"('N')";
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PessoaSuitabilityMetadata.ColumnNames.Dispensa, 5, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = PessoaSuitabilityMetadata.PropertyNames.Dispensa;	
			c.NumericPrecision = 10;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PessoaSuitabilityMetadata.ColumnNames.UltimaAlteracao, 6, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = PessoaSuitabilityMetadata.PropertyNames.UltimaAlteracao;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
		}
		#endregion
	
		static public PessoaSuitabilityMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base._dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base._columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string IdPessoa = "IdPessoa";
			 public const string IdValidacao = "IdValidacao";
			 public const string Perfil = "Perfil";
			 public const string Recusa = "Recusa";
			 public const string Dispensado = "Dispensado";
			 public const string Dispensa = "Dispensa";
			 public const string UltimaAlteracao = "UltimaAlteracao";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string IdPessoa = "IdPessoa";
			 public const string IdValidacao = "IdValidacao";
			 public const string Perfil = "Perfil";
			 public const string Recusa = "Recusa";
			 public const string Dispensado = "Dispensado";
			 public const string Dispensa = "Dispensa";
			 public const string UltimaAlteracao = "UltimaAlteracao";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(PessoaSuitabilityMetadata))
			{
				if(PessoaSuitabilityMetadata.mapDelegates == null)
				{
					PessoaSuitabilityMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (PessoaSuitabilityMetadata.meta == null)
				{
					PessoaSuitabilityMetadata.meta = new PessoaSuitabilityMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();
				

				meta.AddTypeMap("IdPessoa", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("IdValidacao", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("Perfil", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("Recusa", new esTypeMap("char", "System.String"));
				meta.AddTypeMap("Dispensado", new esTypeMap("char", "System.String"));
				meta.AddTypeMap("Dispensa", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("UltimaAlteracao", new esTypeMap("datetime", "System.DateTime"));			
				
				
				
				meta.Source = "PessoaSuitability";
				meta.Destination = "PessoaSuitability";
				
				meta.spInsert = "proc_PessoaSuitabilityInsert";				
				meta.spUpdate = "proc_PessoaSuitabilityUpdate";		
				meta.spDelete = "proc_PessoaSuitabilityDelete";
				meta.spLoadAll = "proc_PessoaSuitabilityLoadAll";
				meta.spLoadByPrimaryKey = "proc_PessoaSuitabilityLoadByPrimaryKey";
				
				this._providerMetadataMaps["esDefault"] = meta;
			}
			
			return this._providerMetadataMaps["esDefault"];
		}

		#endregion

		static private PessoaSuitabilityMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
