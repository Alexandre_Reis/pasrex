/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 08/05/2015 12:56:47
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;


using EntitySpaces.Interfaces;
using EntitySpaces.Core;
using Financial.InvestidorCotista;



namespace Financial.CRM
{

	[Serializable]
	abstract public class esTemplateCotistaCollection : esEntityCollection
	{
		public esTemplateCotistaCollection()
		{

		}

		protected override string GetCollectionName()
		{
			return "TemplateCotistaCollection";
		}

		#region Query Logic
		protected void InitQuery(esTemplateCotistaQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntityCollection)this).Connection;
		}

		protected bool OnQueryLoaded(DataTable table)
		{
			this.PopulateCollection(table);
			return (this.RowCount > 0) ? true : false;
		}
		
		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery(query as esTemplateCotistaQuery);
		}
		#endregion
		
		virtual public TemplateCotista DetachEntity(TemplateCotista entity)
		{
			return base.DetachEntity(entity) as TemplateCotista;
		}
		
		virtual public TemplateCotista AttachEntity(TemplateCotista entity)
		{
			return base.AttachEntity(entity) as TemplateCotista;
		}
		
		virtual public void Combine(TemplateCotistaCollection collection)
		{
			base.Combine(collection);
		}
		
		new public TemplateCotista this[int index]
		{
			get
			{
				return base[index] as TemplateCotista;
			}
		}

		public override Type GetEntityType()
		{
			return typeof(TemplateCotista);
		}
	}



	[Serializable]
	abstract public class esTemplateCotista : esEntity
	{
		/// <summary>
		/// Used internally by the entity's DynamicQuery mechanism.
		/// </summary>
		virtual protected esTemplateCotistaQuery GetDynamicQuery()
		{
			return null;
		}

		public esTemplateCotista()
		{

		}

		public esTemplateCotista(DataRow row)
			: base(row)
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.Int32 idCotista)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idCotista);
			else
				return LoadByPrimaryKeyStoredProcedure(idCotista);
		}

		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.Int32 idCotista)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idCotista);
			else
				return LoadByPrimaryKeyStoredProcedure(idCotista);
		}

		private bool LoadByPrimaryKeyDynamic(System.Int32 idCotista)
		{
			esTemplateCotistaQuery query = this.GetDynamicQuery();
			query.Where(query.IdCotista == idCotista);
			return query.Load();
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.Int32 idCotista)
		{
			esParameters parms = new esParameters();
			parms.Add("IdCotista",idCotista);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		
		
		#region Properties
		
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}

		public override void SetProperty(string name, object value)
		{
			if(this.Row == null) this.AddNew();
			
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value.GetType().ToString() == "System.String")
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "IdCotista": this.str.IdCotista = (string)value; break;							
						case "Descricao": this.str.Descricao = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "IdCotista":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdCotista = (System.Int32?)value;
							break;
					

						default:
							break;
					}
				}
			}
			else if(this.Row.Table.Columns.Contains(name))
			{
				this.Row[name] = value;
			}
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}
		
		
		/// <summary>
		/// Maps to TemplateCotista.IdCotista
		/// </summary>
		virtual public System.Int32? IdCotista
		{
			get
			{
				return base.GetSystemInt32(TemplateCotistaMetadata.ColumnNames.IdCotista);
			}
			
			set
			{
				base.SetSystemInt32(TemplateCotistaMetadata.ColumnNames.IdCotista, value);
			}
		}
		
		/// <summary>
		/// Maps to TemplateCotista.Descricao
		/// </summary>
		virtual public System.String Descricao
		{
			get
			{
				return base.GetSystemString(TemplateCotistaMetadata.ColumnNames.Descricao);
			}
			
			set
			{
				base.SetSystemString(TemplateCotistaMetadata.ColumnNames.Descricao, value);
			}
		}
		
		#endregion	

		#region String Properties


		[BrowsableAttribute( false )]
		public esStrings str
		{
			get
			{
				if (esstrings == null)
				{
					esstrings = new esStrings(this);
				}
				return esstrings;
			}
		}


		[Serializable]
		sealed public class esStrings
		{
			public esStrings(esTemplateCotista entity)
			{
				this.entity = entity;
			}
			
	
			public System.String IdCotista
			{
				get
				{
					System.Int32? data = entity.IdCotista;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdCotista = null;
					else entity.IdCotista = Convert.ToInt32(value);
				}
			}
				
			public System.String Descricao
			{
				get
				{
					System.String data = entity.Descricao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Descricao = null;
					else entity.Descricao = Convert.ToString(value);
				}
			}
			

			private esTemplateCotista entity;
		}
		#endregion

		#region Query Logic
		protected void InitQuery(esTemplateCotistaQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntity)this).Connection;
		}

		[System.Diagnostics.DebuggerNonUserCode]
		protected bool OnQueryLoaded(DataTable table)
		{
			bool dataFound = this.PopulateEntity(table);

			if (this.RowCount > 1)
			{
				throw new Exception("esTemplateCotista can only hold one record of data");
			}

			return dataFound;
		}
		#endregion
		
		[NonSerialized]
		private esStrings esstrings;
	}


	
	public partial class TemplateCotista : esTemplateCotista
	{

		#region UpToCotista - One To One
		/// <summary>
		/// One to One
		/// Foreign Key Name - Cotista_TemplateCotista_FK1
		/// </summary>

		[XmlIgnore]
		public Cotista UpToCotista
		{
			get
			{
				if(this._UpToCotista == null
					&& IdCotista != null					)
				{
					this._UpToCotista = new Cotista();
					this._UpToCotista.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToCotista", this._UpToCotista);
					this._UpToCotista.Query.Where(this._UpToCotista.Query.IdCotista == this.IdCotista);
					this._UpToCotista.Query.Load();
				}

				return this._UpToCotista;
			}
			
			set 
			{ 
				this.RemovePreSave("UpToCotista");

				if(value == null)
				{
					this._UpToCotista = null;
				}
				else
				{
					this._UpToCotista = value;
					this.SetPreSave("UpToCotista", this._UpToCotista);
				}
				
				
			} 
		}

		private Cotista _UpToCotista;
		#endregion

		
		/// <summary>
		/// Used internally by the entity's hierarchical properties.
		/// </summary>
		protected override List<esPropertyDescriptor> GetHierarchicalProperties()
		{
			List<esPropertyDescriptor> props = new List<esPropertyDescriptor>();
			
		
			return props;
		}	
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PreSave.
		/// </summary>
		protected override void ApplyPreSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostSave.
		/// </summary>
		protected override void ApplyPostSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostOneToOneSave.
		/// </summary>
		protected override void ApplyPostOneSaveKeys()
		{
		}
		
	}



	[Serializable]
	abstract public class esTemplateCotistaQuery : esDynamicQuery
	{
		override protected IMetadata Meta
		{
			get
			{
				return TemplateCotistaMetadata.Meta();
			}
		}	
		

		public esQueryItem IdCotista
		{
			get
			{
				return new esQueryItem(this, TemplateCotistaMetadata.ColumnNames.IdCotista, esSystemType.Int32);
			}
		} 
		
		public esQueryItem Descricao
		{
			get
			{
				return new esQueryItem(this, TemplateCotistaMetadata.ColumnNames.Descricao, esSystemType.String);
			}
		} 
		
	}



	[Serializable]
	[XmlType("TemplateCotistaCollection")]
	public partial class TemplateCotistaCollection : esTemplateCotistaCollection, IEnumerable<TemplateCotista>
	{
		public TemplateCotistaCollection()
		{

		}
		
		public static implicit operator List<TemplateCotista>(TemplateCotistaCollection coll)
		{
			List<TemplateCotista> list = new List<TemplateCotista>();
			
			foreach (TemplateCotista emp in coll)
			{
				list.Add(emp);
			}
			
			return list;
		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return  TemplateCotistaMetadata.Meta();
			}
		}
		
		
		
		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new TemplateCotistaQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		
		override protected esEntity CreateEntityForCollection(DataRow row)
		{
			return new TemplateCotista(row);
		}

		override protected esEntity CreateEntity()
		{
			return new TemplateCotista();
		}
		
		
		#endregion


		[BrowsableAttribute( false )]
		public TemplateCotistaQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new TemplateCotistaQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}

		public bool Load(TemplateCotistaQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		public TemplateCotista AddNew()
		{
			TemplateCotista entity = base.AddNewEntity() as TemplateCotista;
			
			return entity;
		}

		public TemplateCotista FindByPrimaryKey(System.Int32 idCotista)
		{
			return base.FindByPrimaryKey(idCotista) as TemplateCotista;
		}


		#region IEnumerable<TemplateCotista> Members

		IEnumerator<TemplateCotista> IEnumerable<TemplateCotista>.GetEnumerator()
		{
			System.Collections.IEnumerable enumer = this as System.Collections.IEnumerable;
			System.Collections.IEnumerator iterator = enumer.GetEnumerator();

			while(iterator.MoveNext())
			{
				yield return iterator.Current as TemplateCotista;
			}
		}

		#endregion
		
		private TemplateCotistaQuery query;
	}


	/// <summary>
	/// Encapsulates the 'TemplateCotista' table
	/// </summary>

	[Serializable]
	public partial class TemplateCotista : esTemplateCotista
	{
		public TemplateCotista()
		{

		}
	
		public TemplateCotista(DataRow row)
			: base(row)
		{

		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return TemplateCotistaMetadata.Meta();
			}
		}
		
		
		
		override protected esTemplateCotistaQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new TemplateCotistaQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		#endregion
		



		[BrowsableAttribute( false )]
		public TemplateCotistaQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new TemplateCotistaQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}
		

		public bool Load(TemplateCotistaQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		private TemplateCotistaQuery query;
	}



	[Serializable]
	public partial class TemplateCotistaQuery : esTemplateCotistaQuery
	{
		public TemplateCotistaQuery()
		{

		}		
		
		public TemplateCotistaQuery(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
			
	}



	[Serializable]
	public partial class TemplateCotistaMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected TemplateCotistaMetadata()
		{
			_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(TemplateCotistaMetadata.ColumnNames.IdCotista, 0, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = TemplateCotistaMetadata.PropertyNames.IdCotista;
			c.IsInPrimaryKey = true;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TemplateCotistaMetadata.ColumnNames.Descricao, 1, typeof(System.String), esSystemType.String);
			c.PropertyName = TemplateCotistaMetadata.PropertyNames.Descricao;
			c.CharacterMaxLength = 100;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
		}
		#endregion
	
		static public TemplateCotistaMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base._dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base._columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string IdCotista = "IdCotista";
			 public const string Descricao = "Descricao";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string IdCotista = "IdCotista";
			 public const string Descricao = "Descricao";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(TemplateCotistaMetadata))
			{
				if(TemplateCotistaMetadata.mapDelegates == null)
				{
					TemplateCotistaMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (TemplateCotistaMetadata.meta == null)
				{
					TemplateCotistaMetadata.meta = new TemplateCotistaMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();
				

				meta.AddTypeMap("IdCotista", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("Descricao", new esTypeMap("varchar", "System.String"));			
				
				
				
				meta.Source = "TemplateCotista";
				meta.Destination = "TemplateCotista";
				
				meta.spInsert = "proc_TemplateCotistaInsert";				
				meta.spUpdate = "proc_TemplateCotistaUpdate";		
				meta.spDelete = "proc_TemplateCotistaDelete";
				meta.spLoadAll = "proc_TemplateCotistaLoadAll";
				meta.spLoadByPrimaryKey = "proc_TemplateCotistaLoadByPrimaryKey";
				
				this._providerMetadataMaps["esDefault"] = meta;
			}
			
			return this._providerMetadataMaps["esDefault"];
		}

		#endregion

		static private TemplateCotistaMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
