/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 23/05/2014 15:02:32
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;


using EntitySpaces.Interfaces;
using EntitySpaces.Core;
using Financial.Investidor;





































		



			
					


		

		
		
		

		

























		






	

















				
		
		




		








		
				
				












	

















					
								
											
	










		





		




				
				








				




		

		
		
		
		
		





namespace Financial.CRM
{

	[Serializable]
	abstract public class esPerfilInvestidorCollection : esEntityCollection
	{
		public esPerfilInvestidorCollection()
		{

		}

		protected override string GetCollectionName()
		{
			return "PerfilInvestidorCollection";
		}

		#region Query Logic
		protected void InitQuery(esPerfilInvestidorQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntityCollection)this).Connection;
		}

		protected bool OnQueryLoaded(DataTable table)
		{
			this.PopulateCollection(table);
			return (this.RowCount > 0) ? true : false;
		}
		
		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery(query as esPerfilInvestidorQuery);
		}
		#endregion
		
		virtual public PerfilInvestidor DetachEntity(PerfilInvestidor entity)
		{
			return base.DetachEntity(entity) as PerfilInvestidor;
		}
		
		virtual public PerfilInvestidor AttachEntity(PerfilInvestidor entity)
		{
			return base.AttachEntity(entity) as PerfilInvestidor;
		}
		
		virtual public void Combine(PerfilInvestidorCollection collection)
		{
			base.Combine(collection);
		}
		
		new public PerfilInvestidor this[int index]
		{
			get
			{
				return base[index] as PerfilInvestidor;
			}
		}

		public override Type GetEntityType()
		{
			return typeof(PerfilInvestidor);
		}
	}



	[Serializable]
	abstract public class esPerfilInvestidor : esEntity
	{
		/// <summary>
		/// Used internally by the entity's DynamicQuery mechanism.
		/// </summary>
		virtual protected esPerfilInvestidorQuery GetDynamicQuery()
		{
			return null;
		}

		public esPerfilInvestidor()
		{

		}

		public esPerfilInvestidor(DataRow row)
			: base(row)
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.Byte idPerfilInvestidor)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idPerfilInvestidor);
			else
				return LoadByPrimaryKeyStoredProcedure(idPerfilInvestidor);
		}

		/// <summary>
		/// Loads an entity by primary key
		/// </summary>
		/// <remarks>
		/// EntitySpaces requires primary keys be defined on all tables.
		/// If a table does not have a primary key set,
		/// this method will not compile.
		/// Does not support sqlAcessType. It only works with DynamicQuery
		/// </remarks>
		/// <param name="fieldsToReturn">Fields desired</param>
		public virtual bool LoadByPrimaryKey(List<esQueryItem> fieldsToReturn, System.Byte idPerfilInvestidor)
		{
			esQueryItem[] fields = fieldsToReturn.ToArray();			
			esPerfilInvestidorQuery query = this.GetDynamicQuery();
			query
			    .Select(fields)
			    .Where(query.IdPerfilInvestidor == idPerfilInvestidor);
		
			return query.Load();
		}
		
		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.Byte idPerfilInvestidor)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idPerfilInvestidor);
			else
				return LoadByPrimaryKeyStoredProcedure(idPerfilInvestidor);
		}

		private bool LoadByPrimaryKeyDynamic(System.Byte idPerfilInvestidor)
		{
			esPerfilInvestidorQuery query = this.GetDynamicQuery();
			query.Where(query.IdPerfilInvestidor == idPerfilInvestidor);
			return query.Load();
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.Byte idPerfilInvestidor)
		{
			esParameters parms = new esParameters();
			parms.Add("IdPerfilInvestidor",idPerfilInvestidor);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		
		
		#region Properties
		
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}

		public override void SetProperty(string name, object value)
		{
			if(this.Row == null) this.AddNew();
			
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value.GetType().ToString() == "System.String")
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "IdPerfilInvestidor": this.str.IdPerfilInvestidor = (string)value; break;							
						case "Descricao": this.str.Descricao = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "IdPerfilInvestidor":
						
							if (value == null || value.GetType().ToString() == "System.Byte")
								this.IdPerfilInvestidor = (System.Byte?)value;
							break;
					

						default:
							break;
					}
				}
			}
			else if(this.Row.Table.Columns.Contains(name))
			{
				this.Row[name] = value;
			}
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}
		
		
		/// <summary>
		/// Maps to PerfilInvestidor.IdPerfilInvestidor
		/// </summary>
		virtual public System.Byte? IdPerfilInvestidor
		{
			get
			{
				return base.GetSystemByte(PerfilInvestidorMetadata.ColumnNames.IdPerfilInvestidor);
			}
			
			set
			{
				base.SetSystemByte(PerfilInvestidorMetadata.ColumnNames.IdPerfilInvestidor, value);
			}
		}
		
		/// <summary>
		/// Maps to PerfilInvestidor.Descricao
		/// </summary>
		virtual public System.String Descricao
		{
			get
			{
				return base.GetSystemString(PerfilInvestidorMetadata.ColumnNames.Descricao);
			}
			
			set
			{
				base.SetSystemString(PerfilInvestidorMetadata.ColumnNames.Descricao, value);
			}
		}
		
		#endregion	

		#region String Properties


		[BrowsableAttribute( false )]
		public esStrings str
		{
			get
			{
				if (esstrings == null)
				{
					esstrings = new esStrings(this);
				}
				return esstrings;
			}
		}


		[Serializable]
		sealed public class esStrings
		{
			public esStrings(esPerfilInvestidor entity)
			{
				this.entity = entity;
			}
			
	
			public System.String IdPerfilInvestidor
			{
				get
				{
					System.Byte? data = entity.IdPerfilInvestidor;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdPerfilInvestidor = null;
					else entity.IdPerfilInvestidor = Convert.ToByte(value);
				}
			}
				
			public System.String Descricao
			{
				get
				{
					System.String data = entity.Descricao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Descricao = null;
					else entity.Descricao = Convert.ToString(value);
				}
			}
			

			private esPerfilInvestidor entity;
		}
		#endregion

		#region Query Logic
		protected void InitQuery(esPerfilInvestidorQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntity)this).Connection;
		}

		[System.Diagnostics.DebuggerNonUserCode]
		protected bool OnQueryLoaded(DataTable table)
		{
			bool dataFound = this.PopulateEntity(table);

			if (this.RowCount > 1)
			{
				throw new Exception("esPerfilInvestidor can only hold one record of data");
			}

			return dataFound;
		}
		#endregion
		
		[NonSerialized]
		private esStrings esstrings;
	}


	
	public partial class PerfilInvestidor : esPerfilInvestidor
	{

				
		#region PessoaCollectionByIdPerfilInvestidor - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - PerfilInvestidor_Pessoa_FK1
		/// </summary>

		[XmlIgnore]
		public PessoaCollection PessoaCollectionByIdPerfilInvestidor
		{
			get
			{
				if(this._PessoaCollectionByIdPerfilInvestidor == null)
				{
					this._PessoaCollectionByIdPerfilInvestidor = new PessoaCollection();
					this._PessoaCollectionByIdPerfilInvestidor.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("PessoaCollectionByIdPerfilInvestidor", this._PessoaCollectionByIdPerfilInvestidor);
				
					if(this.IdPerfilInvestidor != null)
					{
						this._PessoaCollectionByIdPerfilInvestidor.Query.Where(this._PessoaCollectionByIdPerfilInvestidor.Query.IdPerfilInvestidor == this.IdPerfilInvestidor);
						this._PessoaCollectionByIdPerfilInvestidor.Query.Load();

						// Auto-hookup Foreign Keys
						this._PessoaCollectionByIdPerfilInvestidor.fks.Add(PessoaMetadata.ColumnNames.IdPerfilInvestidor, this.IdPerfilInvestidor);
					}
				}

				return this._PessoaCollectionByIdPerfilInvestidor;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._PessoaCollectionByIdPerfilInvestidor != null) 
				{ 
					this.RemovePostSave("PessoaCollectionByIdPerfilInvestidor"); 
					this._PessoaCollectionByIdPerfilInvestidor = null;
					
				} 
			} 			
		}

		private PessoaCollection _PessoaCollectionByIdPerfilInvestidor;
		#endregion

				
		#region SuitabilityPerfilCollectionByIdPerfilInvestidor - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - FK_SuitabilityPerfil_PerfilInvestidor
		/// </summary>

		[XmlIgnore]
		public SuitabilityPerfilCollection SuitabilityPerfilCollectionByIdPerfilInvestidor
		{
			get
			{
				if(this._SuitabilityPerfilCollectionByIdPerfilInvestidor == null)
				{
					this._SuitabilityPerfilCollectionByIdPerfilInvestidor = new SuitabilityPerfilCollection();
					this._SuitabilityPerfilCollectionByIdPerfilInvestidor.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("SuitabilityPerfilCollectionByIdPerfilInvestidor", this._SuitabilityPerfilCollectionByIdPerfilInvestidor);
				
					if(this.IdPerfilInvestidor != null)
					{
						this._SuitabilityPerfilCollectionByIdPerfilInvestidor.Query.Where(this._SuitabilityPerfilCollectionByIdPerfilInvestidor.Query.IdPerfilInvestidor == this.IdPerfilInvestidor);
						this._SuitabilityPerfilCollectionByIdPerfilInvestidor.Query.Load();

						// Auto-hookup Foreign Keys
						this._SuitabilityPerfilCollectionByIdPerfilInvestidor.fks.Add(SuitabilityPerfilMetadata.ColumnNames.IdPerfilInvestidor, this.IdPerfilInvestidor);
					}
				}

				return this._SuitabilityPerfilCollectionByIdPerfilInvestidor;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._SuitabilityPerfilCollectionByIdPerfilInvestidor != null) 
				{ 
					this.RemovePostSave("SuitabilityPerfilCollectionByIdPerfilInvestidor"); 
					this._SuitabilityPerfilCollectionByIdPerfilInvestidor = null;
					
				} 
			} 			
		}

		private SuitabilityPerfilCollection _SuitabilityPerfilCollectionByIdPerfilInvestidor;
		#endregion

		
		/// <summary>
		/// Used internally by the entity's hierarchical properties.
		/// </summary>
		protected override List<esPropertyDescriptor> GetHierarchicalProperties()
		{
			List<esPropertyDescriptor> props = new List<esPropertyDescriptor>();
			
			props.Add(new esPropertyDescriptor(this, "PessoaCollectionByIdPerfilInvestidor", typeof(PessoaCollection), new Pessoa()));
			props.Add(new esPropertyDescriptor(this, "SuitabilityPerfilCollectionByIdPerfilInvestidor", typeof(SuitabilityPerfilCollection), new SuitabilityPerfil()));
		
			return props;
		}	
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PreSave.
		/// </summary>
		protected override void ApplyPreSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostSave.
		/// </summary>
		protected override void ApplyPostSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostOneToOneSave.
		/// </summary>
		protected override void ApplyPostOneSaveKeys()
		{
		}
		
	}



	[Serializable]
	abstract public class esPerfilInvestidorQuery : esDynamicQuery
	{
		override protected IMetadata Meta
		{
			get
			{
				return PerfilInvestidorMetadata.Meta();
			}
		}	
		

		public esQueryItem IdPerfilInvestidor
		{
			get
			{
				return new esQueryItem(this, PerfilInvestidorMetadata.ColumnNames.IdPerfilInvestidor, esSystemType.Byte);
			}
		} 
		
		public esQueryItem Descricao
		{
			get
			{
				return new esQueryItem(this, PerfilInvestidorMetadata.ColumnNames.Descricao, esSystemType.String);
			}
		} 
		
	}



	[Serializable]
	[XmlType("PerfilInvestidorCollection")]
	public partial class PerfilInvestidorCollection : esPerfilInvestidorCollection, IEnumerable<PerfilInvestidor>
	{
		public PerfilInvestidorCollection()
		{

		}
		
		public static implicit operator List<PerfilInvestidor>(PerfilInvestidorCollection coll)
		{
			List<PerfilInvestidor> list = new List<PerfilInvestidor>();
			
			foreach (PerfilInvestidor emp in coll)
			{
				list.Add(emp);
			}
			
			return list;
		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return  PerfilInvestidorMetadata.Meta();
			}
		}
		
		
		
		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new PerfilInvestidorQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		
		override protected esEntity CreateEntityForCollection(DataRow row)
		{
			return new PerfilInvestidor(row);
		}

		override protected esEntity CreateEntity()
		{
			return new PerfilInvestidor();
		}
		
		
		#endregion


		[BrowsableAttribute( false )]
		public PerfilInvestidorQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new PerfilInvestidorQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}

		public bool Load(PerfilInvestidorQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		public PerfilInvestidor AddNew()
		{
			PerfilInvestidor entity = base.AddNewEntity() as PerfilInvestidor;
			
			return entity;
		}

		public PerfilInvestidor FindByPrimaryKey(System.Byte idPerfilInvestidor)
		{
			return base.FindByPrimaryKey(idPerfilInvestidor) as PerfilInvestidor;
		}


		#region IEnumerable<PerfilInvestidor> Members

		IEnumerator<PerfilInvestidor> IEnumerable<PerfilInvestidor>.GetEnumerator()
		{
			System.Collections.IEnumerable enumer = this as System.Collections.IEnumerable;
			System.Collections.IEnumerator iterator = enumer.GetEnumerator();

			while(iterator.MoveNext())
			{
				yield return iterator.Current as PerfilInvestidor;
			}
		}

		#endregion
		
		private PerfilInvestidorQuery query;
	}


	/// <summary>
	/// Encapsulates the 'PerfilInvestidor' table
	/// </summary>

	[Serializable]
	public partial class PerfilInvestidor : esPerfilInvestidor
	{
		public PerfilInvestidor()
		{

		}
	
		public PerfilInvestidor(DataRow row)
			: base(row)
		{

		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return PerfilInvestidorMetadata.Meta();
			}
		}
		
		
		
		override protected esPerfilInvestidorQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new PerfilInvestidorQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		#endregion
		



		[BrowsableAttribute( false )]
		public PerfilInvestidorQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new PerfilInvestidorQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}
		

		public bool Load(PerfilInvestidorQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		private PerfilInvestidorQuery query;
	}



	[Serializable]
	public partial class PerfilInvestidorQuery : esPerfilInvestidorQuery
	{
		public PerfilInvestidorQuery()
		{

		}		
		
		public PerfilInvestidorQuery(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
			
	}



	[Serializable]
	public partial class PerfilInvestidorMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected PerfilInvestidorMetadata()
		{
			_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(PerfilInvestidorMetadata.ColumnNames.IdPerfilInvestidor, 0, typeof(System.Byte), esSystemType.Byte);
			c.PropertyName = PerfilInvestidorMetadata.PropertyNames.IdPerfilInvestidor;
			c.IsInPrimaryKey = true;	
			c.NumericPrecision = 3;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PerfilInvestidorMetadata.ColumnNames.Descricao, 1, typeof(System.String), esSystemType.String);
			c.PropertyName = PerfilInvestidorMetadata.PropertyNames.Descricao;
			c.CharacterMaxLength = 50;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
		}
		#endregion
	
		static public PerfilInvestidorMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base._dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base._columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string IdPerfilInvestidor = "IdPerfilInvestidor";
			 public const string Descricao = "Descricao";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string IdPerfilInvestidor = "IdPerfilInvestidor";
			 public const string Descricao = "Descricao";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(PerfilInvestidorMetadata))
			{
				if(PerfilInvestidorMetadata.mapDelegates == null)
				{
					PerfilInvestidorMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (PerfilInvestidorMetadata.meta == null)
				{
					PerfilInvestidorMetadata.meta = new PerfilInvestidorMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();
				

				meta.AddTypeMap("IdPerfilInvestidor", new esTypeMap("tinyint", "System.Byte"));
				meta.AddTypeMap("Descricao", new esTypeMap("varchar", "System.String"));			
				
				
				
				meta.Source = "PerfilInvestidor";
				meta.Destination = "PerfilInvestidor";
				
				meta.spInsert = "proc_PerfilInvestidorInsert";				
				meta.spUpdate = "proc_PerfilInvestidorUpdate";		
				meta.spDelete = "proc_PerfilInvestidorDelete";
				meta.spLoadAll = "proc_PerfilInvestidorLoadAll";
				meta.spLoadByPrimaryKey = "proc_PerfilInvestidorLoadByPrimaryKey";
				
				this._providerMetadataMaps["esDefault"] = meta;
			}
			
			return this._providerMetadataMaps["esDefault"];
		}

		#endregion

		static private PerfilInvestidorMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
