/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 23/05/2014 15:02:35
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;


using EntitySpaces.Interfaces;
using EntitySpaces.Core;





































		



			
					


		

		
		
		

		

























		






	

















				
		
		




		








		
				
				












	

















					
								
											
	










		





		




				
				








				




		

		
		
		
		
		





namespace Financial.CRM
{

	[Serializable]
	abstract public class esPessoaEnderecoCollection : esEntityCollection
	{
		public esPessoaEnderecoCollection()
		{

		}

		protected override string GetCollectionName()
		{
			return "PessoaEnderecoCollection";
		}

		#region Query Logic
		protected void InitQuery(esPessoaEnderecoQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntityCollection)this).Connection;
		}

		protected bool OnQueryLoaded(DataTable table)
		{
			this.PopulateCollection(table);
			return (this.RowCount > 0) ? true : false;
		}
		
		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery(query as esPessoaEnderecoQuery);
		}
		#endregion
		
		virtual public PessoaEndereco DetachEntity(PessoaEndereco entity)
		{
			return base.DetachEntity(entity) as PessoaEndereco;
		}
		
		virtual public PessoaEndereco AttachEntity(PessoaEndereco entity)
		{
			return base.AttachEntity(entity) as PessoaEndereco;
		}
		
		virtual public void Combine(PessoaEnderecoCollection collection)
		{
			base.Combine(collection);
		}
		
		new public PessoaEndereco this[int index]
		{
			get
			{
				return base[index] as PessoaEndereco;
			}
		}

		public override Type GetEntityType()
		{
			return typeof(PessoaEndereco);
		}
	}



	[Serializable]
	abstract public class esPessoaEndereco : esEntity
	{
		/// <summary>
		/// Used internally by the entity's DynamicQuery mechanism.
		/// </summary>
		virtual protected esPessoaEnderecoQuery GetDynamicQuery()
		{
			return null;
		}

		public esPessoaEndereco()
		{

		}

		public esPessoaEndereco(DataRow row)
			: base(row)
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.Int32 idEndereco, System.Int32 idPessoa)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idEndereco, idPessoa);
			else
				return LoadByPrimaryKeyStoredProcedure(idEndereco, idPessoa);
		}

		/// <summary>
		/// Loads an entity by primary key
		/// </summary>
		/// <remarks>
		/// EntitySpaces requires primary keys be defined on all tables.
		/// If a table does not have a primary key set,
		/// this method will not compile.
		/// Does not support sqlAcessType. It only works with DynamicQuery
		/// </remarks>
		/// <param name="fieldsToReturn">Fields desired</param>
		public virtual bool LoadByPrimaryKey(List<esQueryItem> fieldsToReturn, System.Int32 idEndereco, System.Int32 idPessoa)
		{
			esQueryItem[] fields = fieldsToReturn.ToArray();			
			esPessoaEnderecoQuery query = this.GetDynamicQuery();
			query
			    .Select(fields)
			    .Where(query.IdEndereco == idEndereco, query.IdPessoa == idPessoa);
		
			return query.Load();
		}
		
		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.Int32 idEndereco, System.Int32 idPessoa)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idEndereco, idPessoa);
			else
				return LoadByPrimaryKeyStoredProcedure(idEndereco, idPessoa);
		}

		private bool LoadByPrimaryKeyDynamic(System.Int32 idEndereco, System.Int32 idPessoa)
		{
			esPessoaEnderecoQuery query = this.GetDynamicQuery();
			query.Where(query.IdEndereco == idEndereco, query.IdPessoa == idPessoa);
			return query.Load();
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.Int32 idEndereco, System.Int32 idPessoa)
		{
			esParameters parms = new esParameters();
			parms.Add("IdEndereco",idEndereco);			parms.Add("IdPessoa",idPessoa);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		
		
		#region Properties
		
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}

		public override void SetProperty(string name, object value)
		{
			if(this.Row == null) this.AddNew();
			
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value.GetType().ToString() == "System.String")
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "IdEndereco": this.str.IdEndereco = (string)value; break;							
						case "IdPessoa": this.str.IdPessoa = (string)value; break;							
						case "RecebeCorrespondencia": this.str.RecebeCorrespondencia = (string)value; break;							
						case "Endereco": this.str.Endereco = (string)value; break;							
						case "Numero": this.str.Numero = (string)value; break;							
						case "Complemento": this.str.Complemento = (string)value; break;							
						case "Bairro": this.str.Bairro = (string)value; break;							
						case "Cidade": this.str.Cidade = (string)value; break;							
						case "Cep": this.str.Cep = (string)value; break;							
						case "Uf": this.str.Uf = (string)value; break;							
						case "Pais": this.str.Pais = (string)value; break;							
						case "TipoEndereco": this.str.TipoEndereco = (string)value; break;							
						case "CodigoMunicipio": this.str.CodigoMunicipio = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "IdEndereco":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdEndereco = (System.Int32?)value;
							break;
						
						case "IdPessoa":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdPessoa = (System.Int32?)value;
							break;
						
						case "CodigoMunicipio":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.CodigoMunicipio = (System.Int32?)value;
							break;
					

						default:
							break;
					}
				}
			}
			else if(this.Row.Table.Columns.Contains(name))
			{
				this.Row[name] = value;
			}
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}
		
		
		/// <summary>
		/// Maps to PessoaEndereco.IdEndereco
		/// </summary>
		virtual public System.Int32? IdEndereco
		{
			get
			{
				return base.GetSystemInt32(PessoaEnderecoMetadata.ColumnNames.IdEndereco);
			}
			
			set
			{
				base.SetSystemInt32(PessoaEnderecoMetadata.ColumnNames.IdEndereco, value);
			}
		}
		
		/// <summary>
		/// Maps to PessoaEndereco.IdPessoa
		/// </summary>
		virtual public System.Int32? IdPessoa
		{
			get
			{
				return base.GetSystemInt32(PessoaEnderecoMetadata.ColumnNames.IdPessoa);
			}
			
			set
			{
				if(base.SetSystemInt32(PessoaEnderecoMetadata.ColumnNames.IdPessoa, value))
				{
					this._UpToPessoaByIdPessoa = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to PessoaEndereco.RecebeCorrespondencia
		/// </summary>
		virtual public System.String RecebeCorrespondencia
		{
			get
			{
				return base.GetSystemString(PessoaEnderecoMetadata.ColumnNames.RecebeCorrespondencia);
			}
			
			set
			{
				base.SetSystemString(PessoaEnderecoMetadata.ColumnNames.RecebeCorrespondencia, value);
			}
		}
		
		/// <summary>
		/// Maps to PessoaEndereco.Endereco
		/// </summary>
		virtual public System.String Endereco
		{
			get
			{
				return base.GetSystemString(PessoaEnderecoMetadata.ColumnNames.Endereco);
			}
			
			set
			{
				base.SetSystemString(PessoaEnderecoMetadata.ColumnNames.Endereco, value);
			}
		}
		
		/// <summary>
		/// Maps to PessoaEndereco.Numero
		/// </summary>
		virtual public System.String Numero
		{
			get
			{
				return base.GetSystemString(PessoaEnderecoMetadata.ColumnNames.Numero);
			}
			
			set
			{
				base.SetSystemString(PessoaEnderecoMetadata.ColumnNames.Numero, value);
			}
		}
		
		/// <summary>
		/// Maps to PessoaEndereco.Complemento
		/// </summary>
		virtual public System.String Complemento
		{
			get
			{
				return base.GetSystemString(PessoaEnderecoMetadata.ColumnNames.Complemento);
			}
			
			set
			{
				base.SetSystemString(PessoaEnderecoMetadata.ColumnNames.Complemento, value);
			}
		}
		
		/// <summary>
		/// Maps to PessoaEndereco.Bairro
		/// </summary>
		virtual public System.String Bairro
		{
			get
			{
				return base.GetSystemString(PessoaEnderecoMetadata.ColumnNames.Bairro);
			}
			
			set
			{
				base.SetSystemString(PessoaEnderecoMetadata.ColumnNames.Bairro, value);
			}
		}
		
		/// <summary>
		/// Maps to PessoaEndereco.Cidade
		/// </summary>
		virtual public System.String Cidade
		{
			get
			{
				return base.GetSystemString(PessoaEnderecoMetadata.ColumnNames.Cidade);
			}
			
			set
			{
				base.SetSystemString(PessoaEnderecoMetadata.ColumnNames.Cidade, value);
			}
		}
		
		/// <summary>
		/// Maps to PessoaEndereco.CEP
		/// </summary>
		virtual public System.String Cep
		{
			get
			{
				return base.GetSystemString(PessoaEnderecoMetadata.ColumnNames.Cep);
			}
			
			set
			{
				base.SetSystemString(PessoaEnderecoMetadata.ColumnNames.Cep, value);
			}
		}
		
		/// <summary>
		/// Maps to PessoaEndereco.UF
		/// </summary>
		virtual public System.String Uf
		{
			get
			{
				return base.GetSystemString(PessoaEnderecoMetadata.ColumnNames.Uf);
			}
			
			set
			{
				base.SetSystemString(PessoaEnderecoMetadata.ColumnNames.Uf, value);
			}
		}
		
		/// <summary>
		/// Maps to PessoaEndereco.Pais
		/// </summary>
		virtual public System.String Pais
		{
			get
			{
				return base.GetSystemString(PessoaEnderecoMetadata.ColumnNames.Pais);
			}
			
			set
			{
				base.SetSystemString(PessoaEnderecoMetadata.ColumnNames.Pais, value);
			}
		}
		
		/// <summary>
		/// Maps to PessoaEndereco.TipoEndereco
		/// </summary>
		virtual public System.String TipoEndereco
		{
			get
			{
				return base.GetSystemString(PessoaEnderecoMetadata.ColumnNames.TipoEndereco);
			}
			
			set
			{
				base.SetSystemString(PessoaEnderecoMetadata.ColumnNames.TipoEndereco, value);
			}
		}
		
		/// <summary>
		/// Maps to PessoaEndereco.CodigoMunicipio
		/// </summary>
		virtual public System.Int32? CodigoMunicipio
		{
			get
			{
				return base.GetSystemInt32(PessoaEnderecoMetadata.ColumnNames.CodigoMunicipio);
			}
			
			set
			{
				base.SetSystemInt32(PessoaEnderecoMetadata.ColumnNames.CodigoMunicipio, value);
			}
		}
		
		[CLSCompliant(false)]
		internal protected Pessoa _UpToPessoaByIdPessoa;
		#endregion	

		#region String Properties


		[BrowsableAttribute( false )]
		public esStrings str
		{
			get
			{
				if (esstrings == null)
				{
					esstrings = new esStrings(this);
				}
				return esstrings;
			}
		}


		[Serializable]
		sealed public class esStrings
		{
			public esStrings(esPessoaEndereco entity)
			{
				this.entity = entity;
			}
			
	
			public System.String IdEndereco
			{
				get
				{
					System.Int32? data = entity.IdEndereco;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdEndereco = null;
					else entity.IdEndereco = Convert.ToInt32(value);
				}
			}
				
			public System.String IdPessoa
			{
				get
				{
					System.Int32? data = entity.IdPessoa;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdPessoa = null;
					else entity.IdPessoa = Convert.ToInt32(value);
				}
			}
				
			public System.String RecebeCorrespondencia
			{
				get
				{
					System.String data = entity.RecebeCorrespondencia;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.RecebeCorrespondencia = null;
					else entity.RecebeCorrespondencia = Convert.ToString(value);
				}
			}
				
			public System.String Endereco
			{
				get
				{
					System.String data = entity.Endereco;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Endereco = null;
					else entity.Endereco = Convert.ToString(value);
				}
			}
				
			public System.String Numero
			{
				get
				{
					System.String data = entity.Numero;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Numero = null;
					else entity.Numero = Convert.ToString(value);
				}
			}
				
			public System.String Complemento
			{
				get
				{
					System.String data = entity.Complemento;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Complemento = null;
					else entity.Complemento = Convert.ToString(value);
				}
			}
				
			public System.String Bairro
			{
				get
				{
					System.String data = entity.Bairro;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Bairro = null;
					else entity.Bairro = Convert.ToString(value);
				}
			}
				
			public System.String Cidade
			{
				get
				{
					System.String data = entity.Cidade;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Cidade = null;
					else entity.Cidade = Convert.ToString(value);
				}
			}
				
			public System.String Cep
			{
				get
				{
					System.String data = entity.Cep;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Cep = null;
					else entity.Cep = Convert.ToString(value);
				}
			}
				
			public System.String Uf
			{
				get
				{
					System.String data = entity.Uf;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Uf = null;
					else entity.Uf = Convert.ToString(value);
				}
			}
				
			public System.String Pais
			{
				get
				{
					System.String data = entity.Pais;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Pais = null;
					else entity.Pais = Convert.ToString(value);
				}
			}
				
			public System.String TipoEndereco
			{
				get
				{
					System.String data = entity.TipoEndereco;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.TipoEndereco = null;
					else entity.TipoEndereco = Convert.ToString(value);
				}
			}
				
			public System.String CodigoMunicipio
			{
				get
				{
					System.Int32? data = entity.CodigoMunicipio;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.CodigoMunicipio = null;
					else entity.CodigoMunicipio = Convert.ToInt32(value);
				}
			}
			

			private esPessoaEndereco entity;
		}
		#endregion

		#region Query Logic
		protected void InitQuery(esPessoaEnderecoQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntity)this).Connection;
		}

		[System.Diagnostics.DebuggerNonUserCode]
		protected bool OnQueryLoaded(DataTable table)
		{
			bool dataFound = this.PopulateEntity(table);

			if (this.RowCount > 1)
			{
				throw new Exception("esPessoaEndereco can only hold one record of data");
			}

			return dataFound;
		}
		#endregion
		
		[NonSerialized]
		private esStrings esstrings;
	}


	
	public partial class PessoaEndereco : esPessoaEndereco
	{

				
		#region UpToPessoaByIdPessoa - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - Pessoa_PessoaEndereco_FK1
		/// </summary>

		[XmlIgnore]
		public Pessoa UpToPessoaByIdPessoa
		{
			get
			{
				if(this._UpToPessoaByIdPessoa == null
					&& IdPessoa != null					)
				{
					this._UpToPessoaByIdPessoa = new Pessoa();
					this._UpToPessoaByIdPessoa.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToPessoaByIdPessoa", this._UpToPessoaByIdPessoa);
					this._UpToPessoaByIdPessoa.Query.Where(this._UpToPessoaByIdPessoa.Query.IdPessoa == this.IdPessoa);
					this._UpToPessoaByIdPessoa.Query.Load();
				}

				return this._UpToPessoaByIdPessoa;
			}
			
			set
			{
				this.RemovePreSave("UpToPessoaByIdPessoa");
				

				if(value == null)
				{
					this.IdPessoa = null;
					this._UpToPessoaByIdPessoa = null;
				}
				else
				{
					this.IdPessoa = value.IdPessoa;
					this._UpToPessoaByIdPessoa = value;
					this.SetPreSave("UpToPessoaByIdPessoa", this._UpToPessoaByIdPessoa);
				}
				
			}
		}
		#endregion
		

		
		/// <summary>
		/// Used internally by the entity's hierarchical properties.
		/// </summary>
		protected override List<esPropertyDescriptor> GetHierarchicalProperties()
		{
			List<esPropertyDescriptor> props = new List<esPropertyDescriptor>();
			
		
			return props;
		}	
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PreSave.
		/// </summary>
		protected override void ApplyPreSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostSave.
		/// </summary>
		protected override void ApplyPostSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostOneToOneSave.
		/// </summary>
		protected override void ApplyPostOneSaveKeys()
		{
		}
		
	}



	[Serializable]
	abstract public class esPessoaEnderecoQuery : esDynamicQuery
	{
		override protected IMetadata Meta
		{
			get
			{
				return PessoaEnderecoMetadata.Meta();
			}
		}	
		

		public esQueryItem IdEndereco
		{
			get
			{
				return new esQueryItem(this, PessoaEnderecoMetadata.ColumnNames.IdEndereco, esSystemType.Int32);
			}
		} 
		
		public esQueryItem IdPessoa
		{
			get
			{
				return new esQueryItem(this, PessoaEnderecoMetadata.ColumnNames.IdPessoa, esSystemType.Int32);
			}
		} 
		
		public esQueryItem RecebeCorrespondencia
		{
			get
			{
				return new esQueryItem(this, PessoaEnderecoMetadata.ColumnNames.RecebeCorrespondencia, esSystemType.String);
			}
		} 
		
		public esQueryItem Endereco
		{
			get
			{
				return new esQueryItem(this, PessoaEnderecoMetadata.ColumnNames.Endereco, esSystemType.String);
			}
		} 
		
		public esQueryItem Numero
		{
			get
			{
				return new esQueryItem(this, PessoaEnderecoMetadata.ColumnNames.Numero, esSystemType.String);
			}
		} 
		
		public esQueryItem Complemento
		{
			get
			{
				return new esQueryItem(this, PessoaEnderecoMetadata.ColumnNames.Complemento, esSystemType.String);
			}
		} 
		
		public esQueryItem Bairro
		{
			get
			{
				return new esQueryItem(this, PessoaEnderecoMetadata.ColumnNames.Bairro, esSystemType.String);
			}
		} 
		
		public esQueryItem Cidade
		{
			get
			{
				return new esQueryItem(this, PessoaEnderecoMetadata.ColumnNames.Cidade, esSystemType.String);
			}
		} 
		
		public esQueryItem Cep
		{
			get
			{
				return new esQueryItem(this, PessoaEnderecoMetadata.ColumnNames.Cep, esSystemType.String);
			}
		} 
		
		public esQueryItem Uf
		{
			get
			{
				return new esQueryItem(this, PessoaEnderecoMetadata.ColumnNames.Uf, esSystemType.String);
			}
		} 
		
		public esQueryItem Pais
		{
			get
			{
				return new esQueryItem(this, PessoaEnderecoMetadata.ColumnNames.Pais, esSystemType.String);
			}
		} 
		
		public esQueryItem TipoEndereco
		{
			get
			{
				return new esQueryItem(this, PessoaEnderecoMetadata.ColumnNames.TipoEndereco, esSystemType.String);
			}
		} 
		
		public esQueryItem CodigoMunicipio
		{
			get
			{
				return new esQueryItem(this, PessoaEnderecoMetadata.ColumnNames.CodigoMunicipio, esSystemType.Int32);
			}
		} 
		
	}



	[Serializable]
	[XmlType("PessoaEnderecoCollection")]
	public partial class PessoaEnderecoCollection : esPessoaEnderecoCollection, IEnumerable<PessoaEndereco>
	{
		public PessoaEnderecoCollection()
		{

		}
		
		public static implicit operator List<PessoaEndereco>(PessoaEnderecoCollection coll)
		{
			List<PessoaEndereco> list = new List<PessoaEndereco>();
			
			foreach (PessoaEndereco emp in coll)
			{
				list.Add(emp);
			}
			
			return list;
		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return  PessoaEnderecoMetadata.Meta();
			}
		}
		
		
		
		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new PessoaEnderecoQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		
		override protected esEntity CreateEntityForCollection(DataRow row)
		{
			return new PessoaEndereco(row);
		}

		override protected esEntity CreateEntity()
		{
			return new PessoaEndereco();
		}
		
		
		#endregion


		[BrowsableAttribute( false )]
		public PessoaEnderecoQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new PessoaEnderecoQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}

		public bool Load(PessoaEnderecoQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		public PessoaEndereco AddNew()
		{
			PessoaEndereco entity = base.AddNewEntity() as PessoaEndereco;
			
			return entity;
		}

		public PessoaEndereco FindByPrimaryKey(System.Int32 idEndereco, System.Int32 idPessoa)
		{
			return base.FindByPrimaryKey(idEndereco, idPessoa) as PessoaEndereco;
		}


		#region IEnumerable<PessoaEndereco> Members

		IEnumerator<PessoaEndereco> IEnumerable<PessoaEndereco>.GetEnumerator()
		{
			System.Collections.IEnumerable enumer = this as System.Collections.IEnumerable;
			System.Collections.IEnumerator iterator = enumer.GetEnumerator();

			while(iterator.MoveNext())
			{
				yield return iterator.Current as PessoaEndereco;
			}
		}

		#endregion
		
		private PessoaEnderecoQuery query;
	}


	/// <summary>
	/// Encapsulates the 'PessoaEndereco' table
	/// </summary>

	[Serializable]
	public partial class PessoaEndereco : esPessoaEndereco
	{
		public PessoaEndereco()
		{

		}
	
		public PessoaEndereco(DataRow row)
			: base(row)
		{

		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return PessoaEnderecoMetadata.Meta();
			}
		}
		
		
		
		override protected esPessoaEnderecoQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new PessoaEnderecoQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		#endregion
		



		[BrowsableAttribute( false )]
		public PessoaEnderecoQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new PessoaEnderecoQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}
		

		public bool Load(PessoaEnderecoQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		private PessoaEnderecoQuery query;
	}



	[Serializable]
	public partial class PessoaEnderecoQuery : esPessoaEnderecoQuery
	{
		public PessoaEnderecoQuery()
		{

		}		
		
		public PessoaEnderecoQuery(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
			
	}



	[Serializable]
	public partial class PessoaEnderecoMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected PessoaEnderecoMetadata()
		{
			_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(PessoaEnderecoMetadata.ColumnNames.IdEndereco, 0, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = PessoaEnderecoMetadata.PropertyNames.IdEndereco;
			c.IsInPrimaryKey = true;
			c.IsAutoIncrement = true;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PessoaEnderecoMetadata.ColumnNames.IdPessoa, 1, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = PessoaEnderecoMetadata.PropertyNames.IdPessoa;
			c.IsInPrimaryKey = true;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PessoaEnderecoMetadata.ColumnNames.RecebeCorrespondencia, 2, typeof(System.String), esSystemType.String);
			c.PropertyName = PessoaEnderecoMetadata.PropertyNames.RecebeCorrespondencia;
			c.CharacterMaxLength = 1;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PessoaEnderecoMetadata.ColumnNames.Endereco, 3, typeof(System.String), esSystemType.String);
			c.PropertyName = PessoaEnderecoMetadata.PropertyNames.Endereco;
			c.CharacterMaxLength = 200;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PessoaEnderecoMetadata.ColumnNames.Numero, 4, typeof(System.String), esSystemType.String);
			c.PropertyName = PessoaEnderecoMetadata.PropertyNames.Numero;
			c.CharacterMaxLength = 50;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PessoaEnderecoMetadata.ColumnNames.Complemento, 5, typeof(System.String), esSystemType.String);
			c.PropertyName = PessoaEnderecoMetadata.PropertyNames.Complemento;
			c.CharacterMaxLength = 50;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PessoaEnderecoMetadata.ColumnNames.Bairro, 6, typeof(System.String), esSystemType.String);
			c.PropertyName = PessoaEnderecoMetadata.PropertyNames.Bairro;
			c.CharacterMaxLength = 100;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PessoaEnderecoMetadata.ColumnNames.Cidade, 7, typeof(System.String), esSystemType.String);
			c.PropertyName = PessoaEnderecoMetadata.PropertyNames.Cidade;
			c.CharacterMaxLength = 100;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PessoaEnderecoMetadata.ColumnNames.Cep, 8, typeof(System.String), esSystemType.String);
			c.PropertyName = PessoaEnderecoMetadata.PropertyNames.Cep;
			c.CharacterMaxLength = 20;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PessoaEnderecoMetadata.ColumnNames.Uf, 9, typeof(System.String), esSystemType.String);
			c.PropertyName = PessoaEnderecoMetadata.PropertyNames.Uf;
			c.CharacterMaxLength = 2;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PessoaEnderecoMetadata.ColumnNames.Pais, 10, typeof(System.String), esSystemType.String);
			c.PropertyName = PessoaEnderecoMetadata.PropertyNames.Pais;
			c.CharacterMaxLength = 100;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PessoaEnderecoMetadata.ColumnNames.TipoEndereco, 11, typeof(System.String), esSystemType.String);
			c.PropertyName = PessoaEnderecoMetadata.PropertyNames.TipoEndereco;
			c.CharacterMaxLength = 1;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PessoaEnderecoMetadata.ColumnNames.CodigoMunicipio, 12, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = PessoaEnderecoMetadata.PropertyNames.CodigoMunicipio;	
			c.NumericPrecision = 10;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
		}
		#endregion
	
		static public PessoaEnderecoMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base._dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base._columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string IdEndereco = "IdEndereco";
			 public const string IdPessoa = "IdPessoa";
			 public const string RecebeCorrespondencia = "RecebeCorrespondencia";
			 public const string Endereco = "Endereco";
			 public const string Numero = "Numero";
			 public const string Complemento = "Complemento";
			 public const string Bairro = "Bairro";
			 public const string Cidade = "Cidade";
			 public const string Cep = "CEP";
			 public const string Uf = "UF";
			 public const string Pais = "Pais";
			 public const string TipoEndereco = "TipoEndereco";
			 public const string CodigoMunicipio = "CodigoMunicipio";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string IdEndereco = "IdEndereco";
			 public const string IdPessoa = "IdPessoa";
			 public const string RecebeCorrespondencia = "RecebeCorrespondencia";
			 public const string Endereco = "Endereco";
			 public const string Numero = "Numero";
			 public const string Complemento = "Complemento";
			 public const string Bairro = "Bairro";
			 public const string Cidade = "Cidade";
			 public const string Cep = "Cep";
			 public const string Uf = "Uf";
			 public const string Pais = "Pais";
			 public const string TipoEndereco = "TipoEndereco";
			 public const string CodigoMunicipio = "CodigoMunicipio";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(PessoaEnderecoMetadata))
			{
				if(PessoaEnderecoMetadata.mapDelegates == null)
				{
					PessoaEnderecoMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (PessoaEnderecoMetadata.meta == null)
				{
					PessoaEnderecoMetadata.meta = new PessoaEnderecoMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();
				

				meta.AddTypeMap("IdEndereco", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("IdPessoa", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("RecebeCorrespondencia", new esTypeMap("char", "System.String"));
				meta.AddTypeMap("Endereco", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("Numero", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("Complemento", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("Bairro", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("Cidade", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("CEP", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("UF", new esTypeMap("char", "System.String"));
				meta.AddTypeMap("Pais", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("TipoEndereco", new esTypeMap("char", "System.String"));
				meta.AddTypeMap("CodigoMunicipio", new esTypeMap("int", "System.Int32"));			
				
				
				
				meta.Source = "PessoaEndereco";
				meta.Destination = "PessoaEndereco";
				
				meta.spInsert = "proc_PessoaEnderecoInsert";				
				meta.spUpdate = "proc_PessoaEnderecoUpdate";		
				meta.spDelete = "proc_PessoaEnderecoDelete";
				meta.spLoadAll = "proc_PessoaEnderecoLoadAll";
				meta.spLoadByPrimaryKey = "proc_PessoaEnderecoLoadByPrimaryKey";
				
				this._providerMetadataMaps["esDefault"] = meta;
			}
			
			return this._providerMetadataMaps["esDefault"];
		}

		#endregion

		static private PessoaEnderecoMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
