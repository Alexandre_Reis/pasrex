﻿using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Text;
using System.Data;

using EntitySpaces.Interfaces;
using EntitySpaces.Core;
using Financial.Util;
using Financial.Common.Enums;
using Financial.ContaCorrente.Exceptions;
using Financial.Investidor.Exceptions;
using log4net;
using Financial.Common;
using Financial.Investidor;

namespace Financial.ContaCorrente {
    public partial class SaldoCaixa : esSaldoCaixa {

        /// <summary>
        /// Busca o saldo de caixa de fechamento do dia anterior, para compor o saldo de abertura do dia.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="data"></param>
        public void CalculaSaldoCaixaAbertura(int idCliente, DateTime data) 
        {
            Cliente cliente = new Cliente();
            cliente.Query.Select(cliente.Query.IdLocal);
            cliente.Query.Where(cliente.Query.IdCliente.Equal(idCliente));
            cliente.Query.Load();
            int idLocal = cliente.IdLocal.Value;

            DateTime dataAnterior = new DateTime();
            if (idLocal == LocalFeriadoFixo.Brasil)
            {
                dataAnterior = Calendario.SubtraiDiaUtil(data, 1, LocalFeriadoFixo.Brasil, TipoFeriado.Brasil);
            }
            else
            {
                dataAnterior = Calendario.SubtraiDiaUtil(data, 1, idLocal, TipoFeriado.Outros);
            }

            SaldoCaixaCollection saldoCaixaCollection = new SaldoCaixaCollection();
            SaldoCaixaCollection saldoCaixaCollectionInserir = new SaldoCaixaCollection();

            saldoCaixaCollection.BuscaSaldoCaixa(idCliente, dataAnterior);

            for (int i = 0; i < saldoCaixaCollection.Count; i++) {
                SaldoCaixa saldoCaixa = (SaldoCaixa)saldoCaixaCollection[i];
                SaldoCaixa saldoCaixaInserir = new SaldoCaixa();
                saldoCaixaInserir.AddNew();
                saldoCaixaInserir.IdCliente = idCliente;
                saldoCaixaInserir.Data = data;
                saldoCaixaInserir.IdConta = saldoCaixa.IdConta.Value;
                saldoCaixaInserir.SaldoAbertura = saldoCaixa.SaldoFechamento.GetValueOrDefault(0);

                saldoCaixaCollectionInserir.AttachEntity(saldoCaixaInserir);
            }

            saldoCaixaCollectionInserir.Save();
        }

        /// <summary>
        /// Calcula o saldo de caixa de fechamento, computando os valores liquidados no dia, 
        /// em cima do saldo de abertura.
        /// ContaDefaultNaoCadastradaException caso nenhuma conta default tenha sido cadastrada para o cliente.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="data"></param>
        public void CalculaSaldoCaixaFechamento(int idCliente, DateTime data) {
            //Busca todas as contas correntes do cliente, p/ver se alguma tem saldo e/ou liquidação na data

            Investidor.ContaCorrenteCollection contaCorrenteCollection = new Investidor.ContaCorrenteCollection();
            SaldoCaixaCollection saldoCaixaCollectionInserir = new SaldoCaixaCollection();

            Cliente cliente = new Cliente();
            int? idPessoa = null;
            if (cliente.LoadByPrimaryKey(idCliente))
            {
                idPessoa = cliente.IdPessoa;
            }

            contaCorrenteCollection.BuscaContaCorrente(idPessoa.Value);

            bool liquidouContaDefault = false;
            for (int i = 0; i < contaCorrenteCollection.Count; i++) 
            {
                Investidor.ContaCorrente contaCorrente = contaCorrenteCollection[i];
                int idConta = contaCorrente.IdConta.Value;
                
                SaldoCaixa saldoCaixa = new SaldoCaixa();
                bool temSaldo = false;
                decimal saldoAbertura = 0;
                if (saldoCaixa.LoadByPrimaryKey(idCliente, data, idConta)) 
                {
                    temSaldo = true;
                    saldoAbertura = saldoCaixa.SaldoAbertura.Value;
                }

                //Busca o total liquidado na idConta para o cliente
                Liquidacao liquidacao = new Liquidacao();
                decimal valorLiquidacaoDia = liquidacao.RetornaLiquidacao(idCliente, data, idConta);
                
                decimal saldoFechamento = saldoAbertura + valorLiquidacaoDia;

                //Usada p/ garantir liquidações com conta nula, em apenas 1 conta default                
                if (contaCorrente.IsContaDefault() && liquidouContaDefault == false) 
                {
                    liquidacao = new Liquidacao();
                    liquidacao.BuscaLiquidacaoContaNula(idCliente, data);
                    decimal valorLiquidacaoSemConta = liquidacao.Valor.Value;
                    liquidouContaDefault = true;
                    saldoFechamento = saldoFechamento + valorLiquidacaoSemConta;
                }

                if (temSaldo) //Update em SaldoCaixa
                {
                    saldoCaixa.SaldoFechamento = saldoFechamento;
                    saldoCaixa.Save();
                }
                else //Insert em SaldoCaixa
                {
                    SaldoCaixa saldoCaixaInserir = new SaldoCaixa();
                    saldoCaixaInserir.AddNew();
                    saldoCaixaInserir.IdCliente = idCliente;
                    saldoCaixaInserir.Data = data;
                    saldoCaixaInserir.IdConta = idConta;
                    saldoCaixaInserir.SaldoAbertura = saldoAbertura;
                    saldoCaixaInserir.SaldoFechamento = saldoFechamento;

                    saldoCaixaCollectionInserir.AttachEntity(saldoCaixaInserir);
                }
            }

            if (!liquidouContaDefault) {
                throw new ContaDefaultNaoCadastradaException("Conta default não cadastrada para o cliente " + idCliente + " / pessoa " + idPessoa.Value);
            }

            saldoCaixaCollectionInserir.Save();
        }

        /// <summary>
        /// Carrega o objeto SaldoCaixa com os campos SaldoAbertura.Sum() e SaldoFechamento.Sum().
        /// Se não achar registros SaldoAbertura = null ou SaldoFechamento = null
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="data"></param>                
        public void BuscaSaldoCaixa(int idCliente, DateTime data)
        {
            this.QueryReset();
            this.Query
                 .Select(this.Query.SaldoAbertura.Sum(), this.Query.SaldoFechamento.Sum())
                 .Where(this.Query.IdCliente == idCliente,
                        this.Query.Data.Equal(data));

            this.Query.Load();
        }

        /// <summary>
        /// Carrega o objeto SaldoCaixa com os campos SaldoAbertura.Sum() e SaldoFechamento.Sum().
        /// Se não achar registros SaldoAbertura = null ou SaldoFechamento = null.
        /// Leva em conta o IdMoeda passado e faz a conversão quando necessário.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="data"></param>
        /// <param name="idMoedaCliente"></param>
        public void BuscaSaldoCaixa(int idCliente, DateTime data, int idMoedaCliente)
        {
            SaldoCaixaCollection saldoCaixaCollection = new SaldoCaixaCollection();
            saldoCaixaCollection.Query.Select(saldoCaixaCollection.Query.SaldoAbertura.Sum(),
                                              saldoCaixaCollection.Query.SaldoFechamento.Sum(),
                                              saldoCaixaCollection.Query.IdConta);
            saldoCaixaCollection.Query.Where(saldoCaixaCollection.Query.IdCliente.Equal(idCliente),
                                             saldoCaixaCollection.Query.Data.Equal(data));
            saldoCaixaCollection.Query.GroupBy(saldoCaixaCollection.Query.IdConta);
            saldoCaixaCollection.Query.Load();

            decimal ptax = 0;
            if (idMoedaCliente == (int)ListaMoedaFixo.Dolar && saldoCaixaCollection.Count > 0)
            {
                CotacaoIndice cotacaoIndice = new CotacaoIndice();
                ptax = cotacaoIndice.BuscaCotacaoIndice((int)ListaIndiceFixo.PTAX_800VENDA, data);
            }

            decimal saldoAberturaTotal = 0;
            decimal saldoFechamentoTotal = 0;
            foreach (SaldoCaixa saldoCaixa in saldoCaixaCollection)
            {
                int idConta = saldoCaixa.IdConta.Value;
                decimal saldoAbertura = saldoCaixa.SaldoAbertura.GetValueOrDefault(0);
                decimal saldoFechamento = saldoCaixa.SaldoFechamento.GetValueOrDefault(0);

                //Trata multi-moeda
                Investidor.ContaCorrente contaCorrente = new Investidor.ContaCorrente();
                List<esQueryItem> campos = new List<esQueryItem>();
                campos.Add(contaCorrente.Query.IdMoeda);
                contaCorrente.LoadByPrimaryKey(campos, idConta);
                int idMoedaConta = contaCorrente.IdMoeda.Value;
                
                if (idMoedaCliente != idMoedaConta)
                {
                    decimal fatorConversao = 0;
                    if (idMoedaCliente == (int)ListaMoedaFixo.Dolar && idMoedaConta == (int)ListaMoedaFixo.Real)
                    {
                        fatorConversao = 1 / ptax;
                    }
                    else
                    {
                        ConversaoMoeda conversaoMoeda = new ConversaoMoeda();
                        conversaoMoeda.Query.Where(conversaoMoeda.Query.IdMoedaDe.Equal(idMoedaCliente),
                                                   conversaoMoeda.Query.IdMoedaPara.Equal(idMoedaConta));
                        if (conversaoMoeda.Query.Load())
                        {
                            int idIndiceConversao = conversaoMoeda.IdIndice.Value;
                            CotacaoIndice cotacaoIndice = new CotacaoIndice();
                            if (conversaoMoeda.Tipo.Value == (byte)TipoConversaoMoeda.Divide)
                            {
                                fatorConversao = 1 / cotacaoIndice.BuscaCotacaoIndice(idIndiceConversao, data);
                            }
                            else
                            {
                                fatorConversao = cotacaoIndice.BuscaCotacaoIndice(idIndiceConversao, data);
                            }
                        }
                    }

                    saldoAbertura = Math.Round(saldoAbertura * fatorConversao, 2);
                    saldoFechamento = Math.Round(saldoFechamento * fatorConversao, 2);
                }

                saldoAberturaTotal += saldoAbertura;
                saldoFechamentoTotal += saldoFechamento;
            }

            this.SaldoAbertura = saldoAberturaTotal;
            this.SaldoFechamento = saldoFechamentoTotal;
        }               
    }
}
