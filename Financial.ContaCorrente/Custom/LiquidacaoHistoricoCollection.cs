﻿using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Text;
using System.Data;

using EntitySpaces.Interfaces;
using EntitySpaces.Core;
using log4net;

namespace Financial.ContaCorrente
{
	public partial class LiquidacaoHistoricoCollection : esLiquidacaoHistoricoCollection
	{        
        // Construtor
        // Cria uma nova LiquidacaoHistoricoCollection com os dados de LiquidacaoCollection
        // Todos os dados de LiquidacaoCollection são copiados, adicionando a dataHistorico
        public LiquidacaoHistoricoCollection(LiquidacaoCollection liquidacaoCollection, DateTime dataHistorico)
        {
            using (esTransactionScope scope = new esTransactionScope())
            {
                int max = 0;
                LiquidacaoHistorico liquidacaoHistoricoMax = new LiquidacaoHistorico();
                liquidacaoHistoricoMax.Query.Select(liquidacaoHistoricoMax.Query.IdLiquidacao.Max());
                liquidacaoHistoricoMax.Query.Load();

                if (liquidacaoHistoricoMax.IdLiquidacao.HasValue)
                {
                    max = liquidacaoHistoricoMax.IdLiquidacao.Value + 1;
                }
                else
                {
                    max = 1;
                }

                for (int i = 0; i < liquidacaoCollection.Count; i++)
                {
                    LiquidacaoHistorico p = new LiquidacaoHistorico();

                    // Para cada Coluna de Liquidacao copia para LiquidacaoHistorico
                    foreach (esColumnMetadata colLiquidacao in liquidacaoCollection.es.Meta.Columns)
                    {
                        // Copia todas as colunas 
                        esColumnMetadata colLiquidacaoHistorico = p.es.Meta.Columns.FindByPropertyName(colLiquidacao.PropertyName);
                        if (liquidacaoCollection[i].GetColumn(colLiquidacao.Name) != null)
                        {
                            p.SetColumn(colLiquidacaoHistorico.Name, liquidacaoCollection[i].GetColumn(colLiquidacao.Name));
                        }
                    }
                    
                    p.DataHistorico = dataHistorico;
                    p.IdLiquidacao = max;

                    max += 1;

                    this.AttachEntity(p);
                }

                scope.Complete();
            }
        }    

        /// <summary>
        /// Carrega o objeto LiquidacaoHistoricoCollection com todos os campos de LiquidacaoHistorico.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="dataHistorico"></param>
        public void BuscaLiquidacaoHistoricoCompleta(int idCliente, DateTime dataHistorico)
        {
            this.QueryReset();
            this.Query.Where(this.Query.IdCliente == idCliente,
                             this.Query.DataHistorico.Equal(dataHistorico));
            this.Query.Load();
        }

        /// <summary>
        /// Deleta todas as liquidações históricas com data >= que a data passada.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="dataHistorico"></param>
        public void DeletaLiquidacaoHistoricoDataHistoricoMaiorIgual(int idCliente, DateTime dataHistorico)
        {
            this.QueryReset();
            this.Query
                    .Select(this.Query.IdLiquidacao, this.Query.DataHistorico)
                    .Where(this.Query.IdCliente == idCliente,
                           this.Query.DataHistorico.GreaterThanOrEqual(dataHistorico));

            this.Query.Load();

            this.MarkAllAsDeleted();
            this.Save();
        }

        /// <summary>
        /// Deleta todas as liquidações históricas com data > que a data passada.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="dataHistorico"></param>
        public void DeletaLiquidacaoHistoricoDataHistoricoMaior(int idCliente, DateTime dataHistorico)
        {
            this.QueryReset();
            this.Query
                    .Select(this.Query.IdLiquidacao, this.Query.DataHistorico)
                    .Where(this.Query.IdCliente == idCliente,
                           this.Query.DataHistorico.GreaterThan(dataHistorico));

            this.Query.Load();

            this.MarkAllAsDeleted();
            this.Save();
        }

        /// <summary>
        /// Carrega o objeto LiquidacaoCollection com os campos Descricao, Valor.Sum().
        /// Filtro para DataVencimento.GreaterThanOrEqual(dataReferencia),
        ///             DataLancamento.LessThanOrEqual(dataReferencia),
        ///             DataHistorico.Equal(dataReferencia).
        /// GroupBy(Descricao).
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="dataReferencia"></param>
        public void BuscaLiquidacaoAgrupado(int idCliente, DateTime dataReferencia)
        {
            this.QueryReset();
            this.Query.Select(this.Query.Descricao, this.Query.Valor.Sum());
            this.Query.Where(this.Query.IdCliente == idCliente,
                             this.Query.DataVencimento.GreaterThanOrEqual(dataReferencia),
                             this.Query.DataLancamento.LessThanOrEqual(dataReferencia),
                             this.Query.DataHistorico.Equal(dataReferencia));
            this.Query.GroupBy(this.Query.Descricao);
            this.Query.Load();
        }

        /// <summary>
        /// Carrega o objeto LiquidacaoCollection com todos os campos de LiquidacaoHistorico.
        /// Filtro para DataVencimento.GreaterThan(data),
        ///             DataLancamento.LessThanOrEqual(data),
        ///             DataHistorico.Equal(data).
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="data"></param>
        public void BuscaLiquidacaoVencimentoMaior(int idCliente, DateTime data)
        {
            this.QueryReset();
            this.Query.Where(this.Query.IdCliente == idCliente,
                             this.Query.DataVencimento.GreaterThan(data),
                             this.Query.DataLancamento.LessThanOrEqual(data),
                             this.Query.DataHistorico.Equal(data));
            this.Query.Load();
        }

	}
}
