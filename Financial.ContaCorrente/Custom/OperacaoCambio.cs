﻿using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Text;
using System.Data;

using EntitySpaces.Interfaces;
using EntitySpaces.Core;
using Financial.ContaCorrente.Enums;

namespace Financial.ContaCorrente
{
    public partial class OperacaoCambio : esOperacaoCambio
	{
        /// <summary>
        /// Joga na Liquidacao as operações de entrada e saida de câmbio em cada c/c.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="data"></param>
        public void ProcessaOperacao(int idCliente, DateTime data)
        {
            OperacaoCambioCollection operacaoCambioCollection = new OperacaoCambioCollection();
            operacaoCambioCollection.Query.Where(operacaoCambioCollection.Query.IdCliente.Equal(idCliente),
                                                 operacaoCambioCollection.Query.DataRegistro.Equal(data));
            operacaoCambioCollection.Query.Load();
                        
            foreach (OperacaoCambio operacaoCambio in operacaoCambioCollection)
            {
                DateTime dataLiquidacao = operacaoCambio.DataLiquidacao.Value;
                int idContaOrigem = operacaoCambio.IdContaOrigem.Value;
                int idContaDestino = operacaoCambio.IdContaDestino.Value;

                Investidor.ContaCorrente contaOrigem = new Investidor.ContaCorrente();
                contaOrigem.LoadByPrimaryKey(idContaOrigem);

                Investidor.ContaCorrente contaDestino = new Investidor.ContaCorrente();
                contaDestino.LoadByPrimaryKey(idContaDestino);

                decimal principalOrigem = operacaoCambio.PrincipalOrigem.Value;
                decimal principalDestino = operacaoCambio.PrincipalDestino.Value;
                decimal custosOrigem = operacaoCambio.CustosOrigem.Value;
                decimal custosDestino = operacaoCambio.CustosDestino.Value;
                decimal tributosOrigem = operacaoCambio.TributosOrigem.Value;
                decimal tributosDestino = operacaoCambio.TributosDestino.Value;                

                #region Lancamento em Liquidacao (Principal origem)
                Liquidacao liquidacao = new Liquidacao();
                liquidacao.DataLancamento = data;

                liquidacao.DataVencimento = dataLiquidacao;
                liquidacao.Descricao = "Câmbio de origem - conta (" + contaOrigem.Numero + ")";
                liquidacao.Valor = principalOrigem * -1;
                liquidacao.Situacao = (byte)SituacaoLancamentoLiquidacao.Normal;
                liquidacao.Origem = (int)OrigemLancamentoLiquidacao.Cambio.PrincipalOrigem;
                liquidacao.Fonte = (int)FonteLancamentoLiquidacao.Interno;
                liquidacao.IdCliente = idCliente;
                liquidacao.IdConta = idContaOrigem;
                liquidacao.Save();
                #endregion

                #region Lancamento em Liquidacao (Principal destino)
                liquidacao = new Liquidacao();
                liquidacao.DataLancamento = data;

                liquidacao.DataVencimento = dataLiquidacao;
                liquidacao.Descricao = "Câmbio de destino - conta (" + contaDestino.Numero + ")";
                liquidacao.Valor = principalDestino;
                liquidacao.Situacao = (byte)SituacaoLancamentoLiquidacao.Normal;
                liquidacao.Origem = (int)OrigemLancamentoLiquidacao.Cambio.PrincipalDestino;
                liquidacao.Fonte = (int)FonteLancamentoLiquidacao.Interno;
                liquidacao.IdCliente = idCliente;
                liquidacao.IdConta = idContaDestino;
                liquidacao.Save();
                #endregion

                #region Lancamento em Liquidacao (Custos origem)
                liquidacao = new Liquidacao();
                liquidacao.DataLancamento = data;

                liquidacao.DataVencimento = dataLiquidacao;
                liquidacao.Descricao = "Custos s/ Câmbio de origem - conta (" + contaOrigem.Numero + ")";
                liquidacao.Valor = custosOrigem * -1;
                liquidacao.Situacao = (byte)SituacaoLancamentoLiquidacao.Normal;
                liquidacao.Origem = (int)OrigemLancamentoLiquidacao.Cambio.Custos;
                liquidacao.Fonte = (int)FonteLancamentoLiquidacao.Interno;
                liquidacao.IdCliente = idCliente;
                liquidacao.IdConta = idContaOrigem;
                liquidacao.Save();
                #endregion

                #region Lancamento em Liquidacao (Custos destino)
                liquidacao = new Liquidacao();
                liquidacao.DataLancamento = data;

                liquidacao.DataVencimento = dataLiquidacao;
                liquidacao.Descricao = "Custos s/ Câmbio de destino - conta (" + contaDestino.Numero + ")";
                liquidacao.Valor = custosDestino * -1;
                liquidacao.Situacao = (byte)SituacaoLancamentoLiquidacao.Normal;
                liquidacao.Origem = (int)OrigemLancamentoLiquidacao.Cambio.Custos;
                liquidacao.Fonte = (int)FonteLancamentoLiquidacao.Interno;
                liquidacao.IdCliente = idCliente;
                liquidacao.IdConta = idContaDestino;
                liquidacao.Save();
                #endregion

                #region Lancamento em Liquidacao (Tributos origem)
                liquidacao = new Liquidacao();
                liquidacao.DataLancamento = data;

                liquidacao.DataVencimento = dataLiquidacao;
                liquidacao.Descricao = "Tributos s/ Câmbio de origem - conta (" + contaOrigem.Numero + ")";
                liquidacao.Valor = tributosOrigem * -1;
                liquidacao.Situacao = (byte)SituacaoLancamentoLiquidacao.Normal;
                liquidacao.Origem = (int)OrigemLancamentoLiquidacao.Cambio.Tributos;
                liquidacao.Fonte = (int)FonteLancamentoLiquidacao.Interno;
                liquidacao.IdCliente = idCliente;
                liquidacao.IdConta = idContaOrigem;
                liquidacao.Save();
                #endregion

                #region Lancamento em Liquidacao (Tributos destino)
                liquidacao = new Liquidacao();
                liquidacao.DataLancamento = data;

                liquidacao.DataVencimento = dataLiquidacao;
                liquidacao.Descricao = "Tributos s/ Câmbio de destino - conta (" + contaDestino.Numero + ")";
                liquidacao.Valor = tributosDestino * -1;
                liquidacao.Situacao = (byte)SituacaoLancamentoLiquidacao.Normal;
                liquidacao.Origem = (int)OrigemLancamentoLiquidacao.Cambio.Tributos;
                liquidacao.Fonte = (int)FonteLancamentoLiquidacao.Interno;
                liquidacao.IdCliente = idCliente;
                liquidacao.IdConta = idContaDestino;
                liquidacao.Save();
                #endregion
            }
        }
	}
}
