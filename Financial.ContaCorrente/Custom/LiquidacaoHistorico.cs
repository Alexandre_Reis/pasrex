﻿using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Text;
using System.Data;

using EntitySpaces.Interfaces;
using EntitySpaces.Core;
using Financial.ContaCorrente.Enums;

namespace Financial.ContaCorrente
{
	public partial class LiquidacaoHistorico : esLiquidacaoHistorico
	{
        /// <summary>
        /// Retorna o total de lancamentos a vencer em Liquidacao.
        /// Filtra por DataHistorico.Equal(dataHistorico), DataVencimento.GreaterThan(dataHistorico), DataLancamento.LessThanOrEqual(dataHistorico).
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="dataHistorico"></param>
        public decimal RetornaLiquidacaoHistorico(int idCliente, DateTime dataHistorico)
        {
            this.QueryReset();
            this.Query
              .Select(this.Query.Valor.Sum())
              .Where(this.Query.IdCliente == idCliente,
                     this.Query.DataHistorico.Equal(dataHistorico),
                     this.Query.DataVencimento.GreaterThan(dataHistorico),
                     this.Query.DataLancamento.LessThanOrEqual(dataHistorico));

            this.Query.Load();

            decimal valor = 0;
            if (this.es.HasData)
            {
                valor = this.Valor.HasValue ? this.Valor.Value : 0;
            }

            return valor;
        }

        /// <summary>
        /// Retorna o total de lancamentos a vencer em Liquidacao.
        /// Filtra por DataHistorico.Equal(dataHistorico), DataVencimento.GreaterThan(dataHistorico), DataLancamento.LessThanOrEqual(dataHistorico).
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="dataHistorico"></param>
        public decimal RetornaLiquidacaoHistoricoPagar(int idCliente, DateTime dataHistorico)
        {
            this.QueryReset();
            this.Query
              .Select(this.Query.Valor.Sum())
              .Where(this.Query.IdCliente == idCliente,
                     this.Query.DataHistorico.Equal(dataHistorico),
                     this.Query.DataVencimento.GreaterThan(dataHistorico),
                     this.Query.DataLancamento.LessThanOrEqual(dataHistorico),
                     this.Query.Valor.LessThan(0));

            this.Query.Load();

            decimal valor = 0;
            if (this.es.HasData)
            {
                valor = this.Valor.HasValue ? this.Valor.Value : 0;
            }

            return valor;
        }

        /// <summary>
        /// Retorna o total de lancamentos a vencer em Liquidacao.
        /// Filtra por DataHistorico.Equal(dataHistorico), DataVencimento.GreaterThan(dataHistorico), DataLancamento.LessThanOrEqual(dataHistorico).
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="dataHistorico"></param>
        public decimal RetornaLiquidacaoHistoricoReceber(int idCliente, DateTime dataHistorico)
        {
            this.QueryReset();
            this.Query
              .Select(this.Query.Valor.Sum())
              .Where(this.Query.IdCliente == idCliente,
                     this.Query.DataHistorico.Equal(dataHistorico),
                     this.Query.DataVencimento.GreaterThan(dataHistorico),
                     this.Query.DataLancamento.LessThanOrEqual(dataHistorico),
                     this.Query.Valor.GreaterThan(0));

            this.Query.Load();

            decimal valor = 0;
            if (this.es.HasData)
            {
                valor = this.Valor.HasValue ? this.Valor.Value : 0;
            }

            return valor;
        }

        /// <summary>
        /// Função básica para copiar de LiquidacaoHistorico para LiquidacaoFuturo.
        /// Fltra apenas lançamentos com Fonte = Manual e Origem = Outros e DataHistorico.GreaterThan(data).
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="data"></param>
        public void GeraLiquidacaoFuturo(int idCliente, DateTime data)
        {
            LiquidacaoFuturoCollection liquidacaoFuturoCollectionDeletar = new LiquidacaoFuturoCollection();
            liquidacaoFuturoCollectionDeletar.Query.Where(liquidacaoFuturoCollectionDeletar.Query.IdCliente.Equal(idCliente),
                                             liquidacaoFuturoCollectionDeletar.Query.Fonte.Equal((byte)FonteLancamentoLiquidacao.Manual),
                                             liquidacaoFuturoCollectionDeletar.Query.Origem.In((int)OrigemLancamentoLiquidacao.Outros, (int)OrigemLancamentoLiquidacao.CarteiraGerencial),
                                             liquidacaoFuturoCollectionDeletar.Query.DataHistorico.GreaterThanOrEqual(data));
            liquidacaoFuturoCollectionDeletar.Query.Load();
            liquidacaoFuturoCollectionDeletar.MarkAllAsDeleted();
            liquidacaoFuturoCollectionDeletar.Save();

            LiquidacaoHistoricoCollection liquidacaoHistoricoCollection = new LiquidacaoHistoricoCollection();
            liquidacaoHistoricoCollection.Query.Where(liquidacaoHistoricoCollection.Query.IdCliente.Equal(idCliente),
                                             liquidacaoHistoricoCollection.Query.Fonte.Equal((byte)FonteLancamentoLiquidacao.Manual),
                                             liquidacaoHistoricoCollection.Query.Origem.In((int)OrigemLancamentoLiquidacao.Outros, (int)OrigemLancamentoLiquidacao.CarteiraGerencial),
                                             liquidacaoHistoricoCollection.Query.DataLancamento.Equal(liquidacaoHistoricoCollection.Query.DataHistorico),
                                             liquidacaoHistoricoCollection.Query.DataHistorico.GreaterThanOrEqual(data));
            liquidacaoHistoricoCollection.Query.Load();

            LiquidacaoFuturoCollection liquidacaoFuturoCollection = new LiquidacaoFuturoCollection();
            
            #region Copia de LiquidacaoHistorico para LiquidacaoFuturo
            for (int i = 0; i < liquidacaoHistoricoCollection.Count; i++)
            {
                LiquidacaoHistorico liquidacaoHistorico = liquidacaoHistoricoCollection[i];

                LiquidacaoFuturo liquidacaoFuturo = liquidacaoFuturoCollection.AddNew();

                liquidacaoFuturo.DataHistorico = liquidacaoHistorico.DataHistorico;
                liquidacaoFuturo.IdLiquidacao = liquidacaoHistorico.IdLiquidacao;
                liquidacaoFuturo.IdCliente = liquidacaoHistorico.IdCliente;
                liquidacaoFuturo.DataLancamento = liquidacaoHistorico.DataLancamento;
                liquidacaoFuturo.DataVencimento = liquidacaoHistorico.DataVencimento;
                liquidacaoFuturo.Descricao = liquidacaoHistorico.Descricao;
                liquidacaoFuturo.Valor = liquidacaoHistorico.Valor;
                liquidacaoFuturo.Situacao = liquidacaoHistorico.Situacao;
                liquidacaoFuturo.Origem = liquidacaoHistorico.Origem;
                liquidacaoFuturo.Fonte = liquidacaoHistorico.Fonte;
                liquidacaoFuturo.IdAgente = liquidacaoHistorico.IdAgente;
                liquidacaoFuturo.IdentificadorOrigem = liquidacaoHistorico.IdentificadorOrigem;
                liquidacaoFuturo.IdConta = liquidacaoHistorico.IdConta;
                liquidacaoFuturo.IdEvento = liquidacaoHistorico.IdEvento;
                liquidacaoFuturo.IdEventoVencimento = liquidacaoHistorico.IdEventoVencimento;
                liquidacaoFuturo.CodigoAtivo = liquidacaoHistorico.CodigoAtivo;
            }
            #endregion

            liquidacaoFuturoCollection.Save();
        }

        public static LiquidacaoHistorico GeraLiquidacaoHistorico(Liquidacao liquidacao, DateTime dataPosicao)
        {
            LiquidacaoHistorico liquidacaoHistorico = new LiquidacaoHistorico();

            liquidacaoHistorico.IdLiquidacao = liquidacao.IdLiquidacao;
            liquidacaoHistorico.IdCliente = liquidacao.IdCliente;
            liquidacaoHistorico.DataHistorico = dataPosicao;
            liquidacaoHistorico.DataLancamento = liquidacao.DataLancamento;
            liquidacaoHistorico.DataVencimento = liquidacao.DataVencimento;
            liquidacaoHistorico.Descricao = liquidacao.Descricao;
            liquidacaoHistorico.Valor = liquidacao.Valor;
            liquidacaoHistorico.Situacao = liquidacao.Situacao;
            liquidacaoHistorico.Origem = liquidacao.Origem;
            liquidacaoHistorico.Fonte = liquidacao.Fonte;
            liquidacaoHistorico.IdAgente = liquidacao.IdAgente;
            liquidacaoHistorico.IdentificadorOrigem = liquidacao.IdentificadorOrigem;
            liquidacaoHistorico.IdConta = liquidacao.IdConta;
            liquidacaoHistorico.IdEvento = liquidacao.IdEvento;
            liquidacaoHistorico.IdEventoVencimento = liquidacao.IdEventoVencimento;
            liquidacaoHistorico.CodigoAtivo = liquidacao.CodigoAtivo;


            liquidacaoHistorico.Save();
            return liquidacaoHistorico;
        }

	}
}
