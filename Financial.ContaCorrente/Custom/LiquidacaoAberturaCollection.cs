﻿using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Text;
using System.Data;

using EntitySpaces.Interfaces;
using EntitySpaces.Core;
using log4net;
using Financial.ContaCorrente.Enums;

namespace Financial.ContaCorrente
{
	public partial class LiquidacaoAberturaCollection : esLiquidacaoAberturaCollection
	{

         public LiquidacaoAberturaCollection(LiquidacaoCollection posicaoLiquidacaoCollection, DateTime dataHistorico)
        {
            for (int i = 0; i < posicaoLiquidacaoCollection.Count; i++)
            {
                //
                LiquidacaoAbertura p = new LiquidacaoAbertura();

                // Para cada Coluna de Liquidacao copia para Liquidacao
                foreach (esColumnMetadata colPosicao in posicaoLiquidacaoCollection.es.Meta.Columns)
                {
                    // Copia todas as colunas menos a Data 
                    esColumnMetadata colLiquidacao = p.es.Meta.Columns.FindByPropertyName(colPosicao.PropertyName);
                    if (posicaoLiquidacaoCollection[i].GetColumn(colPosicao.Name) != null)
                    {
                        p.SetColumn(colLiquidacao.Name, posicaoLiquidacaoCollection[i].GetColumn(colPosicao.Name));
                    }
                    p.DataHistorico = dataHistorico;
                }
                this.AttachEntity(p);
            }
        }

        /// <summary>
        /// Carrega o objeto LiquidacaoAberturaCollection com todos os campos de LiquidacaoAbertura.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="dataHistorico"></param>
        public void BuscaLiquidacaoAberturaCompleta(int idCliente, DateTime dataHistorico)
        {
            this.QueryReset();
            this.Query.Where(this.Query.IdCliente == idCliente,
                             this.Query.DataHistorico.Equal(dataHistorico));
            this.Query.Load();
        }

        /// <summary>
        /// Carrega o objeto LiquidacaoAberturaCollection com todos os campos de LiquidacaoAbertura.
        /// Filtra por: Fonte.Equal((byte)FonteLancamentoLiquidacao.Interno)
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="dataHistorico"></param>
        public void BuscaLiquidacaoAberturaCompletaSomenteInternos(int idCliente, DateTime dataHistorico)
        {
            this.QueryReset();
            this.Query.Where(this.Query.IdCliente == idCliente,
                             this.Query.DataHistorico.Equal(dataHistorico),
                             this.Query.Fonte.Equal((byte)FonteLancamentoLiquidacao.Interno));
            this.Query.Load();
        }

        /// <summary>
        /// Deleta todas as liquidações históricas com data > que a data passada.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="dataHistorico"></param>
        public void DeletaLiquidacaoAberturaDataHistoricoMaior(int idCliente, DateTime dataHistorico)
        {
            this.QueryReset();
            this.Query
                    .Select(this.Query.IdLiquidacao, this.Query.DataHistorico)
                    .Where(this.Query.IdCliente == idCliente,
                           this.Query.DataHistorico.GreaterThan(dataHistorico));

            this.Query.Load();

            this.MarkAllAsDeleted();
            this.Save();
        }

	}
}
