﻿using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Text;
using System.Data;

using EntitySpaces.Interfaces;
using EntitySpaces.Core;
using Financial.ContaCorrente.Enums;
using log4net;
using Financial.Investidor;
using Financial.Investidor.Enums;

namespace Financial.ContaCorrente {
    public partial class LiquidacaoCollection : esLiquidacaoCollection 
    {
        // Construtor
        // Cria uma nova LiquidacaoCollection com os dados de LiquidacaoHistoricoCollection
        // Todos do dados de LiquidacaoHistoricoCollection são copiados com excessão da dataHistorico
        public LiquidacaoCollection(LiquidacaoHistoricoCollection liquidacaoHistoricoCollection) {
            for (int i = 0; i < liquidacaoHistoricoCollection.Count; i++) {
                //
                Liquidacao l = new Liquidacao();

                // Para cada Coluna de LiquidacaoHistorico copia para Liquidacao
                foreach (esColumnMetadata colLiquidacaoHistorico in liquidacaoHistoricoCollection.es.Meta.Columns) {
                    // Copia todas as colunas menos a Data Historico
                    if (colLiquidacaoHistorico.PropertyName != LiquidacaoHistoricoMetadata.ColumnNames.DataHistorico) {
                        esColumnMetadata colLiquidacao = l.es.Meta.Columns.FindByPropertyName(colLiquidacaoHistorico.PropertyName);
                        if (liquidacaoHistoricoCollection[i].GetColumn(colLiquidacao.Name) != null) {
                            l.SetColumn(colLiquidacao.Name, liquidacaoHistoricoCollection[i].GetColumn(colLiquidacao.Name));
                        }
                    }
                }
                this.AttachEntity(l);
            }
        }

        /// <summary>
        /// Recupera o DataTable da Liquidacao
        /// </summary>
        /// <returns></returns>
        public DataTable GetTable() {
            return this.Table;
        }

        /// <summary>
        /// Deleta lançamentos importados pelo arquivo CMDF. Deleta tb qq lancto com origem do tipo proventos.
        /// Somente para clientes com dataDia menor que dataVencimento, 
        /// ou igual à dataVencimento, status diferente de fechado e que esteja ativo.
        /// </summary>
        /// <param name="dataVencimento"></param>
        public void DeletaLiquidacaoCMDF(DateTime dataVencimento) 
        {
            this.QueryReset();
            this.Query
                 .Select(this.Query.IdLiquidacao, this.Query.IdCliente)
                 .Where(this.Query.Fonte == FonteLancamentoLiquidacao.ArquivoCMDF,
                        this.Query.DataVencimento.Equal(dataVencimento));
            this.Query.Load();

            LiquidacaoCollection liquidacaoCollection2 = new LiquidacaoCollection();
            liquidacaoCollection2.Query.Select(liquidacaoCollection2.Query.IdLiquidacao, liquidacaoCollection2.Query.IdCliente);
            liquidacaoCollection2.Query.Where(liquidacaoCollection2.Query.Fonte.NotEqual((byte)FonteLancamentoLiquidacao.ArquivoCMDF),
                                              liquidacaoCollection2.Query.DataVencimento.Equal(dataVencimento),
                                              liquidacaoCollection2.Query.Origem.In((byte)OrigemLancamentoLiquidacao.Bolsa.Dividendo,
                                                                                    (byte)OrigemLancamentoLiquidacao.Bolsa.JurosCapital,
                                                                                    (byte)OrigemLancamentoLiquidacao.Bolsa.Rendimento,
                                                                                    (byte)OrigemLancamentoLiquidacao.Bolsa.RestituicaoCapital));
            liquidacaoCollection2.Query.Load();

            this.Combine(liquidacaoCollection2);


            LiquidacaoCollection liquidacaoCollectionDeletar = new LiquidacaoCollection();
            foreach (Liquidacao liquidacao in this) {
                int idCliente = liquidacao.IdCliente.Value;
                Cliente cliente = new Cliente();
                List<esQueryItem> campos = new List<esQueryItem>();
                campos.Add(cliente.Query.DataDia);
                campos.Add(cliente.Query.Status);
                campos.Add(cliente.Query.StatusAtivo);
                cliente.LoadByPrimaryKey(campos, idCliente);

                DateTime dataDia = cliente.DataDia.Value;
                byte status = cliente.Status.Value;
                byte statusAtivo = cliente.StatusAtivo.Value;

                if ((DateTime.Compare(dataVencimento, dataDia) > 0) ||
                     (DateTime.Compare(dataVencimento, dataDia) == 0 &&
                      status != (byte)StatusCliente.Divulgado &&
                      statusAtivo == (byte)StatusAtivoCliente.Ativo))
                {
                    liquidacaoCollectionDeletar.AttachEntity(liquidacao);
                }
            }

            liquidacaoCollectionDeletar.MarkAllAsDeleted();
            liquidacaoCollectionDeletar.Save();
        }

        /// <summary>
        /// Deleta Liquidação por cliente, por OrigemLancamentoLiquidacao e fonteLancamentoLiquidacao da seguinte forma:
        /// Origem.In(origens.ToString()
        /// Fonte.In(fontes.ToString(), 
        /// </summary>
        /// <param name="idCliente"></param>     
        /// <param name="origemLancamentoLiquidacao">Deve ser do tipo do enum OrigemLancamentoLiquidacao.</param>
        /// <param name="fonteLancamentoLiquidacao">Deve ser do tipo do enum FonteLancamentoLiquidacao.</param>
        /// <returns></returns>
        public void DeletaLiquidacao(int idCliente,
                                     List<int> origemLancamentoLiquidacao,
                                     List<int> fonteLancamentoLiquidacao)
        {
            this.DeletaLiquidacao(idCliente, origemLancamentoLiquidacao, fonteLancamentoLiquidacao, null);
        }

        /// <summary>
        /// Deleta Liquidação por cliente, por OrigemLancamentoLiquidacao e fonteLancamentoLiquidacao da seguinte forma:
        /// Origem.In(origens.ToString()
        /// Fonte.In(fontes.ToString(), 
        /// </summary>
        /// <param name="idCliente"></param>     
        /// <param name="origemLancamentoLiquidacao">Deve ser do tipo do enum OrigemLancamentoLiquidacao.</param>
        /// <param name="fonteLancamentoLiquidacao">Deve ser do tipo do enum FonteLancamentoLiquidacao.</param>
        /// <param name="dataLancamento"></param>
        /// <returns></returns>
        public void DeletaLiquidacao(int idCliente,
                                     List<int> origemLancamentoLiquidacao,
                                     List<int> fonteLancamentoLiquidacao,
                                     DateTime? dataLancamento)
        {
            // Transforma no formato origem1','origem2','origem3
            // A primeira e a ultima aspas é colocada automaticamente
            StringBuilder origens = new StringBuilder();
            for (int i = 0; i < origemLancamentoLiquidacao.Count; i++)
            {
                string origem = Convert.ToString(origemLancamentoLiquidacao[i]);
                if (String.IsNullOrEmpty(origens.ToString()))
                {
                    origens.Append(origem);
                }
                else
                {
                    origens.Append("','" + origem);
                }
            }

            // Transforma no formato fonte1','fonte2','fonte3
            // A primeira e a ultima aspas é colocada automaticamente
            StringBuilder fontes = new StringBuilder();
            for (int i = 0; i < fonteLancamentoLiquidacao.Count; i++)
            {
                string fonte = Convert.ToString(fonteLancamentoLiquidacao[i]);
                if (String.IsNullOrEmpty(fontes.ToString()))
                {
                    fontes.Append(fonte);
                }
                else
                {
                    fontes.Append("','" + fonte);
                }
            }

            this.QueryReset();
            this.Query
                 .Select(this.Query.IdLiquidacao)
                 .Where(this.Query.IdCliente == idCliente,
                        this.Query.Origem.In(origens.ToString()),
                        this.Query.Fonte.In(fontes.ToString()));

            if (dataLancamento.HasValue)
                this.Query.Where(this.Query.DataLancamento.Equal(dataLancamento.Value));

            this.Query.Load();

            this.MarkAllAsDeleted();
            this.Save();
        }

        /// <summary>
        /// Deleta Liquidação por cliente e por OrigemLancamentoLiquidacao.
        /// Filtra apenas lançamentos internos e importados do Sinacor.
        /// </summary>
        /// <param name="idCliente"></param>     
        /// <param name="origemLancamentoLiquidacao">Deve ser do tipo do enum OrigemLancamentoLiquidacao.</param>
        /// <returns></returns>
        public void DeletaLiquidacao(int idCliente, List<int> origemLancamentoLiquidacao) 
        {
            // Transforma no formato origem1','origem2','origem3
            // A primeira e a ultima aspas é colocada automaticamente
            StringBuilder origens = new StringBuilder();
            for (int i = 0; i < origemLancamentoLiquidacao.Count; i++) {
                string origem = Convert.ToString(origemLancamentoLiquidacao[i]);
                if (String.IsNullOrEmpty(origens.ToString())) {
                    origens.Append(origem);
                }
                else {
                    origens.Append("','" + origem);
                }
            }

            this.QueryReset();
            this.Query
                 .Select(this.Query.IdLiquidacao)
                 .Where(this.Query.IdCliente == idCliente,
                        this.Query.Origem.In(origens.ToString()),
                        this.Query.Fonte.In((byte)FonteLancamentoLiquidacao.Interno, (byte)FonteLancamentoLiquidacao.Sinacor));
            this.Query.Load();

            this.MarkAllAsDeleted();
            this.Save();
        }

        /// <summary>
        /// Deleta Liquidação por cliente.
        /// DataVencimento.GreaterThanOrEqual(dataVencimento).
        /// Elimina da query lançamentos manuais/CMDF com data de lançamento posterior à data passada!
        /// </summary>
        /// <param name="idCliente"></param>     
        /// <param name="data"></param>
        /// <returns></returns>
        public void DeletaLiquidacao(int idCliente, DateTime data) 
        {
            LiquidacaoQuery liquidacaoQuerySub = new LiquidacaoQuery();

            LiquidacaoQuery liquidacaoQuery = new LiquidacaoQuery();
            //
            liquidacaoQuery.Select(liquidacaoQuery.IdLiquidacao);
            //            
            liquidacaoQuery.From
                (
                    liquidacaoQuerySub.Select(liquidacaoQuerySub.IdLiquidacao,
                                           liquidacaoQuerySub.DataLancamento,
                                           liquidacaoQuerySub.DataVencimento,
                                           liquidacaoQuerySub.Fonte)
                                    .Where(liquidacaoQuerySub.IdCliente == idCliente &
                                           liquidacaoQuerySub.DataVencimento >= data)
                ).As("A");
            //Elimina da query lançamentos manuais e CMDF com data de lançamento posterior à data passada!
            liquidacaoQuery.Where(liquidacaoQuery.Fonte.NotIn((byte)FonteLancamentoLiquidacao.Manual, 
                                                              (byte)FonteLancamentoLiquidacao.ArquivoCMDF) |
                                  liquidacaoQuery.DataLancamento <= data);

            this.QueryReset();
            this.Load(liquidacaoQuery);
            //
            this.MarkAllAsDeleted();
            this.Save();
        }

        /// <summary>
        /// Deleta Liquidação por cliente.
        /// Fonte.Equal((byte)FonteLancamentoLiquidacao.Interno),
        /// DataLancamento.GreaterThanOrEqual(dataLancamento)
        /// </summary>
        /// <param name="idCliente"></param>     
        /// <param name="dataVencimento"></param>
        /// <returns></returns>
        public void DeletaLiquidacaoInterno(int idCliente, DateTime dataVencimento) 
        {
            this.QueryReset();
            this.Query
                 .Select(this.Query.IdLiquidacao)
                 .Where(this.Query.IdCliente == idCliente,
                        this.Query.Fonte.Equal((byte)FonteLancamentoLiquidacao.Interno),
                        this.Query.DataVencimento.GreaterThanOrEqual(dataVencimento));

            this.Query.Load();

            this.MarkAllAsDeleted();
            this.Save();
        }

        /// <summary>
        /// Deleta Liquidacao com Fonte=Interno, (Origem=Dividendo ou JurosCapital ou Rendimento e afins) e Vcto >= data.
        /// Deleta Liquidacao com Fonte=Manual, (Origem=Dividendo ou JurosCapital ou Rendimento e afins) e Lancamento = data.
        /// </summary>
        /// <param name="idCliente"></param>     
        /// <param name="dataVencimento"></param>
        /// <returns></returns>
        public void DeletaLiquidacaoProventos(int idCliente, DateTime data)
        {
            //Internos (oriundos do CDIX)
            this.QueryReset();
            this.Query
                 .Select(this.Query.IdLiquidacao)
                 .Where(this.Query.IdCliente == idCliente,
                        this.Query.Fonte.Equal((byte)FonteLancamentoLiquidacao.Interno),
                        this.Query.Origem.In(OrigemLancamentoLiquidacao.Bolsa.Dividendo,
                                             OrigemLancamentoLiquidacao.Bolsa.DividendoDistribuicao,
                                             OrigemLancamentoLiquidacao.Bolsa.JurosCapital,
                                             OrigemLancamentoLiquidacao.Bolsa.Rendimento,
                                             OrigemLancamentoLiquidacao.Bolsa.RestituicaoCapital,
                                             OrigemLancamentoLiquidacao.Bolsa.CreditoFracoesAcoes,
                                             OrigemLancamentoLiquidacao.Bolsa.OutrosProventos,
                                             OrigemLancamentoLiquidacao.Bolsa.Amortizacao,
                                             OrigemLancamentoLiquidacao.Bolsa.IRProventos),
                        this.Query.DataVencimento.GreaterThanOrEqual(data));

            this.Query.Load();

            this.MarkAllAsDeleted();
            this.Save();

            //Manuais (oriundos do PROD e/ou lancados na mão via ProventoBolsa)
            this.QueryReset();
            this.Query
                 .Select(this.Query.IdLiquidacao)
                 .Where(this.Query.IdCliente == idCliente,
                        this.Query.Fonte.Equal((byte)FonteLancamentoLiquidacao.Manual),
                        this.Query.Origem.In(OrigemLancamentoLiquidacao.Bolsa.Dividendo,
                                             OrigemLancamentoLiquidacao.Bolsa.DividendoDistribuicao,
                                             OrigemLancamentoLiquidacao.Bolsa.JurosCapital,
                                             OrigemLancamentoLiquidacao.Bolsa.Rendimento,
                                             OrigemLancamentoLiquidacao.Bolsa.RestituicaoCapital,
                                             OrigemLancamentoLiquidacao.Bolsa.CreditoFracoesAcoes,
                                             OrigemLancamentoLiquidacao.Bolsa.OutrosProventos,
                                             OrigemLancamentoLiquidacao.Bolsa.Amortizacao,
                                             OrigemLancamentoLiquidacao.Bolsa.IRProventos),
                        this.Query.DataLancamento.Equal(data));

            this.Query.Load();

            this.MarkAllAsDeleted();
            this.Save();
        }

        /// <summary>
        /// Carrega o objeto LiquidacaoCollection com todos os campos de Liquidacao.
        /// Filtro para DataVencimento.GreaterThanOrEqual(dataVencimento).
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="dataVencimento"></param>
        public void BuscaLiquidacaoCompleta(int idCliente, DateTime dataVencimento) 
        {
            this.QueryReset();
            this.Query.Where(this.Query.IdCliente == idCliente,
                             this.Query.DataVencimento.GreaterThanOrEqual(dataVencimento));
            this.Query.Load();
        }

        /// <summary>
        /// Carrega o objeto LiquidacaoCollection com todos os campos de Liquidacao.
        /// Filtro para DataVencimento.GreaterThanOrEqual(dataVencimento),
        ///             DataLancamento.LessThanOrEqual(dataLancamento).
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="dataLancamento"></param>
        /// <param name="dataVencimento"></param>
        public void BuscaLiquidacaoCompleta(int idCliente, DateTime dataLancamento, DateTime dataVencimento) 
        {
            this.QueryReset();
            this.Query.Where(this.Query.IdCliente == idCliente,
                             this.Query.DataVencimento.GreaterThanOrEqual(dataVencimento),
                             this.Query.DataLancamento.LessThanOrEqual(dataLancamento));
            this.Query.Load();
        }

        /// <summary>
        /// Carrega o objeto LiquidacaoCollection com todos os campos de Liquidacao.
        /// Filtro para DataVencimento.GreaterThan(dataVencimento),
        ///             DataLancamento.LessThanOrEqual(dataLancamento).
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="dataLancamento"></param>
        /// <param name="dataVencimento"></param>
        public void BuscaLiquidacaoVencimentoMaior(int idCliente, DateTime dataLancamento, DateTime dataVencimento)
        {
            this.QueryReset();
            this.Query.Where(this.Query.IdCliente == idCliente,
                             this.Query.DataVencimento.GreaterThan(dataVencimento),
                             this.Query.DataLancamento.LessThanOrEqual(dataLancamento));
            this.Query.Load();
        }

        /// <summary>
        /// Método de inserção em Liquidacao a partir de LiquidacaoAbertura.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="dataHistorico"></param>
        public void InsereLiquidacaoAbertura(int idCliente, DateTime dataHistorico) {
            //            
            this.QueryReset();
            //
            LiquidacaoAberturaCollection liquidacaoAberturaCollection = new LiquidacaoAberturaCollection();
            liquidacaoAberturaCollection.BuscaLiquidacaoAberturaCompleta(idCliente, dataHistorico);
            
            for (int i = 0; i < liquidacaoAberturaCollection.Count; i++) {
                LiquidacaoAbertura liquidacaoAbertura = liquidacaoAberturaCollection[i];

                #region Copia de LiquidacaoAbertura para Liquidacao
                // Copia de LiquidacaoAbertura para Liquidacao                  
                
                esParameters esParams = new esParameters();
                esParams.Add("IdLiquidacao", liquidacaoAbertura.IdLiquidacao.Value);
                esParams.Add("IdCliente", liquidacaoAbertura.IdCliente.Value);
                esParams.Add("DataLancamento", liquidacaoAbertura.DataLancamento.Value);
                esParams.Add("DataVencimento", liquidacaoAbertura.DataVencimento.Value);
                esParams.Add("Descricao", liquidacaoAbertura.Descricao);
                esParams.Add("Valor", liquidacaoAbertura.Valor.Value);
                esParams.Add("Situacao", liquidacaoAbertura.Situacao.Value);
                esParams.Add("Origem", liquidacaoAbertura.Origem.Value);
                esParams.Add("Fonte", liquidacaoAbertura.Fonte.Value);
                                
                // Parametros que podem ser null
                if (!liquidacaoAbertura.IdAgente.HasValue) {
                    esParams.Add("IdAgente", DBNull.Value);
                }
                else {
                    esParams.Add("IdAgente", liquidacaoAbertura.IdAgente.Value);
                }

                // Parametros que podem ser null
                if (!liquidacaoAbertura.IdentificadorOrigem.HasValue) {
                    esParams.Add("IdentificadorOrigem", DBNull.Value);
                }
                else {
                    esParams.Add("IdentificadorOrigem", liquidacaoAbertura.IdentificadorOrigem.Value);
                }

                if (!liquidacaoAbertura.IdEvento.HasValue)
                {
                    esParams.Add("IdEvento", DBNull.Value);
                }
                else
                {
                    esParams.Add("IdEvento", liquidacaoAbertura.IdEvento.Value);
                }

                if (!liquidacaoAbertura.IdEventoVencimento.HasValue)
                {
                    esParams.Add("IdEventoVencimento", DBNull.Value);
                }
                else
                {
                    esParams.Add("IdEventoVencimento", liquidacaoAbertura.IdEventoVencimento.Value);
                }

                if (string.IsNullOrEmpty(liquidacaoAbertura.CodigoAtivo))
                {
                    esParams.Add("CodigoAtivo", DBNull.Value);
                }
                else
                {
                    esParams.Add("CodigoAtivo", liquidacaoAbertura.CodigoAtivo);
                }

                esParams.Add("IdConta", liquidacaoAbertura.IdConta.Value);
                //
                StringBuilder sqlBuilder = new StringBuilder();
                sqlBuilder.Append("SET IDENTITY_INSERT Liquidacao ON ");
                //
                sqlBuilder.Append("INSERT INTO Liquidacao (");
                sqlBuilder.Append(LiquidacaoMetadata.ColumnNames.IdLiquidacao);
                sqlBuilder.Append("," + LiquidacaoMetadata.ColumnNames.IdCliente);
                sqlBuilder.Append("," + LiquidacaoMetadata.ColumnNames.DataLancamento);
                sqlBuilder.Append("," + LiquidacaoMetadata.ColumnNames.DataVencimento);
                sqlBuilder.Append("," + LiquidacaoMetadata.ColumnNames.Descricao);
                sqlBuilder.Append("," + LiquidacaoMetadata.ColumnNames.Valor);
                sqlBuilder.Append("," + LiquidacaoMetadata.ColumnNames.Situacao);
                sqlBuilder.Append("," + LiquidacaoMetadata.ColumnNames.Origem);
                sqlBuilder.Append("," + LiquidacaoMetadata.ColumnNames.Fonte);
                sqlBuilder.Append("," + LiquidacaoMetadata.ColumnNames.IdAgente);
                sqlBuilder.Append("," + LiquidacaoMetadata.ColumnNames.IdentificadorOrigem);
                sqlBuilder.Append("," + LiquidacaoMetadata.ColumnNames.IdEvento);
                sqlBuilder.Append("," + LiquidacaoMetadata.ColumnNames.IdEventoVencimento);
                sqlBuilder.Append("," + LiquidacaoMetadata.ColumnNames.IdConta);
                sqlBuilder.Append("," + LiquidacaoMetadata.ColumnNames.CodigoAtivo);
                //              
                sqlBuilder.Append(") VALUES ( ");
                sqlBuilder.Append("@" + LiquidacaoMetadata.ColumnNames.IdLiquidacao);
                sqlBuilder.Append(",");
                sqlBuilder.Append("@" + LiquidacaoMetadata.ColumnNames.IdCliente);
                sqlBuilder.Append(",");
                sqlBuilder.Append("@" + LiquidacaoMetadata.ColumnNames.DataLancamento);
                sqlBuilder.Append(",");
                sqlBuilder.Append("@" + LiquidacaoMetadata.ColumnNames.DataVencimento);
                sqlBuilder.Append(",");
                sqlBuilder.Append("@" + LiquidacaoMetadata.ColumnNames.Descricao);
                sqlBuilder.Append(",");
                sqlBuilder.Append("@" + LiquidacaoMetadata.ColumnNames.Valor);
                sqlBuilder.Append(",");
                sqlBuilder.Append("@" + LiquidacaoMetadata.ColumnNames.Situacao);
                sqlBuilder.Append(",");
                sqlBuilder.Append("@" + LiquidacaoMetadata.ColumnNames.Origem);
                sqlBuilder.Append(",");
                sqlBuilder.Append("@" + LiquidacaoMetadata.ColumnNames.Fonte);
                sqlBuilder.Append(",");
                sqlBuilder.Append("@" + LiquidacaoMetadata.ColumnNames.IdAgente);
                sqlBuilder.Append(",");
                sqlBuilder.Append("@" + LiquidacaoMetadata.ColumnNames.IdentificadorOrigem);
                sqlBuilder.Append(",");
                sqlBuilder.Append("@" + LiquidacaoMetadata.ColumnNames.IdEvento);
                sqlBuilder.Append(",");
                sqlBuilder.Append("@" + LiquidacaoMetadata.ColumnNames.IdEventoVencimento);
                sqlBuilder.Append(",");
                sqlBuilder.Append("@" + LiquidacaoMetadata.ColumnNames.IdConta);
                sqlBuilder.Append(",");
                sqlBuilder.Append("@" + LiquidacaoMetadata.ColumnNames.CodigoAtivo);
                sqlBuilder.Append(")");
                sqlBuilder.Append("SET IDENTITY_INSERT Liquidacao OFF ");
                esUtility u = new esUtility();
                u.ExecuteNonQuery(esQueryType.Text, sqlBuilder.ToString(), esParams);
                #endregion
            }
        }

        /// <summary>
        /// Somente para lancamentos feitos na propria data do historico, 
        /// para complementar a insercao dos lactos vindos de LiquidacaoAbertura.
        /// Filtra por: 
        /// liquidacaoHistoricoCollection.Query.IdCliente.Equal(idCliente),
        /// liquidacaoHistoricoCollection.Query.DataHistorico.Equal(dataHistorico),
        /// liquidacaoHistoricoCollection.Query.Fonte.NotEqual((dataHistorico)(byte)FonteLancamentoLiquidacao.Interno)
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="dataHistorico"></param>
        public void InsereLiquidacaoHistoricoNaoInterno(int idCliente, DateTime dataHistorico) {
            //
            this.QueryReset();
            LiquidacaoHistoricoCollection liquidacaoHistoricoCollection = new LiquidacaoHistoricoCollection();
            liquidacaoHistoricoCollection.Query.Where(liquidacaoHistoricoCollection.Query.IdCliente.Equal(idCliente),
                                                      liquidacaoHistoricoCollection.Query.DataHistorico.Equal(dataHistorico),
                                                      liquidacaoHistoricoCollection.Query.Fonte.NotEqual((byte)FonteLancamentoLiquidacao.Interno),
                                                      liquidacaoHistoricoCollection.Query.DataLancamento.GreaterThan(dataHistorico));
            liquidacaoHistoricoCollection.Query.Load();

            for (int i = 0; i < liquidacaoHistoricoCollection.Count; i++) {
                LiquidacaoHistorico liquidacaoHistorico = liquidacaoHistoricoCollection[i];

                #region Copia de LiquidacaoHistorico para Liquidacao
                // Copia de LiquidacaoHistorico para Liquidacao                  
                Liquidacao liquidacao = new Liquidacao();

                esParameters esParams = new esParameters();
                esParams.Add("IdLiquidacao", liquidacaoHistorico.IdLiquidacao.Value);
                esParams.Add("IdCliente", liquidacaoHistorico.IdCliente.Value);
                esParams.Add("DataLancamento", liquidacaoHistorico.DataLancamento.Value);
                esParams.Add("DataVencimento", liquidacaoHistorico.DataVencimento.Value);
                esParams.Add("Descricao", liquidacaoHistorico.Descricao);
                esParams.Add("Valor", liquidacaoHistorico.Valor.Value);
                esParams.Add("Situacao", liquidacaoHistorico.Situacao.Value);
                esParams.Add("Origem", liquidacaoHistorico.Origem.Value);
                esParams.Add("Fonte", liquidacaoHistorico.Fonte.Value);

                // Parametros que podem ser null
                if (!liquidacaoHistorico.IdAgente.HasValue) {
                    esParams.Add("IdAgente", DBNull.Value);
                }
                else {
                    esParams.Add("IdAgente", liquidacaoHistorico.IdAgente.Value);
                }

                // Parametros que podem ser null
                if (!liquidacaoHistorico.IdentificadorOrigem.HasValue) {
                    esParams.Add("IdentificadorOrigem", DBNull.Value);
                }
                else {
                    esParams.Add("IdentificadorOrigem", liquidacaoHistorico.IdentificadorOrigem.Value);
                }

                if (!liquidacaoHistorico.IdEvento.HasValue)
                {
                    esParams.Add("IdEvento", DBNull.Value);
                }
                else
                {
                    esParams.Add("IdEvento", liquidacaoHistorico.IdEvento.Value);
                }

                if (!liquidacaoHistorico.IdEventoVencimento.HasValue)
                {
                    esParams.Add("IdEventoVencimento", DBNull.Value);
                }
                else
                {
                    esParams.Add("IdEventoVencimento", liquidacaoHistorico.IdEventoVencimento.Value);
                }

                if (string.IsNullOrEmpty(liquidacaoHistorico.CodigoAtivo))
                {
                    esParams.Add("CodigoAtivo", DBNull.Value);
                }
                else
                {
                    esParams.Add("CodigoAtivo", liquidacaoHistorico.CodigoAtivo);
                }

                esParams.Add("IdConta", liquidacaoHistorico.IdConta.Value);
                //
                StringBuilder sqlBuilder = new StringBuilder();
                sqlBuilder.Append("SET IDENTITY_INSERT Liquidacao ON ");
                //
                sqlBuilder.Append("INSERT INTO Liquidacao (");
                sqlBuilder.Append(LiquidacaoMetadata.ColumnNames.IdLiquidacao);
                sqlBuilder.Append("," + LiquidacaoMetadata.ColumnNames.IdCliente);
                sqlBuilder.Append("," + LiquidacaoMetadata.ColumnNames.DataLancamento);
                sqlBuilder.Append("," + LiquidacaoMetadata.ColumnNames.DataVencimento);
                sqlBuilder.Append("," + LiquidacaoMetadata.ColumnNames.Descricao);
                sqlBuilder.Append("," + LiquidacaoMetadata.ColumnNames.Valor);
                sqlBuilder.Append("," + LiquidacaoMetadata.ColumnNames.Situacao);
                sqlBuilder.Append("," + LiquidacaoMetadata.ColumnNames.Origem);
                sqlBuilder.Append("," + LiquidacaoMetadata.ColumnNames.Fonte);
                sqlBuilder.Append("," + LiquidacaoMetadata.ColumnNames.IdAgente);
                sqlBuilder.Append("," + LiquidacaoMetadata.ColumnNames.IdentificadorOrigem);
                sqlBuilder.Append("," + LiquidacaoMetadata.ColumnNames.IdEvento);
                sqlBuilder.Append("," + LiquidacaoMetadata.ColumnNames.IdEventoVencimento);
                sqlBuilder.Append("," + LiquidacaoMetadata.ColumnNames.IdConta);
                sqlBuilder.Append("," + LiquidacaoMetadata.ColumnNames.CodigoAtivo);
                //              
                sqlBuilder.Append(") VALUES ( ");
                sqlBuilder.Append("@" + LiquidacaoMetadata.ColumnNames.IdLiquidacao);
                sqlBuilder.Append(",");
                sqlBuilder.Append("@" + LiquidacaoMetadata.ColumnNames.IdCliente);
                sqlBuilder.Append(",");
                sqlBuilder.Append("@" + LiquidacaoMetadata.ColumnNames.DataLancamento);
                sqlBuilder.Append(",");
                sqlBuilder.Append("@" + LiquidacaoMetadata.ColumnNames.DataVencimento);
                sqlBuilder.Append(",");
                sqlBuilder.Append("@" + LiquidacaoMetadata.ColumnNames.Descricao);
                sqlBuilder.Append(",");
                sqlBuilder.Append("@" + LiquidacaoMetadata.ColumnNames.Valor);
                sqlBuilder.Append(",");
                sqlBuilder.Append("@" + LiquidacaoMetadata.ColumnNames.Situacao);
                sqlBuilder.Append(",");
                sqlBuilder.Append("@" + LiquidacaoMetadata.ColumnNames.Origem);
                sqlBuilder.Append(",");
                sqlBuilder.Append("@" + LiquidacaoMetadata.ColumnNames.Fonte);
                sqlBuilder.Append(",");
                sqlBuilder.Append("@" + LiquidacaoMetadata.ColumnNames.IdAgente);
                sqlBuilder.Append(",");
                sqlBuilder.Append("@" + LiquidacaoMetadata.ColumnNames.IdentificadorOrigem);
                sqlBuilder.Append(",");
                sqlBuilder.Append("@" + LiquidacaoMetadata.ColumnNames.IdEvento);
                sqlBuilder.Append(",");
                sqlBuilder.Append("@" + LiquidacaoMetadata.ColumnNames.IdEventoVencimento);
                sqlBuilder.Append(",");
                sqlBuilder.Append("@" + LiquidacaoMetadata.ColumnNames.IdConta);
                sqlBuilder.Append(",");
                sqlBuilder.Append("@" + LiquidacaoMetadata.ColumnNames.CodigoAtivo);
                sqlBuilder.Append(")");
                sqlBuilder.Append("SET IDENTITY_INSERT Liquidacao OFF ");
                esUtility u = new esUtility();
                u.ExecuteNonQuery(esQueryType.Text, sqlBuilder.ToString(), esParams);
                #endregion
            }
        }

        /// <summary>
        /// Método de inserção em Liquidacao a partir de LiquidacaoAbertura.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="dataHistorico"></param>
        public void InsereLiquidacaoAberturaSomenteInternos(int idCliente, DateTime dataHistorico) {
            //
            this.QueryReset();
            LiquidacaoAberturaCollection liquidacaoAberturaCollection = new LiquidacaoAberturaCollection();
            liquidacaoAberturaCollection.BuscaLiquidacaoAberturaCompletaSomenteInternos(idCliente, dataHistorico);
            //
            for (int i = 0; i < liquidacaoAberturaCollection.Count; i++) {
                LiquidacaoAbertura liquidacaoAbertura = liquidacaoAberturaCollection[i];

                #region Copia de LiquidacaoAbertura para Liquidacao
                // Copia de LiquidacaoAbertura para Liquidacao                  
                Liquidacao liquidacao = new Liquidacao();

                esParameters esParams = new esParameters();
                esParams.Add("IdLiquidacao", liquidacaoAbertura.IdLiquidacao.Value);
                esParams.Add("IdCliente", liquidacaoAbertura.IdCliente.Value);
                esParams.Add("DataLancamento", liquidacaoAbertura.DataLancamento.Value);
                esParams.Add("DataVencimento", liquidacaoAbertura.DataVencimento.Value);
                esParams.Add("Descricao", liquidacaoAbertura.Descricao);
                esParams.Add("Valor", liquidacaoAbertura.Valor.Value);
                esParams.Add("Situacao", liquidacaoAbertura.Situacao.Value);
                esParams.Add("Origem", liquidacaoAbertura.Origem.Value);
                esParams.Add("Fonte", liquidacaoAbertura.Fonte.Value);

                // Parametros que podem ser null
                if (!liquidacaoAbertura.IdAgente.HasValue) {
                    esParams.Add("IdAgente", DBNull.Value);
                }
                else {
                    esParams.Add("IdAgente", liquidacaoAbertura.IdAgente.Value);
                }

                // Parametros que podem ser null
                if (!liquidacaoAbertura.IdentificadorOrigem.HasValue) {
                    esParams.Add("IdentificadorOrigem", DBNull.Value);
                }
                else {
                    esParams.Add("IdentificadorOrigem", liquidacaoAbertura.IdentificadorOrigem.Value);
                }

                if (!liquidacaoAbertura.IdEvento.HasValue)
                {
                    esParams.Add("IdEvento", DBNull.Value);
                }
                else
                {
                    esParams.Add("IdEvento", liquidacaoAbertura.IdEvento.Value);
                }

                if (!liquidacaoAbertura.IdEventoVencimento.HasValue)
                {
                    esParams.Add("IdEventoVencimento", DBNull.Value);
                }
                else
                {
                    esParams.Add("IdEventoVencimento", liquidacaoAbertura.IdEventoVencimento.Value);
                }


                if (string.IsNullOrEmpty(liquidacaoAbertura.CodigoAtivo))
                {
                    esParams.Add("CodigoAtivo", DBNull.Value);
                }
                else
                {
                    esParams.Add("CodigoAtivo", liquidacaoAbertura.CodigoAtivo);
                }

                esParams.Add("IdConta", liquidacaoAbertura.IdConta.Value);
                //
                StringBuilder sqlBuilder = new StringBuilder();
                sqlBuilder.Append("SET IDENTITY_INSERT Liquidacao ON ");
                //
                sqlBuilder.Append("INSERT INTO Liquidacao (");
                sqlBuilder.Append(LiquidacaoMetadata.ColumnNames.IdLiquidacao);
                sqlBuilder.Append("," + LiquidacaoMetadata.ColumnNames.IdCliente);
                sqlBuilder.Append("," + LiquidacaoMetadata.ColumnNames.DataLancamento);
                sqlBuilder.Append("," + LiquidacaoMetadata.ColumnNames.DataVencimento);
                sqlBuilder.Append("," + LiquidacaoMetadata.ColumnNames.Descricao);
                sqlBuilder.Append("," + LiquidacaoMetadata.ColumnNames.Valor);
                sqlBuilder.Append("," + LiquidacaoMetadata.ColumnNames.Situacao);
                sqlBuilder.Append("," + LiquidacaoMetadata.ColumnNames.Origem);
                sqlBuilder.Append("," + LiquidacaoMetadata.ColumnNames.Fonte);
                sqlBuilder.Append("," + LiquidacaoMetadata.ColumnNames.IdAgente);
                sqlBuilder.Append("," + LiquidacaoMetadata.ColumnNames.IdentificadorOrigem);
                sqlBuilder.Append("," + LiquidacaoMetadata.ColumnNames.IdEvento);
                sqlBuilder.Append("," + LiquidacaoMetadata.ColumnNames.IdEventoVencimento);
                sqlBuilder.Append("," + LiquidacaoMetadata.ColumnNames.IdConta);
                sqlBuilder.Append("," + LiquidacaoMetadata.ColumnNames.CodigoAtivo);
                //              
                sqlBuilder.Append(") VALUES ( ");
                sqlBuilder.Append("@" + LiquidacaoMetadata.ColumnNames.IdLiquidacao);
                sqlBuilder.Append(",");
                sqlBuilder.Append("@" + LiquidacaoMetadata.ColumnNames.IdCliente);
                sqlBuilder.Append(",");
                sqlBuilder.Append("@" + LiquidacaoMetadata.ColumnNames.DataLancamento);
                sqlBuilder.Append(",");
                sqlBuilder.Append("@" + LiquidacaoMetadata.ColumnNames.DataVencimento);
                sqlBuilder.Append(",");
                sqlBuilder.Append("@" + LiquidacaoMetadata.ColumnNames.Descricao);
                sqlBuilder.Append(",");
                sqlBuilder.Append("@" + LiquidacaoMetadata.ColumnNames.Valor);
                sqlBuilder.Append(",");
                sqlBuilder.Append("@" + LiquidacaoMetadata.ColumnNames.Situacao);
                sqlBuilder.Append(",");
                sqlBuilder.Append("@" + LiquidacaoMetadata.ColumnNames.Origem);
                sqlBuilder.Append(",");
                sqlBuilder.Append("@" + LiquidacaoMetadata.ColumnNames.Fonte);
                sqlBuilder.Append(",");
                sqlBuilder.Append("@" + LiquidacaoMetadata.ColumnNames.IdAgente);
                sqlBuilder.Append(",");
                sqlBuilder.Append("@" + LiquidacaoMetadata.ColumnNames.IdentificadorOrigem);
                sqlBuilder.Append(",");
                sqlBuilder.Append("@" + LiquidacaoMetadata.ColumnNames.IdEvento);
                sqlBuilder.Append(",");
                sqlBuilder.Append("@" + LiquidacaoMetadata.ColumnNames.IdEventoVencimento);
                sqlBuilder.Append(",");
                sqlBuilder.Append("@" + LiquidacaoMetadata.ColumnNames.IdConta);
                sqlBuilder.Append(",");
                sqlBuilder.Append("@" + LiquidacaoMetadata.ColumnNames.CodigoAtivo);
                sqlBuilder.Append(")");
                sqlBuilder.Append("SET IDENTITY_INSERT Liquidacao OFF ");
                esUtility u = new esUtility();
                u.ExecuteNonQuery(esQueryType.Text, sqlBuilder.ToString(), esParams);
                #endregion
            }
        }

        /// <summary>
        /// Método de inserção em Liquidacao a partir de LiquidacaoHistorico.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="dataHistorico"></param>
        public void InsereLiquidacaoHistorico(int idCliente, DateTime dataHistorico) {
            //
            this.QueryReset();
            LiquidacaoHistoricoCollection liquidacaoHistoricoCollection = new LiquidacaoHistoricoCollection();
            liquidacaoHistoricoCollection.BuscaLiquidacaoHistoricoCompleta(idCliente, dataHistorico);
            //
            for (int i = 0; i < liquidacaoHistoricoCollection.Count; i++) {
                LiquidacaoHistorico liquidacaoHistorico = liquidacaoHistoricoCollection[i];

                #region Copia de LiquidacaoHistorico para Liquidacao
                // Copia de LiquidacaoHistorico para Liquidacao                  
                Liquidacao liquidacao = new Liquidacao();

                esParameters esParams = new esParameters();
                esParams.Add("IdLiquidacao", liquidacaoHistorico.IdLiquidacao.Value);
                esParams.Add("IdCliente", liquidacaoHistorico.IdCliente.Value);
                esParams.Add("DataLancamento", liquidacaoHistorico.DataLancamento.Value);
                esParams.Add("DataVencimento", liquidacaoHistorico.DataVencimento.Value);
                esParams.Add("Descricao", liquidacaoHistorico.Descricao);
                esParams.Add("Valor", liquidacaoHistorico.Valor.Value);
                esParams.Add("Situacao", liquidacaoHistorico.Situacao.Value);
                esParams.Add("Origem", liquidacaoHistorico.Origem.Value);
                esParams.Add("Fonte", liquidacaoHistorico.Fonte.Value);

                // Parametros que podem ser null
                if (!liquidacaoHistorico.IdAgente.HasValue) {
                    esParams.Add("IdAgente", DBNull.Value);
                }
                else {
                    esParams.Add("IdAgente", liquidacaoHistorico.IdAgente.Value);
                }

                // Parametros que podem ser null
                if (!liquidacaoHistorico.IdentificadorOrigem.HasValue) {
                    esParams.Add("IdentificadorOrigem", DBNull.Value);
                }
                else {
                    esParams.Add("IdentificadorOrigem", liquidacaoHistorico.IdentificadorOrigem.Value);
                }

                if (!liquidacaoHistorico.IdEvento.HasValue)
                {
                    esParams.Add("IdEvento", DBNull.Value);
                }
                else
                {
                    esParams.Add("IdEvento", liquidacaoHistorico.IdEvento.Value);
                }

                if (!liquidacaoHistorico.IdEventoVencimento.HasValue)
                {
                    esParams.Add("IdEventoVencimento", DBNull.Value);
                }
                else
                {
                    esParams.Add("IdEventoVencimento", liquidacaoHistorico.IdEventoVencimento.Value);
                }

                if (string.IsNullOrEmpty(liquidacaoHistorico.CodigoAtivo))
                {
                    esParams.Add("CodigoAtivo", DBNull.Value);
                }
                else
                {
                    esParams.Add("CodigoAtivo", liquidacaoHistorico.CodigoAtivo);
                }

                esParams.Add("IdConta", liquidacaoHistorico.IdConta.Value);
                //
                StringBuilder sqlBuilder = new StringBuilder();
                sqlBuilder.Append("SET IDENTITY_INSERT Liquidacao ON ");
                //
                sqlBuilder.Append("INSERT INTO Liquidacao (");
                sqlBuilder.Append(LiquidacaoMetadata.ColumnNames.IdLiquidacao);
                sqlBuilder.Append("," + LiquidacaoMetadata.ColumnNames.IdCliente);
                sqlBuilder.Append("," + LiquidacaoMetadata.ColumnNames.DataLancamento);
                sqlBuilder.Append("," + LiquidacaoMetadata.ColumnNames.DataVencimento);
                sqlBuilder.Append("," + LiquidacaoMetadata.ColumnNames.Descricao);
                sqlBuilder.Append("," + LiquidacaoMetadata.ColumnNames.Valor);
                sqlBuilder.Append("," + LiquidacaoMetadata.ColumnNames.Situacao);
                sqlBuilder.Append("," + LiquidacaoMetadata.ColumnNames.Origem);
                sqlBuilder.Append("," + LiquidacaoMetadata.ColumnNames.Fonte);
                sqlBuilder.Append("," + LiquidacaoMetadata.ColumnNames.IdAgente);
                sqlBuilder.Append("," + LiquidacaoMetadata.ColumnNames.IdentificadorOrigem);
                sqlBuilder.Append("," + LiquidacaoMetadata.ColumnNames.IdEvento);
                sqlBuilder.Append("," + LiquidacaoMetadata.ColumnNames.IdEventoVencimento);
                sqlBuilder.Append("," + LiquidacaoMetadata.ColumnNames.IdConta);
                sqlBuilder.Append("," + LiquidacaoMetadata.ColumnNames.CodigoAtivo);
                //              
                sqlBuilder.Append(") VALUES ( ");
                sqlBuilder.Append("@" + LiquidacaoMetadata.ColumnNames.IdLiquidacao);
                sqlBuilder.Append(",");
                sqlBuilder.Append("@" + LiquidacaoMetadata.ColumnNames.IdCliente);
                sqlBuilder.Append(",");
                sqlBuilder.Append("@" + LiquidacaoMetadata.ColumnNames.DataLancamento);
                sqlBuilder.Append(",");
                sqlBuilder.Append("@" + LiquidacaoMetadata.ColumnNames.DataVencimento);
                sqlBuilder.Append(",");
                sqlBuilder.Append("@" + LiquidacaoMetadata.ColumnNames.Descricao);
                sqlBuilder.Append(",");
                sqlBuilder.Append("@" + LiquidacaoMetadata.ColumnNames.Valor);
                sqlBuilder.Append(",");
                sqlBuilder.Append("@" + LiquidacaoMetadata.ColumnNames.Situacao);
                sqlBuilder.Append(",");
                sqlBuilder.Append("@" + LiquidacaoMetadata.ColumnNames.Origem);
                sqlBuilder.Append(",");
                sqlBuilder.Append("@" + LiquidacaoMetadata.ColumnNames.Fonte);
                sqlBuilder.Append(",");
                sqlBuilder.Append("@" + LiquidacaoMetadata.ColumnNames.IdAgente);
                sqlBuilder.Append(",");
                sqlBuilder.Append("@" + LiquidacaoMetadata.ColumnNames.IdentificadorOrigem);
                sqlBuilder.Append(",");
                sqlBuilder.Append("@" + LiquidacaoMetadata.ColumnNames.IdEvento);
                sqlBuilder.Append(",");
                sqlBuilder.Append("@" + LiquidacaoMetadata.ColumnNames.IdEventoVencimento);
                sqlBuilder.Append(",");
                sqlBuilder.Append("@" + LiquidacaoMetadata.ColumnNames.IdConta);
                sqlBuilder.Append(",");
                sqlBuilder.Append("@" + LiquidacaoMetadata.ColumnNames.CodigoAtivo);
                sqlBuilder.Append(")");
                sqlBuilder.Append("SET IDENTITY_INSERT Liquidacao OFF ");
                esUtility u = new esUtility();
                u.ExecuteNonQuery(esQueryType.Text, sqlBuilder.ToString(), esParams);
                #endregion
            }
        }

        /// <summary>
        /// Carrega o objeto LiquidacaoCollection com os campos Descricao, Valor.Sum().
        /// Filtro para DataVencimento.GreaterThanOrEqual(dataReferencia),
        ///             DataLancamento.LessThanOrEqual(dataReferencia).
        /// GroupBy(Descricao).
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="dataReferencia"></param>
        public void BuscaLiquidacaoAgrupado(int idCliente, DateTime dataReferencia)
        {
            this.QueryReset();
            this.Query.Select(this.Query.Descricao, this.Query.Valor.Sum());
            this.Query.Where(this.Query.IdCliente == idCliente,
                             this.Query.DataVencimento.GreaterThanOrEqual(dataReferencia),
                             this.Query.DataLancamento.LessThanOrEqual(dataReferencia));
            this.Query.GroupBy(this.Query.Descricao);
            this.Query.Load();
        }
    }
}
