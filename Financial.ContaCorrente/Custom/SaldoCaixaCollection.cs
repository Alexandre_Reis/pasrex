using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Text;
using System.Data;

using EntitySpaces.Interfaces;
using EntitySpaces.Core;
using log4net;

namespace Financial.ContaCorrente
{
	public partial class SaldoCaixaCollection : esSaldoCaixaCollection
	{
        private static readonly ILog log = LogManager.GetLogger(typeof(SaldoCaixaCollection));

        /// <summary>
        /// Adiciona uma Coluna Nova na Collection
        /// </summary>
        /// <param name="columnName"></param>
        /// <param name="typeColumn"></param>
        public void AddColumn(string columnName, Type typeColumn) {
            if (this.Table != null && !this.Table.Columns.Contains(columnName)) {
                this.Table.Columns.Add(columnName, typeColumn);
            }
        }

        /// <summary>
        /// Carrega o objeto SaldoCaixaCollection com todos todos os campos de SaldoCaixa.
        /// </summary>
        /// <param name="idCliente"></param>  
        /// <param name="data"></param>
        /// <returns>booleano para indicar se achou registro.</returns>
        public bool BuscaSaldoCaixa(int idCliente, DateTime data)
        {
            // TODO log da funcao                              

            this.QueryReset();
            this.Query
                 .Select()
                 .Where(this.query.IdCliente == idCliente,
                        this.query.Data.Equal(data));

            bool retorno = this.Query.Load();

            #region Log Sql
            if (log.IsInfoEnabled)
            {
                string sql = this.Query.es.LastQuery;
                sql = sql.Replace("@IdCliente1", "'" + idCliente + "'");
                sql = sql.Replace("@Data2", "'" + data + "'");
                log.Info(sql);
            }
            #endregion

            #region logSaida
            if (log.IsDebugEnabled)
            {
                log.Debug("Entrada nomeMetodo: ");
            }
            #endregion

            return retorno;
        }

        /// <summary>
        /// Deleta os saldos de caixa de abertura/fechamento do cliente.
        /// Filtra por Data.GreaterThan(data).
        /// </summary>
        /// <param name="idCliente"></param>  
        /// <param name="data"></param>
        /// <returns>booleano para indicar se achou registro.</returns>
        public bool DeletaSaldoMaiorData(int idCliente, DateTime data)
        {
            // TODO log da funcao                              

            this.QueryReset();
            this.Query
                 .Select()
                 .Where(this.query.IdCliente == idCliente,
                        this.query.Data.GreaterThan(data));

            bool retorno = this.Query.Load();

            #region Log Sql
            if (log.IsInfoEnabled)
            {
                string sql = this.Query.es.LastQuery;
                sql = sql.Replace("@IdCliente1", "'" + idCliente + "'");
                sql = sql.Replace("@Data2", "'" + data + "'");
                log.Info(sql);
            }
            #endregion

            #region logSaida
            if (log.IsDebugEnabled)
            {
                log.Debug("Entrada nomeMetodo: ");
            }
            #endregion

            this.MarkAllAsDeleted();
            this.Save();

            return retorno;
        }

        /// <summary>
        /// Procura na collection SaldoCaixaCollection pelo registro idCliente, Data e IdConta
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="data"></param>
        /// <param name="idConta"></param>
        /// <returns> posicao que achou o registro ou -1 se nao encontrou
        /// </returns>
        private int FindBySaldoCaixaPrimaryKey(int idCliente, DateTime data, int idConta) {
            int retorno = 0;
            for (int i = 0; i < this.Count; i++) {
			    SaldoCaixa saldoCaixa = this[i];
                if(saldoCaixa.IdCliente == idCliente && 
                    saldoCaixa.Data == data &&
                    saldoCaixa.IdConta == idConta) {
                    retorno = i;
                    break;
                }
			}            
            return retorno;
        }
	}
}
