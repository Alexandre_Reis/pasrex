﻿using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Text;
using System.Data;

using EntitySpaces.Interfaces;
using EntitySpaces.Core;
using log4net;
using System.Configuration;
using System.IO;
using System.Reflection;
using System.Collections;
using Financial.Interfaces.Import.Bolsa.Exceptions;
using Financial.Common;
using Financial.Interfaces.Import.Bolsa;
using Financial.Investidor;
using Financial.ContaCorrente.Enums;
using Financial.Investidor.Enums;
using Financial.InvestidorCotista;
using Financial.InvestidorCotista.Enums;
using Financial.Fundo;
using Financial.Util;
using Financial.Interfaces.Import.Bolsa.Enums;
using Financial.Interfaces.Sinacor;
using Financial.Bolsa.Exceptions;
using Financial.Bolsa;
using Financial.Common.Enums;
using Financial.Fundo.Enums;
using Financial.Contabil.Enums;
using Financial.Contabil;

namespace Financial.ContaCorrente
{
    public partial class Liquidacao : esLiquidacao
    {
        /// <summary>
        /// Adiciona uma Coluna Nova na Entidade
        /// </summary>
        /// <param name="columnName"></param>
        /// <param name="typeColumn"></param>
        public void AddColumn(string columnName, Type typeColumn)
        {
            if (this.Table != null && !this.Table.Columns.Contains(columnName))
            {
                this.Table.Columns.Add(columnName, typeColumn);
            }
        }

        /// <summary>
        /// Atualiza o valor em Liquidacao, baseado no IdentificadorOrigem.
        /// O valor passado é somado ao valor atual da Liquidacao.
        /// </summary>
        /// <param name="identificadorOrigem"></param>
        /// <param name="valor"></param>
        public bool AtualizaLiquidacaoPorIdentificadorOrigem(int identificadorOrigem, decimal valor)
        {
            LiquidacaoCollection liquidacaoCollection = new LiquidacaoCollection();

            liquidacaoCollection.Query
                 .Where(liquidacaoCollection.Query.IdentificadorOrigem.Equal(identificadorOrigem));

            bool achou = liquidacaoCollection.Query.Load();

            foreach (Liquidacao liquidacao in liquidacaoCollection)
            {
                liquidacao.Valor += valor;
            }

            liquidacaoCollection.Save();

            return achou;
        }

        /// <summary>
        /// Método de inserção na Liquidacao.
        /// 
        /// throws ArgumentNullException se campos Obrigatórios da inclusão estiverem nulos
        /// liquidacao.DataLancamento ou liquidacao.DataVencimento ou liquidacao.Descricao ou
        /// liquidacao.Valor ou liquidacao.Situacao ou liquidacao.Origem ou
        /// liquidacao.Fonte ou liquidacao.IdCliente.
        /// </summary>
        /// <param name="liquidacao">objeto Liquidacao com os dados a inserir.</param>
        public void InsereLiquidacao(Liquidacao liquidacao)
        {

            #region ArgumentosNulos - throw ArgumentNullException
            if (!liquidacao.DataLancamento.HasValue ||
                !liquidacao.DataVencimento.HasValue ||
                String.IsNullOrEmpty(liquidacao.Descricao) ||
                !liquidacao.Valor.HasValue ||
                !liquidacao.Situacao.HasValue ||
                !liquidacao.Origem.HasValue ||
                !liquidacao.Fonte.HasValue ||
                !liquidacao.IdCliente.HasValue)
            {

                StringBuilder mensagem = new StringBuilder();
                mensagem.Append("Falta Preencher ")
                .Append("liquidacao.DataLancamento ou ")
                .Append("liquidacao.DataVencimento ou ")
                .Append("liquidacao.Descricao ou ")
                .Append("liquidacao.Valor ou ")
                .Append("liquidacao.Situacao ou ")
                .Append("liquidacao.Origem ou ")
                .Append("operacaoBolsa.Fonte ou ")
                .Append("operacaoBolsa.IdCliente");

                throw new ArgumentNullException(mensagem.ToString());
            }
            #endregion

            Liquidacao liquidacaoInserir = new Liquidacao();
            liquidacaoInserir.AddNew();
            //            
            // Campos Obrigatórios para a inserção
            liquidacaoInserir.DataLancamento = liquidacao.DataLancamento;
            liquidacaoInserir.DataVencimento = liquidacao.DataVencimento;
            liquidacaoInserir.Descricao = liquidacao.Descricao;
            liquidacaoInserir.Valor = liquidacao.Valor;
            liquidacaoInserir.Situacao = liquidacao.Situacao;
            liquidacaoInserir.Origem = liquidacao.Origem;
            liquidacaoInserir.Fonte = liquidacao.Fonte;
            liquidacaoInserir.IdCliente = liquidacao.IdCliente;
            liquidacaoInserir.CodigoAtivo = liquidacao.CodigoAtivo;

            // Campos não obrigatórios - se não for passado coloca valor default             
            liquidacaoInserir.IdAgente = liquidacao.IdAgente.HasValue ? liquidacao.IdAgente : null;
            liquidacaoInserir.IdConta = liquidacao.IdConta.HasValue ? liquidacao.IdConta : null;
            liquidacaoInserir.IdentificadorOrigem = liquidacao.IdentificadorOrigem.HasValue ? liquidacao.IdentificadorOrigem : null;
            liquidacaoInserir.IdEvento = liquidacao.IdEvento.HasValue ? liquidacao.IdEvento : null;
            liquidacaoInserir.IdEventoVencimento = liquidacao.IdEventoVencimento.HasValue ? liquidacao.IdEventoVencimento : null;

            liquidacaoInserir.Save();
        }

        /// <summary>
        /// Retorna o total de lancamentos a vencer em Liquidacao.
        /// Filtra por DataLancamento.LessThanOrEqual(dataLancamento), DataVencimento.GreaterThan(dataVencimento).
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="dataLancamento"></param>
        /// <param name="dataVencimento"></param>        
        public decimal RetornaLiquidacaoVencimentoMaior(int idCliente, DateTime dataLancamento, DateTime dataVencimento)
        {
            this.QueryReset();
            this.Query
              .Select(this.Query.Valor.Sum())
              .Where(this.Query.IdCliente == idCliente,
                     this.Query.DataLancamento.LessThanOrEqual(dataLancamento),
                     this.Query.DataVencimento.GreaterThan(dataVencimento));

            this.Query.Load();

            decimal valor = 0;
            if (this.es.HasData)
            {
                valor = this.Valor.HasValue ? this.Valor.Value : 0;
            }

            return valor;
        }

        /// <summary>
        /// Retorna o total de lancamentos a vencer em Liquidacao.
        /// Filtra por DataLancamento.LessThanOrEqual(data), DataVencimento.GreaterThan(data).
        /// Leva em conta IdMoeda passado e faz as devidas conversões.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="data"></param>
        /// <param name="idMoedaCliente"></param>        
        public decimal RetornaLiquidacaoVencimentoMaior(int idCliente, DateTime data, int idMoedaCliente)
        {
            LiquidacaoCollection liquidacaoCollection = new LiquidacaoCollection();
            liquidacaoCollection.Query.Select(liquidacaoCollection.Query.Valor.Sum(), liquidacaoCollection.Query.IdConta);
            liquidacaoCollection.Query.Where(liquidacaoCollection.Query.IdCliente.Equal(idCliente),
                                             liquidacaoCollection.Query.DataLancamento.LessThanOrEqual(data),
                                             liquidacaoCollection.Query.DataVencimento.GreaterThan(data));
            liquidacaoCollection.Query.GroupBy(liquidacaoCollection.Query.IdConta);
            liquidacaoCollection.Query.Load();

            decimal totalValor = 0;
            foreach (Liquidacao liquidacao in liquidacaoCollection)
            {
                int idConta = liquidacao.IdConta.Value;
                decimal valor = liquidacao.Valor.Value;

                //Trata multi-moeda
                Investidor.ContaCorrente contaCorrente = new Investidor.ContaCorrente();
                List<esQueryItem> campos = new List<esQueryItem>();
                campos.Add(contaCorrente.Query.IdMoeda);
                contaCorrente.LoadByPrimaryKey(campos, idConta);
                int idMoedaConta = contaCorrente.IdMoeda.Value;

                decimal ptax = 0;
                if (idMoedaCliente == (int)ListaMoedaFixo.Dolar)
                {
                    CotacaoIndice cotacaoIndice = new CotacaoIndice();
                    ptax = cotacaoIndice.BuscaCotacaoIndice((int)ListaIndiceFixo.PTAX_800VENDA, data);
                }

                if (idMoedaCliente != idMoedaConta)
                {
                    decimal fatorConversao = 0;
                    if (idMoedaCliente == (int)ListaMoedaFixo.Dolar && idMoedaConta == (int)ListaMoedaFixo.Real)
                    {
                        fatorConversao = 1 / ptax;
                    }
                    else
                    {
                        ConversaoMoeda conversaoMoeda = new ConversaoMoeda();
                        conversaoMoeda.Query.Where(conversaoMoeda.Query.IdMoedaDe.Equal(idMoedaCliente),
                                                   conversaoMoeda.Query.IdMoedaPara.Equal(idMoedaConta));
                        if (conversaoMoeda.Query.Load())
                        {
                            int idIndiceConversao = conversaoMoeda.IdIndice.Value;
                            CotacaoIndice cotacaoIndice = new CotacaoIndice();
                            if (conversaoMoeda.Tipo.Value == (byte)TipoConversaoMoeda.Divide)
                            {
                                fatorConversao = 1 / cotacaoIndice.BuscaCotacaoIndice(idIndiceConversao, data);
                            }
                            else
                            {
                                fatorConversao = cotacaoIndice.BuscaCotacaoIndice(idIndiceConversao, data);
                            }
                        }
                    }

                    valor = Math.Round(valor * fatorConversao, 2);
                }

                totalValor += valor;
            }

            return totalValor;
        }

        /// <summary>
        /// Retorna o total de lancamentos a vencer em Liquidacao.
        /// Filtra por DataLancamento.LessThanOrEqual(dataLancamento), DataVencimento.GreaterThan(dataVencimento).
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="dataLancamento"></param>
        /// <param name="dataVencimento"></param>        
        public decimal RetornaLiquidacaoVencimentoMaiorIgual(int idCliente, DateTime dataLancamento, DateTime dataVencimento)
        {
            this.QueryReset();
            this.Query
              .Select(this.Query.Valor.Sum())
              .Where(this.Query.IdCliente == idCliente,
                     this.Query.DataLancamento.LessThan(dataLancamento),
                     this.Query.DataVencimento.GreaterThanOrEqual(dataVencimento));

            this.Query.Load();

            decimal valor = 0;
            if (this.es.HasData)
            {
                valor = this.Valor.HasValue ? this.Valor.Value : 0;
            }

            return valor;
        }

        /// <summary>
        /// Retorna o total de lancamentos a vencer em Liquidacao.
        /// Filtra por DataLancamento.Equal(dataLancamento), DataVencimento.GreaterThanOrEqual(dataVencimento),
        /// Fonte.Equal(FonteLancamentoLiquidacao.Interno).
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="dataLancamento"></param>
        /// <param name="dataVencimento"></param>        
        public decimal RetornaLiquidacaoLancamentoNaData(int idCliente, DateTime dataLancamento, DateTime dataVencimento)
        {
            this.QueryReset();
            this.Query
              .Select(this.Query.Valor.Sum())
              .Where(this.Query.IdCliente == idCliente,
                     this.Query.DataLancamento.Equal(dataLancamento),
                     this.Query.DataVencimento.GreaterThanOrEqual(dataVencimento),
                     this.Query.Fonte.Equal(FonteLancamentoLiquidacao.Interno));

            this.Query.Load();

            decimal valor = 0;
            if (this.es.HasData)
            {
                valor = this.Valor.HasValue ? this.Valor.Value : 0;
            }

            return valor;
        }

        /// <summary>
        /// Retorna o total de lancamentos a vencer em Liquidacao na data passada e para a descricao informada.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="descricao"></param>
        /// <param name="dataVencimento"></param>        
        public decimal RetornaLiquidacaoNaData(int idCliente, string descricao, DateTime dataVencimento)
        {
            this.QueryReset();
            this.Query
              .Select(this.Query.Valor.Sum())
              .Where(this.Query.IdCliente == idCliente,
                     this.Query.Descricao.Equal(descricao),
                     this.Query.Situacao.NotEqual(SituacaoLancamentoLiquidacao.Compensacao),
                     this.Query.DataVencimento.Equal(dataVencimento));

            this.Query.Load();

            decimal valor = 0;
            if (this.es.HasData)
            {
                valor = this.Valor.HasValue ? this.Valor.Value : 0;
            }

            return valor;
        }

        /// <summary>
        /// Retorna o total de lancamentos a vencer em Liquidacao na data passada.
        /// Filtra por Situacao.NotEqual(SituacaoLancamentoLiquidacao.Compensacao).
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="dataVencimento"></param>        
        public decimal RetornaLiquidacaoNaData(int idCliente, DateTime dataVencimento)
        {
            this.QueryReset();
            this.Query
              .Select(this.Query.Valor.Sum())
              .Where(this.Query.IdCliente == idCliente,
                     this.Query.Situacao.NotEqual(SituacaoLancamentoLiquidacao.Compensacao),
                     this.Query.DataVencimento.Equal(dataVencimento));

            this.Query.Load();

            decimal valor = 0;
            if (this.es.HasData)
            {
                valor = this.Valor.HasValue ? this.Valor.Value : 0;
            }

            return valor;
        }

        /// <summary>
        /// Retorna o total de lancamentos a vencer em Liquidacao >= que a data passada e para a descricao informada.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="descricao"></param>
        /// <param name="dataVencimento"></param>        
        public decimal RetornaLiquidacaoMaiorIgualData(int idCliente, string descricao, DateTime dataVencimento)
        {
            this.QueryReset();
            this.Query
              .Select(this.Query.Valor.Sum())
              .Where(this.Query.IdCliente == idCliente,
                     this.Query.Descricao.Equal(descricao),
                     this.Query.Situacao.NotEqual(SituacaoLancamentoLiquidacao.Compensacao),
                     this.Query.DataVencimento.GreaterThanOrEqual(dataVencimento));

            this.Query.Load();

            decimal valor = 0;
            if (this.es.HasData)
            {
                valor = this.Valor.HasValue ? this.Valor.Value : 0;
            }

            return valor;
        }

        /// <summary>
        /// Carrega objeto Liquidacao com campos IdLiquidacao, Valor.
        /// </summary>
        /// <param name="identificadorOrigem"></param>
        /// <param name="origem"></param>
        /// <param name="idCliente"></param>
        /// <returns>bool indicando se achou registro.</returns>
        public bool BuscaLiquidacao(int identificadorOrigem, int origem, int idCliente)
        {
            this.QueryReset();
            this.Query
              .Select(this.Query.IdLiquidacao, this.Query.Valor)
              .Where(this.Query.IdentificadorOrigem == identificadorOrigem,
                     this.Query.Origem == origem,
                     this.Query.IdCliente == idCliente);

            bool retorno = this.Query.Load();

            return retorno;
        }

        /// <summary>
        /// Verifica se já houve resgate de cotista em D0 nessa data
        /// </summary>
        /// <param name="identificadorOrigem"></param>
        /// <param name="origem"></param>
        /// <param name="idCliente"></param>
        /// <returns>bool indicando se achou registro.</returns>
        public decimal VerificaJaResgatouCotistaD0(int idCliente, DateTime data)
        {
            this.QueryReset();
            this.Query
              .Select(this.Query.Valor.Sum())
              .Where(this.Query.IdCliente == idCliente,
                     this.Query.DataLancamento == data,
                     this.Query.DataVencimento == data,
                     this.Query.Origem.In(OrigemLancamentoLiquidacao.Cotista.Resgate,
                                          OrigemLancamentoLiquidacao.Fundo.Resgate));

            bool retorno = this.Query.Load();

            decimal valor = 0;
            if (this.es.HasData)
            {
                valor = this.Valor.HasValue ? this.Valor.Value : 0;
            }

            return valor;
        }

        /// <summary>
        /// Retorna o valor total de liquidações.
        /// Filtra por Situacao.NotEqual(SituacaoLancamentoLiquidacao.Compensacao).
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="dataVencimento"></param>
        /// <param name="idConta"></param>
        public decimal RetornaLiquidacao(int idCliente, DateTime dataVencimento, int idConta)
        {
            this.QueryReset();
            this.Query
              .Select(this.Query.Valor.Sum())
              .Where(this.Query.IdCliente == idCliente,
                     this.Query.DataVencimento.Equal(dataVencimento),
                     this.Query.IdConta == idConta,
                     this.Query.Situacao.NotEqual(SituacaoLancamentoLiquidacao.Compensacao));

            this.Query.Load();

            decimal valor = 0;
            if (this.es.HasData)
            {
                valor = this.Valor.HasValue ? this.Valor.Value : 0;
            }

            return valor;
        }

        /// <summary>
        /// Retorna o valor total de liquidações de impostos sobre resgate em D0 ( IR, IOF e PFEE ).
        /// Filtra por Situacao.NotEqual(SituacaoLancamentoLiquidacao.Compensacao).
        /// Leva em conta IdMoeda passado e faz as devidas conversões.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="dataVencimento"></param>
        /// <param name="idConta"></param>
        public decimal RetornaLiquidacaoImpostosResgateD0(int idCliente, DateTime data, int idMoedaCliente)
        {
            LiquidacaoCollection liquidacaoCollection = new LiquidacaoCollection();
            liquidacaoCollection.Query.Select(liquidacaoCollection.Query.Valor.Sum(), liquidacaoCollection.Query.IdConta);
            liquidacaoCollection.Query.Where(liquidacaoCollection.Query.IdCliente.Equal(idCliente),
                                             liquidacaoCollection.Query.DataLancamento.Equal(data),
                                             liquidacaoCollection.Query.DataVencimento.Equal(data),
                                             liquidacaoCollection.Query.Situacao.NotEqual(SituacaoLancamentoLiquidacao.Compensacao),
                                             liquidacaoCollection.Query.Origem.In((int)OrigemLancamentoLiquidacao.RendaFixa.IR,
                                                                                  (int)OrigemLancamentoLiquidacao.RendaFixa.IOF,
                                                                                  (int)OrigemLancamentoLiquidacao.Fundo.IRResgate,
                                                                                  (int)OrigemLancamentoLiquidacao.Fundo.IOFResgate,
                                                                                  (int)OrigemLancamentoLiquidacao.IR.IRRendaVariavel,
                                                                                  (int)OrigemLancamentoLiquidacao.IR.IRFonteDayTrade,
                                                                                  (int)OrigemLancamentoLiquidacao.IR.IRFonteOperacao));
            liquidacaoCollection.Query.GroupBy(liquidacaoCollection.Query.IdConta);
            liquidacaoCollection.Query.Load();

            decimal totalValor = 0;
            foreach (Liquidacao liquidacao in liquidacaoCollection)
            {
                int idConta = liquidacao.IdConta.Value;
                decimal valor = liquidacao.Valor.Value;

                //Trata multi-moeda
                Investidor.ContaCorrente contaCorrente = new Investidor.ContaCorrente();
                List<esQueryItem> campos = new List<esQueryItem>();
                campos.Add(contaCorrente.Query.IdMoeda);
                contaCorrente.LoadByPrimaryKey(campos, idConta);
                int idMoedaConta = contaCorrente.IdMoeda.Value;

                decimal ptax = 0;
                if (idMoedaCliente == (int)ListaMoedaFixo.Dolar)
                {
                    CotacaoIndice cotacaoIndice = new CotacaoIndice();
                    ptax = cotacaoIndice.BuscaCotacaoIndice((int)ListaIndiceFixo.PTAX_800VENDA, data);
                }

                if (idMoedaCliente != idMoedaConta)
                {
                    decimal fatorConversao = 0;
                    if (idMoedaCliente == (int)ListaMoedaFixo.Dolar && idMoedaConta == (int)ListaMoedaFixo.Real)
                    {
                        fatorConversao = 1 / ptax;
                    }
                    else
                    {
                        ConversaoMoeda conversaoMoeda = new ConversaoMoeda();
                        conversaoMoeda.Query.Where(conversaoMoeda.Query.IdMoedaDe.Equal(idMoedaCliente),
                                                   conversaoMoeda.Query.IdMoedaPara.Equal(idMoedaConta));
                        if (conversaoMoeda.Query.Load())
                        {
                            int idIndiceConversao = conversaoMoeda.IdIndice.Value;
                            CotacaoIndice cotacaoIndice = new CotacaoIndice();
                            if (conversaoMoeda.Tipo.Value == (byte)TipoConversaoMoeda.Divide)
                            {
                                fatorConversao = 1 / cotacaoIndice.BuscaCotacaoIndice(idIndiceConversao, data);
                            }
                            else
                            {
                                fatorConversao = cotacaoIndice.BuscaCotacaoIndice(idIndiceConversao, data);
                            }
                        }
                    }

                    valor = Math.Round(valor * fatorConversao, 2);
                }

                totalValor += valor;
            }

            return totalValor;
        }

        /// <summary>
        /// Retorna o valor total de liquidações do tipo = compensação.
        /// DataVencimento.Equal(dataVencimento),
        /// Situacao.Equal(SituacaoLancamentoLiquidacao.Compensacao).
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="dataVencimento"></param>  
        ///        
        public decimal RetornaLiquidacaoCompensacao(int idCliente, DateTime dataVencimento)
        {
            this.QueryReset();
            this.Query
              .Select(this.Query.Valor.Sum())
              .Where(this.Query.IdCliente == idCliente,
                     this.Query.DataVencimento.Equal(dataVencimento),
                     this.Query.Situacao.Equal(SituacaoLancamentoLiquidacao.Compensacao),
                     this.Query.Origem.NotEqual(OrigemLancamentoLiquidacao.AjusteCompensacaoCota));

            this.Query.Load();

            decimal valor = 0;
            if (this.es.HasData)
            {
                valor = this.Valor.HasValue ? this.Valor.Value : 0;
            }

            return valor;
        }
        


        /// <summary>
        /// Retorna o valor total de liquidações do tipo = compensação.
        /// DataVencimento.Equal(dataVencimento),
        /// Situacao.Equal(SituacaoLancamentoLiquidacao.Compensacao),
        /// Situacao.Equal(OrigemLancamentoLiquidacao.Fundo.AplicacaoConverterFundo)
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="dataVencimento"></param>        
        public decimal RetornaLiquidacaoCompensacaoFundos(int idCliente, DateTime dataVencimento)
        {
            this.QueryReset();
            this.Query
              .Select(this.Query.Valor.Sum())
              .Where(this.Query.IdCliente == idCliente,
                     this.Query.DataVencimento.Equal(dataVencimento),
                     this.Query.Situacao.Equal(SituacaoLancamentoLiquidacao.Compensacao),
                     this.Query.Origem.Equal(OrigemLancamentoLiquidacao.Fundo.AplicacaoConverter));

            this.Query.Load();

            decimal valor = 0;
            if (this.es.HasData)
            {
                valor = this.Valor.HasValue ? this.Valor.Value : 0;
            }

            return valor;
        }

        /// <summary>
        /// Carrega objeto Liquidacao com o campo Valor.Sum (calculado).
        /// Filtra apenas para quando IdConta for NULL em Liquidacao.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="dataVencimento"></param>        
        public void BuscaLiquidacaoContaNula(int idCliente, DateTime dataVencimento)
        {
            this.QueryReset();
            this.Query
              .Select(this.Query.Valor.Sum())
              .Where(this.Query.IdCliente == idCliente,
                     this.Query.DataVencimento.Equal(dataVencimento),
                     this.Query.IdConta.IsNull());

            this.Query.Load();

            if (this.es.HasData)
            {
                this.Valor = this.Valor.HasValue ? this.Valor.Value : 0;
            }
        }

        /// <summary>
        /// Lança em Liquidacao a partir dos lançamentos do arquivo CMDF.
        /// Apenas são lançados para data de liquidação posterior à data dia do cliente.
        /// </summary>
        /// <param name="cmdfCollection"></param>
        /// <param name="data"></param>
        public void CarregaLiquidacaoCmdf(CmdfCollection cmdfCollection, DateTime data)
        {

            #region GetIdAgente do Arquivo Cmdf
            int codigoBovespaAgente = cmdfCollection.CollectionCmdf[0].CodigoAgente.Value;
            AgenteMercado agenteMercado = new AgenteMercado();
            // Da exceção se Agente Mercado não existir
            int idAgente = agenteMercado.BuscaIdAgenteMercadoBovespa(codigoBovespaAgente);
            #endregion

            LiquidacaoCollection liquidacaoCollection = new LiquidacaoCollection();

            Cmdf cmdf = new Cmdf();

            // Pega os lançamentos do arquivo de Cmdf
            DateTime? dataLancamento = null;

            //Deleta todas as liquidações importadas pelo CMDF com vencimento no dia
            if (cmdfCollection.CollectionCmdf.Count > 0)
            {
                LiquidacaoCollection liquidacaoCollectionDeletar = new LiquidacaoCollection();
                liquidacaoCollectionDeletar.DeletaLiquidacaoCMDF(data);
            }
            //

            for (int i = 0; i < cmdfCollection.CollectionCmdf.Count; i++)
            {
                cmdf = cmdfCollection.CollectionCmdf[i];

                if (cmdf.IsTipoRegistroHeader())
                {
                    dataLancamento = cmdf.DataMovimento.Value;
                }

                if (cmdf.IsTipoRegistroMovimentoDia())
                {
                    // Informações do Arquivo
                    int codigoClienteBovespa = cmdf.CodigoCliente.Value;
                    int codigoGrupo = Convert.ToInt32(cmdf.CodigoGrupo.Value);
                    string cdAtivoBolsa = cmdf.CodigoNegocio;
                    decimal quantidade = cmdf.Quantidade.Value;
                    string quantidadeFormatada = quantidade.ToString("N0");
                    DateTime dataEfetivacao = cmdf.DataEfetivacao.Value;

                    StringBuilder descricao = new StringBuilder();
                    descricao.Append(cmdf.DescricaoLancamento).Append(" ").Append(quantidadeFormatada)
                                                              .Append(" ").Append(cdAtivoBolsa);

                    string tipoLancamento = cmdf.TipoLancamento; //D-Débito  C-Crédito
                    decimal valor = cmdf.Valor.Value;

                    if (tipoLancamento == "D")
                    {
                        valor = valor * -1;
                    }

                    decimal valorIR = cmdf.ValorIR.Value * -1; //Apenas para o caso de Juros sobre Capital
                    //       

                    CodigoClienteAgente codigoClienteAgente = new CodigoClienteAgente();
                    bool idClienteExiste = codigoClienteAgente.BuscaIdClienteInternoBovespa(codigoBovespaAgente, codigoClienteBovespa);
                    if (idClienteExiste)
                    {
                        int idCliente = codigoClienteAgente.IdCliente.Value;

                        Cliente cliente = new Cliente();
                        List<esQueryItem> campos = new List<esQueryItem>();
                        campos.Add(cliente.Query.StatusAtivo);
                        campos.Add(cliente.Query.DataDia);
                        bool existeCliente = cliente.LoadByPrimaryKey(idCliente);

                        if (existeCliente && cliente.IsAtivo)
                        { //Só importa para clientes ativos!
                            DateTime dataDia = cliente.DataDia.Value;
                            byte status = cliente.Status.Value;

                            //Busca o idContaDefault do cliente
                            Investidor.ContaCorrente contaCorrente = new Investidor.ContaCorrente();
                            int idContaDefault = contaCorrente.RetornaContaDefault(idCliente);
                            //
                            if (codigoGrupo == 4)
                            { //Grupo 4 = Proventos. A ser refatorado em caso da entrada de novos grupos
                                //Só insere se cliente está com data menor que a data de vencimento,
                                // ou data igual à data de vencimento e status diferente de fechado
                                if ((DateTime.Compare(dataEfetivacao, dataDia) > 0) ||
                                    (DateTime.Compare(dataEfetivacao, dataDia) == 0 && status != (byte)StatusCliente.Divulgado))
                                {
                                    #region Insere em Liquidacao
                                    Liquidacao liquidacao = new Liquidacao();
                                    liquidacao.AddNew();
                                    liquidacao.IdCliente = idCliente;
                                    liquidacao.DataLancamento = dataEfetivacao;
                                    liquidacao.DataVencimento = dataEfetivacao;
                                    liquidacao.Descricao = descricao.ToString();
                                    liquidacao.Valor = valor;
                                    liquidacao.Situacao = (byte)SituacaoLancamentoLiquidacao.Normal;

                                    int? idEvento = null;
                                    int origem = 0;
                                    if (cmdf.CodigoLancamento.Value == (int)CodigoLancamentoCMDF.Dividendo)
                                    {
                                        origem = (int)OrigemLancamentoLiquidacao.Bolsa.Dividendo;
                                        idEvento = ContabRoteiro.RetornaIdEvento(ContabilOrigem.Bolsa.PagamentoDividendosReceber);
                                    }
                                    else if (cmdf.CodigoLancamento.Value == (int)CodigoLancamentoCMDF.JurosCapital)
                                    {
                                        origem = (int)OrigemLancamentoLiquidacao.Bolsa.JurosCapital;
                                        idEvento = ContabRoteiro.RetornaIdEvento(ContabilOrigem.Bolsa.PagamentoJurosCapitalReceber);
                                    }
                                    else
                                    {
                                        origem = (int)OrigemLancamentoLiquidacao.Bolsa.JurosCapital;
                                        idEvento = ContabRoteiro.RetornaIdEvento(ContabilOrigem.Bolsa.PagamentoJurosCapitalReceber);
                                    }

                                    liquidacao.Origem = origem;
                                    liquidacao.Fonte = (byte)FonteLancamentoLiquidacao.ArquivoCMDF;
                                    liquidacao.IdAgente = idAgente;
                                    liquidacao.IdConta = idContaDefault;
                                    liquidacao.IdEvento = idEvento;

                                    liquidacaoCollection.AttachEntity(liquidacao);
                                    #endregion
                                }
                            }
                        }
                    }
                }
            }

            liquidacaoCollection.Save();
        }

        /// <summary>
        /// Retorna o valor total liquidando na data.
        /// </summary>
        /// <param name="idCliente"></param>     
        /// <param name="dataVencimento"></param>
        /// <param name="origemLancamentoLiquidacao"></param>
        /// <returns></returns>
        public decimal RetornaValor(int idCliente, DateTime dataVencimento, List<int> origemLancamentoLiquidacao)
        {
            return this.RetornaValor(idCliente, dataVencimento, origemLancamentoLiquidacao, SinalValorLiquidacao.Entrada_Retirada);
        }

        /// <summary>
        /// Retorna o valor total liquidando na data.
        /// </summary>
        /// <param name="idCliente"></param>     
        /// <param name="dataVencimento"></param>
        /// <param name="sinalValorLiquidacao">indica se maior que zero, menor que zero, ou todos</param>
        /// <returns></returns>
        public decimal RetornaValor(int idCliente, DateTime dataVencimento, SinalValorLiquidacao sinalValorLiquidacao)
        {
            this.QueryReset();
            this.Query
                 .Select(this.Query.Valor.Sum())
                 .Where(this.Query.IdCliente == idCliente,
                        this.Query.DataVencimento.Equal(dataVencimento),
                        this.Query.Situacao == (Int16)SituacaoLancamentoLiquidacao.Normal);

            if (sinalValorLiquidacao == SinalValorLiquidacao.Entrada)
            {
                this.Query.Where(this.Query.Valor.GreaterThan(0));
            }
            else if (sinalValorLiquidacao == SinalValorLiquidacao.Retirada)
            {
                this.Query.Where(this.Query.Valor.LessThan(0));
            }

            this.Query.Load();

            return this.Valor.HasValue ? this.Valor.Value : 0;
        }

        /// <summary>
        /// Retorna o valor total liquidando na data.
        /// </summary>
        /// <param name="idCliente"></param>     
        /// <param name="dataVencimento"></param>
        /// <param name="origem"></param>
        /// <param name="sinalValorLiquidacao">indica se maior que zero, menor que zero, ou todos</param>
        /// <returns></returns>
        public decimal RetornaValor(int idCliente, DateTime dataVencimento, List<int> origem, SinalValorLiquidacao sinalValorLiquidacao)
        {
            this.QueryReset();
            this.Query
                 .Select(this.Query.Valor.Sum())
                 .Where(this.Query.IdCliente == idCliente,
                        this.Query.Origem.In(origem),
                        this.Query.DataVencimento.Equal(dataVencimento),
                        this.Query.Situacao == (Int16)SituacaoLancamentoLiquidacao.Normal);

            if (sinalValorLiquidacao == SinalValorLiquidacao.Entrada)
            {
                this.Query.Where(this.Query.Valor.GreaterThan(0));
            }
            else if (sinalValorLiquidacao == SinalValorLiquidacao.Retirada)
            {
                this.Query.Where(this.Query.Valor.LessThan(0));
            }

            this.Query.Load();

            return this.Valor.HasValue ? this.Valor.Value : 0;
        }

        /// <summary>
        /// Retorna o valor total liquidando em datas posteriores à dataVencimento passada,
        /// e menores ou iguais à dataVencimento passada.
        /// </summary>
        /// <param name="idCliente"></param>     
        /// <param name="dataVencimento"></param>
        /// <param name="origem"></param>
        /// <param name="sinalValorLiquidacao">indica se maior que zero, menor que zero, ou todos</param>
        /// <returns></returns>
        public decimal RetornaValorVencer(int idCliente, DateTime data, List<int> origem, SinalValorLiquidacao sinalValorLiquidacao)
        {
            this.QueryReset();
            this.Query
                 .Select(this.Query.Valor.Sum())
                 .Where(this.Query.IdCliente == idCliente,
                        this.Query.Origem.In(origem),
                        this.Query.DataVencimento.GreaterThan(data),
                        this.Query.DataLancamento.LessThanOrEqual(data),
                        this.Query.Situacao == (Int16)SituacaoLancamentoLiquidacao.Normal);

            if (sinalValorLiquidacao == SinalValorLiquidacao.Entrada)
            {
                this.Query.Where(this.Query.Valor.GreaterThan(0));
            }
            else if (sinalValorLiquidacao == SinalValorLiquidacao.Retirada)
            {
                this.Query.Where(this.Query.Valor.LessThan(0));
            }

            this.Query.Load();

            return this.Valor.HasValue ? this.Valor.Value : 0;
        }

        /// <summary>
        /// Retorna o valor total liquidando em datas posteriores à data passada,
        /// e menores ou iguais à data passada.
        /// </summary>
        /// <param name="idCliente"></param>     
        /// <param name="dataVencimento"></param>
        /// <param name="sinalValorLiquidacao">indica se maior que zero, menor que zero, ou todos</param>
        /// <returns></returns>
        public decimal RetornaValorVencer(int idCliente, DateTime data, SinalValorLiquidacao sinalValorLiquidacao)
        {
            this.QueryReset();
            this.Query
                 .Select(this.Query.Valor.Sum())
                 .Where(this.Query.IdCliente == idCliente,
                        this.Query.DataVencimento.GreaterThan(data),
                        this.Query.DataLancamento.LessThanOrEqual(data),
                        this.Query.Situacao == (Int16)SituacaoLancamentoLiquidacao.Normal);

            if (sinalValorLiquidacao == SinalValorLiquidacao.Entrada)
            {
                this.Query.Where(this.Query.Valor.GreaterThan(0));
            }
            else if (sinalValorLiquidacao == SinalValorLiquidacao.Retirada)
            {
                this.Query.Where(this.Query.Valor.LessThan(0));
            }

            this.Query.Load();

            return this.Valor.HasValue ? this.Valor.Value : 0;
        }

        /// <summary>
        /// Função básica para copiar de Liquidacao para LiquidacaoHistorico.
        /// Apenas são copiados registros com data de vencimento >= que a data passada
        /// e data de lançamento menores ou iguais que a data passada.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="data"></param>
        public void GeraBackup(int idCliente, DateTime data)
        {
            try
            {
                LiquidacaoHistoricoCollection liquidacaoDeletarHistoricoCollection = new LiquidacaoHistoricoCollection();
                liquidacaoDeletarHistoricoCollection.DeletaLiquidacaoHistoricoDataHistoricoMaiorIgual(idCliente, data);
            }
            catch { }

            LiquidacaoCollection liquidacaoCollection = new LiquidacaoCollection();
            liquidacaoCollection.BuscaLiquidacaoCompleta(idCliente, data, data);
            //
            LiquidacaoHistoricoCollection liquidacaoHistoricoCollection = new LiquidacaoHistoricoCollection();
            //
            #region Copia de Liquidacao para LiquidacaoHistorico
            for (int i = 0; i < liquidacaoCollection.Count; i++)
            {
                Liquidacao liquidacao = liquidacaoCollection[i];

                LiquidacaoHistorico liquidacaoHistorico = liquidacaoHistoricoCollection.AddNew();

                liquidacaoHistorico.DataHistorico = data;
                liquidacaoHistorico.IdLiquidacao = liquidacao.IdLiquidacao;
                liquidacaoHistorico.IdCliente = liquidacao.IdCliente;
                liquidacaoHistorico.DataLancamento = liquidacao.DataLancamento;
                liquidacaoHistorico.DataVencimento = liquidacao.DataVencimento;
                liquidacaoHistorico.Descricao = liquidacao.Descricao;
                liquidacaoHistorico.Valor = liquidacao.Valor;
                liquidacaoHistorico.Situacao = liquidacao.Situacao;
                liquidacaoHistorico.Origem = liquidacao.Origem;
                liquidacaoHistorico.Fonte = liquidacao.Fonte;
                liquidacaoHistorico.IdAgente = liquidacao.IdAgente;
                liquidacaoHistorico.IdentificadorOrigem = liquidacao.IdentificadorOrigem;
                liquidacaoHistorico.IdConta = liquidacao.IdConta;
                liquidacaoHistorico.IdEvento = liquidacao.IdEvento;
                liquidacaoHistorico.IdEventoVencimento = liquidacao.IdEventoVencimento;
                liquidacaoHistorico.CodigoAtivo = liquidacao.CodigoAtivo;
            }
            #endregion

            liquidacaoHistoricoCollection.Save();
        }

        /// <summary>
        /// Função básica para copiar de Liquidacao para LiquidacaoAbertura.
        /// Apenas são copiados registros com data de vencimento >= que a data passada
        /// e data de lançamento menores ou iguais que a data passada.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="data"></param>
        public void GeraLiquidacaoAbertura(int idCliente, DateTime data)
        {
            LiquidacaoCollection liquidacaoCollection = new LiquidacaoCollection();
            liquidacaoCollection.BuscaLiquidacaoCompleta(idCliente, data, data);
            //
            LiquidacaoAberturaCollection liquidacaoAberturaCollection = new LiquidacaoAberturaCollection();
            //
            #region Copia de Liquidacao para LiquidacaoAbertura
            for (int i = 0; i < liquidacaoCollection.Count; i++)
            {
                Liquidacao liquidacao = liquidacaoCollection[i];

                LiquidacaoAbertura liquidacaoAbertura = liquidacaoAberturaCollection.AddNew();

                liquidacaoAbertura.DataHistorico = data;
                liquidacaoAbertura.IdLiquidacao = liquidacao.IdLiquidacao;
                liquidacaoAbertura.IdCliente = liquidacao.IdCliente;
                liquidacaoAbertura.DataLancamento = liquidacao.DataLancamento;
                liquidacaoAbertura.DataVencimento = liquidacao.DataVencimento;
                liquidacaoAbertura.Descricao = liquidacao.Descricao;
                liquidacaoAbertura.Valor = liquidacao.Valor;
                liquidacaoAbertura.Situacao = liquidacao.Situacao;
                liquidacaoAbertura.Origem = liquidacao.Origem;
                liquidacaoAbertura.Fonte = liquidacao.Fonte;
                liquidacaoAbertura.IdAgente = liquidacao.IdAgente;
                liquidacaoAbertura.IdentificadorOrigem = liquidacao.IdentificadorOrigem;
                liquidacaoAbertura.IdConta = liquidacao.IdConta;
                liquidacaoAbertura.IdEvento = liquidacao.IdEvento;
                liquidacaoAbertura.IdEventoVencimento = liquidacao.IdEventoVencimento;
                liquidacaoAbertura.CodigoAtivo = liquidacao.CodigoAtivo;
            }
            #endregion

            liquidacaoAberturaCollection.Save();
        }

        /// <summary>
        /// Função básica para copiar de Liquidacao para LiquidacaoFuturo.
        /// Apenas são copiados registros com data de vencimento >= que a data passada
        /// e data de lançamento menores ou iguais que a data passada.
        /// Fltra tb apenas lançamentos com Fonte = Manual e Origem = Outros.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="data"></param>
        public void GeraLiquidacaoFuturo(int idCliente, DateTime data, DateTime dataProcessamento)
        {
            List<LiquidacaoFuturo> lstLiqFuturo = new List<LiquidacaoFuturo>();
            LiquidacaoFuturoCollection liquidacaoFuturoCollectionAux = new LiquidacaoFuturoCollection();
            LiquidacaoCollection liquidacaoCollection = new LiquidacaoCollection();

            liquidacaoCollection.Query.Where(liquidacaoCollection.Query.IdCliente.Equal(idCliente) &
                                             liquidacaoCollection.Query.DataLancamento.GreaterThanOrEqual(data) &
                                             liquidacaoCollection.Query.Fonte.Equal((byte)FonteLancamentoLiquidacao.Manual) &
                                             liquidacaoCollection.Query.Origem.In((int)OrigemLancamentoLiquidacao.Outros, (int)OrigemLancamentoLiquidacao.CarteiraGerencial)); 
            liquidacaoCollection.Query.Load();

            #region Carrega LiquidacaoFuturo 
            liquidacaoFuturoCollectionAux.Query.Where(liquidacaoFuturoCollectionAux.Query.IdCliente.Equal(idCliente) &
                                 liquidacaoFuturoCollectionAux.Query.DataLancamento.GreaterThanOrEqual(dataProcessamento) &
                                 liquidacaoFuturoCollectionAux.Query.Fonte.Equal((byte)FonteLancamentoLiquidacao.Manual) &
                                 liquidacaoFuturoCollectionAux.Query.Origem.In((int)OrigemLancamentoLiquidacao.Outros, (int)OrigemLancamentoLiquidacao.CarteiraGerencial));

            if (liquidacaoFuturoCollectionAux.Query.Load())
                lstLiqFuturo = (List<LiquidacaoFuturo>)liquidacaoFuturoCollectionAux;
            #endregion
            //
            LiquidacaoFuturoCollection liquidacaoFuturoCollection = new LiquidacaoFuturoCollection();
            //
            #region Copia de Liquidacao para LiquidacaoFuturo
            for (int i = 0; i < liquidacaoCollection.Count; i++)
            {
                Liquidacao liquidacao = liquidacaoCollection[i];

                LiquidacaoFuturo liquidacaoFuturo = new LiquidacaoFuturo();
                if (!liquidacaoFuturo.LoadByPrimaryKey((int)liquidacao.IdLiquidacao, data))
                    liquidacaoFuturo = liquidacaoFuturoCollection.AddNew();

                liquidacaoFuturo.DataHistorico = data;
                liquidacaoFuturo.IdLiquidacao = liquidacao.IdLiquidacao;
                liquidacaoFuturo.IdCliente = liquidacao.IdCliente;
                liquidacaoFuturo.DataLancamento = liquidacao.DataLancamento;
                liquidacaoFuturo.DataVencimento = liquidacao.DataVencimento;
                liquidacaoFuturo.Descricao = liquidacao.Descricao;
                liquidacaoFuturo.Valor = liquidacao.Valor;
                liquidacaoFuturo.Situacao = liquidacao.Situacao;
                liquidacaoFuturo.Origem = liquidacao.Origem;
                liquidacaoFuturo.Fonte = liquidacao.Fonte;
                liquidacaoFuturo.IdAgente = liquidacao.IdAgente;
                liquidacaoFuturo.IdentificadorOrigem = liquidacao.IdentificadorOrigem;
                liquidacaoFuturo.IdConta = liquidacao.IdConta;
                liquidacaoFuturo.IdEvento = liquidacao.IdEvento;
                liquidacaoFuturo.IdEventoVencimento = liquidacao.IdEventoVencimento;
                liquidacaoFuturo.CodigoAtivo = liquidacao.CodigoAtivo;
            }
            #endregion

            liquidacaoFuturoCollection.Save();
        }

        /// <summary>
        /// Função básica para copiar de Liquidacao para LiquidacaoAbertura.
        /// Apenas são copiados registros com data de vencimento >= que a data passada
        /// e data de lançamento iguais à data passada, com Origem.In(
        ///                                         (int)OrigemLancamentoLiquidacao.Provisao.ProvisaoOutros,
        ///                                         (int)OrigemLancamentoLiquidacao.Provisao.TaxaPerformance,
        ///                                         (int)OrigemLancamentoLiquidacao.Provisao.TaxaGestao,
        ///                                         (int)OrigemLancamentoLiquidacao.Provisao.TaxaAdministracao).
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="data"></param>
        public void GeraLiquidacaoAberturaLancamentoNaData(int idCliente, DateTime data)
        {
            LiquidacaoCollection liquidacaoCollectionDeletar = new LiquidacaoCollection();
            liquidacaoCollectionDeletar.Query.Where(liquidacaoCollectionDeletar.Query.IdCliente == idCliente,
                                                     liquidacaoCollectionDeletar.Query.DataLancamento.LessThan(data),
                                                     liquidacaoCollectionDeletar.Query.Fonte.Equal((byte)FonteLancamentoLiquidacao.Interno),
                                                     liquidacaoCollectionDeletar.Query.Origem.In(
                                                        (int)OrigemLancamentoLiquidacao.Provisao.ProvisaoOutros,
                                                        (int)OrigemLancamentoLiquidacao.Provisao.TaxaPerformance,
                                                        (int)OrigemLancamentoLiquidacao.Provisao.TaxaGestao,
                                                        (int)OrigemLancamentoLiquidacao.Provisao.TaxaAdministracao));
            liquidacaoCollectionDeletar.Query.Load();
            liquidacaoCollectionDeletar.MarkAllAsDeleted();
            liquidacaoCollectionDeletar.Save();

            LiquidacaoAberturaCollection liquidacaoAberturaCollectionDeletar = new LiquidacaoAberturaCollection();
            liquidacaoAberturaCollectionDeletar.Query.Where(liquidacaoAberturaCollectionDeletar.Query.IdCliente == idCliente,
                                                     liquidacaoAberturaCollectionDeletar.Query.DataHistorico.Equal(data),
                                                     liquidacaoAberturaCollectionDeletar.Query.Fonte.Equal((byte)FonteLancamentoLiquidacao.Interno),
                                                     liquidacaoAberturaCollectionDeletar.Query.Origem.In(
                                                        (int)OrigemLancamentoLiquidacao.Provisao.ProvisaoOutros,
                                                        (int)OrigemLancamentoLiquidacao.Provisao.TaxaPerformance,
                                                        (int)OrigemLancamentoLiquidacao.Provisao.TaxaGestao,
                                                        (int)OrigemLancamentoLiquidacao.Provisao.TaxaAdministracao));
            liquidacaoAberturaCollectionDeletar.Query.Load();
            liquidacaoAberturaCollectionDeletar.MarkAllAsDeleted();
            liquidacaoAberturaCollectionDeletar.Save();

            LiquidacaoCollection liquidacaoCollection = new LiquidacaoCollection();
            liquidacaoCollection.Query.Where(liquidacaoCollection.Query.IdCliente == idCliente,
                                             liquidacaoCollection.Query.DataLancamento == data,
                                             liquidacaoCollection.Query.Fonte.Equal((byte)FonteLancamentoLiquidacao.Interno),
                                             liquidacaoCollection.Query.Origem.In(
                                                (int)OrigemLancamentoLiquidacao.Provisao.ProvisaoOutros,
                                                (int)OrigemLancamentoLiquidacao.Provisao.TaxaPerformance,
                                                (int)OrigemLancamentoLiquidacao.Provisao.TaxaGestao,
                                                (int)OrigemLancamentoLiquidacao.Provisao.TaxaAdministracao));
            liquidacaoCollection.Query.Load();
            //
            LiquidacaoAberturaCollection liquidacaoAberturaCollection = new LiquidacaoAberturaCollection();
            //
            #region Copia de Liquidacao para LiquidacaoAbertura
            for (int i = 0; i < liquidacaoCollection.Count; i++)
            {
                Liquidacao liquidacao = liquidacaoCollection[i];

                LiquidacaoAbertura liquidacaoAbertura = liquidacaoAberturaCollection.AddNew();

                liquidacaoAbertura.DataHistorico = data;
                liquidacaoAbertura.IdLiquidacao = liquidacao.IdLiquidacao;
                liquidacaoAbertura.IdCliente = liquidacao.IdCliente;
                liquidacaoAbertura.DataLancamento = liquidacao.DataLancamento;
                liquidacaoAbertura.DataVencimento = liquidacao.DataVencimento;
                liquidacaoAbertura.Descricao = liquidacao.Descricao;
                liquidacaoAbertura.Valor = liquidacao.Valor;
                liquidacaoAbertura.Situacao = liquidacao.Situacao;
                liquidacaoAbertura.Origem = liquidacao.Origem;
                liquidacaoAbertura.Fonte = liquidacao.Fonte;
                liquidacaoAbertura.IdAgente = liquidacao.IdAgente;
                liquidacaoAbertura.IdentificadorOrigem = liquidacao.IdentificadorOrigem;
                liquidacaoAbertura.IdConta = liquidacao.IdConta;
                liquidacaoAbertura.IdEvento = liquidacao.IdEvento;
                liquidacaoAbertura.IdEventoVencimento = liquidacao.IdEventoVencimento;
                liquidacaoAbertura.CodigoAtivo = liquidacao.CodigoAtivo;
            }
            #endregion

            liquidacaoAberturaCollection.Save();
        }

        /// <summary>
        /// Importa do Sinacor todos os eventos para o idCliente e data passados.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="data"></param>        
        public void IntegraCCSinacorFull(int idCliente, List<int> listaCodigos, DateTime data, List<int> listaEventosNaoImportar)
        {
            VTccmovtoCollection vTccmovtoCollection = new VTccmovtoCollection();
            vTccmovtoCollection.BuscaLancamentoDataFull(listaCodigos, data, listaEventosNaoImportar);

            //Busca o idContaDefault do cliente
            Investidor.ContaCorrente contaCorrente = new Investidor.ContaCorrente();
            int idContaDefault = contaCorrente.RetornaContaDefault(idCliente);
            //

            LiquidacaoCollection liquidacaoCollection = new LiquidacaoCollection();
            foreach (VTccmovto vTccmovto in vTccmovtoCollection)
            {
                string descricao = vTccmovto.DsLancamento;
                DateTime dataLancamento = vTccmovto.DtLancamento.Value;
                DateTime dataVencimento = vTccmovto.DtLiquidacao.Value;
                decimal valor = vTccmovto.VlLancamento.Value;

                Liquidacao liquidacao = liquidacaoCollection.AddNew();
                liquidacao.Descricao = descricao;
                liquidacao.DataLancamento = dataLancamento;
                liquidacao.DataVencimento = dataVencimento;
                liquidacao.Fonte = (byte)FonteLancamentoLiquidacao.Sinacor;
                liquidacao.IdCliente = idCliente;
                liquidacao.IdConta = idContaDefault;
                liquidacao.Origem = OrigemLancamentoLiquidacao.Outros;
                liquidacao.Situacao = (byte)SituacaoLancamentoLiquidacao.Normal;
                liquidacao.Valor = valor;
            }

            liquidacaoCollection.Save();
        }

        /// <summary>
        /// Importa do Sinacor os eventos listados na lista passada, para o idCliente e data passados.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="data"></param>
        /// <param name="listaEventos"></param>
        public void IntegraCCSinacor(int idCliente, List<int> listaCodigos, DateTime data, List<int> listaEventos)
        {
            VTccmovtoCollection vTccmovtoCollection = new VTccmovtoCollection();
            vTccmovtoCollection.BuscaLiquidacaoData(listaCodigos, data, listaEventos);

            //Busca o idContaDefault do cliente
            Investidor.ContaCorrente contaCorrente = new Investidor.ContaCorrente();
            int idContaDefault = contaCorrente.RetornaContaDefault(idCliente);
            //

            LiquidacaoCollection liquidacaoCollection = new LiquidacaoCollection();
            foreach (VTccmovto vTccmovto in vTccmovtoCollection)
            {
                string descricao = vTccmovto.DsLancamento;
                DateTime dataVencimento = vTccmovto.DtLiquidacao.Value;
                decimal valor = vTccmovto.VlLancamento.Value;

                Liquidacao liquidacao = liquidacaoCollection.AddNew();
                liquidacao.Descricao = descricao;
                liquidacao.DataLancamento = dataVencimento;
                liquidacao.DataVencimento = dataVencimento;
                liquidacao.Fonte = (byte)FonteLancamentoLiquidacao.Sinacor;
                liquidacao.IdCliente = idCliente;
                liquidacao.IdConta = idContaDefault;
                liquidacao.Origem = OrigemLancamentoLiquidacao.Outros;
                liquidacao.Situacao = (byte)SituacaoLancamentoLiquidacao.Normal;
                liquidacao.Valor = valor;
            }

            liquidacaoCollection.Save();
        }

        /// <summary>
        /// Importa do Sinacor os eventos listados na lista passada, para o idCliente e data passados.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="data"></param>
        /// <param name="listaEventos"></param>
        public void IntegraCCSinacorAporte(int idCliente, List<int> listaCodigos, DateTime data, List<int> listaEventos)
        {
            VTccmovtoCollection vTccmovtoCollection = new VTccmovtoCollection();
            vTccmovtoCollection.BuscaLiquidacaoData(listaCodigos, data, listaEventos);

            OperacaoCotistaCollection operacaoCotistaCollectionDeletar = new OperacaoCotistaCollection();
            operacaoCotistaCollectionDeletar.Query.Select(operacaoCotistaCollectionDeletar.Query.IdOperacao);
            operacaoCotistaCollectionDeletar.Query.Where(operacaoCotistaCollectionDeletar.Query.IdCarteira.Equal(idCliente),
                                                         operacaoCotistaCollectionDeletar.Query.DataOperacao.Equal(data),
                                                         operacaoCotistaCollectionDeletar.Query.TipoOperacao.Equal((byte)TipoOperacaoCotista.Aplicacao));
            operacaoCotistaCollectionDeletar.Query.Load();
            operacaoCotistaCollectionDeletar.MarkAllAsDeleted();
            operacaoCotistaCollectionDeletar.Save();

            OperacaoCotistaCollection operacaoCotistaCollection = new OperacaoCotistaCollection();

            foreach (VTccmovto vTccmovto in vTccmovtoCollection)
            {
                string descricao = vTccmovto.DsLancamento.Trim();
                if (descricao.Length > 400)
                {
                    descricao = descricao.Substring(0, 400);
                }

                decimal valor = Math.Abs(vTccmovto.VlLancamento.Value);

                OperacaoCotista operacaoCotista = operacaoCotistaCollection.AddNew();

                operacaoCotista.IdCarteira = idCliente;
                operacaoCotista.IdCotista = idCliente;
                operacaoCotista.DataOperacao = data;
                operacaoCotista.DataConversao = data;
                operacaoCotista.DataLiquidacao = data;
                operacaoCotista.TipoOperacao = (byte)TipoOperacaoCotista.Aplicacao;
                operacaoCotista.DataAgendamento = data;
                operacaoCotista.TipoResgate = null;
                operacaoCotista.Observacao = descricao;
                operacaoCotista.ValorBruto = valor;
                operacaoCotista.ValorLiquido = 0;
                operacaoCotista.Quantidade = 0;
                operacaoCotista.IdFormaLiquidacao = 1; // Fixo
                operacaoCotista.Fonte = (byte)FonteOperacaoCotista.Manual;

                #region Cria o Cotista se não existir
                Cotista c = new Cotista();
                if (!c.LoadByPrimaryKey(idCliente))
                {
                    Cotista cotistaAux = new Cotista();
                    //            
                    Cliente cliente = new Cliente();
                    List<esQueryItem> campos = new List<esQueryItem>();
                    campos.Add(cliente.Query.Apelido);
                    cliente.LoadByPrimaryKey(campos, idCliente);

                    cotistaAux.IdCotista = idCliente;
                    cotistaAux.Nome = cliente.str.Apelido;
                    cotistaAux.Apelido = cliente.str.Apelido;
                    cotistaAux.IsentoIR = "S";
                    cotistaAux.IsentoIOF = "S";
                    cotistaAux.StatusAtivo = (byte)StatusAtivoCotista.Ativo;
                    cotistaAux.Save();
                }
                #endregion

            }


            operacaoCotistaCollection.Save();
        }

        /// <summary>
        /// Importa do Sinacor os eventos listados na lista passada, para o idCliente e data passados.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="data"></param>
        /// <param name="listaEventos"></param>
        public void IntegraCCSinacorResgate(int idCliente, List<int> listaCodigos, DateTime data, List<int> listaEventos)
        {
            VTccmovtoCollection vTccmovtoCollection = new VTccmovtoCollection();
            vTccmovtoCollection.BuscaLiquidacaoData(listaCodigos, data, listaEventos);

            OperacaoCotistaCollection operacaoCotistaCollectionDeletar = new OperacaoCotistaCollection();
            operacaoCotistaCollectionDeletar.Query.Select(operacaoCotistaCollectionDeletar.Query.IdOperacao);
            operacaoCotistaCollectionDeletar.Query.Where(operacaoCotistaCollectionDeletar.Query.IdCarteira.Equal(idCliente),
                                                         operacaoCotistaCollectionDeletar.Query.DataOperacao.Equal(data),
                                                         operacaoCotistaCollectionDeletar.Query.TipoOperacao.Equal((byte)TipoOperacaoCotista.ResgateBruto));
            operacaoCotistaCollectionDeletar.Query.Load();
            operacaoCotistaCollectionDeletar.MarkAllAsDeleted();
            operacaoCotistaCollectionDeletar.Save();

            OperacaoCotistaCollection operacaoCotistaCollection = new OperacaoCotistaCollection();

            foreach (VTccmovto vTccmovto in vTccmovtoCollection)
            {
                string descricao = vTccmovto.DsLancamento.Trim();
                if (descricao.Length > 400)
                {
                    descricao = descricao.Substring(0, 400);
                }

                decimal valor = Math.Abs(vTccmovto.VlLancamento.Value);

                OperacaoCotista operacaoCotista = operacaoCotistaCollection.AddNew();

                operacaoCotista.IdCarteira = idCliente;
                operacaoCotista.IdCotista = idCliente;
                operacaoCotista.DataOperacao = data;
                operacaoCotista.DataConversao = data;
                operacaoCotista.DataLiquidacao = data;
                operacaoCotista.TipoOperacao = (byte)TipoOperacaoCotista.ResgateBruto;
                operacaoCotista.DataAgendamento = data;
                operacaoCotista.TipoResgate = null;
                operacaoCotista.Observacao = descricao;
                operacaoCotista.ValorBruto = valor;
                operacaoCotista.ValorLiquido = 0;
                operacaoCotista.Quantidade = 0;
                operacaoCotista.IdFormaLiquidacao = 1; // Fixo
                operacaoCotista.Fonte = (byte)FonteOperacaoCotista.Manual;

                #region Cria o Cotista se não existir
                Cotista c = new Cotista();
                if (!c.LoadByPrimaryKey(idCliente))
                {
                    Cotista cotistaAux = new Cotista();
                    //            
                    Cliente cliente = new Cliente();
                    List<esQueryItem> campos = new List<esQueryItem>();
                    campos.Add(cliente.Query.Apelido);
                    cliente.LoadByPrimaryKey(campos, idCliente);

                    cotistaAux.IdCotista = idCliente;
                    cotistaAux.Nome = cliente.str.Apelido;
                    cotistaAux.Apelido = cliente.str.Apelido;
                    cotistaAux.IsentoIR = "S";
                    cotistaAux.IsentoIOF = "S";
                    cotistaAux.StatusAtivo = (byte)StatusAtivoCotista.Ativo;
                    cotistaAux.Save();
                }
                #endregion

            }


            operacaoCotistaCollection.Save();
        }

        /// <summary>
        /// Importa do Sinacor os eventos listados na lista passada, para o idCliente e data passados.
        /// Trata apenas eventos de c/c de chamada e devolução de margem.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="data"></param>
        /// <param name="listaEventos"></param>
        public void IntegraCCSinacorMargem(int idCliente, List<int> listaCodigos, DateTime data, List<int> listaEventos)
        {
            VTccmovtoCollection vTccmovtoCollection = new VTccmovtoCollection();
            vTccmovtoCollection.BuscaLiquidacaoData(listaCodigos, data, listaEventos, true);

            //Busca o idContaDefault do cliente
            Investidor.ContaCorrente contaCorrente = new Investidor.ContaCorrente();
            int idContaDefault = contaCorrente.RetornaContaDefault(idCliente);
            //
            foreach (VTccmovto vTccmovto in vTccmovtoCollection)
            {
                string descricao = vTccmovto.DsLancamento;
                decimal valor = vTccmovto.VlLancamento.Value;

                if (valor < 0)
                {
                    Liquidacao liquidacao = new Liquidacao();
                    liquidacao.Descricao = descricao;
                    liquidacao.DataLancamento = data;
                    liquidacao.DataVencimento = data;
                    liquidacao.Fonte = (byte)FonteLancamentoLiquidacao.Sinacor;
                    liquidacao.IdCliente = idCliente;
                    liquidacao.IdConta = idContaDefault;
                    liquidacao.Origem = OrigemLancamentoLiquidacao.Margem.ChamadaMargem;
                    liquidacao.Situacao = (byte)SituacaoLancamentoLiquidacao.Normal;
                    liquidacao.Valor = valor;

                    liquidacao.Save();

                    Liquidacao liquidacaoDevolucao = new Liquidacao();
                    liquidacaoDevolucao.Descricao = "Devolução Margem";
                    liquidacaoDevolucao.DataLancamento = data;
                    liquidacaoDevolucao.DataVencimento = new DateTime(4000, 2, 29);
                    liquidacaoDevolucao.Fonte = (byte)FonteLancamentoLiquidacao.Interno;
                    liquidacaoDevolucao.IdCliente = idCliente;
                    liquidacaoDevolucao.IdConta = idContaDefault;
                    liquidacaoDevolucao.Origem = OrigemLancamentoLiquidacao.Margem.DevolucaoMargem;
                    liquidacaoDevolucao.Situacao = (byte)SituacaoLancamentoLiquidacao.Pendente;
                    liquidacaoDevolucao.Valor = valor * -1;

                    liquidacaoDevolucao.Save();
                }
                else
                {
                    Liquidacao liquidacaoEstorno = new Liquidacao();
                    liquidacaoEstorno.Descricao = descricao;
                    liquidacaoEstorno.DataLancamento = data;
                    liquidacaoEstorno.DataVencimento = data;
                    liquidacaoEstorno.Fonte = (byte)FonteLancamentoLiquidacao.Sinacor;
                    liquidacaoEstorno.IdCliente = idCliente;
                    liquidacaoEstorno.IdConta = idContaDefault;
                    liquidacaoEstorno.Origem = OrigemLancamentoLiquidacao.Margem.DevolucaoMargem;
                    liquidacaoEstorno.Situacao = (byte)SituacaoLancamentoLiquidacao.Normal;
                    liquidacaoEstorno.Valor = valor;

                    liquidacaoEstorno.Save();

                    LiquidacaoCollection liquidacaoCollectionMargem = new LiquidacaoCollection();
                    liquidacaoCollectionMargem.Query.Where(liquidacaoCollectionMargem.Query.IdCliente.Equal(idCliente),
                                                           liquidacaoCollectionMargem.Query.Origem.Equal(OrigemLancamentoLiquidacao.Margem.DevolucaoMargem),
                                                           liquidacaoCollectionMargem.Query.DataVencimento.GreaterThan(data),
                                                           liquidacaoCollectionMargem.Query.Situacao.Equal((byte)SituacaoLancamentoLiquidacao.Pendente));
                    liquidacaoCollectionMargem.Query.OrderBy(liquidacaoCollectionMargem.Query.IdLiquidacao.Ascending);
                    liquidacaoCollectionMargem.Query.Load();

                    decimal valorRestante = Math.Abs(valor);
                    foreach (Liquidacao liquidacaoMargem in liquidacaoCollectionMargem)
                    {
                        if (liquidacaoMargem.Valor.Value > valorRestante)
                        {
                            liquidacaoMargem.Valor = liquidacaoMargem.Valor.Value - valorRestante;
                            valorRestante = 0;
                        }
                        else
                        {
                            valorRestante -= liquidacaoMargem.Valor.Value;
                            liquidacaoMargem.Valor = 0;
                        }

                        if (valorRestante == 0)
                        {
                            break;
                        }
                    }
                    liquidacaoCollectionMargem.Save();
                }

            }

        }

        /// <summary>
        /// Verifica se existe algum lançamento em LiquidacaoFuturo na dataHistorico referente à data passada.
        /// Caso exista, deleta todos os lançamentos manuais com origem = Outros em Liquidacao e insere os lancamentos de LiquidacaoFuturo.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="data"></param>
        public void CarregaLiquidacaoFuturo(int idCliente, DateTime data)
        {
            LiquidacaoFuturoCollection liquidacaoFuturoCollection = new LiquidacaoFuturoCollection();
            liquidacaoFuturoCollection.Query.Where(liquidacaoFuturoCollection.Query.IdCliente.Equal(idCliente),
                                                   liquidacaoFuturoCollection.Query.DataHistorico.Equal(data));
            liquidacaoFuturoCollection.Query.Load();

            if (liquidacaoFuturoCollection.Count == 0)
                return;

            LiquidacaoCollection liquidacaoCollectionDeletar = new LiquidacaoCollection();
            liquidacaoCollectionDeletar.Query.Select(liquidacaoCollectionDeletar.Query.IdLiquidacao);
            liquidacaoCollectionDeletar.Query.Where(liquidacaoCollectionDeletar.Query.IdCliente.Equal(idCliente),
                                             liquidacaoCollectionDeletar.Query.DataLancamento.Equal(data),
                                             liquidacaoCollectionDeletar.Query.DataVencimento.GreaterThanOrEqual(data),
                                             liquidacaoCollectionDeletar.Query.Fonte.Equal((byte)FonteLancamentoLiquidacao.Manual),
                                             liquidacaoCollectionDeletar.Query.Origem.In((int)OrigemLancamentoLiquidacao.Outros, (int)OrigemLancamentoLiquidacao.CarteiraGerencial));
            liquidacaoCollectionDeletar.Query.Load();
            liquidacaoCollectionDeletar.MarkAllAsDeleted();
            liquidacaoCollectionDeletar.Save();

            LiquidacaoCollection liquidacaoCollection = new LiquidacaoCollection();
            foreach (LiquidacaoFuturo liquidacaoFuturo in liquidacaoFuturoCollection)
            {
                Liquidacao liquidacao = new Liquidacao();
                liquidacao.Query.Select(liquidacao.Query.IdLiquidacao);
                liquidacao.Query.Where(liquidacao.Query.IdLiquidacao.Equal(liquidacaoFuturo.IdLiquidacao.Value));
                if (!liquidacao.Query.Load()) //Garante que só vai trazer da LiquidacaoFuturo lancamentos com Id nao presentes na Liquidacao (evita conflito de PK)
                {
                    #region Copia de LiquidacaoFuturo para Liquidacao
                    esParameters esParams = new esParameters();
                    esParams.Add("IdLiquidacao", liquidacaoFuturo.IdLiquidacao.Value);
                    esParams.Add("IdCliente", liquidacaoFuturo.IdCliente.Value);
                    esParams.Add("DataLancamento", liquidacaoFuturo.DataLancamento.Value);
                    esParams.Add("DataVencimento", liquidacaoFuturo.DataVencimento.Value);
                    esParams.Add("Descricao", liquidacaoFuturo.Descricao);
                    esParams.Add("Valor", liquidacaoFuturo.Valor.Value);
                    esParams.Add("Situacao", liquidacaoFuturo.Situacao.Value);
                    esParams.Add("Origem", liquidacaoFuturo.Origem.Value);
                    esParams.Add("Fonte", liquidacaoFuturo.Fonte.Value);

                    // Parametros que podem ser null
                    if (!liquidacaoFuturo.IdAgente.HasValue)
                    {
                        esParams.Add("IdAgente", DBNull.Value);
                    }
                    else
                    {
                        esParams.Add("IdAgente", liquidacaoFuturo.IdAgente.Value);
                    }

                    // Parametros que podem ser null
                    if (!liquidacaoFuturo.IdentificadorOrigem.HasValue)
                    {
                        esParams.Add("IdentificadorOrigem", DBNull.Value);
                    }
                    else
                    {
                        esParams.Add("IdentificadorOrigem", liquidacaoFuturo.IdentificadorOrigem.Value);
                    }

                    if (!liquidacaoFuturo.IdEvento.HasValue)
                    {
                        esParams.Add("IdEvento", DBNull.Value);
                    }
                    else
                    {
                        esParams.Add("IdEvento", liquidacaoFuturo.IdEvento.Value);
                    }

                    if (!liquidacaoFuturo.IdEventoVencimento.HasValue)
                    {
                        esParams.Add("IdEventoVencimento", DBNull.Value);
                    }
                    else
                    {
                        esParams.Add("IdEventoVencimento", liquidacaoFuturo.IdEventoVencimento.Value);
                    }

                    if (string.IsNullOrEmpty(liquidacaoFuturo.CodigoAtivo))
                    {
                        esParams.Add("CodigoAtivo", DBNull.Value);
                    }
                    else
                    {
                        esParams.Add("CodigoAtivo", liquidacaoFuturo.CodigoAtivo);
                    }

                    esParams.Add("IdConta", liquidacaoFuturo.IdConta.Value);
                    //
                    StringBuilder sqlBuilder = new StringBuilder();
                    sqlBuilder.Append("SET IDENTITY_INSERT Liquidacao ON ");
                    //
                    sqlBuilder.Append("INSERT INTO Liquidacao (");
                    sqlBuilder.Append(LiquidacaoMetadata.ColumnNames.IdLiquidacao);
                    sqlBuilder.Append("," + LiquidacaoMetadata.ColumnNames.IdCliente);
                    sqlBuilder.Append("," + LiquidacaoMetadata.ColumnNames.DataLancamento);
                    sqlBuilder.Append("," + LiquidacaoMetadata.ColumnNames.DataVencimento);
                    sqlBuilder.Append("," + LiquidacaoMetadata.ColumnNames.Descricao);
                    sqlBuilder.Append("," + LiquidacaoMetadata.ColumnNames.Valor);
                    sqlBuilder.Append("," + LiquidacaoMetadata.ColumnNames.Situacao);
                    sqlBuilder.Append("," + LiquidacaoMetadata.ColumnNames.Origem);
                    sqlBuilder.Append("," + LiquidacaoMetadata.ColumnNames.Fonte);
                    sqlBuilder.Append("," + LiquidacaoMetadata.ColumnNames.IdAgente);
                    sqlBuilder.Append("," + LiquidacaoMetadata.ColumnNames.IdentificadorOrigem);
                    sqlBuilder.Append("," + LiquidacaoMetadata.ColumnNames.IdEvento);
                    sqlBuilder.Append("," + LiquidacaoMetadata.ColumnNames.IdEventoVencimento);
                    sqlBuilder.Append("," + LiquidacaoMetadata.ColumnNames.IdConta);
                    sqlBuilder.Append("," + LiquidacaoMetadata.ColumnNames.CodigoAtivo);
                    //              
                    sqlBuilder.Append(") VALUES ( ");
                    sqlBuilder.Append("@" + LiquidacaoMetadata.ColumnNames.IdLiquidacao);
                    sqlBuilder.Append(",");
                    sqlBuilder.Append("@" + LiquidacaoMetadata.ColumnNames.IdCliente);
                    sqlBuilder.Append(",");
                    sqlBuilder.Append("@" + LiquidacaoMetadata.ColumnNames.DataLancamento);
                    sqlBuilder.Append(",");
                    sqlBuilder.Append("@" + LiquidacaoMetadata.ColumnNames.DataVencimento);
                    sqlBuilder.Append(",");
                    sqlBuilder.Append("@" + LiquidacaoMetadata.ColumnNames.Descricao);
                    sqlBuilder.Append(",");
                    sqlBuilder.Append("@" + LiquidacaoMetadata.ColumnNames.Valor);
                    sqlBuilder.Append(",");
                    sqlBuilder.Append("@" + LiquidacaoMetadata.ColumnNames.Situacao);
                    sqlBuilder.Append(",");
                    sqlBuilder.Append("@" + LiquidacaoMetadata.ColumnNames.Origem);
                    sqlBuilder.Append(",");
                    sqlBuilder.Append("@" + LiquidacaoMetadata.ColumnNames.Fonte);
                    sqlBuilder.Append(",");
                    sqlBuilder.Append("@" + LiquidacaoMetadata.ColumnNames.IdAgente);
                    sqlBuilder.Append(",");
                    sqlBuilder.Append("@" + LiquidacaoMetadata.ColumnNames.IdentificadorOrigem);
                    sqlBuilder.Append(",");
                    sqlBuilder.Append("@" + LiquidacaoMetadata.ColumnNames.IdEvento);
                    sqlBuilder.Append(",");
                    sqlBuilder.Append("@" + LiquidacaoMetadata.ColumnNames.IdEventoVencimento);
                    sqlBuilder.Append(",");
                    sqlBuilder.Append("@" + LiquidacaoMetadata.ColumnNames.IdConta);
                    sqlBuilder.Append(",");
                    sqlBuilder.Append("@" + LiquidacaoMetadata.ColumnNames.CodigoAtivo);
                    sqlBuilder.Append(")");
                    sqlBuilder.Append("SET IDENTITY_INSERT Liquidacao OFF ");
                    esUtility u = new esUtility();
                    u.ExecuteNonQuery(esQueryType.Text, sqlBuilder.ToString(), esParams);
                    #endregion
                }
            }
            liquidacaoCollection.Save();

            liquidacaoFuturoCollection.MarkAllAsDeleted();
            liquidacaoFuturoCollection.Save();
        }

        /// <summary>
        /// Retorna o total liquidado (pago), buscando em Liquidacao.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="origem"></param>
        /// <param name="dataInicio"></param>
        /// <param name="dataFim"></param>
        /// <returns></returns>
        public decimal RetornaValorLiquidado(int idCliente, int origem, DateTime dataInicio, DateTime dataFim)
        {
            Liquidacao liquidacao = new Liquidacao();
            liquidacao.Query.Select(liquidacao.Query.Valor.Sum());
            liquidacao.Query.Where(liquidacao.Query.IdCliente.Equal(idCliente),
                                            liquidacao.Query.Origem.Equal(origem),
                                            liquidacao.Query.DataVencimento.GreaterThanOrEqual(dataInicio),
                                            liquidacao.Query.DataVencimento.LessThanOrEqual(dataFim),
                                            liquidacao.Query.Valor.LessThan(0));
            liquidacao.Query.Load();

            decimal valor = 0;
            if (liquidacao.Valor.HasValue)
            {
                valor = liquidacao.Valor.Value;
            }

            return valor;
        }
    }
}
