﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Reflection;

namespace Financial.ContaCorrente.Enums {

    public static class OrigemLancamentoLiquidacao {
        
        public static class Bolsa{
            /* Nenhuma */
            public const int None = 0;
            /* TipoOperacao = Compra,CompraDaytrade, Origem = Primaria, TipoMercado = Vista */
            public const int CompraAcoes = 1;
            /* TipoOperacao = Venda,VendaDaytrade, Origem = Primaria, TipoMercado = Vista */
            public const int VendaAcoes = 2;
            /* TipoOperacao = Compra,CompraDaytrade, Origem = Primaria, TipoMercado = OpcaoCompra, OpcaoVenda */
            public const int CompraOpcoes = 3;
            /* TipoOperacao = Venda,VendaDaytrade, Origem = Primaria, TipoMercado = OpcaoCompra, OpcaoVenda */
            public const int VendaOpcoes = 4;
            /* TipoOperacao = Compra, Origem = ExercicioOpcao, TipoMercado = Vista */
            public const int ExercicioCompra = 5;
            /* TipoOperacao = Venda, Origem = ExercicioOpcao, TipoMercado = Vista */
            public const int ExercicioVenda = 6;
            public const int CompraTermo = 7;
            public const int VendaTermo = 8;
            public const int AntecipacaoTermo = 9;
            public const int EmprestimoDoado = 10;
            public const int EmprestimoTomado = 11;
            public const int AntecipacaoEmprestimo = 12;
            public const int AjusteOperacaoFuturo = 13;            
            public const int AjustePosicaoFuturo = 14;
            public const int LiquidacaoFinalFuturo = 15;
            public const int DespesasTaxas = 16;
            public const int Corretagem = 17;
            public const int Dividendo = 18;
            public const int JurosCapital = 19;
            public const int Rendimento = 20;
            public const int RestituicaoCapital = 21;
            public const int CreditoFracoesAcoes = 22;
            public const int IRProventos = 23;
            public const int OutrosProventos = 24;
            public const int Amortizacao = 25; //para FII
            public const int TaxaCustodia = 40;
            public const int DividendoDistribuicao = 41;
            public const int Outros = 100;

            /// <summary>
            /// Valores possiveis para o enum
            /// </summary>
            /// <returns></returns>
            public static List<int> Values() {
                List<int> valoresPossiveis = new List<int>();
                valoresPossiveis.Add(None);
                valoresPossiveis.Add(CompraAcoes);
                valoresPossiveis.Add(VendaAcoes);
                valoresPossiveis.Add(CompraOpcoes);
                valoresPossiveis.Add(VendaOpcoes);
                valoresPossiveis.Add(ExercicioCompra);
                valoresPossiveis.Add(ExercicioVenda);
                valoresPossiveis.Add(CompraTermo);
                valoresPossiveis.Add(VendaTermo);
                valoresPossiveis.Add(AntecipacaoTermo);
                valoresPossiveis.Add(EmprestimoDoado);
                valoresPossiveis.Add(EmprestimoTomado);
                valoresPossiveis.Add(AntecipacaoEmprestimo);
                valoresPossiveis.Add(AjusteOperacaoFuturo);
                valoresPossiveis.Add(AjustePosicaoFuturo);
                valoresPossiveis.Add(LiquidacaoFinalFuturo);
                valoresPossiveis.Add(DespesasTaxas);
                valoresPossiveis.Add(Corretagem);
                valoresPossiveis.Add(Dividendo);
                valoresPossiveis.Add(DividendoDistribuicao);
                valoresPossiveis.Add(JurosCapital);
                valoresPossiveis.Add(Rendimento);
                valoresPossiveis.Add(RestituicaoCapital);
                valoresPossiveis.Add(CreditoFracoesAcoes);
                valoresPossiveis.Add(IRProventos);
                valoresPossiveis.Add(OutrosProventos);
                valoresPossiveis.Add(TaxaCustodia); 
                valoresPossiveis.Add(Outros);

                //TODO: comparar numero de variaveis com numero de elementos no vetor
                /*
                Type tipo = typeof(Financial.ContaCorrente.Enums.OrigemLancamentoLiquidacao.Bolsa);
                FieldInfo[] variaveisPublic = tipo.GetFields(BindingFlags.Public| BindingFlags.Instance |
                                       BindingFlags.DeclaredOnly);
                int numeroDeElementos = variaveisPublic.Length;

                //Assert.fail
                */
                return valoresPossiveis;
            }
        }

        public static class BMF
        {   // Nenhuma 
            public const int None = 200;         
            /* TipoOperacao = Compra,CompraDaytrade, Origem = Primaria, TipoMercado = Vista */
            public const int CompraVista = 201;
            /* TipoOperacao = Venda,VendaDaytrade, Origem = Primaria, TipoMercado = Vista */
            public const int VendaVista = 202;            
            /* TipoOperacao = Compra,CompraDaytrade, Origem = Primaria, TipoMercado = OpcaoDisponivel, OpcaoFuturo */
            public const int CompraOpcoes = 203;
            /* TipoOperacao = Venda,VendaDaytrade, Origem = Primaria, TipoMercado = OpcaoDisponivel, OpcaoFuturo */
            public const int VendaOpcoes = 204;            
            /* TipoOperacao = Compra, Origem = ExercicioOpcao, (TipoMercado != OpcaoDisponivel,OpcaoFuturo) */
            public const int ExercicioCompra = 205;
            /* TipoOperacao = Venda, Origem = ExercicioOpcao, (TipoMercado != OpcaoDisponivel,OpcaoFuturo) */
            public const int ExercicioVenda = 206;            
            public const int DespesasTaxas = 220;
            public const int Corretagem = 221;
            public const int Emolumento = 222;
            public const int Registro = 223;
            public const int OutrosCustos = 224;
            public const int AjustePosicao = 250;
            public const int AjusteOperacao = 251;
            public const int TaxaPermanencia = 260;
            public const int TaxaClearing = 270;
            public const int Outros = 300;

            /// <summary>
            /// Valores possiveis para o enum
            /// </summary>
            /// <returns></returns>
            /// 
            public static List<int> Values() {
                List<int> valoresPossiveis = new List<int>();
                valoresPossiveis.Add(None);
                valoresPossiveis.Add(CompraVista);
                valoresPossiveis.Add(VendaVista);
                valoresPossiveis.Add(CompraOpcoes);
                valoresPossiveis.Add(VendaOpcoes);
                valoresPossiveis.Add(ExercicioCompra);
                valoresPossiveis.Add(ExercicioVenda);
                valoresPossiveis.Add(DespesasTaxas);
                valoresPossiveis.Add(Corretagem);
                valoresPossiveis.Add(AjustePosicao);
                valoresPossiveis.Add(AjusteOperacao);
                valoresPossiveis.Add(Outros);

                return valoresPossiveis;
            }
        }

        public static class RendaFixa
        {   // Nenhuma 
            public const int None = 500;
            public const int CompraFinal = 501;
            public const int VendaFinal = 502;
            public const int CompraRevenda = 503;
            public const int VendaRecompra = 504;
            public const int ExercicioOpcao = 601;
            public const int NetOperacaoCasada = 505;
            public const int Vencimento = 510;
            public const int Revenda = 511;
            public const int Recompra = 512;
            public const int Juros = 520;
            public const int Amortizacao = 521;
            public const int IR = 530;
            public const int IOF = 531;
            public const int Corretagem = 540;
            public const int TaxaCustodia = 550;
            public const int Outros = 600;

            /// <summary>
            /// Valores possiveis para o enum
            /// </summary>
            /// <returns></returns>
            /// 
            public static List<int> Values()
            {
                List<int> valoresPossiveis = new List<int>();
                valoresPossiveis.Add(None);
                valoresPossiveis.Add(CompraFinal);
                valoresPossiveis.Add(VendaFinal);
                valoresPossiveis.Add(CompraRevenda);
                valoresPossiveis.Add(VendaRecompra);
                valoresPossiveis.Add(NetOperacaoCasada);
                valoresPossiveis.Add(Vencimento);
                valoresPossiveis.Add(Revenda);
                valoresPossiveis.Add(Recompra);
                valoresPossiveis.Add(Juros);
                valoresPossiveis.Add(Amortizacao);
                valoresPossiveis.Add(IR);
                valoresPossiveis.Add(IOF);

                return valoresPossiveis;
            }
        }

        public static class Swap
        {
            public const int LiquidacaoVencimento = 701;
            public const int LiquidacaoAntecipacao = 702;
            public const int DespesasTaxas = 703;

            /// <summary>
            /// Valores possiveis para o enum
            /// </summary>
            /// <returns></returns>
            /// 
            public static List<int> Values()
            {
                List<int> valoresPossiveis = new List<int>();
                valoresPossiveis.Add(LiquidacaoVencimento);
                valoresPossiveis.Add(LiquidacaoAntecipacao);
                valoresPossiveis.Add(DespesasTaxas);
                
                return valoresPossiveis;
            }
        }

        public static class Provisao
        {
            public const int TaxaAdministracao = 801;
            public const int PagtoTaxaAdministracao = 802;
            public const int TaxaGestao = 810;
            public const int PagtoTaxaGestao = 811;
            public const int TaxaPerformance = 820;
            public const int PagtoTaxaPerformance = 821;
            public const int CPMF = 830;
            public const int TaxaFiscalizacaoCVM = 840;
            public const int PagtoTaxaFiscalizacaoCVM = 841;
            public const int ProvisaoOutros = 890;
            public const int PagtoProvisaoOutros = 891;
            public const int TaxaCustodia = 851;
            public const int PagtoTaxaCustodia = 852;

            /// <summary>
            /// Valores possiveis para o enum
            /// </summary>
            /// <returns></returns>
            public static List<int> Values() {
                List<int> valoresPossiveis = new List<int>();
                valoresPossiveis.Add(TaxaAdministracao);
                valoresPossiveis.Add(PagtoTaxaAdministracao);
                valoresPossiveis.Add(TaxaGestao);
                valoresPossiveis.Add(PagtoTaxaGestao);
                valoresPossiveis.Add(TaxaPerformance);
                valoresPossiveis.Add(PagtoTaxaPerformance);
                valoresPossiveis.Add(CPMF);
                valoresPossiveis.Add(TaxaFiscalizacaoCVM);
                valoresPossiveis.Add(PagtoTaxaFiscalizacaoCVM);
                valoresPossiveis.Add(ProvisaoOutros);
                valoresPossiveis.Add(PagtoProvisaoOutros);

                return valoresPossiveis;
            }
        }

        public static class Fundo
        {   // Nenhuma 
            public const int None = 1000;
            public const int Aplicacao = 1001;
            public const int Resgate = 1002;
            public const int IRResgate = 1003;
            public const int IOFResgate = 1004;
            public const int PfeeResgate = 1005;
            public const int ComeCotas = 1006;
            public const int AplicacaoConverter = 1010;
            public const int TransferenciaSaldo = 1020;
            public const int Amortizacao = 1021;
            public const int Juros = 1022;
            public const int JurosIR = 1023;
            public const int Dividendo = 1024;
            
            /// <summary>
            /// Valores possiveis para o enum
            /// </summary>
            /// <returns></returns>
            public static List<int> Values() {
                List<int> valoresPossiveis = new List<int>();
                valoresPossiveis.Add(None);
                valoresPossiveis.Add(Aplicacao);
                valoresPossiveis.Add(Resgate);
                valoresPossiveis.Add(IRResgate);
                valoresPossiveis.Add(IOFResgate);
                valoresPossiveis.Add(PfeeResgate);
                valoresPossiveis.Add(ComeCotas);
                valoresPossiveis.Add(AplicacaoConverter);
                valoresPossiveis.Add(TransferenciaSaldo);
            
                return valoresPossiveis;
            }         
        }

        public static class Cotista
        {   // Nenhuma 
            public const int None = 1100;
            public const int Aplicacao = 1101;
            public const int Resgate = 1102;
            public const int IRResgate = 1103;
            public const int IOFResgate = 1104;
            public const int PfeeResgate = 1105;
            public const int ComeCotas = 1106;
            public const int AplicacaoConverter = 1110;
            public const int Amortizacao = 1111;
            public const int Juros = 1112;
            public const int JurosIR = 1113;
            public const int Dividendo = 1114;
            
            /// <summary>
            /// Valores possiveis para o enum
            /// </summary>
            /// <returns></returns>
            public static List<int> Values()
            {
                List<int> valoresPossiveis = new List<int>();
                valoresPossiveis.Add(None);
                valoresPossiveis.Add(Aplicacao);
                valoresPossiveis.Add(Resgate);
                valoresPossiveis.Add(IRResgate);
                valoresPossiveis.Add(IOFResgate);
                valoresPossiveis.Add(PfeeResgate);
                valoresPossiveis.Add(ComeCotas);
                valoresPossiveis.Add(AplicacaoConverter);
                valoresPossiveis.Add(Amortizacao);
                valoresPossiveis.Add(Juros);
                valoresPossiveis.Add(JurosIR);
                valoresPossiveis.Add(Dividendo);
                
                return valoresPossiveis;
            }
        }

        public static class IR {
            public const int IRFonteOperacao = 2000;
            public const int IRFonteDayTrade = 2001;
            public const int IRRendaVariavel = 2002;

            /// <summary>
            /// Valores possiveis para o enum
            /// </summary>
            /// <returns></returns>
            public static List<int> Values() {
                List<int> valoresPossiveis = new List<int>();
                valoresPossiveis.Add(IRFonteOperacao);
                valoresPossiveis.Add(IRFonteDayTrade);
                valoresPossiveis.Add(IRRendaVariavel);

                return valoresPossiveis;
            }         
        }

        public static class Margem
        {
            public const int ChamadaMargem = 3000;
            public const int DevolucaoMargem = 3001;
        }

        public static class Cambio
        {
            public const int PrincipalOrigem = 4000;
            public const int PrincipalDestino = 4001;
            public const int Tributos = 4002;
            public const int Custos = 4003;
        }

        public const int AjusteCompensacaoCota = 5000;
        public const int AjusteCompensacaoCotaEvento = 5001;
        public const int Outros = 100000;
        public const int CarteiraGerencial = 100001;
    }

    public enum FonteLancamentoLiquidacao {
        Interno = 1,
        Manual = 2,
        Sinacor = 3,
        ArquivoCMDF = 4
    }

    public enum SituacaoLancamentoLiquidacao {
        Normal = 1,
        Pendente = 2,
        Compensacao = 3
    }

    public enum VizualizacaoOperacaoBolsa {
        Analitico = 1,
        Consolidado = 2
    }

    public enum VizualizacaoOperacaoBMF {
        Analitico = 1,
        Consolidado = 2        
    }

    public static class LiquidacaoMercado {
        // Data da possivel mudança - 01/01/3000
        private static DateTime dataMudanca = new DateTime(3000,1,1);
        //Acoes liquidam em D+3 em relacao ao pregao
        private const int acoes = 3;
        //Opcoes Bovespa liquidam em D+1 em relacao ao pregao
        private const int opcoesBolsa = 1;
        //Futuros Bovespa liquidam em D+1 em relacao ao pregao
        private const int futurosBolsa = 1;
        //Futuros BMF liquidam em D+1 em relacao ao pregao
        private const int futurosBMF = 1;
        //Opcoes BMF liquidam em D+1 em relacao ao pregao
        private const int opcoesBMF = 1;
        //Juros/Emol de Emprestimos Bolsa liquidam D+1 em relacao a data de termino
        private const int emprestimoBolsa = 1;
        
        # region GetAcoes
        private static int GetAcoes(DateTime data) {
            if(data.CompareTo(dataMudanca) < 0) {
                return acoes; 
            }
            else {
                return acoes;                
            }
        }

        public static int Acoes {
            get {
                DateTime dataHoje = DateTime.Today;
                return LiquidacaoMercado.GetAcoes(dataHoje);
            }
        }
        #endregion

        # region GetOpcoesBolsa
        private static int GetOpcoesBolsa(DateTime data) {
            if(data.CompareTo(dataMudanca) < 0) {
                return opcoesBolsa; 
            }
            else {
                return opcoesBolsa;                
            }
        }

        public static int OpcoesBolsa {
            get {
                DateTime dataHoje = DateTime.Today;
                return LiquidacaoMercado.GetOpcoesBolsa(dataHoje);
            }
        }
        #endregion

        # region GetFuturosBolsa
        private static int GetFuturosBolsa(DateTime data) {
            if(data.CompareTo(dataMudanca) < 0) {
                return futurosBolsa; 
            }
            else {
                return futurosBolsa;                
            }
        }

        public static int FuturosBolsa {
            get {
                DateTime dataHoje = DateTime.Today;
                return LiquidacaoMercado.GetFuturosBolsa(dataHoje);
            }
        }
        #endregion

        # region GetFuturosBMF
        private  static int GetFuturosBMF(DateTime data) {
            if(data.CompareTo(dataMudanca) < 0) {
                return futurosBMF; 
            }
            else {
                return futurosBMF;                
            }
        }

        public static int FuturosBMF {
            get {
                DateTime dataHoje = DateTime.Today;
                return LiquidacaoMercado.GetFuturosBMF(dataHoje);
            }
        }
        #endregion

        # region GetOpcoesBMF
        private static int GetOpcoesBMF(DateTime data) {
            if(data.CompareTo(dataMudanca) < 0) {
                return opcoesBMF; 
            }
            else {
                return opcoesBMF;                
            }
        }

        public static int OpcoesBMF {
            get {
                DateTime dataHoje = DateTime.Today;
                return LiquidacaoMercado.GetOpcoesBMF(dataHoje);
            }
        }
        #endregion

        # region GetEmprestimoBolsa
        private static int GetEmprestimoBolsa(DateTime data) {
            if(data.CompareTo(dataMudanca) < 0) {
                return emprestimoBolsa; 
            }
            else {
                return emprestimoBolsa;                
            }
        }

        public static int EmprestimoBolsa {
            get {
                DateTime dataHoje = DateTime.Today;
                return LiquidacaoMercado.GetEmprestimoBolsa(dataHoje);
            }
        }

        #endregion
    }

    public enum TipoLiquidacao
    {
        Adm = 1,
        Reserva = 2
    }

    public enum SinalValorLiquidacao {
        Entrada = 1,
        Retirada = 2,
        Entrada_Retirada = 3
    }
}
