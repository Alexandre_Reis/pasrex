﻿using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Text;
using System.Data;

using EntitySpaces.Interfaces;
using EntitySpaces.Core;
using System.Reflection;

namespace Financial.ContaCorrente.Relatorio {
    // Deprecated
    public partial class ExtratoContaCorrenteCollection {
        /*
        private DataTable dataTable = new DataTable(); // DataTable usado no relatorio

        // colunas DataTable      
        #region ColunasDataTable
        private class ColunasDataTable {
            public string IdCliente = "IdCliente";
            public string IdConta = "IdConta";
            public string DataVencimento = "DataVencimento";
            public string DataLancamento = "DataLancamento";
            public string Descricao = "Descricao";
            public string Valor = "Valor";
        }
        #endregion

        private ColunasDataTable colunasDataTable = new ColunasDataTable();

        /// <summary>
        /// Inicializa o DataTable com os mesmos atributos da classe
        /// </summary>         
        private void InicializaDataTablePrivate(esColumnMetadataCollection col) {
            foreach (esColumnMetadata extEsCol in col) {
                if (!this.dataTable.Columns.Contains(extEsCol.Name)) {
                    this.dataTable.Columns.Add(new DataColumn(extEsCol.Name, extEsCol.Type));
                }
            }
        }

        /// <summary>
        ///     Método usado para preencher o DataTable
        /// </summary>
        /// <returns></returns>
        public DataTable FillDataTable(DateTime dataInicio, DateTime dataFim, int idCliente) {
           
            #region Merge das Collections
            LiquidacaoCollection liquidacao1 = new LiquidacaoCollection();
            liquidacao1.Query.Select(liquidacao1.Query.IdCliente, liquidacao1.Query.IdConta,
                         liquidacao1.Query.DataVencimento, liquidacao1.Query.DataLancamento,
                         liquidacao1.Query.Descricao, liquidacao1.Query.Valor)
                  .Where(liquidacao1.Query.DataLancamento.Between(dataInicio, dataFim),
                         liquidacao1.Query.IdCliente == idCliente);
            liquidacao1.Query.Load();

            // Salva todos os idContas num array
            List<int> idContas = new List<int>();
            for (int i = 0; i < liquidacao1.Count; i++) {
                int idConta = liquidacao1[i].IdConta.Value;
                if (!idContas.Contains(idConta)) {
                    idContas.Add(idConta);
                }
            }

            // ContaCorrente para um determinado cliente que não está na tabela de liquidacao
            Financial.Investidor.ContaCorrenteCollection contaCorrenteCollection = new Financial.Investidor.ContaCorrenteCollection();
            contaCorrenteCollection.QueryReset();
            contaCorrenteCollection.Query
                 .Select(contaCorrenteCollection.Query.IdCliente, contaCorrenteCollection.Query.IdConta,
                         contaCorrenteCollection.Query.TipoConta)
                  .Where(contaCorrenteCollection.Query.IdCliente == idCliente,
                          contaCorrenteCollection.Query.IdConta.NotIn(idContas)
                        );
            contaCorrenteCollection.Query.Load();

            //             
            LiquidacaoCollection liquidacaoMerge = new LiquidacaoCollection();

            for (int i = 0; i < contaCorrenteCollection.Count; i++) {
                Liquidacao liquidacao = new Liquidacao();
                liquidacao.IdCliente = contaCorrenteCollection[i].IdCliente;
                liquidacao.IdConta = contaCorrenteCollection[i].IdConta;
                liquidacao.DataVencimento = null;
                liquidacao.DataLancamento = null;
                liquidacao.Descricao = "";
                liquidacaoMerge.AttachEntity(liquidacao);
            }

            liquidacaoMerge.Combine(liquidacao1);
            #endregion

            // Metadata da Collection
            esColumnMetadataCollection cols = liquidacaoMerge.es.Meta.Columns;
            
            // Cria um dataTable com a mesma estrutura da collection
            this.InicializaDataTablePrivate(cols);

            // Copia dados da collection para o DataTable
            for (int i = 0; i < liquidacaoMerge.Count; i++) {
                DataRow dataRow = this.dataTable.NewRow();                
                #region Preenche Valores
                dataRow[this.colunasDataTable.IdCliente] = liquidacaoMerge[i].IdCliente;
                dataRow[this.colunasDataTable.IdConta] = liquidacaoMerge[i].IdConta;
                dataRow[this.colunasDataTable.DataVencimento] = liquidacaoMerge[i].DataVencimento;
                dataRow[this.colunasDataTable.DataLancamento] = liquidacaoMerge[i].DataLancamento;
                dataRow[this.colunasDataTable.Descricao] = liquidacaoMerge[i].Descricao;
                dataRow[this.colunasDataTable.Valor] = liquidacaoMerge[i].Valor;
                #endregion
                this.dataTable.Rows.Add(dataRow);
            }                                  
            //
            return this.dataTable;
        }*/
    }
}

