﻿using log4net;
using Financial.ContaCorrente.Enums;
using System.Collections.Generic;
using System;
using Financial.Util;
using Financial.Common.Enums;
using System.Text;
using System.Data.SqlClient;
using EntitySpaces.Interfaces;
using EntitySpaces.Core;
using Financial.ContaCorrente;
using Financial.Fundo.Enums;
using Financial.Investidor.Enums;
using Financial.Investidor.Controller;
using Financial.Investidor;
using Financial.Bolsa;
using Financial.Bolsa.Exceptions;
using Financial.Interfaces.Sinacor;
using Financial.Fundo;

namespace Financial.ContaCorrente.Controller
{
    /// <summary>
    /// 
    /// </summary>
    public class ControllerContaCorrente
    {
        /// <summary>
        /// Reprocessa o saldo de caixa abertura e fechamento, 
        /// levando em conta a data e o tipo de reprocessamento passados.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="data"></param>
        /// <param name="tipoReprocessamento">Verificar se é necessario</param>
        /// thows ArgumentException se tipoReprocessamento for diferente dos valores do 
        /// Enum TipoReprocessamentoBMF
        public void ReprocessaPosicaoSaldoCaixa(int idCliente, DateTime data, TipoReprocessamento tipoReprocessamento)
        {
            SaldoCaixaCollection saldoCaixaCollectionDeletar = new SaldoCaixaCollection();
            saldoCaixaCollectionDeletar.DeletaSaldoMaiorData(idCliente, data);

            if (tipoReprocessamento != TipoReprocessamento.Fechamento)
            {
                SaldoCaixaCollection saldoCaixaCollection = new SaldoCaixaCollection();
                saldoCaixaCollection.BuscaSaldoCaixa(idCliente, data);

                for (int i = 0; i < saldoCaixaCollection.Count; i++)
                {
                    SaldoCaixa saldoCaixa = saldoCaixaCollection[i];
                    saldoCaixa.SaldoFechamento = null;
                }
                saldoCaixaCollection.Save();
            }
        }

        /// <summary>
        /// Reprocessa toda a parte de liquidacao, 
        /// levando em conta a data e o tipo de reprocessamento passados.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="data"></param>
        /// <param name="tipoReprocessamento">Verificar se é necessario</param>
        /// <param name="mantemLancamentosFuturos"></param>
        /// thows ArgumentException se tipoReprocessamento for diferente dos valores do 
        /// Enum TipoReprocessamentoBMF
        public void ReprocessaPosicaoLiquidacao(int idCliente, DateTime data, TipoReprocessamento tipoReprocessamento, bool mantemLancamentosFuturos)
        {
            if (tipoReprocessamento != TipoReprocessamento.CalculoDiario && mantemLancamentosFuturos)
            {
                #region Copia de LiquidacaoHistorico para LiquidacaoFuturo
                LiquidacaoHistorico liquidacaoHistorico = new LiquidacaoHistorico();
                liquidacaoHistorico.GeraLiquidacaoFuturo(idCliente, data);
                #endregion

                Cliente cliente = new Cliente();
                List<esQueryItem> campos = new List<esQueryItem>();
                campos.Add(cliente.Query.DataDia);
                campos.Add(cliente.Query.Status);
                cliente.LoadByPrimaryKey(campos, idCliente);

                if (cliente.Status.Value != (byte)StatusCliente.Divulgado)
                {
                    #region Copia de Liquidacao para LiquidacaoFuturo
                    Liquidacao liquidacao = new Liquidacao();
                    liquidacao.GeraLiquidacaoFuturo(idCliente, cliente.DataDia.Value, data);
                    #endregion
                }
            }

            #region Delete Liquidacao
            if (tipoReprocessamento == TipoReprocessamento.CalculoDiario)
            {
                LiquidacaoCollection liquidacaoCollectionDeletar = new LiquidacaoCollection();
                liquidacaoCollectionDeletar.DeletaLiquidacaoInterno(idCliente, data);
            }
            else
            {
                LiquidacaoCollection liquidacaoCollectionDeletar = new LiquidacaoCollection();
                liquidacaoCollectionDeletar.DeletaLiquidacao(idCliente, data);
            }
            #endregion

            #region Inserts em Liquidacao a partir de LiquidacaoHistorico ou LiquidacaoAbertura
            //Para SqlServer precisa dar um tratamento especial para a PK Identity
            // Nome da Configuração Default SQL
            if (esConfigSettings.DefaultConnection.Provider == "EntitySpaces.SqlClientProvider")
            {
                using (esTransactionScope scope = new esTransactionScope())
                {
                    esUtility u = new esUtility();
                    string sql = " set identity_insert Liquidacao on ";
                    u.ExecuteNonQuery(esQueryType.Text, sql, "");

                    if (tipoReprocessamento == TipoReprocessamento.CalculoDiario)
                    {
                        #region Insert Liquidacao a partir de LiquidacaoAbertura
                        LiquidacaoCollection liquidacaoCollection = new LiquidacaoCollection();
                        try
                        {
                            liquidacaoCollection.InsereLiquidacaoAberturaSomenteInternos(idCliente, data);
                        }
                        catch (SqlException e)
                        {
                            Console.WriteLine("Insert Liquidacao");
                            Console.WriteLine(e.LineNumber);
                            Console.WriteLine(e.Message);
                            Console.WriteLine(e.StackTrace);
                            Console.WriteLine(e.ToString());
                        }
                        #endregion
                    }
                    else if (tipoReprocessamento == TipoReprocessamento.Fechamento)
                    {
                        #region Insert Liquidacao a partir de LiquidacaoHistorico
                        LiquidacaoCollection liquidacaoCollection = new LiquidacaoCollection();
                        try
                        {
                            liquidacaoCollection.InsereLiquidacaoHistorico(idCliente, data);
                        }
                        catch (SqlException e)
                        {
                            Console.WriteLine("Insert Liquidacao");
                            Console.WriteLine(e.LineNumber);
                            Console.WriteLine(e.Message);
                            Console.WriteLine(e.StackTrace);
                            Console.WriteLine(e.ToString());
                        }
                        #endregion
                    }

                    u = new esUtility();
                    sql = " set identity_insert Liquidacao off ";
                    u.ExecuteNonQuery(esQueryType.Text, sql, "");

                    scope.Complete();
                }
            }
            else
            {
                //TODO ESCREVER NA UNHA O SQL, POIS O ORACLE NÃO PERMITE DESLIGAR A SEQUENCE
                if (tipoReprocessamento == TipoReprocessamento.Fechamento)
                {
                    #region Insert Liquidacao a partir de LiquidacaoHistorico
                    LiquidacaoCollection liquidacaoCollection = new LiquidacaoCollection();
                    try
                    {
                        liquidacaoCollection.InsereLiquidacaoHistorico(idCliente, data);
                    }
                    catch (SqlException e)
                    {
                        Console.WriteLine("Insert Liquidacao");
                        Console.WriteLine(e.LineNumber);
                        Console.WriteLine(e.Message);
                        Console.WriteLine(e.StackTrace);
                        Console.WriteLine(e.ToString());
                    }
                    #endregion
                }
            }
            #endregion

            this.DeletaPosicoesAbertura(idCliente, data);

            this.DeletaPosicoesHistorico(idCliente, data);

        }

        /// <summary>
        /// Calcula o saldo de abertura.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="data"></param>
        public void ExecutaAbertura(int idCliente, DateTime data)
        {
            SaldoCaixa saldoCaixa = new SaldoCaixa();
            saldoCaixa.CalculaSaldoCaixaAbertura(idCliente, data);

            Liquidacao liquidacao = new Liquidacao();

            //A cópia só é gerada para registros com data de vencimento >= que a data passada
            //e data lancamento <= que a data passada
            liquidacao.GeraLiquidacaoAbertura(idCliente, data);

            //Caso exista algum lancamento em LiquidacaoFuturo, estes substituem os lancamentos manuais (com origem = Outros) de Liquidacao
            liquidacao.CarregaLiquidacaoFuturo(idCliente, data);
        }

        /// <summary>
        /// Método auxiliar específico para o caso de fundos com cota de abertura e lancamentos de provisão, adm e pfee.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="data"></param>
        public void ExecutaAberturaLancamentoNaData(int idCliente, DateTime data)
        {
            Liquidacao liquidacao = new Liquidacao();

            //A cópia só é gerada para registros com data de vencimento >= que a data passada
            //e data lancamento = que a data passada, com Fonte = Interno
            liquidacao.GeraLiquidacaoAberturaLancamentoNaData(idCliente, data);
        }

        /// <summary>
        /// Backup de Liquidacao para LiquidacaoHistorico.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="data"></param>
        public void ExecutaDivulgacao(int idCliente, DateTime data)
        {
            this.DeletaPosicoesHistorico(idCliente, data);

            Liquidacao liquidacao = new Liquidacao();

            //O Backup só é gerado para registros com data de vencimento >= que a data passada
            //e data lancamento <= que a data passada
            liquidacao.GeraBackup(idCliente, data);
        }

        /// <summary>
        /// Calcula o saldo de caixa de fechamento.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="data"></param>
        /// <param name="parametrosProcessamento"></param>
        public void ExecutaFechamento(int idCliente, DateTime data, ParametrosProcessamento parametrosProcessamento)
        {
            this.ReprocessaPosicaoSaldoCaixa(idCliente, data, TipoReprocessamento.CalculoDiario);

            if (parametrosProcessamento.ProcessaCambio)
            {
                OperacaoCambio operacaoCambio = new OperacaoCambio();
                operacaoCambio.ProcessaOperacao(idCliente, data);                
            }

            if (parametrosProcessamento.ListaTabelaInterfaceCliente.Contains((int)ListaInterfaceCliente.ContaCorrente.SinacorFull))
            {
                //Integra todos os lançamentos de c/c com lancamento na data, sem fazer distinção do que é aporte ou resgate de $
                this.IntegraCCSinacorFull(idCliente, data);
            }
            else if (parametrosProcessamento.IntegraCC)
            {
                this.IntegraCCSinacor(idCliente, data);
            }
            
            SaldoCaixa saldoCaixa = new SaldoCaixa();
            saldoCaixa.CalculaSaldoCaixaFechamento(idCliente, data);

            #region Calcula Prazo Médio
            this.CalculaPrazoMedioContaCorrente(idCliente, data);
            #endregion
        }

        /// <summary>
        /// Calcula o saldo de caixa de fechamento, sem precisar de reprocessamento do saldo de abertura.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="data"></param>
        public void ExecutaCalculoSaldoFechamento(int idCliente, DateTime data)
        {
            SaldoCaixa saldoCaixa = new SaldoCaixa();
            saldoCaixa.CalculaSaldoCaixaFechamento(idCliente, data);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="data"></param>
        /// <param name="interno"></param>
        public void DeletaLiquidacao(int idCliente, DateTime data, bool interno)
        {
            LiquidacaoCollection liquidacaoCollectionDeletar = new LiquidacaoCollection();

            if (interno)
            {
                liquidacaoCollectionDeletar.DeletaLiquidacaoInterno(idCliente, data);
            }
            else
            {
                liquidacaoCollectionDeletar.DeletaLiquidacao(idCliente, data);
            }
        }

        /// <summary>
        /// Integra lançamentos de c/c, incluindo lançamentos de aporte, resgate e chamada/devolução de margem.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="data"></param>
        private void IntegraCCSinacor(int idCliente, DateTime data)
        {
            string debug = "ini_integrasinacor:" + idCliente + ";data:" + data;
            try
            {
                LiquidacaoCollection liquidacaoCollectionDeletar = new LiquidacaoCollection();
                liquidacaoCollectionDeletar.Query.Select(liquidacaoCollectionDeletar.Query.IdLiquidacao);
                liquidacaoCollectionDeletar.Query.Where(liquidacaoCollectionDeletar.Query.IdCliente.Equal(idCliente),
                                                 liquidacaoCollectionDeletar.Query.DataLancamento.Equal(data),
                                                 liquidacaoCollectionDeletar.Query.Fonte.Equal((byte)FonteLancamentoLiquidacao.Sinacor));
                liquidacaoCollectionDeletar.Query.Load();
                liquidacaoCollectionDeletar.MarkAllAsDeleted();
                liquidacaoCollectionDeletar.Save();

                debug += ";apos delecao liquidacao collection";

                Cliente cliente = new Cliente();
                List<esQueryItem> campos = new List<esQueryItem>();
                campos.Add(cliente.Query.IdTipo);
                cliente.LoadByPrimaryKey(campos, idCliente);

                debug += ";apos load cliente";

                List<int> codigosCliente = new List<int>();
                ClienteBolsa clienteBolsa = new ClienteBolsa();
                codigosCliente = clienteBolsa.RetornaCodigosBovespa(idCliente);

                debug += ";apos retorna codigosCliente - count:" + codigosCliente.Count;

                string listaCodigosCC = ParametrosConfiguracaoSistema.Integracoes.IntegracaoCodigosCC;
                ClienteInterface clienteInterface = new ClienteInterface();
                if (clienteInterface.LoadByPrimaryKey(idCliente))
                {
                    debug += ";carreguei cliente interface:" + clienteInterface.InterfaceSinacorCC;
                    if (clienteInterface.InterfaceSinacorCC != "")
                    {
                        listaCodigosCC = clienteInterface.InterfaceSinacorCC;
                    }
                }

                if (listaCodigosCC != "" && codigosCliente.Count > 0)
                {
                    debug += ";listaCodigosCC != null && codigosCliente.Count > 0";
                    bool isFundo = (cliente.IdTipo.Value == (int)TipoClienteFixo.Clube) ||
                                   (cliente.IdTipo.Value == (int)TipoClienteFixo.Fundo) ||
                                   (cliente.IdTipo.Value == (int)TipoClienteFixo.FDIC);

                    List<int> listaEventos = new List<int>();
                    List<int> listaEventosAporte = new List<int>();
                    List<int> listaEventosResgate = new List<int>();
                    List<int> listaEventosMargem = new List<int>();
                    listaCodigosCC = listaCodigosCC.Replace(",", ";");
                    debug += ";listaCodigosReplace:" + listaCodigosCC;
                    string[] codigos = listaCodigosCC.Split(new Char[] { ';' });
                    debug += ";apos split codigoscount=" + codigos.Length;
                    foreach (string codigo in codigos)
                    {
                        debug += ";codigo:" + codigo;
                        if (codigo.Contains("M"))
                       {
                           debug += ";codigoM";
                        
                            listaEventosMargem.Add(Convert.ToInt32(codigo.Replace("M", "")));
                        }
                        else if (codigo.Contains("+") && !isFundo)
                        {
                            debug += ";codigo+";
                            listaEventosAporte.Add(Convert.ToInt32(codigo.Replace("+", "")));
                        }
                        else if (codigo.Contains("-") && !isFundo)
                        {
                            debug += ";codigo-";
                            listaEventosResgate.Add(Convert.ToInt32(codigo.Replace("-", "")));
                        }
                        else
                        {
                            debug += ";codigoOutro";
                            if ((!isFundo) || (!codigo.Contains("+") && !codigo.Contains("-")))
                            {
                                debug += "codigoOutroInner";
                                listaEventos.Add(Convert.ToInt32(codigo));
                            }
                        }
                    }

                    Liquidacao liquidacao = new Liquidacao();
                    if (listaEventos.Count > 0)
                    {
                        debug += ";IntegraCCSinacorLiquidacao";
                        liquidacao.IntegraCCSinacor(idCliente, codigosCliente, data, listaEventos);
                    }

                    if (listaEventosAporte.Count > 0)
                    {
                        debug += ";IntegraCCSinacorAporte";
                        liquidacao.IntegraCCSinacorAporte(idCliente, codigosCliente, data, listaEventosAporte);
                    }

                    if (listaEventosResgate.Count > 0)
                    {
                        debug += ";IntegraCCSinacorResgate";
                        liquidacao.IntegraCCSinacorResgate(idCliente, codigosCliente, data, listaEventosResgate);
                    }

                    if (listaEventosMargem.Count > 0)
                    {
                        debug += ";IntegraCCSinacorMargem";
                        liquidacao.IntegraCCSinacorMargem(idCliente, codigosCliente, data, listaEventosMargem);
                    }
                }
            }
            catch(Exception e){
                Financial.Security.HistoricoLog historicoLog = new Financial.Security.HistoricoLog();
                historicoLog.InsereHistoricoLog(DateTime.Now,
                                                DateTime.Now,
                                                debug,
                                                "admin",
                                                "",
                                                "",
                                                Financial.Security.Enums.HistoricoLogOrigem.Outros);

                throw e;
            }
        }

        /// <summary>
        /// Integra todos lançamentos de c/c para Liquidacao.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="data"></param>
        private void IntegraCCSinacorFull(int idCliente, DateTime data)
        {
            Cliente cliente = new Cliente();
            List<esQueryItem> campos = new List<esQueryItem>();
            campos.Add(cliente.Query.IdTipo);
            campos.Add(cliente.Query.DataImplantacao);
            cliente.LoadByPrimaryKey(campos, idCliente);

            List<int> codigosCliente = new List<int>();
            ClienteBolsa clienteBolsa = new ClienteBolsa();
            codigosCliente = clienteBolsa.RetornaCodigosBovespa(idCliente);

            decimal saldoCC = 0;
            if (data == cliente.DataImplantacao.Value)
            {
                Tccsaldo tccsaldo = new Tccsaldo();
                tccsaldo.Query.Select(tccsaldo.Query.VlDisponivel.Sum());
                tccsaldo.Query.Where(tccsaldo.Query.CdCliente.In(codigosCliente));
                tccsaldo.Query.Load();

                saldoCC = tccsaldo.VlDisponivel.HasValue ? tccsaldo.VlDisponivel.Value : 0;

                if (saldoCC != 0)
                {
                    //Busca o idContaDefault do cliente
                    Investidor.ContaCorrente contaCorrente = new Investidor.ContaCorrente();
                    int idContaDefault = contaCorrente.RetornaContaDefault(idCliente);
                    //

                    SaldoCaixa saldoCaixa = new SaldoCaixa();
                    if (saldoCaixa.LoadByPrimaryKey(idCliente, data, idContaDefault))
                    {
                        saldoCaixa.SaldoFechamento = saldoCC;
                        saldoCaixa.Save();
                    }
                    else
                    {
                        saldoCaixa = new SaldoCaixa();
                        saldoCaixa.IdCliente = idCliente;
                        saldoCaixa.Data = data;
                        saldoCaixa.IdConta = idContaDefault;
                        saldoCaixa.SaldoAbertura = saldoCC;
                        saldoCaixa.SaldoFechamento = saldoCC;
                        saldoCaixa.Save();
                    }
                }
            }

            LiquidacaoCollection liquidacaoCollectionDeletar = new LiquidacaoCollection();
            liquidacaoCollectionDeletar.Query.Select(liquidacaoCollectionDeletar.Query.IdLiquidacao);
            liquidacaoCollectionDeletar.Query.Where(liquidacaoCollectionDeletar.Query.IdCliente.Equal(idCliente),
                                             liquidacaoCollectionDeletar.Query.DataLancamento.Equal(data),
                                             liquidacaoCollectionDeletar.Query.Fonte.NotEqual((byte)FonteLancamentoLiquidacao.Manual));
            liquidacaoCollectionDeletar.Query.Load();
            liquidacaoCollectionDeletar.MarkAllAsDeleted();
            liquidacaoCollectionDeletar.Save();

            List<int> listaEventosNaoImportar = new List<int>();
            List<int> listaEventosAporte = new List<int>();
            List<int> listaEventosResgate = new List<int>();
            List<int> listaEventosMargem = new List<int>();

            string listaCodigosCC = ParametrosConfiguracaoSistema.Integracoes.IntegracaoCodigosCC;
            ClienteInterface clienteInterface = new ClienteInterface();
            if (clienteInterface.LoadByPrimaryKey(idCliente))
            {
                if (clienteInterface.InterfaceSinacorCC != "")
                {
                    listaCodigosCC = clienteInterface.InterfaceSinacorCC;
                }
            }

            if (listaCodigosCC != "")
            {
                bool isFundo = (cliente.IdTipo.Value == (int)TipoClienteFixo.Clube) ||
                               (cliente.IdTipo.Value == (int)TipoClienteFixo.Fundo) ||
                               (cliente.IdTipo.Value == (int)TipoClienteFixo.FDIC);

                listaCodigosCC = listaCodigosCC.Replace(",", ";");
                string[] codigos = listaCodigosCC.Split(new Char[] { ';' });
                foreach (string codigo in codigos)
                {
                    if (codigo.Contains("M"))
                    {
                        listaEventosMargem.Add(Convert.ToInt32(codigo.Replace("M", "")));
                        listaEventosNaoImportar.Add(Convert.ToInt32(codigo.Replace("M", "")));
                    }
                    else if (codigo.Contains("+") && !isFundo)
                    {
                        listaEventosAporte.Add(Convert.ToInt32(codigo.Replace("+", "")));
                        listaEventosNaoImportar.Add(Convert.ToInt32(codigo.Replace("+", "")));
                    }
                    else if (codigo.Contains("-") && !isFundo)
                    {
                        listaEventosResgate.Add(Convert.ToInt32(codigo.Replace("-", "")));
                        listaEventosNaoImportar.Add(Convert.ToInt32(codigo.Replace("-", "")));
                    }
                }
            }

            Liquidacao liquidacao = new Liquidacao();
            if (listaEventosAporte.Count > 0)
            {
                liquidacao.IntegraCCSinacorAporte(idCliente, codigosCliente, data, listaEventosAporte);
            }

            if (listaEventosResgate.Count > 0)
            {
                liquidacao.IntegraCCSinacorResgate(idCliente, codigosCliente, data, listaEventosResgate);
            }

            if (listaEventosMargem.Count > 0)
            {
                liquidacao.IntegraCCSinacorMargem(idCliente, codigosCliente, data, listaEventosMargem);
            }

            if (codigosCliente.Count > 0)
            {
                liquidacao.IntegraCCSinacorFull(idCliente, codigosCliente, data, listaEventosNaoImportar);
            }
        }

        /// <summary>
        /// Deleta as posicoes de abertura em Liquidacao.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="data"></param>
        public void DeletaPosicoesAbertura(int idCliente, DateTime data)
        {
            LiquidacaoAberturaCollection liquidacaoAberturaCollection = new LiquidacaoAberturaCollection();
            liquidacaoAberturaCollection.DeletaLiquidacaoAberturaDataHistoricoMaior(idCliente, data);
        }

        /// <summary>
        /// Deleta as posicoes de historico em Liquidacao.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="data"></param>
        public void DeletaPosicoesHistorico(int idCliente, DateTime data)
        {
            LiquidacaoHistoricoCollection liquidacaoHistoricoCollection = new LiquidacaoHistoricoCollection();
            liquidacaoHistoricoCollection.DeletaLiquidacaoHistoricoDataHistoricoMaiorIgual(idCliente, data);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="data"></param>
        public void CalculaPrazoMedioContaCorrente(int idCliente, DateTime data)
        {
            PrazoMedioCollection prazoMedioColl = new PrazoMedioCollection();
            prazoMedioColl.DeletaPrazoMedioMaiorIgual(idCliente, data, TipoAtivoAuxiliar.ContaCorrente);

            Carteira carteira = new Carteira();
            carteira.LoadByPrimaryKey(idCliente);
            string calculaPrazoMedio = string.IsNullOrEmpty(carteira.CalculaPrazoMedio) ? string.Empty : carteira.CalculaPrazoMedio;

            if (!calculaPrazoMedio.Equals("S"))
                return;

            SaldoCaixaCollection saldoCaixaColl = new SaldoCaixaCollection();
            SaldoCaixaQuery saldoCaixaQuery = new SaldoCaixaQuery("saldoCaixa");

            saldoCaixaQuery.Select(saldoCaixaQuery.SaldoFechamento.Sum().Coalesce("0"),
                                   saldoCaixaQuery.IdCliente);
            saldoCaixaQuery.Where(saldoCaixaQuery.IdCliente.Equal(idCliente) & saldoCaixaQuery.Data.Equal(data));
            saldoCaixaQuery.GroupBy(saldoCaixaQuery.IdCliente, saldoCaixaQuery.Data);

            if (saldoCaixaColl.Load(saldoCaixaQuery))
            {
                decimal valorPosicao = (saldoCaixaColl[0].SaldoFechamento.Value < 0 ? 0 : saldoCaixaColl[0].SaldoFechamento.Value);
                PrazoMedio prazoMedio = new PrazoMedio();
                prazoMedio.InserePrazoMedio(data, idCliente, "Conta corrente", (int)TipoAtivoAuxiliar.ContaCorrente, valorPosicao, 1, null);

            }
        }
    }
}
