/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 28/03/2016 14:36:16
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;


using EntitySpaces.Interfaces;
using EntitySpaces.Core;





































		



			
					


		

		
		
		

		

























		






	




using Financial.Common;
using Financial.Investidor;














				
		
		




		








		
				
				












	

















					
								
											
	










		





		




				
				








				




		

		
		
		
		
		





namespace Financial.ContaCorrente
{

	[Serializable]
	abstract public class esLiquidacaoHistoricoCollection : esEntityCollection
	{
		public esLiquidacaoHistoricoCollection()
		{

		}

		protected override string GetCollectionName()
		{
			return "LiquidacaoHistoricoCollection";
		}

		#region Query Logic
		protected void InitQuery(esLiquidacaoHistoricoQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntityCollection)this).Connection;
		}

		protected bool OnQueryLoaded(DataTable table)
		{
			this.PopulateCollection(table);
			return (this.RowCount > 0) ? true : false;
		}
		
		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery(query as esLiquidacaoHistoricoQuery);
		}
		#endregion
		
		virtual public LiquidacaoHistorico DetachEntity(LiquidacaoHistorico entity)
		{
			return base.DetachEntity(entity) as LiquidacaoHistorico;
		}
		
		virtual public LiquidacaoHistorico AttachEntity(LiquidacaoHistorico entity)
		{
			return base.AttachEntity(entity) as LiquidacaoHistorico;
		}
		
		virtual public void Combine(LiquidacaoHistoricoCollection collection)
		{
			base.Combine(collection);
		}
		
		new public LiquidacaoHistorico this[int index]
		{
			get
			{
				return base[index] as LiquidacaoHistorico;
			}
		}

		public override Type GetEntityType()
		{
			return typeof(LiquidacaoHistorico);
		}
	}



	[Serializable]
	abstract public class esLiquidacaoHistorico : esEntity
	{
		/// <summary>
		/// Used internally by the entity's DynamicQuery mechanism.
		/// </summary>
		virtual protected esLiquidacaoHistoricoQuery GetDynamicQuery()
		{
			return null;
		}

		public esLiquidacaoHistorico()
		{

		}

		public esLiquidacaoHistorico(DataRow row)
			: base(row)
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.Int32 idLiquidacao, System.DateTime dataHistorico)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idLiquidacao, dataHistorico);
			else
				return LoadByPrimaryKeyStoredProcedure(idLiquidacao, dataHistorico);
		}

		/// <summary>
		/// Loads an entity by primary key
		/// </summary>
		/// <remarks>
		/// EntitySpaces requires primary keys be defined on all tables.
		/// If a table does not have a primary key set,
		/// this method will not compile.
		/// Does not support sqlAcessType. It only works with DynamicQuery
		/// </remarks>
		/// <param name="fieldsToReturn">Fields desired</param>
		public virtual bool LoadByPrimaryKey(List<esQueryItem> fieldsToReturn, System.Int32 idLiquidacao, System.DateTime dataHistorico)
		{
			esQueryItem[] fields = fieldsToReturn.ToArray();			
			esLiquidacaoHistoricoQuery query = this.GetDynamicQuery();
			query
			    .Select(fields)
			    .Where(query.IdLiquidacao == idLiquidacao, query.DataHistorico == dataHistorico);
		
			return query.Load();
		}
		
		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.Int32 idLiquidacao, System.DateTime dataHistorico)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idLiquidacao, dataHistorico);
			else
				return LoadByPrimaryKeyStoredProcedure(idLiquidacao, dataHistorico);
		}

		private bool LoadByPrimaryKeyDynamic(System.Int32 idLiquidacao, System.DateTime dataHistorico)
		{
			esLiquidacaoHistoricoQuery query = this.GetDynamicQuery();
			query.Where(query.IdLiquidacao == idLiquidacao, query.DataHistorico == dataHistorico);
			return query.Load();
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.Int32 idLiquidacao, System.DateTime dataHistorico)
		{
			esParameters parms = new esParameters();
			parms.Add("IdLiquidacao",idLiquidacao);			parms.Add("DataHistorico",dataHistorico);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		
		
		#region Properties
		
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}

		public override void SetProperty(string name, object value)
		{
			if(this.Row == null) this.AddNew();
			
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value.GetType().ToString() == "System.String")
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "IdLiquidacao": this.str.IdLiquidacao = (string)value; break;							
						case "DataHistorico": this.str.DataHistorico = (string)value; break;							
						case "IdCliente": this.str.IdCliente = (string)value; break;							
						case "DataLancamento": this.str.DataLancamento = (string)value; break;							
						case "DataVencimento": this.str.DataVencimento = (string)value; break;							
						case "Descricao": this.str.Descricao = (string)value; break;							
						case "Valor": this.str.Valor = (string)value; break;							
						case "Situacao": this.str.Situacao = (string)value; break;							
						case "Origem": this.str.Origem = (string)value; break;							
						case "Fonte": this.str.Fonte = (string)value; break;							
						case "IdAgente": this.str.IdAgente = (string)value; break;							
						case "IdentificadorOrigem": this.str.IdentificadorOrigem = (string)value; break;							
						case "IdConta": this.str.IdConta = (string)value; break;							
						case "IdEvento": this.str.IdEvento = (string)value; break;							
                        case "IdEventoVencimento": this.str.IdEventoVencimento = (string)value; break;
						case "CodigoAtivo": this.str.CodigoAtivo = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "IdLiquidacao":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdLiquidacao = (System.Int32?)value;
							break;
						
						case "DataHistorico":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.DataHistorico = (System.DateTime?)value;
							break;
						
						case "IdCliente":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdCliente = (System.Int32?)value;
							break;
						
						case "DataLancamento":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.DataLancamento = (System.DateTime?)value;
							break;
						
						case "DataVencimento":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.DataVencimento = (System.DateTime?)value;
							break;
						
						case "Valor":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.Valor = (System.Decimal?)value;
							break;
						
						case "Situacao":
						
							if (value == null || value.GetType().ToString() == "System.Byte")
								this.Situacao = (System.Byte?)value;
							break;
						
						case "Origem":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.Origem = (System.Int32?)value;
							break;
						
						case "Fonte":
						
							if (value == null || value.GetType().ToString() == "System.Byte")
								this.Fonte = (System.Byte?)value;
							break;
						
						case "IdAgente":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdAgente = (System.Int32?)value;
							break;
						
						case "IdentificadorOrigem":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdentificadorOrigem = (System.Int32?)value;
							break;
						
						case "IdConta":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdConta = (System.Int32?)value;
							break;
						
						case "IdEvento":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdEvento = (System.Int32?)value;
							break;
							
						case "IdEventoVencimento":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdEventoVencimento = (System.Int32?)value;
							break;
					

						default:
							break;
					}
				}
			}
			else if(this.Row.Table.Columns.Contains(name))
			{
				this.Row[name] = value;
			}
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}
		
		
		/// <summary>
		/// Maps to LiquidacaoHistorico.IdLiquidacao
		/// </summary>
		virtual public System.Int32? IdLiquidacao
		{
			get
			{
				return base.GetSystemInt32(LiquidacaoHistoricoMetadata.ColumnNames.IdLiquidacao);
			}
			
			set
			{
				base.SetSystemInt32(LiquidacaoHistoricoMetadata.ColumnNames.IdLiquidacao, value);
			}
		}
		
		/// <summary>
		/// Maps to LiquidacaoHistorico.DataHistorico
		/// </summary>
		virtual public System.DateTime? DataHistorico
		{
			get
			{
				return base.GetSystemDateTime(LiquidacaoHistoricoMetadata.ColumnNames.DataHistorico);
			}
			
			set
			{
				base.SetSystemDateTime(LiquidacaoHistoricoMetadata.ColumnNames.DataHistorico, value);
			}
		}
		
		/// <summary>
		/// Maps to LiquidacaoHistorico.IdCliente
		/// </summary>
		virtual public System.Int32? IdCliente
		{
			get
			{
				return base.GetSystemInt32(LiquidacaoHistoricoMetadata.ColumnNames.IdCliente);
			}
			
			set
			{
				if(base.SetSystemInt32(LiquidacaoHistoricoMetadata.ColumnNames.IdCliente, value))
				{
					this._UpToClienteByIdCliente = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to LiquidacaoHistorico.DataLancamento
		/// </summary>
		virtual public System.DateTime? DataLancamento
		{
			get
			{
				return base.GetSystemDateTime(LiquidacaoHistoricoMetadata.ColumnNames.DataLancamento);
			}
			
			set
			{
				base.SetSystemDateTime(LiquidacaoHistoricoMetadata.ColumnNames.DataLancamento, value);
			}
		}
		
		/// <summary>
		/// Maps to LiquidacaoHistorico.DataVencimento
		/// </summary>
		virtual public System.DateTime? DataVencimento
		{
			get
			{
				return base.GetSystemDateTime(LiquidacaoHistoricoMetadata.ColumnNames.DataVencimento);
			}
			
			set
			{
				base.SetSystemDateTime(LiquidacaoHistoricoMetadata.ColumnNames.DataVencimento, value);
			}
		}
		
		/// <summary>
		/// Maps to LiquidacaoHistorico.Descricao
		/// </summary>
		virtual public System.String Descricao
		{
			get
			{
				return base.GetSystemString(LiquidacaoHistoricoMetadata.ColumnNames.Descricao);
			}
			
			set
			{
				base.SetSystemString(LiquidacaoHistoricoMetadata.ColumnNames.Descricao, value);
			}
		}
		
		/// <summary>
		/// Maps to LiquidacaoHistorico.Valor
		/// </summary>
		virtual public System.Decimal? Valor
		{
			get
			{
				return base.GetSystemDecimal(LiquidacaoHistoricoMetadata.ColumnNames.Valor);
			}
			
			set
			{
				base.SetSystemDecimal(LiquidacaoHistoricoMetadata.ColumnNames.Valor, value);
			}
		}
		
		/// <summary>
		/// Maps to LiquidacaoHistorico.Situacao
		/// </summary>
		virtual public System.Byte? Situacao
		{
			get
			{
				return base.GetSystemByte(LiquidacaoHistoricoMetadata.ColumnNames.Situacao);
			}
			
			set
			{
				base.SetSystemByte(LiquidacaoHistoricoMetadata.ColumnNames.Situacao, value);
			}
		}
		
		/// <summary>
		/// Maps to LiquidacaoHistorico.Origem
		/// </summary>
		virtual public System.Int32? Origem
		{
			get
			{
				return base.GetSystemInt32(LiquidacaoHistoricoMetadata.ColumnNames.Origem);
			}
			
			set
			{
				base.SetSystemInt32(LiquidacaoHistoricoMetadata.ColumnNames.Origem, value);
			}
		}
		
		/// <summary>
		/// Maps to LiquidacaoHistorico.Fonte
		/// </summary>
		virtual public System.Byte? Fonte
		{
			get
			{
				return base.GetSystemByte(LiquidacaoHistoricoMetadata.ColumnNames.Fonte);
			}
			
			set
			{
				base.SetSystemByte(LiquidacaoHistoricoMetadata.ColumnNames.Fonte, value);
			}
		}
		
		/// <summary>
		/// Maps to LiquidacaoHistorico.IdAgente
		/// </summary>
		virtual public System.Int32? IdAgente
		{
			get
			{
				return base.GetSystemInt32(LiquidacaoHistoricoMetadata.ColumnNames.IdAgente);
			}
			
			set
			{
				if(base.SetSystemInt32(LiquidacaoHistoricoMetadata.ColumnNames.IdAgente, value))
				{
					this._UpToAgenteMercadoByIdAgente = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to LiquidacaoHistorico.IdentificadorOrigem
		/// </summary>
		virtual public System.Int32? IdentificadorOrigem
		{
			get
			{
				return base.GetSystemInt32(LiquidacaoHistoricoMetadata.ColumnNames.IdentificadorOrigem);
			}
			
			set
			{
				base.SetSystemInt32(LiquidacaoHistoricoMetadata.ColumnNames.IdentificadorOrigem, value);
			}
		}
		
		/// <summary>
		/// Maps to LiquidacaoHistorico.IdConta
		/// </summary>
		virtual public System.Int32? IdConta
		{
			get
			{
				return base.GetSystemInt32(LiquidacaoHistoricoMetadata.ColumnNames.IdConta);
			}
			
			set
			{
				base.SetSystemInt32(LiquidacaoHistoricoMetadata.ColumnNames.IdConta, value);
			}
		}
		
		/// <summary>
		/// Maps to LiquidacaoHistorico.IdEvento
		/// </summary>
		virtual public System.Int32? IdEvento
		{
			get
			{
				return base.GetSystemInt32(LiquidacaoHistoricoMetadata.ColumnNames.IdEvento);
			}
			
			set
			{
				base.SetSystemInt32(LiquidacaoHistoricoMetadata.ColumnNames.IdEvento, value);
			}
		}
		
		/// <summary>
		/// Maps to LiquidacaoHistorico.IdEventoVencimento
		/// </summary>
		virtual public System.Int32? IdEventoVencimento
		{
			get
			{
				return base.GetSystemInt32(LiquidacaoHistoricoMetadata.ColumnNames.IdEventoVencimento);
			}
			
			set
			{
				base.SetSystemInt32(LiquidacaoHistoricoMetadata.ColumnNames.IdEventoVencimento, value);
			}
		}

        /// <summary>
		/// Maps to LiquidacaoHistorico.CodigoAtivo
		/// </summary>
		virtual public System.String CodigoAtivo
		{
			get
			{
				return base.GetSystemString(LiquidacaoHistoricoMetadata.ColumnNames.CodigoAtivo);
			}
			
			set
			{
				base.SetSystemString(LiquidacaoHistoricoMetadata.ColumnNames.CodigoAtivo, value);
			}
		}
		
		[CLSCompliant(false)]
		internal protected AgenteMercado _UpToAgenteMercadoByIdAgente;
		[CLSCompliant(false)]
		internal protected Cliente _UpToClienteByIdCliente;
		#endregion	

		#region String Properties


		[BrowsableAttribute( false )]
		public esStrings str
		{
			get
			{
				if (esstrings == null)
				{
					esstrings = new esStrings(this);
				}
				return esstrings;
			}
		}


		[Serializable]
		sealed public class esStrings
		{
			public esStrings(esLiquidacaoHistorico entity)
			{
				this.entity = entity;
			}
			
	
			public System.String IdLiquidacao
			{
				get
				{
					System.Int32? data = entity.IdLiquidacao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdLiquidacao = null;
					else entity.IdLiquidacao = Convert.ToInt32(value);
				}
			}
				
			public System.String DataHistorico
			{
				get
				{
					System.DateTime? data = entity.DataHistorico;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DataHistorico = null;
					else entity.DataHistorico = Convert.ToDateTime(value);
				}
			}
				
			public System.String IdCliente
			{
				get
				{
					System.Int32? data = entity.IdCliente;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdCliente = null;
					else entity.IdCliente = Convert.ToInt32(value);
				}
			}
				
			public System.String DataLancamento
			{
				get
				{
					System.DateTime? data = entity.DataLancamento;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DataLancamento = null;
					else entity.DataLancamento = Convert.ToDateTime(value);
				}
			}
				
			public System.String DataVencimento
			{
				get
				{
					System.DateTime? data = entity.DataVencimento;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DataVencimento = null;
					else entity.DataVencimento = Convert.ToDateTime(value);
				}
			}
				
			public System.String Descricao
			{
				get
				{
					System.String data = entity.Descricao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Descricao = null;
					else entity.Descricao = Convert.ToString(value);
				}
			}
				
			public System.String Valor
			{
				get
				{
					System.Decimal? data = entity.Valor;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Valor = null;
					else entity.Valor = Convert.ToDecimal(value);
				}
			}
				
			public System.String Situacao
			{
				get
				{
					System.Byte? data = entity.Situacao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Situacao = null;
					else entity.Situacao = Convert.ToByte(value);
				}
			}
				
			public System.String Origem
			{
				get
				{
					System.Int32? data = entity.Origem;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Origem = null;
					else entity.Origem = Convert.ToInt32(value);
				}
			}
				
			public System.String Fonte
			{
				get
				{
					System.Byte? data = entity.Fonte;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Fonte = null;
					else entity.Fonte = Convert.ToByte(value);
				}
			}
				
			public System.String IdAgente
			{
				get
				{
					System.Int32? data = entity.IdAgente;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdAgente = null;
					else entity.IdAgente = Convert.ToInt32(value);
				}
			}
				
			public System.String IdentificadorOrigem
			{
				get
				{
					System.Int32? data = entity.IdentificadorOrigem;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdentificadorOrigem = null;
					else entity.IdentificadorOrigem = Convert.ToInt32(value);
				}
			}
				
			public System.String IdConta
			{
				get
				{
					System.Int32? data = entity.IdConta;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdConta = null;
					else entity.IdConta = Convert.ToInt32(value);
				}
			}
				
			public System.String IdEvento
			{
				get
				{
					System.Int32? data = entity.IdEvento;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdEvento = null;
					else entity.IdEvento = Convert.ToInt32(value);
				}
			}
				
			public System.String CodigoAtivo
			{
				get
				{
					System.String data = entity.CodigoAtivo;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.CodigoAtivo = null;
					else entity.CodigoAtivo = Convert.ToString(value);
				}
			}
			
			public System.String IdEventoVencimento
			{
				get
				{
					System.Int32? data = entity.IdEventoVencimento;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdEventoVencimento = null;
					else entity.IdEventoVencimento = Convert.ToInt32(value);
				}
			}
			

			private esLiquidacaoHistorico entity;
		}
		#endregion

		#region Query Logic
		protected void InitQuery(esLiquidacaoHistoricoQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntity)this).Connection;
		}

		[System.Diagnostics.DebuggerNonUserCode]
		protected bool OnQueryLoaded(DataTable table)
		{
			bool dataFound = this.PopulateEntity(table);

			if (this.RowCount > 1)
			{
				throw new Exception("esLiquidacaoHistorico can only hold one record of data");
			}

			return dataFound;
		}
		#endregion
		
		[NonSerialized]
		private esStrings esstrings;
	}


	
	public partial class LiquidacaoHistorico : esLiquidacaoHistorico
	{

				
		#region UpToAgenteMercadoByIdAgente - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - AgenteMercado_LiquidacaoHistorico_FK1
		/// </summary>

		[XmlIgnore]
		public AgenteMercado UpToAgenteMercadoByIdAgente
		{
			get
			{
				if(this._UpToAgenteMercadoByIdAgente == null
					&& IdAgente != null					)
				{
					this._UpToAgenteMercadoByIdAgente = new AgenteMercado();
					this._UpToAgenteMercadoByIdAgente.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToAgenteMercadoByIdAgente", this._UpToAgenteMercadoByIdAgente);
					this._UpToAgenteMercadoByIdAgente.Query.Where(this._UpToAgenteMercadoByIdAgente.Query.IdAgente == this.IdAgente);
					this._UpToAgenteMercadoByIdAgente.Query.Load();
				}

				return this._UpToAgenteMercadoByIdAgente;
			}
			
			set
			{
				this.RemovePreSave("UpToAgenteMercadoByIdAgente");
				

				if(value == null)
				{
					this.IdAgente = null;
					this._UpToAgenteMercadoByIdAgente = null;
				}
				else
				{
					this.IdAgente = value.IdAgente;
					this._UpToAgenteMercadoByIdAgente = value;
					this.SetPreSave("UpToAgenteMercadoByIdAgente", this._UpToAgenteMercadoByIdAgente);
				}
				
			}
		}
		#endregion
		

				
		#region UpToClienteByIdCliente - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - Cliente_LiquidacaoHistorico_FK1
		/// </summary>

		[XmlIgnore]
		public Cliente UpToClienteByIdCliente
		{
			get
			{
				if(this._UpToClienteByIdCliente == null
					&& IdCliente != null					)
				{
					this._UpToClienteByIdCliente = new Cliente();
					this._UpToClienteByIdCliente.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToClienteByIdCliente", this._UpToClienteByIdCliente);
					this._UpToClienteByIdCliente.Query.Where(this._UpToClienteByIdCliente.Query.IdCliente == this.IdCliente);
					this._UpToClienteByIdCliente.Query.Load();
				}

				return this._UpToClienteByIdCliente;
			}
			
			set
			{
				this.RemovePreSave("UpToClienteByIdCliente");
				

				if(value == null)
				{
					this.IdCliente = null;
					this._UpToClienteByIdCliente = null;
				}
				else
				{
					this.IdCliente = value.IdCliente;
					this._UpToClienteByIdCliente = value;
					this.SetPreSave("UpToClienteByIdCliente", this._UpToClienteByIdCliente);
				}
				
			}
		}
		#endregion
		

		
		/// <summary>
		/// Used internally by the entity's hierarchical properties.
		/// </summary>
		protected override List<esPropertyDescriptor> GetHierarchicalProperties()
		{
			List<esPropertyDescriptor> props = new List<esPropertyDescriptor>();
			
		
			return props;
		}	
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PreSave.
		/// </summary>
		protected override void ApplyPreSaveKeys()
		{
			if(!this.es.IsDeleted && this._UpToAgenteMercadoByIdAgente != null)
			{
				this.IdAgente = this._UpToAgenteMercadoByIdAgente.IdAgente;
			}
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostSave.
		/// </summary>
		protected override void ApplyPostSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostOneToOneSave.
		/// </summary>
		protected override void ApplyPostOneSaveKeys()
		{
		}
		
	}



	[Serializable]
	abstract public class esLiquidacaoHistoricoQuery : esDynamicQuery
	{
		override protected IMetadata Meta
		{
			get
			{
				return LiquidacaoHistoricoMetadata.Meta();
			}
		}	
		

		public esQueryItem IdLiquidacao
		{
			get
			{
				return new esQueryItem(this, LiquidacaoHistoricoMetadata.ColumnNames.IdLiquidacao, esSystemType.Int32);
			}
		} 
		
		public esQueryItem DataHistorico
		{
			get
			{
				return new esQueryItem(this, LiquidacaoHistoricoMetadata.ColumnNames.DataHistorico, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem IdCliente
		{
			get
			{
				return new esQueryItem(this, LiquidacaoHistoricoMetadata.ColumnNames.IdCliente, esSystemType.Int32);
			}
		} 
		
		public esQueryItem DataLancamento
		{
			get
			{
				return new esQueryItem(this, LiquidacaoHistoricoMetadata.ColumnNames.DataLancamento, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem DataVencimento
		{
			get
			{
				return new esQueryItem(this, LiquidacaoHistoricoMetadata.ColumnNames.DataVencimento, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem Descricao
		{
			get
			{
				return new esQueryItem(this, LiquidacaoHistoricoMetadata.ColumnNames.Descricao, esSystemType.String);
			}
		} 
		
		public esQueryItem Valor
		{
			get
			{
				return new esQueryItem(this, LiquidacaoHistoricoMetadata.ColumnNames.Valor, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem Situacao
		{
			get
			{
				return new esQueryItem(this, LiquidacaoHistoricoMetadata.ColumnNames.Situacao, esSystemType.Byte);
			}
		} 
		
		public esQueryItem Origem
		{
			get
			{
				return new esQueryItem(this, LiquidacaoHistoricoMetadata.ColumnNames.Origem, esSystemType.Int32);
			}
		} 
		
		public esQueryItem Fonte
		{
			get
			{
				return new esQueryItem(this, LiquidacaoHistoricoMetadata.ColumnNames.Fonte, esSystemType.Byte);
			}
		} 
		
		public esQueryItem IdAgente
		{
			get
			{
				return new esQueryItem(this, LiquidacaoHistoricoMetadata.ColumnNames.IdAgente, esSystemType.Int32);
			}
		} 
		
		public esQueryItem IdentificadorOrigem
		{
			get
			{
				return new esQueryItem(this, LiquidacaoHistoricoMetadata.ColumnNames.IdentificadorOrigem, esSystemType.Int32);
			}
		} 
		
		public esQueryItem IdConta
		{
			get
			{
				return new esQueryItem(this, LiquidacaoHistoricoMetadata.ColumnNames.IdConta, esSystemType.Int32);
			}
		} 
		
		public esQueryItem IdEvento
		{
			get
			{
				return new esQueryItem(this, LiquidacaoHistoricoMetadata.ColumnNames.IdEvento, esSystemType.Int32);
			}
		} 

        public esQueryItem IdEventoVencimento
		{
			get
			{
				return new esQueryItem(this, LiquidacaoHistoricoMetadata.ColumnNames.IdEventoVencimento, esSystemType.Int32);
			}
		} 

		public esQueryItem CodigoAtivo
		{
			get
			{
				return new esQueryItem(this, LiquidacaoHistoricoMetadata.ColumnNames.CodigoAtivo, esSystemType.String);
			}
		} 
		
	}



	[Serializable]
	[XmlType("LiquidacaoHistoricoCollection")]
	public partial class LiquidacaoHistoricoCollection : esLiquidacaoHistoricoCollection, IEnumerable<LiquidacaoHistorico>
	{
		public LiquidacaoHistoricoCollection()
		{

		}
		
		public static implicit operator List<LiquidacaoHistorico>(LiquidacaoHistoricoCollection coll)
		{
			List<LiquidacaoHistorico> list = new List<LiquidacaoHistorico>();
			
			foreach (LiquidacaoHistorico emp in coll)
			{
				list.Add(emp);
			}
			
			return list;
		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return  LiquidacaoHistoricoMetadata.Meta();
			}
		}
		
		
		
		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new LiquidacaoHistoricoQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		
		override protected esEntity CreateEntityForCollection(DataRow row)
		{
			return new LiquidacaoHistorico(row);
		}

		override protected esEntity CreateEntity()
		{
			return new LiquidacaoHistorico();
		}
		
		
		#endregion


		[BrowsableAttribute( false )]
		public LiquidacaoHistoricoQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new LiquidacaoHistoricoQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}

		public bool Load(LiquidacaoHistoricoQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		public LiquidacaoHistorico AddNew()
		{
			LiquidacaoHistorico entity = base.AddNewEntity() as LiquidacaoHistorico;
			
			return entity;
		}

		public LiquidacaoHistorico FindByPrimaryKey(System.Int32 idLiquidacao, System.DateTime dataHistorico)
		{
			return base.FindByPrimaryKey(idLiquidacao, dataHistorico) as LiquidacaoHistorico;
		}


		#region IEnumerable<LiquidacaoHistorico> Members

		IEnumerator<LiquidacaoHistorico> IEnumerable<LiquidacaoHistorico>.GetEnumerator()
		{
			System.Collections.IEnumerable enumer = this as System.Collections.IEnumerable;
			System.Collections.IEnumerator iterator = enumer.GetEnumerator();

			while(iterator.MoveNext())
			{
				yield return iterator.Current as LiquidacaoHistorico;
			}
		}

		#endregion
		
		private LiquidacaoHistoricoQuery query;
	}


	/// <summary>
	/// Encapsulates the 'LiquidacaoHistorico' table
	/// </summary>

	[Serializable]
	public partial class LiquidacaoHistorico : esLiquidacaoHistorico
	{
		public LiquidacaoHistorico()
		{

		}
	
		public LiquidacaoHistorico(DataRow row)
			: base(row)
		{

		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return LiquidacaoHistoricoMetadata.Meta();
			}
		}
		
		
		
		override protected esLiquidacaoHistoricoQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new LiquidacaoHistoricoQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		#endregion
		



		[BrowsableAttribute( false )]
		public LiquidacaoHistoricoQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new LiquidacaoHistoricoQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}
		

		public bool Load(LiquidacaoHistoricoQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		private LiquidacaoHistoricoQuery query;
	}



	[Serializable]
	public partial class LiquidacaoHistoricoQuery : esLiquidacaoHistoricoQuery
	{
		public LiquidacaoHistoricoQuery()
		{

		}		
		
		public LiquidacaoHistoricoQuery(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
			
	}



	[Serializable]
	public partial class LiquidacaoHistoricoMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected LiquidacaoHistoricoMetadata()
		{
			_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(LiquidacaoHistoricoMetadata.ColumnNames.IdLiquidacao, 0, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = LiquidacaoHistoricoMetadata.PropertyNames.IdLiquidacao;
			c.IsInPrimaryKey = true;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(LiquidacaoHistoricoMetadata.ColumnNames.DataHistorico, 1, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = LiquidacaoHistoricoMetadata.PropertyNames.DataHistorico;
			c.IsInPrimaryKey = true;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(LiquidacaoHistoricoMetadata.ColumnNames.IdCliente, 2, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = LiquidacaoHistoricoMetadata.PropertyNames.IdCliente;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(LiquidacaoHistoricoMetadata.ColumnNames.DataLancamento, 3, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = LiquidacaoHistoricoMetadata.PropertyNames.DataLancamento;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(LiquidacaoHistoricoMetadata.ColumnNames.DataVencimento, 4, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = LiquidacaoHistoricoMetadata.PropertyNames.DataVencimento;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(LiquidacaoHistoricoMetadata.ColumnNames.Descricao, 5, typeof(System.String), esSystemType.String);
			c.PropertyName = LiquidacaoHistoricoMetadata.PropertyNames.Descricao;
			c.CharacterMaxLength = 600;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(LiquidacaoHistoricoMetadata.ColumnNames.Valor, 6, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = LiquidacaoHistoricoMetadata.PropertyNames.Valor;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(LiquidacaoHistoricoMetadata.ColumnNames.Situacao, 7, typeof(System.Byte), esSystemType.Byte);
			c.PropertyName = LiquidacaoHistoricoMetadata.PropertyNames.Situacao;	
			c.NumericPrecision = 3;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(LiquidacaoHistoricoMetadata.ColumnNames.Origem, 8, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = LiquidacaoHistoricoMetadata.PropertyNames.Origem;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(LiquidacaoHistoricoMetadata.ColumnNames.Fonte, 9, typeof(System.Byte), esSystemType.Byte);
			c.PropertyName = LiquidacaoHistoricoMetadata.PropertyNames.Fonte;	
			c.NumericPrecision = 3;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(LiquidacaoHistoricoMetadata.ColumnNames.IdAgente, 10, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = LiquidacaoHistoricoMetadata.PropertyNames.IdAgente;	
			c.NumericPrecision = 10;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(LiquidacaoHistoricoMetadata.ColumnNames.IdentificadorOrigem, 11, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = LiquidacaoHistoricoMetadata.PropertyNames.IdentificadorOrigem;	
			c.NumericPrecision = 10;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(LiquidacaoHistoricoMetadata.ColumnNames.IdConta, 12, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = LiquidacaoHistoricoMetadata.PropertyNames.IdConta;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(LiquidacaoHistoricoMetadata.ColumnNames.IdEvento, 13, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = LiquidacaoHistoricoMetadata.PropertyNames.IdEvento;	
			c.NumericPrecision = 10;
			c.IsNullable = true;
			_columns.Add(c); 
			

			c = new esColumnMetadata(LiquidacaoHistoricoMetadata.ColumnNames.IdEventoVencimento, 14, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = LiquidacaoHistoricoMetadata.PropertyNames.IdEventoVencimento;	
			c.NumericPrecision = 10;	
            c.IsNullable = true;
			_columns.Add(c); 


			c = new esColumnMetadata(LiquidacaoHistoricoMetadata.ColumnNames.CodigoAtivo, 14, typeof(System.String), esSystemType.String);
			c.PropertyName = LiquidacaoHistoricoMetadata.PropertyNames.CodigoAtivo;
			c.CharacterMaxLength = 40;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
		}
		#endregion
	
		static public LiquidacaoHistoricoMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base._dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base._columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string IdLiquidacao = "IdLiquidacao";
			 public const string DataHistorico = "DataHistorico";
			 public const string IdCliente = "IdCliente";
			 public const string DataLancamento = "DataLancamento";
			 public const string DataVencimento = "DataVencimento";
			 public const string Descricao = "Descricao";
			 public const string Valor = "Valor";
			 public const string Situacao = "Situacao";
			 public const string Origem = "Origem";
			 public const string Fonte = "Fonte";
			 public const string IdAgente = "IdAgente";
			 public const string IdentificadorOrigem = "IdentificadorOrigem";
			 public const string IdConta = "IdConta";
			 public const string IdEvento = "IdEvento";
			 public const string IdEventoVencimento = "IdEventoVencimento";
			 public const string CodigoAtivo = "CodigoAtivo";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string IdLiquidacao = "IdLiquidacao";
			 public const string DataHistorico = "DataHistorico";
			 public const string IdCliente = "IdCliente";
			 public const string DataLancamento = "DataLancamento";
			 public const string DataVencimento = "DataVencimento";
			 public const string Descricao = "Descricao";
			 public const string Valor = "Valor";
			 public const string Situacao = "Situacao";
			 public const string Origem = "Origem";
			 public const string Fonte = "Fonte";
			 public const string IdAgente = "IdAgente";
			 public const string IdentificadorOrigem = "IdentificadorOrigem";
			 public const string IdConta = "IdConta";
			 public const string IdEvento = "IdEvento";
			 public const string IdEventoVencimento = "IdEventoVencimento";
			 public const string CodigoAtivo = "CodigoAtivo";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(LiquidacaoHistoricoMetadata))
			{
				if(LiquidacaoHistoricoMetadata.mapDelegates == null)
				{
					LiquidacaoHistoricoMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (LiquidacaoHistoricoMetadata.meta == null)
				{
					LiquidacaoHistoricoMetadata.meta = new LiquidacaoHistoricoMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();
				

				meta.AddTypeMap("IdLiquidacao", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("DataHistorico", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("IdCliente", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("DataLancamento", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("DataVencimento", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("Descricao", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("Valor", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("Situacao", new esTypeMap("tinyint", "System.Byte"));
				meta.AddTypeMap("Origem", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("Fonte", new esTypeMap("tinyint", "System.Byte"));
				meta.AddTypeMap("IdAgente", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("IdentificadorOrigem", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("IdConta", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("IdEvento", new esTypeMap("int", "System.Int32"));
                meta.AddTypeMap("IdEventoVencimento", new esTypeMap("int", "System.Int32"));			
                meta.AddTypeMap("CodigoAtivo", new esTypeMap("varchar", "System.String"));			
				
				
				
				meta.Source = "LiquidacaoHistorico";
				meta.Destination = "LiquidacaoHistorico";
				
				meta.spInsert = "proc_LiquidacaoHistoricoInsert";				
				meta.spUpdate = "proc_LiquidacaoHistoricoUpdate";		
				meta.spDelete = "proc_LiquidacaoHistoricoDelete";
				meta.spLoadAll = "proc_LiquidacaoHistoricoLoadAll";
				meta.spLoadByPrimaryKey = "proc_LiquidacaoHistoricoLoadByPrimaryKey";
				
				this._providerMetadataMaps["esDefault"] = meta;
			}
			
			return this._providerMetadataMaps["esDefault"];
		}

		#endregion

		static private LiquidacaoHistoricoMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
