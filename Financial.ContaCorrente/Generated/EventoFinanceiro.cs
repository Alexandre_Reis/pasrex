/*
* Template MyGeneration - Generated/Generated - esCollection
* Resolve os imports dos arquivos financial
* Para compilar outros projetos que n�o o financial deve-se mudar o if Inicial (string usings = "1")
* Data: 30/05/2011
*/

/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 14/04/2016 12:54:23
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;


using EntitySpaces.Interfaces;
using EntitySpaces.Core;
using Financial.Contabil;





































		



			
					


		

		
		
		

		

























		






	
















				
		
		




		








		
				
				











	
















					
								
											
	









		





		




				
				








				




		

		
		
		
		
		





namespace Financial.ContaCorrente
{

	[Serializable]
	abstract public class esEventoFinanceiroCollection : esEntityCollection
	{
		public esEventoFinanceiroCollection()
		{

		}

		protected override string GetCollectionName()
		{
			return "EventoFinanceiroCollection";
		}

		#region Query Logic
		protected void InitQuery(esEventoFinanceiroQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntityCollection)this).Connection;
		}

		protected bool OnQueryLoaded(DataTable table)
		{
			this.PopulateCollection(table);
			return (this.RowCount > 0) ? true : false;
		}
		
		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery(query as esEventoFinanceiroQuery);
		}
		#endregion
		
		virtual public EventoFinanceiro DetachEntity(EventoFinanceiro entity)
		{
			return base.DetachEntity(entity) as EventoFinanceiro;
		}
		
		virtual public EventoFinanceiro AttachEntity(EventoFinanceiro entity)
		{
			return base.AttachEntity(entity) as EventoFinanceiro;
		}
		
		virtual public void Combine(EventoFinanceiroCollection collection)
		{
			base.Combine(collection);
		}
		
		new public EventoFinanceiro this[int index]
		{
			get
			{
				return base[index] as EventoFinanceiro;
			}
		}

		public override Type GetEntityType()
		{
			return typeof(EventoFinanceiro);
		}
	}



	[Serializable]
	abstract public class esEventoFinanceiro : esEntity
	{
		/// <summary>
		/// Used internally by the entity's DynamicQuery mechanism.
		/// </summary>
		virtual protected esEventoFinanceiroQuery GetDynamicQuery()
		{
			return null;
		}

		public esEventoFinanceiro()
		{

		}

		public esEventoFinanceiro(DataRow row)
			: base(row)
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.Int32 idEventoFinanceiro)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idEventoFinanceiro);
			else
				return LoadByPrimaryKeyStoredProcedure(idEventoFinanceiro);
		}

		/*
* Template MyGeneration - Generated/Generated - esEntity
* Funcionalidade de LoadByPrimaryKey - acrescentar codigo abaixo na linha 141
* Data: 30/05/2011
*/		
		
		
		
		
		/// <summary>
		/// Loads an entity by primary key
		/// </summary>
		/// <remarks>
		/// EntitySpaces requires primary keys be defined on all tables.
		/// If a table does not have a primary key set,
		/// this method will not compile.
		/// Does not support sqlAcessType. It only works with DynamicQuery
		/// </remarks>
		/// <param name="fieldsToReturn">Fields desired</param>
		public virtual bool LoadByPrimaryKey(List<esQueryItem> fieldsToReturn, System.Int32 idEventoFinanceiro)
		{
			esQueryItem[] fields = fieldsToReturn.ToArray();			
			esEventoFinanceiroQuery query = this.GetDynamicQuery();
			query
			    .Select(fields)
			    .Where(query.IdEventoFinanceiro == idEventoFinanceiro);
		
			return query.Load();
		}
	
		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.Int32 idEventoFinanceiro)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idEventoFinanceiro);
			else
				return LoadByPrimaryKeyStoredProcedure(idEventoFinanceiro);
		}

		private bool LoadByPrimaryKeyDynamic(System.Int32 idEventoFinanceiro)
		{
			esEventoFinanceiroQuery query = this.GetDynamicQuery();
			query.Where(query.IdEventoFinanceiro == idEventoFinanceiro);
			return query.Load();
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.Int32 idEventoFinanceiro)
		{
			esParameters parms = new esParameters();
			parms.Add("IdEventoFinanceiro",idEventoFinanceiro);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		
		
		#region Properties
		
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}

		public override void SetProperty(string name, object value)
		{
			if(this.Row == null) this.AddNew();
			
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value.GetType().ToString() == "System.String")
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "IdEventoFinanceiro": this.str.IdEventoFinanceiro = (string)value; break;							
						case "Descricao": this.str.Descricao = (string)value; break;							
						case "ClassificacaoAnbima": this.str.ClassificacaoAnbima = (string)value; break;							
						case "IdEventoProvisao": this.str.IdEventoProvisao = (string)value; break;							
						case "IdEventoPagamento": this.str.IdEventoPagamento = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "IdEventoFinanceiro":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdEventoFinanceiro = (System.Int32?)value;
							break;
						
						case "ClassificacaoAnbima":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.ClassificacaoAnbima = (System.Int32?)value;
							break;
						
						case "IdEventoProvisao":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdEventoProvisao = (System.Int32?)value;
							break;
						
						case "IdEventoPagamento":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdEventoPagamento = (System.Int32?)value;
							break;
					

						default:
							break;
					}
				}
			}
			else if(this.Row.Table.Columns.Contains(name))
			{
				this.Row[name] = value;
			}
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}
		
		
		/// <summary>
		/// Maps to EventoFinanceiro.IdEventoFinanceiro
		/// </summary>
		virtual public System.Int32? IdEventoFinanceiro
		{
			get
			{
				return base.GetSystemInt32(EventoFinanceiroMetadata.ColumnNames.IdEventoFinanceiro);
			}
			
			set
			{
				base.SetSystemInt32(EventoFinanceiroMetadata.ColumnNames.IdEventoFinanceiro, value);
			}
		}
		
		/// <summary>
		/// Maps to EventoFinanceiro.Descricao
		/// </summary>
		virtual public System.String Descricao
		{
			get
			{
				return base.GetSystemString(EventoFinanceiroMetadata.ColumnNames.Descricao);
			}
			
			set
			{
				base.SetSystemString(EventoFinanceiroMetadata.ColumnNames.Descricao, value);
			}
		}
		
		/// <summary>
		/// Maps to EventoFinanceiro.ClassificacaoAnbima
		/// </summary>
		virtual public System.Int32? ClassificacaoAnbima
		{
			get
			{
				return base.GetSystemInt32(EventoFinanceiroMetadata.ColumnNames.ClassificacaoAnbima);
			}
			
			set
			{
				base.SetSystemInt32(EventoFinanceiroMetadata.ColumnNames.ClassificacaoAnbima, value);
			}
		}
		
		/// <summary>
		/// Maps to EventoFinanceiro.IdEventoProvisao
		/// </summary>
		virtual public System.Int32? IdEventoProvisao
		{
			get
			{
				return base.GetSystemInt32(EventoFinanceiroMetadata.ColumnNames.IdEventoProvisao);
			}
			
			set
			{
				if(base.SetSystemInt32(EventoFinanceiroMetadata.ColumnNames.IdEventoProvisao, value))
				{
					this._UpToContabRoteiroByIdEventoProvisao = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to EventoFinanceiro.IdEventoPagamento
		/// </summary>
		virtual public System.Int32? IdEventoPagamento
		{
			get
			{
				return base.GetSystemInt32(EventoFinanceiroMetadata.ColumnNames.IdEventoPagamento);
			}
			
			set
			{
				if(base.SetSystemInt32(EventoFinanceiroMetadata.ColumnNames.IdEventoPagamento, value))
				{
					this._UpToContabRoteiroByIdEventoPagamento = null;
				}
			}
		}
		
		[CLSCompliant(false)]
		internal protected ContabRoteiro _UpToContabRoteiroByIdEventoPagamento;
		[CLSCompliant(false)]
		internal protected ContabRoteiro _UpToContabRoteiroByIdEventoProvisao;
		#endregion	

		#region String Properties


		[BrowsableAttribute( false )]
		public esStrings str
		{
			get
			{
				if (esstrings == null)
				{
					esstrings = new esStrings(this);
				}
				return esstrings;
			}
		}


		[Serializable]
		sealed public class esStrings
		{
			public esStrings(esEventoFinanceiro entity)
			{
				this.entity = entity;
			}
			
	
			public System.String IdEventoFinanceiro
			{
				get
				{
					System.Int32? data = entity.IdEventoFinanceiro;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdEventoFinanceiro = null;
					else entity.IdEventoFinanceiro = Convert.ToInt32(value);
				}
			}
				
			public System.String Descricao
			{
				get
				{
					System.String data = entity.Descricao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Descricao = null;
					else entity.Descricao = Convert.ToString(value);
				}
			}
				
			public System.String ClassificacaoAnbima
			{
				get
				{
					System.Int32? data = entity.ClassificacaoAnbima;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ClassificacaoAnbima = null;
					else entity.ClassificacaoAnbima = Convert.ToInt32(value);
				}
			}
				
			public System.String IdEventoProvisao
			{
				get
				{
					System.Int32? data = entity.IdEventoProvisao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdEventoProvisao = null;
					else entity.IdEventoProvisao = Convert.ToInt32(value);
				}
			}
				
			public System.String IdEventoPagamento
			{
				get
				{
					System.Int32? data = entity.IdEventoPagamento;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdEventoPagamento = null;
					else entity.IdEventoPagamento = Convert.ToInt32(value);
				}
			}
			

			private esEventoFinanceiro entity;
		}
		#endregion

		#region Query Logic
		protected void InitQuery(esEventoFinanceiroQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntity)this).Connection;
		}

		[System.Diagnostics.DebuggerNonUserCode]
		protected bool OnQueryLoaded(DataTable table)
		{
			bool dataFound = this.PopulateEntity(table);

			if (this.RowCount > 1)
			{
				throw new Exception("esEventoFinanceiro can only hold one record of data");
			}

			return dataFound;
		}
		#endregion
		
		[NonSerialized]
		private esStrings esstrings;
	}


	
	public partial class EventoFinanceiro : esEventoFinanceiro
	{

				
		#region UpToContabRoteiroByIdEventoPagamento - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - FK_EventoFinanceiro_ContabRoteiro
		/// </summary>

		[XmlIgnore]
		public ContabRoteiro UpToContabRoteiroByIdEventoPagamento
		{
			get
			{
				if(this._UpToContabRoteiroByIdEventoPagamento == null
					&& IdEventoPagamento != null					)
				{
					this._UpToContabRoteiroByIdEventoPagamento = new ContabRoteiro();
					this._UpToContabRoteiroByIdEventoPagamento.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToContabRoteiroByIdEventoPagamento", this._UpToContabRoteiroByIdEventoPagamento);
					this._UpToContabRoteiroByIdEventoPagamento.Query.Where(this._UpToContabRoteiroByIdEventoPagamento.Query.IdEvento == this.IdEventoPagamento);
					this._UpToContabRoteiroByIdEventoPagamento.Query.Load();
				}

				return this._UpToContabRoteiroByIdEventoPagamento;
			}
			
			set
			{
				this.RemovePreSave("UpToContabRoteiroByIdEventoPagamento");
				

				if(value == null)
				{
					this.IdEventoPagamento = null;
					this._UpToContabRoteiroByIdEventoPagamento = null;
				}
				else
				{
					this.IdEventoPagamento = value.IdEvento;
					this._UpToContabRoteiroByIdEventoPagamento = value;
					this.SetPreSave("UpToContabRoteiroByIdEventoPagamento", this._UpToContabRoteiroByIdEventoPagamento);
				}
				
			}
		}
		#endregion
		

				
		#region UpToContabRoteiroByIdEventoProvisao - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - FK_EventoFinanceiro_ContabRoteiro1
		/// </summary>

		[XmlIgnore]
		public ContabRoteiro UpToContabRoteiroByIdEventoProvisao
		{
			get
			{
				if(this._UpToContabRoteiroByIdEventoProvisao == null
					&& IdEventoProvisao != null					)
				{
					this._UpToContabRoteiroByIdEventoProvisao = new ContabRoteiro();
					this._UpToContabRoteiroByIdEventoProvisao.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToContabRoteiroByIdEventoProvisao", this._UpToContabRoteiroByIdEventoProvisao);
					this._UpToContabRoteiroByIdEventoProvisao.Query.Where(this._UpToContabRoteiroByIdEventoProvisao.Query.IdEvento == this.IdEventoProvisao);
					this._UpToContabRoteiroByIdEventoProvisao.Query.Load();
				}

				return this._UpToContabRoteiroByIdEventoProvisao;
			}
			
			set
			{
				this.RemovePreSave("UpToContabRoteiroByIdEventoProvisao");
				

				if(value == null)
				{
					this.IdEventoProvisao = null;
					this._UpToContabRoteiroByIdEventoProvisao = null;
				}
				else
				{
					this.IdEventoProvisao = value.IdEvento;
					this._UpToContabRoteiroByIdEventoProvisao = value;
					this.SetPreSave("UpToContabRoteiroByIdEventoProvisao", this._UpToContabRoteiroByIdEventoProvisao);
				}
				
			}
		}
		#endregion
		

		
		/// <summary>
		/// Used internally by the entity's hierarchical properties.
		/// </summary>
		protected override List<esPropertyDescriptor> GetHierarchicalProperties()
		{
			List<esPropertyDescriptor> props = new List<esPropertyDescriptor>();
			
		
			return props;
		}	
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PreSave.
		/// </summary>
		protected override void ApplyPreSaveKeys()
		{
			if(!this.es.IsDeleted && this._UpToContabRoteiroByIdEventoPagamento != null)
			{
				this.IdEventoPagamento = this._UpToContabRoteiroByIdEventoPagamento.IdEvento;
			}
			if(!this.es.IsDeleted && this._UpToContabRoteiroByIdEventoProvisao != null)
			{
				this.IdEventoProvisao = this._UpToContabRoteiroByIdEventoProvisao.IdEvento;
			}
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostSave.
		/// </summary>
		protected override void ApplyPostSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostOneToOneSave.
		/// </summary>
		protected override void ApplyPostOneSaveKeys()
		{
		}
		
	}



	[Serializable]
	abstract public class esEventoFinanceiroQuery : esDynamicQuery
	{
		override protected IMetadata Meta
		{
			get
			{
				return EventoFinanceiroMetadata.Meta();
			}
		}	
		

		public esQueryItem IdEventoFinanceiro
		{
			get
			{
				return new esQueryItem(this, EventoFinanceiroMetadata.ColumnNames.IdEventoFinanceiro, esSystemType.Int32);
			}
		} 
		
		public esQueryItem Descricao
		{
			get
			{
				return new esQueryItem(this, EventoFinanceiroMetadata.ColumnNames.Descricao, esSystemType.String);
			}
		} 
		
		public esQueryItem ClassificacaoAnbima
		{
			get
			{
				return new esQueryItem(this, EventoFinanceiroMetadata.ColumnNames.ClassificacaoAnbima, esSystemType.Int32);
			}
		} 
		
		public esQueryItem IdEventoProvisao
		{
			get
			{
				return new esQueryItem(this, EventoFinanceiroMetadata.ColumnNames.IdEventoProvisao, esSystemType.Int32);
			}
		} 
		
		public esQueryItem IdEventoPagamento
		{
			get
			{
				return new esQueryItem(this, EventoFinanceiroMetadata.ColumnNames.IdEventoPagamento, esSystemType.Int32);
			}
		} 
		
	}



	[Serializable]
	[XmlType("EventoFinanceiroCollection")]
	public partial class EventoFinanceiroCollection : esEventoFinanceiroCollection, IEnumerable<EventoFinanceiro>
	{
		public EventoFinanceiroCollection()
		{

		}
		
		public static implicit operator List<EventoFinanceiro>(EventoFinanceiroCollection coll)
		{
			List<EventoFinanceiro> list = new List<EventoFinanceiro>();
			
			foreach (EventoFinanceiro emp in coll)
			{
				list.Add(emp);
			}
			
			return list;
		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return  EventoFinanceiroMetadata.Meta();
			}
		}
		
		
		
		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new EventoFinanceiroQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		
		override protected esEntity CreateEntityForCollection(DataRow row)
		{
			return new EventoFinanceiro(row);
		}

		override protected esEntity CreateEntity()
		{
			return new EventoFinanceiro();
		}
		
		
		#endregion


		[BrowsableAttribute( false )]
		public EventoFinanceiroQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new EventoFinanceiroQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}

		public bool Load(EventoFinanceiroQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		public EventoFinanceiro AddNew()
		{
			EventoFinanceiro entity = base.AddNewEntity() as EventoFinanceiro;
			
			return entity;
		}

		public EventoFinanceiro FindByPrimaryKey(System.Int32 idEventoFinanceiro)
		{
			return base.FindByPrimaryKey(idEventoFinanceiro) as EventoFinanceiro;
		}


		#region IEnumerable<EventoFinanceiro> Members

		IEnumerator<EventoFinanceiro> IEnumerable<EventoFinanceiro>.GetEnumerator()
		{
			System.Collections.IEnumerable enumer = this as System.Collections.IEnumerable;
			System.Collections.IEnumerator iterator = enumer.GetEnumerator();

			while(iterator.MoveNext())
			{
				yield return iterator.Current as EventoFinanceiro;
			}
		}

		#endregion
		
		private EventoFinanceiroQuery query;
	}


	/// <summary>
	/// Encapsulates the 'EventoFinanceiro' table
	/// </summary>

	[Serializable]
	public partial class EventoFinanceiro : esEventoFinanceiro
	{
		public EventoFinanceiro()
		{

		}
	
		public EventoFinanceiro(DataRow row)
			: base(row)
		{

		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return EventoFinanceiroMetadata.Meta();
			}
		}
		
		
		
		override protected esEventoFinanceiroQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new EventoFinanceiroQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		#endregion
		



		[BrowsableAttribute( false )]
		public EventoFinanceiroQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new EventoFinanceiroQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}
		

		public bool Load(EventoFinanceiroQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		private EventoFinanceiroQuery query;
	}



	[Serializable]
	public partial class EventoFinanceiroQuery : esEventoFinanceiroQuery
	{
		public EventoFinanceiroQuery()
		{

		}		
		
		public EventoFinanceiroQuery(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
			
	}



	[Serializable]
	public partial class EventoFinanceiroMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected EventoFinanceiroMetadata()
		{
			_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(EventoFinanceiroMetadata.ColumnNames.IdEventoFinanceiro, 0, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = EventoFinanceiroMetadata.PropertyNames.IdEventoFinanceiro;
			c.IsInPrimaryKey = true;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(EventoFinanceiroMetadata.ColumnNames.Descricao, 1, typeof(System.String), esSystemType.String);
			c.PropertyName = EventoFinanceiroMetadata.PropertyNames.Descricao;
			c.CharacterMaxLength = 200;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(EventoFinanceiroMetadata.ColumnNames.ClassificacaoAnbima, 2, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = EventoFinanceiroMetadata.PropertyNames.ClassificacaoAnbima;	
			c.NumericPrecision = 10;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(EventoFinanceiroMetadata.ColumnNames.IdEventoProvisao, 3, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = EventoFinanceiroMetadata.PropertyNames.IdEventoProvisao;	
			c.NumericPrecision = 10;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(EventoFinanceiroMetadata.ColumnNames.IdEventoPagamento, 4, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = EventoFinanceiroMetadata.PropertyNames.IdEventoPagamento;	
			c.NumericPrecision = 10;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
		}
		#endregion
	
		static public EventoFinanceiroMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base._dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base._columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string IdEventoFinanceiro = "IdEventoFinanceiro";
			 public const string Descricao = "Descricao";
			 public const string ClassificacaoAnbima = "ClassificacaoAnbima";
			 public const string IdEventoProvisao = "IdEventoProvisao";
			 public const string IdEventoPagamento = "IdEventoPagamento";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string IdEventoFinanceiro = "IdEventoFinanceiro";
			 public const string Descricao = "Descricao";
			 public const string ClassificacaoAnbima = "ClassificacaoAnbima";
			 public const string IdEventoProvisao = "IdEventoProvisao";
			 public const string IdEventoPagamento = "IdEventoPagamento";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(EventoFinanceiroMetadata))
			{
				if(EventoFinanceiroMetadata.mapDelegates == null)
				{
					EventoFinanceiroMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (EventoFinanceiroMetadata.meta == null)
				{
					EventoFinanceiroMetadata.meta = new EventoFinanceiroMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();
				

				meta.AddTypeMap("IdEventoFinanceiro", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("Descricao", new esTypeMap("nvarchar", "System.String"));
				meta.AddTypeMap("ClassificacaoAnbima", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("IdEventoProvisao", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("IdEventoPagamento", new esTypeMap("int", "System.Int32"));			
				
				
				
				meta.Source = "EventoFinanceiro";
				meta.Destination = "EventoFinanceiro";
				
				meta.spInsert = "proc_EventoFinanceiroInsert";				
				meta.spUpdate = "proc_EventoFinanceiroUpdate";		
				meta.spDelete = "proc_EventoFinanceiroDelete";
				meta.spLoadAll = "proc_EventoFinanceiroLoadAll";
				meta.spLoadByPrimaryKey = "proc_EventoFinanceiroLoadByPrimaryKey";
				
				this._providerMetadataMaps["esDefault"] = meta;
			}
			
			return this._providerMetadataMaps["esDefault"];
		}

		#endregion

		static private EventoFinanceiroMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
