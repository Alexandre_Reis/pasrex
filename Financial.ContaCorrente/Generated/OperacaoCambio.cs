/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 3/16/2015 4:29:04 PM
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;


using EntitySpaces.Interfaces;
using EntitySpaces.Core;
using Financial.Investidor;
using Financial.Common;





































		



			
					


		

		
		
		

		

























		






	

















				
		
		




		








		
				
				












	

















					
								
											
	










		





		




				
				








				




		

		
		
		
		
		





namespace Financial.ContaCorrente
{

	[Serializable]
	abstract public class esOperacaoCambioCollection : esEntityCollection
	{
		public esOperacaoCambioCollection()
		{

		}

		protected override string GetCollectionName()
		{
			return "OperacaoCambioCollection";
		}

		#region Query Logic
		protected void InitQuery(esOperacaoCambioQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntityCollection)this).Connection;
		}

		protected bool OnQueryLoaded(DataTable table)
		{
			this.PopulateCollection(table);
			return (this.RowCount > 0) ? true : false;
		}
		
		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery(query as esOperacaoCambioQuery);
		}
		#endregion
		
		virtual public OperacaoCambio DetachEntity(OperacaoCambio entity)
		{
			return base.DetachEntity(entity) as OperacaoCambio;
		}
		
		virtual public OperacaoCambio AttachEntity(OperacaoCambio entity)
		{
			return base.AttachEntity(entity) as OperacaoCambio;
		}
		
		virtual public void Combine(OperacaoCambioCollection collection)
		{
			base.Combine(collection);
		}
		
		new public OperacaoCambio this[int index]
		{
			get
			{
				return base[index] as OperacaoCambio;
			}
		}

		public override Type GetEntityType()
		{
			return typeof(OperacaoCambio);
		}
	}



	[Serializable]
	abstract public class esOperacaoCambio : esEntity
	{
		/// <summary>
		/// Used internally by the entity's DynamicQuery mechanism.
		/// </summary>
		virtual protected esOperacaoCambioQuery GetDynamicQuery()
		{
			return null;
		}

		public esOperacaoCambio()
		{

		}

		public esOperacaoCambio(DataRow row)
			: base(row)
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.Int32 idOperacao)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idOperacao);
			else
				return LoadByPrimaryKeyStoredProcedure(idOperacao);
		}

		/// <summary>
		/// Loads an entity by primary key
		/// </summary>
		/// <remarks>
		/// EntitySpaces requires primary keys be defined on all tables.
		/// If a table does not have a primary key set,
		/// this method will not compile.
		/// Does not support sqlAcessType. It only works with DynamicQuery
		/// </remarks>
		/// <param name="fieldsToReturn">Fields desired</param>
		public virtual bool LoadByPrimaryKey(List<esQueryItem> fieldsToReturn, System.Int32 idOperacao)
		{
			esQueryItem[] fields = fieldsToReturn.ToArray();			
			esOperacaoCambioQuery query = this.GetDynamicQuery();
			query
			    .Select(fields)
			    .Where(query.IdOperacao == idOperacao);
		
			return query.Load();
		}
		
		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.Int32 idOperacao)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idOperacao);
			else
				return LoadByPrimaryKeyStoredProcedure(idOperacao);
		}

		private bool LoadByPrimaryKeyDynamic(System.Int32 idOperacao)
		{
			esOperacaoCambioQuery query = this.GetDynamicQuery();
			query.Where(query.IdOperacao == idOperacao);
			return query.Load();
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.Int32 idOperacao)
		{
			esParameters parms = new esParameters();
			parms.Add("IdOperacao",idOperacao);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		
		
		#region Properties
		
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}

		public override void SetProperty(string name, object value)
		{
			if(this.Row == null) this.AddNew();
			
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value.GetType().ToString() == "System.String")
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "IdOperacao": this.str.IdOperacao = (string)value; break;							
						case "IdCliente": this.str.IdCliente = (string)value; break;							
						case "DataRegistro": this.str.DataRegistro = (string)value; break;							
						case "DataLiquidacao": this.str.DataLiquidacao = (string)value; break;							
						case "TaxaCambio": this.str.TaxaCambio = (string)value; break;							
						case "IdMoedaOrigem": this.str.IdMoedaOrigem = (string)value; break;							
						case "IdContaOrigem": this.str.IdContaOrigem = (string)value; break;							
						case "PrincipalOrigem": this.str.PrincipalOrigem = (string)value; break;							
						case "CustosOrigem": this.str.CustosOrigem = (string)value; break;							
						case "TributosOrigem": this.str.TributosOrigem = (string)value; break;							
						case "TotalOrigem": this.str.TotalOrigem = (string)value; break;							
						case "IdMoedaDestino": this.str.IdMoedaDestino = (string)value; break;							
						case "IdContaDestino": this.str.IdContaDestino = (string)value; break;							
						case "PrincipalDestino": this.str.PrincipalDestino = (string)value; break;							
						case "CustosDestino": this.str.CustosDestino = (string)value; break;							
						case "TributosDestino": this.str.TributosDestino = (string)value; break;							
						case "TotalDestino": this.str.TotalDestino = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "IdOperacao":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdOperacao = (System.Int32?)value;
							break;
						
						case "IdCliente":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdCliente = (System.Int32?)value;
							break;
						
						case "DataRegistro":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.DataRegistro = (System.DateTime?)value;
							break;
						
						case "DataLiquidacao":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.DataLiquidacao = (System.DateTime?)value;
							break;
						
						case "TaxaCambio":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.TaxaCambio = (System.Decimal?)value;
							break;
						
						case "IdMoedaOrigem":
						
							if (value == null || value.GetType().ToString() == "System.Int16")
								this.IdMoedaOrigem = (System.Int16?)value;
							break;
						
						case "IdContaOrigem":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdContaOrigem = (System.Int32?)value;
							break;
						
						case "PrincipalOrigem":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.PrincipalOrigem = (System.Decimal?)value;
							break;
						
						case "CustosOrigem":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.CustosOrigem = (System.Decimal?)value;
							break;
						
						case "TributosOrigem":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.TributosOrigem = (System.Decimal?)value;
							break;
						
						case "TotalOrigem":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.TotalOrigem = (System.Decimal?)value;
							break;
						
						case "IdMoedaDestino":
						
							if (value == null || value.GetType().ToString() == "System.Int16")
								this.IdMoedaDestino = (System.Int16?)value;
							break;
						
						case "IdContaDestino":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdContaDestino = (System.Int32?)value;
							break;
						
						case "PrincipalDestino":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.PrincipalDestino = (System.Decimal?)value;
							break;
						
						case "CustosDestino":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.CustosDestino = (System.Decimal?)value;
							break;
						
						case "TributosDestino":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.TributosDestino = (System.Decimal?)value;
							break;
						
						case "TotalDestino":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.TotalDestino = (System.Decimal?)value;
							break;
					

						default:
							break;
					}
				}
			}
			else if(this.Row.Table.Columns.Contains(name))
			{
				this.Row[name] = value;
			}
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}
		
		
		/// <summary>
		/// Maps to OperacaoCambio.IdOperacao
		/// </summary>
		virtual public System.Int32? IdOperacao
		{
			get
			{
				return base.GetSystemInt32(OperacaoCambioMetadata.ColumnNames.IdOperacao);
			}
			
			set
			{
				base.SetSystemInt32(OperacaoCambioMetadata.ColumnNames.IdOperacao, value);
			}
		}
		
		/// <summary>
		/// Maps to OperacaoCambio.IdCliente
		/// </summary>
		virtual public System.Int32? IdCliente
		{
			get
			{
				return base.GetSystemInt32(OperacaoCambioMetadata.ColumnNames.IdCliente);
			}
			
			set
			{
				if(base.SetSystemInt32(OperacaoCambioMetadata.ColumnNames.IdCliente, value))
				{
					this._UpToClienteByIdCliente = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to OperacaoCambio.DataRegistro
		/// </summary>
		virtual public System.DateTime? DataRegistro
		{
			get
			{
				return base.GetSystemDateTime(OperacaoCambioMetadata.ColumnNames.DataRegistro);
			}
			
			set
			{
				base.SetSystemDateTime(OperacaoCambioMetadata.ColumnNames.DataRegistro, value);
			}
		}
		
		/// <summary>
		/// Maps to OperacaoCambio.DataLiquidacao
		/// </summary>
		virtual public System.DateTime? DataLiquidacao
		{
			get
			{
				return base.GetSystemDateTime(OperacaoCambioMetadata.ColumnNames.DataLiquidacao);
			}
			
			set
			{
				base.SetSystemDateTime(OperacaoCambioMetadata.ColumnNames.DataLiquidacao, value);
			}
		}
		
		/// <summary>
		/// Maps to OperacaoCambio.TaxaCambio
		/// </summary>
		virtual public System.Decimal? TaxaCambio
		{
			get
			{
				return base.GetSystemDecimal(OperacaoCambioMetadata.ColumnNames.TaxaCambio);
			}
			
			set
			{
				base.SetSystemDecimal(OperacaoCambioMetadata.ColumnNames.TaxaCambio, value);
			}
		}
		
		/// <summary>
		/// Maps to OperacaoCambio.IdMoedaOrigem
		/// </summary>
		virtual public System.Int16? IdMoedaOrigem
		{
			get
			{
				return base.GetSystemInt16(OperacaoCambioMetadata.ColumnNames.IdMoedaOrigem);
			}
			
			set
			{
				if(base.SetSystemInt16(OperacaoCambioMetadata.ColumnNames.IdMoedaOrigem, value))
				{
					this._UpToMoedaByIdMoedaOrigem = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to OperacaoCambio.IdContaOrigem
		/// </summary>
		virtual public System.Int32? IdContaOrigem
		{
			get
			{
				return base.GetSystemInt32(OperacaoCambioMetadata.ColumnNames.IdContaOrigem);
			}
			
			set
			{
				if(base.SetSystemInt32(OperacaoCambioMetadata.ColumnNames.IdContaOrigem, value))
				{
					this._UpToContaCorrenteByIdContaOrigem = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to OperacaoCambio.PrincipalOrigem
		/// </summary>
		virtual public System.Decimal? PrincipalOrigem
		{
			get
			{
				return base.GetSystemDecimal(OperacaoCambioMetadata.ColumnNames.PrincipalOrigem);
			}
			
			set
			{
				base.SetSystemDecimal(OperacaoCambioMetadata.ColumnNames.PrincipalOrigem, value);
			}
		}
		
		/// <summary>
		/// Maps to OperacaoCambio.CustosOrigem
		/// </summary>
		virtual public System.Decimal? CustosOrigem
		{
			get
			{
				return base.GetSystemDecimal(OperacaoCambioMetadata.ColumnNames.CustosOrigem);
			}
			
			set
			{
				base.SetSystemDecimal(OperacaoCambioMetadata.ColumnNames.CustosOrigem, value);
			}
		}
		
		/// <summary>
		/// Maps to OperacaoCambio.TributosOrigem
		/// </summary>
		virtual public System.Decimal? TributosOrigem
		{
			get
			{
				return base.GetSystemDecimal(OperacaoCambioMetadata.ColumnNames.TributosOrigem);
			}
			
			set
			{
				base.SetSystemDecimal(OperacaoCambioMetadata.ColumnNames.TributosOrigem, value);
			}
		}
		
		/// <summary>
		/// Maps to OperacaoCambio.TotalOrigem
		/// </summary>
		virtual public System.Decimal? TotalOrigem
		{
			get
			{
				return base.GetSystemDecimal(OperacaoCambioMetadata.ColumnNames.TotalOrigem);
			}
			
			set
			{
				base.SetSystemDecimal(OperacaoCambioMetadata.ColumnNames.TotalOrigem, value);
			}
		}
		
		/// <summary>
		/// Maps to OperacaoCambio.IdMoedaDestino
		/// </summary>
		virtual public System.Int16? IdMoedaDestino
		{
			get
			{
				return base.GetSystemInt16(OperacaoCambioMetadata.ColumnNames.IdMoedaDestino);
			}
			
			set
			{
				if(base.SetSystemInt16(OperacaoCambioMetadata.ColumnNames.IdMoedaDestino, value))
				{
					this._UpToMoedaByIdMoedaDestino = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to OperacaoCambio.IdContaDestino
		/// </summary>
		virtual public System.Int32? IdContaDestino
		{
			get
			{
				return base.GetSystemInt32(OperacaoCambioMetadata.ColumnNames.IdContaDestino);
			}
			
			set
			{
				if(base.SetSystemInt32(OperacaoCambioMetadata.ColumnNames.IdContaDestino, value))
				{
					this._UpToContaCorrenteByIdContaDestino = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to OperacaoCambio.PrincipalDestino
		/// </summary>
		virtual public System.Decimal? PrincipalDestino
		{
			get
			{
				return base.GetSystemDecimal(OperacaoCambioMetadata.ColumnNames.PrincipalDestino);
			}
			
			set
			{
				base.SetSystemDecimal(OperacaoCambioMetadata.ColumnNames.PrincipalDestino, value);
			}
		}
		
		/// <summary>
		/// Maps to OperacaoCambio.CustosDestino
		/// </summary>
		virtual public System.Decimal? CustosDestino
		{
			get
			{
				return base.GetSystemDecimal(OperacaoCambioMetadata.ColumnNames.CustosDestino);
			}
			
			set
			{
				base.SetSystemDecimal(OperacaoCambioMetadata.ColumnNames.CustosDestino, value);
			}
		}
		
		/// <summary>
		/// Maps to OperacaoCambio.TributosDestino
		/// </summary>
		virtual public System.Decimal? TributosDestino
		{
			get
			{
				return base.GetSystemDecimal(OperacaoCambioMetadata.ColumnNames.TributosDestino);
			}
			
			set
			{
				base.SetSystemDecimal(OperacaoCambioMetadata.ColumnNames.TributosDestino, value);
			}
		}
		
		/// <summary>
		/// Maps to OperacaoCambio.TotalDestino
		/// </summary>
		virtual public System.Decimal? TotalDestino
		{
			get
			{
				return base.GetSystemDecimal(OperacaoCambioMetadata.ColumnNames.TotalDestino);
			}
			
			set
			{
				base.SetSystemDecimal(OperacaoCambioMetadata.ColumnNames.TotalDestino, value);
			}
		}
		
		[CLSCompliant(false)]
		internal protected Cliente _UpToClienteByIdCliente;
		[CLSCompliant(false)]
		internal protected Investidor.ContaCorrente _UpToContaCorrenteByIdContaOrigem;
		[CLSCompliant(false)]
        internal protected Investidor.ContaCorrente _UpToContaCorrenteByIdContaDestino;
		[CLSCompliant(false)]
		internal protected Moeda _UpToMoedaByIdMoedaDestino;
		[CLSCompliant(false)]
		internal protected Moeda _UpToMoedaByIdMoedaOrigem;
		#endregion	

		#region String Properties


		[BrowsableAttribute( false )]
		public esStrings str
		{
			get
			{
				if (esstrings == null)
				{
					esstrings = new esStrings(this);
				}
				return esstrings;
			}
		}


		[Serializable]
		sealed public class esStrings
		{
			public esStrings(esOperacaoCambio entity)
			{
				this.entity = entity;
			}
			
	
			public System.String IdOperacao
			{
				get
				{
					System.Int32? data = entity.IdOperacao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdOperacao = null;
					else entity.IdOperacao = Convert.ToInt32(value);
				}
			}
				
			public System.String IdCliente
			{
				get
				{
					System.Int32? data = entity.IdCliente;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdCliente = null;
					else entity.IdCliente = Convert.ToInt32(value);
				}
			}
				
			public System.String DataRegistro
			{
				get
				{
					System.DateTime? data = entity.DataRegistro;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DataRegistro = null;
					else entity.DataRegistro = Convert.ToDateTime(value);
				}
			}
				
			public System.String DataLiquidacao
			{
				get
				{
					System.DateTime? data = entity.DataLiquidacao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DataLiquidacao = null;
					else entity.DataLiquidacao = Convert.ToDateTime(value);
				}
			}
				
			public System.String TaxaCambio
			{
				get
				{
					System.Decimal? data = entity.TaxaCambio;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.TaxaCambio = null;
					else entity.TaxaCambio = Convert.ToDecimal(value);
				}
			}
				
			public System.String IdMoedaOrigem
			{
				get
				{
					System.Int16? data = entity.IdMoedaOrigem;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdMoedaOrigem = null;
					else entity.IdMoedaOrigem = Convert.ToInt16(value);
				}
			}
				
			public System.String IdContaOrigem
			{
				get
				{
					System.Int32? data = entity.IdContaOrigem;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdContaOrigem = null;
					else entity.IdContaOrigem = Convert.ToInt32(value);
				}
			}
				
			public System.String PrincipalOrigem
			{
				get
				{
					System.Decimal? data = entity.PrincipalOrigem;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.PrincipalOrigem = null;
					else entity.PrincipalOrigem = Convert.ToDecimal(value);
				}
			}
				
			public System.String CustosOrigem
			{
				get
				{
					System.Decimal? data = entity.CustosOrigem;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.CustosOrigem = null;
					else entity.CustosOrigem = Convert.ToDecimal(value);
				}
			}
				
			public System.String TributosOrigem
			{
				get
				{
					System.Decimal? data = entity.TributosOrigem;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.TributosOrigem = null;
					else entity.TributosOrigem = Convert.ToDecimal(value);
				}
			}
				
			public System.String TotalOrigem
			{
				get
				{
					System.Decimal? data = entity.TotalOrigem;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.TotalOrigem = null;
					else entity.TotalOrigem = Convert.ToDecimal(value);
				}
			}
				
			public System.String IdMoedaDestino
			{
				get
				{
					System.Int16? data = entity.IdMoedaDestino;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdMoedaDestino = null;
					else entity.IdMoedaDestino = Convert.ToInt16(value);
				}
			}
				
			public System.String IdContaDestino
			{
				get
				{
					System.Int32? data = entity.IdContaDestino;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdContaDestino = null;
					else entity.IdContaDestino = Convert.ToInt32(value);
				}
			}
				
			public System.String PrincipalDestino
			{
				get
				{
					System.Decimal? data = entity.PrincipalDestino;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.PrincipalDestino = null;
					else entity.PrincipalDestino = Convert.ToDecimal(value);
				}
			}
				
			public System.String CustosDestino
			{
				get
				{
					System.Decimal? data = entity.CustosDestino;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.CustosDestino = null;
					else entity.CustosDestino = Convert.ToDecimal(value);
				}
			}
				
			public System.String TributosDestino
			{
				get
				{
					System.Decimal? data = entity.TributosDestino;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.TributosDestino = null;
					else entity.TributosDestino = Convert.ToDecimal(value);
				}
			}
				
			public System.String TotalDestino
			{
				get
				{
					System.Decimal? data = entity.TotalDestino;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.TotalDestino = null;
					else entity.TotalDestino = Convert.ToDecimal(value);
				}
			}
			

			private esOperacaoCambio entity;
		}
		#endregion

		#region Query Logic
		protected void InitQuery(esOperacaoCambioQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntity)this).Connection;
		}

		[System.Diagnostics.DebuggerNonUserCode]
		protected bool OnQueryLoaded(DataTable table)
		{
			bool dataFound = this.PopulateEntity(table);

			if (this.RowCount > 1)
			{
				throw new Exception("esOperacaoCambio can only hold one record of data");
			}

			return dataFound;
		}
		#endregion
		
		[NonSerialized]
		private esStrings esstrings;
	}


	
	public partial class OperacaoCambio : esOperacaoCambio
	{

				
		#region UpToClienteByIdCliente - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - FK_OperacaoCambio_Cliente
		/// </summary>

		[XmlIgnore]
		public Cliente UpToClienteByIdCliente
		{
			get
			{
				if(this._UpToClienteByIdCliente == null
					&& IdCliente != null					)
				{
					this._UpToClienteByIdCliente = new Cliente();
					this._UpToClienteByIdCliente.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToClienteByIdCliente", this._UpToClienteByIdCliente);
					this._UpToClienteByIdCliente.Query.Where(this._UpToClienteByIdCliente.Query.IdCliente == this.IdCliente);
					this._UpToClienteByIdCliente.Query.Load();
				}

				return this._UpToClienteByIdCliente;
			}
			
			set
			{
				this.RemovePreSave("UpToClienteByIdCliente");
				

				if(value == null)
				{
					this.IdCliente = null;
					this._UpToClienteByIdCliente = null;
				}
				else
				{
					this.IdCliente = value.IdCliente;
					this._UpToClienteByIdCliente = value;
					this.SetPreSave("UpToClienteByIdCliente", this._UpToClienteByIdCliente);
				}
				
			}
		}
		#endregion
		

				
		#region UpToContaCorrenteByIdContaOrigem - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - FK_OperacaoCambio_ContaCorrente1
		/// </summary>

		[XmlIgnore]
        public Investidor.ContaCorrente UpToContaCorrenteByIdContaOrigem
		{
			get
			{
				if(this._UpToContaCorrenteByIdContaOrigem == null
					&& IdContaOrigem != null					)
				{
                    this._UpToContaCorrenteByIdContaOrigem = new Investidor.ContaCorrente();
					this._UpToContaCorrenteByIdContaOrigem.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToContaCorrenteByIdContaOrigem", this._UpToContaCorrenteByIdContaOrigem);
					this._UpToContaCorrenteByIdContaOrigem.Query.Where(this._UpToContaCorrenteByIdContaOrigem.Query.IdConta == this.IdContaOrigem);
					this._UpToContaCorrenteByIdContaOrigem.Query.Load();
				}

				return this._UpToContaCorrenteByIdContaOrigem;
			}
			
			set
			{
				this.RemovePreSave("UpToContaCorrenteByIdContaOrigem");
				

				if(value == null)
				{
					this.IdContaOrigem = null;
					this._UpToContaCorrenteByIdContaOrigem = null;
				}
				else
				{
					this.IdContaOrigem = value.IdConta;
					this._UpToContaCorrenteByIdContaOrigem = value;
					this.SetPreSave("UpToContaCorrenteByIdContaOrigem", this._UpToContaCorrenteByIdContaOrigem);
				}
				
			}
		}
		#endregion
		

				
		#region UpToContaCorrenteByIdContaDestino - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - FK_OperacaoCambio_ContaCorrente2
		/// </summary>

		[XmlIgnore]
        public Investidor.ContaCorrente UpToContaCorrenteByIdContaDestino
		{
			get
			{
				if(this._UpToContaCorrenteByIdContaDestino == null
					&& IdContaDestino != null					)
				{
                    this._UpToContaCorrenteByIdContaDestino = new Investidor.ContaCorrente();
					this._UpToContaCorrenteByIdContaDestino.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToContaCorrenteByIdContaDestino", this._UpToContaCorrenteByIdContaDestino);
					this._UpToContaCorrenteByIdContaDestino.Query.Where(this._UpToContaCorrenteByIdContaDestino.Query.IdConta == this.IdContaDestino);
					this._UpToContaCorrenteByIdContaDestino.Query.Load();
				}

				return this._UpToContaCorrenteByIdContaDestino;
			}
			
			set
			{
				this.RemovePreSave("UpToContaCorrenteByIdContaDestino");
				

				if(value == null)
				{
					this.IdContaDestino = null;
					this._UpToContaCorrenteByIdContaDestino = null;
				}
				else
				{
					this.IdContaDestino = value.IdConta;
					this._UpToContaCorrenteByIdContaDestino = value;
					this.SetPreSave("UpToContaCorrenteByIdContaDestino", this._UpToContaCorrenteByIdContaDestino);
				}
				
			}
		}
		#endregion
		

				
		#region UpToMoedaByIdMoedaDestino - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - FK_OperacaoCambio_Moeda1
		/// </summary>

		[XmlIgnore]
		public Moeda UpToMoedaByIdMoedaDestino
		{
			get
			{
				if(this._UpToMoedaByIdMoedaDestino == null
					&& IdMoedaDestino != null					)
				{
					this._UpToMoedaByIdMoedaDestino = new Moeda();
					this._UpToMoedaByIdMoedaDestino.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToMoedaByIdMoedaDestino", this._UpToMoedaByIdMoedaDestino);
					this._UpToMoedaByIdMoedaDestino.Query.Where(this._UpToMoedaByIdMoedaDestino.Query.IdMoeda == this.IdMoedaDestino);
					this._UpToMoedaByIdMoedaDestino.Query.Load();
				}

				return this._UpToMoedaByIdMoedaDestino;
			}
			
			set
			{
				this.RemovePreSave("UpToMoedaByIdMoedaDestino");
				

				if(value == null)
				{
					this.IdMoedaDestino = null;
					this._UpToMoedaByIdMoedaDestino = null;
				}
				else
				{
					this.IdMoedaDestino = value.IdMoeda;
					this._UpToMoedaByIdMoedaDestino = value;
					this.SetPreSave("UpToMoedaByIdMoedaDestino", this._UpToMoedaByIdMoedaDestino);
				}
				
			}
		}
		#endregion
		

				
		#region UpToMoedaByIdMoedaOrigem - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - FK_OperacaoCambio_Moeda2
		/// </summary>

		[XmlIgnore]
		public Moeda UpToMoedaByIdMoedaOrigem
		{
			get
			{
				if(this._UpToMoedaByIdMoedaOrigem == null
					&& IdMoedaOrigem != null					)
				{
					this._UpToMoedaByIdMoedaOrigem = new Moeda();
					this._UpToMoedaByIdMoedaOrigem.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToMoedaByIdMoedaOrigem", this._UpToMoedaByIdMoedaOrigem);
					this._UpToMoedaByIdMoedaOrigem.Query.Where(this._UpToMoedaByIdMoedaOrigem.Query.IdMoeda == this.IdMoedaOrigem);
					this._UpToMoedaByIdMoedaOrigem.Query.Load();
				}

				return this._UpToMoedaByIdMoedaOrigem;
			}
			
			set
			{
				this.RemovePreSave("UpToMoedaByIdMoedaOrigem");
				

				if(value == null)
				{
					this.IdMoedaOrigem = null;
					this._UpToMoedaByIdMoedaOrigem = null;
				}
				else
				{
					this.IdMoedaOrigem = value.IdMoeda;
					this._UpToMoedaByIdMoedaOrigem = value;
					this.SetPreSave("UpToMoedaByIdMoedaOrigem", this._UpToMoedaByIdMoedaOrigem);
				}
				
			}
		}
		#endregion
		

		
		/// <summary>
		/// Used internally by the entity's hierarchical properties.
		/// </summary>
		protected override List<esPropertyDescriptor> GetHierarchicalProperties()
		{
			List<esPropertyDescriptor> props = new List<esPropertyDescriptor>();
			
		
			return props;
		}	
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PreSave.
		/// </summary>
		protected override void ApplyPreSaveKeys()
		{
			if(!this.es.IsDeleted && this._UpToContaCorrenteByIdContaOrigem != null)
			{
				this.IdContaOrigem = this._UpToContaCorrenteByIdContaOrigem.IdConta;
			}
			if(!this.es.IsDeleted && this._UpToContaCorrenteByIdContaDestino != null)
			{
				this.IdContaDestino = this._UpToContaCorrenteByIdContaDestino.IdConta;
			}
			if(!this.es.IsDeleted && this._UpToMoedaByIdMoedaDestino != null)
			{
				this.IdMoedaDestino = this._UpToMoedaByIdMoedaDestino.IdMoeda;
			}
			if(!this.es.IsDeleted && this._UpToMoedaByIdMoedaOrigem != null)
			{
				this.IdMoedaOrigem = this._UpToMoedaByIdMoedaOrigem.IdMoeda;
			}
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostSave.
		/// </summary>
		protected override void ApplyPostSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostOneToOneSave.
		/// </summary>
		protected override void ApplyPostOneSaveKeys()
		{
		}
		
	}



	[Serializable]
	abstract public class esOperacaoCambioQuery : esDynamicQuery
	{
		override protected IMetadata Meta
		{
			get
			{
				return OperacaoCambioMetadata.Meta();
			}
		}	
		

		public esQueryItem IdOperacao
		{
			get
			{
				return new esQueryItem(this, OperacaoCambioMetadata.ColumnNames.IdOperacao, esSystemType.Int32);
			}
		} 
		
		public esQueryItem IdCliente
		{
			get
			{
				return new esQueryItem(this, OperacaoCambioMetadata.ColumnNames.IdCliente, esSystemType.Int32);
			}
		} 
		
		public esQueryItem DataRegistro
		{
			get
			{
				return new esQueryItem(this, OperacaoCambioMetadata.ColumnNames.DataRegistro, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem DataLiquidacao
		{
			get
			{
				return new esQueryItem(this, OperacaoCambioMetadata.ColumnNames.DataLiquidacao, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem TaxaCambio
		{
			get
			{
				return new esQueryItem(this, OperacaoCambioMetadata.ColumnNames.TaxaCambio, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem IdMoedaOrigem
		{
			get
			{
				return new esQueryItem(this, OperacaoCambioMetadata.ColumnNames.IdMoedaOrigem, esSystemType.Int16);
			}
		} 
		
		public esQueryItem IdContaOrigem
		{
			get
			{
				return new esQueryItem(this, OperacaoCambioMetadata.ColumnNames.IdContaOrigem, esSystemType.Int32);
			}
		} 
		
		public esQueryItem PrincipalOrigem
		{
			get
			{
				return new esQueryItem(this, OperacaoCambioMetadata.ColumnNames.PrincipalOrigem, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem CustosOrigem
		{
			get
			{
				return new esQueryItem(this, OperacaoCambioMetadata.ColumnNames.CustosOrigem, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem TributosOrigem
		{
			get
			{
				return new esQueryItem(this, OperacaoCambioMetadata.ColumnNames.TributosOrigem, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem TotalOrigem
		{
			get
			{
				return new esQueryItem(this, OperacaoCambioMetadata.ColumnNames.TotalOrigem, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem IdMoedaDestino
		{
			get
			{
				return new esQueryItem(this, OperacaoCambioMetadata.ColumnNames.IdMoedaDestino, esSystemType.Int16);
			}
		} 
		
		public esQueryItem IdContaDestino
		{
			get
			{
				return new esQueryItem(this, OperacaoCambioMetadata.ColumnNames.IdContaDestino, esSystemType.Int32);
			}
		} 
		
		public esQueryItem PrincipalDestino
		{
			get
			{
				return new esQueryItem(this, OperacaoCambioMetadata.ColumnNames.PrincipalDestino, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem CustosDestino
		{
			get
			{
				return new esQueryItem(this, OperacaoCambioMetadata.ColumnNames.CustosDestino, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem TributosDestino
		{
			get
			{
				return new esQueryItem(this, OperacaoCambioMetadata.ColumnNames.TributosDestino, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem TotalDestino
		{
			get
			{
				return new esQueryItem(this, OperacaoCambioMetadata.ColumnNames.TotalDestino, esSystemType.Decimal);
			}
		} 
		
	}



	[Serializable]
	[XmlType("OperacaoCambioCollection")]
	public partial class OperacaoCambioCollection : esOperacaoCambioCollection, IEnumerable<OperacaoCambio>
	{
		public OperacaoCambioCollection()
		{

		}
		
		public static implicit operator List<OperacaoCambio>(OperacaoCambioCollection coll)
		{
			List<OperacaoCambio> list = new List<OperacaoCambio>();
			
			foreach (OperacaoCambio emp in coll)
			{
				list.Add(emp);
			}
			
			return list;
		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return  OperacaoCambioMetadata.Meta();
			}
		}
		
		
		
		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new OperacaoCambioQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		
		override protected esEntity CreateEntityForCollection(DataRow row)
		{
			return new OperacaoCambio(row);
		}

		override protected esEntity CreateEntity()
		{
			return new OperacaoCambio();
		}
		
		
		#endregion


		[BrowsableAttribute( false )]
		public OperacaoCambioQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new OperacaoCambioQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}

		public bool Load(OperacaoCambioQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		public OperacaoCambio AddNew()
		{
			OperacaoCambio entity = base.AddNewEntity() as OperacaoCambio;
			
			return entity;
		}

		public OperacaoCambio FindByPrimaryKey(System.Int32 idOperacao)
		{
			return base.FindByPrimaryKey(idOperacao) as OperacaoCambio;
		}


		#region IEnumerable<OperacaoCambio> Members

		IEnumerator<OperacaoCambio> IEnumerable<OperacaoCambio>.GetEnumerator()
		{
			System.Collections.IEnumerable enumer = this as System.Collections.IEnumerable;
			System.Collections.IEnumerator iterator = enumer.GetEnumerator();

			while(iterator.MoveNext())
			{
				yield return iterator.Current as OperacaoCambio;
			}
		}

		#endregion
		
		private OperacaoCambioQuery query;
	}


	/// <summary>
	/// Encapsulates the 'OperacaoCambio' table
	/// </summary>

	[Serializable]
	public partial class OperacaoCambio : esOperacaoCambio
	{
		public OperacaoCambio()
		{

		}
	
		public OperacaoCambio(DataRow row)
			: base(row)
		{

		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return OperacaoCambioMetadata.Meta();
			}
		}
		
		
		
		override protected esOperacaoCambioQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new OperacaoCambioQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		#endregion
		



		[BrowsableAttribute( false )]
		public OperacaoCambioQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new OperacaoCambioQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}
		

		public bool Load(OperacaoCambioQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		private OperacaoCambioQuery query;
	}



	[Serializable]
	public partial class OperacaoCambioQuery : esOperacaoCambioQuery
	{
		public OperacaoCambioQuery()
		{

		}		
		
		public OperacaoCambioQuery(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
			
	}



	[Serializable]
	public partial class OperacaoCambioMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected OperacaoCambioMetadata()
		{
			_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(OperacaoCambioMetadata.ColumnNames.IdOperacao, 0, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = OperacaoCambioMetadata.PropertyNames.IdOperacao;
			c.IsInPrimaryKey = true;
			c.IsAutoIncrement = true;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OperacaoCambioMetadata.ColumnNames.IdCliente, 1, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = OperacaoCambioMetadata.PropertyNames.IdCliente;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OperacaoCambioMetadata.ColumnNames.DataRegistro, 2, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = OperacaoCambioMetadata.PropertyNames.DataRegistro;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OperacaoCambioMetadata.ColumnNames.DataLiquidacao, 3, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = OperacaoCambioMetadata.PropertyNames.DataLiquidacao;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OperacaoCambioMetadata.ColumnNames.TaxaCambio, 4, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = OperacaoCambioMetadata.PropertyNames.TaxaCambio;	
			c.NumericPrecision = 16;
			c.NumericScale = 4;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OperacaoCambioMetadata.ColumnNames.IdMoedaOrigem, 5, typeof(System.Int16), esSystemType.Int16);
			c.PropertyName = OperacaoCambioMetadata.PropertyNames.IdMoedaOrigem;	
			c.NumericPrecision = 5;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OperacaoCambioMetadata.ColumnNames.IdContaOrigem, 6, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = OperacaoCambioMetadata.PropertyNames.IdContaOrigem;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OperacaoCambioMetadata.ColumnNames.PrincipalOrigem, 7, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = OperacaoCambioMetadata.PropertyNames.PrincipalOrigem;	
			c.NumericPrecision = 30;
			c.NumericScale = 2;
			c.HasDefault = true;
			c.Default = @"((0))";
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OperacaoCambioMetadata.ColumnNames.CustosOrigem, 8, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = OperacaoCambioMetadata.PropertyNames.CustosOrigem;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			c.HasDefault = true;
			c.Default = @"((0))";
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OperacaoCambioMetadata.ColumnNames.TributosOrigem, 9, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = OperacaoCambioMetadata.PropertyNames.TributosOrigem;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			c.HasDefault = true;
			c.Default = @"((0))";
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OperacaoCambioMetadata.ColumnNames.TotalOrigem, 10, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = OperacaoCambioMetadata.PropertyNames.TotalOrigem;	
			c.NumericPrecision = 30;
			c.NumericScale = 2;
			c.HasDefault = true;
			c.Default = @"((0))";
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OperacaoCambioMetadata.ColumnNames.IdMoedaDestino, 11, typeof(System.Int16), esSystemType.Int16);
			c.PropertyName = OperacaoCambioMetadata.PropertyNames.IdMoedaDestino;	
			c.NumericPrecision = 5;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OperacaoCambioMetadata.ColumnNames.IdContaDestino, 12, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = OperacaoCambioMetadata.PropertyNames.IdContaDestino;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OperacaoCambioMetadata.ColumnNames.PrincipalDestino, 13, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = OperacaoCambioMetadata.PropertyNames.PrincipalDestino;	
			c.NumericPrecision = 30;
			c.NumericScale = 2;
			c.HasDefault = true;
			c.Default = @"((0))";
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OperacaoCambioMetadata.ColumnNames.CustosDestino, 14, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = OperacaoCambioMetadata.PropertyNames.CustosDestino;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			c.HasDefault = true;
			c.Default = @"((0))";
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OperacaoCambioMetadata.ColumnNames.TributosDestino, 15, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = OperacaoCambioMetadata.PropertyNames.TributosDestino;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			c.HasDefault = true;
			c.Default = @"((0))";
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OperacaoCambioMetadata.ColumnNames.TotalDestino, 16, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = OperacaoCambioMetadata.PropertyNames.TotalDestino;	
			c.NumericPrecision = 30;
			c.NumericScale = 2;
			c.HasDefault = true;
			c.Default = @"((0))";
			_columns.Add(c); 
			
				
		}
		#endregion
	
		static public OperacaoCambioMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base._dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base._columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string IdOperacao = "IdOperacao";
			 public const string IdCliente = "IdCliente";
			 public const string DataRegistro = "DataRegistro";
			 public const string DataLiquidacao = "DataLiquidacao";
			 public const string TaxaCambio = "TaxaCambio";
			 public const string IdMoedaOrigem = "IdMoedaOrigem";
			 public const string IdContaOrigem = "IdContaOrigem";
			 public const string PrincipalOrigem = "PrincipalOrigem";
			 public const string CustosOrigem = "CustosOrigem";
			 public const string TributosOrigem = "TributosOrigem";
			 public const string TotalOrigem = "TotalOrigem";
			 public const string IdMoedaDestino = "IdMoedaDestino";
			 public const string IdContaDestino = "IdContaDestino";
			 public const string PrincipalDestino = "PrincipalDestino";
			 public const string CustosDestino = "CustosDestino";
			 public const string TributosDestino = "TributosDestino";
			 public const string TotalDestino = "TotalDestino";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string IdOperacao = "IdOperacao";
			 public const string IdCliente = "IdCliente";
			 public const string DataRegistro = "DataRegistro";
			 public const string DataLiquidacao = "DataLiquidacao";
			 public const string TaxaCambio = "TaxaCambio";
			 public const string IdMoedaOrigem = "IdMoedaOrigem";
			 public const string IdContaOrigem = "IdContaOrigem";
			 public const string PrincipalOrigem = "PrincipalOrigem";
			 public const string CustosOrigem = "CustosOrigem";
			 public const string TributosOrigem = "TributosOrigem";
			 public const string TotalOrigem = "TotalOrigem";
			 public const string IdMoedaDestino = "IdMoedaDestino";
			 public const string IdContaDestino = "IdContaDestino";
			 public const string PrincipalDestino = "PrincipalDestino";
			 public const string CustosDestino = "CustosDestino";
			 public const string TributosDestino = "TributosDestino";
			 public const string TotalDestino = "TotalDestino";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(OperacaoCambioMetadata))
			{
				if(OperacaoCambioMetadata.mapDelegates == null)
				{
					OperacaoCambioMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (OperacaoCambioMetadata.meta == null)
				{
					OperacaoCambioMetadata.meta = new OperacaoCambioMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();
				

				meta.AddTypeMap("IdOperacao", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("IdCliente", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("DataRegistro", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("DataLiquidacao", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("TaxaCambio", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("IdMoedaOrigem", new esTypeMap("smallint", "System.Int16"));
				meta.AddTypeMap("IdContaOrigem", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("PrincipalOrigem", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("CustosOrigem", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("TributosOrigem", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("TotalOrigem", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("IdMoedaDestino", new esTypeMap("smallint", "System.Int16"));
				meta.AddTypeMap("IdContaDestino", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("PrincipalDestino", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("CustosDestino", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("TributosDestino", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("TotalDestino", new esTypeMap("decimal", "System.Decimal"));			
				
				
				
				meta.Source = "OperacaoCambio";
				meta.Destination = "OperacaoCambio";
				
				meta.spInsert = "proc_OperacaoCambioInsert";				
				meta.spUpdate = "proc_OperacaoCambioUpdate";		
				meta.spDelete = "proc_OperacaoCambioDelete";
				meta.spLoadAll = "proc_OperacaoCambioLoadAll";
				meta.spLoadByPrimaryKey = "proc_OperacaoCambioLoadByPrimaryKey";
				
				this._providerMetadataMaps["esDefault"] = meta;
			}
			
			return this._providerMetadataMaps["esDefault"];
		}

		#endregion

		static private OperacaoCambioMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
