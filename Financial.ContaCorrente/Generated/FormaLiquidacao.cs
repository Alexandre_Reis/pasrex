/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 1/6/2015 6:54:26 PM
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;


using EntitySpaces.Interfaces;
using EntitySpaces.Core;





































		



			
					


		

		
		
		

		

























		






	


using Financial.InvestidorCotista;
using Financial.Fundo;
















				
		
		




		








		
				
				












	

















					
								
											
	










		





		




				
				








				




		

		
		
		
		
		





namespace Financial.ContaCorrente
{

	[Serializable]
	abstract public class esFormaLiquidacaoCollection : esEntityCollection
	{
		public esFormaLiquidacaoCollection()
		{

		}

		protected override string GetCollectionName()
		{
			return "FormaLiquidacaoCollection";
		}

		#region Query Logic
		protected void InitQuery(esFormaLiquidacaoQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntityCollection)this).Connection;
		}

		protected bool OnQueryLoaded(DataTable table)
		{
			this.PopulateCollection(table);
			return (this.RowCount > 0) ? true : false;
		}
		
		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery(query as esFormaLiquidacaoQuery);
		}
		#endregion
		
		virtual public FormaLiquidacao DetachEntity(FormaLiquidacao entity)
		{
			return base.DetachEntity(entity) as FormaLiquidacao;
		}
		
		virtual public FormaLiquidacao AttachEntity(FormaLiquidacao entity)
		{
			return base.AttachEntity(entity) as FormaLiquidacao;
		}
		
		virtual public void Combine(FormaLiquidacaoCollection collection)
		{
			base.Combine(collection);
		}
		
		new public FormaLiquidacao this[int index]
		{
			get
			{
				return base[index] as FormaLiquidacao;
			}
		}

		public override Type GetEntityType()
		{
			return typeof(FormaLiquidacao);
		}
	}



	[Serializable]
	abstract public class esFormaLiquidacao : esEntity
	{
		/// <summary>
		/// Used internally by the entity's DynamicQuery mechanism.
		/// </summary>
		virtual protected esFormaLiquidacaoQuery GetDynamicQuery()
		{
			return null;
		}

		public esFormaLiquidacao()
		{

		}

		public esFormaLiquidacao(DataRow row)
			: base(row)
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.Byte idFormaLiquidacao)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idFormaLiquidacao);
			else
				return LoadByPrimaryKeyStoredProcedure(idFormaLiquidacao);
		}

		/// <summary>
		/// Loads an entity by primary key
		/// </summary>
		/// <remarks>
		/// EntitySpaces requires primary keys be defined on all tables.
		/// If a table does not have a primary key set,
		/// this method will not compile.
		/// Does not support sqlAcessType. It only works with DynamicQuery
		/// </remarks>
		/// <param name="fieldsToReturn">Fields desired</param>
		public virtual bool LoadByPrimaryKey(List<esQueryItem> fieldsToReturn, System.Byte idFormaLiquidacao)
		{
			esQueryItem[] fields = fieldsToReturn.ToArray();			
			esFormaLiquidacaoQuery query = this.GetDynamicQuery();
			query
			    .Select(fields)
			    .Where(query.IdFormaLiquidacao == idFormaLiquidacao);
		
			return query.Load();
		}
		
		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.Byte idFormaLiquidacao)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idFormaLiquidacao);
			else
				return LoadByPrimaryKeyStoredProcedure(idFormaLiquidacao);
		}

		private bool LoadByPrimaryKeyDynamic(System.Byte idFormaLiquidacao)
		{
			esFormaLiquidacaoQuery query = this.GetDynamicQuery();
			query.Where(query.IdFormaLiquidacao == idFormaLiquidacao);
			return query.Load();
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.Byte idFormaLiquidacao)
		{
			esParameters parms = new esParameters();
			parms.Add("IdFormaLiquidacao",idFormaLiquidacao);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		
		
		#region Properties
		
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}

		public override void SetProperty(string name, object value)
		{
			if(this.Row == null) this.AddNew();
			
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value.GetType().ToString() == "System.String")
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "IdFormaLiquidacao": this.str.IdFormaLiquidacao = (string)value; break;							
						case "Descricao": this.str.Descricao = (string)value; break;							
						case "TipoLiquidacao": this.str.TipoLiquidacao = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "IdFormaLiquidacao":
						
							if (value == null || value.GetType().ToString() == "System.Byte")
								this.IdFormaLiquidacao = (System.Byte?)value;
							break;
						
						case "TipoLiquidacao":
						
							if (value == null || value.GetType().ToString() == "System.Byte")
								this.TipoLiquidacao = (System.Byte?)value;
							break;
					

						default:
							break;
					}
				}
			}
			else if(this.Row.Table.Columns.Contains(name))
			{
				this.Row[name] = value;
			}
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}
		
		
		/// <summary>
		/// Maps to FormaLiquidacao.IdFormaLiquidacao
		/// </summary>
		virtual public System.Byte? IdFormaLiquidacao
		{
			get
			{
				return base.GetSystemByte(FormaLiquidacaoMetadata.ColumnNames.IdFormaLiquidacao);
			}
			
			set
			{
				base.SetSystemByte(FormaLiquidacaoMetadata.ColumnNames.IdFormaLiquidacao, value);
			}
		}
		
		/// <summary>
		/// Maps to FormaLiquidacao.Descricao
		/// </summary>
		virtual public System.String Descricao
		{
			get
			{
				return base.GetSystemString(FormaLiquidacaoMetadata.ColumnNames.Descricao);
			}
			
			set
			{
				base.SetSystemString(FormaLiquidacaoMetadata.ColumnNames.Descricao, value);
			}
		}
		
		/// <summary>
		/// Maps to FormaLiquidacao.TipoLiquidacao
		/// </summary>
		virtual public System.Byte? TipoLiquidacao
		{
			get
			{
				return base.GetSystemByte(FormaLiquidacaoMetadata.ColumnNames.TipoLiquidacao);
			}
			
			set
			{
				base.SetSystemByte(FormaLiquidacaoMetadata.ColumnNames.TipoLiquidacao, value);
			}
		}
		
		#endregion	

		#region String Properties


		[BrowsableAttribute( false )]
		public esStrings str
		{
			get
			{
				if (esstrings == null)
				{
					esstrings = new esStrings(this);
				}
				return esstrings;
			}
		}


		[Serializable]
		sealed public class esStrings
		{
			public esStrings(esFormaLiquidacao entity)
			{
				this.entity = entity;
			}
			
	
			public System.String IdFormaLiquidacao
			{
				get
				{
					System.Byte? data = entity.IdFormaLiquidacao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdFormaLiquidacao = null;
					else entity.IdFormaLiquidacao = Convert.ToByte(value);
				}
			}
				
			public System.String Descricao
			{
				get
				{
					System.String data = entity.Descricao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Descricao = null;
					else entity.Descricao = Convert.ToString(value);
				}
			}
				
			public System.String TipoLiquidacao
			{
				get
				{
					System.Byte? data = entity.TipoLiquidacao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.TipoLiquidacao = null;
					else entity.TipoLiquidacao = Convert.ToByte(value);
				}
			}
			

			private esFormaLiquidacao entity;
		}
		#endregion

		#region Query Logic
		protected void InitQuery(esFormaLiquidacaoQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntity)this).Connection;
		}

		[System.Diagnostics.DebuggerNonUserCode]
		protected bool OnQueryLoaded(DataTable table)
		{
			bool dataFound = this.PopulateEntity(table);

			if (this.RowCount > 1)
			{
				throw new Exception("esFormaLiquidacao can only hold one record of data");
			}

			return dataFound;
		}
		#endregion
		
		[NonSerialized]
		private esStrings esstrings;
	}


	
	public partial class FormaLiquidacao : esFormaLiquidacao
	{

				
		#region OperacaoCotistaCollectionByIdFormaLiquidacao - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - FormaLiquidacao_OperacaoCotista_FK1
		/// </summary>

		[XmlIgnore]
		public OperacaoCotistaCollection OperacaoCotistaCollectionByIdFormaLiquidacao
		{
			get
			{
				if(this._OperacaoCotistaCollectionByIdFormaLiquidacao == null)
				{
					this._OperacaoCotistaCollectionByIdFormaLiquidacao = new OperacaoCotistaCollection();
					this._OperacaoCotistaCollectionByIdFormaLiquidacao.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("OperacaoCotistaCollectionByIdFormaLiquidacao", this._OperacaoCotistaCollectionByIdFormaLiquidacao);
				
					if(this.IdFormaLiquidacao != null)
					{
						this._OperacaoCotistaCollectionByIdFormaLiquidacao.Query.Where(this._OperacaoCotistaCollectionByIdFormaLiquidacao.Query.IdFormaLiquidacao == this.IdFormaLiquidacao);
						this._OperacaoCotistaCollectionByIdFormaLiquidacao.Query.Load();

						// Auto-hookup Foreign Keys
						this._OperacaoCotistaCollectionByIdFormaLiquidacao.fks.Add(OperacaoCotistaMetadata.ColumnNames.IdFormaLiquidacao, this.IdFormaLiquidacao);
					}
				}

				return this._OperacaoCotistaCollectionByIdFormaLiquidacao;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._OperacaoCotistaCollectionByIdFormaLiquidacao != null) 
				{ 
					this.RemovePostSave("OperacaoCotistaCollectionByIdFormaLiquidacao"); 
					this._OperacaoCotistaCollectionByIdFormaLiquidacao = null;
					
				} 
			} 			
		}

		private OperacaoCotistaCollection _OperacaoCotistaCollectionByIdFormaLiquidacao;
		#endregion

				
		#region OperacaoFundoCollectionByIdFormaLiquidacao - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - FormaLiquidacao_OperacaoFundo_FK1
		/// </summary>

		[XmlIgnore]
		public OperacaoFundoCollection OperacaoFundoCollectionByIdFormaLiquidacao
		{
			get
			{
				if(this._OperacaoFundoCollectionByIdFormaLiquidacao == null)
				{
					this._OperacaoFundoCollectionByIdFormaLiquidacao = new OperacaoFundoCollection();
					this._OperacaoFundoCollectionByIdFormaLiquidacao.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("OperacaoFundoCollectionByIdFormaLiquidacao", this._OperacaoFundoCollectionByIdFormaLiquidacao);
				
					if(this.IdFormaLiquidacao != null)
					{
						this._OperacaoFundoCollectionByIdFormaLiquidacao.Query.Where(this._OperacaoFundoCollectionByIdFormaLiquidacao.Query.IdFormaLiquidacao == this.IdFormaLiquidacao);
						this._OperacaoFundoCollectionByIdFormaLiquidacao.Query.Load();

						// Auto-hookup Foreign Keys
						this._OperacaoFundoCollectionByIdFormaLiquidacao.fks.Add(OperacaoFundoMetadata.ColumnNames.IdFormaLiquidacao, this.IdFormaLiquidacao);
					}
				}

				return this._OperacaoFundoCollectionByIdFormaLiquidacao;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._OperacaoFundoCollectionByIdFormaLiquidacao != null) 
				{ 
					this.RemovePostSave("OperacaoFundoCollectionByIdFormaLiquidacao"); 
					this._OperacaoFundoCollectionByIdFormaLiquidacao = null;
					
				} 
			} 			
		}

		private OperacaoFundoCollection _OperacaoFundoCollectionByIdFormaLiquidacao;
		#endregion

				
		#region OrdemCotistaCollectionByIdFormaLiquidacao - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - FormaLiquidacao_OrdemCotista_FK1
		/// </summary>

		[XmlIgnore]
		public OrdemCotistaCollection OrdemCotistaCollectionByIdFormaLiquidacao
		{
			get
			{
				if(this._OrdemCotistaCollectionByIdFormaLiquidacao == null)
				{
					this._OrdemCotistaCollectionByIdFormaLiquidacao = new OrdemCotistaCollection();
					this._OrdemCotistaCollectionByIdFormaLiquidacao.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("OrdemCotistaCollectionByIdFormaLiquidacao", this._OrdemCotistaCollectionByIdFormaLiquidacao);
				
					if(this.IdFormaLiquidacao != null)
					{
						this._OrdemCotistaCollectionByIdFormaLiquidacao.Query.Where(this._OrdemCotistaCollectionByIdFormaLiquidacao.Query.IdFormaLiquidacao == this.IdFormaLiquidacao);
						this._OrdemCotistaCollectionByIdFormaLiquidacao.Query.Load();

						// Auto-hookup Foreign Keys
						this._OrdemCotistaCollectionByIdFormaLiquidacao.fks.Add(OrdemCotistaMetadata.ColumnNames.IdFormaLiquidacao, this.IdFormaLiquidacao);
					}
				}

				return this._OrdemCotistaCollectionByIdFormaLiquidacao;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._OrdemCotistaCollectionByIdFormaLiquidacao != null) 
				{ 
					this.RemovePostSave("OrdemCotistaCollectionByIdFormaLiquidacao"); 
					this._OrdemCotistaCollectionByIdFormaLiquidacao = null;
					
				} 
			} 			
		}

		private OrdemCotistaCollection _OrdemCotistaCollectionByIdFormaLiquidacao;
		#endregion

				
		#region OrdemFundoCollectionByIdFormaLiquidacao - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - FormaLiquidacao_OrdemFundo_FK1
		/// </summary>

		[XmlIgnore]
		public OrdemFundoCollection OrdemFundoCollectionByIdFormaLiquidacao
		{
			get
			{
				if(this._OrdemFundoCollectionByIdFormaLiquidacao == null)
				{
					this._OrdemFundoCollectionByIdFormaLiquidacao = new OrdemFundoCollection();
					this._OrdemFundoCollectionByIdFormaLiquidacao.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("OrdemFundoCollectionByIdFormaLiquidacao", this._OrdemFundoCollectionByIdFormaLiquidacao);
				
					if(this.IdFormaLiquidacao != null)
					{
						this._OrdemFundoCollectionByIdFormaLiquidacao.Query.Where(this._OrdemFundoCollectionByIdFormaLiquidacao.Query.IdFormaLiquidacao == this.IdFormaLiquidacao);
						this._OrdemFundoCollectionByIdFormaLiquidacao.Query.Load();

						// Auto-hookup Foreign Keys
						this._OrdemFundoCollectionByIdFormaLiquidacao.fks.Add(OrdemFundoMetadata.ColumnNames.IdFormaLiquidacao, this.IdFormaLiquidacao);
					}
				}

				return this._OrdemFundoCollectionByIdFormaLiquidacao;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._OrdemFundoCollectionByIdFormaLiquidacao != null) 
				{ 
					this.RemovePostSave("OrdemFundoCollectionByIdFormaLiquidacao"); 
					this._OrdemFundoCollectionByIdFormaLiquidacao = null;
					
				} 
			} 			
		}

		private OrdemFundoCollection _OrdemFundoCollectionByIdFormaLiquidacao;
		#endregion

		
		/// <summary>
		/// Used internally by the entity's hierarchical properties.
		/// </summary>
		protected override List<esPropertyDescriptor> GetHierarchicalProperties()
		{
			List<esPropertyDescriptor> props = new List<esPropertyDescriptor>();
			
			props.Add(new esPropertyDescriptor(this, "OperacaoCotistaCollectionByIdFormaLiquidacao", typeof(OperacaoCotistaCollection), new OperacaoCotista()));
			props.Add(new esPropertyDescriptor(this, "OperacaoFundoCollectionByIdFormaLiquidacao", typeof(OperacaoFundoCollection), new OperacaoFundo()));
			props.Add(new esPropertyDescriptor(this, "OrdemCotistaCollectionByIdFormaLiquidacao", typeof(OrdemCotistaCollection), new OrdemCotista()));
			props.Add(new esPropertyDescriptor(this, "OrdemFundoCollectionByIdFormaLiquidacao", typeof(OrdemFundoCollection), new OrdemFundo()));
		
			return props;
		}	
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PreSave.
		/// </summary>
		protected override void ApplyPreSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostSave.
		/// </summary>
		protected override void ApplyPostSaveKeys()
		{
			if(this._OperacaoCotistaCollectionByIdFormaLiquidacao != null)
			{
				foreach(OperacaoCotista obj in this._OperacaoCotistaCollectionByIdFormaLiquidacao)
				{
					if(obj.es.IsAdded)
					{
						obj.IdFormaLiquidacao = this.IdFormaLiquidacao;
					}
				}
			}
			if(this._OperacaoFundoCollectionByIdFormaLiquidacao != null)
			{
				foreach(OperacaoFundo obj in this._OperacaoFundoCollectionByIdFormaLiquidacao)
				{
					if(obj.es.IsAdded)
					{
						obj.IdFormaLiquidacao = this.IdFormaLiquidacao;
					}
				}
			}
			if(this._OrdemCotistaCollectionByIdFormaLiquidacao != null)
			{
				foreach(OrdemCotista obj in this._OrdemCotistaCollectionByIdFormaLiquidacao)
				{
					if(obj.es.IsAdded)
					{
						obj.IdFormaLiquidacao = this.IdFormaLiquidacao;
					}
				}
			}
			if(this._OrdemFundoCollectionByIdFormaLiquidacao != null)
			{
				foreach(OrdemFundo obj in this._OrdemFundoCollectionByIdFormaLiquidacao)
				{
					if(obj.es.IsAdded)
					{
						obj.IdFormaLiquidacao = this.IdFormaLiquidacao;
					}
				}
			}
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostOneToOneSave.
		/// </summary>
		protected override void ApplyPostOneSaveKeys()
		{
		}
		
	}



	[Serializable]
	abstract public class esFormaLiquidacaoQuery : esDynamicQuery
	{
		override protected IMetadata Meta
		{
			get
			{
				return FormaLiquidacaoMetadata.Meta();
			}
		}	
		

		public esQueryItem IdFormaLiquidacao
		{
			get
			{
				return new esQueryItem(this, FormaLiquidacaoMetadata.ColumnNames.IdFormaLiquidacao, esSystemType.Byte);
			}
		} 
		
		public esQueryItem Descricao
		{
			get
			{
				return new esQueryItem(this, FormaLiquidacaoMetadata.ColumnNames.Descricao, esSystemType.String);
			}
		} 
		
		public esQueryItem TipoLiquidacao
		{
			get
			{
				return new esQueryItem(this, FormaLiquidacaoMetadata.ColumnNames.TipoLiquidacao, esSystemType.Byte);
			}
		} 
		
	}



	[Serializable]
	[XmlType("FormaLiquidacaoCollection")]
	public partial class FormaLiquidacaoCollection : esFormaLiquidacaoCollection, IEnumerable<FormaLiquidacao>
	{
		public FormaLiquidacaoCollection()
		{

		}
		
		public static implicit operator List<FormaLiquidacao>(FormaLiquidacaoCollection coll)
		{
			List<FormaLiquidacao> list = new List<FormaLiquidacao>();
			
			foreach (FormaLiquidacao emp in coll)
			{
				list.Add(emp);
			}
			
			return list;
		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return  FormaLiquidacaoMetadata.Meta();
			}
		}
		
		
		
		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new FormaLiquidacaoQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		
		override protected esEntity CreateEntityForCollection(DataRow row)
		{
			return new FormaLiquidacao(row);
		}

		override protected esEntity CreateEntity()
		{
			return new FormaLiquidacao();
		}
		
		
		#endregion


		[BrowsableAttribute( false )]
		public FormaLiquidacaoQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new FormaLiquidacaoQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}

		public bool Load(FormaLiquidacaoQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		public FormaLiquidacao AddNew()
		{
			FormaLiquidacao entity = base.AddNewEntity() as FormaLiquidacao;
			
			return entity;
		}

		public FormaLiquidacao FindByPrimaryKey(System.Byte idFormaLiquidacao)
		{
			return base.FindByPrimaryKey(idFormaLiquidacao) as FormaLiquidacao;
		}


		#region IEnumerable<FormaLiquidacao> Members

		IEnumerator<FormaLiquidacao> IEnumerable<FormaLiquidacao>.GetEnumerator()
		{
			System.Collections.IEnumerable enumer = this as System.Collections.IEnumerable;
			System.Collections.IEnumerator iterator = enumer.GetEnumerator();

			while(iterator.MoveNext())
			{
				yield return iterator.Current as FormaLiquidacao;
			}
		}

		#endregion
		
		private FormaLiquidacaoQuery query;
	}


	/// <summary>
	/// Encapsulates the 'FormaLiquidacao' table
	/// </summary>

	[Serializable]
	public partial class FormaLiquidacao : esFormaLiquidacao
	{
		public FormaLiquidacao()
		{

		}
	
		public FormaLiquidacao(DataRow row)
			: base(row)
		{

		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return FormaLiquidacaoMetadata.Meta();
			}
		}
		
		
		
		override protected esFormaLiquidacaoQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new FormaLiquidacaoQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		#endregion
		



		[BrowsableAttribute( false )]
		public FormaLiquidacaoQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new FormaLiquidacaoQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}
		

		public bool Load(FormaLiquidacaoQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		private FormaLiquidacaoQuery query;
	}



	[Serializable]
	public partial class FormaLiquidacaoQuery : esFormaLiquidacaoQuery
	{
		public FormaLiquidacaoQuery()
		{

		}		
		
		public FormaLiquidacaoQuery(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
			
	}



	[Serializable]
	public partial class FormaLiquidacaoMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected FormaLiquidacaoMetadata()
		{
			_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(FormaLiquidacaoMetadata.ColumnNames.IdFormaLiquidacao, 0, typeof(System.Byte), esSystemType.Byte);
			c.PropertyName = FormaLiquidacaoMetadata.PropertyNames.IdFormaLiquidacao;
			c.IsInPrimaryKey = true;
			c.IsAutoIncrement = true;	
			c.NumericPrecision = 3;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(FormaLiquidacaoMetadata.ColumnNames.Descricao, 1, typeof(System.String), esSystemType.String);
			c.PropertyName = FormaLiquidacaoMetadata.PropertyNames.Descricao;
			c.CharacterMaxLength = 50;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(FormaLiquidacaoMetadata.ColumnNames.TipoLiquidacao, 2, typeof(System.Byte), esSystemType.Byte);
			c.PropertyName = FormaLiquidacaoMetadata.PropertyNames.TipoLiquidacao;	
			c.NumericPrecision = 3;
			_columns.Add(c); 
			
				
		}
		#endregion
	
		static public FormaLiquidacaoMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base._dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base._columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string IdFormaLiquidacao = "IdFormaLiquidacao";
			 public const string Descricao = "Descricao";
			 public const string TipoLiquidacao = "TipoLiquidacao";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string IdFormaLiquidacao = "IdFormaLiquidacao";
			 public const string Descricao = "Descricao";
			 public const string TipoLiquidacao = "TipoLiquidacao";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(FormaLiquidacaoMetadata))
			{
				if(FormaLiquidacaoMetadata.mapDelegates == null)
				{
					FormaLiquidacaoMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (FormaLiquidacaoMetadata.meta == null)
				{
					FormaLiquidacaoMetadata.meta = new FormaLiquidacaoMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();
				

				meta.AddTypeMap("IdFormaLiquidacao", new esTypeMap("tinyint", "System.Byte"));
				meta.AddTypeMap("Descricao", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("TipoLiquidacao", new esTypeMap("tinyint", "System.Byte"));			
				
				
				
				meta.Source = "FormaLiquidacao";
				meta.Destination = "FormaLiquidacao";
				
				meta.spInsert = "proc_FormaLiquidacaoInsert";				
				meta.spUpdate = "proc_FormaLiquidacaoUpdate";		
				meta.spDelete = "proc_FormaLiquidacaoDelete";
				meta.spLoadAll = "proc_FormaLiquidacaoLoadAll";
				meta.spLoadByPrimaryKey = "proc_FormaLiquidacaoLoadByPrimaryKey";
				
				this._providerMetadataMaps["esDefault"] = meta;
			}
			
			return this._providerMetadataMaps["esDefault"];
		}

		#endregion

		static private FormaLiquidacaoMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
