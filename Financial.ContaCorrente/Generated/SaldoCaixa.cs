/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 1/6/2015 6:54:30 PM
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;


using EntitySpaces.Interfaces;
using EntitySpaces.Core;





































		



			
					


		

		
		
		

		

























		






	







using Financial.Investidor;











				
		
		




		








		
				
				












	

















					
								
											
	










		





		




				
				








				




		

		
		
		
		
		





namespace Financial.ContaCorrente
{

	[Serializable]
	abstract public class esSaldoCaixaCollection : esEntityCollection
	{
		public esSaldoCaixaCollection()
		{

		}

		protected override string GetCollectionName()
		{
			return "SaldoCaixaCollection";
		}

		#region Query Logic
		protected void InitQuery(esSaldoCaixaQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntityCollection)this).Connection;
		}

		protected bool OnQueryLoaded(DataTable table)
		{
			this.PopulateCollection(table);
			return (this.RowCount > 0) ? true : false;
		}
		
		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery(query as esSaldoCaixaQuery);
		}
		#endregion
		
		virtual public SaldoCaixa DetachEntity(SaldoCaixa entity)
		{
			return base.DetachEntity(entity) as SaldoCaixa;
		}
		
		virtual public SaldoCaixa AttachEntity(SaldoCaixa entity)
		{
			return base.AttachEntity(entity) as SaldoCaixa;
		}
		
		virtual public void Combine(SaldoCaixaCollection collection)
		{
			base.Combine(collection);
		}
		
		new public SaldoCaixa this[int index]
		{
			get
			{
				return base[index] as SaldoCaixa;
			}
		}

		public override Type GetEntityType()
		{
			return typeof(SaldoCaixa);
		}
	}



	[Serializable]
	abstract public class esSaldoCaixa : esEntity
	{
		/// <summary>
		/// Used internally by the entity's DynamicQuery mechanism.
		/// </summary>
		virtual protected esSaldoCaixaQuery GetDynamicQuery()
		{
			return null;
		}

		public esSaldoCaixa()
		{

		}

		public esSaldoCaixa(DataRow row)
			: base(row)
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.Int32 idCliente, System.DateTime data, System.Int32 idConta)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idCliente, data, idConta);
			else
				return LoadByPrimaryKeyStoredProcedure(idCliente, data, idConta);
		}

		/// <summary>
		/// Loads an entity by primary key
		/// </summary>
		/// <remarks>
		/// EntitySpaces requires primary keys be defined on all tables.
		/// If a table does not have a primary key set,
		/// this method will not compile.
		/// Does not support sqlAcessType. It only works with DynamicQuery
		/// </remarks>
		/// <param name="fieldsToReturn">Fields desired</param>
		public virtual bool LoadByPrimaryKey(List<esQueryItem> fieldsToReturn, System.Int32 idCliente, System.DateTime data, System.Int32 idConta)
		{
			esQueryItem[] fields = fieldsToReturn.ToArray();			
			esSaldoCaixaQuery query = this.GetDynamicQuery();
			query
			    .Select(fields)
			    .Where(query.IdCliente == idCliente, query.Data == data, query.IdConta == idConta);
		
			return query.Load();
		}
		
		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.Int32 idCliente, System.DateTime data, System.Int32 idConta)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idCliente, data, idConta);
			else
				return LoadByPrimaryKeyStoredProcedure(idCliente, data, idConta);
		}

		private bool LoadByPrimaryKeyDynamic(System.Int32 idCliente, System.DateTime data, System.Int32 idConta)
		{
			esSaldoCaixaQuery query = this.GetDynamicQuery();
			query.Where(query.IdCliente == idCliente, query.Data == data, query.IdConta == idConta);
			return query.Load();
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.Int32 idCliente, System.DateTime data, System.Int32 idConta)
		{
			esParameters parms = new esParameters();
			parms.Add("IdCliente",idCliente);			parms.Add("Data",data);			parms.Add("IdConta",idConta);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		
		
		#region Properties
		
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}

		public override void SetProperty(string name, object value)
		{
			if(this.Row == null) this.AddNew();
			
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value.GetType().ToString() == "System.String")
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "IdCliente": this.str.IdCliente = (string)value; break;							
						case "Data": this.str.Data = (string)value; break;							
						case "IdConta": this.str.IdConta = (string)value; break;							
						case "SaldoAbertura": this.str.SaldoAbertura = (string)value; break;							
						case "SaldoFechamento": this.str.SaldoFechamento = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "IdCliente":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdCliente = (System.Int32?)value;
							break;
						
						case "Data":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.Data = (System.DateTime?)value;
							break;
						
						case "IdConta":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdConta = (System.Int32?)value;
							break;
						
						case "SaldoAbertura":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.SaldoAbertura = (System.Decimal?)value;
							break;
						
						case "SaldoFechamento":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.SaldoFechamento = (System.Decimal?)value;
							break;
					

						default:
							break;
					}
				}
			}
			else if(this.Row.Table.Columns.Contains(name))
			{
				this.Row[name] = value;
			}
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}
		
		
		/// <summary>
		/// Maps to SaldoCaixa.IdCliente
		/// </summary>
		virtual public System.Int32? IdCliente
		{
			get
			{
				return base.GetSystemInt32(SaldoCaixaMetadata.ColumnNames.IdCliente);
			}
			
			set
			{
				if(base.SetSystemInt32(SaldoCaixaMetadata.ColumnNames.IdCliente, value))
				{
					this._UpToClienteByIdCliente = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to SaldoCaixa.Data
		/// </summary>
		virtual public System.DateTime? Data
		{
			get
			{
				return base.GetSystemDateTime(SaldoCaixaMetadata.ColumnNames.Data);
			}
			
			set
			{
				base.SetSystemDateTime(SaldoCaixaMetadata.ColumnNames.Data, value);
			}
		}
		
		/// <summary>
		/// Maps to SaldoCaixa.IdConta
		/// </summary>
		virtual public System.Int32? IdConta
		{
			get
			{
				return base.GetSystemInt32(SaldoCaixaMetadata.ColumnNames.IdConta);
			}
			
			set
			{
				if(base.SetSystemInt32(SaldoCaixaMetadata.ColumnNames.IdConta, value))
				{
					this._UpToContaCorrenteByIdConta = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to SaldoCaixa.SaldoAbertura
		/// </summary>
		virtual public System.Decimal? SaldoAbertura
		{
			get
			{
				return base.GetSystemDecimal(SaldoCaixaMetadata.ColumnNames.SaldoAbertura);
			}
			
			set
			{
				base.SetSystemDecimal(SaldoCaixaMetadata.ColumnNames.SaldoAbertura, value);
			}
		}
		
		/// <summary>
		/// Maps to SaldoCaixa.SaldoFechamento
		/// </summary>
		virtual public System.Decimal? SaldoFechamento
		{
			get
			{
				return base.GetSystemDecimal(SaldoCaixaMetadata.ColumnNames.SaldoFechamento);
			}
			
			set
			{
				base.SetSystemDecimal(SaldoCaixaMetadata.ColumnNames.SaldoFechamento, value);
			}
		}
		
		[CLSCompliant(false)]
		internal protected Cliente _UpToClienteByIdCliente;
		[CLSCompliant(false)]
		internal protected Financial.Investidor.ContaCorrente _UpToContaCorrenteByIdConta;
		#endregion	

		#region String Properties


		[BrowsableAttribute( false )]
		public esStrings str
		{
			get
			{
				if (esstrings == null)
				{
					esstrings = new esStrings(this);
				}
				return esstrings;
			}
		}


		[Serializable]
		sealed public class esStrings
		{
			public esStrings(esSaldoCaixa entity)
			{
				this.entity = entity;
			}
			
	
			public System.String IdCliente
			{
				get
				{
					System.Int32? data = entity.IdCliente;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdCliente = null;
					else entity.IdCliente = Convert.ToInt32(value);
				}
			}
				
			public System.String Data
			{
				get
				{
					System.DateTime? data = entity.Data;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Data = null;
					else entity.Data = Convert.ToDateTime(value);
				}
			}
				
			public System.String IdConta
			{
				get
				{
					System.Int32? data = entity.IdConta;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdConta = null;
					else entity.IdConta = Convert.ToInt32(value);
				}
			}
				
			public System.String SaldoAbertura
			{
				get
				{
					System.Decimal? data = entity.SaldoAbertura;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.SaldoAbertura = null;
					else entity.SaldoAbertura = Convert.ToDecimal(value);
				}
			}
				
			public System.String SaldoFechamento
			{
				get
				{
					System.Decimal? data = entity.SaldoFechamento;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.SaldoFechamento = null;
					else entity.SaldoFechamento = Convert.ToDecimal(value);
				}
			}
			

			private esSaldoCaixa entity;
		}
		#endregion

		#region Query Logic
		protected void InitQuery(esSaldoCaixaQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntity)this).Connection;
		}

		[System.Diagnostics.DebuggerNonUserCode]
		protected bool OnQueryLoaded(DataTable table)
		{
			bool dataFound = this.PopulateEntity(table);

			if (this.RowCount > 1)
			{
				throw new Exception("esSaldoCaixa can only hold one record of data");
			}

			return dataFound;
		}
		#endregion
		
		[NonSerialized]
		private esStrings esstrings;
	}


	
	public partial class SaldoCaixa : esSaldoCaixa
	{

				
		#region UpToClienteByIdCliente - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - Cliente_SaldoCaixa_FK1
		/// </summary>

		[XmlIgnore]
		public Cliente UpToClienteByIdCliente
		{
			get
			{
				if(this._UpToClienteByIdCliente == null
					&& IdCliente != null					)
				{
					this._UpToClienteByIdCliente = new Cliente();
					this._UpToClienteByIdCliente.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToClienteByIdCliente", this._UpToClienteByIdCliente);
					this._UpToClienteByIdCliente.Query.Where(this._UpToClienteByIdCliente.Query.IdCliente == this.IdCliente);
					this._UpToClienteByIdCliente.Query.Load();
				}

				return this._UpToClienteByIdCliente;
			}
			
			set
			{
				this.RemovePreSave("UpToClienteByIdCliente");
				

				if(value == null)
				{
					this.IdCliente = null;
					this._UpToClienteByIdCliente = null;
				}
				else
				{
					this.IdCliente = value.IdCliente;
					this._UpToClienteByIdCliente = value;
					this.SetPreSave("UpToClienteByIdCliente", this._UpToClienteByIdCliente);
				}
				
			}
		}
		#endregion
		

				
		#region UpToContaCorrenteByIdConta - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - ContaCorrente_SaldoCaixa_FK1
		/// </summary>

		[XmlIgnore]
		public Financial.Investidor.ContaCorrente UpToContaCorrenteByIdConta
		{
			get
			{
				if(this._UpToContaCorrenteByIdConta == null
					&& IdConta != null					)
				{
					this._UpToContaCorrenteByIdConta = new Financial.Investidor.ContaCorrente();
					this._UpToContaCorrenteByIdConta.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToContaCorrenteByIdConta", this._UpToContaCorrenteByIdConta);
					this._UpToContaCorrenteByIdConta.Query.Where(this._UpToContaCorrenteByIdConta.Query.IdConta == this.IdConta);
					this._UpToContaCorrenteByIdConta.Query.Load();
				}

				return this._UpToContaCorrenteByIdConta;
			}
			
			set
			{
				this.RemovePreSave("UpToContaCorrenteByIdConta");
				

				if(value == null)
				{
					this.IdConta = null;
					this._UpToContaCorrenteByIdConta = null;
				}
				else
				{
					this.IdConta = value.IdConta;
					this._UpToContaCorrenteByIdConta = value;
					this.SetPreSave("UpToContaCorrenteByIdConta", this._UpToContaCorrenteByIdConta);
				}
				
			}
		}
		#endregion
		

		
		/// <summary>
		/// Used internally by the entity's hierarchical properties.
		/// </summary>
		protected override List<esPropertyDescriptor> GetHierarchicalProperties()
		{
			List<esPropertyDescriptor> props = new List<esPropertyDescriptor>();
			
		
			return props;
		}	
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PreSave.
		/// </summary>
		protected override void ApplyPreSaveKeys()
		{
			if(!this.es.IsDeleted && this._UpToContaCorrenteByIdConta != null)
			{
				this.IdConta = this._UpToContaCorrenteByIdConta.IdConta;
			}
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostSave.
		/// </summary>
		protected override void ApplyPostSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostOneToOneSave.
		/// </summary>
		protected override void ApplyPostOneSaveKeys()
		{
		}
		
	}



	[Serializable]
	abstract public class esSaldoCaixaQuery : esDynamicQuery
	{
		override protected IMetadata Meta
		{
			get
			{
				return SaldoCaixaMetadata.Meta();
			}
		}	
		

		public esQueryItem IdCliente
		{
			get
			{
				return new esQueryItem(this, SaldoCaixaMetadata.ColumnNames.IdCliente, esSystemType.Int32);
			}
		} 
		
		public esQueryItem Data
		{
			get
			{
				return new esQueryItem(this, SaldoCaixaMetadata.ColumnNames.Data, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem IdConta
		{
			get
			{
				return new esQueryItem(this, SaldoCaixaMetadata.ColumnNames.IdConta, esSystemType.Int32);
			}
		} 
		
		public esQueryItem SaldoAbertura
		{
			get
			{
				return new esQueryItem(this, SaldoCaixaMetadata.ColumnNames.SaldoAbertura, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem SaldoFechamento
		{
			get
			{
				return new esQueryItem(this, SaldoCaixaMetadata.ColumnNames.SaldoFechamento, esSystemType.Decimal);
			}
		} 
		
	}



	[Serializable]
	[XmlType("SaldoCaixaCollection")]
	public partial class SaldoCaixaCollection : esSaldoCaixaCollection, IEnumerable<SaldoCaixa>
	{
		public SaldoCaixaCollection()
		{

		}
		
		public static implicit operator List<SaldoCaixa>(SaldoCaixaCollection coll)
		{
			List<SaldoCaixa> list = new List<SaldoCaixa>();
			
			foreach (SaldoCaixa emp in coll)
			{
				list.Add(emp);
			}
			
			return list;
		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return  SaldoCaixaMetadata.Meta();
			}
		}
		
		
		
		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new SaldoCaixaQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		
		override protected esEntity CreateEntityForCollection(DataRow row)
		{
			return new SaldoCaixa(row);
		}

		override protected esEntity CreateEntity()
		{
			return new SaldoCaixa();
		}
		
		
		#endregion


		[BrowsableAttribute( false )]
		public SaldoCaixaQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new SaldoCaixaQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}

		public bool Load(SaldoCaixaQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		public SaldoCaixa AddNew()
		{
			SaldoCaixa entity = base.AddNewEntity() as SaldoCaixa;
			
			return entity;
		}

		public SaldoCaixa FindByPrimaryKey(System.Int32 idCliente, System.DateTime data, System.Int32 idConta)
		{
			return base.FindByPrimaryKey(idCliente, data, idConta) as SaldoCaixa;
		}


		#region IEnumerable<SaldoCaixa> Members

		IEnumerator<SaldoCaixa> IEnumerable<SaldoCaixa>.GetEnumerator()
		{
			System.Collections.IEnumerable enumer = this as System.Collections.IEnumerable;
			System.Collections.IEnumerator iterator = enumer.GetEnumerator();

			while(iterator.MoveNext())
			{
				yield return iterator.Current as SaldoCaixa;
			}
		}

		#endregion
		
		private SaldoCaixaQuery query;
	}


	/// <summary>
	/// Encapsulates the 'SaldoCaixa' table
	/// </summary>

	[Serializable]
	public partial class SaldoCaixa : esSaldoCaixa
	{
		public SaldoCaixa()
		{

		}
	
		public SaldoCaixa(DataRow row)
			: base(row)
		{

		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return SaldoCaixaMetadata.Meta();
			}
		}
		
		
		
		override protected esSaldoCaixaQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new SaldoCaixaQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		#endregion
		



		[BrowsableAttribute( false )]
		public SaldoCaixaQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new SaldoCaixaQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}
		

		public bool Load(SaldoCaixaQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		private SaldoCaixaQuery query;
	}



	[Serializable]
	public partial class SaldoCaixaQuery : esSaldoCaixaQuery
	{
		public SaldoCaixaQuery()
		{

		}		
		
		public SaldoCaixaQuery(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
			
	}



	[Serializable]
	public partial class SaldoCaixaMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected SaldoCaixaMetadata()
		{
			_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(SaldoCaixaMetadata.ColumnNames.IdCliente, 0, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = SaldoCaixaMetadata.PropertyNames.IdCliente;
			c.IsInPrimaryKey = true;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(SaldoCaixaMetadata.ColumnNames.Data, 1, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = SaldoCaixaMetadata.PropertyNames.Data;
			c.IsInPrimaryKey = true;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(SaldoCaixaMetadata.ColumnNames.IdConta, 2, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = SaldoCaixaMetadata.PropertyNames.IdConta;
			c.IsInPrimaryKey = true;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(SaldoCaixaMetadata.ColumnNames.SaldoAbertura, 3, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = SaldoCaixaMetadata.PropertyNames.SaldoAbertura;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(SaldoCaixaMetadata.ColumnNames.SaldoFechamento, 4, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = SaldoCaixaMetadata.PropertyNames.SaldoFechamento;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
		}
		#endregion
	
		static public SaldoCaixaMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base._dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base._columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string IdCliente = "IdCliente";
			 public const string Data = "Data";
			 public const string IdConta = "IdConta";
			 public const string SaldoAbertura = "SaldoAbertura";
			 public const string SaldoFechamento = "SaldoFechamento";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string IdCliente = "IdCliente";
			 public const string Data = "Data";
			 public const string IdConta = "IdConta";
			 public const string SaldoAbertura = "SaldoAbertura";
			 public const string SaldoFechamento = "SaldoFechamento";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(SaldoCaixaMetadata))
			{
				if(SaldoCaixaMetadata.mapDelegates == null)
				{
					SaldoCaixaMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (SaldoCaixaMetadata.meta == null)
				{
					SaldoCaixaMetadata.meta = new SaldoCaixaMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();
				

				meta.AddTypeMap("IdCliente", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("Data", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("IdConta", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("SaldoAbertura", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("SaldoFechamento", new esTypeMap("decimal", "System.Decimal"));			
				
				
				
				meta.Source = "SaldoCaixa";
				meta.Destination = "SaldoCaixa";
				
				meta.spInsert = "proc_SaldoCaixaInsert";				
				meta.spUpdate = "proc_SaldoCaixaUpdate";		
				meta.spDelete = "proc_SaldoCaixaDelete";
				meta.spLoadAll = "proc_SaldoCaixaLoadAll";
				meta.spLoadByPrimaryKey = "proc_SaldoCaixaLoadByPrimaryKey";
				
				this._providerMetadataMaps["esDefault"] = meta;
			}
			
			return this._providerMetadataMaps["esDefault"];
		}

		#endregion

		static private SaldoCaixaMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
