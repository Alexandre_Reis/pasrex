/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 28/03/2016 14:36:16
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;


using EntitySpaces.Interfaces;
using EntitySpaces.Core;





































		



			
					


		

		
		
		

		

























		






	





using Financial.Investidor;













				
		
		




		








		
				
				












	

















					
								
											
	










		





		




				
				








				




		

		
		
		
		
		





namespace Financial.ContaCorrente
{

	[Serializable]
	abstract public class esLiquidacaoAberturaCollection : esEntityCollection
	{
		public esLiquidacaoAberturaCollection()
		{

		}

		protected override string GetCollectionName()
		{
			return "LiquidacaoAberturaCollection";
		}

		#region Query Logic
		protected void InitQuery(esLiquidacaoAberturaQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntityCollection)this).Connection;
		}

		protected bool OnQueryLoaded(DataTable table)
		{
			this.PopulateCollection(table);
			return (this.RowCount > 0) ? true : false;
		}
		
		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery(query as esLiquidacaoAberturaQuery);
		}
		#endregion
		
		virtual public LiquidacaoAbertura DetachEntity(LiquidacaoAbertura entity)
		{
			return base.DetachEntity(entity) as LiquidacaoAbertura;
		}
		
		virtual public LiquidacaoAbertura AttachEntity(LiquidacaoAbertura entity)
		{
			return base.AttachEntity(entity) as LiquidacaoAbertura;
		}
		
		virtual public void Combine(LiquidacaoAberturaCollection collection)
		{
			base.Combine(collection);
		}
		
		new public LiquidacaoAbertura this[int index]
		{
			get
			{
				return base[index] as LiquidacaoAbertura;
			}
		}

		public override Type GetEntityType()
		{
			return typeof(LiquidacaoAbertura);
		}
	}



	[Serializable]
	abstract public class esLiquidacaoAbertura : esEntity
	{
		/// <summary>
		/// Used internally by the entity's DynamicQuery mechanism.
		/// </summary>
		virtual protected esLiquidacaoAberturaQuery GetDynamicQuery()
		{
			return null;
		}

		public esLiquidacaoAbertura()
		{

		}

		public esLiquidacaoAbertura(DataRow row)
			: base(row)
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.Int32 idLiquidacao, System.DateTime dataHistorico)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idLiquidacao, dataHistorico);
			else
				return LoadByPrimaryKeyStoredProcedure(idLiquidacao, dataHistorico);
		}

		/// <summary>
		/// Loads an entity by primary key
		/// </summary>
		/// <remarks>
		/// EntitySpaces requires primary keys be defined on all tables.
		/// If a table does not have a primary key set,
		/// this method will not compile.
		/// Does not support sqlAcessType. It only works with DynamicQuery
		/// </remarks>
		/// <param name="fieldsToReturn">Fields desired</param>
		public virtual bool LoadByPrimaryKey(List<esQueryItem> fieldsToReturn, System.Int32 idLiquidacao, System.DateTime dataHistorico)
		{
			esQueryItem[] fields = fieldsToReturn.ToArray();			
			esLiquidacaoAberturaQuery query = this.GetDynamicQuery();
			query
			    .Select(fields)
			    .Where(query.IdLiquidacao == idLiquidacao, query.DataHistorico == dataHistorico);
		
			return query.Load();
		}
		
		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.Int32 idLiquidacao, System.DateTime dataHistorico)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idLiquidacao, dataHistorico);
			else
				return LoadByPrimaryKeyStoredProcedure(idLiquidacao, dataHistorico);
		}

		private bool LoadByPrimaryKeyDynamic(System.Int32 idLiquidacao, System.DateTime dataHistorico)
		{
			esLiquidacaoAberturaQuery query = this.GetDynamicQuery();
			query.Where(query.IdLiquidacao == idLiquidacao, query.DataHistorico == dataHistorico);
			return query.Load();
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.Int32 idLiquidacao, System.DateTime dataHistorico)
		{
			esParameters parms = new esParameters();
			parms.Add("IdLiquidacao",idLiquidacao);			parms.Add("DataHistorico",dataHistorico);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		
		
		#region Properties
		
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}

		public override void SetProperty(string name, object value)
		{
			if(this.Row == null) this.AddNew();
			
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value.GetType().ToString() == "System.String")
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "IdLiquidacao": this.str.IdLiquidacao = (string)value; break;							
						case "DataHistorico": this.str.DataHistorico = (string)value; break;							
						case "IdCliente": this.str.IdCliente = (string)value; break;							
						case "DataLancamento": this.str.DataLancamento = (string)value; break;							
						case "DataVencimento": this.str.DataVencimento = (string)value; break;							
						case "Descricao": this.str.Descricao = (string)value; break;							
						case "Valor": this.str.Valor = (string)value; break;							
						case "Situacao": this.str.Situacao = (string)value; break;							
						case "Origem": this.str.Origem = (string)value; break;							
						case "Fonte": this.str.Fonte = (string)value; break;							
						case "IdAgente": this.str.IdAgente = (string)value; break;							
						case "IdentificadorOrigem": this.str.IdentificadorOrigem = (string)value; break;							
						case "IdConta": this.str.IdConta = (string)value; break;							
						case "IdEvento": this.str.IdEvento = (string)value; break;							
						case "IdEventoVencimento": this.str.IdEventoVencimento = (string)value; break;
                        case "CodigoAtivo": this.str.CodigoAtivo = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "IdLiquidacao":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdLiquidacao = (System.Int32?)value;
							break;
						
						case "DataHistorico":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.DataHistorico = (System.DateTime?)value;
							break;
						
						case "IdCliente":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdCliente = (System.Int32?)value;
							break;
						
						case "DataLancamento":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.DataLancamento = (System.DateTime?)value;
							break;
						
						case "DataVencimento":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.DataVencimento = (System.DateTime?)value;
							break;
						
						case "Valor":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.Valor = (System.Decimal?)value;
							break;
						
						case "Situacao":
						
							if (value == null || value.GetType().ToString() == "System.Byte")
								this.Situacao = (System.Byte?)value;
							break;
						
						case "Origem":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.Origem = (System.Int32?)value;
							break;
						
						case "Fonte":
						
							if (value == null || value.GetType().ToString() == "System.Byte")
								this.Fonte = (System.Byte?)value;
							break;
						
						case "IdAgente":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdAgente = (System.Int32?)value;
							break;
						
						case "IdentificadorOrigem":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdentificadorOrigem = (System.Int32?)value;
							break;
						
						case "IdConta":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdConta = (System.Int32?)value;
							break;
						
						case "IdEvento":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdEvento = (System.Int32?)value;
							break;
							
						case "IdEventoVencimento":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdEventoVencimento = (System.Int32?)value;
							break;
					

						default:
							break;
					}
				}
			}
			else if(this.Row.Table.Columns.Contains(name))
			{
				this.Row[name] = value;
			}
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}
		
		
		/// <summary>
		/// Maps to LiquidacaoAbertura.IdLiquidacao
		/// </summary>
		virtual public System.Int32? IdLiquidacao
		{
			get
			{
				return base.GetSystemInt32(LiquidacaoAberturaMetadata.ColumnNames.IdLiquidacao);
			}
			
			set
			{
				base.SetSystemInt32(LiquidacaoAberturaMetadata.ColumnNames.IdLiquidacao, value);
			}
		}
		
		/// <summary>
		/// Maps to LiquidacaoAbertura.DataHistorico
		/// </summary>
		virtual public System.DateTime? DataHistorico
		{
			get
			{
				return base.GetSystemDateTime(LiquidacaoAberturaMetadata.ColumnNames.DataHistorico);
			}
			
			set
			{
				base.SetSystemDateTime(LiquidacaoAberturaMetadata.ColumnNames.DataHistorico, value);
			}
		}
		
		/// <summary>
		/// Maps to LiquidacaoAbertura.IdCliente
		/// </summary>
		virtual public System.Int32? IdCliente
		{
			get
			{
				return base.GetSystemInt32(LiquidacaoAberturaMetadata.ColumnNames.IdCliente);
			}
			
			set
			{
				if(base.SetSystemInt32(LiquidacaoAberturaMetadata.ColumnNames.IdCliente, value))
				{
					this._UpToClienteByIdCliente = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to LiquidacaoAbertura.DataLancamento
		/// </summary>
		virtual public System.DateTime? DataLancamento
		{
			get
			{
				return base.GetSystemDateTime(LiquidacaoAberturaMetadata.ColumnNames.DataLancamento);
			}
			
			set
			{
				base.SetSystemDateTime(LiquidacaoAberturaMetadata.ColumnNames.DataLancamento, value);
			}
		}
		
		/// <summary>
		/// Maps to LiquidacaoAbertura.DataVencimento
		/// </summary>
		virtual public System.DateTime? DataVencimento
		{
			get
			{
				return base.GetSystemDateTime(LiquidacaoAberturaMetadata.ColumnNames.DataVencimento);
			}
			
			set
			{
				base.SetSystemDateTime(LiquidacaoAberturaMetadata.ColumnNames.DataVencimento, value);
			}
		}
		
		/// <summary>
		/// Maps to LiquidacaoAbertura.Descricao
		/// </summary>
		virtual public System.String Descricao
		{
			get
			{
				return base.GetSystemString(LiquidacaoAberturaMetadata.ColumnNames.Descricao);
			}
			
			set
			{
				base.SetSystemString(LiquidacaoAberturaMetadata.ColumnNames.Descricao, value);
			}
		}
		
		/// <summary>
		/// Maps to LiquidacaoAbertura.Valor
		/// </summary>
		virtual public System.Decimal? Valor
		{
			get
			{
				return base.GetSystemDecimal(LiquidacaoAberturaMetadata.ColumnNames.Valor);
			}
			
			set
			{
				base.SetSystemDecimal(LiquidacaoAberturaMetadata.ColumnNames.Valor, value);
			}
		}
		
		/// <summary>
		/// Maps to LiquidacaoAbertura.Situacao
		/// </summary>
		virtual public System.Byte? Situacao
		{
			get
			{
				return base.GetSystemByte(LiquidacaoAberturaMetadata.ColumnNames.Situacao);
			}
			
			set
			{
				base.SetSystemByte(LiquidacaoAberturaMetadata.ColumnNames.Situacao, value);
			}
		}
		
		/// <summary>
		/// Maps to LiquidacaoAbertura.Origem
		/// </summary>
		virtual public System.Int32? Origem
		{
			get
			{
				return base.GetSystemInt32(LiquidacaoAberturaMetadata.ColumnNames.Origem);
			}
			
			set
			{
				base.SetSystemInt32(LiquidacaoAberturaMetadata.ColumnNames.Origem, value);
			}
		}
		
		/// <summary>
		/// Maps to LiquidacaoAbertura.Fonte
		/// </summary>
		virtual public System.Byte? Fonte
		{
			get
			{
				return base.GetSystemByte(LiquidacaoAberturaMetadata.ColumnNames.Fonte);
			}
			
			set
			{
				base.SetSystemByte(LiquidacaoAberturaMetadata.ColumnNames.Fonte, value);
			}
		}
		
		/// <summary>
		/// Maps to LiquidacaoAbertura.IdAgente
		/// </summary>
		virtual public System.Int32? IdAgente
		{
			get
			{
				return base.GetSystemInt32(LiquidacaoAberturaMetadata.ColumnNames.IdAgente);
			}
			
			set
			{
				base.SetSystemInt32(LiquidacaoAberturaMetadata.ColumnNames.IdAgente, value);
			}
		}
		
		/// <summary>
		/// Maps to LiquidacaoAbertura.IdentificadorOrigem
		/// </summary>
		virtual public System.Int32? IdentificadorOrigem
		{
			get
			{
				return base.GetSystemInt32(LiquidacaoAberturaMetadata.ColumnNames.IdentificadorOrigem);
			}
			
			set
			{
				base.SetSystemInt32(LiquidacaoAberturaMetadata.ColumnNames.IdentificadorOrigem, value);
			}
		}
		
		/// <summary>
		/// Maps to LiquidacaoAbertura.IdConta
		/// </summary>
		virtual public System.Int32? IdConta
		{
			get
			{
				return base.GetSystemInt32(LiquidacaoAberturaMetadata.ColumnNames.IdConta);
			}
			
			set
			{
				base.SetSystemInt32(LiquidacaoAberturaMetadata.ColumnNames.IdConta, value);
			}
		}
		
		/// <summary>
		/// Maps to LiquidacaoAbertura.IdEvento
		/// </summary>
		virtual public System.Int32? IdEvento
		{
			get
			{
				return base.GetSystemInt32(LiquidacaoAberturaMetadata.ColumnNames.IdEvento);
			}
			
			set
			{
				base.SetSystemInt32(LiquidacaoAberturaMetadata.ColumnNames.IdEvento, value);
			}
		}
		
		/// <summary>
		/// Maps to LiquidacaoAbertura.IdEventoVencimento
		/// </summary>
		virtual public System.Int32? IdEventoVencimento
		{
			get
			{
				return base.GetSystemInt32(LiquidacaoAberturaMetadata.ColumnNames.IdEventoVencimento);
			}
			
			set
			{
				base.SetSystemInt32(LiquidacaoAberturaMetadata.ColumnNames.IdEventoVencimento, value);
			}
		}

		/// <summary>
		/// Maps to LiquidacaoAbertura.CodigoAtivo
		/// </summary>
		virtual public System.String CodigoAtivo
		{
			get
			{
				return base.GetSystemString(LiquidacaoAberturaMetadata.ColumnNames.CodigoAtivo);
			}
			
			set
			{
				base.SetSystemString(LiquidacaoAberturaMetadata.ColumnNames.CodigoAtivo, value);
			}
		}
		
		[CLSCompliant(false)]
		internal protected Cliente _UpToClienteByIdCliente;
		#endregion	

		#region String Properties


		[BrowsableAttribute( false )]
		public esStrings str
		{
			get
			{
				if (esstrings == null)
				{
					esstrings = new esStrings(this);
				}
				return esstrings;
			}
		}


		[Serializable]
		sealed public class esStrings
		{
			public esStrings(esLiquidacaoAbertura entity)
			{
				this.entity = entity;
			}
			
	
			public System.String IdLiquidacao
			{
				get
				{
					System.Int32? data = entity.IdLiquidacao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdLiquidacao = null;
					else entity.IdLiquidacao = Convert.ToInt32(value);
				}
			}
				
			public System.String DataHistorico
			{
				get
				{
					System.DateTime? data = entity.DataHistorico;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DataHistorico = null;
					else entity.DataHistorico = Convert.ToDateTime(value);
				}
			}
				
			public System.String IdCliente
			{
				get
				{
					System.Int32? data = entity.IdCliente;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdCliente = null;
					else entity.IdCliente = Convert.ToInt32(value);
				}
			}
				
			public System.String DataLancamento
			{
				get
				{
					System.DateTime? data = entity.DataLancamento;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DataLancamento = null;
					else entity.DataLancamento = Convert.ToDateTime(value);
				}
			}
				
			public System.String DataVencimento
			{
				get
				{
					System.DateTime? data = entity.DataVencimento;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DataVencimento = null;
					else entity.DataVencimento = Convert.ToDateTime(value);
				}
			}
				
			public System.String Descricao
			{
				get
				{
					System.String data = entity.Descricao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Descricao = null;
					else entity.Descricao = Convert.ToString(value);
				}
			}
				
			public System.String Valor
			{
				get
				{
					System.Decimal? data = entity.Valor;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Valor = null;
					else entity.Valor = Convert.ToDecimal(value);
				}
			}
				
			public System.String Situacao
			{
				get
				{
					System.Byte? data = entity.Situacao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Situacao = null;
					else entity.Situacao = Convert.ToByte(value);
				}
			}
				
			public System.String Origem
			{
				get
				{
					System.Int32? data = entity.Origem;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Origem = null;
					else entity.Origem = Convert.ToInt32(value);
				}
			}
				
			public System.String Fonte
			{
				get
				{
					System.Byte? data = entity.Fonte;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Fonte = null;
					else entity.Fonte = Convert.ToByte(value);
				}
			}
				
			public System.String IdAgente
			{
				get
				{
					System.Int32? data = entity.IdAgente;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdAgente = null;
					else entity.IdAgente = Convert.ToInt32(value);
				}
			}
				
			public System.String IdentificadorOrigem
			{
				get
				{
					System.Int32? data = entity.IdentificadorOrigem;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdentificadorOrigem = null;
					else entity.IdentificadorOrigem = Convert.ToInt32(value);
				}
			}
				
			public System.String IdConta
			{
				get
				{
					System.Int32? data = entity.IdConta;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdConta = null;
					else entity.IdConta = Convert.ToInt32(value);
				}
			}
				
			public System.String IdEvento
			{
				get
				{
					System.Int32? data = entity.IdEvento;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdEvento = null;
					else entity.IdEvento = Convert.ToInt32(value);
				}
			}
				
			public System.String CodigoAtivo
			{
				get
				{
					System.String data = entity.CodigoAtivo;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.CodigoAtivo = null;
					else entity.CodigoAtivo = Convert.ToString(value);
				}
			}
			
			public System.String IdEventoVencimento
			{
				get
				{
					System.Int32? data = entity.IdEventoVencimento;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdEventoVencimento = null;
					else entity.IdEventoVencimento = Convert.ToInt32(value);
				}
			}
			

			private esLiquidacaoAbertura entity;
		}
		#endregion

		#region Query Logic
		protected void InitQuery(esLiquidacaoAberturaQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntity)this).Connection;
		}

		[System.Diagnostics.DebuggerNonUserCode]
		protected bool OnQueryLoaded(DataTable table)
		{
			bool dataFound = this.PopulateEntity(table);

			if (this.RowCount > 1)
			{
				throw new Exception("esLiquidacaoAbertura can only hold one record of data");
			}

			return dataFound;
		}
		#endregion
		
		[NonSerialized]
		private esStrings esstrings;
	}


	
	public partial class LiquidacaoAbertura : esLiquidacaoAbertura
	{

				
		#region UpToClienteByIdCliente - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - Cliente_LiquidacaoAbertura_FK1
		/// </summary>

		[XmlIgnore]
		public Cliente UpToClienteByIdCliente
		{
			get
			{
				if(this._UpToClienteByIdCliente == null
					&& IdCliente != null					)
				{
					this._UpToClienteByIdCliente = new Cliente();
					this._UpToClienteByIdCliente.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToClienteByIdCliente", this._UpToClienteByIdCliente);
					this._UpToClienteByIdCliente.Query.Where(this._UpToClienteByIdCliente.Query.IdCliente == this.IdCliente);
					this._UpToClienteByIdCliente.Query.Load();
				}

				return this._UpToClienteByIdCliente;
			}
			
			set
			{
				this.RemovePreSave("UpToClienteByIdCliente");
				

				if(value == null)
				{
					this.IdCliente = null;
					this._UpToClienteByIdCliente = null;
				}
				else
				{
					this.IdCliente = value.IdCliente;
					this._UpToClienteByIdCliente = value;
					this.SetPreSave("UpToClienteByIdCliente", this._UpToClienteByIdCliente);
				}
				
			}
		}
		#endregion
		

		
		/// <summary>
		/// Used internally by the entity's hierarchical properties.
		/// </summary>
		protected override List<esPropertyDescriptor> GetHierarchicalProperties()
		{
			List<esPropertyDescriptor> props = new List<esPropertyDescriptor>();
			
		
			return props;
		}	
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PreSave.
		/// </summary>
		protected override void ApplyPreSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostSave.
		/// </summary>
		protected override void ApplyPostSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostOneToOneSave.
		/// </summary>
		protected override void ApplyPostOneSaveKeys()
		{
		}
		
	}



	[Serializable]
	abstract public class esLiquidacaoAberturaQuery : esDynamicQuery
	{
		override protected IMetadata Meta
		{
			get
			{
				return LiquidacaoAberturaMetadata.Meta();
			}
		}	
		

		public esQueryItem IdLiquidacao
		{
			get
			{
				return new esQueryItem(this, LiquidacaoAberturaMetadata.ColumnNames.IdLiquidacao, esSystemType.Int32);
			}
		} 
		
		public esQueryItem DataHistorico
		{
			get
			{
				return new esQueryItem(this, LiquidacaoAberturaMetadata.ColumnNames.DataHistorico, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem IdCliente
		{
			get
			{
				return new esQueryItem(this, LiquidacaoAberturaMetadata.ColumnNames.IdCliente, esSystemType.Int32);
			}
		} 
		
		public esQueryItem DataLancamento
		{
			get
			{
				return new esQueryItem(this, LiquidacaoAberturaMetadata.ColumnNames.DataLancamento, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem DataVencimento
		{
			get
			{
				return new esQueryItem(this, LiquidacaoAberturaMetadata.ColumnNames.DataVencimento, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem Descricao
		{
			get
			{
				return new esQueryItem(this, LiquidacaoAberturaMetadata.ColumnNames.Descricao, esSystemType.String);
			}
		} 
		
		public esQueryItem Valor
		{
			get
			{
				return new esQueryItem(this, LiquidacaoAberturaMetadata.ColumnNames.Valor, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem Situacao
		{
			get
			{
				return new esQueryItem(this, LiquidacaoAberturaMetadata.ColumnNames.Situacao, esSystemType.Byte);
			}
		} 
		
		public esQueryItem Origem
		{
			get
			{
				return new esQueryItem(this, LiquidacaoAberturaMetadata.ColumnNames.Origem, esSystemType.Int32);
			}
		} 
		
		public esQueryItem Fonte
		{
			get
			{
				return new esQueryItem(this, LiquidacaoAberturaMetadata.ColumnNames.Fonte, esSystemType.Byte);
			}
		} 
		
		public esQueryItem IdAgente
		{
			get
			{
				return new esQueryItem(this, LiquidacaoAberturaMetadata.ColumnNames.IdAgente, esSystemType.Int32);
			}
		} 
		
		public esQueryItem IdentificadorOrigem
		{
			get
			{
				return new esQueryItem(this, LiquidacaoAberturaMetadata.ColumnNames.IdentificadorOrigem, esSystemType.Int32);
			}
		} 
		
		public esQueryItem IdConta
		{
			get
			{
				return new esQueryItem(this, LiquidacaoAberturaMetadata.ColumnNames.IdConta, esSystemType.Int32);
			}
		} 
		
		public esQueryItem IdEvento
		{
			get
			{
				return new esQueryItem(this, LiquidacaoAberturaMetadata.ColumnNames.IdEvento, esSystemType.Int32);
			}
		} 

		public esQueryItem IdEventoVencimento
		{
			get
			{
				return new esQueryItem(this, LiquidacaoAberturaMetadata.ColumnNames.IdEventoVencimento, esSystemType.Int32);
			}
		} 
		
		public esQueryItem CodigoAtivo
		{
			get
			{
				return new esQueryItem(this, LiquidacaoAberturaMetadata.ColumnNames.CodigoAtivo, esSystemType.String);
			}
		} 
		
	}



	[Serializable]
	[XmlType("LiquidacaoAberturaCollection")]
	public partial class LiquidacaoAberturaCollection : esLiquidacaoAberturaCollection, IEnumerable<LiquidacaoAbertura>
	{
		public LiquidacaoAberturaCollection()
		{

		}
		
		public static implicit operator List<LiquidacaoAbertura>(LiquidacaoAberturaCollection coll)
		{
			List<LiquidacaoAbertura> list = new List<LiquidacaoAbertura>();
			
			foreach (LiquidacaoAbertura emp in coll)
			{
				list.Add(emp);
			}
			
			return list;
		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return  LiquidacaoAberturaMetadata.Meta();
			}
		}
		
		
		
		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new LiquidacaoAberturaQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		
		override protected esEntity CreateEntityForCollection(DataRow row)
		{
			return new LiquidacaoAbertura(row);
		}

		override protected esEntity CreateEntity()
		{
			return new LiquidacaoAbertura();
		}
		
		
		#endregion


		[BrowsableAttribute( false )]
		public LiquidacaoAberturaQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new LiquidacaoAberturaQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}

		public bool Load(LiquidacaoAberturaQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		public LiquidacaoAbertura AddNew()
		{
			LiquidacaoAbertura entity = base.AddNewEntity() as LiquidacaoAbertura;
			
			return entity;
		}

		public LiquidacaoAbertura FindByPrimaryKey(System.Int32 idLiquidacao, System.DateTime dataHistorico)
		{
			return base.FindByPrimaryKey(idLiquidacao, dataHistorico) as LiquidacaoAbertura;
		}


		#region IEnumerable<LiquidacaoAbertura> Members

		IEnumerator<LiquidacaoAbertura> IEnumerable<LiquidacaoAbertura>.GetEnumerator()
		{
			System.Collections.IEnumerable enumer = this as System.Collections.IEnumerable;
			System.Collections.IEnumerator iterator = enumer.GetEnumerator();

			while(iterator.MoveNext())
			{
				yield return iterator.Current as LiquidacaoAbertura;
			}
		}

		#endregion
		
		private LiquidacaoAberturaQuery query;
	}


	/// <summary>
	/// Encapsulates the 'LiquidacaoAbertura' table
	/// </summary>

	[Serializable]
	public partial class LiquidacaoAbertura : esLiquidacaoAbertura
	{
		public LiquidacaoAbertura()
		{

		}
	
		public LiquidacaoAbertura(DataRow row)
			: base(row)
		{

		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return LiquidacaoAberturaMetadata.Meta();
			}
		}
		
		
		
		override protected esLiquidacaoAberturaQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new LiquidacaoAberturaQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		#endregion
		



		[BrowsableAttribute( false )]
		public LiquidacaoAberturaQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new LiquidacaoAberturaQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}
		

		public bool Load(LiquidacaoAberturaQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		private LiquidacaoAberturaQuery query;
	}



	[Serializable]
	public partial class LiquidacaoAberturaQuery : esLiquidacaoAberturaQuery
	{
		public LiquidacaoAberturaQuery()
		{

		}		
		
		public LiquidacaoAberturaQuery(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
			
	}



	[Serializable]
	public partial class LiquidacaoAberturaMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected LiquidacaoAberturaMetadata()
		{
			_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(LiquidacaoAberturaMetadata.ColumnNames.IdLiquidacao, 0, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = LiquidacaoAberturaMetadata.PropertyNames.IdLiquidacao;
			c.IsInPrimaryKey = true;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(LiquidacaoAberturaMetadata.ColumnNames.DataHistorico, 1, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = LiquidacaoAberturaMetadata.PropertyNames.DataHistorico;
			c.IsInPrimaryKey = true;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(LiquidacaoAberturaMetadata.ColumnNames.IdCliente, 2, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = LiquidacaoAberturaMetadata.PropertyNames.IdCliente;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(LiquidacaoAberturaMetadata.ColumnNames.DataLancamento, 3, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = LiquidacaoAberturaMetadata.PropertyNames.DataLancamento;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(LiquidacaoAberturaMetadata.ColumnNames.DataVencimento, 4, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = LiquidacaoAberturaMetadata.PropertyNames.DataVencimento;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(LiquidacaoAberturaMetadata.ColumnNames.Descricao, 5, typeof(System.String), esSystemType.String);
			c.PropertyName = LiquidacaoAberturaMetadata.PropertyNames.Descricao;
			c.CharacterMaxLength = 600;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(LiquidacaoAberturaMetadata.ColumnNames.Valor, 6, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = LiquidacaoAberturaMetadata.PropertyNames.Valor;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(LiquidacaoAberturaMetadata.ColumnNames.Situacao, 7, typeof(System.Byte), esSystemType.Byte);
			c.PropertyName = LiquidacaoAberturaMetadata.PropertyNames.Situacao;	
			c.NumericPrecision = 3;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(LiquidacaoAberturaMetadata.ColumnNames.Origem, 8, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = LiquidacaoAberturaMetadata.PropertyNames.Origem;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(LiquidacaoAberturaMetadata.ColumnNames.Fonte, 9, typeof(System.Byte), esSystemType.Byte);
			c.PropertyName = LiquidacaoAberturaMetadata.PropertyNames.Fonte;	
			c.NumericPrecision = 3;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(LiquidacaoAberturaMetadata.ColumnNames.IdAgente, 10, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = LiquidacaoAberturaMetadata.PropertyNames.IdAgente;	
			c.NumericPrecision = 10;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(LiquidacaoAberturaMetadata.ColumnNames.IdentificadorOrigem, 11, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = LiquidacaoAberturaMetadata.PropertyNames.IdentificadorOrigem;	
			c.NumericPrecision = 10;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(LiquidacaoAberturaMetadata.ColumnNames.IdConta, 12, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = LiquidacaoAberturaMetadata.PropertyNames.IdConta;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(LiquidacaoAberturaMetadata.ColumnNames.IdEvento, 13, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = LiquidacaoAberturaMetadata.PropertyNames.IdEvento;	
			c.NumericPrecision = 10;
			c.IsNullable = true;
			_columns.Add(c); 
			

			c = new esColumnMetadata(LiquidacaoAberturaMetadata.ColumnNames.IdEventoVencimento, 14, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = LiquidacaoAberturaMetadata.PropertyNames.IdEventoVencimento;	
			c.NumericPrecision = 10;	
			c.IsNullable = true;
			_columns.Add(c); 


			c = new esColumnMetadata(LiquidacaoAberturaMetadata.ColumnNames.CodigoAtivo, 14, typeof(System.String), esSystemType.String);
			c.PropertyName = LiquidacaoAberturaMetadata.PropertyNames.CodigoAtivo;
			c.CharacterMaxLength = 40;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
		}
		#endregion
	
		static public LiquidacaoAberturaMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base._dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base._columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string IdLiquidacao = "IdLiquidacao";
			 public const string DataHistorico = "DataHistorico";
			 public const string IdCliente = "IdCliente";
			 public const string DataLancamento = "DataLancamento";
			 public const string DataVencimento = "DataVencimento";
			 public const string Descricao = "Descricao";
			 public const string Valor = "Valor";
			 public const string Situacao = "Situacao";
			 public const string Origem = "Origem";
			 public const string Fonte = "Fonte";
			 public const string IdAgente = "IdAgente";
			 public const string IdentificadorOrigem = "IdentificadorOrigem";
			 public const string IdConta = "IdConta";
			 public const string IdEvento = "IdEvento";
			 public const string IdEventoVencimento = "IdEventoVencimento";
			 public const string CodigoAtivo = "CodigoAtivo";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string IdLiquidacao = "IdLiquidacao";
			 public const string DataHistorico = "DataHistorico";
			 public const string IdCliente = "IdCliente";
			 public const string DataLancamento = "DataLancamento";
			 public const string DataVencimento = "DataVencimento";
			 public const string Descricao = "Descricao";
			 public const string Valor = "Valor";
			 public const string Situacao = "Situacao";
			 public const string Origem = "Origem";
			 public const string Fonte = "Fonte";
			 public const string IdAgente = "IdAgente";
			 public const string IdentificadorOrigem = "IdentificadorOrigem";
			 public const string IdConta = "IdConta";
			 public const string IdEvento = "IdEvento";
			 public const string IdEventoVencimento = "IdEventoVencimento";
			 public const string CodigoAtivo = "CodigoAtivo";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(LiquidacaoAberturaMetadata))
			{
				if(LiquidacaoAberturaMetadata.mapDelegates == null)
				{
					LiquidacaoAberturaMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (LiquidacaoAberturaMetadata.meta == null)
				{
					LiquidacaoAberturaMetadata.meta = new LiquidacaoAberturaMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();
				

				meta.AddTypeMap("IdLiquidacao", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("DataHistorico", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("IdCliente", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("DataLancamento", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("DataVencimento", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("Descricao", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("Valor", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("Situacao", new esTypeMap("tinyint", "System.Byte"));
				meta.AddTypeMap("Origem", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("Fonte", new esTypeMap("tinyint", "System.Byte"));
				meta.AddTypeMap("IdAgente", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("IdentificadorOrigem", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("IdConta", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("IdEvento", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("IdEventoVencimento", new esTypeMap("int", "System.Int32"));			
				meta.AddTypeMap("CodigoAtivo", new esTypeMap("varchar", "System.String"));			
				
				
				
				meta.Source = "LiquidacaoAbertura";
				meta.Destination = "LiquidacaoAbertura";
				
				meta.spInsert = "proc_LiquidacaoAberturaInsert";				
				meta.spUpdate = "proc_LiquidacaoAberturaUpdate";		
				meta.spDelete = "proc_LiquidacaoAberturaDelete";
				meta.spLoadAll = "proc_LiquidacaoAberturaLoadAll";
				meta.spLoadByPrimaryKey = "proc_LiquidacaoAberturaLoadByPrimaryKey";
				
				this._providerMetadataMaps["esDefault"] = meta;
			}
			
			return this._providerMetadataMaps["esDefault"];
		}

		#endregion

		static private LiquidacaoAberturaMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
