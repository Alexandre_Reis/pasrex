﻿using System;

namespace Financial.ContaCorrente.Exceptions
{
    /// <summary>
    /// Classe base de Exceção do componente de ContaCorrente
    /// </summary>
    public class ContaCorrenteException : Exception
    {
        /// <summary>
        ///  Construtor
        /// </summary>
        public ContaCorrenteException() { }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="mensagem"></param>
        public ContaCorrenteException(string mensagem) : base(mensagem) { }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="mensagem"></param>
        /// <param name="inner"></param>
        public ContaCorrenteException(string mensagem, Exception inner) : base(mensagem, inner) { }
    }      

}


