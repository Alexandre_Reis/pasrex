﻿using System;
using Financial.Util;
using System.Collections.Generic;
using Financial.Investidor;
using Financial.Bolsa;
using Financial.RendaFixa;
using Financial.RendaFixa.Enums;
using Financial.ContaCorrente;
using Financial.Fundo;
using Financial.Common;
using EntitySpaces.Interfaces;
using Financial.Common.Enums;
using Financial.BMF;
using Financial.Bolsa.Enums;
using Financial.Investidor.Enums;
using System.Data;
using System.Reflection;

namespace FinancialDesk.BO.Tables
{
    public class TableValuesIndicadores
    {
        #region Atributos e properties
        int idCarteira;
        public int IdCarteira
        {
            get { return idCarteira; }
            set { idCarteira = value; }
        }

        int idBenchmark;
        public int IdBenchmark
        {
            get { return idBenchmark; }
            set { idBenchmark = value; }
        }

        string descricaoBenchmark;
        public string DescricaoBenchmark
        {
            get { return descricaoBenchmark; }
            set { descricaoBenchmark = value; }
        }

        DateTime data;
        public DateTime Data
        {
            get { return data; }
            set { data = value; }
        }

        byte tipoIndiceBenchmark;
        public byte TipoIndiceBenchmark
        {
            get { return tipoIndiceBenchmark; }
            set { tipoIndiceBenchmark = value; }
        }

        DateTime dataInicioCota;
        public DateTime DataInicioCota
        {
            get { return dataInicioCota; }
            set { dataInicioCota = value; }
        }

        List<CalculoMedida.EstatisticaRetornoMensal> listaEstatisticaRetornoMensal;
        public List<CalculoMedida.EstatisticaRetornoMensal> ListaEstatisticaRetornoMensal
        {
            get { return listaEstatisticaRetornoMensal; }
            set { listaEstatisticaRetornoMensal = value; }
        }
        #endregion

        public TableValuesIndicadores(int idCarteira, int idBenchmark, DateTime data)
        {
            this.idCarteira = idCarteira;
            this.idBenchmark = idBenchmark;

            //Seta a dataAtual do Cliente
            Cliente cliente = new Cliente();
            List<esQueryItem> campos = new List<esQueryItem>();
            campos.Add(cliente.Query.DataDia);
            campos.Add(cliente.Query.Status);
            cliente.LoadByPrimaryKey(campos, idCarteira);

            if (cliente.DataDia.Value > data)
            {
                this.data = data;
            }
            else
            {
                //Se a carteira não foi calculada na data ou está com último cálculo não ok, pega a data anterior
                if (cliente.Status.Value == (byte)StatusCliente.Fechado || cliente.Status.Value == (byte)StatusCliente.Divulgado)
                {
                    this.data = cliente.DataDia.Value;
                }
                else
                {
                    this.data = Calendario.SubtraiDiaUtil(cliente.DataDia.Value, 1);
                }
            }

            Indice indice = new Indice();
            campos = new List<esQueryItem>();
            campos.Add(indice.Query.Tipo);
            campos.Add(indice.Query.Descricao);
            indice.LoadByPrimaryKey(campos, (short)idBenchmark);
            this.tipoIndiceBenchmark = indice.Tipo.Value;
            this.descricaoBenchmark = indice.Descricao;

            Carteira carteira = new Carteira();
            campos = new List<esQueryItem>();
            campos.Add(carteira.Query.DataInicioCota);
            carteira.LoadByPrimaryKey(campos, idCarteira);

            this.dataInicioCota = carteira.DataInicioCota.Value;

            CalculoMedida calculoMedida = new CalculoMedida(this.idCarteira, this.idBenchmark, DateTime.Now, data);
            this.listaEstatisticaRetornoMensal = calculoMedida.RetornaListaRetornosMensais(this.dataInicioCota, this.data);
        }

        public class QuadroRetorno
        {
            private int ano;
            public int Ano
            {
                get { return ano; }
                set { ano = value; }
            }

            string benchmark;
            public string Benchmark
            {
                get { return benchmark; }
                set { benchmark = value; }
            }

            #region atributos/properties dos retornos da carteira
            decimal? carteiraJan;
            public decimal? CarteiraJan
            {
                get { return carteiraJan; }
                set { carteiraJan = value; }
            }

            decimal? carteiraFev;
            public decimal? CarteiraFev
            {
                get { return carteiraFev; }
                set { carteiraFev = value; }
            }

            decimal? carteiraMar;
            public decimal? CarteiraMar
            {
                get { return carteiraMar; }
                set { carteiraMar = value; }
            }

            decimal? carteiraAbr;
            public decimal? CarteiraAbr
            {
                get { return carteiraAbr; }
                set { carteiraAbr = value; }
            }

            decimal? carteiraMai;
            public decimal? CarteiraMai
            {
                get { return carteiraMai; }
                set { carteiraMai = value; }
            }

            decimal? carteiraJun;
            public decimal? CarteiraJun
            {
                get { return carteiraJun; }
                set { carteiraJun = value; }
            }

            decimal? carteiraJul;
            public decimal? CarteiraJul
            {
                get { return carteiraJul; }
                set { carteiraJul = value; }
            }

            decimal? carteiraAgo;
            public decimal? CarteiraAgo
            {
                get { return carteiraAgo; }
                set { carteiraAgo = value; }
            }

            decimal? carteiraSet;
            public decimal? CarteiraSet
            {
                get { return carteiraSet; }
                set { carteiraSet = value; }
            }

            decimal? carteiraOut;
            public decimal? CarteiraOut
            {
                get { return carteiraOut; }
                set { carteiraOut = value; }
            }

            decimal? carteiraNov;
            public decimal? CarteiraNov
            {
                get { return carteiraNov; }
                set { carteiraNov = value; }
            }

            decimal? carteiraDez;
            public decimal? CarteiraDez
            {
                get { return carteiraDez; }
                set { carteiraDez = value; }
            }

            decimal? carteiraAno;
            public decimal? CarteiraAno
            {
                get { return carteiraAno; }
                set { carteiraAno = value; }
            }
            #endregion

            #region atributos/properties dos retornos do indice
            decimal? indiceJan;
            public decimal? IndiceJan
            {
                get { return indiceJan; }
                set { indiceJan = value; }
            }

            decimal? indiceFev;
            public decimal? IndiceFev
            {
                get { return indiceFev; }
                set { indiceFev = value; }
            }

            decimal? indiceMar;
            public decimal? IndiceMar
            {
                get { return indiceMar; }
                set { indiceMar = value; }
            }

            decimal? indiceAbr;
            public decimal? IndiceAbr
            {
                get { return indiceAbr; }
                set { indiceAbr = value; }
            }

            decimal? indiceMai;
            public decimal? IndiceMai
            {
                get { return indiceMai; }
                set { indiceMai = value; }
            }

            decimal? indiceJun;
            public decimal? IndiceJun
            {
                get { return indiceJun; }
                set { indiceJun = value; }
            }

            decimal? indiceJul;
            public decimal? IndiceJul
            {
                get { return indiceJul; }
                set { indiceJul = value; }
            }

            decimal? indiceAgo;
            public decimal? IndiceAgo
            {
                get { return indiceAgo; }
                set { indiceAgo = value; }
            }

            decimal? indiceSet;
            public decimal? IndiceSet
            {
                get { return indiceSet; }
                set { indiceSet = value; }
            }

            decimal? indiceOut;
            public decimal? IndiceOut
            {
                get { return indiceOut; }
                set { indiceOut = value; }
            }

            decimal? indiceNov;
            public decimal? IndiceNov
            {
                get { return indiceNov; }
                set { indiceNov = value; }
            }

            decimal? indiceDez;
            public decimal? IndiceDez
            {
                get { return indiceDez; }
                set { indiceDez = value; }
            }

            decimal? indiceAno;
            public decimal? IndiceAno
            {
                get { return indiceAno; }
                set { indiceAno = value; }
            }
            #endregion

            #region atributos/properties dos retornos do diferencial
            decimal? diferencialJan;
            public decimal? DiferencialJan
            {
                get { return diferencialJan; }
                set { diferencialJan = value; }
            }

            decimal? diferencialFev;
            public decimal? DiferencialFev
            {
                get { return diferencialFev; }
                set { diferencialFev = value; }
            }

            decimal? diferencialMar;
            public decimal? DiferencialMar
            {
                get { return diferencialMar; }
                set { diferencialMar = value; }
            }

            decimal? diferencialAbr;
            public decimal? DiferencialAbr
            {
                get { return diferencialAbr; }
                set { diferencialAbr = value; }
            }

            decimal? diferencialMai;
            public decimal? DiferencialMai
            {
                get { return diferencialMai; }
                set { diferencialMai = value; }
            }

            decimal? diferencialJun;
            public decimal? DiferencialJun
            {
                get { return diferencialJun; }
                set { diferencialJun = value; }
            }

            decimal? diferencialJul;
            public decimal? DiferencialJul
            {
                get { return diferencialJul; }
                set { diferencialJul = value; }
            }

            decimal? diferencialAgo;
            public decimal? DiferencialAgo
            {
                get { return diferencialAgo; }
                set { diferencialAgo = value; }
            }

            decimal? diferencialSet;
            public decimal? DiferencialSet
            {
                get { return diferencialSet; }
                set { diferencialSet = value; }
            }

            decimal? diferencialOut;
            public decimal? DiferencialOut
            {
                get { return diferencialOut; }
                set { diferencialOut = value; }
            }

            decimal? diferencialNov;
            public decimal? DiferencialNov
            {
                get { return diferencialNov; }
                set { diferencialNov = value; }
            }

            decimal? diferencialDez;
            public decimal? DiferencialDez
            {
                get { return diferencialDez; }
                set { diferencialDez = value; }
            }

            decimal? diferencialAno;
            public decimal? DiferencialAno
            {
                get { return diferencialAno; }
                set { diferencialAno = value; }
            }
            #endregion

        }

        public class Estatistica1
        {
            string descricao;
            public string Descricao
            {
                get { return descricao; }
                set { descricao = value; }
            }

            int quantidade;
            public int Quantidade
            {
                get { return quantidade; }
                set { quantidade = value; }
            }

            decimal percentual;
            public decimal Percentual
            {
                get { return percentual; }
                set { percentual = value; }
            }
        }

        public class Estatistica2
        {
            string descricao;
            public string Descricao
            {
                get { return descricao; }
                set { descricao = value; }
            }

            decimal retorno;
            public decimal Retorno
            {
                get { return retorno; }
                set { retorno = value; }
            }

            string mes;
            public string Mes
            {
                get { return mes; }
                set { mes = value; }
            }
        }

        public class RetornoPeriodo
        {
            #region atributos e properties
            private string periodo;

            public string Periodo
            {
                get { return periodo; }
                set { periodo = value; }
            }

            private decimal? retornoCarteira;

            public decimal? RetornoCarteira
            {
                get { return retornoCarteira; }
                set { retornoCarteira = value; }
            }

            private decimal? retornoBenchmark;

            public decimal? RetornoBenchmark
            {
                get { return retornoBenchmark; }
                set { retornoBenchmark = value; }
            }

            private decimal? retornoDiferencial;

            public decimal? RetornoDiferencial
            {
                get { return retornoDiferencial; }
                set { retornoDiferencial = value; }
            }
            #endregion

            public RetornoPeriodo MontaRetornoMes(DateTime data, byte tipoIndice, CalculoMedida calculoMedida, int numeroMeses)
            {
                RetornoPeriodo retornoPeriodo = new RetornoPeriodo();
                retornoPeriodo.Periodo = numeroMeses.ToString() + " meses";
                retornoPeriodo.retornoCarteira = null;
                retornoPeriodo.retornoBenchmark = null;
                retornoPeriodo.retornoDiferencial = null;
                try
                {
                    retornoPeriodo.retornoCarteira = calculoMedida.CalculaRetornoPeriodoMes(data, numeroMeses);
                }
                catch (Exception e)
                {
                }

                try
                {
                    retornoPeriodo.retornoBenchmark = calculoMedida.CalculaRetornoPeriodoMesIndice(data, numeroMeses);
                }
                catch (Exception e)
                {
                }

                if (retornoPeriodo.retornoCarteira.HasValue && retornoPeriodo.retornoBenchmark.HasValue)
                {
                    if (tipoIndice == (byte)TipoIndice.Decimal)
                    {
                        retornoPeriodo.retornoDiferencial = retornoPeriodo.retornoCarteira.Value - retornoPeriodo.retornoBenchmark.Value;
                    }
                    else
                    {
                        if (retornoPeriodo.retornoBenchmark.Value == 0)
                        {
                            retornoPeriodo.retornoDiferencial = 0;
                        }
                        else
                        {
                            retornoPeriodo.retornoDiferencial = (retornoPeriodo.retornoCarteira.Value / retornoPeriodo.retornoBenchmark.Value) * 100M;
                        }
                    }
                }

                return retornoPeriodo;
            }
        }

        public class Indicadores
        {
            private decimal? vol3Meses;

            public decimal? Vol3Meses
            {
                get { return vol3Meses; }
                set { vol3Meses = value; }
            }

            private decimal? vol6Meses;

            public decimal? Vol6Meses
            {
                get { return vol6Meses; }
                set { vol6Meses = value; }
            }

            private decimal? vol12Meses;

            public decimal? Vol12Meses
            {
                get { return vol12Meses; }
                set { vol12Meses = value; }
            }

            private decimal? sharpe3Meses;

            public decimal? Sharpe3Meses
            {
                get { return sharpe3Meses; }
                set { sharpe3Meses = value; }
            }

            private decimal? sharpe6Meses;

            public decimal? Sharpe6Meses
            {
                get { return sharpe6Meses; }
                set { sharpe6Meses = value; }
            }

            private decimal? sharpe12Meses;

            public decimal? Sharpe12Meses
            {
                get { return sharpe12Meses; }
                set { sharpe12Meses = value; }
            }
        }

        public List<QuadroRetorno> RetornaListaQuadroRetorno(DataTable datatable)
        {
            List<QuadroRetorno> listaQuadroRetorno = new List<QuadroRetorno>();

            for (int rowCount = 1; rowCount <= datatable.Rows.Count; rowCount++)
            {
                QuadroRetorno quadroRetorno = new QuadroRetorno();

                string propertyPrefix = null;
                int rowCountMod = rowCount % 3;
                if (rowCountMod == 1)
                {
                    propertyPrefix = "Carteira";
                }
                else if (rowCountMod == 2)
                {
                    propertyPrefix = "Indice";
                }
                else
                {
                    propertyPrefix = "Diferencial";
                }

                string[] PROPERTIES_REPETIDAS = { "Teste1", "Teste2" };

                Type quadroRetornoType = quadroRetorno.GetType();
                foreach (string propertyRepetida in PROPERTIES_REPETIDAS)
                {
                    PropertyInfo property = quadroRetornoType.GetProperty(propertyRepetida);
                    //property.SetValue(quadroRetorno, Decimal.Parse(linha.dsTit4), null);
                    //i++;
                }
                //quadroRetorno.                

                listaQuadroRetorno.Add(quadroRetorno);
            }

            return listaQuadroRetorno;

            /*if (this.listaEstatisticaRetornoMensal.Count == 0)
            {
                return lista; //retorna vazia
            }

            CalculoMedida.EstatisticaRetornoMensal estatisticaRetornoMensal = new CalculoMedida.EstatisticaRetornoMensal();
            estatisticaRetornoMensal = this.listaEstatisticaRetornoMensal[0];
            DateTime dataRetorno = estatisticaRetornoMensal.Data;
            int ano = dataRetorno.Year;
            int anoAnterior = ano;
            int i = 0;
            while (i < listaEstatisticaRetornoMensal.Count)
            {
                QuadroRetorno quadroRetorno = new QuadroRetorno();
                quadroRetorno.Ano = ano;

                if (this.tipoIndiceBenchmark == (byte)TipoIndice.Decimal)
                {
                    quadroRetorno.Benchmark = "(-) " + this.descricaoBenchmark;
                }
                else
                {
                    quadroRetorno.Benchmark = "(%) " + this.descricaoBenchmark;
                }

                anoAnterior = ano;

                decimal fatorAno = 1;
                decimal fatorAnoBenchmark = 1;
                while (ano == anoAnterior)
                {
                    switch (dataRetorno.Month)
                    {
                        case 1:
                            quadroRetorno.CarteiraJan = estatisticaRetornoMensal.Retorno;
                            quadroRetorno.DiferencialJan = estatisticaRetornoMensal.RetornoDiferencial;
                            break;
                        case 2:
                            quadroRetorno.CarteiraFev = estatisticaRetornoMensal.Retorno;
                            quadroRetorno.DiferencialFev = estatisticaRetornoMensal.RetornoDiferencial;
                            break;
                        case 3:
                            quadroRetorno.CarteiraMar = estatisticaRetornoMensal.Retorno;
                            quadroRetorno.DiferencialMar = estatisticaRetornoMensal.RetornoDiferencial;
                            break;
                        case 4:
                            quadroRetorno.CarteiraAbr = estatisticaRetornoMensal.Retorno;
                            quadroRetorno.DiferencialAbr = estatisticaRetornoMensal.RetornoDiferencial;
                            break;
                        case 5:
                            quadroRetorno.CarteiraMai = estatisticaRetornoMensal.Retorno;
                            quadroRetorno.DiferencialMai = estatisticaRetornoMensal.RetornoDiferencial;
                            break;
                        case 6:
                            quadroRetorno.CarteiraJun = estatisticaRetornoMensal.Retorno;
                            quadroRetorno.DiferencialJun = estatisticaRetornoMensal.RetornoDiferencial;
                            break;
                        case 7:
                            quadroRetorno.CarteiraJul = estatisticaRetornoMensal.Retorno;
                            quadroRetorno.DiferencialJul = estatisticaRetornoMensal.RetornoDiferencial;
                            break;
                        case 8:
                            quadroRetorno.CarteiraAgo = estatisticaRetornoMensal.Retorno;
                            quadroRetorno.DiferencialAgo = estatisticaRetornoMensal.RetornoDiferencial;
                            break;
                        case 9:
                            quadroRetorno.CarteiraSet = estatisticaRetornoMensal.Retorno;
                            quadroRetorno.DiferencialSet = estatisticaRetornoMensal.RetornoDiferencial;
                            break;
                        case 10:
                            quadroRetorno.CarteiraOut = estatisticaRetornoMensal.Retorno;
                            quadroRetorno.DiferencialOut = estatisticaRetornoMensal.RetornoDiferencial;
                            break;
                        case 11:
                            quadroRetorno.CarteiraNov = estatisticaRetornoMensal.Retorno;
                            quadroRetorno.DiferencialNov = estatisticaRetornoMensal.RetornoDiferencial;
                            break;
                        case 12:
                            quadroRetorno.CarteiraDez = estatisticaRetornoMensal.Retorno;
                            quadroRetorno.DiferencialDez = estatisticaRetornoMensal.RetornoDiferencial;
                            break;
                    }

                    //Calcula o fator do retorno ano da carteira e do diferencial
                    fatorAno = fatorAno * (estatisticaRetornoMensal.Retorno / 100M + 1);

                    if (estatisticaRetornoMensal.RetornoBenchmark.HasValue)
                    {
                        fatorAnoBenchmark = fatorAnoBenchmark * (estatisticaRetornoMensal.RetornoBenchmark.Value / 100M + 1);
                    }

                    i += 1;

                    if (i == listaEstatisticaRetornoMensal.Count)
                    {
                        break;
                    }
                    else
                    {
                        estatisticaRetornoMensal = listaEstatisticaRetornoMensal[i];
                        dataRetorno = estatisticaRetornoMensal.Data;
                        ano = dataRetorno.Year;
                    }
                }

                decimal retornoAno = (fatorAno - 1) * 100M;
                decimal retornoAnoBenchmark = (fatorAnoBenchmark - 1) * 100M;

                quadroRetorno.CarteiraAno = retornoAno;

                if (this.tipoIndiceBenchmark == (byte)TipoIndice.Decimal)
                {
                    quadroRetorno.DiferencialAno = retornoAno - retornoAnoBenchmark;
                }
                else
                {
                    if (retornoAnoBenchmark == 0)
                    {
                        quadroRetorno.DiferencialAno = 0;
                    }
                    else
                    {
                        quadroRetorno.DiferencialAno = (retornoAno / retornoAnoBenchmark) * 100M;
                    }
                }

                lista.Add(quadroRetorno);
            }*/


        }

        public List<Estatistica1> RetornaListaEstatistica1()
        {
            List<Estatistica1> lista = new List<Estatistica1>();

            if (this.listaEstatisticaRetornoMensal.Count == 0)
            {
                return lista;
            }

            decimal mesesPositivos = 0;
            decimal mesesNegativos = 0;
            decimal mesesAcimaBenchmark = 0;
            decimal mesesAbaixoBenchmark = 0;
            foreach (CalculoMedida.EstatisticaRetornoMensal estatisticaRetornoMensal in this.listaEstatisticaRetornoMensal)
            {
                if (estatisticaRetornoMensal.Retorno >= 0)
                {
                    mesesPositivos += 1;
                }
                else
                {
                    mesesNegativos += 1;
                }

                if (estatisticaRetornoMensal.Retorno >= estatisticaRetornoMensal.RetornoBenchmark)
                {
                    mesesAcimaBenchmark += 1;
                }
                else
                {
                    mesesAbaixoBenchmark += 1;
                }
            }

            Estatistica1 estatistica1 = new Estatistica1();
            estatistica1.Descricao = "Meses com retorno positivo";
            estatistica1.Quantidade = Convert.ToInt32(mesesPositivos);
            estatistica1.Percentual = Math.Round(mesesPositivos / (mesesPositivos + mesesNegativos) * 100M, 2);
            lista.Add(estatistica1);

            estatistica1 = new Estatistica1();
            estatistica1.Descricao = "Meses com retorno negativo";
            estatistica1.Quantidade = Convert.ToInt32(mesesNegativos);
            estatistica1.Percentual = Math.Round(mesesNegativos / (mesesPositivos + mesesNegativos) * 100M, 2);
            lista.Add(estatistica1);

            estatistica1 = new Estatistica1();
            estatistica1.Descricao = "Meses acima do benchmark";
            estatistica1.Quantidade = Convert.ToInt32(mesesAcimaBenchmark);
            estatistica1.Percentual = Math.Round(mesesAcimaBenchmark / (mesesAcimaBenchmark + mesesAbaixoBenchmark) * 100M, 2);
            lista.Add(estatistica1);

            estatistica1 = new Estatistica1();
            estatistica1.Descricao = "Meses abaixo do benchmark";
            estatistica1.Quantidade = Convert.ToInt32(mesesAbaixoBenchmark);
            estatistica1.Percentual = Math.Round(mesesAbaixoBenchmark / (mesesAcimaBenchmark + mesesAbaixoBenchmark) * 100M, 2);
            lista.Add(estatistica1);

            return lista;
        }

        public List<Estatistica2> RetornaListaEstatistica2()
        {
            List<Estatistica2> lista = new List<Estatistica2>();

            if (this.listaEstatisticaRetornoMensal.Count == 0)
            {
                return lista;
            }

            decimal maiorRetorno = -99999999999999999;
            decimal menorRetorno = 99999999999999999;
            string mesMaiorRetorno = "";
            string mesMenorRetorno = "";
            foreach (CalculoMedida.EstatisticaRetornoMensal estatisticaRetornoMensal in this.listaEstatisticaRetornoMensal)
            {
                if (estatisticaRetornoMensal.Retorno > maiorRetorno)
                {
                    maiorRetorno = estatisticaRetornoMensal.Retorno.Value;
                    mesMaiorRetorno = estatisticaRetornoMensal.Data.Month.ToString() + "/" + estatisticaRetornoMensal.Data.Year.ToString();
                }

                if (estatisticaRetornoMensal.Retorno < menorRetorno)
                {
                    menorRetorno = estatisticaRetornoMensal.Retorno.Value;
                    mesMenorRetorno = estatisticaRetornoMensal.Data.Month.ToString() + "/" + estatisticaRetornoMensal.Data.Year.ToString();
                }
            }

            Estatistica2 estatistica2 = new Estatistica2();
            estatistica2.Descricao = "Maior retorno da carteira ";
            estatistica2.Retorno = maiorRetorno;
            estatistica2.Mes = mesMaiorRetorno;
            lista.Add(estatistica2);

            estatistica2 = new Estatistica2();
            estatistica2.Descricao = "Menor retorno da carteira ";
            estatistica2.Retorno = menorRetorno;
            estatistica2.Mes = mesMenorRetorno;
            lista.Add(estatistica2);

            return lista;
        }

        public List<RetornoPeriodo> RetornaListaRetornoPeriodo()
        {
            List<RetornoPeriodo> listaRetornoPeriodo = new List<RetornoPeriodo>();
            CalculoMedida calculoMedida = new CalculoMedida(this.idCarteira, this.idBenchmark, DateTime.Now, this.data);
            RetornoPeriodo retornoPeriodo = new RetornoPeriodo();

            int numeroMeses = 3;
            retornoPeriodo = retornoPeriodo.MontaRetornoMes(this.data, this.tipoIndiceBenchmark, calculoMedida, numeroMeses);
            listaRetornoPeriodo.Add(retornoPeriodo);

            numeroMeses = 6;
            retornoPeriodo = retornoPeriodo.MontaRetornoMes(this.data, this.tipoIndiceBenchmark, calculoMedida, numeroMeses);
            listaRetornoPeriodo.Add(retornoPeriodo);

            numeroMeses = 12;
            retornoPeriodo = retornoPeriodo.MontaRetornoMes(this.data, this.tipoIndiceBenchmark, calculoMedida, numeroMeses);
            listaRetornoPeriodo.Add(retornoPeriodo);

            numeroMeses = 24;
            retornoPeriodo = retornoPeriodo.MontaRetornoMes(this.data, this.tipoIndiceBenchmark, calculoMedida, numeroMeses);
            listaRetornoPeriodo.Add(retornoPeriodo);

            numeroMeses = 36;
            retornoPeriodo = retornoPeriodo.MontaRetornoMes(this.data, this.tipoIndiceBenchmark, calculoMedida, numeroMeses);
            listaRetornoPeriodo.Add(retornoPeriodo);

            //Desde o início
            retornoPeriodo = new RetornoPeriodo();
            retornoPeriodo.Periodo = "Desde o início";
            retornoPeriodo.RetornoCarteira = null;
            retornoPeriodo.RetornoBenchmark = null;
            retornoPeriodo.RetornoDiferencial = null;
            try
            {
                retornoPeriodo.RetornoCarteira = calculoMedida.CalculaRetornoInicio(this.data);
            }
            catch (Exception e)
            {
            }

            try
            {
                retornoPeriodo.RetornoBenchmark = calculoMedida.CalculaRetornoInicioIndice(this.data);
            }
            catch (Exception)
            {
            }

            if (retornoPeriodo.RetornoCarteira.HasValue && retornoPeriodo.RetornoBenchmark.HasValue)
            {
                if (this.tipoIndiceBenchmark == (byte)TipoIndice.Decimal)
                {
                    retornoPeriodo.RetornoDiferencial = retornoPeriodo.RetornoCarteira.Value - retornoPeriodo.RetornoBenchmark.Value;
                }
                else
                {
                    if (retornoPeriodo.RetornoBenchmark.Value == 0)
                    {
                        retornoPeriodo.RetornoDiferencial = 0;
                    }
                    else
                    {
                        retornoPeriodo.RetornoDiferencial = (retornoPeriodo.RetornoCarteira.Value / retornoPeriodo.RetornoBenchmark.Value) * 100M;
                    }
                }
            }
            listaRetornoPeriodo.Add(retornoPeriodo);
            //

            return listaRetornoPeriodo;
        }

        public Indicadores RetornaIndicadores()
        {
            Indicadores indicadores = new Indicadores();

            CalculoMedida calculoMedida = new CalculoMedida(this.idCarteira, this.idBenchmark, DateTime.Now, this.data);

            if (calculoMedida.CarteiraSimulada)
            {
                calculoMedida.InitListaRetornoMensal(calculoMedida.DataInicioJanela.Value, calculoMedida.DataFimJanela.Value);
            }
            else
            {
                calculoMedida.InitListaRetornoMensal(dataInicioCota, this.data);
            }
            int elementos = calculoMedida.ListaRetornoMensal.Count;
            DateTime dataUltima = new DateTime();
            if (elementos > 0)
            {
                dataUltima = calculoMedida.ListaRetornoMensal[elementos - 1].Data;
            }

            decimal? vol3Meses = null;
            decimal? vol6Meses = null;
            decimal? vol12Meses = null;
            decimal? sharpe3Meses = null;
            decimal? sharpe6Meses = null;
            decimal? sharpe12Meses = null;

            IndicadoresCarteira indicadorCarteira = calculoMedida.retornaIndicadorCarteira(this.idCarteira, dataUltima); 

            DateTime? data3Meses = null;
            if (elementos >= 3)
            {
                data3Meses = calculoMedida.ListaRetornoMensal[elementos - 3].Data;
                if (!Calendario.IsDiaUtil(data3Meses.Value))
                {
                    data3Meses = Calendario.AdicionaDiaUtil(data3Meses.Value, 1);
                }
                vol3Meses = calculoMedida.CalculaVolatilidade(data3Meses.Value, dataUltima);

                if (indicadorCarteira.IdCarteira > 0 && indicadorCarteira.Sharpe3Meses != null)
                    sharpe3Meses = indicadorCarteira.Sharpe3Meses;
                else
                    sharpe3Meses = calculoMedida.CalculaSharpe(data3Meses.Value, dataUltima, vol3Meses);


            }
            DateTime? data6Meses = null;
            if (elementos >= 6)
            {
                data6Meses = calculoMedida.ListaRetornoMensal[elementos - 6].Data;
                if (!Calendario.IsDiaUtil(data6Meses.Value))
                {
                    data6Meses = Calendario.AdicionaDiaUtil(data6Meses.Value, 1);
                }
                vol6Meses = calculoMedida.CalculaVolatilidade(data6Meses.Value, dataUltima);

                if (indicadorCarteira.IdCarteira > 0 && indicadorCarteira.Sharpe6Meses != null)
                    sharpe6Meses = indicadorCarteira.Sharpe6Meses;
                else
                    sharpe6Meses = calculoMedida.CalculaSharpe(data6Meses.Value, dataUltima, vol6Meses);

            }
            DateTime? data12Meses = null;
            if (elementos >= 12)
            {
                data12Meses = calculoMedida.ListaRetornoMensal[elementos - 12].Data;
                if (!Calendario.IsDiaUtil(data12Meses.Value))
                {
                    data12Meses = Calendario.AdicionaDiaUtil(data12Meses.Value, 1);
                }
                vol12Meses = calculoMedida.CalculaVolatilidade(data12Meses.Value, dataUltima);

                if (indicadorCarteira.IdCarteira > 0 && indicadorCarteira.Sharpe12Meses != null)
                    sharpe12Meses = indicadorCarteira.Sharpe12Meses;
                else
                    sharpe12Meses = calculoMedida.CalculaSharpe(data12Meses.Value, dataUltima, vol12Meses);
            }


            indicadores.Vol3Meses = vol3Meses;
            indicadores.Vol6Meses = vol6Meses;
            indicadores.Vol12Meses = vol12Meses;
            indicadores.Sharpe3Meses = sharpe3Meses;
            indicadores.Sharpe6Meses = sharpe6Meses;
            indicadores.Sharpe12Meses = sharpe12Meses;

            return indicadores;
        }

 
    }

    public class TableValuesPosicaoMercado
    {
        #region Atributos e properties
        int idCliente;
        public int IdCliente
        {
            get { return idCliente; }
            set { idCliente = value; }
        }

        DateTime dataReferencia;
        public DateTime DataReferencia
        {
            get { return dataReferencia; }
            set { dataReferencia = value; }
        }

        bool isPosicaoHistorica;
        public bool IsPosicaoHistorica
        {
            get { return isPosicaoHistorica; }
            set { isPosicaoHistorica = value; }
        }
        #endregion

        public TableValuesPosicaoMercado(int idCliente, DateTime data)
        {
            this.idCliente = idCliente;
            this.dataReferencia = data;

            Cliente cliente = new Cliente();
            List<esQueryItem> campos = new List<esQueryItem>();
            campos.Add(cliente.Query.DataDia);
            cliente.LoadByPrimaryKey(campos, idCliente);
            DateTime dataDia = cliente.DataDia.Value;

            if (DateTime.Compare(data, dataDia) >= 0)
            {
                this.isPosicaoHistorica = false;
            }
            else
            {
                this.isPosicaoHistorica = true;
            }
        }

        public class PosicaoBolsaSintetica
        {
            string cdAtivoBolsa;
            public string CdAtivoBolsa
            {
                get { return cdAtivoBolsa; }
                set { cdAtivoBolsa = value; }
            }

            DateTime? dataVencimento;
            public DateTime? DataVencimento
            {
                get { return dataVencimento; }
                set { dataVencimento = value; }
            }

            decimal puCusto;
            public decimal PuCusto
            {
                get { return puCusto; }
                set { puCusto = value; }
            }

            decimal valorCusto;
            public decimal ValorCusto
            {
                get { return valorCusto; }
                set { valorCusto = value; }
            }

            decimal quantidade;
            public decimal Quantidade
            {
                get { return quantidade; }
                set { quantidade = value; }
            }

            decimal puMercado;
            public decimal PuMercado
            {
                get { return puMercado; }
                set { puMercado = value; }
            }

            decimal valorMercado;
            public decimal ValorMercado
            {
                get { return valorMercado; }
                set { valorMercado = value; }
            }

            decimal resultadoRealizar;
            public decimal ResultadoRealizar
            {
                get { return resultadoRealizar; }
                set { resultadoRealizar = value; }
            }
        }

        public class PosicaoTermoBolsaSintetica
        {
            string cdAtivoBolsa;
            public string CdAtivoBolsa
            {
                get { return cdAtivoBolsa; }
                set { cdAtivoBolsa = value; }
            }

            DateTime dataOperacao;
            public DateTime DataOperacao
            {
                get { return dataOperacao; }
                set { dataOperacao = value; }
            }

            DateTime dataVencimento;
            public DateTime DataVencimento
            {
                get { return dataVencimento; }
                set { dataVencimento = value; }
            }

            decimal puTermo;
            public decimal PuTermo
            {
                get { return puTermo; }
                set { puTermo = value; }
            }

            decimal valorTermo;
            public decimal ValorTermo
            {
                get { return valorTermo; }
                set { valorTermo = value; }
            }

            decimal quantidade;
            public decimal Quantidade
            {
                get { return quantidade; }
                set { quantidade = value; }
            }

            decimal puMercado;
            public decimal PuMercado
            {
                get { return puMercado; }
                set { puMercado = value; }
            }

            decimal valorMercado;
            public decimal ValorMercado
            {
                get { return valorMercado; }
                set { valorMercado = value; }
            }
        }

        public class PosicaoFuturoBMFSintetica
        {
            string cdAtivoBMF;
            public string CdAtivoBMF
            {
                get { return cdAtivoBMF; }
                set { cdAtivoBMF = value; }
            }

            string serie;
            public string Serie
            {
                get { return serie; }
                set { serie = value; }
            }

            DateTime dataVencimento;
            public DateTime DataVencimento
            {
                get { return dataVencimento; }
                set { dataVencimento = value; }
            }

            decimal quantidade;
            public decimal Quantidade
            {
                get { return quantidade; }
                set { quantidade = value; }
            }

            decimal puMercado;
            public decimal PuMercado
            {
                get { return puMercado; }
                set { puMercado = value; }
            }

            decimal ajusteDiario;
            public decimal AjusteDiario
            {
                get { return ajusteDiario; }
                set { ajusteDiario = value; }
            }
        }

        public class PosicaoFundoSintetica
        {
            string nomeFundo;
            public string NomeFundo
            {
                get { return nomeFundo; }
                set { nomeFundo = value; }
            }

            decimal quantidade;
            public decimal Quantidade
            {
                get { return quantidade; }
                set { quantidade = value; }
            }

            decimal valorCota;
            public decimal ValorCota
            {
                get { return valorCota; }
                set { valorCota = value; }
            }

            decimal saldoBruto;
            public decimal SaldoBruto
            {
                get { return saldoBruto; }
                set { saldoBruto = value; }
            }

            decimal valorIR;
            public decimal ValorIR
            {
                get { return valorIR; }
                set { valorIR = value; }
            }

            decimal valorIOF;
            public decimal ValorIOF
            {
                get { return valorIOF; }
                set { valorIOF = value; }
            }

            decimal saldoLiquido;
            public decimal SaldoLiquido
            {
                get { return saldoLiquido; }
                set { saldoLiquido = value; }
            }

            decimal rendimento;
            public decimal Rendimento
            {
                get { return rendimento; }
                set { rendimento = value; }
            }
        }

        public class PosicaoFundoAnalitica
        {
            int idPosicao;
            public int IdPosicao
            {
                get { return idPosicao; }
                set { idPosicao = value; }
            }

            DateTime dataAplicacao;
            public DateTime DataAplicacao
            {
                get { return dataAplicacao; }
                set { dataAplicacao = value; }
            }

            decimal cotaAplicacao;
            public decimal CotaAplicacao
            {
                get { return cotaAplicacao; }
                set { cotaAplicacao = value; }
            }

            decimal valorAplicado;
            public decimal ValorAplicado
            {
                get { return valorAplicado; }
                set { valorAplicado = value; }
            }

            decimal quantidade;
            public decimal Quantidade
            {
                get { return quantidade; }
                set { quantidade = value; }
            }

            decimal saldoBruto;
            public decimal SaldoBruto
            {
                get { return saldoBruto; }
                set { saldoBruto = value; }
            }

            decimal valorIR;
            public decimal ValorIR
            {
                get { return valorIR; }
                set { valorIR = value; }
            }

            decimal valorIOF;
            public decimal ValorIOF
            {
                get { return valorIOF; }
                set { valorIOF = value; }
            }

            decimal saldoLiquido;
            public decimal SaldoLiquido
            {
                get { return saldoLiquido; }
                set { saldoLiquido = value; }
            }

            decimal rendimento;
            public decimal Rendimento
            {
                get { return rendimento; }
                set { rendimento = value; }
            }
        }

        public class PosicaoRendaFixaSintetica
        {
            string descricaoTitulo;
            public string DescricaoTitulo
            {
                get { return descricaoTitulo; }
                set { descricaoTitulo = value; }
            }

            DateTime dataOperacao;
            public DateTime DataOperacao
            {
                get { return dataOperacao; }
                set { dataOperacao = value; }
            }

            DateTime dataVencimento;
            public DateTime DataVencimento
            {
                get { return dataVencimento; }
                set { dataVencimento = value; }
            }

            decimal quantidade;
            public decimal Quantidade
            {
                get { return quantidade; }
                set { quantidade = value; }
            }

            decimal puOperacao;
            public decimal PuOperacao
            {
                get { return puOperacao; }
                set { puOperacao = value; }
            }

            decimal puMercado;
            public decimal PuMercado
            {
                get { return puMercado; }
                set { puMercado = value; }
            }

            decimal saldoBruto;
            public decimal SaldoBruto
            {
                get { return saldoBruto; }
                set { saldoBruto = value; }
            }

            decimal valorIR;
            public decimal ValorIR
            {
                get { return valorIR; }
                set { valorIR = value; }
            }

            decimal valorIOF;
            public decimal ValorIOF
            {
                get { return valorIOF; }
                set { valorIOF = value; }
            }

            decimal saldoLiquido;
            public decimal SaldoLiquido
            {
                get { return saldoLiquido; }
                set { saldoLiquido = value; }
            }

            decimal taxaOperacao;
            public decimal TaxaOperacao
            {
                get { return taxaOperacao; }
                set { taxaOperacao = value; }
            }

            decimal vlAplicAtualizado;
            public decimal VlAplicAtualizado
            {
                get { return vlAplicAtualizado; }
                set { vlAplicAtualizado = value; }
            }

        }

        public PosicaoBolsaSintetica RetornaPosicaoBolsaSintetica(string cdAtivoBolsa)
        {
            PosicaoBolsaSintetica posicaoBolsaSintetica = new PosicaoBolsaSintetica();

            if (this.isPosicaoHistorica)
            {
                PosicaoBolsaHistorico posicaoBolsaHistorico = new PosicaoBolsaHistorico();
                posicaoBolsaHistorico.Query.Select(posicaoBolsaHistorico.Query.DataVencimento.Max(),
                                                   posicaoBolsaHistorico.Query.PUCustoLiquido.Avg(),
                                                   posicaoBolsaHistorico.Query.ValorCustoLiquido.Sum(),
                                                   posicaoBolsaHistorico.Query.Quantidade.Sum(),
                                                   posicaoBolsaHistorico.Query.PUMercado.Avg(),
                                                   posicaoBolsaHistorico.Query.ValorMercado.Sum(),
                                                   posicaoBolsaHistorico.Query.ResultadoRealizar.Sum());
                posicaoBolsaHistorico.Query.Where(posicaoBolsaHistorico.Query.IdCliente.Equal(this.idCliente),
                                                  posicaoBolsaHistorico.Query.DataHistorico.Equal(this.dataReferencia),
                                                  posicaoBolsaHistorico.Query.CdAtivoBolsa.Equal(cdAtivoBolsa));
                posicaoBolsaHistorico.Query.Load();

                if (posicaoBolsaHistorico.es.HasData && posicaoBolsaHistorico.Quantidade.HasValue)
                {
                    posicaoBolsaSintetica.CdAtivoBolsa = cdAtivoBolsa;

                    posicaoBolsaSintetica.DataVencimento = null;
                    if (posicaoBolsaHistorico.DataVencimento.HasValue)
                    {
                        posicaoBolsaSintetica.DataVencimento = posicaoBolsaHistorico.DataVencimento.Value;
                    }

                    posicaoBolsaSintetica.PuCusto = posicaoBolsaHistorico.PUCustoLiquido.Value;
                    posicaoBolsaSintetica.ValorCusto = posicaoBolsaHistorico.ValorCustoLiquido.Value;
                    posicaoBolsaSintetica.Quantidade = posicaoBolsaHistorico.Quantidade.Value;
                    posicaoBolsaSintetica.PuMercado = posicaoBolsaHistorico.PUMercado.Value;
                    posicaoBolsaSintetica.ValorMercado = posicaoBolsaHistorico.ValorMercado.Value;
                    posicaoBolsaSintetica.ResultadoRealizar = posicaoBolsaHistorico.ResultadoRealizar.Value;
                }
            }
            else
            {
                PosicaoBolsa posicaoBolsa = new PosicaoBolsa();
                posicaoBolsa.Query.Select(posicaoBolsa.Query.DataVencimento.Max(),
                                           posicaoBolsa.Query.PUCustoLiquido.Avg(),
                                           posicaoBolsa.Query.ValorCustoLiquido.Sum(),
                                           posicaoBolsa.Query.Quantidade.Sum(),
                                           posicaoBolsa.Query.PUMercado.Avg(),
                                           posicaoBolsa.Query.ValorMercado.Sum(),
                                           posicaoBolsa.Query.ResultadoRealizar.Sum());
                posicaoBolsa.Query.Where(posicaoBolsa.Query.IdCliente.Equal(this.idCliente),
                                        posicaoBolsa.Query.CdAtivoBolsa.Equal(cdAtivoBolsa));
                posicaoBolsa.Query.Load();

                if (posicaoBolsa.es.HasData && posicaoBolsa.Quantidade.HasValue)
                {
                    posicaoBolsaSintetica.CdAtivoBolsa = cdAtivoBolsa;

                    posicaoBolsaSintetica.DataVencimento = null;
                    if (posicaoBolsa.DataVencimento.HasValue)
                    {
                        posicaoBolsaSintetica.DataVencimento = posicaoBolsa.DataVencimento.Value;
                    }

                    posicaoBolsaSintetica.PuCusto = posicaoBolsa.PUCustoLiquido.Value;
                    posicaoBolsaSintetica.ValorCusto = posicaoBolsa.ValorCustoLiquido.Value;
                    posicaoBolsaSintetica.Quantidade = posicaoBolsa.Quantidade.Value;
                    posicaoBolsaSintetica.PuMercado = posicaoBolsa.PUMercado.Value;
                    posicaoBolsaSintetica.ValorMercado = posicaoBolsa.ValorMercado.Value;
                    posicaoBolsaSintetica.ResultadoRealizar = posicaoBolsa.ResultadoRealizar.Value;
                }
            }

            return posicaoBolsaSintetica;
        }

        public PosicaoTermoBolsaSintetica RetornaPosicaoTermoBolsaSintetica(string cdAtivoBolsa)
        {
            PosicaoTermoBolsaSintetica posicaoTermoBolsaSintetica = new PosicaoTermoBolsaSintetica();

            if (this.isPosicaoHistorica)
            {
                PosicaoTermoBolsaHistorico posicaoTermoBolsaHistorico = new PosicaoTermoBolsaHistorico();
                posicaoTermoBolsaHistorico.Query.Select(posicaoTermoBolsaHistorico.Query.DataVencimento.Max(),
                                                   posicaoTermoBolsaHistorico.Query.DataOperacao.Max(),
                                                   posicaoTermoBolsaHistorico.Query.PUTermo.Avg(),
                                                   posicaoTermoBolsaHistorico.Query.ValorTermo.Sum(),
                                                   posicaoTermoBolsaHistorico.Query.Quantidade.Sum(),
                                                   posicaoTermoBolsaHistorico.Query.PUMercado.Avg(),
                                                   posicaoTermoBolsaHistorico.Query.ValorMercado.Sum(),
                                                   posicaoTermoBolsaHistorico.Query.PrazoDecorrer.Avg(),
                                                   posicaoTermoBolsaHistorico.Query.PrazoTotal.Avg());
                posicaoTermoBolsaHistorico.Query.Where(posicaoTermoBolsaHistorico.Query.IdCliente.Equal(this.idCliente),
                                                  posicaoTermoBolsaHistorico.Query.DataHistorico.Equal(this.dataReferencia),
                                                  posicaoTermoBolsaHistorico.Query.CdAtivoBolsa.Equal(cdAtivoBolsa));
                posicaoTermoBolsaHistorico.Query.Load();

                if (posicaoTermoBolsaHistorico.es.HasData && posicaoTermoBolsaHistorico.Quantidade.HasValue)
                {
                    string cdAtivoBolsaAcao = AtivoBolsa.RetornaCdAtivoBolsaAcao(cdAtivoBolsa);
                    string vencimento = cdAtivoBolsa.Replace(cdAtivoBolsaAcao, "").Replace("T", "");
                    vencimento = vencimento.Substring(0, 2) + "/" + vencimento.Substring(3, 2) + "/" + vencimento.Substring(4, 4);
                    string cdAtivoBolsaDescricao = cdAtivoBolsaAcao + "T - " + vencimento;

                    posicaoTermoBolsaSintetica.CdAtivoBolsa = cdAtivoBolsa;
                    posicaoTermoBolsaSintetica.DataVencimento = posicaoTermoBolsaHistorico.DataVencimento.Value;
                    posicaoTermoBolsaSintetica.DataOperacao = posicaoTermoBolsaHistorico.DataOperacao.Value;
                    posicaoTermoBolsaSintetica.PuTermo = posicaoTermoBolsaHistorico.PUTermo.Value;
                    posicaoTermoBolsaSintetica.ValorTermo = posicaoTermoBolsaHistorico.ValorTermo.Value;
                    posicaoTermoBolsaSintetica.Quantidade = posicaoTermoBolsaHistorico.Quantidade.Value;
                    posicaoTermoBolsaSintetica.PuMercado = posicaoTermoBolsaHistorico.PUMercado.Value;
                    posicaoTermoBolsaSintetica.ValorMercado = posicaoTermoBolsaHistorico.ValorMercado.Value;
                }
            }
            else
            {
                PosicaoTermoBolsa posicaoTermoBolsa = new PosicaoTermoBolsa();
                posicaoTermoBolsa.Query.Select(posicaoTermoBolsa.Query.DataVencimento.Max(),
                                               posicaoTermoBolsa.Query.DataOperacao.Max(),
                                               posicaoTermoBolsa.Query.PUTermo.Avg(),
                                               posicaoTermoBolsa.Query.ValorTermo.Sum(),
                                               posicaoTermoBolsa.Query.Quantidade.Sum(),
                                               posicaoTermoBolsa.Query.PUMercado.Avg(),
                                               posicaoTermoBolsa.Query.ValorMercado.Sum(),
                                               posicaoTermoBolsa.Query.PrazoDecorrer.Avg(),
                                               posicaoTermoBolsa.Query.PrazoTotal.Avg());
                posicaoTermoBolsa.Query.Where(posicaoTermoBolsa.Query.IdCliente.Equal(this.idCliente),
                                            posicaoTermoBolsa.Query.CdAtivoBolsa.Equal(cdAtivoBolsa));
                posicaoTermoBolsa.Query.Load();

                if (posicaoTermoBolsa.es.HasData && posicaoTermoBolsa.Quantidade.HasValue)
                {
                    string cdAtivoBolsaAcao = AtivoBolsa.RetornaCdAtivoBolsaAcao(cdAtivoBolsa);
                    string vencimento = cdAtivoBolsa.Replace(cdAtivoBolsaAcao, "").Replace("T", "");
                    vencimento = vencimento.Substring(0, 2) + "/" + vencimento.Substring(3, 2) + "/" + vencimento.Substring(4, 4);
                    string cdAtivoBolsaDescricao = cdAtivoBolsaAcao + "T - " + vencimento;

                    posicaoTermoBolsaSintetica.CdAtivoBolsa = cdAtivoBolsaDescricao;
                    posicaoTermoBolsaSintetica.DataVencimento = posicaoTermoBolsa.DataVencimento.Value;
                    posicaoTermoBolsaSintetica.DataOperacao = posicaoTermoBolsa.DataOperacao.Value;
                    posicaoTermoBolsaSintetica.PuTermo = posicaoTermoBolsa.PUTermo.Value;
                    posicaoTermoBolsaSintetica.ValorTermo = posicaoTermoBolsa.ValorTermo.Value;
                    posicaoTermoBolsaSintetica.Quantidade = posicaoTermoBolsa.Quantidade.Value;
                    posicaoTermoBolsaSintetica.PuMercado = posicaoTermoBolsa.PUMercado.Value;
                    posicaoTermoBolsaSintetica.ValorMercado = posicaoTermoBolsa.ValorMercado.Value;
                }
            }

            return posicaoTermoBolsaSintetica;
        }

        public PosicaoFuturoBMFSintetica RetornaPosicaoFuturoBMFSintetica(string cdAtivoBMFCompleto)
        {
            string cdAtivoBMF = cdAtivoBMFCompleto.Substring(0, 3);
            string serie = cdAtivoBMFCompleto.Replace(cdAtivoBMF, "");

            PosicaoFuturoBMFSintetica posicaoFuturoBMFSintetica = new PosicaoFuturoBMFSintetica();

            if (this.isPosicaoHistorica)
            {
                PosicaoBMFHistorico posicaoBMFHistorico = new PosicaoBMFHistorico();
                posicaoBMFHistorico.Query.Select(posicaoBMFHistorico.Query.Quantidade.Sum(),
                                                   posicaoBMFHistorico.Query.PUMercado.Avg(),
                                                   posicaoBMFHistorico.Query.DataVencimento.Max(),
                                                   posicaoBMFHistorico.Query.AjusteDiario.Sum());
                posicaoBMFHistorico.Query.Where(posicaoBMFHistorico.Query.IdCliente.Equal(this.idCliente),
                                                  posicaoBMFHistorico.Query.DataHistorico.Equal(this.dataReferencia),
                                                  posicaoBMFHistorico.Query.CdAtivoBMF.Equal(cdAtivoBMF),
                                                  posicaoBMFHistorico.Query.Serie.Equal(serie));
                posicaoBMFHistorico.Query.Load();

                if (posicaoBMFHistorico.es.HasData && posicaoBMFHistorico.Quantidade.HasValue)
                {
                    posicaoFuturoBMFSintetica.CdAtivoBMF = cdAtivoBMF;
                    posicaoFuturoBMFSintetica.Serie = serie;
                    posicaoFuturoBMFSintetica.Quantidade = posicaoBMFHistorico.Quantidade.Value;
                    posicaoFuturoBMFSintetica.PuMercado = posicaoBMFHistorico.PUMercado.Value;
                    posicaoFuturoBMFSintetica.DataVencimento = posicaoBMFHistorico.DataVencimento.Value;
                    posicaoFuturoBMFSintetica.AjusteDiario = posicaoBMFHistorico.AjusteDiario.Value;
                }
            }
            else
            {
                PosicaoBMF posicaoBMF = new PosicaoBMF();
                posicaoBMF.Query.Select(posicaoBMF.Query.Quantidade.Sum(),
                                                   posicaoBMF.Query.PUMercado.Avg(),
                                                   posicaoBMF.Query.DataVencimento.Max(),
                                                   posicaoBMF.Query.AjusteDiario.Sum());
                posicaoBMF.Query.Where(posicaoBMF.Query.IdCliente.Equal(this.idCliente),
                                                  posicaoBMF.Query.CdAtivoBMF.Equal(cdAtivoBMF),
                                                  posicaoBMF.Query.Serie.Equal(serie));
                posicaoBMF.Query.Load();

                if (posicaoBMF.es.HasData && posicaoBMF.Quantidade.HasValue)
                {
                    posicaoFuturoBMFSintetica.CdAtivoBMF = cdAtivoBMF;
                    posicaoFuturoBMFSintetica.Serie = serie;
                    posicaoFuturoBMFSintetica.Quantidade = posicaoBMF.Quantidade.Value;
                    posicaoFuturoBMFSintetica.PuMercado = posicaoBMF.PUMercado.Value;
                    posicaoFuturoBMFSintetica.DataVencimento = posicaoBMF.DataVencimento.Value;
                    posicaoFuturoBMFSintetica.AjusteDiario = posicaoBMF.AjusteDiario.Value;
                }
            }

            return posicaoFuturoBMFSintetica;
        }

        public PosicaoRendaFixaSintetica RetornaPosicaoRendaFixaSintetica(int idTitulo)
        {
            PosicaoRendaFixaSintetica posicaoRendaFixaSintetica = new PosicaoRendaFixaSintetica();

            string descricaoTitulo = "";
            if (this.isPosicaoHistorica)
            {
                PosicaoRendaFixaHistorico posicaoRendaFixaHistorico = new PosicaoRendaFixaHistorico();
                posicaoRendaFixaHistorico.Query.Select(posicaoRendaFixaHistorico.Query.DataVencimento.Max(),
                                                   posicaoRendaFixaHistorico.Query.DataOperacao.Max(),
                                                   posicaoRendaFixaHistorico.Query.PUOperacao.Avg(),
                                                   posicaoRendaFixaHistorico.Query.PUMercado.Avg(),
                                                   posicaoRendaFixaHistorico.Query.Quantidade.Sum(),
                                                   posicaoRendaFixaHistorico.Query.ValorIR.Sum(),
                                                   posicaoRendaFixaHistorico.Query.ValorIOF.Sum(),
                                                   posicaoRendaFixaHistorico.Query.ValorMercado.Sum(),
                                                   posicaoRendaFixaHistorico.Query.TaxaOperacao.Avg()
                                                   );
                posicaoRendaFixaHistorico.Query.Where(posicaoRendaFixaHistorico.Query.IdCliente.Equal(this.idCliente),
                                                  posicaoRendaFixaHistorico.Query.DataHistorico.Equal(this.dataReferencia),
                                                  posicaoRendaFixaHistorico.Query.IdTitulo.Equal(idTitulo));
                posicaoRendaFixaHistorico.Query.Load();

                if (posicaoRendaFixaHistorico.es.HasData && posicaoRendaFixaHistorico.Quantidade.HasValue)
                {
                    TituloRendaFixa tituloRendaFixa = new TituloRendaFixa();
                    List<esQueryItem> campos = new List<esQueryItem>();
                    campos.Add(tituloRendaFixa.Query.Descricao);
                    tituloRendaFixa.LoadByPrimaryKey(campos, idTitulo);
                    descricaoTitulo = tituloRendaFixa.Descricao;

                    posicaoRendaFixaSintetica.DescricaoTitulo = descricaoTitulo;
                    posicaoRendaFixaSintetica.DataVencimento = posicaoRendaFixaHistorico.DataVencimento.Value;
                    posicaoRendaFixaSintetica.DataOperacao = posicaoRendaFixaHistorico.DataOperacao.Value;
                    posicaoRendaFixaSintetica.PuOperacao = posicaoRendaFixaHistorico.PUOperacao.Value;
                    posicaoRendaFixaSintetica.PuMercado = posicaoRendaFixaHistorico.PUMercado.Value;
                    posicaoRendaFixaSintetica.Quantidade = posicaoRendaFixaHistorico.Quantidade.Value;
                    posicaoRendaFixaSintetica.TaxaOperacao = posicaoRendaFixaHistorico.TaxaOperacao.Value;

                    decimal valorMercado = posicaoRendaFixaHistorico.ValorMercado.Value;
                    decimal valorIR = posicaoRendaFixaHistorico.ValorIR.Value;
                    decimal valorIOF = posicaoRendaFixaHistorico.ValorIOF.Value;

                    posicaoRendaFixaSintetica.SaldoBruto = valorMercado;
                    posicaoRendaFixaSintetica.SaldoLiquido = valorMercado - valorIR - valorIOF;
                    posicaoRendaFixaSintetica.ValorIOF = valorIOF;
                    posicaoRendaFixaSintetica.ValorIR = valorIR;
                    posicaoRendaFixaSintetica.VlAplicAtualizado = Math.Round((posicaoRendaFixaSintetica.Quantidade * posicaoRendaFixaSintetica.PuOperacao), 2);
                }
            }
            else
            {
                PosicaoRendaFixa posicaoRendaFixa = new PosicaoRendaFixa();
                posicaoRendaFixa.Query.Select(posicaoRendaFixa.Query.DataVencimento.Max(),
                                               posicaoRendaFixa.Query.DataOperacao.Max(),
                                               posicaoRendaFixa.Query.PUOperacao.Avg(),
                                               posicaoRendaFixa.Query.PUMercado.Avg(),
                                               posicaoRendaFixa.Query.Quantidade.Sum(),
                                               posicaoRendaFixa.Query.ValorIR.Sum(),
                                               posicaoRendaFixa.Query.ValorIOF.Sum(),
                                               posicaoRendaFixa.Query.ValorMercado.Sum(),
                                               posicaoRendaFixa.Query.TaxaOperacao.Avg());
                posicaoRendaFixa.Query.Where(posicaoRendaFixa.Query.IdCliente.Equal(this.idCliente),
                                            posicaoRendaFixa.Query.IdTitulo.Equal(idTitulo));
                posicaoRendaFixa.Query.Load();

                if (posicaoRendaFixa.es.HasData && posicaoRendaFixa.Quantidade.HasValue)
                {
                    TituloRendaFixa tituloRendaFixa = new TituloRendaFixa();
                    List<esQueryItem> campos = new List<esQueryItem>();
                    campos.Add(tituloRendaFixa.Query.Descricao);
                    tituloRendaFixa.LoadByPrimaryKey(campos, idTitulo);
                    descricaoTitulo = tituloRendaFixa.Descricao;

                    posicaoRendaFixaSintetica.DescricaoTitulo = descricaoTitulo;
                    posicaoRendaFixaSintetica.DataVencimento = posicaoRendaFixa.DataVencimento.Value;
                    posicaoRendaFixaSintetica.DataOperacao = posicaoRendaFixa.DataOperacao.Value;
                    posicaoRendaFixaSintetica.PuOperacao = posicaoRendaFixa.PUOperacao.Value;
                    posicaoRendaFixaSintetica.PuMercado = posicaoRendaFixa.PUMercado.Value;
                    posicaoRendaFixaSintetica.Quantidade = posicaoRendaFixa.Quantidade.Value;
                    posicaoRendaFixaSintetica.TaxaOperacao = posicaoRendaFixa.TaxaOperacao.Value;

                    decimal valorMercado = posicaoRendaFixa.ValorMercado.Value;
                    decimal valorIR = posicaoRendaFixa.ValorIR.Value;
                    decimal valorIOF = posicaoRendaFixa.ValorIOF.Value;

                    posicaoRendaFixaSintetica.SaldoBruto = valorMercado;
                    posicaoRendaFixaSintetica.SaldoLiquido = valorMercado - valorIR - valorIOF;
                    posicaoRendaFixaSintetica.ValorIOF = valorIOF;
                    posicaoRendaFixaSintetica.ValorIR = valorIR;
                    posicaoRendaFixaSintetica.VlAplicAtualizado = Math.Round((posicaoRendaFixaSintetica.Quantidade * posicaoRendaFixaSintetica.PuOperacao), 2);
                }
            }

            return posicaoRendaFixaSintetica;
        }

        public PosicaoFundoSintetica RetornaPosicaoFundoSintetica(int idCarteira)
        {
            PosicaoFundoSintetica posicaoFundoSintetica = new PosicaoFundoSintetica();

            string nomeCarteira = "";
            if (this.isPosicaoHistorica)
            {
                PosicaoFundoHistorico posicaoFundoHistorico = new PosicaoFundoHistorico();
                posicaoFundoHistorico.Query.Select(posicaoFundoHistorico.Query.CotaDia.Avg(),
                                                   posicaoFundoHistorico.Query.CotaAplicacao.Avg(),
                                                   posicaoFundoHistorico.Query.Quantidade.Sum(),
                                                   posicaoFundoHistorico.Query.ValorBruto.Sum(),
                                                   posicaoFundoHistorico.Query.ValorIR.Sum(),
                                                   posicaoFundoHistorico.Query.ValorIOF.Sum(),
                                                   posicaoFundoHistorico.Query.ValorLiquido.Sum());
                posicaoFundoHistorico.Query.Where(posicaoFundoHistorico.Query.IdCliente.Equal(this.idCliente),
                                                  posicaoFundoHistorico.Query.DataHistorico.Equal(this.dataReferencia),
                                                  posicaoFundoHistorico.Query.IdCarteira.Equal(idCarteira),
                                                  posicaoFundoHistorico.Query.Quantidade.NotEqual(0));
                posicaoFundoHistorico.Query.Load();

                if (posicaoFundoHistorico.es.HasData && posicaoFundoHistorico.Quantidade.HasValue)
                {
                    Carteira carteira = new Carteira();
                    List<esQueryItem> campos = new List<esQueryItem>();
                    campos.Add(carteira.Query.Apelido);
                    carteira.LoadByPrimaryKey(campos, idCarteira);
                    nomeCarteira = carteira.Apelido;

                    posicaoFundoSintetica.NomeFundo = nomeCarteira;
                    posicaoFundoSintetica.Quantidade = posicaoFundoHistorico.Quantidade.Value;
                    posicaoFundoSintetica.SaldoBruto = posicaoFundoHistorico.ValorBruto.Value;
                    posicaoFundoSintetica.SaldoLiquido = posicaoFundoHistorico.ValorLiquido.Value;
                    posicaoFundoSintetica.ValorCota = posicaoFundoHistorico.CotaDia.Value;
                    posicaoFundoSintetica.ValorIOF = posicaoFundoHistorico.ValorIOF.Value;
                    posicaoFundoSintetica.ValorIR = posicaoFundoHistorico.ValorIR.Value;

                    decimal rendimento = Math.Round(posicaoFundoHistorico.Quantidade.Value * (posicaoFundoHistorico.CotaDia.Value - posicaoFundoHistorico.CotaAplicacao.Value), 2);
                    posicaoFundoSintetica.Rendimento = rendimento;
                }
            }
            else
            {
                PosicaoFundo posicaoFundo = new PosicaoFundo();
                posicaoFundo.Query.Select(posicaoFundo.Query.CotaDia.Avg(),
                                           posicaoFundo.Query.CotaAplicacao.Avg(),
                                           posicaoFundo.Query.Quantidade.Sum(),
                                           posicaoFundo.Query.ValorBruto.Sum(),
                                           posicaoFundo.Query.ValorIR.Sum(),
                                           posicaoFundo.Query.ValorIOF.Sum(),
                                           posicaoFundo.Query.ValorLiquido.Sum());
                posicaoFundo.Query.Where(posicaoFundo.Query.IdCliente.Equal(this.idCliente),
                                         posicaoFundo.Query.IdCarteira.Equal(idCarteira),
                                         posicaoFundo.Query.Quantidade.NotEqual(0));
                posicaoFundo.Query.Load();

                if (posicaoFundo.es.HasData && posicaoFundo.Quantidade.HasValue)
                {
                    Carteira carteira = new Carteira();
                    List<esQueryItem> campos = new List<esQueryItem>();
                    campos.Add(carteira.Query.Apelido);
                    carteira.LoadByPrimaryKey(campos, idCarteira);
                    nomeCarteira = carteira.Apelido;

                    posicaoFundoSintetica.NomeFundo = nomeCarteira;
                    posicaoFundoSintetica.Quantidade = posicaoFundo.Quantidade.Value;
                    posicaoFundoSintetica.SaldoBruto = posicaoFundo.ValorBruto.Value;
                    posicaoFundoSintetica.SaldoLiquido = posicaoFundo.ValorLiquido.Value;
                    posicaoFundoSintetica.ValorCota = posicaoFundo.CotaDia.Value;
                    posicaoFundoSintetica.ValorIOF = posicaoFundo.ValorIOF.Value;
                    posicaoFundoSintetica.ValorIR = posicaoFundo.ValorIR.Value;

                    decimal rendimento = Math.Round(posicaoFundo.Quantidade.Value * (posicaoFundo.CotaDia.Value - posicaoFundo.CotaAplicacao.Value), 2);
                    posicaoFundoSintetica.Rendimento = rendimento;
                }
            }

            return posicaoFundoSintetica;
        }

        public List<PosicaoFundoAnalitica> RetornaPosicaoFundoAnalitica(int idCarteira)
        {
            List<PosicaoFundoAnalitica> listaPosicaoFundoAnalitica = new List<PosicaoFundoAnalitica>();

            if (this.isPosicaoHistorica)
            {
                PosicaoFundoHistoricoCollection posicaoFundoHistoricoCollection = new PosicaoFundoHistoricoCollection();
                posicaoFundoHistoricoCollection.Query.Select(posicaoFundoHistoricoCollection.Query.IdPosicao,
                                                   posicaoFundoHistoricoCollection.Query.CotaAplicacao,
                                                   posicaoFundoHistoricoCollection.Query.DataAplicacao,
                                                   posicaoFundoHistoricoCollection.Query.DataConversao,
                                                   posicaoFundoHistoricoCollection.Query.Quantidade,
                                                   posicaoFundoHistoricoCollection.Query.ValorAplicacao,
                                                   posicaoFundoHistoricoCollection.Query.ValorBruto,
                                                   posicaoFundoHistoricoCollection.Query.ValorIOF,
                                                   posicaoFundoHistoricoCollection.Query.ValorIR,
                                                   posicaoFundoHistoricoCollection.Query.ValorLiquido,
                                                   posicaoFundoHistoricoCollection.Query.CotaDia);
                posicaoFundoHistoricoCollection.Query.Where(posicaoFundoHistoricoCollection.Query.IdCliente.Equal(this.idCliente),
                                                  posicaoFundoHistoricoCollection.Query.DataHistorico.Equal(this.dataReferencia),
                                                  posicaoFundoHistoricoCollection.Query.IdCarteira.Equal(idCarteira),
                                                  posicaoFundoHistoricoCollection.Query.Quantidade.NotEqual(0));
                posicaoFundoHistoricoCollection.Query.Load();

                if (posicaoFundoHistoricoCollection.HasData)
                {
                    foreach (PosicaoFundoHistorico posicaoFundoHistorico in posicaoFundoHistoricoCollection)
                    {
                        PosicaoFundoAnalitica posicaoFundoAnalitica = new PosicaoFundoAnalitica();
                        posicaoFundoAnalitica.IdPosicao = posicaoFundoHistorico.IdPosicao.Value;
                        posicaoFundoAnalitica.CotaAplicacao = posicaoFundoHistorico.CotaAplicacao.Value;
                        posicaoFundoAnalitica.DataAplicacao = posicaoFundoHistorico.DataAplicacao.Value;
                        posicaoFundoAnalitica.Quantidade = posicaoFundoHistorico.Quantidade.Value;
                        posicaoFundoAnalitica.ValorAplicado = posicaoFundoHistorico.ValorAplicacao.Value;
                        posicaoFundoAnalitica.SaldoBruto = posicaoFundoHistorico.ValorBruto.Value;
                        posicaoFundoAnalitica.SaldoLiquido = posicaoFundoHistorico.ValorLiquido.Value;
                        posicaoFundoAnalitica.ValorIOF = posicaoFundoHistorico.ValorIOF.Value;
                        posicaoFundoAnalitica.ValorIR = posicaoFundoHistorico.ValorIR.Value;

                        decimal rendimento = Math.Round(posicaoFundoHistorico.Quantidade.Value * (posicaoFundoHistorico.CotaDia.Value - posicaoFundoHistorico.CotaAplicacao.Value), 2);
                        posicaoFundoAnalitica.Rendimento = rendimento;

                        listaPosicaoFundoAnalitica.Add(posicaoFundoAnalitica);
                    }
                }
            }
            else
            {
                PosicaoFundoCollection posicaoFundoCollection = new PosicaoFundoCollection();
                posicaoFundoCollection.Query.Select(posicaoFundoCollection.Query.IdPosicao,
                                                   posicaoFundoCollection.Query.CotaAplicacao,
                                                   posicaoFundoCollection.Query.DataAplicacao,
                                                   posicaoFundoCollection.Query.DataConversao,
                                                   posicaoFundoCollection.Query.Quantidade,
                                                   posicaoFundoCollection.Query.ValorAplicacao,
                                                   posicaoFundoCollection.Query.ValorBruto,
                                                   posicaoFundoCollection.Query.ValorIOF,
                                                   posicaoFundoCollection.Query.ValorIR,
                                                   posicaoFundoCollection.Query.ValorLiquido,
                                                   posicaoFundoCollection.Query.CotaDia);
                posicaoFundoCollection.Query.Where(posicaoFundoCollection.Query.IdCliente.Equal(this.idCliente),
                                                   posicaoFundoCollection.Query.IdCarteira.Equal(idCarteira),
                                                   posicaoFundoCollection.Query.Quantidade.NotEqual(0));
                posicaoFundoCollection.Query.Load();

                if (posicaoFundoCollection.HasData)
                {
                    foreach (PosicaoFundo posicaoFundo in posicaoFundoCollection)
                    {
                        PosicaoFundoAnalitica posicaoFundoAnalitica = new PosicaoFundoAnalitica();
                        posicaoFundoAnalitica.IdPosicao = posicaoFundo.IdPosicao.Value;
                        posicaoFundoAnalitica.CotaAplicacao = posicaoFundo.CotaAplicacao.Value;
                        posicaoFundoAnalitica.DataAplicacao = posicaoFundo.DataAplicacao.Value;
                        posicaoFundoAnalitica.Quantidade = posicaoFundo.Quantidade.Value;
                        posicaoFundoAnalitica.ValorAplicado = posicaoFundo.ValorAplicacao.Value;
                        posicaoFundoAnalitica.SaldoBruto = posicaoFundo.ValorBruto.Value;
                        posicaoFundoAnalitica.SaldoLiquido = posicaoFundo.ValorLiquido.Value;
                        posicaoFundoAnalitica.ValorIOF = posicaoFundo.ValorIOF.Value;
                        posicaoFundoAnalitica.ValorIR = posicaoFundo.ValorIR.Value;

                        decimal rendimento = Math.Round(posicaoFundo.Quantidade.Value * (posicaoFundo.CotaDia.Value - posicaoFundo.CotaAplicacao.Value), 2);
                        posicaoFundoAnalitica.Rendimento = rendimento;

                        listaPosicaoFundoAnalitica.Add(posicaoFundoAnalitica);
                    }
                }
            }

            return listaPosicaoFundoAnalitica;
        }
    }

    public class TableValuesOperacaoMercado
    {
        #region Atributos e properties
        int idCliente;
        public int IdCliente
        {
            get { return idCliente; }
            set { idCliente = value; }
        }

        DateTime dataInicio;
        public DateTime DataInicio
        {
            get { return dataInicio; }
            set { dataInicio = value; }
        }

        DateTime dataFim;
        public DateTime DataFim
        {
            get { return dataFim; }
            set { dataFim = value; }
        }

        DateTime dataDia;
        public DateTime DataDia
        {
            get { return dataDia; }
            set { dataDia = value; }
        }

        bool isPosicaoHistorica;
        public bool IsPosicaoHistorica
        {
            get { return isPosicaoHistorica; }
            set { isPosicaoHistorica = value; }
        }
        #endregion

        public TableValuesOperacaoMercado(int idCliente, DateTime dataInicio, DateTime dataFim)
        {
            this.idCliente = idCliente;
            this.dataInicio = dataInicio;
            this.dataFim = dataFim;

            Cliente cliente = new Cliente();
            List<esQueryItem> campos = new List<esQueryItem>();
            campos.Add(cliente.Query.DataDia);
            cliente.LoadByPrimaryKey(campos, idCliente);
            DateTime dataDia = cliente.DataDia.Value;

            this.dataDia = dataDia;

            if (DateTime.Compare(dataInicio, dataDia) >= 0)
            {
                this.isPosicaoHistorica = false;
            }
            else
            {
                this.isPosicaoHistorica = true;
            }
        }

        public class OperacaoBolsaSintetica
        {
            DateTime data;
            public DateTime Data
            {
                get { return data; }
                set { data = value; }
            }

            decimal quantidadeCompra;
            public decimal QuantidadeCompra
            {
                get { return quantidadeCompra; }
                set { quantidadeCompra = value; }
            }

            decimal precoCompra;
            public decimal PrecoCompra
            {
                get { return precoCompra; }
                set { precoCompra = value; }
            }

            decimal valorCompra;
            public decimal ValorCompra
            {
                get { return valorCompra; }
                set { valorCompra = value; }
            }

            decimal quantidadeVenda;
            public decimal QuantidadeVenda
            {
                get { return quantidadeVenda; }
                set { quantidadeVenda = value; }
            }

            decimal precoVenda;
            public decimal PrecoVenda
            {
                get { return precoVenda; }
                set { precoVenda = value; }
            }

            decimal valorVenda;
            public decimal ValorVenda
            {
                get { return valorVenda; }
                set { valorVenda = value; }
            }

            decimal quantidadePosicao;
            public decimal QuantidadePosicao
            {
                get { return quantidadePosicao; }
                set { quantidadePosicao = value; }
            }

            decimal precoPosicao;
            public decimal PrecoPosicao
            {
                get { return precoPosicao; }
                set { precoPosicao = value; }
            }

            decimal valorPosicao;
            public decimal ValorPosicao
            {
                get { return valorPosicao; }
                set { valorPosicao = value; }
            }

            //Constructor
            public OperacaoBolsaSintetica()
            {
                this.quantidadeCompra = 0;
                this.precoCompra = 0;
                this.valorCompra = 0;
                this.quantidadeVenda = 0;
                this.precoVenda = 0;
                this.valorVenda = 0;
                this.quantidadePosicao = 0;
                this.precoPosicao = 0;
                this.valorPosicao = 0;
            }
            //
        }

        public class OperacaoBolsaAnalitica
        {
            DateTime data;
            public DateTime Data
            {
                get { return data; }
                set { data = value; }
            }

            string tipoOperacao;
            public string TipoOperacao
            {
                get { return tipoOperacao; }
                set { tipoOperacao = value; }
            }

            decimal quantidade;
            public decimal Quantidade
            {
                get { return quantidade; }
                set { quantidade = value; }
            }

            decimal preco;
            public decimal Preco
            {
                get { return preco; }
                set { preco = value; }
            }

            decimal valor;
            public decimal Valor
            {
                get { return valor; }
                set { valor = value; }
            }
        }

        public class OperacaoBMFAnalitica
        {
            DateTime data;
            public DateTime Data
            {
                get { return data; }
                set { data = value; }
            }

            string tipoOperacao;
            public string TipoOperacao
            {
                get { return tipoOperacao; }
                set { tipoOperacao = value; }
            }

            decimal quantidade;
            public decimal Quantidade
            {
                get { return quantidade; }
                set { quantidade = value; }
            }

            decimal preco;
            public decimal Preco
            {
                get { return preco; }
                set { preco = value; }
            }

            decimal valor;
            public decimal Valor
            {
                get { return valor; }
                set { valor = value; }
            }
        }

        public class OperacaoRendaFixaAnalitica
        {
            DateTime data;
            public DateTime Data
            {
                get { return data; }
                set { data = value; }
            }

            byte tipoOperacao;
            public byte TipoOperacao
            {
                get { return tipoOperacao; }
                set { tipoOperacao = value; }
            }

            decimal puOperacao;
            public decimal PuOperacao
            {
                get { return puOperacao; }
                set { puOperacao = value; }
            }

            decimal quantidade;
            public decimal Quantidade
            {
                get { return quantidade; }
                set { quantidade = value; }
            }

            decimal valorBruto;
            public decimal ValorBruto
            {
                get { return valorBruto; }
                set { valorBruto = value; }
            }

            decimal valorIR;
            public decimal ValorIR
            {
                get { return valorIR; }
                set { valorIR = value; }
            }

            decimal valorIOF;
            public decimal ValorIOF
            {
                get { return valorIOF; }
                set { valorIOF = value; }
            }

            decimal valorLiquido;
            public decimal ValorLiquido
            {
                get { return valorLiquido; }
                set { valorLiquido = value; }
            }
        }

        public class OperacaoResgateFundo
        {
            DateTime dataResgate;
            public DateTime DataResgate
            {
                get { return dataResgate; }
                set { dataResgate = value; }
            }

            decimal cotaResgate;
            public decimal CotaResgate
            {
                get { return cotaResgate; }
                set { cotaResgate = value; }
            }

            decimal quantidade;
            public decimal Quantidade
            {
                get { return quantidade; }
                set { quantidade = value; }
            }

            decimal valorBruto;
            public decimal ValorBruto
            {
                get { return valorBruto; }
                set { valorBruto = value; }
            }
        }

        public class ValoresLiquidar
        {
            DateTime dataLancamento;
            public DateTime DataLancamento
            {
                get { return dataLancamento; }
                set { dataLancamento = value; }
            }

            DateTime dataVencimento;
            public DateTime DataVencimento
            {
                get { return dataVencimento; }
                set { dataVencimento = value; }
            }

            string descricao;
            public string Descricao
            {
                get { return descricao; }
                set { descricao = value; }
            }

            decimal valor;
            public decimal Valor
            {
                get { return valor; }
                set { valor = value; }
            }
        }

        public List<OperacaoBolsaSintetica> RetornaOperacaoBolsaSintetica(string cdAtivoBolsa)
        {
            decimal fator = 1;
            FatorCotacaoBolsa fatorCotacaoBolsa = new FatorCotacaoBolsa();
            if (fatorCotacaoBolsa.BuscaFatorCotacaoBolsa(cdAtivoBolsa, this.dataFim))
            {
                fator = fatorCotacaoBolsa.Fator.Value;
            }

            List<OperacaoBolsaSintetica> listaOperacaoBolsa = new List<OperacaoBolsaSintetica>();

            DateTime dataAux = this.dataInicio;
            bool acabou = false;
            while (!acabou)
            {
                if (dataAux > this.dataFim)
                {
                    acabou = true;
                }

                OperacaoBolsaSintetica operacaoBolsaSintetica = new OperacaoBolsaSintetica();

                OperacaoBolsa operacaoBolsaCompra = new OperacaoBolsa();
                operacaoBolsaCompra.Query.Select(operacaoBolsaCompra.Query.Quantidade.Sum(),
                                                 operacaoBolsaCompra.Query.Valor.Sum());
                operacaoBolsaCompra.Query.Where(operacaoBolsaCompra.Query.IdCliente.Equal(this.idCliente),
                                                operacaoBolsaCompra.Query.Data.Equal(dataAux),
                                                operacaoBolsaCompra.Query.CdAtivoBolsa.Equal(cdAtivoBolsa),
                                                operacaoBolsaCompra.Query.TipoOperacao.In(TipoOperacaoBolsa.Compra, TipoOperacaoBolsa.CompraDaytrade),
                                                operacaoBolsaCompra.Query.Quantidade.NotEqual(0));
                operacaoBolsaCompra.Query.Load();

                if (operacaoBolsaCompra.es.HasData && operacaoBolsaCompra.Quantidade.HasValue)
                {
                    decimal quantidade = operacaoBolsaCompra.Quantidade.Value;
                    decimal valor = operacaoBolsaCompra.Valor.Value;
                    decimal pu = (valor / quantidade) * fator;

                    operacaoBolsaSintetica.Data = dataAux;
                    operacaoBolsaSintetica.PrecoCompra = pu;
                    operacaoBolsaSintetica.QuantidadeCompra = quantidade;
                    operacaoBolsaSintetica.ValorCompra = valor;
                }

                OperacaoBolsa operacaoBolsaVenda = new OperacaoBolsa();
                operacaoBolsaVenda.Query.Select(operacaoBolsaVenda.Query.Quantidade.Sum(),
                                                 operacaoBolsaVenda.Query.Valor.Sum());
                operacaoBolsaVenda.Query.Where(operacaoBolsaVenda.Query.IdCliente.Equal(this.idCliente),
                                                operacaoBolsaVenda.Query.Data.Equal(dataAux),
                                                operacaoBolsaVenda.Query.CdAtivoBolsa.Equal(cdAtivoBolsa),
                                                operacaoBolsaVenda.Query.TipoOperacao.In(TipoOperacaoBolsa.Venda, TipoOperacaoBolsa.VendaDaytrade),
                                                operacaoBolsaVenda.Query.Quantidade.NotEqual(0));
                operacaoBolsaVenda.Query.Load();

                if (operacaoBolsaVenda.es.HasData && operacaoBolsaVenda.Quantidade.HasValue)
                {
                    decimal quantidade = operacaoBolsaVenda.Quantidade.Value;
                    decimal valor = operacaoBolsaVenda.Valor.Value;
                    decimal pu = (valor / quantidade) * fator;

                    operacaoBolsaSintetica.Data = dataAux;
                    operacaoBolsaSintetica.PrecoVenda = pu;
                    operacaoBolsaSintetica.QuantidadeVenda = quantidade;
                    operacaoBolsaSintetica.ValorVenda = valor;
                }

                if ((operacaoBolsaCompra.es.HasData && operacaoBolsaCompra.Quantidade.HasValue) ||
                        (operacaoBolsaVenda.es.HasData && operacaoBolsaVenda.Quantidade.HasValue))
                {
                    if (DateTime.Compare(this.dataDia, dataAux) > 0)
                    {
                        PosicaoBolsaHistorico posicaoBolsaHistorico = new PosicaoBolsaHistorico();
                        posicaoBolsaHistorico.Query.Select(posicaoBolsaHistorico.Query.Quantidade.Sum(),
                                                           posicaoBolsaHistorico.Query.ValorMercado.Sum());
                        posicaoBolsaHistorico.Query.Where(posicaoBolsaHistorico.Query.IdCliente.Equal(this.idCliente),
                                                          posicaoBolsaHistorico.Query.CdAtivoBolsa.Equal(cdAtivoBolsa),
                                                          posicaoBolsaHistorico.Query.DataHistorico.Equal(dataAux),
                                                          posicaoBolsaHistorico.Query.Quantidade.NotEqual(0));
                        posicaoBolsaHistorico.Query.Load();

                        if (posicaoBolsaHistorico.es.HasData && posicaoBolsaHistorico.Quantidade.HasValue)
                        {
                            decimal quantidadePosicao = posicaoBolsaHistorico.Quantidade.Value;
                            decimal valorPosicao = posicaoBolsaHistorico.ValorMercado.Value;
                            decimal puPosicao = 0;

                            if (quantidadePosicao != 0)
                            {
                                puPosicao = (valorPosicao / quantidadePosicao) * fator;
                            }

                            operacaoBolsaSintetica.QuantidadePosicao = quantidadePosicao;
                            operacaoBolsaSintetica.PrecoPosicao = puPosicao;
                            operacaoBolsaSintetica.QuantidadePosicao = quantidadePosicao;
                        }
                    }
                    else
                    {
                        PosicaoBolsa posicaoBolsa = new PosicaoBolsa();
                        posicaoBolsa.Query.Select(posicaoBolsa.Query.Quantidade.Sum(),
                                                posicaoBolsa.Query.ValorMercado.Sum());
                        posicaoBolsa.Query.Where(posicaoBolsa.Query.IdCliente.Equal(this.idCliente),
                                              posicaoBolsa.Query.CdAtivoBolsa.Equal(cdAtivoBolsa));
                        posicaoBolsa.Query.Load();

                        if (posicaoBolsa.es.HasData && posicaoBolsa.Quantidade.HasValue)
                        {
                            decimal quantidadePosicao = posicaoBolsa.Quantidade.Value;
                            decimal valorPosicao = posicaoBolsa.ValorMercado.Value;
                            decimal puPosicao = (valorPosicao / quantidadePosicao) * fator;

                            operacaoBolsaSintetica.QuantidadePosicao = quantidadePosicao;
                            operacaoBolsaSintetica.PrecoPosicao = puPosicao;
                            operacaoBolsaSintetica.QuantidadePosicao = quantidadePosicao;
                        }
                    }

                    listaOperacaoBolsa.Add(operacaoBolsaSintetica);
                }

                dataAux = Calendario.AdicionaDiaUtil(dataAux, 1, LocalFeriadoFixo.Bovespa, TipoFeriado.Brasil);
            }

            return listaOperacaoBolsa;
        }

        public List<OperacaoBolsaAnalitica> RetornaOperacaoBolsaAnalitica(string cdAtivoBolsa)
        {
            List<OperacaoBolsaAnalitica> listaOperacaoBolsa = new List<OperacaoBolsaAnalitica>();

            OperacaoBolsaCollection operacaoBolsaCollection = new OperacaoBolsaCollection();
            operacaoBolsaCollection.Query.Select(operacaoBolsaCollection.Query.Data,
                                                 operacaoBolsaCollection.Query.TipoOperacao,
                                                 operacaoBolsaCollection.Query.Quantidade,
                                                 operacaoBolsaCollection.Query.Pu,
                                                 operacaoBolsaCollection.Query.Valor);
            operacaoBolsaCollection.Query.Where(operacaoBolsaCollection.Query.IdCliente.Equal(this.idCliente),
                                                operacaoBolsaCollection.Query.CdAtivoBolsa.Equal(cdAtivoBolsa),
                                                operacaoBolsaCollection.Query.Data.GreaterThanOrEqual(this.dataInicio),
                                                operacaoBolsaCollection.Query.Data.LessThanOrEqual(this.dataFim));
            operacaoBolsaCollection.Query.Load();

            foreach (OperacaoBolsa operacaoBolsa in operacaoBolsaCollection)
            {
                OperacaoBolsaAnalitica operacaoBolsaAnalitica = new OperacaoBolsaAnalitica();
                operacaoBolsaAnalitica.Data = operacaoBolsa.Data.Value;
                operacaoBolsaAnalitica.Preco = operacaoBolsa.Pu.Value;
                operacaoBolsaAnalitica.Quantidade = operacaoBolsa.Quantidade.Value;
                operacaoBolsaAnalitica.TipoOperacao = operacaoBolsa.TipoOperacao;
                operacaoBolsaAnalitica.Valor = operacaoBolsa.Valor.Value;

                listaOperacaoBolsa.Add(operacaoBolsaAnalitica);
            }

            return listaOperacaoBolsa;
        }

        public List<OperacaoBMFAnalitica> RetornaOperacaoBMFAnalitica(string cdAtivoBMFCompleto)
        {
            string cdAtivoBMF = cdAtivoBMFCompleto.Substring(0, 3);
            string serie = cdAtivoBMFCompleto.Replace(cdAtivoBMF, "");

            List<OperacaoBMFAnalitica> listaOperacaoBMF = new List<OperacaoBMFAnalitica>();

            OperacaoBMFCollection operacaoBMFCollection = new OperacaoBMFCollection();
            operacaoBMFCollection.Query.Select(operacaoBMFCollection.Query.Data,
                                                 operacaoBMFCollection.Query.TipoOperacao,
                                                 operacaoBMFCollection.Query.Quantidade,
                                                 operacaoBMFCollection.Query.Pu,
                                                 operacaoBMFCollection.Query.Valor);
            operacaoBMFCollection.Query.Where(operacaoBMFCollection.Query.IdCliente.Equal(this.idCliente),
                                                operacaoBMFCollection.Query.CdAtivoBMF.Equal(cdAtivoBMF),
                                                operacaoBMFCollection.Query.Serie.Equal(serie),
                                                operacaoBMFCollection.Query.Data.GreaterThanOrEqual(this.dataInicio),
                                                operacaoBMFCollection.Query.Data.LessThanOrEqual(this.dataFim));
            operacaoBMFCollection.Query.Load();

            foreach (OperacaoBMF operacaoBMF in operacaoBMFCollection)
            {
                OperacaoBMFAnalitica operacaoBMFAnalitica = new OperacaoBMFAnalitica();
                operacaoBMFAnalitica.Data = operacaoBMF.Data.Value;
                operacaoBMFAnalitica.Preco = operacaoBMF.Pu.Value;
                operacaoBMFAnalitica.Quantidade = operacaoBMF.Quantidade.Value;
                operacaoBMFAnalitica.TipoOperacao = operacaoBMF.TipoOperacao;
                operacaoBMFAnalitica.Valor = operacaoBMF.Valor.Value;

                listaOperacaoBMF.Add(operacaoBMFAnalitica);
            }

            return listaOperacaoBMF;
        }

        public List<OperacaoRendaFixaAnalitica> RetornaOperacaoRendaFixaAnalitica(int idTitulo)
        {
            List<OperacaoRendaFixaAnalitica> listaOperacaoRendaFixa = new List<OperacaoRendaFixaAnalitica>();

            OperacaoRendaFixaCollection operacaoRendaFixaCollection = new OperacaoRendaFixaCollection();
            operacaoRendaFixaCollection.Query.Select(operacaoRendaFixaCollection.Query.DataOperacao,
                                                     operacaoRendaFixaCollection.Query.TipoOperacao,
                                                     operacaoRendaFixaCollection.Query.PUOperacao,
                                                     operacaoRendaFixaCollection.Query.Quantidade,
                                                     operacaoRendaFixaCollection.Query.Valor,
                                                     operacaoRendaFixaCollection.Query.ValorIR,
                                                     operacaoRendaFixaCollection.Query.ValorIOF);
            operacaoRendaFixaCollection.Query.Where(operacaoRendaFixaCollection.Query.IdCliente.Equal(this.idCliente),
                                                    operacaoRendaFixaCollection.Query.IdTitulo.Equal(idTitulo),
                                                    operacaoRendaFixaCollection.Query.DataOperacao.GreaterThanOrEqual(dataInicio),
                                                    operacaoRendaFixaCollection.Query.DataOperacao.LessThanOrEqual(dataFim));
            operacaoRendaFixaCollection.Query.Load();

            foreach (OperacaoRendaFixa operacaoRendaFixa in operacaoRendaFixaCollection)
            {
                OperacaoRendaFixaAnalitica operacaoRendaFixaAnalitica = new OperacaoRendaFixaAnalitica();
                operacaoRendaFixaAnalitica.Data = operacaoRendaFixa.DataOperacao.Value;
                operacaoRendaFixaAnalitica.PuOperacao = operacaoRendaFixa.PUOperacao.Value;
                operacaoRendaFixaAnalitica.Quantidade = operacaoRendaFixa.Quantidade.Value;
                operacaoRendaFixaAnalitica.TipoOperacao = operacaoRendaFixa.TipoOperacao.Value;
                operacaoRendaFixaAnalitica.ValorBruto = operacaoRendaFixa.Valor.Value;
                operacaoRendaFixaAnalitica.ValorIOF = operacaoRendaFixa.ValorIOF.Value;
                operacaoRendaFixaAnalitica.ValorIR = operacaoRendaFixa.ValorIR.Value;
                decimal valorLiquido = operacaoRendaFixa.Valor.Value - operacaoRendaFixa.ValorIR.Value - operacaoRendaFixa.ValorIOF.Value;
                operacaoRendaFixaAnalitica.ValorLiquido = valorLiquido;

                listaOperacaoRendaFixa.Add(operacaoRendaFixaAnalitica);
            }

            return listaOperacaoRendaFixa;
        }

        public List<OperacaoResgateFundo> RetornaOperacaoResgateFundo(int idPosicao)
        {
            List<OperacaoResgateFundo> listaOperacaoResgateFundo = new List<OperacaoResgateFundo>();

            //Só vai ser implementado qdo existir a tabela DetalheResgateFundo

            return listaOperacaoResgateFundo;
        }

        public List<ValoresLiquidar> RetornaValoresLiquidar()
        {
            List<ValoresLiquidar> listaValoresLiquidar = new List<ValoresLiquidar>();

            if (this.isPosicaoHistorica)
            {
                LiquidacaoHistoricoCollection liquidacaoHistoricoCollection = new LiquidacaoHistoricoCollection();
                liquidacaoHistoricoCollection.BuscaLiquidacaoVencimentoMaior(this.idCliente, this.dataInicio); //Assume datainicio = datafim

                foreach (LiquidacaoHistorico liquidacaoHistorico in liquidacaoHistoricoCollection)
                {
                    ValoresLiquidar valoresLiquidar = new ValoresLiquidar();
                    valoresLiquidar.DataLancamento = liquidacaoHistorico.DataLancamento.Value;
                    valoresLiquidar.DataVencimento = liquidacaoHistorico.DataVencimento.Value;
                    valoresLiquidar.Descricao = liquidacaoHistorico.Descricao;
                    valoresLiquidar.Valor = liquidacaoHistorico.Valor.Value;

                    listaValoresLiquidar.Add(valoresLiquidar);
                }
            }
            else
            {
                LiquidacaoCollection liquidacaoCollection = new LiquidacaoCollection();
                liquidacaoCollection.BuscaLiquidacaoVencimentoMaior(this.idCliente, this.dataInicio, this.dataFim);

                foreach (Liquidacao liquidacao in liquidacaoCollection)
                {
                    ValoresLiquidar valoresLiquidar = new ValoresLiquidar();
                    valoresLiquidar.DataLancamento = liquidacao.DataLancamento.Value;
                    valoresLiquidar.DataVencimento = liquidacao.DataVencimento.Value;
                    valoresLiquidar.Descricao = liquidacao.Descricao;
                    valoresLiquidar.Valor = liquidacao.Valor.Value;

                    listaValoresLiquidar.Add(valoresLiquidar);
                }
            }

            return listaValoresLiquidar;
        }


    }

}