﻿using System;
using Financial.Util;
using System.Collections.Generic;
using Financial.Investidor;
using Financial.Bolsa;
using Financial.RendaFixa;
using Financial.RendaFixa.Enums;
using Financial.ContaCorrente;
using Financial.Fundo;
using Financial.Common;
using EntitySpaces.Interfaces;
using Financial.Common.Enums;
using Financial.Investidor.Enums;
using System.Globalization;

namespace FinancialDesk.BO.Series
{
    public class MultipleSerieValues
    {
        #region Atributos e properties
        private int idCliente;

        public int IdCliente
        {
            get { return idCliente; }
            set { idCliente = value; }
        }

        private string apelido;

        public string Apelido
        {
            get { return apelido; }
            set { apelido = value; }
        }

        private short idBenchmark;

        public short IdBenchmark
        {
            get { return idBenchmark; }
            set { idBenchmark = value; }
        }

        private DateTime data;

        public DateTime Data
        {
            get { return data; }
            set { data = value; }
        }

        private DateTime dataInicioCota;

        public DateTime DataInicioCota
        {
            get { return dataInicioCota; }
            set { dataInicioCota = value; }
        }

        private Chart chart;

        public Chart Chart
        {
            get { return chart; }
            set { chart = value; }
        }
        #endregion

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="data"></param>
        public MultipleSerieValues(int idCliente, DateTime data, short idBenchmark)
        {
            this.idCliente = idCliente;

            //Seta a dataAtual do Cliente
            Cliente cliente = new Cliente();
            List<esQueryItem> campos = new List<esQueryItem>();
            campos.Add(cliente.Query.DataDia);
            campos.Add(cliente.Query.Status);
            campos.Add(cliente.Query.Apelido);
            cliente.LoadByPrimaryKey(campos, idCliente);

            if (cliente.DataDia.Value > data)
            {
                this.data = data;
            }
            else
            {
                //Se a carteira não foi calculada na data ou está com último cálculo não ok, pega a data anterior
                if (cliente.Status.Value == (byte)StatusCliente.Fechado || cliente.Status.Value == (byte)StatusCliente.Divulgado)
                {
                    this.data = cliente.DataDia.Value;
                }
                else
                {
                    this.data = Calendario.SubtraiDiaUtil(cliente.DataDia.Value, 1);
                }
            }


            this.apelido = cliente.Apelido;
            this.idBenchmark = idBenchmark;

            Carteira carteira = new Carteira();
            campos = new List<esQueryItem>();
            campos.Add(carteira.Query.DataInicioCota);
            carteira.LoadByPrimaryKey(campos, idCliente);

            this.dataInicioCota = carteira.DataInicioCota.Value;

            if (data < this.dataInicioCota)
            {
                this.dataInicioCota = data;
            }

            this.Chart = new Chart();
        }

        #region Classe Interna que Contem o IdCarteira E os Dados de Rentabilidade da Carteira
        protected class DadosCarteira
        {
            //
            public int idCarteira;
            public string nomeFundo;

            /// <summary>
            /// Dicionario onde a chave é a Data Do Fundo e o Valor é a Rentabilidade na Data
            /// </summary>
            public Dictionary<DateTime, decimal> dadosRentabilidadeFundo = new Dictionary<DateTime, decimal>();
        }
        //
        private DadosCarteira[] dadosCarteira;
        #endregion

        #region Classe Interna que Contem o IdIndice E os Dados de Rentabilidade do Indice
        protected class DadosIndice
        {
            public int idIndice;
            public string nomeIndice;

            /// <summary>
            /// Dicionario onde a chave é a Data Do Fundo e o Valor é a Rentabilidade na Data
            /// </summary>
            public Dictionary<DateTime, decimal> dadosRentabilidadeIndice = new Dictionary<DateTime, decimal>();
        }
        //
        private DadosIndice[] dadosIndice;
        #endregion

        private void InitCalculoMedidaParaRetornoAcumulado(CalculoMedida calculoMedida)
        {
            // Inicializa a classe De Dados de Carteira
            this.dadosCarteira = new DadosCarteira[1];

            int idCarteira = calculoMedida.carteira.IdCarteira.Value;

            this.dadosCarteira[0] = new DadosCarteira();
            this.dadosCarteira[0].idCarteira = idCarteira;

            Carteira carteira = new Carteira();
            List<esQueryItem> campos = new List<esQueryItem>();
            campos.Add(carteira.Query.Apelido);
            carteira.LoadByPrimaryKey(campos, idCarteira);
            //
            this.dadosCarteira[0].nomeFundo = carteira.Apelido.Trim();
            //

            int idIndice = calculoMedida.indice.IdIndice.Value;

            // Inicializa a classe De Dados de Indice
            this.dadosIndice = new DadosIndice[1];
            this.dadosIndice[0] = new DadosIndice();
            this.dadosIndice[0].idIndice = idIndice;

            Indice indice = new Indice();
            campos = new List<esQueryItem>();
            campos.Add(indice.Query.Descricao);
            indice.LoadByPrimaryKey(campos, (short)idIndice);
            //
            this.dadosIndice[0].nomeIndice = indice.Descricao.Trim();

            this.CarregaDadosRentabilidade(calculoMedida);
        }
        private void CarregaDadosRentabilidade(CalculoMedida calculoMedida)
        {

            #region Limpa os Dicionarios
            // Limpa o Dicionario de Carteira
            for (int i = 0; i < this.dadosCarteira.Length; i++)
            {
                this.dadosCarteira[i].dadosRentabilidadeFundo.Clear();
            }

            // Limpa o Dicionario de Indice
            for (int i = 0; i < this.dadosIndice.Length; i++)
            {
                this.dadosIndice[i].dadosRentabilidadeIndice.Clear();
            }
            #endregion



            /*CalculoMedida.DicsEstatisticaRetornoAcumulado dicsRetornosAcumulados = calculoMedida.RetornaDicsRetornosAcumulados(calculoMedida.DataInicio.Value, calculoMedida.DataAtual.Value);
            // Só tem 1 Carteira
            for (int j = 0; j < this.dadosCarteira.Length; j++)
            {
                this.dadosCarteira[j].dadosRentabilidadeFundo = dicsRetornosAcumulados.retornoAcumulado;
            }

            #region Dados do Indice
            for (int j = 0; j < this.dadosIndice.Length; j++)
            {
                this.dadosIndice[j].dadosRentabilidadeIndice = dicsRetornosAcumulados.retornoBenchmarkAcumulado;
            }*/

        }
        /// <summary>
        /// Gráfico de retorno acumulado no período.
        /// </summary>
        /// 
        public bool MontaChartRetornoAcumulado()
        {
            CalculoMedida calculoMedida = new CalculoMedida(idCliente, idBenchmark);
            this.InitCalculoMedidaParaRetornoAcumulado(calculoMedida);

            //calculoMedida.InitListaRetornoMensal(calculoMedida.DataInicio.Value, calculoMedida.DataAtual.Value);

            Dataset dataset1 = new Dataset();
            Dataset dataset2 = new Dataset();

            CalculoMedida.DicsEstatisticaRetornoAcumulado dicsRetornosAcumulados = calculoMedida.RetornaDicsRetornosAcumulados(calculoMedida.DataInicio.Value, calculoMedida.DataAtual.Value);
            Dictionary<DateTime, decimal> dadosRentabilidadeFundo = dicsRetornosAcumulados.retornoAcumulado;
            Dictionary<DateTime, decimal> dadosRentabilidadeIndice = dicsRetornosAcumulados.retornoBenchmarkAcumulado;
            List<String> labelsDataInseridos = new List<string>();
            foreach (KeyValuePair<DateTime, decimal> kvp in dadosRentabilidadeFundo)
            {
                string labelData = kvp.Key.ToString("MMM/yy", new CultureInfo("pt-BR")).ToUpper();

                /*if (labelsDataInseridos.Contains(labelData))
                {
                    labelData = kvp.Key.ToString("dd/MMM/yy", new CultureInfo("pt-BR")).ToUpper();
                }*/

                labelsDataInseridos.Add(labelData);
                Category category = new Category(labelData);

                chart.categories.Add(category);
                dataset1.sets.Add(new Set((double)kvp.Value));

                if (dadosRentabilidadeIndice.ContainsKey(kvp.Key))
                {
                    dataset2.sets.Add(new Set((double)dadosRentabilidadeIndice[kvp.Key]));
                }

            }

            dataset1.seriesName = calculoMedida.carteira.Apelido;
            dataset2.seriesName = calculoMedida.indice.Descricao;

            chart.datasets.Add(dataset1);
            chart.datasets.Add(dataset2);

            return true;

        }

        /// <summary>
        /// Gráfico de retorno efetivo (dada a janela passada) no período.
        /// </summary>
        public bool MontaChartRetornoEfetivo(int janelaDias, int numeroAnos)
        {
            Indice indice = new Indice();
            indice.LoadByPrimaryKey(idBenchmark);

            DateTime dataInicio = data.AddYears(numeroAnos * -1);

            if (DateTime.Compare(this.dataInicioCota, dataInicio) > 0)
            {
                dataInicio = this.dataInicioCota;
            }

            DateTime dataInicioTeste = Calendario.SubtraiDiaUtil(dataInicio, janelaDias);

            if (DateTime.Compare(this.dataInicioCota, dataInicioTeste) > 0)
            {
                dataInicio = Calendario.AdicionaDiaUtil(dataInicio, janelaDias);
            }

            if (DateTime.Compare(dataInicio, data) >= 0)
            {
                return false;
            }

            int numeroDias = Calendario.NumeroDias(dataInicio, data);

            int intervalo = Convert.ToInt32(numeroDias / 20); //Hoje supõe um gráfico com 20 pontos, pode ser parametrizado depois

            Dataset dataset1 = new Dataset();
            Dataset dataset2 = new Dataset();

            DateTime dataAux = dataInicio;
            DateTime dataAuxAnterior = dataAux;
            CalculoMedida calculoMedida = new CalculoMedida(idCliente, idBenchmark);

            while (DateTime.Compare(dataAux, data) <= 0)
            {
                if (!Calendario.IsDiaUtil(dataAux))
                {
                    dataAux = Calendario.AdicionaDiaUtil(dataAux, 1);
                }

                DateTime dataInicioJanela = Calendario.SubtraiDiaUtil(dataAux, janelaDias);

                decimal retornoCarteira = 0;
                decimal retornoBenchmark = 0;
                try
                {
                    retornoCarteira = calculoMedida.CalculaRetorno(dataInicioJanela, dataAux);
                    retornoBenchmark = calculoMedida.CalculaRetornoIndice(dataInicioJanela, dataAux);
                }
                catch (Exception)
                {

                    return false;
                }

                chart.categories.Add(new Category(dataAux.Month.ToString() + "/" + dataAux.Year.ToString().Substring(2, 2)));
                dataset1.sets.Add(new Set((double)retornoCarteira));
                dataset2.sets.Add(new Set((double)retornoBenchmark));

                if (DateTime.Compare(dataAux, data) == 0)
                {
                    break;
                }

                dataAuxAnterior = dataAux;

                dataAux = Calendario.AdicionaDiaUtil(dataAux, intervalo);

                if (dataAux.Month == dataAuxAnterior.Month)
                {
                    dataAux = dataAux.AddMonths(1);
                }

                if (DateTime.Compare(dataAux, data) > 0)
                {
                    dataAux = data;
                }
            }

            dataset1.seriesName = this.apelido;
            dataset2.seriesName = indice.Descricao;

            chart.datasets.Add(dataset1);
            chart.datasets.Add(dataset2);

            return true;
        }

        /// <summary>
        /// Gráfico de retorno no número de meses no período.
        /// </summary>
        /// <param name="numeroMeses"></param>
        /// <param name="preencheDiferencial"></param>
        public bool MontaChartRetornoMeses(int numeroMeses, bool preencheDiferencial)
        {

            CalculoMedida calculoMedida = new CalculoMedida(idCliente, idBenchmark);

            calculoMedida.InitListaRetornoMensal(calculoMedida.DataInicio.Value, calculoMedida.DataAtual.Value);

            Dataset dataset1 = new Dataset();

            Dictionary<DateTime, decimal> dadosRentabilidadeFundo = calculoMedida.RetornaDicRetornosMensais(12);
            foreach (KeyValuePair<DateTime, decimal> kvp in dadosRentabilidadeFundo)
            {
                //chart.categories.Add(new Category(kvp.Key.Month.ToString() + "/" + kvp.Key.Year.ToString().Substring(2, 2)));
                chart.categories.Add(new Category(kvp.Key.ToString("MMM/yy", new CultureInfo("pt-BR")).ToUpper()));
                dataset1.sets.Add(new Set((double)kvp.Value));
            }

            dataset1.seriesName = calculoMedida.carteira.Apelido;

            chart.datasets.Add(dataset1);
            return true;


        }

        public bool MontaChartEvolucaoPL(DateTime dataInicio, DateTime dataFim)
        {

            CalculoMedida calculoMedida = new CalculoMedida(idCliente, idBenchmark);

            int unidade;
            Dictionary<DateTime, decimal> dadosEvolucaoPL = calculoMedida.RetornaDadosPLUnidadeMoedaAjustada(dataInicio, dataFim, out unidade);
            Dataset dataset1 = new Dataset();

            foreach (KeyValuePair<DateTime, decimal> kvp in dadosEvolucaoPL)
            {
                //chart.categories.Add(new Category(kvp.Key.Month.ToString() + "/" + kvp.Key.Year.ToString().Substring(2, 2)));
                chart.categories.Add(new Category(kvp.Key.ToString("MMM/yy", new CultureInfo("pt-BR")).ToUpper()));
                dataset1.sets.Add(new Set((double)kvp.Value));
            }

            dataset1.seriesName = calculoMedida.carteira.Apelido;

            chart.datasets.Add(dataset1);
            return true;

        }


        /// <summary>
        /// Gráfico de vol (dada a janela passada) no período.
        /// </summary>
        public bool MontaChartVolatilidade(int janelaDias, int numeroAnos)
        {
            Indice indice = new Indice();
            indice.LoadByPrimaryKey(idBenchmark);

            DateTime dataInicio = data.AddYears(numeroAnos * -1);

            if (DateTime.Compare(this.dataInicioCota, dataInicio) > 0)
            {
                dataInicio = this.dataInicioCota;
            }

            DateTime dataInicioTeste = Calendario.SubtraiDiaUtil(dataInicio, janelaDias);

            if (DateTime.Compare(this.dataInicioCota, dataInicioTeste) > 0)
            {
                dataInicio = Calendario.AdicionaDiaUtil(dataInicio, janelaDias);
            }

            if (DateTime.Compare(dataInicio, data) >= 0)
            {
                return false;
            }

            int numeroDias = Calendario.NumeroDias(dataInicio, data);

            int intervalo = Convert.ToInt32(numeroDias / 30); //Hoje supõe um gráfico com 30 pontos, pode ser parametrizado depois

            Dataset dataset1 = new Dataset();
            Dataset dataset2 = new Dataset();

            DateTime dataAux = dataInicio;
            DateTime dataAuxAnterior = dataAux;
            CalculoMedida calculoMedida = new CalculoMedida(idCliente, idBenchmark);

            while (DateTime.Compare(dataAux, data) <= 0)
            {
                if (!Calendario.IsDiaUtil(dataAux))
                {
                    dataAux = Calendario.AdicionaDiaUtil(dataAux, 1);
                }

                DateTime dataInicioJanela = Calendario.SubtraiDiaUtil(dataAux, janelaDias);

                decimal volCarteira = 0;
                decimal volBenchmark = 0;
                try
                {
                    volCarteira = calculoMedida.CalculaVolatilidade(dataInicioJanela, dataAux);
                    volBenchmark = calculoMedida.CalculaVolatilidadeBenchmark(dataInicioJanela, dataAux, idBenchmark);
                }
                catch (Exception)
                {

                    return false;
                }

                chart.categories.Add(new Category(dataAux.Month.ToString() + "/" + dataAux.Year.ToString().Substring(2, 2)));
                dataset1.sets.Add(new Set((double)volCarteira));
                dataset2.sets.Add(new Set((double)volBenchmark));

                if (DateTime.Compare(dataAux, data) == 0)
                {
                    break;
                }

                dataAuxAnterior = dataAux;

                dataAux = Calendario.AdicionaDiaUtil(dataAux, intervalo);

                if (dataAux.Month == dataAuxAnterior.Month)
                {
                    dataAux = dataAux.AddMonths(1);
                }

                if (DateTime.Compare(dataAux, data) > 0)
                {
                    dataAux = data;
                }
            }

            dataset1.seriesName = this.apelido;
            dataset2.seriesName = indice.Descricao;

            chart.datasets.Add(dataset1);
            chart.datasets.Add(dataset2);

            return true;
        }

    }

}

