﻿using System;
using Financial.Util;
using System.Collections.Generic;
using Financial.Investidor;
using Financial.Bolsa;
using Financial.RendaFixa;
using Financial.RendaFixa.Enums;
using Financial.ContaCorrente;
using Financial.Fundo;
using Financial.Common;
using EntitySpaces.Interfaces;
using Financial.Fundo.Enums;
using Financial.Bolsa.Enums;
using Financial.Investidor.Enums;
using System.Text.RegularExpressions;

namespace FinancialDesk.BO.Series
{
    public class SingleSerieValues
    {
        #region Atributos e properties
        private int idCliente;

        public int IdCliente
        {
            get { return idCliente; }
            set { idCliente = value; }
        }

        private DateTime data;

        public DateTime Data
        {
            get { return data; }
            set { data = value; }
        }

        private Chart chart;

        public Chart Chart
        {
            get { return chart; }
            set { chart = value; }
        }
        #endregion

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="data"></param>
        public SingleSerieValues(int idCliente, DateTime data)
        {
            this.idCliente = idCliente;
            
            //Seta a dataAtual do Cliente
            Cliente cliente = new Cliente();
            List<esQueryItem> campos = new List<esQueryItem>();
            campos.Add(cliente.Query.DataDia);
            campos.Add(cliente.Query.Status);
            cliente.LoadByPrimaryKey(campos, idCliente);

            if (cliente.DataDia.Value > data)
            {
                this.data = data;
            }
            else
            {
                //Se a carteira não foi calculada na data ou está com último cálculo não ok, pega a data anterior
                if (cliente.Status.Value == (byte)StatusCliente.Fechado || cliente.Status.Value == (byte)StatusCliente.Divulgado)
                {
                    this.data = cliente.DataDia.Value;
                }
                else
                {
                    this.data = Calendario.SubtraiDiaUtil(cliente.DataDia.Value, 1);
                }
            }

            this.Chart = new Chart();
        }

        /// <summary>
        /// Gráfico de distribuição por alocação.
        /// </summary>
        public void MontaChartAlocacao(bool explodeFundos)
        {
            CalculoMedida c = new CalculoMedida(explodeFundos, idCliente);
            List<CalculoMedida.AlocacaoEstrategia> lista = c.RetornaListaAlocacaoEstrategia(data);

            Dataset dataset = new Dataset();
            foreach (CalculoMedida.AlocacaoEstrategia alocacao in lista)
            {
                chart.categories.Add(new Category(alocacao.Descricao));
                dataset.sets.Add(new Set((double)alocacao.Valor));
            }

            chart.datasets.Add(dataset);
        }

        public void MontaChartLiquidez(bool explodeFundos)
        {
            CalculoMedida c = new CalculoMedida(explodeFundos, idCliente);
            List<CalculoMedida.Liquidez> lista = c.RetornaListaLiquidez(data);
            Dataset dataset = new Dataset();
            foreach (CalculoMedida.Liquidez liquidez in lista)
            {
                string[] valuesDia = Regex.Split(liquidez.Dia, "\r\n");
                chart.categories.Add(new Category(valuesDia[0]));
                dataset.sets.Add(new Set((double)liquidez.Valor));
            }

            chart.datasets.Add(dataset);

        }

        /// <summary>
        /// Gráfico de distribuição por gestor.
        /// </summary>
        public void MontaChartGestor(bool explodeFundos)
        {
            CalculoMedida c = new CalculoMedida(explodeFundos, idCliente);
            List<CalculoMedida.AlocacaoGestor> lista = c.RetornaListaAlocacaoGestor(data);

            Dataset dataset = new Dataset();
            foreach (CalculoMedida.AlocacaoGestor alocacao in lista)
            {
                chart.categories.Add(new Category(alocacao.Nome));
                dataset.sets.Add(new Set((double)alocacao.Valor));
            }

            chart.datasets.Add(dataset);
        }

        /// <summary>
        /// Gráfico de distribuição por ativo.
        /// </summary>
        public void MontaChartAtivo(bool explodeFundos)
        {
            CalculoMedida c = new CalculoMedida(explodeFundos, idCliente);
            List<CalculoMedida.AlocacaoAtivo> lista = c.RetornaListaAlocacaoAtivo(data);

            Dataset dataset = new Dataset();
            foreach (CalculoMedida.AlocacaoAtivo alocacao in lista)
            {
                chart.categories.Add(new Category(alocacao.Descricao));
                dataset.sets.Add(new Set((double)alocacao.Valor));
            }

            chart.datasets.Add(dataset);
        }

        /// <summary>
        /// Gráfico de distribuição por risco.
        /// </summary>
        public void MontaChartRisco()
        {
            decimal riscoMaisBaixo = 0;
            decimal riscoBaixo = 0;
            decimal riscoModerado = 0;
            decimal riscoAlto = 0;
            decimal riscoMaisAlto = 0;
            
            Cliente cliente = new Cliente();
            cliente.LoadByPrimaryKey(idCliente);
            
            if (DateTime.Compare(cliente.DataDia.Value, data) > 0)
            {
                #region Ações (risco alto)
                PosicaoBolsaHistoricoCollection posicaoBolsaHistoricoCollection = new PosicaoBolsaHistoricoCollection();
                posicaoBolsaHistoricoCollection.Query.Select(posicaoBolsaHistoricoCollection.Query.CdAtivoBolsa, posicaoBolsaHistoricoCollection.Query.ValorMercado.Sum());
                posicaoBolsaHistoricoCollection.Query.Where(posicaoBolsaHistoricoCollection.Query.IdCliente.Equal(idCliente),
                                             posicaoBolsaHistoricoCollection.Query.Quantidade.NotEqual(0),
                                             posicaoBolsaHistoricoCollection.Query.DataHistorico.Equal(data),
                                             posicaoBolsaHistoricoCollection.Query.TipoMercado.Equal(TipoMercadoBolsa.MercadoVista));
                posicaoBolsaHistoricoCollection.Query.GroupBy(posicaoBolsaHistoricoCollection.Query.CdAtivoBolsa);
                posicaoBolsaHistoricoCollection.Query.Load();

                foreach (PosicaoBolsaHistorico posicaoBolsaHistorico in posicaoBolsaHistoricoCollection)
                {
                    riscoAlto += posicaoBolsaHistorico.ValorMercado.Value;
                }
                #endregion

                #region Opções compradas (risco alto)
                posicaoBolsaHistoricoCollection = new PosicaoBolsaHistoricoCollection();
                posicaoBolsaHistoricoCollection.Query.Select(posicaoBolsaHistoricoCollection.Query.CdAtivoBolsa, posicaoBolsaHistoricoCollection.Query.ValorMercado.Sum());
                posicaoBolsaHistoricoCollection.Query.Where(posicaoBolsaHistoricoCollection.Query.IdCliente.Equal(idCliente),
                                             posicaoBolsaHistoricoCollection.Query.Quantidade.GreaterThan(0),
                                             posicaoBolsaHistoricoCollection.Query.TipoMercado.In(TipoMercadoBolsa.OpcaoCompra, TipoMercadoBolsa.OpcaoVenda),
                                             posicaoBolsaHistoricoCollection.Query.DataHistorico.Equal(data));
                posicaoBolsaHistoricoCollection.Query.GroupBy(posicaoBolsaHistoricoCollection.Query.CdAtivoBolsa);
                posicaoBolsaHistoricoCollection.Query.Load();

                foreach (PosicaoBolsaHistorico posicaoBolsaHistorico in posicaoBolsaHistoricoCollection)
                {
                    riscoAlto += posicaoBolsaHistorico.ValorMercado.Value;
                }
                #endregion

                #region Opções vendidas (risco muito alto se vendidas a seco, risco baixo se vendidas cobertas)
                PosicaoBolsaHistoricoQuery posicaoBolsaHistoricoQuery = new PosicaoBolsaHistoricoQuery("P");
                AtivoBolsaQuery ativoBolsaQuery = new AtivoBolsaQuery("A");
                posicaoBolsaHistoricoQuery.Select(ativoBolsaQuery.CdAtivoBolsaObjeto, posicaoBolsaHistoricoQuery.Quantidade.Sum(),
                                                  posicaoBolsaHistoricoQuery.ValorMercado.Sum());
                posicaoBolsaHistoricoQuery.InnerJoin(ativoBolsaQuery).On(ativoBolsaQuery.CdAtivoBolsa == posicaoBolsaHistoricoQuery.CdAtivoBolsa);
                posicaoBolsaHistoricoQuery.Where(posicaoBolsaHistoricoQuery.IdCliente.Equal(idCliente),
                                       posicaoBolsaHistoricoQuery.TipoMercado.In(TipoMercadoBolsa.OpcaoCompra,
                                                                        TipoMercadoBolsa.OpcaoVenda),
                                       posicaoBolsaHistoricoQuery.Quantidade.LessThan(0),
                                       posicaoBolsaHistoricoQuery.DataHistorico.Equal(data));
                posicaoBolsaHistoricoQuery.GroupBy(ativoBolsaQuery.CdAtivoBolsaObjeto);

                posicaoBolsaHistoricoCollection = new PosicaoBolsaHistoricoCollection();
                posicaoBolsaHistoricoCollection.Load(posicaoBolsaHistoricoQuery);

                foreach (PosicaoBolsaHistorico posicaoBolsaHistorico in posicaoBolsaHistoricoCollection)
                {
                    string cdAtivoBolsaObjeto = Convert.ToString(posicaoBolsaHistorico.GetColumn(AtivoBolsaMetadata.ColumnNames.CdAtivoBolsaObjeto));

                    decimal quantidadeAbs = Math.Abs(posicaoBolsaHistorico.Quantidade.Value);
                    PosicaoBolsaHistorico posicaoBolsaCoberta = new PosicaoBolsaHistorico();
                    posicaoBolsaCoberta.Query.Select(posicaoBolsaCoberta.Query.Quantidade.Sum());
                    posicaoBolsaCoberta.Query.Where(posicaoBolsaCoberta.Query.IdCliente.Equal(idCliente),
                                                    posicaoBolsaCoberta.Query.CdAtivoBolsa.Equal(cdAtivoBolsaObjeto),
                                                    posicaoBolsaCoberta.Query.DataHistorico.Equal(data));
                    posicaoBolsaCoberta.Query.Load();

                    if (posicaoBolsaCoberta.Quantidade.HasValue)
                    {
                        if (quantidadeAbs > posicaoBolsaCoberta.Quantidade.Value)
                        {
                            riscoMaisAlto += Math.Abs(posicaoBolsaHistorico.ValorMercado.Value);
                        }
                        else
                        {
                            riscoModerado += Math.Abs(posicaoBolsaHistorico.ValorMercado.Value);
                        }
                    }
                    else
                    {
                        riscoMaisAlto += Math.Abs(posicaoBolsaHistorico.ValorMercado.Value);
                    }

                }
                #endregion

                #region Termo - comprado e vendido (risco moderado)
                PosicaoTermoBolsaHistoricoCollection posicaoTermoBolsaHistoricoCollection = new PosicaoTermoBolsaHistoricoCollection();
                posicaoTermoBolsaHistoricoCollection.Query.Select(posicaoTermoBolsaHistoricoCollection.Query.CdAtivoBolsa, posicaoTermoBolsaHistoricoCollection.Query.ValorMercado.Sum());
                posicaoTermoBolsaHistoricoCollection.Query.Where(posicaoTermoBolsaHistoricoCollection.Query.IdCliente.Equal(idCliente),
                                             posicaoTermoBolsaHistoricoCollection.Query.Quantidade.NotEqual(0),
                                             posicaoTermoBolsaHistoricoCollection.Query.DataHistorico.Equal(data));
                posicaoTermoBolsaHistoricoCollection.Query.GroupBy(posicaoTermoBolsaHistoricoCollection.Query.CdAtivoBolsa);
                posicaoTermoBolsaHistoricoCollection.Query.Load();

                foreach (PosicaoTermoBolsaHistorico posicaoTermoBolsaHistorico in posicaoTermoBolsaHistoricoCollection)
                {
                    riscoModerado += Math.Abs(posicaoTermoBolsaHistorico.ValorMercado.Value);
                }
                #endregion

                #region Fundos (depende do tipo de fundo - RF ou RV - e se é ou não multimercado - dada a descrição da categoria)
                PosicaoFundoHistoricoCollection posicaoFundoHistoricoCollection = new PosicaoFundoHistoricoCollection();
                posicaoFundoHistoricoCollection.Query.Select(posicaoFundoHistoricoCollection.Query.IdCarteira, posicaoFundoHistoricoCollection.Query.ValorBruto.Sum());
                posicaoFundoHistoricoCollection.Query.Where(posicaoFundoHistoricoCollection.Query.IdCliente.Equal(idCliente),
                                             posicaoFundoHistoricoCollection.Query.Quantidade.NotEqual(0),
                                             posicaoFundoHistoricoCollection.Query.DataHistorico.Equal(data));
                posicaoFundoHistoricoCollection.Query.GroupBy(posicaoFundoHistoricoCollection.Query.IdCarteira);
                posicaoFundoHistoricoCollection.Query.Load();

                foreach (PosicaoFundoHistorico posicaoFundoHistorico in posicaoFundoHistoricoCollection)
                {
                    int idCarteira = posicaoFundoHistorico.IdCarteira.Value;

                    Carteira carteira = new Carteira();
                    List<esQueryItem> campos = new List<esQueryItem>();
                    campos.Add(carteira.Query.TipoCarteira);
                    campos.Add(carteira.Query.IdCategoria);
                    carteira.LoadByPrimaryKey(campos, idCarteira);

                    if (carteira.TipoCarteira == (byte)TipoCarteiraFundo.RendaFixa)
                    {
                        riscoBaixo += posicaoFundoHistorico.ValorBruto.Value;
                    }
                    else
                    {
                        int idCategoria = carteira.IdCategoria.Value;
                        CategoriaFundo categoriaFundo = new CategoriaFundo();
                        categoriaFundo.LoadByPrimaryKey(idCategoria);
                        string descricaoCategoria = categoriaFundo.Descricao.ToUpper();
                        if (descricaoCategoria.Contains("MULTIMERCADO") || descricaoCategoria.Contains("MULTI-MERCADO") ||
                            descricaoCategoria.Contains("MULTI MERCADO"))
                        {
                            riscoMaisAlto += posicaoFundoHistorico.ValorBruto.Value;
                        }
                        else
                        {
                            riscoAlto += posicaoFundoHistorico.ValorBruto.Value;
                        }
                    }
                }
                #endregion

                #region Renda Fixa (risco baixo para titulos privados, risco muito baixo para titulos publicos)
                PosicaoRendaFixaHistoricoCollection posicaoRendaFixaHistoricoCollection = new PosicaoRendaFixaHistoricoCollection();
                posicaoRendaFixaHistoricoCollection.Query.Select(posicaoRendaFixaHistoricoCollection.Query.IdTitulo, posicaoRendaFixaHistoricoCollection.Query.ValorMercado.Sum());
                posicaoRendaFixaHistoricoCollection.Query.Where(posicaoRendaFixaHistoricoCollection.Query.IdCliente.Equal(idCliente),
                                             posicaoRendaFixaHistoricoCollection.Query.Quantidade.NotEqual(0),
                                             posicaoRendaFixaHistoricoCollection.Query.DataHistorico.Equal(data));
                posicaoRendaFixaHistoricoCollection.Query.GroupBy(posicaoRendaFixaHistoricoCollection.Query.IdTitulo);
                posicaoRendaFixaHistoricoCollection.Query.Load();

                foreach (PosicaoRendaFixaHistorico posicaoRendaFixaHistorico in posicaoRendaFixaHistoricoCollection)
                {
                    int idTitulo = posicaoRendaFixaHistorico.IdTitulo.Value;
                    TituloRendaFixa tituloRendaFixa = new TituloRendaFixa();
                    List<esQueryItem> campos = new List<esQueryItem>();
                    campos.Add(tituloRendaFixa.Query.IdPapel);
                    tituloRendaFixa.LoadByPrimaryKey(campos, idTitulo);
                    int idPapel = tituloRendaFixa.IdPapel.Value;

                    PapelRendaFixa papelRendaFixa = new PapelRendaFixa();
                    campos = new List<esQueryItem>();
                    campos.Add(papelRendaFixa.Query.TipoPapel);
                    papelRendaFixa.LoadByPrimaryKey(campos, idPapel);
                    if (papelRendaFixa.TipoPapel.Value == (byte)TipoPapelTitulo.Privado)
                    {
                        riscoBaixo += posicaoRendaFixaHistorico.ValorMercado.Value;
                    }
                    else
                    {
                        riscoMaisBaixo += posicaoRendaFixaHistorico.ValorMercado.Value;
                    }
                }
                #endregion                                
            }
            else
            {
                #region Ações (risco alto)
                PosicaoBolsaCollection posicaoBolsaCollection = new PosicaoBolsaCollection();
                posicaoBolsaCollection.Query.Select(posicaoBolsaCollection.Query.CdAtivoBolsa, posicaoBolsaCollection.Query.ValorMercado.Sum());
                posicaoBolsaCollection.Query.Where(posicaoBolsaCollection.Query.IdCliente.Equal(idCliente),
                                             posicaoBolsaCollection.Query.Quantidade.NotEqual(0),
                                             posicaoBolsaCollection.Query.TipoMercado.Equal(TipoMercadoBolsa.MercadoVista));
                posicaoBolsaCollection.Query.GroupBy(posicaoBolsaCollection.Query.CdAtivoBolsa);
                posicaoBolsaCollection.Query.Load();

                foreach (PosicaoBolsa posicaoBolsa in posicaoBolsaCollection)
                {
                    riscoAlto += posicaoBolsa.ValorMercado.Value;
                }
                #endregion

                #region Opções compradas (risco alto)
                posicaoBolsaCollection = new PosicaoBolsaCollection();
                posicaoBolsaCollection.Query.Select(posicaoBolsaCollection.Query.CdAtivoBolsa, posicaoBolsaCollection.Query.ValorMercado.Sum());
                posicaoBolsaCollection.Query.Where(posicaoBolsaCollection.Query.IdCliente.Equal(idCliente),
                                             posicaoBolsaCollection.Query.Quantidade.GreaterThan(0),
                                             posicaoBolsaCollection.Query.TipoMercado.In(TipoMercadoBolsa.OpcaoCompra, TipoMercadoBolsa.OpcaoVenda));
                posicaoBolsaCollection.Query.GroupBy(posicaoBolsaCollection.Query.CdAtivoBolsa);
                posicaoBolsaCollection.Query.Load();

                foreach (PosicaoBolsa posicaoBolsa in posicaoBolsaCollection)
                {
                    riscoAlto += posicaoBolsa.ValorMercado.Value;
                }
                #endregion

                #region Opções vendidas (risco muito alto se vendidas a seco, risco baixo se vendidas cobertas)
                PosicaoBolsaQuery posicaoBolsaQuery = new PosicaoBolsaQuery("P");
                AtivoBolsaQuery ativoBolsaQuery = new AtivoBolsaQuery("A");
                posicaoBolsaQuery.Select(ativoBolsaQuery.CdAtivoBolsaObjeto, posicaoBolsaQuery.Quantidade.Sum(), posicaoBolsaQuery.ValorMercado.Sum());
                posicaoBolsaQuery.InnerJoin(ativoBolsaQuery).On(ativoBolsaQuery.CdAtivoBolsa == posicaoBolsaQuery.CdAtivoBolsa);
                posicaoBolsaQuery.Where(posicaoBolsaQuery.IdCliente.Equal(idCliente),
                                       posicaoBolsaQuery.TipoMercado.In(TipoMercadoBolsa.OpcaoCompra,
                                                                        TipoMercadoBolsa.OpcaoVenda),
                                       posicaoBolsaQuery.Quantidade.LessThan(0));
                posicaoBolsaQuery.GroupBy(ativoBolsaQuery.CdAtivoBolsaObjeto);

                posicaoBolsaCollection = new PosicaoBolsaCollection();
                posicaoBolsaCollection.Load(posicaoBolsaQuery);

                foreach (PosicaoBolsa posicaoBolsa in posicaoBolsaCollection)
                {
                    string cdAtivoBolsaObjeto = Convert.ToString(posicaoBolsa.GetColumn(AtivoBolsaMetadata.ColumnNames.CdAtivoBolsaObjeto));
                    
                    decimal quantidadeAbs = Math.Abs(posicaoBolsa.Quantidade.Value);
                    PosicaoBolsa posicaoBolsaCoberta = new PosicaoBolsa();
                    posicaoBolsaCoberta.Query.Select(posicaoBolsaCoberta.Query.Quantidade.Sum());
                    posicaoBolsaCoberta.Query.Where(posicaoBolsaCoberta.Query.IdCliente.Equal(idCliente),
                                                    posicaoBolsaCoberta.Query.CdAtivoBolsa.Equal(cdAtivoBolsaObjeto));
                    posicaoBolsaCoberta.Query.Load();

                    if (posicaoBolsaCoberta.Quantidade.HasValue)
                    {
                        if (quantidadeAbs > posicaoBolsaCoberta.Quantidade.Value)
                        {
                            riscoMaisAlto += Math.Abs(posicaoBolsa.ValorMercado.Value);
                        }
                        else
                        {
                            riscoModerado += Math.Abs(posicaoBolsa.ValorMercado.Value);
                        }
                    }
                    else
                    {
                        riscoMaisAlto += Math.Abs(posicaoBolsa.ValorMercado.Value);
                    }

                }
                #endregion

                #region Termo - comprado e vendido (risco moderado) 
                PosicaoTermoBolsaCollection posicaoTermoBolsaCollection = new PosicaoTermoBolsaCollection();
                posicaoTermoBolsaCollection.Query.Select(posicaoTermoBolsaCollection.Query.CdAtivoBolsa, posicaoTermoBolsaCollection.Query.ValorMercado.Sum());
                posicaoTermoBolsaCollection.Query.Where(posicaoTermoBolsaCollection.Query.IdCliente.Equal(idCliente),
                                             posicaoTermoBolsaCollection.Query.Quantidade.NotEqual(0));
                posicaoTermoBolsaCollection.Query.GroupBy(posicaoTermoBolsaCollection.Query.CdAtivoBolsa);
                posicaoTermoBolsaCollection.Query.Load();

                foreach (PosicaoTermoBolsa posicaoTermoBolsa in posicaoTermoBolsaCollection)
                {
                    riscoModerado += Math.Abs(posicaoTermoBolsa.ValorMercado.Value);
                }
                #endregion

                #region Fundos (depende do tipo de fundo - RF ou RV - e se é ou não multimercado - dada a descrição da categoria)
                PosicaoFundoCollection posicaoFundoCollection = new PosicaoFundoCollection();
                posicaoFundoCollection.Query.Select(posicaoFundoCollection.Query.IdCarteira, posicaoFundoCollection.Query.ValorBruto.Sum());
                posicaoFundoCollection.Query.Where(posicaoFundoCollection.Query.IdCliente.Equal(idCliente),
                                             posicaoFundoCollection.Query.Quantidade.NotEqual(0));
                posicaoFundoCollection.Query.GroupBy(posicaoFundoCollection.Query.IdCarteira);
                posicaoFundoCollection.Query.Load();

                foreach (PosicaoFundo posicaoFundo in posicaoFundoCollection)
                {
                    int idCarteira = posicaoFundo.IdCarteira.Value;

                    Carteira carteira = new Carteira();
                    List<esQueryItem> campos = new List<esQueryItem>();
                    campos.Add(carteira.Query.TipoCarteira);
                    campos.Add(carteira.Query.IdCategoria);
                    carteira.LoadByPrimaryKey(campos, idCarteira);

                    if (carteira.TipoCarteira == (byte)TipoCarteiraFundo.RendaFixa)
                    {
                        riscoBaixo += posicaoFundo.ValorBruto.Value;
                    }
                    else
                    {
                        int idCategoria = carteira.IdCategoria.Value;
                        CategoriaFundo categoriaFundo = new CategoriaFundo();
                        categoriaFundo.LoadByPrimaryKey(idCategoria);
                        string descricaoCategoria = categoriaFundo.Descricao.ToUpper();
                        if (descricaoCategoria.Contains("MULTIMERCADO") || descricaoCategoria.Contains("MULTI-MERCADO") ||
                            descricaoCategoria.Contains("MULTI MERCADO"))
                        {
                            riscoMaisAlto += posicaoFundo.ValorBruto.Value;
                        }
                        else
                        {
                            riscoAlto += posicaoFundo.ValorBruto.Value;
                        }
                    }
                }
                #endregion

                #region Renda Fixa (risco baixo para titulos privados, risco muito baixo para titulos publicos)
                PosicaoRendaFixaCollection posicaoRendaFixaCollection = new PosicaoRendaFixaCollection();
                posicaoRendaFixaCollection.Query.Select(posicaoRendaFixaCollection.Query.IdTitulo, posicaoRendaFixaCollection.Query.ValorMercado.Sum());
                posicaoRendaFixaCollection.Query.Where(posicaoRendaFixaCollection.Query.IdCliente.Equal(idCliente),
                                             posicaoRendaFixaCollection.Query.Quantidade.NotEqual(0));
                posicaoRendaFixaCollection.Query.GroupBy(posicaoRendaFixaCollection.Query.IdTitulo);
                posicaoRendaFixaCollection.Query.Load();

                foreach (PosicaoRendaFixa posicaoRendaFixa in posicaoRendaFixaCollection)
                {
                    int idTitulo = posicaoRendaFixa.IdTitulo.Value;
                    TituloRendaFixa tituloRendaFixa = new TituloRendaFixa();
                    List<esQueryItem> campos = new List<esQueryItem>();
                    campos.Add(tituloRendaFixa.Query.IdPapel);
                    tituloRendaFixa.LoadByPrimaryKey(campos, idTitulo);
                    int idPapel = tituloRendaFixa.IdPapel.Value;

                    PapelRendaFixa papelRendaFixa = new PapelRendaFixa();
                    campos = new List<esQueryItem>();
                    campos.Add(papelRendaFixa.Query.TipoPapel);
                    papelRendaFixa.LoadByPrimaryKey(campos, idPapel);
                    if (papelRendaFixa.TipoPapel.Value == (byte)TipoPapelTitulo.Privado)
                    {
                        riscoBaixo += posicaoRendaFixa.ValorMercado.Value;
                    }
                    else
                    {
                        riscoMaisBaixo += posicaoRendaFixa.ValorMercado.Value;
                    }
                }
                #endregion
            }

            #region Saldo em CC (risco muito baixo)
            SaldoCaixa saldoCaixa = new SaldoCaixa();
            saldoCaixa.Query.Select(saldoCaixa.Query.SaldoFechamento.Sum());
            saldoCaixa.Query.Where(saldoCaixa.Query.IdCliente.Equal(idCliente),
                                   saldoCaixa.Query.Data.Equal(data));
            saldoCaixa.Query.Load();

            if (saldoCaixa.SaldoFechamento.HasValue)
            {
                riscoMaisBaixo += saldoCaixa.SaldoFechamento.Value;
            }
            #endregion

            Dataset dataset = new Dataset();

            if (riscoMaisBaixo != 0)
            {
                chart.categories.Add(new Category("Baixíssimo Risco"));
                dataset.sets.Add(new Set((double)riscoMaisBaixo));
            }
            if (riscoBaixo != 0)
            {
                chart.categories.Add(new Category("Baixo Risco"));
                dataset.sets.Add(new Set((double)riscoBaixo));
            }
            if (riscoModerado != 0)
            {
                chart.categories.Add(new Category("Risco Moderado"));
                dataset.sets.Add(new Set((double)riscoModerado));
            }
            if (riscoAlto != 0)
            {
                chart.categories.Add(new Category("Alto Risco"));
                dataset.sets.Add(new Set((double)riscoAlto));
            }
            if (riscoMaisAlto != 0)
            {
                chart.categories.Add(new Category("Altíssimo Risco"));
                dataset.sets.Add(new Set((double)riscoMaisAlto));
            }
            
            chart.datasets.Add(dataset);
        }

        /// <summary>
        /// Gráfico de distribuição por indústria (somente ações).
        /// </summary>
        public void MontaChartIndustria()
        {
            Cliente cliente = new Cliente();
            cliente.LoadByPrimaryKey(idCliente);

            #region Monta os valores
            if (DateTime.Compare(cliente.DataDia.Value, data) > 0)
            {
                PosicaoBolsaHistoricoQuery posicaoBolsaHistoricoQuery = new PosicaoBolsaHistoricoQuery("P");
                AtivoBolsaQuery ativoBolsaQuery = new AtivoBolsaQuery("A");
                EmissorQuery emissorQuery = new EmissorQuery("E");
                SetorQuery setorQuery = new SetorQuery("S");

                posicaoBolsaHistoricoQuery.Select(setorQuery.Nome, posicaoBolsaHistoricoQuery.ValorMercado.Sum());
                posicaoBolsaHistoricoQuery.InnerJoin(ativoBolsaQuery).On(ativoBolsaQuery.CdAtivoBolsa == posicaoBolsaHistoricoQuery.CdAtivoBolsa);
                posicaoBolsaHistoricoQuery.InnerJoin(emissorQuery).On(emissorQuery.IdEmissor == ativoBolsaQuery.IdEmissor);
                posicaoBolsaHistoricoQuery.InnerJoin(setorQuery).On(setorQuery.IdSetor == emissorQuery.IdSetor);
                posicaoBolsaHistoricoQuery.Where(posicaoBolsaHistoricoQuery.IdCliente.Equal(idCliente),
                                                 posicaoBolsaHistoricoQuery.DataHistorico.Equal(data));
                posicaoBolsaHistoricoQuery.GroupBy(setorQuery.Nome);

                PosicaoBolsaHistoricoCollection posicaoBolsaHistoricoCollection = new PosicaoBolsaHistoricoCollection();
                posicaoBolsaHistoricoCollection.Load(posicaoBolsaHistoricoQuery);

                Dataset dataset = new Dataset();
                foreach (PosicaoBolsaHistorico posicaoBolsaHistorico in posicaoBolsaHistoricoCollection)
                {
                    string nome = (string)posicaoBolsaHistorico.GetColumn("Nome");
                    decimal valor = posicaoBolsaHistorico.ValorMercado.Value;

                    chart.categories.Add(new Category(nome));
                    dataset.sets.Add(new Set((double)valor));
                }
                chart.datasets.Add(dataset);
            }
            else
            {
                PosicaoBolsaQuery posicaoBolsaQuery = new PosicaoBolsaQuery("P");
                AtivoBolsaQuery ativoBolsaQuery = new AtivoBolsaQuery("A");
                EmissorQuery emissorQuery = new EmissorQuery("E");
                SetorQuery setorQuery = new SetorQuery("S");

                posicaoBolsaQuery.Select(setorQuery.Nome, posicaoBolsaQuery.ValorMercado.Sum());
                posicaoBolsaQuery.InnerJoin(ativoBolsaQuery).On(ativoBolsaQuery.CdAtivoBolsa == posicaoBolsaQuery.CdAtivoBolsa);
                posicaoBolsaQuery.InnerJoin(emissorQuery).On(emissorQuery.IdEmissor == ativoBolsaQuery.IdEmissor);
                posicaoBolsaQuery.InnerJoin(setorQuery).On(setorQuery.IdSetor == emissorQuery.IdSetor);
                posicaoBolsaQuery.Where(posicaoBolsaQuery.IdCliente.Equal(idCliente));
                posicaoBolsaQuery.GroupBy(setorQuery.Nome);

                PosicaoBolsaCollection posicaoBolsaCollection = new PosicaoBolsaCollection();
                posicaoBolsaCollection.Load(posicaoBolsaQuery);

                Dataset dataset = new Dataset();
                foreach (PosicaoBolsa posicaoBolsa in posicaoBolsaCollection)
                {
                    string nome = (string)posicaoBolsa.GetColumn("Nome");
                    decimal valor = posicaoBolsa.ValorMercado.Value;

                    chart.categories.Add(new Category(nome));
                    dataset.sets.Add(new Set((double)valor));
                }
                chart.datasets.Add(dataset);
            }
            #endregion
            
        }
        
    }
    
}

