﻿using System;
using Financial.Util;
using System.Collections.Generic;

namespace FinancialDesk.BO
{
    /// <summary>
    /// 
    /// </summary>
    /// 

    public enum RelatorioEnum
    {
        MapaResultado = 1,
        MapaOperacaoBolsa = 2,
        MapaOperacaoBMF = 3,
        ExtratoContaCorrente = 4,
        GanhoRendaVariavel = 5
    }

    public enum ListaTipoFixa
    {
        Bolsa = 1,
        RendaFixa = 2,
        Fundos = 3,
        Caixa = 4,
        Liquidacao = 5,
        BMF = 6,
        Swap = 7
    }

    public class RelatorioInfo
    {
        string url;
        public string Url
        {
            get { return url; }
            set { url = value; }
        }

        string singular;
        public string Singular
        {
            get { return singular; }
            set { singular = value; }
        }

        bool isLeaf;//leaf ou branch
        public bool IsLeaf
        {
            get { return isLeaf; }
            set { isLeaf = value; }
        }

        int level;//identacao
        public int Level
        {
            get { return level; }
            set { level = value; }
        }

        public RelatorioInfo(string url, string singular, bool isLeaf, int level)
        {
            this.url = url;
            this.singular = singular;
            this.isLeaf = isLeaf;
            this.level = level;
        }
    }

    public class FinancialDeskTipoAtivo
    {
        int idTipo;
        public int IdTipo
        {
            get { return idTipo; }
            set { idTipo = value; }
        }

        string tipo;
        public string Tipo
        {
            get { return tipo; }
            set { tipo = value; }
        }

        string singular;
        public string Singular
        {
            get { return singular; }
            set { singular = value; }
        }

        string plural;
        public string Plural
        {
            get { return plural; }
            set { plural = value; }
        }

        public FinancialDeskTipoAtivo(int idTipo, string tipo, string singular, string plural)
        {
            this.idTipo = idTipo;
            this.tipo = tipo;
            this.singular = singular;
            this.plural = plural;
        }
    }

    public class EstruturaTipos
    {
        public Dictionary<ListaTipoFixa, FinancialDeskTipoAtivo> listaTiposNomeada = new Dictionary<ListaTipoFixa, FinancialDeskTipoAtivo>();
        public EstruturaTipos()
        {
            listaTiposNomeada.Add(ListaTipoFixa.Bolsa, new FinancialDeskTipoAtivo((int)ListaTipoFixa.Bolsa, "Acao", "Ação", "Ações"));
            listaTiposNomeada.Add(ListaTipoFixa.RendaFixa, new FinancialDeskTipoAtivo((int)ListaTipoFixa.RendaFixa, "RendaFixa", "Renda Fixa", "Renda Fixa"));
            listaTiposNomeada.Add(ListaTipoFixa.Fundos, new FinancialDeskTipoAtivo((int)ListaTipoFixa.Fundos, "CotaInvestimento", "Fundo de Investimento", "Fundos de Investimento"));
            listaTiposNomeada.Add(ListaTipoFixa.Caixa, new FinancialDeskTipoAtivo((int)ListaTipoFixa.Caixa, "Caixa", "Saldo em CC", "Saldo em CC"));
            listaTiposNomeada.Add(ListaTipoFixa.Liquidacao, new FinancialDeskTipoAtivo((int)ListaTipoFixa.Liquidacao, "Liquidar", "Liquidação Proj.", "Liquidação Proj."));
            listaTiposNomeada.Add(ListaTipoFixa.BMF, new FinancialDeskTipoAtivo((int)ListaTipoFixa.BMF, "BMF", "BMF", "BMF"));
            listaTiposNomeada.Add(ListaTipoFixa.Swap, new FinancialDeskTipoAtivo((int)ListaTipoFixa.Swap, "Swap", "Swap", "Swap"));


            /*listaTiposNomeada.Add(ListaTipoFixa.Acao, new FinancialDeskTipoAtivo((int)ListaTipoFixa.Acao, "Acao", "Ação", "Ações"));
            listaTiposNomeada.Add(ListaTipoFixa.OpcaoBolsa, new FinancialDeskTipoAtivo((int)ListaTipoFixa.OpcaoBolsa, "OpcaoBolsa", "Opção Bolsa", "Opções Bolsa"));
            listaTiposNomeada.Add(ListaTipoFixa.TermoBolsa, new FinancialDeskTipoAtivo((int)ListaTipoFixa.TermoBolsa, "TermoBolsa", "Termo Bolsa", "Termos Bolsa"));
            listaTiposNomeada.Add(ListaTipoFixa.FuturoBMF, new FinancialDeskTipoAtivo((int)ListaTipoFixa.FuturoBMF, "FuturoBMF", "Futuro BMF", "Futuros BMF"));
            listaTiposNomeada.Add(ListaTipoFixa.OpcaoBMF, new FinancialDeskTipoAtivo((int)ListaTipoFixa.FuturoBMF, "OpcaoBMF", "Opção BMF", "Opções BMF"));
            listaTiposNomeada.Add(ListaTipoFixa.RendaFixa, new FinancialDeskTipoAtivo((int)ListaTipoFixa.RendaFixa, "RendaFixa", "Renda Fixa", "Renda Fixa"));
            listaTiposNomeada.Add(ListaTipoFixa.CotaInvestimento, new FinancialDeskTipoAtivo((int)ListaTipoFixa.CotaInvestimento, "CotaInvestimento", "Fundo de Investimento", "Fundos de Investimento"));
            listaTiposNomeada.Add(ListaTipoFixa.Caixa, new FinancialDeskTipoAtivo((int)ListaTipoFixa.Caixa, "Caixa", "Saldo em CC", "Saldo em CC"));
            listaTiposNomeada.Add(ListaTipoFixa.Liquidar, new FinancialDeskTipoAtivo((int)ListaTipoFixa.Liquidar, "Liquidar", "Liquidação Proj.", "Liquidação Proj."));*/
        }
    }

    /*public class RelatoriosInfo
    {
        public Dictionary<RelatorioEnum, RelatorioInfo> info = new Dictionary<RelatorioEnum, RelatorioInfo>();
        public RelatoriosInfo(){            
            info.Add(RelatorioEnum.ExtratoContaCorrente, new RelatorioInfo((int)RelatorioEnum.ExtratoContaCorrente, "ExtratoContaCorrente", "Extrato Conta Corrente"));
            info.Add(RelatorioEnum.GanhoRendaVariavel, new RelatorioInfo((int)RelatorioEnum.GanhoRendaVariavel, "GanhoRendaVariavel", "Ganho Renda Variável"));
            info.Add(RelatorioEnum.MapaOperacaoBMF, new RelatorioInfo((int)RelatorioEnum.MapaOperacaoBMF, "MapaOperacaoBMF", "Mapa Operação BMF"));
            info.Add(RelatorioEnum.MapaOperacaoBolsa, new RelatorioInfo((int)RelatorioEnum.MapaOperacaoBolsa, "MapaOperacaoBolsa", "Mapa Operação Bolsa"));
            info.Add(RelatorioEnum.MapaResultado, new RelatorioInfo((int)RelatorioEnum.MapaResultado, "MapaResultado", "Mapa Resultado"));
        }
    }*/

}
