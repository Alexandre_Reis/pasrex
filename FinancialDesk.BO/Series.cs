﻿using System;
using Financial.Util;
using System.Collections.Generic;

namespace FinancialDesk.BO.Series
{
    public class Chart
    {
        public List<Category> categories;
        public List<Dataset> datasets;
        public Chart()
        {
            this.categories = new List<Category>();
            this.datasets = new List<Dataset>();
        }
    }

    public class Category
    {
        public string label;
        public Category(string label)
        {
            this.label = label;
        }
    }

    public class Dataset
    {
        public string seriesName;
        public List<Set> sets;
        public Dataset()
        {
            this.sets = new List<Set>();
        }
    }

    public class Set
    {
        public double value;
        public Set(double value)
        {
            this.value = value;
        }
    }
}
