﻿using System;
using System.Collections.Generic;
using System.Text;
using Financial.Fundo;
using Financial.Util;
using Financial.Investidor;
using Financial.Bolsa;
using Financial.Bolsa.Enums;
using FinancialDesk.BO;
using Financial.BMF;
using Financial.RendaFixa;
using Financial.ContaCorrente;
using Financial.BMF.Enums;
using EntitySpaces.Interfaces;
using Financial.Common;
using System.Data;

namespace FinancialDesk.BO
{
    public class RetornoAtivo
    {
        public string IdAtivo;
        public int IdTipo; //TipoMercado
        public string TipoPlural; //Estrategia
        public string Nome; //DescricaoAtivo
        public decimal RetornoMes;
        public decimal RetornoAno;
        public decimal Retorno3Meses;
        public decimal Retorno6Meses;
        public decimal Retorno12Meses;
        public decimal Retorno24Meses;
        public decimal Retorno36Meses;
        public decimal? PercentualAlocacao;
    }

    public class ListaRetornoAtivo
    {
        public List<RetornoAtivo> RetornaListaRetornoAtivo(DataTable datatable)
        {
            List<RetornoAtivo> listaRetornoAtivo = new List<RetornoAtivo>();
            foreach (DataRow row in datatable.Rows)
            {
                RetornoAtivo retornoAtivo = new RetornoAtivo();
                
                retornoAtivo.Nome = (string)row["DescricaoAtivo"];
                retornoAtivo.TipoPlural = (string)row["Estrategia"];
                
                
                if (row["CodigoAtivo"] != System.DBNull.Value)
                {
                    retornoAtivo.IdAtivo = (string)row["CodigoAtivo"];
                }

                if (row["TipoAtivo"] != System.DBNull.Value)
                {
                    retornoAtivo.IdTipo = Convert.ToInt16(row["TipoAtivo"]);
                }

                if (row["RetornoMes"] != System.DBNull.Value)
                {
                    retornoAtivo.RetornoMes = Convert.ToDecimal(row["RetornoMes"]);
                }

                if (row["RetornoAno"] != System.DBNull.Value)
                {
                    retornoAtivo.RetornoAno = Convert.ToDecimal(row["RetornoAno"]);
                }

                if (row["Retorno3Meses"] != System.DBNull.Value)
                {
                    retornoAtivo.Retorno3Meses = Convert.ToDecimal(row["Retorno3Meses"]);
                }

                if (row["Retorno6Meses"] != System.DBNull.Value)
                {
                    retornoAtivo.Retorno6Meses = Convert.ToDecimal(row["Retorno6Meses"]);
                }

                if (row["Retorno12Meses"] != System.DBNull.Value)
                {
                    retornoAtivo.Retorno12Meses = Convert.ToDecimal(row["Retorno12Meses"]);
                }

                if (row["Retorno24Meses"] != System.DBNull.Value)
                {
                    retornoAtivo.Retorno24Meses = Convert.ToDecimal(row["Retorno24Meses"]);
                }

                if (row["Retorno36Meses"] != System.DBNull.Value)
                {
                    retornoAtivo.Retorno36Meses = Convert.ToDecimal(row["Retorno36Meses"]);
                }

                if (row["PercentualAlocacao"] != System.DBNull.Value)
                {
                    retornoAtivo.PercentualAlocacao = Convert.ToDecimal(row["PercentualAlocacao"]);
                }
                else
                {
                    retornoAtivo.PercentualAlocacao = null;
                }

                listaRetornoAtivo.Add(retornoAtivo);
            }

            return listaRetornoAtivo;
        }
    }

    public class ComposicaoFundo
    {
        #region Atributos e properties
        private string idAtivo;
        public string IdAtivo
        {
            get { return idAtivo; }
            set { idAtivo = value; }
        }

        private string nome;
        public string Nome
        {
            get { return nome; }
            set { nome = value; }
        }

        private string tipoPlural;
        public string TipoPlural
        {
            get { return tipoPlural; }
            set { tipoPlural = value; }
        }

        private int idTipo;
        public int IdTipo
        {
            get { return idTipo; }
            set { idTipo = value; }
        }

        private decimal saldoBruto;
        public decimal SaldoBruto
        {
            get { return saldoBruto; }
            set { saldoBruto = value; }
        }

        private int percentualAtivos;
        public int PercentualAtivos
        {
            get { return percentualAtivos; }
            set { percentualAtivos = value; }
        }


        #endregion

        /// <summary>
        /// Constructor para zerar os valores default
        /// </summary>
        public ComposicaoFundo()
        {
            //Seta os valores default
            this.saldoBruto = 0;
        }
    }

    public class CarteiraSintetica
    {
        #region Atributos e properties
        private string idAtivo;
        public string IdAtivo
        {
            get { return idAtivo; }
            set { idAtivo = value; }
        }

        private string nome;
        public string Nome
        {
            get { return nome; }
            set { nome = value; }
        }

        private string tipoPlural;
        public string TipoPlural
        {
            get { return tipoPlural; }
            set { tipoPlural = value; }
        }

        private int idTipo;
        public int IdTipo
        {
            get { return idTipo; }
            set { idTipo = value; }
        }

        private decimal saldoAnterior;
        public decimal SaldoAnterior
        {
            get { return saldoAnterior; }
            set { saldoAnterior = value; }
        }

        private decimal? aplicacoes;
        public decimal? Aplicacoes
        {
            get { return aplicacoes; }
            set { aplicacoes = value; }
        }

        private decimal? resgates;
        public decimal? Resgates
        {
            get { return resgates; }
            set { resgates = value; }
        }

        private decimal impostoPago;
        public decimal ImpostoPago
        {
            get { return impostoPago; }
            set { impostoPago = value; }
        }

        private decimal saldoBruto;
        public decimal SaldoBruto
        {
            get { return saldoBruto; }
            set { saldoBruto = value; }
        }

        
        private decimal provisaoTributo;
        public decimal ProvisaoTributo
        {
            get { return provisaoTributo; }
            set { provisaoTributo = value; }
        }

        private decimal saldoLiquido;
        public decimal SaldoLiquido
        {
            get { return saldoLiquido; }
            set { saldoLiquido = value; }
        }

        #endregion

        /// <summary>
        /// Constructor para zerar os valores default
        /// </summary>
        public CarteiraSintetica()
        {
            //Seta os valores default
            this.saldoAnterior = 0;
            this.aplicacoes = 0;
            this.resgates = 0;
            this.impostoPago = 0;
            this.saldoBruto = 0;
            this.provisaoTributo = 0;
            this.saldoLiquido = 0;
        }
    }

    public class CarteiraOnline
    {
        private string grupoAtivo;
        private string idAtivo;
        private string nome;
        private decimal quantidade;
        private decimal puCusto;
        private decimal valorCusto;
        private decimal puMercado;
        private decimal valorMercado;
        private decimal resultadoRealizar;

        public string GrupoAtivo
        {
            get { return grupoAtivo; }
            set { grupoAtivo = value; }
        }
        public string IdAtivo
        {
            get { return idAtivo; }
            set { idAtivo = value; }
        }
        public string Nome
        {
            get { return nome; }
            set { nome = value; }
        }
        public decimal Quantidade
        {
            get { return quantidade; }
            set { quantidade = value; }
        }
        public decimal PuCusto
        {
            get { return puCusto; }
            set { puCusto = value; }
        }
        public decimal ValorCusto
        {
            get { return valorCusto; }
            set { valorCusto = value; }
        }
        public decimal PuMercado
        {
            get { return puMercado; }
            set { puMercado = value; }
        }
        public decimal ValorMercado
        {
            get { return valorMercado; }
            set { valorMercado = value; }
        }
        public decimal ResultadoRealizar
        {
            get { return resultadoRealizar; }
            set { resultadoRealizar = value; }
        }
    }

    public class ListaCarteiraSintetica
    {
        public List<CarteiraSintetica> RetornaListaCarteiraSintetica(DataTable datatable)
        {
            List<CarteiraSintetica> listaCarteiraSintetica = new List<CarteiraSintetica>();
            decimal saldoAnterior = 0, impostoPago = 0, saldoBruto = 0,
                provisaoTributo = 0, saldoLiquido = 0,
                saldoAnteriorTotal = 0, impostoPagoTotal = 0, saldoBrutoTotal = 0,
                provisaoTributoTotal = 0, saldoLiquidoTotal = 0;

            foreach (DataRow row in datatable.Rows)
            {
                CarteiraSintetica carteiraSintetica = new CarteiraSintetica();

                carteiraSintetica.IdAtivo = Convert.ToString(row["IdAtivo"]);
                carteiraSintetica.Nome = (string)row["DescricaoAtivo"];
                carteiraSintetica.TipoPlural = (string)row["Estrategia"];
                carteiraSintetica.IdTipo = Convert.ToInt16(row["TipoMercado"]);

                saldoAnterior = (decimal)row["SaldoAnterior"];
                carteiraSintetica.SaldoAnterior = saldoAnterior;
                
                carteiraSintetica.Aplicacoes = (decimal)row["Aplicacoes"];
                carteiraSintetica.Resgates = (decimal)row["Resgates"];
                
                impostoPago = (decimal)row["ImpostoPago"];
                carteiraSintetica.ImpostoPago = impostoPago;

                saldoBruto = (decimal)row["SaldoBruto"];
                carteiraSintetica.SaldoBruto = saldoBruto;

                provisaoTributo = (decimal)row["ProvisaoTributo"];
                carteiraSintetica.ProvisaoTributo = provisaoTributo;

                saldoLiquido = (decimal)row["SaldoLiquido"];
                carteiraSintetica.SaldoLiquido = saldoLiquido;

                listaCarteiraSintetica.Add(carteiraSintetica);

                saldoAnteriorTotal += saldoAnterior;
                impostoPagoTotal += impostoPago;
                saldoBrutoTotal += saldoBruto;
                provisaoTributoTotal += provisaoTributo;
                saldoLiquidoTotal += saldoLiquido;

            }

            CarteiraSintetica carteiraSinteticaGeral = new CarteiraSintetica();
            carteiraSinteticaGeral.IdAtivo = "";
            carteiraSinteticaGeral.Nome = "";
            carteiraSinteticaGeral.IdTipo = 0;
            carteiraSinteticaGeral.TipoPlural = "Total Geral";

            carteiraSinteticaGeral.SaldoAnterior = saldoAnteriorTotal;
            carteiraSinteticaGeral.Aplicacoes = null;
            carteiraSinteticaGeral.Resgates = null;
            carteiraSinteticaGeral.ImpostoPago = impostoPagoTotal;
            carteiraSinteticaGeral.SaldoBruto = saldoBrutoTotal;
            carteiraSinteticaGeral.ProvisaoTributo = provisaoTributoTotal;
            carteiraSinteticaGeral.SaldoLiquido = saldoLiquidoTotal;

            listaCarteiraSintetica.Add(carteiraSinteticaGeral);

            return listaCarteiraSintetica;

            //{"IdAtivo":"164","Nome":"CDB_BRADESCO -> Vcto: 03/01/2012 -> 101,0000% CDI","TipoPlural":"Renda Fixa","IdTipo":20,"SaldoAnterior":516315.10,"Entradas":0.0,"Saidas":0.0,"Quantidade":500.00,"PuCusto":1000.000000000000,"ValorCusto":500000.00,"PuMercado":1033.086002960000,"ValorBruto":516543.00,"ValorLiquido":512820.82,"PercentualPL":13.93},

            

            /* DEPRECATED -------> Usando codigo do Subreport PosicaoAtivos
            DateTime dataAnterior = Calendario.SubtraiDiaUtil(data, 1);

            Cliente cliente = new Cliente();
            cliente.LoadByPrimaryKey(idCliente);

            decimal valorPL = 0;
            HistoricoCota historicoCota = new HistoricoCota();
            historicoCota.BuscaValorPatrimonioDia(idCliente, data);
            valorPL = historicoCota.PLFechamento.Value;

            PosicaoBolsaCollection posicaoBolsaCollection = new PosicaoBolsaCollection();
            PosicaoTermoBolsaCollection posicaoTermoBolsaCollection = new PosicaoTermoBolsaCollection();
            PosicaoBMFCollection posicaoBMFCollection = new PosicaoBMFCollection();
            PosicaoRendaFixaCollection posicaoRendaFixaCollection = new PosicaoRendaFixaCollection();
            PosicaoFundoCollection posicaoFundoCollection = new PosicaoFundoCollection();
            Liquidacao liquidacao = new Liquidacao();
            SaldoCaixa saldoCaixa = new SaldoCaixa();
            EstruturaTipos estruturaTipos = new EstruturaTipos();

            if (DateTime.Compare(cliente.DataDia.Value, data) <= 0)
            {
                posicaoBolsaCollection.BuscaPosicaoBolsaAgrupado(idCliente);
                posicaoTermoBolsaCollection.BuscaPosicaoTermoBolsaAgrupado(idCliente);
                posicaoBMFCollection.BuscaPosicaoBMFAgrupado(idCliente);
                posicaoRendaFixaCollection.BuscaPosicaoRendaFixaAgrupado(idCliente);
                posicaoFundoCollection.BuscaPosicaoFundoAgrupado(idCliente);
            }
            else
            {
                PosicaoBolsaHistoricoCollection posicaoBolsaHistoricoCollection = new PosicaoBolsaHistoricoCollection();
                PosicaoTermoBolsaHistoricoCollection posicaoTermoBolsaHistoricoCollection = new PosicaoTermoBolsaHistoricoCollection();
                PosicaoBMFHistoricoCollection posicaoBMFHistoricoCollection = new PosicaoBMFHistoricoCollection();
                PosicaoRendaFixaHistoricoCollection posicaoRendaFixaHistoricoCollection = new PosicaoRendaFixaHistoricoCollection();
                PosicaoFundoHistoricoCollection posicaoFundoHistoricoCollection = new PosicaoFundoHistoricoCollection();
                LiquidacaoHistoricoCollection liquidacaoHistoricoCollection = new LiquidacaoHistoricoCollection();

                posicaoBolsaHistoricoCollection.BuscaPosicaoBolsaAgrupado(idCliente, data);
                posicaoTermoBolsaHistoricoCollection.BuscaPosicaoTermoBolsaAgrupado(idCliente, data);
                posicaoBMFHistoricoCollection.BuscaPosicaoBMFAgrupado(idCliente, data);
                posicaoRendaFixaHistoricoCollection.BuscaPosicaoRendaFixaAgrupado(idCliente, data);
                posicaoFundoHistoricoCollection.BuscaPosicaoFundoAgrupado(idCliente, data);

                posicaoBolsaCollection = new PosicaoBolsaCollection(posicaoBolsaHistoricoCollection);
                posicaoTermoBolsaCollection = new PosicaoTermoBolsaCollection(posicaoTermoBolsaHistoricoCollection);
                posicaoBMFCollection = new PosicaoBMFCollection(posicaoBMFHistoricoCollection);
                posicaoRendaFixaCollection = new PosicaoRendaFixaCollection(posicaoRendaFixaHistoricoCollection);
                posicaoFundoCollection = new PosicaoFundoCollection(posicaoFundoHistoricoCollection);
            }

            List<CarteiraSintetica> listaCarteiraSintetica = new List<CarteiraSintetica>();

            decimal saldoAnteriorTotal = 0;
            decimal totalComprasTotal = 0;
            decimal totalVendasTotal = 0;
            decimal valorBrutoTotal = 0;
            decimal valorLiquidoTotal = 0;
            decimal valorCustoTotal = 0;
            decimal percPLTotal = 0;

            #region PosicaoBolsa
            foreach (PosicaoBolsa posicaoBolsa in posicaoBolsaCollection)
            {
                string cdAtivoBolsa = posicaoBolsa.CdAtivoBolsa;
                decimal quantidade = posicaoBolsa.Quantidade.Value;
                decimal puCusto = posicaoBolsa.PUCustoLiquido.Value;
                decimal puMercado = posicaoBolsa.PUMercado.Value;
                decimal valorMercado = posicaoBolsa.ValorMercado.Value;
                decimal valorCusto = posicaoBolsa.ValorCustoLiquido.Value;

                //Totais de entrada (compra) e saida (venda)
                OperacaoBolsa operacaoBolsa = new OperacaoBolsa();
                decimal totalCompras = operacaoBolsa.RetornaTotalCompras(idCliente, cdAtivoBolsa, data, data);
                decimal totalVendas = operacaoBolsa.RetornaTotalVendas(idCliente, cdAtivoBolsa, data, data);

                //Saldo anterior
                PosicaoBolsaHistorico posicaoBolsaHistorico = new PosicaoBolsaHistorico();
                decimal saldoAnterior = posicaoBolsaHistorico.RetornaValorMercadoSum(idCliente, cdAtivoBolsa, dataAnterior);

                //Perc. do PL
                decimal percPL = 0;
                if (valorPL == 0)
                {
                    percPL = 0;
                }
                else
                {
                    percPL = Math.Round(valorMercado / valorPL * 100, 2);
                }

                CarteiraSintetica carteiraSintetica = new CarteiraSintetica();
                carteiraSintetica.IdAtivo = cdAtivoBolsa;
                carteiraSintetica.Nome = cdAtivoBolsa;

                if (posicaoBolsa.TipoMercado == TipoMercadoBolsa.MercadoVista)
                {
                    carteiraSintetica.IdTipo = (int)ListaTipoFixa.Acao;
                    carteiraSintetica.TipoPlural = estruturaTipos.listaTiposNomeada[ListaTipoFixa.Acao].Plural;
                }
                else if (posicaoBolsa.TipoMercado == TipoMercadoBolsa.OpcaoCompra ||
                         posicaoBolsa.TipoMercado == TipoMercadoBolsa.OpcaoVenda)
                {
                    carteiraSintetica.IdTipo = (int)ListaTipoFixa.OpcaoBolsa;
                    carteiraSintetica.TipoPlural = estruturaTipos.listaTiposNomeada[ListaTipoFixa.OpcaoBolsa].Plural;
                }

                carteiraSintetica.SaldoAnterior = saldoAnterior;
                carteiraSintetica.Entradas = totalCompras;
                carteiraSintetica.Saidas = totalVendas;
                carteiraSintetica.PuCusto = puCusto;
                carteiraSintetica.PuMercado = puMercado;
                carteiraSintetica.Quantidade = quantidade;
                carteiraSintetica.ValorBruto = valorMercado;
                carteiraSintetica.ValorLiquido = valorMercado;
                carteiraSintetica.ValorCusto = valorCusto;
                carteiraSintetica.PercentualPL = percPL;

                listaCarteiraSintetica.Add(carteiraSintetica);

                saldoAnteriorTotal += saldoAnterior;
                totalComprasTotal += totalCompras;
                totalVendasTotal += totalVendas;
                valorBrutoTotal += valorMercado;
                valorLiquidoTotal += valorMercado;
                valorCustoTotal += valorCusto;
                percPLTotal += percPL;
            }
            #endregion

            #region PosicaoTermoBolsa
            foreach (PosicaoTermoBolsa posicaoTermoBolsa in posicaoTermoBolsaCollection)
            {
                string cdAtivoBolsa = posicaoTermoBolsa.CdAtivoBolsa;

                string cdAtivoBolsaAcao = AtivoBolsa.RetornaCdAtivoBolsaAcao(cdAtivoBolsa);
                string vencimento = cdAtivoBolsa.Replace(cdAtivoBolsaAcao, "").Replace("T", "");
                vencimento = vencimento.Substring(0, 2) + "/" + vencimento.Substring(3, 2) + "/" + vencimento.Substring(4, 4);
                string cdAtivoBolsaDescricao = cdAtivoBolsaAcao + "T - " + vencimento;

                decimal quantidade = posicaoTermoBolsa.Quantidade.Value;
                decimal puCusto = posicaoTermoBolsa.PUTermoLiquido.Value;
                decimal puMercado = posicaoTermoBolsa.PUMercado.Value;
                decimal valorMercado = posicaoTermoBolsa.ValorMercado.Value;
                decimal valorCusto = posicaoTermoBolsa.ValorTermoLiquido.Value;

                //Totais de entrada (compra) e saida (venda)
                OperacaoBolsa operacaoBolsa = new OperacaoBolsa();
                decimal totalCompras = operacaoBolsa.RetornaTotalCompras(idCliente, cdAtivoBolsa, data, data);
                decimal totalVendas = operacaoBolsa.RetornaTotalVendas(idCliente, cdAtivoBolsa, data, data);

                //Saldo anterior
                PosicaoTermoBolsaHistorico posicaoTermoBolsaHistorico = new PosicaoTermoBolsaHistorico();
                decimal saldoAnterior = posicaoTermoBolsaHistorico.RetornaValorMercadoSum(idCliente, cdAtivoBolsa, dataAnterior);

                //Perc. do PL
                decimal percPL = 0;
                if (valorPL == 0)
                {
                    percPL = 0;
                }
                else
                {
                    percPL = Math.Round(valorMercado / valorPL * 100, 2);
                }

                CarteiraSintetica carteiraSintetica = new CarteiraSintetica();
                carteiraSintetica.IdAtivo = cdAtivoBolsa;
                carteiraSintetica.Nome = cdAtivoBolsaDescricao;

                carteiraSintetica.IdTipo = (int)ListaTipoFixa.TermoBolsa;
                carteiraSintetica.TipoPlural = estruturaTipos.listaTiposNomeada[ListaTipoFixa.TermoBolsa].Plural;

                carteiraSintetica.SaldoAnterior = saldoAnterior;
                carteiraSintetica.Entradas = totalCompras;
                carteiraSintetica.Saidas = totalVendas;
                carteiraSintetica.PuCusto = puCusto;
                carteiraSintetica.PuMercado = puMercado;
                carteiraSintetica.Quantidade = quantidade;
                carteiraSintetica.ValorBruto = valorMercado;
                carteiraSintetica.ValorLiquido = valorMercado;
                carteiraSintetica.ValorCusto = valorCusto;
                carteiraSintetica.PercentualPL = percPL;

                listaCarteiraSintetica.Add(carteiraSintetica);

                saldoAnteriorTotal += saldoAnterior;
                totalComprasTotal += totalCompras;
                totalVendasTotal += totalVendas;
                valorBrutoTotal += valorMercado;
                valorLiquidoTotal += valorMercado;
                valorCustoTotal += valorCusto;
                percPLTotal += percPL;
            }
            #endregion

            #region PosicaoBMF
            foreach (PosicaoBMF posicaoBMF in posicaoBMFCollection)
            {
                string cdAtivo = posicaoBMF.CdAtivoBMF + posicaoBMF.Serie;
                decimal quantidade = posicaoBMF.Quantidade.Value;
                decimal puCusto = posicaoBMF.PUCustoLiquido.Value;
                decimal puMercado = posicaoBMF.PUMercado.Value;
                decimal valorMercado = 0;
                decimal valorCusto = 0;

                //Totais de entrada (compra) e saida (venda) em quantidade
                OperacaoBMF operacaoBMF = new OperacaoBMF();
                decimal totalCompras = operacaoBMF.RetornaTotalQuantidadeCompra(idCliente, posicaoBMF.CdAtivoBMF, posicaoBMF.Serie, data, data);
                decimal totalVendas = operacaoBMF.RetornaTotalQuantidadeVenda(idCliente, posicaoBMF.CdAtivoBMF, posicaoBMF.Serie, data, data);

                //Saldo anterior
                PosicaoBMFHistorico posicaoBMFHistorico = new PosicaoBMFHistorico();
                //decimal saldoAnterior = posicaoBMFHistorico.RetornaValorMercadoSum(idCliente, cdAtivoBolsa, dataAnterior);
                decimal saldoAnterior = 0;

                //Perc. do PL
                decimal percPL = 0;
                if (valorPL == 0)
                {
                    percPL = 0;
                }
                else
                {
                    percPL = Math.Round(valorMercado / valorPL * 100, 2);
                }

                CarteiraSintetica carteiraSintetica = new CarteiraSintetica();
                carteiraSintetica.IdAtivo = cdAtivo;
                carteiraSintetica.Nome = cdAtivo;

                if (posicaoBMF.TipoMercado == (byte)TipoMercadoBMF.Futuro)
                {
                    valorMercado = 0;
                    valorCusto = 0;
                    carteiraSintetica.IdTipo = (int)ListaTipoFixa.FuturoBMF;
                    carteiraSintetica.TipoPlural = estruturaTipos.listaTiposNomeada[ListaTipoFixa.FuturoBMF].Plural;
                }
                else if (posicaoBMF.TipoMercado == (byte)TipoMercadoBMF.OpcaoDisponivel ||
                         posicaoBMF.TipoMercado == (byte)TipoMercadoBMF.OpcaoFuturo)
                {
                    valorMercado = posicaoBMF.ValorMercado.Value;
                    valorCusto = posicaoBMF.ValorCustoLiquido.Value;
                    carteiraSintetica.IdTipo = (int)ListaTipoFixa.OpcaoBMF;
                    carteiraSintetica.TipoPlural = estruturaTipos.listaTiposNomeada[ListaTipoFixa.OpcaoBMF].Plural;
                }

                carteiraSintetica.SaldoAnterior = saldoAnterior;
                carteiraSintetica.Entradas = totalCompras;
                carteiraSintetica.Saidas = totalVendas;
                carteiraSintetica.PuCusto = puCusto;
                carteiraSintetica.PuMercado = puMercado;
                carteiraSintetica.Quantidade = quantidade;
                carteiraSintetica.ValorBruto = valorMercado;
                carteiraSintetica.ValorLiquido = valorMercado;
                carteiraSintetica.ValorCusto = valorCusto;
                carteiraSintetica.PercentualPL = percPL;

                listaCarteiraSintetica.Add(carteiraSintetica);

                saldoAnteriorTotal += saldoAnterior;
                totalComprasTotal += totalCompras;
                totalVendasTotal += totalVendas;
                valorBrutoTotal += valorMercado;
                valorLiquidoTotal += valorMercado;
                valorCustoTotal += valorCusto;
                percPLTotal += percPL;
            }
            #endregion

            #region PosicaoRendaFixa
            foreach (PosicaoRendaFixa posicaoRendaFixa in posicaoRendaFixaCollection)
            {
                int idTitulo = posicaoRendaFixa.IdTitulo.Value;
                TituloRendaFixa tituloRendaFixa = new TituloRendaFixa();
                tituloRendaFixa.LoadByPrimaryKey(idTitulo);

                string modificadorIndice = "";
                decimal? idIndice = tituloRendaFixa.IdIndice;
                string descricaoIndice = "";
                if (idIndice.HasValue)
                {
                    Indice indice = new Indice();
                    indice.LoadByPrimaryKey((short)idIndice.Value);
                    descricaoIndice = indice.Descricao.Trim();
                }
                string percIndice = tituloRendaFixa.Percentual.HasValue ? tituloRendaFixa.Percentual.Value.ToString().Trim() : "";
                modificadorIndice = " -> " + percIndice + "%" + " " + descricaoIndice;

                string cdAtivo = tituloRendaFixa.Descricao + " -> Vcto: " + tituloRendaFixa.DataVencimento.Value.ToShortDateString()
                                                           + modificadorIndice;

                decimal quantidade = posicaoRendaFixa.Quantidade.Value;
                decimal puCusto = posicaoRendaFixa.PUOperacao.Value;
                decimal puMercado = posicaoRendaFixa.PUMercado.Value;
                decimal valorMercado = posicaoRendaFixa.ValorMercado.Value;
                decimal valorTributos = posicaoRendaFixa.ValorIR.Value + posicaoRendaFixa.ValorIOF.Value;
                decimal valorLiquido = valorMercado - valorTributos;
                decimal valorCusto = Math.Round(puCusto * quantidade, 2);

                //Totais de entrada (compra) e saida (venda)
                OperacaoRendaFixa operacaoRendaFixa = new OperacaoRendaFixa();
                decimal totalCompras = operacaoRendaFixa.RetornaTotalCompras(idCliente, idTitulo, data, data);
                decimal totalVendas = operacaoRendaFixa.RetornaTotalVendas(idCliente, idTitulo, data, data);

                //Saldo anterior
                PosicaoRendaFixaHistorico posicaoRendaFixaHistorico = new PosicaoRendaFixaHistorico();
                decimal saldoAnterior = posicaoRendaFixaHistorico.RetornaValorMercado(idCliente, dataAnterior, idTitulo);

                //Perc. do PL
                decimal percPL = 0;
                if (valorPL == 0)
                {
                    percPL = 0;
                }
                else
                {
                    percPL = Math.Round(valorMercado / valorPL * 100, 2);
                }

                CarteiraSintetica carteiraSintetica = new CarteiraSintetica();
                carteiraSintetica.IdAtivo = idTitulo.ToString();
                carteiraSintetica.Nome = cdAtivo;

                carteiraSintetica.IdTipo = (int)ListaTipoFixa.RendaFixa;
                carteiraSintetica.TipoPlural = estruturaTipos.listaTiposNomeada[ListaTipoFixa.RendaFixa].Plural;

                carteiraSintetica.SaldoAnterior = saldoAnterior;
                carteiraSintetica.Entradas = totalCompras;
                carteiraSintetica.Saidas = totalVendas;
                carteiraSintetica.PuCusto = puCusto;
                carteiraSintetica.PuMercado = puMercado;
                carteiraSintetica.Quantidade = quantidade;
                carteiraSintetica.ValorBruto = valorMercado;
                carteiraSintetica.ValorLiquido = valorLiquido;
                carteiraSintetica.ValorCusto = valorCusto;
                carteiraSintetica.PercentualPL = percPL;

                listaCarteiraSintetica.Add(carteiraSintetica);

                saldoAnteriorTotal += saldoAnterior;
                totalComprasTotal += totalCompras;
                totalVendasTotal += totalVendas;
                valorBrutoTotal += valorMercado;
                valorLiquidoTotal += valorLiquido;
                valorCustoTotal += valorCusto;
                percPLTotal += percPL;
            }
            #endregion

            #region PosicaoFundo
            foreach (PosicaoFundo posicaoFundo in posicaoFundoCollection)
            {
                int idCarteira = posicaoFundo.IdCarteira.Value;
                Carteira carteira = new Carteira();
                carteira.LoadByPrimaryKey(idCarteira);
                string nomeCarteira = carteira.Apelido;

                decimal quantidade = posicaoFundo.Quantidade.Value;
                decimal puCusto = posicaoFundo.CotaAplicacao.Value;
                decimal puMercado = posicaoFundo.CotaDia.Value;
                decimal valorBruto = posicaoFundo.ValorBruto.Value;
                decimal valorLiquido = posicaoFundo.ValorLiquido.Value;
                decimal valorCusto = posicaoFundo.ValorAplicacao.Value;

                //Totais de entrada (compra) e saida (venda)
                OperacaoFundo operacaoFundo = new OperacaoFundo();
                decimal totalCompras = operacaoFundo.RetornaTotalAplicacao(idCliente, idCarteira, data, data);
                decimal totalVendas = operacaoFundo.RetornaTotalResgate(idCliente, idCarteira, data, data);

                //Saldo anterior
                PosicaoFundoHistorico posicaoFundoHistorico = new PosicaoFundoHistorico();
                decimal saldoAnterior = posicaoFundoHistorico.RetornaSumValorBruto(idCarteira, idCliente, dataAnterior);

                //Perc. do PL
                decimal percPL = 0;
                if (valorPL == 0)
                {
                    percPL = 0;
                }
                else
                {
                    percPL = Math.Round(valorBruto / valorPL * 100, 2);
                }

                CarteiraSintetica carteiraSintetica = new CarteiraSintetica();
                carteiraSintetica.IdAtivo = idCarteira.ToString();
                carteiraSintetica.Nome = nomeCarteira;

                carteiraSintetica.IdTipo = (int)ListaTipoFixa.CotaInvestimento;
                carteiraSintetica.TipoPlural = estruturaTipos.listaTiposNomeada[ListaTipoFixa.CotaInvestimento].Plural;

                carteiraSintetica.SaldoAnterior = saldoAnterior;
                carteiraSintetica.Entradas = totalCompras;
                carteiraSintetica.Saidas = totalVendas;
                carteiraSintetica.PuCusto = puCusto;
                carteiraSintetica.PuMercado = puMercado;
                carteiraSintetica.Quantidade = quantidade;
                carteiraSintetica.ValorBruto = valorBruto;
                carteiraSintetica.ValorLiquido = valorLiquido;
                carteiraSintetica.ValorCusto = valorCusto;
                carteiraSintetica.PercentualPL = percPL;

                listaCarteiraSintetica.Add(carteiraSintetica);

                saldoAnteriorTotal += saldoAnterior;
                totalComprasTotal += totalCompras;
                totalVendasTotal += totalVendas;
                valorBrutoTotal += valorBruto;
                valorLiquidoTotal += valorLiquido;
                valorCustoTotal += valorCusto;
                percPLTotal += percPL;
            }
            #endregion

            #region Liquidacao
            LiquidacaoHistorico liquidacaoHistorico = new LiquidacaoHistorico();

            decimal valorLiquidacao = 0;
            if (DateTime.Compare(cliente.DataDia.Value, data) <= 0)
            {
                valorLiquidacao = liquidacao.RetornaLiquidacaoVencimentoMaior(idCliente, data, data);
            }
            else
            {
                valorLiquidacao = liquidacaoHistorico.RetornaLiquidacaoHistorico(idCliente, data);
            }

            decimal valorLiquidacaoAnterior = liquidacaoHistorico.RetornaLiquidacaoHistorico(idCliente, dataAnterior);

            //Perc. do PL
            decimal percPL_Liquidacao = 0;
            if (valorPL == 0)
            {
                percPL_Liquidacao = 0;
            }
            else
            {
                percPL_Liquidacao = Math.Round(valorLiquidacao / valorPL * 100, 2);
            }            

            CarteiraSintetica carteiraSintetica_CC = new CarteiraSintetica();
            carteiraSintetica_CC.IdAtivo = "liquidar";
            carteiraSintetica_CC.Nome = "Valores a Liquidar";

            carteiraSintetica_CC.IdTipo = (int)ListaTipoFixa.Liquidar;
            carteiraSintetica_CC.TipoPlural = estruturaTipos.listaTiposNomeada[ListaTipoFixa.Liquidar].Plural;

            carteiraSintetica_CC.SaldoAnterior = valorLiquidacaoAnterior;
            carteiraSintetica_CC.Entradas = 0;
            carteiraSintetica_CC.Saidas = 0;
            carteiraSintetica_CC.PuCusto = 0;
            carteiraSintetica_CC.PuMercado = 0;
            carteiraSintetica_CC.Quantidade = 0;
            carteiraSintetica_CC.ValorBruto = valorLiquidacao;
            carteiraSintetica_CC.ValorLiquido = valorLiquidacao;
            carteiraSintetica_CC.ValorCusto = valorLiquidacao;
            carteiraSintetica_CC.PercentualPL = percPL_Liquidacao;

            listaCarteiraSintetica.Add(carteiraSintetica_CC);

            saldoAnteriorTotal += valorLiquidacaoAnterior;
            totalComprasTotal += 0;
            totalVendasTotal += 0;
            valorBrutoTotal += valorLiquidacao;
            valorLiquidoTotal += valorLiquidacao;
            valorCustoTotal += valorLiquidacao;
            percPLTotal += percPL_Liquidacao;
            #endregion

            #region SaldoCaixa
            decimal saldoCC = 0;
            saldoCaixa.BuscaSaldoCaixa(idCliente, data);
            if (saldoCaixa.SaldoFechamento.HasValue)
            {
                saldoCC = saldoCaixa.SaldoFechamento.Value;
            }

            decimal saldoCCAnterior = 0;
            saldoCaixa = new SaldoCaixa();
            saldoCaixa.BuscaSaldoCaixa(idCliente, dataAnterior);
            if (saldoCaixa.SaldoFechamento.HasValue)
            {
                saldoCCAnterior = saldoCaixa.SaldoFechamento.Value;
            }

            Liquidacao liquidacaoDia = new Liquidacao();
            decimal entradaCC = liquidacaoDia.RetornaValor(idCliente, data, Financial.ContaCorrente.Enums.SinalValorLiquidacao.Entrada);
            decimal saidaCC = liquidacaoDia.RetornaValor(idCliente, data, Financial.ContaCorrente.Enums.SinalValorLiquidacao.Retirada);

            //Perc. do PL
            decimal percPL_CC = 0;
            if (valorPL == 0)
            {
                percPL_CC = 0;
            }
            else
            {
                percPL_CC = Math.Round(saldoCC / valorPL * 100, 2);
            }
            
            CarteiraSintetica carteiraSintetica_Liquidacao = new CarteiraSintetica();
            carteiraSintetica_Liquidacao.IdAtivo = "saldoCC";
            carteiraSintetica_Liquidacao.Nome = "Saldo CC";

            carteiraSintetica_Liquidacao.IdTipo = (int)ListaTipoFixa.Caixa;
            carteiraSintetica_Liquidacao.TipoPlural = estruturaTipos.listaTiposNomeada[ListaTipoFixa.Caixa].Plural;

            carteiraSintetica_Liquidacao.SaldoAnterior = saldoCCAnterior;
            carteiraSintetica_Liquidacao.Entradas = entradaCC;
            carteiraSintetica_Liquidacao.Saidas = saidaCC;
            carteiraSintetica_Liquidacao.PuCusto = 0;
            carteiraSintetica_Liquidacao.PuMercado = 0;
            carteiraSintetica_Liquidacao.Quantidade = 0;
            carteiraSintetica_Liquidacao.ValorBruto = saldoCC;
            carteiraSintetica_Liquidacao.ValorLiquido = saldoCC;
            carteiraSintetica_Liquidacao.ValorCusto = saldoCC;
            carteiraSintetica_Liquidacao.PercentualPL = percPL_CC;

            listaCarteiraSintetica.Add(carteiraSintetica_Liquidacao);

            saldoAnteriorTotal += saldoCCAnterior;
            totalComprasTotal += entradaCC;
            totalVendasTotal += saidaCC;
            valorBrutoTotal += saldoCC;
            valorLiquidoTotal += saldoCC;
            valorCustoTotal += saldoCC;
            percPLTotal += percPL_CC;
            #endregion

            #region Total geral
            CarteiraSintetica carteiraSinteticaGeral = new CarteiraSintetica();
            carteiraSinteticaGeral.IdAtivo = "";
            carteiraSinteticaGeral.Nome = "";
            carteiraSinteticaGeral.IdTipo = 0;
            carteiraSinteticaGeral.TipoPlural = "Total Geral";

            carteiraSinteticaGeral.SaldoAnterior = saldoAnteriorTotal;
            carteiraSinteticaGeral.Entradas = totalComprasTotal;
            carteiraSinteticaGeral.Saidas = totalVendasTotal;
            carteiraSinteticaGeral.PuCusto = 0;
            carteiraSinteticaGeral.PuMercado = 0;
            carteiraSinteticaGeral.Quantidade = 0;
            carteiraSinteticaGeral.ValorBruto = valorBrutoTotal;
            carteiraSinteticaGeral.ValorLiquido = valorLiquidoTotal;
            carteiraSinteticaGeral.ValorCusto = valorCustoTotal;
            carteiraSinteticaGeral.PercentualPL = percPLTotal;

            listaCarteiraSintetica.Add(carteiraSinteticaGeral);
            
            return listaCarteiraSintetica;
            #endregion
             */ 
        }
    }

    public class ListaComposicaoFundo
    {
        public List<ComposicaoFundo> RetornaListaComposicaoFundo(DataTable datatable)
        {
            List<ComposicaoFundo> listaComposicaoFundo = new List<ComposicaoFundo>();
            decimal saldoBruto = 0,
                saldoBrutoTotal = 0;

            foreach (DataRow row in datatable.Rows)
            {
                ComposicaoFundo composicaoFundo = new ComposicaoFundo();

                composicaoFundo.IdAtivo = Convert.ToString(row["IdAtivo"]);
                composicaoFundo.Nome = (string)row["DescricaoAtivo"];
                composicaoFundo.TipoPlural = (string)row["Estrategia"];
                composicaoFundo.IdTipo = Convert.ToInt16(row["TipoMercado"]);
                composicaoFundo.PercentualAtivos = Convert.ToInt16(row["PercentualAtivos"]);

                saldoBruto = (decimal)row["SaldoBruto"];
                composicaoFundo.SaldoBruto = saldoBruto;

                listaComposicaoFundo.Add(composicaoFundo);

                saldoBrutoTotal += saldoBruto;

            }

            ComposicaoFundo composicaoFundoGeral = new ComposicaoFundo();
            composicaoFundoGeral.IdAtivo = "";
            composicaoFundoGeral.Nome = "";
            composicaoFundoGeral.IdTipo = 0;
            composicaoFundoGeral.TipoPlural = "Total Geral";

            composicaoFundoGeral.SaldoBruto = saldoBrutoTotal;
            composicaoFundoGeral.PercentualAtivos = 100;
            
            listaComposicaoFundo.Add(composicaoFundoGeral);

            return listaComposicaoFundo;
        }
    }

    public class ListaCarteiraOnline
    {
        public List<CarteiraOnline> RetornaListaCarteiraOnline(int idCliente)
        {
            List<CarteiraOnline> lista = new List<CarteiraOnline>();

            PosicaoBolsaQuery posicaoBolsaQuery = new PosicaoBolsaQuery("P");
            AtivoBolsaQuery ativoBolsaQuery = new AtivoBolsaQuery("A");
            EstrategiaQuery estrategiaQuery = new EstrategiaQuery("E");

            posicaoBolsaQuery.Select(estrategiaQuery.Descricao,
                                    posicaoBolsaQuery.CdAtivoBolsa,
                                    posicaoBolsaQuery.Quantidade,
                                    posicaoBolsaQuery.PUCustoLiquido,
                                    posicaoBolsaQuery.PUMercado,
                                    posicaoBolsaQuery.ValorMercado,
                                    posicaoBolsaQuery.ValorCustoLiquido,
                                    posicaoBolsaQuery.ResultadoRealizar);
            posicaoBolsaQuery.InnerJoin(ativoBolsaQuery).On(ativoBolsaQuery.CdAtivoBolsa == posicaoBolsaQuery.CdAtivoBolsa);
            posicaoBolsaQuery.InnerJoin(estrategiaQuery).On(estrategiaQuery.IdEstrategia == ativoBolsaQuery.IdEstrategia);
            posicaoBolsaQuery.Where(posicaoBolsaQuery.IdCliente.Equal(idCliente),
                                    posicaoBolsaQuery.Quantidade.NotEqual(0));
            posicaoBolsaQuery.OrderBy(estrategiaQuery.Descricao.Ascending, posicaoBolsaQuery.CdAtivoBolsa.Ascending);
            
            PosicaoBolsaCollection posicaoBolsaCollection = new PosicaoBolsaCollection();
            posicaoBolsaCollection.Load(posicaoBolsaQuery);

            foreach (PosicaoBolsa posicaoBolsa in posicaoBolsaCollection)
            {
                CarteiraOnline carteiraOnline = new CarteiraOnline();
                carteiraOnline.GrupoAtivo = Convert.ToString(posicaoBolsa.GetColumn(EstrategiaMetadata.ColumnNames.Descricao)).Trim();
                carteiraOnline.IdAtivo = posicaoBolsa.CdAtivoBolsa;
                carteiraOnline.Nome = posicaoBolsa.CdAtivoBolsa;
                carteiraOnline.Quantidade= posicaoBolsa.Quantidade.Value;
                carteiraOnline.PuCusto = posicaoBolsa.PUCustoLiquido.Value;
                carteiraOnline.PuMercado = posicaoBolsa.PUMercado.Value;
                carteiraOnline.ValorMercado = posicaoBolsa.ValorMercado.Value;
                carteiraOnline.ValorCusto = posicaoBolsa.ValorCustoLiquido.Value;
                carteiraOnline.ResultadoRealizar = posicaoBolsa.ResultadoRealizar.Value;

                lista.Add(carteiraOnline);
            }

            return lista;
        }
    }
}
