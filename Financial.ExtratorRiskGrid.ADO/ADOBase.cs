﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

namespace Financial.ExtratorRiskGrid.ADO
{
    public class ADOBase
    {
        #region Enum com os tipos de Banco
        public enum TipoBanco
        {
            SQLServer = 0,
            Oracle = 1,
            OLDB = 2
        }
        #endregion

        #region Enum com os Projetos
        public enum Projeto
        {
            ExtratorRiskGrid = 0,
            ExtratorRiskGridSAC = 1,
            NetBol = 2,
            NetReport = 3,
            FOL = 4,
            None,
            Teste
        }
        #endregion


        #region Variáveis Globais

        protected string _strConnStringSQL = string.Empty;
        //protected string strConnStringOracle = string.Empty;
        //protected string strConnStringOLDB = string.Empty;

        #endregion

        #region Construtor

        //public ADOBase(Projeto projeto_)
        //{
        //    switch (projeto_)
        //    {
        //        case Projeto.NetBol:
        //            break;
        //        case Projeto.NetReport:
        //            break;
        //        default:
        //            break;
        //    }
        //}

        public ADOBase()
        {
            //_strConnStringSQL = ConfigurationManager.ConnectionStrings["SAC"].ConnectionString;
            //_strConnStringSQL = ConfigurationManager.ConnectionStrings["Teste"].ConnectionString;
        }
        #endregion 

        /// <summary>
        /// 
        /// </summary>
        /// <param name="Data"></param>
        /// <returns></returns>
        public static string formatadataSQL(DateTime Data)
        {
            try
            {
                return string.Format("'{0}'", Data.ToString("yyyyMMdd"));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #region Método Genérico de Execução de Query
        /// <summary>
        /// Método genérico que invoca os métodos correspondentes ao Tipo do Banco para execução do comando.
        /// </summary>
        /// <param name="query"></param>
        /// <param name="tipoBanco"></param>
        /// <returns>Se o número de linhas afetadas for maior que 0 retorna TRUE</returns>
        public bool ExecutarComando(string query, TipoBanco tipoBanco)
        {
            bool bRet = false;

            try
            {
                switch (tipoBanco)
                {
                    case TipoBanco.SQLServer:
                        bRet = ExecutarQuerySQL(query);
                        break;
                    case TipoBanco.Oracle:
                        bRet = true;
                        break;
                    case TipoBanco.OLDB:
                        break;
                    default:
                        break;
                }
            }
            catch (Exception ex)
            {
                bRet = false;
                throw ex;
            }
            finally
            {

            }
            return bRet;
        }
        #endregion

        #region Método Genérico de Execução de Query
        /// <summary>
        /// Método genérico que invoca os métodos correspondentes ao Tipo do Banco para execução do comando.
        /// </summary>
        /// <param name="query"></param>
        /// <param name="tipoBanco"></param>
        /// <returns>Se o número de linhas afetadas for maior que 0 retorna TRUE</returns>
        public bool ExecutarComando(string query, Projeto projeto)
        {
            bool bRet = false;

            try
            {
                if (projeto == Projeto.ExtratorRiskGridSAC)
                    _strConnStringSQL = ConfigurationManager.ConnectionStrings["ExtratorRiskGridSAC"].ConnectionString;
                else
                {

                    _strConnStringSQL = ConfigurationManager.ConnectionStrings["ExtratorRiskGridCOT"].ConnectionString;
                }


                if (projeto == Projeto.Teste)
                    _strConnStringSQL = ConfigurationManager.ConnectionStrings["Teste"].ConnectionString;



                bRet = ExecutarQuerySQL(query);
            }
            catch (Exception ex)
            {
                bRet = false;
                throw ex;
            }
            finally
            {

            }
            return bRet;
        }
        #endregion

        #region Execução de Query no Banco SQL
        /// <summary>
        /// Método responsável por executar o Comando passado por parâmetro, no Banco SQL Server
        /// </summary>
        /// <param name="query"></param>
        /// <returns>Se o número de linhas afetadas for maior que 0 retorna TRUE</returns>
        private bool ExecutarQuerySQL(string query)
        {
            bool bRet = false;
            SqlConnection sqlConn = null;
            SqlCommand cmd = null;
            try
            {
                using (sqlConn = new SqlConnection(_strConnStringSQL))
                {
                    using (cmd = new SqlCommand(query, sqlConn))
                    {
                        sqlConn.Open();
                        cmd.CommandType = System.Data.CommandType.Text;
                        cmd.CommandTimeout = 60000;
                        if (cmd.ExecuteNonQuery() > 0)
                            bRet = true;
                        else
                            bRet = false;
                    }
                }
            }
            catch (Exception ex)
            {
                bRet = false;
                throw ex;
            }
            finally
            {
                if (sqlConn.State != System.Data.ConnectionState.Closed)
                    sqlConn.Close();

                if (cmd != null)
                    cmd.Dispose();

            }
            return bRet;

        }

        public bool RealizarComandoSQL(string query)
        {
            bool bRet = false;
            SqlConnection sqlConn = null;
            SqlCommand cmd = null;
            try
            {
                using (sqlConn = new SqlConnection(_strConnStringSQL))
                {
                    using (cmd = new SqlCommand(query, sqlConn))
                    {
                        sqlConn.Open();
                        cmd.CommandType = System.Data.CommandType.Text;
                        //cmd.CommandTimeout = 6000;
                        if (cmd.ExecuteNonQuery() > 0)
                            bRet = true;
                        else
                            bRet = false;
                    }
                }
            }
            catch (Exception ex)
            {
                bRet = false;
                throw ex;
            }
            finally
            {
                if (sqlConn.State != System.Data.ConnectionState.Closed)
                    sqlConn.Close();

                if (cmd != null)
                    cmd.Dispose();
            }
            return bRet;

        }
        #endregion



        #region Método Genérico de Execução de Select
        /// <summary>
        /// Método genérico de Consulta.
        /// </summary>
        /// <param name="query"></param>
        /// <param name="tipoBanco"></param>
        /// <returns>'List de Objetos' com o resultado do Select</returns>
        public List<T> ExecutarQueryPesquisa<T>(T objeto, string query, Projeto projeto)
        {
            DataTable dt = null;
            List<T> lstCarteiras = null;
            string colunaObjeto = string.Empty;
            string colunaDataTable = string.Empty;

            try
            {
                if (projeto == Projeto.ExtratorRiskGridSAC)
                    _strConnStringSQL = ConfigurationManager.ConnectionStrings["SAC"].ConnectionString;
                else
                    if (projeto == Projeto.FOL)
                        _strConnStringSQL = EntitySpaces.Interfaces.esConfigSettings.ConnectionInfo.Connections["Financial_Interno"].ConnectionString;
                if (projeto == Projeto.Teste)
                    _strConnStringSQL = ConfigurationManager.ConnectionStrings["Teste"].ConnectionString;


                dt = ExecutarPesquisaSQL(query);

                if (dt != null && dt.Rows.Count > 0)
                {
                    lstCarteiras = new List<T>();

                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        Type t = typeof(T);
                        object obj = Activator.CreateInstance(t);
                        System.Reflection.PropertyInfo[] ps = t.GetProperties();

                        for (int j = 0; j < ps.Length; j++)
                        {
                            if (ps[j].GetCustomAttributes(true).Length > 0)
                            {
                                DbAttribute d = ps[j].GetCustomAttributes(true)[0] as DbAttribute;

                                if (d != null)
                                    if (dt.Columns.Contains(d.Campo))
                                    {
                                        colunaDataTable = dt.Columns[d.Campo].ToString().Trim();
                                        colunaObjeto = d.Campo;

                                        if (colunaObjeto.Contains(colunaDataTable))
                                        {
                                            ps[j].SetValue(obj, dt.Rows[i][colunaDataTable], null);
                                        }
                                    }
                            }
                        }

                        lstCarteiras.Add((T)obj);
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return lstCarteiras;
        }
        #endregion


        #region Método Genérico de Execução de Select
        /// <summary>
        /// Método genérico de Consulta.
        /// </summary>
        /// <param name="query"></param>
        /// <param name="tipoBanco"></param>
        /// <returns>'List de Objetos' com o resultado do Select</returns>
        public DataTable ExecutarQueryPesquisa(string query, Projeto projeto)
        {
            DataTable dt = null;
            string colunaObjeto = string.Empty;
            string colunaDataTable = string.Empty;

            try
            {

                if (projeto == Projeto.ExtratorRiskGridSAC)
                    _strConnStringSQL = ConfigurationManager.ConnectionStrings["ExtratorRiskGridSAC"].ConnectionString;
                else
                    _strConnStringSQL = ConfigurationManager.ConnectionStrings["ExtratorRiskGridCOT"].ConnectionString;


                if (projeto == Projeto.Teste)
                    _strConnStringSQL = ConfigurationManager.ConnectionStrings["Teste"].ConnectionString;

                dt = ExecutarPesquisaSQL(query);

            }
            catch (Exception ex)
            {
                throw ex;
            }

            return dt;
        }
        #endregion

        #region Método responsável por executar o Select no Banco SQL Server
        /// <summary>
        /// Método responsável por executar o Select passado por parâmetro, no Banco SQL Server
        /// </summary>
        /// <param name="query"></param>
        /// <returns>DataTable com o resultado do Select</returns>
        private DataTable ExecutarPesquisaSQL(string query)
        {
            SqlConnection sqlConn = null;
            SqlCommand cmd = null;
            SqlDataAdapter da = null;
            DataTable dt = null;

            try
            {
                using (sqlConn = new SqlConnection(_strConnStringSQL))
                {
                    using (cmd = new SqlCommand(query, sqlConn))
                    {
                        using (da = new SqlDataAdapter(cmd))
                        {
                            using (dt = new DataTable())
                            {
                                cmd.CommandTimeout = 100000;
                                sqlConn.Open();
                                cmd.CommandType = System.Data.CommandType.Text;
                                da.Fill(dt);
                                return dt;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (sqlConn.State != System.Data.ConnectionState.Closed)
                    sqlConn.Close();

                if (cmd != null)
                    cmd.Dispose();
            }
        }
        #endregion

        //#region Método responsável por executar o Select no Banco ORACLE
        ///// <summary>
        ///// Método responsável por executar o Select passado por parâmetro, no Banco ORACLE
        ///// </summary>
        ///// <param name="query"></param>
        ///// <returns>DataTable com o resultado do Select</returns>
        //private DataTable executaPesquisaOracle(string query)
        //{
        //    OracleConnection orclConn = null;
        //    OracleCommand cmd = null;
        //    OracleDataAdapter da = null;
        //    DataTable dt = null;
        //    try
        //    {
        //        using (orclConn = new OracleConnection(strConnStringOracle))
        //        {
        //            using (cmd = new OracleCommand(query, orclConn))
        //            {
        //                using (da = new OracleDataAdapter(cmd))
        //                {
        //                    using (dt = new DataTable())
        //                    {
        //                        orclConn.Open();
        //                        cmd.CommandType = System.Data.CommandType.Text;
        //                        cmd.CommandTimeout = 6000;
        //                        da.Fill(dt);
        //                        return dt;
        //                    }
        //                }
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //    finally
        //    {
        //        if (orclConn.State != System.Data.ConnectionState.Closed)
        //            orclConn.Close();

        //        if (cmd != null)
        //            cmd.Dispose();

        //    }
        //}
        //#endregion

        //#region Método responsável por executar o Select (OLDB)
        ///// <summary>
        ///// Método responsável por executar o Select passado por parâmetro, Conexão OLDB
        ///// </summary>
        ///// <param name="query"></param>
        ///// <returns>DataTable com o resultado do Select</returns>
        //private DataTable executaPesquisaOLDB(string query)
        //{
        //    OleDbConnection OLDBConn = null;
        //    OleDbCommand cmd = null;
        //    OleDbDataAdapter da = null;
        //    DataTable dt = null;
        //    try
        //    {
        //        using (OLDBConn = new OleDbConnection(strConnStringOLDB))
        //        {
        //            using (cmd = new OleDbCommand(query, OLDBConn))
        //            {
        //                using (da = new OleDbDataAdapter(cmd))
        //                {
        //                    using (dt = new DataTable())
        //                    {
        //                        OLDBConn.Open();
        //                        cmd.CommandType = System.Data.CommandType.Text;
        //                        cmd.CommandTimeout = 6000;
        //                        da.Fill(dt);
        //                        return dt;
        //                    }
        //                }
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //    finally
        //    {
        //        if (OLDBConn.State != System.Data.ConnectionState.Closed)
        //        {
        //            OLDBConn.Close();
        //            OLDBConn.Dispose();
        //        }

        //        if (cmd != null)
        //            cmd.Dispose();
        //    }
        //}
        //#endregion

        #region Executa Procedure.
        /// <summary>
        /// Método genérico que chama os métodos correspondentes ao Tipo do Banco para execução da Procedure.
        /// </summary>
        /// <param name="query"></param>
        /// <param name="tipoBanco"></param>
        /// <returns></returns>
        public object ExecutarProc(string nameProc, TipoBanco tipoBanco, object[] parametersValues,
                                string[] parametersNames, SqlDbType[] sqlDBType, ParameterDirection[] direcao)
        {
            object objRet = false;
            try
            {
                switch (tipoBanco)
                {
                    case TipoBanco.SQLServer:
                        objRet = ExecutarProcSQL(nameProc, parametersValues, parametersNames, sqlDBType, direcao);
                        break;
                    case TipoBanco.Oracle:
                        break;
                    case TipoBanco.OLDB:
                        break;
                    default:
                        break;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {

            }
            return objRet;
        }
        #endregion

        #region Executa Procedure no Banco SQL.
        /// <summary>
        /// Método responsável por executar Procedure. Banco SQL
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        private object ExecutarProcSQL(string nameProc, object[] values, string[] parametros, SqlDbType[] sqlDBType, ParameterDirection[] direcao)
        {
            //Precisa colocar o tipo de retorno input ou output
            //Cria um overload.

            _strConnStringSQL = ConfigurationManager.ConnectionStrings["ExtratorRiskGridSAC"].ConnectionString;

            if (values.Length != parametros.Length)//Número de valores e de parâmetros devem ser iguais
                return false;


            SqlConnection sqlConn = null;
            SqlCommand cmd = null;

            object objRet = null;
            try
            {
                using (sqlConn = new SqlConnection(_strConnStringSQL))
                {
                    using (cmd = new SqlCommand(nameProc, sqlConn))
                    {
                        sqlConn.Open();
                        cmd.CommandType = CommandType.StoredProcedure;

                        int iOut = -1;

                        for (int i = 0; i < parametros.Length; i++)
                        {
                            cmd.Parameters.Add(string.Format("{0}", parametros[i]), sqlDBType[i]);
                            cmd.Parameters[i].Value = values[i];
                            cmd.Parameters[i].Direction = direcao[i];

                            if (direcao[i] == ParameterDirection.InputOutput || direcao[i] == ParameterDirection.Output || direcao[i] == ParameterDirection.ReturnValue)
                                iOut = i;
                        }

                        if (cmd.ExecuteNonQuery() > 0)
                            objRet = cmd.Parameters[iOut].Value;
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (sqlConn.State != ConnectionState.Closed)
                    sqlConn.Close();
            }
            return objRet;
        }
        #endregion

        #region Método que verifica conexão
        /// <summary>
        /// Método genérico que verifica conexão de acordo com o Banco(sql, oracle, etc.)
        /// </summary>
        /// <param name="strConnection"></param>
        /// <param name="tipoBanco"></param>
        /// <returns></returns>
        public bool VerificarConnexao(TipoBanco tipoBanco)
        {
            bool bRet = false;

            try
            {
                switch (tipoBanco)
                {
                    case TipoBanco.SQLServer:
                        bRet = VerificarConexaoSQL();
                        break;
                    case TipoBanco.Oracle:
                        break;
                    case TipoBanco.OLDB:
                        break;
                    default:
                        break;
                }
            }
            catch (Exception)
            {
                bRet = false;
                throw;
            }
            return bRet;
        }
        #endregion

        #region Método que verifica conexão SQL Server
        /// <summary>
        /// Método responsável por verificar conexão no banco SQL.
        /// </summary>
        /// <param name="strConnection"></param>
        /// <returns></returns>
        private bool VerificarConexaoSQL()
        {
            bool bRet = false;
            SqlConnection cn = null;

            try
            {
                using (cn = new SqlConnection(_strConnStringSQL))
                {
                    cn.Open();
                    if (cn.State != ConnectionState.Closed)
                        cn.Close();

                    bRet = true;
                }
            }
            catch (Exception ex)
            {
                bRet = false;
                throw ex;
            }
            return bRet;
        }
        #endregion
    }

    public class DbAttribute : Attribute
    {
        private string campo;
        public String Campo
        {
            get { return campo; }
            set
            {
            	campo = value;
            }
        }
    }
}
