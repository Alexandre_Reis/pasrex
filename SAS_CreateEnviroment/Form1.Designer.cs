﻿namespace SAS_CreateEnviroment {
    partial class Form1 {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.btnAmbiente = new System.Windows.Forms.Button();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.txtScript = new System.Windows.Forms.TextBox();
            this.btnScript = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.txtBanco = new System.Windows.Forms.TextBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.txtInformation = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.txtCriaIIS = new System.Windows.Forms.Button();
            this.archive1 = new Dart.PowerTCP.Zip.Archive();
            this.unizp = new System.Windows.Forms.Button();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.btnMapping = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnAmbiente
            // 
            this.btnAmbiente.Location = new System.Drawing.Point(384, 98);
            this.btnAmbiente.Name = "btnAmbiente";
            this.btnAmbiente.Size = new System.Drawing.Size(106, 23);
            this.btnAmbiente.TabIndex = 0;
            this.btnAmbiente.Text = "Cria Ambiente";
            this.btnAmbiente.UseVisualStyleBackColor = true;
            this.btnAmbiente.Click += new System.EventHandler(this.btnAmbiente_Click);
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.Filter = "(*.SQL)|*.SQL|All files (*.*)|*.*";
            this.openFileDialog1.InitialDirectory = "C:\\";
            // 
            // txtScript
            // 
            this.txtScript.Enabled = false;
            this.txtScript.Location = new System.Drawing.Point(163, 53);
            this.txtScript.Name = "txtScript";
            this.txtScript.Size = new System.Drawing.Size(264, 20);
            this.txtScript.TabIndex = 1;
            // 
            // btnScript
            // 
            this.btnScript.Location = new System.Drawing.Point(433, 51);
            this.btnScript.Name = "btnScript";
            this.btnScript.Size = new System.Drawing.Size(75, 23);
            this.btnScript.TabIndex = 2;
            this.btnScript.Text = "Script";
            this.btnScript.UseVisualStyleBackColor = true;
            this.btnScript.Click += new System.EventHandler(this.btnScript_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 33);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(151, 13);
            this.label1.TabIndex = 3;
            this.label1.Text = "Nome Banco/ Diretorio Virtual:";
            // 
            // txtBanco
            // 
            this.txtBanco.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtBanco.Location = new System.Drawing.Point(163, 30);
            this.txtBanco.Name = "txtBanco";
            this.txtBanco.Size = new System.Drawing.Size(264, 20);
            this.txtBanco.TabIndex = 0;
            this.txtBanco.Text = "FIN_";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.txtInformation);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.btnAmbiente);
            this.groupBox1.Controls.Add(this.txtScript);
            this.groupBox1.Controls.Add(this.btnScript);
            this.groupBox1.Controls.Add(this.txtBanco);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(517, 137);
            this.groupBox1.TabIndex = 5;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Criação Ambiente";
            // 
            // txtInformation
            // 
            this.txtInformation.AutoSize = true;
            this.txtInformation.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtInformation.Location = new System.Drawing.Point(6, 108);
            this.txtInformation.Name = "txtInformation";
            this.txtInformation.Size = new System.Drawing.Size(75, 13);
            this.txtInformation.TabIndex = 5;
            this.txtInformation.Text = "Progresso...";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(59, 60);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(98, 13);
            this.label2.TabIndex = 4;
            this.label2.Text = "Script Carga Inicial:";
            // 
            // txtCriaIIS
            // 
            this.txtCriaIIS.Enabled = false;
            this.txtCriaIIS.Location = new System.Drawing.Point(535, 31);
            this.txtCriaIIS.Name = "txtCriaIIS";
            this.txtCriaIIS.Size = new System.Drawing.Size(106, 23);
            this.txtCriaIIS.TabIndex = 5;
            this.txtCriaIIS.Text = "Cria IIS";
            this.txtCriaIIS.UseVisualStyleBackColor = true;
            this.txtCriaIIS.Click += new System.EventHandler(this.txtCriaIIS_Click);
            // 
            // archive1
            // 
            this.archive1.Comment = "";
            this.archive1.DestinationDirectory = ".";
            this.archive1.ExcludePattern = "";
            this.archive1.TempFileDirectory = "";
            this.archive1.VolumeLabel = "";
            // 
            // unizp
            // 
            this.unizp.Enabled = false;
            this.unizp.Location = new System.Drawing.Point(535, 62);
            this.unizp.Name = "unizp";
            this.unizp.Size = new System.Drawing.Size(106, 23);
            this.unizp.TabIndex = 6;
            this.unizp.Text = "Unzip";
            this.unizp.UseVisualStyleBackColor = true;
            this.unizp.Click += new System.EventHandler(this.unizp_Click);
            // 
            // labelControl1
            // 
            this.labelControl1.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.labelControl1.Location = new System.Drawing.Point(560, 12);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(38, 13);
            this.labelControl1.TabIndex = 7;
            this.labelControl1.Text = "Testes";
            // 
            // btnMapping
            // 
            this.btnMapping.Enabled = false;
            this.btnMapping.Location = new System.Drawing.Point(535, 91);
            this.btnMapping.Name = "btnMapping";
            this.btnMapping.Size = new System.Drawing.Size(106, 23);
            this.btnMapping.TabIndex = 8;
            this.btnMapping.Text = "Mapping";
            this.btnMapping.UseVisualStyleBackColor = true;
            this.btnMapping.Click += new System.EventHandler(this.btnMapping_Click);
            // 
            // button1
            // 
            this.button1.Enabled = false;
            this.button1.Location = new System.Drawing.Point(535, 120);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(106, 23);
            this.button1.TabIndex = 9;
            this.button1.Text = "Teste IIS HTTP";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.ISS_HTTP);
            // 
            // button2
            // 
            this.button2.Enabled = false;
            this.button2.Location = new System.Drawing.Point(519, 155);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(126, 23);
            this.button2.TabIndex = 10;
            this.button2.Text = "WebServiceCreateDir";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.WebServiceCreateDir);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(667, 186);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.btnMapping);
            this.Controls.Add(this.labelControl1);
            this.Controls.Add(this.txtCriaIIS);
            this.Controls.Add(this.unizp);
            this.Controls.Add(this.groupBox1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnAmbiente;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.TextBox txtScript;
        private System.Windows.Forms.Button btnScript;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtBanco;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button txtCriaIIS;
        private Dart.PowerTCP.Zip.Archive archive1;
        private System.Windows.Forms.Button unizp;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private System.Windows.Forms.Label txtInformation;
        private System.Windows.Forms.Button btnMapping;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
    }
}

