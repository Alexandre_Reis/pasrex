﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Text;
using EntitySpaces.Interfaces;
using System.IO;
using EntitySpaces.Core;
using System.Data.SqlClient;
using System.Windows.Forms;
using EntitySpaces.LoaderMT;
using Microsoft.SqlServer.Management.Smo;
using Microsoft.SqlServer.Management.Sdk.Sfc;
using System.Collections.Specialized;
using Microsoft.SqlServer.Management.Common;
using Microsoft.Web.Administration;
using System.Net;
using Dart.PowerTCP.Zip;
using System.Collections;
using System.Data;

namespace SAS_CreateEnviroment {
    public partial class Form1 : Form {               
        // Contem o Script para Criação das Tables
        private string scriptTables = "";
        
        public Form1() {
            InitializeComponent();
        }

        /// <summary>
        /// Mostra a Informação do Arquivo Escolhido no textbox
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnScript_Click(object sender, EventArgs e) {
            DialogResult dr = this.openFileDialog1.ShowDialog();

            if (dr == DialogResult.OK) {
                this.txtScript.Text = this.openFileDialog1.FileName;
            }
        }

        /// <summary>
        /// Start do Processo de Criação da Database e suas tables baseado no Script do Financial
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnAmbiente_Click(object sender, EventArgs e) {            
            if (!this.TrataErros()) {
                Cursor.Current = Cursors.WaitCursor;

                bool erro = false;
                try {
                    /* Cria A Base da Dados - Endereço SAAS */
                    this.txtInformation.Text = "Criando DataBase SAAS...";
                    System.Windows.Forms.Application.DoEvents();
                    this.CreateDatabase();

                    /* Gera Modelo Financial - Server Local */
                    this.txtInformation.Text = "Gerando Script Financial...";
                    System.Windows.Forms.Application.DoEvents();
                    this.GeraScriptModeloFinancial();

                    /* Cria Tabelas Baseada no Modelo Financial - Endereço SAAS */
                    this.txtInformation.Text = "Criando Tables SAAS...";
                    System.Windows.Forms.Application.DoEvents();
                    this.CreateTables();

                    /* Load Script de Carga de Dados - Endereço SAAS */
                    this.txtInformation.Text = "Rodando Scripts Carga SAAS...";
                    System.Windows.Forms.Application.DoEvents();
                    this.LoadScriptCarga();

                    /* Cria Aplicação no IIS  - SAAS */
                    this.txtInformation.Text = "Criando Aplicação IIS - SAAS...";
                    System.Windows.Forms.Application.DoEvents();
                    this.CreateAplication();

                    /* Cria Arquivo Zip baseado no Financial_Atatika - Server Local */
                    /* no diretório A:/TransferidosUpload */
                    this.txtInformation.Text = "Criando Arquivo ZIP...";
                    System.Windows.Forms.Application.DoEvents();
                    this.CreateZip();

                    /* Faz Upload do Arquivo Zip para SAAS */
                    this.txtInformation.Text = "Uploading Arquivo p/ SAAS...";
                    System.Windows.Forms.Application.DoEvents();
                    this.UploadZip();

                    /* UniZip do zip Financial no SAAS */
                    this.txtInformation.Text = "Unzip Arquivo SAAS...";
                    System.Windows.Forms.Application.DoEvents();
                    this.UnZipFile();

                    /* Realiza o User-Mappings no banco SAAS */
                    this.txtInformation.Text = "Criando Permissão de Usuários Database SAAS...";
                    System.Windows.Forms.Application.DoEvents();
                    this.DatabasePermissaoUsuario();                    
                }
                catch (Exception e1) {
                    erro = true;
                    MessageBox.Show(e1.Message);                    
                }

                if (!erro) {
                    //this.txtInformation.Text = "";
                    //System.Windows.Forms.Application.DoEvents();
                    MessageBox.Show("Processamento Concluído.");
                }

                //this.txtInformation.Text = "";
                //System.Windows.Forms.Application.DoEvents();

                Cursor.Current = Cursors.Default;
            }
        }

        /// <summary>
        /// Tratamento de Erros
        /// </summary>
        /// <returns></returns>
        private bool TrataErros() {
            bool erro = false;

            if (this.txtBanco.Text.Trim() == "" || this.txtBanco.Text.Trim() == "FIN_") {
                MessageBox.Show("Banco de Dados Obrigatório.");
                erro = true;
            }

            else if (this.openFileDialog1.FileName.Trim() == "") {
                MessageBox.Show("Script de Carga SQL Obrigatório.");
                erro = true;
            }
            return erro;
        }

        /// <summary>
        /// Cria Database baseada no arquivo de Resource
        /// Usa ConnectionString do AppConfig - Endereço SAAS
        /// </summary>
        /// <exception cref="Exception">Se Occorreu Algum erro</exception>
        private void CreateDatabase() {

            string conteudoSQL = Properties.Resources.CreateDataBase;
            conteudoSQL = conteudoSQL.Replace("#DATABASE_NAME#", this.txtBanco.Text.ToUpper().Trim());
            // Substitui o caminho Default da Database
            conteudoSQL = conteudoSQL.Replace("MSSQL2005", "SQL2005");
            
            // Cada nova Linha com o commando go é um novo comando
            string[] commands = conteudoSQL.Split(new string[] { "\nGO" }, StringSplitOptions.RemoveEmptyEntries);
            foreach (string command in commands) {

                esUtility u = new esUtility();
                //u.ConnectionName = "FIN_DEMO_SAS";
                try {
                    u.ExecuteNonQuery(esQueryType.Text, command, "");
                }
                catch (SqlException e) {
                    string commandAux = "\n" + command.Replace("\r\n\r", "");

                    commandAux += " não executado\n";
                    throw new Exception(commandAux);
                }
            }
        }

        /// <summary>
        /// Endereço Local - Financial para pegar o Script Financial
        /// </summary>
        /// <exception cref="Exception">Se Occorreu Algum erro</exception>
        public void GeraScriptModeloFinancial() {                        
            string FILENAME = @"C:/Temp/CreateTables.txt"; // Arquivo é gerado aqui para conferir mas não é necessario
            if (!Directory.Exists(@"C:/Temp/")) {
                FILENAME = @"C:/CreateTables.txt"; 
            }

            String sqlServerLogin = "sa";
            String password = "teste";
            String instanceName = "Financial";            
            String remoteSvrName = "Server";

            //String remoteSvrName = "192.168.0.195"; 
           
            //// Connecting to an instance of SQL Server using SQL Server Authentication
            //Server srv1 = new Server();   // connects to default instance
            //srv1.ConnectionContext.LoginSecure = false;   // set to true for Windows Authentication
            //srv1.ConnectionContext.Login = sqlServerLogin;
            //srv1.ConnectionContext.Password = password;
            //Console.WriteLine(srv1.Information.Version);   // connection is established

            //// Connecting to a named instance of SQL Server with SQL Server Authentication using ServerConnection
            //ServerConnection srvConn = new ServerConnection();
            //srvConn.ServerInstance = @".\" + instanceName;   // connects to named instance
            //srvConn.LoginSecure = false;   // set to true for Windows Authentication
            //srvConn.Login = sqlServerLogin;
            //srvConn.Password = password;
            //Server srv2 = new Server(srvConn);
            //Console.WriteLine(srv2.Information.Version);   // connection is established

            try {

                // For remote connection, remote server name / ServerInstance needs to be specified
                ServerConnection srvConn2 = new ServerConnection(remoteSvrName);
                srvConn2.LoginSecure = false;
                srvConn2.Login = sqlServerLogin;
                srvConn2.Password = password;
                //
                Server srv3 = new Server(srvConn2);
                Console.WriteLine("Conectado: " + srv3.Information.Version);   // connection is established

                /* -------------------------------------------------------------------- */

                Database db = srv3.Databases["Financial"];

                Scripter scrp = new Scripter(srv3);
                scrp.Options.ScriptDrops = false;
                //scrp.Options.WithDependencies = true;
                scrp.Options.DriDefaults = true;
                scrp.Options.DriForeignKeys = true;
                scrp.Options.DriPrimaryKey = true;
                scrp.Options.DriAllConstraints = true;
                scrp.Options.FileName = FILENAME;
                scrp.Options.AppendToFile = true;
                //scrp.Options.FullTextCatalogs = true;
                //scrp.Options.ScriptSchema = true;

                //scrp.Options.IncludeIfNotExists = true;

                // Zera o arquivo de saida
                File.WriteAllText(FILENAME, "");

                Urn[] smoObjects = new Urn[2];

                this.scriptTables = ""; // Salva na Variavel de Classe
                this.scriptTables = "USE [" + this.txtBanco.Text.ToUpper().Trim() + "]\nGO\n";

                int j = 0;
                foreach (Table tb in db.Tables) {

                    smoObjects = new Urn[1];
                    smoObjects[0] = tb.Urn;

                    //string tableObject = @"Server[@Name='SERVER']/Database[@Name='Financial']/Table[@Name='Agencia' and @Schema='dbo']";

                    //if (smoObjects[0] == tableObject) {
                    if (!tb.IsSystemObject) {
                        StringCollection sc = scrp.Script(smoObjects);

                        foreach (string st in sc) {
                            //Console.WriteLine(st);
                            this.scriptTables += "\n" + st;
                            this.scriptTables += "\nGO\n";
                        }
                    }
                    //}
                    //else {
                    //    j++;
                    //    continue;
                    //}
                }
            }
            catch (Exception ex1) {
                throw new Exception("Processo de Geração Script Financial com Problemas. " + ex1.Message);
            }
        }

        /// <summary>
        /// Cria Tables baseada no conteudo da variavel de Classe scriptTables
        /// Procedimento feito em duas etapas: primeiro é executado os CreateTables
        /// e depois os Alter Tables
        /// 
        /// Endereço SAAS
        /// </summary>
        /// <exception cref="Exception">Se Occorreu Algum erro</exception>
        private void CreateTables() {
            List<string> alterTablesCommands = new List<string>(); // Guarda os Alter Tables
            
            // Cada nova Linha com o commando go é um novo comando
            string[] commands = this.scriptTables.Split(new string[] { "\nGO" }, StringSplitOptions.RemoveEmptyEntries);
            foreach (string command in commands) {

                // alterando o comando para incluir a Database
                string command1 = command.Replace("[dbo]", "[" + this.txtBanco.Text.ToUpper().Trim() + "].[dbo]");

                // Adiciona na lista de Alter Tables
                if (command.Contains("ALTER TABLE")) {
                    alterTablesCommands.Add(command1);
                }
                else {
                    #region Create Tables
                    esUtility u = new esUtility();
                    //u.ConnectionName = "FIN_DEMO1_SAS";

                    try {
                        u.ExecuteNonQuery(esQueryType.Text, command1);
                    }
                    catch (SqlException e) {
                        string commandAux = "\n" + command1.Replace("\r\n\r", "");

                        commandAux += " não executado\n";
                        throw new Exception(commandAux);
                    }
                    #endregion
                }
            }

            for (int i = 0; i < alterTablesCommands.Count; i++) {
                #region Alter Tables
                esUtility u = new esUtility();

                try {
                    u.ExecuteNonQuery(esQueryType.Text, alterTablesCommands[i], "");
                }
                catch (SqlException e1) {
                    string commandAux = "\n" + alterTablesCommands[i].Replace("\r\n\r", "");

                    commandAux += " não executado\n";

                    throw new Exception(commandAux);
                }
                #endregion
            }
        }

        /// <summary>
        /// Baseado num Arquivo de Carga txt Executa o Script no SAAS
        /// </summary>
        /// <exception cref="Exception">Se Occorreu Algum erro</exception>
        private void LoadScriptCarga() {
            string arquivo = this.openFileDialog1.FileName.Trim();
            //
            string conteudoSQL = File.ReadAllText(arquivo, Encoding.Default);
            
            // Alterando o comando para incluir a Database
            conteudoSQL = conteudoSQL.Replace("\ngo", "\nGO"); // Ajusta go tudo para maisculo
            
            // Cada nova Linha com o commando go ou GO é um novo comando
            string[] commands = conteudoSQL.Split(new string[] { "\nGO" }, StringSplitOptions.RemoveEmptyEntries);
            foreach (string command in commands) {

                if (!String.IsNullOrEmpty(command.Trim())) {
                    // alterando o comando para incluir a Database
                    string command1 = command.Replace("[dbo]", "[" + this.txtBanco.Text.ToUpper().Trim() + "].[dbo]");

                    // Se o comando não tiver o nome do novo banco gera Erro para impedir que comando seja erroneamente executado
                    // em banco incorreto - Não trata adequadamente comandos multiplos
                    if (!command1.Contains("[" + this.txtBanco.Text.Trim().Trim() + "].")) {
                        throw new Exception("Comando incorreto: " + command1);
                    }

                    esUtility u = new esUtility();
                    try {
                        u.ExecuteNonQuery(esQueryType.Text, command1, "");
                    }
                    catch (SqlException e) {
                        string commandAux = "\n" + command1.Replace("\r\n\r", "");

                        commandAux += " não executado\n";

                        throw new Exception(commandAux);
                    }
                }
            }
        }

        /// <summary>
        /// Cria a Aplicação no IIS SAAS - Usa um WebService
        /// </summary>
        private void CreateAplication() {
            br.com.financialonline.atatika.Service1 s = new br.com.financialonline.atatika.Service1();

            s.Create_IIS_Directory(this.txtBanco.Text.ToUpper());
        }

        /// <summary>
        /// Cria um Zip  a partir do Diretorio A:\WebApp\Financial_Atatika
        /// Servidor Atatika deve estar mapeado como letra A:/ para Funcionar
        /// </summary>
        private void CreateZip() {
            const string PATH_SERVIDOR = @"A:\WebApp\Financial_Atatika\*";
            const string PATH_ZIP_CREATION = @"A:\TransferidosUpload\";

            //Add a folder to the zip file.
            //Include subfolders, preserve paths
            archive1.Clear();
            //
            archive1.Overwrite = Overwrite.Always;
            archive1.IncludeSubs = true;
            archive1.Add(PATH_SERVIDOR);

            string arquivo = DateTime.Now.ToString("dd_MM_yyyy") + "_Financial.zip";

            // Listar o Zip e Retirar o que não precisa
            ArrayList a = archive1.Items;

            #region Ajusta os Arquivos do ZIP

            // Percorre de tras para frente
            for (int i = a.Count - 1; i >= 0; i--) {

                // Apaga tudo do Diretorio de Downloads
                if (((ArchiveItem)a[i]).Path == "Downloads/") {
                    if (((ArchiveItem)a[i]).Attributes == FileAttributes.Archive && ((ArchiveItem)a[i]).Name != "") {

                        archive1.RemoveAt(i);
                    }
                }

                // Apaga imagens Personalizadas Desnecessárias
                if (((ArchiveItem)a[i]).Path == "imagensPersonalizadas/") {

                    //if (((ArchiveItem)a[i]).Name != "" &&
                    //     ((ArchiveItem)a[i]).Name != "header_default.JPG" &&
                    //     ((ArchiveItem)a[i]).Name != "login_Default.png" &&
                    //     ((ArchiveItem)a[i]).Name != "logoReport_Default.JPG") {

                    //    archive1.RemoveAt(i);
                    //}

                    if (((ArchiveItem)a[i]).Name != "" && ((ArchiveItem)a[i]).Name != "favicon.ico") {
                        archive1.RemoveAt(i);
                    }
                }

                // Apaga o Diretorio imagensPersonalizadas/layout/
                if (((ArchiveItem)a[i]).Path == "imagensPersonalizadas/layout/") {
                    archive1.RemoveAt(i);
                }

                // Apaga web.config
                if (((ArchiveItem)a[i]).Path == "") {
                    if (((ArchiveItem)a[i]).Attributes == FileAttributes.Archive && ((ArchiveItem)a[i]).Name == "web.config") {
                        archive1.RemoveAt(i);
                    }
                }
            }
            #endregion

            #region Ajusta WebConfig a partir do Arquivo Modelo Padrão Contido no Diretorio de Resources

            /* Variaveis do WebConfig */
            string CONNECTION_INFO = this.txtBanco.Text.ToUpper().Trim(); // {0} e {1}
            string CATALOG = this.txtBanco.Text.ToUpper().Trim(); // {2}
            //
            string CLIENTE = this.txtBanco.Text.ToUpper().Trim();
            if (CLIENTE.Contains("FIN_")) {
                CLIENTE = CLIENTE.Replace("FIN_", "");
            }
            //
            string DIRETORIO_BASE_APLICACAO = this.txtBanco.Text.ToUpper().Trim();
            //
            string HOST_APLICACAO = this.txtBanco.Text.ToLower().Trim();
            if (HOST_APLICACAO.Contains("fin_")) {
                HOST_APLICACAO = HOST_APLICACAO.Replace("fin_", "");
            }

            //
            string arquivoWebConfig = Properties.Resources.Modelo_WebConfig;
            arquivoWebConfig = arquivoWebConfig.Replace("{0}", CONNECTION_INFO);
            arquivoWebConfig = arquivoWebConfig.Replace("{1}", CONNECTION_INFO);
            arquivoWebConfig = arquivoWebConfig.Replace("{2}", CATALOG);
            arquivoWebConfig = arquivoWebConfig.Replace("{3}", CLIENTE);
            arquivoWebConfig = arquivoWebConfig.Replace("{4}", DIRETORIO_BASE_APLICACAO);
            arquivoWebConfig = arquivoWebConfig.Replace("{5}", HOST_APLICACAO);

            // convert string to stream
            byte[] byteArray = Encoding.GetEncoding("ISO-8859-1").GetBytes(arquivoWebConfig);

            // GetBytes method is used to to create a byte array
            MemoryStream webConfig = new MemoryStream(byteArray);

            ArchiveItem item = new ArchiveItem(webConfig);
            item.Name = "web.config";
            //
            archive1.Add(item);
            #endregion

            #region Adiciona cssCustom
            string arquivoCssCustom = Properties.Resources.financialdesk;

            // convert string to stream
            byte[] byteArray1 = Encoding.GetEncoding("ISO-8859-1").GetBytes(arquivoCssCustom);

            MemoryStream financialdesk = new MemoryStream(byteArray1);

            /* Necessário voltar o ponteiro do arquivo para o Inicio para ler corretamente */
            financialdesk.Seek(0, SeekOrigin.Begin);
            //
            archive1.Add(financialdesk); // Memory Stream
            archive1[archive1.Items.Count - 1].Name = "financialdesk.css"; // Nome do Arquivo                        
            archive1[archive1.Items.Count - 1].Path = "cssCustom/";
            
            #endregion

            try {
                //Create the Zip File
                archive1.Zip(PATH_ZIP_CREATION + arquivo);
            }
            catch (Exception ex) {
                MessageBox.Show("Error Zip: " + ex.Message);
            }
        }
        
        /// <summary>
        /// Upload do Arquivo Zip para o servidor SAAS
        /// </summary>
        private void UploadZip() {
            string PATH_ZIP_CREATION = @"A:\TransferidosUpload\";
            //
            string arquivo = PATH_ZIP_CREATION + DateTime.Now.ToString("dd_MM_yyyy") + "_Financial.zip";

            // Confere se Arquivo Existe nesse Path
            if (File.Exists(arquivo)) {
                this.UploadFile(arquivo);
            }
        }

        /// <summary>
        /// UnZip do Arquivo Zip no servidor SAAS - Chama WebService
        /// </summary>        
        private void UnZipFile() {
            br.com.financialonline.atatika.Service1 s = new br.com.financialonline.atatika.Service1();
            //
            //string arquivoZip = "D:/FTP/Upload/19_01_2012_Financial.zip";
            //string diretorioUnzip = "D:/Temp/";

            // Paths no Servidor SAAS
            string arq = DateTime.Now.ToString("dd_MM_yyyy") + "_Financial.zip";
            string arquivoZip = "D:/FTP/" + arq;
            
            string dir = this.txtBanco.Text.ToUpper().Trim();
            string diretorioUnzip = "D:/HTTP/wwwroot/"+dir;
           
            s.UnzipFile(arquivoZip, diretorioUnzip);
        }

        /// <summary>
        /// Realiza o User-Mappings no Banco SAAS
        /// Cria user financial com permissao db_owner no banco SAAS e associa com o login financial já existente
        /// </summary>
        private void DatabasePermissaoUsuario() {

            /* Conectando no SAAS */
            ServerConnection connection = new ServerConnection("foco.financialonline.com.br");
            connection.LoginSecure = false;
            connection.Login = "sa";
            //connection.Password = "macacoproc";
            connection.Password = "fin_98%34";            
            //
            Server server = new Server(connection);

            // Pega o Login de Nome financial
            Login login = server.Logins["financial"];

            // Pega a DataBase Criada
            string DATABASE = this.txtBanco.Text.ToUpper().Trim();
            Database database = server.Databases[DATABASE];

            // Cria o novo User chamado financial
            User user = new User(database, "financial");
            
            // Associa User e Login
            user.Login = login.Name;
            //
            user.UserType = UserType.SqlLogin;
            
            // Cria Usuario
            user.Create();
            
            // Adiciona permissão de db_owner
            user.AddToRole("db_owner");

            connection.Disconnect();
        }

        #region Funções Auxiliares

        /// <summary>
        /// 
        /// </summary>
        /// <param name="zipFile">Path Completo do Arquivo</param>
        private void UploadFile(string zipFile) {
            //WebClient Client = new WebClient();
            //Client.UploadFile("ftp://atatika.financialonline.com.br/testeCelso1.txt", "S:/Temp/testeCelso1.txt");
            //Client.UploadFile("http://teste3.financialonline.com.br/fin_teste3/", "S:/Temp/testeCelso1.txt");                      
            //Client.UploadFile("ftp://afreitas:Verbatim!@#@atatika.financialonline.com.br/FTP/", "S:/Temp/testeCelso1.txt");

            const string SERVER = @"ftp://atatika.financialonline.com.br";
            const string USER = "afreitas";
            //const string PASS = "Verbatim!@#";
            const string PASS = "teste123";

            FtpWebRequest request = (FtpWebRequest)FtpWebRequest.Create(SERVER + "/" + Path.GetFileName(zipFile) );
            request.Method = WebRequestMethods.Ftp.UploadFile;
            request.Credentials = new NetworkCredential(USER, PASS);

            // Copy the contents of the file to the request stream. - Arquivo txt
            //StreamReader sourceStream = new StreamReader("S:/Temp/testeCelso1.txt");
            //byte[] fileContents = Encoding.UTF8.GetBytes(sourceStream.ReadToEnd());
            //sourceStream.Close();

            byte[] fileContents = this.StreamFile(zipFile); // arquivo zip em bytes[]

            request.ContentLength = fileContents.Length;
            request.Timeout = -1;

            Stream requestStream = request.GetRequestStream();
            requestStream.Write(fileContents, 0, fileContents.Length);
            requestStream.Close();

            FtpWebResponse response = null;
            try {
                response = (FtpWebResponse)request.GetResponse();
            }
            catch (Exception e) {               
                MessageBox.Show("Problema Upload: " +response.StatusDescription + e.Message);                
            }
            
            //Console.WriteLine("Upload File Complete, status {0}", response.StatusDescription);
            response.Close();
        }
      
        /// <summary>
        /// Lê um aquivo zip e retorna um vetor de bytes
        /// </summary>
        /// <param name="filename"></param>
        /// <returns></returns>
        private byte[] StreamFile(string filename) {
            FileStream fs = new FileStream(filename, FileMode.Open, FileAccess.Read);

            // Create a byte array of file stream length
            byte[] ImageData = new byte[fs.Length];

            //Read block of bytes from stream into the byte array
            fs.Read(ImageData, 0, System.Convert.ToInt32(fs.Length));

            //Close the File Stream
            fs.Close();
            return ImageData; //return the byte data
        }

        #endregion

        #region Testes

        /// <summary>
        /// Chama o webService
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtCriaIIS_Click(object sender, EventArgs e) {
            br.com.financialonline.atatika.Service1 s = new br.com.financialonline.atatika.Service1();

            /*-------------------------------------------*/
            Cursor.Current = Cursors.WaitCursor;

            // Descomentar
            //s.Create_IIS_Directory(this.txtBanco.Text.ToUpper());

            // Cria Stream - Arquivo de ZIP
            this.CreateZip();

            string PATH_ZIP_CREATION = @"A:\TransferidosUpload\";
            //
            string arquivo = PATH_ZIP_CREATION + DateTime.Now.ToString("dd_MM_yyyy") + "_Financial.zip";

            // Confere se Arquivo Existe nesse Path
            if (File.Exists(arquivo)) {
                this.UploadFile(arquivo);
            }

            /*-------------------------------------------*/
            Cursor.Current = Cursors.Default;
        }

        /// <summary>
        /// Chama o webService Unzip
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void unizp_Click(object sender, EventArgs e) {
            //this.UnzipFile("");

            br.com.financialonline.atatika.Service1 s = new br.com.financialonline.atatika.Service1();
            //
            string arquivoZip = "D:/FTP/Upload/19_01_2012_Financial.zip";
            string diretorioUnzip = "D:/Temp/";

            s.UnzipFile(arquivoZip, diretorioUnzip);
        }

        private void btnMapping_Click(object sender, EventArgs e) {
            this.DatabasePermissaoUsuario();

            /* Conectando Localmente */
            //ServerConnection connection = new ServerConnection("Server");
            //connection.LoginSecure = false;
            //connection.Login = "sa";
            //connection.Password = "teste";
            ////
            //Server server = new Server(connection);

            //// Pega o Login
            //Login login = server.Logins["Financial"];

            //// Pega a DataBase
            //Database database = server.Databases["TesteCelso"];

            //// Cria o User
            //User user = new User(database, "financial");
            //user.Login = login.Name;
            //user.UserType = UserType.SqlLogin;
            //user.Create();
            //user.AddToRole("db_owner");

            ///* Conectando no SAAS */
            //ServerConnection connection = new ServerConnection("foco.financialonline.com.br");
            //connection.LoginSecure = false;
            //connection.Login = "sa";
            //connection.Password = "macacoproc";
            //////
            //Server server = new Server(connection);
            //Console.WriteLine("Conectado: " + server.Information.Version);   // connection is established

            #region Teste
            //ServerConnection connection = new ServerConnection("Server");
            //connection.LoginSecure = false;
            //connection.Login = "sa";
            //connection.Password = "teste";
            ////
            //Server server = new Server(connection);

            //foreach (Database db in server.Databases) {
            //    Console.WriteLine("====================================");
            //    Console.WriteLine("Login Mappings for the database: " + db.Name);
            //    Console.WriteLine(" ");
            //    //Run the EnumLoginMappings method and return details of database user-login mappings to a DataTable object variable. 
            //    DataTable d = db.EnumLoginMappings();
            //    //Display the mapping information. 
            //    foreach (DataRow r in d.Rows) {
            //        foreach (DataColumn c in r.Table.Columns) {
            //            Console.WriteLine(c.ColumnName + " = " + r[c]);
            //        }
            //        Console.WriteLine(" ");
            //    }
            //}
            #endregion
        }

        #region WebServices
        [Obsolete("Virou um WebService - Contem Código Util")]
        private void btnDiretorioVirtual_Click_Obsolete(object sender, EventArgs e) {
            //ServerManager serverMgr = new ServerManager();
            //ServerManager serverMgr = ServerManager.OpenRemote("foco.financialonline.com.br");
            //ServerManager serverMgr = ServerManager.OpenRemote("64.34.169.186,8080");
            //ServerManager serverMgr = ServerManager.OpenRemote("64.34.169.186");

            //ServerManager serverMgr = ServerManager.OpenRemote("sb6.solveasp.net");

            //bool returnValue = LogonUser(user, userDomain, password, LOGON32_LOGON_INTERACTIVE, LOGON32_PROVIDER_DEFAULT, ref _userHandle);

            //if (!returnValue) throw new ApplicationException("Could not impersonate user");

            //WindowsIdentity newId = new WindowsIdentity(_userHandle);

            //_impersonatedUser = newId.Impersonate();

            //int retCode = CoInitializeSecurity(IntPtr.Zero, -1, IntPtr.Zero, IntPtr.Zero, RpcAuthnLevel.PktPrivacy, RpcImpLevel.Impersonate, IntPtr.Zero, EoAuthnCap.DynamicCloaking, IntPtr.Zero);

            //serverManager = ServerManager.OpenRemote(servername);

            //Configuration config = serverManager.GetAdministrationConfiguration();

            //Cria Diretorio            
            //string activeDir = @"C:\inetpub\wwwroot"; // Sem Permissao de Acesso
            string activeDir = @"S:\TesteIIS\wwwroot";
            string newPath = Path.Combine(activeDir, "Fin_Celso1");
            if (!Directory.Exists(newPath)) {

                Directory.CreateDirectory(newPath);
                //
                ServerManager serverMgr = new ServerManager();
                //SiteCollection mySite = serverMgr.Sites;

                Site site = serverMgr.Sites[0]; // Default Site

                // Cria um Diretorio Virtual no Default Site
                Microsoft.Web.Administration.Application rootApp = site.Applications[0];
                //rootApp.VirtualDirectories.Add("/Fin_Celso", @"C:\inetpub\wwwroot\Fin_Celso");

                // Cria uma Aplicação no Default Site
                //site.Applications.Add("/Fin_Celso", @"C:\inetpub\wwwroot\Fin_Celso");
                site.Applications.Add("/Fin_Celso1", newPath);


                //Site mySite = serverMgr.Sites.Add("Fin_Celso3", @"C:\inetpub\wwwroot", 8080);            
                ////Site mySite = serverMgr.Sites.Add("Fin_Celso3", @"D:\HTTP\wwwroot", 8080);            

                //serverMgr.ApplicationPools.Add("MyAppPool");
                //mySite.ApplicationDefaults.ApplicationPoolName = "MyAppPool";

                //mySite.TraceFailedRequestsLogging.Enabled = true;
                //mySite.TraceFailedRequestsLogging.Directory = "@C:\temp\fin_celso3";

                serverMgr.CommitChanges();
            }
        }

        /// <summary>
        /// WebService
        /// </summary>
        /// <param name="diretorio"></param>
        /// <returns></returns>
        [Obsolete("Virou um WebService ")]
        private void UnzipFile(string arquivoZip, string diretorioUnzip) {

            #region Arquivo Zip
            // Descompacta
            Archive arquivo = new Archive();
            arquivo.PreservePath = true;
            arquivo.Clear();
            arquivo.Overwrite = Overwrite.Always;

            //string arquivoZip = "A:/TransferidosUpload/19_01_2012_Financial.zip";
            //string pathUnzip = "A:/TransferidosUpload/";

            try {
                arquivo.QuickUnzip(arquivoZip, diretorioUnzip);
            }
            catch (Exception e) {
                throw new Exception("Unzip problem: " + e.Message);
            }
            #endregion
        }

        #endregion

        private void ISS_HTTP(object sender, EventArgs e) {
            string activeDir = @"S:\TesteIIS\wwwroot";
            string newPath = Path.Combine(activeDir, "Fin_Celso8");
            //
            if (!Directory.Exists(newPath)) {

                Directory.CreateDirectory(newPath);
                //
                ServerManager serverMgr = new ServerManager();

                Site site = serverMgr.Sites[0]; // Default Site

                site.Applications.Add("/Fin_Celso8", newPath);
                
                Microsoft.Web.Administration.Configuration config = serverMgr.GetApplicationHostConfiguration();
                
                string siteName = "Default Web Site";
                string vdRelativePath = "Fin_Celso8";
                //
                ConfigurationSection accessSection = config.GetSection("system.webServer/security/access", siteName + "/" + vdRelativePath);                
                accessSection["sslFlags"] = "Ssl";

                serverMgr.CommitChanges();
                //
                //
                ServerManager serverMgr1 = new ServerManager();
                Microsoft.Web.Administration.Configuration config1 = serverMgr1.GetApplicationHostConfiguration();
                ConfigurationSection httpErrorsSection = config1.GetSection("system.webServer/httpErrors", siteName + "/" + vdRelativePath);
                //
                ConfigurationElementCollection httpErrorsCollection = httpErrorsSection.GetCollection();
                httpErrorsCollection.SetAttributeValue("errorMode", 1);
                
                ConfigurationElement errorElement = httpErrorsCollection.CreateElement("error");
                errorElement["statusCode"] = 403;
                errorElement["subStatusCode"] = 4;
                errorElement["responseMode"] = 0;       
                errorElement["path"] = @"c:\inetpub\redirectToHttps.htm";
                //
                httpErrorsCollection.Add(errorElement);
                //
                serverMgr1.CommitChanges();
            }
        }

        private void WebServiceCreateDir(object sender, EventArgs e) {
            br.com.financialonline.atatika.Service1 s = new br.com.financialonline.atatika.Service1();

            s.Create_IIS_Directory(this.txtBanco.Text.ToUpper());
        }

        #endregion        
    }
}