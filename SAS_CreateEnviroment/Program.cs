using System;
using System.Collections.Generic;
using System.Windows.Forms;
using EntitySpaces.Interfaces;

namespace SAS_CreateEnviroment {
    static class Program {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main() {
            esProviderFactory.Factory = new EntitySpaces.LoaderMT.esDataProviderFactory();

            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new Form1());
        }
    }
}