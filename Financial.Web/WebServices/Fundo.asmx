<%@ WebService Language="C#" Class="FundoWS" %>

using System;
using System.Web;
using System.Web.Services;
using System.Web.Services.Protocols;
using Financial.Fundo;
using Financial.Common;
using Financial.Investidor;
using System.Collections.Generic;
using Financial.Security;
using System.Collections.Specialized;

[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
public class FundoWS : System.Web.Services.WebService
{

    [SoapHeader("Authentication")]
    [WebMethod]
    public CarteiraViewModel ExportaCarteiraPorCpfcnpj(string Cpfcnpj)
    {
        if (Authentication == null)
        {
            string msgErro = "Credenciais de autentica��o n�o foram informadas";
            throw new Exception(msgErro);
        }
        else if (!CheckAuthentication(Authentication.Username, Authentication.Password))
        {
            string msgErro = "Usu�rio n�o autenticado";
            throw new Exception(msgErro);
        }

        if (String.IsNullOrEmpty(Cpfcnpj.Trim()))
        {
            throw new Exception("Favor informar um CPF/CNPJ v�lido");
        }

        Carteira carteira = new CarteiraCollection().BuscaCarteiraPorCNPJ(Cpfcnpj);
        if (carteira == null)
        {
            throw new Exception("N�o foi poss�vel encontrar a carteira com o CNPJ:" + Cpfcnpj);
        }
        CarteiraViewModel carteiraViewModel = new CarteiraViewModel(carteira);


        return carteiraViewModel;
    }

    [SoapHeader("Authentication")]
    [WebMethod]
    public List<CarteiraViewModel> ExportaListaCarteira(List<int> TiposCliente)
    {
        if (Authentication == null)
        {
            string msgErro = "Credenciais de autentica��o n�o foram informadas";
            throw new Exception(msgErro);
        }
        else if (!CheckAuthentication(Authentication.Username, Authentication.Password))
        {
            string msgErro = "Usu�rio n�o autenticado";
            throw new Exception(msgErro);
        }

        List<CarteiraViewModel> listaCarteiraViewModel = new List<CarteiraViewModel>();

        CarteiraCollection carteiraCollection = new CarteiraCollection();
        carteiraCollection.LoadAll();

        foreach (Carteira carteira in carteiraCollection)
        {
            if (TiposCliente != null)
            {
                Cliente cliente = new Cliente();
                if (cliente.LoadByPrimaryKey(carteira.IdCarteira.Value) &&
                    !TiposCliente.Contains(cliente.IdTipo.Value))
                {
                    continue;
                }
            }
            listaCarteiraViewModel.Add(new CarteiraViewModel(carteira));
        }

        return listaCarteiraViewModel;
    }

    public class CarteiraViewModel
    {
        public string Nome;
        public string CodigoAnbid;
        public string Cpfcnpj;
        public int IdCarteira;
        public string Estrategia;
        public decimal ValorMinimoAplicacao;

        public string Categoria;
        public string GrauRisco;
        public string AgenteGestor;
        public string Objetivo;
        public string AgenteAdministrador;

        public string EnderecoAgenteAdministrador;
        public string CNPJAgenteAdministrador;
        public string CEPAgenteAdministrador;
        public string CidadeAgenteAdministrador;
        public string EstadoAgenteAdministrador;

        public DateTime DataInicioCota;
        //public decimal PLMedio12Meses;

        public decimal ValorUltimaCota;
        public decimal PLMaisRecente;
        public string Benchmark;

        public byte TipoTributacao;

        /*public decimal? RetornoUltimoMes;
        public decimal? RetornoAno;
        public decimal? Retorno12Meses;
        public decimal? Retorno24Meses;
        public decimal? Retorno36Meses;*/
        public decimal ValorMinimoInicial;
        public decimal ValorMinimoResgate;
        public decimal ValorMinimoSaldo;
        public string TaxaAdministracao;
        public string TaxaPerformance;
        public int TipoCota;
        public int DiasCotizacaoAplicacao;
        public int DiasCotizacaoResgate;
        public int DiasLiquidacaoAplicacao;
        public int DiasLiquidacaoResgate;
        public DateTime? HorarioFim;
        public DateTime? HorarioLimiteCotizacao;

        public CarteiraViewModel()
        {
        }
        public CarteiraViewModel(Carteira carteira)
        {

            this.Nome = carteira.Nome;
            this.IdCarteira = carteira.IdCarteira.Value;

            if (carteira.IdGrauRisco.HasValue)
            {
                GrauRisco grauRisco = new GrauRisco();
                grauRisco.LoadByPrimaryKey(carteira.IdGrauRisco.Value);
                this.GrauRisco = grauRisco.Descricao;
            }
            this.CodigoAnbid = carteira.CodigoAnbid;

            Cliente cliente = new Cliente();
            cliente.LoadByPrimaryKey(carteira.IdCarteira.Value);
            
            Financial.CRM.Pessoa pessoa = new Financial.CRM.Pessoa();
            pessoa.LoadByPrimaryKey(cliente.IdPessoa.Value);

            this.Cpfcnpj = pessoa.Cpfcnpj;

            AgenteMercado agenteGestor = new AgenteMercado();
            agenteGestor.LoadByPrimaryKey(carteira.IdAgenteGestor.Value);
            this.AgenteGestor = agenteGestor.Nome;

            this.Objetivo = carteira.Objetivo;
            AgenteMercado agenteAdministrador = new AgenteMercado();
            agenteAdministrador.LoadByPrimaryKey(carteira.IdAgenteAdministrador.Value);
            this.AgenteAdministrador = agenteAdministrador.Nome;
            this.CNPJAgenteAdministrador = agenteAdministrador.Cnpj;
            this.EnderecoAgenteAdministrador = agenteAdministrador.Endereco;
            this.CEPAgenteAdministrador = agenteAdministrador.Cep;
            this.CidadeAgenteAdministrador = agenteAdministrador.Cidade;
            this.EstadoAgenteAdministrador = agenteAdministrador.Uf;

            this.DataInicioCota = carteira.DataInicioCota.Value;

            CategoriaFundo categoriaFundo = new CategoriaFundo();
            categoriaFundo.LoadByPrimaryKey(carteira.IdCategoria.Value);
            this.Categoria = categoriaFundo.Descricao;

            //Patrimonio mais recente
            HistoricoCota historicoCotaMax = new HistoricoCota();
            DateTime? dataMaxCota = historicoCotaMax.BuscaDataMaxCota(carteira.IdCarteira.Value);
            DateTime dataFimCalculoMedida;
            if (dataMaxCota.HasValue)
            {
                historicoCotaMax = new HistoricoCota();
                historicoCotaMax.LoadByPrimaryKey(dataMaxCota.Value, carteira.IdCarteira.Value);
                if (carteira.TipoCota.Value == (byte)Financial.Fundo.Enums.TipoCotaFundo.Abertura)
                {
                    this.ValorUltimaCota = historicoCotaMax.CotaAbertura.Value;
                    this.PLMaisRecente = historicoCotaMax.PLAbertura.Value;
                }
                else
                {
                    this.ValorUltimaCota = historicoCotaMax.CotaFechamento.Value;
                    this.PLMaisRecente = historicoCotaMax.PLFechamento.Value;
                }
                dataFimCalculoMedida = dataMaxCota.Value;
            }
            else
            {
                dataFimCalculoMedida = DateTime.Today;
            }

            //CalculoMedida calculoMedida = new CalculoMedida(carteira.IdCarteira.Value, carteira.IdIndiceBenchmark.Value, carteira.DataInicioCota.Value, dataFimCalculoMedida);
            //calculoMedida.SetAjustaCota(true);

            if (carteira.IdEstrategia.HasValue)
            {
                Estrategia estrategia = new Estrategia();
                estrategia.LoadByPrimaryKey(carteira.IdEstrategia.Value);
                this.Estrategia = estrategia.Descricao;
            }

            /*try
            {
                this.PLMedio12Meses = calculoMedida.CalculaPLMedio12Meses(calculoMedida.DataFimJanela.Value);
            }
            catch { };*/

            Indice indiceBenchmark = new Indice();
            indiceBenchmark.LoadByPrimaryKey(carteira.IdIndiceBenchmark.Value);
            this.Benchmark = indiceBenchmark.Descricao;

            this.TipoTributacao = (byte)carteira.TipoTributacao;

            /*try
            {
                this.RetornoUltimoMes = calculoMedida.CalculaRetornoMesFechado(DateTime.Today.AddMonths(-1));
            }
            catch { }

            try
            {
                this.RetornoAno = calculoMedida.CalculaRetornoAno(calculoMedida.DataFimJanela.Value);
            }
            catch { }

            try
            {
                this.Retorno12Meses = calculoMedida.CalculaRetornoPeriodoMes(calculoMedida.DataFimJanela.Value, 12);
            }
            catch { }

            try
            {
                this.Retorno24Meses = calculoMedida.CalculaRetornoPeriodoMes(calculoMedida.DataFimJanela.Value, 24);
            }
            catch { }

            try { this.Retorno36Meses = calculoMedida.CalculaRetornoPeriodoMes(calculoMedida.DataFimJanela.Value, 36); }
            catch { }*/

            this.ValorMinimoAplicacao = carteira.ValorMinimoAplicacao.Value;
            this.ValorMinimoInicial = carteira.ValorMinimoInicial.Value;
            this.ValorMinimoResgate = carteira.ValorMinimoResgate.Value;
            this.ValorMinimoSaldo = carteira.ValorMinimoSaldo.Value;
            this.TipoCota = carteira.TipoCota.Value;
            this.ValorMinimoSaldo = carteira.ValorMinimoSaldo.Value;
            this.DiasCotizacaoAplicacao = carteira.DiasCotizacaoAplicacao.Value;
            this.DiasCotizacaoResgate = carteira.DiasCotizacaoResgate.Value;
            this.DiasLiquidacaoAplicacao = carteira.DiasLiquidacaoAplicacao.Value;
            this.DiasLiquidacaoResgate = carteira.DiasLiquidacaoResgate.Value;
            if (carteira.HorarioFim.HasValue)
            {
                this.HorarioFim = carteira.HorarioFim.Value;
            }

            if (carteira.HorarioLimiteCotizacao.HasValue)
            {
                this.HorarioLimiteCotizacao = carteira.HorarioLimiteCotizacao.Value;
            }

            decimal? taxaAdministracaoNum;

            TabelaTaxaAdministracaoCollection taxas = new TabelaTaxaAdministracaoCollection();
            taxas.BuscaTabelaTaxaAdministracao(carteira.IdCarteira.Value);
            if (taxas.Count >= 1)
            {
                taxaAdministracaoNum = taxas[0].Taxa;
            }
            else
            {
                taxaAdministracaoNum = null;
            }
            this.TaxaPerformance = "";


            if (taxaAdministracaoNum.HasValue)
            {
                string percentFormat = Financial.Util.StringEnum.GetStringValue(Financial.Relatorio.ReportBase.NumeroCasasDecimais.DuasCasasDecimaisPorcentagem);
                this.TaxaAdministracao = (taxaAdministracaoNum / 100).Value.ToString(percentFormat);
            }

        }
    }

    public ValidateLogin Authentication;

    public class ValidateLogin : SoapHeader
    {
        public string Username;
        public string Password;
    }

    private bool CheckAuthentication(string username, string password)
    {
        FinancialMembershipProvider financialMembershipProvider = new FinancialMembershipProvider();
        NameValueCollection listaValores = new NameValueCollection();
        listaValores.Add("passwordFormat", "Encrypted");
        financialMembershipProvider.Initialize(null, listaValores);

        bool autenticado = financialMembershipProvider.ValidateUser(username, password);
        return autenticado;
    }

}

