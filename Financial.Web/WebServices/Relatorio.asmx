<%@ WebService Language="C#" Class="Relatorio" %>

using System;
using System.Data;
using System.Web;
using System.Collections;
using System.Web.Services;
using System.Web.Services.Protocols;
using System.ComponentModel;
using Financial.Fundo;
using System.Collections.Generic;


/// <summary>
/// Summary description for Relatorios
/// </summary>
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
public class Relatorio : System.Web.Services.WebService
{
        
    //[SoapHeader("Authentication")]
    [WebMethod]
    public List<ExtratoCliente> ExportaExtratoCliente(int idCarteira, int idIndice, DateTime dataInicio, DateTime dataFim)
    {
        //int idCarteira = 10114;
        //int idIndice = 1;
        //DateTime dataInicio = Convert.ToDateTime("2013-09-11");
        //DateTime dataFim = Convert.ToDateTime("2014-04-30");

        //this.InitConnection();

        //if (Authentication == null)
        //{
        //    string msgErro = "Credenciais de autentica��o n�o foram informadas";
        //    throw new Exception(msgErro);
        //}
        //else if (!CheckAuthentication(Authentication.Username, Authentication.Password))
        //{
        //    string msgErro = "Usu�rio n�o autenticado";
        //    throw new Exception(msgErro);
        //}

        List<ExtratoCliente> Lista = new List<ExtratoCliente>();

        ExtratoCliente ec = new ExtratoCliente(idCarteira, idIndice, dataInicio, dataFim);

        Lista.Add(ec);

        return Lista;
    }

    public class ExtratoCliente
    {

        public ExtratoCliente()
        {
        }

        #region Privates
        private int idCarteira;
        private int idIndice;
        private DateTime dataInicio;
        private DateTime dataFim;
        private CalculoMedida calculoMedida;
        private CalculoMedida.InfoRiscoRetorno infoRiscoRetorno;
        private DataTable dt;

        /* Lista interna de Detalhes */        
        private List<PosicaoAtivos> lista = new List<PosicaoAtivos>();
        public AnaliseRisco analiseRisco = new AnaliseRisco();
        public RetornoCarteira retornoCarteira = new RetornoCarteira();
        #endregion


        #region Properties
        public int IdCarteira
        {
            get { return idCarteira; }
            set { idCarteira = value; }
        }
        
        public int IdIndice
        {
            get { return idIndice; }
            set { idIndice = value; }
        }

        public DateTime DataInicio
        {
            get { return dataInicio; }
            set { dataInicio = value; }
        }

        public DateTime DataFim
        {
            get { return dataFim; }
            set { dataFim = value; }
        }
        #endregion

        #region Acesso Externo        
        /// <summary>
        /// Acesso Externo
        /// </summary>
        /// 
        public AnaliseRisco GetAnaliseRisco
        {
            get { return this.analiseRisco; }
        }

        public RetornoCarteira GetRetornoCarteira
        {
            get { return this.retornoCarteira; }
        }

        public List<PosicaoAtivos> GetListPosicaoAtivos
        {
            get { return this.lista; }
        }
        #endregion

        public ExtratoCliente(int idCarteira, int idIndice, DateTime dataInicio, DateTime dataFim)
        {
            
            this.idCarteira = idCarteira;

            this.idIndice = idIndice;

            this.dataInicio = dataInicio;

            this.dataFim = dataFim;
            
            Carteira carteira = new Carteira();

            if (carteira.LoadByPrimaryKey(idCarteira))
            {

                this.calculoMedida = new CalculoMedida(idCarteira, idIndice, dataInicio, dataFim);

                DataTable datatable = calculoMedida.RetornaTabelaPosicaoAtivos(dataInicio, dataFim, true, false);

                this.dt = datatable;

                this.calculoMedida.InitListaRetornoMensal(carteira.DataInicioCota.Value, dataFim);

                this.infoRiscoRetorno = this.calculoMedida.RetornaInfoRiscoRetorno();

                this.SetaAnaliseRisco();

                this.SetaRetornoCarteira();

                this.SetaPosicaoAtivos();
            }
        }

        public class RetornoCarteira
        {
            public RetornoCarteira() { }

            public RetornoCarteira(decimal? a, decimal? b, decimal? c, decimal? d, decimal? e,
                decimal? f, decimal? g, decimal? h, decimal? i, decimal? j, decimal? k,
                decimal? l, decimal? m, decimal? n, decimal? o, decimal? p, decimal? q, decimal? r)
            {
                this.rentabCarteira3Meses = a;
                this.rentabBenchMark3Meses = b;
                this.rentabDiferencial3Meses = c;
                this.rentabCarteira6Meses = d;
                this.rentabBenchMark6Meses = e;
                this.rentabDiferencial6Meses = f;
                this.rentabCarteira12Meses = g;
                this.rentabBenchMark12Meses = h;
                this.rentabDiferencial12Meses = i;
                this.rentabCarteira24Meses = j;
                this.rentabBenchMark24Meses = k;
                this.rentabDiferencial24Meses = l;
                this.rentabCarteira36Meses = m;
                this.rentabBenchMark36Meses = n;
                this.rentabDiferencial36Meses = o;
                this.rentabCarteiraDesdeInicio = p;
                this.rentabBenchMarkDesdeInicio = q;
                this.rentabDiferencialDesdeInicio = r;
            }

            private decimal? rentabCarteira3Meses;
            private decimal? rentabBenchMark3Meses;
            private decimal? rentabDiferencial3Meses;
            private decimal? rentabCarteira6Meses;
            private decimal? rentabBenchMark6Meses;
            private decimal? rentabDiferencial6Meses;
            private decimal? rentabCarteira12Meses;
            private decimal? rentabBenchMark12Meses;
            private decimal? rentabDiferencial12Meses;
            private decimal? rentabCarteira24Meses;
            private decimal? rentabBenchMark24Meses;
            private decimal? rentabDiferencial24Meses;
            private decimal? rentabCarteira36Meses;
            private decimal? rentabBenchMark36Meses;
            private decimal? rentabDiferencial36Meses;
            private decimal? rentabCarteiraDesdeInicio;
            private decimal? rentabBenchMarkDesdeInicio;
            private decimal? rentabDiferencialDesdeInicio;

            public decimal? RentabCarteira3Meses
            {
                get { return rentabCarteira3Meses; }
                set { rentabCarteira3Meses = value; }
            }

            public decimal? RentabBenchMark3Meses
            {
                get { return rentabBenchMark3Meses; }
                set { rentabBenchMark3Meses = value; }
            }

            public decimal? RentabDiferencial3Meses
            {
                get { return rentabDiferencial3Meses; }
                set { rentabDiferencial3Meses = value; }
            }

            public decimal? RentabCarteira6Meses
            {
                get { return rentabCarteira6Meses; }
                set { rentabCarteira6Meses = value; }
            }

            public decimal? RentabBenchMark6Meses
            {
                get { return rentabBenchMark6Meses; }
                set { rentabBenchMark6Meses = value; }
            }

            public decimal? RentabDiferencial6Meses
            {
                get { return rentabDiferencial6Meses; }
                set { rentabDiferencial6Meses = value; }
            }

            public decimal? RentabCarteira12Meses
            {
                get { return rentabCarteira12Meses; }
                set { rentabCarteira12Meses = value; }
            }

            public decimal? RentabBenchMark12Meses
            {
                get { return rentabBenchMark12Meses; }
                set { rentabBenchMark12Meses = value; }
            }

            public decimal? RentabDiferencial12Meses
            {
                get { return rentabDiferencial12Meses; }
                set { rentabDiferencial12Meses = value; }
            }

            public decimal? RentabCarteira24Meses
            {
                get { return rentabCarteira24Meses; }
                set { rentabCarteira24Meses = value; }
            }

            public decimal? RentabBenchMark24Meses
            {
                get { return rentabBenchMark24Meses; }
                set { rentabBenchMark24Meses = value; }
            }

            public decimal? RentabDiferencial24Meses
            {
                get { return rentabDiferencial24Meses; }
                set { rentabDiferencial24Meses = value; }
            }

            public decimal? RentabCarteira36Meses
            {
                get { return rentabCarteira36Meses; }
                set { rentabCarteira36Meses = value; }
            }

            public decimal? RentabBenchMark36Meses
            {
                get { return rentabBenchMark36Meses; }
                set { rentabBenchMark36Meses = value; }
            }

            public decimal? RentabDiferencial36Meses
            {
                get { return rentabDiferencial36Meses; }
                set { rentabDiferencial36Meses = value; }
            }

            public decimal? RentabCarteiraDesdeInicio
            {
                get { return rentabCarteiraDesdeInicio; }
                set { rentabCarteiraDesdeInicio = value; }
            }

            public decimal? RentabBenchMarkDesdeInicio
            {
                get { return rentabBenchMarkDesdeInicio; }
                set { rentabBenchMarkDesdeInicio = value; }
            }

            public decimal? RentabDiferencialDesdeInicio
            {
                get { return rentabDiferencialDesdeInicio; }
                set { rentabDiferencialDesdeInicio = value; }
            }
            
        }

        public class AnaliseRisco{
            
            public AnaliseRisco() { }

            public AnaliseRisco(int a, int b, decimal c, decimal d, decimal e, decimal f, string g, string h,
                decimal i, decimal j, decimal k, decimal l, decimal m, decimal n)
            {
                this.mesesAcimaBenchMark = a;
                this.mesesAbaixoBenchMark = b;
                this.percentualAcimaBenchMark = c;
                this.percentualAbaixoBenchMark = d;
                this.maiorRentabilidadeCarteira = e;
                this.menorRentabilidadeCarteira = f;
                this.mesMaiorRentabilidade = g;
                this.mesMenorRentabilidade = h;
                this.vol3Meses = i;
                this.vol6Meses = j;
                this.vol12Meses = k;
                this.sharpe3Meses = l;
                this.sharpe6Meses = m;
                this.sharpe12Meses = n;
            }

            private int mesesAcimaBenchMark;
            private int mesesAbaixoBenchMark;
            private decimal percentualAcimaBenchMark;
            private decimal percentualAbaixoBenchMark ;
            private decimal maiorRentabilidadeCarteira ;
            private decimal menorRentabilidadeCarteira ;
            private string mesMaiorRentabilidade ;
            private string mesMenorRentabilidade ;
            private decimal? vol3Meses;
            private decimal? vol6Meses;
            private decimal? vol12Meses;
            private decimal? sharpe3Meses;
            private decimal? sharpe6Meses;
            private decimal? sharpe12Meses;

            public int MesesAcimaBenchMark
            {
                get { return mesesAcimaBenchMark; }
                set { mesesAcimaBenchMark = value; }
            }

            public int MesesAbaixoBenchMark
            {
                get { return mesesAbaixoBenchMark; }
                set { mesesAbaixoBenchMark = value; }
            }

            public decimal PercentualAcimaBenchMark
            {
                get { return percentualAcimaBenchMark; }
                set { percentualAcimaBenchMark = value; }
            }

            public decimal PercentualAbaixoBenchMark
            {
                get { return percentualAbaixoBenchMark; }
                set { percentualAbaixoBenchMark = value; }
            }

            public decimal MaiorRentabilidadeCarteira
            {
                get { return maiorRentabilidadeCarteira; }
                set { maiorRentabilidadeCarteira = value; }
            }

            public decimal MenorRentabilidadeCarteira
            {
                get { return menorRentabilidadeCarteira; }
                set { menorRentabilidadeCarteira = value; }
            }

            public string MesMaiorRentabilidade
            {
                get { return mesMaiorRentabilidade; }
                set { mesMaiorRentabilidade = value; }
            }

            public string MesMenorRentabilidade
            {
                get { return mesMenorRentabilidade; }
                set { mesMenorRentabilidade = value; }
            }

            public decimal? Vol3Meses
            {
                get { return vol3Meses; }
                set { vol3Meses = value; }
            }

            public decimal? Vol6Meses
            {
                get { return vol6Meses; }
                set { vol6Meses = value; }
            }

            public decimal? Vol12Meses
            {
                get { return vol12Meses; }
                set { vol12Meses = value; }
            }

            public decimal? Sharpe3Meses
            {
                get { return sharpe3Meses; }
                set { sharpe3Meses = value; }
            }

            public decimal? Sharpe6Meses
            {
                get { return sharpe6Meses; }
                set { sharpe6Meses = value; }
            }

            public decimal? Sharpe12Meses
            {
                get { return sharpe12Meses; }
                set { sharpe12Meses = value; }
            }

        }
                
        public class PosicaoAtivos
        {
            public PosicaoAtivos() { }


            public PosicaoAtivos(string a, string b, decimal? c, decimal? d, decimal? e, decimal? f, decimal? g,
                decimal? h, decimal? i, decimal? j, string l, int m)
            {

                this.estrategia = a;
                this.descricaoAtivo = b;
                this.saldoAnterior = c;
                this.aplicacoes = d;
                this.resgates = e;
                this.impostoPago = f;
                this.rendas = g;
                this.saldoBruto = h;
                this.provisaoTributo = i;
                this.saldoLiquido = j;
                this.idAtivo = l;
                this.tipoMercado = m;

            }

            private string estrategia;
            private string descricaoAtivo;
            private decimal? saldoAnterior;
            private decimal? aplicacoes;
            private decimal? resgates;
            private decimal? impostoPago;
            private decimal? rendas;
            private decimal? saldoBruto;
            private decimal? provisaoTributo;
            private decimal? saldoLiquido;
            private string idAtivo;
            private int tipoMercado;

            #region Properties
            public string Estrategia
            {
                get { return estrategia; }
                set { estrategia = value; }
            }
            public string DescricaoAtivo
            {
                get { return descricaoAtivo; }
                set { descricaoAtivo = value; }
            }
            public decimal? SaldoAnterior
            {
                get { return saldoAnterior; }
                set { saldoAnterior = value; }
            }
            public decimal? Aplicacoes
            {
                get { return aplicacoes; }
                set { aplicacoes = value; }
            }

            public decimal? Resgates
            {
                get { return resgates; }
                set { resgates = value; }
            }

            public decimal? ImpostoPago
            {
                get { return impostoPago; }
                set { impostoPago = value; }
            }

            public decimal? Rendas
            {
                get { return rendas; }
                set { rendas = value; }
            }

            public decimal? SaldoBruto
            {
                get { return saldoBruto; }
                set { saldoBruto = value; }
            }

            public decimal? ProvisaoTributo
            {
                get { return provisaoTributo; }
                set { provisaoTributo = value; }
            }

            public decimal? SaldoLiquido
            {
                get { return saldoLiquido; }
                set { saldoLiquido = value; }
            }

            public string IdAtivo
            {
                get { return idAtivo; }
                set { idAtivo = value; }
            }

            public int TipoMercado
            {
                get { return tipoMercado; }
                set { tipoMercado = value; }
            }
            #endregion
        }
        
        #region Fun��es
        private void SetaAnaliseRisco()
        {
            this.analiseRisco.MesesAcimaBenchMark = this.infoRiscoRetorno.MesesAcimaBenchMark;
            this.analiseRisco.MesesAbaixoBenchMark = this.infoRiscoRetorno.MesesAbaixoBenchMark;
            this.analiseRisco.PercentualAcimaBenchMark = this.infoRiscoRetorno.PercentualAcimaBenchMark;
            this.analiseRisco.PercentualAbaixoBenchMark = this.infoRiscoRetorno.PercentualAbaixoBenchMark;
            this.analiseRisco.MaiorRentabilidadeCarteira = this.infoRiscoRetorno.MaiorRentabilidadeCarteira;
            this.analiseRisco.MenorRentabilidadeCarteira = this.infoRiscoRetorno.MenorRentabilidadeCarteira;
            this.analiseRisco.MesMaiorRentabilidade = this.infoRiscoRetorno.MesMaiorRentabilidade;
            this.analiseRisco.MesMenorRentabilidade = this.infoRiscoRetorno.MesMenorRentabilidade;
            this.analiseRisco.Vol3Meses = this.infoRiscoRetorno.Vol3Meses;
            this.analiseRisco.Vol6Meses = this.infoRiscoRetorno.Vol6Meses;
            this.analiseRisco.Vol12Meses = this.infoRiscoRetorno.Vol12Meses;
            this.analiseRisco.Sharpe3Meses = this.infoRiscoRetorno.Sharpe3Meses;
            this.analiseRisco.Sharpe6Meses = this.infoRiscoRetorno.Sharpe6Meses;
            this.analiseRisco.Sharpe12Meses = this.infoRiscoRetorno.Sharpe12Meses;
        }

        private void SetaPosicaoAtivos()
        {
            for (int i = 0; i < this.dt.Rows.Count; i++)
            {
                PosicaoAtivos d = new PosicaoAtivos();
                //
                d.Estrategia = dt.Rows[i]["Estrategia"].ToString();
                d.DescricaoAtivo = dt.Rows[i]["DescricaoAtivo"].ToString();
                if (!Convert.IsDBNull(dt.Rows[i]["SaldoAnterior"]))
                {
                    d.SaldoAnterior = (decimal?)dt.Rows[i]["SaldoAnterior"];
                }
                if (!Convert.IsDBNull(dt.Rows[i]["Aplicacoes"]))
                {
                    d.Aplicacoes = (decimal?)dt.Rows[i]["Aplicacoes"];
                }
                if (!Convert.IsDBNull(dt.Rows[i]["Resgates"]))
                {                    
                    d.Resgates = (decimal)dt.Rows[i]["Resgates"];
                }
                if (!Convert.IsDBNull(dt.Rows[i]["ImpostoPago"]))
                {                    
                    d.ImpostoPago = (decimal?)dt.Rows[i]["ImpostoPago"];
                }
                if (!Convert.IsDBNull(dt.Rows[i]["Rendas"]))
                {                    
                    d.Rendas = (decimal?)dt.Rows[i]["Rendas"];
                }
                if (!Convert.IsDBNull(dt.Rows[i]["SaldoBruto"]))
                {
                    d.SaldoBruto = (decimal?)dt.Rows[i]["SaldoBruto"];
                }
                if (!Convert.IsDBNull(dt.Rows[i]["ProvisaoTributo"]))
                {
                    d.ProvisaoTributo = (decimal?)dt.Rows[i]["ProvisaoTributo"];
                }
                if (!Convert.IsDBNull(dt.Rows[i]["SaldoLiquido"]))
                {                    
                    d.SaldoLiquido = (decimal?)dt.Rows[i]["SaldoLiquido"];
                }
                
                d.IdAtivo = dt.Rows[i]["IdAtivo"].ToString();
                
                if (!Convert.IsDBNull(dt.Rows[i]["TipoMercado"]))
                {                    
                    d.TipoMercado = Convert.ToInt32(dt.Rows[i]["TipoMercado"]);
                }

                this.lista.Add(d);
            }
        }

        private void SetaRetornoCarteira()
        {
            this.retornoCarteira.RentabCarteira3Meses = this.infoRiscoRetorno.rentabCarteira3Meses;
            this.retornoCarteira.RentabBenchMark3Meses = this.infoRiscoRetorno.rentabBenchMark3Meses;
            this.retornoCarteira.RentabDiferencial3Meses = this.infoRiscoRetorno.rentabDiferencial3Meses;
            this.retornoCarteira.RentabCarteira6Meses = this.infoRiscoRetorno.rentabCarteira6Meses;
            this.retornoCarteira.RentabBenchMark6Meses = this.infoRiscoRetorno.rentabBenchMark6Meses;
            this.retornoCarteira.RentabDiferencial6Meses = this.infoRiscoRetorno.rentabDiferencial6Meses;
            this.retornoCarteira.RentabCarteira12Meses = this.infoRiscoRetorno.rentabCarteira12Meses;
            this.retornoCarteira.RentabBenchMark12Meses = this.infoRiscoRetorno.rentabBenchMark12Meses;
            this.retornoCarteira.RentabDiferencial12Meses = this.infoRiscoRetorno.rentabDiferencial12Meses;
            this.retornoCarteira.RentabCarteira24Meses = this.infoRiscoRetorno.rentabCarteira24Meses;
            this.retornoCarteira.RentabBenchMark24Meses = this.infoRiscoRetorno.rentabBenchMark24Meses;
            this.retornoCarteira.RentabDiferencial24Meses = this.infoRiscoRetorno.rentabDiferencial24Meses;
            this.retornoCarteira.RentabCarteira36Meses = this.infoRiscoRetorno.rentabCarteira36Meses;
            this.retornoCarteira.RentabBenchMark36Meses = this.infoRiscoRetorno.rentabBenchMark36Meses;
            this.retornoCarteira.RentabDiferencial36Meses = this.infoRiscoRetorno.rentabDiferencial36Meses;
            this.retornoCarteira.RentabCarteiraDesdeInicio = this.infoRiscoRetorno.rentabCarteiraDesdeInicio;
            this.retornoCarteira.RentabBenchMarkDesdeInicio = this.infoRiscoRetorno.rentabBenchMarkDesdeInicio;
            this.retornoCarteira.RentabDiferencialDesdeInicio = this.infoRiscoRetorno.rentabDiferencialDesdeInicio;
        }
        #endregion
    }
}

