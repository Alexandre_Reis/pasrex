﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Threading;
using System.Web.Configuration;
using System.Globalization;
using Financial.Web.Util;
using Financial.WebConfigConfiguration;
using System.IO;
using System.Drawing;
using Financial.Security;
using Financial.Security.Enums;
using Financial.Util;
using System.Collections.Specialized;

public partial class LoginInitPortoPar : Financial.Web.Common.BasePage
{
    protected override void InitializeCulture()
    {
        PersonalizeCulture.InicializaCulturePersonalizada();
        base.InitializeCulture();
    }

    new protected void Page_Load(object sender, EventArgs e)
    {
        this.Title = String.IsNullOrEmpty(ConfigurationManager.AppSettings["TituloPagina"]) ? "Financial" : ConfigurationManager.AppSettings["TituloPagina"];

        string userAgent = Request.ServerVariables["HTTP_USER_AGENT"].ToUpper();
        string httpAccept = Request.ServerVariables["HTTP_ACCEPT"].ToUpper();
        MobileDetect mobileDetect = new MobileDetect(userAgent, httpAccept);

        if (mobileDetect.DetectTierTablet() || mobileDetect.DetectSmartphone())
        {
            //Pular para mobile
            const string URL_MOBILE = "~/mobile/index.html";
            Response.Redirect(URL_MOBILE);
            return;
        }

        const string LOGIN_FAILURE_TEXT_DEFAULT = "Usuário ou senha inválidos. Tente novamente.";
        TextBox Username = Login1.FindControl("Username") as TextBox;
        TextBox Password = Login1.FindControl("Password") as TextBox;
        
        //Verificar se estamos enviando infos de login no post data
        string requestToken = Page.Request.Form["Login1$Username"];
        if (!string.IsNullOrEmpty(requestToken) && !Page.IsPostBack)
        {
            System.Web.Security.FormsAuthentication.SignOut();

            //Tentar validar Token usando WebService do Itau
            /*WSLoginItau.LoginIntegracao loginIntegracao = new WSLoginItau.LoginIntegracao();
            WSLoginItau.RespostaOfLoginIntegracaoEntidadeGa_SAZF5R respostaLoginIntegracao = loginIntegracao.AutenticarUsuarioIntegracao(requestToken);
            
            if (!respostaLoginIntegracao.Dados.Autenticado)
            {
                throw new Exception("Token inválido");
            }*/

            //string username = respostaLoginIntegracao.Dados.UsuarioSistema;

            throw new Exception("Custom page");
            string username = "admin";
            Usuario usuario = new Usuario();
            if (!usuario.BuscaUsuario(username))
            {
                throw new Exception("Usuário " + username + " inválido");
            }

            FinancialMembershipProvider financialMembershipProvider = new FinancialMembershipProvider();
            NameValueCollection listaValores = new NameValueCollection();
            listaValores.Add("passwordFormat", "Encrypted");
            financialMembershipProvider.Initialize(null, listaValores);

            string password = financialMembershipProvider.UnEncodePassword(usuario.Senha);

            Username.Text = username;
            Password.Attributes.Add("value", password);
            AutoSubmit.Text = "1";
        }

        if (!Page.IsPostBack && WebConfig.AppSettings.TecladoVirtual)
        {
            ScriptKeyboard();
            Password.Attributes.Add("onkeydown", "alert('Para digitar a senha, deve ser usado o teclado virtual!'); this.value = '';");
        }

        Page.ClientScript.RegisterStartupScript(this.GetType(), "MainPageReload",
        "<script type=\"text/javascript\">" +
        "if (self.parent!=self) self.parent.location.reload(true);" +
        "</script>");

        LinkButton LoginButton = Login1.FindControl("LoginButton") as LinkButton;

        Username.Attributes.Add("onKeyPress", "javascript:if (event.keyCode == 13) __doPostBack('" + LoginButton.UniqueID + "','')");
        Password.Attributes.Add("onKeyPress", "javascript:if (event.keyCode == 13) __doPostBack('" + LoginButton.UniqueID + "','')");

        if (!Page.IsPostBack)
        {
            string failureText = ConfigurationManager.AppSettings["LoginFailureText"];
            Login1.FailureText = String.IsNullOrEmpty(failureText) ? LOGIN_FAILURE_TEXT_DEFAULT : failureText;
            this.InitializeCulture();
        }

        Response.Cache.SetCacheability(HttpCacheability.ServerAndNoCache);
        if (Context.User.Identity.IsAuthenticated)
        {
            Usuario usuario = new Usuario();
            if (usuario.BuscaUsuario(HttpContext.Current.User.Identity.Name))
            {
                usuario.DataLogout = DateTime.Now;
                usuario.Save();
            }

            FormsAuthentication.SignOut();
            Session.Abandon();
            Session.Clear();
        }
        this.Login1.Focus();
    }

    private void ScriptKeyboard()
    {
        HtmlGenericControl Include = new HtmlGenericControl("script");
        Include.Attributes.Add("type", "text/javascript");
        Include.Attributes.Add("src", "../jsKey/keyboard.js");
        this.Page.Header.Controls.Add(Include);
    }

    protected void btnLembrarSenha_Click(object sender, EventArgs e)
    {
        Response.Redirect("LembrarSenha.aspx");
    }

    protected void Login1_LoggedIn(object sender, EventArgs e)
    {
        TextBox Username = Login1.FindControl("Username") as TextBox;
        CheckBox RememberMe = Login1.FindControl("RememberMe") as CheckBox;

        if (new FinancialMembershipProvider().IsUserMaster(Username.Text.ToString().ToLower()))
        {
            GrupoUsuarioCollection grupoUsuarioCollection = new GrupoUsuarioCollection();

            grupoUsuarioCollection.Query.Where(grupoUsuarioCollection.Query.TipoPerfil == (byte)TipoPerfilGrupo.Administrador,
                                               grupoUsuarioCollection.Query.IdGrupo != 0)
                                        .OrderBy(grupoUsuarioCollection.Query.IdGrupo.Ascending);

            grupoUsuarioCollection.Query.Load();

            // Sempre existe
            int idGrupo = grupoUsuarioCollection[0].IdGrupo.Value;

            UsuarioCollection usuarioCollection = new UsuarioCollection();
            usuarioCollection.Query.Where(usuarioCollection.Query.IdGrupo == idGrupo)
                             .OrderBy(usuarioCollection.Query.IdUsuario.Ascending);

            usuarioCollection.Query.Load(); // Sempre existe
            //
            string login = usuarioCollection[0].Login.Trim().ToLower();

            Session["IsUserMaster"] = true; // Indica que é usuario master
            //
            FormsAuthentication.SetAuthCookie(login, RememberMe.Checked);
        }
        else
        {
            Usuario usuario = new Usuario();
            usuario.BuscaUsuario(Username.Text.ToString());

            bool senhaExpirada = false;

            if (ParametrosConfiguracaoSistema.Seguranca.TempoExpiracaoSenha > 0)
            {
                DateTime dataExpiracao = usuario.DataUltimaAlteracao.Value.AddDays(ParametrosConfiguracaoSistema.Seguranca.TempoExpiracaoSenha);
                if (DateTime.Today >= dataExpiracao)
                {
                    senhaExpirada = true;
                }
            }

            if (usuario.TrocaSenha == "S" || senhaExpirada)
            {
                string returnURL = Request.Params["ReturnUrl"];

                //O trocar senha precisa saber se trata-se de um user com acesso ao portal cliente ou interno
                if (String.IsNullOrEmpty(returnURL))
                {
                    returnURL = "~/default.aspx";
                }

                //System.Web.Security.FormsAuthentication.SignOut();
                Response.Redirect("~/Login/TrocarSenha.aspx?ReturnURL=" + returnURL);
                return;
            }

            Session["IsUserMaster"] = false;
            FormsAuthentication.SetAuthCookie(Username.Text.ToString(), RememberMe.Checked);
        }


    }
}