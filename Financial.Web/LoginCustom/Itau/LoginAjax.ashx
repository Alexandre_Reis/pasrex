<%@ WebHandler Language="C#" Class="LoginAjaxItau" %>

using System;
using System.Web;
using Financial.Security;
using System.Web.Security;
using System.Collections.Specialized;

public class LoginAjaxItau : IHttpHandler, System.Web.SessionState.IRequiresSessionState {
    
    public void ProcessRequest (HttpContext context) {

        bool valid = false;

        string guid = context.Request["login"];
        
        //Tentar validar Token usando WebService do Itau
        WSLoginItau.LoginIntegracao loginIntegracao = new WSLoginItau.LoginIntegracao();
        string usuarioIntegracao = Financial.Util.ParametrosConfiguracaoSistema.Integracoes.UsuarioIntegracao;
        string senhaIntegracao = Financial.Util.ParametrosConfiguracaoSistema.Integracoes.SenhaIntegracao;

        if (!String.IsNullOrEmpty(usuarioIntegracao) && !String.IsNullOrEmpty(senhaIntegracao))
        {
            System.Net.CredentialCache cache = new System.Net.CredentialCache();
            cache.Add(new Uri(loginIntegracao.Url), "Basic", new System.Net.NetworkCredential(usuarioIntegracao, senhaIntegracao));
            loginIntegracao.Credentials = cache;
        }
        
        WSLoginItau.RespostaOfLoginIntegracaoEntidadeGa_SAZF5R respostaLoginIntegracao = loginIntegracao.AutenticarUsuarioIntegracao(guid);
                
        if (respostaLoginIntegracao.Dados.Autenticado)
        {
            string username = respostaLoginIntegracao.Dados.UsuarioSistema;

            Usuario usuario = new Usuario();
            if (usuario.BuscaUsuario(username))
            {
                FinancialMembershipProvider financialMembershipProvider = new FinancialMembershipProvider();
                NameValueCollection listaValores = new NameValueCollection();
                listaValores.Add("passwordFormat", "Encrypted");
                financialMembershipProvider.Initialize(null, listaValores);
                
                string password = financialMembershipProvider.UnEncodePassword(usuario.Senha);

                if (financialMembershipProvider.ValidateUser(username, password))
                {
                    valid = true;
                    bool rememberMe = false;

                    if (financialMembershipProvider.IsUserMaster(username.ToLower()))
                    {
                        GrupoUsuarioCollection grupoUsuarioCollection = new GrupoUsuarioCollection();

                        grupoUsuarioCollection.Query.Where(grupoUsuarioCollection.Query.TipoPerfil == (byte)Financial.Security.Enums.TipoPerfilGrupo.Administrador,
                                                           grupoUsuarioCollection.Query.IdGrupo != 0)
                                                    .OrderBy(grupoUsuarioCollection.Query.IdGrupo.Ascending);

                        grupoUsuarioCollection.Query.Load();

                        // Sempre existe
                        int idGrupo = grupoUsuarioCollection[0].IdGrupo.Value;

                        UsuarioCollection usuarioCollection = new UsuarioCollection();
                        usuarioCollection.Query.Where(usuarioCollection.Query.IdGrupo == idGrupo)
                                         .OrderBy(usuarioCollection.Query.IdUsuario.Ascending);

                        usuarioCollection.Query.Load(); // Sempre existe
                        //
                        string login = usuarioCollection[0].Login.Trim().ToLower();

                        context.Session["IsUserMaster"] = true; // Indica que � usuario master
                        //
                        FormsAuthentication.SetAuthCookie(login, rememberMe);
                    }
                    else
                    {
                        context.Session["IsUserMaster"] = false;
                        FormsAuthentication.SetAuthCookie(username, rememberMe);
                    }
                }
            }
        }
        
        context.Response.ContentType = "text/javascript";
        context.Response.Write("{success: " + valid.ToString().ToLower() + "}");
    }
 
    public bool IsReusable {
        get {
            return false;
        }
    }

}