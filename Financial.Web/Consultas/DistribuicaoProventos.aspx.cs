﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using Financial.Security;
using Financial.ContaCorrente;
using System.Text;
using DevExpress.Web;
using Financial.Web.Util;
using System.Drawing;
using Financial.Investidor;
using Financial.Fundo;
using Financial.Fundo.Enums;
using Financial.Fundo.Exceptions;
using Financial.Security.Enums;
using System.Collections.Generic;
using EntitySpaces.Interfaces;
using Financial.Web.Common;
using Financial.Util;
using Financial.Investidor.Enums;
using Financial.InvestidorCotista;

public partial class Consultas_DistribuicaoProventos : ConsultaBasePage 
{
    new protected void Page_Load(object sender, EventArgs e) {
        this.HasPopupCarteira = true;
        btnEditCodigoCarteira.Focus();
        base.Page_Load(sender, e);
    }

    #region DataSources
    protected void EsDSProventos_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        if (!Page.IsPostBack) {
            gridConsulta.Visible = false;
            e.Collection = new SaldoCaixaCollection();
        }
        else {
            // Confere Campos Obrigatorios para o caso de Mudança de Pagina e Scroolbar
            this.TrataCamposObrigatorios();
            //
            OperacaoCotistaCollection operacaoCotistaCollection = new OperacaoCotistaCollection();

            OperacaoCotistaQuery operacaoCotistaQuery = new OperacaoCotistaQuery("OC");
            CotistaQuery cotistaQuery = new CotistaQuery("C");
            ComplementoProventosCotistaQuery complementoProventosCotistaQuery = new ComplementoProventosCotistaQuery("CPC");

            operacaoCotistaQuery.Select(complementoProventosCotistaQuery.IdOperacaoReferencia,
                                        operacaoCotistaQuery.IdCotista,
                                        cotistaQuery.Nome,
                                        cotistaQuery.IsentoIOF,
                                        cotistaQuery.IsentoIR,
                                        complementoProventosCotistaQuery.DataAplicacao,
                                        complementoProventosCotistaQuery.Quantidade,
                                        operacaoCotistaQuery.TipoOperacao,
                                        operacaoCotistaQuery.ValorBruto,
                                        operacaoCotistaQuery.ValorIR,
                                        operacaoCotistaQuery.ValorIOF,
                                        operacaoCotistaQuery.ValorLiquido);
            operacaoCotistaQuery.InnerJoin(cotistaQuery).On(operacaoCotistaQuery.IdCotista.Equal(cotistaQuery.IdCotista));
            operacaoCotistaQuery.InnerJoin(complementoProventosCotistaQuery).On(operacaoCotistaQuery.IdOperacao.Equal(complementoProventosCotistaQuery.IdOperacao));

            if (btnEditCodigoCarteira.Text != "") {
                operacaoCotistaQuery.Where(operacaoCotistaQuery.IdCarteira.Equal(Convert.ToInt32(btnEditCodigoCarteira.Text)));
            }
            if (textDataInicio.Text != "") {
                operacaoCotistaQuery.Where(operacaoCotistaQuery.DataOperacao.GreaterThanOrEqual(Convert.ToDateTime(textDataInicio.Text)));
            }
            if (textDataFim.Text != "") {
                operacaoCotistaQuery.Where(operacaoCotistaQuery.DataOperacao.LessThanOrEqual(Convert.ToDateTime(textDataFim.Text)));
            }

            operacaoCotistaQuery.OrderBy(operacaoCotistaQuery.DataOperacao.Descending);

            operacaoCotistaCollection.Load(operacaoCotistaQuery);

            gridConsulta.Visible = true;
            e.Collection = operacaoCotistaCollection;
        }
    }

    protected void EsDSCarteira_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e) {
        CarteiraCollection carteiraCollection = new CarteiraCollection();
        carteiraCollection.BuscaCarteirasComAcessoTodas(HttpContext.Current.User.Identity.Name);

        e.Collection = carteiraCollection;
    }

    #endregion

    /// <summary>
    /// 
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void ASPxCallback1_Callback(object source, DevExpress.Web.CallbackEventArgs e) {
        string nome = "";
        if (!String.IsNullOrEmpty(e.Parameter) && e.Parameter != "null") {
            int idCarteira = Convert.ToInt32(e.Parameter);
            Carteira carteira = new Carteira();
            carteira.LoadByPrimaryKey(idCarteira);

            if (carteira.str.Apelido != "") {
                if (carteira.IsAtivo) {
                    //O permissionamento da carteira é dado direto no cliente vinculado à carteira
                    PermissaoCliente permissaoCliente = new PermissaoCliente();
                    if (permissaoCliente.RetornaAcessoCliente(idCarteira, HttpContext.Current.User.Identity.Name)) {
                        nome = carteira.str.Apelido;
                    }
                    else {
                        nome = "no_access";
                    }
                }
                else {
                    nome = "no_active";
                }
            }
        }
        e.Result = nome;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridConsulta_HtmlDataCellPrepared(object sender, ASPxGridViewTableDataCellEventArgs e) {

    }
        
    #region btnRun_Click, btnPDF_Click, btnExcel_Click

    /// <summary>
    /// Trata os campos Obrigatorios do Form
    /// </summary>
    private void TrataCamposObrigatorios() {
        #region Campos obrigatórios
        List<Control> controles = new List<Control>(new Control[] { 
            this.textDataInicio, this.textDataFim });

        if (base.TestaObrigatorio(controles) != "") {
            throw new Exception("Campos com * são obrigatórios!");
        }
        #endregion
    }

    new protected void btnRun_Click(object sender, EventArgs e) {
        this.TrataCamposObrigatorios();
        base.btnRun_Click(sender, e); // DataBind do Grid
    }

    new protected void btnPDF_Click(object sender, EventArgs e) {
        this.TrataCamposObrigatorios();
        this.SelecionaRelatorio(VisaoRelatorio.visaoPDF);
    }

    new protected void btnExcel_Click(object sender, EventArgs e) {
        this.TrataCamposObrigatorios();
        this.SelecionaRelatorio(VisaoRelatorio.visaoExcel);
    }

    /// <summary>
    /// Seleciona o Relatorio a Ser Executado
    /// </summary>
    /// <param name="visao">PDF, Excel ou Report</param>
    private void SelecionaRelatorio(string visao) {
        Response.BufferOutput = true;
        //
        // Salva os Parametros na Session
        Session["gridExportHistoricoCota"] = gridExport;
        Response.Redirect("~/Consultas/DistribuicaoProventosExporta.aspx?Visao=" + visao);
    }

    #endregion
}