﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;

using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Text;

using Financial.Security;
using Financial.Investidor;
using Financial.Investidor.Enums;
using Financial.Common.Enums;

using DevExpress.Web;
using Financial.Web.Common;
using Financial.RendaFixa;

public partial class Consultas_MemoriaCalculoRF : ConsultaBasePage {
    new protected void Page_Load(object sender, EventArgs e) {
        base.Page_Load(sender, e);
    }

    #region DataSources
    protected void EsDSPosicaoRendaFixa_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e) {
        PosicaoRendaFixaCollection posicaoRendaFixaCollection = new PosicaoRendaFixaCollection();

        PosicaoRendaFixaQuery posicaoRendaFixaQuery = new PosicaoRendaFixaQuery("P");
        ClienteQuery clienteQuery = new ClienteQuery("C");
        TituloRendaFixaQuery tituloRendaFixaQuery = new TituloRendaFixaQuery("T");

        posicaoRendaFixaQuery.Select(posicaoRendaFixaQuery, clienteQuery.Apelido, clienteQuery.DataDia, tituloRendaFixaQuery.DescricaoCompleta);
        posicaoRendaFixaQuery.InnerJoin(clienteQuery).On(clienteQuery.IdCliente == posicaoRendaFixaQuery.IdCliente);
        posicaoRendaFixaQuery.InnerJoin(tituloRendaFixaQuery).On(tituloRendaFixaQuery.IdTitulo == posicaoRendaFixaQuery.IdTitulo);
        posicaoRendaFixaQuery.Where(posicaoRendaFixaQuery.Quantidade.NotEqual(0));

        posicaoRendaFixaCollection.Load(posicaoRendaFixaQuery);

        e.Collection = posicaoRendaFixaCollection;
    }
    #endregion

    protected void gridConsulta_CustomCallback(object sender, DevExpress.Web.ASPxGridViewCustomCallbackEventArgs e) {
        gridConsulta.DataBind();
        gridConsulta.Selection.UnselectAll();
        gridConsulta.CancelEdit();
    }
}