﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using Financial.Security;
using Financial.ContaCorrente;
using System.Text;
using DevExpress.Web;
using Financial.Web.Util;
using System.Drawing;
using Financial.Investidor;
using Financial.Bolsa;
using System.Collections.Generic;
using EntitySpaces.Interfaces;
using Financial.Common;
using Financial.InvestidorCotista;
using Financial.Fundo;
using Financial.Security.Enums;
using Financial.Web.Common;

public partial class Consultas_PosicaoCotistaPorCotista : ConsultaBasePage {
    new protected void Page_Load(object sender, EventArgs e) {
        this.HasPopupCotista = true;
        btnEditCodigoCotista.Focus();
        base.Page_Load(sender, e);

        TrataTravamentoCampos();
    }

    #region DataSources
    protected void EsDSPosicaoCotista_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e) {
        if (!Page.IsPostBack) {
            gridConsulta.Visible = false;
            e.Collection = new PosicaoBolsaCollection();
        }
        else {
            // Confere Campos Obrigatorios para o caso de Mudança de Pagina e Scroolbar
            this.TrataCamposObrigatorios();
            //

            PosicaoCotistaCollection posicaoCotistaCollection = new PosicaoCotistaCollection();
            PosicaoCotistaQuery posicaoCotistaQuery = new PosicaoCotistaQuery("P");

            CarteiraQuery carteiraQuery = new CarteiraQuery("Q");
            posicaoCotistaQuery.InnerJoin(carteiraQuery).On(posicaoCotistaQuery.IdCarteira == carteiraQuery.IdCarteira);
            posicaoCotistaQuery.Select(posicaoCotistaQuery.IdCarteira, posicaoCotistaQuery.DataAplicacao, posicaoCotistaQuery.CotaDia,
                posicaoCotistaQuery.Quantidade, posicaoCotistaQuery.QuantidadeBloqueada, posicaoCotistaQuery.ValorBruto,
                posicaoCotistaQuery.ValorIR, posicaoCotistaQuery.ValorIOF, posicaoCotistaQuery.ValorLiquido, 
                carteiraQuery.Apelido, posicaoCotistaQuery.DataConversao, posicaoCotistaQuery.ValorPendenteLiquidacao, posicaoCotistaQuery.QtdePendenteLiquidacao);
            posicaoCotistaQuery.Where(posicaoCotistaQuery.IdCotista.Equal(Convert.ToInt32(btnEditCodigoCotista.Text)));
            posicaoCotistaQuery.Where(posicaoCotistaQuery.Quantidade.NotEqual(0) | posicaoCotistaQuery.QtdePendenteLiquidacao.NotEqual(0));

            posicaoCotistaQuery.OrderBy(posicaoCotistaQuery.IdCotista.Ascending, posicaoCotistaQuery.DataAplicacao.Descending);

            posicaoCotistaCollection.Load(posicaoCotistaQuery);
            e.Collection = posicaoCotistaCollection;

            gridConsulta.Visible = true;
        }
    }

    protected void EsDSCarteira_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e) {
        CarteiraCollection carteiraCollection = new CarteiraCollection();
        carteiraCollection.BuscaCarteirasComAcessoTodas(HttpContext.Current.User.Identity.Name);

        e.Collection = carteiraCollection;
    }

    protected void EsDSCotista_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e) {
        CotistaCollection cotistaCollection = new CotistaCollection();
        cotistaCollection.BuscaCotistasComAcesso(HttpContext.Current.User.Identity.Name);

        e.Collection = cotistaCollection;
    }
    #endregion

    /// <summary>
    /// 
    /// </summary>
    private void TrataTravamentoCampos() {
        Usuario usuario = new Usuario();
        usuario.BuscaUsuario(HttpContext.Current.User.Identity.Name);
        int tipoTrava = usuario.TipoTrava.Value;

        if (tipoTrava == (int)TipoTravaUsuario.TravaCotista) {
            btnEditCodigoCotista.Enabled = false;

            PermissaoCotista permissaoCotista = new PermissaoCotista();
            int? idCotista = permissaoCotista.RetornaCotistaAssociado(HttpContext.Current.User.Identity.Name);

            if (idCotista.HasValue) {
                Cotista cotista = new Cotista();

                List<esQueryItem> campos = new List<esQueryItem>();
                campos.Add(cotista.Query.Apelido);
                cotista.LoadByPrimaryKey(campos, idCotista.Value);
                string apelido = cotista.Apelido;

                btnEditCodigoCotista.Text = Convert.ToString(idCotista.Value);
                textNome.Text = apelido;
            }
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void ASPxCallback1_Callback(object source, DevExpress.Web.CallbackEventArgs e) {
        string nome = "";
        if (!String.IsNullOrEmpty(e.Parameter) && e.Parameter != "null") {
            int idCotista = Convert.ToInt32(e.Parameter);
            Cotista cotista = new Cotista();
            cotista.LoadByPrimaryKey(idCotista);

            if (cotista.str.Apelido != "") {
                if (cotista.IsAtivo) {
                    //O permissionamento da cotista é dado direto no cliente vinculado à cotista
                    PermissaoCotista permissaoCotista = new PermissaoCotista();
                    if (permissaoCotista.RetornaAcessoCotista(idCotista, HttpContext.Current.User.Identity.Name)) {
                        nome = cotista.str.Apelido;
                    }
                    else {
                        nome = "no_access";
                    }
                }
                else {
                    nome = "no_active";
                }
            }
        }
        e.Result = nome;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridConsulta_HtmlDataCellPrepared(object sender, ASPxGridViewTableDataCellEventArgs e) {
        if (e.DataColumn.FieldName == "ValorIR" || e.DataColumn.FieldName == "ValorIOF" ||
            e.DataColumn.FieldName == "ValorPerformance") {
            e.Cell.ForeColor = Color.Red;
        }
    }

    #region btnRun_Click, btnPDF_Click, btnExcel_Click

    /// <summary>
    /// Trata os campos Obrigatorios do Form
    /// </summary>
    private void TrataCamposObrigatorios() {
        #region Campos obrigatórios
        List<Control> controles = new List<Control>(new Control[] { 
            this.btnEditCodigoCotista });

        if (base.TestaObrigatorio(controles) != "") {
            throw new Exception("Campos com * são obrigatórios!");
        }
        #endregion
    }

    new protected void btnRun_Click(object sender, EventArgs e) {
        this.TrataCamposObrigatorios();
        base.btnRun_Click(sender, e); // DataBind do Grid
    }

    new protected void btnPDF_Click(object sender, EventArgs e) {
        this.TrataCamposObrigatorios();
        this.SelecionaRelatorio(VisaoRelatorio.visaoPDF);
    }

    new protected void btnExcel_Click(object sender, EventArgs e) {
        this.TrataCamposObrigatorios();
        this.SelecionaRelatorio(VisaoRelatorio.visaoExcel);
    }

    /// <summary>
    /// Seleciona o Relatorio a Ser Executado
    /// </summary>
    /// <param name="visao">PDF, Excel ou Report</param>
    private void SelecionaRelatorio(string visao) {
        Response.BufferOutput = true;
        //
        // Salva os Parametros na Session
        Session["gridExportPosicaoCotistaPorCotista"] = gridExport;
        Response.Redirect("~/Consultas/PosicaoCotistaPorCotistaExporta.aspx?Visao=" + visao);
    }

    #endregion
}