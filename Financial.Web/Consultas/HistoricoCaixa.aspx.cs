﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using Financial.Security;
using Financial.ContaCorrente;
using System.Text;
using DevExpress.Web;
using Financial.Web.Util;
using System.Drawing;
using Financial.Investidor;
using System.Collections.Generic;
using EntitySpaces.Interfaces;
using Financial.Security.Enums;
using Financial.Web.Common;

public partial class Consultas_HistoricoCaixa : ConsultaBasePage {
    new protected void Page_Load(object sender, EventArgs e) {
        this.HasPopupCliente = true;
        btnEditCodigoCliente.Focus();
        base.Page_Load(sender, e);

        TrataTravamentoCampos();
    }

    #region DataSources
    protected void EsDSSaldoCaixa_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e) {
        if (!Page.IsPostBack) {
            gridConsulta.Visible = false;
            e.Collection = new SaldoCaixaCollection();
        }
        else {

            // Confere Campos Obrigatorios para o caso de Mudança de Pagina e Scroolbar
            this.TrataCamposObrigatorios();

            #region Consulta
            SaldoCaixaCollection saldoCaixaCollection = new SaldoCaixaCollection();
            //
            if (btnEditCodigoCliente.Text != "") {
                saldoCaixaCollection.Query.Where(saldoCaixaCollection.Query.IdCliente == Convert.ToInt32(btnEditCodigoCliente.Text));
            }
            if (textDataInicio.Text != "") {
                saldoCaixaCollection.Query.Where(saldoCaixaCollection.Query.Data >= Convert.ToDateTime(textDataInicio.Text));
            }
            if (textDataFim.Text != "") {
                saldoCaixaCollection.Query.Where(saldoCaixaCollection.Query.Data <= Convert.ToDateTime(textDataFim.Text));
            }

            saldoCaixaCollection.Query.OrderBy(saldoCaixaCollection.Query.Data.Descending);
            //
            saldoCaixaCollection.Query.Load();
            //
            // Adiciona Coluna Extra
            SaldoCaixaCollection saldoCaixaCollectionAux = new SaldoCaixaCollection();
            saldoCaixaCollectionAux.CreateColumnsForBinding();
            saldoCaixaCollectionAux.AddColumn("ValorVencimentos", typeof(System.Decimal));
            //
            for (int i = 0; i < saldoCaixaCollection.Count; i++) 
            {
                int idCliente = saldoCaixaCollection[i].IdCliente.Value;
                DateTime data = saldoCaixaCollection[i].Data.Value;
                //
                Liquidacao liquidacao = new Liquidacao();
                decimal valorVencimentos = liquidacao.RetornaLiquidacao(idCliente, data, saldoCaixaCollection[i].IdConta.Value);
                //                
                // Seta Valores
                SaldoCaixa s = saldoCaixaCollectionAux.AddNew();
                //
                s.IdCliente = saldoCaixaCollection[i].IdCliente.Value;
                s.IdConta = saldoCaixaCollection[i].IdConta.Value;
                s.Data = saldoCaixaCollection[i].Data.Value;
                s.SaldoAbertura = saldoCaixaCollection[i].SaldoAbertura.Value;
                s.SaldoFechamento = saldoCaixaCollection[i].SaldoFechamento.Value;
                s.SetColumn("ValorVencimentos", valorVencimentos);
            }
            #endregion

            //saldoCaixaCollectionAux.LowLevelBind();
            //
            gridConsulta.Visible = true;
            e.Collection = saldoCaixaCollectionAux;
        }
    }

    protected void EsDSCliente_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e) {
        ClienteCollection clienteCollection = new ClienteCollection();
        clienteCollection.BuscaClientesComAcesso(HttpContext.Current.User.Identity.Name);

        e.Collection = clienteCollection;
    }
    #endregion

    /// <summary>
    /// 
    /// </summary>
    private void TrataTravamentoCampos() {
        Usuario usuario = new Usuario();
        usuario.BuscaUsuario(HttpContext.Current.User.Identity.Name);
        int tipoTrava = usuario.TipoTrava.Value;

        if (tipoTrava == (int)TipoTravaUsuario.TravaCliente) {
            btnEditCodigoCliente.Enabled = false;

            PermissaoCliente permissaoCliente = new PermissaoCliente();
            int? idCliente = permissaoCliente.RetornaClienteAssociado(HttpContext.Current.User.Identity.Name);

            if (idCliente.HasValue) {
                Cliente cliente = new Cliente();

                List<esQueryItem> campos = new List<esQueryItem>();
                campos.Add(cliente.Query.Apelido);
                cliente.LoadByPrimaryKey(campos, idCliente.Value);
                string apelido = cliente.Apelido;

                btnEditCodigoCliente.Text = Convert.ToString(idCliente.Value);
                textNome.Text = apelido;
            }
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void ASPxCallback1_Callback(object source, DevExpress.Web.CallbackEventArgs e) {
        string nome = "";
        if (!String.IsNullOrEmpty(e.Parameter) && e.Parameter != "null") {
            int idCliente = Convert.ToInt32(e.Parameter);
            Cliente cliente = new Cliente();
            cliente.LoadByPrimaryKey(idCliente);

            if (cliente.str.Apelido != "") {
                if (cliente.IsAtivo) {
                    //O permissionamento da cliente é dado direto no cliente vinculado à cliente
                    PermissaoCliente permissaoCliente = new PermissaoCliente();
                    if (permissaoCliente.RetornaAcessoClienteComControle(idCliente, HttpContext.Current.User.Identity.Name)) {
                        nome = cliente.str.Apelido;
                    }
                    else {
                        nome = "no_access";
                    }
                }
                else {
                    nome = "no_active";
                }
            }
        }
        e.Result = nome;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridConsulta_HtmlDataCellPrepared(object sender, ASPxGridViewTableDataCellEventArgs e) {
        if (e.DataColumn.FieldName == SaldoCaixaMetadata.ColumnNames.SaldoAbertura) {
            decimal value = (decimal)e.GetValue(SaldoCaixaMetadata.ColumnNames.SaldoAbertura);
            if (value < 0)
                e.Cell.ForeColor = Color.Red;
        }
        else if (e.DataColumn.FieldName == SaldoCaixaMetadata.ColumnNames.SaldoFechamento) {
            decimal value = (decimal)e.GetValue(SaldoCaixaMetadata.ColumnNames.SaldoFechamento);
            if (value < 0)
                e.Cell.ForeColor = Color.Red;
        }
        else if (e.DataColumn.FieldName == "ValorVencimentos") {
            decimal value = (decimal)e.GetValue("ValorVencimentos");
            if (value < 0)
                e.Cell.ForeColor = Color.Red;
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    [Obsolete("Está sendo feito no carregamento do DataSource")]
    protected void gridConsulta_CustomUnboundColumnData(object sender, ASPxGridViewColumnDataEventArgs e) {
        //if (e.Column.FieldName == "ValorVencimentos") {
        //    int idCliente = Convert.ToInt32(e.GetListSourceFieldValue("IdCliente"));
        //    DateTime data = Convert.ToDateTime(e.GetListSourceFieldValue("Data"));

        //    Liquidacao liquidacao = new Liquidacao();
        //    decimal valorVencimentos = liquidacao.RetornaLiquidacaoNaData(idCliente, data);

        //    e.Value = valorVencimentos;
        //}
    }


    #region btnRun_Click, btnPDF_Click, btnExcel_Click

    /// <summary>
    /// Trata os campos Obrigatorios do Form
    /// </summary>
    private void TrataCamposObrigatorios() {
        #region Campos obrigatórios
        List<Control> controles = new List<Control>(new Control[] { 
            this.btnEditCodigoCliente, this.textDataInicio, this.textDataFim });

        if (base.TestaObrigatorio(controles) != "") {
            throw new Exception("Campos com * são obrigatórios!");
        }
        #endregion
    }

    new protected void btnRun_Click(object sender, EventArgs e) {
        this.TrataCamposObrigatorios();        
        base.btnRun_Click(sender, e); // DataBind do Grid
    }

    new protected void btnPDF_Click(object sender, EventArgs e) {
        this.TrataCamposObrigatorios();
        this.SelecionaRelatorio(VisaoRelatorio.visaoPDF);
    }

    new protected void btnExcel_Click(object sender, EventArgs e) {
        this.TrataCamposObrigatorios();
        this.SelecionaRelatorio(VisaoRelatorio.visaoExcel);
    }

    /// <summary>
    /// Seleciona o Relatorio a Ser Executado
    /// </summary>
    /// <param name="visao">PDF, Excel ou Report</param>
    private void SelecionaRelatorio(string visao) {
        Response.BufferOutput = true;
        //
        // Salva os Parametros na Session
        Session["gridExportHistoricoCaixa"] = gridExport;
        Response.Redirect("~/Consultas/HistoricoCaixaExporta.aspx?Visao=" + visao);
    }

    #endregion
}