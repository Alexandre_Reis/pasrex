﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;

using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Text;

using Financial.Security;
using Financial.Investidor;
using Financial.Investidor.Enums;
using Financial.Common.Enums;

using DevExpress.Web;
using Financial.Web.Common;
using Financial.RendaFixa;
using Financial.Common;

public partial class Consultas_SimulacaoCalculoRF : ConsultaBasePage {
    new protected void Page_Load(object sender, EventArgs e) {
        base.Page_Load(sender, e);
    }

    #region DataSources
    protected void EsDSTituloRendaFixa_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e) {
        TituloRendaFixaCollection tituloRendaFixaCollection = new TituloRendaFixaCollection();

        PapelRendaFixaQuery papelRendaFixaQuery = new PapelRendaFixaQuery("P");
        TituloRendaFixaQuery tituloRendaFixaQuery = new TituloRendaFixaQuery("T");

        tituloRendaFixaQuery.Select(papelRendaFixaQuery.Descricao, tituloRendaFixaQuery);
        tituloRendaFixaQuery.InnerJoin(papelRendaFixaQuery).On(papelRendaFixaQuery.IdPapel == tituloRendaFixaQuery.IdPapel);
        tituloRendaFixaQuery.OrderBy(tituloRendaFixaQuery.DescricaoCompleta.Ascending);

        tituloRendaFixaCollection.Load(tituloRendaFixaQuery);

        e.Collection = tituloRendaFixaCollection;
    }

    protected void EsDSPapelRendaFixa_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        PapelRendaFixaCollection coll = new PapelRendaFixaCollection();

        coll.Query.OrderBy(coll.Query.Descricao.Ascending);
        coll.LoadAll();

        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }

    protected void EsDSIndice_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        IndiceCollection coll = new IndiceCollection();

        coll.Query.OrderBy(coll.Query.Descricao.Ascending);
        coll.LoadAll();

        Indice i = new Indice();
        i.IdIndice = null;
        i.Descricao = "";

        // Attach
        coll.AttachEntity(i);
        coll.Sort = IndiceMetadata.ColumnNames.Descricao + " ASC";

        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }
    #endregion

    protected void gridConsulta_CustomCallback(object sender, DevExpress.Web.ASPxGridViewCustomCallbackEventArgs e) {
        gridConsulta.DataBind();
        gridConsulta.Selection.UnselectAll();
        gridConsulta.CancelEdit();
    }
}