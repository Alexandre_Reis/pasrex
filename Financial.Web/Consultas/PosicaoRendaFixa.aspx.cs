﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using Financial.Security;
using Financial.ContaCorrente;
using System.Text;
using DevExpress.Web;
using Financial.Web.Util;
using System.Drawing;
using Financial.Investidor;
using Financial.RendaFixa;
using System.Collections.Generic;
using EntitySpaces.Interfaces;
using Financial.Common;
using Financial.Security.Enums;
using Financial.Web.Common;
using System.Threading;

public partial class Consultas_PosicaoRendaFixa : ConsultaBasePage {

    // Define se consulta será histórica ou não
    private enum TipoPesquisa {
        PosicaoRendaFixa = 0,
        PosicaoRendaFixaHistorico = 1
    }
    TipoPesquisa tipoPesquisa = TipoPesquisa.PosicaoRendaFixa;
        
    new protected void Page_Load(object sender, EventArgs e) {
        this.HasPopupCliente = true;
        btnEditCodigoCliente.Focus();
        base.Page_Load(sender, e);

        //Pager para Testar
        //this.gridConsulta.SettingsPager.PageSize = 2;
        //this.gridConsulta.Settings.VerticalScrollableHeight = 100;

        TrataTravamentoCampos();
    }

    #region DataSources
    protected void EsDSPosicaoRendaFixa_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e) {
        if (!Page.IsPostBack) {
            #region Condição Inicial
            gridConsulta.Visible = false;
            e.Collection = new PosicaoRendaFixaCollection();
            #endregion
        }
        else {
            // Confere Campos Obrigatorios para o caso de Mudança de Pagina e Scroolbar
            e.Collection = new PosicaoRendaFixaCollection();
            this.TrataCamposObrigatorios();

            TituloRendaFixaQuery tituloRendaFixaQuery = new TituloRendaFixaQuery("T");

            if (!String.IsNullOrEmpty(btnEditCodigoCliente.Text.Trim())) {
                #region Escolhido Apenas 1 Cliente
                //Checa se busca da posição atual ou histórica
                int idCliente = Convert.ToInt32(btnEditCodigoCliente.Text);
                DateTime data = Convert.ToDateTime(textData.Text);

                Cliente cliente = new Cliente();
                List<esQueryItem> campos = new List<esQueryItem>();
                campos.Add(cliente.Query.DataDia);
                cliente.LoadByPrimaryKey(idCliente);

                DateTime dataDia = cliente.DataDia.Value;

                bool historico = false;
                if (DateTime.Compare(data, dataDia) < 0) {
                    historico = true;
                }
                //

                PosicaoRendaFixaHistoricoQuery posicaoRendaFixaHistoricoQuery = new PosicaoRendaFixaHistoricoQuery("P");
                EmissorQuery emissorQuery = new EmissorQuery("E");

                PosicaoRendaFixaHistoricoCollection posicaoRendaFixaCollection = new PosicaoRendaFixaHistoricoCollection();
                posicaoRendaFixaHistoricoQuery.Select(posicaoRendaFixaHistoricoQuery,
                                                      (posicaoRendaFixaHistoricoQuery.ValorMercado - posicaoRendaFixaHistoricoQuery.ValorIR - posicaoRendaFixaHistoricoQuery.ValorIOF).As("ValorLiquido"),  
                                                      tituloRendaFixaQuery.DescricaoCompleta, 
                                                      emissorQuery.Nome.As("NomeEmissor"));
                posicaoRendaFixaHistoricoQuery.InnerJoin(tituloRendaFixaQuery).On(tituloRendaFixaQuery.IdTitulo == posicaoRendaFixaHistoricoQuery.IdTitulo);
                posicaoRendaFixaHistoricoQuery.InnerJoin(emissorQuery).On(emissorQuery.IdEmissor == tituloRendaFixaQuery.IdEmissor);
                posicaoRendaFixaHistoricoQuery.Where(posicaoRendaFixaHistoricoQuery.IdCliente == Convert.ToInt32(btnEditCodigoCliente.Text));
                posicaoRendaFixaHistoricoQuery.Where(posicaoRendaFixaHistoricoQuery.DataHistorico == Convert.ToDateTime(textData.Text));

                posicaoRendaFixaHistoricoQuery.OrderBy(posicaoRendaFixaHistoricoQuery.IdTitulo.Ascending);
                posicaoRendaFixaCollection.Load(posicaoRendaFixaHistoricoQuery);
                
                e.Collection = posicaoRendaFixaCollection;                
                #endregion
            }
            else {
                #region Varios Clientes
                ClienteCollection clienteCollection = new ClienteCollection();
                clienteCollection.BuscaClientesComAcesso(HttpContext.Current.User.Identity.Name);

                if (clienteCollection.Count == 0) { // Se nao tem Cliente com Acesso
                    e.Collection = new PosicaoRendaFixaCollection();
                }
                else {
                    DateTime data = Convert.ToDateTime(textData.Text);
                    // Collections usadas para o Bindind
                    PosicaoRendaFixaCollection pBinding = new PosicaoRendaFixaCollection();
                    pBinding.CreateColumnsForBinding();
                    pBinding.AddColumn("DescricaoCompleta", typeof(System.String));
                    pBinding.AddColumn("NomeEmissor", typeof(System.String));
                    pBinding.AddColumn("ValorLiquido", typeof(System.Decimal));
                    
                    // Para Cada Cliente de Acordo com a DataDia Consulta em Posicao ou PosicaoHistorico
                    for (int i = 0; i < clienteCollection.Count; i++) {
                        int idClienteAux = clienteCollection[i].IdCliente.Value;

                        Cliente cliente = new Cliente();
                        List<esQueryItem> campos = new List<esQueryItem>();
                        campos.Add(cliente.Query.DataDia);
                        cliente.LoadByPrimaryKey(campos, idClienteAux);

                        PosicaoRendaFixaHistoricoCollection posicaoRendaFixaHistoricoCollection = new PosicaoRendaFixaHistoricoCollection();                        

                        PosicaoRendaFixaHistoricoQuery posicaoRendaFixaHistoricoQuery = new PosicaoRendaFixaHistoricoQuery("P");
                        EmissorQuery emissorQuery = new EmissorQuery("E");
                        posicaoRendaFixaHistoricoQuery.Select(posicaoRendaFixaHistoricoQuery, 
                                                              (posicaoRendaFixaHistoricoQuery.ValorMercado - posicaoRendaFixaHistoricoQuery.ValorIR - posicaoRendaFixaHistoricoQuery.ValorIOF).As("ValorLiquido"),
                                                              tituloRendaFixaQuery.DescricaoCompleta, 
                                                              emissorQuery.Nome.As("NomeEmissor"));
                        posicaoRendaFixaHistoricoQuery.InnerJoin(tituloRendaFixaQuery).On(tituloRendaFixaQuery.IdTitulo == posicaoRendaFixaHistoricoQuery.IdTitulo);
                        posicaoRendaFixaHistoricoQuery.InnerJoin(emissorQuery).On(emissorQuery.IdEmissor == tituloRendaFixaQuery.IdEmissor);
                        posicaoRendaFixaHistoricoQuery.Where(posicaoRendaFixaHistoricoQuery.IdCliente == idClienteAux,
                                                             posicaoRendaFixaHistoricoQuery.DataHistorico.Equal(data));

                        posicaoRendaFixaHistoricoCollection.Load(posicaoRendaFixaHistoricoQuery);
                        //
                        for (int j = 0; j < posicaoRendaFixaHistoricoCollection.Count; j++)
                        {
                            PosicaoRendaFixa p = pBinding.AddNew();
                            p.IdCliente = posicaoRendaFixaHistoricoCollection[j].IdCliente;
                            p.DataOperacao = posicaoRendaFixaHistoricoCollection[j].DataOperacao;
                            p.IdTitulo = posicaoRendaFixaHistoricoCollection[j].IdTitulo;
                            p.DataVencimento = posicaoRendaFixaHistoricoCollection[j].DataVencimento;
                            p.TipoOperacao = posicaoRendaFixaHistoricoCollection[j].TipoOperacao;
                            p.Quantidade = posicaoRendaFixaHistoricoCollection[j].Quantidade;
                            p.PUOperacao = posicaoRendaFixaHistoricoCollection[j].PUOperacao;
                            p.QuantidadeBloqueada = posicaoRendaFixaHistoricoCollection[j].QuantidadeBloqueada;
                            p.PUMercado = posicaoRendaFixaHistoricoCollection[j].PUMercado;
                            p.ValorMercado = posicaoRendaFixaHistoricoCollection[j].ValorMercado;
                            p.PUCurva = posicaoRendaFixaHistoricoCollection[j].PUCurva;
                            p.ValorCurva = posicaoRendaFixaHistoricoCollection[j].ValorCurva;
                            p.ValorIR = posicaoRendaFixaHistoricoCollection[j].ValorIR;
                            p.ValorIOF = posicaoRendaFixaHistoricoCollection[j].ValorIOF;
                            p.TaxaOperacao = posicaoRendaFixaHistoricoCollection[j].TaxaOperacao;

                            p.SetColumn(TituloRendaFixaMetadata.ColumnNames.DescricaoCompleta, posicaoRendaFixaHistoricoCollection[j].GetColumn(TituloRendaFixaMetadata.ColumnNames.DescricaoCompleta));
                            p.SetColumn("NomeEmissor", posicaoRendaFixaHistoricoCollection[j].GetColumn("NomeEmissor"));
                            p.SetColumn("ValorLiquido", posicaoRendaFixaHistoricoCollection[j].GetColumn("ValorLiquido"));
                        }                                                      
                        
                    }
                    pBinding.Sort = PosicaoRendaFixaMetadata.ColumnNames.IdCliente + " ASC, " + PosicaoRendaFixaMetadata.ColumnNames.DataOperacao + " ASC";
                    e.Collection = pBinding;
                }               
                #endregion
            }

            // Define Grid como Visivel
            gridConsulta.Visible = true;
        }
    }

    protected void EsDSCliente_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e) {
        ClienteCollection clienteCollection = new ClienteCollection();
        clienteCollection.BuscaClientesComAcesso(HttpContext.Current.User.Identity.Name);

        e.Collection = clienteCollection;
    }

    protected void EsDSTituloRendaFixa_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e) {
        TituloRendaFixaCollection coll = new TituloRendaFixaCollection();

        coll.Query.OrderBy(coll.Query.Descricao.Ascending);
        coll.LoadAll();

        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }
    #endregion

    /// <summary>
    /// 
    /// </summary>
    private void TrataTravamentoCampos() {
        Usuario usuario = new Usuario();
        usuario.BuscaUsuario(HttpContext.Current.User.Identity.Name);
        int tipoTrava = usuario.TipoTrava.Value;

        if (tipoTrava == (int)TipoTravaUsuario.TravaCliente) {
            btnEditCodigoCliente.Enabled = false;

            PermissaoCliente permissaoCliente = new PermissaoCliente();
            int? idCliente = permissaoCliente.RetornaClienteAssociado(HttpContext.Current.User.Identity.Name);

            if (idCliente.HasValue) {
                Cliente cliente = new Cliente();

                List<esQueryItem> campos = new List<esQueryItem>();
                campos.Add(cliente.Query.Apelido);
                cliente.LoadByPrimaryKey(campos, idCliente.Value);
                string apelido = cliente.Apelido;

                btnEditCodigoCliente.Text = Convert.ToString(idCliente.Value);
                textNome.Text = apelido;
            }
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void ASPxCallback1_Callback(object source, DevExpress.Web.CallbackEventArgs e) {
        string nome = "";
        if (!String.IsNullOrEmpty(e.Parameter) && e.Parameter != "null") {
            int idCliente = Convert.ToInt32(e.Parameter);
            Cliente cliente = new Cliente();
            cliente.LoadByPrimaryKey(idCliente);

            if (cliente.str.Apelido != "") {
                if (cliente.IsAtivo) {
                    //O permissionamento da cliente é dado direto no cliente vinculado à cliente
                    PermissaoCliente permissaoCliente = new PermissaoCliente();
                    if (permissaoCliente.RetornaAcessoClienteComControle(idCliente, HttpContext.Current.User.Identity.Name)) {
                        DateTime dataDia = cliente.DataDia.Value;
                        nome = cliente.str.Apelido + "|" + dataDia.ToShortDateString() + "|" + Thread.CurrentThread.CurrentCulture.Name;
                    }
                    else {
                        nome = "no_access";
                    }
                }
                else {
                    nome = "no_active";
                }
            }
        }
        e.Result = nome;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridConsulta_HtmlDataCellPrepared(object sender, ASPxGridViewTableDataCellEventArgs e) {
        if (e.DataColumn.FieldName == "Quantidade") {
            decimal value = (decimal)e.GetValue("Quantidade");
            if (value < 0)
                e.Cell.ForeColor = Color.Red;
        }
        else if (e.DataColumn.FieldName == "ValorMercado") {
            decimal value = (decimal)e.GetValue("ValorMercado");
            if (value < 0)
                e.Cell.ForeColor = Color.Red;
        }
    }

    /// <summary>
    /// Imprime o Footer
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridConsulta_HtmlFooterCellPrepared(object sender, ASPxGridViewTableFooterCellEventArgs e) {
        GridViewDataColumn column = e.Column as GridViewDataColumn;
        if (column == null) return;

        ASPxSummaryItem item = this.gridConsulta.TotalSummary[column.FieldName];
        if (item == null) return;

        decimal summaryValue = Convert.ToDecimal(e.GetSummaryValue(item));

        if (summaryValue < 0) {
            e.Cell.ForeColor = Color.Red;
        }

        // Escreve Total na Primeira Coluna
        if (column.FieldName == "IdCliente") {
            e.Cell.Text = "Total: ";
        }

        if (this.gridConsulta.VisibleRowCount == 0) {
            e.Cell.Text = ""; // Quando tem 0 registros imprime 0.00
        }
    }

    #region btnRun_Click, btnPDF_Click, btnExcel_Click

    /// <summary>
    /// Trata os campos Obrigatorios do Form
    /// </summary>
    private void TrataCamposObrigatorios() {
        #region Campos obrigatórios
        List<Control> controles = new List<Control>(new Control[] { 
            this.textData });

        if (base.TestaObrigatorio(controles) != "") {
            throw new Exception("Campos com * são obrigatórios!");
        }
        #endregion
    }

    new protected void btnRun_Click(object sender, EventArgs e) {
        this.TrataCamposObrigatorios();
        base.btnRun_Click(sender, e); // DataBind do Grid
    }

    new protected void btnPDF_Click(object sender, EventArgs e) {
        this.TrataCamposObrigatorios();
        this.SelecionaRelatorio(VisaoRelatorio.visaoPDF);
    }

    new protected void btnExcel_Click(object sender, EventArgs e) {
        this.TrataCamposObrigatorios();
        this.SelecionaRelatorio(VisaoRelatorio.visaoExcel);
    }

    /// <summary>
    /// Seleciona o Relatorio a Ser Executado
    /// </summary>
    /// <param name="visao">PDF, Excel ou Report</param>
    private void SelecionaRelatorio(string visao) {
        Response.BufferOutput = true;
        //
        // Salva os Parametros na Session
        Session["gridExportPosicaoRendaFixa"] = gridExport;
        Response.Redirect("~/Consultas/PosicaoRendaFixaExporta.aspx?Visao=" + visao);
    }

    #endregion
}