﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using Financial.Security;
using System.Text;
using DevExpress.Web;
using Financial.Web.Util;
using System.Drawing;
using Financial.Investidor;
using System.Collections.Generic;
using EntitySpaces.Interfaces;
using Financial.Common;
using Financial.InvestidorCotista;
using Financial.Fundo;
using Financial.Security.Enums;
using Financial.Web.Common;

public partial class Consultas_PrejuizoCotistaPorFundo : ConsultaBasePage {
    new protected void Page_Load(object sender, EventArgs e) {
        this.HasPopupCarteira = true;
        this.HasPopupCotista = true;
        btnEditCodigoCarteira.Focus();
        base.Page_Load(sender, e);

        TrataTravamentoCampos();
    }

    #region DataSources
    protected void EsDSPrejuizoCotista_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e) {
        if (!Page.IsPostBack) {
            gridConsulta.Visible = false;
            e.Collection = new PrejuizoCotistaCollection();
        }
        else {

            // Confere Campos Obrigatorios para o caso de Mudança de Pagina e Scroolbar
            this.TrataCamposObrigatorios();
            //

            //Checa se busca da posição atual ou histórica
            DateTime data = Convert.ToDateTime(textData.Text);

            Usuario usuario = new Usuario();
            usuario.BuscaUsuario(Context.User.Identity.Name);
            int idUsuario = usuario.IdUsuario.Value;

            PrejuizoCotistaCollection prejuizoCotistaCollection = new PrejuizoCotistaCollection();
            
            CotistaQuery cotistaQuery = new CotistaQuery("C");
            ClienteQuery clienteQuery = new ClienteQuery("L");
            PrejuizoCotistaQuery prejuizoCotistaQuery = new PrejuizoCotistaQuery("P");
            PermissaoCotistaQuery permissaoCotistaQuery = new PermissaoCotistaQuery("R");

            prejuizoCotistaQuery.Select(prejuizoCotistaQuery, cotistaQuery.Apelido.As("ApelidoCotista"), clienteQuery.Apelido.As("ApelidoCarteira"));
            prejuizoCotistaQuery.InnerJoin(cotistaQuery).On(cotistaQuery.IdCotista == prejuizoCotistaQuery.IdCotista);
            prejuizoCotistaQuery.InnerJoin(clienteQuery).On(clienteQuery.IdCliente == prejuizoCotistaQuery.IdCarteira);
            prejuizoCotistaQuery.InnerJoin(permissaoCotistaQuery).On(permissaoCotistaQuery.IdCotista == prejuizoCotistaQuery.IdCotista);
            prejuizoCotistaQuery.Where(permissaoCotistaQuery.IdUsuario.Equal(idUsuario));
            prejuizoCotistaQuery.Where(clienteQuery.DataDia.LessThanOrEqual(Convert.ToDateTime(textData.Text)));
            prejuizoCotistaQuery.Where(prejuizoCotistaQuery.ValorPrejuizo.NotEqual(0));

            if (btnEditCodigoCotista.Text != "") 
            {
                prejuizoCotistaQuery.Where(prejuizoCotistaQuery.IdCotista.Equal(Convert.ToInt32(btnEditCodigoCotista.Text)));
            }

            if (btnEditCodigoCarteira.Text != "")
            {
                prejuizoCotistaQuery.Where(prejuizoCotistaQuery.IdCarteira.Equal(Convert.ToInt32(btnEditCodigoCarteira.Text)));
            }

            prejuizoCotistaQuery.OrderBy(prejuizoCotistaQuery.IdCotista.Ascending,
                                        prejuizoCotistaQuery.Data.Descending);
                            
            prejuizoCotistaCollection.Load(prejuizoCotistaQuery);
            

            cotistaQuery = new CotistaQuery("C");
            clienteQuery = new ClienteQuery("L");
            PrejuizoCotistaHistoricoQuery prejuizoCotistaHistoricoQuery = new PrejuizoCotistaHistoricoQuery("P");
            permissaoCotistaQuery = new PermissaoCotistaQuery("R");

            prejuizoCotistaHistoricoQuery.Select(prejuizoCotistaHistoricoQuery, cotistaQuery.Apelido.As("ApelidoCotista"), clienteQuery.Apelido.As("ApelidoCarteira"));
            prejuizoCotistaHistoricoQuery.InnerJoin(cotistaQuery).On(cotistaQuery.IdCotista == prejuizoCotistaHistoricoQuery.IdCotista);
            prejuizoCotistaHistoricoQuery.InnerJoin(clienteQuery).On(clienteQuery.IdCliente == prejuizoCotistaHistoricoQuery.IdCarteira);
            prejuizoCotistaHistoricoQuery.InnerJoin(permissaoCotistaQuery).On(permissaoCotistaQuery.IdCotista == prejuizoCotistaHistoricoQuery.IdCotista);
            prejuizoCotistaHistoricoQuery.Where(permissaoCotistaQuery.IdUsuario.Equal(idUsuario));
            prejuizoCotistaHistoricoQuery.Where(prejuizoCotistaHistoricoQuery.DataHistorico.Equal(Convert.ToDateTime(textData.Text)));
            prejuizoCotistaHistoricoQuery.Where(clienteQuery.DataDia.GreaterThan(Convert.ToDateTime(textData.Text)));
            prejuizoCotistaHistoricoQuery.Where(prejuizoCotistaHistoricoQuery.ValorPrejuizo.NotEqual(0));

            if (btnEditCodigoCotista.Text != "")
            {
                prejuizoCotistaHistoricoQuery.Where(prejuizoCotistaHistoricoQuery.IdCotista.Equal(Convert.ToInt32(btnEditCodigoCotista.Text)));
            }

            if (btnEditCodigoCarteira.Text != "")
            {
                prejuizoCotistaHistoricoQuery.Where(prejuizoCotistaHistoricoQuery.IdCarteira.Equal(Convert.ToInt32(btnEditCodigoCarteira.Text)));
            }

            prejuizoCotistaHistoricoQuery.OrderBy(prejuizoCotistaHistoricoQuery.IdCotista.Ascending,
                                                 prejuizoCotistaHistoricoQuery.Data.Descending);

            PrejuizoCotistaHistoricoCollection prejuizoCotistaHistoricoCollection = new PrejuizoCotistaHistoricoCollection();
            prejuizoCotistaHistoricoCollection.Load(prejuizoCotistaHistoricoQuery);

            PrejuizoCotistaCollection prejuizoCotistaCollectionAux = new PrejuizoCotistaCollection(prejuizoCotistaHistoricoCollection);
            // Adiciona Colunas Extras
            prejuizoCotistaCollectionAux.CreateColumnsForBinding();
            prejuizoCotistaCollectionAux.AddColumn("ApelidoCotista", typeof(System.String));
            prejuizoCotistaCollectionAux.AddColumn("ApelidoCarteira", typeof(System.String));

            // Copia Campos de ApelidoCotista e ApelidoCarteira
            for (int i = 0; i < prejuizoCotistaHistoricoCollection.Count; i++) {
                prejuizoCotistaCollectionAux[i].SetColumn("ApelidoCotista", prejuizoCotistaHistoricoCollection[i].GetColumn("ApelidoCotista"));
                prejuizoCotistaCollectionAux[i].SetColumn("ApelidoCarteira", prejuizoCotistaHistoricoCollection[i].GetColumn("ApelidoCarteira"));                
            }

            prejuizoCotistaCollection.Combine(prejuizoCotistaCollectionAux);

            
            e.Collection = prejuizoCotistaCollection;
        
            gridConsulta.Visible = true;
        }

    }

    protected void EsDSCarteira_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e) {
        CarteiraCollection carteiraCollection = new CarteiraCollection();
        carteiraCollection.BuscaCarteirasComAcessoTodas(HttpContext.Current.User.Identity.Name);

        e.Collection = carteiraCollection;
    }

    protected void EsDSCotista_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e) {
        CotistaCollection cotistaCollection = new CotistaCollection();
        cotistaCollection.BuscaCotistasComAcesso(HttpContext.Current.User.Identity.Name);

        e.Collection = cotistaCollection;
    }
    #endregion

    /// <summary>
    /// 
    /// </summary>
    private void TrataTravamentoCampos() {
        Usuario usuario = new Usuario();
        usuario.BuscaUsuario(HttpContext.Current.User.Identity.Name);
        int tipoTrava = usuario.TipoTrava.Value;

        if (tipoTrava == (int)TipoTravaUsuario.TravaCliente) {
            btnEditCodigoCarteira.Enabled = false;

            PermissaoCliente permissaoCliente = new PermissaoCliente();
            int? idCliente = permissaoCliente.RetornaClienteAssociado(HttpContext.Current.User.Identity.Name);

            if (idCliente.HasValue) {
                Cliente cliente = new Cliente();

                List<esQueryItem> campos = new List<esQueryItem>();
                campos.Add(cliente.Query.Apelido);
                cliente.LoadByPrimaryKey(campos, idCliente.Value);
                string apelido = cliente.Apelido;

                btnEditCodigoCarteira.Text = Convert.ToString(idCliente.Value);
                textNome.Text = apelido;
            }
        }

        if (tipoTrava == (int)TipoTravaUsuario.TravaCotista) {
            //Travamento do campo de cotista
            btnEditCodigoCotista.Enabled = false;

            PermissaoCotista permissaoCotista = new PermissaoCotista();
            int? idCotista = permissaoCotista.RetornaCotistaAssociado(HttpContext.Current.User.Identity.Name);

            if (idCotista.HasValue) {
                Cotista cotista = new Cotista();

                List<esQueryItem> campos = new List<esQueryItem>();
                campos.Add(cotista.Query.Apelido);
                cotista.LoadByPrimaryKey(campos, idCotista.Value);
                string apelido = cotista.Apelido;

                btnEditCodigoCotista.Text = Convert.ToString(idCotista.Value);
                textNomeCotista.Text = apelido;
            }
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void ASPxCallback1_Callback(object source, DevExpress.Web.CallbackEventArgs e) {
        string nome = "";
        if (!String.IsNullOrEmpty(e.Parameter) && e.Parameter != "null") {
            int idCarteira = Convert.ToInt32(e.Parameter);
            Carteira carteira = new Carteira();
            carteira.LoadByPrimaryKey(idCarteira);

            if (carteira.str.Apelido != "") {
                if (carteira.IsAtivo) {
                    //O permissionamento da carteira é dado direto no cliente vinculado à carteira
                    PermissaoCliente permissaoCliente = new PermissaoCliente();
                    if (permissaoCliente.RetornaAcessoCliente(idCarteira, HttpContext.Current.User.Identity.Name)) {
                        nome = carteira.str.Apelido;
                    }
                    else {
                        nome = "no_access";
                    }
                }
                else {
                    nome = "no_active";
                }
            }
        }
        e.Result = nome;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void ASPxCallback2_Callback(object source, DevExpress.Web.CallbackEventArgs e) {
        string nome = "";
        if (!String.IsNullOrEmpty(e.Parameter) && e.Parameter != "null") {
            int idCotista = Convert.ToInt32(e.Parameter);
            Cotista cotista = new Cotista();
            cotista.LoadByPrimaryKey(idCotista);

            if (cotista.str.Apelido != "") {
                if (cotista.IsAtivo) {
                    PermissaoCotista permissaoCotista = new PermissaoCotista();
                    if (permissaoCotista.RetornaAcessoCotista(idCotista, HttpContext.Current.User.Identity.Name)) {
                        nome = cotista.str.Apelido;
                    }
                    else {
                        nome = "no_access";
                    }
                }
                else {
                    nome = "no_active";
                }
            }
        }
        e.Result = nome;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridConsulta_HtmlDataCellPrepared(object sender, ASPxGridViewTableDataCellEventArgs e) {
        if (e.DataColumn.FieldName == "ValorPrejuizo") {
            e.Cell.ForeColor = Color.Red;
        }
    }

    #region btnRun_Click, btnPDF_Click, btnExcel_Click

    /// <summary>
    /// Trata os campos Obrigatorios do Form
    /// </summary>
    private void TrataCamposObrigatorios() {
        #region Campos obrigatórios
        List<Control> controles = new List<Control>(new Control[] { this.textData });

        if (base.TestaObrigatorio(controles) != "") {
            throw new Exception("Campos com * são obrigatórios!");
        }
        #endregion
    }

    new protected void btnRun_Click(object sender, EventArgs e) {
        this.TrataCamposObrigatorios();
        base.btnRun_Click(sender, e); // DataBind do Grid
    }

    new protected void btnPDF_Click(object sender, EventArgs e) {
        this.TrataCamposObrigatorios();
        this.SelecionaRelatorio(VisaoRelatorio.visaoPDF);
    }

    new protected void btnExcel_Click(object sender, EventArgs e) {
        this.TrataCamposObrigatorios();
        this.SelecionaRelatorio(VisaoRelatorio.visaoExcel);
    }

    /// <summary>
    /// Seleciona o Relatorio a Ser Executado
    /// </summary>
    /// <param name="visao">PDF, Excel ou Report</param>
    private void SelecionaRelatorio(string visao) {
        Response.BufferOutput = true;
        //
        // Salva os Parametros na Session
        Session["gridExportPrejuizoCotistaPorFundo"] = gridExport;
        Response.Redirect("~/Consultas/PrejuizoCotistaPorFundoExporta.aspx?Visao=" + visao);
    }

    #endregion
}