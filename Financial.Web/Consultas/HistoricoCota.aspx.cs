﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using Financial.Security;
using Financial.ContaCorrente;
using System.Text;
using DevExpress.Web;
using Financial.Web.Util;
using System.Drawing;
using Financial.Investidor;
using Financial.Fundo;
using Financial.Fundo.Enums;
using Financial.Fundo.Exceptions;
using Financial.Security.Enums;
using System.Collections.Generic;
using EntitySpaces.Interfaces;
using Financial.Web.Common;
using Financial.Util;
using Financial.Investidor.Enums;

public partial class Consultas_HistoricoCota : ConsultaBasePage 
{
    new protected void Page_Load(object sender, EventArgs e) {
        this.HasPopupCarteira = true;
        btnEditCodigoCarteira.Focus();
        base.Page_Load(sender, e);
    }

    #region DataSources
    protected void EsDSHistoricoCota_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e) {
        if (!Page.IsPostBack) {
            gridConsulta.Visible = false;
            e.Collection = new SaldoCaixaCollection();
        }
        else {
            // Confere Campos Obrigatorios para o caso de Mudança de Pagina e Scroolbar
            this.TrataCamposObrigatorios();
            //
            HistoricoCotaCollection historicoCotaCollection = new HistoricoCotaCollection();

            HistoricoCotaQuery historicoCotaQuery = new HistoricoCotaQuery("H");
            CarteiraQuery carteiraQuery = new CarteiraQuery("C");
            ClienteQuery clienteQuery = new ClienteQuery("D");
            historicoCotaQuery.InnerJoin(carteiraQuery).On(historicoCotaQuery.IdCarteira.Equal(carteiraQuery.IdCarteira));
            historicoCotaQuery.InnerJoin(clienteQuery).On(carteiraQuery.IdCarteira.Equal(clienteQuery.IdCliente));

            if (btnEditCodigoCarteira.Text != "") {
                historicoCotaQuery.Where(historicoCotaQuery.IdCarteira.Equal(Convert.ToInt32(btnEditCodigoCarteira.Text)));
            }
            if (textDataInicio.Text != "") {
                historicoCotaQuery.Where(historicoCotaQuery.Data.GreaterThanOrEqual(Convert.ToDateTime(textDataInicio.Text)));
            }
            if (textDataFim.Text != "") {
                historicoCotaQuery.Where(historicoCotaQuery.Data.LessThanOrEqual(Convert.ToDateTime(textDataFim.Text)));
            }
            if (dropTipoCliente.Text != "")
            {
                historicoCotaQuery.Where(clienteQuery.IdTipo.Equal(Convert.ToInt32(dropTipoCliente.SelectedItem.Value)));
            }

            historicoCotaQuery.Where(clienteQuery.StatusAtivo.Equal(StatusAtivoCliente.Ativo));

            historicoCotaQuery.OrderBy(historicoCotaCollection.Query.Data.Descending);

            historicoCotaCollection.Load(historicoCotaQuery);

            //
            // Adiciona Colunas Extras
            HistoricoCotaCollection historicoCotaCollectionAux = new HistoricoCotaCollection();
            historicoCotaCollectionAux.CreateColumnsForBinding();
            //
            historicoCotaCollectionAux.AddColumn("RetornoDia", typeof(System.Decimal));
            historicoCotaCollectionAux.AddColumn("RetornoMes", typeof(System.Decimal));
            historicoCotaCollectionAux.AddColumn("RetornoAno", typeof(System.Decimal));
            //
            for (int i = 0; i < historicoCotaCollection.Count; i++) {
                int idCarteira = historicoCotaCollection[i].IdCarteira.Value;
                DateTime data = historicoCotaCollection[i].Data.Value;
                //
                CalculoMedida calculoMedida = new CalculoMedida(idCarteira);

                calculoMedida.SetAjustaCota(ParametrosConfiguracaoSistema.Fundo.RetornoFDICAjustado == "S");

                #region Retornos
                decimal retornoDia;
                try {
                    retornoDia = calculoMedida.CalculaRetornoDia(data);
                }
                catch (HistoricoCotaNaoCadastradoException) {
                    retornoDia = 0M;
                }
    
                decimal retornoMes;
                try {
                    retornoMes = calculoMedida.CalculaRetornoMes(data);
                }
                catch (HistoricoCotaNaoCadastradoException) {
                    retornoMes = 0M;
                }

                decimal retornoAno;
                try {
                    retornoAno = calculoMedida.CalculaRetornoAno(data);
                }
                catch (HistoricoCotaNaoCadastradoException) {
                    retornoAno = 0M;
                }
                #endregion

                // Seta Valores
                HistoricoCota h = historicoCotaCollectionAux.AddNew();
                //
                h.Data = historicoCotaCollection[i].Data.Value;
                h.IdCarteira = historicoCotaCollection[i].IdCarteira.Value;
                h.CotaBruta = historicoCotaCollection[i].CotaBruta.Value;
                h.CotaAbertura = historicoCotaCollection[i].CotaAbertura.Value;
                h.CotaFechamento = historicoCotaCollection[i].CotaFechamento.Value;
                h.PLAbertura = historicoCotaCollection[i].PLAbertura.Value;
                h.PLFechamento = historicoCotaCollection[i].PLFechamento.Value;
                h.PatrimonioBruto = historicoCotaCollection[i].PatrimonioBruto.Value;
                h.QuantidadeFechamento = historicoCotaCollection[i].QuantidadeFechamento.Value;
                //
                h.SetColumn("RetornoDia", retornoDia);
                h.SetColumn("RetornoMes", retornoMes);
                h.SetColumn("RetornoAno", retornoAno);
            }

            gridConsulta.Visible = true;
            e.Collection = historicoCotaCollectionAux;
        }
    }

    protected void EsDSCarteira_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e) {
        CarteiraCollection carteiraCollection = new CarteiraCollection();
        carteiraCollection.BuscaCarteirasComAcessoTodas(HttpContext.Current.User.Identity.Name);

        e.Collection = carteiraCollection;
    }

    protected void EsDSTipoCliente_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        TipoClienteCollection tipoClienteCollection = new TipoClienteCollection();
        tipoClienteCollection.LoadAll();

        e.Collection = tipoClienteCollection;
    }
    #endregion

    /// <summary>
    /// 
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void ASPxCallback1_Callback(object source, DevExpress.Web.CallbackEventArgs e) {
        string nome = "";
        if (!String.IsNullOrEmpty(e.Parameter) && e.Parameter != "null") {
            int idCarteira = Convert.ToInt32(e.Parameter);
            Carteira carteira = new Carteira();
            carteira.LoadByPrimaryKey(idCarteira);

            if (carteira.str.Apelido != "") {
                if (carteira.IsAtivo) {
                    //O permissionamento da carteira é dado direto no cliente vinculado à carteira
                    PermissaoCliente permissaoCliente = new PermissaoCliente();
                    if (permissaoCliente.RetornaAcessoCliente(idCarteira, HttpContext.Current.User.Identity.Name)) {
                        nome = carteira.str.Apelido;
                    }
                    else {
                        nome = "no_access";
                    }
                }
                else {
                    nome = "no_active";
                }
            }
        }
        e.Result = nome;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridConsulta_HtmlDataCellPrepared(object sender, ASPxGridViewTableDataCellEventArgs e) {
        if (e.DataColumn.FieldName == "RetornoDia") {
            decimal value = (decimal)e.GetValue("RetornoDia");
            if (value < 0)
                e.Cell.ForeColor = Color.Red;
        }
        else if (e.DataColumn.FieldName == "RetornoMes") {
            decimal value = (decimal)e.GetValue("RetornoMes");
            if (value < 0)
                e.Cell.ForeColor = Color.Red;
        }
        else if (e.DataColumn.FieldName == "RetornoAno") {
            decimal value = (decimal)e.GetValue("RetornoAno");
            if (value < 0)
                e.Cell.ForeColor = Color.Red;
        }
    }
        
    #region btnRun_Click, btnPDF_Click, btnExcel_Click

    /// <summary>
    /// Trata os campos Obrigatorios do Form
    /// </summary>
    private void TrataCamposObrigatorios() {
        #region Campos obrigatórios
        List<Control> controles = new List<Control>(new Control[] { 
            this.textDataInicio, this.textDataFim });

        if (base.TestaObrigatorio(controles) != "") {
            throw new Exception("Campos com * são obrigatórios!");
        }
        #endregion
    }

    new protected void btnRun_Click(object sender, EventArgs e) {
        this.TrataCamposObrigatorios();
        base.btnRun_Click(sender, e); // DataBind do Grid
    }

    new protected void btnPDF_Click(object sender, EventArgs e) {
        this.TrataCamposObrigatorios();
        this.SelecionaRelatorio(VisaoRelatorio.visaoPDF);
    }

    new protected void btnExcel_Click(object sender, EventArgs e) {
        this.TrataCamposObrigatorios();
        this.SelecionaRelatorio(VisaoRelatorio.visaoExcel);
    }

    /// <summary>
    /// Seleciona o Relatorio a Ser Executado
    /// </summary>
    /// <param name="visao">PDF, Excel ou Report</param>
    private void SelecionaRelatorio(string visao) {
        Response.BufferOutput = true;
        //
        // Salva os Parametros na Session
        Session["gridExportHistoricoCota"] = gridExport;
        Response.Redirect("~/Consultas/HistoricoCotaExporta.aspx?Visao=" + visao);
    }

    #endregion
}