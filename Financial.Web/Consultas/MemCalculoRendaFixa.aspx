﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="MemCalculoRendaFixa.aspx.cs"
    Inherits="CadastrosBasicos_MemCalculoRendaFixa" %>

<%@ Register Assembly="System.Web, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"
    Namespace="System.Web.UI.HtmlControls" TagPrefix="cc2" %>
<%@ Register Assembly="EntitySpaces.Web" Namespace="EntitySpaces.Web" TagPrefix="cc1" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxe" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxcb" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxpc" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxwgv" %>

<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <link href="~/css/forms.css" rel="stylesheet" type="text/css" />

    <script type="text/javascript" language="Javascript" src="../../js/global.js"></script>

    <script type="text/javascript" language="Javascript">
        var popup = true;
        document.onkeydown = onDocumentKeyDown;

        function OnGetDataCliente(data) 
        {
            btnEditCodigoCliente.SetValue(data);        
            callbackCliente.SendCallback(btnEditCodigoCliente.GetValue());
            popupCliente.HideWindow();
            btnEditCodigoCliente.Focus();
        }     
        
        function AdjustSize() {
            var height = Math.max(0, document.documentElement.clientHeight);
            height -= height * 0.25;
            gridConsulta.SetHeight(height);   
        }
        window.onresize = AdjustSize;           
    </script>

</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="true"
            EnableScriptLocalization="true" />
        <dxcb:ASPxCallback ID="callbackCliente" runat="server" OnCallback="callBackCliente_Callback">
            <ClientSideEvents CallbackComplete="function(s, e) {                    
                    OnCallBackCompleteCliente(s, e, popupMensagemCliente, btnEditCodigoCliente, document.getElementById('textNome'));              
            }" />
        </dxcb:ASPxCallback>
        <div class="divPanel">
            <table width="100%">
                <tr>
                    <td>
                        <div id="container_small">
                            <div id="header">
                                <asp:Label ID="lblHeader" runat="server" Text="Memória de cálculo "></asp:Label>
                            </div>
                            <div id="mainContent">
                                <div class="reportFilter">
                                    <div style="height: 20px">
                                    </div>
                                    <table cellpadding="2" cellspacing="2">
                                        <tr>
                                            <td class="td_Label">
                                                <asp:Label ID="labelCliente" runat="server" CssClass="labelRequired" Text="Cliente:" />
                                            </td>
                                            <td>
                                                <dxe:ASPxSpinEdit ID="btnEditCodigoCliente" runat="server" CssClass="textButtonEdit"
                                                    EnableClientSideAPI="True" ClientInstanceName="btnEditCodigoCliente" MaxLength="15"
                                                    NumberType="Integer" AllowMouseWheel="false">
                                                    <Buttons>
                                                        <dxe:EditButton>
                                                        </dxe:EditButton>
                                                    </Buttons>
                                                    <ClientSideEvents KeyPress="function(s, e) {document.getElementById('textNome').value = '';}"
                                                        ButtonClick="function(s, e) {popupCliente.ShowAtElementByID(s.name);}" LostFocus="function(s, e) {OnLostFocus(popupMensagemCliente, callbackCliente, btnEditCodigoCliente);}" />
                                                </dxe:ASPxSpinEdit>
                                            </td>
                                            <td colspan="2">
                                                <asp:TextBox ID="textNome" runat="server" CssClass="textNome" Enabled="false"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="td_Label">
                                                <asp:Label ID="labelDataInicio" runat="server" CssClass="labelRequired" Text="Início:"></asp:Label>
                                            </td>
                                            <td>
                                                <dxe:ASPxDateEdit ID="textDataInicio" runat="server" ClientInstanceName="textDataInicio" />
                                            </td>
                                            <td colspan="2">
                                                <table>
                                                    <tr>
                                                        <td>
                                                            <asp:Label ID="labelDataFim" runat="server" CssClass="labelRequired" Text="Fim:" /></td>
                                                        <td>
                                                            <dxe:ASPxDateEdit ID="textDataFim" runat="server" ClientInstanceName="textDataFim" />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="td_Label">
                                                <asp:Label ID="labelTitulo" runat="server" CssClass="labelRequired" Text="Título:" />
                                            </td>
                                            <td colspan="2" width="450">
                                                <dx:ASPxGridLookup ID="dropTituloRendaFixa" ClientInstanceName="dropTituloRendaFixa"
                                                    runat="server" SelectionMode="Single" KeyFieldName="IdTitulo" DataSourceID="EsDSTituloRendaFixaFiltro"
                                                    Width="450" TextFormatString="{5}" Font-Size="11px" IncrementalFilteringMode="contains"
                                                    AllowUserInput="false">
                                                    <Columns>
                                                        <dxwgv:GridViewCommandColumn ShowSelectCheckbox="True" Width="4%" />
                                                        <dxwgv:GridViewDataColumn FieldName="IdTitulo" Caption="Id" Width="10%" CellStyle-Font-Size="X-Small" />
                                                        <dxwgv:GridViewDataColumn FieldName="DescricaoPapel" Caption="Papel" CellStyle-Font-Size="X-Small"
                                                            Settings-AutoFilterCondition="Contains" Width="20%" />
                                                        <dxwgv:GridViewDataColumn FieldName="Descricao" Caption="Descrição" CellStyle-Font-Size="X-Small"
                                                            Settings-AutoFilterCondition="Contains" Width="45%" />
                                                        <dxwgv:GridViewDataDateColumn FieldName="DataEmissao" Caption="Data Emissão" CellStyle-Font-Size="X-Small"
                                                            Width="10%" />
                                                        <dxwgv:GridViewDataDateColumn FieldName="DataVencimento" Caption="Data Vencimento"
                                                            CellStyle-Font-Size="X-Small" Width="10%" />
                                                        <dxwgv:GridViewDataTextColumn FieldName="DescricaoCompleta" Caption="Descrição Completa"
                                                            CellStyle-Font-Size="X-Small" Settings-AutoFilterCondition="Contains" Visible="false" />
                                                    </Columns>
                                                    <GridViewProperties>
                                                        <Settings ShowFilterRow="True" />
                                                    </GridViewProperties>
                                                </dx:ASPxGridLookup>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                                <div id="progressBar" class="progressBar" runat="server">
                                    <asp:UpdateProgress ID="UpdateProgress1" runat="server">
                                        <ProgressTemplate>
                                            <dxe:ASPxImage runat="server" ID="roller" ImageUrl="~/imagens/roller.gif" ClientInstanceName="roller" />
                                            <asp:Label ID="lblCarregar" runat="server" Text="Carregando..." />
                                        </ProgressTemplate>
                                    </asp:UpdateProgress>
                                </div>
                                <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                    <ContentTemplate>
                                        <div class="linkButton linkButtonNoBorder">
                                            <asp:LinkButton ID="btnRun" runat="server" Font-Overline="false" CssClass="btnRun"
                                                OnClick="btnRun_Click">
                                                <asp:Literal ID="Literal1" runat="server" Text="Consultar" /><div>
                                                </div>
                                            </asp:LinkButton>
                                            <asp:LinkButton ID="btnPDF" runat="server" Font-Overline="false" CssClass="btnPdf"
                                                OnClick="btnPDF_Click">
                                                <asp:Literal ID="Literal2" runat="server" Text="Gerar PDF" /><div>
                                                </div>
                                            </asp:LinkButton>
                                            <asp:LinkButton ID="btnExcel" runat="server" Font-Overline="false" CssClass="btnExcel"
                                                OnClick="btnExcel_Click">
                                                <asp:Literal ID="Literal3" runat="server" Text="Gerar Excel" /><div>
                                                </div>
                                            </asp:LinkButton>
                                        </div>
                                        <div class="divDataGrid">
                                            <div class="divDataGrid">
                                                <dxwgv:ASPxGridView ID="gridConsulta" ClientInstanceName="gridConsulta" runat="server"
                                                    DataSourceID="EsDSMemCalculoRendaFixa" OnCustomCallback="gridConsulta_CustomCallback"
                                                    AutoGenerateColumns="False" OnLoad="gridConsulta_Load" Width="100%" Settings-HorizontalScrollBarMode="Visible"
                                                    SettingsBehavior-ColumnResizeMode="Control">
                                                    <Columns>
                                                        <dxwgv:GridViewCommandColumn ShowSelectCheckbox="True" VisibleIndex="0" Width="5%" Visible="false" ButtonType="Image" ShowClearFilterButton="True">
                                                            <HeaderTemplate>
                                                                <dxe:ASPxCheckBox ID="cbAll" ClientInstanceName="cbAll" runat="server" ToolTip="Seleciona todos os CheckBoxs de todas as páginas" BackColor="white" ClientSideEvents-CheckedChanged="function(s, e) {OnAllCheckedChanged(s, e, gridConsulta);}" OnInit="CheckBoxSelectAll"/>
                                                            </HeaderTemplate>
                                                        </dxwgv:GridViewCommandColumn>
                                                        <dxwgv:GridViewDataDateColumn FieldName="DataAtual" Caption="Dt.Atual" ReadOnly="True"
                                                            VisibleIndex="1">
                                                        </dxwgv:GridViewDataDateColumn>
                                                        <dxwgv:GridViewDataTextColumn FieldName="ClienteID" ReadOnly="True" VisibleIndex="2"
                                                            Caption="Id.Cliente">
                                                        </dxwgv:GridViewDataTextColumn>
                                                        <dxwgv:GridViewDataTextColumn FieldName="Apelido" ReadOnly="True" VisibleIndex="3"
                                                            Caption="Nome">
                                                        </dxwgv:GridViewDataTextColumn>
                                                        <dxwgv:GridViewDataTextColumn FieldName="IdOperacao" ReadOnly="True" VisibleIndex="2"
                                                            Caption="Id.Operação">
                                                        </dxwgv:GridViewDataTextColumn>
                                                        <dxwgv:GridViewDataDateColumn FieldName="DataFluxo" Caption="Dt.Fluxo" ReadOnly="True"
                                                            VisibleIndex="3">
                                                        </dxwgv:GridViewDataDateColumn>
                                                        <dxwgv:GridViewDataComboBoxColumn FieldName="TipoPreco" ReadOnly="True" VisibleIndex="4">
                                                            <PropertiesComboBox ValueType="System.String">
                                                            </PropertiesComboBox>
                                                        </dxwgv:GridViewDataComboBoxColumn>
                                                        <dxwgv:GridViewDataComboBoxColumn FieldName="TipoProcessamento" ReadOnly="True" VisibleIndex="5">
                                                            <PropertiesComboBox ValueType="System.String">
                                                            </PropertiesComboBox>
                                                        </dxwgv:GridViewDataComboBoxColumn>
                                                        <dxwgv:GridViewDataComboBoxColumn FieldName="TipoEventoTitulo" ReadOnly="True" VisibleIndex="6">
                                                            <PropertiesComboBox ValueType="System.String">
                                                            </PropertiesComboBox>
                                                        </dxwgv:GridViewDataComboBoxColumn>
                                                        <dxwgv:GridViewDataTextColumn FieldName="DescricaoTitulo" Caption="Título" VisibleIndex="7">
                                                        </dxwgv:GridViewDataTextColumn>
                                                        <dxwgv:GridViewDataDateColumn FieldName="DataVencimento" Caption="Dt.Vencimento"
                                                            VisibleIndex="8">
                                                        </dxwgv:GridViewDataDateColumn>
                                                        <dxwgv:GridViewDataTextColumn FieldName="DescricaoPapel" Caption="Papel" VisibleIndex="9">
                                                        </dxwgv:GridViewDataTextColumn>
                                                        <dxwgv:GridViewDataTextColumn FieldName="TaxaDesconto" Caption="Taxa Desconto - %a.a."
                                                            VisibleIndex="10">
                                                            <PropertiesTextEdit DisplayFormatString="{0:#,##0.00000000;(#,##0.00000000);0.00000000}">
                                                            </PropertiesTextEdit>
                                                        </dxwgv:GridViewDataTextColumn>
                                                        <dxwgv:GridViewDataTextColumn FieldName="ValorPresente" VisibleIndex="11">
                                                            <PropertiesTextEdit DisplayFormatString="{0:#,##0.00;(#,##0.00);0.00}">
                                                            </PropertiesTextEdit>
                                                        </dxwgv:GridViewDataTextColumn>
                                                        <dxwgv:GridViewDataTextColumn FieldName="ValorVencimento" VisibleIndex="12">
                                                            <PropertiesTextEdit DisplayFormatString="{0:#,##0.00;(#,##0.00);0.00}">
                                                            </PropertiesTextEdit>
                                                        </dxwgv:GridViewDataTextColumn>
                                                        <dxwgv:GridViewDataTextColumn FieldName="ValorNominal" VisibleIndex="13">
                                                            <PropertiesTextEdit DisplayFormatString="{0:#,##0.00;(#,##0.00);0.00}">
                                                            </PropertiesTextEdit>
                                                        </dxwgv:GridViewDataTextColumn>
                                                        <dxwgv:GridViewDataTextColumn FieldName="ValorNominalAjustado" VisibleIndex="14">
                                                            <PropertiesTextEdit DisplayFormatString="{0:#,##0.00;(#,##0.00);0.00}">
                                                            </PropertiesTextEdit>
                                                        </dxwgv:GridViewDataTextColumn>
                                                    </Columns>
                                                    <Settings ShowFilterRow="True" ShowStatusBar="Visible" ShowFooter="True" VerticalScrollBarMode="Visible"
                                                        VerticalScrollableHeight="340" VerticalScrollBarStyle="Virtual" />
                                                    <SettingsCustomizationWindow Enabled="True" />
                                                    <SettingsText EmptyDataRow="0 registros" />
                                                    <Styles>
                                                        <Footer Font-Bold="True" />
                                                    </Styles>
                                                    <ClientSideEvents Init="AdjustSize" EndCallback="AdjustSize" />
                                                    <SettingsCommandButton>
                                                        <ClearFilterButton Image-Url="../../imagens/funnel--minus.png">
                                                            <Image Url="../../imagens/funnel--minus.png">
                                                            </Image>
                                                        </ClearFilterButton>
                                                    </SettingsCommandButton>
                                                </dxwgv:ASPxGridView>
                                            </div>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </div>
                        </div>
                    </td>
                </tr>
            </table>
        </div>
        <dxwgv:ASPxGridViewExporter ID="gridExport" runat="server" GridViewID="gridConsulta"
            LeftMargin="50" RightMargin="50" />
        <cc1:esDataSource ID="EsDSMemCalculoRendaFixa" runat="server" OnesSelect="EsDSMemCalculoRendaFixa_esSelect" />
        <cc1:esDataSource ID="EsDSCliente" runat="server" OnesSelect="EsDSCliente_esSelect" />
        <cc1:esDataSource ID="EsDSTituloRendaFixaFiltro" runat="server" OnesSelect="EsDSTituloRendaFixaFiltro_esSelect" />
    </form>

    <script type="text/javascript">  
    // UpdateProgress Para Telas de Consultas com Impressao de Relatorio
    // UpdateProgress Condicional que só atua no elemento Button - btnRun

    var pageManager = Sys.WebForms.PageRequestManager.getInstance();
    pageManager.add_beginRequest(BeginRequestHandler);
    pageManager.add_endRequest(EndRequestHandler);

    function BeginRequestHandler(sender, args) {              

        var elem = args.get_postBackElement();
        // First set associatedUpdatePanelId to non existant control
        // this will force the updateprogress not to try and show itself.               
        var o = $find('<%= UpdateProgress1.ClientID %>');
        o.set_associatedUpdatePanelId('Non_Existant_Control_Id');

        // Then based on the control ID, show the updateprogress
        if (elem.id == '<%= btnRun.ClientID %>') {        
            var updateProgress1 = $get('<%= UpdateProgress1.ClientID %>');  

            sleep(900); // Espera um pouco quando tem a mensagem de campos InvÃ¡lidos

            updateProgress1.style.display = '';
        }
    }

    function EndRequestHandler(sender, args) {
        var updateProgress1 = $get('<%= UpdateProgress1.ClientID %>');        
        updateProgress1.style.display = (updateProgress1.style.display == '') ? 'none' : '';     
    }

    function sleep(milliseconds) {
      var start = new Date().getTime();
      for (var i = 0; i < 1e7; i++) {
        if ((new Date().getTime() - start) > milliseconds){
          break;
        }
      }
    }
    </script>

</body>
</html>
