﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using Financial.Security;
using Financial.ContaCorrente;
using System.Text;
using DevExpress.Web;
using Financial.Web.Util;
using System.Drawing;
using Financial.Investidor;
using Financial.Bolsa;
using System.Collections.Generic;
using EntitySpaces.Interfaces;
using Financial.Common;
using Financial.Security.Enums;
using Financial.Web.Common;

public partial class Consultas_PosicaoEmprestimoBolsa : ConsultaBasePage {
    new protected void Page_Load(object sender, EventArgs e) {
        this.HasPopupCliente = true;
        btnEditCodigoCliente.Focus();
        base.Page_Load(sender, e);

        TrataTravamentoCampos();
    }

    #region DataSources
    protected void EsDSPosicaoEmprestimoBolsa_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e) {
        if (!Page.IsPostBack) {
            gridConsulta.Visible = false;
            e.Collection = new PosicaoEmprestimoBolsaCollection();
        }
        else {

            // Confere Campos Obrigatorios para o caso de Mudança de Pagina e Scroolbar
            this.TrataCamposObrigatorios();
            //

            //Checa se busca da posição atual ou histórica
            int idCliente = Convert.ToInt32(btnEditCodigoCliente.Text);
            DateTime data = Convert.ToDateTime(textData.Text);

            Cliente cliente = new Cliente();
            List<esQueryItem> campos = new List<esQueryItem>();
            campos.Add(cliente.Query.DataDia);
            cliente.LoadByPrimaryKey(idCliente);

            DateTime dataDia = cliente.DataDia.Value;

            bool historico = false;
            if (DateTime.Compare(data, dataDia) < 0) {
                historico = true;
            }
            //

            if (historico) {
                PosicaoEmprestimoBolsaHistoricoCollection posicaoEmprestimoBolsaCollection = new PosicaoEmprestimoBolsaHistoricoCollection();

                if (btnEditCodigoCliente.Text != "")
                    posicaoEmprestimoBolsaCollection.Query.Where(posicaoEmprestimoBolsaCollection.Query.IdCliente.Equal(Convert.ToInt32(btnEditCodigoCliente.Text)));

                posicaoEmprestimoBolsaCollection.Query.Where(posicaoEmprestimoBolsaCollection.Query.DataHistorico.Equal(Convert.ToDateTime(textData.Text)));

                posicaoEmprestimoBolsaCollection.Query.OrderBy(posicaoEmprestimoBolsaCollection.Query.PontaEmprestimo.Ascending,
                                                               posicaoEmprestimoBolsaCollection.Query.CdAtivoBolsa.Ascending,
                                                               posicaoEmprestimoBolsaCollection.Query.DataVencimento.Ascending);
                posicaoEmprestimoBolsaCollection.Query.Load();
                e.Collection = posicaoEmprestimoBolsaCollection;
            }
            else {
                PosicaoEmprestimoBolsaCollection posicaoEmprestimoBolsaCollection = new PosicaoEmprestimoBolsaCollection();

                if (btnEditCodigoCliente.Text != "")
                    posicaoEmprestimoBolsaCollection.Query.Where(posicaoEmprestimoBolsaCollection.Query.IdCliente.Equal(Convert.ToInt32(btnEditCodigoCliente.Text)));

                posicaoEmprestimoBolsaCollection.Query.OrderBy(posicaoEmprestimoBolsaCollection.Query.PontaEmprestimo.Ascending,
                                                               posicaoEmprestimoBolsaCollection.Query.CdAtivoBolsa.Ascending,
                                                               posicaoEmprestimoBolsaCollection.Query.DataVencimento.Ascending);
                posicaoEmprestimoBolsaCollection.Query.Load();
                e.Collection = posicaoEmprestimoBolsaCollection;
            }

            gridConsulta.Visible = true;
        }

    }

    protected void EsDSCliente_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e) {
        ClienteCollection clienteCollection = new ClienteCollection();
        clienteCollection.BuscaClientesComAcesso(HttpContext.Current.User.Identity.Name);

        e.Collection = clienteCollection;
    }
    #endregion

    /// <summary>
    /// 
    /// </summary>
    private void TrataTravamentoCampos() {
        Usuario usuario = new Usuario();
        usuario.BuscaUsuario(HttpContext.Current.User.Identity.Name);
        int tipoTrava = usuario.TipoTrava.Value;

        if (tipoTrava == (int)TipoTravaUsuario.TravaCliente) {
            btnEditCodigoCliente.Enabled = false;

            PermissaoCliente permissaoCliente = new PermissaoCliente();
            int? idCliente = permissaoCliente.RetornaClienteAssociado(HttpContext.Current.User.Identity.Name);

            if (idCliente.HasValue) {
                Cliente cliente = new Cliente();

                List<esQueryItem> campos = new List<esQueryItem>();
                campos.Add(cliente.Query.Apelido);
                cliente.LoadByPrimaryKey(campos, idCliente.Value);
                string apelido = cliente.Apelido;

                btnEditCodigoCliente.Text = Convert.ToString(idCliente.Value);
                textNome.Text = apelido;
            }
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void ASPxCallback1_Callback(object source, DevExpress.Web.CallbackEventArgs e) {
        string nome = "";
        if (!String.IsNullOrEmpty(e.Parameter) && e.Parameter != "null") {
            int idCliente = Convert.ToInt32(e.Parameter);
            Cliente cliente = new Cliente();
            cliente.LoadByPrimaryKey(idCliente);

            if (cliente.str.Apelido != "") {
                if (cliente.IsAtivo) {
                    //O permissionamento da cliente é dado direto no cliente vinculado à cliente
                    PermissaoCliente permissaoCliente = new PermissaoCliente();
                    if (permissaoCliente.RetornaAcessoClienteComControle(idCliente, HttpContext.Current.User.Identity.Name)) {
                        nome = cliente.str.Apelido;
                    }
                    else {
                        nome = "no_access";
                    }
                }
                else {
                    nome = "no_active";
                }
            }
        }
        e.Result = nome;
    }

    #region btnRun_Click, btnPDF_Click, btnExcel_Click

    /// <summary>
    /// Trata os campos Obrigatorios do Form
    /// </summary>
    private void TrataCamposObrigatorios() {
        #region Campos obrigatórios
        List<Control> controles = new List<Control>(new Control[] { 
            this.btnEditCodigoCliente, this.textData });

        if (base.TestaObrigatorio(controles) != "") {
            throw new Exception("Campos com * são obrigatórios!");
        }
        #endregion
    }

    new protected void btnRun_Click(object sender, EventArgs e) {
        this.TrataCamposObrigatorios();
        base.btnRun_Click(sender, e); // DataBind do Grid
    }

    new protected void btnPDF_Click(object sender, EventArgs e) {
        this.TrataCamposObrigatorios();
        this.SelecionaRelatorio(VisaoRelatorio.visaoPDF);
    }

    new protected void btnExcel_Click(object sender, EventArgs e) {
        this.TrataCamposObrigatorios();
        this.SelecionaRelatorio(VisaoRelatorio.visaoExcel);
    }

    /// <summary>
    /// Seleciona o Relatorio a Ser Executado
    /// </summary>
    /// <param name="visao">PDF, Excel ou Report</param>
    private void SelecionaRelatorio(string visao) {
        Response.BufferOutput = true;
        //
        // Salva os Parametros na Session
        Session["gridExportPosicaoEmprestimoBolsa"] = gridExport;
        Response.Redirect("~/Consultas/PosicaoEmprestimoBolsaExporta.aspx?Visao=" + visao);
    }

    #endregion
}