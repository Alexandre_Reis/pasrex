﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="PainelCarteira.aspx.cs" Inherits="Consultas_PainelCarteira" %>

<%@ Register Assembly="System.Web, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a" Namespace="System.Web.UI.HtmlControls" TagPrefix="cc2" %>
<%@ Register TagPrefix="atk" Namespace="Atatika.Web.UI" Assembly="Atatika.Web.UI" %>

<%@ Register Assembly="DevExpress.Web.v15.2" Namespace="DevExpress.Web" TagPrefix="dxcp" %>
<%@ Register Assembly="DevExpress.Web.v15.2" Namespace="DevExpress.Web" TagPrefix="dxrp" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxe" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxwgv" %>

<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxpc" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxcb" %>
<%@ Register Assembly="DevExpress.Web.v15.2" Namespace="DevExpress.Web" TagPrefix="dxtc" %>
<%@ Register Assembly="DevExpress.Web.v15.2" Namespace="DevExpress.Web" tagprefix="dxw" %>    

<%@ Register Assembly="EntitySpaces.Web" Namespace="EntitySpaces.Web" TagPrefix="cc1" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title>Untitled Page</title>
    
    <link href="~/css/forms.css" rel="stylesheet" type="text/css" />
    
    <script language="JavaScript">
    var backgroundColor;
    function OnGetData(values) {
        btnEditCodigo.SetValue(values);                    
        popupCarteira.HideWindow();
        ASPxCallback1.SendCallback(btnEditCodigo.GetValue());        
        btnEditCodigo.Focus();    
    }
    function ClearPainelCarteira()
    {
        document.getElementById('labelValorCaixaAbertura').value = '';
        document.getElementById('labelValorCaixaFechamento').value = '';
        document.getElementById('labelValorVencimentosDia').value = '';
    }
    </script>

    <script type="text/javascript" language="Javascript" src="../js/global.js"></script>
    
</head>
<body>
    <form id="form1" runat="server">
        
        <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="true" EnableScriptLocalization="true"/>
        
        <asp:TextBox style="display:none" ID="textMsgNaoExiste" runat="server" Text="Carteira não existe"/>
        <asp:TextBox style="display:none" ID="textMsgUsuarioSemAcesso" runat="server" Text="Usuário sem acesso a esta carteira"/>
        <asp:TextBox style="display:none" ID="textMsgInativo" runat="server" Text="Carteira inativa"/>
        
        <dxcb:ASPxCallback ID="ASPxCallback1" runat="server" OnCallback="ASPxCallback1_Callback">
            <ClientSideEvents CallbackComplete="function(s, e) {                             
                OnCallBackCompleteCliente(s, e, popupMensagem, btnEditCodigo, document.getElementById('textNome'), textData);
            }        
            "/>
        </dxcb:ASPxCallback>
        
        <dxpc:ASPxPopupControl ID="popupMensagem" ShowHeader="false" PopupElementID="btnEditCodigo" CloseAction="OuterMouseClick" 
            ForeColor="Red" PopupVerticalAlign="Middle" PopupHorizontalAlign="OutsideRight" Left="100"
            PopupAction="None" HeaderText="" runat="server">
        </dxpc:ASPxPopupControl>
        
        <dxpc:ASPxPopupControl ID="popupCarteira" runat="server" HeaderText="" Width="500px" 
                        ContentStyle-VerticalAlign="Top"  
                        PopupVerticalAlign="Middle" PopupHorizontalAlign="OutsideRight" AllowDragging="True">
        
        <ContentCollection><dxpc:PopupControlContentControl runat="server">                     
        <div>
            <dxwgv:ASPxGridView ID="gridCarteira" runat="server" Width="100%" EnableCallBacks="true"
                    ClientInstanceName="gridCarteira" AutoGenerateColumns="False" 
                    DataSourceID="EsDSCarteira" KeyFieldName="IdCarteira"
                    OnCustomDataCallback="gridCarteira_CustomDataCallback" 
                    OnHtmlRowCreated="gridCarteira_HtmlRowCreated">                    
            <Columns>
                <dxwgv:GridViewDataTextColumn FieldName="IdCarteira" ReadOnly="True" VisibleIndex="0" Width="20%"/>
                <dxwgv:GridViewDataTextColumn FieldName="Apelido" VisibleIndex="1" Width="80%"/>                    
            </Columns>            
            <Settings ShowFilterRow="True" ShowTitlePanel="True"/>            
            <SettingsBehavior ColumnResizeMode="Disabled" />
            <ClientSideEvents RowDblClick="function(s, e) {
            gridCarteira.GetValuesOnCustomCallback(e.visibleIndex, OnGetData);}" Init="function(s, e) {
	        e.cancel = true;
	        }"
	        />
            <SettingsDetail ShowDetailButtons="False" />
            <Styles Cell-Wrap="False" AlternatingRow-CssClass="MyClass">
                <Header ImageSpacing="5px" SortingImageSpacing="5px">
                </Header>                    
                <Cell Wrap="False">
                </Cell>
            </Styles>
            <Images></Images>
            <SettingsText EmptyDataRow="0 Registros" Title="Pesquisa Carteira" />
            </dxwgv:ASPxGridView>    
        
        </div>      
        </dxpc:PopupControlContentControl></ContentCollection>
        <ClientSideEvents CloseUp="function(s, e) {gridCarteira.ClearFilter(); }" />
        </dxpc:ASPxPopupControl>
                        
        <div class="divPanel">
        
        <table width="100%"><tr><td>
        <div id="container">        
        <div id="header">Painel de Carteira</div>  
              
        <div style="position:absolute; right:5%">       
        <asp:UpdateProgress ID="UpdateProgress1" runat="server" AssociatedUpdatePanelID="UpdatePanel1" DisplayAfter="500">
        <ProgressTemplate>                    
           <asp:Image runat="server" ID="roller" ImageUrl="~/imagens/roller.gif" />
           <asp:Label ID="Label1" runat="server" Text="Carregando..."></asp:Label>                        
        </ProgressTemplate>                    
        </asp:UpdateProgress>
        </div>
           
        <div id="mainContentSpace">
            
            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
            <ContentTemplate>
            
            <div class="reportFilter">
                
                <dxpc:ASPxPopupControl ID="popupLiquidacao" runat="server" HeaderText="" Width="700px" 
                        ContentStyle-VerticalAlign="Top" CloseAction="CloseButton"  
                        PopupVerticalAlign="Middle" PopupHorizontalAlign="WindowCenter" AllowDragging="True">
                <ContentStyle Paddings-PaddingLeft="3px" Paddings-PaddingRight="3px" Paddings-PaddingBottom="3px"></ContentStyle>        
                <ContentCollection><dxpc:PopupControlContentControl runat="server">
                <div>
                    <dxwgv:ASPxGridView ID="gridLiquidacao" runat="server" Width="100%" EnableCallBacks="true"
                            ClientInstanceName="gridLiquidacao"  AutoGenerateColumns="False" 
                            DataSourceID="EsDSLiquidacao" KeyFieldName="IdPosicao"                                                
                            OnHtmlDataCellPrepared="gridLiquidacao_HtmlDataCellPrepared">                    
                    <Columns>
                        <dxwgv:GridViewDataDateColumn FieldName="DataLancamento" Caption="Lançamento" VisibleIndex="0" Width="10%"/>                        
                        <dxwgv:GridViewDataDateColumn FieldName="DataVencimento" Caption="Vencimento" VisibleIndex="1" Width="10%"/>                        
                        <dxwgv:GridViewDataColumn FieldName="Descricao" Caption="Descrição" VisibleIndex="2" Width="55%"/>                    
                        
                        <dxwgv:GridViewDataTextColumn FieldName="Valor" VisibleIndex="3" Width="25%" 
                                        HeaderStyle-HorizontalAlign="Right" CellStyle-HorizontalAlign="Right"
                                        FooterCellStyle-HorizontalAlign="Right">                        
                        <PropertiesTextEdit DisplayFormatString="{0:#,##0.00;(#,##0.00);0.00}"></PropertiesTextEdit>                            
                            <CellStyle HorizontalAlign="Right"></CellStyle>
                            <FooterCellStyle HorizontalAlign="Right"></FooterCellStyle>
                        </dxwgv:GridViewDataTextColumn>
                    </Columns>
                    <TotalSummary>
                        <dxwgv:ASPxSummaryItem DisplayFormat="{0:#,##0.00;(#,##0.00);0.00}"
                            FieldName="Valor" ShowInColumn="Valor" SummaryType="Sum" />
                    </TotalSummary>            
                    <Settings ShowFilterRow="True" ShowTitlePanel="True" ShowFooter="true" VerticalScrollBarMode="Visible" VerticalScrollableHeight="250"/>            
                    <SettingsPager PageSize="30"></SettingsPager>
                    <SettingsBehavior ColumnResizeMode="Disabled" />            
                    <Styles Cell-Wrap="False" AlternatingRow-CssClass="MyClass">
                        <Header ImageSpacing="5px" SortingImageSpacing="5px" />
                        <Cell Wrap="False" />
                    </Styles>
                    <Images></Images>
                    
                    <SettingsText EmptyDataRow="0 Registros" Title="Valores a Liquidar" />
                    </dxwgv:ASPxGridView>        
                </div>      
                </dxpc:PopupControlContentControl></ContentCollection>                        
                </dxpc:ASPxPopupControl>
                
                <dxpc:ASPxPopupControl ID="popupPosicaoBolsa" runat="server" HeaderText="" Width="670px" 
                        ContentStyle-VerticalAlign="Top" CloseAction="CloseButton"
                        PopupVerticalAlign="Middle" PopupHorizontalAlign="WindowCenter" AllowDragging="True">
                <ContentStyle Paddings-PaddingLeft="3px" Paddings-PaddingRight="3px" Paddings-PaddingBottom="3px"></ContentStyle>
                <ContentCollection><dxpc:PopupControlContentControl runat="server">
                <div>
                    <dxwgv:ASPxGridView ID="gridPosicaoBolsa" runat="server" Width="100%" EnableCallBacks="true"
                            ClientInstanceName="gridPosicaoBolsa"  AutoGenerateColumns="False" 
                            DataSourceID="EsDSPosicaoBolsa" KeyFieldName="IdPosicao"                                                
                            OnHtmlDataCellPrepared="gridPosicaoBolsa_HtmlDataCellPrepared"
                            OnHtmlFooterCellPrepared="gridPosicaoBolsa_HtmlFooterCellPrepared">
                    <Columns>
                        <dxwgv:GridViewDataTextColumn FieldName="CdAtivoBolsa" Caption="Código" VisibleIndex="0" Width="18%" />
                        
                        <dxwgv:GridViewDataTextColumn FieldName="Quantidade" VisibleIndex="1" Width="19%" 
                                                      HeaderStyle-HorizontalAlign="Right" CellStyle-HorizontalAlign="Right">                    
                        <PropertiesTextEdit DisplayFormatString="{0:#,##0;(#,##0);0}"></PropertiesTextEdit>
                            <CellStyle HorizontalAlign="Right">
                            </CellStyle>
                        </dxwgv:GridViewDataTextColumn>
                        
                        <dxwgv:GridViewDataTextColumn FieldName="PUCustoLiquido" Caption="PU Custo" VisibleIndex="2" Width="15%" 
                                                      HeaderStyle-HorizontalAlign="Right" CellStyle-HorizontalAlign="Right">                    
                        <PropertiesTextEdit DisplayFormatString="{0:#,##0.0000;(#,##0.0000);0.0000}"></PropertiesTextEdit>                        
                            <CellStyle HorizontalAlign="Right">
                            </CellStyle>
                        </dxwgv:GridViewDataTextColumn>
                        
                        <dxwgv:GridViewDataTextColumn FieldName="PUMercado" VisibleIndex="3" Width="15%" 
                                                      HeaderStyle-HorizontalAlign="Right" CellStyle-HorizontalAlign="Right">                    
                        <PropertiesTextEdit DisplayFormatString="{0:#,##0.00;(#,##0.00);0.00}"></PropertiesTextEdit>
                            <CellStyle HorizontalAlign="Right">
                            </CellStyle>
                        </dxwgv:GridViewDataTextColumn>
                        
                        <dxwgv:GridViewDataTextColumn FieldName="ValorMercado" VisibleIndex="4" Width="15%" 
                                          FooterCellStyle-HorizontalAlign="Right" HeaderStyle-HorizontalAlign="Right" CellStyle-HorizontalAlign="Right">                    
                        <PropertiesTextEdit DisplayFormatString="{0:#,##0.00;(#,##0.00);0.00}"></PropertiesTextEdit>
                            <CellStyle HorizontalAlign="Right">
                            </CellStyle>
                            <FooterCellStyle HorizontalAlign="Right">
                            </FooterCellStyle>
                        </dxwgv:GridViewDataTextColumn>
                        
                        <dxwgv:GridViewDataTextColumn FieldName="ResultadoRealizar" VisibleIndex="5" Width="18%" 
                                          FooterCellStyle-HorizontalAlign="Right" HeaderStyle-HorizontalAlign="Right" CellStyle-HorizontalAlign="Right">                    
                        <PropertiesTextEdit DisplayFormatString="{0:#,##0.00;(#,##0.00);0.00}"></PropertiesTextEdit>                            
                            <CellStyle HorizontalAlign="Right">
                            </CellStyle>
                            <FooterCellStyle HorizontalAlign="Right">
                            </FooterCellStyle>
                        </dxwgv:GridViewDataTextColumn>
                    </Columns>
                    
                    <TotalSummary>
                        <dxwgv:ASPxSummaryItem DisplayFormat="{0:#,##0.00;(#,##0.00);0.00}"
                            FieldName="ResultadoRealizar" ShowInColumn="Resultado Realizar" SummaryType="Sum" />
                        <dxwgv:ASPxSummaryItem DisplayFormat="{0:#,##0.00;(#,##0.00);0.00}"
                            FieldName="ValorMercado" ShowInColumn="Valor Mercado" SummaryType="Sum" />
                    </TotalSummary>
                    
                    <Settings ShowFilterRow="True" ShowTitlePanel="True" ShowFooter="true" VerticalScrollBarMode="Visible" VerticalScrollableHeight="250"/>            
                    <SettingsPager PageSize="30"></SettingsPager>
                    <SettingsBehavior ColumnResizeMode="Disabled" />            
                    <Styles Cell-Wrap="False" AlternatingRow-CssClass="MyClass" Footer-HorizontalAlign="Right">
                        <Header ImageSpacing="5px" SortingImageSpacing="5px" />
                        <Cell Wrap="False" />
                    </Styles>
                    <Images></Images>
                    <SettingsText EmptyDataRow="0 Registros" Title="Posição em Bolsa" />
                    
                    </dxwgv:ASPxGridView>        
                </div>      
                </dxpc:PopupControlContentControl></ContentCollection>                        
                </dxpc:ASPxPopupControl>
                
                <dxpc:ASPxPopupControl ID="popupPosicaoBMF" runat="server" HeaderText="" Width="700px" 
                        ContentStyle-VerticalAlign="Top" CloseAction="CloseButton"  
                        PopupVerticalAlign="Middle" PopupHorizontalAlign="WindowCenter" AllowDragging="True">
                <ContentStyle Paddings-PaddingLeft="3px" Paddings-PaddingRight="3px" Paddings-PaddingBottom="3px"></ContentStyle>                        
                <ContentCollection><dxpc:PopupControlContentControl runat="server">
                <div>
                    <dxwgv:ASPxGridView ID="gridPosicaoBMF" runat="server" Width="100%" EnableCallBacks="true"
                            ClientInstanceName="gridPosicaoBMF"  AutoGenerateColumns="False" 
                            DataSourceID="EsDSPosicaoBMF" KeyFieldName="IdPosicao"                    
                            OnHtmlDataCellPrepared="gridPosicaoBMF_HtmlDataCellPrepared"
                            OnHtmlFooterCellPrepared="gridPosicaoBMF_HtmlFooterCellPrepared">                    
                    <Columns>
                        <dxwgv:GridViewDataTextColumn FieldName="CdAtivoBMF" Caption="Código" VisibleIndex="0" Width="10%"/>                        
                        <dxwgv:GridViewDataTextColumn FieldName="Serie" Caption="Série" VisibleIndex="1" Width="10%"/>
                        
                        <dxwgv:GridViewDataTextColumn FieldName="Quantidade" VisibleIndex="2" Width="20%" 
                                                      HeaderStyle-HorizontalAlign="Right" CellStyle-HorizontalAlign="Right">                    
                        <PropertiesTextEdit DisplayFormatString="{0:#,##0;(#,##0);0}"></PropertiesTextEdit>
                            <CellStyle HorizontalAlign="Right">
                            </CellStyle>
                        </dxwgv:GridViewDataTextColumn>
                        
                        <dxwgv:GridViewDataTextColumn FieldName="PUCustoLiquido" Caption="PU Custo" VisibleIndex="3" Width="15%" 
                                                      HeaderStyle-HorizontalAlign="Right" CellStyle-HorizontalAlign="Right">                    
                        <PropertiesTextEdit DisplayFormatString="{0:#,##0.0000;(#,##0.0000);0.0000}"></PropertiesTextEdit>
                            <CellStyle HorizontalAlign="Right">
                            </CellStyle>
                        </dxwgv:GridViewDataTextColumn>
                        
                        <dxwgv:GridViewDataTextColumn FieldName="PUMercado" VisibleIndex="4" Width="15%" 
                                                      HeaderStyle-HorizontalAlign="Right" CellStyle-HorizontalAlign="Right">                    
                        <PropertiesTextEdit DisplayFormatString="{0:#,##0.00;(#,##0.00);0.00}"></PropertiesTextEdit>
                            <CellStyle HorizontalAlign="Right">
                            </CellStyle>
                        </dxwgv:GridViewDataTextColumn>
                        
                        <dxwgv:GridViewDataTextColumn FieldName="ValorMercado" VisibleIndex="5" Width="15%" 
                                             FooterCellStyle-HorizontalAlign="Right" HeaderStyle-HorizontalAlign="Right" CellStyle-HorizontalAlign="Right">                    
                        <PropertiesTextEdit DisplayFormatString="{0:#,##0.00;(#,##0.00);0.00}"></PropertiesTextEdit>
                            <CellStyle HorizontalAlign="Right">
                            </CellStyle>
                            <FooterCellStyle HorizontalAlign="Right">
                            </FooterCellStyle>
                        </dxwgv:GridViewDataTextColumn>
                        
                        <dxwgv:GridViewDataTextColumn FieldName="ResultadoRealizar" VisibleIndex="6" Width="15%" 
                                             FooterCellStyle-HorizontalAlign="Right" HeaderStyle-HorizontalAlign="Right" CellStyle-HorizontalAlign="Right">                    
                        <PropertiesTextEdit DisplayFormatString="{0:#,##0.00;(#,##0.00);0.00}"></PropertiesTextEdit>
                            <CellStyle HorizontalAlign="Right">
                            </CellStyle>
                            <FooterCellStyle HorizontalAlign="Right">
                            </FooterCellStyle>
                        </dxwgv:GridViewDataTextColumn>
                    </Columns>
                    
                    <TotalSummary>
                        <dxwgv:ASPxSummaryItem DisplayFormat="{0:#,##0.00;(#,##0.00);0.00}"
                            FieldName="ResultadoRealizar" ShowInColumn="Resultado Realizar" SummaryType="Sum" />
                        <dxwgv:ASPxSummaryItem DisplayFormat="{0:#,##0.00;(#,##0.00);0.00}"
                            FieldName="ValorMercado" ShowInColumn="Valor Mercado" SummaryType="Sum" />
                    </TotalSummary>
                    
                    <Settings ShowFilterRow="True" ShowTitlePanel="True" ShowFooter="true" VerticalScrollBarMode="Visible" VerticalScrollableHeight="250"/>            
                    <SettingsPager PageSize="30"></SettingsPager>
                    <SettingsBehavior ColumnResizeMode="Disabled" />            
                    <Styles Cell-Wrap="False" AlternatingRow-CssClass="MyClass" Footer-HorizontalAlign="Right">
                        <Header ImageSpacing="5px" SortingImageSpacing="5px"/>
                        <Cell Wrap="False"/>
                    </Styles>
                    <Images></Images>
                    <SettingsText EmptyDataRow="0 Registros" Title="Posição em BMF" />
                    
                    </dxwgv:ASPxGridView>        
                </div>      
                </dxpc:PopupControlContentControl></ContentCollection>                        
                </dxpc:ASPxPopupControl>
                
                <dxpc:ASPxPopupControl ID="popupPosicaoFundo" runat="server" HeaderText="" Width="700px" 
                        ContentStyle-VerticalAlign="Top" CloseAction="CloseButton"  
                        PopupVerticalAlign="Middle" PopupHorizontalAlign="WindowCenter" AllowDragging="True">
                <ContentStyle Paddings-PaddingLeft="3px" Paddings-PaddingRight="3px" Paddings-PaddingBottom="3px"></ContentStyle>                        
                <ContentCollection><dxpc:PopupControlContentControl runat="server">
                <div>
                    <dxwgv:ASPxGridView ID="gridPosicaoFundo" runat="server" Width="100%" EnableCallBacks="true"
                            ClientInstanceName="gridPosicaoBolsa"  AutoGenerateColumns="False" 
                            DataSourceID="EsDSPosicaoFundo" KeyFieldName="IdPosicao"
                            OnHtmlDataCellPrepared="gridPosicaoFundo_HtmlDataCellPrepared"
                            OnHtmlFooterCellPrepared="gridPosicaoFundo_HtmlFooterCellPrepared">                    
                    <Columns>
                        <dxwgv:GridViewDataTextColumn FieldName="CarteiraApelido" Caption="Nome" VisibleIndex="0" Width="30%"/>
                        
                        <dxwgv:GridViewDataTextColumn FieldName="Quantidade" VisibleIndex="1" Width="20%" 
                                                      HeaderStyle-HorizontalAlign="Right" CellStyle-HorizontalAlign="Right">                    
                        <PropertiesTextEdit DisplayFormatString="{0:#,##0.00000000;(#,##0.00000000);0.00000000}"></PropertiesTextEdit>                            
                            <CellStyle HorizontalAlign="Right">
                            </CellStyle>
                        </dxwgv:GridViewDataTextColumn>
                        
                        <dxwgv:GridViewDataTextColumn FieldName="ValorAplicacao" VisibleIndex="2" Width="15%" 
                                                      HeaderStyle-HorizontalAlign="Right" CellStyle-HorizontalAlign="Right">                    
                        <PropertiesTextEdit DisplayFormatString="{0:#,##0.00;(#,##0.00);0.00}"></PropertiesTextEdit>
                            <CellStyle HorizontalAlign="Right">
                            </CellStyle>
                        </dxwgv:GridViewDataTextColumn>                        
                        
                        <dxwgv:GridViewDataTextColumn FieldName="CotaDia" VisibleIndex="3" Width="20%" 
                                                      HeaderStyle-HorizontalAlign="Right" CellStyle-HorizontalAlign="Right">                    
                        <PropertiesTextEdit DisplayFormatString="{0:#,##0.00000000;(#,##0.00000000);0.00000000}"></PropertiesTextEdit>
                            <CellStyle HorizontalAlign="Right">
                            </CellStyle>
                        </dxwgv:GridViewDataTextColumn>
                        
                        <dxwgv:GridViewDataTextColumn FieldName="ValorBruto" VisibleIndex="4" Width="15%" 
                                                      HeaderStyle-HorizontalAlign="Right" CellStyle-HorizontalAlign="Right"
                                                      FooterCellStyle-HorizontalAlign="Right">                    
                        <PropertiesTextEdit DisplayFormatString="{0:#,##0.00;(#,##0.00);0.00}"></PropertiesTextEdit>
                            <CellStyle HorizontalAlign="Right">
                            </CellStyle>
                            <FooterCellStyle HorizontalAlign="Right">
                            </FooterCellStyle>
                        </dxwgv:GridViewDataTextColumn>                        
                    </Columns>
                    
                    <TotalSummary>
                        <dxwgv:ASPxSummaryItem DisplayFormat="{0:#,##0.00;(#,##0.00);0.00}"
                            FieldName="ValorBruto" ShowInColumn="Valor Bruto" SummaryType="Sum" />
                    </TotalSummary>   
                             
                    <Settings ShowFilterRow="True" ShowTitlePanel="True" ShowFooter="true" VerticalScrollBarMode="Visible" VerticalScrollableHeight="250"/>            
                    <SettingsPager PageSize="30"></SettingsPager>
                    <SettingsBehavior ColumnResizeMode="Disabled" />            
                    <Styles Cell-Wrap="False" AlternatingRow-CssClass="MyClass">
                        <Header ImageSpacing="5px" SortingImageSpacing="5px"/>
                        <Cell Wrap="False"/>
                    </Styles>
                    <Images></Images>
                    
                    <SettingsText EmptyDataRow="0 Registros" Title="Posição em Cotas de Investimentos" />
                    </dxwgv:ASPxGridView>        
                </div>      
                </dxpc:PopupControlContentControl></ContentCollection>                        
                </dxpc:ASPxPopupControl>
                
                <dxpc:ASPxPopupControl ID="popupPosicaoRendaFixa" runat="server" HeaderText="" Width="700px" 
                        ContentStyle-VerticalAlign="Top" CloseAction="CloseButton"  
                        PopupVerticalAlign="Middle" PopupHorizontalAlign="WindowCenter" AllowDragging="True">
                <ContentStyle Paddings-PaddingLeft="3px" Paddings-PaddingRight="3px" Paddings-PaddingBottom="3px"></ContentStyle>                        
                <ContentCollection><dxpc:PopupControlContentControl runat="server">
                <div>
                    <dxwgv:ASPxGridView ID="gridPosicaoRendaFixa" runat="server" Width="100%" EnableCallBacks="true"
                            ClientInstanceName="gridPosicaoRendaFixa"  AutoGenerateColumns="False" 
                            DataSourceID="EsDSPosicaoRendaFixa" KeyFieldName="IdPosicao"
                            OnHtmlDataCellPrepared="gridPosicaoRendaFixa_HtmlDataCellPrepared"
                            OnHtmlFooterCellPrepared="gridPosicaoRendaFixa_HtmlFooterCellPrepared">                    
                    <Columns>
                        <dxwgv:GridViewDataTextColumn FieldName="Descricao" Caption="Título" VisibleIndex="0" Width="20%"/>                        
                        <dxwgv:GridViewDataDateColumn FieldName="DataVencimento" Caption="Vencimento" VisibleIndex="1" Width="10%"/>
                        
                        <dxwgv:GridViewDataTextColumn FieldName="Quantidade" VisibleIndex="2" Width="15%" 
                                                      HeaderStyle-HorizontalAlign="Right" CellStyle-HorizontalAlign="Right">                    
                        <PropertiesTextEdit DisplayFormatString="{0:#,##0.00000000;(#,##0.00000000);0.00000000}"></PropertiesTextEdit>                            
                            <CellStyle HorizontalAlign="Right"></CellStyle>
                        </dxwgv:GridViewDataTextColumn>
                        
                        <dxwgv:GridViewDataTextColumn FieldName="PUOperacao" Caption="PU Operação" VisibleIndex="3" Width="20%" 
                                                      HeaderStyle-HorizontalAlign="Right" CellStyle-HorizontalAlign="Right">                    
                        <PropertiesTextEdit DisplayFormatString="{0:#,##0.00000000;(#,##0.00000000);0.00000000}"></PropertiesTextEdit>
                            <CellStyle HorizontalAlign="Right"></CellStyle>
                        </dxwgv:GridViewDataTextColumn>
                        
                        <dxwgv:GridViewDataTextColumn FieldName="PUMercado" Caption="PU Mercado" VisibleIndex="4" Width="20%" 
                                                      HeaderStyle-HorizontalAlign="Right" CellStyle-HorizontalAlign="Right">                    
                        <PropertiesTextEdit DisplayFormatString="{0:#,##0.00000000;(#,##0.00000000);0.00000000}"></PropertiesTextEdit>
                            <CellStyle HorizontalAlign="Right"></CellStyle>
                        </dxwgv:GridViewDataTextColumn>
                                                
                        <dxwgv:GridViewDataTextColumn FieldName="ValorMercado" VisibleIndex="5" Width="15%" 
                                            FooterCellStyle-HorizontalAlign="Right" HeaderStyle-HorizontalAlign="Right" CellStyle-HorizontalAlign="Right">                    
                        <PropertiesTextEdit DisplayFormatString="{0:#,##0.00;(#,##0.00);0.00}"></PropertiesTextEdit>
                            <CellStyle HorizontalAlign="Right"></CellStyle>
                            <FooterCellStyle HorizontalAlign="Right"></FooterCellStyle>
                        </dxwgv:GridViewDataTextColumn>                        
                    </Columns>
                    
                    <TotalSummary>
                        <dxwgv:ASPxSummaryItem DisplayFormat="{0:#,##0.00;(#,##0.00);0.00}" FieldName="ValorMercado" ShowInColumn="Valor Mercado" SummaryType="Sum" />
                    </TotalSummary>            
                    
                    <Settings ShowFilterRow="True" ShowTitlePanel="True" ShowFooter="true" VerticalScrollBarMode="Visible" VerticalScrollableHeight="250"/>            
                    <SettingsPager PageSize="30"></SettingsPager>
                    <SettingsBehavior ColumnResizeMode="Disabled" />            
                    <Styles Cell-Wrap="False" AlternatingRow-CssClass="MyClass">
                        <Header ImageSpacing="5px" SortingImageSpacing="5px"/>
                        <Cell Wrap="False"/>
                    </Styles>
                    <Images></Images>
                    
                    <SettingsText EmptyDataRow="0 Registros" Title="Posição em Renda Fixa" />
                    </dxwgv:ASPxGridView>        
                </div>      
                </dxpc:PopupControlContentControl></ContentCollection>                        
                </dxpc:ASPxPopupControl>
                
                <dxpc:ASPxPopupControl ID="popupPosicaoSwap" runat="server" HeaderText="" Width="800px" 
                        ContentStyle-VerticalAlign="Top" CloseAction="CloseButton"    
                        PopupVerticalAlign="Middle" PopupHorizontalAlign="WindowCenter" AllowDragging="True">
                <ContentStyle Paddings-PaddingLeft="3px" Paddings-PaddingRight="3px" Paddings-PaddingBottom="3px"></ContentStyle>                        
                <ContentCollection><dxpc:PopupControlContentControl ID="PopupControlContentControl1" runat="server">
                <div>
                    <dxwgv:ASPxGridView ID="gridPosicaoSwap" runat="server" Width="100%" EnableCallBacks="true"
                            ClientInstanceName="gridPosicaoSwap"  AutoGenerateColumns="False" 
                            DataSourceID="EsDSPosicaoSwap" KeyFieldName="IdPosicao"                    
                            OnHtmlDataCellPrepared="gridPosicaoSwap_HtmlDataCellPrepared"
                            OnHtmlRowPrepared="gridPosicaoSwap_HtmlRowPrepared"
                            OnHtmlFooterCellPrepared="gridPosicaoSwap_HtmlFooterCellPrepared">                    
                    <Columns>
                        
                        <dxwgv:GridViewDataDateColumn FieldName="DataEmissao" Caption="Emissão" VisibleIndex="0" Width="10%"/>                        
                        <dxwgv:GridViewDataDateColumn FieldName="DataVencimento" Caption="Vencimento" VisibleIndex="1" Width="10%"/>                        
                        <dxwgv:GridViewDataTextColumn FieldName="DescricaoIndice" Caption="Índice" VisibleIndex="2" Width="10%"/>
                        
                        <dxwgv:GridViewDataTextColumn FieldName="TaxaJuros" Caption="Taxa" VisibleIndex="4" Width="5%" 
                                                      HeaderStyle-HorizontalAlign="Right" CellStyle-HorizontalAlign="Right">                    
                        <PropertiesTextEdit DisplayFormatString="{0:#,##0.00;(#,##0.00);0.00}"></PropertiesTextEdit>                            
                            <CellStyle HorizontalAlign="Right">
                            </CellStyle>
                        </dxwgv:GridViewDataTextColumn>
                        
                        <dxwgv:GridViewDataTextColumn FieldName="ValorParte" Caption="Valor Ativo" VisibleIndex="4" Width="18%" 
                                              FooterCellStyle-HorizontalAlign="Right" HeaderStyle-HorizontalAlign="Right" CellStyle-HorizontalAlign="Right">                    
                        <PropertiesTextEdit DisplayFormatString="{0:#,##0.00;(#,##0.00);0.00}"></PropertiesTextEdit>                            
                            <CellStyle HorizontalAlign="Right">
                            </CellStyle>
                            <FooterCellStyle HorizontalAlign="Right">
                            </FooterCellStyle>
                        </dxwgv:GridViewDataTextColumn>
                        
                        <dxwgv:GridViewDataTextColumn FieldName="DescricaoIndiceContraParte" Caption="Índice" VisibleIndex="5" Width="10%"/>
                        
                        <dxwgv:GridViewDataTextColumn FieldName="TaxaJurosContraParte" Caption="Taxa" VisibleIndex="6" Width="5%" 
                                                      HeaderStyle-HorizontalAlign="Right" CellStyle-HorizontalAlign="Right">                    
                        <PropertiesTextEdit DisplayFormatString="{0:#,##0.00;(#,##0.00);0.00}"></PropertiesTextEdit>                            
                            <CellStyle HorizontalAlign="Right">
                            </CellStyle>
                        </dxwgv:GridViewDataTextColumn>
                        
                        <dxwgv:GridViewDataTextColumn FieldName="ValorContraParte" Caption="Valor Passivo" VisibleIndex="7" Width="18%" 
                                               FooterCellStyle-HorizontalAlign="Right" HeaderStyle-HorizontalAlign="Right" CellStyle-HorizontalAlign="Right">                    
                        <PropertiesTextEdit DisplayFormatString="{0:#,##0.00;(#,##0.00);0.00}"></PropertiesTextEdit>                            
                            <CellStyle HorizontalAlign="Right">
                            </CellStyle>
                            <FooterCellStyle HorizontalAlign="Right">
                            </FooterCellStyle>
                        </dxwgv:GridViewDataTextColumn>
                        
                        <dxwgv:GridViewDataTextColumn FieldName="Saldo" VisibleIndex="8" Width="15%" 
                                               FooterCellStyle-HorizontalAlign="Right" HeaderStyle-HorizontalAlign="Right" CellStyle-HorizontalAlign="Right">                    
                        <PropertiesTextEdit DisplayFormatString="{0:#,##0.00;(#,##0.00);0.00}"></PropertiesTextEdit>                            
                            <CellStyle HorizontalAlign="Right">
                            </CellStyle>
                            <FooterCellStyle HorizontalAlign="Right">
                            </FooterCellStyle>
                        </dxwgv:GridViewDataTextColumn>
                        
                    </Columns>
                    
                    <TotalSummary>
                        <dxwgv:ASPxSummaryItem DisplayFormat="{0:#,##0.00;(#,##0.00);0.00}"
                            FieldName="ValorParte" ShowInColumn="ValorParte" SummaryType="Sum" />
                        <dxwgv:ASPxSummaryItem DisplayFormat="{0:#,##0.00;(#,##0.00);0.00}"
                            FieldName="ValorContraParte" ShowInColumn="ValorContraParte" SummaryType="Sum" />
                        <dxwgv:ASPxSummaryItem DisplayFormat="{0:#,##0.00;(#,##0.00);0.00}"
                            FieldName="Saldo" ShowInColumn="Saldo" SummaryType="Sum" />    
                    </TotalSummary>            
                    
                    <Settings ShowFilterRow="True" ShowTitlePanel="True" ShowFooter="true" VerticalScrollBarMode="Visible" VerticalScrollableHeight="250"/>            
                    <SettingsPager PageSize="30"></SettingsPager>
                    <SettingsBehavior ColumnResizeMode="Disabled" />            
                    <Styles Cell-Wrap="False" AlternatingRow-CssClass="MyClass">
                        <Header ImageSpacing="5px" SortingImageSpacing="5px"/>
                        <Cell Wrap="False"/>
                    </Styles>
                    <Images></Images>
                    
                    <SettingsText EmptyDataRow="0 Registros" Title="Posição em Swap" />
                    </dxwgv:ASPxGridView>        
                </div>      
                </dxpc:PopupControlContentControl></ContentCollection>                        
                </dxpc:ASPxPopupControl>            
                
                <atk:AKRequiredValidator ID="RequiredValidator1" runat="server" ValidationGroup="ATK" />
                
                <div class="dataMessage">
                <asp:ValidationSummary ID="validationSummary" runat="server" 
                                 HeaderText="Campos com * são obrigatórios." EnableClientScript="true"
                                 DisplayMode="SingleParagraph" ShowSummary="true" ValidationGroup="ATK"/>
                </div>                                      
                
                <table cellpadding="3">
                    <tr>
                    <td >
                        <asp:Label ID="labelCarteira" runat="server" CssClass="labelRequired" Text="Carteira:"></asp:Label>
                    </td>
                    
                    <td>
                        <dxe:ASPxSpinEdit ID="btnEditCodigo" runat="server" CssClass="textButtonEdit" 
                                            CssFilePath="../css/forms.css" EnableClientSideAPI="True"
                                            ClientInstanceName="btnEditCodigo" SpinButtons-ShowIncrementButtons="false"
                                            MaxLength="9" NumberType="Integer" AllowMouseWheel="false">  
                        <Buttons>
                            <dxe:EditButton>
                            </dxe:EditButton>                                
                        </Buttons>        
                        <ClientSideEvents
                                 KeyPress="function(s, e) {document.getElementById('labelCaixaAbertura').value = ''; document.getElementById('textNome').value = '';}" 
                                 ButtonClick="function(s, e) {popupCarteira.ShowAtElementByID(s.name);}" 
                                 LostFocus="function(s, e) {OnLostFocus(popupMensagem, ASPxCallback1, btnEditCodigo);}"
                        />                            
                            <SpinButtons ShowIncrementButtons="False">
                            </SpinButtons>
                        </dxe:ASPxSpinEdit>                
                    </td>                          
                    
                    <td>
                        <asp:TextBox ID="textNome" runat="server" CssClass="textNome" Enabled="false"></asp:TextBox>                        
                    </td>
                    
                    <td>
                        <asp:Label ID="labelStatus" runat="server"  style="color:#284F8E;font-weight:bold;"></asp:Label>
                    </td>
                    </tr>
                                                    
                    <tr>
                    <td >
                        <asp:Label ID="labelData" CssClass="labelRequired"  runat="server"  Text="Data Ref.:"></asp:Label>
                    </td>                        
                    <td>
                        <dxe:ASPxDateEdit ID="textData" runat="server" ClientInstanceName="textData" CssClass="textData" AllowNull ="false" />
                    </td>                      
                    
                    <td>
                    <div class="linkButton linkButtonNoBorder">
                           <asp:LinkButton ID="btnRun" runat="server" Font-Overline="false" ValidationGroup="ATK" CssClass="btnRun" OnClick="btnRun_Click"><asp:Literal ID="Literal1" runat="server" Text="Consultar"/><div></div></asp:LinkButton>
                        </div>                    
                    </td>
                    </tr>               
                </table> 
                
                <div  class="linhaH" >
                </div><br />
                
                <table class="tabelaAlinhada">
                <tr><td class="btn"></td>
                <th>                    
                    <asp:Label ID="labelCaixaAbertura" runat="server" Text="Caixa Abertura"></asp:Label>                
                </th><td class="btn"></td>
                <td>
                    <atk:AKNumericLabel ID="labelValorCaixaAbertura" runat="server"  Text=""></atk:AKNumericLabel>                
                </td>                    
                
                <th>
                    <asp:Label ID="labelVencimentosDia" runat="server"  Text="Vencimentos Dia"></asp:Label>                
                </th>
                <td>
                    <atk:AKNumericLabel ID="labelValorVencimentosDia" runat="server"  Text=""></atk:AKNumericLabel>                
                </td>                    
                </tr>
                <tr><td class="btn">
                    <asp:LinkButton ID="btnCaixa" runat="server" Font-Overline="false" Text="..." OnClientClick="popupLiquidacao.ShowAtElementByID(); return false;"></asp:LinkButton>                    
                </td>
                <th>
                    <asp:Label ID="labelCaixaFechamento" runat="server"  Text="Caixa Fechamento"></asp:Label>                
                 </th><td class="btn"></td>
                 <td>
                    <atk:AKNumericLabel ID="labelValorCaixaFechamento" runat="server"  Text=""></atk:AKNumericLabel>                
                </td>
                </tr>                
                </table>
                
                <div class="linhaH"></div><br />
                
                <table class="tabelaAlinhada">
                
                <tr>
                <td  class="btn">
                    <asp:LinkButton ID="btnBolsa" runat="server" Font-Overline="false" Text="..." OnClientClick="popupPosicaoBolsa.ShowAtElementByID(); return false;"></asp:LinkButton>                    
                </td><th>
                    <asp:Label ID="labelBolsa" runat="server"  Text="Bolsa"></asp:Label>                                                        
                </th><td class="btn">
                </td><td>
                    <atk:AKNumericLabel ID="labelValorBolsa" runat="server"  Text=""></atk:AKNumericLabel>                
                </td>
                
                <th>
                    <asp:Label ID="labelPerformance" runat="server"  Text="Performance Pagar"></asp:Label>                                    
                </th>
                <td>
                    <atk:AKNumericLabel ID="labelValorPerformance" runat="server"  Text=""></atk:AKNumericLabel>                
                </td>
                </tr>
                </tr>
                
                <tr>
                <td class="btn">
                    <asp:LinkButton ID="btnBMF" runat="server" Font-Overline="false" Text="..." OnClientClick="popupPosicaoBMF.ShowAtElementByID(); return false;"></asp:LinkButton>                    
                </td><th>
                    <asp:Label ID="labelBMF" runat="server"  Text="BMF"></asp:Label>                                                        
                </th><td class="btn">
                </td><td>
                    <atk:AKNumericLabel ID="labelValorBMF" runat="server"  Text=""></atk:AKNumericLabel>                
                </td>
                
                <th>
                    <asp:Label ID="labelAdm" runat="server"  Text="Administração Pagar"></asp:Label>                                    
                </th><td>
                    <atk:AKNumericLabel ID="labelValorAdm" runat="server"  Text=""></atk:AKNumericLabel>                
                </td>
                </tr>
                </tr>
                
                <tr>
                <td class="btn">
                    <asp:LinkButton ID="btnRendaFixa" runat="server" Font-Overline="false" Text="..." OnClientClick="popupPosicaoRendaFixa.ShowAtElementByID(); return false;"></asp:LinkButton>
                </td><th>
                    <asp:Label ID="labelRendaFixa" runat="server" CssClass="labelTitulo" Text="Renda Fixa + Swap "></asp:Label>                                    
                </th><td class="btn">
                    <asp:LinkButton ID="btnSwap" runat="server" Font-Overline="false" Text="..." OnClientClick="popupPosicaoSwap.ShowAtElementByID(); return false;"></asp:LinkButton>
                </td><td>
                    <atk:AKNumericLabel ID="labelValorRendaFixa" runat="server"  Text=""></atk:AKNumericLabel>                
                </td>
                <th>
                    <asp:Label ID="labelGestao" runat="server"  Text="Gestão Pagar"></asp:Label>                                    
                </th><td>
                    <atk:AKNumericLabel ID="labelValorGestao" runat="server"  Text=""></atk:AKNumericLabel>                
                </td>
                <tr>
                <td class="btn">
                    <asp:LinkButton ID="btnCotasInvestimento" runat="server" Font-Overline="false" Text="..." OnClientClick="popupPosicaoFundo.ShowAtElementByID(); return false;"></asp:LinkButton>
                    
                </td><th>
                    <asp:Label ID="labelCotasInvestimento" runat="server"  Text="Cotas de Investimento"></asp:Label>                                                        
                </th><td class="btn">
                </td><td>
                    <atk:AKNumericLabel ID="labelValorCotasInvestimento" runat="server"  Text=""></atk:AKNumericLabel>                
                </td>
                
                <tr>
                <td class="btn"></td>
                <th>
                    <asp:Label ID="labelReceberBolsa" runat="server"  Text="Rec. Bolsa"></asp:Label>                                                        
                </th><td class="btn"></td><td>
                    <atk:AKNumericLabel ID="labelValorReceberBolsa" runat="server"  Text=""></atk:AKNumericLabel>                
                </td>
                
                <th>
                    <asp:Label ID="labelPagarBolsa" runat="server"  Text="Pagar Bolsa"></asp:Label>                                                        
                </th><td>
                    <atk:AKNumericLabel ID="labelValorPagarBolsa" runat="server"  Text=""></atk:AKNumericLabel>                
                </td>
                </tr>
                
                <tr>
                <td class="btn"></td>
                <th>
                    <asp:Label ID="labelReceberBMF" runat="server"  Text="Rec. BMF"></asp:Label>                                                        
                </th><td class="btn"></td><td>
                    <atk:AKNumericLabel ID="labelValorReceberBMF" runat="server"  Text=""></atk:AKNumericLabel>                
                </td>
                
                <th>
                    <asp:Label ID="labelPagarBMF" runat="server"  Text="Pagar BMF"></asp:Label>                                                        
                </th><td>
                    <atk:AKNumericLabel ID="labelValorPagarBMF" runat="server"  Text=""></atk:AKNumericLabel>                
                </td>
                </tr>
                
                <tr>
                <td class="btn"></td>
                <th>
                    <asp:Label ID="labelReceberOutros" runat="server"  Text="Rec. Outros"></asp:Label>                                                        
                </th><td class="btn"></td><td>
                    <atk:AKNumericLabel ID="labelValorReceberOutros" runat="server"  Text=""></atk:AKNumericLabel>                
                </td>
                
                <th>
                    <asp:Label ID="labelPagarOutros" runat="server"  Text="Pagar Outros"></asp:Label>                                                        
                </th>
                <td>
                    <atk:AKNumericLabel ID="labelValorPagarOutros" runat="server"  Text=""></atk:AKNumericLabel>                
                </td>
                </tr>
                </table>
                
                <div class="linhaH" >
                </div><br />
                
                <table class="tabelaAlinhada">
                <tr>
                <th>
                    <asp:Label ID="labelAplicacoesDia" runat="server"  Text="Aplicações Dia"></asp:Label>                                                        
                </th><td>
                    <atk:AKNumericLabel ID="labelValorAplicacoesDia" runat="server"  Text=""></atk:AKNumericLabel>                
                </td>
                
                <th>
                    <asp:Label ID="labelPatBruto" runat="server"  Text="Pat. Bruto"></asp:Label>                                                        
                </th><td>
                    <atk:AKNumericLabel ID="labelValorPatBruto" runat="server"  Text=""></atk:AKNumericLabel>                
                </td>
                
                <th>
                    <asp:Label ID="labelPatLiquido" runat="server"  Text="Pat. Líquido"></asp:Label>                                                        
                </th><td>
                    <atk:AKNumericLabel ID="labelValorPatLiquido" runat="server"  Text=""></atk:AKNumericLabel>                
                </td>
                </tr>
                
                <tr>
                <th>
                    <asp:Label ID="labelResgatesDia" runat="server"  Text="Resgates Dia"></asp:Label>                                                        
                </th><td>
                    <atk:AKNumericLabel ID="labelValorResgatesDia" runat="server"  Text=""></atk:AKNumericLabel>                
                </td>
                
                <th>
                    <asp:Label ID="labelCotaBruta" runat="server"  Text="Cota Bruta"></asp:Label>                                                        
                </th><td>
                    <atk:AKNumericLabel ID="labelValorCotaBruta" runat="server"  Text=""></atk:AKNumericLabel>                
                </td>
                
                <th>
                    <asp:Label ID="labelCotaLiquida" runat="server"  Text="Cota Líquida"></asp:Label>                                                        
                </th><td>
                    <atk:AKNumericLabel ID="labelValorCotaLiquida" runat="server"  Text=""></atk:AKNumericLabel>                
                </td>
                </tr>
                
                <tr>
                <th>
                    <asp:Label ID="labelNet" runat="server"  Text="Net (Aplic - Resg)"></asp:Label>                                                        
                </th><td>
                    <atk:AKNumericLabel ID="labelValorNet" runat="server"  Text=""></atk:AKNumericLabel>                
                </td>
                
                <th>
                    <asp:Label ID="labelQtdeCotas" runat="server"  Text="Qtde Cotas"></asp:Label>                                                        
                </th><td>
                    <atk:AKNumericLabel ID="labelValorQtdeCotas" runat="server"  Text=""></atk:AKNumericLabel>                
                </td>
                </tr>
                </table>
                
                <div class="linhaH" >
                </div><br />
                
                <table class="tabelaAlinhada">
                <tr>
                <th>
                    <asp:Label ID="labelRentDia" runat="server"  Text="Rent. Dia"></asp:Label>                                                        
                </th><td class="tdEsq">
                    <atk:AKNumericLabel ID="labelValorRentDia" runat="server"  Text=""></atk:AKNumericLabel>                
                </td>
                
                <th>
                    <asp:Label ID="labelRentMes" runat="server"  Text="Rent. Mês"></asp:Label>                                                        
                </th><td class="tdEsq">
                    <atk:AKNumericLabel ID="labelValorRentMes" runat="server"  Text=""></atk:AKNumericLabel>                
                </td>

                <th>
                    <asp:Label ID="labelRentAno" runat="server"  Text="Rent. Ano"></asp:Label>                                                        
                </th><td class="tdEsq">
                    <atk:AKNumericLabel ID="labelValorRentAno" runat="server"  Text=""></atk:AKNumericLabel>                
                </td>
                
                <th>
                    <asp:Label ID="labelRent12" runat="server"  Text="Rent. 12 Meses"></asp:Label>                                                        
                </th><td class="tdEsq">
                    <atk:AKNumericLabel ID="labelValorRent12" runat="server"  Text=""></atk:AKNumericLabel>                
                </td>
                                
                </tr>
                </table><br />
            
            </div>
            
            </ContentTemplate>
            </asp:UpdatePanel>
                
        </div>
        </div>
        </td></tr></table>                
        </div>
        
        <cc1:esDataSource ID="EsDSCarteira" runat="server" OnesSelect="EsDSCarteira_esSelect" />
        <cc1:esDataSource ID="EsDSLiquidacao" runat="server" OnesSelect="EsDSLiquidacao_esSelect" />                
        <cc1:esDataSource ID="EsDSPosicaoBolsa" runat="server" OnesSelect="EsDSPosicaoBolsa_esSelect" />        
        <cc1:esDataSource ID="EsDSPosicaoBMF" runat="server" OnesSelect="EsDSPosicaoBMF_esSelect" />        
        <cc1:esDataSource ID="EsDSPosicaoFundo" runat="server" OnesSelect="EsDSPosicaoFundo_esSelect" />        
        <cc1:esDataSource ID="EsDSPosicaoRendaFixa" runat="server" OnesSelect="EsDSPosicaoRendaFixa_esSelect" />        
        <cc1:esDataSource ID="EsDSPosicaoSwap" runat="server" OnesSelect="EsDSPosicaoSwap_esSelect" />        
        
    </form>
</body>
</html>