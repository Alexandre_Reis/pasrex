﻿using System;
using System.Web;
using Financial.Security;
using Financial.Fundo;
using System.Collections.Generic;
using Financial.Web.Common;
using Financial.RendaFixa;
using Financial.Investidor;
using Financial.RendaFixa.Enums;
using Financial.Common.Enums;
using Financial.Util;
using Financial.Common;
using Financial.Bolsa;
using Financial.Bolsa.Enums;
using Financial.Fundo.Enums;
using Financial.Investidor.Enums;

public partial class Consultas_Vencimentos : ConsultaBasePage {
    new protected void Page_Load(object sender, EventArgs e) {
        this.HasPopupCarteira = true;
        btnEditCodigoCarteira.Focus();
        //
        base.Page_Load(sender, e);
    }

    protected void EsDSVencimentos_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e) {

        #region Extra Binding
        PosicaoRendaFixaCollection posicaoRendaFixaCollectionBinding = new PosicaoRendaFixaCollection();

        posicaoRendaFixaCollectionBinding.CreateColumnsForBinding();
        posicaoRendaFixaCollectionBinding.AddColumn("Id", typeof(System.Int32));
        //
        posicaoRendaFixaCollectionBinding.AddColumn("Nome", typeof(System.String));
        posicaoRendaFixaCollectionBinding.AddColumn("Ativo", typeof(System.String));
        posicaoRendaFixaCollectionBinding.AddColumn("Vencimento", typeof(System.DateTime));
        posicaoRendaFixaCollectionBinding.AddColumn("Evento", typeof(System.String));
        posicaoRendaFixaCollectionBinding.AddColumn("Agente", typeof(System.String));
        //
        posicaoRendaFixaCollectionBinding.AddColumn("ValorBruto", typeof(System.Decimal));
        posicaoRendaFixaCollectionBinding.AddColumn("ValorLiquido", typeof(System.Decimal));
        posicaoRendaFixaCollectionBinding.AddColumn("Tributos", typeof(System.Decimal));
        #endregion

        if (!Page.IsPostBack) {
            gridConsulta.Visible = false;
            //
            e.Collection = posicaoRendaFixaCollectionBinding;
        }
        else {

            PosicaoRendaFixaCollection posicaoRendaFixaCollection = new PosicaoRendaFixaCollection();
            #region Consulta em PosicaoRendaFixa
            PosicaoRendaFixaQuery posicaoRendaFixaQuery = new PosicaoRendaFixaQuery("P");
            ClienteQuery clienteQuery = new ClienteQuery("C");
            TituloRendaFixaQuery tituloRendaFixaQuery = new TituloRendaFixaQuery("T");
            AgendaRendaFixaQuery agendaRendaFixaQuery = new AgendaRendaFixaQuery("A");
            AgenteMercadoQuery agenteMercadoQuery = new AgenteMercadoQuery("G");
            //
            agendaRendaFixaQuery.es.Distinct = true;
            agendaRendaFixaQuery.Select(agendaRendaFixaQuery.IdTitulo);
            
            posicaoRendaFixaQuery.Select(clienteQuery.IdCliente, clienteQuery.Apelido,
                                         agenteMercadoQuery.Nome,
                                         tituloRendaFixaQuery.DescricaoCompleta,
                                         tituloRendaFixaQuery.DataVencimento,
                                         tituloRendaFixaQuery.IsentoIR,
                                         posicaoRendaFixaQuery.ValorMercado.Sum(),
                                         posicaoRendaFixaQuery.ValorIR.Sum(),
                                         posicaoRendaFixaQuery.ValorIOF.Sum());

            posicaoRendaFixaQuery.InnerJoin(clienteQuery).On(clienteQuery.IdCliente == posicaoRendaFixaQuery.IdCliente);
            posicaoRendaFixaQuery.InnerJoin(tituloRendaFixaQuery).On(tituloRendaFixaQuery.IdTitulo == posicaoRendaFixaQuery.IdTitulo);
            posicaoRendaFixaQuery.LeftJoin(agenteMercadoQuery).On(agenteMercadoQuery.IdAgente == posicaoRendaFixaQuery.IdAgente);
            //            
            posicaoRendaFixaQuery.Where(posicaoRendaFixaQuery.DataVencimento.GreaterThanOrEqual(clienteQuery.DataDia),
                                        tituloRendaFixaQuery.IdTitulo.NotIn(agendaRendaFixaQuery),
                                        clienteQuery.StatusAtivo.Equal((byte)StatusAtivoCliente.Ativo));

            if (btnEditCodigoCarteira.Text != "") {
                posicaoRendaFixaQuery.Where(posicaoRendaFixaQuery.IdCliente == Convert.ToInt32(btnEditCodigoCarteira.Text));
            }
            if (textDataInicio.Text != "") {
                posicaoRendaFixaQuery.Where(posicaoRendaFixaQuery.DataVencimento >= Convert.ToDateTime(textDataInicio.Text));
            }
            if (textDataFim.Text != "") {
                posicaoRendaFixaQuery.Where(posicaoRendaFixaQuery.DataVencimento <= Convert.ToDateTime(textDataFim.Text));
            }

            posicaoRendaFixaQuery.GroupBy(clienteQuery.IdCliente,
                                         clienteQuery.Apelido,
                                         agenteMercadoQuery.Nome,
                                         tituloRendaFixaQuery.DescricaoCompleta,
                                         tituloRendaFixaQuery.DataVencimento,
                                         tituloRendaFixaQuery.IsentoIR);

            posicaoRendaFixaQuery.OrderBy(clienteQuery.Apelido.Ascending);
            //
            posicaoRendaFixaCollection.Load(posicaoRendaFixaQuery);
            #endregion

            #region Extra Binding (PosicaoRendaFixa)
            for (int i = 0; i < posicaoRendaFixaCollection.Count; i++)
            {
                PosicaoRendaFixa p = posicaoRendaFixaCollection[i];
                //
                PosicaoRendaFixa pBinding = posicaoRendaFixaCollectionBinding.AddNew();
                //
                pBinding.SetColumn("Id", Convert.ToInt32(p.GetColumn(ClienteMetadata.ColumnNames.IdCliente)));
                //
                pBinding.SetColumn("Nome", p.GetColumn(ClienteMetadata.ColumnNames.Apelido));
                pBinding.SetColumn("Ativo", p.GetColumn(TituloRendaFixaMetadata.ColumnNames.DescricaoCompleta));
                pBinding.SetColumn("Vencimento", p.GetColumn(TituloRendaFixaMetadata.ColumnNames.DataVencimento));

                if (Convert.IsDBNull(p.GetColumn(AgenteMercadoMetadata.ColumnNames.Nome)))
                {
                    pBinding.SetColumn("Agente", "");
                }
                else
                {
                    pBinding.SetColumn("Agente", p.GetColumn(AgenteMercadoMetadata.ColumnNames.Nome));
                }

                pBinding.SetColumn("Evento", "Vencimento");
                //
                decimal valorBruto = 0;
                decimal tributos = 0;
                decimal valorLiquido = 0;
                if (Convert.ToByte(p.GetColumn(TituloRendaFixaMetadata.ColumnNames.IsentoIR)) == (byte)TituloIsentoIR.NaoIsento)
                {
                    valorBruto = p.ValorMercado.Value;
                    tributos = p.ValorIR.Value;
                    valorLiquido = valorBruto - tributos;
                }
                else
                {
                    valorLiquido = p.ValorMercado.Value - p.ValorIR.Value;
                    tributos = 0;
                    valorBruto = valorLiquido; //Retira o grossup
                }
                pBinding.SetColumn("ValorBruto", valorBruto);
                pBinding.SetColumn("Tributos", tributos);
                pBinding.SetColumn("ValorLiquido", valorLiquido);
            }
            #endregion

            #region Consulta em AgendaRendaFixa
            posicaoRendaFixaCollection = new PosicaoRendaFixaCollection();
            AgendaRendaFixaCollection agendaRendaFixaCollection = new AgendaRendaFixaCollection();
            //
            posicaoRendaFixaQuery = new PosicaoRendaFixaQuery("P");
            agendaRendaFixaQuery = new AgendaRendaFixaQuery("A");
            clienteQuery = new ClienteQuery("C");
            agenteMercadoQuery = new AgenteMercadoQuery("G");
            
            posicaoRendaFixaQuery.Select(clienteQuery.IdCliente, clienteQuery.Apelido, clienteQuery.DataDia,
                                         posicaoRendaFixaQuery,
                                         agendaRendaFixaQuery,
                                         agenteMercadoQuery.Nome);

            posicaoRendaFixaQuery.InnerJoin(clienteQuery).On(clienteQuery.IdCliente == posicaoRendaFixaQuery.IdCliente);
            posicaoRendaFixaQuery.LeftJoin(agenteMercadoQuery).On(agenteMercadoQuery.IdAgente == posicaoRendaFixaQuery.IdAgente);
            posicaoRendaFixaQuery.InnerJoin(agendaRendaFixaQuery).On(agendaRendaFixaQuery.IdTitulo == posicaoRendaFixaQuery.IdTitulo);
            posicaoRendaFixaQuery.Where(agendaRendaFixaQuery.DataPagamento.GreaterThanOrEqual(clienteQuery.DataDia),
                                        clienteQuery.StatusAtivo.Equal((byte)StatusAtivoCliente.Ativo));

            if (btnEditCodigoCarteira.Text != "")
            {
                posicaoRendaFixaQuery.Where(posicaoRendaFixaQuery.IdCliente == Convert.ToInt32(btnEditCodigoCarteira.Text));
            }
            if (textDataInicio.Text != "")
            {
                posicaoRendaFixaQuery.Where(agendaRendaFixaQuery.DataPagamento >= Convert.ToDateTime(textDataInicio.Text));
            }
            if (textDataFim.Text != "")
            {
                posicaoRendaFixaQuery.Where(agendaRendaFixaQuery.DataPagamento <= Convert.ToDateTime(textDataFim.Text));
            }

            posicaoRendaFixaQuery.OrderBy(clienteQuery.Apelido.Ascending);
            //
            posicaoRendaFixaCollection.Load(posicaoRendaFixaQuery);
            #endregion

            #region Extra Binding (AgendaRendaFixa)
            for (int i = 0; i < posicaoRendaFixaCollection.Count; i++)
            {
                PosicaoRendaFixa p = posicaoRendaFixaCollection[i];
                DateTime dataOperacao = p.DataOperacao.Value;
                int idCliente = Convert.ToInt32(p.GetColumn(ClienteMetadata.ColumnNames.IdCliente));
                DateTime dataDiaCliente = Convert.ToDateTime(p.GetColumn(ClienteMetadata.ColumnNames.DataDia));
                DateTime dataEvento = Convert.ToDateTime(p.GetColumn(AgendaRendaFixaMetadata.ColumnNames.DataEvento));
                byte tipoEvento = Convert.ToByte(p.GetColumn(AgendaRendaFixaMetadata.ColumnNames.TipoEvento));                

                decimal valorEvento = 0;
                if (p.GetColumn(AgendaRendaFixaMetadata.ColumnNames.Valor) != null)
                {
                    valorEvento = Convert.ToDecimal(p.GetColumn(AgendaRendaFixaMetadata.ColumnNames.Valor));
                }

                decimal taxaEvento = Convert.ToDecimal(p.GetColumn(AgendaRendaFixaMetadata.ColumnNames.Taxa));
                int idTitulo = p.IdTitulo.Value;

                TituloRendaFixa tituloRendaFixa = new TituloRendaFixa();
                tituloRendaFixa.LoadByPrimaryKey(idTitulo);

                PapelRendaFixa papelRendaFixa = new PapelRendaFixa();
                papelRendaFixa.LoadByPrimaryKey(tituloRendaFixa.IdPapel.Value);
                //
                PosicaoRendaFixa pBinding = posicaoRendaFixaCollectionBinding.AddNew();
                //
                pBinding.SetColumn("Id", idCliente);                //
                pBinding.SetColumn("Nome", p.GetColumn(ClienteMetadata.ColumnNames.Apelido));
                pBinding.SetColumn("Vencimento", dataEvento);
                pBinding.SetColumn("Ativo", tituloRendaFixa.DescricaoCompleta);

                if (Convert.IsDBNull(p.GetColumn(AgenteMercadoMetadata.ColumnNames.Nome)))
                {
                    pBinding.SetColumn("Agente", "");
                }
                else
                {
                    pBinding.SetColumn("Agente", p.GetColumn(AgenteMercadoMetadata.ColumnNames.Nome));
                }
                                
                if (tipoEvento == (byte)TipoEventoTitulo.Amortizacao)
                {
                    pBinding.SetColumn("Evento", "Amortização");
                }
                if (tipoEvento == (byte)TipoEventoTitulo.PagamentoPU)
                {
                    pBinding.SetColumn("Evento", "Pagamento PU");
                }
                else if (tipoEvento == (byte)TipoEventoTitulo.IncorporacaoJuros ||
                         tipoEvento == (byte)TipoEventoTitulo.Juros)
                {
                    pBinding.SetColumn("Evento", "Juros");
                }
                else if (tipoEvento == (byte)TipoEventoTitulo.JurosCorrecao)
                {
                    pBinding.SetColumn("Evento", "Juros");
                }
                else if (tipoEvento == (byte)TipoEventoTitulo.PagamentoCorrecao)
                {
                    pBinding.SetColumn("Evento", "Correção");
                }
                //

                #region Calcula Valor do Evento
                DateTime dataBase = Convert.ToDateTime(p.GetColumn(TituloRendaFixaMetadata.ColumnNames.DataEmissao));

                decimal fatorCorrecao = 1;
                #region Calcula Fator de Correção
                DateTime dataBaseCorrecao = dataBase;
                Indice indice = new Indice();                
                if (tituloRendaFixa.IdIndice.HasValue)
                {
                    #region Acha data base de correção do indice
                    dataBaseCorrecao = tituloRendaFixa.DataEmissao.Value;

                    DateTime dataHoje = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day);
                    DateTime dataOntem = Calendario.SubtraiDiaUtil(dataHoje, 1);
                    DateTime dataCriterio = dataOntem;

                    if (dataEvento < dataCriterio)
                    {
                        dataCriterio = dataEvento;
                    }

                    AgendaRendaFixaCollection agendaRendaFixaCollectionCorrecao = new AgendaRendaFixaCollection();
                    agendaRendaFixaCollectionCorrecao.Query.Select(agendaRendaFixaCollectionCorrecao.Query.DataEvento);
                    agendaRendaFixaCollectionCorrecao.Query.Where(agendaRendaFixaCollectionCorrecao.Query.IdTitulo.Equal(idTitulo),
                                                          agendaRendaFixaCollectionCorrecao.Query.DataEvento.LessThan(dataCriterio),
                                                          agendaRendaFixaCollectionCorrecao.Query.TipoEvento.In((byte)TipoEventoTitulo.JurosCorrecao,
                                                                                                                (byte)TipoEventoTitulo.PagamentoCorrecao));
                    agendaRendaFixaCollectionCorrecao.Query.OrderBy(agendaRendaFixaCollectionCorrecao.Query.DataEvento.Descending);
                    agendaRendaFixaCollectionCorrecao.Query.Load();

                    if (agendaRendaFixaCollectionCorrecao.Count > 0)
                    {
                        dataBaseCorrecao = agendaRendaFixaCollectionCorrecao[0].DataEvento.Value;
                    }
                    #endregion

                    indice.LoadByPrimaryKey(tituloRendaFixa.IdIndice.Value);

                    CalculoRendaFixa calculoRendaFixa = new CalculoRendaFixa();
                    try
                    {
                        fatorCorrecao = calculoRendaFixa.RetornaFatorCorrecao(dataDiaCliente, dataBaseCorrecao, papelRendaFixa, tituloRendaFixa, indice);
                    }
                    catch (Exception e1)
                    {   
                    }
                    
                }
                #endregion

                decimal puNominalAmortizado = tituloRendaFixa.PUNominal.Value;
                #region Ajusta o PU nominal pelas amortizações ocorridas
                AgendaRendaFixaCollection agendaRendaFixaCollectionAmortizacao = new AgendaRendaFixaCollection();
                agendaRendaFixaCollectionAmortizacao.Query.Select(agendaRendaFixaCollectionAmortizacao.Query.Taxa,
                                                                  agendaRendaFixaCollectionAmortizacao.Query.Valor);
                agendaRendaFixaCollectionAmortizacao.Query.Where(agendaRendaFixaCollectionAmortizacao.Query.IdTitulo.Equal(idTitulo),
                                                      agendaRendaFixaCollectionAmortizacao.Query.DataPagamento.LessThan(dataEvento),
                                                      agendaRendaFixaCollectionAmortizacao.Query.TipoEvento.In((byte)TipoEventoTitulo.Amortizacao,
                                                                                                               (byte)TipoEventoTitulo.AmortizacaoCorrigida),
                                                      agendaRendaFixaCollectionAmortizacao.Query.Taxa.IsNotNull());
                agendaRendaFixaCollectionAmortizacao.Query.Load();

                foreach (AgendaRendaFixa agendaRendaFixaAmortizacao in agendaRendaFixaCollectionAmortizacao)
                {
                    if (agendaRendaFixaAmortizacao.Valor.HasValue && agendaRendaFixaAmortizacao.Valor.Value != 0 )
                    {
                        puNominalAmortizado = puNominalAmortizado - agendaRendaFixaAmortizacao.Valor.Value;
                    }
                    else
                    {
                        decimal taxaAmortizacao = agendaRendaFixaAmortizacao.Taxa.Value;
                        puNominalAmortizado = puNominalAmortizado - tituloRendaFixa.PUNominal.Value * taxaAmortizacao / 100M;
                    }
                }
                #endregion

                DateTime dataUltimoJuro = tituloRendaFixa.DataEmissao.Value;
                #region Acha data do último juro
                AgendaRendaFixaCollection agendaRendaFixaCollectionJuros = new AgendaRendaFixaCollection();
                agendaRendaFixaCollectionJuros.Query.Select(agendaRendaFixaCollectionJuros.Query.DataEvento, 
                                                            agendaRendaFixaCollectionJuros.Query.DataPagamento);
                agendaRendaFixaCollectionJuros.Query.Where(agendaRendaFixaCollectionJuros.Query.IdTitulo.Equal(idTitulo),
                                                      agendaRendaFixaCollectionJuros.Query.DataPagamento.LessThan(dataEvento),
                                                      agendaRendaFixaCollectionJuros.Query.TipoEvento.In((byte)TipoEventoTitulo.Juros,
                                                                                                         (byte)TipoEventoTitulo.JurosCorrecao));
                agendaRendaFixaCollectionJuros.Query.OrderBy(agendaRendaFixaCollectionJuros.Query.DataEvento.Descending);
                agendaRendaFixaCollectionJuros.Query.Load();

                if (agendaRendaFixaCollectionJuros.Count > 0)
                {
                    dataUltimoJuro = agendaRendaFixaCollectionJuros[0].DataPagamento.Value;
                }
                #endregion

                //Se a taxa for informada para os juros, calculo o valor a ser pago, senão assume o valor informado
                if (tipoEvento == (byte)TipoEventoTitulo.Amortizacao || tipoEvento == (byte)TipoEventoTitulo.AmortizacaoCorrigida)
                {
                    if (taxaEvento != 0)
                    {
                        valorEvento = tituloRendaFixa.PUNominal.Value * taxaEvento / 100M;
                    }

                    if (tipoEvento == (byte)TipoEventoTitulo.AmortizacaoCorrigida)
                    {
                        valorEvento = valorEvento * fatorCorrecao;
                    }
                }
                else if (tipoEvento == (byte)TipoEventoTitulo.PagamentoPrincipal)
                {
                    valorEvento = puNominalAmortizado;

                    valorEvento = valorEvento * fatorCorrecao;
                }
                else if (tipoEvento == (byte)TipoEventoTitulo.PagamentoCorrecao)
                {
                    #region Trata IncorporacaoCorrecao
                    if (taxaEvento == 0)
                    {
                        taxaEvento = 100;
                    }

                    if (taxaEvento == 100)
                    {
                        valorEvento = (puNominalAmortizado * fatorCorrecao) - puNominalAmortizado;
                    }
                    else
                    {
                        decimal valorAmortizar = tituloRendaFixa.PUNominal.Value * taxaEvento / 100M;
                        valorEvento = (valorAmortizar * fatorCorrecao) - valorAmortizar;
                    }
                    #endregion
                }
                else if (taxaEvento != 0 && valorEvento == 0 && tipoEvento != (byte)TipoEventoTitulo.PagamentoPU)
                {
                    #region Trata eventos com taxa != 0 (Pagamento de Juros)
                    int numeroDiasNumerador = 0;
                    int numeroDiasBase = papelRendaFixa.BaseAno.Value;
                    if (papelRendaFixa.PagamentoJuros == (byte)PagamentoJurosTitulo.ContaDias || papelRendaFixa.PagamentoJuros == (byte)PagamentoJurosTitulo.ContaDiasCorridos)
                    {
                        if (papelRendaFixa.ContagemDias == (byte)ContagemDiasTitulo.Corridos)
                        {
                            numeroDiasNumerador = Calendario.NumeroDias(dataUltimoJuro, dataEvento);
                        }
                        else if (papelRendaFixa.ContagemDias == (byte)ContagemDiasTitulo.Dias360)
                        {
                            numeroDiasNumerador = Calendario.NumeroDias360(dataUltimoJuro, dataEvento);
                        }
                        else
                        {
                            numeroDiasNumerador = Calendario.NumeroDias(dataUltimoJuro, dataEvento, (byte)LocalFeriadoFixo.Brasil, TipoFeriado.Brasil);
                        }
                    }
                    else
                    {
                        numeroDiasNumerador = numeroDiasBase;
                    }

                    decimal fatorJuros = 1;
                    if (papelRendaFixa.TipoCurva == (byte)TipoCurvaTitulo.Exponencial)
                    {
                        fatorJuros = Math.Round((decimal)Math.Pow((double)(1 + taxaEvento / 100M), (double)numeroDiasNumerador / numeroDiasBase), 9);
                    }
                    else
                    {
                        fatorJuros = Math.Round((taxaEvento / 100M) * numeroDiasNumerador / numeroDiasBase, 9) + 1;
                    }

                    if (tipoEvento == (byte)TipoEventoTitulo.Juros)
                    {
                        valorEvento = (puNominalAmortizado * fatorCorrecao) * (fatorJuros - 1);
                    }
                    else if (tipoEvento == (byte)TipoEventoTitulo.JurosCorrecao) //Descarrega toda a correção
                    {
                        valorEvento = (puNominalAmortizado * fatorJuros * fatorCorrecao) - puNominalAmortizado;
                    }
                    #endregion
                }
                else
                {
                    if (tipoEvento != (byte)TipoEventoTitulo.PagamentoPU)
                    {
                        if (tipoEvento == (byte)TipoEventoTitulo.JurosCorrecao) //Descarrega toda a correção
                        {
                            decimal fatorJuros = valorEvento / puNominalAmortizado + 1M;
                            valorEvento = (puNominalAmortizado * fatorJuros * fatorCorrecao) - puNominalAmortizado;
                        }
                        else
                        {
                            valorEvento = valorEvento * fatorCorrecao;
                        }
                    }
                }

                decimal quantidade = p.Quantidade.Value; ;
                decimal valorLiquidacao = Utilitario.Truncate(valorEvento * quantidade, 2);
                #endregion

                pBinding.SetColumn("ValorBruto", valorLiquidacao);

                #region Calcula IR do evento
                decimal valorIR = 0;                
                Cliente cliente = new Cliente();
                cliente.LoadByPrimaryKey(idCliente);

                ClienteRendaFixa clienteRendaFixa = new ClienteRendaFixa();
                if (clienteRendaFixa.LoadByPrimaryKey(idCliente))
                {
                    if (tipoEvento != (byte)TipoEventoTitulo.Amortizacao && tituloRendaFixa.IsentoIR.Value == (byte)TituloIsentoIR.NaoIsento)
                    {
                        CalculoRendaFixa c = new CalculoRendaFixa();
                        valorIR = c.RetornaIRCalculado(cliente, clienteRendaFixa, dataOperacao, dataEvento, valorLiquidacao);
                    }
                }
                #endregion

                pBinding.SetColumn("Tributos", valorIR);
                //
                decimal valorLiquido = valorLiquidacao - valorIR;
                pBinding.SetColumn("ValorLiquido", valorLiquido);
            }
            #endregion

            #region Consulta em ProventoBolsaCliente
            ProventoBolsaClienteCollection proventoBolsaClienteCollection = new ProventoBolsaClienteCollection();
            ProventoBolsaClienteQuery proventoBolsaClienteQuery = new ProventoBolsaClienteQuery("P");
            clienteQuery = new ClienteQuery("C");
            agenteMercadoQuery = new AgenteMercadoQuery("G");

            proventoBolsaClienteQuery.Select(clienteQuery.IdCliente, clienteQuery.Apelido,
                                         proventoBolsaClienteQuery.CdAtivoBolsa,
                                         proventoBolsaClienteQuery.TipoProvento,
                                         proventoBolsaClienteQuery.DataPagamento,
                                         proventoBolsaClienteQuery.PercentualIR,
                                         agenteMercadoQuery.Nome,
                                         proventoBolsaClienteQuery.Valor.Sum());

            proventoBolsaClienteQuery.InnerJoin(clienteQuery).On(clienteQuery.IdCliente == proventoBolsaClienteQuery.IdCliente);
            proventoBolsaClienteQuery.LeftJoin(agenteMercadoQuery).On(agenteMercadoQuery.IdAgente == proventoBolsaClienteQuery.IdAgenteMercado);
            proventoBolsaClienteQuery.Where(proventoBolsaClienteQuery.DataPagamento.GreaterThanOrEqual(clienteQuery.DataDia),
                                            clienteQuery.StatusAtivo.Equal((byte)StatusAtivoCliente.Ativo));

            if (btnEditCodigoCarteira.Text != "")
            {
                proventoBolsaClienteQuery.Where(proventoBolsaClienteQuery.IdCliente == Convert.ToInt32(btnEditCodigoCarteira.Text));
            }
            if (textDataInicio.Text != "")
            {
                proventoBolsaClienteQuery.Where(proventoBolsaClienteQuery.DataPagamento >= Convert.ToDateTime(textDataInicio.Text));
            }
            if (textDataFim.Text != "")
            {
                proventoBolsaClienteQuery.Where(proventoBolsaClienteQuery.DataPagamento <= Convert.ToDateTime(textDataFim.Text));
            }

            proventoBolsaClienteQuery.GroupBy(clienteQuery.IdCliente,
                                              clienteQuery.Apelido,
                                              proventoBolsaClienteQuery.CdAtivoBolsa,
                                              proventoBolsaClienteQuery.TipoProvento,
                                              proventoBolsaClienteQuery.DataPagamento,
                                              proventoBolsaClienteQuery.PercentualIR,
                                              agenteMercadoQuery.Nome);

            proventoBolsaClienteQuery.OrderBy(clienteQuery.Apelido.Ascending);
            //
            proventoBolsaClienteCollection.Load(proventoBolsaClienteQuery);
            #endregion

            #region Extra Binding (ProventoBolsaCliente)
            for (int i = 0; i < proventoBolsaClienteCollection.Count; i++)
            {
                ProventoBolsaCliente p = proventoBolsaClienteCollection[i];
                //
                PosicaoRendaFixa pBinding = posicaoRendaFixaCollectionBinding.AddNew();
                //
                pBinding.SetColumn("Id", Convert.ToInt32(p.GetColumn(ClienteMetadata.ColumnNames.IdCliente)));
                //
                pBinding.SetColumn("Nome", p.GetColumn(ClienteMetadata.ColumnNames.Apelido));
                pBinding.SetColumn("Ativo", p.CdAtivoBolsa);
                pBinding.SetColumn("Vencimento", p.DataPagamento.Value);

                if (Convert.IsDBNull(p.GetColumn(AgenteMercadoMetadata.ColumnNames.Nome)))
                {
                    pBinding.SetColumn("Agente", "");
                }
                else
                {
                    pBinding.SetColumn("Agente", p.GetColumn(AgenteMercadoMetadata.ColumnNames.Nome));
                }

                byte tipoProvento = p.TipoProvento.Value;

                if (tipoProvento == (byte)TipoProventoBolsa.Dividendo)
                {
                    pBinding.SetColumn("Evento", "Dividendo");
                }
                else if (tipoProvento == (byte)TipoProventoBolsa.JurosSobreCapital)
                {
                    pBinding.SetColumn("Evento", "Juros s/ Capital");
                }
                else
                {
                    pBinding.SetColumn("Evento", "Rendimento");
                }                
                //

                decimal valor = p.Valor.Value;
                decimal percentual = p.PercentualIR.Value;
                decimal valorIR = Utilitario.Truncate(valor * percentual / 100, 2);
                decimal valorLiquido = valor - valorIR;

                pBinding.SetColumn("ValorBruto", valor);
                pBinding.SetColumn("Tributos", valorIR);
                pBinding.SetColumn("ValorLiquido", valorLiquido);
            }
            #endregion

            #region Consulta em OperacaoFundo
            OperacaoFundoCollection operacaoFundoCollection = new OperacaoFundoCollection();
            OperacaoFundoQuery operacaoFundoQuery = new OperacaoFundoQuery("O");
            CarteiraQuery carteiraQuery = new CarteiraQuery("A");
            clienteQuery = new ClienteQuery("C");

            operacaoFundoQuery.Select(clienteQuery.IdCliente, clienteQuery.Apelido,
                                      carteiraQuery.IdCarteira, carteiraQuery.Apelido,
                                      operacaoFundoQuery.DataLiquidacao,
                                      operacaoFundoQuery.TipoOperacao,
                                      operacaoFundoQuery.ValorBruto,
                                      operacaoFundoQuery.ValorLiquido,
                                      operacaoFundoQuery.ValorIR,
                                      operacaoFundoQuery.ValorIOF);

            operacaoFundoQuery.InnerJoin(clienteQuery).On(clienteQuery.IdCliente == operacaoFundoQuery.IdCliente);
            operacaoFundoQuery.InnerJoin(carteiraQuery).On(carteiraQuery.IdCarteira == operacaoFundoQuery.IdCarteira);
            operacaoFundoQuery.Where(operacaoFundoQuery.DataConversao.LessThanOrEqual(clienteQuery.DataDia));
            operacaoFundoQuery.Where(operacaoFundoQuery.DataLiquidacao.GreaterThanOrEqual(clienteQuery.DataDia));
            operacaoFundoQuery.Where(operacaoFundoQuery.IdCliente.NotEqual(operacaoFundoQuery.IdCarteira));
            operacaoFundoQuery.Where(operacaoFundoQuery.TipoOperacao.NotEqual((byte)TipoOperacaoFundo.ComeCotas));
            operacaoFundoQuery.Where(clienteQuery.StatusAtivo.Equal((byte)StatusAtivoCliente.Ativo));

            if (btnEditCodigoCarteira.Text != "")
            {
                operacaoFundoQuery.Where(operacaoFundoQuery.IdCliente == Convert.ToInt32(btnEditCodigoCarteira.Text));
            }
            if (textDataInicio.Text != "")
            {
                operacaoFundoQuery.Where(operacaoFundoQuery.DataLiquidacao >= Convert.ToDateTime(textDataInicio.Text));
            }
            if (textDataFim.Text != "")
            {
                operacaoFundoQuery.Where(operacaoFundoQuery.DataLiquidacao <= Convert.ToDateTime(textDataFim.Text));
            }

            operacaoFundoQuery.OrderBy(clienteQuery.Apelido.Ascending);
            //
            operacaoFundoCollection.Load(operacaoFundoQuery);
            #endregion

            #region Extra Binding (OperacaoFundo)
            for (int i = 0; i < operacaoFundoCollection.Count; i++)
            {
                OperacaoFundo o = operacaoFundoCollection[i];
                //
                PosicaoRendaFixa pBinding = posicaoRendaFixaCollectionBinding.AddNew();
                //
                pBinding.SetColumn("Id", Convert.ToInt32(o.GetColumn(ClienteMetadata.ColumnNames.IdCliente)));
                //
                pBinding.SetColumn("Nome", o.GetColumn(ClienteMetadata.ColumnNames.Apelido));
                pBinding.SetColumn("Ativo", o.GetColumn(CarteiraMetadata.ColumnNames.Apelido));
                pBinding.SetColumn("Vencimento", o.DataLiquidacao.Value);
                pBinding.SetColumn("Evento", "Resgate");
                pBinding.SetColumn("Agente", "");

                byte tipoOperacao = o.TipoOperacao.Value;

                decimal valorBruto = o.ValorBruto.Value;
                decimal tributos = o.ValorIR.Value + o.ValorIOF.Value;
                decimal valorLiquido = o.ValorLiquido.Value;
                //
                pBinding.SetColumn("ValorBruto", valorBruto);
                pBinding.SetColumn("Tributos", tributos);
                pBinding.SetColumn("ValorLiquido", valorLiquido);
            }
            #endregion

            posicaoRendaFixaCollectionBinding.LowLevelBind();
            //
            e.Collection = posicaoRendaFixaCollectionBinding;

            e.Collection.Sort = "Id ASC, Vencimento ASC";
        }
    }

    protected void EsDSCarteira_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e) {
        CarteiraCollection carteiraCollection = new CarteiraCollection();
        carteiraCollection.BuscaCarteirasComAcessoTodas(HttpContext.Current.User.Identity.Name);

        e.Collection = carteiraCollection;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void ASPxCallback1_Callback(object source, DevExpress.Web.CallbackEventArgs e) {
        string nome = "";
        if (!String.IsNullOrEmpty(e.Parameter) && e.Parameter != "null") {
            int idCarteira = Convert.ToInt32(e.Parameter);
            Carteira carteira = new Carteira();
            carteira.LoadByPrimaryKey(idCarteira);

            if (carteira.str.Apelido != "") {
                if (carteira.IsAtivo) {
                    //O permissionamento da carteira é dado direto no cliente vinculado à carteira
                    PermissaoCliente permissaoCliente = new PermissaoCliente();
                    if (permissaoCliente.RetornaAcessoCliente(idCarteira, HttpContext.Current.User.Identity.Name)) {
                        nome = carteira.str.Apelido;
                    }
                    else {
                        nome = "no_access";
                    }
                }
                else {
                    nome = "no_active";
                }
            }
        }
        e.Result = nome;
    }

    #region btnRun_Click, btnPDF_Click, btnExcel_Click

    new protected void btnRun_Click(object sender, EventArgs e) {
        base.btnRun_Click(sender, e); // DataBind do Grid
    }

    new protected void btnPDF_Click(object sender, EventArgs e) {
        this.SelecionaRelatorio(VisaoRelatorio.visaoPDF);
    }

    new protected void btnExcel_Click(object sender, EventArgs e) {
        this.SelecionaRelatorio(VisaoRelatorio.visaoExcel);
    }

    /// <summary>
    /// Seleciona o Relatorio a Ser Executado
    /// </summary>
    /// <param name="visao">PDF, Excel ou Report</param>
    private void SelecionaRelatorio(string visao) {
        Response.BufferOutput = true;
        //
        // Salva os Parametros na Session
        Session["gridExportVencimentos"] = gridExport;
        Response.Redirect("~/Consultas/VencimentosExporta.aspx?Visao=" + visao);
    }

    #endregion
}