﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Text;
using System.Collections.Generic;
using System.Drawing;

using DevExpress.Web;

using EntitySpaces.Interfaces;

using Financial.Security;
using Financial.ContaCorrente;
using Financial.Web.Util;
using Financial.Investidor;
using Financial.Bolsa;
using Financial.Common;
using Financial.InvestidorCotista;
using Financial.Fundo;
using Financial.Security.Enums;
using Financial.Web.Common;

public partial class Consultas_PosicaoCotistaGeral : ConsultaBasePage
{
    new protected void Page_Load(object sender, EventArgs e)
    {
        this.HasPopupCotista = true;
        btnEditCodigoCotista.Focus();
        this.HasPopupCarteira = true;
        btnEditCodigoCarteira.Focus();
        //this.HasPanelFieldsLoading = true;
        base.Page_Load(sender, e);

        this.PersonalizaGridConsulta();

        TrataTravamentoCampos();

    }

    /// <summary>
    /// Configura Grid de Consulta
    /// </summary>
    private void PersonalizaGridConsulta()
    {
        // Enable ViewState
        this.gridConsulta.EnableViewState = true;
    }

    #region DataSources
    protected void EsDSPosicaoGeral_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        if (!Page.IsPostBack)
        {
            gridConsulta.Visible = false;
            this.btnCustomFields.Visible = false;
            e.Collection = new PosicaoBolsaCollection();
        }
        else
        {

            bool historico = false;

            //Checa se busca da posição atual ou histórica
            if (textData.Text != "")
            {
                DateTime data = Convert.ToDateTime(textData.Text);
                if (btnEditCodigoCarteira.Text == "")
                {
                    //se a data for preenchida e a carteira NAO escolhida pega TUDO da PosicaoCotistaHistorico
                    historico = true;
                }
                else
                if (btnEditCodigoCarteira.Text != "")
                {
                    //se a data for preenchida e a carteira escolhida, faz o teste baseado na data dia
                    int idCliente = Convert.ToInt32(btnEditCodigoCarteira.Text);
                    Cliente cliente = new Cliente();
                    List<esQueryItem> campos = new List<esQueryItem>();
                    campos.Add(cliente.Query.DataDia);
                    cliente.LoadByPrimaryKey(idCliente);
                    DateTime dataDia = cliente.DataDia.Value;
                    if (DateTime.Compare(data, dataDia) < 0)
                    {
                        historico = true;
                    }
                }
            }
            else
            {
                //se a data NAO for preenchida, pega TUDO da PosicaoCotista
                historico = false;
            }
            //

            Usuario usuario = new Usuario();
            usuario.BuscaUsuario(Context.User.Identity.Name);
            int idUsuario = usuario.IdUsuario.Value;
            
            #region Historico
            if (historico) {
                PosicaoCotistaHistoricoQuery posicaoCotistaHistoricoQuery = new PosicaoCotistaHistoricoQuery("P");
                PermissaoCotistaQuery permissaoCotistaQuery = new PermissaoCotistaQuery("R");
                CotistaQuery cotistaQuery = new CotistaQuery("S");
                CarteiraQuery carteiraQuery = new CarteiraQuery("T");
                //ClienteInterface clienteInterface = new ClienteInterface("CI");
                //
                posicaoCotistaHistoricoQuery.InnerJoin(cotistaQuery).On(posicaoCotistaHistoricoQuery.IdCotista == cotistaQuery.IdCotista);
                posicaoCotistaHistoricoQuery.InnerJoin(permissaoCotistaQuery).On(permissaoCotistaQuery.IdCotista == posicaoCotistaHistoricoQuery.IdCotista);                                
                posicaoCotistaHistoricoQuery.InnerJoin(carteiraQuery).On(posicaoCotistaHistoricoQuery.IdCarteira == carteiraQuery.IdCarteira);                

                posicaoCotistaHistoricoQuery.Select(posicaoCotistaHistoricoQuery,                    
                    //(cotistaQuery.Apelido + " (" + cotistaQuery.CodigoInterface + " )").As("Apelido"),
                    cotistaQuery, 
                    carteiraQuery.IdCarteira, carteiraQuery.Apelido.As("Carteira"), 
                    (posicaoCotistaHistoricoQuery.ValorIOF + posicaoCotistaHistoricoQuery.ValorIR).As("Tributos")
                    );

                posicaoCotistaHistoricoQuery.Where(permissaoCotistaQuery.IdUsuario.Equal(idUsuario));
                
                if (textData.Text != "") {
                    posicaoCotistaHistoricoQuery.Where(posicaoCotistaHistoricoQuery.DataHistorico.Equal(Convert.ToDateTime(textData.Text)));
                }
                posicaoCotistaHistoricoQuery.Where(posicaoCotistaHistoricoQuery.Quantidade.NotEqual(0));
                
                if (btnEditCodigoCarteira.Text != "") {
                    posicaoCotistaHistoricoQuery.Where(posicaoCotistaHistoricoQuery.IdCarteira.Equal(Convert.ToInt32(btnEditCodigoCarteira.Text)));
                }

                if (btnEditCodigoCotista.Text != "") {
                    posicaoCotistaHistoricoQuery.Where(posicaoCotistaHistoricoQuery.IdCotista.Equal(Convert.ToInt32(btnEditCodigoCotista.Text)));
                }

                posicaoCotistaHistoricoQuery.OrderBy(posicaoCotistaHistoricoQuery.IdCotista.Ascending,
                                                     posicaoCotistaHistoricoQuery.DataAplicacao.Descending);

                PosicaoCotistaHistoricoCollection posicaoCotistaHistoricoCollection = new PosicaoCotistaHistoricoCollection();
                posicaoCotistaHistoricoCollection.Load(posicaoCotistaHistoricoQuery);

                for (int i = 0; i < posicaoCotistaHistoricoCollection.Count; i++) {
                    string codigoInterfaceCotista = Convert.ToString(posicaoCotistaHistoricoCollection[i].GetColumn(CotistaMetadata.ColumnNames.CodigoInterface));
                    string apelido = Convert.ToString(posicaoCotistaHistoricoCollection[i].GetColumn(CotistaMetadata.ColumnNames.Apelido));
                    int idCarteira = Convert.ToInt32(posicaoCotistaHistoricoCollection[i].GetColumn(CarteiraMetadata.ColumnNames.IdCarteira));
                    string apelidoCarteira = Convert.ToString(posicaoCotistaHistoricoCollection[i].GetColumn("Carteira"));

                    if(!String.IsNullOrEmpty(codigoInterfaceCotista)) {
                        apelido += " (" + codigoInterfaceCotista + " )";

                        posicaoCotistaHistoricoCollection[i].SetColumn("Apelido", apelido);
                    }

                    ClienteInterface clienteInterface = new ClienteInterface();
                    List<esQueryItem> campos = new List<esQueryItem>();
                    campos.Add(clienteInterface.Query.CodigoYMF);
                    clienteInterface.LoadByPrimaryKey(campos, idCarteira);

                    string codigoInterfaceCarteira = "";
                    if (!String.IsNullOrEmpty(clienteInterface.CodigoYMF)) {
                        codigoInterfaceCarteira = clienteInterface.CodigoYMF.Trim();
                        apelidoCarteira += " (" + codigoInterfaceCarteira + ") ";

                        posicaoCotistaHistoricoCollection[i].SetColumn("Carteira", apelidoCarteira);
                    }                                                                
                }

                e.Collection = posicaoCotistaHistoricoCollection;
            }
            #endregion

            #region Não Historico
            else {

                PosicaoCotistaQuery posicaoCotistaQuery = new PosicaoCotistaQuery("P");
                PermissaoCotistaQuery permissaoCotistaQuery = new PermissaoCotistaQuery("R");
                CotistaQuery cotistaQuery = new CotistaQuery("S");
                CarteiraQuery carteiraQuery = new CarteiraQuery("T");

                posicaoCotistaQuery.InnerJoin(cotistaQuery).On(posicaoCotistaQuery.IdCotista == cotistaQuery.IdCotista);
                posicaoCotistaQuery.InnerJoin(permissaoCotistaQuery).On(permissaoCotistaQuery.IdCotista == posicaoCotistaQuery.IdCotista);                
                posicaoCotistaQuery.InnerJoin(carteiraQuery).On(posicaoCotistaQuery.IdCarteira == carteiraQuery.IdCarteira);

                posicaoCotistaQuery.Select(posicaoCotistaQuery,
                    //(cotistaQuery.Apelido + " (" + cotistaQuery.CodigoInterface + " )").As("Apelido"),
                    cotistaQuery,
                    carteiraQuery.IdCarteira, carteiraQuery.Apelido.As("Carteira"), 
                    (posicaoCotistaQuery.ValorIOF + posicaoCotistaQuery.ValorIR).As("Tributos")
                    );

                posicaoCotistaQuery.Where(permissaoCotistaQuery.IdUsuario.Equal(idUsuario));
                posicaoCotistaQuery.Where(posicaoCotistaQuery.Quantidade.NotEqual(0));

                if (btnEditCodigoCarteira.Text != "") {
                    posicaoCotistaQuery.Where(posicaoCotistaQuery.IdCarteira.Equal(Convert.ToInt32(btnEditCodigoCarteira.Text)));
                }

                if (btnEditCodigoCotista.Text != "") {
                    posicaoCotistaQuery.Where(posicaoCotistaQuery.IdCotista.Equal(Convert.ToInt32(btnEditCodigoCotista.Text)));
                }
                
                posicaoCotistaQuery.OrderBy(posicaoCotistaQuery.IdCotista.Ascending,
                                            posicaoCotistaQuery.DataAplicacao.Descending);

                PosicaoCotistaCollection posicaoCotistaCollection = new PosicaoCotistaCollection();
                posicaoCotistaCollection.Load(posicaoCotistaQuery);

                for (int i = 0; i < posicaoCotistaCollection.Count; i++) {
                    string codigoInterfaceCotista = Convert.ToString(posicaoCotistaCollection[i].GetColumn(CotistaMetadata.ColumnNames.CodigoInterface));
                    string apelido = Convert.ToString(posicaoCotistaCollection[i].GetColumn(CotistaMetadata.ColumnNames.Apelido));
                    int idCarteira = Convert.ToInt32(posicaoCotistaCollection[i].GetColumn(CarteiraMetadata.ColumnNames.IdCarteira));
                    string apelidoCarteira = Convert.ToString(posicaoCotistaCollection[i].GetColumn("Carteira"));

                    if (!String.IsNullOrEmpty(codigoInterfaceCotista)) {
                        apelido += " (" + codigoInterfaceCotista + " )";

                        posicaoCotistaCollection[i].SetColumn("Apelido", apelido);
                    }

                    ClienteInterface clienteInterface = new ClienteInterface();
                    List<esQueryItem> campos = new List<esQueryItem>();
                    campos.Add(clienteInterface.Query.CodigoYMF);
                    clienteInterface.LoadByPrimaryKey(campos, idCarteira);

                    string codigoInterfaceCarteira = "";
                    if (!String.IsNullOrEmpty(clienteInterface.CodigoYMF)) {
                        codigoInterfaceCarteira = clienteInterface.CodigoYMF.Trim();
                        apelidoCarteira += " (" + codigoInterfaceCarteira + ") ";

                        posicaoCotistaCollection[i].SetColumn("Carteira", apelidoCarteira);
                    }
                }

                e.Collection = posicaoCotistaCollection;
            }
            #endregion

            gridConsulta.Visible = true;

            // Visibilidade do botão Mais Campos
            this.btnCustomFields.Visible = e.Collection.Count != 0;
        }

    }
    
    protected void EsDSCarteira_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        CarteiraCollection carteiraCollection = new CarteiraCollection();
        carteiraCollection.BuscaCarteirasComAcessoTodas(HttpContext.Current.User.Identity.Name);

        e.Collection = carteiraCollection;
    }

    protected void EsDSCotista_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        CotistaCollection cotistaCollection = new CotistaCollection();
        cotistaCollection.BuscaCotistasComAcesso(HttpContext.Current.User.Identity.Name);

        e.Collection = cotistaCollection;
    }
    #endregion

    /// <summary>
    /// 
    /// </summary>
    private void TrataTravamentoCampos()
    {
        Usuario usuario = new Usuario();
        usuario.BuscaUsuario(HttpContext.Current.User.Identity.Name);
        int tipoTrava = usuario.TipoTrava.Value;

        if (tipoTrava == (int)TipoTravaUsuario.TravaCliente)
        {
            //Travamento do campo de carteira
            btnEditCodigoCarteira.Enabled = false;

            PermissaoCliente permissaoCliente = new PermissaoCliente();
            int? idCliente = permissaoCliente.RetornaClienteAssociado(HttpContext.Current.User.Identity.Name);

            if (idCliente.HasValue)
            {
                Cliente cliente = new Cliente();

                List<esQueryItem> campos = new List<esQueryItem>();
                campos.Add(cliente.Query.Apelido);
                cliente.LoadByPrimaryKey(campos, idCliente.Value);
                string apelido = cliente.Apelido;

                btnEditCodigoCarteira.Text = Convert.ToString(idCliente.Value);
                textNomeCarteira.Text = apelido;
            }
        }

        if (tipoTrava == (int)TipoTravaUsuario.TravaCotista)
        {
            //Travamento do campo de cotista
            btnEditCodigoCotista.Enabled = false;

            PermissaoCotista permissaoCotista = new PermissaoCotista();
            int? idCotista = permissaoCotista.RetornaCotistaAssociado(HttpContext.Current.User.Identity.Name);

            if (idCotista.HasValue)
            {
                Cotista cotista = new Cotista();

                List<esQueryItem> campos = new List<esQueryItem>();
                campos.Add(cotista.Query.Apelido);
                cotista.LoadByPrimaryKey(campos, idCotista.Value);
                string apelido = cotista.Apelido;

                btnEditCodigoCotista.Text = Convert.ToString(idCotista.Value);
                textNomeCotista.Text = apelido;
            }
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void ASPxCallback1_Callback(object source, DevExpress.Web.CallbackEventArgs e)
    {
        string nome = "";
        if (!String.IsNullOrEmpty(e.Parameter) && e.Parameter != "null")
        {
            int idCarteira = Convert.ToInt32(e.Parameter);
            Carteira carteira = new Carteira();
            carteira.LoadByPrimaryKey(idCarteira);

            if (carteira.str.Apelido != "")
            {
                if (carteira.IsAtivo)
                {
                    //O permissionamento da carteira é dado direto no cliente vinculado à carteira
                    PermissaoCliente permissaoCliente = new PermissaoCliente();
                    if (permissaoCliente.RetornaAcessoCliente(idCarteira, HttpContext.Current.User.Identity.Name))
                    {
                        nome = carteira.str.Apelido;
                    }
                    else
                    {
                        nome = "no_access";
                    }
                }
                else
                {
                    nome = "no_active";
                }
            }
        }
        e.Result = nome;
    }


    protected void ASPxCallback2_Callback(object source, DevExpress.Web.CallbackEventArgs e)
    {
        string nome = "";
        if (!String.IsNullOrEmpty(e.Parameter) && e.Parameter != "null")
        {
            int idCotista = Convert.ToInt32(e.Parameter);
            Cotista cotista = new Cotista();
            cotista.LoadByPrimaryKey(idCotista);

            if (cotista.str.Apelido != "")
            {
                if (cotista.IsAtivo)
                {
                    PermissaoCotista permissaoCotista = new PermissaoCotista();
                    nome = permissaoCotista.RetornaAcessoCotista(idCotista, HttpContext.Current.User.Identity.Name)
                            ? cotista.str.Apelido : "no_access";
                }
                else
                {
                    nome = "no_active";
                }
            }
        }
        e.Result = nome;
    }

    #region btnRun_Click, btnPDF_Click, btnExcel_Click



    new protected void btnRun_Click(object sender, EventArgs e)
    {
        base.btnRun_Click(sender, e); // DataBind do Grid
    }

    new protected void btnPDF_Click(object sender, EventArgs e)
    {
        this.SelecionaRelatorio(VisaoRelatorio.visaoPDF);
    }

    new protected void btnExcel_Click(object sender, EventArgs e)
    {
        this.SelecionaRelatorio(VisaoRelatorio.visaoExcel);
    }

    /// <summary>
    /// Seleciona o Relatorio a Ser Executado
    /// </summary>
    /// <param name="visao">PDF, Excel ou Report</param>
    private void SelecionaRelatorio(string visao)
    {
        Response.BufferOutput = true;
        //
        // Salva os Parametros na Session
        Session["gridExportPosicaoCotistaPorFundo"] = gridExport;
        Response.Redirect("~/Consultas/PosicaoCotistaPorFundoExporta.aspx?Visao=" + visao);
    }

    #endregion
}
