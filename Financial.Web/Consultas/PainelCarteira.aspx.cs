﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Text;
using System.Drawing;
using System.Collections.Generic;

using Financial.Fundo;
using Financial.Security;
using Financial.ContaCorrente;
using Financial.Web.Util;
using Financial.RendaFixa;
using Financial.Investidor;
using Financial.Swap;
using Financial.Bolsa;
using Financial.BMF;
using Financial.Common;
using Financial.InvestidorCotista;
using Financial.Web.Common;

using DevExpress.Web;

using EntitySpaces.Interfaces;

using Atatika.Web.UI;

using Financial.Investidor.Enums;
using Financial.ContaCorrente.Enums;
using Financial.Security.Enums;
using Financial.Fundo.Enums;

using Financial.Fundo.Exceptions;
using System.Threading;

public partial class Consultas_PainelCarteira : ConsultaBasePage {
    new protected void Page_Load(object sender, EventArgs e) {
        btnEditCodigo.Focus();
        ConfiguraLabels();

        base.Page_Load(sender, e);

        TrataTravamentoCampos();
    }

    /// <summary>
    /// 
    /// </summary>
    private void ConfiguraLabels() {
        labelValorRentDia.Symbol.HorizontalAlignment = Symbol.SymbolHorizontalAlignment.Right;
        labelValorRentMes.Symbol.HorizontalAlignment = Symbol.SymbolHorizontalAlignment.Right;
        labelValorRentAno.Symbol.HorizontalAlignment = Symbol.SymbolHorizontalAlignment.Right;
        labelValorRent12.Symbol.HorizontalAlignment = Symbol.SymbolHorizontalAlignment.Right;

        labelValorRentDia.Symbol.SymbolSign = "%";
        labelValorRentMes.Symbol.SymbolSign = "%";
        labelValorRentAno.Symbol.SymbolSign = "%";
        labelValorRent12.Symbol.SymbolSign = "%";
    }

    #region Preenche datasources
    protected void EsDSCarteira_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e) {
        CarteiraCollection carteiraCollection = new CarteiraCollection();
        carteiraCollection.BuscaCarteirasComAcesso(HttpContext.Current.User.Identity.Name);

        e.Collection = carteiraCollection;
    }

    protected void EsDSLiquidacao_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e) {
        if (btnEditCodigo.Text != "" && textData.Text != "") {
            int idCliente = Convert.ToInt32(btnEditCodigo.Text);

            LiquidacaoCollection liquidacaoCollection = new LiquidacaoCollection();
            liquidacaoCollection.BuscaLiquidacaoCompleta(idCliente, Convert.ToDateTime(textData.Text));

            e.Collection = liquidacaoCollection;
        }
        else {
            e.Collection = new LiquidacaoCollection();
        }
    }

    protected void EsDSPosicaoBolsa_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e) {
        if (btnEditCodigo.Text != "" && textData.Text != "") {
            int idCliente = Convert.ToInt32(btnEditCodigo.Text);

            Cliente cliente = new Cliente();
            List<esQueryItem> campos = new List<esQueryItem>();
            campos.Add(cliente.Query.DataDia);
            cliente.LoadByPrimaryKey(idCliente);
            DateTime dataDia = cliente.DataDia.Value;

            //Se dataDia > data de referência, busca no historico, senão busca na posicao atual
            if (DateTime.Compare(dataDia, Convert.ToDateTime(textData.Text)) > 0) {
                PosicaoBolsaHistoricoCollection posicaoBolsaHistoricoCollection = new PosicaoBolsaHistoricoCollection();
                posicaoBolsaHistoricoCollection.Query.Where(posicaoBolsaHistoricoCollection.Query.IdCliente.Equal(idCliente),
                                                            posicaoBolsaHistoricoCollection.Query.DataHistorico.Equal(Convert.ToDateTime(textData.Text)));
                posicaoBolsaHistoricoCollection.Query.OrderBy(posicaoBolsaHistoricoCollection.Query.CdAtivoBolsa.Ascending);
                posicaoBolsaHistoricoCollection.Query.Load();

                e.Collection = posicaoBolsaHistoricoCollection;
            }
            else {
                PosicaoBolsaCollection posicaoBolsaCollection = new PosicaoBolsaCollection();
                posicaoBolsaCollection.Query.Where(posicaoBolsaCollection.Query.IdCliente.Equal(idCliente));
                posicaoBolsaCollection.Query.OrderBy(posicaoBolsaCollection.Query.CdAtivoBolsa.Ascending);
                posicaoBolsaCollection.Query.Load();

                e.Collection = posicaoBolsaCollection;
            }
        }
        else {
            e.Collection = new PosicaoBolsaCollection();
        }
    }

    protected void EsDSPosicaoBMF_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e) {
        if (btnEditCodigo.Text != "" && textData.Text != "") {
            int idCliente = Convert.ToInt32(btnEditCodigo.Text);
            Cliente cliente = new Cliente();
            List<esQueryItem> campos = new List<esQueryItem>();
            campos.Add(cliente.Query.DataDia);
            cliente.LoadByPrimaryKey(idCliente);
            DateTime dataDia = cliente.DataDia.Value;

            //Se dataDia > data de referência, busca no historico, senão busca na posicao atual
            if (DateTime.Compare(dataDia, Convert.ToDateTime(textData.Text)) > 0) {
                PosicaoBMFHistoricoCollection posicaoBMFHistoricoCollection = new PosicaoBMFHistoricoCollection();
                posicaoBMFHistoricoCollection.BuscaPosicaoBMFHistoricoCompleta(idCliente, Convert.ToDateTime(textData.Text));

                e.Collection = posicaoBMFHistoricoCollection;
            }
            else {
                PosicaoBMFCollection posicaoBMFCollection = new PosicaoBMFCollection();
                posicaoBMFCollection.BuscaPosicaoBMF(idCliente);

                e.Collection = posicaoBMFCollection;
            }
        }
        else {
            e.Collection = new PosicaoBMFCollection();
        }
    }

    protected void EsDSPosicaoFundo_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e) {
        if (btnEditCodigo.Text != "" && textData.Text != "") {
            int idCliente = Convert.ToInt32(btnEditCodigo.Text);
            Cliente cliente = new Cliente();
            List<esQueryItem> campos = new List<esQueryItem>();
            campos.Add(cliente.Query.DataDia);
            cliente.LoadByPrimaryKey(campos, idCliente);
            DateTime dataDia = cliente.DataDia.Value;

            //Se dataDia > data de referência, busca no historico, senão busca na posicao atual
            if (DateTime.Compare(dataDia, Convert.ToDateTime(textData.Text)) > 0) {
                PosicaoFundoHistoricoQuery posicaoFundoHistoricoQuery = new PosicaoFundoHistoricoQuery("P");
                CarteiraQuery carteiraQuery = new CarteiraQuery("C");

                posicaoFundoHistoricoQuery.Select(posicaoFundoHistoricoQuery.Quantidade.Sum(),
                                            posicaoFundoHistoricoQuery.ValorAplicacao.Sum(),
                                            posicaoFundoHistoricoQuery.ValorBruto.Sum(),
                                            posicaoFundoHistoricoQuery.CotaDia.Avg(),
                                            carteiraQuery.Apelido.As("CarteiraApelido"));
                posicaoFundoHistoricoQuery.InnerJoin(carteiraQuery).On(posicaoFundoHistoricoQuery.IdCarteira == carteiraQuery.IdCarteira);

                posicaoFundoHistoricoQuery.Where(posicaoFundoHistoricoQuery.IdCliente.Equal(idCliente),
                                            posicaoFundoHistoricoQuery.Quantidade.NotEqual(0),
                                            posicaoFundoHistoricoQuery.DataHistorico.Equal(Convert.ToDateTime(textData.Text)));
                posicaoFundoHistoricoQuery.GroupBy(posicaoFundoHistoricoQuery.IdCarteira, carteiraQuery.Apelido);
                posicaoFundoHistoricoQuery.OrderBy(carteiraQuery.Apelido.Ascending);

                PosicaoFundoHistoricoCollection coll = new PosicaoFundoHistoricoCollection();
                coll.Load(posicaoFundoHistoricoQuery);

                // Assign the esDataSourcSelectEvenArgs Collection property
                e.Collection = coll;
            }
            else {
                PosicaoFundoQuery posicaoFundoQuery = new PosicaoFundoQuery("P");
                CarteiraQuery carteiraQuery = new CarteiraQuery("C");

                posicaoFundoQuery.Select(posicaoFundoQuery.Quantidade.Sum(), posicaoFundoQuery.ValorAplicacao.Sum(),
                                    posicaoFundoQuery.ValorBruto.Sum(), posicaoFundoQuery.CotaDia.Avg(),
                                    carteiraQuery.Apelido.As("CarteiraApelido"));
                posicaoFundoQuery.InnerJoin(carteiraQuery).On(posicaoFundoQuery.IdCarteira == carteiraQuery.IdCarteira);

                posicaoFundoQuery.Where(posicaoFundoQuery.IdCliente.Equal(idCliente),
                                        posicaoFundoQuery.Quantidade.NotEqual(0));
                posicaoFundoQuery.GroupBy(posicaoFundoQuery.IdCarteira, carteiraQuery.Apelido);
                posicaoFundoQuery.OrderBy(carteiraQuery.Apelido.Ascending);

                PosicaoFundoCollection coll = new PosicaoFundoCollection();
                coll.Load(posicaoFundoQuery);

                // Assign the esDataSourcSelectEvenArgs Collection property
                e.Collection = coll;
            }
        }
        else {
            e.Collection = new PosicaoBolsaCollection();
        }
    }

    protected void EsDSPosicaoRendaFixa_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e) {
        if (btnEditCodigo.Text != "" && textData.Text != "") {
            int idCliente = Convert.ToInt32(btnEditCodigo.Text);
            Cliente cliente = new Cliente();
            List<esQueryItem> campos = new List<esQueryItem>();
            campos.Add(cliente.Query.DataDia);
            cliente.LoadByPrimaryKey(idCliente);
            DateTime dataDia = cliente.DataDia.Value;

            //Se dataDia > data de referência, busca no historico, senão busca na posicao atual
            if (DateTime.Compare(dataDia, Convert.ToDateTime(textData.Text)) > 0) {
                PosicaoRendaFixaHistoricoQuery posicaoRendaFixaQuery = new PosicaoRendaFixaHistoricoQuery("P");
                TituloRendaFixaQuery tituloRendaFixaQuery = new TituloRendaFixaQuery("T");

                posicaoRendaFixaQuery.Select(posicaoRendaFixaQuery.Quantidade.Sum(), posicaoRendaFixaQuery.PUOperacao.Avg(),
                                    posicaoRendaFixaQuery.PUOperacao.Avg(), posicaoRendaFixaQuery.PUMercado.Avg(),
                                    posicaoRendaFixaQuery.ValorMercado.Sum(),
                                    posicaoRendaFixaQuery.DataVencimento, tituloRendaFixaQuery.Descricao);
                posicaoRendaFixaQuery.InnerJoin(tituloRendaFixaQuery).On(posicaoRendaFixaQuery.IdTitulo == tituloRendaFixaQuery.IdTitulo);

                posicaoRendaFixaQuery.Where(posicaoRendaFixaQuery.IdCliente.Equal(idCliente),
                                            posicaoRendaFixaQuery.Quantidade.NotEqual(0),
                                            posicaoRendaFixaQuery.DataHistorico.Equal(Convert.ToDateTime(textData.Text)));
                posicaoRendaFixaQuery.GroupBy(tituloRendaFixaQuery.Descricao, posicaoRendaFixaQuery.DataVencimento);
                posicaoRendaFixaQuery.OrderBy(posicaoRendaFixaQuery.DataVencimento.Ascending);

                PosicaoRendaFixaHistoricoCollection coll = new PosicaoRendaFixaHistoricoCollection();
                coll.Load(posicaoRendaFixaQuery);

                // Assign the esDataSourcSelectEvenArgs Collection property
                e.Collection = coll;
            }
            else {
                PosicaoRendaFixaQuery posicaoRendaFixaQuery = new PosicaoRendaFixaQuery("P");
                TituloRendaFixaQuery tituloRendaFixaQuery = new TituloRendaFixaQuery("T");

                posicaoRendaFixaQuery.Select(posicaoRendaFixaQuery.Quantidade.Sum(), posicaoRendaFixaQuery.PUOperacao.Avg(),
                                    posicaoRendaFixaQuery.PUOperacao.Avg(), posicaoRendaFixaQuery.PUMercado.Avg(),
                                    posicaoRendaFixaQuery.ValorMercado.Sum(),
                                    posicaoRendaFixaQuery.DataVencimento, tituloRendaFixaQuery.Descricao);
                posicaoRendaFixaQuery.InnerJoin(tituloRendaFixaQuery).On(posicaoRendaFixaQuery.IdTitulo == tituloRendaFixaQuery.IdTitulo);

                posicaoRendaFixaQuery.Where(posicaoRendaFixaQuery.IdCliente.Equal(idCliente),
                                            posicaoRendaFixaQuery.Quantidade.NotEqual(0));
                posicaoRendaFixaQuery.GroupBy(tituloRendaFixaQuery.Descricao, posicaoRendaFixaQuery.DataVencimento);
                posicaoRendaFixaQuery.OrderBy(posicaoRendaFixaQuery.DataVencimento.Ascending);

                PosicaoRendaFixaCollection coll = new PosicaoRendaFixaCollection();
                coll.Load(posicaoRendaFixaQuery);

                // Assign the esDataSourcSelectEvenArgs Collection property
                e.Collection = coll;
            }
        }
        else {
            e.Collection = new PosicaoRendaFixaCollection();
        }
    }

    protected void EsDSPosicaoSwap_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e) {
        if (btnEditCodigo.Text != "" && textData.Text != "") {
            int idCliente = Convert.ToInt32(btnEditCodigo.Text);
            Cliente cliente = new Cliente();
            List<esQueryItem> campos = new List<esQueryItem>();
            campos.Add(cliente.Query.DataDia);
            cliente.LoadByPrimaryKey(idCliente);
            DateTime dataDia = cliente.DataDia.Value;

            //Se dataDia > data de referência, busca no historico, senão busca na posicao atual
            if (DateTime.Compare(dataDia, Convert.ToDateTime(textData.Text)) > 0) {
                PosicaoSwapHistoricoQuery posicaoSwapQuery = new PosicaoSwapHistoricoQuery("P");
                IndiceQuery indiceQuery = new IndiceQuery("I");
                IndiceQuery indiceQuery2 = new IndiceQuery("I2");

                posicaoSwapQuery.Select(posicaoSwapQuery.DataEmissao, posicaoSwapQuery.DataVencimento, posicaoSwapQuery.ValorBase,
                                    posicaoSwapQuery.TipoPonta, posicaoSwapQuery.IdIndice, indiceQuery.Descricao.Substring(1, 8).As("DescricaoIndice"),
                                    posicaoSwapQuery.TaxaJuros, posicaoSwapQuery.Percentual,
                                    posicaoSwapQuery.TipoPontaContraParte, posicaoSwapQuery.IdIndiceContraParte, indiceQuery2.Descricao.Substring(1, 8).As("DescricaoIndiceContraParte"),
                                    posicaoSwapQuery.TaxaJurosContraParte, posicaoSwapQuery.PercentualContraParte,
                                    posicaoSwapQuery.ValorParte, posicaoSwapQuery.ValorContraParte, posicaoSwapQuery.Saldo);
                posicaoSwapQuery.LeftJoin(indiceQuery).On(posicaoSwapQuery.IdIndice == indiceQuery.IdIndice);
                posicaoSwapQuery.LeftJoin(indiceQuery2).On(posicaoSwapQuery.IdIndiceContraParte == indiceQuery2.IdIndice);

                posicaoSwapQuery.Where(posicaoSwapQuery.IdCliente.Equal(idCliente),
                                       posicaoSwapQuery.DataHistorico.Equal(Convert.ToDateTime(textData.Text)));

                PosicaoSwapHistoricoCollection coll = new PosicaoSwapHistoricoCollection();
                coll.Load(posicaoSwapQuery);

                // Assign the esDataSourcSelectEvenArgs Collection property
                e.Collection = coll;
            }
            else {
                PosicaoSwapQuery posicaoSwapQuery = new PosicaoSwapQuery("P");
                IndiceQuery indiceQuery = new IndiceQuery("I");
                IndiceQuery indiceQuery2 = new IndiceQuery("I2");

                posicaoSwapQuery.Select(posicaoSwapQuery.DataEmissao, posicaoSwapQuery.DataVencimento, posicaoSwapQuery.ValorBase,
                                    posicaoSwapQuery.TipoPonta, posicaoSwapQuery.IdIndice, indiceQuery.Descricao.Substring(1, 8).As("DescricaoIndice"),
                                    posicaoSwapQuery.TaxaJuros, posicaoSwapQuery.Percentual,
                                    posicaoSwapQuery.TipoPontaContraParte, posicaoSwapQuery.IdIndiceContraParte, indiceQuery2.Descricao.Substring(1, 8).As("DescricaoIndiceContraParte"),
                                    posicaoSwapQuery.TaxaJurosContraParte, posicaoSwapQuery.PercentualContraParte,
                                    posicaoSwapQuery.ValorParte, posicaoSwapQuery.ValorContraParte, posicaoSwapQuery.Saldo);
                posicaoSwapQuery.LeftJoin(indiceQuery).On(posicaoSwapQuery.IdIndice == indiceQuery.IdIndice);
                posicaoSwapQuery.LeftJoin(indiceQuery2).On(posicaoSwapQuery.IdIndiceContraParte == indiceQuery2.IdIndice);

                posicaoSwapQuery.Where(posicaoSwapQuery.IdCliente.Equal(idCliente));

                PosicaoSwapCollection coll = new PosicaoSwapCollection();
                coll.Load(posicaoSwapQuery);

                // Assign the esDataSourcSelectEvenArgs Collection property
                e.Collection = coll;
            }
        }
        else {
            e.Collection = new PosicaoSwapCollection();
        }
    }
    #endregion

    /// <summary>
    /// 
    /// </summary>
    private void TrataTravamentoCampos() {
        Usuario usuario = new Usuario();
        usuario.BuscaUsuario(HttpContext.Current.User.Identity.Name);
        int tipoTrava = usuario.TipoTrava.Value;

        if (tipoTrava == (int)TipoTravaUsuario.TravaCliente) {
            btnEditCodigo.Enabled = false;

            PermissaoCliente permissaoCliente = new PermissaoCliente();
            int? idCliente = permissaoCliente.RetornaClienteAssociado(HttpContext.Current.User.Identity.Name);

            if (idCliente.HasValue) {
                Cliente cliente = new Cliente();

                List<esQueryItem> campos = new List<esQueryItem>();
                campos.Add(cliente.Query.Apelido);
                cliente.LoadByPrimaryKey(campos, idCliente.Value);
                string apelido = cliente.Apelido;

                btnEditCodigo.Text = Convert.ToString(idCliente.Value);
                textNome.Text = apelido;
            }
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    new protected void gridCarteira_HtmlRowCreated(object sender, DevExpress.Web.ASPxGridViewTableRowEventArgs e) {
        e.Row.Attributes.Add("onmouseover", "backgroundColor = this.style.backgroundColor; this.style.backgroundColor='#E0E9EB';");
        e.Row.Attributes.Add("onmouseout", "this.style.backgroundColor=backgroundColor;");
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void ASPxCallback1_Callback(object source, DevExpress.Web.CallbackEventArgs e) {
        string nome = "";
        if (!String.IsNullOrEmpty(e.Parameter) && e.Parameter != "null") {
            int idCarteira = Convert.ToInt32(e.Parameter);
            Carteira carteira = new Carteira();
            carteira.LoadByPrimaryKey(idCarteira);

            if (carteira.str.Apelido != "") {
                if (carteira.IsAtivo) {
                    //O permissionamento da carteira é dado direto no cliente vinculado à carteira
                    PermissaoCliente permissaoCliente = new PermissaoCliente();
                    if (permissaoCliente.RetornaAcessoClienteComControle(idCarteira, HttpContext.Current.User.Identity.Name)) {
                        Cliente cliente = new Cliente();
                        cliente.LoadByPrimaryKey(idCarteira);

                        DateTime dataDia = cliente.DataDia.Value;
                        if (btnEditCodigo.Text != "" && textData.Text != "")
                            dataDia = Convert.ToDateTime(textData.Text);

                        nome = carteira.str.Apelido + "|" + dataDia.ToShortDateString() + "|" + Thread.CurrentThread.CurrentCulture.Name;
                    }
                    else {
                        nome = "no_access";
                    }
                }
                else {
                    nome = "no_active";
                }
            }
        }
        e.Result = nome;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    new protected void gridCarteira_CustomDataCallback(object sender, DevExpress.Web.ASPxGridViewCustomDataCallbackEventArgs e) {
        e.Result = gridCarteira.GetRowValues(Convert.ToInt32(e.Parameters), CarteiraMetadata.ColumnNames.IdCarteira);
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    new protected void btnRun_Click(object sender, EventArgs e) {
        string formatoValor = "n2";

        int idCliente = Convert.ToInt32(btnEditCodigo.Text);
        DateTime dataRelatorio = Convert.ToDateTime(textData.Text);

        #region Checa se deve buscar de posições históricas, ou posições atuais
        Cliente cliente = new Cliente();
        List<esQueryItem> campos = new List<esQueryItem>();
        campos.Add(cliente.Query.DataDia);
        campos.Add(cliente.Query.Status);
        DateTime dataDia = new DateTime();
        int status = 0;
        if (cliente.LoadByPrimaryKey(campos, idCliente)) {
            dataDia = cliente.DataDia.Value;
            status = cliente.Status.Value;
        }

        bool buscaHistorico = false;
        if (DateTime.Compare(dataDia, dataRelatorio) > 0) {
            buscaHistorico = true;
        }
        #endregion

        #region Preenche o status da carteira
        if (status != 0) {
            string statusDescricao = TraducaoEnumsInvestidor.EnumStatusCliente.TraduzEnum(status);
            labelStatus.Text = statusDescricao + " - " + dataDia.ToString("d");
        }
        #endregion

        #region Caixa Abert, Fech e Vencimentos Dia
        SaldoCaixa saldoCaixa = new SaldoCaixa();
        saldoCaixa.BuscaSaldoCaixa(idCliente, dataRelatorio);

        if (saldoCaixa.SaldoAbertura.HasValue) {
            labelValorCaixaAbertura.Text = saldoCaixa.SaldoAbertura.Value.ToString(formatoValor);
        }
        else {
            labelValorCaixaAbertura.Text = "";
        }

        if (saldoCaixa.SaldoFechamento.HasValue) {
            labelValorCaixaFechamento.Text = saldoCaixa.SaldoFechamento.Value.ToString(formatoValor);
        }
        else {
            labelValorCaixaFechamento.Text = "";
        }

        Liquidacao liquidacao = new Liquidacao();
        decimal valorLiquidacao = liquidacao.RetornaLiquidacaoNaData(idCliente, dataRelatorio);
        labelValorVencimentosDia.Text = valorLiquidacao.ToString(formatoValor);
        #endregion

        #region Bolsa, BMF
        decimal valorMercadoBolsa = 0;
        decimal valorMercadoBMF = 0;
        if (buscaHistorico) {
            PosicaoBolsaHistorico posicaoBolsaHistorico = new PosicaoBolsaHistorico();
            PosicaoBMFHistorico posicaoBMFHistorico = new PosicaoBMFHistorico();
            valorMercadoBolsa = posicaoBolsaHistorico.RetornaValorMercado(idCliente, dataRelatorio);
            valorMercadoBMF = posicaoBMFHistorico.RetornaValorMercado(idCliente, dataRelatorio);
        }
        else {
            PosicaoBolsa posicaoBolsa = new PosicaoBolsa();
            PosicaoBMF posicaoBMF = new PosicaoBMF();
            valorMercadoBolsa = posicaoBolsa.RetornaValorMercado(idCliente);
            valorMercadoBMF = posicaoBMF.RetornaValorMercado(idCliente);
        }

        labelValorBolsa.Text = valorMercadoBolsa.ToString(formatoValor);
        labelValorBMF.Text = valorMercadoBMF.ToString(formatoValor);
        #endregion

        #region Renda Fixa, Swap
        decimal valorMercadoRendaFixa = 0;
        decimal valorMercadoSwap = 0;
        if (buscaHistorico) {
            PosicaoRendaFixaHistorico posicaoRendaFixaHistorico = new PosicaoRendaFixaHistorico();
            PosicaoSwapHistorico posicaoSwapHistorico = new PosicaoSwapHistorico();
            valorMercadoRendaFixa = posicaoRendaFixaHistorico.RetornaValorMercado(idCliente, dataRelatorio);
            valorMercadoSwap = posicaoSwapHistorico.RetornaValorSaldoTotal(idCliente, dataRelatorio);
        }
        else {
            PosicaoRendaFixa posicaoRendaFixa = new PosicaoRendaFixa();
            PosicaoSwap posicaoSwap = new PosicaoSwap();
            valorMercadoRendaFixa = posicaoRendaFixa.RetornaValorMercado(idCliente);
            valorMercadoSwap = posicaoSwap.RetornaValorSaldoTotal(idCliente);
        }

        decimal valorTotal = valorMercadoRendaFixa + valorMercadoSwap;

        labelValorRendaFixa.Text = valorTotal.ToString(formatoValor);
        #endregion

        #region Cotas de Investimento
        decimal valorMercadoFundo = 0;
        if (buscaHistorico) {
            PosicaoFundoHistorico posicaoFundoHistorico = new PosicaoFundoHistorico();
            valorMercadoFundo = posicaoFundoHistorico.RetornaValorBruto(idCliente, dataRelatorio);
        }
        else {
            PosicaoFundo posicaoFundo = new PosicaoFundo();
            valorMercadoFundo = posicaoFundo.RetornaValorBruto(idCliente);
        }

        labelValorCotasInvestimento.Text = valorMercadoFundo.ToString(formatoValor);
        #endregion

        #region Taxa de Performance
        decimal valorPerformance = 0;
        if (buscaHistorico) {
            CalculoPerformanceHistorico calculoPerformanceHistorico = new CalculoPerformanceHistorico();
            valorPerformance = calculoPerformanceHistorico.RetornaValorPerformance(idCliente, dataRelatorio);
        }
        else {
            CalculoPerformance calculoPerformance = new CalculoPerformance();
            valorPerformance = calculoPerformance.RetornaValorPerformance(idCliente);
        }
        valorPerformance = valorPerformance * -1;
        labelValorPerformance.Text = valorPerformance.ToString(formatoValor);
        #endregion

        #region Taxas de Administração, Gestão
        decimal valorAdministracao = 0;
        decimal valorGestao = 0;
        if (buscaHistorico) {
            CalculoAdministracaoHistorico calculoAdministracaoHistorico = new CalculoAdministracaoHistorico();
            valorAdministracao = calculoAdministracaoHistorico.RetornaValorAdministracao(idCliente, dataRelatorio,
                                                                           (int)TipoCadastroAdministracao.TaxaAdministracao);
            valorGestao = calculoAdministracaoHistorico.RetornaValorAdministracao(idCliente, dataRelatorio,
                                                                           (int)TipoCadastroAdministracao.TaxaGestao);
        }
        else {
            CalculoAdministracao calculoAdministracao = new CalculoAdministracao();
            valorAdministracao = calculoAdministracao.RetornaValorAdministracao(idCliente,
                                                                         (int)TipoCadastroAdministracao.TaxaAdministracao);
            valorGestao = calculoAdministracao.RetornaValorAdministracao(idCliente,
                                                                       (int)TipoCadastroAdministracao.TaxaGestao);
        }
        valorAdministracao = valorAdministracao * -1;
        valorGestao = valorGestao * -1;
        labelValorAdm.Text = valorAdministracao.ToString(formatoValor);
        labelValorGestao.Text = valorGestao.ToString(formatoValor);
        #endregion

        #region Contas a Receber/Pagar
        liquidacao = new Liquidacao();
        List<int> listaOrigemLancamentoLiquidacaoBolsa = OrigemLancamentoLiquidacao.Bolsa.Values();
        List<int> listaOrigemLancamentoLiquidacaoBMF = OrigemLancamentoLiquidacao.BMF.Values();
        List<int> listaOrigemLancamentoLiquidacaoFundo = OrigemLancamentoLiquidacao.Fundo.Values();
        List<int> listaOrigemLancamentoLiquidacaoIR = OrigemLancamentoLiquidacao.IR.Values();
        List<int> listaOrigemLancamentoLiquidacaoProvisao = OrigemLancamentoLiquidacao.Provisao.Values();
        List<int> listaOrigemLancamentoLiquidacaoRendaFixa = OrigemLancamentoLiquidacao.RendaFixa.Values();
        List<int> listaOrigemLancamentoLiquidacaoSwap = OrigemLancamentoLiquidacao.Swap.Values();
        List<int> listaOrigemLancamentoLiquidacaoNaoBolsa = new List<int>();
        listaOrigemLancamentoLiquidacaoNaoBolsa = listaOrigemLancamentoLiquidacaoFundo;
        listaOrigemLancamentoLiquidacaoNaoBolsa.AddRange(listaOrigemLancamentoLiquidacaoIR);
        listaOrigemLancamentoLiquidacaoNaoBolsa.AddRange(listaOrigemLancamentoLiquidacaoProvisao);
        listaOrigemLancamentoLiquidacaoNaoBolsa.AddRange(listaOrigemLancamentoLiquidacaoRendaFixa);
        listaOrigemLancamentoLiquidacaoNaoBolsa.AddRange(listaOrigemLancamentoLiquidacaoSwap);
        listaOrigemLancamentoLiquidacaoNaoBolsa.AddRange(listaOrigemLancamentoLiquidacaoSwap);
        listaOrigemLancamentoLiquidacaoNaoBolsa.Add(OrigemLancamentoLiquidacao.Outros);

        decimal valorReceberBolsa = liquidacao.RetornaValorVencer(idCliente, dataRelatorio, listaOrigemLancamentoLiquidacaoBolsa,
                                                SinalValorLiquidacao.Entrada);
        decimal valorReceberBMF = liquidacao.RetornaValorVencer(idCliente, dataRelatorio, listaOrigemLancamentoLiquidacaoBMF,
                                                SinalValorLiquidacao.Entrada);
        decimal valorReceberOutros = liquidacao.RetornaValorVencer(idCliente, dataRelatorio, listaOrigemLancamentoLiquidacaoNaoBolsa,
                                                SinalValorLiquidacao.Entrada);

        decimal valorPagarBolsa = liquidacao.RetornaValorVencer(idCliente, dataRelatorio, listaOrigemLancamentoLiquidacaoBolsa,
                                                SinalValorLiquidacao.Retirada);
        decimal valorPagarBMF = liquidacao.RetornaValorVencer(idCliente, dataRelatorio, listaOrigemLancamentoLiquidacaoBMF,
                                                SinalValorLiquidacao.Retirada);
        decimal valorPagarOutros = liquidacao.RetornaValorVencer(idCliente, dataRelatorio, listaOrigemLancamentoLiquidacaoNaoBolsa,
                                                SinalValorLiquidacao.Retirada);

        labelValorReceberBolsa.Text = valorReceberBolsa.ToString(formatoValor);
        labelValorReceberBMF.Text = valorReceberBMF.ToString(formatoValor);
        labelValorReceberOutros.Text = valorReceberOutros.ToString(formatoValor);

        labelValorPagarBolsa.Text = valorPagarBolsa.ToString(formatoValor);
        labelValorPagarBMF.Text = valorPagarBMF.ToString(formatoValor);
        labelValorPagarOutros.Text = valorPagarOutros.ToString(formatoValor);
        #endregion

        #region Aplic, Resg
        OperacaoCotista operacaoCotista = new OperacaoCotista();
        decimal valorAplicacoes = operacaoCotista.RetornaSumValorBrutoAplicacao(idCliente, dataRelatorio, dataRelatorio);
        decimal valorResgates = operacaoCotista.RetornaSumValorBrutoResgate(idCliente, dataRelatorio, dataRelatorio);
        decimal valorNet = valorAplicacoes - valorResgates;

        labelValorAplicacoesDia.Text = valorAplicacoes.ToString(formatoValor);
        labelValorResgatesDia.Text = valorResgates.ToString(formatoValor);
        labelValorNet.Text = valorNet.ToString(formatoValor);
        #endregion

        #region Cota, PL, Qtde Cotas
        //Patrimonio
        HistoricoCota historicoCota = new HistoricoCota();
        historicoCota.BuscaValorPatrimonioDia(idCliente, dataRelatorio);

        if (historicoCota.PatrimonioBruto.HasValue) {
            labelValorPatBruto.Text = historicoCota.PatrimonioBruto.Value.ToString(formatoValor);
        }
        else {
            labelValorPatBruto.Text = "";
        }
        if (historicoCota.PLFechamento.HasValue) {
            labelValorPatLiquido.Text = historicoCota.PLFechamento.Value.ToString(formatoValor);
        }
        else {
            labelValorPatLiquido.Text = "";
        }

        //Cota
        historicoCota = new HistoricoCota();
        historicoCota.BuscaValorCotaDia(idCliente, dataRelatorio);

        if (historicoCota.CotaBruta.HasValue) {
            labelValorCotaBruta.Text = historicoCota.CotaBruta.Value.ToString("n8");
        }
        else {
            labelValorCotaBruta.Text = "";
        }

        if (historicoCota.CotaFechamento.HasValue) {
            labelValorCotaLiquida.Text = historicoCota.CotaFechamento.Value.ToString("n8");
        }
        else {
            labelValorCotaLiquida.Text = "";
        }

        historicoCota = new HistoricoCota();
        decimal quantidadeCotas = historicoCota.RetornaQuantidadeCotas(idCliente, dataRelatorio);

        labelValorQtdeCotas.Text = quantidadeCotas.ToString("n8");
        #endregion

        #region Rentabilidades
        CalculoMedida calculoMedida = new CalculoMedida(idCliente);

        decimal? retornoDia = null;
        decimal? retornoMes = null;
        decimal? retornoAno = null;
        decimal? retorno12Meses = null;

        try {
            retornoDia = calculoMedida.CalculaRetornoDia(dataRelatorio);
        }
        catch (HistoricoCotaNaoCadastradoException) {
            labelValorRentDia.Text = "-";
        }

        try {
            retornoMes = calculoMedida.CalculaRetornoMes(dataRelatorio);
        }
        catch (HistoricoCotaNaoCadastradoException) {
            labelValorRentMes.Text = "-";
        }

        try {
            retornoAno = calculoMedida.CalculaRetornoAno(dataRelatorio);
        }
        catch (HistoricoCotaNaoCadastradoException) {
            labelValorRentAno.Text = "-";
        }

        try {
            retorno12Meses = calculoMedida.CalculaRetornoPeriodoMes(dataRelatorio, 12);
        }
        catch (HistoricoCotaNaoCadastradoException) {
            labelValorRent12.Text = "-";
        }

        if (retornoDia.HasValue) {
            decimal retorno = retornoDia.Value;
            labelValorRentDia.Text = retorno.ToString("n4");
        }

        if (retornoMes.HasValue) {
            decimal retorno = retornoMes.Value;
            labelValorRentMes.Text = retorno.ToString("n4");
        }

        if (retornoAno.HasValue) {
            decimal retorno = retornoAno.Value;
            labelValorRentAno.Text = retorno.ToString("n4");
        }

        if (retorno12Meses.HasValue) {
            decimal retorno = retorno12Meses.Value;
            labelValorRent12.Text = retorno.ToString("n4");
        }
        #endregion

        gridPosicaoBolsa.DataBind();
        gridPosicaoFundo.DataBind();
        gridPosicaoBMF.DataBind();
        gridPosicaoRendaFixa.DataBind();
        gridLiquidacao.DataBind();
        gridPosicaoSwap.DataBind();

    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridLiquidacao_HtmlDataCellPrepared(object sender, ASPxGridViewTableDataCellEventArgs e) {
        if (e.DataColumn.FieldName == "Valor") {
            decimal value = (decimal)e.GetValue("Valor");
            if (value < 0) {
                e.Cell.ForeColor = Color.Red;
            }
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridPosicaoBolsa_HtmlDataCellPrepared(object sender, ASPxGridViewTableDataCellEventArgs e) {
        if (e.DataColumn.FieldName == "Quantidade") {
            decimal value = (decimal)e.GetValue("Quantidade");
            if (value < 0)
                e.Cell.ForeColor = Color.Red;
        }
        else if (e.DataColumn.FieldName == "ValorMercado") {
            decimal value = (decimal)e.GetValue("ValorMercado");
            if (value < 0) {
                e.Cell.ForeColor = Color.Red;
            }
        }
        else if (e.DataColumn.FieldName == "ResultadoRealizar") {
            decimal value = (decimal)e.GetValue("ResultadoRealizar");
            if (value < 0) {
                e.Cell.ForeColor = Color.Red;
            }
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridPosicaoBMF_HtmlDataCellPrepared(object sender, ASPxGridViewTableDataCellEventArgs e) {
        if (e.DataColumn.FieldName == "Quantidade") {
            decimal value = (int)e.GetValue("Quantidade");
            if (value < 0)
                e.Cell.ForeColor = Color.Red;
        }
        else if (e.DataColumn.FieldName == "ValorMercado") {
            decimal value = (decimal)e.GetValue("ValorMercado");
            if (value < 0) {
                e.Cell.ForeColor = Color.Red;
            }
        }
        else if (e.DataColumn.FieldName == "ResultadoRealizar") {
            decimal value = (decimal)e.GetValue("ResultadoRealizar");
            if (value < 0) {
                e.Cell.ForeColor = Color.Red;
            }
        }

    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridPosicaoFundo_HtmlDataCellPrepared(object sender, ASPxGridViewTableDataCellEventArgs e) {
        //if (e.DataColumn.FieldName == "Quantidade")
        //{
        //    decimal value = (decimal)e.GetValue("ResultadoRealizar");
        //    if (value < 0)
        //        e.Cell.ForeColor = Color.Red;
        //}
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridPosicaoRendaFixa_HtmlDataCellPrepared(object sender, ASPxGridViewTableDataCellEventArgs e) {
        //if (e.DataColumn.FieldName == "Quantidade")
        //{
        //    decimal value = (decimal)e.GetValue("ResultadoRealizar");
        //    if (value < 0)
        //        e.Cell.ForeColor = Color.Red;
        //}
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridPosicaoSwap_HtmlDataCellPrepared(object sender, ASPxGridViewTableDataCellEventArgs e) {
        if (e.DataColumn.FieldName == "ValorParte") {
            decimal value = (decimal)e.GetValue("ValorParte");
            if (value < 0) {
                e.Cell.ForeColor = Color.Red;
            }
        }
        else if (e.DataColumn.FieldName == "ValorContraParte") {
            decimal value = (decimal)e.GetValue("ValorContraParte");
            if (value < 0) {
                e.Cell.ForeColor = Color.Red;
            }
        }
        else if (e.DataColumn.FieldName == "Saldo") {
            decimal value = (decimal)e.GetValue("Saldo");
            if (value < 0) {
                e.Cell.ForeColor = Color.Red;
            }

        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridPosicaoSwap_HtmlRowPrepared(object sender, DevExpress.Web.ASPxGridViewTableRowEventArgs e) {
        if (e.RowType != DevExpress.Web.GridViewRowType.Footer) return;

        //decimal value = (decimal)e.GetValue("Saldo");
        //if (value > 1000)
        //    e.Row.ForeColor = Color.Red;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridPosicaoBolsa_HtmlFooterCellPrepared(object sender, ASPxGridViewTableFooterCellEventArgs e) {
        GridViewDataColumn column = e.Column as GridViewDataColumn;
        if (column == null) return;
        ASPxSummaryItem item = gridPosicaoBolsa.TotalSummary[column.FieldName];
        if (item == null) return;
        decimal summaryValue = Convert.ToDecimal(e.GetSummaryValue(item));
        if (summaryValue < 0)
            e.Cell.ForeColor = Color.Red;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridPosicaoBMF_HtmlFooterCellPrepared(object sender, ASPxGridViewTableFooterCellEventArgs e) {
        GridViewDataColumn column = e.Column as GridViewDataColumn;
        if (column == null) return;
        ASPxSummaryItem item = gridPosicaoBMF.TotalSummary[column.FieldName];
        if (item == null) return;
        decimal summaryValue = Convert.ToDecimal(e.GetSummaryValue(item));
        if (summaryValue < 0)
            e.Cell.ForeColor = Color.Red;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridPosicaoFundo_HtmlFooterCellPrepared(object sender, ASPxGridViewTableFooterCellEventArgs e) {
        GridViewDataColumn column = e.Column as GridViewDataColumn;
        if (column == null) return;
        ASPxSummaryItem item = gridPosicaoFundo.TotalSummary[column.FieldName];
        if (item == null) return;
        decimal summaryValue = Convert.ToDecimal(e.GetSummaryValue(item));
        if (summaryValue < 0)
            e.Cell.ForeColor = Color.Red;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridPosicaoRendaFixa_HtmlFooterCellPrepared(object sender, ASPxGridViewTableFooterCellEventArgs e) {
        GridViewDataColumn column = e.Column as GridViewDataColumn;
        if (column == null) return;
        ASPxSummaryItem item = gridPosicaoRendaFixa.TotalSummary[column.FieldName];
        if (item == null) return;
        decimal summaryValue = Convert.ToDecimal(e.GetSummaryValue(item));
        if (summaryValue < 0)
            e.Cell.ForeColor = Color.Red;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridPosicaoSwap_HtmlFooterCellPrepared(object sender, ASPxGridViewTableFooterCellEventArgs e) {
        GridViewDataColumn column = e.Column as GridViewDataColumn;
        if (column == null) return;
        ASPxSummaryItem item = gridPosicaoSwap.TotalSummary[column.FieldName];
        if (item == null) return;
        decimal summaryValue = Convert.ToDecimal(e.GetSummaryValue(item));
        if (summaryValue < 0)
            e.Cell.ForeColor = Color.Red;
    }
}