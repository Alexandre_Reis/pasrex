﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using Financial.Security;
using Financial.ContaCorrente;
using System.Text;
using DevExpress.Web;
using Financial.Web.Util;
using System.Drawing;
using Financial.Investidor;
using Financial.BMF;
using System.Collections.Generic;
using EntitySpaces.Interfaces;
using Financial.Common;
using Financial.Security.Enums;

public partial class Consultas_PosicaoBMF : Financial.Web.Common.ConsultaBasePage {

    // Define se consulta será histórica ou não
    private enum TipoPesquisa {
        PosicaoBMF = 0,
        PosicaoBMFHistorico = 1
    }
    TipoPesquisa tipoPesquisa = TipoPesquisa.PosicaoBMF;

    new protected void Page_Load(object sender, EventArgs e) {
        this.HasPopupCliente = true;
        btnEditCodigoCliente.Focus();
        base.Page_Load(sender, e);

        TrataTravamentoCampos();
    }

    #region DataSources
    protected void EsDSPosicaoBMF_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e) {
        if (!Page.IsPostBack) {
            #region Condição Inicial
            gridConsulta.Visible = false;
            e.Collection = new PosicaoBMFCollection();
            #endregion
        }
        else {

            // Confere Campos Obrigatorios para o caso de Mudança de Pagina e Scroolbar
            e.Collection = new PosicaoBMFCollection();
            this.TrataCamposObrigatorios();
            //
            
            if (!String.IsNullOrEmpty(btnEditCodigoCliente.Text.Trim())) {
                #region Escolhido Apenas 1 Cliente

                //Checa se busca da posição atual ou histórica
                int idCliente = Convert.ToInt32(btnEditCodigoCliente.Text);
                DateTime data = Convert.ToDateTime(textData.Text);

                Cliente cliente = new Cliente();
                List<esQueryItem> campos = new List<esQueryItem>();
                campos.Add(cliente.Query.DataDia);
                cliente.LoadByPrimaryKey(idCliente);

                DateTime dataDia = cliente.DataDia.Value;

                if (data < dataDia) {
                    #region PosicaoHistorico
                    PosicaoBMFHistoricoCollection posicaoBMFCollection = new PosicaoBMFHistoricoCollection();
                    //                   
                    posicaoBMFCollection.Query.Where(posicaoBMFCollection.Query.IdCliente == idCliente,
                                                     posicaoBMFCollection.Query.DataHistorico == data);
                    
                    posicaoBMFCollection.Query.Load();
                    //
                    e.Collection = posicaoBMFCollection;
                    #endregion
                }
                else {
                    #region PosicaoBMF
                    PosicaoBMFCollection posicaoBMFCollection = new PosicaoBMFCollection();                    
                    posicaoBMFCollection.Query.Where(posicaoBMFCollection.Query.IdCliente == idCliente);
                    //
                    posicaoBMFCollection.Query.Load();

                    e.Collection = posicaoBMFCollection;
                    #endregion
                }
                #endregion
            }
            else {
                #region Varios Clientes
                ClienteCollection clienteCollection = new ClienteCollection();
                clienteCollection.BuscaClientesComAcesso(HttpContext.Current.User.Identity.Name);

                if (clienteCollection.Count == 0) { // Se nao tem Cliente com Acesso
                    e.Collection = new PosicaoBMFCollection();
                }
                else {
                    DateTime data = Convert.ToDateTime(textData.Text);
                    // Collections usadas para o Bindind
                    PosicaoBMFCollection pBinding = new PosicaoBMFCollection();

                    // Para Cada Cliente de Acordo com a DataDia Consulta em Posicao ou PosicaoHistorico
                    for (int i = 0; i < clienteCollection.Count; i++) {
                        int idClienteAux = clienteCollection[i].IdCliente.Value;

                        Cliente cliente = new Cliente();
                        List<esQueryItem> campos = new List<esQueryItem>();
                        campos.Add(cliente.Query.DataDia);
                        cliente.LoadByPrimaryKey(campos, idClienteAux);

                        this.tipoPesquisa = data >= cliente.DataDia.Value ? TipoPesquisa.PosicaoBMF : TipoPesquisa.PosicaoBMFHistorico;

                        if (this.tipoPesquisa == TipoPesquisa.PosicaoBMF) {
                            #region PosicaoBMF
                            PosicaoBMFCollection posicaoBMFCollection = new PosicaoBMFCollection();
                            posicaoBMFCollection.Query.Where(posicaoBMFCollection.Query.IdCliente == idClienteAux);
                            //
                            posicaoBMFCollection.Query.Load();
                            //
                            pBinding.Combine(posicaoBMFCollection);

                            #endregion
                        }
                        else {
                            #region PosicaoHistorico
                            PosicaoBMFHistoricoCollection pHistoricoCollection = new PosicaoBMFHistoricoCollection();
                            pHistoricoCollection.Query.Where(pHistoricoCollection.Query.IdCliente == idClienteAux,
                                                             pHistoricoCollection.Query.DataHistorico == data);
                            pHistoricoCollection.Query.Load();
                            //
                            for (int j = 0; j < pHistoricoCollection.Count; j++) {
                                PosicaoBMF p = pBinding.AddNew();
                                p.IdCliente = pHistoricoCollection[j].IdCliente;
                                p.IdAgente = pHistoricoCollection[j].IdAgente;
                                p.CdAtivoBMF = pHistoricoCollection[j].CdAtivoBMF;
                                p.Serie = pHistoricoCollection[j].Serie;
                                p.Quantidade = pHistoricoCollection[j].Quantidade;
                                p.PUCustoLiquido = pHistoricoCollection[j].PUCustoLiquido;
                                p.PUMercado = pHistoricoCollection[j].PUMercado;
                                p.ValorMercado = pHistoricoCollection[j].ValorMercado;
                                p.ResultadoRealizar = pHistoricoCollection[j].ResultadoRealizar;
                            }
                            #endregion
                        }
                    }
                    pBinding.Sort = PosicaoBMFMetadata.ColumnNames.IdCliente + " ASC, " + PosicaoBMFMetadata.ColumnNames.CdAtivoBMF + " ASC, " + PosicaoBMFMetadata.ColumnNames.Serie + " ASC";
                    e.Collection = pBinding;
                }
                #endregion
            }

            // Define Grid como Visivel
            gridConsulta.Visible = true;
        }
    }

    protected void EsDSCliente_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e) {
        ClienteCollection clienteCollection = new ClienteCollection();
        clienteCollection.BuscaClientesComAcesso(HttpContext.Current.User.Identity.Name);

        e.Collection = clienteCollection;
    }

    protected void EsDSAgenteMercado_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e) {
        AgenteMercadoCollection coll = new AgenteMercadoCollection();

        coll.Query.Where(coll.Query.FuncaoCustodiante.Equal("S"));
        coll.Query.OrderBy(coll.Query.Nome.Ascending);
        coll.LoadAll();

        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }
    #endregion

    /// <summary>
    /// 
    /// </summary>
    private void TrataTravamentoCampos() {
        Usuario usuario = new Usuario();
        usuario.BuscaUsuario(HttpContext.Current.User.Identity.Name);
        int tipoTrava = usuario.TipoTrava.Value;

        if (tipoTrava == (int)TipoTravaUsuario.TravaCliente) {
            btnEditCodigoCliente.Enabled = false;

            PermissaoCliente permissaoCliente = new PermissaoCliente();
            int? idCliente = permissaoCliente.RetornaClienteAssociado(HttpContext.Current.User.Identity.Name);

            if (idCliente.HasValue) {
                Cliente cliente = new Cliente();

                List<esQueryItem> campos = new List<esQueryItem>();
                campos.Add(cliente.Query.Apelido);
                cliente.LoadByPrimaryKey(campos, idCliente.Value);
                string apelido = cliente.Apelido;

                btnEditCodigoCliente.Text = Convert.ToString(idCliente.Value);
                textNome.Text = apelido;
            }
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void ASPxCallback1_Callback(object source, DevExpress.Web.CallbackEventArgs e) {
        string nome = "";
        if (!String.IsNullOrEmpty(e.Parameter) && e.Parameter != "null") {
            int idCliente = Convert.ToInt32(e.Parameter);
            Cliente cliente = new Cliente();
            cliente.LoadByPrimaryKey(idCliente);

            if (cliente.str.Apelido != "") {
                if (cliente.IsAtivo) {
                    //O permissionamento da cliente é dado direto no cliente vinculado à cliente
                    PermissaoCliente permissaoCliente = new PermissaoCliente();
                    if (permissaoCliente.RetornaAcessoClienteComControle(idCliente, HttpContext.Current.User.Identity.Name)) {
                        nome = cliente.str.Apelido;
                    }
                    else {
                        nome = "no_access";
                    }
                }
                else {
                    nome = "no_active";
                }
            }
        }
        e.Result = nome;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridConsulta_HtmlDataCellPrepared(object sender, ASPxGridViewTableDataCellEventArgs e) {
        if (e.DataColumn.FieldName == "Quantidade") {
            int value = (int)e.GetValue("Quantidade");
            if (value < 0)
                e.Cell.ForeColor = Color.Red;
        }
        else if (e.DataColumn.FieldName == "ValorMercado") {
            decimal value = (decimal)e.GetValue("ValorMercado");
            if (value < 0)
                e.Cell.ForeColor = Color.Red;
        }
        else if (e.DataColumn.FieldName == "ResultadoRealizar") {
            decimal value = (decimal)e.GetValue("ResultadoRealizar");
            if (value < 0)
                e.Cell.ForeColor = Color.Red;
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void exporter_RenderBrick(object sender, ASPxGridViewExportRenderingEventArgs e) {
        //GridViewDataColumn dataColumn = e.Column as GridViewDataColumn;
        ////
        //if (dataColumn.FieldName == PosicaoBMFMetadata.ColumnNames.Quantidade) {
        //    int value = (int)e.GetValue(PosicaoBMFMetadata.ColumnNames.Quantidade);
        //    if (value < 0)
        //        e.BrickStyle.ForeColor = Color.Red;
        //}
        //else if (dataColumn.FieldName == PosicaoBMFMetadata.ColumnNames.ValorMercado) {
        //    decimal value = (decimal)e.GetValue(PosicaoBMFMetadata.ColumnNames.ValorMercado);
        //    if (value < 0)
        //        e.BrickStyle.ForeColor = Color.Red;
        //}
        //else if (dataColumn.FieldName == PosicaoBMFMetadata.ColumnNames.ResultadoRealizar) {
        //    decimal value = (decimal)e.GetValue(PosicaoBMFMetadata.ColumnNames.ResultadoRealizar);
        //    if (value < 0)
        //        e.BrickStyle.ForeColor = Color.Red;
        //}        
    }

    #region btnRun_Click, btnPDF_Click, btnExcel_Click

    /// <summary>
    /// Trata os campos Obrigatorios do Form
    /// </summary>
    private void TrataCamposObrigatorios() {
        #region Campos obrigatórios
        List<Control> controles = new List<Control>(new Control[] { this.textData });

        if (base.TestaObrigatorio(controles) != "") {
            throw new Exception("Campos com * são obrigatórios!");
        }
        #endregion
    }

    new protected void btnRun_Click(object sender, EventArgs e) {
        this.TrataCamposObrigatorios();
        base.btnRun_Click(sender, e); // DataBind do Grid
    }

    new protected void btnPDF_Click(object sender, EventArgs e) {
        this.TrataCamposObrigatorios();
        this.SelecionaRelatorio(VisaoRelatorio.visaoPDF);
    }

    new protected void btnExcel_Click(object sender, EventArgs e) {
        this.TrataCamposObrigatorios();
        this.SelecionaRelatorio(VisaoRelatorio.visaoExcel);
    }

    /// <summary>
    /// Seleciona o Relatorio a Ser Executado
    /// </summary>
    /// <param name="visao">PDF, Excel ou Report</param>
    private void SelecionaRelatorio(string visao) {
        Response.BufferOutput = true;
        //
        // Salva os Parametros na Session
        Session["gridExportPosicaoBMF"] = gridExport;
        Response.Redirect("~/Consultas/PosicaoBMFExporta.aspx?Visao=" + visao);
    }

    #endregion
}