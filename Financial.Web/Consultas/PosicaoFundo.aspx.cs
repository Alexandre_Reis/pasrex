﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using Financial.Security;
using Financial.ContaCorrente;
using System.Text;
using DevExpress.Web;
using Financial.Web.Util;
using System.Drawing;
using Financial.Investidor;
using Financial.Bolsa;
using System.Collections.Generic;
using EntitySpaces.Interfaces;
using Financial.Common;
using Financial.Fundo;
using Financial.Security.Enums;
using Financial.Web.Common;
using System.Threading;

public partial class Consultas_PosicaoFundo : ConsultaBasePage {

    // Define se consulta será histórica ou não
    private enum TipoPesquisa {
        PosicaoFundo = 0,
        PosicaoFundoHistorico = 1
    }
    TipoPesquisa tipoPesquisa = TipoPesquisa.PosicaoFundo;

    new protected void Page_Load(object sender, EventArgs e) {
        this.HasPopupCliente = true;
        btnEditCodigoCliente.Focus();
        base.Page_Load(sender, e);

        TrataTravamentoCampos();
    }

    #region DataSources
    protected void EsDSPosicaoFundo_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e) {
        if (!Page.IsPostBack) {
            #region Condição Inicial
            gridConsulta.Visible = false;
            e.Collection = new PosicaoFundoCollection();
            #endregion
        }
        else {

            // Confere Campos Obrigatorios para o caso de Mudança de Pagina e Scroolbar
            e.Collection = new PosicaoFundoCollection();
            this.TrataCamposObrigatorios();

            if (!String.IsNullOrEmpty(btnEditCodigoCliente.Text.Trim())) 
            {
                #region Escolhido Apenas 1 Cliente
                //Checa se busca da posição atual ou histórica
                int idCliente = Convert.ToInt32(btnEditCodigoCliente.Text);
                DateTime data = Convert.ToDateTime(textData.Text);

                Cliente cliente = new Cliente();
                List<esQueryItem> campos = new List<esQueryItem>();
                campos.Add(cliente.Query.DataDia);
                cliente.LoadByPrimaryKey(idCliente);

                DateTime dataDia = cliente.DataDia.Value;

                ClienteQuery clienteFundoQuery = new ClienteQuery("L");                
                PosicaoFundoHistoricoCollection posicaoFundoCollection = new PosicaoFundoHistoricoCollection();

                PosicaoFundoHistoricoQuery posicaoFundoHistoricoQuery = new PosicaoFundoHistoricoQuery("P");
                CarteiraQuery carteiraQuery = new CarteiraQuery("A");

                posicaoFundoHistoricoQuery.Select(posicaoFundoHistoricoQuery,
                                                 clienteFundoQuery.Apelido.As("ApelidoCliente"),
                                                 carteiraQuery.Apelido.As("ApelidoFundo"),                        
                    ((posicaoFundoHistoricoQuery.CotaDia - posicaoFundoHistoricoQuery.CotaAplicacao) * posicaoFundoHistoricoQuery.Quantidade).As("Rendimento"));
                    
                posicaoFundoHistoricoQuery.InnerJoin(clienteFundoQuery).On(clienteFundoQuery.IdCliente == posicaoFundoHistoricoQuery.IdCarteira);
                posicaoFundoHistoricoQuery.InnerJoin(carteiraQuery).On(carteiraQuery.IdCarteira == posicaoFundoHistoricoQuery.IdCarteira);
                posicaoFundoHistoricoQuery.Where(posicaoFundoHistoricoQuery.IdCliente == idCliente,
                                                 posicaoFundoHistoricoQuery.DataHistorico == data);

                posicaoFundoHistoricoQuery.Where(posicaoFundoHistoricoQuery.Quantidade != 0 | posicaoFundoHistoricoQuery.QtdePendenteLiquidacao != 0);

                posicaoFundoCollection.Load(posicaoFundoHistoricoQuery);
                e.Collection = posicaoFundoCollection;                    
                #endregion
            }
            else {
                #region Varios Clientes
                ClienteCollection clienteCollection = new ClienteCollection();
                clienteCollection.BuscaClientesComAcesso(HttpContext.Current.User.Identity.Name);

                if (clienteCollection.Count == 0) { // Se nao tem Cliente com Acesso
                    e.Collection = new PosicaoFundoCollection();
                }
                else {
                    DateTime data = Convert.ToDateTime(textData.Text);
                    // Collections usadas para o Bindind
                    PosicaoFundoCollection pBinding = new PosicaoFundoCollection();
                    
                    pBinding.CreateColumnsForBinding();                                        
                    pBinding.AddColumn("ApelidoCliente", typeof(System.String));
                    pBinding.AddColumn("ApelidoFundo", typeof(System.String));
                    pBinding.AddColumn("Rendimento", typeof(System.Decimal));
                    
                    // Para Cada Cliente de Acordo com a DataDia Consulta em Posicao ou PosicaoHistorico
                    for (int i = 0; i < clienteCollection.Count; i++) {
                        int idClienteAux = clienteCollection[i].IdCliente.Value;

                        PosicaoFundoHistoricoCollection posicaoFundoCollection = new PosicaoFundoHistoricoCollection();

                        PosicaoFundoHistoricoQuery posicaoFundoQuery = new PosicaoFundoHistoricoQuery("P");
                        ClienteQuery clienteFundoQuery = new ClienteQuery("L");
                        CarteiraQuery carteiraQuery = new CarteiraQuery("A");

                        posicaoFundoQuery.Select(posicaoFundoQuery,
                                                 clienteFundoQuery.Apelido.As("ApelidoCliente"),
                                                 carteiraQuery.Apelido.As("ApelidoFundo"),
                                            ((posicaoFundoQuery.CotaDia - posicaoFundoQuery.CotaAplicacao) * posicaoFundoQuery.Quantidade).As("Rendimento"));
                        posicaoFundoQuery.InnerJoin(clienteFundoQuery).On(clienteFundoQuery.IdCliente == posicaoFundoQuery.IdCarteira);
                        posicaoFundoQuery.InnerJoin(carteiraQuery).On(carteiraQuery.IdCarteira == posicaoFundoQuery.IdCarteira);
                        posicaoFundoQuery.Where(posicaoFundoQuery.IdCliente == idClienteAux,
                                                posicaoFundoQuery.DataHistorico.Equal(data));

                        posicaoFundoQuery.Where(posicaoFundoQuery.Quantidade != 0 | posicaoFundoQuery.QtdePendenteLiquidacao != 0);

                        posicaoFundoCollection.Load(posicaoFundoQuery);
                        //

                        for (int j = 0; j < posicaoFundoCollection.Count; j++)
                        {
                            PosicaoFundo p = pBinding.AddNew();
                            p.IdCliente = posicaoFundoCollection[j].IdCliente;
                            p.IdCarteira = posicaoFundoCollection[j].IdCarteira;
                            p.DataAplicacao = posicaoFundoCollection[j].DataAplicacao;
                            p.CotaDia = posicaoFundoCollection[j].CotaDia;
                            p.CotaAplicacao = posicaoFundoCollection[j].CotaAplicacao;
                            p.Quantidade = posicaoFundoCollection[j].Quantidade;
                            p.ValorBruto = posicaoFundoCollection[j].ValorBruto;
                            p.ValorIR = posicaoFundoCollection[j].ValorIR;
                            p.ValorIOF = posicaoFundoCollection[j].ValorIOF;
                            p.ValorLiquido = posicaoFundoCollection[j].ValorLiquido;
                            p.SetColumn("ApelidoCliente", posicaoFundoCollection[j].GetColumn("ApelidoCliente"));
                            p.SetColumn("ApelidoFundo", posicaoFundoCollection[j].GetColumn("ApelidoFundo"));
                            p.SetColumn("Rendimento", posicaoFundoCollection[j].GetColumn("Rendimento"));                                
                        }                            
                        
                    }
                    pBinding.Sort = PosicaoFundoMetadata.ColumnNames.IdCliente + " ASC, " + PosicaoFundoMetadata.ColumnNames.IdCarteira + " ASC, " + PosicaoFundoMetadata.ColumnNames.DataAplicacao + " ASC";
                    e.Collection = pBinding;
                }
                #endregion
            }

            // Define Grid como Visivel
            gridConsulta.Visible = true;
        }
    }

    protected void EsDSCarteira_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e) {
        CarteiraCollection carteiraCollection = new CarteiraCollection();
        carteiraCollection.BuscaCarteirasComAcessoTodas(HttpContext.Current.User.Identity.Name);

        e.Collection = carteiraCollection;
    }

    protected void EsDSCliente_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e) {
        ClienteCollection clienteCollection = new ClienteCollection();
        clienteCollection.BuscaClientesComAcesso(HttpContext.Current.User.Identity.Name);

        e.Collection = clienteCollection;
    }
    #endregion

    /// <summary>
    /// 
    /// </summary>
    private void TrataTravamentoCampos() {
        Usuario usuario = new Usuario();
        usuario.BuscaUsuario(HttpContext.Current.User.Identity.Name);
        int tipoTrava = usuario.TipoTrava.Value;

        if (tipoTrava == (int)TipoTravaUsuario.TravaCliente) {
            btnEditCodigoCliente.Enabled = false;

            PermissaoCliente permissaoCliente = new PermissaoCliente();
            int? idCliente = permissaoCliente.RetornaClienteAssociado(HttpContext.Current.User.Identity.Name);

            if (idCliente.HasValue) {
                Cliente cliente = new Cliente();

                List<esQueryItem> campos = new List<esQueryItem>();
                campos.Add(cliente.Query.Apelido);
                cliente.LoadByPrimaryKey(campos, idCliente.Value);
                string apelido = cliente.Apelido;

                btnEditCodigoCliente.Text = Convert.ToString(idCliente.Value);
                textNome.Text = apelido;
            }
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void ASPxCallback1_Callback(object source, DevExpress.Web.CallbackEventArgs e) {
        string nome = "";
        if (!String.IsNullOrEmpty(e.Parameter) && e.Parameter != "null") {
            int idCliente = Convert.ToInt32(e.Parameter);
            Cliente cliente = new Cliente();
            cliente.LoadByPrimaryKey(idCliente);

            if (cliente.str.Apelido != "") {
                if (cliente.IsAtivo) {
                    PermissaoCliente permissaoCliente = new PermissaoCliente();
                    if (permissaoCliente.RetornaAcessoClienteComControle(idCliente, HttpContext.Current.User.Identity.Name)) {
                        DateTime dataDia = cliente.DataDia.Value;
                        nome = cliente.str.Apelido + "|" + dataDia.ToShortDateString() + "|" + Thread.CurrentThread.CurrentCulture.Name;
                    }
                    else {
                        nome = "no_access";
                    }
                }
                else {
                    nome = "no_active";
                }
            }
        }
        e.Result = nome;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridConsulta_HtmlDataCellPrepared(object sender, ASPxGridViewTableDataCellEventArgs e) {
        if (e.DataColumn.FieldName == "ValorIR" || e.DataColumn.FieldName == "ValorIOF" || e.DataColumn.FieldName == "ValorPerformance") {
            e.Cell.ForeColor = Color.Red;
        }
    }

    /// <summary>
    /// Imprime o Footer
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridConsulta_HtmlFooterCellPrepared(object sender, ASPxGridViewTableFooterCellEventArgs e) {
        GridViewDataColumn column = e.Column as GridViewDataColumn;
        if (column == null) return;

        ASPxSummaryItem item = this.gridConsulta.TotalSummary[column.FieldName];
        if (item == null) return;

        decimal summaryValue = Convert.ToDecimal(e.GetSummaryValue(item));

        if (summaryValue < 0) {
            e.Cell.ForeColor = Color.Red;
        }

        // Escreve Total na Primeira Coluna
        if (column.FieldName == "IdCliente") {
            e.Cell.Text = "Total: ";
        }

        if (this.gridConsulta.VisibleRowCount == 0) {
            e.Cell.Text = ""; // Quando tem 0 registros imprime 0.00
        }
    }

    #region btnRun_Click, btnPDF_Click, btnExcel_Click

    /// <summary>
    /// Trata os campos Obrigatorios do Form
    /// </summary>
    private void TrataCamposObrigatorios() {
        #region Campos obrigatórios
        List<Control> controles = new List<Control>(new Control[] { this.textData });

        if (base.TestaObrigatorio(controles) != "") {
            throw new Exception("Campos com * são obrigatórios!");
        }
        #endregion
    }

    new protected void btnRun_Click(object sender, EventArgs e) {
        this.TrataCamposObrigatorios();
        base.btnRun_Click(sender, e); // DataBind do Grid
    }

    new protected void btnPDF_Click(object sender, EventArgs e) {
        this.TrataCamposObrigatorios();
        this.SelecionaRelatorio(VisaoRelatorio.visaoPDF);
    }

    new protected void btnExcel_Click(object sender, EventArgs e) {
        this.TrataCamposObrigatorios();
        this.SelecionaRelatorio(VisaoRelatorio.visaoExcel);
    }

    /// <summary>
    /// Seleciona o Relatorio a Ser Executado
    /// </summary>
    /// <param name="visao">PDF, Excel ou Report</param>
    private void SelecionaRelatorio(string visao) {
        Response.BufferOutput = true;
        //
        // Salva os Parametros na Session
        Session["gridExportPosicaoFundo"] = gridExport;
        Response.Redirect("~/Consultas/PosicaoFundoExporta.aspx?Visao=" + visao);
    }

    #endregion
}