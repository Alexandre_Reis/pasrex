﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="DistribuicaoProventos.aspx.cs" Inherits="Consultas_DistribuicaoProventos" %>

<%@ Register Assembly="System.Web, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"
    Namespace="System.Web.UI.HtmlControls" TagPrefix="cc2" %>

<%@ Register Assembly="EntitySpaces.Web" Namespace="EntitySpaces.Web" TagPrefix="cc1" %>

<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxe" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxwgv" %>
    
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxpc" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxcb" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">    
    <link href="~/css/forms.css" rel="stylesheet" type="text/css" />
    
    <script type="text/javascript" language="Javascript" src="../js/global.js"></script>
    <script language="JavaScript">
    function OnGetDataCarteira(values) {
        btnEditCodigoCarteira.SetValue(values);                    
        popupCarteira.HideWindow();
        ASPxCallback1.SendCallback(btnEditCodigoCarteira.GetValue());        
        btnEditCodigoCarteira.Focus();    
    }
    </script>
</head>
<body>
    <form id="form1" runat="server">
        
        <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="true" EnableScriptLocalization="true"/>
        
        <dxcb:ASPxCallback ID="ASPxCallback1" runat="server" OnCallback="ASPxCallback1_Callback">
            <ClientSideEvents CallbackComplete="function(s, e) {   
                OnCallBackCompleteCliente(s, e, popupMensagemCarteira, btnEditCodigoCarteira, document.getElementById('textNome'));
            }        
            "/>
        </dxcb:ASPxCallback>
        
        <div class="divPanel">
        <table width="100%"><tr><td>
        <div id="container_small">
        
        <div id="header">
            <asp:Label ID="lblHeader" runat="server" Text="Distribuição Proventos"></asp:Label>
        </div>
            
        <div id="mainContent">    
            
            <div class="reportFilter">
                <div style="height:20px"></div>                     
                                            
                <table cellpadding="2" cellspacing="2">
                <tr>
                <td class="td_Label">
                    <asp:Label ID="labelCarteira" runat="server" CssClass="labelNormal" Text="Carteira:"></asp:Label>
                </td>
                
                <td>
                    <dxe:ASPxSpinEdit ID="btnEditCodigoCarteira" runat="server" CssClass="textButtonEdit"
                                        EnableClientSideAPI="True" ClientInstanceName="btnEditCodigoCarteira"
                                        MaxLength="9" NumberType="Integer" AllowMouseWheel="false">
                    <Buttons>
                        <dxe:EditButton></dxe:EditButton>                                
                    </Buttons>        
                    <ClientSideEvents
                             KeyPress="function(s, e) {document.getElementById('textNome').value = '';}" 
                             ButtonClick="function(s, e) {popupCarteira.ShowAtElementByID(s.name);}" 
                             LostFocus="function(s, e) {OnLostFocus(popupMensagemCarteira, ASPxCallback1, btnEditCodigoCarteira);}"
                    />                            
                    </dxe:ASPxSpinEdit>                
                </td>                          
                
                <td>
                    <asp:TextBox ID="textNome" runat="server" CssClass="textNome" Enabled="false"></asp:TextBox>
                </td>
                </tr>                                                                               
            </table>
                        
            <table cellpadding="2" cellspacing="2">
                <tr>
                    <td class="td_Label">                    
                        <asp:Label ID="labelDataInicio" runat="server" CssClass="labelRequired" Text="Início:"></asp:Label>
                    </td>                        
                    <td>
                       <dxe:ASPxDateEdit ID="textDataInicio" runat="server" ClientInstanceName="textDataInicio" />                     
                    </td>  
                    
                    <td>
                        <asp:Label ID="labelDataFim" runat="server" CssClass="labelRequired" Text="Fim:"></asp:Label>                        
                    </td>                                                                            
                    <td>
                        <dxe:ASPxDateEdit ID="textDataFim" runat="server" ClientInstanceName="textDataFim" />
                    </td>
                </tr>                                                                               
            </table>
                                   
            </div>
            
            <div id="progressBar" class="progressBar" runat=server>
            <asp:UpdateProgress ID="UpdateProgress1" runat="server">
                <ProgressTemplate>            
                    <dxe:ASPxImage runat="server" ID="roller" ImageUrl="~/imagens/roller.gif" ClientInstanceName="roller"/>
                    <asp:Label ID="lblCarregar" runat="server" Text="Carregando..."/>
                </ProgressTemplate>
            </asp:UpdateProgress>
            </div>
                                               
            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
            <ContentTemplate>
                
                <div class="linkButton linkButtonNoBorder" >
                   <asp:LinkButton ID="btnRun" runat="server" Font-Overline="false" CssClass="btnRun" OnClick="btnRun_Click"><asp:Literal ID="Literal1" runat="server" Text="Consultar"/><div></div></asp:LinkButton>
                   <asp:LinkButton ID="btnPDF" runat="server" Font-Overline="false" CssClass="btnPdf" OnClick="btnPDF_Click"><asp:Literal ID="Literal2" runat="server" Text="Gerar PDF"/><div></div></asp:LinkButton>
                   <asp:LinkButton ID="btnExcel" runat="server" Font-Overline="false" CssClass="btnExcel" OnClick="btnExcel_Click"><asp:Literal ID="Literal3" runat="server" Text="Gerar Excel"/><div></div></asp:LinkButton>
                </div>
                            
                <div class="divDataGrid">    
         
                    <dxwgv:ASPxGridView ID="gridConsulta" runat="server" ClientInstanceName="gridConsulta" EnableCallBacks="true"                        
                        DataSourceID="EsDSProventos" OnHtmlDataCellPrepared="gridConsulta_HtmlDataCellPrepared">
                        <Columns>                                                     
                           
                            <dxwgv:GridViewDataTextColumn FieldName="IdOperacaoReferencia" Caption="Id Operação" VisibleIndex="1" Width="5%" HeaderStyle-HorizontalAlign="Left" CellStyle-HorizontalAlign="Left" />
                            
                            <dxwgv:GridViewDataTextColumn FieldName="IdCotista" Caption="Id Cotista" VisibleIndex="2" Width="5%" HeaderStyle-HorizontalAlign="Left" CellStyle-HorizontalAlign="Left" />
                            
                            <dxwgv:GridViewDataTextColumn FieldName="Nome" Caption="Nome" VisibleIndex="2" Width="30%" HeaderStyle-HorizontalAlign="Left" CellStyle-HorizontalAlign="Left" />

                            <dxwgv:GridViewDataTextColumn FieldName="IsentoIR"  Caption="Isento IR" VisibleIndex="3" Width="5%" HeaderStyle-HorizontalAlign="center" CellStyle-HorizontalAlign="center"/>
                            
                            <dxwgv:GridViewDataTextColumn FieldName="IsentoIOF"  Caption="Isento IOF" VisibleIndex="4" Width="5%" HeaderStyle-HorizontalAlign="center" CellStyle-HorizontalAlign="center"/>
                            
                            <dxwgv:GridViewDataDateColumn FieldName="DataAplicacao" Caption="Aplicação" VisibleIndex="5" Width="8%"/> 

                            <dxwgv:GridViewDataTextColumn FieldName="Quantidade" Caption="Qtde Cotas" VisibleIndex="6" Width="12%" HeaderStyle-HorizontalAlign="Right" CellStyle-HorizontalAlign="Right">
                            <PropertiesTextEdit DisplayFormatString="{0:#,##0.00000000;(#,##0.00000000);0.00000000}"></PropertiesTextEdit>
                            </dxwgv:GridViewDataTextColumn>  
                            
                            
                            <dxwgv:GridViewDataComboBoxColumn FieldName="TipoOperacao" Caption="Tipo" VisibleIndex="7" Width="10%">
                                <PropertiesComboBox EncodeHtml="false">
                                    <Items>
                                        <dxe:ListEditItem Value="80" Text="Amortização" />
                                        <dxe:ListEditItem Value="82" Text="Juros" />
                                        <dxe:ListEditItem Value="84" Text="Amort.+ Juros" />
                                        <dxe:ListEditItem Value="112" Text="Dividendo" />
                                    </Items>
                                </PropertiesComboBox>
                            </dxwgv:GridViewDataComboBoxColumn>
                            
                            <dxwgv:GridViewDataTextColumn FieldName="ValorBruto" Caption="Valor Bruto" VisibleIndex="8" Width="10%" HeaderStyle-HorizontalAlign="Right" CellStyle-HorizontalAlign="Right">                        
                            <PropertiesTextEdit DisplayFormatString="{0:#,##0.00;(#,##0.00);0.00}"></PropertiesTextEdit>
                            </dxwgv:GridViewDataTextColumn>
                            
                            <dxwgv:GridViewDataTextColumn FieldName="ValorIR" Caption="Valor IR" VisibleIndex="9" Width="10%" HeaderStyle-HorizontalAlign="Right" CellStyle-HorizontalAlign="Right">                        
                            <PropertiesTextEdit DisplayFormatString="{0:#,##0.00;(#,##0.00);0.00}"></PropertiesTextEdit>
                            </dxwgv:GridViewDataTextColumn>
                            
                            <dxwgv:GridViewDataTextColumn FieldName="ValorLiquido" Caption="Valor Líquido" VisibleIndex="10" Width="10%" HeaderStyle-HorizontalAlign="Right" CellStyle-HorizontalAlign="Right">                        
                            <PropertiesTextEdit DisplayFormatString="{0:#,##0.00;(#,##0.00);0.00}"></PropertiesTextEdit>
                            </dxwgv:GridViewDataTextColumn>
                        </Columns>
                    
                         <SettingsPager PageSize="15"/>
                        
                        <Settings ShowFilterRow="True" ShowStatusBar="Visible" VerticalScrollBarMode="Visible" 
                            VerticalScrollableHeight="340" VerticalScrollBarStyle="Virtual" />
                        
                        <SettingsText EmptyDataRow="0 registros"/>
                    </dxwgv:ASPxGridView>
                
                </div>
                        
            </ContentTemplate>
            </asp:UpdatePanel>
    
        </div>
        </div>
        </td></tr></table>
        </div>   
        
        <dxwgv:ASPxGridViewExporter ID="gridExport" runat="server" GridViewID="gridConsulta" Landscape="true" />
        
        <cc1:esDataSource ID="EsDSCarteira" runat="server" OnesSelect="EsDSCarteira_esSelect" />
        <cc1:esDataSource ID="EsDSProventos" runat="server" OnesSelect="EsDSProventos_esSelect" />
        
    </form>
    
<script type="text/javascript">  
// UpdateProgress Para Telas de Consultas com Impressao de Relatorio
// UpdateProgress Condicional que só atua no elemento Button - btnRun

var pageManager = Sys.WebForms.PageRequestManager.getInstance();
pageManager.add_beginRequest(BeginRequestHandler);
pageManager.add_endRequest(EndRequestHandler);

function BeginRequestHandler(sender, args) {              

    var elem = args.get_postBackElement();
    // First set associatedUpdatePanelId to non existant control
    // this will force the updateprogress not to try and show itself.               
    var o = $find('<%= UpdateProgress1.ClientID %>');
    o.set_associatedUpdatePanelId('Non_Existant_Control_Id');

    // Then based on the control ID, show the updateprogress
    if (elem.id == '<%= btnRun.ClientID %>') {        
        var updateProgress1 = $get('<%= UpdateProgress1.ClientID %>');  

        sleep(900); // Espera um pouco quando tem a mensagem de campos InvÃ¡lidos

        //o.set_displayAfter(1600);        
        //updateProgress1.set_associatedUpdatePanelId('UpdatePanel1');
        //updateProgress1.set_displayAfter(600);    

        updateProgress1.style.display = '';
    }
}

function EndRequestHandler(sender, args) {
    var updateProgress1 = $get('<%= UpdateProgress1.ClientID %>');        
    updateProgress1.style.display = (updateProgress1.style.display == '') ? 'none' : ''; 
    
    //var o = $find('<%= UpdateProgress1.ClientID %>');
    //o.set_associatedUpdatePanelId('Non_Existant_Control_Id');   
}

function sleep(milliseconds) {
  var start = new Date().getTime();
  for (var i = 0; i < 1e7; i++) {
    if ((new Date().getTime() - start) > milliseconds){
      break;
    }
  }
}
</script>
 
</body>
</html>