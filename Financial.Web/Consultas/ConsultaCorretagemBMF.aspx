﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ConsultaCorretagemBMF.aspx.cs" Inherits="ConsultaCorretagemBMF" %>

<%@ Register Assembly="System.Web, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a" Namespace="System.Web.UI.HtmlControls" TagPrefix="cc2" %>
<%@ Register Assembly="EntitySpaces.Web" Namespace="EntitySpaces.Web" TagPrefix="cc1" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxe" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxwgv" %>

<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxpc" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxcb" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head4" runat="server">

    <link href="~/css/forms.css" rel="stylesheet" type="text/css" />    
    <script type="text/javascript" language="Javascript" src="../js/global.js"></script>
    <script type="text/javascript" language="Javascript">
   
    function OnGetDataCliente(values) {
        btnEditCodigoCliente.SetValue(values);                    
        popupCliente.HideWindow();
        ASPxCallback1.SendCallback(btnEditCodigoCliente.GetValue());        
        btnEditCodigoCliente.Focus();    
    }
    
     function OnGetDataAtivoBMF(data) {
     
        btnEditAtivoBMF.SetValue(data);        
        ASPxCallback2.SendCallback(btnEditAtivoBMF.GetValue());
        popupAtivoBMF.HideWindow();
        btnEditAtivoBMF.Focus();
    }    
          
    </script>
</head>

<body>
    <form id="form1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="true" EnableScriptLocalization="true"/>
    

   <dxcb:ASPxCallback ID="ASPxCallback1" runat="server" OnCallback="ASPxCallback1_Callback">
            <ClientSideEvents CallbackComplete="function(s, e) {   
                OnCallBackCompleteCliente(s, e, popupMensagemCliente, btnEditCodigoCliente, document.getElementById('textNome'));
            }        
            "/>
        </dxcb:ASPxCallback>
    
    
    <dxcb:ASPxCallback ID="ASPxCallback2" runat="server" OnCallback="ASPxCallback2_Callback">
        <ClientSideEvents CallbackComplete="function(s, e) {
            var resultSplit = e.result.split('|');
            e.result = resultSplit[0];
            
            if (e.result == '' && btnEditAtivoBMF.GetValue() != null)
            {
                popupMensagemAtivoBMF.SetContentHTML(document.getElementById('textMsgNaoExiste').value);
                popupMensagemAtivoBMF.ShowAtElementByID(btnEditAtivoBMF.name);
                btnEditAtivoBMF.SetValue(''); 
                btnEditAtivoBMF.Focus();                 
            }
                                    
            var textTipoMercado = document.getElementById('gridConsulta_DXPEForm_ef' + gridConsulta.cp_EditVisibleIndex + '_textTipoMercado');
            var textPeso = document.getElementById('gridConsulta_DXPEForm_ef' + gridConsulta.cp_EditVisibleIndex + '_textPeso');
            
            if (textTipoMercado != null && textPeso != null)
            {
                if (resultSplit[1] != null)            
                {
                    textTipoMercado.value = resultSplit[1];
                    textPeso.value = resultSplit[2];                
                }
                else
                {
                    textTipoMercado.value = '';
                    textPeso.value = '';                
                }
            }
        }        
        "/>
  </dxcb:ASPxCallback>   
    
  <%--  <dxcb:ASPxCallback ID="callbackClone" runat="server" OnCallback="callbackClone_Callback">
        <ClientSideEvents CallbackComplete="function(s, e) 
        {    
            alert('Clonagem executada');
            gridCadastro.UpdateEdit();
        }        
        "/>
    </dxcb:ASPxCallback>--%>
        
    <div class="divPanel">
    <table width="100%"><tr><td>
    <div id="container">

    <div id="header">
        <asp:Label ID="lblHeader" runat="server" Text="Relatório de Corretagem BMF" />
    </div>
    
    <div id="mainContent">


<div class="reportFilter">
        <div style="height:20px"></div>
        <table cellpadding="2" cellspacing="2">
            <tr>
            <td class="td_Label">
                <asp:Label ID="labelCliente" runat="server" CssClass="labelNornal" Text="Cliente:"></asp:Label>
            </td>
            
            <td>
               <dxe:ASPxSpinEdit ID="btnEditCodigoCliente" runat="server" CssClass="textButtonEdit" 
                                                EnableClientSideAPI="True"
                                                ClientInstanceName="btnEditCodigoCliente"
                                                MaxLength="9" NumberType="Integer" AllowMouseWheel="false">  
                            <Buttons>
                                <dxe:EditButton>
                                </dxe:EditButton>                                
                            </Buttons>        
                            <ClientSideEvents
                                     KeyPress="function(s, e) {document.getElementById('textNome').value = '';}" 
                                     ButtonClick="function(s, e) {popupCliente.ShowAtElementByID(s.name);}" 
                                     LostFocus="function(s, e) {OnLostFocus(popupMensagemCliente, ASPxCallback1, btnEditCodigoCliente);}"
                            />                            
                            </dxe:ASPxSpinEdit>     
            </td>                          
            
            <td colspan="2" width="300">
                <asp:TextBox ID="textNome" runat="server" CssClass="textNome" Enabled="false"></asp:TextBox>       
            </td>
            </tr>
                        
            <tr>
            <td class="td_Label">                
                <asp:Label ID="labelDataInicio" runat="server" CssClass="labelRequired" Text="Início:"></asp:Label>
            </td>                        
            <td>
                <dxe:ASPxDateEdit ID="textDataInicio" runat="server" ClientInstanceName="textDataInicio" />
            </td>  
            
             <td colspan="2">
                <table>
                    <tr><td><asp:Label ID="labelDataFim" runat="server" CssClass="labelRequired" Text="Fim:"/></td>
                    <td><dxe:ASPxDateEdit ID="textDataFim" runat="server" ClientInstanceName="textDataFim"/></td></tr>
                </table>
            </td>                                                                   
            </tr>
            
            <tr>
            <td class="td_Label">
                <asp:Label ID="labelTipoMercado" runat="server" CssClass="labelNornal" Text="Corretora:"></asp:Label>
            </td>
            
            <td colspan="3">
                <dxe:ASPxComboBox ID="dropAgenteMercado" 
                                  runat="server"
                                  AllowMouseWheel="true"
                                  TextField="Nome" 
                                  ValueField="IdAgente"
                                  DataSourceID="EsDSAgenteMercado">
                </dxe:ASPxComboBox>
            </td>
            </tr>
            
            <tr>
                <td class="td_Label">            
                    <asp:Label ID="labelCdAtivo" runat="server" CssClass="labelNornal" Text="Ativo:"></asp:Label>
                </td>                
                <td colspan="3">
                     <dxe:ASPxButtonEdit ID="btnEditAtivoBMF" runat="server" CssClass="textAtivoCurto" MaxLength="20" 
                                                        ClientInstanceName="btnEditAtivoBMF">            
                                    <Buttons>
                                        <dxe:EditButton></dxe:EditButton>                
                                    </Buttons>           
                                    <ClientSideEvents                              
                                         ButtonClick="function(s, e) {popupAtivoBMF.ShowAtElementByID(s.name);}" 
                                         LostFocus="function(s, e) {popupMensagemAtivoBMF.HideWindow();
                                                                    if (btnEditAtivoBMF.GetValue() != null)
                                                                    {
                                                                        ASPxCallback2.SendCallback(btnEditAtivoBMF.GetValue());
                                                                    }
                                                                    }"
                                    />                    
                                    </dxe:ASPxButtonEdit>
                </td>    
            </tr>
            
        </table>                                                                                 
        
        </div>


            <div id="progressBar" class="progressBar" runat="server">
            <asp:UpdateProgress ID="UpdateProgress1" runat="server">
            <ProgressTemplate>
            
            <dxe:ASPxImage runat="server" ID="roller" ImageUrl="~/imagens/roller.gif" ClientInstanceName="roller"/>
            <asp:Label ID="lblCarregar" runat="server" Text="Carregando..."/>                       
            
            </ProgressTemplate>
            </asp:UpdateProgress>
            </div>

        <asp:UpdatePanel ID="UpdatePanel1" runat="server">           
        <ContentTemplate>

               
        <div class="linkButton" >               
           <asp:LinkButton ID="btnRun" runat="server" Font-Overline="false" CssClass="btnRun" OnClick="btnRun_Click"><asp:Literal ID="Literal1" runat="server" Text="Consultar"/><div></div></asp:LinkButton>
           <asp:LinkButton ID="btnPDF" runat="server" Font-Overline="false" ValidationGroup="ATK" CssClass="btnPdf" OnClick="btnPDF_Click"><asp:Literal ID="Literal2" runat="server" Text="Gerar PDF"/><div></div></asp:LinkButton>
           <asp:LinkButton ID="btnExcel" runat="server" Font-Overline="false" ValidationGroup="ATK" CssClass="btnExcel" OnClick="btnExcel_Click"><asp:Literal ID="Literal3" runat="server" Text="Gerar Excel"/><div></div></asp:LinkButton>
        </div>

        <div class="divDataGrid">
        <dxwgv:ASPxGridView ID="gridConsulta" runat="server" EnableCallBacks="true"
            KeyFieldName="CompositeKey" DataSourceID="EsRelatorioCorretagem">
                
        <Columns>  
            <dxwgv:GridViewCommandColumn ShowSelectCheckbox="True" VisibleIndex="0" ButtonType="Image" Width="5%" Visible="false" ShowClearFilterButton="True">
                <HeaderTemplate>
                    <dxe:ASPxCheckBox ID="cbAll" ClientInstanceName="cbAll" runat="server" ToolTip="Seleciona todos os CheckBoxs de todas as páginas" BackColor="white" ClientSideEvents-CheckedChanged="function(s, e) {OnAllCheckedChanged(s, e, gridCadastro);}" OnInit="CheckBoxSelectAll"/>
                </HeaderTemplate>
            </dxwgv:GridViewCommandColumn>                
            
            <dxwgv:GridViewDataTextColumn FieldName="Id" Visible="false" />
            <dxwgv:GridViewDataTextColumn FieldName="CompositeKey" UnboundType="String" Visible="false" />
            <dxwgv:GridViewDataTextColumn FieldName="DataReferencia" Caption="Data" UnboundType="datetime" VisibleIndex="0" Visible="true" Width="10%" >
                <PropertiesTextEdit DisplayFormatString="dd/MM/yyyy"></PropertiesTextEdit>
            </dxwgv:GridViewDataTextColumn>
            <dxwgv:GridViewDataTextColumn FieldName="Cliente" Caption="Cliente" UnboundType="String" Visible="true" VisibleIndex="1" Width="20%"/>
            <dxwgv:GridViewDataTextColumn FieldName="Corretora" Caption="Corretora" UnboundType="String" Visible="true" VisibleIndex="2" Width="20%" />
            <dxwgv:GridViewDataTextColumn FieldName="AtivoBMF" Caption="Ativo" UnboundType="String" Visible="true" VisibleIndex="3" />
            <dxwgv:GridViewDataTextColumn FieldName="SerieBMF" Caption="Serie" UnboundType="String" Visible="true" VisibleIndex="4" />
            <dxwgv:GridViewDataTextColumn FieldName="TipoOperacao" Caption="Orientação" UnboundType="String" Visible="true" VisibleIndex="5" />
            <dxwgv:GridViewDataTextColumn FieldName="Quantidade" Caption="Quantidade" UnboundType="String" Visible="true" VisibleIndex="6">
                <PropertiesTextEdit DisplayFormatString="{0:#,##0.0000;(#,##0.0000);0.0000}"></PropertiesTextEdit>
            </dxwgv:GridViewDataTextColumn>
            
            <dxwgv:GridViewDataTextColumn FieldName="Corretagem" Caption="Valor Total Corretagem" UnboundType="decimal" Visible="true" VisibleIndex="7">
                <PropertiesTextEdit DisplayFormatString="{0:#,##0.00;(#,##0.00);0.00}"></PropertiesTextEdit>
            </dxwgv:GridViewDataTextColumn>
                                           
        </Columns>
        
        
        <SettingsPopup EditForm-Width="500px" />
        <SettingsCommandButton>
            <ClearFilterButton Image-Url="../../imagens/funnel--minus.png"/>
        </SettingsCommandButton>
        
    </dxwgv:ASPxGridView>            
    </div>
    
    </ContentTemplate>
    </asp:UpdatePanel>
                
    </div>
    </div>
    </td></tr></table>
    </div>
    
    <dxwgv:ASPxGridViewExporter ID="gridExport" runat="server" GridViewID="gridConsulta" Landscape="true" LeftMargin="45" RightMargin="45" />
        
    <cc1:esDataSource ID="EsRelatorioCorretagem" runat="server" OnesSelect="EsRelatorioCorretagem_esSelect" LowLevelBind="true" />
    <cc1:esDataSource ID="EsDSCliente" runat="server" OnesSelect="EsDSCliente_esSelect" />
    <cc1:esDataSource ID="EsDSAgenteMercado" runat="server" OnesSelect="EsDSAgenteMercado_esSelect" />            
    <cc1:esDataSource ID="EsDSAtivoBMF" runat="server" OnesSelect="EsDSAtivoBMF_esSelect" />    
        
    </form>
    
    
    <script type="text/javascript">
// UpdateProgress Para Telas de Consultas com Impressao de Relatorio
// UpdateProgress Condicional que só atua no elemento Button - btnRun

var pageManager = Sys.WebForms.PageRequestManager.getInstance();
pageManager.add_beginRequest(BeginRequestHandler);
pageManager.add_endRequest(EndRequestHandler);

function BeginRequestHandler(sender, args) {              

    var elem = args.get_postBackElement();
    // First set associatedUpdatePanelId to non existant control
    // this will force the updateprogress not to try and show itself.               
    var o = $find('<%= UpdateProgress1.ClientID %>');
    o.set_associatedUpdatePanelId('Non_Existant_Control_Id');

    // Then based on the control ID, show the updateprogress
    if (elem.id == '<%= btnRun.ClientID %>') {        
        var updateProgress1 = $get('<%= UpdateProgress1.ClientID %>');  

        sleep(900); // Espera um pouco quando tem a mensagem de campos InvÃ¡lidos

        updateProgress1.style.display = '';
    }
}

function EndRequestHandler(sender, args) {
    var updateProgress1 = $get('<%= UpdateProgress1.ClientID %>');        
    updateProgress1.style.display = (updateProgress1.style.display == '') ? 'none' : '';     
}

function sleep(milliseconds) {
  var start = new Date().getTime();
  for (var i = 0; i < 1e7; i++) {
    if ((new Date().getTime() - start) > milliseconds){
      break;
    }
  }
}
</script>
    
</body>
</html>