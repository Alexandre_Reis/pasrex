﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Collections.Generic;
using System.Text;
using DevExpress.Web;
using Financial.RendaFixa;
using Financial.Security;
using Financial.Web.Util;
using Financial.Security.Enums;
using Financial.Util;
using System.Drawing;
using Financial.Web.Common;
using Financial.Common;
using Financial.RendaFixa.Enums;
using Financial.Investidor.Enums;
using Financial.Investidor;
using System.Threading;

public partial class CadastrosBasicos_MemCalculoRendaFixa : ConsultaBasePage
{
    private const string DescricaoVirtual = "< case when ltrim(rtrim(isNull(T.[DescricaoCompleta],''))) = '' then T.[Descricao] else T.[DescricaoCompleta] end as DescricaoVirtual>";
    new protected void Page_Load(object sender, EventArgs e)
    {
        this.HasPopupCliente = true;
        base.Page_Load(sender, e);
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void EsDSMemCalculoRendaFixa_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        MemoriaCalculoRendaFixaCollection coll = new MemoriaCalculoRendaFixaCollection();

        if (!Page.IsPostBack)
        {
            #region Condição Inicial
            gridConsulta.Visible = false;
            #endregion
        }
        else
        {
            MemoriaCalculoRendaFixaQuery memCalculoRendaFixaQuery = new MemoriaCalculoRendaFixaQuery("memoria");
            OperacaoRendaFixaQuery operacaoRendaFixaQuery = new OperacaoRendaFixaQuery("operacao");
            TituloRendaFixaQuery tituloRendaFixaQuery = new TituloRendaFixaQuery("titulo");
            PapelRendaFixaQuery papelRendaFixaQuery = new PapelRendaFixaQuery("papel");
            ClienteQuery clienteQuery = new ClienteQuery("cliente");

            memCalculoRendaFixaQuery.Select(memCalculoRendaFixaQuery,
                                            operacaoRendaFixaQuery.IdOperacao,
                                            operacaoRendaFixaQuery.IdTitulo,
                                            tituloRendaFixaQuery.DataVencimento,
                                            tituloRendaFixaQuery.Descricao.As("DescricaoTitulo"),
                                            papelRendaFixaQuery.Descricao.As("DescricaoPapel"),
                                            clienteQuery.IdCliente.As("ClienteID"),
                                            clienteQuery.Apelido.As("Apelido"));
            memCalculoRendaFixaQuery.InnerJoin(operacaoRendaFixaQuery).On(memCalculoRendaFixaQuery.IdOperacao.Equal(operacaoRendaFixaQuery.IdOperacao));
            memCalculoRendaFixaQuery.InnerJoin(clienteQuery).On(operacaoRendaFixaQuery.IdCliente.Equal(clienteQuery.IdCliente));
            memCalculoRendaFixaQuery.InnerJoin(tituloRendaFixaQuery).On(operacaoRendaFixaQuery.IdTitulo.Equal(tituloRendaFixaQuery.IdTitulo));
            memCalculoRendaFixaQuery.InnerJoin(papelRendaFixaQuery).On(tituloRendaFixaQuery.IdPapel.Equal(papelRendaFixaQuery.IdPapel));

            if (!string.IsNullOrEmpty(btnEditCodigoCliente.Text))
            {
                memCalculoRendaFixaQuery.Where(memCalculoRendaFixaQuery.IdCliente.Equal(Convert.ToInt32(btnEditCodigoCliente.Text)));
            }

            if (!string.IsNullOrEmpty(textDataInicio.Text))
            {
                memCalculoRendaFixaQuery.Where(memCalculoRendaFixaQuery.DataAtual.GreaterThanOrEqual(textDataInicio.Text));
            }

            if (dropTituloRendaFixa.Value != null)
            {
                memCalculoRendaFixaQuery.Where(tituloRendaFixaQuery.IdTitulo.Equal(Convert.ToInt32(dropTituloRendaFixa.Value)));
            }

            if (!string.IsNullOrEmpty(textDataFim.Text))
            {
                memCalculoRendaFixaQuery.Where(memCalculoRendaFixaQuery.DataAtual.LessThanOrEqual(textDataFim.Text));
            }

            memCalculoRendaFixaQuery.OrderBy(memCalculoRendaFixaQuery.DataAtual.Descending, 
                                             memCalculoRendaFixaQuery.IdOperacao.Ascending, 
                                             memCalculoRendaFixaQuery.TipoProcessamento.Descending,
                                             memCalculoRendaFixaQuery.TipoPreco.Descending,
                                             memCalculoRendaFixaQuery.TipoEventoTitulo.Descending,
                                             memCalculoRendaFixaQuery.DataFluxo.Descending);

            coll.Load(memCalculoRendaFixaQuery);
        }

        e.Collection = coll;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void callBackCliente_Callback(object source, DevExpress.Web.CallbackEventArgs e)
    {
        string resultado = string.Empty;
        if (!String.IsNullOrEmpty(e.Parameter) && e.Parameter != "null")
        {
            int idCliente = Convert.ToInt32(e.Parameter);
            Cliente cliente = new Cliente();
            cliente.LoadByPrimaryKey(idCliente);
            resultado = cliente.str.Apelido;
        }
        e.Result = resultado;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void EsDSCliente_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        ClienteCollection coll = new ClienteCollection();
        coll.BuscaClientesComAcesso(Context.User.Identity.Name);

        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void EsDSTituloRendaFixaFiltro_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        TituloRendaFixaQuery tituloRendaFixaQuery = new TituloRendaFixaQuery("T");
        PapelRendaFixaQuery papelRendaFixaQuery = new PapelRendaFixaQuery("P");
        tituloRendaFixaQuery.Select(tituloRendaFixaQuery.IdTitulo,
                                    tituloRendaFixaQuery.Descricao,
                                    tituloRendaFixaQuery.DataEmissao,
                                    tituloRendaFixaQuery.DataVencimento,
                                    tituloRendaFixaQuery.DescricaoCompleta.Trim(),
                                    papelRendaFixaQuery.Descricao.As("DescricaoPapel"),
                                    DescricaoVirtual);
        tituloRendaFixaQuery.InnerJoin(papelRendaFixaQuery).On(papelRendaFixaQuery.IdPapel == tituloRendaFixaQuery.IdPapel);
        tituloRendaFixaQuery.OrderBy(tituloRendaFixaQuery.IdTitulo.Ascending, tituloRendaFixaQuery.DataVencimento.Ascending);

        TituloRendaFixaCollection coll = new TituloRendaFixaCollection();
        coll.Load(tituloRendaFixaQuery);

        //
        e.Collection = coll;
    }

    protected void gridConsulta_CustomCallback(object sender, DevExpress.Web.ASPxGridViewCustomCallbackEventArgs e)
    {
        gridConsulta.DataBind();
        gridConsulta.Selection.UnselectAll();
        gridConsulta.CancelEdit();
    }

    protected void gridConsulta_Load(object sender, EventArgs e)
    {
        GridViewDataComboBoxColumn TipoPreco = gridConsulta.Columns["TipoPreco"] as GridViewDataComboBoxColumn;
        if (TipoPreco != null)
        {
            TipoPreco.PropertiesComboBox.Items.Clear();
            foreach (int i in Enum.GetValues(typeof(TipoPreco)))
            {
                TipoPreco.PropertiesComboBox.Items.Add(TipoPrecoDescricao.RetornaStringValue(i), i);
            }
        }

        GridViewDataComboBoxColumn TipoProcessamento = gridConsulta.Columns["TipoProcessamento"] as GridViewDataComboBoxColumn;
        if (TipoProcessamento != null)
        {
            TipoProcessamento.PropertiesComboBox.Items.Clear();
            foreach (int i in Enum.GetValues(typeof(TipoProcessamento)))
            {
                TipoProcessamento.PropertiesComboBox.Items.Add(TipoProcessamentoDescricao.RetornaStringValue(i), i);
            }
        }

        GridViewDataComboBoxColumn tipoEventoTitulo = gridConsulta.Columns["TipoEventoTitulo"] as GridViewDataComboBoxColumn;
        if (tipoEventoTitulo != null)
        {
            tipoEventoTitulo.PropertiesComboBox.Items.Clear();
            foreach (int i in Enum.GetValues(typeof(TipoEventoTitulo)))
            {
                tipoEventoTitulo.PropertiesComboBox.Items.Add(TipoEventoTituloDescricao.RetornaStringValue(i), i);
            }
        }
    }

    /// <summary>
    /// Trata os Erros de Relatorio
    /// </summary>
    private void TrataErros()
    {
        #region Campos obrigatórios
        List<Control> controles = new List<Control>(new Control[] { this.textDataInicio, this.dropTituloRendaFixa, this.textDataFim, this.btnEditCodigoCliente });

        if (base.TestaObrigatorio(controles) != "")
        {
            throw new Exception("Campos com * são obrigatórios!");
        }        
        #endregion

    }

    protected void btnRun_Click(object sender, EventArgs e)
    {
        try
        {
            this.TrataErros();

            gridConsulta.DataBind();
            gridConsulta.Selection.UnselectAll();
            gridConsulta.CancelEdit();
        }
        catch (Exception exception)
        {
            new Financial.Web.Util.ErrorHandler().HandleArrayIndexError(exception);
        }
    }

    new protected void btnPDF_Click(object sender, EventArgs e)
    {
        //this.TrataCamposObrigatorios();
        this.SelecionaRelatorio(VisaoRelatorio.visaoPDF);
    }

    new protected void btnExcel_Click(object sender, EventArgs e)
    {
        //this.TrataCamposObrigatorios();
        this.SelecionaRelatorio(VisaoRelatorio.visaoExcel);
    }

    /// <summary>
    /// Seleciona o Relatorio a Ser Executado
    /// </summary>
    /// <param name="visao">PDF, Excel ou Report</param>
    private void SelecionaRelatorio(string visao)
    {
        Response.BufferOutput = true;
        //
        // Salva os Parametros na Session
        Session["gridExportMemCalculoRendaFixa"] = gridExport;
        Response.Redirect("~/Consultas/MemCalculoRendaFixaExporta.aspx?Visao=" + visao);
    }


}