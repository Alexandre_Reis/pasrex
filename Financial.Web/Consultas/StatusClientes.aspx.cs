using System;
using System.Data;
using System.Configuration;
using System.Collections;

using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Text;

using Financial.Security;
using Financial.Investidor;
using Financial.Investidor.Enums;
using Financial.Common.Enums;

using DevExpress.Web;
using Financial.Web.Common;
using Financial.Fundo;
using Financial.Captacao.Enums;

public partial class Consultas_StatusClientes : ConsultaBasePage
{
    new protected void Page_Load(object sender, EventArgs e)
    {
        base.Page_Load(sender, e);
    }

    #region DataSources
    protected void EsDSCliente_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        Usuario usuario = new Usuario();
        usuario.BuscaUsuario(Context.User.Identity.Name);
        int idUsuario = usuario.IdUsuario.Value;

        ClienteQuery clienteQuery = new ClienteQuery("L");
        PermissaoClienteQuery permissaoClienteQuery = new PermissaoClienteQuery("P");
        GrupoProcessamentoQuery grupoProcessamentoQuery = new GrupoProcessamentoQuery("G");

        clienteQuery.Select(clienteQuery, grupoProcessamentoQuery.Descricao);
        clienteQuery.InnerJoin(permissaoClienteQuery).On(permissaoClienteQuery.IdCliente == clienteQuery.IdCliente);
        clienteQuery.InnerJoin(grupoProcessamentoQuery).On(clienteQuery.IdGrupoProcessamento == grupoProcessamentoQuery.IdGrupoProcessamento);
        //
        clienteQuery.Where(permissaoClienteQuery.IdUsuario == idUsuario);

        clienteQuery.Where(clienteQuery.TipoControle.NotEqual(TipoControleCliente.ApenasCotacao),
                           clienteQuery.StatusAtivo.Equal(StatusAtivoCliente.Ativo));
        //
        clienteQuery.OrderBy(clienteQuery.Nome.Ascending, grupoProcessamentoQuery.Descricao.Descending);

        ClienteCollection coll = new ClienteCollection();
        coll.Load(clienteQuery);



        clienteQuery = new ClienteQuery("L");
        CarteiraQuery carteiraQuery = new CarteiraQuery("A");
        grupoProcessamentoQuery = new GrupoProcessamentoQuery("G");

        clienteQuery.Select(clienteQuery, grupoProcessamentoQuery.Descricao);
        clienteQuery.InnerJoin(grupoProcessamentoQuery).On(clienteQuery.IdGrupoProcessamento == grupoProcessamentoQuery.IdGrupoProcessamento);
        clienteQuery.InnerJoin(carteiraQuery).On(carteiraQuery.IdCarteira == clienteQuery.IdCliente);
        //
        clienteQuery.Where(clienteQuery.TipoControle.Equal(TipoControleCliente.ApenasCotacao),
                           clienteQuery.StatusAtivo.Equal(StatusAtivoCliente.Ativo),
                           carteiraQuery.TipoVisaoFundo.NotEqual((byte)TipoVisaoFundoRebate.NaoTrata));
        //
        clienteQuery.OrderBy(clienteQuery.Nome.Ascending, grupoProcessamentoQuery.Descricao.Descending);

        ClienteCollection coll2 = new ClienteCollection();
        coll2.Load(clienteQuery);

        coll.Combine(coll2);

        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }

    protected void EsDSGrupoProcessamento_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        GrupoProcessamentoCollection coll = new GrupoProcessamentoCollection();
        coll.Query.OrderBy(coll.Query.Descricao.Descending);
        //
        coll.LoadAll();

        e.Collection = coll;
        //
        GrupoProcessamento g = new GrupoProcessamento();
        g.IdGrupoProcessamento = null;
        g.Descricao = "";

        coll.AttachEntity(g);
        //
        coll.Sort = GrupoProcessamentoMetadata.ColumnNames.Descricao + " ASC";
    }
    #endregion

    protected void gridConsulta_CustomUnboundColumnData(object sender, DevExpress.Web.ASPxGridViewColumnDataEventArgs e)
    {
        if (e.Column.FieldName == "StatusDescricao")
        {
            int status = Convert.ToInt32(e.GetListSourceFieldValue(ClienteMetadata.ColumnNames.Status));

            string statusDescricao = TraducaoEnumsInvestidor.EnumStatusCliente.TraduzEnum(status);

            e.Value = statusDescricao;
        }
    }

    protected void gridConsulta_CustomCallback(object sender, DevExpress.Web.ASPxGridViewCustomCallbackEventArgs e)
    {
        gridConsulta.DataBind();
        gridConsulta.Selection.UnselectAll();
        gridConsulta.CancelEdit();
    }
}