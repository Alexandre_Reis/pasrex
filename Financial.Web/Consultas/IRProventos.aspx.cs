﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using Financial.Security;
using Financial.ContaCorrente;
using System.Text;
using DevExpress.Web;
using Financial.Web.Util;
using System.Drawing;
using Financial.Investidor;
using Financial.Fundo;
using Financial.Fundo.Enums;
using Financial.Fundo.Exceptions;
using Financial.Security.Enums;
using System.Collections.Generic;
using EntitySpaces.Interfaces;
using Financial.Web.Common;
using Financial.Util;
using Financial.Investidor.Enums;
using Financial.InvestidorCotista;

public partial class Consultas_IRProventos : ConsultaBasePage 
{
    new protected void Page_Load(object sender, EventArgs e) {
        this.HasPopupCarteira = true;
        btnEditCodigoCarteira.Focus();
        base.Page_Load(sender, e);
    }

    #region DataSources
    protected void EsDSIRProventos_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        if (!Page.IsPostBack) {
            gridConsulta.Visible = false;
            e.Collection = new SaldoCaixaCollection();
        }
        else {
            // Confere Campos Obrigatorios para o caso de Mudança de Pagina e Scroolbar
            this.TrataCamposObrigatorios();
            //
            IRProventosQuery iRProventosQuery = new IRProventosQuery("IP");
            CarteiraQuery carteiraQuery = new CarteiraQuery("C");
            CotistaQuery cotistaQuery = new CotistaQuery("T");
            iRProventosQuery.Select(carteiraQuery.Apelido, cotistaQuery.Apelido.As("ApelidoCotista"), iRProventosQuery);
            iRProventosQuery.InnerJoin(cotistaQuery).On(iRProventosQuery.IdCotista.Equal(cotistaQuery.IdCotista));
            iRProventosQuery.InnerJoin(carteiraQuery).On(iRProventosQuery.IdCarteira.Equal(carteiraQuery.IdCarteira));
            iRProventosQuery.Where(iRProventosQuery.IdCarteira.Equal(Convert.ToInt32(btnEditCodigoCarteira.Text)),
                                   iRProventosQuery.DataVencimento.Between(Convert.ToDateTime(textDataInicio.Text), Convert.ToDateTime(textDataFim.Text)));

            IRProventosCollection iRProventosCollection = new IRProventosCollection();
            iRProventosCollection.Load(iRProventosQuery);

            e.Collection = iRProventosCollection;

        }
    }

    protected void EsDSCarteira_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e) {
        CarteiraCollection carteiraCollection = new CarteiraCollection();
        carteiraCollection.BuscaCarteirasComAcessoTodas(HttpContext.Current.User.Identity.Name);

        e.Collection = carteiraCollection;
    }

    #endregion

    /// <summary>
    /// 
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void ASPxCallback1_Callback(object source, DevExpress.Web.CallbackEventArgs e) {
        string nome = "";
        if (!String.IsNullOrEmpty(e.Parameter) && e.Parameter != "null") {
            int idCarteira = Convert.ToInt32(e.Parameter);
            Carteira carteira = new Carteira();
            carteira.LoadByPrimaryKey(idCarteira);

            if (carteira.str.Apelido != "") {
                if (carteira.IsAtivo) {
                    //O permissionamento da carteira é dado direto no cliente vinculado à carteira
                    PermissaoCliente permissaoCliente = new PermissaoCliente();
                    if (permissaoCliente.RetornaAcessoCliente(idCarteira, HttpContext.Current.User.Identity.Name)) {
                        nome = carteira.str.Apelido;
                    }
                    else {
                        nome = "no_access";
                    }
                }
                else {
                    nome = "no_active";
                }
            }
        }
        e.Result = nome;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridConsulta_HtmlDataCellPrepared(object sender, ASPxGridViewTableDataCellEventArgs e) {
        if (e.DataColumn.FieldName == "RetornoDia") {
            decimal value = (decimal)e.GetValue("RetornoDia");
            if (value < 0)
                e.Cell.ForeColor = Color.Red;
        }
        else if (e.DataColumn.FieldName == "RetornoMes") {
            decimal value = (decimal)e.GetValue("RetornoMes");
            if (value < 0)
                e.Cell.ForeColor = Color.Red;
        }
        else if (e.DataColumn.FieldName == "RetornoAno") {
            decimal value = (decimal)e.GetValue("RetornoAno");
            if (value < 0)
                e.Cell.ForeColor = Color.Red;
        }
    }
        
    #region btnRun_Click, btnPDF_Click, btnExcel_Click

    /// <summary>
    /// Trata os campos Obrigatorios do Form
    /// </summary>
    private void TrataCamposObrigatorios() {
        #region Campos obrigatórios
        List<Control> controles = new List<Control>(new Control[] { this.btnEditCodigoCarteira,
            this.textDataInicio, this.textDataFim });

        if (base.TestaObrigatorio(controles) != "") {
            throw new Exception("Campos com * são obrigatórios!");
        }
        #endregion
    }

    new protected void btnRun_Click(object sender, EventArgs e) {
        this.TrataCamposObrigatorios();
        base.btnRun_Click(sender, e); // DataBind do Grid
    }

    new protected void btnPDF_Click(object sender, EventArgs e) {
        this.TrataCamposObrigatorios();
        this.SelecionaRelatorio(VisaoRelatorio.visaoPDF);
    }

    new protected void btnExcel_Click(object sender, EventArgs e) {
        this.TrataCamposObrigatorios();
        this.SelecionaRelatorio(VisaoRelatorio.visaoExcel);
    }

    /// <summary>
    /// Seleciona o Relatorio a Ser Executado
    /// </summary>
    /// <param name="visao">PDF, Excel ou Report</param>
    private void SelecionaRelatorio(string visao) {
        Response.BufferOutput = true;
        //
        // Salva os Parametros na Session
        Session["gridExportIRProventos"] = gridExport;
        Response.Redirect("~/Consultas/IRProventosExporta.aspx?Visao=" + visao);
    }

    #endregion
}