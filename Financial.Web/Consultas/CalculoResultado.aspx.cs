﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using Financial.Security;
using Financial.ContaCorrente;
using System.Text;
using DevExpress.Web;
using Financial.Web.Util;
using System.Drawing;
using Financial.Investidor;
using Financial.Bolsa;
using System.Collections.Generic;
using EntitySpaces.Interfaces;
using Financial.Common;
using Financial.Security.Enums;
using Financial.Web.Common;
using System.Threading;
using Financial.Fundo;

public partial class Consultas_CalculoResultado : ConsultaBasePage
{
    new protected void Page_Load(object sender, EventArgs e)
    {
        this.HasPopupCliente = true;
        btnEditCodigoCliente.Focus();
        base.Page_Load(sender, e);

        TrataTravamentoCampos();
    }

    #region DataSources
    protected void EsDSCalculoResultado_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        if (!Page.IsPostBack)
        {
            #region Condição Inicial
            gridConsulta.Visible = false;
            e.Collection = new CalculoResultadoCollection();
            #endregion
        }
        else
        {
            // Confere Campos Obrigatorios para o caso de Mudança de Pagina e Scroolbar
            CalculoResultadoCollection coll = new CalculoResultadoCollection();
            CalculoResultadoQuery calculoResultadoQuery = new CalculoResultadoQuery("calculoResultado");
            ClienteQuery clienteQuery = new ClienteQuery("cliente");
            TraderQuery traderQuery = new TraderQuery("trader");
            this.TrataCamposObrigatorios();

            calculoResultadoQuery.Select(calculoResultadoQuery, clienteQuery.Apelido.As("ApelidoCliente"), traderQuery.Nome.As("NomeTrader"));
            calculoResultadoQuery.InnerJoin(clienteQuery).On(clienteQuery.IdCliente.Equal(calculoResultadoQuery.IdCliente));
            calculoResultadoQuery.LeftJoin(traderQuery).On(clienteQuery.IdCliente.Equal(traderQuery.IdCarteira));
            calculoResultadoQuery.Where(calculoResultadoQuery.DataReferencia.Equal(Convert.ToDateTime(this.textData.Text)));

            if (!String.IsNullOrEmpty(btnEditCodigoCliente.Text))
                calculoResultadoQuery.Where(calculoResultadoQuery.IdCliente.Equal(Convert.ToInt32(this.btnEditCodigoCliente.Text)));

            coll.Load(calculoResultadoQuery);

            // Define Grid como Visivel
            gridConsulta.Visible = true;
            e.Collection = coll;
        }
    }

    protected void EsDSCliente_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        ClienteCollection clienteCollection = new ClienteCollection();
        clienteCollection.BuscaClientesComAcesso(HttpContext.Current.User.Identity.Name);

        e.Collection = clienteCollection;
    }

    #endregion

    /// <summary>
    /// 
    /// </summary>
    private void TrataTravamentoCampos()
    {
        Usuario usuario = new Usuario();
        usuario.BuscaUsuario(HttpContext.Current.User.Identity.Name);
        int tipoTrava = usuario.TipoTrava.Value;

        if (tipoTrava == (int)TipoTravaUsuario.TravaCliente)
        {
            btnEditCodigoCliente.Enabled = false;

            PermissaoCliente permissaoCliente = new PermissaoCliente();
            int? idCliente = permissaoCliente.RetornaClienteAssociado(HttpContext.Current.User.Identity.Name);

            if (idCliente.HasValue)
            {
                Cliente cliente = new Cliente();

                List<esQueryItem> campos = new List<esQueryItem>();
                campos.Add(cliente.Query.Apelido);
                cliente.LoadByPrimaryKey(campos, idCliente.Value);
                string apelido = cliente.Apelido;

                btnEditCodigoCliente.Text = Convert.ToString(idCliente.Value);
                textNome.Text = apelido;
            }
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void callbackCliente_Callback(object source, DevExpress.Web.CallbackEventArgs e)
    {
        string nome = "";
        if (!String.IsNullOrEmpty(e.Parameter) && e.Parameter != "null")
        {
            int idCliente = Convert.ToInt32(e.Parameter);
            Cliente cliente = new Cliente();
            cliente.LoadByPrimaryKey(idCliente);

            if (cliente.str.Apelido != "")
            {
                if (cliente.IsAtivo)
                {
                    //O permissionamento da cliente é dado direto no cliente vinculado à cliente
                    PermissaoCliente permissaoCliente = new PermissaoCliente();
                    if (permissaoCliente.RetornaAcessoClienteComControle(idCliente, HttpContext.Current.User.Identity.Name))
                    {
                        DateTime dataDia = cliente.DataDia.Value;
                        nome = cliente.str.Apelido + "|" + dataDia.ToShortDateString() + "|" + Thread.CurrentThread.CurrentCulture.Name;
                    }
                    else
                    {
                        nome = "no_access";
                    }
                }
                else
                {
                    nome = "no_active";
                }
            }
        }
        e.Result = nome;
    }

    #region btnRun_Click, btnPDF_Click, btnExcel_Click

    /// <summary>
    /// Trata os campos Obrigatorios do Form
    /// </summary>
    private void TrataCamposObrigatorios()
    {
        #region Campos obrigatórios
        List<Control> controles = new List<Control>(new Control[] { this.textData });

        if (base.TestaObrigatorio(controles) != "")
        {
            throw new Exception("Campos com * são obrigatórios!");
        }
        #endregion
    }

    new protected void btnRun_Click(object sender, EventArgs e)
    {
        this.TrataCamposObrigatorios();
        base.btnRun_Click(sender, e); // DataBind do Grid
    }

    new protected void btnPDF_Click(object sender, EventArgs e)
    {
        this.TrataCamposObrigatorios();
        this.SelecionaRelatorio(VisaoRelatorio.visaoPDF);
    }

    new protected void btnExcel_Click(object sender, EventArgs e)
    {
        this.TrataCamposObrigatorios();
        this.SelecionaRelatorio(VisaoRelatorio.visaoExcel);
    }

    /// <summary>
    /// Seleciona o Relatorio a Ser Executado
    /// </summary>
    /// <param name="visao">PDF, Excel ou Report</param>
    private void SelecionaRelatorio(string visao)
    {
        Response.BufferOutput = true;
        //
        // Salva os Parametros na Session
        Session["gridExportCalculoResultado"] = gridExport;
        Response.Redirect("~/Consultas/CalculoResultadoExporta.aspx?Visao=" + visao);
    }

    #endregion
}