﻿using System;
using System.Web;
using System.Globalization;
using System.IO;
using Financial.Relatorio;
using Financial.Web.Common;
using DevExpress.Web;

public partial class _VencimentosExporta : ConsultaBasePage {

    new protected void Page_Load(object sender, EventArgs e) {
        string visao = Request.QueryString["Visao"].ToString();

        this.gridExport = (ASPxGridViewExporter)Session["gridExportVencimentos"];
        Session.Remove("gridExportVencimentos");

        if (visao == VisaoRelatorio.visaoPDF) {
            #region PDF
            MemoryStream ms = new MemoryStream();
            
            //this.gridExport.GridView.Columns[0].Width = 1;
            //this.gridExport.GridView.Columns[1].Width = 1;
            //this.gridExport.GridView.Columns[2].Width = 1;
            //this.gridExport.GridView.Columns[3].Width = 1;
            //this.gridExport.GridView.Columns[4].Width = 1;
            //this.gridExport.GridView.Columns[5].Width = 1;
            //this.gridExport.GridView.Columns[6].Width = 1;

            this.gridExport.WritePdf(ms);
            ms.Seek(0, SeekOrigin.Begin);
            Response.ClearContent();
            Response.ClearHeaders();
            Response.Buffer = true;
            Response.Cache.SetCacheability(HttpCacheability.Private);
            Response.ContentType = "application/pdf";
            Response.AddHeader("Content-Length", ms.Length.ToString(CultureInfo.CurrentCulture));
            Response.AddHeader("Content-Disposition", String.Format("attachment;filename=gridConsultaVencimentos.pdf"));

            Response.BinaryWrite(ms.ToArray());
            ms.Close();
            HttpContext.Current.ApplicationInstance.CompleteRequest();
            Response.End();
            #endregion
        }
        else if (visao == VisaoRelatorio.visaoExcel) {
            #region Excel
            MemoryStream ms = new MemoryStream();
            this.gridExport.WriteXls(ms);
            ms.Seek(0, SeekOrigin.Begin);
            Response.ClearContent();
            Response.ClearHeaders();
            Response.Buffer = true;
            Response.Cache.SetCacheability(HttpCacheability.Private);
            Response.ContentType = "application/vnd.ms-excel";
            Response.AddHeader("Content-Length", ms.Length.ToString(CultureInfo.CurrentCulture));
            Response.AddHeader("Content-Disposition", String.Format("attachment;filename=gridConsultaVencimentos.xls"));

            Response.BinaryWrite(ms.ToArray());
            ms.Close();
            HttpContext.Current.ApplicationInstance.CompleteRequest();
            Response.End();

            #endregion
        }
    }
}