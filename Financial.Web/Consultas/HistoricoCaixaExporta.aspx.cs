﻿using System;
using System.Web;
using System.Globalization;
using System.IO;
using Financial.Relatorio;
using Financial.Web.Common;
using DevExpress.Web;

public partial class _HistoricoCaixaExporta : BasePage {

    class VisaoRelatorio {
        public static string visaoPDF = "PDF";
        public static string visaoExcel = "Excel";
    }

    new protected void Page_Load(object sender, EventArgs e) {
        string visao = Request.QueryString["Visao"].ToString();

        this.gridExport = (ASPxGridViewExporter)Session["gridExportHistoricoCaixa"];
        Session.Remove("gridExportHistoricoCaixa");

        if (visao == VisaoRelatorio.visaoPDF) {
            #region PDF
            MemoryStream ms = new MemoryStream();
            this.gridExport.WritePdf(ms);
            ms.Seek(0, SeekOrigin.Begin);
            Response.ClearContent();
            Response.ClearHeaders();
            Response.Buffer = true;
            Response.Cache.SetCacheability(HttpCacheability.Private);
            Response.ContentType = "application/rtf";
            Response.AddHeader("Content-Length", ms.Length.ToString(CultureInfo.CurrentCulture));
            Response.AddHeader("Content-Disposition", String.Format("attachment;filename=gridConsultaHistoricoCaixa.pdf"));

            Response.BinaryWrite(ms.ToArray());
            ms.Close();
            HttpContext.Current.ApplicationInstance.CompleteRequest();
            Response.End();           
            #endregion
        }
        else if (visao == VisaoRelatorio.visaoExcel) {
            #region Excel
            MemoryStream ms = new MemoryStream();
            this.gridExport.WriteXls(ms);            
            ms.Seek(0, SeekOrigin.Begin);
            Response.ClearContent();
            Response.ClearHeaders();
            Response.Buffer = true;
            Response.Cache.SetCacheability(HttpCacheability.Private);
            Response.ContentType = "application/vnd.ms-excel";
            Response.AddHeader("Content-Length", ms.Length.ToString(CultureInfo.CurrentCulture));
            Response.AddHeader("Content-Disposition", String.Format("attachment;filename=gridConsultaHistoricoCaixa.xls"));

            Response.BinaryWrite(ms.ToArray());
            ms.Close();
            HttpContext.Current.ApplicationInstance.CompleteRequest();
            Response.End();

            #endregion
        }
    }
}