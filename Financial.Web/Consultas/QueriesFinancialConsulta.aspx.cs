﻿using System;
using System.IO;
using System.Data;
using System.Data.OleDb;
using System.Configuration;
using System.Collections;
using System.Text;
using System.Xml;
using System.Drawing;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Korzh.WebControls.XControls;
using Korzh.EasyQuery;
using EntitySpaces.Core;
using EntitySpaces.Interfaces;
using Financial.Common;
using Financial.Fundo;
using System.Reflection;
using DevExpress.Web;
using DevExpress.Utils;
using System.Collections.Generic;
using Financial.Security;
using Financial.Web.Common;


public partial class Consultas_QueriesFinancialConsulta : Page {

    
    


    

    protected void Page_Load(object sender, EventArgs e) {



    
        

        

        

        InicializagridQuery();
        gridResultado.DataBind();
    }

    #region devexpress

    public void teste()
    {
        
    }

    protected void InicializagridQuery()
    {
        //ASPxGridView gridQuery = this.FindControl("gridQuery") as ASPxGridView;
        #region Properties básicas
        
        
        gridQuery.ClientInstanceName = "gridQuery";
        gridQuery.AutoGenerateColumns = false;
        gridQuery.Width = Unit.Percentage(100);
        gridQuery.SettingsBehavior.AllowSort = false;
        gridQuery.EnableViewState = true;
        gridQuery.EnableCallBacks = true;

        gridQuery.Settings.ShowFilterRowMenu = true;
        
        //((GridViewCommandColumn)gridQuery.Columns[0]).Width = Unit.Percentage(5);
        gridQuery.SettingsText.PopupEditFormCaption = "";
        gridQuery.SettingsEditing.Mode = GridViewEditingMode.PopupEditForm;
        gridQuery.SettingsEditing.PopupEditFormModal = true;
        gridQuery.SettingsEditing.PopupEditFormHorizontalAlign = PopupHorizontalAlign.WindowCenter;
        gridQuery.SettingsEditing.PopupEditFormVerticalAlign = PopupVerticalAlign.WindowCenter;



        //Pager
        gridQuery.SettingsPager.PageSize = 30;

        

        //Styles
        gridQuery.Styles.Header.SortingImageSpacing = Unit.Pixel(5);
        gridQuery.Styles.AlternatingRow.Enabled = DefaultBoolean.True;
        gridQuery.Styles.Cell.Wrap = DefaultBoolean.True;
        gridQuery.Styles.CommandColumn.Cursor = "hand";

        

        //((GridViewCommandColumn)gridQuery.Columns[0]).Width = Unit.Percentage(10);
        //((GridViewCommandColumn)gridQuery.Columns[0]).UpdateButton.Image.Url = "~/imagens/ico_form_ok_inline.gif";
        //((GridViewCommandColumn)gridQuery.Columns[0]).CancelButton.Image.Url = "~/imagens/ico_form_back_inline.gif";
        //((GridViewCommandColumn)gridQuery.Columns[0]).UpdateButton.Text = "Salvar";
        //((GridViewCommandColumn)gridQuery.Columns[0]).CancelButton.Text = "Cancelar";
        
        //gridQuery.SettingsEditing.Mode = GridViewEditingMode.Inline;

        
                                


        #endregion

        
    }

    protected void callbackErro_Callback(object source, DevExpress.Web.CallbackEventArgs e)
    {
        e.Result = ""; 
        ASPxTextBox textQuery = gridQuery.FindEditFormTemplateControl("textQuery") as ASPxTextBox;
        if (!textQuery.Text.StartsWith("SELECT", StringComparison.InvariantCultureIgnoreCase))
        {
            e.Result = "Apenas consultas SELECT simples são aceitas.";
        }
    }

    protected void CheckBoxSelectAll(object sender, EventArgs e)
    {
        ASPxCheckBox chk = sender as ASPxCheckBox;
        ASPxGridView grid = (chk.NamingContainer as GridViewHeaderTemplateContainer).Grid;
        chk.Checked = (grid.Selection.Count == grid.VisibleRowCount);
    }

    protected void EsDSQuery_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        QueryCustomCollection coll = new QueryCustomCollection();
        coll.LoadAll();
        e.Collection = coll;

        
    }



    protected void gridResultado_CustomCallback(object sender, ASPxGridViewCustomCallbackEventArgs e)
    {   
        CarregaGrid();
        gridResultado.DataBind();
    }


    protected void gridResultado_OnBeforePerformDataSelect(object sender, EventArgs e)
    {        
        CarregaGrid();
    }
    




    protected void gridQuery_CustomCallback(object sender, ASPxGridViewCustomCallbackEventArgs e)
    {
        if (e.Parameters == "btnRun")
        {
            
        }


        if (e.Parameters == "btnDelete")
        {
            List<object> keyValuesId = gridQuery.GetSelectedFieldValues(QueryCustomMetadata.ColumnNames.IdQuery);
            for (int i = 0; i < keyValuesId.Count; i++)
            {
                int idQuery = Convert.ToInt32(keyValuesId[i]);
                QueryCustom q = new QueryCustom();
                if (q.LoadByPrimaryKey(idQuery))
                {
                    q.MarkAsDeleted();
                    q.Save();
                }
            }
        }

        gridQuery.DataBind();
        gridQuery.Selection.UnselectAll();
        gridQuery.CancelEdit();
    }

    protected void gridQuery_RowInserting(object sender, DevExpress.Web.Data.ASPxDataInsertingEventArgs e)
    {
        QueryCustom q = new QueryCustom();
        
        ASPxTextBox textNome = gridQuery.FindEditFormTemplateControl("textNome") as ASPxTextBox;
        ASPxTextBox textDescricao = gridQuery.FindEditFormTemplateControl("textDescricao") as ASPxTextBox;
        ASPxTextBox textQuery = gridQuery.FindEditFormTemplateControl("textQuery") as ASPxTextBox;

        q.Nome = textNome.Text;
        q.Descricao = textDescricao.Text;
        q.QueryCustom = textQuery.Text;

        q.Save();

        e.Cancel = true;
        gridQuery.CancelEdit();
        gridQuery.DataBind();
    }

    protected void gridQuery_RowUpdating(object sender, DevExpress.Web.Data.ASPxDataUpdatingEventArgs e)
    {
        QueryCustom q = new QueryCustom();
        int idQuery = (int)e.Keys[0];

        if (q.LoadByPrimaryKey(idQuery))
        {
            q.Nome = e.NewValues[QueryCustomMetadata.ColumnNames.Nome].ToString();
            q.Descricao = e.NewValues[QueryCustomMetadata.ColumnNames.Descricao].ToString();
            q.QueryCustom = e.NewValues[QueryCustomMetadata.ColumnNames.QueryCustom].ToString();

            q.Save();
        }

        e.Cancel = true;
        gridQuery.CancelEdit();
        gridQuery.DataBind();
    }





    
    #endregion


    public void CarregaGrid()
    {

        bool existeConsulta = false;
        for (int i = 0; i < EntitySpaces.Interfaces.esConfigSettings.ConnectionInfo.Connections.Count; i++)
        {
            if (EntitySpaces.Interfaces.esConfigSettings.ConnectionInfo.Connections[i].Name == "Financial_Consulta")
            {
                existeConsulta = true;
                break;
            }
        }

        if (!existeConsulta)
        {
            ClientScript.RegisterClientScriptBlock(this.GetType(), "ClosePopUp", "<script>alert('Não foi possível encontrar a conexão Financial_Consulta no Web.Config.');</script>");
            ClientScript.RegisterStartupScript(typeof(Page), "closePage", "window.close();", true);
            return;
        }
        
        String query = SelectField.Value;
        if (query.StartsWith("SELECT", StringComparison.InvariantCultureIgnoreCase))
        {
            esUtility util = new esUtility();
            util.ConnectionName = "Financial_Consulta";

            DataTable d = util.FillDataTable(esQueryType.Text,query.Trim());
            gridResultado.DataSource = d;
            gridResultado.Columns.Clear();
            gridResultado.AutoGenerateColumns = true;
            gridResultado.KeyFieldName = String.Empty;
        }
    }



    protected void UpdateManualQueries_Click(object sender, EventArgs e)
    {
        CarregaGrid();
    }

    protected void UpdateResultBtn_Click(object sender, EventArgs e) 
    {
        CarregaGrid();        
    }


    protected void ExportExcelBtn_Click(object sender, EventArgs e) {
        ASPxGridViewExporter gridExport = this.FindControl("gridExport") as ASPxGridViewExporter;
        gridExport.WriteXlsToResponse();
    }




}

