﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using Financial.Security;
using Financial.ContaCorrente;
using System.Text;
using DevExpress.Web;
using Financial.Web.Util;
using System.Drawing;
using Financial.Investidor;
using Financial.Bolsa;
using System.Collections.Generic;
using EntitySpaces.Interfaces;
using Financial.Common;
using Financial.InvestidorCotista;
using Financial.Fundo;
using Financial.Security.Enums;
using Financial.Web.Common;

public partial class Consultas_PosicaoCotistaPorFundo : ConsultaBasePage {
    new protected void Page_Load(object sender, EventArgs e) {
        this.HasPopupCarteira = true;
        btnEditCodigoCarteira.Focus();
        base.Page_Load(sender, e);

        this.PersonalizaGridConsulta();

        TrataTravamentoCampos();
    }
    
    /// <summary>
    /// Configura Grid de Consulta
    /// </summary>
    private void PersonalizaGridConsulta() {
        // Enable ViewState
        this.gridConsulta.EnableViewState = true;
    }

    #region DataSources
    protected void EsDSPosicaoCotista_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e) {
        if (!Page.IsPostBack) {
            gridConsulta.Visible = false;
            this.btnMemoriaCalculo.Visible = false;
            e.Collection = new PosicaoBolsaCollection();
        }
        else {

            // Confere Campos Obrigatorios para o caso de Mudança de Pagina e Scroolbar
            this.TrataCamposObrigatorios();
            //

            //Checa se busca da posição atual ou histórica
            int idCliente = Convert.ToInt32(btnEditCodigoCarteira.Text);
            DateTime data = Convert.ToDateTime(textData.Text);

            Cliente cliente = new Cliente();
            List<esQueryItem> campos = new List<esQueryItem>();
            campos.Add(cliente.Query.DataDia);
            cliente.LoadByPrimaryKey(idCliente);

            DateTime dataDia = cliente.DataDia.Value;

            bool historico = false;
            if (DateTime.Compare(data, dataDia) < 0) {
                historico = true;
            }
            //

            Usuario usuario = new Usuario();
            usuario.BuscaUsuario(Context.User.Identity.Name);
            int idUsuario = usuario.IdUsuario.Value;

            if (historico) {
                PosicaoCotistaHistoricoQuery posicaoCotistaHistoricoQuery = new PosicaoCotistaHistoricoQuery("P");
                PermissaoCotistaQuery permissaoCotistaQuery = new PermissaoCotistaQuery("R");

                posicaoCotistaHistoricoQuery.InnerJoin(permissaoCotistaQuery).On(permissaoCotistaQuery.IdCotista == posicaoCotistaHistoricoQuery.IdCotista);
                CotistaQuery cotistaQuery = new CotistaQuery("S");
                posicaoCotistaHistoricoQuery.InnerJoin(cotistaQuery).On(posicaoCotistaHistoricoQuery.IdCotista == cotistaQuery.IdCotista);

                posicaoCotistaHistoricoQuery.Where(permissaoCotistaQuery.IdUsuario.Equal(idUsuario));
                posicaoCotistaHistoricoQuery.Where(posicaoCotistaHistoricoQuery.IdCarteira.Equal(Convert.ToInt32(btnEditCodigoCarteira.Text)));
                posicaoCotistaHistoricoQuery.Where(posicaoCotistaHistoricoQuery.DataHistorico.Equal(Convert.ToDateTime(textData.Text)));
                posicaoCotistaHistoricoQuery.Where(posicaoCotistaHistoricoQuery.Quantidade.NotEqual(0) | posicaoCotistaHistoricoQuery.QtdePendenteLiquidacao.NotEqual(0));

                posicaoCotistaHistoricoQuery.OrderBy(posicaoCotistaHistoricoQuery.IdCotista.Ascending,
                                                     posicaoCotistaHistoricoQuery.DataAplicacao.Descending);

                PosicaoCotistaHistoricoCollection posicaoCotistaHistoricoCollection = new PosicaoCotistaHistoricoCollection();
                posicaoCotistaHistoricoCollection.Load(posicaoCotistaHistoricoQuery);

                e.Collection = posicaoCotistaHistoricoCollection;
            }
            else {
                PosicaoCotistaQuery posicaoCotistaQuery = new PosicaoCotistaQuery("P");
                PermissaoCotistaQuery permissaoCotistaQuery = new PermissaoCotistaQuery("R");

                posicaoCotistaQuery.InnerJoin(permissaoCotistaQuery).On(permissaoCotistaQuery.IdCotista == posicaoCotistaQuery.IdCotista);
                CotistaQuery cotistaQuery = new CotistaQuery("S");
                posicaoCotistaQuery.InnerJoin(cotistaQuery).On(posicaoCotistaQuery.IdCotista == cotistaQuery.IdCotista);

                posicaoCotistaQuery.Where(permissaoCotistaQuery.IdUsuario.Equal(idUsuario));
                posicaoCotistaQuery.Where(posicaoCotistaQuery.IdCarteira.Equal(Convert.ToInt32(btnEditCodigoCarteira.Text)));
                posicaoCotistaQuery.Where(posicaoCotistaQuery.Quantidade.NotEqual(0) | posicaoCotistaQuery.QtdePendenteLiquidacao.NotEqual(0));

                posicaoCotistaQuery.OrderBy(posicaoCotistaQuery.IdCotista.Ascending,
                                            posicaoCotistaQuery.DataAplicacao.Descending);

                PosicaoCotistaCollection posicaoCotistaCollection = new PosicaoCotistaCollection();
                posicaoCotistaCollection.Load(posicaoCotistaQuery);

                e.Collection = posicaoCotistaCollection;
            }

            gridConsulta.Visible = true;

            // Visibilidade do botão Memoria de Calculo
            this.btnMemoriaCalculo.Visible = e.Collection.Count != 0;
        }

    }

    protected void EsDSCarteira_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e) {
        CarteiraCollection carteiraCollection = new CarteiraCollection();
        carteiraCollection.BuscaCarteirasComAcessoTodas(HttpContext.Current.User.Identity.Name);

        e.Collection = carteiraCollection;
    }

    protected void EsDSCotista_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e) {
        CotistaCollection cotistaCollection = new CotistaCollection();
        cotistaCollection.BuscaCotistasComAcesso(HttpContext.Current.User.Identity.Name);

        e.Collection = cotistaCollection;
    }
    #endregion

    /// <summary>
    /// 
    /// </summary>
    private void TrataTravamentoCampos() {
        Usuario usuario = new Usuario();
        usuario.BuscaUsuario(HttpContext.Current.User.Identity.Name);
        int tipoTrava = usuario.TipoTrava.Value;

        if (tipoTrava == (int)TipoTravaUsuario.TravaCliente) {
            btnEditCodigoCarteira.Enabled = false;

            PermissaoCliente permissaoCliente = new PermissaoCliente();
            int? idCliente = permissaoCliente.RetornaClienteAssociado(HttpContext.Current.User.Identity.Name);

            if (idCliente.HasValue) {
                Cliente cliente = new Cliente();

                List<esQueryItem> campos = new List<esQueryItem>();
                campos.Add(cliente.Query.Apelido);
                cliente.LoadByPrimaryKey(campos, idCliente.Value);
                string apelido = cliente.Apelido;

                btnEditCodigoCarteira.Text = Convert.ToString(idCliente.Value);
                textNome.Text = apelido;
            }
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void ASPxCallback1_Callback(object source, DevExpress.Web.CallbackEventArgs e) {
        string nome = "";
        if (!String.IsNullOrEmpty(e.Parameter) && e.Parameter != "null") {
            int idCarteira = Convert.ToInt32(e.Parameter);
            Carteira carteira = new Carteira();
            carteira.LoadByPrimaryKey(idCarteira);

            if (carteira.str.Apelido != "") {
                if (carteira.IsAtivo) {
                    //O permissionamento da carteira é dado direto no cliente vinculado à carteira
                    PermissaoCliente permissaoCliente = new PermissaoCliente();
                    if (permissaoCliente.RetornaAcessoCliente(idCarteira, HttpContext.Current.User.Identity.Name)) {
                        nome = carteira.str.Apelido;
                    }
                    else {
                        nome = "no_access";
                    }
                }
                else {
                    nome = "no_active";
                }
            }
        }
        e.Result = nome;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void callbackErro_Callback(object source, DevExpress.Web.CallbackEventArgs e) {
        e.Result = "";

        List<object> keyValuesIdPosicao = this.gridConsulta.GetSelectedFieldValues(PosicaoCotistaHistoricoMetadata.ColumnNames.IdPosicao);
        if (keyValuesIdPosicao.Count == 0) {
            e.Result = "Escolha uma Posição.";
            return;
        }
        else { // Se não deu erro - Processa Memória Calculo
            #region Processa Memória Cálculo
            List<int> idPosicao = keyValuesIdPosicao.ConvertAll<int>(delegate(object v) { return Convert.ToInt32(v); });

            this.GeraMemoriaCalculo(idPosicao);
            #endregion
        }
    }

    /// <summary>
    /// Gera Memoria Calculo
    /// </summary>
    /// <param name="idPosicao">Lista de Posições Selecionadas no Grid</param>
    private void GeraMemoriaCalculo(List<int> idPosicao) {
        //Response.Redirect("~/Relatorios/RendaFixa/ReportProjecaoTributoRendaFixa.ashx?idcarteira=106");
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridConsulta_HtmlDataCellPrepared(object sender, ASPxGridViewTableDataCellEventArgs e) {
        if (e.DataColumn.FieldName == "ValorIR" || e.DataColumn.FieldName == "ValorIOF" ||
            e.DataColumn.FieldName == "ValorPerformance") {
            e.Cell.ForeColor = Color.Red;
        }
    }

    #region btnRun_Click, btnPDF_Click, btnExcel_Click

    /// <summary>
    /// Trata os campos Obrigatorios do Form
    /// </summary>
    private void TrataCamposObrigatorios() {
        #region Campos obrigatórios
        List<Control> controles = new List<Control>(new Control[] { 
            this.btnEditCodigoCarteira, this.textData });

        if (base.TestaObrigatorio(controles) != "") {
            throw new Exception("Campos com * são obrigatórios!");
        }
        #endregion
    }

    new protected void btnRun_Click(object sender, EventArgs e) {
        this.TrataCamposObrigatorios();
        base.btnRun_Click(sender, e); // DataBind do Grid
    }

    new protected void btnPDF_Click(object sender, EventArgs e) {
        this.TrataCamposObrigatorios();
        this.SelecionaRelatorio(VisaoRelatorio.visaoPDF);
    }

    new protected void btnExcel_Click(object sender, EventArgs e) {
        this.TrataCamposObrigatorios();
        this.SelecionaRelatorio(VisaoRelatorio.visaoExcel);
    }
   
    /// <summary>
    /// Seleciona o Relatorio a Ser Executado
    /// </summary>
    /// <param name="visao">PDF, Excel ou Report</param>
    private void SelecionaRelatorio(string visao) {
        Response.BufferOutput = true;
        //
        // Salva os Parametros na Session
        Session["gridExportPosicaoCotistaPorFundo"] = gridExport;
        Response.Redirect("~/Consultas/PosicaoCotistaPorFundoExporta.aspx?Visao=" + visao);
    }

    #endregion
}