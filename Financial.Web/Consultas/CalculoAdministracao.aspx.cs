﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using Financial.Security;
using Financial.ContaCorrente;
using System.Text;
using DevExpress.Web;
using Financial.Web.Util;
using System.Drawing;
using Financial.Investidor;
using Financial.Fundo;
using Financial.Investidor.Enums;
using System.Collections.Generic;
using Financial.Web.Common;

public partial class Consultas_CalculoAdministracao : ConsultaBasePage {
    
    new protected void Page_Load(object sender, EventArgs e) {
        btnEditCodigo.Focus();
        base.Page_Load(sender, e);
    }

    #region DataSources
    protected void EsDSCalculoAdministracao_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e) {
        if (!Page.IsPostBack) {
            gridConsulta.Visible = false;
            e.Collection = new SaldoCaixaCollection();
        }
        else {
            // Confere Campos Obrigatorios para o caso de Mudança de Pagina e Scroolbar
            this.TrataCamposObrigatorios();
            //

            CalculoAdministracaoHistoricoQuery calculoAdministracaoHistoricoQuery = new CalculoAdministracaoHistoricoQuery("A");
            CarteiraQuery carteiraQuery = new CarteiraQuery("C");

            calculoAdministracaoHistoricoQuery.Select(carteiraQuery.Apelido,
                            calculoAdministracaoHistoricoQuery.DataHistorico,
                            calculoAdministracaoHistoricoQuery.DataFimApropriacao,
                            calculoAdministracaoHistoricoQuery.DataPagamento,
                            calculoAdministracaoHistoricoQuery.ValorDia,
                            calculoAdministracaoHistoricoQuery.ValorAcumulado);
            calculoAdministracaoHistoricoQuery.Where(calculoAdministracaoHistoricoQuery.IdTabela.Equal(Convert.ToInt32(hiddenIdTabela.Text)));
            calculoAdministracaoHistoricoQuery.Where(calculoAdministracaoHistoricoQuery.DataHistorico.GreaterThanOrEqual(Convert.ToDateTime(textDataInicio.Text)));
            calculoAdministracaoHistoricoQuery.Where(calculoAdministracaoHistoricoQuery.DataHistorico.LessThanOrEqual(Convert.ToDateTime(textDataFim.Text)));
            calculoAdministracaoHistoricoQuery.InnerJoin(carteiraQuery).On(calculoAdministracaoHistoricoQuery.IdCarteira == carteiraQuery.IdCarteira);
            calculoAdministracaoHistoricoQuery.OrderBy(calculoAdministracaoHistoricoQuery.DataHistorico.Descending);
            
            CalculoAdministracaoHistoricoCollection calculoAdministracaoHistoricoCollection = new CalculoAdministracaoHistoricoCollection();
            calculoAdministracaoHistoricoCollection.Load(calculoAdministracaoHistoricoQuery);
            
            //
            gridConsulta.Visible = true;
            e.Collection = calculoAdministracaoHistoricoCollection;
        }
    }

    protected void EsDSTabelaTaxas_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e) {
        TabelaTaxaAdministracaoQuery tabelaTaxasQuery = new TabelaTaxaAdministracaoQuery("T");
        CarteiraQuery carteiraQuery = new CarteiraQuery("C");
        CadastroTaxaAdministracaoQuery cadastroTaxasQuery = new CadastroTaxaAdministracaoQuery("A");
        PermissaoClienteQuery permissaoClienteQuery = new PermissaoClienteQuery("P");
        UsuarioQuery usuarioQuery = new UsuarioQuery("U");
        ClienteQuery clienteQuery = new ClienteQuery("E");
        tabelaTaxasQuery.Select(tabelaTaxasQuery.IdTabela,
                              tabelaTaxasQuery.DataReferencia,
                              carteiraQuery.IdCarteira,
                              carteiraQuery.Apelido,
                              cadastroTaxasQuery.IdCadastro,
                              cadastroTaxasQuery.Descricao);
        tabelaTaxasQuery.InnerJoin(cadastroTaxasQuery).On(tabelaTaxasQuery.IdCadastro == cadastroTaxasQuery.IdCadastro);
        tabelaTaxasQuery.InnerJoin(carteiraQuery).On(tabelaTaxasQuery.IdCarteira == carteiraQuery.IdCarteira);
        tabelaTaxasQuery.InnerJoin(permissaoClienteQuery).On(permissaoClienteQuery.IdCliente == carteiraQuery.IdCarteira);
        tabelaTaxasQuery.InnerJoin(usuarioQuery).On(permissaoClienteQuery.IdUsuario == usuarioQuery.IdUsuario);
        tabelaTaxasQuery.InnerJoin(clienteQuery).On(carteiraQuery.IdCarteira == clienteQuery.IdCliente);
        tabelaTaxasQuery.Where(usuarioQuery.Login == HttpContext.Current.User.Identity.Name,
                               clienteQuery.TipoControle.NotEqual(TipoControleCliente.ApenasCotacao),
                               clienteQuery.StatusAtivo.Equal(StatusAtivoCliente.Ativo));
        tabelaTaxasQuery.OrderBy(clienteQuery.Apelido.Ascending);

        TabelaTaxaAdministracaoCollection coll = new TabelaTaxaAdministracaoCollection();
        coll.Load(tabelaTaxasQuery);

        // Assign the esDataSourceSelectEvenArgs Collection property
        e.Collection = coll;
    }
    #endregion

    protected void ASPxCallback1_Callback(object source, DevExpress.Web.CallbackEventArgs e) {
        string texto = "";
        if (!String.IsNullOrEmpty(e.Parameter) && e.Parameter != "null") {
            #region Procura o idTabela se o Parameter for do tipo Int
            //
            //int idTabela;
            //if (int.TryParse(e.Parameter, out idTabela)) {
            int idTabela = Convert.ToInt32(e.Parameter);
            TabelaTaxaAdministracaoQuery tabelaTaxas = new TabelaTaxaAdministracaoQuery("T");
            CarteiraQuery carteira = new CarteiraQuery("C");
            CadastroTaxaAdministracaoQuery cadastroTaxas = new CadastroTaxaAdministracaoQuery("P");
            tabelaTaxas.Select(tabelaTaxas.IdTabela,
                                  tabelaTaxas.DataReferencia,
                                  carteira.IdCarteira,
                                  carteira.Apelido,
                                  cadastroTaxas.IdCadastro,
                                  cadastroTaxas.Descricao);
            tabelaTaxas.InnerJoin(cadastroTaxas).On(tabelaTaxas.IdCadastro == cadastroTaxas.IdCadastro);
            tabelaTaxas.InnerJoin(carteira).On(tabelaTaxas.IdCarteira == carteira.IdCarteira);
            tabelaTaxas.Where(tabelaTaxas.IdTabela == idTabela);

            TabelaTaxaAdministracaoCollection tabelaTaxaAdministracaoCollection = new TabelaTaxaAdministracaoCollection();
            tabelaTaxaAdministracaoCollection.Load(tabelaTaxas);

            //
            if (tabelaTaxaAdministracaoCollection.HasData) {
                //int idTabelaAux = (int)tabelaTaxaAdministracaoCollection[0].GetColumn(TabelaTaxaAdministracaoMetadata.ColumnNames.IdTabela);                
                string nome = (string)tabelaTaxaAdministracaoCollection[0].GetColumn(CarteiraMetadata.ColumnNames.Apelido);
                string descricao = (string)tabelaTaxaAdministracaoCollection[0].GetColumn(CadastroTaxaAdministracaoMetadata.ColumnNames.Descricao);
                DateTime dataReferencia = (DateTime)tabelaTaxaAdministracaoCollection[0].GetColumn(TabelaTaxaAdministracaoMetadata.ColumnNames.DataReferencia);

                texto = ' ' + nome + " ->   " + labelTaxa.Text + descricao + " ->   " + labelDataReferencia.Text + dataReferencia.ToString("d");
            }
            //}
            #endregion
        }
        //
        e.Result = texto;
    }

    protected void gridTabela_CustomDataCallback(object sender, DevExpress.Web.ASPxGridViewCustomDataCallbackEventArgs e) {
        e.Result = gridTabela.GetRowValues(Convert.ToInt32(e.Parameters), "IdTabela") +
                        "|" + gridTabela.GetRowValues(Convert.ToInt32(e.Parameters), "IdCarteira");
    }

    protected void gridTabela_HtmlRowCreated(object sender, DevExpress.Web.ASPxGridViewTableRowEventArgs e) {
        e.Row.Attributes.Add("onmouseover", "backgroundColor = this.style.backgroundColor; this.style.backgroundColor='#E0E9EB';");
        e.Row.Attributes.Add("onmouseout", "this.style.backgroundColor=backgroundColor;");
    }

    #region btnRun_Click, btnPDF_Click, btnExcel_Click
    
    /// <summary>
    /// Trata os campos Obrigatorios do Form
    /// </summary>
    private void TrataCamposObrigatorios() {
        #region Campos obrigatórios
        List<Control> controles = new List<Control>(new Control[] { 
            this.btnEditCodigo, this.textDataInicio, this.textDataFim });

        if (base.TestaObrigatorio(controles) != "") {
            throw new Exception("Campos com * são obrigatórios!");
        }        
        #endregion
    }
    
    new protected void btnRun_Click(object sender, EventArgs e) {        
        this.TrataCamposObrigatorios();               
        base.btnRun_Click(sender, e); // DataBind do Grid
    }
  
    new protected void btnPDF_Click(object sender, EventArgs e) {
        this.TrataCamposObrigatorios();
        this.SelecionaRelatorio(VisaoRelatorio.visaoPDF);
    }

    new protected void btnExcel_Click(object sender, EventArgs e) {
        this.TrataCamposObrigatorios();
        this.SelecionaRelatorio(VisaoRelatorio.visaoExcel);
    }

    /// <summary>
    /// Seleciona o Relatorio a Ser Executado
    /// </summary>
    /// <param name="visao">PDF, Excel ou Report</param>
    private void SelecionaRelatorio(string visao) {
        Response.BufferOutput = true;
        //
        // Salva os Parametros na Session
        Session["gridExportCalculoAdministracao"] = gridExport;
        Response.Redirect("~/Consultas/CalculoAdministracaoExporta.aspx?Visao=" + visao);
    }

    #endregion       
}