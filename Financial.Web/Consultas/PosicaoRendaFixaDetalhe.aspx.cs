﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using Financial.Security;
using Financial.ContaCorrente;
using System.Text;
using DevExpress.Web;
using Financial.Web.Util;
using System.Drawing;
using Financial.Investidor;
using Financial.RendaFixa;
using System.Collections.Generic;
using EntitySpaces.Interfaces;
using Financial.Common;
using Financial.Security.Enums;
using Financial.Web.Common;
using System.Threading;

public partial class Consultas_PosicaoRendaFixaDetalhe : ConsultaBasePage
{
    new protected void Page_Load(object sender, EventArgs e)
    {
        this.HasPopupCliente = true;
        btnEditCodigoCliente.Focus();
        base.Page_Load(sender, e);

        //Pager para Testar
        //this.gridConsulta.SettingsPager.PageSize = 2;
        //this.gridConsulta.Settings.VerticalScrollableHeight = 100;

        TrataTravamentoCampos();
    }

    #region DataSources
    protected void EsDSPosicaoRendaFixaDetalhe_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        if (!Page.IsPostBack)
        {
            #region Condição Inicial
            gridConsulta.Visible = false;
            e.Collection = new PosicaoRendaFixaDetalheCollection();
            #endregion
        }
        else
        {
            // Confere Campos Obrigatorios para o caso de Mudança de Pagina e Scroolbar
            e.Collection = new PosicaoRendaFixaCollection();
            this.TrataCamposObrigatorios();

            TituloRendaFixaQuery tituloRendaFixaQuery = new TituloRendaFixaQuery("T");

            if (!String.IsNullOrEmpty(btnEditCodigoCliente.Text.Trim()))
            {
                #region Escolhido Apenas 1 Cliente
                int idCliente = Convert.ToInt32(btnEditCodigoCliente.Text);
                                
                PosicaoRendaFixaDetalheQuery posicaoRendaFixaDetalheQuery = new PosicaoRendaFixaDetalheQuery("P");

                PosicaoRendaFixaDetalheCollection posicaoRendaFixaDetalheCollection = new PosicaoRendaFixaDetalheCollection();
                posicaoRendaFixaDetalheQuery.Select(posicaoRendaFixaDetalheQuery, tituloRendaFixaQuery.DescricaoCompleta);
                posicaoRendaFixaDetalheQuery.InnerJoin(tituloRendaFixaQuery).On(tituloRendaFixaQuery.IdTitulo == posicaoRendaFixaDetalheQuery.IdTitulo);
                posicaoRendaFixaDetalheQuery.Where(posicaoRendaFixaDetalheQuery.IdCliente == Convert.ToInt32(btnEditCodigoCliente.Text));
                posicaoRendaFixaDetalheQuery.Where(posicaoRendaFixaDetalheQuery.DataHistorico == Convert.ToDateTime(textData.Text));

                posicaoRendaFixaDetalheQuery.OrderBy(posicaoRendaFixaDetalheQuery.IdTitulo.Ascending);
                posicaoRendaFixaDetalheCollection.Load(posicaoRendaFixaDetalheQuery);
                #endregion
                e.Collection = posicaoRendaFixaDetalheCollection;                
            }
            else
            {
                #region Varios Clientes
                ClienteCollection clienteCollection = new ClienteCollection();
                clienteCollection.BuscaClientesComAcesso(HttpContext.Current.User.Identity.Name);

                if (clienteCollection.Count == 0)
                { // Se nao tem Cliente com Acesso
                    e.Collection = new PosicaoRendaFixaCollection();
                }
                else
                {
                    DateTime data = Convert.ToDateTime(textData.Text);
                    // Collections usadas para o Bindind
                    PosicaoRendaFixaDetalheCollection pBinding = new PosicaoRendaFixaDetalheCollection();
                    pBinding.CreateColumnsForBinding();
                    pBinding.AddColumn("DescricaoCompleta", typeof(System.String));

                    // Para Cada Cliente de Acordo com a DataDia Consulta em Posicao ou PosicaoHistorico
                    for (int i = 0; i < clienteCollection.Count; i++)
                    {
                        int idClienteAux = clienteCollection[i].IdCliente.Value;

                        Cliente cliente = new Cliente();
                        List<esQueryItem> campos = new List<esQueryItem>();
                        campos.Add(cliente.Query.DataDia);
                        cliente.LoadByPrimaryKey(campos, idClienteAux);

                        PosicaoRendaFixaDetalheCollection posicaoRendaFixaDetalheCollection = new PosicaoRendaFixaDetalheCollection();

                        PosicaoRendaFixaDetalheQuery posicaoRendaFixaDetalheQuery = new PosicaoRendaFixaDetalheQuery("P");
                        posicaoRendaFixaDetalheQuery.Select(posicaoRendaFixaDetalheQuery, tituloRendaFixaQuery.DescricaoCompleta);
                        posicaoRendaFixaDetalheQuery.InnerJoin(tituloRendaFixaQuery).On(tituloRendaFixaQuery.IdTitulo == posicaoRendaFixaDetalheQuery.IdTitulo);
                        posicaoRendaFixaDetalheQuery.Where(posicaoRendaFixaDetalheQuery.IdCliente == idClienteAux,
                                                           posicaoRendaFixaDetalheQuery.DataHistorico.Equal(data));

                        posicaoRendaFixaDetalheCollection.Load(posicaoRendaFixaDetalheQuery);
                        //
                        for (int j = 0; j < posicaoRendaFixaDetalheCollection.Count; j++)
                        {
                            PosicaoRendaFixaDetalhe p = pBinding.AddNew();
                            p.IdCliente = posicaoRendaFixaDetalheCollection[j].IdCliente;
                            p.IdTitulo = posicaoRendaFixaDetalheCollection[j].IdTitulo;
                            p.QuantidadeBloqueada = posicaoRendaFixaDetalheCollection[j].QuantidadeBloqueada;
                            p.Motivo = posicaoRendaFixaDetalheCollection[j].Motivo;
                            p.SetColumn(TituloRendaFixaMetadata.ColumnNames.DescricaoCompleta, posicaoRendaFixaDetalheCollection[j].GetColumn(TituloRendaFixaMetadata.ColumnNames.DescricaoCompleta));
                        }                         
                    }
                    pBinding.Sort = PosicaoRendaFixaDetalheMetadata.ColumnNames.IdCliente + " ASC";
                    e.Collection = pBinding;
                }
                #endregion
            }

            // Define Grid como Visivel
            gridConsulta.Visible = true;
        }
    }

    protected void EsDSCliente_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        ClienteCollection clienteCollection = new ClienteCollection();
        clienteCollection.BuscaClientesComAcesso(HttpContext.Current.User.Identity.Name);

        e.Collection = clienteCollection;
    }

    protected void EsDSTituloRendaFixa_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        TituloRendaFixaCollection coll = new TituloRendaFixaCollection();

        coll.Query.OrderBy(coll.Query.Descricao.Ascending);
        coll.LoadAll();

        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }
    #endregion

    /// <summary>
    /// 
    /// </summary>
    private void TrataTravamentoCampos()
    {
        Usuario usuario = new Usuario();
        usuario.BuscaUsuario(HttpContext.Current.User.Identity.Name);
        int tipoTrava = usuario.TipoTrava.Value;

        if (tipoTrava == (int)TipoTravaUsuario.TravaCliente)
        {
            btnEditCodigoCliente.Enabled = false;

            PermissaoCliente permissaoCliente = new PermissaoCliente();
            int? idCliente = permissaoCliente.RetornaClienteAssociado(HttpContext.Current.User.Identity.Name);

            if (idCliente.HasValue)
            {
                Cliente cliente = new Cliente();

                List<esQueryItem> campos = new List<esQueryItem>();
                campos.Add(cliente.Query.Apelido);
                cliente.LoadByPrimaryKey(campos, idCliente.Value);
                string apelido = cliente.Apelido;

                btnEditCodigoCliente.Text = Convert.ToString(idCliente.Value);
                textNome.Text = apelido;
            }
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void ASPxCallback1_Callback(object source, DevExpress.Web.CallbackEventArgs e)
    {
        string nome = "";
        if (!String.IsNullOrEmpty(e.Parameter) && e.Parameter != "null")
        {
            int idCliente = Convert.ToInt32(e.Parameter);
            Cliente cliente = new Cliente();
            cliente.LoadByPrimaryKey(idCliente);

            if (cliente.str.Apelido != "")
            {
                if (cliente.IsAtivo)
                {
                    //O permissionamento da cliente é dado direto no cliente vinculado à cliente
                    PermissaoCliente permissaoCliente = new PermissaoCliente();
                    if (permissaoCliente.RetornaAcessoClienteComControle(idCliente, HttpContext.Current.User.Identity.Name))
                    {
                        DateTime dataDia = cliente.DataDia.Value;
                        nome = cliente.str.Apelido + "|" + dataDia.ToShortDateString() + "|" + Thread.CurrentThread.CurrentCulture.Name;
                    }
                    else
                    {
                        nome = "no_access";
                    }
                }
                else
                {
                    nome = "no_active";
                }
            }
        }
        e.Result = nome;
    }
    

    #region btnRun_Click, btnPDF_Click, btnExcel_Click

    /// <summary>
    /// Trata os campos Obrigatorios do Form
    /// </summary>
    private void TrataCamposObrigatorios()
    {
        #region Campos obrigatórios
        List<Control> controles = new List<Control>(new Control[] { 
            this.textData });

        if (base.TestaObrigatorio(controles) != "")
        {
            throw new Exception("Campos com * são obrigatórios!");
        }
        #endregion
    }

    new protected void btnRun_Click(object sender, EventArgs e)
    {
        this.TrataCamposObrigatorios();
        base.btnRun_Click(sender, e); // DataBind do Grid
    }

    new protected void btnPDF_Click(object sender, EventArgs e)
    {
        this.TrataCamposObrigatorios();
        this.SelecionaRelatorio(VisaoRelatorio.visaoPDF);
    }

    new protected void btnExcel_Click(object sender, EventArgs e)
    {
        this.TrataCamposObrigatorios();
        this.SelecionaRelatorio(VisaoRelatorio.visaoExcel);
    }

    /// <summary>
    /// Seleciona o Relatorio a Ser Executado
    /// </summary>
    /// <param name="visao">PDF, Excel ou Report</param>
    private void SelecionaRelatorio(string visao)
    {
        Response.BufferOutput = true;
        //
        // Salva os Parametros na Session
        Session["gridExportPosicaoRendaFixaDetalhe"] = gridExport;
        Response.Redirect("~/Consultas/PosicaoRendaFixaDetalheExporta.aspx?Visao=" + visao);
    }

    #endregion
}