﻿using System;
using System.Data;
using System.Configuration;
using System.Drawing;
using System.Collections;
using System.Collections.Generic;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using System.Web.UI.HtmlControls;
using Financial.Common;
using Financial.Util;
using DevExpress.Web;
using Financial.Web.Common;
using Financial.BMF.Enums;
using Financial.BMF;
using Financial.Bolsa;
using Financial.Investidor;
using Financial.Security;
using Financial.Web.Util;
using Financial.Security.Enums;
using Financial.Investidor.Enums;

public partial class ConsultaCorretagemBMF : ConsultaBasePage {
        
    new protected void Page_Load(object sender, EventArgs e) {
        this.HasPopupCliente = true;
        this.HasPopupAtivoBMF = true;

        if (!IsPostBack)
        {
            this.dropAgenteMercado.DataBind();
            this.dropAgenteMercado.Items.Insert(0, new ListEditItem(""));
        }

        base.Page_Load ( sender, e );
   }

    #region Log
    void Log (string method, Exception ex ) {

        String err = string.Empty;
        string logErr = string.Empty;
        string stack = string.Empty;

        err = ex.Message.ToString();
        if (null != ex.InnerException)
            err += @"\n\n" + ex.InnerException.Message.ToString();
        stack = ex.StackTrace.ToString();

        logErr = string.Format("Error: {0} -  Stack:{1}", err, stack);

        HistoricoLog historicoLog = new HistoricoLog();
        historicoLog.InsereHistoricoLog(DateTime.Now,
                                         DateTime.Now,
                                         string.Format("Cadastro de Perfis de Repasse  - Operacao: {0} --> Codigo: {1}", method, logErr),
                                         HttpContext.Current.User.Identity.Name,
                                         UtilitarioWeb.GetIP(Request),
                                         "",
                                         HistoricoLogOrigem.Outros);
    }

    #endregion

    #region [ Properties ]
    string dataInicio {

        get {

            return this.textDataInicio.Text;
        }
    }

    string dataFim {
        get {
            return this.textDataFim.Text;
        }
    }

    int IdUsuario
    {
        get
        {
            Usuario u = new Usuario();
            u.BuscaUsuario(this.Context.User.Identity.Name);
            if (u.IdUsuario.HasValue)
                return u.IdUsuario.Value;
            //
            return 0;
        }
    }

    int IdCliente {

        get {
            if ( !string.IsNullOrEmpty(this.btnEditCodigoCliente.Text ))
                return int.Parse ( this.btnEditCodigoCliente.Text );
            //
            return 0;
        }
    }

    int IdAgente {

        get {

            if (null != this.dropAgenteMercado.SelectedItem)
                if (!string.IsNullOrEmpty(this.dropAgenteMercado.SelectedItem.Value.ToString()))
                    return int.Parse(this.dropAgenteMercado.SelectedItem.Value.ToString());
            return 0;
        }
    }

    String AtivoBMF {

        get {

            if (null != this.btnEditAtivoBMF.Text && !string.IsNullOrEmpty(this.btnEditAtivoBMF.Text)) {
                return this.btnEditAtivoBMF.Text;
            }
            return "";
        }
    }
    #endregion

    #region DataSources

    protected void EsRelatorioCorretagem_esSelect ( object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e ) {
        
        e.Collection = this.ExecuteQuery ( );
    }

    protected void EsDSAtivoBMF_esSelect ( object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e )
    {
        AtivoBMFCollection coll = new AtivoBMFCollection ( );
        coll.Query.OrderBy ( coll.Query.CdAtivoBMF.Ascending, coll.Query.Serie.Ascending );
        coll.Query.Load ( );

        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }
    protected void EsDSAgenteMercado_esSelect ( object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e ) {
    
        AgenteMercadoCollection coll = new AgenteMercadoCollection ( );
        coll.Query.Where ( coll.Query.FuncaoCorretora.Equal ( "S" ) );
        coll.Query.OrderBy ( coll.Query.Nome.Ascending );
        coll.Query.Load ( );


        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }

    protected void EsDSCliente_esSelect ( object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e ) {
    
        ClienteCollection clienteCollection = new ClienteCollection ( );
        clienteCollection.BuscaClientesComAcesso ( HttpContext.Current.User.Identity.Name );
        e.Collection = clienteCollection;
    }

    #endregion

    #region CallBacks

    protected void ASPxCallback1_Callback ( object source, DevExpress.Web.CallbackEventArgs e )
    {
        string nome = "";
        if ( !String.IsNullOrEmpty ( e.Parameter ) && e.Parameter != "null" )
        {
            int idCliente = Convert.ToInt32 ( e.Parameter );
            Cliente cliente = new Cliente ( );
            cliente.LoadByPrimaryKey ( idCliente );

            if ( cliente.str.Apelido != "" )
            {
                if ( cliente.IsAtivo )
                {
                    PermissaoCliente permissaoCliente = new PermissaoCliente ( );
                    if ( permissaoCliente.RetornaAcessoClienteComControle ( idCliente, HttpContext.Current.User.Identity.Name ) )
                        nome = cliente.str.Apelido;
                    else
                        nome = "no_access";
                }
                else
                    nome = "no_active";
            }
        }
        e.Result = nome;
    }

    protected void ASPxCallback2_Callback ( object source, DevExpress.Web.CallbackEventArgs e )
    {
        string cdAtivoBMF = "";
        string serie = "";
        string peso = "";
        string tipoMercado = "";
        e.Result = "";
        if ( !String.IsNullOrEmpty ( e.Parameter ) && e.Parameter != "null" )
        {
            AtivoBMF ativoBMF = new AtivoBMF ( );
            string paramAtivo = ativoBMF.RetornaCdAtivoSplitado ( Convert.ToString ( e.Parameter ) );
            string paramSerie = ativoBMF.RetornaSerieSplitada ( Convert.ToString ( e.Parameter ) );
            ativoBMF.LoadByPrimaryKey ( paramAtivo, paramSerie );

            if ( ativoBMF.str.CdAtivoBMF != "" )
            {
                cdAtivoBMF = ativoBMF.str.CdAtivoBMF;
                serie = ativoBMF.str.Serie;
                peso = ativoBMF.Peso.ToString ( );
                //
                e.Result = cdAtivoBMF + serie + "|" + tipoMercado + "|" + peso;
            }
        }
    }

    OperacaoBMFCollection ExecuteQuery()
    {
        if (!IsPostBack)
            return new OperacaoBMFCollection();
        //
        String tableOperacao = "tableOperacaoBMF";
        String tablePermissaoCliente = "tablePermissaoCliente";
        String tableCliente = "tableCliente";
        String tableAgenteMercado = "tableAgenteMercado";


        String fieldData = "DataReferencia";
        String fieldApelidoCliente = "Cliente";
        String fieldApelidoCorretora = "Corretora";
        String fieldAtivo = "AtivoBMF";
        String fieldSerie = "SerieBMF";
        String fieldTipoOperacao = "TipoOperacao";
        String fieldQuantidade = "Quantidade";
        String fieldCorretagem = "Corretagem";
        //
        OperacaoBMFCollection result = new OperacaoBMFCollection();


        try
        {
            OperacaoBMFQuery operacao = new OperacaoBMFQuery(tableOperacao);
            PermissaoClienteQuery permissaoCliente = new PermissaoClienteQuery(tablePermissaoCliente);
            ClienteQuery cliente = new ClienteQuery(tableCliente);
            AgenteMercadoQuery agente = new AgenteMercadoQuery(tableAgenteMercado);

            operacao.Select(operacao.Data.As(fieldData),
                            cliente.Apelido.As(fieldApelidoCliente),
                            agente.Apelido.As(fieldApelidoCorretora),
                            operacao.CdAtivoBMF.As(fieldAtivo),
                            operacao.Serie.As(fieldSerie),
                            operacao.TipoOperacao.As(fieldTipoOperacao),
                            operacao.Quantidade.As(fieldQuantidade),
                            operacao.Corretagem.As(fieldCorretagem));
            
            //
            operacao.InnerJoin(cliente).On(operacao.IdCliente.Equal(cliente.IdCliente));
            operacao.InnerJoin(permissaoCliente).On(permissaoCliente.IdCliente.Equal(cliente.IdCliente));
            operacao.InnerJoin(agente).On(operacao.IdAgenteCorretora.Equal(agente.IdAgente));
            
            //filtro padrão...
            operacao.Where(permissaoCliente.IdUsuario.Equal(this.IdUsuario));
            operacao.Where(operacao.Origem.NotEqual((byte)OrigemOperacaoBMF.AjusteFuturo));
            operacao.Where(operacao.Data.GreaterThanOrEqual(this.dataInicio));
            operacao.Where(operacao.Data.LessThanOrEqual(this.dataFim));

            //filtros opcionais...
            if (0 < this.IdCliente)
                operacao.Where(operacao.IdCliente.Equal(this.IdCliente));
            if (0 < this.IdAgente)
                operacao.Where(operacao.IdAgenteCorretora.Equal(this.IdAgente));
            if (!string.IsNullOrEmpty(this.AtivoBMF))
            {
                string[] aux = this.AtivoBMF.Split(new char[] { '-' }, StringSplitOptions.RemoveEmptyEntries);
                //
                if (null != aux && 0 < aux.Length)
                    operacao.Where(operacao.CdAtivoBMF.Equal(aux[0].ToString()));
            }
            //ordenação...
            operacao.OrderBy(operacao.Data.Descending, agente.Nome.Ascending);
            //
            result.Load(operacao);
            //
            if (null != result && 0 < result.Count)
            {
                foreach (OperacaoBMF i in result)
                {
                    object tipoOperacao = i.GetColumn(fieldTipoOperacao);
                    String tipoOperacaoTraduzida = this.traduzirTipoOperacao(tipoOperacao.ToString());
                    i.SetColumn(fieldTipoOperacao, tipoOperacaoTraduzida);
                }
            }
        }
        catch (Exception ex)
        {
            this.Log("Consulta", ex);
        }
        return result;
    }


    private void TrataCamposObrigatorios ( )
    {
        #region Campos obrigatórios
        List<Control> controles = new List<Control> ( new Control[ ] { this.textDataInicio, this.textDataFim } );

        if ( base.TestaObrigatorio ( controles ) != "" )
        {
            throw new Exception ( "Campos com * são obrigatórios!" );
        }
        #endregion
    }

    String traduzirTipoOperacao ( string value ) {

        switch ( value.Trim ( ).ToUpper ( ) ) { 
        
            case "V":
                return "Venda";
            case "C":
                return "Compra";
            case "VD":
                return "Venda DT";
            case "CD":
                return "Compra DT";
            default:
                return value;
        }
    }

    #endregion


    #region Grid

  
    #endregion

    new protected void btnRun_Click ( object sender, EventArgs e )
    {
        this.TrataCamposObrigatorios ( );
        base.btnRun_Click ( sender, e ); // DataBind do Grid
    }

    new protected void btnPDF_Click ( object sender, EventArgs e )
    {
        this.TrataCamposObrigatorios ( );
        this.SelecionaRelatorio ( VisaoRelatorio.visaoPDF );
    }

    new protected void btnExcel_Click ( object sender, EventArgs e )
    {
        this.TrataCamposObrigatorios ( );
        this.SelecionaRelatorio ( VisaoRelatorio.visaoExcel );
    }

    /// <summary>
    /// Seleciona o Relatorio a Ser Executado
    /// </summary>
    /// <param name="visao">PDF, Excel ou Report</param>
    private void SelecionaRelatorio ( string visao )
    {
        Response.BufferOutput = true;
        //
        // Salva os Parametros na Session
        Session["gridExportConsultaCorretagemBMF"] = gridExport;
        Response.Redirect ( "~/Consultas/ConsultaCorretagemBMFExporta.aspx?Visao=" + visao );
    }
}

