﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using Financial.Common;
using System.Collections.Generic;
using Financial.Util;
using System.Text;
using DevExpress.Web;
using System.Drawing;
using Financial.Tributo;
using Financial.Enquadra;
using Financial.Fundo;
using EntitySpaces.Interfaces;
using Financial.Enquadra.Enums;
using Financial.Security;
using Financial.Investidor;
using Financial.Web.Common;

public partial class Consultas_EnquadraResultado : CadastroBasePage {
    new protected void Page_Load(object sender, EventArgs e) {
        this.HasFiltro = true;
        this.HasEditControl = false;
        base.Page_Load(sender, e);
    }

    #region DataSources
    protected void EsDSEnquadraResultado_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e) {
        Usuario usuario = new Usuario();
        usuario.BuscaUsuario(Context.User.Identity.Name);
        int idUsuario = usuario.IdUsuario.Value;

        EnquadraResultadoQuery enquadraResultadoQuery = new EnquadraResultadoQuery("E");
        EnquadraRegraQuery enquadraRegraQuery = new EnquadraRegraQuery("R");
        CarteiraQuery carteiraQuery = new CarteiraQuery("C");
        PermissaoClienteQuery permissaoClienteQuery = new PermissaoClienteQuery("P");
        ClienteQuery clienteQuery = new ClienteQuery("L");

        enquadraResultadoQuery.InnerJoin(enquadraRegraQuery).On(enquadraResultadoQuery.IdRegra == enquadraRegraQuery.IdRegra);
        enquadraResultadoQuery.InnerJoin(carteiraQuery).On(enquadraResultadoQuery.IdCarteira == carteiraQuery.IdCarteira);
        enquadraResultadoQuery.InnerJoin(clienteQuery).On(clienteQuery.IdCliente == enquadraResultadoQuery.IdCarteira);
        enquadraResultadoQuery.InnerJoin(permissaoClienteQuery).On(permissaoClienteQuery.IdCliente == clienteQuery.IdCliente);
        enquadraResultadoQuery.Where(permissaoClienteQuery.IdUsuario.Equal(idUsuario));

        ASPxDateEdit textDataInicio = popupFiltro.FindControl("textDataInicio") as ASPxDateEdit;
        ASPxDateEdit textDataFim = popupFiltro.FindControl("textDataFim") as ASPxDateEdit;

        if (this.textDataInicio.Text != "") {
            enquadraResultadoQuery.Where(enquadraResultadoQuery.Data.GreaterThanOrEqual(textDataInicio.Text));
        }

        if (this.textDataFim.Text != "") {
            enquadraResultadoQuery.Where(enquadraResultadoQuery.Data.LessThanOrEqual(textDataFim.Text));
        }

        enquadraResultadoQuery.OrderBy(enquadraResultadoQuery.Data.Descending, carteiraQuery.Apelido.Ascending,
                                       enquadraRegraQuery.TipoRegra.Ascending, enquadraRegraQuery.Descricao.Ascending);

        EnquadraResultadoCollection coll = new EnquadraResultadoCollection();
        coll.Load(enquadraResultadoQuery);

        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }

    protected void EsDSEnquadraResultadoDetalhe_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e) {
        if (this.hiddenId.Text == "") {
            e.Collection = new EnquadraResultadoDetalheCollection(); ;
        }
        else {
            EnquadraResultadoDetalheCollection coll = new EnquadraResultadoDetalheCollection();

            int index = Convert.ToInt32(this.hiddenId.Text);
            int idResultado = Convert.ToInt32(gridCadastro.GetRowValues(index, "IdResultado"));

            coll.Query.Where(coll.Query.IdResultadoOrigem.Equal(idResultado));
            coll.LoadAll();
            // Assign the esDataSourcSelectEvenArgs Collection property
            e.Collection = coll;
        }
    }
    #endregion

    #region grid Consulta
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    new protected void gridCadastro_PreRender(object sender, EventArgs e) {
        Label labelFiltro = gridCadastro.FindStatusBarTemplateControl("labelFiltro") as Label;
        Label labelFiltroHeader = gridCadastro.FindStatusBarTemplateControl("labelFiltroHeader") as Label;
        StringBuilder texto = new StringBuilder();
        texto.Append("");
        if (textDataInicio.Text != "") {
            texto.Append(" Data Início >= ").Append(textDataInicio.Text.Substring(0, 10).Trim());
        }
        if (textDataFim.Text != "") {
            if (textDataInicio.Text != "")
                texto.Append(" |");

            texto.Append(" Data Fim <= ").Append(textDataFim.Text.Substring(0, 10).Trim());
        }

        labelFiltro.Text = texto.ToString();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    new protected void gridCadastro_HtmlDataCellPrepared(object sender, ASPxGridViewTableDataCellEventArgs e) {
        int idResultado = Convert.ToInt32(e.KeyValue);
        EnquadraResultado enquadraResultado = new EnquadraResultado();
        List<esQueryItem> campos = new List<esQueryItem>();
        campos.Add(enquadraResultado.Query.IdRegra);
        enquadraResultado.LoadByPrimaryKey(campos, idResultado);
        int idRegra = enquadraResultado.IdRegra.Value;

        EnquadraRegra enquadraRegra = new EnquadraRegra();
        campos = new List<esQueryItem>();
        campos.Add(enquadraRegra.Query.TipoRegra);
        campos.Add(enquadraRegra.Query.SubTipoRegra);
        enquadraRegra.LoadByPrimaryKey(campos, idRegra);
        int tipoRegra = enquadraRegra.TipoRegra.Value;

        if (e.DataColumn.FieldName == "Enquadrado") {
            if (Convert.ToString(e.CellValue) == "N") {
                e.Cell.ForeColor = Color.Red;
            }
        }
        else if (e.DataColumn.FieldName == "Resultado") {
            if (tipoRegra == (int)TipoRegraEnquadra.LimiteOscilacao ||
                             enquadraRegra.TipoRegra == (int)TipoRegraEnquadra.NumeroCotista) {
                e.Cell.Text = "-";
            }
        }
        else if (e.DataColumn.FieldName == "ValorBase") {
            if (tipoRegra == (int)TipoRegraEnquadra.NumeroCotista ||
                             enquadraRegra.TipoRegra == (int)TipoRegraEnquadra.PosicaoDescoberto ||
                             enquadraRegra.TipoRegra == (int)TipoRegraEnquadra.LimiteOscilacao ||
                             enquadraRegra.TipoRegra == (int)TipoRegraEnquadra.MediaMovel) {
                e.Cell.Text = "-";
            }
        }
        else if (e.DataColumn.FieldName == "ValorCriterio") {
            if (tipoRegra == (int)TipoRegraEnquadra.MediaMovel) {
                e.Cell.Text = "-";
            }
            else if (tipoRegra == (int)TipoRegraEnquadra.NumeroCotista) {
                e.Cell.Text = e.CellValue.ToString().Replace(",00", "");
            }

        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_CustomCallback(object sender, ASPxGridViewCustomCallbackEventArgs e) {        
        gridCadastro.DataBind();        
    }
    #endregion

    #region grid PopuUp - ResultadoDetalhe
    protected void gridResultadoDetalhe_HtmlDataCellPrepared(object sender, ASPxGridViewTableDataCellEventArgs e) {
        if (e.DataColumn.FieldName == "Enquadrado") {
            if (Convert.ToString(e.CellValue) == "N") {
                e.Cell.ForeColor = Color.Red;
            }
        }
    }

    protected void gridResultadoDetalhe_CustomCallback(object sender, ASPxGridViewCustomCallbackEventArgs e) {
        gridResultadoDetalhe.DataBind();
    }
    #endregion
}