﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using Financial.Security;
using Financial.ContaCorrente;
using System.Text;
using DevExpress.Web;
using Financial.Web.Util;
using System.Drawing;
using Financial.Investidor;
using Financial.Bolsa;
using System.Collections.Generic;
using EntitySpaces.Interfaces;
using Financial.Common;
using Financial.InvestidorCotista;
using Financial.Fundo;
using Financial.Security.Enums;
using Financial.Web.Common;
using Financial.Util;
using Financial.InvestidorCotista.Custom;
using Financial.Investidor.Enums;
using Financial.Fundo.Enums;

public partial class Consultas_SaldosInformeCotista : ConsultaBasePage
{
    new protected void Page_Load(object sender, EventArgs e) {
        this.HasPopupCotista = true;
        this.HasPopupCarteira = true;
        btnEditCodigoCarteira.Focus();
        base.Page_Load(sender, e);
    }

    #region Classe para Binding
    public class BindingDSCollection : AtivoBolsaCollection {
        /// <summary>
        /// Adiciona uma Coluna Nova na Collection
        /// </summary>
        /// <param name="columnName"></param>
        /// <param name="typeColumn"></param>
        public void AddColumn(string columnName, Type typeColumn) {
            if (this.Table != null && !this.Table.Columns.Contains(columnName)) {
                this.Table.Columns.Add(columnName, typeColumn);
            }
        }
    }
    #endregion
    
    #region DataSources
    protected void EsDSInforme_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e) 
    {
        BindingDSCollection b = new BindingDSCollection();
        b.CreateColumnsForBinding();
        b.AddColumn("Ano", typeof(System.Int32));
        b.AddColumn("IdFundo", typeof(System.Int32));
        b.AddColumn("CodFundo", typeof(System.String));
        b.AddColumn("NomeFundo", typeof(System.String));
        b.AddColumn("Tipo", typeof(System.String));
        b.AddColumn("IdCotista", typeof(System.Int32));
        b.AddColumn("CodCotista", typeof(System.String));
        b.AddColumn("NomeCotista", typeof(System.String));
        b.AddColumn("DataSaldo", typeof(System.DateTime));
        b.AddColumn("QuantidadeCotas", typeof(System.Decimal));
        b.AddColumn("SaldoFinanceiro", typeof(System.Decimal));
        
        if (!Page.IsPostBack) {
            gridConsulta.Visible = false;
            e.Collection = new BindingDSCollection();
        }
        else 
        {
            int ano = DateTime.Now.Year - 1;
            if (textAno.Text != "")
            {
                ano = Convert.ToInt32(textAno.Text);
            }
            
            DateTime dataFimAno = Calendario.RetornaUltimoDiaUtilAno(new DateTime(ano, 1, 1));
            
            int? idCarteiraParam = null;
            if (btnEditCodigoCarteira.Text != "") {
                idCarteiraParam = Convert.ToInt32(btnEditCodigoCarteira.Text);
            }

            int? idCotistaParam = null;
            if (btnEditCodigoCotista.Text != "") {
                idCotistaParam = Convert.ToInt32(btnEditCodigoCotista.Text);
            }

            PosicaoCotistaHistoricoCollection posicaoCotistaHistoricoCollection = new PosicaoCotistaHistoricoCollection();
            CarteiraQuery carteiraQuery = new CarteiraQuery("A");
            ClienteQuery clienteQuery = new ClienteQuery("L");
            ClienteInterfaceQuery clienteInterfaceQuery = new ClienteInterfaceQuery("C");
            CotistaQuery cotistaQuery = new CotistaQuery("O");
            PosicaoCotistaHistoricoQuery posicaoCotistaHistoricoQuery = new PosicaoCotistaHistoricoQuery("P");
            //
            posicaoCotistaHistoricoQuery.Select(posicaoCotistaHistoricoQuery.IdCarteira,
                                                posicaoCotistaHistoricoQuery.IdCotista,
                                                carteiraQuery.TipoTributacao,
                                                carteiraQuery.TipoCota,
                                                clienteQuery.Nome.As("NomeFundo"),
                                                clienteQuery.IdTipo,
                                                cotistaQuery.Nome.As("NomeCotista"),
                                                cotistaQuery.CodigoInterface,
                                                clienteInterfaceQuery.CodigoYMF);
            //
            posicaoCotistaHistoricoQuery.InnerJoin(cotistaQuery).On(cotistaQuery.IdCotista == posicaoCotistaHistoricoQuery.IdCotista);
            posicaoCotistaHistoricoQuery.InnerJoin(carteiraQuery).On(carteiraQuery.IdCarteira == posicaoCotistaHistoricoQuery.IdCarteira);
            posicaoCotistaHistoricoQuery.InnerJoin(clienteQuery).On(clienteQuery.IdCliente == posicaoCotistaHistoricoQuery.IdCarteira);
            posicaoCotistaHistoricoQuery.LeftJoin(clienteInterfaceQuery).On(clienteInterfaceQuery.IdCliente == posicaoCotistaHistoricoQuery.IdCarteira);
            //
            posicaoCotistaHistoricoQuery.Where(posicaoCotistaHistoricoQuery.IdCarteira.NotEqual(posicaoCotistaHistoricoQuery.IdCotista),
                                               posicaoCotistaHistoricoQuery.DataHistorico.Equal(dataFimAno), 
                                               clienteQuery.TipoControle.In((byte)TipoControleCliente.Completo, (byte)TipoControleCliente.Cotista),
                                               clienteQuery.IdTipo.In(TipoClienteFixo.Fundo, TipoClienteFixo.Clube, TipoClienteFixo.FDIC));
            
            if (idCotistaParam.HasValue)
            {
                posicaoCotistaHistoricoQuery.Where(posicaoCotistaHistoricoQuery.IdCotista.Equal(idCotistaParam.Value));
            }

            if (idCarteiraParam.HasValue)
            {
                posicaoCotistaHistoricoQuery.Where(posicaoCotistaHistoricoQuery.IdCarteira.Equal(idCarteiraParam.Value));
            }

            posicaoCotistaHistoricoQuery.GroupBy(posicaoCotistaHistoricoQuery.IdCarteira,
                                                 posicaoCotistaHistoricoQuery.IdCotista,
                                                 carteiraQuery.TipoTributacao,
                                                 carteiraQuery.TipoCota,
                                                 clienteQuery.Nome.As("NomeFundo"),
                                                 clienteQuery.IdTipo,
                                                 cotistaQuery.Nome.As("NomeCotista"),
                                                 cotistaQuery.CodigoInterface,
                                                 clienteInterfaceQuery.CodigoYMF);
            
            posicaoCotistaHistoricoCollection.Load(posicaoCotistaHistoricoQuery);


            foreach (PosicaoCotistaHistorico posicaoCotistaHistorico in posicaoCotistaHistoricoCollection)
            {
                int idCarteira = posicaoCotistaHistorico.IdCarteira.Value;
                int idCotista = posicaoCotistaHistorico.IdCotista.Value;

                string codigoFundo = "";
                if (!Convert.IsDBNull(posicaoCotistaHistorico.GetColumn(ClienteInterfaceMetadata.ColumnNames.CodigoYMF)))
                {
                    codigoFundo = Convert.ToString(posicaoCotistaHistorico.GetColumn(ClienteInterfaceMetadata.ColumnNames.CodigoYMF)).Trim();
                }

                string codigoCotista = "";
                if (!Convert.IsDBNull(posicaoCotistaHistorico.GetColumn(CotistaMetadata.ColumnNames.CodigoInterface)))
                {
                    codigoCotista = Convert.ToString(posicaoCotistaHistorico.GetColumn(CotistaMetadata.ColumnNames.CodigoInterface)).Trim();
                }

                string nomeFundo = Convert.ToString(posicaoCotistaHistorico.GetColumn("NomeFundo")).Trim();
                string tipo = Convert.ToInt32(posicaoCotistaHistorico.GetColumn(ClienteMetadata.ColumnNames.IdTipo)) == TipoClienteFixo.Clube? "Clube": "Fundo";
                string nomeCotista = Convert.ToString(posicaoCotistaHistorico.GetColumn("NomeCotista")).Trim();
                byte tipoTributacao = Convert.ToByte(posicaoCotistaHistorico.GetColumn(CarteiraMetadata.ColumnNames.TipoTributacao));
                byte tipoCota = Convert.ToByte(posicaoCotistaHistorico.GetColumn(CarteiraMetadata.ColumnNames.TipoCota));

                AtivoBolsa a = b.AddNew();
                a.SetColumn("Ano", ano);
                a.SetColumn("DataSaldo", dataFimAno);
                a.SetColumn("IdFundo", posicaoCotistaHistorico.IdCarteira.Value);
                a.SetColumn("CodFundo", codigoFundo);
                a.SetColumn("NomeFundo", nomeFundo);
                a.SetColumn("Tipo", tipo);
                a.SetColumn("IdCotista", posicaoCotistaHistorico.IdCotista.Value);
                a.SetColumn("CodCotista", codigoCotista);
                a.SetColumn("NomeCotista", nomeCotista);

                decimal quantidade = 0;
                decimal saldo = 0;
                if (tipoTributacao == (byte)TipoTributacaoFundo.Acoes)
                {
                    #region Calculo para Fundo Acoes
                    PosicaoCotistaHistorico posicaoCotistaHistoricoAcoes = new PosicaoCotistaHistorico();
                    posicaoCotistaHistoricoAcoes.Query.Select(posicaoCotistaHistoricoAcoes.Query.Quantidade.Sum(),
                                                             (posicaoCotistaHistoricoAcoes.Query.Quantidade * posicaoCotistaHistoricoAcoes.Query.CotaAplicacao).As("SaldoPosicao").Sum());
                    posicaoCotistaHistoricoAcoes.Query.Where(posicaoCotistaHistoricoAcoes.Query.IdCarteira.Equal(idCarteira),
                                                             posicaoCotistaHistoricoAcoes.Query.IdCotista.Equal(idCotista),
                                                             posicaoCotistaHistoricoAcoes.Query.DataHistorico.Equal(dataFimAno));
                    posicaoCotistaHistoricoAcoes.Query.Load();


                    if (posicaoCotistaHistoricoAcoes.Quantidade.HasValue)
                    {
                        quantidade = posicaoCotistaHistoricoAcoes.Quantidade.Value;
                        saldo = (decimal)posicaoCotistaHistoricoAcoes.GetColumn("SaldoPosicao");
                    }
                    #endregion
                }
                else
                {
                    #region Calculo para Fundo com comecotas
                    PosicaoCotistaHistoricoCollection posicaoCotistaHistoricoCollectionAux = new PosicaoCotistaHistoricoCollection();
                    posicaoCotistaHistoricoCollectionAux.Query.Select(posicaoCotistaHistoricoCollectionAux.Query.DataConversao,
                                                                      posicaoCotistaHistoricoCollectionAux.Query.DataUltimaCobrancaIR,
                                                                      posicaoCotistaHistoricoCollectionAux.Query.Quantidade,
                                                                      posicaoCotistaHistoricoCollectionAux.Query.CotaAplicacao,
                                                                      posicaoCotistaHistoricoCollectionAux.Query.CotaDia);
                    posicaoCotistaHistoricoCollectionAux.Query.Where(posicaoCotistaHistoricoCollectionAux.Query.IdCarteira.Equal(idCarteira),
                                                                     posicaoCotistaHistoricoCollectionAux.Query.IdCotista.Equal(idCotista),
                                                                     posicaoCotistaHistoricoCollectionAux.Query.DataHistorico.Equal(dataFimAno));
                    posicaoCotistaHistoricoCollectionAux.Query.Load();

                    foreach (PosicaoCotistaHistorico posicaoCotistaHistoricoAux in posicaoCotistaHistoricoCollectionAux)
                    {
                        #region Computa Saldo Final do cotista
                        DateTime dataConversao = posicaoCotistaHistoricoAux.DataConversao.Value;
                        DateTime dataUltimaCobrancaIR = posicaoCotistaHistoricoAux.DataUltimaCobrancaIR.Value;
                        decimal quantidadePosicao = posicaoCotistaHistoricoAux.Quantidade.Value;
                        decimal cotaAplicacao = posicaoCotistaHistoricoAux.CotaAplicacao.Value;
                        decimal cotaDia = posicaoCotistaHistoricoAux.CotaDia.Value;

                        quantidade += quantidadePosicao;

                        decimal cotaCusto = 0;
                        decimal saldoPosicao = 0;
                        if (dataUltimaCobrancaIR > dataConversao)
                        {
                            HistoricoCota historicoCota = new HistoricoCota();
                            historicoCota.Query.Select(historicoCota.Query.CotaAbertura,
                                                       historicoCota.Query.CotaFechamento);
                            historicoCota.Query.Where(historicoCota.Query.IdCarteira.Equal(idCarteira),
                                                      historicoCota.Query.Data.Equal(dataUltimaCobrancaIR));
                            historicoCota.Query.Load();

                            if (tipoCota == (byte)TipoCotaFundo.Abertura)
                            {
                                cotaCusto = historicoCota.CotaAbertura.Value;
                            }
                            else
                            {
                                if (historicoCota.CotaFechamento.HasValue)
                                {
                                    cotaCusto = historicoCota.CotaFechamento.Value;
                                }
                            }
                        }
                        else
                        {
                            cotaCusto = cotaAplicacao;
                        }

                        saldoPosicao = quantidadePosicao * cotaCusto;

                        saldo += saldoPosicao;
                        #endregion
                    }
                    #endregion
                }

                a.SetColumn("QuantidadeCotas", quantidade);
                a.SetColumn("SaldoFinanceiro", saldo);                
            }
               
            e.Collection = b;
            gridConsulta.Visible = true;
        }
    }

    protected void EsDSCarteira_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e) {
        CarteiraCollection carteiraCollection = new CarteiraCollection();
        carteiraCollection.BuscaCarteirasComAcessoTodas(HttpContext.Current.User.Identity.Name);

        e.Collection = carteiraCollection;
    }

    protected void EsDSCotista_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e) {
        CotistaCollection cotistaCollection = new CotistaCollection();
        cotistaCollection.BuscaCotistasComAcesso(HttpContext.Current.User.Identity.Name);

        e.Collection = cotistaCollection;
    }
    #endregion

    /// <summary>
    /// 
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void ASPxCallback1_Callback(object source, DevExpress.Web.CallbackEventArgs e)
    {
        string nome = "";
        if (!String.IsNullOrEmpty(e.Parameter) && e.Parameter != "null")
        {
            int idCarteira = Convert.ToInt32(e.Parameter);
            Carteira carteira = new Carteira();
            carteira.LoadByPrimaryKey(idCarteira);

            if (carteira.str.Apelido != "")
            {
                if (carteira.IsAtivo)
                {
                    //O permissionamento da carteira é dado direto no cliente vinculado à carteira
                    PermissaoCliente permissaoCliente = new PermissaoCliente();
                    if (permissaoCliente.RetornaAcessoCliente(idCarteira, HttpContext.Current.User.Identity.Name))
                    {
                        nome = carteira.str.Apelido;
                    }
                    else
                    {
                        nome = "no_access";
                    }
                }
                else
                {
                    nome = "no_active";
                }
            }
        }
        e.Result = nome;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void ASPxCallback2_Callback(object source, DevExpress.Web.CallbackEventArgs e) {
        string nome = "";
        if (!String.IsNullOrEmpty(e.Parameter) && e.Parameter != "null") {
            int idCotista = Convert.ToInt32(e.Parameter);
            Cotista cotista = new Cotista();
            cotista.LoadByPrimaryKey(idCotista);

            if (cotista.str.Apelido != "") {
                if (cotista.IsAtivo) {
                    //O permissionamento da cotista é dado direto no cliente vinculado à cotista
                    PermissaoCotista permissaoCotista = new PermissaoCotista();
                    if (permissaoCotista.RetornaAcessoCotista(idCotista, HttpContext.Current.User.Identity.Name)) {
                        nome = cotista.str.Apelido;
                    }
                    else {
                        nome = "no_access";
                    }
                }
                else {
                    nome = "no_active";
                }
            }
        }
        e.Result = nome;
    }

    #region btnRun_Click, btnPDF_Click, btnExcel_Click

    new protected void btnRun_Click(object sender, EventArgs e) {
        base.btnRun_Click(sender, e); // DataBind do Grid
    }

    new protected void btnPDF_Click(object sender, EventArgs e) {
        this.SelecionaRelatorio(VisaoRelatorio.visaoPDF);
    }

    new protected void btnExcel_Click(object sender, EventArgs e) {
        this.SelecionaRelatorio(VisaoRelatorio.visaoExcel);
    }

    /// <summary>
    /// Seleciona o Relatorio a Ser Executado
    /// </summary>
    /// <param name="visao">PDF, Excel ou Report</param>
    private void SelecionaRelatorio(string visao) {
        Response.BufferOutput = true;
        //
        // Salva os Parametros na Session
        Session["gridExportSaldosInformeCotista"] = gridExport;
        Response.Redirect("~/Consultas/SaldosInformeCotistaExporta.aspx?Visao=" + visao);
    }

    #endregion
}