﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="MemoriaCalculoRF.aspx.cs" Inherits="Consultas_MemoriaCalculoRF" %>

<%@ Register Assembly="System.Web, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"
    Namespace="System.Web.UI.HtmlControls" TagPrefix="cc2" %>

<%@ Register Assembly="EntitySpaces.Web" Namespace="EntitySpaces.Web" TagPrefix="cc1" %>

<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxe" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxwgv" %>

<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxpc" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title>Untitled Page</title>
    <link href="~/css/forms.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" language="Javascript" src="../js/global.js"></script>     
    <script type="text/javascript" language="Javascript"> 
    document.onkeydown=onDocumentKeyDown;
    </script>
    
    <script type="text/javascript" language="Javascript"> 
    
    function Exportar(selectedValues){
            if(selectedValues.length == 0)
            {
                alert('Alguma posição deve ser escolhida');
                return;
            }
            else if(selectedValues.length > 1)
            {
                alert('Apenas uma posição deve ser escolhida');
                return;
            }
        
            var idPosicaoRendaFixa = selectedValues[0][0];
            var dataCalculo = selectedValues[0][1];
            var dataCalculoTokens = dataCalculo.getFullYear() + '-' + (dataCalculo.getMonth()+1) + '-' + dataCalculo.getDate();
            var eventosData = chkEventosData.GetChecked();

            
            var iframeId = 'iframe_download';
            var iframe = document.getElementById(iframeId);

            if (!iframe && document.createElement && (iframe =
                document.createElement('iframe'))) {
               
                iframe.name = iframe.id = iframeId;
                iframe.width = 0;
                iframe.height = 0;
                iframe.src = 'about:blank';
                document.body.appendChild(iframe);                                                                    
            }
            
            var taxa = 0;
            if (textTaxa.GetText() != null) 
            {
                taxa = textTaxa.GetText();
            }
            
            var dataParam = textData.GetText();
            
            iframe.src = '../relatorios/rendafixa/reportpricingdiario.ashx?idposicaorendafixa=' + idPosicaoRendaFixa + '&datacalculo=' + dataCalculoTokens +
                                                                                            '&eventosData=' + eventosData + '&taxa=' + taxa + '&dataparam=' + dataParam;
            return false;
                   
    }
    </script>
    
</head>
<body>    
    <form id="form1" runat="server">
            
    <div class="divPanel">
    <table width="100%"><tr><td>
    <div id="container">
    
    <div id="header">Memória de Cálculo para Renda Fixa</div>
        
    <div id="mainContent">    
        
        <table border="0"> 
        <tr>
            <td>
                <div class="linkButton linkButtonNoBorder">                       
                    <asp:LinkButton ID="btnPDF" runat="server" Font-Overline="false" CssClass="btnPdf" OnClick="btnPDF_Click"><asp:Literal ID="Literal4" runat="server" Text="Gerar PDF"/><div></div></asp:LinkButton>
                    <asp:LinkButton ID="btnExcel" runat="server" Font-Overline="false" CssClass="btnExcel" OnClick="btnExcel_Click"><asp:Literal ID="Literal5" runat="server" Text="Gerar Excel"/><div></div></asp:LinkButton>
                    <asp:LinkButton ID="btnRefresh" runat="server" Font-Overline="false" CssClass="btnRefresh" 
                            OnClientClick="gridConsulta.PerformCallback('btnRefresh'); return false;"><asp:Literal ID="Literal7" runat="server" Text="Atualizar"/><div></div></asp:LinkButton>
                    <asp:LinkButton ID="btnPricing" runat="server" Font-Overline="false" ForeColor="Black" CssClass="btnOK" 
                                                        OnClientClick="gridConsulta.GetSelectedFieldValues('IdPosicao;DataDia', Exportar);return false;">
                                                        <asp:Literal ID="Literal9" runat="server" Text="Exportar" />                                                                            
                    <div></div>
                    </asp:LinkButton>                    
                </div>
            </td>            
            <td class="td_Label">
                <asp:Label ID="labelValor" runat="server" CssClass="labelNormal" Text="TIR Indicada:"/>
            </td>
            <td>
                <dxe:ASPxSpinEdit ID="textTaxa" runat="server" CssClass="textValor_5" ClientInstanceName="textTaxa" MaxLength="12" NumberType="Float" DecimalPlaces="8" />
            </td>
            <td class="td_Label">
                <asp:Label ID="label1" runat="server" CssClass="labelNormal" Text="Data:"/>
            </td>
            <td>
                <dxe:ASPxDateEdit ID="textData" runat="server" ClientInstanceName="textData" />
            </td>
            <td>
                <dxe:ASPxCheckBox ID="chkEventosData" ClientInstanceName="chkEventosData" runat="server" Text="Considera eventos da data" Checked="true"/>
            </td>            
        </tr>
        </table>
               
       <div class="divDataGrid">
            
                <dxwgv:ASPxGridView ID="gridConsulta" runat="server" EnableCallBacks="true" 
                        ClientInstanceName="gridConsulta" AutoGenerateColumns="False" 
                        DataSourceID="EsDSPosicaoRendaFixa" KeyFieldName="IdPosicao"
                        OnHtmlRowCreated="gridConsulta_HtmlRowCreated" Width="100%"
                        OnCustomCallback="gridConsulta_CustomCallback">                    
                <Columns>
                    <dxwgv:GridViewCommandColumn ShowSelectCheckbox="True" VisibleIndex="0" Width="5%" ButtonType="Image" ShowClearFilterButton="True"/>
                    
                    <dxwgv:GridViewDataTextColumn FieldName="IdCliente" Caption="Id Cliente" VisibleIndex="1" Width="7%" CellStyle-HorizontalAlign="left"/>
                                                            
                    <dxwgv:GridViewDataTextColumn FieldName="IdPosicao" Caption="Posição" VisibleIndex="2" Width="6%" CellStyle-HorizontalAlign="left"/>
                    <dxwgv:GridViewDataTextColumn FieldName="IdTitulo" Caption="Título" VisibleIndex="3" Width="6%" CellStyle-HorizontalAlign="left"/>
                    
                    <dxwgv:GridViewDataTextColumn FieldName="DescricaoCompleta" Caption="Descrição" VisibleIndex="10" Width="13%" Settings-AutoFilterCondition="Contains"/>
                    
                    <dxwgv:GridViewDataDateColumn FieldName="DataVencimento" Caption="Vcto" VisibleIndex="12" Width="10%"/>
                    <dxwgv:GridViewDataDateColumn FieldName="DataOperacao" Caption="Oper." VisibleIndex="14" Width="10%"/>
                    
                    <dxwgv:GridViewDataTextColumn FieldName="TaxaOperacao" Caption="Taxa" VisibleIndex="16" Width="5%" HeaderStyle-HorizontalAlign="Right" CellStyle-HorizontalAlign="Right">
                        <PropertiesTextEdit DisplayFormatString="{0:#,##0.00;(#,##0.00);0.00}"></PropertiesTextEdit>
                    </dxwgv:GridViewDataTextColumn>
                    
                    <dxwgv:GridViewDataTextColumn FieldName="Quantidade" Caption="Qtde" VisibleIndex="18" Width="8%" HeaderStyle-HorizontalAlign="Right" CellStyle-HorizontalAlign="Right">
                        <PropertiesTextEdit DisplayFormatString="{0:#,##0;(#,##0);0}"></PropertiesTextEdit>
                    </dxwgv:GridViewDataTextColumn>
                    
                    <dxwgv:GridViewDataTextColumn FieldName="PUOperacao" Caption="PU Operação" VisibleIndex="20" Width="10%" HeaderStyle-HorizontalAlign="Right" CellStyle-HorizontalAlign="Right">
                        <PropertiesTextEdit DisplayFormatString="{0:#,##0.00000000;(#,##0.00000000);0.00000000}"></PropertiesTextEdit>
                    </dxwgv:GridViewDataTextColumn>
                    
                    <dxwgv:GridViewDataTextColumn FieldName="PUMercado" Caption="PU Mercado" VisibleIndex="22" Width="10%" HeaderStyle-HorizontalAlign="Right" CellStyle-HorizontalAlign="Right">
                        <PropertiesTextEdit DisplayFormatString="{0:#,##0.00000000;(#,##0.00000000);0.00000000}"></PropertiesTextEdit>
                    </dxwgv:GridViewDataTextColumn>
                    
                    <dxwgv:GridViewDataTextColumn FieldName="ValorMercado" Caption="Valor" VisibleIndex="24" Width="10%" HeaderStyle-HorizontalAlign="Right" CellStyle-HorizontalAlign="Right">
                        <PropertiesTextEdit DisplayFormatString="{0:#,##0.00;(#,##0.00);0.00}"></PropertiesTextEdit>
                    </dxwgv:GridViewDataTextColumn>                    
                    
                    <dxwgv:GridViewDataTextColumn FieldName="DataDia" Visible="false"/>
                </Columns>            
                                
                <SettingsPager PageSize="30"></SettingsPager>
                <Settings ShowFilterRow="True" ShowStatusBar="Visible" VerticalScrollBarMode="Visible" 
                         VerticalScrollableHeight="340" VerticalScrollBarStyle="Virtual" />
                <SettingsText EmptyDataRow="0 registros"/>

                <Images/>
                <Styles>                
                    <Header ImageSpacing="5px" SortingImageSpacing="5px" />
                    <AlternatingRow Enabled="True" />
                    <Cell Wrap="False" />
                </Styles>
                <SettingsCommandButton>
                    <ClearFilterButton Image-Url="../imagens/funnel--minus.png"/>
                </SettingsCommandButton>                    
                              
                </dxwgv:ASPxGridView>                                                    
            </div>
                      
    </div>
    </div>
    </td></tr></table>
    </div>   

    <dxwgv:ASPxGridViewExporter ID="gridExport" runat="server" GridViewID="gridConsulta" />        
        
    <cc1:esDataSource ID="EsDSPosicaoRendaFixa" runat="server" OnesSelect="EsDSPosicaoRendaFixa_esSelect" LowLevelBind="true"/>    
        
    </form>        
</body>
</html>