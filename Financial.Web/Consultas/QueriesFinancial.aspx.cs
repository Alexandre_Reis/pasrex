﻿using System;
using System.IO;
using System.Data;
using System.Data.OleDb;
using System.Configuration;
using System.Collections;
using System.Text;
using System.Xml;
using System.Drawing;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Korzh.WebControls.XControls;
using Korzh.EasyQuery;
using EntitySpaces.Core;
using EntitySpaces.Interfaces;
using Financial.Common;
using Financial.Fundo;
using System.Reflection;
using DevExpress.Web;
using DevExpress.Utils;
using System.Collections.Generic;
using Financial.Security;
using System.Linq;

public partial class QueryMainForm : Page
{

    private string baseDataPath = null;
    private Korzh.EasyQuery.Query query = null;

    protected void Page_Init(object sender, EventArgs e)
    {
        this.baseDataPath = this.MapPath("./Data");
        this.query = (Korzh.EasyQuery.Query)Session["Query"];
    }

    protected void Page_Load(object sender, EventArgs e)
    {

        if (!Page.IsPostBack)
        {
            string queryName = Page.Request.QueryString.Get("query");
            if (queryName != null)
            {
                query.LoadFromFile(baseDataPath + "\\" + queryName + ".xml");
            }
        }

        query.Model.ReloadResources();
        query.Model.UpdateOperatorsTexts();

        QueryPanel1.Query = query;

        QueryColumnsPanel1.Query = QueryPanel1.Query;
        EntitiesPanel1.Query = QueryPanel1.Query;

        SortColumnsPanel1.Query = QueryPanel1.Query;

        query.ColumnsChanged += new Korzh.EasyQuery.ColumnsChangedEventHandler(query_ColumnsChanged);
        query.ConditionsChanged += new Korzh.EasyQuery.ConditionsChangedEventHandler(query_ConditionsChanged);
        query.SortOrderChanged += new Korzh.EasyQuery.SortOrderChangedEventHandler(query_SortOrderChanged);

        AssemblyFileVersionAttribute versionAttr =
            (AssemblyFileVersionAttribute)Attribute.GetCustomAttribute(QueryPanel1.GetType().Assembly, typeof(System.Reflection.AssemblyFileVersionAttribute));

        InicializagridQuery();

        BuscaFinancial_Consulta();        

    }

    #region devexpress

    protected void InicializagridQuery()
    {
        //ASPxGridView gridQuery = this.FindControl("gridQuery") as ASPxGridView;
        #region Properties básicas
        gridQuery.ClientInstanceName = "gridQuery";
        gridQuery.AutoGenerateColumns = false;
        gridQuery.Width = Unit.Percentage(100);
        gridQuery.SettingsBehavior.AllowSort = false;
        gridQuery.EnableViewState = true;
        gridQuery.EnableCallBacks = true;        

        gridQuery.Settings.ShowFilterRowMenu = true;

        //((GridViewCommandColumn)gridQuery.Columns[0]).Width = Unit.Percentage(5);
        gridQuery.SettingsText.PopupEditFormCaption = "";
        gridQuery.SettingsEditing.Mode = GridViewEditingMode.PopupEditForm;

        //gridQuery.SettingsEditing.PopupEditFormModal = true;
        //gridQuery.SettingsEditing.PopupEditFormHorizontalAlign = PopupHorizontalAlign.WindowCenter;
        //gridQuery.SettingsEditing.PopupEditFormVerticalAlign = PopupVerticalAlign.WindowCenter;
        gridQuery.SettingsPopup.EditForm.Modal = true;
        gridQuery.SettingsPopup.EditForm.HorizontalAlign = PopupHorizontalAlign.WindowCenter;
        gridQuery.SettingsPopup.EditForm.VerticalAlign = PopupVerticalAlign.WindowCenter;

        //Pager
        gridQuery.SettingsPager.Mode = GridViewPagerMode.EndlessPaging;
        gridQuery.SettingsPager.PageSize = 5;

        //Styles
        gridQuery.Styles.Header.SortingImageSpacing = Unit.Pixel(5);
        gridQuery.Styles.AlternatingRow.Enabled = DefaultBoolean.True;
        gridQuery.Styles.Cell.Wrap = DefaultBoolean.True;
        gridQuery.Styles.CommandColumn.Cursor = "hand";

        ((GridViewCommandColumn)gridQuery.Columns[0]).ShowSelectCheckbox = true;

        //((GridViewCommandColumn)gridQuery.Columns[0]).Width = Unit.Percentage(10);
        //((GridViewCommandColumn)gridQuery.Columns[0]).UpdateButton.Image.Url = "~/imagens/ico_form_ok_inline.gif";
        //((GridViewCommandColumn)gridQuery.Columns[0]).CancelButton.Image.Url = "~/imagens/ico_form_back_inline.gif";
        //((GridViewCommandColumn)gridQuery.Columns[0]).UpdateButton.Text = "Salvar";
        //((GridViewCommandColumn)gridQuery.Columns[0]).CancelButton.Text = "Cancelar";

        //gridQuery.SettingsEditing.Mode = GridViewEditingMode.Inline;





        #endregion


    }

    protected void callbackErro_Callback(object source, DevExpress.Web.CallbackEventArgs e)
    {
        e.Result = "";

        ASPxTextBox textQuery = gridQuery.FindEditFormTemplateControl("textQuery") as ASPxTextBox;
        if (!textQuery.Text.StartsWith("SELECT", StringComparison.InvariantCultureIgnoreCase))
        {
            e.Result = "Apenas consultas SELECT simples são aceitas.";
        }
    }

    protected void CheckBoxSelectAll(object sender, EventArgs e)
    {
        ASPxCheckBox chk = sender as ASPxCheckBox;
        ASPxGridView grid = (chk.NamingContainer as GridViewHeaderTemplateContainer).Grid;
        chk.Checked = (grid.Selection.Count == grid.VisibleRowCount);
    }

    protected void EsDSQuery_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        QueryCustomCollection coll = new QueryCustomCollection();
        coll.LoadAll();
        e.Collection = coll;


    }

    protected void gridResultado_CustomCallback(object sender, ASPxGridViewCustomCallbackEventArgs e)
    {
        CarregaGrid();
        gridResultado.DataBind();
    }

    protected void gridQuery_CustomCallback(object sender, ASPxGridViewCustomCallbackEventArgs e)
    {
        if (e.Parameters == "btnDelete")
        {
            List<object> keyValuesId = gridQuery.GetSelectedFieldValues(QueryCustomMetadata.ColumnNames.IdQuery);
            for (int i = 0; i < keyValuesId.Count; i++)
            {
                int idQuery = Convert.ToInt32(keyValuesId[i]);
                QueryCustom q = new QueryCustom();
                if (q.LoadByPrimaryKey(idQuery))
                {
                    q.MarkAsDeleted();
                    q.Save();
                }
            }
        }

        gridQuery.DataBind();
        gridQuery.Selection.UnselectAll();
        gridQuery.CancelEdit();
    }

    protected void gridQuery_RowInserting(object sender, DevExpress.Web.Data.ASPxDataInsertingEventArgs e)
    {
        QueryCustom q = new QueryCustom();

        ASPxTextBox textNome = gridQuery.FindEditFormTemplateControl("textNome") as ASPxTextBox;
        ASPxTextBox textDescricao = gridQuery.FindEditFormTemplateControl("textDescricao") as ASPxTextBox;
        ASPxTextBox textQuery = gridQuery.FindEditFormTemplateControl("textQuery") as ASPxTextBox;

        q.Nome = textNome.Text;
        q.Descricao = textDescricao.Text;
        q.QueryCustom = textQuery.Text;

        q.Save();

        e.Cancel = true;
        gridQuery.CancelEdit();
        gridQuery.DataBind();
    }

    protected void gridQuery_RowUpdating(object sender, DevExpress.Web.Data.ASPxDataUpdatingEventArgs e)
    {
        QueryCustom q = new QueryCustom();
        int idQuery = (int)e.Keys[0];

        if (q.LoadByPrimaryKey(idQuery))
        {
            q.Nome = e.NewValues[QueryCustomMetadata.ColumnNames.Nome].ToString();
            q.Descricao = e.NewValues[QueryCustomMetadata.ColumnNames.Descricao].ToString();
            q.QueryCustom = e.NewValues[QueryCustomMetadata.ColumnNames.QueryCustom].ToString();

            q.Save();
        }

        e.Cancel = true;
        gridQuery.CancelEdit();
        gridQuery.DataBind();
    }

    protected void gridQuery_HtmlRowPrepared(object sender, ASPxGridViewTableRowEventArgs e)
    {
    }


    //protected void grid_DataBinding(object sender, EventArgs e)
    //{      
    //    esUtility util = new esUtility();
    //    gridResultado.DataSource = util.ExecuteReader(esQueryType.Text, this.ManualSqlTextBox.Text.Trim());        
    //}



    #endregion


    protected void Page_Unload(object sender, EventArgs e)
    {
        //QueryPanel1.Query.ColumnsChanged -= new Korzh.EasyQuery.ColumnsChangedEventHandler(query_ColumnsChanged);
        //QueryPanel1.Query.ConditionsChanged -= new Korzh.EasyQuery.ConditionsChangedEventHandler(query_ConditionsChanged);
        //QueryPanel1.Query.SortOrderChanged -= new Korzh.EasyQuery.SortOrderChangedEventHandler(query_SortOrderChanged);
    }

    protected void LoadBtn_Click(object sender, EventArgs e)
    {
        if (SavedQueryUpload.HasFile)
        {
            try
            {
                TextReader reader = new StreamReader(SavedQueryUpload.PostedFile.InputStream);

                // Copy the byte array into a string.
                string QueryText = reader.ReadToEnd();

                reader.Close();

                QueryPanel1.Query.LoadFromString(QueryText);
            }
            catch (Exception ex)
            {
                ErrorLabel.Text = "Error during loading: " + ex.Message;
                ErrorLabel.Visible = true;
            }
        }
    }

    protected void SaveBtn_Click(object sender, EventArgs e)
    {
        Response.ClearHeaders();
        Response.ContentType = "text/xml";
        Response.Clear();

        Response.BufferOutput = true;
        Response.AddHeader("Content-Disposition", "attachment;filename=query.xml");

        string QueryText = QueryPanel1.Query.SaveToString();
        byte[] output = System.Text.UnicodeEncoding.UTF8.GetBytes(QueryText);

        Response.OutputStream.Write(output, 0, output.GetLength(0));

        Response.End();
    }

    protected void ClearBtn_Click(object sender, EventArgs e)
    {
        this.QueryPanel1.Query.Clear();
        this.SqlTextBox.Text = "";
        //this.ResultGrid.Columns.Clear();
        //
        this.ErrorLabel.Text = "";
        this.ErrorLabel.Visible = false;
    }


    protected void QueryPanel1_SqlExecute(object sender, Korzh.EasyQuery.WebControls.SqlExecuteEventArgs e)
    {
        //CheckConnection();
        //OleDbDataAdapter resultDA = new OleDbDataAdapter(e.SQL, DbConnection);

        //DataSet tempDS = new DataSet();
        //resultDA.Fill(tempDS, "Result");

        //StringWriter strWriter = new StringWriter();
        //tempDS.WriteXml(strWriter);
        //e.ListItems.LoadFromXml(strWriter.ToString());
    }

    protected void QueryPanel1_ListRequest(object sender, Korzh.EasyQuery.WebControls.ListRequestEventArgs e)
    {
        //if (e.ListName == "RegionList") {
        //    e.ListItems.Add("British Columbia", "BC");
        //    e.ListItems.Add("Colorado", "CO");
        //    e.ListItems.Add("Oregon", "OR");
        //    e.ListItems.Add("Washington", "WA");
        //}
    }

    protected void query_ColumnsChanged(object sender, Korzh.EasyQuery.ColumnsChangeEventArgs e)
    {
        this.UpdateSql();
    }

    protected void query_ConditionsChanged(object sender, Korzh.EasyQuery.ConditionsChangeEventArgs e)
    {
        this.UpdateSql();
    }

    protected void query_SortOrderChanged(object sender, Korzh.EasyQuery.SortOrderChangedEventArgs e)
    {
        this.UpdateSql();
    }

    protected void UpdateSql()
    {
        Korzh.EasyQuery.Query query = (Korzh.EasyQuery.Query)Session["Query"];
        try
        {
            query.BuildSQL();
            SqlTextBox.Text = query.Result.SQL;
            QueryTextFormats formats = new QueryTextFormats();
            formats.UseHtml = true;
            formats.UseMathSymbolsForOperators = true;
            //this.Literal1.Text = query.GetConditionsText(formats);

            //this.ResultGrid.Visible = false;
        }
        catch
        {
            this.SqlTextBox.Text = "";
            //this.Literal1.Text = "";
        }
    }

    public void CarregaGrid()
    {
        String query = SelectField.Value;
        if (query.StartsWith("SELECT", StringComparison.InvariantCultureIgnoreCase))
        {
            esUtility util = new esUtility();
            util.ConnectionName = "Financial_Consulta";

            DataTable d = new DataTable();
            try
            {
                d = util.FillDataTable(esQueryType.Text, query.Trim());
            }
            catch (Exception ex)
            {
                gridResultado.JSProperties["cpMessage"] = ex.Message;
                return; 
            }

            gridResultado.DataSource = d;
            gridResultado.Columns.Clear();
            gridResultado.AutoGenerateColumns = true;
            gridResultado.KeyFieldName = String.Empty;

        }
    }

    protected void UpdateManualQueries_Click(object sender, EventArgs e)
    {
        CarregaGrid();
    }

    protected void UpdateResultBtn_Click(object sender, EventArgs e)
    {
        CarregaGrid();
    }

    protected void QueryPanel1_CreateValueElement(object sender, Korzh.EasyQuery.WebControls.CreateValueElementEventArgs e)
    {
        // this method demonstrates an ability to change value elelements at run-time
        // for example in this case we change element from ListRowElement to EditRowElement if list of available values is too long

        //if (e.ConditionRow.Condition is Query.SimpleCondition) {
        //    Expression baseExpr = ((Query.SimpleCondition)e.ConditionRow.Condition).BaseExpr;
        //    DataModel.EntityAttr attr = ((EntityAttrExpr)baseExpr).Attribute;
        //    if (attr.DefaultEditor is SqlListValueEditor) {
        //        string sql = ((SqlListValueEditor)attr.DefaultEditor).SQL;
        //        if (ResultSetIsTooBig(sql)) { //or put your condition here
        //            e.Element = new EditXElement();
        //        }
        //    }
        //}
    }

    private bool ResultSetIsTooBig(string sql)
    {
        return true;
        //CheckConnection();
        //OleDbDataAdapter resultDA = new OleDbDataAdapter(sql, DbConnection);

        //DataSet tempDS = new DataSet();
        //resultDA.Fill(tempDS, "Result");
        //return tempDS.Tables[0].Rows.Count > 100;
    }

    protected void Button1_Click(object sender, EventArgs e)
    {
        //string path = AppDomain.CurrentDomain.BaseDirectory;
        //path += "data\\nwind2.xml";
        //query.Model.LoadFromFile(path);
        //QueryPanel1.UpdateModelInfo();

        //QueryColumnsPanel1.UpdateModelInfo();
    }



    protected void ExportExcelBtn_Click(object sender, EventArgs e)
    {
        ASPxGridViewExporter gridExport = this.FindControl("gridExport") as ASPxGridViewExporter;
        gridExport.WriteXlsToResponse();
    }


    private void BuscaFinancial_Consulta()
    {
        List<EntitySpaces.Interfaces.esConnectionElement> lstConnectionElement = EntitySpaces.Interfaces.esConfigSettings.ConnectionInfo.Connections.Cast<esConnectionElement>().ToList();

        EntitySpaces.Interfaces.esConnectionElement esConnection = lstConnectionElement.Find(x => x.Name.Equals("Financial_Consulta"));

        if (esConnection == null)
        {
            ClientScript.RegisterClientScriptBlock(this.GetType(), "ClosePopUp", "<script>alert('Não foi possível encontrar a conexão Financial_Consulta no Web.Config.');</script>");
            ClientScript.RegisterStartupScript(typeof(Page), "closePage", "window.close();", true);
            return;
        }
    }

    protected void gridResultado_OnBeforePerformDataSelect(object sender, EventArgs e)
    {
        CarregaGrid();
    }

}

