﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="StatusClientes.aspx.cs" Inherits="Consultas_StatusClientes" %>

<%@ Register Assembly="System.Web, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"
    Namespace="System.Web.UI.HtmlControls" TagPrefix="cc2" %>

<%@ Register Assembly="EntitySpaces.Web" Namespace="EntitySpaces.Web" TagPrefix="cc1" %>

<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxe" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxwgv" %>

<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxpc" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title>Untitled Page</title>
    <link href="~/css/forms.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" language="Javascript" src="../js/global.js"></script>     
    <script type="text/javascript" language="Javascript"> 
    document.onkeydown=onDocumentKeyDown;
    </script>
    
</head>
<body>    
    <form id="form1" runat="server">
            
    <div class="divPanel">
    <table width="100%"><tr><td>
    <div id="container">
    
    <div id="header">Status Geral de Clientes</div>
        
    <div id="mainContent">    
                              
        <div class="linkButton">                       
            <asp:LinkButton ID="btnPDF" runat="server" Font-Overline="false" CssClass="btnPdf" OnClick="btnPDF_Click"><asp:Literal ID="Literal4" runat="server" Text="Gerar PDF"/><div></div></asp:LinkButton>
            <asp:LinkButton ID="btnExcel" runat="server" Font-Overline="false" CssClass="btnExcel" OnClick="btnExcel_Click"><asp:Literal ID="Literal5" runat="server" Text="Gerar Excel"/><div></div></asp:LinkButton>
            <asp:LinkButton ID="btnRefresh" runat="server" Font-Overline="false" CssClass="btnRefresh" 
                    OnClientClick="gridConsulta.PerformCallback('btnRefresh'); return false;"><asp:Literal ID="Literal7" runat="server" Text="Atualizar"/><div></div></asp:LinkButton>
        </div>
               
       <div class="divDataGrid">
            
                <dxwgv:ASPxGridView ID="gridConsulta" runat="server" EnableCallBacks="true" 
                        ClientInstanceName="gridConsulta" AutoGenerateColumns="False" 
                        DataSourceID="EsDSCliente" KeyFieldName="IdCliente"
                        OnHtmlRowCreated="gridConsulta_HtmlRowCreated" Width="100%"
                        OnCustomUnboundColumnData="gridConsulta_CustomUnboundColumnData"
                        OnCustomCallback="gridConsulta_CustomCallback">                    
                <Columns>
                                                            
                    <dxwgv:GridViewDataComboBoxColumn Caption="Grupo" FieldName="IdGrupoProcessamento" VisibleIndex="1" Width="15%" >
                        <EditFormSettings Visible="False" />
                        <PropertiesComboBox DataSourceID="EsDSGrupoProcessamento" TextField="Descricao" ValueField="IdGrupoProcessamento" DropDownStyle="DropDownList" />
                    </dxwgv:GridViewDataComboBoxColumn>
       
                    <dxwgv:GridViewDataTextColumn FieldName="IdCliente" ReadOnly="True" VisibleIndex="2" Width="10%" CellStyle-HorizontalAlign="left"/>
                    <dxwgv:GridViewDataTextColumn FieldName="Apelido" VisibleIndex="3" Width="50%"/>
                    <dxwgv:GridViewDataDateColumn FieldName="DataDia" VisibleIndex="4" Width="10%"/>                    
                    <dxwgv:GridViewDataTextColumn FieldName="StatusDescricao" Caption="Status" UnboundType="String" VisibleIndex="5" Width="15%"/>
                    
                    <dxwgv:GridViewDataTextColumn FieldName="Status" Visible="False"/>
                </Columns>            
                                
                <SettingsPager PageSize="30"></SettingsPager>
                <Settings ShowFilterRow="True" ShowStatusBar="Visible" VerticalScrollBarMode="Visible" 
                         VerticalScrollableHeight="340" VerticalScrollBarStyle="Virtual" />
                <SettingsText EmptyDataRow="0 registros"/>

                <Images />
                <Styles>                
                    <Header ImageSpacing="5px" SortingImageSpacing="5px" />
                    <AlternatingRow Enabled="True" />
                    <Cell Wrap="False" />
                </Styles>                    
                              
                </dxwgv:ASPxGridView>                                                    
            </div>
                      
    </div>
    </div>
    </td></tr></table>
    </div>   

    <dxwgv:ASPxGridViewExporter ID="gridExport" runat="server" GridViewID="gridConsulta" />        
        
    <cc1:esDataSource ID="EsDSCliente" runat="server" OnesSelect="EsDSCliente_esSelect" LowLevelBind="true" />
    <cc1:esDataSource ID="EsDSGrupoProcessamento" runat="server" OnesSelect="EsDSGrupoProcessamento_esSelect" />
        
    </form>        
</body>
</html>