﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using Financial.Security;
using Financial.ContaCorrente;
using System.Text;
using DevExpress.Web;
using Financial.Web.Util;
using System.Drawing;
using Financial.Investidor;
using Financial.Fundo;
using Financial.Investidor.Enums;
using Financial.Web.Common;
using System.Collections.Generic;

public partial class Consultas_CalculoProvisao : ConsultaBasePage {
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    new protected void Page_Load(object sender, EventArgs e) {
        btnEditCodigo.Focus();
        base.Page_Load(sender, e);
    }

    #region DataSources
    protected void EsDSCalculoProvisao_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e) {
        if (!Page.IsPostBack) {
            gridConsulta.Visible = false;
            e.Collection = new CalculoProvisaoCollection();
        }
        else {

            // Confere Campos Obrigatorios para o caso de Mudança de Pagina e Scroolbar
            this.TrataCamposObrigatorios();
            //

            CalculoProvisaoHistoricoQuery calculoProvisaoHistoricoQuery = new CalculoProvisaoHistoricoQuery("A");
            CarteiraQuery carteiraQuery = new CarteiraQuery("C");

            calculoProvisaoHistoricoQuery.Select(carteiraQuery.Apelido,
                            calculoProvisaoHistoricoQuery.DataHistorico,
                            calculoProvisaoHistoricoQuery.DataFimApropriacao,
                            calculoProvisaoHistoricoQuery.DataPagamento,
                            calculoProvisaoHistoricoQuery.ValorDia,
                            calculoProvisaoHistoricoQuery.ValorAcumulado);
            calculoProvisaoHistoricoQuery.Where(calculoProvisaoHistoricoQuery.IdTabela.Equal(Convert.ToInt32(hiddenIdTabela.Text)));
            calculoProvisaoHistoricoQuery.Where(calculoProvisaoHistoricoQuery.DataHistorico.GreaterThanOrEqual(Convert.ToDateTime(textDataInicio.Text)));
            calculoProvisaoHistoricoQuery.Where(calculoProvisaoHistoricoQuery.DataHistorico.LessThanOrEqual(Convert.ToDateTime(textDataFim.Text)));
            calculoProvisaoHistoricoQuery.InnerJoin(carteiraQuery).On(calculoProvisaoHistoricoQuery.IdCarteira == carteiraQuery.IdCarteira);

            calculoProvisaoHistoricoQuery.OrderBy(calculoProvisaoHistoricoQuery.DataHistorico.Descending);

            CalculoProvisaoHistoricoCollection calculoProvisaoHistoricoCollection = new CalculoProvisaoHistoricoCollection();
            calculoProvisaoHistoricoCollection.Load(calculoProvisaoHistoricoQuery);
            
            gridConsulta.Visible = true;
            e.Collection = calculoProvisaoHistoricoCollection;
        }

    }

    protected void EsDSTabelaTaxas_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e) {
        TabelaProvisaoQuery tabelaProvisaoQuery = new TabelaProvisaoQuery("T");
        CarteiraQuery carteiraQuery = new CarteiraQuery("C");
        CadastroProvisaoQuery cadastroProvisaoQuery = new CadastroProvisaoQuery("A");
        PermissaoClienteQuery permissaoClienteQuery = new PermissaoClienteQuery("P");
        UsuarioQuery usuarioQuery = new UsuarioQuery("U");
        ClienteQuery clienteQuery = new ClienteQuery("E");
        tabelaProvisaoQuery.Select(tabelaProvisaoQuery.IdTabela,
                              tabelaProvisaoQuery.DataReferencia,
                              carteiraQuery.IdCarteira,
                              carteiraQuery.Apelido,
                              cadastroProvisaoQuery.IdCadastro,
                              cadastroProvisaoQuery.Descricao);
        tabelaProvisaoQuery.InnerJoin(cadastroProvisaoQuery).On(tabelaProvisaoQuery.IdCadastro == cadastroProvisaoQuery.IdCadastro);
        tabelaProvisaoQuery.InnerJoin(carteiraQuery).On(tabelaProvisaoQuery.IdCarteira == carteiraQuery.IdCarteira);
        tabelaProvisaoQuery.InnerJoin(permissaoClienteQuery).On(permissaoClienteQuery.IdCliente == carteiraQuery.IdCarteira);
        tabelaProvisaoQuery.InnerJoin(usuarioQuery).On(permissaoClienteQuery.IdUsuario == usuarioQuery.IdUsuario);
        tabelaProvisaoQuery.InnerJoin(clienteQuery).On(carteiraQuery.IdCarteira == clienteQuery.IdCliente);
        tabelaProvisaoQuery.Where(usuarioQuery.Login == HttpContext.Current.User.Identity.Name,
                               clienteQuery.TipoControle.NotEqual(TipoControleCliente.ApenasCotacao),
                               clienteQuery.StatusAtivo.Equal(StatusAtivoCliente.Ativo));
        tabelaProvisaoQuery.OrderBy(clienteQuery.Apelido.Ascending);

        TabelaProvisaoCollection coll = new TabelaProvisaoCollection();
        coll.Load(tabelaProvisaoQuery);

        // Assign the esDataSourceSelectEvenArgs Collection property
        e.Collection = coll;
    }
    #endregion

    /// <summary>
    /// 
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void ASPxCallback1_Callback(object source, DevExpress.Web.CallbackEventArgs e) {
        string texto = "";
        if (!String.IsNullOrEmpty(e.Parameter) && e.Parameter != "null") {
            #region Procura o idTabela se o Parameter for do tipo Int
            //
            //int idTabela;
            //if (int.TryParse(e.Parameter, out idTabela)) {
            int idTabela = Convert.ToInt32(e.Parameter);
            TabelaProvisaoQuery tabelaTaxas = new TabelaProvisaoQuery("T");
            CarteiraQuery carteira = new CarteiraQuery("C");
            CadastroProvisaoQuery cadastroTaxas = new CadastroProvisaoQuery("P");
            tabelaTaxas.Select(tabelaTaxas.IdTabela,
                                  tabelaTaxas.DataReferencia,
                                  carteira.IdCarteira,
                                  carteira.Apelido,
                                  cadastroTaxas.IdCadastro,
                                  cadastroTaxas.Descricao);
            tabelaTaxas.InnerJoin(cadastroTaxas).On(tabelaTaxas.IdCadastro == cadastroTaxas.IdCadastro);
            tabelaTaxas.InnerJoin(carteira).On(tabelaTaxas.IdCarteira == carteira.IdCarteira);
            tabelaTaxas.Where(tabelaTaxas.IdTabela == idTabela);

            TabelaProvisaoCollection tabelaProvisaoCollection = new TabelaProvisaoCollection();
            tabelaProvisaoCollection.Load(tabelaTaxas);

            //
            if (tabelaProvisaoCollection.HasData) {
                //int idTabelaAux = (int)tabelaTaxaProvisaoCollection[0].GetColumn(TabelaTaxaProvisaoMetadata.ColumnNames.IdTabela);                
                string nome = (string)tabelaProvisaoCollection[0].GetColumn(CarteiraMetadata.ColumnNames.Apelido);
                string descricao = (string)tabelaProvisaoCollection[0].GetColumn(CadastroProvisaoMetadata.ColumnNames.Descricao);
                DateTime dataReferencia = (DateTime)tabelaProvisaoCollection[0].GetColumn(TabelaProvisaoMetadata.ColumnNames.DataReferencia);

                texto = ' ' + nome + " ->   " + labelTaxa.Text + descricao + " ->   " + labelDataReferencia.Text + dataReferencia.ToString("d");
            }
            //}
            #endregion
        }
        //
        e.Result = texto;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridTabela_CustomDataCallback(object sender, DevExpress.Web.ASPxGridViewCustomDataCallbackEventArgs e) {
        e.Result = gridTabela.GetRowValues(Convert.ToInt32(e.Parameters), "IdTabela") +
                        "|" + gridTabela.GetRowValues(Convert.ToInt32(e.Parameters), "IdCarteira");
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridTabela_HtmlRowCreated(object sender, DevExpress.Web.ASPxGridViewTableRowEventArgs e) {
        e.Row.Attributes.Add("onmouseover", "backgroundColor = this.style.backgroundColor; this.style.backgroundColor='#E0E9EB';");
        e.Row.Attributes.Add("onmouseout", "this.style.backgroundColor=backgroundColor;");
    }

    #region btnRun_Click, btnPDF_Click, btnExcel_Click

    /// <summary>
    /// Trata os campos Obrigatorios do Form
    /// </summary>
    private void TrataCamposObrigatorios() {
        #region Campos obrigatórios
        List<Control> controles = new List<Control>(new Control[] { 
            this.btnEditCodigo, this.textDataInicio, this.textDataFim });

        if (base.TestaObrigatorio(controles) != "") {
            throw new Exception("Campos com * são obrigatórios!");
        }
        #endregion
    }

    new protected void btnRun_Click(object sender, EventArgs e) {
        this.TrataCamposObrigatorios();
        base.btnRun_Click(sender, e); // DataBind do Grid
    }

    new protected void btnPDF_Click(object sender, EventArgs e) {
        this.TrataCamposObrigatorios();
        this.SelecionaRelatorio(VisaoRelatorio.visaoPDF);
    }

    new protected void btnExcel_Click(object sender, EventArgs e) {
        this.TrataCamposObrigatorios();
        this.SelecionaRelatorio(VisaoRelatorio.visaoExcel);
    }

    /// <summary>
    /// Seleciona o Relatorio a Ser Executado
    /// </summary>
    /// <param name="visao">PDF, Excel ou Report</param>
    private void SelecionaRelatorio(string visao) {
        Response.BufferOutput = true;
        //
        // Salva os Parametros na Session
        Session["gridExportCalculoProvisao"] = gridExport;
        Response.Redirect("~/Consultas/CalculoProvisaoExporta.aspx?Visao=" + visao);
    }

    #endregion
}