using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Financial.Investidor;
using Financial.Fundo;

public partial class Search : Financial.Web.Common.BasePage
{
    private string filtro;

    new protected void Page_Load(object sender, EventArgs e)
    {
        filtro = "%" + Request.QueryString["search"] + "%";
        gridSearch.DataSource = esDSSearch;
        gridSearch.DataBind();
    }
        
    protected void esDSSearch_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        ClienteCollection coll = new ClienteCollection();
        coll.Query.Where(coll.Query.Nome.Like(filtro));
        coll.Query.OrderBy(coll.Query.Nome.Ascending);
        coll.LoadAll();

        CarteiraCollection coll2 = new CarteiraCollection();
        coll2.Query.Where(coll.Query.Nome.Like(filtro));
        coll2.Query.OrderBy(coll.Query.Nome.Ascending);
        coll2.LoadAll();

        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }
}
