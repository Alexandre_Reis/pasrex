﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Exportacao.aspx.cs" Inherits="Exportacao" %>

<%@ Register Assembly="System.Web, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a" Namespace="System.Web.UI.HtmlControls" TagPrefix="cc2" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxcb" %>
<%@ Register Assembly="System.Web, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a" Namespace="System.Web.UI" TagPrefix="cc1" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxnb" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxe" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxwgv" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxpc" %>
<%@ Register Assembly="EntitySpaces.Web" Namespace="EntitySpaces.Web" TagPrefix="cc1" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxlp" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxrp" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxp" %>

<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxtc" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxw" %>

<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <link href="~/css/forms.css" rel="stylesheet" type="text/css" />

    <script type="text/javascript" language="Javascript" src="../js/global.js"></script>

    <script type="text/javascript">
        function btnExportaClick(tipoData) {
            LoadingPanel.Show();
            callbackExporta.SendCallback(tipoData);
        }

        function btnExporta1Click(tipoExportacao) {
            LoadingPanel1.Show();
            callbackExporta1.SendCallback(tipoExportacao);
        }

        // Select All dos AspGridLookup
        function OnAllCheckedChanged1(s, e, grid) {
            if (s.GetChecked()) {
                grid.GetGridView().SelectRows();
            }
            else {
                grid.GetGridView().UnselectRows();
            }
        }

        // Checkbox do Select All do GridConsulta1
        function GridConsulta1_OnAllChecked(s, e, grid) {
            if (s.GetChecked()) {
                grid.SelectRows();
            }
            else {
                grid.UnselectRows();
            }
        }

        function HabilitaDiasConfidencialidade(s, e) {
            if (s.GetValue() == 100 || s.GetValue() == 101) {
                spinDiasConf.SetEnabled(true);
            }
            else {
                spinDiasConf.SetEnabled(false);
            }
        }

    </script>

</head>
<body>
    <dxcb:ASPxCallback ID="callbackExporta" runat="server" OnCallback="callbackExporta_Callback">
        <ClientSideEvents CallbackComplete="function(s, e) 
        {   
            LoadingPanel.Hide();
            if(e && e.result.length){
                if(e.result.indexOf('redirect') === 0){
                    var newLocation = (e.result.split(':'))[1];
                    var iframeId = 'iframe_download';
                    var iframe = document.getElementById(iframeId);

                    if (!iframe && document.createElement && (iframe =
                        document.createElement('iframe'))) {
               
                        iframe.name = iframe.id = iframeId;
                        iframe.width = 0;
                        iframe.height = 0;
                        iframe.src = 'about:blank';
                        document.body.appendChild(iframe);                                                                    
                    }
                    iframe.src = newLocation;
                   
                }else{
                    alert(e.result);
                }
            }
        }" />
    </dxcb:ASPxCallback>

    <dxcb:ASPxCallback ID="callbackExporta1" runat="server" OnCallback="callbackExporta1_Callback">
        <ClientSideEvents CallbackComplete="function(s, e) 
        {   
            LoadingPanel1.Hide();
            if(e && e.result.length){
                if(e.result.indexOf('redirect') === 0){
                    var newLocation = (e.result.split(':'))[1];
                    
                    var iframeId = 'iframe_download';
                    var iframe = document.getElementById(iframeId);

                    if (!iframe && document.createElement && (iframe = document.createElement('iframe'))) {
               
                        iframe.name = iframe.id = iframeId;
                        iframe.width = 0;
                        iframe.height = 0;
                        iframe.src = 'about:blank';
                        document.body.appendChild(iframe);                                                                    
                    }
                    iframe.src = newLocation;
                   
                } else {
                    alert(e.result);
                }
            }
        }" />
    </dxcb:ASPxCallback>

    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" AsyncPostBackTimeout="360000" runat="server" EnableScriptGlobalization="true" EnableScriptLocalization="true" />

        <asp:UpdatePanel ID="UpdatePanel2" runat="server">
            <ContentTemplate>
                <div class="divPanel divPanelNew">
                    <div id="container_small">
                        <div id="header">
                            <asp:Label ID="lblHeader" runat="server" Text="Exportação de Arquivos"></asp:Label>
                        </div>
                        <div id="mainContent" style="width: 850px">
                            <div class="reportFilter">
                                <div class="dataMessage">
                                </div>
                                <dxrp:ASPxRoundPanel ID="ASPxRoundPanel1" runat="server" ShowHeader="False" BackColor="White" Border-BorderColor="#AECAF0">
                                    <Border BorderColor="#AECAF0"></Border>
                                    <PanelCollection>
                                        <dxp:PanelContent runat="server">
                                            <dxtc:ASPxPageControl ID="tabArquivos" runat="server"
                                                ClientInstanceName="tabArquivos" Height="100%" Width="100%" TabSpacing="0px"
                                                ActiveTabIndex="1">
                                                <TabPages>
                                                    <dxtc:TabPage Text="Exportação Informes Legais">
                                                        <ContentCollection>
                                                            <dxw:ContentControl runat="server">
                                                                <div class="divDataGrid">
                                                                    <dxwgv:ASPxGridView ID="gridConsulta" runat="server" EnableCallBacks="true" ClientInstanceName="gridConsulta"
                                                                        AutoGenerateColumns="False" DataSourceID="EsDSCliente" KeyFieldName="IdCliente"
                                                                        OnHtmlRowCreated="gridConsulta_HtmlRowCreated" Width="850px" OnCustomUnboundColumnData="gridConsulta_CustomUnboundColumnData"
                                                                        OnCustomCallback="gridConsulta_CustomCallback">
                                                                        <Columns>
                                                                            <dxwgv:GridViewCommandColumn ShowSelectCheckbox="True" VisibleIndex="0" Width="5%" ButtonType="Image" ShowClearFilterButton="True">
                                                                                <HeaderStyle HorizontalAlign="Center" />
                                                                                <HeaderTemplate>
                                                                                    <dxe:ASPxCheckBox ID="cbAll" ClientInstanceName="cbAll" runat="server" ToolTip="Seleciona todos os CheckBoxs de todas as páginas" BackColor="white" ClientSideEvents-CheckedChanged="function(s, e) {OnAllCheckedChanged(s, e, gridConsulta);}" OnInit="CheckBoxSelectAll" />
                                                                                </HeaderTemplate>
                                                                            </dxwgv:GridViewCommandColumn>
                                                                            <dxwgv:GridViewDataTextColumn FieldName="IdCliente" ReadOnly="True" VisibleIndex="1" Width="7%" CellStyle-HorizontalAlign="left">
                                                                                <CellStyle HorizontalAlign="Left">
                                                                                </CellStyle>
                                                                            </dxwgv:GridViewDataTextColumn>
                                                                            <dxwgv:GridViewDataTextColumn FieldName="Apelido" VisibleIndex="2" Width="25%"
                                                                                Settings-AutoFilterCondition="Contains">
                                                                                <Settings AutoFilterCondition="Contains"></Settings>
                                                                            </dxwgv:GridViewDataTextColumn>
                                                                            <dxwgv:GridViewDataDateColumn FieldName="DataDia" VisibleIndex="3" Width="9%" />
                                                                            <dxwgv:GridViewDataTextColumn FieldName="StatusDescricao" Caption="Status"
                                                                                UnboundType="String" VisibleIndex="4" Width="12%"
                                                                                Settings-AutoFilterCondition="Contains">
                                                                                <Settings AutoFilterCondition="Contains"></Settings>
                                                                            </dxwgv:GridViewDataTextColumn>
                                                                            <dxwgv:GridViewDataTextColumn FieldName="Status" Visible="False" />
                                                                            <dxwgv:GridViewDataComboBoxColumn Caption="Tipo Cliente" FieldName="IdTipo" VisibleIndex="5" Width="15%">
                                                                                <EditFormSettings Visible="False" />
                                                                                <PropertiesComboBox DataSourceID="EsDSTipoCliente" TextField="Descricao" ValueField="IdTipo" />
                                                                            </dxwgv:GridViewDataComboBoxColumn>
                                                                            <dxwgv:GridViewDataComboBoxColumn Caption="Ativo" FieldName="StatusAtivo" VisibleIndex="8"
                                                                                Width="5%" ExportWidth="100">
                                                                                <EditFormSettings Visible="False" />
                                                                                <PropertiesComboBox EncodeHtml="false">
                                                                                    <Items>
                                                                                        <dxe:ListEditItem Value="1" Text="<div title='Sim'>Sim</div>" />
                                                                                        <dxe:ListEditItem Value="2" Text="<div title='Não'>Não</div>" />
                                                                                    </Items>
                                                                                </PropertiesComboBox>
                                                                            </dxwgv:GridViewDataComboBoxColumn>

                                                                        </Columns>
                                                                        <SettingsPager Mode="ShowAllRecords">
                                                                        </SettingsPager>
                                                                        <Settings ShowFilterRow="True" ShowStatusBar="Visible" VerticalScrollBarMode="Visible" />
                                                                        <SettingsText EmptyDataRow="0 registros" />
                                                                        <Images />
                                                                        <Styles>
                                                                            <Header ImageSpacing="5px" SortingImageSpacing="5px" />
                                                                            <AlternatingRow Enabled="True" />
                                                                            <Cell Wrap="False" />
                                                                        </Styles>
                                                                        <SettingsCommandButton>
                                                                            <ClearFilterButton Image-Url="../imagens/funnel--minus.png">
                                                                                <Image Url="../imagens/funnel--minus.png">
                                                                                </Image>
                                                                            </ClearFilterButton>
                                                                        </SettingsCommandButton>
                                                                    </dxwgv:ASPxGridView>
                                                                </div>
                                                                <table border="0">
                                                                    <tr>
                                                                        <td>

                                                                            <table cellpadding="2" cellspacing="2" border="0">
                                                                                <tr>
                                                                                    <td style="width: 50px;">
                                                                                        <asp:Label ID="label2" runat="server" CssClass="labelRequired labelFloat" Text="Diário:" />
                                                                                    </td>
                                                                                    <td>
                                                                                        <dxe:ASPxComboBox ID="dropTipo_ddmmyyyy" runat="server" CssClass="dropDownList" ClientInstanceName="dropTipo_ddmmyyyy" Width="250px">
                                                                                            <Items>
                                                                                                <dxe:ListEditItem Value="" Text="" />
                                                                                                <dxe:ListEditItem Value="1" Text="BacenJud" />
                                                                                                <dxe:ListEditItem Value="13" Text="BacenCCS" />
                                                                                                <dxe:ListEditItem Value="2" Text="Contábil Folhamatic" />
                                                                                                <dxe:ListEditItem Value="7" Text="Contábil Sinacor" />
                                                                                                <dxe:ListEditItem Value="10" Text="CCOU Sinacor" />
                                                                                                <dxe:ListEditItem Value="20" Text="Informe Diário CVM – Versão 1.0" />
                                                                                                <dxe:ListEditItem Value="3" Text="Informe Diário CVM – Versão 2.0" />
                                                                                                <dxe:ListEditItem Value="19" Text="Informe Diário CVM – Versão 3.0" />
                                                                                                <dxe:ListEditItem Value="4" Text="Informe Diário Anbima" />
                                                                                                <dxe:ListEditItem Value="5" Text="XML Anbima" />
                                                                                                <dxe:ListEditItem Value="9" Text="YMF: TX2" />
                                                                                                <dxe:ListEditItem Value="8" Text="YMF: TX3" />
                                                                                                <dxe:ListEditItem Value="6" Text="Matriz Ativos" />
                                                                                                <dxe:ListEditItem Value="17" Text="Informações Complementares - CVM - PDF" />
                                                                                                <dxe:ListEditItem Value="18" Text="Informações Complementares - CVM - XML" />
                                                                                            </Items>
                                                                                            <ClientSideEvents SelectedIndexChanged="function(s, e) {                                                                                                                                                                                        
                                                                           if(s.GetValue() == 6) {
                                                                                var d=new Date();
                                                                                textData_ddmmyyyy.SetDate(d);
                                                                                textData_ddmmyyyy.SetEnabled(false);                                                                                
                                                                           }                                                                                            
                                                                           else {
                                                                                textData_ddmmyyyy.SetEnabled(true);
                                                                           }
                                                                           return false;}" />
                                                                                        </dxe:ASPxComboBox>
                                                                                    </td>
                                                                                    <td class="td_Label" style="width: 50px;">
                                                                                        <asp:Label ID="label3" runat="server" CssClass="labelRequired" Text="Data:" />
                                                                                    </td>
                                                                                    <td>
                                                                                        <dxe:ASPxDateEdit ID="textData_ddmmyyyy" runat="server" ClientInstanceName="textData_ddmmyyyy" />
                                                                                    </td>
                                                                                    <td>
                                                                                        <div id="Div1" class="linkButton linkButtonNoBorder  linkButtonNoMargin">
                                                                                            <asp:LinkButton ID="btnTipo_ddmmyyyy" runat="server" Font-Overline="false" CssClass="btnRun"
                                                                                                OnClientClick="btnExportaClick('ddmmyyyy'); return false;">
                                                                                                <asp:Literal ID="Literal2" runat="server" Text="Exportar" /><div>
                                                                                                </div>
                                                                                            </asp:LinkButton></div></td></tr></table></td></tr><tr>
                                                                        <td>

                                                                            <table cellpadding="2" cellspacing="2" border="0">
                                                                                <tr>
                                                                                    <td style="width: 50px;">
                                                                                        <asp:Label ID="label1" runat="server" CssClass="labelRequired labelFloat" Text="Mensal:" />
                                                                                    </td>
                                                                                    <td>
                                                                                        <dxe:ASPxComboBox ID="dropTipo_mmyyyy" runat="server" CssClass="dropDownList" ClientInstanceName="dropTipo_mmyyyy" Width="250px">
                                                                                            <Items>
                                                                                                <dxe:ListEditItem Value="" Text="" />
                                                                                                <dxe:ListEditItem Value="100" Text="CDA 2.0" />
                                                                                                <dxe:ListEditItem Value="101" Text="CDA 3.0" />
                                                                                                <dxe:ListEditItem Value="1014" Text="CDA 4.0" />

                                                                                                <dxe:ListEditItem Value="102" Text="PICL" />

                                                                                                <dxe:ListEditItem Value="103" Text="Perfil Mensal V1.0(CVM) - XML" />
                                                                                                <dxe:ListEditItem Value="140" Text="Perfil Mensal V3.0(CVM) - XML" />

                                                                                                <dxe:ListEditItem Value="106" Text="Perfil Mensal V1.0(CVM) - PDF" />
                                                                                                <dxe:ListEditItem Value="141" Text="Perfil Mensal V3.0(CVM) - PDF" />

                                                                                                <dxe:ListEditItem Value="121" Text="Lâmina Info. Essenciais - Versão 1 - PDF" />
                                                                                                <dxe:ListEditItem Value="120" Text="Lâmina Info. Essenciais - Versão 2 - PDF" />

                                                                                                <dxe:ListEditItem Value="131" Text="Lâmina Info. Essenciais - Versão 1 - XML" />
                                                                                                <dxe:ListEditItem Value="130" Text="Lâmina Info. Essenciais - Versão 2 - XML" />

                                                                                                <dxe:ListEditItem Value="104" Text="Ranking Anbima Ativo" />

                                                                                                <dxe:ListEditItem Value="108" Text="Ranking Anbima Passivo" />
                                                                                                <dxe:ListEditItem Value="109" Text="Cosif4016" />
                                                                                                <dxe:ListEditItem Value="110" Text="Cosif4010" />
                                                                                            </Items>
                                                                                            <ClientSideEvents SelectedIndexChanged="function(s, e) {
                                                                                                        HabilitaDiasConfidencialidade(s, e);
                                                                                                   }"
                                                                                                Init="function(s, e) {
                                                                                                        HabilitaDiasConfidencialidade(s, e);
                                                                                                   }" />
                                                                                        </dxe:ASPxComboBox>
                                                                                    </td>
                                                                                    <td class="td_Label" style="width: 50px;">
                                                                                        <asp:Label ID="label4" runat="server" CssClass="labelRequired" Text="Data:" />
                                                                                    </td>
                                                                                    <td>
                                                                                        <dxe:ASPxDateEdit ID="textData_mmyyyy" runat="server" ClientInstanceName="textData_mmyyyy"
                                                                                            EditFormat="Custom" EditFormatString="MM/yyyy" />
                                                                                    </td>
                                                                                    <td class="td_Label" style="width: 40px;">
                                                                                        <asp:Label ID="label10" runat="server" CssClass="labelRequired" Text="Dias:" />
                                                                                    </td>
                                                                                    <td>
                                                                                        <dxe:ASPxSpinEdit ID="spinDiasConf" runat="server" ClientInstanceName="spinDiasConf"
                                                                                            NumberType="Integer" Number="0" MinValue="0" MaxValue="90"
                                                                                            AllowNull="false" Width="98" />
                                                                                    </td>
                                                                                    <td>
                                                                                        <div id="Div2" class="linkButton linkButtonNoBorder  linkButtonNoMargin">
                                                                                            <asp:LinkButton ID="btnTipo_mmyyyy" runat="server" Font-Overline="false" CssClass="btnRun"
                                                                                                OnClientClick="btnExportaClick('mmyyyy'); return false;">
                                                                                                <asp:Literal ID="Literal1" runat="server" Text="Exportar" /><div>
                                                                                                </div>
                                                                                            </asp:LinkButton></div></td></tr></table></td></tr><tr>
                                                                        <td>

                                                                            <table cellpadding="2" cellspacing="2" border="0">
                                                                                <tr>
                                                                                    <td style="width: 50px;">
                                                                                        <asp:Label ID="label5" runat="server" CssClass="labelRequired labelFloat" Text="Anual:" />
                                                                                    </td>
                                                                                    <td>
                                                                                        <dxe:ASPxComboBox ID="dropTipo_yyyy" runat="server" CssClass="dropDownList" ClientInstanceName="dropTipo_yyyy" Width="250px">
                                                                                            <Items>
                                                                                                <dxe:ListEditItem Value="" Text="" />
                                                                                                <dxe:ListEditItem Value="500" Text="DIRF" />
                                                                                                <dxe:ListEditItem Value="501" Text="Informe Rend." />
                                                                                            </Items>
                                                                                        </dxe:ASPxComboBox>
                                                                                    </td>
                                                                                    <td class="td_Label" style="width: 50px;">
                                                                                        <asp:Label ID="label6" runat="server" CssClass="labelRequired" Text="Data:" />
                                                                                    </td>
                                                                                    <td>
                                                                                        <dxe:ASPxDateEdit ID="textData_yyyy" runat="server" ClientInstanceName="textData_yyyy"
                                                                                            EditFormat="Custom" EditFormatString="yyyy" />
                                                                                    </td>
                                                                                    <td>
                                                                                        <div id="Div3" class="linkButton linkButtonNoBorder  linkButtonNoMargin">
                                                                                            <asp:LinkButton ID="btnTipo_yyyy" runat="server" Font-Overline="false" CssClass="btnRun"
                                                                                                OnClientClick="btnExportaClick('yyyy'); return false;">
                                                                                                <asp:Literal ID="Literal3" runat="server" Text="Exportar" /><div>
                                                                                                </div>
                                                                                            </asp:LinkButton></div></td></tr></table></td></tr><tr>
                                                                        <td>

                                                                            <table cellpadding="2" cellspacing="2" border="0">
                                                                                <tr>
                                                                                    <td>
                                                                                        <asp:Label ID="label7" runat="server" CssClass="labelRequired labelFloat" Text="Período:" />
                                                                                    </td>
                                                                                    <td>
                                                                                        <dxe:ASPxComboBox ID="dropTipo_Periodo" runat="server" CssClass="dropDownList" ClientInstanceName="dropTipo_Periodo" Width="250px">
                                                                                            <Items>
                                                                                                <dxe:ListEditItem Value="" Text="" />
                                                                                                <dxe:ListEditItem Value="1000" Text="Contábil Att" />
                                                                                                <dxe:ListEditItem Value="1010" Text="Contábil Infobanc" />
                                                                                                <dxe:ListEditItem Value="1011" Text="Contábil Infobanc - Conta Full" />
                                                                                            </Items>
                                                                                        </dxe:ASPxComboBox>
                                                                                    </td>
                                                                                    <td class="td_Label" style="width: 50px;">
                                                                                        <asp:Label ID="label8" runat="server" CssClass="labelRequired" Text="Ínicio:" />
                                                                                    </td>
                                                                                    <td>
                                                                                        <dxe:ASPxDateEdit ID="textDataInicio_Periodo" runat="server" ClientInstanceName="textDataInicio_Periodo" />
                                                                                    </td>
                                                                                    <td style="width: 35px;" class="td_Label">
                                                                                        <asp:Label ID="label22" runat="server" CssClass="labelRequired" Text="Fim:"></asp:Label></td><td>
                                                                                        <dxe:ASPxDateEdit ID="textDataFim_Periodo" runat="server" ClientInstanceName="textDataFim_Periodo" />
                                                                                    </td>
                                                                                    <td>
                                                                                        <div id="Div4" class="linkButton linkButtonNoBorder linkButtonNoMargin">
                                                                                            <asp:LinkButton ID="btnTipo_Periodo" runat="server" Font-Overline="false" CssClass="btnRun"
                                                                                                OnClientClick="btnExportaClick('Periodo'); return false;">
                                                                                                <asp:Literal ID="Literal4" runat="server" Text="Exportar" /><div>
                                                                                                </div>
                                                                                            </asp:LinkButton></div></td></tr></table></td></tr></table></dxw:ContentControl></ContentCollection></dxtc:TabPage><dxtc:TabPage Text="Exportação Implantação">
                                                        <ContentCollection>
                                                            <dxw:ContentControl runat="server">
                                                                <div class="divDataGrid">
                                                                    <dxwgv:ASPxGridView ID="gridConsulta1" runat="server" EnableCallBacks="true" ClientInstanceName="gridConsulta1"
                                                                        AutoGenerateColumns="False" DataSourceID="EsDSCliente" KeyFieldName="IdCliente"
                                                                        OnHtmlRowCreated="gridConsulta_HtmlRowCreated" Width="850px" OnCustomUnboundColumnData="gridConsulta_CustomUnboundColumnData"
                                                                        OnCustomCallback="gridConsulta1_CustomCallback">
                                                                        <Columns>
                                                                            <dxwgv:GridViewCommandColumn ShowSelectCheckbox="True" VisibleIndex="0" Width="5%" ButtonType="Image" ShowClearFilterButton="True">
                                                                                <HeaderStyle HorizontalAlign="Center" />
                                                                                <HeaderTemplate>
                                                                                    <dxe:ASPxCheckBox ID="cbAllGrid1" ClientInstanceName="cbAllGrid1" runat="server" ToolTip="Seleciona todos os CheckBoxs de todas as páginas" BackColor="white" ClientSideEvents-CheckedChanged="function(s, e) { 
                                                                                                                                                                GridConsulta1_OnAllChecked(s, e, gridConsulta1);
                                                                                                                                                            }"
                                                                                        OnInit="CheckBoxSelectAll" />
                                                                                </HeaderTemplate>
                                                                            </dxwgv:GridViewCommandColumn>
                                                                            <dxwgv:GridViewDataTextColumn FieldName="IdCliente" ReadOnly="True" VisibleIndex="1" Width="7%" CellStyle-HorizontalAlign="left">
                                                                                <CellStyle HorizontalAlign="Left">
                                                                                </CellStyle>
                                                                            </dxwgv:GridViewDataTextColumn>
                                                                            <dxwgv:GridViewDataTextColumn FieldName="Apelido" VisibleIndex="2" Width="25%"
                                                                                Settings-AutoFilterCondition="Contains">
                                                                                <Settings AutoFilterCondition="Contains"></Settings>
                                                                            </dxwgv:GridViewDataTextColumn>
                                                                            <dxwgv:GridViewDataDateColumn FieldName="DataDia" VisibleIndex="3" Width="9%" />
                                                                            <dxwgv:GridViewDataTextColumn FieldName="StatusDescricao" Caption="Status"
                                                                                UnboundType="String" VisibleIndex="4" Width="12%"
                                                                                Settings-AutoFilterCondition="Contains">
                                                                                <Settings AutoFilterCondition="Contains"></Settings>
                                                                            </dxwgv:GridViewDataTextColumn>
                                                                            <dxwgv:GridViewDataTextColumn FieldName="Status" Visible="False" />
                                                                            <dxwgv:GridViewDataComboBoxColumn Caption="Tipo Cliente" FieldName="IdTipo" VisibleIndex="5" Width="15%">
                                                                                <EditFormSettings Visible="False" />
                                                                                <PropertiesComboBox DataSourceID="EsDSTipoCliente" TextField="Descricao" ValueField="IdTipo" />
                                                                            </dxwgv:GridViewDataComboBoxColumn>
                                                                            <dxwgv:GridViewDataComboBoxColumn Caption="Ativo" FieldName="StatusAtivo" VisibleIndex="8"
                                                                                Width="5%" ExportWidth="100">
                                                                                <EditFormSettings Visible="False" />
                                                                                <PropertiesComboBox EncodeHtml="false">
                                                                                    <Items>
                                                                                        <dxe:ListEditItem Value="1" Text="<div title='Sim'>Sim</div>" />
                                                                                        <dxe:ListEditItem Value="2" Text="<div title='Não'>Não</div>" />
                                                                                    </Items>
                                                                                </PropertiesComboBox>
                                                                            </dxwgv:GridViewDataComboBoxColumn>
                                                                        </Columns>
                                                                        <SettingsPager Mode="ShowAllRecords">
                                                                        </SettingsPager>
                                                                        <Settings ShowFilterRow="True" ShowStatusBar="Visible" VerticalScrollBarMode="Visible" />
                                                                        <SettingsText EmptyDataRow="0 registros" />
                                                                        <Images />
                                                                        <Styles>
                                                                            <Header ImageSpacing="5px" SortingImageSpacing="5px" />
                                                                            <AlternatingRow Enabled="True" />
                                                                            <Cell Wrap="False" />
                                                                        </Styles>
                                                                        <SettingsCommandButton>
                                                                            <ClearFilterButton Image-Url="../imagens/funnel--minus.png">
                                                                                <Image Url="../imagens/funnel--minus.png">
                                                                                </Image>
                                                                            </ClearFilterButton>
                                                                        </SettingsCommandButton>
                                                                    </dxwgv:ASPxGridView>
                                                                </div>
                                                                <table border="0">
                                                                    <tr>
                                                                        <td>

                                                                            <table cellpadding="2" cellspacing="2" border="0">
                                                                                <tr>
                                                                                    <td style="width: 60px;">
                                                                                        <asp:Label ID="label9" runat="server" CssClass="labelRequired labelFloat" Text="Posição:" />
                                                                                    </td>
                                                                                    <td>
                                                                                        <dx:ASPxGridLookup ID="dropPosicao" ClientInstanceName="dropPosicao" runat="server" SelectionMode="Multiple" DataSourceID="EsDSPosicao" KeyFieldName="Value" AllowUserInput="false"
                                                                                            Width="400px" TextFormatString="{0}" MultiTextSeparator=", " Font-Size="11px" IncrementalFilteringMode="StartsWith" AutoGenerateColumns="False">
                                                                                            <Columns>
                                                                                                <dxwgv:GridViewCommandColumn ShowSelectCheckbox="True" Width="4%" VisibleIndex="0" ButtonType="Image" ShowClearFilterButton="True">
                                                                                                    <HeaderStyle HorizontalAlign="Center" />
                                                                                                    <HeaderTemplate>
                                                                                                        <dxe:ASPxCheckBox ID="cbAll1" ClientInstanceName="cbAll1" runat="server" ToolTip="Seleciona todos" OnInit="CheckBoxSelectAll" BackColor="white" ClientSideEvents-CheckedChanged="function(s, e) {
                                                                    OnAllCheckedChanged1(s, e, dropPosicao);
                                                                }" />
                                                                                                    </HeaderTemplate>
                                                                                                </dxwgv:GridViewCommandColumn>

                                                                                                <dxwgv:GridViewDataColumn FieldName="Value" Caption=" " CellStyle-Font-Size="X-Small" VisibleIndex="1" />

                                                                                                <dxwgv:GridViewDataColumn FieldName="Id" Visible="false" />
                                                                                            </Columns>

                                                                                            <GridViewProperties>
                                                                                                <SettingsBehavior AllowFocusedRow="True" AllowSelectByRowClick="True" />
                                                                                                <SettingsCommandButton>
                                                                                                    <ClearFilterButton Image-Url="../imagens/funnel--minus.png">
                                                                                                        <Image Url="../imagens/funnel--minus.png">
                                                                                                        </Image>
                                                                                                    </ClearFilterButton>
                                                                                                </SettingsCommandButton>
                                                                                            </GridViewProperties>

                                                                                        </dx:ASPxGridLookup>

                                                                                    </td>
                                                                                    <td colspan="5">
                                                                                        <div id="Div5" class="linkButton linkButtonNoBorder  linkButtonNoMargin">
                                                                                            <asp:LinkButton ID="btnPosicao" runat="server" Font-Overline="false" CssClass="btnRun"
                                                                                                OnClientClick="btnExporta1Click('btnPosicao'); return false;">
                                                                                                <asp:Literal ID="Literal5" runat="server" Text="Exportar" /><div>
                                                                                                </div>
                                                                                            </asp:LinkButton></div></td></tr></table></td></tr><tr>
                                                                        <td>

                                                                            <table cellpadding="2" cellspacing="2" border="0">
                                                                                <tr>
                                                                                    <td style="width: 60px;">
                                                                                        <asp:Label ID="label13" runat="server" CssClass="labelRequired labelFloat" Text="Histórico:" />
                                                                                    </td>
                                                                                    <td>

                                                                                        <dx:ASPxGridLookup ID="dropHistorico" ClientInstanceName="dropHistorico" runat="server" SelectionMode="Multiple" DataSourceID="EsDSHistorico" KeyFieldName="Value" AllowUserInput="false"
                                                                                            Width="400px" TextFormatString="{0}" MultiTextSeparator=", " Font-Size="11px" IncrementalFilteringMode="StartsWith" AutoGenerateColumns="False">
                                                                                            <Columns>
                                                                                                <dxwgv:GridViewCommandColumn ShowSelectCheckbox="True" Width="4%" VisibleIndex="0" ButtonType="Image" ShowClearFilterButton="True">
                                                                                                    <HeaderStyle HorizontalAlign="Center" />
                                                                                                    <HeaderTemplate>
                                                                                                        <dxe:ASPxCheckBox ID="cbAll2" ClientInstanceName="cbAll2" runat="server" ToolTip="Seleciona todos" OnInit="CheckBoxSelectAll" BackColor="white" ClientSideEvents-CheckedChanged="function(s, e) {
                                                                    OnAllCheckedChanged1(s, e, dropHistorico);
                                                                }" />
                                                                                                    </HeaderTemplate>
                                                                                                </dxwgv:GridViewCommandColumn>

                                                                                                <dxwgv:GridViewDataColumn FieldName="Value" Caption=" " CellStyle-Font-Size="X-Small" VisibleIndex="1" />

                                                                                                <dxwgv:GridViewDataColumn FieldName="Id" Visible="false" />
                                                                                            </Columns>

                                                                                            <GridViewProperties>
                                                                                                <SettingsBehavior AllowFocusedRow="True" AllowSelectByRowClick="True" />
                                                                                                <SettingsCommandButton>
                                                                                                    <ClearFilterButton Image-Url="../imagens/funnel--minus.png">
                                                                                                        <Image Url="../imagens/funnel--minus.png">
                                                                                                        </Image>
                                                                                                    </ClearFilterButton>
                                                                                                </SettingsCommandButton>
                                                                                            </GridViewProperties>

                                                                                        </dx:ASPxGridLookup>
                                                                                    </td>
                                                                                    <td class="td_Label" style="width: 41px;">
                                                                                        <asp:Label ID="label14" runat="server" CssClass="labelRequired" Text="Data:" />
                                                                                    </td>
                                                                                    <td>
                                                                                        <dxe:ASPxDateEdit ID="textDataHistorico" runat="server" ClientInstanceName="textDataHistorico" />
                                                                                    </td>
                                                                                    <td colspan="3">
                                                                                        <div id="Div7" class="linkButton linkButtonNoBorder  linkButtonNoMargin">
                                                                                            <asp:LinkButton ID="btnHistorico" runat="server" Font-Overline="false" CssClass="btnRun"
                                                                                                OnClientClick="btnExporta1Click('btnHistorico'); return false;">
                                                                                                <asp:Literal ID="Literal7" runat="server" Text="Exportar" /><div>
                                                                                                </div>
                                                                                            </asp:LinkButton></div></td></tr></table></td></tr><tr>
                                                                        <td>

                                                                            <table cellpadding="2" cellspacing="2" border="0">
                                                                                <tr>
                                                                                    <td style="width: 60px;">
                                                                                        <asp:Label ID="label15" runat="server" CssClass="labelRequired labelFloat" Text="Operação:" />
                                                                                    </td>
                                                                                    <td>

                                                                                        <dx:ASPxGridLookup ID="dropOperacao" ClientInstanceName="dropOperacao" runat="server" SelectionMode="Multiple" DataSourceID="EsDSOperacao" KeyFieldName="Value" AllowUserInput="false"
                                                                                            Width="400px" TextFormatString="{0}" MultiTextSeparator=", " Font-Size="11px" IncrementalFilteringMode="StartsWith" AutoGenerateColumns="False">
                                                                                            <Columns>
                                                                                                <dxwgv:GridViewCommandColumn ShowSelectCheckbox="True" Width="4%" VisibleIndex="0" ButtonType="Image" ShowClearFilterButton="True">
                                                                                                    <HeaderStyle HorizontalAlign="Center" />
                                                                                                    <HeaderTemplate>
                                                                                                        <dxe:ASPxCheckBox ID="cball3" ClientInstanceName="cball3" runat="server" ToolTip="Seleciona todos" OnInit="CheckBoxSelectAll" BackColor="white" ClientSideEvents-CheckedChanged="function(s, e) {
                                                                   OnAllCheckedChanged1(s, e, dropOperacao);
                                                                }" />
                                                                                                    </HeaderTemplate>
                                                                                                </dxwgv:GridViewCommandColumn>

                                                                                                <dxwgv:GridViewDataColumn FieldName="Value" Caption=" " CellStyle-Font-Size="X-Small" VisibleIndex="1" />

                                                                                                <dxwgv:GridViewDataColumn FieldName="Id" Visible="false" />
                                                                                            </Columns>

                                                                                            <GridViewProperties>
                                                                                                <SettingsBehavior AllowFocusedRow="True" AllowSelectByRowClick="True" />
                                                                                                <SettingsCommandButton>
                                                                                                    <ClearFilterButton Image-Url="../imagens/funnel--minus.png">
                                                                                                        <Image Url="../imagens/funnel--minus.png">
                                                                                                        </Image>
                                                                                                    </ClearFilterButton>
                                                                                                </SettingsCommandButton>
                                                                                            </GridViewProperties>

                                                                                        </dx:ASPxGridLookup>

                                                                                    </td>
                                                                                    <td class="td_Label" style="width: 50px;">
                                                                                        <asp:Label ID="label16" runat="server" CssClass="labelRequired" Text="Ínicio:" />
                                                                                    </td>
                                                                                    <td>
                                                                                        <dxe:ASPxDateEdit ID="textDataInicioOperacao" runat="server" ClientInstanceName="textDataInicioOperacao" />
                                                                                    </td>
                                                                                    <td style="width: 35px;" class="td_Label">
                                                                                        <asp:Label ID="label17" runat="server" CssClass="labelRequired" Text="Fim:"></asp:Label></td><td>
                                                                                        <dxe:ASPxDateEdit ID="textDataFimOperacao" runat="server" ClientInstanceName="textDataFimOperacao" />
                                                                                    </td>
                                                                                    <td>
                                                                                        <div id="Div8" class="linkButton linkButtonNoBorder linkButtonNoMargin">
                                                                                            <asp:LinkButton ID="btnOperacao" runat="server" Font-Overline="false" CssClass="btnRun"
                                                                                                OnClientClick="btnExporta1Click('btnOperacao'); return false;">
                                                                                                <asp:Literal ID="Literal8" runat="server" Text="Exportar" /><div>
                                                                                                </div>
                                                                                            </asp:LinkButton></div></td></tr></table></td></tr></table></dxw:ContentControl></ContentCollection></dxtc:TabPage><dxtc:TabPage Name="TabGalgo" Text="Galgo">
                                                        <ContentCollection>
                                                            <dxw:ContentControl runat="server">
                                                                <div class="divDataGrid">
                                                                    <dxwgv:ASPxGridView ID="gridConsultaGalgo" runat="server" EnableCallBacks="true" ClientInstanceName="gridConsultaGalgo"
                                                                        AutoGenerateColumns="False" DataSourceID="EsDSClienteGalgo" KeyFieldName="IdCliente"
                                                                        OnHtmlRowCreated="gridConsulta_HtmlRowCreated" Width="850px" OnCustomUnboundColumnData="gridConsulta_CustomUnboundColumnData"
                                                                        OnCustomCallback="gridConsultaGalgo_CustomCallback">
                                                                        <Columns>
                                                                            <dxwgv:GridViewCommandColumn ShowSelectCheckbox="True" VisibleIndex="0" Width="5%" ButtonType="Image" ShowClearFilterButton="True">
                                                                                <HeaderStyle HorizontalAlign="Center" />
                                                                                <HeaderTemplate>
                                                                                    <dxe:ASPxCheckBox ID="cbAll" ClientInstanceName="cbAll" runat="server" ToolTip="Seleciona todos os CheckBoxs de todas as páginas" BackColor="white" ClientSideEvents-CheckedChanged="function(s, e) {OnAllCheckedChanged(s, e, gridConsultaGalgo);}" OnInit="CheckBoxSelectAll" />
                                                                                </HeaderTemplate>
                                                                            </dxwgv:GridViewCommandColumn>
                                                                            <dxwgv:GridViewDataTextColumn FieldName="IdCliente" ReadOnly="True" VisibleIndex="1" Width="7%" CellStyle-HorizontalAlign="left">
                                                                                <CellStyle HorizontalAlign="Left">
                                                                                </CellStyle>
                                                                            </dxwgv:GridViewDataTextColumn>
                                                                            <dxwgv:GridViewDataTextColumn FieldName="Apelido" VisibleIndex="2" Width="25%"
                                                                                Settings-AutoFilterCondition="Contains">
                                                                                <Settings AutoFilterCondition="Contains"></Settings>
                                                                            </dxwgv:GridViewDataTextColumn>
                                                                            <dxwgv:GridViewDataDateColumn FieldName="DataDia" VisibleIndex="3" Width="9%" />
                                                                            <dxwgv:GridViewDataTextColumn FieldName="StatusDescricao" Caption="Status"
                                                                                UnboundType="String" VisibleIndex="4" Width="12%"
                                                                                Settings-AutoFilterCondition="Contains">
                                                                                <Settings AutoFilterCondition="Contains"></Settings>
                                                                            </dxwgv:GridViewDataTextColumn>
                                                                            <dxwgv:GridViewDataTextColumn FieldName="Status" Visible="False" />
                                                                            <dxwgv:GridViewDataComboBoxColumn Caption="Tipo Cliente" FieldName="IdTipo" VisibleIndex="5" Width="15%">
                                                                                <EditFormSettings Visible="False" />
                                                                                <PropertiesComboBox DataSourceID="EsDSTipoCliente" TextField="Descricao" ValueField="IdTipo" />
                                                                            </dxwgv:GridViewDataComboBoxColumn>
                                                                            <dxwgv:GridViewDataComboBoxColumn Caption="Ativo" FieldName="StatusAtivo" VisibleIndex="8"
                                                                                Width="5%" ExportWidth="100">
                                                                                <EditFormSettings Visible="False" />
                                                                                <PropertiesComboBox EncodeHtml="false">
                                                                                    <Items>
                                                                                        <dxe:ListEditItem Value="1" Text="<div title='Sim'>Sim</div>" />
                                                                                        <dxe:ListEditItem Value="2" Text="<div title='Não'>Não</div>" />
                                                                                    </Items>
                                                                                </PropertiesComboBox>
                                                                            </dxwgv:GridViewDataComboBoxColumn>

                                                                        </Columns>
                                                                        <SettingsPager Mode="ShowAllRecords">
                                                                        </SettingsPager>
                                                                        <Settings ShowFilterRow="True" ShowStatusBar="Visible" VerticalScrollBarMode="Visible" />
                                                                        <SettingsText EmptyDataRow="0 registros" />
                                                                        <Images />
                                                                        <Styles>
                                                                            <Header ImageSpacing="5px" SortingImageSpacing="5px" />
                                                                            <AlternatingRow Enabled="True" />
                                                                            <Cell Wrap="False" />
                                                                        </Styles>
                                                                        <SettingsCommandButton>
                                                                            <ClearFilterButton Image-Url="../imagens/funnel--minus.png">
                                                                                <Image Url="../imagens/funnel--minus.png">
                                                                                </Image>
                                                                            </ClearFilterButton>
                                                                        </SettingsCommandButton>
                                                                    </dxwgv:ASPxGridView>
                                                                </div>
                                                                <table border="0">
                                                                    <tr>
                                                                        <td>

                                                                            <table cellpadding="2" cellspacing="2" border="0">
                                                                                <tr>
                                                                                    <td>
                                                                                        <asp:Label ID="lbl_PlCota" runat="server" CssClass="labelRequired labelFloat" Text="PL/Cota (Arquivo):" />
                                                                                    </td>
                                                                                    <td></td>
                                                                                    <td class="td_Label" style="width: 50px;">
                                                                                        <asp:Label ID="label11" runat="server" CssClass="labelRequired" Text="Ínicio:" />
                                                                                    </td>
                                                                                    <td>
                                                                                        <dxe:ASPxDateEdit ID="textDataInicioGalgoPlCotaArquivo" runat="server" ClientInstanceName="textDataInicioGalgoPlCotaArquivo" />
                                                                                    </td>
                                                                                    <td style="width: 35px;" class="td_Label">
                                                                                        <asp:Label ID="label12" runat="server" CssClass="labelRequired" Text="Fim:"></asp:Label></td><td>
                                                                                        <dxe:ASPxDateEdit ID="textDataFimGalgoPlCotaArquivo" runat="server" ClientInstanceName="textDataFimGalgoPlCotaArquivo" />
                                                                                    </td>
                                                                                    <td>
                                                                                        <div id="Div6" class="linkButton linkButtonNoBorder linkButtonNoMargin">
                                                                                            <asp:LinkButton ID="btnTipo_GalgoPlCotaArquivo" runat="server" Font-Overline="false" CssClass="btnRun"
                                                                                                OnClientClick="btnExportaClick('GalgoPlCotaArquivo'); return false;">
                                                                                                <asp:Literal ID="Literal6" runat="server" Text="Exportar" /><div>
                                                                                                </div>
                                                                                            </asp:LinkButton></div></td></tr><tr>
                                                                                    <td>
                                                                                        <asp:Label ID="label18" runat="server" CssClass="labelRequired labelFloat" Text="PL/Cota (WebService):" />
                                                                                    </td>
                                                                                    <td></td>
                                                                                    <td class="td_Label" style="width: 50px;">
                                                                                        <asp:Label ID="label19" runat="server" CssClass="labelRequired" Text="Ínicio:" />
                                                                                    </td>
                                                                                    <td>
                                                                                        <dxe:ASPxDateEdit ID="textDataInicioGalgoPlCotaWcf" runat="server" ClientInstanceName="textDataInicioGalgoPlCotaWcf" />
                                                                                    </td>
                                                                                    <td style="width: 35px;" class="td_Label">
                                                                                        <asp:Label ID="label20" runat="server" CssClass="labelRequired" Text="Fim:"></asp:Label></td><td>
                                                                                        <dxe:ASPxDateEdit ID="textDataFimGalgoPlCotaWcf" runat="server" ClientInstanceName="textDataFimGalgoPlCotaWcf" />
                                                                                    </td>
                                                                                    <td>
                                                                                        <div id="Div9" class="linkButton linkButtonNoBorder linkButtonNoMargin">
                                                                                            <asp:LinkButton ID="btnTipo_GalgoPlCotaWcf" runat="server" Font-Overline="false" CssClass="btnRun"
                                                                                                OnClientClick="btnExportaClick('GalgoPlCotaWcf'); return false;">
                                                                                                <asp:Literal ID="Literal9" runat="server" Text="Exportar" /><div>
                                                                                                </div>
                                                                                            </asp:LinkButton></div></td></tr></table></td></tr></table></dxw:ContentControl></ContentCollection></dxtc:TabPage></TabPages></dxtc:ASPxPageControl></dxp:PanelContent></PanelCollection></dxrp:ASPxRoundPanel></div></div></div></div></ContentTemplate></asp:UpdatePanel><cc1:esDataSource ID="EsDSCliente" runat="server" OnesSelect="EsDSCliente_esSelect" LowLevelBind="true" />
        <cc1:esDataSource ID="EsDSClienteGalgo" runat="server" OnesSelect="EsDSClienteGalgo_esSelect" LowLevelBind="true" />
        <cc1:esDataSource ID="EsDSTipoCliente" runat="server" OnesSelect="EsDSTipoCliente_esSelect" />
        <dxwgv:ASPxGridView ID="gridExportacao1" runat="server" Visible="false" />
        <dxwgv:ASPxGridViewExporter ID="gridExport" runat="server" GridViewID="gridExportacao1" />

        <!-- Combos aspxLookupGrid -->
        <cc1:esDataSource ID="EsDSPosicao" runat="server" OnesSelect="EsDSPosicao_esSelect" />
        <cc1:esDataSource ID="EsDSHistorico" runat="server" OnesSelect="EsDSHistorico_esSelect" />
        <cc1:esDataSource ID="EsDSOperacao" runat="server" OnesSelect="EsDSOperacao_esSelect" />

        <dxlp:ASPxLoadingPanel ID="LoadingPanel" runat="server" Text="Exportando arquivo, aguarde..." ClientInstanceName="LoadingPanel" Modal="true" />
        <dxlp:ASPxLoadingPanel ID="LoadingPanel1" runat="server" Text="Exportando arquivo, aguarde..." ClientInstanceName="LoadingPanel1" Modal="true" />
    </form>
</body>
</html>
