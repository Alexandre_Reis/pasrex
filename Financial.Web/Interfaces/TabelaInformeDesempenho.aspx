﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="TabelaInformeDesempenho.aspx.cs"
    Inherits="CadastrosBasicos_TabelaInformeDesempenho" %>

<%@ Register Assembly="System.Web, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"
    Namespace="System.Web.UI.HtmlControls" TagPrefix="cc2" %>
<%@ Register Assembly="EntitySpaces.Web" Namespace="EntitySpaces.Web" TagPrefix="cc1" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxe" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxwgv" %>

<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxpc" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxcb" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxtc" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxw" %>       
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxlp" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <link href="~/css/forms.css" rel="stylesheet" type="text/css" />

    <script type="text/javascript" language="Javascript" src="../js/global.js"></script>

    <script type="text/javascript" language="Javascript">
    var popup = true;
    document.onkeydown=onDocumentKeyDownWithCallback;
    var operacao = '';
    
    function OnGetDataCarteira(data) {
        btnEditCodigoCarteira.SetValue(data);
        popupCarteira.HideWindow();
        ASPxCallback1.SendCallback(btnEditCodigoCarteira.GetValue());
        btnEditCodigoCarteira.Focus();
    }   
       
    function OnGetDataCarteira1(data) {
        btnEditCodigoCarteiraF.SetValue(data);        
        ASPxCallback2.SendCallback(btnEditCodigoCarteiraF.GetValue());
        popupCarteira1.HideWindow();
        btnEditCodigoCarteiraF.Focus();    
    }
    </script>

</head>
<body>
    <form id="form1" runat="server">
        <dxcb:ASPxCallback ID="callbackErro" runat="server" OnCallback="callbackErro_Callback">
            <ClientSideEvents CallbackComplete="function(s, e) {             
            if (e.result != '') {                   
                alert(e.result);                    
            }
            else {
                if (operacao == 'salvar' || gridCadastro.cp_EditVisibleIndex != 'new') {             
                    gridCadastro.UpdateEdit();
                }
                else {
                    callbackAdd.SendCallback();
                }    
            }
            
            operacao = '';
        }        
        " />
        </dxcb:ASPxCallback>
        
        <dxcb:ASPxCallback ID="callbackAdd" runat="server" OnCallback="callbackAdd_Callback">
            <ClientSideEvents CallbackComplete="function(s, e) {
            alert('Operação feita com sucesso.');
        }
        " />
        </dxcb:ASPxCallback>
        
        <dxcb:ASPxCallback ID="ASPxCallback1" runat="server" OnCallback="ASPxCallback1_Callback">
            <ClientSideEvents CallbackComplete="function(s, e) {                                       
            var textNome = document.getElementById('gridCadastro_DXPEForm_ef'+ gridCadastro.cp_EditVisibleIndex + '_tabCadastro_textNome');            
            OnCallBackCompleteCliente(s, e, popupMensagemCarteira, btnEditCodigoCarteira, textNome);                        
            }        
        " />
        </dxcb:ASPxCallback>
                           
        <dxcb:ASPxCallback ID="ASPxCallback2" runat="server" OnCallback="ASPxCallback2_Callback">
            <ClientSideEvents CallbackComplete="function(s, e) {
                var textNomeF = document.getElementById('gridCadastro_DXPEForm_ef'+ gridCadastro.cp_EditVisibleIndex + '_tabCadastro_textNomeF');
                OnCallBackCompleteCliente(s, e, popupMensagemCarteira1, btnEditCodigoCarteiraF, textNomeF);
            }        
            "/>
        </dxcb:ASPxCallback>

        <dxcb:ASPxCallback ID="callBackClonar" runat="server" OnCallback="callBackClonar_Callback">
            <ClientSideEvents CallbackComplete="function(s, e) {            
            LoadingPanelClonar.Hide();
            
            if (e.result != '') {
                alert(e.result);

                if (e.result == 'Processo executado com sucesso.') {
                     
                    popupClonar.Hide();
                    gridCadastro.PerformCallback('btnRefresh');
                }
            }
        }        
        " />
        </dxcb:ASPxCallback>                
        
        <dxpc:ASPxPopupControl ID="popupClonar" AllowDragging="true" PopupElementID="popupLote"
                                    EnableClientSideAPI="True" PopupVerticalAlign="Middle" PopupHorizontalAlign="OutsideRight"
                                    CloseAction="CloseButton" Width="500" Left="100" Top="3" HeaderText="Clonar"
                                    runat="server" HeaderStyle-BackColor="#EBECEE" HeaderStyle-Font-Bold="true" HeaderStyle-Font-Size="11px">
                                    <ContentCollection>
                                        <dxpc:PopupControlContentControl ID="PopupControlContentControl3" runat="server">
                                            <table border="0" cellspacing="2" cellpadding="2">
                                                                                                                                                
                                                <tr>
                                                    <td class="td_Label">
                                                     <asp:Label ID="labelInfo" runat="server" CssClass="labelRequired" Text="Carteira a Clonar:" />
                                                    </td>
                                                    <td colspan="3">
                                                        <dx:ASPxGridLookup ID="dropInfo" ClientInstanceName="dropInfo" runat="server"                                                         
                                                        KeyFieldName="CompositeKey" DataSourceID="EsDSTabelaInformeDesempenhoClonar"
                                                                       Width="350px" TextFormatString="{0} - {1} - {3}" Font-Size="11px" AllowUserInput="false">
                                                         <Columns>
                                                             <dxwgv:GridViewCommandColumn ShowSelectCheckbox="True" Width="4%" />                        
                                                             <dxwgv:GridViewDataColumn FieldName="IdCarteira" Caption="Id" Width="7%" CellStyle-Font-Size="X-Small"/>                         
                                                             <dxwgv:GridViewDataColumn FieldName="Nome" CellStyle-Font-Size="X-Small" Width="90%" CellStyle-Wrap="false"/>                            
                                                             <dxwgv:GridViewDataDateColumn FieldName="Data" Width="10%"/>
                                                             <dxwgv:GridViewDataColumn FieldName="DataInicioVigenciaString" Visible="false" />    
                                                             <dxwgv:GridViewDataColumn FieldName="CompositeKey" Visible="false" />
                                                        </Columns>
                                                        
                                                        <GridViewProperties>
                                                            <Settings ShowFilterRow="True" />
                                                        </GridViewProperties>
                                                        
                                                        </dx:ASPxGridLookup>
                                                   </td>                                                                                                                                                                                                                                                                                                                                                                                                                       
                                                </tr>
                                                
                                                <tr>
                                                    <td class="td_Label">
                                                        <asp:Label ID="labelCarteira" runat="server" CssClass="labelRequired" Text="Nova Carteira:"/>
                                                    </td>
                                                    
                                                    <td class="td_Label">
                                                        <dx:ASPxGridLookup ID="dropCarteiraDestino" ClientInstanceName="dropCarteiraDestino" runat="server" KeyFieldName="IdCarteira" DataSourceID="EsDSCarteiraClonar"
                                                                                           Width="250px" TextFormatString="{0}" Font-Size="11px" AllowUserInput="false">
                                                                             <Columns>
                                                                                 <dxwgv:GridViewCommandColumn ShowSelectCheckbox="True" Width="4%" />                        
                                                                                 <dxwgv:GridViewDataColumn FieldName="IdCarteira" Caption="Id" Width="7%" CellStyle-Font-Size="X-Small"/>                         
                                                                                 <dxwgv:GridViewDataColumn FieldName="Apelido" CellStyle-Font-Size="X-Small" Width="89%" CellStyle-Wrap="false"/>                            
                                                                            </Columns>
                                                                            
                                                                            <GridViewProperties>
                                                                                <Settings ShowFilterRow="True" />
                                                                            </GridViewProperties>
                                                                            
                                                                        </dx:ASPxGridLookup>
                                                    </td>
                                                   
                                                </tr>
                                                
                                                <tr>
                                                    <td class="td_Label">
                                                        <asp:Label ID="label1" runat="server" CssClass="labelRequired" Text="Início Vigência:"></asp:Label>
                                                    </td>
                                                 <td>       
                                                    <dxe:ASPxDateEdit ID="textDataClonar" runat="server" ClientInstanceName="textDataClonar" />
                                                </td>  
                                                </tr>
                                            </table>
                                            <div class="linkButton linkButtonNoBorder" style="margin-top: 20px">
                                                <asp:LinkButton ID="btnProcessaClonar" runat="server" Font-Overline="false" ForeColor="Black" CssClass="btnOK"
                                                
                                                    OnClientClick="                                                                                                                                                                                                                                                                                                                                                                                                                                               
                                                                   LoadingPanelClonar.Show();
                                                                   callBackClonar.SendCallback();                                                                        
                                                                   
                                                                   return false;
                                                                   ">
                                                    
                                                    <asp:Literal ID="Literal15" runat="server" Text="Clonar" /><div></div>
                                                </asp:LinkButton>
                                            </div>
                                        </dxpc:PopupControlContentControl>
                                    </ContentCollection>
                                </dxpc:ASPxPopupControl>
                                
        <div class="divPanel">
            <table width="100%">
                <tr>
                    <td>
                        <div id="container">
                            <div id="header">
                                <asp:Label ID="lblHeader" runat="server" Text="Tabela de Informe de Desempenho (Despesas)"></asp:Label>
                            </div>
                            <div id="mainContent">
                                <div class="linkButton">
                                    <asp:LinkButton ID="btnAdd" runat="server" Font-Overline="false" ValidationGroup="ATK"
                                        CssClass="btnAdd" OnClientClick="gridCadastro.AddNewRow(); return false;">
                                        <asp:Literal ID="Literal1" runat="server" Text="Novo" /><div>
                                        </div>
                                    </asp:LinkButton>
                                    <asp:LinkButton ID="btnDelete" runat="server" Font-Overline="false" ValidationGroup="ATK"
                                        CssClass="btnDelete" OnClientClick=" if (confirm('Tem certeza que quer excluir?')==true) gridCadastro.PerformCallback('btnDelete');return false;">
                                        <asp:Literal ID="Literal2" runat="server" Text="Excluir" /><div>
                                        </div>
                                    </asp:LinkButton>
                                    <asp:LinkButton ID="btnPDF" runat="server" Font-Overline="false" ValidationGroup="ATK"
                                        CssClass="btnPdf" OnClick="btnPDF_Click">
                                        <asp:Literal ID="Literal4" runat="server" Text="Gerar PDF" /><div>
                                        </div>
                                    </asp:LinkButton>
                                    <asp:LinkButton ID="btnExcel" runat="server" Font-Overline="false" ValidationGroup="ATK"
                                        CssClass="btnExcel" OnClick="btnExcel_Click">
                                        <asp:Literal ID="Literal5" runat="server" Text="Gerar Excel" /><div>
                                        </div>
                                    </asp:LinkButton>
                                    <asp:LinkButton ID="btnRefresh" runat="server" Font-Overline="false" CssClass="btnRefresh"
                                        OnClientClick=" gridCadastro.PerformCallback('btnRefresh'); return false;">
                                        <asp:Literal ID="Literal6" runat="server" Text="Atualizar" /><div>
                                        </div>
                                    </asp:LinkButton>
                                     <asp:LinkButton ID="btnClonar" runat="server" Font-Overline="false" CssClass="btnLote"
                                        OnClientClick="
                                                                                
                                            dropInfo.SetText('');
                                            dropInfo.SetValue('');
                                            
                                            dropCarteiraDestino.SetText('');
                                            dropCarteiraDestino.SetValue('');
                                            
                                            textDataClonar.SetValue(null);
                                        
                                        popupClonar.ShowWindow(); return false;">
                                        <asp:Literal ID="Literal8" runat="server" Text="Clonar" /><div></div>
                                    </asp:LinkButton>              
                                </div>
                                <div class="divDataGrid">
                                    <dxwgv:ASPxGridView ID="gridCadastro" runat="server" EnableCallBacks="true" KeyFieldName="CompositeKey"
                                        DataSourceID="EsDSTabelaInformeDesempenho" OnCustomCallback="gridCadastro_CustomCallback"
                                        OnRowInserting="gridCadastro_RowInserting" OnRowUpdating="gridCadastro_RowUpdating"
                                        OnBeforeGetCallbackResult="gridCadastro_PreRender" OnCustomUnboundColumnData="gridCadastro_CustomUnboundColumnData"
                                        OnCustomJSProperties="gridCadastro_CustomJSProperties">
                                        <Columns>
                                            <dxwgv:GridViewCommandColumn VisibleIndex="0" ShowClearFilterButton="True">
                                                <HeaderTemplate>
                                                    <dxe:ASPxCheckBox ID="cbAll" ClientInstanceName="cbAll" runat="server" ToolTip="Seleciona todos os CheckBoxs de todas as páginas" BackColor="white" ClientSideEvents-CheckedChanged="function(s, e) {OnAllCheckedChanged(s, e, gridCadastro);}" OnInit="CheckBoxSelectAll"/>
                                                </HeaderTemplate>
                                            </dxwgv:GridViewCommandColumn>
                                            
                                            <dxwgv:GridViewDataColumn FieldName="IdCarteira" VisibleIndex="1" Width="8%" CellStyle-HorizontalAlign="left" />
                                            
                                            <dxwgv:GridViewDataColumn FieldName="Apelido" Caption="Nome" VisibleIndex="2" Width="30%" ExportWidth="275" />
                                           
                                            <dxwgv:GridViewDataComboBoxColumn Caption="Categoria" FieldName="IdCategoria" VisibleIndex="3" Width="20%">
                                                <EditFormSettings Visible="False" />
                                                <PropertiesComboBox DataSourceID="EsDSCategoriaFundo" TextField="Descricao" ValueField="IdCategoria" DropDownStyle="DropDown" IncrementalFilteringMode="Contains" />
                                            </dxwgv:GridViewDataComboBoxColumn>
                                             
                                            <dxwgv:GridViewDataDateColumn FieldName="Data" Caption="Mês/Ano" VisibleIndex="4" Width="10%" PropertiesDateEdit-DisplayFormatString="MM/yyyy" PropertiesDateEdit-EditFormatString="MM/yyyy" />
                                            
                                            
                                           
                                                                                      
                                            <%-- <dxwgv:GridViewDataSpinEditColumn FieldName="ValorAdmAdministrador" Caption="Adm. Paga (Administrador)"
                                                VisibleIndex="4" Width="15%" HeaderStyle-HorizontalAlign="Right" CellStyle-HorizontalAlign="Right">
                                                <PropertiesSpinEdit DisplayFormatString="{0:#,##0.00;(#,##0.00);0.00}">
                                                </PropertiesSpinEdit>
                                            </dxwgv:GridViewDataSpinEditColumn>
                                            <dxwgv:GridViewDataSpinEditColumn FieldName="ValorPfeeAdministrador" Caption="Pfee Paga (Administrador)"
                                                VisibleIndex="5" Width="15%" HeaderStyle-HorizontalAlign="Right" CellStyle-HorizontalAlign="Right">
                                                <PropertiesSpinEdit DisplayFormatString="{0:#,##0.00;(#,##0.00);0.00}">
                                                </PropertiesSpinEdit>
                                            </dxwgv:GridViewDataSpinEditColumn>
                                            <dxwgv:GridViewDataSpinEditColumn FieldName="ValorOutrasAdministrador" Caption="Outras (Administrador)"
                                                VisibleIndex="6" Width="15%" HeaderStyle-HorizontalAlign="Right" CellStyle-HorizontalAlign="Right">
                                                <PropertiesSpinEdit DisplayFormatString="{0:#,##0.00;(#,##0.00);0.00}">
                                                </PropertiesSpinEdit>
                                            </dxwgv:GridViewDataSpinEditColumn>
                                            <dxwgv:GridViewDataSpinEditColumn FieldName="ValorAdmGestor" Caption="Adm. Paga (Gestor)"
                                                VisibleIndex="7" Width="12%" HeaderStyle-HorizontalAlign="Right" CellStyle-HorizontalAlign="Right">
                                                <PropertiesSpinEdit DisplayFormatString="{0:#,##0.00;(#,##0.00);0.00}">
                                                </PropertiesSpinEdit>
                                            </dxwgv:GridViewDataSpinEditColumn>
                                            <dxwgv:GridViewDataSpinEditColumn FieldName="ValorPfeeGestor" Caption="Pfee Paga (Gestor)"
                                                VisibleIndex="8" Width="12%" HeaderStyle-HorizontalAlign="Right" CellStyle-HorizontalAlign="Right">
                                                <PropertiesSpinEdit DisplayFormatString="{0:#,##0.00;(#,##0.00);0.00}">
                                                </PropertiesSpinEdit>
                                            </dxwgv:GridViewDataSpinEditColumn>
                                            <dxwgv:GridViewDataSpinEditColumn FieldName="ValorOutrasGestor" Caption="Outras (Gestor)"
                                                VisibleIndex="9" Width="12%" HeaderStyle-HorizontalAlign="Right" CellStyle-HorizontalAlign="Right">
                                                <PropertiesSpinEdit DisplayFormatString="{0:#,##0.00;(#,##0.00);0.00}">
                                                </PropertiesSpinEdit>
                                            </dxwgv:GridViewDataSpinEditColumn>
--%>                                            <dxwgv:GridViewDataTextColumn FieldName="CompositeKey" UnboundType="String" Visible="False" />
                                        </Columns>
                                        <Templates>
                                            <EditForm>
                                                <asp:Panel ID="panelEdicao" runat="server" OnLoad="panelEdicao_Load">
                                                    <div class="editForm">
                                                        <dxtc:ASPxPageControl ID="tabCadastro" runat="server" ClientInstanceName="tabCadastro"
                                                        Height="100%" Width="100%" ActiveTabIndex="0" TabSpacing="0px" OnLoad="tabCadastroOnLoad">
                                                            <TabPages>
                                                                <dxtc:TabPage Text="Clube de Investimento">
                                                                    <ContentCollection>
                                                                        <dxw:ContentControl runat="server">
                                                                        <table>
                                                        <tr>
                                                            <td class="td_Label">
                                                                <asp:Label ID="labelCarteira" runat="server" CssClass="labelRequired" Text="Carteira:"></asp:Label>
                                                            </td>
                                                            <td>
                                                                <dxe:ASPxSpinEdit ID="btnEditCodigoCarteira" runat="server" CssClass="textButtonEdit"
                                                                    ClientInstanceName="btnEditCodigoCarteira" Text='<%# Eval("IdCarteira") %>' MaxLength="10"
                                                                    NumberType="Integer" OnInit="btnEditCodigoCarteira_Init">
                                                                    <Buttons>
                                                                        <dxe:EditButton>
                                                                        </dxe:EditButton>
                                                                    </Buttons>
                                                                    <ClientSideEvents 
                                                                        KeyPress="function(s, e) {document.getElementById('gridCadastro_DXPEForm_ef' + gridCadastro.cp_EditVisibleIndex + '_textNome').value = '';} "
                                                                        ButtonClick="function(s, e) {popupCarteira.ShowAtElementByID(s.name);}" 
                                                                        LostFocus="function(s, e) {OnLostFocus(popupMensagemCarteira, ASPxCallback1, btnEditCodigoCarteira);}" />
                                                                </dxe:ASPxSpinEdit>
                                                            </td>
                                                            <td colspan="2">
                                                                <asp:TextBox ID="textNome" runat="server" CssClass="textLongo5" Enabled="False"
                                                                    Text='<%# Eval("Apelido") %>'></asp:TextBox>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="td_Label_Curto">
                                                                <asp:Label ID="labelData" runat="server" CssClass="labelRequired" Text="Data:"></asp:Label>
                                                            </td>
                                                            <td>
                                                                <dxe:ASPxDateEdit ID="textData" runat="server" ClientInstanceName="textData" EditFormatString="MM/yyyy"
                                                                    Value='<%#Eval("Data")%>' OnInit="textData_Init" />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="td_Label">
                                                                <asp:Label ID="labelCodigoBovespa" runat="server" CssClass="labelRequired" Text="Valor Administração Paga (Administrador):"></asp:Label>
                                                            </td>
                                                            <td>
                                                                <dxe:ASPxSpinEdit ID="textValorAdmAdministrador" CssClass="textNormal_5" runat="server"
                                                                    ClientInstanceName="textValorAdmAdministrador" Text='<%# Eval("ValorAdmAdministrador") %>'
                                                                    NumberType="Float" MaxLength="16" DecimalPlaces="2">
                                                                </dxe:ASPxSpinEdit>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="td_Label">
                                                                <asp:Label ID="label1" runat="server" CssClass="labelRequired" Text="Valor Pfee Paga (Administrador):"></asp:Label>
                                                            </td>
                                                            <td>
                                                                <dxe:ASPxSpinEdit ID="textValorPfeeAdministrador" CssClass="textNormal_5" runat="server"
                                                                    ClientInstanceName="textValorPfeeAdministrador" Text='<%# Eval("ValorPfeeAdministrador") %>'
                                                                    NumberType="Float" MaxLength="16" DecimalPlaces="2">
                                                                </dxe:ASPxSpinEdit>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="td_Label">
                                                                <asp:Label ID="label2" runat="server" CssClass="labelRequired" Text="Valor Outras Desp. Pagas (Administrador):"></asp:Label>
                                                            </td>
                                                            <td>
                                                                <dxe:ASPxSpinEdit ID="textValorOutrasAdministrador" CssClass="textNormal_5" runat="server"
                                                                    ClientInstanceName="textValorOutrasAdministrador" Text='<%# Eval("ValorOutrasAdministrador") %>'
                                                                    NumberType="Float" MaxLength="16" DecimalPlaces="2">
                                                                </dxe:ASPxSpinEdit>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="td_Label">
                                                                <asp:Label ID="label3" runat="server" CssClass="labelRequired" Text="Valor Administração Paga (Gestor):"></asp:Label>
                                                            </td>
                                                            <td>
                                                                <dxe:ASPxSpinEdit ID="textValorAdmGestor" CssClass="textNormal_5" runat="server"
                                                                    ClientInstanceName="textValorAdmGestor" Text='<%# Eval("ValorAdmGestor") %>'
                                                                    NumberType="Float" MaxLength="16" DecimalPlaces="2">
                                                                </dxe:ASPxSpinEdit>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="td_Label">
                                                                <asp:Label ID="label4" runat="server" CssClass="labelRequired" Text="Valor Pfee Paga (Gestor):"></asp:Label>
                                                            </td>
                                                            <td>
                                                                <dxe:ASPxSpinEdit ID="textValorPfeeGestor" CssClass="textNormal_5" runat="server"
                                                                    ClientInstanceName="textValorPfeeGestor" Text='<%# Eval("ValorPfeeGestor") %>'
                                                                    NumberType="Float" MaxLength="16" DecimalPlaces="2">
                                                                </dxe:ASPxSpinEdit>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="td_Label">
                                                                <asp:Label ID="label5" runat="server" CssClass="labelRequired" Text="Valor Outras Desp. Paga (Gestor):"></asp:Label>
                                                            </td>
                                                            <td>
                                                                <dxe:ASPxSpinEdit ID="textValorOutrasGestor" CssClass="textNormal_5" runat="server"
                                                                    ClientInstanceName="textValorOutrasGestor" Text='<%# Eval("ValorOutrasGestor") %>'
                                                                    NumberType="Float" MaxLength="16" DecimalPlaces="2">
                                                                </dxe:ASPxSpinEdit>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                                        </dxw:ContentControl>
                                                                    </ContentCollection>
                                                                </dxtc:TabPage>
                                                                <dxtc:TabPage Text="Fundo de Investimento">
                                                                    <ContentCollection>
                                                                        <dxw:ContentControl runat="server">
                                                                            <table>
                                                        <tr>
                                                            <td class="td_Label">
                                                                <asp:Label ID="label6" runat="server" CssClass="labelRequired" Text="Carteira:"></asp:Label>
                                                            </td>
                                                            <td>
                                                                <dxe:ASPxSpinEdit ID="btnEditCodigoCarteiraF" runat="server" CssClass="textButtonEdit"
                                                                    ClientInstanceName="btnEditCodigoCarteiraF" Text='<%# Eval("IdCarteira") %>' MaxLength="10"
                                                                    NumberType="Integer" OnInit="btnEditCodigoCarteira_Init">
                                                                    <Buttons>
                                                                        <dxe:EditButton>
                                                                        </dxe:EditButton>
                                                                    </Buttons>
                                                                    <ClientSideEvents                                                           
                                                                         KeyPress="function(s, e) {document.getElementById('textNomeF').value = '';}" 
                                                                         ButtonClick="function(s, e) {popupCarteira1.ShowAtElementByID(s.name);}" 
                                                                         LostFocus="function(s, e) {popupMensagemCarteira1.HideWindow();                                                                                        
                                                                            ASPxCallback2.SendCallback(btnEditCodigoCarteiraF.GetValue());
                                                                         }"
                                                                         />                    
                                                                </dxe:ASPxSpinEdit>
                                                            </td>
                                                            <td colspan="2">
                                                                <asp:TextBox ID="textNomeF" runat="server" CssClass="textLongo5" Enabled="False"
                                                                    Text='<%# Eval("Apelido") %>'></asp:TextBox>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="td_Label_Curto">
                                                                <asp:Label ID="label7" runat="server" CssClass="labelRequired" Text="Data:"></asp:Label>
                                                            </td>
                                                            <td>
                                                                <dxe:ASPxDateEdit ID="textDataF" runat="server" ClientInstanceName="textDataF" EditFormatString="MM/yyyy"
                                                                    Value='<%#Eval("Data")%>' OnInit="textData_Init" />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="td_Label">
                                                                <asp:Label ID="label8" runat="server" CssClass="labelRequired" Text="Valor Administração Paga (Administrador):"></asp:Label>
                                                            </td>
                                                            <td>
                                                                <dxe:ASPxSpinEdit ID="textValorAdministracaoPaga" CssClass="textNormal_5" runat="server"
                                                                    ClientInstanceName="textValorAdministracaoPaga" Text='<%# Eval("ValorAdministracaoPaga") %>'
                                                                    NumberType="Float" MaxLength="16" DecimalPlaces="2">
                                                                </dxe:ASPxSpinEdit>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="td_Label">
                                                                <asp:Label ID="label9" runat="server" CssClass="labelRequired" Text="Valor Pfee Paga (Administrador):"></asp:Label>
                                                            </td>
                                                            <td>
                                                                <dxe:ASPxSpinEdit ID="textValorPfeePaga" CssClass="textNormal_5" runat="server"
                                                                    ClientInstanceName="textValorPfeePaga" Text='<%# Eval("ValorPfeePaga") %>'
                                                                    NumberType="Float" MaxLength="16" DecimalPlaces="2">
                                                                </dxe:ASPxSpinEdit>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="td_Label">
                                                                <asp:Label ID="label10" runat="server" CssClass="labelRequired" Text="Valor Custódia Paga (Administrador):"></asp:Label>
                                                            </td>
                                                            <td>
                                                                <dxe:ASPxSpinEdit ID="textValorCustodiaPaga" CssClass="textNormal_5" runat="server"
                                                                    ClientInstanceName="textValorCustodiaPaga" Text='<%# Eval("ValorCustodiaPaga") %>'
                                                                    NumberType="Float" MaxLength="16" DecimalPlaces="2">
                                                                </dxe:ASPxSpinEdit>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="td_Label">
                                                                <asp:Label ID="label11" runat="server" CssClass="labelRequired" Text="Valor Outras Desp. Pagas (Administrador):"></asp:Label>
                                                            </td>
                                                            <td>
                                                                <dxe:ASPxSpinEdit ID="textValorOutrasDespesasPagas" CssClass="textNormal_5" runat="server"
                                                                    ClientInstanceName="textValorOutrasDespesasPagas" Text='<%# Eval("ValorOutrasDespesasPagas") %>'
                                                                    NumberType="Float" MaxLength="16" DecimalPlaces="2">
                                                                </dxe:ASPxSpinEdit>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="td_Label">
                                                                <asp:Label ID="label12" runat="server" CssClass="labelRequired" Text="Valor Administração Pagas (Grupo Econômico ADM):"></asp:Label>
                                                            </td>
                                                            <td>
                                                                <dxe:ASPxSpinEdit ID="textValorAdministracaoPagaGrupoEconomicoADM" CssClass="textNormal_5" runat="server"
                                                                    ClientInstanceName="textValorAdministracaoPagaGrupoEconomicoADM" Text='<%# Eval("ValorAdministracaoPagaGrupoEconomicoADM") %>'
                                                                    NumberType="Float" MaxLength="16" DecimalPlaces="2">
                                                                </dxe:ASPxSpinEdit>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="td_Label">
                                                                <asp:Label ID="label13" runat="server" CssClass="labelRequired" Text="Valor Despesas Operacionais e de Serviços (Grupo Econômico ADM):"></asp:Label>
                                                            </td>
                                                            <td>
                                                                <dxe:ASPxSpinEdit ID="textValorDespesasOperacionaisGrupoEconomicoADM" CssClass="textNormal_5" runat="server"
                                                                    ClientInstanceName="textValorDespesasOperacionaisGrupoEconomicoADM" Text='<%# Eval("ValorDespesasOperacionaisGrupoEconomicoADM") %>'
                                                                    NumberType="Float" MaxLength="16" DecimalPlaces="2">
                                                                </dxe:ASPxSpinEdit>
                                                            </td>
                                                        </tr>
                                                        
                                                         <tr>
                                                            <td class="td_Label">
                                                                <asp:Label ID="label14" runat="server" CssClass="labelRequired" Text="Valor Administração Paga (Grupo Econômico Gestor):"></asp:Label>
                                                            </td>
                                                            <td>
                                                                <dxe:ASPxSpinEdit ID="textValorAdministracaoPagaGrupoEconomicoGestor" CssClass="textNormal_5" runat="server"
                                                                    ClientInstanceName="textValorAdministracaoPagaGrupoEconomicoGestor" Text='<%# Eval("ValorAdministracaoPagaGrupoEconomicoGestor") %>'
                                                                    NumberType="Float" MaxLength="16" DecimalPlaces="2">
                                                                </dxe:ASPxSpinEdit>
                                                            </td>
                                                        </tr>
                                                        
                                                         <tr>
                                                            <td class="td_Label">
                                                                <asp:Label ID="label15" runat="server" CssClass="labelRequired" Text="Valor Despesas Operacionais e de Serviços (Grupo Econômico Gestor):"></asp:Label>
                                                            </td>
                                                            <td>
                                                                <dxe:ASPxSpinEdit ID="textValorDespesasOperacionaisGrupoEconomicoGestor" CssClass="textNormal_5" runat="server"
                                                                    ClientInstanceName="textValorDespesasOperacionaisGrupoEconomicoGestor" Text='<%# Eval("ValorDespesasOperacionaisGrupoEconomicoGestor") %>'
                                                                    NumberType="Float" MaxLength="16" DecimalPlaces="2">
                                                                </dxe:ASPxSpinEdit>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                                        </dxw:ContentControl>
                                                                    </ContentCollection>
                                                                </dxtc:TabPage>                                                                
                                                            </TabPages>
                                                        </dxtc:ASPxPageControl>
                                                        <div class="linkButton linkButtonNoBorder popupFooter">
                                                        <asp:LinkButton ID="btnOKAdd" runat="server" Font-Overline="false" CssClass="btnSaveadd"
                                                            OnInit="btnOKAdd_Init" OnClientClick="if (operacao != '') return false; operacao='salvarAdd'; callbackErro.SendCallback(); return false;">
                                                            <asp:Literal ID="Literal7" runat="server" Text="OK+" /><div>
                                                            </div>
                                                        </asp:LinkButton>
                                                        <asp:LinkButton ID="btnOK" runat="server" Font-Overline="false" CssClass="btnOK"
                                                            OnClientClick="if (operacao != '') return false; operacao='salvar'; callbackErro.SendCallback(); return false;">
                                                            <asp:Literal ID="Literal5" runat="server" Text="OK" /><div>
                                                            </div>
                                                        </asp:LinkButton>
                                                        <asp:LinkButton ID="btnCancel" runat="server" Font-Overline="false" CssClass="btnCancel"
                                                            OnClientClick="gridCadastro.CancelEdit(); return false;">
                                                            <asp:Literal ID="Literal3" runat="server" Text="Cancelar" /><div>
                                                            </div>
                                                        </asp:LinkButton>
                                                    </div>
                                                    </div>
                                                </asp:Panel>
                                            </EditForm>
                                        </Templates>
                                        <SettingsPopup EditForm-Width="500px" />
                                        <SettingsCommandButton>
                                            <ClearFilterButton Image-Url="../imagens/funnel--minus.png"/>
                                        </SettingsCommandButton>
                                    </dxwgv:ASPxGridView>
                                </div>
                            </div>
                        </div>
                    </td>
                </tr>
            </table>
        </div>
        <dxwgv:ASPxGridViewExporter ID="gridExport" runat="server" GridViewID="gridCadastro" />
        
        <cc1:esDataSource ID="EsDSTabelaInformeDesempenho" runat="server" OnesSelect="EsDSTabelaInformeDesempenho_esSelect" LowLevelBind="true" />
        <cc1:esDataSource ID="EsDSCarteira" runat="server" OnesSelect="EsDSCarteira_esSelect" />
        <cc1:esDataSource ID="EsDSCarteira1" runat="server" OnesSelect="EsDSCarteira_esSelect" />
        
        <cc1:esDataSource ID="EsDSCategoriaFundo" runat="server" OnesSelect="EsDSCategoriaFundo_esSelect" />
        
        
        <dxlp:ASPxLoadingPanel ID="LoadingPanelClonar" runat="server" Text="Processando, aguarde..." ClientInstanceName="LoadingPanelClonar" Modal="True"/>        
        <cc1:esDataSource ID="EsDSTabelaInformeDesempenhoClonar" runat="server" OnesSelect="EsDSTabelaInformeDesempenhoClonar_esSelect" LowLevelBind="true" />
        <cc1:esDataSource ID="EsDSCarteiraClonar" runat="server" OnesSelect="EsDSCarteiraClonar_esSelect" LowLevelBind="true" />

        
    </form>
</body>
</html>
