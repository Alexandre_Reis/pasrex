﻿using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using EntitySpaces.Interfaces;
using System.Collections.Generic;
using Financial.Web.Common;
using System.IO;
using System.Globalization;
using Dart.PowerTCP.Zip;
using System.Text;

public partial class _ExibeExportacaoImplantacao : BasePage {

    #region Enums
    enum TipoExportacao {
        Posicao = 1,
        Historico = 2,
        Operacao = 3,
    }
    #endregion

    void ResponseFile(Dictionary<string, MemoryStream> dictionary)
    {
        if (dictionary.Count == 1)
        {
            List<string> nomeArquivo = new List<string>(dictionary.Keys);
            List<MemoryStream> mPosicao = new List<MemoryStream>(dictionary.Values);

            
            Response.ClearContent();
            Response.ClearHeaders();
            Response.Buffer = true;
            Response.Cache.SetCacheability(HttpCacheability.Private);
            Response.ContentType = "application/xls";
            Response.AddHeader("Content-Length", mPosicao[0].Length.ToString(CultureInfo.CurrentCulture));
            Response.AppendHeader("Content-Disposition", String.Format("attachment;filename=" + nomeArquivo[0]));

            Response.BinaryWrite(mPosicao[0].ToArray());
            mPosicao[0].Close();
            //
            HttpContext.Current.ApplicationInstance.CompleteRequest();
            Response.End();
        }
        else if (dictionary.Count > 1)
        {
            #region Zip

            Archive arquivo = new Archive();
            MemoryStream msZip = new MemoryStream();



            try
            {

                int j = 0;
                foreach (KeyValuePair<string, MemoryStream> pair in dictionary)
                {
                    arquivo.Add(pair.Value); // Memory Stream
                    arquivo[j].Name = pair.Key; // Nome do Arquivo
                    //
                    j++;
                }

                //
                arquivo.Zip(msZip);
            }
            catch (Exception e1)
            {
                throw new Exception(e1.Message + "Erro ao Zipar o Arquivo");
            }
            Response.ClearContent();
            Response.ClearHeaders();
            Response.Buffer = true;
            Response.Cache.SetCacheability(HttpCacheability.Private);
            Response.ContentType = "application/zip";
            Response.AddHeader("Content-Length", msZip.Length.ToString(CultureInfo.CurrentCulture));
            Response.AppendHeader("Content-Disposition", String.Format("attachment;filename=PlanilhasPosicao.zip"));

            Response.BinaryWrite(msZip.ToArray());
            msZip.Close();

            HttpContext.Current.ApplicationInstance.CompleteRequest();
            Response.End();
     

   
            #endregion
        }

    }
    
    new protected void Page_Load(object sender, EventArgs e) {
        
        string tipo = Request.QueryString["Tipo"];

        //DateTime data = Convert.ToDateTime(Session["textData"]);

        switch ((int)Enum.Parse(typeof(TipoExportacao), tipo)) {
            case (int)TipoExportacao.Posicao:
                #region Posicao

                Dictionary<string, MemoryStream> msPosicao = (Dictionary<string, MemoryStream>)Session["streamPosicao"];                
                //            

                if (msPosicao.Count == 1) { // Excel
                    List<string> nomeArquivoPosicao = new List<string>(msPosicao.Keys);
                    List<MemoryStream> mPosicao = new List<MemoryStream>(msPosicao.Values);

                    #region Excel
                    Response.ClearContent();
                    Response.ClearHeaders();
                    Response.Buffer = true;
                    Response.Cache.SetCacheability(HttpCacheability.Private);
                    Response.ContentType = "application/xls";
                    Response.AddHeader("Content-Length", mPosicao[0].Length.ToString(CultureInfo.CurrentCulture));
                    Response.AppendHeader("Content-Disposition", String.Format("attachment;filename=" + nomeArquivoPosicao[0]));

                    Response.BinaryWrite(mPosicao[0].ToArray());
                    mPosicao[0].Close();
                    //
                    HttpContext.Current.ApplicationInstance.CompleteRequest();
                    Response.End();
                    #endregion
                }
                else { // Gera Zip
                    #region Zip

                    Archive arquivo = new Archive();
                    MemoryStream msZip = new MemoryStream();

                   
                            
                    try {

                        int j = 0;
                        foreach (KeyValuePair<string, MemoryStream> pair in msPosicao) {
                            arquivo.Add(pair.Value); // Memory Stream
                            arquivo[j].Name = pair.Key; // Nome do Arquivo
                            //
                            j++;
                        }

                        //
                        arquivo.Zip(msZip);
                    }
                    catch (Exception e1) {
                        throw new Exception(e1.Message + "Erro ao Zipar o Arquivo");
                    }
                    Response.ClearContent();
                    Response.ClearHeaders();
                    Response.Buffer = true;
                    Response.Cache.SetCacheability(HttpCacheability.Private);
                    Response.ContentType = "application/zip";
                    Response.AddHeader("Content-Length", msZip.Length.ToString(CultureInfo.CurrentCulture));
                    Response.AppendHeader("Content-Disposition", String.Format("attachment;filename=PlanilhasPosicao.zip"));

                    Response.BinaryWrite(msZip.ToArray());
                    msZip.Close();

                    HttpContext.Current.ApplicationInstance.CompleteRequest();
                    Response.End();
                    #endregion
                }                                
                #endregion
                break;
            case (int)TipoExportacao.Historico:
               

                Dictionary<string, MemoryStream> msHistorico = (Dictionary<string, MemoryStream>)Session["streamHistorico"];

                try
                {
                    ResponseFile(msHistorico);
                }
                catch (Exception ex)
                {
                    
                    throw;
                }
                

                
                /*if (msHistorico.Count > 0) {

                    List<string> nomeArquivoHistorico = new List<string>(msHistorico.Keys);
                    List<MemoryStream> mHistorico = new List<MemoryStream>(msHistorico.Values);

                    HttpContext oc = HttpContext.Current;
                    for (int i = 0; i <= nomeArquivoHistorico.Count; i++)
                    {

                        #region Excel

                        oc.Response.ClearContent();
                        oc.Response.ClearHeaders();
                        oc.Response.Buffer = true;
                        oc.Response.Cache.SetCacheability(HttpCacheability.Private);
                        oc.Response.ContentType = "application/xls";
                        oc.Response.AddHeader("Content-Length", mHistorico[i].Length.ToString(CultureInfo.CurrentCulture));
                        oc.Response.AppendHeader("Content-Disposition", String.Format("attachment;filename=" + nomeArquivoHistorico[i]));

                        oc.Response.BinaryWrite(mHistorico[i].ToArray());
                        mHistorico[i].Close();
                        //
                        //HttpContext.Current.ApplicationInstance.CompleteRequest();
                        //Response.End();
                        oc.ApplicationInstance.CompleteRequest();
                        
                    }
                   
                    oc.Response.End();
                    
                    #endregion
                }                
                #endregion */
                break;
            case (int)TipoExportacao.Operacao:
                #region Operacao

                Dictionary<string, MemoryStream> msOperacao = (Dictionary<string, MemoryStream>)Session["streamOperacao"];

                if (msOperacao.Count == 1) { // Excel           
                    List<string> nomeArquivoOperacao = new List<string>(msOperacao.Keys);
                    List<MemoryStream> mOperacao = new List<MemoryStream>(msOperacao.Values);

                    #region Excel
                    Response.ClearContent();
                    Response.ClearHeaders();
                    Response.Buffer = true;
                    Response.Cache.SetCacheability(HttpCacheability.Private);
                    Response.ContentType = "application/xls";
                    Response.AddHeader("Content-Length", mOperacao[0].Length.ToString(CultureInfo.CurrentCulture));
                    Response.AppendHeader("Content-Disposition", String.Format("attachment;filename=" + nomeArquivoOperacao[0]));

                    Response.BinaryWrite(mOperacao[0].ToArray());
                    mOperacao[0].Close();
                    //
                    HttpContext.Current.ApplicationInstance.CompleteRequest();
                    Response.End();
                    #endregion
                }
                else { // Gera Zip
                    #region Zip

                    Archive arquivo = new Archive();
                    MemoryStream msZip = new MemoryStream();
                    //                
                    try {
                        int j = 0;
                        foreach (KeyValuePair<string, MemoryStream> pair in msOperacao) {
                            arquivo.Add(pair.Value); // Memory Stream
                            arquivo[j].Name = pair.Key; // Nome do Arquivo
                            //
                            j++;
                        }
                        //
                        arquivo.Zip(msZip);
                    }
                    catch (Exception e1) {
                        throw new Exception(e1.Message + "Erro ao Zipar o Arquivo");
                    }
                    Response.ClearContent();
                    Response.ClearHeaders();
                    Response.Buffer = true;
                    Response.Cache.SetCacheability(HttpCacheability.Private);
                    Response.ContentType = "application/zip";
                    Response.AddHeader("Content-Length", msZip.Length.ToString(CultureInfo.CurrentCulture));
                    Response.AppendHeader("Content-Disposition", String.Format("attachment;filename=PlanilhasOperacao.zip"));

                    Response.BinaryWrite(msZip.ToArray());
                    msZip.Close();

                    HttpContext.Current.ApplicationInstance.CompleteRequest();
                    Response.End();
                    #endregion
                }
                #endregion
                break;
        }               
    }
}