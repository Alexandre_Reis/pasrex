﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="CadastroTipoDePara.aspx.cs"
    Inherits="Configuracao_CadastroTipoDePara" %>

<%@ Register Assembly="System.Web, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"
    Namespace="System.Web.UI.HtmlControls" TagPrefix="cc2" %>
<%@ Register Assembly="EntitySpaces.Web" Namespace="EntitySpaces.Web" TagPrefix="cc1" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxe" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxcb" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxpc" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxwgv" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <link href="~/css/forms.css" rel="stylesheet" type="text/css" />

    <script type="text/javascript" language="Javascript" src="../js/global.js"></script>

    <script type="text/javascript" language="Javascript">
        var popup = true;
        document.onkeydown = onDocumentKeyDown;
        var operacao = '';
    </script>

    <script type="text/javascript" language="Javascript">
        var isCustomCallback = false;   // usada para controlar o refresh após um delete feito no grid
    </script>

</head>
<body>
    <form id="form1" runat="server">
        <dxcb:ASPxCallback ID="callbackErro" runat="server" OnCallback="callbackErro_Callback">
            <ClientSideEvents CallbackComplete="function(s, e) {             
            if (e.result != '')
            {                   
                alert(e.result);                              
            }
            else
            {
                if (operacao == 'salvar')
                {             
                    gridCadastro.UpdateEdit();
                }
                else
                {
                    callbackAdd.SendCallback();
                }  
            } 
            operacao = '';
        }        
        " />
        </dxcb:ASPxCallback>
        <dxcb:ASPxCallback ID="callbackAdd" runat="server" OnCallback="callbackAdd_Callback">
            <ClientSideEvents CallbackComplete="function(s, e) 
        {
            alert('Cadastro feito com sucesso.');
        }        
        " />
        </dxcb:ASPxCallback>
        <div class="divPanel">
            <table width="100%">
                <tr>
                    <td>
                        <div id="container">
                            <div id="header">
                                <asp:Label ID="lblHeader" runat="server" Text="Cadastro de Tipo De Para"></asp:Label>
                            </div>
                            <div id="mainContent">
                                <div class="linkButton">
                                    <asp:LinkButton ID="btnAdd" runat="server" Font-Overline="false" ValidationGroup="ATK"
                                        CssClass="btnAdd" OnClientClick="gridCadastro.AddNewRow(); return false;">
                                        <asp:Literal ID="Literal1" runat="server" Text="Novo" /><div>
                                        </div>
                                    </asp:LinkButton><asp:LinkButton ID="btnDelete" runat="server" Font-Overline="false"
                                        ValidationGroup="ATK" CssClass="btnDelete" OnClientClick=" if (confirm('Tem certeza que quer excluir?')==true) gridCadastro.PerformCallback('btnDelete');return false;">
                                        <asp:Literal ID="Literal2" runat="server" Text="Excluir" /><div>
                                        </div>
                                    </asp:LinkButton><asp:LinkButton ID="btnPDF" runat="server" Font-Overline="false"
                                        ValidationGroup="ATK" CssClass="btnPdf" OnClick="btnPDF_Click">
                                        <asp:Literal ID="Literal4" runat="server" Text="Gerar PDF" /><div>
                                        </div>
                                    </asp:LinkButton><asp:LinkButton ID="btnExcel" runat="server" Font-Overline="false"
                                        ValidationGroup="ATK" CssClass="btnExcel" OnClick="btnExcel_Click">
                                        <asp:Literal ID="Literal5" runat="server" Text="Gerar Excel" /><div>
                                        </div>
                                    </asp:LinkButton><asp:LinkButton ID="btnRefresh" runat="server" Font-Overline="false"
                                        CssClass="btnRefresh" OnClientClick=" gridCadastro.PerformCallback('btnRefresh'); return false;">
                                        <asp:Literal ID="Literal6" runat="server" Text="Atualizar" /><div>
                                        </div>
                                    </asp:LinkButton></div>
                                <div class="divDataGrid">
                                    <dxwgv:ASPxGridView ID="gridCadastro" runat="server" KeyFieldName="IdTipoDePara"
                                        DataSourceID="EsDSTipoDePara" OnCustomCallback="gridCadastro_CustomCallback"
                                        OnRowInserting="gridCadastro_RowInserting" OnRowUpdating="gridCadastro_RowUpdating"
                                        OnPreRender="gridCadastro_PreRender" AutoGenerateColumns="False" >
                                        <Columns>
                                            <dxwgv:GridViewCommandColumn VisibleIndex="0" ShowClearFilterButton="True">
                                                <HeaderTemplate>
                                                    <dxe:ASPxCheckBox ID="cbAll" ClientInstanceName="cbAll" runat="server" ToolTip="Seleciona todos os CheckBoxs de todas as páginas" BackColor="white" ClientSideEvents-CheckedChanged="function(s, e) {OnAllCheckedChanged(s, e, gridCadastro);}" OnInit="CheckBoxSelectAll"/>
                                                </HeaderTemplate>
                                            </dxwgv:GridViewCommandColumn>
                                            <dxwgv:GridViewDataSpinEditColumn FieldName="IdTipoDePara" VisibleIndex="1" Width="15%"
                                                ReadOnly="True">
                                                <PropertiesSpinEdit SpinButtons-ShowIncrementButtons="false" DisplayFormatString="n0">
                                                    <SpinButtons ShowIncrementButtons="False">
                                                    </SpinButtons>
                                                    <ValidationSettings RequiredField-ErrorText="">
                                                        <RequiredField ErrorText=""></RequiredField>
                                                    </ValidationSettings>
                                                </PropertiesSpinEdit>
                                            </dxwgv:GridViewDataSpinEditColumn>
                                            <dxwgv:GridViewDataComboBoxColumn FieldName="TipoCampo" Caption="Tipo Campo" Width="15%" VisibleIndex="2" >
                                            <PropertiesComboBox EncodeHtml="false">
                                                <Items>                                                    
                                                    <dxe:ListEditItem Value="1" Text="<div title='Inteiro'>Inteiro</div>" />
                                                    <dxe:ListEditItem Value="2" Text="<div title='Data'>Data</div>" />
                                                    <dxe:ListEditItem Value="3" Text="<div title='Texto'>Texto</div>" />
                                                </Items>
                                            </PropertiesComboBox>
                                            </dxwgv:GridViewDataComboBoxColumn>  
                                            <dxwgv:GridViewDataTextColumn FieldName="Descricao" Caption="Descrição" Width="20%" VisibleIndex="3" />
                                            <dxwgv:GridViewDataTextColumn FieldName="Observacao" Caption="Observação" Width="15%" VisibleIndex="4" />
                                            <dxwgv:GridViewDataComboBoxColumn FieldName="EnumTipoDePara" Caption="EnumTipoDePara" Width="25%" VisibleIndex="5" >                            
                                            <PropertiesComboBox EncodeHtml="false">
                                                <Items>                                                    
                                                    <dxe:ListEditItem Value="1" Text="<div title='Fincs Indexador'>Fincs Indexador</div>" />
                                                    <dxe:ListEditItem Value="2" Text="<div title='Importacao ASEL007 Indice'>Importacao ASEL007 Indice</div>" />
                                                </Items>
                                            </PropertiesComboBox>
                                            </dxwgv:GridViewDataComboBoxColumn>
                                            
                                        </Columns>
                                        <Templates>
                                            <EditForm>
                                                <div class="editForm">
                                                    <dxe:ASPxTextBox ID="hiddenIdTipoDePara" runat="server" CssClass="hiddenField" ClientInstanceName="hiddenIdTipoDePara" Text='<%#Eval("IdTipoDePara")%>' />
                                                    <table>
                                                        <tr>
                                                            <td class="td_Label">
                                                                <asp:Label ID="labelDescricao" runat="server" AssociatedControlID="textDescricao"
                                                                    CssClass="labelRequired" Text="Descrição:" />
                                                            </td>
                                                            <td>
                                                                <dxe:ASPxTextBox ID="textDescricao" ClientInstanceName="textDescricao" runat="server"
                                                                    CssClass="textLongo" MaxLength="50" Text='<%#Eval("Descricao")%>' />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="td_Label">
                                                                <asp:Label ID="labelTipoCampo" runat="server" CssClass="labelRequired"
                                                                    Text="Tipo:" />
                                                            </td>
                                                            <td>
                                                                <dxe:ASPxComboBox ID="dropTipoCampo" runat="server" ShowShadow="false" DropDownStyle="DropDownList"
                                                                    CssClass="dropDownListCurto_1" OnLoad="TipoCampo_OnLoad" Text='<%#Eval("TipoCampo")%>'>
                                                                </dxe:ASPxComboBox>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="td_Label">
                                                                <asp:Label ID="labelObservacao" runat="server" AssociatedControlID="textObservacao" Text="Observação:" />
                                                            </td>
                                                            <td>
                                                                <dxe:ASPxTextBox ID="textObservacao" ClientInstanceName="textObservacao" runat="server"
                                                                    CssClass="textLongo" MaxLength="50" Text='<%#Eval("Observacao")%>'/>
                                                            </td>
                                                        </tr>
                                                         <tr>
                                                            <td class="td_Label">
                                                                <asp:Label ID="labelEnumTipoDePara" runat="server" Text="Tipo De Para:" />
                                                            </td>
                                                            <td>
                                                                <dxe:ASPxComboBox ID="dropEnumTipoDePara" runat="server" ShowShadow="false" DropDownStyle="DropDownList"
                                                                    CssClass="dropDownListCurto_1" OnLoad="enumTipoDePara_OnLoad" Text='<%#Eval("EnumTipoDePara")%>'>
                                                                </dxe:ASPxComboBox>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                    <div class="linhaH">
                                                    </div>
                                                    <div class="linkButton linkButtonNoBorder popupFooter">
                                                        <asp:LinkButton ID="btnOKAdd" runat="server" Font-Overline="false" CssClass="btnSaveadd"
                                                            OnInit="btnOKAdd_Init" OnClientClick="if (operacao != '') return false; operacao='salvarAdd'; callbackErro.SendCallback(); return false;">
                                                            <asp:Literal ID="Literal5" runat="server" Text="OK+" /><div>
                                                            </div>
                                                        </asp:LinkButton><asp:LinkButton ID="btnOK" runat="server" Font-Overline="false"
                                                            CssClass="btnOK" OnClientClick="if (operacao != '') return false; operacao='salvar'; callbackErro.SendCallback(); return false;">
                                                            <asp:Literal ID="Literal1" runat="server" Text="OK" /><div>
                                                            </div>
                                                        </asp:LinkButton><asp:LinkButton ID="btnCancel" runat="server" Font-Overline="false"
                                                            CssClass="btnCancel" OnClientClick=" gridCadastro.PerformCallback('btnCancel'); return false;">
                                                            <asp:Literal ID="Literal2" runat="server" Text="Cancelar" /><div>
                                                            </div>
                                                        </asp:LinkButton></div>
                                                </div>
                                            </EditForm>
                                        </Templates>
                                        <SettingsPopup EditForm-Width="600px" />
                                        <ClientSideEvents Init="function(s, e) { FocusField(gridCadastro.cpTextDescricao);}"
                                            BeginCallback="function(s, e) {
		                if (e.command == 'CUSTOMCALLBACK') {
                            isCustomCallback = true;
                        }						
                    }" EndCallback="function(s, e) {
			            if (isCustomCallback) {
                            isCustomCallback = false;
                            s.Refresh();
                        }
                    }" />
                                        <SettingsCommandButton>
                                            <ClearFilterButton Image-Url="../../imagens/funnel--minus.png">
                                                <Image Url="../../imagens/funnel--minus.png">
                                                </Image>
                                            </ClearFilterButton>
                                            <UpdateButton Image-Url="../../imagens/ico_form_ok_inline.gif">
                                                <Image Url="../../imagens/ico_form_ok_inline.gif">
                                                </Image>
                                            </UpdateButton>
                                            <CancelButton Image-Url="../../imagens/ico_form_back_inline.gif">
                                                <Image Url="../../imagens/ico_form_back_inline.gif">
                                                </Image>
                                            </CancelButton>
                                        </SettingsCommandButton>
                                    </dxwgv:ASPxGridView>
                                </div>
                            </div>
                        </div>
                    </td>
                </tr>
            </table>
        </div>
        <dxwgv:ASPxGridViewExporter ID="gridExport" runat="server" GridViewID="gridCadastro"
            LeftMargin="50" RightMargin="50" />
        <cc1:esDataSource ID="EsDSTipoDePara" runat="server" OnesSelect="EsDSTipoDePara_esSelect" />
    </form>
</body>
</html>
