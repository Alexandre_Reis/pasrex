﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Web.UI.HtmlControls;
using Financial.Common;
using System.Collections.Generic;
using Financial.Util;
using System.Text;
using DevExpress.Web;
using System.Drawing;
using Financial.CRM;
using Financial.Security;
using Financial.Fundo;
using Financial.Web.Util;
using Financial.Security.Enums;
using Financial.Web.Common;
using EntitySpaces.Interfaces;
using Financial.Bolsa;
using Financial.Investidor;

public partial class CadastrosBasicos_TabelaInterfaceCliente : CadastroBasePage {
    new protected void Page_Load(object sender, EventArgs e) {
        
        this.AllowUpdate = false; // Não permite Update
        //
        this.HasPopupCliente = true;   // PopUp Cliente
        //
        base.Page_Load(sender, e);

        UtilitarioGrid.ExportacaoPDFExcel(this.gridExport,
                         new List<string>(new string[] { TabelaInterfaceClienteMetadata.ColumnNames.TipoInterface }));
    }

    #region DataSources

    /// <summary>
    /// Consulta Principal
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void EsDSTabelaInterfaceCliente_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e) {
        TabelaInterfaceClienteQuery tabelaInterfaceClienteQuery = new TabelaInterfaceClienteQuery("T");
        ClienteQuery clienteQuery = new ClienteQuery("C");        
        //
        tabelaInterfaceClienteQuery.Select(tabelaInterfaceClienteQuery.IdCliente, tabelaInterfaceClienteQuery.TipoInterface,
                                           (clienteQuery.IdCliente.Cast(esCastType.String) + " - " + clienteQuery.Apelido).As("NomeCliente")                                                                                      
                                           );
        //
        tabelaInterfaceClienteQuery.InnerJoin(clienteQuery).On(tabelaInterfaceClienteQuery.IdCliente == clienteQuery.IdCliente);
        //
        tabelaInterfaceClienteQuery.OrderBy(tabelaInterfaceClienteQuery.IdCliente.Ascending);
        //
        TabelaInterfaceClienteCollection coll = new TabelaInterfaceClienteCollection();
        coll.Load(tabelaInterfaceClienteQuery);
        //       
        e.Collection = coll;
    }

    /// <summary>
    /// Popup Carteira
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void EsDSCliente_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e) {
        ClienteCollection coll = new ClienteCollection();
        coll.BuscaClientesComAcesso(Context.User.Identity.Name);
        e.Collection = coll;
    }    
    #endregion

    #region Popup Cliente
    /// <summary>
    /// Pop Up Cliente
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void ASPxCallback1_Callback(object source, DevExpress.Web.CallbackEventArgs e) {
        string nome = "";
        if (!String.IsNullOrEmpty(e.Parameter) && e.Parameter != "null") {
            int idCliente = Convert.ToInt32(e.Parameter);
            Cliente cliente = new Cliente();
            cliente.LoadByPrimaryKey(idCliente);

            if (cliente.str.Apelido != "") {
                if (cliente.IsAtivo) {
                    PermissaoCliente permissaoCliente = new PermissaoCliente();
                    nome = permissaoCliente.RetornaAcessoClienteComControle(idCliente, HttpContext.Current.User.Identity.Name)
                            ? cliente.str.Apelido : "no_access";
                }
                else {
                    nome = "no_active";
                }
            }
        }
        e.Result = nome;
    }

    #endregion

    /// <summary>
    /// Tratamento Erro
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void callbackErro_Callback(object source, DevExpress.Web.CallbackEventArgs e) {
        e.Result = "";

        ASPxSpinEdit btnEditCodigo = gridCadastro.FindEditFormTemplateControl("btnEditCodigo") as ASPxSpinEdit;
        ASPxComboBox dropTipoInterface = gridCadastro.FindEditFormTemplateControl("dropTipoInterface") as ASPxComboBox;

        #region Campos obrigatórios
        List<Control> controles = new List<Control>();
        controles.Add(btnEditCodigo);
        controles.Add(dropTipoInterface);

        if (base.TestaObrigatorio(controles) != "" || dropTipoInterface.SelectedIndex == 0) {
            e.Result = "Campos com * são obrigatórios!";
            return;
        }
        #endregion

        int idCliente = Convert.ToInt32(btnEditCodigo.Text);
        int tipoInterface = Convert.ToInt32(dropTipoInterface.SelectedItem.Value);

        TabelaInterfaceCliente tabelaInterfaceCliente = new TabelaInterfaceCliente();
        if (tabelaInterfaceCliente.LoadByPrimaryKey(idCliente, tipoInterface)) {
            e.Result = "Registro já existente.";
        }
    }

    protected void callbackAdd_Callback(object source, DevExpress.Web.CallbackEventArgs e) {
        this.SalvarNovo();
    }

    private void SalvarNovo()
    {
        ASPxSpinEdit btnEditCodigo = gridCadastro.FindEditFormTemplateControl("btnEditCodigo") as ASPxSpinEdit;
        ASPxComboBox dropTipoInterface = gridCadastro.FindEditFormTemplateControl("dropTipoInterface") as ASPxComboBox;
        //
        TabelaInterfaceCliente tabelaInterfaceCliente = new TabelaInterfaceCliente();
        ////
        tabelaInterfaceCliente.IdCliente = Convert.ToInt32(btnEditCodigo.Text);
        tabelaInterfaceCliente.TipoInterface = Convert.ToInt32(dropTipoInterface.SelectedItem.Value);

        tabelaInterfaceCliente.Save();

        #region Log do Processo
        HistoricoLog historicoLog = new HistoricoLog();
        historicoLog.InsereHistoricoLog(DateTime.Now,
                                        DateTime.Now,
                                        "Cadastro de TabelaInterfaceCliente - Operacao: Insert TabelaInterfaceCliente: " + tabelaInterfaceCliente.IdCliente.Value + "; " + tabelaInterfaceCliente.TipoInterface.Value + "; " + UtilitarioWeb.ToString(tabelaInterfaceCliente),
                                        HttpContext.Current.User.Identity.Name,
                                        UtilitarioWeb.GetIP(Request),
                                        "",
                                        HistoricoLogOrigem.Outros);
        #endregion
    }

    protected void btnOKAdd_Init(object sender, EventArgs e)
    {
        if (!gridCadastro.IsNewRowEditing)
            (sender as LinkButton).Visible = false;
    }

    /// <summary>
    /// Inserção
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_RowInserting(object sender, DevExpress.Web.Data.ASPxDataInsertingEventArgs e) {
        this.SalvarNovo();

        e.Cancel = true;
        gridCadastro.CancelEdit();
        gridCadastro.DataBind();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_CustomUnboundColumnData(object sender, DevExpress.Web.ASPxGridViewColumnDataEventArgs e) {
        if (e.Column.FieldName == "CompositeKey") {
            //Compose a primary key value
            string idCliente = Convert.ToString(e.GetListSourceFieldValue(TabelaInterfaceClienteMetadata.ColumnNames.IdCliente));
            string tipoInterface = Convert.ToString(e.GetListSourceFieldValue(TabelaInterfaceClienteMetadata.ColumnNames.TipoInterface));

            e.Value = idCliente + tipoInterface;
        }
    }

    /// <summary>
    /// Tratamento Delete
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_CustomCallback(object sender, DevExpress.Web.ASPxGridViewCustomCallbackEventArgs e) {
        if (e.Parameters == "btnDelete") {
            #region Delete
            List<object> keyValuesIdCliente = gridCadastro.GetSelectedFieldValues(TabelaInterfaceClienteMetadata.ColumnNames.IdCliente);
            List<object> keyValuesTipoInterface = gridCadastro.GetSelectedFieldValues(TabelaInterfaceClienteMetadata.ColumnNames.TipoInterface);

            for (int i = 0; i < keyValuesIdCliente.Count; i++) {
                int idCliente = Convert.ToInt32(keyValuesIdCliente[i]);
                int tipoInterface = Convert.ToInt32(keyValuesTipoInterface[i]);

                TabelaInterfaceCliente tabelaInterfaceCliente = new TabelaInterfaceCliente();
                if (tabelaInterfaceCliente.LoadByPrimaryKey(idCliente, tipoInterface)) {
                    TabelaInterfaceCliente tabelaInterfaceClienteClone = (TabelaInterfaceCliente)Utilitario.Clone(tabelaInterfaceCliente);
                    //

                    tabelaInterfaceCliente.MarkAsDeleted();
                    tabelaInterfaceCliente.Save();

                    #region Log do Processo
                    HistoricoLog historicoLog = new HistoricoLog();
                    historicoLog.InsereHistoricoLog(DateTime.Now,
                                                    DateTime.Now,
                                                    "Cadastro de TabelaInterfaceCliente - Operacao: Delete TabelaInterfaceCliente: " + idCliente + "; " + tipoInterface + "; " + UtilitarioWeb.ToString(tabelaInterfaceClienteClone),
                                                    HttpContext.Current.User.Identity.Name,
                                                    UtilitarioWeb.GetIP(Request),
                                                    "",
                                                    HistoricoLogOrigem.Outros);
                    #endregion
                }
            }
            #endregion
        }

        gridCadastro.DataBind();
        gridCadastro.Selection.UnselectAll();
        gridCadastro.CancelEdit();
    }

    /// <summary>
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    new protected void gridCadastro_PreRender(object sender, EventArgs e) {        
        base.gridCadastro_PreRender(sender, e);        
    }
}