﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Web.UI.HtmlControls;
using Financial.Common;
using System.Collections.Generic;
using Financial.Util;
using System.Text;
using DevExpress.Web;
using System.Drawing;
using Financial.Bolsa;
using Financial.Web.Common;
using Financial.Security;
using Financial.Web.Util;
using Financial.Security.Enums;
using Financial.Fundo;

public partial class CadastrosBasicos_TabelaReclamacoesCotistas : CadastroBasePage
{
    new protected void Page_Load(object sender, EventArgs e) {
        base.Page_Load(sender, e);
    }

    protected void EsDSTabela_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e) 
    {
        TabelaReclamacoesCotistasCollection coll = new TabelaReclamacoesCotistasCollection();

        coll.Query.OrderBy(coll.Query.Data.Descending);
        coll.LoadAll();

        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }

    protected void callbackErro_Callback(object source, DevExpress.Web.CallbackEventArgs e)
    {
        e.Result = "";
        ASPxDateEdit textData = gridCadastro.FindEditFormTemplateControl("textData") as ASPxDateEdit;
        
        #region Campos obrigatórios
        List<Control> controles = new List<Control>();
        controles.Add(textData);
        
        if (base.TestaObrigatorio(controles) != "")
        {
            e.Result = "Campos com * são obrigatórios!";
            return;
        }
        #endregion

        if (gridCadastro.IsNewRowEditing)
        {
            DateTime data = Convert.ToDateTime(textData.Text);

            TabelaReclamacoesCotistas tabelaReclamacoesCotistas = new TabelaReclamacoesCotistas();
            if (tabelaReclamacoesCotistas.LoadByPrimaryKey(data))
            {
                e.Result = "Registro já existente";
            }
        }
    }
    
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void textData_Init(object sender, EventArgs e) {
        if (!gridCadastro.IsNewRowEditing)
            (sender as ASPxDateEdit).Enabled = false;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_RowUpdating(object sender, DevExpress.Web.Data.ASPxDataUpdatingEventArgs e) 
    {
        TabelaReclamacoesCotistas tabelaReclamacoesCotistas = new TabelaReclamacoesCotistas();

        ASPxDateEdit textData = gridCadastro.FindEditFormTemplateControl("textData") as ASPxDateEdit;
        ASPxSpinEdit textProcedentes = gridCadastro.FindEditFormTemplateControl("textProcedentes") as ASPxSpinEdit;
        ASPxSpinEdit textImprocedentes = gridCadastro.FindEditFormTemplateControl("textImprocedentes") as ASPxSpinEdit;
        ASPxSpinEdit textResolvidas = gridCadastro.FindEditFormTemplateControl("textResolvidas") as ASPxSpinEdit;
        ASPxSpinEdit textNaoResolvidas = gridCadastro.FindEditFormTemplateControl("textNaoResolvidas") as ASPxSpinEdit;
        ASPxSpinEdit textAdministracao = gridCadastro.FindEditFormTemplateControl("textAdministracao") as ASPxSpinEdit;
        ASPxSpinEdit textGestao = gridCadastro.FindEditFormTemplateControl("textGestao") as ASPxSpinEdit;
        ASPxSpinEdit textTaxas = gridCadastro.FindEditFormTemplateControl("textTaxas") as ASPxSpinEdit;
        ASPxSpinEdit textPoliticaInv = gridCadastro.FindEditFormTemplateControl("textPoliticaInv") as ASPxSpinEdit;
        ASPxSpinEdit textAportes = gridCadastro.FindEditFormTemplateControl("textAportes") as ASPxSpinEdit;
        ASPxSpinEdit textResgates = gridCadastro.FindEditFormTemplateControl("textResgates") as ASPxSpinEdit;
        ASPxSpinEdit textFornecimentoInfos = gridCadastro.FindEditFormTemplateControl("textFornecimentoInfos") as ASPxSpinEdit;
        ASPxSpinEdit textAssembleiasGerais = gridCadastro.FindEditFormTemplateControl("textAssembleiasGerais") as ASPxSpinEdit;
        ASPxSpinEdit textCotas = gridCadastro.FindEditFormTemplateControl("textCotas") as ASPxSpinEdit;
        ASPxSpinEdit textOutros = gridCadastro.FindEditFormTemplateControl("textOutros") as ASPxSpinEdit;

        DateTime data = Convert.ToDateTime(textData.Text);
        int procedentes = textProcedentes.Text == ""? 0: Convert.ToInt32(textProcedentes.Text);
        int improcedentes = textImprocedentes.Text == "" ? 0 : Convert.ToInt32(textImprocedentes.Text);
        int resolvidas = textResolvidas.Text == "" ? 0 : Convert.ToInt32(textResolvidas.Text);
        int naoResolvidas = textNaoResolvidas.Text == "" ? 0 : Convert.ToInt32(textNaoResolvidas.Text);
        int administracao = textAdministracao.Text == "" ? 0 : Convert.ToInt32(textAdministracao.Text);
        int gestao = textGestao.Text == "" ? 0 : Convert.ToInt32(textGestao.Text);
        int taxas = textTaxas.Text == "" ? 0 : Convert.ToInt32(textTaxas.Text);
        int politicaInv = textPoliticaInv.Text == "" ? 0 : Convert.ToInt32(textPoliticaInv.Text);
        int aportes = textAportes.Text == "" ? 0 : Convert.ToInt32(textAportes.Text);
        int resgates = textResgates.Text == "" ? 0 : Convert.ToInt32(textResgates.Text);
        int fornecimentoInfos = textFornecimentoInfos.Text == "" ? 0 : Convert.ToInt32(textFornecimentoInfos.Text);
        int assembleiasGerais = textAssembleiasGerais.Text == "" ? 0 : Convert.ToInt32(textAssembleiasGerais.Text);
        int cotas = textCotas.Text == "" ? 0 : Convert.ToInt32(textCotas.Text);
        int outros = textOutros.Text == "" ? 0 : Convert.ToInt32(textOutros.Text);
        
        if (tabelaReclamacoesCotistas.LoadByPrimaryKey(data))
        {
            tabelaReclamacoesCotistas.Procedentes = procedentes;
            tabelaReclamacoesCotistas.Improcedentes = improcedentes;
            tabelaReclamacoesCotistas.Resolvidas = resolvidas;
            tabelaReclamacoesCotistas.NaoResolvidas = naoResolvidas;
            tabelaReclamacoesCotistas.Administracao = administracao;
            tabelaReclamacoesCotistas.Gestao = gestao;
            tabelaReclamacoesCotistas.Taxas = taxas;
            tabelaReclamacoesCotistas.PoliticaInvestimento = politicaInv;
            tabelaReclamacoesCotistas.Aportes = aportes;
            tabelaReclamacoesCotistas.Resgates = resgates;
            tabelaReclamacoesCotistas.FornecimentoInformacoes = fornecimentoInfos;
            tabelaReclamacoesCotistas.AssembleiasGerais = assembleiasGerais;
            tabelaReclamacoesCotistas.Cotas = cotas;
            tabelaReclamacoesCotistas.Outros = outros;
            tabelaReclamacoesCotistas.Save();
            
            #region Log do Processo
            HistoricoLog historicoLog = new HistoricoLog();
            historicoLog.InsereHistoricoLog(DateTime.Now,
                                            DateTime.Now,
                                            "Cadastro de TabelaReclamacoesCotistas - Operacao: Update TabelaReclamacoesCotistas: " + data + UtilitarioWeb.ToString(tabelaReclamacoesCotistas),
                                            HttpContext.Current.User.Identity.Name,
                                            UtilitarioWeb.GetIP(Request),
                                            "",
                                            HistoricoLogOrigem.Outros);
            #endregion
        }

        e.Cancel = true;
        gridCadastro.CancelEdit();
        gridCadastro.DataBind();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_RowInserting(object sender, DevExpress.Web.Data.ASPxDataInsertingEventArgs e) 
    {
        TabelaReclamacoesCotistas tabelaReclamacoesCotistas = new TabelaReclamacoesCotistas();

        ASPxDateEdit textData = gridCadastro.FindEditFormTemplateControl("textData") as ASPxDateEdit;
        ASPxSpinEdit textProcedentes = gridCadastro.FindEditFormTemplateControl("textProcedentes") as ASPxSpinEdit;
        ASPxSpinEdit textImprocedentes = gridCadastro.FindEditFormTemplateControl("textImprocedentes") as ASPxSpinEdit;
        ASPxSpinEdit textResolvidas = gridCadastro.FindEditFormTemplateControl("textResolvidas") as ASPxSpinEdit;
        ASPxSpinEdit textNaoResolvidas = gridCadastro.FindEditFormTemplateControl("textNaoResolvidas") as ASPxSpinEdit;
        ASPxSpinEdit textAdministracao = gridCadastro.FindEditFormTemplateControl("textAdministracao") as ASPxSpinEdit;
        ASPxSpinEdit textGestao = gridCadastro.FindEditFormTemplateControl("textGestao") as ASPxSpinEdit;
        ASPxSpinEdit textTaxas = gridCadastro.FindEditFormTemplateControl("textTaxas") as ASPxSpinEdit;
        ASPxSpinEdit textPoliticaInv = gridCadastro.FindEditFormTemplateControl("textPoliticaInv") as ASPxSpinEdit;
        ASPxSpinEdit textAportes = gridCadastro.FindEditFormTemplateControl("textAportes") as ASPxSpinEdit;
        ASPxSpinEdit textResgates = gridCadastro.FindEditFormTemplateControl("textResgates") as ASPxSpinEdit;
        ASPxSpinEdit textFornecimentoInfos = gridCadastro.FindEditFormTemplateControl("textFornecimentoInfos") as ASPxSpinEdit;
        ASPxSpinEdit textAssembleiasGerais = gridCadastro.FindEditFormTemplateControl("textAssembleiasGerais") as ASPxSpinEdit;
        ASPxSpinEdit textCotas = gridCadastro.FindEditFormTemplateControl("textCotas") as ASPxSpinEdit;
        ASPxSpinEdit textOutros = gridCadastro.FindEditFormTemplateControl("textOutros") as ASPxSpinEdit;

        DateTime data = Convert.ToDateTime(textData.Text);
        int procedentes = textProcedentes.Text == "" ? 0 : Convert.ToInt32(textProcedentes.Text);
        int improcedentes = textImprocedentes.Text == "" ? 0 : Convert.ToInt32(textImprocedentes.Text);
        int resolvidas = textResolvidas.Text == "" ? 0 : Convert.ToInt32(textResolvidas.Text);
        int naoResolvidas = textNaoResolvidas.Text == "" ? 0 : Convert.ToInt32(textNaoResolvidas.Text);
        int administracao = textAdministracao.Text == "" ? 0 : Convert.ToInt32(textAdministracao.Text);
        int gestao = textGestao.Text == "" ? 0 : Convert.ToInt32(textGestao.Text);
        int taxas = textTaxas.Text == "" ? 0 : Convert.ToInt32(textTaxas.Text);
        int politicaInv = textPoliticaInv.Text == "" ? 0 : Convert.ToInt32(textPoliticaInv.Text);
        int aportes = textAportes.Text == "" ? 0 : Convert.ToInt32(textAportes.Text);
        int resgates = textResgates.Text == "" ? 0 : Convert.ToInt32(textResgates.Text);
        int fornecimentoInfos = textFornecimentoInfos.Text == "" ? 0 : Convert.ToInt32(textFornecimentoInfos.Text);
        int assembleiasGerais = textAssembleiasGerais.Text == "" ? 0 : Convert.ToInt32(textAssembleiasGerais.Text);
        int cotas = textCotas.Text == "" ? 0 : Convert.ToInt32(textCotas.Text);
        int outros = textOutros.Text == "" ? 0 : Convert.ToInt32(textOutros.Text);

        tabelaReclamacoesCotistas.Data = data;
        tabelaReclamacoesCotistas.Procedentes = procedentes;
        tabelaReclamacoesCotistas.Improcedentes = improcedentes;
        tabelaReclamacoesCotistas.Resolvidas = resolvidas;
        tabelaReclamacoesCotistas.NaoResolvidas = naoResolvidas;
        tabelaReclamacoesCotistas.Administracao = administracao;
        tabelaReclamacoesCotistas.Gestao = gestao;
        tabelaReclamacoesCotistas.Taxas = taxas;
        tabelaReclamacoesCotistas.PoliticaInvestimento = politicaInv;
        tabelaReclamacoesCotistas.Aportes = aportes;
        tabelaReclamacoesCotistas.Resgates = resgates;
        tabelaReclamacoesCotistas.FornecimentoInformacoes = fornecimentoInfos;
        tabelaReclamacoesCotistas.AssembleiasGerais = assembleiasGerais;
        tabelaReclamacoesCotistas.Cotas = cotas;
        tabelaReclamacoesCotistas.Outros = outros;
        tabelaReclamacoesCotistas.Save();

        #region Log do Processo
        HistoricoLog historicoLog = new HistoricoLog();
        historicoLog.InsereHistoricoLog(DateTime.Now,
                                        DateTime.Now,
                                        "Cadastro de TabelaReclamacoesCotistas - Operacao: Insert TabelaReclamacoesCotistas: " + tabelaReclamacoesCotistas.Data + UtilitarioWeb.ToString(tabelaReclamacoesCotistas),
                                        HttpContext.Current.User.Identity.Name,
                                        UtilitarioWeb.GetIP(Request),
                                        "",
                                        HistoricoLogOrigem.Outros);
        #endregion

        e.Cancel = true;
        gridCadastro.CancelEdit();
        gridCadastro.DataBind();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_CustomCallback(object sender, DevExpress.Web.ASPxGridViewCustomCallbackEventArgs e) 
    {
        if (e.Parameters == "btnDelete") 
        {
            List<object> keyValuesData = gridCadastro.GetSelectedFieldValues(TabelaReclamacoesCotistasMetadata.ColumnNames.Data);
            for (int i = 0; i < keyValuesData.Count; i++) 
            {
                DateTime data = Convert.ToDateTime(keyValuesData[i]);

                TabelaReclamacoesCotistas tabelaReclamacoesCotistas = new TabelaReclamacoesCotistas();
                if (tabelaReclamacoesCotistas.LoadByPrimaryKey(data))
                {
                    TabelaReclamacoesCotistas tabelaReclamacoesCotistasClone = (TabelaReclamacoesCotistas)Utilitario.Clone(tabelaReclamacoesCotistas);
                    //                    
                    tabelaReclamacoesCotistas.MarkAsDeleted();
                    tabelaReclamacoesCotistas.Save();

                    #region Log do Processo
                    HistoricoLog historicoLog = new HistoricoLog();
                    historicoLog.InsereHistoricoLog(DateTime.Now,
                                                    DateTime.Now,
                                                    "Cadastro de TabelaReclamacoesCotistas - Operacao: Delete TabelaReclamacoesCotistas: " + data + UtilitarioWeb.ToString(tabelaReclamacoesCotistasClone),
                                                    HttpContext.Current.User.Identity.Name,
                                                    UtilitarioWeb.GetIP(Request),
                                                    "",
                                                    HistoricoLogOrigem.Outros);
                    #endregion
                }
            }
        }

        gridCadastro.DataBind();
        gridCadastro.Selection.UnselectAll();
        gridCadastro.CancelEdit();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    new protected void gridCadastro_CustomJSProperties(object sender, ASPxGridViewClientJSPropertiesEventArgs e) {
        base.gridCadastro_CustomJSProperties(sender, e);

        ASPxGridView gridView = sender as ASPxGridView;
        if (gridView.IsEditing) {
            ASPxDateEdit textData = gridCadastro.FindEditFormTemplateControl("textData") as ASPxDateEdit;
            e.Properties["cpTextData"] = textData.ClientID;
        }
    }
    
}