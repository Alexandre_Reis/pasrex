﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Importacao.aspx.cs" Inherits="CadastrosBasicos_Importacao" %>
<%@ Register Assembly="System.Web, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"
    Namespace="System.Web.UI.HtmlControls" TagPrefix="cc2" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxe" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxcb" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxuc" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxlp" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxrp" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxp" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxtc" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxw" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxwgv" %>
<%@ Register Assembly="EntitySpaces.Web" Namespace="EntitySpaces.Web" TagPrefix="cc1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <link href="~/css/forms.css" rel="stylesheet" type="text/css" />

    <script type="text/javascript" language="Javascript" src="../js/global.js"></script>

    <script type="text/javascript" language="Javascript" src="../js/ext-core.js"></script>

    <script type="text/javascript" language="javascript">
    
// Now here is one i use frequently, especially in javascript files: 
// highlight some code, then press Ctrl-M Ctrl-H to hide it, which will produce a little expander +- next to the collapsed code. 
// Press Ctrl-M Ctrl-O to expand it again.
    
        var msg = '';
        var cont = 0;
        var totalCont = 0;

        function EnableFileSelection(value)
        {
            if (value == 1)
                uplPosicaoCotista.SetClientVisible(true);
            else
                uplPosicaoCotista.SetClientVisible(false);
        }
        
        function Uploader_OnUploadComplete(args) {                
        
            //Reset checkIgnora
            checkIgnora.SetChecked(false);
            
            cont += 1;
            if (args.callbackData != '')
            { msg = msg + args.callbackData; }
            
            if (cont == totalCont)
            {
                LoadingPanel.Hide();
                
                if (msg == '')
                {
                    alert('Arquivo(s) importado(s) com sucesso.');
                }
                else
                {               
                    // Se Ocorreu erro em alguma importação Concatena mensagem Concluida
                    msg += '\n -------------------------------------------------------------------------';
                    msg += '\n\t\t\t\tImportação concluída';
                    alert(msg);
                }    
            }
        }
                
        function ChecagemContUpload(tabPanelName) {
            var divTabArquivos = Ext.get(tabPanelName);
            var fileInputs = divTabArquivos.query('input[type=file]');
            
            totalCont = 0;
            for(var count=0; count < fileInputs.length; count++) {
                if (fileInputs[count].value.length > 0) {
                   totalCont += 1;
                }
            }
            if (dropDefineFonteImportacao.GetValue() == 2) totalCont += 1;

        }
                       
        function LimparUploads() {
            uplCDIX.ClearText();
            uplCMDF.ClearText();
            uplCONR.ClearText();
            uplCONL.ClearText();
            uplDBTC.ClearText();
            uplDBTL.ClearText();
            uplPAPT.ClearText();
            
            uplPROD.ClearText();
            uplNEGS.ClearText();
            uplPESC.ClearText();
            uplNotaCorretagem.ClearText();
            uplNotaCorretagemPDF.ClearText();
            uplNotaCorretagemPDF_BMF.ClearText();
            uplRComiesp.ClearText();
            uplCRCA.ClearText();
            uplPOSR.ClearText();
            uplCSGD.ClearText();
            uplDBTCPosicao.ClearText();
        }
        
        function LimparUploadsExcel()
        {
            uplOrdemBolsa.ClearText();
            uplOperacaoEmprestimoBolsa.ClearText();
            uplOrdemBMF.ClearText();                        
            uplOperacaoFundo.ClearText();
            uplOperacaoRendaFixa.ClearText();
            uplCotacaoBolsa.ClearText();
            uplCotacaoBMF.ClearText();
            uplCotacaoIndice.ClearText();
            uplCotacaoSerie.ClearText();
            uplCotacaoAnbimaPublico.ClearText();
            uplHistoricoCota.ClearText();
            uplAtivoBolsa.ClearText();  
            uplOperacaoCotista.ClearText();                                 
            uplContabLancamento.ClearText();
            uplTaxaCurvaRendaFixa.ClearText();
            uplColagemResgates.ClearText();
            uplColagemComeCotas.ClearText();
            uplTransferenciaBMF.ClearText();
            uplTransferenciaBolsa.ClearText();
            uplCravaCota.ClearText();
            uplMTMManual.ClearText();
            uplLiquidacaoDiaria.ClearText();
        }
        
        // Retorna boolean Indicando se há algum Arquivo Diario com Valor
        function ChecaArquivosDiarios() {
            if(uplCDIX.GetText()!= '' || uplCMDF.GetText()!= '' ||
               uplCONR.GetText()!= '' || uplCONL.GetText()!= '' ||
               uplDBTC.GetText()!= '' || uplDBTL.GetText()!= '' ||
               uplPAPT.GetText()!= '' || uplPROD.GetText()!= '' ||
               uplNEGS.GetText()!= '' || uplPESC.GetText()!= '' ||
               uplNotaCorretagem.GetText()!= '' || uplNotaCorretagemPDF.GetText()!= '' || 
               uplNotaCorretagemPDF_BMF.GetText()!= '' || 
               uplRComiesp.GetText()!= '' || uplCRCA.GetText()!= '' || 
               uplPOSR.GetText()!= '' || uplCSGD.GetText()!= '' ||
               uplDBTCPosicao.GetText()!= '') {
               
               return true;
            }
            return false;
        }
        
        function ChecaExcel() {
            if(uplOrdemBolsa.GetText()!= '' || uplOperacaoEmprestimoBolsa.GetText()!= '' ||
               uplOrdemBMF.GetText()!= '' || uplOperacaoFundo.GetText()!= '' || 
               uplOperacaoRendaFixa.GetText()!= '' || uplCotacaoBolsa.GetText()!= '' || uplCotacaoBMF.GetText()!= '' ||                              
               uplCotacaoIndice.GetText()!= '' || uplCotacaoSerie.GetText()!= '' || uplCotacaoAnbimaPublico.GetText()!= '' || uplAtivoBolsa.GetText()!= '' ||
               uplHistoricoCota.GetText()!= '' || uplOperacaoCotista.GetText()!= '' ||
               uplContabLancamento.GetText() != '' || uplTaxaCurvaRendaFixa.GetText() != '' || uplColagemResgates.GetText() != '' || uplColagemComeCotas.GetText() != '' ||
               uplTransferenciaBMF.GetText() != '' || uplTransferenciaBolsa.GetText() != '' || uplCravaCota.GetText() != '' || uplMTMManual.GetText() != '' ||
               uplLiquidacaoDiaria.GetText()!= '') {               
               
                //Verificar se existe arquivos cujos erros de importação devam ser ignorados
                if(uplOperacaoFundo.GetText()!= '' || uplOperacaoCotista.GetText()!= '' || 
                   uplOperacaoRendaFixa.GetText()!= '' || uplOrdemBolsa.GetText()!= '' || 
                   uplOrdemBMF.GetText()!= '' || uplOperacaoEmprestimoBolsa.GetText()!= ''){
                    if(confirm("Deseja ignorar eventuais erros na importação, importando apenas os dados válidos?")){
                        checkIgnora.SetChecked(true);
                    } else {
                        //Reset checkIgnora
                        checkIgnora.SetChecked(false);
                    }
                }
               
                return true;    
            }
            
            return false;
        }
                              
        function ChecaExcelImplantacao() {
           if(uplPosicaoBolsa.GetText()!= '' || uplPosicaoBMF.GetText()!= '' || 
               uplPosicaoFundo.GetText()!= '' || uplPosicaoRendaFixa.GetText()!= '' ||
               uplPosicaoCotistaHistorico.GetText()!= '' ||
               uplPosicaoCotista.GetText()!= '' || uplPosicaoEmprestimoBolsa.GetText()!= '' ||
               uplPosicaoSwap.GetText()!= '' || uplPosicaoTermo.GetText()!= '' ||               
               uplCalculoAdministracao.GetText()!= '' || uplCalculoProvisao.GetText()!= '' ||
               uplCalculoPerformance.GetText()!= '' || uplPrejuizoCotista.GetText()!= '' ||
               uplCadastroPessoa.GetText()!= '' || uplCadastroClienteCarteira.GetText()!= '' || uplContabConta.GetText()!= '' || 
               uplContabRoteiro.GetText()!= '' || uplEmissor.GetText()!= '' ||
               uplLiquidacao.GetText()!= '' || uplApuracaoIR.GetText()!= '' ||
               uplTituloRendaFixa.GetText()!= '' || uplAgendaEventos.GetText()!= '' || uplAgendaEventosFundo.GetText()!= '' ||
               uplCadastroCotistaComIdPessoa.GetText()!= '' || uplContaCorrente.GetText()!= '' || dropDefineFonteImportacao.GetValue() == '2') {
               
                return true;    
            }
            
            return false;
        }
        
        function ChecaExcelImplantacaoOutros(){
            if( uplYMF.GetText()!= '' || uplEICL.GetText()!= '' ||
                uplPosicaoSMAFundo.GetText()!= '' || uplMovimentoSMAFundo.GetText() != '' ||
                uplPosicaoXMLAnbid.GetText() != '' || uplIRTema.GetText() != '' || uplYMFFIX.GetText()!= '' || uplXMLClienteMellon.GetText()!='' || 
                uplInfoComplementarFundo.GetText()!='' || uplTabelaPerfilMensal.GetText()!='' ||
                uplInformeDesempenho.GetText()!='') 
            {                                                                             
                return true;
            }
            else 
            {
                return false;
            }
        }
        
        function ChecaGalgo(){
            if( uplPlCotaGalgo.GetText()!='' ) 
            {                                                                             
                return true;
            }
            else 
            {
                return false;
            }
        }
        
        function LimparUploadsExcelImplantacao() {
            uplPosicaoBolsa.ClearText();
            uplPosicaoBMF.ClearText();
            uplPosicaoFundo.ClearText();
            uplPosicaoRendaFixa.ClearText();
            //
            uplPosicaoCotista.ClearText();
            uplPosicaoCotistaHistorico.ClearText();
            uplPosicaoEmprestimoBolsa.ClearText();
            uplPosicaoSwap.ClearText();
            uplPosicaoTermo.ClearText();
            //
            uplCalculoAdministracao.ClearText();
            uplCalculoProvisao.ClearText();
            uplCalculoPerformance.ClearText();
            uplPrejuizoCotista.ClearText();
            uplLiquidacao.ClearText();
            uplEmissor.ClearText();
            uplTituloRendaFixa.ClearText();
            uplApuracaoIR.ClearText();
            uplAgendaEventos.ClearText();            
            uplAgendaEventosFundo.ClearText(); 
            uplCadastroPessoa.ClearText();
            uplCadastroClienteCarteira.ClearText();
            uplContabConta.ClearText();
            uplContabRoteiro.ClearText();
            uplContaCorrente.ClearText();
            uplCadastroCotistaComIdPessoa.ClearText();
        }
        
        function LimparUploadsExcelImplantacaoOutros() {
            uplYMF.ClearText();
            uplEICL.ClearText();
            uplPosicaoSMAFundo.ClearText();
            uplMovimentoSMAFundo.ClearText();
            uplPosicaoXMLAnbid.ClearText();
            uplIRTema.ClearText();
            uplYMFFIX.ClearText();
            uplXMLClienteMellon.ClearText();
            uplInfoComplementarFundo.ClearText();
            uplTabelaPerfilMensal.ClearText();
            uplInformeDesempenho.ClearText();
        }
     
        function onFileUploadStart(s, e) {
            totalCont = 0;
            msg = '';

        }
        function onFilesUploadComplete(s, e) {
            
            if (msg == '') {
                alert('Arquivo(s) importado(s) com sucesso.');
            }
            else {
                msg += '\n -------------------------------------------------------------------------';
                msg += '\n\t\t\t\tImportação concluída';
                alert(msg);
            }
        }
        
        function onFileUploadComplete(s, e) {
            totalCont = totalCont + 1;
            
            if (e.callbackData != '')
            { msg = msg + e.callbackData; }

        }
    </script>

    <style type="text/css">
        .style1 { width: 191px; }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <dxcb:ASPxHiddenField runat="server" ID="HiddenField" ClientInstanceName="HiddenField"></dxcb:ASPxHiddenField>
        <asp:ScriptManager ID="ScriptManager1" runat="server" AsyncPostBackTimeout="360000"
            EnableScriptGlobalization="true" EnableScriptLocalization="true" />
        <dxcb:ASPxCallback ID="callbackArquivos" runat="server" OnCallback="callbackArquivos_Callback">
            <ClientSideEvents CallbackComplete="function(s, e) 
        {   
            if (e.result != '')
            {                   
                alert(e.result);                              
            }    
            else
            {
                ChecagemContUpload('innerPanel_TabArquivos');
                                   
                if (totalCont == 0)
                {
                    return;
                }
                
                msg = '';
                cont = 0;
                                
                LoadingPanel.Show();
                                
                uplCDIX.UploadFile();
                uplCMDF.UploadFile();
                uplCONR.UploadFile();
                uplCONL.UploadFile();
                uplDBTC.UploadFile();
                uplDBTL.UploadFile();
                uplPAPT.UploadFile();
                
                uplPROD.UploadFile();
                uplNEGS.UploadFile();
                uplPESC.UploadFile();
                uplNotaCorretagem.UploadFile();
                uplNotaCorretagemPDF.UploadFile();
                uplNotaCorretagemPDF_BMF.UploadFile();
                uplRComiesp.UploadFile();
                uplCRCA.UploadFile();
                uplPOSR.UploadFile();
                uplCSGD.UploadFile();
                uplDBTCPosicao.UploadFile();
            }
        }        
        " />
        </dxcb:ASPxCallback>
        <dxcb:ASPxCallback ID="callbackExcel" runat="server" OnCallback="callbackExcel_Callback">
            <ClientSideEvents CallbackComplete="function(s, e) 
        {   
            if (e.result != '')
            {                   
                alert(e.result);                              
            }    
            else
            {
                ChecagemContUpload('innerPanel_TabImportacaoExcel'); 
                                   
                if (totalCont == 0)
                {
                    return;
                }
                
                msg = '';
                cont = 0;
                                
                LoadingPanel.Show();
                                
                uplOrdemBolsa.UploadFile();
                uplOperacaoEmprestimoBolsa.UploadFile();                
                uplOrdemBMF.UploadFile();
                uplOperacaoFundo.UploadFile();
                uplOperacaoRendaFixa.UploadFile();                
                uplCotacaoBolsa.UploadFile();
                uplAtivoBolsa.UploadFile();
                uplCotacaoBMF.UploadFile();
                uplCotacaoIndice.UploadFile();
                uplCotacaoSerie.UploadFile();
                uplCotacaoAnbimaPublico.UploadFile();
                uplHistoricoCota.UploadFile();                
                uplOperacaoCotista.UploadFile();
                uplContabLancamento.UploadFile();
                uplTaxaCurvaRendaFixa.UploadFile();
                uplColagemResgates.UploadFile(); 
                uplColagemComeCotas.UploadFile();     
                uplTransferenciaBMF.UploadFile();
                uplTransferenciaBolsa.UploadFile();
                uplCravaCota.UploadFile();       
                uplMTMManual.UploadFile();       
                uplLiquidacaoDiaria.UploadFile();     
            }
        }        
        " />
        </dxcb:ASPxCallback>
        <dxcb:ASPxCallback ID="callbackErroInternet" runat="server" OnCallback="callbackErroInternet_Callback">
            <ClientSideEvents CallbackComplete="function(s, e) 
        {             
            if (e.result != '')
            {                   
                alert(e.result);                              
            }    
            else
            {
                LoadingPanel.Show();
                callbackImportacaoInternet.SendCallback();                
            }
        }        
        " />
        </dxcb:ASPxCallback>
        <dxcb:ASPxCallback ID="callbackImportacaoInternet" runat="server" OnCallback="callbackImportacaoInternet_Callback">
            <ClientSideEvents CallbackComplete="function(s, e) 
        {             
            if (e.result != '')
            {   
                LoadingPanel.Hide();                                              
                alert(e.result);
            }            
        }        
        " />
        </dxcb:ASPxCallback>
        
        <dxcb:ASPxCallback ID="callbackErroGalgoPlCotaWcf" runat="server" OnCallback="callbackErroGalgoPlCotaWcf_Callback">
            <ClientSideEvents CallbackComplete="function(s, e) 
        {             
            if (e.result != '')
            {                   
                alert(e.result);                              
            }    
            else
            {
                LoadingPanel.Show();
                callbackImportacaoGalgoPlCotaWcf.SendCallback();                
            }
        }        
        " />
        </dxcb:ASPxCallback>
        
        <dxcb:ASPxCallback ID="callbackImportacaoGalgoPlCotaWcf" runat="server" OnCallback="callbackImportacaoGalgoPlCotaWcf_Callback">
            <ClientSideEvents CallbackComplete="function(s, e) 
        {             
            if (e.result != '')
            {   
                LoadingPanel.Hide();                                              
                alert(e.result);
            }            
        }        
        " />
        </dxcb:ASPxCallback>
        
        <dxcb:ASPxCallback ID="callbackErroGalgoPlCotaCorrecaoWcf" runat="server" OnCallback="callbackErroGalgoPlCotaCorrecaoWcf_Callback">
            <ClientSideEvents CallbackComplete="function(s, e) 
        {             
            if (e.result != '')
            {                   
                alert(e.result);                              
            }    
            else
            {
                LoadingPanel.Show();
                callbackImportacaoGalgoPlCotaCorrecaoWcf.SendCallback();                
            }
        }        
        " />
        </dxcb:ASPxCallback>
        
        <dxcb:ASPxCallback ID="callbackImportacaoGalgoPlCotaCorrecaoWcf" runat="server" OnCallback="callbackImportacaoGalgoPlCotaCorrecaoWcf_Callback">
            <ClientSideEvents CallbackComplete="function(s, e) 
        {             
            if (e.result != '')
            {   
                LoadingPanel.Hide();                                              
                alert(e.result);
            }            
        }        
        " />
        </dxcb:ASPxCallback>
        
        <dxcb:ASPxCallback ID="callbackErroGalgoPlCotaPendentesWcf" runat="server" OnCallback="callbackErroGalgoPlCotaPendentesWcf_Callback">
            <ClientSideEvents CallbackComplete="function(s, e) 
        {             
            if (e.result != '')
            {                   
                alert(e.result);                              
            }    
            else
            {
            
                LoadingPanel.Show();
                callbackImportacaoGalgoPlCotaPendentesWcf.SendCallback();                
            }
        }        
        " />
        </dxcb:ASPxCallback>
        
        <dxcb:ASPxCallback ID="callbackImportacaoGalgoPlCotaPendentesWcf" runat="server" OnCallback="callbackImportacaoGalgoPlCotaPendentesWcf_Callback">
            <ClientSideEvents CallbackComplete="function(s, e) 
        {             
            if (e.result != '')
            {   
                LoadingPanel.Hide();                                              
                alert(e.result);
            }            
        }        
        " />
        </dxcb:ASPxCallback>
        
        <dxcb:ASPxCallback ID="callbackExcelImplantacao" runat="server" OnCallback="callbackExcelImplantacao_Callback">
            <ClientSideEvents CallbackComplete="function(s, e) 
        {   
            if (e.result != '') {                   
                alert(e.result);                              
            }    
            else {
                ChecagemContUpload('innerPanel_TabImplantacao');
                                   
                if (totalCont == 0) {
                    return;
                }
                
                msg = '';
                cont = 0;
                                
                LoadingPanel.Show();
                                
                uplPosicaoBolsa.UploadFile();
                uplPosicaoBMF.UploadFile();
                
                uplPosicaoRendaFixa.UploadFile();
                //
                
                uplPosicaoCotistaHistorico.UploadFile();
                uplPosicaoEmprestimoBolsa.UploadFile();
                uplPosicaoSwap.UploadFile();
                uplPosicaoTermo.UploadFile();
                //
                uplCalculoAdministracao.UploadFile();
                uplCalculoProvisao.UploadFile();
                uplCalculoPerformance.UploadFile();
                uplPrejuizoCotista.UploadFile();
                uplLiquidacao.UploadFile();
                uplTituloRendaFixa.UploadFile();
                uplEmissor.UploadFile();
                uplApuracaoIR.UploadFile();
                uplAgendaEventos.UploadFile();
                uplAgendaEventosFundo.UploadFile();
                uplCadastroPessoa.UploadFile();
                uplCadastroClienteCarteira.UploadFile();
                uplContabConta.UploadFile();
                uplContabRoteiro.UploadFile();
                uplContaCorrente.UploadFile();
                uplCadastroCotistaComIdPessoa.UploadFile();

                uplPosicaoFundo.UploadFile();
                if(dropDefineFonteImportacao.GetValue() == '1')
                {
                    uplPosicaoCotista.UploadFile();
                }
                else
                {
                    callbackImportaTabelaPosicao.SendCallback();
                }
            }
        }        
        " />
        </dxcb:ASPxCallback>

        <dxcb:ASPxCallback ID="callbackImportaTabelaPosicao" runat="server" OnCallback="callbackImportaTabelaPosicao_Callback">
            <ClientSideEvents CallbackComplete="function(s, e) 
        {   
            LoadingPanel.Hide();

            if (e.result != '') {                   
                alert(e.result);                              
            } 
            else
            {
                //Reset checkIgnora
                checkIgnora.SetChecked(false);
                
                alert('Arquivo(s) importado(s) com sucesso.');
            }    
             

        }        
        " />
        </dxcb:ASPxCallback>

        <dxcb:ASPxCallback ID="callbackGalgoPlCotaArquivo" runat="server" OnCallback="callbackGalgoPlCotaArquivo_Callback">
            <ClientSideEvents CallbackComplete="function(s, e) 
        {   
            if (e.result != '') {                   
                alert(e.result);                              
            }    
            else {
                
                ChecagemContUpload('innerPanel_TabGalgo');
                                   
                if (totalCont == 0)
                {
                    return;
                }
                
                msg = '';
                cont = 0;
                                
                LoadingPanel.Show();

                uplPlCotaGalgo.UploadFile();
            }
        }        
        " />
        </dxcb:ASPxCallback>

        <dxcb:ASPxCallback ID="callbackExcelImplantacaoOutros" runat="server" OnCallback="callbackExcelImplantacaoOutros_Callback">
            <ClientSideEvents CallbackComplete="function(s, e) 
        {   
            if (e.result != '') {                   
                alert(e.result);                              
            }    
            else {
                ChecagemContUpload('innerPanel_TabImplantacaoOutros');
                                   
                if (totalCont == 0) {
                    return;
                }
                
                if(uplYMFFIX.GetText()!= '' && !(document.location.hostname == 'localhost' || document.location.hostname == '127.0.0.1')){
                    alert('Essa rotina pode destruir dados, só podendo ser executado no localhost');
                    return false;
                }
                
                msg = '';
                cont = 0;
                                
                LoadingPanel.Show();

                uplYMF.UploadFile();
                uplEICL.UploadFile();
                uplPosicaoSMAFundo.UploadFile();
                uplMovimentoSMAFundo.UploadFile();
                uplPosicaoXMLAnbid.UploadFile();
                uplIRTema.UploadFile();
                uplYMFFIX.UploadFile();
                uplXMLClienteMellon.UploadFile();
                uplInfoComplementarFundo.UploadFile();
                uplTabelaPerfilMensal.UploadFile();
                uplInformeDesempenho.UploadFile();
            }
        }        
        " />
        </dxcb:ASPxCallback>

        <dxcb:ASPxCallback ID="callbackMultiSelectArquivos" runat="server" OnCallback="callbackMultiSelectArquivos_Callback">
            <ClientSideEvents CallbackComplete="function(s, e) 
        {   
            if (e.result != '')
            {   
                totalCont = 0;
                alert(e.result);                              
            }    
            else
            {
                DocumentsUploadControl.UploadFile();
            }
        }        
        " />
        </dxcb:ASPxCallback>

        <div class="divPanel divPanelNew">
            <div id="container_small">
                <div id="header">
                    <asp:Label ID="lblHeader" runat="server" Text="Importação de Arquivos"></asp:Label>
                </div>
                <div id="mainContent">
                    <div class="reportFilter">
                        <div class="dataMessage">
                        </div>
                        <dxrp:ASPxRoundPanel ID="ASPxRoundPanel1" runat="server" ShowHeader="False" BackColor="White"
                            Border-BorderColor="#AECAF0">
                            <Border BorderColor="#AECAF0"></Border>
                            <PanelCollection>
                                <dxp:PanelContent runat="server">
                                    <dxtc:ASPxPageControl ID="tabArquivos" runat="server" ClientInstanceName="tabArquivos"
                                        Height="100%" Width="100%" ActiveTabIndex="0" TabSpacing="0px">
                                        <TabPages>
                                            <dxtc:TabPage Text="Público/Internet">
                                                <ContentCollection>
                                                    <dxw:ContentControl runat="server">
                                                        <div class="innerPanel" style="font-weight: bold">
                                                            <table border="0">
                                                                <tr>
                                                                    <td>
                                                                        <asp:Label ID="labelData" runat="server" CssClass="labelNormal labelFloat" Text="Ínicio:"></asp:Label>
                                                                    </td>
                                                                    <td>
                                                                        <dxe:ASPxDateEdit ID="textDataInicioInternet" runat="server" ClientInstanceName="textDataInicioInternet" />
                                                                    </td>
                                                                    <td>
                                                                        <asp:Label ID="label50" runat="server" CssClass="labelNormal labelFloat" Text="Fim:" />
                                                                    </td>
                                                                    <td>
                                                                        <dxe:ASPxDateEdit ID="textDataFimInternet" runat="server" ClientInstanceName="textDataFimInternet" />
                                                                    </td>
                                                                    <td>
                                                                        <div class="linkButton linkButtonNoBorder" style="margin-left: 40px; margin-top: 20px">
                                                                            <asp:LinkButton ID="btnRunInternet" runat="server" Font-Overline="false" CssClass="btnRun"
                                                                                OnClientClick="
                                                                    {                                                        
                                                                        callbackErroInternet.SendCallback(); return false;
                                                                    }                                                    
                                                                    ">
                                                                                <asp:Literal ID="Literal1" runat="server" Text="Importar" /><div>
                                                                                </div>
                                                                            </asp:LinkButton>
                                                                        </div>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                            <div class="clear">
                                                            </div>
                                                            <div class="formColumn formColumnFirst">
                                                                <asp:CheckBox ID="chkBDIN" runat="server" Text="BDIN" /><br />
                                                                <asp:CheckBox ID="chkBDPregao" runat="server" Text="BDPregao" /><br />
                                                                <asp:CheckBox ID="chkIndic" runat="server" Text="Indic (Cotação Índice)" /><br />
                                                                <asp:CheckBox ID="chkTarPar" runat="server" Text="TarPar (Parâmetro BMF, Cotação Índice)" /><br />
                                                                <asp:CheckBox ID="chkAndimaIndice" runat="server" Text="Anbima Indicadores" /><br />
                                                                <asp:CheckBox ID="chkAndimaMercado" runat="server" Text="Anbima Títulos Públicos" /><br />
                                                                <asp:CheckBox ID="chkIma" runat="server" Text="IMA" Enabled="true" /><br />
                                                                <asp:CheckBox ID="chkTesouro" runat="server" Text="Cotação Tesouro" /><br />
                                                                <asp:CheckBox ID="chkIBGE" runat="server" Text="Cotação IBGE (IPCA/INPC)" /><br />
                                                                <asp:CheckBox ID="chkDebenture" runat="server" Text="Anbima Debêntures" /><br />
                                                            </div>
                                                            <div class="formColumn">
                                                                <asp:CheckBox ID="chkOffshore" runat="server" Text="Cotações Offshore" /><br />
                                                                <asp:CheckBox ID="chkAndima238" runat="server" Text="Andima 238" /><br />
                                                                <asp:CheckBox ID="chkAndima550" runat="server" Text="Andima 550" /><br />
                                                                <asp:CheckBox ID="chkTarPreg" runat="server" Text="TarPreg" /><br />
                                                                <asp:CheckBox ID="chkRefVol" runat="server" Text="RefVol (Volatilidade Estratégica)" /><br />
                                                                <asp:CheckBox ID="chkTaxaSwap" runat="server" Text="TaxaSwap" /><br />
                                                                <asp:CheckBox ID="chkListaAtivoCVM" runat="server" Text="Lista Ativo CVM" /><br />
                                                                <asp:CheckBox ID="chkEmissoresIsin" runat="server" Text="Emissores/Isin" /><br />
                                                                <asp:CheckBox ID="chkASEL007" runat="server" Text="ASEL007" /><br />
                                                                <asp:CheckBox ID="chkPlCotaGalgoWcf" runat="server" Text="PL/Cota - Galgo (WCF)" /><br />

                                                            </div>
                                                        </div>
                                                    </dxw:ContentControl>
                                                </ContentCollection>
                                            </dxtc:TabPage>
                                        </TabPages>
                                        <TabPages>
                                            <dxtc:TabPage Text="Arquivos - Bolsa/BMF">
                                                <ContentCollection>
                                                    <dxw:ContentControl runat="server">
                                                        <div class="innerPanel" id="innerPanel_TabArquivos">
                                                            <table>
                                                                <tr>
                                                                    <td>
                                                                        <asp:Label ID="label1" runat="server" CssClass="labelNormal labelFloat" Text="Data:"></asp:Label>
                                                                    </td>
                                                                    <td>
                                                                        <dxe:ASPxDateEdit ID="textDataArquivos" runat="server" ClientInstanceName="textDataArquivos" />
                                                                    </td>
                                                                    <td>
                                                                        <div class="linkButton linkButtonNoBorder" style="margin-left: 40px; margin-top: 20px">
                                                                            <asp:LinkButton ID="btnRunArquivos" runat="server" Font-Overline="false" CssClass="btnRun"
                                                                                OnClientClick="
                                                                    {
                                                                        if(!ChecaArquivosDiarios()) {
                                                                            alert('Escolha algum arquivo para importação!');                                                                            
                                                                        }
                                                                        else {                                                                    
                                                                            callbackArquivos.SendCallback();                                                                             
                                                                        }
                                                                        return false;
                                                                    }
                                                                    ">
                                                                                <asp:Literal ID="Literal2" runat="server" Text="Importar" /><div>
                                                                                </div>
                                                                            </asp:LinkButton>
                                                                            <asp:LinkButton ID="btnLimpar" runat="server" Font-Overline="false" CssClass="btnClear"
                                                                                OnClientClick="
                                                                    {
                                                                        LimparUploads();
                                                                        return false;
                                                                    }
                                                                    ">
                                                                                <asp:Literal ID="Literal3" runat="server" Text="Limpar" /><div>
                                                                                </div>
                                                                            </asp:LinkButton>
                                                                        </div>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                            <div class="clear">
                                                            </div>
                                                            <div class="formColumn formColumnFirst">
                                                                <asp:Label ID="label2" runat="server" CssClass="labelNormal labelFloat" Text="CDIX"></asp:Label>
                                                                <dxuc:ASPxUploadControl ID="uplCDIX" runat="server" ClientInstanceName="uplCDIX"
                                                                    BrowseButtonStyle-CssClass="height:100px" OnFileUploadComplete="uplImportacaoDiarios_FileUploadComplete"
                                                                    CssClass="labelNormal" FileUploadMode="OnPageLoad">
                                                                    <ValidationSettings MaxFileSize="100000000" />
                                                                    <ClientSideEvents FileUploadComplete="function(s, e) { Uploader_OnUploadComplete(e); }">
                                                                    </ClientSideEvents>
                                                                </dxuc:ASPxUploadControl>
                                                                <asp:Label ID="label4" runat="server" CssClass="labelNormal labelFloat" Text="CMDF"></asp:Label>
                                                                <dxuc:ASPxUploadControl ID="uplCMDF" runat="server" ClientInstanceName="uplCMDF"
                                                                    OnFileUploadComplete="uplImportacaoDiarios_FileUploadComplete" CssClass="labelNormal"
                                                                    FileUploadMode="OnPageLoad">
                                                                    <ValidationSettings MaxFileSize="100000000" />
                                                                    <ClientSideEvents FileUploadComplete="function(s, e) { Uploader_OnUploadComplete(e); }">
                                                                    </ClientSideEvents>
                                                                </dxuc:ASPxUploadControl>
                                                                <asp:Label ID="label5" runat="server" CssClass="labelNormal labelFloat" Text="CONR"></asp:Label>
                                                                <dxuc:ASPxUploadControl ID="uplCONR" runat="server" ClientInstanceName="uplCONR"
                                                                    OnFileUploadComplete="uplImportacaoDiarios_FileUploadComplete" CssClass="labelNormal"
                                                                    FileUploadMode="OnPageLoad">
                                                                    <ValidationSettings MaxFileSize="100000000" />
                                                                    <ClientSideEvents FileUploadComplete="function(s, e) { Uploader_OnUploadComplete(e); }">
                                                                    </ClientSideEvents>
                                                                </dxuc:ASPxUploadControl>
                                                                <asp:Label ID="label6" runat="server" CssClass="labelNormal labelFloat" Text="CONL"></asp:Label>
                                                                <dxuc:ASPxUploadControl ID="uplCONL" runat="server" ClientInstanceName="uplCONL"
                                                                    OnFileUploadComplete="uplImportacaoDiarios_FileUploadComplete" CssClass="labelNormal"
                                                                    FileUploadMode="OnPageLoad">
                                                                    <ValidationSettings MaxFileSize="100000000" />
                                                                    <ClientSideEvents FileUploadComplete="function(s, e) { Uploader_OnUploadComplete(e); }">
                                                                    </ClientSideEvents>
                                                                </dxuc:ASPxUploadControl>
                                                                <asp:Label ID="label7" runat="server" CssClass="labelNormal labelFloat" Text="DBTC (Operação)"></asp:Label>
                                                                <dxuc:ASPxUploadControl ID="uplDBTC" runat="server" ClientInstanceName="uplDBTC"
                                                                    OnFileUploadComplete="uplImportacaoDiarios_FileUploadComplete" CssClass="labelNormal"
                                                                    FileUploadMode="OnPageLoad">
                                                                    <ValidationSettings MaxFileSize="100000000" />
                                                                    <ClientSideEvents FileUploadComplete="function(s, e) { Uploader_OnUploadComplete(e); }">
                                                                    </ClientSideEvents>
                                                                </dxuc:ASPxUploadControl>
                                                                <asp:Label ID="label8" runat="server" CssClass="labelNormal labelFloat" Text="DBTL"></asp:Label>
                                                                <dxuc:ASPxUploadControl ID="uplDBTL" runat="server" ClientInstanceName="uplDBTL"
                                                                    OnFileUploadComplete="uplImportacaoDiarios_FileUploadComplete" CssClass="labelNormal"
                                                                    FileUploadMode="OnPageLoad">
                                                                    <ValidationSettings MaxFileSize="100000000" />
                                                                    <ClientSideEvents FileUploadComplete="function(s, e) { Uploader_OnUploadComplete(e); }">
                                                                    </ClientSideEvents>
                                                                </dxuc:ASPxUploadControl>
                                                                <asp:Label ID="label3" runat="server" CssClass="labelNormal labelFloat" Text="PAPT"></asp:Label>
                                                                <dxuc:ASPxUploadControl ID="uplPAPT" runat="server" ClientInstanceName="uplPAPT"
                                                                    OnFileUploadComplete="uplImportacaoDiarios_FileUploadComplete" CssClass="labelNormal"
                                                                    FileUploadMode="OnPageLoad">
                                                                    <ValidationSettings MaxFileSize="100000000" />
                                                                    <ClientSideEvents FileUploadComplete="function(s, e) { Uploader_OnUploadComplete(e); }">
                                                                    </ClientSideEvents>
                                                                </dxuc:ASPxUploadControl>
                                                                <asp:Label ID="label14" runat="server" CssClass="labelNormal labelFloat" Text="CSGD"></asp:Label>
                                                                <dxuc:ASPxUploadControl ID="uplCSGD" runat="server" ClientInstanceName="uplCSGD"
                                                                    OnFileUploadComplete="uplImportacaoDiarios_FileUploadComplete" CssClass="labelNormal"
                                                                    FileUploadMode="OnPageLoad">
                                                                    <ValidationSettings MaxFileSize="100000000" />
                                                                    <ClientSideEvents FileUploadComplete="function(s, e) { Uploader_OnUploadComplete(e); }">
                                                                    </ClientSideEvents>
                                                                </dxuc:ASPxUploadControl>
                                                                <asp:Label ID="label57" runat="server" CssClass="labelNormal labelFloat" Text="DBTC (Posição)"></asp:Label>
                                                                <dxuc:ASPxUploadControl ID="uplDBTCPosicao" runat="server" ClientInstanceName="uplDBTCPosicao"
                                                                    OnFileUploadComplete="uplImportacaoDiarios_FileUploadComplete" CssClass="labelNormal"
                                                                    FileUploadMode="OnPageLoad">
                                                                    <ValidationSettings MaxFileSize="100000000" />
                                                                    <ClientSideEvents FileUploadComplete="function(s, e) { Uploader_OnUploadComplete(e); }">
                                                                    </ClientSideEvents>
                                                                </dxuc:ASPxUploadControl>
                                                            </div>
                                                            <div class="formColumn">
                                                                <asp:Label ID="label9" runat="server" CssClass="labelNormal labelFloat" Text="PROD"></asp:Label>
                                                                <asp:CheckBox ID="checkIgnoraPROD" runat="server" Text="Ignora erros" />
                                                                <dxuc:ASPxUploadControl ID="uplPROD" runat="server" ClientInstanceName="uplPROD"
                                                                    OnFileUploadComplete="uplImportacaoDiarios_FileUploadComplete" CssClass="labelNormal"
                                                                    FileUploadMode="OnPageLoad">
                                                                    <ValidationSettings MaxFileSize="100000000" />
                                                                    <ClientSideEvents FileUploadComplete="function(s, e) { Uploader_OnUploadComplete(e); }">
                                                                    </ClientSideEvents>
                                                                </dxuc:ASPxUploadControl>
                                                                <asp:Label ID="label10" runat="server" CssClass="labelNormal labelFloat" Text="NEGS"></asp:Label>
                                                                <dxuc:ASPxUploadControl ID="uplNEGS" runat="server" ClientInstanceName="uplNEGS"
                                                                    OnFileUploadComplete="uplImportacaoDiarios_FileUploadComplete" CssClass="labelNormal"
                                                                    FileUploadMode="OnPageLoad">
                                                                    <ValidationSettings MaxFileSize="100000000" />
                                                                    <ClientSideEvents FileUploadComplete="function(s, e) { Uploader_OnUploadComplete(e); }">
                                                                    </ClientSideEvents>
                                                                </dxuc:ASPxUploadControl>
                                                                <asp:Label ID="label40" runat="server" CssClass="labelNormal labelFloat" Text="PESC"></asp:Label>
                                                                <dxuc:ASPxUploadControl ID="uplPESC" runat="server" ClientInstanceName="uplPESC"
                                                                    OnFileUploadComplete="uplImportacaoDiarios_FileUploadComplete" CssClass="labelNormal"
                                                                    FileUploadMode="OnPageLoad">
                                                                    <ValidationSettings MaxFileSize="100000000" />
                                                                    <ClientSideEvents FileUploadComplete="function(s, e) { Uploader_OnUploadComplete(e); }">
                                                                    </ClientSideEvents>
                                                                </dxuc:ASPxUploadControl>
                                                                <asp:Label ID="label41" runat="server" CssClass="labelNormal labelFloat" Text="Notas Corretagens (Html/mht/.zip)"
                                                                    ToolTip="Importação Nota Corretagem em formato html/htm/mht ou várias notas de Corretagem em formato .zip" />
                                                                <dxuc:ASPxUploadControl ID="uplNotaCorretagem" runat="server" ClientInstanceName="uplNotaCorretagem"
                                                                    OnFileUploadComplete="uplImportacaoDiarios_FileUploadComplete" CssClass="labelNormal"
                                                                    FileUploadMode="OnPageLoad">
                                                                    <ValidationSettings MaxFileSize="100000000" />
                                                                    <ClientSideEvents FileUploadComplete="function(s, e) { Uploader_OnUploadComplete(e); }">
                                                                    </ClientSideEvents>
                                                                </dxuc:ASPxUploadControl>
                                                                <asp:Label ID="label54" runat="server" CssClass="labelNormal labelFloat" Text="Notas Corretagens Bolsa (.pdf)"
                                                                    ToolTip="Importação Nota Corretagem em formato pdf ou várias notas de Corretagem em formato .zip" />
                                                                <dxuc:ASPxUploadControl ID="uplNotaCorretagemPDF" runat="server" ClientInstanceName="uplNotaCorretagemPDF"
                                                                    OnFileUploadComplete="uplImportacaoDiarios_FileUploadComplete" CssClass="labelNormal"
                                                                    FileUploadMode="OnPageLoad">
                                                                    <ValidationSettings MaxFileSize="100000000" />
                                                                    <ClientSideEvents FileUploadComplete="function(s, e) { Uploader_OnUploadComplete(e); }">
                                                                    </ClientSideEvents>
                                                                </dxuc:ASPxUploadControl>
                                                                <asp:Label ID="label11" runat="server" CssClass="labelNormal labelFloat" Text="RComiesp"></asp:Label>
                                                                <dxuc:ASPxUploadControl ID="uplRComiesp" runat="server" ClientInstanceName="uplRComiesp"
                                                                    OnFileUploadComplete="uplImportacaoDiarios_FileUploadComplete" CssClass="labelNormal"
                                                                    FileUploadMode="OnPageLoad">
                                                                    <ValidationSettings MaxFileSize="100000000" />
                                                                    <ClientSideEvents FileUploadComplete="function(s, e) { Uploader_OnUploadComplete(e); }">
                                                                    </ClientSideEvents>
                                                                </dxuc:ASPxUploadControl>
                                                                <asp:Label ID="label62" runat="server" CssClass="labelNormal labelFloat" Text="Notas Corretagens BMF (.pdf)"
                                                                    ToolTip="Importação Nota Corretagem em formato pdf ou várias notas de Corretagem em formato .zip" />
                                                                <dxuc:ASPxUploadControl ID="uplNotaCorretagemPDF_BMF" runat="server" ClientInstanceName="uplNotaCorretagemPDF_BMF"
                                                                    OnFileUploadComplete="uplImportacaoDiarios_FileUploadComplete" CssClass="labelNormal"
                                                                    FileUploadMode="OnPageLoad">
                                                                    <ValidationSettings MaxFileSize="100000000" />
                                                                    <ClientSideEvents FileUploadComplete="function(s, e) { Uploader_OnUploadComplete(e); }">
                                                                    </ClientSideEvents>
                                                                </dxuc:ASPxUploadControl>
                                                                <asp:Label ID="label12" runat="server" CssClass="labelNormal labelFloat" Text="CRCA"></asp:Label>
                                                                <dxuc:ASPxUploadControl ID="uplCRCA" runat="server" ClientInstanceName="uplCRCA"
                                                                    OnFileUploadComplete="uplImportacaoDiarios_FileUploadComplete" CssClass="labelNormal"
                                                                    FileUploadMode="OnPageLoad">
                                                                    <ValidationSettings MaxFileSize="100000000" />
                                                                    <ClientSideEvents FileUploadComplete="function(s, e) { Uploader_OnUploadComplete(e); }">
                                                                    </ClientSideEvents>
                                                                </dxuc:ASPxUploadControl>
                                                                <asp:Label ID="label13" runat="server" CssClass="labelNormal labelFloat" Text="POSR"></asp:Label>
                                                                <dxuc:ASPxUploadControl ID="uplPOSR" runat="server" ClientInstanceName="uplPOSR"
                                                                    OnFileUploadComplete="uplImportacaoDiarios_FileUploadComplete" CssClass="labelNormal"
                                                                    FileUploadMode="OnPageLoad">
                                                                    <ValidationSettings MaxFileSize="100000000" />
                                                                    <ClientSideEvents FileUploadComplete="function(s, e) { Uploader_OnUploadComplete(e); }">
                                                                    </ClientSideEvents>
                                                                </dxuc:ASPxUploadControl>
                                                            </div>
                                                        </div>
                                                    </dxw:ContentControl>
                                                </ContentCollection>
                                            </dxtc:TabPage>
                                        </TabPages>
                                        <TabPages>
                                            <dxtc:TabPage Text="Bolsa/BMF - Por Arquivos">
	                                                <ContentCollection>
		                                                <dxw:ContentControl runat="server">
			                                                <div class="innerPanel" id="innerPanel_MultiTabArquivos">
				                                                <table>
					                                                <tr>
						                                                <td>
							                                                <asp:Label ID="label68" runat="server" CssClass="labelNormal labelFloat" Text="Data:"></asp:Label>
						                                                </td>
						                                                <td>
							                                                <dxe:ASPxDateEdit ID="textDataMultiArquivos" runat="server" ClientInstanceName="textDataMultiArquivos" />
						                                                </td>
						                                                <td>
							                                                <div class="linkButton linkButtonNoBorder" style="margin-left: 40px; margin-top: 20px">
								                                                <asp:LinkButton ID="LinkButton7" runat="server" Font-Overline="false" CssClass="btnRun"
                                                                                OnClientClick="
                                                                    {
                                                                        
                                                                            callbackMultiSelectArquivos.SendCallback();                                                                             
                                                                        
                                                                        return false;
                                                                    }
                                                                    ">
								                                                <asp:Literal ID="Literal14" runat="server" Text="Importar" /><div></div>
                                                                                    </asp:LinkButton>
							                                                </div>
						                                                </td>
					                                                </tr>
				                                                </table>
                                                                <div class="clear">
                                                                 </div>
                                                               <div id="dropZone" class="formColumn formColumnFirst">
                                                                    <dxuc:ASPxUploadControl ID="DocumentsUploadControl" ClientInstanceName="DocumentsUploadControl"  runat="server"
                                                                        ShowProgressPanel="True"
                                                                        FileUploadMode="OnPageLoad"
                                                                        
                                                                        BrowseButton-Text="Selecione os Arquivos"
                                                                        OnFileUploadComplete="DocumentsUploadControl_FileUploadComplete"
                                                                        NullText="Selecione os arquivos..." UploadMode="Auto">
                                                                        <AdvancedModeSettings EnableMultiSelect="True" EnableFileList="True" EnableDragAndDrop="True" ExternalDropZoneID="dropZone"/>
                                                                        <ValidationSettings MaxFileSize="100000000" AllowedFileExtensions=".txt,.xml,.zip,.pdf,">
                                                                        </ValidationSettings>
                                                                        <ClientSideEvents  
                                                                                        FileUploadComplete="onFileUploadComplete"
                                                                                        FilesUploadComplete="onFilesUploadComplete"
                                                                                        FilesUploadStart="onFileUploadStart" />
                                                                    </dxuc:ASPxUploadControl>
                                                                   <dxuc:ASPxValidationSummary runat="server" ID="ValidationSummary" ClientInstanceName="ValidationSummary"
                                                                        RenderMode="Table" Width="250px" ShowErrorAsLink="false">
                                                                    </dxuc:ASPxValidationSummary>

                                                                </div>
                                                                 <div class="formColumn">
                                                                    <table>
                                                                       <tr>
                                                                           <td>
                                                                               <asp:Label ID="label81" runat="server" CssClass="labelNormal labelFloat" Text="Prefixos Validos:" />
                                                                        <asp:Label ID="Label80" runat="server" 
                                                                        Text="<br /><b>CDIX_<br /> CMDF_<br /> CONR_<br /> CONL_<br /> DBTCOper_</b> (DBTC Operação)<br /> <b>DBTL_<br /> PAPT_<br /> CSGD_<br /> DBTCPos_</b> (DBTC Posição)<br /> <b>PROD_<br /> NEGS_<br /> PESC_<br /> NotasZip_</b> (Notas Corretagens (Html/mht/.zip) )<br /> <b>NotasBolsa_</b> (Notas Corretagens Bolsa (.pdf) )<br /> <b>RComiesp_<br /> NotasBmf_</b> (Notas Corretagens BMF (.pdf) )<br /> <b>CRCA_<br /> POSR_<br /></b> <br />Exemplo:<br />NotasBmf_20160407.pdf, RComiesp_cliente.txt, PAPT_20160407.txt">
                                                                        </asp:Label>
                                                                               </td>
                                                                           </tr>
                                                                    </table>
                                                                         
                                                                 </div>
			                                                </div>
		                                                </dxw:ContentControl>
	                                                </ContentCollection>
                                                </dxtc:TabPage>
                                        </TabPages>
                                        <TabPages>
                                            <dxtc:TabPage Text="Excel - Diário">
                                                <ContentCollection>
                                                    <dxw:ContentControl runat="server">
                                                        <div class="innerPanel" id="innerPanel_TabImportacaoExcel">
                                                            <table>
                                                                <tr>
                                                                    <td>
                                                                        <div class="linkButton linkButtonNoBorder" style="margin-left: 40px; margin-top: 20px">
                                                                            <table>
                                                                                <tr>
                                                                                    <td>
                                                                                        <asp:LinkButton ID="btnRunExcel" runat="server" Font-Overline="false" CssClass="btnRun"
                                                                                            OnClientClick="
                                                                    {
                                                                        if(!ChecaExcel()) {
                                                                            alert('Escolha algum arquivo para importação!');                                                                            
                                                                        }
                                                                        else {
                                                                            callbackExcel.SendCallback();                                                                             
                                                                        }
                                                                        return false;
                                                                    }
                                                                    ">
                                                                                            <asp:Literal ID="Literal4" runat="server" Text="Importar" /><div>
                                                                                            </div>
                                                                                        </asp:LinkButton>
                                                                                    </td>
                                                                                    <td width="80px">
                                                                                        <asp:LinkButton ID="btnLimparExcel" runat="server" Font-Overline="false" CssClass="btnClear"
                                                                                            OnClientClick="
                                                                    {
                                                                        LimparUploadsExcel();
                                                                        return false;
                                                                    }
                                                                    ">
                                                                                            <asp:Literal ID="Literal5" runat="server" Text="Limpar" /><div>
                                                                                            </div>
                                                                                        </asp:LinkButton>
                                                                                    </td>
                                                                                    <td>
                                                                                        <div style="display: none">
                                                                                            <dxe:ASPxCheckBox ClientInstanceName="checkIgnora" ID="checkIgnora" runat="server"
                                                                                                Text="Ignora erros de cadastro" ToolTip="Se marcado ignora clientes, carteiras, ativos não cadastrados (apenas para operações)" />
                                                                                        </div>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </div>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                            <div class="clear">
                                                            </div>
                                                            <div class="formColumn formColumnFirst">
                                                                <asp:LinkButton ID="btnOrdemBolsa" runat="server" OnClick="DownloadPlanilhaModeloDiario_Click"
                                                                    ToolTip="Planilha de Modelo Ordem Bolsa" BorderStyle="None">
                                                <img alt="" src="../imagens/document-excel.png"/>
                                                                </asp:LinkButton>
                                                                <asp:Label ID="label17" runat="server" CssClass="labelNormal labelFloat" Text="Ordem Bolsa" />
                                                                <dxuc:ASPxUploadControl ID="uplOrdemBolsa" runat="server" ClientInstanceName="uplOrdemBolsa"
                                                                    OnFileUploadComplete="uplOrdemBolsa_FileUploadComplete" CssClass="labelNormal"
                                                                    FileUploadMode="OnPageLoad">
                                                                    <ValidationSettings MaxFileSize="100000000" />
                                                                    <ClientSideEvents FileUploadComplete="function(s, e) { Uploader_OnUploadComplete(e); }" />
                                                                </dxuc:ASPxUploadControl>
                                                                <asp:LinkButton ID="btnOperacaoEmprestimoBolsa" runat="server" OnClick="DownloadPlanilhaModeloDiario_Click"
                                                                    ToolTip="Planilha de Modelo Operação Empréstimo Bolsa" BorderStyle="None">
                                                <img alt="" src="../imagens/document-excel.png" />
                                                                </asp:LinkButton>
                                                                <asp:Label ID="label20" runat="server" CssClass="labelNormal labelFloat" Text="Operação Empréstimo Bolsa" />
                                                                <dxuc:ASPxUploadControl ID="uplOperacaoEmprestimoBolsa" runat="server" ClientInstanceName="uplOperacaoEmprestimoBolsa"
                                                                    OnFileUploadComplete="uplOperacaoEmprestimoBolsa_FileUploadComplete" CssClass="labelNormal"
                                                                    FileUploadMode="OnPageLoad">
                                                                    <ValidationSettings MaxFileSize="100000000" />
                                                                    <ClientSideEvents FileUploadComplete="function(s, e) { Uploader_OnUploadComplete(e); }" />
                                                                </dxuc:ASPxUploadControl>
                                                                <asp:LinkButton ID="btnOrdemBMF" runat="server" OnClick="DownloadPlanilhaModeloDiario_Click"
                                                                    ToolTip="Planilha de Modelo Ordem BMF" BorderStyle="None">
                                                <img alt="" src="../imagens/document-excel.png" />
                                                                </asp:LinkButton>
                                                                <asp:Label ID="label18" runat="server" CssClass="labelNormal labelFloat" Text="Ordem BMF" />
                                                                <dxuc:ASPxUploadControl ID="uplOrdemBMF" runat="server" ClientInstanceName="uplOrdemBMF"
                                                                    OnFileUploadComplete="uplOrdemBMF_FileUploadComplete" CssClass="labelNormal"
                                                                    FileUploadMode="OnPageLoad">
                                                                    <ValidationSettings MaxFileSize="100000000" />
                                                                    <ClientSideEvents FileUploadComplete="function(s, e) { Uploader_OnUploadComplete(e); }" />
                                                                </dxuc:ASPxUploadControl>
                                                                <asp:LinkButton ID="btnOperacaoFundo" runat="server" OnClick="DownloadPlanilhaModeloDiario_Click"
                                                                    ToolTip="Planilha de Modelo Operação Fundo" BorderStyle="None">
                                                <img alt="" src="../imagens/document-excel.png" />
                                                                </asp:LinkButton>
                                                                <asp:Label ID="label15" runat="server" CssClass="labelNormal labelFloat" Text="Operação Fundo" />
                                                                <dxuc:ASPxUploadControl ID="uplOperacaoFundo" runat="server" ClientInstanceName="uplOperacaoFundo"
                                                                    OnFileUploadComplete="uplOperacaoFundo_FileUploadComplete" CssClass="labelNormal"
                                                                    FileUploadMode="OnPageLoad">
                                                                    <ValidationSettings MaxFileSize="100000000" />
                                                                    <ClientSideEvents FileUploadComplete="function(s, e) { Uploader_OnUploadComplete(e); }" />
                                                                </dxuc:ASPxUploadControl>
                                                                <asp:LinkButton ID="btnOperacaoRendaFixa" runat="server" OnClick="DownloadPlanilhaModeloDiario_Click"
                                                                    ToolTip="Planilha de Modelo Operação Renda Fixa" BorderStyle="None">
                                                <img alt="" src="../imagens/document-excel.png" />
                                                                </asp:LinkButton>
                                                                <asp:Label ID="label39" runat="server" CssClass="labelNormal labelFloat" Text="Operação Renda Fixa" />
                                                                <dxuc:ASPxUploadControl ID="uplOperacaoRendaFixa" runat="server" ClientInstanceName="uplOperacaoRendaFixa"
                                                                    OnFileUploadComplete="uplOperacaoRendaFixa_FileUploadComplete" CssClass="labelNormal"
                                                                    FileUploadMode="OnPageLoad">
                                                                    <ValidationSettings MaxFileSize="100000000" />
                                                                    <ClientSideEvents FileUploadComplete="function(s, e) { Uploader_OnUploadComplete(e); }" />
                                                                </dxuc:ASPxUploadControl>
                                                                <asp:LinkButton ID="btnOperacaoCotista" runat="server" OnClick="DownloadPlanilhaModeloDiario_Click"
                                                                    ToolTip="Planilha de Modelo Operação Cotista" BorderStyle="None">
                                                <img alt="" src="../imagens/document-excel.png" />
                                                                </asp:LinkButton>
                                                                <asp:Label ID="label19" runat="server" CssClass="labelNormal labelFloat" Text="Operação Cotista" />
                                                                <dxuc:ASPxUploadControl ID="uplOperacaoCotista" runat="server" ClientInstanceName="uplOperacaoCotista"
                                                                    OnFileUploadComplete="uplOperacaoCotista_FileUploadComplete" CssClass="labelNormal"
                                                                    FileUploadMode="OnPageLoad">
                                                                    <ValidationSettings MaxFileSize="100000000" />
                                                                    <ClientSideEvents FileUploadComplete="function(s, e) { Uploader_OnUploadComplete(e); }" />
                                                                </dxuc:ASPxUploadControl>
                                                                <asp:LinkButton ID="btnContabLancamento" runat="server" OnClick="DownloadPlanilhaModeloDiario_Click"
                                                                    ToolTip="Planilha de Modelo Lançamento Contábil" BorderStyle="None">
                                                <img alt="" src="../imagens/document-excel.png" />
                                                                </asp:LinkButton>
                		                                        
		                                                        <asp:Label ID="label60" runat="server" CssClass="labelNormal labelFloat" Text="Lançamento Contábil"/>        		                            
		                                                        <dxuc:ASPxUploadControl ID="uplContabLancamento" runat="server" ClientInstanceName="uplContabLancamento"
			                                                            OnFileUploadComplete="uplContabLancamento_FileUploadComplete" 
			                                                            CssClass="labelNormal" FileUploadMode="OnPageLoad">
		                                                             <ValidationSettings MaxFileSize="100000000" />
		                                                             <ClientSideEvents FileUploadComplete="function(s, e) { Uploader_OnUploadComplete(e); }" />
		                                                        </dxuc:ASPxUploadControl>

		                                                        <asp:LinkButton ID="btnColagemResgates" runat="server" OnClick="DownloadPlanilhaModeloDiario_Click" ToolTip="Planilha de Modelo Resgates - Colagem" BorderStyle="None">
                                               <img alt="" src="../imagens/document-excel.png" />
                                                                </asp:LinkButton>
                		                                        
        		                            	                <asp:Label ID="labelColagemResgates" runat="server" CssClass="labelNormal labelFloat" Text="Colagem - Resgates"/>
                                                                <dxuc:ASPxUploadControl ID="uplColagemResgates" runat="server" ClientInstanceName="uplColagemResgates"
			                                                            OnFileUploadComplete="uplColagemResgates_FileUploadComplete"
			                                                            CssClass="labelNormal" FileUploadMode="OnPageLoad">
		                                                             <ValidationSettings MaxFileSize="100000000" />
		                                                             <ClientSideEvents FileUploadComplete="function(s, e) { Uploader_OnUploadComplete(e); }" />
		                                                        </dxuc:ASPxUploadControl>
                		                                        
		                                                        <asp:LinkButton ID="btnTransferenciaBolsa" runat="server" OnClick="DownloadPlanilhaModeloDiario_Click" ToolTip="Planilha de Modelo Transferência - Bolsa" BorderStyle="None">
                                               <img alt="" src="../imagens/document-excel.png" />
                                                                </asp:LinkButton>
                		                                        
        		                            	                <asp:Label ID="labelTransferenciaBolsa" runat="server" CssClass="labelNormal labelFloat" Text="Transferência - Bolsa"/>
                                                                <dxuc:ASPxUploadControl ID="uplTransferenciaBolsa" runat="server" ClientInstanceName="uplTransferenciaBolsa"
			                                                            OnFileUploadComplete="uplTransferenciaBolsa_FileUploadComplete"
			                                                            CssClass="labelNormal" FileUploadMode="OnPageLoad">
		                                                             <ValidationSettings MaxFileSize="100000000" />
		                                                             <ClientSideEvents FileUploadComplete="function(s, e) { Uploader_OnUploadComplete(e); }" />
		                                                        </dxuc:ASPxUploadControl>
                		                                        
		                                                        <asp:LinkButton ID="btnTaxaCurvaRendaFixa" runat="server" OnClick="DownloadPlanilhaModeloDiario_Click" ToolTip="Planilha de Modelo Taxa - Curva Renda Fixa" BorderStyle="None">
                                               <img alt="" src="../imagens/document-excel.png" />
                                                                </asp:LinkButton>   
                                                                
		                                                        <asp:Label ID="labelTaxaCurvaRendaFixa" runat="server" CssClass="labelNormal labelFloat" Text="Taxa - Curva Renda Fixa"/>
                                                                <dxuc:ASPxUploadControl ID="uplTaxaCurvaRendaFixa" runat="server" ClientInstanceName="uplTaxaCurvaRendaFixa"
			                                                            OnFileUploadComplete="uplTaxaCurvaRendaFixa_FileUploadComplete"
			                                                            CssClass="labelNormal" FileUploadMode="OnPageLoad">
		                                                             <ValidationSettings MaxFileSize="100000000" />
		                                                             <ClientSideEvents FileUploadComplete="function(s, e) { Uploader_OnUploadComplete(e); }" />
		                                                        </dxuc:ASPxUploadControl>
                		                    
		                                        <asp:LinkButton ID="btnMTMManual" runat="server" OnClick="DownloadPlanilhaModeloDiario_Click" ToolTip="Planilha de Modelo MTM Manual" BorderStyle="None">
                                                                <img alt="" src="../imagens/document-excel.png" />
                                                                </asp:LinkButton>   
                                                                
		                                                        <asp:Label ID="labelMTMManual" runat="server" CssClass="labelNormal labelFloat" Text="MTM Manual"/>
                                                                <dxuc:ASPxUploadControl ID="uplMTMManual" runat="server" ClientInstanceName="uplMTMManual"
			                                                            OnFileUploadComplete="uplMTMManual_FileUploadComplete"
			                                                            CssClass="labelNormal" FileUploadMode="OnPageLoad">
		                                                             <ValidationSettings MaxFileSize="100000000" />
		                                                             <ClientSideEvents FileUploadComplete="function(s, e) { Uploader_OnUploadComplete(e); }" />
		                                                        </dxuc:ASPxUploadControl>
                		                                        
                                                            </div>
	                                        
	                                        <div class="formColumn">

                                                <asp:LinkButton ID="btnAtivoBolsa" runat="server" OnClick="DownloadPlanilhaModeloDiario_Click"  
                                                                    ToolTip="Planilha de Modelo Ativo Bolsa" BorderStyle="None">
                                                <img alt="" src="../imagens/document-excel.png" />
                                                                </asp:LinkButton>
                                                                <asp:Label ID="label55" runat="server" CssClass="labelNormal labelFloat" Text="Ativo Bolsa" />
                                                                <dxuc:ASPxUploadControl ID="uplAtivoBolsa" runat="server" ClientInstanceName="uplAtivoBolsa"
                                                                    OnFileUploadComplete="uplAtivoBolsa_FileUploadComplete" CssClass="labelNormal"
                                                                    FileUploadMode="OnPageLoad">
                                                                    <ValidationSettings MaxFileSize="100000000" />
                                                                    <ClientSideEvents FileUploadComplete="function(s, e) { Uploader_OnUploadComplete(e); }" />
                                                                </dxuc:ASPxUploadControl>
                                                                
                                                <asp:LinkButton ID="btnCotacaoBolsa" runat="server" OnClick="DownloadPlanilhaModeloDiario_Click"
                                                                    ToolTip="Planilha de Modelo Cotação Bolsa" BorderStyle="None">
                                                <img alt="" src="../imagens/document-excel.png" />
                                                                </asp:LinkButton>
                                                                <asp:Label ID="label38" runat="server" CssClass="labelNormal labelFloat" Text="Cotação Bolsa" />
                                                                <dxuc:ASPxUploadControl ID="uplCotacaoBolsa" runat="server" ClientInstanceName="uplCotacaoBolsa"
                                                                    OnFileUploadComplete="uplCotacaoBolsa_FileUploadComplete" CssClass="labelNormal"
                                                                    FileUploadMode="OnPageLoad">
                                                                    <ValidationSettings MaxFileSize="100000000" />
                                                                    <ClientSideEvents FileUploadComplete="function(s, e) { Uploader_OnUploadComplete(e); }" />
                                                                </dxuc:ASPxUploadControl>
                                                                
                                                <asp:LinkButton ID="btnCotacaoBMF" runat="server" OnClick="DownloadPlanilhaModeloDiario_Click"
                                                                    ToolTip="Planilha de Modelo Cotação BMF" BorderStyle="None">
                                                <img alt="" src="../imagens/document-excel.png" />
                                                                </asp:LinkButton>
                                                                <asp:Label ID="label51" runat="server" CssClass="labelNormal labelFloat" Text="Cotação BMF" />
                                                                <dxuc:ASPxUploadControl ID="uplCotacaoBMF" runat="server" ClientInstanceName="uplCotacaoBMF"
                                                                    OnFileUploadComplete="uplCotacaoBMF_FileUploadComplete" CssClass="labelNormal"
                                                                    FileUploadMode="OnPageLoad">
                                                                    <ValidationSettings MaxFileSize="100000000" />
                                                                    <ClientSideEvents FileUploadComplete="function(s, e) { Uploader_OnUploadComplete(e); }" />
                                                                </dxuc:ASPxUploadControl>
                                                                
                                                <asp:LinkButton ID="btnCotacaoIndice" runat="server" OnClick="DownloadPlanilhaModeloDiario_Click"
                                                                    ToolTip="Planilha de Modelo Cotação Índice" BorderStyle="None">
                                                <img alt="" src="../imagens/document-excel.png" />
                                                                </asp:LinkButton>
                                                                <asp:Label ID="label21" runat="server" CssClass="labelNormal labelFloat" Text="Cotação Índice" />
                                                                <dxuc:ASPxUploadControl ID="uplCotacaoIndice" runat="server" ClientInstanceName="uplCotacaoIndice"
                                                                    OnFileUploadComplete="uplCotacaoIndice_FileUploadComplete" CssClass="labelNormal"
                                                                    FileUploadMode="OnPageLoad">
                                                                    <ValidationSettings MaxFileSize="100000000" />
                                                                    <ClientSideEvents FileUploadComplete="function(s, e) { Uploader_OnUploadComplete(e); }" />
                                                                </dxuc:ASPxUploadControl>
                                                                
                                                <asp:LinkButton ID="btnCotacaoSerie" runat="server" OnClick="DownloadPlanilhaModeloDiario_Click"
                                                                    ToolTip="Planilha de Modelo Cotação Série" BorderStyle="None">
                                                <img alt="" src="../imagens/document-excel.png" />
                                                                </asp:LinkButton>
                                                                <asp:Label ID="label43" runat="server" CssClass="labelNormal labelFloat" Text="Cotação Série" />
                                                                <dxuc:ASPxUploadControl ID="uplCotacaoSerie" runat="server" ClientInstanceName="uplCotacaoSerie"
                                                                    OnFileUploadComplete="uplCotacaoSerie_FileUploadComplete" CssClass="labelNormal"
                                                                    FileUploadMode="OnPageLoad">
                                                                    <ValidationSettings MaxFileSize="100000000" />
                                                                    <ClientSideEvents FileUploadComplete="function(s, e) { Uploader_OnUploadComplete(e); }" />
                                                                </dxuc:ASPxUploadControl>
                                                <asp:LinkButton ID="btnCotacaoAnbimaPublico" runat="server" OnClick="DownloadPlanilhaModeloDiario_Click"
                                                                    ToolTip="Planilha de Modelo Cotação Anbima Público" BorderStyle="None">
                                                <img alt="" src="../imagens/document-excel.png" />
                                                                </asp:LinkButton>
                                                                <asp:Label ID="label44" runat="server" CssClass="labelNormal labelFloat" Text="Cotação Anbima Público" />
                                                                <dxuc:ASPxUploadControl ID="uplCotacaoAnbimaPublico" runat="server" ClientInstanceName="uplCotacaoAnbimaPublico"
                                                                    OnFileUploadComplete="uplCotacaoAnbimaPublico_FileUploadComplete" CssClass="labelNormal"
                                                                    FileUploadMode="OnPageLoad">
                                                                    <ValidationSettings MaxFileSize="100000000" />
                                                                    <ClientSideEvents FileUploadComplete="function(s, e) { Uploader_OnUploadComplete(e); }" />
                                                                </dxuc:ASPxUploadControl>
                                                                
                                                <asp:LinkButton ID="btnHistoricoCota" runat="server" OnClick="DownloadPlanilhaModeloDiario_Click"
                                                                    ToolTip="Planilha de Modelo Histórico Cota" BorderStyle="None">
                                                <img alt="" src="../imagens/document-excel.png" />
                                                                </asp:LinkButton>
                                                                <asp:Label ID="label16" runat="server" CssClass="labelNormal labelFloat" Text="Histórico Cota" />
                                                                <dxuc:ASPxUploadControl ID="uplHistoricoCota" runat="server" ClientInstanceName="uplHistoricoCota"
                                                                    OnFileUploadComplete="uplHistoricoCota_FileUploadComplete" CssClass="labelNormal"
                                                                    FileUploadMode="OnPageLoad">
                                                                    <ValidationSettings MaxFileSize="100000000" />
                                                                    <ClientSideEvents FileUploadComplete="function(s, e) { Uploader_OnUploadComplete(e); }" />
                                                                </dxuc:ASPxUploadControl>
                                                                
                                                <asp:LinkButton ID="btnColagemComeCotas" runat="server" OnClick="DownloadPlanilhaModeloDiario_Click"
                                                                    ToolTip="Planilha de Modelo ComeCotas - Colagem" BorderStyle="None">
                                                <img alt="" src="../imagens/document-excel.png" />
                                                                </asp:LinkButton>
                                                                <asp:Label ID="labelColagemComeCotas" runat="server" CssClass="labelNormal labelFloat"
                                                                    Text="Colagem - ComeCotas" />
                                                                <dxuc:ASPxUploadControl ID="uplColagemComeCotas" runat="server" ClientInstanceName="uplColagemComeCotas"
                                                                    OnFileUploadComplete="uplColagemComeCotas_FileUploadComplete" CssClass="labelNormal"
                                                                    FileUploadMode="OnPageLoad">
                                                                    <ValidationSettings MaxFileSize="100000000" />
                                                                    <ClientSideEvents FileUploadComplete="function(s, e) { Uploader_OnUploadComplete(e); }" />
                                                                </dxuc:ASPxUploadControl>

                                                                
		                                        <asp:LinkButton ID="btnTransferenciaBMF" runat="server" OnClick="DownloadPlanilhaModeloDiario_Click" 
		                                                            ToolTip="Planilha de Modelo Transferência - BMF" BorderStyle="None">
                                                <img alt="" src="../imagens/document-excel.png" />
                                                                </asp:LinkButton>
        		                            	                <asp:Label ID="label69" runat="server" CssClass="labelNormal labelFloat" Text="Transferência - BMF"/>
                                                                <dxuc:ASPxUploadControl ID="uplTransferenciaBMF" runat="server" ClientInstanceName="uplTransferenciaBMF"
			                                                            OnFileUploadComplete="uplTransferenciaBMF_FileUploadComplete"
			                                                            CssClass="labelNormal" FileUploadMode="OnPageLoad">
		                                                             <ValidationSettings MaxFileSize="100000000" />
		                                                             <ClientSideEvents FileUploadComplete="function(s, e) { Uploader_OnUploadComplete(e); }" />
		                                                        </dxuc:ASPxUploadControl>
		                                        
		                                        <asp:LinkButton ID="btnCravaCota" runat="server" OnClick="DownloadPlanilhaModeloDiario_Click" 
		                                                            ToolTip="Planilha de Modelo Crava Cota" BorderStyle="None">
                                                <img alt="" src="../imagens/document-excel.png" />
                                                                </asp:LinkButton>   
		                                                        <asp:Label ID="labelCravaCota" runat="server" CssClass="labelNormal labelFloat" Text="Crava Cota"/>
                                                                <dxuc:ASPxUploadControl ID="uplCravaCota" runat="server" ClientInstanceName="uplCravaCota"
			                                                            OnFileUploadComplete="uplCravaCota_FileUploadComplete"
			                                                            CssClass="labelNormal" FileUploadMode="OnPageLoad">
		                                                             <ValidationSettings MaxFileSize="100000000" />
		                                                             <ClientSideEvents FileUploadComplete="function(s, e) { Uploader_OnUploadComplete(e); }" />
		                                                        </dxuc:ASPxUploadControl>
                                                                
                                                                

                                                 <asp:LinkButton ID="btnLiquidacaoDiaria" runat="server" OnClick="DownloadPlanilhaModeloDiario_Click"
                                                                    ToolTip="Planilha de Modelo Liquidação Diária" BorderStyle="None">
                                                 <img alt="" src="../imagens/document-excel.png" />
                                                                </asp:LinkButton>
                                                                <asp:Label ID="lblLiquidacaoDiaria" runat="server" CssClass="labelNormal labelFloat" Text="Liquidação Diária" />
                                                                <dxuc:ASPxUploadControl ID="uplLiquidacaoDiaria" runat="server" ClientInstanceName="uplLiquidacaoDiaria"
                                                                    OnFileUploadComplete="uplLiquidacaoDiaria_FileUploadComplete" CssClass="labelNormal"
                                                                    FileUploadMode="OnPageLoad">
                                                                    <ValidationSettings MaxFileSize="100000000" />
                                                                    <ClientSideEvents FileUploadComplete="function(s, e) { Uploader_OnUploadComplete(e); }" />
                                                                </dxuc:ASPxUploadControl>                                                                

                                                            </div>
                                                        </div>
                                                    </dxw:ContentControl>
                                                </ContentCollection>
                                            </dxtc:TabPage>
                                        </TabPages>
                                        <TabPages>
                                            <dxtc:TabPage Text="Excel - Implantação">
                                                <ContentCollection>
                                                    <dxw:ContentControl runat="server">
                                                        <div class="innerPanel" id="innerPanel_TabImplantacao">
                                                            <table>
                                                                <tr>
                                                                    <td>
                                                                        <div class="linkButton linkButtonNoBorder" style="margin-left: 40px; margin-top: 20px">
                                                                            <asp:LinkButton ID="btnRunExcelImplantacao" runat="server" Font-Overline="false"
                                                                                CssClass="btnRun" OnClientClick="
                                                                    {
                                                                        if(!ChecaExcelImplantacao()) {
                                                                            alert('Escolha algum arquivo para importação!');                                                                            
                                                                        }
                                                                        else {                                                                    
                                                                            callbackExcelImplantacao.SendCallback();                                                                             
                                                                        }
                                                                        return false;
                                                                    }
                                                                    ">
                                                                                <asp:Literal ID="Literal6" runat="server" Text="Importar" /><div>
                                                                                </div>
                                                                            </asp:LinkButton>
                                                                            <asp:LinkButton ID="btnLimparExcelImplantacao" runat="server" Font-Overline="false"
                                                                                CssClass="btnClear" OnClientClick="
                                                                    {
                                                                        LimparUploadsExcelImplantacao();
                                                                        return false;
                                                                    }
                                                                    ">
                                                                                <asp:Literal ID="Literal7" runat="server" Text="Limpar" /><div>
                                                                                </div>
                                                                            </asp:LinkButton>
                                                                        </div>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                            <div class="clear">
                                                            </div>
                                                            <div class="formColumn formColumnFirst">
                                                                <asp:LinkButton ID="btnPosicaoBolsa" runat="server" OnClick="DownloadPlanilhaModeloImplantacao_Click"
                                                                    ToolTip="Planilha de Modelo Posição Bolsa" BorderStyle="None">
                                                <img alt="" src="../imagens/document-excel.png" />
                                                                </asp:LinkButton>
                                                                <asp:Label ID="label22" runat="server" CssClass="labelNormal labelFloat" Text="Posição Bolsa" />
                                                                <dxuc:ASPxUploadControl ID="uplPosicaoBolsa" runat="server" ClientInstanceName="uplPosicaoBolsa"
                                                                    OnFileUploadComplete="uplPosicaoBolsa_FileUploadComplete" CssClass="labelNormal"
                                                                    FileUploadMode="OnPageLoad">
                                                                    <ValidationSettings MaxFileSize="100000000" />
                                                                    <ClientSideEvents FileUploadComplete="function(s, e) { Uploader_OnUploadComplete(e); }" />
                                                                </dxuc:ASPxUploadControl>
                                                                
                                                                <asp:LinkButton ID="btnPosicaoTermo" runat="server" OnClick="DownloadPlanilhaModeloImplantacao_Click"
                                                                    ToolTip="Planilha de Modelo Posição Termo Bolsa" BorderStyle="None">
                                                <img alt="" src="../imagens/document-excel.png" />
                                                                </asp:LinkButton>
                                                                <asp:Label ID="label29" runat="server" CssClass="labelNormal labelFloat" Text="Posição Termo" />
                                                                <dxuc:ASPxUploadControl ID="uplPosicaoTermo" runat="server" ClientInstanceName="uplPosicaoTermo"
                                                                    OnFileUploadComplete="uplPosicaoTermo_FileUploadComplete" CssClass="labelNormal"
                                                                    FileUploadMode="OnPageLoad">
                                                                    <ValidationSettings MaxFileSize="100000000" />
                                                                    <ClientSideEvents FileUploadComplete="function(s, e) { Uploader_OnUploadComplete(e); }" />
                                                                </dxuc:ASPxUploadControl>
                                                                
                                                                <asp:LinkButton ID="btnPosicaoEmprestimoBolsa" runat="server" OnClick="DownloadPlanilhaModeloImplantacao_Click"
                                                                    ToolTip="Planilha de Modelo Posição Empréstimo Bolsa" BorderStyle="None">
                                                <img alt="" src="../imagens/document-excel.png" />
                                                                </asp:LinkButton>
                                                                <asp:Label ID="label27" runat="server" CssClass="labelNormal labelFloat" Text="Posição Empréstimo Bolsa" />
                                                                <dxuc:ASPxUploadControl ID="uplPosicaoEmprestimoBolsa" runat="server" ClientInstanceName="uplPosicaoEmprestimoBolsa"
                                                                    OnFileUploadComplete="uplPosicaoEmprestimoBolsa_FileUploadComplete" CssClass="labelNormal"
                                                                    FileUploadMode="OnPageLoad">
                                                                    <ValidationSettings MaxFileSize="100000000" />
                                                                    <ClientSideEvents FileUploadComplete="function(s, e) { Uploader_OnUploadComplete(e); }" />
                                                                </dxuc:ASPxUploadControl>
                                                                
                                                                <asp:LinkButton ID="btnPosicaoBMF" runat="server" OnClick="DownloadPlanilhaModeloImplantacao_Click"
                                                                    ToolTip="Planilha de Modelo Posicão BMF" BorderStyle="None">
                                                <img alt="" src="../imagens/document-excel.png" />
                                                                </asp:LinkButton>
                                                                <asp:Label ID="label23" runat="server" CssClass="labelNormal labelFloat" Text="Posição BMF" />
                                                                <dxuc:ASPxUploadControl ID="uplPosicaoBMF" runat="server" ClientInstanceName="uplPosicaoBMF"
                                                                    OnFileUploadComplete="uplPosicaoBMF_FileUploadComplete" CssClass="labelNormal"
                                                                    FileUploadMode="OnPageLoad">
                                                                    <ValidationSettings MaxFileSize="100000000" />
                                                                    <ClientSideEvents FileUploadComplete="function(s, e) { Uploader_OnUploadComplete(e); }" />
                                                                </dxuc:ASPxUploadControl>
                                                                
                                                                <asp:LinkButton ID="btnPosicaoFundo" runat="server" OnClick="DownloadPlanilhaModeloImplantacao_Click"
                                                                    ToolTip="Planilha de Modelo Posição Fundo" BorderStyle="None">
                                                <img alt="" src="../imagens/document-excel.png" />
                                                                </asp:LinkButton>
                                                                <asp:Label ID="label24" runat="server" CssClass="labelNormal labelFloat" Text="Posição Fundo com Cálculo Residual" />
                                                                <dxuc:ASPxUploadControl ID="uplPosicaoFundo" runat="server" ClientInstanceName="uplPosicaoFundo"
                                                                    OnFileUploadComplete="uplPosicaoFundo_FileUploadComplete" CssClass="labelNormal"
                                                                    FileUploadMode="OnPageLoad">
                                                                    <ValidationSettings MaxFileSize="100000000" />
                                                                    <ClientSideEvents FileUploadComplete="function(s, e) { Uploader_OnUploadComplete(e); }" />
                                                                </dxuc:ASPxUploadControl>
                                                                
                                                                <asp:LinkButton ID="btnPosicaoRendaFixa" runat="server" OnClick="DownloadPlanilhaModeloImplantacao_Click"
                                                                    ToolTip="Planilha de Modelo Posicão Renda Fixa" BorderStyle="None">
                                                <img alt="" src="../imagens/document-excel.png" />
                                                                </asp:LinkButton>
                                                                <asp:Label ID="label25" runat="server" CssClass="labelNormal labelFloat" Text="Posição Renda Fixa" />
                                                                <dxuc:ASPxUploadControl ID="uplPosicaoRendaFixa" runat="server" ClientInstanceName="uplPosicaoRendaFixa"
                                                                    OnFileUploadComplete="uplPosicaoRendaFixa_FileUploadComplete" CssClass="labelNormal"
                                                                    FileUploadMode="OnPageLoad">
                                                                    <ValidationSettings MaxFileSize="100000000" />
                                                                    <ClientSideEvents FileUploadComplete="function(s, e) { Uploader_OnUploadComplete(e); }" />
                                                                </dxuc:ASPxUploadControl>
                                                                
                                                                <asp:LinkButton ID="btnPosicaoSwap" runat="server" OnClick="DownloadPlanilhaModeloImplantacao_Click"
                                                                    ToolTip="Planilha de Modelo Posicão Swap" BorderStyle="None">
                                                <img alt="" src="../imagens/document-excel.png" />
                                                                </asp:LinkButton>
                                                                <asp:Label ID="label28" runat="server" CssClass="labelNormal labelFloat" Text="Posição Swap" />
                                                                <dxuc:ASPxUploadControl ID="uplPosicaoSwap" runat="server" ClientInstanceName="uplPosicaoSwap"
                                                                    OnFileUploadComplete="uplPosicaoSwap_FileUploadComplete" CssClass="labelNormal"
                                                                    FileUploadMode="OnPageLoad">
                                                                    <ValidationSettings MaxFileSize="100000000" />
                                                                    <ClientSideEvents FileUploadComplete="function(s, e) { Uploader_OnUploadComplete(e); }" />
                                                                </dxuc:ASPxUploadControl>
                                                                
                                                                <asp:LinkButton ID="btnPosicaoCotista" runat="server" OnClick="DownloadPlanilhaModeloImplantacao_Click"
                                                                    ToolTip="Planilha de Modelo Posição Cotista" BorderStyle="None">
                                                <img alt="" src="../imagens/document-excel.png" />
                                                                </asp:LinkButton>
                                                                <asp:Label ID="label46" runat="server" CssClass="labelNormal labelFloat" Text="Posição Cotista com Cálculo Residual" />
                                                                <dxuc:ASPxComboBox ID="dropDefineFonteImportacao" runat="server" ClientInstanceName="dropDefineFonteImportacao"
                                                                    ShowShadow="false" DropDownStyle="DropDownList" >
                                                                    <items>
                                                                        <dxe:ListEditItem Value="1" Text="Via Planilha" />
                                                                        <dxe:ListEditItem Value="2" Text="Via Tabela Auxiliar" />
                                                                    </items>
                                                                    <ClientSideEvents ValueChanged="function(s, e){EnableFileSelection(s.GetValue())}" />
                                                                </dxuc:ASPxComboBox>
                                                                <dxuc:ASPxUploadControl ID="uplPosicaoCotista" runat="server" ClientInstanceName="uplPosicaoCotista"
                                                                    OnFileUploadComplete="uplPosicaoCotista_FileUploadComplete" CssClass="labelNormal" Paddings-PaddingTop="3px"
                                                                    FileUploadMode="OnPageLoad" ClientVisible="false">
                                                                    <ValidationSettings MaxFileSize="100000000" />
                                                                    <ClientSideEvents FileUploadComplete="function(s, e) { Uploader_OnUploadComplete(e); }" />
                                                                </dxuc:ASPxUploadControl>
                                                                
                                                <asp:LinkButton ID="btnPosicaoCotistaHistorico" runat="server" OnClick="DownloadPlanilhaModeloImplantacao_Click" ToolTip="Planilha de Modelo Posição Cotista Histórico" BorderStyle="None">
                                                <img alt="" src="../imagens/document-excel.png" />
                                                </asp:LinkButton>
                                                
		                                        <asp:Label ID="label58" runat="server" CssClass="labelNormal labelFloat" Text="Posição Cotista Histórico" />        		                            
		                                        <dxuc:ASPxUploadControl ID="uplPosicaoCotistaHistorico" runat="server" ClientInstanceName="uplPosicaoCotistaHistorico"
			                                            OnFileUploadComplete="uplPosicaoCotistaHistorico_FileUploadComplete" 
			                                            CssClass="labelNormal" FileUploadMode="OnPageLoad">
		                                             <ValidationSettings MaxFileSize="100000000" />
		                                             <ClientSideEvents FileUploadComplete="function(s, e) { Uploader_OnUploadComplete(e); }" />
		                                        </dxuc:ASPxUploadControl>
		                                        		                                        		                                        
		                                        <asp:LinkButton ID="btnCadastroPessoa" runat="server" OnClick="DownloadPlanilhaModeloImplantacao_Click" ToolTip="Planilha de Modelo Cadastro Pessoa" BorderStyle="None">
                                                <img alt="" src="../imagens/document-excel.png" />
                                                </asp:LinkButton>
		                                        
		                                        <asp:Label ID="label47" runat="server" CssClass="labelNormal labelFloat" Text="Cadastro Pessoa"/>        		                            
		                                        <dxuc:ASPxUploadControl ID="uplCadastroPessoa" runat="server" ClientInstanceName="uplCadastroPessoa"
			                                            OnFileUploadComplete="uplCadastroPessoa_FileUploadComplete" 
			                                            CssClass="labelNormal" FileUploadMode="OnPageLoad">
		                                             <ValidationSettings MaxFileSize="100000000" />
		                                             <ClientSideEvents FileUploadComplete="function(s, e) { Uploader_OnUploadComplete(e); }" />
		                                        </dxuc:ASPxUploadControl>   	                                        
		                                        
		                                        <asp:LinkButton ID="btnCadastroClienteCarteira" runat="server" OnClick="DownloadPlanilhaModeloImplantacao_Click" ToolTip="Planilha de Modelo Cadastro Cliente/Carteira" BorderStyle="None">
                                                <img alt="" src="../imagens/document-excel.png" />
                                                </asp:LinkButton>
		                                        
		                                        <asp:Label ID="lblCadastroClienteCarteira" runat="server" CssClass="labelNormal labelFloat" Text="Cadastro Cliente/Carteira"/>        		                            
		                                        <dxuc:ASPxUploadControl ID="uplCadastroClienteCarteira" runat="server" ClientInstanceName="uplCadastroClienteCarteira"
			                                            OnFileUploadComplete="uplCadastroClienteCarteira_FileUploadComplete" 
			                                            CssClass="labelNormal" FileUploadMode="OnPageLoad">
		                                             <ValidationSettings MaxFileSize="100000000" />
		                                             <ClientSideEvents FileUploadComplete="function(s, e) { Uploader_OnUploadComplete(e); }" />
		                                        </dxuc:ASPxUploadControl>  
		                                        		                                        
		                                        <asp:LinkButton ID="btnContabRoteiro" runat="server" OnClick="DownloadPlanilhaModeloImplantacao_Click" ToolTip="Planilha de Modelo Roteiro Contábil" BorderStyle="None">
                                                <img alt="" src="../imagens/document-excel.png" />
                                                </asp:LinkButton>
		                                        
		                                        <asp:Label ID="label37" runat="server" CssClass="labelNormal labelFloat" Text="Roteiro Contábil"/>
		                                        <dxuc:ASPxUploadControl ID="uplContabRoteiro" runat="server" ClientInstanceName="uplContabRoteiro"
			                                            OnFileUploadComplete="uplContabRoteiro_FileUploadComplete" 
			                                            CssClass="labelNormal" FileUploadMode="OnPageLoad">
		                                             <ValidationSettings MaxFileSize="100000000" />
		                                             <ClientSideEvents FileUploadComplete="function(s, e) { Uploader_OnUploadComplete(e); }" />
		                                        </dxuc:ASPxUploadControl>  
		                                        
		                                        <asp:LinkButton ID="btnCadastroCotistaComIdPessoa" runat="server" OnClick="DownloadPlanilhaModeloDiario_Click" ToolTip="Planilha de Modelo Cadastro Cotista" BorderStyle="None">
                                                <img alt="" src="../imagens/document-excel.png" />
                                                </asp:LinkButton>
		                                        
		                                        <asp:Label ID="label36" runat="server" CssClass="labelNormal labelFloat" Text="Cadastro Cotista"/>        		                            
		                                        <dxuc:ASPxUploadControl ID="uplCadastroCotistaComIdPessoa" runat="server" ClientInstanceName="uplCadastroCotistaComIdPessoa"
			                                            OnFileUploadComplete="uplCadastroCotistaComIdPessoa_FileUploadComplete" 
			                                            CssClass="labelNormal" FileUploadMode="OnPageLoad">
		                                             <ValidationSettings MaxFileSize="100000000" />
		                                             <ClientSideEvents FileUploadComplete="function(s, e) { Uploader_OnUploadComplete(e); }" />
		                                        </dxuc:ASPxUploadControl>
		                                        		                                        
                                            </div>
	                                        
	                                        <div class="formColumn">

                                                <asp:LinkButton ID="btnTituloRendaFixa" runat="server" OnClick="DownloadPlanilhaModeloImplantacao_Click" ToolTip="Planilha de Modelo Titulo Renda Fixa" BorderStyle="None">
                                                <img alt="" src="../imagens/document-excel.png" />
                                                                </asp:LinkButton>
                                                                <asp:Label ID="label42" runat="server" CssClass="labelNormal labelFloat" Text="Titulo Renda Fixa" />
                                                                <dxuc:ASPxUploadControl ID="uplTituloRendaFixa" runat="server" ClientInstanceName="uplTituloRendaFixa"
                                                                    OnFileUploadComplete="uplTituloRendaFixa_FileUploadComplete" CssClass="labelNormal"
                                                                    FileUploadMode="OnPageLoad">
                                                                    <ValidationSettings MaxFileSize="100000000" />
                                                                    <ClientSideEvents FileUploadComplete="function(s, e) { Uploader_OnUploadComplete(e); }" />
                                                                </dxuc:ASPxUploadControl>
                                                                
                                                <asp:LinkButton ID="btnEmissor" runat="server" OnClick="DownloadPlanilhaModeloImplantacao_Click"
                                                    ToolTip="Planilha de Modelo Emissor" BorderStyle="None">
                                                <img alt="" src="../imagens/document-excel.png" />
                                                                </asp:LinkButton>
                                                                <asp:Label ID="label56" runat="server" CssClass="labelNormal labelFloat" Text="Emissor" />
                                                                <dxuc:ASPxUploadControl ID="uplEmissor" runat="server" ClientInstanceName="uplEmissor"
                                                                    OnFileUploadComplete="uplEmissor_FileUploadComplete" CssClass="labelNormal" FileUploadMode="OnPageLoad">
                                                                    <ValidationSettings MaxFileSize="100000000" />
                                                                    <ClientSideEvents FileUploadComplete="function(s, e) { Uploader_OnUploadComplete(e); }" />
                                                                </dxuc:ASPxUploadControl>
                                                                
                                                <asp:LinkButton ID="btnAgendaEventos" runat="server" OnClick="DownloadPlanilhaModeloImplantacao_Click"
                                                    ToolTip="Planilha de Modelo Agenda de Eventos (Renda Fixa)" BorderStyle="None">
                                                <img alt="" src="../imagens/document-excel.png" />
                                                                </asp:LinkButton>
                                                                <asp:Label ID="label48" runat="server" CssClass="labelNormal labelFloat" Text="Agenda de Eventos (Renda Fixa)" />
                                                                <dxuc:ASPxUploadControl ID="uplAgendaEventos" runat="server" ClientInstanceName="uplAgendaEventos"
                                                                    OnFileUploadComplete="uplAgendaEventos_FileUploadComplete" CssClass="labelNormal"
                                                                    FileUploadMode="OnPageLoad">
                                                                    <ValidationSettings MaxFileSize="100000000" />
                                                                    <ClientSideEvents FileUploadComplete="function(s, e) { Uploader_OnUploadComplete(e); }" />
                                                                </dxuc:ASPxUploadControl>
                                                                
                                                <asp:LinkButton ID="btnLiquidacao" runat="server" OnClick="DownloadPlanilhaModeloImplantacao_Click"
                                                    ToolTip="Planilha de Modelo Liquidação" BorderStyle="None">
                                                <img alt="" src="../imagens/document-excel.png" />
                                                                </asp:LinkButton>
                                                                <asp:Label ID="label34" runat="server" CssClass="labelNormal labelFloat" Text="Liquidação" />
                                                                <dxuc:ASPxUploadControl ID="uplLiquidacao" runat="server" ClientInstanceName="uplLiquidacao"
                                                                    OnFileUploadComplete="uplLiquidacao_FileUploadComplete" CssClass="labelNormal"
                                                                    FileUploadMode="OnPageLoad">
                                                                    <ValidationSettings MaxFileSize="100000000" />
                                                                    <ClientSideEvents FileUploadComplete="function(s, e) { Uploader_OnUploadComplete(e); }" />
                                                                </dxuc:ASPxUploadControl>
                                                                
                                                <asp:LinkButton ID="btnApuracaoIR" runat="server" OnClick="DownloadPlanilhaModeloImplantacao_Click"
                                                    ToolTip="Planilha de Modelo Apuração IR" BorderStyle="None">
                                                <img alt="" src="../imagens/document-excel.png" />
                                                                </asp:LinkButton>
                                                                <asp:Label ID="label35" runat="server" CssClass="labelNormal labelFloat" Text="Apuração IR - RV" />
                                                                <dxuc:ASPxUploadControl ID="uplApuracaoIR" runat="server" ClientInstanceName="uplApuracaoIR"
                                                                    OnFileUploadComplete="uplApuracaoIR_FileUploadComplete" CssClass="labelNormal"
                                                                    FileUploadMode="OnPageLoad">
                                                                    <ValidationSettings MaxFileSize="100000000" />
                                                                    <ClientSideEvents FileUploadComplete="function(s, e) { Uploader_OnUploadComplete(e); }" />
                                                                </dxuc:ASPxUploadControl>
                                                                
                                                <asp:LinkButton ID="btnCalculoAdministracao" runat="server" OnClick="DownloadPlanilhaModeloImplantacao_Click"
                                                    ToolTip="Planilha de Modelo Cálculo Administração" BorderStyle="None">
                                                <img alt="" src="../imagens/document-excel.png" />
                                                                </asp:LinkButton>
                                                                <asp:Label ID="label30" runat="server" CssClass="labelNormal labelFloat" Text="Cálc. Administração" />
                                                                <dxuc:ASPxUploadControl ID="uplCalculoAdministracao" runat="server" ClientInstanceName="uplCalculoAdministracao"
                                                                    OnFileUploadComplete="uplCalculoAdministracao_FileUploadComplete" CssClass="labelNormal"
                                                                    FileUploadMode="OnPageLoad">
                                                                    <ValidationSettings MaxFileSize="100000000" />
                                                                    <ClientSideEvents FileUploadComplete="function(s, e) { Uploader_OnUploadComplete(e); }" />
                                                                </dxuc:ASPxUploadControl>
                                                <asp:LinkButton ID="btnCalculoProvisao" runat="server" OnClick="DownloadPlanilhaModeloImplantacao_Click"
                                                    ToolTip="Planilha de Modelo Cálculo Provisão" BorderStyle="None">
                                                <img alt="" src="../imagens/document-excel.png" />
                                                                </asp:LinkButton>
                                                                <asp:Label ID="label31" runat="server" CssClass="labelNormal labelFloat" Text="Cálc. Provisão" />
                                                                <dxuc:ASPxUploadControl ID="uplCalculoProvisao" runat="server" ClientInstanceName="uplCalculoProvisao"
                                                                    OnFileUploadComplete="uplCalculoProvisao_FileUploadComplete" CssClass="labelNormal"
                                                                    FileUploadMode="OnPageLoad">
                                                                    <ValidationSettings MaxFileSize="100000000" />
                                                                    <ClientSideEvents FileUploadComplete="function(s, e) { Uploader_OnUploadComplete(e); }" />
                                                                </dxuc:ASPxUploadControl>
                                                                
                                                <asp:LinkButton ID="btnCalculoPerformance" runat="server" OnClick="DownloadPlanilhaModeloImplantacao_Click"
                                                    ToolTip="Planilha de Modelo Cálculo Performance" BorderStyle="None">
                                                <img alt="" src="../imagens/document-excel.png" />
                                                                </asp:LinkButton>
                                                                <asp:Label ID="label32" runat="server" CssClass="labelNormal labelFloat" Text="Cálc. Performance " />
                                                                <dxuc:ASPxUploadControl ID="uplCalculoPerformance" runat="server" ClientInstanceName="uplCalculoPerformance"
                                                                    OnFileUploadComplete="uplCalculoPerformance_FileUploadComplete" CssClass="labelNormal"
                                                                    FileUploadMode="OnPageLoad">
                                                                    <ValidationSettings MaxFileSize="100000000" />
                                                                    <ClientSideEvents FileUploadComplete="function(s, e) { Uploader_OnUploadComplete(e); }" />
                                                                </dxuc:ASPxUploadControl>
                                                                
                                                <asp:LinkButton ID="btnPrejuizoCotista" runat="server" OnClick="DownloadPlanilhaModeloImplantacao_Click"
                                                    ToolTip="Planilha de Modelo Prejuízo Cotista" BorderStyle="None">
                                                <img alt="" src="../imagens/document-excel.png" />
                                                                </asp:LinkButton>
                                                                <asp:Label ID="label33" runat="server" CssClass="labelNormal labelFloat" Text="Prejuízo Compensar (Fundo)" />
                                                                <dxuc:ASPxUploadControl ID="uplPrejuizoCotista" runat="server" ClientInstanceName="uplPrejuizoCotista"
                                                                    OnFileUploadComplete="uplPrejuizoCotista_FileUploadComplete" CssClass="labelNormal"
                                                                    FileUploadMode="OnPageLoad">
                                                                    <ValidationSettings MaxFileSize="100000000" />
                                                                    <ClientSideEvents FileUploadComplete="function(s, e) { Uploader_OnUploadComplete(e); }" />
                                                                </dxuc:ASPxUploadControl>
                                                                
                                                <asp:LinkButton ID="btnContabConta" runat="server" OnClick="DownloadPlanilhaModeloImplantacao_Click"
                                                    ToolTip="Planilha de Modelo Plano de Contas" BorderStyle="None">
                                                <img alt="" src="../imagens/document-excel.png" />
                                                                </asp:LinkButton>
                                                                <asp:Label ID="label26" runat="server" CssClass="labelNormal labelFloat" Text="Plano de Contas" />
                                                                <dxuc:ASPxUploadControl ID="uplContabConta" runat="server" ClientInstanceName="uplContabConta"
                                                                    OnFileUploadComplete="uplContabConta_FileUploadComplete" CssClass="labelNormal"
                                                                    FileUploadMode="OnPageLoad">
                                                                    <ValidationSettings MaxFileSize="100000000" />
                                                                    <ClientSideEvents FileUploadComplete="function(s, e) { Uploader_OnUploadComplete(e); }" />
                                                                </dxuc:ASPxUploadControl>
                                                                
                                                <asp:LinkButton ID="btnContaCorrente" runat="server" OnClick="DownloadPlanilhaModeloImplantacao_Click"
                                                    ToolTip="Planilha de Conta Corrente" BorderStyle="None">
                                                <img alt="" src="../imagens/document-excel.png" />
                                                                </asp:LinkButton>
                		                                        
		                                                        <asp:Label ID="label64" runat="server" CssClass="labelNormal labelFloat" Text="Conta Corrente"/>
		                                                        <dxuc:ASPxUploadControl ID="uplContaCorrente" runat="server" ClientInstanceName="uplContaCorrente"
			                                                            OnFileUploadComplete="uplContaCorrente_FileUploadComplete" 
			                                                            CssClass="labelNormal" FileUploadMode="OnPageLoad">
		                                                             <ValidationSettings MaxFileSize="100000000" />
		                                                             <ClientSideEvents FileUploadComplete="function(s, e) { Uploader_OnUploadComplete(e); }" />
		                                                        </dxuc:ASPxUploadControl> 
                		                                        
                                                <asp:LinkButton ID="btnAgendaEventosFundos" runat="server" OnClick="DownloadPlanilhaModeloImplantacao_Click" 
                                                    ToolTip="Planilha de Modelo Agenda de Eventos (Fundos)" BorderStyle="None">
                                                <img alt="" src="../imagens/document-excel.png" />
                                                                </asp:LinkButton>

		                                                        <asp:Label ID="label79" runat="server" CssClass="labelNormal labelFloat" Text="Agenda de Eventos (Fundos)"/>
		                                                        <dxuc:ASPxUploadControl ID="uplAgendaEventosFundo" runat="server" ClientInstanceName="uplAgendaEventosFundo"
			                                                            OnFileUploadComplete="uplAgendaEventosFundo_FileUploadComplete" 
			                                                            CssClass="labelNormal" FileUploadMode="OnPageLoad">
		                                                             <ValidationSettings MaxFileSize="100000000" />
		                                                             <ClientSideEvents FileUploadComplete="function(s, e) { Uploader_OnUploadComplete(e); }" />
		                                                        </dxuc:ASPxUploadControl>
		                                        		                                        
                                            </div>
	                                </div>       

                                        </dxw:ContentControl></ContentCollection>
                                    </dxtc:TabPage>     
                                 </TabPages>
                                 
                                 <TabPages>
                                    <dxtc:TabPage Text="Importação - Outros">
                                        <ContentCollection><dxw:ContentControl runat="server">
                                
	                                    <div class="innerPanel" id="innerPanel_TabImplantacaoOutros">
                                            
                                            <table>
                                                <tr>
                                                <td>
                                                    <div class="linkButton linkButtonNoBorder" style="margin-left: 40px; margin-top: 20px">
                                                        <asp:LinkButton ID="LinkButton1" runat="server" Font-Overline="false" CssClass="btnRun"
                                                            OnClientClick="
                                                                    {
                                                                        if(!ChecaExcelImplantacaoOutros()) {
                                                                            alert('Escolha algum arquivo para importação!');                                                                            
                                                                        }
                                                                        else {                                                                    
                                                                            callbackExcelImplantacaoOutros.SendCallback();                                                                             
                                                                        }
                                                                        return false;
                                                                    }
                                                                    ">
                                                                                <asp:Literal ID="Literal8" runat="server" Text="Importar" /><div>
                                                                                </div>
                                                                            </asp:LinkButton>
                                                                            <asp:LinkButton ID="LinkButton2" runat="server" Font-Overline="false" CssClass="btnClear"
                                                                                OnClientClick="
                                                                    {
                                                                        LimparUploadsExcelImplantacaoOutros();
                                                                        return false;
                                                                    }
                                                                    ">
                                                                                <asp:Literal ID="Literal9" runat="server" Text="Limpar" /><div>
                                                                                </div>
                                                                            </asp:LinkButton>
                                                                        </div>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                            <div class="clear">
                                                            </div>
                                                            <div class="formColumn formColumnFirst">
                                                                <asp:Label ID="label59" runat="server" CssClass="labelNormal labelFloat" Text="YMF" />
                                                                <dxuc:ASPxUploadControl ID="uplYMF" runat="server" ClientInstanceName="uplYMF" OnFileUploadComplete="uplYMF_FileUploadComplete"
                                                                    CssClass="labelNormal" FileUploadMode="OnPageLoad">
                                                                    <ValidationSettings MaxFileSize="100000000" />
                                                                    <ClientSideEvents FileUploadComplete="function(s, e) { Uploader_OnUploadComplete(e); }" />
                                                                </dxuc:ASPxUploadControl>
                                                                <asp:Label ID="label45" runat="server" CssClass="labelNormal labelFloat" Text="EICL" />
                                                                <dxuc:ASPxUploadControl ID="uplEICL" runat="server" ClientInstanceName="uplEICL"
                                                                    OnFileUploadComplete="uplEICL_FileUploadComplete" CssClass="labelNormal" FileUploadMode="OnPageLoad">
                                                                    <ValidationSettings MaxFileSize="100000000" />
                                                                    <ClientSideEvents FileUploadComplete="function(s, e) { Uploader_OnUploadComplete(e); }" />
                                                                </dxuc:ASPxUploadControl>
                                                                <asp:Label ID="label49" runat="server" CssClass="labelNormal labelFloat" Text="Calculadora IR" />
                                                                <dxuc:ASPxUploadControl ID="uplIRTema" runat="server" ClientInstanceName="uplIRTema"
                                                                    OnFileUploadComplete="uplIRTema_FileUploadComplete" CssClass="labelNormal" FileUploadMode="OnPageLoad">
                                                                    <ValidationSettings MaxFileSize="100000000" />
                                                                    <ClientSideEvents FileUploadComplete="function(s, e) { Uploader_OnUploadComplete(e); }" />
                                                                </dxuc:ASPxUploadControl>
                                                                <asp:Label ID="label61" runat="server" CssClass="labelNormal labelFloat" Text="Ajuste YMF2" />
                                                                <dxuc:ASPxUploadControl ID="uplYMFFIX" runat="server" ClientInstanceName="uplYMFFIX"
                                                                    OnFileUploadComplete="uplYMFFIX_FileUploadComplete" CssClass="labelNormal" FileUploadMode="OnPageLoad">
                                                                    <ValidationSettings MaxFileSize="100000000" />
                                                                    <ClientSideEvents FileUploadComplete="function(s, e) { Uploader_OnUploadComplete(e); }" />
                                                                </dxuc:ASPxUploadControl>
                                                            </div>
                                                            <div class="formColumn">
                                                                <asp:Label ID="label52" runat="server" CssClass="labelNormal labelFloat" Text="Posição Fundo Analítico (SMA)" />
                                                                <dxuc:ASPxUploadControl ID="uplPosicaoSMAFundo" runat="server" ClientInstanceName="uplPosicaoSMAFundo"
                                                                    OnFileUploadComplete="uplPosicaoSMAFundo_FileUploadComplete" CssClass="labelNormal"
                                                                    FileUploadMode="OnPageLoad">
                                                                    <ValidationSettings MaxFileSize="100000000" />
                                                                    <ClientSideEvents FileUploadComplete="function(s, e) { Uploader_OnUploadComplete(e); }" />
                                                                </dxuc:ASPxUploadControl>
                                                                <asp:Label ID="label53" runat="server" CssClass="labelNormal labelFloat" Text="Movimento Fundo (SMA)" />
                                                                <dxuc:ASPxUploadControl ID="uplMovimentoSMAFundo" runat="server" ClientInstanceName="uplMovimentoSMAFundo"
                                                                    OnFileUploadComplete="uplMovimentoSMAFundo_FileUploadComplete" CssClass="labelNormal"
                                                                    FileUploadMode="OnPageLoad">
                                                                    <ValidationSettings MaxFileSize="100000000" />
                                                                    <ClientSideEvents FileUploadComplete="function(s, e) { Uploader_OnUploadComplete(e); }" />
                                                                </dxuc:ASPxUploadControl>
                                                                <asp:Label ID="label540" runat="server" CssClass="labelNormal labelFloat" Text="Posição XML Anbima" />
                                                                <dxuc:ASPxUploadControl ID="uplPosicaoXMLAnbid" runat="server" ClientInstanceName="uplPosicaoXMLAnbid"
                                                                    OnFileUploadComplete="uplPosicaoXMLAnbid_FileUploadComplete" CssClass="labelNormal"
                                                                    FileUploadMode="OnPageLoad">
                                                                    <ValidationSettings MaxFileSize="100000000" />
                                                                    <ClientSideEvents FileUploadComplete="function(s, e) { Uploader_OnUploadComplete(e); }" />
                                                                </dxuc:ASPxUploadControl>
                                                                <asp:Label ID="label63" runat="server" CssClass="labelNormal labelFloat" Text="XML Cliente(Mellon)" />
                                                                <dxuc:ASPxUploadControl ID="uplXMLClienteMellon" runat="server" ClientInstanceName="uplXMLClienteMellon"
                                                                    OnFileUploadComplete="uplXMLClienteMellon_FileUploadComplete" CssClass="labelNormal"
                                                                    FileUploadMode="OnPageLoad">
                                                                    <ValidationSettings MaxFileSize="100000000" />
                                                                    <ClientSideEvents FileUploadComplete="function(s, e) { Uploader_OnUploadComplete(e); }" />
                                                                </dxuc:ASPxUploadControl>
                                                                <asp:LinkButton ID="btnInfoComplementar" runat="server" OnClick="DownloadPlanilhaModeloImplantacao_Click"
                                                                    ToolTip="Planilha de Modelo Informações Complementares Fundo" BorderStyle="None">
                                                <img alt="" src="../imagens/document-excel.png" />
                                                                </asp:LinkButton>
                                                                <asp:Label ID="label65" runat="server" CssClass="labelNormal labelFloat" Text="Informações Complementares Fundo" />
                                                                <dxuc:ASPxUploadControl ID="uplInfoComplementarFundo" runat="server" ClientInstanceName="uplInfoComplementarFundo"
                                                                    OnFileUploadComplete="uplInfoComplementarFundo_FileUploadComplete" CssClass="labelNormal"
                                                                    FileUploadMode="OnPageLoad">
                                                                    <ValidationSettings MaxFileSize="100000000" />
                                                                    <ClientSideEvents FileUploadComplete="function(s, e) { Uploader_OnUploadComplete(e); }" />
                                                                </dxuc:ASPxUploadControl>
                                                                <asp:LinkButton ID="btnTabelaPerfilMensal" runat="server" OnClick="DownloadPlanilhaModeloImplantacao_Click"
                                                                    ToolTip="Planilha de Modelo Perfil Mensal" BorderStyle="None">
                                                <img alt="" src="../imagens/document-excel.png" />
                                                                </asp:LinkButton>
                                                                <asp:Label ID="label66" runat="server" CssClass="labelNormal labelFloat" Text="Tabela Perfil Mensal" />
                                                                <dxuc:ASPxUploadControl ID="uplTabelaPerfilMensal" runat="server" ClientInstanceName="uplTabelaPerfilMensal"
                                                                    OnFileUploadComplete="uplTabelaPerfilMensal_FileUploadComplete" CssClass="labelNormal"
                                                                    FileUploadMode="OnPageLoad">
                                                                    <ValidationSettings MaxFileSize="100000000" />
                                                                    <ClientSideEvents FileUploadComplete="function(s, e) { Uploader_OnUploadComplete(e); }" />
                                                                </dxuc:ASPxUploadControl>
                                                                
                                                                 <asp:LinkButton ID="btnTabelaInformeDesempenho" runat="server" OnClick="DownloadPlanilhaModeloImplantacao_Click"
                                                                    ToolTip="Planilha de Modelo Tabela Informe Desempenho" BorderStyle="None">
                                                                <img alt="" src="../imagens/document-excel.png" />
                                                                </asp:LinkButton>
                                                                
                                                                <asp:Label ID="label77" runat="server" CssClass="labelNormal labelFloat" Text="Tabela Informe Desempenho" />
                                                                <dxuc:ASPxUploadControl ID="uplInformeDesempenho" runat="server" ClientInstanceName="uplInformeDesempenho"
                                                                    OnFileUploadComplete="uplInformeDesempenho_FileUploadComplete" CssClass="labelNormal"
                                                                    FileUploadMode="OnPageLoad">
                                                                    <ValidationSettings MaxFileSize="100000000" />
                                                                    <ClientSideEvents FileUploadComplete="function(s, e) { Uploader_OnUploadComplete(e); }" />
                                                                </dxuc:ASPxUploadControl>
                                                                
                                                            </div>
                                                        </div>
                                                    </dxw:ContentControl>
                                                </ContentCollection>
                                            </dxtc:TabPage>
                                        </TabPages>
                                        <TabPages>
                                            <dxtc:TabPage Name="TabGalgo" Text="Galgo">
                                                <ContentCollection>
                                                    <dxw:ContentControl runat="server">
                                                        <div class="divDataGrid" id="innerPanel_TabGalgo">
                                                            <dxwgv:ASPxGridView ID="gridConsulta" runat="server" EnableCallBacks="true" ClientInstanceName="gridConsulta"
                                                                AutoGenerateColumns="False" DataSourceID="EsDSClienteGalgo" KeyFieldName="IdCliente"
                                                                OnHtmlRowCreated="gridConsulta_HtmlRowCreated" Width="850px" OnCustomUnboundColumnData="gridConsulta_CustomUnboundColumnData"
                                                                OnCustomCallback="gridConsulta_CustomCallback">
                                                                <Columns>
                                                                    <dxwgv:GridViewCommandColumn ShowSelectCheckbox="True" VisibleIndex="0" Width="5%" ButtonType="Image" ShowClearFilterButton="True">
                                                                        <HeaderStyle HorizontalAlign="Center"/>
                                                                        <HeaderTemplate>
                                                                            <dxe:ASPxCheckBox ID="cbAll" ClientInstanceName="cbAll" runat="server" ToolTip="Seleciona todos os CheckBoxs de todas as páginas" BackColor="white" ClientSideEvents-CheckedChanged="function(s, e) {OnAllCheckedChanged(s, e, gridConsulta);}" OnInit="CheckBoxSelectAll"/>
                                                                        </HeaderTemplate>
                                                                    </dxwgv:GridViewCommandColumn>
                                                                    <dxwgv:GridViewDataTextColumn FieldName="IdCliente" ReadOnly="True" VisibleIndex="1"
                                                                        Width="7%" CellStyle-HorizontalAlign="left">
                                                                        <CellStyle HorizontalAlign="Left">
                                                                        </CellStyle>
                                                                    </dxwgv:GridViewDataTextColumn>
                                                                    <dxwgv:GridViewDataTextColumn FieldName="Apelido" VisibleIndex="2" Width="25%" Settings-AutoFilterCondition="Contains">
                                                                        <Settings AutoFilterCondition="Contains"></Settings>
                                                                    </dxwgv:GridViewDataTextColumn>
                                                                    <dxwgv:GridViewDataDateColumn FieldName="DataDia" VisibleIndex="3" Width="9%" />
                                                                    <dxwgv:GridViewDataTextColumn FieldName="StatusDescricao" Caption="Status" UnboundType="String"
                                                                        VisibleIndex="4" Width="12%" Settings-AutoFilterCondition="Contains">
                                                                        <Settings AutoFilterCondition="Contains"></Settings>
                                                                    </dxwgv:GridViewDataTextColumn>
                                                                    <dxwgv:GridViewDataTextColumn FieldName="Status" Visible="False" />
                                                                    <dxwgv:GridViewDataComboBoxColumn Caption="Tipo Cliente" FieldName="IdTipo" VisibleIndex="5"
                                                                        Width="15%">
                                                                        <EditFormSettings Visible="False" />
                                                                        <PropertiesComboBox DataSourceID="EsDSTipoCliente" TextField="Descricao" ValueField="IdTipo" />
                                                                    </dxwgv:GridViewDataComboBoxColumn>
                                                                    <dxwgv:GridViewDataComboBoxColumn Caption="Ativo" FieldName="StatusAtivo" VisibleIndex="8"
                                                                        Width="5%" ExportWidth="100">
                                                                        <EditFormSettings Visible="False" />
                                                                        <PropertiesComboBox EncodeHtml="false">
                                                                            <Items>
                                                                                <dxe:ListEditItem Value="1" Text="<div title='Sim'>Sim</div>" />
                                                                                <dxe:ListEditItem Value="2" Text="<div title='Não'>Não</div>" />
                                                                            </Items>
                                                                        </PropertiesComboBox>
                                                                    </dxwgv:GridViewDataComboBoxColumn>
                                                                </Columns>
                                                                <SettingsPager Mode="ShowAllRecords">
                                                                </SettingsPager>
                                                                <Settings ShowFilterRow="True" ShowStatusBar="Visible" VerticalScrollBarMode="Visible" />
                                                                <SettingsText EmptyDataRow="0 registros" />
                                                                <Images />
                                                                <Styles>
                                                                    <Header ImageSpacing="5px" SortingImageSpacing="5px" />
                                                                    <AlternatingRow Enabled="True" />
                                                                    <Cell Wrap="False" />
                                                                </Styles>
                                                                <SettingsCommandButton>
                                                                    <ClearFilterButton Image-Url="../imagens/funnel--minus.png">
                                                                        <Image Url="../imagens/funnel--minus.png">
                                                                        </Image>
                                                                    </ClearFilterButton>
                                                                </SettingsCommandButton>
                                                            </dxwgv:ASPxGridView>
                                                            <table border="0">
                                                                <tr>
                                                                    <td style="width: 50px;">
                                                                        <asp:Label ID="label72" runat="server" CssClass="labelRequired labelFloat" Text="PL/Cota - Arquivo:" />
                                                                    </td>
                                                                    <td>
                                                                    </td>
                                                                    <td>
                                                                        <dxuc:ASPxUploadControl ID="uplPlCotaGalgo" runat="server" ClientInstanceName="uplPlCotaGalgo"
                                                                            OnFileUploadComplete="uplPlCotaGalgo_FileUploadComplete" CssClass="labelNormal"
                                                                            FileUploadMode="OnPageLoad">
                                                                            <ValidationSettings MaxFileSize="100000000" />
                                                                            <ClientSideEvents FileUploadComplete="function(s, e) { Uploader_OnUploadComplete(e); }" />
                                                                        </dxuc:ASPxUploadControl>
                                                                    </td>
                                                                    <td></td>
                                                                    <td>
                                                                        <div class="linkButton linkButtonNoBorder" style="margin-left: 40px; margin-top: 20px">
                                                                            <asp:LinkButton ID="LinkButton3" runat="server" Font-Overline="false" CssClass="btnRun"
                                                                                OnClientClick="
                                                                                                                        {
                                                                                                                            if(!ChecaGalgo()) {
                                                                                                                                alert('Escolha algum arquivo para importação!');
                                                                                                                            }
                                                                                                                            else {
                                                                                                                                callbackGalgoPlCotaArquivo.SendCallback();
                                                                                                                            }
                                                                                                                            return false;
                                                                                                                        }
                                                                                                                        ">
                                                                                <asp:Literal ID="Literal10" runat="server" Text="Importar" /><div>
                                                                                </div>
                                                                            </asp:LinkButton>
                                                                        </div>
                                                                    </td>
                                                                </tr>
                                                                </table>
                                                                <table border="0">
                                                                <tr>
                                                                    <td>
                                                                        <asp:Label ID="label74" runat="server" CssClass="labelRequired labelFloat" Text="PL/Cota - WebService:" />
                                                                    </td>
                                                                    <td class="td_Label" style="width: 50px;">
                                                                        <asp:Label ID="label75" runat="server" CssClass="labelRequired" Text="Ínicio:" />
                                                                    </td>
                                                                    <td>
                                                                        <dxe:ASPxDateEdit ID="textDataInicio_Periodo" runat="server" ClientInstanceName="textDataInicio_Periodo" />
                                                                    </td>
                                                                    <td style="width: 35px;" class="td_Label">
                                                                        <asp:Label ID="label76" runat="server" CssClass="labelRequired" Text="Fim:"></asp:Label>
                                                                    </td>
                                                                    <td>
                                                                        <dxe:ASPxDateEdit ID="textDataFim_Periodo" runat="server" ClientInstanceName="textDataFim_Periodo" />
                                                                    </td>
                                                                    <td>
                                                                        <div class="linkButton linkButtonNoBorder" style="margin-left: 40px; margin-top: 20px">
                                                                            <asp:LinkButton ID="LinkButton4" runat="server" Font-Overline="false" CssClass="btnRun"
                                                                                OnClientClick="
                                                                                                                {                                                        
                                                                                                                    callbackErroGalgoPlCotaWcf.SendCallback(); return false;
                                                                                                                }                                                    
                                                                                                                ">
                                                                                <asp:Literal ID="Literal11" runat="server" Text="Importar" /><div>
                                                                                </div>
                                                                            </asp:LinkButton>
                                                                        </div>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <asp:Label ID="label1567" runat="server" CssClass="labelRequired labelFloat" Text="PL/Cota - WebService - Pendentes:" />
                                                                    </td>
                                                                    <td class="td_Label" style="width: 50px;">
                                                                        <asp:Label ID="label78" runat="server" CssClass="labelRequired" Text="Marcador:" />
                                                                    </td>
                                                                    <td>
                                                                        <dxe:ASPxSpinEdit ID="textMarcador" runat="server" CssClass="textValor_4" ClientInstanceName="textMarcador" MaxLength="12" NumberType="integer">
                                                                                        </dxe:ASPxSpinEdit>
                                                                    </td>
                                                                    <td style="width: 35px;" class="td_Label">
                                                                        
                                                                    </td>
                                                                    <td>
                                                                        
                                                                    </td>
                                                                    <td>
                                                                        <div class="linkButton linkButtonNoBorder" style="margin-left: 40px; margin-top: 20px">
                                                                            <asp:LinkButton ID="LinkButton5" runat="server" Font-Overline="false" CssClass="btnRun"
                                                                                OnClientClick="
                                                                                                                    {                                                        
                                                                                                                        callbackErroGalgoPlCotaPendentesWcf.SendCallback(); return false;
                                                                                                                    }                                                    
                                                                                                                    ">
                                                                                <asp:Literal ID="Literal12" runat="server" Text="Importar" /><div>
                                                                                </div>
                                                                            </asp:LinkButton>
                                                                        </div>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <asp:Label ID="label70" runat="server" CssClass="labelRequired labelFloat" Text="PL/Cota - WebService - Correção:" />
                                                                    </td>
                                                                    <td class="td_Label" style="width: 50px;">
                                                                        <asp:Label ID="label71" runat="server" CssClass="labelRequired" Text="Ínicio:" />
                                                                    </td>
                                                                    <td>
                                                                        <dxe:ASPxDateEdit ID="textDataInicioCorrecao_Periodo" runat="server" ClientInstanceName="textDataInicioCorrecao_Periodo" />
                                                                    </td>
                                                                    <td style="width: 35px;" class="td_Label">
                                                                        <asp:Label ID="label73" runat="server" CssClass="labelRequired" Text="Fim:"></asp:Label>
                                                                    </td>
                                                                    <td>
                                                                        <dxe:ASPxDateEdit ID="textDataFimCorrecao_Periodo" runat="server" ClientInstanceName="textDataFimCorrecao_Periodo" />
                                                                    </td>
                                                                    <td>
                                                                        <div class="linkButton linkButtonNoBorder" style="margin-left: 40px; margin-top: 20px">
                                                                            <asp:LinkButton ID="LinkButton6" runat="server" Font-Overline="false" CssClass="btnRun"
                                                                                OnClientClick="
                                                                                                                    {                                                        
                                                                                                                        callbackErroGalgoPlCotaCorrecaoWcf.SendCallback(); return false;
                                                                                                                    }                                                    
                                                                                                                    ">
                                                                                <asp:Literal ID="Literal13" runat="server" Text="Importar" /><div>
                                                                                </div>
                                                                            </asp:LinkButton>
                                                                        </div>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </div>
                                                    </dxw:ContentControl>
                                                </ContentCollection>
                                            </dxtc:TabPage>
                                        </TabPages>
                                    </dxtc:ASPxPageControl>
                                </dxp:PanelContent>
                            </PanelCollection>
                        </dxrp:ASPxRoundPanel>
                    </div>
                    <dxlp:ASPxLoadingPanel ID="LoadingPanel" runat="server" Text="Importando arquivos, aguarde..."
                        ClientInstanceName="LoadingPanel" Modal="True" />
                </div>
            </div>
        </div>
        <cc1:esDataSource ID="EsDSCliente" runat="server" OnesSelect="EsDSCliente_esSelect"
            LowLevelBind="true" />
        <cc1:esDataSource ID="EsDSClienteGalgo" runat="server" OnesSelect="EsDSClienteGalgo_esSelect"
            LowLevelBind="true" />
        <cc1:esDataSource ID="EsDSTipoCliente" runat="server" OnesSelect="EsDSTipoCliente_esSelect" />
        <dxwgv:ASPxGridView ID="gridExportacao1" runat="server" Visible="false" />
        <dxwgv:ASPxGridViewExporter ID="gridExport" runat="server" GridViewID="gridExportacao1" />
    </form>
</body>
</html>
