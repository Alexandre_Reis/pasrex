﻿using System;
using System.Web;

using System.Text;
using System.Collections.Generic;
using System.IO;
using Financial.Investidor;
using Financial.Investidor.Enums;
using DevExpress.Web;
using Financial.Util;
using Financial.WebConfigConfiguration;
using Financial.Security;
using Financial.Fundo;
using Financial.Fundo.Galgo_ServicePLCota;
using EntitySpaces.Interfaces;
using System.ServiceModel.Configuration;
using System.Linq;
using System.Web.UI.WebControls;
using System.Reflection;

public partial class CadastrosBasicos_Importacao : ImportacaoBasePage
{
    #region Events
    new protected void Page_Load(object sender, EventArgs e)
    {
        base.Page_Load(sender, e);

        foreach (DevExpress.Web.TabPage page in this.tabArquivos.TabPages)
        {
            if (page.Name == "TabGalgo")
            {
                if (ParametrosConfiguracaoSistema.Integracoes.IntegraGalgoPlCota == (int)Financial.Util.Enums.IntegraGalgoPlCota.NaoIntegra)
                    page.Visible = false;
                else
                    page.Visible = true;
            }
        }
    }

    protected void gridConsulta_HtmlRowCreated(object sender, ASPxGridViewTableRowEventArgs e)
    {
        //this.SetaCoresGrid(e);
    }

    #endregion

    #region Funções Gerais

    /// <summary>
    /// Faz Download das planilhas Modelos Excel
    /// </summary>
    /// <param name="filePath">Path do Arquivo</param>
    public void DownloadFile(string filePath)
    {
        FileInfo file = new FileInfo(HttpContext.Current.Server.MapPath(filePath));
        if (file.Exists)
        {
            HttpContext.Current.Response.Clear();
            HttpContext.Current.Response.AddHeader("Content-Disposition", "attachment; filename=" + file.Name);
            HttpContext.Current.Response.AddHeader("Content-Length", file.Length.ToString());
            HttpContext.Current.Response.ContentType = "application/octet-stream";
            HttpContext.Current.Response.WriteFile(file.FullName);
            HttpContext.Current.Response.End();
        }
    }
    #endregion

    #region Funções Arquivos Internet - 1º Aba

    /// <summary>
    /// 
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void callbackErroInternet_Callback(object source, DevExpress.Web.CallbackEventArgs e)
    {
        e.Result = "";

        if (!this.ChecksMarcadosInternet())
        {
            e.Result = "Algum arquivo deve ser marcado para importação!";
            return;
        }

        // chkOffshore/Lista Ativo CVM e chkEmissoresIsin não incluso
        CheckBox[] checks = { 
            this.chkBDIN, this.chkBDPregao, this.chkIndic, this.chkRefVol, 
            this.chkTarPar, this.chkTarPreg, this.chkTaxaSwap, this.chkAndimaMercado, this.chkIma,
            this.chkTesouro, this.chkAndima238, this.chkAndima550, this.chkIBGE,
            this.chkDebenture, this.chkAndimaIndice, this.chkASEL007        
        };

        if (this.IsCheck(checks))
        { // Dentro do Conjunto especificado define se algum CheckBox está Checado
            if (textDataInicioInternet.Text == "" || textDataFimInternet.Text == "")
            {
                e.Result = "Data deve ser selecionada!";
                return;
            }

            if (Convert.ToDateTime(textDataInicioInternet.Text) > Convert.ToDateTime(textDataFimInternet.Text))
            {
                e.Result = "Data Fim deve ser maior igual que Data Início.";
                return;
            }
        }

        string path = DiretorioAplicacao.DiretorioBaseAplicacao + WebConfig.AppSettings.DiretorioDownloads;
        DirectoryInfo dir = new DirectoryInfo(path);
        if (!dir.Exists)
        {
            e.Result = "Diretório de Download: " + dir + " não existe. Configure no WebConfig.";
            return;
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void callbackImportacaoInternet_Callback(object source, DevExpress.Web.CallbackEventArgs e)
    {
        this.ImportaArquivosInternet();
        //           
        e.Result = base.MensagemErroInternet.ToString() != "" ? base.MensagemErroInternet.ToString() : "Arquivo(s) importado(s) com sucesso.";
    }

    /// <summary>
    /// Retorna boolean Indicando se há algum Arquivo da Internet Checado
    /// </summary>
    /// <returns></returns>
    private bool ChecksMarcadosInternet()
    {
        return this.chkBDIN.Checked || this.chkBDPregao.Checked ||
               this.chkIndic.Checked || this.chkRefVol.Checked ||
               this.chkTarPar.Checked || this.chkTarPreg.Checked ||
               this.chkTaxaSwap.Checked || this.chkAndimaMercado.Checked ||
               this.chkIma.Checked || this.chkListaAtivoCVM.Checked ||
               this.chkTesouro.Checked || this.chkAndima238.Checked ||
               this.chkAndima550.Checked || this.chkOffshore.Checked ||
               this.chkIBGE.Checked || this.chkDebenture.Checked ||
               this.chkAndimaIndice.Checked || this.chkEmissoresIsin.Checked ||
               this.chkASEL007.Checked;
    }

    /// <summary>
    /// Dentro do Conjunto de CheckBoxs que deve ser feito conferência de Datas retorna se algum está checado
    /// chkOffshore/Lista Ativo CVM não incluso
    /// </summary>
    /// <param name="checkBoxs"></param>
    /// <returns></returns>
    private bool IsCheck(params CheckBox[] checkBoxs)
    {
        for (int i = 0; i < checkBoxs.Length; i++)
        {
            if (checkBoxs[i].Checked)
            {
                return true;
            }
        }
        return false;
    }

    /// <summary>
    /// 
    /// </summary>   
    private void ImportaArquivosInternet()
    {
        //string path = "E:/Downloads/"; //Settings.Default.Downloads;
        string path = DiretorioAplicacao.DiretorioBaseAplicacao + WebConfig.AppSettings.DiretorioDownloads;
        if (!path.Trim().EndsWith("\\"))
        {
            path += "\\";
        }
        path = path.Replace("\\", "\\\\");

        // Zera a mensagem de Erro
        base.MensagemErroInternet = new StringBuilder();
        //
        DateTime dataInicio = new DateTime();
        DateTime dataFim = new DateTime();
        //
        if (!String.IsNullOrEmpty(this.textDataInicioInternet.Text))
        {
            dataInicio = Convert.ToDateTime(this.textDataInicioInternet.Text);
        }
        if (!String.IsNullOrEmpty(this.textDataFimInternet.Text))
        {
            dataFim = Convert.ToDateTime(this.textDataFimInternet.Text);
        }

        //
        if (this.chkBDIN.Checked) { this.ImportaBdin(dataInicio, dataFim, path); }
        if (this.chkBDPregao.Checked) { this.ImportaBdPregao(dataInicio, dataFim, path); }
        if (this.chkIndic.Checked) { this.ImportaIndic(dataInicio, dataFim, path); }
        if (this.chkTarPar.Checked) { this.ImportaTarPar(dataInicio, dataFim, path); }
        if (this.chkAndimaIndice.Checked) { this.ImportaCotacaoAndimaIndice(dataInicio, dataFim, path); }
        if (this.chkAndimaMercado.Checked) { this.ImportaAndimaMercado(dataInicio, dataFim, path); }
        if (this.chkIma.Checked) { this.ImportaIma(dataInicio, dataFim); }
        if (this.chkTesouro.Checked) { this.ImportaCotacaoTesouro(dataInicio, dataFim, path); }
        if (this.chkIBGE.Checked) { this.ImportaCotacaoIBGE(dataInicio, dataFim, path); }
        if (this.chkDebenture.Checked) { this.ImportaCotacaoDebenture(dataInicio, dataFim, path); }
        if (this.chkOffshore.Checked) { this.ImportaOffshore(dataInicio, dataFim, path); }
        if (this.chkAndima238.Checked) { this.ImportaAndima238(dataInicio, dataFim, path); }
        if (this.chkAndima550.Checked) { this.ImportaAndima550(dataInicio, dataFim, path); }
        if (this.chkTarPreg.Checked) { this.ImportaTarPreg(dataInicio, dataFim, path); }
        if (this.chkRefVol.Checked) { this.ImportaRefVol(dataInicio, dataFim, path); }
        if (this.chkTaxaSwap.Checked) { this.ImportaTaxaSwap(dataInicio, dataFim, path); }
        if (this.chkListaAtivoCVM.Checked) { this.ImportaListaAtivoCVM(path); }
        if (this.chkEmissoresIsin.Checked) { this.ImportaEmissoresIsin(path); }
        if (this.chkASEL007.Checked) { this.ImportaASEL007(dataInicio, dataFim, path); }
        // if (this.chkPlCotaGalgoWcf.Checked) { this.ImportaPlCotaGalgoWcf(path); }

        //
        // Se Ocorreu erro em alguma importação Concatena mensagem Concluida
        if (base.MensagemErroInternet.ToString() != "")
        {
            base.MensagemErroInternet.AppendLine();
            base.MensagemErroInternet.AppendLine("-----------------------------------------------------------------------------------------");
            base.MensagemErroInternet.AppendLine("\t\t\t\tImportação concluída");
        }
    }

    #endregion

    #region Funções Arquivos Diários - 2º Aba

    /// <summary>
    /// 
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void callbackArquivos_Callback(object source, DevExpress.Web.CallbackEventArgs e)
    {
        e.Result = "";

        if (textDataArquivos.Text == "")
        {
            e.Result = "Data deve ser selecionada!";
            return;
        }
    }

    protected void callbackMultiSelectArquivos_Callback(object source, DevExpress.Web.CallbackEventArgs e)
    {
        e.Result = "";

        if (textDataMultiArquivos.Text == "")
        {
            e.Result = "Data deve ser selecionada!";
            return;
        }
    }

    /// <summary>
    /// De acordo com o Controle de Upload da aba de Arquivos Diarios define A função a ser chamada
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void uplImportacaoDiarios_FileUploadComplete(object sender, FileUploadCompleteEventArgs e)
    {
        // de acordo com o sender chama a função correta
        ASPxUploadControl s = sender as ASPxUploadControl;

        bool geraExceptionPROD = true;
        if (checkIgnoraPROD.Checked)
        {
            geraExceptionPROD = false;
        }

        switch (s.ID)
        {
            case "uplCDIX": base.ImportaCdix(e, this.textDataArquivos); break;
            case "uplCMDF": base.ImportaCmdf(e, this.textDataArquivos); break;
            case "uplCONR": base.ImportaConr(e, this.textDataArquivos); break;
            case "uplCONL": base.ImportaConl(e, this.textDataArquivos); break;
            case "uplDBTC": base.ImportaDbtcOperacao(e, this.textDataArquivos); break;
            case "uplDBTL": base.ImportaDbtl(e, this.textDataArquivos); break;
            case "uplPAPT": base.ImportaPapt(e, this.textDataArquivos); break;
            case "uplPROD": base.ImportaProd(e, this.textDataArquivos, geraExceptionPROD); break;
            case "uplNEGS": base.ImportaNegs(e, this.textDataArquivos); break;
            case "uplPESC": base.ImportaPesc(e, this.textDataArquivos); break;
            case "uplNotaCorretagem": base.ImportaNotaCorretagem(e, this.textDataArquivos); break;
            case "uplNotaCorretagemPDF": base.ImportaNotaCorretagemPDF(e, this.textDataArquivos); break;
            case "uplNotaCorretagemPDF_BMF": base.ImportaNotaCorretagemPDF_BMF(e, this.textDataArquivos); break;
            case "uplRComiesp": base.ImportaRComiesp(e, this.textDataArquivos); break;
            case "uplCRCA": base.ImportaCrca(e, this.textDataArquivos); break;
            case "uplPOSR": base.ImportaPosr(e, this.textDataArquivos); break;
            case "uplCSGD": base.ImportaCsgd(e, this.textDataArquivos); break;
            case "uplDBTCPosicao": base.ImportaDbtcPosicao(e, this.textDataArquivos); break;
        }
    }
    #endregion

    #region Funções Arquivos Excel - 3º Aba

    /// <summary>
    /// 
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void callbackExcel_Callback(object source, DevExpress.Web.CallbackEventArgs e)
    {
        e.Result = "";
    }

    /// <summary>
    /// De Acordo com o Id do LinkButton estabelece o path das Planilhas Excel
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void DownloadPlanilhaModeloDiario_Click(object sender, EventArgs e)
    {

        #region path das Planilhas Excel Modelos
        Dictionary<string, string> dict = new Dictionary<string, string>();
        //
        dict.Add("Path_ImportacaoOrdemBolsa", @"../Data/Diario/ImportacaoOrdemBolsa.xls");
        dict.Add("Path_ImportacaoOperacaoEmprestimoBolsa", @"../Data/Diario/ImportacaoOperacaoEmprestimoBolsa.xls");
        dict.Add("Path_ImportacaoOrdemBMF", @"../Data/Diario/ImportacaoOrdemBMF.xls");
        dict.Add("Path_ImportacaoOperacaoFundo", @"../Data/Diario/ImportacaoOperacaoFundo.xls");
        dict.Add("Path_ImportacaoOperacaoRendaFixa", @"../Data/Diario/ImportacaoOperacaoRendaFixa.xls");
        dict.Add("Path_ImpportacaoCadastroCotistas", @"../Data/Diario/ImportacaoCadastroCotistas.xls");
        dict.Add("Path_ImportacaoCotacaoBolsa", @"../Data/Diario/ImportacaoCotacaoBolsa.xls");
        dict.Add("Path_ImportacaoAtivoBolsa", @"../Data/Diario/ImportacaoAtivoBolsa.xls");
        dict.Add("Path_ImportacaoCotacaoBMF", @"../Data/Diario/ImportacaoCotacaoBMF.xls");
        dict.Add("Path_ImportacaoCotacaoIndice", @"../Data/Diario/ImportacaoCotacaoIndice.xls");
        dict.Add("Path_ImportacaoCotacaoSerie", @"../Data/Diario/ImportacaoCotacaoSerie.xls");
        dict.Add("Path_ImportacaoCotacaoAnbimaPublico", @"../Data/Diario/ImportacaoCotacaoAnbimaPublico.xls");
        dict.Add("Path_ImportacaoHistoricoCota", @"../Data/Diario/ImportacaoHistoricoCota.xls");
        dict.Add("Path_ImportacaoOperacaoCotista", @"../Data/Diario/ImportacaoOperacaoCotista.xls");
        dict.Add("Path_ImportacaoContabLancamento", @"../Data/Diario/ImportacaoContabLancamento.xls");
        dict.Add("Path_ImportacaoTaxaCurva", @"../Data/Diario/ImportacaoTaxaCurva.xls");
        dict.Add("Path_ImportacaoColagemResgates", @"../Data/Diario/ImportacaoColagemResgates.xls");
        dict.Add("Path_ImportacaoColagemComeCotas", @"../Data/Diario/ImportacaoColagemComeCotas.xls");
        dict.Add("Path_ImportacaoTransferenciaBolsa", @"../Data/Diario/ImportacaoTransferenciaBolsa.xls");
        dict.Add("Path_ImportacaoTransferenciaBMF", @"../Data/Diario/ImportacaoTransferenciaBMF.xls");
        dict.Add("Path_ImportacaoCravaCotas", @"../Data/Diario/ImportacaoCravaCota.xls");
        dict.Add("Path_ImpportacaoCadastroCotistaComIdPessoa", @"../Data/Diario/ImportacaoCadastroCotistaComIdPessoa.xls");
        dict.Add("Path_ImportacaoMTMManual", @"../Data/Diario/ImportacaoMTMManual.xls");
        dict.Add("Path_ImportacaoLiquidacaoDiaria", @"../Data/Diario/ImportacaoLiquidacaoDiaria.xls");
        //
        #endregion

        LinkButton s = sender as LinkButton;

        switch (s.ID)
        {
            case "btnOrdemBolsa": this.DownloadFile(dict["Path_ImportacaoOrdemBolsa"]); break;
            case "btnOperacaoEmprestimoBolsa": this.DownloadFile(dict["Path_ImportacaoOperacaoEmprestimoBolsa"]); break;
            case "btnOrdemBMF": this.DownloadFile(dict["Path_ImportacaoOrdemBMF"]); break;
            case "btnOperacaoFundo": this.DownloadFile(dict["Path_ImportacaoOperacaoFundo"]); break;
            case "btnOperacaoRendaFixa": this.DownloadFile(dict["Path_ImportacaoOperacaoRendaFixa"]); break;
            case "btnCadastroCotista": this.DownloadFile(dict["Path_ImpportacaoCadastroCotistas"]); break;
            case "btnCotacaoBolsa": this.DownloadFile(dict["Path_ImportacaoCotacaoBolsa"]); break;
            case "btnAtivoBolsa": this.DownloadFile(dict["Path_ImportacaoAtivoBolsa"]); break;
            case "btnCotacaoBMF": this.DownloadFile(dict["Path_ImportacaoCotacaoBMF"]); break;
            case "btnCotacaoIndice": this.DownloadFile(dict["Path_ImportacaoCotacaoIndice"]); break;
            case "btnCotacaoSerie": this.DownloadFile(dict["Path_ImportacaoCotacaoSerie"]); break;
            case "btnCotacaoAnbimaPublico": this.DownloadFile(dict["Path_ImportacaoCotacaoAnbimaPublico"]); break;
            case "btnCotacaoAnbimaDebenture": this.DownloadFile(dict["Path_ImportacaoCotacaoAnbimaDebenture"]); break;
            case "btnHistoricoCota": this.DownloadFile(dict["Path_ImportacaoHistoricoCota"]); break;
            case "btnOperacaoCotista": this.DownloadFile(dict["Path_ImportacaoOperacaoCotista"]); break;
            case "btnContabLancamento": this.DownloadFile(dict["Path_ImportacaoContabLancamento"]); break;
            case "btnTaxaCurvaRendaFixa": this.DownloadFile(dict["Path_ImportacaoTaxaCurva"]); break;
            case "btnColagemResgates": this.DownloadFile(dict["Path_ImportacaoColagemResgates"]); break;
            case "btnColagemComeCotas": this.DownloadFile(dict["Path_ImportacaoColagemComeCotas"]); break;
            case "btnTransferenciaBolsa": this.DownloadFile(dict["Path_ImportacaoTransferenciaBolsa"]); break;
            case "btnTransferenciaBMF": this.DownloadFile(dict["Path_ImportacaoTransferenciaBMF"]); break;
            case "btnCravaCota": this.DownloadFile(dict["Path_ImportacaoCravaCotas"]); break;
            case "btnCadastroCotistaComIdPessoa": this.DownloadFile(dict["Path_ImpportacaoCadastroCotistaComIdPessoa"]); break;
            case "btnMTMManual": this.DownloadFile(dict["Path_ImportacaoMTMManual"]); break;
            case "btnLiquidacaoDiaria": this.DownloadFile(dict["Path_ImportacaoLiquidacaoDiaria"]); break;
        }
    }

    #region Operações
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void uplOperacaoFundo_FileUploadComplete(object sender, FileUploadCompleteEventArgs e)
    {
        this.uplOperacaoFundo_FileUploadComplete(e, this.checkIgnora);
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void uplOperacaoCotista_FileUploadComplete(object sender, FileUploadCompleteEventArgs e)
    {
        this.uplOperacaoCotista_FileUploadComplete(e, this.checkIgnora);
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void uplOperacaoRendaFixa_FileUploadComplete(object sender, FileUploadCompleteEventArgs e)
    {
        this.uplOperacaoRendaFixa_FileUploadComplete(e, this.checkIgnora);
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void uplOrdemBolsa_FileUploadComplete(object sender, FileUploadCompleteEventArgs e)
    {
        this.uplOrdemBolsa_FileUploadComplete(e, this.checkIgnora);
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void uplOrdemBMF_FileUploadComplete(object sender, FileUploadCompleteEventArgs e)
    {
        this.uplOrdemBMF_FileUploadComplete(e, this.checkIgnora);
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void uplOperacaoEmprestimoBolsa_FileUploadComplete(object sender, FileUploadCompleteEventArgs e)
    {
        this.uplOperacaoEmprestimoBolsa_FileUploadComplete(e, this.checkIgnora);
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void uplContabLancamento_FileUploadComplete(object sender, FileUploadCompleteEventArgs e)
    {
        this.uplContabLancamento_FileUploadComplete(e, this.checkIgnora);
    }
    #endregion

    #endregion

    #region Funções Carga Excel Implantação - Posições - 4º Aba

    /// <summary>
    /// De Acordo com o Id do LinkButton estabelece o path das Planilhas Excel
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void DownloadPlanilhaModeloImplantacao_Click(object sender, EventArgs e)
    {
        #region path das Planilhas Excel Modelos
        Dictionary<string, string> dict = new Dictionary<string, string>();
        //
        dict.Add("Path_ImportacaoPosicaoBolsa", @"../Data/Implantacao/ImportacaoPosicaoBolsa.xls");
        dict.Add("Path_ImportacaoTermoBolsa", @"../Data/Implantacao/ImportacaoTermoBolsa.xls");
        dict.Add("Path_ImportacaoPosicaoEmprestimoBolsa", @"../Data/Implantacao/ImportacaoPosicaoEmprestimoBolsa.xls");
        dict.Add("Path_ImportacaoPosicaoBMF", @"../Data/Implantacao/ImportacaoPosicaoBMF.xls");
        dict.Add("Path_ImportacaoPosicaoFundo", @"../Data/Implantacao/ImportacaoPosicaoFundo.xls");
        dict.Add("Path_ImportacaoPosicaoRendaFixa", @"../Data/Implantacao/ImportacaoPosicaoRendaFixa.xls");
        dict.Add("Path_ImportacaoPosicaoSwap", @"../Data/Implantacao/ImportacaoPosicaoSwap.xls");
        dict.Add("Path_ImportacaoPrejuizoCotista", @"../Data/Implantacao/ImportacaoPrejuizoCotista.xls");
        dict.Add("Path_ImportacaoAgendaEventosRendaFixa", @"../Data/Implantacao/ImportacaoAgendaEventosRendaFixa.xls");
        //dict.Add("Path_ImportacaoAgendaEventosRendaFixaReduzida", @"Data/ImportacaoPosicaoFundo.xls");
        dict.Add("Path_ImportacaoTituloRendaFixa", @"../Data/Implantacao/ImportacaoTituloRendaFixa.xls");
        dict.Add("Path_ImportacaoEmissor", @"../Data/Implantacao/ImportacaoEmissor.xls");
        dict.Add("Path_ImportacaoLiquidacao", @"../Data/Implantacao/ImportacaoLiquidacao.xls");
        dict.Add("Path_ImportacaoApuracaoIR", @"../Data/Implantacao/ImportacaoApuracaoIR.xls");
        dict.Add("Path_ImportacaoCalcAdm", @"../Data/Implantacao/ImportacaoCalcAdm.xls");
        dict.Add("Path_ImportacaoCalcProvisao", @"../Data/Implantacao/ImportacaoCalcProvisao.xls");
        dict.Add("Path_ImportacaoCalcPfee", @"../Data/Implantacao/ImportacaoCalcPfee.xls");
        dict.Add("Path_ImportacaoPosicaoCotista", @"../Data/Implantacao/ImportacaoPosicaoCotista.xls");
        dict.Add("Path_ImportacaoPosicaoCotistaHistorico", @"../Data/Implantacao/ImportacaoPosicaoCotistaHistorico.xls");
        dict.Add("Path_ImportacaoCadastroPessoa", @"../Data/Implantacao/ImportacaoPessoa.xls");
        dict.Add("Path_ImportacaoCadastroClienteCarteira", @"../Data/Implantacao/ImportacaoClienteCarteira.xls");
        dict.Add("Path_ImportacaoContabConta", @"../Data/Implantacao/ImportacaoPlanoContas.xls");
        dict.Add("Path_ImportacaoContabRoteiro", @"../Data/Implantacao/ImportacaoRoteiroContabil.xls");
        dict.Add("Path_ImportacaoMovimentoYMF", @"../Data/Implantacao/ImportacaoMovimentoYMF.xls");
        dict.Add("Path_ImportacaoMovimentoSMA", @"../Data/Implantacao/ImportacaoMovimentoSMA.xls");
        dict.Add("Path_ImportacaoContaCorrente", @"../Data/Implantacao/ImportacaoContaCorrente.xls");
        dict.Add("Path_ImportacaoAgendaComeCotas", @"../Data/Implantacao/ImportacaoAgendaComeCotas.xls");
        dict.Add("Path_ImportacaoAgendaEventosFundos", @"../Data/Implantacao/ImportacaoAgendaEventosFundo.xls");
        dict.Add("Path_ImportacaoInfoComplementar", @"../Data/Implantacao/ImportacaoInfoComplementar.xls");
        dict.Add("Path_TabelaPerfilMensal", @"../Data/Implantacao/ImportacaoTabelaPerfil.xls");
        dict.Add("Path_TabelaInformeDesempenho", @"../Data/Implantacao/ImportacaoTabelaInformeDesempenho.xls");
        //
        #endregion

        LinkButton s = sender as LinkButton;

        switch (s.ID)
        {
            case "btnPosicaoBolsa": this.DownloadFile(dict["Path_ImportacaoPosicaoBolsa"]); break;
            case "btnPosicaoTermo": this.DownloadFile(dict["Path_ImportacaoTermoBolsa"]); break;
            case "btnPosicaoEmprestimoBolsa": this.DownloadFile(dict["Path_ImportacaoPosicaoEmprestimoBolsa"]); break;
            case "btnPosicaoBMF": this.DownloadFile(dict["Path_ImportacaoPosicaoBMF"]); break;
            case "btnPosicaoFundo": this.DownloadFile(dict["Path_ImportacaoPosicaoFundo"]); break;
            case "btnPosicaoRendaFixa": this.DownloadFile(dict["Path_ImportacaoPosicaoRendaFixa"]); break;
            case "btnPosicaoSwap": this.DownloadFile(dict["Path_ImportacaoPosicaoSwap"]); break;
            case "btnPrejuizoCotista": this.DownloadFile(dict["Path_ImportacaoPrejuizoCotista"]); break;
            case "btnAgendaEventos": this.DownloadFile(dict["Path_ImportacaoAgendaEventosRendaFixa"]); break;
            case "btnTituloRendaFixa": this.DownloadFile(dict["Path_ImportacaoTituloRendaFixa"]); break;
            case "btnEmissor": this.DownloadFile(dict["Path_ImportacaoEmissor"]); break;
            case "btnLiquidacao": this.DownloadFile(dict["Path_ImportacaoLiquidacao"]); break;
            case "btnApuracaoIR": this.DownloadFile(dict["Path_ImportacaoApuracaoIR"]); break;
            case "btnCalculoAdministracao": this.DownloadFile(dict["Path_ImportacaoCalcAdm"]); break;
            case "btnCalculoProvisao": this.DownloadFile(dict["Path_ImportacaoCalcProvisao"]); break;
            case "btnCalculoPerformance": this.DownloadFile(dict["Path_ImportacaoCalcPfee"]); break;
            case "btnPosicaoCotista": this.DownloadFile(dict["Path_ImportacaoPosicaoCotista"]); break;
            case "btnPosicaoCotistaHistorico": this.DownloadFile(dict["Path_ImportacaoPosicaoCotistaHistorico"]); break;
            case "btnCadastroPessoa": this.DownloadFile(dict["Path_ImportacaoCadastroPessoa"]); break;
            case "btnCadastroClienteCarteira": this.DownloadFile(dict["Path_ImportacaoCadastroClienteCarteira"]); break;
            case "btnContabConta": this.DownloadFile(dict["Path_ImportacaoContabConta"]); break;
            case "btnContabRoteiro": this.DownloadFile(dict["Path_ImportacaoContabRoteiro"]); break;
            case "btnMovimentoYMFFundo": this.DownloadFile(dict["Path_ImportacaoMovimentoYMF"]); break;
            case "btnMovimentoYMFCotista": this.DownloadFile(dict["Path_ImportacaoMovimentoYMF"]); break;
            case "btnMovimentoSMAFundo": this.DownloadFile(dict["Path_ImportacaoMovimentoSMA"]); break;
            case "btnPosicaoSMAFundo": this.DownloadFile(dict["Path_btnPosicaoSMAFundo"]); break;
            case "btnContaCorrente": this.DownloadFile(dict["Path_ImportacaoContaCorrente"]); break;
            case "btnAgendaComeCotas": this.DownloadFile(dict["Path_ImportacaoAgendaComeCotas"]); break;
            case "btnAgendaEventosFundos": this.DownloadFile(dict["Path_ImportacaoAgendaEventosFundos"]); break;
            case "btnInfoComplementar": this.DownloadFile(dict["Path_ImportacaoInfoComplementar"]); break;
            case "btnTabelaPerfilMensal": this.DownloadFile(dict["Path_TabelaPerfilMensal"]); break;
            case "btnTabelaInformeDesempenho": this.DownloadFile(dict["Path_TabelaInformeDesempenho"]); break;
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void callbackExcelImplantacao_Callback(object source, DevExpress.Web.CallbackEventArgs e)
    {
        e.Result = "";
    }

    #endregion

    #region Funções - 5º Aba

    protected void callbackExcelImplantacaoOutros_Callback(object source, DevExpress.Web.CallbackEventArgs e)
    {
        e.Result = "";
    }
    #endregion

    #region Funções - 6º Aba

    protected void callbackGalgoPlCotaArquivo_Callback(object source, DevExpress.Web.CallbackEventArgs e)
    {
        e.Result = "";
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void callbackErroGalgoPlCotaWcf_Callback(object source, DevExpress.Web.CallbackEventArgs e)
    {
        e.Result = "";

        if (textDataInicio_Periodo.Text == "" || textDataFim_Periodo.Text == "")
        {
            e.Result = "Data deve ser selecionada!";
            return;
        }

        if (Convert.ToDateTime(textDataInicio_Periodo.Text) > Convert.ToDateTime(textDataFim_Periodo.Text))
        {
            e.Result = "Data Fim deve ser maior igual que Data Início.";
            return;
        }

        List<object> keyValuesId = gridConsulta.GetSelectedFieldValues("IdCliente");

        if (keyValuesId.Count == 0)
        {
            e.Result = "Algum cliente deve ser selecionado!";
            return;
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void callbackErroGalgoPlCotaPendentesWcf_Callback(object source, DevExpress.Web.CallbackEventArgs e)
    {
        e.Result = "";

        if (textMarcador.Text == "")
        {
            e.Result = "O Marcador deve ser informado!";
            return;
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void callbackErroGalgoPlCotaCorrecaoWcf_Callback(object source, DevExpress.Web.CallbackEventArgs e)
    {
        e.Result = "";

        if (textDataInicioCorrecao_Periodo.Text == "" || textDataFimCorrecao_Periodo.Text == "")
        {
            e.Result = "Data deve ser selecionada!";
            return;
        }

        if (Convert.ToDateTime(textDataInicioCorrecao_Periodo.Text) > Convert.ToDateTime(textDataFimCorrecao_Periodo.Text))
        {
            e.Result = "Data Fim deve ser maior igual que Data Início.";
            return;
        }

        List<object> keyValuesId = gridConsulta.GetSelectedFieldValues("IdCliente");

        if (keyValuesId.Count == 0)
        {
            e.Result = "Algum cliente deve ser selecionado!";
            return;
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void callbackImportacaoGalgoPlCotaPendentesWcf_Callback(object source, DevExpress.Web.CallbackEventArgs e)
    {
        try
        {
            string url = System.Configuration.ConfigurationManager.AppSettings["Galgo_ServicePLCota.ServicePLCota"];

            if (string.IsNullOrEmpty(url))
            {
                e.Result = string.Format("Url do servico galgo Pl/Cota nao encontrada. (Galgo_ServicePLCota.ServicePLCota)");
                return;
            }
            List<int> idClientes = new List<int>();
            DateTime dataInicio = new DateTime();
            DateTime dataFim = new DateTime();
            long marcador = Convert.ToInt64(textMarcador.Text);

            Financial.Fundo.Galgo.InterfaceGalgo galgo = new Financial.Fundo.Galgo.InterfaceGalgo();
            galgo.ImportaPlCotaWcf(url, idClientes, dataInicio, dataFim, Financial.Util.Enums.MetodoWcfGalgo.Pendentes, marcador, HttpContext.Current.User.Identity.Name);

        }
        catch (Financial.Fundo.Exceptions.GalgoException ex)
        {
            e.Result = ex.Message;
            return;
        }
        catch (Exception ex)
        {
            e.Result = string.Format("{0}{1}", "Erro na interface com PL/Cota do Galgo: ", ex.Message);
            return;
        }

        e.Result = "Pl/Cota Galgo importado com sucesso.";
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void callbackImportacaoGalgoPlCotaWcf_Callback(object source, DevExpress.Web.CallbackEventArgs e)
    {
        // obtem a lista de clientes
        List<object> keyValuesId = gridConsulta.GetSelectedFieldValues("IdCliente");
        List<int> idClientes = keyValuesId.ConvertAll<int>(delegate(object v) { return Convert.ToInt32(v); });

        try
        {
            string url = System.Configuration.ConfigurationManager.AppSettings["Galgo_ServicePLCota.ServicePLCota"];

            if (string.IsNullOrEmpty(url))
            {
                e.Result = string.Format("Url do servico galgo Pl/Cota nao encontrada. (Galgo_ServicePLCota.ServicePLCota)");
                return;
            }

            DateTime dataInicio = new DateTime();
            DateTime dataFim = new DateTime();
            //
            if (!String.IsNullOrEmpty(this.textDataInicio_Periodo.Text))
            {
                dataInicio = Convert.ToDateTime(this.textDataInicio_Periodo.Text);
            }
            if (!String.IsNullOrEmpty(this.textDataFim_Periodo.Text))
            {
                dataFim = Convert.ToDateTime(this.textDataFim_Periodo.Text);
            }
            Financial.Fundo.Galgo.InterfaceGalgo galgo = new Financial.Fundo.Galgo.InterfaceGalgo();
            galgo.ImportaPlCotaWcf(url, idClientes, dataInicio, dataFim, Financial.Util.Enums.MetodoWcfGalgo.Consumir, 0, HttpContext.Current.User.Identity.Name);

        }
        catch (Financial.Fundo.Exceptions.GalgoException ex)
        {
            e.Result = ex.Message;
            return;
        }
        catch (Exception ex)
        {
            e.Result = string.Format("{0}{1}", "Erro na interface com PL/Cota do Galgo: ", ex.Message);
            return;
        }

        e.Result = "Pl/Cota Galgo importado com sucesso.";
    }


    /// <summary>
    /// 
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void callbackImportacaoGalgoPlCotaCorrecaoWcf_Callback(object source, DevExpress.Web.CallbackEventArgs e)
    {
        // obtem a lista de clientes
        List<object> keyValuesId = gridConsulta.GetSelectedFieldValues("IdCliente");
        List<int> idClientes = keyValuesId.ConvertAll<int>(delegate(object v) { return Convert.ToInt32(v); });

        try
        {
            string url = System.Configuration.ConfigurationManager.AppSettings["Galgo_ServicePLCota.ServicePLCota"];

            if (string.IsNullOrEmpty(url))
            {
                e.Result = string.Format("Url do servico galgo Pl/Cota nao encontrada. (Galgo_ServicePLCota.ServicePLCota)");
                return;
            }

            DateTime dataInicio = new DateTime();
            DateTime dataFim = new DateTime();
            //
            if (!String.IsNullOrEmpty(this.textDataInicioCorrecao_Periodo.Text))
            {
                dataInicio = Convert.ToDateTime(this.textDataInicioCorrecao_Periodo.Text);
            }
            if (!String.IsNullOrEmpty(this.textDataFimCorrecao_Periodo.Text))
            {
                dataFim = Convert.ToDateTime(this.textDataFimCorrecao_Periodo.Text);
            }
            Financial.Fundo.Galgo.InterfaceGalgo galgo = new Financial.Fundo.Galgo.InterfaceGalgo();
            galgo.ImportaPlCotaWcf(url, idClientes, dataInicio, dataFim, Financial.Util.Enums.MetodoWcfGalgo.Reenviados, 0, HttpContext.Current.User.Identity.Name);

        }
        catch (Financial.Fundo.Exceptions.GalgoException ex)
        {
            e.Result = ex.Message;
            return;
        }
        catch (Exception ex)
        {
            e.Result = string.Format("{0}{1}", "Erro na interface com PL/Cota do Galgo: ", ex.Message);
            return;
        }

        e.Result = "Pl/Cota Galgo importado com sucesso.";
    }

    protected void uplPlCotaGalgo_FileUploadComplete(object sender, FileUploadCompleteEventArgs e)
    {
        base.ImportaArquivoPlCota(e);
    }

    #endregion

    #region Grid
    protected void gridConsulta_CustomUnboundColumnData(object sender, DevExpress.Web.ASPxGridViewColumnDataEventArgs e)
    {
        if (e.Column.FieldName == "StatusDescricao")
        {
            int status = Convert.ToInt32(e.GetListSourceFieldValue(ClienteMetadata.ColumnNames.Status));

            string statusDescricao = TraducaoEnumsInvestidor.EnumStatusCliente.TraduzEnum(status);

            e.Value = statusDescricao;
        }
    }

    protected void gridConsulta_CustomCallback(object sender, DevExpress.Web.ASPxGridViewCustomCallbackEventArgs e)
    {
        gridConsulta.DataBind();
        gridConsulta.Selection.UnselectAll();
        gridConsulta.CancelEdit();
    }

    /// <summary>
    /// Função para selecionar todos os checkboxs de um Grid 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    new protected void CheckBoxSelectAll(object sender, EventArgs e)
    {
        ASPxCheckBox chk = sender as ASPxCheckBox;
        ASPxGridView grid = (chk.NamingContainer as GridViewHeaderTemplateContainer).Grid;
        chk.Checked = (grid.Selection.Count == grid.VisibleRowCount);
    }
    #endregion

    #region DataSources

    protected void EsDSClienteGalgo_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        Usuario usuario = new Usuario();
        usuario.BuscaUsuario(Context.User.Identity.Name);
        int idUsuario = usuario.IdUsuario.Value;

        ClienteQuery clienteQuery = new ClienteQuery("L");
        CarteiraQuery carteiraQuery = new CarteiraQuery("C");
        PermissaoClienteQuery permissaoClienteQuery = new PermissaoClienteQuery("P");
        GrupoProcessamentoQuery grupoProcessamentoQuery = new GrupoProcessamentoQuery("G");

        clienteQuery.Select(clienteQuery, grupoProcessamentoQuery.Descricao);
        clienteQuery.InnerJoin(permissaoClienteQuery).On(permissaoClienteQuery.IdCliente == clienteQuery.IdCliente);
        clienteQuery.InnerJoin(grupoProcessamentoQuery).On(clienteQuery.IdGrupoProcessamento == grupoProcessamentoQuery.IdGrupoProcessamento);
        clienteQuery.InnerJoin(carteiraQuery).On(carteiraQuery.IdCarteira == clienteQuery.IdCliente);

        //
        clienteQuery.Where(permissaoClienteQuery.IdUsuario == idUsuario);

        clienteQuery.Where(clienteQuery.TipoControle.NotIn((int)TipoControleCliente.ApenasCotacao, (int)TipoControleCliente.IRRendaVariavel));

        clienteQuery.Where(carteiraQuery.ExportaGalgo == "N");

        clienteQuery.Where(carteiraQuery.CodigoIsin.IsNotNull() && carteiraQuery.CodigoSTI.IsNotNull());

        clienteQuery.OrderBy(clienteQuery.Apelido.Ascending, grupoProcessamentoQuery.Descricao.Descending);

        ClienteCollection coll = new ClienteCollection();
        coll.Load(clienteQuery);

        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }

    protected void EsDSCliente_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        Usuario usuario = new Usuario();
        usuario.BuscaUsuario(Context.User.Identity.Name);
        int idUsuario = usuario.IdUsuario.Value;

        ClienteQuery clienteQuery = new ClienteQuery("L");
        CarteiraQuery carteiraQuery = new CarteiraQuery("C");
        PermissaoClienteQuery permissaoClienteQuery = new PermissaoClienteQuery("P");
        GrupoProcessamentoQuery grupoProcessamentoQuery = new GrupoProcessamentoQuery("G");

        clienteQuery.Select(clienteQuery, grupoProcessamentoQuery.Descricao);
        clienteQuery.InnerJoin(permissaoClienteQuery).On(permissaoClienteQuery.IdCliente == clienteQuery.IdCliente);
        clienteQuery.InnerJoin(grupoProcessamentoQuery).On(clienteQuery.IdGrupoProcessamento == grupoProcessamentoQuery.IdGrupoProcessamento);
        clienteQuery.InnerJoin(carteiraQuery).On(carteiraQuery.IdCarteira == clienteQuery.IdCliente);

        //
        clienteQuery.Where(permissaoClienteQuery.IdUsuario == idUsuario);

        clienteQuery.Where(clienteQuery.TipoControle.NotIn((int)TipoControleCliente.ApenasCotacao, (int)TipoControleCliente.IRRendaVariavel));

        clienteQuery.Where(carteiraQuery.CodigoIsin.IsNotNull() && carteiraQuery.CodigoSTI.IsNotNull());

        clienteQuery.OrderBy(clienteQuery.Apelido.Ascending, grupoProcessamentoQuery.Descricao.Descending);

        ClienteCollection coll = new ClienteCollection();
        coll.Load(clienteQuery);

        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }
    protected void EsDSTipoCliente_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        TipoClienteCollection coll = new TipoClienteCollection();
        coll.LoadAll();

        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }
    #endregion

    #region Multiple Upload member
    /// <summary>
    /// Handles the FileUploadComplete event of the DocumentsUploadControl control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="DevExpress.Web.FileUploadCompleteEventArgs"/> instance containing the event data.</param>
    protected void DocumentsUploadControl_FileUploadComplete(object sender, DevExpress.Web.FileUploadCompleteEventArgs e)
    {
        try
        {
            var uploadedFileInfo = e.UploadedFile.FileName.Split('_');

            if (uploadedFileInfo.Length == 1 )
            {
                uploadedFileInfo = uploadedFileInfo[0].Split('.');
            }

            var prefixo = uploadedFileInfo[0];

            var methodList =
                base.GetType()
                    .GetMethods(BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.Instance |
                                BindingFlags.Static);
            var method = methodList
                .Where(p => p.IsDefined(typeof (TipoImportacao), false))
                .Single(
                    p =>
                        ((TipoImportacao) (p.GetCustomAttributes(typeof (TipoImportacao), false).First())).Prefixo.ToUpper() ==
                        prefixo.ToUpper());

            method.Invoke(this, new object[] {e, textDataMultiArquivos});
        }
        catch (InvalidOperationException ex)
        {
            throw new Exception(string.Format("Arquivo {0} nao possui um prefixo valido.", e.UploadedFile.FileName));
        }
        catch (Exception ex)
        {
            throw;
        }
    }

    #endregion
}
