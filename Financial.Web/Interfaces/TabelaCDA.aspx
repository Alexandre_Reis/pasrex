﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="TabelaCDA.aspx.cs" Inherits="CadastrosBasicos_TabelaCDA" %>

<%@ Register Assembly="System.Web, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a" Namespace="System.Web.UI.HtmlControls" TagPrefix="cc2" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxe" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxwgv" %>

<%@ Register Assembly="EntitySpaces.Web" Namespace="EntitySpaces.Web" TagPrefix="cc1" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <link href="~/css/forms.css" rel="stylesheet" type="text/css" />    
    <script type="text/javascript" language="Javascript" src="../js/global.js"></script>    
    <script type="text/javascript" language="Javascript">
    var popup = false;
    document.onkeydown=onDocumentKeyDown;
    </script>
</head>

<body>
    <form id="form1" runat="server">
    
    <div class="divPanel">
    <table width="100%"><tr><td>
    <div id="container">
    
    <div id="header">
        <asp:Label ID="lblHeader" runat="server" Text="Tabela Arquivo CDA" />
    </div>
        
    <div id="mainContent">
            
        <div class="linkButton" >                          
           <asp:LinkButton ID="btnPDF" runat="server" Font-Overline="false" CssClass="btnPdf" OnClick="btnPDF_Click"><asp:Literal ID="Literal3" runat="server" Text="Gerar PDF"/><div></div></asp:LinkButton>
           <asp:LinkButton ID="btnExcel" runat="server" Font-Overline="false" CssClass="btnExcel" OnClick="btnExcel_Click"><asp:Literal ID="Literal4" runat="server" Text="Gerar Excel"/><div></div></asp:LinkButton>
           <asp:LinkButton ID="btnRefresh" runat="server" Font-Overline="false" CssClass="btnRefresh" OnClientClick=" gridCadastro.PerformCallback('btnRefresh'); return false;"><asp:Literal ID="Literal5" runat="server" Text="Atualizar"/><div></div></asp:LinkButton>
        </div>
    
        <div class="divDataGrid">            
            <dxwgv:ASPxGridView ID="gridCadastro" ClientInstanceName="gridCadastro" runat="server" EnableCallBacks="true"
            KeyFieldName="CompositeKey" DataSourceID="EsDSCDA"            
            OnCustomUnboundColumnData="gridCadastro_CustomUnboundColumnData"
            OnRowUpdating="gridCadastro_RowUpdating"
            OnCellEditorInitialize="gridCadastro_CellEditorInitialize">
                    
            <Columns>                           
                <dxwgv:GridViewCommandColumn VisibleIndex="0" ShowClearFilterButton="True">
                    <HeaderTemplate>
                        <dxe:ASPxCheckBox ID="cbAll" ClientInstanceName="cbAll" runat="server" ToolTip="Seleciona todos os CheckBoxs de todas as páginas" BackColor="white" ClientSideEvents-CheckedChanged="function(s, e) {OnAllCheckedChanged(s, e, gridCadastro);}" OnInit="CheckBoxSelectAll"/>
                    </HeaderTemplate>
                </dxwgv:GridViewCommandColumn>
                
                <dxwgv:GridViewDataTextColumn FieldName="CompositeKey" UnboundType="String" Visible="false" />
                
                <dxwgv:GridViewDataTextColumn FieldName="DS_Codigo" Caption="Código" Width="12%" VisibleIndex="1" ReadOnly="true" Settings-AutoFilterCondition="Contains" >                
                </dxwgv:GridViewDataTextColumn>
                
                <dxwgv:GridViewDataTextColumn FieldName="DS_Descricao" Caption="Descrição" Width="40%" VisibleIndex="2" ReadOnly="true" ExportWidth="430" Settings-AutoFilterCondition="Contains" />
                                
                <dxwgv:GridViewDataComboBoxColumn FieldName="DS_TipoMercado" Caption="Tipo Mercado" VisibleIndex="3" ReadOnly="true" Width="18%" ExportWidth="110">
                    <PropertiesComboBox EncodeHtml="false">
                    <Items>
                        <dxe:ListEditItem Value="Ações / Opções" Text="<div title='Ações/Opções'>Ações / Opções</div>" />
                        <dxe:ListEditItem Value="Ativos BMF" Text="<div title='Ativos BMF'>Ativos BMF</div>" />
                        <dxe:ListEditItem Value="Cotas Investimento" Text="<div title='Cotas Investimento'>Cotas Investimento</div>" />
                        <dxe:ListEditItem Value="Renda Fixa" Text="<div title='RendaFixa'>Renda Fixa</div>" />
                    </Items>
                    <ValidationSettings RequiredField-ErrorText=""/>
                    </PropertiesComboBox>
                </dxwgv:GridViewDataComboBoxColumn>
                                                              
                <dxwgv:GridViewDataSpinEditColumn FieldName="DS_CodigoCDA" Caption="Código CDA" Width="15%" VisibleIndex="4">
                <PropertiesSpinEdit MaxLength="9" MinValue="0" NumberType="Integer"></PropertiesSpinEdit>
                </dxwgv:GridViewDataSpinEditColumn>
                
                <dxwgv:GridViewDataDateColumn FieldName="DS_DataInicioVigencia" Caption="Início Vigência" Width="12%" VisibleIndex="6">                
                </dxwgv:GridViewDataDateColumn>
            </Columns>
            <SettingsCommandButton>
                <ClearFilterButton Image-Url="../imagens/funnel--minus.png"/>
                <UpdateButton Image-Url="../imagens/ico_form_ok_inline.gif"/>
                <CancelButton Image-Url="../imagens/ico_form_back_inline.gif"/>
            </SettingsCommandButton>            
            
        </dxwgv:ASPxGridView>            
        </div>
          
    </div>
    
    </div>
    </td></tr></table>
    </div>        
    
    <dxwgv:ASPxGridViewExporter ID="gridExport" runat="server" GridViewID="gridCadastro" 
    LeftMargin = "50"
    RightMargin = "50"
    />
            
    <cc1:esDataSource ID="EsDSCDA" runat="server" OnesSelect="EsDSCDA_esSelect" />
        
    </form>
</body>
</html>