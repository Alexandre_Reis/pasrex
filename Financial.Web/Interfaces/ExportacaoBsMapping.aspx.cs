﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using EntitySpaces;
using EntitySpaces.Interfaces;
using EntitySpaces.Core;
using Financial.BsMapping;
using Financial.Web.Common;
using System.Linq;
using System.Collections;
using System.Collections.Generic;


public partial class Interfaces_ExportacaoBsMapping : CadastroBasePage
{
    new protected void Page_Load(object sender, EventArgs e)
    {
        base.Page_Load(sender, e);
        if (!IsPostBack)
        {
            textDataReferencia.Date = DateTime.Now;
        }
    }
    

    protected void callbackExporta_Callback(object source, DevExpress.Web.CallbackEventArgs e)
    {
        if (textDataReferencia.Text == "")
            throw new Exception("Favor preencher a data de referência");
                     
        List<EntitySpaces.Interfaces.esConnectionElement> lstConnectionElement = EntitySpaces.Interfaces.esConfigSettings.ConnectionInfo.Connections.Cast<esConnectionElement>().ToList();

        EntitySpaces.Interfaces.esConnectionElement esConnection =  lstConnectionElement.Find(x => x.Name.Equals("BsMapping"));

        if(esConnection == null)
        {
            throw new Exception("Não existe connection string para o BsMapping");

        }

        string nomeDaProcedure = ConfigurationManager.AppSettings["NomeDaProcedureBsMapping"];
        string connectionString = esConnection.ConnectionString;
        int commandTimeOut = esConnection.CommandTimeout.GetValueOrDefault(30);

        using (var conn = new SqlConnection(connectionString))
        {
            conn.Open();
            using (var cmd = new SqlCommand(nomeDaProcedure, conn))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandTimeout = commandTimeOut; // Increase this to allow the proc longer to run
                cmd.Parameters.AddWithValue("@DataRef", textDataReferencia.Date);
                cmd.ExecuteNonQuery();
            }
        }

    }


    protected void EsDSLoadLog_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        LoadLogCollection coll = new LoadLogCollection();
        coll.es.Connection.Name = "BsMapping";        
        coll.LoadAll();
        e.Collection = coll;
    }


    //protected void callbackExporta_Callback(object source, DevExpress.Web.CallbackEventArgs e)
    //{
    //    if (textDataReferencia.Text == "")
    //        throw new Exception("Favor preencher a data de referência");
    //    string connectionString = ConfigurationManager.ConnectionStrings["BsMapping"].ConnectionString;
    //    string nomeDaProcedure = ConfigurationManager.AppSettings["NomeDaProcedureBsMapping"];


    //    using (SqlConnection con = new SqlConnection(connectionString))
    //    {
    //        //Create the SqlCommand object
    //        SqlCommand cmd = new SqlCommand(nomeDaProcedure, con);

    //        //Specify that the SqlCommand is a stored procedure
    //        cmd.CommandType = System.Data.CommandType.StoredProcedure;

    //        //Add the input parameters to the command object
    //        cmd.Parameters.AddWithValue("@DataRef", textDataReferencia.Date);
    //        cmd.CommandTimeout = 60 * 60;

    //        //Open the connection and execute the query
    //        con.Open();
    //        cmd.ExecuteNonQuery();
    //    }
    //}

}
