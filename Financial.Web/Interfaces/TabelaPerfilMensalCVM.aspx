﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="TabelaPerfilMensalCVM.aspx.cs"
    Inherits="CadastrosBasicos_TabelaPerfilMensalCVM" %>

<%@ Register Assembly="System.Web, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"
    Namespace="System.Web.UI.HtmlControls" TagPrefix="cc2" %>
<%@ Register Assembly="EntitySpaces.Web" Namespace="EntitySpaces.Web" TagPrefix="cc1" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxe" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxcb" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxwgv" %>

<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxpc" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxtc" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxw" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxlp" %>
    
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <link href="~/css/forms.css" rel="stylesheet" type="text/css" />

    <script type="text/javascript" language="Javascript" src="../js/global.js"></script>

    <script type="text/javascript" language="Javascript">    
    var operacao = '';  
    
    function OnGetDataCarteira(data) {
        btnEditCodigoCarteira.SetValue(data);
        popupCarteira.HideWindow();
        ASPxCallback1.SendCallback(btnEditCodigoCarteira.GetValue());
        btnEditCodigoCarteira.Focus();
    }
    </script>

</head>
<body>
    <form id="form1" runat="server">
        <dxcb:ASPxCallback ID="ASPxCallback1" runat="server" OnCallback="ASPxCallback1_Callback">
            <ClientSideEvents CallbackComplete="function(s, e) {    
            var textNome = document.getElementById('gridCadastro_DXPEForm_ef' + gridCadastro.cp_EditVisibleIndex + '_textNome');            
            OnCallBackCompleteCliente(s, e, popupMensagemCarteira, btnEditCodigoCarteira, textNome);                        
            }        
        " />
        </dxcb:ASPxCallback>
        
        <dxcb:ASPxCallback ID="callbackErro" runat="server" OnCallback="callbackErro_Callback">
            <ClientSideEvents CallbackComplete="function(s, e) {             
            if (e.result != '')
            {                   
                alert(e.result);                              
            }
            else
            {
                gridCadastro.UpdateEdit();                
            }
            operacao = '';
        }        
        " />
        </dxcb:ASPxCallback>
        
        <dxcb:ASPxCallback ID="callBackClonar" runat="server" OnCallback="callBackClonar_Callback">
            <ClientSideEvents CallbackComplete="function(s, e) {            
            LoadingPanelClonar.Hide();
            
            if (e.result != '') {
                alert(e.result);

                if (e.result == 'Processo executado com sucesso.') {
                     
                    popupClonar.Hide();
                    gridCadastro.PerformCallback('btnRefresh');
                }
            }
        }        
        " />
        </dxcb:ASPxCallback>
        
        <div class="divPanel">
            <table width="100%">
                <tr>
                    <td>
                        <div id="container">
                            <div id="header">
                                <asp:Label ID="lblHeader" runat="server" Text="Tabela Perfil Mensal CVM (Informações não calculadas)"></asp:Label>
                            </div>
                            <div id="mainContent">
                            
                            <dxpc:ASPxPopupControl ID="popupClonar" AllowDragging="true" PopupElementID="popupLote"
                                    EnableClientSideAPI="True" PopupVerticalAlign="Middle" PopupHorizontalAlign="OutsideRight"
                                    CloseAction="CloseButton" Width="500" Left="150" Top="10" HeaderText="Clonar"
                                    runat="server" HeaderStyle-BackColor="#EBECEE" HeaderStyle-Font-Bold="true" HeaderStyle-Font-Size="11px">
                                    <ContentCollection>
                                        <dxpc:PopupControlContentControl ID="PopupControlContentControl3" runat="server">
                                            <table border="0" cellspacing="2" cellpadding="2">
                                                                                                                                                
                                                <tr>
                                                    <td class="td_Label">
                                                     <asp:Label ID="labelInfo" runat="server" CssClass="labelRequired" Text="Carteira a Clonar:" />
                                                    </td>
                                                    <td colspan="3">
                                                        <dx:ASPxGridLookup ID="dropInfoComplementares" ClientInstanceName="dropInfoComplementares" runat="server"                                                         
                                                        KeyFieldName="CompositeKey" DataSourceID="EsDSTabelaPerfilClonar"
                                                                       Width="350px" TextFormatString="{0} - {1} - {3}" Font-Size="11px" AllowUserInput="false">
                                                         <Columns>
                                                             <dxwgv:GridViewCommandColumn ShowSelectCheckbox="True" Width="4%" />                        
                                                             <dxwgv:GridViewDataColumn FieldName="IdCarteira" Caption="Id" Width="7%" CellStyle-Font-Size="X-Small"/>                         
                                                             <dxwgv:GridViewDataColumn FieldName="Nome" CellStyle-Font-Size="X-Small" Width="90%" CellStyle-Wrap="false"/>                            
                                                             <dxwgv:GridViewDataDateColumn FieldName="Data" Width="10%"/>
                                                             <dxwgv:GridViewDataColumn FieldName="DataInicioVigenciaString" Visible="false" />    
                                                             <dxwgv:GridViewDataColumn FieldName="CompositeKey" Visible="false" />
                                                        </Columns>
                                                        
                                                        <GridViewProperties>
                                                            <Settings ShowFilterRow="True" />
                                                        </GridViewProperties>
                                                        
                                                        </dx:ASPxGridLookup>
                                                   </td>                                                                                                                                                                                                                                                                                                                                                                                                                       
                                                </tr>
                                                
                                                <tr>
                                                    <td class="td_Label">
                                                        <asp:Label ID="labelCarteira" runat="server" CssClass="labelRequired" Text="Nova Carteira:"/>
                                                    </td>
                                                    
                                                    <td class="td_Label">
                                                        <dx:ASPxGridLookup ID="dropCarteiraDestino" ClientInstanceName="dropCarteiraDestino" runat="server" KeyFieldName="IdCarteira" DataSourceID="EsDSCarteiraClonar"
                                                                                           Width="250px" TextFormatString="{0}" Font-Size="11px" AllowUserInput="false">
                                                                             <Columns>
                                                                                 <dxwgv:GridViewCommandColumn ShowSelectCheckbox="True" Width="4%" />                        
                                                                                 <dxwgv:GridViewDataColumn FieldName="IdCarteira" Caption="Id" Width="7%" CellStyle-Font-Size="X-Small"/>                         
                                                                                 <dxwgv:GridViewDataColumn FieldName="Apelido" CellStyle-Font-Size="X-Small" Width="89%" CellStyle-Wrap="false"/>                            
                                                                            </Columns>
                                                                            
                                                                            <GridViewProperties>
                                                                                <Settings ShowFilterRow="True" />
                                                                            </GridViewProperties>
                                                                            
                                                                        </dx:ASPxGridLookup>
                                                    </td>
                                                   
                                                </tr>
                                                
                                                <tr>
                                                    <td class="td_Label">
                                                        <asp:Label ID="label1" runat="server" CssClass="labelRequired" Text="Início Vigência:"></asp:Label>
                                                    </td>
                                                 <td>       
                                                    <dxe:ASPxDateEdit ID="textDataClonar" runat="server" ClientInstanceName="textDataClonar" />
                                                </td>  
                                                </tr>
                                            </table>
                                            <div class="linkButton linkButtonNoBorder" style="margin-top: 20px">
                                                <asp:LinkButton ID="btnProcessaClonar" runat="server" Font-Overline="false" ForeColor="Black" CssClass="btnOK"
                                                
                                                    OnClientClick="                                                                                                                                                                                                                                                                                                                                                                                                                                               
                                                                   LoadingPanelClonar.Show();
                                                                   callBackClonar.SendCallback();                                                                        
                                                                   
                                                                   return false;
                                                                   ">
                                                    
                                                    <asp:Literal ID="Literal15" runat="server" Text="Clonar" /><div></div>
                                                </asp:LinkButton>
                                            </div>
                                        </dxpc:PopupControlContentControl>
                                    </ContentCollection>
                                </dxpc:ASPxPopupControl>
                            
                                <div class="linkButton">
                                    <asp:LinkButton ID="btnAdd" runat="server" Font-Overline="false" ValidationGroup="ATK"
                                        CssClass="btnAdd" OnClientClick="gridCadastro.AddNewRow(); return false;">
                                        <asp:Literal ID="Literal1" runat="server" Text="Novo" /><div>
                                        </div>
                                    </asp:LinkButton>
                                    <asp:LinkButton ID="btnDelete" runat="server" Font-Overline="false" ValidationGroup="ATK"
                                        CssClass="btnDelete" OnClientClick=" if (confirm('Tem certeza que quer excluir?')==true) gridCadastro.PerformCallback('btnDelete');return false;">
                                        <asp:Literal ID="Literal2" runat="server" Text="Excluir" /><div>
                                        </div>
                                    </asp:LinkButton>
                                    <asp:LinkButton ID="btnPDF" runat="server" Font-Overline="false" ValidationGroup="ATK"
                                        CssClass="btnPdf" OnClick="btnPDF_Click">
                                        <asp:Literal ID="Literal4" runat="server" Text="Gerar PDF" /><div>
                                        </div>
                                    </asp:LinkButton>
                                    <asp:LinkButton ID="btnExcel" runat="server" Font-Overline="false" ValidationGroup="ATK"
                                        CssClass="btnExcel" OnClick="btnExcel_Click">
                                        <asp:Literal ID="Literal5" runat="server" Text="Gerar Excel" /><div>
                                        </div>
                                    </asp:LinkButton>
                                    <asp:LinkButton ID="btnRefresh" runat="server" Font-Overline="false" CssClass="btnRefresh"
                                        OnClientClick=" gridCadastro.PerformCallback('btnRefresh'); return false;">
                                        <asp:Literal ID="Literal6" runat="server" Text="Atualizar" /><div>
                                        </div>
                                    </asp:LinkButton>
                                    <asp:LinkButton ID="btnClonar" runat="server" Font-Overline="false" CssClass="btnLote"
                                        OnClientClick="
                                                                                
                                            dropInfoComplementares.SetText('');
                                            dropInfoComplementares.SetValue('');
                                            
                                            dropCarteiraDestino.SetText('');
                                            dropCarteiraDestino.SetValue('');
                                            
                                            textDataClonar.SetValue(null);
                                        
                                        popupClonar.ShowWindow(); return false;">
                                        <asp:Literal ID="Literal8" runat="server" Text="Clonar" /><div></div>
                                    </asp:LinkButton>              
                                    
                                </div>
                                <div class="divDataGrid">
                                    <dxwgv:ASPxGridView ID="gridCadastro" runat="server" EnableCallBacks="true" KeyFieldName="CompositeKey"
                                        DataSourceID="EsDSTabela" OnCustomUnboundColumnData="gridCadastro_CustomUnboundColumnData"
                                        OnRowUpdating="gridCadastro_RowUpdating" OnRowInserting="gridCadastro_RowInserting"
                                        OnCustomCallback="gridCadastro_CustomCallback" OnCustomJSProperties="gridCadastro_CustomJSProperties">
                                        <Columns>
                                            <dxwgv:GridViewCommandColumn ShowSelectCheckbox="True" VisibleIndex="0" Width="13%" ButtonType="Image" ShowClearFilterButton="True">
                                                <HeaderTemplate>
                                                    <dxe:ASPxCheckBox ID="cbAll" ClientInstanceName="cbAll" runat="server" ToolTip="Seleciona todos os CheckBoxs de todas as páginas" BackColor="white" ClientSideEvents-CheckedChanged="function(s, e) {OnAllCheckedChanged(s, e, gridCadastro);}" OnInit="CheckBoxSelectAll"/>
                                                </HeaderTemplate>
                                            </dxwgv:GridViewCommandColumn>
                                            <dxwgv:GridViewDataDateColumn FieldName="Data" Caption="Referência" VisibleIndex="1"
                                                Width="10%" />
                                            <dxwgv:GridViewDataColumn FieldName="IdCarteira" VisibleIndex="2" Width="10%" CellStyle-HorizontalAlign="left" />
                                            <dxwgv:GridViewDataColumn FieldName="Apelido" Caption="Nome" VisibleIndex="3" Width="67%" />
                                            <dxwgv:GridViewDataTextColumn FieldName="CompositeKey" UnboundType="String" Visible="False" />
                                        </Columns>
                                        <Templates>
                                            <EditForm>
                                                <div class="editForm">
                                                    <dxtc:ASPxPageControl ID="tabCadastro" runat="server" ActiveTabIndex="0" TabSpacing="0px"
                                                        TabAlign="Left" TabPosition="Top" Height="210px">
                                                        <TabPages>
                                                            <dxtc:TabPage Text="Informações Básicas">
                                                                <ContentCollection>
                                                                    <dxw:ContentControl runat="server">
                                                                        <table>
                                                                            <tr>
                                                                                <td class="td_Label_Curto">
                                                                                    <asp:Label ID="labelData" runat="server" CssClass="labelRequired" Text="Data:"></asp:Label>
                                                                                </td>
                                                                                <td>
                                                                                    <dxe:ASPxDateEdit ID="textData" runat="server" ClientInstanceName="textData" Value='<%#Eval("Data")%>'
                                                                                        OnInit="textData_Init" />
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td class="td_Label">
                                                                                    <asp:Label ID="labelCarteira" runat="server" CssClass="labelRequired" Text="Carteira:"></asp:Label>
                                                                                </td>
                                                                                <td>
                                                                                    <dxe:ASPxSpinEdit ID="btnEditCodigoCarteira" runat="server" CssClass="textButtonEdit1"
                                                                                        ClientInstanceName="btnEditCodigoCarteira" Text='<%# Eval("IdCarteira") %>' MaxLength="10"
                                                                                        NumberType="Integer">
                                                                                        <Buttons>
                                                                                            <dxe:EditButton>
                                                                                            </dxe:EditButton>
                                                                                        </Buttons>
                                                                                        <ClientSideEvents KeyPress="function(s, e) {document.getElementById('gridCadastro_DXPEForm_ef' + gridCadastro.cp_EditVisibleIndex + '_textNome').value = '';} "
                                                                                            ButtonClick="function(s, e) {popupCarteira.ShowAtElementByID(s.name);}" LostFocus="function(s, e) {OnLostFocus(popupMensagemCarteira, ASPxCallback1, btnEditCodigoCarteira);}" />
                                                                                    </dxe:ASPxSpinEdit>
                                                                                </td>
                                                                                <td colspan="2">
                                                                                    <asp:TextBox ID="textNome" runat="server" CssClass="textNome" Enabled="false" Text='<%#Eval("Apelido")%>'></asp:TextBox>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td class="td_Label" colspan="2">
                                                                                    <asp:Label ID="labelSecao3" runat="server" CssClass="labelNormal" Text="Resumo de teor dos votos proferidos:" />
                                                                                </td>
                                                                                <td>
                                                                                    <asp:TextBox ID="textSecao3" runat="server" TextMode="MultiLine" Rows="4" CssClass="textLongo5"
                                                                                        MaxLength="4000" Text='<%#Eval("Secao3")%>' />
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td class="td_Label" colspan="2">
                                                                                    <asp:Label ID="label1" runat="server" CssClass="labelNormal" Text="Justificativa sumária para abstenção:" />
                                                                                </td>
                                                                                <td>
                                                                                    <asp:TextBox ID="textSecao4" runat="server" TextMode="MultiLine" Rows="4" CssClass="textLongo5"
                                                                                        MaxLength="4000" Text='<%#Eval("Secao4")%>' />
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td class="td_Label" colspan="2">
                                                                                    <asp:Label ID="label2" runat="server" CssClass="labelNormal" Text="VAR diário:" />
                                                                                </td>
                                                                                <td>
                                                                                    <dxe:ASPxSpinEdit ID="textSecao5" runat="server" CssClass="textValor" ClientInstanceName="textSecao5"
                                                                                        Text='<%# Eval("Secao5") %>' MaxLength="10" NumberType="Float" DecimalPlaces="4" />
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td class="td_Label" colspan="2">
                                                                                    <asp:Label ID="labelTipo" runat="server" CssClass="labelNormal" Text="Tipo:"></asp:Label>
                                                                                </td>
                                                                                <td>
                                                                                    <dxe:ASPxComboBox ID="dropSecao6" runat="server" ClientInstanceName="dropSecao6"
                                                                                        ShowShadow="true" Text='<%# Eval("Secao6") %>' DropDownStyle="DropDownList" CssClass="dropDownListLongo">
                                                                                        <Items>
                                                                                            <dxe:ListEditItem Value="1" Text="Paramétrico" />
                                                                                            <dxe:ListEditItem Value="2" Text="Não Paramétrico" />
                                                                                            <dxe:ListEditItem Value="3" Text="Simulação Monte Carlo" />
                                                                                        </Items>
                                                                                    </dxe:ASPxComboBox>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td class="td_Label" colspan="2">
                                                                                    <asp:Label ID="label3" runat="server" CssClass="labelNormal" Text="Deliberações aprovadas em assembléia:" />
                                                                                </td>
                                                                                <td>
                                                                                    <asp:TextBox ID="textSecao8" runat="server" TextMode="MultiLine" Rows="4" CssClass="textLongo5"
                                                                                        MaxLength="4000" Text='<%#Eval("Secao8")%>' />
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </dxw:ContentControl>
                                                                </ContentCollection>
                                                            </dxtc:TabPage>
                                                            <dxtc:TabPage Text="Fatores de Risco">
                                                                <ContentCollection>
                                                                    <dxw:ContentControl runat="server">
                                                                        <table>
                                                                            <tr>
                                                                                <td class="td_Label" colspan="2">
                                                                                    <asp:Label ID="label20" runat="server" CssClass="labelNormal" Text="% Variação Cota pelos fatores de risco:" />
                                                                                </td>
                                                                                <td>
                                                                                    <dxe:ASPxSpinEdit ID="textSecao11PercentCota" runat="server" CssClass="textValor"
                                                                                        ClientInstanceName="textSecao11PercentCota" Text='<%# Eval("Secao11_PercentCota") %>'
                                                                                        MaxLength="6" NumberType="Float" DecimalPlaces="2" />
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td class="td_Label" colspan="2">
                                                                                    <asp:Label ID="label9" runat="server" CssClass="labelNormal" Text="Fator de risco 1:" />
                                                                                </td>
                                                                                <td>
                                                                                    <asp:TextBox ID="textSecao11Fator1" runat="server" CssClass="textLongo5" Text='<%#Eval("Secao11_Fator1")%>' />
                                                                                </td>
                                                                                <td class="td_Label" colspan="2">
                                                                                    <asp:Label ID="label11" runat="server" CssClass="labelNormal" Text="Cenário 1:" />
                                                                                </td>
                                                                                <td>
                                                                                    <asp:TextBox ID="textSecao11Cenario1" runat="server" CssClass="textLongo5" Text='<%#Eval("Secao11_Cenario1")%>' />
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td class="td_Label" colspan="2">
                                                                                    <asp:Label ID="label12" runat="server" CssClass="labelNormal" Text="Fator de risco 2:" />
                                                                                </td>
                                                                                <td>
                                                                                    <asp:TextBox ID="textSecao11Fator2" runat="server" CssClass="textLongo5" Text='<%#Eval("Secao11_Fator2")%>' />
                                                                                </td>
                                                                                <td class="td_Label" colspan="2">
                                                                                    <asp:Label ID="label13" runat="server" CssClass="labelNormal" Text="Cenário 2:" />
                                                                                </td>
                                                                                <td>
                                                                                    <asp:TextBox ID="textSecao11Cenario2" runat="server" CssClass="textLongo5" Text='<%#Eval("Secao11_Cenario2")%>' />
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td class="td_Label" colspan="2">
                                                                                    <asp:Label ID="label14" runat="server" CssClass="labelNormal" Text="Fator de risco 3:" />
                                                                                </td>
                                                                                <td>
                                                                                    <asp:TextBox ID="textSecao11Fator3" runat="server" CssClass="textLongo5" Text='<%#Eval("Secao11_Fator3")%>' />
                                                                                </td>
                                                                                <td class="td_Label" colspan="2">
                                                                                    <asp:Label ID="label15" runat="server" CssClass="labelNormal" Text="Cenário 3:" />
                                                                                </td>
                                                                                <td>
                                                                                    <asp:TextBox ID="textSecao11Cenario3" runat="server" CssClass="textLongo5" Text='<%#Eval("Secao11_Cenario3")%>' />
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td class="td_Label" colspan="2">
                                                                                    <asp:Label ID="label16" runat="server" CssClass="labelNormal" Text="Fator de risco 4:" />
                                                                                </td>
                                                                                <td>
                                                                                    <asp:TextBox ID="textSecao11Fator4" runat="server" CssClass="textLongo5" Text='<%#Eval("Secao11_Fator4")%>' />
                                                                                </td>
                                                                                <td class="td_Label" colspan="2">
                                                                                    <asp:Label ID="label17" runat="server" CssClass="labelNormal" Text="Cenário 4:" />
                                                                                </td>
                                                                                <td>
                                                                                    <asp:TextBox ID="textSecao11Cenario4" runat="server" CssClass="textLongo5" Text='<%#Eval("Secao11_Cenario4")%>' />
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td class="td_Label" colspan="2">
                                                                                    <asp:Label ID="label18" runat="server" CssClass="labelNormal" Text="Fator de risco 5:" />
                                                                                </td>
                                                                                <td>
                                                                                    <asp:TextBox ID="textSecao11Fator5" runat="server" CssClass="textLongo5" Text='<%#Eval("Secao11_Fator5")%>' />
                                                                                </td>
                                                                                <td class="td_Label" colspan="2">
                                                                                    <asp:Label ID="label19" runat="server" CssClass="labelNormal" Text="Cenário 5:" />
                                                                                </td>
                                                                                <td>
                                                                                    <asp:TextBox ID="textSecao11Cenario5" runat="server" CssClass="textLongo5" Text='<%#Eval("Secao11_Cenario5")%>' />
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td class="td_Label" colspan="2">
                                                                                    <asp:Label ID="label4" runat="server" CssClass="labelNormal" Text="% Variação pior cenário de estresse:" />
                                                                                </td>
                                                                                <td>
                                                                                    <dxe:ASPxSpinEdit ID="textSecao12" runat="server" CssClass="textValor" ClientInstanceName="textSecao12"
                                                                                        Text='<%# Eval("Secao12") %>' MaxLength="6" NumberType="Float" DecimalPlaces="2" />
                                                                                </td>
                                                                                <td class="td_Label" colspan="2">
                                                                                    <asp:Label ID="label5" runat="server" CssClass="labelNormal" Text="% Variação (Fator Pré):" />
                                                                                </td>
                                                                                <td>
                                                                                    <dxe:ASPxSpinEdit ID="textSecao13" runat="server" CssClass="textValor" ClientInstanceName="textSecao13"
                                                                                        Text='<%# Eval("Secao13") %>' MaxLength="6" NumberType="Float" DecimalPlaces="2" />
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td class="td_Label" colspan="2">
                                                                                    <asp:Label ID="label6" runat="server" CssClass="labelNormal" Text="% Variação (Fator Dólar):" />
                                                                                </td>
                                                                                <td>
                                                                                    <dxe:ASPxSpinEdit ID="textSecao14" runat="server" CssClass="textValor" ClientInstanceName="textSecao14"
                                                                                        Text='<%# Eval("Secao14") %>' MaxLength="6" NumberType="Float" DecimalPlaces="2" />
                                                                                </td>
                                                                                <td class="td_Label" colspan="2">
                                                                                    <asp:Label ID="label7" runat="server" CssClass="labelNormal" Text="% Variação (Ibovespa):" />
                                                                                </td>
                                                                                <td>
                                                                                    <dxe:ASPxSpinEdit ID="textSecao15" runat="server" CssClass="textValor" ClientInstanceName="textSecao15"
                                                                                        Text='<%# Eval("Secao15") %>' MaxLength="6" NumberType="Float" DecimalPlaces="2" />
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td class="td_Label" colspan="2">
                                                                                    <asp:Label ID="label8" runat="server" CssClass="labelNormal" Text="Principal fator de risco:" />
                                                                                </td>
                                                                                <td>
                                                                                    <asp:TextBox ID="textSecao16Fator" runat="server" CssClass="textLongo5" Text='<%#Eval("Secao16_Fator")%>' />
                                                                                </td>
                                                                                <td class="td_Label" colspan="2">
                                                                                    <asp:Label ID="label10" runat="server" CssClass="labelNormal" Text="Variação % da cota:" />
                                                                                </td>
                                                                                <td>
                                                                                    <dxe:ASPxSpinEdit ID="textSecao16Variacao" runat="server" CssClass="textValor" ClientInstanceName="textSecao16Variacao"
                                                                                        Text='<%# Eval("Secao16_Variacao") %>' MaxLength="10" NumberType="Float" DecimalPlaces="4" />
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </dxw:ContentControl>
                                                                </ContentCollection>
                                                            </dxtc:TabPage>
                                                            <dxtc:TabPage Text="Composição Fundo">
                                                                <ContentCollection>
                                                                    <dxw:ContentControl runat="server">
                                                                        <table border="0">
                                                                        <tr>
                                                                        <td class="td_Label" colspan="6" style="text-align:left;" >
                                                                                <asp:Label ID="label437" runat="server" CssClass="labelNormal" style="color:Black;font-weight:bold;" Text="Mercado Balcão"></asp:Label>
                                                                        </td>                                                                        
                                                                        <tr>
                                                                            <td class="td_Label">
                                                                                <asp:Label ID="label38" runat="server" CssClass="labelNormal" Text="CNPJ Comitente 1:" />
                                                                            </td>
                                                                            <td>
                                                                                <dxe:ASPxTextBox MaxLength="18" ID="textCNPJ1" runat="server" ClientInstanceName="textCNPJ1" CssClass="textNormal10" Text='<%# Eval("Secao18_CnpjComitente1") %>' >
                                                                                 
                                                                                    <ClientSideEvents KeyPress="function(s,e)  {                                                                                            
	                                                                                        var cpfcnpj = textCNPJ1.GetText();
												
	                                                                                        cpfcnpj = cpfcnpj.replace(/\D/g,'');                     
	                                                                                        cpfcnpj = cpfcnpj.replace(/^(\d{2})(\d)/,'$1.$2')
	                                                                                        cpfcnpj = cpfcnpj.replace(/^(\d{2})\.(\d{3})(\d)/,'$1.$2.$3')   
	                                                                                        cpfcnpj = cpfcnpj.replace(/\.(\d{3})(\d)/,'.$1/$2')             
	                                                                                        cpfcnpj = cpfcnpj.replace(/(\d{4})(\d)/,'$1-$2')        
	     
	                                                                                        textCNPJ1.SetValue(cpfcnpj);                                                                                            
	                                                                                    }" />

                                                                                    	<ClientSideEvents Init="function(s,e)  {
	                                                                                            var cpfcnpj = textCNPJ1.GetText();
																																		     
	                                                                                            cpfcnpj = cpfcnpj.replace(/\D/g,'');                     
	                                                                                            cpfcnpj = cpfcnpj.replace(/^(\d{2})(\d)/,'$1.$2')
	                                                                                            cpfcnpj = cpfcnpj.replace(/^(\d{2})\.(\d{3})(\d)/,'$1.$2.$3')   
	                                                                                            cpfcnpj = cpfcnpj.replace(/\.(\d{3})(\d)/,'.$1/$2')             
	                                                                                            cpfcnpj = cpfcnpj.replace(/(\d{4})(\d)/,'$1-$2')                
	                                                                                            textCNPJ1.SetValue(cpfcnpj);
                                                                                        }" />
                                                                                           
                                                                                 </dxe:ASPxTextBox>
                                                                            </td>
                                                                            
                                                                            <td class="td_Label">
                                                                                <asp:Label ID="label39" runat="server" CssClass="labelNormal" Text="Parte Rel 1:"></asp:Label>
                                                                            </td>
                                                                            <td>
                                                                                <dxe:ASPxComboBox ID="dropParteRelacionada1" runat="server" ClientInstanceName="dropParteRelacionada1"
                                                                                    ShowShadow="False" CssClass="dropDownListCurto_6" ValueType="System.String" Text='<%#Eval("Secao18_ParteRelacionada1")%>' >
                                                                                    <Items>
                                                                                        <dxe:ListEditItem Value="" Text="" />
                                                                                        <dxe:ListEditItem Value="S" Text="Sim" />
                                                                                        <dxe:ListEditItem Value="N" Text="Não" />
                                                                                    </Items> 
                                                                                </dxe:ASPxComboBox>
                                                                            </td>
                                                                            
                                                                            <td class="td_Label_Longo">
                                                                                 <asp:Label ID="label40" runat="server" CssClass="labelNormal" Text="Valor Total 1:"> </asp:Label>
                                                                            </td>                                                                                                                             
                                                                            <td >
                                                                                <dxe:ASPxSpinEdit ID="textValor1" runat="server" CssClass="textValor_5" ClientInstanceName="textValor1"
                                                                                    MaxLength="10" NumberType="Float" DecimalPlaces="2"
                                                                                    Text='<%# Eval("Secao18_ValorTotal1") %>' >
                                                                                </dxe:ASPxSpinEdit>
                                                                            </td>    
                                                                        </tr>
                                                                        <tr>
                                                                            <td class="td_Label">
                                                                                <asp:Label ID="label41" runat="server" CssClass="labelNormal" Text="CNPJ Comitente 2" />
                                                                            </td>
                                                                            <td>
                                                                                <dxe:ASPxTextBox MaxLength="18" ID="textCNPJ2" runat="server" ClientInstanceName="textCNPJ2" CssClass="textNormal10" Text='<%# Eval("Secao18_CnpjComitente2") %>' >
                                                                                     <ClientSideEvents KeyPress="function(s,e)  {                                                                                            
	                                                                                        var cpfcnpj = textCNPJ2.GetText();
												
	                                                                                        cpfcnpj = cpfcnpj.replace(/\D/g,'');                     
	                                                                                        cpfcnpj = cpfcnpj.replace(/^(\d{2})(\d)/,'$1.$2')
	                                                                                        cpfcnpj = cpfcnpj.replace(/^(\d{2})\.(\d{3})(\d)/,'$1.$2.$3')   
	                                                                                        cpfcnpj = cpfcnpj.replace(/\.(\d{3})(\d)/,'.$1/$2')             
	                                                                                        cpfcnpj = cpfcnpj.replace(/(\d{4})(\d)/,'$1-$2')        
	     
	                                                                                        textCNPJ2.SetValue(cpfcnpj);                                                                                            
	                                                                                    }" />

                                                                                      <ClientSideEvents Init="function(s,e)  {
	                                                                                            var cpfcnpj = textCNPJ2.GetText();
																																		     
	                                                                                            cpfcnpj = cpfcnpj.replace(/\D/g,'');                     
	                                                                                            cpfcnpj = cpfcnpj.replace(/^(\d{2})(\d)/,'$1.$2')
	                                                                                            cpfcnpj = cpfcnpj.replace(/^(\d{2})\.(\d{3})(\d)/,'$1.$2.$3')   
	                                                                                            cpfcnpj = cpfcnpj.replace(/\.(\d{3})(\d)/,'.$1/$2')             
	                                                                                            cpfcnpj = cpfcnpj.replace(/(\d{4})(\d)/,'$1-$2')                
	                                                                                            textCNPJ2.SetValue(cpfcnpj);
                                                                                        }" />     
                                                                                 </dxe:ASPxTextBox>

                                                                            </td>
                                                                            
                                                                            <td class="td_Label">
                                                                                <asp:Label ID="label42" runat="server" CssClass="labelNormal" Text="Parte Rel 2:"></asp:Label>
                                                                            </td>
                                                                            <td>
                                                                                <dxe:ASPxComboBox ID="dropParteRelacionada2" runat="server" ClientInstanceName="dropParteRelacionada2"
                                                                                    ShowShadow="False" CssClass="dropDownListCurto_6" ValueType="System.String" Text='<%#Eval("Secao18_ParteRelacionada2")%>' >
                                                                                    <Items>
                                                                                        <dxe:ListEditItem Value="" Text="" />
                                                                                        <dxe:ListEditItem Value="S" Text="Sim" />
                                                                                        <dxe:ListEditItem Value="N" Text="Não" />
                                                                                    </Items>
                                                                                </dxe:ASPxComboBox>
                                                                            </td>
                                                                            
                                                                            <td class="td_Label_Longo">
                                                                                 <asp:Label ID="label43" runat="server" CssClass="labelNormal" Text="Valor Total 2:"> </asp:Label>
                                                                            </td>                                                                                                                             
                                                                            <td >
                                                                                <dxe:ASPxSpinEdit ID="textValor2" runat="server" CssClass="textValor_5" ClientInstanceName="textValor2"
                                                                                    MaxLength="10" NumberType="Float" DecimalPlaces="2"
                                                                                    Text='<%# Eval("Secao18_ValorTotal2") %>' >
                                                                                </dxe:ASPxSpinEdit>
                                                                            </td>    
                                                                        </tr>
                                                                        <tr>
                                                                            <td class="td_Label">
                                                                                <asp:Label ID="label44" runat="server" CssClass="labelNormal" Text="CNPJ Comitente 3" />
                                                                            </td>
                                                                            <td>
                                                                                <dxe:ASPxTextBox MaxLength="18" ID="textCNPJ3" runat="server" ClientInstanceName="textCNPJ3" CssClass="textNormal10" Text='<%# Eval("Secao18_CnpjComitente3") %>' > 

                                                                                    <ClientSideEvents KeyPress="function(s,e)  {                                                                                            
	                                                                                        var cpfcnpj = textCNPJ3.GetText();
												
	                                                                                        cpfcnpj = cpfcnpj.replace(/\D/g,'');                     
	                                                                                        cpfcnpj = cpfcnpj.replace(/^(\d{2})(\d)/,'$1.$2')
	                                                                                        cpfcnpj = cpfcnpj.replace(/^(\d{2})\.(\d{3})(\d)/,'$1.$2.$3')   
	                                                                                        cpfcnpj = cpfcnpj.replace(/\.(\d{3})(\d)/,'.$1/$2')             
	                                                                                        cpfcnpj = cpfcnpj.replace(/(\d{4})(\d)/,'$1-$2')        
	     
	                                                                                        textCNPJ3.SetValue(cpfcnpj);
	                                                                                    }" />

                                                                                        <ClientSideEvents Init="function(s,e)  {
	                                                                                            var cpfcnpj = textCNPJ3.GetText();
																																		     
	                                                                                            cpfcnpj = cpfcnpj.replace(/\D/g,'');                     
	                                                                                            cpfcnpj = cpfcnpj.replace(/^(\d{2})(\d)/,'$1.$2')
	                                                                                            cpfcnpj = cpfcnpj.replace(/^(\d{2})\.(\d{3})(\d)/,'$1.$2.$3')   
	                                                                                            cpfcnpj = cpfcnpj.replace(/\.(\d{3})(\d)/,'.$1/$2')             
	                                                                                            cpfcnpj = cpfcnpj.replace(/(\d{4})(\d)/,'$1-$2')                
	                                                                                            textCNPJ3.SetValue(cpfcnpj);
                                                                                        }" />        
                                                                                 </dxe:ASPxTextBox>
                                                                            </td> 
                                                                            
                                                                            <td class="td_Label">
                                                                                <asp:Label ID="label45" runat="server" CssClass="labelNormal" Text="Parte Rel 3:"></asp:Label>
                                                                            </td>
                                                                            <td>
                                                                                <dxe:ASPxComboBox ID="dropParteRelacionada3" runat="server" ClientInstanceName="dropParteRelacionada3"
                                                                                    ShowShadow="False" CssClass="dropDownListCurto_6" ValueType="System.String" Text='<%#Eval("Secao18_ParteRelacionada3")%>' >
                                                                                    <Items>
                                                                                        <dxe:ListEditItem Value="" Text="" />
                                                                                        <dxe:ListEditItem Value="S" Text="Sim" />
                                                                                        <dxe:ListEditItem Value="N" Text="Não" />
                                                                                    </Items>
                                                                                </dxe:ASPxComboBox>
                                                                            </td>
                                                                            
                                                                            <td class="td_Label_Longo">
                                                                                 <asp:Label ID="label46" runat="server" CssClass="labelNormal" Text="Valor Total 3:"> </asp:Label>
                                                                            </td>                                                                                                                             
                                                                            <td >
                                                                                <dxe:ASPxSpinEdit ID="textValor3" runat="server" CssClass="textValor_5" ClientInstanceName="textValor3"
                                                                                    MaxLength="10" NumberType="Float" DecimalPlaces="2"
                                                                                    Text='<%# Eval("Secao18_ValorTotal3") %>' >
                                                                                </dxe:ASPxSpinEdit>
                                                                            </td>    
                                                                        </tr>
                                                                        <tr>
                                                                        <td class="td_Label" colspan="6" style="text-align:left;">
                                                                                <asp:Label ID="label47" runat="server" CssClass="labelBold" style="color:Black;font-weight:bold;" Text="Taxa Performance"></asp:Label>
                                                                        </td>  
                                                                        </tr>
                                                                        <tr>
                                                                         <td class="td_Label">
                                                                                <asp:Label ID="label4557" runat="server" CssClass="labelNormal" Text="Vedada Cobrança Taxa:"></asp:Label>
                                                                            </td>
                                                                            <td colspan="5">
                                                                                <dxe:ASPxComboBox ID="dropVedadaCobrancaTaxa" runat="server" ClientInstanceName="dropVedadaCobrancaTaxa"
                                                                                    ShowShadow="False" CssClass="dropDownListCurto_6" ValueType="System.String" Text='<%#Eval("Secao18_VedadaCobrancaTaxa")%>' >
                                                                                    <Items>
                                                                                        <dxe:ListEditItem Value="" Text="" />
                                                                                        <dxe:ListEditItem Value="S" Text="Sim" />
                                                                                        <dxe:ListEditItem Value="N" Text="Não" />
                                                                                    </Items>
                                                                                </dxe:ASPxComboBox>
                                                                            </td>
                                                                        
                                                                        </tr>
                                                                        <tr>
                                                                        <td class="td_Label">
                                                                            <asp:Label ID="label49" runat="server" CssClass="labelNormal" Text="Data Última Cobrança:" />
                                                                        </td>
                                                                        <td>
                                                                            <dxe:ASPxDateEdit ID="textDataUltimaCobranca" runat="server" ClientInstanceName="textDataUltimaCobranca"
                                                                                Value='<%#Eval("Secao18_DataUltimaCobranca")%>' />
                                                                        </td>                                     
                                            
                                                                        
                                                                        <td class="td_Label_Longo">
                                                                                 <asp:Label ID="label48" runat="server" CssClass="labelNormal" Text="Valor Última Cota:"> </asp:Label>
                                                                        </td>                                                                                                                             
                                                                        <td colspan="3">
                                                                            <dxe:ASPxSpinEdit ID="textValorUtimaCota" runat="server" CssClass="textValor_5" ClientInstanceName="textValorUtimaCota"
                                                                                MaxLength="10" NumberType="Float" DecimalPlaces="2"
                                                                                Text='<%# Eval("Secao18_ValorUltimaCota") %>' >
                                                                            </dxe:ASPxSpinEdit>
                                                                        </td>    
                                                                        
                                                                        </tr>
                                                                        </table>                                                                        
                                                                    </dxw:ContentControl>
                                                                </ContentCollection>
                                                            </dxtc:TabPage>
                                                        </TabPages>
                                                    </dxtc:ASPxPageControl>
                                                    <div class="linhaH">
                                                    </div>
                                                    <div class="linkButton linkButtonNoBorder popupFooter">
                                                        <asp:LinkButton ID="btnOK" runat="server" Font-Overline="false" CssClass="btnOK"
                                                            OnClientClick="callbackErro.SendCallback(); return false;">
                                                            <asp:Literal ID="Literal3" runat="server" Text="OK" /><div>
                                                            </div>
                                                        </asp:LinkButton>
                                                        <asp:LinkButton ID="btnCancel" runat="server" Font-Overline="false" CssClass="btnCancel"
                                                            OnClientClick="gridCadastro.CancelEdit(); return false;">
                                                            <asp:Literal ID="Literal9" runat="server" Text="Cancelar" /><div>
                                                            </div>
                                                        </asp:LinkButton>
                                                    </div>
                                                </div>
                                            </EditForm>
                                        </Templates>
                                        <SettingsPopup EditForm-Width="250px" />
                                        <SettingsCommandButton>
                                            <ClearFilterButton Image-Url="../imagens/funnel--minus.png"/>
                                        </SettingsCommandButton>
                                    </dxwgv:ASPxGridView>
                                </div>
                            </div>
                        </div>
                    </td>
                </tr>
            </table>
        </div>
        <dxwgv:ASPxGridViewExporter ID="gridExport" runat="server" GridViewID="gridCadastro" />
        <cc1:esDataSource ID="EsDSTabela" runat="server" OnesSelect="EsDSTabela_esSelect" LowLevelBind="True" />
        <cc1:esDataSource ID="EsDSCarteira" runat="server" OnesSelect="EsDSCarteira_esSelect" />
        
        
        <dxlp:ASPxLoadingPanel ID="LoadingPanelClonar" runat="server" Text="Processando, aguarde..." ClientInstanceName="LoadingPanelClonar" Modal="True"/>        
        <cc1:esDataSource ID="EsDSTabelaPerfilClonar" runat="server" OnesSelect="EsDSTabelaPerfilClonar_esSelect" LowLevelBind="true" />
        <cc1:esDataSource ID="EsDSCarteiraClonar" runat="server" OnesSelect="EsDSCarteiraClonar_esSelect" LowLevelBind="true" />

    </form>
</body>
</html>
