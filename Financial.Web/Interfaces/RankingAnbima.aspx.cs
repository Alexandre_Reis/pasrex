﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Financial.Web.Common;
using Financial.Fundo;
using DevExpress.Web;

using System.Collections.Generic;
using Financial.ContaCorrente;
using Financial.Util.ConfiguracaoSistema;
using Financial.Util.Enums;
using Financial.Util;
using Financial.InvestidorCotista;

public partial class Interfaces_RankingAnbima : ExportacaoBasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    //"EmployeeIDLabel"

    protected void EsDSCarteira_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        CarteiraCollection coll = new CarteiraCollection();
        coll.LoadAll();

        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }

    protected void EsDSControladoriaAtivo_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        CarteiraCollection coll = new CarteiraCollection();
        coll.LoadAll();

        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }

    protected void callback_Panel(object source, CallbackEventArgsBase e)
    {
        this.ativoPanel.Visible = false;
        this.passivoPanel.Visible = false;
        this.geralPanel.Visible = false;

        List<int> idsCarteira = this.gridConsulta.GetSelectedFieldValues("IdCarteira")
            .ConvertAll(new Converter<object, int>(delegate(object o)
            {
                return int.Parse(o.ToString());
            }));

        if (this.tipoRanking.SelectedIndex == -1)
            return;

        if (idsCarteira.Count == 0)
            throw new Exception("É necessario selecionar ao menos uma carteira.");

        if (string.IsNullOrEmpty(this.dataRanking.Text))        
            throw new Exception("É necessario preencher a data.");            
        

        PreencheInstituicional();

        if (this.tipoRanking.SelectedItem.Text == "Ativo")
        {
            this.ativoPanel.Visible = true;
            this.passivoPanel.Visible = false;
            this.geralPanel.Visible = true;

            PreencheRankingAtivo(idsCarteira, this.dataRanking.Date);
        }
        else if (this.tipoRanking.SelectedItem.Text == "Passivo")
        {
            this.ativoPanel.Visible = false;
            this.passivoPanel.Visible = true;
            this.geralPanel.Visible = true;

            PreencheRankingPassivo(idsCarteira,this.dataRanking.Date);
        }
    }

    #region preenche campos

    public void PreencheInstituicional()
    {
        this.instituicaoLabel.Text = Convert.ToString(ParametrosConfiguracaoSistema.Integracoes.NomeInstituicao);
        this.instituicaoCodLabel.Text = Convert.ToString(ParametrosConfiguracaoSistema.Integracoes.CodInstituicao);
        this.responsavelLabel.Text = Convert.ToString(ParametrosConfiguracaoSistema.Integracoes.ResponsavelInstituicao);
        this.telefoneLabel.Text = Convert.ToString(ParametrosConfiguracaoSistema.Integracoes.TelefoneInstituicao);
        this.emailLabel.Text = Convert.ToString(ParametrosConfiguracaoSistema.Integracoes.EmailInstituicao);
    }

    
    public void PreencheRankingAtivo(List<int> idsCarteira, DateTime data)
    {
        #region geral
        totalGeralAtivosLabel.Text = FormataValor(TotalDeAtivos(idsCarteira, data, null, null, null, null, true));
        #endregion

        #region proprio
        totalAtivosPropriosLabel.Text = FormataValor(TotalDeAtivos(idsCarteira, data, true, null, null, null, true));
        admFundoInvestimentosPropriosLabel.Text = FormataValor(PatrimonioLiquido(idsCarteira, data, true, 1, false, null, true));
        admCarteiraAdministradaPropriosLabel.Text = FormataValor(PatrimonioLiquido(idsCarteira, data, true, 1, true, null, true));
        admTotalPropriosLabel.Text = FormataValor(PatrimonioLiquido(idsCarteira, data, true, 1, null, null, true));
        efpcFundosDeInvestimentoPropriosLabel.Text = FormataValor(PatrimonioLiquido(idsCarteira, data, true, 2, false, null, true));
        efpcCarteiraAdministradaPropriosLabel.Text = FormataValor(PatrimonioLiquido(idsCarteira, data, true, 2, true, null, true));
        efpcTotalPropriosLabel.Text = FormataValor(PatrimonioLiquido(idsCarteira, data, true, 2, null, null, true));
        empFundosDeInvestimentosPropriosLabel.Text = FormataValor(PatrimonioLiquido(idsCarteira, data, true, 3, false, null, true));
        empCarteiraAdministradaPropriosLabel.Text = FormataValor(PatrimonioLiquido(idsCarteira, data, true, 3, true, null, true));
        empTotalPropriosLabel.Text = FormataValor(PatrimonioLiquido(idsCarteira, data, true, 3, null, null, true));
        
        segFundosDeInvestimentosPropriosLabel.Text = FormataValor(PatrimonioLiquido(idsCarteira, data, true, 4, false, null, true));
        segCarteiraAdministradaPropriosLabel.Text = FormataValor(PatrimonioLiquido(idsCarteira, data, true, 4, true, null, true));        
        segTotalPropriosLabel.Text = FormataValor(PatrimonioLiquido(idsCarteira, data, true, 4, null, null, true));

        fidcPropriosLabel.Text = FormataValor(PatrimonioLiquido(idsCarteira, data, true, 5, null, null, true));
        clubesDeInvestimentosPropriosLabel.Text = FormataValor(PatrimonioLiquido(idsCarteira, data, true, 6, null, null, true));
        outrosInvestidoresInstitucionaisPropriosLabel.Text = FormataValor(PatrimonioLiquido(idsCarteira, data, true, 7, null, null, true));
        numeroClientesPropriosLabel.Text = FormataQuantidade(NumeroClientes(idsCarteira, true, null, null, null, true));
        quantidadeVeiculosPropriosLabel.Text = FormataQuantidade(QuantidadeDeVeiculos(idsCarteira, true, null, null, null, true));

        #endregion

        #region não proprio
        totalAtivosOutrosLabel.Text = FormataValor(TotalDeAtivos(idsCarteira, data, false, null, null, null, true));
        admFundoInvestimentosOutrosLabel.Text = FormataValor(PatrimonioLiquido(idsCarteira, data, false, 1, false, null, true));
        admCarteiraAdministradaOutrosLabel.Text = FormataValor(PatrimonioLiquido(idsCarteira, data, false, 1, true, null, true));
        admTotalOutrosLabel.Text = FormataValor(PatrimonioLiquido(idsCarteira, data, false, 1, null, null, true));
        efpcFundosDeInvestimentoOutrosLabel.Text = FormataValor(PatrimonioLiquido(idsCarteira, data, false, 2, false, null, true));
        efpcCarteiraAdministradaOutrosLabel.Text = FormataValor(PatrimonioLiquido(idsCarteira, data, false, 2, true, null, true));
        efpcTotalOutrosLabel.Text = FormataValor(PatrimonioLiquido(idsCarteira, data, false, 2, null, null, true));
        empFundosDeInvestimentosOutrosLabel.Text = FormataValor(PatrimonioLiquido(idsCarteira, data, false, 3, false, null, true));
        empCarteiraAdministradaOutrosLabel.Text = FormataValor(PatrimonioLiquido(idsCarteira, data, false, 3, true, null, true));
        empTotalOutrosLabel.Text = FormataValor(PatrimonioLiquido(idsCarteira, data, false, 3, null, null, true));
        
        segFundosDeInvestimentosOutrosLabel.Text = FormataValor(PatrimonioLiquido(idsCarteira, data, false, 4, false, null, true));
        segCarteiraAdministradaOutrosLabel.Text = FormataValor(PatrimonioLiquido(idsCarteira, data, false, 4, true, null, true));
        segTotalOutrosLabel.Text = FormataValor(PatrimonioLiquido(idsCarteira, data, false, 4, null, null, true));
        
        fidcOutrosLabel.Text = FormataValor(PatrimonioLiquido(idsCarteira, data, false, 5, null, null, true));
        clubesDeInvestimentosOutrosLabel.Text = FormataValor(PatrimonioLiquido(idsCarteira, data, false, 6, null, null, true));
        outrosInvestidoresInstitucionaisOutrosLabel.Text = FormataValor(PatrimonioLiquido(idsCarteira, data, false, 7, null, null, true));
        numeroClientesOutrosLabel.Text = FormataQuantidade(NumeroClientes(idsCarteira, false, null, null, null, true));
        quantidadeVeiculosOutrosLabel.Text = FormataQuantidade(QuantidadeDeVeiculos(idsCarteira, false, null, null, null,true));
        #endregion
    }

    public void PreencheRankingPassivo(List<int> idsCarteira, DateTime data)
    {
        #region geral
        totalGeralVeiculosLabel.Text = FormataQuantidade(QuantidadeDeVeiculos(idsCarteira, null, null, null, null, false));
        totalGeralCotistasLabel.Text = FormataQuantidade(NumeroCotistas(idsCarteira, data, null, null, null, null, false));
        #endregion

        #region proprios
        totalVeiculosPropriosLabel.Text = FormataQuantidade(QuantidadeDeVeiculos(idsCarteira, true, null, null, null, false));
        fundosPropriosLabel.Text = FormataQuantidade(QuantidadeDeVeiculos(idsCarteira, true, null, null, false, false));
        clubesPropriosLabel.Text = FormataQuantidade(QuantidadeDeVeiculos(idsCarteira, true, null, null, true, false));
        numeroCotistasPropriosLabel.Text = FormataQuantidade(NumeroCotistas(idsCarteira, data, true, null, null, null, false));
        #endregion 

        #region outros

        totalVeiculosOutrosLabel.Text = FormataQuantidade(QuantidadeDeVeiculos(idsCarteira, false, null, null, null, false));
        fundosOutrosLabel.Text = FormataQuantidade(QuantidadeDeVeiculos(idsCarteira, false, null, null, false, false));
        clubesOutrosLabel.Text = FormataQuantidade(QuantidadeDeVeiculos(idsCarteira, false, null, null, true, false));
        numeroCotistasOutrosLabel.Text = FormataQuantidade(NumeroCotistas(idsCarteira, data, false, null, null, null, false));
        #endregion

    }




    #endregion

    

    #region Calculos

    public CarteiraQuery MontaCarteiraQuery(List<int> idsCarteira, bool? mesmoConglomerado, int? categoriaRankingAnbima, bool? carteiraAdministrada, bool? clubeDeInvestimento, bool ativo)
    {
        CarteiraQuery carteiraQuery = new CarteiraQuery("CA");
        
        carteiraQuery.Where(carteiraQuery.IdCarteira.In(idsCarteira));

        if (mesmoConglomerado.HasValue)
        {
            if(mesmoConglomerado.Value)
                carteiraQuery.Where(carteiraQuery.MesmoConglomerado == 'S');
            else
                carteiraQuery.Where(carteiraQuery.MesmoConglomerado == 'N');
        }

        if(categoriaRankingAnbima.HasValue)
            carteiraQuery.Where(carteiraQuery.CategoriaAnbima == categoriaRankingAnbima);


        if (carteiraAdministrada.HasValue && clubeDeInvestimento.HasValue)
            throw new Exception("Atenção este metodo permite o preenchimento de categoria ou carteiraAdministrada apenas");

        if (carteiraAdministrada.HasValue)
        {
            if (carteiraAdministrada.Value == true)
                carteiraQuery.Where(carteiraQuery.IdCategoria == 1);
            else
                carteiraQuery.Where(carteiraQuery.IdCategoria != 1);
        }

        if (clubeDeInvestimento.HasValue)
        {
            if (clubeDeInvestimento.Value)
                carteiraQuery.Where(carteiraQuery.CategoriaAnbima == 6);
            else
                carteiraQuery.Where(carteiraQuery.CategoriaAnbima != 6);
        }

        return carteiraQuery;
    }
    public decimal PatrimonioLiquido(List<int> idsCarteira, DateTime data, bool? mesmoConglomerado, int? categoriaRankingAnbima, bool? carteiraAdministrada, bool? clubeDeInvestimento, bool ativo)
    {
        HistoricoCotaQuery historicoCotaQuery = new HistoricoCotaQuery("h");
        CarteiraQuery carteiraQuery = MontaCarteiraQuery(idsCarteira, mesmoConglomerado, categoriaRankingAnbima, carteiraAdministrada, clubeDeInvestimento, ativo);

        carteiraQuery.Select(historicoCotaQuery.PLFechamento.Sum().As("teste"));
        carteiraQuery.InnerJoin(historicoCotaQuery).On(carteiraQuery.IdCarteira == historicoCotaQuery.IdCarteira);

        DateTime dataRanking = Calendario.RetornaUltimoDiaUtilMes(data);

        carteiraQuery.Where(historicoCotaQuery.Data.Date() == dataRanking);

        Carteira carteira = new Carteira();

        if (carteira.Load(carteiraQuery) && carteira.GetColumn("teste").GetType() != typeof(System.DBNull))
            return decimal.Parse(carteira.GetColumn("teste").ToString());
        else
            return 0;
    }
    public decimal QuantidadeDeVeiculos(List<int> idsCarteira, bool? mesmoConglomerado, int? categoriaRankingAnbima, bool? carteiraAdministrada, bool? clubeDeInvestimento, bool ativo)
    {
        CarteiraQuery carteiraQuery = MontaCarteiraQuery(idsCarteira, mesmoConglomerado, categoriaRankingAnbima, carteiraAdministrada, clubeDeInvestimento,ativo);
        carteiraQuery.Select(carteiraQuery.IdCarteira.Count().As("carteiras"));
        Carteira carteira = new Carteira();

        

        if (carteira.Load(carteiraQuery) && carteira.GetColumn("carteiras").GetType() != typeof(System.DBNull))
            return decimal.Parse(carteira.GetColumn("carteiras").ToString());
        else
            return 0;
    }
    public decimal NumeroClientes(List<int> idsCarteira, bool? mesmoConglomerado, int? categoriaRankingAnbima, bool? carteiraAdministrada, bool? clubeDeInvestimento, bool ativo)
    {
        CarteiraQuery carteiraQuery = MontaCarteiraQuery(idsCarteira, mesmoConglomerado, categoriaRankingAnbima, carteiraAdministrada, clubeDeInvestimento, ativo);
        carteiraQuery.es.Distinct = true;
        carteiraQuery.Select(carteiraQuery.Contratante);
        CarteiraCollection coll = new CarteiraCollection();



        if (coll.Load(carteiraQuery))
            return coll.Count;
        else return 0;
        
        
        //return decimal.Parse(carteira.GetColumn("contratante").ToString());
        //else
        //    return 0;




    }
    public decimal TotalDeAtivos(List<int> idsCarteira, DateTime data, bool? mesmoConglomerado, int? categoriaRankingAnbima, bool? carteiraAdministrada, bool? clubeDeInvestimento, bool ativo)
    {
        Carteira carteira = new Carteira();

        #region pl
        decimal pl = PatrimonioLiquido(idsCarteira, data, mesmoConglomerado, categoriaRankingAnbima, carteiraAdministrada, clubeDeInvestimento, ativo);
        #endregion

        #region liquidacao(CPR)

        CarteiraQuery carteiraQuery = MontaCarteiraQuery(idsCarteira, mesmoConglomerado, categoriaRankingAnbima, carteiraAdministrada, clubeDeInvestimento, ativo);
        LiquidacaoHistoricoQuery liquidacaoQuery = new LiquidacaoHistoricoQuery("lh");
        carteiraQuery.Select(liquidacaoQuery.Valor.Sum().As("liquidacao"));
        carteiraQuery.InnerJoin(liquidacaoQuery).On(carteiraQuery.IdCarteira == liquidacaoQuery.IdCliente);
        carteiraQuery.Where(
            liquidacaoQuery.DataLancamento.Date() <= data.Date &&
            liquidacaoQuery.DataVencimento.Date() > data.Date &&
            liquidacaoQuery.DataHistorico.Date() == data.Date
        );

        decimal liquidacao = 0;
        if (carteira.Load(carteiraQuery) && carteira.GetColumn("liquidacao").GetType() != typeof(System.DBNull))
            liquidacao = decimal.Parse(carteira.GetColumn("liquidacao").ToString());

        #endregion

        #region saldo
        carteiraQuery = MontaCarteiraQuery(idsCarteira, mesmoConglomerado, categoriaRankingAnbima, carteiraAdministrada, clubeDeInvestimento, ativo);        
        SaldoCaixaQuery saldoQuery = new SaldoCaixaQuery("sa");
        carteiraQuery.Select(saldoQuery.SaldoFechamento.Sum().As("saldo"));
        carteiraQuery.InnerJoin(saldoQuery).On(carteiraQuery.IdCarteira == saldoQuery.IdCliente);
        carteiraQuery.Where(
            saldoQuery.Data.Date() == data.Date
        );

        decimal saldo = 0;
        if (carteira.Load(carteiraQuery) && carteira.GetColumn("saldo").GetType() != typeof(System.DBNull))
            liquidacao = decimal.Parse(carteira.GetColumn("saldo").ToString());

        #endregion

        return pl - liquidacao - saldo;
    }
    public decimal NumeroCotistas(List<int> idsCarteira,DateTime data, bool? mesmoConglomerado, int? categoriaRankingAnbima, bool? carteiraAdministrada, bool? clubeDeInvestimento, bool ativo)
    {

        CarteiraQuery carteiraQuery = MontaCarteiraQuery(idsCarteira, mesmoConglomerado, categoriaRankingAnbima, carteiraAdministrada, clubeDeInvestimento, ativo);
        PosicaoCotistaHistoricoQuery posicaoQuery = new PosicaoCotistaHistoricoQuery("pq");

        carteiraQuery.InnerJoin(posicaoQuery).On(carteiraQuery.IdCarteira == posicaoQuery.IdCarteira);
        DateTime primeiroDia = Calendario.RetornaPrimeiroDiaUtilMes(data);
        DateTime ultimodia = Calendario.RetornaUltimoDiaUtilMes(data);
        

        carteiraQuery.Where(posicaoQuery.DataHistorico.Date().Between(primeiroDia,ultimodia));
        carteiraQuery.es.Distinct = true;
        carteiraQuery.Select(posicaoQuery.IdCotista);
            
        CarteiraCollection coll = new CarteiraCollection();

        
        if (coll.Load(carteiraQuery))
            return coll.Count;
        else
            return 0;

    }
    #endregion

    public string FormataQuantidade(decimal numero)
    {
        return numero.ToString("N0");
    }

    public string FormataValor(decimal numero)
    {
        return (numero/1000).ToString("N0");
    }


}
