﻿using System;
using System.Data;
using System.Collections;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Text;
using System.Collections.Generic;

using EntitySpaces.Interfaces;
using log4net;
using Financial.Web.Common;
using Financial.Fundo;
using System.IO;
using DevExpress.Web;
using Financial.Export;
using Financial.Util;
using Bytescout.Spreadsheet;
using Financial.Security;
using Financial.Investidor;
using Financial.Investidor.Enums;
using Financial.Captacao.Enums;
using Financial.Bolsa;
using Financial.Export.YMF;

public partial class Exportacao : ExportacaoBasePage
{
    private static readonly ILog log = LogManager.GetLogger(typeof(Exportacao));

    #region Primeira ABA
    #region Enums
    enum TipoExportacao_FormatoDDMMYYYY
    {
        BacenJud = 1,
        ContabilFolhamatic = 2,
        InformeDiario_1_0 = 20,
        InformeDiario_2_0 = 3,
        InformeDiario_3_0 = 19,
        InformeAnbima = 4,
        XMLPosicaoAnbid = 5, // Exportacao Anbid
        Matriz_Ativos = 6,
        ContabilSinacor = 7,
        YMF_TX3 = 8,
        YMF_TX2 = 9,
        CCOUSinacor = 10,
        CETIP_Cliente = 11,
        CETIP_OperacaoRF = 12,
        BacenCCS = 13,
        InformacoesComplementaresCVM = 17,
        InformacoesComplementaresCVM_XML = 18
    }

    enum TipoExportacao_FormatoMMYY
    {
        CDA20 = 100,
        CDA30 = 101,
        CDA40 = 1014,
        Picl = 102,
        //
        PerfilMensalCVM = 103,
        PerfilMensalCVM_XML_V3 = 140,
        //
        PerfilMensalCVM_PDF = 106,
        PerfilMensalCVM_PDF_V3 = 141,
        //
        RankingAnbimaAtivo = 104,
        RankingAnbimaPassivo = 108,
        //
        LaminaPDF = 120,
        LaminaPDF_V1 = 121,
        //
        LaminaXML = 130,
        LaminaXML_V1 = 131,
        Cosif4016 = 109,
        Cosif4010 = 110
    }

    enum TipoExportacao_FormatoYYYY
    {
        Dirf = 500,
        InformeRendimento = 501
    }

    enum TipoExportacao_FormatoPeriodo
    {
        ContabilAtt = 1000,
        ContabilInfoBanc = 1010,
        ContabilInfoBancContaFull = 1011,
        Galgo_PlCotaWcf = 1030,
        Galgo_PlCota = 1020
    }
    #endregion

    new protected void Page_Load(object sender, EventArgs e)
    {
        // Configura a largura dos gris dentro dos combos
        this.ConfiguraLookupGrid();

        if (!Page.IsPostBack)
        {
            DateTime hoje = DateTime.Now;

            #region Formato ddmmyyyy
            this.textData_ddmmyyyy.Text = hoje.ToShortDateString().ToString();
            #endregion

            #region Formato mmyyyy
            this.textData_mmyyyy.Value = new DateTime(hoje.Year, hoje.Month, 01); // 01/Mes/Ano - Representa Somente o Mes/Ano
            #endregion

            #region Formato yyyy
            this.textData_yyyy.Value = new DateTime(hoje.Year, 01, 01); // 01/01/Ano - Representa Somente o Ano
            #endregion

            #region Formato Periodo
            this.textDataInicio_Periodo.Text = hoje.ToShortDateString().ToString();
            this.textDataFim_Periodo.Text = hoje.ToShortDateString().ToString();
            this.textDataInicioGalgoPlCotaArquivo.Text = hoje.ToShortDateString().ToString();
            this.textDataFimGalgoPlCotaArquivo.Text = hoje.ToShortDateString().ToString();
            this.textDataInicioGalgoPlCotaWcf.Text = hoje.ToShortDateString().ToString();
            this.textDataFimGalgoPlCotaWcf.Text = hoje.ToShortDateString().ToString();
            #endregion

            tabArquivos.TabPages.FindByName("TabGalgo").ClientVisible= ParametrosConfiguracaoSistema.Integracoes.IntegraGalgoPlCota == (int)Financial.Util.Enums.IntegraGalgoPlCota.NaoIntegra ? false : true;
        }

    }

    protected void UpdatePanel_Unload(object sender, EventArgs e)
    {
        //foreach (System.Reflection.MethodInfo methodInfo in typeof(ScriptManager).GetMethods(System.Reflection.BindingFlags.NonPublic | System.Reflection.BindingFlags.Instance))
        //{
        //    if (methodInfo.Name.Equals("System.Web.UI.IScriptManagerInternal.RegisterUpdatePanel"))
        //    {
        //        methodInfo.Invoke(ScriptManager.GetCurrent(Page), new object[] { sender as UpdatePanel });
        //    }
        //}
    }

    #region DataSources
    protected void EsDSClienteGalgo_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        Usuario usuario = new Usuario();
        usuario.BuscaUsuario(Context.User.Identity.Name);
        int idUsuario = usuario.IdUsuario.Value;

        ClienteQuery clienteQuery = new ClienteQuery("L");
        CarteiraQuery carteiraQuery = new CarteiraQuery("C");
        PermissaoClienteQuery permissaoClienteQuery = new PermissaoClienteQuery("P");
        GrupoProcessamentoQuery grupoProcessamentoQuery = new GrupoProcessamentoQuery("G");

        clienteQuery.Select(clienteQuery, grupoProcessamentoQuery.Descricao);
        clienteQuery.InnerJoin(permissaoClienteQuery).On(permissaoClienteQuery.IdCliente == clienteQuery.IdCliente);
        clienteQuery.InnerJoin(grupoProcessamentoQuery).On(clienteQuery.IdGrupoProcessamento == grupoProcessamentoQuery.IdGrupoProcessamento);
        clienteQuery.InnerJoin(carteiraQuery).On(carteiraQuery.IdCarteira == clienteQuery.IdCliente);

        //
        clienteQuery.Where(permissaoClienteQuery.IdUsuario == idUsuario);

        clienteQuery.Where(clienteQuery.TipoControle.NotIn((int)TipoControleCliente.ApenasCotacao, (int)TipoControleCliente.IRRendaVariavel));

        clienteQuery.Where(carteiraQuery.ExportaGalgo == "S");

        clienteQuery.Where(carteiraQuery.CodigoIsin.IsNotNull() && carteiraQuery.CodigoSTI.IsNotNull());

        clienteQuery.OrderBy(clienteQuery.Apelido.Ascending, grupoProcessamentoQuery.Descricao.Descending);

        ClienteCollection coll = new ClienteCollection();
        coll.Load(clienteQuery);

        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }
    protected void EsDSCliente_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        Usuario usuario = new Usuario();
        usuario.BuscaUsuario(Context.User.Identity.Name);
        int idUsuario = usuario.IdUsuario.Value;

        ClienteQuery clienteQuery = new ClienteQuery("L");
        PermissaoClienteQuery permissaoClienteQuery = new PermissaoClienteQuery("P");
        GrupoProcessamentoQuery grupoProcessamentoQuery = new GrupoProcessamentoQuery("G");

        clienteQuery.Select(clienteQuery, grupoProcessamentoQuery.Descricao);
        clienteQuery.InnerJoin(permissaoClienteQuery).On(permissaoClienteQuery.IdCliente == clienteQuery.IdCliente);
        clienteQuery.InnerJoin(grupoProcessamentoQuery).On(clienteQuery.IdGrupoProcessamento == grupoProcessamentoQuery.IdGrupoProcessamento);
        //
        clienteQuery.Where(permissaoClienteQuery.IdUsuario == idUsuario);

        clienteQuery.Where(clienteQuery.TipoControle.NotIn((int)TipoControleCliente.ApenasCotacao, (int)TipoControleCliente.IRRendaVariavel));

        clienteQuery.OrderBy(clienteQuery.Apelido.Ascending, grupoProcessamentoQuery.Descricao.Descending);

        ClienteCollection coll = new ClienteCollection();
        coll.Load(clienteQuery);

        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }

    protected void EsDSTipoCliente_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        TipoClienteCollection coll = new TipoClienteCollection();
        coll.LoadAll();

        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }
    #endregion

    #region Grid
    protected void gridConsulta_CustomUnboundColumnData(object sender, DevExpress.Web.ASPxGridViewColumnDataEventArgs e)
    {
        if (e.Column.FieldName == "StatusDescricao")
        {
            int status = Convert.ToInt32(e.GetListSourceFieldValue(ClienteMetadata.ColumnNames.Status));

            string statusDescricao = TraducaoEnumsInvestidor.EnumStatusCliente.TraduzEnum(status);

            e.Value = statusDescricao;
        }
    }

    protected void gridConsulta_CustomCallback(object sender, DevExpress.Web.ASPxGridViewCustomCallbackEventArgs e)
    {
        gridConsulta.DataBind();
        gridConsulta.Selection.UnselectAll();
        gridConsulta.CancelEdit();
    }
    #endregion

    /// <summary>
    /// Trata os Erros de Exportação
    /// </summary>
    /// <param name="tipo">Arquivo que se quer Exportar. Null se tipo Arquivo é nulo</param>
    private void TrataErros(int? tipo)
    {
        if (!tipo.HasValue)
        {
            throw new Exception("Tipo Obrigatório.");
        }

        List<object> keyValuesId = gridConsulta.GetSelectedFieldValues("IdCliente");

        if (tipo != (int)TipoExportacao_FormatoDDMMYYYY.CETIP_Cliente &&
            tipo != (int)TipoExportacao_FormatoDDMMYYYY.CETIP_OperacaoRF &&
            tipo != (int)TipoExportacao_FormatoPeriodo.Galgo_PlCota &&
            tipo != (int)TipoExportacao_FormatoPeriodo.Galgo_PlCotaWcf)
        {
            if (keyValuesId.Count == 0)
            {
                throw new Exception("Algum cliente deve ser selecionado!");
            }
        }

        List<object> keyValuesIdGalgo = gridConsultaGalgo.GetSelectedFieldValues("IdCliente");

        if (tipo == (int)TipoExportacao_FormatoPeriodo.Galgo_PlCota ||
            tipo == (int)TipoExportacao_FormatoPeriodo.Galgo_PlCotaWcf)
        {
            if (keyValuesIdGalgo.Count == 0)
            {
                throw new Exception("Algum cliente deve ser selecionado!");
            }
        }

        List<Control> controles1 = new List<Control>(new Control[] { this.textData_ddmmyyyy });
        List<Control> controles2 = new List<Control>(new Control[] { this.textData_mmyyyy });
        List<Control> controles3 = new List<Control>(new Control[] { this.textData_yyyy });
        List<Control> controles4 = new List<Control>(new Control[] { this.textDataInicio_Periodo, this.textDataFim_Periodo });
        List<Control> controles5 = new List<Control>(new Control[] { this.textDataInicioGalgoPlCotaArquivo, this.textDataFimGalgoPlCotaArquivo });
        List<Control> controles6 = new List<Control>(new Control[] { this.textDataInicioGalgoPlCotaWcf, this.textDataFimGalgoPlCotaWcf });

        switch (tipo)
        {
            #region Formato ddmmyyyy

            case (int)TipoExportacao_FormatoDDMMYYYY.BacenJud:
                if (base.TestaObrigatorio(controles1) != "") throw new Exception("Data Bacen Jud Obrigatório.");
                break;

            case (int)TipoExportacao_FormatoDDMMYYYY.ContabilFolhamatic:
                if (base.TestaObrigatorio(controles1) != "") throw new Exception("Data Contábil Folhamatic Obrigatório.");
                break;

            case (int)TipoExportacao_FormatoDDMMYYYY.InformeDiario_1_0:
            case (int)TipoExportacao_FormatoDDMMYYYY.InformeDiario_2_0:
            case (int)TipoExportacao_FormatoDDMMYYYY.InformeDiario_3_0:
                if (base.TestaObrigatorio(controles1) != "") throw new Exception("Data Informe Diário Obrigatório.");
                break;

            case (int)TipoExportacao_FormatoDDMMYYYY.InformeAnbima:
                if (base.TestaObrigatorio(controles1) != "") throw new Exception("Data Informe Anbima Obrigatório.");
                break;

            case (int)TipoExportacao_FormatoDDMMYYYY.XMLPosicaoAnbid:
                if (base.TestaObrigatorio(controles1) != "") throw new Exception("Data XML Posição Anbid Obrigatório.");
                break;

            case (int)TipoExportacao_FormatoDDMMYYYY.ContabilSinacor:
                if (base.TestaObrigatorio(controles1) != "") throw new Exception("Data Contábil Sinacor Obrigatório.");
                break;
            case (int)TipoExportacao_FormatoDDMMYYYY.CCOUSinacor:
                if (base.TestaObrigatorio(controles1) != "") throw new Exception("Data CCOU Sinacor Obrigatório.");
                break;
            case (int)TipoExportacao_FormatoDDMMYYYY.YMF_TX3:
                if (base.TestaObrigatorio(controles1) != "") throw new Exception("Data TX3 YMF Obrigatório.");
                break;

            case (int)TipoExportacao_FormatoDDMMYYYY.YMF_TX2:
                if (base.TestaObrigatorio(controles1) != "") throw new Exception("Data TX2 YMF Obrigatório.");
                break;

            case (int)TipoExportacao_FormatoDDMMYYYY.CETIP_Cliente:
                if (base.TestaObrigatorio(controles1) != "") throw new Exception("Data Cetip Cliente Obrigatória.");
                break;

            case (int)TipoExportacao_FormatoDDMMYYYY.CETIP_OperacaoRF:
                if (base.TestaObrigatorio(controles1) != "") throw new Exception("Data Cetip Operação Obrigatória.");
                break;

            case (int)TipoExportacao_FormatoDDMMYYYY.BacenCCS:
                if (base.TestaObrigatorio(controles1) != "") throw new Exception("Data Bacen CCS Obrigatório.");
                break;

            case (int)TipoExportacao_FormatoDDMMYYYY.InformacoesComplementaresCVM:
                if (base.TestaObrigatorio(controles1) != "") throw new Exception("Data Info. Complementares - CVM Obrigatório.");

                DateTime dataInfoComplementarTela = Convert.ToDateTime(this.textData_ddmmyyyy.Value);

                List<string> clientesInfoNaoProcessados = new List<string>();

                for (int i = 0; i < keyValuesId.Count; i++)
                {
                    bool processado = this.isClientePossueDadosComplementares(Convert.ToInt32(keyValuesId[i]), dataInfoComplementarTela);

                    if (!processado)
                    {
                        clientesInfoNaoProcessados.Add(Convert.ToString(keyValuesId[i]));
                    }
                }
                if (clientesInfoNaoProcessados.Count > 0)
                {
                    string clientes = string.Join(",", clientesInfoNaoProcessados.ToArray());
                    //
                    string msg = clientesInfoNaoProcessados.Count >= 2
                                 ? "Clientes " + clientes + " não possuem dados de informações Complementares de Fundo cadastrado menores ou iguais a data " + dataInfoComplementarTela.ToString("d")
                                 : "Cliente " + clientes + " não possue dados de informações Complementares de Fundo cadastrado menor ou igual a data " + dataInfoComplementarTela.ToString("d");
                    //
                    throw new Exception(msg);
                }

                break;

            case (int)TipoExportacao_FormatoDDMMYYYY.InformacoesComplementaresCVM_XML:
                if (base.TestaObrigatorio(controles1) != "") throw new Exception("Data Info. Complementares - CVM Obrigatório.");

                DateTime dataInfoComplementarTelaXML = Convert.ToDateTime(this.textData_ddmmyyyy.Value);

                List<string> clientesInfoNaoProcessadosXML = new List<string>();

                for (int i = 0; i < keyValuesId.Count; i++)
                {
                    bool processado = this.isClientePossueDadosComplementares(Convert.ToInt32(keyValuesId[i]), dataInfoComplementarTelaXML);

                    if (!processado)
                    {
                        clientesInfoNaoProcessadosXML.Add(Convert.ToString(keyValuesId[i]));
                    }
                }
                if (clientesInfoNaoProcessadosXML.Count > 0)
                {
                    string clientes = string.Join(",", clientesInfoNaoProcessadosXML.ToArray());
                    //
                    string msg = clientesInfoNaoProcessadosXML.Count >= 2
                                 ? "Clientes " + clientes + " não possuem dados de informações Complementares de Fundo cadastrado menores ou iguais a data " + dataInfoComplementarTelaXML.ToString("d")
                                 : "Cliente " + clientes + " não possue dados de informações Complementares de Fundo cadastrado menor ou igual a data " + dataInfoComplementarTelaXML.ToString("d");
                    //
                    throw new Exception(msg);
                }

                break;
            #endregion

            #region Formato mmyyyy

            case (int)TipoExportacao_FormatoMMYY.CDA20:
                if (base.TestaObrigatorio(controles2) != "") throw new Exception("Data CDA 2.0 Obrigatório.");
                break;
            case (int)TipoExportacao_FormatoMMYY.CDA30:
                if (base.TestaObrigatorio(controles2) != "") throw new Exception("Data CDA 3.0 Obrigatório.");
                break;
            case (int)TipoExportacao_FormatoMMYY.Picl:
                if (base.TestaObrigatorio(controles2) != "") throw new Exception("Data Picl Obrigatório.");
                break;
            case (int)TipoExportacao_FormatoMMYY.PerfilMensalCVM_PDF:
            case (int)TipoExportacao_FormatoMMYY.PerfilMensalCVM_PDF_V3:
            case (int)TipoExportacao_FormatoMMYY.PerfilMensalCVM: // XML v1
            case (int)TipoExportacao_FormatoMMYY.PerfilMensalCVM_XML_V3: // XML v3
                if (base.TestaObrigatorio(controles2) != "") throw new Exception("Data Tabela Perfil Mensal CVM Obrigatório.");
                break;
            case (int)TipoExportacao_FormatoMMYY.RankingAnbimaAtivo:
                if (base.TestaObrigatorio(controles2) != "") throw new Exception("Data Ranking Anbima Obrigatório.");
                break;
            case (int)TipoExportacao_FormatoMMYY.RankingAnbimaPassivo:
                if (base.TestaObrigatorio(controles2) != "") throw new Exception("Data Ranking Anbima Obrigatório.");
                break;
            case (int)TipoExportacao_FormatoMMYY.LaminaPDF:
            case (int)TipoExportacao_FormatoMMYY.LaminaPDF_V1:
            case (int)TipoExportacao_FormatoMMYY.LaminaXML:
            case (int)TipoExportacao_FormatoMMYY.LaminaXML_V1:
                if (base.TestaObrigatorio(controles2) != "") throw new Exception("Data Lâmina Informações Essenciais Obrigatório.");

                DateTime dataTela = Convert.ToDateTime(this.textData_mmyyyy.Value);
                DateTime dataUltima = Calendario.RetornaUltimoDiaUtilMes(dataTela, 0);

                List<string> clientesNaoProcessados = new List<string>();
                // Confere se cliente está processado até o ultimo dia util do mes informado
                for (int i = 0; i < keyValuesId.Count; i++)
                {
                    bool processado = this.isClienteProcessadoAteData(Convert.ToInt32(keyValuesId[i]), dataUltima);

                    if (!processado)
                    {
                        clientesNaoProcessados.Add(Convert.ToString(keyValuesId[i]));
                    }
                }
                if (clientesNaoProcessados.Count > 0)
                {
                    string clientes = string.Join(",", clientesNaoProcessados.ToArray());
                    //
                    string msg = clientesNaoProcessados.Count >= 2
                                 ? "Clientes " + clientes + " não processados até " + dataUltima.ToString("d")
                                 : "Cliente " + clientes + " não processado até " + dataUltima.ToString("d");
                    //
                    throw new Exception(msg);
                }

                // Confere Dados da Lamina
                clientesNaoProcessados = new List<string>();
                for (int i = 0; i < keyValuesId.Count; i++)
                {
                    bool processado = this.isClientePossueDadosLamina(Convert.ToInt32(keyValuesId[i]), dataUltima);

                    if (!processado)
                    {
                        clientesNaoProcessados.Add(Convert.ToString(keyValuesId[i]));
                    }
                }
                if (clientesNaoProcessados.Count > 0)
                {
                    string clientes = string.Join(",", clientesNaoProcessados.ToArray());
                    //
                    string msg = clientesNaoProcessados.Count >= 2
                                 ? "Clientes " + clientes + " não possuem dados de Lâmina cadastrado para a data " + dataUltima.ToString("d")
                                 : "Cliente " + clientes + " não possue dados de Lâmina cadastrado para a data " + dataUltima.ToString("d");
                    //
                    throw new Exception(msg);
                }

                break;
            #endregion

            #region Formato yyyy
            case (int)TipoExportacao_FormatoYYYY.Dirf:
                if (base.TestaObrigatorio(controles3) != "") throw new Exception("Data DIRF Obrigatório.");
                break;
            #endregion

            #region Formato Periodo
            case (int)TipoExportacao_FormatoPeriodo.ContabilInfoBanc:
            case (int)TipoExportacao_FormatoPeriodo.ContabilInfoBancContaFull:
                if (base.TestaObrigatorio(controles4) != "") throw new Exception("Datas Início e Fim para Contábil Infobanc Obrigatórias.");
                break;
            case (int)TipoExportacao_FormatoPeriodo.ContabilAtt:
                if (base.TestaObrigatorio(controles4) != "") throw new Exception("Datas Início e Fim para Contábil Att Obrigatórias.");
                break;
            case (int)TipoExportacao_FormatoPeriodo.Galgo_PlCota:
                if (base.TestaObrigatorio(controles5) != "") throw new Exception("Datas Início e Fim para Galgo PL/Cota Obrigatórias.");
                break;
            case (int)TipoExportacao_FormatoPeriodo.Galgo_PlCotaWcf:
                if (base.TestaObrigatorio(controles6) != "") throw new Exception("Datas Início e Fim para Galgo PL/Cota Obrigatórias.");
                break;

            #endregion
        }
    }

    /// <summary>
    /// Retorna se cliente está processado até a data
    /// </summary>
    /// <param name="data"></param>
    /// <returns></returns>
    private bool isClienteProcessadoAteData(int idCliente, DateTime data)
    {
        // Carrega o Cliente
        Cliente c = new Cliente();
        List<esQueryItem> campos = new List<esQueryItem>();
        campos.Add(c.Query.DataDia);
        //
        c.LoadByPrimaryKey(campos, idCliente);

        return c.DataDia >= data;
    }

    /// <summary>
    /// Retorna se cliente tem dados de lamina cadastrados para um periodo de vigência
    /// </summary>
    /// <param name="data"></param>
    /// <returns></returns>
    private bool isClientePossueDadosLamina(int idCliente, DateTime data)
    {
        LaminaCollection laminaCollection = new LaminaCollection();

        // Busca todas as laminas menores que a data de referencia
        laminaCollection.Query
             .Where(laminaCollection.Query.InicioVigencia <= data,
                    laminaCollection.Query.IdCarteira == idCliente);

        laminaCollection.Query.Load();

        return laminaCollection.HasData;
    }

    /// <summary>
    /// Retorna se cliente tem dados de Informações Complementares para um periodo de vigência
    /// </summary>
    /// <param name="data"></param>
    /// <returns></returns>
    private bool isClientePossueDadosComplementares(int idCliente, DateTime data)
    {
        InformacoesComplementaresFundoCollection infoCollection = new InformacoesComplementaresFundoCollection();

        // Busca todas as informações complementares menores que a data de referencia
        infoCollection.Query
             .Where(infoCollection.Query.DataInicioVigencia <= data,
                    infoCollection.Query.IdCarteira == idCliente);

        infoCollection.Query.Load();

        return infoCollection.HasData;
    }

    /// <summary>
    /// Seleciona o relatorio a ser executado
    /// </summary>
    /// <param name="tipo">Arquivo que se quer exportar</param>
    private void SelecionaExportacao(int tipo)
    {
        Response.BufferOutput = true;
        //
        // Salva os parametros na session
        DateTime dataExportacao;
        List<object> keyValuesId;
        List<int> idClientes;

        switch (tipo)
        {
            #region Formato ddmmyyyy
            #region BacenJud
            case (int)TipoExportacao_FormatoDDMMYYYY.BacenJud:
                MemoryStream msBacenJud = new MemoryStream();
                string nomeArquivoBacenJud;
                //                
                this.ProcessaBacenJud(out msBacenJud, out nomeArquivoBacenJud);
                //
                // Salva Memory Stream do Arquivo txt na Session           
                Session["streamArquivoBacenJud"] = msBacenJud;
                Session["nomeArquivoBacenJud"] = nomeArquivoBacenJud;
                //
                Session["textData"] = this.textData_ddmmyyyy.Text;
                break;
            #endregion

            #region BacenCCS
            case (int)TipoExportacao_FormatoDDMMYYYY.BacenCCS:
                MemoryStream msBacenCCS = new MemoryStream();
                string nomeArquivoBacenCCS;
                //                
                this.ProcessaBacenCCS(out msBacenCCS, out nomeArquivoBacenCCS);
                //
                // Salva Memory Stream do Arquivo txt na Session           
                Session["streamArquivoBacenCCS"] = msBacenCCS;
                Session["nomeArquivoBacenCCS"] = nomeArquivoBacenCCS;
                //
                Session["textData"] = this.textData_ddmmyyyy.Text;
                break;
            #endregion

            #region Contabil Folhamatic
            case (int)TipoExportacao_FormatoDDMMYYYY.ContabilFolhamatic:
                MemoryStream[] memoryStreams;
                List<string> nomeArquivos = new List<string>();
                //
                this.ProcessaContabilFolhamatic(out memoryStreams, out nomeArquivos);

                // Salva Memory Stream do Arquivo Excel na Session                
                Session["streamContabil"] = memoryStreams;
                Session["nomeArquivosContabil"] = nomeArquivos;

                //
                Session["textData"] = this.textData_ddmmyyyy.Text;
                //                
                break;
            #endregion

            #region YMF TX2
            case (int)TipoExportacao_FormatoDDMMYYYY.YMF_TX2:
                Session["textData"] = this.textData_ddmmyyyy.Text;

                dataExportacao = DateTime.ParseExact(this.textData_ddmmyyyy.Text, "dd/MM/yyyy", null);

                keyValuesId = gridConsulta.GetSelectedFieldValues("IdCliente");
                idClientes = keyValuesId.ConvertAll<int>(delegate(object v) { return Convert.ToInt32(v); });
                Session["idClientes"] = idClientes;

                foreach (int idCliente in idClientes)
                {
                    Cliente cliente = new Cliente();
                    cliente.LoadByPrimaryKey(idCliente);

                    if (cliente.DataDia.Value.Date < dataExportacao)
                    {
                        throw new Exception("O fundo " +
                            cliente.Nome.Trim() + " se encontra em data anterior à " + dataExportacao.ToString("dd/MM/yyyy"));
                    }
                    else if (cliente.DataDia.Value.Date == dataExportacao.Date && cliente.Status != (byte)StatusCliente.Divulgado)
                    {
                        throw new Exception("O fundo " +
                            cliente.Nome.Trim() + " não se encontra fechado na data " + dataExportacao.ToString("dd/MM/yyyy"));
                    }
                }
                //                
                break;
            #endregion

            #region YMF TX3
            case (int)TipoExportacao_FormatoDDMMYYYY.YMF_TX3:
                Session["textData"] = this.textData_ddmmyyyy.Text;

                dataExportacao = DateTime.ParseExact(this.textData_ddmmyyyy.Text, "dd/MM/yyyy", null);

                keyValuesId = gridConsulta.GetSelectedFieldValues("IdCliente");
                idClientes = keyValuesId.ConvertAll<int>(delegate(object v) { return Convert.ToInt32(v); });
                Session["idClientes"] = idClientes;

                foreach (int idCliente in idClientes)
                {

                    Cliente cliente = new Cliente();
                    cliente.LoadByPrimaryKey(idCliente);

                    if (cliente.DataDia.Value.Date < dataExportacao)
                    {
                        throw new Exception("O fundo " +
                            cliente.Nome.Trim() + " se encontra em data anterior à " + dataExportacao.ToString("dd/MM/yyyy"));
                    }
                    else if (cliente.DataDia.Value.Date == dataExportacao.Date && cliente.Status != (byte)StatusCliente.Divulgado)
                    {
                        throw new Exception("O fundo " +
                            cliente.Nome.Trim() + " não se encontra fechado na data " + dataExportacao.ToString("dd/MM/yyyy"));
                    }

                }
                break;
            #endregion

            #region Cetip Cliente
            case (int)TipoExportacao_FormatoDDMMYYYY.CETIP_Cliente:
                Session["textData"] = this.textData_ddmmyyyy.Text;
                dataExportacao = DateTime.ParseExact(this.textData_ddmmyyyy.Text, "dd/MM/yyyy", null);

                break;
            #endregion

            #region Cetip Operacao
            case (int)TipoExportacao_FormatoDDMMYYYY.CETIP_OperacaoRF:
                Session["textData"] = this.textData_ddmmyyyy.Text;
                dataExportacao = DateTime.ParseExact(this.textData_ddmmyyyy.Text, "dd/MM/yyyy", null);

                break;
            #endregion

            #region Contabil Sinacor
            case (int)TipoExportacao_FormatoDDMMYYYY.ContabilSinacor:
                MemoryStream memoryStreamSinacor;
                string nomeArquivoSinacor = "";
                //
                this.ProcessaContabilSinacor(out memoryStreamSinacor, out nomeArquivoSinacor);

                // Salva Memory Stream do Arquivo txt na Session                
                Session["streamContabilSinacor"] = memoryStreamSinacor;
                Session["nomeArquivoContabilSinacor"] = nomeArquivoSinacor;

                //
                Session["textData"] = this.textData_ddmmyyyy.Text;
                //                
                break;
            #endregion

            #region CCOU Sinacor
            case (int)TipoExportacao_FormatoDDMMYYYY.CCOUSinacor:
                MemoryStream memoryStreamCCOUSinacor;
                string nomeArquivoCCOUSinacor = "";
                //
                this.ProcessaCCOUSinacor(out memoryStreamCCOUSinacor, out nomeArquivoCCOUSinacor);

                // Salva Memory Stream do Arquivo txt na Session                
                Session["streamCCOUSinacor"] = memoryStreamCCOUSinacor;
                Session["nomeArquivoCCOUSinacor"] = nomeArquivoCCOUSinacor;

                //
                Session["textData"] = this.textData_ddmmyyyy.Text;
                //                
                break;
            #endregion

            #region InformeDiario_1_0
            case (int)TipoExportacao_FormatoDDMMYYYY.InformeDiario_1_0:
                MemoryStream msInformeDiario = new MemoryStream();
                string nomeArquivoInformeDiario;
                //                
                this.ProcessaInformeDiario_1_0(out msInformeDiario, out nomeArquivoInformeDiario);
                //
                // Salva Memory Stream do Arquivo txt na Session           
                Session["streamArquivoInformeDiario_1_0"] = msInformeDiario;
                Session["nomeArquivoInformeDiario_1_0"] = nomeArquivoInformeDiario;
                //
                Session["textData"] = this.textData_ddmmyyyy.Text;
                break;
            #endregion


            #region InformeDiario_2_0
            case (int)TipoExportacao_FormatoDDMMYYYY.InformeDiario_2_0:
                MemoryStream msInformeDiario_2_0 = new MemoryStream();
                string nomeArquivoInformeDiario_2_0;
                //                
                this.ProcessaInformeDiario_2_0(out msInformeDiario_2_0, out nomeArquivoInformeDiario_2_0);
                //
                // Salva Memory Stream do Arquivo txt na Session           
                Session["streamArquivoInformeDiario_2_0"] = msInformeDiario_2_0;
                Session["nomeArquivoInformeDiario_2_0"] = nomeArquivoInformeDiario_2_0;
                //
                Session["textData"] = this.textData_ddmmyyyy.Text;
                break;
            #endregion

            #region InformeDiario_3_0
            case (int)TipoExportacao_FormatoDDMMYYYY.InformeDiario_3_0:
                MemoryStream msInformeDiario_3_0 = new MemoryStream();
                string nomeArquivoInformeDiario_3_0;
                //                
                this.ProcessaInformeDiario_3_0(out msInformeDiario_3_0, out nomeArquivoInformeDiario_3_0);
                //
                // Salva Memory Stream do Arquivo txt na Session           
                Session["streamArquivoInformeDiario_3_0"] = msInformeDiario_3_0;
                Session["nomeArquivoInformeDiario_3_0"] = nomeArquivoInformeDiario_3_0;
                //
                Session["textData"] = this.textData_ddmmyyyy.Text;
                break;
            #endregion

            #region Informe Anbima
            case (int)TipoExportacao_FormatoDDMMYYYY.InformeAnbima:
                MemoryStream msInformeAnbid = new MemoryStream();
                string nomeArquivoInformeAnbid;
                //                
                this.ProcessaInformeAnbima(out msInformeAnbid, out nomeArquivoInformeAnbid);
                //
                // Salva Memory Stream do Arquivo txt na Session           
                Session["streamArquivoInformeAnbid"] = msInformeAnbid;
                Session["nomeArquivoInformeAnbid"] = nomeArquivoInformeAnbid;
                //
                Session["textData"] = this.textData_ddmmyyyy.Text;
                break;
            #endregion

            #region XMLPosicaoAnbid
            case (int)TipoExportacao_FormatoDDMMYYYY.XMLPosicaoAnbid:
                MemoryStream msXMLAnbima = new MemoryStream();
                string nomeArquivoXMLAnbima;
                //                
                this.ProcessaXMLPosicaoAnbima(out msXMLAnbima, out nomeArquivoXMLAnbima);
                //
                // Salva Memory Stream do Arquivo xml na Session           
                Session["streamArquivoXMLAnbid"] = msXMLAnbima;
                Session["nomeArquivoXMLAnbima"] = nomeArquivoXMLAnbima;
                //
                Session["textData"] = this.textData_ddmmyyyy.Text;
                break;

                return;
            #endregion

            #region MateraCC
            //case TipoExportacao.MateraCC:
            //    MateraCC materaCC = new MateraCC();
            //    materaCC.ExportaLiquidacaoMatera(Convert.ToDateTime(this.textDataMateraCC.Text));
            //    return;
            #endregion

            #region MatrizAtivos
            case (int)TipoExportacao_FormatoDDMMYYYY.Matriz_Ativos:
                MemoryStream ms = this.ProcessaMatrizAtivos();
                // Salva Memory Stream do Arquivo Excel na Session                
                Session["streamArquivo"] = ms;
                //
                Session["textData"] = this.textData_ddmmyyyy.Text;
                //              
                break;
            #endregion

            #region Informacoes Complementares
            case (int)TipoExportacao_FormatoDDMMYYYY.InformacoesComplementaresCVM:
                // Dicionario Nome do Arquivo - Memory Stream do arquivo
                Dictionary<string, MemoryStream> retInfoComplementares = new Dictionary<string, MemoryStream>();
                //                
                this.ProcessaInfoComplementar(out retInfoComplementares);
                //
                // Salva Dicionary na Session           
                Session["streamInfoComplementares"] = retInfoComplementares;
                //
                Session["textData"] = this.textData_ddmmyyyy.Value;
                break;
            #endregion

            #region Informacoes Complementares XML
            case (int)TipoExportacao_FormatoDDMMYYYY.InformacoesComplementaresCVM_XML:
                MemoryStream msInfoComplementar = new MemoryStream();
                string nomeArquivoInfoComplementar;
                //                
                this.ProcessaInfoComplementarXML(out msInfoComplementar, out nomeArquivoInfoComplementar);
                //
                // Salva Memory Stream do Arquivo txt na Session           
                Session["streamArquivoInfoComplementar"] = msInfoComplementar;
                Session["nomeArquivoInfoComplementar"] = nomeArquivoInfoComplementar;
                //
                Session["textData"] = this.textData_ddmmyyyy.Text;
                break;
            #endregion

            #endregion

            #region Formato mmyyyy
            #region CDA 2.0
            case (int)TipoExportacao_FormatoMMYY.CDA20:
                // Dicionario Nome do Arquivo - Memory Stream do arquivo
                Dictionary<string, MemoryStream> retCDA20 = new Dictionary<string, MemoryStream>();
                //                
                this.ProcessaCDA20(out retCDA20);
                //
                // Salva Dicionary na Session           
                Session["streamCDA"] = retCDA20;
                //
                Session["textData"] = this.textData_mmyyyy.Value; // Repassa 01/01/Ano
                break;
            #endregion

            #region Cosif4016
            case (int)TipoExportacao_FormatoMMYY.Cosif4016:
                MemoryStream msCosif4016 = new MemoryStream();
                string nomeArquivoCosif4016;

                this.ProcessaCosif4016(out msCosif4016, out nomeArquivoCosif4016);
                //
                // Salva Memory Stream do Arquivo txt na Session           
                Session["streamArquivoCosif"] = msCosif4016;
                Session["nomeArquivoCosif"] = nomeArquivoCosif4016;
                //
                Session["textData"] = this.textData_mmyyyy.Value;
                break;
            #endregion

            #region Cosif4010
            case (int)TipoExportacao_FormatoMMYY.Cosif4010:
                MemoryStream msCosif4010 = new MemoryStream();
                string nomeArquivoCosif4010;

                this.ProcessaCosif4010(out msCosif4010, out nomeArquivoCosif4010);
                //
                // Salva Memory Stream do Arquivo txt na Session           
                Session["streamArquivoCosif"] = msCosif4010;
                Session["nomeArquivoCosif"] = nomeArquivoCosif4010;
                //
                Session["textData"] = this.textData_mmyyyy.Value;
                break;
            #endregion

            #region CDA 3.0
            case (int)TipoExportacao_FormatoMMYY.CDA30:
                // Dicionario Nome do Arquivo - Memory Stream do arquivo
                Dictionary<string, MemoryStream> retCDA30 = new Dictionary<string, MemoryStream>();
                //                
                this.ProcessaCDA30(out retCDA30);
                //
                // Salva Dicionary na Session           
                Session["streamCDA"] = retCDA30;
                //
                Session["textData"] = this.textData_mmyyyy.Value; // Repassa 01/01/Ano
                break;
            #endregion

            #region CDA 4.0
            case (int)TipoExportacao_FormatoMMYY.CDA40:
                // Dicionario Nome do Arquivo - Memory Stream do arquivo
                Dictionary<string, MemoryStream> retCda40;
                ProcessaCda40(out retCda40);
                // Salva Dicionary na Session           
                Session["streamCDA"] = retCda40;
                Session["textData"] = this.textData_mmyyyy.Value; // Repassa 01/01/Ano
                break;
            #endregion

            #region Picl
            case (int)TipoExportacao_FormatoMMYY.Picl:
                MemoryStream msPicl = new MemoryStream();
                string nomeArquivoPicl;
                //                
                this.ProcessaPicl(out msPicl, out nomeArquivoPicl);
                //
                // Salva Memory Stream do Arquivo txt na Session           
                Session["streamArquivoPicl"] = msPicl;
                Session["nomeArquivoPicl"] = nomeArquivoPicl;
                //
                Session["textData"] = this.textData_mmyyyy.Value;
                break;
            #endregion

            #region Tabela Perfil Mensal
            case (int)TipoExportacao_FormatoMMYY.PerfilMensalCVM:
                MemoryStream msTabelaPerfil = new MemoryStream();
                string nomeArquivoTabelaPerfil;
                //                
                this.ProcessaTabelaPerfilMensal(out msTabelaPerfil, out nomeArquivoTabelaPerfil);
                //
                // Salva Memory Stream do Arquivo xml na Session           
                Session["streamArquivoTabelaPerfilMensal"] = msTabelaPerfil;
                Session["nomeArquivoTabelaPerfilMensal"] = nomeArquivoTabelaPerfil;
                //

                DateTime dAux = Convert.ToDateTime(this.textData_mmyyyy.Value);

                DateTime d = Calendario.RetornaUltimoDiaUtilMes(dAux);
                //
                Session["textData"] = d;
                break;
            #endregion

            #region Perfil Mensal XML 3.0
            case (int)TipoExportacao_FormatoMMYY.PerfilMensalCVM_XML_V3:
                MemoryStream msTabelaPerfil_XMl_3 = new MemoryStream();
                string nomeArquivoTabelaPerfil_XMl_3;
                //                
                this.ProcessaTabelaPerfilMensal_XMl_3(out msTabelaPerfil_XMl_3, out nomeArquivoTabelaPerfil_XMl_3);
                //
                // Salva Memory Stream do Arquivo xml na Session           
                Session["streamArquivoTabelaPerfilMensal_XMl_3"] = msTabelaPerfil_XMl_3;
                Session["nomeArquivoTabelaPerfilMensal_XMl_3"] = nomeArquivoTabelaPerfil_XMl_3;
                //

                DateTime dAux_XMl_3 = Convert.ToDateTime(this.textData_mmyyyy.Value);

                DateTime d_XMl_3 = Calendario.RetornaUltimoDiaUtilMes(dAux_XMl_3);
                //
                Session["textData"] = d_XMl_3;
                break;
            #endregion

            #region RankingAnbima
            case (int)TipoExportacao_FormatoMMYY.RankingAnbimaAtivo:
                RankingAnbima rankingAnbima = new RankingAnbima();
                keyValuesId = gridConsulta.GetSelectedFieldValues("IdCliente");
                idClientes = keyValuesId.ConvertAll<int>(delegate(object v) { return Convert.ToInt32(v); });
                rankingAnbima.ExportaRankingAnbimaAtivo(Convert.ToDateTime(this.textData_mmyyyy.Text), idClientes);
                // Salva Memory Stream do Arquivo txt na Session           
                Session["streamArquivoRankingAnbima"] = rankingAnbima.ExcelMemoryStream;
                Session["nomeArquivoRankingAnbima"] = rankingAnbima.NomeArquivoExcel;

                break;
            case (int)TipoExportacao_FormatoMMYY.RankingAnbimaPassivo:
                RankingAnbima rankingAnbimaPassivo = new RankingAnbima();
                keyValuesId = gridConsulta.GetSelectedFieldValues("IdCliente");
                idClientes = keyValuesId.ConvertAll<int>(delegate(object v) { return Convert.ToInt32(v); });
                rankingAnbimaPassivo.ExportaRankingAnbimaPassivo(Convert.ToDateTime(this.textData_mmyyyy.Text), idClientes);
                // Salva Memory Stream do Arquivo txt na Session           
                Session["streamArquivoRankingAnbima"] = rankingAnbimaPassivo.ExcelMemoryStream;
                Session["nomeArquivoRankingAnbima"] = rankingAnbimaPassivo.NomeArquivoExcel;

                break;
            #endregion

            #region MensalClube
            //case TipoExportacao.MensalClube:
            //    MemoryStream ms1, ms2 = new MemoryStream();
            //    string nomeArquivo1, nomeArquivo2;
            //    //                
            //    this.ProcessaMensalClubes(out ms1, out nomeArquivo1, out ms2, out nomeArquivo2);
            //    //
            //    // Salva Memory Stream do Arquivo txt na Session                
            //    Session["streamArquivoMensalClube"] = ms1;
            //    Session["streamArquivoAtivoClube"] = ms2;
            //    Session["nomeArquivoMensalClube"] = nomeArquivo1;
            //    Session["nomeArquivoAtivoClube"] = nomeArquivo2;
            //    //
            //    Session["textData"] = this.textDataMensalClube.Value; // Repassa 01/Mes/Ano
            //    break;
            #endregion

            #region LaminaPDF
            case (int)TipoExportacao_FormatoMMYY.LaminaPDF:
                // Dicionario Nome do Arquivo - Memory Stream do arquivo
                Dictionary<string, MemoryStream> retLamina = new Dictionary<string, MemoryStream>();
                //                
                this.ProcessaLamina(out retLamina);
                //
                // Salva Dicionary na Session           
                Session["streamLamina"] = retLamina;
                //
                Session["textData"] = this.textData_mmyyyy.Value; // Repassa 01/01/Ano
                break;
            #endregion

            #region Lamina XML
            case (int)TipoExportacao_FormatoMMYY.LaminaXML:
                MemoryStream msLamina = new MemoryStream();
                string nomeArquivoLamina;
                //                
                this.ProcessaLaminaXML(out msLamina, out nomeArquivoLamina);
                //
                // Salva Memory Stream do Arquivo txt na Session           
                Session["streamArquivoLamina"] = msLamina;
                Session["nomeArquivoLamina"] = nomeArquivoLamina;
                //
                Session["textData"] = this.textData_mmyyyy.Value; // Repassa 01/Mes/Ano
                break;
            #endregion

            #region LaminaPDF_V1
            case (int)TipoExportacao_FormatoMMYY.LaminaPDF_V1:
                // Dicionario Nome do Arquivo - Memory Stream do arquivo
                Dictionary<string, MemoryStream> retLaminaV1 = new Dictionary<string, MemoryStream>();
                //                
                this.ProcessaLaminaV1(out retLaminaV1);
                //
                // Salva Dicionary na Session           
                Session["streamLaminaV1"] = retLaminaV1;
                //
                Session["textData"] = this.textData_mmyyyy.Value; // Repassa 01/01/Ano
                break;
            #endregion

            #region Lamina XML_V1
            case (int)TipoExportacao_FormatoMMYY.LaminaXML_V1:
                MemoryStream msLaminaV1 = new MemoryStream();
                string nomeArquivoLaminaV1;
                //                
                this.ProcessaLaminaXMLV1(out msLaminaV1, out nomeArquivoLaminaV1);
                //
                // Salva Memory Stream do Arquivo txt na Session           
                Session["streamArquivoLaminaV1"] = msLaminaV1;
                Session["nomeArquivoLaminaV1"] = nomeArquivoLaminaV1;
                //
                Session["textData"] = this.textData_mmyyyy.Value; // Repassa 01/Mes/Ano
                break;
            #endregion

            #region Tabela Perfil Mensal V1 PDF
            case (int)TipoExportacao_FormatoMMYY.PerfilMensalCVM_PDF:
                // Dicionario Nome do Arquivo - Memory Stream do arquivo
                Dictionary<string, MemoryStream> retTabelaPerfil = new Dictionary<string, MemoryStream>();
                //                
                this.ProcessaTabelaPerfilMensalPDF(out retTabelaPerfil);
                //
                // Salva Dicionary na Session           
                Session["streamTabelaPerfil"] = retTabelaPerfil;
                //
                DateTime dAux1 = Convert.ToDateTime(this.textData_mmyyyy.Value);
                DateTime d1 = Calendario.RetornaUltimoDiaUtilMes(dAux1);
                //
                Session["textData"] = d1;

                break;
            #endregion

            #region Tabela Perfil Mensal V3 PDF
            case (int)TipoExportacao_FormatoMMYY.PerfilMensalCVM_PDF_V3:
                // Dicionario Nome do Arquivo - Memory Stream do arquivo
                Dictionary<string, MemoryStream> retTabelaPerfil_PDF_3 = new Dictionary<string, MemoryStream>();
                //                
                this.ProcessaTabelaPerfilMensalPDF_3(out retTabelaPerfil_PDF_3);
                //
                // Salva Dicionary na Session           
                Session["streamTabelaPerfilPDF_3"] = retTabelaPerfil_PDF_3;
                //
                DateTime dAux1_PDF_3 = Convert.ToDateTime(this.textData_mmyyyy.Value);
                DateTime d2 = Calendario.RetornaUltimoDiaUtilMes(dAux1_PDF_3);
                //
                Session["textData"] = d2;

                break;
            #endregion
            #endregion

            #region Formato yyyy
            #region Dirf
            case (int)TipoExportacao_FormatoYYYY.Dirf:
                MemoryStream msDirf = new MemoryStream();
                string nomeArquivoDirf;
                //                
                this.ProcessaDirf(out msDirf, out nomeArquivoDirf);
                //
                // Salva Memory Stream do Arquivo txt na Session           
                Session["streamArquivoDirf"] = msDirf;
                Session["nomeArquivoDirf"] = nomeArquivoDirf;
                //
                Session["textData"] = this.textData_yyyy.Value; // Repassa 01/01/Ano
                break;
            #endregion
            #region Informe Rend.
            case (int)TipoExportacao_FormatoYYYY.InformeRendimento:
                MemoryStream msInformeRendimento = new MemoryStream();
                string nomeArquivoInformeRendimento;
                //                
                this.ProcessaInformeRendimento(out msInformeRendimento, out nomeArquivoInformeRendimento);
                //
                // Salva Memory Stream do Arquivo txt na Session           
                Session["streamArquivoInformeRendimento"] = msInformeRendimento;
                Session["nomeArquivoInformeRendimento"] = nomeArquivoInformeRendimento;
                //
                Session["textData"] = this.textData_yyyy.Value; // Repassa 01/01/Ano
                break;
            #endregion
            #endregion

            #region Formato Periodo
            #region Contabil Att
            case (int)TipoExportacao_FormatoPeriodo.ContabilAtt:

                MemoryStream msContabLancamentoAtt = new MemoryStream();
                string nomeArquivoContabLancamentoAtt;
                //
                List<object> keyValuesIdAtt = gridConsulta.GetSelectedFieldValues("IdCliente");
                List<int> idClientesAtt = keyValuesIdAtt.ConvertAll<int>(delegate(object v) { return Convert.ToInt32(v); });
                //
                ContabilAtt contabilAtt = new ContabilAtt();
                contabilAtt.ExportaContabLancamento(Convert.ToDateTime(this.textDataInicio_Periodo.Text),
                                                    Convert.ToDateTime(this.textDataFim_Periodo.Text),
                                                    idClientesAtt,
                                                    out msContabLancamentoAtt, out nomeArquivoContabLancamentoAtt);

                // Salva Memory Stream do Arquivo txt na Session           
                Session["streamArquivoContabilAtt"] = msContabLancamentoAtt;
                Session["nomeArquivoContabilAtt"] = nomeArquivoContabLancamentoAtt;
                //                

                break;
            #endregion

            #region Contabil Infobanc
            case (int)TipoExportacao_FormatoPeriodo.ContabilInfoBanc:

                // Dicionario Nome do Arquivo - Memory Stream do arquivo
                Dictionary<string, MemoryStream> retInfoBanc = new Dictionary<string, MemoryStream>();
                //
                List<object> keyValuesIdInfobanc = gridConsulta.GetSelectedFieldValues("IdCliente");
                List<int> idClientesInfobanc = keyValuesIdInfobanc.ConvertAll<int>(delegate(object v) { return Convert.ToInt32(v); });
                //
                new ContabilInfobanc().ExportaContabLancamento(Convert.ToDateTime(this.textDataInicio_Periodo.Text),
                                                               Convert.ToDateTime(this.textDataFim_Periodo.Text),
                                                               idClientesInfobanc,
                                                               out retInfoBanc);

                // Salva Memory Stream do Arquivo Excel na Session
                Session["streamContabilInfoBanc"] = retInfoBanc;
                //                
                break;

            #endregion

            #region Contabil Infobanc - Conta Full
            case (int)TipoExportacao_FormatoPeriodo.ContabilInfoBancContaFull:

                // Dicionario Nome do Arquivo - Memory Stream do arquivo
                Dictionary<string, MemoryStream> retInfoBancContaFull = new Dictionary<string, MemoryStream>();
                //
                List<object> keyValuesIdInfoBancContaFull = gridConsulta.GetSelectedFieldValues("IdCliente");
                List<int> idClientesInfoBancContaFull = keyValuesIdInfoBancContaFull.ConvertAll<int>(delegate(object v) { return Convert.ToInt32(v); });
                //
                new ContabilInfoBancContaFull().ExportaContabLancamento(Convert.ToDateTime(this.textDataInicio_Periodo.Text),
                                                               Convert.ToDateTime(this.textDataFim_Periodo.Text),
                                                               idClientesInfoBancContaFull,
                                                               out retInfoBancContaFull);
                break;

            #endregion

            #region Galgo_PlCota
            case (int)TipoExportacao_FormatoPeriodo.Galgo_PlCota:
            case (int)TipoExportacao_FormatoPeriodo.Galgo_PlCotaWcf:

                MemoryStream memoryStreamGalgoPlCota = new MemoryStream();
                string nomeArquivoGalgoPlCota;

                keyValuesId = gridConsultaGalgo.GetSelectedFieldValues("IdCliente");
                idClientes = keyValuesId.ConvertAll<int>(delegate(object v) { return Convert.ToInt32(v); });

                DateTime dataInicial = DateTime.ParseExact(Convert.ToString(this.textDataInicioGalgoPlCotaArquivo.Text), "dd/MM/yyyy", null);
                DateTime dataFinal = DateTime.ParseExact(Convert.ToString(this.textDataFimGalgoPlCotaArquivo.Text), "dd/MM/yyyy", null);

                Financial.Fundo.Galgo.InterfaceGalgo galgo = new Financial.Fundo.Galgo.InterfaceGalgo();

                galgo.ExportaPlCota(dataInicial, dataFinal, idClientes, out memoryStreamGalgoPlCota, out nomeArquivoGalgoPlCota);

                Session["memoryStreamGalgoPlCota"] = memoryStreamGalgoPlCota;
                Session["nomeArquivoGalgoPlCota"] = nomeArquivoGalgoPlCota;

                break;

            #endregion
        }
    }

    protected void callbackExporta_Callback(object source, DevExpress.Web.CallbackEventArgs e)
    {
        e.Result = "";

        int? tipo = null;
        if (e.Parameter == "ddmmyyyy")
        {
            if (this.dropTipo_ddmmyyyy.SelectedIndex > 0)
            { // se foi selecionado
                tipo = Convert.ToInt32(this.dropTipo_ddmmyyyy.SelectedItem.Value);
            }
        }
        else if (e.Parameter == "mmyyyy")
        {
            if (this.dropTipo_mmyyyy.SelectedIndex > 0)
            { // se foi selecionado
                tipo = Convert.ToInt32(this.dropTipo_mmyyyy.SelectedItem.Value);
            }
        }
        else if (e.Parameter == "yyyy")
        {
            if (this.dropTipo_yyyy.SelectedIndex > 0)
            { // se foi selecionado
                tipo = Convert.ToInt32(this.dropTipo_yyyy.SelectedItem.Value);
            }

        }
        else if (e.Parameter == "Periodo")
        {
            if (this.dropTipo_Periodo.SelectedIndex > 0)
            { // se foi selecionado
                tipo = Convert.ToInt32(this.dropTipo_Periodo.SelectedItem.Value);
            }
        }
        else if (e.Parameter == "GalgoPlCotaArquivo")
        {
            tipo = (int)TipoExportacao_FormatoPeriodo.Galgo_PlCota;
        }
        else if (e.Parameter == "GalgoPlCotaWcf")
        {
            tipo = (int)TipoExportacao_FormatoPeriodo.Galgo_PlCotaWcf;
        }
        else
        {
            e.Result = "Tipo de data de exportação não suportado";
        }

        if (e.Result != "")
        {
            return;
        }

        try
        {
            this.TrataErros(tipo);
            this.SelecionaExportacao(tipo.Value); // Sempre vai ter valor se chegar aqui
        }
        catch (Exception erro)
        {
            e.Result = erro.Message;
            return;
        }

        string newLocation = HttpContext.Current.Request.Url.AbsolutePath.Replace("Exportacao.aspx",
            "ExibeExportacao.aspx?Tipo=" + tipo);
        e.Result = "redirect:" + newLocation;
    }

    #region Processamento
    /// <summary>
    /// 
    /// </summary>
    /// <param name="memoryStreams">Array de memory stream com os arquivos Contabils</param>
    /// <param name="nomeArquivos">Array com o nome dos arquivos Contabils</param>
    private void ProcessaContabilFolhamatic(out MemoryStream[] memoryStreams, out List<string> nomeArquivos)
    {
        ContabilFolhamatic contabil = new ContabilFolhamatic();
        DateTime data = Convert.ToDateTime(this.textData_ddmmyyyy.Text);
        //
        List<object> keyValuesId = gridConsulta.GetSelectedFieldValues("IdCliente");
        List<int> idClientes = keyValuesId.ConvertAll<int>(delegate(object v) { return Convert.ToInt32(v); });
        //
        try
        {
            memoryStreams = contabil.ExportaContabilFolhamatic(data, idClientes, out nomeArquivos);
        }
        catch (Exception e1)
        {
            string msg = "Arquivo Contábil " + data.ToShortDateString() + " não exportado:  " + e1.Message;
            throw new Exception(msg);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="memoryStreamSinacor">memory stream com o arquivo Contabil Sinacor</param>
    /// <param name="nomeArquivoSinacor">nome do arquivo Contabil Sinacor</param>
    private void ProcessaContabilSinacor(out MemoryStream memoryStreamSinacor, out string nomeArquivoSinacor)
    {
        ContabilSinacor contabilSinacor = new ContabilSinacor();
        DateTime data = Convert.ToDateTime(this.textData_ddmmyyyy.Text);
        //
        List<object> keyValuesId = gridConsulta.GetSelectedFieldValues("IdCliente");
        List<int> idClientes = keyValuesId.ConvertAll<int>(delegate(object v) { return Convert.ToInt32(v); });
        //
        try
        {
            contabilSinacor.ExportaContabLancamento(data, idClientes, out memoryStreamSinacor, out nomeArquivoSinacor);
        }
        catch (Exception e1)
        {
            string msg = "Arquivo Contábil Sinacor " + data.ToShortDateString() + " não exportado:  " + e1.Message;
            throw new Exception(msg);
        }
    }

    private void ProcessaCCOUSinacor(out MemoryStream memoryStreamSinacor, out string nomeArquivoSinacor)
    {
        CCOUSinacor ccouSinacor = new CCOUSinacor();
        DateTime data = Convert.ToDateTime(this.textData_ddmmyyyy.Text);
        //
        List<object> keyValuesId = gridConsulta.GetSelectedFieldValues("IdCliente");
        List<int> idClientes = keyValuesId.ConvertAll<int>(delegate(object v) { return Convert.ToInt32(v); });
        //
        try
        {
            ccouSinacor.ExportaCCOUSinacor(data, idClientes, out memoryStreamSinacor, out nomeArquivoSinacor);
        }
        catch (Exception e1)
        {
            string msg = "Arquivo Contábil Sinacor " + data.ToShortDateString() + " não exportado:  " + e1.Message;
            throw new Exception(msg);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="msXMLAnbima">Saida: Stream do Arquivo XMLAnbima em formato xml</param>
    /// <param name="nomeArquivoXMLAnbima">Saida: Nome do Arquivo XMLAnbima</param>
    private void ProcessaXMLPosicaoAnbima(out MemoryStream msXMLAnbima, out string nomeArquivoXMLAnbima)
    {
        #region XMLPosicaoAnbima
        DateTime data = Convert.ToDateTime(this.textData_ddmmyyyy.Text);

        List<object> keyValuesId = gridConsulta.GetSelectedFieldValues("IdCliente");
        List<int> idClientes = keyValuesId.ConvertAll<int>(delegate(object v) { return Convert.ToInt32(v); });

        XMLPosicaoAnbid xmlAnbid = new XMLPosicaoAnbid();
        //
        try
        {
            xmlAnbid.ExportaCarteiraXMLAnbid(data, idClientes, out msXMLAnbima, out nomeArquivoXMLAnbima);
        }
        catch (Exception e1)
        {
            throw new Exception(e1.Message);
        }
        #endregion
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="ms1">Stream do Arquivo MensalClubes em formato txt</param>
    /// <param name="nomeArquivo1">Nome do arquivo MensalClubes</param>
    /// <param name="ms2">Stream do Arquivo AtivosClubes em formato txt</param>
    /// <param name="nomeArquivo2">Nome do arquivo AtivosClubes</param>
    [Obsolete("Removido")]
    private void ProcessaMensalClubes(out MemoryStream ms1, out string nomeArquivo1,
                                      out MemoryStream ms2, out string nomeArquivo2)
    {
        #region MensalClube
        DateTime data = Convert.ToDateTime(this.textData_mmyyyy.Text);
        data = Calendario.RetornaUltimoDiaUtilMes(data, 0);

        MensalClubes mensalClubes = new MensalClubes();
        AtivosClubes ativosClubes = new AtivosClubes();
        //
        try
        {
            ms1 = mensalClubes.ExportaMensalClubes(data, out nomeArquivo1);
            ms2 = ativosClubes.ExportaAtivosClubes(data, out nomeArquivo2);
        }
        catch (Exception e1)
        {
            throw new Exception(e1.Message);

            // Testando - Mensagem
            //this.mensagem.AppendLine(e1.Message);

            //ms1 = null;
            //ms2 = null;
            //nomeArquivo1 = null;
            //nomeArquivo2 = null;
        }

        #endregion
    }

    /// <summary>
    /// 
    /// </summary>
    /// <returns>Memory Stream de um Arquivo Excel de Matriz Ativos</returns>
    private MemoryStream ProcessaMatrizAtivos()
    {

        DateTime data = Convert.ToDateTime(this.textData_ddmmyyyy.Text);
        //
        List<object> keyValuesId = gridConsulta.GetSelectedFieldValues("IdCliente");
        List<int> idClientes = keyValuesId.ConvertAll<int>(delegate(object v) { return Convert.ToInt32(v); });
        //
        MatrizAtivos matrizAtivos = new MatrizAtivos();
        string nomeArquivo = "MatrizAtivos" + data.ToString("dd/MM/yyyy").Replace("/", "") + ".xls";
        //
        MemoryStream ms = new MemoryStream();
        //
        try
        {
            System.Data.DataTable dtMatriz = matrizAtivos.RetornaMatrizAtivos(data, idClientes);

            // Novo DataTable com no maximo 256 Colunas
            //System.Data.DataTable dtMatrizLimitada = this.CopyDataTable(dtMatriz);

            //this.gridExportacao.DataSource = dtMatrizLimitada;
            //this.gridExportacao.DataSource = dtMatriz;

            //this.gridExportacao.DataBind();

            //this.viewExportacao.WriteXls(ms);

            DevExpress.XtraPrinting.XlsExportOptions a = new DevExpress.XtraPrinting.XlsExportOptions();
            a.Suppress256ColumnsWarning = true;
            this.gridExportacao1.DataSource = dtMatriz;
            this.gridExport.WriteXls(ms, a);
        }
        catch (Exception e1)
        {
            string msg = "Matriz de Ativos com data: " + data.ToString("dd/MM/yyyy") + " não exportado:  " + e1.Message;
            throw new Exception(msg);
        }

        return ms;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="msBacenJud">Saida: Stream do Arquivo BacenJud em formato txt</param>
    /// <param name="nomeArquivoBacenJud">Saida: Nome do Arquivo BacenJud</param>
    private void ProcessaBacenJud(out MemoryStream msBacenJud, out string nomeArquivoBacenJud)
    {
        #region BacenJud
        DateTime data = Convert.ToDateTime(this.textData_ddmmyyyy.Text);

        List<object> keyValuesId = gridConsulta.GetSelectedFieldValues("IdCliente");
        List<int> idClientes = keyValuesId.ConvertAll<int>(delegate(object v) { return Convert.ToInt32(v); });

        BacenJud bacenJud = new BacenJud();
        //
        try
        {
            bacenJud.ExportaBacenJud(data, idClientes, out msBacenJud, out nomeArquivoBacenJud);
        }
        catch (Exception e1)
        {
            throw new Exception(e1.Message);
        }

        #endregion
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="msBacenCCS">Saida: Stream do Arquivo BacenCCS em formato txt</param>
    /// <param name="nomeArquivoBacenJud">Saida: Nome do Arquivo BacenCCS</param>
    private void ProcessaBacenCCS(out MemoryStream msBacenCCS, out string nomeArquivoBacenCCS)
    {
        #region BacenCCS
        DateTime data = Convert.ToDateTime(this.textData_ddmmyyyy.Text);

        List<object> keyValuesId = gridConsulta.GetSelectedFieldValues("IdCliente");
        List<int> idClientes = keyValuesId.ConvertAll<int>(delegate(object v) { return Convert.ToInt32(v); });

        BacenCCS bacenCCS = new BacenCCS();
        //
        try
        {
            bacenCCS.ExportaBacenCCS(data, idClientes, out msBacenCCS, out nomeArquivoBacenCCS);
        }
        catch (Exception e1)
        {
            throw new Exception(e1.Message);
        }

        #endregion
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="msDirf">Saida: Stream do Arquivo Dirf em formato txt</param>
    /// <param name="nomeArquivoDirf">Saida: Nome do Arquivo Dirf</param>
    private void ProcessaDirf(out MemoryStream msDirf, out string nomeArquivoDirf)
    {
        #region Dirf
        DateTime data = Convert.ToDateTime(this.textData_yyyy.Value);

        List<object> keyValuesId = gridConsulta.GetSelectedFieldValues("IdCliente");
        List<int> idClientes = keyValuesId.ConvertAll<int>(delegate(object v) { return Convert.ToInt32(v); });

        Dirf dirf = new Dirf();
        //
        try
        {
            dirf.ExportaDirf(data, idClientes, out msDirf, out nomeArquivoDirf);
        }
        catch (Exception e1)
        {
            throw new Exception(e1.Message);
        }

        #endregion
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="msDirf">Saida: Stream do Arquivo Dirf em formato txt</param>
    /// <param name="nomeArquivoDirf">Saida: Nome do Arquivo Dirf</param>
    private void ProcessaInformeRendimento(out MemoryStream msInformeRend, out string nomeArquivoInformeRend)
    {
        #region Informe Rendimento
        int ano = ((DateTime)this.textData_yyyy.Value).Year;

        List<object> keyValuesId = gridConsulta.GetSelectedFieldValues("IdCliente");
        List<int> idClientes = keyValuesId.ConvertAll<int>(delegate(object v) { return Convert.ToInt32(v); });

        InformeRendimentoExport informeRendimentoExport = new InformeRendimentoExport();

        //
        try
        {
            informeRendimentoExport.ExportaInformeRendimento(ano, idClientes, out msInformeRend, out nomeArquivoInformeRend);
        }
        catch (Exception e1)
        {
            throw new Exception(e1.Message);
        }

        #endregion
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="msDirf">Saida: Stream do Arquivo Dirf em formato txt</param>
    /// <param name="nomeArquivoDirf">Saida: Nome do Arquivo Dirf</param>
    private void ProcessaCosif4016(out MemoryStream msCosif4016, out string nomeArquivoCosif4016)
    {
        #region Cosif
        DateTime data = Convert.ToDateTime(this.textData_mmyyyy.Text);

        List<object> keyValuesId = gridConsulta.GetSelectedFieldValues("IdCliente");
        List<int> idClientes = keyValuesId.ConvertAll<int>(delegate(object v) { return Convert.ToInt32(v); });

        Cosif cosif = new Cosif();

        //
        try
        {
            cosif.Exporta4016(data, idClientes, out msCosif4016, out nomeArquivoCosif4016);
        }
        catch (Exception e1)
        {
            throw new Exception(e1.Message);
        }

        #endregion
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="msDirf">Saida: Stream do Arquivo Dirf em formato txt</param>
    /// <param name="nomeArquivoDirf">Saida: Nome do Arquivo Dirf</param>
    private void ProcessaCosif4010(out MemoryStream msCosif4010, out string nomeArquivoCosif4010)
    {
        #region Cosif
        DateTime data = Convert.ToDateTime(this.textData_mmyyyy.Text);

        List<object> keyValuesId = gridConsulta.GetSelectedFieldValues("IdCliente");
        List<int> idClientes = keyValuesId.ConvertAll<int>(delegate(object v) { return Convert.ToInt32(v); });

        Cosif cosif = new Cosif();

        //
        try
        {
            cosif.Exporta4010(data, idClientes, out msCosif4010, out nomeArquivoCosif4010);
        }
        catch (Exception e1)
        {
            throw new Exception(e1.Message);
        }

        #endregion
    }


    /// <summary>
    /// 
    /// </summary>
    /// <param name="msCDA">Dicionario com o nome do Arquivo - MemoryStream do Arquivo</param>
    private void ProcessaCDA20(out Dictionary<string, MemoryStream> msCDA)
    {
        #region CDA
        DateTime data = Convert.ToDateTime(this.textData_mmyyyy.Text);
        int diasConf = Convert.ToInt16(this.spinDiasConf.Text);

        List<object> keyValuesId = gridConsulta.GetSelectedFieldValues("IdCliente");
        List<int> idClientes = keyValuesId.ConvertAll<int>(delegate(object v) { return Convert.ToInt32(v); });

        CDA20 cda20 = new CDA20();
        //
        try
        {
            cda20.ExportaCDA(data, idClientes, diasConf, out msCDA);
        }
        catch (Exception e1)
        {
            throw new Exception(e1.Message);
        }

        #endregion
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="msCDA">Dicionario com o nome do Arquivo - MemoryStream do Arquivo</param>
    private void ProcessaCDA30(out Dictionary<string, MemoryStream> msCDA)
    {
        #region CDA
        DateTime data = Convert.ToDateTime(this.textData_mmyyyy.Text);
        int diasConf = Convert.ToInt16(this.spinDiasConf.Text);

        List<object> keyValuesId = gridConsulta.GetSelectedFieldValues("IdCliente");
        List<int> idClientes = keyValuesId.ConvertAll<int>(delegate(object v) { return Convert.ToInt32(v); });

        CDA cda30 = new CDA();
        //
        try
        {
            cda30.ExportaCDA(data, idClientes, diasConf, out msCDA);
        }
        catch (Exception e1)
        {
            throw new Exception(e1.Message);
        }

        #endregion
    }

    /// <summary>
    /// Processas the cda40.
    /// </summary>
    /// <param name="msCda">The ms cda.</param>
    /// <exception cref="Exception"></exception>
    private void ProcessaCda40(out Dictionary<string, MemoryStream> msCda)
    {
        #region CDA
        var data = Convert.ToDateTime(this.textData_mmyyyy.Text);
        int diasConf = Convert.ToInt16(this.spinDiasConf.Text);

       var keyValuesId = gridConsulta.GetSelectedFieldValues("IdCliente");
        var  idClientes = keyValuesId.ConvertAll<int>(delegate (object v) { return Convert.ToInt32(v); });

        var cda = new Cda40();
        try
        {
            cda.ExportaCDA(data, idClientes, diasConf, out msCda);
        }
        catch (Exception e1)
        {
            throw new Exception(e1.Message);
        }

        #endregion
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="msInformeAnbid"></param>
    /// <param name="nomeArquivoInformeAnbid"></param>
    private void ProcessaInformeAnbima(out MemoryStream msInformeAnbid, out string nomeArquivoInformeAnbid)
    {
        #region InformeAnbid
        DateTime data = Convert.ToDateTime(this.textData_ddmmyyyy.Text);
        //
        List<object> keyValuesId = gridConsulta.GetSelectedFieldValues("IdCliente");
        List<int> idClientes = keyValuesId.ConvertAll<int>(delegate(object v) { return Convert.ToInt32(v); });

        InformeAnbid inf = new InformeAnbid();
        //
        try
        {
            inf.ExportaInformeAnbid(data, idClientes, out msInformeAnbid, out nomeArquivoInformeAnbid);
        }
        catch (Exception e1)
        {
            throw new Exception(e1.Message);
        }

        #endregion
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="msInformeDiario"></param>
    /// <param name="nomeArquivoInformeDiario"></param>
    private void ProcessaInformeDiario_1_0(out MemoryStream msInformeDiario, out string nomeArquivoInformeDiario)
    {
        #region InformeDiario
        DateTime data = Convert.ToDateTime(this.textData_ddmmyyyy.Text);
        //
        List<object> keyValuesId = gridConsulta.GetSelectedFieldValues("IdCliente");
        List<int> idClientes = keyValuesId.ConvertAll<int>(delegate(object v) { return Convert.ToInt32(v); });

        InformeDiario_1_0 inf = new InformeDiario_1_0();
        //
        try
        {
            inf.ExportaInformeDiario(data, idClientes, out msInformeDiario, out nomeArquivoInformeDiario);
        }
        catch (Exception e1)
        {
            throw new Exception(e1.Message);
        }

        #endregion
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="msInformeDiario"></param>
    /// <param name="nomeArquivoInformeDiario"></param>
    private void ProcessaInformeDiario_2_0(out MemoryStream msInformeDiario, out string nomeArquivoInformeDiario)
    {
        #region InformeDiario
        DateTime data = Convert.ToDateTime(this.textData_ddmmyyyy.Text);
        //
        List<object> keyValuesId = gridConsulta.GetSelectedFieldValues("IdCliente");
        List<int> idClientes = keyValuesId.ConvertAll<int>(delegate(object v) { return Convert.ToInt32(v); });

        InformeDiario_2_0 inf = new InformeDiario_2_0();
        //
        try
        {
            inf.ExportaInformeDiario(data, idClientes, out msInformeDiario, out nomeArquivoInformeDiario);
        }
        catch (Exception e1)
        {
            throw new Exception(e1.Message);
        }

        #endregion
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="msInformeDiario"></param>
    /// <param name="nomeArquivoInformeDiario"></param>
    private void ProcessaInformeDiario_3_0(out MemoryStream msInformeDiario, out string nomeArquivoInformeDiario)
    {
        #region InformeDiario
        DateTime data = Convert.ToDateTime(this.textData_ddmmyyyy.Text);
        //
        List<object> keyValuesId = gridConsulta.GetSelectedFieldValues("IdCliente");
        List<int> idClientes = keyValuesId.ConvertAll<int>(delegate(object v) { return Convert.ToInt32(v); });

        InformeDiario_3_0 inf = new InformeDiario_3_0();
        //
        try
        {
            inf.ExportaInformeDiario(data, idClientes, out msInformeDiario, out nomeArquivoInformeDiario);
        }
        catch (Exception e1)
        {
            throw new Exception(e1.Message);
        }
        #endregion
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="msPicl">Saida: Stream do Arquivo Picl em formato txt</param>
    /// <param name="nomeArquivoPicl">Saida: Nome do Arquivo Picl</param>
    private void ProcessaPicl(out MemoryStream msPicl, out string nomeArquivoPicl)
    {
        #region Picl
        DateTime data = Convert.ToDateTime(this.textData_mmyyyy.Value);
        DateTime dataPicl = Calendario.RetornaUltimoDiaUtilMes(data);
        //
        List<object> keyValuesId = gridConsulta.GetSelectedFieldValues("IdCliente");
        List<int> idClientes = keyValuesId.ConvertAll<int>(delegate(object v) { return Convert.ToInt32(v); });
        //
        Picl picl = new Picl();
        //
        try
        {
            picl.ExportaPicl(dataPicl, idClientes, out msPicl, out nomeArquivoPicl);
        }
        catch (Exception e1)
        {
            throw new Exception(e1.Message);
        }

        #endregion
    }

    #region Lamina Versoes 1 e 2
    /// <summary>
    /// Lamina 2.0 
    /// </summary>
    /// <param name="msLamina">Dicionario com o nome do Arquivo - MemoryStream do Arquivo</param>
    private void ProcessaLamina(out Dictionary<string, MemoryStream> msLamina)
    {
        #region Lamina
        DateTime data = Convert.ToDateTime(this.textData_mmyyyy.Text);

        List<object> keyValuesId = gridConsulta.GetSelectedFieldValues("IdCliente");
        List<int> idClientes = keyValuesId.ConvertAll<int>(delegate(object v) { return Convert.ToInt32(v); });

        LaminaEssencial laminaEssencial = new LaminaEssencial();
        //
        try
        {
            laminaEssencial.ExportaLamina(data, idClientes, out msLamina);
        }
        catch (Exception e1)
        {
            throw new Exception(e1.Message);
        }

        #endregion
    }

    /// <summary>
    /// Exportacao XML - Lamina 2.0
    /// </summary>
    /// <param name="msLamina"></param>
    /// <param name="nomeArquivoLamina"></param>
    private void ProcessaLaminaXML(out MemoryStream msLamina, out string nomeArquivoLamina)
    {
        #region Lamina
        DateTime data = Convert.ToDateTime(this.textData_mmyyyy.Text);

        List<object> keyValuesId = gridConsulta.GetSelectedFieldValues("IdCliente");
        List<int> idClientes = keyValuesId.ConvertAll<int>(delegate(object v) { return Convert.ToInt32(v); });

        LaminaEssencialXML lamina = new LaminaEssencialXML();
        //
        try
        {
            lamina.ExportaLaminaXML(data, idClientes, out msLamina, out nomeArquivoLamina);
        }
        catch (Exception e1)
        {
            throw new Exception(e1.Message);
        }
        #endregion
    }

    /// <summary>
    /// Lamina 1.0
    /// </summary>
    /// <param name="msLamina">Dicionario com o nome do Arquivo - MemoryStream do Arquivo</param>
    private void ProcessaLaminaV1(out Dictionary<string, MemoryStream> msLamina)
    {
        #region Lamina
        DateTime data = Convert.ToDateTime(this.textData_mmyyyy.Text);

        List<object> keyValuesId = gridConsulta.GetSelectedFieldValues("IdCliente");
        List<int> idClientes = keyValuesId.ConvertAll<int>(delegate(object v) { return Convert.ToInt32(v); });

        LaminaEssencialV1 laminaEssencial = new LaminaEssencialV1();
        //
        try
        {
            laminaEssencial.ExportaLaminaV1(data, idClientes, out msLamina);
        }
        catch (Exception e1)
        {
            throw new Exception(e1.Message);
        }

        #endregion
    }

    /// <summary>
    /// Exportacao XML - Lamina 1.0
    /// </summary>
    /// <param name="msLamina"></param>
    /// <param name="nomeArquivoLamina"></param>
    private void ProcessaLaminaXMLV1(out MemoryStream msLamina, out string nomeArquivoLamina)
    {
        #region Lamina
        DateTime data = Convert.ToDateTime(this.textData_mmyyyy.Text);

        List<object> keyValuesId = gridConsulta.GetSelectedFieldValues("IdCliente");
        List<int> idClientes = keyValuesId.ConvertAll<int>(delegate(object v) { return Convert.ToInt32(v); });

        LaminaEssencialXMLV1 lamina = new LaminaEssencialXMLV1();
        //
        try
        {
            lamina.ExportaLaminaXMLV1(data, idClientes, out msLamina, out nomeArquivoLamina);
        }
        catch (Exception e1)
        {
            throw new Exception(e1.Message);
        }
        #endregion
    }
    #endregion

    /// <summary>
    /// 
    /// </summary>
    /// <param name="msCDA">Dicionario com o nome do Arquivo - MemoryStream do Arquivo</param>
    private void ProcessaInfoComplementar(out Dictionary<string, MemoryStream> msInfoComplementar)
    {
        #region InfoComplementar
        DateTime data = Convert.ToDateTime(this.textData_ddmmyyyy.Text);

        List<object> keyValuesId = gridConsulta.GetSelectedFieldValues("IdCliente");
        List<int> idClientes = keyValuesId.ConvertAll<int>(delegate(object v) { return Convert.ToInt32(v); });

        InfoComplementar infoComplementar = new InfoComplementar();
        //
        try
        {
            infoComplementar.ExportaInfoComplementar(data, idClientes, out msInfoComplementar);
        }
        catch (Exception e1)
        {
            throw new Exception(e1.Message);
        }

        #endregion
    }

    /// <summary>
    /// Exportacao XML
    /// </summary>
    /// <param name="msInfoComplementar"></param>
    /// <param name="nomeArquivoInfoComplementar"></param>
    private void ProcessaInfoComplementarXML(out MemoryStream msInfoComplementar, out string nomeArquivoInfoComplementar)
    {
        #region InfoComplementar
        DateTime data = Convert.ToDateTime(this.textData_ddmmyyyy.Text);

        List<object> keyValuesId = gridConsulta.GetSelectedFieldValues("IdCliente");
        List<int> idClientes = keyValuesId.ConvertAll<int>(delegate(object v) { return Convert.ToInt32(v); });

        InfoComplementarXML infoComplementar = new InfoComplementarXML();
        //
        try
        {
            infoComplementar.ExportaInfoComplementarXML(data, idClientes, out msInfoComplementar, out nomeArquivoInfoComplementar);
        }
        catch (Exception e1)
        {
            throw new Exception(e1.Message);
        }
        #endregion
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="msTabelaPerfilMensal">Saida: Stream do Arquivo TabelaPerfilMensal em formato xml</param>
    /// <param name="nomeArquivoTabelaPerfilMensal">Saida: Nome do Arquivo TabelaPerfilMensal</param>
    private void ProcessaTabelaPerfilMensal(out MemoryStream msTabelaPerfilMensal, out string nomeArquivoTabelaPerfilMensal)
    {
        #region Tabela Perfil Mensal

        DateTime dAux = Convert.ToDateTime(this.textData_mmyyyy.Value);
        DateTime d = Calendario.RetornaUltimoDiaUtilMes(dAux);
        //
        List<object> keyValuesId = gridConsulta.GetSelectedFieldValues("IdCliente");
        List<int> idClientes = keyValuesId.ConvertAll<int>(delegate(object v) { return Convert.ToInt32(v); });
        //
        //        
        TabelaPerfilCVM tabelaPerfilCVM = new TabelaPerfilCVM();
        //
        try
        {
            tabelaPerfilCVM.ExportaTabelaPerfilCVM(d, idClientes, out msTabelaPerfilMensal, out nomeArquivoTabelaPerfilMensal);
        }
        catch (Exception e1)
        {
            throw new Exception(e1.Message);
        }

        #endregion
    }

    /// <summary>
    /// Perfil mensal XML versão 3
    /// </summary>
    /// <param name="msTabelaPerfilMensal">Saida: Stream do Arquivo TabelaPerfilMensal em formato xml</param>
    /// <param name="nomeArquivoTabelaPerfilMensal">Saida: Nome do Arquivo TabelaPerfilMensal</param>
    private void ProcessaTabelaPerfilMensal_XMl_3(out MemoryStream msTabelaPerfilMensal, out string nomeArquivoTabelaPerfilMensal)
    {
        #region Tabela Perfil Mensal XML versao 3

        DateTime dAux = Convert.ToDateTime(this.textData_mmyyyy.Value);
        DateTime d = Calendario.RetornaUltimoDiaUtilMes(dAux);
        //
        List<object> keyValuesId = gridConsulta.GetSelectedFieldValues("IdCliente");
        List<int> idClientes = keyValuesId.ConvertAll<int>(delegate(object v) { return Convert.ToInt32(v); });
        //
        //        
        TabelaPerfilCVMV3 tabelaPerfilCVM = new TabelaPerfilCVMV3();
        //
        try
        {
            tabelaPerfilCVM.ExportaTabelaPerfilCVM_XMl_3(d, idClientes, out msTabelaPerfilMensal, out nomeArquivoTabelaPerfilMensal);
        }
        catch (Exception e1)
        {
            throw new Exception(e1.Message);
        }

        #endregion
    }

    /// <summary>
    /// Perfil Mensal Versao 1 PDF
    /// </summary>
    /// <param name="msInfoComplementar"></param>
    private void ProcessaTabelaPerfilMensalPDF(out Dictionary<string, MemoryStream> msTabelaPerfil)
    {

        #region Perfil Mensal
        DateTime dAux = Convert.ToDateTime(this.textData_mmyyyy.Value);
        DateTime d = Calendario.RetornaUltimoDiaUtilMes(dAux);
        //
        List<object> keyValuesId = gridConsulta.GetSelectedFieldValues("IdCliente");
        List<int> idClientes = keyValuesId.ConvertAll<int>(delegate(object v) { return Convert.ToInt32(v); });
        //
        //        
        TabelaPerfilCVM_PDF tabelaPerfilCVM = new TabelaPerfilCVM_PDF();
        //
        try
        {
            tabelaPerfilCVM.ExportaTabelaPerfilCVM_PDF(d, idClientes, out msTabelaPerfil);
        }
        catch (Exception e1)
        {
            throw new Exception(e1.Message);
        }
        #endregion
    }

    /// <summary>
    /// Perfil Mensal Versao 3 PDF
    /// </summary>
    /// <param name="msInfoComplementar"></param>
    private void ProcessaTabelaPerfilMensalPDF_3(out Dictionary<string, MemoryStream> msTabelaPerfil)
    {

        #region Perfil Mensal
        DateTime dAux = Convert.ToDateTime(this.textData_mmyyyy.Value);
        DateTime d = Calendario.RetornaUltimoDiaUtilMes(dAux);
        //
        List<object> keyValuesId = gridConsulta.GetSelectedFieldValues("IdCliente");
        List<int> idClientes = keyValuesId.ConvertAll<int>(delegate(object v) { return Convert.ToInt32(v); });
        //
        //        
        TabelaPerfilCVM_PDF tabelaPerfilCVM = new TabelaPerfilCVM_PDF();
        //
        try
        {
            tabelaPerfilCVM.ExportaTabelaPerfilCVM_PDF_V3(d, idClientes, out msTabelaPerfil);
        }
        catch (Exception e1)
        {
            throw new Exception(e1.Message);
        }
        #endregion
    }
    #endregion
            #endregion
    #endregion

    #region Segunda ABA
    #region Enums
    enum TipoExportacao
    {
        Posicao = 1,
        Historico = 2,
        Operacao = 3,
        None = 4
    }
    #endregion

    #region Classe para Binding
    public class BindingDSCollection : AtivoBolsaCollection
    {

        /// <summary>
        /// Adiciona uma Coluna Nova na Collection
        /// </summary>
        /// <param name="columnName"></param>
        /// <param name="typeColumn"></param>
        public void AddColumn(string columnName, Type typeColumn)
        {
            if (this.Table != null && !this.Table.Columns.Contains(columnName))
            {
                this.Table.Columns.Add(columnName, typeColumn);
            }
        }
    }
    #endregion

    #region AspxLookup
    /// <summary>
    /// Configura a largura dos combos de Posicao/Operacao e Historico
    /// </summary>
    private void ConfiguraLookupGrid()
    {
        //this.dropPosicao.GridView.Width = this.dropPosicao.Width;
        //this.dropOperacao.GridView.Width = this.dropOperacao.Width;
        //this.dropHistorico.GridView.Width = this.dropHistorico.Width;

        this.dropPosicao.GridView.Width = Unit.Point(150);
        this.dropOperacao.GridView.Width = Unit.Point(150);
        this.dropHistorico.GridView.Width = Unit.Point(150);
    }

    /// <summary>
    /// Preenche o combo de Posição manualmente
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void EsDSPosicao_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        BindingDSCollection b = new BindingDSCollection();
        b.CreateColumnsForBinding();
        b.AddColumn("Id", typeof(System.String));
        b.AddColumn("Value", typeof(System.String));

        AtivoBolsa a = b.AddNew();
        a.SetColumn("Id", "1");
        a.SetColumn("Value", "Posição Bolsa");

        AtivoBolsa a1 = b.AddNew();
        a1.SetColumn("Id", "2");
        a1.SetColumn("Value", "Posição BMF");

        AtivoBolsa a2 = b.AddNew();
        a2.SetColumn("Id", "3");
        a2.SetColumn("Value", "Posição Empréstimo Bolsa");

        AtivoBolsa a3 = b.AddNew();
        a3.SetColumn("Id", "4");
        a3.SetColumn("Value", "Posição Termo Bolsa");

        AtivoBolsa a4 = b.AddNew();
        a4.SetColumn("Id", "5");
        a4.SetColumn("Value", "Posição Fundos");

        AtivoBolsa a5 = b.AddNew();
        a5.SetColumn("Id", "6");
        a5.SetColumn("Value", "Posição Renda Fixa");

        AtivoBolsa a6 = b.AddNew();
        a6.SetColumn("Id", "7");
        a6.SetColumn("Value", "Posição Cotista");

        AtivoBolsa a7 = b.AddNew();
        a7.SetColumn("Id", "8");
        a7.SetColumn("Value", "Liquidação");

        AtivoBolsa a8 = b.AddNew();
        a8.SetColumn("Id", "9");
        a8.SetColumn("Value", "Cadastro Cotista");

        e.Collection = b;
    }

    /// <summary>
    /// Preenche o combo de Historico manualmente
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void EsDSHistorico_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        BindingDSCollection b = new BindingDSCollection();
        b.CreateColumnsForBinding();
        b.AddColumn("Id", typeof(System.String));
        b.AddColumn("Value", typeof(System.String));

        AtivoBolsa a = b.AddNew();
        a.SetColumn("Id", "1");
        a.SetColumn("Value", "Posição Cotista");

        AtivoBolsa riskGrid = b.AddNew();
        riskGrid.SetColumn("Id", "2");
        riskGrid.SetColumn("Value", "Posição RiskGrid");

        e.Collection = b;
    }

    /// <summary>
    /// Preenche o combo de Operação manualmente
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void EsDSOperacao_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        BindingDSCollection b = new BindingDSCollection();
        b.CreateColumnsForBinding();
        b.AddColumn("Id", typeof(System.String));
        b.AddColumn("Value", typeof(System.String));

        AtivoBolsa a = b.AddNew();
        a.SetColumn("Id", "1");
        a.SetColumn("Value", "Operação Cotista");

        AtivoBolsa a1 = b.AddNew();
        a1.SetColumn("Id", "2");
        a1.SetColumn("Value", "Histórico Cota");

        e.Collection = b;
    }

    /// <summary>
    /// Função para selecionar todos os checkboxs de um Grid 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    new protected void CheckBoxSelectAll(object sender, EventArgs e)
    {
        ASPxCheckBox chk = sender as ASPxCheckBox;
        ASPxGridView grid = (chk.NamingContainer as GridViewHeaderTemplateContainer).Grid;
        chk.Checked = (grid.Selection.Count == grid.VisibleRowCount);
    }
    #endregion

    #region Grid1
    protected void gridConsulta1_CustomCallback(object sender, DevExpress.Web.ASPxGridViewCustomCallbackEventArgs e)
    {
        this.gridConsulta1.DataBind();
        this.gridConsulta1.Selection.UnselectAll();
        this.gridConsulta1.CancelEdit();
    }
    #endregion

    /// <summary>
    /// CallBack para os botões Posição, Histórico, Operação
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void callbackExporta1_Callback(object source, DevExpress.Web.CallbackEventArgs e)
    {
        e.Result = "";

        TipoExportacao tipoExportacao = TipoExportacao.None;
        if (e.Parameter == "btnPosicao")
        {
            tipoExportacao = TipoExportacao.Posicao;
        }
        else if (e.Parameter == "btnHistorico")
        {
            tipoExportacao = TipoExportacao.Historico;
        }
        else if (e.Parameter == "btnOperacao")
        {
            tipoExportacao = TipoExportacao.Operacao;
        }
        else
        {
            e.Result = "Tipo de data de exportação não suportado";
        }

        if (e.Result != "")
        {
            return;
        }

        try
        {
            this.TrataErros1(tipoExportacao);
            this.SelecionaExportacao1(tipoExportacao);
        }
        catch (Exception erro)
        {
            e.Result = erro.Message;
            return;
        }

        string newLocation = HttpContext.Current.Request.Url.AbsolutePath.Replace("Exportacao.aspx", "ExibeExportacaoImplantacao.aspx?Tipo=" + tipoExportacao);
        e.Result = "redirect:" + newLocation;
    }

    /// <summary>
    /// Trata os Erros de Exportação para cada Categoria
    /// </summary>
    /// <param name="tipo">Categoria da Exportação, representa que botão foi clicado</param>
    private void TrataErros1(TipoExportacao tipo)
    {
        List<object> keyValuesCliente = this.gridConsulta1.GetSelectedFieldValues(ClienteMetadata.ColumnNames.IdCliente);
        if (keyValuesCliente.Count == 0)
        {
            throw new Exception("Algum cliente deve ser selecionado!");
        }

        switch (tipo)
        {

            case TipoExportacao.Posicao:
                List<Control> controles1 = new List<Control>(new Control[] { this.dropPosicao });
                if (base.TestaObrigatorio(controles1) != "") throw new Exception("Tipo da Posição deve ser Selecionado.");
                break;

            case TipoExportacao.Historico:
                List<Control> controles2 = new List<Control>(new Control[] { this.dropHistorico });
                if (base.TestaObrigatorio(controles2) != "") throw new Exception("Tipo do Histórico deve ser Selecionado.");
                if (this.textDataHistorico.Text == "") throw new Exception("Data do Histórico Obrigatório.");
                break;

            case TipoExportacao.Operacao:
                List<Control> controles3 = new List<Control>(new Control[] { this.dropOperacao });
                if (base.TestaObrigatorio(controles3) != "") throw new Exception("Tipo da Operação deve ser Selecionado.");
                if (this.textDataInicioOperacao.Text == "" || this.textDataFimOperacao.Text == "") throw new Exception("Data Obrigatório");
                break;
        }
    }

    /// <summary>
    /// Seleciona as planilhas a serem geradas
    /// </summary>
    /// <param name="tipo">Categoria da Exportação, representa que botão foi clicado</param>
    private void SelecionaExportacao1(TipoExportacao tipo)
    {
        Response.BufferOutput = true;

        List<object> keyValuesId = gridConsulta1.GetSelectedFieldValues("IdCliente");
        List<int> idsCarteiras = keyValuesId.ConvertAll<int>(delegate(object v) { return Convert.ToInt32(v); });

        //
        // Salva os parametros na session

        switch (tipo)
        {
            case TipoExportacao.Posicao:
                List<object> keyValuesPosicao = this.dropPosicao.GridView.GetSelectedFieldValues("Id");
                List<int> ids = keyValuesPosicao.ConvertAll<int>(delegate(object v) { return Convert.ToInt32(v); });
                //

                // Dicionario Nome do Arquivo - Memory Stream do arquivo
                Dictionary<string, MemoryStream> retPosicao = new Dictionary<string, MemoryStream>();

                //
                this.ProcessaPlanilhasPosicao(ids, idsCarteiras, out retPosicao);

                // Salva Memory Stream do Arquivo Excel na Session                
                Session["streamPosicao"] = retPosicao;

                break;
            case TipoExportacao.Historico:
                List<object> keyValuesHistorico = this.dropHistorico.GridView.GetSelectedFieldValues("Id");
                List<int> ids1 = keyValuesHistorico.ConvertAll<int>(delegate(object v) { return Convert.ToInt32(v); });
                //

                // Dicionario Nome do Arquivo - Memory Stream do arquivo
                Dictionary<string, MemoryStream> retHistorico = new Dictionary<string, MemoryStream>();

                //
                this.ProcessaPlanilhasHistorico(ids1, idsCarteiras, Convert.ToDateTime(this.textDataHistorico.Value),
                                                out retHistorico);

                // Salva Memory Stream do Arquivo Excel na Session                
                Session["streamHistorico"] = retHistorico;

                break;
            case TipoExportacao.Operacao:
                List<object> keyValuesOperacao = this.dropOperacao.GridView.GetSelectedFieldValues("Id");
                List<int> ids2 = keyValuesOperacao.ConvertAll<int>(delegate(object v) { return Convert.ToInt32(v); });
                //

                // Dicionario Nome do Arquivo - Memory Stream do arquivo
                Dictionary<string, MemoryStream> retOperacao = new Dictionary<string, MemoryStream>();

                //
                this.ProcessaPlanilhasOperacao(ids2, idsCarteiras,
                                               Convert.ToDateTime(this.textDataInicioOperacao.Value),
                                               Convert.ToDateTime(this.textDataFimOperacao.Value),
                                               out retOperacao);

                // Salva Memory Stream do Arquivo Excel na Session                
                Session["streamOperacao"] = retOperacao;

                break;
        }
    }
    #endregion

    #region Terceira ABA
    protected void gridConsultaGalgo_CustomCallback(object sender, DevExpress.Web.ASPxGridViewCustomCallbackEventArgs e)
    {
        gridConsulta.DataBind();
        gridConsulta.Selection.UnselectAll();
        gridConsulta.CancelEdit();
    }
    #endregion

}