﻿using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using EntitySpaces.Interfaces;
using System.Collections.Generic;
using Financial.Web.Common;
using System.IO;
using System.Globalization;
using Dart.PowerTCP.Zip;
using System.Text;

public partial class _ExibeExportacao : BasePage
{

    #region Enums
    enum TipoExportacao_FormatoDDMMYYYY
    {
        BacenJud = 1,
        ContabilFolhamatic = 2,
        InformeDiario_1_0 = 20,
        InformeDiario_2_0 = 3,
        InformeDiario_3_0 = 19,
        InformeAnbima = 4,
        XMLPosicaoAnbid = 5, // Exportacao Anbid
        Matriz_Ativos = 6,
        //MateraCC = 5,
        ContabilSinacor = 7,
        YMF_TX3 = 8,
        YMF_TX2 = 9,
        CCOUSinacor = 10,
        CETIP_Cliente = 11,
        CETIP_OperacaoRF = 12,
        BacenCCS = 13,
        CETIP_OperacaoRF_IPO_Corretora = 14,
        CETIP_OperacaoRF_IPO_Clientes = 15,
        InformacoesComplementaresCVM = 17,
        InformacoesComplementaresCVM_XML = 18
    }

    enum TipoExportacao_FormatoMMYY
    {
        CDA20 = 100,
        CDA30 = 101,
        CDA40 = 1014,
        Picl = 102,
        //
        PerfilMensalCVM = 103,
        PerfilMensalCVM_XML_V3 = 140,
        //
        PerfilMensalCVM_PDF = 106,
        PerfilMensalCVM_PDF_V3 = 141,
        //
        RankingAnbimaAtivo = 104,
        RankingAnbimaPassivo = 108,
        //
        LaminaPDF = 120,
        LaminaPDF_V1 = 121,
        //
        LaminaXML = 130,
        LaminaXML_V1 = 131,
        //
        Cosif4016 = 109,
        Cosif4010 = 110
    }

    enum TipoExportacao_FormatoYYYY
    {
        Dirf = 500,
        InformeRendimento = 501
    }

    enum TipoExportacao_FormatoPeriodo
    {
        ContabilAtt = 1000,
        ContabilInfoBanc = 1010,
        Galgo_PlCota = 1020,
        Galgo_PlCotaWcf = 1030,
        ContabilInfoBancContaFull = 1011
    }
    #endregion

    new protected void Page_Load(object sender, EventArgs e)
    {

        string tipo = Request.QueryString["Tipo"];

        DateTime data = Convert.ToDateTime(Session["textData"]);

        #region Formato ddmmyyyy
        if (Convert.ToInt32(tipo) <= 99)
        {
            DateTime dataExportacao;
            string dataExportacaoParam;

            List<int> idClientes;

            switch ((int)Enum.Parse(typeof(TipoExportacao_FormatoDDMMYYYY), tipo))
            {
                case (int)TipoExportacao_FormatoDDMMYYYY.BacenJud:
                    #region BacenJud

                    MemoryStream msBacenJud = (MemoryStream)(Session["streamArquivoBacenJud"]);
                    string nomeArquivoBacenJud = Convert.ToString(Session["nomeArquivoBacenJud"]);
                    //            
                    Response.ClearContent();
                    Response.ClearHeaders();
                    Response.Buffer = true;
                    Response.Cache.SetCacheability(HttpCacheability.Private);
                    Response.ContentType = "text/plain";
                    Response.AddHeader("Content-Length", msBacenJud.Length.ToString(CultureInfo.CurrentCulture));
                    Response.AppendHeader("Content-Disposition", String.Format("attachment;filename=" + nomeArquivoBacenJud));

                    Response.BinaryWrite(msBacenJud.ToArray());
                    msBacenJud.Close();
                    //
                    HttpContext.Current.ApplicationInstance.CompleteRequest();
                    Response.End();

                    #endregion
                    break;
                case (int)TipoExportacao_FormatoDDMMYYYY.BacenCCS:
                    #region BacenCCS

                    MemoryStream msBacenCCS = (MemoryStream)(Session["streamArquivoBacenCCS"]);
                    string nomeArquivoBacenCCS = Convert.ToString(Session["nomeArquivoBacenCCS"]);
                    //            
                    Response.ClearContent();
                    Response.ClearHeaders();
                    Response.Buffer = true;
                    Response.Cache.SetCacheability(HttpCacheability.Private);
                    Response.ContentType = "text/plain";
                    Response.AddHeader("Content-Length", msBacenCCS.Length.ToString(CultureInfo.CurrentCulture));
                    Response.AppendHeader("Content-Disposition", String.Format("attachment;filename=" + nomeArquivoBacenCCS));

                    Response.BinaryWrite(msBacenCCS.ToArray());
                    msBacenCCS.Close();
                    //
                    HttpContext.Current.ApplicationInstance.CompleteRequest();
                    Response.End();

                    #endregion
                    break;
                case (int)TipoExportacao_FormatoDDMMYYYY.ContabilFolhamatic:
                    #region Contabil

                    MemoryStream[] msContabil = (MemoryStream[])(Session["streamContabil"]);
                    List<string> nomeArquivoContabil = (List<string>)(Session["nomeArquivosContabil"]);

                    Archive arquivoContabil = new Archive();

                    // Saida
                    MemoryStream msContabilZip = new MemoryStream();
                    //                
                    string mes = data.Month.ToString("MM");
                    string arquivoContabilZip = "Contabil_" + mes + ".zip";

                    try
                    {
                        // For dos Arquivos
                        for (int i = 0; i < msContabil.Length; i++)
                        {
                            arquivoContabil.Add(msContabil[i]);
                            arquivoContabil[i].Name = nomeArquivoContabil[i];
                        }
                        //
                        arquivoContabil.Zip(msContabilZip);
                    }
                    catch (Exception e1)
                    {
                        throw new Exception(e1.Message + "Erro ao Zipar o Arquivo");
                    }

                    Response.ClearContent();
                    Response.ClearHeaders();
                    Response.Buffer = true;
                    Response.Cache.SetCacheability(HttpCacheability.Private);
                    Response.ContentType = "application/zip";
                    Response.AddHeader("Content-Length", msContabilZip.Length.ToString(CultureInfo.CurrentCulture));
                    Response.AppendHeader("Content-Disposition", String.Format("attachment;filename=" + arquivoContabilZip));

                    Response.BinaryWrite(msContabilZip.ToArray());
                    msContabilZip.Close();

                    HttpContext.Current.ApplicationInstance.CompleteRequest();
                    Response.End();

                    #endregion
                    break;
                case (int)TipoExportacao_FormatoDDMMYYYY.InformeDiario_1_0:
                    #region InformeDiario_1_0

                    MemoryStream msInformeDiario_1_0 = (MemoryStream)(Session["streamArquivoInformeDiario_1_0"]);
                    string nomeArquivoInformeDiario_1_0 = Convert.ToString(Session["nomeArquivoInformeDiario_1_0"]);
                    //            
                    Response.ClearContent();
                    Response.ClearHeaders();
                    Response.Buffer = true;
                    Response.Cache.SetCacheability(HttpCacheability.Private);
                    Response.ContentType = "text/plain";
                    Response.AddHeader("Content-Length", msInformeDiario_1_0.Length.ToString(CultureInfo.CurrentCulture));
                    Response.AppendHeader("Content-Disposition", String.Format("attachment;filename=" + nomeArquivoInformeDiario_1_0));

                    Response.BinaryWrite(msInformeDiario_1_0.ToArray());
                    msInformeDiario_1_0.Close();
                    //
                    HttpContext.Current.ApplicationInstance.CompleteRequest();
                    Response.End();

                    #endregion
                    break;
                case (int)TipoExportacao_FormatoDDMMYYYY.InformeDiario_2_0:
                    #region InformeDiario_2_0

                    MemoryStream msInformeDiario_2_0 = (MemoryStream)(Session["streamArquivoInformeDiario_2_0"]);
                    string nomeArquivoInformeDiario_2_0 = Convert.ToString(Session["nomeArquivoInformeDiario_2_0"]);
                    //            
                    Response.ClearContent();
                    Response.ClearHeaders();
                    Response.Buffer = true;
                    Response.Cache.SetCacheability(HttpCacheability.Private);
                    Response.ContentType = "text/plain";
                    Response.AddHeader("Content-Length", msInformeDiario_2_0.Length.ToString(CultureInfo.CurrentCulture));
                    Response.AppendHeader("Content-Disposition", String.Format("attachment;filename=" + nomeArquivoInformeDiario_2_0));

                    Response.BinaryWrite(msInformeDiario_2_0.ToArray());
                    msInformeDiario_2_0.Close();
                    //
                    HttpContext.Current.ApplicationInstance.CompleteRequest();
                    Response.End();

                    #endregion
                    break;
                case (int)TipoExportacao_FormatoDDMMYYYY.InformeDiario_3_0:
                    #region InformeDiario_3_0

                    MemoryStream msInformeDiario_3_0 = (MemoryStream)(Session["streamArquivoInformeDiario_3_0"]);
                    string nomeArquivoInformeDiario_3_0 = Convert.ToString(Session["nomeArquivoInformeDiario_3_0"]);
                    //            
                    Response.ClearContent();
                    Response.ClearHeaders();
                    Response.Buffer = true;
                    Response.Cache.SetCacheability(HttpCacheability.Private);
                    Response.ContentType = "text/plain";
                    Response.AddHeader("Content-Length", msInformeDiario_3_0.Length.ToString(CultureInfo.CurrentCulture));
                    Response.AppendHeader("Content-Disposition", String.Format("attachment;filename=" + nomeArquivoInformeDiario_3_0));

                    Response.BinaryWrite(msInformeDiario_3_0.ToArray());
                    msInformeDiario_3_0.Close();
                    //
                    HttpContext.Current.ApplicationInstance.CompleteRequest();
                    Response.End();

                    #endregion
                    break;
                case (int)TipoExportacao_FormatoDDMMYYYY.InformeAnbima:
                    #region InformeAnbima

                    MemoryStream msInformeAnbid = (MemoryStream)(Session["streamArquivoInformeAnbid"]);
                    string nomeArquivoInformeAnbid = Convert.ToString(Session["nomeArquivoInformeAnbid"]);
                    //            
                    Response.ClearContent();
                    Response.ClearHeaders();
                    Response.Buffer = true;
                    Response.Cache.SetCacheability(HttpCacheability.Private);
                    Response.ContentType = "text/plain";
                    Response.AddHeader("Content-Length", msInformeAnbid.Length.ToString(CultureInfo.CurrentCulture));
                    Response.AppendHeader("Content-Disposition", String.Format("attachment;filename=" + nomeArquivoInformeAnbid));

                    Response.BinaryWrite(msInformeAnbid.ToArray());
                    msInformeAnbid.Close();
                    //
                    HttpContext.Current.ApplicationInstance.CompleteRequest();
                    Response.End();

                    #endregion
                    break;

                case (int)TipoExportacao_FormatoDDMMYYYY.XMLPosicaoAnbid:
                    #region XMLPosicaoAnbid

                    MemoryStream msXMLAnbid = (MemoryStream)(Session["streamArquivoXMLAnbid"]);
                    string nomeArquivoXMLAnbid = Convert.ToString(Session["nomeArquivoXMLAnbima"]);
                    //            
                    Response.ClearContent();
                    Response.ClearHeaders();
                    Response.Buffer = true;
                    Response.Cache.SetCacheability(HttpCacheability.Private);
                    Response.ContentType = "text/plain";
                    Response.AddHeader("Content-Length", msXMLAnbid.Length.ToString(CultureInfo.CurrentCulture));
                    Response.AppendHeader("Content-Disposition", String.Format("attachment;filename=" + nomeArquivoXMLAnbid));

                    Response.BinaryWrite(msXMLAnbid.ToArray());
                    msXMLAnbid.Close();
                    //
                    HttpContext.Current.ApplicationInstance.CompleteRequest();
                    Response.End();

                    #endregion
                    break;

                case (int)TipoExportacao_FormatoDDMMYYYY.Matriz_Ativos:
                    #region MatrizAtivos

                    MemoryStream ms5 = (MemoryStream)(Session["streamArquivo"]);
                    string nomeArquivo = "MatrizAtivos" + data.ToString("dd/MM/yyyy").Replace("/", "") + ".xls";
                    //
                    ms5.Seek(0, SeekOrigin.Begin);
                    Response.ClearContent();
                    Response.ClearHeaders();
                    Response.Buffer = true;
                    Response.Cache.SetCacheability(HttpCacheability.Private);
                    Response.ContentType = "application/vnd.ms-excel";
                    Response.AddHeader("Content-Length", ms5.Length.ToString(CultureInfo.CurrentCulture));
                    Response.AddHeader("Content-Disposition", String.Format("attachment;filename=" + nomeArquivo));

                    Response.BinaryWrite(ms5.ToArray());
                    ms5.Close();
                    HttpContext.Current.ApplicationInstance.CompleteRequest();
                    Response.End();

                    #endregion
                    break;
                case (int)TipoExportacao_FormatoDDMMYYYY.ContabilSinacor:
                    #region Contabil Sinacor

                    MemoryStream msContabilSinacor = (MemoryStream)(Session["streamContabilSinacor"]);
                    string nomeArquivoSinacor = Convert.ToString(Session["nomeArquivoContabilSinacor"]);
                    //            
                    Response.ClearContent();
                    Response.ClearHeaders();
                    Response.Buffer = true;
                    Response.Cache.SetCacheability(HttpCacheability.Private);
                    Response.ContentType = "text/plain";
                    Response.AddHeader("Content-Length", msContabilSinacor.Length.ToString(CultureInfo.CurrentCulture));
                    Response.AppendHeader("Content-Disposition", String.Format("attachment;filename=" + nomeArquivoSinacor));

                    Response.BinaryWrite(msContabilSinacor.ToArray());
                    msContabilSinacor.Close();
                    //
                    HttpContext.Current.ApplicationInstance.CompleteRequest();
                    Response.End();

                    #endregion
                    break;
                case (int)TipoExportacao_FormatoDDMMYYYY.CCOUSinacor:
                    #region CCOUSinacor

                    MemoryStream msCCOUSinacor = (MemoryStream)(Session["streamCCOUSinacor"]);
                    string nomeArquivoCCOUSinacor = Convert.ToString(Session["nomeArquivoCCOUSinacor"]);
                    //            
                    Response.ClearContent();
                    Response.ClearHeaders();
                    Response.Buffer = true;
                    Response.Cache.SetCacheability(HttpCacheability.Private);
                    Response.ContentType = "text/plain";
                    Response.AddHeader("Content-Length", msCCOUSinacor.Length.ToString(CultureInfo.CurrentCulture));
                    Response.AppendHeader("Content-Disposition", String.Format("attachment;filename=" + nomeArquivoCCOUSinacor));

                    Response.BinaryWrite(msCCOUSinacor.ToArray());
                    msCCOUSinacor.Close();
                    //
                    HttpContext.Current.ApplicationInstance.CompleteRequest();
                    Response.End();

                    #endregion
                    break;
                
                case (int)TipoExportacao_FormatoDDMMYYYY.CETIP_Cliente:
                    #region CETIP_Cliente
                    //dataExportacao = DateTime.ParseExact(Convert.ToString(Session["textData"]), "dd/MM/yyyy",null);
                    dataExportacaoParam = Request.QueryString["DataExportacao"];
                    dataExportacao = DateTime.ParseExact(dataExportacaoParam, "yyyy-MM-dd", null);


                    string conteudoArquivo;
                    string nomeArquivoCetipClente;

                    Financial.Export.Cetip.Cliente exportaCetipCliente = new Financial.Export.Cetip.Cliente();
                    exportaCetipCliente.ExportaArquivo(dataExportacao, out conteudoArquivo, out nomeArquivoCetipClente);

                    Response.ClearContent();
                    Response.ClearHeaders();
                    Response.Buffer = true;
                    Response.Cache.SetCacheability(HttpCacheability.Private);
                    Response.ContentType = "text/plain";
                    Response.AddHeader("Content-Length", conteudoArquivo.Length.ToString(System.Globalization.CultureInfo.CurrentCulture));
                    Response.AppendHeader("Content-Disposition", String.Format("attachment;filename=" + nomeArquivoCetipClente));
                    Response.Write(conteudoArquivo);

                    HttpContext.Current.ApplicationInstance.CompleteRequest();
                    Response.End();
                    #endregion
                    break;

                case (int)TipoExportacao_FormatoDDMMYYYY.CETIP_OperacaoRF:
                    #region CETIP_OperacaoRF
                    //dataExportacao = DateTime.ParseExact(Convert.ToString(Session["textData"]), "dd/MM/yyyy", null);
//                    dataExportacao = new DateTime(2010, 3, 9);
                    dataExportacaoParam = Request.QueryString["DataExportacao"];
                    dataExportacao = DateTime.ParseExact(dataExportacaoParam, "yyyy-MM-dd", null);
                                        
                    MemoryStream memoryStreamCetipOperacaoRF;
                    string nomeArquivoCetipOperacaoRF;

                    Financial.Export.Cetip.OperacaoRF exportaCetipOperacaoRF = new Financial.Export.Cetip.OperacaoRF();
                    exportaCetipOperacaoRF.ExportaArquivo(dataExportacao, out memoryStreamCetipOperacaoRF, out nomeArquivoCetipOperacaoRF);

                    Response.ClearContent();
                    Response.ClearHeaders();
                    Response.Buffer = true;
                    Response.Cache.SetCacheability(HttpCacheability.Private);
                    Response.ContentType = "application/zip";
                    Response.AddHeader("Content-Length", memoryStreamCetipOperacaoRF.Length.ToString(CultureInfo.CurrentCulture));
                    Response.AppendHeader("Content-Disposition", String.Format("attachment;filename=" + nomeArquivoCetipOperacaoRF));

                    Response.BinaryWrite(memoryStreamCetipOperacaoRF.ToArray());
                    memoryStreamCetipOperacaoRF.Close();
                    //
                    HttpContext.Current.ApplicationInstance.CompleteRequest();
                    Response.End();
                    #endregion

                    break;

                case (int)TipoExportacao_FormatoDDMMYYYY.CETIP_OperacaoRF_IPO_Corretora:
                    #region CETIP_OperacaoRF_IPO_Corretora
                    int idOperacaoRFIPOCorretora = Convert.ToInt32(Request.QueryString["IdOperacao"]);

                    string conteudoArquivoCetipOperacaoRFIPOCorretora;
                    string nomeArquivoCetipOperacaoRFIPOCorretora;

                    Financial.Export.Cetip.OperacaoIPOCorretora exportaCetipOperacaoRFIPOCorretora = new Financial.Export.Cetip.OperacaoIPOCorretora();
                    exportaCetipOperacaoRFIPOCorretora.ExportaArquivo(idOperacaoRFIPOCorretora, out conteudoArquivoCetipOperacaoRFIPOCorretora, out nomeArquivoCetipOperacaoRFIPOCorretora);

                    Response.ClearContent();
                    Response.ClearHeaders();
                    Response.Buffer = true;
                    Response.Cache.SetCacheability(HttpCacheability.Private);
                    Response.ContentType = "text/plain";
                    Response.AddHeader("Content-Length", conteudoArquivoCetipOperacaoRFIPOCorretora.Length.ToString(System.Globalization.CultureInfo.CurrentCulture));
                    Response.AppendHeader("Content-Disposition", String.Format("attachment;filename=" + nomeArquivoCetipOperacaoRFIPOCorretora));
                    Response.Write(conteudoArquivoCetipOperacaoRFIPOCorretora);

                    HttpContext.Current.ApplicationInstance.CompleteRequest();
                    Response.End();
                    #endregion
                    break;

                case (int)TipoExportacao_FormatoDDMMYYYY.CETIP_OperacaoRF_IPO_Clientes:

                    int idOperacaoRFIPOClientes = Convert.ToInt32(Request.QueryString["IdOperacao"]);

                    string conteudoArquivoCetipOperacaoRFIPOClientes;
                    string nomeArquivoCetipOperacaoRFIPOClientes;

                    Financial.Export.Cetip.OperacaoIPOClientes exportaCetipOperacaoRFIPOClientes = new Financial.Export.Cetip.OperacaoIPOClientes();
                    exportaCetipOperacaoRFIPOClientes.ExportaArquivo(idOperacaoRFIPOClientes, out conteudoArquivoCetipOperacaoRFIPOClientes, out nomeArquivoCetipOperacaoRFIPOClientes);

                    Response.ClearContent();
                    Response.ClearHeaders();
                    Response.Buffer = true;
                    Response.Cache.SetCacheability(HttpCacheability.Private);
                    Response.ContentType = "text/plain";
                    Response.AddHeader("Content-Length",  conteudoArquivoCetipOperacaoRFIPOClientes.Length.ToString(System.Globalization.CultureInfo.CurrentCulture));
                    Response.AppendHeader("Content-Disposition", String.Format("attachment;filename=" + nomeArquivoCetipOperacaoRFIPOClientes));
                    Response.Write(conteudoArquivoCetipOperacaoRFIPOClientes);

                    HttpContext.Current.ApplicationInstance.CompleteRequest();
                    Response.End();

                    break;

                case (int)TipoExportacao_FormatoDDMMYYYY.YMF_TX2:

                    dataExportacao = DateTime.ParseExact(Convert.ToString(Session["textData"]), "dd/MM/yyyy", null);
                    idClientes = (List<int>)Session["idClientes"];

                    MemoryStream memoryStreamYMF_TX2;
                    string nomeArquivoYMF_TX2;

                    Financial.Export.YMF.TX2 exportaTX2 = new Financial.Export.YMF.TX2();
                    exportaTX2.ExportaArquivo(dataExportacao, idClientes, out memoryStreamYMF_TX2, out nomeArquivoYMF_TX2);

                    Response.ClearContent();
                    Response.ClearHeaders();
                    Response.Buffer = true;
                    Response.Cache.SetCacheability(HttpCacheability.Private);
                    Response.ContentType = "application/zip";
                    Response.AddHeader("Content-Length", memoryStreamYMF_TX2.Length.ToString(CultureInfo.CurrentCulture));
                    Response.AppendHeader("Content-Disposition", String.Format("attachment;filename=" + nomeArquivoYMF_TX2));

                    Response.BinaryWrite(memoryStreamYMF_TX2.ToArray());
                    memoryStreamYMF_TX2.Close();
                    //
                    HttpContext.Current.ApplicationInstance.CompleteRequest();
                    Response.End();


                    break;
                
                case (int)TipoExportacao_FormatoDDMMYYYY.YMF_TX3:
                    #region YMF TX3

                    dataExportacao = DateTime.ParseExact(Convert.ToString(Session["textData"]), "dd/MM/yyyy", null);
                    idClientes = (List<int>)Session["idClientes"];

                    MemoryStream memoryStreamYMF_TX3;
                    string nomeArquivoYMF_TX3;

                    Financial.Export.YMF.TX3 exportaTX3 = new Financial.Export.YMF.TX3();
                    exportaTX3.ExportaArquivo(dataExportacao, idClientes, out memoryStreamYMF_TX3, out nomeArquivoYMF_TX3);

                    Response.ClearContent();
                    Response.ClearHeaders();
                    Response.Buffer = true;
                    Response.Cache.SetCacheability(HttpCacheability.Private);
                    Response.ContentType = "application/zip";
                    Response.AddHeader("Content-Length", memoryStreamYMF_TX3.Length.ToString(CultureInfo.CurrentCulture));
                    Response.AppendHeader("Content-Disposition", String.Format("attachment;filename=" + nomeArquivoYMF_TX3));

                    Response.BinaryWrite(memoryStreamYMF_TX3.ToArray());
                    memoryStreamYMF_TX3.Close();
                    //
                    HttpContext.Current.ApplicationInstance.CompleteRequest();
                    Response.End();
                    

                    #endregion
                    break;

                case (int)TipoExportacao_FormatoDDMMYYYY.InformacoesComplementaresCVM:
                    #region InformacoesComplementaresCVM - PDF

                    Dictionary<string, MemoryStream> msInfo = (Dictionary<string, MemoryStream>)Session["streamInfoComplementares"];
                    Archive arquivoInfo = new Archive();

                    // Saida
                    MemoryStream msZipInfo = new MemoryStream();
                    string arquivoZipInfo = "InformacoesComplementares.zip";

                    try {
                        int j = 0;
                        foreach (KeyValuePair<string, MemoryStream> pair in msInfo) {
                            arquivoInfo.Add(pair.Value); // Memory Stream
                            arquivoInfo[j].Name = pair.Key; // Nome do Arquivo
                            //
                            j++;
                        }
                        //
                        arquivoInfo.Zip(msZipInfo);
                    }
                    catch (Exception e1) {
                        throw new Exception(e1.Message + "Erro ao Zipar o Arquivo");
                    }

                    Response.ClearContent();
                    Response.ClearHeaders();
                    Response.Buffer = true;
                    Response.Cache.SetCacheability(HttpCacheability.Private);
                    Response.ContentType = "application/zip";
                    Response.AddHeader("Content-Length", msZipInfo.Length.ToString(CultureInfo.CurrentCulture));
                    Response.AppendHeader("Content-Disposition", String.Format("attachment;filename=" + arquivoZipInfo));

                    Response.BinaryWrite(msZipInfo.ToArray());
                    msZipInfo.Close();

                    HttpContext.Current.ApplicationInstance.CompleteRequest();
                    Response.End();

                    #endregion
                    break;

                case (int)TipoExportacao_FormatoDDMMYYYY.InformacoesComplementaresCVM_XML:
                    #region InfoComplementares

                    MemoryStream msInfoComplementar = (MemoryStream)(Session["streamArquivoInfoComplementar"]);
                    string nomeArquivoInfoComplementar = Convert.ToString(Session["nomeArquivoInfoComplementar"]);
                    //            
                    Response.ClearContent();
                    Response.ClearHeaders();
                    Response.Buffer = true;
                    Response.Cache.SetCacheability(HttpCacheability.Private);
                    Response.ContentType = "text/plain";
                    Response.AddHeader("Content-Length", msInfoComplementar.Length.ToString(CultureInfo.CurrentCulture));
                    Response.AppendHeader("Content-Disposition", String.Format("attachment;filename=" + nomeArquivoInfoComplementar));

                    Response.BinaryWrite(msInfoComplementar.ToArray());
                    msInfoComplementar.Close();
                    //
                    HttpContext.Current.ApplicationInstance.CompleteRequest();
                    Response.End();

                    #endregion
                    break;

            }


        }
        #endregion

        #region Formato mmyyyy
        if ((Convert.ToInt32(tipo) >= 100 && Convert.ToInt32(tipo) <= 499) || Convert.ToInt32(tipo) == 1014)
        {
            switch ((int)Enum.Parse(typeof(TipoExportacao_FormatoMMYY), tipo))
            {
                case (int)TipoExportacao_FormatoMMYY.CDA20:
                    #region CDA 2.0

                    Dictionary<string, MemoryStream> ms20 = (Dictionary<string, MemoryStream>)Session["streamCDA"];
                    Archive arquivoCDA20 = new Archive();

                    // Saida
                    MemoryStream msZipCDA20 = new MemoryStream();
                    string arquivoZipCDA20 = "CDA20.zip";

                    try
                    {
                        int j = 0;
                        foreach (KeyValuePair<string, MemoryStream> pair in ms20)
                        {
                            arquivoCDA20.Add(pair.Value); // Memory Stream
                            arquivoCDA20[j].Name = pair.Key; // Nome do Arquivo
                            //
                            j++;
                        }
                        //
                        arquivoCDA20.Zip(msZipCDA20);
                    }
                    catch (Exception e1)
                    {
                        throw new Exception(e1.Message + "Erro ao Zipar o Arquivo");
                    }

                    // Gera o Arquivo a partir da memory stream
                    //int k = 0;
                    //foreach (KeyValuePair<string, MemoryStream> pair in ms) {

                    //    using (FileStream fs = File.Open("S:/teste"+k+".xml", FileMode.Create, FileAccess.Write)) {
                    //        pair.Value.WriteTo(fs);
                    //        pair.Value.Dispose();
                    //    }

                    //    k++;
                    //}

                    Response.ClearContent();
                    Response.ClearHeaders();
                    Response.Buffer = true;
                    Response.Cache.SetCacheability(HttpCacheability.Private);
                    Response.ContentType = "application/zip";
                    Response.AddHeader("Content-Length", msZipCDA20.Length.ToString(CultureInfo.CurrentCulture));
                    Response.AppendHeader("Content-Disposition", String.Format("attachment;filename=" + arquivoZipCDA20));

                    Response.BinaryWrite(msZipCDA20.ToArray());
                    msZipCDA20.Close();

                    HttpContext.Current.ApplicationInstance.CompleteRequest();
                    Response.End();

                    #endregion
                    break;
                case (int)TipoExportacao_FormatoMMYY.CDA30:
                    #region CDA 3.0

                    Dictionary<string, MemoryStream> ms30 = (Dictionary<string, MemoryStream>)Session["streamCDA"];
                    Archive arquivoCDA30 = new Archive();

                    // Saida
                    MemoryStream msZipCDA30 = new MemoryStream();
                    string arquivoZipCDA30 = "CDA30.zip";

                    try
                    {
                        int j = 0;
                        foreach (KeyValuePair<string, MemoryStream> pair in ms30)
                        {
                            arquivoCDA30.Add(pair.Value); // Memory Stream
                            arquivoCDA30[j].Name = pair.Key; // Nome do Arquivo
                            //
                            j++;
                        }
                        //
                        arquivoCDA30.Zip(msZipCDA30);
                    }
                    catch (Exception e1)
                    {
                        throw new Exception(e1.Message + "Erro ao Zipar o Arquivo");
                    }

                    // Gera o Arquivo a partir da memory stream
                    //int k = 0;
                    //foreach (KeyValuePair<string, MemoryStream> pair in ms) {

                    //    using (FileStream fs = File.Open("S:/teste"+k+".xml", FileMode.Create, FileAccess.Write)) {
                    //        pair.Value.WriteTo(fs);
                    //        pair.Value.Dispose();
                    //    }

                    //    k++;
                    //}

                    Response.ClearContent();
                    Response.ClearHeaders();
                    Response.Buffer = true;
                    Response.Cache.SetCacheability(HttpCacheability.Private);
                    Response.ContentType = "application/zip";
                    Response.AddHeader("Content-Length", msZipCDA30.Length.ToString(CultureInfo.CurrentCulture));
                    Response.AppendHeader("Content-Disposition", String.Format("attachment;filename=" + arquivoZipCDA30));

                    Response.BinaryWrite(msZipCDA30.ToArray());
                    msZipCDA30.Close();

                    HttpContext.Current.ApplicationInstance.CompleteRequest();
                    Response.End();

                    #endregion
                    break;
                case (int)TipoExportacao_FormatoMMYY.CDA40:
                    #region CDA 4.0

                    var ms40 = (Dictionary<string, MemoryStream>)Session["streamCDA"];
                    var arquivoCda40 = new Archive();

                    // Saida
                    var msZipCda40 = new MemoryStream();
                    const string arquivoZipCda40 = "CDA40.zip";

                    try
                    {
                        var j = 0;
                        foreach (var pair in ms40)
                        {
                            arquivoCda40.Add(pair.Value); // Memory Stream
                            arquivoCda40[j].Name = pair.Key; // Nome do Arquivo
                            j++;
                        }
                        arquivoCda40.Zip(msZipCda40);
                    }
                    catch (Exception e1)
                    {
                        throw new Exception(e1.Message + "Erro ao Zipar o Arquivo");
                    }

                    Response.ClearContent();
                    Response.ClearHeaders();
                    Response.Buffer = true;
                    Response.Cache.SetCacheability(HttpCacheability.Private);
                    Response.ContentType = "application/zip";
                    Response.AddHeader("Content-Length", msZipCda40.Length.ToString(CultureInfo.CurrentCulture));
                    Response.AppendHeader("Content-Disposition", String.Format("attachment;filename=" + arquivoZipCda40));

                    Response.BinaryWrite(msZipCda40.ToArray());
                    msZipCda40.Close();

                    HttpContext.Current.ApplicationInstance.CompleteRequest();
                    Response.End();

                    #endregion
                    break;
                case (int)TipoExportacao_FormatoMMYY.Picl:
                    #region Picl

                    MemoryStream msPicl = (MemoryStream)(Session["streamArquivoPicl"]);
                    string nomeArquivoPicl = Convert.ToString(Session["nomeArquivoPicl"]);
                    //            
                    Response.ClearContent();
                    Response.ClearHeaders();
                    Response.Buffer = true;
                    Response.Cache.SetCacheability(HttpCacheability.Private);
                    Response.ContentType = "text/plain";
                    Response.AddHeader("Content-Length", msPicl.Length.ToString(CultureInfo.CurrentCulture));
                    Response.AppendHeader("Content-Disposition", String.Format("attachment;filename=" + nomeArquivoPicl));

                    Response.BinaryWrite(msPicl.ToArray());
                    msPicl.Close();
                    //
                    HttpContext.Current.ApplicationInstance.CompleteRequest();
                    Response.End();

                    #endregion
                    break;
                case (int)TipoExportacao_FormatoMMYY.PerfilMensalCVM:
                    #region TabelaPerfilMensal

                    MemoryStream msTabelaPerfilMensal = (MemoryStream)(Session["streamArquivoTabelaPerfilMensal"]);
                    string nomeArquivoTabelaPerfilMensal = Convert.ToString(Session["nomeArquivoTabelaPerfilMensal"]);
                    //            
                    Response.ClearContent();
                    Response.ClearHeaders();
                    Response.Buffer = true;
                    Response.Cache.SetCacheability(HttpCacheability.Private);
                    Response.ContentType = "text/plain";
                    Response.AddHeader("Content-Length", msTabelaPerfilMensal.Length.ToString(CultureInfo.CurrentCulture));
                    Response.AppendHeader("Content-Disposition", String.Format("attachment;filename=" + nomeArquivoTabelaPerfilMensal));

                    Response.BinaryWrite(msTabelaPerfilMensal.ToArray());
                    msTabelaPerfilMensal.Close();
                    //
                    HttpContext.Current.ApplicationInstance.CompleteRequest();
                    Response.End();

                    #endregion
                    break;
                case (int)TipoExportacao_FormatoMMYY.PerfilMensalCVM_XML_V3:
                    #region TabelaPerfilMensal

                    MemoryStream msTabelaPerfilMensal_XMl_3 = (MemoryStream)(Session["streamArquivoTabelaPerfilMensal_XMl_3"]);
                    string nomeArquivoTabelaPerfilMensal_XMl_3 = Convert.ToString(Session["nomeArquivoTabelaPerfilMensal_XMl_3"]);
                    //            
                    Response.ClearContent();
                    Response.ClearHeaders();
                    Response.Buffer = true;
                    Response.Cache.SetCacheability(HttpCacheability.Private);
                    Response.ContentType = "text/plain";
                    Response.AddHeader("Content-Length", msTabelaPerfilMensal_XMl_3.Length.ToString(CultureInfo.CurrentCulture));
                    Response.AppendHeader("Content-Disposition", String.Format("attachment;filename=" + nomeArquivoTabelaPerfilMensal_XMl_3));

                    Response.BinaryWrite(msTabelaPerfilMensal_XMl_3.ToArray());
                    msTabelaPerfilMensal_XMl_3.Close();
                    //
                    HttpContext.Current.ApplicationInstance.CompleteRequest();
                    Response.End();

                    #endregion
                    break;
                case (int)TipoExportacao_FormatoMMYY.RankingAnbimaAtivo:
                case (int)TipoExportacao_FormatoMMYY.RankingAnbimaPassivo:
                    #region RankingAnbima

                    MemoryStream msRankingAnbima = (MemoryStream)(Session["streamArquivoRankingAnbima"]);
                    string nomeArquivoRankingAnbima = Convert.ToString(Session["nomeArquivoRankingAnbima"]);
                    //            
                    Response.ClearContent();
                    Response.ClearHeaders();
                    Response.Buffer = true;
                    Response.Cache.SetCacheability(HttpCacheability.Private);
                    Response.ContentType = "text/plain";
                    Response.AddHeader("Content-Length", msRankingAnbima.Length.ToString(CultureInfo.CurrentCulture));
                    Response.AppendHeader("Content-Disposition", String.Format("attachment;filename=" + nomeArquivoRankingAnbima));

                    Response.BinaryWrite(msRankingAnbima.ToArray());
                    msRankingAnbima.Close();
                    //
                    HttpContext.Current.ApplicationInstance.CompleteRequest();
                    Response.End();

                    #endregion
                    break;
                case (int)TipoExportacao_FormatoMMYY.LaminaPDF:
                    #region Lamina

                    Dictionary<string, MemoryStream> msLamina = (Dictionary<string, MemoryStream>)Session["streamLamina"];
                    Archive arquivoLamina = new Archive();

                    // Saida
                    MemoryStream msZipLamina = new MemoryStream();
                    string arquivoZipLamina = "LaminaEssencial.zip";

                    try {
                        int j = 0;
                        foreach (KeyValuePair<string, MemoryStream> pair in msLamina) {
                            arquivoLamina.Add(pair.Value); // Memory Stream
                            arquivoLamina[j].Name = pair.Key; // Nome do Arquivo
                            //
                            j++;
                        }
                        //
                        arquivoLamina.Zip(msZipLamina);
                    }
                    catch (Exception e1) {
                        throw new Exception(e1.Message + "Erro ao Zipar o Arquivo");
                    }

                    Response.ClearContent();
                    Response.ClearHeaders();
                    Response.Buffer = true;
                    Response.Cache.SetCacheability(HttpCacheability.Private);
                    Response.ContentType = "application/zip";
                    Response.AddHeader("Content-Length", msZipLamina.Length.ToString(CultureInfo.CurrentCulture));
                    Response.AppendHeader("Content-Disposition", String.Format("attachment;filename=" + arquivoZipLamina));

                    Response.BinaryWrite(msZipLamina.ToArray());
                    msZipLamina.Close();

                    HttpContext.Current.ApplicationInstance.CompleteRequest();
                    Response.End();

                    #endregion
                    break;

                case (int)TipoExportacao_FormatoMMYY.LaminaXML:
                    #region Lamina XML

                    MemoryStream msLaminaXML = (MemoryStream)(Session["streamArquivoLamina"]);
                    string nomeArquivoLaminaXML = Convert.ToString(Session["nomeArquivoLamina"]);
                    //            
                    Response.ClearContent();
                    Response.ClearHeaders();
                    Response.Buffer = true;
                    Response.Cache.SetCacheability(HttpCacheability.Private);
                    Response.ContentType = "text/plain";
                    Response.AddHeader("Content-Length", msLaminaXML.Length.ToString(CultureInfo.CurrentCulture));
                    Response.AppendHeader("Content-Disposition", String.Format("attachment;filename=" + nomeArquivoLaminaXML));

                    Response.BinaryWrite(msLaminaXML.ToArray());
                    msLaminaXML.Close();
                    //
                    HttpContext.Current.ApplicationInstance.CompleteRequest();
                    Response.End();

                    #endregion
                    break;

                case (int)TipoExportacao_FormatoMMYY.LaminaPDF_V1:
                    #region Lamina

                    Dictionary<string, MemoryStream> msLaminaV1 = (Dictionary<string, MemoryStream>)Session["streamLaminaV1"];
                    Archive arquivoLaminaV1 = new Archive();

                    // Saida
                    MemoryStream msZipLaminaV1 = new MemoryStream();
                    string arquivoZipLaminaV1 = "LaminaEssencialV1.zip";

                    try {
                        int j = 0;
                        foreach (KeyValuePair<string, MemoryStream> pair in msLaminaV1) {
                            arquivoLaminaV1.Add(pair.Value); // Memory Stream
                            arquivoLaminaV1[j].Name = pair.Key; // Nome do Arquivo
                            //
                            j++;
                        }
                        //
                        arquivoLaminaV1.Zip(msZipLaminaV1);
                    }
                    catch (Exception e1) {
                        throw new Exception(e1.Message + "Erro ao Zipar o Arquivo");
                    }

                    Response.ClearContent();
                    Response.ClearHeaders();
                    Response.Buffer = true;
                    Response.Cache.SetCacheability(HttpCacheability.Private);
                    Response.ContentType = "application/zip";
                    Response.AddHeader("Content-Length", msZipLaminaV1.Length.ToString(CultureInfo.CurrentCulture));
                    Response.AppendHeader("Content-Disposition", String.Format("attachment;filename=" + arquivoZipLaminaV1));

                    Response.BinaryWrite(msZipLaminaV1.ToArray());
                    msZipLaminaV1.Close();

                    HttpContext.Current.ApplicationInstance.CompleteRequest();
                    Response.End();

                    #endregion
                    break;

                case (int)TipoExportacao_FormatoMMYY.LaminaXML_V1:
                    #region Lamina XML

                    MemoryStream msLaminaXMLV1 = (MemoryStream)(Session["streamArquivoLaminaV1"]);
                    string nomeArquivoLaminaXMLV1 = Convert.ToString(Session["nomeArquivoLaminaV1"]);
                    //            
                    Response.ClearContent();
                    Response.ClearHeaders();
                    Response.Buffer = true;
                    Response.Cache.SetCacheability(HttpCacheability.Private);
                    Response.ContentType = "text/plain";
                    Response.AddHeader("Content-Length", msLaminaXMLV1.Length.ToString(CultureInfo.CurrentCulture));
                    Response.AppendHeader("Content-Disposition", String.Format("attachment;filename=" + nomeArquivoLaminaXMLV1));

                    Response.BinaryWrite(msLaminaXMLV1.ToArray());
                    msLaminaXMLV1.Close();
                    //
                    HttpContext.Current.ApplicationInstance.CompleteRequest();
                    Response.End();

                    #endregion
                    break;
                case (int)TipoExportacao_FormatoMMYY.PerfilMensalCVM_PDF:

                    #region TabelaPerfilMensal
                    Dictionary<string, MemoryStream> msPerfilMensal = (Dictionary<string, MemoryStream>)Session["streamTabelaPerfil"];
                    Archive arquivoPerfilMensal = new Archive();
                    
                    // Saida
                    MemoryStream msZipPerfilMensal = new MemoryStream();
                    string arquivoZipPerfilMensal = "PerfilMensal.zip";

                    try {
                        int j = 0;

                        foreach (KeyValuePair<string, MemoryStream> pair in msPerfilMensal) {
                            arquivoPerfilMensal.Add(pair.Value); // Memory Stream
                            arquivoPerfilMensal[j].Name = pair.Key; // Nome do Arquivo
                            //
                            j++;
                        }
                        //
                        arquivoPerfilMensal.Zip(msZipPerfilMensal);

                   }
                   catch (Exception e1) {
                      throw new Exception(e1.Message + "Erro ao Zipar o Arquivo");
                   }

                   Response.ClearContent();
                   Response.ClearHeaders();
                   Response.Buffer = true;
                   Response.Cache.SetCacheability(HttpCacheability.Private);
                   Response.ContentType = "application/zip";
                   Response.AddHeader("Content-Length", msZipPerfilMensal.Length.ToString(CultureInfo.CurrentCulture));
                   Response.AppendHeader("Content-Disposition", String.Format("attachment;filename=" + arquivoZipPerfilMensal));

                   Response.BinaryWrite(msZipPerfilMensal.ToArray());
                   msZipPerfilMensal.Close();

                   HttpContext.Current.ApplicationInstance.CompleteRequest();
                   Response.End();

                   #endregion
                   break;
               case (int)TipoExportacao_FormatoMMYY.PerfilMensalCVM_PDF_V3:

                   #region TabelaPerfilMensal versao 3 PDF
                   Dictionary<string, MemoryStream> msPerfilMensal_PDF_V3 = (Dictionary<string, MemoryStream>)Session["streamTabelaPerfilPDF_3"];
                   Archive arquivoPerfilMensal_PDF_V3 = new Archive();

                   // Saida
                   MemoryStream msZipPerfilMensal_PDF_V3 = new MemoryStream();
                   string arquivoZipPerfilMensal_PDF_V3 = "PerfilMensalV3.zip";

                   try {
                       int j = 0;

                       foreach (KeyValuePair<string, MemoryStream> pair in msPerfilMensal_PDF_V3) {
                           arquivoPerfilMensal_PDF_V3.Add(pair.Value); // Memory Stream
                           arquivoPerfilMensal_PDF_V3[j].Name = pair.Key; // Nome do Arquivo
                           //
                           j++;
                       }
                       //
                       arquivoPerfilMensal_PDF_V3.Zip(msZipPerfilMensal_PDF_V3);

                   }
                   catch (Exception e1) {
                       throw new Exception(e1.Message + "Erro ao Zipar o Arquivo");
                   }

                   Response.ClearContent();
                   Response.ClearHeaders();
                   Response.Buffer = true;
                   Response.Cache.SetCacheability(HttpCacheability.Private);
                   Response.ContentType = "application/zip";
                   Response.AddHeader("Content-Length", msZipPerfilMensal_PDF_V3.Length.ToString(CultureInfo.CurrentCulture));
                   Response.AppendHeader("Content-Disposition", String.Format("attachment;filename=" + arquivoZipPerfilMensal_PDF_V3));

                   Response.BinaryWrite(msZipPerfilMensal_PDF_V3.ToArray());
                   msZipPerfilMensal_PDF_V3.Close();

                   HttpContext.Current.ApplicationInstance.CompleteRequest();
                   Response.End();

                   #endregion
                   break;
               case (int)TipoExportacao_FormatoMMYY.Cosif4010:
                   #region Cosif

                   MemoryStream msCosif4010 = (MemoryStream)(Session["streamArquivoCosif"]);
                   string nomeArquivoCosif4010= Convert.ToString(Session["nomeArquivoCosif"]);
                   //            
                   Response.ClearContent();
                   Response.ClearHeaders();
                   Response.Buffer = true;
                   Response.Cache.SetCacheability(HttpCacheability.Private);
                   Response.ContentType = "text/plain";
                   Response.AddHeader("Content-Length", msCosif4010.Length.ToString(CultureInfo.CurrentCulture));
                   Response.AppendHeader("Content-Disposition", String.Format("attachment;filename=" + nomeArquivoCosif4010));

                   Response.BinaryWrite(msCosif4010.ToArray());
                   msCosif4010.Close();
                   //
                   HttpContext.Current.ApplicationInstance.CompleteRequest();
                   Response.End();

                   #endregion
                   break;

               case (int)TipoExportacao_FormatoMMYY.Cosif4016:
                   #region Cosif

                   MemoryStream msCosif4016 = (MemoryStream)(Session["streamArquivoCosif"]);
                   string nomeArquivoCosif4016 = Convert.ToString(Session["nomeArquivoCosif"]);
                   //            
                   Response.ClearContent();
                   Response.ClearHeaders();
                   Response.Buffer = true;
                   Response.Cache.SetCacheability(HttpCacheability.Private);
                   Response.ContentType = "text/plain";
                   Response.AddHeader("Content-Length", msCosif4016.Length.ToString(CultureInfo.CurrentCulture));
                   Response.AppendHeader("Content-Disposition", String.Format("attachment;filename=" + nomeArquivoCosif4016));

                   Response.BinaryWrite(msCosif4016.ToArray());
                   msCosif4016.Close();
                   //
                   HttpContext.Current.ApplicationInstance.CompleteRequest();
                   Response.End();

                   #endregion
                   break;

            }
        }
        #endregion

        #region Formato yyyy
        if (Convert.ToInt32(tipo) >= 500 && Convert.ToInt32(tipo) <= 999)
        {
            switch ((int)Enum.Parse(typeof(TipoExportacao_FormatoYYYY), tipo))
            {
                case (int)TipoExportacao_FormatoYYYY.Dirf:
                    #region Dirf

                    MemoryStream msDirf = (MemoryStream)(Session["streamArquivoDirf"]);
                    string nomeArquivoDirf = Convert.ToString(Session["nomeArquivoDirf"]);
                    //            
                    Response.ClearContent();
                    Response.ClearHeaders();
                    Response.Buffer = true;
                    Response.Cache.SetCacheability(HttpCacheability.Private);
                    Response.ContentType = "text/plain";
                    Response.AddHeader("Content-Length", msDirf.Length.ToString(CultureInfo.CurrentCulture));
                    Response.AppendHeader("Content-Disposition", String.Format("attachment;filename=" + nomeArquivoDirf));

                    Response.BinaryWrite(msDirf.ToArray());
                    msDirf.Close();
                    //
                    HttpContext.Current.ApplicationInstance.CompleteRequest();
                    Response.End();

                    #endregion
                    break;
                case (int)TipoExportacao_FormatoYYYY.InformeRendimento:
                    #region InformeRendimento

                    MemoryStream msInformeRendimento = (MemoryStream)(Session["streamArquivoInformeRendimento"]);
                    string nomeArquivoInformeRendimento = Convert.ToString(Session["nomeArquivoInformeRendimento"]);
                    //            
                    Response.ClearContent();
                    Response.ClearHeaders();
                    Response.Buffer = true;
                    Response.Cache.SetCacheability(HttpCacheability.Private);
                    Response.ContentType = "text/plain";
                    Response.AddHeader("Content-Length", msInformeRendimento.Length.ToString(CultureInfo.CurrentCulture));
                    Response.AppendHeader("Content-Disposition", String.Format("attachment;filename=" + nomeArquivoInformeRendimento));

                    Response.BinaryWrite(msInformeRendimento.ToArray());
                    msInformeRendimento.Close();
                    //
                    HttpContext.Current.ApplicationInstance.CompleteRequest();
                    Response.End();

                    #endregion
                    break;
            }
        }
        #endregion

        #region Formato Periodo
        if (Convert.ToInt32(tipo) >= 1000)
        {
            switch ((int)Enum.Parse(typeof(TipoExportacao_FormatoPeriodo), tipo))
            {
                case (int)TipoExportacao_FormatoPeriodo.ContabilAtt:
                    #region ContabAtt

                    MemoryStream msContabilAtt = (MemoryStream)(Session["streamArquivoContabilAtt"]);
                    string nomeArquivoContabilAtt = Convert.ToString(Session["nomeArquivoContabilAtt"]);
                    //            
                    Response.ClearContent();
                    Response.ClearHeaders();
                    Response.Buffer = true;
                    Response.Cache.SetCacheability(HttpCacheability.Private);
                    Response.ContentType = "text/plain";
                    Response.AddHeader("Content-Length", msContabilAtt.Length.ToString(CultureInfo.CurrentCulture));
                    Response.AppendHeader("Content-Disposition", String.Format("attachment;filename=" + nomeArquivoContabilAtt));

                    Response.BinaryWrite(msContabilAtt.ToArray());
                    msContabilAtt.Close();
                    //
                    HttpContext.Current.ApplicationInstance.CompleteRequest();
                    Response.End();

                    #endregion
                    break;
                case (int)TipoExportacao_FormatoPeriodo.ContabilInfoBanc:
                case (int)TipoExportacao_FormatoPeriodo.ContabilInfoBancContaFull:

                    #region Contabil

                    Dictionary<string, MemoryStream> msContabil = (Dictionary<string, MemoryStream>)Session["streamContabilInfoBanc"];
                    //            

                    #region Zip

                    Archive arquivo = new Archive();
                    MemoryStream msZip = new MemoryStream();
                    //                
                    try
                    {

                        int j = 0;
                        foreach (KeyValuePair<string, MemoryStream> pair in msContabil)
                        {
                            arquivo.Add(pair.Value); // Memory Stream
                            arquivo[j].Name = pair.Key; // Nome do Arquivo
                            //
                            j++;
                        }

                        //
                        arquivo.Zip(msZip);
                    }
                    catch (Exception e1)
                    {
                        throw new Exception(e1.Message + "Erro ao Zipar o Arquivo");
                    }
                    Response.ClearContent();
                    Response.ClearHeaders();
                    Response.Buffer = true;
                    Response.Cache.SetCacheability(HttpCacheability.Private);
                    Response.ContentType = "application/zip";
                    Response.AddHeader("Content-Length", msZip.Length.ToString(CultureInfo.CurrentCulture));
                    Response.AppendHeader("Content-Disposition", String.Format("attachment;filename=Contabil.zip"));

                    Response.BinaryWrite(msZip.ToArray());
                    msZip.Close();

                    HttpContext.Current.ApplicationInstance.CompleteRequest();
                    Response.End();
                    #endregion

                    #endregion
                    break;
                case (int)TipoExportacao_FormatoPeriodo.Galgo_PlCota:
                    #region Galgo - Pl Cota

                    MemoryStream memoryStreamGalgoPlCota = (MemoryStream)(Session["memoryStreamGalgoPlCota"]);
                    string nomeArquivoGalgoPlCota = Convert.ToString(Session["nomeArquivoGalgoPlCota"]);

                    Response.ClearContent();
                    Response.ClearHeaders();
                    Response.Buffer = true;
                    Response.Cache.SetCacheability(HttpCacheability.Private);
                    Response.ContentType = "text/plain";
                    Response.AddHeader("Content-Length", memoryStreamGalgoPlCota.Length.ToString(CultureInfo.CurrentCulture));
                    Response.AppendHeader("Content-Disposition", String.Format("attachment;filename=" + nomeArquivoGalgoPlCota));

                    Response.BinaryWrite(memoryStreamGalgoPlCota.ToArray());
                    memoryStreamGalgoPlCota.Close();
                    //
                    HttpContext.Current.ApplicationInstance.CompleteRequest();
                    Response.End();

                    #endregion
                    break;
                case (int)TipoExportacao_FormatoPeriodo.Galgo_PlCotaWcf:
                    #region Galgo - Pl Cota

                    string url = System.Configuration.ConfigurationManager.AppSettings["Galgo_ServicePLCota.ServicePLCota"];
                    DateTime dataInicialwcf = DateTime.ParseExact(Convert.ToString(Session["textDataInicial"]), "dd/MM/yyyy", null);
                    DateTime dataFinalwcf = DateTime.ParseExact(Convert.ToString(Session["textDataFinal"]), "dd/MM/yyyy", null);
                    List<int> idClienteswcf = (List<int>)Session["idClientes"];

                    Financial.Fundo.Galgo.InterfaceGalgo galgoWcf = new Financial.Fundo.Galgo.InterfaceGalgo();
                    try
                    {
                        galgoWcf.ExportaPlCotaWcf(url, dataInicialwcf, dataFinalwcf, idClienteswcf);
                    }
                    catch (Exception ex)
                    {
                        throw new Exception(ex.Message);
                    }

                    Response.ClearContent();
                    Response.ClearHeaders();
                    Response.Buffer = true;
                    Response.Cache.SetCacheability(HttpCacheability.Private);
                    
                    HttpContext.Current.ApplicationInstance.CompleteRequest();
                    Response.End();

                    #endregion
                    break;
            }
        }
        #endregion
    }
}