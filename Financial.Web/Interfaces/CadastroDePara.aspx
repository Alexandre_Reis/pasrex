﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="CadastroDePara.aspx.cs" Inherits="Configuracao_CadastroDePara" %>

<%@ Register Assembly="System.Web, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"
    Namespace="System.Web.UI.HtmlControls" TagPrefix="cc2" %>
<%@ Register Assembly="EntitySpaces.Web" Namespace="EntitySpaces.Web" TagPrefix="cc1" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxe" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxcb" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxpc" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxwgv" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <link href="~/css/forms.css" rel="stylesheet" type="text/css" />

    <script type="text/javascript" language="Javascript" src="../js/global.js"></script>

    <script type="text/javascript" language="Javascript">
        var popup = true;
        document.onkeydown = onDocumentKeyDown;
        var operacao = '';
    </script>

    <script type="text/javascript" language="Javascript">
        var isCustomCallback = false;   // usada para controlar o refresh após um delete feito no grid
    </script>

</head>
<body>
    <form id="form1" runat="server">
        <dxcb:ASPxCallback ID="callbackErro" runat="server" OnCallback="callbackErro_Callback">
            <ClientSideEvents CallbackComplete="function(s, e) {             
            if (e.result != '')
            {                   
                alert(e.result);                              
            }
            else
            {
                if (operacao == 'salvar')
                {             
                    gridCadastro.UpdateEdit();
                }
                else
                {
                    callbackAdd.SendCallback();
                }  
            } 
            operacao = '';
        }        
        " />
        </dxcb:ASPxCallback>
        <dxcb:ASPxCallback ID="callbackAdd" runat="server" OnCallback="callbackAdd_Callback">
            <ClientSideEvents CallbackComplete="function(s, e) 
        {
            alert('Cadastro feito com sucesso.');
        }        
        " />
        </dxcb:ASPxCallback>
        <div class="divPanel">
            <table width="100%">
                <tr>
                    <td>
                        <div id="container">
                            <div id="header">
                                <asp:Label ID="lblHeader" runat="server" Text="Cadastro de DePara"></asp:Label>
                            </div>
                            <div id="mainContent">
                                <div class="linkButton">
                                    <asp:LinkButton ID="btnAdd" runat="server" Font-Overline="false" ValidationGroup="ATK"
                                        CssClass="btnAdd" OnClientClick="gridCadastro.AddNewRow(); return false;">
                                        <asp:Literal ID="Literal1" runat="server" Text="Novo" /><div>
                                        </div>
                                    </asp:LinkButton>
                                    <asp:LinkButton ID="btnDelete" runat="server" Font-Overline="false" ValidationGroup="ATK"
                                        CssClass="btnDelete" OnClientClick=" if (confirm('Tem certeza que quer excluir?')==true) gridCadastro.PerformCallback('btnDelete');return false;">
                                        <asp:Literal ID="Literal2" runat="server" Text="Excluir" /><div>
                                        </div>
                                    </asp:LinkButton>
                                    <asp:LinkButton ID="btnPDF" runat="server" Font-Overline="false" ValidationGroup="ATK"
                                        CssClass="btnPdf" OnClick="btnPDF_Click">
                                        <asp:Literal ID="Literal4" runat="server" Text="Gerar PDF" /><div>
                                        </div>
                                    </asp:LinkButton>
                                    <asp:LinkButton ID="btnExcel" runat="server" Font-Overline="false" ValidationGroup="ATK"
                                        CssClass="btnExcel" OnClick="btnExcel_Click">
                                        <asp:Literal ID="Literal5" runat="server" Text="Gerar Excel" /><div>
                                        </div>
                                    </asp:LinkButton>
                                    <asp:LinkButton ID="btnRefresh" runat="server" Font-Overline="false" CssClass="btnRefresh"
                                        OnClientClick=" gridCadastro.PerformCallback('btnRefresh'); return false;">
                                        <asp:Literal ID="Literal6" runat="server" Text="Atualizar" /><div>
                                        </div>
                                    </asp:LinkButton>
                                </div>
                                <div class="divDataGrid">
                                    <dxwgv:ASPxGridView ID="gridCadastro" runat="server" KeyFieldName="IdDePara" DataSourceID="EsDSDePara"
                                        OnCustomCallback="gridCadastro_CustomCallback" OnRowInserting="gridCadastro_RowInserting"
                                        OnRowUpdating="gridCadastro_RowUpdating"
                                        OnPreRender="gridCadastro_PreRender"
                                        AutoGenerateColumns="False">
                                        <Columns>
                                            <dxwgv:GridViewCommandColumn VisibleIndex="0" ShowClearFilterButton="True">
                                                <HeaderTemplate>
                                                    <dxe:ASPxCheckBox ID="cbAll" ClientInstanceName="cbAll" runat="server" ToolTip="Seleciona todos os CheckBoxs de todas as páginas" BackColor="white" ClientSideEvents-CheckedChanged="function(s, e) {OnAllCheckedChanged(s, e, gridCadastro);}" OnInit="CheckBoxSelectAll"/>
                                                </HeaderTemplate>
                                            </dxwgv:GridViewCommandColumn>
                                            <dxwgv:GridViewDataSpinEditColumn FieldName="IdDePara" VisibleIndex="1" Width="15%"
                                                ReadOnly="True">
                                                <PropertiesSpinEdit DisplayFormatString="n0" NumberFormat="Custom">
                                                    <SpinButtons ShowIncrementButtons="False">
                                                    </SpinButtons>
                                                    <ValidationSettings>
                                                        <RequiredField ErrorText=""></RequiredField>
                                                    </ValidationSettings>
                                                </PropertiesSpinEdit>
                                            </dxwgv:GridViewDataSpinEditColumn>
                                            <dxwgv:GridViewDataComboBoxColumn FieldName="IdTipoDePara" Caption="Tipo De Para" Width="35%"
                                                VisibleIndex="2">
                                                <PropertiesComboBox datasourceid="EsDSTipoDePara" textfield="Descricao" valuefield="IdTipoDePara"
                                                    incrementalfilteringmode="startswith" />
                                            </dxwgv:GridViewDataComboBoxColumn>
                                            <dxwgv:GridViewDataTextColumn FieldName="CodigoInterno" Caption="Código Interno"
                                                Width="15%" VisibleIndex="3" />
                                            <dxwgv:GridViewDataTextColumn FieldName="CodigoExterno" Caption="Código Externo"
                                                Width="25%" VisibleIndex="4" />
                                            <dxwgv:GridViewDataTextColumn FieldName="DescricaoOrigem" Caption="Descrição Origem"
                                                Width="25%" VisibleIndex="5" />
                                        </Columns>
                                        <Templates>
                                            <EditForm>
                                                <div class="editForm">
                                                    <table>
                                                        <tr>
                                                            <td class="td_Label">
                                                                <asp:Label ID="labelIdTipoDePara" runat="server" CssClass="labelRequired" Text="Tipo de Para:" />
                                                            </td>
                                                            <td>
                                                                <dxe:ASPxComboBox ID="dropTipoDePara" runat="server" ShowShadow="false" DropDownStyle="DropDownList"
                                                                    CssClass="dropDownListLongo" DataSourceID="EsDSTipoDePara" TextField="Descricao" Text='<%#Eval("IdTipoDePara")%>'
                                                                    ValueField="IdTipoDePara" />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="td_Label">
                                                                <asp:Label ID="labelCodigoInterno" runat="server" CssClass="labelRequired" Text="Código Interno:" />
                                                            </td>
                                                            <td>
                                                                <dxe:ASPxTextBox ID="textCodigoInterno" ClientInstanceName="textCodigoInterno" runat="server" CssClass="textLongo"
                                                                    MaxLength="50" Text='<%#Eval("CodigoInterno")%>' />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="td_Label">
                                                                <asp:Label ID="labelCodigoExterno" runat="server" Text="Código Externo:" CssClass="labelRequired" />
                                                            </td>
                                                            <td>
                                                                <dxe:ASPxTextBox ID="textCodigoExterno" ClientInstanceName="textCodigoExterno" runat="server"
                                                                    CssClass="textLongo" MaxLength="50" Text='<%#Eval("CodigoExterno")%>' />
                                                            </td>
                                                        </tr>
                                                    </table>
                                                    <div class="linhaH">
                                                    </div>
                                                    <div class="linkButton linkButtonNoBorder popupFooter">
                                                        <asp:LinkButton ID="btnOKAdd" runat="server" Font-Overline="false" CssClass="btnSaveadd"
                                                            OnInit="btnOKAdd_Init" OnClientClick="if (operacao != '') return false; operacao='salvarAdd'; callbackErro.SendCallback(); return false;">
                                                            <asp:Literal ID="Literal5" runat="server" Text="OK+" /><div>
                                                            </div>
                                                        </asp:LinkButton><asp:LinkButton ID="btnOK" runat="server" Font-Overline="false"
                                                            CssClass="btnOK" OnClientClick="if (operacao != '') return false; operacao='salvar'; callbackErro.SendCallback(); return false;">
                                                            <asp:Literal ID="Literal1" runat="server" Text="OK" /><div>
                                                            </div>
                                                        </asp:LinkButton><asp:LinkButton ID="btnCancel" runat="server" Font-Overline="false"
                                                            CssClass="btnCancel" OnClientClick=" gridCadastro.PerformCallback('btnCancel'); return false;">
                                                            <asp:Literal ID="Literal2" runat="server" Text="Cancelar" /><div>
                                                            </div>
                                                        </asp:LinkButton></div>
                                                </div>
                                            </EditForm>
                                        </Templates>
                                        <SettingsPopup EditForm-Width="300px" />
                                        <ClientSideEvents Init="function(s, e) { FocusField(gridCadastro.cpTextDescricao);}"
                                            BeginCallback="function(s, e) {
		                if (e.command == 'CUSTOMCALLBACK') {
                            isCustomCallback = true;
                        }						
                    }" EndCallback="function(s, e) {
			            if (isCustomCallback) {
                            isCustomCallback = false;
                            s.Refresh();
                        }
                    }" />
                                        <SettingsCommandButton>
                                            <ClearFilterButton>
                                                <Image Url="../../imagens/funnel--minus.png">
                                                </Image>
                                            </ClearFilterButton>
                                            <UpdateButton>
                                                <Image Url="../../imagens/ico_form_ok_inline.gif">
                                                </Image>
                                            </UpdateButton>
                                            <CancelButton>
                                                <Image Url="../../imagens/ico_form_back_inline.gif">
                                                </Image>
                                            </CancelButton>
                                        </SettingsCommandButton>
                                    </dxwgv:ASPxGridView>
                                </div>
                            </div>
                        </div>
                    </td>
                </tr>
            </table>
        </div>
        <div class="divDataGrid">
            &nbsp;</div>
        <dxwgv:ASPxGridViewExporter ID="gridExport" runat="server" GridViewID="gridCadastro"
            LeftMargin="50" RightMargin="50" />
        <cc1:esDataSource ID="EsDSDePara" runat="server" OnesSelect="EsDSDePara_esSelect" />
        <cc1:esDataSource ID="EsDSTipoDePara" runat="server" OnesSelect="EsDSTipoDePara_esSelect" />
    </form>
</body>
</html>
