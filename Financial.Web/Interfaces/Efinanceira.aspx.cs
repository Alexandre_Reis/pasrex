﻿using System.Linq;
using DevExpress.Utils;
using DevExpress.Web;
using Financial.BMF;
using Financial.Common;
using Financial.CRM;
using Financial.Export;
using Financial.Fundo;
using Financial.Interfaces.Import.Fundo;
using Financial.Investidor;
using Financial.ReconLight;
using Financial.RendaFixa;
using Financial.RendaFixa.Enums;
using Financial.Security;
using Financial.Web.Common;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.IO;
using System.Text;
using System.Web;
using System.Web.UI.WebControls;
using System.Xml;

/// <summary>
/// 
/// </summary>
public partial class CadastrosBasicos_Efinanceira : CadastroBasePage
{
    #region Fields
    private Atlas.Link.Settings.AtlasLinkSection _config;
    #endregion

    #region page load event
    /// <summary>
    /// Usado quando a pagina de cadastro está dentro de um tabPage
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    new protected void Page_Load(object sender, EventArgs e)
    {
        #region Fields
        _config = (Atlas.Link.Settings.AtlasLinkSection)ConfigurationManager.GetSection("atlasLinkGroup/atlasLink");
        #endregion

        var possuiReconLight = _config.Efinanceira.PossuiInterface;

        if (!possuiReconLight)
            Server.Transfer("~/default.aspx", false);

        //passar o valor do para um hiddenfield para que o javascript possa utilizar
        timeoutAtualizacaoRecon.Value = _config.Efinanceira.TimeoutAtualizacao.ToString();

        //gridcadastro
        HasFiltro = false;
        HasPopupCliente = false;
        HasPopupAtivoBolsa = false;
        HasPanelFieldsLoading = false;
        AllowDelete = false;

        base.Page_Load(sender, e);

        if (!Page.IsPostBack)
        {
        }

        InicializaGridFila();

        int idRecon = 0;
        if (Request.QueryString["Download"] == "true" && int.TryParse(Request.QueryString["IdRecon"], out idRecon))
            Download(idRecon);

    }
    #endregion

    #region Structures
    /// <summary>
    /// CarteiraXml
    /// </summary>
    public struct CarteiraXml
    {
        #region Properties
        public String Cnpj;
        public String CodigoIsin;
        public String Nome;
        public Int32? IdCarteira;
        #endregion

        #region constructor
        /// <summary>
        /// Initializes a new instance of the <see cref="CarteiraXml"/> struct.
        /// </summary>
        /// <param name="codigoIsin">The codigo isin.</param>
        /// <param name="cnpj">The CNPJ.</param>
        /// <param name="nome">The nome.</param>
        /// <param name="idCarteira">The identifier carteira.</param>
        public CarteiraXml(String codigoIsin, String cnpj, String nome, Int32? idCarteira)
        {
            Cnpj = cnpj;
            CodigoIsin = codigoIsin;
            Nome = nome;
            IdCarteira = idCarteira;
        }
        #endregion
    }
    #endregion

    #region Enums
    /// <summary>
    /// TipoAgente
    /// </summary>
    private enum TipoAgente
    {
        Administrador = 1,
        Gestor = 2,
        Custodiante = 3
    }
    #endregion

    #region Metodos de validacao

    #endregion

    #region metodos de persistencia
    /// <summary>
    /// Adds the recon.
    /// </summary>
    /// <param name="xmlClienteStream">The XML cliente stream.</param>
    /// <param name="descricao">The descricao.</param>
    /// <param name="idUsuario">The identifier usuario.</param>
    /// <exception cref="System.Exception">
    /// Erro na leitura do xml:  + ex.Message
    /// or
    /// Existem campos necessarios para o processamento que estão nulos ou vazios no banco de dados. Para mais detalhes favor verificar a observação desta recon.
    /// or
    /// Erro ao criar xml:  + ex.Message
    /// or
    /// Erro ao salvar xml em disco:  + ex.Message
    /// or
    /// Erro na leitura do xml: Não foram encontrados clientes validos
    /// </exception>
    public void AddRecon()
    {
        var controller = new ControllerEfinanceira();

        var usuario = new Usuario();
        usuario.Query.Where(usuario.Query.Login.Equal(HttpContext.Current.User.Identity.Name));
        if (!usuario.Query.Load()) return;

        var recon = new Recon { DataRegistro = DateTime.Now, IdUsuario = usuario.IdUsuario.Value, Tipo = Atlas.Link.Model.Enums.Tipo.EFINANCEIRA.ToString() };
        recon.Save();
    }
    #endregion

    #region Eventos
    //estou salvando no metodo de upload do xml pois é necessario salvar o arquivo apenas apos a criação no banco de dados
    /// <summary>
    /// Handles the RowInserting event of the gridFila control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="DevExpress.Web.Data.ASPxDataInsertingEventArgs"/> instance containing the event data.</param>
    protected void gridFila_RowInserting(object sender, DevExpress.Web.Data.ASPxDataInsertingEventArgs e)
    {
        e.Cancel = true;
        gridFila.CancelEdit();

    }

    /// <summary>
    /// Handles the RowUpdating event of the gridFila control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="DevExpress.Web.Data.ASPxDataUpdatingEventArgs"/> instance containing the event data.</param>
    protected void gridFila_RowUpdating(object sender, DevExpress.Web.Data.ASPxDataUpdatingEventArgs e)
    {
        e.Cancel = true;
        gridFila.CancelEdit();
    }

    /// <summary>
    /// Handles the HtmlRowPrepared event of the gridCadastro control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="ASPxGridViewTableRowEventArgs"/> instance containing the event data.</param>
    protected void gridCadastro_HtmlRowPrepared(object sender, ASPxGridViewTableRowEventArgs e)
    {
        if (e.RowType != GridViewRowType.Data) return;
        if (e.GetValue("DataCancelamento") is DateTime)
            e.Row.BackColor = System.Drawing.Color.FromArgb(255, 240, 240);
        else
            e.Row.BackColor = System.Drawing.Color.FromArgb(240, 255, 240);
    }

    /// <summary>
    /// Handles the HtmlRowPrepared event of the gridFila control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="ASPxGridViewTableRowEventArgs"/> instance containing the event data.</param>
    protected void gridFila_HtmlRowPrepared(object sender, ASPxGridViewTableRowEventArgs e)
    {
        if (e.RowType != GridViewRowType.Data) return;
        if (e.GetValue("DataInicioProcesso") is DateTime)
            e.Row.BackColor = System.Drawing.Color.FromArgb(255, 255, 240);
    }

    /// <summary>
    /// Handles the CustomButtonInitialize event of the gridCadastro control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="ASPxGridViewCustomButtonEventArgs"/> instance containing the event data.</param>
    protected void gridCadastro_CustomButtonInitialize(object sender, ASPxGridViewCustomButtonEventArgs e)
    {
        var view = (((System.Data.DataRowView)(((ASPxGridView)sender).GetRow(e.VisibleIndex))));
        if (view == null) return;

        if (view.Row.ItemArray[6] is DateTime)
        {
            if (e.ButtonID != "downBtn") return;
            e.Visible = DefaultBoolean.False;
        }
        else
        {
            if (e.ButtonID != "canceladoBtn") return;
            e.Visible = DefaultBoolean.False;
            e.Enabled = false;
        }
    }
    
    /// <summary>
    /// Handles the CustomCallback event of the gridFila control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="ASPxGridViewCustomCallbackEventArgs"/> instance containing the event data.</param>
    protected void gridFila_CustomCallback(object sender, ASPxGridViewCustomCallbackEventArgs e)
    {
        var r = new Recon();
        r.Query.Where(r.Query.IdRecon.Equal(Int32.Parse(e.Parameters)));
        r.Query.Load();

        r.Cancela("EFinanceira cancelada pelo usuario");
        r.Save();

        gridFila.DataBind();
    }

    /// <summary>
    /// Handles the Load event of the btnOk control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
    protected void btnOk_Load(object sender, EventArgs e)
    {
        var btnOk = (LinkButton)sender;
        //btnOk.OnClientClick = "gridFila.UpdateEdit(); return false;";
    }

    #endregion

    #region metodos estaticos
    /// <summary>
    /// Copies the stream.
    /// </summary>
    /// <param name="input">The input.</param>
    /// <param name="output">The output.</param>
    public static void CopyStream(Stream input, Stream output)
    {
        var buffer = new byte[16 * 1024];
        int read;
        while ((read = input.Read(buffer, 0, buffer.Length)) > 0)
        {
            output.Write(buffer, 0, read);
        }
    }
    #endregion

    #region Member
    /// <summary>
    /// Downloads the specified identifier recon.
    /// </summary>
    /// <param name="idRecon">The identifier recon.</param>
    protected void Download(Int32 idRecon)
    {
        var controller = new ControllerEfinanceira();
        var fileInfo = new FileInfo(controller.XlsResultado(idRecon));
        if (fileInfo.Exists)
        {
            Response.Clear();
            Response.AddHeader("Content-Disposition", "attachment; filename=" + fileInfo.Name);
            Response.AddHeader("Content-Length", fileInfo.Length.ToString());
            Response.ContentType = "application/octet-stream";
            Response.Flush();
            Response.TransmitFile(fileInfo.FullName);
            Response.End();
        }
        else
        {
            var coll = new ReconCollection();
            var reconQuery = new ReconQuery();
            coll.Load(reconQuery);
            var r = coll.FindByPrimaryKey(idRecon);
            r.Cancela("Arquivo resultado não foi encontrado.");

            coll.Save();
            Response.Write("<script typ>alert('teste');</script>");
            Response.Flush();
        }
    }
    /// <summary>
    /// Inicializas the grid fila.
    /// </summary>
    protected void InicializaGridFila()
    {
        var gridFila = this.FindControl("gridFila") as ASPxGridView;
        #region Properties básicas
        gridFila.ClientInstanceName = "gridFila";
        gridFila.AutoGenerateColumns = false;
        gridFila.Width = Unit.Percentage(100);
        gridFila.SettingsBehavior.AllowSort = false;
        gridFila.EnableViewState = true;
        gridFila.EnableCallBacks = true;


        //((GridViewCommandColumn)gridFila.Columns[0]).Width = Unit.Percentage(5);
        gridFila.SettingsText.PopupEditFormCaption = "";
        gridFila.SettingsEditing.Mode = GridViewEditingMode.PopupEditForm;
        gridFila.SettingsEditing.PopupEditFormModal = true;
        gridFila.SettingsEditing.PopupEditFormHorizontalAlign = PopupHorizontalAlign.WindowCenter;
        gridFila.SettingsEditing.PopupEditFormVerticalAlign = PopupVerticalAlign.WindowCenter;

        //Pager
        gridFila.SettingsPager.PageSize = 30;

        //Styles
        gridFila.Styles.Header.SortingImageSpacing = Unit.Pixel(5);
        gridFila.Styles.AlternatingRow.Enabled = DefaultBoolean.True;
        gridFila.Styles.Cell.Wrap = DefaultBoolean.True;
        gridFila.Styles.CommandColumn.Cursor = "hand";
        #endregion

    }

    #endregion

    #region data source
    /// <summary>
    /// Handles the esSelect event of the EsDSRecon control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="EntitySpaces.Web.esDataSourceSelectEventArgs"/> instance containing the event data.</param>
    protected void EsDSRecon_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        var coll = new ReconCollection();
        var reconQuery = new ReconQuery("r");
        var usuarioQuery = new UsuarioQuery("u");
        reconQuery.LeftJoin(usuarioQuery).On(reconQuery.IdUsuario == usuarioQuery.IdUsuario);
        reconQuery.Where(reconQuery.Tipo == Atlas.Link.Model.Enums.Tipo.EFINANCEIRA);
        reconQuery.Where(reconQuery.Or(reconQuery.DataFimProcesso.IsNotNull(), reconQuery.DataCancelamento.IsNotNull()));

        reconQuery.Select(reconQuery, usuarioQuery.Nome.As("Usuario"));
        reconQuery.OrderBy(reconQuery.IdRecon.Descending);
        coll.Load(reconQuery);
        e.Collection = coll;
    }

    /// <summary>
    /// Handles the esSelect event of the EsDSFila control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="EntitySpaces.Web.esDataSourceSelectEventArgs"/> instance containing the event data.</param>
    protected void EsDSFila_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        var coll = new ReconCollection();
        var reconQuery = new ReconQuery("r");
        var usuarioQuery = new UsuarioQuery("u");
        reconQuery.LeftJoin(usuarioQuery).On(reconQuery.IdUsuario == usuarioQuery.IdUsuario);
        reconQuery.Where(reconQuery.Tipo == Atlas.Link.Model.Enums.Tipo.EFINANCEIRA);
        reconQuery.Where(reconQuery.DataFimProcesso.IsNull(), reconQuery.DataCancelamento.IsNull());
        reconQuery.Select(reconQuery, usuarioQuery.Nome.As("Usuario"));
        reconQuery.OrderBy(reconQuery.IdRecon.Ascending);
        coll.Load(reconQuery);

        e.Collection = coll;
    }
    #endregion

    #region callbacks

    /// <summary>
    /// Handles the Callback event of the callbackDownload control.
    /// </summary>
    /// <param name="source">The source of the event.</param>
    /// <param name="e">The <see cref="DevExpress.Web.CallbackEventArgs"/> instance containing the event data.</param>
    protected void callbackDownload_Callback(object source, CallbackEventArgs e)
    {
        Int32 idRecon = 0;
        if (Int32.TryParse(((DevExpress.Web.CallbackEventArgsBase)(e)).Parameter, out idRecon))
        {
            var controller = new ControllerEfinanceira();
            var fileInfo = new FileInfo(controller.XlsResultado(idRecon));

            if (fileInfo.Exists)
                e.Result = "PossuiArquivo";
        }
    }

    /// <summary>
    /// Handles the Callback event of the callbackAtualiza control.
    /// </summary>
    /// <param name="source">The source of the event.</param>
    /// <param name="e">The <see cref="DevExpress.Web.CallbackEventArgs"/> instance containing the event data.</param>
    protected void callbackAtualiza_Callback(object source, CallbackEventArgs e)
    {
        var controller = new ControllerEfinanceira();
        e.Result = controller.Atualiza().ToString();


        gridFila.CancelEdit();
    }
    #endregion

    protected void btnOK_Click(object sender, EventArgs e)
    {
        AddRecon();
        gridFila.CancelEdit();
        gridFila.DataBind();
    }
}
