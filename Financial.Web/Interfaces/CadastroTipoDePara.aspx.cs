﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Financial.Web.Common;
using Financial.Common;
using DevExpress.Web;
using System.Collections.Generic;
using Financial.Web.Util;
using Financial.Security.Enums;
using Financial.Security;
using Financial.Util;
using EntitySpaces.Interfaces;
using Financial.Common.Enums;

public partial class Configuracao_CadastroTipoDePara : CadastroBasePage
{
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void EsDSTipoDePara_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        TipoDeParaCollection coll = new TipoDeParaCollection();

        coll.Query.OrderBy(coll.Query.IdTipoDePara.Ascending);
        coll.LoadAll();

        //Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_RowUpdating(object sender, DevExpress.Web.Data.ASPxDataUpdatingEventArgs e)
    {
        TipoDePara tipoDePara = new TipoDePara();

        int IdTipoDePara = (int)e.Keys[0];

        if (tipoDePara.LoadByPrimaryKey(IdTipoDePara))
        {
            ASPxTextBox textDescricao = gridCadastro.FindEditFormTemplateControl("textDescricao") as ASPxTextBox;
            ASPxComboBox dropTipoCampo = gridCadastro.FindEditFormTemplateControl("dropTipoCampo") as ASPxComboBox;
            ASPxTextBox textObservacao = gridCadastro.FindEditFormTemplateControl("textObservacao") as ASPxTextBox;
            ASPxComboBox dropEnumTipoDePara = gridCadastro.FindEditFormTemplateControl("dropEnumTipoDePara") as ASPxComboBox;

            tipoDePara.Descricao = textDescricao.Text;
            tipoDePara.TipoCampo = Convert.ToInt16(dropTipoCampo.SelectedItem.Value);
            tipoDePara.Observacao = textObservacao.Text;
            if (dropEnumTipoDePara.SelectedIndex != -1)
                tipoDePara.EnumTipoDePara = Convert.ToInt16(dropEnumTipoDePara.SelectedItem.Value);
            else
                tipoDePara.EnumTipoDePara = null;

            tipoDePara.Save();

            #region Log do Processo
            HistoricoLog historicoLog = new HistoricoLog();
            historicoLog.InsereHistoricoLog(DateTime.Now,
                                            DateTime.Now,
                                            "Cadastro de Tipo De Para - Operacao: Update Tipo De Para: " + IdTipoDePara + UtilitarioWeb.ToString(tipoDePara),
                                            HttpContext.Current.User.Identity.Name,
                                            UtilitarioWeb.GetIP(Request),
                                            "",
                                            HistoricoLogOrigem.Outros);
            #endregion
        }

        e.Cancel = true;
        gridCadastro.CancelEdit();
        gridCadastro.DataBind();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_RowInserting(object sender, DevExpress.Web.Data.ASPxDataInsertingEventArgs e)
    {
        SalvarNovo();

        e.Cancel = true;
        gridCadastro.CancelEdit();
        gridCadastro.DataBind();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_CustomCallback(object sender, DevExpress.Web.ASPxGridViewCustomCallbackEventArgs e)
    {
        TipoDePara tipoDeParaClone;
        TipoDePara tipoDePara;

        if (e.Parameters == "btnDelete")
        {
            List<object> keyValuesId = gridCadastro.GetSelectedFieldValues(TipoDeParaMetadata.ColumnNames.IdTipoDePara);
            for (int i = 0; i < keyValuesId.Count; i++)
            {
                int idTipoDePara = Convert.ToInt32(keyValuesId[i]);

                tipoDePara = new TipoDePara();
                if (tipoDePara.LoadByPrimaryKey(idTipoDePara))
                {
                    tipoDeParaClone = (TipoDePara)Utilitario.Clone(tipoDePara);
                    tipoDePara = new TipoDePara();
                    tipoDePara.Query.Where(tipoDePara.Query.IdTipoDePara == idTipoDePara);
                    tipoDePara.Query.Load();

                    tipoDePara.MarkAsDeleted();
                    tipoDePara.Save();

                    #region Log do Processo
                    HistoricoLog historicoLog = new HistoricoLog();
                    historicoLog.InsereHistoricoLog(DateTime.Now,
                                                    DateTime.Now,
                                                    "Cadastro de Tipo de Para - Operacao: Delete Tipo  De Para: " + idTipoDePara + UtilitarioWeb.ToString(tipoDeParaClone),
                                                    HttpContext.Current.User.Identity.Name,
                                                    UtilitarioWeb.GetIP(Request),
                                                    "",
                                                    HistoricoLogOrigem.Outros);
                    #endregion
                }
            }
        }


        gridCadastro.DataBind();
        gridCadastro.Selection.UnselectAll();
        gridCadastro.CancelEdit();
    }

    protected void btnOKAdd_Init(object sender, EventArgs e)
    {
        if (!gridCadastro.IsNewRowEditing)
            (sender as LinkButton).Visible = false;
    }

    /// <summary>
    /// Verifica quais campos possuem preenchimento obrigatório
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void callbackErro_Callback(object source, DevExpress.Web.CallbackEventArgs e)
    {
        e.Result = "";

        ASPxTextBox textDescricao = gridCadastro.FindEditFormTemplateControl("textDescricao") as ASPxTextBox;
        ASPxComboBox dropTipoCampo = gridCadastro.FindEditFormTemplateControl("dropTipoCampo") as ASPxComboBox;
        ASPxTextBox textObservacao = gridCadastro.FindEditFormTemplateControl("textObservacao") as ASPxTextBox;
        ASPxComboBox dropEnumTipoDePara = gridCadastro.FindEditFormTemplateControl("dropEnumTipoDePara") as ASPxComboBox;
        ASPxTextBox hiddenIdTipoDePara = gridCadastro.FindEditFormTemplateControl("hiddenIdTipoDePara") as ASPxTextBox;

        #region Campos obrigatórios
        List<Control> controles = new List<Control>();
        controles.Add(textDescricao);
        controles.Add(dropTipoCampo);

        if (base.TestaObrigatorio(controles) != "")
        {
            e.Result = "Campos com * são obrigatórios!";
            return;
        }

        if (dropEnumTipoDePara.SelectedIndex != -1)
        {
            TipoDeParaCollection coll = new TipoDeParaCollection();
            int tipoCampo = Convert.ToInt32(dropTipoCampo.SelectedItem.Value);
            int enumTipoDePara = Convert.ToInt32(dropEnumTipoDePara.SelectedItem.Value);

            //Faz a busca na query
            coll.Query.Select(coll.Query.EnumTipoDePara, coll.Query.TipoCampo);
            coll.Query.Where(coll.Query.EnumTipoDePara.Equal(enumTipoDePara) & coll.Query.TipoCampo.Equal(tipoCampo));

            if (!string.IsNullOrEmpty(hiddenIdTipoDePara.Text))
                coll.Query.Where(coll.Query.IdTipoDePara.NotEqual(Convert.ToInt32(hiddenIdTipoDePara.Text)));

            //Verifica se a query retornou valor
            if (coll.Query.Load())
                throw new Exception("Tipo De Para já cadastrado");
        }

        #endregion
    }

    protected void callbackAdd_Callback(object source, DevExpress.Web.CallbackEventArgs e)
    {
        SalvarNovo();

        e.Result = "";
        if (e.Parameter == "clone")
        {
            //e.Result = 0;
        }
        else
        {
            e.Result = "Cadastro feito com sucesso.";
        }
    }

    /// <summary>
    /// Busca os dados preenchidos do formulario e preenche o objeto para ser salvo no banco de dados
    /// </summary>
    private void SalvarNovo()
    {
        ASPxTextBox textDescricao = gridCadastro.FindEditFormTemplateControl("textDescricao") as ASPxTextBox;
        ASPxComboBox dropTipoCampo = gridCadastro.FindEditFormTemplateControl("dropTipoCampo") as ASPxComboBox;
        ASPxTextBox textObservacao = gridCadastro.FindEditFormTemplateControl("textObservacao") as ASPxTextBox;
        ASPxComboBox dropEnumTipoDePara = gridCadastro.FindEditFormTemplateControl("dropEnumTipoDePara") as ASPxComboBox;

        TipoDePara tipoDePara = new TipoDePara();        
        string EnumTipoDePara = string.Empty;

        tipoDePara.Descricao = textDescricao.Text;
        tipoDePara.TipoCampo = Convert.ToInt16(dropTipoCampo.SelectedItem.Value);
        tipoDePara.Observacao = textObservacao.Text;
        if (dropEnumTipoDePara.SelectedIndex != -1)
            tipoDePara.EnumTipoDePara = Convert.ToInt16(dropEnumTipoDePara.SelectedItem.Value);
        else
            tipoDePara.EnumTipoDePara = null;

        tipoDePara.Save();

        #region Log do Processo
        HistoricoLog historicoLog = new HistoricoLog();
        historicoLog.InsereHistoricoLog(DateTime.Now,
                                        DateTime.Now,
                                        "Cadastro de Tipo De Para - Operacao: Insert Tipo De Para: " + tipoDePara.IdTipoDePara + UtilitarioWeb.ToString(tipoDePara),
                                        HttpContext.Current.User.Identity.Name,
                                        UtilitarioWeb.GetIP(Request),
                                        "",
                                        HistoricoLogOrigem.Outros);
        #endregion
    }

    /// <summary>
    /// Carrega o Tipo Campo com os valores do Enum
    /// </summary>
    protected void TipoCampo_OnLoad(object sender, EventArgs e)
    {
        if (sender is ASPxComboBox)
        {
            ASPxComboBox dropTipoCampo = (ASPxComboBox)sender;
            foreach (int r in Enum.GetValues(typeof(TipoDado)))
            {
                dropTipoCampo.Items.Add(Enum.GetName(typeof(TipoDado), r), r.ToString());
            }
        }
    }

    /// <summary>
    /// Carrega o Tipo De Para com os valores do Enum
    /// </summary>
    protected void enumTipoDePara_OnLoad(object sender, EventArgs e)
    {
        if (sender is ASPxComboBox)
        {
            ASPxComboBox dropEnumTipoDePara = (ASPxComboBox)sender;
            foreach (int r in Enum.GetValues(typeof(EnumTipoDePara)))
            {
                dropEnumTipoDePara.Items.Add(EnumTipoDeParaDescricao.RetornaStringValue(r), r.ToString());
            }

            if (!gridCadastro.IsNewRowEditing)
                (sender as ASPxComboBox).ClientEnabled = false;
        }
    }
}
