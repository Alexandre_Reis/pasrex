﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Collections.Generic;
using System.Text;
using System.Drawing;

using DevExpress.Web;

using Financial.Investidor;
using Financial.Fundo;
using Financial.Common;
using Financial.Util;
using Financial.Web.Common;
using EntitySpaces.Interfaces;
using Financial.Bolsa;
using Financial.BMF;
using Financial.RendaFixa;
using Financial.Security;
using Financial.Web.Util;
using Financial.Security.Enums;
using EntitySpaces.Core;

public partial class CadastrosBasicos_TabelaCDA : CadastroBasePage 
{
    enum TipoMercado {
        Bolsa = 1,
        BMF = 2,
        Fundo = 3,
        RendaFixa = 4
    }

    new protected void Page_Load(object sender, EventArgs e) {
        base.Page_Load(sender, e);

        UtilitarioGrid.ExportacaoPDFExcel(this.gridExport,
                         new List<string>(new string[] { "DS_TipoMercado" }));
    }

    #region Classe para Binding
    public class BindingDSCollection : AtivoBolsaCollection {

        /// <summary>
        /// Adiciona uma Coluna Nova na Collection
        /// </summary>
        /// <param name="columnName"></param>
        /// <param name="typeColumn"></param>
        public void AddColumn(string columnName, Type typeColumn) {
            if (this.Table != null && !this.Table.Columns.Contains(columnName)) {
                this.Table.Columns.Add(columnName, typeColumn);
            }
        }
       
        public void SetNull(string columnName) {
            this.Table.Columns[columnName].AllowDBNull = true;
        }

        public void DatatableToES(DataTable table) {
            this.PopulateCollection(table);            
        }

    }
    #endregion

    #region DataSources
    protected void EsDSCDA_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e) 
    {
        esUtility u = new esUtility();

        #region SQL
        StringBuilder sqlText = new StringBuilder();

        #region AtivoBolsa
        sqlText.AppendLine(" SELECT CdAtivoBolsa as DS_Codigo, Descricao as DS_Descricao, 'Ações / Opções' as DS_TipoMercado, CodigoCDA as DS_CodigoCDA, DataInicioVigencia as DS_DataInicioVigencia FROM AtivoBolsa ");
        #endregion

        #region AtivoBMF
        sqlText.AppendLine(" Union All ");
        sqlText.AppendLine(" SELECT (cdativobmf + serie) as DS_Codigo, (cdativobmf + serie) as DS_Descricao, 'Ativos BMF' as DS_TipoMercado, CodigoCDA as DS_CodigoCDA, DataInicioVigencia as DS_DataInicioVigencia FROM AtivoBMF ");
        #endregion

        #region Carteira
        sqlText.AppendLine(" Union All ");
        sqlText.AppendLine(" SELECT CONVERT(VARCHAR, idcarteira) as DS_Codigo, apelido as DS_Descricao, 'Cotas Investimento' as DS_TipoMercado, CodigoCDA as DS_CodigoCDA, null as DS_DataInicioVigencia FROM Carteira ");
        #endregion

        #region TituloRendaFixa
        sqlText.AppendLine(" Union All ");
        sqlText.AppendLine(" SELECT CONVERT(VARCHAR, idtitulo) as DS_Codigo, descricaocompleta as DS_Descricao, 'Renda Fixa' as DS_TipoMercado, CodigoCDA as DS_CodigoCDA, null as DS_DataInicioVigencia FROM TituloRendaFixa ");
        #endregion

        sqlText.AppendLine(" Order by DS_TipoMercado ASC, DS_Descricao ASC");

        #endregion
        //
        DataTable dt = u.FillDataTable(esQueryType.Text, sqlText.ToString());

        BindingDSCollection b = new BindingDSCollection();
        b.DatatableToES(dt);

        e.Collection = b;
    }
    #endregion

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_CustomUnboundColumnData(object sender, DevExpress.Web.ASPxGridViewColumnDataEventArgs e) 
    {
        if (e.Column.FieldName == "CompositeKey") {
            //Compose a primary key value
            string codigo = Convert.ToString(e.GetListSourceFieldValue("DS_Codigo"));
            string tipoMercado = Convert.ToString(e.GetListSourceFieldValue("DS_TipoMercado"));

            int tipoMercadoInt = 0;
            if (tipoMercado == "Ações / Opções") {
                tipoMercadoInt = Convert.ToInt32(TipoMercado.Bolsa);
            }
            else if (tipoMercado == "Ativos BMF") {
                tipoMercadoInt = Convert.ToInt32(TipoMercado.BMF);
            }
            else if (tipoMercado == "Cotas Investimento") {
                tipoMercadoInt = Convert.ToInt32(TipoMercado.Fundo);
            }
            else if (tipoMercado == "Renda Fixa") {
                tipoMercadoInt = Convert.ToInt32(TipoMercado.RendaFixa);
            }

            e.Value = codigo + "|" + tipoMercadoInt;
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_RowUpdating(object sender, DevExpress.Web.Data.ASPxDataUpdatingEventArgs e) {       
        string chave = (string)e.Keys[0];
        string codigo = Convert.ToString(chave.Split('|')[0]);
        int tipoMercado = Convert.ToInt32(chave.Split('|')[1]);

        if (tipoMercado == (int)TipoMercado.Bolsa) {
            #region Bolsa
            AtivoBolsa ativoBolsa = new AtivoBolsa();
            if (ativoBolsa.LoadByPrimaryKey(codigo)) {
                if (String.IsNullOrEmpty( Convert.ToString(e.NewValues["DS_CodigoCDA"]))) {
                    ativoBolsa.CodigoCDA = null;
                }
                else {
                    ativoBolsa.CodigoCDA = Convert.ToInt32(e.NewValues["DS_CodigoCDA"]);
                }

                try
                {
                    DateTime dataInicioVigencia = Convert.ToDateTime(e.NewValues["DS_DataInicioVigencia"]);
                    ativoBolsa.DataInicioVigencia = dataInicioVigencia;
                }
                catch (Exception)
                {
                    ativoBolsa.DataInicioVigencia = null;
                }
                
                ativoBolsa.Save();

                #region Log do Processo
                HistoricoLog historicoLog = new HistoricoLog();
                historicoLog.InsereHistoricoLog(DateTime.Now,
                                                DateTime.Now,
                                                "Cadastro de CodigoCDA - Operacao: Update CDA AtivoBolsa: " + codigo + "; " + UtilitarioWeb.ToString(ativoBolsa),
                                                HttpContext.Current.User.Identity.Name,
                                                UtilitarioWeb.GetIP(Request),
                                                "",
                                                HistoricoLogOrigem.Outros);
                #endregion
            }            
            #endregion
        }
        else if (tipoMercado == (int)TipoMercado.BMF) {
            #region BMF
            AtivoBMF ativoBMF = new AtivoBMF();
            string cdativoBMF = ativoBMF.RetornaCdAtivoSplitado(codigo);
            string serie = ativoBMF.RetornaSerieSplitada(codigo);

            if (ativoBMF.LoadByPrimaryKey(cdativoBMF, serie)) {
                if (String.IsNullOrEmpty( Convert.ToString(e.NewValues["DS_CodigoCDA"]))) {
                    ativoBMF.CodigoCDA = null;
                }
                else {
                    ativoBMF.CodigoCDA = Convert.ToInt32(e.NewValues["DS_CodigoCDA"]);
                }

                try
                {
                    DateTime dataInicioVigencia = Convert.ToDateTime(e.NewValues["DS_DataInicioVigencia"]);
                    ativoBMF.DataInicioVigencia = dataInicioVigencia;
                }
                catch (Exception)
                {
                    ativoBMF.DataInicioVigencia = null;
                }

                ativoBMF.Save();

                #region Log do Processo
                HistoricoLog historicoLog = new HistoricoLog();
                historicoLog.InsereHistoricoLog(DateTime.Now,
                                                DateTime.Now,
                                                "Cadastro de CodigoCDA - Operacao: Update CDA AtivoBMF: " + cdativoBMF + "; " + serie + UtilitarioWeb.ToString(ativoBMF),
                                                HttpContext.Current.User.Identity.Name,
                                                UtilitarioWeb.GetIP(Request),
                                                "",
                                                HistoricoLogOrigem.Outros);
                #endregion
            }            
            #endregion
        }
        else if (tipoMercado == (int)TipoMercado.Fundo) {
            #region Fundo
            Carteira carteira = new Carteira();
            if (carteira.LoadByPrimaryKey(Convert.ToInt32(codigo))) {
                if (String.IsNullOrEmpty(Convert.ToString(e.NewValues["DS_CodigoCDA"]))) {
                    carteira.CodigoCDA = null;
                }
                else {
                    carteira.CodigoCDA = Convert.ToInt32(e.NewValues["DS_CodigoCDA"]);
                }
                carteira.Save();

                #region Log do Processo
                HistoricoLog historicoLog = new HistoricoLog();
                historicoLog.InsereHistoricoLog(DateTime.Now,
                                                DateTime.Now,
                                                "Cadastro de CodigoCDA - Operacao: Update CDA Carteira: " + codigo + "; " + UtilitarioWeb.ToString(carteira),
                                                HttpContext.Current.User.Identity.Name,
                                                UtilitarioWeb.GetIP(Request),
                                                "",
                                                HistoricoLogOrigem.Outros);
                #endregion
            }
            #endregion
        }
        else if (tipoMercado == (int)TipoMercado.RendaFixa) {
            #region RendaFixa
            TituloRendaFixa titulo = new TituloRendaFixa();
            if (titulo.LoadByPrimaryKey(Convert.ToInt32(codigo))) {
                if (String.IsNullOrEmpty(Convert.ToString(e.NewValues["DS_CodigoCDA"]))) {
                    titulo.CodigoCDA = null;
                }
                else {
                    titulo.CodigoCDA = Convert.ToInt32(e.NewValues["DS_CodigoCDA"]);
                }
                titulo.Save();

                #region Log do Processo
                HistoricoLog historicoLog = new HistoricoLog();
                historicoLog.InsereHistoricoLog(DateTime.Now,
                                                DateTime.Now,
                                                "Cadastro de CodigoCDA - Operacao: Update CDA Titulo Renda Fixa: " + codigo + "; " + UtilitarioWeb.ToString(titulo),
                                                HttpContext.Current.User.Identity.Name,
                                                UtilitarioWeb.GetIP(Request),
                                                "",
                                                HistoricoLogOrigem.Outros);
                #endregion
            }
            #endregion
        }
       
        e.Cancel = true;
        gridCadastro.CancelEdit();
        //gridCadastro.DataBind();
    }

    /// <summary>
    /// Cor cinza nos campos Disables
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    new protected void gridCadastro_CellEditorInitialize(object sender, ASPxGridViewEditorEventArgs e) 
    {
        base.gridCadastro_CellEditorInitialize(sender, e);
        
        if (e.Column.FieldName == "DS_Codigo" || e.Column.FieldName == "DS_Descricao" || e.Column.FieldName == "DS_TipoMercado") {

            (e.Editor as ASPxTextEdit).ValidationSettings.RequiredField.IsRequired = false;
            (e.Editor as ASPxTextEdit).ValidationSettings.RequiredField.ErrorText = "";
            //
            e.Editor.BackColor = Color.FromName("#EBEBEB");
            e.Editor.ReadOnly = true;
        }
        else if (e.Column.FieldName == "DS_CodigoCDA")
        {
            (e.Editor as ASPxSpinEdit).ValidationSettings.RequiredField.IsRequired = false;
            (e.Editor as ASPxSpinEdit).ValidationSettings.RequiredField.ErrorText = "";
        }
        else if (e.Column.FieldName == "DS_DataInicioVigencia")
        {
            (e.Editor as ASPxDateEdit).ValidationSettings.RequiredField.IsRequired = false;
            (e.Editor as ASPxDateEdit).ValidationSettings.RequiredField.ErrorText = "";
        }

    }
}