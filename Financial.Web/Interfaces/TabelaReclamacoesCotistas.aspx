﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="TabelaReclamacoesCotistas.aspx.cs" Inherits="CadastrosBasicos_TabelaReclamacoesCotistas" %>

<%@ Register Assembly="System.Web, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"
    Namespace="System.Web.UI.HtmlControls" TagPrefix="cc2" %>
<%@ Register Assembly="EntitySpaces.Web" Namespace="EntitySpaces.Web" TagPrefix="cc1" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxe" %>    
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxcb" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxwgv" %>

<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxpc" %>    

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    
    <link href="~/css/forms.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" language="Javascript" src="~/js/global.js"></script>    
    <script type="text/javascript" language="Javascript">
    document.onkeydown=onDocumentKeyDown;    
    </script>
</head>

<body>
    <form id="form1" runat="server">
    
    <dxcb:ASPxCallback ID="callbackErro" runat="server" OnCallback="callbackErro_Callback">
        <ClientSideEvents CallbackComplete="function(s, e) {             
            if (e.result != '')
            {                   
                alert(e.result);                              
            }
            else
            {
                gridCadastro.UpdateEdit();                
            }            
        }        
        "/>
    </dxcb:ASPxCallback>
            
    <div class="divPanel">
    <table width="100%"><tr><td>
    <div id="container">
    
    <div id="header">
        <asp:Label ID="lblHeader" runat="server" Text="Tabela de Reclamações de Cotistas (Clubes de Investimentos)"></asp:Label>
    </div>
        
    <div id="mainContent">
            
            <div class="linkButton" >               
               <asp:LinkButton ID="btnAdd" runat="server" Font-Overline="false" ValidationGroup="ATK" CssClass="btnAdd" OnClientClick="gridCadastro.AddNewRow(); return false;"><asp:Literal ID="Literal1" runat="server" Text="Novo"/><div></div></asp:LinkButton>
               <asp:LinkButton ID="btnDelete" runat="server" Font-Overline="false" ValidationGroup="ATK" CssClass="btnDelete" OnClientClick=" if (confirm('Tem certeza que quer excluir?')==true) gridCadastro.PerformCallback('btnDelete');return false;"><asp:Literal ID="Literal2" runat="server" Text="Excluir"/><div></div></asp:LinkButton>
               <asp:LinkButton ID="btnPDF" runat="server" Font-Overline="false" ValidationGroup="ATK" CssClass="btnPdf" OnClick="btnPDF_Click"><asp:Literal ID="Literal4" runat="server" Text="Gerar PDF"/><div></div></asp:LinkButton>
               <asp:LinkButton ID="btnExcel" runat="server" Font-Overline="false" ValidationGroup="ATK" CssClass="btnExcel" OnClick="btnExcel_Click"><asp:Literal ID="Literal5" runat="server" Text="Gerar Excel"/><div></div></asp:LinkButton>              
               <asp:LinkButton ID="btnRefresh" runat="server" Font-Overline="false" CssClass="btnRefresh" OnClientClick=" gridCadastro.PerformCallback('btnRefresh'); return false;"><asp:Literal ID="Literal6" runat="server" Text="Atualizar"/><div></div></asp:LinkButton>
            </div>
    
            <div class="divDataGrid">            
                <dxwgv:ASPxGridView ID="gridCadastro" runat="server" EnableCallBacks="true"
                            KeyFieldName="Data" DataSourceID="EsDSTabela"
                            OnRowUpdating="gridCadastro_RowUpdating"
                            OnRowInserting="gridCadastro_RowInserting"
                            OnCustomCallback="gridCadastro_CustomCallback"
                            OnCustomJSProperties="gridCadastro_CustomJSProperties"
                             >        
                                
                    <Columns>           
                        <dxwgv:GridViewCommandColumn ShowSelectCheckbox="True" VisibleIndex="0" Width="13%" ButtonType="Image" ShowClearFilterButton="True">
                            <HeaderTemplate>
                                <dxe:ASPxCheckBox ID="cbAll" ClientInstanceName="cbAll" runat="server" ToolTip="Seleciona todos os CheckBoxs de todas as páginas" BackColor="white" ClientSideEvents-CheckedChanged="function(s, e) {OnAllCheckedChanged(s, e, gridCadastro);}" OnInit="CheckBoxSelectAll"/>
                            </HeaderTemplate>
                        </dxwgv:GridViewCommandColumn>
                                                        
                        <dxwgv:GridViewDataDateColumn FieldName="Data" Caption="Data" VisibleIndex="1" Width="10%"/>
                        <dxwgv:GridViewDataSpinEditColumn FieldName="Procedentes" Caption="Proced." VisibleIndex="3" Width="6%"/>
                        <dxwgv:GridViewDataSpinEditColumn FieldName="Improcedentes" Caption="Improc." VisibleIndex="4" Width="6%"/>
                        <dxwgv:GridViewDataSpinEditColumn FieldName="Resolvidas" Caption="Resolv." VisibleIndex="5" Width="6%"/>
                        <dxwgv:GridViewDataSpinEditColumn FieldName="NaoResolvidas" Caption="Ñ Resolv." VisibleIndex="6" Width="6%"/>
                        <dxwgv:GridViewDataSpinEditColumn FieldName="Administracao" Caption="Adm" VisibleIndex="7" Width="6%"/>
                        <dxwgv:GridViewDataSpinEditColumn FieldName="Gestao" Caption="Gestão" VisibleIndex="8" Width="6%"/>
                        <dxwgv:GridViewDataSpinEditColumn FieldName="Taxas" Caption="Taxas" VisibleIndex="9" Width="6%"/>
                        <dxwgv:GridViewDataSpinEditColumn FieldName="PoliticaInvestimento" Caption="Pol.Invest." VisibleIndex="10" Width="6%"/>
                        <dxwgv:GridViewDataSpinEditColumn FieldName="Aportes" Caption="Aportes" VisibleIndex="11" Width="6%"/>
                        <dxwgv:GridViewDataSpinEditColumn FieldName="Resgates" Caption="Resgates" VisibleIndex="12" Width="6%"/>
                        <dxwgv:GridViewDataSpinEditColumn FieldName="FornecimentoInformacoes" Caption="Infos" VisibleIndex="13" Width="6%"/>
                        <dxwgv:GridViewDataSpinEditColumn FieldName="AssembleiasGerais" Caption="Assemb." VisibleIndex="14" Width="6%"/>
                        <dxwgv:GridViewDataSpinEditColumn FieldName="Cotas" Caption="Cotas" VisibleIndex="15" Width="6%"/>
                        <dxwgv:GridViewDataSpinEditColumn FieldName="Outros" Caption="Outros" VisibleIndex="16" Width="6%"/>                        
                    </Columns>
                    
                    <Templates>
                        <EditForm>
                                                    
                            <div class="editForm">        
                                 
                                 <table>                    
                                    <tr>                                        
                                        <td class="td_Label_Curto">
                                            <asp:Label ID="labelData" runat="server" CssClass="labelRequired" Text="Data:"></asp:Label>
                                        </td>
                                                                
                                        <td>             
                                            <dxe:ASPxDateEdit ID="textData" runat="server" ClientInstanceName="textData" Value='<%#Eval("Data")%>' OnInit="textData_Init"/>
                                        </td>  
                                    </tr>
                                        
                                    <tr>
                                        <td class="td_Label_Curto">
                                            <asp:Label ID="labelProcedentes" runat="server" CssClass="labelNormal" Text="Procedentes:"></asp:Label>
                                        </td>
                                        <td>
                                            <dxe:ASPxSpinEdit ID="textProcedentes" runat="server" CssClass="textCurto" ClientInstanceName="textProcedentes"
                                    		      MaxLength="6" NumberType="integer" Text="<%#Bind('Procedentes')%>">            
                                            </dxe:ASPxSpinEdit>                                        
                                        </td>
                                        
                                        <td class="td_Label_Curto">
                                            <asp:Label ID="label1" runat="server" CssClass="labelNormal" Text="Improcedentes:"></asp:Label>
                                        </td>
                                        <td>
                                            <dxe:ASPxSpinEdit ID="textImprocedentes" runat="server" CssClass="textCurto" ClientInstanceName="textImprocedentes"
                                    		      MaxLength="6" NumberType="integer" Text="<%#Bind('Improcedentes')%>">            
                                            </dxe:ASPxSpinEdit>                                        
                                        </td>
                                    </tr>
                                    
                                    <tr>
                                        <td class="td_Label_Curto">
                                            <asp:Label ID="label2" runat="server" CssClass="labelNormal" Text="Resolvidas:"></asp:Label>
                                        </td>
                                        <td>
                                            <dxe:ASPxSpinEdit ID="textResolvidas" runat="server" CssClass="textCurto" ClientInstanceName="textResolvidas"
                                    		      MaxLength="6" NumberType="integer" Text="<%#Bind('Resolvidas')%>">            
                                            </dxe:ASPxSpinEdit>                                        
                                        </td>
                                        
                                        <td class="td_Label_Curto">
                                            <asp:Label ID="label3" runat="server" CssClass="labelNormal" Text="Não Resolvidas:"></asp:Label>
                                        </td>
                                        <td>
                                            <dxe:ASPxSpinEdit ID="textNaoResolvidas" runat="server" CssClass="textCurto" ClientInstanceName="textNaoResolvidas"
                                    		      MaxLength="6" NumberType="integer" Text="<%#Bind('NaoResolvidas')%>">            
                                            </dxe:ASPxSpinEdit>                                        
                                        </td>
                                    </tr>
                                    
                                    <tr>
                                        <td class="td_Label_Curto">
                                            <asp:Label ID="label4" runat="server" CssClass="labelNormal" Text="Administração:"></asp:Label>
                                        </td>
                                        <td>
                                            <dxe:ASPxSpinEdit ID="textAdministracao" runat="server" CssClass="textCurto" ClientInstanceName="textAdministracao"
                                    		      MaxLength="6" NumberType="integer" Text="<%#Bind('Administracao')%>">            
                                            </dxe:ASPxSpinEdit>                                        
                                        </td>
                                        
                                        <td class="td_Label_Curto">
                                            <asp:Label ID="label5" runat="server" CssClass="labelNormal" Text="Gestão:"></asp:Label>
                                        </td>
                                        <td>
                                            <dxe:ASPxSpinEdit ID="textGestao" runat="server" CssClass="textCurto" ClientInstanceName="textGestao"
                                    		      MaxLength="6" NumberType="integer" Text="<%#Bind('Gestao')%>">            
                                            </dxe:ASPxSpinEdit>                                        
                                        </td>
                                    </tr>
                                    
                                    <tr>
                                        <td class="td_Label_Curto">
                                            <asp:Label ID="label7" runat="server" CssClass="labelNormal" Text="Taxas:"></asp:Label>
                                        </td>
                                        <td>
                                            <dxe:ASPxSpinEdit ID="textTaxas" runat="server" CssClass="textCurto" ClientInstanceName="textTaxas"
                                    		      MaxLength="6" NumberType="integer" Text="<%#Bind('Taxas')%>">
                                            </dxe:ASPxSpinEdit>                                        
                                        </td>
                                        
                                        <td class="td_Label_Curto">
                                            <asp:Label ID="label6" runat="server" CssClass="labelNormal" Text="Política Investimento:"></asp:Label>
                                        </td>
                                        <td>
                                            <dxe:ASPxSpinEdit ID="textPoliticaInv" runat="server" CssClass="textCurto" ClientInstanceName="textPoliticaInv"
                                    		      MaxLength="6" NumberType="integer" Text="<%#Bind('PoliticaInvestimento')%>">            
                                            </dxe:ASPxSpinEdit>                                        
                                        </td>
                                    </tr>
                                    
                                    <tr>
                                        <td class="td_Label_Curto">
                                            <asp:Label ID="label8" runat="server" CssClass="labelNormal" Text="Aportes:"></asp:Label>
                                        </td>
                                        <td>
                                            <dxe:ASPxSpinEdit ID="textAportes" runat="server" CssClass="textCurto" ClientInstanceName="textAportes"
                                    		      MaxLength="6" NumberType="integer" Text="<%#Bind('Aportes')%>">
                                            </dxe:ASPxSpinEdit>                                        
                                        </td>
                                        
                                        <td class="td_Label_Curto">
                                            <asp:Label ID="label9" runat="server" CssClass="labelNormal" Text="Resgates:"></asp:Label>
                                        </td>
                                        <td>
                                            <dxe:ASPxSpinEdit ID="textResgates" runat="server" CssClass="textCurto" ClientInstanceName="textResgates"
                                    		      MaxLength="6" NumberType="integer" Text="<%#Bind('Resgates')%>">            
                                            </dxe:ASPxSpinEdit>                                        
                                        </td>
                                    </tr>
                                    
                                    <tr>
                                        <td class="td_Label_Curto">
                                            <asp:Label ID="label10" runat="server" CssClass="labelNormal" Text="Fornecimento infos:"></asp:Label>
                                        </td>
                                        <td>
                                            <dxe:ASPxSpinEdit ID="textFornecimentoInfos" runat="server" CssClass="textCurto" ClientInstanceName="textFornecimentoInfos"
                                    		      MaxLength="6" NumberType="integer" Text="<%#Bind('FornecimentoInformacoes')%>">
                                            </dxe:ASPxSpinEdit>                                        
                                        </td>
                                        
                                        <td class="td_Label_Curto">
                                            <asp:Label ID="label11" runat="server" CssClass="labelNormal" Text="Assembléias Gerais:"></asp:Label>
                                        </td>
                                        <td>
                                            <dxe:ASPxSpinEdit ID="textAssembleiasGerais" runat="server" CssClass="textCurto" ClientInstanceName="textAssembleiasGerais"
                                    		      MaxLength="6" NumberType="integer" Text="<%#Bind('AssembleiasGerais')%>">            
                                            </dxe:ASPxSpinEdit>                                        
                                        </td>
                                    </tr>
                                    
                                    <tr>
                                        <td class="td_Label_Curto">
                                            <asp:Label ID="label12" runat="server" CssClass="labelNormal" Text="Cotas:"></asp:Label>
                                        </td>
                                        <td>
                                            <dxe:ASPxSpinEdit ID="textCotas" runat="server" CssClass="textCurto" ClientInstanceName="textCotas"
                                    		      MaxLength="6" NumberType="integer" Text="<%#Bind('Cotas')%>">
                                            </dxe:ASPxSpinEdit>                                        
                                        </td>
                                        
                                        <td class="td_Label_Curto">
                                            <asp:Label ID="label13" runat="server" CssClass="labelNormal" Text="Outros:"></asp:Label>
                                        </td>
                                        <td>
                                            <dxe:ASPxSpinEdit ID="textOutros" runat="server" CssClass="textCurto" ClientInstanceName="textOutros"
                                    		      MaxLength="6" NumberType="integer" Text="<%#Bind('Outros')%>">            
                                            </dxe:ASPxSpinEdit>                                        
                                        </td>
                                    </tr>
                                </table>
                                
                                <div class="linhaH"></div>
                        
                                <div class="linkButton linkButtonNoBorder popupFooter">
                                    <asp:LinkButton ID="btnOK" runat="server" Font-Overline="false" CssClass="btnOK"
                                                            OnClientClick="callbackErro.SendCallback(); return false;"><asp:Literal ID="Literal3" runat="server" Text="OK"/><div></div></asp:LinkButton>
                                    <asp:LinkButton ID="btnCancel" runat="server" Font-Overline="false" CssClass="btnCancel" OnClientClick="gridCadastro.CancelEdit(); return false;"><asp:Literal ID="Literal9" runat="server" Text="Cancelar"/><div></div></asp:LinkButton>
                                </div>    
                            </div>
                        </EditForm>                        
                    </Templates>
                    
                    <SettingsPopup EditForm-Width="250px" />
                    <SettingsCommandButton>
                        <ClearFilterButton Image-Url="../imagens/funnel--minus.png"/>
                    </SettingsCommandButton>                                        
                                
                </dxwgv:ASPxGridView>            
            </div>                          
    </div>
    </div>
    </td></tr></table>
    </div>
                       
    <dxwgv:ASPxGridViewExporter ID="gridExport" runat="server" GridViewID="gridCadastro" />
        
    <cc1:esDataSource ID="EsDSTabela" runat="server" OnesSelect="EsDSTabela_esSelect" />
        
    </form>
</body>
</html>