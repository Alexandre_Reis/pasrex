﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Financial.Web.Common;
using Financial.Common;
using DevExpress.Web;
using System.Collections.Generic;
using Financial.Web.Util;
using Financial.Security.Enums;
using Financial.Security;
using Financial.Util;
using EntitySpaces.Interfaces;
using Financial.Common.Enums;
using System.Data.SqlClient;
using System.Globalization;

public partial class Configuracao_CadastroDePara : CadastroBasePage
{
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void EsDSDePara_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        DeParaCollection coll = new DeParaCollection();

        coll.Query.OrderBy(coll.Query.IdDePara.Ascending);
        coll.LoadAll();

        //Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_RowUpdating(object sender, DevExpress.Web.Data.ASPxDataUpdatingEventArgs e)
    {
        DePara dePara = new DePara();

        int IdDePara = (int)e.Keys[0];

        if (dePara.LoadByPrimaryKey(IdDePara))
        {
            ASPxComboBox dropTipoDePara = gridCadastro.FindEditFormTemplateControl("dropTipoDePara") as ASPxComboBox;
            ASPxTextBox textCodigoInterno = gridCadastro.FindEditFormTemplateControl("textCodigoInterno") as ASPxTextBox;
            ASPxTextBox textCodigoExterno = gridCadastro.FindEditFormTemplateControl("textCodigoExterno") as ASPxTextBox;
            
            TipoDePara tipoDePara = new TipoDePara();
            tipoDePara.LoadByPrimaryKey(Convert.ToInt32(dropTipoDePara.SelectedItem.Value.ToString()));

            int TipoCampoColl = tipoDePara.TipoCampo.Value;

            dePara.IdTipoDePara = Convert.ToInt16(dropTipoDePara.SelectedItem.Value.ToString());

            //Verifica o tipo de campo retornado
            if (TipoCampoColl.Equals((int)TipoDado.Inteiro))
            {
                int codigoInterno = 0;
                int codigoExterno = 0;

                if (int.TryParse(textCodigoInterno.Text, out codigoInterno))
                {
                    dePara.CodigoInterno = codigoInterno.ToString();
                }
                else
                {
                    throw new Exception("Campo: Origem deve ser um tipo inteiro");
                }

                if (int.TryParse(textCodigoExterno.Text, out codigoExterno))
                {
                    dePara.CodigoExterno = codigoExterno.ToString();
                }
                else
                {
                    throw new Exception("Campo: Destino deve ser um tipo inteiro");
                }
            }
            else if (TipoCampoColl.Equals((int)TipoDado.Data))
            {
                DateTime codigoInterno = new DateTime();
                DateTime codigoExterno = new DateTime();

                if (DateTime.TryParse(textCodigoInterno.Text,
                                      CultureInfo.CreateSpecificCulture("pt-BR"),
                                      DateTimeStyles.None,
                                      out codigoInterno))
                {
                    dePara.CodigoInterno = codigoInterno.ToString("MM/dd/yyyy");
                }
                else
                {
                    throw new Exception("Campo: Origem deve ser uma Data e deve estar no formato dd/MM/aaaa");
                }
                if (DateTime.TryParse(textCodigoExterno.Text,
                                      CultureInfo.CreateSpecificCulture("pt-BR"),
                                      DateTimeStyles.None,
                                      out codigoExterno))
                {
                    dePara.CodigoExterno = codigoExterno.ToString("MM/dd/yyyy");
                }
                else
                {
                    throw new Exception("Campo: Destino deve ser uma Data e deve estar no formato dd/MM/aaaa");
                }
            }
            else
            {
                if (!string.IsNullOrEmpty(textCodigoInterno.Text))
                {
            dePara.CodigoInterno = textCodigoInterno.Text;
                }
                else
                {
                    throw new Exception("Campo: Origem deve ser preenchido");
                }

                if (!string.IsNullOrEmpty(textCodigoExterno.Text))
                {
                    dePara.CodigoExterno = textCodigoExterno.Text;
                }
                else
                {
                    throw new Exception("Campo: Destino deve ser preenchido");
                }
            }

            dePara.Save();

            #region Log do Processo
            HistoricoLog historicoLog = new HistoricoLog();
            historicoLog.InsereHistoricoLog(DateTime.Now,
                                            DateTime.Now,
                                            "Cadastro de De Para - Operacao: Update De Para: " + IdDePara + UtilitarioWeb.ToString(dePara),
                                            HttpContext.Current.User.Identity.Name,
                                            UtilitarioWeb.GetIP(Request),
                                            "",
                                            HistoricoLogOrigem.Outros);
            #endregion
        }
        
        e.Cancel = true;
        gridCadastro.CancelEdit();
        gridCadastro.DataBind();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_RowInserting(object sender, DevExpress.Web.Data.ASPxDataInsertingEventArgs e)
    {
        SalvarNovo();

        e.Cancel = true;
        gridCadastro.CancelEdit();
        gridCadastro.DataBind();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_CustomCallback(object sender, DevExpress.Web.ASPxGridViewCustomCallbackEventArgs e)
    {
        DePara deParaClone;
        DePara dePara;

        if (e.Parameters == "btnDelete")
        {
            List<object> keyValuesId = gridCadastro.GetSelectedFieldValues(DeParaMetadata.ColumnNames.IdDePara);
            for (int i = 0; i < keyValuesId.Count; i++)
            {
                int idDePara = Convert.ToInt32(keyValuesId[i]);

                dePara = new DePara();
                if (dePara.LoadByPrimaryKey(idDePara))
                {
                    deParaClone = (DePara)Utilitario.Clone(dePara);
                    dePara = new DePara();
                    dePara.Query.Where(dePara.Query.IdDePara == idDePara);
                    dePara.Query.Load();

                    dePara.MarkAsDeleted();
                    dePara.Save();

                    #region Log do Processo
                    HistoricoLog historicoLog = new HistoricoLog();
                    historicoLog.InsereHistoricoLog(DateTime.Now,
                                                    DateTime.Now,
                                                    "Cadastro de De Para - Operacao: Delete De Para: " + idDePara + UtilitarioWeb.ToString(deParaClone),
                                                    HttpContext.Current.User.Identity.Name,
                                                    UtilitarioWeb.GetIP(Request),
                                                    "",
                                                    HistoricoLogOrigem.Outros);
                    #endregion
                }
            }
        }


        gridCadastro.DataBind();
        gridCadastro.Selection.UnselectAll();
        gridCadastro.CancelEdit();
    }

    protected void btnOKAdd_Init(object sender, EventArgs e)
    {
        if (!gridCadastro.IsNewRowEditing)
            (sender as LinkButton).Visible = false;
    }

    /// <summary>
    /// Verifica quais campos possuem preenchimento obrigatório
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void callbackErro_Callback(object source, DevExpress.Web.CallbackEventArgs e)
    {
        e.Result = "";

        ASPxComboBox dropTipoDePara = gridCadastro.FindEditFormTemplateControl("dropTipoDePara") as ASPxComboBox;
        ASPxTextBox textCodigoInterno = gridCadastro.FindEditFormTemplateControl("textCodigoInterno") as ASPxTextBox;
        ASPxTextBox textCodigoExterno = gridCadastro.FindEditFormTemplateControl("textCodigoExterno") as ASPxTextBox;

        #region Campos obrigatórios
        List<Control> controles = new List<Control>();
        controles.Add(dropTipoDePara);
        controles.Add(textCodigoInterno);
        controles.Add(textCodigoExterno);

        if (base.TestaObrigatorio(controles) != "")
        {
            e.Result = "Campos com * são obrigatórios!";
            return;
        }
        #endregion
    }

    protected void callbackAdd_Callback(object source, DevExpress.Web.CallbackEventArgs e)
    {
        SalvarNovo();

        e.Result = "";
        if (e.Parameter == "clone")
        {
            //e.Result = 0;
        }
        else
        {
            e.Result = "Cadastro feito com sucesso.";
        }
    }

    /// <summary>
    /// Busca os dados preenchidos do formulario e preenche o objeto para ser salvo no banco de dados
    /// </summary>
    private void SalvarNovo()
    {
        //Instãncia
        DePara dePara = new DePara();
        TipoDeParaCollection coll = new TipoDeParaCollection();
        int TipoCampoColl = 0;
        
        //Busca controles do DevExpress na tela
        ASPxComboBox dropTipoDePara = gridCadastro.FindEditFormTemplateControl("dropTipoDePara") as ASPxComboBox;
        ASPxTextBox textCodigoInterno = gridCadastro.FindEditFormTemplateControl("textCodigoInterno") as ASPxTextBox;
        ASPxTextBox textCodigoExterno = gridCadastro.FindEditFormTemplateControl("textCodigoExterno") as ASPxTextBox;

        dePara.IdTipoDePara = Convert.ToInt16(dropTipoDePara.SelectedItem.Value.ToString());

        //Busca o tipo de campo selecionado
        coll.Query.Select(coll.Query.TipoCampo).Where(coll.Query.EnumTipoDePara == dePara.IdTipoDePara);
        
        //Verifica se a query retornou valor
        if (coll.Query.Load())
            TipoCampoColl = coll[0].TipoCampo.Value;

        //Verifica o tipo de campo retornado
        if (TipoCampoColl.Equals((int)TipoDado.Inteiro))
        {
            int codigoInterno = 0;
            int codigoExterno = 0;

            if (int.TryParse(textCodigoInterno.Text, out codigoInterno))
            {
                dePara.CodigoInterno = codigoInterno.ToString();
            }
            else
            {
                throw new Exception("Campo: Origem deve ser um tipo inteiro");
            }

            if (int.TryParse(textCodigoExterno.Text, out codigoExterno))
            {
                dePara.CodigoExterno = codigoExterno.ToString();
            }
            else
            {
                throw new Exception("Campo: Destino deve ser um tipo inteiro");
            }
        }
        else if (TipoCampoColl.Equals((int)TipoDado.Data))
        {
            DateTime codigoInterno = new DateTime();
            DateTime codigoExterno = new DateTime();

            if (DateTime.TryParse(textCodigoInterno.Text,
								  CultureInfo.CreateSpecificCulture("pt-BR"),
                                  DateTimeStyles.None, 
								  out codigoInterno))
            {
                dePara.CodigoInterno = codigoInterno.ToString("MM/dd/yyyy");
            }
            else
            {
                throw new Exception("Campo: Origem deve ser uma Data e deve estar no formato dd/MM/aaaa");
            }
            if (DateTime.TryParse(textCodigoExterno.Text,
                                  CultureInfo.CreateSpecificCulture("pt-BR"),
                                  DateTimeStyles.None,
                                  out codigoExterno))
            {
                dePara.CodigoExterno = codigoExterno.ToString("MM/dd/yyyy");
            }
            else
            {
                throw new Exception("Campo: Destino deve ser uma Data e deve estar no formato dd/MM/aaaa");
            }
        }
        else
        {
            if (!string.IsNullOrEmpty(textCodigoInterno.Text))
            {
                dePara.CodigoInterno = textCodigoInterno.Text;
            }
            else
            {
                throw new Exception("Campo: Origem deve ser preenchido");
            }

            if (!string.IsNullOrEmpty(textCodigoExterno.Text))
            {
                dePara.CodigoExterno = textCodigoExterno.Text;
            }
            else
            {
                throw new Exception("Campo: Destino deve ser preenchido");
            }
        }
            
        //Salva as informações na base de dados
        dePara.Save();

        #region Log do Processo
        HistoricoLog historicoLog = new HistoricoLog();
        historicoLog.InsereHistoricoLog(DateTime.Now,
                                        DateTime.Now,
                                        "Cadastro de De Para - Operacao: Insert De Para: " + dePara.IdDePara + UtilitarioWeb.ToString(dePara),
                                        HttpContext.Current.User.Identity.Name,
                                        UtilitarioWeb.GetIP(Request),
                                        "",
                                        HistoricoLogOrigem.Outros);
        #endregion
    }

    protected void EsDSTipoDePara_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        TipoDeParaCollection coll = new TipoDeParaCollection();
        coll.Query.OrderBy(coll.Query.IdTipoDePara.Ascending);
        coll.LoadAll();

        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }
}
