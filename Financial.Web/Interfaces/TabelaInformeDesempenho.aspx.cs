﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Web.UI.HtmlControls;
using Financial.Common;
using System.Collections.Generic;
using Financial.Util;
using System.Text;
using DevExpress.Web;
using System.Drawing;
using Financial.CRM;
using Financial.Security;
using Financial.Fundo;
using Financial.Investidor;
using Financial.Web.Util;
using Financial.Security.Enums;
using Financial.Investidor.Enums;
using EntitySpaces.Interfaces;

public partial class CadastrosBasicos_TabelaInformeDesempenho : Financial.Web.Common.CadastroBasePage {
    new protected void Page_Load(object sender, EventArgs e) {
        this.HasPopupCarteira = true; // aba1
        this.HasPopupCarteira1 = true; // aba2
        base.Page_Load(sender, e);
    }

    #region DataSources
    protected void EsDSTabelaInformeDesempenho_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e) {
        Usuario usuario = new Usuario();
        usuario.BuscaUsuario(Context.User.Identity.Name);
        int idUsuario = usuario.IdUsuario.Value;

        TabelaInformeDesempenhoQuery tabelaInformeDesempenhoQuery = new TabelaInformeDesempenhoQuery("T");
        CarteiraQuery carteiraQuery = new CarteiraQuery("C");
        ClienteQuery clienteQuery = new ClienteQuery("Q");
        PermissaoClienteQuery permissaoClienteQuery = new PermissaoClienteQuery("P");

        tabelaInformeDesempenhoQuery.Select(tabelaInformeDesempenhoQuery, carteiraQuery.Apelido, carteiraQuery.IdCategoria);
        tabelaInformeDesempenhoQuery.InnerJoin(carteiraQuery).On(tabelaInformeDesempenhoQuery.IdCarteira == carteiraQuery.IdCarteira);
        tabelaInformeDesempenhoQuery.InnerJoin(clienteQuery).On(clienteQuery.IdCliente == tabelaInformeDesempenhoQuery.IdCarteira);
        tabelaInformeDesempenhoQuery.InnerJoin(permissaoClienteQuery).On(permissaoClienteQuery.IdCliente == clienteQuery.IdCliente);
        tabelaInformeDesempenhoQuery.Where(permissaoClienteQuery.IdUsuario.Equal(idUsuario));

        tabelaInformeDesempenhoQuery.OrderBy(tabelaInformeDesempenhoQuery.Data.Descending);

        TabelaInformeDesempenhoCollection coll = new TabelaInformeDesempenhoCollection();
        coll.Load(tabelaInformeDesempenhoQuery);

        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }

    protected void EsDSCarteira_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e) {
        CarteiraCollection coll = new CarteiraCollection();
        //coll.BuscaCarteirasComAcessoTodas(Context.User.Identity.Name);
        coll.BuscaCarteirasFundosClubes();


        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }

    protected void EsDSCategoriaFundo_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e) {
        CategoriaFundoCollection coll = new CategoriaFundoCollection();
        coll.Query.OrderBy(coll.Query.Descricao.Ascending);
        coll.LoadAll();

        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }

    protected void EsDSTabelaInformeDesempenhoClonar_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e) {
        TabelaInformeDesempenhoCollection coll = new TabelaInformeDesempenhoCollection();

        TabelaInformeDesempenhoQuery info = new TabelaInformeDesempenhoQuery("e");
        CarteiraQuery c = new CarteiraQuery("C");

        info.Select(info.IdCarteira, info.Data, c.Nome, c.Nome.As("DataInicioVigenciaString"),
                   (info.IdCarteira.Cast(esCastType.String) + "-" + info.Data.Cast(esCastType.String)).As("CompositeKey")
            );
        info.InnerJoin(c).On(info.IdCarteira == c.IdCarteira);
        //
        info.OrderBy(info.IdCarteira.Ascending, info.Data.Descending);

        coll.Load(info);

        for (int i = 0; i < coll.Count; i++) {
            DateTime dataVigencia = Convert.ToDateTime(coll[i].GetColumn(TabelaInformeDesempenhoMetadata.ColumnNames.Data));
            //
            //DateTime novaData = new DateTime(dataVigencia.Year, dataVigencia.Month, dataVigencia.Day);

            coll[i].SetColumn("DataInicioVigenciaString", dataVigencia.ToString("d"));
        }

        //
        e.Collection = coll;
    }

    protected void EsDSCarteiraClonar_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e) {
        CarteiraCollection coll = new CarteiraCollection();
        coll.BuscaCarteirasFundosClubes();
        //        
        e.Collection = coll;
    }
    #endregion

    /// <summary>
    /// Faz Clonagem
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void callBackClonar_Callback(object source, DevExpress.Web.CallbackEventArgs e) {

        #region Campos obrigatórios
        List<Control> controles = new List<Control>();
        controles.Add(dropInfo);
        controles.Add(dropCarteiraDestino);
        controles.Add(textDataClonar);

        if (base.TestaObrigatorio(controles) != "") {
            e.Result = "Campos com * são obrigatórios!";
            return;
        }
        #endregion

        #region Confere registro já existente
        TabelaInformeDesempenho infoVelha = new TabelaInformeDesempenho();
        infoVelha.LoadByPrimaryKey(Convert.ToInt32(dropCarteiraDestino.Text.Trim()), Convert.ToDateTime(textDataClonar.Value));

        if (infoVelha.es.HasData) {
            e.Result = "Registro Já existente";
            return;
        }
        #endregion

        string selecionado = dropInfo.Text.Trim();
        string[] selecionadoAux = selecionado.Split(new Char[] { '-' });
        int idCarteira = Convert.ToInt32(selecionadoAux[0].Trim());
        DateTime data = Convert.ToDateTime(selecionadoAux[2].Trim());
        //        

        //Testa se idCarteira é Clube ou Fundo
        Cliente cliente = new Cliente();
        cliente.LoadByPrimaryKey(idCarteira);

        if ( cliente.IdTipo.Value != (int)TipoClienteFixo.Clube && 
             cliente.IdTipo.Value != (int)TipoClienteFixo.Fundo ) {

            e.Result = "Carteira deve ser um Clube ou um Fundo.";
            return;
        }

        // Carrega Informação velha
        TabelaInformeDesempenho infoNova = new TabelaInformeDesempenho();
        infoNova.LoadByPrimaryKey(idCarteira, data);

        infoNova.MarkAllColumnsAsDirty(DataRowState.Added);

        // Acrescenta nova Carteira e Data
        infoNova.IdCarteira = Convert.ToInt32(dropCarteiraDestino.Text.Trim());
        infoNova.Data = Convert.ToDateTime(textDataClonar.Value);
        //
        infoNova.Save();

        //
        e.Result = "Processo executado com sucesso.";
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnEditCodigoCarteira_Init(object sender, EventArgs e) {
        if (!gridCadastro.IsNewRowEditing)
            (sender as ASPxSpinEdit).Enabled = false;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void textData_Init(object sender, EventArgs e) {
        if (!gridCadastro.IsNewRowEditing)
            (sender as ASPxDateEdit).Enabled = false;
    }

    protected void btnOKAdd_Init(object sender, EventArgs e) {
        if (!gridCadastro.IsNewRowEditing)
            (sender as LinkButton).Visible = false;
    }

    protected void callbackErro_Callback(object source, DevExpress.Web.CallbackEventArgs e) 
    {
        e.Result = "";

        ASPxPageControl pageControl = gridCadastro.FindEditFormTemplateControl("tabCadastro") as ASPxPageControl;
        //
        ASPxDateEdit textData = pageControl.FindControl("textData") as ASPxDateEdit;
        ASPxSpinEdit btnEditCodigoCarteira = pageControl.FindControl("btnEditCodigoCarteira") as ASPxSpinEdit;
        //
        ASPxDateEdit textDataFundos = pageControl.FindControl("textDataF") as ASPxDateEdit;
        ASPxSpinEdit btnEditCodigoCarteiraFundos = pageControl.FindControl("btnEditCodigoCarteiraF") as ASPxSpinEdit;
        //
        ASPxSpinEdit textValorAdmAdministrador = pageControl.FindControl("textValorAdmAdministrador") as ASPxSpinEdit;
        ASPxSpinEdit textValorPfeeAdministrador = pageControl.FindControl("textValorPfeeAdministrador") as ASPxSpinEdit;
        ASPxSpinEdit textValorOutrasAdministrador = pageControl.FindControl("textValorOutrasAdministrador") as ASPxSpinEdit;
        ASPxSpinEdit textValorAdmGestor = pageControl.FindControl("textValorAdmGestor") as ASPxSpinEdit;
        ASPxSpinEdit textValorPfeeGestor = pageControl.FindControl("textValorPfeeGestor") as ASPxSpinEdit;
        ASPxSpinEdit textValorOutrasGestor = pageControl.FindControl("textValorOutrasGestor") as ASPxSpinEdit;
        //
        ASPxSpinEdit textValorAdministracaoPaga = pageControl.FindControl("textValorAdministracaoPaga") as ASPxSpinEdit;
        ASPxSpinEdit textValorPfeePaga = pageControl.FindControl("textValorPfeePaga") as ASPxSpinEdit;
        ASPxSpinEdit textValorCustodiaPaga = pageControl.FindControl("textValorCustodiaPaga") as ASPxSpinEdit;
        ASPxSpinEdit textValorOutrasDespesasPagas = pageControl.FindControl("textValorOutrasDespesasPagas") as ASPxSpinEdit;
        ASPxSpinEdit textValorAdministracaoPagaGrupoEconomicoADM = pageControl.FindControl("textValorAdministracaoPagaGrupoEconomicoADM") as ASPxSpinEdit;
        ASPxSpinEdit textValorDespesasOperacionaisGrupoEconomicoADM = pageControl.FindControl("textValorDespesasOperacionaisGrupoEconomicoADM") as ASPxSpinEdit;
        ASPxSpinEdit textValorAdministracaoPagaGrupoEconomicoGestor = pageControl.FindControl("textValorAdministracaoPagaGrupoEconomicoGestor") as ASPxSpinEdit;
        ASPxSpinEdit textValorDespesasOperacionaisGrupoEconomicoGestor = pageControl.FindControl("textValorDespesasOperacionaisGrupoEconomicoGestor") as ASPxSpinEdit;

        #region Update
        if (!gridCadastro.IsNewRowEditing) {

            int idCarteira = Convert.ToInt32(btnEditCodigoCarteira.Text);

            Cliente cliente = new Cliente();
            cliente.LoadByPrimaryKey(idCarteira);

            #region Campos obrigatórios
            List<Control> controles = new List<Control>();

            if (cliente.IdTipo.Value == TipoClienteFixo.Clube) {
                controles.Add(btnEditCodigoCarteira);
                controles.Add(textData);
                controles.Add(textValorAdmAdministrador);
                controles.Add(textValorPfeeAdministrador);
                controles.Add(textValorOutrasAdministrador);
                controles.Add(textValorAdmGestor);
                controles.Add(textValorPfeeGestor);
                controles.Add(textValorOutrasGestor);
            }
            else if (cliente.IdTipo.Value == TipoClienteFixo.Fundo) {
                controles.Add(btnEditCodigoCarteiraFundos);
                controles.Add(textDataFundos);
                controles.Add(textValorAdministracaoPaga);
                controles.Add(textValorPfeePaga);
                controles.Add(textValorCustodiaPaga);
                controles.Add(textValorOutrasDespesasPagas);
                controles.Add(textValorAdministracaoPagaGrupoEconomicoADM);
                controles.Add(textValorDespesasOperacionaisGrupoEconomicoADM);
                controles.Add(textValorAdministracaoPagaGrupoEconomicoGestor);
                controles.Add(textValorDespesasOperacionaisGrupoEconomicoGestor);
            }

            if (base.TestaObrigatorio(controles) != "") {
                e.Result = "Campos com * são obrigatórios!";
                return;
            }
            #endregion
        }
        #endregion

        #region Insert
        else if (gridCadastro.IsNewRowEditing) {
            // Primeira Aba
            if (pageControl.ActiveTabIndex == 0) {

                #region Campos obrigatórios
                List<Control> controles = new List<Control>();
                controles.Add(btnEditCodigoCarteira);
                controles.Add(textData);
                controles.Add(textValorAdmAdministrador);
                controles.Add(textValorPfeeAdministrador);
                controles.Add(textValorOutrasAdministrador);
                controles.Add(textValorAdmGestor);
                controles.Add(textValorPfeeGestor);
                controles.Add(textValorOutrasGestor);

                if (base.TestaObrigatorio(controles) != "") {
                    e.Result = "Campos com * são obrigatórios!";
                    return;
                }
                #endregion


                //Testa se idCarteira é Clube
                int idCarteira = Convert.ToInt32(btnEditCodigoCarteira.Text);

                Cliente cliente = new Cliente();
                cliente.LoadByPrimaryKey(idCarteira);

                if (!(cliente.IdTipo.Value == (int)TipoClienteFixo.Clube)) {
                    e.Result = "Carteira deve ser um Clube.";
                    return;
                }
            }
            // Segunda Aba
            else if (pageControl.ActiveTabIndex == 1) {

                #region Campos obrigatórios
                List<Control> controles = new List<Control>();
                controles.Add(btnEditCodigoCarteiraFundos);
                controles.Add(textDataFundos);
                controles.Add(textValorAdministracaoPaga);
                controles.Add(textValorPfeePaga);
                controles.Add(textValorCustodiaPaga);
                controles.Add(textValorOutrasDespesasPagas);
                controles.Add(textValorAdministracaoPagaGrupoEconomicoADM);
                controles.Add(textValorDespesasOperacionaisGrupoEconomicoADM);
                controles.Add(textValorAdministracaoPagaGrupoEconomicoGestor);
                controles.Add(textValorDespesasOperacionaisGrupoEconomicoGestor);

                if (base.TestaObrigatorio(controles) != "") {
                    e.Result = "Campos com * são obrigatórios!";
                    return;
                }
                #endregion

                //Testa se idCarteira é Fundo
                int idCarteira = Convert.ToInt32(btnEditCodigoCarteiraFundos.Text);

                Cliente cliente = new Cliente();
                cliente.LoadByPrimaryKey(idCarteira);

                if (!(cliente.IdTipo.Value == (int)TipoClienteFixo.Fundo)) {
                    e.Result = "Carteira deve ser um Fundo.";
                    return;
                }
            }

            TabelaInformeDesempenhoCollection tabelaInformeDesempenhoCollection = new TabelaInformeDesempenhoCollection();

            // Primeira Aba
            if (pageControl.ActiveTabIndex == 0) {
                tabelaInformeDesempenhoCollection.Query.Where(
                                                tabelaInformeDesempenhoCollection.Query.IdCarteira == Convert.ToInt32(btnEditCodigoCarteira.Text) &&
                                                tabelaInformeDesempenhoCollection.Query.Data == Convert.ToDateTime(textData.Text));
                tabelaInformeDesempenhoCollection.Query.Load();

            }
            // Segunda Aba
            else if (pageControl.ActiveTabIndex == 1) {

                tabelaInformeDesempenhoCollection.Query.Where(
                                                tabelaInformeDesempenhoCollection.Query.IdCarteira == Convert.ToInt32(btnEditCodigoCarteiraFundos.Text) &&
                                                tabelaInformeDesempenhoCollection.Query.Data == Convert.ToDateTime(textDataFundos.Text));
                tabelaInformeDesempenhoCollection.Query.Load();
            }

            if (tabelaInformeDesempenhoCollection.Count > 0) {
                e.Result = "Registro da tabela já existente!";
                return;
            }
        }
        #endregion
    }

    protected void callbackAdd_Callback(object source, DevExpress.Web.CallbackEventArgs e) {
        SalvarNovo();
    }

    protected void ASPxCallback1_Callback(object source, DevExpress.Web.CallbackEventArgs e) {
        string nome = "";
        if (!String.IsNullOrEmpty(e.Parameter) && e.Parameter != "null") {
            int idCliente = Convert.ToInt32(e.Parameter);
            Cliente cliente = new Cliente();
            cliente.LoadByPrimaryKey(idCliente);

            if (cliente.str.Apelido != "") {
                if (cliente.IsAtivo) {
                    PermissaoCliente permissaoCliente = new PermissaoCliente();
                    if (permissaoCliente.RetornaAcessoCliente(idCliente, HttpContext.Current.User.Identity.Name)) {
                        nome = cliente.str.Apelido;
                    }
                    else {
                        nome = "no_access";
                    }
                }
                else {
                    nome = "no_active";
                }
            }
        }
        e.Result = nome;
    }

    protected void ASPxCallback2_Callback(object source, DevExpress.Web.CallbackEventArgs e) {
        string nome = "";
        if (!String.IsNullOrEmpty(e.Parameter) && e.Parameter != "null") {
            int idCliente = Convert.ToInt32(e.Parameter);
            Cliente cliente = new Cliente();
            cliente.LoadByPrimaryKey(idCliente);

            if (cliente.str.Apelido != "") {
                if (cliente.IsAtivo) {
                    PermissaoCliente permissaoCliente = new PermissaoCliente();
                    if (permissaoCliente.RetornaAcessoCliente(idCliente, HttpContext.Current.User.Identity.Name)) {
                        nome = cliente.str.Apelido;
                    }
                    else {
                        nome = "no_access";
                    }
                }
                else {
                    nome = "no_active";
                }
            }
        }
        e.Result = nome;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_RowUpdating(object sender, DevExpress.Web.Data.ASPxDataUpdatingEventArgs e) {
        TabelaInformeDesempenho tabelaInformeDesempenho = new TabelaInformeDesempenho();
        //
        ASPxPageControl pageControl = gridCadastro.FindEditFormTemplateControl("tabCadastro") as ASPxPageControl;
        //
        ASPxDateEdit textData = pageControl.FindControl("textData") as ASPxDateEdit;
        ASPxSpinEdit btnEditCodigoCarteira = pageControl.FindControl("btnEditCodigoCarteira") as ASPxSpinEdit;
        //
        ASPxSpinEdit textValorAdmAdministrador = pageControl.FindControl("textValorAdmAdministrador") as ASPxSpinEdit;
        ASPxSpinEdit textValorPfeeAdministrador = pageControl.FindControl("textValorPfeeAdministrador") as ASPxSpinEdit;
        ASPxSpinEdit textValorOutrasAdministrador = pageControl.FindControl("textValorOutrasAdministrador") as ASPxSpinEdit;
        ASPxSpinEdit textValorAdmGestor = pageControl.FindControl("textValorAdmGestor") as ASPxSpinEdit;
        ASPxSpinEdit textValorPfeeGestor = pageControl.FindControl("textValorPfeeGestor") as ASPxSpinEdit;
        ASPxSpinEdit textValorOutrasGestor = pageControl.FindControl("textValorOutrasGestor") as ASPxSpinEdit;
        //
        ASPxSpinEdit textValorAdministracaoPaga = pageControl.FindControl("textValorAdministracaoPaga") as ASPxSpinEdit;
        ASPxSpinEdit textValorPfeePaga = pageControl.FindControl("textValorPfeePaga") as ASPxSpinEdit;
        ASPxSpinEdit textValorCustodiaPaga = pageControl.FindControl("textValorCustodiaPaga") as ASPxSpinEdit;
        ASPxSpinEdit textValorOutrasDespesasPagas = pageControl.FindControl("textValorOutrasDespesasPagas") as ASPxSpinEdit;
        ASPxSpinEdit textValorAdministracaoPagaGrupoEconomicoADM = pageControl.FindControl("textValorAdministracaoPagaGrupoEconomicoADM") as ASPxSpinEdit;
        ASPxSpinEdit textValorDespesasOperacionaisGrupoEconomicoADM = pageControl.FindControl("textValorDespesasOperacionaisGrupoEconomicoADM") as ASPxSpinEdit;
        ASPxSpinEdit textValorAdministracaoPagaGrupoEconomicoGestor = pageControl.FindControl("textValorAdministracaoPagaGrupoEconomicoGestor") as ASPxSpinEdit;
        ASPxSpinEdit textValorDespesasOperacionaisGrupoEconomicoGestor = pageControl.FindControl("textValorDespesasOperacionaisGrupoEconomicoGestor") as ASPxSpinEdit;
        //
        DateTime data = Convert.ToDateTime(textData.Text);
        int idCarteira = Convert.ToInt32(btnEditCodigoCarteira.Text);

        Cliente cliente = new Cliente();
        cliente.LoadByPrimaryKey(idCarteira);

        if (cliente.IdTipo.Value == TipoClienteFixo.Clube) {
            #region Clubes
            if (tabelaInformeDesempenho.LoadByPrimaryKey(idCarteira, data)) {
                tabelaInformeDesempenho.ValorAdmAdministrador = Convert.ToDecimal(textValorAdmAdministrador.Text);
                tabelaInformeDesempenho.ValorPfeeAdministrador = Convert.ToDecimal(textValorPfeeAdministrador.Text);
                tabelaInformeDesempenho.ValorOutrasAdministrador = Convert.ToDecimal(textValorOutrasAdministrador.Text);
                tabelaInformeDesempenho.ValorAdmGestor = Convert.ToDecimal(textValorAdmGestor.Text);
                tabelaInformeDesempenho.ValorPfeeGestor = Convert.ToDecimal(textValorPfeeGestor.Text);
                tabelaInformeDesempenho.ValorOutrasGestor = Convert.ToDecimal(textValorOutrasGestor.Text);
                //
                tabelaInformeDesempenho.ValorAdministracaoPaga = 0;
                tabelaInformeDesempenho.ValorPfeePaga = 0;
                tabelaInformeDesempenho.ValorCustodiaPaga = 0;
                tabelaInformeDesempenho.ValorOutrasDespesasPagas = 0;
                tabelaInformeDesempenho.ValorAdministracaoPagaGrupoEconomicoADM = 0;
                tabelaInformeDesempenho.ValorDespesasOperacionaisGrupoEconomicoADM = 0;
                tabelaInformeDesempenho.ValorAdministracaoPagaGrupoEconomicoGestor = 0;
                tabelaInformeDesempenho.ValorDespesasOperacionaisGrupoEconomicoGestor = 0;
                //
                tabelaInformeDesempenho.Save();

                #region Log do Processo
                HistoricoLog historicoLog = new HistoricoLog();
                historicoLog.InsereHistoricoLog(DateTime.Now,
                                                DateTime.Now,
                                                "Cadastro de TabelaInformeDesempenho - Operacao: Update TabelaInformeDesempenho: Carteira: " + idCarteira.ToString() +
                                                                            "- Data" + data.ToShortDateString() + UtilitarioWeb.ToString(tabelaInformeDesempenho),
                                                HttpContext.Current.User.Identity.Name,
                                                UtilitarioWeb.GetIP(Request),
                                                "",
                                                HistoricoLogOrigem.Outros);
                #endregion
            }
            #endregion
        }
        else if (cliente.IdTipo.Value == TipoClienteFixo.Fundo) {
            #region Fundos
            if (tabelaInformeDesempenho.LoadByPrimaryKey(idCarteira, data)) {
                tabelaInformeDesempenho.ValorAdministracaoPaga = Convert.ToDecimal(textValorAdministracaoPaga.Text);
                tabelaInformeDesempenho.ValorPfeePaga = Convert.ToDecimal(textValorPfeePaga.Text);
                tabelaInformeDesempenho.ValorCustodiaPaga = Convert.ToDecimal(textValorCustodiaPaga.Text);
                tabelaInformeDesempenho.ValorOutrasDespesasPagas = Convert.ToDecimal(textValorOutrasDespesasPagas.Text);
                tabelaInformeDesempenho.ValorAdministracaoPagaGrupoEconomicoADM = Convert.ToDecimal(textValorAdministracaoPagaGrupoEconomicoADM.Text);
                tabelaInformeDesempenho.ValorDespesasOperacionaisGrupoEconomicoADM = Convert.ToDecimal(textValorDespesasOperacionaisGrupoEconomicoADM.Text);
                tabelaInformeDesempenho.ValorAdministracaoPagaGrupoEconomicoGestor = Convert.ToDecimal(textValorAdministracaoPagaGrupoEconomicoGestor.Text);
                tabelaInformeDesempenho.ValorDespesasOperacionaisGrupoEconomicoGestor = Convert.ToDecimal(textValorDespesasOperacionaisGrupoEconomicoGestor.Text);
                //
                tabelaInformeDesempenho.ValorAdmAdministrador = 0;
                tabelaInformeDesempenho.ValorPfeeAdministrador = 0;
                tabelaInformeDesempenho.ValorOutrasAdministrador = 0;
                tabelaInformeDesempenho.ValorAdmGestor = 0;
                tabelaInformeDesempenho.ValorPfeeGestor = 0;
                tabelaInformeDesempenho.ValorOutrasGestor = 0;
                //
                tabelaInformeDesempenho.Save();

                #region Log do Processo
                HistoricoLog historicoLog = new HistoricoLog();
                historicoLog.InsereHistoricoLog(DateTime.Now,
                                                DateTime.Now,
                                                "Cadastro de TabelaInformeDesempenho - Operacao: Update TabelaInformeDesempenho: Carteira: " + idCarteira.ToString() +
                                                                            "- Data" + data.ToShortDateString() + UtilitarioWeb.ToString(tabelaInformeDesempenho),
                                                HttpContext.Current.User.Identity.Name,
                                                UtilitarioWeb.GetIP(Request),
                                                "",
                                                HistoricoLogOrigem.Outros);
                #endregion
            }
            #endregion
        }

        e.Cancel = true;
        gridCadastro.CancelEdit();
        gridCadastro.DataBind();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_RowInserting(object sender, DevExpress.Web.Data.ASPxDataInsertingEventArgs e) {
        this.SalvarNovo();

        e.Cancel = true;
        gridCadastro.CancelEdit();
        gridCadastro.DataBind();
    }

    private void SalvarNovo() {

        TabelaInformeDesempenho tabelaInformeDesempenho = new TabelaInformeDesempenho();
        //
        ASPxPageControl pageControl = gridCadastro.FindEditFormTemplateControl("tabCadastro") as ASPxPageControl;
        //
        ASPxDateEdit textData = pageControl.FindControl("textData") as ASPxDateEdit;
        ASPxSpinEdit btnEditCodigoCarteira = pageControl.FindControl("btnEditCodigoCarteira") as ASPxSpinEdit;

        ASPxDateEdit textDataFundos = pageControl.FindControl("textDataF") as ASPxDateEdit;
        ASPxSpinEdit btnEditCodigoCarteiraFundos = pageControl.FindControl("btnEditCodigoCarteiraF") as ASPxSpinEdit;
        //

        ASPxSpinEdit textValorAdmAdministrador = pageControl.FindControl("textValorAdmAdministrador") as ASPxSpinEdit;
        ASPxSpinEdit textValorPfeeAdministrador = pageControl.FindControl("textValorPfeeAdministrador") as ASPxSpinEdit;
        ASPxSpinEdit textValorOutrasAdministrador = pageControl.FindControl("textValorOutrasAdministrador") as ASPxSpinEdit;
        ASPxSpinEdit textValorAdmGestor = pageControl.FindControl("textValorAdmGestor") as ASPxSpinEdit;
        ASPxSpinEdit textValorPfeeGestor = pageControl.FindControl("textValorPfeeGestor") as ASPxSpinEdit;
        ASPxSpinEdit textValorOutrasGestor = pageControl.FindControl("textValorOutrasGestor") as ASPxSpinEdit;
        //
        ASPxSpinEdit textValorAdministracaoPaga = pageControl.FindControl("textValorAdministracaoPaga") as ASPxSpinEdit;
        ASPxSpinEdit textValorPfeePaga = pageControl.FindControl("textValorPfeePaga") as ASPxSpinEdit;
        ASPxSpinEdit textValorCustodiaPaga = pageControl.FindControl("textValorCustodiaPaga") as ASPxSpinEdit;
        ASPxSpinEdit textValorOutrasDespesasPagas = pageControl.FindControl("textValorOutrasDespesasPagas") as ASPxSpinEdit;
        ASPxSpinEdit textValorAdministracaoPagaGrupoEconomicoADM = pageControl.FindControl("textValorAdministracaoPagaGrupoEconomicoADM") as ASPxSpinEdit;
        ASPxSpinEdit textValorDespesasOperacionaisGrupoEconomicoADM = pageControl.FindControl("textValorDespesasOperacionaisGrupoEconomicoADM") as ASPxSpinEdit;
        ASPxSpinEdit textValorAdministracaoPagaGrupoEconomicoGestor = pageControl.FindControl("textValorAdministracaoPagaGrupoEconomicoGestor") as ASPxSpinEdit;
        ASPxSpinEdit textValorDespesasOperacionaisGrupoEconomicoGestor = pageControl.FindControl("textValorDespesasOperacionaisGrupoEconomicoGestor") as ASPxSpinEdit;

        // Primeira Aba
        if (pageControl.ActiveTabIndex == 0) {
            DateTime data = Convert.ToDateTime(textData.Text);
            int idCarteira = Convert.ToInt32(btnEditCodigoCarteira.Text);

            tabelaInformeDesempenho.Data = data;
            tabelaInformeDesempenho.IdCarteira = idCarteira;
            tabelaInformeDesempenho.ValorAdmAdministrador = Convert.ToDecimal(textValorAdmAdministrador.Text);
            tabelaInformeDesempenho.ValorPfeeAdministrador = Convert.ToDecimal(textValorPfeeAdministrador.Text);
            tabelaInformeDesempenho.ValorOutrasAdministrador = Convert.ToDecimal(textValorOutrasAdministrador.Text);
            tabelaInformeDesempenho.ValorAdmGestor = Convert.ToDecimal(textValorAdmGestor.Text);
            tabelaInformeDesempenho.ValorPfeeGestor = Convert.ToDecimal(textValorPfeeGestor.Text);
            tabelaInformeDesempenho.ValorOutrasGestor = Convert.ToDecimal(textValorOutrasGestor.Text);
            //
            tabelaInformeDesempenho.ValorAdministracaoPaga = 0;
            tabelaInformeDesempenho.ValorPfeePaga = 0;
            tabelaInformeDesempenho.ValorCustodiaPaga = 0;
            tabelaInformeDesempenho.ValorOutrasDespesasPagas = 0;
            tabelaInformeDesempenho.ValorAdministracaoPagaGrupoEconomicoADM = 0;
            tabelaInformeDesempenho.ValorDespesasOperacionaisGrupoEconomicoADM = 0;
            tabelaInformeDesempenho.ValorAdministracaoPagaGrupoEconomicoGestor = 0;
            tabelaInformeDesempenho.ValorDespesasOperacionaisGrupoEconomicoGestor = 0;
            //
            tabelaInformeDesempenho.Save();
        }
        // Segunda Aba
        else if (pageControl.ActiveTabIndex == 1) {
            DateTime data = Convert.ToDateTime(textDataFundos.Text);
            int idCarteira = Convert.ToInt32(btnEditCodigoCarteiraFundos.Text);

            tabelaInformeDesempenho.Data = data;
            tabelaInformeDesempenho.IdCarteira = idCarteira;
            //
            tabelaInformeDesempenho.ValorAdministracaoPaga = Convert.ToDecimal(textValorAdministracaoPaga.Text);
            tabelaInformeDesempenho.ValorPfeePaga = Convert.ToDecimal(textValorPfeePaga.Text);
            tabelaInformeDesempenho.ValorCustodiaPaga = Convert.ToDecimal(textValorCustodiaPaga.Text);
            tabelaInformeDesempenho.ValorOutrasDespesasPagas = Convert.ToDecimal(textValorOutrasDespesasPagas.Text);
            tabelaInformeDesempenho.ValorAdministracaoPagaGrupoEconomicoADM = Convert.ToDecimal(textValorAdministracaoPagaGrupoEconomicoADM.Text);
            tabelaInformeDesempenho.ValorDespesasOperacionaisGrupoEconomicoADM = Convert.ToDecimal(textValorDespesasOperacionaisGrupoEconomicoADM.Text);
            tabelaInformeDesempenho.ValorAdministracaoPagaGrupoEconomicoGestor = Convert.ToDecimal(textValorAdministracaoPagaGrupoEconomicoGestor.Text);
            tabelaInformeDesempenho.ValorDespesasOperacionaisGrupoEconomicoGestor = Convert.ToDecimal(textValorDespesasOperacionaisGrupoEconomicoGestor.Text);
            //
            tabelaInformeDesempenho.ValorAdmAdministrador = 0;
            tabelaInformeDesempenho.ValorPfeeAdministrador = 0;
            tabelaInformeDesempenho.ValorOutrasAdministrador = 0;
            tabelaInformeDesempenho.ValorAdmGestor = 0;
            tabelaInformeDesempenho.ValorPfeeGestor = 0;
            tabelaInformeDesempenho.ValorOutrasGestor = 0;
            //
            tabelaInformeDesempenho.Save();
        }

        #region Log do Processo
        HistoricoLog historicoLog = new HistoricoLog();
        historicoLog.InsereHistoricoLog(DateTime.Now,
                                        DateTime.Now,
                                        "Cadastro de TabelaInformeDesempenho - Operacao: Insert TabelaInformeDesempenho: " + UtilitarioWeb.ToString(tabelaInformeDesempenho),
                                        HttpContext.Current.User.Identity.Name,
                                        UtilitarioWeb.GetIP(Request),
                                        "",
                                        HistoricoLogOrigem.Outros);
        #endregion
    }

    protected void gridCadastro_CustomCallback(object sender, DevExpress.Web.ASPxGridViewCustomCallbackEventArgs e)
    {
        if (e.Parameters == "btnDelete") {
            List<object> keyValuesData = gridCadastro.GetSelectedFieldValues(TabelaInformeDesempenhoMetadata.ColumnNames.Data);
            List<object> keyValuesIdCarteira = gridCadastro.GetSelectedFieldValues(TabelaInformeDesempenhoMetadata.ColumnNames.IdCarteira);

            for (int i = 0; i < keyValuesIdCarteira.Count; i++) {
                int idCarteira = Convert.ToInt32(keyValuesIdCarteira[i]);
                DateTime data = Convert.ToDateTime(keyValuesData[i]);

                TabelaInformeDesempenho tabelaInformeDesempenho = new TabelaInformeDesempenho();
                if (tabelaInformeDesempenho.LoadByPrimaryKey(idCarteira, data)) {
                    //
                    TabelaInformeDesempenho tabelaInformeDesempenhoClone = (TabelaInformeDesempenho)Utilitario.Clone(tabelaInformeDesempenho);
                    //

                    tabelaInformeDesempenho.MarkAsDeleted();
                    tabelaInformeDesempenho.Save();

                    #region Log do Processo
                    HistoricoLog historicoLog = new HistoricoLog();
                    historicoLog.InsereHistoricoLog(DateTime.Now,
                                                    DateTime.Now,
                                                    "Cadastro de TabelaInformeDesempenho - Operacao: Delete TabelaInformeDesempenho: Carteira: " + idCarteira.ToString() +
                                                                        "- Data" + data.ToShortDateString() + UtilitarioWeb.ToString(tabelaInformeDesempenhoClone),
                                                    HttpContext.Current.User.Identity.Name,
                                                    UtilitarioWeb.GetIP(Request),
                                                    "",
                                                    HistoricoLogOrigem.Outros);
                    #endregion
                }
            }
        }

        gridCadastro.DataBind();
        gridCadastro.Selection.UnselectAll();
        gridCadastro.CancelEdit();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_CustomUnboundColumnData(object sender, DevExpress.Web.ASPxGridViewColumnDataEventArgs e)
    {
        if (e.Column.FieldName == "CompositeKey") {
            //Compose a primary key value
            string data = Convert.ToString(e.GetListSourceFieldValue(TabelaInformeDesempenhoMetadata.ColumnNames.Data));
            int idCarteira = Convert.ToInt32(e.GetListSourceFieldValue(TabelaInformeDesempenhoMetadata.ColumnNames.IdCarteira));
            e.Value = data + "|" + idCarteira.ToString();
        }
    }

    new protected void gridCadastro_CustomJSProperties(object sender, ASPxGridViewClientJSPropertiesEventArgs e) {
        base.gridCadastro_CustomJSProperties(sender, e);

        //ASPxGridView gridView = sender as ASPxGridView;
        //if (gridView.IsEditing) {

        //    ASPxPageControl pageControl = gridCadastro.FindEditFormTemplateControl("tabCadastro") as ASPxPageControl;

        //    ASPxSpinEdit btnEditCodigoCarteira = pageControl.FindControl("btnEditCodigoCarteira") as ASPxSpinEdit;
        //    e.Properties["cpBtnEditCodigoCarteira"] = btnEditCodigoCarteira.ClientID;
        //}
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    new protected void gridCadastro_PreRender(object sender, EventArgs e) {
        //        this.FocaCampoGrid("btnEditCodigoCarteira", "textValorAdmAdministrador");
        base.gridCadastro_PreRender(sender, e);
    }

    /// <summary>
    /// TabControl - Controla visibilidade da 1 e 2 ABA
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    new protected void tabCadastroOnLoad(object sender, EventArgs e) {
        ASPxPageControl pageControl = sender as ASPxPageControl;

        //Somente no Update
        if (!gridCadastro.IsNewRowEditing) {
            int idCarteira = Convert.ToInt32(this.gridCadastro.GetRowValues(this.gridCadastro.EditingRowVisibleIndex, CarteiraMetadata.ColumnNames.IdCarteira));

            Cliente cliente = new Cliente();
            cliente.LoadByPrimaryKey(idCarteira);

            pageControl.TabPages[0].ClientVisible = false;
            pageControl.TabPages[1].ClientVisible = false;
            //

            if (cliente.IdTipo.Value == TipoClienteFixo.Clube) {
                pageControl.TabPages[0].ClientVisible = true;
            }

            if (cliente.IdTipo.Value == TipoClienteFixo.Fundo) {
                pageControl.TabPages[1].ClientVisible = true;
            }
        }
    }
}