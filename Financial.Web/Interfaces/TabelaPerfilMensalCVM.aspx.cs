﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Web.UI.HtmlControls;
using Financial.Common;
using System.Collections.Generic;
using Financial.Util;
using System.Text;
using DevExpress.Web;
using System.Drawing;
using Financial.Bolsa;
using Financial.Web.Common;
using Financial.Security;
using Financial.Web.Util;
using Financial.Security.Enums;
using Financial.Fundo;
using Financial.Investidor;
using EntitySpaces.Interfaces;

public partial class CadastrosBasicos_TabelaPerfilMensalCVM : CadastroBasePage {
    new protected void Page_Load(object sender, EventArgs e) {
        this.HasPopupCarteira = true;
        base.Page_Load(sender, e);
    }

    #region Load dos esDataSources (grid principal, eventuais grids de popup e dropdownlists)
    protected void EsDSTabela_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e) {
        Usuario usuario = new Usuario();
        usuario.BuscaUsuario(Context.User.Identity.Name);
        int idUsuario = usuario.IdUsuario.Value;

        TabelaPerfilMensalCVMQuery tabelaPerfilMensalCVMQuery = new TabelaPerfilMensalCVMQuery("V");
        CarteiraQuery carteiraQuery = new CarteiraQuery("C");
        ClienteQuery clienteQuery = new ClienteQuery("Q");
        PermissaoClienteQuery permissaoClienteQuery = new PermissaoClienteQuery("P");

        tabelaPerfilMensalCVMQuery.Select(tabelaPerfilMensalCVMQuery, carteiraQuery.Apelido);
        tabelaPerfilMensalCVMQuery.InnerJoin(carteiraQuery).On(tabelaPerfilMensalCVMQuery.IdCarteira == carteiraQuery.IdCarteira);
        tabelaPerfilMensalCVMQuery.InnerJoin(clienteQuery).On(clienteQuery.IdCliente == tabelaPerfilMensalCVMQuery.IdCarteira);
        tabelaPerfilMensalCVMQuery.InnerJoin(permissaoClienteQuery).On(permissaoClienteQuery.IdCliente == clienteQuery.IdCliente);
        tabelaPerfilMensalCVMQuery.Where(permissaoClienteQuery.IdUsuario.Equal(idUsuario));

        tabelaPerfilMensalCVMQuery.OrderBy(tabelaPerfilMensalCVMQuery.IdCarteira.Ascending, tabelaPerfilMensalCVMQuery.Data.Descending);

        TabelaPerfilMensalCVMCollection coll = new TabelaPerfilMensalCVMCollection();
        coll.Load(tabelaPerfilMensalCVMQuery);

        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }

    protected void EsDSCarteira_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e) {
        CarteiraCollection coll = new CarteiraCollection();
        coll.BuscaCarteirasComAcessoTotal(Context.User.Identity.Name);

        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }

    protected void EsDSTabelaPerfilClonar_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e) {
        TabelaPerfilMensalCVMCollection coll = new TabelaPerfilMensalCVMCollection();

        TabelaPerfilMensalCVMQuery info = new TabelaPerfilMensalCVMQuery("e");
        CarteiraQuery c = new CarteiraQuery("C");

        info.Select(info.IdCarteira, info.Data, c.Nome, c.Nome.As("DataInicioVigenciaString"),
                   (info.IdCarteira.Cast(esCastType.String) + "-" + info.Data.Cast(esCastType.String)).As("CompositeKey")
            );
        info.InnerJoin(c).On(info.IdCarteira == c.IdCarteira);
        //
        info.OrderBy(info.IdCarteira.Ascending, info.Data.Descending);

        coll.Load(info);

        for (int i = 0; i < coll.Count; i++) {
            DateTime dataVigencia = Convert.ToDateTime(coll[i].GetColumn(TabelaPerfilMensalCVMMetadata.ColumnNames.Data));
            //
            //DateTime novaData = new DateTime(dataVigencia.Year, dataVigencia.Month, dataVigencia.Day);

            coll[i].SetColumn("DataInicioVigenciaString", dataVigencia.ToString("d"));
        }

        //
        e.Collection = coll;
    }

    protected void EsDSCarteiraClonar_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e) {
        CarteiraCollection coll = new CarteiraCollection();
        coll.BuscaCarteirasComAcesso(Context.User.Identity.Name);
        //        
        e.Collection = coll;
    }

    #endregion

    /// <summary>
    /// Faz Clonagem
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void callBackClonar_Callback(object source, DevExpress.Web.CallbackEventArgs e) {

        #region Campos obrigatórios
        List<Control> controles = new List<Control>();
        controles.Add(dropInfoComplementares);
        controles.Add(dropCarteiraDestino);
        controles.Add(textDataClonar);

        if (base.TestaObrigatorio(controles) != "") {
            e.Result = "Campos com * são obrigatórios!";
            return;
        }
        #endregion

        #region Confere registro já existente
        TabelaPerfilMensalCVM infoVelha = new TabelaPerfilMensalCVM();
        infoVelha.LoadByPrimaryKey(Convert.ToDateTime(textDataClonar.Value), Convert.ToInt32(dropCarteiraDestino.Text.Trim()));

        if (infoVelha.es.HasData) {
            e.Result = "Registro Já existente";
            return;
        }
        #endregion

        string selecionado = dropInfoComplementares.Text.Trim();
        string[] selecionadoAux = selecionado.Split(new Char[] { '-' });
        int idCarteira = Convert.ToInt32(selecionadoAux[0].Trim());
        DateTime data = Convert.ToDateTime(selecionadoAux[2].Trim());
        //        

        // Carrega Informação velha
        TabelaPerfilMensalCVM infoNova = new TabelaPerfilMensalCVM();
        infoNova.LoadByPrimaryKey(data, idCarteira);

        infoNova.MarkAllColumnsAsDirty(DataRowState.Added);

        // Acrescenta nova Carteira e Data
        infoNova.IdCarteira = Convert.ToInt32(dropCarteiraDestino.Text.Trim());
        infoNova.Data = Convert.ToDateTime(textDataClonar.Value);
        //
        infoNova.Save();

        //
        e.Result = "Processo executado com sucesso.";
    }

    protected void callbackErro_Callback(object source, DevExpress.Web.CallbackEventArgs e) {

        e.Result = "";

        ASPxPageControl pageControl = gridCadastro.FindEditFormTemplateControl("tabCadastro") as ASPxPageControl;

        ASPxDateEdit textData = pageControl.FindControl("textData") as ASPxDateEdit;
        ASPxSpinEdit btnEditCodigoCarteira = pageControl.FindControl("btnEditCodigoCarteira") as ASPxSpinEdit;

        #region Campos obrigatórios
        List<Control> controles = new List<Control>();
        controles.Add(textData);
        controles.Add(btnEditCodigoCarteira);

        if (base.TestaObrigatorio(controles) != "") {
            e.Result = "Campos com * são obrigatórios!";
            return;
        }
        #endregion

        if (gridCadastro.IsNewRowEditing) {
            DateTime data = Convert.ToDateTime(textData.Text);
            int idCarteira = Convert.ToInt32(btnEditCodigoCarteira.Text);

            TabelaPerfilMensalCVM tabelaPerfilMensalCVM = new TabelaPerfilMensalCVM();
            if (tabelaPerfilMensalCVM.LoadByPrimaryKey(data, idCarteira)) {
                e.Result = "Registro já existente";
            }
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void textData_Init(object sender, EventArgs e) {
        if (!gridCadastro.IsNewRowEditing)
            (sender as ASPxDateEdit).Enabled = false;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnEditCodigoCarteira_Init(object sender, EventArgs e) {
        if (!gridCadastro.IsNewRowEditing)
            (sender as ASPxSpinEdit).Enabled = false;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void ASPxCallback1_Callback(object source, DevExpress.Web.CallbackEventArgs e) {
        string nome = "";
        if (!String.IsNullOrEmpty(e.Parameter) && e.Parameter != "null") {
            int idCarteira = Convert.ToInt32(e.Parameter);
            Carteira carteira = new Carteira();
            carteira.LoadByPrimaryKey(idCarteira);

            if (carteira.str.Apelido != "") {
                if (carteira.IsAtivo) {
                    PermissaoCliente permissaoCliente = new PermissaoCliente();
                    if (permissaoCliente.RetornaAcessoCliente(idCarteira, HttpContext.Current.User.Identity.Name)) {
                        nome = carteira.str.Apelido;
                    }
                    else {
                        nome = "no_access";
                    }
                }
                else {
                    nome = "no_active";
                }
            }
        }
        e.Result = nome;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_RowUpdating(object sender, DevExpress.Web.Data.ASPxDataUpdatingEventArgs e) {
        TabelaPerfilMensalCVM tabelaPerfilMensalCVM = new TabelaPerfilMensalCVM();

        ASPxPageControl pageControl = gridCadastro.FindEditFormTemplateControl("tabCadastro") as ASPxPageControl;

        ASPxDateEdit textData = pageControl.FindControl("textData") as ASPxDateEdit;
        ASPxSpinEdit btnEditCodigoCarteira = pageControl.FindControl("btnEditCodigoCarteira") as ASPxSpinEdit;

        TextBox textSecao3 = pageControl.FindControl("textSecao3") as TextBox;
        TextBox textSecao4 = pageControl.FindControl("textSecao4") as TextBox;
        ASPxSpinEdit textSecao5 = pageControl.FindControl("textSecao5") as ASPxSpinEdit;
        ASPxComboBox dropSecao6 = pageControl.FindControl("dropSecao6") as ASPxComboBox;
        TextBox textSecao8 = pageControl.FindControl("textSecao8") as TextBox;
        ASPxSpinEdit textSecao11PercentCota = pageControl.FindControl("textSecao11PercentCota") as ASPxSpinEdit;
        TextBox textSecao11Fator1 = pageControl.FindControl("textSecao11Fator1") as TextBox;
        TextBox textSecao11Cenario1 = pageControl.FindControl("textSecao11Cenario1") as TextBox;
        TextBox textSecao11Fator2 = pageControl.FindControl("textSecao11Fator2") as TextBox;
        TextBox textSecao11Cenario2 = pageControl.FindControl("textSecao11Cenario2") as TextBox;
        TextBox textSecao11Fator3 = pageControl.FindControl("textSecao11Fator3") as TextBox;
        TextBox textSecao11Cenario3 = pageControl.FindControl("textSecao11Cenario3") as TextBox;
        TextBox textSecao11Fator4 = pageControl.FindControl("textSecao11Fator4") as TextBox;
        TextBox textSecao11Cenario4 = pageControl.FindControl("textSecao11Cenario4") as TextBox;
        TextBox textSecao11Fator5 = pageControl.FindControl("textSecao11Fator5") as TextBox;
        TextBox textSecao11Cenario5 = pageControl.FindControl("textSecao11Cenario5") as TextBox;
        ASPxSpinEdit textSecao12 = pageControl.FindControl("textSecao12") as ASPxSpinEdit;
        ASPxSpinEdit textSecao13 = pageControl.FindControl("textSecao13") as ASPxSpinEdit;
        ASPxSpinEdit textSecao14 = pageControl.FindControl("textSecao14") as ASPxSpinEdit;
        ASPxSpinEdit textSecao15 = pageControl.FindControl("textSecao15") as ASPxSpinEdit;
        TextBox textSecao16Fator = pageControl.FindControl("textSecao16Fator") as TextBox;
        ASPxSpinEdit textSecao16Variacao = pageControl.FindControl("textSecao16Variacao") as ASPxSpinEdit;

        #region Aba 3
        ASPxTextBox textCNPJ1 = pageControl.FindControl("textCNPJ1") as ASPxTextBox;
        ASPxTextBox textCNPJ2 = pageControl.FindControl("textCNPJ2") as ASPxTextBox;
        ASPxTextBox textCNPJ3 = pageControl.FindControl("textCNPJ3") as ASPxTextBox;
        //
        ASPxComboBox dropParteRelacionada1 = pageControl.FindControl("dropParteRelacionada1") as ASPxComboBox;
        ASPxComboBox dropParteRelacionada2 = pageControl.FindControl("dropParteRelacionada2") as ASPxComboBox;
        ASPxComboBox dropParteRelacionada3 = pageControl.FindControl("dropParteRelacionada3") as ASPxComboBox;
        //
        ASPxSpinEdit textValor1 = pageControl.FindControl("textValor1") as ASPxSpinEdit;
        ASPxSpinEdit textValor2 = pageControl.FindControl("textValor1") as ASPxSpinEdit;
        ASPxSpinEdit textValor3 = pageControl.FindControl("textValor1") as ASPxSpinEdit;
        //
        ASPxComboBox dropVedadaCobrancaTaxa = pageControl.FindControl("dropVedadaCobrancaTaxa") as ASPxComboBox;
        ASPxDateEdit textDataUltimaCobranca = pageControl.FindControl("textDataUltimaCobranca") as ASPxDateEdit;
        ASPxSpinEdit textValorUtimaCota = pageControl.FindControl("textValorUtimaCota") as ASPxSpinEdit;
        //
        #endregion

        DateTime data = Convert.ToDateTime(textData.Text);
        int idCarteira = Convert.ToInt32(btnEditCodigoCarteira.Text);

        string secao3 = Convert.ToString(textSecao3.Text);
        string secao4 = Convert.ToString(textSecao4.Text);
        decimal? secao5 = null;
        if (textSecao5.Text != "") {
            secao5 = Convert.ToDecimal(textSecao5.Text);
        }

        byte? secao6 = null;
        if (dropSecao6.SelectedIndex != -1) {
            secao6 = Convert.ToByte(dropSecao6.SelectedItem.Value);
        }

        string secao8 = Convert.ToString(textSecao8.Text);
        decimal? secao11PercentCota = null;
        if (textSecao11PercentCota.Text != "") {
            secao11PercentCota = Convert.ToDecimal(textSecao11PercentCota.Text);
        }

        string secao11Fator1 = Convert.ToString(textSecao11Fator1.Text);
        string secao11Cenario1 = Convert.ToString(textSecao11Cenario1.Text);
        string secao11Fator2 = Convert.ToString(textSecao11Fator2.Text);
        string secao11Cenario2 = Convert.ToString(textSecao11Cenario2.Text);
        string secao11Fator3 = Convert.ToString(textSecao11Fator3.Text);
        string secao11Cenario3 = Convert.ToString(textSecao11Cenario3.Text);
        string secao11Fator4 = Convert.ToString(textSecao11Fator4.Text);
        string secao11Cenario4 = Convert.ToString(textSecao11Cenario4.Text);
        string secao11Fator5 = Convert.ToString(textSecao11Fator5.Text);
        string secao11Cenario5 = Convert.ToString(textSecao11Cenario5.Text);

        decimal? secao12 = null;
        if (textSecao12.Text != "") {
            secao12 = Convert.ToDecimal(textSecao12.Text);
        }
        decimal? secao13 = null;
        if (textSecao13.Text != "") {
            secao13 = Convert.ToDecimal(textSecao13.Text);
        }
        decimal? secao14 = null;
        if (textSecao14.Text != "") {
            secao14 = Convert.ToDecimal(textSecao14.Text);
        }
        decimal? secao15 = null;
        if (textSecao15.Text != "") {
            secao15 = Convert.ToDecimal(textSecao15.Text);
        }
        string secao16Fator = Convert.ToString(textSecao16Fator.Text);

        decimal? secao16Variacao = null;
        if (textSecao16Variacao.Text != "") {
            secao16Variacao = Convert.ToDecimal(textSecao16Variacao.Text);
        }

        if (tabelaPerfilMensalCVM.LoadByPrimaryKey(data, idCarteira)) {
            tabelaPerfilMensalCVM.Secao3 = secao3;
            tabelaPerfilMensalCVM.Secao4 = secao4;
            tabelaPerfilMensalCVM.Secao5 = secao5;
            tabelaPerfilMensalCVM.Secao6 = secao6;
            tabelaPerfilMensalCVM.Secao8 = secao8;
            tabelaPerfilMensalCVM.Secao11PercentCota = secao11PercentCota;
            tabelaPerfilMensalCVM.Secao11Fator1 = secao11Fator1;
            tabelaPerfilMensalCVM.Secao11Cenario1 = secao11Cenario1;
            tabelaPerfilMensalCVM.Secao11Fator2 = secao11Fator2;
            tabelaPerfilMensalCVM.Secao11Cenario2 = secao11Cenario2;
            tabelaPerfilMensalCVM.Secao11Fator3 = secao11Fator3;
            tabelaPerfilMensalCVM.Secao11Cenario3 = secao11Cenario3;
            tabelaPerfilMensalCVM.Secao11Fator4 = secao11Fator4;
            tabelaPerfilMensalCVM.Secao11Cenario4 = secao11Cenario4;
            tabelaPerfilMensalCVM.Secao11Fator5 = secao11Fator5;
            tabelaPerfilMensalCVM.Secao11Cenario5 = secao11Cenario5;
            tabelaPerfilMensalCVM.Secao12 = secao12;
            tabelaPerfilMensalCVM.Secao13 = secao13;
            tabelaPerfilMensalCVM.Secao14 = secao14;
            tabelaPerfilMensalCVM.Secao15 = secao15;
            tabelaPerfilMensalCVM.Secao16Fator = secao16Fator;
            tabelaPerfilMensalCVM.Secao16Variacao = secao16Variacao;

            #region Aba 3

            tabelaPerfilMensalCVM.Secao18CnpjComitente1 = !String.IsNullOrEmpty(textCNPJ1.Text) ? textCNPJ1.Text.Trim().Replace("/", "").Replace(".", "").Replace("-", "") : "";
            tabelaPerfilMensalCVM.Secao18CnpjComitente2 = !String.IsNullOrEmpty(textCNPJ2.Text) ? textCNPJ2.Text.Trim().Replace("/", "").Replace(".", "").Replace("-", "") : "";
            tabelaPerfilMensalCVM.Secao18CnpjComitente3 = !String.IsNullOrEmpty(textCNPJ3.Text) ? textCNPJ3.Text.Trim().Replace("/", "").Replace(".", "").Replace("-", "") : "";

            if (dropParteRelacionada1.SelectedIndex > 0) {
                tabelaPerfilMensalCVM.Secao18ParteRelacionada1 = Convert.ToString(dropParteRelacionada1.SelectedItem.Value);
            }
            else {
                tabelaPerfilMensalCVM.Secao18ParteRelacionada1 = null;
            }
            if (dropParteRelacionada2.SelectedIndex > 0) {
                tabelaPerfilMensalCVM.Secao18ParteRelacionada2 = Convert.ToString(dropParteRelacionada2.SelectedItem.Value);
            }
            else {
                tabelaPerfilMensalCVM.Secao18ParteRelacionada2 = null;
            }
            if (dropParteRelacionada3.SelectedIndex > 0) {
                tabelaPerfilMensalCVM.Secao18ParteRelacionada3 = Convert.ToString(dropParteRelacionada3.SelectedItem.Value);
            }
            else {
                tabelaPerfilMensalCVM.Secao18ParteRelacionada3 = null;
            }

            tabelaPerfilMensalCVM.Secao18ValorTotal1 = null;
            if (!String.IsNullOrEmpty(textValor1.Text)) {
                tabelaPerfilMensalCVM.Secao18ValorTotal1 = Convert.ToDecimal(textValor1.Text);
            }
            tabelaPerfilMensalCVM.Secao18ValorTotal2 = null;
            if (!String.IsNullOrEmpty(textValor2.Text)) {
                tabelaPerfilMensalCVM.Secao18ValorTotal2 = Convert.ToDecimal(textValor2.Text);
            }
            tabelaPerfilMensalCVM.Secao18ValorTotal3 = null;
            if (!String.IsNullOrEmpty(textValor3.Text)) {
                tabelaPerfilMensalCVM.Secao18ValorTotal3 = Convert.ToDecimal(textValor3.Text);
            }

            if (dropVedadaCobrancaTaxa.SelectedIndex > 0) {
                tabelaPerfilMensalCVM.Secao18VedadaCobrancaTaxa = Convert.ToString(dropVedadaCobrancaTaxa.SelectedItem.Value);
            }
            else {
                tabelaPerfilMensalCVM.Secao18VedadaCobrancaTaxa = null;
            }

            tabelaPerfilMensalCVM.Secao18DataUltimaCobranca = null;
            if (!String.IsNullOrEmpty(textDataUltimaCobranca.Text)) {
                tabelaPerfilMensalCVM.Secao18DataUltimaCobranca = Convert.ToDateTime(textDataUltimaCobranca.Text);
            }

            tabelaPerfilMensalCVM.Secao18ValorUltimaCota = null;
            if (!String.IsNullOrEmpty(textValorUtimaCota.Text)) {
                tabelaPerfilMensalCVM.Secao18ValorUltimaCota = Convert.ToDecimal(textValorUtimaCota.Text);
            }
            #endregion

            tabelaPerfilMensalCVM.Save();

            #region Log do Processo
            HistoricoLog historicoLog = new HistoricoLog();
            historicoLog.InsereHistoricoLog(DateTime.Now,
                                            DateTime.Now,
                                            "Cadastro de TabelaPerfilMensalCVM - Operacao: Update TabelaPerfilMensalCVM: Carteira: " + idCarteira.ToString() +
                                                                        "- Data" + data.ToShortDateString() + UtilitarioWeb.ToString(tabelaPerfilMensalCVM),
                                            HttpContext.Current.User.Identity.Name,
                                            UtilitarioWeb.GetIP(Request),
                                            "",
                                            HistoricoLogOrigem.Outros);
            #endregion
        }

        e.Cancel = true;
        gridCadastro.CancelEdit();
        gridCadastro.DataBind();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_RowInserting(object sender, DevExpress.Web.Data.ASPxDataInsertingEventArgs e) {
        TabelaPerfilMensalCVM tabelaPerfilMensalCVM = new TabelaPerfilMensalCVM();

        ASPxPageControl pageControl = gridCadastro.FindEditFormTemplateControl("tabCadastro") as ASPxPageControl;

        ASPxDateEdit textData = pageControl.FindControl("textData") as ASPxDateEdit;
        ASPxSpinEdit btnEditCodigoCarteira = pageControl.FindControl("btnEditCodigoCarteira") as ASPxSpinEdit;

        TextBox textSecao3 = pageControl.FindControl("textSecao3") as TextBox;
        TextBox textSecao4 = pageControl.FindControl("textSecao4") as TextBox;
        ASPxSpinEdit textSecao5 = pageControl.FindControl("textSecao5") as ASPxSpinEdit;
        ASPxComboBox dropSecao6 = pageControl.FindControl("dropSecao6") as ASPxComboBox;
        TextBox textSecao8 = pageControl.FindControl("textSecao8") as TextBox;
        ASPxSpinEdit textSecao11PercentCota = pageControl.FindControl("textSecao11PercentCota") as ASPxSpinEdit;
        TextBox textSecao11Fator1 = pageControl.FindControl("textSecao11Fator1") as TextBox;
        TextBox textSecao11Cenario1 = pageControl.FindControl("textSecao11Cenario1") as TextBox;
        TextBox textSecao11Fator2 = pageControl.FindControl("textSecao11Fator2") as TextBox;
        TextBox textSecao11Cenario2 = pageControl.FindControl("textSecao11Cenario2") as TextBox;
        TextBox textSecao11Fator3 = pageControl.FindControl("textSecao11Fator3") as TextBox;
        TextBox textSecao11Cenario3 = pageControl.FindControl("textSecao11Cenario3") as TextBox;
        TextBox textSecao11Fator4 = pageControl.FindControl("textSecao11Fator4") as TextBox;
        TextBox textSecao11Cenario4 = pageControl.FindControl("textSecao11Cenario4") as TextBox;
        TextBox textSecao11Fator5 = pageControl.FindControl("textSecao11Fator5") as TextBox;
        TextBox textSecao11Cenario5 = pageControl.FindControl("textSecao11Cenario5") as TextBox;
        ASPxSpinEdit textSecao12 = pageControl.FindControl("textSecao12") as ASPxSpinEdit;
        ASPxSpinEdit textSecao13 = pageControl.FindControl("textSecao13") as ASPxSpinEdit;
        ASPxSpinEdit textSecao14 = pageControl.FindControl("textSecao14") as ASPxSpinEdit;
        ASPxSpinEdit textSecao15 = pageControl.FindControl("textSecao15") as ASPxSpinEdit;
        TextBox textSecao16Fator = pageControl.FindControl("textSecao16Fator") as TextBox;
        ASPxSpinEdit textSecao16Variacao = pageControl.FindControl("textSecao16Variacao") as ASPxSpinEdit;

        #region Aba 3
        ASPxTextBox textCNPJ1 = pageControl.FindControl("textCNPJ1") as ASPxTextBox;
        ASPxTextBox textCNPJ2 = pageControl.FindControl("textCNPJ2") as ASPxTextBox;
        ASPxTextBox textCNPJ3 = pageControl.FindControl("textCNPJ3") as ASPxTextBox;
        //
        ASPxComboBox dropParteRelacionada1 = pageControl.FindControl("dropParteRelacionada1") as ASPxComboBox;
        ASPxComboBox dropParteRelacionada2 = pageControl.FindControl("dropParteRelacionada2") as ASPxComboBox;
        ASPxComboBox dropParteRelacionada3 = pageControl.FindControl("dropParteRelacionada3") as ASPxComboBox;
        //
        ASPxSpinEdit textValor1 = pageControl.FindControl("textValor1") as ASPxSpinEdit;
        ASPxSpinEdit textValor2 = pageControl.FindControl("textValor1") as ASPxSpinEdit;
        ASPxSpinEdit textValor3 = pageControl.FindControl("textValor1") as ASPxSpinEdit;
        //
        ASPxComboBox dropVedadaCobrancaTaxa = pageControl.FindControl("dropVedadaCobrancaTaxa") as ASPxComboBox;
        ASPxDateEdit textDataUltimaCobranca = pageControl.FindControl("textDataUltimaCobranca") as ASPxDateEdit;
        ASPxSpinEdit textValorUtimaCota = pageControl.FindControl("textValorUtimaCota") as ASPxSpinEdit;
        //
        #endregion

        DateTime data = Convert.ToDateTime(textData.Text);
        int idCarteira = Convert.ToInt32(btnEditCodigoCarteira.Text);

        string secao3 = Convert.ToString(textSecao3.Text);
        string secao4 = Convert.ToString(textSecao4.Text);
        decimal? secao5 = null;
        if (textSecao5.Text != "") {
            secao5 = Convert.ToDecimal(textSecao5.Text);
        }

        byte? secao6 = null;
        if (dropSecao6.SelectedIndex != -1) {
            secao6 = Convert.ToByte(dropSecao6.SelectedItem.Value);
        }

        string secao8 = Convert.ToString(textSecao8.Text);
        decimal? secao11PercentCota = null;
        if (textSecao11PercentCota.Text != "") {
            secao11PercentCota = Convert.ToDecimal(textSecao11PercentCota.Text);
        }

        string secao11Fator1 = Convert.ToString(textSecao11Fator1.Text);
        string secao11Cenario1 = Convert.ToString(textSecao11Cenario1.Text);
        string secao11Fator2 = Convert.ToString(textSecao11Fator2.Text);
        string secao11Cenario2 = Convert.ToString(textSecao11Cenario2.Text);
        string secao11Fator3 = Convert.ToString(textSecao11Fator3.Text);
        string secao11Cenario3 = Convert.ToString(textSecao11Cenario3.Text);
        string secao11Fator4 = Convert.ToString(textSecao11Fator4.Text);
        string secao11Cenario4 = Convert.ToString(textSecao11Cenario4.Text);
        string secao11Fator5 = Convert.ToString(textSecao11Fator5.Text);
        string secao11Cenario5 = Convert.ToString(textSecao11Cenario5.Text);

        decimal? secao12 = null;
        if (textSecao12.Text != "") {
            secao12 = Convert.ToDecimal(textSecao12.Text);
        }
        decimal? secao13 = null;
        if (textSecao13.Text != "") {
            secao13 = Convert.ToDecimal(textSecao13.Text);
        }
        decimal? secao14 = null;
        if (textSecao14.Text != "") {
            secao14 = Convert.ToDecimal(textSecao14.Text);
        }
        decimal? secao15 = null;
        if (textSecao15.Text != "") {
            secao15 = Convert.ToDecimal(textSecao15.Text);
        }
        string secao16Fator = Convert.ToString(textSecao16Fator.Text);

        decimal? secao16Variacao = null;
        if (textSecao16Variacao.Text != "") {
            secao16Variacao = Convert.ToDecimal(textSecao16Variacao.Text);
        }

        tabelaPerfilMensalCVM.Data = data;
        tabelaPerfilMensalCVM.IdCarteira = idCarteira;

        tabelaPerfilMensalCVM.Secao3 = secao3;
        tabelaPerfilMensalCVM.Secao4 = secao4;
        tabelaPerfilMensalCVM.Secao5 = secao5;
        tabelaPerfilMensalCVM.Secao6 = secao6;
        tabelaPerfilMensalCVM.Secao8 = secao8;
        tabelaPerfilMensalCVM.Secao7 = 0;
        tabelaPerfilMensalCVM.Secao11PercentCota = secao11PercentCota;
        tabelaPerfilMensalCVM.Secao11Fator1 = secao11Fator1;
        tabelaPerfilMensalCVM.Secao11Cenario1 = secao11Cenario1;
        tabelaPerfilMensalCVM.Secao11Fator2 = secao11Fator2;
        tabelaPerfilMensalCVM.Secao11Cenario2 = secao11Cenario2;
        tabelaPerfilMensalCVM.Secao11Fator3 = secao11Fator3;
        tabelaPerfilMensalCVM.Secao11Cenario3 = secao11Cenario3;
        tabelaPerfilMensalCVM.Secao11Fator4 = secao11Fator4;
        tabelaPerfilMensalCVM.Secao11Cenario4 = secao11Cenario4;
        tabelaPerfilMensalCVM.Secao11Fator5 = secao11Fator5;
        tabelaPerfilMensalCVM.Secao11Cenario5 = secao11Cenario5;
        tabelaPerfilMensalCVM.Secao12 = secao12;
        tabelaPerfilMensalCVM.Secao13 = secao13;
        tabelaPerfilMensalCVM.Secao14 = secao14;
        tabelaPerfilMensalCVM.Secao15 = secao15;
        tabelaPerfilMensalCVM.Secao16Fator = secao16Fator;
        tabelaPerfilMensalCVM.Secao16Variacao = secao16Variacao;

        #region Aba 3
        tabelaPerfilMensalCVM.Secao18CnpjComitente1 = !String.IsNullOrEmpty(textCNPJ1.Text) ? textCNPJ1.Text.Trim().Replace("/","").Replace(".","").Replace("-","") : "";
        tabelaPerfilMensalCVM.Secao18CnpjComitente2 = !String.IsNullOrEmpty(textCNPJ2.Text) ? textCNPJ2.Text.Trim().Replace("/", "").Replace(".", "").Replace("-", "") : "";
        tabelaPerfilMensalCVM.Secao18CnpjComitente3 = !String.IsNullOrEmpty(textCNPJ3.Text) ? textCNPJ3.Text.Trim().Replace("/", "").Replace(".", "").Replace("-", "") : "";

        if (dropParteRelacionada1.SelectedIndex > 0) {
            tabelaPerfilMensalCVM.Secao18ParteRelacionada1 = Convert.ToString(dropParteRelacionada1.SelectedItem.Value);
        }
        else {
            tabelaPerfilMensalCVM.Secao18ParteRelacionada1 = null;
        }
        if (dropParteRelacionada2.SelectedIndex > 0) {
            tabelaPerfilMensalCVM.Secao18ParteRelacionada2 = Convert.ToString(dropParteRelacionada2.SelectedItem.Value);
        }
        else {
            tabelaPerfilMensalCVM.Secao18ParteRelacionada2 = null;
        }
        if (dropParteRelacionada3.SelectedIndex > 0) {
            tabelaPerfilMensalCVM.Secao18ParteRelacionada3 = Convert.ToString(dropParteRelacionada3.SelectedItem.Value);
        }
        else {
            tabelaPerfilMensalCVM.Secao18ParteRelacionada3 = null;
        }

        tabelaPerfilMensalCVM.Secao18ValorTotal1 = null;
        if (!String.IsNullOrEmpty(textValor1.Text)) {
            tabelaPerfilMensalCVM.Secao18ValorTotal1 = Convert.ToDecimal(textValor1.Text);
        }
        tabelaPerfilMensalCVM.Secao18ValorTotal2 = null;
        if (!String.IsNullOrEmpty(textValor2.Text)) {
            tabelaPerfilMensalCVM.Secao18ValorTotal2 = Convert.ToDecimal(textValor2.Text);
        }
        tabelaPerfilMensalCVM.Secao18ValorTotal3 = null;
        if (!String.IsNullOrEmpty(textValor3.Text)) {
            tabelaPerfilMensalCVM.Secao18ValorTotal3 = Convert.ToDecimal(textValor3.Text);
        }

        if (dropVedadaCobrancaTaxa.SelectedIndex > 0) {
            tabelaPerfilMensalCVM.Secao18VedadaCobrancaTaxa = Convert.ToString(dropVedadaCobrancaTaxa.SelectedItem.Value);
        }
        else {
            tabelaPerfilMensalCVM.Secao18VedadaCobrancaTaxa = null;
        }

        tabelaPerfilMensalCVM.Secao18DataUltimaCobranca = null;
        if (!String.IsNullOrEmpty(textDataUltimaCobranca.Text)) {
            tabelaPerfilMensalCVM.Secao18DataUltimaCobranca = Convert.ToDateTime(textDataUltimaCobranca.Text);
        }

        tabelaPerfilMensalCVM.Secao18ValorUltimaCota = null;
        if (!String.IsNullOrEmpty(textValorUtimaCota.Text)) {
            tabelaPerfilMensalCVM.Secao18ValorUltimaCota = Convert.ToDecimal(textValorUtimaCota.Text);
        }
        #endregion

        tabelaPerfilMensalCVM.Save();

        #region Log do Processo
        HistoricoLog historicoLog = new HistoricoLog();
        historicoLog.InsereHistoricoLog(DateTime.Now,
                                        DateTime.Now,
                                        "Cadastro de TabelaPerfilMensalCVM - Operacao: Insert TabelaPerfilMensalCVM: " + UtilitarioWeb.ToString(tabelaPerfilMensalCVM),
                                        HttpContext.Current.User.Identity.Name,
                                        UtilitarioWeb.GetIP(Request),
                                        "",
                                        HistoricoLogOrigem.Outros);
        #endregion

        e.Cancel = true;
        gridCadastro.CancelEdit();
        gridCadastro.DataBind();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_CustomUnboundColumnData(object sender, DevExpress.Web.ASPxGridViewColumnDataEventArgs e) {
        if (e.Column.FieldName == "CompositeKey") {
            //Compose a primary key value
            string data = Convert.ToString(e.GetListSourceFieldValue(TabelaPerfilMensalCVMMetadata.ColumnNames.Data));
            int idCarteira = Convert.ToInt32(e.GetListSourceFieldValue(TabelaPerfilMensalCVMMetadata.ColumnNames.IdCarteira));
            e.Value = data + "|" + idCarteira.ToString();
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_CustomCallback(object sender, DevExpress.Web.ASPxGridViewCustomCallbackEventArgs e) {
        if (e.Parameters == "btnDelete") {
            List<object> keyValuesData = gridCadastro.GetSelectedFieldValues(TabelaPerfilMensalCVMMetadata.ColumnNames.Data);
            List<object> keyValuesIdCarteira = gridCadastro.GetSelectedFieldValues(TabelaPerfilMensalCVMMetadata.ColumnNames.IdCarteira);
            for (int i = 0; i < keyValuesData.Count; i++) {
                DateTime data = Convert.ToDateTime(keyValuesData[i]);
                int idCarteira = Convert.ToInt32(keyValuesIdCarteira[i]);

                TabelaPerfilMensalCVM tabelaPerfilMensalCVM = new TabelaPerfilMensalCVM();
                if (tabelaPerfilMensalCVM.LoadByPrimaryKey(data, idCarteira)) {
                    TabelaPerfilMensalCVM tabelaPerfilMensalCVMClone = (TabelaPerfilMensalCVM)Utilitario.Clone(tabelaPerfilMensalCVM);
                    //                    
                    tabelaPerfilMensalCVM.MarkAsDeleted();
                    tabelaPerfilMensalCVM.Save();

                    #region Log do Processo
                    HistoricoLog historicoLog = new HistoricoLog();
                    historicoLog.InsereHistoricoLog(DateTime.Now,
                                                    DateTime.Now,
                                                    "Cadastro de TabelaPerfilMensalCVM - Operacao: Delete TabelaPerfilMensalCVM: Carteira: " + idCarteira.ToString() +
                                                                        "- Data" + data.ToShortDateString() + UtilitarioWeb.ToString(tabelaPerfilMensalCVMClone),
                                                    HttpContext.Current.User.Identity.Name,
                                                    UtilitarioWeb.GetIP(Request),
                                                    "",
                                                    HistoricoLogOrigem.Outros);
                    #endregion
                }
            }
        }

        gridCadastro.DataBind();
        gridCadastro.Selection.UnselectAll();
        gridCadastro.CancelEdit();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    new protected void gridCadastro_CustomJSProperties(object sender, ASPxGridViewClientJSPropertiesEventArgs e) {
        base.gridCadastro_CustomJSProperties(sender, e);

        ASPxGridView gridView = sender as ASPxGridView;
        if (gridView.IsEditing) {
            ASPxPageControl pageControl = gridCadastro.FindEditFormTemplateControl("tabCadastro") as ASPxPageControl;

            ASPxDateEdit textData = pageControl.FindControl("textData") as ASPxDateEdit;
            e.Properties["cpTextData"] = textData.ClientID;

            ASPxSpinEdit btnEditCodigoCarteira = pageControl.FindControl("btnEditCodigoCarteira") as ASPxSpinEdit;
            e.Properties["cpBtnEditCodigoCarteira"] = btnEditCodigoCarteira.ClientID;
        }
    }

}