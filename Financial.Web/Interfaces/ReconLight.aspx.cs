﻿using System.Linq;
using DevExpress.Utils;
using DevExpress.Web;
using Financial.BMF;
using Financial.Common;
using Financial.CRM;
using Financial.Export;
using Financial.Fundo;
using Financial.Interfaces.Import.Fundo;
using Financial.Investidor;
using Financial.ReconLight;
using Financial.RendaFixa;
using Financial.RendaFixa.Enums;
using Financial.Security;
using Financial.Web.Common;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.IO;
using System.Text;
using System.Web;
using System.Web.UI.WebControls;
using System.Xml;

/// <summary>
/// 
/// </summary>
public partial class CadastrosBasicos_ReconLight : CadastroBasePage
{
    #region Fields
    private Atlas.Link.Settings.AtlasLinkSection _config;
    #endregion

    #region page load event
    /// <summary>
    /// Usado quando a pagina de cadastro está dentro de um tabPage
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    new protected void Page_Load(object sender, EventArgs e)
    {
        #region Fields
        _config = (Atlas.Link.Settings.AtlasLinkSection)ConfigurationManager.GetSection("atlasLinkGroup/atlasLink");
        #endregion

        var possuiReconLight = _config.ReconLight.PossuiInterface;

        if (!possuiReconLight)
            Server.Transfer("~/default.aspx", false);

        //passar o valor do para um hiddenfield para que o javascript possa utilizar
        timeoutAtualizacaoRecon.Value = _config.ReconLight.TimeoutAtualizacao.ToString();

        //gridcadastro
        HasFiltro = false;
        HasPopupCliente = false;
        HasPopupAtivoBolsa = false;
        HasPanelFieldsLoading = false;
        AllowDelete = false;

        base.Page_Load(sender, e);

        if (!Page.IsPostBack)
        {
        }

        InicializaGridFila();

        int idRecon = 0;
        if (Request.QueryString["Download"] == "true" && int.TryParse(Request.QueryString["IdRecon"], out idRecon))
            Download(idRecon);

    }
    #endregion

    #region Structures
    /// <summary>
    /// CarteiraXml
    /// </summary>
    public struct CarteiraXml
    {
        #region Properties
        public String Cnpj;
        public String CodigoIsin;
        public String Nome;
        public Int32? IdCarteira;
        #endregion

        #region constructor
        /// <summary>
        /// Initializes a new instance of the <see cref="CarteiraXml"/> struct.
        /// </summary>
        /// <param name="codigoIsin">The codigo isin.</param>
        /// <param name="cnpj">The CNPJ.</param>
        /// <param name="nome">The nome.</param>
        /// <param name="idCarteira">The identifier carteira.</param>
        public CarteiraXml(String codigoIsin, String cnpj, String nome, Int32? idCarteira)
        {
            Cnpj = cnpj;
            CodigoIsin = codigoIsin;
            Nome = nome;
            IdCarteira = idCarteira;
        }
        #endregion
    }
    #endregion

    #region Enums
    /// <summary>
    /// TipoAgente
    /// </summary>
    private enum TipoAgente
    {
        Administrador = 1,
        Gestor = 2,
        Custodiante = 3
    }
    #endregion

    #region Metodos de validacao

    /// <summary>
    /// Validas the pessoas.
    /// </summary>
    /// <param name="idsCliente">The ids cliente.</param>
    /// <returns></returns>
    private String ValidaPessoas(List<int> idsCliente)
    {
        var clientes = new ClienteCollection();
        var clienteQuery = new ClienteQuery("cli");
        var pessoaQuery = new PessoaQuery("pes");

        clienteQuery.es.Distinct = true;
        clienteQuery.Select(clienteQuery.IdCliente, pessoaQuery.IdPessoa, pessoaQuery.Cpfcnpj);
        clienteQuery.LeftJoin(pessoaQuery).On(clienteQuery.IdPessoa.Equal(pessoaQuery.IdPessoa));

        if (idsCliente.Count > 0)
            clienteQuery.Where(clienteQuery.IdCliente.In(idsCliente));

        clienteQuery.Where((pessoaQuery.IdPessoa.IsNull() | pessoaQuery.Cpfcnpj.IsNull() | pessoaQuery.Cpfcnpj == ""));
        clienteQuery.es.Distinct = true;
        clientes.Load(clienteQuery);

        if (clientes.Count > 0)
        {
            var s = new StringBuilder();
            s.AppendLine(String.Format("|{0,20}|{1,20}|",
                "IdPessoa",
                "CNPJ"
                ));
            foreach (var cliente in clientes)
            {
                s.AppendLine(String.Format("|{0,20}|{1,20}|",
                   (cliente.GetColumn(pessoaQuery.IdPessoa) ?? "").ToString().PadLeft(20, '.'),
                   (cliente.GetColumn(pessoaQuery.Cpfcnpj) ?? "").ToString().PadLeft(20, '.')));
            }

            return Environment.NewLine + "As seguintes pessoas estão com problemas cadastrais:" + Environment.NewLine + s;
        }
        return String.Empty;
    }

    /// <summary>
    /// Validas the agentes.
    /// </summary>
    /// <param name="idsCliente">The ids cliente.</param>
    /// <param name="tipo">The tipo.</param>
    /// <returns></returns>
    private String ValidaAgentes(List<int> idsCliente, TipoAgente tipo)
    {
        var carteiras = new CarteiraCollection();
        var car = new CarteiraQuery("car");
        var adm = new AgenteMercadoQuery("adm");

        switch (tipo)
        {
            case TipoAgente.Administrador:
                car.LeftJoin(adm).On(adm.IdAgente.Equal(car.IdAgenteAdministrador));
                break;
            case TipoAgente.Gestor:
                car.LeftJoin(adm).On(adm.IdAgente.Equal(car.IdAgenteGestor));
                break;
            case TipoAgente.Custodiante:
                car.LeftJoin(adm).On(adm.IdAgente.Equal(car.IdAgenteCustodiante));
                break;
        }

        car.Select(adm.IdAgente, adm.Nome, adm.Cnpj);

        if (idsCliente.Count > 0)
            car.Where(car.IdCarteira.In(idsCliente));

        car.Where(adm.Cnpj.IsNull() | adm.Cnpj == "");
        car.es.Distinct = true;
        carteiras.Load(car);

        if (carteiras.Count > 0)
        {
            var s = new StringBuilder();
            s.AppendLine(String.Format("|{0,20}|{1,20}|{2,20}|",
                    "Id Agente",
                    "Nome Agente",
                    "CNPJ Agente"));

            foreach (Carteira carteira in carteiras)
            {
                s.AppendLine(String.Format("|{0,20}|{1,20}|{2,20}|",
                    (carteira.GetColumn(adm.IdAgente) ?? "").ToString().PadLeft(20, '.'),
                    (carteira.GetColumn(adm.Nome) ?? "").ToString().PadLeft(20, '.'),
                    (carteira.GetColumn(adm.Cnpj) ?? "").ToString().PadLeft(20, '.')
                    ));
            }
            return Environment.NewLine + "Os seguintes agentes estão com problemas cadastrais:" + Environment.NewLine + s;
        }
        return String.Empty;
    }

    /// <summary>
    /// Validas the emissores renda fixa.
    /// </summary>
    /// <param name="idsClientes">The ids clientes.</param>
    /// <returns></returns>
    private String ValidaEmissoresRendaFixa(List<int> idsClientes)
    {
        var posicoes = new PosicaoRendaFixaCollection();
        var posRf = new PosicaoRendaFixaQuery("posRf");
        var titRf = new TituloRendaFixaQuery("titRf");
        var emiRf = new EmissorQuery("emiRf");
        var papRf = new PapelRendaFixaQuery("papRf");

        posRf.Select(emiRf.IdEmissor, emiRf.Nome, emiRf.Cnpj);
        posRf.LeftJoin(titRf).On(titRf.IdTitulo.Equal(posRf.IdTitulo));
        posRf.LeftJoin(emiRf).On(emiRf.IdEmissor.Equal(titRf.IdEmissor));
        posRf.LeftJoin(papRf).On(papRf.IdPapel.Equal(titRf.IdPapel));
        if (idsClientes.Count > 0)
            posRf.Where(posRf.IdCliente.In(idsClientes));

        posRf.Where(
              papRf.IdPapel.Equal((byte)TipoPapelTitulo.Privado)
            & papRf.Classe.NotEqual((int)ClasseRendaFixa.Debenture)
            & posRf.Quantidade.NotEqual(0)
            & (emiRf.Cnpj.IsNull() | emiRf.Cnpj == ""));
        posRf.es.Distinct = true;
        posicoes.Load(posRf);

        var posicoesH = new PosicaoRendaFixaHistoricoCollection();
        var posRfH = new PosicaoRendaFixaHistoricoQuery("posRfH");

        posRfH.Select(emiRf.IdEmissor, emiRf.Nome, emiRf.Cnpj);
        posRfH.LeftJoin(titRf).On(posRfH.IdTitulo.Equal(posRfH.IdTitulo));
        posRfH.LeftJoin(emiRf).On(emiRf.IdEmissor.Equal(titRf.IdEmissor));
        posRfH.LeftJoin(papRf).On(papRf.IdPapel.Equal(titRf.IdPapel));

        if (idsClientes.Count > 0)
            posRfH.Where(posRfH.IdCliente.In(idsClientes));

        posRfH.Where(
             papRf.IdPapel.Equal((byte)TipoPapelTitulo.Privado)
            & papRf.Classe.NotEqual((int)ClasseRendaFixa.Debenture)
            & posRfH.Quantidade.NotEqual(0)
            & (emiRf.Cnpj.IsNull() | emiRf.Cnpj == ""));
        posRfH.es.Distinct = true;
        posicoesH.Load(posRfH);

        var s = new StringBuilder();
        s.AppendLine(String.Format("|{0,20}|{1,20}|{2,20}|",
                "Id Emissor",
                "Nome Emissor",
                "CNPJ Emissor"
                ));

        if (posicoes.Count > 0)
        {
            foreach (var pos in posicoes)
            {
                s.AppendLine(String.Format("|{0,20}|{1,20}|{2,20}|",
                        (pos.GetColumn(emiRf.IdEmissor) ?? "").ToString().PadLeft(20, '.'),
                        (pos.GetColumn(emiRf.Nome) ?? "").ToString().PadLeft(20, '.'),
                        (pos.GetColumn(emiRf.Cnpj) ?? "").ToString().PadLeft(20, '.')
                    ));
            }
        }

        if (posicoesH.Count > 0)
        {
            foreach (var pos in posicoesH)
            {
                s.AppendLine(String.Format("|{0,20}|{1,20}|{2,20}|",
                        (pos.GetColumn(emiRf.IdEmissor) ?? "").ToString().PadLeft(20, '.'),
                        (pos.GetColumn(emiRf.Nome) ?? "").ToString().PadLeft(20, '.'),
                        (pos.GetColumn(emiRf.Cnpj) ?? "").ToString().PadLeft(20, '.')
                    ));
            }
        }

        if (posicoes.Count > 0 || posicoesH.Count > 0)
            return Environment.NewLine + "Os seguintes emissores estão com problemas cadastrais:" + Environment.NewLine + s;
        return String.Empty;
    }

    /// <summary>
    /// Validas the agentes BMF.
    /// </summary>
    /// <param name="idsClientes">The ids clientes.</param>
    /// <returns></returns>
    private String ValidaAgentesBMF(List<int> idsClientes)
    {
        var posicoes = new PosicaoBMFCollection();
        var posicoesH = new PosicaoBMFHistoricoCollection();

        var posBmf = new PosicaoBMFQuery("posBMF");
        var posBmfH = new PosicaoBMFHistoricoQuery("posBmfH");
        var ag = new AgenteMercadoQuery("ag");

        posBmf.Select(posBmf.IdAgente, ag.Cnpj);
        posBmf.LeftJoin(ag).On(ag.IdAgente.Equal(posBmf.IdAgente));

        if (idsClientes.Count > 0)
            posBmf.Where(posBmf.IdCliente.In(idsClientes));

        posBmf.Where((ag.Cnpj.IsNull() | ag.Cnpj == ""));
        posBmf.es.Distinct = true;
        posicoes.Load(posBmf);

        posBmfH.Select(posBmfH.IdAgente, ag.Cnpj);
        posBmfH.LeftJoin(ag).On(ag.IdAgente.Equal(posBmfH.IdAgente));

        if (idsClientes.Count > 0)
            posBmfH.Where(posBmfH.IdCliente.In(idsClientes));

        posBmfH.Where(ag.Cnpj.IsNull() | ag.Cnpj == "");
        posBmfH.es.Distinct = true;
        posicoesH.Load(posBmfH);

        var s = new StringBuilder();
        s.AppendLine(String.Format("|{0,20}|{1,20}|{2,20}|",
                "Id Agente",
                "Nome Agente",
                "CNPJ Agente"
                ));

        if (posicoes.Count > 0)
        {
            foreach (var pos in posicoes)
            {
                s.AppendLine(String.Format("|{0,20}|{1,20}|{2,20}|",
                        pos.IdAgente.ToString().PadLeft(20, '.'),
                        (pos.GetColumn(ag.Nome) ?? "").ToString().PadLeft(20, '.'),
                        (pos.GetColumn(ag.Cnpj) ?? "").ToString().PadLeft(20, '.')
                    ));
            }
        }

        if (posicoesH.Count > 0)
        {
            foreach (var pos in posicoesH)
            {
                s.AppendLine(String.Format("|{0,20}|{1,20}|",
                        pos.IdAgente.ToString().PadLeft(20, '.'),
                        (pos.GetColumn(ag.Nome) ?? "").ToString().PadLeft(20, '.')
                    ));
            }
        }

        if (posicoes.Count > 0 || posicoesH.Count > 0)
            return Environment.NewLine + "Os seguintes agentes estão com problemas cadastrais:" + Environment.NewLine + s;
        return String.Empty;
    }

    /// <summary>
    /// Validas the clientes.
    /// </summary>
    /// <param name="idsCliente">The ids cliente.</param>
    /// <returns></returns>
    private String ValidaClientes(List<int> idsCliente)
    {
        return ValidaPessoas(idsCliente) + ValidaAgentes(idsCliente, TipoAgente.Administrador) +
            ValidaAgentes(idsCliente, TipoAgente.Custodiante) + ValidaAgentes(idsCliente, TipoAgente.Gestor) +
            ValidaEmissoresRendaFixa(idsCliente) + ValidaAgentesBMF(idsCliente);
    }

    #endregion

    #region metodos de persistencia
    /// <summary>
    /// Adds the recon.
    /// </summary>
    /// <param name="xmlClienteStream">The XML cliente stream.</param>
    /// <param name="descricao">The descricao.</param>
    /// <param name="idUsuario">The identifier usuario.</param>
    /// <exception cref="System.Exception">
    /// Erro na leitura do xml:  + ex.Message
    /// or
    /// Existem campos necessarios para o processamento que estão nulos ou vazios no banco de dados. Para mais detalhes favor verificar a observação desta recon.
    /// or
    /// Erro ao criar xml:  + ex.Message
    /// or
    /// Erro ao salvar xml em disco:  + ex.Message
    /// or
    /// Erro na leitura do xml: Não foram encontrados clientes validos
    /// </exception>
    public void AddRecon(Stream xmlClienteStream, string descricao, int idUsuario)
    {
        var controller = new ControllerRecon();

        var recon = new Recon { DataRegistro = DateTime.Now, IdUsuario = idUsuario , Tipo = Atlas.Link.Model.Enums.Tipo.RECON.ToString()};
        recon.Save();

        xmlClienteStream.Position = 0;

        //para retirar o bom do arquivo
        var s = new StreamReader(xmlClienteStream, Encoding.UTF8);
        var xmlString = s.ReadToEnd();
        var m = new MemoryStream(Encoding.UTF8.GetBytes(xmlString));

        try
        {
            using (var fileStream = File.Create(controller.XmlCliente(recon.IdRecon.Value), (int)xmlClienteStream.Length))
                CopyStream(m, fileStream);

        }
        catch
        {
            throw;
        }

        xmlClienteStream.Position = 0;

        var x = new XMLAnbid();

        try
        {
            x.LerArquivoXML(new StreamReader(xmlClienteStream));
        }
        catch (Exception ex)
        {
            throw new Exception("Erro na leitura do xml: " + ex.Message, ex);
        }

        var header = x.Secao.GetHeader;

        var carteirasXml = new List<CarteiraXml>();

        for (var i = 0; i < header.Rows.Count; i++)
        {
            carteirasXml.Add(new CarteiraXml(
                (string)header.Rows[i].ItemArray[0],
                (string)header.Rows[i].ItemArray[1],
                (string)header.Rows[i].ItemArray[2],
                null));
        }

        var carteiraQuery = new CarteiraQuery();

        carteiraQuery.Select(carteiraQuery.IdCarteira, carteiraQuery.CodigoIsin);

        var codigosIsin = carteirasXml.FindAll(c => !string.IsNullOrEmpty(c.CodigoIsin))
        .ConvertAll(cx => cx.CodigoIsin);

        var coll = new CarteiraCollection();
        var idsCarteira = new List<int>();

        if (codigosIsin.Count > 0)
        {
            carteiraQuery.Where(carteiraQuery.CodigoIsin.In(codigosIsin));
            coll.Load(carteiraQuery);

            foreach (var c in coll)
            {
                if (c.IdCarteira.HasValue)
                {
                    idsCarteira.Add(c.IdCarteira.Value);
                    carteirasXml.RemoveAll(xml => xml.CodigoIsin == c.CodigoIsin);
                }
            }
        }

        carteiraQuery = new CarteiraQuery("Ca");
        var pessoaQuery = new PessoaQuery("p");
        var clienteQuery = new ClienteQuery("cli");

        carteiraQuery.Select(carteiraQuery.IdCarteira, pessoaQuery.Cpfcnpj);
        carteiraQuery.InnerJoin(clienteQuery).On(clienteQuery.IdCliente == carteiraQuery.IdCarteira);
        carteiraQuery.InnerJoin(pessoaQuery).On(clienteQuery.IdPessoa == pessoaQuery.IdPessoa);

        var listaCnpj = carteirasXml.FindAll(c => !string.IsNullOrEmpty(c.Cnpj))
        .ConvertAll(cx => cx.Cnpj);

        if (listaCnpj.Count > 0)
        {
            carteiraQuery.Where(pessoaQuery.Cpfcnpj.In(listaCnpj));

            coll.Load(carteiraQuery);

            foreach (var c in coll.Where(c => c.IdCarteira.HasValue))
            {
                idsCarteira.Add(c.IdCarteira.Value);
                carteirasXml.RemoveAll(
                    xml => xml.Cnpj == (c.GetColumn(PessoaMetadata.ColumnNames.Cpfcnpj) ?? "").ToString());
            }
        }

        carteiraQuery = new CarteiraQuery();
        carteiraQuery.Select(carteiraQuery.IdCarteira, carteiraQuery.Nome);
        var nomes = carteirasXml.FindAll(c => !string.IsNullOrEmpty(c.Nome))
            .ConvertAll(cx => cx.Nome);

        if (nomes.Count > 0)
        {
            carteiraQuery.Where(carteiraQuery.Nome.In(nomes));


            coll.Load(carteiraQuery);

            foreach (var c in coll.Where(c => c.IdCarteira.HasValue))
            {
                idsCarteira.Add(c.IdCarteira.Value);
                carteirasXml.RemoveAll(xml => xml.Nome == c.Nome);
            }
        }
        DateTime dataPosicao;
        try
        {
            dataPosicao = DateTime.ParseExact(header.Rows[0].ItemArray[3].ToString(), "yyyyMMdd", CultureInfo.InvariantCulture);
        }
        catch
        {
            dataPosicao = DateTime.ParseExact(header.Rows[0].ItemArray[2].ToString(), "yyyyMMdd", CultureInfo.InvariantCulture);
        }

        var erro = ValidaClientes(idsCarteira);
        if (erro != String.Empty)
        {
            recon.Cancela("Para poder executar esta recon novamente favor corrigir os problemas abaixo." + Environment.NewLine + erro);
            recon.Save();
            throw new Exception("Existem campos necessarios para o processamento que estão nulos ou vazios no banco de dados. Para mais detalhes favor verificar a observação desta recon.");
        }

        if (idsCarteira.Count > 0)
        {
            MemoryStream ms;
            try
            {
                var xmlPosicaoAnbid = new XMLPosicaoAnbid();
                string nome;
                xmlPosicaoAnbid.ExportaCarteiraXMLAnbid(dataPosicao, idsCarteira, out ms, out nome);
            }
            catch (Exception ex)
            {
                throw new Exception("Erro ao criar xml: " + ex.Message, ex);
            }

            ms.Position = 0;
            try
            {
                var xml = new XmlDocument();
                xml.Load(ms);
                foreach (XmlNode node in xml)
                {
                    if (node.NodeType == XmlNodeType.XmlDeclaration)
                    {
                        var dec = xml.CreateXmlDeclaration("1.0", null, null);
                        xml.ReplaceChild(dec, node);
                    }
                }

                ms = new MemoryStream();

                using (var fileStream = File.Create(controller.XmlPas(recon.IdRecon.Value), (int)xmlClienteStream.Length))
                    xml.Save(fileStream);
                //CopyStream(ms, fileStream);
            }
            catch (Exception ex)
            {
                throw new Exception("Erro ao salvar xml em disco: " + ex.Message, ex);
            }
        }
        else
        {
            throw new Exception("Erro na leitura do xml: Não foram encontrados clientes validos");
        }

        recon.Save();
    }
    #endregion

    #region Eventos
    //estou salvando no metodo de upload do xml pois é necessario salvar o arquivo apenas apos a criação no banco de dados
    /// <summary>
    /// Handles the RowInserting event of the gridFila control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="DevExpress.Web.Data.ASPxDataInsertingEventArgs"/> instance containing the event data.</param>
    protected void gridFila_RowInserting(object sender, DevExpress.Web.Data.ASPxDataInsertingEventArgs e)
    {
        e.Cancel = true;
        gridFila.CancelEdit();

    }

    /// <summary>
    /// Handles the RowUpdating event of the gridFila control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="DevExpress.Web.Data.ASPxDataUpdatingEventArgs"/> instance containing the event data.</param>
    protected void gridFila_RowUpdating(object sender, DevExpress.Web.Data.ASPxDataUpdatingEventArgs e)
    {
        e.Cancel = true;
        gridFila.CancelEdit();
    }

    /// <summary>
    /// Handles the HtmlRowPrepared event of the gridCadastro control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="ASPxGridViewTableRowEventArgs"/> instance containing the event data.</param>
    protected void gridCadastro_HtmlRowPrepared(object sender, ASPxGridViewTableRowEventArgs e)
    {
        if (e.RowType != GridViewRowType.Data) return;
        if (e.GetValue("DataCancelamento") is DateTime)
            e.Row.BackColor = System.Drawing.Color.FromArgb(255, 240, 240);
        else
            e.Row.BackColor = System.Drawing.Color.FromArgb(240, 255, 240);
    }

    /// <summary>
    /// Handles the HtmlRowPrepared event of the gridFila control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="ASPxGridViewTableRowEventArgs"/> instance containing the event data.</param>
    protected void gridFila_HtmlRowPrepared(object sender, ASPxGridViewTableRowEventArgs e)
    {
        if (e.RowType != GridViewRowType.Data) return;
        if (e.GetValue("DataInicioProcesso") is DateTime)
            e.Row.BackColor = System.Drawing.Color.FromArgb(255, 255, 240);
    }

    /// <summary>
    /// Handles the CustomButtonInitialize event of the gridCadastro control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="ASPxGridViewCustomButtonEventArgs"/> instance containing the event data.</param>
    protected void gridCadastro_CustomButtonInitialize(object sender, ASPxGridViewCustomButtonEventArgs e)
    {
        var view = (((System.Data.DataRowView)(((ASPxGridView)sender).GetRow(e.VisibleIndex))));
        if (view == null) return;

        if (view.Row.ItemArray[6] is DateTime)
        {
            if (e.ButtonID != "downBtn") return;
            e.Visible = DefaultBoolean.False;
        }
        else
        {
            if (e.ButtonID != "canceladoBtn") return;
            e.Visible = DefaultBoolean.False;
            e.Enabled = false;
        }
    }
    /// <summary>
    /// Handles the FileUploadComplete event of the uplXmlAnbima control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="FileUploadCompleteEventArgs"/> instance containing the event data.</param>
    protected void uplXmlAnbima_FileUploadComplete(object sender, FileUploadCompleteEventArgs e)
    {
        var ms = new MemoryStream();
        CopyStream(e.UploadedFile.FileContent, ms);

        var usuario = new Usuario();
        usuario.Query.Where(usuario.Query.Login.Equal(HttpContext.Current.User.Identity.Name));

        //ASPxTextBox textTaxaTermo = gridFila.FindEditFormTemplateControl("textObservacao") as ASPxTextBox;
        try
        {
            if (!usuario.Query.Load()) return;
            AddRecon(ms, "", usuario.IdUsuario.Value);
            gridFila.DataBind();
            //gridCadastro.DataBind();
        }
        catch (Exception ex)
        {
            e.ErrorText = ex.Message;
        }
    }
    /// <summary>
    /// Handles the CustomCallback event of the gridFila control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="ASPxGridViewCustomCallbackEventArgs"/> instance containing the event data.</param>
    protected void gridFila_CustomCallback(object sender, ASPxGridViewCustomCallbackEventArgs e)
    {
        var r = new Recon();
        r.Query.Where(r.Query.IdRecon.Equal(Int32.Parse(e.Parameters)));
        r.Query.Load();

        r.Cancela("Recon cancelada pelo usuario");
        r.Save();

        gridFila.DataBind();
    }
    /// <summary>
    /// Handles the Load event of the uplXmlAnbima control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
    protected void uplXmlAnbima_Load(object sender, EventArgs e)
    {
        if (!gridFila.IsNewRowEditing)
        {
            var asPxUploadControl = sender as ASPxUploadControl;
            if (asPxUploadControl != null) asPxUploadControl.Enabled = false;
        }
    }

    /// <summary>
    /// Handles the Load event of the btnOk control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
    protected void btnOk_Load(object sender, EventArgs e)
    {
        var btnOk = (LinkButton)sender;
        btnOk.OnClientClick = gridFila.IsNewRowEditing ? "uplXmlAnbima.UploadFile('testeDoTeste'); return  false;" : "gridFila.UpdateEdit(); return false;";
    }

    #endregion

    #region metodos estaticos
    /// <summary>
    /// Copies the stream.
    /// </summary>
    /// <param name="input">The input.</param>
    /// <param name="output">The output.</param>
    public static void CopyStream(Stream input, Stream output)
    {
       var buffer = new byte[16 * 1024];
        int read;
        while ((read = input.Read(buffer, 0, buffer.Length)) > 0)
        {
            output.Write(buffer, 0, read);
        }
    }
    #endregion

    #region Member
    /// <summary>
    /// Downloads the specified identifier recon.
    /// </summary>
    /// <param name="idRecon">The identifier recon.</param>
    protected void Download(Int32 idRecon)
    {
        var controller = new ControllerRecon();
        var fileInfo = new FileInfo(controller.XlsResultado(idRecon));
        if (fileInfo.Exists)
        {
            Response.Clear();
            Response.AddHeader("Content-Disposition", "attachment; filename=" + fileInfo.Name);
            Response.AddHeader("Content-Length", fileInfo.Length.ToString());
            Response.ContentType = "application/octet-stream";
            Response.Flush();
            Response.TransmitFile(fileInfo.FullName);
            Response.End();
        }
        else
        {
            var coll = new ReconCollection();
            var reconQuery = new ReconQuery();
            coll.Load(reconQuery);
            var r = coll.FindByPrimaryKey(idRecon);
            r.Cancela("Arquivo resultado não foi encontrado.");

            coll.Save();
            Response.Write("<script typ>alert('teste');</script>");
            Response.Flush();
        }
    }
    /// <summary>
    /// Inicializas the grid fila.
    /// </summary>
    protected void InicializaGridFila()
    {
        var gridFila = this.FindControl("gridFila") as ASPxGridView;
        #region Properties básicas
        gridFila.ClientInstanceName = "gridFila";
        gridFila.AutoGenerateColumns = false;
        gridFila.Width = Unit.Percentage(100);
        gridFila.SettingsBehavior.AllowSort = false;
        gridFila.EnableViewState = true;
        gridFila.EnableCallBacks = true;


        //((GridViewCommandColumn)gridFila.Columns[0]).Width = Unit.Percentage(5);
        gridFila.SettingsText.PopupEditFormCaption = "";
        gridFila.SettingsEditing.Mode = GridViewEditingMode.PopupEditForm;
        gridFila.SettingsEditing.PopupEditFormModal = true;
        gridFila.SettingsEditing.PopupEditFormHorizontalAlign = PopupHorizontalAlign.WindowCenter;
        gridFila.SettingsEditing.PopupEditFormVerticalAlign = PopupVerticalAlign.WindowCenter;

        //Pager
        gridFila.SettingsPager.PageSize = 30;

        //Styles
        gridFila.Styles.Header.SortingImageSpacing = Unit.Pixel(5);
        gridFila.Styles.AlternatingRow.Enabled = DefaultBoolean.True;
        gridFila.Styles.Cell.Wrap = DefaultBoolean.True;
        gridFila.Styles.CommandColumn.Cursor = "hand";
        #endregion

    }

    #endregion

    #region data source
    /// <summary>
    /// Handles the esSelect event of the EsDSRecon control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="EntitySpaces.Web.esDataSourceSelectEventArgs"/> instance containing the event data.</param>
    protected void EsDSRecon_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        var coll = new ReconCollection();
        var reconQuery = new ReconQuery("r");
        var usuarioQuery = new UsuarioQuery("u");
        reconQuery.LeftJoin(usuarioQuery).On(reconQuery.IdUsuario == usuarioQuery.IdUsuario);
        reconQuery.Where(reconQuery.Tipo == Atlas.Link.Model.Enums.Tipo.RECON);
        reconQuery.Where(reconQuery.Or(reconQuery.DataFimProcesso.IsNotNull(), reconQuery.DataCancelamento.IsNotNull()));

        reconQuery.Select(reconQuery, usuarioQuery.Nome.As("Usuario"));
        reconQuery.OrderBy(reconQuery.IdRecon.Descending);
        coll.Load(reconQuery);
        e.Collection = coll;
    }

    /// <summary>
    /// Handles the esSelect event of the EsDSFila control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="EntitySpaces.Web.esDataSourceSelectEventArgs"/> instance containing the event data.</param>
    protected void EsDSFila_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        var coll = new ReconCollection();
        var reconQuery = new ReconQuery("r");
        var usuarioQuery = new UsuarioQuery("u");
        reconQuery.LeftJoin(usuarioQuery).On(reconQuery.IdUsuario == usuarioQuery.IdUsuario);
        reconQuery.Where(reconQuery.Tipo == Atlas.Link.Model.Enums.Tipo.RECON);
        reconQuery.Where(reconQuery.DataFimProcesso.IsNull(), reconQuery.DataCancelamento.IsNull());
        reconQuery.Select(reconQuery, usuarioQuery.Nome.As("Usuario"));
        reconQuery.OrderBy(reconQuery.IdRecon.Ascending);
        coll.Load(reconQuery);

        e.Collection = coll;
    }
    #endregion

    #region callbacks

    /// <summary>
    /// Handles the Callback event of the callbackDownload control.
    /// </summary>
    /// <param name="source">The source of the event.</param>
    /// <param name="e">The <see cref="DevExpress.Web.CallbackEventArgs"/> instance containing the event data.</param>
    protected void callbackDownload_Callback(object source, CallbackEventArgs e)
    {
        Int32 idRecon = 0;
        if (Int32.TryParse(((DevExpress.Web.CallbackEventArgsBase)(e)).Parameter, out idRecon))
        {
            var controller = new ControllerRecon();
            var fileInfo = new FileInfo(controller.XlsResultado(idRecon));

            if (fileInfo.Exists)
                e.Result = "PossuiArquivo";
        }
    }

    /// <summary>
    /// Handles the Callback event of the callbackAtualiza control.
    /// </summary>
    /// <param name="source">The source of the event.</param>
    /// <param name="e">The <see cref="DevExpress.Web.CallbackEventArgs"/> instance containing the event data.</param>
    protected void callbackAtualiza_Callback(object source, CallbackEventArgs e)
    {
        var controller = new ControllerRecon();
        e.Result = controller.Atualiza().ToString();


        gridFila.CancelEdit();
    }
    #endregion

}
