using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using HtmlAgilityPack;

public partial class Help : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        string topic = Request.Params["topic"];
        string app = Request.Params["app"];

        string topicURL = String.Format("{0}/{1}.htm", app, topic);
        string topicFile = HttpContext.Current.Server.MapPath(topicURL);

        this.Header.Controls.AddAt(0, new BaseTag(Request, app));

        HtmlDocument htmlDocument = new HtmlDocument();
        htmlDocument.Load(topicFile, System.Text.Encoding.UTF8);

        HtmlNode htmlDivContent = htmlDocument.DocumentNode.SelectNodes("//div[@class='description_on_page']")[0];

        content.InnerHtml = htmlDivContent.WriteTo();
    }
}

public class BaseTag : HtmlControl
{
    public BaseTag(HttpRequest request, string app)
        : base("base")
    {
        base.Attributes.Add("href",
            request.Url.Scheme + "://" +
            request.Url.Authority +
            request.Path.Replace("/help.aspx", "") + "/" + app + "/");
    }
}