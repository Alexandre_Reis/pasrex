﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Collections.Generic;
using System.Text;
using System.Drawing;

using Financial.Common;
using Financial.Util;
using Financial.Investidor;
using Financial.ContaCorrente;
using Financial.Fundo;
using Financial.InvestidorCotista;
using Financial.Bolsa;
using Financial.BMF;

using Financial.Investidor.Enums;

using DevExpress.Web;
using Financial.Web.Common;
using Financial.Security;
using Financial.Web.Util;
using Financial.Security.Enums;
using Financial.CRM;
using System.Text.RegularExpressions;
using EntitySpaces.Interfaces;

public partial class CadastrosBasicos_InformacoesComplementares : CadastroBasePage
{
    new protected void Page_Load(object sender, EventArgs e) {

        this.HasPopupCarteira = true;

        base.Page_Load(sender, e);    
    }

    #region Load dos esDataSources (grid principal, eventuais grids de popup e dropdownlists)
    protected void EsDSInfoComplementar_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e) {
        Usuario usuario = new Usuario();
        usuario.BuscaUsuario(Context.User.Identity.Name);
        int idUsuario = usuario.IdUsuario.Value;

        InformacoesComplementaresFundoQuery info = new InformacoesComplementaresFundoQuery("L");
        CarteiraQuery carteiraQuery = new CarteiraQuery("C");
        ClienteQuery clienteQuery = new ClienteQuery("Q");
        PermissaoClienteQuery permissaoClienteQuery = new PermissaoClienteQuery("P");

        info.Select(info, carteiraQuery.IdCarteira, carteiraQuery.Apelido);
        info.InnerJoin(carteiraQuery).On(info.IdCarteira == carteiraQuery.IdCarteira);
        info.InnerJoin(clienteQuery).On(clienteQuery.IdCliente == info.IdCarteira);
        info.InnerJoin(permissaoClienteQuery).On(permissaoClienteQuery.IdCliente == clienteQuery.IdCliente);
        info.Where(clienteQuery.StatusAtivo.Equal((byte)StatusAtivoCliente.Ativo),
                   permissaoClienteQuery.IdUsuario.Equal(idUsuario));

        info.OrderBy(carteiraQuery.IdCarteira.Ascending, info.DataInicioVigencia.Ascending);

        InformacoesComplementaresFundoCollection coll = new InformacoesComplementaresFundoCollection();
        coll.Load(info);
        
        e.Collection = coll;
    }

    protected void EsDSCarteira_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e) {
        CarteiraCollection coll = new CarteiraCollection();
        coll.BuscaCarteirasComAcessoTodas(Context.User.Identity.Name);

        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }

    protected void EsDSnfoComplementarClonar_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e) {
        InformacoesComplementaresFundoCollection coll = new InformacoesComplementaresFundoCollection();

        InformacoesComplementaresFundoQuery info = new InformacoesComplementaresFundoQuery("e");
        CarteiraQuery c = new CarteiraQuery("C");

        info.Select(info.IdCarteira, info.DataInicioVigencia, c.Nome, c.Nome.As("DataInicioVigenciaString"),
                   (info.IdCarteira.Cast(esCastType.String) + "-" + info.DataInicioVigencia.Cast(esCastType.String)).As("CompositeKey")
            );
        info.InnerJoin(c).On(info.IdCarteira == c.IdCarteira);
        //
        info.OrderBy(info.IdCarteira.Ascending, info.DataInicioVigencia.Descending);

        coll.Load(info);

        for (int i = 0; i < coll.Count; i++) {
            DateTime dataVigencia = Convert.ToDateTime( coll[i].GetColumn(InformacoesComplementaresFundoMetadata.ColumnNames.DataInicioVigencia) );
            //
            //DateTime novaData = new DateTime(dataVigencia.Year, dataVigencia.Month, dataVigencia.Day);

            coll[i].SetColumn("DataInicioVigenciaString", dataVigencia.ToString("d"));
        }

        //
        e.Collection = coll;
    }

    protected void EsDSCarteiraClonar_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e) {
        CarteiraCollection coll = new CarteiraCollection();
        coll.BuscaCarteirasComAcesso(Context.User.Identity.Name);
        //        
        e.Collection = coll;
    }
    
    #endregion

    /// <summary>
    /// Faz Clonagem
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void callBackClonar_Callback(object source, DevExpress.Web.CallbackEventArgs e) {

        #region Campos obrigatórios
        List<Control> controles = new List<Control>();
        controles.Add(dropInfoComplementares);
        controles.Add(dropCarteiraDestino);
        controles.Add(textDataClonar);
        
        if (base.TestaObrigatorio(controles) != "") {
            e.Result = "Campos com * são obrigatórios!";
            return;
        }
        #endregion

        #region Confere registro já existente
        InformacoesComplementaresFundo infoVelha = new InformacoesComplementaresFundo();
        infoVelha.LoadByPrimaryKey(Convert.ToInt32(dropCarteiraDestino.Text.Trim()), Convert.ToDateTime(textDataClonar.Value));

        if (infoVelha.es.HasData) {
            e.Result = "Registro Já existente";
            return;
        }
        #endregion

        string selecionado = dropInfoComplementares.Text.Trim();
        string[] selecionadoAux = selecionado.Split(new Char[] { '-' });
        int idCarteira = Convert.ToInt32(selecionadoAux[0].Trim());
        DateTime data = Convert.ToDateTime(selecionadoAux[2].Trim());
        //        

        // Carrega Informação velha
        InformacoesComplementaresFundo infoNova = new InformacoesComplementaresFundo();
        infoNova.LoadByPrimaryKey(idCarteira, data);
               
        infoNova.MarkAllColumnsAsDirty(DataRowState.Added);

        // Acrescenta nova Carteira e Data
        infoNova.IdCarteira = Convert.ToInt32( dropCarteiraDestino.Text.Trim() );
        infoNova.DataInicioVigencia = Convert.ToDateTime(textDataClonar.Value);
        //
        infoNova.Save();

        //
        e.Result = "Processo executado com sucesso.";            
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void ASPxCallback1_Callback(object source, DevExpress.Web.CallbackEventArgs e) {
        string nome = "";
        if (!String.IsNullOrEmpty(e.Parameter) && e.Parameter != "null") {
            int idCarteira = Convert.ToInt32(e.Parameter);
            Carteira carteira = new Carteira();
            carteira.LoadByPrimaryKey(idCarteira);

            if (carteira.str.Apelido != "") {
                if (carteira.IsAtivo) {
                    PermissaoCliente permissaoCliente = new PermissaoCliente();
                    if (permissaoCliente.RetornaAcessoCliente(idCarteira, HttpContext.Current.User.Identity.Name)) {
                        nome = carteira.str.Apelido;
                    }
                    else {
                        nome = "no_access";
                    }
                }
                else {
                    nome = "no_active";
                }
            }
        }
        e.Result = nome;
    }


    /// <summary>
    /// 
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void callbackErro_Callback(object source, DevExpress.Web.CallbackEventArgs e)
    {
        e.Result = "";
        ASPxPageControl pageControl = gridCadastro.FindEditFormTemplateControl("tabCadastro") as ASPxPageControl;
        ASPxSpinEdit btnCarteira = pageControl.FindControl("btnEditCodigoCarteira") as ASPxSpinEdit;
        ASPxDateEdit textVigencia = pageControl.FindControl("textDataInicioVigencia") as ASPxDateEdit;
        //
        ASPxTextBox textFatoresRisco = pageControl.FindControl("textFatoresRisco") as ASPxTextBox;
        ASPxTextBox textPoliticaExercicioVoto = pageControl.FindControl("textPoliticaExercicioVoto") as ASPxTextBox;
        ASPxTextBox textTributacaoAplicavel = pageControl.FindControl("textTributacaoAplicavel") as ASPxTextBox;
        ASPxTextBox textPoliticaAdministracaoRisco = pageControl.FindControl("textPoliticaAdministracaoRisco") as ASPxTextBox;
        ASPxTextBox textAgenciaClassificacaoRisco = pageControl.FindControl("textAgenciaClassificacaoRisco") as ASPxTextBox;
        ASPxTextBox textRecursosServicosGestor = pageControl.FindControl("textRecursosServicosGestor") as ASPxTextBox;
        ASPxTextBox textPrestadoresServicos = pageControl.FindControl("textPrestadoresServicos") as ASPxTextBox;
        ASPxTextBox textPoliticaDistribuicaoCotas = pageControl.FindControl("textPoliticaDistribuicaoCotas") as ASPxTextBox;
        ASPxTextBox textObservacoes = pageControl.FindControl("textObservacoes") as ASPxTextBox;
        ASPxComboBox dropGestorVotaAssembleia = pageControl.FindControl("dropGestorVotaAssembleia") as ASPxComboBox;
        ASPxComboBox dropAGENC_CLASSIF_RATIN = pageControl.FindControl("dropAGENC_CLASSIF_RATIN") as ASPxComboBox;
        ASPxComboBox dropCOD_DISTR_OFERTA_PUB = pageControl.FindControl("dropCOD_DISTR_OFERTA_PUB") as ASPxComboBox;
        ASPxTextBox textDescricaoLocalDivulgacao = pageControl.FindControl("textDescricaoLocalDivulgacao") as ASPxTextBox;
        ASPxTextBox textDescricaoResponsavel = pageControl.FindControl("textDescricaoResponsavel") as ASPxTextBox;
        ASPxTextBox textApresentavaoAdministrador = pageControl.FindControl("textApresentavaoAdministrador") as ASPxTextBox;
        ASPxTextBox textINFORM_AUTOREGUL_ANBIMA = pageControl.FindControl("textINFORM_AUTOREGUL_ANBIMA") as ASPxTextBox;
        ASPxTextBox textDsServicoPrestado = pageControl.FindControl("textDsServicoPrestado") as ASPxTextBox;
        ASPxComboBox dropCodMeioDivulg = pageControl.FindControl("dropCodMeioDivulg") as ASPxComboBox;
        ASPxTextBox textDsLocal = pageControl.FindControl("textDsLocal") as ASPxTextBox;
        ASPxTextBox textDisclAdvert = pageControl.FindControl("textDisclAdvert") as ASPxTextBox;
        ASPxTextBox textNmPrest = pageControl.FindControl("textNmPrest") as ASPxTextBox;
        ASPxTextBox textNrCnpj = pageControl.FindControl("textNrCnpj") as ASPxTextBox;
        ////
        ASPxRoundPanel panel1 = pageControl.FindControl("Panel1") as ASPxRoundPanel;
        ASPxRoundPanel panel2 = pageControl.FindControl("Panel2") as ASPxRoundPanel;
        ////
        ASPxTextBox textPeriodicidade = panel1.FindControl("textPeriodicidade") as ASPxTextBox;
        ASPxTextBox textLocalFormaDivulgacao = panel1.FindControl("textLocalFormaDivulgacao") as ASPxTextBox;
        ASPxTextBox textLocalFormaSolicitacaoCotista = panel2.FindControl("textLocalFormaSolicitacaoCotista") as ASPxTextBox;

        #region Campos obrigatórios
        List<Control> controles = new List<Control>();
        controles.Add(btnCarteira);
        controles.Add(textVigencia);
        controles.Add(textPeriodicidade); //
        controles.Add(textLocalFormaDivulgacao); //
        controles.Add(textLocalFormaSolicitacaoCotista); //
        controles.Add(textFatoresRisco);
        controles.Add(textPoliticaExercicioVoto);
        controles.Add(textTributacaoAplicavel);
        controles.Add(textPoliticaAdministracaoRisco);
        controles.Add(textAgenciaClassificacaoRisco);
        controles.Add(textRecursosServicosGestor);
        controles.Add(textPrestadoresServicos);
        controles.Add(textPoliticaDistribuicaoCotas);
        controles.Add(textObservacoes);
        controles.Add(dropGestorVotaAssembleia);
        controles.Add(dropAGENC_CLASSIF_RATIN);
        controles.Add(dropCOD_DISTR_OFERTA_PUB);
        controles.Add(textDescricaoLocalDivulgacao);
        controles.Add(textDescricaoResponsavel);
        controles.Add(textApresentavaoAdministrador);
        controles.Add(textINFORM_AUTOREGUL_ANBIMA);
        controles.Add(dropCodMeioDivulg);
        controles.Add(textDsLocal);
        controles.Add(textDisclAdvert);
        controles.Add(textNmPrest);
        controles.Add(textNrCnpj);

        if (base.TestaObrigatorio(controles) != "") {
            e.Result = "Campos com * são obrigatórios!";
            return;
        }
        #endregion

        int idCarteira = Convert.ToInt32(btnCarteira.Text);
        DateTime data = Convert.ToDateTime(textVigencia.Text);

        if (gridCadastro.IsNewRowEditing) {
            InformacoesComplementaresFundo info = new InformacoesComplementaresFundo();
            if (info.LoadByPrimaryKey(idCarteira, data)) {
                e.Result = "Registro já existente.";
            }
        }

        #region Verifica CPF/CNPJ
        if (!string.IsNullOrEmpty(textNrCnpj.Text))
        {
            if (!ValidaCNPJ(textNrCnpj.Text))
            {
                e.Result = "CNPJ inválido!";
                return;
            }
        }
        #endregion

    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_RowUpdating(object sender, DevExpress.Web.Data.ASPxDataUpdatingEventArgs e)
    {
        #region Elementos
        ASPxPageControl pageControl = gridCadastro.FindEditFormTemplateControl("tabCadastro") as ASPxPageControl;
        ASPxSpinEdit btnCarteira = pageControl.FindControl("btnEditCodigoCarteira") as ASPxSpinEdit;
        ASPxDateEdit textVigencia = pageControl.FindControl("textDataInicioVigencia") as ASPxDateEdit;
        //
        ASPxTextBox textFatoresRisco = pageControl.FindControl("textFatoresRisco") as ASPxTextBox;
        ASPxTextBox textPoliticaExercicioVoto = pageControl.FindControl("textPoliticaExercicioVoto") as ASPxTextBox;
        ASPxTextBox textTributacaoAplicavel = pageControl.FindControl("textTributacaoAplicavel") as ASPxTextBox;
        ASPxTextBox textPoliticaAdministracaoRisco = pageControl.FindControl("textPoliticaAdministracaoRisco") as ASPxTextBox;
        ASPxTextBox textAgenciaClassificacaoRisco = pageControl.FindControl("textAgenciaClassificacaoRisco") as ASPxTextBox;
        ASPxTextBox textRecursosServicosGestor = pageControl.FindControl("textRecursosServicosGestor") as ASPxTextBox;
        ASPxTextBox textPrestadoresServicos = pageControl.FindControl("textPrestadoresServicos") as ASPxTextBox;
        ASPxTextBox textPoliticaDistribuicaoCotas = pageControl.FindControl("textPoliticaDistribuicaoCotas") as ASPxTextBox;
        ASPxTextBox textObservacoes = pageControl.FindControl("textObservacoes") as ASPxTextBox;
        ASPxComboBox dropGestorVotaAssembleia = pageControl.FindControl("dropGestorVotaAssembleia") as ASPxComboBox;
        ASPxComboBox dropAGENC_CLASSIF_RATIN = pageControl.FindControl("dropAGENC_CLASSIF_RATIN") as ASPxComboBox;
        ASPxComboBox dropCOD_DISTR_OFERTA_PUB = pageControl.FindControl("dropCOD_DISTR_OFERTA_PUB") as ASPxComboBox;
        ASPxTextBox textDescricaoLocalDivulgacao = pageControl.FindControl("textDescricaoLocalDivulgacao") as ASPxTextBox;
        ASPxTextBox textDescricaoResponsavel = pageControl.FindControl("textDescricaoResponsavel") as ASPxTextBox;
        ASPxTextBox textApresentavaoAdministrador = pageControl.FindControl("textApresentavaoAdministrador") as ASPxTextBox;
        ASPxTextBox textINFORM_AUTOREGUL_ANBIMA = pageControl.FindControl("textINFORM_AUTOREGUL_ANBIMA") as ASPxTextBox;
        ASPxTextBox textDsServicoPrestado = pageControl.FindControl("textDsServicoPrestado") as ASPxTextBox;
        ASPxComboBox dropCodMeioDivulg = pageControl.FindControl("dropCodMeioDivulg") as ASPxComboBox;
        ASPxTextBox textDsLocal = pageControl.FindControl("textDsLocal") as ASPxTextBox;
        ASPxTextBox textDisclAdvert = pageControl.FindControl("textDisclAdvert") as ASPxTextBox;
        ASPxTextBox textNmPrest = pageControl.FindControl("textNmPrest") as ASPxTextBox;
        ASPxTextBox textNrCnpj = pageControl.FindControl("textNrCnpj") as ASPxTextBox;
        ////
        ASPxRoundPanel panel1 = pageControl.FindControl("Panel1") as ASPxRoundPanel;
        ASPxRoundPanel panel2 = pageControl.FindControl("Panel2") as ASPxRoundPanel;
        ////
        ASPxTextBox textPeriodicidade = panel1.FindControl("textPeriodicidade") as ASPxTextBox;
        ASPxTextBox textLocalFormaDivulgacao = panel1.FindControl("textLocalFormaDivulgacao") as ASPxTextBox;
        ASPxTextBox textLocalFormaSolicitacaoCotista = panel2.FindControl("textLocalFormaSolicitacaoCotista") as ASPxTextBox;
        #endregion

        string idCarteira = btnCarteira.Text.ToString();
        string vigencia = textVigencia.Text.ToString();
        //
        InformacoesComplementaresFundo info = new InformacoesComplementaresFundo();
        if (info.LoadByPrimaryKey(Convert.ToInt32(idCarteira), Convert.ToDateTime(vigencia))) {

            #region InformacoesComplementaresFundo
            info.DataInicioVigencia = Convert.ToDateTime(textVigencia.Text);
            info.Periodicidade = Convert.ToString(textPeriodicidade.Text.Trim());
            info.LocalFormaDivulgacao = Convert.ToString(textLocalFormaDivulgacao.Text.Trim());
            info.LocalFormaSolicitacaoCotista = Convert.ToString(textLocalFormaSolicitacaoCotista.Text.Trim());
            info.FatoresRisco = Convert.ToString(textFatoresRisco.Text.Trim());
            info.PoliticaExercicioVoto = Convert.ToString(textPoliticaExercicioVoto.Text.Trim());
            info.TributacaoAplicavel = Convert.ToString(textTributacaoAplicavel.Text.Trim());
            info.PoliticaAdministracaoRisco = Convert.ToString(textPoliticaAdministracaoRisco.Text.Trim());
            info.AgenciaClassificacaoRisco = Convert.ToString(textAgenciaClassificacaoRisco.Text.Trim());
            info.RecursosServicosGestor = Convert.ToString(textRecursosServicosGestor.Text.Trim());
            info.PrestadoresServicos = Convert.ToString(textPrestadoresServicos.Text.Trim());
            info.PoliticaDistribuicaoCotas = Convert.ToString(textPoliticaDistribuicaoCotas.Text.Trim());
            info.Observacoes = Convert.ToString(textObservacoes.Text.Trim());
            info.CodVotoGestAssemb = dropGestorVotaAssembleia.SelectedItem.Value.ToString();
            info.AgencClassifRatin = dropAGENC_CLASSIF_RATIN.SelectedItem.Value.ToString();
            info.CodDistrOfertaPub = dropCOD_DISTR_OFERTA_PUB.SelectedItem.Value.ToString();
            info.DsLocalDivulg = textDescricaoLocalDivulgacao.Text.Trim();
            info.DsResp = textDescricaoResponsavel.Text.Trim();
            info.ApresDetalheAdm = textApresentavaoAdministrador.Text.Trim();
            info.InformAutoregulAnbima = textINFORM_AUTOREGUL_ANBIMA.Text.Trim();
            info.DsServicoPrestado = textDsServicoPrestado.Text.Trim();
            info.CodMeioDivulg = dropCodMeioDivulg.SelectedItem.Value.ToString();
            info.CodMeio = dropCOD_DISTR_OFERTA_PUB.SelectedItem.Value.ToString();
            info.DsLocal = textDsLocal.Text.Trim();
            info.NrCnpj = textNrCnpj.Text.Replace(".", "").Replace("-", "").Replace("/", "").Trim();
            info.NmPrest = textNmPrest.Text.Trim();
            info.DisclAdvert = textDisclAdvert.Text.Trim();
            #endregion

            info.Save();

            #region Log do Processo
            HistoricoLog historicoLog = new HistoricoLog();
            historicoLog.InsereHistoricoLog(DateTime.Now,
                                            DateTime.Now,
                                            "Cadastro de InformacoesComplementaresFundo - Operacao: Update InformacoesComplementaresFundo: " + idCarteira + " " + vigencia + UtilitarioWeb.ToString(info),
                                            HttpContext.Current.User.Identity.Name,
                                            UtilitarioWeb.GetIP(Request),
                                            "",
                                            HistoricoLogOrigem.Outros);
            #endregion
        }

        e.Cancel = true;
        gridCadastro.CancelEdit();
        gridCadastro.DataBind();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_RowInserting(object sender, DevExpress.Web.Data.ASPxDataInsertingEventArgs e)
    {

        #region Elementos
        ASPxPageControl pageControl = gridCadastro.FindEditFormTemplateControl("tabCadastro") as ASPxPageControl;
        ASPxSpinEdit btnCarteira = pageControl.FindControl("btnEditCodigoCarteira") as ASPxSpinEdit;
        ASPxDateEdit textVigencia = pageControl.FindControl("textDataInicioVigencia") as ASPxDateEdit;
        //
        ASPxTextBox textFatoresRisco = pageControl.FindControl("textFatoresRisco") as ASPxTextBox;
        ASPxTextBox textPoliticaExercicioVoto = pageControl.FindControl("textPoliticaExercicioVoto") as ASPxTextBox;
        ASPxTextBox textTributacaoAplicavel = pageControl.FindControl("textTributacaoAplicavel") as ASPxTextBox;
        ASPxTextBox textPoliticaAdministracaoRisco = pageControl.FindControl("textPoliticaAdministracaoRisco") as ASPxTextBox;
        ASPxTextBox textAgenciaClassificacaoRisco = pageControl.FindControl("textAgenciaClassificacaoRisco") as ASPxTextBox;
        ASPxTextBox textRecursosServicosGestor = pageControl.FindControl("textRecursosServicosGestor") as ASPxTextBox;
        ASPxTextBox textPrestadoresServicos = pageControl.FindControl("textPrestadoresServicos") as ASPxTextBox;
        ASPxTextBox textPoliticaDistribuicaoCotas = pageControl.FindControl("textPoliticaDistribuicaoCotas") as ASPxTextBox;
        ASPxTextBox textObservacoes = pageControl.FindControl("textObservacoes") as ASPxTextBox;
        ASPxComboBox dropGestorVotaAssembleia = pageControl.FindControl("dropGestorVotaAssembleia") as ASPxComboBox;
        ASPxComboBox dropAGENC_CLASSIF_RATIN = pageControl.FindControl("dropAGENC_CLASSIF_RATIN") as ASPxComboBox;
        ASPxComboBox dropCOD_DISTR_OFERTA_PUB = pageControl.FindControl("dropCOD_DISTR_OFERTA_PUB") as ASPxComboBox;
        ASPxTextBox textDescricaoLocalDivulgacao = pageControl.FindControl("textDescricaoLocalDivulgacao") as ASPxTextBox;
        ASPxTextBox textDescricaoResponsavel = pageControl.FindControl("textDescricaoResponsavel") as ASPxTextBox;
        ASPxTextBox textApresentavaoAdministrador = pageControl.FindControl("textApresentavaoAdministrador") as ASPxTextBox;
        ASPxTextBox textINFORM_AUTOREGUL_ANBIMA = pageControl.FindControl("textINFORM_AUTOREGUL_ANBIMA") as ASPxTextBox;
        ASPxTextBox textDsServicoPrestado = pageControl.FindControl("textDsServicoPrestado") as ASPxTextBox;
        ASPxComboBox dropCodMeioDivulg = pageControl.FindControl("dropCodMeioDivulg") as ASPxComboBox;
        ASPxTextBox textDsLocal = pageControl.FindControl("textDsLocal") as ASPxTextBox;
        ASPxTextBox textDisclAdvert = pageControl.FindControl("textDisclAdvert") as ASPxTextBox;
        ASPxTextBox textNmPrest = pageControl.FindControl("textNmPrest") as ASPxTextBox;
        ASPxTextBox textNrCnpj = pageControl.FindControl("textNrCnpj") as ASPxTextBox;
        ////
        ASPxRoundPanel panel1 = pageControl.FindControl("Panel1") as ASPxRoundPanel;
        ASPxRoundPanel panel2 = pageControl.FindControl("Panel2") as ASPxRoundPanel;
        ////
        ASPxTextBox textPeriodicidade = panel1.FindControl("textPeriodicidade") as ASPxTextBox;
        ASPxTextBox textLocalFormaDivulgacao = panel1.FindControl("textLocalFormaDivulgacao") as ASPxTextBox;
        ASPxTextBox textLocalFormaSolicitacaoCotista = panel2.FindControl("textLocalFormaSolicitacaoCotista") as ASPxTextBox;
        #endregion

        InformacoesComplementaresFundo info = new InformacoesComplementaresFundo();

        #region InformacoesComplementaresFundo
        info.IdCarteira = Convert.ToInt32(btnCarteira.Text);
        info.DataInicioVigencia = Convert.ToDateTime(textVigencia.Text);
        info.Periodicidade = Convert.ToString(textPeriodicidade.Text.Trim());
        info.LocalFormaDivulgacao = Convert.ToString(textLocalFormaDivulgacao.Text.Trim());
        info.LocalFormaSolicitacaoCotista = Convert.ToString(textLocalFormaSolicitacaoCotista.Text.Trim());
        info.FatoresRisco = Convert.ToString(textFatoresRisco.Text.Trim());
        info.PoliticaExercicioVoto = Convert.ToString(textPoliticaExercicioVoto.Text.Trim());
        info.TributacaoAplicavel = Convert.ToString(textTributacaoAplicavel.Text.Trim());
        info.PoliticaAdministracaoRisco = Convert.ToString(textPoliticaAdministracaoRisco.Text.Trim());
        info.AgenciaClassificacaoRisco = Convert.ToString(textAgenciaClassificacaoRisco.Text.Trim());
        info.RecursosServicosGestor = Convert.ToString(textRecursosServicosGestor.Text.Trim());
        info.PrestadoresServicos = Convert.ToString(textPrestadoresServicos.Text.Trim());
        info.PoliticaDistribuicaoCotas = Convert.ToString(textPoliticaDistribuicaoCotas.Text.Trim());
        info.Observacoes = Convert.ToString(textObservacoes.Text.Trim());
        info.CodVotoGestAssemb = dropGestorVotaAssembleia.SelectedItem.Value.ToString();
        info.AgencClassifRatin = dropAGENC_CLASSIF_RATIN.SelectedItem.Value.ToString();
        info.CodDistrOfertaPub = dropCOD_DISTR_OFERTA_PUB.SelectedItem.Value.ToString();
        info.DsLocalDivulg = textDescricaoLocalDivulgacao.Text.Trim();
        info.DsResp = textDescricaoResponsavel.Text.Trim();
        info.ApresDetalheAdm = textApresentavaoAdministrador.Text.Trim();
        info.InformAutoregulAnbima = textINFORM_AUTOREGUL_ANBIMA.Text.Trim();
        info.DsServicoPrestado = textDsServicoPrestado.Text.Trim();
        info.CodDistrOfertaPub = dropCOD_DISTR_OFERTA_PUB.SelectedItem.Value.ToString();
	    info.InformAutoregulAnbima = textINFORM_AUTOREGUL_ANBIMA.Text.Trim();
	    info.CodMeioDivulg = dropCodMeioDivulg.SelectedItem.Value.ToString();
	    info.CodMeio = dropCOD_DISTR_OFERTA_PUB.SelectedItem.Value.ToString();
	    info.DsLocal= textDsLocal.Text.Trim();
        info.NrCnpj = textNrCnpj.Text.Replace(".", "").Replace("-", "").Replace("/", "").Trim();
        info.NmPrest = textNmPrest.Text.Trim();
        info.DisclAdvert = textDisclAdvert.Text.Trim();
        #endregion

        info.Save();

        #region Log do Processo
        HistoricoLog historicoLog = new HistoricoLog();
        historicoLog.InsereHistoricoLog(DateTime.Now,
                                        DateTime.Now,
                                        "Cadastro de InformacoesComplementaresFundo - Operacao: Insert InformacoesComplementaresFundo: " + info.IdCarteira + " " + textVigencia.Text + UtilitarioWeb.ToString(info),
                                        HttpContext.Current.User.Identity.Name,
                                        UtilitarioWeb.GetIP(Request),
                                        "",
                                        HistoricoLogOrigem.Outros);
        #endregion

        e.Cancel = true;
        gridCadastro.CancelEdit();
        gridCadastro.DataBind();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_CustomUnboundColumnData(object sender, DevExpress.Web.ASPxGridViewColumnDataEventArgs e) {
        if (e.Column.FieldName == "CompositeKey") {
            //Compose a primary key value
            string idCarteira = Convert.ToString(e.GetListSourceFieldValue(InformacoesComplementaresFundoMetadata.ColumnNames.IdCarteira));
            string data = Convert.ToString(e.GetListSourceFieldValue(InformacoesComplementaresFundoMetadata.ColumnNames.DataInicioVigencia));
            e.Value = idCarteira + "-" + data;
        }        
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_CustomCallback(object sender, ASPxGridViewCustomCallbackEventArgs e)
    {
        if (e.Parameters == "btnDelete") {

            List<object> keyValues1 = gridCadastro.GetSelectedFieldValues(InformacoesComplementaresFundoMetadata.ColumnNames.IdCarteira);
            List<object> keyValues2 = gridCadastro.GetSelectedFieldValues(InformacoesComplementaresFundoMetadata.ColumnNames.DataInicioVigencia);

            for (int i = 0; i < keyValues1.Count; i++) {
                string idCarteira = Convert.ToString(keyValues1[i]);
                string vigencia = Convert.ToString(keyValues2[i]);

                InformacoesComplementaresFundo info = new InformacoesComplementaresFundo();
                if (info.LoadByPrimaryKey(Convert.ToInt32(idCarteira), Convert.ToDateTime(vigencia))) {
                    InformacoesComplementaresFundo infoClone = (InformacoesComplementaresFundo)Utilitario.Clone(info);
                    //
                    info.MarkAsDeleted();
                    info.Save();

                    #region Log do Processo
                    HistoricoLog historicoLog = new HistoricoLog();
                    historicoLog.InsereHistoricoLog(DateTime.Now,
                                                    DateTime.Now,
                                                    "Cadastro de InformacoesComplementaresFundo - Operacao: Delete InformacoesComplementaresFundo: " + infoClone.IdCarteira + " " + infoClone.DataInicioVigencia.ToString() + UtilitarioWeb.ToString(infoClone),
                                                    HttpContext.Current.User.Identity.Name,
                                                    UtilitarioWeb.GetIP(Request),
                                                    "",
                                                    HistoricoLogOrigem.Outros);
                    #endregion
                }
            }
        }

        gridCadastro.DataBind();
        gridCadastro.Selection.UnselectAll();
        gridCadastro.CancelEdit();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    new protected void gridCadastro_PreRender(object sender, EventArgs e)
    {
        //this.FocaCampoGrid("textNome", isOnTabPage);
        base.gridCadastro_PreRender(sender, e);
    }

    /// <summary>
    /// Verifica se o CNPJ é válido
    /// </summary>
    /// <param name="cnpj">Valor a ser verificado</param>
    /// <returns>Condição de validação</returns>
    private static bool ValidaCNPJ(string vrCnpj)
    {
        string CNPJ = vrCnpj.Replace(".", "");
        CNPJ = CNPJ.Replace("/", "");
        CNPJ = CNPJ.Replace("-", "");
        int[] digitos, soma, resultado;
        int nrDig;
        string ftmt;
        bool[] CNPJOk;
        ftmt = "6543298765432";
        digitos = new int[14];
        soma = new int[2];
        soma[0] = 0;
        soma[1] = 0;
        resultado = new int[2];
        resultado[0] = 0;
        resultado[1] = 0;
        CNPJOk = new bool[2];
        CNPJOk[0] = false;
        CNPJOk[1] = false;
        try
        {
            for (nrDig = 0; nrDig < 14; nrDig++)
            {
                digitos[nrDig] = int.Parse(CNPJ.Substring(nrDig, 1));
                if (nrDig <= 11) soma[0] += (digitos[nrDig] * int.Parse(ftmt.Substring(nrDig + 1, 1)));
                if (nrDig <= 12) soma[1] += (digitos[nrDig] * int.Parse(ftmt.Substring(nrDig, 1)));
            }
            for (nrDig = 0; nrDig < 2; nrDig++)
            {
                resultado[nrDig] = (soma[nrDig] % 11);
                if ((resultado[nrDig] == 0) || (resultado[nrDig] == 1))
                    CNPJOk[nrDig] = (digitos[12 + nrDig] == 0);
                else
                    CNPJOk[nrDig] = (digitos[12 + nrDig] == (11 - resultado[nrDig]));
            }
            return (CNPJOk[0] && CNPJOk[1]);
        }
        catch
        {
            return false;
        }
    }

}