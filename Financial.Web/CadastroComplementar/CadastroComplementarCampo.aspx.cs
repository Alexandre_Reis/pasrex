﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Collections.Generic;
using System.Text;
using System.Drawing;

using Financial.Common;
using Financial.Util;
using Financial.Investidor;
using Financial.ContaCorrente;
using Financial.Security;
using Financial.Fundo;
using Financial.InvestidorCotista;
using Financial.Bolsa;
using Financial.RendaFixa;
using Financial.Web.Common;

using DevExpress.Web;
using DevExpress.Web.Data;

using Financial.ContaCorrente.Enums;
using Financial.InvestidorCotista.Enums;
using Financial.Investidor.Enums;
using Financial.Common.Enums;
using Financial.Bolsa.Enums;
using Financial.RendaFixa.Enums;
using System.Threading;
using EntitySpaces.Interfaces;
using Financial.Security.Enums;
using Financial.Web.Util;
using System.ComponentModel;
using System.Reflection;

public partial class CadastroComplementar_CadastroComplementarCampo : CadastroBasePage
{
    #region Instância de Classes
    CadastroComplementarCampos camposComplementares = new CadastroComplementarCampos();
    CadastroComplementarTipoLista cadastroComplementarTipoLista = new CadastroComplementarTipoLista();
    CadastroComplementarCamposCollection camposComplementaresCollection = new CadastroComplementarCamposCollection();
    CadastroComplementarTipoListaCollection cadastroComplementarTipoListaCollection = new CadastroComplementarTipoListaCollection();
    #endregion

    #region EntitySpace - Base de Dados
    /// <summary>
    /// Acesso a base de dados
    /// </summary>
    protected void EsDSCamposComplementares_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        camposComplementaresCollection = new CadastroComplementarCamposCollection();

        camposComplementaresCollection.Query.OrderBy(camposComplementaresCollection.Query.IdCamposComplementares.Ascending);
        camposComplementaresCollection.Query.Load();
        e.Collection = camposComplementaresCollection;
    }
    #endregion

    #region CallBack
    /// <summary>
    /// 
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void callbackAdd_Callback(object source, DevExpress.Web.CallbackEventArgs e)
    {
        this.Cadastra();
    }

    /// <summary>
    /// Efetua a validação da tela
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void callbackErro_Callback(object source, DevExpress.Web.CallbackEventArgs e)
    {
        e.Result = "";

        if (!e.Parameter.Equals("delete"))
        {
            //Busca controles na tela
            ASPxComboBox dropTipoCadastro = gridCadastro.FindEditFormTemplateControl("dropTipoCadastro") as ASPxComboBox;
            ASPxComboBox dropTipoCampo = gridCadastro.FindEditFormTemplateControl("dropTipoCadastro") as ASPxComboBox;
            ASPxComboBox dropCampoObrigatorio = gridCadastro.FindEditFormTemplateControl("dropCampoObrigatorio") as ASPxComboBox;
            ASPxTextBox textNomeCampo = gridCadastro.FindEditFormTemplateControl("textNomeCampo") as ASPxTextBox;

            if (string.IsNullOrEmpty(textNomeCampo.Text)
                || dropTipoCadastro.SelectedItem == null
                || dropTipoCampo.SelectedItem == null
                || dropCampoObrigatorio.SelectedItem == null)
                e.Result = "Campos com * devem ser preenchidos!";
        }
    }

    /// <summary>
    /// Atribui mascara dinamicamente para os campos dependendo do tipo de campo
    /// OBS: O método é um AspxCallBackPanel, ele funciona como o UpdatePanel.
    /// Sua utilização é necessária para que não ocorra o postback na página
    /// e a mascara perca o seu valor.
    /// </summary>
    protected void cbPanel_Callback(object source, CallbackEventArgsBase e)
    {
        ASPxCallbackPanel cbPanel = gridCadastro.FindEditFormTemplateControl("cbPanel") as ASPxCallbackPanel;
        ASPxTextBox t = cbPanel.FindControl("textValorDefault") as ASPxTextBox;
        ASPxComboBox dropTipoCampo = gridCadastro.FindEditFormTemplateControl("dropTipoCampo") as ASPxComboBox;

        //Limpa o texto do controle
        t.Text = string.Empty;

        //Numerico
        if (dropTipoCampo.SelectedItem.Value.Equals("-1"))
        {
            t.MaskSettings.Mask = "<0.." + t.Text.PadRight(200, '9') + ">";
        }
        //Alfanumerico
        if (dropTipoCampo.SelectedItem.Value.Equals("-2"))
        {

        }
        //Data
        if (dropTipoCampo.SelectedItem.Value.Equals("-3"))
        {
            t.MaskSettings.Mask = "dd/MM/yyyy";
            t.Text = DateTime.Now.ToString();
        }
    }
    #endregion

    #region gridCadastro
    /// <summary>
    /// Efetua a alteração dos dados dos registros
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_RowUpdating(object sender, DevExpress.Web.Data.ASPxDataUpdatingEventArgs e)
    {
        int idCamposComplementares = (int)e.Keys[0];
        if (camposComplementares.LoadByPrimaryKey(idCamposComplementares))
        {
            //Preenche a entidade dinamicamente
            camposComplementares = PreencheEntidade<CadastroComplementarCampos>(gridCadastro.FindEditFormTemplateControl("panelEdicao").Controls, camposComplementares);
            camposComplementares.IdCamposComplementares = idCamposComplementares;
            camposComplementares.Save();

            #region Log do Processo
            HistoricoLog historicoLog = new HistoricoLog();
            historicoLog.InsereHistoricoLog(DateTime.Now,
                                            DateTime.Now,
                                            "Cadastro de CadastroComplementarCampo - Operacao: Update CadastroComplementarCampo: " + idCamposComplementares + UtilitarioWeb.ToString(camposComplementares),
                                            HttpContext.Current.User.Identity.Name,
                                            UtilitarioWeb.GetIP(Request),
                                            "",
                                            HistoricoLogOrigem.Outros);
            #endregion
        }

        e.Cancel = true;
        gridCadastro.CancelEdit();
        gridCadastro.DataBind();
    }

    /// <summary>
    /// Efetua o cadastro
    /// </summary>
    protected void gridCadastro_RowInserting(object sender, DevExpress.Web.Data.ASPxDataInsertingEventArgs e)
    {
        this.Cadastra();

        e.Cancel = true;
        gridCadastro.CancelEdit();
    }

    /// <summary>
    /// Deleta os registros selecionados
    /// </summary>
    protected void gridCadastro_CustomCallback(object sender, ASPxGridViewCustomCallbackEventArgs e)
    {
        if (e.Parameters == "btnDelete")
        {
            List<object> keyValuesId = gridCadastro.GetSelectedFieldValues("IdCamposComplementares");
            for (int i = 0; i < keyValuesId.Count; i++)
            {
                int idCamposComplementares = Convert.ToInt32(keyValuesId[i]);
                camposComplementares = new CadastroComplementarCampos();

                if (camposComplementares.LoadByPrimaryKey(idCamposComplementares))
                {
                    CadastroComplementarCampos camposComplementaresClone = (CadastroComplementarCampos)Utilitario.Clone(camposComplementares);

                    camposComplementares.MarkAsDeleted();
                    camposComplementares.Save();

                    #region Log do Processo
                    HistoricoLog historicoLog = new HistoricoLog();
                    historicoLog.InsereHistoricoLog(DateTime.Now,
                                                    DateTime.Now,
                                                    "Cadastro de CadastroComplementarCampo - Operacao: Delete CadastroComplementarCampo: " + idCamposComplementares + "; " + UtilitarioWeb.ToString(camposComplementaresClone),
                                                    HttpContext.Current.User.Identity.Name,
                                                    UtilitarioWeb.GetIP(Request),
                                                    "",
                                                    HistoricoLogOrigem.Outros);
                    #endregion
                }
            }
        }

        gridCadastro.DataBind();
        gridCadastro.Selection.UnselectAll();
        gridCadastro.CancelEdit();
    }

    /// <summary>
    /// Efetua o DePara dos valores da coluna de cadastro
    /// </summary>
    protected void gridCadastro_CustomColumnDisplayText(object sender, ASPxGridViewColumnDisplayTextEventArgs e)
    {
        if (e.Column.FieldName == "TipoCadastro")
        {
            e.DisplayText = ListaCadastroDescricao.RetornaStringValue(Convert.ToInt32(e.Value));
        }
        if (e.Column.FieldName == "TipoCampo")
        {
            //Verifica se o valor é negativo, referenciando a lista de ENUM
            if (Convert.ToInt32(e.Value) < 0)
                e.DisplayText = ListaCadastroComplementarDescricao.RetornaStringValue(Convert.ToInt32(e.Value));
            else
                e.DisplayText = ListaTipoCampoDescricao(Convert.ToInt32(e.Value));
        }
    }

    /// <summary>
    /// Fecha o EditForm e atualiza a grid
    /// Evento é chamado quando o usuário clica no botão para fechar a grid
    /// </summary>
    protected void gridCadastro_CancelRowEditing(object sender, ASPxStartRowEditingEventArgs e)
    {
        e.Cancel = false;
        gridCadastro.DataBind();
    }

    /// <summary>
    /// Carrega os combobox dos filtros de pesquisa da grid
    /// </summary>
    protected void gridCadastro_PreRender(object sender, EventArgs e)
    {
        #region cadastro
        //Busca o controle de filtro da coluna CADASTRO da grid
        GridViewDataComboBoxColumn cadastro = gridCadastro.Columns["Cadastro"] as GridViewDataComboBoxColumn;
        //Adiciona registro vazio para limpar o filtro
        cadastro.PropertiesComboBox.Items.Add(string.Empty);
        //Percorre o ENUM para preencher o controle do combobox
        foreach (int r in Enum.GetValues(typeof(ListaCadastro)))
        {
            cadastro.PropertiesComboBox.Items.Add(ListaCadastroDescricao.RetornaStringValue(r), r.ToString());
        }
        #endregion

        #region Tipo Lista
        //Busca o controle de filtro da coluna CADASTRO da grid
        GridViewDataComboBoxColumn tipoCampo = gridCadastro.Columns["TipoCampo"] as GridViewDataComboBoxColumn;
        //Limpa a lista de items para remover possiveis items duplicados
        tipoCampo.PropertiesComboBox.Items.Clear();
        //Adiciona registro vazio para limpar o filtro
        tipoCampo.PropertiesComboBox.Items.Add(string.Empty);
        //Percorre o ENUM para preencher o controle do combobox
        foreach (int r in Enum.GetValues(typeof(ListaCadastroComplementar)))
        {
            if (r == -1 || r == -2 || r == -3)
                tipoCampo.PropertiesComboBox.Items.Add(ListaCadastroComplementarDescricao.RetornaStringValue(r), r.ToString());
        }

        //Carrega Lista de campos cadastrados
        cadastroComplementarTipoListaCollection = new CadastroComplementarTipoListaCollection();
        cadastroComplementarTipoListaCollection.Query.Select();
        cadastroComplementarTipoListaCollection.Query.Load();

        foreach (CadastroComplementarTipoLista item in cadastroComplementarTipoListaCollection)
        {
            tipoCampo.PropertiesComboBox.Items.Add(new ListEditItem(item.NomeLista, item.IdTipoLista));
        }
        #endregion
    }
    #endregion

    #region OnLoad
    /// <summary>
    /// Carrega o dropdown com o valores do Enum
    /// </summary>
    protected void dropTipoCadastro_OnLoad(object sender, EventArgs e)
    {
        ASPxComboBox dropTipoCadastro = (ASPxComboBox)sender;

        foreach (int r in Enum.GetValues(typeof(ListaCadastro)))
        {
            dropTipoCadastro.Items.Add(ListaCadastroDescricao.RetornaStringValue(r), r.ToString());
        }
    }

    /// <summary>
    /// Carrega o dropdown com o valores do Enum
    /// </summary>
    protected void dropTipoCampo_OnLoad(object sender, EventArgs e)
    {
        ASPxComboBox dropTipoCampo = (ASPxComboBox)sender;

        foreach (int r in Enum.GetValues(typeof(ListaCadastroComplementar)))
        {
            if (r == -1 || r == -2 || r == -3)
                dropTipoCampo.Items.Add(ListaCadastroComplementarDescricao.RetornaStringValue(r), r.ToString());
        }

        //Carrega Lista de campos cadastrados
        cadastroComplementarTipoListaCollection = new CadastroComplementarTipoListaCollection();
        cadastroComplementarTipoListaCollection.Query.Select();
        cadastroComplementarTipoListaCollection.Query.Load();

        foreach (CadastroComplementarTipoLista item in cadastroComplementarTipoListaCollection)
        {
            dropTipoCampo.Items.Add(new ListEditItem(item.NomeLista, item.IdTipoLista));
        }
    }
    #endregion

    #region Suporte
    /// <summary>
    /// Configura Visibilidade do botão Ok++
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnOKAdd_Init(object sender, EventArgs e)
    {
        // Se for Update Não Aparece botão de Ok++
        if (!gridCadastro.IsNewRowEditing)
            (sender as LinkButton).Visible = false;
    }

    /// <summary>
    /// Inserção de um novo registro
    /// </summary>
    private void Cadastra()
    {
        camposComplementares = PreencheEntidade<CadastroComplementarCampos>(gridCadastro.FindEditFormTemplateControl("panelEdicao").Controls, null);
        camposComplementares.Save();

        #region Log do Processo
        HistoricoLog historicoLog = new HistoricoLog();
        historicoLog.InsereHistoricoLog(DateTime.Now,
                                        DateTime.Now,
                                        "Cadastro Complementar Campos - Operacao: Insert CadastroComplementarCampo: " + camposComplementares.IdCamposComplementares + UtilitarioWeb.ToString(camposComplementares),
                                        HttpContext.Current.User.Identity.Name,
                                        UtilitarioWeb.GetIP(Request),
                                        "",
                                        HistoricoLogOrigem.Outros);
        #endregion
    }

    /// <summary>
    /// Converte o valor para o tipo especificado
    /// </summary>
    /// <param name="value">O valor a ser convertido</param>
    /// <param name="conversionType">O tipo a ser convertido</param>
    /// <returns>Retorna o valor convertido</returns>
    public static object ChangeType(object value, Type conversionType)
    {
        try
        {
            if (conversionType == null)
            {
                throw new ArgumentNullException("conversionType");
            }
            if (conversionType.IsGenericType && conversionType.GetGenericTypeDefinition().Equals(typeof(Nullable<>)))
            {
                if (value == null)
                {
                    return null;
                }
                NullableConverter nullableConverter = new NullableConverter(conversionType);
                conversionType = nullableConverter.UnderlyingType;
            }
            return Convert.ChangeType(value, conversionType);
        }
        catch
        {
            //Caso não consiga converter o valor, uma nova instância é gerada para setar um valor default
            return value = Activator.CreateInstance(conversionType);
        }
    }

    /// <summary>
    /// Preenche a entidade com os valores dos controles 
    /// OBS: É necessário que os controles da tela possuam seu ID correspondente as colunas do Banco de Dados
    /// </summary>
    /// <typeparam name="T">Tipo de classe a ser preenchida</typeparam>
    /// <param name="controlCollection">Lista de controles a ser percorrida</param>
    /// <param name="Entidade">Entidade do update</param>
    /// <returns>Entidade preenchida</returns>
    public T PreencheEntidade<T>(ControlCollection controlCollection, T Entidade) where T : new()
    {
        if (Entidade == null)
            Entidade = new T();

        //Instancia lista de controles obrigatórios
        List<string> listaCamposObrigatorios = new List<string>();

        #region Campos Obrigatórios
        foreach (Control control in controlCollection)
        {
            if (control is Label)
            {
                //Verifica se o campo tem preenchimento obrigatório
                if (control.GetType().GetProperty("CssClass").GetValue(control, null).Equals("labelRequired"))
                {
                    //Devolve o nome da propriedade
                    string controle = control.ClientID.Substring(control.ClientID.IndexOf("label")).Replace("label", "");
                    //Adiciona os campos obrigatórios
                    listaCamposObrigatorios.Add(controle);
                }
            }
        }
        #endregion

        #region Preenche Entidade
        //Percorre a lista de controles
        foreach (Control control in controlCollection)
        {
            //Verifica somente os controles que possuem valor para preencher a entidade
            if (!(control is Label))
            {
                //Instância valor que irá receber o valor dos controles
                object valor = new object();

                //Retira os prefixos dos controles para encontrar as propriedades da entidade(WebControls e Controles do DevExpress)
                string propriedade = control.ClientID.Substring(control.ClientID.LastIndexOf("_")).Replace("text", "").Replace("drop", "").Replace("_", "");

                //Verifica se o controle é um dropdown
                if (control.GetType().GetProperty("Value") != null && control.GetType().GetProperty("Value").GetValue(control, null) != null)
                    //DropDown
                    valor = control.GetType().GetProperty("Value").GetValue(control, null);
                else if (control.GetType().GetProperty("Text") != null && control.GetType().GetProperty("Text").GetValue(control, null) != null)
                    //TextBox
                    valor = control.GetType().GetProperty("Text").GetValue(control, null);

                //Verifica se há campos obrigatórios
                if (listaCamposObrigatorios.Contains(propriedade) && valor.Equals(string.Empty))
                    throw new Exception("Campos com * são obrigatórios!");

                //Verifica se foi encontrado essa propriedade na entidade
                if (Entidade.GetType().GetProperty(propriedade) != null)
                    //Preenche a entidade com o valor encontrado
                    Entidade.GetType().GetProperty(propriedade).SetValue(Entidade,
                                                                         ChangeType(valor, Entidade.GetType().GetProperty(propriedade).PropertyType),
                                                                         null);
            }
        }
        #endregion

        //Retorna a entidade preenchida
        return Entidade;
    }

    /// <summary>
    /// Cria dinamicamente os controle na tela
    /// </summary>
    /// <typeparam name="T">Tipo do controle a ser criado</typeparam>
    /// <param name="idControle">ID do controle a ser criado</param>
    /// <param name="textoControle">Texto do controle a ser exibido na tela</param>
    /// <returns>Controle com as propriedades principais criadas</returns>
    private T CriaControleTela<T>(string idControle, string textoControle) where T : new()
    {
        //Instancia o tipo de controle a ser criado
        T controleCriado = new T();
        //Pega a propriedade ID do controle
        PropertyInfo id = controleCriado.GetType().GetProperty("ID");
        //Seta o valor ID para o controle
        id.SetValue(controleCriado, idControle, null);
        //Pega a propriedade TEXT do controle
        PropertyInfo texto = controleCriado.GetType().GetProperty("Text");
        //Seta o valor TEXT para o controle
        texto.SetValue(controleCriado, textoControle, null);
        //Retorna o controle com as propriedades preenchidas
        return controleCriado;
    }

    /// <summary>
    /// Retorna a descrição da lista cadastrada
    /// </summary>
    /// <param name="id">Identificador da lista</param>
    /// <returns>Nome da lista</returns>
    private string ListaTipoCampoDescricao(int id)
    {
        //Instancia entidades
        cadastroComplementarTipoListaCollection = new CadastroComplementarTipoListaCollection();
        cadastroComplementarTipoLista = new CadastroComplementarTipoLista();

        //Carrega Lista de campos cadastrados
        cadastroComplementarTipoListaCollection.Query.Select(cadastroComplementarTipoListaCollection.Query.NomeLista);
        cadastroComplementarTipoListaCollection.Query.Where(cadastroComplementarTipoListaCollection.Query.IdTipoLista == id);
        //cadastroComplementarTipoListaCollection.Query.Load();
        cadastroComplementarTipoLista.Load(cadastroComplementarTipoListaCollection.Query);

        return cadastroComplementarTipoLista.NomeLista;
    }
    #endregion
}