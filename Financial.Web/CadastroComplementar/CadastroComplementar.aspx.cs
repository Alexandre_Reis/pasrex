﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Collections.Generic;
using System.Text;
using System.Drawing;

using Financial.Common;
using Financial.Util;
using Financial.Investidor;
using Financial.ContaCorrente;
using Financial.Security;
using Financial.Fundo;
using Financial.InvestidorCotista;
using Financial.Bolsa;
using Financial.RendaFixa;
using Financial.Web.Common;

using DevExpress.Web;
using DevExpress.Web.Data;

using Financial.ContaCorrente.Enums;
using Financial.InvestidorCotista.Enums;
using Financial.Investidor.Enums;
using Financial.Common.Enums;
using Financial.Bolsa.Enums;
using Financial.RendaFixa.Enums;
using System.Threading;
using EntitySpaces.Interfaces;
using Financial.Security.Enums;
using Financial.Web.Util;
using System.ComponentModel;
using System.Reflection;
using Financial.BMF;
using System.Transactions;

public partial class CadastroComplementar_CadastroComplementar : CadastroBasePage
{
	public String Separador()
    {
        if (ConfigurationManager.AppSettings["SeparadorCadastroComplementar"] != null)
            return ConfigurationManager.AppSettings["SeparadorCadastroComplementar"];
        else
            return "%%%";
    }
	
    protected void Page_Load(object sender, EventArgs e)
    {
        base.Page_Load(sender, e);
        //Efetua a persistência dos campos na tela
        //OBS: Como os controles são gerados dinamicamente, os seus valores são perdidos no 
        //momento do PostBack, portanto é necessario criar os controles novamente(Exatamente com o mesmo ID)
        //para que seus valores sejam persistidos
        CriaControleTela();
    }

    #region Instância de Classes
    //Collection
    CadastroComplementarItemListaCollection cadastroComplementarItemListaCollection = new CadastroComplementarItemListaCollection();
    CadastroComplementarCamposCollection cadastroComplementarCamposCollection = new CadastroComplementarCamposCollection();
    CadastroComplementarTipoListaCollection cadastroComplementarTipoListaCollection = new CadastroComplementarTipoListaCollection();
    CadastroComplementarCollection cadastroComplementarCollection = new CadastroComplementarCollection();
    AgenteMercadoCollection agenteMercadoCollection = new AgenteMercadoCollection();
    CotistaCollection cotistaCollection = new CotistaCollection();
    EmissorCollection emissorCollection = new EmissorCollection();
    ClienteCollection clienteCollection = new ClienteCollection();
    AtivoBolsaCollection ativoBolsaCollection = new AtivoBolsaCollection();
    AtivoBMFCollection ativoBMFCollection = new AtivoBMFCollection();
    TituloRendaFixaCollection tituloRendaFixaCollection = new TituloRendaFixaCollection();
    PapelRendaFixaCollection papelRendaFixaCollection = new PapelRendaFixaCollection();
    //Entidade
    CadastroComplementar cadastroComplementar = new CadastroComplementar();
    CadastroComplementarCampos camposComplementares = new CadastroComplementarCampos();
    CadastroComplementarTipoLista cadastroComplementarTipoLista = new CadastroComplementarTipoLista();
    //Query
    CadastroComplementarCamposQuery cadastroComplementarCamposQuery = new CadastroComplementarCamposQuery();
    CadastroComplementarQuery cadastroComplementarQuery = new CadastroComplementarQuery();
    #endregion

    #region EntitySpace - Base de Dados
    /// <summary>
    /// Acesso a base de dados
    /// </summary>
    protected void EsDSCadastroComplementar_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        CadastroComplementarQuery cadastroComplementarQuery = new CadastroComplementarQuery("a");
        CadastroComplementarCamposQuery cadastroComplementarCamposQuery = new CadastroComplementarCamposQuery("b");

        cadastroComplementarQuery.InnerJoin(cadastroComplementarCamposQuery).On(cadastroComplementarQuery.IdCamposComplementares == cadastroComplementarCamposQuery.IdCamposComplementares);
        cadastroComplementarQuery.OrderBy(cadastroComplementarQuery.IdCadastroComplementares.Ascending);
        cadastroComplementarCollection.Load(cadastroComplementarQuery);

        e.Collection = cadastroComplementarCollection;
    }
    #endregion

    #region CallBack
    /// <summary>
    /// Efetua o cadastro dos valores
    /// </summary>
    protected void callbackAdd_Callback(object source, DevExpress.Web.CallbackEventArgs e)
    {
        this.Cadastra();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void callbackErro_Callback(object source, DevExpress.Web.CallbackEventArgs e)
    {
        e.Result = "";
    }

    /// <summary>
    /// Limpa os controles quando o usuário selecionar outro tipo de cadastro
    /// </summary>
    protected void callbackPanel_Callback(object sender, CallbackEventArgsBase e)
    {

    }

    /// <summary>
    /// Efetua o cadastro dos campos dinâmicos
    /// </summary>
    protected void callbackCadastro_Callback(object sender, DevExpress.Web.CallbackEventArgs e)
    {
        this.Cadastra();
    }
    #endregion

    #region GridCadastro
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_RowUpdating(object sender, DevExpress.Web.Data.ASPxDataUpdatingEventArgs e)
    {
        int idCadastroComplementares = (int)e.Keys[0];
        if (cadastroComplementar.LoadByPrimaryKey(idCadastroComplementares))
        {
            int tipoCampo = cadastroComplementar.UpToCadastroComplementarCamposByIdCamposComplementares.TipoCampo.Value;

            if(tipoCampo != -1 && tipoCampo != -2 && tipoCampo != -3)
            {
                ASPxComboBox dropValorCampo = gridCadastro.FindEditFormTemplateControl("dropValorCampo") as ASPxComboBox;
                
                if (dropValorCampo.SelectedIndex != -1)
                {
                    CadastroComplementarItemLista cadastroItemLista = new CadastroComplementarItemLista();
                    if (cadastroItemLista.LoadByPrimaryKey(Convert.ToInt32(dropValorCampo.SelectedItem.Value)))
                        cadastroComplementar.ValorCampo = cadastroItemLista.ItemLista;
                }
            }
            else if (tipoCampo == -1)
            {
                ASPxSpinEdit spinValorCampo = gridCadastro.FindEditFormTemplateControl("spinValorCampo") as ASPxSpinEdit;
                cadastroComplementar.ValorCampo = spinValorCampo.Text;
            }
            else if (tipoCampo == -2)
            {
                ASPxTextBox textValorCampo = gridCadastro.FindEditFormTemplateControl("textValorCampo") as ASPxTextBox;
                cadastroComplementar.ValorCampo = textValorCampo.Text;
            }
            else if (tipoCampo == -3)
            {
                ASPxDateEdit dataValorCampo = gridCadastro.FindEditFormTemplateControl("dataValorCampo") as ASPxDateEdit;
                cadastroComplementar.ValorCampo = dataValorCampo.Text;
            }
            else
            {
                cadastroComplementar.ValorCampo = null;
            }

            cadastroComplementar.Save();

            #region Log do Processo
            HistoricoLog historicoLog = new HistoricoLog();
            historicoLog.InsereHistoricoLog(DateTime.Now,
                                            DateTime.Now,
                                            "Cadastro de CadastroComplementar - Operacao: Update CadastroComplementar: " + idCadastroComplementares + UtilitarioWeb.ToString(cadastroComplementar),
                                            HttpContext.Current.User.Identity.Name,
                                            UtilitarioWeb.GetIP(Request),
                                            "",
                                            HistoricoLogOrigem.Outros);
            #endregion
        }

        e.Cancel = true;
        gridCadastro.CancelEdit();
        gridCadastro.DataBind();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_RowInserting(object sender, DevExpress.Web.Data.ASPxDataInsertingEventArgs e)
    {
        this.Cadastra();

        e.Cancel = true;
        gridCadastro.CancelEdit();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_CustomCallback(object sender, ASPxGridViewCustomCallbackEventArgs e)
    {
        if (e.Parameters == "btnDelete")
        {
            List<object> keyValuesId = gridCadastro.GetSelectedFieldValues("IdCadastroComplementares");
            List<object> tipoCadastro = gridCadastro.GetSelectedFieldValues("TipoCadastro");
            for (int i = 0; i < keyValuesId.Count; i++)
            {
                int idCadastroComplementares = Convert.ToInt32(keyValuesId[i]);
                cadastroComplementar = new CadastroComplementar();

                if (cadastroComplementar.LoadByPrimaryKey(idCadastroComplementares))
                {
                    string idMercadoTipoPessoa = cadastroComplementar.IdMercadoTipoPessoa;

                    cadastroComplementarCamposCollection = new CadastroComplementarCamposCollection();
                    cadastroComplementarCamposQuery = new CadastroComplementarCamposQuery();
                    cadastroComplementarCollection = new CadastroComplementarCollection();
                    cadastroComplementarQuery = new CadastroComplementarQuery();

                    cadastroComplementarCamposQuery.Select(cadastroComplementarCamposQuery.IdCamposComplementares);
                    cadastroComplementarCamposQuery.Where(cadastroComplementarCamposQuery.TipoCadastro == tipoCadastro[i]);

                    if (cadastroComplementarCamposCollection.Load(cadastroComplementarCamposQuery))
                    {
                        List<int?> lstCamposComplementares = new List<int?>();

                        for (int k = 0; k < cadastroComplementarCamposCollection.Count; k++)
                        {
                            lstCamposComplementares.Add(Convert.ToInt32(cadastroComplementarCamposCollection[k].IdCamposComplementares));
                        }

                        cadastroComplementarQuery.Select(cadastroComplementarQuery.IdCadastroComplementares);
                        cadastroComplementarQuery.Where(cadastroComplementarQuery.IdMercadoTipoPessoa == idMercadoTipoPessoa,
                                                        cadastroComplementarQuery.IdCamposComplementares.In(lstCamposComplementares.ToArray()));
                        cadastroComplementarCollection.Load(cadastroComplementarQuery);

                        CadastroComplementarCollection cadastroComplementarCollectionClone = (CadastroComplementarCollection)Utilitario.Clone(cadastroComplementarCollection);

                        cadastroComplementarCollection.MarkAllAsDeleted();
                        cadastroComplementarCollection.Save();

                        for (int j = 0; j < cadastroComplementarCollectionClone.Count; j++)
                        {
                            #region Log do Processo
                            HistoricoLog historicoLog = new HistoricoLog();
                            historicoLog.InsereHistoricoLog(DateTime.Now,
                                                            DateTime.Now,
                                                            "Cadastro de CadastroComplementar - Operacao: Delete CadastroComplementar: " + cadastroComplementarCollectionClone[j].IdCadastroComplementares + "; " + UtilitarioWeb.ToString(cadastroComplementarCollectionClone[j]),
                                                            HttpContext.Current.User.Identity.Name,
                                                            UtilitarioWeb.GetIP(Request),
                                                            "",
                                                            HistoricoLogOrigem.Outros);
                            #endregion
                        }
                    }
                }
            }
        }

        gridCadastro.DataBind();
        gridCadastro.Selection.UnselectAll();
        gridCadastro.CancelEdit();
    }

    /// <summary>
    /// Exibe a SequÃªncia dos registros na Grid
    /// </summary>
    protected void gridCadastro_CustomColumnDisplayText(object sender, ASPxGridViewColumnDisplayTextEventArgs e)
    {
        if (e.Column.FieldName == "TipoCadastro")
        {
            e.DisplayText = ListaCadastroDescricao.RetornaStringValue(Convert.ToInt32(e.Value));
        }
        if (e.Column.FieldName == "TipoCampo")
        {
            //Verifica se o valor é negativo, referenciando a lista de ENUM
            if (Convert.ToInt32(e.Value) < 0)
                e.DisplayText = ListaCadastroComplementarDescricao.RetornaStringValue(Convert.ToInt32(e.Value));
            else
                e.DisplayText = ListaTipoCampoDescricao(Convert.ToInt32(e.Value));
        }
    }

    /// <summary>
    /// Fecha o EditForm e atualiza a grid
    /// Evento Ã© chamado quando o usuÃ¡rio clica no botÃ£o para fechar a grid
    /// </summary>
    protected void gridCadastro_CancelRowEditing(object sender, ASPxStartRowEditingEventArgs e)
    {
        e.Cancel = false;
        gridCadastro.DataBind();
    }

    /// <summary>
    /// Carrega os combobox dos filtros de pesquisa da grid
    /// </summary>
    protected void gridCadastro_PreRender(object sender, EventArgs e)
    {
        #region Cadastro
        //Busca o controle de filtro da coluna CADASTRO da grid
        GridViewDataComboBoxColumn cadastro = gridCadastro.Columns["Cadastro"] as GridViewDataComboBoxColumn;
        //Adiciona registro vazio para limpar o filtro
        cadastro.PropertiesComboBox.Items.Add(string.Empty);
        //Percorre o ENUM para preencher o controle do combobox
        foreach (int r in Enum.GetValues(typeof(ListaCadastro)))
        {
            cadastro.PropertiesComboBox.Items.Add(ListaCadastroDescricao.RetornaStringValue(r), r.ToString());
        }
        #endregion

        #region Tipo Campo
        //Busca o controle de filtro da coluna CADASTRO da grid
        GridViewDataComboBoxColumn tipoCampo = gridCadastro.Columns["TipoCampo"] as GridViewDataComboBoxColumn;
        //Limpa a lista de items para remover possiveis items duplicados
        tipoCampo.PropertiesComboBox.Items.Clear();
        //Adiciona registro vazio para limpar o filtro
        tipoCampo.PropertiesComboBox.Items.Add(string.Empty);
        //Percorre o ENUM para preencher o controle do combobox
        foreach (int r in Enum.GetValues(typeof(ListaCadastroComplementar)))
        {
            if (r == -1 || r == -2 || r == -3)
                tipoCampo.PropertiesComboBox.Items.Add(ListaCadastroComplementarDescricao.RetornaStringValue(r), r.ToString());
        }

        //Carrega Lista de campos cadastrados
        cadastroComplementarTipoListaCollection = new CadastroComplementarTipoListaCollection();
        cadastroComplementarTipoListaCollection.Query.Select();
        cadastroComplementarTipoListaCollection.Query.Load();

        foreach (CadastroComplementarTipoLista item in cadastroComplementarTipoListaCollection)
        {
            tipoCampo.PropertiesComboBox.Items.Add(new ListEditItem(item.NomeLista, item.IdTipoLista));
        }
        #endregion

    }

    protected void gridCadastro_CellEditorInitialize(object sender, ASPxGridViewEditorEventArgs e)
    {
        if (e.Column.FieldName != "ValorCampo")
        {
            e.Column.ReadOnly = true;
            e.Editor.ReadOnly = true;
            e.Editor.Enabled = false;
        }
    }

    #endregion


    #region OnLoad
    /// <summary>
    /// Carrega o dropdown com o valores do Enum
    /// </summary>
    protected void dropCadastro_OnLoad(object sender, EventArgs e)
    {
        ASPxComboBox dropTipoCadastro = (ASPxComboBox)sender;
        dropTipoCadastro.Items.Clear();
        foreach (int r in Enum.GetValues(typeof(ListaCadastro)))
        {
            dropTipoCadastro.Items.Add(ListaCadastroDescricao.RetornaStringValue(r), r.ToString());
        }
    }

    /// <summary>
    /// Carrega o Mercado de acordo com o cadastro selecionado
    /// </summary>
    protected void dropMercado_OnLoad(object sender, EventArgs e)
    {
        string mercado = dropCadastro.SelectedItem != null ? dropCadastro.SelectedItem.Text : string.Empty;
        if (mercado.Equals("Cotista"))
        {
            cotistaCollection = new CotistaCollection();
            cotistaCollection.Query.Select(cotistaCollection.Query.IdCotista.As("Id"), cotistaCollection.Query.Nome);
            cotistaCollection.Query.Load();
            dropMercado.GridView.DataSource = cotistaCollection;
        }
        else if (mercado.Equals("Carteira/Fundo"))
        {
            clienteCollection = new ClienteCollection();
            clienteCollection.Query.Select(clienteCollection.Query.IdCliente.As("Id"), clienteCollection.Query.Nome);
            clienteCollection.Query.Load();
            dropMercado.GridView.DataSource = clienteCollection;
        }
        else if (mercado.Equals("Emissores"))
        {
            emissorCollection = new EmissorCollection();
            emissorCollection.Query.Select(emissorCollection.Query.IdEmissor.As("Id"), emissorCollection.Query.Nome);
            emissorCollection.Query.Load();
            dropMercado.GridView.DataSource = emissorCollection;
        }
        else if (mercado.Equals("Ativo Bolsa"))
        {
            ativoBolsaCollection = new AtivoBolsaCollection();
            ativoBolsaCollection.Query.Select(ativoBolsaCollection.Query.CdAtivoBolsa.As("Id"), ativoBolsaCollection.Query.Descricao.As("Nome"));
            ativoBolsaCollection.Query.Load();
            dropMercado.GridView.DataSource = ativoBolsaCollection;
        }
        else if (mercado.Equals("Ativo BMF"))
        {
            string fieldChave = "Id";
            string fieldNome = "Nome";
            ativoBMFCollection = new AtivoBMFCollection();
            ativoBMFCollection.Query.Select((ativoBMFCollection.Query.CdAtivoBMF.Cast(esCastType.String)).As(fieldChave), (ativoBMFCollection.Query.CdAtivoBMF.Cast(esCastType.String)).As(fieldNome));
            ativoBMFCollection.Query.es.Distinct = true;
            ativoBMFCollection.Query.Load();
            dropMercado.GridView.DataSource = ativoBMFCollection;
        }
        else if (mercado.Equals("Renda Fixa Titulo"))
        {
            tituloRendaFixaCollection = new TituloRendaFixaCollection();
            tituloRendaFixaCollection.Query.Select(tituloRendaFixaCollection.Query.IdTitulo.As("Id"), tituloRendaFixaCollection.Query.DescricaoCompleta.As("Nome"));
            tituloRendaFixaCollection.Query.Where(tituloRendaFixaCollection.Query.DescricaoCompleta.NotEqual(string.Empty));
            tituloRendaFixaCollection.Query.Load();
            dropMercado.GridView.DataSource = tituloRendaFixaCollection;
        }
        else if (mercado.Equals("Renda Fixa Papel"))
        {
            papelRendaFixaCollection = new PapelRendaFixaCollection();
            papelRendaFixaCollection.Query.Select(papelRendaFixaCollection.Query.IdPapel.As("Id"), papelRendaFixaCollection.Query.Descricao.As("Nome"));
            papelRendaFixaCollection.Query.Load();
            dropMercado.GridView.DataSource = papelRendaFixaCollection;
        }
        else
        {
            //Busca dados do mercado que serão utilizados para popular o combo de mercado
            if (!string.IsNullOrEmpty(mercado))
                dropMercado.GridView.DataSource = ListaMercadoTipoPessoa(mercado);
        }
        dropMercado.GridView.DataBind();
    }
    #endregion

    #region OnClick
    /// <summary>
    /// Gera controles na tela
    /// </summary>
    protected void btnGerarCadastro_OnClick(object sender, EventArgs e)
    {
        CriaControleTela();
    }
    #endregion

    #region Suporte
    /// <summary>
    /// Cria dinamicamente os controle na tela
    /// </summary>
    /// <typeparam name="T">Tipo do controle a ser criado</typeparam>
    /// <param name="idControle">ID do controle a ser criado</param>
    /// <param name="textoControle">Texto do controle a ser exibido na tela</param>
    /// <returns>Controle com as propriedades principais criadas</returns>
    private T CriaControleTela<T>(string idControle, string textoControle, string cssclass, int? tamanho, int? tipoCampo) where T : new()
    {
        //Instancia o tipo de controle a ser criado
        T controleCriado = new T();

        //Propriedade ID do controle
        PropertyInfo id = controleCriado.GetType().GetProperty("ID");
        id.SetValue(controleCriado, idControle, null);

        //Propriedade Client ID do controle
        PropertyInfo clientId = controleCriado.GetType().GetProperty("ClientInstanceName");
        clientId.SetValue(controleCriado, idControle, null);

        //Propriedade TEXT do controle
        if (!string.IsNullOrEmpty(textoControle))
        {
            PropertyInfo texto = controleCriado.GetType().GetProperty("Text");
            texto.SetValue(controleCriado, textoControle, null);
        }

        //Aplicar o valor do CSSCLASS para o controle
        if (!string.IsNullOrEmpty(cssclass))
        {
            PropertyInfo css = controleCriado.GetType().GetProperty("CssClass");
            css.SetValue(controleCriado, cssclass, null);
        }

        //Aplicar o valor do MAXLENGTH para o controle
        if (tamanho != 0)
        {
            PropertyInfo maxlength = controleCriado.GetType().GetProperty("MaxLength");
            maxlength.SetValue(controleCriado, tamanho, null);
        }

        

        //Retorna o controle com as propriedades preenchidas
        return controleCriado;
    }

    /// <summary>
    /// Armazena os controles da tela na lista 
    /// </summary>
    /// <typeparam name="T">Tipo do controle a ser encontrado</typeparam>
    /// <param name="controlCollection">Container onde os controles irão ser encontrados</param>
    /// <param name="resultCollection">Lista que irá armazenar os controles do container</param>
    private void ListaControles<T>(ControlCollection controlCollection, List<T> listaPreenchida) where T : Control
    {
        foreach (Control control in controlCollection)
        {
            //Verifica se o controle encontrado é do tipo especificado
            if (control is T)
                listaPreenchida.Add((T)control);

            //Verifica se há controles a serem percorridos
            if (control.HasControls())
                ListaControles(control.Controls, listaPreenchida);
        }
    }

    /// <summary>
    /// Pega os controles da tela e valida os seus valores
    /// </summary>
    protected void btnGerarCadastro_Click(object sender, EventArgs e)
    {
        CriaControleTela();
    }

    /// <summary>
    /// Recria os controls na tela
    /// </summary>
    private void CriaControleTela()
    {
        //Instancia
        cadastroComplementarCamposCollection = new CadastroComplementarCamposCollection();
        cadastroComplementarItemListaCollection = new CadastroComplementarItemListaCollection();

        //Executa a query 
        Panel p = popupDinamico.FindControl("panelDinamico") as Panel;

        if (p != null)
        {
            if (dropCadastro != null && dropCadastro.SelectedItem != null)
            {
                cadastroComplementarCamposCollection.Query.Where(cadastroComplementarCamposCollection.Query.TipoCadastro == dropCadastro.SelectedItem.Value);
                cadastroComplementarCamposCollection.Query.Select();
                cadastroComplementarCamposCollection.LoadAll();
            }

            //Percorre os registros encontrados no cadastro complementar
            foreach (CadastroComplementarCampos item in cadastroComplementarCamposCollection)
            {
                string valor = "";
                if (dropMercado.Value != null)
                {
                    CadastroComplementar e = new CadastroComplementar();
                    CadastroComplementarQuery query = new CadastroComplementarQuery();

                    query.Where(query.IdCamposComplementares == item.IdCamposComplementares & query.IdMercadoTipoPessoa == dropMercado.Value.ToString());
                    e.Load(query);
                    valor = e.ValorCampo;
                }
                string cssclass = string.Empty;
                if (item.CampoObrigatorio == "S")
                    cssclass = "labelRequired";
                
                //Verifica se o campo é uma lista
                if (!item.TipoCampo.Equals(-1) && !item.TipoCampo.Equals(-2) && !item.TipoCampo.Equals(-3))
                {
                    ASPxLabel label = p.FindControl("label" + Separador() + item.NomeCampo) as ASPxLabel;
                    if (label == null)
                        p.Controls.Add(CriaControleTela<ASPxLabel>("label" + Separador() + item.NomeCampo, item.DescricaoCampo, cssclass, 0, 0));

                    ASPxComboBox combo = p.FindControl("drop" + Separador() + item.NomeCampo) as ASPxComboBox;
                    if (combo == null)
                    {
                        p.Controls.Add(CriaControleTela<ASPxComboBox>("drop" + Separador() + item.NomeCampo, string.Empty, string.Empty, item.Tamanho, 0));
                        #region DROPDINAMICO
                        //Instancia controle dinâmico
                        ASPxComboBox dropDinamico = p.FindControl("drop" + Separador() + item.NomeCampo) as ASPxComboBox;
                        //Adiciona os items na lista
                        if (dropDinamico != null)
                        {
                            cadastroComplementarItemListaCollection = new CadastroComplementarItemListaCollection();
                            cadastroComplementarItemListaCollection.Query.Select();
                            cadastroComplementarItemListaCollection.Query.Where(cadastroComplementarItemListaCollection.Query.IdTipoLista == item.TipoCampo);
                            cadastroComplementarItemListaCollection.Query.Load();
                            dropDinamico.DataSource = cadastroComplementarItemListaCollection;
                            dropDinamico.TextField = cadastroComplementarItemListaCollection.Query.ItemLista;
                            dropDinamico.ValueField = cadastroComplementarItemListaCollection.Query.IdLista;
                            dropDinamico.DataBind();

                            dropDinamico.Value = valor;
                        }
                        #endregion
                    }
                }
                else
                {
                    ASPxLabel label = p.FindControl("label" + Separador() + item.NomeCampo) as ASPxLabel;
                    if (label == null)
                        p.Controls.Add(CriaControleTela<ASPxLabel>("label" + Separador() + item.NomeCampo, item.DescricaoCampo, cssclass, 0, 0));

                    ASPxTextBox text = p.FindControl("text" + Separador() + item.NomeCampo) as ASPxTextBox;
                    if (text == null)
                    {
                        ASPxTextBox t = CriaControleTela<ASPxTextBox>("text" + Separador() + item.NomeCampo, item.ValorDefault, string.Empty, item.Tamanho, item.TipoCampo);
                        VerificaEventoTipoCampo(t, item.TipoCampo, item.CasasDecimais, item.Tamanho);
                        p.Controls.Add(t);

                        t.Text = valor;
                    }
                }

                p.Controls.Add(new LiteralControl("<br/>"));
            }
        }
    }

    /// <summary>
    /// Insere de um novo registro
    /// </summary>
    private void Cadastra()
    {
        #region Lista campos obrigatorios
        //Instancia lista de controles obrigatórios
        List<string> listaCamposObrigatorios = new List<string>();
        if (dropCadastro.SelectedItem == null || dropCadastro.SelectedItem.Value.Equals(string.Empty))
            throw new Exception("Campos com * são obrigatórios!");

        if (string.IsNullOrEmpty(dropMercado.Text))
            throw new Exception("Campos com * são obrigatórios!");

        foreach (Control control in popupDinamico.FindControl("panelDinamico").Controls)
        {
            if (control is ASPxLabel)
            {
                //Verifica se o campo tem preenchimento obrigatório
                if (control.GetType().GetProperty("CssClass").GetValue(control, null).Equals("labelRequired"))
                {
                    //Devolve o nome da propriedade
                    string controle = control.ClientID.Substring(control.ClientID.IndexOf("label")).Replace("label", "");
                    //Adiciona os campos obrigatórios
                    listaCamposObrigatorios.Add(controle);
                }
            }
        }
        #endregion

        #region Preenche Entidade
        try
        {
            #region Verifica campos obrigatorios
            //Percorre a lista de controles
            foreach (Control control in popupDinamico.FindControl("panelDinamico").Controls)
            {
                //Instancia 
                cadastroComplementar = new CadastroComplementar();

                //Verifica somente os controles que possuem valor para preencher a entidade
                if (!(control is ASPxLabel))
                {
                    //Instancia valor que recebe o valor dos controles
                    object valor = new object();

                    //Retira os prefixos dos controles para encontrar as propriedades da entidade(WebControls e Controles do DevExpress)
                    //string propriedade = control.ClientID.Substring(control.ClientID.LastIndexOf("_")).Replace("text", "").Replace("drop", "").Replace("_", "");
                    string propriedade = "";
                    if(!(control.ClientID.LastIndexOf(Separador()) < 0))
                        propriedade = control.ClientID.Substring(control.ClientID.LastIndexOf(Separador())).Replace(Separador(), "");

                    if (!string.IsNullOrEmpty(propriedade))
                    {
                        valor = control.GetType().GetProperty("Text").GetValue(control, null);

                        //Verifica se há campos obrigatórios
                        if (listaCamposObrigatorios.Contains(propriedade) && valor.Equals(string.Empty))
                            throw new Exception("Campos com * são obrigatórios!");
                    }
                }
            }
            #endregion

            using(TransactionScope trans = new TransactionScope())
            {
                CadastroComplementarCollection coll = new CadastroComplementarCollection();
                coll.Query.Where(coll.Query.IdMercadoTipoPessoa == dropMercado.Value.ToString());
                coll.LoadAll();
                coll.MarkAllAsDeleted();
                coll.Save();
                //Percorre a lista de controles
                foreach (Control control in popupDinamico.FindControl("panelDinamico").Controls)
                {
                    //Instancia 
                    cadastroComplementar = new CadastroComplementar();

                    //Verifica somente os controles que possuem valor para preencher a entidade
                    if (!(control is ASPxLabel))
                    {
                        //Instancia valor que recebe o valor dos controles
                        object valor = new object();

                        //Retira os prefixos dos controles para encontrar as propriedades da entidade(WebControls e Controles do DevExpress)
                        //string propriedade = control.ClientID.Substring(control.ClientID.LastIndexOf("_")).Replace("text", "").Replace("drop", "").Replace("_", "");

                        string propriedade = "";
                        if (!(control.ClientID.LastIndexOf(Separador()) < 0))
                            propriedade = control.ClientID.Substring(control.ClientID.LastIndexOf(Separador())).Replace(Separador(), "");

                        if (!string.IsNullOrEmpty(propriedade))
                        {
                            valor = control.GetType().GetProperty("Text").GetValue(control, null);
                            cadastroComplementar.IdCamposComplementares = ChaveCamposComplementares(Convert.ToInt32(dropCadastro.SelectedItem.Value), propriedade);
                            cadastroComplementar.DescricaoMercadoTipoPessoa = dropMercado.Text.Substring(0, (dropMercado.Text.Length < 50 ? dropMercado.Text.Length : 50));
                            cadastroComplementar.IdMercadoTipoPessoa = dropMercado.Value.ToString();
                            cadastroComplementar.ValorCampo = valor.ToString();
                            cadastroComplementar.Save();
                        }
                    }
                    #region Log do Processo
                    HistoricoLog historicoLog = new HistoricoLog();
                    historicoLog.InsereHistoricoLog(DateTime.Now,
                                                    DateTime.Now,
                                                    "Cadastro Complementar Campos - Operacao: Insert CadastroComplementarCampo: " + camposComplementares.IdCamposComplementares + UtilitarioWeb.ToString(camposComplementares),
                                                    HttpContext.Current.User.Identity.Name,
                                                    UtilitarioWeb.GetIP(Request),
                                                    "",
                                                    HistoricoLogOrigem.Outros);
                    #endregion
                }

                trans.Complete();                
            }
        }
        catch(System.Data.SqlClient.SqlException ex)
        {
            if (ex.Number == 2627)
            {
                throw new Exception("Erro: cadastro complementar ja existente. Apague o cadastro para prosseguir");
            }
        }
        catch (Exception ex)
        {
            
            throw;
        }
        #endregion
    }

    /// <summary>
    /// Retorna a descrição da lista cadastrada
    /// </summary>
    /// <param name="id">Identificador da lista</param>
    /// <returns>Nome da lista</returns>
    private string ListaTipoCampoDescricao(int id)
    {
        //Instancia entidades
        cadastroComplementarTipoListaCollection = new CadastroComplementarTipoListaCollection();
        cadastroComplementarTipoLista = new CadastroComplementarTipoLista();

        //Carrega Lista de campos cadastrados
        cadastroComplementarTipoListaCollection.Query.Select(cadastroComplementarTipoListaCollection.Query.NomeLista);
        cadastroComplementarTipoListaCollection.Query.Where(cadastroComplementarTipoListaCollection.Query.IdTipoLista == id);
        cadastroComplementarTipoLista.Load(cadastroComplementarTipoListaCollection.Query);

        return cadastroComplementarTipoLista.NomeLista;
    }

    /// <summary>
    /// Pequisa a lista de mercado a ser carregado na tela
    /// </summary>
    private AgenteMercadoCollection ListaMercadoTipoPessoa(string mercado)
    {
        //Instãncia
        agenteMercadoCollection = new AgenteMercadoCollection();

        //Seleciona o nome do Mercado/Tipo Pessoa
        agenteMercadoCollection.Query.Select(agenteMercadoCollection.Query.IdAgente.As("Id"), agenteMercadoCollection.Query.Nome);

        //Verifica qual o mercado solicitado
        switch (mercado)
        {
            case "Administradores":
                agenteMercadoCollection.Query.Where(agenteMercadoCollection.Query.FuncaoAdministrador == "S");
                agenteMercadoCollection.Query.Load();
                break;
            case "Custodiantes":
                agenteMercadoCollection.Query.Where(agenteMercadoCollection.Query.FuncaoCustodiante == "S");
                agenteMercadoCollection.Query.Load();
                break;
            case "Corretora":
                agenteMercadoCollection.Query.Where(agenteMercadoCollection.Query.FuncaoCorretora == "S");
                agenteMercadoCollection.Query.Load();
                break;
            case "Gestor":
                agenteMercadoCollection.Query.Where(agenteMercadoCollection.Query.FuncaoGestor == "S");
                agenteMercadoCollection.Query.Load();
                break;
            case "Liquidante":
                agenteMercadoCollection.Query.Where(agenteMercadoCollection.Query.FuncaoLiquidante == "S");
                agenteMercadoCollection.Query.Load();
                break;
            case "Distribuidor":
                agenteMercadoCollection.Query.Where(agenteMercadoCollection.Query.FuncaoDistribuidor == "S");
                agenteMercadoCollection.Query.Load();
                break;
            default:
                break;
        }

        //Retorna a lista preenchida
        return agenteMercadoCollection;
    }

    /// <summary>
    /// Pesquisa a chave primaria dos campos a serem cadastrados
    /// </summary>
    /// <param name="tipoCadastro">Verifica qual o tipo de cadastro selecionado</param>
    /// <param name="nomeCampo">campo a ser pesquisado na base</param>
    /// <returns>chave dos campos</returns>
    private int? ChaveCamposComplementares(int tipoCadastro, string nomeCampo)
    {
        //Instancia
        cadastroComplementarCamposCollection = new CadastroComplementarCamposCollection();
        cadastroComplementarCamposQuery = new CadastroComplementarCamposQuery();

        //Executa query para encontrar o ID dos campos
        cadastroComplementarCamposQuery.Select(cadastroComplementarCamposQuery.IdCamposComplementares);
        cadastroComplementarCamposQuery.Where(cadastroComplementarCamposQuery.TipoCadastro == tipoCadastro,
                                              cadastroComplementarCamposQuery.NomeCampo == nomeCampo);
        cadastroComplementarCamposCollection.Load(cadastroComplementarCamposQuery);

        //Retorna o ID
        return cadastroComplementarCamposCollection[0].IdCamposComplementares;
    }

    /// <summary>
    /// Atribui propriedades para o controle
    /// </summary>
    /// <param name="t">Controle a ser atribuido</param>
    /// <param name="tipoCampo">Numerico, Data, Alfanumerico</param>
    /// <param name="casaDecimal">numero de casas decimais a ser atribuido</param>
    /// <param name="tamanho">tamanho total do textbox</param>
    private void VerificaEventoTipoCampo(ASPxTextBox t, int? tipoCampo, int? casaDecimal, int? tamanho)
    {
        string numero = string.Empty;
        string casaDec = string.Empty;

        //Numerico
        if (tipoCampo == -1)
        {
            if (casaDecimal != 0 && tamanho != 0)
                t.MaskSettings.Mask = "<0.." + casaDec.PadRight(Convert.ToInt32(tamanho), '9') + ">.<0.." + casaDec.PadRight(Convert.ToInt32(casaDecimal), '9') + ">";
            else if (casaDecimal != 0)
                t.MaskSettings.Mask = "<0.." + casaDec.PadRight(Convert.ToInt32(250), '9') + ">.<0.." + casaDec.PadRight(Convert.ToInt32(casaDecimal), '9') + ">";
            else if (tamanho != 0)
                t.MaskSettings.Mask = "<0.." + casaDec.PadRight(Convert.ToInt32(tamanho), '9') + ">";
            else
                t.MaskSettings.Mask = "<0.." + casaDec.PadRight(250, '9') + ">";
        }
        //Data
        if (tipoCampo == -3)
        {
            t.MaskSettings.Mask = "dd/MM/yyyy";
            if (string.IsNullOrEmpty(t.Text))
                t.Text = DateTime.Now.ToString();
        }
        else
        {
            t.Width = Unit.Percentage(100);
        }
    }
    #endregion

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void EsDSCadastroComplementarItemLista_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        CadastroComplementarItemListaCollection cadastroComplementarItemListaColl = new CadastroComplementarItemListaCollection();

        ASPxTextBox textTipoCampo = gridCadastro.FindEditFormTemplateControl("textTipoCampo") as ASPxTextBox;

        if (!string.IsNullOrEmpty(textTipoCampo.Text))
        {
            cadastroComplementarItemListaColl.Query.Select();
            cadastroComplementarItemListaColl.Query.Where(cadastroComplementarItemListaColl.Query.IdTipoLista.Equal(Convert.ToInt32(textTipoCampo.Text)));
            cadastroComplementarItemListaColl.Query.Load();
        }

        e.Collection = cadastroComplementarItemListaColl;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void callbackTextCampo_Callback(object sender, DevExpress.Web.CallbackEventArgs e)
    {
        e.Result = "500";

        TextBox textValorCampo = gridCadastro.FindEditFormTemplateControl("textValorCampo") as TextBox;

        if (textValorCampo != null)
        {
            int idCadastroComplementares = Convert.ToInt32(this.gridCadastro.GetRowValues(this.gridCadastro.EditingRowVisibleIndex, CadastroComplementarMetadata.ColumnNames.IdCadastroComplementares));

            CadastroComplementar cadastroComplementar = new CadastroComplementar();

            if (cadastroComplementar.LoadByPrimaryKey(idCadastroComplementares))
                e.Result = Convert.ToString(cadastroComplementar.UpToCadastroComplementarCamposByIdCamposComplementares.Tamanho.GetValueOrDefault(500));

        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void textTipoCampo_Init(object sender, EventArgs e)
    {        
        int tipoCampo = Convert.ToInt32(this.gridCadastro.GetRowValues(this.gridCadastro.EditingRowVisibleIndex, "TipoCampo"));

        if (tipoCampo == -1 || tipoCampo == -2 || tipoCampo == -3)
        {
            (sender as ASPxTextBox).Text = ListaCadastroComplementarDescricao.RetornaStringValue(tipoCampo);
            return;
        }
        
        //Carrega Lista de campos cadastrados
        CadastroComplementarTipoLista cadastroComplementarTipoLista = new CadastroComplementarTipoLista();
        cadastroComplementarTipoLista.LoadByPrimaryKey(tipoCampo);

        (sender as ASPxTextBox).Text = cadastroComplementarTipoLista.NomeLista;
    }
}