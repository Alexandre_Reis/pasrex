﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Collections.Generic;
using System.Text;
using System.Drawing;

using Financial.Common;
using Financial.Util;
using Financial.Investidor;
using Financial.ContaCorrente;
using Financial.Security;
using Financial.Fundo;
using Financial.InvestidorCotista;
using Financial.Bolsa;
using Financial.RendaFixa;
using Financial.Web.Common;

using DevExpress.Web;
using DevExpress.Web.Data;

using Financial.ContaCorrente.Enums;
using Financial.InvestidorCotista.Enums;
using Financial.Investidor.Enums;
using Financial.Common.Enums;
using Financial.Bolsa.Enums;
using Financial.RendaFixa.Enums;
using System.Threading;
using EntitySpaces.Interfaces;
using Financial.Security.Enums;
using Financial.Web.Util;
using System.Reflection;
using System.ComponentModel;

public partial class CadastroComplementar_CadastroComplementarListas : CadastroBasePage
{
    #region Instância de Classes
    //Item Lista
    CadastroComplementarItemListaCollection cadastroComplementarItemListaCollection = new CadastroComplementarItemListaCollection();
    CadastroComplementarItemLista cadastroComplementarItemLista = new CadastroComplementarItemLista();
    //Tipo Lista
    CadastroComplementarTipoListaCollection cadastroComplementarTipoListaCollection = new CadastroComplementarTipoListaCollection();
    CadastroComplementarTipoLista cadastroComplementarTipoLista = new CadastroComplementarTipoLista();
    //Agente Mercado
    AgenteMercadoCollection agenteMercadoCollection = new AgenteMercadoCollection();
    //Emissor
    EmissorCollection emissorCollection = new EmissorCollection();
    #endregion

    #region Entity Space - Base de Dados
    protected void EsDSCadastroComplementarItemLista_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        CadastroComplementarTipoListaQuery TipoListaQuery = new CadastroComplementarTipoListaQuery("tl");
        CadastroComplementarItemListaQuery ItemListaQuery = new CadastroComplementarItemListaQuery("il");

        ItemListaQuery.InnerJoin(TipoListaQuery).On(TipoListaQuery.IdTipoLista == ItemListaQuery.IdTipoLista);
        ItemListaQuery.Where(TipoListaQuery.TipoLista.NotIn(-4, -5, -6, -7, -8, -9, -10, -11, -12, -13));
        ItemListaQuery.OrderBy(ItemListaQuery.IdLista.Ascending);
        cadastroComplementarItemListaCollection.Load(ItemListaQuery);

        e.Collection = cadastroComplementarItemListaCollection;
    }
    protected void EsDSCadastroComplementarTipoLista_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        cadastroComplementarTipoListaCollection = new CadastroComplementarTipoListaCollection();

        cadastroComplementarTipoListaCollection.Query.OrderBy(cadastroComplementarTipoListaCollection.Query.IdTipoLista.Ascending);
        cadastroComplementarTipoListaCollection.Query.Load();
        e.Collection = cadastroComplementarTipoListaCollection;
    }
    #endregion

    #region Callback
    /// <summary>
    /// 
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void callbackAdd_Callback(object source, DevExpress.Web.CallbackEventArgs e)
    {
        e.Result = "";

        //Procura os controles no popup de tipo de lista
        ASPxCallbackPanel cbPanel = gridCadastro.FindEditFormTemplateControl("cbPanel") as ASPxCallbackPanel;
        ASPxCallbackPanel cbItemLista = gridCadastro.FindEditFormTemplateControl("cbItemLista") as ASPxCallbackPanel;

        ASPxComboBox dropIdTipoLista = cbPanel.FindControl("dropIdTipoLista") as ASPxComboBox;
        ASPxTextBox textItemLista = cbItemLista.FindControl("textItemLista") as ASPxTextBox;

        if (dropIdTipoLista.SelectedItem != null || !string.IsNullOrEmpty(textItemLista.Text))
            this.Cadastra();
        else
            e.Result = "Campos com * são obrigatórios!";

        gridCadastro.CancelEdit();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void callbackErro_Callback(object source, DevExpress.Web.CallbackEventArgs e)
    {
        e.Result = "";

        if (!e.Parameter.Equals("delete"))
        {

            //Busca controles na tela
            ASPxCallbackPanel cbPanel = gridCadastro.FindEditFormTemplateControl("cbPanel") as ASPxCallbackPanel;
            ASPxCallbackPanel cbItemLista = gridCadastro.FindEditFormTemplateControl("cbItemLista") as ASPxCallbackPanel;
            ASPxComboBox dropIdTipoLista = cbPanel.FindControl("dropIdTipoLista") as ASPxComboBox;
            ASPxTextBox textItemLista = cbItemLista.FindControl("textItemLista") as ASPxTextBox;

            if (string.IsNullOrEmpty(textItemLista.Text) || dropIdTipoLista.SelectedItem == null)
                e.Result = "Campos com * devem ser preenchidos!";
        }
    }

    /// <summary>
    /// Exibe o popup de cadastro de tipo de lista
    /// </summary>
    protected void callbackPanel_Callback(object sender, DevExpress.Web.CallbackEventArgs e)
    {
        if (!String.IsNullOrEmpty(e.Parameter) && e.Parameter != "null")
            e.Result = e.Parameter;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_CustomCallback(object sender, ASPxGridViewCustomCallbackEventArgs e)
    {
        if (e.Parameters == "btnDelete")
        {
            List<object> keyValuesId = gridCadastro.GetSelectedFieldValues("IdLista");
            for (int i = 0; i < keyValuesId.Count; i++)
            {
                int idLista = Convert.ToInt32(keyValuesId[i]);
                cadastroComplementarItemLista = new CadastroComplementarItemLista();

                if (cadastroComplementarItemLista.LoadByPrimaryKey(idLista))
                {
                    CadastroComplementarItemLista cadastroComplementarItemListaClone = (CadastroComplementarItemLista)Utilitario.Clone(cadastroComplementarItemLista);
                    cadastroComplementarItemLista.MarkAsDeleted();
                    cadastroComplementarItemLista.Save();

                    #region Log do Processo
                    HistoricoLog historicoLog = new HistoricoLog();
                    historicoLog.InsereHistoricoLog(DateTime.Now,
                                                    DateTime.Now,
                                                    "Cadastro de CadastroComplementarListas - Operacao: Delete CadastroComplementarListas: " + idLista + "; " + UtilitarioWeb.ToString(cadastroComplementarItemListaClone),
                                                    HttpContext.Current.User.Identity.Name,
                                                    UtilitarioWeb.GetIP(Request),
                                                    "",
                                                    HistoricoLogOrigem.Outros);
                    #endregion
                }
            }
        }

        gridCadastro.DataBind();
        gridCadastro.Selection.UnselectAll();
        gridCadastro.CancelEdit();
    }

    /// <summary>
    /// Efetua o cadastro do Tipo de Lista
    /// </summary>
    protected void callBackTipoLista_Callback(object sender, DevExpress.Web.CallbackEventArgs e)
    {
        e.Result = "";

        //Procura os controles no popup de tipo de lista
        //ASPxCallbackPanel cbPanel = gridCadastro.FindEditFormTemplateControl("cbPanel") as ASPxCallbackPanel;
        //ASPxCallbackPanel cbItemLista = gridCadastro.FindEditFormTemplateControl("cbItemLista") as ASPxCallbackPanel;

        ASPxComboBox dropTipoLista = popupTipoLista.FindControl("dropTipoLista") as ASPxComboBox;
        ASPxTextBox textNomeLista = popupTipoLista.FindControl("textNomeLista") as ASPxTextBox;

        if (dropTipoLista.SelectedItem != null && !string.IsNullOrEmpty(textNomeLista.Text))
        {
            this.CadastraTipoLista();
            e.Result = "Lista cadastrada com sucesso";
        }
        else
            e.Result = "Campos com * são obrigatórios!";

        gridCadastro.CancelEdit();
    }

    /// <summary>
    /// Deleta os registros
    /// </summary>
    protected void callBackDeleteTipoCampo_Callback(object sender, DevExpress.Web.CallbackEventArgs e)
    {
        //Deleta primeiro os items da lista e em seguida deleta a lista
        List<object> keyValues = gridTipoCampo.GetSelectedFieldValues(CadastroComplementarTipoListaMetadata.ColumnNames.IdTipoLista);
        for (int i = 0; i < keyValues.Count; i++)
        {
            cadastroComplementarTipoLista = new CadastroComplementarTipoLista();
            cadastroComplementarItemLista = new CadastroComplementarItemLista();
            cadastroComplementarItemListaCollection = new CadastroComplementarItemListaCollection();

            int IdTipoLista = Convert.ToInt32(keyValues[i]);

            //Deleta os items
            cadastroComplementarItemListaCollection.Query.Select();
            cadastroComplementarItemListaCollection.Query.Where(cadastroComplementarItemListaCollection.Query.IdTipoLista == IdTipoLista);
            cadastroComplementarItemListaCollection.LoadAll();
            CadastroComplementarItemListaCollection cadastroComplementarItemListaCollectionClone = (CadastroComplementarItemListaCollection)Utilitario.Clone(cadastroComplementarItemListaCollection);
            cadastroComplementarItemListaCollection.DeleteAll();
            cadastroComplementarItemListaCollection.Save();

            #region Log do Processo
            HistoricoLog historicoLog = new HistoricoLog();
            historicoLog.InsereHistoricoLog(DateTime.Now,
                                            DateTime.Now,
                                            "Cadastro de Item Lista - Operacao: Delete Item Lista: " + IdTipoLista + UtilitarioWeb.ToString(cadastroComplementarItemLista),
                                            HttpContext.Current.User.Identity.Name,
                                            UtilitarioWeb.GetIP(Request),
                                            "",
                                            HistoricoLogOrigem.Outros);
            #endregion

            //Deleta os tipos de lista
            if (cadastroComplementarTipoLista.LoadByPrimaryKey(IdTipoLista))
            {
                CadastroComplementarTipoLista cadastroComplementarTipoListaClone = (CadastroComplementarTipoLista)Utilitario.Clone(cadastroComplementarTipoLista);
                cadastroComplementarTipoLista.MarkAsDeleted();
                cadastroComplementarTipoLista.Save();

                #region Log do Processo
                HistoricoLog historicoLogTipoLista = new HistoricoLog();
                historicoLogTipoLista.InsereHistoricoLog(DateTime.Now,
                                                        DateTime.Now,
                                                        "Cadastro de Tipo Lista - Operacao: Delete Tipo Lista: " + IdTipoLista + UtilitarioWeb.ToString(cadastroComplementarTipoListaClone),
                                                        HttpContext.Current.User.Identity.Name,
                                                        UtilitarioWeb.GetIP(Request),
                                                        "",
                                                        HistoricoLogOrigem.Outros);
                #endregion
            }
        }

        gridTipoCampo.DataBind();
        gridTipoCampo.Selection.UnselectAll();
        gridTipoCampo.CancelEdit();

    }

    /// <summary>
    /// Atribui mascara dinamicamente para os campos dependendo do tipo de campo
    /// OBS: O método é um AspxCallBackPanel, ele funciona como o UpdatePanel.
    /// Sua utilização é necessária para que não ocorra o postback em toda a página
    /// </summary>
    protected void cbPanel_Callback(object source, CallbackEventArgsBase e)
    {
        ASPxCallbackPanel cbPanel = gridCadastro.FindEditFormTemplateControl("cbPanel") as ASPxCallbackPanel;
        ASPxComboBox dropIdTipoLista = cbPanel.FindControl("dropIdTipoLista") as ASPxComboBox;

        dropIdTipoLista_OnLoad(dropIdTipoLista, e);
    }

    /// <summary>
    /// Atribui mascara para o tipo de lista selecionada
    /// </summary>
    protected void cbItemLista_Callback(object source, CallbackEventArgsBase e)
    {
        ASPxCallbackPanel cbItemLista = gridCadastro.FindEditFormTemplateControl("cbItemLista") as ASPxCallbackPanel;
        ASPxCallbackPanel cbPanel = gridCadastro.FindEditFormTemplateControl("cbPanel") as ASPxCallbackPanel;
        ASPxTextBox textItemLista = cbItemLista.FindControl("textItemLista") as ASPxTextBox;
        ASPxComboBox dropIdTipoLista = cbPanel.FindControl("dropIdTipoLista") as ASPxComboBox;
        textItemLista.Text = string.Empty;

        cadastroComplementarTipoListaCollection = new CadastroComplementarTipoListaCollection();
        cadastroComplementarTipoListaCollection.Query.Select(cadastroComplementarTipoListaCollection.Query.TipoLista);
        cadastroComplementarTipoListaCollection.Query.Where(cadastroComplementarTipoListaCollection.Query.NomeLista == dropIdTipoLista.SelectedItem.Text);
        cadastroComplementarTipoListaCollection.Query.Load();

        foreach (CadastroComplementarTipoLista item in cadastroComplementarTipoListaCollection)
        {
            //Numerico
            if (item.TipoLista.Equals(-1))
                textItemLista.MaskSettings.Mask = "<0.." + textItemLista.Text.PadRight(50, '9') + ">";
            //Data
            if (item.TipoLista.Equals(-3))
            {
                textItemLista.MaskSettings.Mask = "dd/MM/yyyy";
                textItemLista.Text = DateTime.Now.ToString();
            }
        }
    }
    #endregion

    #region GridCadastro
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_RowUpdating(object sender, DevExpress.Web.Data.ASPxDataUpdatingEventArgs e)
    {
        ASPxCallbackPanel cbPanel = gridCadastro.FindEditFormTemplateControl("cbPanel") as ASPxCallbackPanel;
        ASPxCallbackPanel cbItemLista = gridCadastro.FindEditFormTemplateControl("cbItemLista") as ASPxCallbackPanel;

        ASPxComboBox dropIdTipoLista = cbPanel.FindControl("dropIdTipoLista") as ASPxComboBox;
        ASPxTextBox textItemLista = cbItemLista.FindControl("textItemLista") as ASPxTextBox;

        int idLista = (int)e.Keys[0];
        if (cadastroComplementarItemLista.LoadByPrimaryKey(idLista))
        {
            //Preenche a entidade dinamicamente
            //cadastroComplementarItemLista = PreencheEntidade<CadastroComplementarItemLista>(gridCadastro.FindEditFormTemplateControl("panelEdicao").Controls, cadastroComplementarItemLista);
            cadastroComplementarItemLista.IdLista = idLista;
            cadastroComplementarItemLista.IdTipoLista = Convert.ToInt32(dropIdTipoLista.SelectedItem.Value);
            cadastroComplementarItemLista.ItemLista = textItemLista.Text;
            cadastroComplementarItemLista.Save();

            #region Log do Processo
            HistoricoLog historicoLog = new HistoricoLog();
            historicoLog.InsereHistoricoLog(DateTime.Now,
                                            DateTime.Now,
                                            "Cadastro de CadastroComplementarListas - Operacao: Update CadastroComplementarLista: " + idLista + UtilitarioWeb.ToString(cadastroComplementarItemLista),
                                            HttpContext.Current.User.Identity.Name,
                                            UtilitarioWeb.GetIP(Request),
                                            "",
                                            HistoricoLogOrigem.Outros);
            #endregion
        }

        e.Cancel = true;
        gridCadastro.CancelEdit();
        gridCadastro.DataBind();
    }

    /// <summary>
    /// Efetua o cadastro
    /// </summary>
    protected void gridCadastro_RowInserting(object sender, DevExpress.Web.Data.ASPxDataInsertingEventArgs e)
    {
        this.Cadastra();

        e.Cancel = true;
        gridCadastro.CancelEdit();
    }

    ///// <summary>
    ///// 
    ///// </summary>
    ///// <param name="sender"></param>
    ///// <param name="e"></param>
    //protected void gridCadastro_RowInserting(object sender, DevExpress.Web.Data.ASPxDataInsertingEventArgs e)
    //{

    //    this.Cadastra();

    //    e.Cancel = true;
    //    gridCadastro.CancelEdit();
    //}

    /// <summary>
    /// Exibe a Sequência dos registros na Grid
    /// </summary>
    protected void gridCadastro_CustomColumnDisplayText(object sender, ASPxGridViewColumnDisplayTextEventArgs e)
    {
        if (e.Column.Caption == "Sequência")
        {
            e.DisplayText = (e.VisibleRowIndex + 1).ToString();
        }
        if (e.Column.FieldName == "TipoLista")
        {
            e.DisplayText = ListaCadastroComplementarDescricao.RetornaStringValue(Convert.ToInt32(e.Value));
        }
    }

    /// <summary>
    /// Fecha o EditForm e atualiza a grid
    /// Evento é chamado quando o usuário clica no botão para fechar a grid
    /// </summary>
    protected void gridCadastro_CancelRowEditing(object sender, ASPxStartRowEditingEventArgs e)
    {
        e.Cancel = false;
        gridCadastro.DataBind();
    }

    /// <summary>
    /// Preenche o campo de filtro 
    /// </summary>
    protected void gridCadastro_PreRender(object sender, EventArgs e)
    {
        //Busca o controle de filtro da coluna CADASTRO da grid
        GridViewDataComboBoxColumn tipoLista = gridCadastro.Columns["TipoLista"] as GridViewDataComboBoxColumn;
        //Limpa a lista de items para remover possiveis items duplicados
        tipoLista.PropertiesComboBox.Items.Clear();
        //Adiciona registro vazio para limpar o filtro
        tipoLista.PropertiesComboBox.Items.Add(string.Empty);
        //Percorre o ENUM para preencher o controle do combobox
        foreach (int r in Enum.GetValues(typeof(ListaCadastroComplementar)))
        {
            if (r.Equals(-1) || r.Equals(-2) || r.Equals(-3))
                tipoLista.PropertiesComboBox.Items.Add(ListaCadastroComplementarDescricao.RetornaStringValue(r), r.ToString());
        }
    }
    #endregion

    #region GridTipoCampo
    /// <summary>
    /// Realiza a formatação das colunas da grid
    /// </summary>
    protected void gridTipoCampo_CustomColumnDisplayText(object sender, ASPxGridViewColumnDisplayTextEventArgs e)
    {
        if (e.Column.FieldName == "TipoLista")
        {
            e.DisplayText = ListaCadastroComplementarDescricao.RetornaStringValue(Convert.ToInt32(e.Value));
        }
    }
    #endregion

    #region Suporte
    /// <summary>
    /// Configura Visibilidade do botão Ok++
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnOKAdd_Init(object sender, EventArgs e)
    {
        // Se for Update Não Aparece botão de Ok++
        if (!gridCadastro.IsNewRowEditing)
            (sender as LinkButton).Visible = false;
    }

    /// <summary>
    /// Converte o valor para o tipo especificado
    /// </summary>
    /// <param name="value">O valor a ser convertido</param>
    /// <param name="conversionType">O tipo a ser convertido</param>
    /// <returns>Retorna o valor convertido</returns>
    public static object ChangeType(object value, Type conversionType)
    {
        try
        {
            if (conversionType == null)
            {
                throw new ArgumentNullException("conversionType");
            }
            if (conversionType.IsGenericType && conversionType.GetGenericTypeDefinition().Equals(typeof(Nullable<>)))
            {
                if (value == null)
                {
                    return null;
                }
                NullableConverter nullableConverter = new NullableConverter(conversionType);
                conversionType = nullableConverter.UnderlyingType;
            }
            return Convert.ChangeType(value, conversionType);
        }
        catch
        {
            //Caso não consiga converter o valor, uma nova instância é gerada para setar um valor default
            return value = Activator.CreateInstance(conversionType);
        }
    }

    /// <summary>
    /// Preenche a entidade com os valores dos controles 
    /// OBS: É necessário que os controles da tela possuam seu ID correspondente as colunas do Banco de Dados
    /// </summary>
    /// <typeparam name="T">Tipo de classe a ser preenchida</typeparam>
    /// <param name="controlCollection">Lista de controles a ser percorrida</param>
    /// <param name="Entidade">Entidade do update</param>
    /// <returns>Entidade preenchida</returns>
    public T PreencheEntidade<T>(ControlCollection controlCollection, T Entidade) where T : new()
    {
        if (Entidade == null)
            Entidade = new T();

        //Instancia lista de controles obrigatórios
        List<string> listaCamposObrigatorios = new List<string>();

        #region Campos Obrigatórios
        foreach (Control control in controlCollection)
        {
            if (control is Label)
            {
                //Verifica se o campo tem preenchimento obrigatório
                if (control.GetType().GetProperty("CssClass").GetValue(control, null).Equals("labelRequired"))
                {
                    //Devolve o nome da propriedade
                    string controle = control.ClientID.Substring(control.ClientID.IndexOf("label")).Replace("label", "");
                    //Adiciona os campos obrigatórios
                    listaCamposObrigatorios.Add(controle);
                }
            }
        }
        #endregion

        #region Preenche Entidade
        //Percorre a lista de controles
        foreach (Control control in controlCollection)
        {
            //Verifica somente os controles que possuem valor para preencher a entidade
            if (!(control is Label))
            {
                //Instância valor que irá receber o valor dos controles
                object valor = new object();

                //Retira os prefixos dos controles para encontrar as propriedades da entidade(WebControls e Controles do DevExpress)
                string propriedade = control.ClientID.Substring(control.ClientID.LastIndexOf("_")).Replace("text", "").Replace("drop", "").Replace("_", "");

                //Verifica se o controle é um dropdown
                if (control.GetType().GetProperty("Value") != null && control.GetType().GetProperty("Value").GetValue(control, null) != null)
                    //DropDown
                    valor = control.GetType().GetProperty("Value").GetValue(control, null);
                else if (control.GetType().GetProperty("Text") != null && control.GetType().GetProperty("Text").GetValue(control, null) != null)
                    //TextBox
                    valor = control.GetType().GetProperty("Text").GetValue(control, null);

                //Verifica se há campos obrigatórios
                if (listaCamposObrigatorios.Contains(propriedade) && valor.Equals(string.Empty))
                    throw new Exception("Campos com * são obrigatórios!");

                //Verifica se foi encontrado essa propriedade na entidade
                if (Entidade.GetType().GetProperty(propriedade) != null)
                    //Preenche a entidade com o valor encontrado
                    Entidade.GetType().GetProperty(propriedade).SetValue(Entidade,
                                                                         ChangeType(valor, Entidade.GetType().GetProperty(propriedade).PropertyType),
                                                                         null);
            }
        }
        #endregion

        //Retorna a entidade preenchida
        return Entidade;
    }

    /// <summary>
    /// Inserção de um novo registro "ItemLista"
    /// </summary>
    private void Cadastra()
    {
        ASPxCallbackPanel cbPanel = gridCadastro.FindEditFormTemplateControl("cbPanel") as ASPxCallbackPanel;
        ASPxCallbackPanel cbItemLista = gridCadastro.FindEditFormTemplateControl("cbItemLista") as ASPxCallbackPanel;

        ASPxComboBox dropIdTipoLista = cbPanel.FindControl("dropIdTipoLista") as ASPxComboBox;
        ASPxTextBox textItemLista = cbItemLista.FindControl("textItemLista") as ASPxTextBox;

        cadastroComplementarItemLista = new CadastroComplementarItemLista();
        cadastroComplementarItemLista.IdTipoLista = Convert.ToInt32(dropIdTipoLista.SelectedItem.Value);
        cadastroComplementarItemLista.ItemLista = textItemLista.Text;
        cadastroComplementarItemLista.Save();

        #region Log do Processo
        HistoricoLog historicoLog = new HistoricoLog();
        historicoLog.InsereHistoricoLog(DateTime.Now,
                                        DateTime.Now,
                                        "Cadastro Complementar Listas - Operacao: Insert CadastroComplementarListas: " + cadastroComplementarItemLista.IdLista + UtilitarioWeb.ToString(cadastroComplementarItemLista),
                                        HttpContext.Current.User.Identity.Name,
                                        UtilitarioWeb.GetIP(Request),
                                        "",
                                        HistoricoLogOrigem.Outros);
        #endregion
    }

    /// <summary>
    /// Inserção de um novo registro "TipoLista"
    /// </summary>
    private void CadastraTipoLista()
    {
        //Instancia
        cadastroComplementarTipoLista = new CadastroComplementarTipoLista();
        cadastroComplementarTipoListaCollection = new CadastroComplementarTipoListaCollection();

        //Procura os controles no popup de tipo de lista
        ASPxComboBox dropTipoLista = popupTipoLista.FindControl("dropTipoLista") as ASPxComboBox;
        ASPxTextBox textNomeLista = popupTipoLista.FindControl("textNomeLista") as ASPxTextBox;

        #region Query
        //Retorna lista para verificar se a lista que está sendo cadastrado já existe
        cadastroComplementarTipoListaCollection = RetornaTipoLista(textNomeLista.Text);
        #endregion

        #region Tipo Lista
        //Verifica se o tipo de lista existe na base de dados
        if (cadastroComplementarTipoListaCollection.Count == 0)
        {
            //Preenche a entidade com os campos da tela
            cadastroComplementarTipoLista = PreencheEntidade<CadastroComplementarTipoLista>(popupTipoLista.Controls, null);
            //Cadastra o tipo de lista na base de dados
            cadastroComplementarTipoLista.Save();

            //Verifica se o tipo de lista foi selecionado
            if (dropTipoLista.SelectedItem != null)
            {
                //Verifica se é uma lista prédefinida(EX: Custodiante, Administradores, Emissores)
                if (Convert.ToInt32(dropTipoLista.SelectedItem.Value) >= -6 && Convert.ToInt32(dropTipoLista.SelectedItem.Value) <= -4)
                {
                    //Efetua o cadastro dos items das listas predefinidas 
                    CadastraListaBase();
                }
            }
        }
        else
        {
            //Verifica na lista retornada qual o nome da lista que já está cadastrada
            foreach (CadastroComplementarTipoLista item in cadastroComplementarTipoListaCollection)
            {
                //Verifica se o tipo da lista é diferente
                if (item.TipoLista != Convert.ToInt32(dropTipoLista.SelectedItem.Value))
                {
                    throw new Exception("Lista: " + "'" + textNomeLista.Text + "'" + " já cadastrada para o tipo " + "'" + ListaCadastroComplementarDescricao.RetornaDescricao(Convert.ToInt32(item.TipoLista)) + "'");
                }
                else
                {
                    throw new Exception("Lista já existente!");
                }
            }
        }
        #endregion

        #region Log do Processo
        HistoricoLog historicoLog = new HistoricoLog();
        historicoLog.InsereHistoricoLog(DateTime.Now,
                                        DateTime.Now,
                                        "Cadastro Complementar Listas - Operacao: Insert CadastroComplementarListas: " + cadastroComplementarItemLista.IdLista + UtilitarioWeb.ToString(cadastroComplementarItemLista),
                                        HttpContext.Current.User.Identity.Name,
                                        UtilitarioWeb.GetIP(Request),
                                        "",
                                        HistoricoLogOrigem.Outros);
        #endregion

        gridTipoCampo.DataBind();
    }

    /// <summary>
    /// Carrega os dados da base referente ao Tipo de Lista
    /// </summary>
    /// <param name="nome">Nome da lista</param>
    /// <returns>Query com os valores</returns>
    private CadastroComplementarTipoListaCollection RetornaTipoLista(string nome)
    {
        cadastroComplementarTipoListaCollection = new CadastroComplementarTipoListaCollection();
        cadastroComplementarTipoListaCollection.Query.Select();
        cadastroComplementarTipoListaCollection.Query.Where(cadastroComplementarTipoListaCollection.Query.NomeLista == nome);
        cadastroComplementarTipoListaCollection.Query.Load();

        return cadastroComplementarTipoListaCollection;
    }

    /// <summary>
    /// Retorna lista contendo todos os tipos de listas cadastrados na base
    /// </summary>
    /// <returns>Lista com os registros carregados</returns>
    private CadastroComplementarTipoListaCollection BuscaTipoLista()
    {
        //Carrega Lista de campos cadastrados
        cadastroComplementarTipoListaCollection = new CadastroComplementarTipoListaCollection();
        cadastroComplementarTipoListaCollection.Query.Select();
        cadastroComplementarTipoListaCollection.Query.Load();

        return cadastroComplementarTipoListaCollection;
    }

    /// <summary>
    /// Lista os emissores disponíveis
    /// </summary>
    /// <returns>Lista contendo os valores do Emissor</returns>
    private AgenteMercadoCollection BuscaListaEmissor()
    {
        agenteMercadoCollection = new AgenteMercadoCollection();
        agenteMercadoCollection.Query.Select(agenteMercadoCollection.Query.Nome);
        agenteMercadoCollection.Query.Load();

        return agenteMercadoCollection;
    }

    /// <summary>
    /// Lista os agentes de mercado disponíveis
    /// </summary>
    /// <returns>Lista de acordo com o agente selecionado</returns>
    private AgenteMercadoCollection BuscaListaAgenteMercado(int agente)
    {
        agenteMercadoCollection = new AgenteMercadoCollection();

        //Custodiante
        if (agente.Equals(-6))
            agenteMercadoCollection.Query.Where(agenteMercadoCollection.Query.FuncaoCustodiante == "S");

        //Administradores
        if (agente.Equals(-5))
            agenteMercadoCollection.Query.Where(agenteMercadoCollection.Query.FuncaoAdministrador == "S");

        agenteMercadoCollection.Query.Select(agenteMercadoCollection.Query.Nome);
        agenteMercadoCollection.Query.Load();

        return agenteMercadoCollection;
    }

    /// <summary>
    /// Cadastra Lista já preexistentes na base de dados
    /// EX: Custodiante, Administradores, Emissores...
    /// </summary>
    private void CadastraListaBase()
    {
        //Busca o controle no EDITFORM da grid
        ASPxComboBox dropTipoLista = popupTipoLista.FindControl("dropTipoLista") as ASPxComboBox;
        ASPxTextBox textNomeLista = popupTipoLista.FindControl("textNomeLista") as ASPxTextBox;

        if (dropTipoLista.SelectedItem != null)
        {
            //Preenche o tipo de lista
            int idTipoLista = BuscaIdTipoLista(textNomeLista.Text, Convert.ToInt32(dropTipoLista.SelectedItem.Value));
            if (idTipoLista == 0)
                throw new Exception("Lista não encontrada");

            //Emissores
            if (Convert.ToInt32(dropTipoLista.SelectedItem.Value).Equals(-4))
            {
                foreach (AgenteMercado item in BuscaListaEmissor())
                {
                    cadastroComplementarItemLista = new CadastroComplementarItemLista();
                    cadastroComplementarItemLista.IdTipoLista = idTipoLista;
                    cadastroComplementarItemLista.ItemLista = item.Nome;
                    cadastroComplementarItemLista.Save();
                }
            }

            //Administradores, Custodiantes
            if (Convert.ToInt32(dropTipoLista.SelectedItem.Value).Equals(-5) || Convert.ToInt32(dropTipoLista.SelectedItem.Value).Equals(-6))
            {
                foreach (AgenteMercado item in BuscaListaAgenteMercado(Convert.ToInt32(dropTipoLista.SelectedItem.Value)))
                {
                    cadastroComplementarItemLista = new CadastroComplementarItemLista();
                    cadastroComplementarItemLista.IdTipoLista = idTipoLista;
                    cadastroComplementarItemLista.ItemLista = item.Nome;
                    cadastroComplementarItemLista.Save();
                }
            }
        }
    }

    /// <summary>
    /// Retorna o ID da tabela tipo de lista 
    /// </summary>
    /// <returns>Id referente a tabela de Tipo Lista</returns>
    private int BuscaIdTipoLista(string nomeLista, int tipoLista)
    {
        cadastroComplementarTipoListaCollection = new CadastroComplementarTipoListaCollection();
        cadastroComplementarTipoListaCollection.Query.Select(cadastroComplementarTipoListaCollection.Query.IdTipoLista);
        cadastroComplementarTipoListaCollection.Query.Where(cadastroComplementarTipoListaCollection.Query.NomeLista == nomeLista,
                                                            cadastroComplementarTipoListaCollection.Query.TipoLista == tipoLista);
        cadastroComplementarTipoListaCollection.Query.Load();

        if (cadastroComplementarTipoListaCollection.Count > 0)
            return Convert.ToInt32(cadastroComplementarTipoListaCollection[0].IdTipoLista);

        return 0;
    }
    #endregion

    #region OnLoad
    /// <summary>
    /// Carrega o dropdown com o valores do Enum
    /// </summary>
    protected void dropTipoLista_OnLoad(object sender, EventArgs e)
    {
        ASPxComboBox dropTipoLista = (ASPxComboBox)sender;

        //Limpa os items da lista
        dropTipoLista.Items.Clear();
        foreach (int r in Enum.GetValues(typeof(ListaCadastroComplementar)))
        {
            if (r > -7)
                dropTipoLista.Items.Add(ListaCadastroComplementarDescricao.RetornaStringValue(r), r.ToString());
        }    

    }

    /// <summary>
    /// Carrega os nomes das listas cadastradas
    /// </summary>
    protected void dropIdTipoLista_OnLoad(object sender, EventArgs e)
    {
        //Instancia o controle
        ASPxComboBox dropIdTipoLista = (ASPxComboBox)sender;

        //Carrega Lista de campos cadastrados
        cadastroComplementarTipoListaCollection = BuscaTipoLista();

        //Limpa a lista para evitar registros duplicados
        dropIdTipoLista.Items.Clear();

        //Insere o item que serve para cadastrar o tipo de lista
        dropIdTipoLista.Items.Insert(0, new ListEditItem("...", 0));

        //Percorre a lista e insere os items que retornaram na busca do banco de dados
        foreach (CadastroComplementarTipoLista item in cadastroComplementarTipoListaCollection)
        {
            //Não inclui lista do tipo: Emissores, Administradores e Custodiantes
            if (item.TipoLista != -4
             && item.TipoLista != -5
             && item.TipoLista != -6)
                dropIdTipoLista.Items.Add(new ListEditItem(item.NomeLista, item.IdTipoLista));
        }
    }
    #endregion
}
