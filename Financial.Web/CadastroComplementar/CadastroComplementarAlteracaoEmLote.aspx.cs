﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Collections.Generic;
using System.Text;
using System.Drawing;

using Financial.Common;
using Financial.Util;
using Financial.Investidor;
using Financial.ContaCorrente;
using Financial.Security;
using Financial.Fundo;
using Financial.InvestidorCotista;
using Financial.Bolsa;
using Financial.RendaFixa;
using Financial.Web.Common;

using DevExpress.Web;
using DevExpress.Web.Data;

using Financial.ContaCorrente.Enums;
using Financial.InvestidorCotista.Enums;
using Financial.Investidor.Enums;
using Financial.Common.Enums;
using Financial.Bolsa.Enums;
using Financial.RendaFixa.Enums;
using System.Threading;
using EntitySpaces.Interfaces;
using Financial.Security.Enums;
using Financial.Web.Util;
using System.ComponentModel;
using System.Reflection;

public partial class CadastroComplementar_CadastroComplementarAlteracaoEmLote : CadastroBasePage
{
    #region InstÃ¢ncia de Classes
    //Collection
    CadastroComplementarItemListaCollection cadastroComplementarItemListaCollection = new CadastroComplementarItemListaCollection();
    CadastroComplementarCamposCollection cadastroComplementarCamposCollection = new CadastroComplementarCamposCollection();
    CadastroComplementarTipoListaCollection cadastroComplementarTipoListaCollection = new CadastroComplementarTipoListaCollection();
    CadastroComplementarCollection cadastroComplementarCollection = new CadastroComplementarCollection();
    AgenteMercadoCollection agenteMercadoCollection = new AgenteMercadoCollection();
    CotistaCollection cotistaCollection = new CotistaCollection();
    EmissorCollection emissorCollection = new EmissorCollection();
    CarteiraCollection carteiraCollection = new CarteiraCollection();
    //Entidade
    CadastroComplementar cadastroComplementar = new CadastroComplementar();
    CadastroComplementarCampos camposComplementares = new CadastroComplementarCampos();
    CadastroComplementarTipoLista cadastroComplementarTipoLista = new CadastroComplementarTipoLista();
    //Query
    CadastroComplementarCamposQuery cadastroComplementarCamposQuery = new CadastroComplementarCamposQuery();
    #endregion

    #region EntitySpace - Base de Dados
    /// <summary>
    /// Acesso a base de dados
    /// </summary>
    protected void EsDSCadastroComplementar_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        CadastroComplementarQuery cadastroComplementarQuery = new CadastroComplementarQuery("a");
        CadastroComplementarCamposQuery cadastroComplementarCamposQuery = new CadastroComplementarCamposQuery("b");

        cadastroComplementarQuery.InnerJoin(cadastroComplementarCamposQuery).On(cadastroComplementarQuery.IdCamposComplementares == cadastroComplementarCamposQuery.IdCamposComplementares);
        cadastroComplementarQuery.OrderBy(cadastroComplementarQuery.IdCadastroComplementares.Ascending);
        cadastroComplementarCollection.Load(cadastroComplementarQuery);
        e.Collection = cadastroComplementarCollection;
    }
    #endregion

    #region CallBack
    /// <summary>
    /// 
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void callbackErro_Callback(object source, DevExpress.Web.CallbackEventArgs e)
    {
        e.Result = "";
    }
    #endregion

    #region GridCadastro
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_RowUpdating(object sender, DevExpress.Web.Data.ASPxDataUpdatingEventArgs e)
    {
        ASPxTextBox textNovoValor = gridCadastro.FindEditFormTemplateControl("textNovoValor") as ASPxTextBox;
        int idCadastroComplementares = (int)e.Keys[0];
        if (cadastroComplementar.LoadByPrimaryKey(idCadastroComplementares))
        {
            //Preenche a entidade dinamicamente
            cadastroComplementar.ValorCampo = textNovoValor.Text;
            cadastroComplementar.Save();

            #region Log do Processo
            HistoricoLog historicoLog = new HistoricoLog();
            historicoLog.InsereHistoricoLog(DateTime.Now,
                                            DateTime.Now,
                                            "Cadastro de CadastroComplementar - Operacao: Update CadastroComplementar: " + idCadastroComplementares + UtilitarioWeb.ToString(cadastroComplementar),
                                            HttpContext.Current.User.Identity.Name,
                                            UtilitarioWeb.GetIP(Request),
                                            "",
                                            HistoricoLogOrigem.Outros);
            #endregion
        }

        e.Cancel = true;
        gridCadastro.CancelEdit();
        gridCadastro.DataBind();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_RowInserting(object sender, DevExpress.Web.Data.ASPxDataInsertingEventArgs e)
    {
        ASPxTextBox textNovoValor = gridCadastro.FindEditFormTemplateControl("textNovoValor") as ASPxTextBox;
        
        if (!string.IsNullOrEmpty(textNovoValor.Text))
        {
            #region Verifica tipo campo
            List<object> listaTipoCampo = gridCadastro.GetSelectedFieldValues("TipoCampo");
            List<object> listaVerificaTipoCampo = new List<object>();

            //Verifica se nos registros selecionados pelo usuário há tipos diferentes de dados
            foreach (object item in listaTipoCampo)
            {
                if (!listaVerificaTipoCampo.Contains(item))
                    listaVerificaTipoCampo.Add(item);
            }
            #endregion

            if (listaVerificaTipoCampo.Count <= 1)
            {
                List<object> keyValuesId = gridCadastro.GetSelectedFieldValues("IdCadastroComplementares");
                for (int i = 0; i < keyValuesId.Count; i++)
                {
                    int idCadastroComplementares = Convert.ToInt32(keyValuesId[i]);
                    cadastroComplementar = new CadastroComplementar();

                    if (cadastroComplementar.LoadByPrimaryKey(idCadastroComplementares))
                    {
                        cadastroComplementar.ValorCampo = textNovoValor.Text;
                        cadastroComplementar.Save();

                        #region Log do Processo
                        HistoricoLog historicoLog = new HistoricoLog();
                        historicoLog.InsereHistoricoLog(DateTime.Now,
                                                        DateTime.Now,
                                                        "Cadastro de CadastroComplementar - Operacao: Alterarar CadastroComplementar: " + idCadastroComplementares + "; " + cadastroComplementar,
                                                        HttpContext.Current.User.Identity.Name,
                                                        UtilitarioWeb.GetIP(Request),
                                                        "",
                                                        HistoricoLogOrigem.Outros);
                        #endregion
                    }
                }
            }
            else
                throw new Exception("Os dados selecionados devem ser do mesmo tipo");
        }
        
        e.Cancel = true;
        gridCadastro.Selection.UnselectAll();
        gridCadastro.CancelEdit();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_CustomCallback(object sender, ASPxGridViewCustomCallbackEventArgs e)
    {
        if (e.Parameters == "btnDelete")
        {
            List<object> keyValuesId = gridCadastro.GetSelectedFieldValues("IdCadastroComplementares");
            for (int i = 0; i < keyValuesId.Count; i++)
            {
                int idCadastroComplementares = Convert.ToInt32(keyValuesId[i]);
                cadastroComplementar = new CadastroComplementar();

                if (cadastroComplementar.LoadByPrimaryKey(idCadastroComplementares))
                {
                    CadastroComplementar cadastroComplementarClone = (CadastroComplementar)Utilitario.Clone(cadastroComplementar);

                    cadastroComplementar.MarkAsDeleted();
                    cadastroComplementar.Save();

                    #region Log do Processo
                    HistoricoLog historicoLog = new HistoricoLog();
                    historicoLog.InsereHistoricoLog(DateTime.Now,
                                                    DateTime.Now,
                                                    "Cadastro de CadastroComplementar - Operacao: Delete CadastroComplementar: " + idCadastroComplementares + "; " + UtilitarioWeb.ToString(cadastroComplementarClone),
                                                    HttpContext.Current.User.Identity.Name,
                                                    UtilitarioWeb.GetIP(Request),
                                                    "",
                                                    HistoricoLogOrigem.Outros);
                    #endregion
                }
            }
        }

        gridCadastro.DataBind();
        gridCadastro.Selection.UnselectAll();
        gridCadastro.CancelEdit();
    }

    /// <summary>
    /// Exibe a SequÃƒÂªncia dos registros na Grid
    /// </summary>
    protected void gridCadastro_CustomColumnDisplayText(object sender, ASPxGridViewColumnDisplayTextEventArgs e)
    {
        if (e.Column.FieldName == "TipoCadastro")
        {
            e.DisplayText = ListaCadastroDescricao.RetornaStringValue(Convert.ToInt32(e.Value));
        }
        if (e.Column.FieldName == "TipoCampo")
        {
            //Verifica se o valor Ã© negativo, referenciando a lista de ENUM
            if (Convert.ToInt32(e.Value) < 0)
                e.DisplayText = ListaCadastroComplementarDescricao.RetornaStringValue(Convert.ToInt32(e.Value));
            else
                e.DisplayText = ListaTipoCampoDescricao(Convert.ToInt32(e.Value));
        }
        //if (e.Column.FieldName == "MercadoTipoPessoa")
        //{
        //    object val = ((ASPxGridView)sender).GetRowValues(e.VisibleRowIndex, "TipoCadastro");
        //    string mercado = ListaCadastroDescricao.RetornaStringValue(Convert.ToInt32(val));

        //    if (mercado.Equals("Cotista"))
        //    {
        //        cotistaCollection = new CotistaCollection();
        //        cotistaCollection.Query.Select(cotistaCollection.Query.IdCotista, cotistaCollection.Query.Nome);
        //        cotistaCollection.Query.Load();

        //        e.DisplayText = cotistaCollection.FindByPrimaryKey(Convert.ToInt32(e.Value)).Nome;
        //    }
        //    else if (mercado.Equals("Carteira/Fundo"))
        //    {
        //        carteiraCollection = new CarteiraCollection();
        //        carteiraCollection.Query.Select(carteiraCollection.Query.IdCarteira, carteiraCollection.Query.Nome);
        //        carteiraCollection.Query.Load();

        //        e.DisplayText = carteiraCollection.FindByPrimaryKey(Convert.ToInt32(e.Value)).Nome;
        //    }
        //    else if (mercado.Equals("Emissores"))
        //    {
        //        emissorCollection = new EmissorCollection();
        //        emissorCollection.Query.Select(emissorCollection.Query.IdEmissor, emissorCollection.Query.Nome);
        //        emissorCollection.Query.Load();

        //        e.DisplayText = emissorCollection.FindByPrimaryKey(Convert.ToInt32(e.Value)).Nome;
        //    }
        //    else
        //    {
        //        e.DisplayText = ListaMercadoTipoPessoa(mercado).FindByPrimaryKey(Convert.ToInt32(e.Value)).Nome;
        //    }
        //}
    }

    /// <summary>
    /// Fecha o EditForm e atualiza a grid
    /// Evento ÃƒÂ© chamado quando o usuÃƒÂ¡rio clica no botÃƒÂ£o para fechar a grid
    /// </summary>
    protected void gridCadastro_CancelRowEditing(object sender, ASPxStartRowEditingEventArgs e)
    {
        e.Cancel = false;
        gridCadastro.DataBind();
    }

    /// <summary>
    /// Carrega os combobox dos filtros de pesquisa da grid
    /// </summary>
    protected void gridCadastro_PreRender(object sender, EventArgs e)
    {
        #region Cadastro
        //Busca o controle de filtro da coluna CADASTRO da grid
        GridViewDataComboBoxColumn cadastro = gridCadastro.Columns["Cadastro"] as GridViewDataComboBoxColumn;
        //Adiciona registro vazio para limpar o filtro
        cadastro.PropertiesComboBox.Items.Add(string.Empty);
        //Percorre o ENUM para preencher o controle do combobox
        foreach (int r in Enum.GetValues(typeof(ListaCadastro)))
        {
            cadastro.PropertiesComboBox.Items.Add(ListaCadastroDescricao.RetornaStringValue(r), r.ToString());
        }
        #endregion

        #region Tipo Campo
        //Busca o controle de filtro da coluna CADASTRO da grid
        GridViewDataComboBoxColumn tipoCampo = gridCadastro.Columns["TipoCampo"] as GridViewDataComboBoxColumn;
        //Limpa a lista de items para remover possiveis items duplicados
        tipoCampo.PropertiesComboBox.Items.Clear();
        //Adiciona registro vazio para limpar o filtro
        tipoCampo.PropertiesComboBox.Items.Add(string.Empty);
        //Percorre o ENUM para preencher o controle do combobox
        foreach (int r in Enum.GetValues(typeof(ListaCadastroComplementar)))
        {
            if (r == -1 || r == -2 || r == -3)
                tipoCampo.PropertiesComboBox.Items.Add(ListaCadastroComplementarDescricao.RetornaStringValue(r), r.ToString());
        }

        //Carrega Lista de campos cadastrados
        cadastroComplementarTipoListaCollection = new CadastroComplementarTipoListaCollection();
        cadastroComplementarTipoListaCollection.Query.Select();
        cadastroComplementarTipoListaCollection.Query.Load();

        foreach (CadastroComplementarTipoLista item in cadastroComplementarTipoListaCollection)
        {
            tipoCampo.PropertiesComboBox.Items.Add(new ListEditItem(item.NomeLista, item.IdTipoLista));
        }
        #endregion
    }
    #endregion

    #region OnLoad
    /// <summary>
    /// Carrega o dropdown com o valores do Enum
    /// </summary>
    protected void dropCadastro_OnLoad(object sender, EventArgs e)
    {
        ASPxComboBox dropTipoCadastro = (ASPxComboBox)sender;
        dropTipoCadastro.Items.Clear();
        foreach (int r in Enum.GetValues(typeof(ListaCadastro)))
        {
            dropTipoCadastro.Items.Add(ListaCadastroDescricao.RetornaStringValue(r), r.ToString());
        }
    }
    #endregion

    #region Suporte
    /// <summary>
    /// Cria dinamicamente os controle na tela
    /// </summary>
    /// <typeparam name="T">Tipo do controle a ser criado</typeparam>
    /// <param name="idControle">ID do controle a ser criado</param>
    /// <param name="textoControle">Texto do controle a ser exibido na tela</param>
    /// <returns>Controle com as propriedades principais criadas</returns>
    private T CriaControleTela<T>(string idControle, string textoControle, string cssclass, int? tamanho, int? tipoCampo) where T : new()
    {
        //Instancia o tipo de controle a ser criado
        T controleCriado = new T();

        //Propriedade ID do controle
        PropertyInfo id = controleCriado.GetType().GetProperty("ID");
        id.SetValue(controleCriado, idControle, null);

        //Propriedade Client ID do controle
        PropertyInfo clientId = controleCriado.GetType().GetProperty("ClientInstanceName");
        clientId.SetValue(controleCriado, idControle, null);

        //Propriedade TEXT do controle
        if (!string.IsNullOrEmpty(textoControle))
        {
            PropertyInfo texto = controleCriado.GetType().GetProperty("Text");
            texto.SetValue(controleCriado, textoControle, null);
        }

        //Aplicar o valor do CSSCLASS para o controle
        if (!string.IsNullOrEmpty(cssclass))
        {
            PropertyInfo css = controleCriado.GetType().GetProperty("CssClass");
            css.SetValue(controleCriado, cssclass, null);
        }

        //Aplicar o valor do MAXLENGTH para o controle
        if (tamanho != 0)
        {
            PropertyInfo maxlength = controleCriado.GetType().GetProperty("MaxLength");
            maxlength.SetValue(controleCriado, tamanho, null);
        }

        //Retorna o controle com as propriedades preenchidas
        return controleCriado;
    }

    /// <summary>
    /// Armazena os controles da tela na lista 
    /// </summary>
    /// <typeparam name="T">Tipo do controle a ser encontrado</typeparam>
    /// <param name="controlCollection">Container onde os controles irÃ£o ser encontrados</param>
    /// <param name="resultCollection">Lista que irÃ¡ armazenar os controles do container</param>
    private void ListaControles<T>(ControlCollection controlCollection, List<T> listaPreenchida) where T : Control
    {
        foreach (Control control in controlCollection)
        {
            //Verifica se o controle encontrado Ã© do tipo especificado
            if (control is T)
                listaPreenchida.Add((T)control);

            //Verifica se hÃ¡ controles a serem percorridos
            if (control.HasControls())
                ListaControles(control.Controls, listaPreenchida);
        }
    }

    /// <summary>
    /// Retorna a descriÃ§Ã£o da lista cadastrada
    /// </summary>
    /// <param name="id">Identificador da lista</param>
    /// <returns>Nome da lista</returns>
    private string ListaTipoCampoDescricao(int id)
    {
        //Instancia entidades
        cadastroComplementarTipoListaCollection = new CadastroComplementarTipoListaCollection();
        cadastroComplementarTipoLista = new CadastroComplementarTipoLista();

        //Carrega Lista de campos cadastrados
        cadastroComplementarTipoListaCollection.Query.Select(cadastroComplementarTipoListaCollection.Query.NomeLista);
        cadastroComplementarTipoListaCollection.Query.Where(cadastroComplementarTipoListaCollection.Query.IdTipoLista == id);
        cadastroComplementarTipoLista.Load(cadastroComplementarTipoListaCollection.Query);

        return cadastroComplementarTipoLista.NomeLista;
    }

    /// <summary>
    /// Pequisa a lista de mercado a ser carregado na tela
    /// </summary>
    private AgenteMercadoCollection ListaMercadoTipoPessoa(string mercado)
    {
        //InstÃ£ncia
        agenteMercadoCollection = new AgenteMercadoCollection();

        //Seleciona o nome do Mercado/Tipo Pessoa
        agenteMercadoCollection.Query.Select(agenteMercadoCollection.Query.IdAgente, agenteMercadoCollection.Query.Nome);

        //Verifica qual o mercado solicitado
        switch (mercado)
        {
            case "Administradores":
                agenteMercadoCollection.Query.Where(agenteMercadoCollection.Query.FuncaoAdministrador == "S");
                agenteMercadoCollection.Query.Load();
                break;
            case "Custodiantes":
                agenteMercadoCollection.Query.Where(agenteMercadoCollection.Query.FuncaoCustodiante == "S");
                agenteMercadoCollection.Query.Load();
                break;
            case "Corretora":
                agenteMercadoCollection.Query.Where(agenteMercadoCollection.Query.FuncaoCorretora == "S");
                agenteMercadoCollection.Query.Load();
                break;
            case "Gestor":
                agenteMercadoCollection.Query.Where(agenteMercadoCollection.Query.FuncaoGestor == "S");
                agenteMercadoCollection.Query.Load();
                break;
            case "Liquidante":
                agenteMercadoCollection.Query.Where(agenteMercadoCollection.Query.FuncaoLiquidante == "S");
                agenteMercadoCollection.Query.Load();
                break;
            case "Distribuidor":
                agenteMercadoCollection.Query.Where(agenteMercadoCollection.Query.FuncaoDistribuidor == "S");
                agenteMercadoCollection.Query.Load();
                break;
            default:
                break;
        }

        //Retorna a lista preenchida
        return agenteMercadoCollection;
    }

    /// <summary>
    /// Pesquisa a chave primaria dos campos a serem cadastrados
    /// </summary>
    /// <param name="tipoCadastro">Verifica qual o tipo de cadastro selecionado</param>
    /// <param name="nomeCampo">campo a ser pesquisado na base</param>
    /// <returns>chave dos campos</returns>
    private int? ChaveCamposComplementares(int tipoCadastro, string nomeCampo)
    {
        //Instancia
        cadastroComplementarCamposCollection = new CadastroComplementarCamposCollection();
        cadastroComplementarCamposQuery = new CadastroComplementarCamposQuery();

        //Executa query para encontrar o ID dos campos
        cadastroComplementarCamposQuery.Select(cadastroComplementarCamposQuery.IdCamposComplementares);
        cadastroComplementarCamposQuery.Where(cadastroComplementarCamposQuery.TipoCadastro == tipoCadastro,
                                              cadastroComplementarCamposQuery.NomeCampo == nomeCampo);
        cadastroComplementarCamposCollection.Load(cadastroComplementarCamposQuery);

        //Retorna o ID
        return cadastroComplementarCamposCollection[0].IdCamposComplementares;
    }
    #endregion
}