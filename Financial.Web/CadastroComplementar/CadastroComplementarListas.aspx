﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="CadastroComplementarListas.aspx.cs"
    Inherits="CadastroComplementar_CadastroComplementarListas" %>

<%@ Register Assembly="System.Web, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"
    Namespace="System.Web.UI.HtmlControls" TagPrefix="cc2" %>
<%@ Register Assembly="EntitySpaces.Web" Namespace="EntitySpaces.Web" TagPrefix="cc1" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxe" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxwgv" %>

<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxpc" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxcb" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxcp" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <link href="~/css/forms.css" rel="stylesheet" type="text/css" />

    <script type="text/javascript" language="Javascript" src="../js/global.js"></script>

    <script type="text/javascript" language="Javascript">
    var popup = true;    
    document.onkeydown=onDocumentKeyDown;        
    var operacao = '';
    </script>

    <script type="text/javascript" language="Javascript">
        var isCustomCallback = false;   // usada para controlar o refresh após um delete feito no grid
    </script>

</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="true"
            EnableScriptLocalization="true" />
        <dxcb:ASPxCallback ID="callbackPanel" runat="server" OnCallback="callbackPanel_Callback">
            <ClientSideEvents CallbackComplete="function(s, e) {
            if(e.result != null && e.result == '0')
               popupTipoLista.ShowWindow();
            else
               cbItemLista.PerformCallback();
        }        
        " />
        </dxcb:ASPxCallback>
        <dxcb:ASPxCallback ID="callbackErro" runat="server" OnCallback="callbackErro_Callback">
            <ClientSideEvents CallbackComplete="function(s, e) { 
            if (e.result != '')
            {                   
                alert(e.result);                                              
            }
            else
            {
                if (operacao == 'deletar')
                {
                    gridCadastro.PerformCallback('btnDelete');                    
                }
                else
                {   
                    if (operacao == 'salvar' || gridCadastro.cp_EditVisibleIndex != 'new')
                    {             
                        gridCadastro.UpdateEdit();
                    }
                    else
                    {
                        callbackAdd.SendCallback();
                    }    
                }
            }
            
            operacao = '';
        }        
        " />
        </dxcb:ASPxCallback>
        <dxcb:ASPxCallback ID="callbackAdd" runat="server" OnCallback="callbackAdd_Callback">
            <ClientSideEvents CallbackComplete="function(s, e) {
            alert('Operação feita com sucesso.');
        }        
        " />
        </dxcb:ASPxCallback>
        <dxcb:ASPxCallback ID="callBackTipoLista" runat="server" OnCallback="callBackTipoLista_Callback">
            <ClientSideEvents CallbackComplete="function(s, e) {
            alert(e.result);
            cbPanel.PerformCallback();
        }        
        " />
        </dxcb:ASPxCallback>
        <dxcb:ASPxCallback ID="callBackDeleteTipoCampo" runat="server" OnCallback="callBackDeleteTipoCampo_Callback" />
        <div class="divPanel">
            <div id="container">
                <div id="header">
                    <asp:Label ID="lblHeader" runat="server" Text="Cadastro de Listas"></asp:Label>
                </div>
                <div id="mainContent">
                    <dxpc:ASPxPopupControl ID="popupTipoLista" AllowDragging="true" PopupElementID="popupTipoLista"
                        EnableClientSideAPI="True" PopupVerticalAlign="Middle" PopupHorizontalAlign="OutsideRight"
                        CloseAction="CloseButton" Width="400" Left="250" Top="70" HeaderText="Cadastro Tipo de Lista"
                        runat="server" HeaderStyle-BackColor="#EBECEE" HeaderStyle-Font-Bold="true" HeaderStyle-Font-Size="11px">
                        <ContentCollection>
                            <dxpc:PopupControlContentControl runat="server">
                                <table>
                                    <tr>
                                        <td class="td_Label">
                                            <asp:Label ID="labelNomeLista" runat="server" CssClass="labelRequired" Text="Nome:" />
                                        </td>
                                        <td>
                                            <dxe:ASPxTextBox ID="textNomeLista" runat="server" ClientInstanceName="textNomeLista"
                                                CssClass="textLongo" />
                                        </td>
                                        <td class="td_Label">
                                            <asp:Label ID="labelTipoLista" runat="server" CssClass="labelRequired" Text="Tipo:" />
                                        </td>
                                        <td>
                                            <dxe:ASPxComboBox ID="dropTipoLista" runat="server" ShowShadow="False" CssClass="dropDownListCurto_2"
                                                OnLoad="dropTipoLista_OnLoad" ValueType="System.String" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="4">
                                            <dxwgv:ASPxGridView ID="gridTipoCampo" ClientInstanceName="gridTipoCampo" runat="server"
                                                KeyFieldName="IdTipoLista" OnCustomCallback="gridCadastro_CustomCallback" OnRowInserting="gridCadastro_RowInserting"
                                                OnRowUpdating="gridCadastro_RowUpdating" DataSourceID="EsDSCadastroComplementarTipoLista"
                                                Width="100%" AutoGenerateColumns="False" OnCustomColumnDisplayText="gridTipoCampo_CustomColumnDisplayText">
                                                <Columns>
                                                    <dxwgv:GridViewCommandColumn ShowSelectCheckbox="True" VisibleIndex="0" Width="5%" ButtonType="Image" ShowClearFilterButton="True">
                                                        <HeaderTemplate>
                                                            <dxe:ASPxCheckBox ID="cbAll" ClientInstanceName="cbAll" runat="server" ToolTip="Seleciona todos os CheckBoxs de todas as páginas" BackColor="white" ClientSideEvents-CheckedChanged="function(s, e) {OnAllCheckedChanged(s, e, gridTipoCampo);}" OnInit="CheckBoxSelectAll"/>
                                                        </HeaderTemplate>
                                                        <HeaderStyle HorizontalAlign="Center"/>
                                                    </dxwgv:GridViewCommandColumn>
                                                    <dxwgv:GridViewDataTextColumn FieldName="IdTipoLista" Width="15%" VisibleIndex="1">
                                                        <PropertiesTextEdit MaxLength="10">
                                                        </PropertiesTextEdit>
                                                    </dxwgv:GridViewDataTextColumn>
                                                    <dxwgv:GridViewDataComboBoxColumn FieldName="NomeLista" Caption="Nome Lista" VisibleIndex="2"
                                                        Width="30%">
                                                        <PropertiesComboBox DataSourceID="EsDSCadastroComplementarTipoLista" TextField="NomeLista"
                                                            ValueField="IdTipoLista" ValueType="System.String">
                                                        </PropertiesComboBox>
                                                    </dxwgv:GridViewDataComboBoxColumn>
                                                    <dxwgv:GridViewDataTextColumn FieldName="TipoLista" Width="15%" VisibleIndex="3">
                                                        <PropertiesTextEdit MaxLength="10">
                                                        </PropertiesTextEdit>
                                                    </dxwgv:GridViewDataTextColumn>
                                                </Columns>
                                                <ClientSideEvents BeginCallback="function(s, e) {
		            if (e.command == 'CUSTOMCALLBACK') {
                        isCustomCallback = true;
                    }						
                }" EndCallback="function(s, e) {
			        if (isCustomCallback) {
                        isCustomCallback = false;
                        s.Refresh();
                    }
                }" />
                                                <SettingsCommandButton>
                                                    <ClearFilterButton>
                                                        <Image Url="../../imagens/funnel--minus.png">
                                                        </Image>
                                                    </ClearFilterButton>
                                                </SettingsCommandButton>
                                            </dxwgv:ASPxGridView>
                                        </td>
                                    </tr>
                                </table>
                                <div class="linhaH">
                                </div>
                                <div class="linkButton linkButtonNoBorder popupFooter">
                                    <div class="linkButton linkButtonNoBorder" style="margin-top: 20px">
                                        <asp:LinkButton ID="LinkButton2" runat="server" Font-Overline="False" CssClass="btnDelete"
                                            OnClientClick=" if (confirm('Tem certeza que quer excluir?')==true) callBackDeleteTipoCampo.PerformCallback(); gridTipoCampo.Refresh(); return false;">
                                            <asp:Literal ID="Literal9" runat="server" Text="Excluir" />
                                            <div>
                                            </div>
                                        </asp:LinkButton>
                                        <asp:LinkButton ID="btnTipoLista" runat="server" Font-Overline="False" ForeColor="Black"
                                            CssClass="btnOK" OnClientClick="callBackTipoLista.SendCallback(); gridTipoCampo.Refresh(); return false;">
                                            <asp:Literal ID="Literal11" runat="server" Text="Cadastrar" />
                                            <div>
                                            </div>
                                        </asp:LinkButton>
                                    </div>
                                </div>
                            </dxpc:PopupControlContentControl>
                        </ContentCollection>
                        <HeaderStyle BackColor="#EBECEE" Font-Bold="True" Font-Size="11px" />
                    </dxpc:ASPxPopupControl>
                    <div class="linkButton">
                        <asp:LinkButton ID="btnAdd" runat="server" Font-Overline="false" ValidationGroup="ATK"
                            CssClass="btnAdd" OnClientClick="gridCadastro.AddNewRow(); return false; ">
                            <asp:Literal ID="Literal1" runat="server" Text="Novo" /><div>
                            </div>
                        </asp:LinkButton>
                        <asp:LinkButton ID="btnDelete" runat="server" Font-Overline="false" ValidationGroup="ATK"
                            CssClass="btnDelete" OnClientClick=" if (confirm('Tem certeza que quer excluir?')==true) {operacao='deletar'; callbackErro.SendCallback('delete');} return false;">
                            <asp:Literal ID="Literal2" runat="server" Text="Excluir" /><div>
                            </div>
                        </asp:LinkButton>
                        <asp:LinkButton ID="btnPDF" runat="server" Font-Overline="false" ValidationGroup="ATK"
                            CssClass="btnPdf" OnClick="btnPDF_Click">
                            <asp:Literal ID="Literal6" runat="server" Text="Gerar PDF" /><div>
                            </div>
                        </asp:LinkButton>
                        <asp:LinkButton ID="btnExcel" runat="server" Font-Overline="false" ValidationGroup="ATK"
                            CssClass="btnExcel" OnClick="btnExcel_Click">
                            <asp:Literal ID="Literal7" runat="server" Text="Gerar Excel" /><div>
                            </div>
                        </asp:LinkButton>
                        <asp:LinkButton ID="btnRefresh" runat="server" Font-Overline="false" CssClass="btnRefresh"
                            OnClientClick=" gridCadastro.PerformCallback('btnRefresh'); return false;">
                            <asp:Literal ID="Literal8" runat="server" Text="Atualizar" /><div>
                            </div>
                        </asp:LinkButton>
                    </div>
                    <div class="divDataGrid">
                        <dxwgv:ASPxGridView ID="gridCadastro" runat="server" EnableCallBacks="true" KeyFieldName="IdLista"
                            OnRowUpdating="gridCadastro_RowUpdating" OnRowInserting="gridCadastro_RowInserting"
                            OnCustomCallback="gridCadastro_CustomCallback" OnBeforeGetCallbackResult="gridCadastro_PreRender"
                            OnCustomColumnDisplayText="gridCadastro_CustomColumnDisplayText" OnCancelRowEditing="gridCadastro_CancelRowEditing"
                            DataSourceID="EsDSCadastroComplementarItemLista" OnPreRender="gridCadastro_PreRender">
                            <Columns>
                                <dxwgv:GridViewCommandColumn ShowSelectCheckbox="True" VisibleIndex="0" Width="5%" ButtonType="Image" ShowClearFilterButton="True">
                                    <HeaderTemplate>
                                        <dxe:ASPxCheckBox ID="cbAll" ClientInstanceName="cbAll" runat="server" ToolTip="Seleciona todos os CheckBoxs de todas as páginas" BackColor="white" ClientSideEvents-CheckedChanged="function(s, e) {OnAllCheckedChanged(s, e, gridCadastro);}" OnInit="CheckBoxSelectAll"/>
                                    </HeaderTemplate>
                                </dxwgv:GridViewCommandColumn>
                                <dxwgv:GridViewDataColumn Caption="Sequência" VisibleIndex="1" Width="10%" HeaderStyle-HorizontalAlign="Left"
                                    CellStyle-HorizontalAlign="Left" ExportWidth="100" />
                                <dxwgv:GridViewDataColumn FieldName="NomeLista" Caption="Nome Lista" VisibleIndex="2"
                                    Width="40%" HeaderStyle-HorizontalAlign="Left" CellStyle-HorizontalAlign="Left"
                                    ExportWidth="100" />
                                <dxwgv:GridViewDataComboBoxColumn FieldName="TipoLista" Caption="Tipo Lista" VisibleIndex="3"
                                    Width="10%" HeaderStyle-HorizontalAlign="Left" CellStyle-HorizontalAlign="Left"
                                    ExportWidth="430">
                                </dxwgv:GridViewDataComboBoxColumn>
                                <dxwgv:GridViewDataColumn FieldName="ItemLista" Caption="Item Lista" VisibleIndex="4"
                                    Width="40%" HeaderStyle-HorizontalAlign="Left" CellStyle-HorizontalAlign="Left"
                                    ExportWidth="100" />
                            </Columns>
                            <Templates>
                                <EditForm>
                                    <asp:Label ID="labelEdicao" runat="server" CssClass="labelInformation" Text=""></asp:Label>
                                    <asp:Panel ID="panelEdicao" runat="server">
                                        <div class="editForm">
                                            <table width="50%">
                                                <tr>
                                                    <td class="td_Label">
                                                        <asp:Label ID="labelNomeLista" runat="server" CssClass="labelRequired" Text="Nome Lista:" />
                                                    </td>
                                                    <td>
                                                        <dxcp:ASPxCallbackPanel runat="server" ClientInstanceName="cbPanel" ID="cbPanel"
                                                            OnCallback="cbPanel_Callback">
                                                            <PanelCollection>
                                                                <dxp:PanelContent runat="server">
                                                                    <dxe:ASPxComboBox ID="dropIdTipoLista" ClientInstanceName="dropIdTipoLista" runat="server"
                                                                        ShowShadow="false" DropDownStyle="DropDownList" CssClass="dropDownListLongo"
                                                                        OnLoad="dropIdTipoLista_OnLoad" Text='<%#Eval("[IdTipoLista]")%>'>
                                                                        <ClientSideEvents ValueChanged="function(s, e) {callbackPanel.PerformCallback(s.GetValue());}" />
                                                                    </dxe:ASPxComboBox>
                                                                </dxp:PanelContent>
                                                            </PanelCollection>
                                                        </dxcp:ASPxCallbackPanel>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="td_Label">
                                                        <asp:Label ID="labelItemLista" runat="server" CssClass="labelRequired" Text="Item Lista:" />
                                                    </td>
                                                    <td>
                                                        <dxcp:ASPxCallbackPanel runat="server" ClientInstanceName="cbItemLista" ID="cbItemLista"
                                                            OnCallback="cbItemLista_Callback">
                                                            <PanelCollection>
                                                                <dxp:PanelContent runat="server">
                                                                    <dxe:ASPxTextBox ID="textItemLista" runat="server" ClientInstanceName="textItemLista"
                                                                        CssClass="textLongo" Text='<%#Eval("[ItemLista]")%>' />
                                                                </dxp:PanelContent>
                                                            </PanelCollection>
                                                        </dxcp:ASPxCallbackPanel>
                                                    </td>
                                                </tr>
                                            </table>
                                            <div class="linhaH">
                                            </div>
                                            <div class="linkButton linkButtonNoBorder popupFooter">
                                                <asp:LinkButton ID="btnOKAdd" runat="server" Font-Overline="false" CssClass="btnSaveadd"
                                                    OnInit="btnOKAdd_Init" OnClientClick="if (operacao != '') return false; operacao='salvarAdd'; callbackErro.SendCallback(); return false;">
                                                    <asp:Literal ID="Literal5" runat="server" Text="OK+" /><div>
                                                    </div>
                                                </asp:LinkButton>
                                                <asp:LinkButton ID="btnOK" runat="server" Font-Overline="false" CssClass="btnOK"
                                                    OnClientClick="if (operacao != '') return false; operacao='salvar'; callbackErro.SendCallback(); return false;">
                                                    <asp:Literal ID="Literal3" runat="server" Text="OK" /><div>
                                                    </div>
                                                </asp:LinkButton>
                                                <asp:LinkButton ID="btnCancel" runat="server" Font-Overline="false" CssClass="btnCancel"
                                                    OnClientClick="gridCadastro.CancelEdit(); return false;">
                                                    <asp:Literal ID="Literal7" runat="server" Text="Cancelar" /><div>
                                                    </div>
                                                </asp:LinkButton>
                                            </div>
                                        </div>
                                    </asp:Panel>
                                </EditForm>
                                <StatusBar>
                                    <asp:Label ID="labelFiltro" runat="server" CssClass="labelNormal" Text="" />
                                </StatusBar>
                            </Templates>
                            <SettingsPopup EditForm-Width="600px" />
                            <SettingsCommandButton>
                                <ClearFilterButton Image-Url="../../imagens/funnel--minus.png"/>
                            </SettingsCommandButton>
                        </dxwgv:ASPxGridView>
                    </div>
                </div>
            </div>
        </div>
        <dxwgv:ASPxGridViewExporter ID="gridExport" runat="server" GridViewID="gridCadastro"
            Landscape="true" />
        <cc1:esDataSource ID="EsDSCadastroComplementarItemLista" runat="server" OnesSelect="EsDSCadastroComplementarItemLista_esSelect" />
        <cc1:esDataSource ID="EsDSCadastroComplementarTipoLista" runat="server" OnesSelect="EsDSCadastroComplementarTipoLista_esSelect" />
    </form>
</body>
</html>
