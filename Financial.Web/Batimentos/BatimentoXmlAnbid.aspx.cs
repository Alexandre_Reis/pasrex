﻿using System;
using Financial.Web.Common;
using Financial.Investidor;
using Financial.WebConfigConfiguration;
using DevExpress.Web;
using System.IO;
using Financial.Security;
using System.Web;
using Financial.Util;
using Financial.Security.Enums;
using Financial.Batimentos;
using System.Collections.Generic;
using System.ComponentModel;

using Financial.Bolsa;
using System.Drawing;

public partial class Batimento_BatimentoXmlAnbid : ConsultaBasePage {
    new protected void Page_Load(object sender, EventArgs e) {
        base.Page_Load(sender, e);
        //
        // Remove Da Sessão
        //Session.Remove("dataSource");
    }

    protected void uplBatimentoXml_FileUploadComplete(object sender, FileUploadCompleteEventArgs e) {
        #region Trata Extensão Valida
        if (!this.isExtensaoValida(e.UploadedFile.FileName.Trim())) {
            e.CallbackData = "Arquivo XML Anbid Inválido: Extensão do arquivo deve ser .xml \n\n";
            return;
        }
        #endregion

        StreamReader sr = new StreamReader(e.UploadedFile.FileContent);
        //
        string msg = "";
        bool erro = false;

        DateTime dataInicio = DateTime.Now;

        try {
            BatimentoXmlAnbid batimentoXmlAnbid = new BatimentoXmlAnbid();
            // Salva o DataSource na Sessão
            Session["dataSource"] = batimentoXmlAnbid.CarregaGridXmlAnbid(sr);
        }
        catch (Exception ex) {
            erro = true;
            //
            msg = "Arquivo Anbid com problemas: " + ex.Message + "\n\n";
        }

        #region Log
        if (!erro) {
            //Log do Processo
            DateTime dataFim = DateTime.Now;
            //
            HistoricoLog historicoLog = new HistoricoLog();
            historicoLog.Descricao = "Batimento Anbid realizado";
            historicoLog.Login = HttpContext.Current.User.Identity.Name;
            historicoLog.Maquina = Utilitario.GetLocalIp();
            historicoLog.Cultura = "";
            historicoLog.DataInicio = dataInicio;
            historicoLog.DataFim = dataFim;
            historicoLog.Origem = (int)HistoricoLogOrigem.Outros;
            historicoLog.Save();
        }
        #endregion

        e.CallbackData = msg;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridConsulta_CustomCallback(object sender, ASPxGridViewCustomCallbackEventArgs e) {
        if (e.Parameters == "Refresh") {

            //AtivoBolsaCollection a = new AtivoBolsaCollection();
            //AtivoBolsa a1 = new AtivoBolsa();
            //a1.CdAtivoBolsa = "Petr4";
            //a.AttachEntity(a1);
            //this.gridConsulta.DataSource = a;
            //gridConsulta.DataBind();
            //
            List<BatimentoXmlAnbid> ds1 = (List<BatimentoXmlAnbid>)Session["dataSource"];
            this.gridConsulta.DataSource = ds1;
            this.gridConsulta.DataBind();
            //
            // Remove Da Sessão
            //Session.Remove("dataSource");
        }
    }

    protected void gridConsulta_HtmlDataCellPrepared(object sender, ASPxGridViewTableDataCellEventArgs e) {
        if (e.DataColumn.FieldName == "Diferenca") {
            decimal value = (decimal)e.GetValue("Diferenca");
            if (value < 0) {
                e.Cell.ForeColor = Color.Red;
            }
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void callbackArquivoXml_Callback(object source, DevExpress.Web.CallbackEventArgs e) {
        e.Result = "";

        //if (textDataArquivos.Text == "") {
        //    e.Result = "Data deve ser selecionada!";
        //    return;
        //}
    }

    /// <summary>
    /// false se arquivo não possui extensão txt ou xml
    /// true se arquivo possui extensão txt ou xml
    /// </summary>
    /// <param name="arquivo">Nome do arquivo</param>
    /// <returns></returns>
    public bool isExtensaoValida(string arquivo) {
        string extensao = new FileInfo(arquivo).Extension;
        extensao = extensao.ToLower();
        if (extensao != ".xml") {
            return false;
        }
        return true;
    }


    new protected void btnPDF_Click(object sender, EventArgs e) {
        if (Session["dataSource"] != null) {
            this.gridConsulta.DataSource = (List<BatimentoXmlAnbid>)Session["dataSource"];
        }
        gridExport.WritePdfToResponse();
    }

    new protected void btnExcel_Click(object sender, EventArgs e) {
        if (Session["dataSource"] != null) {
            this.gridConsulta.DataSource = (List<BatimentoXmlAnbid>)Session["dataSource"];
        }
        gridExport.WriteXlsToResponse();
    }
}