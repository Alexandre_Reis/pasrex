﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="BatimentoXmlAnbid.aspx.cs" Inherits="Batimento_BatimentoXmlAnbid" %>
    
<%@ Register Assembly="System.Web, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a" Namespace="System.Web.UI.HtmlControls" TagPrefix="cc2" %>
<%@ Register Assembly="EntitySpaces.Web" Namespace="EntitySpaces.Web" TagPrefix="cc1" %>

<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxuc" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxe" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxwgv" %>

<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxpc" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxcb" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxw" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxlp" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">

    <link href="~/css/forms.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" language="Javascript" src="../js/global.js"></script>
        
    <script type="text/javascript" language="javascript">        
       function Uploader_OnUploadComplete(args) {
            if (args.callbackData == '') {
                //alert('Arquivo(s) importado(s) com sucesso.');
                
                gridConsulta.PerformCallback('Refresh');
                
            }
            else {
                // Se Ocorreu erro em alguma importação Concatena mensagem Concluida
                alert(args.callbackData);
            }
       }                                                                         
    </script>        
</head>
<body>
    <form id="form1" runat="server">

    <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="true" EnableScriptLocalization="true" />

    <dxcb:ASPxCallback ID="callbackArquivoXmlAnbid" runat="server" OnCallback="callbackArquivoXml_Callback">
        <ClientSideEvents CallbackComplete="function(s, e) {
            if (e.result != '') {                   
                alert(e.result);                              
            }    
            else {                                
                uplBatimentoXml.UploadFile();

            }
        }        
        " />
    </dxcb:ASPxCallback>
                                                            
        <div class="divPanel">
            <table width="100%">
                <tr>
                    <td>
                        <div id="container">
                            <div id="header">
                                <asp:Label ID="lblHeader" runat="server" Text="Batimento XML Anbid" />
                            </div>
                            <div id="mainContent">

                                <table class="dropDownInlineWrapper" border=0 width="500">
                                    <tr>
                                        <td class ="textNormal_5">
                                            <asp:Label ID="labelArquivo" runat="server" Text="Entre com o arquivo:"></asp:Label>
                                        </td>
                                    <td>   
                                    
                                        <dxuc:ASPxUploadControl ID="uplBatimentoXml" runat="server" ClientInstanceName="uplBatimentoXml"
	                                        OnFileUploadComplete="uplBatimentoXml_FileUploadComplete" 
	                                        CssClass="labelNormal" EnableDefaultAppearance="False" FileUploadMode="OnPageLoad" width="250">
                                         <ClientSideEvents FileUploadComplete="function(s, e) { Uploader_OnUploadComplete(e); }" />
                                        </dxuc:ASPxUploadControl>                                        
                                                                                 
                                        </td>
                                    </tr>
                                </table>
                                
                                <div class="linkButton">
                                     
                                    <asp:LinkButton ID="btnRun" runat="server" Font-Overline="false" CssClass="btnRun"
                                            OnClientClick=" {
                                                        if(uplBatimentoXml.GetText()== '') {
                                                            alert('Escolha o arquivo Xml Anbid!');                                                                            
                                                        }
                                                        else {                                                                    
                                                            callbackArquivoXmlAnbid.SendCallback();                                                                             
                                                        }
                                                        return false;                                                                                                                                                                                                                                             
                                                    } 
                                                    ">
                                                    <asp:Literal ID="Literal1" runat="server" Text="Consultar"/><div></div></asp:LinkButton>
                                                                                                                                                                        
                                    <asp:LinkButton ID="btnPDF" runat="server" Font-Overline="false" CssClass="btnPdf" OnClick="btnPDF_Click"><asp:Literal ID="Literal4" runat="server" Text="Exportar"/><div></div></asp:LinkButton>

                                    <asp:LinkButton ID="btnExcel" runat="server" Font-Overline="false" CssClass="btnExcel" OnClick="btnExcel_Click"><asp:Literal ID="Literal5" runat="server" Text="Exportar"/><div></div></asp:LinkButton>

                                    <asp:LinkButton ID="btnRefresh" runat="server" Font-Overline="false" CssClass="btnRefresh" OnClientClick=" gridConsulta.PerformCallback('btnRefresh'); return false;"><asp:Literal ID="Literal6" runat="server" Text="Atualizar"/><div></div></asp:LinkButton>
                                                                                                            
                                </div>
                                
                                <div class="divDataGrid">
                                    <dxwgv:ASPxGridView ID="gridConsulta" ClientInstanceName="gridConsulta" SettingsBehavior-AllowSort="false"
                                        runat="server" OnCustomCallback="gridConsulta_CustomCallback" EnableCallBacks="true"
                                        OnHtmlDataCellPrepared="gridConsulta_HtmlDataCellPrepared">
                                        
                                        <Columns>
                                                                                                                                                                                                                                                 
                                            <dxwgv:GridViewDataColumn FieldName="IdCliente" Caption="Código" VisibleIndex="1" Width="10%" >
                                                <CellStyle HorizontalAlign="Left"/>
                                            </dxwgv:GridViewDataColumn>
                                                                                
                                            <dxwgv:GridViewDataColumn FieldName="Apelido" Caption="Nome" VisibleIndex="2" Width="30%" />
                                                                                                                                    
                                            <dxwgv:GridViewDataColumn FieldName="CdAtivo" Caption="Ativo" VisibleIndex="3" Width="15%" />
                                            
                                            <dxwgv:GridViewDataSpinEditColumn FieldName="ValorXmlAnbid" VisibleIndex="4" Width="10%" >
                                                <PropertiesSpinEdit DisplayFormatString="{0:#,##0.00;(#,##0.00);0.00}"></PropertiesSpinEdit>
                                            </dxwgv:GridViewDataSpinEditColumn>
                                            
                                            <dxwgv:GridViewDataSpinEditColumn FieldName="ValorFinancial" VisibleIndex="5" Width="10%" >
                                                <PropertiesSpinEdit DisplayFormatString="{0:#,##0.00;(#,##0.00);0.00}"></PropertiesSpinEdit>
                                            </dxwgv:GridViewDataSpinEditColumn>
                                            
                                            <dxwgv:GridViewDataSpinEditColumn FieldName="Diferenca" Caption="Diferença" VisibleIndex="6" Width="10%" >
                                                <PropertiesSpinEdit DisplayFormatString="{0:#,##0.00;(#,##0.00);0.00}"></PropertiesSpinEdit>
                                            </dxwgv:GridViewDataSpinEditColumn>
                                                                                                                           
                                        </Columns>
                                        
                                        <SettingsPopup EditForm-Width="500px" />
                                    </dxwgv:ASPxGridView>
                                </div>
                            </div>
                        </div>
                    </td>
                </tr>
            </table>
        </div>
        
        <dxwgv:ASPxGridViewExporter ID="gridExport" runat="server" GridViewID="gridConsulta" Landscape="true"/>

    </form>
       
</body>

</html>