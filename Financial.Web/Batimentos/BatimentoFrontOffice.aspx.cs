﻿using System;
using System.Drawing;
using System.IO;
using System.Web;
using System.Collections.Generic;
using System.ComponentModel;

using Financial.Web.Common;
using Financial.Investidor;
using Financial.WebConfigConfiguration;
using Financial.Security;
using Financial.Util;
using Financial.Batimentos;
using Financial.Bolsa;

using Financial.Security.Enums;

using DevExpress.Web;

public partial class Batimento_BatimentoFrontOffice : ConsultaBasePage {
    new protected void Page_Load(object sender, EventArgs e) {
        base.Page_Load(sender, e);
        //
        if (!Page.IsPostBack) {
            textData.Value = (DateTime)DateTime.Today;
        }
    }

    protected void uplBatimentoFrontOffice_FileUploadComplete(object sender, FileUploadCompleteEventArgs e) {
        #region Trata Extensão Valida
        if (!this.isExtensaoValida(e.UploadedFile.FileName.Trim())) {
            e.CallbackData = "Arquivo Negs Inválido: Extensão do arquivo deve ser .txt ou .dat \n\n";
            return;
        }
        #endregion

        StreamReader sr = new StreamReader(e.UploadedFile.FileContent);
        //
        string msg = "";
        bool erro = false;
        
        DateTime dataReferencia = Convert.ToDateTime(textData.Text);
        //
        string nomeArquivo = e.UploadedFile.FileName.Trim();

        #region Trata IdCliente do Arquivo
        char[] arquivoCliente = nomeArquivo.ToCharArray();

        string idClienteString = "";
        for (int i = 0; char.IsDigit(arquivoCliente[i]); i++) {
            idClienteString += arquivoCliente[i];
        }
        #endregion

        if (String.IsNullOrEmpty(idClienteString)) {
            msg = "\tNegs com problemas - \n";
            msg += "Nome do Arquivo deve conter o Identificador do Cliente. Por favor Renomear o Arquivo.";
            //
            e.CallbackData = msg;
            return;
        }

        int idCliente = Convert.ToInt32(idClienteString);

        //
        DateTime dataInicio = DateTime.Now;

        try {
            BatimentoFrontOffice batimentoFrontOffice = new BatimentoFrontOffice();
            // Salva o DataSource na Sessão
            List<OrdemBolsa.BatimentoOrdemBolsa> ds1 = batimentoFrontOffice.CarregaGridFrontOffice(idCliente, sr, dataReferencia);
            if (ds1.Count == 0) {                               
                e.CallbackData = "Não há Inconsistências.!";
                return;
            }            
            Session["dataSource"] = ds1;
        }
        catch (Exception ex) {
            erro = true;
            //
            msg = "Arquivo Negs com problemas: " + ex.Message + "\n\n";
        }

        #region Log
        if (!erro) {
            //Log do Processo
            DateTime dataFim = DateTime.Now;
            //
            HistoricoLog historicoLog = new HistoricoLog();
            historicoLog.Descricao = "Batimento Front Office realizado";
            historicoLog.Login = HttpContext.Current.User.Identity.Name;
            historicoLog.Maquina = Utilitario.GetLocalIp();
            historicoLog.Cultura = "";
            historicoLog.DataInicio = dataInicio;
            historicoLog.DataFim = dataFim;
            historicoLog.Origem = (int)HistoricoLogOrigem.Outros;
            historicoLog.Save();
        }
        #endregion

        e.CallbackData = msg;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridConsulta_CustomCallback(object sender, ASPxGridViewCustomCallbackEventArgs e) {
        if (e.Parameters == "Refresh") {
            List<OrdemBolsa.BatimentoOrdemBolsa> ds1 = (List<OrdemBolsa.BatimentoOrdemBolsa>)Session["dataSource"];
            this.gridConsulta.DataSource = ds1;
            this.gridConsulta.DataBind();
        }
    }

    protected void gridConsulta_HtmlDataCellPrepared(object sender, ASPxGridViewTableDataCellEventArgs e) {
        //if (e.DataColumn.FieldName == "Diferenca") {
        //    decimal value = (decimal)e.GetValue("Diferenca");
        //    if (value < 0) {
        //        e.Cell.ForeColor = Color.Red;
        //    }
        //}
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void callbackArquivoFrontOffice_Callback(object source, DevExpress.Web.CallbackEventArgs e) {
        e.Result = "";

        if (this.textData.Text == "") {
            e.Result = "Data deve ser selecionada!";
            return;
        }
    }

    /// <summary>
    /// false se arquivo não possui extensão txt ou dat
    /// true se arquivo possui extensão txt ou dat
    /// </summary>
    /// <param name="arquivo">Nome do arquivo</param>
    /// <returns></returns>
    public bool isExtensaoValida(string arquivo) {
        string extensao = new FileInfo(arquivo).Extension;
        extensao = extensao.ToLower();
        if (extensao != ".txt" && extensao != ".dat") {
            return false;
        }
        return true;
    }


    new protected void btnPDF_Click(object sender, EventArgs e) {
        if (Session["dataSource"] != null) {
            this.gridConsulta.DataSource = (List<OrdemBolsa.BatimentoOrdemBolsa>)Session["dataSource"];
        }
        gridExport.WritePdfToResponse();
    }

    new protected void btnExcel_Click(object sender, EventArgs e) {
        if (Session["dataSource"] != null) {
            this.gridConsulta.DataSource = (List<OrdemBolsa.BatimentoOrdemBolsa>)Session["dataSource"];
        }
        gridExport.WriteXlsToResponse();
    }
}