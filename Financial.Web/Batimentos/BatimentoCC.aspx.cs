﻿using System;
using Financial.Web.Common;
using Financial.Investidor;
using Financial.WebConfigConfiguration;

public partial class Batimento_BatimentoCC : ConsultaBasePage {
    new protected void Page_Load(object sender, EventArgs e) {
        base.Page_Load(sender, e);
    }

    protected void EsDSGrupoProcessamento_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e) {
        GrupoProcessamentoCollection coll = new GrupoProcessamentoCollection();
        coll.LoadAll();

        GrupoProcessamento g = new GrupoProcessamento();
        g.IdGrupoProcessamento = -1;
        g.Descricao = "";

        coll.AttachEntity(g);

        coll.Sort = GrupoProcessamentoMetadata.ColumnNames.Descricao + " ASC";

        e.Collection = coll;
    }

    /// <summary>
    /// Carrega o TextBox de Lista Sinacor
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void ListaNumeros_OnPreRender(object sender, EventArgs e) {
        this.textLista.Text = WebConfig.AppSettings.ListaNumeros.Trim();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridConsulta_CustomCallback(object sender, DevExpress.Web.ASPxGridViewCustomCallbackEventArgs e) {
        gridConsulta.DataBind();
    }
}