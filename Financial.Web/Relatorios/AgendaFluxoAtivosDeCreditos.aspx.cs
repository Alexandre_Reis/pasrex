﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using DevExpress.Web;



using Financial.Web.Common;
using Financial.Investidor;
using Financial.Fundo;
using Financial.Investidor.Enums;
using Financial.Security;
using System.Threading;


public partial class Relatorios_AgendaFluxoAtivosDeCreditos : CadastroBasePage
{
    new protected void Page_Load(object sender, EventArgs e)
    {
        this.HasFiltro = true;
        this.HasPopupCarteira = true;
        base.Page_Load(sender, e);
    }

    protected void ASPxCallback2_Callback(object source, DevExpress.Web.CallbackEventArgs e)
    {
        ASPxComboBox dropTipoOperacao = gridCadastro.FindEditFormTemplateControl("dropTipoOperacao") as ASPxComboBox;
        Carteira carteira = new Carteira();

        string nome = "";
        string resultado = "";
        if (!String.IsNullOrEmpty(e.Parameter) && e.Parameter != "null")
        {
            int idCarteira = Convert.ToInt32(e.Parameter);
            if (carteira.LoadByPrimaryKey(idCarteira))
            {
                Cliente cliente = new Cliente();
                cliente.LoadByPrimaryKey(idCarteira);

                if (cliente.Status.Value != (byte)StatusCliente.Divulgado)
                {
                    if (carteira.IsAtivo)
                    {
                        //O permissionamento da carteira é dado direto no cliente vinculado à carteira
                        PermissaoCliente permissaoCliente = new PermissaoCliente();
                        if (permissaoCliente.RetornaAcessoClienteComControle(idCarteira, HttpContext.Current.User.Identity.Name))
                        {

                            nome = carteira.str.Apelido;
                            ASPxDateEdit textDataOperacao = gridCadastro.FindEditFormTemplateControl("textDataOperacao") as ASPxDateEdit;

                            DateTime dataOperacao = (textDataOperacao != null && textDataOperacao.Text != "")
                                           ? Convert.ToDateTime(textDataOperacao.Text)
                                           : cliente.DataDia.Value;

                            resultado = nome + "|" + dataOperacao.ToString().Substring(0, 10) + "|" + Thread.CurrentThread.CurrentCulture.Name;
                        }
                        else
                        {
                            resultado = "no_access";
                        }
                    }
                    else
                    {
                        resultado = "no_active";
                    }
                }
                else
                {
                    resultado = cliente.str.Apelido + "|status_closed";
                }
            }
        }

        e.Result = resultado;
    }
   
    protected void EsDSAgendaEFluxo_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        ViewGPSAgendaFluxoAtivosCreditosCollection coll = new ViewGPSAgendaFluxoAtivosCreditosCollection();
        ViewGPSAgendaFluxoAtivosCreditosQuery query = new ViewGPSAgendaFluxoAtivosCreditosQuery();


        if (!String.IsNullOrEmpty(btnEditCodigoCarteiraFiltro.Text))
        {
            query.Where(query.IdCliente == Convert.ToInt32(btnEditCodigoCarteiraFiltro.Text));
        }        

        if (!String.IsNullOrEmpty(textDataInicio.Text))
        {
            query.Where(query.Data >= textDataInicio.Text);
        }

        if (!String.IsNullOrEmpty(textDataFim.Text))
        {
            query.Where(query.Data <= textDataFim.Text);
        }
        

        
        coll.Load(query);
        e.Collection = coll;

    }

    protected void gridCadastro_CustomCallback(object sender, ASPxGridViewCustomCallbackEventArgs e)
    {
        gridCadastro.DataBind();
        gridCadastro.Selection.UnselectAll();
        gridCadastro.CancelEdit();
    }

 

    protected void EsDSCarteira_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        CarteiraCollection coll = new CarteiraCollection();
        coll.BuscaCarteirasComAcesso(Context.User.Identity.Name);

        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }



}
