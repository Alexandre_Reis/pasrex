﻿using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Financial.Common;
using EntitySpaces.Interfaces;
using System.Collections.Generic;
using Financial.Relatorio;
using System.IO;
using System.Globalization;
using DevExpress.XtraReports.UI;

public partial class _ReportMapaMovimentacaoCustodiaBMF : Financial.Web.Common.BasePage {
    /*  Como Usar o Relatório
        DateTime dataInicio = new DateTime(2000, 1, 1);
        DateTime dataFim = new DateTime(2010, 1, 25);
        string cdAtivoBMF = "DO";
        int tipoMercado = 2;
        int idCliente = 32;

        // 5 parametros
        ReportViewer1.Report = new ReportMapaMovimentacaoCustodiaBMF(dataInicio, dataFim, tipoMercado, idCliente, cdAtivoBMF);
        
        // 3 parametros
        //ReportViewer1.Report = new ReportMapaMovimentacaoCustodiaBMF(dataInicio, dataFim, idCliente);
        //ReportViewer1.Report = new ReportMapaMovimentacaoCustodiaBMF(dataInicio, dataFim, cdAtivoBMF);
        //ReportViewer1.Report = new ReportMapaMovimentacaoCustodiaBMF(tipoMercado, dataInicio, dataFim);

        // 4 parametros
        //ReportViewer1.Report = new ReportMapaMovimentacaoCustodiaBMF(dataInicio, dataFim, cdAtivoBMF, idCliente);
        //ReportViewer1.Report = new ReportMapaMovimentacaoCustodiaBMF(tipoMercado, dataInicio, dataFim, cdAtivoBMF);
        //ReportViewer1.Report = new ReportMapaMovimentacaoCustodiaBMF(tipoMercado, idCliente, dataInicio, dataFim);       

        // 2 parametros
        //ReportViewer1.Report = new ReportMapaMovimentacaoCustodiaBMF(dataInicio, dataFim);            
     */
    new protected void Page_Load(object sender, EventArgs e) {
        string visao = Request.QueryString["Visao"].ToString();
        //
        //Response.Write(Request.Form["textDataInicio"].ToString());
        //Response.Write("<br>" +Request.Form["textDataFim"].ToString());
        //Response.Write("<br>" + Request.Form["btnEditCodigo"].ToString());
        //Response.Write("<br>" + Request.Form["dropTipoMercado"].ToString());
        //Response.Write("<br>" + Request.Form["textCdAtivo"].ToString());
        
        DateTime dataInicio = new DateTime();
        DateTime dataFim = new DateTime();
        string cdAtivoBMF = null;
        int? idCliente = null;
        int? tipoMercado = null;
       	
        // Obrigatorio
        dataInicio = Convert.ToDateTime(Session["textDataInicio"]);
        dataFim = Convert.ToDateTime(Session["textDataFim"]);
        //Opcionais
        cdAtivoBMF = (string)Session["textCdAtivo"];
        if (!String.IsNullOrEmpty((string)Session["btnEditCodigo"])) {
            idCliente = Convert.ToInt32(Session["btnEditCodigo"]);
        }
        if (!String.IsNullOrEmpty((string)Session["dropTipoMercado"])) {
            tipoMercado = Convert.ToInt32(Session["dropTipoMercado"]);
        }

        if (visao == "Report") {
            // XtraReport rep = XtraReport.FromFile(@"C:\Documents and Settings\rtortima\Desktop\Cliente.repx", true);
            ReportViewer1.Report = new ReportMapaMovimentacaoCustodiaBMF(dataInicio, dataFim, tipoMercado, idCliente, cdAtivoBMF);
            //ReportViewer1.Report = rep;
        }                                
        else if (visao == "PDF") {
           #region PDF
            ReportMapaMovimentacaoCustodiaBMF report = new ReportMapaMovimentacaoCustodiaBMF(dataInicio, dataFim, tipoMercado, idCliente, cdAtivoBMF);
            //XtraReport rep = XtraReport.FromFile(@"C:\Documents and Settings\rtortima\Desktop\ReportAtivoBolsa.repx", true);
                       
            MemoryStream ms = new MemoryStream();
            report.ExportToPdf(ms);
            ms.Seek(0, SeekOrigin.Begin);
            Response.ClearContent();
            Response.ClearHeaders();
            Response.Buffer = true;
            Response.Cache.SetCacheability(HttpCacheability.Private);
            Response.ContentType = "application/rtf";
            Response.AddHeader("Content-Length", ms.Length.ToString(CultureInfo.CurrentCulture));
            Response.AddHeader("Content-Disposition", String.Format("attachment;filename=ReportMapaMovimentacaoCustodiaBMF.pdf"));

            Response.BinaryWrite(ms.ToArray());
            ms.Close();
            HttpContext.Current.ApplicationInstance.CompleteRequest();
            Response.End();
            #endregion
        }
        else if (visao == "Excel") {
            #region Excel
            ReportMapaMovimentacaoCustodiaBMF report = new ReportMapaMovimentacaoCustodiaBMF(dataInicio, dataFim, tipoMercado, idCliente, cdAtivoBMF);

            MemoryStream ms = new MemoryStream();
            report.ExportToXls(ms);
            ms.Seek(0, SeekOrigin.Begin);
            Response.ClearContent();
            Response.ClearHeaders();
            Response.Buffer = true;
            Response.Cache.SetCacheability(HttpCacheability.Private);
            Response.ContentType = "application/vnd.ms-excel";
            Response.AddHeader("Content-Length", ms.Length.ToString(CultureInfo.CurrentCulture));
            Response.AddHeader("Content-Disposition", String.Format("attachment;filename=ReportMapaMovimentacaoCustodiaBMF.xls"));

            Response.BinaryWrite(ms.ToArray());
            ms.Close();
            HttpContext.Current.ApplicationInstance.CompleteRequest();
            Response.End();

            #endregion
        }         
    }
}