﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Financial.Fundo;
using Financial.Investidor;
using System.Collections.Generic;
using Financial.Web.Util;
using Financial.Investidor.Enums;
using Financial.Security;
using log4net;
using Financial.Security.Enums;
using EntitySpaces.Interfaces;
using DevExpress.Web;
using System.Text;
using Financial.Web.Common;
using Financial.Common;
using System.Threading;

public partial class FiltroReportMapaOperacaoBMF : FiltroReportBasePage {
    private static readonly ILog log = LogManager.GetLogger(typeof(FiltroReportMapaOperacaoBMF));

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    new protected void Page_Load(object sender, EventArgs e) {
        btnEditCodigo.Focus();

        if (!Page.IsPostBack) {
            List<Hashtable> list = Combos.carregaComboTipoMercadoBMF(ConfiguracaoCombo.OpcaoBranco);

            for (int i = 0; i < list.Count; i++) {
                // Hashtable
                Hashtable hashtable = (Hashtable)list[i];

                foreach (DictionaryEntry item in hashtable) {
                    dropTipoMercado.Items.Add(item.Value.ToString(), item.Key.ToString());
                }
            }
        }

        this.HasPopupCliente = true;
        base.Page_Load(sender, e);

        this.PopulaPeriodoDatas(this.dropTipoPeriodo);

        TrataTravamentoCampos();
    }

    /// <summary>
    /// 
    /// </summary>
    private void TrataTravamentoCampos() {
        Usuario usuario = new Usuario();
        usuario.BuscaUsuario(HttpContext.Current.User.Identity.Name);
        int tipoTrava = usuario.TipoTrava.Value;

        if (tipoTrava == (int)TipoTravaUsuario.TravaCliente) {
            btnEditCodigo.Enabled = false;

            PermissaoCliente permissaoCliente = new PermissaoCliente();
            int? idCliente = permissaoCliente.RetornaClienteAssociado(HttpContext.Current.User.Identity.Name);

            if (idCliente.HasValue) {
                Cliente cliente = new Cliente();

                List<esQueryItem> campos = new List<esQueryItem>();
                campos.Add(cliente.Query.Apelido);
                cliente.LoadByPrimaryKey(campos, idCliente.Value);
                string apelido = cliente.Apelido;

                btnEditCodigo.Text = Convert.ToString(idCliente.Value);
                textNome.Text = apelido;
            }
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void ASPxCallback1_Callback(object source, DevExpress.Web.CallbackEventArgs e) {
        string nome = "";
        if (!String.IsNullOrEmpty(e.Parameter) && e.Parameter != "null") {
            int idCliente = Convert.ToInt32(e.Parameter);
            Cliente cliente = new Cliente();
            cliente.LoadByPrimaryKey(idCliente);

            if (cliente.str.Apelido != "") {
                if (cliente.IsAtivo) {
                    PermissaoCliente permissaoCliente = new PermissaoCliente();
                    nome = permissaoCliente.RetornaAcessoClienteComControle(idCliente, HttpContext.Current.User.Identity.Name)
                            ? cliente.str.Apelido : "no_access";                                       
                }
                else {
                    nome = "no_active";
                }
            }
        }
        e.Result = nome;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void callBackPeriodo_Callback(object source, DevExpress.Web.CallbackEventArgs e)
    {
        e.Result = "";
        if (btnEditCodigo.Text != "")
        {
            int idCliente = Convert.ToInt32(btnEditCodigo.Text);

            Cliente cliente = new Cliente();

            DateTime dataInicio = new DateTime();
            DateTime dataFim = new DateTime();

            if (dropTipoPeriodo.SelectedIndex > 0)
            {
                cliente.RetornaDatas(idCliente, (TipoPeriodoDatas)dropTipoPeriodo.SelectedIndex, ref dataInicio, ref dataFim);

                e.Result = dataInicio.ToShortDateString() + "|" + dataFim.ToShortDateString() + "|" + Thread.CurrentThread.CurrentCulture.Name;
            }
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void EsDSCliente_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e) {
        ClienteCollection clienteCollection = new ClienteCollection();
        clienteCollection.BuscaClientesComAcesso(HttpContext.Current.User.Identity.Name);

        e.Collection = clienteCollection;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void EsDSAgenteMercado_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        AgenteMercadoCollection agenteMercadoCollection = new AgenteMercadoCollection();
        //
        agenteMercadoCollection.Query
                        .Select(agenteMercadoCollection.Query.IdAgente, agenteMercadoCollection.Query.Nome)
                        .Where(agenteMercadoCollection.Query.FuncaoCorretora == 'S')
                        .OrderBy(agenteMercadoCollection.Query.Nome.Ascending);

        agenteMercadoCollection.Query.Load();
        //
        AgenteMercado a = new AgenteMercado();
        a.IdAgente = 0;
        a.Nome = "";

        agenteMercadoCollection.AttachEntity(a);
        //
        agenteMercadoCollection.Sort = AgenteMercadoMetadata.ColumnNames.Nome + " ASC";
        //
        e.Collection = agenteMercadoCollection; 
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnVisualiza_Click(object sender, EventArgs e) {
        this.TrataErros();
        this.SelecionaRelatorio(VisaoRelatorio.visaoHTML);
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnPDF_Click(object sender, EventArgs e) {
        this.TrataErros();
        this.SelecionaRelatorio(VisaoRelatorio.visaoPDF);
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnExcel_Click(object sender, EventArgs e) {
        this.TrataErros();
        this.SelecionaRelatorio(VisaoRelatorio.visaoExcel);
    }

    /// <summary>
    /// Trata os Erros de Relatorio
    /// </summary>
    private void TrataErros() {

        #region Campos obrigatórios
        List<Control> controles = new List<Control>(new Control[] { 
    	    this.textDataInicio, this.textDataFim });

        if (base.TestaObrigatorio(controles) != "") {
            throw new Exception("Campos com * são obrigatórios!");
        }
        #endregion
    }

    /// <summary>
    /// Seleciona o relatorio a ser executado
    /// </summary>
    /// <param name="visao">PDF, Excel ou Report</param>
    private void SelecionaRelatorio(string visao) {
        Response.BufferOutput = true;
        //
        // Salva os parametros na session
        Session["textDataInicio"] = textDataInicio.Text;
        Session["textDataFim"] = textDataFim.Text;
        Session["btnEditCodigo"] = btnEditCodigo.Text;
        Session["dropTipoMercado"] = Convert.ToString(dropTipoMercado.SelectedItem.Value);
        Session["textCdAtivo"] = textCdAtivo.Text;

        string idAgente = "";
        if (dropAgente.SelectedIndex > 0) {
            idAgente = Convert.ToString(dropAgente.SelectedItem.Value);
        }
        Session["dropAgente"] = idAgente;

        string tipoOperacao = "";
        if (this.dropTipoOperacao.SelectedIndex > 0) {
            tipoOperacao = Convert.ToString(dropTipoOperacao.SelectedItem.Value);
        }
        Session["dropTipoOperacao"] = tipoOperacao;

        //Response.Redirect("~/Relatorios/BMF/ReportMapaOperacaoBMF.aspx?Visao=" + visao);

        string url = "~/Relatorios/BMF/ReportMapaOperacaoBMF.aspx?Visao=" + visao;
        string redirectURL = Page.ResolveClientUrl(url);

        string script = "window.location = '" + redirectURL + "';";
        ScriptManager.RegisterStartupScript(this, typeof(Page), "RedirectTo", script, true);
    }
}