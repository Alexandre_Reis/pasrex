using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Financial.Common;
using EntitySpaces.Interfaces;
using System.Collections.Generic;
using Financial.Relatorio;
using System.IO;
using System.Globalization;
using Financial.Web.Common;

public partial class _ReportMapaOperacaoBMF : BasePage {

    new protected void Page_Load(object sender, EventArgs e) {
        string visao = Request.QueryString["Visao"].ToString();        
    
        DateTime dataInicio = new DateTime();
        DateTime dataFim = new DateTime();
        string cdAtivoBMF = null;
        int? idCliente = null;
        int? tipoMercado = null;
        int? idAgenteCorretora = null;
        string tipoOperacao = null;
               	
        // Obrigatorio
        dataInicio = Convert.ToDateTime(Session["textDataInicio"]);
        dataFim = Convert.ToDateTime(Session["textDataFim"]);
        //Opcionais
        cdAtivoBMF = (string)Session["textCdAtivo"];
        if (!String.IsNullOrEmpty((string)Session["btnEditCodigo"])) {
            idCliente = Convert.ToInt32(Session["btnEditCodigo"]);
        }
        if (!String.IsNullOrEmpty((string)Session["dropTipoMercado"])) {
            tipoMercado = Convert.ToInt32(Session["dropTipoMercado"]);
        }
        if (!String.IsNullOrEmpty((string)Session["dropAgente"])) {
            idAgenteCorretora = Convert.ToInt32(Session["dropAgente"]);
        }
        if (!String.IsNullOrEmpty((string)Session["dropTipoOperacao"])) {
            tipoOperacao = Convert.ToString(Session["dropTipoOperacao"]);
        }

        if (visao == "Report") {
            ReportViewer1.Report = new ReportMapaOperacaoBMF(dataInicio, dataFim, tipoMercado, idCliente, cdAtivoBMF, idAgenteCorretora, tipoOperacao);
        }                                
        else if (visao == "PDF") {
           #region PDF
            ReportMapaOperacaoBMF report = new ReportMapaOperacaoBMF(dataInicio, dataFim, tipoMercado, idCliente, cdAtivoBMF, idAgenteCorretora, tipoOperacao);
           
            MemoryStream ms = new MemoryStream();
            report.ExportToPdf(ms);
            ms.Seek(0, SeekOrigin.Begin);
            Response.ClearContent();
            Response.ClearHeaders();
            Response.Buffer = true;
            Response.Cache.SetCacheability(HttpCacheability.Private);
            Response.ContentType = "application/pdf";
            Response.AddHeader("Content-Length", ms.Length.ToString(CultureInfo.CurrentCulture));            
            Response.AddHeader("Content-Disposition", String.Format("attachment;filename=ReportMapaOperacaoBMF.pdf"));

            Response.BinaryWrite(ms.ToArray());
            ms.Close();
            HttpContext.Current.ApplicationInstance.CompleteRequest();
            Response.End();
            #endregion
        }
        else if (visao == "Excel") {
           #region Excel
            ReportMapaOperacaoBMF report = new ReportMapaOperacaoBMF(dataInicio, dataFim, tipoMercado, idCliente, cdAtivoBMF, idAgenteCorretora, tipoOperacao);

            MemoryStream ms = new MemoryStream();
            report.ExportToXls(ms);
            ms.Seek(0, SeekOrigin.Begin);
            Response.ClearContent();
            Response.ClearHeaders();
            Response.Buffer = true;
            Response.Cache.SetCacheability(HttpCacheability.Private);
            Response.ContentType = "application/vnd.ms-excel";
            Response.AddHeader("Content-Length", ms.Length.ToString(CultureInfo.CurrentCulture));            
            Response.AddHeader("Content-Disposition", String.Format("attachment;filename=ReportMapaOperacaoBMF.xls"));

            Response.BinaryWrite(ms.ToArray());
            ms.Close();
            HttpContext.Current.ApplicationInstance.CompleteRequest();
            Response.End();

            #endregion
        }         
    }
}