﻿using System;
using System.Web;
using System.Globalization;
using System.IO;
using Financial.Relatorio;
using Financial.Web.Common;

public partial class _ReportRebateGestor : BasePage {
    private static class VisaoRelatorio {
        public static string visaoHTML = "Report";
        public static string visaoPDF = "PDF";
        public static string visaoExcel = "Excel";
    }

    private enum TipoRelatorio { 
        Sintetico = 1,
        GestorCliente = 2,
        ClienteGestor = 3,
    }

    new protected void Page_Load(object sender, EventArgs e) {
        //
        string visao = Request.QueryString["Visao"].ToString();
        int mes = Convert.ToInt32(Session["dropMes"]);
        int ano = Convert.ToInt32(Session["dropAno"]);
        //
        int tipo = Convert.ToInt32(Session["dropTipo"]);
        //

        switch (tipo) {
            case (int)TipoRelatorio.Sintetico:
                #region ReportRebateGestor

                if (visao == VisaoRelatorio.visaoHTML) {
                    ReportViewer1.Report = new ReportRebateGestor(mes, ano);
                }
                else if (visao == VisaoRelatorio.visaoPDF) {
                    #region PDF
                    ReportRebateGestor report = new ReportRebateGestor(mes, ano);

                    MemoryStream ms = new MemoryStream();
                    report.ExportToPdf(ms);
                    ms.Seek(0, SeekOrigin.Begin);
                    Response.ClearContent();
                    Response.ClearHeaders();
                    Response.Buffer = true;
                    Response.Cache.SetCacheability(HttpCacheability.Private);
                    Response.ContentType = "application/pdf";
                    Response.AddHeader("Content-Length", ms.Length.ToString(CultureInfo.CurrentCulture));
                    Response.AddHeader("Content-Disposition", String.Format("attachment;filename=ReportRebateGestor.pdf"));

                    Response.BinaryWrite(ms.ToArray());
                    ms.Close();
                    HttpContext.Current.ApplicationInstance.CompleteRequest();
                    Response.End();
                    #endregion
                }
                else if (visao == VisaoRelatorio.visaoExcel) {
                    #region Excel
                    ReportRebateGestor report = new ReportRebateGestor(mes, ano);

                    MemoryStream ms = new MemoryStream();
                    report.ExportToXls(ms);
                    ms.Seek(0, SeekOrigin.Begin);
                    Response.ClearContent();
                    Response.ClearHeaders();
                    Response.Buffer = true;
                    Response.Cache.SetCacheability(HttpCacheability.Private);
                    Response.ContentType = "application/vnd.ms-excel";
                    Response.AddHeader("Content-Length", ms.Length.ToString(CultureInfo.CurrentCulture));
                    Response.AddHeader("Content-Disposition", String.Format("attachment;filename=ReportRebateGestor.xls"));

                    Response.BinaryWrite(ms.ToArray());
                    ms.Close();
                    HttpContext.Current.ApplicationInstance.CompleteRequest();
                    Response.End();

                    #endregion
                }
                #endregion
                break;
            case (int)TipoRelatorio.GestorCliente:
                #region ReportRebateGestorCliente
                if (visao == VisaoRelatorio.visaoHTML) {
                    ReportViewer1.Report = new ReportRebateGestorCliente(mes, ano);
                }
                else if (visao == VisaoRelatorio.visaoPDF) {
                    #region PDF
                    ReportRebateGestorCliente report = new ReportRebateGestorCliente(mes, ano);

                    MemoryStream ms = new MemoryStream();
                    report.ExportToPdf(ms);
                    ms.Seek(0, SeekOrigin.Begin);
                    Response.ClearContent();
                    Response.ClearHeaders();
                    Response.Buffer = true;
                    Response.Cache.SetCacheability(HttpCacheability.Private);
                    Response.ContentType = "application/pdf";
                    Response.AddHeader("Content-Length", ms.Length.ToString(CultureInfo.CurrentCulture));
                    Response.AddHeader("Content-Disposition", String.Format("attachment;filename=ReportRebateGestorCliente.pdf"));

                    Response.BinaryWrite(ms.ToArray());
                    ms.Close();
                    HttpContext.Current.ApplicationInstance.CompleteRequest();
                    Response.End();
                    #endregion
                }
                else if (visao == VisaoRelatorio.visaoExcel) {
                    #region Excel
                    ReportRebateGestorCliente report = new ReportRebateGestorCliente(mes, ano);

                    MemoryStream ms = new MemoryStream();
                    report.ExportToXls(ms);
                    ms.Seek(0, SeekOrigin.Begin);
                    Response.ClearContent();
                    Response.ClearHeaders();
                    Response.Buffer = true;
                    Response.Cache.SetCacheability(HttpCacheability.Private);
                    Response.ContentType = "application/vnd.ms-excel";
                    Response.AddHeader("Content-Length", ms.Length.ToString(CultureInfo.CurrentCulture));
                    Response.AddHeader("Content-Disposition", String.Format("attachment;filename=ReportRebateGestorCliente.xls"));

                    Response.BinaryWrite(ms.ToArray());
                    ms.Close();
                    HttpContext.Current.ApplicationInstance.CompleteRequest();
                    Response.End();

                    #endregion
                }
                #endregion
                break;
            case (int)TipoRelatorio.ClienteGestor:
                #region ReportRebateClienteGestor
                if (visao == VisaoRelatorio.visaoHTML) {
                    ReportViewer1.Report = new ReportRebateClienteGestor(mes, ano);
                }
                else if (visao == VisaoRelatorio.visaoPDF) {
                    #region PDF
                    ReportRebateClienteGestor report = new ReportRebateClienteGestor(mes, ano);

                    MemoryStream ms = new MemoryStream();
                    report.ExportToPdf(ms);
                    ms.Seek(0, SeekOrigin.Begin);
                    Response.ClearContent();
                    Response.ClearHeaders();
                    Response.Buffer = true;
                    Response.Cache.SetCacheability(HttpCacheability.Private);
                    Response.ContentType = "application/pdf";
                    Response.AddHeader("Content-Length", ms.Length.ToString(CultureInfo.CurrentCulture));
                    Response.AddHeader("Content-Disposition", String.Format("attachment;filename=ReportRebateClienteGestor.pdf"));

                    Response.BinaryWrite(ms.ToArray());
                    ms.Close();
                    HttpContext.Current.ApplicationInstance.CompleteRequest();
                    Response.End();
                    #endregion
                }
                else if (visao == VisaoRelatorio.visaoExcel) {
                    #region Excel
                    ReportRebateClienteGestor report = new ReportRebateClienteGestor(mes, ano);

                    MemoryStream ms = new MemoryStream();
                    report.ExportToXls(ms);
                    ms.Seek(0, SeekOrigin.Begin);
                    Response.ClearContent();
                    Response.ClearHeaders();
                    Response.Buffer = true;
                    Response.Cache.SetCacheability(HttpCacheability.Private);
                    Response.ContentType = "application/vnd.ms-excel";
                    Response.AddHeader("Content-Length", ms.Length.ToString(CultureInfo.CurrentCulture));
                    Response.AddHeader("Content-Disposition", String.Format("attachment;filename=ReportRebateClienteGestor.xls"));

                    Response.BinaryWrite(ms.ToArray());
                    ms.Close();
                    HttpContext.Current.ApplicationInstance.CompleteRequest();
                    Response.End();

                    #endregion
                }
                #endregion
                break;
        }                                                        
    }
}