using System;
using Financial.Web.Common;
using System.IO;
using System.Web;
using System.Collections.Generic;
using Financial.Relatorio;
using System.Globalization;

public partial class _ReportEvolucaoFundo : BasePage {
    private static class VisaoRelatorio {
        public static string visaoHTML = "Report";
        public static string visaoPDF = "PDF";
        public static string visaoExcel = "Excel";
    }

    private enum TipoRelatorio {
        RetornoAcumulado = 1,
        EvolucaoPL = 2,
        EvolucaoCota = 3
    }

    new protected void Page_Load(object sender, EventArgs e) {
        //
        string visao = Request.QueryString["Visao"].ToString();
        //
        // Obrigatorio
        DateTime dataInicio = Convert.ToDateTime(Session["textDataInicio"]);
        DateTime dataFim = Convert.ToDateTime(Session["textDataFim"]);
        //        
        TipoRelatorio tipoRelatorio = (TipoRelatorio)Session["dropTipoRelatorio"];
        
        // Nome Do Arquivo
        string fileName = "";
        //
        switch (tipoRelatorio) {
            case TipoRelatorio.RetornoAcumulado: fileName = "RetornoAcumulado";
                break;
            case TipoRelatorio.EvolucaoPL: fileName = "EvolucaoPL";
                break;
            case TipoRelatorio.EvolucaoCota: fileName = "EvolucaoCota";
                break;
        }              

        if (visao == VisaoRelatorio.visaoHTML) {
            #region HTML
            switch (tipoRelatorio) {
                case TipoRelatorio.RetornoAcumulado:
                    List<int> listaIdCarteiras = (List<int>)(Session["listaIdCarteiras"]);
                    List<int> listaIdIndices = (List<int>)(Session["listaIdIndices"]);
                    ReportViewer1.Report = new ReportGraficoRetornoAcumulado(listaIdCarteiras, listaIdIndices, dataInicio, dataFim);
                    break;
                case TipoRelatorio.EvolucaoPL:
                    listaIdCarteiras = (List<int>)(Session["listaIdCarteiras"]);
                    ReportViewer1.Report = new ReportGraficoEvolucaoPL(listaIdCarteiras, dataInicio, dataFim);
                    break;
                case TipoRelatorio.EvolucaoCota:
                    listaIdCarteiras = (List<int>)(Session["listaIdCarteiras"]);
                    ReportViewer1.Report = new ReportGraficoEvolucaoCota(listaIdCarteiras, dataInicio, dataFim);
                    break;
            }
            #endregion
        }
        else if (visao == VisaoRelatorio.visaoPDF) {
            #region PDF
            MemoryStream ms = new MemoryStream();
            //            
            switch (tipoRelatorio) {
                case TipoRelatorio.RetornoAcumulado:
                    List<int> listaIdCarteiras = (List<int>)(Session["listaIdCarteiras"]);
                    List<int> listaIdIndices = (List<int>)(Session["listaIdIndices"]);
                    ReportGraficoRetornoAcumulado report = new ReportGraficoRetornoAcumulado(listaIdCarteiras, listaIdIndices, dataInicio, dataFim);
                    report.ExportToPdf(ms);
                    break;
                case TipoRelatorio.EvolucaoPL:
                    listaIdCarteiras = (List<int>)(Session["listaIdCarteiras"]);
                    ReportGraficoEvolucaoPL report1 = new ReportGraficoEvolucaoPL(listaIdCarteiras, dataInicio, dataFim);
                    report1.ExportToPdf(ms);
                    break;
                case TipoRelatorio.EvolucaoCota:
                    listaIdCarteiras = (List<int>)(Session["listaIdCarteiras"]);
                    ReportGraficoEvolucaoCota report2 = new ReportGraficoEvolucaoCota(listaIdCarteiras, dataInicio, dataFim);
                    report2.ExportToPdf(ms);
                    break;
            }
            //        

            //Session.Remove("textDataInicio");
            //Session.Remove("textDataFim");
            ////
            //Session.Remove("listaIdCarteiras");
            //Session.Remove("listaIdIndices");
            ////
            //Session.Remove("dropTipoRelatorio");

            ms.Seek(0, SeekOrigin.Begin);
            Response.ClearContent();
            Response.ClearHeaders();
            Response.Buffer = true;
            Response.Cache.SetCacheability(HttpCacheability.Private);
            Response.ContentType = "application/pdf";
            Response.AddHeader("Content-Length", ms.Length.ToString(CultureInfo.CurrentCulture));
            Response.AddHeader("Content-Disposition", String.Format("attachment;filename=" + fileName + ".pdf"));

            Response.BinaryWrite(ms.ToArray());
            ms.Close();
            HttpContext.Current.ApplicationInstance.CompleteRequest();
            Response.End();
            #endregion
        }
        else if (visao == VisaoRelatorio.visaoExcel) {
            #region Excel

            MemoryStream ms = new MemoryStream();
            //
            switch (tipoRelatorio) {
                case TipoRelatorio.RetornoAcumulado:
                    List<int> listaIdCarteiras = (List<int>)(Session["listaIdCarteiras"]);
                    List<int> listaIdIndices = (List<int>)(Session["listaIdIndices"]);
                    ReportGraficoRetornoAcumulado report = new ReportGraficoRetornoAcumulado(listaIdCarteiras, listaIdIndices, dataInicio, dataFim);
                    report.ExportToXls(ms);
                    break;
                case TipoRelatorio.EvolucaoPL:
                    listaIdCarteiras = (List<int>)(Session["listaIdCarteiras"]);
                    ReportGraficoEvolucaoPL report1 = new ReportGraficoEvolucaoPL(listaIdCarteiras, dataInicio, dataFim);
                    report1.ExportToXls(ms);
                    break;
                case TipoRelatorio.EvolucaoCota:
                    listaIdCarteiras = (List<int>)(Session["listaIdCarteiras"]);
                    ReportGraficoEvolucaoCota report2 = new ReportGraficoEvolucaoCota(listaIdCarteiras, dataInicio, dataFim);
                    report2.ExportToXls(ms);
                    break;
            }
            //
            ms.Seek(0, SeekOrigin.Begin);
            Response.ClearContent();
            Response.ClearHeaders();
            Response.Buffer = true;
            Response.Cache.SetCacheability(HttpCacheability.Private);
            Response.ContentType = "application/vnd.ms-excel";
            Response.AddHeader("Content-Length", ms.Length.ToString(CultureInfo.CurrentCulture));
            Response.AddHeader("Content-Disposition", String.Format("attachment;filename=" + fileName + ".xls"));

            Response.BinaryWrite(ms.ToArray());
            ms.Close();
            HttpContext.Current.ApplicationInstance.CompleteRequest();
            Response.End();

            #endregion
        }
    }
}