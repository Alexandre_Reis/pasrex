﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using Financial.Investidor;
using Financial.Security;
using Financial.Investidor.Enums;
using EntitySpaces.Interfaces;
using System.Drawing;
using Financial.Fundo;
using Financial.Common;
using log4net;
using System.Collections.Generic;
using Financial.Util;
using Financial.Common.Enums;
using DevExpress.Web;
using Financial.Web.Common;

public partial class FiltroReportGraficoRetornoRisco : FiltroReportBasePage {
    // TODO: Traduzir
    const string MSG_ERRO = "Data Inicio deve ser menor que a Data Fim";
    const string MSG_ERRO1 = "Dia Não Útil";    

    private static readonly ILog log = LogManager.GetLogger(typeof(FiltroReportGraficoRetornoRisco));

    new protected void Page_Load(object sender, EventArgs e) {
        this.dropCarteira.GridView.Width = this.dropCarteira.Width;
    }

    protected void EsDSCarteira_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e) {
        CarteiraCollection coll = new CarteiraCollection();
        coll.BuscaCarteirasComAcessoTodas(HttpContext.Current.User.Identity.Name);
        //
        coll.Sort = CarteiraMetadata.ColumnNames.Nome + " ASC";
        //
        e.Collection = coll;
    }

    /// <summary>
    /// Trata os Erros de Relatorio
    /// </summary>
    /// <exception cref="Exception"> - Lança Exceção se Data Inicio > DataFim
    ///                              - Lança Exceção se Data Inicio For um Dia Não Útil
    ///                              - Lança Exceção se Data Final For um Dia Não Útil
    /// </exception>
    private void TrataErros() {

        #region Campos obrigatórios
        List<Control> controles = new List<Control>(new Control[] { this.textDataInicio, this.textDataFim, this.dropTipoRetorno });

        if (base.TestaObrigatorio(controles) != "") {
            throw new Exception("Campos com * são obrigatórios!");
        }
        #endregion

        if (this.dropCarteira.Text.Trim() == "") {
            throw new Exception("Campos com * são obrigatórios!");
        }

        DateTime dataInicio = Convert.ToDateTime(this.textDataInicio.Text);
        DateTime dataFim = Convert.ToDateTime(this.textDataFim.Text);
        DateTime dataAnterior = Calendario.SubtraiDiaUtil(dataInicio, 1);

        if (dataInicio > dataFim) {
            throw new Exception(MSG_ERRO);
        }
        else if (!Calendario.IsDiaUtil(dataInicio, LocalFeriadoFixo.Brasil, TipoFeriado.Brasil)) {
            throw new Exception(MSG_ERRO1 + ": " + dataInicio.ToString("d"));
        }
        else if (!Calendario.IsDiaUtil(dataFim, LocalFeriadoFixo.Brasil, TipoFeriado.Brasil)) {
            throw new Exception(MSG_ERRO1 + ": " + dataFim.ToString("d"));
        }        
    }

    /// <summary>
    /// Gera Relatório em HTML
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnVisualiza_Click(object sender, EventArgs e) {
        this.TrataErros();
        this.SelecionaRelatorio(VisaoRelatorio.visaoHTML);
    }

    /// <summary>
    /// Gera Relatório em PDF
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnPDF_Click(object sender, EventArgs e) {
        this.TrataErros();
        this.SelecionaRelatorio(VisaoRelatorio.visaoPDF);
    }

    /// <summary>
    /// Gera Relatório em Excel
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnExcel_Click(object sender, EventArgs e) {
        this.TrataErros();
        this.SelecionaRelatorio(VisaoRelatorio.visaoExcel);
    }

    /// <summary>
    /// Seleciona o Relatorio a ser executado
    /// </summary>
    /// <param name="visao">PDF, Excel ou HTML</param>
    private void SelecionaRelatorio(string visao) {
        Response.BufferOutput = true;

        List<int> listaIdCarteiras = new List<int>();

        // Monta a Lista de IdCarteiras selecionadas
        if (dropCarteira.Text.Trim() != "") {
            String[] listaCarteiras = dropCarteira.Text.Trim().Split(new Char[] { ',' });
            foreach (string idCarteira in listaCarteiras) {
                listaIdCarteiras.Add(Convert.ToInt32(idCarteira));
            }
            Session["listaIdCarteiras"] = listaIdCarteiras;
        }

        // Salva os Parametros na Session
        Session["dropTipoRetorno"] = dropTipoRetorno.SelectedItem.Value;
        Session["textDataInicio"] = textDataInicio.Text;
        Session["textDataFim"] = textDataFim.Text;


        // Objetos do Tipo List<int>
        Session["listaIdCarteiras"] = listaIdCarteiras;
        //
        //Response.Redirect("~/Relatorios/Captacao/ReportGraficoRetornoRisco.aspx?Visao=" + visao);

        string url = "~/Relatorios/Captacao/ReportGraficoRetornoRisco.aspx?Visao=" + visao;
        string redirectURL = Page.ResolveClientUrl(url);

        string script = "window.location = '" + redirectURL + "';";
        ScriptManager.RegisterStartupScript(this, typeof(Page), "RedirectTo", script, true);
    }
}