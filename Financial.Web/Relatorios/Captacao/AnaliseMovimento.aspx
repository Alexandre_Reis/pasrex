﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="AnaliseMovimento.aspx.cs" Inherits="AnaliseMovimento" %>

<%@ Register Assembly="System.Web, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"
    Namespace="System.Web.UI.HtmlControls" TagPrefix="cc2" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxe" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxwgv" %>

<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxlp" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxcb" %>
<%@ Register Assembly="EntitySpaces.Web" Namespace="EntitySpaces.Web" TagPrefix="cc1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <link href="../../css/forms.css" rel="stylesheet" type="text/css" />

    <script type="text/javascript" language="Javascript" src="../../js/global.js"></script>

    <script type="text/javascript">    
    function btnExportaExcelClick(){
        LoadingPanel.Show();
        callbackExporta.SendCallback();
    }        
    function getElementsByClassName(node, classname) {
        var a = [];
        var re = new RegExp('(^| )'+classname+'( |$)');
        var els = node.getElementsByTagName("*");
        for(var i=0,j=els.length; i<j; i++)
            if(re.test(els[i].className))a.push(els[i]);
        return a;
    }
    </script>

</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="true"
            EnableScriptLocalization="true" />
        <dxcb:ASPxCallback ID="callbackExporta" runat="server" OnCallback="callbackExporta_Callback">
            <ClientSideEvents CallbackComplete="function(s, e) 
        {   

            LoadingPanel.Hide();
            if(e && e.result.length){
                if(e.result.indexOf('redirect') === 0){
                    var newLocation = (e.result.split(':'))[1];
                    var iframeId = 'iframe_download';
                    var iframe = document.getElementById(iframeId);

                    if (!iframe && document.createElement && (iframe =
                        document.createElement('iframe'))) {
               
                        iframe.name = iframe.id = iframeId;
                        iframe.width = 0;
                        iframe.height = 0;
                        iframe.src = 'about:blank';
                        document.body.appendChild(iframe);                                                                    
                    }
                    iframe.src = newLocation;
                   
                } else{
                    alert(e.result);
                }
            }
        }" />
        </dxcb:ASPxCallback>
        <div class="divPanel">
            <table width="100%">
                <tr>
                    <td>
                        <div id="container_small">
                            <div id="header">
                                <asp:Label ID="lblHeader" runat="server" Text="Análise de Captações/Resgates/Saldos" />
                            </div>
                            <div id="mainContent">
                                <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                    <ContentTemplate>
                                        <div class="reportFilter">
                                            <div style="height: 20px">
                                            </div>
                                            <table cellpadding="2" cellspacing="2">
                                                <tr>
                                                    <td class="td_Label">
                                                        <asp:Label ID="labelDataInicio" runat="server" CssClass="labelRequired" Text="Início:" />
                                                    </td>
                                                    <td>
                                                        <dxe:ASPxDateEdit ID="textDataInicio" runat="server" ClientInstanceName="textDataInicio" />
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="labelDataFim" Style="margin-left: 10px;" runat="server" CssClass="labelRequired"
                                                            Text="Fim:" />
                                                    </td>
                                                    <td>
                                                        <dxe:ASPxDateEdit ID="textDataFim" runat="server" ClientInstanceName="textDataFim" />
                                                    </td>
                                                    <td class="td_Label">
                                                        <asp:Label ID="label7" runat="server" CssClass="labelRequired labelFloat" Text="Período:" />
                                                    </td>
                                                    <td>
                                                        <dxe:ASPxComboBox ID="dropTipoPeriodo" runat="server" CssClass="dropDownListCurto_5"
                                                            ClientInstanceName="dropTipoPeriodo">
                                                            <Items>
                                                                <dxe:ListEditItem Value="1" Text="Mês" />
                                                                <dxe:ListEditItem Value="2" Text="Trimestre" />
                                                                <dxe:ListEditItem Value="3" Text="Semestre" />
                                                                <dxe:ListEditItem Value="4" Text="Ano" />
                                                            </Items>
                                                        </dxe:ASPxComboBox>
                                                    </td>
                                                    <td class="td_Label">
                                                        <asp:Label ID="label1" runat="server" CssClass="labelRequired labelFloat" Text="Informação:" />
                                                    </td>
                                                    <td>
                                                        <dxe:ASPxComboBox ID="dropTipoInformacao" runat="server" CssClass="dropDownListCurto"
                                                            ClientInstanceName="dropTipoInformacao">
                                                            <Items>
                                                                <dxe:ListEditItem Value="1" Text="Aportes" />
                                                                <dxe:ListEditItem Value="2" Text="Resgates" />
                                                                <dxe:ListEditItem Value="3" Text="Captações Líquidas" />
                                                                <dxe:ListEditItem Value="10" Text="Saldos na Data" />
                                                            </Items>
                                                        </dxe:ASPxComboBox>
                                                    </td>
                                                    <td class="td_Label">
                                                        <asp:Label ID="label2" runat="server" CssClass="labelRequired labelFloat" Text="Agrupa por:" />
                                                    </td>
                                                    <td>
                                                        <dxe:ASPxComboBox ID="dropTipoAgrupamento" runat="server" CssClass="dropDownListCurto_4"
                                                            ClientInstanceName="dropTipoAgrupamento">                                                            
                                                            <Items>
                                                                <dxe:ListEditItem Value="1" Text="Tipo Cliente" />
                                                                <dxe:ListEditItem Value="2" Text="Officer" />
                                                                <dxe:ListEditItem Value="3" Text="Gestor" />
                                                                <dxe:ListEditItem Value="4" Text="Distribuidor" />
                                                                <dxe:ListEditItem Value="5" Text="Área" />
                                                                <dxe:ListEditItem Value="6" Text="Tipo Produto" />
                                                            </Items>
                                                        </dxe:ASPxComboBox>
                                                    </td>
                                                    <td>
                                                        <div id="progressBar" class="progressBar progressBarLeft" runat="server">
                                                            <asp:UpdateProgress ID="UpdateProgress1" runat="server">
                                                                <ProgressTemplate>
                                                                    <dxe:ASPxImage runat="server" ID="roller" ImageUrl="~/imagens/roller.gif" ClientInstanceName="roller" />
                                                                    <asp:Label ID="lblCarregar" runat="server" Text="Carregando..." />
                                                                </ProgressTemplate>
                                                            </asp:UpdateProgress>
                                                        </div>
                                                        <div class="linkButton linkButtonNoBorder">
                                                            <asp:LinkButton ID="btnRun" runat="server" Font-Overline="false" CssClass="btnRun"
                                                                OnClick="btnRun_Click">
                                                                <asp:Literal ID="Literal4" runat="server" Text="Consultar" /><div>
                                                                </div>
                                                            </asp:LinkButton>
                                                        </div>
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                        <div style="height: 10px">
                                        </div>
                                        <div class="linkButton linkButtonNoBorder">
                                            <asp:LinkButton ID="btnColapsa" runat="server" Font-Overline="false" CssClass="btnAdd"
                                                OnClientClick="gridCadastro.CollapseAll(); return false;" Visible="false">
                                                <asp:Literal ID="Literal1" runat="server" Text="Colapsa todas as Linhas" /><div>
                                                </div>
                                            </asp:LinkButton>
                                            <asp:LinkButton ID="btnExpande" runat="server" Font-Overline="false" CssClass="btnAdd"
                                                OnClientClick=" gridCadastro.ExpandAll(); return false;" Visible="false">
                                                <asp:Literal ID="Literal2" runat="server" Text="Expanda todas as Linhas" /><div>
                                                </div>
                                            </asp:LinkButton>
                                            <asp:LinkButton ID="btnExcel" runat="server" Font-Overline="false" CssClass="btnExcel"
                                                OnClientClick="btnExportaExcelClick()" Visible="false">
                                                <asp:Literal ID="Literal5" runat="server" Text="Gerar Excel" /><div>
                                                </div>
                                            </asp:LinkButton>
                                        </div>
                                        <div style="height: 5px">
                                        </div>
                                        <div class="divDataGrid">
                                            <dxwgv:ASPxGridView ID="gridCadastro" ClientInstanceName="gridCadastro" runat="server"
                                                OnPreRender="Grid_OnPreRender" Visible="false" Width="100%">
                                                <Columns>
                                                    <dxwgv:GridViewDataColumn FieldName="Id" Visible="false" Width="0" VisibleIndex="0" />
                                                    <dxwgv:GridViewDataColumn FieldName="IdCliente" Width="85" Settings-AllowDragDrop="false"
                                                        VisibleIndex="1" />
                                                    <dxwgv:GridViewDataColumn FieldName="Descricao3" Caption="Nome Cliente" Width="200"
                                                        Settings-AllowDragDrop="false" VisibleIndex="2" />
                                                    <%--   Grupos --%>
                                                    <dxwgv:GridViewDataColumn FieldName="Descricao1" Caption="Tipo Cliente" VisibleIndex="1000"
                                                        GroupIndex="1" />
                                                    <dxwgv:GridViewDataColumn FieldName="Descricao2" Caption="Produto" VisibleIndex="1001"
                                                        GroupIndex="2" />
                                                </Columns>
                                                <GroupSummary>
                                                    <dxwgv:ASPxSummaryItem FieldName="IdCliente" SummaryType="Count" DisplayFormat="Núm. Clientes={0:N0}"
                                                        ShowInGroupFooterColumn="IdCliente" />
                                                </GroupSummary>
                                                <TotalSummary>
                                                    <dxwgv:ASPxSummaryItem FieldName="IdCliente" SummaryType="Count" DisplayFormat="Total Clientes={0:N0}" />
                                                </TotalSummary>
                                                <Templates>
                                                    <GroupRowContent>
                                                        <%# Container.GroupText %>
                                                    </GroupRowContent>
                                                </Templates>
                                                <Settings ShowGroupPanel="true" ShowFooter="true" ShowGroupFooter="VisibleIfExpanded" />
                                                <SettingsText GroupContinuedOnNextPage="                      - Continua na Próxima Página"
                                                    EmptyDataRow="0 Registros" />
                                                <SettingsLoadingPanel Text="Carregando" />
                                                <Styles Footer-Font-Bold="true" Cell-Wrap="False" GroupFooter-Font-Bold="true" />
                                                <SettingsPager Mode="ShowAllRecords">
                                                </SettingsPager>
                                            </dxwgv:ASPxGridView>
                                        </div>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </div>
                        </div>
                    </td>
                </tr>
            </table>
        </div>
        <dxwgv:ASPxGridViewExporter ID="gridExport" runat="server" GridViewID="gridCadastro" />
        <dxlp:ASPxLoadingPanel ID="LoadingPanel" runat="server" Text="Exportando arquivo, aguarde..."
            ClientInstanceName="LoadingPanel" Modal="true" />
    </form>

    <script type="text/javascript">  
// UpdateProgress Para Telas de Consultas com Impressao de Relatorio
// UpdateProgress Condicional que só atua no elemento Button - btnRun

var pageManager = Sys.WebForms.PageRequestManager.getInstance();
pageManager.add_beginRequest(BeginRequestHandler);
pageManager.add_endRequest(EndRequestHandler);

function BeginRequestHandler(sender, args) {              

    var elem = args.get_postBackElement();
    
    if (elem.id == '<%= btnRun.ClientID %>') {        
        var linkButtons = getElementsByClassName(document.body,'linkButton');
        for(var i=0; i<linkButtons.length; i++){
            linkButtons[i].style.visibility = 'hidden';
        }  
    }    
        
    // First set associatedUpdatePanelId to non existant control
    // this will force the updateprogress not to try and show itself.               
    var o = $find('<%= UpdateProgress1.ClientID %>');
    o.set_associatedUpdatePanelId('Non_Existant_Control_Id');

    // Then based on the control ID, show the updateprogress
    if (elem.id == '<%= btnRun.ClientID %>') {        
        var updateProgress1 = $get('<%= UpdateProgress1.ClientID %>');  

        sleep(900); // Espera um pouco quando tem a mensagem de campos InvÃ¡lidos

        updateProgress1.style.display = '';
    }       
}

function EndRequestHandler(sender, args) {

var linkButtons = getElementsByClassName(document.body,'linkButton');
        for(var i=0; i<linkButtons.length; i++){
            linkButtons[i].style.visibility = 'visible';
        }

    var updateProgress1 = $get('<%= UpdateProgress1.ClientID %>');        
    updateProgress1.style.display = (updateProgress1.style.display == '') ? 'none' : ''; 
}

function sleep(milliseconds) {
  var start = new Date().getTime();
  for (var i = 0; i < 1e7; i++) {
    if ((new Date().getTime() - start) > milliseconds){
      break;
    }
  }
}
           
</script>

</body>
</html>
