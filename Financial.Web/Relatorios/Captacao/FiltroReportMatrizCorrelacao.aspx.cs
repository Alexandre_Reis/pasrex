﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

using EntitySpaces.Interfaces;

using log4net;
using System.Collections.Generic;

using Financial.Investidor.Enums;
using Financial.Common.Enums;

using DevExpress.Web;
using DevExpress.XtraPrinting;

using Financial.Fundo;
using Financial.Common;
using Financial.Investidor;
using Financial.Security;
using Financial.Util;
using Financial.Web.Common;

public partial class FiltroReportMatrizCorrelacao : FiltroReportBasePage {
    // TODO: Traduzir
    const string MSG_ERRO = "Escolha pelo menos um Fundo e um Índice";
    const string MSG_ERRO1 = "Data Inicio deve ser menor que a Data Fim";
    const string MSG_ERRO2 = "Número de Pontos Insuficiente para Gerar o Gráfico.\nAumente o Período de Consulta";
    const string MSG_ERRO3 = "Escolha pelo menos um Fundo";

    // Numero de Pontos considerado ideal para gerar um gráfico
    const int NUMERO_PONTOS_IDEAL = 15;

    private static readonly ILog log = LogManager.GetLogger(typeof(FiltroReportMatrizCorrelacao));

    new protected void Page_Load(object sender, EventArgs e) {
        this.dropCarteira.GridView.Width = this.dropCarteira.Width;
        this.dropIndice.GridView.Width = this.dropIndice.Width;
    }

    #region DataSource
    protected void EsDSCarteira_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e) {
        CarteiraCollection coll = new CarteiraCollection();
        coll.BuscaCarteirasComAcessoTodas(HttpContext.Current.User.Identity.Name);
        //
        e.Collection = coll;
    }

    protected void EsDSIndice_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e) {
        IndiceCollection coll = new IndiceCollection();
        //
        coll.Query.Select(coll.Query.IdIndice, coll.Query.Descricao)
                  .OrderBy(coll.Query.Descricao.Ascending);

        coll.Query.Load();
        //       
        e.Collection = coll;
    }
    #endregion

    /// <summary>
    /// Gera Relatório em PDF
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnPDF_Click(object sender, EventArgs e) {
        this.TrataErros();
        this.MontaRelatorio();              
        this.SelecionaRelatorio(VisaoRelatorio.visaoPDF);
    }

    /// <summary>
    /// Gera Relatório em Excel
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnExcel_Click(object sender, EventArgs e) {
        this.TrataErros();
        this.MontaRelatorio();
        this.SelecionaRelatorio(VisaoRelatorio.visaoExcel);
    }

    /// <summary>
    /// Trata os Erros de Relatorio
    /// </summary>
    private void TrataErros() {

        #region Campos obrigatários
        List<Control> controles = new List<Control>(new Control[] { this.textDataInicio, this.textDataFim });

        if (base.TestaObrigatorio(controles) != "") {
            throw new Exception("Campos com * são obrigatórios!");
        }
        #endregion

        DateTime dataInicio = Convert.ToDateTime(this.textDataInicio.Text);
        DateTime dataFim = Convert.ToDateTime(this.textDataFim.Text);

        if (dropCarteira.Text.Trim() == "") {
            throw new Exception(MSG_ERRO3);
        }

        if (Convert.ToDateTime(this.textDataInicio.Text) > Convert.ToDateTime(this.textDataFim.Text)) {
            throw new Exception(MSG_ERRO1);
        }
        else {
            // Calcula o Numero de Dias uteis no periodo            
            int numeroDiasUteis = Calendario.NumeroDias(dataInicio, dataFim, LocalFeriadoFixo.Brasil, TipoFeriado.Brasil);
            //
            if (numeroDiasUteis < NUMERO_PONTOS_IDEAL) {
                throw new Exception(MSG_ERRO2);
            }
        }

        DateTime dataAnterior = Calendario.SubtraiDiaUtil(dataInicio, 1);

        if (dropCarteira.Text.Trim() != "") {
            String[] listaCarteiras = dropCarteira.Text.Trim().Split(new Char[] { ',' });
            foreach (string idCarteiraString in listaCarteiras) {
                int idCarteira = Convert.ToInt32(idCarteiraString);

                //Testa se existe cota na data anterior a data inicio e na data fim
                string mensagem = "";
                HistoricoCota historicoCota = new HistoricoCota();
                if (!historicoCota.BuscaValorCota(idCarteira, dataAnterior)) {
                    mensagem = "Não existe cota na data " + dataAnterior.ToShortDateString() + " para a carteira " + idCarteira.ToString();
                }
                else if (!historicoCota.CotaFechamento.HasValue) {
                    mensagem = "Não existe cota na data " + dataAnterior.ToShortDateString() + " para a carteira " + idCarteira.ToString();
                }

                historicoCota = new HistoricoCota();
                if (!historicoCota.BuscaValorCota(idCarteira, dataFim)) {
                    mensagem = "Não existe cota na data " + dataFim.ToShortDateString() + " para a carteira " + idCarteira.ToString();
                }
                else if (!historicoCota.CotaFechamento.HasValue) {
                    mensagem = "Não existe cota na data " + dataFim.ToShortDateString() + " para a carteira " + idCarteira.ToString();
                }

                if (mensagem != "") {
                    throw new Exception(mensagem);
                }
            }
        }

        if (dropIndice.Text.Trim() != "") {
            String[] listaIndices = dropIndice.Text.Trim().Split(new Char[] { ',' });
            foreach (string idIndiceString in listaIndices) {
                int idIndice = Convert.ToInt32(idIndiceString);

                //Testa se existe valor de indice na data anterior Ã  data inicio e na data fim
                string mensagem = "";
                CotacaoIndice cotacaoIndice = new CotacaoIndice();
                try {
                    cotacaoIndice.BuscaCotacaoIndice(idIndice, dataAnterior);
                }
                catch {
                    Indice indice = new Indice();
                    indice.LoadByPrimaryKey((short)idIndice);
                    string descricao = indice.Descricao;
                    mensagem = "Não existe cotação para o índice " + descricao + " na data " + dataAnterior.ToShortDateString();
                }

                cotacaoIndice = new CotacaoIndice();
                try {
                    cotacaoIndice.BuscaCotacaoIndice(idIndice, dataFim);
                }
                catch {
                    Indice indice = new Indice();
                    indice.LoadByPrimaryKey((short)idIndice);
                    string descricao = indice.Descricao;
                    mensagem = "Não existe cotação para o índice " + descricao + " na data " + dataFim.ToShortDateString();
                }

                if (mensagem != "") {
                    throw new Exception(mensagem);
                }
            }
        }
    }

    /// <summary>
    /// Seleciona o Relatorio a Ser Executado
    /// </summary>
    /// <param name="visao">PDF, Excel ou Report</param>
    private void SelecionaRelatorio(string visao) {
        Response.BufferOutput = true;
        //
        // Salva os Parametros na Session
        Session["gridExportMatrizCorrelacao"] = gridExport;
        Response.Redirect("~/Relatorios/Captacao/ReportMatrizCorrelacao.aspx?Visao=" + visao);
    }

    /// <summary>
    /// DataBind dos Dados do Relátorio
    /// </summary>    
    private void MontaRelatorio() {

        DateTime dataInicio = Convert.ToDateTime(textDataInicio.Text);
        DateTime dataFim = Convert.ToDateTime(textDataFim.Text);

        List<int> listaIdCarteiras = new List<int>();
        List<int> listaIdIndices = new List<int>();

        // Monta a Lista de IdCarteiras selecionados
        if (dropCarteira.Text.Trim() != "") {
            String[] listaCarteiras = dropCarteira.Text.Trim().Split(new Char[] { ',' });
            foreach (string idCarteira in listaCarteiras) {
                listaIdCarteiras.Add(Convert.ToInt32(idCarteira));
            }
        }

        // Monta a Lista de IdIndices selecionados
        if (dropIndice.Text.Trim() != "") {
            String[] listaIndices = dropIndice.Text.Trim().Split(new Char[] { ',' });
            foreach (string idIndice in listaIndices) {
                listaIdIndices.Add(Convert.ToInt16(idIndice));
            }
        }

        DataTable dt = new DataTable();

        DataColumn col = new DataColumn();
        col.DataType = Type.GetType("System.String");
        col.ColumnName = "#";
        dt.Columns.Add(col);

        foreach (int idCarteira in listaIdCarteiras) {
            Carteira carteira = new Carteira();
            carteira.LoadByPrimaryKey(idCarteira);
            string apelido = carteira.Apelido;

            if (apelido.Length > 20) {
                apelido = apelido.Substring(0, 20);
            }

            col = new DataColumn();
            col.DataType = Type.GetType("System.String");
            col.AllowDBNull = true;
            col.ColumnName = apelido;
            dt.Columns.Add(col);
        }

        foreach (short idIndice in listaIdIndices) {
            Indice indice = new Indice();
            indice.LoadByPrimaryKey(idIndice);
            string descricao = indice.Descricao;

            if (descricao.Length > 20) {
                descricao = descricao.Substring(0, 20);
            }

            col = new DataColumn();
            col.DataType = Type.GetType("System.String");
            col.ColumnName = descricao;
            dt.Columns.Add(col);
        }

        int total = listaIdCarteiras.Count + listaIdIndices.Count;
        int linha = 0;
        int coluna = 0;

        //Geração do DataTable com o produto cartesiano entre fundos e indices escolhidos

        #region Gera linhas para Carteiras
        foreach (int idCarteira in listaIdCarteiras) {
            linha += 1;
            DataRow dr = dt.NewRow();

            Carteira carteira = new Carteira();
            carteira.LoadByPrimaryKey(idCarteira);
            string apelido = carteira.Apelido;
            if (apelido.Length > 20) {
                apelido = apelido.Substring(0, 20);
            }

            dr["#"] = apelido;
            dt.Rows.Add(dr);

            coluna = 0;
            #region Carteira X Carteira
            foreach (int idCarteira2 in listaIdCarteiras) {
                coluna += 1;

                decimal? correlacao = null;
                if (coluna == linha) {
                    correlacao = 1;
                }
                //else if (coluna > linha) {
                else {
                    CalculoMedida calculoMedida = new CalculoMedida();

                    calculoMedida.SetIdCarteira(idCarteira);
                    List<decimal> lista1 = calculoMedida.RetornaListaRetornos(1, dataInicio, dataFim);
                    
                    calculoMedida.SetIdCarteira(idCarteira2);
                    List<decimal> lista2 = calculoMedida.RetornaListaRetornos(1, dataInicio, dataFim);

                    try {
                        correlacao = Math.Round(calculoMedida.CalculaCorrelacao(lista1, lista2) / 100M, 4);
                    }
                    catch (DivideByZeroException ex1) {
                        correlacao = null;
                    }
                }

                carteira = new Carteira();
                carteira.LoadByPrimaryKey(idCarteira2);
                string apelido2 = carteira.Apelido;
                if (apelido2.Length > 20) {
                    apelido2 = apelido2.Substring(0, 20);
                }

                dr[apelido2] = correlacao;
            }
            #endregion

            #region Carteira X Indice
            foreach (short idIndice in listaIdIndices) {
                coluna += 1;

                decimal? correlacao = null;
                if (coluna == linha) {
                    correlacao = 1;
                }
                //else if (coluna > linha) {
                else {
                    CalculoMedida calculoMedida = new CalculoMedida();

                    calculoMedida.SetIdCarteira(idCarteira);
                    List<decimal> lista1 = calculoMedida.RetornaListaRetornos(1, dataInicio, dataFim);
                    
                    calculoMedida.SetIdIndice(idIndice);
                    List<decimal> lista2 = calculoMedida.RetornaListaRetornosIndice(1, dataInicio, dataFim);

                    try {
                        correlacao = Math.Round(calculoMedida.CalculaCorrelacao(lista1, lista2) / 100M, 4);
                    }
                    catch (DivideByZeroException ex1) {
                        correlacao = null;
                    }
                }

                Indice indice = new Indice();
                indice.LoadByPrimaryKey(idIndice);
                string descricao = indice.Descricao;
                if (descricao.Length > 20) {
                    descricao = descricao.Substring(0, 20);
                }

                dr[descricao] = correlacao;
            }
            #endregion
        }
        #endregion

        #region Gera linhas para Indices
        foreach (short idIndice in listaIdIndices) {
            linha += 1;
            DataRow dr = dt.NewRow();

            Indice indice = new Indice();
            indice.LoadByPrimaryKey(idIndice);
            string descricao = indice.Descricao;
            if (descricao.Length > 20) {
                descricao = descricao.Substring(0, 20);
            }

            dr["#"] = descricao;
            dt.Rows.Add(dr);

            coluna = 0;
            #region Indice X Carteira
            foreach (int idCarteira in listaIdCarteiras) {
                coluna += 1;

                decimal? correlacao = null;
                if (coluna == linha) {
                    correlacao = 1;
                }
                //else if (coluna > linha) {
                else {
                    CalculoMedida calculoMedida = new CalculoMedida();
                    
                    calculoMedida.SetIdIndice(idIndice);                    
                    List<decimal> lista1 = calculoMedida.RetornaListaRetornosIndice(1, dataInicio, dataFim);
                                       
                    calculoMedida.SetIdCarteira(idCarteira);
                    List<decimal> lista2 = calculoMedida.RetornaListaRetornos(1, dataInicio, dataFim);

                    try {
                        correlacao = Math.Round(calculoMedida.CalculaCorrelacao(lista1, lista2) / 100M, 4);
                    }
                    catch (DivideByZeroException ex1) {
                        correlacao = null;
                    }
                }

                Carteira carteira = new Carteira();
                carteira.LoadByPrimaryKey(idCarteira);
                string apelido = carteira.Apelido;
                if (apelido.Length > 20) {
                    apelido = apelido.Substring(0, 20);
                }

                dr[apelido] = correlacao;
            }
            #endregion

            #region Indice X Indice
            foreach (short idIndice2 in listaIdIndices) {
                coluna += 1;

                decimal? correlacao = null;
                if (coluna == linha) {
                    correlacao = 1;
                }
                // else if (coluna > linha) {
                else {
                    CalculoMedida calculoMedida = new CalculoMedida();
                    
                    calculoMedida.SetIdIndice(idIndice);
                    List<decimal> lista1 = calculoMedida.RetornaListaRetornosIndice(1, dataInicio, dataFim);
                    
                    calculoMedida.SetIdIndice(idIndice2);
                    List<decimal> lista2 = calculoMedida.RetornaListaRetornosIndice(1, dataInicio, dataFim);

                    try {
                        correlacao = Math.Round(calculoMedida.CalculaCorrelacao(lista1, lista2) / 100M, 4);
                    }
                    catch (DivideByZeroException ex1) {
                        correlacao = null;
                    }
                }

                indice = new Indice();
                indice.LoadByPrimaryKey(idIndice2);
                descricao = indice.Descricao;
                if (descricao.Length > 20) {
                    descricao = descricao.Substring(0, 20);
                }

                dr[descricao] = correlacao;
            }
            #endregion
        }
        #endregion

        //gridExport.Landscape = false;
        gridExport.ReportHeader = "Matriz de Correlação - De: " + textDataInicio.Text + " a " + textDataFim.Text;

        gridMain.DataSource = dt;
        gridMain.DataBind();
    }
}