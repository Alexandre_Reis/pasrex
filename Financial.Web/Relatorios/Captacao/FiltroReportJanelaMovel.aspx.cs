﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using Financial.Investidor;
using Financial.Security;
using Financial.Investidor.Enums;
using EntitySpaces.Interfaces;
using System.Drawing;
using Financial.Fundo;
using Financial.Common;
using log4net;
using System.Collections.Generic;
using DevExpress.Web;
using Financial.Util;
using Financial.Common.Enums;
using Financial.Web.Common;

public partial class FiltroReportJanelaMovel : FiltroReportBasePage {
    // TODO: Traduzir
    const string MSG_ERRO = "Escolha pelo menos um Fundo ou Índice";
    const string MSG_ERRO1 = "Data Inicio deve ser menor que a Data Fim";
    const string MSG_ERRO2 = "Número de Pontos Insuficiente para gerar um gráfico.\nAumente o período de Consulta.";

    // Numero de Pontos considerado ideal para gerar um gráfico
    const int NUMERO_PONTOS_IDEAL = 15;

    private static readonly ILog log = LogManager.GetLogger(typeof(FiltroReportJanelaMovel));

    private enum TipoRelatorio {
        Volatilidade = 1,
        RetornoEfetivo = 2,
        TrackingError = 3
    }

    new protected void Page_Load(object sender, EventArgs e) {
        this.dropCarteira.GridView.Width = this.dropCarteira.Width;
        this.dropIndice.GridView.Width = this.dropIndice.Width;
    }

    #region DataSource
    protected void EsDSCarteira_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e) {
        CarteiraCollection coll = new CarteiraCollection();
        coll.BuscaCarteirasComAcessoTodas(HttpContext.Current.User.Identity.Name);
        //
        e.Collection = coll;
    }

    protected void EsDSIndice_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e) {
        IndiceCollection coll = new IndiceCollection();
        //
        coll.Query.Select(coll.Query.IdIndice, coll.Query.Descricao)
                  .OrderBy(coll.Query.Descricao.Ascending);

        coll.Query.Load();
        //       
        e.Collection = coll;
    }
    #endregion

    /// <summary>
    /// Trata os Erros de Relatorio
    /// </summary>
    private void TrataErros() {

        #region Campos obrigatórios
        List<Control> controles = new List<Control>(new Control[] { this.textDataInicio, this.textDataFim, this.textJanelaMovel });

        if (base.TestaObrigatorio(controles) != "") {
            throw new Exception("Campos com * são obrigatórios!");
        }
        #endregion

        TipoRelatorio tipo = (TipoRelatorio)Enum.Parse(typeof(TipoRelatorio), this.dropTipoRelatorio.Value.ToString());

        if (this.dropCarteira.Text.Trim() == "" && this.dropIndice.Text.Trim() == "") {
            throw new Exception(MSG_ERRO);
        }

        if (Convert.ToDateTime(this.textDataInicio.Text) > Convert.ToDateTime(this.textDataFim.Text)) {
            throw new Exception(MSG_ERRO1);
        }
        else {
            // Calcula o Numero de Dias uteis no periodo
            DateTime dataInicio = Convert.ToDateTime(this.textDataInicio.Text);
            DateTime dataFim = Convert.ToDateTime(this.textDataFim.Text);
            int numeroDiasUteis = Calendario.NumeroDias(dataInicio, dataFim, LocalFeriadoFixo.Brasil, TipoFeriado.Brasil);
            //
            if (numeroDiasUteis < NUMERO_PONTOS_IDEAL) {
                throw new Exception(MSG_ERRO2);
            }
        }
    }

    /// <summary>
    /// Gera Relatório em HTML
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnVisualiza_Click(object sender, EventArgs e) {
        this.TrataErros();
        this.SelecionaRelatorio(VisaoRelatorio.visaoHTML);
    }

    /// <summary>
    /// Gera Relatório em PDF
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnPDF_Click(object sender, EventArgs e) {
        this.TrataErros();
        this.SelecionaRelatorio(VisaoRelatorio.visaoPDF);
    }

    /// <summary>
    /// Gera Relatório em Excel
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnExcel_Click(object sender, EventArgs e) {
        this.TrataErros();
        this.SelecionaRelatorio(VisaoRelatorio.visaoExcel);
    }


    /// <summary>
    /// Seleciona o Relatorio a ser executado
    /// </summary>
    /// <param name="visao">PDF, Excel ou HTML</param>
    private void SelecionaRelatorio(string visao) {
        Response.BufferOutput = true;

        List<int> listaIdCarteiras = new List<int>();
        List<int> listaIdIndices = new List<int>();

        // Monta a Lista de IdCarteiras selecionados
        if (dropCarteira.Text.Trim() != "") {
            String[] listaCarteiras = dropCarteira.Text.Trim().Split(new Char[] { ',' });
            foreach (string idCarteira in listaCarteiras) {
                listaIdCarteiras.Add(Convert.ToInt32(idCarteira));
            }
            Session["listaIdCarteiras"] = listaIdCarteiras;
        }

        // Monta a Lista de IdIndices selecionados
        if (dropIndice.Text.Trim() != "") {
            String[] listaIndices = dropIndice.Text.Trim().Split(new Char[] { ',' });
            foreach (string idIndice in listaIndices) {
                listaIdIndices.Add(Convert.ToInt32(idIndice));
            }
            Session["listaIdIndices"] = listaIdIndices;
        }
        else {
            Session["listaIdIndices"] = "";
        }

        // Salva os Parametros na Session
        Session["textDataInicio"] = textDataInicio.Text;
        Session["textDataFim"] = textDataFim.Text;
        // Int
        Session["textJanelaMovel"] = Convert.ToInt32(this.textJanelaMovel.Text);
        // Salva como TipoRelatorio        
        //Session["dropTipoRelatorio"] = (TipoRelatorio)Enum.Parse(typeof(TipoRelatorio), this.dropTipoRelatorio.Text.Replace(" ", ""));
        Session["dropTipoRelatorio"] = (TipoRelatorio)Enum.Parse(typeof(TipoRelatorio), this.dropTipoRelatorio.Value.ToString());

        // Objetos do Tipo List<int>
        Session["listaIdCarteiras"] = listaIdCarteiras;
        Session["listaIdIndices"] = listaIdIndices;
        //
        Response.Redirect("~/Relatorios/Captacao/ReportJanelaMovel.aspx?Visao=" + visao);
    }
}