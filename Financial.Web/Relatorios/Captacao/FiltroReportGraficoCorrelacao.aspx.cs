﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using Financial.Investidor;
using Financial.Security;
using Financial.Investidor.Enums;
using EntitySpaces.Interfaces;
using System.Drawing;
using Financial.Fundo;
using Financial.Common;
using log4net;
using System.Collections.Generic;
using DevExpress.Web;
using Financial.Util;
using Financial.Common.Enums;
using Financial.Web.Common;

public partial class FiltroReportGraficoCorrelacao : FiltroReportBasePage {
    // TODO: Traduzir
    const string MSG_ERRO = "Escolha pelo menos um Fundo ou Índice";
    const string MSG_ERRO1 = "Data Inicio deve ser menor que a Data Fim";
    const string MSG_ERRO2 = "Número de Pontos Insuficiente para gerar um gráfico.\nAumente o período de Consulta.";

    // Numero de Pontos considerado ideal para gerar um gráfico
    const int NUMERO_PONTOS_IDEAL = 15;

    private static readonly ILog log = LogManager.GetLogger(typeof(FiltroReportGraficoCorrelacao));

    new protected void Page_Load(object sender, EventArgs e) {
        this.dropCarteira.GridView.Width = this.dropCarteira.Width;
        this.dropIndice.GridView.Width = this.dropIndice.Width;
    }

    #region DataSource
    protected void EsDSCarteira_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e) {
        CarteiraCollection coll = new CarteiraCollection();
        coll.BuscaCarteirasComAcessoTodas(HttpContext.Current.User.Identity.Name);
        //
        e.Collection = coll;
    }

    protected void EsDSIndice_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e) {
        IndiceCollection coll = new IndiceCollection();
        //
        coll.Query.Select(coll.Query.IdIndice, coll.Query.Descricao)
                  .OrderBy(coll.Query.Descricao.Ascending);

        coll.Query.Load();
        //       
        e.Collection = coll;
    }
    #endregion

    /// <summary>
    /// Gera Relatório em HTML
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnVisualiza_Click(object sender, EventArgs e) {
        this.TrataErros();
        this.SelecionaRelatorio(VisaoRelatorio.visaoHTML);
    }

    /// <summary>
    /// Gera Relatório em PDF
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnPDF_Click(object sender, EventArgs e) {
        this.TrataErros();
        this.SelecionaRelatorio(VisaoRelatorio.visaoPDF);
    }

    /// <summary>
    /// Gera Relatório em Excel
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnExcel_Click(object sender, EventArgs e) {
        this.TrataErros();
        this.SelecionaRelatorio(VisaoRelatorio.visaoExcel);
    }

    /// <summary>
    /// Trata os Erros de Relatorio
    /// </summary>
    private void TrataErros() {

        #region Campos obrigatórios
        List<Control> controles = new List<Control>(new Control[] { this.textDataInicio, this.textDataFim, this.textJanelaMovel });

        if (base.TestaObrigatorio(controles) != "") {
            throw new Exception("Campos com * são obrigatórios!");
        }
        #endregion

        DateTime dataInicio = Convert.ToDateTime(this.textDataInicio.Text);
        DateTime dataFim = Convert.ToDateTime(this.textDataFim.Text);

        if (this.dropCarteira.Text.Trim() == "" && this.dropIndice.Text.Trim() == "") {
             throw new Exception(MSG_ERRO);
        }

        if (Convert.ToDateTime(this.textDataInicio.Text) > Convert.ToDateTime(this.textDataFim.Text)) {
            throw new Exception(MSG_ERRO1);
        }
        else {
            // Calcula o Numero de Dias uteis no periodo            
            int numeroDiasUteis = Calendario.NumeroDias(dataInicio, dataFim, LocalFeriadoFixo.Brasil, TipoFeriado.Brasil);
            //
            if (numeroDiasUteis < NUMERO_PONTOS_IDEAL) {
                throw new Exception(MSG_ERRO2);
            }
        }

        DateTime dataAnterior = Calendario.SubtraiDiaUtil(dataInicio, 1);

        if (this.dropCarteira.Text.Trim() != "") {
            String[] listaCarteiras = this.dropCarteira.Text.Trim().Split(new Char[] { ',' });
            foreach (string idCarteiraString in listaCarteiras) {
                int idCarteira = Convert.ToInt32(idCarteiraString);

                //Testa se existe cota na data anterior à data inicio e na data fim
                string mensagem = "";
                HistoricoCota historicoCota = new HistoricoCota();
                if (!historicoCota.BuscaValorCota(idCarteira, dataAnterior)) {
                    mensagem = "Não existe cota na data " + dataAnterior.ToShortDateString() + " para a carteira " + idCarteira.ToString();
                }
                else if (!historicoCota.CotaFechamento.HasValue) {
                    mensagem = "Não existe cota na data " + dataAnterior.ToShortDateString() + " para a carteira " + idCarteira.ToString();
                }

                historicoCota = new HistoricoCota();
                if (!historicoCota.BuscaValorCota(idCarteira, dataFim)) {
                    mensagem = "Não existe cota na data " + dataFim.ToShortDateString() + " para a carteira " + idCarteira.ToString();
                }
                else if (!historicoCota.CotaFechamento.HasValue) {
                    mensagem = "Não existe cota na data " + dataFim.ToShortDateString() + " para a carteira " + idCarteira.ToString();
                }

                if (mensagem != "") {
                    throw new Exception(mensagem);
                }
            }
        }

        if (this.dropIndice.Text.Trim() != "") {
            String[] listaIndices = dropIndice.Text.Trim().Split(new Char[] { ',' });
            foreach (string idIndiceString in listaIndices) {
                int idIndice = Convert.ToInt32(idIndiceString);

                //Testa se existe valor de indice na data anterior à data inicio e na data fim
                string mensagem = "";
                CotacaoIndice cotacaoIndice = new CotacaoIndice();
                try {
                    cotacaoIndice.BuscaCotacaoIndice(idIndice, dataAnterior);
                }
                catch {
                    Indice indice = new Indice();
                    indice.LoadByPrimaryKey((short)idIndice);
                    string descricao = indice.Descricao;
                    mensagem = "Não existe cotação para o índice " + descricao + " na data " + dataAnterior.ToShortDateString();
                }

                cotacaoIndice = new CotacaoIndice();
                try {
                    cotacaoIndice.BuscaCotacaoIndice(idIndice, dataFim);
                }
                catch {
                    Indice indice = new Indice();
                    indice.LoadByPrimaryKey((short)idIndice);
                    string descricao = indice.Descricao;
                    mensagem = "Não existe cotação para o índice " + descricao + " na data " + dataFim.ToShortDateString();
                }

                if (mensagem != "") {
                    throw new Exception(mensagem);
                }
            }
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <returns></returns>
    private List<string> GeraListaElementos() {
        List<string> listaElementos = new List<string>();

        List<int> listaIdCarteiras = new List<int>();
        List<int> listaIdIndices = new List<int>();

        // Monta a Lista de IdCarteiras selecionados
        if (this.dropCarteira.Text.Trim() != "") {
            String[] listaCarteiras = this.dropCarteira.Text.Trim().Split(new Char[] { ',' });
            foreach (string idCarteira in listaCarteiras) {
                listaIdCarteiras.Add(Convert.ToInt32(idCarteira));
            }
        }

        // Monta a Lista de IdIndices selecionados
        if (this.dropIndice.Text.Trim() != "") {
            String[] listaIndices = this.dropIndice.Text.Trim().Split(new Char[] { ',' });
            foreach (string idIndice in listaIndices) {
                listaIdIndices.Add(Convert.ToInt32(idIndice));
            }
        }

        int linha = 0;
        int coluna = 0;

        #region Gera linhas para Carteiras
        foreach (int idCarteira in listaIdCarteiras) {
            linha += 1;

            coluna = 0;
            foreach (int idCarteira2 in listaIdCarteiras) {
                coluna += 1;

                if (coluna > linha) {
                    listaElementos.Add(idCarteira.ToString() + "|1|" + idCarteira2.ToString() + "|1");
                }
            }

            foreach (short idIndice in listaIdIndices) {
                coluna += 1;

                if (coluna > linha) {
                    listaElementos.Add(idCarteira.ToString() + "|1|" + idIndice.ToString() + "|2");
                }
            }
        }
        #endregion

        #region Gera linhas para Indices
        foreach (short idIndice in listaIdIndices) {
            linha += 1;

            coluna = 0;
            foreach (int idCarteira in listaIdCarteiras) {
                coluna += 1;

                if (coluna > linha) {
                    listaElementos.Add(idIndice.ToString() + "|2|" + idCarteira.ToString() + "|1");
                }
            }

            foreach (short idIndice2 in listaIdIndices) {
                coluna += 1;

                if (coluna > linha) {
                    listaElementos.Add(idIndice.ToString() + "|2|" + idIndice2.ToString() + "|2");
                }
            }
        }
        #endregion

        return listaElementos;
    }

    /// <summary>
    /// Seleciona o Relatorio a ser executado
    /// </summary>
    /// <param name="visao">PDF, Excel ou HTML</param>
    private void SelecionaRelatorio(string visao) {
        Response.BufferOutput = true;

        List<string> listaElementos = GeraListaElementos();

        // Salva os Parametros na Session
        Session["textDataInicio"] = textDataInicio.Text;
        Session["textDataFim"] = textDataFim.Text;
        Session["textJanelaMovel"] = Convert.ToInt32(this.textJanelaMovel.Text);
        Session["listaElementos"] = listaElementos;

        //Response.Redirect("~/Relatorios/Captacao/ReportGraficoCorrelacao.aspx?Visao=" + visao);

        string url = "~/Relatorios/Captacao/ReportGraficoCorrelacao.aspx?Visao=" + visao;
        string redirectURL = Page.ResolveClientUrl(url);

        string script = "window.location = '" + redirectURL + "';";
        ScriptManager.RegisterStartupScript(this, typeof(Page), "RedirectTo", script, true);
    }
}