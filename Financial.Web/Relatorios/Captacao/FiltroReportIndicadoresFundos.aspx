﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="FiltroReportIndicadoresFundos.aspx.cs"
    Inherits="FiltroReportIndicadoresFundos" %>

<%@ Register Assembly="EntitySpaces.Web" Namespace="EntitySpaces.Web" TagPrefix="cc1" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxe" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxcb" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dx" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <link href="~/css/forms.css" rel="stylesheet" type="text/css" />

    <script type="text/javascript">
    // Select All dos AspGridLookup
    function OnAllCheckedChanged(s, e, grid) {
        if (s.GetChecked()) {
            grid.SelectRows();
        }
        else {
            grid.UnselectRows();
        }    
    }
    
    function OnAllCheckedChanged1(s, e, grid) {
        debugger;
        if (s.GetChecked()) {
            grid.GetGridView().SelectRows();
        }
        else {
            grid.GetGridView().UnselectRows();
        }    
    }
    
        function AddSelectedItems() {
            lbChoosen.BeginUpdate();
            gridBuscaFundo.GetSelectedFieldValues('IdCarteira;Nome', function(selectedValues){
                for(var count=0; count < selectedValues.length; count++) {
                    var selectedItem = selectedValues[count];
                    var codigoFundo = selectedItem[0];
                    var nomeFantasia = selectedItem[1];
                    
                    var duplicate = false;
                    var countCheckboxItems = lbChoosen.GetItemCount();
                    for(var i = 0; i < countCheckboxItems; i++) {
                        if(codigoFundo == lbChoosen.GetItem(i).value){
                            duplicate = true;
                            break;
                        }
                    }
                    
                    if(!duplicate){
                        lbChoosen.AddItem(codigoFundo + ' ' + nomeFantasia, codigoFundo);
                    }
                    
                }
                lbChoosen.EndUpdate();    
                
                gridBuscaFundo.UnselectRows();
            });
        }
        
        function RemoveSelectedItems() {
            
            lbChoosen.BeginUpdate();
            var items = lbChoosen.GetSelectedItems();
            for(var i = items.length - 1; i >= 0; i = i - 1) {
                lbChoosen.RemoveItem(items[i].index);
            }
            lbChoosen.EndUpdate();
        }
        
        function RemoveAllItems() {
            lbChoosen.ClearItems();
        }

    </script>

</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="true"
            EnableScriptLocalization="true" />
        <div class="divPanel">
            <table width="100%">
                <tr>
                    <td>
                        <div id="container_small">
                            <div id="header">
                                <asp:Label ID="lblHeader" runat="server" Text="Quadro de Indicadores" />
                            </div>
                            <div id="mainContentSpace">
                                <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                    <ContentTemplate>
                                        <div class="reportFilter" style="margin-left: 10px;">
                                            
                                            <table>
                                                <tr>
                                                    <td>
                                                        <div id="gridBuscaFundoWrapper" style="margin-top: 0px; " class="divDataGrid">
                                                            <table style="width: 700px;">
                                                                <tr>
                                                                    <td style="width: 550px;">
                                                                        <p>
                                                                            Selecione os fundos utilizando a lista abaixo:</p>
                                                                        <dx:ASPxGridView ID="gridBuscaFundo" DataSourceID="EsDSCarteira" runat="server" EnableCallBacks="true"
                                                                            ClientInstanceName="gridBuscaFundo" KeyFieldName="IdCarteira" Width="100%" OnCustomDataCallback="gridBuscaFundo_CustomDataCallback"
                                                                            OnCustomCallback="gridBuscaFundo_CustomCallback" OnHtmlRowCreated="gridBuscaFundo_HtmlRowCreated">
                                                                            <Columns>
                                                                                <dx:GridViewCommandColumn ShowSelectCheckbox="True" VisibleIndex="0" Width="5%" ButtonType="Image" ShowClearFilterButton="True">
                                                                                    <HeaderTemplate>
                                                                                        <dxe:ASPxCheckBox ID="cbAll" ClientInstanceName="cbAll" runat="server" ToolTip="Seleciona todos os CheckBoxs de todas as páginas" BackColor="white" ClientSideEvents-CheckedChanged="function(s, e) {OnAllCheckedChanged(s, e, gridBuscaFundo);}" OnInit="CheckBoxSelectAll"/>
                                                                                    </HeaderTemplate>
                                                                                </dx:GridViewCommandColumn>
                                                                                <dx:GridViewDataTextColumn FieldName="Descricao" Caption="Categoria" VisibleIndex="1" Width="20%" Settings-AutoFilterCondition="Contains" />
                                                                                <dx:GridViewDataTextColumn FieldName="IdCarteira" ReadOnly="True" VisibleIndex="2"
                                                                                    Width="7%" CellStyle-HorizontalAlign="left">
                                                                                    <CellStyle HorizontalAlign="Left">
                                                                                    </CellStyle>
                                                                                </dx:GridViewDataTextColumn>                                                                                
                                                                                <dx:GridViewDataTextColumn FieldName="Cpfcnpj" Caption="CNPJ" VisibleIndex="3" Width="10%" />
                                                                                <dx:GridViewDataTextColumn FieldName="Nome" Caption="Nome" VisibleIndex="4" Width="40%" />
                                                                            </Columns>
                                                                            <Settings ShowFilterRow="true" ShowTitlePanel="True" />
                                                                            <SettingsBehavior ColumnResizeMode="Disabled" />
                                                                            <ClientSideEvents RowDblClick="function(s, e) {
                                                    gridEvento.GetValuesOnCustomCallback(e.visibleIndex,OnGetDataEvento);}" Init="function(s, e) {e.cancel = true;}" />
                                                                            <SettingsDetail ShowDetailButtons="False" />
                                                                            <Styles AlternatingRow-Enabled="True">
                                                                                <Header ImageSpacing="5px" SortingImageSpacing="5px" />
                                                                            </Styles>
                                                                            <Images>
                                                                                <PopupEditFormWindowClose Height="17px" Width="17px" />
                                                                            </Images>
                                                                            <SettingsText EmptyDataRow="0 Registros" Title="Pesquisa Fundos" />
                                                                            <ClientSideEvents RowDblClick="function(s, e) {
                                                            gridBuscaFundo.GetValuesOnCustomCallback(e.visibleIndex, Financial.Carteira.UpdateFieldsFundo)
                                                        }" />
                                                                            <SettingsCommandButton>
                                                                                <ClearFilterButton Image-Url="../../imagens/funnel--minus.png"/>
                                                                            </SettingsCommandButton>
                                                                        </dx:ASPxGridView>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </div>
                                                    </td>
                                                </tr>
                                            </table>
                                            <table style="margin-top: 20px;">
                                                <tr style="height: 40px;">
                                                    <td style="text-align: right">
                                                        Indicadores Qualitativos:</td>
                                                    <td>
                                                        <dx:ASPxGridLookup ID="dropIndicadoresQualitativos" ClientInstanceName="dropIndicadoresQualitativos"
                                                            runat="server" SelectionMode="Multiple" DataSourceID="EsDSIndicadoresQualitativos"
                                                            KeyFieldName="Value" AllowUserInput="false" Width="400px" TextFormatString="{0}"
                                                            MultiTextSeparator=", " Font-Size="11px" IncrementalFilteringMode="StartsWith"
                                                            AutoGenerateColumns="False">
                                                            <Columns>
                                                                <dx:GridViewCommandColumn ShowSelectCheckbox="True" Width="8%" VisibleIndex="0" ButtonType="Image" ShowClearFilterButton="True">
                                                                    <HeaderStyle HorizontalAlign="Center"/>
                                                                    <HeaderTemplate>
                                                                        <dxe:ASPxCheckBox ID="cbAll1" ClientInstanceName="cbAll1" runat="server" ToolTip="Seleciona todos" OnInit="CheckBoxSelectAll" BackColor="white" ClientSideEvents-CheckedChanged="function(s, e) {
                                                                                                                                        OnAllCheckedChanged1(s, e, dropIndicadoresQualitativos);
                                                                                                                                    }"/>
                                                                    </HeaderTemplate>
                                                                </dx:GridViewCommandColumn>
                                                                <dx:GridViewDataColumn CellStyle-Wrap="False" FieldName="Value" Caption="Indicador"
                                                                    CellStyle-Font-Size="X-Small" VisibleIndex="1" />
                                                                <dx:GridViewDataColumn FieldName="Id" Visible="false" />
                                                            </Columns>
                                                            <GridViewProperties>
                                                                <SettingsBehavior AllowFocusedRow="True" AllowSelectByRowClick="True" />
                                                                <SettingsPager Mode="ShowAllRecords">
                                                                </SettingsPager>
                                                                <Settings VerticalScrollBarMode="Visible" VerticalScrollBarStyle="Standard" />
                                                                <SettingsCommandButton>
                                                                    <ClearFilterButton Image-Url="../imagens/funnel--minus.png">
                                                                        <Image Url="../imagens/funnel--minus.png">
                                                                        </Image>
                                                                    </ClearFilterButton>
                                                                </SettingsCommandButton>
                                                            </GridViewProperties>
                                                        </dx:ASPxGridLookup>
                                                    </td>
                                                </tr>
                                                <tr style="height: 40px;">
                                                    <td style="text-align: right">
                                                        Indicadores Quantitativos:
                                                    </td>
                                                    <td>
                                                        <dx:ASPxGridLookup ID="dropIndicadoresQuantitativosPeriodo" ClientInstanceName="dropIndicadoresQuantitativosPeriodo"
                                                            runat="server" SelectionMode="Multiple" DataSourceID="EsDSIndicadoresQuantitativosPeriodo"
                                                            KeyFieldName="Value" AllowUserInput="false" Width="400px" TextFormatString="{0}"
                                                            MultiTextSeparator=", " Font-Size="11px" IncrementalFilteringMode="StartsWith"
                                                            AutoGenerateColumns="False">
                                                            <Columns>
                                                                <dx:GridViewCommandColumn ShowSelectCheckbox="True" Width="4%" VisibleIndex="0" ButtonType="Image" ShowClearFilterButton="True">
                                                                    <HeaderStyle HorizontalAlign="Center"/>
                                                                    <HeaderTemplate>
                                                                        <dxe:ASPxCheckBox ID="cbAll2" ClientInstanceName="cbAll2" runat="server" ToolTip="Seleciona todos" OnInit="CheckBoxSelectAll" BackColor="white" ClientSideEvents-CheckedChanged="function(s, e) {
                                                                                                                                        OnAllCheckedChanged1(s, e, dropIndicadoresQuantitativosPeriodo);
                                                                                                                                    }"/>
                                                                    </HeaderTemplate>
                                                                </dx:GridViewCommandColumn>
                                                                <dx:GridViewDataColumn CellStyle-Wrap="False" FieldName="Value" Caption="Indicador"
                                                                    CellStyle-Font-Size="X-Small" VisibleIndex="1" Width="18%" />
                                                                <dx:GridViewDataColumn CellStyle-Wrap="False" FieldName="Descricao" Caption="Observação"
                                                                    CellStyle-Font-Size="X-Small" VisibleIndex="2" />
                                                                <dx:GridViewDataColumn FieldName="Id" Visible="false" />
                                                            </Columns>
                                                            <GridViewProperties>
                                                                <SettingsBehavior AllowFocusedRow="True" AllowSelectByRowClick="True" />
                                                                <SettingsPager Mode="ShowAllRecords">
                                                                </SettingsPager>
                                                                <Settings VerticalScrollBarMode="Visible" VerticalScrollBarStyle="Standard" />
                                                                <SettingsCommandButton>
                                                                    <ClearFilterButton Image-Url="../imagens/funnel--minus.png">
                                                                        <Image Url="../imagens/funnel--minus.png">
                                                                        </Image>
                                                                    </ClearFilterButton>
                                                                </SettingsCommandButton>
                                                            </GridViewProperties>
                                                        </dx:ASPxGridLookup>
                                        </div>
                                        </td>
                                        <td>
                                            <dxe:ASPxCheckBox runat="server" ID="chkRetornoBenchmark" Text="Mostra retorno benchmark"
                                                ValueChecked="true">
                                            </dxe:ASPxCheckBox>
                                        </td>
                                        </tr></table>
                                        <table>
                                            <tr style="height: 40px;">
                                                <td style="width: 128px; text-align: right;">
                                                    Diferença Retornos:
                                                </td>
                                                <td>
                                                    <dxe:ASPxComboBox Width="243px" ID="dropExcessoRetorno" runat="server" CssClass="dropDownList"
                                                        ClientInstanceName="dropExcessoRetorno">
                                                        <Items>
                                                            <dxe:ListEditItem Selected="true" Value="1" Text="Por retornos anualizados" />
                                                            <dxe:ListEditItem Value="2" Text="Por média das diferenças" />
                                                            <dxe:ListEditItem Value="3" Text="Por diferença das médias" />
                                                        </Items>
                                                    </dxe:ASPxComboBox>
                                                </td>
                                            </tr>
                                        </table>
                                        <table>
                                            <tr style="height: 40px;">
                                                <td style="width: 128px;" class="td_Label">
                                                    <asp:Label ID="labelDataInicio" runat="server" CssClass="labelRequired" Text="Início:"></asp:Label>
                                                </td>
                                                <td>
                                                    <table border="0" cellpadding="0" cellspacing="0">
                                                        <tr>
                                                            <td>
                                                                <dxe:ASPxDateEdit ID="textDataInicio" runat="server" ClientInstanceName="textDataInicio" />
                                                            </td>
                                                            <td class="labelCurto">
                                                                <asp:Label ID="labelDataFim" runat="server" Style="margin-left: 10px;" CssClass="labelRequired"
                                                                    Text="Fim: "></asp:Label>
                                                            </td>
                                                            <td>
                                                                <dxe:ASPxDateEdit ID="textDataFim" runat="server" ClientInstanceName="textDataFim" />
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                        <div id="reportLinkButton" style="margin-left: 125px;" class="linkButton linkButtonNoBorder">
                                            <asp:LinkButton ID="btnExcel" runat="server" Font-Overline="false" CssClass="btnExcel"
                                                OnClick="btnExcel_Click">
                                                <asp:Literal ID="Literal3" runat="server" Text="Gerar Excel" /><div>
                                                </div>
                                            </asp:LinkButton>
                                        </div>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </div>
                        </div>
                    </td>
                </tr>
            </table>
        </div>
        <cc1:esDataSource ID="EsDSIndice" runat="server" OnesSelect="EsDSIndice_esSelect" />
        <cc1:esDataSource ID="EsDSCarteira" runat="server" OnesSelect="EsDSCarteira_esSelect" />
        <cc1:esDataSource ID="EsDSTipoCliente" runat="server" OnesSelect="EsDSTipoCliente_esSelect" />
        <cc1:esDataSource ID="EsDSIndicadoresQualitativos" runat="server" OnesSelect="EsDSIndicadoresQualitativos_esSelect" />
        <cc1:esDataSource ID="EsDSIndicadoresQuantitativosPeriodo" runat="server" OnesSelect="EsDSIndicadoresQuantitativosPeriodo_esSelect" />
    </form>
</body>
</html>
