using System;
using System.Collections.Generic;
using Financial.Relatorio;
using System.IO;
using System.Web;
using System.Globalization;
using Financial.Web.Common;

public partial class _ReportGraficoCorrelacao : BasePage
{
    private static class VisaoRelatorio {
        public static string visaoHTML = "Report";
        public static string visaoPDF = "PDF";
        public static string visaoExcel = "Excel";
    }

    new protected void Page_Load(object sender, EventArgs e) {        
        //
        string visao = Request.QueryString["Visao"].ToString();
        //
        // Obrigatorio
        DateTime dataInicio = Convert.ToDateTime(Session["textDataInicio"]);
        DateTime dataFim = Convert.ToDateTime(Session["textDataFim"]);
        //
        int janelaMovel = (int)Session["textJanelaMovel"];
        
        // Lista de IdCarteiras e IdIndices
        List<string> listaElementos = (List<string>)(Session["listaElementos"]);
        
        //Session.Remove("textDataInicio");
        //Session.Remove("textDataFim");
        //Session.Remove("textJanelaMovel");
        //Session.Remove("listaElementos");

        string fileName = "GraficoCorrelacao";
        
        if (visao == VisaoRelatorio.visaoHTML) {
            #region HTML
            ReportViewer1.Report = new ReportGraficoCorrelacao(listaElementos, janelaMovel, dataInicio, dataFim);
            #endregion
        }
        else if (visao == VisaoRelatorio.visaoPDF) {
            #region PDF

            MemoryStream ms = new MemoryStream();
            //
            ReportGraficoCorrelacao report = new ReportGraficoCorrelacao(listaElementos, janelaMovel, dataInicio, dataFim);
            report.ExportToPdf(ms);
            //            
            ms.Seek(0, SeekOrigin.Begin);
            Response.ClearContent();
            Response.ClearHeaders();
            Response.Buffer = true;
            Response.Cache.SetCacheability(HttpCacheability.Private);
            Response.ContentType = "application/pdf";
            Response.AddHeader("Content-Length", ms.Length.ToString(CultureInfo.CurrentCulture));
            Response.AddHeader("Content-Disposition", String.Format("attachment;filename=" + fileName + ".pdf"));

            Response.BinaryWrite(ms.ToArray());
            ms.Close();
            HttpContext.Current.ApplicationInstance.CompleteRequest();
            Response.End();
            #endregion
        }
        else if (visao == VisaoRelatorio.visaoExcel) {
            #region Excel

            MemoryStream ms = new MemoryStream();
            //
            ReportGraficoCorrelacao report = new ReportGraficoCorrelacao(listaElementos, janelaMovel, dataInicio, dataFim);
            report.ExportToXls(ms);
            //
            ms.Seek(0, SeekOrigin.Begin);
            Response.ClearContent();
            Response.ClearHeaders();
            Response.Buffer = true;
            Response.Cache.SetCacheability(HttpCacheability.Private);
            Response.ContentType = "application/vnd.ms-excel";
            Response.AddHeader("Content-Length", ms.Length.ToString(CultureInfo.CurrentCulture));
            Response.AddHeader("Content-Disposition", String.Format("attachment;filename=" + fileName + ".xls"));

            Response.BinaryWrite(ms.ToArray());
            ms.Close();
            HttpContext.Current.ApplicationInstance.CompleteRequest();
            Response.End();

            #endregion
        }
    }
}