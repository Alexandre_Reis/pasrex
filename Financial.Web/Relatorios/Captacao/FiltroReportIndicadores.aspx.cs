﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using Financial.Investidor;
using Financial.Security;
using Financial.Investidor.Enums;
using EntitySpaces.Interfaces;
using System.Drawing;
using Financial.Fundo;
using Financial.Common;
using System.Collections.Generic;
using Financial.Util;
using Financial.Common.Enums;
using DevExpress.Web;
using Financial.Web.Common;
using Financial.Bolsa;

public partial class FiltroReportIndicadores : FiltroReportBasePage
{
    // TODO: Traduzir
    const string MSG_ERRO1 = "Data Inicio deve ser menor ou igual que a Data Fim";
    const string MSG_ERRO2 = "Escolha pelo menos uma Carteira";

    new protected void Page_Load(object sender, EventArgs e)
    {
        this.dropIndicadoresQuantitativosPeriodo.GridView.Width = Unit.Point(800);
        this.dropIndicadoresQualitativos.GridView.Width = Unit.Pixel(400);
    }

    #region DataSource
    protected void EsDSCarteira_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        Usuario usuario = new Usuario();
        usuario.BuscaUsuario(Context.User.Identity.Name);
        int idUsuario = usuario.IdUsuario.Value;

        CarteiraQuery carteiraQuery = new CarteiraQuery("C");
        ClienteQuery clienteQuery = new ClienteQuery("L");
        PermissaoClienteQuery permissaoClienteQuery = new PermissaoClienteQuery("P");

        carteiraQuery.Select(carteiraQuery, clienteQuery.IdTipo, clienteQuery.TipoControle);
        carteiraQuery.InnerJoin(clienteQuery).On(clienteQuery.IdCliente == carteiraQuery.IdCarteira);
        carteiraQuery.InnerJoin(permissaoClienteQuery).On(permissaoClienteQuery.IdCliente == clienteQuery.IdCliente);
        carteiraQuery.Where(permissaoClienteQuery.IdUsuario.Equal(idUsuario),
                            clienteQuery.TipoControle.NotEqual((byte)TipoControleCliente.CarteiraSimulada),
                            clienteQuery.TipoControle.NotEqual((byte)TipoControleCliente.ApenasCotacao),
                            clienteQuery.StatusAtivo.Equal((byte)StatusAtivoCliente.Ativo));
        carteiraQuery.OrderBy(carteiraQuery.Apelido.Ascending);

        CarteiraCollection coll = new CarteiraCollection();
        coll.Load(carteiraQuery);

        carteiraQuery = new CarteiraQuery("C");
        clienteQuery = new ClienteQuery("L");

        carteiraQuery.Select();
        carteiraQuery.InnerJoin(clienteQuery).On(clienteQuery.IdCliente == carteiraQuery.IdCarteira);
        carteiraQuery.Where(clienteQuery.TipoControle.Equal((byte)TipoControleCliente.ApenasCotacao),
                            clienteQuery.StatusAtivo.Equal((byte)StatusAtivoCliente.Ativo));
        carteiraQuery.OrderBy(carteiraQuery.Apelido.Ascending);

        CarteiraCollection coll2 = new CarteiraCollection();
        coll2.Load(carteiraQuery);

        coll.Combine(coll2);

        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }

    protected void EsDSIndice_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        IndiceCollection coll = new IndiceCollection();
        coll.Query.OrderBy(coll.Query.Descricao.Ascending);
        coll.Query.Load();

        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }

    protected void EsDSTipoCliente_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        TipoClienteCollection coll = new TipoClienteCollection();
        coll.LoadAll();

        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }
    #endregion

    /// <summary>
    /// Gera Relatório em HTML
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    /// <summary>
    /// Gera Relatório em Excel
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnExcel_Click(object sender, EventArgs e)
    {
        this.TrataErros();
        this.SelecionaRelatorio(VisaoRelatorio.visaoExcel);
    }

    /// <summary>
    /// Trata os Erros de Relatorio
    /// </summary>
    private void TrataErros()
    {
        #region Campos obrigatórios
        List<Control> controles = new List<Control>(new Control[] { 
    	    this.textDataInicio, this.textDataFim });

        if (base.TestaObrigatorio(controles) != "")
        {
            throw new Exception("Campos com * são obrigatórios!");
        }
        #endregion

        List<object> keyValuesId = gridConsulta.GetSelectedFieldValues("IdCarteira");

        if (keyValuesId.Count == 0)
        {
            throw new Exception(MSG_ERRO2);
        }

        if (Convert.ToDateTime(this.textDataInicio.Text) > Convert.ToDateTime(this.textDataFim.Text))
        {
            throw new Exception(MSG_ERRO1);
        }


    }

    /// <summary>
    /// Seleciona o Relatorio a ser executado
    /// </summary>
    /// <param name="visao">PDF, Excel ou HTML</param>
    private void SelecionaRelatorio(string visao)
    {
        Response.BufferOutput = true;

        List<object> keyValuesId = gridConsulta.GetSelectedFieldValues("IdCarteira");
        
        List<int> listaIdCarteiras = keyValuesId.ConvertAll<int>(delegate(object v) { return Convert.ToInt32(v); });
        Session["listaIdCarteiras"] = listaIdCarteiras;

        keyValuesId = gridConsulta.GetSelectedFieldValues("IdIndiceBenchmark");
        List<int> listaIdIndicesBenchmark = keyValuesId.ConvertAll<int>(delegate(object v) { return Convert.ToInt32(v); });
        Session["listaIdIndicesBenchmark"] = listaIdIndicesBenchmark;

        // Monta a Lista de Indicadores selecionados
        if (this.dropIndicadoresQualitativos.Text.Trim() != "")
        {
            keyValuesId = dropIndicadoresQualitativos.GridView.GetSelectedFieldValues("Id");
            List<string> listaIndicadoresQualitativos = keyValuesId.ConvertAll<string>(delegate(object v) { return Convert.ToString(v); });
            
            Session["listaIndicadoresQualitativos"] = listaIndicadoresQualitativos;
        }
        else
        {
            Session["listaIndicadoresQualitativos"] = new string[] { "" };
        }

        if (this.dropIndicadoresQuantitativosPeriodo.Text.Trim() != "")
        {
            keyValuesId = dropIndicadoresQuantitativosPeriodo.GridView.GetSelectedFieldValues("Id");
            List<string> listaIndicadoresQuantitativosPeriodo = keyValuesId.ConvertAll<string>(delegate(object v) { return Convert.ToString(v); });

            Session["listaIndicadoresQuantitativosPeriodo"] = listaIndicadoresQuantitativosPeriodo;
        }
        else
        {
            Session["listaIndicadoresQuantitativosPeriodo"] = new string[] { "" };
        }

        Session["excessoRetorno"] = dropExcessoRetorno.Value;
        Session["mostraRetornoBenchmark"] = chkRetornoBenchmark.Value;
        Session["mostraRetornoDiferencial"] = chkRetornoDiferencial.Value;
        
        // Salva os Parametros na Session
        Session["textDataInicio"] = textDataInicio.Text;
        Session["textDataFim"] = textDataFim.Text;

        Response.Redirect("~/Relatorios/Captacao/ReportIndicadores.aspx?Visao=" + visao);
    }

    new protected void CheckBoxSelectAll(object sender, EventArgs e)
    {
        ASPxCheckBox chk = sender as ASPxCheckBox;
        ASPxGridView grid = (chk.NamingContainer as GridViewHeaderTemplateContainer).Grid;
        chk.Checked = (grid.Selection.Count == grid.VisibleRowCount);
    }

    #region Classe para Binding
    public class BindingDSCollection : AtivoBolsaCollection
    {
        /// <summary>
        /// Adiciona uma Coluna Nova na Collection
        /// </summary>
        /// <param name="columnName"></param>
        /// <param name="typeColumn"></param>
        public void AddColumn(string columnName, Type typeColumn)
        {
            if (this.Table != null && !this.Table.Columns.Contains(columnName))
            {
                this.Table.Columns.Add(columnName, typeColumn);
            }
        }
    }
    #endregion

    protected void EsDSIndicadoresQualitativos_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        BindingDSCollection b = new BindingDSCollection();
        b.CreateColumnsForBinding();
        b.AddColumn("Id", typeof(System.String));
        b.AddColumn("Value", typeof(System.String));

        Dictionary<string, string> fieldNameMapping = new Dictionary<string, string>();
        fieldNameMapping.Add("Cpfcnpj", "CNPJ");
        fieldNameMapping.Add("CodigoAnbid", "Código Anbima");
        fieldNameMapping.Add("DataInicioCota", "Data Início");
        fieldNameMapping.Add("IdIndiceBenchmark", "Benchmark");
        fieldNameMapping.Add("Administrador", "Administrador");
        fieldNameMapping.Add("Gestor", "Gestor");
        fieldNameMapping.Add("DiasCotizacaoAplicacao", "Conversão cotas aplicação");
        fieldNameMapping.Add("DiasCotizacaoResgate", "Conversão cotas resgate");
        fieldNameMapping.Add("DiasLiquidacaoResgate", "Liquidação resgate");
        fieldNameMapping.Add("ClassificacaoAnbima", "Classificação Anbima");
        fieldNameMapping.Add("TaxaAdministracao", "Taxa de administração");
        fieldNameMapping.Add("TaxaAdministracaoMinima", "Valor Mínimo de administração");
        fieldNameMapping.Add("TaxaPerformance", "Taxa de performance");
        fieldNameMapping.Add("ValorMinimoInicial", "Aplicação mínima");
        fieldNameMapping.Add("ValorMinimoAplicacao", "Movimentação mínima");
        fieldNameMapping.Add("ValorMinimoSaldo", "Saldo mínimo");

        foreach (KeyValuePair<string, string> mapping in fieldNameMapping)
        {
            AtivoBolsa a = b.AddNew();
            a.SetColumn("Id", mapping.Key);
            a.SetColumn("Value", mapping.Value);
        }

        e.Collection = b;
    }

    protected void EsDSIndicadoresQuantitativosPeriodo_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        BindingDSCollection b = new BindingDSCollection();
        b.CreateColumnsForBinding();
        b.AddColumn("Id", typeof(System.String));
        b.AddColumn("Value", typeof(System.String));
        b.AddColumn("Descricao", typeof(System.String));

        Dictionary<string, string> fieldNameMapping = new Dictionary<string, string>();
        fieldNameMapping.Add("RetornoCarteiraPeriodo", "Retorno efetivo");        
        fieldNameMapping.Add("ExcessoRetornoCarteiraPeriodo", "Excesso de retorno|Retorno da carteira subtraído do retorno do benchmark");
        fieldNameMapping.Add("RetornoDiferencialPeriodo", "Diferença em %|Retorno da carteira em % do retorno do benchmark");
        fieldNameMapping.Add("VolatilidadeDiaria", "Volatilidade diária");
        fieldNameMapping.Add("VolatilidadePeriodo", "Volatilidade anualizada");
        fieldNameMapping.Add("MaiorRetorno", "Máximo retorno mensal");
        fieldNameMapping.Add("MesMaiorRetorno", "Data do máximo retorno mensal");
        fieldNameMapping.Add("MenorRetorno", "Mínimo retorno mensal");
        fieldNameMapping.Add("MesMenorRetorno", "Data do mínimo retorno mensal");
        fieldNameMapping.Add("RetornoMedioMensal", "Retorno médio mensal");
        fieldNameMapping.Add("MesesPositivos", "Meses com retorno positivo");
        fieldNameMapping.Add("MesesNegativos", "Meses com retorno negativo");
        fieldNameMapping.Add("MesesAcimaBenchmark", "Meses c/ retorno acima do benchmark");
        fieldNameMapping.Add("MesesAbaixoBenchmark", "Meses c/ retorno abaixo do benchmark");
        fieldNameMapping.Add("Sharpe", "Sharpe|Razão entre o excesso de retorno da carteira em relação ao riskfree, e a volatilidade da carteira");
        fieldNameMapping.Add("SharpeGeneralizado", "Sharpe generalizado|Razão entre o excesso de retorno da carteira em relação ao benchmark, e a volatilidade da carteira");
        fieldNameMapping.Add("Sortino", "Sortino|Razão entre o excesso de retorno da carteira em relação ao riskfree, e o 'downside-risk'(volatilidade da carteira expurgada dos retornos negativos)");
        fieldNameMapping.Add("TrackingError", "Tracking error|Desvio padrão do excesso de retorno da carteira em relação ao benchmark");
        fieldNameMapping.Add("Treynor", "Treynor|Razão entre o excesso de retorno da carteira em relação ao riskfree, e o beta da carteira");
        fieldNameMapping.Add("InformationRatio", "Information Ratio|Razão entre o excesso de retorno da carteira em relação ao riskfree, e o tracking error");
        fieldNameMapping.Add("InformationRatioSimples", "Information Ratio simples|Razão entre a média dos retornos da carteira e a sua volatilidade");
        fieldNameMapping.Add("Alfa", "Alfa|Baseado no modelo CAPM, usa o Beta da carteira, e os excessos de retorno da carteira em relação do riskfree e do benchmark em relação ao riskfree");
        fieldNameMapping.Add("Beta", "Beta|Razão entre a covariância da carteira com seu benchmark, e a variância do benchmark");
        fieldNameMapping.Add("CoeficienteVariacao", "Coeficiente de variação|Razão entre o desvio padrão da carteira e a média de retorno da carteira");
        fieldNameMapping.Add("ErroQuadraticoMedio", "Erro Quadrático Médio|Razão entre a soma dos quadrados dos excessos de retorno da carteira em relação ao benchmark, e o total de observações");
        fieldNameMapping.Add("IndicadorSucessoDiario", "Indicador de sucesso diário|Dias em que a carteira superou o benchmark em % do total");
        fieldNameMapping.Add("IndicadorSucessoMensal", "Indicador de sucesso mensal|Meses em que a carteira superou o benchmark em % do total");
        fieldNameMapping.Add("MaximoDrawdown", "Máximo drawdown|Máximo declínio em % desde o maior valor até ao mínimo valor durante um determinado período de tempo");
        fieldNameMapping.Add("Correlacao", "Correlação|Correlação entre a carteira e o seu benchmark");
        fieldNameMapping.Add("Modigliani", "Modigliani M2|Relação entre as volatilidades da carteira e do riskfree, dadas as diferenças de retorno entre carteira e benchmark e riskfree");
        fieldNameMapping.Add("VarCota90", "ValueAtRisk 90%|VAR diário da cota com 90% de limite de confiança");
        fieldNameMapping.Add("VarCota95", "ValueAtRisk 95%|VAR diário da cota com 95% de limite de confiança");
        fieldNameMapping.Add("VarCota975", "ValueAtRisk 97,5%|VAR diário da cota com 97,5% de limite de confiança");
        fieldNameMapping.Add("VarCota99", "ValueAtRisk 99%|VAR diário da cota com 99% de limite de confiança");
        fieldNameMapping.Add("PLDia", "PL na data|Valor do PL na data fim escolhida");
        fieldNameMapping.Add("CotaDia", "Cota na data|Valor da cota na data fim escolhida");
        fieldNameMapping.Add("PL12Meses", "PL médio 12 meses|PL médio considerando 12 meses em relação à data fim escolhida");
        fieldNameMapping.Add("PL12Meses_Ano1", "PL médio 12 meses Anterior|PL médio do ano anterior em relação à data fim escolhida");
        fieldNameMapping.Add("RetornoDia", "Retorno dia|Retorno dia na data fim escolhida");
        fieldNameMapping.Add("RetornoSemana", "Retorno semana|Retorno semana em relação à data fim escolhida");
        fieldNameMapping.Add("RetornoMes", "Retorno mês|Retorno mês em relação à data fim escolhida");
        fieldNameMapping.Add("RetornoAno", "Retorno ano|Retorno ano em relação à data fim escolhida");        
        fieldNameMapping.Add("Retorno3Meses", "Retorno 3 meses|Retorno 3 meses em relação à data fim escolhida");
        fieldNameMapping.Add("Retorno6Meses", "Retorno 6 meses|Retorno 6 meses em relação à data fim escolhida");
        fieldNameMapping.Add("Retorno12Meses", "Retorno 12 meses|Retorno 12 meses em relação à data fim escolhida");
        fieldNameMapping.Add("Retorno24Meses", "Retorno 24 meses|Retorno 24 meses em relação à data fim escolhida");
        fieldNameMapping.Add("Retorno36Meses", "Retorno 36 meses|Retorno 36 meses em relação à data fim escolhida");
        fieldNameMapping.Add("RetornoInicio", "Retorno desde Início|Retorno desde o início da carteira");
        fieldNameMapping.Add("RetornoAno_1", "Retorno ano anterior|Retorno fechado do ano anterior em relação à data fim escolhida");
        fieldNameMapping.Add("RetornoAno_2", "Retorno ano - 2|Retorno fechado do ano - 2 em relação à data fim escolhida");

        foreach (KeyValuePair<string, string> mapping in fieldNameMapping)
        {
            AtivoBolsa a = b.AddNew();
            a.SetColumn("Id", mapping.Key);

            string[] valueInfo = mapping.Value.Split('|');
            a.SetColumn("Value", valueInfo[0]);
            if (valueInfo.Length > 1)
            {
                a.SetColumn("Descricao", valueInfo[1]);
            }
        }

        e.Collection = b;
    }

    protected void gridConsulta_HtmlRowCreated(object sender, ASPxGridViewTableRowEventArgs e)
    {
        this.SetaCoresGrid(e);
    }

    protected void gridConsulta_CustomUnboundColumnData(object sender, DevExpress.Web.ASPxGridViewColumnDataEventArgs e)
    {
        if (e.Column.FieldName == "StatusDescricao")
        {
            int status = Convert.ToInt32(e.GetListSourceFieldValue(ClienteMetadata.ColumnNames.Status));

            string statusDescricao = TraducaoEnumsInvestidor.EnumStatusCliente.TraduzEnum(status);

            e.Value = statusDescricao;
        }
    }

    protected void gridConsulta_CustomCallback(object sender, DevExpress.Web.ASPxGridViewCustomCallbackEventArgs e)
    {
        gridConsulta.DataBind();
        gridConsulta.Selection.UnselectAll();
        gridConsulta.CancelEdit();
    }
}