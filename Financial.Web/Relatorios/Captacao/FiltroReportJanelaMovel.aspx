﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="FiltroReportJanelaMovel.aspx.cs" Inherits="FiltroReportJanelaMovel" %>

<%@ Register Assembly="EntitySpaces.Web" Namespace="EntitySpaces.Web" TagPrefix="cc1" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxe" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxcb" %>

<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx" %>

    
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">

<head id="Head1" runat="server">
    <link href="~/css/forms.css" rel="stylesheet" type="text/css" />    
</head>

<body>
    <form id="form1" runat="server">
            
    <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="true" EnableScriptLocalization="true" />
                        
    <div class="divPanel">
    <table width="100%"><tr><td>
    <div id="container_small">
                       
    <div id="header">
        <asp:Label ID="lblHeader" runat="server" Text="Gráfico de Janela Móvel"></asp:Label>
    </div>
        
    <div id="mainContentSpace">
    
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
    <ContentTemplate>

        <div class="reportFilter">
                                        
        <div style="height:20px"></div>
            
            <table cellpadding="2" cellspacing="2">            
            <tr>
                <td class="td_Label">
                    <asp:Label ID="labelTipoRelatorio" runat="server" CssClass="labelNormal" Text="Relatório:"></asp:Label>
                </td>
                <td>    
                    <dxe:ASPxComboBox ID="dropTipoRelatorio" runat="server" CssClass="dropDownListCurto" SelectedIndex=0>
                        <Items>
                            <dxe:ListEditItem Text="Volatilidade" Value="1"/>
                            <dxe:ListEditItem Text="Retorno Efetivo" Value="2" />
                            <dxe:ListEditItem Text="Tracking Error" Value="3" />
                        </Items>
                    </dxe:ASPxComboBox>
                </td>
            </tr>                
            
            <tr>
                <td class="td_Label">
                    <asp:Label ID="labelCarteira" runat="server" CssClass="labelNormal" Text="Carteiras:"></asp:Label>
                </td>
                <td>
                     <dx:ASPxGridLookup ID="dropCarteira" ClientInstanceName="dropCarteira" runat="server" SelectionMode="Multiple" KeyFieldName="IdCarteira" DataSourceID="EsDSCarteira"
                                   Width="350px" TextFormatString="{0}" MultiTextSeparator=", " Font-Size="11px" IncrementalFilteringMode="StartsWith">
                     <Columns>
                        <dx:GridViewCommandColumn ShowSelectCheckbox="True" Width="4%" />                        
                         <dx:GridViewDataColumn FieldName="IdCarteira" Caption="Id" Width="7%" CellStyle-Font-Size="X-Small"/>                         
                         <dx:GridViewDataColumn FieldName="Nome" CellStyle-Font-Size="X-Small"/>                            
                    </Columns>
                    
                    <GridViewProperties>
                        <Settings ShowFilterRow="True" />
                    </GridViewProperties>
                    
                </dx:ASPxGridLookup>
                </td>
            </tr>
            
            <tr>
                <td class="td_Label">
                    <asp:Label ID="labelIndice" runat="server" CssClass="labelNormal" Text="Índices:"></asp:Label>
                </td>
                <td>
                    <dx:ASPxGridLookup ID="dropIndice" ClientInstanceName="dropIndice" runat="server" SelectionMode="Multiple" KeyFieldName="IdIndice" DataSourceID="EsDSIndice"
                                   Width="350px" TextFormatString="{0}" MultiTextSeparator=", " Font-Size="11px" IncrementalFilteringMode="StartsWith">
                     <Columns>
                         <dx:GridViewCommandColumn ShowSelectCheckbox="True" Width="4%" />                        
                         <dx:GridViewDataColumn FieldName="IdIndice" Caption="Id" Width="7%" CellStyle-Font-Size="X-Small"/>                         
                         <dx:GridViewDataColumn FieldName="Descricao" CellStyle-Font-Size="X-Small"/>                            
                    </Columns>
                                        
                    <GridViewProperties>
                        <Settings ShowFilterRow="True" />
                    </GridViewProperties>
                    
                </dx:ASPxGridLookup>
                
                </td>
            </tr>  

            <tr>
                <td class="td_Label">
                    <asp:Label ID="label2" runat="server" CssClass="labelRequired" Text="Nr Dias:" />
                </td>                        
                <td>                                                                            
                    <dxe:ASPxSpinEdit ID="textJanelaMovel" runat="server" CssClass="textCurto" ClientInstanceName="textJanelaMovel" 
                    SpinButtons-ShowIncrementButtons="false" NumberType="Integer" MaxLength="3" MinValue="1" MaxValue="252" ToolTip="Número de Dias da Janela Móvel: Entre 1 e 252 Dias"/>                        
                </td>
            </tr>

            <tr>
                <td class="td_Label"><asp:Label ID="labelDataInicio" runat="server" CssClass="labelRequired" Text="Início:"/></td>
                <td>
                    <table cellpadding="0" cellspacing="0">                                                   
                    <tr>
                        <td><dxe:ASPxDateEdit ID="textDataInicio" runat="server" ClientInstanceName="textDataInicio"/></td>
                        <td class="labelCurto"><asp:Label ID="labelDataFim" runat="server" CssClass="labelRequired" Text="Fim:"/></td>
                        <td><dxe:ASPxDateEdit ID="textDataFim" runat="server" ClientInstanceName="textDataFim"/></td>
                    </tr>                
                    </table>
                </td>
            </tr>                                                                                                          

            </table>
                                                                                                                                                                                                                                                                                                                                                  
            <div id="reportLinkButton" class="linkButton linkButtonNoBorder">
               <asp:LinkButton ID="btnVisualiza" runat="server" Font-Overline="false" CssClass="btnReport" OnClick="btnVisualiza_Click"><asp:Literal ID="Literal1" runat="server" Text="Visualizar"/><div></div></asp:LinkButton>
               <asp:LinkButton ID="btnPDF" runat="server" Font-Overline="false" CssClass="btnPdf" OnClick="btnPDF_Click"><asp:Literal ID="Literal2" runat="server" Text="Gerar PDF"/><div></div></asp:LinkButton>
               <asp:LinkButton ID="btnExcel" runat="server" Font-Overline="false"  CssClass="btnExcel" OnClick="btnExcel_Click"><asp:Literal ID="Literal3" runat="server" Text="Gerar Excel"/><div></div></asp:LinkButton>
            </div>
    
        </ContentTemplate>
        </asp:UpdatePanel>            
                            
        </div>
        
        </div>
        </td></tr></table>
        </div>    
                
        <cc1:esDataSource ID="EsDSCarteira" runat="server" OnesSelect="EsDSCarteira_esSelect" />
        <cc1:esDataSource ID="EsDSIndice" runat="server" OnesSelect="EsDSIndice_esSelect" />
        
    </form>
</body>
</html>