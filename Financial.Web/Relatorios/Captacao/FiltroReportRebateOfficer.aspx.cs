using log4net;
using Financial.Web.Common;
using System;
using System.Web.UI.WebControls;
using Financial.Captacao;
using DevExpress.Web;

public partial class FiltroReportRebateOfficer : FiltroReportBasePage {
    private static readonly ILog log = LogManager.GetLogger(typeof(FiltroReportRebateOfficer));

    new protected void Page_Load(object sender, EventArgs e) {

        // Carrega o Combo de Ano
        #region Combo de Ano em portugues
        // Ano Atual
        int anoAtual = DateTime.Today.Year;
        int primeiroMes = 2007; // Valor Fixo
        for (int i = anoAtual; i >= primeiroMes; i--) {
            ListItem listItem = new ListItem();
            //
            listItem.Text = Convert.ToString(i);
            listItem.Value = Convert.ToString(i);
            //
            this.dropAno.Items.Add(listItem);
        }
        #endregion

        base.Page_Load(sender, e);
    }

    protected void EsDSOfficer_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        OfficerCollection coll = new OfficerCollection();
        coll.Query.Load();

        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }
   
    protected void btnVisualiza_Click(object sender, EventArgs e) {
        this.SelecionaRelatorio(VisaoRelatorio.visaoHTML);
    }

    protected void btnPDF_Click(object sender, EventArgs e) {
        this.SelecionaRelatorio(VisaoRelatorio.visaoPDF);
    }

    protected void btnExcel_Click(object sender, EventArgs e) {
        this.SelecionaRelatorio(VisaoRelatorio.visaoExcel);
    }

    /// <summary>
    /// Seleciona o relatorio a ser executado
    /// </summary>
    /// <param name="visao">PDF, Excel ou Report</param>
    private void SelecionaRelatorio(string visao) {
        Response.BufferOutput = true;
        //
        // Salva os parametros na session
        Session["dropMes"] = this.dropMes.Text;
        Session["dropAno"] = this.dropAno.Text;

        Session["dropOfficer"] = null;

        if (this.dropOfficer.SelectedIndex > -1)
        {
            Session["dropOfficer"] = this.dropOfficer.SelectedItem.Value;
        }

        Response.Redirect("~/Relatorios/Captacao/ReportRebateOfficer.aspx?Visao=" + visao);
    }
}