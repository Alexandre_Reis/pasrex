﻿using System;
using System.Web;
using System.Globalization;
using System.IO;
using Financial.Relatorio;
using Financial.Web.Common;

public partial class _ReportRebateCliente : BasePage {
    private static class VisaoRelatorio {
        public static string visaoHTML = "Report";
        public static string visaoPDF = "PDF";
        public static string visaoExcel = "Excel";
    }

    new protected void Page_Load(object sender, EventArgs e) {
        //
        string visao = Request.QueryString["Visao"].ToString();
        int mes = Convert.ToInt32(Session["dropMes"]);
        int ano = Convert.ToInt32(Session["dropAno"]);
        //

        if (visao == VisaoRelatorio.visaoHTML) {
            ReportViewer1.Report = new ReportRebateCliente(mes, ano);
        }
        else if (visao == VisaoRelatorio.visaoPDF) {
            #region PDF
            ReportRebateCliente report = new ReportRebateCliente(mes, ano);

            MemoryStream ms = new MemoryStream();
            report.ExportToPdf(ms);
            ms.Seek(0, SeekOrigin.Begin);
            Response.ClearContent();
            Response.ClearHeaders();
            Response.Buffer = true;
            Response.Cache.SetCacheability(HttpCacheability.Private);
            Response.ContentType = "application/pdf";
            Response.AddHeader("Content-Length", ms.Length.ToString(CultureInfo.CurrentCulture));
            Response.AddHeader("Content-Disposition", String.Format("attachment;filename=ReportRebateCliente.pdf"));

            Response.BinaryWrite(ms.ToArray());
            ms.Close();
            HttpContext.Current.ApplicationInstance.CompleteRequest();
            Response.End();
            #endregion
        }
        else if (visao == VisaoRelatorio.visaoExcel) {
            #region Excel
            ReportRebateCliente report = new ReportRebateCliente(mes, ano);

            MemoryStream ms = new MemoryStream();
            report.ExportToXls(ms);
            ms.Seek(0, SeekOrigin.Begin);
            Response.ClearContent();
            Response.ClearHeaders();
            Response.Buffer = true;
            Response.Cache.SetCacheability(HttpCacheability.Private);
            Response.ContentType = "application/vnd.ms-excel";
            Response.AddHeader("Content-Length", ms.Length.ToString(CultureInfo.CurrentCulture));
            Response.AddHeader("Content-Disposition", String.Format("attachment;filename=ReportRebateCliente.xls"));

            Response.BinaryWrite(ms.ToArray());
            ms.Close();
            HttpContext.Current.ApplicationInstance.CompleteRequest();
            Response.End();

            #endregion
        }
    }
}