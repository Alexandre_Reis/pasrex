﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.IO;
using Bytescout.Spreadsheet;
using System.Globalization;
using Financial.Fundo;
using System.Collections.Generic;
using Financial.CRM;
using Financial.Common;
using System.Drawing;
using Bytescout.Spreadsheet.Constants;
using Financial.Common.Enums;

public partial class Relatorios_Captacao_ReportIndicadoresFundos : System.Web.UI.Page
{

    Spreadsheet document;
    Worksheet worksheet;

    protected void Page_Load(object sender, EventArgs e)
    {

        // Obrigatorio
        DateTime dataInicio = Convert.ToDateTime(Session["textDataInicio"]);
        DateTime dataFim = Convert.ToDateTime(Session["textDataFim"]);
        //
        byte excessoRetorno = Convert.ToByte(Session["excessoRetorno"]);
        bool mostraRetornoBenchmark = Convert.ToBoolean(Session["mostraRetornoBenchmark"]);

        Dictionary<string, string> fieldNameMapping = new Dictionary<string, string>();
        fieldNameMapping.Add("Nome", "Nome Fundo");
        fieldNameMapping.Add("Cpfcnpj", "CNPJ");
        fieldNameMapping.Add("CodigoAnbid", "Código Anbima");
        fieldNameMapping.Add("DataInicioCota", "Data Início");
        fieldNameMapping.Add("IdIndiceBenchmark", "Benchmark");
        fieldNameMapping.Add("Administrador", "Administrador");
        fieldNameMapping.Add("Gestor", "Gestor");
        fieldNameMapping.Add("DiasCotizacaoAplicacao", "Dias cotas aplicação");
        fieldNameMapping.Add("DiasCotizacaoResgate", "Dias cotas resgate");
        fieldNameMapping.Add("DiasLiquidacaoResgate", "Dias liq. resgate");
        fieldNameMapping.Add("ClassificacaoAnbima", "Classificação Anbima");
        fieldNameMapping.Add("TaxaAdministracao", "% Administração");
        fieldNameMapping.Add("TaxaPerformance", "% Performance");
        fieldNameMapping.Add("ValorMinimoInicial", "Aplicação mínima");
        fieldNameMapping.Add("ValorMinimoAplicacao", "Movimentação mínima");
        fieldNameMapping.Add("ValorMinimoSaldo", "Saldo mínimo");

        int anoAnterior = dataFim.Year - 1;
        int ano_2 = anoAnterior - 1;

        fieldNameMapping.Add("RetornoCarteiraPeriodo", "Retorno efetivo (%)");
        fieldNameMapping.Add("ExcessoRetornoCarteiraPeriodo", "Excesso de retorno");
        fieldNameMapping.Add("RetornoDiferencialPeriodo", "Diferença em %");
        fieldNameMapping.Add("VolatilidadeDiaria", "Volatilidade diária (%)");
        fieldNameMapping.Add("VolatilidadePeriodo", "Volatilidade anualizada (%)");
        fieldNameMapping.Add("MaiorRetorno", "Máximo retorno mensal (%)");
        fieldNameMapping.Add("MesMaiorRetorno", "Data do máximo retorno mensal");
        fieldNameMapping.Add("MenorRetorno", "Mínimo retorno mensal (%)");
        fieldNameMapping.Add("MesMenorRetorno", "Data do mínimo retorno mensal");
        fieldNameMapping.Add("RetornoMedioMensal", "Retorno médio mensal (%)");
        fieldNameMapping.Add("MesesPositivos", "Meses com retorno positivo");
        fieldNameMapping.Add("MesesNegativos", "Meses com retorno negativo");
        fieldNameMapping.Add("MesesAcimaBenchmark", "Meses c/ retorno > benchmark");
        fieldNameMapping.Add("MesesAbaixoBenchmark", "Meses c/ retorno < benchmark");
        fieldNameMapping.Add("Sharpe", "Sharpe");
        fieldNameMapping.Add("SharpeGeneralizado", "Sharpe generalizado");
        fieldNameMapping.Add("Sortino", "Sortino");
        fieldNameMapping.Add("TrackingError", "Tracking error (%)");
        fieldNameMapping.Add("Treynor", "Treynor");
        fieldNameMapping.Add("InformationRatio", "Information Ratio");
        fieldNameMapping.Add("InformationRatioSimples", "Information Ratio simples");
        fieldNameMapping.Add("Alfa", "Alfa (%)");
        fieldNameMapping.Add("Beta", "Beta");
        fieldNameMapping.Add("CoeficienteVariacao", "Coeficiente de variação");
        fieldNameMapping.Add("ErroQuadraticoMedio", "Erro Quadrático Médio");
        fieldNameMapping.Add("IndicadorSucessoDiario", "Indicador de sucesso diário (%)");
        fieldNameMapping.Add("IndicadorSucessoMensal", "Indicador de sucesso mensal (%)");
        fieldNameMapping.Add("MaximoDrawdown", "Máximo drawdown (%)");
        fieldNameMapping.Add("Correlacao", "Correlação");
        fieldNameMapping.Add("Modigliani", "Modigliani-M2(%)");
        fieldNameMapping.Add("VarCota90", "ValueAtRisk 90%");
        fieldNameMapping.Add("VarCota95", "ValueAtRisk 95%");
        fieldNameMapping.Add("VarCota975", "ValueAtRisk 97,5%");
        fieldNameMapping.Add("VarCota99", "ValueAtRisk 99%");
        fieldNameMapping.Add("PLDia", "PL em " + dataFim.ToShortDateString());
        fieldNameMapping.Add("CotaDia", "Cota em " + dataFim.ToShortDateString());
        fieldNameMapping.Add("PL12Meses", "PL médio 12 meses (%)");
        fieldNameMapping.Add("PL12Meses_Ano1", "PL médio ano " + anoAnterior.ToString() + " (%)");
        fieldNameMapping.Add("RetornoDia", "Retorno dia (%)");
        fieldNameMapping.Add("RetornoSemana", "Retorno semana (%)");
        fieldNameMapping.Add("RetornoMes", "Retorno mês (%)");
        fieldNameMapping.Add("RetornoAno", "Retorno ano (%)");        
        fieldNameMapping.Add("Retorno3Meses", "Retorno 3 meses (%)");
        fieldNameMapping.Add("Retorno6Meses", "Retorno 6 meses (%)");
        fieldNameMapping.Add("Retorno12Meses", "Retorno 12 meses (%)");
        fieldNameMapping.Add("Retorno24Meses", "Retorno 24 meses (%)");
        fieldNameMapping.Add("Retorno36Meses", "Retorno 36 meses (%)");
        fieldNameMapping.Add("RetornoAno_1", "Retorno " + anoAnterior.ToString() + " (%)");
        fieldNameMapping.Add("RetornoAno_2", "Retorno " + ano_2.ToString() + " (%)");

        document = new Spreadsheet();
        worksheet = document.Workbook.Worksheets.Add("Indicadores");

        List<string> indicadoresQualitativos = new List<string>();
        try
        {
            indicadoresQualitativos = (List<string>)(Session["listaIndicadoresQualitativos"]);
        }
        catch { }

        List<string> indicadoresQuantitativosPeriodo = new List<string>();
        try
        {
            indicadoresQuantitativosPeriodo = (List<string>)(Session["listaIndicadoresQuantitativosPeriodo"]);
        }
        catch { }


        //Imprimir headers
        int lineCount = 0;
        int columnCount = 0, columnCountRetornoDia = 0, columnCountRetornoSemana = 0, columnCountRetornoMes = 0, columnCountRetornoAno = 0, 
                            columnCountRetorno3Meses = 0, columnCountRetorno6Meses = 0, columnCountRetorno12Meses = 0, columnCountRetorno24Meses = 0, columnCountRetorno36Meses = 0;

        const int COLUMN_WIDTH_MULTIPLIER = 8;

        indicadoresQualitativos.Insert(0, "Nome");
        
        if (indicadoresQualitativos != null)
        {
            foreach (string indicadorQualitativo in indicadoresQualitativos)
            {
                worksheet.Cell(lineCount, columnCount).Font = new Font("Arial", 8, FontStyle.Bold);
                worksheet.Cell(lineCount, columnCount).Value = fieldNameMapping[indicadorQualitativo];

                Color contentColor = Color.Silver;
                Color borderColor = Color.FromArgb(218, 220, 221);

                Cell cell = worksheet.Cell(lineCount, columnCount);
                cell.FillPattern = PatternStyle.Solid;
                cell.FillPatternForeColor = contentColor;
                cell.BottomBorderStyle = LineStyle.Thin;
                cell.BottomBorderColor = borderColor;
                cell.RightBorderStyle = LineStyle.Thin;
                cell.RightBorderColor = borderColor;
                cell.LeftBorderStyle = LineStyle.Thin;
                cell.LeftBorderColor = borderColor;
                cell.TopBorderStyle = LineStyle.Thin;
                cell.TopBorderColor = borderColor;

                uint newWidth = (uint)worksheet.Cell(lineCount, columnCount).Value.ToString().Length * COLUMN_WIDTH_MULTIPLIER;
                worksheet.Columns[columnCount].Width = newWidth;

                columnCount++;
            }
        }

        if (indicadoresQuantitativosPeriodo != null)
        {
            foreach (string indicadorQuantitativosPeriodo in indicadoresQuantitativosPeriodo)
            {
                worksheet.Cell(lineCount, columnCount).Font = new Font("Arial", 8, FontStyle.Bold);

                worksheet.Cell(lineCount, columnCount).Value = fieldNameMapping[indicadorQuantitativosPeriodo];

                Color contentColor = Color.Silver;
                Color borderColor = Color.FromArgb(218, 220, 221);

                Cell cell = worksheet.Cell(lineCount, columnCount);
                cell.FillPattern = PatternStyle.Solid;
                cell.FillPatternForeColor = contentColor;
                cell.BottomBorderStyle = LineStyle.Thin;
                cell.BottomBorderColor = borderColor;
                cell.RightBorderStyle = LineStyle.Thin;
                cell.RightBorderColor = borderColor;
                cell.LeftBorderStyle = LineStyle.Thin;
                cell.LeftBorderColor = borderColor;
                cell.TopBorderStyle = LineStyle.Thin;
                cell.TopBorderColor = borderColor;

                uint newWidth = (uint)worksheet.Cell(lineCount, columnCount).Value.ToString().Length * COLUMN_WIDTH_MULTIPLIER;
                worksheet.Columns[columnCount].Width = newWidth;

                columnCount++;
            }
        }

        List<int> codigosFundos = (List<int>)(Session["listaIdCarteiras"]);
        //List<int> idIndicesBenchmark = (List<int>)(Session["listaIdIndicesBenchmark"]);

        lineCount++;
        int carteiraCount = 0;
        columnCount = 0;
        bool poeCor = false;
        foreach (int codigoFundo in codigosFundos)
        {
            columnCount = 0;
            int idCarteira = codigoFundo;
            Carteira carteira = new Carteira();
            if (!carteira.LoadByPrimaryKey(idCarteira))
            {
                //Importar carteira
                try
                {
                //    carteira.ImportaFundoSIAnbid(codigoFundo.ToString());
                }catch{
                    continue;
                }
            }
            else
            {
                try
                {
                    //Atualizar historico cotas (se necessario)
                  //  carteira.ImportaCotasSIAnbid();
                }
                catch
                {
                    continue;
                }
            }

            #region Preenchimento Característiscas da carteira
            try
            {
                carteira.InitFundoSIAnbid();
            }
            catch
            {
                continue;
            }

            Pessoa pessoa = new Pessoa();
            pessoa.LoadByPrimaryKey(idCarteira);

            string gestor, administrador, custodiante, auditoria, taxaAdministracao,
                taxaPerformance, indiceDescricao, classificacaoAnbima;
            decimal? taxaAdministracaoNum;
            int idIndiceBenchmark = carteira.IdIndiceBenchmark.Value;
            byte? tipoIndice = null;

            if (carteira.FundoSIAnbid.Loaded)
            {
                //Fundo Anbima
                gestor = carteira.FundoSIAnbid.NomeGestor;
                administrador = carteira.FundoSIAnbid.NomeAdministrador;
                custodiante = carteira.FundoSIAnbid.Custodiante;
                auditoria = carteira.FundoSIAnbid.Auditoria;
                taxaAdministracaoNum = carteira.FundoSIAnbid.TaxaAdministracao;
                taxaPerformance = carteira.FundoSIAnbid.TaxaPerformance;
                classificacaoAnbima = carteira.FundoSIAnbid.ClassificacaoAnbima;
            }
            else
            {

                //Carteira do Financial
                Financial.Common.AgenteMercado agenteMercado = new Financial.Common.AgenteMercado();

                agenteMercado.LoadByPrimaryKey(carteira.IdAgenteGestor.Value);
                gestor = agenteMercado.Nome;

                if (carteira.IdAgenteAdministrador.Value != carteira.IdAgenteGestor)
                {
                    agenteMercado.LoadByPrimaryKey(carteira.IdAgenteAdministrador.Value);
                }

                administrador = agenteMercado.Nome;
                custodiante = "";
                auditoria = "";

                TabelaTaxaAdministracaoCollection taxas = new TabelaTaxaAdministracaoCollection();
                taxas.BuscaTabelaTaxaAdministracao(carteira.IdCarteira.Value);
                if (taxas.Count >= 1)
                {
                    taxaAdministracaoNum = taxas[0].Taxa;
                }
                else
                {
                    taxaAdministracaoNum = null;
                }

                taxaPerformance = "";

                classificacaoAnbima = "";
            }

            string percentFormat = Financial.Util.StringEnum.GetStringValue(Financial.Relatorio.ReportBase.NumeroCasasDecimais.DuasCasasDecimaisPorcentagem);
            if (taxaAdministracaoNum.HasValue)
            {
                taxaAdministracao = (taxaAdministracaoNum / 100).Value.ToString(percentFormat);
            }
            else
            {
                taxaAdministracao = "";
            }

            Financial.Common.Indice indice = new Financial.Common.Indice();
            indice.LoadByPrimaryKey((short)idIndiceBenchmark);
            indiceDescricao = indice.Descricao;
            tipoIndice = indice.Tipo;

            columnCount = FillExcelStringColumn("Nome", carteira.Nome, lineCount, columnCount, indicadoresQualitativos);

            if (indicadoresQualitativos.Contains("Cpfcnpj"))
            {
                columnCount = FillExcelStringColumn("Cpfcnpj", pessoa.Cpfcnpj, lineCount, columnCount, indicadoresQualitativos);
            }

            if (indicadoresQualitativos.Contains("CodigoAnbid"))
            {
                columnCount = FillExcelStringColumn("CodigoAnbid", carteira.CodigoAnbid, lineCount, columnCount, indicadoresQualitativos);
            }
            if (indicadoresQualitativos.Contains("DataInicioCota"))
            {
                columnCount = FillExcelStringColumn("DataInicioCota", carteira.DataInicioCota.Value.ToShortDateString(), lineCount, columnCount, indicadoresQualitativos);
            }

            if (indicadoresQualitativos.Contains("IdIndiceBenchmark"))
            {
                columnCount = FillExcelStringColumn("IdIndiceBenchmark", indiceDescricao, lineCount, columnCount, indicadoresQualitativos);
            }

            if (indicadoresQualitativos.Contains("Administrador"))
            {
                columnCount = FillExcelStringColumn("Administrador", administrador, lineCount, columnCount, indicadoresQualitativos);
            }

            if (indicadoresQualitativos.Contains("Gestor"))
            {
                columnCount = FillExcelStringColumn("Gestor", gestor, lineCount, columnCount, indicadoresQualitativos);
            }

            if (indicadoresQualitativos.Contains("DiasCotizacaoAplicacao"))
            {
                columnCount = FillExcelIntColumn("DiasCotizacaoAplicacao", carteira.DiasCotizacaoAplicacao, lineCount, columnCount, indicadoresQualitativos);
            }

            if (indicadoresQualitativos.Contains("DiasCotizacaoResgate"))
            {
                columnCount = FillExcelIntColumn("DiasCotizacaoResgate", carteira.DiasCotizacaoResgate, lineCount, columnCount, indicadoresQualitativos);
            }

            if (indicadoresQualitativos.Contains("DiasLiquidacaoResgate"))
            {
                columnCount = FillExcelIntColumn("DiasLiquidacaoResgate", carteira.DiasLiquidacaoResgate, lineCount, columnCount, indicadoresQualitativos);
            }

            if (indicadoresQualitativos.Contains("ClassificacaoAnbima"))
            {
                columnCount = FillExcelStringColumn("ClassificacaoAnbima", classificacaoAnbima, lineCount, columnCount, indicadoresQualitativos);
            }

            if (indicadoresQualitativos.Contains("TaxaAdministracao"))
            {
                columnCount = FillExcelStringColumn("TaxaAdministracao", taxaAdministracao, lineCount, columnCount, indicadoresQualitativos);
            }

            if (indicadoresQualitativos.Contains("TaxaPerformance"))
            {
                columnCount = FillExcelStringColumn("TaxaPerformance", taxaPerformance, lineCount, columnCount, indicadoresQualitativos);
            }

            if (indicadoresQualitativos.Contains("ValorMinimoInicial"))
            {
                columnCount = FillExcelDecimalColumn("ValorMinimoInicial", carteira.ValorMinimoInicial, lineCount, columnCount, indicadoresQualitativos);
            }

            if (indicadoresQualitativos.Contains("ValorMinimoAplicacao"))
            {
                columnCount = FillExcelDecimalColumn("ValorMinimoAplicacao", carteira.ValorMinimoAplicacao, lineCount, columnCount, indicadoresQualitativos);
            }

            if (indicadoresQualitativos.Contains("ValorMinimoSaldo"))
            {
                columnCount = FillExcelDecimalColumn("ValorMinimoSaldo", carteira.ValorMinimoSaldo, lineCount, columnCount, indicadoresQualitativos);
            }

            #endregion

            #region Preenchimento Estatísticas da carteira
            CalculoMedida calculoMedida;
            try
            {
                calculoMedida = new CalculoMedida(idCarteira, idIndiceBenchmark, dataInicio, dataFim);
            }
            catch
            {
                continue;
                //throw new Exception("idcarteira: " + idCarteira + " idIndiceBench" + idIndiceBenchmark + " dtini" + dataInicio + "dtim" + dataFim);
            }
            calculoMedida.SetAjustaCota(true);
            calculoMedida.ExcessoRetorno = excessoRetorno;

            decimal? retornoCarteiraPeriodo = null,
                retornoIndicePeriodo = null, excessoRetornoCarteiraPeriodo = null, retornoDiferencialPeriodo = null;

            if (indicadoresQuantitativosPeriodo.Contains("RetornoCarteiraPeriodo") ||
                indicadoresQuantitativosPeriodo.Contains("ExcessoRetornoCarteiraPeriodo") ||
                indicadoresQuantitativosPeriodo.Contains("RetornoDiferencialPeriodo")
                )
            {
                try
                {
                    retornoCarteiraPeriodo =
                    calculoMedida.CalculaRetorno(calculoMedida.DataInicioJanela.Value, calculoMedida.DataFimJanela.Value);

                    retornoIndicePeriodo =
                        calculoMedida.CalculaRetornoIndice(calculoMedida.DataInicioJanela.Value, calculoMedida.DataFimJanela.Value);

                    excessoRetornoCarteiraPeriodo = retornoCarteiraPeriodo - retornoIndicePeriodo;

                    retornoDiferencialPeriodo = 0;
                    if (tipoIndice.HasValue)
                    {
                        if (tipoIndice.Value == (byte)Financial.Common.Enums.TipoIndice.Decimal)
                        {
                            retornoDiferencialPeriodo = retornoCarteiraPeriodo - retornoIndicePeriodo;
                        }
                        else
                        {
                            if (retornoIndicePeriodo == 0)
                            {
                                retornoDiferencialPeriodo = 0;
                            }
                            else
                            {
                                retornoDiferencialPeriodo = (retornoCarteiraPeriodo / retornoIndicePeriodo) * 100M;
                            }
                        }
                    }
                }
                catch { }
            }

            decimal? volatilidadePeriodo = null, volatilidadeDiaria = null;
            if (indicadoresQuantitativosPeriodo.Contains("VolatilidadeDiaria") ||
                indicadoresQuantitativosPeriodo.Contains("VolatilidadePeriodo"))
            {
                try
                {
                    volatilidadePeriodo = calculoMedida.CalculaVolatilidade(calculoMedida.DataInicioJanela.Value, calculoMedida.DataFimJanela.Value);
                    volatilidadeDiaria = (decimal)((double)volatilidadePeriodo / Math.Sqrt((double)252M));
                }
                catch { }
            }


            decimal maiorRetorno = -99999999999999999;
            decimal menorRetorno = 99999999999999999;
            string mesMaiorRetorno = "";
            string mesMenorRetorno = "";
            int mesesPositivos = 0;
            int mesesNegativos = 0;
            int mesesAcimaBenchmark = 0;
            int mesesAbaixoBenchmark = 0;
            decimal retornoMedioMensal = 0;

            if (indicadoresQuantitativosPeriodo.Contains("MaiorRetorno") ||
                indicadoresQuantitativosPeriodo.Contains("MesMaiorRetorno") ||
                indicadoresQuantitativosPeriodo.Contains("MenorRetorno") ||
                indicadoresQuantitativosPeriodo.Contains("MesMenorRetorno") ||
                indicadoresQuantitativosPeriodo.Contains("MesesPositivos") ||
                indicadoresQuantitativosPeriodo.Contains("MesesNegativos") ||
                indicadoresQuantitativosPeriodo.Contains("MesesAcimaBenchmark") ||
                indicadoresQuantitativosPeriodo.Contains("MesesAbaixoBenchmark") ||
                indicadoresQuantitativosPeriodo.Contains("RetornoMedioMensal")
                )
            {
                try
                {
                    List<CalculoMedida.EstatisticaRetornoMensal> listaEstatisticaRetornoMensal =
                        calculoMedida.RetornaListaRetornosMensais(calculoMedida.DataInicioJanela.Value, calculoMedida.DataFimJanela.Value);
                    decimal somaRetornos = 0;
                    decimal totalRetornos = 0;
                    foreach (CalculoMedida.EstatisticaRetornoMensal estatisticaRetornoMensal in listaEstatisticaRetornoMensal)
                    {
                        if (estatisticaRetornoMensal.Retorno.HasValue)
                        {
                            somaRetornos += estatisticaRetornoMensal.Retorno.Value;
                            totalRetornos++;
                        }

                        if (estatisticaRetornoMensal.Retorno > maiorRetorno)
                        {
                            maiorRetorno = estatisticaRetornoMensal.Retorno.Value;
                            mesMaiorRetorno = estatisticaRetornoMensal.Data.Month.ToString().PadLeft(2, '0') + "/" + estatisticaRetornoMensal.Data.Year.ToString();
                        }

                        if (estatisticaRetornoMensal.Retorno < menorRetorno)
                        {
                            menorRetorno = estatisticaRetornoMensal.Retorno.Value;
                            mesMenorRetorno = estatisticaRetornoMensal.Data.Month.ToString().PadLeft(2, '0') + "/" + estatisticaRetornoMensal.Data.Year.ToString();
                        }

                        if (estatisticaRetornoMensal.Retorno >= 0)
                        {
                            mesesPositivos += 1;
                        }
                        else
                        {
                            mesesNegativos += 1;
                        }

                        if (estatisticaRetornoMensal.Retorno > estatisticaRetornoMensal.RetornoBenchmark)
                        {
                            mesesAcimaBenchmark += 1;
                        }
                        else
                        {
                            mesesAbaixoBenchmark += 1;
                        }

                    }

                    if (totalRetornos > 0)
                    {
                        retornoMedioMensal = somaRetornos / totalRetornos;
                    }
                }
                catch { }
            }

            decimal? sharpe = null, sharpeGeneralizado = null, sortino = null, trackingError = null, treynor = null,
                informationRatio = null, informationRatioSimples = null,
                alfa = null, beta = null, coeficienteVariacao = null, erroQuadraticoMedio = null,
                indicadorSucessoDiario = null, indicadorSucessoMensal = null,
                maximoDrawdown = null, correlacao = null, modigliani = null,
                varCota90 = null, varCota95 = null, varCota975 = null, varCota99 = null,
                valorPL = null, valorCota = null, PL12Meses = null, PL12Meses_1 = null,
                retornoDia = null, retornoSemana = null, retornoMes = null, retornoAno = null,
                retorno3Meses = null, retorno6Meses = null, retorno12Meses = null, retorno24Meses = null, retorno36Meses = null,
                retornoAno_1 = null, retornoAno_2 = null,
                retornoDiaBenchmark = null, retornoSemanaBenchmark = null, retornoMesBenchmark = null, retornoAnoBenchmark = null,
                retorno3MesesBenchmark = null, retorno6MesesBenchmark = null, retorno12MesesBenchmark = null, retorno24MesesBenchmark = null, retorno36MesesBenchmark = null,
                retornoDiaDiferencial = null, retornoSemanaDiferencial = null, retornoMesDiferencial = null, retornoAnoDiferencial = null,
                retorno3MesesDiferencial = null, retorno6MesesDiferencial = null, retorno12MesesDiferencial = null, retorno24MesesDiferencial = null, retorno36MesesDiferencial = null;

            if (indicadoresQuantitativosPeriodo.Contains("Sharpe"))
            {
                try
                {
                    sharpe = calculoMedida.CalculaSharpe(calculoMedida.DataInicioJanela.Value, calculoMedida.DataFimJanela.Value);
                }
                catch { }
            }

            if (indicadoresQuantitativosPeriodo.Contains("SharpeGeneralizado"))
            {
                try
                {
                    sharpeGeneralizado = calculoMedida.CalculaSharpeGeneralizado(calculoMedida.DataInicioJanela.Value, calculoMedida.DataFimJanela.Value);
                }
                catch { }
            }

            if (indicadoresQuantitativosPeriodo.Contains("Sortino"))
            {
                try
                {
                    sortino = calculoMedida.CalculaSortino(calculoMedida.DataInicioJanela.Value, calculoMedida.DataFimJanela.Value);
                }
                catch { }
            }

            if (indicadoresQuantitativosPeriodo.Contains("TrackingError"))
            {
                try
                {
                    trackingError = calculoMedida.CalculaTrackingError(calculoMedida.DataInicioJanela.Value, calculoMedida.DataFimJanela.Value);
                }
                catch { }
            }

            if (indicadoresQuantitativosPeriodo.Contains("Treynor"))
            {
                try
                {
                    treynor = calculoMedida.CalculaTreynor(calculoMedida.DataInicioJanela.Value, calculoMedida.DataFimJanela.Value);
                }
                catch { }
            }

            if (indicadoresQuantitativosPeriodo.Contains("InformationRatio"))
            {
                try
                {
                    informationRatio = calculoMedida.CalculaInformationRatio(calculoMedida.DataInicioJanela.Value, calculoMedida.DataFimJanela.Value);
                }
                catch { }
            }

            if (indicadoresQuantitativosPeriodo.Contains("InformationRatioSimples"))
            {
                try
                {
                    informationRatioSimples = calculoMedida.CalculaInformationRatioSimples(calculoMedida.DataInicioJanela.Value, calculoMedida.DataFimJanela.Value);
                }
                catch { }
            }

            if (indicadoresQuantitativosPeriodo.Contains("Alfa"))
            {
                try
                {
                    alfa = calculoMedida.CalculaAlfa(calculoMedida.DataInicioJanela.Value, calculoMedida.DataFimJanela.Value);
                }
                catch { }
            }

            if (indicadoresQuantitativosPeriodo.Contains("Beta"))
            {
                try
                {
                    beta = calculoMedida.CalculaBeta(calculoMedida.DataInicioJanela.Value, calculoMedida.DataFimJanela.Value);
                }
                catch { }
            }

            if (indicadoresQuantitativosPeriodo.Contains("CoeficienteVariacao"))
            {
                try
                {
                    coeficienteVariacao = calculoMedida.CalculaCoeficienteVariacao(calculoMedida.DataInicioJanela.Value, calculoMedida.DataFimJanela.Value);
                }
                catch { }
            }

            if (indicadoresQuantitativosPeriodo.Contains("ErroQuadraticoMedio"))
            {
                try
                {
                    erroQuadraticoMedio = calculoMedida.CalculaErroQuadraticoMedio(calculoMedida.DataInicioJanela.Value, calculoMedida.DataFimJanela.Value);
                }
                catch { }
            }

            if (indicadoresQuantitativosPeriodo.Contains("IndicadorSucessoDiario"))
            {
                try
                {
                    indicadorSucessoDiario = calculoMedida.CalculaIndicadorSucessoDiario(calculoMedida.DataInicioJanela.Value, calculoMedida.DataFimJanela.Value);
                }
                catch { }
            }

            if (indicadoresQuantitativosPeriodo.Contains("IndicadorSucessoMensal"))
            {
                try
                {
                    indicadorSucessoMensal = calculoMedida.CalculaIndicadorSucessoMensal(calculoMedida.DataInicioJanela.Value, calculoMedida.DataFimJanela.Value);
                }
                catch { }
            }

            if (indicadoresQuantitativosPeriodo.Contains("MaximoDrawdown"))
            {
                try
                {
                    maximoDrawdown = calculoMedida.CalculaMaximoDrawDown(calculoMedida.DataInicioJanela.Value, calculoMedida.DataFimJanela.Value);
                }
                catch { }
            }

            if (indicadoresQuantitativosPeriodo.Contains("Correlacao"))
            {
                try
                {
                    correlacao = calculoMedida.CalculaCorrelacao(calculoMedida.DataInicioJanela.Value, calculoMedida.DataFimJanela.Value);
                }
                catch { }
            }

            if (indicadoresQuantitativosPeriodo.Contains("Modigliani"))
            {
                try
                {
                    modigliani = calculoMedida.CalculaModigliani(calculoMedida.DataInicioJanela.Value, calculoMedida.DataFimJanela.Value);
                }
                catch { }
            }

            if (indicadoresQuantitativosPeriodo.Contains("VarCota90"))
            {
                try
                {
                    varCota90 = calculoMedida.CalculaVARCota(calculoMedida.DataInicioJanela.Value, calculoMedida.DataFimJanela.Value, 90M);
                }
                catch { }
            }

            if (indicadoresQuantitativosPeriodo.Contains("VarCota95"))
            {
                try
                {
                    varCota95 = calculoMedida.CalculaVARCota(calculoMedida.DataInicioJanela.Value, calculoMedida.DataFimJanela.Value, 95M);
                }
                catch { }
            }

            if (indicadoresQuantitativosPeriodo.Contains("VarCota975"))
            {
                try
                {
                    varCota975 = calculoMedida.CalculaVARCota(calculoMedida.DataInicioJanela.Value, calculoMedida.DataFimJanela.Value, 97.5M);
                }
                catch { }
            }

            if (indicadoresQuantitativosPeriodo.Contains("VarCota99"))
            {
                try
                {
                    varCota99 = calculoMedida.CalculaVARCota(calculoMedida.DataInicioJanela.Value, calculoMedida.DataFimJanela.Value, 99M);
                }
                catch { }
            }

            if (indicadoresQuantitativosPeriodo.Contains("PLDia") ||
                indicadoresQuantitativosPeriodo.Contains("CotaDia"))
            {
                try
                {
                    HistoricoCota historicoCota = new HistoricoCota();
                    if (historicoCota.BuscaValorCota(idCarteira, calculoMedida.DataFimJanela.Value))
                    {
                        valorCota = historicoCota.CotaFechamento.Value;
                    }

                    historicoCota = new HistoricoCota();
                    historicoCota.BuscaValorPatrimonioDia(idCarteira, calculoMedida.DataFimJanela.Value);
                    valorPL = historicoCota.PLFechamento.Value;
                }
                catch { }
            }

            if (indicadoresQuantitativosPeriodo.Contains("PL12Meses"))
            {
                try
                {
                    PL12Meses = calculoMedida.CalculaPLMedio12Meses(calculoMedida.DataFimJanela.Value);
                }
                catch { }
            }

            if (indicadoresQuantitativosPeriodo.Contains("PL12Meses_Ano1"))
            {
                try
                {
                    PL12Meses_1 = calculoMedida.CalculaPLMedio(calculoMedida.DataFimJanela.Value.Year - 1);
                }
                catch { }
            }

            if (indicadoresQuantitativosPeriodo.Contains("RetornoDia"))
            {
                try
                {
                    retornoDia = calculoMedida.CalculaRetornoDia(calculoMedida.DataFimJanela.Value);
                }
                catch { }

                if (mostraRetornoBenchmark)
                {
                    try
                    {
                        retornoDiaBenchmark = calculoMedida.CalculaRetornoDiaIndice(calculoMedida.DataFimJanela.Value);
                        retornoDiaDiferencial = calculoMedida.indice.Tipo.Value == (byte)TipoIndice.Decimal ?
                                    (retornoDia - retornoDiaBenchmark) : (retornoDia / retornoDiaBenchmark * 100M);
                    }
                    catch { }
                }
            }

            if (indicadoresQuantitativosPeriodo.Contains("RetornoSemana"))
            {
                try
                {
                    retornoSemana = calculoMedida.CalculaRetornoSemana(calculoMedida.DataFimJanela.Value);
                }
                catch { }

                if (mostraRetornoBenchmark)
                {
                    try
                    {
                        retornoSemanaBenchmark = calculoMedida.CalculaRetornoSemanaIndice(calculoMedida.DataFimJanela.Value);
                        retornoSemanaDiferencial = calculoMedida.indice.Tipo.Value == (byte)TipoIndice.Decimal ?
                                    (retornoSemana - retornoSemanaBenchmark) : (retornoSemana / retornoSemanaBenchmark * 100M);
                    }
                    catch { }
                }
            }

            if (indicadoresQuantitativosPeriodo.Contains("RetornoMes"))
            {
                try
                {
                    retornoMes = calculoMedida.CalculaRetornoMes(calculoMedida.DataFimJanela.Value);
                }
                catch { }

                if (mostraRetornoBenchmark)
                {
                    try
                    {
                        retornoMesBenchmark = calculoMedida.CalculaRetornoMesIndice(calculoMedida.DataFimJanela.Value);
                        retornoMesDiferencial = calculoMedida.indice.Tipo.Value == (byte)TipoIndice.Decimal ?
                                    (retornoMes - retornoMesBenchmark) : (retornoMes / retornoMesBenchmark * 100M);
                    }
                    catch { }
                }
            }

            if (indicadoresQuantitativosPeriodo.Contains("RetornoAno"))
            {
                try
                {
                    retornoAno = calculoMedida.CalculaRetornoAno(calculoMedida.DataFimJanela.Value);
                }
                catch { }

                if (mostraRetornoBenchmark)
                {
                    try
                    {
                        retornoAnoBenchmark = calculoMedida.CalculaRetornoAnoIndice(calculoMedida.DataFimJanela.Value);
                        retornoAnoDiferencial = calculoMedida.indice.Tipo.Value == (byte)TipoIndice.Decimal ?
                                    (retornoAno - retornoAnoBenchmark) : (retornoAno / retornoAnoBenchmark * 100M);
                    }
                    catch { }
                }
            }

            if (indicadoresQuantitativosPeriodo.Contains("Retorno3Meses"))
            {
                try
                {
                    retorno3Meses = calculoMedida.CalculaRetornoPeriodoMes(calculoMedida.DataFimJanela.Value, 3);
                }
                catch { }

                if (mostraRetornoBenchmark)
                {
                    try
                    {
                        retorno3MesesBenchmark = calculoMedida.CalculaRetornoPeriodoMesIndice(calculoMedida.DataFimJanela.Value, 3);
                        retorno3MesesDiferencial = calculoMedida.indice.Tipo.Value == (byte)TipoIndice.Decimal ?
                                    (retorno3Meses - retorno3MesesBenchmark) : (retorno3Meses / retorno3MesesBenchmark * 100M);
                    }
                    catch { }
                }
            }

            if (indicadoresQuantitativosPeriodo.Contains("Retorno6Meses"))
            {
                try
                {
                    retorno6Meses = calculoMedida.CalculaRetornoPeriodoMes(calculoMedida.DataFimJanela.Value, 6);
                }
                catch { }

                if (mostraRetornoBenchmark)
                {
                    try
                    {
                        retorno6MesesBenchmark = calculoMedida.CalculaRetornoPeriodoMesIndice(calculoMedida.DataFimJanela.Value, 6);
                        retorno6MesesDiferencial = calculoMedida.indice.Tipo.Value == (byte)TipoIndice.Decimal ?
                                    (retorno6Meses - retorno6MesesBenchmark) : (retorno6Meses / retorno6MesesBenchmark * 100M);
                    }
                    catch { }
                }
            }

            if (indicadoresQuantitativosPeriodo.Contains("Retorno12Meses"))
            {
                try
                {
                    retorno12Meses = calculoMedida.CalculaRetornoPeriodoMes(calculoMedida.DataFimJanela.Value, 12);
                }
                catch { }

                if (mostraRetornoBenchmark)
                {
                    try
                    {
                        retorno12MesesBenchmark = calculoMedida.CalculaRetornoPeriodoMesIndice(calculoMedida.DataFimJanela.Value, 12);
                        retorno12MesesDiferencial = calculoMedida.indice.Tipo.Value == (byte)TipoIndice.Decimal ?
                                    (retorno12Meses - retorno12MesesBenchmark) : (retorno12Meses / retorno12MesesBenchmark * 100M);
                    }
                    catch { }
                }
            }

            if (indicadoresQuantitativosPeriodo.Contains("Retorno24Meses"))
            {
                try
                {
                    retorno24Meses = calculoMedida.CalculaRetornoPeriodoMes(calculoMedida.DataFimJanela.Value, 24);
                }
                catch { }

                if (mostraRetornoBenchmark)
                {
                    try
                    {
                        retorno24MesesBenchmark = calculoMedida.CalculaRetornoPeriodoMesIndice(calculoMedida.DataFimJanela.Value, 24);
                        retorno24MesesDiferencial = calculoMedida.indice.Tipo.Value == (byte)TipoIndice.Decimal ?
                                    (retorno24Meses - retorno24MesesBenchmark) : (retorno24Meses / retorno24MesesBenchmark * 100M);
                    }
                    catch { }
                }
            }

            if (indicadoresQuantitativosPeriodo.Contains("Retorno36Meses"))
            {
                try
                {
                    retorno36Meses = calculoMedida.CalculaRetornoPeriodoMes(calculoMedida.DataFimJanela.Value, 36);
                }
                catch { }

                if (mostraRetornoBenchmark)
                {
                    try
                    {
                        retorno36MesesBenchmark = calculoMedida.CalculaRetornoPeriodoMesIndice(calculoMedida.DataFimJanela.Value, 36);
                        retorno36MesesDiferencial = calculoMedida.indice.Tipo.Value == (byte)TipoIndice.Decimal ?
                                    (retorno36Meses - retorno36MesesBenchmark) : (retorno36Meses / retorno36MesesBenchmark * 100M);
                    }
                    catch { }
                }
            }

            if (indicadoresQuantitativosPeriodo.Contains("RetornoAno_1"))
            {
                try
                {
                    retornoAno_1 = calculoMedida.CalculaRetornoAnoFechado(calculoMedida.DataFimJanela.Value.AddYears(-1));
                }
                catch { }
            }

            if (indicadoresQuantitativosPeriodo.Contains("RetornoAno_2"))
            {
                try
                {
                    retornoAno_2 = calculoMedida.CalculaRetornoAnoFechado(calculoMedida.DataFimJanela.Value.AddYears(-2));
                }
                catch { }
            }

            columnCount = FillExcelDecimalColumn("RetornoCarteiraPeriodo", retornoCarteiraPeriodo, lineCount, columnCount, indicadoresQuantitativosPeriodo);
            columnCount = FillExcelDecimalColumn("ExcessoRetornoCarteiraPeriodo", excessoRetornoCarteiraPeriodo, lineCount, columnCount, indicadoresQuantitativosPeriodo);
            columnCount = FillExcelDecimalColumn("RetornoDiferencialPeriodo", retornoDiferencialPeriodo, lineCount, columnCount, indicadoresQuantitativosPeriodo);
            columnCount = FillExcelDecimalColumn("VolatilidadeDiaria", volatilidadeDiaria, lineCount, columnCount, indicadoresQuantitativosPeriodo);
            columnCount = FillExcelDecimalColumn("VolatilidadePeriodo", volatilidadePeriodo, lineCount, columnCount, indicadoresQuantitativosPeriodo);
            columnCount = FillExcelDecimalColumn("MaiorRetorno", maiorRetorno, lineCount, columnCount, indicadoresQuantitativosPeriodo);
            columnCount = FillExcelStringColumn("MesMaiorRetorno", mesMaiorRetorno, lineCount, columnCount, indicadoresQuantitativosPeriodo);
            columnCount = FillExcelDecimalColumn("MenorRetorno", menorRetorno, lineCount, columnCount, indicadoresQuantitativosPeriodo);
            columnCount = FillExcelStringColumn("MesMenorRetorno", mesMenorRetorno, lineCount, columnCount, indicadoresQuantitativosPeriodo);
            columnCount = FillExcelDecimalColumn("RetornoMedioMensal", retornoMedioMensal, lineCount, columnCount, indicadoresQuantitativosPeriodo);
            columnCount = FillExcelIntColumn("MesesPositivos", mesesPositivos, lineCount, columnCount, indicadoresQuantitativosPeriodo);
            columnCount = FillExcelIntColumn("MesesNegativos", mesesNegativos, lineCount, columnCount, indicadoresQuantitativosPeriodo);
            columnCount = FillExcelIntColumn("MesesAcimaBenchmark", mesesAcimaBenchmark, lineCount, columnCount, indicadoresQuantitativosPeriodo);
            columnCount = FillExcelIntColumn("MesesAbaixoBenchmark", mesesAbaixoBenchmark, lineCount, columnCount, indicadoresQuantitativosPeriodo);
            columnCount = FillExcelDecimalColumn("Sharpe", sharpe, lineCount, columnCount, indicadoresQuantitativosPeriodo);
            columnCount = FillExcelDecimalColumn("SharpeGeneralizado", sharpeGeneralizado, lineCount, columnCount, indicadoresQuantitativosPeriodo);
            columnCount = FillExcelDecimalColumn("Sortino", sortino, lineCount, columnCount, indicadoresQuantitativosPeriodo);
            columnCount = FillExcelDecimalColumn("TrackingError", trackingError, lineCount, columnCount, indicadoresQuantitativosPeriodo);
            columnCount = FillExcelDecimalColumn("Treynor", treynor, lineCount, columnCount, indicadoresQuantitativosPeriodo);
            columnCount = FillExcelDecimalColumn("InformationRatio", informationRatio, lineCount, columnCount, indicadoresQuantitativosPeriodo);
            columnCount = FillExcelDecimalColumn("InformationRatioSimples", informationRatioSimples, lineCount, columnCount, indicadoresQuantitativosPeriodo);
            columnCount = FillExcelDecimalColumn("Alfa", alfa, lineCount, columnCount, indicadoresQuantitativosPeriodo);
            columnCount = FillExcelDecimalColumn("Beta", beta, lineCount, columnCount, indicadoresQuantitativosPeriodo);
            columnCount = FillExcelDecimalColumn("CoeficienteVariacao", coeficienteVariacao, lineCount, columnCount, indicadoresQuantitativosPeriodo);
            columnCount = FillExcelDecimalColumn("ErroQuadraticoMedio", erroQuadraticoMedio, lineCount, columnCount, indicadoresQuantitativosPeriodo);
            columnCount = FillExcelDecimalColumn("IndicadorSucessoDiario", indicadorSucessoDiario, lineCount, columnCount, indicadoresQuantitativosPeriodo);
            columnCount = FillExcelDecimalColumn("IndicadorSucessoMensal", indicadorSucessoMensal, lineCount, columnCount, indicadoresQuantitativosPeriodo);
            columnCount = FillExcelDecimalColumn("MaximoDrawdown", maximoDrawdown, lineCount, columnCount, indicadoresQuantitativosPeriodo);
            columnCount = FillExcelDecimalColumn("Correlacao", correlacao, lineCount, columnCount, indicadoresQuantitativosPeriodo);
            columnCount = FillExcelDecimalColumn("Modigliani", modigliani, lineCount, columnCount, indicadoresQuantitativosPeriodo);
            columnCount = FillExcelDecimalColumn("VarCota90", varCota90, lineCount, columnCount, indicadoresQuantitativosPeriodo);
            columnCount = FillExcelDecimalColumn("VarCota95", varCota95, lineCount, columnCount, indicadoresQuantitativosPeriodo);
            columnCount = FillExcelDecimalColumn("VarCota975", varCota975, lineCount, columnCount, indicadoresQuantitativosPeriodo);
            columnCount = FillExcelDecimalColumn("VarCota99", varCota99, lineCount, columnCount, indicadoresQuantitativosPeriodo);
            columnCount = FillExcelDecimalColumn("PLDia", valorPL, lineCount, columnCount, indicadoresQuantitativosPeriodo);
            columnCount = FillExcelDecimalColumn("CotaDia", valorCota, lineCount, columnCount, indicadoresQuantitativosPeriodo);
            columnCount = FillExcelDecimalColumn("PL12Meses", PL12Meses, lineCount, columnCount, indicadoresQuantitativosPeriodo);
            columnCount = FillExcelDecimalColumn("PL12Meses_Ano1", PL12Meses_1, lineCount, columnCount, indicadoresQuantitativosPeriodo);            
            columnCount = FillExcelDecimalColumn("RetornoDia", retornoDia, lineCount, columnCount, indicadoresQuantitativosPeriodo);
            columnCountRetornoDia = columnCount - 1;
            columnCount = FillExcelDecimalColumn("RetornoSemana", retornoSemana, lineCount, columnCount, indicadoresQuantitativosPeriodo);
            columnCountRetornoSemana = columnCount - 1;
            columnCount = FillExcelDecimalColumn("RetornoMes", retornoMes, lineCount, columnCount, indicadoresQuantitativosPeriodo);
            columnCountRetornoMes = columnCount - 1;
            columnCount = FillExcelDecimalColumn("RetornoAno", retornoAno, lineCount, columnCount, indicadoresQuantitativosPeriodo);
            columnCountRetornoAno = columnCount - 1;
            columnCount = FillExcelDecimalColumn("Retorno3Meses", retorno3Meses, lineCount, columnCount, indicadoresQuantitativosPeriodo);
            columnCountRetorno3Meses = columnCount - 1;
            columnCount = FillExcelDecimalColumn("Retorno6Meses", retorno6Meses, lineCount, columnCount, indicadoresQuantitativosPeriodo);
            columnCountRetorno6Meses = columnCount - 1;
            columnCount = FillExcelDecimalColumn("Retorno12Meses", retorno12Meses, lineCount, columnCount, indicadoresQuantitativosPeriodo);
            columnCountRetorno12Meses = columnCount - 1;
            columnCount = FillExcelDecimalColumn("Retorno24Meses", retorno24Meses, lineCount, columnCount, indicadoresQuantitativosPeriodo);
            columnCountRetorno24Meses = columnCount - 1;
            columnCount = FillExcelDecimalColumn("Retorno36Meses", retorno36Meses, lineCount, columnCount, indicadoresQuantitativosPeriodo);
            columnCountRetorno36Meses = columnCount - 1;
            columnCount = FillExcelDecimalColumn("RetornoAno_1", retornoAno_1, lineCount, columnCount, indicadoresQuantitativosPeriodo);
            columnCount = FillExcelDecimalColumn("RetornoAno_2", retornoAno_2, lineCount, columnCount, indicadoresQuantitativosPeriodo);

            #endregion


            if (!mostraRetornoBenchmark)
            {
                Color contentColor = Color.White;
                Color borderColor = Color.White;

                if (poeCor)
                {
                    contentColor = Color.FromArgb(141, 180, 227);
                    borderColor = Color.FromArgb(218, 220, 221);

                    poeCor = false;
                }
                else
                {
                    poeCor = true;
                }

                for (int j = 0; j < columnCount; j++)
                {
                    Cell cell = worksheet.Cell(lineCount, j);
                    cell.FillPattern = PatternStyle.Solid;
                    cell.FillPatternForeColor = contentColor;
                    cell.BottomBorderStyle = LineStyle.Thin;
                    cell.BottomBorderColor = borderColor;
                    cell.RightBorderStyle = LineStyle.Thin;
                    cell.RightBorderColor = borderColor;
                    cell.LeftBorderStyle = LineStyle.Thin;
                    cell.LeftBorderColor = borderColor;
                    cell.TopBorderStyle = LineStyle.Thin;
                    cell.TopBorderColor = borderColor;
                    
                }
            }

            for (int k = 0; k < columnCount; k++)
            {
                uint currentWidth = worksheet.Columns[k].Width;

                Cell cell = worksheet.Cell(lineCount, k);

                if (cell.Value != null)
                {
                    uint newWidth = (uint)cell.Value.ToString().Length * COLUMN_WIDTH_MULTIPLIER;
                    if (newWidth > currentWidth)
                    {
                        worksheet.Columns[k].Width = newWidth;
                    }
                }
            }

            //Inserir linhas de comparativo de benchmark
            if (mostraRetornoBenchmark)
            {
                lineCount++;
                FillExcelStringColumn("Nome", "% " + indiceDescricao, lineCount, 0, indicadoresQualitativos);
                FillExcelDecimalColumn("RetornoDia", retornoDiaDiferencial, lineCount, columnCountRetornoDia, indicadoresQuantitativosPeriodo);
                FillExcelDecimalColumn("RetornoSemana", retornoSemanaDiferencial, lineCount, columnCountRetornoSemana, indicadoresQuantitativosPeriodo);
                FillExcelDecimalColumn("RetornoMes", retornoMesDiferencial, lineCount, columnCountRetornoMes, indicadoresQuantitativosPeriodo);
                FillExcelDecimalColumn("RetornoAno", retornoAnoDiferencial, lineCount, columnCountRetornoAno, indicadoresQuantitativosPeriodo);
                FillExcelDecimalColumn("Retorno3Meses", retorno3MesesDiferencial, lineCount, columnCountRetorno3Meses, indicadoresQuantitativosPeriodo);
                FillExcelDecimalColumn("Retorno6Meses", retorno6MesesDiferencial, lineCount, columnCountRetorno6Meses, indicadoresQuantitativosPeriodo);
                FillExcelDecimalColumn("Retorno12Meses", retorno12MesesDiferencial, lineCount, columnCountRetorno12Meses, indicadoresQuantitativosPeriodo);
                FillExcelDecimalColumn("Retorno24Meses", retorno24MesesDiferencial, lineCount, columnCountRetorno24Meses, indicadoresQuantitativosPeriodo);
                FillExcelDecimalColumn("Retorno36Meses", retorno36MesesDiferencial, lineCount, columnCountRetorno36Meses, indicadoresQuantitativosPeriodo);                
            }

            lineCount++;
            carteiraCount++;
        }

        #region Esconde Planilha Demo do componente
        const string BYTESCOUT_WORKSHEET_NAME = "Bytescout Spreadsheet SDK";
        worksheet = document.Workbook.Worksheets.ByName(BYTESCOUT_WORKSHEET_NAME);
        if (worksheet == null)
        {
            worksheet = document.Workbook.Worksheets.Add(BYTESCOUT_WORKSHEET_NAME);
        }
        worksheet.Visible = Bytescout.Spreadsheet.Constants.SHEETVISIBILITY.Hidden;
        #endregion


        MemoryStream ms = new MemoryStream();
        
        document.SaveToStreamXLS(ms);
        document.Close();

        string fileName = "Indicadores";

        ms.Seek(0, SeekOrigin.Begin);
        Response.ClearContent();
        Response.ClearHeaders();
        Response.Buffer = true;
        Response.Cache.SetCacheability(HttpCacheability.Private);
        Response.ContentType = "application/vnd.ms-excel";
        Response.AddHeader("Content-Length", ms.Length.ToString(CultureInfo.CurrentCulture));
        Response.AddHeader("Content-Disposition", String.Format("attachment;filename=" + fileName + ".xls"));

        Response.BinaryWrite(ms.ToArray());
        ms.Close();
        HttpContext.Current.ApplicationInstance.CompleteRequest();
        Response.End();

    }

    private int FillExcelDecimalColumn(string columnName, decimal? columnValue, int lineCount, int columnCount,
        List<string> indicadores)
    {
        if (indicadores.Contains(columnName))
        {
            worksheet.Cell(lineCount, columnCount).Font = new Font("Arial", 8, FontStyle.Regular);
            if (columnValue.HasValue)
            {
                if (columnName == "PLDia" || columnName == "PL12Meses" || columnName == "PL12Meses_Ano1" || columnName.Contains("ValorMinimo"))
                {
                    worksheet.Cell(lineCount, columnCount).NumberFormatString = "#,##0.00";
                }
                else if (columnName == "CotaDia")
                {
                    worksheet.Cell(lineCount, columnCount).NumberFormatString = "#,##0.00000000";
                }
                else
                {
                    worksheet.Cell(lineCount, columnCount).NumberFormatString = "#,##0.0000";
                }

                worksheet.Cell(lineCount, columnCount).ValueAsDouble = (double)columnValue.Value;
            }
            else
            {
                worksheet.Cell(lineCount, columnCount).Value = "n.d.";
            }


            columnCount++;
        }

        return columnCount;
    }

    private int FillExcelStringColumn(string columnName, string columnValue, int lineCount, int columnCount,
        List<string> indicadores)
    {
        if (indicadores.Contains(columnName))
        {
            worksheet.Cell(lineCount, columnCount).Font = new Font("Arial", 8, FontStyle.Regular);

            worksheet.Cell(lineCount, columnCount).Value = columnValue;
            columnCount++;
        }
        return columnCount;
    }

    private int FillExcelIntColumn(string columnName, int? columnValue, int lineCount, int columnCount,
        List<string> indicadores)
    {
        if (indicadores.Contains(columnName))
        {
            worksheet.Cell(lineCount, columnCount).Font = new Font("Arial", 8, FontStyle.Regular);

            if (columnValue.HasValue)
            {
                worksheet.Cell(lineCount, columnCount).Value = columnValue.Value;
            }
            else
            {
                worksheet.Cell(lineCount, columnCount).Value = "n.d.";
            }
            columnCount++;
        }
        return columnCount;
    }
}
