using System;
using Financial.Web.Common;
using System.Collections.Generic;
using Financial.Relatorio;
using System.IO;
using System.Web;
using System.Globalization;

public partial class _ReportJanelaMovel : BasePage
{
    private static class VisaoRelatorio
    {
        public static string visaoHTML = "Report";
        public static string visaoPDF = "PDF";
        public static string visaoExcel = "Excel";
    }

    private enum TipoRelatorio
    {
        Volatilidade = 1,
        RetornoEfetivo = 2,
        TrackingError = 3
    }

    new protected void Page_Load(object sender, EventArgs e)
    {
        //

        string visao = Request.QueryString["Visao"].ToString();
        //
        // Obrigatorio
        DateTime dataInicio = Convert.ToDateTime(Session["textDataInicio"]);
        DateTime dataFim = Convert.ToDateTime(Session["textDataFim"]);
        //
        int janelaMovel = (int)Session["textJanelaMovel"];
        TipoRelatorio tipoRelatorio = (TipoRelatorio)Session["dropTipoRelatorio"];

        // Lista de IdCarteiras e IdIndices
        List<int> listaIdCarteiras = (List<int>)(Session["listaIdCarteiras"]);
        List<int> listaIdIndices = (List<int>)(Session["listaIdIndices"]);

        //Session.Remove("textDataInicio");
        //Session.Remove("textDataFim");
        //Session.Remove("textJanelaMovel");
        //Session.Remove("dropTipoRelatorio");
        //Session.Remove("listaIdCarteiras");
        //Session.Remove("listaIdIndices");

        // Nome Do Arquivo
        string fileName = "";
        //
        switch (tipoRelatorio)
        {
            case TipoRelatorio.Volatilidade: fileName = "GraficoVolatilidade";
                break;
            case TipoRelatorio.RetornoEfetivo: fileName = "GraficoRetornoEfetivo";
                break;
            case TipoRelatorio.TrackingError: fileName = "GraficoTrackingError";
                break;
        }

        if (visao == VisaoRelatorio.visaoHTML)
        {
            #region HTML
            switch (tipoRelatorio)
            {
                case TipoRelatorio.Volatilidade:
                    ReportViewer1.Report = new ReportGraficoVolatilidade(listaIdCarteiras, listaIdIndices, janelaMovel, dataInicio, dataFim);
                    break;
                case TipoRelatorio.RetornoEfetivo:
                    ReportViewer1.Report = new ReportGraficoRetornoEfetivo(listaIdCarteiras, listaIdIndices, janelaMovel, dataInicio, dataFim);
                    break;
                case TipoRelatorio.TrackingError:
                    ReportViewer1.Report = new ReportGraficoTrackingError(listaIdCarteiras, janelaMovel, dataInicio, dataFim);
                    break;
            }
            #endregion
        }
        else if (visao == VisaoRelatorio.visaoPDF)
        {
            #region PDF

            MemoryStream ms = new MemoryStream();
            //
            switch (tipoRelatorio)
            {
                case TipoRelatorio.Volatilidade:
                    ReportGraficoVolatilidade report = new ReportGraficoVolatilidade(listaIdCarteiras, listaIdIndices, janelaMovel, dataInicio, dataFim);
                    report.ExportToPdf(ms);
                    break;
                case TipoRelatorio.RetornoEfetivo:
                    ReportGraficoRetornoEfetivo report1 = new ReportGraficoRetornoEfetivo(listaIdCarteiras, listaIdIndices, janelaMovel, dataInicio, dataFim);
                    report1.ExportToPdf(ms);
                    break;
                case TipoRelatorio.TrackingError:
                    ReportGraficoTrackingError report2 = new ReportGraficoTrackingError(listaIdCarteiras, janelaMovel, dataInicio, dataFim);
                    report2.ExportToPdf(ms);
                    break;
            }
            //            
            ms.Seek(0, SeekOrigin.Begin);
            Response.ClearContent();
            Response.ClearHeaders();
            Response.Buffer = true;
            Response.Cache.SetCacheability(HttpCacheability.Private);
            Response.ContentType = "application/pdf";
            Response.AddHeader("Content-Length", ms.Length.ToString(CultureInfo.CurrentCulture));
            Response.AddHeader("Content-Disposition", String.Format("attachment;filename=" + fileName + ".pdf"));

            Response.BinaryWrite(ms.ToArray());
            ms.Close();
            HttpContext.Current.ApplicationInstance.CompleteRequest();
            Response.End();
            #endregion
        }
        else if (visao == VisaoRelatorio.visaoExcel)
        {
            #region Excel

            MemoryStream ms = new MemoryStream();
            //
            switch (tipoRelatorio)
            {
                case TipoRelatorio.Volatilidade:
                    ReportGraficoVolatilidade report = new ReportGraficoVolatilidade(listaIdCarteiras, listaIdIndices, janelaMovel, dataInicio, dataFim);
                    report.ExportToXls(ms);
                    break;
                case TipoRelatorio.RetornoEfetivo:
                    ReportGraficoRetornoEfetivo report1 = new ReportGraficoRetornoEfetivo(listaIdCarteiras, listaIdIndices, janelaMovel, dataInicio, dataFim);
                    report1.ExportToXls(ms);
                    break;
                case TipoRelatorio.TrackingError:
                    ReportGraficoTrackingError report2 = new ReportGraficoTrackingError(listaIdCarteiras, janelaMovel, dataInicio, dataFim);
                    report2.ExportToXls(ms);
                    break;
            }
            //
            ms.Seek(0, SeekOrigin.Begin);
            Response.ClearContent();
            Response.ClearHeaders();
            Response.Buffer = true;
            Response.Cache.SetCacheability(HttpCacheability.Private);
            Response.ContentType = "application/vnd.ms-excel";
            Response.AddHeader("Content-Length", ms.Length.ToString(CultureInfo.CurrentCulture));
            Response.AddHeader("Content-Disposition", String.Format("attachment;filename=" + fileName + ".xls"));

            Response.BinaryWrite(ms.ToArray());
            ms.Close();
            HttpContext.Current.ApplicationInstance.CompleteRequest();
            Response.End();

            #endregion
        }

    }
}