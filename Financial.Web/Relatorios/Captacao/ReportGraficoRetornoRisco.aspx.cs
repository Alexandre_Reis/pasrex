using Financial.Web.Common;
using System;
using System.IO;
using System.Web;
using Financial.Relatorio;
using System.Collections.Generic;
using System.Globalization;

public partial class _ReportGraficoRetornoRisco : BasePage {
    private static class VisaoRelatorio {
        public static string visaoHTML = "Report";
        public static string visaoPDF = "PDF";
        public static string visaoExcel = "Excel";
    }

    new protected void Page_Load(object sender, EventArgs e) {
        //
        string visao = Request.QueryString["Visao"].ToString();
        //
        // Obrigatorio
        byte tipoRetorno = Convert.ToByte(Session["dropTipoRetorno"]);
        DateTime dataInicio = Convert.ToDateTime(Session["textDataInicio"]);
        DateTime dataFim = Convert.ToDateTime(Session["textDataFim"]);        
        //
        // Lista de IdCarteiras
        List<int> listaIdCarteiras = (List<int>)(Session["listaIdCarteiras"]);

        // Nome Do Arquivo
        string fileName = "RetornoRisco";
        //
        if (visao == VisaoRelatorio.visaoHTML) {
            ReportViewer1.Report = new ReportGraficoRetornoRisco(listaIdCarteiras, dataInicio, dataFim, tipoRetorno);
        }
        else if (visao == VisaoRelatorio.visaoPDF) {
            #region PDF
            ReportGraficoRetornoRisco report = new ReportGraficoRetornoRisco(listaIdCarteiras, dataInicio, dataFim, tipoRetorno);
            //
            MemoryStream ms = new MemoryStream();
            report.ExportToPdf(ms);
            ms.Seek(0, SeekOrigin.Begin);
            Response.ClearContent();
            Response.ClearHeaders();
            Response.Buffer = true;
            Response.Cache.SetCacheability(HttpCacheability.Private);
            Response.ContentType = "application/pdf";
            Response.AddHeader("Content-Length", ms.Length.ToString(CultureInfo.CurrentCulture));
            Response.AddHeader("Content-Disposition", String.Format("attachment;filename=" + fileName + ".pdf"));

            Response.BinaryWrite(ms.ToArray());
            ms.Close();
            HttpContext.Current.ApplicationInstance.CompleteRequest();
            Response.End();
            #endregion
        }
        else if (visao == VisaoRelatorio.visaoExcel) {
            #region Excel
            ReportGraficoRetornoRisco report = new ReportGraficoRetornoRisco(listaIdCarteiras, dataInicio, dataFim, tipoRetorno);
            //
            MemoryStream ms = new MemoryStream();
            report.ExportToXls(ms);
            ms.Seek(0, SeekOrigin.Begin);
            Response.ClearContent();
            Response.ClearHeaders();
            Response.Buffer = true;
            Response.Cache.SetCacheability(HttpCacheability.Private);
            Response.ContentType = "application/vnd.ms-excel";
            Response.AddHeader("Content-Length", ms.Length.ToString(CultureInfo.CurrentCulture));
            Response.AddHeader("Content-Disposition", String.Format("attachment;filename=" + fileName + ".xls"));

            Response.BinaryWrite(ms.ToArray());
            ms.Close();
            HttpContext.Current.ApplicationInstance.CompleteRequest();
            Response.End();

            #endregion
        }        
    }
}