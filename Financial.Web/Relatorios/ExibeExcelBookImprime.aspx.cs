﻿using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using EntitySpaces.Interfaces;
using System.Collections.Generic;
using Financial.Web.Common;
using System.IO;
using System.Globalization;
using Dart.PowerTCP.Zip;
using System.Text;

public partial class _ExibeExcelBookImprime : BasePage {
       
    new protected void Page_Load(object sender, EventArgs e) {
        
        #region Gera Excel

        Dictionary<string, MemoryStream> msExcel = (Dictionary<string, MemoryStream>)Session["streamReports"];

        if (msExcel.Count == 1) { // Excel           
            List<string> nomeArquivoExcel = new List<string>(msExcel.Keys);
            List<MemoryStream> mExcel = new List<MemoryStream>(msExcel.Values);

            #region Excel
            Response.ClearContent();
            Response.ClearHeaders();
            Response.Buffer = true;
            Response.Cache.SetCacheability(HttpCacheability.Private);
            Response.ContentType = "application/xls";
            Response.AddHeader("Content-Length", mExcel[0].Length.ToString(CultureInfo.CurrentCulture));
            Response.AppendHeader("Content-Disposition", String.Format("attachment;filename=" + nomeArquivoExcel[0]));

            Response.BinaryWrite(mExcel[0].ToArray());
            mExcel[0].Close();
            //
            HttpContext.Current.ApplicationInstance.CompleteRequest();
            Response.End();
            #endregion
        }
        else { // Gera Zip
            #region Zip

            Archive arquivo = new Archive();
            MemoryStream msZip = new MemoryStream();
            //                
            try {
                int j = 0;
                foreach (KeyValuePair<string, MemoryStream> pair in msExcel) {

                    /* Necessário voltar o ponteiro do arquivo para o Inicio para ler corretamente */
                    ((MemoryStream)pair.Value).Seek(0, SeekOrigin.Begin);

                    arquivo.Add(pair.Value); // Memory Stream
                    arquivo[j].Name = pair.Key; // Nome do Arquivo

                    if (arquivo[j].Name.Contains("BookGeral")) {
                        string diretorio = arquivo[j].Name.Substring(0, arquivo[j].Name.IndexOf("_") );
                        arquivo[j].Path = diretorio + "/";
                    }

                    // Controla Criação de um Diretório para Arquivos Excel que podem ter multiplas partes
                    // Nome do arquivo vai ter um "#" na posicao 0
                    /*
                     ExtratoCotista = Tipo 104 e ExtratoCotista = Tipo 105
                     NotasAplicacaoResgate = Tipo 115
                     ApuracaoGanhoRV = Tipo 170
                     */
                    if (arquivo[j].Name[0] == '#') {
                        arquivo[j].Name = arquivo[j].Name.Replace("#", "");

                        if (arquivo[j].Name.Contains("_ExtratoCotista") || arquivo[j].Name.Contains("_ExtratoCotistaComCarteira")) {
                            arquivo[j].Path = "ExtratoCotista/";
                        }
                        else if (arquivo[j].Name.Contains("_ApuracaoGanhhoRV")) {
                            arquivo[j].Path = "ApuracaoGanhhoRV/";
                        }
                        else if (arquivo[j].Name.Contains("_NotaAplicacao") || arquivo[j].Name.Contains("_NotaAplicacaoResgate")) {
                            arquivo[j].Path = "NotaAplicacao/";
                        }
                    }

                    //
                    j++;
                }
                //
                arquivo.Zip(msZip);
            }
            catch (Exception e1) {
                throw new Exception(e1.Message + "Erro ao Zipar o Arquivo");
            }
            Response.ClearContent();
            Response.ClearHeaders();
            Response.Buffer = true;
            Response.Cache.SetCacheability(HttpCacheability.Private);
            Response.ContentType = "application/zip";
            Response.AddHeader("Content-Length", msZip.Length.ToString(CultureInfo.CurrentCulture));
            Response.AppendHeader("Content-Disposition", String.Format("attachment;filename=BookImprime.zip"));

            Response.BinaryWrite(msZip.ToArray());
            msZip.Close();

            HttpContext.Current.ApplicationInstance.CompleteRequest();
            Response.End();
            #endregion
        }
        
        #endregion        
    }
}