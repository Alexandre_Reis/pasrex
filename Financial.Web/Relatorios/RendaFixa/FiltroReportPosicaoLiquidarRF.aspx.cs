﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Collections.Generic;
using System.Web.UI.HtmlControls;
using System.Text;

using Financial.Fundo;
using Financial.Investidor;
using Financial.Web.Util;
using Financial.Investidor.Enums;
using Financial.Security;

using log4net;
using Financial.Security.Enums;

using EntitySpaces.Interfaces;

using DevExpress.Web;
using Financial.RendaFixa;
using Financial.Web.Common;
using Financial.Common;

public partial class FiltroReportPosicaoLiquidarRF : FiltroReportBasePage
{
    private static readonly ILog log = LogManager.GetLogger(typeof(FiltroReportPosicaoLiquidarRF));

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    new protected void Page_Load(object sender, EventArgs e)
    {
        btnEditCodigo.Focus();
        //    
        this.HasPopupCliente = true;
        base.Page_Load(sender, e);

        this.TrataTravamentoCampos();
    }

    /// <summary>
    /// 
    /// </summary>
    private void TrataTravamentoCampos()
    {
        Usuario usuario = new Usuario();
        usuario.BuscaUsuario(HttpContext.Current.User.Identity.Name);
        int tipoTrava = usuario.TipoTrava.Value;

        if (tipoTrava == (int)TipoTravaUsuario.TravaCliente)
        {
            btnEditCodigo.Enabled = false;

            PermissaoCliente permissaoCliente = new PermissaoCliente();
            int? idCliente = permissaoCliente.RetornaClienteAssociado(HttpContext.Current.User.Identity.Name);

            if (idCliente.HasValue)
            {
                Cliente cliente = new Cliente();

                List<esQueryItem> campos = new List<esQueryItem>();
                campos.Add(cliente.Query.Apelido);
                cliente.LoadByPrimaryKey(campos, idCliente.Value);
                string apelido = cliente.Apelido;

                btnEditCodigo.Text = Convert.ToString(idCliente.Value);
                textNome.Text = apelido;
            }
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void ASPxCallback1_Callback(object source, DevExpress.Web.CallbackEventArgs e)
    {
        string nome = "";
        if (!String.IsNullOrEmpty(e.Parameter) && e.Parameter != "null")
        {
            int idCliente = Convert.ToInt32(e.Parameter);
            Cliente cliente = new Cliente();
            cliente.LoadByPrimaryKey(idCliente);

            if (cliente.str.Apelido != "")
            {
                if (cliente.IsAtivo)
                {
                    PermissaoCliente permissaoCliente = new PermissaoCliente();
                    nome = permissaoCliente.RetornaAcessoClienteComControle(idCliente, HttpContext.Current.User.Identity.Name)
                            ? cliente.str.Apelido : "no_access";
                }
                else
                {
                    nome = "no_active";
                }
            }
        }
        e.Result = nome;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void EsDSCliente_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        ClienteCollection clienteCollection = new ClienteCollection();
        clienteCollection.BuscaClientesComAcesso(HttpContext.Current.User.Identity.Name);

        e.Collection = clienteCollection;
    }
    
    protected void EsDSAssessor_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        AssessorCollection coll = new AssessorCollection();
        coll.LoadAll();

        Assessor a = new Assessor();
        a.IdAssessor = null;
        a.Nome = "";

        coll.AttachEntity(a);
        //
        coll.Sort = AssessorMetadata.ColumnNames.Nome + " ASC";
        e.Collection = coll;
    }

    protected void EsDSGrupoAfinidade_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        GrupoAfinidadeCollection coll = new GrupoAfinidadeCollection();
        coll.LoadAll();

        GrupoAfinidade a = new GrupoAfinidade();
        a.IdGrupo = null;
        a.Descricao = "";

        coll.AttachEntity(a);
        //
        coll.Sort = GrupoAfinidadeMetadata.ColumnNames.Descricao + " ASC";

        e.Collection = coll;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnVisualiza_Click(object sender, EventArgs e)
    {
        this.TrataErros();
        this.SelecionaRelatorio(VisaoRelatorio.visaoHTML);
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnPDF_Click(object sender, EventArgs e)
    {
        this.TrataErros();
        this.SelecionaRelatorio(VisaoRelatorio.visaoPDF);
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnExcel_Click(object sender, EventArgs e)
    {
        this.TrataErros();
        this.SelecionaRelatorio(VisaoRelatorio.visaoExcel);
    }

    /// <summary>
    /// Trata os Erros de Relatorio
    /// </summary>
    /// <exception cref="Exception"> 
    /// </exception>
    private void TrataErros()
    {
    }

    /// <summary>
    /// Seleciona o relatorio a ser executado
    /// </summary>
    /// <param name="visao">PDF, Excel ou Report</param>
    private void SelecionaRelatorio(string visao) {
        Response.BufferOutput = true;
        //
        // Salva os Parametros na Session
        Session["btnEditCodigo"] = btnEditCodigo.Text;
        Session["textIdTitulo"] = textIdTitulo.Text;
        Session["textData"] = textData.Text;

        string idClasse = null;
        if (dropPapel.SelectedIndex > 0) {
            idClasse = Convert.ToString(dropPapel.SelectedItem.Value);
        }
        Session["dropPapel"] = idClasse;

        string idAssessor = null;
        if (dropAssessor.SelectedIndex > 0) {
            idAssessor = Convert.ToString(dropAssessor.SelectedItem.Value);
        }
        Session["dropAssessor"] = idAssessor;

        string idGrupoAfinidade = null;
        if (dropGrupoAfinidade.SelectedIndex > 0) {
            idGrupoAfinidade = Convert.ToString(dropGrupoAfinidade.SelectedItem.Value);
        }
        Session["dropGrupoAfinidade"] = idGrupoAfinidade;

        string idCustodia = null;
        if (dropCustodia.SelectedIndex > 0) {
            idCustodia = Convert.ToString(dropCustodia.SelectedItem.Value);
        }
        Session["dropCustodia"] = idCustodia;

        string tipoPessoa = null;
        if (dropTipoPessoa.SelectedIndex > 0) {
            tipoPessoa = Convert.ToString(dropTipoPessoa.SelectedItem.Value);
        }
        Session["dropTipoPessoa"] = tipoPessoa;

        //Response.Redirect("~/Relatorios/RendaFixa/ReportPosicaoLiquidarRF.aspx?Visao=" + visao);

        string url = "~/Relatorios/RendaFixa/ReportPosicaoLiquidarRF.aspx?Visao=" + visao;
        string redirectURL = Page.ResolveClientUrl(url);

        string script = "window.location = '" + redirectURL + "';";
        ScriptManager.RegisterStartupScript(this, typeof(Page), "RedirectTo", script, true);
    }
}