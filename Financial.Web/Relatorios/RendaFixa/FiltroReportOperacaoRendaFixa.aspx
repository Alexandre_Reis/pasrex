﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="FiltroReportOperacaoRendaFixa.aspx.cs" Inherits="FiltroReportOperacaoRendaFixa" %>

<%@ Register Assembly="System.Web, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"
    Namespace="System.Web.UI.HtmlControls" TagPrefix="cc2" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxcb" %>
<%@ Register Assembly="System.Web, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"
    Namespace="System.Web.UI" TagPrefix="cc1" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxnb" %>    
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxe" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxwgv" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxpc" %>
<%@ Register Assembly="EntitySpaces.Web" Namespace="EntitySpaces.Web" TagPrefix="cc1" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >

<head id="Head1" runat="server">
<link href="~/css/forms.css" rel="stylesheet" type="text/css" />

<script type="text/javascript" language="JavaScript">
var backgroundColor;
function OnGetDataCliente(values) {
    btnEditCodigo.SetValue(values);                    
    popupCliente.HideWindow();
    ASPxCallback1.SendCallback(btnEditCodigo.GetValue());        
    btnEditCodigo.Focus();    
}
</script>
<script type="text/javascript" language="Javascript" src="../../js/global.js"></script>
</head>

<body >
    <form id="form1" runat="server"> 
    
    <dxcb:ASPxCallback ID="ASPxCallback1" runat="server" OnCallback="ASPxCallback1_Callback">
        <ClientSideEvents CallbackComplete="function(s, e) {             
            OnCallBackCompleteCliente(s, e, popupMensagemCliente, btnEditCodigo, document.getElementById('textNome'));
        }        
        "/>
    </dxcb:ASPxCallback>
    
    <dxcb:ASPxCallback ID="callBackPeriodo" runat="server" OnCallback="callBackPeriodo_Callback">
        <ClientSideEvents CallbackComplete="function(s, e) 
        {   
            if (e.result != '')
            {                   
                var resultSplit = e.result.split('|');
                
                var lingua = resultSplit[2];            
                
                var newDate = LocalizedData(resultSplit[0], lingua);
                textDataInicio.SetValue(newDate);
                
                var newDate = LocalizedData(resultSplit[1], lingua);
                textDataFim.SetValue(newDate);
            }
        }        
        "/>
    </dxcb:ASPxCallback>
    
    <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="true" EnableScriptLocalization="true" />
    
    <div class="divPanel">
    <table width="100%"><tr><td>
    <div id="container_small">
    
    <div id="header"><asp:Label ID="LabelOperacaoRendaFixa" runat="server" Text="Mapa de Operações em Renda Fixa" /></div>
        
    <div id="mainContentSpace">
           
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
    <ContentTemplate>
        
        <div class="reportFilter">
        <div style="height:20px"></div>                               
        <table cellpadding="2" cellspacing="2">
            <tr>
            <td class="td_Label">
                <asp:Label ID="labelCliente" runat="server" CssClass="labelNormal" Text="Cliente:"></asp:Label>
            </td>
            
            <td>
                <dxe:ASPxSpinEdit ID="btnEditCodigo" runat="server" CssClass="textButtonEdit" 
                                            EnableClientSideAPI="True"
                                            ClientInstanceName="btnEditCodigo" SpinButtons-ShowIncrementButtons="false"
                                            MaxLength="9" NumberType="Integer" AllowMouseWheel="false">  
                <Buttons>
                    <dxe:EditButton></dxe:EditButton>                                
                </Buttons>        
                <ClientSideEvents                        
                         KeyPress="function(s, e) {document.getElementById('textNome').value = '';}" 
                         ButtonClick="function(s, e) {popupCliente.ShowAtElementByID(s.name);}" 
                         LostFocus="function(s, e) {OnLostFocus(popupMensagemCliente, ASPxCallback1, btnEditCodigo);}"
                />                            
                </dxe:ASPxSpinEdit>                
            </td>                          
            
            <td colspan="2" width="300">
                <asp:TextBox ID="textNome" runat="server" CssClass="textNome" Enabled="false"></asp:TextBox>       
            </td>
            </tr>
            
            <tr>
            <td class="td_Label">            
                <asp:Label ID="labelIdTitulo" runat="server" CssClass="labelNormal" Text="Id Título:"/>
            </td>
            <td colspan="3">                                             
                 <dxe:ASPxSpinEdit ID="textIdTitulo" runat="server" CssClass="textButtonEdit" 
                                            ClientInstanceName="textIdTitulo" MaxLength="9" NumberType="Integer" MinValue="0"/>                
            </td>
            </tr>
                        
            <tr>
                <td class="td_Label">
                    <asp:Label ID="labelDescricaoPapel" runat="server" CssClass="labelNormal" Text="Papel:"/>
                </td>                
                <td colspan="3">
                    <dxe:ASPxComboBox ID="dropPapel" runat="server" ClientInstanceName="dropPapel"
                                        DataSourceID="EsDSPapelRendaFixa" IncrementalFilteringMode="StartsWith"  
                                        ShowShadow="false" DropDownStyle="DropDownList"
                                        CssClass="dropDownListCurto" TextField="Descricao" ValueField="IdPapel">                                        
                    </dxe:ASPxComboBox>         
                </td>
            </tr>
            
            <tr>
            <td class="td_Label">
                <asp:Label ID="label2" runat="server" CssClass="labelNormal" Text="Período:"/>
            </td>
            <td colspan="2">
                <dxe:ASPxComboBox ID="dropTipoPeriodo" runat="server" ClientInstanceName="dropTipoPeriodo" DropDownStyle="DropDown" ShowShadow="False" CssClass="dropDownListCurto_2" 
                            ClientSideEvents-SelectedIndexChanged="function(s, e) { callBackPeriodo.SendCallback(); }">
                                    
                </dxe:ASPxComboBox>            
            </td>
            </tr>
            
            <tr>                                   
                <td class="td_Label">
                    <asp:Label ID="labelClearing" runat="server" CssClass="labelNormal" Text="Clearing: " />
                </td>
                    
                <td colspan="3">
                    <dxe:ASPxComboBox ID="dropClearing" runat="server" CssClass="dropDownListCurto"
                        ClientInstanceName="dropClearing" SelectedIndex="0">
                    <Items>
                        <dxe:ListEditItem Value="0" Text="-" />
                        <dxe:ListEditItem Value="1" Text="Selic" />
                        <dxe:ListEditItem Value="2" Text="Cetip" />
                        <dxe:ListEditItem Value="3" Text="CBLC" />
                    </Items>
                    </dxe:ASPxComboBox>
                </td>            
            </tr>
            
            <tr>                                   
                <td class="td_Label">
                    <asp:Label ID="labelTipoOperacao" runat="server" CssClass="labelNormal" Text="Tipo Operação: " />
                </td>
                    
                <td colspan="3">
                    <dxe:ASPxComboBox ID="dropTipoOperação" runat="server" CssClass="dropDownListCurto"
                        ClientInstanceName="dropTipoOrdenacao" SelectedIndex="0">
                    <Items>
                        <dxe:ListEditItem Value="-1" Text="-" />
                        <dxe:ListEditItem Value="1" Text="Compra Final" />
                        <dxe:ListEditItem Value="2" Text="Venda Final" />
                        <dxe:ListEditItem Value="3" Text="Compra c/ Rev." />
                        <dxe:ListEditItem Value="4" Text="Venda c/ Rec." />
                        <dxe:ListEditItem Value="6" Text="Venda Total" />
                        <dxe:ListEditItem Value="10" Text="Compra Casada" />
                        <dxe:ListEditItem Value="11" Text="Venda Casada" />
                        <dxe:ListEditItem Value="12" Text="Antecipação Revenda" />
                        <dxe:ListEditItem Value="13" Text="Antecipação Recompra" />
                        <dxe:ListEditItem Value="20" Text="Depósito" />
                        <dxe:ListEditItem Value="21" Text="Retirada" />      
                    </Items>
                    </dxe:ASPxComboBox>
                </td>            
            </tr>
            
                        
            <tr>
            <td class="td_Label">                
                <asp:Label ID="labelDataInicio" runat="server" CssClass="labelRequired" Text="Início:"></asp:Label>
            </td>                        
            <td>
                <dxe:ASPxDateEdit ID="textDataInicio" runat="server" ClientInstanceName="textDataInicio"/>
            </td>  
            
            <td colspan="2">
                <table>
                    <tr><td><asp:Label ID="labelDataFim" runat="server" CssClass="labelRequired" Text="Fim:"/></td>
                    <td><dxe:ASPxDateEdit ID="textDataFim" runat="server" ClientInstanceName="textDataFim"/></td></tr>
                </table>
            </td>                                                                          
            </tr>
            
            <tr>                                   
                <td class="td_Label">
                    <asp:Label ID="labelTipoOrdenacao" runat="server" CssClass="labelNormal" Text="Ordena Por: " />
                </td>
                    
                <td colspan="3">
                    <dxe:ASPxComboBox ID="dropTipoOrdenacao" runat="server" CssClass="dropDownListCurto"
                        ClientInstanceName="dropTipoOrdenacao" SelectedIndex="0">
                    <Items>
                        <dxe:ListEditItem Value="0" Text="Por Código" />
                        <dxe:ListEditItem Value="1" Text="Por Nome" />
                    </Items>
                    </dxe:ASPxComboBox>
                </td>            
            </tr>
            

            
            
        </table>                                                                                 
        
        </div>
        
        <div id="reportLinkButton" class="linkButton linkButtonNoBorder">
           <asp:LinkButton ID="btnVisualiza" runat="server" Font-Overline="false" CssClass="btnReport" OnClick="btnVisualiza_Click"><asp:Literal ID="Literal1" runat="server" Text="Visualizar"/><div></div></asp:LinkButton>
           <asp:LinkButton ID="btnPDF" runat="server" Font-Overline="false" CssClass="btnPdf" OnClick="btnPDF_Click"><asp:Literal ID="Literal2" runat="server" Text="Gerar PDF"/><div></div></asp:LinkButton>
           <asp:LinkButton ID="btnExcel" runat="server" Font-Overline="false"  CssClass="btnExcel" OnClick="btnExcel_Click"><asp:Literal ID="Literal3" runat="server" Text="Gerar Excel"/><div></div></asp:LinkButton>
        </div>
        
    </ContentTemplate>
    </asp:UpdatePanel>
    
    </div>
    
    </div>
    </td></tr></table>
    </div>
        
    <cc1:esDataSource ID="EsDSCliente" runat="server" OnesSelect="EsDSCliente_esSelect" />
    <cc1:esDataSource ID="EsDSPapelRendaFixa" runat="server" OnesSelect="EsDSPapelRendaFixa_esSelect" />
        
   </form>
</body>
</html>