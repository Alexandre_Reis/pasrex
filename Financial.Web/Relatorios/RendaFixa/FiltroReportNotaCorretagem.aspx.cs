﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Financial.Fundo;
using Financial.Investidor;
using System.Collections.Generic;
using Financial.Web.Util;
using Financial.Investidor.Enums;
using Financial.Security;
using log4net;
using Financial.Security.Enums;
using EntitySpaces.Interfaces;
using DevExpress.Web;
using System.Text;
using Financial.Common;
using Financial.Web.Common;
using Financial.RendaFixa;
using Financial.Util;
using Financial.RendaFixa.Enums;
using Financial.Common.Enums;
using Financial.CRM;

public partial class FiltroReportNotaCorretagem : FiltroReportBasePage {
    private static readonly ILog log = LogManager.GetLogger(typeof(FiltroReportNotaCorretagem));

    new protected void Page_Load(object sender, EventArgs e) {
        btnEditCodigo.Focus();
        //
        this.HasPopupCliente = true;
        base.Page_Load(sender, e);

        TrataTravamentoCampos();
    }

    private void TrataTravamentoCampos() {
        Usuario usuario = new Usuario();
        usuario.BuscaUsuario(HttpContext.Current.User.Identity.Name);
        int tipoTrava = usuario.TipoTrava.Value;

        if (tipoTrava == (int)TipoTravaUsuario.TravaCliente) {
            btnEditCodigo.Enabled = false;

            PermissaoCliente permissaoCliente = new PermissaoCliente();
            int? idCliente = permissaoCliente.RetornaClienteAssociado(HttpContext.Current.User.Identity.Name);

            if (idCliente.HasValue) {
                Cliente cliente = new Cliente();

                List<esQueryItem> campos = new List<esQueryItem>();
                campos.Add(cliente.Query.Apelido);
                cliente.LoadByPrimaryKey(campos, idCliente.Value);
                string apelido = cliente.Apelido;

                btnEditCodigo.Text = Convert.ToString(idCliente.Value);
                textNome.Text = apelido;
            }
        }
    }

    protected void ASPxCallback1_Callback(object source, DevExpress.Web.CallbackEventArgs e) {
        string nome = "";
        if (!String.IsNullOrEmpty(e.Parameter) && e.Parameter != "null") {
            int idCliente = Convert.ToInt32(e.Parameter);
            Cliente cliente = new Cliente();
            cliente.LoadByPrimaryKey(idCliente);

            if (cliente.str.Apelido != "") {
                if (cliente.IsAtivo) {
                    PermissaoCliente permissaoCliente = new PermissaoCliente();
                    nome = permissaoCliente.RetornaAcessoClienteComControle(idCliente, HttpContext.Current.User.Identity.Name)
                            ? cliente.str.Apelido : "no_access";                                      
                }
                else {
                    nome = "no_active";
                }
            }
        }
        e.Result = nome;
    }

    protected void EsDSAgenteMercado_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e) {
        AgenteMercadoCollection agenteMercadoCollection = new AgenteMercadoCollection();
        //
        agenteMercadoCollection.Query
                        .Select(agenteMercadoCollection.Query.IdAgente, agenteMercadoCollection.Query.Nome)
                        .OrderBy(agenteMercadoCollection.Query.Nome.Ascending);

        agenteMercadoCollection.Query.Load();
        //
        AgenteMercado a = new AgenteMercado();
        a.IdAgente = 0;
        a.Nome = "";

        agenteMercadoCollection.AttachEntity(a);
        //
        agenteMercadoCollection.Sort = AgenteMercadoMetadata.ColumnNames.Nome + " ASC";
        //
        e.Collection = agenteMercadoCollection;
    }

    protected void EsDSCliente_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e) {
        ClienteCollection clienteCollection = new ClienteCollection();
        clienteCollection.BuscaClientesComAcesso(HttpContext.Current.User.Identity.Name);

        e.Collection = clienteCollection;
    }

    protected void EsDSAssessor_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e) {
        AssessorCollection coll = new AssessorCollection();
        coll.LoadAll();

        Assessor a = new Assessor();
        a.IdAssessor = null;
        a.Nome = "";

        coll.AttachEntity(a);
        //
        coll.Sort = AssessorMetadata.ColumnNames.Nome + " ASC";
        e.Collection = coll;
    }

    protected void EsDSGrupoAfinidade_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e) {
        GrupoAfinidadeCollection coll = new GrupoAfinidadeCollection();
        coll.LoadAll();

        GrupoAfinidade a = new GrupoAfinidade();
        a.IdGrupo = null;
        a.Descricao = "";

        coll.AttachEntity(a);
        //
        coll.Sort = GrupoAfinidadeMetadata.ColumnNames.Descricao + " ASC";

        e.Collection = coll;
    }

    protected void EsDSPapelRendaFixa_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e) {
        PapelRendaFixaCollection coll = new PapelRendaFixaCollection();
        coll.LoadAll();

        PapelRendaFixa a = new PapelRendaFixa();
        a.IdPapel = null;
        a.Descricao = "";

        coll.AttachEntity(a);
        //
        coll.Sort = PapelRendaFixaMetadata.ColumnNames.Descricao + " ASC";
        e.Collection = coll;
    }

    protected void btnVisualiza_Click(object sender, EventArgs e) {
        this.TrataErros();
        this.SelecionaRelatorio(VisaoRelatorio.visaoHTML);
    }

    protected void btnPDF_Click(object sender, EventArgs e) {
        this.TrataErros();
        this.SelecionaRelatorio(VisaoRelatorio.visaoPDF);
    }

    protected void btnExcel_Click(object sender, EventArgs e) {
        this.TrataErros();
        this.SelecionaRelatorio(VisaoRelatorio.visaoExcel);
    }

    /// <summary>
    /// Trata os Erros de Relatorio
    /// </summary>
    private void TrataErros() {

        #region Campos obrigatórios
        List<Control> controles = new List<Control>(new Control[] { 
    	    this.textDataInicio, this.textDataFim, this.dropAgente, this.dropCustodia });

        if (base.TestaObrigatorio(controles) != "") {
            throw new Exception("Campos com * são obrigatórios!");
        }

        if (this.dropAgente.SelectedIndex == 0 || this.dropCustodia.SelectedIndex == 0) {
            throw new Exception("Campos com * são obrigatórios!");
        }
        #endregion

        DateTime dataInicio = Convert.ToDateTime(this.textDataInicio.Text);
        DateTime dataFim = Convert.ToDateTime(this.textDataFim.Text);

        if (dataFim < dataInicio) {
            throw new Exception("Data Fim deve ser maior que Data Inicial.");
        }
        
        if (!this.RegistrosEncontrados()) {
            throw new Exception("Não há Notas de Corretagem para o período selecionado.");
        }
    }

    /// <summary>
    /// Retorna boleano indicando se tem Notas de Corretagem
    /// </summary>
    /// <returns></returns>
    private bool RegistrosEncontrados() {

        #region Cliente
        int? idCliente = null;

        if (!String.IsNullOrEmpty(this.btnEditCodigo.Text.Trim())) {
            idCliente = Convert.ToInt32(this.btnEditCodigo.Text.Trim());
        }

        List<int> listaIdClientes = new List<int>();
        if (idCliente.HasValue) {
            listaIdClientes.Add(idCliente.Value);
        }
        else {
            ClienteQuery c = new ClienteQuery("C");
            c.Select(c.IdCliente);

            ClienteCollection cliente = new ClienteCollection();
            cliente.Load(c);

            foreach (Cliente c1 in cliente) {
                listaIdClientes.Add(c1.IdCliente.Value);
            }
        }
        #endregion

        int? idAssessor = null;
        if (this.dropAssessor.SelectedItem.Value != null) { idAssessor = Convert.ToInt32(this.dropAssessor.SelectedItem.Value); }

        int? idGrupo = null;
        if (this.dropGrupoAfinidade.SelectedItem.Value != null) { idGrupo = Convert.ToInt32(this.dropGrupoAfinidade.SelectedItem.Value); }

        int? idPapel = null;
        if(this.dropPapel.SelectedItem.Value != null) { idPapel = Convert.ToInt32(this.dropPapel.SelectedItem.Value); }

        // Obrigatorios
        DateTime dataInicio = Convert.ToDateTime(textDataInicio.Text);
        DateTime dataFim = Convert.ToDateTime(textDataFim.Text);
        int idAgente = Convert.ToInt32(this.dropAgente.SelectedItem.Value);
        LocalCustodiaFixo idCustodia = (LocalCustodiaFixo)Convert.ToInt32(this.dropCustodia.SelectedItem.Value);
        //
        string cnpj = Utilitario.RemoveCaracteresEspeciais(textCPFCNPJ.Text);

        //#region Datas
        //List<DateTime> listaDatas = new List<DateTime>();

        //DateTime dataAux = dataInicio;
        //while (dataAux <= dataFim) {
        //    listaDatas.Add(dataAux);
        //    dataAux = Calendario.AdicionaDiaUtil(dataAux, 1, LocalFeriadoFixo.Brasil, TipoFeriado.Brasil);
        //}
        //#endregion

        //decimal total = 0;
        //for (int i = 0; i < listaIdClientes.Count; i++) {

        //    for (int j = 0; j < listaDatas.Count; j++) {
        //        NotaCorretagemRendaFixa n = new NotaCorretagemRendaFixa(listaIdClientes[i], listaDatas[j],
        //                                                   idAgente, idCustodia,
        //                                                   idAssessor, idGrupo, idPapel, cnpj);

        //        total += n.GetQuantidadeRegistros();
        //    }
        //}

        //return total != 0;

        #region Consulta OperacaoRendaFixa
        OperacaoRendaFixaCollection oprf = new OperacaoRendaFixaCollection();

        OperacaoRendaFixaQuery op = new OperacaoRendaFixaQuery("OP");
        TituloRendaFixaQuery t = new TituloRendaFixaQuery("T");
        ClienteRendaFixaQuery crf = new ClienteRendaFixaQuery("C");
        ClienteQuery clienteRF = new ClienteQuery("C1");
        PapelRendaFixaQuery papelRF = new PapelRendaFixaQuery("P");
        PessoaQuery pessoa = new PessoaQuery("P1");
                
        op.Select(op.IdOperacao);
        //
        op.InnerJoin(t).On(op.IdTitulo == t.IdTitulo);
        op.InnerJoin(crf).On(op.IdCliente == crf.IdCliente);
        op.InnerJoin(clienteRF).On(op.IdCliente == clienteRF.IdCliente);
        op.InnerJoin(papelRF).On(t.IdPapel == papelRF.IdPapel);
        op.InnerJoin(pessoa).On(op.IdCliente == pessoa.IdPessoa);
        //
        op.Where(op.IdCliente.In(listaIdClientes),
                 op.DataOperacao.Between(dataInicio, dataFim),
                 op.IdAgenteCorretora == idAgente,
                 op.IdCustodia == (byte)idCustodia,
                 op.TipoOperacao.In((byte)TipoOperacaoTitulo.CompraFinal, (byte)TipoOperacaoTitulo.VendaFinal));

        if (idAssessor.HasValue) {            
            op.Where(crf.IdAssessor == idAssessor);
        }

        if (idGrupo.HasValue) {
            op.Where(clienteRF.IdGrupoAfinidade == idGrupo);
        }

        if (idPapel.HasValue) {
            op.Where(papelRF.IdPapel == idPapel);
        }

        if (!String.IsNullOrEmpty(cnpj)) {
            op.Where(pessoa.Cpfcnpj.Like(cnpj));
        }
        oprf.Load(op);

        #endregion

        return oprf.Count != 0;
    }

    /// <summary>
    /// Seleciona o relatorio a ser executado
    /// </summary>
    /// <param name="visao">PDF, Excel ou Report</param>
    private void SelecionaRelatorio(string visao) {
        Response.BufferOutput = true;
        //
        // Salva os parametros na Session
        Session["btnEditCodigo"] = this.btnEditCodigo.Text.Trim();
        Session["dropAssessor"] = this.dropAssessor.SelectedItem.Value != null ? Convert.ToString(this.dropAssessor.SelectedItem.Value) : null;
        Session["dropGrupoAfinidade"] = this.dropGrupoAfinidade.SelectedItem.Value != null ? Convert.ToString(this.dropGrupoAfinidade.SelectedItem.Value) : null;
        Session["dropPapel"] = this.dropPapel.SelectedItem.Value != null ? Convert.ToString(this.dropPapel.SelectedItem.Value) : null;
        
        //Obrigatórios
        Session["dropAgente"] = Convert.ToString(this.dropAgente.SelectedItem.Value);
        Session["dropCustodia"] = Convert.ToString(this.dropCustodia.SelectedItem.Value);
        //
        Session["textDataInicio"] = textDataInicio.Text;
        Session["textDataFim"] = textDataFim.Text;
        //
        Session["textCPFCNPJ"] = Utilitario.RemoveCaracteresEspeciais(textCPFCNPJ.Text); 
        //
        //Response.Redirect("~/Relatorios/RendaFixa/ReportNotaCorretagem.aspx?Visao=" + visao);

        string url = "~/Relatorios/RendaFixa/ReportNotaCorretagem.aspx?Visao=" + visao;
        string redirectURL = Page.ResolveClientUrl(url);

        string script = "window.location = '" + redirectURL + "';";
        ScriptManager.RegisterStartupScript(this, typeof(Page), "RedirectTo", script, true);
    }
}