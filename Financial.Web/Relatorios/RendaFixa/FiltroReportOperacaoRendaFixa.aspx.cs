﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Collections.Generic;
using System.Web.UI.HtmlControls;
using System.Text;

using Financial.Fundo;
using Financial.Investidor;
using Financial.Web.Util;
using Financial.Investidor.Enums;
using Financial.Security;

using log4net;
using Financial.Security.Enums;

using EntitySpaces.Interfaces;

using DevExpress.Web;
using Financial.Web.Common;
using Financial.RendaFixa;
using System.Threading;

public partial class FiltroReportOperacaoRendaFixa : FiltroReportBasePage {
    private static readonly ILog log = LogManager.GetLogger(typeof(FiltroReportOperacaoRendaFixa));

    // TODO: Traduzir
    const string MSG_ERRO = "Data Início deve ser Menor que a Data Fim";
    
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    new protected void Page_Load(object sender, EventArgs e) {
        btnEditCodigo.Focus();
        //    
        this.HasPopupCliente = true;
        base.Page_Load(sender, e);

        this.PopulaPeriodoDatas(this.dropTipoPeriodo);

        this.TrataTravamentoCampos();
    }

    /// <summary>
    /// 
    /// </summary>
    private void TrataTravamentoCampos() {
        Usuario usuario = new Usuario();
        usuario.BuscaUsuario(HttpContext.Current.User.Identity.Name);
        int tipoTrava = usuario.TipoTrava.Value;

        if (tipoTrava == (int)TipoTravaUsuario.TravaCliente) {
            btnEditCodigo.Enabled = false;

            PermissaoCliente permissaoCliente = new PermissaoCliente();
            int? idCliente = permissaoCliente.RetornaClienteAssociado(HttpContext.Current.User.Identity.Name);

            if (idCliente.HasValue) {
                Cliente cliente = new Cliente();

                List<esQueryItem> campos = new List<esQueryItem>();
                campos.Add(cliente.Query.Apelido);
                cliente.LoadByPrimaryKey(campos, idCliente.Value);
                string apelido = cliente.Apelido;

                btnEditCodigo.Text = Convert.ToString(idCliente.Value);
                textNome.Text = apelido;
            }
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void ASPxCallback1_Callback(object source, DevExpress.Web.CallbackEventArgs e) {
        string nome = "";
        if (!String.IsNullOrEmpty(e.Parameter) && e.Parameter != "null") {
            int idCliente = Convert.ToInt32(e.Parameter);
            Cliente cliente = new Cliente();
            cliente.LoadByPrimaryKey(idCliente);

            if (cliente.str.Apelido != "") {
                if (cliente.IsAtivo) {
                    PermissaoCliente permissaoCliente = new PermissaoCliente();
                    nome = permissaoCliente.RetornaAcessoClienteComControle(idCliente, HttpContext.Current.User.Identity.Name)
                            ? cliente.str.Apelido : "no_access";
                }
                else {
                    nome = "no_active";
                }
            }
        }
        e.Result = nome;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void callBackPeriodo_Callback(object source, DevExpress.Web.CallbackEventArgs e)
    {
        e.Result = "";
        if (btnEditCodigo.Text != "")
        {
            int idCliente = Convert.ToInt32(btnEditCodigo.Text);

            Cliente cliente = new Cliente();

            DateTime dataInicio = new DateTime();
            DateTime dataFim = new DateTime();

            if (dropTipoPeriodo.SelectedIndex > 0)
            {
                cliente.RetornaDatas(idCliente, (TipoPeriodoDatas)dropTipoPeriodo.SelectedIndex, ref dataInicio, ref dataFim);

                e.Result = dataInicio.ToShortDateString() + "|" + dataFim.ToShortDateString() + "|" + Thread.CurrentThread.CurrentCulture.Name;
            }
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void EsDSCliente_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e) {
        ClienteCollection clienteCollection = new ClienteCollection();
        clienteCollection.BuscaClientesComAcesso(HttpContext.Current.User.Identity.Name);

        e.Collection = clienteCollection;
    }

    protected void EsDSPapelRendaFixa_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        PapelRendaFixaCollection papelRendaFixaCollection = new PapelRendaFixaCollection();
        //
        papelRendaFixaCollection.Query
                        .Select(papelRendaFixaCollection.Query.IdPapel, papelRendaFixaCollection.Query.Descricao)
                        .OrderBy(papelRendaFixaCollection.Query.Descricao.Ascending);

        papelRendaFixaCollection.Query.Load();
        //
        PapelRendaFixa p = new PapelRendaFixa();
        p.IdPapel = 0;
        p.Descricao = "";

        papelRendaFixaCollection.AttachEntity(p);
        //
        papelRendaFixaCollection.Sort = PapelRendaFixaMetadata.ColumnNames.Descricao + " ASC";
        //
        e.Collection = papelRendaFixaCollection;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnVisualiza_Click(object sender, EventArgs e) {
        this.TrataErros();
        this.SelecionaRelatorio(VisaoRelatorio.visaoHTML);
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnPDF_Click(object sender, EventArgs e) {
        this.TrataErros();
        this.SelecionaRelatorio(VisaoRelatorio.visaoPDF);
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnExcel_Click(object sender, EventArgs e) {
        this.TrataErros();
        this.SelecionaRelatorio(VisaoRelatorio.visaoExcel);
    }

    /// <summary>
    /// Trata os Erros de Relatorio
    /// </summary>
    /// <exception cref="Exception"> - Data Inicio Menor que Data Fim
    ///                              - Se for Escolhido um Cliente e
    ///                              - Data passada Anterior à data de implantação da Carteira
    ///                              - Data passada posterior à data atual da carteira
    /// </exception>
    private void TrataErros() {
        #region Campos obrigatórios
        List<Control> controles = new List<Control>(new Control[] { 
    	    this.textDataInicio, this.textDataFim });

        if (base.TestaObrigatorio(controles) != "") {
            throw new Exception("Campos com * são obrigatórios!");
        }
        #endregion
                        
        string idCliente = this.btnEditCodigo.Text;
        DateTime dataInicio = Convert.ToDateTime(this.textDataInicio.Text);
        DateTime dataFim = Convert.ToDateTime(this.textDataFim.Text);

        if (dataInicio > dataFim) {
            throw new Exception(MSG_ERRO);
        }        
    }

    /// <summary>
    /// Seleciona o relatorio a ser executado
    /// </summary>
    /// <param name="visao">PDF, Excel ou Report</param>
    private void SelecionaRelatorio(string visao) {
        Response.BufferOutput = true;
        //
        // Salva os Parametros na Session
        Session["textDataInicio"] = textDataInicio.Text;
        Session["textDataFim"] = textDataFim.Text;
        Session["btnEditCodigo"] = btnEditCodigo.Text;
        Session["dropTipoOrdenacao"] = this.dropTipoOrdenacao.SelectedIndex;

        Session["textIdTitulo"] = textIdTitulo.Text;
        
        Session["dropTipoOperacao"] = this.dropTipoOperação.SelectedItem.Value;
        Session["dropClearing"] = this.dropClearing.SelectedItem.Value;

        //this.dropTipoOrdenacao.SelectedItem.Value
        //Session[] = this.droptdropTipoOperação.SelectedValue
        
        string idPapel = null;
        if (dropPapel.SelectedIndex > 0)
        {
            idPapel = Convert.ToString(dropPapel.SelectedItem.Value);
        }
        Session["dropPapel"] = idPapel;
       
        //Response.Redirect("~/Relatorios/RendaFixa/ReportOperacaoRendaFixa.aspx?Visao=" + visao);

        string url = "~/Relatorios/RendaFixa/ReportOperacaoRendaFixa.aspx?Visao=" + visao;
        string redirectURL = Page.ResolveClientUrl(url);

        string script = "window.location = '" + redirectURL + "';";
        ScriptManager.RegisterStartupScript(this, typeof(Page), "RedirectTo", script, true);
    }
}