<%@ WebHandler Language="C#" Class="ReportProjecaoTributoRendaFixa" %>

using System;
using System.Web;
using System.Text;
using Bytescout.Spreadsheet;
using System.IO;
using System.Collections.Generic;
using Financial.Fundo;
using Financial.Investidor;
using Financial.InvestidorCotista;
using Financial.Fundo.Enums;
using Financial.InvestidorCotista.Debug.PosicaoCotista;
using Financial.Tributo.Debug.CalculoTributo;
using Financial.Fundo.Debug.HistoricoCota;

public class ReportProjecaoTributoRendaFixa : IHttpHandler
{

    public void ProcessRequest(HttpContext context)
    {

        try
        {
            /*PARAMETROS*/
            int idPosicao = Convert.ToInt32(context.Request["idposicao"]);

            PosicaoCotista posicaoCotista = new PosicaoCotista();
            posicaoCotista.LoadByPrimaryKey(idPosicao);

            string dataParam = context.Request["data"];
            DateTime data;
            if (string.IsNullOrEmpty(dataParam))
            {
                Cliente cliente = new Cliente();
                cliente.LoadByPrimaryKey(posicaoCotista.IdCarteira.Value);
                data = cliente.DataDia.Value;
            }
            else
            {
                string[] dateParts = dataParam.Split('-');
                int ano = Convert.ToInt32(dateParts[0]);
                int mes = Convert.ToInt32(dateParts[1]);
                int dia = Convert.ToInt32(dateParts[2]);
                data = new DateTime(ano, mes, dia);
            }

            Carteira carteira = new Carteira();
            carteira.LoadByPrimaryKey(posicaoCotista.IdCarteira.Value);

            List<DebugAtualizaProjecaoTributoRendaFixa> debugInfoList =
                new PosicaoCotista().DebugAtualizaProjecaoTributoRendaFixa(posicaoCotista, data);

            string csv = "";
            foreach (DebugAtualizaProjecaoTributoRendaFixa debugInfo in debugInfoList)
            {
                csv += FormatDebugInfoToCSV(debugInfo);
            }

            HttpResponse response = HttpContext.Current.Response;
            response.ClearContent();
            response.Clear();
            byte[] bytes = Encoding.ASCII.GetBytes(csv);
            using (MemoryStream stream = new MemoryStream(bytes))
            {
                response.AddHeader("Content-Disposition", "attachment; filename=memoria_tributo_rf.csv");
                response.AddHeader("Content-Length", stream.Length.ToString());
                response.ContentType = "text/plain";
                stream.WriteTo(response.OutputStream);
            }
        }
        catch (Exception e)
        {
            HttpResponse response = HttpContext.Current.Response;
            StringBuilder html = new StringBuilder(); ;
            html.Append("<HTML>");
            html.Append("<HEAD>");
            html.Append("<script type=\"text/javascript\">");
            html.Append("alert('" + e.Message + "')");
            html.Append("</script>");
            html.Append("</HEAD>");
            html.Append("<BODY>");
            html.Append("</BODY>");
            html.Append("</HTML>");

            context.Response.ContentType = "text/html";
            context.Response.Write(html.ToString());

        }

    }

    string FormatDebugInfoToCSV(DebugAtualizaProjecaoTributoRendaFixa debugInfo)
    {
        StringBuilder csv = new StringBuilder();

        string line;

        line = "IdCarteira;{0};;QtdeAntesCorte;{1};;DataAplicacao;{2};;AliquotaDiferenciada;{3}";
        csv.AppendFormat(line,
            debugInfo.IdCarteira.Value.ToString(),
            debugInfo.QuantidadeAntesCorte.Value.ToString(),
            debugInfo.DataAplicacao.Value.ToString(),
            debugInfo.AliquotaDiferenciada.Value.ToString());
        csv.AppendLine();

        line = "IdPosicao;{0};;QtdePosicao;{1};;DataConversao;{2};;AliquotaIR;{3}";
        csv.AppendFormat(line,
            debugInfo.IdPosicao.Value.ToString(),
            debugInfo.QuantidadePosicao.Value.ToString(),
            debugInfo.DataConversao.Value.ToString(),
            debugInfo.DebugCalculaIRFundoRendaFixa.AliquotaIR.Value.ToString());
        csv.AppendLine();


        line = "IdCotista;{0};;QtdeCalcular;{1};;DataCalculo;{2};;AliquotaComeCotas;{3}";

        csv.AppendFormat(line,
            debugInfo.IdCotista.Value.ToString(),
            debugInfo.QuantidadeCalcular.Value.ToString(),
            debugInfo.DebugCalculaIRFundoRendaFixa.DataCalculo.Value.ToString(),
            debugInfo.DebugCalculaIRFundoRendaFixa.AliquotaComeCotas.Value.ToString());
        csv.AppendLine();


        line = ";;;;;;DiasTotal;{0};;;";

        csv.AppendFormat(line,
            debugInfo.DiasTotal.Value.ToString());
        csv.AppendLine();


        line = "IsFDIC;{0};;ValorBruto;{1};;DiasLiquidResgate;{2};;ValRendComeCotas;{3}";

        csv.AppendFormat(line,
            debugInfo.DebugCalculaIRFundoRendaFixa.IsFDIC.Value.ToString(),
            debugInfo.ValorBruto.Value.ToString(),
            debugInfo.DiasLiquidResgate.Value.ToString(),
            debugInfo.ValorRendimentoComeCotas.Value.ToString());
        csv.AppendLine();


        line = "TipoTributacao;{0};;ValorIR;{1};;DiasLiquidComeCotas;{2};;IRDescontarFDIC;{3}";

        csv.AppendFormat(line,
            debugInfo.TipoTributacao.Value.ToString(),
            debugInfo.ValorIR.Value.ToString(),
            debugInfo.DiasLiquidResgate.Value.ToString(),
            debugInfo.IRDescontarFDIC.Value.ToString());
        csv.AppendLine();


        line = "TipoCota;{0};;ValorIOF;{1};;;;;;";

        csv.AppendFormat(line,
            debugInfo.TipoCota.Value.ToString(),
            debugInfo.ValorIOF.Value.ToString());
        csv.AppendLine();


        line = ";;;ValorLiquido;{0};;CotaDia;{1};;TotalRendimento;{2}";

        csv.AppendFormat(line,
            debugInfo.ValorLiquido.Value.ToString(),
            debugInfo.CotaDia.Value.ToString(),
            debugInfo.DebugCalculaIRFundoRendaFixa.TotalRendimento.Value.ToString());
        csv.AppendLine();


        line = "IsIsentoIR;{0};;;;;CotaDiaCalculo;{1};;TotalRendimentoCompensado;{2}";

        csv.AppendFormat(line,
            debugInfo.IsIsentoIR.Value.ToString(),
            debugInfo.CotaDiaCalculo.Value.ToString(),
            debugInfo.DebugCalculaIRFundoRendaFixa.TotalRendimentoCompensado.Value.ToString());
        csv.AppendLine();


        line = "IsIsentoIOF;{0};;ValorIOF2;{1};;CotaAplicacao;{2};;RendimentoAnterior;{3}";

        csv.AppendFormat(line,
            debugInfo.IsIsentoIOF.Value.ToString(),
            debugInfo.DebugCalculaIRFundoRendaFixa.ValorIOF.Value.ToString(),
            debugInfo.CotaAplicacao.Value.ToString(),
            debugInfo.DebugCalculaIRFundoRendaFixa.RendimentoAnterior.Value.ToString());
        csv.AppendLine();


        line = "CalculaIOF;{0};;ValorIR2;{1};;CotaAplicCalculo;{2};;RendimentoPosterior;{3}";

        csv.AppendFormat(line,
            debugInfo.CalculaIOF.Value.ToString(),
            debugInfo.DebugCalculaIRFundoRendaFixa.ValorIR.Value.ToString(),
            debugInfo.CotaAplicacaoCalculo.Value.ToString(),
            debugInfo.DebugCalculaIRFundoRendaFixa.RendimentoPosterior.Value.ToString());
        csv.AppendLine();


        line = ";;;;;;;;;RendimentoComeCotas;{0}";

        csv.AppendFormat(line,
            debugInfo.DebugCalculaIRFundoRendaFixa.RendimentoComeCotas.Value.ToString());
        csv.AppendLine();


        line = ";;;;;;;;;CotaUltimoComeCotas;{0}";

        csv.AppendFormat(line,
            debugInfo.DebugCalculaIRFundoRendaFixa.CotaUltimoComeCotas.Value.ToString());
        csv.AppendLine();


        csv.AppendLine();


        line = ";;;;;;;;;PrejuizoCompensar;{0}";

        csv.AppendFormat(line,
            debugInfo.DebugCalculaIRFundoRendaFixa.PrejuizoCompensar.Value.ToString());
        csv.AppendLine();


        line = ";;;;;;;;;TotalPrejuizoUsado;{0}";

        csv.AppendFormat(line,
            debugInfo.DebugCalculaIRFundoRendaFixa.TotalPrejuizoUsado.Value.ToString());
        csv.AppendLine();


        csv.AppendLine();

        if (debugInfo.DebugCotaAplicacaoRecompostaList.Count > 0)
        {
            csv.Append("Calc. Cota Aplic Amort.");
            csv.AppendLine();

            csv.Append("Data Evento;Cota Pre;Cota Pos;Valor Evento;TipoEvento");
            csv.AppendLine();
            foreach (DebugCotaRecomposta debugCotaRecomposta in debugInfo.DebugCotaAplicacaoRecompostaList)
            {
                string cotaPreEvento = debugCotaRecomposta.CotaPreEvento.HasValue ? 
                    debugCotaRecomposta.CotaPreEvento.Value.ToString() : "";
                string cotaPosEvento = debugCotaRecomposta.CotaPosEvento.HasValue ?
                    debugCotaRecomposta.CotaPosEvento.Value.ToString() : "";
                
                string[] values = new string[5]{debugCotaRecomposta.DataEvento.Value.ToString(), 
                      cotaPreEvento,
                      cotaPosEvento, 
                      debugCotaRecomposta.ValorEvento.Value.ToString(),
                      debugCotaRecomposta.TipoEvento.Value.ToString()};
                csv.Append(String.Join(";", values));
                csv.AppendLine();
            }
            csv.AppendLine();
        }

        if (debugInfo.DebugCotaDiaRecompostaList.Count > 0)
        {
            csv.Append("Calc. Cota Dia Amort.");
            csv.AppendLine();

            csv.Append("Data Evento;Cota Pre;Cota Pos;Valor Evento;TipoEvento");
            csv.AppendLine();

            foreach (DebugCotaRecomposta debugCotaRecomposta in debugInfo.DebugCotaDiaRecompostaList)
            {
                string cotaPreEvento = debugCotaRecomposta.CotaPreEvento.HasValue ?
                    debugCotaRecomposta.CotaPreEvento.Value.ToString() : "";
                string cotaPosEvento = debugCotaRecomposta.CotaPosEvento.HasValue ?
                    debugCotaRecomposta.CotaPosEvento.Value.ToString() : "";
                
                string[] values = new string[5]{debugCotaRecomposta.DataEvento.Value.ToString(), 
                      cotaPreEvento,
                      cotaPosEvento, 
                      debugCotaRecomposta.ValorEvento.Value.ToString(),
                      debugCotaRecomposta.TipoEvento.Value.ToString()
                };

                csv.Append(String.Join(";", values));
                csv.AppendLine();
            }
            csv.AppendLine();

        }


        DebugCalculaIRFundoRendaFixa debugCalculaIR = debugInfo.DebugCalculaIRFundoRendaFixa;
        if (debugCalculaIR.DebugComeCotasList.Count > 0)
        {
            string header = "Calculo come cotas";
            csv.Append(header);
            csv.AppendLine();

            //DataFocal;CotaFocal;QtdeFocal;renad?;ir come cotas;ir resid;total ir resid;

            header = "DataFocal;CotaFocal;QtdeFocal;Rend.Compensado;IRComeCotas;IRRestoPagar;TotalIRRestoPagar;;;";

            header += "DataProxComeCotas;DataAnterior;ValorIOFAnterior;";
            header += "RendimentoAnterior;UltimoIRCheio;CotaComeCotasAnterior;IsDataComeCotas;";
            header += "QtdeComeCotas;IRCheio;PrejuizoUsadoComeCotas;";
            header += "DataComeCotasPosterior;ResidualRendimento;ResidualIRCompensado";
            csv.Append(header);
            csv.AppendLine();

            foreach (DebugComeCotas debugComeCotas in debugCalculaIR.DebugComeCotasList)
            {
                string[] values = new string[22] { 
                    debugComeCotas.DataFocal.Value.ToString(),
                    debugComeCotas.CotaFocal.Value.ToString(),      
                    debugComeCotas.QuantidadeFocal.Value.ToString(),
                    debugComeCotas.RendimentoCompensado.Value.ToString(),
                    debugComeCotas.IRComeCotas.Value.ToString(),
                    debugComeCotas.IRRestoPagar.Value.ToString(),
                    debugComeCotas.TotalIRRestoPagar.Value.ToString(),
                    "",
                    "",
                    
                    debugComeCotas.DataProximoComeCotas.Value.ToString(),
                    debugComeCotas.DataAnterior.Value.ToString(), 
                    debugComeCotas.ValorIOFAnterior.Value.ToString(),
                    debugComeCotas.RendimentoAnterior.Value.ToString(), 
                    debugComeCotas.UltimoIRCheio.Value.ToString(),
                    debugComeCotas.CotaComeCotasAnterior.Value.ToString(), 
                    debugComeCotas.IsDataComeCotas.Value.ToString(),
                    debugComeCotas.QuantidadeComeCotas.Value.ToString(),
                    debugComeCotas.IRCheio.Value.ToString(),
                    debugComeCotas.PrejuizoUsadoComeCotas.Value.ToString(), 
                    debugComeCotas.DataComeCotasPosterior.Value.ToString(),
                    debugComeCotas.ResidualRendimento.Value.ToString(), 
                    debugComeCotas.ResidualIRCompensado.Value.ToString()
              };
                csv.Append(String.Join(";", values));
                csv.AppendLine();

            }
        }

        csv.AppendLine();
        csv.AppendLine();
        csv.AppendLine();

        return csv.ToString();
    }

    public bool IsReusable
    {
        get
        {
            return false;
        }
    }

}