using System;
using Financial.Relatorio;
using System.IO;
using System.Web;
using System.Globalization;
using Financial.Web.Common;
using System.Collections.Generic;
using Financial.Investidor;
using Financial.RendaFixa.Enums;
using Financial.Util;
using Financial.Common.Enums;

public partial class _ReportNotaCorretagem : BasePage {        
    new protected void Page_Load(object sender, EventArgs e) {
        
        string visao = Request.QueryString["Visao"].ToString();
        //        
        // Obrigatorios
        DateTime dataInicio = Convert.ToDateTime(Session["textDataInicio"]);
        DateTime dataFim = Convert.ToDateTime(Session["textDataFim"]);
        int idAgente = Convert.ToInt32(Session["dropAgente"]);
        LocalCustodiaFixo idCustodia = (LocalCustodiaFixo)Convert.ToInt32(Session["dropCustodia"]);
        //

        #region Cliente
        int? idCliente = null;        

        if (!String.IsNullOrEmpty((string)Session["btnEditCodigo"])) {
            idCliente = Convert.ToInt32(Session["btnEditCodigo"]);
        }

        List<int> listaIdClientes = new List<int>();
        if (idCliente.HasValue) {
            listaIdClientes.Add(idCliente.Value);
        }
        else {
            ClienteQuery c = new ClienteQuery("C");
            c.Select(c.IdCliente);

            ClienteCollection cliente = new ClienteCollection();
            cliente.Load(c);

            foreach (Cliente c1 in cliente) {
                listaIdClientes.Add(c1.IdCliente.Value);
            }
        }
        #endregion

        int? idAssessor = null;
        if (!String.IsNullOrEmpty((string)Session["dropAssessor"])) {
            idAssessor = Convert.ToInt32(Session["dropAssessor"]);
        }
        int? idGrupo = null;
        if (!String.IsNullOrEmpty((string)Session["dropGrupoAfinidade"])) {
            idGrupo = Convert.ToInt32(Session["dropGrupoAfinidade"]);
        }
        int? idPapel = null;
        if (!String.IsNullOrEmpty((string)Session["dropPapel"])) {
            idPapel = Convert.ToInt32(Session["dropPapel"]);
        }

        string cnpj = Convert.ToString(Session["textCPFCNPJ"]);

        #region Datas
        List<DateTime> listaDatas = new List<DateTime>();

        DateTime dataAux = dataInicio;
        while (dataAux <= dataFim) {
            listaDatas.Add(dataAux);
            dataAux = Calendario.AdicionaDiaUtil(dataAux, 1, LocalFeriadoFixo.Brasil, TipoFeriado.Brasil);
        }
        #endregion

        if (visao == "PDF") {
            #region PDF

            ReportMaster reportMaster = new ReportMaster();
            reportMaster.CreateDocument();
            reportMaster.PrintingSystem.ContinuousPageNumbering = false;
            //

            for (int i = 0; i < listaIdClientes.Count; i++) {

                for (int j = 0; j < listaDatas.Count; j++) {
                    Financial.RendaFixa.Custom1.NotaCorretagemRendaFixa n = new Financial.RendaFixa.Custom1.NotaCorretagemRendaFixa(listaIdClientes[i], listaDatas[j],
                                                                            idAgente, idCustodia,
                                                                            idAssessor, idGrupo, idPapel, cnpj);

                    if (n.GetQuantidadeRegistros() != 0) {

                        ReportNotaCorretagem report = new ReportNotaCorretagem(n);
                        report.CreateDocument();

                        // Anexa o Conteudo do Relatorio no Relatorio Master
                        reportMaster.Pages.AddRange(report.PrintingSystem.Pages);
                    }
                }
            }
                                                  
            MemoryStream ms = new MemoryStream();
            reportMaster.ExportToPdf(ms);
            ms.Seek(0, SeekOrigin.Begin);
            Response.ClearContent();
            Response.ClearHeaders();
            Response.Buffer = true;
            Response.Cache.SetCacheability(HttpCacheability.Private);
            Response.ContentType = "application/pdf";
            Response.AddHeader("Content-Length", ms.Length.ToString(CultureInfo.CurrentCulture));
            Response.AddHeader("Content-Disposition", String.Format("attachment;filename=ReportNotaCorretagem.pdf"));

            Response.BinaryWrite(ms.ToArray());
            ms.Close();
            HttpContext.Current.ApplicationInstance.CompleteRequest();
            Response.End();
            #endregion
        }
    }
}