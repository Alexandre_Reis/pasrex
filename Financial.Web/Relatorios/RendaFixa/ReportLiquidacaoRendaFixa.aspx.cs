using System;
using Financial.Relatorio;
using System.IO;
using System.Web;
using System.Globalization;
using Financial.Web.Common;
using System.Collections.Generic;
using Financial.Investidor;
using Financial.RendaFixa.Enums;
using Financial.Util;
using Financial.Common.Enums;

public partial class _ReportLiquidacaoRendaFixa : BasePage {
    private static class VisaoRelatorio {
        public static string visaoHTML = "Report";
        public static string visaoPDF = "PDF";
        public static string visaoExcel = "Excel";
    }

    new protected void Page_Load(object sender, EventArgs e) {        
        string visao = Request.QueryString["Visao"].ToString();

        #region Parametros
        int? idCliente = null;
        if (!String.IsNullOrEmpty((string)Session["btnEditCodigo"])) {
            idCliente = Convert.ToInt32(Session["btnEditCodigo"]);
        }

        int? idAssessor = null;
        if (!String.IsNullOrEmpty((string)Session["dropAssessor"])) {
            idAssessor = Convert.ToInt32(Session["dropAssessor"]);
        }

        int? idGrupoAfinidade = null;
        if (!String.IsNullOrEmpty((string)Session["dropGrupoAfinidade"])) {
            idGrupoAfinidade = Convert.ToInt32(Session["dropGrupoAfinidade"]);
        }

        int? idClasse = null;
        if (!String.IsNullOrEmpty((string)Session["dropPapel"])) {
            idClasse = Convert.ToInt32(Session["dropPapel"]);
        }

        byte? idCustodia = null;
        if (!String.IsNullOrEmpty((string)Session["dropCustodia"])) {
            idCustodia = Convert.ToByte(Session["dropCustodia"]);
        }

        int? tipoPessoa = null;
        if (!String.IsNullOrEmpty((string)Session["dropTipoPessoa"])) {
            tipoPessoa = Convert.ToInt32(Session["dropTipoPessoa"]);
        }

        // Obrigatorios
        DateTime dataInicio = Convert.ToDateTime(Session["textDataInicio"]);
        DateTime dataFim = Convert.ToDateTime(Session["textDataFim"]);
        //
        //int idAgente = Convert.ToInt32(Session["dropAgente"]);

        string cpfCNPJ = Convert.ToString(Session["textCPFCNPJ"]);

        #endregion

        if (visao == VisaoRelatorio.visaoHTML) {
            ReportViewer1.Report = new ReportLiquidacaoRendaFixa(idCliente, idAssessor, idGrupoAfinidade, idClasse, idCustodia, 
                                                                 tipoPessoa, dataInicio, dataFim, cpfCNPJ);
        }
        else if (visao == VisaoRelatorio.visaoPDF) {
            #region PDF

            ReportLiquidacaoRendaFixa report = new ReportLiquidacaoRendaFixa(idCliente, idAssessor, idGrupoAfinidade, idClasse, idCustodia,
                                                                tipoPessoa, dataInicio, dataFim, cpfCNPJ);

            MemoryStream ms = new MemoryStream();
            report.ExportToPdf(ms);
            ms.Seek(0, SeekOrigin.Begin);
            Response.ClearContent();
            Response.ClearHeaders();
            Response.Buffer = true;
            Response.Cache.SetCacheability(HttpCacheability.Private);
            Response.ContentType = "application/pdf";
            Response.AddHeader("Content-Length", ms.Length.ToString(CultureInfo.CurrentCulture));
            Response.AddHeader("Content-Disposition", String.Format("attachment;filename=ReportLiquidacaoRF.pdf"));

            Response.BinaryWrite(ms.ToArray());
            ms.Close();
            HttpContext.Current.ApplicationInstance.CompleteRequest();
            Response.End();
            #endregion
        }
        else if (visao == VisaoRelatorio.visaoExcel) {
            #region Excel

            ReportLiquidacaoRendaFixa report = new ReportLiquidacaoRendaFixa(idCliente, idAssessor, idGrupoAfinidade, idClasse, idCustodia,
                                                               tipoPessoa, dataInicio, dataFim, cpfCNPJ);

            MemoryStream ms = new MemoryStream();
            report.ExportToXls(ms);
            ms.Seek(0, SeekOrigin.Begin);
            Response.ClearContent();
            Response.ClearHeaders();
            Response.Buffer = true;
            Response.Cache.SetCacheability(HttpCacheability.Private);
            Response.ContentType = "application/vnd.ms-excel";
            Response.AddHeader("Content-Length", ms.Length.ToString(CultureInfo.CurrentCulture));
            Response.AddHeader("Content-Disposition", String.Format("attachment;filename=ReportLiquidacaoRF.xls"));

            Response.BinaryWrite(ms.ToArray());
            ms.Close();
            HttpContext.Current.ApplicationInstance.CompleteRequest();
            Response.End();

            #endregion
        }
   }
}