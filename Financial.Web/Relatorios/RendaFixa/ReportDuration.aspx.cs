using System;
using Financial.Relatorio;
using System.IO;
using System.Web;
using System.Globalization;
using Financial.Web.Common;

public partial class _ReportDuration : BasePage {
    private static class VisaoRelatorio {
        public static string visaoHTML = "Report";
        public static string visaoPDF = "PDF";
        public static string visaoExcel = "Excel";
    }

    new protected void Page_Load(object sender, EventArgs e) {
        string visao = Request.QueryString["Visao"].ToString();

        // Obrigatorio
        DateTime dataInicio = Convert.ToDateTime(Session["textDataInicio"]);

        // Opcional
        int? idCliente = null;
        if (!String.IsNullOrEmpty((string)Session["btnEditCodigo"])) {
            idCliente = Convert.ToInt32(Session["btnEditCodigo"]);
        }

        int? idTitulo = null;
        if (!String.IsNullOrEmpty((string)Session["btnTitulo"])) {
            idTitulo = Convert.ToInt32(Session["btnTitulo"]);
        }            
   
        if (visao == VisaoRelatorio.visaoHTML) {
            ReportViewer1.Report = new ReportDuration(idCliente, idTitulo, dataInicio);
        }
        else if (visao == VisaoRelatorio.visaoPDF) {
            #region PDF
            ReportDuration report = new ReportDuration(idCliente, idTitulo, dataInicio);

            MemoryStream ms = new MemoryStream();
            report.ExportToPdf(ms);
            ms.Seek(0, SeekOrigin.Begin);
            Response.ClearContent();
            Response.ClearHeaders();
            Response.Buffer = true;
            Response.Cache.SetCacheability(HttpCacheability.Private);
            Response.ContentType = "application/pdf";
            Response.AddHeader("Content-Length", ms.Length.ToString(CultureInfo.CurrentCulture));
            Response.AddHeader("Content-Disposition", String.Format("attachment;filename=ReportDuration.pdf"));

            Response.BinaryWrite(ms.ToArray());
            ms.Close();
            HttpContext.Current.ApplicationInstance.CompleteRequest();
            Response.End();
            #endregion
        }
        else if (visao == VisaoRelatorio.visaoExcel) {
            #region Excel
            ReportDuration report = new ReportDuration(idCliente, idTitulo, dataInicio);

            MemoryStream ms = new MemoryStream();
            report.ExportToXls(ms);
            ms.Seek(0, SeekOrigin.Begin);
            Response.ClearContent();
            Response.ClearHeaders();
            Response.Buffer = true;
            Response.Cache.SetCacheability(HttpCacheability.Private);
            Response.ContentType = "application/vnd.ms-excel";
            Response.AddHeader("Content-Length", ms.Length.ToString(CultureInfo.CurrentCulture));
            Response.AddHeader("Content-Disposition", String.Format("attachment;filename=ReportDuration.xls"));

            Response.BinaryWrite(ms.ToArray());
            ms.Close();
            HttpContext.Current.ApplicationInstance.CompleteRequest();
            Response.End();

            #endregion
        }
    }
}