using System;
using Financial.Relatorio;
using System.IO;
using System.Web;
using System.Globalization;
using Financial.Web.Common;

public partial class _ReportOperacaoRendaFixa : BasePage {
    private static class VisaoRelatorio {
        public static string visaoHTML = "Report";
        public static string visaoPDF = "PDF";
        public static string visaoExcel = "Excel";
    }

    new protected void Page_Load(object sender, EventArgs e) {
        string visao = Request.QueryString["Visao"].ToString();

        ReportOperacaoRendaFixa.TipoOrdenacao tipoOrdenacao = ReportOperacaoRendaFixa.TipoOrdenacao.PorCodigo;

        // Obrigatorio
        DateTime dataInicio = Convert.ToDateTime(Session["textDataInicio"]);
        DateTime dataFim = Convert.ToDateTime(Session["textDataFim"]);
        tipoOrdenacao = (ReportOperacaoRendaFixa.TipoOrdenacao)Convert.ToInt32(Session["dropTipoOrdenacao"]);
        //
        // Opcional
        int? idCliente = null;
        if (!String.IsNullOrEmpty((string)Session["btnEditCodigo"])) {
            idCliente = Convert.ToInt32(Session["btnEditCodigo"]);
        }

        int? idPapel = null;
        if (!String.IsNullOrEmpty((string)Session["dropPapel"]))
        {
            idPapel = Convert.ToInt32(Session["dropPapel"]);
        }

        int? idTitulo = null;
        if (!String.IsNullOrEmpty((string)Session["textIdTitulo"]))
        {
            idTitulo = Convert.ToInt32(Session["textIdTitulo"]);
        }

        int? idLiquidacao = null;
        if (!String.IsNullOrEmpty((string)Session["dropClearing"]))
        {
            idLiquidacao = Convert.ToInt32(Session["dropClearing"]);
            if (idLiquidacao <= 0)
                idLiquidacao = null;
        }

        int? idTipoOperacao = null;
        if (!String.IsNullOrEmpty((string)Session["dropTipoOperacao"]))
        {
            idTipoOperacao = Convert.ToInt32(Session["dropTipoOperacao"]);
            if (idTipoOperacao <= 0)
                idTipoOperacao = null;
        }
           
        if (visao == VisaoRelatorio.visaoHTML) {
            ReportViewer1.Report = new ReportOperacaoRendaFixa(idCliente, dataInicio, dataFim, idTitulo, idPapel, tipoOrdenacao, idLiquidacao, idTipoOperacao);
        }
        else if (visao == VisaoRelatorio.visaoPDF) {
            #region PDF
            ReportOperacaoRendaFixa report = new ReportOperacaoRendaFixa(idCliente, dataInicio, dataFim, idTitulo, idPapel, tipoOrdenacao, idLiquidacao, idTipoOperacao);

            MemoryStream ms = new MemoryStream();
            report.ExportToPdf(ms);
            ms.Seek(0, SeekOrigin.Begin);
            Response.ClearContent();
            Response.ClearHeaders();
            Response.Buffer = true;
            Response.Cache.SetCacheability(HttpCacheability.Private);
            Response.ContentType = "application/pdf";
            Response.AddHeader("Content-Length", ms.Length.ToString(CultureInfo.CurrentCulture));
            Response.AddHeader("Content-Disposition", String.Format("attachment;filename=ReportOperacaoRendaFixa.pdf"));

            Response.BinaryWrite(ms.ToArray());
            ms.Close();
            HttpContext.Current.ApplicationInstance.CompleteRequest();
            Response.End();
            #endregion
        }
        else if (visao == VisaoRelatorio.visaoExcel) {
            #region Excel
            ReportOperacaoRendaFixa report = new ReportOperacaoRendaFixa(idCliente, dataInicio, dataFim, idTitulo, idPapel, tipoOrdenacao, idLiquidacao, idTipoOperacao);

            MemoryStream ms = new MemoryStream();
            report.ExportToXls(ms);
            ms.Seek(0, SeekOrigin.Begin);
            Response.ClearContent();
            Response.ClearHeaders();
            Response.Buffer = true;
            Response.Cache.SetCacheability(HttpCacheability.Private);
            Response.ContentType = "application/vnd.ms-excel";
            Response.AddHeader("Content-Length", ms.Length.ToString(CultureInfo.CurrentCulture));
            Response.AddHeader("Content-Disposition", String.Format("attachment;filename=ReportOperacaoRendaFixa.xls"));

            Response.BinaryWrite(ms.ToArray());
            ms.Close();
            HttpContext.Current.ApplicationInstance.CompleteRequest();
            Response.End();

            #endregion
        }
    }
}