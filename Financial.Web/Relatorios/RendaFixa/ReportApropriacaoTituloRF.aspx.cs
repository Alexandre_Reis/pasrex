using System;
using Financial.Relatorio;
using System.IO;
using System.Web;
using System.Globalization;
using Financial.Web.Common;

public partial class _ReportApropriacaoTituloRF : BasePage
{
    private static class VisaoRelatorio
    {
        public static string visaoHTML = "Report";
        public static string visaoPDF = "PDF";
        public static string visaoExcel = "Excel";
    }

    new protected void Page_Load(object sender, EventArgs e)
    {
        string visao = Request.QueryString["Visao"].ToString();

        // Opcional
        int? idCliente = null;
        if (!String.IsNullOrEmpty((string)Session["btnEditCodigo"]))
        {
            idCliente = Convert.ToInt32(Session["btnEditCodigo"]);
        }

        int? idPapel = null;
        if (!String.IsNullOrEmpty((string)Session["dropPapel"]))
        {
            idPapel = Convert.ToInt32(Session["dropPapel"]);
        }

        int? idAssessor = null;
        if (!String.IsNullOrEmpty((string)Session["dropAssessor"]))
        {
            idAssessor = Convert.ToInt32(Session["dropAssessor"]);
        }

        int? idGrupoAfinidade = null;
        if (!String.IsNullOrEmpty((string)Session["dropGrupoAfinidade"]))
        {
            idGrupoAfinidade = Convert.ToInt32(Session["dropGrupoAfinidade"]);
        }

        byte? idCustodia = null;
        if (!String.IsNullOrEmpty((string)Session["dropCustodia"]))
        {
            idCustodia = Convert.ToByte(Session["dropCustodia"]);
        }

        int? idTitulo = null;
        if (!String.IsNullOrEmpty((string)Session["textIdTitulo"]))
        {
            idTitulo = Convert.ToInt32(Session["textIdTitulo"]);
        }

        DateTime? data = null;
        if (!String.IsNullOrEmpty((string)Session["textData"]))
        {
            data = Convert.ToDateTime(Session["textData"]);
        }

        if (visao == VisaoRelatorio.visaoHTML)
        {
            ReportViewer1.Report = new ReportApropriacaoTituloRF(idCliente, idTitulo, idPapel, data, idAssessor, idGrupoAfinidade, idCustodia);
        }
        else if (visao == VisaoRelatorio.visaoPDF)
        {
            #region PDF

            ReportPosicaoRendaFixa report = new ReportPosicaoRendaFixa(idCliente, idTitulo, idPapel, data, idAssessor, idGrupoAfinidade, idCustodia);

            MemoryStream ms = new MemoryStream();
            report.ExportToPdf(ms);
            ms.Seek(0, SeekOrigin.Begin);
            Response.ClearContent();
            Response.ClearHeaders();
            Response.Buffer = true;
            Response.Cache.SetCacheability(HttpCacheability.Private);
            Response.ContentType = "application/pdf";
            Response.AddHeader("Content-Length", ms.Length.ToString(CultureInfo.CurrentCulture));
            Response.AddHeader("Content-Disposition", String.Format("attachment;filename=ReportPosicaoRendaFixa.pdf"));

            Response.BinaryWrite(ms.ToArray());
            ms.Close();
            HttpContext.Current.ApplicationInstance.CompleteRequest();
            Response.End();
            #endregion
        }
        else if (visao == VisaoRelatorio.visaoExcel)
        {
            #region Excel
            ReportPosicaoRendaFixa report = new ReportPosicaoRendaFixa(idCliente, idTitulo, idPapel, data, idAssessor, idGrupoAfinidade, idCustodia);

            MemoryStream ms = new MemoryStream();
            report.ExportToXls(ms);
            ms.Seek(0, SeekOrigin.Begin);
            Response.ClearContent();
            Response.ClearHeaders();
            Response.Buffer = true;
            Response.Cache.SetCacheability(HttpCacheability.Private);
            Response.ContentType = "application/vnd.ms-excel";
            Response.AddHeader("Content-Length", ms.Length.ToString(CultureInfo.CurrentCulture));
            Response.AddHeader("Content-Disposition", String.Format("attachment;filename=ReportPosicaoRendaFixa.xls"));

            Response.BinaryWrite(ms.ToArray());
            ms.Close();
            HttpContext.Current.ApplicationInstance.CompleteRequest();
            Response.End();

            #endregion
        }
    }
}