<%@ WebHandler Language="C#" Class="ReportPricingDiario" %>

using System;
using System.Web;
using Bytescout.Spreadsheet;
using System.IO;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using Financial.RendaFixa;
using Financial.Util;
using Financial.Common.Enums;
using Financial.RendaFixa.Enums;

public class ReportPricingDiario : IHttpHandler
{
    private enum Formatting
    {
        None = 0,
        Datetime = 1,
        Number = 2,
        Invisible = 3
    }

    private class PremissasPrecificacao
    {
        public string label;
        public object value;
        public int formatting;
        public PremissasPrecificacao(string label, object value, int formatting)
        {
            this.label = label;
            this.value = value;
            this.formatting = formatting;
        }
    }
    public void ProcessRequest(HttpContext context)
    {
        /*PARAMETROS*/
        int? idPosicaoRendaFixa = null;
        if (!String.IsNullOrEmpty(context.Request["idposicaorendafixa"]))
        {
            idPosicaoRendaFixa = Int32.Parse(context.Request["idposicaorendafixa"]);
        }

        PosicaoRendaFixa posicaoRendaFixa = new PosicaoRendaFixa();
        if (idPosicaoRendaFixa.HasValue)
        {
            posicaoRendaFixa.LoadByPrimaryKey(idPosicaoRendaFixa.Value);
        }

        int? idTitulo = null;
        if (!String.IsNullOrEmpty(context.Request["idtitulo"]))
        {
            idTitulo = Int32.Parse(context.Request["idtitulo"]);
        }

        TituloRendaFixa tituloRendaFixa = new TituloRendaFixa();
        if (posicaoRendaFixa.es.HasData)
        {
            tituloRendaFixa.LoadByPrimaryKey(posicaoRendaFixa.IdTitulo.Value);
        }
        else if (idTitulo.HasValue)
        {
            tituloRendaFixa.LoadByPrimaryKey(idTitulo.Value);
        }

        PapelRendaFixa papelRendaFixa = tituloRendaFixa.UpToPapelRendaFixaByIdPapel;

        DateTime? dataOperacao = null;
        if (context.Request["dataoperacao"] != "null" && context.Request["dataoperacao"] != null)
        {
            dataOperacao = DateTime.Parse(context.Request["dataoperacao"]);
        }

        decimal? puOperacao = null;
        if (context.Request["puoperacao"] != "null" && context.Request["puoperacao"] != null)
        {
            puOperacao = Decimal.Parse(context.Request["puoperacao"].Replace(".", ","));
        }

        DateTime dataCalculo = DateTime.Parse(context.Request["datacalculo"]);
        if (context.Request["dataparam"] != "null" && context.Request["dataparam"] != null && context.Request["dataparam"] != "01/01/0100")
        {
            dataCalculo = DateTime.Parse(context.Request["dataparam"]);
        }        
        
        bool eventosData = Boolean.Parse(context.Request["eventosData"]);
        decimal taxa = Decimal.Parse(context.Request["taxa"]);

        if (tituloRendaFixa.es.HasData && tituloRendaFixa.IdPapel.HasValue && posicaoRendaFixa.es.HasData && posicaoRendaFixa.IdCliente.HasValue)           
        {
            Financial.Fundo.Carteira carteira = new Financial.Fundo.Carteira();
            carteira.LoadByPrimaryKey(posicaoRendaFixa.IdCliente.Value);

            if (carteira.IdGrupoPerfilMTM.HasValue)
            {
                PerfilMTM perfilMTM = new PerfilMTM();
                perfilMTM.obtemPerfilMTM(dataCalculo, tituloRendaFixa.IdPapel, posicaoRendaFixa.IdOperacao, tituloRendaFixa.IdTitulo, carteira.IdGrupoPerfilMTM.Value);

                if (perfilMTM.UpToSerieRendaFixaByIdSerie.IdSerie.HasValue)
                {
                    TaxaSerie taxaSerie = new TaxaSerie();
                    if (taxaSerie.LoadByPrimaryKey(dataCalculo, perfilMTM.UpToSerieRendaFixaByIdSerie.IdSerie.Value))
                    {
                        taxa = taxaSerie.Taxa.Value;
                    }
                }
            }
        }
         
        string msg = "";
        PricingDiario pricingDiario = new PricingDiario();
        if (idTitulo.HasValue && dataCalculo >= tituloRendaFixa.DataVencimento.Value)
        {
            msg = "Data de refer�ncia informada deve ser menor que a data do vencimento do t�tulo";
            pricingDiario.descricao = "ERRO";
        }
        else if (dataOperacao.HasValue && dataOperacao.Value >= dataCalculo)
        {
            msg = "Data opera��o deve ser menor que a data de refer�ncia informada";
            pricingDiario.descricao = "ERRO";
        }
        else
        {
            try
            {
                if (idPosicaoRendaFixa.HasValue)
                {
                    pricingDiario = new PricingDiario(dataCalculo, posicaoRendaFixa, eventosData, taxa);
                }
                else if (idTitulo.HasValue)
                {
                    pricingDiario = new PricingDiario(dataCalculo, tituloRendaFixa, eventosData, taxa, dataOperacao, puOperacao);
                }
            }
            catch (Exception e)
            {
                msg = e.Message;
                pricingDiario.descricao = "ERRO";
            }
        }

        /*FIM PARAMETROS*/

        const int START_ROW = 2, START_COLUMN = 1;
        const int ROW_HEADER_PRECIFICACAO = 13;
        const int COLUMN_HEADER_PRECIFICACAO = START_COLUMN;
        const int COLUMNS_HEADER_SPACING = 7;
        const int COLUMN_PAGAMENTO_CORRIGIDO = 4;
        const int MAX_ROWS_MODELO = 2500;
        int columnHeaderIndiceCorrecao = COLUMNS_HEADER_SPACING + COLUMN_HEADER_PRECIFICACAO;
        const string HEADER_INDICE_CORRECAO = "�ndice de Corre��o";
        int maxRowPricingDiario = 0;
        int maxRowCorrecaoIndice = 0;

        Spreadsheet spreadsheet = new Bytescout.Spreadsheet.Spreadsheet();

        string pathModelos = System.AppDomain.CurrentDomain.BaseDirectory + "\\Relatorios\\Modelos";

        bool temFluxoCorrecao = papelRendaFixa.Classe.Value == (int)ClasseRendaFixa.PosFixado_FluxoCorrigido;

        string arquivoPlanilha = temFluxoCorrecao ? "report_pricing_diario_correcao.xls" : "report_pricing_diario.xls";
        string pathPlanilhaModelo = String.Format("{0}\\{1}", pathModelos, arquivoPlanilha);

        spreadsheet.LoadFromFile(pathPlanilhaModelo);

        Worksheet worksheet = spreadsheet.Workbook.Worksheets[0];
        Bytescout.Spreadsheet.BaseClasses.CellAddress cellAddress;

        Cell currentCell = worksheet.Cell(START_ROW, START_COLUMN);
        worksheet.Name = pricingDiario.descricao;

        if (msg != "")
        {
            currentCell = worksheet.Cell(1, 1);
            currentCell.Value = msg.ToUpper();
            currentCell.FontColor = System.Drawing.Color.Red;
        }
        else
        {
            string siglaDias = pricingDiario.contagemDias == "Dias Corridos" ? "DC" :
            pricingDiario.contagemDias == "Dias 360" ? "D360" : "DU";

            bool temCorrecaoIndice = pricingDiario.listaCorrecaoIndice.Count > 0;
            bool temAgendaEventos = pricingDiario.listaValores.Count > 0;

            List<PremissasPrecificacao> listaPremissasPrecificacao = new List<PremissasPrecificacao>();
            listaPremissasPrecificacao.Add(new PremissasPrecificacao("Data de Emiss�o", pricingDiario.dataEmissao, (int)Formatting.Datetime));
            listaPremissasPrecificacao.Add(new PremissasPrecificacao("Valor Nominal de Emiss�o", pricingDiario.puNominal, (int)Formatting.Number));
            listaPremissasPrecificacao.Add(new PremissasPrecificacao("Data Opera��o", pricingDiario.dataOperacao, pricingDiario.dataOperacao != null ? (int)Formatting.Datetime : (int)Formatting.Invisible));
            listaPremissasPrecificacao.Add(new PremissasPrecificacao("PU Opera��o", pricingDiario.puOperacao, pricingDiario.puOperacao != 0 ? (int)Formatting.Number : (int)Formatting.Invisible));
            listaPremissasPrecificacao.Add(new PremissasPrecificacao("TIR", pricingDiario.taxaJuros, (int)Formatting.Number));
            listaPremissasPrecificacao.Add(new PremissasPrecificacao("Indexador", pricingDiario.indexador, (int)Formatting.None));
            listaPremissasPrecificacao.Add(new PremissasPrecificacao("Fator de Corre��o", pricingDiario.fatorCorrecao, (int)Formatting.Number));
            listaPremissasPrecificacao.Add(new PremissasPrecificacao("Data de C�lculo", dataCalculo, (int)Formatting.Datetime));
            listaPremissasPrecificacao.Add(new PremissasPrecificacao("PU", pricingDiario.pu, (int)Formatting.Number));
            listaPremissasPrecificacao.Add(new PremissasPrecificacao("PU com Grossup", pricingDiario.puGrossup, (int)Formatting.Number));

            foreach (PremissasPrecificacao premissasPrecificacao in listaPremissasPrecificacao)
            {
                currentCell.Value = premissasPrecificacao.label;
                cellAddress = currentCell.GetAddress();
                currentCell = worksheet.Cell(cellAddress.Row, cellAddress.Column + 2);

                if (premissasPrecificacao.formatting == Formatting.Datetime.GetHashCode())
                {
                    currentCell.ValueAsDateTime = (DateTime)premissasPrecificacao.value;
                }
                else if (premissasPrecificacao.formatting == Formatting.Number.GetHashCode())
                {
                    currentCell.ValueAsDouble = Convert.ToDouble(premissasPrecificacao.value);
                }
                else if (premissasPrecificacao.formatting == Formatting.Invisible.GetHashCode())
                {
                    worksheet.Rows[cellAddress.Row].Height = 0;
                }
                else
                {
                    currentCell.Value = premissasPrecificacao.value;
                }
                currentCell = worksheet.Cell(cellAddress.Row + 1, START_COLUMN);
            }

            if (temFluxoCorrecao)
            {
                //Planilha para t�tulos com fluxo de corre��o
                currentCell = worksheet.Cell(ROW_HEADER_PRECIFICACAO, COLUMN_HEADER_PRECIFICACAO);
                currentCell.Value = "Precifica��o";
                cellAddress = currentCell.GetAddress();

                currentCell = worksheet.Cell(ROW_HEADER_PRECIFICACAO + 1, COLUMN_HEADER_PRECIFICACAO);
                string[] columnsHeaderPrecificacao = { "Taxa", "Valor", "Data Evento",
            "TipoEvento", "Fator Corre��o Fluxo", "PU Nominal Amortizado", "Valor Corrigido", "Fator Corre��o Opera��o", "PU Corrigido"};
                foreach (string columnHeaderPrecificacao in columnsHeaderPrecificacao)
                {
                    currentCell.Value = columnHeaderPrecificacao;
                    cellAddress = currentCell.GetAddress();
                    currentCell = worksheet.Cell(cellAddress.Row, cellAddress.Column + 1);
                    currentCell.Value = "#";
                }

                cellAddress = currentCell.GetAddress();
                currentCell.Value = "";
                currentCell = worksheet.Cell(cellAddress.Row + 1, COLUMN_HEADER_PRECIFICACAO);

                foreach (PricingDiario.ValoresFluxoCorrecao valores in pricingDiario.listaValoresCorrecao)
                {
                    if (valores.taxa.HasValue)
                    {
                        currentCell.ValueAsDouble = Convert.ToDouble(valores.taxa);
                    }
                    cellAddress = currentCell.GetAddress();
                    currentCell = worksheet.Cell(cellAddress.Row, cellAddress.Column + 1);

                    if (valores.valor.HasValue)
                    {
                        currentCell.ValueAsDouble = Convert.ToDouble(valores.valor);
                    }
                    cellAddress = currentCell.GetAddress();
                    currentCell = worksheet.Cell(cellAddress.Row, cellAddress.Column + 1);

                    currentCell.ValueAsDateTime = valores.dataEvento;
                    cellAddress = currentCell.GetAddress();
                    currentCell = worksheet.Cell(cellAddress.Row, cellAddress.Column + 1);

                    currentCell.ValueAsString = valores.tipoEvento;
                    cellAddress = currentCell.GetAddress();
                    currentCell = worksheet.Cell(cellAddress.Row, cellAddress.Column + 1);

                    currentCell.ValueAsDouble = Convert.ToDouble(valores.fatorCorrecaoFluxo);
                    cellAddress = currentCell.GetAddress();
                    currentCell = worksheet.Cell(cellAddress.Row, cellAddress.Column + 1);

                    currentCell.ValueAsDouble = Convert.ToDouble(valores.puNominalAmortizado);
                    cellAddress = currentCell.GetAddress();
                    currentCell = worksheet.Cell(cellAddress.Row, cellAddress.Column + 1);

                    currentCell.ValueAsDouble = Convert.ToDouble(valores.valorCorrigido);
                    cellAddress = currentCell.GetAddress();
                    currentCell = worksheet.Cell(cellAddress.Row, cellAddress.Column + 1);

                    currentCell.ValueAsDouble = Convert.ToDouble(valores.fatorCorrecaoOperacao);
                    cellAddress = currentCell.GetAddress();
                    currentCell = worksheet.Cell(cellAddress.Row, cellAddress.Column + 1);

                    currentCell.ValueAsDouble = Convert.ToDouble(valores.puCorrigido);
                    cellAddress = currentCell.GetAddress();
                    currentCell = worksheet.Cell(cellAddress.Row + 1, START_COLUMN);
                    maxRowPricingDiario = cellAddress.Row + 1;
                }
            }
            else
            {
                //Planilha para t�tulos sem fluxo de corre��o
                currentCell = worksheet.Cell(ROW_HEADER_PRECIFICACAO, COLUMN_HEADER_PRECIFICACAO);

                currentCell.Value = "Precifica��o";
                cellAddress = currentCell.GetAddress();

                currentCell = worksheet.Cell(cellAddress.Row, columnHeaderIndiceCorrecao);

                currentCell.Value = HEADER_INDICE_CORRECAO;

                if (!temAgendaEventos)
                {
                    //N�o existe agenda de eventos - Esconder tabela de precifica��o
                    string rangeIndiceCorrecao = String.Format("{0}{1}:{2}{3}", GetExcelColumnName(columnHeaderIndiceCorrecao),
                        ROW_HEADER_PRECIFICACAO,
                        GetExcelColumnName(worksheet.UsedRangeColumnMax + 10), //pegar range maior para apagar tudo � direita da tabela de �ndice transposta
                        worksheet.UsedRangeRowMax);

                    Range range = worksheet.Range(rangeIndiceCorrecao);

                    range.CopyInto(String.Format("{0}{1}", GetExcelColumnName(COLUMN_HEADER_PRECIFICACAO), ROW_HEADER_PRECIFICACAO));
                    columnHeaderIndiceCorrecao = COLUMN_HEADER_PRECIFICACAO;

                    //aumentar a largura da coluna H (7)
                    worksheet.Columns[7].Width = worksheet.Columns[6].Width;
                }
                else
                {
                    currentCell = worksheet.Cell(ROW_HEADER_PRECIFICACAO + 1, COLUMN_HEADER_PRECIFICACAO);
                    string[] columnsHeaderPrecificacao = { "Data Evento", "Data Pagamento", "Valor Pagamento",
            "Pagamento Corrigido", siglaDias, "VP"};
                    foreach (string columnHeaderPrecificacao in columnsHeaderPrecificacao)
                    {
                        currentCell.Value = columnHeaderPrecificacao;
                        cellAddress = currentCell.GetAddress();
                        currentCell = worksheet.Cell(cellAddress.Row, cellAddress.Column + 1);
                        currentCell.Value = "#";
                    }

                    cellAddress = currentCell.GetAddress();
                    currentCell.Value = "";
                    currentCell = worksheet.Cell(cellAddress.Row + 1, COLUMN_HEADER_PRECIFICACAO);

                    foreach (PricingDiario.ValoresFluxoDesconto valores in pricingDiario.listaValores)
                    {
                        currentCell.ValueAsDateTime = valores.dataEvento;
                        cellAddress = currentCell.GetAddress();
                        currentCell = worksheet.Cell(cellAddress.Row, cellAddress.Column + 1);

                        currentCell.ValueAsDateTime = valores.dataPagamento;
                        cellAddress = currentCell.GetAddress();
                        currentCell = worksheet.Cell(cellAddress.Row, cellAddress.Column + 1);

                        currentCell.ValueAsDouble = Convert.ToDouble(valores.valor);
                        cellAddress = currentCell.GetAddress();
                        currentCell = worksheet.Cell(cellAddress.Row, cellAddress.Column + 1);

                        currentCell.ValueAsDouble = Convert.ToDouble(valores.valorCorrigido);
                        cellAddress = currentCell.GetAddress();
                        currentCell = worksheet.Cell(cellAddress.Row, cellAddress.Column + 1);

                        currentCell.ValueAsInteger = valores.numeroDias;
                        cellAddress = currentCell.GetAddress();
                        currentCell = worksheet.Cell(cellAddress.Row, cellAddress.Column + 1);

                        currentCell.ValueAsDouble = Convert.ToDouble(valores.valorAtual);
                        cellAddress = currentCell.GetAddress();
                        currentCell = worksheet.Cell(cellAddress.Row + 1, START_COLUMN);
                        maxRowPricingDiario = cellAddress.Row + 1;
                    }

                    if (!temCorrecaoIndice)
                    {
                        //Esconder coluna de pagamento corrigido
                        worksheet.Columns[COLUMN_PAGAMENTO_CORRIGIDO].Width = 0;
                    }
                }

                currentCell = worksheet.Cell(ROW_HEADER_PRECIFICACAO + 1, columnHeaderIndiceCorrecao);
                string[] columnsHeaderIndiceCorrecao = { "Data p/ C�lculo", "Data Efetiva", "Data �ndice",
            pricingDiario.indexador, siglaDias, siglaDias + "T", "Fator", "Fator Acum."};

                if (!temCorrecaoIndice)
                {
                    foreach (string column in columnsHeaderIndiceCorrecao)
                    {
                        currentCell.Value = " ";
                        cellAddress = currentCell.GetAddress();
                        worksheet.Columns[cellAddress.Column].Width = 0;
                        currentCell = worksheet.Cell(cellAddress.Row, cellAddress.Column + 1);
                    }
                }
                else
                {
                    foreach (string column in columnsHeaderIndiceCorrecao)
                    {
                        currentCell.Value = column;
                        cellAddress = currentCell.GetAddress();
                        currentCell = worksheet.Cell(cellAddress.Row, cellAddress.Column + 1);
                    }

                    currentCell = worksheet.Cell(cellAddress.Row + 1, columnHeaderIndiceCorrecao);

                    foreach (CalculoFinanceiro.CorrecaoIndice correcaoIndice in pricingDiario.listaCorrecaoIndice)
                    {
                        currentCell.ValueAsDateTime = correcaoIndice.dataCalculo;
                        cellAddress = currentCell.GetAddress();
                        currentCell = worksheet.Cell(cellAddress.Row, cellAddress.Column + 1);

                        currentCell.ValueAsDateTime = correcaoIndice.dataCalculoEfetiva;
                        cellAddress = currentCell.GetAddress();
                        currentCell = worksheet.Cell(cellAddress.Row, cellAddress.Column + 1);

                        currentCell.ValueAsDateTime = correcaoIndice.dataIndice;
                        cellAddress = currentCell.GetAddress();
                        currentCell = worksheet.Cell(cellAddress.Row, cellAddress.Column + 1);

                        currentCell.ValueAsDouble = Convert.ToDouble(correcaoIndice.valorIndice);
                        cellAddress = currentCell.GetAddress();
                        currentCell = worksheet.Cell(cellAddress.Row, cellAddress.Column + 1);

                        currentCell.ValueAsInteger = correcaoIndice.numeroDias;
                        cellAddress = currentCell.GetAddress();
                        currentCell = worksheet.Cell(cellAddress.Row, cellAddress.Column + 1);

                        currentCell.ValueAsInteger = correcaoIndice.numeroDiasTotal;
                        cellAddress = currentCell.GetAddress();
                        currentCell = worksheet.Cell(cellAddress.Row, cellAddress.Column + 1);

                        currentCell.ValueAsDouble = Convert.ToDouble(correcaoIndice.fatorCorrecao);
                        cellAddress = currentCell.GetAddress();
                        currentCell = worksheet.Cell(cellAddress.Row, cellAddress.Column + 1);

                        currentCell.ValueAsDouble = Convert.ToDouble(correcaoIndice.fatorAcumulado);
                        cellAddress = currentCell.GetAddress();
                        currentCell = worksheet.Cell(cellAddress.Row + 1, columnHeaderIndiceCorrecao);
                        maxRowCorrecaoIndice = cellAddress.Row + 1;
                    }
                }



                if (!(pricingDiario.idIndexador == ListaIndiceFixo.IGPM || pricingDiario.idIndexador == ListaIndiceFixo.IPCA
                    || pricingDiario.idIndexador == ListaIndiceFixo.INPC || pricingDiario.idIndexador == ListaIndiceFixo.IGPDI))
                {
                    //Esconder colunas de data calculo e efetiva
                    string rangeIndiceCorrecao = String.Format("{0}{1}:{2}{3}", GetExcelColumnName(columnHeaderIndiceCorrecao + 2),
                        ROW_HEADER_PRECIFICACAO + 1,
                        GetExcelColumnName(worksheet.UsedRangeColumnMax),
                        worksheet.UsedRangeRowMax);

                    Range range = worksheet.Range(rangeIndiceCorrecao);

                    range.CopyInto(String.Format("{0}{1}", GetExcelColumnName(columnHeaderIndiceCorrecao), ROW_HEADER_PRECIFICACAO + 1));

                    worksheet.Cell(ROW_HEADER_PRECIFICACAO, columnHeaderIndiceCorrecao).Value = HEADER_INDICE_CORRECAO;
                }

                //Limpar rows formatadas que nao foram usadas no fim da planilha
                if (msg == "")
                {
                    int maxRow = Math.Max(maxRowCorrecaoIndice, maxRowPricingDiario);
                    worksheet.Rows.Delete(maxRow, MAX_ROWS_MODELO);
                }

            }
        }

        const string BYTESCOUT_WORKSHEET_NAME = "Bytescout Spreadsheet SDK";
        worksheet = spreadsheet.Workbook.Worksheets.ByName(BYTESCOUT_WORKSHEET_NAME);
        if (worksheet == null)
        {
            worksheet = spreadsheet.Workbook.Worksheets.Add(BYTESCOUT_WORKSHEET_NAME);
        }
        worksheet.Visible = Bytescout.Spreadsheet.Constants.SHEETVISIBILITY.Hidden;

        string filename = pricingDiario.descricao;
        
        string regexSearch = new string(Path.GetInvalidFileNameChars()) + new string(Path.GetInvalidPathChars());
        regexSearch += ",";
        Regex r = new Regex(string.Format("[{0}]", Regex.Escape(regexSearch)));
        
        filename = r.Replace(filename, "");
        
        MemoryStream memoryStream = new MemoryStream();
        spreadsheet.SaveToStream(memoryStream);
        spreadsheet.Close();
        
        context.Response.ClearHeaders();
        context.Response.ClearContent();
        context.Response.ContentType = "application/xls";
        context.Response.AddHeader("Content-Disposition",
            String.Format("attachment;filename={0}.xls", filename));

        memoryStream.WriteTo(context.Response.OutputStream);
    }

    private string GetExcelColumnName(int columnNumber)
    {
        int dividend = columnNumber + 1;
        string columnName = String.Empty;
        int modulo;

        while (dividend > 0)
        {
            modulo = (dividend - 1) % 26;
            columnName = Convert.ToChar(65 + modulo).ToString() + columnName;
            dividend = (int)((dividend - modulo) / 26);
        }

        return columnName;
    }

    public bool IsReusable
    {
        get
        {
            return false;
        }
    }

}