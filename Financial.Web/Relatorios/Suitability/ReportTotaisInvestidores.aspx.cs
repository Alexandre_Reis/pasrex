using System;
using Financial.Relatorio;
using System.IO;
using System.Web;
using System.Globalization;

public partial class _ReportTotaisInvestidores : Financial.Web.Common.BasePage {        
    new protected void Page_Load(object sender, EventArgs e) {        
        string visao = Request.QueryString["Visao"].ToString();

        DateTime dataInicio = Convert.ToDateTime(Session["textDataInicio"]);
        DateTime dataFim = Convert.ToDateTime(Session["textDataFim"]);

        if (visao == "Report") {
            ReportViewer1.Report = new ReportTotalInvestidores(dataInicio, dataFim);
        }
        else if (visao == "PDF") {
            #region PDF
            ReportTotalInvestidores report = new ReportTotalInvestidores(dataInicio, dataFim);

            MemoryStream ms = new MemoryStream();
            report.ExportToPdf(ms);
            ms.Seek(0, SeekOrigin.Begin);
            Response.ClearContent();
            Response.ClearHeaders();
            Response.Buffer = true;
            Response.Cache.SetCacheability(HttpCacheability.Private);
            Response.ContentType = "application/pdf";
            Response.AddHeader("Content-Length", ms.Length.ToString(CultureInfo.CurrentCulture));
            Response.AddHeader("Content-Disposition", String.Format("attachment;filename=ReportTotalInvestidores.pdf"));

            Response.BinaryWrite(ms.ToArray());
            ms.Close();
            HttpContext.Current.ApplicationInstance.CompleteRequest();
            Response.End();
            #endregion
        }
        else if (visao == "Excel") {
            #region Excel
            ReportTotalInvestidores report = new ReportTotalInvestidores(dataInicio, dataFim);

            MemoryStream ms = new MemoryStream();
            report.ExportToXls(ms);
            ms.Seek(0, SeekOrigin.Begin);
            Response.ClearContent();
            Response.ClearHeaders();
            Response.Buffer = true;
            Response.Cache.SetCacheability(HttpCacheability.Private);
            Response.ContentType = "application/vnd.ms-excel";
            Response.AddHeader("Content-Length", ms.Length.ToString(CultureInfo.CurrentCulture));
            Response.AddHeader("Content-Disposition", String.Format("attachment;filename=ReportTotalInvestidores.xls"));

            Response.BinaryWrite(ms.ToArray());
            ms.Close();
            HttpContext.Current.ApplicationInstance.CompleteRequest();
            Response.End();

            #endregion
        }
    }
}