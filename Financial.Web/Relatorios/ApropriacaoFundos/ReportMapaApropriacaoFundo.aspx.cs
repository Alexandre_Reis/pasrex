using System;
using Financial.Relatorio;
using System.IO;
using System.Web;
using System.Globalization;

public partial class _ReportMapaApropriacaoFundo : Financial.Web.Common.BasePage {        
   
    new protected void Page_Load(object sender, EventArgs e) {
       
        
        string visao = Request.QueryString["Visao"].ToString();
        

        DateTime dataInicio = new DateTime();
        DateTime dataFim = new DateTime();
        string cdAtivoBolsa = null;
        int? idCliente = null;
        string tipoMercado = null;
        int? idAgenteCorretora = null;
        string tipoOperacao = null;

        // Obrigatorio
        dataInicio = Convert.ToDateTime(Session["textDataHistorico"]);
      
        //Opcionais
        cdAtivoBolsa = (string)Session["textCdAtivo"];
        tipoMercado = (string)Session["dropTipoMercado"];
        if (!String.IsNullOrEmpty((string)Session["btnEditCodigo"])) {
            idCliente = Convert.ToInt32(Session["btnEditCodigo"]);
        }
        if (!String.IsNullOrEmpty((string)Session["dropAgente"])) {
            idAgenteCorretora = Convert.ToInt32(Session["dropAgente"]);
        }
        if (!String.IsNullOrEmpty((string)Session["dropTipoOperacao"])) {
            tipoOperacao = Convert.ToString(Session["dropTipoOperacao"]);
        }

        
        ReportMapaApropriacaoFundo report = new ReportMapaApropriacaoFundo(dataInicio, idCliente, true);
        if (visao == "Report") {
            ReportViewer1.Report = report; // new ReportMapaOperacaoBolsa(dataInicio, dataFim, tipoMercado, idCliente, cdAtivoBolsa, idAgenteCorretora, tipoOperacao);
        }
        else if (visao == "PDF") {
            #region PDF
        
            MemoryStream ms = new MemoryStream();
            report.ExportToPdf(ms);
            ms.Seek(0, SeekOrigin.Begin);
            Response.ClearContent();
            Response.ClearHeaders();
            Response.Buffer = true;
            Response.Cache.SetCacheability(HttpCacheability.Private);
            Response.ContentType = "application/pdf";
            Response.AddHeader("Content-Length", ms.Length.ToString(CultureInfo.CurrentCulture));
            Response.AddHeader("Content-Disposition", String.Format("attachment;filename=ReportMapaApropriacaoFundo.pdf"));

            Response.BinaryWrite(ms.ToArray());
            ms.Close();
            HttpContext.Current.ApplicationInstance.CompleteRequest();
            Response.End();
            #endregion
        }
        else if (visao == "Excel") {
            #region Excel
        
            MemoryStream ms = new MemoryStream();
            report.ExportToXls(ms);
            ms.Seek(0, SeekOrigin.Begin);
            Response.ClearContent();
            Response.ClearHeaders();
            Response.Buffer = true;
            Response.Cache.SetCacheability(HttpCacheability.Private);
            Response.ContentType = "application/vnd.ms-excel";
            Response.AddHeader("Content-Length", ms.Length.ToString(CultureInfo.CurrentCulture));
            Response.AddHeader("Content-Disposition", String.Format("attachment;filename=ReportMapaApropriacaoFundo.xls"));

            Response.BinaryWrite(ms.ToArray());
            ms.Close();
            HttpContext.Current.ApplicationInstance.CompleteRequest();
            Response.End();

            #endregion
        }
    }
}