using System;
using Financial.Relatorio;
using System.IO;
using System.Web;
using System.Globalization;

public partial class _ReportExtratoContaCorrente : Financial.Web.Common.BasePage {
    /* Como Usar:
        //int idCliente = 1;
        int idCliente = 26458;
        //
        DateTime dataInicio = new DateTime(2001, 10, 1);
        DateTime dataFim = new DateTime(2001, 11, 1);
        //
        ReportViewer1.Report = new ReportExtratoContaCorrente(idCliente, dataInicio, dataFim);
     */
    new protected void Page_Load(object sender, EventArgs e) {
        string visao = Request.QueryString["Visao"].ToString();
        //
        //Response.Write(Request.Form["textDataInicio"].ToString());
        //Response.Write("<br>" +Request.Form["textDataFim"].ToString());
        //Response.Write("<br>" + Request.Form["btnEditCodigo"].ToString());

        DateTime dataInicio = new DateTime();
        DateTime dataFim = new DateTime();
        int? idCliente = null;
        int? idConta = null;
        int? idGrupo = null;

        // Obrigatorio
        dataInicio = Convert.ToDateTime(Session["textDataInicio"]);
        dataFim = Convert.ToDateTime(Session["textDataFim"]);
        idCliente = Convert.ToInt32(Session["btnEditCodigo"]);

        if (Session["dropConta"] != null)
        {
            idConta = Convert.ToInt32(Session["dropConta"]);
        }

        if (Session["dropGrupo"] != null)
        {
            idGrupo = Convert.ToInt32(Session["dropGrupo"]);
        }

        string fileName = "ExtratoCC_" + idCliente.Value.ToString();

        if (visao == "Report") {
            ReportViewer1.Report = new ReportExtratoContaCorrente(idCliente.Value, dataInicio, dataFim, idConta, idGrupo);
        }
        else if (visao == "PDF") {
            #region PDF
            ReportExtratoContaCorrente report = new ReportExtratoContaCorrente(idCliente.Value, dataInicio, dataFim, idConta, idGrupo);

            MemoryStream ms = new MemoryStream();
            report.ExportToPdf(ms);
            ms.Seek(0, SeekOrigin.Begin);
            Response.ClearContent();
            Response.ClearHeaders();
            Response.Buffer = true;
            Response.Cache.SetCacheability(HttpCacheability.Private);
            Response.ContentType = "application/pdf";
            Response.AddHeader("Content-Length", ms.Length.ToString(CultureInfo.CurrentCulture));
            Response.AddHeader("Content-Disposition", String.Format("attachment;filename=" + fileName + ".pdf"));

            Response.BinaryWrite(ms.ToArray());
            ms.Close();
            HttpContext.Current.ApplicationInstance.CompleteRequest();
            Response.End();
            #endregion
        }
        else if (visao == "Excel") {
            #region Excel
            ReportExtratoContaCorrente report = new ReportExtratoContaCorrente(idCliente.Value, dataInicio, dataFim, idConta, idGrupo);

            MemoryStream ms = new MemoryStream();
            report.ExportToXls(ms);
            ms.Seek(0, SeekOrigin.Begin);
            Response.ClearContent();
            Response.ClearHeaders();
            Response.Buffer = true;
            Response.Cache.SetCacheability(HttpCacheability.Private);
            Response.ContentType = "application/vnd.ms-excel";
            Response.AddHeader("Content-Length", ms.Length.ToString(CultureInfo.CurrentCulture));
            Response.AddHeader("Content-Disposition", String.Format("attachment;filename=" + fileName + ".xls"));

            Response.BinaryWrite(ms.ToArray());
            ms.Close();
            HttpContext.Current.ApplicationInstance.CompleteRequest();
            Response.End();

            #endregion
        }
    }
}
