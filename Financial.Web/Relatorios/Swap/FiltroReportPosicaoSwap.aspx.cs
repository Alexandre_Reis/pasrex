﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Collections.Generic;
using System.Text;

using Financial.Fundo;
using Financial.Investidor;
using Financial.Web.Util;
using Financial.Security;
using Financial.Util;
using Financial.Web.Common;

using Financial.Common.Enums;
using Financial.Investidor.Enums;
using Financial.Security.Enums;

using log4net;

using EntitySpaces.Interfaces;

using DevExpress.Web;

public partial class FiltroReportPosicaoSwap : FiltroReportBasePage {
    private static readonly ILog log = LogManager.GetLogger(typeof(FiltroReportPosicaoSwap));
    
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    new protected void Page_Load(object sender, EventArgs e) {
        //
        this.RegisterControls(this.textNome.ClientID);

        btnEditCodigo.Focus();

        this.HasPopupCliente = true;
        base.Page_Load(sender, e);

        this.TrataTravamentoCampos();
    }

    protected void RegisterControls(string idTextNome) {
        StringBuilder script = new StringBuilder();
        script.Append(String.Format(" var textNome = '{0}';", idTextNome));
        Page.ClientScript.RegisterClientScriptBlock(Page.GetType(), "", script.ToString(), true);
    }

    /// <summary>
    /// 
    /// </summary>
    private void TrataTravamentoCampos() {
        Usuario usuario = new Usuario();
        usuario.BuscaUsuario(HttpContext.Current.User.Identity.Name);
        int tipoTrava = usuario.TipoTrava.Value;

        if (tipoTrava == (int)TipoTravaUsuario.TravaCliente) {
            btnEditCodigo.Enabled = false;

            PermissaoCliente permissaoCliente = new PermissaoCliente();
            int? idCliente = permissaoCliente.RetornaClienteAssociado(HttpContext.Current.User.Identity.Name);

            if (idCliente.HasValue) {
                Cliente cliente = new Cliente();

                List<esQueryItem> campos = new List<esQueryItem>();
                campos.Add(cliente.Query.Apelido);
                cliente.LoadByPrimaryKey(campos, idCliente.Value);
                string apelido = cliente.Apelido;

                btnEditCodigo.Text = Convert.ToString(idCliente.Value);
                textNome.Text = apelido;
            }
        }
    }
      
    /// <summary>
    /// 
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void ASPxCallback1_Callback(object source, DevExpress.Web.CallbackEventArgs e) {
        string nome = "";
        if (!String.IsNullOrEmpty(e.Parameter) && e.Parameter != "null") {
            int idCliente = Convert.ToInt32(e.Parameter);
            Cliente cliente = new Cliente();
            cliente.LoadByPrimaryKey(idCliente);

            if (cliente.str.Apelido != "") {
                if (cliente.IsAtivo) {
                    PermissaoCliente permissaoCliente = new PermissaoCliente();
                    nome = permissaoCliente.RetornaAcessoClienteComControle(idCliente, HttpContext.Current.User.Identity.Name)
                            ? cliente.str.Apelido : "no_access";
                }
                else {
                    nome = "no_active";
                }
            }
        }
        e.Result = nome;
    }
    
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void EsDSCliente_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e) {
        ClienteCollection clienteCollection = new ClienteCollection();
        clienteCollection.BuscaClientesComAcesso(HttpContext.Current.User.Identity.Name);

        e.Collection = clienteCollection;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnVisualiza_Click(object sender, EventArgs e) {
        this.TrataErros();
        this.SelecionaRelatorio(VisaoRelatorio.visaoHTML);
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnPDF_Click(object sender, EventArgs e) {
        this.TrataErros();
        this.SelecionaRelatorio(VisaoRelatorio.visaoPDF);
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnExcel_Click(object sender, EventArgs e) {
        this.TrataErros();
        this.SelecionaRelatorio(VisaoRelatorio.visaoExcel);
    }

    /// <summary>
    /// Trata os Erros de Relatorio
    /// </summary>
    private void TrataErros() {

        #region Campos obrigatórios
        List<Control> controles = new List<Control>(new Control[] { this.textData});

        if (base.TestaObrigatorio(controles) != "") {
            throw new Exception("Campos com * são obrigatórios!");
        }
        #endregion

        if (this.textData.Text != "" && Utilitario.IsDate(this.textData.Text)) {
            if (!Calendario.IsDiaUtil(Convert.ToDateTime(this.textData.Text), LocalFeriadoFixo.Brasil, TipoFeriado.Brasil)) {
                throw new Exception("Dia não útil");
            }

            if (this.btnEditCodigo.Text != "") {
                DateTime dataReport = Convert.ToDateTime(this.textData.Text);
                Cliente cliente = new Cliente();
                cliente.LoadByPrimaryKey(Convert.ToInt32(this.btnEditCodigo.Text));
                DateTime dataImplantacao = cliente.DataImplantacao.Value;
                DateTime dataDia = cliente.DataDia.Value;

                if (dataImplantacao > dataReport) {
                    throw new Exception("Data passada anterior à data de implantação: " + dataImplantacao.ToString("d"));
                }

                if (dataReport > dataDia){
                    throw new Exception("Data passada posterior à data atual do cliente: " + dataDia.ToString("d"));                    
                }
            }
        }
    }

    /// <summary>
    /// Seleciona o relatorio a ser executado
    /// </summary>
    /// <param name="visao">PDF, Excel ou Report</param>
    private void SelecionaRelatorio(string visao) {
        Response.BufferOutput = true;
        //
        // Salva os parametros na session
        Session["textData"] = textData.Text;
        Session["btnEditCodigo"] = btnEditCodigo.Text;

        //Response.Redirect("~/Relatorios/Swap/ReportPosicaoSwap.aspx?Visao=" + visao);

        string url = "~/Relatorios/Swap/ReportPosicaoSwap.aspx?Visao=" + visao;
        string redirectURL = Page.ResolveClientUrl(url);

        string script = "window.location = '" + redirectURL + "';";
        ScriptManager.RegisterStartupScript(this, typeof(Page), "RedirectTo", script, true);
    }
}