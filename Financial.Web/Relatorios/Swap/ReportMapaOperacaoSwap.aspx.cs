using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Financial.Common;
using EntitySpaces.Interfaces;
using Financial.Relatorio;
using DevExpress.XtraReports.Web;

using System.Diagnostics;
using DevExpress.XtraPrinting;
using DevExpress.XtraReports.UI;
using System.IO;
using System.Globalization;


public partial class _ReportMapaOperacaoSwap : Financial.Web.Common.BasePage {
    /* Como usar o relatorio:
        DateTime dataInicio = new DateTime(2000, 1, 1);
        DateTime dataFim = new DateTime(2010, 1, 1);
        int idCliente = 1;
        //        
        //int idCliente = 32;
        //
        //ReportViewer1.Report = new ReportMapaOperacaoSwap(dataInicio, dataFim);
        ReportViewer1.Report = new ReportMapaOperacaoSwap(dataInicio, dataFim, idCliente);
     */
    new protected void Page_Load(object sender, EventArgs e) {
        string visao = Request.QueryString["Visao"].ToString();
        //
        //Response.Write(Request.Form["textDataInicio"].ToString());
        //Response.Write("<br>" +Request.Form["textDataFim"].ToString());
        //Response.Write("<br>" + Request.Form["btnEditCodigo"].ToString());
        
        DateTime dataInicio = new DateTime();
        DateTime dataFim = new DateTime();        
        int? idCliente = null;
        
        // Obrigatorio
        dataInicio = Convert.ToDateTime(Session["textDataInicio"]);
        dataFim = Convert.ToDateTime(Session["textDataFim"]);
        //Opcionais
        if (!String.IsNullOrEmpty((string)Session["btnEditCodigo"])) {
            idCliente = Convert.ToInt32(Session["btnEditCodigo"]);
        }
        
        if (visao == "Report") {
            ReportViewer1.Report = new ReportMapaOperacaoSwap(dataInicio, dataFim, idCliente);
        }
        else if (visao == "PDF") {
            #region PDF
            ReportMapaOperacaoSwap report = new ReportMapaOperacaoSwap(dataInicio, dataFim, idCliente);

            MemoryStream ms = new MemoryStream();
            report.ExportToPdf(ms);
            ms.Seek(0, SeekOrigin.Begin);
            Response.ClearContent();
            Response.ClearHeaders();
            Response.Buffer = true;
            Response.Cache.SetCacheability(HttpCacheability.Private);
            Response.ContentType = "application/pdf";
            Response.AddHeader("Content-Length", ms.Length.ToString(CultureInfo.CurrentCulture));
            Response.AddHeader("Content-Disposition", String.Format("attachment;filename=ReportMapaOperacaoSwap.pdf"));

            Response.BinaryWrite(ms.ToArray());
            ms.Close();
            HttpContext.Current.ApplicationInstance.CompleteRequest();
            Response.End();
            #endregion
        }
        else if (visao == "Excel") {
            #region Excel
            ReportMapaOperacaoSwap report = new ReportMapaOperacaoSwap(dataInicio, dataFim, idCliente);

            MemoryStream ms = new MemoryStream();
            report.ExportToXls(ms);
            ms.Seek(0, SeekOrigin.Begin);
            Response.ClearContent();
            Response.ClearHeaders();
            Response.Buffer = true;
            Response.Cache.SetCacheability(HttpCacheability.Private);
            Response.ContentType = "application/vnd.ms-excel";
            Response.AddHeader("Content-Length", ms.Length.ToString(CultureInfo.CurrentCulture));
            Response.AddHeader("Content-Disposition", String.Format("attachment;filename=ReportMapaOperacaoSwap.xls"));

            Response.BinaryWrite(ms.ToArray());
            ms.Close();
            HttpContext.Current.ApplicationInstance.CompleteRequest();
            Response.End();

            #endregion
        }            
    }
}
