﻿using System;
using Financial.Relatorio;
using System.IO;
using System.Web;
using System.Globalization;
using Financial.Web.Common;
using DevExpress.XtraReports.UI;
using System.Collections.Generic;

public partial class _ReportBalanceteContabil : BasePage {

    public enum TipoMovimento {
        Aberto = 1,
        Fechado = 2
    }

    new protected void Page_Load(object sender, EventArgs e) {        
        string visao = Request.QueryString["Visao"].ToString();

        // Obrigatorio
        List<DateTime> datas = (List<DateTime>)Session["textDatas"];        
        //
        int idCliente = Convert.ToInt32(Session["btnEditCodigo"]);
        int tipoMovimento = Convert.ToInt32(Session["tipoMovimento"]);
        //

        DateTime dataInicio = datas[0];
        DateTime dataFim = datas[1];

        if (visao != "Excel")
        {
            #region Controlado de forma separada pois em excel só é possível gerar 1 balancete por exportação
            ReportMaster reportMaster = new ReportMaster();
            reportMaster.CreateDocument();
            reportMaster.PrintingSystem.ContinuousPageNumbering = false;

            XtraReport report;
            if (tipoMovimento == (int)TipoMovimento.Aberto)
            {
                report = new ReportBalanceteContabilAberto(idCliente, dataInicio, dataFim);

                if (((ReportBalanceteContabilAberto)report).RelatorioTemDados())
                {

                    report.CreateDocument();

                    // Anexa o Conteudo do Relatorio no Relatorio Master
                    reportMaster.Pages.AddRange(report.PrintingSystem.Pages);
                }
            }
            else
            {
                report = new ReportBalanceteContabilFechado(idCliente, dataInicio, dataFim);

                if (((ReportBalanceteContabilFechado)report).RelatorioTemDados())
                {
                    report.CreateDocument();

                    // Anexa o Conteudo do Relatorio no Relatorio Master
                    reportMaster.Pages.AddRange(report.PrintingSystem.Pages);
                }
            }

            if (reportMaster.Pages.Count == 0)
            {
                ReportSemDados reportSemDados = new ReportSemDados();
                //
                reportSemDados.CreateDocument();

                // Anexa o Conteudo do Relatorio no Relatorio Master
                reportMaster.Pages.AddRange(reportSemDados.PrintingSystem.Pages);
            }

            if (visao == "Report")
            {
                ReportViewer1.Report = reportMaster;
            }
            else if (visao == "PDF")
            {
                #region PDF

                MemoryStream ms = new MemoryStream();
                reportMaster.ExportToPdf(ms);
                ms.Seek(0, SeekOrigin.Begin);
                Response.ClearContent();
                Response.ClearHeaders();
                Response.Buffer = true;
                Response.Cache.SetCacheability(HttpCacheability.Private);
                Response.ContentType = "application/pdf";
                Response.AddHeader("Content-Length", ms.Length.ToString(CultureInfo.CurrentCulture));
                Response.AddHeader("Content-Disposition", String.Format("attachment;filename=ReportBalanceteContabil.pdf"));

                Response.BinaryWrite(ms.ToArray());
                ms.Close();
                HttpContext.Current.ApplicationInstance.CompleteRequest();
                Response.End();
                #endregion
            }
            #endregion
        }
        else
        {
            #region Excel

            MemoryStream ms = new MemoryStream();

            XtraReport report;
            if (tipoMovimento == (int)TipoMovimento.Aberto)
            {
                report = new ReportBalanceteContabilAberto(idCliente, dataInicio, dataFim);

                if (((ReportBalanceteContabilAberto)report).RelatorioTemDados())
                {
                    report.CreateDocument();

                    report.ExportToXls(ms);
                }
                else
                {
                    ReportSemDados reportSemDados = new ReportSemDados();
                    //
                    reportSemDados.CreateDocument();

                    reportSemDados.ExportToXls(ms);
                }
            }
            else
            {
                report = new ReportBalanceteContabilFechado(idCliente, dataInicio, dataFim);

                if (((ReportBalanceteContabilFechado)report).RelatorioTemDados())
                {
                    report.CreateDocument();

                    report.ExportToXls(ms);
                }
                else
                {
                    ReportSemDados reportSemDados = new ReportSemDados();
                    //
                    reportSemDados.CreateDocument();

                    reportSemDados.ExportToXls(ms);
                }
            }

            ms.Seek(0, SeekOrigin.Begin);
            Response.ClearContent();
            Response.ClearHeaders();
            Response.Buffer = true;
            Response.Cache.SetCacheability(HttpCacheability.Private);
            Response.ContentType = "application/vnd.ms-excel";
            Response.AddHeader("Content-Length", ms.Length.ToString(CultureInfo.CurrentCulture));
            Response.AddHeader("Content-Disposition", String.Format("attachment;filename=ReportBalanceteContabil.xls"));

            Response.BinaryWrite(ms.ToArray());
            ms.Close();
            HttpContext.Current.ApplicationInstance.CompleteRequest();
            Response.End();

            #endregion
        }
    }
}