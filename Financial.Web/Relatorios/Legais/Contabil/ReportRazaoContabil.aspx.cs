using System;
using Financial.Relatorio;
using System.IO;
using System.Web;
using System.Globalization;
using Financial.Web.Common;
using Financial.Contabil;

public partial class _ReportRazaoContabil : BasePage {        

    new protected void Page_Load(object sender, EventArgs e) {        
        string visao = Request.QueryString["Visao"].ToString();

        // Obrigatorio
        DateTime dataInicio = Convert.ToDateTime(Session["textDataInicio"]);
        DateTime dataFim = Convert.ToDateTime(Session["textDataFim"]);
        int idCliente = Convert.ToInt32(Session["btnEditCodigo"]);
        string codigo = Convert.ToString(Session["textCodigo"]);
        string codigoReduzido = Convert.ToString(Session["textCodigoReduzido"]);
                
        if (visao == "Report") {
            ReportViewer1.Report = new ReportRazaoContabil(dataInicio, dataFim, idCliente, codigo, codigoReduzido);
        }
        else if (visao == "PDF") {
            #region PDF
            ReportRazaoContabil report = new ReportRazaoContabil(dataInicio, dataFim, idCliente, codigo, codigoReduzido);

            MemoryStream ms = new MemoryStream();
            report.ExportToPdf(ms);
            ms.Seek(0, SeekOrigin.Begin);
            Response.ClearContent();
            Response.ClearHeaders();
            Response.Buffer = true;
            Response.Cache.SetCacheability(HttpCacheability.Private);
            Response.ContentType = "application/pdf";
            Response.AddHeader("Content-Length", ms.Length.ToString(CultureInfo.CurrentCulture));
            Response.AddHeader("Content-Disposition", String.Format("attachment;filename=ReportRazaoContabil.pdf"));

            Response.BinaryWrite(ms.ToArray());
            ms.Close();
            HttpContext.Current.ApplicationInstance.CompleteRequest();
            Response.End();
            #endregion
        }
        else if (visao == "Excel") {
            #region Excel
            ReportRazaoContabil report = new ReportRazaoContabil(dataInicio, dataFim, idCliente, codigo, codigoReduzido);            

            MemoryStream ms = new MemoryStream();
            report.ExportToXls(ms);
            ms.Seek(0, SeekOrigin.Begin);
            Response.ClearContent();
            Response.ClearHeaders();
            Response.Buffer = true;
            Response.Cache.SetCacheability(HttpCacheability.Private);
            Response.ContentType = "application/vnd.ms-excel";
            Response.AddHeader("Content-Length", ms.Length.ToString(CultureInfo.CurrentCulture));
            Response.AddHeader("Content-Disposition", String.Format("attachment;filename=ReportRazaoContabil.xls"));

            Response.BinaryWrite(ms.ToArray());
            ms.Close();
            HttpContext.Current.ApplicationInstance.CompleteRequest();
            Response.End();

            #endregion
        }
    }
}