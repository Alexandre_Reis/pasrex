﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.UI.HtmlControls;
using System.Collections.Generic;
using log4net;

using EntitySpaces.Interfaces;
using DevExpress.Web;
using Financial.Web.Common;
using Financial.Security;
using Financial.Security.Enums;
using Financial.Investidor;
using System.Web.UI;
using Financial.Contabil;
using Financial.Util;

public partial class FiltroReportBalanceteContabil : FiltroReportBasePage {
    private static readonly ILog log = LogManager.GetLogger(typeof(FiltroReportBalanceteContabil));

    // Armazena as datas de execução do relatório
    private List<DateTime> listaDatas = new List<DateTime>();

    new protected void Page_Load(object sender, EventArgs e) {
        this.HasPopupCliente = true;
        base.Page_Load(sender, e);

        TrataTravamentoCampos();
    }

    private void TrataTravamentoCampos() {
        Usuario usuario = new Usuario();
        usuario.BuscaUsuario(HttpContext.Current.User.Identity.Name);
        int tipoTrava = usuario.TipoTrava.Value;

        if (tipoTrava == (int)TipoTravaUsuario.TravaCliente) {
            btnEditCodigo.Enabled = false;

            PermissaoCliente permissaoCliente = new PermissaoCliente();
            int? idCliente = permissaoCliente.RetornaClienteAssociado(HttpContext.Current.User.Identity.Name);

            if (idCliente.HasValue) {
                Cliente cliente = new Cliente();

                List<esQueryItem> campos = new List<esQueryItem>();
                campos.Add(cliente.Query.Apelido);
                cliente.LoadByPrimaryKey(campos, idCliente.Value);
                string apelido = cliente.Apelido;

                btnEditCodigo.Text = Convert.ToString(idCliente.Value);
                textNome.Text = apelido;
            }
        }
    }

    protected void ASPxCallback1_Callback(object source, DevExpress.Web.CallbackEventArgs e) {
        string nome = "";
        if (!String.IsNullOrEmpty(e.Parameter) && e.Parameter != "null") {
            int idCliente = Convert.ToInt32(e.Parameter);
            Cliente cliente = new Cliente();
            cliente.LoadByPrimaryKey(idCliente);

            if (cliente.str.Apelido != "") {
                if (cliente.IsAtivo) {
                    PermissaoCliente permissaoCliente = new PermissaoCliente();
                    nome = permissaoCliente.RetornaAcessoClienteComControle(idCliente, HttpContext.Current.User.Identity.Name)
                            ? cliente.str.Apelido : "no_access";                                      
                }
                else {
                    nome = "no_active";
                }
            }
        }
        e.Result = nome;
    }

    protected void EsDSCliente_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e) {
        ClienteCollection clienteCollection = new ClienteCollection();
        clienteCollection.BuscaClientesComAcesso(HttpContext.Current.User.Identity.Name);

        e.Collection = clienteCollection;
    }

    protected void btnVisualiza_Click(object sender, EventArgs e) {
        this.TrataErros();
        this.SelecionaRelatorio(VisaoRelatorio.visaoHTML);
    }

    protected void btnPDF_Click(object sender, EventArgs e) {
        this.TrataErros();
        this.SelecionaRelatorio(VisaoRelatorio.visaoPDF);
    }

    protected void btnExcel_Click(object sender, EventArgs e) {
        this.TrataErros();

        if (this.textDataInicio.Text != this.textDataFim.Text)
        {
            throw new Exception("Para Relatório tipo Excel, só é possível escolher um mês específico.");
        }

        this.SelecionaRelatorio(VisaoRelatorio.visaoExcel);
    }

    /// <summary>
    /// Trata os Erros de Relatorio
    /// </summary>
    private void TrataErros()
    {

        #region Campos obrigatórios
        List<Control> controles = new List<Control>(new Control[] { 
    	    this.textDataInicio, this.textDataFim, this.btnEditCodigo});

        if (base.TestaObrigatorio(controles) != "")
            throw new Exception("Campos com * são obrigatórios!");

        DateTime dataInicio = Convert.ToDateTime(this.textDataInicio.Text);
        DateTime dataFim = Convert.ToDateTime(this.textDataFim.Text);

        if (dataFim < dataInicio)
            throw new Exception("Data início maior que data fim.");

        listaDatas = new List<DateTime>();
        listaDatas.Add(dataInicio);
        listaDatas.Add(dataFim);

        Cliente cliente = new Cliente();
        cliente.LoadByPrimaryKey(Convert.ToInt32(this.btnEditCodigo.Text));

        if (Calendario.RetornaPrimeiroDiaCorridoMes(cliente.DataImplantacao.Value, 0) > dataInicio)
        {
            throw new Exception("Data de implantação " + cliente.DataImplantacao.Value.ToShortDateString() + " é posterior à data solicitada do relatório.");
        }
        #endregion
    }

    /// <summary>
    /// Seleciona o relatorio a ser executado
    /// </summary>
    /// <param name="visao">PDF, Excel ou Report</param>
    private void SelecionaRelatorio(string visao) {
        Response.BufferOutput = true;
        //
        // Salva os parametros na session
        Session["textDatas"] = listaDatas; // lista de Datas mes a mes em ordem crescente
        Session["btnEditCodigo"] = this.btnEditCodigo.Text;
        Session["tipoMovimento"] = this.dropTipoMovimento.SelectedItem.Value;        
        //
        //Response.Redirect("~/Relatorios/Legais/Contabil/ReportBalanceteContabil.aspx?Visao=" + visao);

        string url = "~/Relatorios/Legais/Contabil/ReportBalanceteContabil.aspx?Visao=" + visao;
        string redirectURL = Page.ResolveClientUrl(url);

        string script = "window.location = '" + redirectURL + "';";
        ScriptManager.RegisterStartupScript(this, typeof(Page), "RedirectTo", script, true);
    }
}