﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.UI.HtmlControls;
using System.Collections.Generic;
using log4net;

using EntitySpaces.Interfaces;
using DevExpress.Web;
using Financial.Web.Common;
using Financial.Security;
using Financial.Security.Enums;
using Financial.Investidor;
using System.Web.UI;
using Financial.Contabil;
using Financial.Util;

public partial class FiltroReportRazaoContabil : FiltroReportBasePage {
    private static readonly ILog log = LogManager.GetLogger(typeof(FiltroReportRazaoContabil));

    new protected void Page_Load(object sender, EventArgs e) {
        this.HasPopupCliente = true;
        this.HasPopupContabConta = true;
        base.Page_Load(sender, e);

        TrataTravamentoCampos();
    }

    private void TrataTravamentoCampos() {
        Usuario usuario = new Usuario();
        usuario.BuscaUsuario(HttpContext.Current.User.Identity.Name);
        int tipoTrava = usuario.TipoTrava.Value;

        if (tipoTrava == (int)TipoTravaUsuario.TravaCliente) {
            btnEditCodigo.Enabled = false;

            PermissaoCliente permissaoCliente = new PermissaoCliente();
            int? idCliente = permissaoCliente.RetornaClienteAssociado(HttpContext.Current.User.Identity.Name);

            if (idCliente.HasValue) {
                Cliente cliente = new Cliente();

                List<esQueryItem> campos = new List<esQueryItem>();
                campos.Add(cliente.Query.Apelido);
                cliente.LoadByPrimaryKey(campos, idCliente.Value);
                string apelido = cliente.Apelido;

                btnEditCodigo.Text = Convert.ToString(idCliente.Value);
                textNome.Text = apelido;
            }
        }
    }

    protected void ASPxCallback1_Callback(object source, DevExpress.Web.CallbackEventArgs e) {
        string nome = "";
        if (!String.IsNullOrEmpty(e.Parameter) && e.Parameter != "null") {
            int idCliente = Convert.ToInt32(e.Parameter);
            Cliente cliente = new Cliente();
            cliente.LoadByPrimaryKey(idCliente);

            if (cliente.str.Apelido != "") {
                if (cliente.IsAtivo) {
                    PermissaoCliente permissaoCliente = new PermissaoCliente();
                    nome = permissaoCliente.RetornaAcessoClienteComControle(idCliente, HttpContext.Current.User.Identity.Name)
                            ? cliente.str.Apelido : "no_access";                                      
                }
                else {
                    nome = "no_active";
                }
            }
        }
        e.Result = nome;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void ASPxCallbackConta_Callback(object source, DevExpress.Web.CallbackEventArgs e) {
        string texto = "";
        if (!String.IsNullOrEmpty(e.Parameter) && e.Parameter != "null") {
            int idConta = Convert.ToInt32(e.Parameter);

            ContabContaCollection contabContaCollection = new ContabContaCollection();
            contabContaCollection.Query.Select(contabContaCollection.Query.Codigo,
                                               contabContaCollection.Query.CodigoReduzida);

            contabContaCollection.Query.Where(contabContaCollection.Query.IdConta == idConta);

            contabContaCollection.Query.Load();

            if (contabContaCollection.HasData) {
                string codigo = contabContaCollection[0].Codigo;
                string codigoReduzida = contabContaCollection[0].CodigoReduzida;
                
                texto = codigo.ToString() + "|" + codigoReduzida + "|" + idConta.ToString();
            }
        }
        e.Result = texto;
    }

    protected void EsDSCliente_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e) {
        ClienteCollection clienteCollection = new ClienteCollection();
        clienteCollection.BuscaClientesComAcesso(HttpContext.Current.User.Identity.Name);

        e.Collection = clienteCollection;
    }

    protected void EsDSContabConta_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e) {
        ContabContaQuery contabContaQuery = new ContabContaQuery("C");
        ContabPlanoQuery contabPlanoQuery = new ContabPlanoQuery("P");
        //
        contabContaQuery.Select(contabContaQuery.IdConta,
                                contabContaQuery.Descricao,
                                contabContaQuery.Codigo,
                                contabContaQuery.CodigoReduzida,
                                contabPlanoQuery.Descricao.As("DescricaoPlano"));
        contabContaQuery.InnerJoin(contabPlanoQuery).On(contabPlanoQuery.IdPlano == contabContaQuery.IdPlano);
        contabContaQuery.OrderBy(contabContaQuery.IdPlano.Ascending,
                                 contabContaQuery.Codigo.Ascending);
        //
        ContabContaCollection coll = new ContabContaCollection();
        coll.Load(contabContaQuery);
        //     
        e.Collection = coll;
    }

    protected void btnVisualiza_Click(object sender, EventArgs e) {
        this.TrataErros();
        this.SelecionaRelatorio(VisaoRelatorio.visaoHTML);
    }

    protected void btnPDF_Click(object sender, EventArgs e) {
        this.TrataErros();
        this.SelecionaRelatorio(VisaoRelatorio.visaoPDF);
    }

    protected void btnExcel_Click(object sender, EventArgs e) {
        this.TrataErros();
        this.SelecionaRelatorio(VisaoRelatorio.visaoExcel);
    }

    /// <summary>
    /// Trata os Erros de Relatorio
    /// </summary>
    private void TrataErros() {

        List<Control> controles = new List<Control>(new Control[] { 
    	    this.textDataInicio, this.textDataFim, this.btnEditCodigo, this.btnEditConta });

        if (base.TestaObrigatorio(controles) != "") {
            throw new Exception("Campos com * são obrigatórios!");
        }

        int idCliente = Convert.ToInt32(this.btnEditCodigo.Text);
        int idConta = Convert.ToInt32(this.hiddenIdConta.Text);

        DateTime dataInicio = Convert.ToDateTime(this.textDataInicio.Text);
        DateTime dataFim = Convert.ToDateTime(this.textDataFim.Text);
        
        ContabSaldo contabSaldo = new ContabSaldo();
        if (!contabSaldo.LoadByPrimaryKey(idCliente, idConta, dataInicio))
        {
            throw new Exception("Data início sem saldo contábil.");
        }        
    }

    /// <summary>
    /// Seleciona o relatorio a ser executado
    /// </summary>
    /// <param name="visao">PDF, Excel ou Report</param>
    private void SelecionaRelatorio(string visao)     
    {        
        Response.BufferOutput = true;
        //
        // Salva os parametros na session
        Session["textDataInicio"] = this.textDataInicio.Text;
        Session["textDataFim"] = this.textDataFim.Text;
        Session["btnEditCodigo"] = this.btnEditCodigo.Text;
        Session["textCodigo"] = this.btnEditConta.Text.Trim();
        Session["textCodigoReduzido"] = this.textContaReduzida.Text.Trim();
        
        //
        //Response.Redirect("~/Relatorios/Legais/Contabil/ReportRazaoContabil.aspx?Visao=" + visao);

        string url = "~/Relatorios/Legais/Contabil/ReportRazaoContabil.aspx?Visao=" + visao;
        string redirectURL = Page.ResolveClientUrl(url);

        string script = "window.location = '" + redirectURL + "';";
        ScriptManager.RegisterStartupScript(this, typeof(Page), "RedirectTo", script, true);
    }
}