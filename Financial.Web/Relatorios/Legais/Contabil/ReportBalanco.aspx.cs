﻿using System;
using Financial.Relatorio;
using System.IO;
using System.Web;
using System.Globalization;
using Financial.Web.Common;
using DevExpress.XtraReports.UI;
using Financial.Util;

public partial class _ReportBalanco : BasePage {
  
    new protected void Page_Load(object sender, EventArgs e) {        
        string visao = Request.QueryString["Visao"].ToString();

        // Obrigatorio        
        DateTime dataAux = new DateTime(Convert.ToInt32(Session["textData"]), 1, 1);
        //
        // Último dia útil do Mês
        DateTime data = Calendario.RetornaUltimoDiaUtilAno(dataAux);

        int idCliente = Convert.ToInt32(Session["btnEditCodigo"]);

        if (visao == "Report") {
            ReportViewer1.Report = new ReportBalanco(idCliente, data);
        }
        else if (visao == "PDF") {
            #region PDF
            ReportBalanco report = new ReportBalanco(idCliente, data);

            MemoryStream ms = new MemoryStream();
            report.ExportToPdf(ms);
            ms.Seek(0, SeekOrigin.Begin);
            Response.ClearContent();
            Response.ClearHeaders();
            Response.Buffer = true;
            Response.Cache.SetCacheability(HttpCacheability.Private);
            Response.ContentType = "application/pdf";
            Response.AddHeader("Content-Length", ms.Length.ToString(CultureInfo.CurrentCulture));
            Response.AddHeader("Content-Disposition", String.Format("attachment;filename=ReportBalanco.pdf"));

            Response.BinaryWrite(ms.ToArray());
            ms.Close();
            HttpContext.Current.ApplicationInstance.CompleteRequest();
            Response.End();
            #endregion
        }
        else if (visao == "Excel") {
            #region Excel
            ReportBalanco report = new ReportBalanco(idCliente, data);

            MemoryStream ms = new MemoryStream();
            report.ExportToXls(ms);
            ms.Seek(0, SeekOrigin.Begin);
            Response.ClearContent();
            Response.ClearHeaders();
            Response.Buffer = true;
            Response.Cache.SetCacheability(HttpCacheability.Private);
            Response.ContentType = "application/vnd.ms-excel";
            Response.AddHeader("Content-Length", ms.Length.ToString(CultureInfo.CurrentCulture));
            Response.AddHeader("Content-Disposition", String.Format("attachment;filename=ReportBalanco.xls"));

            Response.BinaryWrite(ms.ToArray());
            ms.Close();
            HttpContext.Current.ApplicationInstance.CompleteRequest();
            Response.End();

            #endregion
        }
    }
}