using System;
using Financial.Relatorio;
using System.IO;
using System.Globalization;
using System.Web;
using Financial.Investidor;
using Financial.Investidor.Enums;

public partial class _ReportDesempenhoClubes : Financial.Web.Common.BasePage {
    new protected void Page_Load(object sender, EventArgs e) {
        string visao = Request.QueryString["Visao"].ToString();
               
        int? idCliente = null;
        //
        if (!String.IsNullOrEmpty((string)Session["btnEditCodigo"])) {
            idCliente = Convert.ToInt32(Session["btnEditCodigo"]);
        }

        int mesInicial = Convert.ToInt32(Session["dropMesInicial"]);
        int mesFinal = Convert.ToInt32(Session["dropMesFinal"]);
        int ano = Convert.ToInt32(Session["dropAno"]);
        //
        /*******************************************************************************************************/
                
        ReportMaster reportMaster = new ReportMaster();
        reportMaster.CreateDocument();
        //

        if (idCliente.HasValue) {
            ReportDesempenhoClubes reportDesempenhoClubes = new ReportDesempenhoClubes(idCliente, ano, mesInicial, mesFinal);

            reportDesempenhoClubes.CreateDocument();
            // Anexa o Conteudo do Relatorio no Relatorio Master
            reportMaster.Pages.AddRange(reportDesempenhoClubes.PrintingSystem.Pages);
        }
        else {
            ClienteCollection clienteCollection = new ClienteCollection();
            clienteCollection.Query.Where(clienteCollection.Query.StatusAtivo == (byte)StatusAtivoCliente.Ativo,
                                          clienteCollection.Query.TipoControle == (byte)TipoControleCliente.Completo,
                                          clienteCollection.Query.IdTipo == (byte)TipoClienteFixo.Clube);

            clienteCollection.Query.Load();

            for (int i = 0; i < clienteCollection.Count; i++) {
                ReportDesempenhoClubes reportDesempenhoClubes = new ReportDesempenhoClubes(clienteCollection[i].IdCliente.Value, ano, mesInicial, mesFinal);

                reportDesempenhoClubes.CreateDocument();
                // Anexa o Conteudo do Relatorio no Relatorio Master
                reportMaster.Pages.AddRange(reportDesempenhoClubes.PrintingSystem.Pages);
            }
        }

        if (visao == "Report") {
            ReportViewer1.Report = reportMaster;
        }
        else if (visao == "PDF") {
            #region PDF
            MemoryStream ms = new MemoryStream();
            reportMaster.ExportToPdf(ms);
            ms.Seek(0, SeekOrigin.Begin);
            Response.ClearContent();
            Response.ClearHeaders();
            Response.Buffer = true;
            Response.Cache.SetCacheability(HttpCacheability.Private);
            Response.ContentType = "application/pdf";
            Response.AddHeader("Content-Length", ms.Length.ToString(CultureInfo.CurrentCulture));
            Response.AddHeader("Content-Disposition", String.Format("attachment;filename=ReportDesempenhoClubes.pdf"));

            Response.BinaryWrite(ms.ToArray());
            ms.Close();
            HttpContext.Current.ApplicationInstance.CompleteRequest();
            Response.End();
            #endregion
        }
        else if (visao == "Excel") {
            #region Excel
            
            MemoryStream ms = new MemoryStream();
            reportMaster.ExportToXls(ms);
            ms.Seek(0, SeekOrigin.Begin);
            Response.ClearContent();
            Response.ClearHeaders();
            Response.Buffer = true;
            Response.Cache.SetCacheability(HttpCacheability.Private);
            Response.ContentType = "application/vnd.ms-excel";
            Response.AddHeader("Content-Length", ms.Length.ToString(CultureInfo.CurrentCulture));
            Response.AddHeader("Content-Disposition", String.Format("attachment;filename=ReportDesempenhoClubes.xls"));

            Response.BinaryWrite(ms.ToArray());
            ms.Close();
            HttpContext.Current.ApplicationInstance.CompleteRequest();
            Response.End();

            #endregion
        }
    }
}