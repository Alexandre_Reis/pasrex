using System;
using Financial.Relatorio;
using System.IO;
using System.Web;
using System.Globalization;
using Financial.Web.Common;

public partial class _ReportInformeBovespaClube : BasePage {
    private static class VisaoRelatorio {
        public static string visaoHTML = "Report";
        public static string visaoPDF = "PDF";
        public static string visaoExcel = "Excel";
    }

    /* Como usar: 
        DateTime dataPosicao = new DateTime(2007, 2, 5);
        int idCarteira = 2;
        //                
        ReportViewer1.Report = new ReportInformeBovespaClube(idCarteira, dataPosicao);     
     */
    new protected void Page_Load(object sender, EventArgs e) {
        string visao = Request.QueryString["Visao"].ToString();
        //
        //Response.Write(Request.Form["textData"].ToString());
        //Response.Write("<br>" + Request.Form["btnEditCodigo"].ToString());

        DateTime data = new DateTime();
        int? idCarteira = null;

        // Obrigatorio
        data = Convert.ToDateTime(Session["textData"]);            
        idCarteira = Convert.ToInt32(Session["btnEditCodigo"]);

        if (visao == VisaoRelatorio.visaoHTML) {
            //try {
                ReportViewer1.Report = new ReportInformeBovespaClube(idCarteira.Value, data);
            //}
            //catch (HistoricoCotaNaoCadastradoException e1) {
            //    System.Windows.Forms.MessageBox.Show("Test");                   
           // }
           // catch (Exception) {
           //     System.Windows.Forms.MessageBox.Show("Test");
           // }            

        }
        else if (visao == VisaoRelatorio.visaoPDF) {
            #region PDF
            ReportInformeBovespaClube report = new ReportInformeBovespaClube(idCarteira.Value, data);     

            MemoryStream ms = new MemoryStream();
            report.ExportToPdf(ms);
            ms.Seek(0, SeekOrigin.Begin);
            Response.ClearContent();
            Response.ClearHeaders();
            Response.Buffer = true;
            Response.Cache.SetCacheability(HttpCacheability.Private);
            Response.ContentType = "application/pdf";
            Response.AddHeader("Content-Length", ms.Length.ToString(CultureInfo.CurrentCulture));
            Response.AddHeader("Content-Disposition", String.Format("attachment;filename=ReportInformeBovespaClube.pdf"));

            Response.BinaryWrite(ms.ToArray());
            ms.Close();
            HttpContext.Current.ApplicationInstance.CompleteRequest();
            Response.End();
            #endregion
        }
        else if (visao == VisaoRelatorio.visaoExcel) {
            #region Excel
            ReportInformeBovespaClube report = new ReportInformeBovespaClube(idCarteira.Value, data);     

            MemoryStream ms = new MemoryStream();
            report.ExportToXls(ms);
            ms.Seek(0, SeekOrigin.Begin);
            Response.ClearContent();
            Response.ClearHeaders();
            Response.Buffer = true;
            Response.Cache.SetCacheability(HttpCacheability.Private);
            Response.ContentType = "application/vnd.ms-excel";
            Response.AddHeader("Content-Length", ms.Length.ToString(CultureInfo.CurrentCulture));
            Response.AddHeader("Content-Disposition", String.Format("attachment;filename=ReportInformeBovespaClube.xls"));

            Response.BinaryWrite(ms.ToArray());
            ms.Close();
            HttpContext.Current.ApplicationInstance.CompleteRequest();
            Response.End();

            #endregion
        }            
    }
}