﻿using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Financial.Common;
using EntitySpaces.Interfaces;
using System.Collections.Generic;
using Financial.Relatorio;
using System.IO;
using System.Globalization;
using Financial.CRM.Enums;
using Financial.InvestidorCotista;
using Financial.Util;
using Financial.Fundo;
using Financial.Fundo.Enums;
using DevExpress.XtraPrinting;
using Financial.Web.Common;
using Financial.WebConfigConfiguration;
using System.Web.Hosting;
using Financial.Export.Cetip;

public partial class _ReportInformeRendimentos : BasePage {

    // Estrutura com o Cotista e suas Carteiras
    protected class TipoCotista
    {
        public int idCotista;
        public List<int> listaCarteiras;

        // Construtor
        public TipoCotista(int idCotista, List<int> listaCarteiras)
        {
            this.idCotista = idCotista;
            this.listaCarteiras = listaCarteiras;
        }
    };

    // Estrutura com a carteira e seus Cotistas
    protected class TipoCarteira
    {
        public int idCarteira;
        public List<int> listaCotistas;

        // Construtor
        public TipoCarteira(int idCarteira, List<int> listaCotistas)
        {
            this.idCarteira = idCarteira;
            this.listaCotistas = listaCotistas;
        }
    };

    private static class VisaoRelatorio
    {
        public const string visaoHTML = "Report";
        public const string visaoPDF = "PDF";
        public const string visaoExcel = "Excel";
    }

    private static class TipoRelatorio
    {
        public static string Cotista = "Cotista";
        public static string Carteira = "Carteira";
    }

    new protected void Page_Load(object sender, EventArgs e) {

        #region Parametros
        string visao = Request.QueryString["Visao"].ToString();
        // Obrigatórios
        int ano = (int)Session["dropAno"];

        //Opcionais        
        int? idCarteira = null;
        int? idCotista = null;
        string tipoRelatorio = Convert.ToString(Session["tipoRelatorio"]);
        //
        if (!String.IsNullOrEmpty((string)Session["btnEditCodigoCarteira"]))
        {
            idCarteira = Convert.ToInt32(Session["btnEditCodigoCarteira"]);
        }
        if (!String.IsNullOrEmpty((string)Session["btnEditCodigoCotista"]))
        {
            idCotista = Convert.ToInt32(Session["btnEditCodigoCotista"]);
        }
        #endregion

        string fileName = "InformeRendimento_" + ano.ToString();

        CotistaCollection cotistaCollection = new CotistaCollection();

        //Datas para a Posicao               
        DateTime dataInicioPosicao = Calendario.RetornaUltimoDiaUtilAno(new DateTime(ano - 1, 1, 1));
        DateTime dataFimPosicao = Calendario.RetornaUltimoDiaUtilAno(new DateTime(ano, 1, 1));

        //Datas para a Operacao
        DateTime dataInicioOperacao = Calendario.RetornaPrimeiroDiaUtilAno(new DateTime(ano, 1, 1));
        DateTime dataFimOperacao = dataFimPosicao;

        //
        ReportMaster reportMaster = new ReportMaster();
        reportMaster.CreateDocument();
        //
        string login = HttpContext.Current.User.Identity.Name;
        //
        if (tipoRelatorio == TipoRelatorio.Carteira) 
        {
            #region Opção Escolhida = Carteira
            // Executa Relatorio com 1 Carteira e 1 a n Cotistas
            // Chamada: n vezes

            List<int> listaIdCotista = cotistaCollection.BuscaCotistasDaCarteira(idCarteira.Value, dataInicioPosicao, dataFimPosicao,
                                                            dataInicioOperacao, dataFimOperacao, login);
            //
            TipoCarteira tipoCarteira = new TipoCarteira(idCarteira.Value, listaIdCotista);
            for (int i = 0; i < tipoCarteira.listaCotistas.Count; i++) 
            {
                Cotista cotista = new Cotista();
                cotista.LoadByPrimaryKey(tipoCarteira.listaCotistas[i]);
                TipoPessoa tipoPessoa = (TipoPessoa)cotista.UpToPessoaByIdPessoa.Tipo.Value;

                if (tipoPessoa == TipoPessoa.Fisica)
                {
                    #region Pessoa Fisica
                    if (tipoCarteira.idCarteira != tipoCarteira.listaCotistas[i])
                    {
                        ReportInformeRendimentosPF reportInformeRendimentosPF = new ReportInformeRendimentosPF(tipoCarteira.idCarteira, tipoCarteira.listaCotistas[i], ano);
                        // Cria o relatorio para 1 cotista
                        reportInformeRendimentosPF.CreateDocument();
                        // Anexa o Conteudo do Relatorio no Relatorio Master
                        reportMaster.Pages.AddRange(reportInformeRendimentosPF.PrintingSystem.Pages);

                        this.AdicionaContraCapa(reportMaster, tipoCarteira.listaCotistas[i]);
                    }
                    #endregion
                }
                else if (tipoPessoa == TipoPessoa.Juridica)
                {
                    #region Pessoa Juridica
                    if (tipoCarteira.idCarteira != tipoCarteira.listaCotistas[i])
                    {
                        ReportInformeRendimentosPJ reportInformeRendimentosPJ = new ReportInformeRendimentosPJ(tipoCarteira.idCarteira, tipoCarteira.listaCotistas[i], ano);
                        // Cria o relatorio para 1 cotista
                        reportInformeRendimentosPJ.CreateDocument();
                        // Anexa o Conteudo do Relatorio no Relatorio Master
                        reportMaster.Pages.AddRange(reportInformeRendimentosPJ.PrintingSystem.Pages);

                        this.AdicionaContraCapa(reportMaster, tipoCarteira.listaCotistas[i]);
                    }
                    #endregion
                }                                  
            }
            #endregion
        }
        else if (tipoRelatorio == TipoRelatorio.Cotista) 
        {
            #region Opção Escolhida = Cotista
            // Executa Relatorio com 1 cotista e 1 a n Carteiras
            // Chamada: n vezes

            List<int> listaIdCarteira = cotistaCollection.BuscaCarteirasDoCotista(idCotista.Value, dataInicioPosicao, dataFimPosicao,
                                                            dataInicioOperacao, dataFimOperacao);
            //
            TipoCotista tipoCotista = new TipoCotista(idCotista.Value, listaIdCarteira);

            for (int i = 0; i < tipoCotista.listaCarteiras.Count; i++)
            {
                Cotista cotista = new Cotista();
                cotista.LoadByPrimaryKey(tipoCotista.idCotista);
                TipoPessoa tipoPessoa = (TipoPessoa)cotista.UpToPessoaByIdPessoa.Tipo.Value;

                if (tipoPessoa == TipoPessoa.Fisica)
                {
                    #region Pessoa Fisica
                    if (tipoCotista.listaCarteiras[i] != tipoCotista.idCotista)
                    {
                        ReportInformeRendimentosPF reportInformeRendimentosPF = new ReportInformeRendimentosPF(tipoCotista.listaCarteiras[i], tipoCotista.idCotista, ano);
                        // Cria o relatorio para 1 cotista
                        reportInformeRendimentosPF.CreateDocument();
                        // Anexa o Conteudo do Relatorio no Relatorio Master
                        reportMaster.Pages.AddRange(reportInformeRendimentosPF.PrintingSystem.Pages);

                        this.AdicionaContraCapa(reportMaster, tipoCotista.idCotista);
                    }
                    #endregion
                }
                else if (tipoPessoa == TipoPessoa.Juridica)
                {
                    #region Pessoa Juridica
                    if (tipoCotista.listaCarteiras[i] != tipoCotista.idCotista)
                    {
                        ReportInformeRendimentosPJ reportInformeRendimentosPJ = new ReportInformeRendimentosPJ(tipoCotista.listaCarteiras[i], tipoCotista.idCotista, ano);
                        // Cria o relatorio para 1 cotista
                        reportInformeRendimentosPJ.CreateDocument();
                        // Anexa o Conteudo do Relatorio no Relatorio Master
                        reportMaster.Pages.AddRange(reportInformeRendimentosPJ.PrintingSystem.Pages);

                        this.AdicionaContraCapa(reportMaster, tipoCotista.idCotista);
                    }
                    #endregion
                }                                   
            }
            #endregion
        }

        #region Visoes HTML, PDF, Excel

        if (reportMaster.Pages.Count == 0) {
            ReportSemDados reportSemDados = new ReportSemDados();
            //
            reportSemDados.CreateDocument();

            // Anexa o Conteudo do Relatorio no Relatorio Master
            reportMaster.Pages.AddRange(reportSemDados.PrintingSystem.Pages);
        }

        MemoryStream ms = new MemoryStream();
        switch (visao) {
            case VisaoRelatorio.visaoHTML:
                #region Visao HTML
                //                
                ReportViewer1.Report = reportMaster;
                //
                break;
                #endregion
            case VisaoRelatorio.visaoPDF:
                #region Visao PDF
                //
                ms = new MemoryStream();
                reportMaster.ExportToPdf(ms);
                ms.Seek(0, SeekOrigin.Begin);
                Response.ClearContent();
                Response.ClearHeaders();
                Response.Buffer = true;
                Response.Cache.SetCacheability(HttpCacheability.Private);
                Response.ContentType = "application/pdf";
                Response.AddHeader("Content-Length", ms.Length.ToString(CultureInfo.CurrentCulture));
                Response.AddHeader("Content-Disposition", String.Format("attachment;filename=" + fileName + ".pdf"));

                Response.BinaryWrite(ms.ToArray());
                ms.Close();
                HttpContext.Current.ApplicationInstance.CompleteRequest();
                Response.End();
                //
                break;
                #endregion
            case VisaoRelatorio.visaoExcel:
                #region Visao Excel
                //
                ms = new MemoryStream();
                reportMaster.ExportToXls(ms);
                ms.Seek(0, SeekOrigin.Begin);
                Response.ClearContent();
                Response.ClearHeaders();
                Response.Buffer = true;
                Response.Cache.SetCacheability(HttpCacheability.Private);
                Response.ContentType = "application/vnd.ms-excel";
                Response.AddHeader("Content-Length", ms.Length.ToString(CultureInfo.CurrentCulture));
                Response.AddHeader("Content-Disposition", String.Format("attachment;filename=" + fileName + ".xls"));

                Response.BinaryWrite(ms.ToArray());
                ms.Close();
                HttpContext.Current.ApplicationInstance.CompleteRequest();
                Response.End();

                break;
                #endregion
        }
        //
        #endregion
    }

    /// <summary>
    /// Adiciona SubReport ContraCapa se existir a imagem capa_extrato_cotista
    /// Se não existir imagem não adiciona SubReport
    /// </summary>
    /// <param name="r">Report Master que contem o conteúdo do relatório</param>
    /// <param name="idCotista">IdCotista</param>
    private void AdicionaContraCapa(ReportMaster r, int idCotista) {

        string imagemCotistaContraPagina = "~/imagensPersonalizadas/" + "capa_extrato_cotista_" + WebConfig.AppSettings.Cliente + ".png";

        string pathImagem = HostingEnvironment.MapPath(imagemCotistaContraPagina);

        // Se existe imagem Adiciona SubReport ContraCapa
        if (File.Exists(pathImagem)) {
            SubReportContraPagina subReportContraPagina = new SubReportContraPagina(idCotista);
            subReportContraPagina.CreateDocument();
            //
            r.Pages.AddRange(subReportContraPagina.PrintingSystem.Pages);
            //r.Pages.Insert(posicao, subReportContraPagina.Pages[0]);
        }
    }
}