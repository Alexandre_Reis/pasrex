﻿using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.IO;
using System.Collections.Generic;
using System.Globalization;

using DevExpress.XtraReports.Web;
using DevExpress.XtraPrinting;
using DevExpress.XtraReports.UI;

using Financial.Web.Common;
using Financial.Relatorio;
using Financial.Security;
using Financial.Web.Util;
using Financial.Security.Enums;

public partial class _ReportGanhoRendaVariavel : BasePage {
    /* Como usar:
       int idCliente = 1;
       int ano = 2006;
       int mes = 1;

        ReportViewer1.Report = new ReportGanhoRendaVariavel(idCliente, mes, ano);         
     */

    private static class VisaoRelatorio {
        public const string visaoHTML = "Report";
        public const string visaoPDF = "PDF";
        public const string visaoExcel = "Excel";
    }

    private enum TipoRelatorioApuracaoIR
    {
        RendaVariavel = 1,
        FII = 2,
        Completo = 3
    }
    
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    new protected void Page_Load(object sender, EventArgs e) {
        string visao = Request.QueryString["Visao"].ToString();

        // Obrigatorio
        int idCliente = Convert.ToInt32(Session["btnEditCodigo"]);        
        int ano = Convert.ToInt32(Session["dropAno"]);
        int tipo = Convert.ToInt32(Session["dropTipo"]);

        #region Log do Processo
        HistoricoLog historicoLog = new HistoricoLog();
        historicoLog.InsereHistoricoLog(DateTime.Now,
                                        DateTime.Now,
                                        "Visualização Report Ganho de RV: Cliente " + idCliente.ToString(),
                                        HttpContext.Current.User.Identity.Name,
                                        UtilitarioWeb.GetIP(Request),
                                        "",
                                        HistoricoLogOrigem.Outros);
        #endregion

        string fileName = "ReportApuracaoIR" + ano.ToString();
        //
        ReportMaster reportMaster = new ReportMaster();
        reportMaster.CreateDocument();
        //
        /* Executa o Relatorio 3 vezes com os seguintes parâmetros        
        mes 4 - janeiro-abril
        mes 8 - maio-agosto
        mes 12 - setembro-dezembro
        */
        List<int> mes = new List<int>( new int[] { 4,8,12 } );

        if (tipo == (int)TipoRelatorioApuracaoIR.Completo || tipo == (int)TipoRelatorioApuracaoIR.RendaVariavel)
        {
            for (int i = 0; i < 3; i++)
            {
                ReportGanhoRendaVariavel reportGanhoRendaVariavel = new ReportGanhoRendaVariavel(idCliente, mes[i], ano);

                // Cria o relatorio com os resultados de 4 meses
                reportGanhoRendaVariavel.CreateDocument();
                // Anexa o Conteudo do Relatorio no Relatorio Master
                reportMaster.Pages.AddRange(reportGanhoRendaVariavel.PrintingSystem.Pages);
            }
        }

        if (tipo == (int)TipoRelatorioApuracaoIR.Completo || tipo == (int)TipoRelatorioApuracaoIR.FII)
        {
            for (int i = 0; i < 3; i++)
            {
                ReportGanhoFII reportGanhoFII = new ReportGanhoFII(idCliente, mes[i], ano);

                // Cria o relatorio com os resultados de 4 meses
                reportGanhoFII.CreateDocument();
                // Anexa o Conteudo do Relatorio no Relatorio Master
                reportMaster.Pages.AddRange(reportGanhoFII.PrintingSystem.Pages);
            }
        }

        #region Visoes HTML, PDF, Excel

        MemoryStream ms = new MemoryStream();
        switch (visao) {
            case VisaoRelatorio.visaoHTML:
                #region Visao HTML
                //                
                ReportViewer1.Report = reportMaster;
                //
                break;
                #endregion
            case VisaoRelatorio.visaoPDF:
                #region Visao PDF
                //
                ms = new MemoryStream();               
                reportMaster.ExportToPdf(ms);
                ms.Seek(0, SeekOrigin.Begin);
                Response.ClearContent();
                Response.ClearHeaders();
                Response.Buffer = true;
                Response.Cache.SetCacheability(HttpCacheability.Private);
                Response.ContentType = "application/pdf";
                Response.AddHeader("Content-Length", ms.Length.ToString(CultureInfo.CurrentCulture));
                Response.AddHeader("Content-Disposition", String.Format("attachment;filename=" + fileName + ".pdf"));

                Response.BinaryWrite(ms.ToArray());
                ms.Close();
                HttpContext.Current.ApplicationInstance.CompleteRequest();
                Response.End();
                //
                break;
                #endregion
            case VisaoRelatorio.visaoExcel:
                #region Visao Excel
                //
                //ms = new MemoryStream();

                ////ExportMode.DifferentFiles

                //XlsExportOptions options = new XlsExportOptions();                
                //reportMaster.ExportToXls(ms,options);
                //ms.Seek(0, SeekOrigin.Begin);
                //Response.ClearContent();
                //Response.ClearHeaders();
                //Response.Buffer = true;
                //Response.Cache.SetCacheability(HttpCacheability.Private);
                //Response.ContentType = "application/vnd.ms-excel";
                //Response.AddHeader("Content-Length", ms.Length.ToString(CultureInfo.CurrentCulture));
                //Response.AddHeader("Content-Disposition", String.Format("attachment;filename=" + fileName + ".xls"));

                //Response.BinaryWrite(ms.ToArray());
                //ms.Close();
                //HttpContext.Current.ApplicationInstance.CompleteRequest();
                //Response.End();

                break;
                #endregion
        }
        //
        #endregion
    }
}