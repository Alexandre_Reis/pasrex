﻿using System;
using System.Web;
using System.Globalization;
using System.IO;
using Financial.Relatorio;
using Financial.Web.Common;
using DevExpress.Web;
using Docs.Word;
using Financial.Common;
using Financial.CRM;
using Financial.Util;
using Financial.InvestidorCotista;
using Financial.Fundo;
using Financial.RendaFixa;
using Financial.RendaFixa.Enums;
using Financial.Fundo.Enums;
using Financial.Investidor;
using System.Collections.Generic;
using Dart.PowerTCP.Zip;

public partial class _ReportInformeRendimentosGeral : BasePage {

    new protected void Page_Load(object sender, EventArgs e) {
        int ano = Convert.ToInt32(Session["dropAno"]);
        int anoAnterior = ano - 1;

        DateTime dataAno = Calendario.RetornaUltimoDiaUtilAno(new DateTime(ano, 12, 31));
        DateTime dataAnoAnterior = Calendario.RetornaUltimoDiaUtilAno(new DateTime(anoAnterior, 12, 31));

        int? idParam = null;
        if (Session["btnEditCodigo"] != null && Session["btnEditCodigo"] != "") {
            idParam = Convert.ToInt32(Session["btnEditCodigo"]);
        }
        //

        int codigoBovespaDefault = Convert.ToInt32(ParametrosConfiguracaoSistema.Bolsa.CodigoAgenteDefault);

        List<int> listaClientes = new List<int>();
        
        #region Consultas

        OperacaoRendaFixaCollection operacaoRendaFixaCollection = new OperacaoRendaFixaCollection();
        operacaoRendaFixaCollection.Query.Select(operacaoRendaFixaCollection.Query.IdCliente);
        if (idParam.HasValue) {
            operacaoRendaFixaCollection.Query.Where(operacaoRendaFixaCollection.Query.IdCliente.Equal(idParam.Value));
        }

        operacaoRendaFixaCollection.Query.es.Distinct = true;
        operacaoRendaFixaCollection.Query.Load();
        foreach (OperacaoRendaFixa operacaoRendaFixaLista in operacaoRendaFixaCollection) {
            listaClientes.Add(operacaoRendaFixaLista.IdCliente.Value);
        }

        PosicaoCotistaHistoricoCollection posicaoCotistaHistoricoCollection = new PosicaoCotistaHistoricoCollection();
        posicaoCotistaHistoricoCollection.Query.Select(posicaoCotistaHistoricoCollection.Query.IdCotista);
        if (idParam.HasValue) {
            posicaoCotistaHistoricoCollection.Query.Where(posicaoCotistaHistoricoCollection.Query.IdCotista.Equal(idParam.Value));
        }

        posicaoCotistaHistoricoCollection.Query.es.Distinct = true;
        posicaoCotistaHistoricoCollection.Query.Load();
        foreach (PosicaoCotistaHistorico posicaoCotistaHistoricoLista in posicaoCotistaHistoricoCollection) {
            if (!listaClientes.Contains(posicaoCotistaHistoricoLista.IdCotista.Value)) {
                listaClientes.Add(posicaoCotistaHistoricoLista.IdCotista.Value);
            }
        } 
        #endregion

        // Dicionario nome do arquivo e memory stream do conteudo
        Dictionary<string, MemoryStream> msFiles = new Dictionary<string, MemoryStream>();

        // Zip
        Archive arquivo = new Archive();
        MemoryStream msZip = new MemoryStream();
        
        //
        foreach (int idCliente in listaClientes) {
            #region For de Clientes
            Pessoa pessoa = new Pessoa();
            pessoa.LoadByPrimaryKey(idCliente);

            AgenteMercado agenteMercado = new AgenteMercado();

            Carteira carteira = new Carteira();
            bool possuiAgente = false;
            if (carteira.LoadByPrimaryKey(idCliente))
            {
                int idAgenteAdm = (int)carteira.IdAgenteAdministrador;
                agenteMercado.LoadByPrimaryKey(idAgenteAdm);
            }
            else
            {
                int idAgenteAdm = agenteMercado.BuscaIdAgenteMercadoBovespa(ParametrosConfiguracaoSistema.Bolsa.CodigoAgenteDefault);
                agenteMercado.LoadByPrimaryKey(idAgenteAdm);
            }

            #region Info Arquivo
            const string MODELO_PLANILHA = "InformeRendimento.doc";
            string pathModelos = AppDomain.CurrentDomain.BaseDirectory + "\\Relatorios\\Modelos";
            string pathPlanilhaModelo = String.Format("{0}\\{1}", pathModelos, MODELO_PLANILHA);
            //               
            //Document doc = Document.ReadDOC(pathPlanilhaModelo);
            Document.SetLicenseCode("3961AN-CC1173-A1D396-1A0000");
            Document doc = Document.ReadDOC(pathPlanilhaModelo);

            //PageSettings PS = doc.Sections[0].Style.PageSettings;

            //	Change the page properties
            //PS.PageHeight = 500;
            //PS.PageWidth = 400;
            //PS.Margins.Top = 30;
            //PS.Margins.Bottom = 20;
            //PS.Margins.Left = 30;
            //PS.Margins.Right = 20;
            //
            NodeList nodes = doc.Sections.GetNodes(); 
            #endregion

            decimal saldoFinalFundos = 0;
            decimal saldoAnteriorFundos = 0;
            decimal rendimentoFundos = 0;
            #region Trata Fundos/Clubes de Investimentos
            posicaoCotistaHistoricoCollection = new PosicaoCotistaHistoricoCollection();
            posicaoCotistaHistoricoCollection.Query.Select(posicaoCotistaHistoricoCollection.Query.IdCarteira);
            posicaoCotistaHistoricoCollection.Query.Where(
                                     posicaoCotistaHistoricoCollection.Query.IdCotista.Equal(idCliente) &
                                     (posicaoCotistaHistoricoCollection.Query.DataHistorico == dataAno |
                                       posicaoCotistaHistoricoCollection.Query.DataHistorico == dataAnoAnterior) &
                                     posicaoCotistaHistoricoCollection.Query.IdCarteira.NotEqual(posicaoCotistaHistoricoCollection.Query.IdCotista));
            posicaoCotistaHistoricoCollection.Query.es.Distinct = true;

            posicaoCotistaHistoricoCollection.Query.Load();

            foreach (PosicaoCotistaHistorico posicaoCotistaHistorico in posicaoCotistaHistoricoCollection)
            {
                int idCarteira = posicaoCotistaHistorico.IdCarteira.Value;
                decimal saldoFinal = this.RetornaSaldoFundo(dataAno, idCliente, idCarteira);
                saldoFinalFundos += saldoFinal;

                decimal saldoAnterior = this.RetornaSaldoFundo(dataAnoAnterior, idCliente, idCarteira);
                saldoAnteriorFundos += saldoAnterior;

                OperacaoCotista operacaoCotista = new OperacaoCotista();
                operacaoCotista.Query
                               .Select((operacaoCotista.Query.VariacaoResgate - operacaoCotista.Query.ValorIR).As("RendimentoLiquido").Sum())
                               .Where(operacaoCotista.Query.IdCotista == idCliente,
                                      operacaoCotista.Query.IdCarteira == idCarteira,
                                      operacaoCotista.Query.DataConversao.Between(dataAnoAnterior, dataAno));

                operacaoCotista.Query.Load();

                decimal rendimentoLiquido = 0;
                if (operacaoCotista.es.HasData && !Convert.IsDBNull(operacaoCotista.GetColumn("RendimentoLiquido")))
                {
                    rendimentoLiquido = (decimal)operacaoCotista.GetColumn("RendimentoLiquido");
                    if (rendimentoLiquido > 0)
                        rendimentoFundos += rendimentoLiquido;
                }
            }
            #endregion

            decimal saldoFinalRF = 0;
            decimal saldoAnteriorRF = 0;
            decimal rendimentoRF = 0;
            #region Trata Renda Fixa
            PosicaoRendaFixaHistorico posicaoRendaFixaHistorico = new PosicaoRendaFixaHistorico();
            posicaoRendaFixaHistorico.Query.Select((posicaoRendaFixaHistorico.Query.Quantidade * posicaoRendaFixaHistorico.Query.PUOperacao).As("SaldoPosicao").Sum(),
                                                    posicaoRendaFixaHistorico.Query.Quantidade.Sum());
            posicaoRendaFixaHistorico.Query.Where(posicaoRendaFixaHistorico.Query.IdCliente.Equal(idCliente),
                                                  posicaoRendaFixaHistorico.Query.DataHistorico.Equal(dataAno));
            posicaoRendaFixaHistorico.Query.Load();

            if (posicaoRendaFixaHistorico.Quantidade.HasValue)
            {
                saldoFinalRF = (decimal)posicaoRendaFixaHistorico.GetColumn("SaldoPosicao");
            }

            posicaoRendaFixaHistorico = new PosicaoRendaFixaHistorico();
            posicaoRendaFixaHistorico.Query.Select((posicaoRendaFixaHistorico.Query.Quantidade * posicaoRendaFixaHistorico.Query.PUOperacao).As("SaldoPosicao").Sum(),
                                                   posicaoRendaFixaHistorico.Query.Quantidade.Sum());
            posicaoRendaFixaHistorico.Query.Where(posicaoRendaFixaHistorico.Query.IdCliente.Equal(idCliente),
                                                  posicaoRendaFixaHistorico.Query.DataHistorico.Equal(dataAnoAnterior));
            posicaoRendaFixaHistorico.Query.Load();

            if (posicaoRendaFixaHistorico.Quantidade.HasValue)
            {
                saldoAnteriorRF = (decimal)posicaoRendaFixaHistorico.GetColumn("SaldoPosicao");
            }

            LiquidacaoRendaFixa liquidacaoRendaFixa = new LiquidacaoRendaFixa();
            liquidacaoRendaFixa.Query.Select(liquidacaoRendaFixa.Query.Rendimento.Sum(),
                                             liquidacaoRendaFixa.Query.ValorIR.Sum());
            liquidacaoRendaFixa.Query.Where(liquidacaoRendaFixa.Query.IdCliente.Equal(idCliente),
                                            liquidacaoRendaFixa.Query.DataLiquidacao.GreaterThan(dataAnoAnterior),
                                            liquidacaoRendaFixa.Query.DataLiquidacao.LessThanOrEqual(dataAno),
                                            liquidacaoRendaFixa.Query.TipoLancamento.In((byte)TipoLancamentoLiquidacao.Juros,
                                                                                        (byte)TipoLancamentoLiquidacao.PagtoPrincipal,
                                                                                        (byte)TipoLancamentoLiquidacao.Vencimento,
                                                                                        (byte)TipoLancamentoLiquidacao.Venda));
            liquidacaoRendaFixa.Query.Load();

            if (liquidacaoRendaFixa.Rendimento.HasValue)
            {
                rendimentoRF = liquidacaoRendaFixa.Rendimento.Value - liquidacaoRendaFixa.ValorIR.Value;
            }
            #endregion

            #region Substituições contéudo Arquivo
            Search.Replace(nodes, "{0}", ano.ToString());
            Search.Replace(nodes, "{1}", agenteMercado.Nome);
            Search.Replace(nodes, "{2}", Utilitario.MascaraCNPJ(agenteMercado.Cnpj));
            Search.Replace(nodes, "{3}", pessoa.Cpfcnpj);
            Search.Replace(nodes, "{4}", pessoa.Nome);
            //
            Search.Replace(nodes, "{5}", saldoAnteriorFundos.ToString("n2"));
            Search.Replace(nodes, "{6}", saldoFinalFundos.ToString("n2"));
            Search.Replace(nodes, "{7}", rendimentoFundos.ToString("n2"));
            Search.Replace(nodes, "{8}", saldoAnteriorRF.ToString("n2"));
            Search.Replace(nodes, "{9}", saldoFinalRF.ToString("n2"));
            Search.Replace(nodes, "{10}", rendimentoRF.ToString("n2"));
            Search.Replace(nodes, "{11}", dataAno.ToString("dd/MM/yyyy"));
            // 
            #endregion

            // Escreve documento na memory stream
            MemoryStream ms = doc.WriteDOC();

            /* Necessário voltar o ponteiro do arquivo para o Inicio */
            ms.Seek(0, SeekOrigin.Begin);
            //                       
            string nomeArquivo = doc.Properties.DocumentName + "_" + idCliente.ToString() + ".doc";
            
            //Dicionario Final do Arquivo
            msFiles.Add(nomeArquivo, ms);            
            #endregion
        }




        #region Gera Zip

        Archive arquivoInforme = new Archive();

        // Saida
        MemoryStream msZipSaida = new MemoryStream();
        
        try {
            int k = 0;
            foreach (KeyValuePair<string, MemoryStream> pair in msFiles) {
                arquivoInforme.Add(pair.Value); // Memory Stream
                arquivoInforme[k].Name = pair.Key; // Nome do Arquivo
                //
                k++;
            }
            //
            arquivoInforme.Zip(msZipSaida);
        }
        catch (Exception e1) {
            throw new Exception(e1.Message + "Erro ao Zipar o Arquivo");
        }

        if (msFiles.Count == 1) { // word
            #region Gera Word
            List<MemoryStream> mWord = new List<MemoryStream>(msFiles.Values);

            Response.ClearContent();
            Response.ClearHeaders();
            Response.Buffer = true;
            Response.Cache.SetCacheability(HttpCacheability.Private);
            Response.ContentType = "application/vnd.ms-word";
            Response.AddHeader("Content-Length", mWord[0].Length.ToString(CultureInfo.CurrentCulture));
            Response.AddHeader("Content-Disposition", String.Format("attachment;filename=InformeRendimento_" + idParam.ToString() + ".doc"));
            //
            Response.BinaryWrite(mWord[0].ToArray());
            mWord[0].Close();
            HttpContext.Current.ApplicationInstance.CompleteRequest();
            Response.End();
            #endregion
        }

        else { // zip

            Response.ClearContent();
            Response.ClearHeaders();
            Response.Buffer = true;
            Response.Cache.SetCacheability(HttpCacheability.Private);
            Response.ContentType = "application/zip";
            Response.AddHeader("Content-Length", msZipSaida.Length.ToString(CultureInfo.CurrentCulture));
            Response.AppendHeader("Content-Disposition", String.Format("attachment;filename=InformeRendimento.zip"));

            Response.BinaryWrite(msZipSaida.ToArray());
            msZipSaida.Close();

            HttpContext.Current.ApplicationInstance.CompleteRequest();
            Response.End();
            
        }

        #endregion        
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="dataAno"></param>
    /// <param name="idCliente"></param>
    /// <param name="idCarteira"></param>
    /// <returns></returns>
    private decimal RetornaSaldoFundo(DateTime dataAno, int idCliente, int idCarteira)
    {
        Carteira carteira = new Carteira();
        carteira.Query.Select(carteira.Query.TipoTributacao, carteira.Query.TipoCota);
        carteira.Query.Where(carteira.Query.IdCarteira.Equal(idCarteira));
        carteira.Query.Load();

        byte tipoTributacao = carteira.TipoTributacao.Value;
        byte tipoCota = carteira.TipoCota.Value;

        decimal saldo = 0;
        if (tipoTributacao == (byte)TipoTributacaoFundo.Acoes)
        {
            #region Calculo para Fundo Acoes
            PosicaoCotistaHistorico posicaoCotistaHistoricoAcoes = new PosicaoCotistaHistorico();
            posicaoCotistaHistoricoAcoes.Query.Select(posicaoCotistaHistoricoAcoes.Query.Quantidade.Sum(),
                                                     (posicaoCotistaHistoricoAcoes.Query.Quantidade * posicaoCotistaHistoricoAcoes.Query.CotaAplicacao).As("SaldoPosicao").Sum());
            posicaoCotistaHistoricoAcoes.Query.Where(posicaoCotistaHistoricoAcoes.Query.IdCarteira.Equal(idCarteira),
                                                     posicaoCotistaHistoricoAcoes.Query.IdCotista.Equal(idCliente),
                                                     posicaoCotistaHistoricoAcoes.Query.DataHistorico.Equal(dataAno));
            posicaoCotistaHistoricoAcoes.Query.Load();


            if (posicaoCotistaHistoricoAcoes.Quantidade.HasValue)
            {
                saldo = (decimal)posicaoCotistaHistoricoAcoes.GetColumn("SaldoPosicao");
            }
            #endregion
        }
        else
        {
            #region Calculo para Fundo com comecotas
            PosicaoCotistaHistoricoCollection posicaoCotistaHistoricoCollectionAux = new PosicaoCotistaHistoricoCollection();
            posicaoCotistaHistoricoCollectionAux.Query.Select(posicaoCotistaHistoricoCollectionAux.Query.DataConversao,
                                                              posicaoCotistaHistoricoCollectionAux.Query.DataUltimaCobrancaIR,
                                                              posicaoCotistaHistoricoCollectionAux.Query.Quantidade,
                                                              posicaoCotistaHistoricoCollectionAux.Query.CotaAplicacao,
                                                              posicaoCotistaHistoricoCollectionAux.Query.CotaDia);
            posicaoCotistaHistoricoCollectionAux.Query.Where(posicaoCotistaHistoricoCollectionAux.Query.IdCarteira.Equal(idCarteira),
                                                             posicaoCotistaHistoricoCollectionAux.Query.IdCotista.Equal(idCliente),
                                                             posicaoCotistaHistoricoCollectionAux.Query.DataHistorico.Equal(dataAno));
            posicaoCotistaHistoricoCollectionAux.Query.Load();

            foreach (PosicaoCotistaHistorico posicaoCotistaHistoricoAux in posicaoCotistaHistoricoCollectionAux)
            {
                #region Computa Saldo Final do cotista
                DateTime dataConversao = posicaoCotistaHistoricoAux.DataConversao.Value;
                DateTime dataUltimaCobrancaIR = posicaoCotistaHistoricoAux.DataUltimaCobrancaIR.Value;
                decimal quantidadePosicao = posicaoCotistaHistoricoAux.Quantidade.Value;
                decimal cotaAplicacao = posicaoCotistaHistoricoAux.CotaAplicacao.Value;
                decimal cotaDia = posicaoCotistaHistoricoAux.CotaDia.Value;

                decimal cotaCusto = 0;
                decimal saldoPosicao = 0;
                if (dataUltimaCobrancaIR > dataConversao)
                {
                    HistoricoCota historicoCota = new HistoricoCota();
                    historicoCota.Query.Select(historicoCota.Query.CotaAbertura,
                                               historicoCota.Query.CotaFechamento);
                    historicoCota.Query.Where(historicoCota.Query.IdCarteira.Equal(idCarteira),
                                              historicoCota.Query.Data.Equal(dataUltimaCobrancaIR));
                    historicoCota.Query.Load();

                    if (tipoCota == (byte)TipoCotaFundo.Abertura)
                    {
                        cotaCusto = historicoCota.CotaAbertura.Value;
                    }
                    else
                    {
                        if (historicoCota.CotaFechamento.HasValue)
                        {
                            cotaCusto = historicoCota.CotaFechamento.Value;
                        }
                    }
                }
                else
                {
                    cotaCusto = cotaAplicacao;
                }

                saldoPosicao = quantidadePosicao * cotaCusto;

                saldo += saldoPosicao;
                #endregion
            }
            #endregion
        }

        return saldo;
    }
}