﻿using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Financial.Common;
using EntitySpaces.Interfaces;
using System.Collections.Generic;
using Financial.Relatorio;
using System.IO;
using System.Globalization;
using DevExpress.XtraPrinting;

public partial class _ReportDarf : Financial.Web.Common.BasePage {

    private static class VisaoRelatorio {
        public const string visaoHTML = "Report";
        public const string visaoPDF = "PDF";
    }

    new protected void Page_Load(object sender, EventArgs e) {

        string visao = Request.QueryString["Visao"].ToString();
        //
        int mes = Convert.ToInt32(Session["Mes"]);
        int ano = Convert.ToInt32(Session["Ano"]);
        int idCliente = Convert.ToInt32(Session["btnEditCodigo"]);

        string fileName = "Darf_" + mes.ToString().PadLeft(2,'0') + ano;

        if (visao == VisaoRelatorio.visaoHTML) {
            ReportViewer1.Report = new ReportDarf(idCliente, mes, ano);
        }
        else if (visao == "PDF") {
            #region PDF
            ReportDarf report = new ReportDarf(idCliente, mes, ano);

            MemoryStream ms = new MemoryStream();
            report.ExportToPdf(ms);
            ms.Seek(0, SeekOrigin.Begin);
            Response.ClearContent();
            Response.ClearHeaders();
            Response.Buffer = true;
            Response.Cache.SetCacheability(HttpCacheability.Private);
            Response.ContentType = "application/pdf";
            Response.AddHeader("Content-Length", ms.Length.ToString(CultureInfo.CurrentCulture));
            Response.AddHeader("Content-Disposition", String.Format("attachment;filename=" + fileName + ".pdf"));

            Response.BinaryWrite(ms.ToArray());
            ms.Close();
            HttpContext.Current.ApplicationInstance.CompleteRequest();
            Response.End();
            #endregion
        }
    }
}