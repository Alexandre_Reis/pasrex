﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using Financial.Fundo;
using Financial.Investidor;
using Financial.InvestidorCotista;
using Financial.Security;
using System.Collections.Generic;
using EntitySpaces.Interfaces;
using Financial.Security.Enums;
using System.Text;
using Financial.Web.Common;

public partial class FiltroReportInformeRendimentos : FiltroReportBasePage {
    // TODO: Traduzir
    const string MSG_ERRO1 = "Informe a Carteira";
    const string MSG_ERRO2 = "Informe o Cotista";

    private static class TipoRelatorio {
        public static string Cotista = "Cotista";
        public static string Carteira = "Carteira";
    }

    new protected void Page_Load(object sender, EventArgs e) {
        btnEditCodigoCarteira.Focus();

        // Carrega o Combo de Ano
        #region Combo de Ano em portugues
        // Ano Atual
        int anoAtual = DateTime.Today.Year;
        int primeiroMes = 1980; // Valor Fixo
        for (int i = anoAtual; i >= primeiroMes; i--) {
            ListItem listItem = new ListItem();
            //
            listItem.Text = Convert.ToString(i);
            listItem.Value = Convert.ToString(i);
            //
            this.dropAno.Items.Add(listItem);
        }
        #endregion

        this.HasPopupCarteira = true;
        this.HasPopupCotista = true;
        base.Page_Load(sender, e);

        TrataTravamentoCampos();

        if (!IsPostBack && !IsCallback) {
            dropTipoRelatorio.SelectedIndex = 0;
        }
    }

    private void TrataTravamentoCampos() {
        Usuario usuario = new Usuario();
        usuario.BuscaUsuario(HttpContext.Current.User.Identity.Name);
        int tipoTrava = usuario.TipoTrava.Value;

        #region TravaCliente
        if (tipoTrava == (int)TipoTravaUsuario.TravaCliente) {
            //Travamento do campo de carteira
            btnEditCodigoCarteira.Enabled = false;

            PermissaoCliente permissaoCliente = new PermissaoCliente();
            int? idCliente = permissaoCliente.RetornaClienteAssociado(HttpContext.Current.User.Identity.Name);

            if (idCliente.HasValue) {
                Cliente cliente = new Cliente();

                List<esQueryItem> campos = new List<esQueryItem>();
                campos.Add(cliente.Query.Apelido);
                cliente.LoadByPrimaryKey(campos, idCliente.Value);
                string apelido = cliente.Apelido;

                btnEditCodigoCarteira.Text = Convert.ToString(idCliente.Value);
                textNomeCarteira.Text = apelido;
            }
        }
        #endregion

        #region TravaCotista
        if (tipoTrava == (int)TipoTravaUsuario.TravaCotista) {
            //Travamento do campo de cotista
            btnEditCodigoCotista.Enabled = false;

            PermissaoCotista permissaoCotista = new PermissaoCotista();
            int? idCotista = permissaoCotista.RetornaCotistaAssociado(HttpContext.Current.User.Identity.Name);

            if (idCotista.HasValue) {
                Cotista cotista = new Cotista();

                List<esQueryItem> campos = new List<esQueryItem>();
                campos.Add(cotista.Query.Apelido);
                cotista.LoadByPrimaryKey(campos, idCotista.Value);
                string apelido = cotista.Apelido;

                btnEditCodigoCotista.Text = Convert.ToString(idCotista.Value);
                textNomeCotista.Text = apelido;
            }
        }
        #endregion
    }

    protected void ASPxCallback1_Callback(object source, DevExpress.Web.CallbackEventArgs e) {
        string nome = "";
        if (!String.IsNullOrEmpty(e.Parameter) && e.Parameter != "null") {
            int idCarteira = Convert.ToInt32(e.Parameter);
            Carteira carteira = new Carteira();
            carteira.LoadByPrimaryKey(idCarteira);

            if (carteira.str.Apelido != "") {
                if (carteira.IsAtivo) {
                    //O permissionamento da carteira é dado direto no cliente vinculado à carteira
                    PermissaoCliente permissaoCliente = new PermissaoCliente();
                    nome = permissaoCliente.RetornaAcessoCliente(idCarteira, HttpContext.Current.User.Identity.Name)
                                               ? carteira.str.Apelido : "no_access";                                                            
                }
                else {
                    nome = "no_active";
                }
            }
        }
        e.Result = nome;
    }

    protected void ASPxCallback2_Callback(object source, DevExpress.Web.CallbackEventArgs e) {
        string nome = "";
        if (!String.IsNullOrEmpty(e.Parameter) && e.Parameter != "null") {
            int idCotista = Convert.ToInt32(e.Parameter);
            Cotista cotista = new Cotista();
            cotista.LoadByPrimaryKey(idCotista);

            if (cotista.str.Apelido != "") {
                if (cotista.IsAtivo) {
                    PermissaoCotista permissaoCotista = new PermissaoCotista();
                    nome = permissaoCotista.RetornaAcessoCotista(idCotista, HttpContext.Current.User.Identity.Name)
                           ? cotista.str.Apelido : "no_access";                                        
                }
                else {
                    nome = "no_active";
                }
            }
        }
        e.Result = nome;
    }

    protected void EsDSCarteira_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e) {
        CarteiraCollection carteiraCollection = new CarteiraCollection();
        carteiraCollection.BuscaCarteirasComAcesso(HttpContext.Current.User.Identity.Name);

        e.Collection = carteiraCollection;
    }

    protected void EsDSCotista_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e) {
        CotistaCollection cotistaCollection = new CotistaCollection();
        cotistaCollection.BuscaCotistasComAcesso(HttpContext.Current.User.Identity.Name);

        e.Collection = cotistaCollection;
    }

    protected void btnVisualiza_Click(object sender, EventArgs e) {
        this.TrataErros();
        this.SelecionaRelatorio(VisaoRelatorio.visaoHTML);
    }

    protected void btnPDF_Click(object sender, EventArgs e) {
        this.TrataErros();
        this.SelecionaRelatorio(VisaoRelatorio.visaoPDF);
    }

    protected void btnExcel_Click(object sender, EventArgs e) {
        this.TrataErros();
        this.SelecionaRelatorio(VisaoRelatorio.visaoExcel);
    }

    /// <summary>
    /// Trata os Erros de Relatorio
    /// </summary>
    /// <exception cref="Exception"> - 
    /// De Acordo com o Tipo Relatorio
    /// Lança Exceção se Carteira ou Cotista não estiverem Preeenchidos
    /// </exception>
    private void TrataErros() {
        string tipo = this.dropTipoRelatorio.SelectedItem.Text.ToString();

        if (tipo == TipoRelatorio.Carteira && this.btnEditCodigoCarteira.Value == null) {
            throw new Exception(MSG_ERRO1);
        }
        else if (tipo == TipoRelatorio.Cotista && this.btnEditCodigoCotista.Value == null) {
            throw new Exception(MSG_ERRO2);
        }
    }

    /// <summary>
    /// Seleciona o relatorio a ser executado
    /// </summary>
    /// <param name="visao">PDF, Excel ou Report</param>
    private void SelecionaRelatorio(string visao) {
        Response.BufferOutput = true;

        // Salva os Parametros na Session
        Session["btnEditCodigoCotista"] = this.btnEditCodigoCotista.Text;
        Session["btnEditCodigoCarteira"] = this.btnEditCodigoCarteira.Text;
        Session["dropAno"] = Convert.ToInt32(this.dropAno.SelectedItem.Value);
        Session["tipoRelatorio"] = this.dropTipoRelatorio.SelectedItem.Text;
        //
        //Response.Redirect("~/Relatorios/Legais/ImpostoRenda/ReportInformeRendimentos.aspx?Visao=" + visao);

        string url = "~/Relatorios/Legais/ImpostoRenda/ReportInformeRendimentos.aspx?Visao=" + visao;
        string redirectURL = Page.ResolveClientUrl(url);

        string script = "window.location = '" + redirectURL + "';";
        ScriptManager.RegisterStartupScript(this, typeof(Page), "RedirectTo", script, true);
    }
}