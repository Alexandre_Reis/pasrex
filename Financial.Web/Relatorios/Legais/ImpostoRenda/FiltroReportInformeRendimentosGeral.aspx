﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="FiltroReportInformeRendimentosGeral.aspx.cs" Inherits="FiltroReportInformeRendimentosGeral" %>

<%@ Register Assembly="System.Web, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a" Namespace="System.Web.UI.HtmlControls" TagPrefix="cc2" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxcb" %>
<%@ Register Assembly="System.Web, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a" Namespace="System.Web.UI" TagPrefix="cc1" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxnb" %>    
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxe" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxwgv" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxpc" %>
<%@ Register Assembly="EntitySpaces.Web" Namespace="EntitySpaces.Web" TagPrefix="cc1" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >

<head id="Head1" runat="server">
<link href="~/css/forms.css" rel="stylesheet" type="text/css" />

<script type="text/javascript" language="JavaScript">
var backgroundColor;
function OnGetDataPessoa(data) {
    btnEditCodigoPessoa.SetValue(data);
    popupPessoa.HideWindow();
    ASPxCallback1.SendCallback(btnEditCodigoPessoa.GetValue());
    btnEditCodigoPessoa.Focus();
}
</script>
<script type="text/javascript" language="Javascript" src="../../../js/global.js"></script>
</head>

<body >
    <form id="form1" runat="server"> 
    
    <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="true" EnableScriptLocalization="true"/>
    
    <div class="divPanel">
    <table width="100%"><tr><td>
    <div id="container_small">
    
    <div id="header"><asp:Label ID="label1" runat="server" Text="Informe de Rendimentos Cliente" /></div>
        
    <div id="mainContentSpace">
    
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
    <ContentTemplate>
        
        <div class="reportFilter">
        
        <div style="height:20px"></div>                                   
                                        
        <table cellpadding="2" cellspacing="2" border="0">
            <tr>
            <td class="td_Label">
                <asp:Label ID="labelCliente" runat="server" CssClass="labelNormal" Text="Cliente:"></asp:Label>
            </td>
            
            <td>
                <dxe:ASPxSpinEdit ID="btnEditCodigoPessoa" runat="server" CssClass="textButtonEdit" 
                                            EnableClientSideAPI="True"
                                            ClientInstanceName="btnEditCodigoPessoa" SpinButtons-ShowIncrementButtons="false"
                                            MaxLength="9" NumberType="Integer" AllowMouseWheel="false">
                </dxe:ASPxSpinEdit>                
            </td>                                      
            </tr>
                        
            <tr>                            
                <td class="td_Label">
                    <asp:Label ID="labelAno" runat="server" CssClass="labelRequired" Text="Ano:"/>
                </td>
                                                    
                <td colspan="2">
                    <dxe:ASPxComboBox ID="dropAno" runat="server" ClientInstanceName="dropAno" OnPreRender="dropAno_PreRender" 
                        CssClass="dropDownListCurto_2" DropDownRows ="10"/>               
                </td>
            </tr>
            
        </table>                                                                                 
        
        </div>
        
        <div id="reportLinkButton" class="linkButton linkButtonNoBorder">
           <asp:LinkButton ID="btnPDF" runat="server" Font-Overline="false" CssClass="btnReport" OnClick="btnPDF_Click"><asp:Literal ID="Literal2" runat="server" Text="Gerar Arquivo"/><div></div></asp:LinkButton>           
        </div>
        
    </ContentTemplate>
    </asp:UpdatePanel>
    
    </div>
    
    </div>
    </td></tr></table>
    </div>
            
   </form>
</body>
</html>