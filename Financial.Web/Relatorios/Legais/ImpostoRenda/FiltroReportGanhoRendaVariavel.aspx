﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="FiltroReportGanhoRendaVariavel.aspx.cs" Inherits="FiltroReportGanhoRendaVariavel" %>

<%@ Register Assembly="System.Web, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"
    Namespace="System.Web.UI.HtmlControls" TagPrefix="cc2" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxcb" %>
<%@ Register Assembly="System.Web, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"
    Namespace="System.Web.UI" TagPrefix="cc1" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxnb" %>    
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxe" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxwgv" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxpc" %>
<%@ Register Assembly="EntitySpaces.Web" Namespace="EntitySpaces.Web" TagPrefix="cc1" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >

<head id="Head1" runat="server">
<link href="~/css/forms.css" rel="stylesheet" type="text/css" />

<script type="text/javascript" language="JavaScript">
var backgroundColor;
function OnGetDataCliente(values) {
    btnEditCodigo.SetValue(values);                    
    popupCliente.HideWindow();
    ASPxCallback1.SendCallback(btnEditCodigo.GetValue());        
    btnEditCodigo.Focus();    
}
</script>
<script type="text/javascript" language="Javascript" src="../../../js/global.js"></script>
</head>

<body >
    <form id="form1" runat="server"> 
    
    <dxcb:ASPxCallback ID="ASPxCallback1" runat="server" OnCallback="ASPxCallback1_Callback">
        <ClientSideEvents CallbackComplete="function(s, e) {             
            OnCallBackCompleteCliente(s, e, popupMensagemCliente, btnEditCodigo, document.getElementById('textNome'));
        }        
        "/>
    </dxcb:ASPxCallback>
    
    <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="true" EnableScriptLocalization="true"/>
    
    <div class="divPanel">
    <table width="100%"><tr><td>
    <div id="container_small">
    
    <div id="header"><asp:Label ID="label1" runat="server" Text="Apuração de IR s/ Ganhos de Capital" /></div>
        
    <div id="mainContentSpace">
    
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
    <ContentTemplate>
        
        <div class="reportFilter">
        
        <div style="height:20px"></div>                                   
                                        
        <table cellpadding="2" cellspacing="2">
            <tr>
            <td class="td_Label">
                <asp:Label ID="labelCliente" runat="server" CssClass="labelRequired" Text="Cliente:"></asp:Label>
            </td>
            
            <td>
                <dxe:ASPxSpinEdit ID="btnEditCodigo" runat="server" CssClass="textButtonEdit" 
                                            EnableClientSideAPI="True"
                                            ClientInstanceName="btnEditCodigo" SpinButtons-ShowIncrementButtons="false"
                                            MaxLength="9" NumberType="Integer" AllowMouseWheel="false">  
                <Buttons>
                    <dxe:EditButton></dxe:EditButton>                                
                </Buttons>        
                <ClientSideEvents                        
                         KeyPress="function(s, e) {document.getElementById('textNome').value = '';}" 
                         ButtonClick="function(s, e) {popupCliente.ShowAtElementByID(s.name);}" 
                         LostFocus="function(s, e) {OnLostFocus(popupMensagemCliente, ASPxCallback1, btnEditCodigo);}"
                />                            
                </dxe:ASPxSpinEdit>                
            </td>                          
            
            <td>
                <asp:TextBox ID="textNome" runat="server" CssClass="textNome" Enabled="false"></asp:TextBox>  
            </td>
            </tr>
                        
            <tr>                            
                <td class="td_Label">
                    <asp:Label ID="labelAno" runat="server" CssClass="labelNormal" Text="Ano:"/>
                </td>
                                                    
                <td colspan=2>
                    <asp:DropDownList ID="dropAno" runat="server" CssClass="dropDownListCurto_2" />                
                </td>
            </tr>
            
            <tr>                                   
                <td class="td_Label">
                    <asp:Label ID="labelTipo" runat="server" CssClass="labelNormal" Text="Tipo: " />
                </td>
                    
                <td>
                    <dxe:ASPxComboBox ID="dropTipo" runat="server" CssClass="dropDownListCurto_2" IncrementalFilteringMode="StartsWith"
                            ClientInstanceName="dropTipo" SelectedIndex="0" DropDownStyle="DropDownList">
                    <Items>
                        <dxe:ListEditItem Value="1" Text="Renda Variável" />
                        <dxe:ListEditItem Value="2" Text="Fundos Imob." />
                        <dxe:ListEditItem Value="3" Text="Completo" />
                    </Items>
                    </dxe:ASPxComboBox>
                </td>            
            </tr>
        </table>                                                                                 
        
        </div>
        
        <div id="reportLinkButton" class="linkButton linkButtonNoBorder">
           <asp:LinkButton ID="btnVisualiza" runat="server" Font-Overline="false" CssClass="btnReport" OnClick="btnVisualiza_Click"><asp:Literal ID="Literal1" runat="server" Text="Visualizar"/><div></div></asp:LinkButton>
           <asp:LinkButton ID="btnPDF" runat="server" Font-Overline="false" CssClass="btnPdf" OnClick="btnPDF_Click"><asp:Literal ID="Literal2" runat="server" Text="Gerar PDF"/><div></div></asp:LinkButton>           
        </div>
        
    </ContentTemplate>
    </asp:UpdatePanel>
    
    </div>
    
    </div>
    </td></tr></table>
    </div>
        
    <cc1:esDataSource ID="EsDSCliente" runat="server" OnesSelect="EsDSCliente_esSelect" />
        
   </form>
</body>
</html>