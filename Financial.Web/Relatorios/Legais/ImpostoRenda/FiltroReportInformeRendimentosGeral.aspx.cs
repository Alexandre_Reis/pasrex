﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using Financial.Fundo;
using Financial.Investidor;
using System.Collections.Generic;
using Financial.Web.Util;
using Financial.Investidor.Enums;
using Financial.Security;
using log4net;
using Financial.Security.Enums;
using EntitySpaces.Interfaces;
using DevExpress.Web;
using System.Text;
using Financial.Web.Common;
using Docs.Word;

public partial class FiltroReportInformeRendimentosGeral : FiltroReportBasePage {
    private static readonly ILog log = LogManager.GetLogger(typeof(FiltroReportInformeRendimentosGeral));

    new protected void Page_Load(object sender, EventArgs e) {

        //string file1 = @"S:\Temp\Temp1\1.doc";
        //string file2 = @"S:\Temp\Temp1\3.doc";
        ////
        //Document Doc = Document.ReadDOC(file1);
        //NodeList Nodes = Doc.Sections.GetNodes();
        ////
        //string nomeEmpresarial = "Nome Empresa_1";
        //string cnpj = "15151515/15";
        //string cpf = "225.558.651-89";
        //string nome = "Alexandre Matheus";                       
        //decimal valor1 = 456.76M;

        //Search.Replace(Nodes, "{1}", nomeEmpresarial);
        //Search.Replace(Nodes, "{2}", cnpj);
        //Search.Replace(Nodes, "{3}", cpf);
        //Search.Replace(Nodes, "{4}", nome);
        ////
        //Search.Replace(Nodes, "{5}", valor1.ToString("n2"));
        //Search.Replace(Nodes, "{6}", valor1.ToString("n2"));
        //Search.Replace(Nodes, "{7}", valor1.ToString("n2"));
        //Search.Replace(Nodes, "{8}", valor1.ToString("n2"));
        //Search.Replace(Nodes, "{9}", valor1.ToString("n2"));
        //Search.Replace(Nodes, "{10}", valor1.ToString("n2"));
        ////
        //Doc.WriteDOC(file2);
        //
        btnEditCodigoPessoa.Focus();

        base.Page_Load(sender, e);
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void dropAno_PreRender(object sender, EventArgs e) {
        // Ano Atual
        int anoAtual = DateTime.Today.Year;
        int primeiroMes = 1980; // Valor Fixo
        for (int i = anoAtual; i >= primeiroMes; i--) {
            (sender as ASPxComboBox).Items.Add(i.ToString(), i.ToString());
        }
        (sender as ASPxComboBox).Items[0].Selected = true;
    }

    protected void btnVisualiza_Click(object sender, EventArgs e) {
        //this.TrataErros();
        //this.SelecionaRelatorio(VisaoRelatorio.visaoHTML);
    }

    protected void btnPDF_Click(object sender, EventArgs e) {
        this.TrataErros();
        this.SelecionaRelatorio(VisaoRelatorio.visaoPDF);
    }
        
    /// <summary>
    /// Trata os Erros de Relatorio
    /// </summary>
    private void TrataErros() {

        #region Campos obrigatórios
        List<Control> controles = new List<Control>(new Control[] { 
    	    this.dropAno });

        if (base.TestaObrigatorio(controles) != "") {
            throw new Exception("Campos com * são obrigatórios!");
        }
        #endregion
    }

    /// <summary>
    /// Seleciona o relatorio a ser executado
    /// </summary>
    /// <param name="visao">PDF, Excel ou Report</param>
    private void SelecionaRelatorio(string visao) {
        Response.BufferOutput = true;
        //        
        // Salva os parametros na session
        Session["dropAno"] = this.dropAno.SelectedItem.Value;
        Session["btnEditCodigo"] = this.btnEditCodigoPessoa.Text;

        //Response.Redirect("~/Relatorios/Legais/ImpostoRenda/ReportInformeRendimentosGeral.aspx");

        string url = "~/Relatorios/Legais/ImpostoRenda/ReportInformeRendimentosGeral.aspx";
        string redirectURL = Page.ResolveClientUrl(url);

        string script = "window.location = '" + redirectURL + "';";
        ScriptManager.RegisterStartupScript(this, typeof(Page), "RedirectTo", script, true);
    }
}