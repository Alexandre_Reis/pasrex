using System;
using Financial.Relatorio;
using System.IO;
using System.Globalization;
using System.Web;
using Financial.Investidor;
using Financial.Investidor.Enums;
using Financial.Web.Common;
using System.Collections.Generic;
using Dart.PowerTCP.Zip;

public partial class _ReportDesempenhoFundo : BasePage {
    
    new protected void Page_Load(object sender, EventArgs e) {

        DateTime data = Convert.ToDateTime(Session["textData"]);

        #region DesempenhoFundo

        Dictionary<string, MemoryStream> msDesempenhoFundo = (Dictionary<string, MemoryStream>)Session["streamDesempenhoFundo"];
        Archive arquivoDesempenhoFundo = new Archive();

        // Saida
        MemoryStream msZipDesempenhoFundo = new MemoryStream();
        string arquivoZipDesempenhoFundo = "DesempenhoFundo.zip";

        try {
            int j = 0;
            foreach (KeyValuePair<string, MemoryStream> pair in msDesempenhoFundo) {
                arquivoDesempenhoFundo.Add(pair.Value); // Memory Stream
                arquivoDesempenhoFundo[j].Name = pair.Key; // Nome do Arquivo
                //
                j++;
            }
            //
            arquivoDesempenhoFundo.Zip(msZipDesempenhoFundo);
        }
        catch (Exception e1) {
            throw new Exception(e1.Message + "Erro ao Zipar o Arquivo");
        }

        Response.ClearContent();
        Response.ClearHeaders();
        Response.Buffer = true;
        Response.Cache.SetCacheability(HttpCacheability.Private);
        Response.ContentType = "application/zip";
        Response.AddHeader("Content-Length", msZipDesempenhoFundo.Length.ToString(CultureInfo.CurrentCulture));
        Response.AppendHeader("Content-Disposition", String.Format("attachment;filename=" + arquivoZipDesempenhoFundo));

        Response.BinaryWrite(msZipDesempenhoFundo.ToArray());
        msZipDesempenhoFundo.Close();

        HttpContext.Current.ApplicationInstance.CompleteRequest();
        Response.End();

        #endregion
    }
}