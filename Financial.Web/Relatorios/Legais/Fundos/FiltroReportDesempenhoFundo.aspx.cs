﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using Financial.Fundo;
using Financial.Investidor;
using System.Collections.Generic;
using Financial.Web.Util;
using Financial.Investidor.Enums;
using Financial.Security;
using log4net;
using Financial.Security.Enums;
using EntitySpaces.Interfaces;
using DevExpress.Web;
using System.Text;
using Financial.Util;
using Financial.Common.Enums;
using Financial.Web.Common;
using System.IO;
using Financial.Export;

public partial class FiltroReportDesempenhoFundo : FiltroReportBasePage {
    private static readonly ILog log = LogManager.GetLogger(typeof(FiltroReportDesempenhoFundo));
    
    new protected void Page_Load(object sender, EventArgs e) {        
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void dropAno_Init(object sender, EventArgs e) {        
        //Ano Atual
        int anoAtual = DateTime.Today.Year;
        int primeiroMes = 1980; // Valor Fixo
        for (int i = anoAtual; i >= primeiroMes; i--) {
            (sender as ASPxComboBox).Items.Add(i.ToString(), i.ToString());
        }
        (sender as ASPxComboBox).Items[0].Selected = true;
    }

    #region DataSources
    
    protected void EsDSCliente_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        Usuario usuario = new Usuario();
        usuario.BuscaUsuario(Context.User.Identity.Name);
        int idUsuario = usuario.IdUsuario.Value;

        ClienteQuery clienteQuery = new ClienteQuery("L");
        PermissaoClienteQuery permissaoClienteQuery = new PermissaoClienteQuery("P");
        GrupoProcessamentoQuery grupoProcessamentoQuery = new GrupoProcessamentoQuery("G");

        clienteQuery.Select(clienteQuery, grupoProcessamentoQuery.Descricao);
        clienteQuery.InnerJoin(permissaoClienteQuery).On(permissaoClienteQuery.IdCliente == clienteQuery.IdCliente);
        clienteQuery.InnerJoin(grupoProcessamentoQuery).On(clienteQuery.IdGrupoProcessamento == grupoProcessamentoQuery.IdGrupoProcessamento);
        //
        clienteQuery.Where(permissaoClienteQuery.IdUsuario == idUsuario);

        clienteQuery.Where(clienteQuery.TipoControle.NotIn((int)TipoControleCliente.ApenasCotacao, (int)TipoControleCliente.IRRendaVariavel));
        //clienteQuery.Where(clienteQuery.IdCliente == 13712);

        clienteQuery.OrderBy(clienteQuery.Apelido.Ascending, grupoProcessamentoQuery.Descricao.Descending);

        ClienteCollection coll = new ClienteCollection();
        coll.Load(clienteQuery);

        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }

    protected void EsDSTipoCliente_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        TipoClienteCollection coll = new TipoClienteCollection();
        coll.LoadAll();

        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }
    #endregion

    #region Grid

    /// <summary>
    /// Função para selecionar todos os checkboxs de um Grid 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    new protected void CheckBoxSelectAll(object sender, EventArgs e) {
        ASPxCheckBox chk = sender as ASPxCheckBox;
        ASPxGridView grid = (chk.NamingContainer as GridViewHeaderTemplateContainer).Grid;
        chk.Checked = (grid.Selection.Count == grid.VisibleRowCount);
    }

    protected void gridConsulta_HtmlRowCreated(object sender, ASPxGridViewTableRowEventArgs e) {
        this.SetaCoresGrid(e);
    }

    protected void gridConsulta_CustomUnboundColumnData(object sender, DevExpress.Web.ASPxGridViewColumnDataEventArgs e)
    {
        if (e.Column.FieldName == "StatusDescricao")
        {
            int status = Convert.ToInt32(e.GetListSourceFieldValue(ClienteMetadata.ColumnNames.Status));

            string statusDescricao = TraducaoEnumsInvestidor.EnumStatusCliente.TraduzEnum(status);

            e.Value = statusDescricao;
        }
    }

    protected void gridConsulta_CustomCallback(object sender, DevExpress.Web.ASPxGridViewCustomCallbackEventArgs e)
    {
        gridConsulta.DataBind();
        gridConsulta.Selection.UnselectAll();
        gridConsulta.CancelEdit();
    }
    #endregion

    /// <summary>
    /// Start do Processamento
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void callbackExporta_Callback(object source, DevExpress.Web.CallbackEventArgs e) {
        e.Result = "";

        try {
            this.TrataErros();
            this.Exportacao();
        }
        catch (Exception erro) {
            e.Result = erro.Message;
            return;
        }

        string newLocation = HttpContext.Current.Request.Url.AbsolutePath.Replace("FiltroReportDesempenhoFundo.aspx", "ReportDesempenhoFundo.aspx");
        e.Result = "redirect:" + newLocation;
    }

    /// <summary>
    /// Trata os Erros de Exportação
    /// </summary>
    private void TrataErros() {
        List<object> keyValuesId = gridConsulta.GetSelectedFieldValues(ClienteMetadata.ColumnNames.IdCliente);

        if (keyValuesId.Count == 0) {
            throw new Exception("Algum cliente deve ser selecionado!");
        }
        
        int anoInformado = Convert.ToInt32(this.dropAno.SelectedItem.Text);
        int periodo = Convert.ToInt32(this.periodo.Value);

        DateTime dataDez = new DateTime(anoInformado, 12,1);
        DateTime dataJun = new DateTime(anoInformado, 6, 1);
        //
        DateTime dataUltima = periodo == 1 ? Calendario.RetornaUltimoDiaUtilMes(dataDez, 0)
                                           : Calendario.RetornaUltimoDiaUtilMes(dataJun, 0);
        
        List<string> clientesNaoProcessados = new List<string>();
        // Confere se cliente está processado até o ultimo dia util do mes junho/dez
        for (int i = 0; i < keyValuesId.Count; i++) {
            bool processado = this.isClienteProcessadoAteData(Convert.ToInt32(keyValuesId[i]), dataUltima);

            if (!processado) {
                clientesNaoProcessados.Add(Convert.ToString(keyValuesId[i]));
            }
        }
        if (clientesNaoProcessados.Count > 0) {
            string clientes = string.Join(",", clientesNaoProcessados.ToArray());
            //
            string msg = clientesNaoProcessados.Count >= 2
                         ? "Clientes " + clientes + " não processados até " + dataUltima.ToString("d")
                         : "Cliente " + clientes + " não processado até " + dataUltima.ToString("d");
            //
            throw new Exception(msg);
        }
    }

    /// <summary>
    /// Realiza Exportação
    /// </summary>
    private void Exportacao() {
        Response.BufferOutput = true;

        // Dicionario Nome do Arquivo - Memory Stream do arquivo
        Dictionary<string, MemoryStream> retDesempenhoFundo = new Dictionary<string, MemoryStream>();
        //                
        this.ProcessaDesempenhoFundo(out retDesempenhoFundo);
        //
        // Salva Dicionary na Session           
        Session["streamDesempenhoFundo"] = retDesempenhoFundo;
        //

        int anoInformado = Convert.ToInt32(this.dropAno.SelectedItem.Text);
        int periodo = Convert.ToInt32(this.periodo.Value);

        DateTime dataDez = new DateTime(anoInformado, 12, 1);
        DateTime dataJun = new DateTime(anoInformado, 6, 1);
        //
        DateTime data = periodo == 1 ? Calendario.RetornaUltimoDiaUtilMes(dataDez, 0)
                                           : Calendario.RetornaUltimoDiaUtilMes(dataJun, 0);

        Session["textData"] = data; // Repassa 31/12/Ano ou 30/06/Ano
    }
  
    #region Processamento
    /// <summary>
    /// 
    /// </summary>
    /// <param name="msLamina">Dicionario com o nome do Arquivo - MemoryStream do Arquivo</param>
    private void ProcessaDesempenhoFundo(out Dictionary<string, MemoryStream> msDesempenhoFundo) {
        
        #region DesempenhoFundo
        int anoInformado = Convert.ToInt32(this.dropAno.SelectedItem.Text);
        int periodo = Convert.ToInt32(this.periodo.Value);

        DateTime dataDez = new DateTime(anoInformado, 12, 1);
        DateTime dataJun = new DateTime(anoInformado, 6, 1);
        //
        DateTime data = periodo == 1 ? Calendario.RetornaUltimoDiaUtilMes(dataDez, 0)
                                           : Calendario.RetornaUltimoDiaUtilMes(dataJun, 0);
        
        List<object> keyValuesId = gridConsulta.GetSelectedFieldValues(ClienteMetadata.ColumnNames.IdCliente);
        List<int> idClientes = keyValuesId.ConvertAll<int>(delegate(object v) { return Convert.ToInt32(v); });

        DesempenhoFundo desempenhoFundo = new DesempenhoFundo();
        //
        try {
            desempenhoFundo.ExportaDesempenhoFundo(data, idClientes, out msDesempenhoFundo);
        }
        catch (Exception e1)
        {
            throw new Exception(e1.Message);
        }

        #endregion
    }

    /// <summary>
    /// Retorna se cliente está processado até a data
    /// </summary>
    /// <param name="data"></param>
    /// <returns></returns>
    private bool isClienteProcessadoAteData(int idCliente, DateTime data) {
        // Carrega o Cliente
        Cliente c = new Cliente();
        List<esQueryItem> campos = new List<esQueryItem>();
        campos.Add(c.Query.DataDia);
        //
        c.LoadByPrimaryKey(campos, idCliente);

        return c.DataDia >= data;
    }
    #endregion
}