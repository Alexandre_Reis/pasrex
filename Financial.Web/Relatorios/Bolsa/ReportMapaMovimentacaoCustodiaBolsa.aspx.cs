using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Financial.Common;
using EntitySpaces.Interfaces;
using System.Collections.Generic;
using Financial.Relatorio;
using System.IO;
using System.Globalization;

public partial class _ReportMapaMovimentacaoCustodiaBolsa : Financial.Web.Common.BasePage {
    /*  //
        DateTime dataInicio = new DateTime(2000, 1, 1);
        DateTime dataFim = new DateTime(2010, 1, 25);
        string cdAtivoBolsa = "P";
        string tipoMercado = "VIS";
        int idCliente = 32;

        // 5 parametros
        ReportViewer1.Report = new ReportMapaMovimentacaoCustodiaBolsa(dataInicio, dataFim, tipoMercado, idCliente, cdAtivoBolsa);
        
        // 3 parametros
        //ReportViewer1.Report = new ReportMapaMovimentacaoCustodiaBolsa(dataInicio, dataFim, idCliente);
        //ReportViewer1.Report = new ReportMapaMovimentacaoCustodiaBolsa(dataInicio, dataFim, cdAtivoBolsa);
        //ReportViewer1.Report = new ReportMapaMovimentacaoCustodiaBolsa(tipoMercado, dataInicio, dataFim);

        // 4 parametros
        //ReportViewer1.Report = new ReportMapaMovimentacaoCustodiaBolsa(dataInicio, dataFim, cdAtivoBolsa, idCliente);
        //ReportViewer1.Report = new ReportMapaMovimentacaoCustodiaBolsa(dataInicio, dataFim, cdAtivoBolsa, tipoMercado);
        //ReportViewer1.Report = new ReportMapaMovimentacaoCustodiaBolsa(tipoMercado, idCliente, dataInicio, dataFim);
     */
    new protected void Page_Load(object sender, EventArgs e) {            
        string visao = Request.QueryString["Visao"].ToString();
        //
        //Response.Write(Request.Form["textDataInicio"].ToString());
        //Response.Write("<br>" +Request.Form["textDataFim"].ToString());
        //Response.Write("<br>" + Request.Form["btnEditCodigo"].ToString());
        //Response.Write("<br>" + Request.Form["dropTipoMercado"].ToString());
        //Response.Write("<br>" + Request.Form["textCdAtivo"].ToString());
       
        DateTime dataInicio = new DateTime();
        DateTime dataFim = new DateTime();
        string cdAtivoBolsa = null;
        int? idCliente = null;
        string tipoMercado = null;

        // Obrigatorio
        dataInicio = Convert.ToDateTime(Session["textDataInicio"]);
        dataFim = Convert.ToDateTime(Session["textDataFim"]);
        //Opcionais
        cdAtivoBolsa = (string)Session["textCdAtivo"];
        tipoMercado = (string)Session["dropTipoMercado"];
        if (!String.IsNullOrEmpty((string)Session["btnEditCodigo"])) {
            idCliente = Convert.ToInt32(Session["btnEditCodigo"]);
        }
        
        if (visao == "Report") {
            ReportViewer1.Report = new ReportMapaMovimentacaoCustodiaBolsa(dataInicio, dataFim, tipoMercado, idCliente, cdAtivoBolsa);
        }
        else if (visao == "PDF") {
            #region PDF
            ReportMapaMovimentacaoCustodiaBolsa report = new ReportMapaMovimentacaoCustodiaBolsa(dataInicio, dataFim, tipoMercado, idCliente, cdAtivoBolsa);

            MemoryStream ms = new MemoryStream();
            report.ExportToPdf(ms);
            ms.Seek(0, SeekOrigin.Begin);
            Response.ClearContent();
            Response.ClearHeaders();
            Response.Buffer = true;
            Response.Cache.SetCacheability(HttpCacheability.Private);
            Response.ContentType = "application/pdf";
            Response.AddHeader("Content-Length", ms.Length.ToString(CultureInfo.CurrentCulture));            
            Response.AddHeader("Content-Disposition", String.Format("attachment;filename=ReportMapaMovimentacaoCustodiaBolsa.pdf"));

            Response.BinaryWrite(ms.ToArray());
            ms.Close();
            HttpContext.Current.ApplicationInstance.CompleteRequest();
            Response.End();
            #endregion
        }
        else if (visao == "Excel") {
            #region Excel
            ReportMapaMovimentacaoCustodiaBolsa report = new ReportMapaMovimentacaoCustodiaBolsa(dataInicio, dataFim, tipoMercado, idCliente, cdAtivoBolsa);

            MemoryStream ms = new MemoryStream();
            report.ExportToXls(ms);
            ms.Seek(0, SeekOrigin.Begin);
            Response.ClearContent();
            Response.ClearHeaders();
            Response.Buffer = true;
            Response.Cache.SetCacheability(HttpCacheability.Private);
            Response.ContentType = "application/vnd.ms-excel";
            Response.AddHeader("Content-Length", ms.Length.ToString(CultureInfo.CurrentCulture));
            Response.AddHeader("Content-Disposition", String.Format("attachment;filename=ReportMapaMovimentacaoCustodiaBolsa.xls"));

            Response.BinaryWrite(ms.ToArray());
            ms.Close();
            HttpContext.Current.ApplicationInstance.CompleteRequest();
            Response.End();

            #endregion
        }            
    }
}

