using System;
using Financial.Relatorio;
using System.IO;
using System.Globalization;
using System.Web;

public partial class _ReportPosicaoCustodia : Financial.Web.Common.BasePage {
    /*  Como usar o relatorio
        DateTime dataPosicao = new DateTime(4000, 12, 31);
        int idCliente = 4337;

        // 2 parametros
        //ReportViewer1.Report = new ReportPosicaoCustodia(dataPosicao, idCliente);        
     */
    new protected void Page_Load(object sender, EventArgs e) {
        string visao = Request.QueryString["Visao"].ToString();
        //
        //Response.Write(Session["textData"].ToString());
        //Response.Write("<br>" + Session["btnEditCodigo"].ToString());        
               
        int? idCliente = null;
        string cdAtivoBolsa = "";

        // Obrigatorio
        DateTime dataPosicao = Convert.ToDateTime(Session["textData"]);
        //Opcionais        
        if (!String.IsNullOrEmpty((string)Session["btnEditCodigo"])) {
            idCliente = Convert.ToInt32(Session["btnEditCodigo"]);
        }
        if (!String.IsNullOrEmpty((string)Session["textCdAtivo"]))
        {
            cdAtivoBolsa = Convert.ToString(Session["textCdAtivo"]);
        }
        
        if (visao == "Report") {
            ReportViewer1.Report = new ReportPosicaoCustodia(idCliente, dataPosicao, cdAtivoBolsa);
        }
        else if (visao == "PDF") {
            #region PDF
            ReportPosicaoCustodia report = new ReportPosicaoCustodia(idCliente, dataPosicao, cdAtivoBolsa);

            MemoryStream ms = new MemoryStream();
            report.ExportToPdf(ms);
            ms.Seek(0, SeekOrigin.Begin);
            Response.ClearContent();
            Response.ClearHeaders();
            Response.Buffer = true;
            Response.Cache.SetCacheability(HttpCacheability.Private);
            Response.ContentType = "application/pdf";
            Response.AddHeader("Content-Length", ms.Length.ToString(CultureInfo.CurrentCulture));            
            Response.AddHeader("Content-Disposition", String.Format("attachment;filename=ReportPosicaoCustodia.pdf"));

            Response.BinaryWrite(ms.ToArray());
            ms.Close();
            HttpContext.Current.ApplicationInstance.CompleteRequest();
            Response.End();
            #endregion
        }
        else if (visao == "Excel") {
            #region Excel
            ReportPosicaoCustodia report = new ReportPosicaoCustodia(idCliente, dataPosicao, cdAtivoBolsa);

            MemoryStream ms = new MemoryStream();
            report.ExportToXls(ms);
            ms.Seek(0, SeekOrigin.Begin);
            Response.ClearContent();
            Response.ClearHeaders();
            Response.Buffer = true;
            Response.Cache.SetCacheability(HttpCacheability.Private);
            Response.ContentType = "application/vnd.ms-excel";
            Response.AddHeader("Content-Length", ms.Length.ToString(CultureInfo.CurrentCulture));
            Response.AddHeader("Content-Disposition", String.Format("attachment;filename=ReportPosicaoCustodia.xls"));

            Response.BinaryWrite(ms.ToArray());
            ms.Close();
            HttpContext.Current.ApplicationInstance.CompleteRequest();
            Response.End();

            #endregion
        }
    }
}
