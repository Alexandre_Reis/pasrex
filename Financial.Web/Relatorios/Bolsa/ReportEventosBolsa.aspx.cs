using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Financial.Common;
using EntitySpaces.Interfaces;
using System.Collections.Generic;
using Financial.Relatorio;
using System.IO;
using System.Globalization;

public partial class _ReportEventosBolsa : Financial.Web.Common.BasePage {
    /* Como usar:
        DateTime dataInicio = new DateTime(2000, 1, 1);
        DateTime dataFim = new DateTime(2010, 1, 25);
        //string cdAtivoBolsa = "ABABA";
        string cdAtivoBolsa = "PETR4";
        ReportViewer1.Report = new ReportEventosBovespa(dataInicio, dataFim, cdAtivoBolsa, true);
     */
    new protected void Page_Load(object sender, EventArgs e) {
        string visao = Request.QueryString["Visao"].ToString();
        //
        //Response.Write(Request.Form["textDataInicio"].ToString());
        //Response.Write("<br>" +Request.Form["textDataFim"].ToString());
        //Response.Write("<br>" + Request.Form["textCdAtivo"].ToString());
        // "on" Se ligado, vazio se desligado
        //Response.Write("<br>" + Request.Form["checkApenasAcoes"]);

        DateTime dataInicio = new DateTime();
        DateTime dataFim = new DateTime();
        string cdAtivoBolsa = null;        
        bool acoes = false;

        // Obrigatorio
        dataInicio = Convert.ToDateTime(Session["textDataInicio"]);
        dataFim = Convert.ToDateTime(Session["textDataFim"]);
        //Opcionais
        cdAtivoBolsa = (string)Session["textCdAtivo"];            
        acoes = String.IsNullOrEmpty((string)Session["checkApenasAcoes"]) ? false : true;            
        
        if (visao == "Report") {
            ReportViewer1.Report = new ReportEventosBovespa(dataInicio, dataFim, cdAtivoBolsa, acoes);
        }
        else if (visao == "PDF") {
            #region PDF
            ReportEventosBovespa report = new ReportEventosBovespa(dataInicio, dataFim, cdAtivoBolsa, acoes);

            MemoryStream ms = new MemoryStream();
            report.ExportToPdf(ms);
            ms.Seek(0, SeekOrigin.Begin);
            Response.ClearContent();
            Response.ClearHeaders();
            Response.Buffer = true;
            Response.Cache.SetCacheability(HttpCacheability.Private);
            Response.ContentType = "application/pdf";
            Response.AddHeader("Content-Length", ms.Length.ToString(CultureInfo.CurrentCulture));            
            Response.AddHeader("Content-Disposition", String.Format("attachment;filename=ReportEventosBovespa.pdf"));

            Response.BinaryWrite(ms.ToArray());
            ms.Close();
            HttpContext.Current.ApplicationInstance.CompleteRequest();
            Response.End();
            #endregion
        }
        else if (visao == "Excel") {
            #region Excel
            ReportEventosBovespa report = new ReportEventosBovespa(dataInicio, dataFim, cdAtivoBolsa, acoes);

            MemoryStream ms = new MemoryStream();
            report.ExportToXls(ms);
            ms.Seek(0, SeekOrigin.Begin);
            Response.ClearContent();
            Response.ClearHeaders();
            Response.Buffer = true;
            Response.Cache.SetCacheability(HttpCacheability.Private);
            Response.ContentType = "application/vnd.ms-excel";
            Response.AddHeader("Content-Length", ms.Length.ToString(CultureInfo.CurrentCulture));
            Response.AddHeader("Content-Disposition", String.Format("attachment;filename=ReportEventosBovespa.xls"));

            Response.BinaryWrite(ms.ToArray());
            ms.Close();
            HttpContext.Current.ApplicationInstance.CompleteRequest();
            Response.End();

            #endregion
        }            
    }
}

