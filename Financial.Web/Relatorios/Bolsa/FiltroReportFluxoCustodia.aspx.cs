﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Text;
using System.Collections.Generic;

using Financial.Investidor;
using Financial.Web.Util;
using Financial.Web.Common;

using Financial.Security;

using log4net;

using Financial.Investidor.Enums;
using Financial.Security.Enums;

using EntitySpaces.Interfaces;
using DevExpress.Web;

public partial class FiltroReportFluxoCustodia : FiltroReportBasePage
{
    private static readonly ILog log = LogManager.GetLogger(typeof(FiltroReportFluxoCustodia));

    const string MSG_ERRO = "Data Passada Anterior à Data de Implantação da Carteira";

    new protected void Page_Load(object sender, EventArgs e) {
        btnEditCodigo.Focus();

        this.HasPopupCliente = true;
        base.Page_Load(sender, e);

        this.TrataTravamentoCampos();
    }

    private void TrataTravamentoCampos() {
        Usuario usuario = new Usuario();
        usuario.BuscaUsuario(HttpContext.Current.User.Identity.Name);
        int tipoTrava = usuario.TipoTrava.Value;

        if (tipoTrava == (int)TipoTravaUsuario.TravaCliente) {
            btnEditCodigo.Enabled = false;

            PermissaoCliente permissaoCliente = new PermissaoCliente();
            int? idCliente = permissaoCliente.RetornaClienteAssociado(HttpContext.Current.User.Identity.Name);

            if (idCliente.HasValue) {
                Cliente cliente = new Cliente();

                List<esQueryItem> campos = new List<esQueryItem>();
                campos.Add(cliente.Query.Apelido);
                cliente.LoadByPrimaryKey(campos, idCliente.Value);
                string apelido = cliente.Apelido;

                btnEditCodigo.Text = Convert.ToString(idCliente.Value);
                textNome.Text = apelido;
            }
        }
    }

    protected void ASPxCallback1_Callback(object source, DevExpress.Web.CallbackEventArgs e) {
        string nome = "";
        if (!String.IsNullOrEmpty(e.Parameter) && e.Parameter != "null") {
            int idCliente = Convert.ToInt32(e.Parameter);
            Cliente cliente = new Cliente();
            cliente.LoadByPrimaryKey(idCliente);

            if (cliente.str.Apelido != "") {
                if (cliente.IsAtivo) {
                    PermissaoCliente permissaoCliente = new PermissaoCliente();
                    nome = permissaoCliente.RetornaAcessoClienteComControle(idCliente, HttpContext.Current.User.Identity.Name)
                        ? cliente.str.Apelido : "no_access";                    
                }
                else {
                    nome = "no_active";
                }
            }
        }
        e.Result = nome;
    }

    protected void EsDSCliente_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e) {
        ClienteCollection clienteCollection = new ClienteCollection();
        clienteCollection.BuscaClientesComAcesso(HttpContext.Current.User.Identity.Name);

        e.Collection = clienteCollection;
    }

    protected void btnVisualiza_Click(object sender, EventArgs e) {
        this.TrataErros();
        this.SelecionaRelatorio(VisaoRelatorio.visaoHTML);
    }

    protected void btnPDF_Click(object sender, EventArgs e) {
        this.TrataErros();
        this.SelecionaRelatorio(VisaoRelatorio.visaoPDF);
    }

    protected void btnExcel_Click(object sender, EventArgs e) {
        this.TrataErros();
        this.SelecionaRelatorio(VisaoRelatorio.visaoExcel);
    }

    /// <summary>
    /// Trata os Erros de Relatorio
    /// </summary>
    /// <exception cref="Exception"> Data passada Anterior à data de implantação da Carteira
    /// </exception>
    private void TrataErros() {
        #region Campos obrigatórios
        List<Control> controles = new List<Control>(new Control[] { 
    	    this.btnEditCodigo, this.textData });

        if (base.TestaObrigatorio(controles) != "") {
            throw new Exception("Campos com * são obrigatórios!");
        }
        #endregion
                       
        DateTime data = Convert.ToDateTime(this.textData.Text);
        
        Cliente cliente = new Cliente();                
        List<esQueryItem> campos = new List<esQueryItem>();
        campos.Add(cliente.Query.DataImplantacao);
        cliente.LoadByPrimaryKey(campos, Convert.ToInt32(this.btnEditCodigo.Text));

        DateTime dataImplantacao = cliente.DataImplantacao.Value;

        if (data < dataImplantacao) {
            throw new Exception(MSG_ERRO + ": " + dataImplantacao.ToString("d"));
        }
    }

    /// <summary>
    /// Seleciona o relatorio a ser executado
    /// </summary>
    /// <param name="visao">PDF, Excel ou Report</param>
    private void SelecionaRelatorio(string visao) {
        Response.BufferOutput = true;
        //
        // Salva os parametros na session
        Session["textData"] = this.textData.Text;
        Session["btnEditCodigo"] = this.btnEditCodigo.Text;

        // Response.Redirect("~/Relatorios/Bolsa/ReportFluxoCustodia.aspx?Visao=" + visao);

        string url = "~/Relatorios/Bolsa/ReportFluxoCustodia.aspx?Visao=" + visao;
        string redirectURL = Page.ResolveClientUrl(url);

        string script = "window.location = '" + redirectURL + "';";
        ScriptManager.RegisterStartupScript(this, typeof(Page), "RedirectTo", script, true);
    }
}