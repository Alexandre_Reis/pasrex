using System;
using Financial.Relatorio;
using System.IO;
using System.Web;
using System.Globalization;
using Financial.Web.Common;

using System.Collections.Generic;
using Financial.Investidor;
using Financial.Util;
using Financial.Common.Enums;

public partial class _ReportPosicaoBolsa : BasePage {        
    new protected void Page_Load(object sender, EventArgs e) {
        
        string visao = Request.QueryString["Visao"].ToString();
        //        
        // Obrigatorios
        DateTime dataPosicao = Convert.ToDateTime(Session["textDataPosicao"]);

        if (visao == "PDF") {
            #region PDF
            ReportPosicaoBolsaFator report = new ReportPosicaoBolsaFator(dataPosicao);

            MemoryStream ms = new MemoryStream();
            report.ExportToPdf(ms);
            ms.Seek(0, SeekOrigin.Begin);
            Response.ClearContent();
            Response.ClearHeaders();
            Response.Buffer = true;
            Response.Cache.SetCacheability(HttpCacheability.Private);
            Response.ContentType = "application/pdf";
            Response.AddHeader("Content-Length", ms.Length.ToString(CultureInfo.CurrentCulture));
            Response.AddHeader("Content-Disposition", String.Format("attachment;filename=ReportPosicaoBolsa.pdf"));

            Response.BinaryWrite(ms.ToArray());
            ms.Close();
            HttpContext.Current.ApplicationInstance.CompleteRequest();
            Response.End();
            #endregion
        }

        if (visao == "Excel")
        {
            #region Excel
            ReportPosicaoBolsaFator report = new ReportPosicaoBolsaFator(dataPosicao);

            MemoryStream ms = new MemoryStream();
            report.ExportToXls(ms);
            ms.Seek(0, SeekOrigin.Begin);
            Response.ClearContent();
            Response.ClearHeaders();
            Response.Buffer = true;
            Response.Cache.SetCacheability(HttpCacheability.Private);
            Response.ContentType = "application/xls";
            Response.AddHeader("Content-Length", ms.Length.ToString(CultureInfo.CurrentCulture));
            Response.AddHeader("Content-Disposition", String.Format("attachment;filename=ReportPosicaoBolsa.xls"));

            Response.BinaryWrite(ms.ToArray());
            ms.Close();
            HttpContext.Current.ApplicationInstance.CompleteRequest();
            Response.End();
            #endregion
        }
    }
}