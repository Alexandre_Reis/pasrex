﻿using System;
using Financial.Relatorio;
using System.IO;
using System.Web;
using System.Globalization;

public partial class _ReportPosicaoEmprestimoAcoes : Financial.Web.Common.BasePage {

    /* Como usar o relatório
        DateTime dataReferencia = new DateTime(2000, 1, 1);
        string cdAtivoBolsa = "PETR4";
        int idCliente = 32;
        
        // 3 parametros
        ReportViewer1.Report = new ReportPosicaoEmprestimoAcoes(dataReferencia, idCliente, cdAtivoBolsa);

        // 2 parametros
        //ReportViewer1.Report = new ReportPosicaoEmprestimoAcoes(dataReferencia, idCliente);
        //ReportViewer1.Report = new ReportPosicaoEmprestimoAcoes(dataReferencia, cdAtivoBolsa);

        // 1 parametro
        //ReportViewer1.Report = new ReportPosicaoEmprestimoAcoes(dataReferencia);
     */
    new protected void Page_Load(object sender, EventArgs e) {
        string visao = Request.QueryString["Visao"].ToString();
        //
        //Response.Write(Request.Form["textData"].ToString());
        //Response.Write("<br>" + Request.Form["btnEditCodigo"].ToString());        
        //Response.Write("<br>" + Request.Form["textCdAtivo"].ToString());

        DateTime dataReferencia = new DateTime();
        string cdAtivoBolsa = null;
        int? idCliente = null;

        // Obrigatorio
        dataReferencia = Convert.ToDateTime(Session["textData"]);
        //Opcionais
        cdAtivoBolsa = (string)Session["textCdAtivo"];
        if (!String.IsNullOrEmpty((string)Session["btnEditCodigo"])) {
            idCliente = Convert.ToInt32(Session["btnEditCodigo"]);
        }
        
        if (visao == "Report") {
            ReportViewer1.Report = new ReportPosicaoEmprestimoAcoes(dataReferencia, idCliente, cdAtivoBolsa);
        }
        else if (visao == "PDF") {
            #region PDF
            ReportPosicaoEmprestimoAcoes report = new ReportPosicaoEmprestimoAcoes(dataReferencia, idCliente, cdAtivoBolsa);

            MemoryStream ms = new MemoryStream();
            report.ExportToPdf(ms);
            ms.Seek(0, SeekOrigin.Begin);
            Response.ClearContent();
            Response.ClearHeaders();
            Response.Buffer = true;
            Response.Cache.SetCacheability(HttpCacheability.Private);
            Response.ContentType = "application/pdf";
            Response.AddHeader("Content-Length", ms.Length.ToString(CultureInfo.CurrentCulture));
            Response.AddHeader("Content-Disposition", String.Format("attachment;filename=ReportPosicaoEmprestimoAcoes.pdf"));

            Response.BinaryWrite(ms.ToArray());
            ms.Close();
            HttpContext.Current.ApplicationInstance.CompleteRequest();
            Response.End();
            #endregion
        }
        else if (visao == "Excel") {
            #region Excel
            ReportPosicaoEmprestimoAcoes report = new ReportPosicaoEmprestimoAcoes(dataReferencia, idCliente, cdAtivoBolsa);

            MemoryStream ms = new MemoryStream();
            report.ExportToXls(ms);
            ms.Seek(0, SeekOrigin.Begin);
            Response.ClearContent();
            Response.ClearHeaders();
            Response.Buffer = true;
            Response.Cache.SetCacheability(HttpCacheability.Private);
            Response.ContentType = "application/vnd.ms-excel";
            Response.AddHeader("Content-Length", ms.Length.ToString(CultureInfo.CurrentCulture));
            Response.AddHeader("Content-Disposition", String.Format("attachment;filename=ReportPosicaoEmprestimoAcoes.xls"));

            Response.BinaryWrite(ms.ToArray());
            ms.Close();
            HttpContext.Current.ApplicationInstance.CompleteRequest();
            Response.End();

            #endregion
        }        
    }
}
