﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Financial.Fundo;
using Financial.Investidor;
using System.Collections.Generic;
using Financial.Web.Util;
using Financial.Investidor.Enums;
using Financial.Security;
using log4net;
using Financial.Security.Enums;
using EntitySpaces.Interfaces;
using DevExpress.Web;
using System.Text;
using Financial.Common;
using Financial.Web.Common;
using Financial.Util;
using Financial.Common.Enums;
using Financial.Bolsa.Custom1;
using Financial.Bolsa;
using Financial.Bolsa.Enums;
using System.IO;
using Financial.Relatorio;

public partial class FiltroReportPosicaoAcoes : FiltroReportBasePage
{
    private static readonly ILog log = LogManager.GetLogger(typeof(FiltroReportPosicaoAcoes));

    new protected void Page_Load(object sender, EventArgs e) {
        //
        base.Page_Load(sender, e);
    }
            
    protected void btnPDF_Click(object sender, EventArgs e) {
        if (this.textDataPosicao.Text == "")
        {
            throw new Exception("Campos com * são obrigatórios!");
        }
        this.SelecionaRelatorio(VisaoRelatorio.visaoPDF);
    }

    
    protected void btnExcel_Click(object sender, EventArgs e) {
        if (this.textDataPosicao.Text == "")
        {
            throw new Exception("Campos com * são obrigatórios!");
        }
        this.SelecionaRelatorio(VisaoRelatorio.visaoExcel);
    }


    private void SelecionaRelatorio(string visao)
    {
        Response.BufferOutput = true;
        //
        // Salva os parametros na Session        
        Session["textDataPosicao"] = textDataPosicao.Text;
        //
        //Response.Redirect("~/Relatorios/Bolsa/ReportPosicaoAcoes.aspx?Visao=" + visao);

        string url = "~/Relatorios/Bolsa/ReportPosicaoAcoes.aspx?Visao=" + visao;
        string redirectURL = Page.ResolveClientUrl(url);

        string script = "window.location = '" + redirectURL + "';";
        ScriptManager.RegisterStartupScript(this, typeof(Page), "RedirectTo", script, true);
    }
}