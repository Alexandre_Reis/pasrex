using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Financial.Common;
using EntitySpaces.Interfaces;
using System.Collections.Generic;
using Financial.Relatorio;
using System.IO;
using System.Globalization;

public partial class _ReportMapaEmprestimoAcoes : Financial.Web.Common.BasePage {

    /*  Como usar o relatorio
        DateTime dataInicio = new DateTime(2000, 1, 1);
        DateTime dataFim = new DateTime(2010, 1, 25);
        string cdAtivoBolsa = "PETR4";        
        //int idCliente = 32;
        int idCliente = 1;

        // 4 parametros
        ReportViewer1.Report = new ReportMapaEmprestimoAcoes(dataInicio, dataFim, cdAtivoBolsa, idCliente);
        
        // 3 parametros
        //ReportViewer1.Report = new ReportMapaEmprestimoAcoes(idCliente, dataInicio, dataFim);
        //ReportViewer1.Report = new ReportMapaEmprestimoAcoes(dataInicio, dataFim, cdAtivoBolsa);

        // 2 parametros
        //ReportViewer1.Report = new ReportMapaEmprestimoAcoes(dataInicio, dataFim);        
     */
    new protected void Page_Load(object sender, EventArgs e) {
        string visao = Request.QueryString["Visao"].ToString();
        //
        //Response.Write(Request.Form["textDataInicio"].ToString());
        //Response.Write("<br>" +Request.Form["textDataFim"].ToString());
        //Response.Write("<br>" + Request.Form["btnEditCodigo"].ToString());        
        //Response.Write("<br>" + Request.Form["textCdAtivo"].ToString());

        DateTime dataInicio = new DateTime();
        DateTime dataFim = new DateTime();
        string cdAtivoBolsa = null;
        int? idCliente = null;

        // Obrigatorio
        dataInicio = Convert.ToDateTime(Session["textDataInicio"]);
        dataFim = Convert.ToDateTime(Session["textDataFim"]);
        //Opcionais
        cdAtivoBolsa = (string)Session["textCdAtivo"];
        if (!String.IsNullOrEmpty((string)Session["btnEditCodigo"])) {
            idCliente = Convert.ToInt32(Session["btnEditCodigo"]);
        }
        
        if (visao == "Report") {
            ReportViewer1.Report = new ReportMapaEmprestimoAcoes(dataInicio, dataFim, cdAtivoBolsa, idCliente);
        }
        else if (visao == "PDF") {
            #region PDF
            ReportMapaEmprestimoAcoes report = new ReportMapaEmprestimoAcoes(dataInicio, dataFim, cdAtivoBolsa, idCliente);

            MemoryStream ms = new MemoryStream();
            report.ExportToPdf(ms);
            ms.Seek(0, SeekOrigin.Begin);
            Response.ClearContent();
            Response.ClearHeaders();
            Response.Buffer = true;
            Response.Cache.SetCacheability(HttpCacheability.Private);
            Response.ContentType = "application/pdf";
            Response.AddHeader("Content-Length", ms.Length.ToString(CultureInfo.CurrentCulture));            
            Response.AddHeader("Content-Disposition", String.Format("attachment;filename=ReportMapaEmprestimoAcoes.pdf"));

            Response.BinaryWrite(ms.ToArray());
            ms.Close();
            HttpContext.Current.ApplicationInstance.CompleteRequest();
            Response.End();
            #endregion
        }
        else if (visao == "Excel") {
            #region Excel
            ReportMapaEmprestimoAcoes report = new ReportMapaEmprestimoAcoes(dataInicio, dataFim, cdAtivoBolsa, idCliente);

            MemoryStream ms = new MemoryStream();
            report.ExportToXls(ms);
            ms.Seek(0, SeekOrigin.Begin);
            Response.ClearContent();
            Response.ClearHeaders();
            Response.Buffer = true;
            Response.Cache.SetCacheability(HttpCacheability.Private);
            Response.ContentType = "application/vnd.ms-excel";
            Response.AddHeader("Content-Length", ms.Length.ToString(CultureInfo.CurrentCulture));
            Response.AddHeader("Content-Disposition", String.Format("attachment;filename=ReportMapaEmprestimoAcoes.xls"));

            Response.BinaryWrite(ms.ToArray());
            ms.Close();
            HttpContext.Current.ApplicationInstance.CompleteRequest();
            Response.End();

            #endregion
        }
    }
}