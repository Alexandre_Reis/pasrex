﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Financial.Fundo;
using Financial.Investidor;
using System.Collections.Generic;
using Financial.Web.Util;
using Financial.Investidor.Enums;
using Financial.Security;
using log4net;
using Financial.Security.Enums;
using EntitySpaces.Interfaces;
using DevExpress.Web;
using System.Text;
using Financial.Common;
using Financial.Web.Common;
using Financial.Util;
using Financial.Common.Enums;
using Financial.Bolsa.Custom1;
using Financial.Bolsa;
using Financial.Bolsa.Enums;

public partial class FiltroReportNotaCorretagemBolsa : FiltroReportBasePage {
    private static readonly ILog log = LogManager.GetLogger(typeof(FiltroReportNotaCorretagemBolsa));

    new protected void Page_Load(object sender, EventArgs e) {
        btnEditCodigo.Focus();
        //
        this.HasPopupCliente = true;
        base.Page_Load(sender, e);

        TrataTravamentoCampos();
    }

    private void TrataTravamentoCampos() {
        Usuario usuario = new Usuario();
        usuario.BuscaUsuario(HttpContext.Current.User.Identity.Name);
        int tipoTrava = usuario.TipoTrava.Value;

        if (tipoTrava == (int)TipoTravaUsuario.TravaCliente) {
            btnEditCodigo.Enabled = false;

            PermissaoCliente permissaoCliente = new PermissaoCliente();
            int? idCliente = permissaoCliente.RetornaClienteAssociado(HttpContext.Current.User.Identity.Name);

            if (idCliente.HasValue) {
                Cliente cliente = new Cliente();

                List<esQueryItem> campos = new List<esQueryItem>();
                campos.Add(cliente.Query.Apelido);
                cliente.LoadByPrimaryKey(campos, idCliente.Value);
                string apelido = cliente.Apelido;

                btnEditCodigo.Text = Convert.ToString(idCliente.Value);
                textNome.Text = apelido;
            }
        }
    }

    protected void ASPxCallback1_Callback(object source, DevExpress.Web.CallbackEventArgs e) {
        string nome = "";
        if (!String.IsNullOrEmpty(e.Parameter) && e.Parameter != "null") {
            int idCliente = Convert.ToInt32(e.Parameter);
            Cliente cliente = new Cliente();
            cliente.LoadByPrimaryKey(idCliente);

            if (cliente.str.Apelido != "") {
                if (cliente.IsAtivo) {
                    PermissaoCliente permissaoCliente = new PermissaoCliente();
                    nome = permissaoCliente.RetornaAcessoClienteComControle(idCliente, HttpContext.Current.User.Identity.Name)
                            ? cliente.str.Apelido : "no_access";                                      
                }
                else {
                    nome = "no_active";
                }
            }
        }
        e.Result = nome;
    }

    protected void EsDSCliente_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e) {
        ClienteCollection clienteCollection = new ClienteCollection();
        clienteCollection.BuscaClientesComAcesso(HttpContext.Current.User.Identity.Name);

        e.Collection = clienteCollection;
    }

    protected void EsDSAgenteMercado_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e) {
        AgenteMercadoCollection agenteMercadoCollection = new AgenteMercadoCollection();
        //
        agenteMercadoCollection.Query
                        .Select(agenteMercadoCollection.Query.IdAgente, agenteMercadoCollection.Query.Nome)
                        .OrderBy(agenteMercadoCollection.Query.Nome.Ascending);

        agenteMercadoCollection.Query.Load();
        //
        AgenteMercado a = new AgenteMercado();
        a.IdAgente = 0;
        a.Nome = "";

        agenteMercadoCollection.AttachEntity(a);
        //
        agenteMercadoCollection.Sort = AgenteMercadoMetadata.ColumnNames.Nome + " ASC";
        //
        e.Collection = agenteMercadoCollection;
    }

    protected void btnPDF_Click(object sender, EventArgs e) {
        this.TrataErros();
        this.SelecionaRelatorio(VisaoRelatorio.visaoPDF);
    }

    /// <summary>
    /// Trata os Erros de Relatorio
    /// </summary>
    private void TrataErros() {

        #region Campos obrigatórios
        List<Control> controles = new List<Control>(new Control[] { 
    	    this.textDataInicio, this.textDataFim, this.dropAgente, this.dropTipoMercado });

        if (base.TestaObrigatorio(controles) != "") {
            throw new Exception("Campos com * são obrigatórios!");
        }

        if (this.dropTipoMercado.SelectedIndex == 0) {
            throw new Exception("Campos com * são obrigatórios!");
        }
        if (this.dropAgente.SelectedIndex == 0) {
            throw new Exception("Campos com * são obrigatórios!");
        }
        #endregion

        DateTime dataInicio = Convert.ToDateTime(this.textDataInicio.Text);
        DateTime dataFim = Convert.ToDateTime(this.textDataFim.Text);

        if (dataFim < dataInicio) {
            throw new Exception("Data Fim deve ser maior que Data Inicial.");
        }
        
        if (!this.RegistrosEncontrados()) {
            throw new Exception("Não há Notas de Corretagem para o período selecionado.");
        }
    }

    /// <summary>
    /// Retorna boleano indicando se tem Notas de Corretagem
    /// </summary>
    /// <returns></returns>
    private bool RegistrosEncontrados() {

        #region Cliente
        int? idCliente = null;

        if (!String.IsNullOrEmpty(this.btnEditCodigo.Text.Trim())) {
            idCliente = Convert.ToInt32(this.btnEditCodigo.Text.Trim());
        }

        List<int> listaIdClientes = new List<int>();
        if (idCliente.HasValue) {
            listaIdClientes.Add(idCliente.Value);
        }
        else {
            ClienteQuery c = new ClienteQuery("C");
            c.Select(c.IdCliente);

            ClienteCollection cliente = new ClienteCollection();
            cliente.Load(c);

            foreach (Cliente c1 in cliente) {
                listaIdClientes.Add(c1.IdCliente.Value);
            }
        }
        #endregion

        // Obrigatórios
        DateTime dataInicio = Convert.ToDateTime(textDataInicio.Text);
        DateTime dataFim = Convert.ToDateTime(textDataFim.Text);
        int idAgente = Convert.ToInt32(this.dropAgente.SelectedItem.Value);

        string tipoMercado = "";
        if (this.dropTipoMercado.SelectedIndex > 0) {
            tipoMercado = Convert.ToString(this.dropTipoMercado.SelectedItem.Value);
        }

        /*---------------------------------------------------------------------------------------------------------*/
        //#region Datas
        //List<DateTime> listaDatas = new List<DateTime>();

        //DateTime dataAux = dataInicio;
        //while (dataAux <= dataFim) {
        //    listaDatas.Add(dataAux);
        //    dataAux = Calendario.AdicionaDiaUtil(dataAux, 1, LocalFeriadoFixo.Brasil, TipoFeriado.Brasil);
        //}
        //#endregion

        //decimal total = 0;
        //for (int i = 0; i < listaIdClientes.Count; i++) {

        //    for (int j = 0; j < listaDatas.Count; j++) {
        //        NotaCorretagemBolsa n = new Financial.Bolsa.Custom1.NotaCorretagemBolsa(listaIdClientes[i], listaDatas[j], idAgente, tipoMercado);

        //        total += n.GetQuantidadeRegistros();
        //    }
        //}

        //return total != 0;
        /*------------------------------------------------------------------------------------------------------- */

        #region Consulta OperacaoBolsa
        OperacaoBolsaCollection opc = new OperacaoBolsaCollection();
        OperacaoBolsaQuery op = new OperacaoBolsaQuery("OP");
        //
        op.Select(op.IdOperacao);
        op.Where(op.IdCliente.In(listaIdClientes),
                 op.Data.Between(dataInicio, dataFim),
                 op.IdAgenteCorretora == idAgente,
                 op.TipoOperacao.In(TipoOperacaoBolsa.Compra, TipoOperacaoBolsa.Venda,
                                    TipoOperacaoBolsa.CompraDaytrade, TipoOperacaoBolsa.VendaDaytrade));

        if (tipoMercado == "VIS_TER_IMO") {
            op.Where(op.TipoMercado.In(TipoMercadoBolsa.MercadoVista, TipoMercadoBolsa.Termo, TipoMercadoBolsa.Imobiliario));
        }
        else if (tipoMercado == "OPC_OPV") {
            op.Where(op.TipoMercado.In(TipoMercadoBolsa.OpcaoCompra, TipoMercadoBolsa.OpcaoVenda));
        }
        else {
            op.Where(op.TipoMercado == tipoMercado);
        }
        //
        opc.Load(op);

        return opc.Count!=0;
        #endregion
    }

    /// <summary>
    /// Seleciona o relatorio a ser executado
    /// </summary>
    /// <param name="visao">PDF, Excel ou Report</param>
    private void SelecionaRelatorio(string visao) {
        Response.BufferOutput = true;
        //
        // Salva os parametros na Session
        Session["btnEditCodigo"] = this.btnEditCodigo.Text.Trim();
        Session["dropTipoMercado"] = this.dropTipoMercado.SelectedIndex > 0 ? Convert.ToString(this.dropTipoMercado.SelectedItem.Value) : "";
 
        //Obrigatórios
        Session["dropAgente"] = Convert.ToString(this.dropAgente.SelectedItem.Value);
        Session["textDataInicio"] = textDataInicio.Text;
        Session["textDataFim"] = textDataFim.Text;
        //
        //Response.Redirect("~/Relatorios/Bolsa/ReportNotaCorretagemBolsa.aspx?Visao=" + visao);

        string url = "~/Relatorios/Bolsa/ReportNotaCorretagemBolsa.aspx?Visao=" + visao;
        string redirectURL = Page.ResolveClientUrl(url);

        string script = "window.location = '" + redirectURL + "';";
        ScriptManager.RegisterStartupScript(this, typeof(Page), "RedirectTo", script, true);
    }
}