using System;
using Financial.Relatorio;
using System.IO;
using System.Web;
using System.Globalization;
using Financial.Web.Common;

public partial class _ReportProventoProvisionado : BasePage {
    private static class VisaoRelatorio
    {
        public static string visaoHTML = "Report";
        public static string visaoPDF = "PDF";
        public static string visaoExcel = "Excel";
    }

    new protected void Page_Load(object sender, EventArgs e) {
        string visao = Request.QueryString["Visao"].ToString();

        // Opcional
        int? idCliente = null;
        if (!String.IsNullOrEmpty((string)Session["btnEditCodigo"])) {
            idCliente = Convert.ToInt32(Session["btnEditCodigo"]);
        }

        int? tipoProvento = null;
        if ( Session["dropTipoProvento"] != null && Convert.ToInt32(Session["dropTipoProvento"]) != -1 ) {
            tipoProvento = Convert.ToInt32(Session["dropTipoProvento"]);
        }
        
        string cdAtivoBolsa = null;
        if (!String.IsNullOrEmpty((string)Session["textCdAtivoBolsa"])) {
            cdAtivoBolsa = Convert.ToString(Session["textCdAtivoBolsa"]);
        }

        DateTime dataInicio = Convert.ToDateTime(Session["textDataInicio"]);
        DateTime dataFim = Convert.ToDateTime(Session["textDataFim"]);

        if (visao == VisaoRelatorio.visaoHTML) {
            ReportViewer1.Report = new ReportProventoProvisionado(idCliente, cdAtivoBolsa, tipoProvento, dataInicio, dataFim);
        }
        else if (visao == VisaoRelatorio.visaoPDF) {
            #region PDF

            ReportProventoProvisionado report = new ReportProventoProvisionado(idCliente, cdAtivoBolsa, tipoProvento, dataInicio, dataFim);

            MemoryStream ms = new MemoryStream();
            report.ExportToPdf(ms);
            ms.Seek(0, SeekOrigin.Begin);
            Response.ClearContent();
            Response.ClearHeaders();
            Response.Buffer = true;
            Response.Cache.SetCacheability(HttpCacheability.Private);
            Response.ContentType = "application/pdf";
            Response.AddHeader("Content-Length", ms.Length.ToString(CultureInfo.CurrentCulture));
            Response.AddHeader("Content-Disposition", String.Format("attachment;filename=ReportProventoRecebido.pdf"));

            Response.BinaryWrite(ms.ToArray());
            ms.Close();
            HttpContext.Current.ApplicationInstance.CompleteRequest();
            Response.End();
            #endregion
        }
        else if (visao == VisaoRelatorio.visaoExcel) {
            #region Excel
            ReportProventoProvisionado report = new ReportProventoProvisionado(idCliente, cdAtivoBolsa, tipoProvento, dataInicio, dataFim);

            MemoryStream ms = new MemoryStream();
            report.ExportToXls(ms);
            ms.Seek(0, SeekOrigin.Begin);
            Response.ClearContent();
            Response.ClearHeaders();
            Response.Buffer = true;
            Response.Cache.SetCacheability(HttpCacheability.Private);
            Response.ContentType = "application/vnd.ms-excel";
            Response.AddHeader("Content-Length", ms.Length.ToString(CultureInfo.CurrentCulture));
            Response.AddHeader("Content-Disposition", String.Format("attachment;filename=ReportProventoRecebido.xls"));

            Response.BinaryWrite(ms.ToArray());
            ms.Close();
            HttpContext.Current.ApplicationInstance.CompleteRequest();
            Response.End();

            #endregion
        }
    }
}