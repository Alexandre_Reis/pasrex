using System;
using Financial.Relatorio;
using System.IO;
using System.Web;
using System.Globalization;
using Financial.Web.Common;

using System.Collections.Generic;
using Financial.Investidor;
using Financial.Util;
using Financial.Common.Enums;

public partial class _ReportNotaCorretagemBolsa : BasePage {        
    new protected void Page_Load(object sender, EventArgs e) {
        
        string visao = Request.QueryString["Visao"].ToString();
        //        
        // Obrigatorios
        DateTime dataInicio = Convert.ToDateTime(Session["textDataInicio"]);
        DateTime dataFim = Convert.ToDateTime(Session["textDataFim"]);
        int idAgente = Convert.ToInt32(Session["dropAgente"]);
        //
        string tipoMercado = Convert.ToString(Session["dropTipoMercado"]);

        #region Cliente
        int? idCliente = null;        

        if (!String.IsNullOrEmpty((string)Session["btnEditCodigo"])) {
            idCliente = Convert.ToInt32(Session["btnEditCodigo"]);
        }

        List<int> listaIdClientes = new List<int>();
        if (idCliente.HasValue) {
            listaIdClientes.Add(idCliente.Value);
        }
        else {
            ClienteQuery c = new ClienteQuery("C");
            c.Select(c.IdCliente);

            ClienteCollection cliente = new ClienteCollection();
            cliente.Load(c);

            foreach (Cliente c1 in cliente) {
                listaIdClientes.Add(c1.IdCliente.Value);
            }
        }
        #endregion

        #region Datas
        List<DateTime> listaDatas = new List<DateTime>();

        DateTime dataAux = dataInicio;
        while (dataAux <= dataFim) {
            listaDatas.Add(dataAux);
            dataAux = Calendario.AdicionaDiaUtil(dataAux, 1, LocalFeriadoFixo.Brasil, TipoFeriado.Brasil);
        }
        #endregion

        if (visao == "PDF") {
            #region PDF

            ReportMaster reportMaster = new ReportMaster();
            reportMaster.CreateDocument();
            reportMaster.PrintingSystem.ContinuousPageNumbering = false;
            //

            for (int i = 0; i < listaIdClientes.Count; i++) {

                for (int j = 0; j < listaDatas.Count; j++) {

                    Financial.Bolsa.Custom1.NotaCorretagemBolsa n = new Financial.Bolsa.Custom1.NotaCorretagemBolsa(listaIdClientes[i], listaDatas[j], idAgente, tipoMercado);
                    if (n.GetQuantidadeRegistros() != 0) {

                        ReportNotaCorretagemBolsa report = new ReportNotaCorretagemBolsa(n);
                        report.CreateDocument();

                        // Anexa o Conteudo do Relatorio no Relatorio Master
                        reportMaster.Pages.AddRange(report.PrintingSystem.Pages);
                    }
                }
            }
                                                  
            MemoryStream ms = new MemoryStream();
            reportMaster.ExportToPdf(ms);
            ms.Seek(0, SeekOrigin.Begin);
            Response.ClearContent();
            Response.ClearHeaders();
            Response.Buffer = true;
            Response.Cache.SetCacheability(HttpCacheability.Private);
            Response.ContentType = "application/pdf";
            Response.AddHeader("Content-Length", ms.Length.ToString(CultureInfo.CurrentCulture));
            Response.AddHeader("Content-Disposition", String.Format("attachment;filename=ReportNotaCorretagem.pdf"));

            Response.BinaryWrite(ms.ToArray());
            ms.Close();
            HttpContext.Current.ApplicationInstance.CompleteRequest();
            Response.End();
            #endregion
        }
    }
}