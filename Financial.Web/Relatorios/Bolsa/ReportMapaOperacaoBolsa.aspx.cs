using System;
using Financial.Relatorio;
using System.IO;
using System.Web;
using System.Globalization;

public partial class _ReportMapaOperacaoBolsa : Financial.Web.Common.BasePage {        
    /*
     * Como Usar:
        //
        DateTime dataInicio = new DateTime(2000, 1, 1);
        DateTime dataFim = new DateTime(2010, 1, 25);
        string cdAtivoBolsa = "PETR4";
        string tipoMercado = "VIS";
        int idCliente = 32;

        // 5 parametros
        ReportViewer1.Report = new ReportMapaOperacaoBolsa(dataInicio, dataFim, tipoMercado, idCliente, cdAtivoBolsa);

        // 3 parametros
        //ReportViewer1.Report = new ReportMapaOperacaoBolsa(dataInicio, dataFim, idCliente);
        //ReportViewer1.Report = new ReportMapaOperacaoBolsa(dataInicio, dataFim, cdAtivoBolsa);
        //ReportViewer1.Report = new ReportMapaOperacaoBolsa(tipoMercado, dataInicio, dataFim);

        // 4 parametros
        //ReportViewer1.Report = new ReportMapaOperacaoBolsa(dataInicio, dataFim, cdAtivoBolsa, idCliente);
        //ReportViewer1.Report = new ReportMapaOperacaoBolsa(dataInicio, dataFim, cdAtivoBolsa, tipoMercado);
        //ReportViewer1.Report = new ReportMapaOperacaoBolsa(tipoMercado, idCliente, dataInicio, dataFim);        
     */
    new protected void Page_Load(object sender, EventArgs e) {
        //CultureInfo ci = Thread.CurrentThread.CurrentCulture;
        //Response.Write(ci.Name);

        
        //if(ci.Name == "pt-BR") {
            // Seta a lingua para portugues
            //ci.DateTimeFormat.ShortDatePattern = StringEnum.GetStringValue(PatternData.formato2);
            //ci.DateTimeFormat.ShortDatePattern = "d/MM/yy";

            // teste
            //CultureInfo culturaLocal = new CultureInfo("pt-BR");
            //culturaLocal.DateTimeFormat.ShortDatePattern = "d/MM/yy";
            //Thread.CurrentThread.CurrentCulture = culturaLocal;

            //ci = Thread.CurrentThread.CurrentCulture;
        //}
                
        //Response.Write("Short date pattern: "+ci.DateTimeFormat.ShortDatePattern);
        //DateTime data = new DateTime(2000, 9, 1);
        //Response.Write(data.ToShortDateString());
        //Response.Write("\n" + data.ToString("d"));            
        
        string visao = Request.QueryString["Visao"].ToString();
        //
        //Response.Write(Request.Form["textDataInicio"].ToString());
        //Response.Write("<br>" +Request.Form["textDataFim"].ToString());
        //Response.Write("<br>" + Request.Form["btnEditCodigo"].ToString());
        //Response.Write("<br>" + Request.Form["dropTipoMercado"].ToString());
        //Response.Write("<br>" + Request.Form["textCdAtivo"].ToString());

        DateTime dataInicio = new DateTime();
        DateTime dataFim = new DateTime();
        string cdAtivoBolsa = null;
        int? idCliente = null;
        string tipoMercado = null;
        int? idAgenteCorretora = null;
        string tipoOperacao = null;

        // Obrigatorio
        dataInicio = Convert.ToDateTime(Session["textDataInicio"]);
        dataFim = Convert.ToDateTime(Session["textDataFim"]);
        //Opcionais
        cdAtivoBolsa = (string)Session["textCdAtivo"];
        tipoMercado = (string)Session["dropTipoMercado"];
        if (!String.IsNullOrEmpty((string)Session["btnEditCodigo"])) {
            idCliente = Convert.ToInt32(Session["btnEditCodigo"]);
        }
        if (!String.IsNullOrEmpty((string)Session["dropAgente"])) {
            idAgenteCorretora = Convert.ToInt32(Session["dropAgente"]);
        }
        if (!String.IsNullOrEmpty((string)Session["dropTipoOperacao"])) {
            tipoOperacao = Convert.ToString(Session["dropTipoOperacao"]);
        }

        if (visao == "Report") {
            ReportViewer1.Report = new ReportMapaOperacaoBolsa(dataInicio, dataFim, tipoMercado, idCliente, cdAtivoBolsa, idAgenteCorretora, tipoOperacao);
        }
        else if (visao == "PDF") {
            #region PDF
            ReportMapaOperacaoBolsa report = new ReportMapaOperacaoBolsa(dataInicio, dataFim, tipoMercado, idCliente, cdAtivoBolsa, idAgenteCorretora, tipoOperacao);

            MemoryStream ms = new MemoryStream();
            report.ExportToPdf(ms);
            ms.Seek(0, SeekOrigin.Begin);
            Response.ClearContent();
            Response.ClearHeaders();
            Response.Buffer = true;
            Response.Cache.SetCacheability(HttpCacheability.Private);
            Response.ContentType = "application/pdf";
            Response.AddHeader("Content-Length", ms.Length.ToString(CultureInfo.CurrentCulture));
            Response.AddHeader("Content-Disposition", String.Format("attachment;filename=ReportMapaOperacaoBolsa.pdf"));

            Response.BinaryWrite(ms.ToArray());
            ms.Close();
            HttpContext.Current.ApplicationInstance.CompleteRequest();
            Response.End();
            #endregion
        }
        else if (visao == "Excel") {
            #region Excel
            ReportMapaOperacaoBolsa report = new ReportMapaOperacaoBolsa(dataInicio, dataFim, tipoMercado, idCliente, cdAtivoBolsa, idAgenteCorretora, tipoOperacao);            

            MemoryStream ms = new MemoryStream();
            report.ExportToXls(ms);
            ms.Seek(0, SeekOrigin.Begin);
            Response.ClearContent();
            Response.ClearHeaders();
            Response.Buffer = true;
            Response.Cache.SetCacheability(HttpCacheability.Private);
            Response.ContentType = "application/vnd.ms-excel";
            Response.AddHeader("Content-Length", ms.Length.ToString(CultureInfo.CurrentCulture));
            Response.AddHeader("Content-Disposition", String.Format("attachment;filename=ReportMapaOperacaoBolsa.xls"));

            Response.BinaryWrite(ms.ToArray());
            ms.Close();
            HttpContext.Current.ApplicationInstance.CompleteRequest();
            Response.End();

            #endregion
        }
    }
}