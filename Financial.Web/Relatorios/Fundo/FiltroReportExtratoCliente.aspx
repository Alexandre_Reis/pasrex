﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="FiltroReportExtratoCliente.aspx.cs" Inherits="FiltroReportExtratoCliente" %>

<%@ Register Assembly="System.Web, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a" Namespace="System.Web.UI.HtmlControls" TagPrefix="cc2" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxcb" %>
<%@ Register Assembly="System.Web, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a" Namespace="System.Web.UI" TagPrefix="cc1" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxnb" %>    
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxe" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxwgv" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxpc" %>
<%@ Register Assembly="EntitySpaces.Web" Namespace="EntitySpaces.Web" TagPrefix="cc1" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >

<head id="Head1" runat="server">
<link href="~/css/forms.css" rel="stylesheet" type="text/css" />

<script type="text/javascript" language="JavaScript">
var backgroundColor;
function OnGetDataCarteira(values) {
    btnEditCodigo.SetValue(values);                    
    popupCarteira.HideWindow();
    ASPxCallback1.SendCallback(btnEditCodigo.GetValue());        
    btnEditCodigo.Focus();    
}
</script>
<script type="text/javascript" language="Javascript" src="../../js/global.js"></script>
</head>

<body >
    <form id="form1" runat="server"> 
    
    <dxcb:ASPxCallback ID="callBackPeriodo" runat="server" OnCallback="callBackPeriodo_Callback">
        <ClientSideEvents CallbackComplete="function(s, e) 
        {   
            if (e.result != '')
            {                   
                var resultSplit = e.result.split('|');
                
                var lingua = resultSplit[2];            
                
                var newDate = LocalizedData(resultSplit[0], lingua);
                textDataInicio.SetValue(newDate);
                
                var newDate = LocalizedData(resultSplit[1], lingua);
                textDataFim.SetValue(newDate);
            }
        }        
        "/>
    </dxcb:ASPxCallback>
    
    
    <dxcb:ASPxCallback ID="ASPxCallback1" runat="server" OnCallback="ASPxCallback1_Callback">
        <ClientSideEvents CallbackComplete="function(s, e) 
        {
            var resultAux = e.result.split('|');
            e.result = resultAux[0]; // Retira o IdIndice
            //
            
            var indice = resultAux[1]; // Salva o IdIndice
            var mostraCaixa = resultAux[2]; // MostraCaixa
                
            var explodeFundos = resultAux[3]; // Explode Fundos
            
            OnCallBackCompleteCliente(s, e, popupMensagemCarteira, btnEditCodigo, document.getElementById('textNome') );

            //dropIndice.SetVisible(false);
            
            // Seleciona o Indice da Carteira - dropIndice não consegue passar como parametro
            selecionado = false;
            
            for(var i = 0; i < dropIndice.GetItemCount(); i++) 
            {
                if (dropIndice.GetItem(i).value == indice) 
                {                
                    dropIndice.SetSelectedItem(dropIndice.GetItem(i));
                    selecionado = true;
                    break;                    
                }
            }
            
            if(selecionado == false)
            {
                //dropIndice.SetSelectedIndex(-1);
            }
                        
            var status = '';
            if(mostraCaixa == '') { // Nao Checado
                status = false;
            }
            else {
                // N = Checado, S = Nao Checado
                var status = mostraCaixa == 'N' ? true : false;
            }
                        
            chkMostraCaixa.SetChecked(status);
            
            // Se indice for -1 reseta combo
            if(indice == -1) {
                dropIndice.SetSelectedIndex(-1);
            }    
            
            var statusExplodeFundos = '';
            if(explodeFundos == '') { // Nao Checado
                statusExplodeFundos = false;
            }
            else {
                var statusExplodeFundos = explodeFundos == 'S' ? true : false;
            }
            chkExplodeFundos.SetChecked(statusExplodeFundos);
            chkExplodeFundos.SetEnabled(statusExplodeFundos);
        }        
        "/>
    </dxcb:ASPxCallback>
        
        
        
    
    <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="true" EnableScriptLocalization="true"/>
    
    <div class="divPanel">
    <table width="100%"><tr><td>
    <div id="container_small">
    
    <div id="header"><asp:Label ID="label1" runat="server" Text="Extrato Consolidado de Cliente" /></div>
        
    <div id="mainContentSpace">
           
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
    <ContentTemplate>
        
        <div class="reportFilter">
        <div style="height:20px"></div>
        <table cellpadding="2" cellspacing="2" border=0>
            <tr>
            <td class="td_Label">
                <asp:Label ID="labelCarteira" runat="server" CssClass="labelRequired" Text="Carteira:"></asp:Label>
            </td>
            
            <td>
                <dxe:ASPxSpinEdit ID="btnEditCodigo" runat="server" CssClass="textButtonEdit" 
                                            EnableClientSideAPI="True"
                                            ClientInstanceName="btnEditCodigo" SpinButtons-ShowIncrementButtons="false"
                                            MaxLength="9" NumberType="Integer" AllowMouseWheel="false">  
                <Buttons>
                    <dxe:EditButton>
                    </dxe:EditButton>                                
                </Buttons>        
                <ClientSideEvents                        
                         KeyPress="function(s, e) {document.getElementById('textNome').value = '';}" 
                         ButtonClick="function(s, e) {popupCarteira.ShowAtElementByID(s.name);}" 
                         LostFocus="function(s, e) {OnLostFocus(popupMensagemCarteira, ASPxCallback1, btnEditCodigo);}"
                />                            
                </dxe:ASPxSpinEdit>                
            </td>                          
            
            <td colspan="2" width="300">
                <asp:TextBox ID="textNome" runat="server" CssClass="textNome" Enabled="false"></asp:TextBox>       
            </td>
            </tr>
                        
            <tr>
            <td class="td_Label">
                <asp:Label ID="label2" runat="server" CssClass="labelNormal" Text="Período:"/>
            </td>
            <td colspan="2">
                <dxe:ASPxComboBox ID="dropTipoPeriodo" runat="server" ClientInstanceName="dropTipoPeriodo" DropDownStyle="DropDown" ShowShadow="False" CssClass="dropDownListCurto_2" 
                            ClientSideEvents-SelectedIndexChanged="function(s, e) { callBackPeriodo.SendCallback(); }">
                                    
                </dxe:ASPxComboBox>            
            </td>
            </tr>
            
            <tr>
            <td class="td_Label">                
                <asp:Label ID="labelDataInicio" runat="server" CssClass="labelRequired" Text="Início:"></asp:Label>
            </td>                        
            <td>
                <dxe:ASPxDateEdit ID="textDataInicio" runat="server" ClientInstanceName="textDataInicio" />
            </td>  
            
             <td colspan="2">
                <table>
                    <tr><td><asp:Label ID="labelDataFim" runat="server" CssClass="labelRequired" Text="Fim:"/></td>
                    <td><dxe:ASPxDateEdit ID="textDataFim" runat="server" ClientInstanceName="textDataFim"/></td></tr>
                </table>
            </td>   
                                                                            
            </tr>
            <tr>
                <td class="td_Label">
                    <asp:Label ID="labelIndiceFiltro" runat="server" CssClass="labelRequired" Text="Índice:" />
                </td>
                                    
                <td colspan="3">
                    <dxe:ASPxComboBox ID="dropIndice" runat="server" DropDownStyle="DropDownList" CssClass="dropDownListLongo"
                     DataSourceID="EsDSIndice" ValueField="IdIndice" TextField="Descricao" EnableClientSideAPI="True" ClientInstanceName="dropIndice">
                    
                    <ClientSideEvents                        
                         Init="function(s, e) {}"
                    />                                                              
                    </dxe:ASPxComboBox>                                                                               
                </td>    
            </tr>
            
            <tr>                                    
                <td colspan="4" style="padding-left:15px">
                    <dxe:ASPxCheckBox ID="chkMostraCaixa" ClientInstanceName="chkMostraCaixa" runat="server" Text="Mostra C/C"/>
                </td>    
            </tr>
            <tr>                                    
                <td colspan="4" style="padding-left:15px">
                    <dxe:ASPxCheckBox ID="chkExplodeFundos" ClientInstanceName="chkExplodeFundos" runat="server" Text="Explode Cotas de Fundos"/>
                </td>    
            </tr>
        </table>                                                                                 
        
        </div>
        
        <div id="reportLinkButton" class="linkButton linkButtonNoBorder">
           <asp:LinkButton ID="btnVisualiza" runat="server" Font-Overline="false" CssClass="btnReport" OnClick="btnVisualiza_Click"><asp:Literal ID="Literal1" runat="server" Text="Visualizar"/><div></div></asp:LinkButton>
           <asp:LinkButton ID="btnPDF" runat="server" Font-Overline="false" CssClass="btnPdf" OnClick="btnPDF_Click"><asp:Literal ID="Literal2" runat="server" Text="Gerar PDF"/><div></div></asp:LinkButton>
           <asp:LinkButton ID="btnExcel" runat="server" Font-Overline="false"  CssClass="btnExcel" OnClick="btnExcel_Click"><asp:Literal ID="Literal3" runat="server" Text="Gerar Excel"/><div></div></asp:LinkButton>
        </div>
        
    </ContentTemplate>
    </asp:UpdatePanel>
    
    </div>
    
    </div>
    </td></tr></table>
    </div>
        
    <cc1:esDataSource ID="EsDSCarteira" runat="server" OnesSelect="EsDSCarteira_esSelect" />
    <cc1:esDataSource ID="EsDSIndice" runat="server" OnesSelect="EsDSIndice_esSelect" />
        
   </form>
</body>
</html>