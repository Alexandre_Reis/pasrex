using System;
using Financial.Relatorio;
using System.IO;
using System.Web;
using System.Globalization;
using Financial.Web.Common;

public partial class _ReportMapaResultado : BasePage {
    
    /* Como Usar:
        int idCliente = 1;
        DateTime dataInicio = new DateTime(2000, 1, 1);
        DateTime dataFim = new DateTime(2010, 1, 1);

        ReportViewer1.Report = new ReportMapaResultado(idCliente, dataInicio, dataFim);
     */
    new protected void Page_Load(object sender, EventArgs e) {
        string visao = Request.QueryString["Visao"].ToString();
        //
        //Response.Write(Session["textDataInicio"]);
        //Response.Write("<br>" + Session["textDataFim"]);
        //Response.Write("<br>" + Session["btnEditCodigo"]);

        // Parametros vem da session - obrigatorios
        DateTime dataInicio = Convert.ToDateTime(Session["textDataInicio"]);
        DateTime dataFim = Convert.ToDateTime(Session["textDataFim"]);
        int idCarteira = Convert.ToInt32(Session["btnEditCodigo"]);

        if (visao == "Report") {
            ReportViewer1.Report = new ReportMapaResultado(idCarteira, dataInicio, dataFim);
            
            //SubReportMapaResultadoBolsa report = new SubReportMapaResultadoBolsa();
            //report.PersonalInitialize(idCarteira, dataInicio, dataFim);
            
            //SubReportMapaResultadoBMF report = new SubReportMapaResultadoBMF();
            //report.PersonalInitialize(idCarteira, dataInicio, dataFim);

            //ReportMapaResultado report = new ReportMapaResultado(idCarteira, dataInicio, dataFim);
            //
            //ReportViewer1.Report = report;
        }
        else if (visao == "PDF") {
            #region PDF
            ReportMapaResultado report = new ReportMapaResultado(idCarteira, dataInicio, dataFim);

            MemoryStream ms = new MemoryStream();
            report.ExportToPdf(ms);
            ms.Seek(0, SeekOrigin.Begin);
            Response.ClearContent();
            Response.ClearHeaders();
            Response.Buffer = true;
            Response.Cache.SetCacheability(HttpCacheability.Private);
            Response.ContentType = "application/pdf";
            Response.AddHeader("Content-Length", ms.Length.ToString(CultureInfo.CurrentCulture));
            Response.AddHeader("Content-Disposition", String.Format("attachment;filename=ReportMapaResultado.pdf"));

            Response.BinaryWrite(ms.ToArray());
            ms.Close();
            HttpContext.Current.ApplicationInstance.CompleteRequest();
            Response.End();
            #endregion
        }
        else if (visao == "Excel") {
            #region Excel
            ReportMapaResultado report = new ReportMapaResultado(idCarteira, dataInicio, dataFim);

            MemoryStream ms = new MemoryStream();
            report.ExportToXls(ms);
            ms.Seek(0, SeekOrigin.Begin);
            Response.ClearContent();
            Response.ClearHeaders();
            Response.Buffer = true;
            Response.Cache.SetCacheability(HttpCacheability.Private);
            Response.ContentType = "application/vnd.ms-excel";
            Response.AddHeader("Content-Length", ms.Length.ToString(CultureInfo.CurrentCulture));
            Response.AddHeader("Content-Disposition", String.Format("attachment;filename=ReportMapaResultado.xls"));

            Response.BinaryWrite(ms.ToArray());
            ms.Close();
            HttpContext.Current.ApplicationInstance.CompleteRequest();
            Response.End();

            #endregion         
        }
    }
}
