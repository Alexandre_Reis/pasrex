﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using log4net;
using Financial.Web.Common;
using Financial.Security;
using Financial.Security.Enums;
using System.Collections.Generic;
using EntitySpaces.Interfaces;
using Financial.Investidor;
using System.Web.UI;
using Financial.Common;
using Financial.Fundo;
using Financial.Fundo.Enums;
using Financial.Investidor.Enums;
using System.Threading;

public partial class FiltroReportExtratoCliente : FiltroReportBasePage
{
    private static readonly ILog log = LogManager.GetLogger(typeof(FiltroReportExtratoCliente));

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    new protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            btnEditCodigo.Focus();

            this.PopulaPeriodoDatas(this.dropTipoPeriodo);

            this.HasPopupCarteira = true;
            base.Page_Load(sender, e);

            TrataTravamentoCampos();
        }
        catch (Exception exception)
        {
            new Financial.Web.Util.ErrorHandler().HandleArrayIndexError(exception);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void callBackPeriodo_Callback(object source, DevExpress.Web.CallbackEventArgs e)
    {
        e.Result = "";
        if (btnEditCodigo.Text != "")
        {
            int idCliente = Convert.ToInt32(btnEditCodigo.Text);

            Cliente cliente = new Cliente();

            DateTime dataInicio = new DateTime();
            DateTime dataFim = new DateTime();

            if (dropTipoPeriodo.SelectedIndex > 0)
            {
                cliente.RetornaDatas(idCliente, (TipoPeriodoDatas)dropTipoPeriodo.SelectedIndex, ref dataInicio, ref dataFim);

                e.Result = dataInicio.ToShortDateString() + "|" + dataFim.ToShortDateString() + "|" + Thread.CurrentThread.CurrentCulture.Name;
            }
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void ASPxCallback1_Callback(object source, DevExpress.Web.CallbackEventArgs e)
    {
        try
        {
            string nome = "";
            int idIndice = -1;
            string mostraCaixa = "";
            string explodeFundos = "";

            if (!String.IsNullOrEmpty(e.Parameter) && e.Parameter != "null")
            {
                int idCarteira = Convert.ToInt32(e.Parameter);
                Carteira carteira = new Carteira();
                Cliente cliente = new Cliente();
                //
                if (carteira.LoadByPrimaryKey(idCarteira))
                {
                    idIndice = (int)carteira.IdIndiceBenchmark;
                    explodeFundos = carteira.ExplodeCotasDeFundos ?? "N";

                    List<esQueryItem> campos = new List<esQueryItem>();
                    campos.Add(cliente.Query.ZeraCaixa);
                    
                    if (cliente.LoadByPrimaryKey(campos, idCarteira))
                    {
                        mostraCaixa = cliente.ZeraCaixa.Trim();
                    }
                }

                if (carteira.str.Apelido != "")
                {
                    if (carteira.IsAtivo)
                    {
                        //O permissionamento da carteira é dado direto no cliente vinculado à carteira
                        PermissaoCliente permissaoCliente = new PermissaoCliente();
                        nome = permissaoCliente.RetornaAcessoClienteComControle(idCarteira, HttpContext.Current.User.Identity.Name)
                                ? carteira.str.Apelido : "no_access";
                    }
                    else
                    {
                        nome = "no_active";
                    }

                    // Se não tem acesso reseta Indice e Mostra Caixa
                    if (nome == "no_access" || nome == "no_active")
                    {
                        idIndice = -1;
                        mostraCaixa = "";
                        explodeFundos = "";
                    }
                }
            }

            // IdIndice a Selecionar: -1 se não tiver indice
            e.Result = nome + "|" + idIndice + "|" + mostraCaixa + "|" + explodeFundos;
        }
        catch (Exception exception)
        {
            new Financial.Web.Util.ErrorHandler().HandleArrayIndexError(exception);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void EsDSIndice_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        try
        {
        IndiceCollection coll = new IndiceCollection();
        Indice indice = new Indice();

        coll = indice.RetornaCollectionIndicesComCotacoes();

        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
        }
        catch (Exception exception)
        {
            new Financial.Web.Util.ErrorHandler().HandleArrayIndexError(exception);
            new Financial.Web.Util.ErrorHandler().HandleArrayIndexError(exception);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void EsDSCarteira_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        try
        {
        CarteiraCollection carteiraCollection = new CarteiraCollection();
        carteiraCollection.BuscaCarteirasComAcesso(HttpContext.Current.User.Identity.Name);

        e.Collection = carteiraCollection;
        }
        catch (Exception exception)
        {
            new Financial.Web.Util.ErrorHandler().HandleArrayIndexError(exception);
        }
    }

    private void TrataTravamentoCampos()
    {
        try
        {
            //Chegar se foi passado um IdCliente como parametro
            string idClienteParam = Request["idCliente"];
            bool temClienteQueryString = !String.IsNullOrEmpty(idClienteParam);
            int? idClienteQueryString = null;

            if (temClienteQueryString)
            {
                idClienteQueryString = Convert.ToInt32(idClienteParam);

                //Garantir que usuario tem acesso ao cliente
                bool temAcesso = ChecaUsuarioAcessaCliente(idClienteQueryString.Value);

                if (!temAcesso)
                {
                    throw new Exception("Usuário não possui acesso à carteira " + idClienteParam);
                }
            }

            Usuario usuario = new Usuario();
            usuario.BuscaUsuario(HttpContext.Current.User.Identity.Name);
            int tipoTrava = usuario.TipoTrava.Value;

            if (tipoTrava == (int)TipoTravaUsuario.TravaCliente)
            {
                btnEditCodigo.Enabled = false;
            }

            if (temClienteQueryString)
            {
                PreencheCamposCarteira(idClienteQueryString.Value);
            }
            else if (tipoTrava == (int)TipoTravaUsuario.TravaCliente)
            {
                PermissaoCliente permissaoCliente = new PermissaoCliente();
                int? idCarteira = permissaoCliente.RetornaClienteAssociado(HttpContext.Current.User.Identity.Name);

                if (idCarteira.HasValue)
                {
                    PreencheCamposCarteira(idCarteira.Value);
                }
            }
        }
        catch (Exception exception)
        {
            new Financial.Web.Util.ErrorHandler().HandleArrayIndexError(exception);
        }
    }

    protected bool ChecaUsuarioAcessaCliente(int idCliente)
    {
        ClienteCollection clienteCollection = new ClienteCollection();
        clienteCollection.BuscaClientesComAcesso(HttpContext.Current.User.Identity.Name);

        bool temAcesso = false;
        foreach (Cliente cliente in clienteCollection)
        {
            if (cliente.IdCliente.Value == idCliente)
            {
                temAcesso = true;
                break;
            }
        }

        return temAcesso;
    }

    protected void PreencheCamposCarteira(int idCarteira)
    {
        Cliente cliente = new Cliente();

        List<esQueryItem> campos = new List<esQueryItem>();
        campos.Add(cliente.Query.Apelido);
        cliente.LoadByPrimaryKey(campos, idCarteira);
        string apelido = cliente.Apelido;

        btnEditCodigo.Text = Convert.ToString(idCarteira);
        textNome.Text = apelido;
        
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnVisualiza_Click(object sender, EventArgs e)
    {
        try
        {
        this.TrataErros();
        this.SelecionaRelatorio(VisaoRelatorio.visaoHTML);
        }
        catch (Exception exception)
        {
            new Financial.Web.Util.ErrorHandler().HandleArrayIndexError(exception);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnPDF_Click(object sender, EventArgs e)
    {
        try
        {
        this.TrataErros();
        this.SelecionaRelatorio(VisaoRelatorio.visaoPDF);
        }
        catch (Exception exception)
        {
            new Financial.Web.Util.ErrorHandler().HandleArrayIndexError(exception);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnExcel_Click(object sender, EventArgs e)
    {
        try
        {
        this.TrataErros();
        this.SelecionaRelatorio(VisaoRelatorio.visaoExcel);
        }
        catch (Exception exception)
        {
            new Financial.Web.Util.ErrorHandler().HandleArrayIndexError(exception);
        }
    }

    /// <summary>
    /// Trata os Erros de Relatorio
    /// </summary>
    private void TrataErros()
    {
        #region Campos obrigatórios
        List<Control> controles = new List<Control>(new Control[] {
            this.btnEditCodigo, this.textDataInicio, this.textDataFim, this.dropIndice });

        if (base.TestaObrigatorio(controles) != "")
        {
            throw new Exception("Campos com * são obrigatórios!");
        }

        if (Convert.ToDateTime(this.textDataFim.Text) < Convert.ToDateTime(this.textDataInicio.Text))
        {
            throw new Exception("Data Fim menor que Data Início");
        }
        #endregion
    }

    /// <summary>
    /// Seleciona o relatorio a ser executado
    /// </summary>
    /// <param name="visao">PDF, Excel ou Report</param>
    private void SelecionaRelatorio(string visao)
    {

        int? idCarteira = null;
        if (!String.IsNullOrEmpty(this.btnEditCodigo.Text.Trim()))
        {
            idCarteira = Convert.ToInt32(this.btnEditCodigo.Text.Trim());
        }

        if (idCarteira.HasValue && this.TemPermissao(idCarteira.Value))
        {
            Response.BufferOutput = true;
            //
            // Salva os parametros na session
            Session["textDataInicio"] = textDataInicio.Text;
            Session["textDataFim"] = textDataFim.Text;
            Session["btnEditCodigo"] = btnEditCodigo.Text;
            Session["dropIndice"] = this.dropIndice.Value;
            Session["chkMostraCaixa"] = this.chkMostraCaixa.Checked;
            Session["chkExplodeFundos"] = this.chkExplodeFundos.Checked;
            
            //Response.Redirect("~/Relatorios/Fundo/ReportExtratoCliente.aspx?Visao=" + visao);

            string url = "~/Relatorios/Fundo/ReportExtratoCliente.aspx?Visao=" + visao;
            string redirectURL = Page.ResolveClientUrl(url);

            string script = "window.location = '" + redirectURL + "';";
            ScriptManager.RegisterStartupScript(this, typeof(Page), "RedirectTo", script, true);
        }
    }

    /// <summary>
    /// Indica se um Idcarteira tem permissao de executar um relatorio
    /// </summary>
    /// <param name="idCarteira"></param>
    /// <returns></returns>
    private bool TemPermissao(int idCarteira)
    {
        bool retorno = true;

        Carteira carteira = new Carteira();
        string nome = "";
        if (carteira.LoadByPrimaryKey(idCarteira))
        {
            if (carteira.IsAtivo)
            {

                try
                {
                    PermissaoCliente permissaoCliente = new PermissaoCliente();
                    nome = permissaoCliente.RetornaAcessoCliente(idCarteira, HttpContext.Current.User.Identity.Name)
                                            ? carteira.str.Apelido : "no_access";
                }
                catch (Exception e1)
                {
                    return false;
                }
            }
            else
            {
                nome = "no_active";
            }

            if (nome == "no_access" || nome == "no_active")
            {
                return false;
            }
        }
        else
        {
            return false;
        }

        return retorno;
    }
}