using System;
using Financial.Relatorio;
using System.IO;
using System.Web;
using System.Globalization;
using Financial.Web.Common;

public partial class _ReportLiquidacaoCarteira : Financial.Web.Common.BasePage {

    new protected void Page_Load(object sender, EventArgs e) {
        string visao = Request.QueryString["Visao"].ToString();
        //
        //Response.Write(Request.Form["textData"].ToString());
        //Response.Write("<br>" + Request.Form["btnEditCodigoCliente"].ToString());
        //Response.End();
        
        DateTime data = new DateTime();
        int? idCliente = null;

        // Obrigatorio
        data = Convert.ToDateTime(Session["textData"]);
        // Opcional
        if (!String.IsNullOrEmpty((string)Session["btnEditCodigoCliente"])) {
            idCliente = Convert.ToInt32(Session["btnEditCodigoCliente"]);
        }            

        if (visao == "Report") {
            ReportViewer1.Report = new ReportLiquidacaoCarteira(idCliente.Value, data);
        }
        else if (visao == "PDF") {
            #region PDF
            ReportLiquidacaoCarteira report = new ReportLiquidacaoCarteira(idCliente.Value, data);
           
            MemoryStream ms = new MemoryStream();
            report.ExportToPdf(ms);
            ms.Seek(0, SeekOrigin.Begin);
            Response.ClearContent();
            Response.ClearHeaders();
            Response.Buffer = true;
            Response.Cache.SetCacheability(HttpCacheability.Private);
            Response.ContentType = "application/pdf";
            Response.AddHeader("Content-Length", ms.Length.ToString(CultureInfo.CurrentCulture));
            Response.AddHeader("Content-Disposition", String.Format("attachment;filename=ReportLiquidacaoCarteira.pdf"));

            Response.BinaryWrite(ms.ToArray());
            ms.Close();
            HttpContext.Current.ApplicationInstance.CompleteRequest();
            Response.End();
            #endregion
        }
        else if (visao == "Excel") {
            #region Excel
            ReportLiquidacaoCarteira report = new ReportLiquidacaoCarteira(idCliente.Value, data);        

            MemoryStream ms = new MemoryStream();
            report.ExportToXls(ms);
            ms.Seek(0, SeekOrigin.Begin);
            Response.ClearContent();
            Response.ClearHeaders();
            Response.Buffer = true;
            Response.Cache.SetCacheability(HttpCacheability.Private);
            Response.ContentType = "application/vnd.ms-excel";
            Response.AddHeader("Content-Length", ms.Length.ToString(CultureInfo.CurrentCulture));
            Response.AddHeader("Content-Disposition", String.Format("attachment;filename=ReportLiquidacaoCarteira.xls"));

            Response.BinaryWrite(ms.ToArray());
            ms.Close();
            HttpContext.Current.ApplicationInstance.CompleteRequest();
            Response.End();

            #endregion
        }
    }
}
