﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Text;
using System.Threading;
using System.Collections.Generic;

using Financial.Fundo;
using Financial.Investidor;
using Financial.Security;
using Financial.Util;
using Financial.Web.Util;
using Financial.Web.Common;

using log4net;
using Financial.Security.Enums;
using Financial.Investidor.Enums;
using Financial.Common.Enums;

using EntitySpaces.Interfaces;

using DevExpress.Web;

public partial class FiltroReportSaldoAplicacaoFundo : FiltroReportBasePage {
    private static readonly ILog log = LogManager.GetLogger(typeof(FiltroReportSaldoAplicacaoFundo));

    new protected void Page_Load(object sender, EventArgs e) {
        btnEditCodigoCliente.Focus();

        this.HasPopupCarteira = true;
        this.HasPopupCliente = true;
        base.Page_Load(sender, e);

        TrataTravamentoCampos();
    }

    private void TrataTravamentoCampos()
    {
        try
        {
            //Chegar se foi passado um IdCliente como parametro
            string idClienteParam = Request["idCliente"];
            bool temClienteQueryString = !String.IsNullOrEmpty(idClienteParam);
            int? idClienteQueryString = null;

            if (temClienteQueryString)
            {
                idClienteQueryString = Convert.ToInt32(idClienteParam);

                //Garantir que usuario tem acesso ao cliente
                bool temAcesso = ChecaUsuarioAcessaCliente(idClienteQueryString.Value);

                if (!temAcesso)
                {
                    throw new Exception("Usuário não possui acesso à carteira " + idClienteParam);
                }
            }

            Usuario usuario = new Usuario();
            usuario.BuscaUsuario(HttpContext.Current.User.Identity.Name);
            int tipoTrava = usuario.TipoTrava.Value;

            if (tipoTrava == (int)TipoTravaUsuario.TravaCliente)
            {
                btnEditCodigo.Enabled = false;
            }

            if (temClienteQueryString)
            {
                PreencheCamposCarteira(idClienteQueryString.Value);
            }
            else if (tipoTrava == (int)TipoTravaUsuario.TravaCliente)
            {
                PermissaoCliente permissaoCliente = new PermissaoCliente();
                int? idCarteira = permissaoCliente.RetornaClienteAssociado(HttpContext.Current.User.Identity.Name);

                if (idCarteira.HasValue)
                {
                    PreencheCamposCarteira(idCarteira.Value);
                }
            }
        }
        catch (Exception exception)
        {
            new Financial.Web.Util.ErrorHandler().HandleArrayIndexError(exception);
        }
    }

    protected bool ChecaUsuarioAcessaCliente(int idCliente)
    {
        ClienteCollection clienteCollection = new ClienteCollection();
        clienteCollection.BuscaClientesComAcesso(HttpContext.Current.User.Identity.Name);

        bool temAcesso = false;
        foreach (Cliente cliente in clienteCollection)
        {
            if (cliente.IdCliente.Value == idCliente)
            {
                temAcesso = true;
                break;
            }
        }

        return temAcesso;
    }

    protected void PreencheCamposCarteira(int idCarteira)
    {
        Cliente cliente = new Cliente();

        List<esQueryItem> campos = new List<esQueryItem>();
        campos.Add(cliente.Query.Apelido);
        cliente.LoadByPrimaryKey(campos, idCarteira);
        string apelido = cliente.Apelido;

        btnEditCodigo.Text = Convert.ToString(idCarteira);
        textNome.Text = apelido;

    }


    protected void ASPxCallback1_Callback(object source, DevExpress.Web.CallbackEventArgs e) {
        string nome = "";
        if (!String.IsNullOrEmpty(e.Parameter) && e.Parameter != "null") {
            int idCarteira = Convert.ToInt32(e.Parameter);
            Carteira carteira = new Carteira();
            carteira.LoadByPrimaryKey(idCarteira);

            if (carteira.str.Apelido != "") {
                if (carteira.IsAtivo) {
                    //O permissionamento da carteira é dado direto no cliente vinculado à carteira
                    PermissaoCliente permissaoCliente = new PermissaoCliente();
                    if (permissaoCliente.RetornaAcessoCliente(idCarteira, HttpContext.Current.User.Identity.Name)) 
                    {
                        nome = carteira.str.Apelido;
                    }
                    else {
                        nome = "no_access";
                    }
                }
                else {
                    nome = "no_active";
                }
            }
        }
        e.Result = nome;
    }

    protected void ASPxCallback2_Callback(object source, DevExpress.Web.CallbackEventArgs e) {
        string nome = "";
        if (!String.IsNullOrEmpty(e.Parameter) && e.Parameter != "null") {
            int idCliente = Convert.ToInt32(e.Parameter);
            Cliente cliente = new Cliente();
            cliente.LoadByPrimaryKey(idCliente);

            if (cliente.str.Apelido != "") {
                if (cliente.IsAtivo) {
                    //O permissionamento da cliente é dado direto no cliente vinculado à cliente
                    PermissaoCliente permissaoCliente = new PermissaoCliente();
                    if (permissaoCliente.RetornaAcessoClienteComControle(idCliente, HttpContext.Current.User.Identity.Name)) 
                    {                        
                        DateTime dataDia = cliente.DataDia.Value;
                        nome = cliente.str.Apelido + "|" + dataDia.ToShortDateString() + "|" + Thread.CurrentThread.CurrentCulture.Name;
                    }
                    else {
                        nome = "no_access";
                    }
                }
                else {
                    nome = "no_active";
                }
            }
        }
        e.Result = nome;
    }
   
    protected void EsDSCarteira_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e) {
        CarteiraCollection carteiraCollection = new CarteiraCollection();
        carteiraCollection.BuscaCarteirasComAcessoTodas(HttpContext.Current.User.Identity.Name);

        e.Collection = carteiraCollection;
    }

    protected void EsDSCliente_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e) {
        ClienteCollection clienteCollection = new ClienteCollection();
        clienteCollection.BuscaClientesComAcesso(HttpContext.Current.User.Identity.Name);

        e.Collection = clienteCollection;
    }

    protected void btnVisualiza_Click(object sender, EventArgs e) {
        this.TrataErros();
        this.SelecionaRelatorio(VisaoRelatorio.visaoHTML);
    }

    protected void btnPDF_Click(object sender, EventArgs e) {
        this.TrataErros();
        this.SelecionaRelatorio(VisaoRelatorio.visaoPDF);
    }

    protected void btnExcel_Click(object sender, EventArgs e) {
        this.TrataErros();
        this.SelecionaRelatorio(VisaoRelatorio.visaoExcel);
    }

    /// <summary>
    /// Trata os Erros de Relatorio
    /// </summary>
    private void TrataErros() {              
        if (String.IsNullOrEmpty(this.textData.Text)) {
            throw new Exception("Campos com * são obrigatórios!");
        }

        if (btnEditCodigoCliente.Text != "" && Utilitario.IsDate(textData.Text)) {
            DateTime dataReport = Convert.ToDateTime(textData.Text);
            Cliente cliente = new Cliente();
            cliente.LoadByPrimaryKey(Convert.ToInt32(btnEditCodigoCliente.Text));
            DateTime dataImplantacao = cliente.DataImplantacao.Value;
            DateTime dataDia = cliente.DataDia.Value;

            if (dataReport < dataImplantacao) {
                throw new Exception("Data passada anterior à data de implantação do Cliente: " + dataImplantacao.ToString("d"));
            }

            if (dataReport > dataDia) {
                throw new Exception("Data passada posterior à data atual do Cliente: " + dataDia.ToString("d"));
            }
        }
    }

    /// <summary>
    /// Seleciona o Relatorio a ser executado
    /// </summary>
    /// <param name="visao">PDF, Excel ou Report</param>
    private void SelecionaRelatorio(string visao) {
        Response.BufferOutput = true;
        //
        // Salva os parametros na session
        Session["textData"] = this.textData.Text;
        Session["btnEditCodigo"] = this.btnEditCodigo.Text; // Carteira
        Session["btnEditCodigoCliente"] = this.btnEditCodigoCliente.Text;
        Session["dropOpcoesCampos"] = this.dropOpcoesCampos.SelectedItem.Value;
        Session["dropTipoRelatorio"] = this.dropTipoRelatorio.SelectedItem.Value;
        Session["dropTipoGrupamento"] = this.dropTipoGrupamento.SelectedItem.Value;
        Session["dropValorAplicado"] = this.dropValorAplicado.SelectedItem.Value;
        Session["dropLingua"] = this.dropLingua.SelectedItem.Value;
        //
        Session["chkRentabilidade"] = this.chkRentabilidade.Checked;
        //
        //Response.Redirect("~/Relatorios/Fundo/ReportSaldoAplicacaoFundo.aspx?Visao=" + visao);
        //
        string url = "~/Relatorios/Fundo/ReportSaldoAplicacaoFundo.aspx?Visao=" + visao;
        string redirectURL = Page.ResolveClientUrl(url);

        string script = "window.location = '" + redirectURL + "';";
        ScriptManager.RegisterStartupScript(this, typeof(Page), "RedirectTo", script, true);
    }
}