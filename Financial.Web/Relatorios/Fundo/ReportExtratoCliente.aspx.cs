using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Financial.Common;
using EntitySpaces.Interfaces;
using System.Collections.Generic;
using Financial.Relatorio;
using System.IO;
using System.Globalization;
using DevExpress.XtraReports.UI;
using Financial.Web.Common;

public partial class _ReportExtratoCliente : BasePage {

    new protected void Page_Load(object sender, EventArgs e) {
        try
        {
            string visao = Request.QueryString["Visao"].ToString();

            // Obrigatorio
            int idCarteira;
            DateTime dataInicio;
            DateTime dataFim;
            int idIndice;
            bool mostraCaixa;
            bool explodeFundos;

            string btnEditCodigo = (string)Session["btnEditCodigo"];
            
            if (Int32.TryParse(btnEditCodigo, out idCarteira))
            {
                //Usar session
                dataInicio = Convert.ToDateTime(Session["textDataInicio"]);
                dataFim = Convert.ToDateTime(Session["textDataFim"]);
                idIndice = Convert.ToInt32(Session["dropIndice"]);
                mostraCaixa = Convert.ToBoolean(Session["chkMostraCaixa"]);
                explodeFundos = Convert.ToBoolean(Session["chkExplodeFundos"]);
            }
            else
            {
                //Usar Query Strings
                idCarteira = Convert.ToInt32(Request["IdCarteira"]);
                dataInicio = Convert.ToDateTime(Request["DataInicio"]);
                dataFim = Convert.ToDateTime(Request["DataFim"]);
                idIndice = Convert.ToInt32(Request["IdIndice"]);
                mostraCaixa = Convert.ToBoolean(Request["MostraCaixa"]);
                explodeFundos = Convert.ToBoolean(Request["chkExplodeFundos"]);
            }

            if (visao == "Report")
            {
                ReportViewer1.Report = new ReportExtratoCliente(idCarteira, idIndice, dataInicio, dataFim, mostraCaixa, explodeFundos);
            }
            else if (visao == "PDF")
            {
                #region
                ReportExtratoCliente report = new ReportExtratoCliente(idCarteira, idIndice, dataInicio, dataFim, mostraCaixa, explodeFundos);

                MemoryStream ms = new MemoryStream();
                report.ExportToPdf(ms);
                ms.Seek(0, SeekOrigin.Begin);
                Response.ClearContent();
                Response.ClearHeaders();
                Response.Buffer = true;
                Response.Cache.SetCacheability(HttpCacheability.Private);
                Response.ContentType = "application/pdf";
                Response.AddHeader("Content-Length", ms.Length.ToString(CultureInfo.CurrentCulture));
                Response.AddHeader("Content-Disposition", String.Format("attachment;filename=ReportExtratoCliente.pdf"));

                Response.BinaryWrite(ms.ToArray());
                ms.Close();
                HttpContext.Current.ApplicationInstance.CompleteRequest();
                Response.End();
                #endregion
            }
            else if (visao == "Excel")
            {
                #region Excel
                ReportExtratoCliente report = new ReportExtratoCliente(idCarteira, idIndice, dataInicio, dataFim, mostraCaixa, explodeFundos);

                MemoryStream ms = new MemoryStream();
                report.ExportToXls(ms);
                ms.Seek(0, SeekOrigin.Begin);
                Response.ClearContent();
                Response.ClearHeaders();
                Response.Buffer = true;
                Response.Cache.SetCacheability(HttpCacheability.Private);
                Response.ContentType = "application/vnd.ms-excel";
                Response.AddHeader("Content-Length", ms.Length.ToString(CultureInfo.CurrentCulture));
                Response.AddHeader("Content-Disposition", String.Format("attachment;filename=ReportExtratoCliente.xls"));

                Response.BinaryWrite(ms.ToArray());
                ms.Close();
                HttpContext.Current.ApplicationInstance.CompleteRequest();
                Response.End();

                #endregion
           }
        }
        catch (Exception exception)
        {
            new Financial.Web.Util.ErrorHandler().HandleArrayIndexError(exception);
        }  
    }
}