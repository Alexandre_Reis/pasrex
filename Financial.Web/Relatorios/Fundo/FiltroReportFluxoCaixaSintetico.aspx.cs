﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using Financial.Fundo;
using Financial.Investidor;
using System.Collections.Generic;
using Financial.Web.Util;
using Financial.Investidor.Enums;
using Financial.Security;
using log4net;
using Financial.Security.Enums;
using EntitySpaces.Interfaces;
using DevExpress.Web;
using System.Text;
using Financial.Util;
using Financial.Common.Enums;
using Financial.Web.Common;

public partial class FiltroReportFluxoCaixaSintetico : FiltroReportBasePage {
    private static readonly ILog log = LogManager.GetLogger(typeof(FiltroReportFluxoCaixaSintetico));

    private enum TipoRelatorio 
    {        
        SinteticoGeral = 1,
        SinteticoCliente = 2
    }

    new protected void Page_Load(object sender, EventArgs e) {
        btnEditCodigo.Focus();

        this.HasPopupCarteira = true;
        base.Page_Load(sender, e);

        TrataTravamentoCampos();
    }

    protected void EsDSCarteira_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e) {
        CarteiraCollection carteiraCollection = new CarteiraCollection();
        carteiraCollection.BuscaCarteirasComAcesso(HttpContext.Current.User.Identity.Name);

        e.Collection = carteiraCollection;
    }

    private void TrataTravamentoCampos()
    {
        try
        {
            //Chegar se foi passado um IdCliente como parametro
            string idClienteParam = Request["idCliente"];
            bool temClienteQueryString = !String.IsNullOrEmpty(idClienteParam);
            int? idClienteQueryString = null;

            if (temClienteQueryString)
            {
                idClienteQueryString = Convert.ToInt32(idClienteParam);

                //Garantir que usuario tem acesso ao cliente
                bool temAcesso = ChecaUsuarioAcessaCliente(idClienteQueryString.Value);

                if (!temAcesso)
                {
                    throw new Exception("Usuário não possui acesso à carteira " + idClienteParam);
                }
            }

            Usuario usuario = new Usuario();
            usuario.BuscaUsuario(HttpContext.Current.User.Identity.Name);
            int tipoTrava = usuario.TipoTrava.Value;

            if (tipoTrava == (int)TipoTravaUsuario.TravaCliente)
            {
                btnEditCodigo.Enabled = false;
            }

            if (temClienteQueryString)
            {
                PreencheCamposCarteira(idClienteQueryString.Value);
            }
            else if (tipoTrava == (int)TipoTravaUsuario.TravaCliente)
            {
                PermissaoCliente permissaoCliente = new PermissaoCliente();
                int? idCarteira = permissaoCliente.RetornaClienteAssociado(HttpContext.Current.User.Identity.Name);

                if (idCarteira.HasValue)
                {
                    PreencheCamposCarteira(idCarteira.Value);
                }
            }
        }
        catch (Exception exception)
        {
            new Financial.Web.Util.ErrorHandler().HandleArrayIndexError(exception);
        }
    }

    protected bool ChecaUsuarioAcessaCliente(int idCliente)
    {
        ClienteCollection clienteCollection = new ClienteCollection();
        clienteCollection.BuscaClientesComAcesso(HttpContext.Current.User.Identity.Name);

        bool temAcesso = false;
        foreach (Cliente cliente in clienteCollection)
        {
            if (cliente.IdCliente.Value == idCliente)
            {
                temAcesso = true;
                break;
            }
        }

        return temAcesso;
    }

    protected void PreencheCamposCarteira(int idCarteira)
    {
        Cliente cliente = new Cliente();

        List<esQueryItem> campos = new List<esQueryItem>();
        campos.Add(cliente.Query.Apelido);
        cliente.LoadByPrimaryKey(campos, idCarteira);
        string apelido = cliente.Apelido;

        btnEditCodigo.Text = Convert.ToString(idCarteira);
        textNome.Text = apelido;

    }

   
    protected void ASPxCallback1_Callback(object source, DevExpress.Web.CallbackEventArgs e) {
        string nome = "";
        if (!String.IsNullOrEmpty(e.Parameter) && e.Parameter != "null") {
            int idCarteira = Convert.ToInt32(e.Parameter);
            Carteira carteira = new Carteira();
            carteira.LoadByPrimaryKey(idCarteira);

            if (carteira.str.Apelido != "") {
                if (carteira.IsAtivo) {
                    //O permissionamento da carteira é dado direto no cliente vinculado à carteira
                    PermissaoCliente permissaoCliente = new PermissaoCliente();
                    if (permissaoCliente.RetornaAcessoClienteComControle(idCarteira, HttpContext.Current.User.Identity.Name)) {
                        nome = carteira.str.Apelido;
                    }
                    else {
                        nome = "no_access";
                    }
                }
                else {
                    nome = "no_active";
                }
            }
        }
        e.Result = nome;
    }

    protected void btnVisualiza_Click(object sender, EventArgs e) {
        this.TrataErros();
        this.SelecionaRelatorio(VisaoRelatorio.visaoHTML);
    }

    protected void btnPDF_Click(object sender, EventArgs e) {
        this.TrataErros();
        this.SelecionaRelatorio(VisaoRelatorio.visaoPDF);
    }

    protected void btnExcel_Click(object sender, EventArgs e) {
        this.TrataErros();
        this.SelecionaRelatorio(VisaoRelatorio.visaoExcel);
    }

    /// <summary>
    /// Trata os Erros de Relatorio
    /// </summary>
    private void TrataErros() {

        #region Campos obrigatórios
        List<Control> controles = new List<Control>(new Control[] { this.textData, this.dropTipoRelatorio });

        if (base.TestaObrigatorio(controles) != "") {
            throw new Exception("Campos com * são obrigatórios!");
        }
        #endregion

        if ( Convert.ToInt32(this.dropTipoRelatorio.SelectedItem.Value) == (int)TipoRelatorio.SinteticoCliente) {
            if (String.IsNullOrEmpty(this.btnEditCodigo.Text)) {
                throw new Exception("Escolha uma Carteira");
            }
        }

        if (textData.Text != "" && Utilitario.IsDate(textData.Text)) 
        {
            if (this.btnEditCodigo.Text != "")
            {
                Cliente cliente = new Cliente();
                cliente.LoadByPrimaryKey(Convert.ToInt32(btnEditCodigo.Text));

                if (cliente.IdMoeda == (int)ListaMoedaFixo.Real)
                {
                    if (!Calendario.IsDiaUtil(Convert.ToDateTime(textData.Text), LocalFeriadoFixo.Brasil, TipoFeriado.Brasil))
                    {
                        throw new Exception("Dia não útil");
                    }
                }
                else
                {
                    if (!Calendario.IsDiaUtil(Convert.ToDateTime(textData.Text), LocalFeriadoFixo.NovaYork, TipoFeriado.Outros))
                    {
                        throw new Exception("Dia não útil");
                    }
                }

                DateTime dataReport = Convert.ToDateTime(textData.Text);
                
                DateTime dataImplantacao = cliente.DataImplantacao.Value;
                DateTime dataDia = cliente.DataDia.Value;

                if (dataReport < dataImplantacao) {
                    throw new Exception("Data passada Anterior à Data de Implantação: " + dataImplantacao.ToString("d"));
                }
                else if (dataReport > dataDia) {
                    throw new Exception("Data Passada Posterior à Data Atual da Carteira: " + dataDia.ToString("d"));
                }
            }
        }
    }

    /// <summary>
    /// Seleciona o Relatorio a ser Executado
    /// </summary>
    /// <param name="visao">PDF, Excel ou Report</param>
    private void SelecionaRelatorio(string visao) {
        Response.BufferOutput = true;
        //
        // Salva os parametros na session
        Session["textData"] = this.textData.Text;
        Session["btnEditCodigo"] = this.btnEditCodigo.Text;
        Session["tipoRelatorio"] = this.dropTipoRelatorio.SelectedItem.Value;
        Session["chkVencimentoTitulos"] = this.chkVencimentoTitulos.Checked;
        Session["chkResgatesNaoCotizados"] = this.chkResgatesNaoCotizados.Checked;
        Session["chkLiquidezBolsa"] = this.chkLiquidezBolsa.Checked;
        Session["chkLiquidezFundos"] = this.chkLiquidezFundos.Checked;

        if (this.dropTipoCliente.SelectedIndex > -1)
        {
            Session["dropTipoCliente"] = this.dropTipoCliente.SelectedItem.Value;
        }

        //Response.Redirect("~/Relatorios/Fundo/ReportFluxoCaixaSintetico.aspx?Visao=" + visao);


        string url = "~/Relatorios/Fundo/ReportFluxoCaixaSintetico.aspx?Visao=" + visao;
        string redirectURL = Page.ResolveClientUrl(url);

        string script = "window.location = '" + redirectURL + "';";
        ScriptManager.RegisterStartupScript(this, typeof(Page), "RedirectTo", script, true);
    }
}