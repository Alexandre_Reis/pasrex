﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Financial.Fundo;
using Financial.Investidor;
using System.Collections.Generic;
using Financial.Web.Util;
using Financial.Investidor.Enums;
using Financial.Security;
using log4net;
using Financial.Security.Enums;
using EntitySpaces.Interfaces;
using DevExpress.Web;
using System.Text;
using Financial.Util;
using Financial.Common.Enums;

public partial class FiltroReportLaminaMensal : Financial.Web.Common.FiltroReportBasePage {
    private static readonly ILog log = LogManager.GetLogger(typeof(FiltroReportLaminaMensal));

    //new protected void Page_Load(object sender, EventArgs e) {

    //    btnEditCodigo.Focus();

    //    base.Page_Load(sender, e);

    //    TrataTravamentoCampos();
    //}

    //private void TrataTravamentoCampos() {
    //    Usuario usuario = new Usuario();
    //    usuario.BuscaUsuario(HttpContext.Current.User.Identity.Name);
    //    int tipoTrava = usuario.TipoTrava.Value;

    //    if (tipoTrava == (int)TipoTravaUsuario.TravaCliente) {
    //        btnEditCodigo.Enabled = false;

    //        PermissaoCliente permissaoCliente = new PermissaoCliente();
    //        int? IdCarteira = permissaoCliente.RetornaClienteAssociado(HttpContext.Current.User.Identity.Name);

    //        if (IdCarteira.HasValue) {
    //            Cliente cliente = new Cliente();

    //            List<esQueryItem> campos = new List<esQueryItem>();
    //            campos.Add(cliente.Query.Apelido);
    //            cliente.LoadByPrimaryKey(campos, IdCarteira.Value);
    //            string apelido = cliente.Apelido;

    //            btnEditCodigo.Text = Convert.ToString(IdCarteira.Value);
    //            textNome.Text = apelido;
    //        }
    //    }
    //}

    //protected void callbackErro_Callback(object source, DevExpress.Web.CallbackEventArgs e) {
    //    e.Result = "";
    //    if (textData.Text != "" && btnEditCodigo.Text != "" && Utilitario.IsDate(textData.Text)) {
    //        //
    //        DateTime dataReport = Convert.ToDateTime(textData.Text);
    //        //
    //        if (!Calendario.IsDiaUtil(dataReport, LocalFeriadoFixo.Brasil, TipoFeriado.Brasil)) {
    //            e.Result = "Dia não útil";
    //            return;
    //        }

    //        Carteira carteira = new Carteira();
    //        carteira.LoadByPrimaryKey(Convert.ToInt32(btnEditCodigo.Text));
    //        DateTime dataInicioCota = carteira.DataInicioCota.Value;
    //        //
    //        if (DateTime.Compare(dataReport, dataInicioCota) < 0) {
    //            e.Result = "Data Início da Cota para a Carteira " + btnEditCodigo.Text + ":  " + dataInicioCota.ToString("d");
    //            return;
    //        }

    //        //Cliente cliente = new Cliente();
    //        //cliente.LoadByPrimaryKey(Convert.ToInt32(btnEditCodigo.Text));
    //        //DateTime dataImplantacao = cliente.DataImplantacao.Value;
    //        //DateTime dataDia = cliente.DataDia.Value;

    //        //if (DateTime.Compare(dataReport, dataImplantacao) < 0) {
    //        //    e.Result = "Data passada anterior à data de implantação da Carteira: " + dataImplantacao.ToString("d");
    //        //    return;
    //        //}

    //        //if (DateTime.Compare(dataReport, dataDia) > 0) {
    //        //    e.Result = "Data passada posterior à data atual da Carteira: " + dataDia.ToString("d");
    //        //    return;
    //        //}
    //    }
    //}

    //protected void ASPxCallback1_Callback(object source, DevExpress.Web.CallbackEventArgs e) {
    //    string nome = "";
    //    if (!String.IsNullOrEmpty(e.Parameter) && e.Parameter != "null") {
    //        int idCarteira = Convert.ToInt32(e.Parameter);
    //        Carteira carteira = new Carteira();
    //        carteira.LoadByPrimaryKey(idCarteira);

    //        if (carteira.str.Apelido != "") {
    //            if (carteira.IsAtivo) {
    //                //O permissionamento da carteira é dado direto no cliente vinculado à carteira
    //                PermissaoCliente permissaoCliente = new PermissaoCliente();
    //                if (permissaoCliente.RetornaAcessoCliente(idCarteira, HttpContext.Current.User.Identity.Name)) {
    //                    nome = carteira.str.Apelido;
    //                }
    //                else {
    //                    nome = "no_access";
    //                }
    //            }
    //            else {
    //                nome = "no_active";
    //            }
    //        }
    //    }
    //    e.Result = nome;
    //}

    //new protected void gridCarteira_CustomDataCallback(object sender, DevExpress.Web.ASPxGridViewCustomDataCallbackEventArgs e) {
    //    e.Result = gridCarteira.GetRowValues(Convert.ToInt32(e.Parameters), "IdCarteira");
    //}

    //protected void EsDSCarteira_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e) {
    //    CarteiraCollection carteiraCollection = new CarteiraCollection();
    //    carteiraCollection.BuscaCarteirasComAcesso(HttpContext.Current.User.Identity.Name);

    //    e.Collection = carteiraCollection;
    //}

    //new protected void gridCarteira_HtmlRowCreated(object sender, DevExpress.Web.ASPxGridViewTableRowEventArgs e) {
    //    e.Row.Attributes.Add("onmouseover", "backgroundColor = this.style.backgroundColor; this.style.backgroundColor='#E0E9EB';");
    //    e.Row.Attributes.Add("onmouseout", "this.style.backgroundColor=backgroundColor;");
    //}

    //protected void btnVisualiza_Click(object sender, EventArgs e) {
    //    this.SelecionaRelatorio(VisaoRelatorio.visaoHTML);
    //}

    //protected void btnPDF_Click(object sender, EventArgs e) {
    //    this.SelecionaRelatorio(VisaoRelatorio.visaoPDF);
    //}

    //protected void btnExcel_Click(object sender, EventArgs e) {
    //    this.SelecionaRelatorio(VisaoRelatorio.visaoExcel);
    //}

    ///// <summary>
    ///// Seleciona o relatorio a ser executado
    ///// </summary>
    ///// <param name="visao">PDF, Excel ou Report</param>
    //private void SelecionaRelatorio(string visao) {
    //    Response.BufferOutput = true;
    //    //
    //    // Salva os parametros na session
    //    Session["textData"] = this.textData.Text;
    //    Session["btnEditCodigo"] = this.btnEditCodigo.Text;

    //    Response.Redirect("~/Relatorios/Fundo/ReportLaminaMensal.aspx?Visao=" + visao);
    //}
}
