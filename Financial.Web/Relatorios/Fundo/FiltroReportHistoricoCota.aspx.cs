﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using Financial.Fundo;
using Financial.Investidor;
using System.Collections.Generic;
using Financial.Web.Util;
using Financial.Investidor.Enums;
using Financial.Security;
using log4net;
using Financial.Security.Enums;
using EntitySpaces.Interfaces;
using DevExpress.Web;
using System.Text;
using Financial.Web.Common;

public partial class FiltroReportHistoricoCota : FiltroReportBasePage {
    private static readonly ILog log = LogManager.GetLogger(typeof(FiltroReportHistoricoCota));

    new protected void Page_Load(object sender, EventArgs e) {
        btnEditCodigo.Focus();

        this.HasPopupCarteira = true;
        base.Page_Load(sender, e);
    }

    protected void EsDSCarteira_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e) {
        CarteiraCollection carteiraCollection = new CarteiraCollection();
        carteiraCollection.BuscaCarteirasComAcessoTodas(HttpContext.Current.User.Identity.Name);

        e.Collection = carteiraCollection;
    }

    protected void ASPxCallback1_Callback(object source, DevExpress.Web.CallbackEventArgs e) {
        string nome = "";
        if (!String.IsNullOrEmpty(e.Parameter) && e.Parameter != "null") {
            int idCarteira = Convert.ToInt32(e.Parameter);
            Carteira carteira = new Carteira();
            carteira.LoadByPrimaryKey(idCarteira);

            if (carteira.str.Apelido != "") {
                if (carteira.IsAtivo) {
                    //O permissionamento da carteira é dado direto no cliente vinculado à carteira
                    PermissaoCliente permissaoCliente = new PermissaoCliente();
                    nome = permissaoCliente.RetornaAcessoCliente(idCarteira, HttpContext.Current.User.Identity.Name)
                            ? carteira.str.Apelido : "no_access";                    
                }
                else {
                    nome = "no_active";
                }
            }
        }
        e.Result = nome;
    }

    protected void btnVisualiza_Click(object sender, EventArgs e) {
        this.TrataErros();
        this.SelecionaRelatorio(VisaoRelatorio.visaoHTML);
    }

    protected void btnPDF_Click(object sender, EventArgs e) {
        this.TrataErros();
        this.SelecionaRelatorio(VisaoRelatorio.visaoPDF);
    }

    protected void btnExcel_Click(object sender, EventArgs e) {
        this.TrataErros();
        this.SelecionaRelatorio(VisaoRelatorio.visaoExcel);
    }

    /// <summary>
    /// Trata os Erros de Relatorio
    /// </summary>
    private void TrataErros() {

        #region Campos obrigatórios
        List<Control> controles = new List<Control>(new Control[] { 
    	    this.btnEditCodigo, this.textDataInicio, this.textDataFim });

        if (base.TestaObrigatorio(controles) != "") {
            throw new Exception("Campos com * são obrigatórios!");
        }
        #endregion
    }

    /// <summary>
    /// Seleciona o relatorio a ser executado
    /// Dependendo do Tipo do Relatorio envia para ReportHistoricoCota ou ReportHistoricoCotaSintetico
    /// </summary>
    /// <param name="visao">PDF, Excel ou Report</param>
    private void SelecionaRelatorio(string visao) {
        Response.BufferOutput = true;
        //
        string tipoRelatorio = this.dropTipoRelatorio.SelectedValue;
        // Salva os parametros na session
        Session["textDataInicio"] = this.textDataInicio.Text;
        Session["textDataFim"] = this.textDataFim.Text;
        Session["btnEditCodigo"] = this.btnEditCodigo.Text;

        if (tipoRelatorio == "Analitico") {
            //Response.Redirect("~/Relatorios/Fundo/ReportHistoricoCota.aspx?Visao=" + visao);

            string url = "~/Relatorios/Fundo/ReportHistoricoCota.aspx?Visao=" + visao;
            string redirectURL = Page.ResolveClientUrl(url);

            string script = "window.location = '" + redirectURL + "';";
            ScriptManager.RegisterStartupScript(this, typeof(Page), "RedirectTo", script, true);

        }
        else if (tipoRelatorio == "Consolidado") {

            //Response.Redirect("~/Relatorios/Fundo/ReportHistoricoCotaSintetico.aspx?Visao=" + visao);

            string url = "~/Relatorios/Fundo/ReportHistoricoCotaSintetico.aspx?Visao=" + visao;
            string redirectURL = Page.ResolveClientUrl(url);

            string script = "window.location = '" + redirectURL + "';";
            ScriptManager.RegisterStartupScript(this, typeof(Page), "RedirectTo", script, true);

        }
    }
}