using Financial.Web.Common;
using System;
using System.IO;
using System.Web;
using System.Globalization;
using Financial.Relatorio;
using Financial.Fundo;
using Financial.Common.Enums;

public partial class _ReportAnaliseResultados : BasePage 
{
    private static class VisaoRelatorio 
    {
        public static string visaoHTML = "Report";
        public static string visaoPDF = "PDF";
        public static string visaoExcel = "Excel";
    }

    new protected void Page_Load(object sender, EventArgs e)
    {
        string visao = Request.QueryString["Visao"].ToString();

        DateTime dataInicio = Convert.ToDateTime(Session["textDataInicio"]);
        DateTime dataFim = Convert.ToDateTime(Session["textDataFim"]);
        int idCarteira = Convert.ToInt32(Session["btnEditCodigo"]);

        if (visao == VisaoRelatorio.visaoHTML)
        {
            ReportViewer1.Report = new ReportAnaliseResultados(idCarteira, dataFim, dataInicio);
        }
        else if (visao == VisaoRelatorio.visaoPDF)
        {
            #region PDF
            ReportAnaliseResultados report = new ReportAnaliseResultados(idCarteira, dataFim, dataInicio);

            MemoryStream ms = new MemoryStream();
            report.ExportToPdf(ms);
            ms.Seek(0, SeekOrigin.Begin);
            Response.ClearContent();
            Response.ClearHeaders();
            Response.Buffer = true;
            Response.Cache.SetCacheability(HttpCacheability.Private);
            Response.ContentType = "application/pdf";
            Response.AddHeader("Content-Length", ms.Length.ToString(CultureInfo.CurrentCulture));
            Response.AddHeader("Content-Disposition", String.Format("attachment;filename=ReportAnaliseResultados.pdf"));

            Response.BinaryWrite(ms.ToArray());
            ms.Close();
            HttpContext.Current.ApplicationInstance.CompleteRequest();
            Response.End();
            #endregion
        }
        else if (visao == VisaoRelatorio.visaoExcel)
        {
            #region Excel
            ReportAnaliseResultados report = new ReportAnaliseResultados(idCarteira, dataFim, dataInicio);

            MemoryStream ms = new MemoryStream();
            report.ExportToXls(ms);
            ms.Seek(0, SeekOrigin.Begin);
            Response.ClearContent();
            Response.ClearHeaders();
            Response.Buffer = true;
            Response.Cache.SetCacheability(HttpCacheability.Private);
            Response.ContentType = "application/vnd.ms-excel";
            Response.AddHeader("Content-Length", ms.Length.ToString(CultureInfo.CurrentCulture));
            Response.AddHeader("Content-Disposition", String.Format("attachment;filename=ReportAnaliseResultados.xls"));

            Response.BinaryWrite(ms.ToArray());
            ms.Close();
            HttpContext.Current.ApplicationInstance.CompleteRequest();
            Response.End();

            #endregion
        }
    }
}