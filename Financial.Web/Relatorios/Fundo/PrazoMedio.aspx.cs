using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Collections.Generic;
using System.Text;
using DevExpress.Web;
using System.Drawing;
using Financial.Web.Common;
using Financial.Fundo.Enums;
using Financial.Fundo;
using Financial.Investidor;

public partial class CadastrosBasicos_PrazoMedio : CadastroBasePage
{
    new protected void Page_Load(object sender, EventArgs e)
    {
        this.HasFiltro = true;
        base.Page_Load(sender, e);
    }

    new protected void gridCadastro_PreRender(object sender, EventArgs e)
    {
        base.gridCadastro_PreRender(sender, e);
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void EsDSPrazoMedio_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        PrazoMedioCollection coll = new PrazoMedioCollection();

        PrazoMedioQuery prazoMedioQuery = new PrazoMedioQuery("prazoMedio");
        ClienteQuery clienteQuery = new ClienteQuery("cliente");

        prazoMedioQuery.Select(prazoMedioQuery,
                               clienteQuery.Nome);
        prazoMedioQuery.InnerJoin(clienteQuery).On(prazoMedioQuery.IdCliente.Equal(clienteQuery.IdCliente));

        ASPxDateEdit textDataInicio = popupFiltro.FindControl("textDataInicio") as ASPxDateEdit;
        ASPxDateEdit textDataFim = popupFiltro.FindControl("textDataFim") as ASPxDateEdit;

        if (textDataInicio.Text != "")
        {
            prazoMedioQuery.Where(prazoMedioQuery.DataAtual.GreaterThanOrEqual(textDataInicio.Text));
        }

        if (textDataFim.Text != "")
        {
            prazoMedioQuery.Where(prazoMedioQuery.DataAtual.LessThanOrEqual(textDataFim.Text));
        }

        prazoMedioQuery.OrderBy(prazoMedioQuery.DataAtual.Descending, prazoMedioQuery.IdCliente.Ascending, prazoMedioQuery.MercadoAtivo.Ascending);

        coll.Load(prazoMedioQuery);

        e.Collection = coll;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_CustomCallback(object sender, DevExpress.Web.ASPxGridViewCustomCallbackEventArgs e)
    {
        gridCadastro.DataBind();
        gridCadastro.Selection.UnselectAll();
        gridCadastro.CancelEdit();
    }

    protected void gridCadastro_Load(object sender, EventArgs e)
    {
        GridViewDataComboBoxColumn mercadoAtivo = gridCadastro.Columns["MercadoAtivo"] as GridViewDataComboBoxColumn;
        if (mercadoAtivo != null)
        {
            mercadoAtivo.PropertiesComboBox.Items.Clear();
            foreach (int i in Enum.GetValues(typeof(TipoAtivoAuxiliar)))
            {
                mercadoAtivo.PropertiesComboBox.Items.Add(TipoAtivoAuxiliarDescricao.RetornaStringValue(i), i);
            }
        }
    }

    protected void gridCadastro_HtmlRowPrepared(object sender, DevExpress.Web.ASPxGridViewTableRowEventArgs e)
    {
        if (e.GetValue("MercadoAtivo") != null)
        {
            int tipoAtivo = Convert.ToInt32(e.GetValue("MercadoAtivo").ToString());
            if (tipoAtivo == (int)TipoAtivoAuxiliar.Fundo)
            {
                e.Row.Font.Bold = true;
            }
        }
    }
}