using System;
using Financial.Relatorio;
using System.IO;
using System.Web;
using System.Globalization;
using Financial.Web.Common;
public partial class _ReportDiarioCaixaMercado : Financial.Web.Common.BasePage {
    /* Como Usar:
       DateTime dataReferencia = new DateTime(2007, 1, 1);
       //DateTime dataReferencia = new DateTime(2010, 1, 1);
       //        
       ReportViewer1.Report = new ReportDiarioCaixaMercado(dataReferencia);
     */
    new protected void Page_Load(object sender, EventArgs e) {
        string visao = Request.QueryString["Visao"].ToString();
        //
        //Response.Write(Request.Form["textData"].ToString());

        DateTime dataReferencia = new DateTime();

        // Obrigatorio
        dataReferencia = Convert.ToDateTime(Session["textData"]);
        
        if (visao == "Report") {
            ReportViewer1.Report = new ReportDiarioCaixaMercado(dataReferencia, HttpContext.Current.User.Identity.Name);
        }
        else if (visao == "PDF") {
            #region PDF
            ReportDiarioCaixaMercado report = new ReportDiarioCaixaMercado(dataReferencia, HttpContext.Current.User.Identity.Name);

            MemoryStream ms = new MemoryStream();
            report.ExportToPdf(ms);
            ms.Seek(0, SeekOrigin.Begin);
            Response.ClearContent();
            Response.ClearHeaders();
            Response.Buffer = true;
            Response.Cache.SetCacheability(HttpCacheability.Private);
            Response.ContentType = "application/pdf";
            Response.AddHeader("Content-Length", ms.Length.ToString(CultureInfo.CurrentCulture));
            Response.AddHeader("Content-Disposition", String.Format("attachment;filename=ReportDiarioCaixaMercado.pdf"));

            Response.BinaryWrite(ms.ToArray());
            ms.Close();
            HttpContext.Current.ApplicationInstance.CompleteRequest();
            Response.End();
            #endregion
        }
        else if (visao == "Excel") {
            #region Excel
            ReportDiarioCaixaMercado report = new ReportDiarioCaixaMercado(dataReferencia, HttpContext.Current.User.Identity.Name);

            MemoryStream ms = new MemoryStream();
            report.ExportToXls(ms);
            ms.Seek(0, SeekOrigin.Begin);
            Response.ClearContent();
            Response.ClearHeaders();
            Response.Buffer = true;
            Response.Cache.SetCacheability(HttpCacheability.Private);
            Response.ContentType = "application/vnd.ms-excel";
            Response.AddHeader("Content-Length", ms.Length.ToString(CultureInfo.CurrentCulture));
            Response.AddHeader("Content-Disposition", String.Format("attachment;filename=ReportDiarioCaixaMercado.xls"));

            Response.BinaryWrite(ms.ToArray());
            ms.Close();
            HttpContext.Current.ApplicationInstance.CompleteRequest();
            Response.End();

            #endregion
        }
            
    }
}
