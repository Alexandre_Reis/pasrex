using System;
using Financial.Relatorio;
using System.IO;
using System.Web;
using System.Globalization;
using Financial.Web.Common;

public partial class _ReportDemonstrativoProvisoes : Financial.Web.Common.BasePage {
    /* Como usar:
        DateTime dataInicio = new DateTime(2000, 1, 1);
        DateTime dataFim = new DateTime(2010, 1, 1);        
        int idTabela = 1;
        //int idTabela = 32;

        //        
        ReportViewer1.Report = new ReportDemonstrativoProvisoes(dataInicio, dataFim, idTabela);
     */
    new protected void Page_Load(object sender, EventArgs e) {        
        string visao = Request.QueryString["Visao"].ToString();
        //
        //Response.Write(Request.Form["textDataInicio"].ToString());
        //Response.Write("<br>" +Request.Form["textDataFim"].ToString());
        //Response.Write("<br>" + Request.Form["hiddenIdTabela"].ToString());
        //Response.Write("<br>" + Request.Form["hiddenIdCarteira"].ToString());

        DateTime dataInicio = new DateTime();
        DateTime dataFim = new DateTime();
        int? idTabela = null;
        
        // Obrigatorio
        dataInicio = Convert.ToDateTime(Session["textDataInicio"]);
        dataFim = Convert.ToDateTime(Session["textDataFim"]);
        idTabela = Convert.ToInt32(Session["hiddenIdTabela"]);
        int idCarteira = Convert.ToInt32(Session["hiddenIdCarteira"]);

        if (visao == "Report") {
            ReportViewer1.Report = new ReportDemonstrativoProvisoes(dataInicio, dataFim, idTabela.Value, idCarteira);
        }
        else if (visao == "PDF") {
            #region PDF
            ReportDemonstrativoProvisoes report = new ReportDemonstrativoProvisoes(dataInicio, dataFim, idTabela.Value, idCarteira);

            MemoryStream ms = new MemoryStream();
            report.ExportToPdf(ms);
            ms.Seek(0, SeekOrigin.Begin);
            Response.ClearContent();
            Response.ClearHeaders();
            Response.Buffer = true;
            Response.Cache.SetCacheability(HttpCacheability.Private);
            Response.ContentType = "application/pdf";
            Response.AddHeader("Content-Length", ms.Length.ToString(CultureInfo.CurrentCulture));
            Response.AddHeader("Content-Disposition", String.Format("attachment;filename=ReportDemonstrativoProvisoes.pdf"));

            Response.BinaryWrite(ms.ToArray());
            ms.Close();
            HttpContext.Current.ApplicationInstance.CompleteRequest();
            Response.End();
            #endregion
        }
        else if (visao == "Excel") {
            #region Excel
            ReportDemonstrativoProvisoes report = new ReportDemonstrativoProvisoes(dataInicio, dataFim, idTabela.Value, idCarteira);

            MemoryStream ms = new MemoryStream();
            report.ExportToXls(ms);
            ms.Seek(0, SeekOrigin.Begin);
            Response.ClearContent();
            Response.ClearHeaders();
            Response.Buffer = true;
            Response.Cache.SetCacheability(HttpCacheability.Private);
            Response.ContentType = "application/vnd.ms-excel";
            Response.AddHeader("Content-Length", ms.Length.ToString(CultureInfo.CurrentCulture));
            Response.AddHeader("Content-Disposition", String.Format("attachment;filename=ReportDemonstrativoProvisoes.xls"));

            Response.BinaryWrite(ms.ToArray());
            ms.Close();
            HttpContext.Current.ApplicationInstance.CompleteRequest();
            Response.End();

            #endregion
        }            
    }
}
