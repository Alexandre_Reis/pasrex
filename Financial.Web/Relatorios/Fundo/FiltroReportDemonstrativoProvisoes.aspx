﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="FiltroReportDemonstrativoProvisoes.aspx.cs" Inherits="FiltroReportDemonstrativoProvisoes" %>

<%@ Register Assembly="System.Web, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"
    Namespace="System.Web.UI.HtmlControls" TagPrefix="cc2" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxnb" %>    
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxcb" %>
<%@ Register Assembly="System.Web, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"
    Namespace="System.Web.UI" TagPrefix="cc1" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxe" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxwgv" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxpc" %>
<%@ Register Assembly="EntitySpaces.Web" Namespace="EntitySpaces.Web" TagPrefix="cc1" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >

<head id="Head1" runat="server">

<link href="~/css/forms.css" rel="stylesheet" type="text/css" />

<script language="JavaScript">
var backgroundColor;
function OnGetData(values) {
    var resultSplit = values.split('|');
    document.getElementById('hiddenIdCarteira').value = resultSplit[1];
    document.getElementById('hiddenIdTabela').value = resultSplit[0];
    popupTabela.HideWindow();
    ASPxCallback1.SendCallback(resultSplit[0]);
    btnEditCodigo.Focus();
}
</script>
    
</head>
<body >
    <form id="form1" runat="server"> 
    
    <dxcb:ASPxCallback ID="ASPxCallback1" runat="server" OnCallback="ASPxCallback1_Callback">
        <ClientSideEvents CallbackComplete="function(s, e) {                        
            if (e.result == '' && document.getElementById('hiddenIdTabela').value != '' 
                                            && document.getElementById('hiddenIdTabela').value != null)
            {                   
                if (document.getElementById('hiddenIdTabela').value != '' 
                                && document.getElementById('hiddenIdTabela').value != null)
                {
                    popupMensagem.ShowWindow();
                }                
                btnEditCodigo.SetValue(''); 
                btnEditCodigo.Focus();                 
            }
            else
            {
                btnEditCodigo.SetValue(e.result);                
            }
        }        
        "/>
    </dxcb:ASPxCallback>
    
    <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="true" EnableScriptLocalization="true"/>
    
    <asp:HiddenField ID="hiddenIdCarteira" runat="server"></asp:HiddenField> 
    
    <dxpc:ASPxPopupControl ID="popupMensagem" PopupElementID="btnEditCodigo" CloseAction="MouseOut" 
            ForeColor="Red" PopupVerticalAlign="Middle" PopupHorizontalAlign="OutsideRight" Left="100"
            PopupAction="None" HeaderText="Tabela não Existe." runat="server">
    </dxpc:ASPxPopupControl>
          
    <dxpc:ASPxPopupControl ID="popupTabela" runat="server" HeaderText="" Width="500px" 
                        ContentStyle-VerticalAlign="Top" EnableClientSideAPI="True"
                        PopupVerticalAlign="Middle" PopupHorizontalAlign="OutsideRight" AllowDragging="True">
        <ContentCollection><dxpc:PopupControlContentControl runat="server">     
        <div>
            <dxwgv:ASPxGridView ID="gridTabela" runat="server" Width="100%"
                    ClientInstanceName="gridTabela"  AutoGenerateColumns="False" 
                    DataSourceID="EsDSTabelaProvisao" KeyFieldName="IdTabela"
                    OnCustomDataCallback="gridTabela_CustomDataCallback" 
                    OnDataBound="gridTabela_DataBound" 
                    OnHtmlRowCreated="gridTabela_HtmlRowCreated">                    
            <Columns>
                <dxwgv:GridViewDataTextColumn FieldName="IdTabela" Visible="false">                
                </dxwgv:GridViewDataTextColumn>
                <dxwgv:GridViewDataTextColumn FieldName="IdCadastro" Visible="false">                                        
                </dxwgv:GridViewDataTextColumn>
                <dxwgv:GridViewDataDateColumn FieldName="DataReferencia" VisibleIndex="0" Width="10%">
                </dxwgv:GridViewDataDateColumn>
                <dxwgv:GridViewDataTextColumn FieldName="IdCarteira" VisibleIndex="1" Width="10%">
                </dxwgv:GridViewDataTextColumn>
                <dxwgv:GridViewDataTextColumn FieldName="Apelido" VisibleIndex="40">
                </dxwgv:GridViewDataTextColumn>
                <dxwgv:GridViewDataTextColumn FieldName="Descricao" VisibleIndex="40">
                </dxwgv:GridViewDataTextColumn>
            </Columns>            
            <Settings ShowFilterRow="True" ShowTitlePanel="True"/>            
            <SettingsBehavior ColumnResizeMode="Disabled" />
            <ClientSideEvents RowDblClick="function(s, e) {
            gridTabela.GetValuesOnCustomCallback(e.visibleIndex, OnGetData);}" Init="function(s, e) {
	        e.cancel = true;
	        }"
	        />
            <SettingsDetail ShowDetailButtons="False" />
            <Styles Cell-Wrap="False" AlternatingRow-CssClass="MyClass">
                <Header ImageSpacing="5px" SortingImageSpacing="5px"/>
            </Styles>
            <Images></Images>
            <SettingsText EmptyDataRow="0 Registros" Title="Pesquisa Tabela" />
            </dxwgv:ASPxGridView>    
        
        </div>      
        </dxpc:PopupControlContentControl></ContentCollection>
        <ClientSideEvents CloseUp="function(s, e) {gridTabela.ClearFilter(); }" />
    </dxpc:ASPxPopupControl>
    
    <div class="divPanel">
    <table width="100%"><tr><td>
    <div id="container_small">
    
    <div id="header"><asp:Label ID="label1" runat="server" Text="Demonstrativo de Provisões" /></div>
        
    <div id="mainContentSpace">
    
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
    <ContentTemplate>
        
        <div class="reportFilter">
        
        <div style="height:20px"></div>        
        
        <table cellpadding="2" cellspacing="2">
            <tr>
            <td class="td_Label">
                <asp:Label ID="labelTabela" runat="server" CssClass="labelRequired" Text="Tabela:"></asp:Label>
            </td>
            
            <td>
                <asp:Label ID="labelMensagem" runat="server" Text="  (Clique no botão...)" Visible="false"></asp:Label>
                <asp:Label ID="labelProvisao" runat="server" Text="Provisão: " Visible="false"></asp:Label>
                <asp:Label ID="labelDataReferencia" runat="server" Text="Data Referência: " Visible="false"></asp:Label>
                
                <dxe:ASPxButtonEdit ID="btnEditCodigo" runat="server" CssClass="textButtonEdit" 
                                    EnableClientSideAPI="True"
                                    ClientInstanceName="btnEditCodigo" 
                                    ReadOnly="true" Width="600px">
                <Buttons>
                    <dxe:EditButton></dxe:EditButton>                                
                </Buttons>        
                <ClientSideEvents
                         KeyPress="function(s, e) {document.getElementById('hiddenIdTabela').value = '';}" 
                         ButtonClick="function(s, e) {popupTabela.ShowAtElementByID(s.name);}"                          
                />                            
                </dxe:ASPxButtonEdit>                                
                <asp:TextBox ID="hiddenIdTabela" runat="server" CssClass="hiddenField" />             
            </td>                          
            
            <tr>
            <td class="td_Label">                
                <asp:Label ID="labelDataInicio" runat="server" CssClass="labelRequired" Text="Início:"></asp:Label>
            </td>                        
            <td>
              <table cellpadding="1" cellspacing="1">
              <tr>
                <td>
                    <dxe:ASPxDateEdit ID="textDataInicio" runat="server" ClientInstanceName="textDataInicio"/>
                </td>  
                
                <td class="labelCurto">
                <asp:Label ID="labelDataFim" runat="server" CssClass="labelRequired" Text="Fim:"></asp:Label>
                </td>
                
                <td>
                    <dxe:ASPxDateEdit ID="textDataFim" runat="server" ClientInstanceName="textDataFim"/>
                </td>
              </tr>
              </table>          
            </td>
            </tr>            
        </table>     
        
        </div>                                                                                    
    
        <div id="reportLinkButton" class="linkButton linkButtonNoBorder">
           <asp:LinkButton ID="btnVisualiza" runat="server" Font-Overline="false" CssClass="btnReport" OnClick="btnVisualiza_Click"><asp:Literal ID="Literal1" runat="server" Text="Visualizar"/><div></div></asp:LinkButton>
           <asp:LinkButton ID="btnPDF" runat="server" Font-Overline="false" CssClass="btnPdf" OnClick="btnPDF_Click"><asp:Literal ID="Literal2" runat="server" Text="Gerar PDF"/><div></div></asp:LinkButton>
           <asp:LinkButton ID="btnExcel" runat="server" Font-Overline="false"  CssClass="btnExcel" OnClick="btnExcel_Click"><asp:Literal ID="Literal3" runat="server" Text="Gerar Excel"/><div></div></asp:LinkButton>
        </div>
    
    </ContentTemplate>
    </asp:UpdatePanel>
    
    </div>
    
    </div>
    </td></tr></table>
    </div>
    
    <cc1:esDataSource ID="EsDSTabelaProvisao" runat="server" OnesSelect="EsDSTabelaProvisao_esSelect" />
    
   </form>
</body>
</html>