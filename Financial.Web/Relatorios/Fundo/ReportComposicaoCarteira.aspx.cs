using System;
using System.Data;
using Financial.Relatorio;
using System.IO;
using System.Web;
using System.Globalization;
using Financial.Web.Common;
using Financial.Investidor;
using System.Collections.Generic;
using EntitySpaces.Interfaces;
using Financial.Investidor.Enums;
using Financial.Fundo;
using Financial.Fundo.Enums;

public partial class _ReportComposicaoCarteira : BasePage {
    private static class VisaoRelatorio {
        public static string visaoHTML = "Report";
        public static string visaoPDF = "PDF";
        public static string visaoExcel = "Excel";
    }

    private enum RelatorioAExecutar {
        ComposicaoCarteira = 0,
        ComposicaoCarteiraSemResumo = 1
    }

    new protected void Page_Load(object sender, EventArgs e) {
        try
        {
            string visao = Request.QueryString["Visao"].ToString();
            //
            //Response.Write(Request.Form["textData"].ToString());        
            //Response.Write("<br>" + Request.Form["btnEditCodigo"].ToString());
            //Response.Write("<br>" + Request.Form["dropTipoRelatorio"].ToString());

            //Response.Write("<br>" + Session["dropLingua"]);

            DateTime dataReferencia = new DateTime();
            int idCarteira;
            int tipoRelatorioInt;
            int tipoLinguaInt;
            
            //
            ReportComposicaoCarteira.TipoRelatorio tipoRelatorio = ReportComposicaoCarteira.TipoRelatorio.Fechamento;
            ReportComposicaoCarteiraSemResumo.TipoRelatorio tipoRelatorio1 = ReportComposicaoCarteiraSemResumo.TipoRelatorio.Fechamento;
            //
            ReportComposicaoCarteira.TipoLingua tipoLingua = ReportComposicaoCarteira.TipoLingua.Portugues;
            ReportComposicaoCarteiraSemResumo.TipoLingua tipoLingua1 = ReportComposicaoCarteiraSemResumo.TipoLingua.Portugues;

            int? idMoeda = null;

            string btnEditCodigo = (string)Session["btnEditCodigo"];
            bool separaCustodia = Convert.ToString(Session["dropCustodia"]) == "S" ? true : false;

            if (Int32.TryParse(btnEditCodigo, out idCarteira))
            {
                //Usar session
                dataReferencia = Convert.ToDateTime(Session["textData"]);
                tipoRelatorioInt = Convert.ToInt32(Session["dropTipoRelatorio"]);
                tipoLinguaInt = Convert.ToInt32(Session["dropLingua"]);
                if (Session["dropMoeda"] != null)
                {
                    idMoeda = Convert.ToInt32(Session["dropMoeda"]);
                }
            }
            else
            {
                idCarteira = Convert.ToInt32(Request["IdCarteira"]);
                dataReferencia = Convert.ToDateTime(Request["DataReferencia"]);
                tipoLinguaInt = Convert.ToInt32(Request["IdLingua"]);
                idMoeda = Convert.ToInt32(Request["IdMoeda"]);
                Carteira carteira = new Carteira();
                carteira.LoadByPrimaryKey(idCarteira);
                if (carteira.TipoCota == (byte)TipoCotaFundo.Fechamento)
                {
                    tipoRelatorioInt = 1;
                }
                else
                {
                    tipoRelatorioInt = 0;
                }
            }

            
            string fileName = "Carteira_" + idCarteira.ToString() + "_" +
                                  dataReferencia.Day + "-" + dataReferencia.Month + "-" + dataReferencia.Year;

            #region Define qual Relatorio de ComposicaoCarteira Chamar
            Cliente cliente = new Cliente();
            List<esQueryItem> campos = new List<esQueryItem>();
            campos.Add(cliente.Query.TipoControle);
            cliente.LoadByPrimaryKey(campos, idCarteira);

            // Composicao Sem Resumo
            RelatorioAExecutar relatorioAExecutar = cliente.TipoControle == (byte)TipoControleCliente.IRRendaVariavel || 
                                                    cliente.TipoControle == (byte)TipoControleCliente.Carteira
                                                     ? RelatorioAExecutar.ComposicaoCarteiraSemResumo
                                                     : RelatorioAExecutar.ComposicaoCarteira;
            #endregion

            if (relatorioAExecutar == RelatorioAExecutar.ComposicaoCarteira)
            {
                tipoRelatorio = (ReportComposicaoCarteira.TipoRelatorio)tipoRelatorioInt;
                tipoLingua = (ReportComposicaoCarteira.TipoLingua)tipoLinguaInt;
            }
            else
            {
                tipoRelatorio1 = (ReportComposicaoCarteiraSemResumo.TipoRelatorio)tipoRelatorioInt;
                tipoLingua1 = (ReportComposicaoCarteiraSemResumo.TipoLingua)tipoLinguaInt;
            }
            //

            if (visao == VisaoRelatorio.visaoHTML)
            {
                if (relatorioAExecutar == RelatorioAExecutar.ComposicaoCarteira)
                {
                    ReportViewer1.Report = new ReportComposicaoCarteira(idCarteira, dataReferencia, tipoRelatorio, tipoLingua, idMoeda, separaCustodia);
                }
                else
                {
                    ReportViewer1.Report = new ReportComposicaoCarteiraSemResumo(idCarteira, dataReferencia, tipoRelatorio1, tipoLingua1, idMoeda, separaCustodia);
                }
            }
            else if (visao == VisaoRelatorio.visaoPDF)
            {
                #region PDF
                ReportComposicaoCarteira report;
                ReportComposicaoCarteiraSemResumo report1;
                //
                MemoryStream ms = new MemoryStream();
                //
                if (relatorioAExecutar == RelatorioAExecutar.ComposicaoCarteira)
                {
                    report = new ReportComposicaoCarteira(idCarteira, dataReferencia, tipoRelatorio, tipoLingua, idMoeda, separaCustodia);
                    report.ExportToPdf(ms);
                }
                else
                {
                    report1 = new ReportComposicaoCarteiraSemResumo(idCarteira, dataReferencia, tipoRelatorio1, tipoLingua1, idMoeda, separaCustodia);
                    report1.ExportToPdf(ms);
                }

                ms.Seek(0, SeekOrigin.Begin);
                Response.ClearContent();
                Response.ClearHeaders();
                Response.Buffer = true;
                Response.Cache.SetCacheability(HttpCacheability.Private);
                Response.ContentType = "application/pdf";
                Response.AddHeader("Content-Length", ms.Length.ToString(CultureInfo.CurrentCulture));
                Response.AddHeader("Content-Disposition", String.Format("attachment;filename=" + fileName + ".pdf"));

                Response.BinaryWrite(ms.ToArray());
                ms.Close();
                HttpContext.Current.ApplicationInstance.CompleteRequest();
                Response.End();
                #endregion
            }
            else if (visao == VisaoRelatorio.visaoExcel)
            {
                #region Excel
                ReportComposicaoCarteira report;
                ReportComposicaoCarteiraSemResumo report1;
                //
                MemoryStream ms = new MemoryStream();
                //
                if (relatorioAExecutar == RelatorioAExecutar.ComposicaoCarteira)
                {
                    report = new ReportComposicaoCarteira(idCarteira, dataReferencia, tipoRelatorio, tipoLingua, idMoeda, separaCustodia);
                    report.ExportToXls(ms);
                }
                else
                {
                    report1 = new ReportComposicaoCarteiraSemResumo(idCarteira, dataReferencia, tipoRelatorio1, tipoLingua1, idMoeda, separaCustodia);
                    report1.ExportToXls(ms);
                }

                ms.Seek(0, SeekOrigin.Begin);
                Response.ClearContent();
                Response.ClearHeaders();
                Response.Buffer = true;
                Response.Cache.SetCacheability(HttpCacheability.Private);
                Response.ContentType = "application/vnd.ms-excel";
                Response.AddHeader("Content-Length", ms.Length.ToString(CultureInfo.CurrentCulture));
                Response.AddHeader("Content-Disposition", String.Format("attachment;filename=" + fileName + ".xls"));

                Response.BinaryWrite(ms.ToArray());
                ms.Close();
                HttpContext.Current.ApplicationInstance.CompleteRequest();
                Response.End();

                #endregion
            }
        }
        catch (Exception exception)
        {
            new Financial.Web.Util.ErrorHandler().HandleArrayIndexError(exception);
        }
    }

    
}