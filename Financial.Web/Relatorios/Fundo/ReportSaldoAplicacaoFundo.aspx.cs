﻿using System;
using Financial.Relatorio;
using System.IO;
using System.Web;
using System.Globalization;
using Financial.Web.Common;
using Financial.Investidor;

public partial class _ReportSaldoAplicacaoFundo : BasePage {
    private static class VisaoRelatorio {
        public static string visaoHTML = "Report";
        public static string visaoPDF = "PDF";
        public static string visaoExcel = "Excel";
    }

    private enum TipoOpcoesCampo {
        ValorPFee_Participacao = 1,
        Rendimento_PercentualRetorno = 2
    }
   
    new protected void Page_Load(object sender, EventArgs e) {
        string visao = Request.QueryString["Visao"].ToString();

        #region Parametros
        ReportSaldoAplicacaoFundo.TipoRelatorio tipoRelatorio = ReportSaldoAplicacaoFundo.TipoRelatorio.Analitico;
        ReportSaldoAplicacaoFundoView2.TipoRelatorio tipoRelatorioView2 = ReportSaldoAplicacaoFundoView2.TipoRelatorio.Analitico;
        //
        ReportSaldoAplicacaoFundo.TipoGrupamento tipoGrupamento = ReportSaldoAplicacaoFundo.TipoGrupamento.PorNome;
        ReportSaldoAplicacaoFundoView2.TipoGrupamento tipoGrupamentoView2 = ReportSaldoAplicacaoFundoView2.TipoGrupamento.PorNome;
        //
        ReportSaldoAplicacaoFundo.OpcaoValorAplicado opcaoValorAplicado = ReportSaldoAplicacaoFundo.OpcaoValorAplicado.ValorAtualizado;
        ReportSaldoAplicacaoFundoView2.OpcaoValorAplicado opcaoValorAplicadoView2 = ReportSaldoAplicacaoFundoView2.OpcaoValorAplicado.ValorAtualizado;

        ReportSaldoAplicacaoFundo.TipoLingua tipoLingua = ReportSaldoAplicacaoFundo.TipoLingua.Portugues;
        ReportSaldoAplicacaoFundoView2.TipoLingua tipoLinguaView2 = ReportSaldoAplicacaoFundoView2.TipoLingua.Portugues;
        //
        TipoOpcoesCampo tipoOpcoesCampo;

        int? idCarteira = null;
        int? idCliente = null;

        // Obrigatorio
        DateTime data = Convert.ToDateTime(Session["textData"]);

        if (Session["btnEditCodigoCliente"].ToString() != "") { // Cliente
            idCliente = Convert.ToInt32(Session["btnEditCodigoCliente"]);
        }

        if (Session["btnEditCodigo"].ToString() != "") { // Carteira
            idCarteira = Convert.ToInt32(Session["btnEditCodigo"]);
        }

        tipoOpcoesCampo = (TipoOpcoesCampo)Convert.ToInt32(Session["dropOpcoesCampos"]);
        //
        if (tipoOpcoesCampo == TipoOpcoesCampo.ValorPFee_Participacao) {
            tipoRelatorio = (ReportSaldoAplicacaoFundo.TipoRelatorio)Convert.ToInt32(Session["dropTipoRelatorio"]);
            tipoGrupamento = (ReportSaldoAplicacaoFundo.TipoGrupamento)Convert.ToInt32(Session["dropTipoGrupamento"]);
            opcaoValorAplicado = (ReportSaldoAplicacaoFundo.OpcaoValorAplicado)Convert.ToInt32(Session["dropValorAplicado"]);
            tipoLingua = (ReportSaldoAplicacaoFundo.TipoLingua)Convert.ToInt32(Session["dropLingua"]);
        }
        else if (tipoOpcoesCampo == TipoOpcoesCampo.Rendimento_PercentualRetorno) {
            tipoRelatorioView2 = (ReportSaldoAplicacaoFundoView2.TipoRelatorio)Convert.ToInt32(Session["dropTipoRelatorio"]);
            tipoGrupamentoView2 = (ReportSaldoAplicacaoFundoView2.TipoGrupamento)Convert.ToInt32(Session["dropTipoGrupamento"]);
            opcaoValorAplicadoView2 = (ReportSaldoAplicacaoFundoView2.OpcaoValorAplicado)Convert.ToInt32(Session["dropValorAplicado"]);
            tipoLinguaView2 = (ReportSaldoAplicacaoFundoView2.TipoLingua)Convert.ToInt32(Session["dropLingua"]);
        }

        bool chkRentabilidade = Convert.ToBoolean(Session["chkRentabilidade"]);
        #endregion

        ReportMaster r = new ReportMaster();
        string filename = "SaldoAplicacaoFundo";
        //
        if (tipoOpcoesCampo == TipoOpcoesCampo.ValorPFee_Participacao) {
            #region ValorPFee_Participacao

            // Somente 1 Cliente
            if (idCliente.HasValue) {
                #region Apenas 1 Cliente
                ReportSaldoAplicacaoFundo report = new ReportSaldoAplicacaoFundo(idCarteira, idCliente.Value, data, tipoRelatorio,
                                                    tipoGrupamento, opcaoValorAplicado, chkRentabilidade, HttpContext.Current.User.Identity.Name, tipoLingua);

                report.CreateDocument();
                //
                r.PrintingSystem.Pages.AddRange(report.PrintingSystem.Pages); // Adiciona Relatorio

                filename += idCliente.Value.ToString() + "_" + data.Day + "-" + data.Month + "-" + data.Year;
                #endregion
            }
            else { // Varios Clientes
                #region Varios Clientes
                ClienteCollection clienteCollection = new ClienteCollection();
                clienteCollection.BuscaClientesComAcesso(HttpContext.Current.User.Identity.Name);

                for (int i = 0; i < clienteCollection.Count; i++) {
                    int idClienteAux = clienteCollection[i].IdCliente.Value;
                    //
                    ReportSaldoAplicacaoFundo report = new ReportSaldoAplicacaoFundo(idCarteira, idClienteAux, data, tipoRelatorio,
                                    tipoGrupamento, opcaoValorAplicado, chkRentabilidade, HttpContext.Current.User.Identity.Name, tipoLingua);

                    report.CreateDocument();
                    if (report.TemDados) {
                        //
                        r.PrintingSystem.Pages.AddRange(report.PrintingSystem.Pages); // Adiciona Relatorio                                                
                    }
                }
                #endregion

                filename += "_" + data.Day + "-" + data.Month + "-" + data.Year;
            }

            #endregion
        }
        else if (tipoOpcoesCampo == TipoOpcoesCampo.Rendimento_PercentualRetorno) {
            #region ValorPFee_Participacao

            // Somente 1 Cliente
            if (idCliente.HasValue) {
                #region Apenas 1 Cliente
                ReportSaldoAplicacaoFundoView2 report = new ReportSaldoAplicacaoFundoView2(idCarteira, idCliente.Value, data, tipoRelatorioView2, tipoGrupamentoView2,
                                opcaoValorAplicadoView2, chkRentabilidade, HttpContext.Current.User.Identity.Name, tipoLinguaView2);

                report.CreateDocument();
                //
                r.PrintingSystem.Pages.AddRange(report.PrintingSystem.Pages); // Adiciona Relatorio
                //
                filename += idCliente.Value.ToString() + "_" + data.Day + "-" + data.Month + "-" + data.Year;
                #endregion
            }
            else { // Varios Clientes
                #region Varios Clientes
                ClienteCollection clienteCollection = new ClienteCollection();
                clienteCollection.BuscaClientesComAcesso(HttpContext.Current.User.Identity.Name);

                for (int i = 0; i < clienteCollection.Count; i++) {
                    int idClienteAux = clienteCollection[i].IdCliente.Value;
                    //
                    ReportSaldoAplicacaoFundoView2 report = new ReportSaldoAplicacaoFundoView2(idCarteira, idClienteAux, data, tipoRelatorioView2, tipoGrupamentoView2,
                                    opcaoValorAplicadoView2, chkRentabilidade, HttpContext.Current.User.Identity.Name, tipoLinguaView2);

                    report.CreateDocument();
                    if (report.TemDados) {
                        r.PrintingSystem.Pages.AddRange(report.PrintingSystem.Pages); // Adiciona Relatorio
                    }
                }
                #endregion

                filename += "_" + data.Day + "-" + data.Month + "-" + data.Year;
            }

            #endregion
        }

        #region Acrescenta Relatorio Sem Dados se não Existir Dados
        // Não Existe Dados Para Exportar - Exporta PDF Sem Dados
        if (r.PrintingSystem.Pages.Count == 0) {

            #region ReportSemDados
            ReportSemDados reportSemDados = new ReportSemDados();
            reportSemDados.CreateDocument();
            //
            r.PrintingSystem.Pages.AddRange(reportSemDados.PrintingSystem.Pages);
            #endregion
        }
        #endregion

        if (visao == VisaoRelatorio.visaoHTML) {
            ReportViewer1.Report = r;
        }
        else if (visao == VisaoRelatorio.visaoPDF) {
            #region PDF
            MemoryStream ms = new MemoryStream();
            r.ExportToPdf(ms);
            ms.Seek(0, SeekOrigin.Begin);
            Response.ClearContent();
            Response.ClearHeaders();
            Response.Buffer = true;
            Response.Cache.SetCacheability(HttpCacheability.Private);
            Response.ContentType = "application/pdf";
            Response.AddHeader("Content-Length", ms.Length.ToString(CultureInfo.CurrentCulture));
            Response.AddHeader("Content-Disposition", String.Format("attachment;filename=" + filename + ".pdf"));

            Response.BinaryWrite(ms.ToArray());
            ms.Close();
            HttpContext.Current.ApplicationInstance.CompleteRequest();
            Response.End();
            #endregion
        }
    }
}