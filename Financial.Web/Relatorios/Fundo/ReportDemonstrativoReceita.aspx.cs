﻿using System;
using Financial.Relatorio;
using System.IO;
using System.Web;
using System.Globalization;
using Financial.Web.Common;
public partial class _ReportDemonstrativoReceita : Financial.Web.Common.BasePage {

    new protected void Page_Load(object sender, EventArgs e) {
        string visao = Request.QueryString["Visao"].ToString();
        //
        //Response.Write(Request.Form["textData"].ToString());

        // Obrigatório
        DateTime dataReferencia = Convert.ToDateTime(Session["textData"]);
        //
        bool positivoTaxaPerformance = Convert.ToBoolean(Session["chkPositivo"]);
        //

        if (visao == "Report") {
            ReportViewer1.Report = new ReportDemonstrativoReceita(dataReferencia, positivoTaxaPerformance, HttpContext.Current.User.Identity.Name);
        }
        else if (visao == "PDF") {
            #region PDF
            ReportDemonstrativoReceita report = new ReportDemonstrativoReceita(dataReferencia, positivoTaxaPerformance, HttpContext.Current.User.Identity.Name);

            MemoryStream ms = new MemoryStream();
            report.ExportToPdf(ms);
            ms.Seek(0, SeekOrigin.Begin);
            Response.ClearContent();
            Response.ClearHeaders();
            Response.Buffer = true;
            Response.Cache.SetCacheability(HttpCacheability.Private);
            Response.ContentType = "application/pdf";
            Response.AddHeader("Content-Length", ms.Length.ToString(CultureInfo.CurrentCulture));
            Response.AddHeader("Content-Disposition", String.Format("attachment;filename=ReportDemonstrativoReceita.pdf"));

            Response.BinaryWrite(ms.ToArray());
            ms.Close();
            HttpContext.Current.ApplicationInstance.CompleteRequest();
            Response.End();
            #endregion
        }
        else if (visao == "Excel") {
            #region Excel
            ReportDemonstrativoReceita report = new ReportDemonstrativoReceita(dataReferencia, positivoTaxaPerformance, HttpContext.Current.User.Identity.Name);

            MemoryStream ms = new MemoryStream();
            report.ExportToXls(ms);
            ms.Seek(0, SeekOrigin.Begin);
            Response.ClearContent();
            Response.ClearHeaders();
            Response.Buffer = true;
            Response.Cache.SetCacheability(HttpCacheability.Private);
            Response.ContentType = "application/vnd.ms-excel";
            Response.AddHeader("Content-Length", ms.Length.ToString(CultureInfo.CurrentCulture));
            Response.AddHeader("Content-Disposition", String.Format("attachment;filename=ReportDemonstrativoReceita.xls"));

            Response.BinaryWrite(ms.ToArray());
            ms.Close();
            HttpContext.Current.ApplicationInstance.CompleteRequest();
            Response.End();

            #endregion
        }
    }
}

