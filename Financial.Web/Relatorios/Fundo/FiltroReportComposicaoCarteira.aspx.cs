﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Collections.Generic;
using System.Text;

using Financial.Fundo;
using Financial.Investidor;
using Financial.Security;
using Financial.Web.Util;
using Financial.Util;
using Financial.Web.Common;

using log4net;

using EntitySpaces.Interfaces;
using DevExpress.Web;

using Financial.Security.Enums;
using Financial.Common.Enums;
using Financial.Fundo.Enums;
using Financial.Investidor.Enums;
using System.Threading;
using Financial.Common;

public partial class FiltroReportComposicaoCarteira : FiltroReportBasePage
{
    private static readonly ILog log = LogManager.GetLogger(typeof(FiltroReportComposicaoCarteira));

    new protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            btnEditCodigo.Focus();



            if (!Page.IsPostBack)
            {
                dropTipoRelatorio.SelectedIndex = 1;

                if (Session["textData"] != null)                
                    this.textData.Text = Session["textData"].ToString();


                if (Session["btnEditCodigo"] != null)
                    this.btnEditCodigo.Text = Session["btnEditCodigo"].ToString();

                if (Session["dropTipoRelatorio"] != null)
                    this.dropTipoRelatorio.Value = Session["dropTipoRelatorio"].ToString();

                if (Session["dropMoeda"] != null)
                    this.dropMoeda.Value = Session["dropMoeda"].ToString();

                if (Session["dropLingua"] != null)
                    this.dropLingua.Value = Session["dropLingua"].ToString();

                if (Session["dropCustodia"] != null)
                    this.dropCustodia.Value = Session["dropCustodia"].ToString();
            }

            this.HasPopupCliente = true;
            base.Page_Load(sender, e);

            TrataTravamentoCampos();

            

            
                


        }
        catch (Exception exception)
        {
            new Financial.Web.Util.ErrorHandler().HandleArrayIndexError(exception);
        }
    }

    protected void EsDSCliente_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        try
        {
            ClienteCollection clienteCollection = new ClienteCollection();
            clienteCollection.BuscaClientesComAcesso(HttpContext.Current.User.Identity.Name);

            e.Collection = clienteCollection;
        }
        catch (Exception exception)
        {
            new Financial.Web.Util.ErrorHandler().HandleArrayIndexError(exception);
        }
    }

    protected void EsDSMoeda_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        try
        {
            MoedaCollection moedaCollection = new MoedaCollection();
            //
            moedaCollection.Query
                            .Select(moedaCollection.Query.IdMoeda, moedaCollection.Query.Nome)
                            .OrderBy(moedaCollection.Query.Nome.Ascending);

            moedaCollection.Query.Load();
            //
            Moeda m = new Moeda();
            m.IdMoeda = null;
            m.Nome = "";

            moedaCollection.AttachEntity(m);
            //
            moedaCollection.Sort = MoedaMetadata.ColumnNames.Nome + " ASC";
            //
            e.Collection = moedaCollection;
        }
        catch (Exception exception)
        {
            new Financial.Web.Util.ErrorHandler().HandleArrayIndexError(exception);
        }
    }

    private void TrataTravamentoCampos()
    {
        try
        {
            //Chegar se foi passado um IdCliente como parametro
            string idClienteParam = Request["idCliente"];
            bool temClienteQueryString = !String.IsNullOrEmpty(idClienteParam);
            int? idClienteQueryString = null;

            if (temClienteQueryString)
            {
                idClienteQueryString = Convert.ToInt32(idClienteParam);

                //Garantir que usuario tem acesso ao cliente
                bool temAcesso = ChecaUsuarioAcessaCliente(idClienteQueryString.Value);

                if (!temAcesso)
                {
                    throw new Exception("Usuário não possui acesso à carteira " + idClienteParam);
                }
            }

            Usuario usuario = new Usuario();
            usuario.BuscaUsuario(HttpContext.Current.User.Identity.Name);
            int tipoTrava = usuario.TipoTrava.Value;

            if (tipoTrava == (int)TipoTravaUsuario.TravaCliente)
            {
                btnEditCodigo.Enabled = false;
            }

            if (temClienteQueryString)
            {
                PreencheCamposCarteira(idClienteQueryString.Value);
            }
            else if (tipoTrava == (int)TipoTravaUsuario.TravaCliente)
            {
                PermissaoCliente permissaoCliente = new PermissaoCliente();
                int? idCarteira = permissaoCliente.RetornaClienteAssociado(HttpContext.Current.User.Identity.Name);

                if (idCarteira.HasValue)
                {
                    PreencheCamposCarteira(idCarteira.Value);
                }
            }
        }
        catch (Exception exception)
        {
            new Financial.Web.Util.ErrorHandler().HandleArrayIndexError(exception);
        }
    }
    
    protected bool ChecaUsuarioAcessaCliente(int idCliente)
    {
        ClienteCollection clienteCollection = new ClienteCollection();
        clienteCollection.BuscaClientesComAcesso(HttpContext.Current.User.Identity.Name);

        bool temAcesso = false;
        foreach (Cliente cliente in clienteCollection)
        {
            if (cliente.IdCliente.Value == idCliente)
            {
                temAcesso = true;
                break;
            }
        }

        return temAcesso;
    }
    
    protected void PreencheCamposCarteira(int idCarteira)
    {
        Cliente cliente = new Cliente();

        List<esQueryItem> campos = new List<esQueryItem>();
        campos.Add(cliente.Query.Apelido);
        cliente.LoadByPrimaryKey(campos, idCarteira);
        string apelido = cliente.Apelido;

        btnEditCodigo.Text = Convert.ToString(idCarteira);
        textNome.Text = apelido;

        Carteira carteira = new Carteira();
        if (carteira.LoadByPrimaryKey(idCarteira))
        {
            if (carteira.TipoCota == (byte)TipoCotaFundo.Fechamento)
            {
                this.dropTipoRelatorio.Enabled = false;
            }
        }
    }

    protected void ASPxCallback1_Callback(object source, DevExpress.Web.CallbackEventArgs e)
    {
        try
        {
            string nome = "";
            if (!String.IsNullOrEmpty(e.Parameter) && e.Parameter != "null")
            {
                int idCliente = Convert.ToInt32(e.Parameter);
                Cliente cliente = new Cliente();
                cliente.LoadByPrimaryKey(idCliente);

                Carteira carteira = new Carteira();
                string cota = "Fech";
                if (carteira.LoadByPrimaryKey(idCliente))
                {
                    byte tipoCota = carteira.TipoCota.Value;
                    cota = tipoCota == (byte)TipoCotaFundo.Abertura ? "Abert" : "Fech";
                }

                if (cliente.str.Apelido != "")
                {
                    if (cliente.IsAtivo)
                    {
                        //O permissionamento da carteira é dado direto no cliente vinculado à carteira
                        PermissaoCliente permissaoCliente = new PermissaoCliente();
                        if (permissaoCliente.RetornaAcessoCliente(idCliente, HttpContext.Current.User.Identity.Name))
                        {
                            DateTime dataDia = cliente.DataDia.Value;
                            nome = cliente.str.Apelido + "|" + dataDia.ToShortDateString() + "|" + cota + "|" + Thread.CurrentThread.CurrentCulture.Name;
                        }
                        else
                        {
                            nome = "no_access";
                        }
                    }
                    else
                    {
                        nome = "no_active";
                    }
                }
            }
            e.Result = nome;
        }
        catch (Exception exception)
        {
            new Financial.Web.Util.ErrorHandler().HandleArrayIndexError(exception);
        }
    }

    /// <summary>
    /// Seleciona o relatorio a ser executado
    /// </summary>
    /// <param name="visao">PDF, Excel ou Report</param>
    private void SelecionaRelatorio(string visao)
    {
        try
        {
            Response.BufferOutput = true;
            //
            // Salva os parametros na session
            Session["textData"] = this.textData.Text;
            Session["btnEditCodigo"] = this.btnEditCodigo.Text;
            Session["dropTipoRelatorio"] = this.dropTipoRelatorio.SelectedItem.Value;
            Session["dropMoeda"] = this.dropMoeda.SelectedItem.Value;
            Session["dropLingua"] = this.dropLingua.SelectedItem.Value;
            Session["dropCustodia"] = this.dropCustodia.SelectedItem.Value;
            
            //Response.Redirect("~/Relatorios/Fundo/ReportComposicaoCarteira.aspx?Visao=" + visao, false);


            string url = "~/Relatorios/Fundo/ReportComposicaoCarteira.aspx?Visao=" + visao;
            string redirectURL = Page.ResolveClientUrl(url);

            string script = "window.location = '" + redirectURL + "';";
            ScriptManager.RegisterStartupScript(this, typeof(Page), "RedirectTo", script, true);

        }
        catch (Exception exception)
        {
            new Financial.Web.Util.ErrorHandler().HandleArrayIndexError(exception);
        }
    }

    protected void btnVisualiza_Click(object sender, EventArgs e)
    {
        try
        {   
            this.TrataErros();
            this.SelecionaRelatorio(VisaoRelatorio.visaoHTML);
        }
        catch (Exception exception)
        {
            new Financial.Web.Util.ErrorHandler().HandleArrayIndexError(exception);
        }
    }

    protected void btnPDF_Click(object sender, EventArgs e)
    {
        try
        {
            this.TrataErros();
            this.SelecionaRelatorio(VisaoRelatorio.visaoPDF);
        }
        catch (Exception exception)
        {
            new Financial.Web.Util.ErrorHandler().HandleArrayIndexError(exception);
        }
    }

    protected void btnExcel_Click(object sender, EventArgs e)
    {
        try
        {
            this.TrataErros();
            this.SelecionaRelatorio(VisaoRelatorio.visaoExcel);
        }
        catch (Exception exception)
        {
            new Financial.Web.Util.ErrorHandler().HandleArrayIndexError(exception);
        }
    }

    /// <summary>
    /// Trata os Erros de Relatorio
    /// </summary>
    private void TrataErros()
    {
        #region Campos obrigatórios
        List<Control> controles = new List<Control>(new Control[] { 
    	    this.btnEditCodigo, this.textData });

        if (base.TestaObrigatorio(controles) != "")
        {
            throw new Exception("Campos com * são obrigatórios!");
        }
        #endregion

        if (textData.Text != "" && btnEditCodigo.Text != "" && Utilitario.IsDate(textData.Text))
        {
            DateTime dataReport = Convert.ToDateTime(textData.Text);

            Cliente cliente = new Cliente();
            cliente.LoadByPrimaryKey(Convert.ToInt32(btnEditCodigo.Text));

            TipoFeriado tipoFeriado = cliente.IdLocal.Value == (int)LocalFeriadoFixo.Brasil ? TipoFeriado.Brasil : TipoFeriado.Outros;
            if (!Calendario.IsDiaUtil(Convert.ToDateTime(textData.Text), cliente.IdLocal.Value, tipoFeriado))
            {
                throw new Exception("Dia não útil");
            }

            DateTime dataImplantacao = cliente.DataImplantacao.Value;
            DateTime dataDia = cliente.DataDia.Value;

            if (dataImplantacao > dataReport)
            {
                throw new Exception("Data passada anterior à data de implantação: " + dataImplantacao.ToString("d"));
            }

            if (cliente.TipoControle == (byte)TipoControleCliente.CarteiraImportada)
            {
                HistoricoCota historicoCota = new HistoricoCota();
                if (dataReport > historicoCota.BuscaDataMaxCota(cliente.IdCliente.Value))
                {
                    throw new Exception("Data passada posterior à última posição importada da carteira: " + dataDia.ToString("d"));
                }
            }

            if (dataReport > dataDia && cliente.TipoControle != (byte)TipoControleCliente.CarteiraImportada )
            {
                throw new Exception("Data passada posterior à data atual da carteira: " + dataDia.ToString("d"));
            }

            if (dropTipoRelatorio.SelectedIndex == 1)
            { //Relatório de Fechamento
                if (DateTime.Compare(dataReport, dataDia) == 0)
                {
                    if (cliente.Status == (byte)StatusCliente.Aberto ||
                        cliente.Status == (byte)StatusCliente.FechadoComErro)
                    {
                        throw new Exception("Para relatórios de fechamento, o status da carteira deve estar calculado ou fechado.");
                    }
                }
            }
        }
    }
}