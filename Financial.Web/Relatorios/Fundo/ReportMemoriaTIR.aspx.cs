﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.IO;
using Bytescout.Spreadsheet;
using System.Globalization;
using Financial.Fundo;
using System.Collections.Generic;
using Financial.CRM;
using Financial.Common;
using System.Drawing;
using Bytescout.Spreadsheet.Constants;
using Financial.Investidor;
using Financial.Util;

public partial class _ReportMemoriaTIR : System.Web.UI.Page
{

    Spreadsheet document;
    Worksheet worksheet;

    protected void Page_Load(object sender, EventArgs e)
    {
        // Obrigatorio
        int idCliente = Convert.ToInt32(Session["btnEditCodigo"]);
        DateTime dataInicio = Convert.ToDateTime(Session["textDataInicio"]);
        DateTime dataFim = Convert.ToDateTime(Session["textDataFim"]);
        bool fluxoLiquido = Convert.ToBoolean(Session["chkFluxoLiquido"]);

        int? idEstrategia = null;

        if (Session["dropEstrategia"] != "" && Session["dropEstrategia"] != null)
        {
            idEstrategia = Convert.ToInt32(Session["dropEstrategia"]);
        }
        //

        Cliente cliente = new Cliente();
        cliente.LoadByPrimaryKey(idCliente);

        Estrategia estrategia = new Estrategia();
        if (idEstrategia.HasValue)
        {
            estrategia.LoadByPrimaryKey(idEstrategia.Value);
        }

        bool historico = cliente.DataDia.Value > dataFim;

        string descricao = estrategia.Descricao;
        string descricaoCliente = idCliente.ToString() + " - " + cliente.Apelido;

        document = new Spreadsheet();
        worksheet = document.Workbook.Worksheets.Add("MemoriaTIR");

        worksheet.Cell(1, 2).Font = new Font("Arial", 8, FontStyle.Bold);
        worksheet.Cell(1, 2).Value = "Período:  " + dataInicio.ToShortDateString()  + " a " + dataFim.ToShortDateString();

        worksheet.Cell(2, 2).Font = new Font("Arial", 8, FontStyle.Bold);
        worksheet.Cell(2, 2).Value = "Cliente:  " + descricaoCliente;

        if (idEstrategia.HasValue)
        {
            worksheet.Cell(3, 2).Font = new Font("Arial", 8, FontStyle.Bold);
            worksheet.Cell(3, 2).Value = "Estratégia:  " + descricao;
        }

        int colunaData = 2;
        int colunaValor = 3;
        
        //Imprimir headers
        worksheet.Cell(5, colunaData).Font = new Font("Arial", 8, FontStyle.Bold);
        worksheet.Cell(5, colunaValor).Font = new Font("Arial", 8, FontStyle.Bold);

        worksheet.Cell(5, colunaData).Value = "Data";
        worksheet.Cell(5, colunaValor).Value = "Valor";
        worksheet.Cell(5, colunaValor).AlignmentHorizontal = AlignmentHorizontal.Centered;

                
        CalculoMedida calculoMedida = new CalculoMedida(idCliente);

        decimal valorInicial = 0;
        if (fluxoLiquido)
        {
            valorInicial = calculoMedida.RetornaValorMercadoHistoricoLiquido(dataInicio, idEstrategia);
        }
        else
        {
            valorInicial = calculoMedida.RetornaValorMercadoHistorico(dataInicio, idEstrategia);
        }

        decimal valorFinal = 0;
        if (fluxoLiquido)
        {
            valorFinal = calculoMedida.RetornaValorMercadoHistoricoLiquido(dataFim, idEstrategia);            
        }
        else
        {
            valorFinal = calculoMedida.RetornaValorMercadoHistorico(dataFim, idEstrategia);            
        }

        int linha = 6;

        List<CalculoFinanceiro.CashFlow> listaCF;
        if (fluxoLiquido)
        {
            listaCF = calculoMedida.RetornaListaFluxoCaixaLiquido(idCliente, idEstrategia, dataInicio, dataFim, valorInicial, valorFinal);
        }
        else
        {
            listaCF = calculoMedida.RetornaListaFluxoCaixa(idCliente, idEstrategia, dataInicio, dataFim, valorInicial, valorFinal);
        }

        decimal tirAnual = (decimal)CalculoFinanceiro.CalculaTIR(listaCF, dataInicio, Financial.Util.Enums.ContagemDias.Corridos);
                
        TimeSpan diferenca = dataFim - dataInicio;
        int numeroDias = diferenca.Days;

        decimal tirPeriodo = (decimal)Math.Pow((double)(tirAnual + 1), ((double)numeroDias / (double)365M)) - 1;

        decimal resultadoTotal = 0;
        foreach (CalculoFinanceiro.CashFlow cf in listaCF)
        {
            worksheet.Cell(linha, colunaData).AlignmentHorizontal = AlignmentHorizontal.Centered;
            worksheet.Cell(linha, colunaData).Font = new Font("Arial", 8, FontStyle.Regular);
            worksheet.Cell(linha, colunaData).Value = cf.data.ToShortDateString();
            worksheet.Columns[colunaData].Width = 80;

            worksheet.Cell(linha, colunaValor).Font = new Font("Arial", 8, FontStyle.Regular);
            worksheet.Cell(linha, colunaValor).NumberFormatString = "#,##0.00";
            worksheet.Cell(linha, colunaValor).ValueAsDouble = cf.valor;
            worksheet.Columns[colunaValor].Width = 90;

            linha++;

            resultadoTotal += (decimal)cf.valor;
        }

        linha += 2;

        worksheet.Cell(linha, colunaData).AlignmentHorizontal = AlignmentHorizontal.Centered;
        worksheet.Cell(linha, colunaData).Font = new Font("Arial", 8, FontStyle.Regular);
        worksheet.Cell(linha, colunaData).Value = "Resultado em $";
        worksheet.Columns[colunaData].Width = 80;

        worksheet.Cell(linha, colunaValor).Font = new Font("Arial", 8, FontStyle.Regular);
        worksheet.Cell(linha, colunaValor).NumberFormatString = "#,##0.00";
        worksheet.Cell(linha, colunaValor).ValueAsDouble = (double)resultadoTotal;
        worksheet.Columns[colunaValor].Width = 90;

        linha++;

        worksheet.Cell(linha, colunaData).AlignmentHorizontal = AlignmentHorizontal.Centered;
        worksheet.Cell(linha, colunaData).Font = new Font("Arial", 8, FontStyle.Regular);
        worksheet.Cell(linha, colunaData).Value = "TIR Ano";
        worksheet.Columns[colunaData].Width = 80;

        worksheet.Cell(linha, colunaValor).Font = new Font("Arial", 8, FontStyle.Regular);
        worksheet.Cell(linha, colunaValor).NumberFormatString = "#,##0.00%";
        worksheet.Cell(linha, colunaValor).ValueAsDouble = (double)tirAnual;
        worksheet.Columns[colunaValor].Width = 90;

        linha++;

        worksheet.Cell(linha, colunaData).AlignmentHorizontal = AlignmentHorizontal.Centered;
        worksheet.Cell(linha, colunaData).Font = new Font("Arial", 8, FontStyle.Regular);
        worksheet.Cell(linha, colunaData).Value = "Nr Dias";
        worksheet.Columns[colunaData].Width = 80;

        worksheet.Cell(linha, colunaValor).Font = new Font("Arial", 8, FontStyle.Regular);
        worksheet.Cell(linha, colunaValor).NumberFormatString = "#,##0";
        worksheet.Cell(linha, colunaValor).ValueAsDouble = (double)numeroDias;
        worksheet.Columns[colunaValor].Width = 90;

        linha++;

        worksheet.Cell(linha, colunaData).AlignmentHorizontal = AlignmentHorizontal.Centered;
        worksheet.Cell(linha, colunaData).Font = new Font("Arial", 8, FontStyle.Regular);
        worksheet.Cell(linha, colunaData).Value = "TIR Período";
        worksheet.Columns[colunaData].Width = 80;

        worksheet.Cell(linha, colunaValor).Font = new Font("Arial", 8, FontStyle.Regular);
        worksheet.Cell(linha, colunaValor).NumberFormatString = "#,##0.00%";
        worksheet.Cell(linha, colunaValor).ValueAsDouble = (double)tirPeriodo;
        worksheet.Columns[colunaValor].Width = 90;
        MemoryStream ms = new MemoryStream();

        document.SaveToStreamXLS(ms);
        document.Close();

        string fileName = "MemoriaTIR";

        ms.Seek(0, SeekOrigin.Begin);
        Response.ClearContent();
        Response.ClearHeaders();
        Response.Buffer = true;
        Response.Cache.SetCacheability(HttpCacheability.Private);
        Response.ContentType = "application/vnd.ms-excel";
        Response.AddHeader("Content-Length", ms.Length.ToString(CultureInfo.CurrentCulture));
        Response.AddHeader("Content-Disposition", String.Format("attachment;filename=" + fileName + ".xls"));

        Response.BinaryWrite(ms.ToArray());
        ms.Close();
        HttpContext.Current.ApplicationInstance.CompleteRequest();
        Response.End();

    }

    
}
