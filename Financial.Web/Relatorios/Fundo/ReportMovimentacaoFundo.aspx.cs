﻿using System;
using Financial.Relatorio;
using System.IO;
using System.Web;
using System.Globalization;
using Financial.Web.Common;

public partial class _ReportMovimentacaoFundo : BasePage {
    new protected void Page_Load(object sender, EventArgs e) {
        string visao = Request.QueryString["Visao"].ToString();
        //
        //Response.Write(Request.Form["textDataInicio"].ToString());
        //Response.Write("<br>" +Request.Form["textDataFim"].ToString());
        //Response.Write("<br>" + Request.Form["btnEditCodigoCliente"].ToString());
        //Response.Write("<br>" + Request.Form["btnEditCodigoCarteira"].ToString());

        DateTime? dataInicioOperacao = null;
        DateTime? dataFimOperacao = null;
        DateTime? dataInicioConversao = null;
        DateTime? dataFimConversao = null;
        DateTime? dataInicioLiquidacao = null;
        DateTime? dataFimLiquidacao = null;

        byte? tipoOperacao = null;

        #region Data Operação
        if (Convert.ToString(Session["textDataInicioOperacao"]) != "") {
            dataInicioOperacao = Convert.ToDateTime(Session["textDataInicioOperacao"]);
        }
        if (Convert.ToString(Session["textDataFimOperacao"]) != "") {
            dataFimOperacao = Convert.ToDateTime(Session["textDataFimOperacao"]);
        }        
        #endregion

        #region Data Conversão
        if (Convert.ToString(Session["textDataInicioConversao"]) != "") {
            dataInicioConversao = Convert.ToDateTime(Session["textDataInicioConversao"]);
        }
        if (Convert.ToString(Session["textDataFimConversao"]) != "") {
            dataFimConversao = Convert.ToDateTime(Session["textDataFimConversao"]);
        }
        #endregion

        #region Data Liquidação
        if (Convert.ToString(Session["textDataInicioLiquidacao"]) != "") {
            dataInicioLiquidacao = Convert.ToDateTime(Session["textDataInicioLiquidacao"]);
        }
        if (Convert.ToString(Session["textDataFimLiquidacao"]) != "") {
            dataFimLiquidacao = Convert.ToDateTime(Session["textDataFimLiquidacao"]);
        }
        #endregion

        if (Session["dropTipoOperacao"] != null) {
            tipoOperacao = Convert.ToByte(Session["dropTipoOperacao"]);
        }

        int? idCliente = null;
        int? idCarteira = null;
        
        // Opcional
        if (!String.IsNullOrEmpty((string)Session["btnEditCodigoCliente"])) {
            idCliente = Convert.ToInt32(Session["btnEditCodigoCliente"]);
        }            
        if (!String.IsNullOrEmpty((string)Session["btnEditCodigoCarteira"])) {
            idCarteira = Convert.ToInt32(Session["btnEditCodigoCarteira"]);
        }
        
        if (visao == "Report") {
            ReportViewer1.Report = new ReportMovimentacaoFundo(idCliente, idCarteira, 
                                                               dataInicioOperacao, dataFimOperacao, 
                                                               dataInicioConversao,dataFimConversao, 
                                                               dataInicioLiquidacao, dataFimLiquidacao, tipoOperacao);
        }
        else if (visao == "PDF") {
            #region PDF
            ReportMovimentacaoFundo report = new ReportMovimentacaoFundo(idCliente, idCarteira, dataInicioOperacao, dataFimOperacao,
                                                               dataInicioConversao, dataFimConversao, dataInicioLiquidacao, dataFimLiquidacao, tipoOperacao);

            MemoryStream ms = new MemoryStream();
            report.ExportToPdf(ms);
            ms.Seek(0, SeekOrigin.Begin);
            Response.ClearContent();
            Response.ClearHeaders();
            Response.Buffer = true;
            Response.Cache.SetCacheability(HttpCacheability.Private);
            Response.ContentType = "application/pdf";
            Response.AddHeader("Content-Length", ms.Length.ToString(CultureInfo.CurrentCulture));
            Response.AddHeader("Content-Disposition", String.Format("attachment;filename=ReportMovimentacaoFundo.pdf"));

            Response.BinaryWrite(ms.ToArray());
            ms.Close();
            HttpContext.Current.ApplicationInstance.CompleteRequest();
            Response.End();
            #endregion
        }
        else if (visao == "Excel") {
            #region Excel
            ReportMovimentacaoFundo report = new ReportMovimentacaoFundo(idCliente, idCarteira, dataInicioOperacao, dataFimOperacao,
                                                                         dataInicioConversao, dataFimConversao, dataInicioLiquidacao, dataFimLiquidacao, tipoOperacao);

            MemoryStream ms = new MemoryStream();
            report.ExportToXls(ms);
            ms.Seek(0, SeekOrigin.Begin);
            Response.ClearContent();
            Response.ClearHeaders();
            Response.Buffer = true;
            Response.Cache.SetCacheability(HttpCacheability.Private);
            Response.ContentType = "application/vnd.ms-excel";
            Response.AddHeader("Content-Length", ms.Length.ToString(CultureInfo.CurrentCulture));
            Response.AddHeader("Content-Disposition", String.Format("attachment;filename=ReportMovimentacaoFundo.xls"));

            Response.BinaryWrite(ms.ToArray());
            ms.Close();
            HttpContext.Current.ApplicationInstance.CompleteRequest();
            Response.End();

            #endregion
        }
    }
}