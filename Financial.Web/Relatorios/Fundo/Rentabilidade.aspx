﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Rentabilidade.aspx.cs" Inherits="Consultas_Rentabilidade" %>

<%@ Register Assembly="System.Web, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"
    Namespace="System.Web.UI.HtmlControls" TagPrefix="cc2" %>
<%@ Register Assembly="EntitySpaces.Web" Namespace="EntitySpaces.Web" TagPrefix="cc1" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxe" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxwgv" %>

<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxpc" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxcb" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <link href="~/css/forms.css" rel="stylesheet" type="text/css" />

    <script type="text/javascript" language="Javascript" src="../../js/global.js"></script>

    <script language="JavaScript">    
    function OnGetDataCliente(values) {
        btnEditCodigoCliente.SetValue(values);                    
        popupCliente.HideWindow();
        ASPxCallback1.SendCallback(btnEditCodigoCliente.GetValue());        
        btnEditCodigoCliente.Focus();    
    }
    
    function OnGetDataAtivo (data) {
            hiddenCompositeKey.SetValue(data);
            callBackPopupAtivo.SendCallback(data);
            popupAtivo.HideWindow();
            btnEditAtivo.Focus();
        }  
                    
    function AdjustSize() {
        var height = Math.max(0, document.documentElement.clientHeight);
        height -= height * 0.25;
        gridConsulta.SetHeight(height);
    } 
        
    window.onresize = AdjustSize;        
    </script>

</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="true"
            EnableScriptLocalization="true" />
        <dxcb:ASPxCallback ID="ASPxCallback1" runat="server" OnCallback="ASPxCallback1_Callback">
            <ClientSideEvents CallbackComplete="function(s, e) {                   
                OnCallBackCompleteCliente(s, e, popupMensagemCliente, btnEditCodigoCliente, document.getElementById('textNome'));
            }        
            " />
        </dxcb:ASPxCallback>
        <dxcb:ASPxCallback ID="callBackPopupAtivo" runat="server" OnCallback="callBackPopupAtivo_Callback">
            <ClientSideEvents CallbackComplete="function(s, e) {if (e.result != null) btnEditAtivo.SetValue(e.result); } " />
        </dxcb:ASPxCallback>
        <dxpc:ASPxPopupControl ID="popupAtivo" ClientInstanceName="popupAtivo" runat="server"
            Width="550px" HeaderText="" ContentStyle-VerticalAlign="Top" PopupVerticalAlign="Middle"
            PopupHorizontalAlign="OutsideRight" AllowDragging="True">
            <ContentStyle VerticalAlign="Top">
            </ContentStyle>
            <ContentCollection>
                <dxpc:PopupControlContentControl ID="PopupControlContentControl2" runat="server">
                    <div>
                        <dxwgv:ASPxGridView ID="gridAtivo" runat="server" Width="100%" ClientInstanceName="gridAtivo"
                            AutoGenerateColumns="False" DataSourceID="EsDSAtivoFiltro" KeyFieldName="CompositeKey"
                            OnCustomDataCallback="gridAtivo_CustomDataCallback" OnCustomCallback="gridAtivo_CustomCallback"
                            OnLoad="gridAtivo_Load">
                            <Columns>
                                <dxwgv:GridViewDataDateColumn FieldName="CompositeKey" Visible="false" VisibleIndex="0" />
                                <dxwgv:GridViewDataComboBoxColumn FieldName="TipoMercado" VisibleIndex="1">
                                    <PropertiesComboBox ValueType="System.String">
                                    </PropertiesComboBox>
                                </dxwgv:GridViewDataComboBoxColumn>
                                <dxwgv:GridViewDataTextColumn FieldName="IdAtivo" Caption="Id.Ativo" VisibleIndex="2"
                                    Width="14%" />
                                <dxwgv:GridViewDataTextColumn FieldName="Descricao" Caption="Descrição" VisibleIndex="3"
                                    Width="60%" />
                            </Columns>
                            <Settings ShowFilterRow="true" ShowTitlePanel="True" />
                            <SettingsBehavior ColumnResizeMode="Disabled" />
                            <ClientSideEvents RowDblClick="function(s, e) {
                gridAtivo.GetValuesOnCustomCallback(e.visibleIndex,OnGetDataAtivo);}" Init="function(s, e) {e.cancel = true;}" />
                            <SettingsDetail ShowDetailButtons="False" />
                            <Styles AlternatingRow-Enabled="True">
                                <AlternatingRow Enabled="True">
                                </AlternatingRow>
                                <Header ImageSpacing="5px" SortingImageSpacing="5px" />
                            </Styles>
                            <Images>
                                <PopupEditFormWindowClose Height="17px" Width="17px" />
                            </Images>
                            <SettingsText EmptyDataRow="0 Registros" Title="Pesquisa Ativo." />
                        </dxwgv:ASPxGridView>
                    </div>
                </dxpc:PopupControlContentControl>
            </ContentCollection>
            <ClientSideEvents CloseUp="function(s, e) {gridAtivo.ClearFilter(); }" />
        </dxpc:ASPxPopupControl>
        <div class="divPanel">
            <table width="100%">
                <tr>
                    <td>
                        <div id="container_small">
                            <div id="header">
                                <asp:Label ID="lblHeader" runat="server" Text="Rentabilidade"></asp:Label>
                            </div>
                            <div id="mainContent">
                                <div class="reportFilter">
                                    <div style="height: 20px">
                                    </div>
                                    <table cellpadding="2" cellspacing="2">
                                        <tr>
                                            <td class="td_Label">
                                                <asp:Label ID="labelCliente" runat="server" CssClass="labelRequired" Text="Cliente:" />
                                            </td>
                                            <td>
                                                <dxe:ASPxSpinEdit ID="btnEditCodigoCliente" runat="server" CssClass="textButtonEdit"
                                                    EnableClientSideAPI="True" ClientInstanceName="btnEditCodigoCliente" MaxLength="15"
                                                    NumberType="Integer" AllowMouseWheel="false">
                                                    <Buttons>
                                                        <dxe:EditButton>
                                                        </dxe:EditButton>
                                                    </Buttons>
                                                    <ClientSideEvents KeyPress="function(s, e) {document.getElementById('textNome').value = '';}"
                                                        ButtonClick="function(s, e) {popupCliente.ShowAtElementByID(s.name);}" LostFocus="function(s, e) {OnLostFocus(popupMensagemCliente, ASPxCallback1, btnEditCodigoCliente);}" />
                                                </dxe:ASPxSpinEdit>
                                            </td>
                                            <td>
                                                <asp:TextBox ID="textNome" runat="server" CssClass="textNome" Enabled="false"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="td_Label">
                                                <asp:Label ID="labelDataInicio" runat="server" CssClass="labelRequired" Text="Início:"></asp:Label>
                                            </td>
                                            <td>
                                                <dxe:ASPxDateEdit ID="textDataInicio" runat="server" ClientInstanceName="textDataInicio" />
                                            </td>
                                            <td colspan="2">
                                                <table>
                                                    <tr>
                                                        <td>
                                                            <asp:Label ID="labelDataFim" runat="server" CssClass="labelRequired" Text="Fim:" /></td>
                                                        <td>
                                                            <dxe:ASPxDateEdit ID="textDataFim" runat="server" ClientInstanceName="textDataFim" />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="td_Label">
                                                <dxe:ASPxLabel ID="labelTabela" runat="server" CssClass="labelNormal" Text="Ativo:">
                                                </dxe:ASPxLabel>
                                            </td>
                                            <td colspan="2">
                                                <dxe:ASPxButtonEdit ID="btnEditAtivo" runat="server" CssClass="textButtonEdit" EnableClientSideAPI="True"
                                                    ClientInstanceName="btnEditAtivo" ReadOnly="true">
                                                    <Buttons>
                                                        <dxe:EditButton />
                                                    </Buttons>
                                                    <ClientSideEvents ButtonClick="function(s, e) {popupAtivo.ShowAtElementByID(s.name); gridAtivo.PerformCallback('btnRefresh');}" />
                                                </dxe:ASPxButtonEdit>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <dxe:ASPxTextBox ID="hiddenCompositeKey" runat="server" CssClass="hiddenField" ClientInstanceName="hiddenCompositeKey" />
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                                <div id="progressBar" class="progressBar" runat="server">
                                    <asp:UpdateProgress ID="UpdateProgress1" runat="server">
                                        <ProgressTemplate>
                                            <dxe:ASPxImage runat="server" ID="roller" ImageUrl="~/imagens/roller.gif" ClientInstanceName="roller" />
                                            <asp:Label ID="lblCarregar" runat="server" Text="Carregando..." />
                                        </ProgressTemplate>
                                    </asp:UpdateProgress>
                                </div>
                                <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                    <ContentTemplate>
                                        <div class="linkButton linkButtonNoBorder">
                                            <asp:LinkButton ID="btnRun" runat="server" Font-Overline="false" CssClass="btnRun"
                                                OnClick="btnRun_Click">
                                                <asp:Literal ID="Literal1" runat="server" Text="Consultar" /><div>
                                                </div>
                                            </asp:LinkButton>
                                            <asp:LinkButton ID="btnPDF" runat="server" Font-Overline="false" CssClass="btnPdf"
                                                OnClick="btnPDF_Click">
                                                <asp:Literal ID="Literal2" runat="server" Text="Gerar PDF" /><div>
                                                </div>
                                            </asp:LinkButton>
                                            <asp:LinkButton ID="btnExcel" runat="server" Font-Overline="false" CssClass="btnExcel"
                                                OnClick="btnExcel_Click">
                                                <asp:Literal ID="Literal3" runat="server" Text="Gerar Excel" /><div>
                                                </div>
                                            </asp:LinkButton>
                                            <asp:LinkButton ID="btnCustomFields" runat="server" Font-Overline="false" ValidationGroup="ATK"
                                                CssClass="btnCustomFields" OnClientClick="gridConsulta.ShowCustomizationWindow(); return false;">
                                                <asp:Literal ID="Literal11" runat="server" Text="Mais Campos" /><div>
                                                </div>
                                            </asp:LinkButton>
                                        </div>
                                        <div class="divDataGrid">
                                            <dxwgv:ASPxGridView ID="gridConsulta" runat="server" ClientInstanceName="gridConsulta"
                                                EnableCallBacks="true" DataSourceID="EsDSRentabilidade" OnHtmlDataCellPrepared="gridConsulta_HtmlDataCellPrepared"
                                                OnHtmlFooterCellPrepared="gridConsulta_HtmlFooterCellPrepared" OnHtmlRowPrepared="gridConsulta_HtmlRowPrepared"
                                                OnLoad="gridConsulta_Load" Width="100%" Settings-HorizontalScrollBarMode="Visible" AutoGenerateColumns="false" SettingsBehavior-ColumnResizeMode="Control">
                                                <Columns>
                                                    <dxwgv:GridViewDataTextColumn FieldName="IdRentabilidade" Visible="false" VisibleIndex="1"
                                                        ShowInCustomizationForm="false">
                                                    </dxwgv:GridViewDataTextColumn>
                                                    <dxwgv:GridViewDataDateColumn FieldName="Data" VisibleIndex="2">
                                                    </dxwgv:GridViewDataDateColumn>
                                                    <dxwgv:GridViewDataTextColumn FieldName="Apelido" Caption="Cliente" VisibleIndex="3">
                                                    </dxwgv:GridViewDataTextColumn>
                                                    <dxwgv:GridViewDataTextColumn FieldName="CodigoAtivo" VisibleIndex="4">
                                                    </dxwgv:GridViewDataTextColumn>
                                                    <dxwgv:GridViewDataComboBoxColumn FieldName="TipoAtivo" VisibleIndex="5">
                                                        <PropertiesComboBox ValueType="System.String">
                                                        </PropertiesComboBox>
                                                    </dxwgv:GridViewDataComboBoxColumn>
                                                    <dxwgv:GridViewDataTextColumn FieldName="CodigoMoedaPortfolio" Caption="Moeda - Carteira"
                                                        VisibleIndex="6">
                                                    </dxwgv:GridViewDataTextColumn>
                                                    <dxwgv:GridViewDataTextColumn FieldName="CodigoMoedaAtivo" Caption="Moeda - Ativo"
                                                        VisibleIndex="7">
                                                    </dxwgv:GridViewDataTextColumn>
                                                    <dxwgv:GridViewDataTextColumn FieldName="PatrimonioInicialMoedaPortfolio" Caption="Patrimonio Inicial Líquido - Carteira"
                                                        VisibleIndex="8">
                                                        <PropertiesTextEdit DisplayFormatString="{0:#,##0.00;(#,##0.00);0.00}">
                                                        </PropertiesTextEdit>
                                                    </dxwgv:GridViewDataTextColumn>
                                                    <dxwgv:GridViewDataTextColumn FieldName="PatrimonioInicialMoedaAtivo" Caption="Patrimonio Inicial Líquido - Ativo" Visible="false"
                                                        VisibleIndex="9">
                                                        <PropertiesTextEdit DisplayFormatString="{0:#,##0.00;(#,##0.00);0.00}">
                                                        </PropertiesTextEdit>
                                                    </dxwgv:GridViewDataTextColumn>
                                                    <dxwgv:GridViewDataTextColumn FieldName="PatrimonioInicialBrutoMoedaPortfolio" Caption="Patrimonio Inicial Bruto - Carteira"
                                                        VisibleIndex="10">
                                                        <PropertiesTextEdit DisplayFormatString="{0:#,##0.00;(#,##0.00);0.00}">
                                                        </PropertiesTextEdit>
                                                    </dxwgv:GridViewDataTextColumn>                                                         
                                                    <dxwgv:GridViewDataTextColumn FieldName="PatrimonioInicialBrutoMoedaAtivo" Caption="Patrimonio Inicial Bruto - Ativo" Visible="false"
                                                        VisibleIndex="10">
                                                        <PropertiesTextEdit DisplayFormatString="{0:#,##0.00;(#,##0.00);0.00}">
                                                        </PropertiesTextEdit>
                                                    </dxwgv:GridViewDataTextColumn>                                                    
                                                    <dxwgv:GridViewDataTextColumn FieldName="QuantidadeInicialAtivo" Caption="Qtde.Inicial"
                                                        VisibleIndex="11">
                                                        <PropertiesTextEdit DisplayFormatString="{0:#,##0.00000000;(#,##0.00000000);0.00000000}">
                                                        </PropertiesTextEdit>
                                                    </dxwgv:GridViewDataTextColumn>
                                                    <dxwgv:GridViewDataTextColumn FieldName="ValorFinanceiroEntradaMoedaPortfolio" Caption="Vl.Entrada - Carteira"
                                                        VisibleIndex="12">
                                                        <PropertiesTextEdit DisplayFormatString="{0:#,##0.00;(#,##0.00);0.00}">
                                                        </PropertiesTextEdit>
                                                    </dxwgv:GridViewDataTextColumn>
                                                    <dxwgv:GridViewDataTextColumn FieldName="ValorFinanceiroEntradaMoedaAtivo" Caption="Vl.Entrada - Ativo" Visible="false"
                                                        VisibleIndex="13">
                                                        <PropertiesTextEdit DisplayFormatString="{0:#,##0.00;(#,##0.00);0.00}">
                                                        </PropertiesTextEdit>
                                                    </dxwgv:GridViewDataTextColumn>
                                                    <dxwgv:GridViewDataTextColumn FieldName="QuantidadeTotalEntradaAtivo" Caption="Qtde.Entrada"
                                                        VisibleIndex="14">
                                                        <PropertiesTextEdit DisplayFormatString="{0:#,##0.00000000;(#,##0.00000000);0.00000000}">
                                                        </PropertiesTextEdit>
                                                    </dxwgv:GridViewDataTextColumn>
                                                    <dxwgv:GridViewDataTextColumn FieldName="ValorFinanceiroSaidaMoedaPortfolio" Caption="Vl.Saída - Carteira"
                                                        VisibleIndex="15">
                                                        <PropertiesTextEdit DisplayFormatString="{0:#,##0.00;(#,##0.00);0.00}">
                                                        </PropertiesTextEdit>
                                                    </dxwgv:GridViewDataTextColumn>
                                                    <dxwgv:GridViewDataTextColumn FieldName="ValorFinanceiroSaidaMoedaAtivo" Caption="Vl.Saída - Ativo" Visible="false"
                                                        VisibleIndex="16">
                                                        <PropertiesTextEdit DisplayFormatString="{0:#,##0.00;(#,##0.00);0.00}">
                                                        </PropertiesTextEdit>
                                                    </dxwgv:GridViewDataTextColumn>
                                                    <dxwgv:GridViewDataTextColumn FieldName="QuantidadeTotalSaidaAtivo" Caption="Qtde.Saída"
                                                        VisibleIndex="17">
                                                        <PropertiesTextEdit DisplayFormatString="{0:#,##0.00000000;(#,##0.00000000);0.00000000}">
                                                        </PropertiesTextEdit>
                                                    </dxwgv:GridViewDataTextColumn>
                                                    <dxwgv:GridViewDataTextColumn FieldName="ValorFinanceiroRendasMoedaPortfolio" Caption="Vl.Rendas - Carteira"
                                                        VisibleIndex="18">
                                                        <PropertiesTextEdit DisplayFormatString="{0:#,##0.00;(#,##0.00);0.00}">
                                                        </PropertiesTextEdit>
                                                    </dxwgv:GridViewDataTextColumn>
                                                    <dxwgv:GridViewDataTextColumn FieldName="ValorFinanceiroRendasMoedaAtivo" Caption="Vl.Rendas - Ativo" Visible="false"
                                                        VisibleIndex="19">
                                                        <PropertiesTextEdit DisplayFormatString="{0:#,##0.00;(#,##0.00);0.00}">
                                                        </PropertiesTextEdit>
                                                    </dxwgv:GridViewDataTextColumn>
                                                    <dxwgv:GridViewDataTextColumn FieldName="PatrimonioFinalBrutoMoedaPortfolio" Caption="Patrimonio Final Bruto - Carteira"
                                                        VisibleIndex="20">
                                                        <PropertiesTextEdit DisplayFormatString="{0:#,##0.00;(#,##0.00);0.00}">
                                                        </PropertiesTextEdit>
                                                    </dxwgv:GridViewDataTextColumn>
                                                    <dxwgv:GridViewDataTextColumn FieldName="PatrimonioFinalBrutoMoedaAtivo" Caption="Patrimonio Final Bruto - Ativo" Visible="false"
                                                        VisibleIndex="21">
                                                        <PropertiesTextEdit DisplayFormatString="{0:#,##0.00;(#,##0.00);0.00}">
                                                        </PropertiesTextEdit>
                                                    </dxwgv:GridViewDataTextColumn>                                                       
                                                    <dxwgv:GridViewDataTextColumn FieldName="PatrimonioFinalMoedaPortfolio" Caption="Patrimonio Final Líquido - Carteira"
                                                        VisibleIndex="22">
                                                        <PropertiesTextEdit DisplayFormatString="{0:#,##0.00;(#,##0.00);0.00}">
                                                        </PropertiesTextEdit>
                                                    </dxwgv:GridViewDataTextColumn>  
                                                    <dxwgv:GridViewDataTextColumn FieldName="PatrimonioFinalMoedaAtivo" Caption="Patrimonio Final Líquido - Ativo" Visible="false"
                                                        VisibleIndex="23">
                                                        <PropertiesTextEdit DisplayFormatString="{0:#,##0.00;(#,##0.00);0.00}">
                                                        </PropertiesTextEdit>
                                                    </dxwgv:GridViewDataTextColumn>                                                                                                                                                       
                                                    <dxwgv:GridViewDataTextColumn FieldName="QuantidadeFinalAtivo" Caption="Qtde.Final"
                                                        VisibleIndex="24">
                                                        <PropertiesTextEdit DisplayFormatString="{0:#,##0.00000000;(#,##0.00000000);0.00000000}">
                                                        </PropertiesTextEdit>
                                                    </dxwgv:GridViewDataTextColumn>
                                                    <dxwgv:GridViewDataTextColumn FieldName="RentabilidadeMoedaPortfolio" Caption="Rentabilidade Líquida - Carteira"
                                                        VisibleIndex="25">
                                                        <PropertiesTextEdit DisplayFormatString="{0:#,##0.00000000;(#,##0.00000000);0.00000000}%">
                                                        </PropertiesTextEdit>
                                                    </dxwgv:GridViewDataTextColumn>
                                                    <dxwgv:GridViewDataTextColumn FieldName="RentabilidadeMoedaAtivo" Caption="Rentabilidade Líquida - Ativo" Visible="false"
                                                        VisibleIndex="26">
                                                        <PropertiesTextEdit DisplayFormatString="{0:#,##0.00000000;(#,##0.00000000);0.00000000}%">
                                                        </PropertiesTextEdit>
                                                    </dxwgv:GridViewDataTextColumn>
                                                    <dxwgv:GridViewDataTextColumn FieldName="IdCliente" Caption="Id.Cliente" VisibleIndex="25"
                                                        Visible="False" ShowInCustomizationForm="false">
                                                    </dxwgv:GridViewDataTextColumn>
                                                    <dxwgv:GridViewDataTextColumn FieldName="PatrimonioFinalGrossUpMoedaPortfolio" Caption="Vl.Bruto. GrossUp - Carteira"
                                                        VisibleIndex="27">
                                                        <PropertiesTextEdit DisplayFormatString="{0:#,##0.00;(#,##0.00);0.00}">
                                                        </PropertiesTextEdit>
                                                    </dxwgv:GridViewDataTextColumn>                                                    
                                                    <dxwgv:GridViewDataTextColumn FieldName="PatrimonioFinalGrossUpMoedaAtivo" Caption="Vl.Bruto. GrossUp - Ativo" Visible="false"
                                                        VisibleIndex="28">
                                                        <PropertiesTextEdit DisplayFormatString="{0:#,##0.00;(#,##0.00);0.00}">
                                                        </PropertiesTextEdit>
                                                    </dxwgv:GridViewDataTextColumn>
                                                    <dxwgv:GridViewDataTextColumn FieldName="RentabilidadeGrossUpDiariaMoedaPortfolio" Caption="Rent. GrossUp - Carteira"
                                                        VisibleIndex="29">
                                                        <PropertiesTextEdit DisplayFormatString="{0:#,##0.00000000;(#,##0.00000000);0.00000000}%">
                                                        </PropertiesTextEdit>
                                                    </dxwgv:GridViewDataTextColumn>                                                    
                                                    <dxwgv:GridViewDataTextColumn FieldName="RentabilidadeGrossUpDiariaMoedaAtivo" Caption="Rent. GrossUp - Ativo" Visible="false"
                                                        VisibleIndex="30">
                                                        <PropertiesTextEdit DisplayFormatString="{0:#,##0.00000000;(#,##0.00000000);0.00000000}%">
                                                        </PropertiesTextEdit>
                                                    </dxwgv:GridViewDataTextColumn>
                                                    <dxwgv:GridViewDataTextColumn FieldName="RentabilidadeBrutaDiariaMoedaPortfolio" Caption="Rent. S/ GrossUp (Bruta) - Carteira"
                                                        VisibleIndex="31">
                                                        <PropertiesTextEdit DisplayFormatString="{0:#,##0.00000000;(#,##0.00000000);0.00000000}%">
                                                        </PropertiesTextEdit>
                                                    </dxwgv:GridViewDataTextColumn>                                                    
                                                    <dxwgv:GridViewDataTextColumn FieldName="RentabilidadeBrutaDiariaMoedaAtivo" Caption="Rent. S/ GrossUp (Bruta) - Ativo" Visible="false"
                                                        VisibleIndex="32">
                                                        <PropertiesTextEdit DisplayFormatString="{0:#,##0.00000000;(#,##0.00000000);0.00000000}%">
                                                        </PropertiesTextEdit>
                                                    </dxwgv:GridViewDataTextColumn>
                                                    <dxwgv:GridViewDataTextColumn FieldName="RentabilidadeAtivosIcentivados" Caption="Rent. Ativos Incentivados"
                                                        VisibleIndex="33">
                                                        <PropertiesTextEdit DisplayFormatString="{0:#,##0.00000000;(#,##0.00000000);0.00000000}%">
                                                        </PropertiesTextEdit>
                                                    </dxwgv:GridViewDataTextColumn>
                                                    <dxwgv:GridViewDataTextColumn FieldName="RentabilidadeAtivosTributados" Caption="Rent. Ativos Tributados"
                                                        VisibleIndex="34">
                                                        <PropertiesTextEdit DisplayFormatString="{0:#,##0.00000000;(#,##0.00000000);0.00000000}%">
                                                        </PropertiesTextEdit>
                                                    </dxwgv:GridViewDataTextColumn>
                                                    <dxwgv:GridViewDataTextColumn FieldName="CotaGerencial" Caption="Cota Gerencial"
                                                        VisibleIndex="35">
                                                        <PropertiesTextEdit DisplayFormatString="{0:#,##0.00000000;(#,##0.00000000);0.00000000}">
                                                        </PropertiesTextEdit>
                                                    </dxwgv:GridViewDataTextColumn>
                                                    <dxwgv:GridViewDataTextColumn FieldName="CompositeKey" UnboundType="String" Visible="False"
                                                        ShowInCustomizationForm="false" />
                                                </Columns>
                                                <Settings ShowFilterRow="True" ShowStatusBar="Visible" ShowFooter="True" VerticalScrollBarMode="Visible"
                                                    VerticalScrollableHeight="340" VerticalScrollBarStyle="Virtual" />
                                                <SettingsCustomizationWindow Enabled="True" />
                                                <SettingsText EmptyDataRow="0 registros" />
                                                <Styles>
                                                    <Footer Font-Bold="True" />
                                                </Styles>
                                                <ClientSideEvents Init="AdjustSize" EndCallback="AdjustSize" />
                                            </dxwgv:ASPxGridView>                                            
                                        </div>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                                
                            </div>
                        </div>
                    </td>
                </tr>
            </table>
        </div>
        <dxwgv:ASPxGridViewExporter ID="gridExport" runat="server" GridViewID="gridConsulta"
            Landscape="true" />
        <cc1:esDataSource ID="EsDSCliente" runat="server" OnesSelect="EsDSCliente_esSelect" />
        <cc1:esDataSource ID="EsDSRentabilidade" runat="server" OnesSelect="EsDSRentabilidade_esSelect" />
        <cc1:esDataSource ID="EsDSAtivoFiltro" runat="server" OnesSelect="EsDSAtivo_esSelect" />
        <cc1:esDataSource ID="EsDataSource1" runat="server" OnesSelect="EsDSCliente_esSelect" />
    </form>

    <script type="text/javascript">
// UpdateProgress Para Telas de Consultas com Impressao de Relatorio
// UpdateProgress Condicional que só atua no elemento Button - btnRun

var pageManager = Sys.WebForms.PageRequestManager.getInstance();
pageManager.add_beginRequest(BeginRequestHandler);
pageManager.add_endRequest(EndRequestHandler);

function BeginRequestHandler(sender, args) {              

    var elem = args.get_postBackElement();
    // First set associatedUpdatePanelId to non existant control
    // this will force the updateprogress not to try and show itself.               
    var o = $find('<%= UpdateProgress1.ClientID %>');
    o.set_associatedUpdatePanelId('Non_Existant_Control_Id');

    // Then based on the control ID, show the updateprogress
    if (elem.id == '<%= btnRun.ClientID %>') {        
        var updateProgress1 = $get('<%= UpdateProgress1.ClientID %>');  

        sleep(900); // Espera um pouco quando tem a mensagem de campos InvÃ¡lidos

        updateProgress1.style.display = '';
    }
}

function EndRequestHandler(sender, args) {
    var updateProgress1 = $get('<%= UpdateProgress1.ClientID %>');        
    updateProgress1.style.display = (updateProgress1.style.display == '') ? 'none' : '';     
}

function sleep(milliseconds) {
  var start = new Date().getTime();
  for (var i = 0; i < 1e7; i++) {
    if ((new Date().getTime() - start) > milliseconds){
      break;
    }
  }
}
    </script>

</body>
</html>
