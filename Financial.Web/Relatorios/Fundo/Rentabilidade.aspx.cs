﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Collections.Generic;
using System.Text;
using DevExpress.Web;
using System.Drawing;
using Financial.Web.Common;
using Financial.Fundo.Enums;
using Financial.Fundo;
using Financial.Investidor;
using Financial.Investidor.Enums;
using Financial.Web.Util;
using Financial.RendaFixa;
using Financial.Swap;
using Financial.Bolsa;
using Financial.BMF;
using EntitySpaces.Interfaces;

public partial class Consultas_Rentabilidade : ConsultaBasePage
{
    
    new protected void Page_Load(object sender, EventArgs e)
    {
        this.HasPopupCliente = true;
        btnEditCodigoCliente.Focus();
        base.Page_Load(sender, e);
    }

    protected void gridConsulta_Load(object sender, EventArgs e)
    {
        GridViewDataComboBoxColumn tipoAtivo = gridConsulta.Columns["TipoAtivo"] as GridViewDataComboBoxColumn;
        if (tipoAtivo != null)
        {
            tipoAtivo.PropertiesComboBox.Items.Clear();
            foreach (int i in Enum.GetValues(typeof(TipoAtivoAuxiliar)))
            {
                tipoAtivo.PropertiesComboBox.Items.Add(TipoAtivoAuxiliarDescricao.RetornaStringValue(i), i);
            }

            tipoAtivo.PropertiesComboBox.Items.Add("Rentabilidade por Período", (int)TipoAtivoAuxiliar.RentabildiadePeriodo);
        }
    }

    protected void gridAtivo_Load(object sender, EventArgs e)
    {
        GridViewDataComboBoxColumn tipoMercado = gridAtivo.Columns["TipoMercado"] as GridViewDataComboBoxColumn;
        if (tipoMercado != null)
        {
            tipoMercado.PropertiesComboBox.Items.Clear();
            foreach (int i in Enum.GetValues(typeof(TipoAtivoAuxiliar)))
            {
                tipoMercado.PropertiesComboBox.Items.Add(TipoAtivoAuxiliarDescricao.RetornaStringValue(i), i);
            }

        }
    }

    protected void EsDSRentabilidade_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        e.Collection = new RentabilidadeCollection();
        if (!Page.IsPostBack)
        {
            #region Condição Inicial
            gridConsulta.Visible = false;
            #endregion
        }
        else
        {
            RentabilidadeCollection coll = new RentabilidadeCollection();
            RentabilidadeQuery rentabilidadeQuery = new RentabilidadeQuery("rentabilidade");
            ClienteQuery clienteQuery = new ClienteQuery("Cliente");

            #region Filtro
            if (string.IsNullOrEmpty(textDataInicio.Text))            
                return;            

            if (string.IsNullOrEmpty(textDataFim.Text))            
                return;            

            if (string.IsNullOrEmpty(btnEditCodigoCliente.Text))            
                return;            

            DateTime dataInicio = Convert.ToDateTime(textDataInicio.Text);
            DateTime dataFim = Convert.ToDateTime(textDataFim.Text);
            int idCliente = Convert.ToInt32(btnEditCodigoCliente.Text);

            #endregion
            rentabilidadeQuery.Select(rentabilidadeQuery.IdRentabilidade,
                                      rentabilidadeQuery.Data,
                                      rentabilidadeQuery.TipoAtivo,
                                      rentabilidadeQuery.IdCliente,
                                      rentabilidadeQuery.CodigoAtivo,
                                      rentabilidadeQuery.CodigoMoedaAtivo,
                                      rentabilidadeQuery.PatrimonioInicialMoedaAtivo,
                                      rentabilidadeQuery.PatrimonioInicialBrutoMoedaAtivo,
                                      rentabilidadeQuery.QuantidadeInicialAtivo,
                                      rentabilidadeQuery.ValorFinanceiroEntradaMoedaAtivo,
                                      rentabilidadeQuery.QuantidadeTotalEntradaAtivo,
                                      rentabilidadeQuery.ValorFinanceiroSaidaMoedaAtivo,
                                      rentabilidadeQuery.QuantidadeTotalSaidaAtivo,
                                      rentabilidadeQuery.ValorFinanceiroRendasMoedaAtivo,
                                      rentabilidadeQuery.PatrimonioFinalMoedaAtivo,
                                      rentabilidadeQuery.PatrimonioFinalBrutoMoedaAtivo,
                                      rentabilidadeQuery.QuantidadeFinalAtivo,
                                      "< rentabilidade.[RentabilidadeMoedaAtivo] * 100 as RentabilidadeMoedaAtivo>",
                                      rentabilidadeQuery.CodigoMoedaPortfolio,
                                      rentabilidadeQuery.PatrimonioInicialMoedaPortfolio,
                                      rentabilidadeQuery.PatrimonioFinalBrutoMoedaPortfolio,
                                      rentabilidadeQuery.ValorFinanceiroEntradaMoedaPortfolio,
                                      rentabilidadeQuery.ValorFinanceiroSaidaMoedaPortfolio,
                                      rentabilidadeQuery.ValorFinanceiroRendasMoedaPortfolio,
                                      rentabilidadeQuery.PatrimonioFinalMoedaPortfolio,
                                      rentabilidadeQuery.PatrimonioInicialBrutoMoedaPortfolio,
                                      rentabilidadeQuery.PatrimonioFinalBrutoMoedaPortfolio,
                                      "< rentabilidade.[RentabilidadeMoedaPortfolio] * 100 as RentabilidadeMoedaPortfolio>",
                                      rentabilidadeQuery.IdTituloRendaFixa,
                                      rentabilidadeQuery.IdOperacaoSwap,
                                      rentabilidadeQuery.IdCarteira,
                                      rentabilidadeQuery.CdAtivoBolsa,
                                      rentabilidadeQuery.CdAtivoBMF,
                                      rentabilidadeQuery.SerieBMF,
                                      rentabilidadeQuery.PatrimonioInicialGrossUpMoedaAtivo,
                                      rentabilidadeQuery.PatrimonioInicialGrossUpMoedaPortfolio,
                                      rentabilidadeQuery.PatrimonioFinalGrossUpMoedaAtivo,
                                      rentabilidadeQuery.PatrimonioFinalGrossUpMoedaPortfolio,
                                      "< rentabilidade.RentabilidadeGrossUpDiariaMoedaAtivo * 100 as RentabilidadeGrossUpDiariaMoedaAtivo>",
                                      "< rentabilidade.RentabilidadeGrossUpDiariaMoedaPortfolio * 100 as RentabilidadeGrossUpDiariaMoedaPortfolio>",
                                      "< rentabilidade.RentabilidadeAtivosIcentivados * 100 as RentabilidadeAtivosIcentivados>",
                                      "< rentabilidade.RentabilidadeAtivosTributados * 100 as RentabilidadeAtivosTributados>",
                                      "< rentabilidade.RentabilidadeBrutaDiariaMoedaAtivo * 100 as RentabilidadeBrutaDiariaMoedaAtivo>",
                                      "< rentabilidade.RentabilidadeBrutaDiariaMoedaPortfolio * 100 as RentabilidadeBrutaDiariaMoedaPortfolio>",
                                      rentabilidadeQuery.CotaGerencial,
                                      clienteQuery.Apelido);
            rentabilidadeQuery.InnerJoin(clienteQuery).On(clienteQuery.IdCliente.Equal(rentabilidadeQuery.IdCliente));
            rentabilidadeQuery.Where(rentabilidadeQuery.Data.GreaterThanOrEqual(dataInicio)
                                     & rentabilidadeQuery.Data.LessThanOrEqual(dataFim)
                                     & rentabilidadeQuery.IdCliente.Equal(idCliente));

            #region Rentabilidade por Ativo
            if (!string.IsNullOrEmpty(hiddenCompositeKey.Text))
            {
                string[] keys = hiddenCompositeKey.Text.Split('|');
                int tipoMercado = Convert.ToInt32(keys[0]);

                if ((int)TipoAtivoAuxiliar.OperacaoRendaFixa == tipoMercado)
                {
                    rentabilidadeQuery.Where(rentabilidadeQuery.IdTituloRendaFixa.Equal(Convert.ToInt32(keys[1])));
                }
                else if ((int)TipoAtivoAuxiliar.OperacaoBolsa == tipoMercado)
                {
                    rentabilidadeQuery.Where(rentabilidadeQuery.CdAtivoBolsa.Equal(keys[1].ToString()));
                }
                else if ((int)TipoAtivoAuxiliar.OperacaoFundos == tipoMercado)
                {
                    rentabilidadeQuery.Where(rentabilidadeQuery.IdCarteira.Equal(Convert.ToInt32(keys[1])));
                }
                else if ((int)TipoAtivoAuxiliar.OperacaoSwap == tipoMercado)
                {
                    rentabilidadeQuery.Where(rentabilidadeQuery.IdOperacaoSwap.Equal(Convert.ToInt32(keys[1])));
                }
                else if ((int)TipoAtivoAuxiliar.OperacaoBMF == tipoMercado)
                {
                    string[] chaveBMF = keys[1].Split('-');
                    rentabilidadeQuery.Where(rentabilidadeQuery.CdAtivoBMF.Equal(chaveBMF[0]) & rentabilidadeQuery.SerieBMF.Equal(chaveBMF[1]));
                }

                rentabilidadeQuery.OrderBy(rentabilidadeQuery.Data.Ascending);

                decimal valorRentabilidadePeriodoPortifolio = 1;
                decimal valorRentabilidadePeriodoAtivo = 1;
                if (coll.Load(rentabilidadeQuery))
                {
                    string descricaoMoeda = string.Empty;
                    foreach (Rentabilidade rentabilidade in coll)
                    {
                        if (rentabilidade.RentabilidadeMoedaPortfolio.HasValue)
                        {
                            valorRentabilidadePeriodoPortifolio *= (1 + rentabilidade.RentabilidadeMoedaPortfolio.Value);
                        }
                        if (rentabilidade.RentabilidadeMoedaAtivo.HasValue)
                        {
                            valorRentabilidadePeriodoAtivo *= (1 + rentabilidade.RentabilidadeMoedaAtivo.Value);
                        }
                    }

                    valorRentabilidadePeriodoPortifolio -= 1;
                    valorRentabilidadePeriodoAtivo -= 1;

                    Rentabilidade rentabilidadePeriodo = new Rentabilidade();
                    rentabilidadePeriodo.IdCliente = idCliente;
                    rentabilidadePeriodo.CodigoAtivo = "Rentabilidade do Período!";
                    rentabilidadePeriodo.Data = (((List<Rentabilidade>)coll).FindLast(delegate(Rentabilidade x) { return x.Data <= dataFim; })).Data.Value;
                    rentabilidadePeriodo.TipoAtivo = (int)TipoAtivoAuxiliar.RentabildiadePeriodo;
                    rentabilidadePeriodo.RentabilidadeMoedaPortfolio = valorRentabilidadePeriodoPortifolio;
                    rentabilidadePeriodo.RentabilidadeMoedaAtivo = valorRentabilidadePeriodoAtivo;

                    coll.AttachEntity(rentabilidadePeriodo);
                }

                e.Collection = coll;
                return;
            }
            #endregion
            #region rentabilidade por periodo
            else
            {
                rentabilidadeQuery.OrderBy(rentabilidadeQuery.Data.Ascending, rentabilidadeQuery.IdCliente.Ascending, rentabilidadeQuery.TipoAtivo.Ascending);

                RentabilidadeCollection rentabilidadeCollPeriodo = new RentabilidadeCollection();

                decimal valorRentabilidadePeriodoPortifolio = 1;
                decimal valorRentabilidadePeriodoPortifolioGrossUp = 1;
                if (coll.Load(rentabilidadeQuery))
                {
                    rentabilidadeCollPeriodo.Query.Where(rentabilidadeCollPeriodo.Query.Data.GreaterThanOrEqual(dataInicio)
                                 & rentabilidadeCollPeriodo.Query.Data.LessThanOrEqual(dataFim)
                                 & rentabilidadeCollPeriodo.Query.IdCliente.Equal(idCliente)
                                 & rentabilidadeCollPeriodo.Query.TipoAtivo.Equal((int)TipoAtivoAuxiliar.Fundo));
                    rentabilidadeCollPeriodo.Query.OrderBy(rentabilidadeCollPeriodo.Query.Data.Ascending);

                    #region Calculo de Rentabilidade do periodo
                    if (rentabilidadeCollPeriodo.Load(rentabilidadeCollPeriodo.Query))
                    {
                        string descricaoMoeda = string.Empty;
                        foreach (Rentabilidade rentabilidade in rentabilidadeCollPeriodo)
                        {
                            if (rentabilidade.RentabilidadeMoedaPortfolio.HasValue)
                            {
                                rentabilidade.RentabilidadeMoedaPortfolio *= 100;
                                valorRentabilidadePeriodoPortifolio *= (1 + rentabilidade.RentabilidadeMoedaPortfolio.Value);
                            }
                        }

                        valorRentabilidadePeriodoPortifolio -= 1;

                        Rentabilidade rentabilidadePeriodo = new Rentabilidade();
                        rentabilidadePeriodo.IdCliente = idCliente;
                        rentabilidadePeriodo.CodigoAtivo = "Rentabilidade do Período!";
                        rentabilidadePeriodo.Data = (((List<Rentabilidade>)rentabilidadeCollPeriodo).FindLast(delegate(Rentabilidade x) { return x.Data <= dataFim; })).Data.Value;
                        rentabilidadePeriodo.TipoAtivo = (int)TipoAtivoAuxiliar.RentabildiadePeriodo;
                        rentabilidadePeriodo.RentabilidadeMoedaPortfolio = valorRentabilidadePeriodoPortifolio;
                        coll.AttachEntity(rentabilidadePeriodo);

                    }
                    #endregion

                }

                e.Collection = coll;
                return;
            }
            #endregion
        }        
    }

    protected void EsDSAtivo_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        int idCliente = 0;
        DateTime dataInicio = new DateTime();
        DateTime dataFim = new DateTime();
        bool executaBusca = false;
        if (!string.IsNullOrEmpty(btnEditCodigoCliente.Text) && !string.IsNullOrEmpty(textDataFim.Text) && !string.IsNullOrEmpty(textDataInicio.Text))
        {
            idCliente = Convert.ToInt32(btnEditCodigoCliente.Text);
            dataFim = Convert.ToDateTime(textDataFim.Text);
            dataInicio = Convert.ToDateTime(textDataInicio.Text);
            executaBusca = true;
        }

        AtivoMercadoFiltroCollection ativoMercadoColl = new AtivoMercadoFiltroCollection();
        if (executaBusca)
            ativoMercadoColl = ativoMercadoColl.PopulaRetornaTabelaTemporaria(idCliente, true, true, true, true, true, dataInicio, dataFim);

        if (ativoMercadoColl.Count > 0)
            e.Collection = ativoMercadoColl;
        else
            e.Collection = new AtivoMercadoFiltroCollection();
    }

    protected void callBackPopupAtivo_Callback(object source, DevExpress.Web.CallbackEventArgs e)
    {
        e.Result = string.Empty;
        if (!String.IsNullOrEmpty(e.Parameter) && e.Parameter != "null")
        {
            string CompositeKey = Convert.ToString(e.Parameter);

            AtivoMercadoFiltro ativoMercadoFiltro = new AtivoMercadoFiltro();

            ativoMercadoFiltro.Query.Where(ativoMercadoFiltro.Query.CompositeKey.Equal(CompositeKey));

            if (ativoMercadoFiltro.Query.Load())
                e.Result = ativoMercadoFiltro.Descricao;
            //                            
        }
    }

    protected void EsDSCliente_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        ClienteCollection coll = new ClienteCollection();
        coll.BuscaClientesComAcesso(Context.User.Identity.Name);

        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }

    protected void gridConsulta_CustomCallback(object sender, DevExpress.Web.ASPxGridViewCustomCallbackEventArgs e)
    {
        gridConsulta.DataBind();
        gridConsulta.Selection.UnselectAll();
        gridConsulta.CancelEdit();
    }

    protected void ASPxCallback1_Callback(object source, DevExpress.Web.CallbackEventArgs e)
    {
        string resultado = string.Empty;
        if (!String.IsNullOrEmpty(e.Parameter) && e.Parameter != "null")
        {
            int idCliente = Convert.ToInt32(e.Parameter);
            Cliente cliente = new Cliente();
            cliente.LoadByPrimaryKey(idCliente);
            resultado = cliente.str.Apelido;                            
        }
        e.Result = resultado;
    }

    protected void gridConsulta_HtmlRowPrepared(object sender, DevExpress.Web.ASPxGridViewTableRowEventArgs e)
    {
        if (e.GetValue("TipoAtivo") != null)
        {
            int tipoAtivo = Convert.ToInt32(e.GetValue("TipoAtivo").ToString());
            if (tipoAtivo == (int)TipoAtivoAuxiliar.Fundo)
            {
                e.Row.Font.Bold = true;
            }
            else if (tipoAtivo == 99) //Prazo por período
            {
                e.Row. BackColor = Color.LightGray;
            }
        }
    }

    protected void gridAtivo_CustomDataCallback(object sender, ASPxGridViewCustomDataCallbackEventArgs e)
    {
        ASPxGridView gridView = sender as ASPxGridView;
        int visibleIndex = Convert.ToInt32(e.Parameters);
        string CompositeKey = gridView.GetRowValues(visibleIndex, AtivoMercadoFiltroMetadata.ColumnNames.CompositeKey).ToString();
        e.Result = CompositeKey;
    }

    protected void gridAtivo_CustomCallback(object sender, ASPxGridViewCustomCallbackEventArgs e)
    {
        this.gridAtivo.DataBind();
    }


    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridConsulta_CustomUnboundColumnData(object sender, DevExpress.Web.ASPxGridViewColumnDataEventArgs e)
    {
        if (e.Column.FieldName == "CompositeKey")
        {
            //Compose a primary key value
            string TipoAtivo = Convert.ToString(e.GetListSourceFieldValue(RentabilidadeMetadata.ColumnNames.TipoAtivo));
            string IdTituloRendaFixa = Convert.ToString(e.GetListSourceFieldValue(RentabilidadeMetadata.ColumnNames.IdTituloRendaFixa));
            string IdCarteira = Convert.ToString(e.GetListSourceFieldValue(RentabilidadeMetadata.ColumnNames.IdCarteira));
            string IdOperacaoSwap = Convert.ToString(e.GetListSourceFieldValue(RentabilidadeMetadata.ColumnNames.IdOperacaoSwap));
            string CdAtivoBolsa = Convert.ToString(e.GetListSourceFieldValue(RentabilidadeMetadata.ColumnNames.CdAtivoBolsa));
            string CdAtivoBMF = Convert.ToString(e.GetListSourceFieldValue(RentabilidadeMetadata.ColumnNames.CdAtivoBMF));
            string SerieBMF = Convert.ToString(e.GetListSourceFieldValue(RentabilidadeMetadata.ColumnNames.SerieBMF));

            e.Value = TipoAtivo + "|" + IdTituloRendaFixa + IdCarteira + IdOperacaoSwap + CdAtivoBolsa + (!String.IsNullOrEmpty(CdAtivoBMF) ? (CdAtivoBMF + "-" + SerieBMF) : string.Empty);
        }
    }

    /// <summary>
    /// Imprime o Footer
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridConsulta_HtmlFooterCellPrepared(object sender, ASPxGridViewTableFooterCellEventArgs e)
    {
        GridViewDataColumn column = e.Column as GridViewDataColumn;
        if (column == null) return;

        ASPxSummaryItem item = this.gridConsulta.TotalSummary[column.FieldName];
        if (item == null) return;

        decimal summaryValue = Convert.ToDecimal(e.GetSummaryValue(item));

        if (summaryValue < 0)
        {
            e.Cell.ForeColor = Color.Red;
        }

        // Escreve Total na Primeira Coluna
        if (column.FieldName == "IdCliente")
        {
            e.Cell.Text = "Total: ";
        }

        if (this.gridConsulta.VisibleRowCount == 0)
        {
            e.Cell.Text = ""; // Quando tem 0 registros imprime 0.00
        }
    }


    new protected void btnRun_Click(object sender, EventArgs e)
    {
        //this.TrataCamposObrigatorios();
        base.btnRun_Click(sender, e); // DataBind do Grid
    }

    new protected void btnPDF_Click(object sender, EventArgs e)
    {
        //this.TrataCamposObrigatorios();
        this.SelecionaRelatorio(VisaoRelatorio.visaoPDF);
    }

    new protected void btnExcel_Click(object sender, EventArgs e)
    {
        //this.TrataCamposObrigatorios();
        this.SelecionaRelatorio(VisaoRelatorio.visaoExcel);
    }

    /// <summary>
    /// Seleciona o Relatorio a Ser Executado
    /// </summary>
    /// <param name="visao">PDF, Excel ou Report</param>
    private void SelecionaRelatorio(string visao)
    {
        Response.BufferOutput = true;
        //
        // Salva os Parametros na Session
        Session["gridExportRentabilidade"] = gridExport;
        //Response.Redirect("~/Relatorios/Fundo/RentabilidadeExporta.aspx?Visao=" + visao);

        string url = "~/Relatorios/Fundo/RentabilidadeExporta.aspx?Visao=" + visao;
        string redirectURL = Page.ResolveClientUrl(url);

        string script = "window.location = '" + redirectURL + "';";
        ScriptManager.RegisterStartupScript(this, typeof(Page), "RedirectTo", script, true);
    }

    /// <summary>
    /// Trata os campos Obrigatorios do Form
    /// </summary>
    private void TrataCamposObrigatorios()
    {
        #region Campos obrigatórios
        List<Control> controles = new List<Control>(new Control[] { this.textDataFim,this.textDataInicio, this.btnEditCodigoCliente });

        if (base.TestaObrigatorio(controles) != "")
        {
            throw new Exception("Campos com * são obrigatórios!");
        }
        #endregion
    }
}