﻿using Financial.Web.Common;
using Financial.Bolsa;
using System.Data;
using System;
using DevExpress.Web;
using System.Web.UI.WebControls;
using DevExpress.Utils;
using DevExpress.Data;
using Financial.Captacao.Custom;
using System.Web;
using System.Web.UI;
using System.Collections.Generic;
using Financial.Fundo;

public partial class AnaliseAgrupadaAtivos : ConsultaBasePage
{
    // guarda os dados
    private DataTable data = null;
    
    /// <summary>
    /// Pre-Render do Grid. Faz a Consulta e Salva na Session
    /// 3 evento a ser executado
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Grid_OnPreRender(object sender, EventArgs e) {
        this.data = null;

        this.data = this.CreateData();
        this.RemoveColumns();
        this.AddGroupTotalSummary(this.data);
        //
        this.gridCadastro.DataSource = this.data;
        this.gridCadastro.KeyFieldName = "Id";
        //
        this.gridCadastro.DataBind();

        //
        this.ControlaBotaoColapsaExpande();
        //
        Session["DataSourceGroup"] = this.data;            
    }
    
    /// <summary>
    /// 
    /// </summary>
    private void ControlaBotaoColapsaExpande() {
        #region Aparece com os botões colapsa/expande/excel se tem dados no grid
        Control c1 = this.FindControl("btnColapsa");
        c1.Visible = true;

        Control c2 = this.FindControl("btnExpande");
        c2.Visible = true;

        Control c3 = this.FindControl("btnExcel");
        c3.Visible = true;
       
        #endregion
    }

    // Remove da Session - 2 evento a executar
    protected void Page_PreRender() {
        if (Session["DataSourceGroup"] != null) {
            Session.Remove("DataSourceGroup");            
        }
    }
               
    // 1 evento a executar - quando clico para expandir só entra aqui.
    new protected void Page_Load(object sender, EventArgs e) {
        base.Page_Load(sender, e);

        if (Session["DataSourceGroup"] != null) {
            this.gridCadastro.DataSource = Session["DataSourceGroup"];
            this.gridCadastro.KeyFieldName = "Id";
            this.gridCadastro.DataBind();
        }
    }

    #region CallBack Excel
    /// <summary>
    /// 
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void callbackExporta_Callback(object source, DevExpress.Web.CallbackEventArgs e) {
        e.Result = "";

        Response.BufferOutput = true;

        Session["streamGridExport"] = this.gridExport;

        // TODO mudar isso se mudar o nome da pagina
        string newLocation = HttpContext.Current.Request.Url.AbsolutePath.Replace("AnaliseAgrupadaAtivos.aspx", "ExibeAnaliseAgrupadaAtivos.aspx");
        e.Result = "redirect:" + newLocation;
    }    
    #endregion

    /// <summary>
    /// Remove as colunas Valor se existirem
    /// </summary>
    private void RemoveColumns()
    {
        // Remove as colunas Valor GroupSummary se existirem
        for (int i = this.gridCadastro.GroupSummary.Count - 1; i >= 0; i--)
        {
            string fieldName = this.gridCadastro.GroupSummary[i].FieldName;
            if (fieldName.Contains("SaldoBruto") ||
                fieldName.Contains("Tributos") ||
                fieldName.Contains("SaldoLiquido") ||
                fieldName.Contains("Rendimento") ||
                fieldName.Contains("Percentual"))
            {
                this.gridCadastro.GroupSummary.RemoveAt(i);
            }
        }

        // Remove as colunas TotalSummary se existirem
        for (int i = this.gridCadastro.TotalSummary.Count - 1; i >= 0; i--)
        {
            string fieldName = this.gridCadastro.TotalSummary[i].FieldName;
            if (fieldName.Contains("SaldoBruto") ||
                fieldName.Contains("Tributos") ||
                fieldName.Contains("SaldoLiquido") ||
                fieldName.Contains("Rendimento") ||
                fieldName.Contains("Percentual"))
            {
                this.gridCadastro.TotalSummary.RemoveAt(i);
            }
        }
    }

    private void AddGroupTotalSummaryColumn(string columnName) {

        ASPxSummaryItem item = new ASPxSummaryItem();
        item.FieldName = columnName;
        item.SummaryType = SummaryItemType.Sum;

        item.DisplayFormat = columnName == "Percentual" 
                             ? "{0:#,##0.0000%;(#,##0.0000%);0.0000%}" 
                             : "{0:#,##0.00;(#,##0.00);0.00}";
        
        item.ShowInGroupFooterColumn = columnName;
        
        this.gridCadastro.GroupSummary.Add(item);


        ASPxSummaryItem itemTotalSummary = new ASPxSummaryItem();
        itemTotalSummary.FieldName = columnName;
        itemTotalSummary.SummaryType = SummaryItemType.Sum;

        itemTotalSummary.DisplayFormat = columnName == "Percentual" 
                                         ? "{0:#,##0.00%;(#,##0.00%);0.00%}"
                                         : "{0:#,##0.00;(#,##0.00);0.00}";
        
        this.gridCadastro.TotalSummary.Add(itemTotalSummary);
    }

    /// <summary>
    /// Adiciona GroupSummary e TotalSummary da coluna Valor Dinamicamente
    /// </summary>
    /// <param name="d"></param>
    private void AddGroupTotalSummary(DataTable d)
    {

        this.AddGroupTotalSummaryColumn("SaldoBruto");
        this.AddGroupTotalSummaryColumn("Tributos");
        this.AddGroupTotalSummaryColumn("SaldoLiquido");
        this.AddGroupTotalSummaryColumn("Rendimento");
        this.AddGroupTotalSummaryColumn("Percentual");
    }

    
    /// <summary>
    /// DataTable no Formato Id, Descricao1, Descricao2, Descricao3 IdCliente, Valor1, Valor2, ...
    /// </summary>
    /// <returns></returns>
    private DataTable CreateData() {
        DateTime data = Convert.ToDateTime(this.textData.Text);
                        
        ConsultaAtivos.TipoAgrupamento tipoAgrupamento = (ConsultaAtivos.TipoAgrupamento)Enum.Parse(typeof(ConsultaAtivos.TipoAgrupamento), this.dropTipoAgrupamento.SelectedItem.Value.ToString());            
        
        ConsultaAtivos consultaAtivos = new ConsultaAtivos(data, HttpContext.Current.User.Identity.Name, 
                                                            chkRendaVariavel.Checked, chkFundos.Checked, chkRendaFixa.Checked);

        DataTable dt = consultaAtivos.RetornaSaldoCliente(tipoAgrupamento);

        // Adiciona Coluna Percentual
        dt.Columns.Add("Percentual", typeof(System.Decimal));

        #region Preenche Percentual
        //decimal? saldoTotal = null;

        //if (dt.Rows.Count != 0) {
        //    saldoTotal = 0.00M;
        //    for (int i = 0; i < dt.Rows.Count; i++) {
        //        decimal saldoTotal = +Convert.ToDecimal(dt.Rows[i]["SaldoBruto"]);
        //    }
        //}

        decimal? saldoTotal = null;
        if (dt.Rows.Count != 0) {
            saldoTotal = Convert.ToDecimal(dt.Compute("Sum(SaldoBruto)", ""));

            for (int i = 0; i < dt.Rows.Count; i++) {
                if (saldoTotal.HasValue && saldoTotal != 0) {
                    decimal saldoBruto = Convert.ToDecimal(dt.Rows[i]["SaldoBruto"]);
                    //
                    dt.Rows[i]["Percentual"] = saldoBruto / saldoTotal;
                }
            }
        }
        #endregion

        return dt;
    }

    new protected void btnRun_Click(object sender, EventArgs e) {

        #region Campos obrigatórios
        List<Control> controles = new List<Control>(new Control[] { this.textData, this.dropTipoAgrupamento });

        if (base.TestaObrigatorio(controles) != "") {
            throw new Exception("Campos com * são obrigatórios!");
        }
        #endregion

        DateTime data = Convert.ToDateTime(this.textData.Text);

        //
        this.gridCadastro.Visible = true; // torna Vísivel - vai acionar o metodo Grid_OnPreRender
    }
}