﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Collections.Generic;
using System.Text;

using log4net;
using EntitySpaces.Interfaces;
using DevExpress.Web;

using Financial.Web.Util;
using Financial.Util;
using Financial.Web.Common;
using Financial.Common.Enums;

public partial class FiltroReportDemonstrativoConsolidadoCarteiras : FiltroReportBasePage {
    private static readonly ILog log = LogManager.GetLogger(typeof(FiltroReportDemonstrativoConsolidadoCarteiras));

    new protected void Page_Load(object sender, EventArgs e) {
    }

    protected void btnVisualiza_Click(object sender, EventArgs e) {
        this.TrataErros();
        this.SelecionaRelatorio(VisaoRelatorio.visaoHTML);
    }

    protected void btnPDF_Click(object sender, EventArgs e) {
        this.TrataErros();
        this.SelecionaRelatorio(VisaoRelatorio.visaoPDF);
    }

    protected void btnExcel_Click(object sender, EventArgs e) {
        this.TrataErros();
        this.SelecionaRelatorio(VisaoRelatorio.visaoExcel);
    }

    /// <summary>
    /// Trata os Erros de Relatorio
    /// </summary>
    private void TrataErros() {

        #region Campos obrigatórios
        List<Control> controles = new List<Control>(new Control[] { 
    	    this.textData });

        if (base.TestaObrigatorio(controles) != "") {
            throw new Exception("Campos com * são obrigatórios!");
        }
        #endregion

        if (textData.Text != "" && Utilitario.IsDate(textData.Text)) {
            if (!Calendario.IsDiaUtil(Convert.ToDateTime(textData.Text), LocalFeriadoFixo.Brasil, TipoFeriado.Brasil)) {
                throw new Exception("Dia não útil");
            }
        }
    }

    /// <summary>
    /// Seleciona o relatorio a ser executado
    /// </summary>
    /// <param name="visao">PDF, Excel ou Report</param>
    private void SelecionaRelatorio(string visao) {
        Response.BufferOutput = true;
        //
        // Salva os parametros na session
        Session["textData"] = textData.Text;

        Response.Redirect("~/Relatorios/Fundo/ReportDemonstrativoConsolidadoCarteiras.aspx?Visao=" + visao);
    }
}