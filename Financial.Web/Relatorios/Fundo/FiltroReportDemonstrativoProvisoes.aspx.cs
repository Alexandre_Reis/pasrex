﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using Financial.Fundo;
using Financial.Investidor;
using Financial.Security;
using Financial.Investidor.Enums;
using DevExpress.Web;
using System.Text;
using Financial.Web.Common;
using System.Collections.Generic;

public partial class FiltroReportDemonstrativoProvisoes : FiltroReportBasePage {

    new protected void Page_Load(object sender, EventArgs e) {
        btnEditCodigo.Text = labelMensagem.Text;

        if (!Page.IsPostBack && !Page.IsCallback) {
            UtilitarioGrid.GridFilterContains(gridTabela);
        }
    }

    protected void ASPxCallback1_Callback(object source, DevExpress.Web.CallbackEventArgs e) {
        string texto = "";
        if (!String.IsNullOrEmpty(e.Parameter) && e.Parameter != "null") {
            int idTabela = Convert.ToInt32(e.Parameter);
            TabelaProvisaoQuery tabelaProvisao = new TabelaProvisaoQuery("T");
            CarteiraQuery carteira = new CarteiraQuery("C");
            CadastroProvisaoQuery cadastroProvisao = new CadastroProvisaoQuery("P");
            tabelaProvisao.Select(tabelaProvisao.DataReferencia,
                                  carteira.IdCarteira,
                                  carteira.Apelido,
                                  cadastroProvisao.Descricao);
            tabelaProvisao.InnerJoin(cadastroProvisao).On(tabelaProvisao.IdCadastro == cadastroProvisao.IdCadastro);
            tabelaProvisao.InnerJoin(carteira).On(tabelaProvisao.IdCarteira == carteira.IdCarteira);
            tabelaProvisao.Where(tabelaProvisao.IdTabela == idTabela);

            TabelaProvisaoCollection tabelaProvisaoCollection = new TabelaProvisaoCollection();
            tabelaProvisaoCollection.Load(tabelaProvisao);

            if (tabelaProvisaoCollection.HasData) {
                TabelaProvisao tabelaProvisaoAux = new TabelaProvisao();

                string nome = (string)tabelaProvisaoCollection[0].GetColumn(CarteiraMetadata.ColumnNames.Apelido);
                string descricao = (string)tabelaProvisaoCollection[0].GetColumn(CadastroProvisaoMetadata.ColumnNames.Descricao);
                DateTime dataReferencia = (DateTime)tabelaProvisaoCollection[0].GetColumn(TabelaProvisaoMetadata.ColumnNames.DataReferencia);

                texto = ' ' + nome + " ->   " + labelProvisao.Text + descricao + " ->   " + labelDataReferencia.Text + dataReferencia.ToString("d");
            }
        }
        e.Result = texto;
    }

    protected void gridTabela_CustomDataCallback(object sender, DevExpress.Web.ASPxGridViewCustomDataCallbackEventArgs e) {
        e.Result = gridTabela.GetRowValues(Convert.ToInt32(e.Parameters), "IdTabela") +
                        "|" + gridTabela.GetRowValues(Convert.ToInt32(e.Parameters), "IdCarteira");
    }

    protected void gridTabela_DataBound(object sender, EventArgs e) {
        gridTabela.FocusedRowIndex = -1;
    }

    protected void EsDSTabelaProvisao_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e) {
        TabelaProvisaoQuery tabelaProvisaoQuery = new TabelaProvisaoQuery("T");
        CarteiraQuery carteiraQuery = new CarteiraQuery("C");
        CadastroProvisaoQuery cadastroProvisaoQuery = new CadastroProvisaoQuery("A");
        PermissaoClienteQuery permissaoClienteQuery = new PermissaoClienteQuery("P");
        UsuarioQuery usuarioQuery = new UsuarioQuery("U");
        ClienteQuery clienteQuery = new ClienteQuery("E");
        tabelaProvisaoQuery.Select(tabelaProvisaoQuery.IdTabela,
                              tabelaProvisaoQuery.DataReferencia,
                              carteiraQuery.IdCarteira,
                              carteiraQuery.Apelido,
                              cadastroProvisaoQuery.IdCadastro,
                              cadastroProvisaoQuery.Descricao);
        tabelaProvisaoQuery.InnerJoin(cadastroProvisaoQuery).On(tabelaProvisaoQuery.IdCadastro == cadastroProvisaoQuery.IdCadastro);
        tabelaProvisaoQuery.InnerJoin(carteiraQuery).On(tabelaProvisaoQuery.IdCarteira == carteiraQuery.IdCarteira);
        tabelaProvisaoQuery.InnerJoin(permissaoClienteQuery).On(permissaoClienteQuery.IdCliente == carteiraQuery.IdCarteira);
        tabelaProvisaoQuery.InnerJoin(usuarioQuery).On(permissaoClienteQuery.IdUsuario == usuarioQuery.IdUsuario);
        tabelaProvisaoQuery.InnerJoin(clienteQuery).On(carteiraQuery.IdCarteira == clienteQuery.IdCliente);
        tabelaProvisaoQuery.Where(usuarioQuery.Login == HttpContext.Current.User.Identity.Name,
                               clienteQuery.TipoControle.NotEqual(TipoControleCliente.ApenasCotacao),
                               clienteQuery.StatusAtivo.Equal(StatusAtivoCliente.Ativo));
        tabelaProvisaoQuery.OrderBy(clienteQuery.Apelido.Ascending);


        TabelaProvisaoCollection coll = new TabelaProvisaoCollection();
        coll.Load(tabelaProvisaoQuery);

        // Assign the esDataSourceSelectEvenArgs Collection property
        e.Collection = coll;
    }

    protected void gridTabela_HtmlRowCreated(object sender, DevExpress.Web.ASPxGridViewTableRowEventArgs e) {
        e.Row.Attributes.Add("onmouseover", "backgroundColor = this.style.backgroundColor; this.style.backgroundColor='#E0E9EB';");
        e.Row.Attributes.Add("onmouseout", "this.style.backgroundColor=backgroundColor;");
    }

    protected void btnVisualiza_Click(object sender, EventArgs e) {
        this.TrataErros();
        this.SelecionaRelatorio(VisaoRelatorio.visaoHTML);
    }

    protected void btnPDF_Click(object sender, EventArgs e) {
        this.TrataErros();
        this.SelecionaRelatorio(VisaoRelatorio.visaoPDF);
    }

    protected void btnExcel_Click(object sender, EventArgs e) {
        this.TrataErros();
        this.SelecionaRelatorio(VisaoRelatorio.visaoExcel);
    }

    /// <summary>
    /// Trata os Erros de Relatorio
    /// </summary>
    private void TrataErros() {

        #region Campos obrigatórios
        List<Control> controles = new List<Control>(new Control[] { 
    	    this.btnEditCodigo, this.textDataInicio, this.textDataFim });

        if (base.TestaObrigatorio(controles) != "") {
            throw new Exception("Campos com * são obrigatórios!");
        }
        #endregion
    }

    /// <summary>
    /// Seleciona o relatorio a ser executado
    /// </summary>
    /// <param name="visao">PDF, Excel ou Report</param>
    private void SelecionaRelatorio(string visao) {
        Response.BufferOutput = true;
        //
        // Salva os parametros na session
        Session["textDataInicio"] = this.textDataInicio.Text;
        Session["textDataFim"] = this.textDataFim.Text;
        Session["hiddenIdTabela"] = this.hiddenIdTabela.Text;
        Session["hiddenIdCarteira"] = this.hiddenIdCarteira.Value; //vem do aspx
        //        
        //Response.Redirect("~/Relatorios/Fundo/ReportDemonstrativoProvisoes.aspx?Visao=" + visao);

        string url = "~/Relatorios/Fundo/ReportDemonstrativoProvisoes.aspx?Visao=" + visao;
        string redirectURL = Page.ResolveClientUrl(url);

        string script = "window.location = '" + redirectURL + "';";
        ScriptManager.RegisterStartupScript(this, typeof(Page), "RedirectTo", script, true);

    }
}