﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Financial.Fundo;
using Financial.Investidor;
using Financial.Security;
using Financial.Investidor.Enums;
using System.Text;
using DevExpress.Web;
using Financial.Web.Common;
using System.Collections.Generic;

public partial class FiltroReportDemonstrativoTaxas : FiltroReportBasePage {    
    new protected void Page_Load(object sender, EventArgs e) {
        btnEditCodigo.Text = labelMensagem.Text;

        if (!Page.IsPostBack && !Page.IsCallback) {
            UtilitarioGrid.GridFilterContains(gridTabela);
        }
    }

    protected void ASPxCallback1_Callback(object source, DevExpress.Web.CallbackEventArgs e) {
        string texto = "";
        if (!String.IsNullOrEmpty(e.Parameter) && e.Parameter != "null") {
            #region Procura o idTabela se o Parameter for do tipo Int
            //
            //int idTabela;
            //if (int.TryParse(e.Parameter, out idTabela)) {
            int idTabela = Convert.ToInt32(e.Parameter);
            TabelaTaxaAdministracaoQuery tabelaTaxas = new TabelaTaxaAdministracaoQuery("T");
            CarteiraQuery carteira = new CarteiraQuery("C");
            CadastroTaxaAdministracaoQuery cadastroTaxas = new CadastroTaxaAdministracaoQuery("P");
            tabelaTaxas.Select(tabelaTaxas.IdTabela,
                                  tabelaTaxas.DataReferencia,
                                  carteira.IdCarteira,
                                  carteira.Apelido,
                                  cadastroTaxas.IdCadastro,
                                  cadastroTaxas.Descricao);
            tabelaTaxas.InnerJoin(cadastroTaxas).On(tabelaTaxas.IdCadastro == cadastroTaxas.IdCadastro);
            tabelaTaxas.InnerJoin(carteira).On(tabelaTaxas.IdCarteira == carteira.IdCarteira);
            tabelaTaxas.Where(tabelaTaxas.IdTabela == idTabela);

            TabelaTaxaAdministracaoCollection tabelaTaxaAdministracaoCollection = new TabelaTaxaAdministracaoCollection();
            tabelaTaxaAdministracaoCollection.Load(tabelaTaxas);

            //
            if (tabelaTaxaAdministracaoCollection.HasData) {
                //int idTabelaAux = (int)tabelaTaxaAdministracaoCollection[0].GetColumn(TabelaTaxaAdministracaoMetadata.ColumnNames.IdTabela);                
                string nome = (string)tabelaTaxaAdministracaoCollection[0].GetColumn(CarteiraMetadata.ColumnNames.Apelido);
                string descricao = (string)tabelaTaxaAdministracaoCollection[0].GetColumn(CadastroTaxaAdministracaoMetadata.ColumnNames.Descricao);
                DateTime dataReferencia = (DateTime)tabelaTaxaAdministracaoCollection[0].GetColumn(TabelaTaxaAdministracaoMetadata.ColumnNames.DataReferencia);

                texto = ' ' + nome + " ->   " + labelTaxa.Text + descricao + " ->   " + labelDataReferencia.Text + dataReferencia.ToString("d");
            }
            //}
            #endregion
        }
        //
        e.Result = texto;
    }

    protected void gridTabela_CustomDataCallback(object sender, DevExpress.Web.ASPxGridViewCustomDataCallbackEventArgs e) {
        e.Result = gridTabela.GetRowValues(Convert.ToInt32(e.Parameters), "IdTabela") +
                        "|" + gridTabela.GetRowValues(Convert.ToInt32(e.Parameters), "IdCarteira");
    }

    protected void gridTabela_DataBound(object sender, EventArgs e) {
        gridTabela.FocusedRowIndex = -1;
    }

    protected void EsDSTabelaTaxas_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e) {
        TabelaTaxaAdministracaoQuery tabelaTaxasQuery = new TabelaTaxaAdministracaoQuery("T");
        CarteiraQuery carteiraQuery = new CarteiraQuery("C");
        CadastroTaxaAdministracaoQuery cadastroTaxasQuery = new CadastroTaxaAdministracaoQuery("A");
        PermissaoClienteQuery permissaoClienteQuery = new PermissaoClienteQuery("P");
        UsuarioQuery usuarioQuery = new UsuarioQuery("U");
        ClienteQuery clienteQuery = new ClienteQuery("E");
        tabelaTaxasQuery.Select(tabelaTaxasQuery.IdTabela,
                              tabelaTaxasQuery.DataReferencia,
                              carteiraQuery.IdCarteira,
                              carteiraQuery.Apelido,
                              cadastroTaxasQuery.IdCadastro,
                              cadastroTaxasQuery.Descricao);
        tabelaTaxasQuery.InnerJoin(cadastroTaxasQuery).On(tabelaTaxasQuery.IdCadastro == cadastroTaxasQuery.IdCadastro);
        tabelaTaxasQuery.InnerJoin(carteiraQuery).On(tabelaTaxasQuery.IdCarteira == carteiraQuery.IdCarteira);
        tabelaTaxasQuery.InnerJoin(permissaoClienteQuery).On(permissaoClienteQuery.IdCliente == carteiraQuery.IdCarteira);
        tabelaTaxasQuery.InnerJoin(usuarioQuery).On(permissaoClienteQuery.IdUsuario == usuarioQuery.IdUsuario);
        tabelaTaxasQuery.InnerJoin(clienteQuery).On(carteiraQuery.IdCarteira == clienteQuery.IdCliente);
        tabelaTaxasQuery.Where(usuarioQuery.Login == HttpContext.Current.User.Identity.Name,
                               clienteQuery.TipoControle.NotEqual(TipoControleCliente.ApenasCotacao),
                               clienteQuery.StatusAtivo.Equal(StatusAtivoCliente.Ativo));
        tabelaTaxasQuery.OrderBy(clienteQuery.Apelido.Ascending);

        TabelaTaxaAdministracaoCollection coll = new TabelaTaxaAdministracaoCollection();
        coll.Load(tabelaTaxasQuery);

        // Assign the esDataSourceSelectEvenArgs Collection property
        e.Collection = coll;
    }

    protected void gridTabela_HtmlRowCreated(object sender, DevExpress.Web.ASPxGridViewTableRowEventArgs e) {
        e.Row.Attributes.Add("onmouseover", "backgroundColor = this.style.backgroundColor; this.style.backgroundColor='#E0E9EB';");
        e.Row.Attributes.Add("onmouseout", "this.style.backgroundColor=backgroundColor;");
    }

    protected void btnVisualiza_Click(object sender, EventArgs e) {
        this.TrataErros();
        this.SelecionaRelatorio(VisaoRelatorio.visaoHTML);
    }

    protected void btnPDF_Click(object sender, EventArgs e) {
        this.TrataErros();
        this.SelecionaRelatorio(VisaoRelatorio.visaoPDF);
    }

    protected void btnExcel_Click(object sender, EventArgs e) {
        this.TrataErros();
        this.SelecionaRelatorio(VisaoRelatorio.visaoExcel);
    }

    /// <summary>
    /// Trata os Erros de Relatorio
    /// </summary>
    private void TrataErros() {

        #region Campos obrigatórios
        List<Control> controles = new List<Control>(new Control[] { 
    	    this.btnEditCodigo, this.textDataInicio, this.textDataFim });

        if (base.TestaObrigatorio(controles) != "") {
            throw new Exception("Campos com * são obrigatórios!");
        }
        #endregion
    }

    /// <summary>
    /// Seleciona o relatorio a ser executado
    /// </summary>
    /// <param name="visao">PDF, Excel ou Report</param>
    private void SelecionaRelatorio(string visao) {
        Response.BufferOutput = true;
        //
        // Salva os parametros na session
        Session["textDataInicio"] = this.textDataInicio.Text;
        Session["textDataFim"] = this.textDataFim.Text;
        Session["hiddenIdTabela"] = this.hiddenIdTabela.Text;
        Session["hiddenIdCarteira"] = this.hiddenIdCarteira.Value; //vem do aspx

        //Response.Redirect("~/Relatorios/Fundo/ReportDemonstrativoTaxas.aspx?Visao=" + visao);


        string url = "~/Relatorios/Fundo/ReportDemonstrativoTaxas.aspx?Visao=" + visao;
        string redirectURL = Page.ResolveClientUrl(url);

        string script = "window.location = '" + redirectURL + "';";
        ScriptManager.RegisterStartupScript(this, typeof(Page), "RedirectTo", script, true);
    }
}