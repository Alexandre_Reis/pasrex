﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using Financial.Fundo;
using Financial.Investidor;
using System.Collections.Generic;
using Financial.Web.Util;
using Financial.Investidor.Enums;
using Financial.Security;
using log4net;
using Financial.Security.Enums;
using EntitySpaces.Interfaces;
using DevExpress.Web;
using System.Text;
using Financial.Web.Common;

public partial class FiltroReportAlocacaoDistribuicaoFundos : FiltroReportBasePage {
    private static readonly ILog log = LogManager.GetLogger(typeof(FiltroReportAlocacaoDistribuicaoFundos));

    new protected void Page_Load(object sender, EventArgs e) {
        TrataTravamentoCampos();

        btnEditCodigo.Focus();

        this.HasPopupCliente = true;
        base.Page_Load(sender, e);
    }

    private void TrataTravamentoCampos()
    {
        try
        {
            //Chegar se foi passado um IdCliente como parametro
            string idClienteParam = Request["idCliente"];
            bool temClienteQueryString = !String.IsNullOrEmpty(idClienteParam);
            int? idClienteQueryString = null;

            if (temClienteQueryString)
            {
                idClienteQueryString = Convert.ToInt32(idClienteParam);

                //Garantir que usuario tem acesso ao cliente
                bool temAcesso = ChecaUsuarioAcessaCliente(idClienteQueryString.Value);

                if (!temAcesso)
                {
                    throw new Exception("Usuário não possui acesso à carteira " + idClienteParam);
                }
            }

            Usuario usuario = new Usuario();
            usuario.BuscaUsuario(HttpContext.Current.User.Identity.Name);
            int tipoTrava = usuario.TipoTrava.Value;

            if (tipoTrava == (int)TipoTravaUsuario.TravaCliente)
            {
                btnEditCodigo.Enabled = false;
            }

            if (temClienteQueryString)
            {
                PreencheCamposCarteira(idClienteQueryString.Value);
            }
            else if (tipoTrava == (int)TipoTravaUsuario.TravaCliente)
            {
                PermissaoCliente permissaoCliente = new PermissaoCliente();
                int? idCarteira = permissaoCliente.RetornaClienteAssociado(HttpContext.Current.User.Identity.Name);

                if (idCarteira.HasValue)
                {
                    PreencheCamposCarteira(idCarteira.Value);
                }
            }
        }
        catch (Exception exception)
        {
            new Financial.Web.Util.ErrorHandler().HandleArrayIndexError(exception);
        }
    }

    protected bool ChecaUsuarioAcessaCliente(int idCliente)
    {
        ClienteCollection clienteCollection = new ClienteCollection();
        clienteCollection.BuscaClientesComAcesso(HttpContext.Current.User.Identity.Name);

        bool temAcesso = false;
        foreach (Cliente cliente in clienteCollection)
        {
            if (cliente.IdCliente.Value == idCliente)
            {
                temAcesso = true;
                break;
            }
        }

        return temAcesso;
    }

    protected void PreencheCamposCarteira(int idCarteira)
    {
        Cliente cliente = new Cliente();

        List<esQueryItem> campos = new List<esQueryItem>();
        campos.Add(cliente.Query.Apelido);
        cliente.LoadByPrimaryKey(campos, idCarteira);
        string apelido = cliente.Apelido;

        btnEditCodigo.Text = Convert.ToString(idCarteira);
        textNome.Text = apelido;

    }


    protected void ASPxCallback1_Callback(object source, DevExpress.Web.CallbackEventArgs e) {
        string nome = "";
        if (!String.IsNullOrEmpty(e.Parameter) && e.Parameter != "null") {
            int idCliente = Convert.ToInt32(e.Parameter);
            Cliente cliente = new Cliente();
            cliente.LoadByPrimaryKey(idCliente);

            if (cliente.str.Apelido != "") {
                if (cliente.IsAtivo) {
                    PermissaoCliente permissaoCliente = new PermissaoCliente();
                    nome = permissaoCliente.RetornaAcessoClienteComControle(idCliente, HttpContext.Current.User.Identity.Name)
                            ? cliente.str.Apelido : "no_access";                                       
                }
                else {
                    nome = "no_active";
                }
            }
        }
        e.Result = nome;
    }

    protected void EsDSCliente_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e) {
        ClienteCollection clienteCollection = new ClienteCollection();
        clienteCollection.BuscaClientesComAcesso(HttpContext.Current.User.Identity.Name);

        e.Collection = clienteCollection;
    }

    protected void btnVisualiza_Click(object sender, EventArgs e) {
        this.TrataErros();
        this.SelecionaRelatorio(VisaoRelatorio.visaoHTML);
    }

    protected void btnPDF_Click(object sender, EventArgs e) {
        this.TrataErros();
        this.SelecionaRelatorio(VisaoRelatorio.visaoPDF);
    }

    protected void btnExcel_Click(object sender, EventArgs e) {
        this.TrataErros();
        this.SelecionaRelatorio(VisaoRelatorio.visaoExcel);
    }

    /// <summary>
    /// Trata os Erros de Relatorio
    /// </summary>
    private void TrataErros() {

        #region Campos obrigatórios
        List<Control> controles = new List<Control>(new Control[] { 
    	    this.btnEditCodigo, this.textDataInicio, this.textDataFim });

        if (base.TestaObrigatorio(controles) != "") {
            throw new Exception("Campos com * são obrigatórios!");
        }
        #endregion
    }

    /// <summary>
    /// Seleciona o relatorio a ser executado
    /// </summary>
    /// <param name="visao">PDF, Excel ou Report</param>
    private void SelecionaRelatorio(string visao) {
        Response.BufferOutput = true;
        //
        // Salva os parametros na session
        Session["textDataInicio"] = textDataInicio.Text;
        Session["textDataFim"] = textDataFim.Text;
        Session["btnEditCodigo"] = btnEditCodigo.Text;
        //
        //Response.Redirect("~/Relatorios/Fundo/ReportAlocacaoDistribuicaoFundos.aspx?Visao=" + visao);

        string url = "~/Relatorios/Fundo/ReportAlocacaoDistribuicaoFundos.aspx?Visao=" + visao;
        string redirectURL = Page.ResolveClientUrl(url);

        string script = "window.location = '" + redirectURL + "';";
        ScriptManager.RegisterStartupScript(this, typeof(Page), "RedirectTo", script, true);
    }
}