using System;
using Financial.Relatorio;
using System.IO;
using System.Web;
using System.Globalization;
using Financial.Web.Common;
using Financial.Fundo;

public partial class _ReportFluxoCaixaSintetico : Financial.Web.Common.BasePage {

    private enum TipoRelatorio 
    {        
        SinteticoGeral = 1,
        SinteticoCliente = 2
    }

    new protected void Page_Load(object sender, EventArgs e) {
        string visao = Request.QueryString["Visao"].ToString();
        //
        
        DateTime dataReferencia = new DateTime();
        int? idCliente = null;
        int? tipoCliente = null;
        
        // Obrigatorio
        dataReferencia = Convert.ToDateTime(Session["textData"]);

        if (!String.IsNullOrEmpty(Convert.ToString(Session["btnEditCodigo"]))) {
            idCliente = Convert.ToInt32(Session["btnEditCodigo"]);
        }

        if (!String.IsNullOrEmpty(Convert.ToString(Session["dropTipoCliente"])))
        {
            tipoCliente = Convert.ToInt32(Session["dropTipoCliente"]);
        }

        int tipo = Convert.ToInt32(Session["tipoRelatorio"]);

        Carteira.BooleanosFluxoCaixa booleanosFluxoCaixa = new Carteira.BooleanosFluxoCaixa();
        booleanosFluxoCaixa.PosicaoBolsa = Convert.ToBoolean(Session["chkLiquidezBolsa"]);
        booleanosFluxoCaixa.PosicaoFundos = Convert.ToBoolean(Session["chkLiquidezFundos"]);
        booleanosFluxoCaixa.RendaFixa = Convert.ToBoolean(Session["chkVencimentoTitulos"]);
        booleanosFluxoCaixa.ResgatesNaoConvertidos = Convert.ToBoolean(Session["chkResgatesNaoCotizados"]);

        string t = tipo == (int)TipoRelatorio.SinteticoCliente ? "Cliente" : "Geral";
        string c = idCliente.HasValue ? idCliente.Value.ToString() : "";

        string fileName = "CaixaSintetico" + t + "_" + c + "_" +
                              dataReferencia.Day + "-" + dataReferencia.Month + "-" + dataReferencia.Year;

        #region tipo Relatorio SinteticoCliente
        if (tipo == (int)TipoRelatorio.SinteticoCliente) {
            if (visao == "Report") {
                ReportViewer1.Report = new ReportFluxoCaixaSintetico(idCliente.Value, dataReferencia, booleanosFluxoCaixa);
            }
            else if (visao == "PDF") {
                #region PDF
                ReportFluxoCaixaSintetico report = new ReportFluxoCaixaSintetico(idCliente.Value, dataReferencia, booleanosFluxoCaixa);

                MemoryStream ms = new MemoryStream();
                report.ExportToPdf(ms);
                ms.Seek(0, SeekOrigin.Begin);
                Response.ClearContent();
                Response.ClearHeaders();
                Response.Buffer = true;
                Response.Cache.SetCacheability(HttpCacheability.Private);
                Response.ContentType = "application/pdf";
                Response.AddHeader("Content-Length", ms.Length.ToString(CultureInfo.CurrentCulture));
                Response.AddHeader("Content-Disposition", String.Format("attachment;filename=" + fileName + ".pdf"));

                Response.BinaryWrite(ms.ToArray());
                ms.Close();
                HttpContext.Current.ApplicationInstance.CompleteRequest();
                Response.End();
                #endregion
            }
            else if (visao == "Excel") {
                #region Excel
                ReportFluxoCaixaSintetico report = new ReportFluxoCaixaSintetico(idCliente.Value, dataReferencia, booleanosFluxoCaixa);

                MemoryStream ms = new MemoryStream();
                report.ExportToXls(ms);
                ms.Seek(0, SeekOrigin.Begin);
                Response.ClearContent();
                Response.ClearHeaders();
                Response.Buffer = true;
                Response.Cache.SetCacheability(HttpCacheability.Private);
                Response.ContentType = "application/vnd.ms-excel";
                Response.AddHeader("Content-Length", ms.Length.ToString(CultureInfo.CurrentCulture));
                Response.AddHeader("Content-Disposition", String.Format("attachment;filename=" + fileName + ".xls"));

                Response.BinaryWrite(ms.ToArray());
                ms.Close();
                HttpContext.Current.ApplicationInstance.CompleteRequest();
                Response.End();

                #endregion
            }
        }
        #endregion

        #region tipo Relatorio SinteticoGeral
        else if (tipo == (int)TipoRelatorio.SinteticoGeral) {
            if (visao == "Report") {
                ReportViewer1.Report = new ReportFluxoCaixaSinteticoGeral(idCliente, dataReferencia, tipoCliente, booleanosFluxoCaixa);
            }
            else if (visao == "PDF") {
                #region PDF
                ReportFluxoCaixaSinteticoGeral report = new ReportFluxoCaixaSinteticoGeral(idCliente, dataReferencia, tipoCliente, booleanosFluxoCaixa);

                MemoryStream ms = new MemoryStream();
                report.ExportToPdf(ms);
                ms.Seek(0, SeekOrigin.Begin);
                Response.ClearContent();
                Response.ClearHeaders();
                Response.Buffer = true;
                Response.Cache.SetCacheability(HttpCacheability.Private);
                Response.ContentType = "application/pdf";
                Response.AddHeader("Content-Length", ms.Length.ToString(CultureInfo.CurrentCulture));
                Response.AddHeader("Content-Disposition", String.Format("attachment;filename=" + fileName + ".pdf"));

                Response.BinaryWrite(ms.ToArray());
                ms.Close();
                HttpContext.Current.ApplicationInstance.CompleteRequest();
                Response.End();
                #endregion
            }
            else if (visao == "Excel") {
                #region Excel
                ReportFluxoCaixaSinteticoGeral report = new ReportFluxoCaixaSinteticoGeral(idCliente, dataReferencia, tipoCliente, booleanosFluxoCaixa);

                MemoryStream ms = new MemoryStream();
                report.ExportToXls(ms);
                ms.Seek(0, SeekOrigin.Begin);
                Response.ClearContent();
                Response.ClearHeaders();
                Response.Buffer = true;
                Response.Cache.SetCacheability(HttpCacheability.Private);
                Response.ContentType = "application/vnd.ms-excel";
                Response.AddHeader("Content-Length", ms.Length.ToString(CultureInfo.CurrentCulture));
                Response.AddHeader("Content-Disposition", String.Format("attachment;filename=" + fileName + ".xls"));

                Response.BinaryWrite(ms.ToArray());
                ms.Close();
                HttpContext.Current.ApplicationInstance.CompleteRequest();
                Response.End();

                #endregion
            }
        }
        #endregion
    }
}