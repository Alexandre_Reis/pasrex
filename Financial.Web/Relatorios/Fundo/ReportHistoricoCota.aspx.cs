﻿using System;
using Financial.Relatorio;
using System.IO;
using System.Web;
using System.Globalization;
using Financial.Web.Common;

public partial class _ReportHistoricoCota : BasePage {

    /* Como usar:
     *  int idCarteira = 13712;        
        //
        DateTime dataInicio = new DateTime(2006, 1, 1);
        DateTime dataFim = new DateTime(2010, 1, 1);
        //        
        ReportViewer1.Report = new ReportHistoricoCota(idCarteira, dataInicio, dataFim);
     */
    new protected void Page_Load(object sender, EventArgs e) {
        string visao = Request.QueryString["Visao"].ToString();
        //
        //Response.Write(Session["textDataInicio"]);
        //Response.Write("<br>" + Session["textDataFim"]);
        //Response.Write("<br>" + Session["btnEditCodigo"]);

        // Parametros vem da session - obrigatorios
        DateTime dataInicio = Convert.ToDateTime(Session["textDataInicio"]);
        DateTime dataFim = Convert.ToDateTime(Session["textDataFim"]);
        int idCarteira = Convert.ToInt32(Session["btnEditCodigo"]);
        // Clear Session - não pode remover da session pois relatorio em PDF usa da session
        //Session.Remove("textDataInicio");
        //Session.Remove("textDataFim");
        //Session.Remove("btnEditCodigo");
        //
        if (visao == "Report") {
            ReportViewer1.Report = new ReportHistoricoCota(idCarteira, dataInicio, dataFim);
        }
        else if (visao == "PDF") {
            #region PDF
            ReportHistoricoCota report = new ReportHistoricoCota(idCarteira, dataInicio, dataFim);

            MemoryStream ms = new MemoryStream();
            report.ExportToPdf(ms);
            ms.Seek(0, SeekOrigin.Begin);
            Response.ClearContent();
            Response.ClearHeaders();
            Response.Buffer = true;
            Response.Cache.SetCacheability(HttpCacheability.Private);
            Response.ContentType = "application/pdf";
            Response.AddHeader("Content-Length", ms.Length.ToString(CultureInfo.CurrentCulture));            
            Response.AddHeader("Content-Disposition", String.Format("attachment;filename=ReportHistoricoCota.pdf"));

            Response.BinaryWrite(ms.ToArray());
            ms.Close();
            HttpContext.Current.ApplicationInstance.CompleteRequest();
            Response.End();
            #endregion
        }
        else if (visao == "Excel") {
            #region Excel
            ReportHistoricoCota report = new ReportHistoricoCota(idCarteira, dataInicio, dataFim);

            MemoryStream ms = new MemoryStream();
            report.ExportToXls(ms);
            ms.Seek(0, SeekOrigin.Begin);
            Response.ClearContent();
            Response.ClearHeaders();
            Response.Buffer = true;
            Response.Cache.SetCacheability(HttpCacheability.Private);
            Response.ContentType = "application/vnd.ms-excel";
            Response.AddHeader("Content-Length", ms.Length.ToString(CultureInfo.CurrentCulture));
            Response.AddHeader("Content-Disposition", String.Format("attachment;filename=ReportHistoricoCota.xls"));

            Response.BinaryWrite(ms.ToArray());
            ms.Close();
            HttpContext.Current.ApplicationInstance.CompleteRequest();
            Response.End();

            #endregion
        }
    }
}
