using System;
using Financial.Relatorio;
using System.IO;
using System.Web;
using System.Globalization;
using Financial.Web.Common;
public partial class _ReportFluxoCaixaAnalitico : Financial.Web.Common.BasePage
{
    /* Como usar:
        int idCliente = 1;
        DateTime dataFim = new DateTime(2010, 1, 1);

        ReportViewer1.Report = new ReportFluxoCaixaAnalitico(idCliente, dataInicio, dataFim);     
     */
    new protected void Page_Load(object sender, EventArgs e)
    {
        string visao = Request.QueryString["Visao"].ToString();
        //
        //Response.Write(Request.Form["textDataInicio"].ToString());
        //Response.Write("<br>" + Request.Form["btnEditCodigo"].ToString());

        DateTime? dataFim = new DateTime();
        int? idCliente = null;

        // Obrigatorio
        if ((string)Session["textDataFim"] != "")
        {
            dataFim = Convert.ToDateTime(Session["textDataFim"]);
        }

        idCliente = Convert.ToInt32(Session["btnEditCodigo"]);

        if (visao == "Report")
        {
            ReportViewer1.Report = new ReportFluxoCaixaAnalitico(idCliente.Value, dataFim);
        }
        else if (visao == "PDF")
        {
            #region PDF
            ReportFluxoCaixaAnalitico report = new ReportFluxoCaixaAnalitico(idCliente.Value, dataFim);

            MemoryStream ms = new MemoryStream();
            report.ExportToPdf(ms);
            ms.Seek(0, SeekOrigin.Begin);
            Response.ClearContent();
            Response.ClearHeaders();
            Response.Buffer = true;
            Response.Cache.SetCacheability(HttpCacheability.Private);
            Response.ContentType = "application/pdf";
            Response.AddHeader("Content-Length", ms.Length.ToString(CultureInfo.CurrentCulture));
            Response.AddHeader("Content-Disposition", String.Format("attachment;filename=ReportFluxoCaixaAnalitico.pdf"));

            Response.BinaryWrite(ms.ToArray());
            ms.Close();
            HttpContext.Current.ApplicationInstance.CompleteRequest();
            Response.End();
            #endregion
        }
        else if (visao == "Excel")
        {
            #region Excel
            ReportFluxoCaixaAnalitico report = new ReportFluxoCaixaAnalitico(idCliente.Value, dataFim);

            MemoryStream ms = new MemoryStream();
            report.ExportToXls(ms);
            ms.Seek(0, SeekOrigin.Begin);
            Response.ClearContent();
            Response.ClearHeaders();
            Response.Buffer = true;
            Response.Cache.SetCacheability(HttpCacheability.Private);
            Response.ContentType = "application/vnd.ms-excel";
            Response.AddHeader("Content-Length", ms.Length.ToString(CultureInfo.CurrentCulture));
            Response.AddHeader("Content-Disposition", String.Format("attachment;filename=ReportFluxoCaixaAnalitico.xls"));

            Response.BinaryWrite(ms.ToArray());
            ms.Close();
            HttpContext.Current.ApplicationInstance.CompleteRequest();
            Response.End();

            #endregion
        }
    }
}