﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="BookImprime.aspx.cs" Inherits="Consultas_BookImprime" %>

<%@ Register Assembly="System.Web, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"
    Namespace="System.Web.UI.HtmlControls" TagPrefix="cc2" %>

<%@ Register Assembly="EntitySpaces.Web" Namespace="EntitySpaces.Web" TagPrefix="cc1" %>

<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxe" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxwgv" %>

<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxpc" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxcb" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxlp" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">    
    <link href="~/css/forms.css" rel="stylesheet" type="text/css" />
    <link rel="SHORTCUT ICON" href="~/imagensPersonalizadas/favicon.ico" type="image/x-icon" />
    
    <script type="text/javascript" language="Javascript" src="../js/global.js"></script>    
    <script language="JavaScript">
    var operacao = '';
    var envia;
    function OnGetDataCliente(values) {
        btnEditCodigoCliente.SetValue(values);                    
        popupCliente.HideWindow();
        ASPxCallback1.SendCallback(btnEditCodigoCliente.GetValue());        
        btnEditCodigoCliente.Focus();    
    }
    
    function btnExportaClick(){              
       LoadingPanel1.Show();
       callbackExportaExcel.SendCallback();
    }
    </script>
</head>
<body>
    <form id="form1" runat="server">
        
        <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="true" EnableScriptLocalization="true"/>
        
        <dxcb:ASPxCallback ID="callbackRelatorio" runat="server" OnCallback="callbackRelatorio_Callback">
        <ClientSideEvents CallbackComplete="function(s, e) 
        {             
            if (e.result != '')
            {                   
                alert(e.result);                              
            }    
            else
            {
                if (envia == 'pdf')
                {
                    __doPostBack('btnPDF_hidden');
                }
                else
                {
                    LoadingPanel.Show();
                    callbackEmail.SendCallback();
                }
            }        
            operacao = '';
        }        
        "/>
        </dxcb:ASPxCallback>
        
        <dxcb:ASPxCallback ID="callbackEmail" runat="server" OnCallback="callbackEmail_Callback">
        <ClientSideEvents CallbackComplete="function(s, e) 
        {             
            if (e.result != '')
            {   
                LoadingPanel.Hide();                 
                alert(e.result);                              
            }            
        }        
        "/>
        </dxcb:ASPxCallback>
        
        <dxcb:ASPxCallback ID="callbackExportaExcel" runat="server" OnCallback="callbackExportaExcel_Callback">
        <ClientSideEvents CallbackComplete="function(s, e) 
        {   
            LoadingPanel1.Hide();
            if(e && e.result.length){
                if(e.result.indexOf('redirect') === 0){
                    var newLocation = (e.result.split(':'))[1];
                    
                    var iframeId = 'iframe_download';
                    var iframe = document.getElementById(iframeId);

                    if (!iframe && document.createElement && (iframe = document.createElement('iframe'))) {
               
                        iframe.name = iframe.id = iframeId;
                        iframe.width = 0;
                        iframe.height = 0;
                        iframe.src = 'about:blank';
                        document.body.appendChild(iframe);                                                                    
                    }
                    iframe.src = newLocation;
                   
                } else {
                    alert(e.result);
                }
            }                    
        }" />
    </dxcb:ASPxCallback>
        
        <asp:TextBox style="display:none" ID="textMsgNaoExiste" runat="server" Text="Cliente não existe"></asp:TextBox>
        <asp:TextBox style="display:none" ID="textMsgUsuarioSemAcesso" runat="server" Text="Usário sem acesso a este cliente"></asp:TextBox> 
        <asp:TextBox style="display:none" ID="textMsgInativo" runat="server" Text="Cliente inativo"></asp:TextBox>
        
        <dxpc:ASPxPopupControl ID="popupMensagemCliente" ShowHeader="false" PopupElementID="btnEditCodigoCliente" CloseAction="OuterMouseClick" 
                ForeColor="Red" PopupVerticalAlign="Middle" PopupHorizontalAlign="OutsideRight" Left="100"
                PopupAction="None" HeaderText="" runat="server">
        </dxpc:ASPxPopupControl>
        
        <dxcb:ASPxCallback ID="ASPxCallback1" runat="server" OnCallback="ASPxCallback1_Callback">
            <ClientSideEvents CallbackComplete="function(s, e) {   
                OnCallBackCompleteCliente(s, e, popupMensagemCliente, btnEditCodigoCliente, document.getElementById('textNome'));
            }        
            "/>
        </dxcb:ASPxCallback>
        
        <dxpc:ASPxPopupControl ID="popupCliente" runat="server" Width="500px" HeaderText="" ContentStyle-VerticalAlign="Top"  
                        PopupVerticalAlign="Middle" PopupHorizontalAlign="OutsideRight" AllowDragging="True">
        <ContentCollection><dxpc:PopupControlContentControl runat="server">     
            <div>
                <dxwgv:ASPxGridView ID="gridCliente" runat="server" Width="100%"
                        ClientInstanceName="gridCliente"  AutoGenerateColumns="False" 
                        DataSourceID="EsDSCliente" KeyFieldName="IdCliente"
                        OnCustomDataCallback="gridCliente_CustomDataCallback" 
                        OnHtmlRowCreated="gridCliente_HtmlRowCreated">               
                <Columns>
                    <dxwgv:GridViewDataTextColumn FieldName="IdCliente" ReadOnly="True" VisibleIndex="0" Width="20%"/>
                    <dxwgv:GridViewDataTextColumn FieldName="Apelido" VisibleIndex="1" Width="80%" Settings-AutoFilterCondition="Contains" />                    
                </Columns>
                
                <Settings ShowFilterRow="True" ShowTitlePanel="True"/>            
                <SettingsBehavior ColumnResizeMode="Disabled" />
                
                <ClientSideEvents RowDblClick="function(s, e) {
                    gridCliente.GetValuesOnCustomCallback(e.visibleIndex, OnGetDataCliente);}" Init="function(s, e) {
	                e.cancel = true;
	                }"
	            />
    	        
                <SettingsDetail ShowDetailButtons="False" />
                <Styles AlternatingRow-Enabled="True" Cell-Wrap="False">
                    <Header ImageSpacing="5px" SortingImageSpacing="5px"/>
                </Styles>
                <Images>
                    <PopupEditFormWindowClose Height="17px" Width="17px" />
                </Images>
                <SettingsText EmptyDataRow="0 Registros" Title="Pesquisa Cliente" />
                </dxwgv:ASPxGridView>    
            
            </div>      
            </dxpc:PopupControlContentControl></ContentCollection>
            <ClientSideEvents CloseUp="function(s, e) {gridCliente.ClearFilter(); }" />
        </dxpc:ASPxPopupControl>
        
        <div class="divPanel">
        <table width="100%"><tr><td>
        <div id="container_small">
        
        <div id="header">
            <asp:Label ID="lblHeader" runat="server" Text="Book de Relatórios - Impressão"></asp:Label>
        </div>
            
        <div id="mainContent">   
            
            <div class="reportFilter">
                <div class="dataMessage">
                
            </div>                                      
                                            
            <table cellpadding="2" cellspacing="2">
                <tr>
                <td class="td_Label">
                    <asp:Label ID="labelCliente" runat="server" CssClass="labelNormal" Text="Cliente:"></asp:Label>
                </td>
                
                <td>
                    <dxe:ASPxSpinEdit ID="btnEditCodigoCliente" runat="server" CssClass="textButtonEdit" 
                                        EnableClientSideAPI="True" ClientInstanceName="btnEditCodigoCliente" 
                                        MaxLength="9" NumberType="Integer" AllowMouseWheel="false">  
                    <Buttons>
                        <dxe:EditButton/>
                    </Buttons>        
                    <ClientSideEvents
                             KeyPress="function(s, e) {document.getElementById('textNome').value = '';}" 
                             ButtonClick="function(s, e) {popupCliente.ShowAtElementByID(s.name);}" 
                             LostFocus="function(s, e) {OnLostFocus(popupMensagemCliente, ASPxCallback1, btnEditCodigoCliente);}"
                    />                            
                    </dxe:ASPxSpinEdit>                
                </td>                          
                
                <td>
                    <asp:TextBox ID="textNome" runat="server" CssClass="textNome" Enabled="false"></asp:TextBox>                    
                </td>
                </tr>
            </table>
            
            <table cellpadding="2" cellspacing="2">
                <tr>
                    <td class="td_Label">
                        <asp:Label ID="labelBook" runat="server" CssClass="labelNormal" Text="Book:"></asp:Label>
                    </td>
                    <td colspan="2">    
                        <dxe:ASPxComboBox ID="dropBook" runat="server" ClientInstanceName="dropBook"
                                    DataSourceID="EsDSBook"  ShowShadow="false" 
                                    DropDownStyle="DropDown" IncrementalFilteringMode="Contains" 
                                    CssClass="dropDownListLongo3" TextField="Descricao" ValueField="IdBook" >
                            <ClientSideEvents 
                                LostFocus="function(s, e) {if(s.GetSelectedIndex() == -1) 
                                                                s.SetText(null);}" >
                            </ClientSideEvents> 
                        </dxe:ASPxComboBox>         
                    </td>
                    <td>
                        <dxe:ASPxCheckBox ID="checkSenha" runat="server" Text="PDF com senha no email" />
                    </td>
                </tr>                
                                                
                <tr>
                <td class="td_Label">                    
                    <asp:Label ID="labelData" runat="server" CssClass="labelRequired" Text="Data:"></asp:Label>
                </td>                        
                <td colspan="2">                              
                    <dxe:ASPxDateEdit ID="textData" runat="server" ClientInstanceName="textData" />
                </td>  
                
                </tr>
            </table>
            
            <div class="linkButton linkButtonNoBorder" style="margin-left:40px;margin-top:20px" >
               <asp:LinkButton ID="btnPDF_hidden" runat="server" Visible="false" OnClick="btnPDF_Click" /> 
               
               
               <asp:LinkButton ID="btnPDF" runat="server" Font-Overline="false" CssClass="btnPdf"
                                        OnClientClick="envia = 'pdf'; callbackRelatorio.SendCallback(); return false;"><asp:Literal ID="Literal1" runat="server" Text="Gerar PDF"/><div></div></asp:LinkButton>
               
                              
               <asp:LinkButton ID="btnExcel" runat="server" Font-Overline="false" CssClass="btnExcel" Visible="true"
                    OnClientClick="btnExportaClick();return false;">
                    <asp:Literal ID="Literal5" runat="server" Text="Gerar Excel" /><div>
                    </div>
               </asp:LinkButton>

                              
               <asp:LinkButton ID="btnEmail" runat="server" Font-Overline="false" CssClass="btnEmail"
                                        OnClientClick="
                                                    if (operacao != '') {                                                     
                                                        return false;
                                                    }
                                                    else {                                                        
                                                        operacao='enviar'; 
                                                        envia = 'email'; 
                                                        callbackRelatorio.SendCallback(); 
                                                        return false;
                                                    }                                                    
                                                    "><asp:Literal ID="Literal2" runat="server" Text="Enviar Email"/><div></div></asp:LinkButton>
            </div>                      
            </div>
                            
        </div>
        </div>
        </td></tr></table>
        </div>   
        
        <cc1:esDataSource ID="EsDSBook" runat="server" OnesSelect="EsDSBook_esSelect" />
        <cc1:esDataSource ID="EsDSCliente" runat="server" OnesSelect="EsDSCliente_esSelect" />
        
        <dxlp:ASPxLoadingPanel ID="LoadingPanel" runat="server" Text="Enviando Email, aguarde..." ClientInstanceName="LoadingPanel" Modal="True"/>            
        <dxlp:ASPxLoadingPanel ID="LoadingPanel1" runat="server" Text="Exportando arquivo, aguarde..." ClientInstanceName="LoadingPanel1" Modal="true" />
        
    </form>
</body>
</html>