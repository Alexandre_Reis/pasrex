﻿using Financial.Web.Common;
using log4net;
using System;
using System.Collections.Generic;
using System.Web.UI;
using System.Web;
using Financial.Common;
using Financial.Investidor;
using Financial.Security;

public partial class FiltroReportCalculoGerencial : FiltroReportBasePage {
    private static readonly ILog log = LogManager.GetLogger(typeof(FiltroReportCalculoGerencial));

    private enum TipoRelatorio {
        GerencialCota = 1,
        GerencialTrades = 2
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    new protected void Page_Load(object sender, EventArgs e) {
        this.HasPopupCliente = true;
        base.Page_Load(sender, e);
        //
        textData.Focus();
    }

    protected void EsDSCliente_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e) {
        ClienteCollection clienteCollection = new ClienteCollection();
        clienteCollection.BuscaClientesComAcesso(HttpContext.Current.User.Identity.Name);

        e.Collection = clienteCollection;
    }

    protected void EsDSTrader_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e) {
        TraderCollection traderCollection = new TraderCollection();
        //
        traderCollection.Query
                        .Select(traderCollection.Query.IdTrader, traderCollection.Query.Nome)
                        .OrderBy(traderCollection.Query.Nome.Ascending);

        traderCollection.Query.Load();
        //
        Trader t = new Trader();
        t.IdTrader = null;
        t.Nome = "";

        traderCollection.AttachEntity(t);
        //
        traderCollection.Sort = TraderMetadata.ColumnNames.Nome + " ASC";
        //
        e.Collection = traderCollection;
    }

    protected void ASPxCallback1_Callback(object source, DevExpress.Web.CallbackEventArgs e) {
        string nome = "";
        if (!String.IsNullOrEmpty(e.Parameter) && e.Parameter != "null") {
            int idCliente = Convert.ToInt32(e.Parameter);
            Cliente cliente = new Cliente();
            cliente.LoadByPrimaryKey(idCliente);

            if (cliente.str.Apelido != "") {
                if (cliente.IsAtivo) {
                    PermissaoCliente permissaoCliente = new PermissaoCliente();
                    nome = permissaoCliente.RetornaAcessoClienteComControle(idCliente, HttpContext.Current.User.Identity.Name)
                            ? cliente.str.Apelido : "no_access";
                }
                else {
                    nome = "no_active";
                }
            }
        }
        e.Result = nome;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnVisualiza_Click(object sender, EventArgs e) {
        this.TrataErros();
        this.SelecionaRelatorio(VisaoRelatorio.visaoHTML);
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnPDF_Click(object sender, EventArgs e) {
        this.TrataErros();
        this.SelecionaRelatorio(VisaoRelatorio.visaoPDF);
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnExcel_Click(object sender, EventArgs e) {
        this.TrataErros();
        this.SelecionaRelatorio(VisaoRelatorio.visaoExcel);
    }


    /// <summary>
    /// Trata os Erros de Relatorio
    /// </summary>
    private void TrataErros() {

        #region Campos obrigatórios
        List<Control> controles = new List<Control>(new Control[] { this.textData });

        if (base.TestaObrigatorio(controles) != "") {
            throw new Exception("Campos com * são obrigatórios!");
        }
        #endregion
    }

    /// <summary>
    /// Seleciona o relatorio a ser executado
    /// </summary>
    /// <param name="visao">PDF, Excel ou Report</param>
    private void SelecionaRelatorio(string visao) {
        Response.BufferOutput = true;
        //
        // Salva os parametros na session
        Session["textData"] = textData.Text;
        Session["dropTipoRelatorio"] = (TipoRelatorio)Enum.Parse(typeof(TipoRelatorio), this.dropTipoRelatorio.Value.ToString());
        Session["btnEditCodigo"] = btnEditCodigo.Text;

        Session["dropTrader"] = this.dropTrader.SelectedItem.Value;
        Session["dropZerado"] = this.dropZerado.SelectedItem.Value;

        //Response.Redirect("~/Relatorios/Gerencial/ReportCalculoGerencial.aspx?Visao=" + visao);

        string url = "~/Relatorios/Gerencial/ReportCalculoGerencial.aspx?Visao=" + visao;
        string redirectURL = Page.ResolveClientUrl(url);

        string script = "window.location = '" + redirectURL + "';";
        ScriptManager.RegisterStartupScript(this, typeof(Page), "RedirectTo", script, true);
    }
}