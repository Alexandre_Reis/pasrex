using System;
using Financial.Relatorio;
using System.IO;
using System.Web;
using System.Globalization;
using Financial.Web.Common;

public partial class _ReportCalculoGerencial : BasePage {
    private static class VisaoRelatorio {
        public static string visaoHTML = "Report";
        public static string visaoPDF = "PDF";
        public static string visaoExcel = "Excel";
    }

    //private enum TipoRelatorio {
    //    GerencialCota = 1,
    //    GerencialTrades = 2
    //}

    new protected void Page_Load(object sender, EventArgs e) {
        string visao = Request.QueryString["Visao"].ToString();

        int? idCliente = null;
        int? idTrader = null;

        if (!String.IsNullOrEmpty((string)Session["btnEditCodigo"])) {
            idCliente = Convert.ToInt32(Session["btnEditCodigo"]);
        }
        if (Session["dropTrader"] != null) {
            idTrader = Convert.ToInt32(Session["dropTrader"]);
        }

        bool mostraZerado = Convert.ToString(Session["dropZerado"]) == "S";

        // Obrigatorio
        DateTime dataReferencia = Convert.ToDateTime(Session["textData"]);
        ReportCalculoGerencial.TipoRelatorio tipoRelatorio = (ReportCalculoGerencial.TipoRelatorio)Convert.ToInt32(Session["dropTipoRelatorio"]);

        if (visao == VisaoRelatorio.visaoHTML) {
            ReportViewer1.Report = new ReportCalculoGerencial(dataReferencia, tipoRelatorio, idCliente, idTrader, mostraZerado);
        }
        else if (visao == VisaoRelatorio.visaoPDF) {
            #region PDF
            ReportCalculoGerencial report = new ReportCalculoGerencial(dataReferencia, tipoRelatorio, idCliente, idTrader, mostraZerado);

            MemoryStream ms = new MemoryStream();
            report.ExportToPdf(ms);
            ms.Seek(0, SeekOrigin.Begin);
            Response.ClearContent();
            Response.ClearHeaders();
            Response.Buffer = true;
            Response.Cache.SetCacheability(HttpCacheability.Private);
            Response.ContentType = "application/pdf";
            Response.AddHeader("Content-Length", ms.Length.ToString(CultureInfo.CurrentCulture));
            Response.AddHeader("Content-Disposition", String.Format("attachment;filename=ReportCalculoGerencial.pdf"));

            Response.BinaryWrite(ms.ToArray());
            ms.Close();
            HttpContext.Current.ApplicationInstance.CompleteRequest();
            Response.End();
            #endregion
        }
        else if (visao == VisaoRelatorio.visaoExcel) {
            #region Excel
            ReportCalculoGerencial report = new ReportCalculoGerencial(dataReferencia, tipoRelatorio, idCliente, idTrader, mostraZerado);

            MemoryStream ms = new MemoryStream();
            report.ExportToXls(ms);
            ms.Seek(0, SeekOrigin.Begin);
            Response.ClearContent();
            Response.ClearHeaders();
            Response.Buffer = true;
            Response.Cache.SetCacheability(HttpCacheability.Private);
            Response.ContentType = "application/vnd.ms-excel";
            Response.AddHeader("Content-Length", ms.Length.ToString(CultureInfo.CurrentCulture));
            Response.AddHeader("Content-Disposition", String.Format("attachment;filename=ReportCalculoGerencial.xls"));

            Response.BinaryWrite(ms.ToArray());
            ms.Close();
            HttpContext.Current.ApplicationInstance.CompleteRequest();
            Response.End();

            #endregion
        }

    }
}
