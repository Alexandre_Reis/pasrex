﻿using System;
using Financial.Web.Common;
using Financial.InvestidorCotista;
using Financial.Util;
using Financial.Relatorio;
using System.IO;
using System.Web;
using System.Globalization;
using System.Collections.Generic;
using Financial.WebConfigConfiguration;
using System.Web.Hosting;
using FileHelpers;
using System.Text;
using EntitySpaces.Interfaces;
using Financial.CRM;
using Financial.Fundo;
using Financial.Common;
using Financial.Common.Enums;
using Financial.Fundo.Exceptions;
using Financial.Common.Exceptions;
using Financial.Investidor;
using Financial.Fundo.Enums;

public partial class _ReportExtratoCotista : BasePage {

    #region Exportação TXT Extrato
    public class DataHolderTXT {

        public const string REGISTRO4_SALDOANTERIOR = "AA";
        public const string REGISTRO4_APLICACAO = "AP";
        public const string REGISTRO4_SALDOATUAL = "AT";
        public const string REGISTRO4_RESGATE = "RG";

        // Define se consulta será histórica ou não para registro 4
        private enum TipoPesquisa {
            PosicaoCotista = 0,
            PosicaoCotistaHistorico = 1
        }

        [FixedLengthRecord()]
        public class Registro1 {
            [FieldFixedLength(1)]
            public int TipoRegistro;

            [FieldFixedLength(8)]
            public string Zeros;

            [FieldFixedLength(50)]
            public string NomeCliente;

            [FieldFixedLength(6)]
            [FieldAlign(AlignMode.Right, '0')]
            public int CodigoCliente;

            [FieldFixedLength(50)] // não pode ser vazio
            public string Branco1;

            [FieldFixedLength(20)]
            public string Branco2;

            [FieldFixedLength(32)] // não pode ser vazio
            public string Branco3;

            [FieldFixedLength(8)] // não pode ser vazio
            public string Branco4;

            [FieldFixedLength(2)]
            public string Branco5;

            [FieldFixedLength(2)]
            public string UF;

            [FieldFixedLength(10)]
            [FieldConverter(ConverterKind.Date, "dd/MM/yyyy")]
            public DateTime DataInicio;

            [FieldFixedLength(10)]
            [FieldConverter(ConverterKind.Date, "dd/MM/yyyy")]
            public DateTime DataFim;

            [FieldFixedLength(10)]
            [FieldConverter(ConverterKind.Date, "dd/MM/yyyy")]
            public DateTime DataReferencia;
        }

        [FixedLengthRecord()]  /* Patrimonio */
        public class Registro2 {
            [FieldFixedLength(1)]
            public int TipoRegistro;
            
            [FieldFixedLength(6)]
            [FieldAlign(AlignMode.Right, '0')]
            public int CodigoFundo;

            [FieldFixedLength(2)]
            public string Zeros;

            [FieldFixedLength(52)]
            public string NomeFundo;

            [FieldFixedLength(16)]
            [FieldConverter(typeof(EightDecimalConverter))]
            public decimal ValorCota;

            [FieldFixedLength(19)]
            [FieldConverter(typeof(EightDecimalConverter))]
            public decimal QuantidadeCota;

            [FieldFixedLength(18)]
            [FieldConverter(typeof(TwoDecimalConverter))]
            public decimal SaldoBruto;

            [FieldFixedLength(18)]
            [FieldConverter(typeof(TwoDecimalConverter))]
            public decimal IR;

            [FieldFixedLength(18)]
            [FieldConverter(typeof(TwoDecimalConverter))]
            public decimal IOF;

            [FieldFixedLength(18)]
            [FieldConverter(typeof(TwoDecimalConverter))]
            public decimal SaldoLiquido;

            #region Converter 2 e 8 Digitos
            internal class TwoDecimalConverter : ConverterBase {
                // Converte de string para decimal
                public override object StringToField(string from) {
                    return Convert.ToDecimal(from);
                }

                // Converte de decimal para string
                public override string FieldToString(object from) {
                    decimal res = Convert.ToDecimal(from);
                    return res.ToString("N2");
                }
            }

            internal class EightDecimalConverter : ConverterBase {
                // Converte de string para decimal
                public override object StringToField(string from) {
                    return Convert.ToDecimal(from);
                }

                // Converte de decimal para string
                public override string FieldToString(object from) {
                    decimal res = Convert.ToDecimal(from);
                    return res.ToString("N8");
                }
            }
            #endregion
        }

        [FixedLengthRecord()] /* Total Patrimonio */
        public class Registro6 {
            [FieldFixedLength(1)]
            public int TipoRegistro;

            [FieldFixedLength(6)] // não pode ser vazio
            public string Branco;

            [FieldFixedLength(2)]
            public string Zeros;

            [FieldFixedLength(82)]
            public string TotalPatrimonio;

            [FieldFixedLength(18)]
            [FieldConverter(typeof(TwoDecimalConverter))]
            public decimal TotalSaldoBruto;

            [FieldFixedLength(18)]
            [FieldConverter(typeof(TwoDecimalConverter))]
            public decimal TotalIR;

            [FieldFixedLength(18)]
            [FieldConverter(typeof(TwoDecimalConverter))]
            public decimal TotalIOF;

            [FieldFixedLength(18)]
            [FieldConverter(typeof(TwoDecimalConverter))]
            public decimal TotalSaldoLiquido;

            #region Converter 2
            internal class TwoDecimalConverter : ConverterBase {
                // Converte de string para decimal
                public override object StringToField(string from) {
                    return Convert.ToDecimal(from);
                }

                // Converte de decimal para string
                public override string FieldToString(object from) {
                    decimal res = Convert.ToDecimal(from);
                    return res.ToString("N2");
                }
            }            
            #endregion
        }

        [FixedLengthRecord()] /* Indicadores Performance */
        public class Registro3 {
            [FieldFixedLength(1)]
            public int TipoRegistro;

            [FieldFixedLength(6)]
            [FieldAlign(AlignMode.Right, '0')]
            public int CodigoFundo;

            [FieldFixedLength(2)]
            public string Zeros;

            [FieldFixedLength(25)]
            public string NomeFundo;
                   
            [FieldFixedLength(9)]
            [FieldConverter(typeof(TwoDecimalConverter))]
            public decimal RentabilidadeMesFundo;

            [FieldFixedLength(1)]
            public string Simbolo1;

            [FieldFixedLength(9)]
            [FieldConverter(typeof(TwoDecimalConverter))]
            public decimal RentabilidadeMesCDI;

            [FieldFixedLength(1)]
            public string Simbolo2;

            [FieldFixedLength(9)]
            [FieldConverter(typeof(TwoDecimalConverter))]
            public decimal RentabilidadeMesIbov;

            [FieldFixedLength(1)]
            public string Simbolo3;

            [FieldFixedLength(9)]
            [FieldConverter(typeof(TwoDecimalConverter))]
            public decimal AcumuladoAnoFundo;

            [FieldFixedLength(1)]
            public string Simbolo4;

            [FieldFixedLength(9)]
            [FieldConverter(typeof(TwoDecimalConverter))]
            public decimal AcumuladoAnoCDI;

            [FieldFixedLength(1)]
            public string Simbolo5;

            [FieldFixedLength(9)]
            [FieldConverter(typeof(TwoDecimalConverter))]
            public decimal AcumuladoAnoIbov;

            [FieldFixedLength(1)]
            public string Simbolo6;

            [FieldFixedLength(9)]
            [FieldConverter(typeof(TwoDecimalConverter))]
            public decimal RentabilidadeFundoDesdeInicio;

            [FieldFixedLength(1)]
            public string Simbolo7;

            [FieldFixedLength(9)]
            [FieldConverter(typeof(TwoDecimalConverter))]
            public decimal RentabilidadeDesdeInicioCDI;

            [FieldFixedLength(1)]
            public string Simbolo8;

            [FieldFixedLength(9)]
            [FieldConverter(typeof(TwoDecimalConverter))]
            public decimal RentabilidadeDesdeInicioIbov;

            [FieldFixedLength(1)]
            public string Simbolo9;

            [FieldFixedLength(10)]
            [FieldConverter(ConverterKind.Date, "dd/MM/yyyy")]
            public DateTime DataInicio;

            #region Converter 2
            internal class TwoDecimalConverter : ConverterBase {
                // Converte de string para decimal
                public override object StringToField(string from) {
                    return Convert.ToDecimal(from);
                }

                // Converte de decimal para string
                public override string FieldToString(object from) {
                    decimal res = Convert.ToDecimal(from);
                    return res.ToString("N2");
                }
            }
            #endregion
        }

        [FixedLengthRecord()] /* Movimento Periodo */
        public class Registro4 {
           
            [FieldFixedLength(1)]
            public int TipoRegistro;

            [FieldFixedLength(6)]
            [FieldAlign(AlignMode.Right, '0')]
            public int CodigoFundo;

            [FieldFixedLength(2)]
            public string Informacao;

            [FieldFixedLength(52)]
            public string NomeFundo;

            [FieldFixedLength(10)]
            [FieldConverter(ConverterKind.Date, "dd/MM/yyyy")]
            public DateTime DataMovimentoSaldo;

            [FieldFixedLength(19)]
            [FieldConverter(typeof(EightDecimalConverter))]
            public decimal QuantidadeCotas;

            [FieldFixedLength(18)]
            [FieldConverter(typeof(TwoDecimalConverter))]
            public decimal Lancamento; // Valor Bruto

            [FieldFixedLength(16)]
            [FieldConverter(typeof(TwoDecimalConverter))]
            public decimal ValorCota;

            [FieldFixedLength(18)]
            [FieldConverter(typeof(TwoDecimalConverter))]
            public decimal IR;

            [FieldFixedLength(18)]
            [FieldConverter(typeof(TwoDecimalConverter))]
            public decimal IOF;

            [FieldFixedLength(9)]
            [FieldConverter(typeof(TwoDecimalConverter))]
            public decimal ValorLiquido; // ValorOperacao

            #region Converter 2
            internal class TwoDecimalConverter : ConverterBase {
                // Converte de string para decimal
                public override object StringToField(string from) {
                    return Convert.ToDecimal(from);
                }

                // Converte de decimal para string
                public override string FieldToString(object from) {
                    decimal res = Convert.ToDecimal(from);
                    return res.ToString("N2");
                }
            }

            internal class EightDecimalConverter : ConverterBase {
                // Converte de string para decimal
                public override object StringToField(string from) {
                    return Convert.ToDecimal(from);
                }

                // Converte de decimal para string
                public override string FieldToString(object from) {
                    decimal res = Convert.ToDecimal(from);
                    return res.ToString("N8");
                }
            }
            #endregion
        }

        [FixedLengthRecord()] /* Acumulado IR */
        public class Registro7 {
            [FieldFixedLength(1)]
            public int TipoRegistro;

            [FieldFixedLength(9)]
            [FieldConverter(typeof(EightDecimalConverter))]
            public decimal ValorAcumulado;

            #region Converter 2
            internal class EightDecimalConverter : ConverterBase {
                // Converte de string para decimal
                public override object StringToField(string from) {
                    return Convert.ToDecimal(from);
                }

                // Converte de decimal para string
                public override string FieldToString(object from) {
                    decimal res = Convert.ToDecimal(from);
                    return res.ToString("N8");
                }
            }
            #endregion
        }

        /// <summary>
        /// Realiza a Exportacao TXT
        /// </summary>
        /// <param name="idCotista">Pode ser somente uma lista com tamanho 0 ou tamanho 1</param>
        /// <param name="idCarteira"></param>
        /// <param name="dataInicio"></param>
        /// <param name="dataFim"></param>
        /// <param name="ms"></param>
        /// <param name="nomeArquivo"></param>
        public void ExportaTxt(List<int> listaIdCarteira, List<int> listaIdCotista, DateTime dataInicio, DateTime dataFim, out MemoryStream ms, out string nomeArquivo) {
            // out
            ms = new MemoryStream();
            nomeArquivo = "ExtratoGeracao.txt";

            #region Entradas
            FileHelperEngine engine = null;
            StreamWriter arquivo = new StreamWriter(ms, Encoding.GetEncoding("ISO-8859-1"));
            //
            string login = HttpContext.Current.User.Identity.Name;
            //
            CotistaCollection cotistaCollection = new CotistaCollection();
            //
            //Datas para a Posicao               
            DateTime dataInicioPosicao = dataInicio;
            DateTime dataFimPosicao = dataFim;

            //Datas para a Operacao
            DateTime dataInicioOperacao = dataInicioPosicao;
            DateTime dataFimOperacao = dataFimPosicao;
            #endregion

            // Para cada Carteira
            for (int j = 0; j < listaIdCarteira.Count; j++) {
                int idCarteira = listaIdCarteira[j];

                List<int> listaIdCotistaAux = new List<int>();
                if (listaIdCotista.Count == 0) {
                    listaIdCotistaAux = cotistaCollection.BuscaCotistasDaCarteira(listaIdCarteira[j], dataInicioPosicao, dataFimPosicao, dataInicioOperacao, dataFimOperacao, login);
                }
                else { // tamanho é 1
                    listaIdCotistaAux.Add(listaIdCotista[0]);
                }

                #region Carrega Dados da Carteira
                Carteira carteira = new Carteira();
                //
                List<esQueryItem> campos1 = new List<esQueryItem>();
                campos1.Add(carteira.Query.Nome);
                campos1.Add(carteira.Query.TipoCota);
                campos1.Add(carteira.Query.DataInicioCota);
                campos1.Add(carteira.Query.IdCarteira);
                //            
                carteira.LoadByPrimaryKey(campos1, idCarteira);
                #endregion

                DateTime dataInicioCota = carteira.DataInicioCota.Value;

                // Para cada Cotista da lista auxiliar  Carrega os Dados   
                for (int i = 0; i < listaIdCotistaAux.Count; i++) {
                    int idCotista = listaIdCotistaAux[i];

                    #region Carrega Cotista
                    Cotista cotista = new Cotista();
                    List<esQueryItem> campos = new List<esQueryItem>();
                    campos.Add(cotista.Query.Nome);
                    cotista.LoadByPrimaryKey(campos, idCotista);
                    #endregion
                   
                    #region Dados

                    #region Registro 1
                    Registro1 r1 = new Registro1();
                    r1.TipoRegistro = 1;
                    r1.Zeros = "00000000";
                    r1.NomeCliente = cotista.Nome.Trim();
                    r1.CodigoCliente = idCotista;

                    string branco = "";
                    r1.Branco1 = branco.PadLeft(50, ' ');
                    r1.Branco2 = branco.PadLeft(20, ' ');
                    r1.Branco3 = branco.PadLeft(32, ' ');
                    r1.Branco4 = branco.PadLeft(8, ' ');
                    r1.Branco5 = branco.PadLeft(2, ' ');

                    #region Carrega o Endereco
                    PessoaEnderecoQuery p = new PessoaEnderecoQuery("P");
                    CotistaQuery c = new CotistaQuery("C");

                    p.Select(p.Uf);
                    p.InnerJoin(c).On(p.IdPessoa == c.IdPessoa);
                    p.Where(p.RecebeCorrespondencia == "S" & c.IdCotista == idCotista);

                    PessoaEnderecoCollection pessoaEnderecoCollection = new PessoaEnderecoCollection();
                    pessoaEnderecoCollection.Load(p);

                    string uf = "  ";
                    if (pessoaEnderecoCollection.HasData) {
                        if (!String.IsNullOrEmpty(pessoaEnderecoCollection[0].str.Uf.Trim())) {
                            uf = pessoaEnderecoCollection[0].str.Uf.Trim();
                        }
                    }
                    #endregion

                    r1.UF = uf;
                    r1.DataInicio = dataInicio;
                    r1.DataFim = dataFim;
                    r1.DataReferencia = DateTime.Now;
                    #endregion

                    #region Registro 2

                    #region Dados de PosicaoCotista na Data Fim
                    PosicaoCotistaCollection posicaoCotistaCollectionDataFim1 = new PosicaoCotistaCollection();

                    // Verifica A DataDia Do Cliente
                    Cliente clienteAux1 = new Cliente();
                    //
                    TipoPesquisa tipoPesquisaDataFim1 = clienteAux1.IsClienteNaData(idCarteira, dataFim)
                                        ? TipoPesquisa.PosicaoCotista : TipoPesquisa.PosicaoCotistaHistorico;

                    posicaoCotistaCollectionDataFim1.QueryReset();
                    //
                    if (tipoPesquisaDataFim1 == TipoPesquisa.PosicaoCotista) {
                        //                
                        posicaoCotistaCollectionDataFim1.Query
                            .Select(posicaoCotistaCollectionDataFim1.Query.CotaDia.Avg(),
                                    posicaoCotistaCollectionDataFim1.Query.Quantidade.Sum(),
                                    posicaoCotistaCollectionDataFim1.Query.ValorBruto.Sum(),                                    
                                    posicaoCotistaCollectionDataFim1.Query.ValorIR.Sum(),
                                    posicaoCotistaCollectionDataFim1.Query.ValorIOF.Sum(),
                                    posicaoCotistaCollectionDataFim1.Query.ValorLiquido.Sum())
                            .Where(posicaoCotistaCollectionDataFim1.Query.IdCarteira == idCarteira &
                                   posicaoCotistaCollectionDataFim1.Query.IdCotista == idCotista &
                                   posicaoCotistaCollectionDataFim1.Query.Quantidade != 0);
                        //
                        posicaoCotistaCollectionDataFim1.Query.Load();
                    }
                    else if (tipoPesquisaDataFim1 == TipoPesquisa.PosicaoCotistaHistorico) {
                        PosicaoCotistaHistoricoCollection posicaoCotistaHistoricoCollection1 = new PosicaoCotistaHistoricoCollection();
                        posicaoCotistaHistoricoCollection1.QueryReset();

                        posicaoCotistaHistoricoCollection1.Query
                                .Select(posicaoCotistaHistoricoCollection1.Query.CotaDia.Avg(),
                                        posicaoCotistaHistoricoCollection1.Query.Quantidade.Sum(),
                                        posicaoCotistaHistoricoCollection1.Query.ValorBruto.Sum(),
                                        posicaoCotistaHistoricoCollection1.Query.ValorIR.Sum(),
                                        posicaoCotistaHistoricoCollection1.Query.ValorIOF.Sum(),
                                        posicaoCotistaHistoricoCollection1.Query.ValorLiquido.Sum())
                                .Where(posicaoCotistaHistoricoCollection1.Query.IdCarteira == idCarteira &
                                       posicaoCotistaHistoricoCollection1.Query.IdCotista == idCotista &
                                       posicaoCotistaHistoricoCollection1.Query.DataHistorico == dataFim &
                                       posicaoCotistaHistoricoCollection1.Query.Quantidade != 0);
                        //
                        posicaoCotistaHistoricoCollection1.Query.Load();
                        //
                        PosicaoCotista pc = posicaoCotistaCollectionDataFim1.AddNew();
                        pc.CotaDia = posicaoCotistaHistoricoCollection1[0].CotaDia;
                        pc.Quantidade = posicaoCotistaHistoricoCollection1[0].Quantidade;
                        pc.ValorBruto = posicaoCotistaHistoricoCollection1[0].ValorBruto;
                        pc.ValorIR = posicaoCotistaHistoricoCollection1[0].ValorIR;
                        pc.ValorIOF = posicaoCotistaHistoricoCollection1[0].ValorIOF;
                        pc.ValorLiquido = posicaoCotistaHistoricoCollection1[0].ValorLiquido;                                               
                    }
                    #endregion
                    
                    Registro2 r2 = new Registro2();
                    r2.TipoRegistro = 2;
                    r2.CodigoFundo = idCarteira;
                    r2.Zeros = "00";
                    r2.NomeFundo = carteira.str.Nome;

                    if (posicaoCotistaCollectionDataFim1.HasData &&
                        posicaoCotistaCollectionDataFim1[0].CotaDia.HasValue &&
                        posicaoCotistaCollectionDataFim1[0].Quantidade.HasValue &&
                        posicaoCotistaCollectionDataFim1[0].ValorBruto.HasValue &&
                        posicaoCotistaCollectionDataFim1[0].ValorIR.HasValue &&
                        posicaoCotistaCollectionDataFim1[0].ValorIOF.HasValue &&
                        posicaoCotistaCollectionDataFim1[0].ValorLiquido.HasValue) {
                            r2.ValorCota = posicaoCotistaCollectionDataFim1[0].CotaDia.Value;
                            r2.QuantidadeCota = posicaoCotistaCollectionDataFim1[0].Quantidade.Value;
                            r2.SaldoBruto = posicaoCotistaCollectionDataFim1[0].ValorBruto.Value;
                            r2.IR = posicaoCotistaCollectionDataFim1[0].ValorIR.Value;
                            r2.IOF = posicaoCotistaCollectionDataFim1[0].ValorIOF.Value;
                            r2.SaldoLiquido = posicaoCotistaCollectionDataFim1[0].ValorLiquido.Value;                            
                    }                  
                    #endregion

                    #region Registro 6

                    #region Dados de PosicaoCotista na Data Fim
                    PosicaoCotistaCollection posicaoCotistaCollectionDataFim2 = new PosicaoCotistaCollection();

                    // Verifica A DataDia Do Cliente
                    Cliente clienteAux2 = new Cliente();
                    //
                    TipoPesquisa tipoPesquisaDataFim2 = clienteAux2.IsClienteNaData(idCarteira, dataFim)
                                        ? TipoPesquisa.PosicaoCotista : TipoPesquisa.PosicaoCotistaHistorico;

                    posicaoCotistaCollectionDataFim2.QueryReset();
                    //
                    if (tipoPesquisaDataFim2 == TipoPesquisa.PosicaoCotista) {
                        //                
                        posicaoCotistaCollectionDataFim2.Query
                            .Select(posicaoCotistaCollectionDataFim2.Query.ValorBruto.Sum(),
                                    posicaoCotistaCollectionDataFim2.Query.ValorIR.Sum(),
                                    posicaoCotistaCollectionDataFim2.Query.ValorIOF.Sum(),
                                    posicaoCotistaCollectionDataFim2.Query.ValorLiquido.Sum())
                            .Where(posicaoCotistaCollectionDataFim2.Query.IdCotista == idCotista &
                                   posicaoCotistaCollectionDataFim2.Query.Quantidade != 0);
                        //
                        posicaoCotistaCollectionDataFim2.Query.Load();
                    }
                    else if (tipoPesquisaDataFim2 == TipoPesquisa.PosicaoCotistaHistorico) {
                        PosicaoCotistaHistoricoCollection posicaoCotistaHistoricoCollection2 = new PosicaoCotistaHistoricoCollection();
                        posicaoCotistaHistoricoCollection2.QueryReset();

                        posicaoCotistaHistoricoCollection2.Query
                                .Select(posicaoCotistaHistoricoCollection2.Query.ValorBruto.Sum(),
                                        posicaoCotistaHistoricoCollection2.Query.ValorIR.Sum(),
                                        posicaoCotistaHistoricoCollection2.Query.ValorIOF.Sum(),
                                        posicaoCotistaHistoricoCollection2.Query.ValorLiquido.Sum())
                                .Where(posicaoCotistaHistoricoCollection2.Query.IdCotista == idCotista &
                                       posicaoCotistaHistoricoCollection2.Query.DataHistorico == dataFim &
                                       posicaoCotistaHistoricoCollection2.Query.Quantidade != 0);
                        //
                        posicaoCotistaHistoricoCollection2.Query.Load();
                        //
                        PosicaoCotista pc2 = posicaoCotistaCollectionDataFim2.AddNew();
                        pc2.ValorBruto = posicaoCotistaHistoricoCollection2[0].ValorBruto;
                        pc2.ValorIR = posicaoCotistaHistoricoCollection2[0].ValorIR;
                        pc2.ValorIOF = posicaoCotistaHistoricoCollection2[0].ValorIOF;
                        pc2.ValorLiquido = posicaoCotistaHistoricoCollection2[0].ValorLiquido;
                    }
                    #endregion

                    Registro6 r6 = new Registro6();
                    r6.TipoRegistro = 6;
                    //
                    string branco6 = "";
                    r6.Branco = branco6.PadLeft(6, ' ');
                    //
                    r6.Zeros = "00";
                    r6.TotalPatrimonio = "Total Patrimonio";
                    //
                    if (posicaoCotistaCollectionDataFim2.HasData &&                        
                        posicaoCotistaCollectionDataFim2[0].ValorBruto.HasValue &&
                        posicaoCotistaCollectionDataFim2[0].ValorIR.HasValue &&
                        posicaoCotistaCollectionDataFim2[0].ValorIOF.HasValue &&
                        posicaoCotistaCollectionDataFim2[0].ValorLiquido.HasValue) {
                            r6.TotalSaldoBruto = posicaoCotistaCollectionDataFim2[0].ValorBruto.Value;
                            r6.TotalIR = posicaoCotistaCollectionDataFim2[0].ValorIR.Value;
                            r6.TotalIOF = posicaoCotistaCollectionDataFim2[0].ValorIOF.Value;
                            r6.TotalSaldoLiquido = posicaoCotistaCollectionDataFim2[0].ValorLiquido.Value;
                    }
                    #endregion

                    #region Registro 3

                    #region Calcula datas dos periodos
                    DateTime? dataMes = Calendario.RetornaUltimoDiaUtilMes(dataFim, -1, LocalFeriadoFixo.Brasil, TipoFeriado.Brasil);
                    DateTime? dataAno = Calendario.RetornaUltimoDiaUtilAno(dataFim, -1);

                    if (dataMes < dataInicioCota) {
                        dataMes = null;
                    }
                    if (dataAno < dataInicioCota) {
                        dataAno = null;
                    }
                    #endregion

                    #region Calcula Rentabilidade Carteira
                    /*
             * Rentabilidade Carteira
             * (Mês) / (Ano) / Desde Início            
             *
            */
                    List<decimal?> rentabilidadeCarteira = new List<decimal?>();
                    //
                    CalculoMedida calculoMedida = new CalculoMedida();
                    calculoMedida.SetDataInicio(dataInicioCota);
                    //                                     
                    #region Calcula Rentabilidade Carteira

                    calculoMedida.SetIdCarteira(carteira.IdCarteira.Value);

                    #region rentabilidadeMes
                    decimal? rentabilidadeMesNomimal = null;
                    if (dataMes != null) {
                        try {
                            rentabilidadeMesNomimal = calculoMedida.CalculaRetornoMes(dataFim);
                        }
                        catch (HistoricoCotaNaoCadastradoException) { }
                    }
                    #endregion

                    #region rentabilidadeAno
                    decimal? rentabilidadeAnoNomimal = null;
                    if (dataAno != null) {
                        try {
                            rentabilidadeAnoNomimal = calculoMedida.CalculaRetornoAno(dataFim);
                        }
                        catch (HistoricoCotaNaoCadastradoException) { }
                    }
                    #endregion

                    #region rentabilidadePeriodo
                    decimal? rentabilidadePeriodo = null;
                    try {
                        rentabilidadePeriodo = calculoMedida.CalculaRetorno(dataInicioCota, dataFim);
                    }
                    catch (HistoricoCotaNaoCadastradoException) { }
                    #endregion

                    rentabilidadeCarteira.Add(rentabilidadeMesNomimal);
                    rentabilidadeCarteira.Add(rentabilidadeAnoNomimal);
                    rentabilidadeCarteira.Add(rentabilidadePeriodo);

                    #endregion

                    #endregion

                    #region Calcula Rentabilidade Indice CDI
                    /*
             * Rentabilidade Indice CDI
             * (Mês) / (Ano) / (Periodo)
            */
                    List<decimal?> rentabilidadeIndice = new List<decimal?>();
                    //
                    calculoMedida = new CalculoMedida(idCarteira, ListaIndiceFixo.CDI);
                    //                                     
                    #region rentabilidadeMes
                    decimal? rentabilidadeMes = null;
                    if (dataMes != null) {
                        try {
                            rentabilidadeMes = calculoMedida.CalculaRetornoMesIndice(dataFim);
                        }
                        catch (CotacaoIndiceNaoCadastradoException) { }
                    }
                    #endregion

                    #region rentabilidadeAno
                    decimal? rentabilidadeAno = null;
                    if (dataAno != null) {
                        try {
                            rentabilidadeAno = calculoMedida.CalculaRetornoAnoIndice(dataFim);
                        }
                        catch (CotacaoIndiceNaoCadastradoException) { }
                    }
                    #endregion

                    #region rentabilidadePeriodo
                    decimal? rentabilidadePeriodoCDI = null;
                    try {
                        rentabilidadePeriodoCDI = calculoMedida.CalculaRetornoIndice(dataInicioCota, dataFim);
                    }
                    catch (CotacaoIndiceNaoCadastradoException) { }
                    #endregion

                    rentabilidadeIndice.Add(rentabilidadeMes);
                    rentabilidadeIndice.Add(rentabilidadeAno);
                    rentabilidadeIndice.Add(rentabilidadePeriodoCDI);

                    #endregion

                    #region Calcula Rentabilidade Indice IBOV
                    /*
             * Rentabilidade Indice IBOV
             * (Mês) / (Ano) / (Periodo)
            */
                    List<decimal?> rentabilidadeIndiceIbov = new List<decimal?>();
                    //
                    calculoMedida = new CalculoMedida(idCarteira, ListaIndiceFixo.IBOVESPA_FECHA);
                    //                                     
                    #region rentabilidadeMes
                    decimal? rentabilidadeMesIbov = null;
                    if (dataMes != null) {
                        try {
                            rentabilidadeMesIbov = calculoMedida.CalculaRetornoMesIndice(dataFim);
                        }
                        catch (CotacaoIndiceNaoCadastradoException) { }
                    }
                    #endregion

                    #region rentabilidadeAno
                    decimal? rentabilidadeAnoIbov = null;
                    if (dataAno != null) {
                        try {
                            rentabilidadeAnoIbov = calculoMedida.CalculaRetornoAnoIndice(dataFim);
                        }
                        catch (CotacaoIndiceNaoCadastradoException) { }
                    }
                    #endregion

                    #region rentabilidadePeriodo
                    decimal? rentabilidadePeriodoIbov = null;
                    try {
                        rentabilidadePeriodoIbov = calculoMedida.CalculaRetornoIndice(dataInicioCota, dataFim);
                    }
                    catch (CotacaoIndiceNaoCadastradoException) { }
                    #endregion

                    rentabilidadeIndiceIbov.Add(rentabilidadeMesIbov);
                    rentabilidadeIndiceIbov.Add(rentabilidadeAnoIbov);
                    rentabilidadeIndiceIbov.Add(rentabilidadePeriodoIbov);

                    #endregion

                    Registro3 r3 = new Registro3();
                    r3.TipoRegistro = 3;
                    r3.CodigoFundo = idCarteira;
                    r3.Zeros = "00";
                    r3.NomeFundo = carteira.str.Nome;
                    //            
                    r3.RentabilidadeMesFundo = rentabilidadeCarteira[0].HasValue ? rentabilidadeCarteira[0].Value : 0;
                    r3.RentabilidadeMesCDI = rentabilidadeIndice[0].HasValue ? rentabilidadeIndice[0].Value : 0;
                    r3.RentabilidadeMesIbov = rentabilidadeIndiceIbov[0].HasValue ? rentabilidadeIndiceIbov[0].Value : 0; ;
                    //
                    r3.AcumuladoAnoFundo = rentabilidadeCarteira[1].HasValue ? rentabilidadeCarteira[1].Value : 0;
                    r3.AcumuladoAnoCDI = rentabilidadeIndice[1].HasValue ? rentabilidadeIndice[1].Value : 0;
                    r3.AcumuladoAnoIbov = rentabilidadeIndiceIbov[1].HasValue ? rentabilidadeIndiceIbov[1].Value : 0;
                    //
                    r3.RentabilidadeFundoDesdeInicio = rentabilidadeCarteira[2].HasValue ? rentabilidadeCarteira[2].Value : 0;
                    r3.RentabilidadeDesdeInicioCDI = rentabilidadeIndice[2].HasValue ? rentabilidadeIndice[2].Value : 0;
                    r3.RentabilidadeDesdeInicioIbov = rentabilidadeIndiceIbov[2].HasValue ? rentabilidadeIndiceIbov[2].Value : 0;
                    //
                    r3.Simbolo1 = "%";
                    r3.Simbolo2 = "%";
                    r3.Simbolo3 = "%";
                    r3.Simbolo4 = "%";
                    r3.Simbolo5 = "%";
                    r3.Simbolo6 = "%";
                    r3.Simbolo7 = "%";
                    r3.Simbolo8 = "%";
                    r3.Simbolo9 = "%";
                    //
                    r3.DataInicio = dataInicioCota;
                    #endregion

                    #region Registro 4

                    DateTime dataSaldoAnterior = Calendario.SubtraiDiaUtil(dataInicio, 1);
                    if (dataSaldoAnterior < dataInicioCota) {
                        dataSaldoAnterior = dataInicioCota;
                    }

                    #region Consultas
                    #region Dados de PosicaoCotista na Data do Saldo Anterior (D-1 da dataInicio) - 1 Linha
                    PosicaoCotistaCollection posicaoCotistaCollectionDataInicio = new PosicaoCotistaCollection();

                    // Verifica A DataDia Do Cliente
                    Cliente cliente = new Cliente();
                    //
                    TipoPesquisa tipoPesquisa = cliente.IsClienteNaData(idCarteira, dataSaldoAnterior)
                                        ? TipoPesquisa.PosicaoCotista
                                        : TipoPesquisa.PosicaoCotistaHistorico;

                    posicaoCotistaCollectionDataInicio.QueryReset();
                    //
                    if (tipoPesquisa == TipoPesquisa.PosicaoCotista) {
                        posicaoCotistaCollectionDataInicio.Query
                                .Select(posicaoCotistaCollectionDataInicio.Query.ValorBruto.Sum(),
                                        posicaoCotistaCollectionDataInicio.Query.ValorIR.Sum(),
                                        posicaoCotistaCollectionDataInicio.Query.ValorIOF.Sum(),
                                        posicaoCotistaCollectionDataInicio.Query.ValorLiquido.Sum(),
                                        posicaoCotistaCollectionDataInicio.Query.CotaDia.Avg(),
                                        posicaoCotistaCollectionDataInicio.Query.Quantidade.Sum())
                                .Where(posicaoCotistaCollectionDataInicio.Query.IdCarteira == idCarteira &
                                       posicaoCotistaCollectionDataInicio.Query.IdCotista == idCotista &
                                       posicaoCotistaCollectionDataInicio.Query.Quantidade != 0);
                        //
                        posicaoCotistaCollectionDataInicio.Query.Load();
                    }
                    else if (tipoPesquisa == TipoPesquisa.PosicaoCotistaHistorico) {
                        PosicaoCotistaHistoricoCollection posicaoCotistaHistoricoCollection = new PosicaoCotistaHistoricoCollection();
                        posicaoCotistaHistoricoCollection.QueryReset();

                        posicaoCotistaHistoricoCollection.Query
                                .Select(posicaoCotistaHistoricoCollection.Query.ValorBruto.Sum(),
                                        posicaoCotistaHistoricoCollection.Query.ValorIR.Sum(),
                                        posicaoCotistaHistoricoCollection.Query.ValorIOF.Sum(),
                                        posicaoCotistaHistoricoCollection.Query.ValorLiquido.Sum(),
                                        posicaoCotistaHistoricoCollection.Query.CotaDia.Avg(),
                                        posicaoCotistaHistoricoCollection.Query.Quantidade.Sum())
                                .Where(posicaoCotistaHistoricoCollection.Query.IdCarteira == idCarteira &
                                       posicaoCotistaHistoricoCollection.Query.IdCotista == idCotista &
                                       posicaoCotistaHistoricoCollection.Query.DataHistorico == dataSaldoAnterior &
                                       posicaoCotistaHistoricoCollection.Query.Quantidade != 0);
                        //
                        posicaoCotistaHistoricoCollection.Query.Load();
                        //
                        //this.posicaoCotistaCollectionDataInicio = new PosicaoCotistaCollection(posicaoCotistaHistoricoCollection);

                        // Copia campo a campo pois comando acima não funcionou
                        PosicaoCotista p1 = posicaoCotistaCollectionDataInicio.AddNew();
                        p1.ValorBruto = posicaoCotistaHistoricoCollection[0].ValorBruto;
                        p1.ValorIR = posicaoCotistaHistoricoCollection[0].ValorIR;
                        p1.ValorIOF = posicaoCotistaHistoricoCollection[0].ValorIOF;
                        p1.ValorLiquido = posicaoCotistaHistoricoCollection[0].ValorLiquido;
                        p1.CotaDia = posicaoCotistaHistoricoCollection[0].CotaDia;
                        p1.Quantidade = posicaoCotistaHistoricoCollection[0].Quantidade;
                    }
                    #endregion

                    #region Dados de PosicaoCotista na Data Fim - ultima Linha
                    PosicaoCotistaCollection posicaoCotistaCollectionDataFim = new PosicaoCotistaCollection();

                    // Verifica A DataDia Do Cliente
                    Cliente clienteAux = new Cliente();
                    //
                    TipoPesquisa tipoPesquisaDataFim = clienteAux.IsClienteNaData(idCarteira, dataFim)
                                        ? TipoPesquisa.PosicaoCotista : TipoPesquisa.PosicaoCotistaHistorico;

                    posicaoCotistaCollectionDataFim.QueryReset();
                    //
                    if (tipoPesquisaDataFim == TipoPesquisa.PosicaoCotista) {
                        //                
                        posicaoCotistaCollectionDataFim.Query
                            .Select(posicaoCotistaCollectionDataFim.Query.ValorBruto.Sum(),
                                    posicaoCotistaCollectionDataFim.Query.ValorIR.Sum(),
                                    posicaoCotistaCollectionDataFim.Query.ValorIOF.Sum(),
                                    posicaoCotistaCollectionDataFim.Query.ValorLiquido.Sum(),
                                    posicaoCotistaCollectionDataFim.Query.CotaDia.Avg(),
                                    posicaoCotistaCollectionDataFim.Query.Quantidade.Sum())
                            .Where(posicaoCotistaCollectionDataFim.Query.IdCarteira == idCarteira &
                                   posicaoCotistaCollectionDataFim.Query.IdCotista == idCotista &
                                   posicaoCotistaCollectionDataFim.Query.Quantidade != 0);
                        //
                        posicaoCotistaCollectionDataFim.Query.Load();
                    }
                    else if (tipoPesquisaDataFim == TipoPesquisa.PosicaoCotistaHistorico) {
                        PosicaoCotistaHistoricoCollection posicaoCotistaHistoricoCollection = new PosicaoCotistaHistoricoCollection();
                        posicaoCotistaHistoricoCollection.QueryReset();

                        posicaoCotistaHistoricoCollection.Query
                                .Select(posicaoCotistaHistoricoCollection.Query.ValorBruto.Sum(),
                                        posicaoCotistaHistoricoCollection.Query.ValorIR.Sum(),
                                        posicaoCotistaHistoricoCollection.Query.ValorIOF.Sum(),
                                        posicaoCotistaHistoricoCollection.Query.ValorLiquido.Sum(),
                                        posicaoCotistaHistoricoCollection.Query.CotaDia.Avg(),
                                        posicaoCotistaHistoricoCollection.Query.Quantidade.Sum())
                                .Where(posicaoCotistaHistoricoCollection.Query.IdCarteira == idCarteira &
                                       posicaoCotistaHistoricoCollection.Query.IdCotista == idCotista &
                                       posicaoCotistaHistoricoCollection.Query.DataHistorico == dataFim &
                                       posicaoCotistaHistoricoCollection.Query.Quantidade != 0);
                        //
                        posicaoCotistaHistoricoCollection.Query.Load();
                        //
                        //this.posicaoCotistaCollectionDataFim = new PosicaoCotistaCollection(posicaoCotistaHistoricoCollection);
                        // Copia campo a campo pois comando acima não funcionou
                        PosicaoCotista p2 = posicaoCotistaCollectionDataFim.AddNew();
                        p2.ValorBruto = posicaoCotistaHistoricoCollection[0].ValorBruto;
                        p2.ValorIR = posicaoCotistaHistoricoCollection[0].ValorIR;
                        p2.ValorIOF = posicaoCotistaHistoricoCollection[0].ValorIOF;
                        p2.ValorLiquido = posicaoCotistaHistoricoCollection[0].ValorLiquido;
                        p2.CotaDia = posicaoCotistaHistoricoCollection[0].CotaDia;
                        p2.Quantidade = posicaoCotistaHistoricoCollection[0].Quantidade;
                    }
                    #endregion

                    #region Operação Cotista - Dados Intermidiarios
                    OperacaoCotistaCollection operacaoCotistaCollection1 = new OperacaoCotistaCollection();
                    //
                    operacaoCotistaCollection1.QueryReset();
                    //
                    operacaoCotistaCollection1.Query
                         .Select(operacaoCotistaCollection1.Query.DataConversao,
                                 operacaoCotistaCollection1.Query.DataOperacao,
                                 operacaoCotistaCollection1.Query.TipoOperacao,
                                 operacaoCotistaCollection1.Query.ValorBruto,
                                 operacaoCotistaCollection1.Query.ValorIR,
                                 operacaoCotistaCollection1.Query.ValorIOF,
                                 operacaoCotistaCollection1.Query.ValorLiquido,
                                 operacaoCotistaCollection1.Query.CotaOperacao,
                                 operacaoCotistaCollection1.Query.Quantidade)
                         .Where(operacaoCotistaCollection1.Query.IdCarteira == idCarteira,
                                operacaoCotistaCollection1.Query.IdCotista == idCotista,
                                operacaoCotistaCollection1.Query.DataConversao >= dataInicio,
                                operacaoCotistaCollection1.Query.DataConversao <= dataFim);
                    //
                    operacaoCotistaCollection1.Query.Load();
                    #endregion

                    #endregion

                    List<Registro4> registroList4 = new List<Registro4>();

                    #region SaldoInicial

                    Registro4 r4 = new Registro4();
                    r4.TipoRegistro = 4;
                    r4.CodigoFundo = idCarteira;
                    r4.Informacao = REGISTRO4_SALDOANTERIOR;
                    r4.NomeFundo = carteira.str.Nome;
                    r4.DataMovimentoSaldo = dataSaldoAnterior;
                    //
                    if (posicaoCotistaCollectionDataInicio.HasData &&
                        posicaoCotistaCollectionDataInicio[0].Quantidade.HasValue &&
                        posicaoCotistaCollectionDataInicio[0].ValorBruto.HasValue &&
                        posicaoCotistaCollectionDataInicio[0].CotaDia.HasValue &&
                        posicaoCotistaCollectionDataInicio[0].ValorIR.HasValue &&
                        posicaoCotistaCollectionDataInicio[0].ValorIOF.HasValue &&
                        posicaoCotistaCollectionDataInicio[0].ValorLiquido.HasValue) {
                        r4.QuantidadeCotas = posicaoCotistaCollectionDataInicio[0].Quantidade.Value;
                        r4.Lancamento = posicaoCotistaCollectionDataInicio[0].ValorBruto.Value;
                        r4.ValorCota = posicaoCotistaCollectionDataInicio[0].CotaDia.Value;
                        r4.IR = posicaoCotistaCollectionDataInicio[0].ValorIR.Value;
                        r4.IOF = posicaoCotistaCollectionDataInicio[0].ValorIOF.Value;
                        r4.ValorLiquido = posicaoCotistaCollectionDataInicio[0].ValorLiquido.Value;
                    }
                    registroList4.Add(r4);
                    #endregion

                    #region Aplicacao/Resgate
                    for (int k = 0; k < operacaoCotistaCollection1.Count; k++) {
                        Int16 tipoOperacao = operacaoCotistaCollection1[k].TipoOperacao.Value;
                        string info = "";

                        if (tipoOperacao == (byte)TipoOperacaoFundo.Aplicacao ||
                           tipoOperacao == (byte)TipoOperacaoFundo.AplicacaoCotas ||
                           tipoOperacao == (byte)TipoOperacaoFundo.AplicacaoCotasEspecial ||
                           tipoOperacao == (byte)TipoOperacaoFundo.AplicacaoAcoesEspecial) {
                            info = REGISTRO4_APLICACAO;
                        }
                        else if (tipoOperacao == (byte)TipoOperacaoFundo.ResgateBruto ||
                            tipoOperacao == (byte)TipoOperacaoFundo.ResgateLiquido ||
                            tipoOperacao == (byte)TipoOperacaoFundo.ResgateCotas ||
                            tipoOperacao == (byte)TipoOperacaoFundo.ResgateTotal ||
                            tipoOperacao == (byte)TipoOperacaoFundo.ResgateCotasEspecial) {
                            info = REGISTRO4_RESGATE;
                        }

                        Registro4 r4_Operacao = new Registro4();
                        //
                        r4_Operacao.TipoRegistro = 4;
                        r4_Operacao.CodigoFundo = idCarteira;
                        r4_Operacao.Informacao = info;
                        r4_Operacao.NomeFundo = carteira.str.Nome;
                        r4_Operacao.DataMovimentoSaldo = operacaoCotistaCollection1[k].DataConversao.Value;
                        //
                        r4_Operacao.QuantidadeCotas = operacaoCotistaCollection1[k].Quantidade.Value;
                        r4_Operacao.Lancamento = operacaoCotistaCollection1[k].ValorBruto.Value;
                        r4_Operacao.ValorCota = operacaoCotistaCollection1[k].CotaOperacao.Value;
                        r4_Operacao.IR = operacaoCotistaCollection1[k].ValorIR.Value;
                        r4_Operacao.IOF = operacaoCotistaCollection1[k].ValorIOF.Value;
                        r4_Operacao.ValorLiquido = operacaoCotistaCollection1[k].ValorLiquido.Value;
                        //
                        registroList4.Add(r4_Operacao);
                    }
                    #endregion

                    #region SaldoFinal
                    Registro4 r4_SaldoFinal = new Registro4();
                    r4_SaldoFinal.TipoRegistro = 4;
                    r4_SaldoFinal.CodigoFundo = idCarteira;
                    r4_SaldoFinal.Informacao = REGISTRO4_SALDOATUAL;
                    r4_SaldoFinal.NomeFundo = carteira.str.Nome;
                    r4_SaldoFinal.DataMovimentoSaldo = dataFim;
                    //
                    if (posicaoCotistaCollectionDataFim.HasData &&
                        posicaoCotistaCollectionDataFim[0].Quantidade.HasValue &&
                        posicaoCotistaCollectionDataFim[0].ValorBruto.HasValue &&
                        posicaoCotistaCollectionDataFim[0].CotaDia.HasValue &&
                        posicaoCotistaCollectionDataFim[0].ValorIR.HasValue &&
                        posicaoCotistaCollectionDataFim[0].ValorIOF.HasValue &&
                        posicaoCotistaCollectionDataFim[0].ValorLiquido.HasValue) {
                        r4_SaldoFinal.QuantidadeCotas = posicaoCotistaCollectionDataFim[0].Quantidade.Value;
                        r4_SaldoFinal.Lancamento = posicaoCotistaCollectionDataFim[0].ValorBruto.Value;
                        r4_SaldoFinal.ValorCota = posicaoCotistaCollectionDataFim[0].CotaDia.Value;
                        r4_SaldoFinal.IR = posicaoCotistaCollectionDataFim[0].ValorIR.Value;
                        r4_SaldoFinal.IOF = posicaoCotistaCollectionDataFim[0].ValorIOF.Value;
                        r4_SaldoFinal.ValorLiquido = posicaoCotistaCollectionDataFim[0].ValorLiquido.Value;
                    }
                    registroList4.Add(r4_SaldoFinal);
                    #endregion

                    //
                    #endregion

                    #region Registro 7
                    //Registro7 r7 = new Registro7();
                    //r7.TipoRegistro = 7;

                    //if (posicaoCotistaCollectionDataFim.HasData &&
                    //    posicaoCotistaCollectionDataFim[0].ValorIR.HasValue) {
                    //    r7.ValorAcumulado = posicaoCotistaCollectionDataFim[0].ValorIR.Value;
                    //}            
                    #endregion

                    #region Lista dos registros para carregar na engine
                    List<Registro1> registroList = new List<Registro1>();
                    registroList.Add(r1);

                    List<Registro2> registroList2 = new List<Registro2>();
                    registroList2.Add(r2);

                    List<Registro6> registroList6 = new List<Registro6>();
                    registroList6.Add(r6);

                    List<Registro3> registroList3 = new List<Registro3>();
                    registroList3.Add(r3);

                    //List<Registro7> registroList7 = new List<Registro7>();
                    //registroList7.Add(r7); 

                    #endregion

                    #region Engine para Arquivo
                    //
                    engine = new FileHelperEngine(typeof(Registro1));
                    engine.Encoding = Encoding.GetEncoding("ISO-8859-1");
                    engine.WriteStream(arquivo, registroList);

                    engine = new FileHelperEngine(typeof(Registro2));
                    engine.Encoding = Encoding.GetEncoding("ISO-8859-1");
                    engine.WriteStream(arquivo, registroList2);

                    engine = new FileHelperEngine(typeof(Registro6));
                    engine.Encoding = Encoding.GetEncoding("ISO-8859-1");
                    engine.WriteStream(arquivo, registroList6);

                    engine = new FileHelperEngine(typeof(Registro3));
                    engine.Encoding = Encoding.GetEncoding("ISO-8859-1");
                    engine.WriteStream(arquivo, registroList3);

                    engine = new FileHelperEngine(typeof(Registro4));
                    engine.Encoding = Encoding.GetEncoding("ISO-8859-1");
                    engine.WriteStream(arquivo, registroList4);

                    //engine = new FileHelperEngine(typeof(Registro7));
                    //engine.Encoding = Encoding.GetEncoding("ISO-8859-1");
                    //engine.WriteStream(arquivo, registroList7);

                    #endregion

                    #endregion
                }
            }
           
            //Fecha o arquivo
            arquivo.Flush();
        }
    }      
    #endregion

    new protected void Page_Load(object sender, EventArgs e) {
        const string visaoHTML = "Report";
        const string visaoPDF = "PDF";
        const string visaoExcel = "Excel";
        const string visaoTXT = "TXT";

        string visao = Request.QueryString["Visao"].ToString();

        #region Entradas
        // Obrigatorio
        DateTime dataInicio = Convert.ToDateTime(Session["textDataInicio"]);
        DateTime dataFim = Convert.ToDateTime(Session["textDataFim"]);
        bool incluiComposicaoCarteira = Convert.ToBoolean(Session["chkIncluiComposicaoCarteira"]);

        int? idCarteira = null;
        if (Session["btnEditCodigoCarteira"] != null && (string)Session["btnEditCodigoCarteira"] != "") {
            idCarteira = Convert.ToInt32(Session["btnEditCodigoCarteira"]);
        }

        List<int> listaIdCarteira = new List<int>();
        if (Session["textListaCarteiras"] != null && (string)Session["textListaCarteiras"] != "") {
            string listaCarteiras = Convert.ToString(Session["textListaCarteiras"]);
            if (!String.IsNullOrEmpty(listaCarteiras)) {
                listaCarteiras = listaCarteiras.Replace(",", ";"); //só para garantir :)
                string[] lista = null;
                lista = listaCarteiras.Split(new Char[] { ';' });

                foreach (string id in lista) {
                    listaIdCarteira.Add(Convert.ToInt32(id));
                }
            }
        }

        int? idCotista = null;
        if (!String.IsNullOrEmpty((string)Session["btnEditCodigoCotista"])) {
            idCotista = Convert.ToInt32(Session["btnEditCodigoCotista"]);
        }

        string fileName = "ReportExtratoCotista";
        //
        CotistaCollection cotistaCollection = new CotistaCollection();
        //

        //Datas para a Posicao               
        DateTime dataInicioPosicao = dataInicio;
        DateTime dataFimPosicao = dataFim;

        //Datas para a Operacao
        DateTime dataInicioOperacao = dataInicioPosicao;
        DateTime dataFimOperacao = dataFimPosicao;

        ReportMaster reportMaster = new ReportMaster();
        reportMaster.CreateDocument();
        reportMaster.PrintingSystem.ContinuousPageNumbering = false;

        string login = HttpContext.Current.User.Identity.Name;
        
        #endregion

        #region HTML/PDF e Excel

        if (visao == visaoHTML || visao == visaoPDF || visao == visaoExcel) {

            //Contadores para contracapa
            int x = 0;
            int cont = 1;
            //**************************

            if (listaIdCarteira.Count > 0) {
                // Todos os Cotistas e n carteiras escolhidas
                #region Executa Relatorio com n Carteira e 1 a n Cotistas
                for (int j = 0; j < listaIdCarteira.Count; j++) {
                    // Chamada: N vezes
                    List<int> listaIdCotista = cotistaCollection.BuscaCotistasDaCarteira(listaIdCarteira[j], dataInicioPosicao, dataFimPosicao,
                                                        dataInicioOperacao, dataFimOperacao, login);
                    //                                               
                    for (int i = 0; i < listaIdCotista.Count; i++) {
                        ReportExtratoCotista reportExtratoCotista = new ReportExtratoCotista(listaIdCarteira[j], listaIdCotista[i], dataInicio, dataFim, incluiComposicaoCarteira);
                        // Cria o relatorio para 1 cotista
                        reportExtratoCotista.CreateDocument();
                        // Anexa o Conteudo do Relatorio no Relatorio Master

                        reportMaster.Pages.AddRange(reportExtratoCotista.PrintingSystem.Pages);

                        for (int z = x; z < reportMaster.PrintingSystem.Pages.Count - x; z++) {
                            this.AdicionaContraCapa(reportMaster, listaIdCotista[i], x + cont);

                            x++;
                            cont++;
                        }
                    }
                }
                #endregion

                MemoryStream ms = new MemoryStream();
                switch (visao) {
                    case visaoHTML:
                        #region Visao HTML
                        //                
                        ReportViewer1.Report = reportMaster;
                        //
                        break;
                        #endregion
                    case visaoPDF:
                        #region Visao PDF
                        //
                        ms = new MemoryStream();
                        reportMaster.ExportToPdf(ms);
                        ms.Seek(0, SeekOrigin.Begin);
                        Response.ClearContent();
                        Response.ClearHeaders();
                        Response.Buffer = true;
                        Response.Cache.SetCacheability(HttpCacheability.Private);
                        Response.ContentType = "application/pdf";
                        Response.AddHeader("Content-Length", ms.Length.ToString(CultureInfo.CurrentCulture));
                        Response.AddHeader("Content-Disposition", String.Format("attachment;filename=" + fileName + ".pdf"));

                        Response.BinaryWrite(ms.ToArray());
                        ms.Close();
                        HttpContext.Current.ApplicationInstance.CompleteRequest();
                        Response.End();
                        //
                        break;
                        #endregion
                    case visaoExcel:
                        #region Visao Excel
                        //
                        ms = new MemoryStream();

                        //// Get its XLS export options.
                        //XlsExportOptions xlsOptions = reportMaster.ExportOptions.Xls;

                        //// Set XLS-specific export options.
                        //xlsOptions.ShowGridLines = true;
                        //xlsOptions.UseNativeFormat = true;
                        //reportMaster.ExportToXls(ms, xlsOptions);

                        reportMaster.ExportToXls(ms);

                        ms.Seek(0, SeekOrigin.Begin);
                        Response.ClearContent();
                        Response.ClearHeaders();
                        Response.Buffer = true;
                        Response.Cache.SetCacheability(HttpCacheability.Private);
                        Response.ContentType = "application/vnd.ms-excel";
                        Response.AddHeader("Content-Length", ms.Length.ToString(CultureInfo.CurrentCulture));
                        Response.AddHeader("Content-Disposition", String.Format("attachment;filename=" + fileName + ".xls"));

                        Response.BinaryWrite(ms.ToArray());
                        ms.Close();
                        HttpContext.Current.ApplicationInstance.CompleteRequest();
                        Response.End();

                        break;
                        #endregion
                }
            }
            else {
                if (!idCotista.HasValue) {
                    #region Executa Relatorio com 1 Carteira e 1 a n Cotistas
                    // Chamada: N vezes
                    List<int> listaIdCotista = cotistaCollection.BuscaCotistasDaCarteira(idCarteira.Value, dataInicioPosicao, dataFimPosicao,
                                                        dataInicioOperacao, dataFimOperacao, login);
                    //           
                    for (int i = 0; i < listaIdCotista.Count; i++) {
                        ReportExtratoCotista reportExtratoCotista = new ReportExtratoCotista(idCarteira.Value, listaIdCotista[i], dataInicio, dataFim, incluiComposicaoCarteira);
                        // Cria o relatorio para 1 cotista
                        reportExtratoCotista.CreateDocument();
                        // Anexa o Conteudo do Relatorio no Relatorio Master
                        reportMaster.Pages.AddRange(reportExtratoCotista.PrintingSystem.Pages);
                        //
                        for (int z = x; z < reportMaster.PrintingSystem.Pages.Count - x; z++) {
                            this.AdicionaContraCapa(reportMaster, listaIdCotista[i], x + cont);

                            x++;
                            cont++;
                        }
                    }
                    #endregion

                    MemoryStream ms = new MemoryStream();
                    switch (visao) {
                        case visaoHTML:
                            #region Visao HTML
                            //                
                            ReportViewer1.Report = reportMaster;
                            //
                            break;
                            #endregion
                        case visaoPDF:
                            #region Visao PDF
                            //
                            ms = new MemoryStream();
                            reportMaster.ExportToPdf(ms);
                            ms.Seek(0, SeekOrigin.Begin);
                            Response.ClearContent();
                            Response.ClearHeaders();
                            Response.Buffer = true;
                            Response.Cache.SetCacheability(HttpCacheability.Private);
                            Response.ContentType = "application/pdf";
                            Response.AddHeader("Content-Length", ms.Length.ToString(CultureInfo.CurrentCulture));
                            Response.AddHeader("Content-Disposition", String.Format("attachment;filename=" + fileName + ".pdf"));

                            Response.BinaryWrite(ms.ToArray());
                            ms.Close();
                            HttpContext.Current.ApplicationInstance.CompleteRequest();
                            Response.End();
                            //
                            break;
                            #endregion
                        case visaoExcel:
                            #region Visao Excel
                            //
                            ms = new MemoryStream();

                            //// Get its XLS export options.
                            //XlsExportOptions xlsOptions = reportMaster.ExportOptions.Xls;

                            //// Set XLS-specific export options.
                            //xlsOptions.ShowGridLines = true;
                            //xlsOptions.UseNativeFormat = true;
                            //reportMaster.ExportToXls(ms, xlsOptions);

                            reportMaster.ExportToXls(ms);

                            ms.Seek(0, SeekOrigin.Begin);
                            Response.ClearContent();
                            Response.ClearHeaders();
                            Response.Buffer = true;
                            Response.Cache.SetCacheability(HttpCacheability.Private);
                            Response.ContentType = "application/vnd.ms-excel";
                            Response.AddHeader("Content-Length", ms.Length.ToString(CultureInfo.CurrentCulture));
                            Response.AddHeader("Content-Disposition", String.Format("attachment;filename=" + fileName + ".xls"));

                            Response.BinaryWrite(ms.ToArray());
                            ms.Close();
                            HttpContext.Current.ApplicationInstance.CompleteRequest();
                            Response.End();

                            break;
                            #endregion
                    }
                }
                // 1 Único Cotista com 1 ou n carteiras selecionadas
                else {
                    #region 1 Único Cotista

                    List<int> listaIds = new List<int>();
                    if (idCarteira.HasValue) {
                        listaIds.Add(idCarteira.Value);
                    }
                    else {
                        OperacaoCotistaCollection colOper = new OperacaoCotistaCollection();
                        colOper.Query.Select(colOper.Query.IdCarteira);
                        colOper.Query.Where(colOper.Query.IdCotista.Equal(idCotista.Value),
                                            colOper.Query.DataConversao.GreaterThanOrEqual(dataInicio),
                                            colOper.Query.DataConversao.LessThanOrEqual(dataFim));
                        colOper.Query.es.Distinct = true;
                        colOper.Query.Load();

                        foreach (OperacaoCotista oper in colOper) {
                            listaIds.Add(oper.IdCarteira.Value);
                        }

                        PosicaoCotistaCollection colPos = new PosicaoCotistaCollection();
                        colPos.Query.Select(colPos.Query.IdCarteira);
                        colPos.Query.Where(colPos.Query.IdCotista.Equal(idCotista.Value),
                                           colPos.Query.Quantidade.NotEqual(0));

                        if (listaIds.Count > 0) {
                            colPos.Query.Where(colPos.Query.IdCarteira.NotIn(listaIds));
                        }

                        colPos.Query.es.Distinct = true;
                        colPos.Query.Load();

                        foreach (PosicaoCotista pos in colPos) {
                            listaIds.Add(pos.IdCarteira.Value);
                        }
                    }

                    foreach (int idCarteiraLista in listaIds) {
                        ReportExtratoCotista reportExtratoCotista = new ReportExtratoCotista(idCarteiraLista, idCotista.Value, dataInicio, dataFim, incluiComposicaoCarteira);
                        // Cria o relatorio para 1 cotista
                        reportExtratoCotista.CreateDocument();
                        // Anexa o Conteudo do Relatorio no Relatorio Master
                        reportMaster.Pages.AddRange(reportExtratoCotista.PrintingSystem.Pages);

                        for (int z = x; z < reportMaster.PrintingSystem.Pages.Count - x; z++) {
                            this.AdicionaContraCapa(reportMaster, idCotista.Value, x + cont);

                            x++;
                            cont++;
                        }

                    }

                    MemoryStream ms = new MemoryStream();
                    switch (visao) {
                        case visaoHTML:
                            #region Visao HTML
                            //                
                            ReportViewer1.Report = reportMaster;
                            //
                            break;
                            #endregion
                        case visaoPDF:
                            #region Visao PDF
                            //
                            ms = new MemoryStream();
                            reportMaster.ExportToPdf(ms);
                            ms.Seek(0, SeekOrigin.Begin);
                            Response.ClearContent();
                            Response.ClearHeaders();
                            Response.Buffer = true;
                            Response.Cache.SetCacheability(HttpCacheability.Private);
                            Response.ContentType = "application/pdf";
                            Response.AddHeader("Content-Length", ms.Length.ToString(CultureInfo.CurrentCulture));
                            Response.AddHeader("Content-Disposition", String.Format("attachment;filename=" + fileName + ".pdf"));

                            Response.BinaryWrite(ms.ToArray());
                            ms.Close();
                            HttpContext.Current.ApplicationInstance.CompleteRequest();
                            Response.End();
                            //
                            break;
                            #endregion
                        case visaoExcel:
                            #region Visao Excel
                            //
                            ms = new MemoryStream();

                            reportMaster.ExportToXls(ms);

                            ms.Seek(0, SeekOrigin.Begin);
                            Response.ClearContent();
                            Response.ClearHeaders();
                            Response.Buffer = true;
                            Response.Cache.SetCacheability(HttpCacheability.Private);
                            Response.ContentType = "application/vnd.ms-excel";
                            Response.AddHeader("Content-Length", ms.Length.ToString(CultureInfo.CurrentCulture));
                            Response.AddHeader("Content-Disposition", String.Format("attachment;filename=" + fileName + ".xls"));

                            Response.BinaryWrite(ms.ToArray());
                            ms.Close();
                            HttpContext.Current.ApplicationInstance.CompleteRequest();
                            Response.End();

                            break;
                            #endregion
                    }

                    #endregion
                }
            }
        }
        #endregion

        #region TXT
        else if (visao == visaoTXT) {
            #region visaoTXT

            MemoryStream msTxt = null;
            string nomeArquivoTxt = "";

            if (listaIdCarteira.Count > 0) {
                // Todos os Cotistas e n carteiras escolhidas
                #region Executa Relatorio com n Carteira e 1 a n Cotistas

                List<int> listaIdCotista = new List<int>(); // Passa zerado pois vai ser descoberto depois para cada carteira
                /* List<int> listaIdCotista = cotistaCollection.BuscaCotistasDaCarteira(listaIdCarteira[j], dataInicioPosicao, dataFimPosicao, dataInicioOperacao, dataFimOperacao, login); */

                new DataHolderTXT().ExportaTxt(listaIdCarteira, listaIdCotista, dataInicio, dataFim, out msTxt, out nomeArquivoTxt);
                
                #endregion
            }
            else {
                if (!idCotista.HasValue) {
                    #region Executa Relatorio com 1 Carteira e 1 a n Cotistas

                    List<int> listaIdCotista = new List<int>(); // Passa zerado pois vai ser descoberto depois para cada carteira
                    /* List<int> listaIdCotista = cotistaCollection.BuscaCotistasDaCarteira(idCarteira.Value, dataInicioPosicao, dataFimPosicao,
                                                        dataInicioOperacao, dataFimOperacao, login); */

                    List<int> listaCarteira = new List<int>(new int[] { idCarteira.Value }); // 1 só carteira
                    new DataHolderTXT().ExportaTxt(listaCarteira, listaIdCotista, dataInicio, dataFim, out msTxt, out nomeArquivoTxt);

                    #endregion                   
                }
                // 1 Único Cotista com 1 ou n carteiras selecionadas
                else {
                    #region 1 Único Cotista

                    List<int> listaIds = new List<int>();
                    if (idCarteira.HasValue) {
                        listaIds.Add(idCarteira.Value);
                    }
                    else {
                        OperacaoCotistaCollection colOper = new OperacaoCotistaCollection();
                        colOper.Query.Select(colOper.Query.IdCarteira);
                        colOper.Query.Where(colOper.Query.IdCotista.Equal(idCotista.Value),
                                            colOper.Query.DataConversao.GreaterThanOrEqual(dataInicio),
                                            colOper.Query.DataConversao.LessThanOrEqual(dataFim));
                        colOper.Query.es.Distinct = true;
                        colOper.Query.Load();

                        foreach (OperacaoCotista oper in colOper) {
                            listaIds.Add(oper.IdCarteira.Value);
                        }

                        PosicaoCotistaCollection colPos = new PosicaoCotistaCollection();
                        colPos.Query.Select(colPos.Query.IdCarteira);
                        colPos.Query.Where(colPos.Query.IdCotista.Equal(idCotista.Value),
                                           colPos.Query.Quantidade.NotEqual(0));

                        if (listaIds.Count > 0) {
                            colPos.Query.Where(colPos.Query.IdCarteira.NotIn(listaIds));
                        }

                        colPos.Query.es.Distinct = true;
                        colPos.Query.Load();

                        foreach (PosicaoCotista pos in colPos) {
                            listaIds.Add(pos.IdCarteira.Value);
                        }
                    }
                    List<int> listaCotista = new List<int>(new int[] { idCotista.Value }); // 1 só cotista
                    new DataHolderTXT().ExportaTxt(listaIds, listaCotista, dataInicio, dataFim, out msTxt, out nomeArquivoTxt);
                   
                    #endregion
                }
            }
                                                
            Response.ClearContent();
            Response.ClearHeaders();
            Response.Buffer = true;
            Response.Cache.SetCacheability(HttpCacheability.Private);
            Response.ContentType = "text/plain";
            Response.AddHeader("Content-Length", msTxt.Length.ToString(CultureInfo.CurrentCulture));
            Response.AppendHeader("Content-Disposition", String.Format("attachment;filename=" + nomeArquivoTxt));

            Response.BinaryWrite(msTxt.ToArray());
            msTxt.Close();
            //
            HttpContext.Current.ApplicationInstance.CompleteRequest();
            Response.End();

            #endregion
        }
        #endregion
    }

    /// <summary>
    /// Adiciona SubReport ContraCapa se existir a imagem capa_extrato_cotista
    /// Se não existir imagem não adiciona SubReport
    /// </summary>
    /// <param name="r">Report Master que contem o conteúdo do relatório</param>
    /// <param name="idCotista">IdCotista</param>
    private void AdicionaContraCapa(ReportMaster r, int idCotista, int posicao) {

        string imagemCotistaContraPagina = "~/imagensPersonalizadas/" + "capa_extrato_cotista_" + WebConfig.AppSettings.Cliente + ".png";

        string pathImagem = HostingEnvironment.MapPath(imagemCotistaContraPagina);

        // Se existe imagem Adiciona SubReport ContraCapa
        if (File.Exists(pathImagem)) {
            SubReportContraPagina subReportContraPagina = new SubReportContraPagina(idCotista);
            subReportContraPagina.CreateDocument();
            //
            //r.Pages.AddRange(subReportContraPagina.PrintingSystem.Pages);
            r.Pages.Insert(posicao, subReportContraPagina.Pages[0]);
        }
    }
}