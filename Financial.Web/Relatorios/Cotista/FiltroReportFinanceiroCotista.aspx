﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="FiltroReportFinanceiroCotista.aspx.cs" Inherits="FiltroReportFinanceiroCotista" %>

<%@ Register Assembly="System.Web, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"
    Namespace="System.Web.UI.HtmlControls" TagPrefix="cc2" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxnb" %>    
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxcb" %>
<%@ Register Assembly="System.Web, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"
    Namespace="System.Web.UI" TagPrefix="cc1" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxe" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxwgv" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxpc" %>
<%@ Register Assembly="EntitySpaces.Web" Namespace="EntitySpaces.Web" TagPrefix="cc1" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >

<head id="Head1" runat="server">
<link href="~/css/forms.css" rel="stylesheet" type="text/css" />

<script language="JavaScript">
function OnGetDataCarteira(values) {
    btnEditCodigoCarteira.SetValue(values);                    
    popupCarteira.HideWindow();
    ASPxCallback1.SendCallback(btnEditCodigoCarteira.GetValue());        
    btnEditCodigoCarteira.Focus();    
}

function OnGetDataCotista(values) {
    btnEditCodigoCotista.SetValue(values);                    
    popupCotista.HideWindow();
    ASPxCallback2.SendCallback(btnEditCodigoCotista.GetValue());        
    btnEditCodigoCotista.Focus();    
}
</script>
<script type="text/javascript" language="Javascript" src="../../js/global.js"></script>
    
</head>

<body >
    <form id="form1" runat="server"> 
    
    <dxcb:ASPxCallback ID="ASPxCallback1" runat="server" OnCallback="ASPxCallback1_Callback">
        <ClientSideEvents CallbackComplete="function(s, e) {
            OnCallBackCompleteCliente(s, e, popupMensagemCarteira, 
                                      btnEditCodigoCarteira, document.getElementById('textNomeCarteira'));
        }        
        "/>
    </dxcb:ASPxCallback>
           
    <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="true" EnableScriptLocalization="true"/>
       
    <div class="divPanel">
    <table width="100%"><tr><td>
    <div id="container_small">
    
    <div id="header"><asp:Label ID="financeiroCotista" runat="server" Text="Informe Financeiro de Cotistas" /></div>
        
    <div id="mainContentSpace">
    
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
    <ContentTemplate>
        
        <div class="reportFilter">
        
        <div style="height:20px"></div>                                   
                                        
        <table cellpadding="2" cellspacing="2">
            
            <tr>
            <td class="td_Label">                
                <asp:Label ID="labelCarteira" runat="server" CssClass="labelRequired" Text="Carteira:" />
            </td>
            
            <td>
                <dxe:ASPxSpinEdit ID="btnEditCodigoCarteira" runat="server" CssClass="textButtonEdit" 
                                    EnableClientSideAPI="True"
                                    ClientInstanceName="btnEditCodigoCarteira" SpinButtons-ShowIncrementButtons="false"
                                    MaxLength="9" NumberType="Integer" AllowMouseWheel="false">  
                <Buttons>
                    <dxe:EditButton></dxe:EditButton>                                
                </Buttons>        
                <ClientSideEvents
                         KeyPress="function(s, e) {document.getElementById('textNomeCarteira').value = '';}" 
                         ButtonClick="function(s, e) {popupCarteira.ShowAtElementByID(s.name);}" 
                         LostFocus="function(s, e) {OnLostFocus(popupMensagemCarteira, ASPxCallback1, btnEditCodigoCarteira);}"
                />                            
                </dxe:ASPxSpinEdit>                
            </td>                          
            
            <td colspan="2" width=300>
                <asp:TextBox ID="textNomeCarteira" runat="server" CssClass="textNome" Enabled="false" />
            </td>
            </tr>
                                   
            <tr>
            <td class="td_Label">
                <asp:Label ID="labelData" runat="server" CssClass="labelRequired" Text="Data:" />
            </td>                        
            <td colspan="3">
                <dxe:ASPxDateEdit ID="textData" runat="server" ClientInstanceName="textData"/>            
            </td>              
            </tr>  
                                                       
            <tr>
                <td class="td_Label">
                    <asp:Label ID="labelValor" runat="server" CssClass="labelRequired" Text="Valor: " />
                </td>                        
                <td>                                                                            
                    <dxe:ASPxSpinEdit ID="textValor" runat="server" ClientInstanceName="textValor" CssClass="textValor_5"
                    NumberType="Float" MaxLength="12" MinValue="0.01" MaxValue="99999999.99" DecimalPlaces=2 />                    
                </td>
                                   
                <td>
                    <table>
                        <tr>
                                   
                    <td class="td_Label">
                        <asp:Label ID="labelTipoOrdenacao" runat="server" CssClass="labelNormal" Text="Ordena Por: " />
                    </td>
                    
                    <td>
                        <dxe:ASPxComboBox ID="dropTipoOrdenacao" runat="server" CssClass="dropDownListCurto" 
                            ClientInstanceName="dropTipoOrdenacao" SelectedIndex=0>
                        <Items>
                            <dxe:ListEditItem Value="0" Text="Por Código" />
                            <dxe:ListEditItem Value="1" Text="Por Nome" />
                        </Items>
                        </dxe:ASPxComboBox>
                    </td>
                
                    </tr>
                    </table>
                </td>
            </tr>
        </table>                                                                                 
        
        </div>
    
        <div id="reportLinkButton" class="linkButton linkButtonNoBorder">
           <asp:LinkButton ID="btnVisualiza" runat="server" Font-Overline="false" CssClass="btnReport" OnClick="btnVisualiza_Click"><asp:Literal ID="Literal1" runat="server" Text="Visualizar"/><div></div></asp:LinkButton>
           <asp:LinkButton ID="btnPDF" runat="server" Font-Overline="false" CssClass="btnPdf" OnClick="btnPDF_Click"><asp:Literal ID="Literal2" runat="server" Text="Gerar PDF"/><div></div></asp:LinkButton>
           <asp:LinkButton ID="btnExcel" runat="server" Font-Overline="false"  CssClass="btnExcel" OnClick="btnExcel_Click"><asp:Literal ID="Literal3" runat="server" Text="Gerar Excel"/><div></div></asp:LinkButton>
        </div>
        
    </ContentTemplate>
    </asp:UpdatePanel>
    
    </div>
    
    </div>
    </td></tr></table>
    </div>
    <cc1:esDataSource ID="EsDSCarteira" runat="server" OnesSelect="EsDSCarteira_esSelect" />
    
   </form>
</body>
</html>