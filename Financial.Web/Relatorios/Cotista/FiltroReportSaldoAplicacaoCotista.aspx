﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="FiltroReportSaldoAplicacaoCotista.aspx.cs" Inherits="FiltroReportSaldoAplicacaoCotista" %>

<%@ Register Assembly="System.Web, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a" Namespace="System.Web.UI.HtmlControls" TagPrefix="cc2" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxcb" %>
<%@ Register Assembly="System.Web, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a" Namespace="System.Web.UI" TagPrefix="cc1" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxnb" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxe" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxwgv" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxpc" %>
<%@ Register Assembly="EntitySpaces.Web" Namespace="EntitySpaces.Web" TagPrefix="cc1" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >

<head id="Head1" runat="server">
<link href="~/css/forms.css" rel="stylesheet" type="text/css" />

<script type="text/javascript" language="JavaScript">
var backgroundColor;
function OnGetDataCarteira(values) {
    btnEditCodigo.SetValue(values);                    
    popupCarteira.HideWindow();
    ASPxCallback1.SendCallback(btnEditCodigo.GetValue());        
    btnEditCodigo.Focus();    
}
function OnGetDataCotista(values) {
    btnEditCodigoCotista.SetValue(values);                    
    popupCotista.HideWindow();
    ASPxCallback2.SendCallback(btnEditCodigoCotista.GetValue());        
    btnEditCodigoCotista.Focus();    
}
</script>
<script type="text/javascript" language="Javascript" src="../../js/global.js"></script>
</head>

<body >
    <form id="form1" runat="server"> 
    
    <dxcb:ASPxCallback ID="ASPxCallback1" runat="server" OnCallback="ASPxCallback1_Callback">
        <ClientSideEvents CallbackComplete="function(s, e) {
            OnCallBackCompleteCliente(s, e, popupMensagemCarteira, btnEditCodigo, document.getElementById('textNome'), textData);
        }        
        "/>
    </dxcb:ASPxCallback>
    
    <dxcb:ASPxCallback ID="ASPxCallback2" runat="server" OnCallback="ASPxCallback2_Callback">
        <ClientSideEvents CallbackComplete="function(s, e) {             
            OnCallBackCompleteCliente(s, e, popupMensagemCotista, btnEditCodigoCotista, document.getElementById('textNomeCotista'));
        }        
        "/>
    </dxcb:ASPxCallback>
        
    <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="true" EnableScriptLocalization="true" />
    
    <div class="divPanel">
    <table width="100%"><tr><td>
    <div id="container_small">
    
    <div id="header"><asp:Label ID="label3" runat="server" Text="Saldos de Aplicações de Cotistas" /></div>
        
    <div id="mainContentSpace">
    
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
    <ContentTemplate>
        
        <div class="reportFilter">
        
        <div style="height:20px"></div>                              
                                        
        <table>
            <tr>
            <td class="td_Label" >
                <asp:Label ID="labelCarteira" runat="server" CssClass="labelNormal" Text="Carteira:"></asp:Label>
            </td>
            
            <td>
                <dxe:ASPxSpinEdit ID="btnEditCodigo" runat="server" CssClass="textButtonEdit" 
                                            EnableClientSideAPI="True"
                                            ClientInstanceName="btnEditCodigo" SpinButtons-ShowIncrementButtons="false"
                                            MaxLength="9" NumberType="Integer" AllowMouseWheel="false">  
                <Buttons>
                    <dxe:EditButton></dxe:EditButton>                                
                </Buttons>        
                <ClientSideEvents                        
                         KeyPress="function(s, e) {document.getElementById('textNome').value = '';}" 
                         ButtonClick="function(s, e) {popupCarteira.ShowAtElementByID(s.name);}" 
                         LostFocus="function(s, e) {OnLostFocus(popupMensagemCarteira, ASPxCallback1, btnEditCodigo);}"
                />                            
                </dxe:ASPxSpinEdit>                
            </td>                          
            
            <td>
                <asp:TextBox ID="textNome" runat="server" CssClass="textNome" Enabled="false"></asp:TextBox>
            </td>
            </tr>
            
            <tr>
            <td class="td_Label">
                <asp:Label ID="labelCotista" runat="server" CssClass="labelNormal" Text="Cotista:"></asp:Label>
            </td>
            
            <td>
                <dxe:ASPxSpinEdit ID="btnEditCodigoCotista" runat="server" CssClass="textButtonEdit"
                                    EnableClientSideAPI="True" ClientInstanceName="btnEditCodigoCotista" 
                                    SpinButtons-ShowIncrementButtons="false">  
                <Buttons>
                    <dxe:EditButton></dxe:EditButton>
                </Buttons>
                <ClientSideEvents
                         KeyPress="function(s, e) {document.getElementById('textNomeCotista').value = '';}" 
                         ButtonClick="function(s, e) {popupCotista.ShowAtElementByID(s.name);}" 
                         LostFocus="function(s, e) {OnLostFocus(popupMensagemCotista, ASPxCallback2, btnEditCodigoCotista);}"
                />
                </dxe:ASPxSpinEdit>               
            </td>
            
            <td>
                <asp:TextBox ID="textNomeCotista" runat="server" CssClass="textNome" Enabled="false"></asp:TextBox>                
            </td>
            </tr>
                        
            <tr>
            <td class="td_Label" >
                <asp:Label ID="labelData" runat="server" CssClass="labelRequired" Text="Data:"></asp:Label>
            </td>
            
            <td colspan="2">
                <dxe:ASPxDateEdit ID="textData" runat="server" ClientInstanceName="textData"/>
            </td>                                                                
            </tr>
            
            <tr>
            <td class="td_Label" >
                <asp:Label ID="label1" runat="server" CssClass="labelNormal" Text="Campos:"></asp:Label>
            </td>
            
            <td colspan="2">
                <dxe:ASPxComboBox ID="dropOpcoesCampos" runat="server" CssClass="dropDownListCurto2" 
                            ClientInstanceName="dropOpcoesCampos" SelectedIndex="0">
                    <Items>
                        <dxe:ListEditItem Value="1" Text="Valor Pfee/Participação" />
                        <dxe:ListEditItem Value="2" Text="Rendimento/Retorno(%)" />
                    </Items>
                </dxe:ASPxComboBox>
            </td>
            </tr>
                       
            <tr>
            <td class="td_Label" >
                <asp:Label ID="labelValorAplicado" runat="server" CssClass="labelNormal" Text="Vl Aplicado:"></asp:Label>
            </td>
            
            <td colspan="2">
                <dxe:ASPxComboBox ID="dropValorAplicado" runat="server" CssClass="dropDownListCurto" 
                            ClientInstanceName="dropValorAplicado" SelectedIndex="0">
                    <Items>
                        <dxe:ListEditItem Value="1" Text="Valor Atualizado" />
                        <dxe:ListEditItem Value="2" Text="Valor Original" />
                    </Items>
                </dxe:ASPxComboBox>
            </td>
            </tr>
            
            <tr>
            <td class="td_Label" >
                <asp:Label ID="labelTipo" runat="server" CssClass="labelNormal" Text="Tipo:"></asp:Label>
            </td>
            
            <td colspan="2">
                <dxe:ASPxComboBox ID="dropTipoRelatorio" runat="server" CssClass="dropDownListCurto" 
                            ClientInstanceName="dropTipoRelatorio" SelectedIndex="0">
                    <Items>
                        <dxe:ListEditItem Value="1" Text="Analítico" />
                        <dxe:ListEditItem Value="2" Text="Consolidado" />
                    </Items>
                </dxe:ASPxComboBox>
            </td>
            </tr>
            
            <tr>
            <td class="td_Label" >
                <asp:Label ID="labelOrdena" runat="server" CssClass="labelNormal" Text="Ordena:"></asp:Label>
            </td>
            
            <td colspan="2">
                <dxe:ASPxComboBox ID="dropTipoGrupamento" runat="server" CssClass="dropDownListCurto" 
                            ClientInstanceName="dropTipoGrupamento" SelectedIndex="0">
                    <Items>
                        <dxe:ListEditItem Value="1" Text="Por Nome" />
                        <dxe:ListEditItem Value="2" Text="Por Código" />
                    </Items>
                </dxe:ASPxComboBox>
            </td>
            </tr>
            
            <tr>                                   
                <td class="td_Label">
                    <asp:Label ID="labelLingua" runat="server" CssClass="labelNormal" Text="Lingua: " />
                </td>
                    
                <td colspan="3">
                    <dxe:ASPxComboBox ID="dropLingua" runat="server" CssClass="dropDownListCurto" 
                            ClientInstanceName="dropLingua" SelectedIndex="0">
                    <Items>
                        <dxe:ListEditItem Value="1" Text="Português" />
                        <dxe:ListEditItem Value="2" Text="Inglês" />
                    </Items>
                    </dxe:ASPxComboBox>
                </td>            
            </tr>
            
            <tr>                
            <td colspan="2">
                <asp:CheckBox ID="chkRentabilidade" runat="server" Checked = "false" />
                <asp:Label ID="label2" runat="server" CssClass="labelNormal" Text="Incluir Retornos"/>
            </td>            
            </tr>

        </table>                                                                                 
        
        </div>
        
         <div id="reportLinkButton" class="linkButton linkButtonNoBorder">
           <asp:LinkButton ID="btnVisualiza" runat="server" Font-Overline="false" CssClass="btnReport" OnClick="btnVisualiza_Click"><asp:Literal ID="Literal1" runat="server" Text="Visualizar"/><div></div></asp:LinkButton>
           <asp:LinkButton ID="btnPDF" runat="server" Font-Overline="false" CssClass="btnPdf" OnClick="btnPDF_Click"><asp:Literal ID="Literal2" runat="server" Text="Gerar PDF"/><div></div></asp:LinkButton>
           <asp:LinkButton ID="btnExcel" runat="server" Font-Overline="false"  CssClass="btnExcel" OnClick="btnExcel_Click"><asp:Literal ID="Literal3" runat="server" Text="Gerar Excel"/><div></div></asp:LinkButton>
        </div>
        
        
    </ContentTemplate>
    </asp:UpdatePanel>
    
    </div>
    
    </div>
    </td></tr></table>
    </div>
            
    <cc1:esDataSource ID="EsDSCarteira" runat="server" OnesSelect="EsDSCarteira_esSelect" />
    <cc1:esDataSource ID="EsDSCotista" runat="server" OnesSelect="EsDSCotista_esSelect" />
        
   </form>
</body>
</html>