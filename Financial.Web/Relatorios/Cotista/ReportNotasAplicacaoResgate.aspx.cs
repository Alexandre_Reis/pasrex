﻿using System;
using Financial.Web.Common;
using Financial.InvestidorCotista;
using Financial.Relatorio;
using Financial.Fundo.Enums;
using System.IO;
using System.Web;
using System.Globalization;
using Financial.Web.Enums;
using Financial.Security;
using Financial.InvestidorCotista.Enums;

public partial class _ReportNotasAplicacaoResgate : BasePage {
    new protected void Page_Load(object sender, EventArgs e) {
        string visao = Request.QueryString["Visao"].ToString();
        // Dados passados pelo filtro
        int? idCarteiraFiltro = null;
        int? idCotistaFiltro = null;
        byte? tipoOperacaoFiltro = null;
        int? idOperacaoFiltro = null;
        //
        DateTime? dataInicio = null;
        DateTime? dataFim = null;

        #region Salva os Parametros
        if (!String.IsNullOrEmpty((string)Session["btnEditCodigoCarteira"])) {
            idCarteiraFiltro = Convert.ToInt32(Session["btnEditCodigoCarteira"]);
        }
        if (!String.IsNullOrEmpty((string)Session["btnEditCodigoCotista"])) {
            idCotistaFiltro = Convert.ToInt32(Session["btnEditCodigoCotista"]);
        }
        if (!String.IsNullOrEmpty((string)Session["dropTipoOperacao"])) {
            tipoOperacaoFiltro = Convert.ToByte(Session["dropTipoOperacao"]);
        }
        if (!String.IsNullOrEmpty((string)Session["textNota"])) {
            idOperacaoFiltro = Convert.ToInt32(Session["textNota"]);
        }
        //
        if (!String.IsNullOrEmpty((string)Session["textDataInicio"])) {
            dataInicio = Convert.ToDateTime(Session["textDataInicio"]);
        }
        if (!String.IsNullOrEmpty((string)Session["textDataFim"])) {
            dataFim = Convert.ToDateTime(Session["textDataFim"]);
        }

        TipoLingua tipoLingua = (TipoLingua)Convert.ToInt32(Session["dropLingua"]);
        #endregion

        //Monta loop para processar a impressão/pdf de todas as notas trazidas
        OperacaoCotistaCollection operacaoCotistaCollection = new OperacaoCotistaCollection();

        string login = HttpContext.Current.User.Identity.Name;

        OperacaoCotistaQuery operacaoCotistaQuery = new OperacaoCotistaQuery("O");
        UsuarioQuery usuarioQuery = new UsuarioQuery("U");
        PermissaoCotistaQuery permissaoCotistaQuery = new PermissaoCotistaQuery("P");

        #region Monta a Clausula where
        operacaoCotistaQuery.InnerJoin(permissaoCotistaQuery).On(permissaoCotistaQuery.IdCotista == operacaoCotistaQuery.IdCotista);
        operacaoCotistaQuery.InnerJoin(usuarioQuery).On(usuarioQuery.IdUsuario == permissaoCotistaQuery.IdUsuario);
        operacaoCotistaQuery.Where(usuarioQuery.Login.Equal(login));
        operacaoCotistaQuery.Where(operacaoCotistaQuery.IdCotista.NotEqual(operacaoCotistaQuery.IdCarteira),
                                   operacaoCotistaQuery.TipoOperacao.In((byte)TipoOperacaoCotista.Aplicacao,
                                                                      (byte)TipoOperacaoCotista.AplicacaoAcoesEspecial,
                                                                      (byte)TipoOperacaoCotista.IncorporacaoAplicacao,
                                                                      (byte)TipoOperacaoCotista.IncorporacaoResgate,
                                                                      (byte)TipoOperacaoCotista.ResgateBruto,
                                                                      (byte)TipoOperacaoCotista.ResgateCotas,
                                                                      (byte)TipoOperacaoCotista.ResgateLiquido,
                                                                      (byte)TipoOperacaoCotista.ResgateTotal));

        if (idCarteiraFiltro != null) {
            operacaoCotistaQuery.Where(operacaoCotistaQuery.IdCarteira == idCarteiraFiltro.Value);
        }
        if (idCotistaFiltro != null) {
            operacaoCotistaQuery.Where(operacaoCotistaQuery.IdCotista == idCotistaFiltro.Value);
        }
        if (tipoOperacaoFiltro != null) {
            operacaoCotistaQuery.Where(operacaoCotistaQuery.TipoOperacao == tipoOperacaoFiltro.Value);
        }
        if (idOperacaoFiltro != null) {
            operacaoCotistaQuery.Where(operacaoCotistaQuery.IdOperacao == idOperacaoFiltro.Value);
        }
        
        if (dataInicio != null) {
            operacaoCotistaQuery.Where(operacaoCotistaQuery.DataOperacao.GreaterThanOrEqual(dataInicio));
        }
        if (dataFim != null) {
            operacaoCotistaQuery.Where(operacaoCotistaQuery.DataOperacao.LessThanOrEqual(dataFim));
        }
        #endregion
  
        operacaoCotistaCollection.Load(operacaoCotistaQuery);
        //

        string fileName = "NotasAplicResgate";
        ReportMaster reportMaster = new ReportMaster();
        //reportMaster.CreateDocument(); //If this method is executed, I get garbage information on the first page!!!!
        foreach (OperacaoCotista operacaoCotista in operacaoCotistaCollection) {
            int idOperacao = operacaoCotista.IdOperacao.Value;
            int tipoOperacao = operacaoCotista.TipoOperacao.Value;

            if (operacaoCotistaCollection.Count == 1) {
                if (tipoOperacao == (byte)TipoOperacaoFundo.Aplicacao || tipoOperacao == (byte)TipoOperacaoFundo.AplicacaoAcoesEspecial) {
                    fileName = "NotaAplicacao-" + idOperacao.ToString();
                }
                else {
                    fileName = "NotaResgate-" + idOperacao.ToString();
                }
            }

            if (tipoOperacao == (byte)TipoOperacaoFundo.Aplicacao || tipoOperacao == (byte)TipoOperacaoFundo.AplicacaoAcoesEspecial) {
                ReportNotaAplicacao report = new ReportNotaAplicacao(idOperacao, tipoLingua);
                report.CreateDocument();
                reportMaster.PrintingSystem.Pages.AddRange(report.PrintingSystem.Pages);
            }
            else {
                ReportNotaResgate report = new ReportNotaResgate(idOperacao, tipoLingua);
                report.CreateDocument();
                reportMaster.PrintingSystem.Pages.AddRange(report.PrintingSystem.Pages);
            }
        }

        if (visao == "Report") {
            ReportViewer1.Report = reportMaster;
        }
        else if (visao == "PDF") {
            MemoryStream ms = new MemoryStream();
            reportMaster.ExportToPdf(ms);
            ms.Seek(0, SeekOrigin.Begin);
            Response.ClearContent();
            Response.ClearHeaders();
            Response.Buffer = true;
            Response.Cache.SetCacheability(HttpCacheability.Private);
            Response.ContentType = "application/pdf";
            Response.AddHeader("Content-Length", ms.Length.ToString(CultureInfo.CurrentCulture));
            Response.AddHeader("Content-Disposition", String.Format("attachment;filename=" + fileName + ".pdf"));

            Response.BinaryWrite(ms.ToArray());
            ms.Close();
            HttpContext.Current.ApplicationInstance.CompleteRequest();
            Response.End();
        }
    }
}
