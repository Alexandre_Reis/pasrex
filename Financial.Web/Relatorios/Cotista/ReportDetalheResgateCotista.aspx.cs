﻿using Financial.Web.Common;
using System;
using Financial.Relatorio;
using System.IO;
using System.Web;
using System.Globalization;

public partial class _ReportDetalheResgateCotista : BasePage {
    private static class VisaoRelatorio {
        public static string visaoHTML = "Report";
        public static string visaoPDF = "PDF";
        public static string visaoExcel = "Excel";
    }

    new protected void Page_Load(object sender, EventArgs e) {            
        string visao = Request.QueryString["Visao"].ToString();
        
        // Obrigatório
        DateTime dataInicio = Convert.ToDateTime(Session["textDataInicio"]);
        DateTime dataFim = Convert.ToDateTime(Session["textDataFim"]);
        int? idCarteira = null;
        int? idCotista = null;

        if (!String.IsNullOrEmpty((string)Session["btnEditCodigoCarteira"])) {
            idCarteira = Convert.ToInt32(Session["btnEditCodigoCarteira"]);
        }
               
        if (!String.IsNullOrEmpty((string)Session["btnEditCodigoCotista"])) {
            idCotista = Convert.ToInt32(Session["btnEditCodigoCotista"]);
        }

        string fileName = "ReportDetalheResgateCotista";

        if (visao == VisaoRelatorio.visaoHTML) {
            ReportViewer1.Report = new ReportDetalheResgateCotista(idCarteira, idCotista, dataInicio, dataFim);
        }
        else if (visao == VisaoRelatorio.visaoPDF) {
            #region PDF
            ReportDetalheResgateCotista report = new ReportDetalheResgateCotista(idCarteira, idCotista, dataInicio, dataFim);

            MemoryStream ms = new MemoryStream();
            report.ExportToPdf(ms);
            ms.Seek(0, SeekOrigin.Begin);
            Response.ClearContent();
            Response.ClearHeaders();
            Response.Buffer = true;
            Response.Cache.SetCacheability(HttpCacheability.Private);
            Response.ContentType = "application/pdf";
            Response.AddHeader("Content-Length", ms.Length.ToString(CultureInfo.CurrentCulture));
            Response.AddHeader("Content-Disposition", String.Format("attachment;filename=" + fileName + ".pdf"));

            Response.BinaryWrite(ms.ToArray());
            ms.Close();
            HttpContext.Current.ApplicationInstance.CompleteRequest();
            Response.End();
            #endregion
        }
        else if (visao == VisaoRelatorio.visaoExcel) {
            #region Excel
            ReportDetalheResgateCotista report = new ReportDetalheResgateCotista(idCarteira, idCotista, dataInicio, dataFim);

            MemoryStream ms = new MemoryStream();
            report.ExportToXls(ms);
            ms.Seek(0, SeekOrigin.Begin);
            Response.ClearContent();
            Response.ClearHeaders();
            Response.Buffer = true;
            Response.Cache.SetCacheability(HttpCacheability.Private);
            Response.ContentType = "application/vnd.ms-excel";
            Response.AddHeader("Content-Length", ms.Length.ToString(CultureInfo.CurrentCulture));
            Response.AddHeader("Content-Disposition", String.Format("attachment;filename=" + fileName + ".xls"));

            Response.BinaryWrite(ms.ToArray());
            ms.Close();
            HttpContext.Current.ApplicationInstance.CompleteRequest();
            Response.End();

            #endregion
        }
    }
}