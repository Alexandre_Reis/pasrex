﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using Financial.Fundo;
using Financial.Investidor;
using Financial.InvestidorCotista;
using Financial.Security;
using System.Collections.Generic;
using EntitySpaces.Interfaces;
using Financial.Security.Enums;
using System.Text;
using Financial.Web.Common;

public partial class FiltroReportNotasAplicacaoResgate : FiltroReportBasePage {
    new protected void Page_Load(object sender, EventArgs e) {
        btnEditCodigoCarteira.Focus();
        this.dropTipoOperacao.Items.Insert(0, "");

        this.HasPopupCotista = true;
        this.HasPopupCarteira = true;
        base.Page_Load(sender, e);

        TrataTravamentoCampos();
    }

    // Numero de Notas Máximo que podem ser geradas
    const int NUMERO_NOTAS_MAXIMO = 500;
    const string MSG_FILTRO = "Quantidade Máxima de Notas Excedido. Por Favor Filtre a Pesquisa";
    const string NO_RECORD = "Registros não encontrados";

    /// <summary></summary>
    /// <returns></returns>
    /// <exception cref="Exception">
    /// Se Numero de operações for maior que a Constante NUMERO_NOTAS_MAXIMO
    /// </exception>
    private bool RegistrosEncontrados() {
        // Dados passados pelo filtro
        int? idCarteiraFiltro = null;
        int? idCotistaFiltro = null;
        byte? tipoOperacaoFiltro = null;
        int? idOperacaoFiltro = null;
        //
        DateTime? dataInicio = null;
        DateTime? dataFim = null;

        #region Salva os Parametros
        if (!String.IsNullOrEmpty(this.btnEditCodigoCarteira.Text)) {
            idCarteiraFiltro = Convert.ToInt32(this.btnEditCodigoCarteira.Text);
        }
        if (!String.IsNullOrEmpty(this.btnEditCodigoCotista.Text)) {
            idCotistaFiltro = Convert.ToInt32(this.btnEditCodigoCotista.Text);
        }
        if (!String.IsNullOrEmpty(this.dropTipoOperacao.SelectedValue)) {
            tipoOperacaoFiltro = Convert.ToByte(this.dropTipoOperacao.SelectedValue);
        }
        if (!String.IsNullOrEmpty(this.textNota.Text)) {
            idOperacaoFiltro = Convert.ToInt32(this.textNota.Text);
        }
        //
        if (!String.IsNullOrEmpty(this.textDataInicio.Text)) {
            dataInicio = Convert.ToDateTime(this.textDataInicio.Text);
        }
        if (!String.IsNullOrEmpty(this.textDataFim.Text)) {
            dataFim = Convert.ToDateTime(this.textDataFim.Text);
        }
        #endregion

        //Monta loop para processar a impressão/pdf de todas as notas trazidas
        OperacaoCotistaCollection operacaoCotistaCollection = new OperacaoCotistaCollection();

        string login = HttpContext.Current.User.Identity.Name;

        OperacaoCotistaQuery operacaoCotistaQuery = new OperacaoCotistaQuery("O");
        UsuarioQuery usuarioQuery = new UsuarioQuery("U");
        PermissaoCotistaQuery permissaoCotistaQuery = new PermissaoCotistaQuery("P");
     
        #region Monta a Clausula where
        operacaoCotistaQuery.InnerJoin(permissaoCotistaQuery).On(permissaoCotistaQuery.IdCotista == operacaoCotistaQuery.IdCotista);
        operacaoCotistaQuery.InnerJoin(usuarioQuery).On(usuarioQuery.IdUsuario == permissaoCotistaQuery.IdUsuario);
        operacaoCotistaQuery.Where(usuarioQuery.Login.Equal(login));
        operacaoCotistaQuery.Where(operacaoCotistaQuery.IdCotista.NotEqual(operacaoCotistaQuery.IdCarteira));
                
        if (idCarteiraFiltro != null) {
            operacaoCotistaQuery.Where(operacaoCotistaQuery.IdCarteira == idCarteiraFiltro.Value);
        }
        if (idCotistaFiltro != null) {
            operacaoCotistaQuery.Where(operacaoCotistaQuery.IdCotista == idCotistaFiltro.Value);
        }
        if (tipoOperacaoFiltro != null) {
            operacaoCotistaQuery.Where(operacaoCotistaQuery.TipoOperacao == tipoOperacaoFiltro.Value);
        }
        if (idOperacaoFiltro != null) {
            operacaoCotistaQuery.Where(operacaoCotistaQuery.IdOperacao == idOperacaoFiltro.Value);
        }
        //
        if (dataInicio != null) {
            operacaoCotistaQuery.Where(operacaoCotistaQuery.DataOperacao.GreaterThanOrEqual(dataInicio));
        }
        if (dataFim != null) {
            operacaoCotistaQuery.Where(operacaoCotistaQuery.DataOperacao.LessThanOrEqual(dataFim));
        }
        #endregion

        operacaoCotistaCollection.Load(operacaoCotistaQuery);
        //
        if (operacaoCotistaCollection.Count > NUMERO_NOTAS_MAXIMO) {
            throw new Exception(MSG_FILTRO);
        }

        return operacaoCotistaCollection.Count > 0;
    }

    /// <summary>
    /// 
    /// </summary>
    private void TrataTravamentoCampos() {
        Usuario usuario = new Usuario();
        usuario.BuscaUsuario(HttpContext.Current.User.Identity.Name);
        int tipoTrava = usuario.TipoTrava.Value;

        if (tipoTrava == (int)TipoTravaUsuario.TravaCliente) {
            //Travamento do campo de carteira
            btnEditCodigoCarteira.Enabled = false;

            PermissaoCliente permissaoCliente = new PermissaoCliente();
            int? idCliente = permissaoCliente.RetornaClienteAssociado(HttpContext.Current.User.Identity.Name);

            if (idCliente.HasValue) {
                Cliente cliente = new Cliente();

                List<esQueryItem> campos = new List<esQueryItem>();
                campos.Add(cliente.Query.Apelido);
                cliente.LoadByPrimaryKey(campos, idCliente.Value);
                string apelido = cliente.Apelido;

                btnEditCodigoCarteira.Text = Convert.ToString(idCliente.Value);
                textNomeCarteira.Text = apelido;
            }
        }

        if (tipoTrava == (int)TipoTravaUsuario.TravaCotista) {
            //Travamento do campo de cotista
            btnEditCodigoCotista.Enabled = false;

            PermissaoCotista permissaoCotista = new PermissaoCotista();
            int? idCotista = permissaoCotista.RetornaCotistaAssociado(HttpContext.Current.User.Identity.Name);

            if (idCotista.HasValue) {
                Cotista cotista = new Cotista();

                List<esQueryItem> campos = new List<esQueryItem>();
                campos.Add(cotista.Query.Apelido);
                cotista.LoadByPrimaryKey(campos, idCotista.Value);
                string apelido = cotista.Apelido;

                btnEditCodigoCotista.Text = Convert.ToString(idCotista.Value);
                textNomeCotista.Text = apelido;
            }
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void ASPxCallback1_Callback(object source, DevExpress.Web.CallbackEventArgs e) {
        string nome = "";
        if (!String.IsNullOrEmpty(e.Parameter) && e.Parameter != "null") {
            int idCarteira = Convert.ToInt32(e.Parameter);
            Carteira carteira = new Carteira();
            carteira.LoadByPrimaryKey(idCarteira);

            if (carteira.str.Apelido != "") {
                if (carteira.IsAtivo) {
                    //O permissionamento da carteira é dado direto no cliente vinculado à carteira
                    PermissaoCliente permissaoCliente = new PermissaoCliente();
                    if (permissaoCliente.RetornaAcessoClienteComControle(idCarteira, HttpContext.Current.User.Identity.Name)) {
                        nome = carteira.str.Apelido;
                    }
                    else {
                        nome = "no_access";
                    }
                }
                else {
                    nome = "no_active";
                }
            }
        }
        e.Result = nome;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void ASPxCallback2_Callback(object source, DevExpress.Web.CallbackEventArgs e) {
        string nome = "";
        if (!String.IsNullOrEmpty(e.Parameter) && e.Parameter != "null") {
            int idCotista = Convert.ToInt32(e.Parameter);
            Cotista cotista = new Cotista();
            cotista.LoadByPrimaryKey(idCotista);

            if (cotista.str.Apelido != "") {
                if (cotista.IsAtivo) {
                    PermissaoCotista permissaoCotista = new PermissaoCotista();
                    if (permissaoCotista.RetornaAcessoCotista(idCotista, HttpContext.Current.User.Identity.Name)) {
                        nome = cotista.str.Apelido;
                    }
                    else {
                        nome = "no_access";
                    }
                }
                else {
                    nome = "no_active";
                }
            }
        }
        e.Result = nome;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void EsDSCarteira_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e) {
        CarteiraCollection carteiraCollection = new CarteiraCollection();
        carteiraCollection.BuscaCarteirasComAcesso(HttpContext.Current.User.Identity.Name);

        e.Collection = carteiraCollection;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void EsDSCotista_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e) {
        CotistaCollection cotistaCollection = new CotistaCollection();
        cotistaCollection.BuscaCotistasComAcesso(HttpContext.Current.User.Identity.Name);

        e.Collection = cotistaCollection;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnVisualiza_Click(object sender, EventArgs e) {
        this.TrataErros();
        this.SelecionaRelatorio(VisaoRelatorio.visaoHTML);
    }

    /// <summary>
    ///
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnPDF_Click(object sender, EventArgs e) {
        this.TrataErros();
        this.SelecionaRelatorio(VisaoRelatorio.visaoPDF);
    }

    /// <summary>
    /// Trata os Erros de Relatorio
    /// </summary>
    private void TrataErros() {
        if (!this.RegistrosEncontrados()) {
            throw new Exception(NO_RECORD);
        }
    }

    /// <summary>
    /// Seleciona o relatorio a ser executado
    /// </summary>
    /// <param name="visao">PDF, Excel ou Report</param>
    private void SelecionaRelatorio(string visao) {
        Response.BufferOutput = true;
        //
        // Salva os parametros na session
        Session["textDataInicio"] = this.textDataInicio.Text;
        Session["textDataFim"] = this.textDataFim.Text;
        Session["btnEditCodigoCotista"] = this.btnEditCodigoCotista.Text;
        Session["btnEditCodigoCarteira"] = this.btnEditCodigoCarteira.Text;
        Session["dropTipoOperacao"] = this.dropTipoOperacao.SelectedValue;
        Session["textNota"] = this.textNota.Text;
        Session["dropLingua"] = this.dropLingua.SelectedItem.Value;

        //Response.Redirect("~/Relatorios/Cotista/ReportNotasAplicacaoResgate.aspx?Visao=" + visao);

        string url = "~/Relatorios/Cotista/ReportNotasAplicacaoResgate.aspx?Visao=" + visao;
        string redirectURL = Page.ResolveClientUrl(url);

        string script = "window.location = '" + redirectURL + "';";
        ScriptManager.RegisterStartupScript(this, typeof(Page), "RedirectTo", script, true);
    }
}