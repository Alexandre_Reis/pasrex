using System;
using Financial.Relatorio;
using System.IO;
using System.Web;
using System.Globalization;
using Financial.Web.Common;

public partial class _ReportStatusInvestidores : BasePage {
    private static class VisaoRelatorio {
        public static string visaoHTML = "Report";
        public static string visaoPDF = "PDF";
        public static string visaoExcel = "Excel";
    }

    new protected void Page_Load(object sender, EventArgs e) {
        string visao = Request.QueryString["Visao"].ToString();

        

        // Obrigatorio
        DateTime dataInicio = Convert.ToDateTime(Session["textDataInicio"]);
        DateTime dataFim = Convert.ToDateTime(Session["textDataFim"]);
        
        //
        // Opcional
        int? idCotista = null;
        if (!String.IsNullOrEmpty((string)Session["btnEditCodigoCotista"]))
        {
            idCotista = Convert.ToInt32(Session["btnEditCodigoCotista"]);
        }


        //if (this.IsPostBack)
        {
            if (visao == VisaoRelatorio.visaoHTML)
            {
                ReportViewer1.Report = new ReportStatusInvestidores(idCotista, dataInicio, dataFim);
            }
            else if (visao == VisaoRelatorio.visaoPDF)
            {
                #region PDF
                ReportStatusInvestidores report = new ReportStatusInvestidores(idCotista, dataInicio, dataFim);

                MemoryStream ms = new MemoryStream();
                report.ExportToPdf(ms);
                ms.Seek(0, SeekOrigin.Begin);
                Response.ClearContent();
                Response.ClearHeaders();
                Response.Buffer = true;
                Response.Cache.SetCacheability(HttpCacheability.Private);
                Response.ContentType = "application/pdf";
                Response.AddHeader("Content-Length", ms.Length.ToString(CultureInfo.CurrentCulture));
                Response.AddHeader("Content-Disposition", String.Format("attachment;filename=ReportStatusInvestidores.pdf"));

                Response.BinaryWrite(ms.ToArray());
                ms.Close();
                HttpContext.Current.ApplicationInstance.CompleteRequest();
                Response.End();
                #endregion
            }
            else if (visao == VisaoRelatorio.visaoExcel)
            {
                #region Excel
                ReportStatusInvestidores report = new ReportStatusInvestidores(idCotista, dataInicio, dataFim);

                MemoryStream ms = new MemoryStream();
                report.ExportToXls(ms);
                ms.Seek(0, SeekOrigin.Begin);
                Response.ClearContent();
                Response.ClearHeaders();
                Response.Buffer = true;
                Response.Cache.SetCacheability(HttpCacheability.Private);
                Response.ContentType = "application/vnd.ms-excel";
                Response.AddHeader("Content-Length", ms.Length.ToString(CultureInfo.CurrentCulture));
                Response.AddHeader("Content-Disposition", String.Format("attachment;filename=ReportStatusInvestidores.xls"));

                Response.BinaryWrite(ms.ToArray());
                ms.Close();
                HttpContext.Current.ApplicationInstance.CompleteRequest();
                Response.End();

                #endregion
            }
        }
    }
}