﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using Financial.Fundo;
using Financial.Investidor;
using Financial.InvestidorCotista;
using Financial.Security;
using System.Collections.Generic;
using EntitySpaces.Interfaces;
using Financial.Security.Enums;
using System.Text;
using Financial.Web.Common;

public partial class FiltroReportMovimentacaoCotista : FiltroReportBasePage {
    new protected void Page_Load(object sender, EventArgs e) {
        btnEditCodigoCarteira.Focus();

        this.HasPopupCarteira = true;
        this.HasPopupCotista = true;
        base.Page_Load(sender, e);

        TrataTravamentoCampos();
    }

    private void TrataTravamentoCampos() {
        Usuario usuario = new Usuario();
        usuario.BuscaUsuario(HttpContext.Current.User.Identity.Name);
        int tipoTrava = usuario.TipoTrava.Value;

        if (tipoTrava == (int)TipoTravaUsuario.TravaCliente) {
            //Travamento do campo de carteira
            btnEditCodigoCarteira.Enabled = false;

            PermissaoCliente permissaoCliente = new PermissaoCliente();
            int? idCliente = permissaoCliente.RetornaClienteAssociado(HttpContext.Current.User.Identity.Name);

            if (idCliente.HasValue) {
                Cliente cliente = new Cliente();

                List<esQueryItem> campos = new List<esQueryItem>();
                campos.Add(cliente.Query.Apelido);
                cliente.LoadByPrimaryKey(campos, idCliente.Value);
                string apelido = cliente.Apelido;

                btnEditCodigoCarteira.Text = Convert.ToString(idCliente.Value);
                textNomeCarteira.Text = apelido;
            }
        }

        if (tipoTrava == (int)TipoTravaUsuario.TravaCotista) {
            //Travamento do campo de cotista
            btnEditCodigoCotista.Enabled = false;

            PermissaoCotista permissaoCotista = new PermissaoCotista();
            int? idCotista = permissaoCotista.RetornaCotistaAssociado(HttpContext.Current.User.Identity.Name);

            if (idCotista.HasValue) {
                Cotista cotista = new Cotista();

                List<esQueryItem> campos = new List<esQueryItem>();
                campos.Add(cotista.Query.Apelido);
                cotista.LoadByPrimaryKey(campos, idCotista.Value);
                string apelido = cotista.Apelido;

                btnEditCodigoCotista.Text = Convert.ToString(idCotista.Value);
                textNomeCotista.Text = apelido;
            }
        }
    }

    protected void ASPxCallback1_Callback(object source, DevExpress.Web.CallbackEventArgs e) {
        string nome = "";
        if (!String.IsNullOrEmpty(e.Parameter) && e.Parameter != "null") {
            int idCarteira = Convert.ToInt32(e.Parameter);
            Carteira carteira = new Carteira();
            carteira.LoadByPrimaryKey(idCarteira);

            if (carteira.str.Apelido != "") {
                if (carteira.IsAtivo) {
                    //O permissionamento da carteira é dado direto no cliente vinculado à carteira
                    PermissaoCliente permissaoCliente = new PermissaoCliente();
                    nome = permissaoCliente.RetornaAcessoClienteComControle(idCarteira, HttpContext.Current.User.Identity.Name)
                        ? carteira.str.Apelido : "no_access";
                }
                else {
                    nome = "no_active";
                }
            }
        }
        e.Result = nome;
    }

    protected void ASPxCallback2_Callback(object source, DevExpress.Web.CallbackEventArgs e) {
        string nome = "";
        if (!String.IsNullOrEmpty(e.Parameter) && e.Parameter != "null") {
            int idCotista = Convert.ToInt32(e.Parameter);
            Cotista cotista = new Cotista();
            cotista.LoadByPrimaryKey(idCotista);

            if (cotista.str.Apelido != "") {
                if (cotista.IsAtivo) {
                    PermissaoCotista permissaoCotista = new PermissaoCotista();
                    nome = permissaoCotista.RetornaAcessoCotista(idCotista, HttpContext.Current.User.Identity.Name)
                            ? cotista.str.Apelido : "no_access";
                }
                else {
                    nome = "no_active";
                }
            }
        }
        e.Result = nome;
    }

    protected void EsDSCarteira_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e) {
        CarteiraCollection carteiraCollection = new CarteiraCollection();
        carteiraCollection.BuscaCarteirasComAcesso(HttpContext.Current.User.Identity.Name);

        e.Collection = carteiraCollection;
    }

    protected void EsDSCotista_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e) {
        CotistaCollection cotistaCollection = new CotistaCollection();
        cotistaCollection.BuscaCotistasComAcesso(HttpContext.Current.User.Identity.Name);

        e.Collection = cotistaCollection;
    }

    protected void btnVisualiza_Click(object sender, EventArgs e) {
        this.TrataErros();
        this.SelecionaRelatorio(VisaoRelatorio.visaoHTML);
    }

    protected void btnPDF_Click(object sender, EventArgs e) {
        this.TrataErros();
        this.SelecionaRelatorio(VisaoRelatorio.visaoPDF);
    }

    protected void btnExcel_Click(object sender, EventArgs e) {
        this.TrataErros();
        this.SelecionaRelatorio(VisaoRelatorio.visaoExcel);
    }

    /// <summary>
    /// Trata os Erros de Relatorio
    /// </summary>
    private void TrataErros() {

        #region Campos obrigatórios
        if (this.textDataInicioOperacao.Text == "" && this.textDataFimOperacao.Text == "" &&
            this.textDataInicioConversao.Text == "" && this.textDataFimConversao.Text == "" &&
            this.textDataInicioLiquidacao.Text == "" && this.textDataFimLiquidacao.Text == "") {

            throw new Exception("Algum período de datas deve ser escolhido.");
        }

        /*
        [0] dataInicioOperacao
        [1] dataFimOperacao
        [2] dataInicioConversao
        [3] dataFimConversao
        [4] dataInicioLiquidacao
        [5] dataFimLiquidacao
        */
        bool[] isDatasVazia = new bool[] {     this.textDataInicioOperacao.Text == "", this.textDataFimOperacao.Text == "",
                                               this.textDataInicioConversao.Text == "", this.textDataFimConversao.Text == "",
                                               this.textDataInicioLiquidacao.Text == "", this.textDataFimLiquidacao.Text == ""
                                         };

        if (this.TemMaisDeUmParDatasPrenchida(isDatasVazia)) {
            throw new Exception("Preencha somente 1 par de Data Início e Data Fim");
        }
        
        // Se um par de datas não foi preenchido
        if (!this.TemUmParDatasPrenchida(isDatasVazia)) {

            // Se somente uma das datas do par está preenchida                                
            // DataOperação
            if (!isDatasVazia[0] && isDatasVazia[1] ||
                isDatasVazia[0] && !isDatasVazia[1]) {

                string d = isDatasVazia[0] ? "Início" : "Fim";
                throw new Exception(String.Format("Data Operação {0} deve ser prenchida.", d));
            }

            // DataConversão
            else if (!isDatasVazia[2] && isDatasVazia[3] ||
                     isDatasVazia[2] && !isDatasVazia[3]) {

                string d = isDatasVazia[2] ? "Início" : "Fim";
                throw new Exception(String.Format("Data Conversão {0} deve ser prenchida.", d));
            }

        // DataLiquidação
            else if (!isDatasVazia[4] && isDatasVazia[5] ||
                     isDatasVazia[4] && !isDatasVazia[5]) {

                string d = isDatasVazia[4] ? "Início" : "Fim";
                throw new Exception(String.Format("Data Liquidação {0} deve ser prenchida.", d));
            }
        }
        #endregion
    }

    /// <summary>
    /// Retorna true se tem um par de dataInicio e dataFim Prenchido
    /// </summary>
    /// <param name="datas"> vetor de bool indicando se data está vazia ou não
    ///  [0] dataInicioOperacao
    ///  [1] dataFimOperacao
    ///  [2] dataInicioConversao
    ///  [3] dataFimConversao
    ///  [4] dataInicioLiquidacao
    ///  [5] dataFimLiquidacao
    /// </param>
    /// <returns></returns>
    private bool TemUmParDatasPrenchida(bool[] datas) {
        bool retorno = false;

        if (!datas[0] && !datas[1]) {
            retorno = true;
        }
        else if (!datas[2] && !datas[3]) {
            retorno = true;
        }
        else if (!datas[4] && !datas[5]) {
            retorno = true;
        }
        return retorno;
    }

    /// <summary>
    /// Retorna true se tem mais de um par de dataInicio e dataFim Prenchido
    /// </summary>
    /// <param name="datas"> vetor de bool indicando se data está vazia ou não
    ///  [0] dataInicioOperacao
    ///  [1] dataFimOperacao
    ///  [2] dataInicioConversao
    ///  [3] dataFimConversao
    ///  [4] dataInicioLiquidacao
    ///  [5] dataFimLiquidacao
    /// </param>
    /// <returns></returns>
    private bool TemMaisDeUmParDatasPrenchida(bool[] datas) {
        bool retorno = false;

        // 1 com 2
        if (!datas[0] && !datas[1] && !datas[2] && !datas[3]) {
            retorno = true;
        }
        // 1 com 3
        else if (!datas[0] && !datas[1] && !datas[4] && !datas[5]) {
            retorno = true;
        }
        // 2 com 3
        else if (!datas[2] && !datas[3] && !datas[4] && !datas[5]) {
            retorno = true;
        }
        // 1 com 2 com 3
        else if (!datas[0] && !datas[1] && !datas[2] && !datas[3] && !datas[4] && !datas[5]) {
            retorno = true;
        }

        return retorno;
    }

    /// <summary>
    /// Seleciona o relatorio a ser executado
    /// </summary>
    /// <param name="visao">PDF, Excel ou Report</param>
    private void SelecionaRelatorio(string visao) {
        Response.BufferOutput = true;
        //
        // Salva os parametros na session
        Session["textDataInicioOperacao"] = this.textDataInicioOperacao.Text;
        Session["textDataFimOperacao"] = this.textDataFimOperacao.Text;
        Session["textDataInicioConversao"] = this.textDataInicioConversao.Text;
        Session["textDataFimConversao"] = this.textDataFimConversao.Text;
        Session["textDataInicioLiquidacao"] = this.textDataInicioLiquidacao.Text;
        Session["textDataFimLiquidacao"] = this.textDataFimLiquidacao.Text;
        Session["btnEditCodigoCotista"] = this.btnEditCodigoCotista.Text;
        Session["btnEditCodigoCarteira"] = this.btnEditCodigoCarteira.Text;
        Session["dropOpcoesCampos"] = this.dropOpcoesCampos.SelectedItem.Value;
        Session["chkIdOperacao"] = this.chkIdOperacao.Checked;

        if (this.dropTipoOperacao.SelectedIndex > 0)
        {
            Session["dropTipoOperacao"] = this.dropTipoOperacao.SelectedItem.Value;
        }
        else
        {
            Session["dropTipoOperacao"] = null;
        }

        //Response.Redirect("~/Relatorios/Cotista/ReportMovimentacaoCotista.aspx?Visao=" + visao);

        string url = "~/Relatorios/Cotista/ReportMovimentacaoCotista.aspx?Visao=" + visao;
        string redirectURL = Page.ResolveClientUrl(url);

        string script = "window.location = '" + redirectURL + "';";
        ScriptManager.RegisterStartupScript(this, typeof(Page), "RedirectTo", script, true);
    }
}