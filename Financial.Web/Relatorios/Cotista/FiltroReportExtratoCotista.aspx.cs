﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using Financial.Fundo;
using Financial.Investidor;
using Financial.InvestidorCotista;
using Financial.Security;
using System.Collections.Generic;
using EntitySpaces.Interfaces;
using Financial.Security.Enums;
using System.Text;
using Financial.Util;
using Financial.Common.Enums;
using Financial.Web.Common;

public partial class FiltroReportExtratoCotista : FiltroReportBasePage {
    // TODO: Traduzir
    const string MSG_ERRO = "Data Inicio deve ser Menor que a Data Fim";
    const string MSG_ERRO1 = "Dia Não Útil";
    const string MSG_ERRO2 = "Data Início Passada Anterior à Data de Implantação";
    const string MSG_ERRO3 = "Data Final Passada posterior à Data Atual da Carteira";

    new protected void Page_Load(object sender, EventArgs e) {
        textListaCarteiras.Focus();

        this.HasPopupCarteira = true;
        this.HasPopupCotista = true;
        base.Page_Load(sender, e);

        TrataTravamentoCampos();
    }

    private void TrataTravamentoCampos() {
        Usuario usuario = new Usuario();
        usuario.BuscaUsuario(HttpContext.Current.User.Identity.Name);
        int tipoTrava = usuario.TipoTrava.Value;

        if (tipoTrava == (int)TipoTravaUsuario.TravaCliente) {
            //Travamento do campo de carteira
            btnEditCodigoCarteira.Enabled = false;

            PermissaoCliente permissaoCliente = new PermissaoCliente();
            int? idCliente = permissaoCliente.RetornaClienteAssociado(HttpContext.Current.User.Identity.Name);

            if (idCliente.HasValue) {
                Cliente cliente = new Cliente();

                List<esQueryItem> campos = new List<esQueryItem>();
                campos.Add(cliente.Query.Apelido);
                cliente.LoadByPrimaryKey(campos, idCliente.Value);
                string apelido = cliente.Apelido;

                btnEditCodigoCarteira.Text = Convert.ToString(idCliente.Value);
                textNomeCarteira.Text = apelido;
            }
            labelListaCarteiras.Visible = false;
            textListaCarteiras.Visible = false;
            labelLista.Visible = false;
        }

        if (tipoTrava == (int)TipoTravaUsuario.TravaCotista) {
            //Travamento do campo de cotista
            btnEditCodigoCotista.Enabled = false;

            PermissaoCotista permissaoCotista = new PermissaoCotista();
            int? idCotista = permissaoCotista.RetornaCotistaAssociado(HttpContext.Current.User.Identity.Name);

            if (idCotista.HasValue) {
                Cotista cotista = new Cotista();

                List<esQueryItem> campos = new List<esQueryItem>();
                campos.Add(cotista.Query.Apelido);
                cotista.LoadByPrimaryKey(campos, idCotista.Value);
                string apelido = cotista.Apelido;

                btnEditCodigoCotista.Text = Convert.ToString(idCotista.Value);
                textNomeCotista.Text = apelido;
            }
            labelListaCarteiras.Visible = false;
            textListaCarteiras.Visible = false;
            labelLista.Visible = false;
        }
    }

    protected void ASPxCallback1_Callback(object source, DevExpress.Web.CallbackEventArgs e) {
        string nome = "";
        if (!String.IsNullOrEmpty(e.Parameter) && e.Parameter != "null") {
            int idCarteira = Convert.ToInt32(e.Parameter);
            Carteira carteira = new Carteira();
            carteira.LoadByPrimaryKey(idCarteira);

            if (carteira.str.Apelido != "") {
                if (carteira.IsAtivo) {
                    //O permissionamento da carteira é dado direto no cliente vinculado à carteira
                    PermissaoCliente permissaoCliente = new PermissaoCliente();
                    if (permissaoCliente.RetornaAcessoClienteComControle(idCarteira, HttpContext.Current.User.Identity.Name)) {
                        nome = carteira.str.Apelido;
                    }
                    else {
                        nome = "no_access";
                    }
                }
                else {
                    nome = "no_active";
                }
            }
        }
        e.Result = nome;
    }

    protected void ASPxCallback2_Callback(object source, DevExpress.Web.CallbackEventArgs e) {
        string nome = "";
        if (!String.IsNullOrEmpty(e.Parameter) && e.Parameter != "null") {
            int idCotista = Convert.ToInt32(e.Parameter);
            Cotista cotista = new Cotista();
            cotista.LoadByPrimaryKey(idCotista);

            if (cotista.str.Apelido != "") {
                if (cotista.IsAtivo) {
                    PermissaoCotista permissaoCotista = new PermissaoCotista();
                    if (permissaoCotista.RetornaAcessoCotista(idCotista, HttpContext.Current.User.Identity.Name)) {
                        nome = cotista.str.Apelido;
                    }
                    else {
                        nome = "no_access";
                    }
                }
                else {
                    nome = "no_active";
                }
            }
        }
        e.Result = nome;
    }

    protected void EsDSCarteira_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e) {
        CarteiraCollection carteiraCollection = new CarteiraCollection();
        carteiraCollection.BuscaCarteirasComAcesso(HttpContext.Current.User.Identity.Name);

        e.Collection = carteiraCollection;
    }

    protected void EsDSCotista_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e) {
        CotistaCollection cotistaCollection = new CotistaCollection();
        cotistaCollection.BuscaCotistasComAcesso(HttpContext.Current.User.Identity.Name);

        e.Collection = cotistaCollection;
    }

    protected void btnVisualiza_Click(object sender, EventArgs e) {
        this.TrataErros();
        this.SelecionaRelatorio(VisaoRelatorio.visaoHTML);
    }

    protected void btnPDF_Click(object sender, EventArgs e) {
        this.TrataErros();
        this.SelecionaRelatorio(VisaoRelatorio.visaoPDF);
    }

    protected void btnTxt_Click(object sender, EventArgs e) {

        //if (btnEditCodigoCotista.Text.Trim() == "") {
        //    throw new Exception("Cotista deve estar preenchido para exportação .Txt");
        //}

        this.TrataErros();
        this.SelecionaRelatorio("TXT");
    }
    
    /// <summary>
    /// Trata os Erros de Relatorio
    /// </summary>
    /// <exception cref="Exception"> - Lança Exceção se Data Inicio > DataFim
    ///                              - Lança Exceção se Data Inicio For um Dia Não Útil
    ///                              - Lança Exceção se Data Final For um Dia Não Útil
    ///                              - Data Inicial passada Anterior à data de implantação da Carteira
    ///                              - Data Final passada posterior à data atual da carteira
    /// </exception>
    private void TrataErros() {

        #region Campos obrigatórios
        List<Control> controles = new List<Control>(new Control[] { 
    	    this.textDataInicio, this.textDataFim });

        if (base.TestaObrigatorio(controles) != "") {
            throw new Exception("Campos com * são obrigatórios!");
        }
        #endregion

        /*  Se datas estao Preenchidas pelo menos um campo 
        (Carteira, Cotista ou Lista de carteiras deve estar preenchido
        */
        if (btnEditCodigoCarteira.Text.Trim() == "" &&
           btnEditCodigoCotista.Text.Trim() == "" &&
           textListaCarteiras.Text.Trim() == "") {
            throw new Exception("Carteira, Cotista ou Lista de Carteiras deve estar preenchido.");
        }
        
        DateTime dataInicio = Convert.ToDateTime(this.textDataInicio.Text);
        DateTime dataFim = Convert.ToDateTime(this.textDataFim.Text);

        if (dataInicio > dataFim) {
            throw new Exception(MSG_ERRO);
        }
        else if (!Calendario.IsDiaUtil(dataInicio, LocalFeriadoFixo.Brasil, TipoFeriado.Brasil)) {
            throw new Exception(MSG_ERRO1 + ": " + dataInicio.ToString("d"));
        }
        else if (!Calendario.IsDiaUtil(dataFim, LocalFeriadoFixo.Brasil, TipoFeriado.Brasil)) {
            throw new Exception(MSG_ERRO1 + ": " + dataFim.ToString("d"));
        }


        if (btnEditCodigoCarteira.Text != "") {
            Cliente cliente = new Cliente();
            cliente.LoadByPrimaryKey(Convert.ToInt32(btnEditCodigoCarteira.Text));
            DateTime dataImplantacao = cliente.DataImplantacao.Value;
            DateTime dataDia = cliente.DataDia.Value;

            if (DateTime.Compare(dataImplantacao, dataInicio) > 0) {
                throw new Exception(MSG_ERRO2 + ": " + dataImplantacao.ToString("d"));
            }
            else if (DateTime.Compare(dataFim, dataDia) > 0) {
                throw new Exception(MSG_ERRO3 + ": " + dataDia.ToString("d"));
            }
        }
    }

    /// <summary>
    /// DataBinding Inicial da Observação do Extrato Cotista
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void HtmlEditorObservacao_OnPreRender(object sender, EventArgs e) {
        this.htmlEditorObservacao.Height = Unit.Point(190);
        this.htmlEditorObservacao.Width = Unit.Point(380);
        this.htmlEditorObservacao.Html = ParametrosConfiguracaoSistema.ConfiguracaoRelatorios.RelatorioExtratoCotista.ObservacaoCotista;
    }

    /// <summary>
    /// Salvar Observacão
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnSalvarObservacao_Click(object sender, EventArgs e) {
        try {
            ParametrosConfiguracaoSistema.ConfiguracaoRelatorios.RelatorioExtratoCotista.ObservacaoCotista = this.htmlEditorObservacao.Html.Trim();
        }
        catch (Exception ex) {
            throw new Exception(ex.Message);
        }
        throw new Exception("Observação Salva com Sucesso.");
    }

    /// <summary>
    /// Seleciona o relatorio a ser executado
    /// </summary>
    /// <param name="visao">PDF, Excel ou Report</param>
    private void SelecionaRelatorio(string visao) {
        Response.BufferOutput = true;
        //
        // Salva os parametros na session
        Session["textDataInicio"] = this.textDataInicio.Text;
        Session["textDataFim"] = this.textDataFim.Text;
        Session["btnEditCodigoCotista"] = this.btnEditCodigoCotista.Text;
        Session["btnEditCodigoCarteira"] = this.btnEditCodigoCarteira.Text;
        Session["textListaCarteiras"] = this.textListaCarteiras.Text;
        Session["chkIncluiComposicaoCarteira"] = this.chkIncluiComposicaoCarteira.Checked;

        //Response.Redirect("~/Relatorios/Cotista/ReportExtratoCotista.aspx?Visao=" + visao);

        string url = "~/Relatorios/Cotista/ReportExtratoCotista.aspx?Visao=" + visao;
        string redirectURL = Page.ResolveClientUrl(url);

        string script = "window.location = '" + redirectURL + "';";
        ScriptManager.RegisterStartupScript(this, typeof(Page), "RedirectTo", script, true);
    }
}