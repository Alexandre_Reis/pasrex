using System;
using Financial.Relatorio;
using System.IO;
using System.Web;
using System.Globalization;
using Financial.Web.Common;

public partial class _ReportMovimentacaoCotista : BasePage {
   
    new protected void Page_Load(object sender, EventArgs e) {
        string visao = Request.QueryString["Visao"].ToString();
        //
        
        DateTime? dataInicioOperacao = null;
        DateTime? dataFimOperacao = null;
        DateTime? dataInicioConversao = null;
        DateTime? dataFimConversao = null;
        DateTime? dataInicioLiquidacao = null;
        DateTime? dataFimLiquidacao = null;
        byte? tipoOperacao = null;
        int? idCarteira = null;
        int? idCotista = null;
        bool chkIdOperacao = false;

        if (Convert.ToString(Session["textDataInicioOperacao"]) != "")
        {
            dataInicioOperacao = Convert.ToDateTime(Session["textDataInicioOperacao"]);
        }
        if (Convert.ToString(Session["textDataFimOperacao"]) != "")
        {
            dataFimOperacao = Convert.ToDateTime(Session["textDataFimOperacao"]);
        }

        if (Convert.ToString(Session["textDataInicioConversao"]) != "")
        {
            dataInicioConversao = Convert.ToDateTime(Session["textDataInicioConversao"]);
        }
        if (Convert.ToString(Session["textDataFimConversao"]) != "")
        {
            dataFimConversao = Convert.ToDateTime(Session["textDataFimConversao"]);
        }

        if (Convert.ToString(Session["textDataInicioLiquidacao"]) != "")
        {
            dataInicioLiquidacao = Convert.ToDateTime(Session["textDataInicioLiquidacao"]);
        }
        if (Convert.ToString(Session["textDataFimLiquidacao"]) != "")
        {
            dataFimLiquidacao = Convert.ToDateTime(Session["textDataFimLiquidacao"]);
        }

        if (Session["dropTipoOperacao"] != null)
        {
            tipoOperacao = Convert.ToByte(Session["dropTipoOperacao"]);
        }

        if (Session["chkIdOperacao"] != null)
        {
            chkIdOperacao = Convert.ToBoolean(Session["chkIdOperacao"]);
        }

        ReportMovimentacaoCotista.TipoOpcoesCampo tipoOpcoesCampo = (ReportMovimentacaoCotista.TipoOpcoesCampo)Convert.ToInt32(Session["dropOpcoesCampos"]);

        if (!String.IsNullOrEmpty((string)Session["btnEditCodigoCarteira"])) {
            idCarteira = Convert.ToInt32(Session["btnEditCodigoCarteira"]);
        }
        if (!String.IsNullOrEmpty((string)Session["btnEditCodigoCotista"])) {
            idCotista = Convert.ToInt32(Session["btnEditCodigoCotista"]);
        }
        
        if (visao == "Report") {
            if (!chkIdOperacao)
            {
                ReportViewer1.Report = new ReportMovimentacaoCotista(idCotista, idCarteira, dataInicioOperacao, dataFimOperacao, dataInicioConversao,
                                                                 dataFimConversao, dataInicioLiquidacao, dataFimLiquidacao,
                                                                 tipoOperacao, tipoOpcoesCampo);
            }
            else
            {
                ReportViewer1.Report = new ReportMovimentacaoCotistaView2(idCotista, idCarteira, dataInicioOperacao, dataFimOperacao, dataInicioConversao,
                                                                 dataFimConversao, dataInicioLiquidacao, dataFimLiquidacao,
                                                                 tipoOperacao, tipoOpcoesCampo);
            }
        }
        else if (visao == "PDF") {
            #region PDF
            if (!chkIdOperacao)
            {
                ReportMovimentacaoCotista report = new ReportMovimentacaoCotista(idCotista, idCarteira, dataInicioOperacao, dataFimOperacao, dataInicioConversao,
                                                                             dataFimConversao, dataInicioLiquidacao, dataFimLiquidacao,
                                                                             tipoOperacao, tipoOpcoesCampo);
                MemoryStream ms = new MemoryStream();
                report.ExportToPdf(ms);
                ms.Seek(0, SeekOrigin.Begin);
                Response.ClearContent();
                Response.ClearHeaders();
                Response.Buffer = true;
                Response.Cache.SetCacheability(HttpCacheability.Private);
                Response.ContentType = "application/pdf";
                Response.AddHeader("Content-Length", ms.Length.ToString(CultureInfo.CurrentCulture));
                Response.AddHeader("Content-Disposition", String.Format("attachment;filename=ReportMovimentacaoCotista.pdf"));

                Response.BinaryWrite(ms.ToArray());
                ms.Close();
                HttpContext.Current.ApplicationInstance.CompleteRequest();
                Response.End();
            }
            else
            {
                ReportMovimentacaoCotistaView2 report = new ReportMovimentacaoCotistaView2(idCotista, idCarteira, dataInicioOperacao, dataFimOperacao, dataInicioConversao,
                                                                             dataFimConversao, dataInicioLiquidacao, dataFimLiquidacao,
                                                                             tipoOperacao, tipoOpcoesCampo);
                MemoryStream ms = new MemoryStream();
                report.ExportToPdf(ms);
                ms.Seek(0, SeekOrigin.Begin);
                Response.ClearContent();
                Response.ClearHeaders();
                Response.Buffer = true;
                Response.Cache.SetCacheability(HttpCacheability.Private);
                Response.ContentType = "application/pdf";
                Response.AddHeader("Content-Length", ms.Length.ToString(CultureInfo.CurrentCulture));
                Response.AddHeader("Content-Disposition", String.Format("attachment;filename=ReportMovimentacaoCotista.pdf"));

                Response.BinaryWrite(ms.ToArray());
                ms.Close();
                HttpContext.Current.ApplicationInstance.CompleteRequest();
                Response.End();
            }

            #endregion
        }
        else if (visao == "Excel") {
            #region Excel
            if (!chkIdOperacao)
            {
                ReportMovimentacaoCotista report = new ReportMovimentacaoCotista(idCotista, idCarteira, dataInicioOperacao, dataFimOperacao, dataInicioConversao,
                                                                             dataFimConversao, dataInicioLiquidacao, dataFimLiquidacao,
                                                                             tipoOperacao, tipoOpcoesCampo);
                MemoryStream ms = new MemoryStream();
                report.ExportToXls(ms);
                ms.Seek(0, SeekOrigin.Begin);
                Response.ClearContent();
                Response.ClearHeaders();
                Response.Buffer = true;
                Response.Cache.SetCacheability(HttpCacheability.Private);
                Response.ContentType = "application/vnd.ms-excel";
                Response.AddHeader("Content-Length", ms.Length.ToString(CultureInfo.CurrentCulture));
                Response.AddHeader("Content-Disposition", String.Format("attachment;filename=ReportMovimentacaoCotista.xls"));

                Response.BinaryWrite(ms.ToArray());
                ms.Close();
                HttpContext.Current.ApplicationInstance.CompleteRequest();
                Response.End();

            }
            else
            {
                ReportMovimentacaoCotistaView2 report = new ReportMovimentacaoCotistaView2(idCotista, idCarteira, dataInicioOperacao, dataFimOperacao, dataInicioConversao,
                                                                             dataFimConversao, dataInicioLiquidacao, dataFimLiquidacao,
                                                                             tipoOperacao, tipoOpcoesCampo);
                MemoryStream ms = new MemoryStream();
                report.ExportToXls(ms);
                ms.Seek(0, SeekOrigin.Begin);
                Response.ClearContent();
                Response.ClearHeaders();
                Response.Buffer = true;
                Response.Cache.SetCacheability(HttpCacheability.Private);
                Response.ContentType = "application/vnd.ms-excel";
                Response.AddHeader("Content-Length", ms.Length.ToString(CultureInfo.CurrentCulture));
                Response.AddHeader("Content-Disposition", String.Format("attachment;filename=ReportMovimentacaoCotista.xls"));

                Response.BinaryWrite(ms.ToArray());
                ms.Close();
                HttpContext.Current.ApplicationInstance.CompleteRequest();
                Response.End();

            }

            #endregion
        }
    }
}
