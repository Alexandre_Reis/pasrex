﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using Financial.Fundo;
using Financial.Investidor;
using Financial.InvestidorCotista;
using Financial.Security;
using System.Collections.Generic;
using EntitySpaces.Interfaces;
using Financial.Security.Enums;
using System.Text;
using Financial.Util;
using Financial.Common.Enums;
using Financial.Web.Common;

public partial class FiltroReportFinanceiroCotista : FiltroReportBasePage {
    // TODO: Traduzir    
    const string MSG_ERRO = "Dia Não Útil";
    const string MSG_ERRO1 = "Data Passada Anterior à Data de Implantação da Carteira";
    const string MSG_ERRO2 = "Data Passada posterior à Data Atual da Carteira";

    new protected void Page_Load(object sender, EventArgs e) {
        btnEditCodigoCarteira.Focus();

        this.HasPopupCarteira = true;        
        base.Page_Load(sender, e);

        TrataTravamentoCampos();
    }

    private void TrataTravamentoCampos() {
        Usuario usuario = new Usuario();
        usuario.BuscaUsuario(HttpContext.Current.User.Identity.Name);
        int tipoTrava = usuario.TipoTrava.Value;

        if (tipoTrava == (int)TipoTravaUsuario.TravaCliente) {
            //Travamento do campo de carteira
            btnEditCodigoCarteira.Enabled = false;

            PermissaoCliente permissaoCliente = new PermissaoCliente();
            int? idCliente = permissaoCliente.RetornaClienteAssociado(HttpContext.Current.User.Identity.Name);

            if (idCliente.HasValue) {
                Cliente cliente = new Cliente();

                List<esQueryItem> campos = new List<esQueryItem>();
                campos.Add(cliente.Query.Apelido);
                cliente.LoadByPrimaryKey(campos, idCliente.Value);
                string apelido = cliente.Apelido;

                btnEditCodigoCarteira.Text = Convert.ToString(idCliente.Value);
                textNomeCarteira.Text = apelido;
            }
        }
    }

    protected void ASPxCallback1_Callback(object source, DevExpress.Web.CallbackEventArgs e) {
        string nome = "";
        if (!String.IsNullOrEmpty(e.Parameter) && e.Parameter != "null") {
            int idCarteira = Convert.ToInt32(e.Parameter);
            Carteira carteira = new Carteira();
            carteira.LoadByPrimaryKey(idCarteira);

            if (carteira.str.Apelido != "") {
                if (carteira.IsAtivo) {
                    //O permissionamento da carteira é dado direto no cliente vinculado à carteira
                    PermissaoCliente permissaoCliente = new PermissaoCliente();
                    if (permissaoCliente.RetornaAcessoClienteComControle(idCarteira, HttpContext.Current.User.Identity.Name)) {
                        nome = carteira.str.Apelido;
                    }
                    else {
                        nome = "no_access";
                    }
                }
                else {
                    nome = "no_active";
                }
            }
        }
        e.Result = nome;
    }

    protected void EsDSCarteira_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e) {
        CarteiraCollection carteiraCollection = new CarteiraCollection();
        carteiraCollection.BuscaCarteirasComAcesso(HttpContext.Current.User.Identity.Name);

        e.Collection = carteiraCollection;
    }

    protected void btnVisualiza_Click(object sender, EventArgs e) {
        this.TrataErros();
        this.SelecionaRelatorio(VisaoRelatorio.visaoHTML);
    }

    protected void btnPDF_Click(object sender, EventArgs e) {
        this.TrataErros();
        this.SelecionaRelatorio(VisaoRelatorio.visaoPDF);
    }

    protected void btnExcel_Click(object sender, EventArgs e) {
        this.TrataErros();
        this.SelecionaRelatorio(VisaoRelatorio.visaoExcel);
    }

    /// <summary>
    /// Trata os Erros de Relatorio
    /// </summary>
    /// <exception cref="Exception"> - Lança Exceção se Data For um Dia Não Útil   
    ///                              - Data passada Anterior à data de implantação da Carteira
    ///                              - Data passada posterior à data atual da carteira
    /// </exception>
    private void TrataErros() {
        #region Campos obrigatórios
        List<Control> controles = new List<Control>(new Control[] { 
    	    this.btnEditCodigoCarteira, this.textData, this.textValor });

        if (base.TestaObrigatorio(controles) != "") {
            throw new Exception("Campos com * são obrigatórios!");
        }
        #endregion
                
        DateTime data = Convert.ToDateTime(this.textData.Text);

        if (!Calendario.IsDiaUtil(data, LocalFeriadoFixo.Brasil, TipoFeriado.Brasil)) {
            throw new Exception(MSG_ERRO + ": " + data.ToString("d"));
        }

        Cliente cliente = new Cliente();
        cliente.LoadByPrimaryKey(Convert.ToInt32(btnEditCodigoCarteira.Text));
        DateTime dataImplantacao = cliente.DataImplantacao.Value;
        DateTime dataDia = cliente.DataDia.Value;

        if (data < dataImplantacao) {
            throw new Exception(MSG_ERRO1 + ": " + dataImplantacao.ToString("d"));
        }
        else if (data > dataDia) {
            throw new Exception(MSG_ERRO2 + ": " + dataDia.ToString("d"));
        }
    }

    /// <summary>
    /// Seleciona o relatorio a ser executado
    /// </summary>
    /// <param name="visao">PDF, Excel ou Report</param>
    private void SelecionaRelatorio(string visao) {
        Response.BufferOutput = true;
        //
        // Salva os parametros na session
        Session["btnEditCodigoCarteira"] = this.btnEditCodigoCarteira.Text;
        Session["textData"] = this.textData.Text;
        Session["textValor"] = Convert.ToDecimal(this.textValor.Text);        
        Session["dropTipoOrdenacao"] = this.dropTipoOrdenacao.SelectedIndex;

        //Response.Redirect("~/Relatorios/Cotista/ReportFinanceiroCotista.aspx?Visao=" + visao);

        string url = "~/Relatorios/Cotista/ReportFinanceiroCotista.aspx?Visao=" + visao;
        string redirectURL = Page.ResolveClientUrl(url);

        string script = "window.location = '" + redirectURL + "';";
        ScriptManager.RegisterStartupScript(this, typeof(Page), "RedirectTo", script, true);
    }
}
