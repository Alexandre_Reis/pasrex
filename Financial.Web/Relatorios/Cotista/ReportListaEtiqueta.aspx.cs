using Financial.Web.Common;
using System;
using Financial.Relatorio;
using System.IO;
using System.Web;
using System.Globalization;

public partial class _ReportListaEtiqueta : BasePage {
    /* Como Usar:
        int idCarteira = 1000;

        ReportViewer1.Report = new ReportListaEtiqueta();
        //ReportViewer1.Report = new ReportListaEtiqueta(idCarteira);
     */
    new protected void Page_Load(object sender, EventArgs e) {
        
        string visao = Request.QueryString["Visao"].ToString();
        //
        //Response.Write("<br>" + Request.Form["btnEditCodigo"].ToString());

        int? idCarteira = null;

        // Se valores estiverem na Session pega os valores da Session
        if (Session["btnEditCodigo"] == null) {
            Session["btnEditCodigo"] = Request.Form["btnEditCodigo"].ToString();
        }
        else {
            //Opcionais
            if (!String.IsNullOrEmpty((string)Session["btnEditCodigo"])) {
                idCarteira = Convert.ToInt32(Session["btnEditCodigo"]);
            }
        }

        if (visao == "Report") {
            ReportViewer1.Report = new ReportListaEtiqueta(idCarteira.Value);
        }
        else if (visao == "PDF") {
            #region PDF
            ReportListaEtiqueta report = new ReportListaEtiqueta(idCarteira.Value);

            MemoryStream ms = new MemoryStream();
            report.ExportToPdf(ms);
            ms.Seek(0, SeekOrigin.Begin);
            Response.ClearContent();
            Response.ClearHeaders();
            Response.Buffer = true;
            Response.Cache.SetCacheability(HttpCacheability.Private);
            Response.ContentType = "application/pdf";
            Response.AddHeader("Content-Length", ms.Length.ToString(CultureInfo.CurrentCulture));            
            Response.AddHeader("Content-Disposition", String.Format("attachment;filename=ReportListaEtiqueta.pdf"));

            Response.BinaryWrite(ms.ToArray());
            ms.Close();
            HttpContext.Current.ApplicationInstance.CompleteRequest();
            Response.End();
            #endregion
        }
        else if (visao == "Excel") {
            #region Excel
            ReportListaEtiqueta report = new ReportListaEtiqueta(idCarteira.Value);

            MemoryStream ms = new MemoryStream();
            report.ExportToXls(ms);
            ms.Seek(0, SeekOrigin.Begin);
            Response.ClearContent();
            Response.ClearHeaders();
            Response.Buffer = true;
            Response.Cache.SetCacheability(HttpCacheability.Private);
            Response.ContentType = "application/vnd.ms-excel";
            Response.AddHeader("Content-Length", ms.Length.ToString(CultureInfo.CurrentCulture));
            Response.AddHeader("Content-Disposition", String.Format("attachment;filename=ReportListaEtiqueta.xls"));

            Response.BinaryWrite(ms.ToArray());
            ms.Close();
            HttpContext.Current.ApplicationInstance.CompleteRequest();
            Response.End();

            #endregion
        }            
    }
    
    
}
