﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="FiltroReportNotasAplicacaoResgate.aspx.cs" Inherits="FiltroReportNotasAplicacaoResgate" %>

<%@ Register Assembly="System.Web, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"
    Namespace="System.Web.UI.HtmlControls" TagPrefix="cc2" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxnb" %>    
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxcb" %>
<%@ Register Assembly="System.Web, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"
    Namespace="System.Web.UI" TagPrefix="cc1" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxe" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxwgv" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxpc" %>

<%@ Register Assembly="EntitySpaces.Web" Namespace="EntitySpaces.Web" TagPrefix="cc1" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >

<head id="Head1" runat="server">
<link href="~/css/forms.css" rel="stylesheet" type="text/css" />

<script language="JavaScript">
function OnGetDataCarteira(values) {
    btnEditCodigoCarteira.SetValue(values);                    
    popupCarteira.HideWindow();
    ASPxCallback1.SendCallback(btnEditCodigoCarteira.GetValue());        
    btnEditCodigoCarteira.Focus();    
}

function OnGetDataCotista(values) {
    btnEditCodigoCotista.SetValue(values);                    
    popupCotista.HideWindow();
    ASPxCallback2.SendCallback(btnEditCodigoCotista.GetValue());        
    btnEditCodigoCotista.Focus();    
}
</script>
<script type="text/javascript" language="Javascript" src="../../js/global.js"></script>
    
</head>

<body >
    <form id="form1" runat="server"> 
    
    
    <dxcb:ASPxCallback ID="ASPxCallback1" runat="server" OnCallback="ASPxCallback1_Callback">
        <ClientSideEvents CallbackComplete="function(s, e) {
            OnCallBackCompleteCliente(s, e, popupMensagemCarteira, 
                                      btnEditCodigoCarteira, document.getElementById('textNomeCarteira'));
        }        
        "/>
    </dxcb:ASPxCallback>
    
    <dxcb:ASPxCallback ID="ASPxCallback2" runat="server" OnCallback="ASPxCallback2_Callback">
        <ClientSideEvents CallbackComplete="function(s, e) {             
            OnCallBackCompleteCliente(s, e, popupMensagemCotista, 
                                      btnEditCodigoCotista, document.getElementById('textNomeCotista'));
        }        
        "/>
    </dxcb:ASPxCallback>
    
    <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="true" EnableScriptLocalization="true" />
        
    <div class="divPanel">
    <table width="100%"><tr><td>
    <div id="container_small">
    
    <div id="header"><asp:Label ID="label1" runat="server" Text="Notas de Aplicação/Resgate de Cotistas" /></div>
        
    <div id="mainContentSpace">
    
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
    <ContentTemplate>
        
        <div class="reportFilter">
        
        <div style="height:20px"></div>                          
                                        
        <table cellpadding="2" cellspacing="2">
            
            <tr>
            <td class="td_Label">                
                <asp:Label ID="labelCarteira" runat="server" CssClass="labelNormal" Text="Carteira:"></asp:Label>
            </td>
            
            <td>
                <dxe:ASPxSpinEdit ID="btnEditCodigoCarteira" runat="server" CssClass="textButtonEdit" 
                                    EnableClientSideAPI="True" ClientInstanceName="btnEditCodigoCarteira">  
                <Buttons>
                    <dxe:EditButton>
                    </dxe:EditButton>                                
                </Buttons>        
                <ClientSideEvents
                         KeyPress="function(s, e) {document.getElementById('textNomeCarteira').value = '';}" 
                         ButtonClick="function(s, e) {popupCarteira.ShowAtElementByID(s.name);}" 
                         LostFocus="function(s, e) {OnLostFocus(popupMensagemCarteira, ASPxCallback1, btnEditCodigoCarteira);}"
                />                            
                </dxe:ASPxSpinEdit>                
            </td>                          
            
            <td colspan="2">
                <asp:TextBox ID="textNomeCarteira" runat="server" CssClass="textNome" Enabled="false"></asp:TextBox>                
            </td>
            </tr>
            
            <tr>
            <td class="td_Label">
                <asp:Label ID="labelCotista" runat="server" CssClass="labelNormal" Text="Cotista:"></asp:Label>
            </td>
            
            <td>
                <dxe:ASPxSpinEdit ID="btnEditCodigoCotista" runat="server" CssClass="textButtonEdit" 
                                    EnableClientSideAPI="True" ClientInstanceName="btnEditCodigoCotista">  
                <Buttons>
                    <dxe:EditButton>
                    </dxe:EditButton>                                
                </Buttons>        
                <ClientSideEvents
                         KeyPress="function(s, e) {document.getElementById('textNomeCotista').value = '';}" 
                         ButtonClick="function(s, e) {popupCotista.ShowAtElementByID(s.name);}" 
                         LostFocus="function(s, e) {OnLostFocus(popupMensagemCotista, ASPxCallback2, btnEditCodigoCotista);}"
                />                            
                </dxe:ASPxSpinEdit>                
            </td>                          
            
            <td colspan="2">
                <asp:TextBox ID="textNomeCotista" runat="server" CssClass="textNome" Enabled="false" />
            </td>
            </tr>
            
            <tr>
            <td class="td_Label">                
                <asp:Label ID="labelDataInicio" runat="server" CssClass="labelNormal" Text="Início:" />
            </td>                        
            <td>
                <dxe:ASPxDateEdit ID="textDataInicio" runat="server" ClientInstanceName="textDataInicio" />            
            </td>  
            
            <td colspan="2">
                <table>
                    <tr><td><asp:Label ID="labelDataFim" runat="server" CssClass="labelNormal" Text="Fim:"/></td>
                    <td><dxe:ASPxDateEdit ID="textDataFim" runat="server" ClientInstanceName="textDataFim"/></td></tr>
                </table>
            </td>                                                                  
            </tr>         
            
            <tr>
                <td class="td_Label">
                    <asp:Label ID="labelTipoOperacao" runat="server" CssClass="labelNormal" Text="Tipo:"></asp:Label>
                </td>
                
                <td colspan="3">
                    <asp:DropDownList ID="dropTipoOperacao" runat="server" CssClass="dropDownListCurto">
                        <asp:ListItem Value="1" Text="Aplicação"></asp:ListItem>
                        <asp:ListItem Value="2" Text="Resgate Bruto"></asp:ListItem>
                        <asp:ListItem Value="3" Text="Resgate Líquido"></asp:ListItem>
                        <asp:ListItem Value="4" Text="Resgate Cotas"></asp:ListItem>
                        <asp:ListItem Value="5" Text="Resgate Total"></asp:ListItem>
                        <asp:ListItem Value="11" Text="Aplic. Ações"></asp:ListItem>                        
                    </asp:DropDownList>
                </td>
            </tr>   
            
            <tr>
                <td class="td_Label">
                    <asp:Label ID="labelNota" runat="server" CssClass="labelNormal" Text="Nr Nota:"></asp:Label>
                </td>
                
                <td colspan="3">
                    <dxe:ASPxSpinEdit ID="textNota" runat="server" ClientInstanceName="textNota" CssClass="textValor_5" NumberType="Integer" MaxLength="10"/>
                </td>
            </tr>
            
            <tr>                                   
                <td class="td_Label">
                    <asp:Label ID="labelLingua" runat="server" CssClass="labelNormal" Text="Lingua: " />
                </td>
                    
                <td colspan="3">
                    <dxe:ASPxComboBox ID="dropLingua" runat="server" CssClass="dropDownListCurto" 
                            ClientInstanceName="dropLingua" SelectedIndex="0">
                    <Items>
                        <dxe:ListEditItem Value="1" Text="Português" />
                        <dxe:ListEditItem Value="2" Text="Inglês" />
                    </Items>
                    </dxe:ASPxComboBox>
                </td>            
            </tr>

        </table>                                                                                 
        
        </div>
    
        <div id="reportLinkButton" class="linkButton linkButtonNoBorder">
           <asp:LinkButton ID="btnVisualiza" runat="server" Font-Overline="false" CssClass="btnReport" OnClick="btnVisualiza_Click"><asp:Literal ID="Literal1" runat="server" Text="Visualizar"/><div></div></asp:LinkButton>
           <asp:LinkButton ID="btnPDF" runat="server" Font-Overline="false"  CssClass="btnPdf" OnClick="btnPDF_Click"><asp:Literal ID="Literal3" runat="server" Text="Gerar PDF"/><div></div></asp:LinkButton>
        </div>
        
    </ContentTemplate>
    </asp:UpdatePanel>
    
    </div>
    
    </div>
    </td></tr></table>
    </div>
    
    <cc1:esDataSource ID="EsDSCotista" runat="server" OnesSelect="EsDSCotista_esSelect" />
    <cc1:esDataSource ID="EsDSCarteira" runat="server" OnesSelect="EsDSCarteira_esSelect" />
    
   </form>
</body>
</html>