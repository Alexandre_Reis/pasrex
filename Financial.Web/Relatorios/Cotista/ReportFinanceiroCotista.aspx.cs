using Financial.Web.Common;
using System;
using Financial.Relatorio;
using System.Globalization;
using System.Web;
using System.IO;

public partial class _ReportFinanceiroCotista : BasePage {
    private static class VisaoRelatorio {
        public static string visaoHTML = "Report";
        public static string visaoPDF = "PDF";
        public static string visaoExcel = "Excel";
    }

    new protected void Page_Load(object sender, EventArgs e) {
        string visao = Request.QueryString["Visao"].ToString();

        ReportFinanceiroCotista.TipoOrdenacao tipoOrdenacao = ReportFinanceiroCotista.TipoOrdenacao.PorNome;

        // Obrigatorio
        DateTime data = Convert.ToDateTime(Session["textData"]);
        int idCarteira = Convert.ToInt32(Session["btnEditCodigoCarteira"]);
        //        
        tipoOrdenacao = (ReportFinanceiroCotista.TipoOrdenacao)Convert.ToInt32(Session["dropTipoOrdenacao"]);
        decimal valor = Convert.ToDecimal(Session["textValor"]);

        if (visao == VisaoRelatorio.visaoHTML) {
            ReportViewer1.Report = new ReportFinanceiroCotista(idCarteira, data, valor, tipoOrdenacao);
        }
        else if (visao == VisaoRelatorio.visaoPDF) {
            #region PDF
            ReportFinanceiroCotista report = new ReportFinanceiroCotista(idCarteira, data, valor, tipoOrdenacao);

            MemoryStream ms = new MemoryStream();
            report.ExportToPdf(ms);
            ms.Seek(0, SeekOrigin.Begin);
            Response.ClearContent();
            Response.ClearHeaders();
            Response.Buffer = true;
            Response.Cache.SetCacheability(HttpCacheability.Private);
            Response.ContentType = "application/pdf";
            Response.AddHeader("Content-Length", ms.Length.ToString(CultureInfo.CurrentCulture));
            Response.AddHeader("Content-Disposition", String.Format("attachment;filename=ReportFinanceiroCotista.pdf"));

            Response.BinaryWrite(ms.ToArray());
            ms.Close();
            HttpContext.Current.ApplicationInstance.CompleteRequest();
            Response.End();
            #endregion
        }
        else if (visao == VisaoRelatorio.visaoExcel) {
            #region Excel
            ReportFinanceiroCotista report = new ReportFinanceiroCotista(idCarteira, data, valor, tipoOrdenacao);

            MemoryStream ms = new MemoryStream();
            report.ExportToXls(ms);
            ms.Seek(0, SeekOrigin.Begin);
            Response.ClearContent();
            Response.ClearHeaders();
            Response.Buffer = true;
            Response.Cache.SetCacheability(HttpCacheability.Private);
            Response.ContentType = "application/vnd.ms-excel";
            Response.AddHeader("Content-Length", ms.Length.ToString(CultureInfo.CurrentCulture));
            Response.AddHeader("Content-Disposition", String.Format("attachment;filename=ReportFinanceiroCotista.xls"));

            Response.BinaryWrite(ms.ToArray());
            ms.Close();
            HttpContext.Current.ApplicationInstance.CompleteRequest();
            Response.End();

            #endregion
        }
    }
}
