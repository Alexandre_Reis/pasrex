﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using Financial.Fundo;
using Financial.Investidor;
using System.Collections.Generic;
using Financial.Web.Util;
using Financial.Investidor.Enums;
using Financial.Security;
using log4net;
using Financial.Security.Enums;
using EntitySpaces.Interfaces;
using DevExpress.Web;
using System.Text;
using Financial.Util;
using Financial.Common.Enums;
using Financial.InvestidorCotista;
using Financial.Web.Common;
using System.Threading;

public partial class FiltroReportSaldoAplicacaoCotista : FiltroReportBasePage {
    private static readonly ILog log = LogManager.GetLogger(typeof(FiltroReportSaldoAplicacaoCotista));

    new protected void Page_Load(object sender, EventArgs e) {
        btnEditCodigo.Focus();

        this.HasPopupCarteira = true;
        this.HasPopupCotista = true;
        base.Page_Load(sender, e);

        TrataTravamentoCampos();
    }

    private void TrataTravamentoCampos() {
        Usuario usuario = new Usuario();
        usuario.BuscaUsuario(HttpContext.Current.User.Identity.Name);
        int tipoTrava = usuario.TipoTrava.Value;

        if (tipoTrava == (int)TipoTravaUsuario.TravaCliente) {
            btnEditCodigo.Enabled = false;

            PermissaoCliente permissaoCliente = new PermissaoCliente();
            int? IdCarteira = permissaoCliente.RetornaClienteAssociado(HttpContext.Current.User.Identity.Name);

            if (IdCarteira.HasValue) {
                Cliente cliente = new Cliente();

                List<esQueryItem> campos = new List<esQueryItem>();
                campos.Add(cliente.Query.Apelido);
                cliente.LoadByPrimaryKey(campos, IdCarteira.Value);
                string apelido = cliente.Apelido;

                btnEditCodigo.Text = Convert.ToString(IdCarteira.Value);
                textNome.Text = apelido;
            }
        }

        if (tipoTrava == (int)TipoTravaUsuario.TravaCotista) {
            //Travamento do campo de cotista
            btnEditCodigoCotista.Enabled = false;

            PermissaoCotista permissaoCotista = new PermissaoCotista();
            int? idCotista = permissaoCotista.RetornaCotistaAssociado(HttpContext.Current.User.Identity.Name);

            if (idCotista.HasValue) {
                Cotista cotista = new Cotista();

                List<esQueryItem> campos = new List<esQueryItem>();
                campos.Add(cotista.Query.Apelido);
                cotista.LoadByPrimaryKey(campos, idCotista.Value);
                string apelido = cotista.Apelido;

                btnEditCodigoCotista.Text = Convert.ToString(idCotista.Value);
                textNomeCotista.Text = apelido;
            }
        }
    }

    protected void ASPxCallback1_Callback(object source, DevExpress.Web.CallbackEventArgs e) {
        string nome = "";
        if (!String.IsNullOrEmpty(e.Parameter) && e.Parameter != "null") {
            int idCarteira = Convert.ToInt32(e.Parameter);
            Carteira carteira = new Carteira();
            carteira.LoadByPrimaryKey(idCarteira);

            if (carteira.str.Apelido != "") {
                if (carteira.IsAtivo) {
                    //O permissionamento da carteira é dado direto no cliente vinculado à carteira
                    PermissaoCliente permissaoCliente = new PermissaoCliente();
                    if (permissaoCliente.RetornaAcessoClienteComControle(idCarteira, HttpContext.Current.User.Identity.Name)) {
                        Cliente cliente = new Cliente();
                        cliente.LoadByPrimaryKey(idCarteira);

                        DateTime dataDia = cliente.DataDia.Value;

                        nome = carteira.str.Apelido + "|" + dataDia.ToShortDateString() + "|" + Thread.CurrentThread.CurrentCulture.Name;
                    }
                    else {
                        nome = "no_access";
                    }
                }
                else {
                    nome = "no_active";
                }
            }
        }
        e.Result = nome;
    }

    protected void ASPxCallback2_Callback(object source, DevExpress.Web.CallbackEventArgs e) {
        string nome = "";
        if (!String.IsNullOrEmpty(e.Parameter) && e.Parameter != "null") {
            int idCotista = Convert.ToInt32(e.Parameter);
            Cotista cotista = new Cotista();
            cotista.LoadByPrimaryKey(idCotista);

            if (cotista.str.Apelido != "") {
                if (cotista.IsAtivo) {
                    PermissaoCotista permissaoCotista = new PermissaoCotista();
                    if (permissaoCotista.RetornaAcessoCotista(idCotista, HttpContext.Current.User.Identity.Name)) {
                        nome = cotista.str.Apelido;
                    }
                    else {
                        nome = "no_access";
                    }
                }
                else {
                    nome = "no_active";
                }
            }
        }
        e.Result = nome;
    }

    protected void EsDSCarteira_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e) {
        CarteiraCollection carteiraCollection = new CarteiraCollection();
        carteiraCollection.BuscaCarteirasComAcesso(HttpContext.Current.User.Identity.Name);

        e.Collection = carteiraCollection;
    }

    protected void EsDSCotista_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e) {
        CotistaCollection cotistaCollection = new CotistaCollection();
        cotistaCollection.BuscaCotistasComAcesso(HttpContext.Current.User.Identity.Name);

        e.Collection = cotistaCollection;
    }

    protected void btnVisualiza_Click(object sender, EventArgs e) {
        this.TrataErros();
        this.SelecionaRelatorio(VisaoRelatorio.visaoHTML);
    }

    protected void btnPDF_Click(object sender, EventArgs e) {
        this.TrataErros();
        this.SelecionaRelatorio(VisaoRelatorio.visaoPDF);
    }

    protected void btnExcel_Click(object sender, EventArgs e) {

        // Tratamento de erro Extra para Excel - Só pode gerar se tiver escolhido carteira
        if (btnEditCodigo.Text.Trim() == "") {
            throw new Exception("Obrigatório escolher uma Carteira para Relatório tipo Excel.");
        }

        this.TrataErros();
        this.SelecionaRelatorio(VisaoRelatorio.visaoExcel);
    }

    /// <summary>
    /// Trata os Erros de Relatorio
    /// </summary>
    private void TrataErros() {

        if (String.IsNullOrEmpty(this.textData.Text)) {
            throw new Exception("Campos com * são obrigatórios!");
        }

        if (Utilitario.IsDate(textData.Text)) {
            DateTime dataReport = Convert.ToDateTime(textData.Text);
            if (!Calendario.IsDiaUtil(dataReport, LocalFeriadoFixo.Brasil, TipoFeriado.Brasil)) {
                throw new Exception("Dia não útil.");
            }
        }

        if (btnEditCodigo.Text == "" && btnEditCodigoCotista.Text == "")
        {
            throw new Exception("Deve ser escolhido o cotista ou a carteira.");
        }

        if (btnEditCodigo.Text != "" && Utilitario.IsDate(textData.Text)) 
        {
            DateTime dataReport = Convert.ToDateTime(textData.Text);
            Cliente cliente = new Cliente();
            cliente.LoadByPrimaryKey(Convert.ToInt32(btnEditCodigo.Text));
            DateTime dataImplantacao = cliente.DataImplantacao.Value;
            DateTime dataDia = cliente.DataDia.Value;

            if (dataReport < dataImplantacao) {
                throw new Exception("Data passada anterior à data de implantação do Cliente: " + dataImplantacao.ToString("d"));
            }

            if (dataReport > dataDia) {
                throw new Exception("Data passada posterior à data atual do Cliente: " + dataDia.ToString("d"));
            }
        }
    }

    /// <summary>
    /// Seleciona o Relatorio a ser executado
    /// </summary>
    /// <param name="visao">PDF, Excel ou Report</param>
    private void SelecionaRelatorio(string visao) {
        Response.BufferOutput = true;
        //
        // Salva os parametros na session
        Session["textData"] = this.textData.Text;
        Session["btnEditCodigo"] = this.btnEditCodigo.Text;
        Session["btnEditCodigoCotista"] = this.btnEditCodigoCotista.Text;
        Session["dropOpcoesCampos"] = this.dropOpcoesCampos.SelectedItem.Value;
        Session["dropTipoRelatorio"] = this.dropTipoRelatorio.SelectedItem.Value;
        Session["dropTipoGrupamento"] = this.dropTipoGrupamento.SelectedItem.Value;
        Session["dropValorAplicado"] = this.dropValorAplicado.SelectedItem.Value;
        Session["dropLingua"] = this.dropLingua.SelectedItem.Value;
        //
        Session["chkRentabilidade"] = this.chkRentabilidade.Checked;
        //
        //Response.Redirect("~/Relatorios/Cotista/ReportSaldoAplicacaoCotista.aspx?Visao=" + visao);

        string url = "~/Relatorios/Cotista/ReportSaldoAplicacaoCotista.aspx?Visao=" + visao;
        string redirectURL = Page.ResolveClientUrl(url);

        string script = "window.location = '" + redirectURL + "';";
        ScriptManager.RegisterStartupScript(this, typeof(Page), "RedirectTo", script, true);
    }
}