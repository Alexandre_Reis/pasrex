﻿using System;
using Financial.Relatorio;
using System.IO;
using System.Web;
using System.Globalization;
using Financial.Web.Common;
using Financial.Fundo;
using Financial.InvestidorCotista;
using System.Collections.Generic;

public partial class _ReportSaldoAplicacaoCotista : BasePage {
    private static class VisaoRelatorio {
        public static string visaoHTML = "Report";
        public static string visaoPDF = "PDF";
        public static string visaoExcel = "Excel";
    }

    //
    private enum TipoOpcoesCampo {
        ValorPFee_Participacao = 1,
        Rendimento_PercentualRetorno = 2
    }
   
    new protected void Page_Load(object sender, EventArgs e) {
        string visao = Request.QueryString["Visao"].ToString();

        if (visao == VisaoRelatorio.visaoHTML) {
            #region HTML

            #region Parametros
            ReportSaldoAplicacaoCotista.TipoRelatorio tipoRelatorio = ReportSaldoAplicacaoCotista.TipoRelatorio.Analitico;
            ReportSaldoAplicacaoCotistaView2.TipoRelatorio tipoRelatorioView2 = ReportSaldoAplicacaoCotistaView2.TipoRelatorio.Analitico;
            //
            ReportSaldoAplicacaoCotista.TipoGrupamento tipoGrupamento = ReportSaldoAplicacaoCotista.TipoGrupamento.PorNome;
            ReportSaldoAplicacaoCotistaView2.TipoGrupamento tipoGrupamentoView2 = ReportSaldoAplicacaoCotistaView2.TipoGrupamento.PorNome;
            //
            ReportSaldoAplicacaoCotista.OpcaoValorAplicado opcaoValorAplicado = ReportSaldoAplicacaoCotista.OpcaoValorAplicado.ValorAtualizado;
            ReportSaldoAplicacaoCotistaView2.OpcaoValorAplicado opcaoValorAplicadoView2 = ReportSaldoAplicacaoCotistaView2.OpcaoValorAplicado.ValorAtualizado;

            ReportSaldoAplicacaoCotista.TipoLingua tipoLingua = ReportSaldoAplicacaoCotista.TipoLingua.Portugues;
            ReportSaldoAplicacaoCotistaView2.TipoLingua tipoLinguaView2 = ReportSaldoAplicacaoCotistaView2.TipoLingua.Portugues;
            //
            TipoOpcoesCampo tipoOpcoesCampo;

            int? idCarteira = null;
            int? idCotista = null;

            // Obrigatorio
            DateTime data = Convert.ToDateTime(Session["textData"]);

            if (Session["btnEditCodigo"].ToString() != "") {
                idCarteira = Convert.ToInt32(Session["btnEditCodigo"]);
            }

            if (Session["btnEditCodigoCotista"].ToString() != "") {
                idCotista = Convert.ToInt32(Session["btnEditCodigoCotista"]);
            }

            tipoOpcoesCampo = (TipoOpcoesCampo)Convert.ToInt32(Session["dropOpcoesCampos"]);
            //
            if (tipoOpcoesCampo == TipoOpcoesCampo.ValorPFee_Participacao) {
                tipoRelatorio = (ReportSaldoAplicacaoCotista.TipoRelatorio)Convert.ToInt32(Session["dropTipoRelatorio"]);
                tipoGrupamento = (ReportSaldoAplicacaoCotista.TipoGrupamento)Convert.ToInt32(Session["dropTipoGrupamento"]);
                opcaoValorAplicado = (ReportSaldoAplicacaoCotista.OpcaoValorAplicado)Convert.ToInt32(Session["dropValorAplicado"]);
                tipoLingua = (ReportSaldoAplicacaoCotista.TipoLingua)Convert.ToInt32(Session["dropLingua"]);
            }
            else if (tipoOpcoesCampo == TipoOpcoesCampo.Rendimento_PercentualRetorno) {
                tipoRelatorioView2 = (ReportSaldoAplicacaoCotistaView2.TipoRelatorio)Convert.ToInt32(Session["dropTipoRelatorio"]);
                tipoGrupamentoView2 = (ReportSaldoAplicacaoCotistaView2.TipoGrupamento)Convert.ToInt32(Session["dropTipoGrupamento"]);
                opcaoValorAplicadoView2 = (ReportSaldoAplicacaoCotistaView2.OpcaoValorAplicado)Convert.ToInt32(Session["dropValorAplicado"]);
                tipoLinguaView2 = (ReportSaldoAplicacaoCotistaView2.TipoLingua)Convert.ToInt32(Session["dropLingua"]);
            }

            bool chkRentabilidade = Convert.ToBoolean(Session["chkRentabilidade"]);
            #endregion

            // É possivel exportar Excel atraves do HMTL mas para isso não pode ser no reportMaster
            bool usaReportMaster = true;
            if (idCarteira.HasValue) {
                usaReportMaster = false;
            }

            ReportSaldoAplicacaoCotista reportHTML1 = null;
            ReportSaldoAplicacaoCotistaView2 reportHTML2 = null;

            ReportMaster r = new ReportMaster();
            string filename = "SaldoAplicacaoCotista";
            //
            if (tipoOpcoesCampo == TipoOpcoesCampo.ValorPFee_Participacao) {
                #region ValorPFee_Participacao

                // Somente 1 Carteira
                if (idCarteira.HasValue) {
                    #region Apenas 1 Carteira
                    reportHTML1 = new ReportSaldoAplicacaoCotista(idCarteira.Value, idCotista, data, tipoRelatorio, tipoGrupamento, opcaoValorAplicado,
                                                                            chkRentabilidade, HttpContext.Current.User.Identity.Name, tipoLingua);
                    #endregion
                }
                else { // Varias Carteiras (assume então que o IdCotista foi passado)
                    #region Varias Carteiras
                    CotistaCollection cotistaCollection = new CotistaCollection();
                    List<int> idsCarteira = cotistaCollection.BuscaCarteirasDoCotista(idCotista.Value);

                    foreach (int idCarteiraAux in idsCarteira) {
                        ReportSaldoAplicacaoCotista report = new ReportSaldoAplicacaoCotista(idCarteiraAux, idCotista, data, tipoRelatorio, tipoGrupamento, opcaoValorAplicado,
                                                            chkRentabilidade, HttpContext.Current.User.Identity.Name, tipoLingua);

                        report.CreateDocument();
                        if (report.TemDados) {
                            //
                            r.PrintingSystem.Pages.AddRange(report.PrintingSystem.Pages); // Adiciona Relatorio                                                
                        }
                    }
                    #endregion

                    filename += "_" + data.Day + "-" + data.Month + "-" + data.Year;
                }

                #endregion
            }
            else if (tipoOpcoesCampo == TipoOpcoesCampo.Rendimento_PercentualRetorno) {
                #region ValorPFee_PercentualRetorno

                // Somente 1 Carteira
                if (idCarteira.HasValue) {
                    #region Apenas 1 Carteira
                    reportHTML2 = new ReportSaldoAplicacaoCotistaView2(idCarteira.Value, idCotista, data, tipoRelatorioView2, tipoGrupamentoView2,
                                opcaoValorAplicadoView2, chkRentabilidade, HttpContext.Current.User.Identity.Name, tipoLinguaView2);
                    #endregion
                }
                else { // Varias Carteiras (assume então que o IdCotista foi passado)
                    #region Varias Carteiras
                    CotistaCollection cotistaCollection = new CotistaCollection();
                    List<int> idsCarteira = cotistaCollection.BuscaCarteirasDoCotista(idCotista.Value);

                    foreach (int idCarteiraAux in idsCarteira) {
                        ReportSaldoAplicacaoCotistaView2 report = new ReportSaldoAplicacaoCotistaView2(idCarteiraAux, idCotista, data, tipoRelatorioView2, tipoGrupamentoView2,
                            opcaoValorAplicadoView2, chkRentabilidade, HttpContext.Current.User.Identity.Name, tipoLinguaView2);

                        report.CreateDocument();
                        if (report.TemDados) {
                            r.PrintingSystem.Pages.AddRange(report.PrintingSystem.Pages); // Adiciona Relatorio
                        }
                    }
                    #endregion

                    filename += "_" + data.Day + "-" + data.Month + "-" + data.Year;
                }

                #endregion
            }

            #region Acrescenta Relatorio Sem Dados se não Existir Dados
            // Não Existe Dados Para Exportar - Exporta PDF Sem Dados
            if (r.PrintingSystem.Pages.Count == 0) {

                #region ReportSemDados
                ReportSemDados reportSemDados = new ReportSemDados();
                reportSemDados.CreateDocument();
                //
                r.PrintingSystem.Pages.AddRange(reportSemDados.PrintingSystem.Pages);
                #endregion
            }
            #endregion

            if (usaReportMaster) {
                ReportViewer1.Report = r; // reportMaster
            }
            else {
                if (tipoOpcoesCampo == TipoOpcoesCampo.ValorPFee_Participacao) {
                    ReportViewer1.Report = reportHTML1;
                }
                else {
                    ReportViewer1.Report = reportHTML2;
                }
            }
                        
            #endregion
        }
        else if (visao == VisaoRelatorio.visaoPDF) {
            #region PDF

            #region Parametros
            ReportSaldoAplicacaoCotista.TipoRelatorio tipoRelatorio = ReportSaldoAplicacaoCotista.TipoRelatorio.Analitico;
            ReportSaldoAplicacaoCotistaView2.TipoRelatorio tipoRelatorioView2 = ReportSaldoAplicacaoCotistaView2.TipoRelatorio.Analitico;
            //
            ReportSaldoAplicacaoCotista.TipoGrupamento tipoGrupamento = ReportSaldoAplicacaoCotista.TipoGrupamento.PorNome;
            ReportSaldoAplicacaoCotistaView2.TipoGrupamento tipoGrupamentoView2 = ReportSaldoAplicacaoCotistaView2.TipoGrupamento.PorNome;
            //
            ReportSaldoAplicacaoCotista.OpcaoValorAplicado opcaoValorAplicado = ReportSaldoAplicacaoCotista.OpcaoValorAplicado.ValorAtualizado;
            ReportSaldoAplicacaoCotistaView2.OpcaoValorAplicado opcaoValorAplicadoView2 = ReportSaldoAplicacaoCotistaView2.OpcaoValorAplicado.ValorAtualizado;

            ReportSaldoAplicacaoCotista.TipoLingua tipoLingua = ReportSaldoAplicacaoCotista.TipoLingua.Portugues;
            ReportSaldoAplicacaoCotistaView2.TipoLingua tipoLinguaView2 = ReportSaldoAplicacaoCotistaView2.TipoLingua.Portugues;
            //
            TipoOpcoesCampo tipoOpcoesCampo;

            int? idCarteira = null;
            int? idCotista = null;

            // Obrigatorio
            DateTime data = Convert.ToDateTime(Session["textData"]);

            if (Session["btnEditCodigo"].ToString() != "") {
                idCarteira = Convert.ToInt32(Session["btnEditCodigo"]);
            }

            if (Session["btnEditCodigoCotista"].ToString() != "") {
                idCotista = Convert.ToInt32(Session["btnEditCodigoCotista"]);
            }

            tipoOpcoesCampo = (TipoOpcoesCampo)Convert.ToInt32(Session["dropOpcoesCampos"]);
            //
            if (tipoOpcoesCampo == TipoOpcoesCampo.ValorPFee_Participacao) {
                tipoRelatorio = (ReportSaldoAplicacaoCotista.TipoRelatorio)Convert.ToInt32(Session["dropTipoRelatorio"]);
                tipoGrupamento = (ReportSaldoAplicacaoCotista.TipoGrupamento)Convert.ToInt32(Session["dropTipoGrupamento"]);
                opcaoValorAplicado = (ReportSaldoAplicacaoCotista.OpcaoValorAplicado)Convert.ToInt32(Session["dropValorAplicado"]);
                tipoLingua = (ReportSaldoAplicacaoCotista.TipoLingua)Convert.ToInt32(Session["dropLingua"]);
            }
            else if (tipoOpcoesCampo == TipoOpcoesCampo.Rendimento_PercentualRetorno) {
                tipoRelatorioView2 = (ReportSaldoAplicacaoCotistaView2.TipoRelatorio)Convert.ToInt32(Session["dropTipoRelatorio"]);
                tipoGrupamentoView2 = (ReportSaldoAplicacaoCotistaView2.TipoGrupamento)Convert.ToInt32(Session["dropTipoGrupamento"]);
                opcaoValorAplicadoView2 = (ReportSaldoAplicacaoCotistaView2.OpcaoValorAplicado)Convert.ToInt32(Session["dropValorAplicado"]);
                tipoLinguaView2 = (ReportSaldoAplicacaoCotistaView2.TipoLingua)Convert.ToInt32(Session["dropLingua"]);
            }

            bool chkRentabilidade = Convert.ToBoolean(Session["chkRentabilidade"]);
            #endregion

            ReportMaster r = new ReportMaster();
            string filename = "SaldoAplicacaoCotista";
            //
            if (tipoOpcoesCampo == TipoOpcoesCampo.ValorPFee_Participacao) {
                #region ValorPFee_Participacao

                // Somente 1 Carteira
                if (idCarteira.HasValue) {
                    #region Apenas 1 Carteira
                    ReportSaldoAplicacaoCotista report = new ReportSaldoAplicacaoCotista(idCarteira.Value, idCotista, data, tipoRelatorio, tipoGrupamento, opcaoValorAplicado,
                                                                            chkRentabilidade, HttpContext.Current.User.Identity.Name, tipoLingua);

                    report.CreateDocument();
                    //
                    r.PrintingSystem.Pages.AddRange(report.PrintingSystem.Pages); // Adiciona Relatorio

                    filename += idCarteira.Value.ToString() + "_" + data.Day + "-" + data.Month + "-" + data.Year;
                    #endregion
                }
                else { // Varias Carteiras (assume então que o IdCotista foi passado)
                    #region Varias Carteiras
                    CotistaCollection cotistaCollection = new CotistaCollection();
                    List<int> idsCarteira = cotistaCollection.BuscaCarteirasDoCotista(idCotista.Value);

                    foreach (int idCarteiraAux in idsCarteira) {
                        ReportSaldoAplicacaoCotista report = new ReportSaldoAplicacaoCotista(idCarteiraAux, idCotista, data, tipoRelatorio, tipoGrupamento, opcaoValorAplicado,
                                                            chkRentabilidade, HttpContext.Current.User.Identity.Name, tipoLingua);

                        report.CreateDocument();
                        if (report.TemDados) {
                            //
                            r.PrintingSystem.Pages.AddRange(report.PrintingSystem.Pages); // Adiciona Relatorio                                                
                        }
                    }
                    #endregion

                    filename += "_" + data.Day + "-" + data.Month + "-" + data.Year;
                }

                #endregion
            }
            else if (tipoOpcoesCampo == TipoOpcoesCampo.Rendimento_PercentualRetorno) {
                #region ValorPFee_PercentualRetorno

                // Somente 1 Carteira
                if (idCarteira.HasValue) {
                    #region Apenas 1 Carteira
                    ReportSaldoAplicacaoCotistaView2 report = new ReportSaldoAplicacaoCotistaView2(idCarteira.Value, idCotista, data, tipoRelatorioView2, tipoGrupamentoView2,
                                opcaoValorAplicadoView2, chkRentabilidade, HttpContext.Current.User.Identity.Name, tipoLinguaView2);

                    report.CreateDocument();
                    //
                    r.PrintingSystem.Pages.AddRange(report.PrintingSystem.Pages); // Adiciona Relatorio
                    //
                    filename += idCarteira.Value.ToString() + "_" + data.Day + "-" + data.Month + "-" + data.Year;
                    #endregion
                }
                else { // Varias Carteiras (assume então que o IdCotista foi passado)
                    #region Varias Carteiras
                    CotistaCollection cotistaCollection = new CotistaCollection();
                    List<int> idsCarteira = cotistaCollection.BuscaCarteirasDoCotista(idCotista.Value);

                    foreach (int idCarteiraAux in idsCarteira) {
                        ReportSaldoAplicacaoCotistaView2 report = new ReportSaldoAplicacaoCotistaView2(idCarteiraAux, idCotista, data, tipoRelatorioView2, tipoGrupamentoView2,
                            opcaoValorAplicadoView2, chkRentabilidade, HttpContext.Current.User.Identity.Name, tipoLinguaView2);

                        report.CreateDocument();
                        if (report.TemDados) {
                            r.PrintingSystem.Pages.AddRange(report.PrintingSystem.Pages); // Adiciona Relatorio
                        }
                    }
                    #endregion

                    filename += "_" + data.Day + "-" + data.Month + "-" + data.Year;
                }

                #endregion
            }

            #region Acrescenta Relatorio Sem Dados se não Existir Dados
            // Não Existe Dados Para Exportar - Exporta PDF Sem Dados
            if (r.PrintingSystem.Pages.Count == 0) {

                #region ReportSemDados
                ReportSemDados reportSemDados = new ReportSemDados();
                reportSemDados.CreateDocument();
                //
                r.PrintingSystem.Pages.AddRange(reportSemDados.PrintingSystem.Pages);
                #endregion
            }
            #endregion

            #region PDF
            MemoryStream ms = new MemoryStream();
            r.ExportToPdf(ms);
            ms.Seek(0, SeekOrigin.Begin);
            Response.ClearContent();
            Response.ClearHeaders();
            Response.Buffer = true;
            Response.Cache.SetCacheability(HttpCacheability.Private);
            Response.ContentType = "application/pdf";
            Response.AddHeader("Content-Length", ms.Length.ToString(CultureInfo.CurrentCulture));
            Response.AddHeader("Content-Disposition", String.Format("attachment;filename=" + filename + ".pdf"));

            Response.BinaryWrite(ms.ToArray());
            ms.Close();
            HttpContext.Current.ApplicationInstance.CompleteRequest();
            Response.End();
            #endregion

            #endregion
        }
        else if (visao == VisaoRelatorio.visaoExcel) {
            #region Excel - Só exporta se tiver escolhido 1 Carteira - Conferencia já foi feita - IdCarteira deve estar preeenchido

            #region Parametros
            ReportSaldoAplicacaoCotista.TipoRelatorio tipoRelatorio = ReportSaldoAplicacaoCotista.TipoRelatorio.Analitico;
            ReportSaldoAplicacaoCotistaView2.TipoRelatorio tipoRelatorioView2 = ReportSaldoAplicacaoCotistaView2.TipoRelatorio.Analitico;
            //
            ReportSaldoAplicacaoCotista.TipoGrupamento tipoGrupamento = ReportSaldoAplicacaoCotista.TipoGrupamento.PorNome;
            ReportSaldoAplicacaoCotistaView2.TipoGrupamento tipoGrupamentoView2 = ReportSaldoAplicacaoCotistaView2.TipoGrupamento.PorNome;
            //
            ReportSaldoAplicacaoCotista.OpcaoValorAplicado opcaoValorAplicado = ReportSaldoAplicacaoCotista.OpcaoValorAplicado.ValorAtualizado;
            ReportSaldoAplicacaoCotistaView2.OpcaoValorAplicado opcaoValorAplicadoView2 = ReportSaldoAplicacaoCotistaView2.OpcaoValorAplicado.ValorAtualizado;

            ReportSaldoAplicacaoCotista.TipoLingua tipoLingua = ReportSaldoAplicacaoCotista.TipoLingua.Portugues;
            ReportSaldoAplicacaoCotistaView2.TipoLingua tipoLinguaView2 = ReportSaldoAplicacaoCotistaView2.TipoLingua.Portugues;
            //
            TipoOpcoesCampo tipoOpcoesCampo;

            int? idCarteira = null;
            int? idCotista = null;

            // Obrigatorio
            DateTime data = Convert.ToDateTime(Session["textData"]);

            if (Session["btnEditCodigo"].ToString() != "") {
                idCarteira = Convert.ToInt32(Session["btnEditCodigo"]);
            }

            if (Session["btnEditCodigoCotista"].ToString() != "") {
                idCotista = Convert.ToInt32(Session["btnEditCodigoCotista"]);
            }

            tipoOpcoesCampo = (TipoOpcoesCampo)Convert.ToInt32(Session["dropOpcoesCampos"]);
            //
            if (tipoOpcoesCampo == TipoOpcoesCampo.ValorPFee_Participacao) {
                tipoRelatorio = (ReportSaldoAplicacaoCotista.TipoRelatorio)Convert.ToInt32(Session["dropTipoRelatorio"]);
                tipoGrupamento = (ReportSaldoAplicacaoCotista.TipoGrupamento)Convert.ToInt32(Session["dropTipoGrupamento"]);
                opcaoValorAplicado = (ReportSaldoAplicacaoCotista.OpcaoValorAplicado)Convert.ToInt32(Session["dropValorAplicado"]);
                tipoLingua = (ReportSaldoAplicacaoCotista.TipoLingua)Convert.ToInt32(Session["dropLingua"]);
            }
            else if (tipoOpcoesCampo == TipoOpcoesCampo.Rendimento_PercentualRetorno) {
                tipoRelatorioView2 = (ReportSaldoAplicacaoCotistaView2.TipoRelatorio)Convert.ToInt32(Session["dropTipoRelatorio"]);
                tipoGrupamentoView2 = (ReportSaldoAplicacaoCotistaView2.TipoGrupamento)Convert.ToInt32(Session["dropTipoGrupamento"]);
                opcaoValorAplicadoView2 = (ReportSaldoAplicacaoCotistaView2.OpcaoValorAplicado)Convert.ToInt32(Session["dropValorAplicado"]);
                tipoLinguaView2 = (ReportSaldoAplicacaoCotistaView2.TipoLingua)Convert.ToInt32(Session["dropLingua"]);
            }

            bool chkRentabilidade = Convert.ToBoolean(Session["chkRentabilidade"]);
            #endregion
            //
            string filename = "SaldoAplicacaoCotista";
            filename += idCarteira.Value.ToString() + "_" + data.Day + "-" + data.Month + "-" + data.Year;
            //
            ReportSaldoAplicacaoCotista report1 = null;
            ReportSaldoAplicacaoCotistaView2 report2 = null;
            MemoryStream ms = new MemoryStream();
            //
            if (tipoOpcoesCampo == TipoOpcoesCampo.ValorPFee_Participacao) {
                
                report1 = new ReportSaldoAplicacaoCotista(idCarteira.Value, idCotista, data, tipoRelatorio, tipoGrupamento, opcaoValorAplicado,
                                                                            chkRentabilidade, HttpContext.Current.User.Identity.Name, tipoLingua);
                //
                report1.ExportToXls(ms);
            }
            else if (tipoOpcoesCampo == TipoOpcoesCampo.Rendimento_PercentualRetorno) {
                
                report2 = new ReportSaldoAplicacaoCotistaView2(idCarteira.Value, idCotista, data, tipoRelatorioView2, tipoGrupamentoView2,
                            opcaoValorAplicadoView2, chkRentabilidade, HttpContext.Current.User.Identity.Name, tipoLinguaView2);
                //
                report2.ExportToXls(ms);
            }
                                                            
            ms.Seek(0, SeekOrigin.Begin);
            Response.ClearContent();
            Response.ClearHeaders();
            Response.Buffer = true;
            Response.Cache.SetCacheability(HttpCacheability.Private);
            Response.ContentType = "application/vnd.ms-excel";
            Response.AddHeader("Content-Length", ms.Length.ToString(CultureInfo.CurrentCulture));
            Response.AddHeader("Content-Disposition", String.Format("attachment;filename=" + filename + ".xls"));

            Response.BinaryWrite(ms.ToArray());
            ms.Close();
            HttpContext.Current.ApplicationInstance.CompleteRequest();
            Response.End();

            #endregion
        }
    }
}