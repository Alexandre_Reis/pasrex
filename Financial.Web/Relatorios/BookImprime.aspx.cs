﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using Financial.Security;
using Financial.ContaCorrente;
using System.Text;
using DevExpress.Web;
using Financial.Web.Util;
using System.Drawing;
using Financial.Investidor;
using Financial.Fundo;
using Financial.Fundo.Enums;
using Financial.Fundo.Exceptions;
using Financial.Security.Enums;
using System.Collections.Generic;
using EntitySpaces.Interfaces;
using System.IO;
using System.Globalization;
using Financial.Relatorio;
using Financial.Util;
using Financial.Common.Enums;
using System.Net.Mail;
using System.Net.Configuration;
using System.Web.Configuration;
using Financial.InvestidorCotista;
using Financial.InvestidorCotista.Enums;
using Financial.Investidor.Enums;
using System.Threading;
using Financial.WebConfigConfiguration;
using System.Diagnostics;
using Financial.Web.Common;
using Financial.Web.Enums;
using Financial.CRM;
using DevExpress.XtraPrinting;
using System.Net;
using System.Security.Cryptography.X509Certificates;
using System.Net.Security;

public partial class Consultas_BookImprime : BasePage
{

    #region Variaveis Internas
    /* Informações do Cotista para mandar email */
    private class DataHolderEmailCotista
    {
        public int? idCotista = null;
        public int? idCarteira = null;
        public int? idBook = null;
        //
        public string emailOrigem = null;
        public string emailDestino = null;
        public string assunto = null;
        public string mensagem = null;
        //
        public ReportExtratoCotista report = null;
        public string nomeArquivoPdf = null;
    }
    private List<DataHolderEmailCotista> dataHolderEmailCotista = new List<DataHolderEmailCotista>();

    // Guarda a Mensagem de Erro dos Relatórios
    private string mensagem;

    private static class TipoEnvioRelatorio
    {
        public static string Email = "email";
        public static string PDF = "pdf";
    }

    // Numero de Mensagens de Email Enviadas
    private int controleMensagensEnviadas = 0;

    public bool AlgumEmailEnviado
    {
        get
        {
            return this.controleMensagensEnviadas != 0;
        }
    }

    // Senha para PDF
    private string pdfPassword = "";

    // Guarda os emails para enviar um segundo email com a senha do PDF
    private List<string> listaEmailsSenhaPDF = new List<string>();

    private string emailSenha = "";
    #endregion

    new protected void Page_Load(object sender, EventArgs e)
    {
        this.TrataTravamentoCampos();
        //
        // Limpa os Dados
        //
        btnEditCodigoCliente.Focus();
    }

    #region DataSources
    protected void EsDSCliente_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        ClienteCollection clienteCollection = new ClienteCollection();
        clienteCollection.BuscaClientesComAcesso(HttpContext.Current.User.Identity.Name);

        e.Collection = clienteCollection;
    }

    protected void EsDSBook_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        BookCollection bookCollection = new BookCollection();
        bookCollection.LoadAll();

        e.Collection = bookCollection;
    }
    #endregion

    #region Eventos Grid

    /// <summary>
    /// 
    /// </summary>
    /// <returns>String representando a mensagem de erro ou vazio se não houver erro</returns>
    private string TrataErros()
    {
        string retorno = "";

        if (String.IsNullOrEmpty(this.textData.Text))
        {
            retorno = "Campos com * são obrigatórios!";
        }
        else if (this.dropBook.Text == "" && this.btnEditCodigoCliente.Text == "")
        {
            retorno = "Deve ser selecionado o Cliente ou o Book!";
        }
        else if (!this.TemRelatoriosAssociados())
        {
            retorno = "Não há relatórios a serem gerados!";
        }

        return retorno;
    }

    protected void callbackRelatorio_Callback(object source, DevExpress.Web.CallbackEventArgs e)
    {
        e.Result = this.TrataErros();
    }

    protected void gridCliente_CustomDataCallback(object sender, DevExpress.Web.ASPxGridViewCustomDataCallbackEventArgs e)
    {
        e.Result = gridCliente.GetRowValues(Convert.ToInt32(e.Parameters), "IdCliente");
    }

    protected void gridCliente_HtmlRowCreated(object sender, DevExpress.Web.ASPxGridViewTableRowEventArgs e)
    {
        e.Row.Attributes.Add("onmouseover", "backgroundColor = this.style.backgroundColor; this.style.backgroundColor='#E0E9EB';");
        e.Row.Attributes.Add("onmouseout", "this.style.backgroundColor=backgroundColor;");
    }

    private void TrataTravamentoCampos()
    {
        Usuario usuario = new Usuario();
        usuario.BuscaUsuario(HttpContext.Current.User.Identity.Name);
        int tipoTrava = usuario.TipoTrava.Value;

        if (tipoTrava == (int)TipoTravaUsuario.TravaCliente)
        {
            btnEditCodigoCliente.Enabled = false;

            PermissaoCliente permissaoCliente = new PermissaoCliente();
            int? idCliente = permissaoCliente.RetornaClienteAssociado(HttpContext.Current.User.Identity.Name);

            if (idCliente.HasValue)
            {
                Cliente cliente = new Cliente();

                List<esQueryItem> campos = new List<esQueryItem>();
                campos.Add(cliente.Query.Apelido);
                cliente.LoadByPrimaryKey(campos, idCliente.Value);
                string apelido = cliente.Apelido;

                btnEditCodigoCliente.Text = Convert.ToString(idCliente.Value);
                textNome.Text = apelido;
            }
        }
    }

    protected void ASPxCallback1_Callback(object source, DevExpress.Web.CallbackEventArgs e)
    {
        string nome = "";
        if (!String.IsNullOrEmpty(e.Parameter) && e.Parameter != "null")
        {
            int idCliente = Convert.ToInt32(e.Parameter);
            Cliente cliente = new Cliente();
            cliente.LoadByPrimaryKey(idCliente);

            if (cliente.str.Apelido != "")
            {
                if (cliente.IsAtivo)
                {
                    //O permissionamento da cliente Ã© dado direto no cliente vinculado Ã  cliente
                    PermissaoCliente permissaoCliente = new PermissaoCliente();
                    if (permissaoCliente.RetornaAcessoClienteComControle(idCliente, HttpContext.Current.User.Identity.Name))
                    {
                        nome = cliente.str.Apelido;
                    }
                    else
                    {
                        nome = "no_access";
                    }
                }
                else
                {
                    nome = "no_active";
                }
            }
        }
        e.Result = nome;
    }
    #endregion

    #region Constroi PDF na Saida
    private void ExportaPDF(ReportMaster reportMaster, string fileName)
    {
        MemoryStream ms = new MemoryStream();

        //reportMaster.ExportOptions.Pdf.PasswordSecurityOptions.OpenPassword = "teste";

        reportMaster.ExportToPdf(ms);

        ms.Seek(0, SeekOrigin.Begin);
        Response.ClearContent();
        Response.ClearHeaders();
        Response.Buffer = true;
        Response.Cache.SetCacheability(HttpCacheability.Private);
        Response.ContentType = "application/pdf";
        Response.AddHeader("Content-Length", ms.Length.ToString(CultureInfo.CurrentCulture));
        Response.AddHeader("Content-Disposition", String.Format("attachment;filename=" + fileName + ".pdf"));

        Response.BinaryWrite(ms.ToArray());
        ms.Close();
        HttpContext.Current.ApplicationInstance.CompleteRequest();

        try
        {
            Response.End();
        }
        catch (Exception)
        {
            return;
        }
    }
    #endregion

    /// <summary>
    /// CallBack para o botão Excel
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void callbackExportaExcel_Callback(object source, DevExpress.Web.CallbackEventArgs e)
    {
        e.Result = this.TrataErros();

        if (e.Result != "")
        {
            return;
        }

        try
        {

            // Dicionario Nome do Arquivo - Memory Stream dos arquivos excel
            Dictionary<string, MemoryStream> reports = new Dictionary<string, MemoryStream>();
            //
            this.MontaReportExcel(out reports);

            if (!String.IsNullOrEmpty(this.mensagem))
            {
                e.Result = this.mensagem;
                return;
            }

            // Salva Memory Stream do Arquivo Excel na Session                
            Session["streamReports"] = reports;
        }
        catch (Exception erro)
        {
            e.Result = erro.Message;
            return;
        }

        string newLocation = HttpContext.Current.Request.Url.AbsolutePath.Replace("BookImprime.aspx", "ExibeExcelBookImprime.aspx");
        e.Result = "redirect:" + newLocation;
    }

    #region Log
    private void LogEventViewer(string sEvent)
    {
        string sSource = "Financial";
        string sLog = "Application";

        if (!EventLog.SourceExists(sSource))
            EventLog.CreateEventSource(sSource, sLog);

        EventLog.WriteEntry(sSource, sEvent, EventLogEntryType.Warning, 234);
    }
    #endregion

    #region Consultas Internas
    private BookClienteCollection MontaCollectionBookCliente()
    {
        BookClienteQuery bookClienteQuery = new BookClienteQuery("B");
        ClienteQuery clienteQuery = new ClienteQuery("C");

        bookClienteQuery.InnerJoin(clienteQuery).On(clienteQuery.IdCliente == bookClienteQuery.IdCliente);

        bookClienteQuery.Where(clienteQuery.StatusAtivo.Equal((byte)StatusAtivoCliente.Ativo));

        if (btnEditCodigoCliente.Text != "")
        {
            bookClienteQuery.Where(bookClienteQuery.IdCliente.Equal(Convert.ToInt32(btnEditCodigoCliente.Text)));
        }
        if (dropBook.Text != "")
        {
            bookClienteQuery.Where(bookClienteQuery.IdBook.Equal(Convert.ToInt32(dropBook.SelectedItem.Value)));
        }

        bookClienteQuery.Where(bookClienteQuery.Imprime.Equal("S"));
        bookClienteQuery.OrderBy(bookClienteQuery.IdCliente.Ascending);

        BookClienteCollection bookClienteCollection = new BookClienteCollection();
        bookClienteCollection.Load(bookClienteQuery);

        return bookClienteCollection;
    }

    private BookCollection MontaCollectionBookRelatorio()
    {
        BookCollection bookCollection = new BookCollection();

        if (dropBook.Text != "")
        {
            bookCollection.Query.Where(bookCollection.Query.IdBook.Equal(Convert.ToInt32(dropBook.SelectedItem.Value)));
        }

        bookCollection.Query.Where(bookCollection.Query.Tipo.Equal((byte)TipoBook.BookGeral));
        bookCollection.Query.OrderBy(bookCollection.Query.IdBook.Ascending);

        bookCollection.Query.Load();

        return bookCollection;
    }
    #endregion

    #region Button Pdf/Email e Trata Erro
    private bool TemRelatoriosAssociados()
    {
        BookRelatorioQuery bookRelatorioQuery = new BookRelatorioQuery("BR");
        BookClienteQuery bookClienteQuery = new BookClienteQuery("BC");

        if (dropBook.SelectedIndex != -1)
        {
            bookRelatorioQuery.Where(bookRelatorioQuery.IdBook.Equal(Convert.ToInt32(dropBook.SelectedItem.Value)));
        }
        if (btnEditCodigoCliente.Text != "")
        {
            bookRelatorioQuery.InnerJoin(bookClienteQuery).On(bookClienteQuery.IdBook == bookRelatorioQuery.IdBook);
            bookRelatorioQuery.Where(bookClienteQuery.IdCliente.Equal(Convert.ToInt32(btnEditCodigoCliente.Text)));
        }

        BookRelatorioCollection bookRelatorioCollection = new BookRelatorioCollection();
        bookRelatorioCollection.Load(bookRelatorioQuery);

        return bookRelatorioCollection.Count > 0;
    }

    protected void btnPDF_Click(object sender, EventArgs e)
    {
        this.MontaReport(TipoEnvioRelatorio.PDF);
    }

    protected void callbackEmail_Callback(object source, DevExpress.Web.CallbackEventArgs e)
    {

        // Gera nova Senha a Cada vez
        if (checkSenha.Checked)
        {
            this.pdfPassword = Utilitario.GeraSenha();
            this.listaEmailsSenhaPDF.Clear(); // zera a lista
        }

        this.MontaReport(TipoEnvioRelatorio.Email);

        e.Result = !this.AlgumEmailEnviado
                    ? "Nenhum Email Enviado. Critério Não Satisfeito"
                    : this.mensagem == "" ? "Email enviado com sucesso!"
                                          : "Email enviado com sucesso!" + "\n\n" + this.mensagem;
    }
    #endregion

    /// <summary>
    /// Dado uma lista de Reports Monta Attachments para serem incluidos num email   
    /// </summary>
    /// <param name="listaReports"></param>
    /// <param name="filenames">Nomes dos arquivos a serem atachados</param>
    /// <returns>Lista de Arquivos Atachados</returns>
    /// <exception cref="ArgumentException">Se tamanho de listaReports for diferente do tamanho de filenames</exception>
    private List<Attachment> MontaAttachments(List<ReportMaster> listaReports, List<string> filenames)
    {
        if (listaReports.Count != filenames.Count)
        {
            throw new ArgumentException("ListaReports e Filenames devem ter o mesmo numero de elementos");
        }

        List<Attachment> listaAttachment = new List<Attachment>();

        for (int i = 0; i < listaReports.Count; i++)
        {
            string fileName = filenames[i] + ".pdf";
            ReportMaster report = listaReports[i];
            //
            report.ExportOptions.Pdf.PasswordSecurityOptions.OpenPassword = this.pdfPassword;
            //
            MemoryStream ms = new MemoryStream();
            report.ExportToPdf(ms);
            ms.Seek(0, SeekOrigin.Begin);
            Attachment att = new Attachment(ms, fileName, "application/pdf");

            listaAttachment.Add(att);
        }

        return listaAttachment;
    }

    /// <summary>
    /// Envia uma Mensagem de Email
    /// </summary>
    /// <param name="listaEmail">Emails Destino para quem se quer mandar a mensagem</param>
    /// <param name="attachments">Arquivos Attachments a serem mandados na mensagem</param>
    /// <param name="subject">Assunto da Mensagem</param>
    /// <param name="body">Corpo da Mensagem</param>
    private void SendMessage(List<string> listaEmail, List<Attachment> attachments, string from, string subject, string body)
    {
        Configuration config = WebConfigurationManager.OpenWebConfiguration(HttpContext.Current.Request.ApplicationPath);
        MailSettingsSectionGroup settings = (MailSettingsSectionGroup)config.GetSectionGroup("system.net/mailSettings");

        // Create a new message and attach the PDF report to it.
        MailMessage mail = new MailMessage();
        for (int i = 0; i < attachments.Count; i++)
        {
            mail.Attachments.Add(attachments[i]);
        }

        if (listaEmail.Count != 0)
        {
            string emailsEnviados = "";
            for (int j = 0; j < listaEmail.Count; j++)
            {
                mail.To.Add(new MailAddress(listaEmail[j]));

                emailsEnviados = emailsEnviados + ";" + listaEmail[j];
            }
            // Specify sender and recipient options for the e-mail message.
            mail.From = new MailAddress(from);

            // Specify other e-mail options.
            mail.Subject = subject;
            mail.Body = body;

            // Send the e-mail message via the specified SMTP server.
            SmtpClient smtp = new SmtpClient();

            smtp.EnableSsl = Convert.ToBoolean(WebConfig.AppSettings.SMTPEnableSSL);
            ServicePointManager.ServerCertificateValidationCallback = delegate(object s, X509Certificate certificate, X509Chain chain, SslPolicyErrors sslPolicyErrors) { return true; };

            string smtpUsername = ConfigurationManager.AppSettings["SMTPCredentialsUsername"];
            if (!string.IsNullOrEmpty(smtpUsername))
            {
                string smtpPassword = ConfigurationManager.AppSettings["SMTPCredentialsPassword"];
                smtp.Credentials = new System.Net.NetworkCredential(smtpUsername, smtpPassword);
            }

            smtp.Send(mail);

            #region Log do Processo
            HistoricoLog historicoLog = new HistoricoLog();
            historicoLog.InsereHistoricoLog(DateTime.Now,
                                            DateTime.Now,
                                            "Envio de book de relatórios para " + emailsEnviados + " - Assunto (" + subject + ")",
                                            HttpContext.Current.User.Identity.Name,
                                            UtilitarioWeb.GetIP(Request),
                                            "",
                                            HistoricoLogOrigem.Outros);
            #endregion

            this.controleMensagensEnviadas++;

            // Salva lista de emails
            for (int k = 0; k < listaEmail.Count; k++)
            {
                if (!this.listaEmailsSenhaPDF.Contains(listaEmail[k]))
                {
                    this.listaEmailsSenhaPDF.Add(listaEmail[k]);
                }
            }
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="destino"></param>
    /// <param name="attach"></param>
    /// <param name="from"></param>
    /// <param name="subject"></param>
    /// <param name="body"></param>
    private void SendMailExtrato(string destino, Attachment attach, string from, string subject, string body)
    {
        Configuration config = WebConfigurationManager.OpenWebConfiguration(HttpContext.Current.Request.ApplicationPath);
        MailSettingsSectionGroup settings = (MailSettingsSectionGroup)config.GetSectionGroup("system.net/mailSettings");

        // Create a new message and attach the PDF report to it.
        MailMessage mail = new MailMessage();
        mail.Attachments.Add(attach);
        mail.To.Add(new MailAddress(destino));
        mail.From = new MailAddress(from);
        mail.Subject = subject;
        mail.Body = body;

        // Send the e-mail message via the specified SMTP server.
        SmtpClient smtp = new SmtpClient();

        /*if (from.Contains("@gmail"))
        {
            smtp.EnableSsl = true;
        }*/
        smtp.EnableSsl = Convert.ToBoolean(WebConfig.AppSettings.SMTPEnableSSL);

        ServicePointManager.ServerCertificateValidationCallback = delegate(object s, X509Certificate certificate, X509Chain chain, SslPolicyErrors sslPolicyErrors) { return true; };

        string smtpUsername = ConfigurationManager.AppSettings["SMTPCredentialsUsername"];
        if (!string.IsNullOrEmpty(smtpUsername))
        {
            string smtpPassword = ConfigurationManager.AppSettings["SMTPCredentialsPassword"];
            smtp.Credentials = new System.Net.NetworkCredential(smtpUsername, smtpPassword);
        }

        try
        {
            smtp.Send(mail);

            #region Log do Processo
            HistoricoLog historicoLog = new HistoricoLog();
            historicoLog.InsereHistoricoLog(DateTime.Now,
                                            DateTime.Now,
                                            "Envio de book de relatórios (extrato cotista) para " + destino + " - Assunto (" + subject + ")",
                                            HttpContext.Current.User.Identity.Name,
                                            UtilitarioWeb.GetIP(Request),
                                            "",
                                            HistoricoLogOrigem.Outros);
            #endregion
        }
        catch (Exception e)
        {
            LogEventViewer("Exceção (inner exception) >> " + e.InnerException.Message);
            LogEventViewer("Source >> " + e.Source);
            LogEventViewer("Stack Trace >> " + e.StackTrace);
        }

        this.controleMensagensEnviadas++;

        // Salva o email enviado        
        if (!this.listaEmailsSenhaPDF.Contains(destino))
        {
            this.listaEmailsSenhaPDF.Add(destino);
        }
    }

    #region Envia Email
    /// <summary>
    /// 
    /// </summary>
    /// <param name="idBook"></param>
    /// <param name="reports">Reports a serem Atachados</param>
    /// <param name="fileNames">Nomes dos Arquvos Atachados</param>
    private void EnviaEmailPorBook(int idBook, List<ReportMaster> reports, List<string> fileNames)
    {
        #region Pega as Informações de Email
        Book book = new Book();
        book.LoadByPrimaryKey(idBook);

        string listaEmail = !String.IsNullOrEmpty(book.Email) ? book.Email.ToString() : "";
        List<string> destinatarios = new List<string>();

        if (listaEmail != "")
        {
            listaEmail = listaEmail.Replace(",", ";");
            string[] emails = listaEmail.Split(new Char[] { ';' });
            foreach (string email in emails)
            {
                destinatarios.Add(email);
            }
        }
        string subject = !String.IsNullOrEmpty(book.Assunto) ? book.Assunto.ToString() : "";
        string body = !String.IsNullOrEmpty(book.Mensagem) ? book.Mensagem.ToString() : "";
        string from = book.EmailFrom;
        #endregion

        // Prepara os Atachments
        List<Attachment> attachments = this.MontaAttachments(reports, fileNames);

        this.SendMessage(destinatarios, attachments, from, subject, body);

        this.emailSenha = from;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="idCliente"></param>
    /// <param name="reports">Reports a serem Atachados</param>
    /// <param name="fileNames">Nomes dos Arquvos Atachados</param>
    private void EnviaEmailPorCliente(int idCliente, int idBook, List<ReportMaster> reports, List<string> fileNames)
    {
        #region Pega as informações de BookEmail
        Cliente cliente = new Cliente();
        cliente.LoadByPrimaryKey(idCliente);

        string listaEmail = !String.IsNullOrEmpty(cliente.BookEmail) ? cliente.BookEmail.ToString() : "";
        List<string> destinatarios = new List<string>();

        if (listaEmail != "")
        {
            listaEmail = listaEmail.Replace(",", ";");
            string[] emails = listaEmail.Split(new Char[] { ';' });
            foreach (string email in emails)
            {
                destinatarios.Add(email);
            }
        }

        string subject = !String.IsNullOrEmpty(cliente.BookAssunto) ? cliente.BookAssunto.ToString() : "";
        string body = !String.IsNullOrEmpty(cliente.BookMensagem) ? cliente.BookMensagem.ToString() : "";
        #endregion

        Book book = new Book();
        book.Query.Select(book.Query.EmailFrom);
        book.Query.Where(book.Query.IdBook.Equal(idBook));
        book.Query.Load();

        // Prepara os Atachments
        List<Attachment> attachments = this.MontaAttachments(reports, fileNames);

        this.SendMessage(destinatarios, attachments, book.EmailFrom, subject, body);

        this.emailSenha = book.EmailFrom;
    }

    /// <summary>
    /// Envia Report ExtratoCotista por Email
    /// </summary>
    private string EnviaEmailExtratoCotista()
    {
        string mensagemExtrato = "";

        for (int i = 0; i < this.dataHolderEmailCotista.Count; i++)
        {
            ReportExtratoCotista r = this.dataHolderEmailCotista[i].report;
            // Coloca a senha PDF
            r.ExportOptions.Pdf.PasswordSecurityOptions.OpenPassword = this.pdfPassword;
            //
            string arquivo = this.dataHolderEmailCotista[i].nomeArquivoPdf;
            //
            List<Attachment> attach = new List<Attachment>();
            //            
            MemoryStream ms = new MemoryStream();
            r.ExportToPdf(ms);
            ms.Seek(0, SeekOrigin.Begin);
            //
            Attachment att = new Attachment(ms, arquivo, "application/pdf");
            //
            attach.Add(att);

            if (!String.IsNullOrEmpty(this.dataHolderEmailCotista[i].emailDestino) &&
                !String.IsNullOrEmpty(this.dataHolderEmailCotista[i].emailOrigem))
            {
                try
                {
                    this.SendMailExtrato(this.dataHolderEmailCotista[i].emailDestino,
                                     att,
                                     this.dataHolderEmailCotista[i].emailOrigem,
                                     this.dataHolderEmailCotista[i].assunto,
                                     this.dataHolderEmailCotista[i].mensagem);
                }
                catch (Exception e)
                {
                    mensagemExtrato = mensagemExtrato + "Cotista " + this.dataHolderEmailCotista[i].idCotista.Value.ToString() + " - " + this.dataHolderEmailCotista[i].emailDestino + "\n";
                }
            }
        }

        if (mensagemExtrato != "")
        {
            mensagemExtrato = " Alguns problemas ocorreram no envio do extrato para os emails " + mensagemExtrato;
        }

        this.emailSenha = this.dataHolderEmailCotista.Count > 0 ? this.dataHolderEmailCotista[0].emailOrigem : "";

        return mensagemExtrato;
    }
    #endregion

    /// <summary>
    /// De acordo com o tipo de envio Monta arquivos PDFs
    /// 
    /// Se tipoEnvio = pdf, Monta um Único PDF com todos os relatórios pertencentes a um determinado 
    /// book de Relatório
    /// 
    /// Se tipoEnvio = Email, baseado na configuração EnvioEmailPDFUnico presente no WebConfig
    /// Monta um Único PDF com todos os Relatórios Pertencentes a um determinado book de relatório
    /// ou Monta vários PDFS.
    /// </summary>
    /// <param name="tipoEnvio">Envio por Email ou por PDF</param>
    private void MontaReport(string tipoEnvio)
    {
        // Limpa os Dados
        this.dataHolderEmailCotista.Clear();
        //

        #region Monta PDFS para Envio de Email
        bool envioEmailPDFUnico = true;
        if (!String.IsNullOrEmpty(WebConfig.AppSettings.EnvioEmailPDFUnico))
        {
            envioEmailPDFUnico = Convert.ToBoolean(WebConfig.AppSettings.EnvioEmailPDFUnico);
        }

        string fileName = "";
        this.mensagem = "";
        //
        DateTime data = Convert.ToDateTime(textData.Text);
        DateTime dataInicioMes = Calendario.RetornaPrimeiroDiaUtilMes(data, 0, LocalFeriadoFixo.Brasil, TipoFeriado.Brasil);
        string login = HttpContext.Current.User.Identity.Name; //Usado apenas no relatorio de Saldos de Cotistas
        //
        ReportMaster reportMaster = new ReportMaster();
        reportMaster.PrintingSystem.ContinuousPageNumbering = false;

        // Tratamento Para Cada Cliente Do Book = Numero de Emails a serem Enviados
        int numeroRelatoriosClienteGerados = 0;
        BookClienteCollection bookClienteCollection = this.MontaCollectionBookCliente();

        foreach (BookCliente bookCliente in bookClienteCollection)
        {
            // Conjunto de Reports de um Cliente
            List<ReportMaster> reportsCliente = new List<ReportMaster>();
            // Nomes dos Reports de um Cliente
            List<string> nomesReports = new List<string>();

            #region Informações do Cliente
            int idCliente = bookCliente.IdCliente.Value;
            Cliente cliente = new Cliente();
            List<esQueryItem> campos = new List<esQueryItem>();
            campos.Add(cliente.Query.IdCliente);
            campos.Add(cliente.Query.DataDia);
            campos.Add(cliente.Query.DataImplantacao);
            campos.Add(cliente.Query.ZeraCaixa);
            cliente.LoadByPrimaryKey(campos, idCliente);
            #endregion

            #region Tratamento Para Cada Cliente Do Book
            if (cliente.DataDia.Value < data)
            {
                this.mensagem += "Cliente " + idCliente.ToString() + " com data Dia: " + cliente.DataDia.Value.ToString("d") + " anterior a data do relatório \n";
                this.mensagem += "Não pôde ser incluído nos relatórios! \n\n";
            }
            else if (cliente.DataImplantacao.Value > data)
            {
                this.mensagem += "Cliente " + idCliente.ToString() + " com data de implantação: " + cliente.DataImplantacao.Value.ToString("d") + "superior a data do relatório. \n";
                this.mensagem += "Não pôde ser incluído nos relatórios! \n\n";
            }
            else
            {
                int idBook = bookCliente.IdBook.Value;

                BookRelatorioCollection bookRelatorioCollection = new BookRelatorioCollection();
                bookRelatorioCollection.Query.Where(bookRelatorioCollection.Query.IdBook.Equal(idBook));
                bookRelatorioCollection.Query.OrderBy(bookRelatorioCollection.Query.IdRelatorio.Ascending);
                bookRelatorioCollection.Query.Load();

                foreach (BookRelatorio bookRelatorio in bookRelatorioCollection)
                {
                    #region Tratamento Para Cada Relatorio pertencente ao Book
                    string relatorio = "";
                    int idRelatorio = bookRelatorio.IdRelatorio.Value;
                    try
                    {
                        Carteira carteira = new Carteira();
                        DateTime dataInicio = new DateTime();
                        switch (idRelatorio)
                        {
                            case (int)Relatorios.ComposicaoCarteira:
                                #region ComposicaoCarteira
                                numeroRelatoriosClienteGerados++;
                                relatorio = "Composição da Carteira";
                                ReportComposicaoCarteira report1 = new ReportComposicaoCarteira(idCliente, data, ReportComposicaoCarteira.TipoRelatorio.Fechamento, ReportComposicaoCarteira.TipoLingua.Portugues, null, true);
                                report1.CreateDocument();
                                reportMaster.PrintingSystem.Pages.AddRange(report1.PrintingSystem.Pages);

                                if (tipoEnvio == TipoEnvioRelatorio.Email && !envioEmailPDFUnico)
                                {
                                    fileName = "ComposicaoCarteira_" + idCliente.ToString() + "_" + data.ToString().Substring(0, 10).Replace("/", "-");

                                    // Salva o Relatorio a Ser Atachado numa Lista
                                    reportsCliente.Add(reportMaster);
                                    nomesReports.Add(fileName);
                                    //                                    
                                    reportMaster = new ReportMaster(); //Zera o reportMaster em caso de envio de email
                                }
                                break;
                                #endregion
                            case (int)Relatorios.ExtratoClienteMensal:
                                #region ExtratoClienteMensal
                                carteira.LoadByPrimaryKey(idCliente);

                                dataInicio = Calendario.RetornaPrimeiroDiaUtilMes(data, 0, (int)LocalFeriadoFixo.Brasil, TipoFeriado.Brasil);
                                if (dataInicio < carteira.DataInicioCota.Value)
                                {
                                    dataInicio = carteira.DataInicioCota.Value;
                                }

                                numeroRelatoriosClienteGerados++;
                                relatorio = "Extrato de Cliente";
                                bool mostraCaixa = cliente.ZeraCaixa.Trim().ToUpper() == "S" ? false : true;
                                bool explodeFundo = carteira.ExplodeCotasDeFundos.ToUpper() == "S" ? true : false;

                                ReportExtratoCliente report25 = new ReportExtratoCliente(idCliente, carteira.IdIndiceBenchmark.Value, dataInicio, data, mostraCaixa, explodeFundo);
                                report25.CreateDocument();
                                reportMaster.PrintingSystem.Pages.AddRange(report25.PrintingSystem.Pages);

                                if (tipoEnvio == TipoEnvioRelatorio.Email && !envioEmailPDFUnico)
                                {
                                    fileName = "Extrato_" + idCliente.ToString() + "_" + data.ToString().Substring(0, 10).Replace("/", "-");

                                    // Salva o Relatorio a Ser Atachado numa Lista
                                    reportsCliente.Add(reportMaster);
                                    nomesReports.Add(fileName);
                                    //                                    
                                    reportMaster = new ReportMaster(); //Zera o reportMaster em caso de envio de email
                                }
                                break;
                                #endregion
                            case (int)Relatorios.ExtratoClienteMensalSemCC:
                                #region ExtratoClienteMensal sem C/C
                                carteira.LoadByPrimaryKey(idCliente);

                                dataInicio = Calendario.RetornaPrimeiroDiaUtilMes(data, 0, (int)LocalFeriadoFixo.Brasil, TipoFeriado.Brasil);
                                if (dataInicio < carteira.DataInicioCota.Value)
                                {
                                    dataInicio = carteira.DataInicioCota.Value;
                                }

                                numeroRelatoriosClienteGerados++;
                                relatorio = "Extrato de Cliente";
                                ReportExtratoCliente report225 = new ReportExtratoCliente(idCliente, carteira.IdIndiceBenchmark.Value, dataInicio, data, false, false);
                                report225.CreateDocument();
                                reportMaster.PrintingSystem.Pages.AddRange(report225.PrintingSystem.Pages);

                                if (tipoEnvio == TipoEnvioRelatorio.Email && !envioEmailPDFUnico)
                                {
                                    fileName = "Extrato_" + idCliente.ToString() + "_" + data.ToString().Substring(0, 10).Replace("/", "-");

                                    // Salva o Relatorio a Ser Atachado numa Lista
                                    reportsCliente.Add(reportMaster);
                                    nomesReports.Add(fileName);
                                    //                                    
                                    reportMaster = new ReportMaster(); //Zera o reportMaster em caso de envio de email
                                }
                                break;
                                #endregion
                            case (int)Relatorios.ExtratoContaCorrente:
                                #region ExtratoContaCorrente
                                numeroRelatoriosClienteGerados++;
                                relatorio = "Extrato de C/C";
                                ReportExtratoContaCorrente report10 = new ReportExtratoContaCorrente(idCliente, dataInicioMes, data, null, null);
                                report10.CreateDocument();
                                reportMaster.PrintingSystem.Pages.AddRange(report10.PrintingSystem.Pages);

                                if (tipoEnvio == TipoEnvioRelatorio.Email && !envioEmailPDFUnico)
                                {
                                    fileName = "ExtratoCC_" + idCliente.ToString() + "_" + data.ToString().Substring(0, 10).Replace("/", "-");

                                    // Salva o Relatorio a Ser Atachado numa Lista
                                    reportsCliente.Add(reportMaster);
                                    nomesReports.Add(fileName);
                                    //
                                    reportMaster = new ReportMaster(); //Zera o reportMaster em caso de envio de email
                                }
                                break;
                                #endregion
                            case (int)Relatorios.ApuracaoGanhhoRV:
                                #region ApuracaoGanhhoRV
                                numeroRelatoriosClienteGerados++;
                                relatorio = "ApuracaoGanhoRV";
                                ReportGanhoRendaVariavel report60 = new ReportGanhoRendaVariavel(idCliente, 4, data.Year);
                                report60.CreateDocument();
                                reportMaster.PrintingSystem.Pages.AddRange(report60.PrintingSystem.Pages);

                                report60 = new ReportGanhoRendaVariavel(idCliente, 8, data.Year);
                                report60.CreateDocument();
                                reportMaster.PrintingSystem.Pages.AddRange(report60.PrintingSystem.Pages);

                                report60 = new ReportGanhoRendaVariavel(idCliente, 12, data.Year);
                                report60.CreateDocument();
                                reportMaster.PrintingSystem.Pages.AddRange(report60.PrintingSystem.Pages);

                                if (tipoEnvio == TipoEnvioRelatorio.Email && !envioEmailPDFUnico)
                                {
                                    fileName = "ApuracaoGanhoRV_" + idCliente.ToString() + "_" + data.ToString().Substring(0, 10).Replace("/", "-");

                                    // Salva o Relatorio a Ser Atachado numa Lista
                                    reportsCliente.Add(reportMaster);
                                    nomesReports.Add(fileName);
                                    //
                                    reportMaster = new ReportMaster(); //Zera o reportMaster em caso de envio de email
                                }
                                break;
                                #endregion
                            case (int)Relatorios.FluxoCaixaSintetico:
                                #region FluxoCaixaSintetico
                                numeroRelatoriosClienteGerados++;
                                relatorio = "Fluxo de Caixa Sintético";

                                Carteira.BooleanosFluxoCaixa booleanosFluxoCaixa = new Carteira.BooleanosFluxoCaixa();
                                booleanosFluxoCaixa.PosicaoBolsa = false;
                                booleanosFluxoCaixa.PosicaoFundos = false;
                                booleanosFluxoCaixa.RendaFixa = true;
                                booleanosFluxoCaixa.ResgatesNaoConvertidos = false;

                                ReportFluxoCaixaSintetico report16 = new ReportFluxoCaixaSintetico(idCliente, data, booleanosFluxoCaixa);
                                report16.CreateDocument();
                                reportMaster.PrintingSystem.Pages.AddRange(report16.PrintingSystem.Pages);

                                if (tipoEnvio == TipoEnvioRelatorio.Email && !envioEmailPDFUnico)
                                {
                                    fileName = "CaixaSintetico_" + idCliente.ToString() + "_" + data.ToString().Substring(0, 10).Replace("/", "-");

                                    // Salva o Relatorio a Ser Atachado numa Lista
                                    reportsCliente.Add(reportMaster);
                                    nomesReports.Add(fileName);
                                    //
                                    reportMaster = new ReportMaster(); //Zera o reportMaster em caso de envio de email
                                }
                                break;
                                #endregion
                            case (int)Relatorios.InformeConsolidadoCarteiras:
                                #region InformeConsolidadoCarteiras
                                numeroRelatoriosClienteGerados++;
                                relatorio = "Informe Consolidado de Carteiras";
                                ReportDemonstrativoCarteiras report30 = new ReportDemonstrativoCarteiras(data, Context.User.Identity.Name);
                                report30.CreateDocument();
                                reportMaster.PrintingSystem.Pages.AddRange(report30.PrintingSystem.Pages);

                                if (tipoEnvio == TipoEnvioRelatorio.Email && !envioEmailPDFUnico)
                                {
                                    fileName = "InformeCarteiras_" + data.ToString().Substring(0, 10).Replace("/", "-");

                                    // Salva o Relatorio a Ser Atachado numa Lista
                                    reportsCliente.Add(reportMaster);
                                    nomesReports.Add(fileName);
                                    //
                                    reportMaster = new ReportMaster(); //Zera o reportMaster em caso de envio de email
                                }
                                break;
                                #endregion
                            case (int)Relatorios.MapaResultado:
                                #region MapaResultado
                                numeroRelatoriosClienteGerados++;
                                relatorio = "Mapa de Resultado";
                                ReportMapaResultado report50 = new ReportMapaResultado(idCliente, dataInicioMes, data);
                                report50.CreateDocument();
                                reportMaster.PrintingSystem.Pages.AddRange(report50.PrintingSystem.Pages);

                                if (tipoEnvio == TipoEnvioRelatorio.Email && !envioEmailPDFUnico)
                                {
                                    fileName = "MapaResultado_" + idCliente.ToString();
                                    // Salva o Relatorio a Ser Atachado numa Lista
                                    reportsCliente.Add(reportMaster);
                                    nomesReports.Add(fileName);
                                    //
                                    reportMaster = new ReportMaster(); //Zera o reportMaster em caso de envio de email
                                }
                                break;
                                #endregion
                            case (int)Relatorios.DARF:
                                #region DARF
                                numeroRelatoriosClienteGerados++;
                                relatorio = "DARF";
                                ReportDarf report65 = new ReportDarf(idCliente, data.Month, data.Year);
                                report65.CreateDocument();
                                reportMaster.PrintingSystem.Pages.AddRange(report65.PrintingSystem.Pages);

                                if (tipoEnvio == TipoEnvioRelatorio.Email && !envioEmailPDFUnico)
                                {
                                    fileName = "DARF_" + idCliente.ToString();
                                    // Salva o Relatorio a Ser Atachado numa Lista
                                    reportsCliente.Add(reportMaster);
                                    nomesReports.Add(fileName);
                                    //
                                    reportMaster = new ReportMaster(); //Zera o reportMaster em caso de envio de email
                                }
                                break;
                                #endregion
                            case (int)Relatorios.ResultadoEnquadramentoConsolidadoDesenquadrado:
                                #region ResultadoEnquadramentoConsolidadoDesenquadrado
                                numeroRelatoriosClienteGerados++;
                                relatorio = "Enquadramento Consolidado (Desenquadrados)";
                                ReportResultadoConsolidado report70 = new ReportResultadoConsolidado(data, "N");
                                report70.CreateDocument();
                                reportMaster.PrintingSystem.Pages.AddRange(report70.PrintingSystem.Pages);

                                if (tipoEnvio == TipoEnvioRelatorio.Email && !envioEmailPDFUnico)
                                {
                                    fileName = "Enquadra_" + data.ToString().Substring(0, 10).Replace("/", "-");
                                    // Salva o Relatorio a Ser Atachado numa Lista
                                    reportsCliente.Add(reportMaster);
                                    nomesReports.Add(fileName);
                                    //
                                    reportMaster = new ReportMaster(); //Zera o reportMaster em caso de envio de email
                                }
                                break;
                                #endregion
                            case (int)Relatorios.ResultadoEnquadramentoConsolidado:
                                #region ResultadoEnquadramentoConsolidado
                                numeroRelatoriosClienteGerados++;
                                relatorio = "Enquadramento Consolidado (Geral)";
                                ReportResultadoConsolidado report71 = new ReportResultadoConsolidado(data, "");
                                report71.CreateDocument();
                                reportMaster.PrintingSystem.Pages.AddRange(report71.PrintingSystem.Pages);

                                if (tipoEnvio == TipoEnvioRelatorio.Email && !envioEmailPDFUnico)
                                {
                                    fileName = "Enquadra_" + data.ToString().Substring(0, 10).Replace("/", "-");
                                    // Salva o Relatorio a Ser Atachado numa Lista
                                    reportsCliente.Add(reportMaster);
                                    nomesReports.Add(fileName);
                                    //
                                    reportMaster = new ReportMaster(); //Zera o reportMaster em caso de envio de email
                                }
                                break;
                                #endregion
                            case (int)Relatorios.ResultadoEnquadramentoIndividual:
                                #region ResultadoEnquadramentoIndividual
                                numeroRelatoriosClienteGerados++;
                                relatorio = "Enquadramento";
                                ReportResultadoConsolidado report72 = new ReportResultadoConsolidado(idCliente, data, "");
                                report72.CreateDocument();
                                reportMaster.PrintingSystem.Pages.AddRange(report72.PrintingSystem.Pages);

                                if (tipoEnvio == TipoEnvioRelatorio.Email && !envioEmailPDFUnico)
                                {
                                    fileName = "Enquadra_" + data.ToString().Substring(0, 10).Replace("/", "-");
                                    // Salva o Relatorio a Ser Atachado numa Lista
                                    reportsCliente.Add(reportMaster);
                                    nomesReports.Add(fileName);
                                    //
                                    reportMaster = new ReportMaster(); //Zera o reportMaster em caso de envio de email
                                }
                                break;
                                #endregion
                            case (int)Relatorios.NotasAplicacaoResgate:
                                #region NotasAplicacaoResgate
                                relatorio = "Notas de Aplic/Resgate";
                                //
                                OperacaoCotistaCollection operacaoCotistaCollection = new OperacaoCotistaCollection();
                                operacaoCotistaCollection.Query.Where(operacaoCotistaCollection.Query.DataConversao.Equal(data),
                                                                      operacaoCotistaCollection.Query.IdCarteira.Equal(idCliente),
                                                                      operacaoCotistaCollection.Query.TipoOperacao.In((byte)TipoOperacaoCotista.Aplicacao,
                                                                                                                      (byte)TipoOperacaoCotista.AplicacaoAcoesEspecial,
                                                                                                                      (byte)TipoOperacaoCotista.IncorporacaoAplicacao,
                                                                                                                      (byte)TipoOperacaoCotista.IncorporacaoResgate,
                                                                                                                      (byte)TipoOperacaoCotista.ResgateBruto,
                                                                                                                      (byte)TipoOperacaoCotista.ResgateCotas,
                                                                                                                      (byte)TipoOperacaoCotista.ResgateLiquido,
                                                                                                                      (byte)TipoOperacaoCotista.ResgateTotal));
                                operacaoCotistaCollection.Query.Load();

                                // Se tem Dados Relatório será Gerado
                                if (operacaoCotistaCollection.HasData)
                                {
                                    numeroRelatoriosClienteGerados++;
                                }

                                foreach (OperacaoCotista operacaoCotista in operacaoCotistaCollection)
                                {
                                    int idOperacao = operacaoCotista.IdOperacao.Value;
                                    if (operacaoCotista.TipoOperacao == (byte)TipoOperacaoCotista.Aplicacao ||
                                        operacaoCotista.TipoOperacao == (byte)TipoOperacaoCotista.AplicacaoAcoesEspecial)
                                    {
                                        ReportNotaAplicacao report85 = new ReportNotaAplicacao(idOperacao, TipoLingua.Portugues);
                                        report85.CreateDocument();
                                        reportMaster.PrintingSystem.Pages.AddRange(report85.PrintingSystem.Pages);
                                    }
                                    else
                                    {
                                        ReportNotaResgate report86 = new ReportNotaResgate(idOperacao, TipoLingua.Portugues);
                                        report86.CreateDocument();
                                        reportMaster.PrintingSystem.Pages.AddRange(report86.PrintingSystem.Pages);
                                    }
                                }

                                if (tipoEnvio == TipoEnvioRelatorio.Email && !envioEmailPDFUnico)
                                {

                                    // Se não tem OperacaoCotista para aquele Cliente naquela data 
                                    // não adiciona relatorio de Nota de Aplicação
                                    if (operacaoCotistaCollection.HasData)
                                    {

                                        fileName = "NotasAplicResg_" + idCliente.ToString() + "_" + data.ToString().Substring(0, 10).Replace("/", "-");
                                        // Salva o Relatorio a Ser Atachado numa Lista
                                        reportsCliente.Add(reportMaster);
                                        nomesReports.Add(fileName);
                                        //
                                        reportMaster = new ReportMaster(); //Zera o reportMaster em caso de envio de email
                                    }
                                }
                                break;
                                #endregion
                            case (int)Relatorios.ExtratoCotista:
                                #region ExtratoCotista

                                relatorio = "Movimento Extrato Cotista";

                                #region Para 1 Carteira - Acrescenta varios Cotistas
                                //Datas para a Posicao               
                                DateTime dataInicioPosicao = Calendario.RetornaPrimeiroDiaUtilMes(data, 0, (int)LocalFeriadoFixo.Brasil, TipoFeriado.Brasil);

                                DateTime dataFimPosicao = data;

                                //Datas para a Operacao
                                DateTime dataInicioOperacao = dataInicioPosicao;
                                DateTime dataFimOperacao = dataFimPosicao;

                                List<int> listaIdCotista = new CotistaCollection().BuscaCotistasDaCarteira(idCliente, dataInicioPosicao, dataFimPosicao,
                                                                    dataInicioOperacao, dataFimOperacao, HttpContext.Current.User.Identity.Name);
                                //

                                if (listaIdCotista.Count > 0)
                                {
                                    numeroRelatoriosClienteGerados += 1;
                                }

                                if (tipoEnvio == TipoEnvioRelatorio.PDF)
                                {
                                    for (int i = 0; i < listaIdCotista.Count; i++)
                                    {
                                        ReportExtratoCotista report104 = new ReportExtratoCotista(idCliente, listaIdCotista[i], dataInicioOperacao, data);
                                        // Cria o relatorio para 1 cotista
                                        report104.CreateDocument();
                                        // Anexa o Conteudo do Relatorio no Relatorio Master
                                        reportMaster.Pages.AddRange(report104.PrintingSystem.Pages);
                                    }
                                }
                                /* 1 email para cada cotista da carteira */
                                else if (tipoEnvio == TipoEnvioRelatorio.Email)
                                {
                                    for (int i = 0; i < listaIdCotista.Count; i++)
                                    {
                                        PessoaEmailCollection pessoaEmailCollection = new PessoaEmailCollection();
                                        //
                                        pessoaEmailCollection.Query
                                                        .Select(pessoaEmailCollection.Query.Email)
                                                        .Where(pessoaEmailCollection.Query.IdPessoa == listaIdCotista[i])
                                                        .OrderBy(pessoaEmailCollection.Query.Email.Ascending);
                                        pessoaEmailCollection.Query.Load();

                                        if (pessoaEmailCollection.HasData && !String.IsNullOrEmpty(pessoaEmailCollection[0].Email))
                                        {
                                            ReportExtratoCotista reportMasterExtratoCotista = new ReportExtratoCotista(idCliente, listaIdCotista[i], dataInicioOperacao, data);
                                            // Cria o relatorio para 1 cotista
                                            reportMasterExtratoCotista.CreateDocument();

                                            #region Pega as Informações de Email
                                            Book book = new Book();
                                            book.LoadByPrimaryKey(idBook);

                                            string subject = !String.IsNullOrEmpty(book.Assunto) ? book.Assunto.ToString() : "";
                                            string body = !String.IsNullOrEmpty(book.Mensagem) ? book.Mensagem.ToString() : "";
                                            string from = !String.IsNullOrEmpty(book.EmailFrom) ? book.EmailFrom.ToString() : "";
                                            #endregion

                                            // Salva Informações para posterior envio
                                            this.SalvaInfoExtratoCotista(listaIdCotista[i], idCliente, reportMasterExtratoCotista, subject, body, from, idBook);
                                        }
                                    }
                                }
                                #endregion

                                break;
                                #endregion
                            case (int)Relatorios.ExtratoCotistaComCarteira:
                                #region ExtratoCotistaComCarteira

                                relatorio = "Movimento Extrato Cotista com Carteira";

                                #region Para 1 Carteira - Acrescenta varios Cotistas
                                //Datas para a Posicao               
                                DateTime dataInicioPosicao1 = Calendario.RetornaPrimeiroDiaUtilMes(data, 0, (int)LocalFeriadoFixo.Brasil, TipoFeriado.Brasil);

                                DateTime dataFimPosicao1 = data;

                                //Datas para a Operacao
                                DateTime dataInicioOperacao1 = dataInicioPosicao1;
                                DateTime dataFimOperacao1 = dataFimPosicao1;

                                List<int> listaIdCotista1 = new CotistaCollection().BuscaCotistasDaCarteira(idCliente, dataInicioPosicao1, dataFimPosicao1,
                                                                    dataInicioOperacao1, dataFimOperacao1, HttpContext.Current.User.Identity.Name);
                                //

                                if (listaIdCotista1.Count > 0)
                                {
                                    numeroRelatoriosClienteGerados += 1;
                                }

                                if (tipoEnvio == TipoEnvioRelatorio.PDF)
                                {
                                    for (int i = 0; i < listaIdCotista1.Count; i++)
                                    {
                                        ReportExtratoCotista report105 = new ReportExtratoCotista(idCliente, listaIdCotista1[i], dataInicioOperacao1, data, true);
                                        // Cria o relatorio para 1 cotista
                                        report105.CreateDocument();
                                        // Anexa o Conteudo do Relatorio no Relatorio Master
                                        reportMaster.Pages.AddRange(report105.PrintingSystem.Pages);
                                    }
                                }
                                /* 1 email para cada cotista da carteira */
                                else if (tipoEnvio == TipoEnvioRelatorio.Email)
                                {
                                    for (int i = 0; i < listaIdCotista1.Count; i++)
                                    {

                                        PessoaEmailCollection pessoaEmailCollection = new PessoaEmailCollection();
                                        //
                                        pessoaEmailCollection.Query
                                                        .Select(pessoaEmailCollection.Query.Email)
                                                        .Where(pessoaEmailCollection.Query.IdPessoa == listaIdCotista1[i])
                                                        .OrderBy(pessoaEmailCollection.Query.Email.Ascending);
                                        pessoaEmailCollection.Query.Load();

                                        if (pessoaEmailCollection.HasData && !String.IsNullOrEmpty(pessoaEmailCollection[0].Email))
                                        {
                                            ReportExtratoCotista reportMasterExtratoCotista = new ReportExtratoCotista(idCliente, listaIdCotista1[i], dataInicioOperacao1, data, true);
                                            // Cria o relatorio para 1 cotista
                                            reportMasterExtratoCotista.CreateDocument();

                                            #region Pega as Informações de Email
                                            Book book = new Book();
                                            book.LoadByPrimaryKey(idBook);

                                            string subject = !String.IsNullOrEmpty(book.Assunto) ? book.Assunto.ToString() : "";
                                            string body = !String.IsNullOrEmpty(book.Mensagem) ? book.Mensagem.ToString() : "";
                                            string from = !String.IsNullOrEmpty(book.EmailFrom) ? book.EmailFrom.ToString() : "";
                                            #endregion

                                            // Salva Informações para posterior envio
                                            this.SalvaInfoExtratoCotista(listaIdCotista1[i], idCliente, reportMasterExtratoCotista, subject, body, from, idBook);
                                        }
                                    }
                                }
                                #endregion

                                break;
                                #endregion
                            case (int)Relatorios.MovimentoCotista:
                                #region MovimentoCotista
                                numeroRelatoriosClienteGerados++;
                                relatorio = "Movimentação de Cotista";
                                ReportMovimentacaoCotista report90 = new ReportMovimentacaoCotista(null, idCliente, dataInicioMes, data, null, null, null, null, null,
                                                                                                   ReportMovimentacaoCotista.TipoOpcoesCampo.Rendimento);
                                report90.CreateDocument();
                                reportMaster.PrintingSystem.Pages.AddRange(report90.PrintingSystem.Pages);

                                if (tipoEnvio == TipoEnvioRelatorio.Email && !envioEmailPDFUnico)
                                {
                                    fileName = "MovimentoCotista_" + idCliente.ToString();
                                    // Salva o Relatorio a Ser Atachado numa Lista
                                    reportsCliente.Add(reportMaster);
                                    nomesReports.Add(fileName);
                                    //
                                    reportMaster = new ReportMaster(); //Zera o reportMaster em caso de envio de email
                                }
                                break;
                                #endregion
                            case (int)Relatorios.SaldoCotistaAnaliticoCodigo:
                                #region SaldoCotistaAnaliticoCodigo
                                numeroRelatoriosClienteGerados++;
                                relatorio = "Saldos de Aplicação de Cotista";
                                ReportSaldoAplicacaoCotista report100 = new ReportSaldoAplicacaoCotista(idCliente, null, data,
                                                            ReportSaldoAplicacaoCotista.TipoRelatorio.Analitico,
                                                            ReportSaldoAplicacaoCotista.TipoGrupamento.PorCodigo, ReportSaldoAplicacaoCotista.OpcaoValorAplicado.ValorAtualizado,
                                                            login, ReportSaldoAplicacaoCotista.TipoLingua.Portugues);
                                report100.CreateDocument();
                                reportMaster.PrintingSystem.Pages.AddRange(report100.PrintingSystem.Pages);

                                if (tipoEnvio == TipoEnvioRelatorio.Email && !envioEmailPDFUnico)
                                {
                                    fileName = "SaldosCotistas_" + idCliente.ToString() + "_" + data.ToString().Substring(0, 10).Replace("/", "-");
                                    // Salva o Relatorio a Ser Atachado numa Lista
                                    reportsCliente.Add(reportMaster);
                                    nomesReports.Add(fileName);
                                    //
                                    reportMaster = new ReportMaster(); //Zera o reportMaster em caso de envio de email
                                }
                                break;
                                #endregion
                            case (int)Relatorios.SaldoCotistaAnaliticoNome:
                                #region SaldoCotistaAnaliticoNome
                                numeroRelatoriosClienteGerados++;
                                relatorio = "Saldos de Aplicação de Cotista";
                                ReportSaldoAplicacaoCotista report101 = new ReportSaldoAplicacaoCotista(idCliente, null, data,
                                                            ReportSaldoAplicacaoCotista.TipoRelatorio.Analitico,
                                                            ReportSaldoAplicacaoCotista.TipoGrupamento.PorNome,
                                                            ReportSaldoAplicacaoCotista.OpcaoValorAplicado.ValorAtualizado,
                                                            login, ReportSaldoAplicacaoCotista.TipoLingua.Portugues);
                                report101.CreateDocument();
                                reportMaster.PrintingSystem.Pages.AddRange(report101.PrintingSystem.Pages);

                                if (tipoEnvio == TipoEnvioRelatorio.Email && !envioEmailPDFUnico)
                                {
                                    fileName = "SaldosCotistas_" + idCliente.ToString() + "_" + data.ToString().Substring(0, 10).Replace("/", "-");
                                    // Salva o Relatorio a Ser Atachado numa Lista
                                    reportsCliente.Add(reportMaster);
                                    nomesReports.Add(fileName);
                                    //
                                    reportMaster = new ReportMaster(); //Zera o reportMaster em caso de envio de email
                                }
                                break;
                                #endregion
                            case (int)Relatorios.SaldoCotistaConsolidadoCodigo:
                                #region SaldoCotistaConsolidadoCodigo
                                numeroRelatoriosClienteGerados++;
                                relatorio = "Saldos de Aplicação de Cotista";
                                ReportSaldoAplicacaoCotista report102 = new ReportSaldoAplicacaoCotista(idCliente, null, data,
                                                            ReportSaldoAplicacaoCotista.TipoRelatorio.Consolidado,
                                                            ReportSaldoAplicacaoCotista.TipoGrupamento.PorCodigo,
                                                            ReportSaldoAplicacaoCotista.OpcaoValorAplicado.ValorAtualizado,
                                                            login, ReportSaldoAplicacaoCotista.TipoLingua.Portugues);
                                report102.CreateDocument();
                                reportMaster.PrintingSystem.Pages.AddRange(report102.PrintingSystem.Pages);

                                if (tipoEnvio == TipoEnvioRelatorio.Email && !envioEmailPDFUnico)
                                {
                                    fileName = "SaldosCotistas_" + idCliente.ToString() + "_" + data.ToString().Substring(0, 10).Replace("/", "-");
                                    // Salva o Relatorio a Ser Atachado numa Lista
                                    reportsCliente.Add(reportMaster);
                                    nomesReports.Add(fileName);
                                    //
                                    reportMaster = new ReportMaster(); //Zera o reportMaster em caso de envio de email
                                }
                                break;
                                #endregion
                            case (int)Relatorios.SaldoCotistaConsolidadoNome:
                                #region SaldoCotistaConsolidadoCodigo
                                numeroRelatoriosClienteGerados++;
                                relatorio = "Saldos de Aplicação de Cotista";
                                ReportSaldoAplicacaoCotista report103 = new ReportSaldoAplicacaoCotista(idCliente, null, data,
                                                            ReportSaldoAplicacaoCotista.TipoRelatorio.Consolidado,
                                                            ReportSaldoAplicacaoCotista.TipoGrupamento.PorNome,
                                                            ReportSaldoAplicacaoCotista.OpcaoValorAplicado.ValorAtualizado,
                                                            login, ReportSaldoAplicacaoCotista.TipoLingua.Portugues);
                                report103.CreateDocument();
                                reportMaster.PrintingSystem.Pages.AddRange(report103.PrintingSystem.Pages);

                                if (tipoEnvio == TipoEnvioRelatorio.Email && !envioEmailPDFUnico)
                                {
                                    fileName = "SaldosCotistas_" + idCliente.ToString() + "_" + data.ToString().Substring(0, 10).Replace("/", "-");
                                    // Salva o Relatorio a Ser Atachado numa Lista
                                    reportsCliente.Add(reportMaster);
                                    nomesReports.Add(fileName);
                                    //
                                    reportMaster = new ReportMaster(); //Zera o reportMaster em caso de envio de email
                                }
                                break;
                                #endregion
                            case (int)Relatorios.OperacoesBolsa:
                                #region OperacoesBolsa
                                numeroRelatoriosClienteGerados++;
                                relatorio = "Operações de Bolsa";
                                ReportMapaOperacaoBolsa report300 = new ReportMapaOperacaoBolsa(dataInicioMes, data, idCliente);
                                report300.CreateDocument();
                                reportMaster.PrintingSystem.Pages.AddRange(report300.PrintingSystem.Pages);

                                if (tipoEnvio == TipoEnvioRelatorio.Email && !envioEmailPDFUnico)
                                {
                                    fileName = "OperacoesBolsa_" + idCliente.ToString();
                                    // Salva o Relatorio a Ser Atachado numa Lista
                                    reportsCliente.Add(reportMaster);
                                    nomesReports.Add(fileName);
                                    //
                                    reportMaster = new ReportMaster(); //Zera o reportMaster em caso de envio de email
                                }
                                break;
                                #endregion
                            case (int)Relatorios.OperacoesBMF:
                                #region OperacoesBMF
                                numeroRelatoriosClienteGerados++;
                                relatorio = "Operações de BMF";
                                ReportMapaOperacaoBMF report310 = new ReportMapaOperacaoBMF(dataInicioMes, data, idCliente);
                                report310.CreateDocument();
                                reportMaster.PrintingSystem.Pages.AddRange(report310.PrintingSystem.Pages);

                                if (tipoEnvio == TipoEnvioRelatorio.Email && !envioEmailPDFUnico)
                                {
                                    fileName = "OperacoesBMF_" + idCliente.ToString();
                                    // Salva o Relatorio a Ser Atachado numa Lista
                                    reportsCliente.Add(reportMaster);
                                    nomesReports.Add(fileName);
                                    //
                                    reportMaster = new ReportMaster(); //Zera o reportMaster em caso de envio de email
                                }
                                break;
                                #endregion
                            case (int)Relatorios.OperacoesRendaFixa:
                                #region OperacoesRendaFixa
                                numeroRelatoriosClienteGerados++;
                                relatorio = "Operações de Renda Fixa";
                                ReportOperacaoRendaFixa report320 = new ReportOperacaoRendaFixa(idCliente, dataInicioMes, data, null, null, ReportOperacaoRendaFixa.TipoOrdenacao.PorCodigo, null, null);
                                report320.CreateDocument();
                                reportMaster.PrintingSystem.Pages.AddRange(report320.PrintingSystem.Pages);

                                if (tipoEnvio == TipoEnvioRelatorio.Email && !envioEmailPDFUnico)
                                {
                                    fileName = "OperacoesRendaFixa_" + idCliente.ToString();
                                    // Salva o Relatorio a Ser Atachado numa Lista
                                    reportsCliente.Add(reportMaster);
                                    nomesReports.Add(fileName);
                                    //
                                    reportMaster = new ReportMaster(); //Zera o reportMaster em caso de envio de email
                                }
                                break;
                                #endregion
                            case (int)Relatorios.OperacoesFundo:
                                #region OperacoesFundo
                                numeroRelatoriosClienteGerados++;
                                relatorio = "Operações de Fundos";

                                ReportMovimentacaoFundo report330 = new ReportMovimentacaoFundo(idCliente, null, dataInicioMes, data, null, null, null, null, null);
                                report330.CreateDocument();
                                reportMaster.PrintingSystem.Pages.AddRange(report330.PrintingSystem.Pages);

                                if (tipoEnvio == TipoEnvioRelatorio.Email && !envioEmailPDFUnico)
                                {
                                    fileName = "OperacoesFundos_" + idCliente.ToString();
                                    // Salva o Relatorio a Ser Atachado numa Lista
                                    reportsCliente.Add(reportMaster);
                                    nomesReports.Add(fileName);
                                    //
                                    reportMaster = new ReportMaster(); //Zera o reportMaster em caso de envio de email
                                }
                                break;
                                #endregion
                        }
                    }
                    catch (Exception e)
                    {
                        this.mensagem = this.mensagem + "Cliente com problemas no envio: " + idCliente.ToString() + "\n";
                        this.mensagem = this.mensagem + "Relatório: " + relatorio + "\n\n";
                        this.mensagem = this.mensagem + "Erro: " + e.Message + "\n\n";
                    }
                    #endregion
                }

                //Trata Exportação para Email Com Apenas 1 PDF (NOTAR QUE O BOOK DE EXTRATO DE COTISTA É ESPECIFICO, POR ISTO NÃO ENTRA NESTE IF)
                if (this.dataHolderEmailCotista.Count == 0)
                {
                    if (tipoEnvio == TipoEnvioRelatorio.Email && envioEmailPDFUnico)
                    {
                        fileName = "Book_" + idCliente.ToString() + "_" + data.ToString().Substring(0, 10).Replace("/", "-");

                        // Anexa Apenas um Relatorio
                        List<ReportMaster> reportAux = new List<ReportMaster>();
                        reportAux.Add(reportMaster);
                        //
                        List<string> fileNameAux = new List<string>();
                        fileNameAux.Add(fileName);
                        //

                        this.EnviaEmailPorCliente(idCliente, idBook, reportAux, fileNameAux);

                        //
                        reportMaster = new ReportMaster(); //Zera o ReportMaster em caso de envio de email
                    }
                    else if (tipoEnvio == TipoEnvioRelatorio.Email && !envioEmailPDFUnico)
                    {
                        //
                        this.EnviaEmailPorCliente(idCliente, idBook, reportsCliente, nomesReports);
                        //    
                        reportMaster = new ReportMaster(); //Zera o ReportMaster em caso de envio de email
                    }
                }
                //                
            }
            #endregion
        }

        int numeroRelatoriosBookGerados = 0;
        // Trata Book por Book (se escolher o Cliente, perde o Sentido de Gerar Books Gerais)
        if (btnEditCodigoCliente.Text == "")
        {
            BookCollection bookCollection = this.MontaCollectionBookRelatorio();
            foreach (Book book in bookCollection)
            {
                int idBook = book.IdBook.Value;

                BookRelatorioCollection bookRelatorioCollection = new BookRelatorioCollection();
                bookRelatorioCollection.Query.Where(bookRelatorioCollection.Query.IdBook.Equal(idBook));
                bookRelatorioCollection.Query.OrderBy(bookRelatorioCollection.Query.IdRelatorio.Ascending);
                bookRelatorioCollection.Query.Load();

                #region Loop de bookRelatorioCollection

                List<ReportMaster> reportsBook = new List<ReportMaster>();
                List<string> nomesReportsBook = new List<string>();

                foreach (BookRelatorio bookRelatorio in bookRelatorioCollection)
                {
                    int idRelatorio = bookRelatorio.IdRelatorio.Value;
                    try
                    {
                        switch (idRelatorio)
                        {
                            case (int)Relatorios.InformeConsolidadoCarteiras:
                                numeroRelatoriosBookGerados++;
                                #region InformeConsolidadoCarteiras
                                ReportDemonstrativoCarteiras report30 = new ReportDemonstrativoCarteiras(data, Context.User.Identity.Name);
                                report30.CreateDocument();
                                reportMaster.PrintingSystem.Pages.AddRange(report30.PrintingSystem.Pages);

                                if (tipoEnvio == TipoEnvioRelatorio.Email && !envioEmailPDFUnico)
                                {
                                    fileName = "InformeCarteiras_" + data.ToString().Substring(0, 10).Replace("/", "-");
                                    // Salva o Report e o Nome de Exportação
                                    reportsBook.Add(reportMaster);
                                    nomesReportsBook.Add(fileName);
                                    //                                    
                                    reportMaster = new ReportMaster(); //Zera o ReportMaster em caso de envio de email
                                }
                                break;
                                #endregion
                            case (int)Relatorios.ResultadoEnquadramentoConsolidadoDesenquadrado:
                                numeroRelatoriosBookGerados++;
                                #region ResultadoEnquadramentoConsolidadoDesenquadrado
                                ReportResultadoConsolidado report70 = new ReportResultadoConsolidado(data, "N");
                                report70.CreateDocument();
                                reportMaster.PrintingSystem.Pages.AddRange(report70.PrintingSystem.Pages);

                                if (tipoEnvio == TipoEnvioRelatorio.Email && !envioEmailPDFUnico)
                                {
                                    fileName = "Enquadra_" + data.ToString().Substring(0, 10).Replace("/", "-");
                                    // Salva o Report e o Nome de Exportação
                                    reportsBook.Add(reportMaster);
                                    nomesReportsBook.Add(fileName);
                                    //                                    
                                    reportMaster = new ReportMaster(); //Zera o reportMaster em caso de envio de email
                                }
                                break;
                                #endregion
                            case (int)Relatorios.ResultadoEnquadramentoConsolidado:
                                numeroRelatoriosBookGerados++;
                                #region ResultadoEnquadramentoConsolidado
                                ReportResultadoConsolidado report71 = new ReportResultadoConsolidado(data, "");
                                report71.CreateDocument();
                                reportMaster.PrintingSystem.Pages.AddRange(report71.PrintingSystem.Pages);

                                if (tipoEnvio == TipoEnvioRelatorio.Email && !envioEmailPDFUnico)
                                {
                                    fileName = "Enquadra_" + data.ToString().Substring(0, 10).Replace("/", "-");
                                    // Salva o Report e o Nome de Exportação
                                    reportsBook.Add(reportMaster);
                                    nomesReportsBook.Add(fileName);
                                    //                                    
                                    reportMaster = new ReportMaster(); //Zera o ReportMaster em caso de envio de email
                                }
                                break;
                                #endregion
                        }
                    }
                    catch (Exception ex)
                    {
                        this.mensagem = this.mensagem + "Relatório Geral não enviado \n";
                        this.mensagem = this.mensagem + "Erro: " + ex.Message + "\n\n";
                    }
                }
                #endregion

                //Trata Exportação Para Email
                if (tipoEnvio == TipoEnvioRelatorio.Email && envioEmailPDFUnico)
                {
                    // Se não foi gerado nenhum Relatorio, não manda Email
                    if (numeroRelatoriosBookGerados != 0)
                    {
                        fileName = "Book_" + data.ToString().Substring(0, 10).Replace("/", "-");

                        List<ReportMaster> reportAux1 = new List<ReportMaster>();
                        reportAux1.Add(reportMaster);
                        //
                        List<string> fileNameAux1 = new List<string>();
                        fileNameAux1.Add(fileName);
                        //
                        this.EnviaEmailPorBook(idBook, reportAux1, fileNameAux1);
                        //                   
                        reportMaster = new ReportMaster(); //Zera o reportMaster em caso de envio de email
                    }
                }
                else if (tipoEnvio == TipoEnvioRelatorio.Email && !envioEmailPDFUnico)
                {
                    // Se não foi gerado nenhum Relatorio, não manda Email
                    if (numeroRelatoriosBookGerados != 0)
                    {
                        this.EnviaEmailPorBook(idBook, reportsBook, nomesReportsBook);
                        //
                        reportMaster = new ReportMaster(); //Zera o ReportMaster em caso de envio de email
                    }
                }
                //
            }
        }
        #endregion

        #region Monta PDF para Exportação
        if (tipoEnvio == TipoEnvioRelatorio.PDF)
        {
            if (numeroRelatoriosClienteGerados != 0 || numeroRelatoriosBookGerados != 0)
            {

                // Trata Exportação para PDF
                fileName = btnEditCodigoCliente.Text == ""
                    ? "Book_" + data.ToString().Substring(0, 10).Replace("/", "-")
                    : "Book_" + btnEditCodigoCliente.Text.ToString() + "_" + data.ToString().Substring(0, 10).Replace("/", "-");

                this.ExportaPDF(reportMaster, fileName);
            }
            // Não Existe Dados Para Exportar - Exporta PDF Sem Dados
            else
            {
                #region ReportSemDados
                ReportSemDados reportSemDados = new ReportSemDados();
                reportSemDados.CreateDocument();
                ReportMaster r = new ReportMaster();
                r.PrintingSystem.Pages.AddRange(reportSemDados.PrintingSystem.Pages);

                this.ExportaPDF(r, "SemDados");
                #endregion
            }
        }
        #endregion

        #region EnvioEmail ExtratoCotista
        if (tipoEnvio == TipoEnvioRelatorio.Email)
        {
            if (this.dataHolderEmailCotista.Count != 0)
            {
                this.mensagem = this.EnviaEmailExtratoCotista();
            }
        }
        #endregion

        // Apos acabar envia um segundo email com a senha do arquivo PDF
        if (checkSenha.Checked)
        {
            string descricao = "";
            if (bookClienteCollection.Count == 1)
            {
                Book bookDescricao = new Book();
                bookDescricao.LoadByPrimaryKey(bookClienteCollection[0].IdBook.Value);
                descricao = bookDescricao.Descricao + " - " + bookDescricao.Assunto;
            }

            this.EnviaEmailSenhaPDF(descricao);
        }
    }

    /// <summary>
    /// Envia Segundo Email com a senha do arquivo PDF
    /// </summary>
    /// 
    // TODO: a senha esta na variavel this.pdfPassword
    // os emails em listaEmailsSenhaPDF
    private void EnviaEmailSenhaPDF(string descricao)
    {
        Configuration config = WebConfigurationManager.OpenWebConfiguration(HttpContext.Current.Request.ApplicationPath);
        MailSettingsSectionGroup settings = (MailSettingsSectionGroup)config.GetSectionGroup("system.net/mailSettings");

        // Create a new message and attach the PDF report to it.
        MailMessage mail = new MailMessage();

        if (listaEmailsSenhaPDF.Count != 0)
        {
            for (int j = 0; j < listaEmailsSenhaPDF.Count; j++)
            {
                mail.Bcc.Add(new MailAddress(listaEmailsSenhaPDF[j]));
            }
            // Specify sender and recipient options for the e-mail message.
            mail.From = new MailAddress(this.emailSenha);

            // Specify other e-mail options.
            mail.Subject = descricao == "" ? "Senha de acesso para relatório(s)" : descricao + " - Senha de acesso";
            mail.Body = "Caro(a), segue a senha para acesso ao(s) relatório(s) enviado(s). \n\n" + "Senha: " + this.pdfPassword;

            // Send the e-mail message via the specified SMTP server.
            SmtpClient smtp = new SmtpClient();

            smtp.EnableSsl = Convert.ToBoolean(WebConfig.AppSettings.SMTPEnableSSL);

            ServicePointManager.ServerCertificateValidationCallback = delegate(object s, X509Certificate certificate, X509Chain chain, SslPolicyErrors sslPolicyErrors) { return true; };

            string smtpUsername = ConfigurationManager.AppSettings["SMTPCredentialsUsername"];
            if (!string.IsNullOrEmpty(smtpUsername))
            {
                string smtpPassword = ConfigurationManager.AppSettings["SMTPCredentialsPassword"];
                smtp.Credentials = new System.Net.NetworkCredential(smtpUsername, smtpPassword);
            }

            smtp.Send(mail);
        }
    }

    /// <summary>
    /// Salva Informações do Cotista de uma Carteira para posterior envio do email de ExtratoCotista
    /// </summary>
    /// <param name="idCotista"></param>
    /// <param name="idCliente"></param>
    /// <param name="reportMasterExtratoCotista"></param>
    /// <param name="assuntoMsg"></param>
    /// <param name="corpoMsg"></param>
    /// <param name="emailOrigem"></param>
    /// <param name="idBook"></param>
    /// <modifies>this.dataHolderEmailCotista com as informações do Cotista</modifies>
    private void SalvaInfoExtratoCotista(int idCotista, int idCliente, ReportExtratoCotista reportMasterExtratoCotista,
                                         string assuntoMsg, string corpoMsg, string emailOrigem, int idBook)
    {
        //
        DataHolderEmailCotista d = new DataHolderEmailCotista();
        //
        d.idCotista = idCotista;
        d.idCarteira = idCliente;
        d.report = reportMasterExtratoCotista;
        d.idBook = idBook;
        //

        d.assunto = assuntoMsg;
        d.mensagem = corpoMsg;
        d.emailOrigem = emailOrigem;

        // Procura Email        
        PessoaEmailCollection p = new PessoaEmailCollection();
        //
        p.Query.Select(p.Query.Email)
               .Where(p.Query.IdPessoa == idCotista)
               .OrderBy(p.Query.Email.Ascending);

        if (p.Query.Load())
        {
            if (p[0].es.HasData)
            {
                if (!String.IsNullOrEmpty(p[0].Email))
                {
                    d.emailDestino = p[0].Email.Trim();
                }
            }
        }

        d.nomeArquivoPdf = "ExtratoCotista_" + idCotista + ".pdf";
        //
        this.dataHolderEmailCotista.Add(d);
    }

    /// <summary>
    /// Gera um Conjunto de Planilhas Excel de acordo com um book de Cliente ou 1 cliente
    /// </summary>
    /// <param name="dicMStream">Dicionario onde Chave = nome Arquivo Excel e conteudo= MemoryStream do arquivo excel</param>
    protected void MontaReportExcel(out Dictionary<string, MemoryStream> dicMStream)
    {

        dicMStream = new Dictionary<string, MemoryStream>();

        #region Por BookCliente e Cliente

        string fileName = "";
        this.mensagem = "";
        //
        DateTime data = Convert.ToDateTime(textData.Text);
        DateTime dataInicioMes = Calendario.RetornaPrimeiroDiaUtilMes(data, 0, LocalFeriadoFixo.Brasil, TipoFeriado.Brasil);
        string login = HttpContext.Current.User.Identity.Name; //Usado apenas no relatorio de Saldos de Cotistas
        //
        BookClienteCollection bookClienteCollection = this.MontaCollectionBookCliente();

        foreach (BookCliente bookCliente in bookClienteCollection)
        {

            #region Informações do Cliente
            int idCliente = bookCliente.IdCliente.Value;
            Cliente cliente = new Cliente();
            List<esQueryItem> campos = new List<esQueryItem>();
            campos.Add(cliente.Query.IdCliente);
            campos.Add(cliente.Query.DataDia);
            campos.Add(cliente.Query.DataImplantacao);
            campos.Add(cliente.Query.ZeraCaixa);
            cliente.LoadByPrimaryKey(campos, idCliente);
            #endregion

            #region Tratamento Para Cada Cliente Do Book
            if (cliente.DataDia.Value < data)
            {
                //this.mensagem += "Cliente " + idCliente.ToString() + " com data Dia: " + cliente.DataDia.Value.ToString("d") + " anterior a data do relatório \n";
                //this.mensagem += "Não pôde ser incluído nos relatórios! \n\n";
            }
            else if (cliente.DataImplantacao.Value > data)
            {
                //this.mensagem += "Cliente " + idCliente.ToString() + " com data de implantação: " + cliente.DataImplantacao.Value.ToString("d") + "superior a data do relatório. \n";
                //this.mensagem += "Não pôde ser incluído nos relatórios! \n\n";
            }
            else
            {
                int idBook = bookCliente.IdBook.Value;

                BookRelatorioCollection bookRelatorioCollection = new BookRelatorioCollection();
                bookRelatorioCollection.Query.Where(bookRelatorioCollection.Query.IdBook == idBook);
                bookRelatorioCollection.Query.OrderBy(bookRelatorioCollection.Query.IdRelatorio.Ascending);
                bookRelatorioCollection.Query.Load();
                //

                foreach (BookRelatorio bookRelatorio in bookRelatorioCollection)
                {
                    #region Tratamento Para Cada Relatorio pertencente ao Book
                    //string relatorio = "";
                    int idRelatorio = bookRelatorio.IdRelatorio.Value;

                    fileName = idCliente.ToString() + "_" + data.ToString("dd-MM-yyyy") + ".xls";

                    try
                    {
                        switch (idRelatorio)
                        {
                            case (int)Relatorios.ComposicaoCarteira:
                                #region ComposicaoCarteira
                                //numeroRelatoriosClienteGerados++;
                                //relatorio = "Composição Carteira";
                                fileName = fileName.Insert(0, "Book" + idBook + "_ComposicaoCarteira_");
                                ReportComposicaoCarteira report1 = new ReportComposicaoCarteira(idCliente, data, ReportComposicaoCarteira.TipoRelatorio.Fechamento, ReportComposicaoCarteira.TipoLingua.Portugues, null, true);

                                MemoryStream m1 = new MemoryStream();
                                //                                                           
                                report1.ExportToXls(m1);
                                //
                                dicMStream.Add(fileName, m1);
                                //
                                break;
                                #endregion
                            case (int)Relatorios.ExtratoClienteMensal:
                                #region ExtratoClienteMensal

                                Carteira carteira = new Carteira();
                                carteira.LoadByPrimaryKey(idCliente);

                                DateTime dataInicio = Calendario.RetornaPrimeiroDiaUtilMes(data, 0, (int)LocalFeriadoFixo.Brasil, TipoFeriado.Brasil);
                                if (dataInicio < carteira.DataInicioCota.Value)
                                {
                                    dataInicio = carteira.DataInicioCota.Value;
                                }

                                //numeroRelatoriosClienteGerados++;
                                //relatorio = "Extrato de Cliente";
                                bool mostraCaixa = cliente.ZeraCaixa.Trim().ToUpper() == "S" ? false : true;
                                ReportExtratoCliente report25 = new ReportExtratoCliente(idCliente, carteira.IdIndiceBenchmark.Value, dataInicio, data, mostraCaixa, false);
                                //
                                fileName = fileName.Insert(0, "Book" + idBook + "_ExtratoClienteMensal_");
                                //
                                MemoryStream m25 = new MemoryStream();
                                //                                                           
                                report25.ExportToXls(m25);
                                //
                                dicMStream.Add(fileName, m25);

                                break;
                                #endregion
                            case (int)Relatorios.ExtratoContaCorrente:
                                #region ExtratoContaCorrente
                                //numeroRelatoriosClienteGerados++;
                                //relatorio = "Extrato de C/C";
                                ReportExtratoContaCorrente report10 = new ReportExtratoContaCorrente(idCliente, dataInicioMes, data, null, null);

                                fileName = fileName.Insert(0, "Book" + idBook + "_ExtratoCC_");
                                //
                                MemoryStream m10 = new MemoryStream();
                                //                                                           
                                report10.ExportToXls(m10);
                                //
                                dicMStream.Add(fileName, m10);

                                break;
                                #endregion
                            case (int)Relatorios.ApuracaoGanhhoRV:
                                #region ApuracaoGanhhoRV
                                //numeroRelatoriosClienteGerados++;
                                //relatorio = "ApuracaoGanhoRV";

                                // Cria Vários arquivos Excel - NomeArquivo tem um # para indicar que deve ser criado um Diretorio no ZIP para esses arquivos

                                #region Mes 1 a 4
                                ReportGanhoRendaVariavel report60 = new ReportGanhoRendaVariavel(idCliente, 4, data.Year);

                                fileName = idCliente.ToString() + "_" + data.ToString("dd-MM-yyyy") + ".xls";
                                fileName = fileName.Insert(0, "#" + "Book" + idBook + "_ApuracaoGanhhoRV_Jan-Abril_");

                                MemoryStream m60 = new MemoryStream();
                                //                                                           
                                report60.ExportToXls(m60);
                                //
                                dicMStream.Add(fileName, m60);
                                #endregion

                                #region Mes 5 a 8
                                ReportGanhoRendaVariavel report61 = new ReportGanhoRendaVariavel(idCliente, 8, data.Year);

                                fileName = idCliente.ToString() + "_" + data.ToString("dd-MM-yyyy") + ".xls";
                                fileName = fileName.Insert(0, "#" + "Book" + idBook + "_ApuracaoGanhhoRV_Maio-Ago_");

                                MemoryStream m61 = new MemoryStream();
                                //                                                           
                                report61.ExportToXls(m61);
                                //
                                dicMStream.Add(fileName, m61);

                                #endregion

                                #region Mes 9 a 12
                                ReportGanhoRendaVariavel report62 = new ReportGanhoRendaVariavel(idCliente, 12, data.Year);

                                fileName = idCliente.ToString() + "_" + data.ToString("dd-MM-yyyy") + ".xls";
                                fileName = fileName.Insert(0, "#" + "Book" + idBook + "_ApuracaoGanhhoRV_Set-Dez_");

                                MemoryStream m62 = new MemoryStream();
                                //                                                           
                                report62.ExportToXls(m62);
                                //
                                dicMStream.Add(fileName, m62);

                                #endregion

                                #region ReportGanhoRendaVariavel Gera somente 1 Excel xlsx - Funcionou
                                //ReportMaster reportMaster = new ReportMaster();
                                //reportMaster.PrintingSystem.ContinuousPageNumbering = false;


                                //ReportGanhoRendaVariavel report60 = new ReportGanhoRendaVariavel(idCliente, 4, data.Year);
                                //report60.CreateDocument();
                                //reportMaster.PrintingSystem.Pages.AddRange(report60.PrintingSystem.Pages);

                                //report60 = new ReportGanhoRendaVariavel(idCliente, 8, data.Year);
                                //report60.CreateDocument();
                                //reportMaster.PrintingSystem.Pages.AddRange(report60.PrintingSystem.Pages);

                                //report60 = new ReportGanhoRendaVariavel(idCliente, 12, data.Year);
                                //report60.CreateDocument();
                                //reportMaster.PrintingSystem.Pages.AddRange(report60.PrintingSystem.Pages);

                                //fileName = "ApuracaoGanhoRV_" + idCliente.ToString() + "_" + data.ToString("ddMMyyyy") + ".xlsx";

                                ////
                                //MemoryStream m60 = new MemoryStream();
                                ////                      
                                //XlsxExportOptions x = new XlsxExportOptions();
                                //x.ExportMode = XlsxExportMode.SingleFilePageByPage;
                                ////
                                //reportMaster.ExportToXlsx(m60, x);
                                ////
                                //dicMStream.Add(fileName, m60); 
                                #endregion

                                break;
                                #endregion
                            case (int)Relatorios.FluxoCaixaSintetico:
                                #region FluxoCaixaSintetico
                                //numeroRelatoriosClienteGerados++;
                                //relatorio = "Fluxo de Caixa Sintético";

                                Carteira.BooleanosFluxoCaixa booleanosFluxoCaixa = new Carteira.BooleanosFluxoCaixa();
                                booleanosFluxoCaixa.PosicaoBolsa = false;
                                booleanosFluxoCaixa.PosicaoFundos = false;
                                booleanosFluxoCaixa.RendaFixa = true;
                                booleanosFluxoCaixa.ResgatesNaoConvertidos = false;

                                ReportFluxoCaixaSintetico report16 = new ReportFluxoCaixaSintetico(idCliente, data, booleanosFluxoCaixa);
                                //
                                fileName = fileName.Insert(0, "Book" + idBook + "_CaixaSintetico_");

                                MemoryStream m16 = new MemoryStream();
                                //                                                           
                                report16.ExportToXls(m16);
                                //
                                dicMStream.Add(fileName, m16);

                                break;
                                #endregion
                            case (int)Relatorios.InformeConsolidadoCarteiras:
                                #region InformeConsolidadoCarteiras
                                //numeroRelatoriosClienteGerados++;
                                //relatorio = "Informe Consolidado de Carteiras";

                                ReportDemonstrativoCarteiras report30 = new ReportDemonstrativoCarteiras(data, Context.User.Identity.Name);
                                //
                                fileName = fileName.Insert(0, "Book" + idBook + "_InformeCarteiras_");

                                MemoryStream m30 = new MemoryStream();
                                //                                                           
                                report30.ExportToXls(m30);
                                //
                                dicMStream.Add(fileName, m30);

                                break;
                                #endregion
                            case (int)Relatorios.MapaResultado:
                                #region MapaResultado
                                //numeroRelatoriosClienteGerados++;
                                //relatorio = "Mapa de Resultado";

                                ReportMapaResultado report50 = new ReportMapaResultado(idCliente, dataInicioMes, data);
                                //
                                fileName = fileName.Insert(0, "Book" + idBook + "_MapaResultado_");

                                MemoryStream m50 = new MemoryStream();
                                //                                                           
                                report50.ExportToXls(m50);
                                //
                                dicMStream.Add(fileName, m50);

                                break;
                                #endregion
                            case (int)Relatorios.ResultadoEnquadramentoConsolidadoDesenquadrado:
                                #region ResultadoEnquadramentoConsolidadoDesenquadrado
                                //numeroRelatoriosClienteGerados++;
                                //relatorio = "Enquadramento Consolidado (Desenquadrados)";

                                ReportResultadoConsolidado report70 = new ReportResultadoConsolidado(data, "N");

                                fileName = fileName.Insert(0, "Book" + idBook + "_EnquadramentoConsolidadoDesenquadrado_");

                                MemoryStream m70 = new MemoryStream();
                                //                                                           
                                report70.ExportToXls(m70);
                                //
                                dicMStream.Add(fileName, m70);

                                break;
                                #endregion
                            case (int)Relatorios.ResultadoEnquadramentoConsolidado:
                                #region ResultadoEnquadramentoConsolidado
                                //numeroRelatoriosClienteGerados++;
                                //relatorio = "Enquadramento Consolidado (Geral)";

                                ReportResultadoConsolidado report71 = new ReportResultadoConsolidado(data, "");
                                //
                                fileName = fileName.Insert(0, "Book" + idBook + "_EnquadramentoConsolidado_");

                                MemoryStream m71 = new MemoryStream();
                                //                                                           
                                report71.ExportToXls(m71);
                                //
                                dicMStream.Add(fileName, m71);

                                break;
                                #endregion
                            case (int)Relatorios.ResultadoEnquadramentoIndividual:
                                #region ResultadoEnquadramentoIndividual
                                //numeroRelatoriosClienteGerados++;
                                //relatorio = "Enquadramento";

                                ReportResultadoConsolidado report72 = new ReportResultadoConsolidado(idCliente, data, "");
                                //
                                fileName = fileName.Insert(0, "Book" + idBook + "_EnquadramentoIndividual_");

                                MemoryStream m72 = new MemoryStream();
                                //                                                           
                                report72.ExportToXls(m72);
                                //
                                dicMStream.Add(fileName, m72);

                                break;
                                #endregion
                            case (int)Relatorios.NotasAplicacaoResgate:
                                #region NotasAplicacaoResgate
                                // relatorio = "Notas de Aplic/Resgate";
                                //
                                OperacaoCotistaCollection operacaoCotistaCollection = new OperacaoCotistaCollection();
                                operacaoCotistaCollection.Query.Where(operacaoCotistaCollection.Query.DataConversao == data,
                                                                      operacaoCotistaCollection.Query.IdCarteira == idCliente);
                                //
                                operacaoCotistaCollection.Query.Load();

                                foreach (OperacaoCotista operacaoCotista in operacaoCotistaCollection)
                                {
                                    int idOperacao = operacaoCotista.IdOperacao.Value;
                                    if (operacaoCotista.TipoOperacao == (byte)TipoOperacaoCotista.Aplicacao ||
                                        operacaoCotista.TipoOperacao == (byte)TipoOperacaoCotista.AplicacaoAcoesEspecial)
                                    {

                                        ReportNotaAplicacao report85 = new ReportNotaAplicacao(idOperacao, TipoLingua.Portugues);

                                        fileName = idCliente.ToString() + "_" + data.ToString("dd-MM-yyyy") + ".xls";
                                        fileName = fileName.Insert(0, "#" + "Book" + idBook + "_NotaAplicacao" + idOperacao + "_");

                                        MemoryStream m85 = new MemoryStream();
                                        //                                                           
                                        report85.ExportToXls(m85);
                                        //
                                        dicMStream.Add(fileName, m85);
                                    }
                                    else
                                    {
                                        ReportNotaResgate report86 = new ReportNotaResgate(idOperacao, TipoLingua.Portugues);

                                        fileName = idCliente.ToString() + "_" + data.ToString("dd-MM-yyyy") + ".xls";
                                        fileName = fileName.Insert(0, "#" + "Book" + idBook + "_NotaAplicacaoResgate" + idOperacao + "_");

                                        MemoryStream m86 = new MemoryStream();
                                        //                                                           
                                        report86.ExportToXls(m86);
                                        //
                                        dicMStream.Add(fileName, m86);
                                    }
                                }

                                break;
                                #endregion
                            case (int)Relatorios.ExtratoCotista:
                                #region ExtratoCotista

                                #region Para 1 Carteira - Acrescenta varios Cotistas
                                //Datas para a Posicao               
                                DateTime dataInicioPosicao = Calendario.RetornaPrimeiroDiaUtilMes(data, 0, (int)LocalFeriadoFixo.Brasil, TipoFeriado.Brasil);
                                dataInicioPosicao = Calendario.SubtraiDiaUtil(dataInicioPosicao, 1, (int)LocalFeriadoFixo.Brasil, TipoFeriado.Brasil);

                                DateTime dataFimPosicao = data;

                                //Datas para a Operacao
                                DateTime dataInicioOperacao = dataInicioPosicao;
                                DateTime dataFimOperacao = dataFimPosicao;

                                List<int> listaIdCotista = new CotistaCollection().BuscaCotistasDaCarteira(idCliente, dataInicioPosicao, dataFimPosicao,
                                                                    dataInicioOperacao, dataFimOperacao, HttpContext.Current.User.Identity.Name);
                                //

                                for (int i = 0; i < listaIdCotista.Count; i++)
                                {
                                    // Cria Vários arquivos Excel - NomeArquivo tem um # para indicar que deve ser criado um Diretorio no ZIP para esses arquivos

                                    ReportExtratoCotista report104 = new ReportExtratoCotista(idCliente, listaIdCotista[i], dataInicioOperacao, data);
                                    int k = i + 1;
                                    fileName = idCliente.ToString() + "_" + data.ToString("dd-MM-yyyy") + ".xls";
                                    fileName = fileName.Insert(0, "#" + "Book" + idBook + "_" + listaIdCotista[i] + "_ExtratoCotista" + k + "_");

                                    MemoryStream m104 = new MemoryStream();
                                    //                                                           
                                    report104.ExportToXls(m104);
                                    //
                                    dicMStream.Add(fileName, m104);
                                }
                                #endregion

                                break;
                                #endregion
                            case (int)Relatorios.ExtratoCotistaComCarteira:
                                #region ExtratoCotistaComCarteira

                                #region Para 1 Carteira - Acrescenta varios Cotistas
                                //Datas para a Posicao               
                                DateTime dataInicioPosicao2 = Calendario.RetornaPrimeiroDiaUtilMes(data, 0, (int)LocalFeriadoFixo.Brasil, TipoFeriado.Brasil);
                                dataInicioPosicao2 = Calendario.SubtraiDiaUtil(dataInicioPosicao2, 1, (int)LocalFeriadoFixo.Brasil, TipoFeriado.Brasil);

                                DateTime dataFimPosicao2 = data;

                                //Datas para a Operacao
                                DateTime dataInicioOperacao2 = dataInicioPosicao2;
                                DateTime dataFimOperacao2 = dataFimPosicao2;

                                List<int> listaIdCotista2 = new CotistaCollection().BuscaCotistasDaCarteira(idCliente, dataInicioPosicao2, dataFimPosicao2,
                                                                    dataInicioOperacao2, dataFimOperacao2, HttpContext.Current.User.Identity.Name);
                                //

                                for (int i = 0; i < listaIdCotista2.Count; i++)
                                {
                                    // Cria Vários arquivos Excel - NomeArquivo tem um # para indicar que deve ser criado um Diretorio no ZIP para esses arquivos

                                    ReportExtratoCotista report105 = new ReportExtratoCotista(idCliente, listaIdCotista2[i], dataInicioOperacao2, data, true);
                                    int k = i + 1;
                                    fileName = idCliente.ToString() + "_" + data.ToString("dd-MM-yyyy") + ".xls";
                                    fileName = fileName.Insert(0, "#" + "Book" + idBook + "_" + listaIdCotista2[i] + "_ExtratoCotistaComCarteira" + k + "_");

                                    MemoryStream m105 = new MemoryStream();
                                    //                                                           
                                    report105.ExportToXls(m105);
                                    //
                                    dicMStream.Add(fileName, m105);
                                }
                                #endregion

                                break;
                                #endregion
                            case (int)Relatorios.MovimentoCotista:
                                #region MovimentoCotista
                                //numeroRelatoriosClienteGerados++;
                                //relatorio = "Movimentação de Cotista";

                                ReportMovimentacaoCotista report90 = new ReportMovimentacaoCotista(null, idCliente, dataInicioMes, data, null, null, null, null, null,
                                                                                                   ReportMovimentacaoCotista.TipoOpcoesCampo.Rendimento);

                                fileName = fileName.Insert(0, "Book" + idBook + "_MovimentoCotista_");

                                MemoryStream m90 = new MemoryStream();
                                //                                                           
                                report90.ExportToXls(m90);
                                //
                                dicMStream.Add(fileName, m90);

                                break;
                                #endregion
                            case (int)Relatorios.SaldoCotistaAnaliticoCodigo:
                                #region SaldoCotistaAnaliticoCodigo
                                //numeroRelatoriosClienteGerados++;
                                //relatorio = "Saldos de Aplicação de Cotista";

                                ReportSaldoAplicacaoCotista report100 = new ReportSaldoAplicacaoCotista(idCliente, null, data,
                                                            ReportSaldoAplicacaoCotista.TipoRelatorio.Analitico,
                                                            ReportSaldoAplicacaoCotista.TipoGrupamento.PorCodigo, ReportSaldoAplicacaoCotista.OpcaoValorAplicado.ValorAtualizado,
                                                            login, ReportSaldoAplicacaoCotista.TipoLingua.Portugues);

                                fileName = fileName.Insert(0, "Book" + idBook + "_SaldoCotistaAnaliticoCodigo_");

                                MemoryStream m100 = new MemoryStream();
                                //                                                           
                                report100.ExportToXls(m100);
                                //
                                dicMStream.Add(fileName, m100);

                                break;
                                #endregion
                            case (int)Relatorios.SaldoCotistaAnaliticoNome:
                                #region SaldoCotistaAnaliticoNome
                                //numeroRelatoriosClienteGerados++;
                                //relatorio = "Saldos de Aplicação de Cotista";

                                ReportSaldoAplicacaoCotista report101 = new ReportSaldoAplicacaoCotista(idCliente, null, data,
                                                            ReportSaldoAplicacaoCotista.TipoRelatorio.Analitico,
                                                            ReportSaldoAplicacaoCotista.TipoGrupamento.PorNome,
                                                            ReportSaldoAplicacaoCotista.OpcaoValorAplicado.ValorAtualizado,
                                                            login, ReportSaldoAplicacaoCotista.TipoLingua.Portugues);

                                fileName = fileName.Insert(0, "Book" + idBook + "_SaldoCotistaAnaliticoNome_");

                                MemoryStream m101 = new MemoryStream();
                                //                                                           
                                report101.ExportToXls(m101);
                                //
                                dicMStream.Add(fileName, m101);

                                break;
                                #endregion
                            case (int)Relatorios.SaldoCotistaConsolidadoCodigo:
                                #region SaldoCotistaConsolidadoCodigo
                                //numeroRelatoriosClienteGerados++;
                                //relatorio = "Saldos de Aplicação de Cotista";

                                ReportSaldoAplicacaoCotista report102 = new ReportSaldoAplicacaoCotista(idCliente, null, data,
                                                            ReportSaldoAplicacaoCotista.TipoRelatorio.Consolidado,
                                                            ReportSaldoAplicacaoCotista.TipoGrupamento.PorCodigo,
                                                            ReportSaldoAplicacaoCotista.OpcaoValorAplicado.ValorAtualizado,
                                                            login, ReportSaldoAplicacaoCotista.TipoLingua.Portugues);

                                fileName = fileName.Insert(0, "Book" + idBook + "_SaldoCotistaConsolidadoCodigo_");

                                MemoryStream m102 = new MemoryStream();
                                //                                                           
                                report102.ExportToXls(m102);
                                //
                                dicMStream.Add(fileName, m102);

                                break;
                                #endregion
                            case (int)Relatorios.SaldoCotistaConsolidadoNome:
                                #region SaldoCotistaConsolidadoCodigo
                                //numeroRelatoriosClienteGerados++;
                                //relatorio = "Saldos de Aplicação de Cotista";

                                ReportSaldoAplicacaoCotista report103 = new ReportSaldoAplicacaoCotista(idCliente, null, data,
                                                            ReportSaldoAplicacaoCotista.TipoRelatorio.Consolidado,
                                                            ReportSaldoAplicacaoCotista.TipoGrupamento.PorNome,
                                                            ReportSaldoAplicacaoCotista.OpcaoValorAplicado.ValorAtualizado,
                                                            login, ReportSaldoAplicacaoCotista.TipoLingua.Portugues);

                                fileName = fileName.Insert(0, "Book" + idBook + "_SaldoCotistaConsolidadoNome_");

                                MemoryStream m103 = new MemoryStream();
                                //                                                           
                                report103.ExportToXls(m103);
                                //
                                dicMStream.Add(fileName, m103);

                                break;
                                #endregion
                            case (int)Relatorios.OperacoesBolsa:
                                #region OperacoesBolsa
                                //numeroRelatoriosClienteGerados++;
                                //relatorio = "Operações de Bolsa";

                                ReportMapaOperacaoBolsa report300 = new ReportMapaOperacaoBolsa(dataInicioMes, data, idCliente);

                                fileName = fileName.Insert(0, "Book" + idBook + "_OperacoesBolsa_");

                                MemoryStream m300 = new MemoryStream();
                                //                                                           
                                report300.ExportToXls(m300);
                                //
                                dicMStream.Add(fileName, m300);

                                break;
                                #endregion
                            case (int)Relatorios.OperacoesBMF:
                                #region OperacoesBMF
                                //numeroRelatoriosClienteGerados++;
                                //relatorio = "Operações de BMF";

                                ReportMapaOperacaoBMF report310 = new ReportMapaOperacaoBMF(dataInicioMes, data, idCliente);

                                fileName = fileName.Insert(0, "Book" + idBook + "_OperacoesBMF_");

                                MemoryStream m310 = new MemoryStream();
                                //                                                           
                                report310.ExportToXls(m310);
                                //
                                dicMStream.Add(fileName, m310);

                                break;
                                #endregion
                            case (int)Relatorios.OperacoesRendaFixa:
                                #region OperacoesRendaFixa
                                //numeroRelatoriosClienteGerados++;
                                //relatorio = "Operações de Renda Fixa";

                                ReportOperacaoRendaFixa report320 = new ReportOperacaoRendaFixa(idCliente, dataInicioMes, data, null, null, ReportOperacaoRendaFixa.TipoOrdenacao.PorCodigo, null, null);

                                fileName = fileName.Insert(0, "Book" + idBook + "_OperacoesRendaFixa_");

                                MemoryStream m320 = new MemoryStream();
                                //                                                           
                                report320.ExportToXls(m320);
                                //
                                dicMStream.Add(fileName, m320);

                                break;
                                #endregion
                            case (int)Relatorios.OperacoesFundo:
                                #region OperacoesFundo
                                //numeroRelatoriosClienteGerados++;
                                //relatorio = "Operações de Fundos";

                                ReportMovimentacaoFundo report330 = new ReportMovimentacaoFundo(idCliente, null, dataInicioMes, data, null, null, null, null, null);

                                fileName = fileName.Insert(0, "Book" + idBook + "_OperacoesFundos_");

                                MemoryStream m330 = new MemoryStream();
                                //                                                           
                                report330.ExportToXls(m330);
                                //
                                dicMStream.Add(fileName, m330);

                                break;
                                #endregion
                        }
                    }
                    catch (Exception e)
                    {
                        //this.mensagem = this.mensagem + "Cliente com problemas no envio: " + idCliente.ToString() + "\n";
                        //this.mensagem = this.mensagem + "Relatório: " + relatorio + "\n\n";
                        //this.mensagem = this.mensagem + "Erro: " + e.Message + "\n\n";
                    }
                    #endregion
                }
            }
            #endregion
        }
        #endregion

        #region Book do Tipo Geral
        // Trata Book por Book (se escolher o Cliente, perde o Sentido de Gerar Books Gerais)
        if (btnEditCodigoCliente.Text == "")
        {
            BookCollection bookCollection = this.MontaCollectionBookRelatorio();

            foreach (Book book in bookCollection)
            {
                int idBook = book.IdBook.Value;

                BookRelatorioCollection bookRelatorioCollection = new BookRelatorioCollection();
                bookRelatorioCollection.Query.Where(bookRelatorioCollection.Query.IdBook.Equal(idBook));
                bookRelatorioCollection.Query.OrderBy(bookRelatorioCollection.Query.IdRelatorio.Ascending);
                bookRelatorioCollection.Query.Load();

                #region Loop de bookRelatorioCollection

                foreach (BookRelatorio bookRelatorio in bookRelatorioCollection)
                {
                    int idRelatorio = bookRelatorio.IdRelatorio.Value;
                    //
                    fileName = data.ToString("dd-MM-yyyy") + ".xls";

                    try
                    {
                        switch (idRelatorio)
                        {
                            case (int)Relatorios.InformeConsolidadoCarteiras:
                                //numeroRelatoriosBookGerados++;
                                #region InformeConsolidadoCarteiras
                                ReportDemonstrativoCarteiras report30 = new ReportDemonstrativoCarteiras(data, Context.User.Identity.Name);

                                // Formato BookGeral + idBook é usado para criar Diretorio no arquivo ZIP
                                fileName = fileName.Insert(0, "BookGeral" + idBook + "_InformeCarteiras_");

                                MemoryStream m30 = new MemoryStream();
                                //                                                           
                                report30.ExportToXls(m30);
                                //
                                dicMStream.Add(fileName, m30);

                                break;
                                #endregion
                            case (int)Relatorios.ResultadoEnquadramentoConsolidadoDesenquadrado:
                                //numeroRelatoriosBookGerados++;
                                #region ResultadoEnquadramentoConsolidadoDesenquadrado
                                ReportResultadoConsolidado report70 = new ReportResultadoConsolidado(data, "N");

                                // Formato BookGeral + idBook é usado para criar Diretorio no arquivo ZIP
                                fileName = fileName.Insert(0, "BookGeral" + idBook + "_EnquadraConsolidadoDesenquadrado_");

                                MemoryStream m70 = new MemoryStream();
                                //                                                           
                                report70.ExportToXls(m70);
                                //
                                dicMStream.Add(fileName, m70);

                                break;
                                #endregion
                            case (int)Relatorios.ResultadoEnquadramentoConsolidado:
                                //numeroRelatoriosBookGerados++;
                                #region ResultadoEnquadramentoConsolidado
                                ReportResultadoConsolidado report71 = new ReportResultadoConsolidado(data, "");

                                // Formato BookGeral + idBook é usado para criar Diretorio no arquivo ZIP
                                fileName = fileName.Insert(0, "BookGeral" + idBook + "_EnquadraConsolidado_");

                                MemoryStream m71 = new MemoryStream();
                                //                                                           
                                report71.ExportToXls(m71);
                                //
                                dicMStream.Add(fileName, m71);

                                break;
                                #endregion
                        }
                    }
                    catch (Exception ex)
                    {
                        //this.mensagem = this.mensagem + "Relatório Geral não enviado \n";
                        //this.mensagem = this.mensagem + "Erro: " + ex.Message + "\n\n";
                    }
                }
                #endregion
            }
        }
        #endregion

        if (dicMStream.Count == 0)
        {
            this.mensagem = "Não há dados para exportar.";
        }
    }
}