using System;
using Financial.Relatorio;
using System.IO;
using System.Web;
using System.Globalization;
using Financial.Web.Common;

public partial class _ReportResultadoConsolidado : BasePage {

    /* Como Usar:
        DateTime dataReferencia = new DateTime(2007, 1, 1);        
        //
        int idCarteira = 1;
        string enquadrado = "S";
        //                
        //ReportViewer1.Report = new ReportResultadoConsolidado(dataReferencia);
        //
        // 2 parametros        
        ReportViewer1.Report = new ReportResultadoConsolidado(idCarteira, dataReferencia);
        //ReportViewer1.Report = new ReportResultadoConsolidado(dataReferencia, enquadrado);
        
        // 3 parametros
        //ReportViewer1.Report = new ReportResultadoConsolidado(idCarteira, dataReferencia, enquadrado);
     */
    new protected void Page_Load(object sender, EventArgs e) {
        string visao = Request.QueryString["Visao"].ToString();
        //        
        DateTime dataReferencia = new DateTime();
        int? idCarteira = null;
        string enquadrado = null;
        int? tipo = null;

        // Obrigatorio
        dataReferencia = Convert.ToDateTime(Session["textData"]);
        //Opcionais
        enquadrado = (string)Session["dropFiltro"];

        if (Session["dropTipo"] != null)
        {
            tipo = Convert.ToInt32(Session["dropTipo"]);
        }

        if (!String.IsNullOrEmpty((string)Session["btnEditCodigo"])) {
            idCarteira = Convert.ToInt32(Session["btnEditCodigo"]);
        }

        bool mostraDefasado  = Convert.ToBoolean(Session["checkDefasado"]);
        
        if (visao == "Report") {
            ReportViewer1.Report = new ReportResultadoConsolidado(idCarteira, dataReferencia, enquadrado, tipo, mostraDefasado);
        }
        else if (visao == "PDF") {
            #region PDF
            ReportResultadoConsolidado report = new ReportResultadoConsolidado(idCarteira, dataReferencia, enquadrado, tipo, mostraDefasado);

            MemoryStream ms = new MemoryStream();
            report.ExportToPdf(ms);
            ms.Seek(0, SeekOrigin.Begin);
            Response.ClearContent();
            Response.ClearHeaders();
            Response.Buffer = true;
            Response.Cache.SetCacheability(HttpCacheability.Private);
            Response.ContentType = "application/pdf";
            Response.AddHeader("Content-Length", ms.Length.ToString(CultureInfo.CurrentCulture));
            Response.AddHeader("Content-Disposition", String.Format("attachment;filename=ReportResultadoConsolidado.pdf"));

            Response.BinaryWrite(ms.ToArray());
            ms.Close();
            HttpContext.Current.ApplicationInstance.CompleteRequest();
            Response.End();
            #endregion
        }
        else if (visao == "Excel") {
            #region Excel
            ReportResultadoConsolidado report = new ReportResultadoConsolidado(idCarteira, dataReferencia, enquadrado, tipo, mostraDefasado);

            MemoryStream ms = new MemoryStream();
            report.ExportToXls(ms);
            ms.Seek(0, SeekOrigin.Begin);
            Response.ClearContent();
            Response.ClearHeaders();
            Response.Buffer = true;
            Response.Cache.SetCacheability(HttpCacheability.Private);
            Response.ContentType = "application/vnd.ms-excel";
            Response.AddHeader("Content-Length", ms.Length.ToString(CultureInfo.CurrentCulture));
            Response.AddHeader("Content-Disposition", String.Format("attachment;filename=ReportResultadoConsolidado.xls"));

            Response.BinaryWrite(ms.ToArray());
            ms.Close();
            HttpContext.Current.ApplicationInstance.CompleteRequest();
            Response.End();

            #endregion
        }
    }
}
