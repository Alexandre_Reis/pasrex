﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Grupamento.aspx.cs" Inherits="CadastrosBasicos_GrupamentoAux" %>

<%@ Register Assembly="System.Web, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a" Namespace="System.Web.UI.HtmlControls" TagPrefix="cc2" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxe" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxwgv" %>

<%@ Register Assembly="EntitySpaces.Web" Namespace="EntitySpaces.Web" TagPrefix="cc1" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <link href="~/css/forms.css" rel="stylesheet" type="text/css" />    
    <script type="text/javascript" language="Javascript" src="../js/global.js"></script>    
</head>

<body>
    <form id="form1" runat="server">
    
    <div class="divPanel">
    <table width="100%"><tr><td>
    <div id="container">
    
    <div id="header">
        <asp:Label ID="lblHeader" runat="server" Text="Grupamento Grupo - SubGrupo " />
    </div>
        
    <div id="mainContent">
        
        <div class="linkButton linkButtonNoBorder" >               
           <asp:LinkButton ID="btnColapsa" runat="server" Font-Overline="false" CssClass="btnAdd" 
                OnClientClick="gridCadastro.CollapseAll(); return false;">
                <asp:Literal ID="Literal1" runat="server" Text="Colapsa todas as Linhas"/><div></div>
           </asp:LinkButton>
           
           <asp:LinkButton ID="btnExpande" runat="server" Font-Overline="false" CssClass="btnAdd" 
                OnClientClick=" gridCadastro.ExpandAll(); return false;">
                <asp:Literal ID="Literal2" runat="server" Text="Expanda todas as Linhas"/><div></div>
           </asp:LinkButton>
           
           <asp:LinkButton ID="btnExcel" runat="server" Font-Overline="false" CssClass="btnExcel" 
                OnClick="btnExcel_Click1">
                <asp:Literal ID="Literal5" runat="server" Text="Gerar Excel"/><div></div>
           </asp:LinkButton>
           
        </div>

        <div class="divDataGrid">            
            <dxwgv:ASPxGridView ID="gridCadastro" ClientInstanceName="gridCadastro" runat="server" EnableCallBacks="true" 
            OnDataBound="GridOndataBound" Width="100%">
                                            
            <Columns>                           
                <dxwgv:GridViewCommandColumn VisibleIndex="0" Visible="false" Width="0" ShowClearFilterButton="True">
                    <HeaderTemplate>
                        <dxe:ASPxCheckBox ID="cbAll" ClientInstanceName="cbAll" runat="server"/>
                    </HeaderTemplate>
                </dxwgv:GridViewCommandColumn>

                <dxwgv:GridViewDataColumn FieldName="Id" Visible="false" Width="0" VisibleIndex="0" />
                <dxwgv:GridViewDataColumn FieldName="IdCliente" Width="85" Settings-AllowDragDrop="false" VisibleIndex="1" />
                <dxwgv:GridViewDataColumn FieldName="Descricao2" Caption="Descrição 2" Width="200" Settings-AllowDragDrop="false" VisibleIndex="2" /> 
                
                <%--Grupo 1 Nivel --%>
                <dxwgv:GridViewDataColumn FieldName="Descricao1" Caption="Descrição 1" VisibleIndex="1000" GroupIndex="1" />
                
            </Columns>            

            <GroupSummary>
                <dxwgv:ASPxSummaryItem FieldName="IdCliente" SummaryType="Count" DisplayFormat="Count IdCliente={0:N0}" />
            </GroupSummary>

            <TotalSummary>
                <dxwgv:ASPxSummaryItem FieldName="IdCliente" SummaryType="Count" DisplayFormat="Count IdCliente={0:N0}" />
            </TotalSummary>
                            
            <Settings ShowGroupPanel="true" ShowFooter="true" />
            
            <SettingsText GroupContinuedOnNextPage="Continua na Próxima Página" 
                          EmptyDataRow="0 Registros"  />
            
            <Styles Footer-Font-Bold="true" Cell-Wrap="False" />
            
            <SettingsPager PageSize="8"></SettingsPager>
            <SettingsCommandButton>
                <ClearFilterButton Image-Url="../imagens/funnel--minus.png"/>
                <UpdateButton Image-Url="../imagens/ico_form_ok_inline.gif"/>
                <CancelButton Image-Url="../imagens/ico_form_back_inline.gif"/>
            </SettingsCommandButton>
                                
        </dxwgv:ASPxGridView>
        </div>
          
    </div>
    
    </div>
    </td></tr></table>
    </div>        
           
    <dxwgv:ASPxGridViewExporter ID="gridExport" runat="server" GridViewID="gridCadastro" />
    
    </form>
</body>
</html>