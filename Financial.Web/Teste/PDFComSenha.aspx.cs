﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Text;
using System.Web.UI.HtmlControls;
using System.Collections.Generic;
using System.Drawing;

using Financial.Common;
using Financial.Util;

using DevExpress.Web;

using Financial.Investidor;
using Financial.RendaFixa;
using Financial.Bolsa;
using Financial.BMF;
using Financial.Gerencial;
using Financial.Web.Common;
using Financial.Security;
using Financial.Web.Util;
using Financial.Security.Enums;
using DevExpress.XtraPrinting;

public partial class PDFComSenha : CadastroBasePage {
    protected void EsDSTrader_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e) {
        TraderCollection coll = new TraderCollection();

        coll.Query.OrderBy(coll.Query.Nome.Ascending);
        coll.LoadAll();

        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }

    protected void btnPDF_Click1(object sender, EventArgs e) {
        PdfExportOptions a = new PdfExportOptions();
        string password = "teste";

        a.PasswordSecurityOptions.OpenPassword = password;

        gridExport.WritePdfToResponse("teste.pdf", false, a);
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_RowUpdating(object sender, DevExpress.Web.Data.ASPxDataUpdatingEventArgs e) {
        Trader trader = new Trader();
        int idTrader = Convert.ToInt32(e.Keys[0]);

        if (trader.LoadByPrimaryKey(idTrader)) {
            trader.Nome = Convert.ToString(e.NewValues[TraderMetadata.ColumnNames.Nome]);
            trader.AlgoTrader = Convert.ToString(e.NewValues[TraderMetadata.ColumnNames.AlgoTrader]);
            trader.Save();

            #region Log do Processo
            HistoricoLog historicoLog = new HistoricoLog();
            historicoLog.InsereHistoricoLog(DateTime.Now,
                                            DateTime.Now,
                                            "Cadastro de Trader - Operacao: Update Trader: " + idTrader + UtilitarioWeb.ToString(trader),
                                            HttpContext.Current.User.Identity.Name,
                                            UtilitarioWeb.GetIP(Request),
                                            "",
                                            HistoricoLogOrigem.Outros);
            #endregion
        }

        e.Cancel = true;
        gridCadastro.CancelEdit();
        gridCadastro.DataBind();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_RowInserting(object sender, DevExpress.Web.Data.ASPxDataInsertingEventArgs e) {
        Trader trader = new Trader();

        trader.Nome = Convert.ToString(e.NewValues[TraderMetadata.ColumnNames.Nome]);
        trader.AlgoTrader = Convert.ToString(e.NewValues[TraderMetadata.ColumnNames.AlgoTrader]);
        trader.Save();

        #region Log do Processo
        HistoricoLog historicoLog = new HistoricoLog();
        historicoLog.InsereHistoricoLog(DateTime.Now,
                                        DateTime.Now,
                                        "Cadastro de Trader - Operacao: Insert Trader: " + trader.IdTrader + UtilitarioWeb.ToString(trader),
                                        HttpContext.Current.User.Identity.Name,
                                        UtilitarioWeb.GetIP(Request),
                                        "",
                                        HistoricoLogOrigem.Outros);
        #endregion

        e.Cancel = true;
        gridCadastro.CancelEdit();
        gridCadastro.DataBind();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_CustomCallback(object sender, DevExpress.Web.ASPxGridViewCustomCallbackEventArgs e) {
        if (e.Parameters == "btnDelete") {
            List<object> keyValuesId = gridCadastro.GetSelectedFieldValues("IdTrader");
            for (int i = 0; i < keyValuesId.Count; i++) {
                int idTrader = Convert.ToInt32(keyValuesId[i]);

                Trader trader = new Trader();
                if (trader.LoadByPrimaryKey(idTrader)) {
                    OrdemBolsaCollection ordemBolsaCollection = new OrdemBolsaCollection();
                    ordemBolsaCollection.Query.Where(ordemBolsaCollection.Query.IdTrader.Equal(idTrader));
                    if (ordemBolsaCollection.Query.Load()) {
                        throw new Exception("Trader " + trader.Nome + " não pode ser deletado pois possui vínculos.");
                    }
                    OrdemBMFCollection ordemBMFCollection = new OrdemBMFCollection();
                    ordemBMFCollection.Query.Where(ordemBolsaCollection.Query.IdTrader.Equal(idTrader));
                    if (ordemBMFCollection.Query.Load()) {
                        throw new Exception("Trader " + trader.Nome + " não pode ser deletado pois possui vínculos.");
                    }
                    CalculoGerencialCollection calculoGerencialCollection = new CalculoGerencialCollection();
                    calculoGerencialCollection.Query.Where(calculoGerencialCollection.Query.IdTrader.Equal(idTrader));
                    if (calculoGerencialCollection.Query.Load()) {
                        throw new Exception("Trader " + trader.Nome + " não pode ser deletado pois possui vínculos.");
                    }

                    //
                    Trader traderClone = (Trader)Utilitario.Clone(trader);
                    //

                    trader.MarkAsDeleted();
                    trader.Save();

                    #region Log do Processo
                    HistoricoLog historicoLog = new HistoricoLog();
                    historicoLog.InsereHistoricoLog(DateTime.Now,
                                                    DateTime.Now,
                                                    "Cadastro de Trader - Operacao: Delete Trader: " + idTrader + UtilitarioWeb.ToString(traderClone),
                                                    HttpContext.Current.User.Identity.Name,
                                                    UtilitarioWeb.GetIP(Request),
                                                    "",
                                                    HistoricoLogOrigem.Outros);
                    #endregion
                }
            }
        }

        gridCadastro.DataBind();
        gridCadastro.Selection.UnselectAll();
        gridCadastro.CancelEdit();
    }
}