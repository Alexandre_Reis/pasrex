﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="TesteGridXML1.aspx.cs" Inherits="TesteGridXML1" %>

<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxe" %>    
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxwgv" %>
<%@ Register Assembly="EntitySpaces.Web" Namespace="EntitySpaces.Web" TagPrefix="cc1" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">

    <link href="~/css/forms.css" rel="stylesheet" type="text/css" />    
    <script type="text/javascript" language="Javascript" src="../js/global.js"></script>
    <script type="text/javascript" language="Javascript">
    var popup = false;
    document.onkeydown=onDocumentKeyDown;
    </script>
</head>

<body>
    <form id="form1" runat="server">
        
    <div class="divPanel">
    <table width="100%"><tr><td>
    <div id="container">

    <div id="header">
        <asp:Label ID="lblHeader" runat="server" Text="Cadastro de Carteira/ Grupo Econômico"></asp:Label>
    </div>
           
    <div id="mainContent">
    
            <div class="linkButton">
               <asp:LinkButton ID="btnAdd" runat="server" Font-Overline="false" CssClass="btnAdd" OnClientClick="gridCadastro.AddNewRow(); return false;"><asp:Literal ID="Literal3" runat="server" Text="Novo"/><div></div></asp:LinkButton>
               <asp:LinkButton ID="btnDelete" runat="server" Font-Overline="false" CssClass="btnDelete" OnClientClick=" if (confirm('Tem certeza que quer excluir?')==true) gridCadastro.PerformCallback('btnDelete');return false;"><asp:Literal ID="Literal2" runat="server" Text="Excluir"/><div></div></asp:LinkButton>
            </div>
        
            <div class="divDataGrid">
            <dxwgv:ASPxGridView ID="gridCadastro"  runat="server" 
                ClientInstanceName="gridCadastro" DataSourceID="CarteiraGrupoEconomico" KeyFieldName="CompositeKey"
                EnableCallBacks="true"
                OnRowInserting="gridCadastro_RowInserting" 
                OnRowUpdating="gridCadastro_RowUpdating"                 
                OnCustomCallback="gridCadastro_CustomCallback"
                OnCustomUnboundColumnData="gridCadastro_CustomUnboundColumnData"
                OnCellEditorInitialize="gridCadastro_CellEditorInitialize"
                >
                <Columns>
                        <dxwgv:GridViewCommandColumn VisibleIndex="0" ShowClearFilterButton="True">
                            <HeaderTemplate>
                                <dxe:ASPxCheckBox ID="cbAll" ClientInstanceName="cbAll" Width="10%" runat="server" ToolTip="Seleciona todos os CheckBoxs de todas as páginas" BackColor="white" ClientSideEvents-CheckedChanged="function(s, e) {OnAllCheckedChanged(s, e, gridCadastro);}" OnInit="CheckBoxSelectAll"/>
                            </HeaderTemplate>
                        </dxwgv:GridViewCommandColumn>
                    
                    <dxwgv:GridViewDataTextColumn FieldName="CompositeKey" UnboundType="String" Visible="False"></dxwgv:GridViewDataTextColumn>
                    
                    <dxwgv:GridViewDataSpinEditColumn FieldName="IdCarteira" Caption="IdCarteira" VisibleIndex="1" 
                    Width="7%" PropertiesSpinEdit-NumberFormat="Number" PropertiesSpinEdit-NumberType="Integer" 
                    CellStyle-HorizontalAlign="Right" HeaderStyle-HorizontalAlign="Right">
                     
                    </dxwgv:GridViewDataSpinEditColumn>
                    
                    
                    <dxwgv:GridViewDataSpinEditColumn FieldName="IdGrupoEconomico" Caption="IdGrupo Econômico" VisibleIndex="2" 
                    Width="7%" PropertiesSpinEdit-NumberFormat="Number" PropertiesSpinEdit-NumberType="Integer" 
                    CellStyle-HorizontalAlign="Right" HeaderStyle-HorizontalAlign="Right">
                        
                    </dxwgv:GridViewDataSpinEditColumn>

                    <dxwgv:GridViewDataComboBoxColumn FieldName="IdCarteira" Caption="Carteira" VisibleIndex="3" Width="35%">
                    <PropertiesComboBox DataSourceID="EsDSCarteira" TextField="Nome" ValueField="IdCarteira"></PropertiesComboBox>
                    </dxwgv:GridViewDataComboBoxColumn>
                    
                    <dxwgv:GridViewDataComboBoxColumn FieldName="IdGrupoEconomico" Caption="Grupo Econômico" VisibleIndex="4" Width="35%">
                    <PropertiesComboBox DataSourceID="EsDSGrupoEconomico" TextField="Nome" ValueField="IdGrupoEconomico"></PropertiesComboBox>
                    </dxwgv:GridViewDataComboBoxColumn>
                                                                                                   
                </Columns>
                
                <Settings ShowStatusBar="Hidden" />
                <SettingsCommandButton>
                    <ClearFilterButton>
                        <Image Url="~/imagens/funnel--minus.png">
                        </Image>
                    </ClearFilterButton>
                    <UpdateButton>
                        <Image Url="~/imagens/ico_form_ok_inline.gif">
                        </Image>
                    </UpdateButton>
                    <CancelButton>
                        <Image Url="~/imagens/ico_form_back_inline.gif">
                        </Image>
                    </CancelButton>
                </SettingsCommandButton>
            </dxwgv:ASPxGridView>
            </div>                        
    </div>
    </div>
    </td></tr></table>
    </div>
    
    <asp:XmlDataSource ID="CarteiraGrupoEconomico" runat="server" DataFile="~/App_Data/CarteiraGrupoEconomico.xml"></asp:XmlDataSource>    
    <cc1:esDataSource ID="EsDSCarteira" runat="server" OnesSelect="EsDSCarteira_esSelect" LowLevelBind="true" />
    <cc1:esDataSource ID="EsDSGrupoEconomico" runat="server" OnesSelect="EsDSGrupoEconomico_esSelect" LowLevelBind="true" />
    </form>
</body>
</html>