﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class Teste_SessionUser : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        System.Collections.Generic.List<string> Sessions = (System.Collections.Generic.List<string>)Application["Sessions"];
        for (int i = 0; i < Sessions.Count; i++)
        {
            string a = Sessions[i];
            string[] b = a.Split('&');
            Response.Write("Sessão:" + b[0] + " IP:" + b[1] + " usuario:" + b[2] + "<BR>");
        }
    }
}
