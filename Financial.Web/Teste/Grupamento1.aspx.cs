﻿using Financial.Web.Common;
using Financial.Bolsa;
using System.Data;
using System;
using DevExpress.Web;
using System.Web.UI.WebControls;
using DevExpress.Utils;
using DevExpress.Data;
using Financial.Captacao.Custom;
using System.Web;
using System.Web.UI;
using System.Collections.Generic;

public partial class CadastrosBasicos_GrupamentoAux1 : ConsultaBasePage {
    // guarda os dados
    private DataTable data = null;
    
    /// <summary>
    /// Pre-Render do Grid. Faz a Consulta e Salva na Session
    /// 3 evento a ser executado
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Grid_OnPreRender(object sender, EventArgs e) {
        this.data = null;

        this.data = this.CreateData();

        // Se DataSource tem 0 linhas some com o Otal Clientes
        //this.gridCadastro.TotalSummary["IdCliente"].DisplayFormat = this.data.Rows.Count == 0 ? "" : "Total Clientes={0:N0}";
                                  
        this.RemoveColumns();
        this.AddColumns(this.data);
        this.AddGroupTotalSummary(this.data);
        //
        this.gridCadastro.DataSource = this.data;
        this.gridCadastro.KeyFieldName = "Id";
        //
        this.gridCadastro.DataBind();

        //
        this.ControlaBotaoColapsaExpande();
        //
        Session["DataSourceGroup"] = this.data;            
    }
    
    /// <summary>
    /// 
    /// </summary>
    private void ControlaBotaoColapsaExpande() {
        #region Aparece com os botões colapsa/expande/excel se tem dados no grid
        Control c1 = this.FindControl("btnColapsa");
        c1.Visible = true;

        Control c2 = this.FindControl("btnExpande");
        c2.Visible = true;

        Control c3 = this.FindControl("btnExcel");
        c3.Visible = true;
       
        #endregion
    }

    // Remove da Session - 2 evento a executar
    protected void Page_PreRender() {
        if (Session["DataSourceGroup"] != null) {
            Session.Remove("DataSourceGroup");            
        }
    }
               
    // 1 evento a executar - quando clico para expandir só entra aqui.
    new protected void Page_Load(object sender, EventArgs e) {
        base.Page_Load(sender, e);

        if (Session["DataSourceGroup"] != null) {
            this.gridCadastro.DataSource = Session["DataSourceGroup"];
            this.gridCadastro.KeyFieldName = "Id";
            this.gridCadastro.DataBind();
        }
    }

    #region CallBack Excel
    /// <summary>
    /// 
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void callbackExporta_Callback(object source, DevExpress.Web.CallbackEventArgs e) {
        e.Result = "";

        Response.BufferOutput = true;

        Session["streamGridExport"] = this.gridExport;

        // TODO mudar isso se mudar o nome da pagina
        string newLocation = HttpContext.Current.Request.Url.AbsolutePath.ToLower().Replace("grupamento1.aspx", "exibegrupamentoexcel.aspx");
        e.Result = "redirect:" + newLocation;
    }    
    #endregion

    #region Adiciona/Remove Colunas
    /// <summary>
    /// Adiciona Colunas Valor Dinamicamente
    /// </summary>
    /// <param name="d"></param>
    private void AddColumns(DataTable d) {

        int colunasValor = d.Columns.Count - 5;

        for (int i = 1; i <= colunasValor; i++) {
            GridViewDataSpinEditColumn g = new GridViewDataSpinEditColumn();
            g.Name = "Valor" + i;
            g.FieldName = "Valor" + i;
            //g.Caption = "Valor" + i;
            //
            g.Caption = d.Columns[i + 4].Caption;
            //
            g.VisibleIndex = i + 1;
            if (colunasValor >= 15) {
                g.Width = Unit.Point(70);
            }
            g.Settings.AllowDragDrop = DefaultBoolean.False;
            g.HeaderStyle.HorizontalAlign = HorizontalAlign.Right;
            g.PropertiesSpinEdit.DisplayFormatString = "{0:#,##0.00;(#,##0.00);0.00}";

            this.gridCadastro.Columns.Add(g);
        }

        // Adiciona ScroolBar se colunas > 15        
        this.gridCadastro.Settings.ShowHorizontalScrollBar = colunasValor >= 15;
    }

    /// <summary>
    /// Adiciona GroupSummary e TotalSummary da coluna Valor Dinamicamente
    /// </summary>
    /// <param name="d"></param>
    private void AddGroupTotalSummary(DataTable d) {

        int colunasValor = d.Columns.Count - 5;

        #region Adiciona Group Summary
        for (int j = 1; j <= colunasValor; j++) {
            ASPxSummaryItem item = new ASPxSummaryItem();
            //
            item.FieldName = "Valor" + j;
            item.SummaryType = SummaryItemType.Sum;
            //

            //string format = "Total: " +d.Columns[j + 4].Caption;
            item.DisplayFormat = "{0:#,##0.00;(#,##0.00);0.00}";
            //
            item.ShowInGroupFooterColumn = "Valor" + j;
            //
            this.gridCadastro.GroupSummary.Add(item);
        }
        #endregion

        #region Adiciona Total Summary
        for (int j = 1; j <= colunasValor; j++) {
            ASPxSummaryItem item = new ASPxSummaryItem();
            //
            item.FieldName = "Valor" + j;
            item.SummaryType = SummaryItemType.Sum;
            item.DisplayFormat = "{0:#,##0.00;(#,##0.00);0.00}";

            this.gridCadastro.TotalSummary.Add(item);
        }
        #endregion
    }

    /// <summary>
    /// Remove as colunas Valor se existirem
    /// </summary>
    private void RemoveColumns() {
        // Remove as colunas Valor se existirem
        for (int i = this.gridCadastro.Columns.Count - 1; i >= 0; i--) {

            if (this.gridCadastro.Columns[i].Name.Contains("Valor")) {
                this.gridCadastro.Columns[i].Width = Unit.Point(0); // zera a largura da coluna para não dar problemas de width numa segunda consulta
                // remove
                this.gridCadastro.Columns.RemoveAt(i);
            }
        }

        // Remove as colunas Valor GroupSummary se existirem
        for (int i = this.gridCadastro.GroupSummary.Count - 1; i >= 0; i--) {
            if (this.gridCadastro.GroupSummary[i].FieldName.Contains("Valor")) {
                this.gridCadastro.GroupSummary.RemoveAt(i);
            }
        }

        // Remove as colunas TotalSummary se existirem
        for (int i = this.gridCadastro.TotalSummary.Count - 1; i >= 0; i--) {
            if (this.gridCadastro.TotalSummary[i].FieldName.Contains("Valor")) {
                this.gridCadastro.TotalSummary.RemoveAt(i);
            }
        }
    }     
    #endregion

    /// <summary>
    /// DataTable no Formato Id, Descricao1, Descricao2, Descricao3 IdCliente, Valor1, Valor2, ...
    /// </summary>
    /// <returns></returns>
    private DataTable CreateData() {
        DateTime dataInicio = Convert.ToDateTime(this.textDataInicio.Text);
        DateTime dataFim = Convert.ToDateTime(this.textDataFim.Text);
        //                
        ConsultaCaptacao.TipoColunaData tipo = (ConsultaCaptacao.TipoColunaData)Enum.Parse(typeof(ConsultaCaptacao.TipoColunaData), this.dropTipoPeriodo.SelectedItem.Value.ToString());            
        //
        ConsultaCaptacao consultaCaptacao = new ConsultaCaptacao(dataInicio, dataFim, tipo, HttpContext.Current.User.Identity.Name);
        DataTable dt = consultaCaptacao.RetornaMovimentoCliente(ConsultaCaptacao.TipoInformacao.Aportes, ConsultaCaptacao.TipoAgrupamento.TipoCliente);

        return dt;
    }

    new protected void btnRun_Click(object sender, EventArgs e) {

        #region Campos obrigatórios
        List<Control> controles = new List<Control>(new Control[] { 
            this.dropTipoPeriodo, this.textDataInicio, this.textDataFim });

        if (base.TestaObrigatorio(controles) != "") {
            throw new Exception("Campos com * são obrigatórios!");
        }
        #endregion

        DateTime dataInicio = Convert.ToDateTime(this.textDataInicio.Text);
        DateTime dataFim = Convert.ToDateTime(this.textDataFim.Text);

        if (dataInicio > dataFim) {
            throw new Exception("Data Início maior que Data Fim.");
        }
        //
        this.gridCadastro.Visible = true; // torna Vísivel - vai acionar o metodo Grid_OnPreRender
    }
}