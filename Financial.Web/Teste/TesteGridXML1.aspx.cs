﻿using System;
using System.Collections;
using System.IO;
using System.Xml;
using DevExpress.Web;
using System.Web.UI.WebControls;
using System.Collections.Generic;
using Financial.Web.Common;
using Financial.Security;
using Financial.Fundo;
using Financial.Investidor;
using Financial.Investidor.Enums;
using Financial.Common;
using System.Drawing;

public partial class TesteGridXML1 : CadastroBasePage {
    protected readonly static object lockObject = new object();

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_CellEditorInitialize(object sender, ASPxGridViewEditorEventArgs e) {
        base.gridCadastro_CellEditorInitialize(sender, e);

        e.Editor.ReadOnly = false;

        if (e.Column.Caption == "IdCarteira" || e.Column.Caption == "IdGrupo Econômico") {
            e.Editor.Enabled = false;
            e.Editor.BackColor = Color.FromName("#EBEBEB");
            e.Editor.ReadOnly = true;
        }
    }


    #region DataSources
    protected void EsDSCarteira_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e) {
        Usuario usuario = new Usuario();
        usuario.BuscaUsuario(Context.User.Identity.Name);
        int idUsuario = usuario.IdUsuario.Value;

        CarteiraQuery carteiraQuery = new CarteiraQuery("C");
        ClienteQuery clienteQuery = new ClienteQuery("L");
        PermissaoClienteQuery permissaoClienteQuery = new PermissaoClienteQuery("P");

        carteiraQuery.Select(carteiraQuery, clienteQuery.IdTipo);
        carteiraQuery.InnerJoin(clienteQuery).On(clienteQuery.IdCliente == carteiraQuery.IdCarteira);
        carteiraQuery.InnerJoin(permissaoClienteQuery).On(permissaoClienteQuery.IdCliente == clienteQuery.IdCliente);
        carteiraQuery.Where(permissaoClienteQuery.IdUsuario.Equal(idUsuario),
                            clienteQuery.TipoControle.NotEqual((byte)TipoControleCliente.ApenasCotacao));
        carteiraQuery.OrderBy(carteiraQuery.Apelido.Ascending);

        CarteiraCollection coll = new CarteiraCollection();
        coll.Load(carteiraQuery);

        carteiraQuery = new CarteiraQuery("C");
        clienteQuery = new ClienteQuery("L");

        carteiraQuery.Select(carteiraQuery, clienteQuery.IdTipo);
        carteiraQuery.InnerJoin(clienteQuery).On(clienteQuery.IdCliente == carteiraQuery.IdCarteira);
        carteiraQuery.Where(clienteQuery.TipoControle.Equal((byte)TipoControleCliente.ApenasCotacao));
        carteiraQuery.OrderBy(carteiraQuery.Apelido.Ascending);

        CarteiraCollection coll2 = new CarteiraCollection();
        coll2.Load(carteiraQuery);

        coll.Combine(coll2);

        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }

    protected void EsDSGrupoEconomico_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e) {
        GrupoEconomicoCollection coll = new GrupoEconomicoCollection();
        coll.Query.Select(coll.Query.IdGrupo.As("IdGrupoEconomico"), coll.Query.Nome);
        coll.LoadAll();
        //
        e.Collection = coll;
    }

    #endregion

    protected void gridCadastro_CustomUnboundColumnData(object sender, DevExpress.Web.ASPxGridViewColumnDataEventArgs e) {
        if (e.Column.FieldName == "CompositeKey") {
            //Compose a primary key value
            string idCarteira = Convert.ToString(e.GetListSourceFieldValue("IdCarteira"));
            string idGrupoEconomico = Convert.ToString(e.GetListSourceFieldValue("IdGrupoEconomico"));
            e.Value = idCarteira + "|" + idGrupoEconomico;
        }
    }

    /// <summary>
    /// Delete
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_CustomCallback(object sender, DevExpress.Web.ASPxGridViewCustomCallbackEventArgs e) {
        if (e.Parameters == "btnDelete") {
            List<object> keyValuesId = gridCadastro.GetSelectedFieldValues("IdCarteira");
            List<object> keyValuesData = gridCadastro.GetSelectedFieldValues("IdGrupoEconomico");

            for (int i = 0; i < keyValuesData.Count; i++) {
                int idCarteiraDelete = Convert.ToInt32(keyValuesId[i]);
                int IdGrupoEconomicoDelete = Convert.ToInt32(keyValuesData[i]);

                string search = string.Format("Lista/Carteira[@IdCarteira='{0}' and @IdGrupoEconomico='{1}']", idCarteiraDelete, IdGrupoEconomicoDelete);
                
                XmlNode node = this.CarteiraGrupoEconomico.GetXmlDocument().SelectSingleNode(search);
                XmlNode parent = node.ParentNode;
                parent.RemoveChild(node);

                this.SaveXml(this.CarteiraGrupoEconomico);
            }
        }

        gridCadastro.DataBind();
        gridCadastro.Selection.UnselectAll();
        gridCadastro.CancelEdit();
    }

    /// <summary>
    /// Insert no arquivo XML
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_RowInserting(object sender, DevExpress.Web.Data.ASPxDataInsertingEventArgs e) {
        //int maxID = 0;
        //List<int> faixas = new List<int>();
        //ASPxGridView grid = (ASPxGridView)sender;

        //foreach (XmlNode selectedNode in XmlFaixaLiquidez.GetXmlDocument().SelectNodes("Lista/Faixa")) {
        //    int idFaixa = int.Parse(selectedNode.Attributes["IdFaixa"].Value);
        //    faixas.Add(int.Parse(selectedNode.Attributes["MaxDias"].Value));
        //    if (idFaixa > maxID)
        //        maxID = idFaixa;
        //}

        //foreach (int maxDias in faixas) {
        //    if (Convert.ToInt32(e.NewValues["MaxDias"].ToString()) == maxDias) {
        //        grid.JSProperties["cpException"] = string.Format("A faixa de liquidez até '{0}' dias já está cadastrada.", maxDias);
        //        e.Cancel = true;
        //        grid.CancelEdit();
        //        return;
        //    }
        //}

        //e.NewValues.Insert(0, "IdFaixa", maxID + 1);
        
        //XmlNode node = XmlFaixaLiquidez.GetXmlDocument().CreateElement("Faixa");
        //foreach (DictionaryEntry entry in e.NewValues) {
        //    XmlAttribute attribute = XmlFaixaLiquidez.GetXmlDocument().CreateAttribute(entry.Key.ToString());
        //    attribute.Value = entry.Value.ToString();
        //    node.Attributes.Append(attribute);
        //}
        //XmlFaixaLiquidez.GetXmlDocument().SelectSingleNode("Lista").AppendChild(node);
        //SaveXml(XmlFaixaLiquidez);

        //e.Cancel = true;

        //grid.CancelEdit();

        
        XmlNode node = this.CarteiraGrupoEconomico.GetXmlDocument().CreateElement("Carteira");
        foreach (DictionaryEntry entry in e.NewValues) {
            XmlAttribute attribute = this.CarteiraGrupoEconomico.GetXmlDocument().CreateAttribute(entry.Key.ToString());
            attribute.Value = entry.Value.ToString();
            node.Attributes.Append(attribute);
        }
        this.CarteiraGrupoEconomico.GetXmlDocument().SelectSingleNode("Lista").AppendChild(node);
        this.SaveXml(CarteiraGrupoEconomico);

        e.Cancel = true;
        gridCadastro.CancelEdit();
        gridCadastro.DataBind();
    }

    /// <summary>
    /// Salva dados no arquivo XML
    /// </summary>
    /// <param name="xmlDataSource"></param>
    protected void SaveXml(XmlDataSource xmlDataSource) {
        lock (lockObject) {
            xmlDataSource.Save();
        }
    }

    /// <summary>
    /// Update no arquivo XML
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_RowUpdating(object sender, DevExpress.Web.Data.ASPxDataUpdatingEventArgs e) {
        //ASPxGridView grid = (ASPxGridView)sender;

        //int faixa = int.Parse(e.Keys["IdFaixa"].ToString());

        //XmlNode node = XmlFaixaLiquidez.GetXmlDocument().SelectSingleNode(string.Format("Lista/Faixa[@IdFaixa='{0}']", faixa));
        //if (node != null) {
        //    List<int> faixas = new List<int>();
        //    foreach (XmlNode selectedNode in XmlFaixaLiquidez.GetXmlDocument().SelectNodes("Lista/Faixa")) {
        //        faixas.Add(int.Parse(selectedNode.Attributes["MaxDias"].Value));
        //    }

        //    foreach (int maxDias in faixas) {
        //        if (Convert.ToInt32(e.NewValues["MaxDias"].ToString()) == maxDias) {
        //            grid.JSProperties["cpException"] = string.Format("A faixa de liquidez até '{0}' dias já está cadastrada.", maxDias);
        //            e.Cancel = true;
        //            grid.CancelEdit();
        //            return;
        //        }
        //    }
        //    foreach (DictionaryEntry entry in e.NewValues)
        //        node.Attributes[entry.Key.ToString()].Value = entry.Value.ToString();

        //    SaveXml(XmlFaixaLiquidez);
        //}
        //else
        //    grid.JSProperties["cpException"] = string.Format("A faixa = '{0}' foi apagada por outro usuário.", faixa);

        //e.Cancel = true;

        //grid.CancelEdit();

        string compositeKey = e.Keys["CompositeKey"].ToString();
        string[] keys = compositeKey.Split('|');

        int idCarteiraUpdate = Convert.ToInt32(keys[0]);
        int IdGrupoEconomicoUpdate = Convert.ToInt32(keys[1]);

        string search = string.Format("Lista/Carteira[@IdCarteira='{0}' and @IdGrupoEconomico='{1}']", idCarteiraUpdate, IdGrupoEconomicoUpdate);
            
        XmlNode node = this.CarteiraGrupoEconomico.GetXmlDocument().SelectSingleNode(search);

        foreach (DictionaryEntry entry in e.NewValues) {
            node.Attributes[entry.Key.ToString()].Value = entry.Value.ToString();
        }

        this.SaveXml(this.CarteiraGrupoEconomico);


        e.Cancel = true;
        gridCadastro.CancelEdit();
        gridCadastro.DataBind();
    }
}