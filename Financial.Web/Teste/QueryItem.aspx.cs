using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Financial.Investidor;
using Financial.Fundo;
using System.Collections.Generic;
using EntitySpaces.Interfaces;

public partial class Teste_QueryItem : System.Web.UI.Page {
    protected void Page_Load(object sender, EventArgs e) {

        List<DateTime> data = new List<DateTime>( new DateTime[] { new DateTime(2008,1,2), new DateTime(2010,9,24), new DateTime(2013,1,1) } );

        ClienteQuery clienteQuery = new ClienteQuery("C");
        PosicaoFundoHistoricoQuery posicaoFundoHistoricoQuery = new PosicaoFundoHistoricoQuery("P");
        CarteiraQuery carteiraQuery = new CarteiraQuery("A");
        TipoClienteQuery tipoClienteQuery = new TipoClienteQuery("T");

        clienteQuery.Select(tipoClienteQuery.Descricao,
                            carteiraQuery.IdCarteira,
                            carteiraQuery.Nome.As("NomeCarteira"),
                            clienteQuery.IdCliente,
                            clienteQuery.Nome);

        clienteQuery.InnerJoin(tipoClienteQuery).On(tipoClienteQuery.IdTipo == clienteQuery.IdTipo);
        clienteQuery.InnerJoin(posicaoFundoHistoricoQuery).On(posicaoFundoHistoricoQuery.IdCliente == clienteQuery.IdCliente);
        clienteQuery.InnerJoin(carteiraQuery).On(carteiraQuery.IdCarteira == posicaoFundoHistoricoQuery.IdCarteira);

        clienteQuery.es.DefaultConjunction = EntitySpaces.Interfaces.esConjunction.Or;

        // Funcionando
        for (int i = 0; i < data.Count; i++) {
            clienteQuery.Where(posicaoFundoHistoricoQuery.DataHistorico == data[i]);
        }

        //esDynamicQuery a = new esDynamicQuery();

        //for (int i = 0; i < data.Count; i++) {
        //    a.Where(posicaoFundoHistoricoQuery.DataHistorico == data[i]);
        //}
        //clienteQuery.Where = a;

        clienteQuery.GroupBy(tipoClienteQuery.Descricao,
                            carteiraQuery.IdCarteira,
                            carteiraQuery.Nome,
                            clienteQuery.IdCliente,
                            clienteQuery.Nome);

        ClienteCollection clienteCollection = new ClienteCollection();
        clienteCollection.Load(clienteQuery);
    }
}
