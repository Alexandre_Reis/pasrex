using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Financial.Interfaces.Import.Mellon;
using Financial.Investidor;
using System.Collections.Generic;
using System.Diagnostics;
using Financial.Fundo;
using Financial.Investidor.Controller;
using Financial.Interfaces.Import.Bolsa;
using System.IO;
using Financial.CRM;
using System.Xml;
using Financial.Export;
using Financial.InvestidorCotista;
using System.Text;
using Financial.RendaFixa;
using Financial.Interfaces.Export;
using Financial.Common.Enums;
using Financial.WebConfigConfiguration;
using Financial.RendaFixa.Enums;

public partial class Teste_TesteImportaCliente : ImportacaoBasePage
{

    protected void Button1_Click(object sender, EventArgs e)
    {
        XMLCliente xmlCliente = new XMLCliente();

        TextReader reader = new StreamReader(@"C:\ExportacaoCliente.xml");

        xmlCliente.ImportaArquivoXML(reader);
    }
    protected void Button2_Click(object sender, EventArgs e)
    {
        Itau itau = new Itau();

        //teste cac_xml
        //XmlDocument xmlDoc = itau.RetornaCac_Xml(53989);
        XmlDocument xmlDoc = itau.RetornaCac_Xml(99993);

        StreamWriter streamWriter = new StreamWriter(@"c:\temp\CacXml.xml", false, Encoding.UTF8);

        xmlDoc.Save(streamWriter);

        ////teste arq_eop
        //OrdemCotista ordemcotista = new OrdemCotista();

        ////ordemcotista.LoadByPrimaryKey(2);
        //ordemcotista.LoadByPrimaryKey(228);

        //xmlDoc = itau.RetornaEop_Xml(ordemcotista);

        //streamWriter = new StreamWriter(@"c:\temp\ArqEopXml.xml", false, Encoding.UTF8);

        //xmlDoc.Save(streamWriter);

        ////teste cci_xml
        //ContaCorrente contaCorrente = new ContaCorrente();

        ////contaCorrente.LoadByPrimaryKey(2054);
        //contaCorrente.LoadByPrimaryKey(559);

        ////xmlDoc = itau.RetornaCci_XML(53989, contaCorrente, "I");
        //xmlDoc = itau.RetornaCci_XML(111, contaCorrente, "I");

        //streamWriter = new StreamWriter(@"c:\temp\CciXml.xml", false, Encoding.UTF8);

        ////teste cco_xml
        //xmlDoc = itau.RetornaCco_XML(1000048214, "I");

        //streamWriter = new StreamWriter(@"c:\temp\CcoXml.xml", false, Encoding.UTF8);

        //xmlDoc.Save(streamWriter);

        //xmlDoc = itau.RetornaUpc_XML(1000048214, "I");

        //streamWriter = new StreamWriter(@"c:\temp\UpcXml.xml", false, Encoding.UTF8);

        //xmlDoc.Save(streamWriter);
    }

    protected void Button3_Click(object sender, EventArgs e)
    {
        int idCarteira = 5051;

        CalculoMedida calculoMedida = new CalculoMedida(idCarteira, (int)ListaIndiceFixo.CDI);

        List<CalculoMedida.FrequenciaRetorno> listaFrequenciaRetorno =
            calculoMedida.RetornaListaFrequenciaRetorno(Convert.ToDateTime("2013-12-31"), 21, 8, Convert.ToDateTime("2013-01-01"));

    }

    protected void Button4_Click(object sender, EventArgs e)
    {
        int idCarteira = 292;

        CalculoMedida calculoMedida = new CalculoMedida(idCarteira, (int)ListaIndiceFixo.CDI);
        
        string diretorio = DiretorioAplicacao.DiretorioBaseAplicacao;
        XmlDocument xmlDoc = new XmlDocument();
        xmlDoc.Load(diretorio + "/App_Data/FaixaLiquidez.xml");
        
        List<int> faixas = new List<int>();
        foreach (XmlNode selectedNode in xmlDoc.SelectNodes("Lista/Faixa"))
        {
            faixas.Add(int.Parse(selectedNode.Attributes["MaxDias"].Value));
        }
        faixas.Sort();

        List<Financial.Fundo.CalculoMedida.Liquidez> listaLiquidez =
            calculoMedida.RetornaListaLiquidez(Convert.ToDateTime("2010-03-16"), faixas);

        string desc = ClasseRendaFixaDescricao.RetornaDescricao(15);
        desc = ClasseRendaFixaDescricao.RetornaStringValue(15);
    }
}
