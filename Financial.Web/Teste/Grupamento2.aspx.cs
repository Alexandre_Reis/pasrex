﻿using Financial.Web.Common;
using Financial.Bolsa;
using System.Data;
using System;
using DevExpress.Web;
using System.Web.UI.WebControls;
using DevExpress.Utils;
using DevExpress.Data;
using Financial.Captacao.Custom;
using System.Web;
using System.Collections.Generic;
using Financial.Investidor;
using Financial.Investidor.Enums;
using Financial.CRM;
using Financial.Security;
using Financial.InvestidorCotista;
using Financial.Fundo;
using Financial.CRM.Enums;
using Financial.InvestidorCotista.Enums;
using Financial.Fundo.Enums;
using EntitySpaces.Interfaces;
using Financial.Util;

public partial class CadastrosBasicos_GrupamentoAux2 : ConsultaBasePage {
    private int k = 0;

    new protected void Page_Load(object sender, EventArgs e) {
        base.Page_Load(sender, e);

        if (this.k == 0) {

            DateTime dataInicio = new DateTime(2011, 07, 01);
            DateTime dataFim = new DateTime(2013, 02, 28);
            
            ConsultaCaptacao consultaCaptacao1 = new ConsultaCaptacao(dataInicio, dataFim, ConsultaCaptacao.TipoColunaData.Mes, HttpContext.Current.User.Identity.Name);
            BindingDSCollection b1 = consultaCaptacao1.RetornaMovimentoTipoCliente(ConsultaCaptacao.TipoInformacao.Aportes);

            
            //foreach (esColumnMetadata col in b1.es.Meta.Columns.) {
            //    obj.GetColumn(col.Name) + "; ");
            //}


            this.AddColumns(b1);
            this.AddGroupTotalSummary(b1);
        }
        this.k++;
    }

    #region Classe para Binding
    public class BindingDSCollection : AtivoBolsaCollection {

        /// <summary>
        /// Adiciona uma Coluna Nova na Collection
        /// </summary>
        /// <param name="columnName"></param>
        /// <param name="typeColumn"></param>
        public void AddColumn(string columnName, Type typeColumn) {
            if (this.Table != null && !this.Table.Columns.Contains(columnName)) {
                this.Table.Columns.Add(columnName, typeColumn);
            }
        }
    }
    #endregion
            
    protected void EsDSGrupamento_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e) {
        //BindingDSCollection b = new BindingDSCollection();
        //b.CreateColumnsForBinding();
        //b.AddColumn("Id", typeof(System.String));
        //b.AddColumn("Value", typeof(System.String));

        //AtivoBolsa a = b.AddNew();
        //a.SetColumn("Id", "1");
        //a.SetColumn("Value", "Posição Bolsa");

        DateTime dataInicio = new DateTime(2011, 07, 01);
        DateTime dataFim = new DateTime(2013, 02, 28);

        ConsultaCaptacao consultaCaptacao = new ConsultaCaptacao(dataInicio, dataFim, ConsultaCaptacao.TipoColunaData.Mes, HttpContext.Current.User.Identity.Name);
        BindingDSCollection b = consultaCaptacao.RetornaMovimentoTipoCliente(ConsultaCaptacao.TipoInformacao.Aportes);

        //if (!Page.IsPostBack) {
        //    this.AddColumns(b);
        //    this.AddGroupTotalSummary(b);
        //}

        //if (k!=0) {
        //    this.AddColumns(b);
        //    this.AddGroupTotalSummary(b);
        //}

        //k++;

        e.Collection = b;
    }

    /// <summary>
    /// Adiciona Colunas Valor Dinamicamente
    /// </summary>
    /// <param name="d"></param>
    private void AddColumns(BindingDSCollection d) {

        int colunasValor = d.Count - 5;
                       
        for (int i = 1; i <= colunasValor; i++) {
            GridViewDataSpinEditColumn g = new GridViewDataSpinEditColumn();
            g.FieldName = "Valor" + i;
            //g.Caption = "Valor" + i;
            //
            //g.Caption = d.Columns[i + 4].Caption;

            g.Caption = "rever";

            //
            g.VisibleIndex = i + 1;
            if (colunasValor >= 15) {
                g.Width = Unit.Point(70);
            }
            g.Settings.AllowDragDrop = DefaultBoolean.False;
            g.HeaderStyle.HorizontalAlign = HorizontalAlign.Right;
            g.PropertiesSpinEdit.DisplayFormatString = "{0:#,##0.00;(#,##0.00);0.00}";

            this.gridCadastro.Columns.Add(g);
        }

        // Adiciona ScroolBar se colunas > 15
        if (colunasValor >= 15) {
            this.gridCadastro.Settings.HorizontalScrollBarMode = DevExpress.Web.ScrollBarMode.Visible;
        }
    }

    /// <summary>
    /// Adiciona GroupSummary e TotalSummary da coluna Valor Dinamicamente
    /// </summary>
    /// <param name="d"></param>
    private void AddGroupTotalSummary(BindingDSCollection d) {

        int colunasValor = d.Count - 5;

        #region Adiciona Group Summary
        for (int j = 1; j <= colunasValor; j++) {
            ASPxSummaryItem item = new ASPxSummaryItem();
            //
            item.FieldName = "Valor" + j;
            item.SummaryType = SummaryItemType.Sum;
            //

            //string format = "Total: " +d.Columns[j + 4].Caption;
            string format = "Total";
            //
            item.DisplayFormat = format + "={0:#,##0.00;(#,##0.00);0.00}";
            //
            item.ShowInGroupFooterColumn = "Valor" + j;
            //
            this.gridCadastro.GroupSummary.Add(item);
        }
        #endregion

        #region Adiciona Total Summary
        for (int j = 1; j <= colunasValor; j++) {
            ASPxSummaryItem item = new ASPxSummaryItem();
            //
            item.FieldName = "Valor" + j;            
            item.SummaryType = SummaryItemType.Sum;
            item.DisplayFormat = "Total={0:#,##0.00;(#,##0.00);0.00}";
                       
            this.gridCadastro.TotalSummary.Add(item);            
        }
        #endregion
    }

    ///// <summary>
    ///// DataTable no Formato Id, Descricao1, Descricao2, Descricao3 IdCliente, Valor1, Valor2, ...
    ///// </summary>
    ///// <returns></returns>
    //private DataTable CreateData() {
    //    DateTime dataInicio = new DateTime(2011, 07, 01);
    //    DateTime dataFim = new DateTime(2013, 02, 28);

    //    ConsultaCaptacao consultaCaptacao = new ConsultaCaptacao(dataInicio, dataFim, ConsultaCaptacao.TipoColunaData.Mes, HttpContext.Current.User.Identity.Name);
    //    DataTable dt = consultaCaptacao.RetornaMovimentoTipoCliente(ConsultaCaptacao.TipoInformacao.Aportes);

    //    return dt;
    //}

    /* -------------------------------------------------------------------------------------- */
    //------------------------------------------------------------------------------------------------
    public class ConsultaCaptacao {
        #region Atributos
        public enum TipoColunaData {
            Mes = 1,
            Trimestre = 2,
            Semestre = 3,
            Ano = 4
        }

        public enum TipoInformacao {
            Aportes = 1,
            Resgates = 2,
            CaptacoesLiquidas = 3,
            Volume = 10,
            ReceitaAdm = 100,
            ReceitaPfee = 101,
            ReceitaRebateAdm = 102,
            ReceitaRebatePfee = 103,
            ReceitaRebateCorretagem = 104,
            ReceitaTotal = 110
        }

        public enum TipoAgrupamento {
            TipoCliente = 1,
            AreaGeografica = 2,
            FaixaEtaria = 3,
            DiasCadastro = 4,
            Officer = 5,
            GestorProduto = 6,
            DistribuidorProduto = 7
        }

        private string login;
        private DateTime dataInicio;
        private DateTime dataFim;

        private TipoColunaData colunaData;
        private TipoAgrupamento agrupamento;
        #endregion

        #region Construtor
        public ConsultaCaptacao(DateTime dataInicio, DateTime dataFim, TipoColunaData colunaData, string login) {
            this.dataInicio = dataInicio;
            this.dataFim = dataFim;
            this.colunaData = colunaData;
            this.login = login;
        }
        #endregion

        public BindingDSCollection RetornaMovimentoTipoCliente(TipoInformacao tipoInformacao) {
            BindingDSCollection lista = new BindingDSCollection();
            //
            lista.CreateColumnsForBinding();
            lista.AddColumn("Id", typeof(int)); // Chave do Grid
            lista.AddColumn("Descricao1", typeof(string));
            lista.AddColumn("Descricao2", typeof(string));
            lista.AddColumn("Descricao3", typeof(string));
            lista.AddColumn("IdCliente", typeof(int));
            //
            List<DateTime> listaDatas = this.RetornaListaDatas();

            for (int i = 1; i <= listaDatas.Count; i++) {
                lista.AddColumn("Valor" + i, typeof(decimal));
            }
             
            ClienteCollection clienteCollection = new ClienteCollection();

            List<string> listaExcluir = new List<string>();

            #region Tratamento para OperacaoFundo
            #region Consulta
            ClienteQuery clienteQuery = new ClienteQuery("C");
            UsuarioQuery usuarioQuery = new UsuarioQuery("U");
            PermissaoClienteQuery permissaoClienteQuery = new PermissaoClienteQuery("P");
            OperacaoFundoQuery operacaoFundoQuery = new OperacaoFundoQuery("O");
            CarteiraQuery carteiraQuery = new CarteiraQuery("A");
            TipoClienteQuery tipoClienteQuery = new TipoClienteQuery("T");

            clienteQuery.Select(tipoClienteQuery.IdTipo,
                                tipoClienteQuery.Descricao,
                                carteiraQuery.IdCarteira,
                                carteiraQuery.Nome.As("NomeCarteira"),
                                clienteQuery.IdCliente,
                                clienteQuery.Nome);
            clienteQuery.InnerJoin(tipoClienteQuery).On(tipoClienteQuery.IdTipo == clienteQuery.IdTipo);
            clienteQuery.InnerJoin(permissaoClienteQuery).On(permissaoClienteQuery.IdCliente == clienteQuery.IdCliente);
            clienteQuery.InnerJoin(usuarioQuery).On(permissaoClienteQuery.IdUsuario == usuarioQuery.IdUsuario);
            clienteQuery.InnerJoin(operacaoFundoQuery).On(operacaoFundoQuery.IdCliente == clienteQuery.IdCliente);
            clienteQuery.InnerJoin(carteiraQuery).On(carteiraQuery.IdCarteira == operacaoFundoQuery.IdCarteira);
            clienteQuery.Where(usuarioQuery.Login == this.login,
                               clienteQuery.TipoControle.NotEqual(TipoControleCliente.ApenasCotacao),
                               clienteQuery.StatusAtivo.Equal(StatusAtivoCliente.Ativo),
                               operacaoFundoQuery.DataOperacao.Between(this.dataInicio, this.dataFim));
            clienteQuery.GroupBy(tipoClienteQuery.IdTipo,
                                tipoClienteQuery.Descricao,
                                carteiraQuery.IdCarteira,
                                carteiraQuery.Nome,
                                clienteQuery.IdCliente,
                                clienteQuery.Nome);
            clienteQuery.OrderBy(tipoClienteQuery.Descricao.Ascending,
                                 carteiraQuery.Nome.Ascending,
                                 clienteQuery.Nome.Ascending);

            clienteCollection.Load(clienteQuery);
            #endregion

            foreach (Cliente cliente in clienteCollection) {
                #region Para cada Cliente
                string descricaoTipo = Convert.ToString(cliente.GetColumn(TipoClienteMetadata.ColumnNames.Descricao));

                int idCliente = cliente.IdCliente.Value;
                string nomeCliente = cliente.Nome;

                int idCarteira = Convert.ToInt32(cliente.GetColumn(CarteiraMetadata.ColumnNames.IdCarteira));
                string nomeCarteira = Convert.ToString(cliente.GetColumn("NomeCarteira"));

                listaExcluir.Add(idCliente.ToString() + idCarteira.ToString());
                //
                //
                AtivoBolsa a = lista.AddNew();
                a.SetColumn("IdCliente", idCliente);
                a.SetColumn("Descricao1", descricaoTipo);
                a.SetColumn("Descricao2", nomeCarteira);
                a.SetColumn("Descricao3", nomeCliente);
                                
                //
                DateTime dataAnterior = this.dataInicio;
                
                 for (int j = 0; j < listaDatas.Count; j++) {
                     DateTime data = listaDatas[j];

                     if (j > 0) {
                         #region Para cada Data
                         
                         decimal valorAportes = 0, valorResgates = 0, valorCaptacaoLiquida = 0;

                         if (tipoInformacao == TipoInformacao.Aportes || tipoInformacao == TipoInformacao.CaptacoesLiquidas) {
                             #region TipoInformacao.Aportes/CaptacoesLiquidas
                             OperacaoFundo operacaoFundo = new OperacaoFundo();
                             operacaoFundo.Query.Select(operacaoFundo.Query.ValorBruto.Sum());
                             operacaoFundo.Query.Where(operacaoFundo.Query.IdCliente.Equal(idCliente),
                                                       operacaoFundo.Query.IdCarteira.Equal(idCarteira),
                                                       operacaoFundo.Query.DataOperacao.LessThanOrEqual(data),
                                                       operacaoFundo.Query.TipoOperacao.In((byte)TipoOperacaoFundo.Aplicacao));

                             if (dataAnterior == this.dataInicio) {
                                 operacaoFundo.Query.Where(operacaoFundo.Query.DataOperacao.GreaterThanOrEqual(dataAnterior));
                             }
                             else {
                                 operacaoFundo.Query.Where(operacaoFundo.Query.DataOperacao.GreaterThan(dataAnterior));
                             }

                             operacaoFundo.Query.Load();

                             if (operacaoFundo.ValorBruto.HasValue) {
                                 valorAportes = operacaoFundo.ValorBruto.Value;
                             }
                             #endregion
                         }
                         else {
                             #region Outras
                             OperacaoFundo operacaoFundo = new OperacaoFundo();
                             operacaoFundo.Query.Select(operacaoFundo.Query.ValorBruto.Sum());
                             operacaoFundo.Query.Where(operacaoFundo.Query.IdCliente.Equal(idCliente),
                                                       operacaoFundo.Query.IdCarteira.Equal(idCarteira),
                                                       operacaoFundo.Query.DataOperacao.LessThanOrEqual(data),
                                                       operacaoFundo.Query.TipoOperacao.In((byte)TipoOperacaoFundo.ResgateBruto,
                                                                                           (byte)TipoOperacaoFundo.ResgateCotas,
                                                                                           (byte)TipoOperacaoFundo.ResgateLiquido,
                                                                                           (byte)TipoOperacaoFundo.ResgateTotal));

                             if (dataAnterior == this.dataInicio) {
                                 operacaoFundo.Query.Where(operacaoFundo.Query.DataOperacao.GreaterThanOrEqual(dataAnterior));
                             }
                             else {
                                 operacaoFundo.Query.Where(operacaoFundo.Query.DataOperacao.GreaterThan(dataAnterior));
                             }

                             operacaoFundo.Query.Load();

                             if (operacaoFundo.ValorBruto.HasValue) {
                                 valorResgates = operacaoFundo.ValorBruto.Value;
                             }
                             #endregion
                         }

                         decimal valorPeriodo = 0;
                         if (tipoInformacao == TipoInformacao.Aportes) {
                             valorPeriodo = valorAportes;
                         }
                         else if (tipoInformacao == TipoInformacao.Resgates) {
                             valorPeriodo = valorResgates;
                         }
                         else if (tipoInformacao == TipoInformacao.CaptacoesLiquidas) {
                             valorPeriodo = valorCaptacaoLiquida;
                         }

                         a.SetColumn("Valor" + j, valorPeriodo);
                     }
                    #endregion
                    dataAnterior = data;
                }

                #endregion
            }
            #endregion

            #region Tratamento para OperacaoCotista
            #region Consulta Pessoa
            PessoaQuery pessoaQuery = new PessoaQuery("E");
            usuarioQuery = new UsuarioQuery("U");
            PermissaoCotistaQuery permissaoCotistaQuery = new PermissaoCotistaQuery("P");
            OperacaoCotistaQuery operacaoCotistaQuery = new OperacaoCotistaQuery("O");
            carteiraQuery = new CarteiraQuery("A");
            CotistaQuery cotistaQuery = new CotistaQuery("C");
            //
            pessoaQuery.Select(pessoaQuery.Tipo,
                                pessoaQuery.IdPessoa,
                                pessoaQuery.Nome,
                                carteiraQuery.IdCarteira,
                                carteiraQuery.Nome.As("NomeCarteira"),
                                permissaoCotistaQuery.IdUsuario.As("Usuario1"),
                                (operacaoCotistaQuery.IdCotista.Cast(esCastType.String) + operacaoCotistaQuery.IdCarteira.Cast(esCastType.String)).As("Observacao")
                               );
            //
            pessoaQuery.InnerJoin(cotistaQuery).On(pessoaQuery.IdPessoa == cotistaQuery.IdPessoa);
            pessoaQuery.InnerJoin(operacaoCotistaQuery).On(operacaoCotistaQuery.IdCotista == cotistaQuery.IdCotista);
            pessoaQuery.InnerJoin(permissaoCotistaQuery).On(permissaoCotistaQuery.IdCotista == cotistaQuery.IdCotista);
            pessoaQuery.InnerJoin(usuarioQuery).On(usuarioQuery.IdUsuario == permissaoCotistaQuery.IdUsuario);
            pessoaQuery.InnerJoin(carteiraQuery).On(carteiraQuery.IdCarteira == operacaoCotistaQuery.IdCarteira);
            //
            pessoaQuery.Where(usuarioQuery.Login == this.login,
                              carteiraQuery.StatusAtivo.Equal(StatusAtivoCliente.Ativo),
                              operacaoCotistaQuery.DataOperacao.Between(this.dataInicio, this.dataFim),
                              operacaoCotistaQuery.IdCotista.NotEqual(operacaoCotistaQuery.IdCarteira),
                              operacaoCotistaQuery.Observacao.NotIn(listaExcluir));
            //
            pessoaQuery.GroupBy(pessoaQuery.Tipo,
                                pessoaQuery.IdPessoa,
                                pessoaQuery.Nome,
                                carteiraQuery.IdCarteira,
                                carteiraQuery.Nome,
                                permissaoCotistaQuery.IdUsuario,
                                operacaoCotistaQuery.IdCotista,
                                operacaoCotistaQuery.IdCarteira,
                                operacaoCotistaQuery.Observacao);

            pessoaQuery.OrderBy(carteiraQuery.Nome.Ascending,
                                pessoaQuery.Nome.Ascending);

            PessoaCollection pessoaCollection = new PessoaCollection();

            try {
                pessoaCollection.Load(pessoaQuery);
            }
            catch (Exception r) {
                string lastquery = pessoaCollection.Query.es.LastQuery;
            }
            #endregion

            foreach (Pessoa pessoa in pessoaCollection) {
                #region Para cada Pessoa
                string descricaoTipo = pessoa.Tipo.Value == (byte)TipoPessoa.Fisica ? "Pessoa Fisica" : "Pessoa Juridica";

                int idCliente = pessoa.IdPessoa.Value;
                string nomeCliente = pessoa.Nome;

                int idCarteira = Convert.ToInt32(pessoa.GetColumn(CarteiraMetadata.ColumnNames.IdCarteira));
                string nomeCarteira = Convert.ToString(pessoa.GetColumn("NomeCarteira"));

                AtivoBolsa a = lista.AddNew();
                a.SetColumn("IdCliente", idCliente);
                a.SetColumn("Descricao1", descricaoTipo);
                a.SetColumn("Descricao2", idCliente == idCarteira ? "Carteira de investimentos" : nomeCarteira);
                a.SetColumn("Descricao3", nomeCliente);
                
                DateTime dataAnterior = this.dataInicio;

                for (int j = 0; j < listaDatas.Count; j++) {
                    DateTime data = listaDatas[j];

                    if (j > 0) {

                        #region Para cada Data
                        
                        decimal valorAportes = 0, valorResgates = 0, valorCaptacaoLiquida = 0;

                        if (tipoInformacao == TipoInformacao.Aportes || tipoInformacao == TipoInformacao.CaptacoesLiquidas) {
                            #region Aportes/CaptacoesLiquidas
                            OperacaoCotista operacaoCotista = new OperacaoCotista();
                            operacaoCotista.Query.Select(operacaoCotista.Query.ValorBruto.Sum());
                            operacaoCotista.Query.Where(operacaoCotista.Query.IdCotista.Equal(idCliente),
                                                      operacaoCotista.Query.IdCarteira.Equal(idCarteira),
                                                      operacaoCotista.Query.DataOperacao.LessThanOrEqual(data),
                                                      operacaoCotista.Query.TipoOperacao.In((byte)TipoOperacaoCotista.Aplicacao));

                            if (dataAnterior == this.dataInicio) {
                                operacaoCotista.Query.Where(operacaoCotista.Query.DataOperacao.GreaterThanOrEqual(dataAnterior));
                            }
                            else {
                                operacaoCotista.Query.Where(operacaoCotista.Query.DataOperacao.GreaterThan(dataAnterior));
                            }

                            operacaoCotista.Query.Load();

                            if (operacaoCotista.ValorBruto.HasValue) {
                                valorAportes = operacaoCotista.ValorBruto.Value;
                            }
                            #endregion
                        }
                        else {
                            #region Outros
                            OperacaoCotista operacaoCotista = new OperacaoCotista();
                            operacaoCotista.Query.Select(operacaoCotista.Query.ValorBruto.Sum());
                            operacaoCotista.Query.Where(operacaoCotista.Query.IdCotista.Equal(idCliente),
                                                      operacaoCotista.Query.IdCarteira.Equal(idCarteira),
                                                      operacaoCotista.Query.DataOperacao.LessThanOrEqual(data),
                                                      operacaoCotista.Query.TipoOperacao.In((byte)TipoOperacaoCotista.ResgateBruto,
                                                                                          (byte)TipoOperacaoCotista.ResgateCotas,
                                                                                          (byte)TipoOperacaoCotista.ResgateLiquido,
                                                                                          (byte)TipoOperacaoCotista.ResgateTotal));

                            if (dataAnterior == this.dataInicio) {
                                operacaoCotista.Query.Where(operacaoCotista.Query.DataOperacao.GreaterThanOrEqual(dataAnterior));
                            }
                            else {
                                operacaoCotista.Query.Where(operacaoCotista.Query.DataOperacao.GreaterThan(dataAnterior));
                            }

                            operacaoCotista.Query.Load();

                            if (operacaoCotista.ValorBruto.HasValue) {
                                valorResgates = operacaoCotista.ValorBruto.Value;
                            }
                            #endregion
                        }

                        decimal valorPeriodo = 0;
                        if (tipoInformacao == TipoInformacao.Aportes) {
                            valorPeriodo = valorAportes;
                        }
                        else if (tipoInformacao == TipoInformacao.Resgates) {
                            valorPeriodo = valorResgates;
                        }
                        else if (tipoInformacao == TipoInformacao.CaptacoesLiquidas) {
                            valorPeriodo = valorCaptacaoLiquida;
                        }

                        a.SetColumn("Valor" + j, valorPeriodo);
                    }

                    dataAnterior = data;
                    
                    #endregion
                }

                #endregion
            }
            #endregion

            return lista;
        }


        /// <summary>
        /// Retorna datas de acordo com o tipo de agrupamento definido no objeto da classe (mes, trimestre, semestre, ano).
        /// </summary>
        /// <returns></returns>
        public List<DateTime> RetornaListaDatas() {
            List<DateTime> lista = new List<DateTime>();

            DateTime dataAux = this.dataInicio;

            int i = 0;
            while (dataAux < this.dataFim) {
                lista.Add(dataAux);

                if (this.colunaData == TipoColunaData.Mes) {
                    dataAux = Calendario.RetornaUltimoDiaCorridoMes(dataAux, i);

                    i = i + 1;
                }
                else if (this.colunaData == TipoColunaData.Trimestre) {
                    dataAux = Calendario.RetornaUltimoDiaCorridoMes(dataAux, i);

                    i = i + 3;
                }
                else if (this.colunaData == TipoColunaData.Semestre) {
                    dataAux = Calendario.RetornaUltimoDiaCorridoMes(dataAux, i);

                    i = i + 6;
                }
                else if (this.colunaData == TipoColunaData.Ano) {
                    dataAux = Calendario.RetornaUltimoDiaCorridoAno(dataAux, 0);

                    i = i + 1;
                }
            }

            lista.Add(this.dataFim);

            return lista;
        }

        public string RetornaFaixaEtaria(int anos) {
            string faixaEtaria = "";

            if (anos < 10) {
                faixaEtaria = "abaixo de 10 anos";
            }
            else if (anos <= 20) {
                faixaEtaria = "entre 10 e 20 anos";
            }
            else if (anos <= 30) {
                faixaEtaria = "entre 21 e 30 anos";
            }
            else if (anos <= 40) {
                faixaEtaria = "entre 31 e 40 anos";
            }
            else if (anos <= 50) {
                faixaEtaria = "entre 41 e 50 anos";
            }
            else if (anos <= 60) {
                faixaEtaria = "entre 51 e 60 anos";
            }
            else if (anos <= 70) {
                faixaEtaria = "entre 61 e 70 anos";
            }
            else if (anos <= 80) {
                faixaEtaria = "entre 71 e 80 anos";
            }
            else if (anos <= 90) {
                faixaEtaria = "entre 81 e 90 anos";
            }
            else if (anos <= 100) {
                faixaEtaria = "entre 91 e 100 anos";
            }
            else if (anos <= 110) {
                faixaEtaria = "entre 101 e 110 anos";
            }
            else if (anos <= 120) {
                faixaEtaria = "entre 111 e 120 anos";
            }
            else if (anos <= 130) {
                faixaEtaria = "entre 121 e 130 anos";
            }
            else if (anos <= 140) {
                faixaEtaria = "entre 131 e 140 anos";
            }

            return faixaEtaria;
        }

        public string RetornaDiasCadastro(int dias) {
            if (dias < 7) {
                return "menos de 1 semana";
            }
            else if (dias < 28) {
                return "menos de 1 mês";
            }
            else if (dias <= 31) {
                return "aprox. 1 mês";
            }
            else if (dias < 84) {
                return "menos de 3 meses";
            }
            else if (dias < 92) {
                return "aprox. 3 meses";
            }
            else if (dias < 180) {
                return "menos de 6 meses";
            }
            else if (dias < 186) {
                return "aprox. 6 meses";
            }
            else if (dias < 360) {
                return "menos de 1 ano";
            }
            else if (dias > 1830) {
                return "mais de 5 anos";
            }
            else {
                return "mais de 5 anos";
            }
        }
    }
}