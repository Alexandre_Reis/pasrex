﻿using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using EntitySpaces.Interfaces;
using System.Collections.Generic;
using Financial.Web.Common;
using System.IO;
using System.Globalization;
using Dart.PowerTCP.Zip;
using System.Text;
using DevExpress.Web;

public partial class _ExibeGrupamentoExcel : BasePage {   
    new protected void Page_Load(object sender, EventArgs e) {
        
        #region Exporta Excel

        ASPxGridViewExporter grid = (ASPxGridViewExporter)Session["streamGridExport"];
        Session.Remove("streamGridExport");
         
        DevExpress.XtraPrinting.XlsExportOptions a = new DevExpress.XtraPrinting.XlsExportOptions();
        a.Suppress256ColumnsWarning = true;

        grid.WriteXlsToResponse("Grid", false, a);

        HttpContext.Current.ApplicationInstance.CompleteRequest();
        Response.End();

        #endregion
    }
}