﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Grupamento2.aspx.cs" Inherits="CadastrosBasicos_GrupamentoAux2" %>

<%@ Register Assembly="System.Web, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a" Namespace="System.Web.UI.HtmlControls" TagPrefix="cc2" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxe" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxwgv" %>
<%@ Register Assembly="EntitySpaces.Web" Namespace="EntitySpaces.Web" TagPrefix="cc1" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <link href="~/css/forms.css" rel="stylesheet" type="text/css" />    
    <script type="text/javascript" language="Javascript" src="../js/global.js"></script>    
</head>

<body>
    <form id="form1" runat="server">
    
    <div class="divPanel">
    <table width="100%"><tr><td>
    <div id="container">
    
    <div id="header">
        <asp:Label ID="lblHeader" runat="server" Text="Grupamento Teste - Grupo-SubGrupo-Folha" />
    </div>
        
    <div id="mainContent">

        <div class="linkButton linkButtonNoBorder" >               
           <asp:LinkButton ID="btnColapsa" runat="server" Font-Overline="false" CssClass="btnAdd" 
                OnClientClick="gridCadastro.CollapseAll(); return false;">
                <asp:Literal ID="Literal1" runat="server" Text="Colapsa todas as Linhas"/><div></div>
           </asp:LinkButton>
           
           <asp:LinkButton ID="btnExpande" runat="server" Font-Overline="false" CssClass="btnAdd" 
                OnClientClick=" gridCadastro.ExpandAll(); return false;">
                <asp:Literal ID="Literal2" runat="server" Text="Expanda todas as Linhas"/><div></div>
           </asp:LinkButton>
           
        </div>
        
        <div class="divDataGrid">            
            <dxwgv:ASPxGridView ID="gridCadastro" ClientInstanceName="gridCadastro" runat="server" EnableCallBacks="true" 
            DataSourceID="EsDSGrupamento" KeyFieldName="Id" Width="100%" >
                                      
            <Columns>                           
                <dxwgv:GridViewCommandColumn VisibleIndex="0" Visible="false" Width="0" ShowClearFilterButton="True">
                    <HeaderTemplate>
                        <dxe:ASPxCheckBox ID="cbAll" ClientInstanceName="cbAll" runat="server"/>
                    </HeaderTemplate>
                </dxwgv:GridViewCommandColumn>

                <dxwgv:GridViewDataColumn FieldName="Id" Visible="false" Width="0" VisibleIndex="0" />
                <dxwgv:GridViewDataColumn FieldName="IdCliente" Width="85" Settings-AllowDragDrop="false" VisibleIndex="1" />
                <dxwgv:GridViewDataColumn FieldName="Descricao3" Caption="Nome Cliente" Width="200" Settings-AllowDragDrop="false" VisibleIndex="2" />
                <%--   Grupos --%>
                <dxwgv:GridViewDataColumn FieldName="Descricao1" Caption="Tipo Cliente" VisibleIndex="1000" GroupIndex="1" />
                <dxwgv:GridViewDataColumn FieldName="Descricao2" Caption="Cliente" VisibleIndex="1001" GroupIndex="2" />                                
                
            </Columns>            

            <GroupSummary>
                <dxwgv:ASPxSummaryItem FieldName="IdCliente" SummaryType="Count" DisplayFormat="Núm. Clientes={0:N0}" ShowInGroupFooterColumn="IdCliente" />
            </GroupSummary>

            <TotalSummary>
                <dxwgv:ASPxSummaryItem FieldName="IdCliente" SummaryType="Count" DisplayFormat="Total Clientes={0:N0}" />
            </TotalSummary>
                          
            <Templates>
                <GroupRowContent>
                    <%# Container.GroupText %>
                </GroupRowContent>
            </Templates>
                                                                  
            <Settings ShowGroupPanel="true" ShowFooter="true" ShowGroupFooter="VisibleIfExpanded" />            
            
            <SettingsText GroupContinuedOnNextPage="                      - Continua na Próxima Página" 
                          EmptyDataRow="0 Registros" />
                          
            <SettingsLoadingPanel Text = "Carregando" />
            <Styles Footer-Font-Bold="true" Cell-Wrap="False" />
            
            <SettingsPager PageSize="20"></SettingsPager>
            <SettingsCommandButton>
                <ClearFilterButton Image-Url="../imagens/funnel--minus.png"/>
                <UpdateButton Image-Url="../imagens/ico_form_ok_inline.gif"/>
                <CancelButton Image-Url="../imagens/ico_form_back_inline.gif"/>
            </SettingsCommandButton>
            
        </dxwgv:ASPxGridView>
        </div>
          
    </div>
    
    </div>
    </td></tr></table>
    </div>        

    <cc1:esDataSource ID="EsDSGrupamento" runat="server" OnesSelect="EsDSGrupamento_esSelect" />
    
    </form>
</body>
</html>