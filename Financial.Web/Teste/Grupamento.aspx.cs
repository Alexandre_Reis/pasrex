﻿using Financial.Web.Common;
using Financial.Bolsa;
using System.Data;
using System;
using DevExpress.Web;
using System.Web.UI.WebControls;
using DevExpress.Utils;
using DevExpress.Data;
using System.IO;

/*
 // http://documentation.devexpress.com/#AspNet/CustomDocument4262
 * http://documentation.devexpress.com/#AspNet/CustomDocument3758
 * http://demos.devexpress.com/aspxgridviewdemos/groupingsorting/grouping.aspx
 * http://demos.devexpress.com/aspxgridviewdemos/Summary/Total.aspx
 * http://documentation.devexpress.com/#AspNet/CustomDocument5737
 */
public partial class CadastrosBasicos_GrupamentoAux : ConsultaBasePage {
    new protected void Page_Load(object sender, EventArgs e) {
        base.Page_Load(sender, e);

        DataTable dt = this.CreateData(); // Cria dados ficticios com 10 colunas valores
        //
        if (!Page.IsPostBack) {
            this.AddColumns(dt);
            this.AddGroupTotalSummary(dt);
        }
       
        //
        this.gridCadastro.DataSource = dt;
        this.gridCadastro.KeyFieldName = "Id";
        //

        this.gridCadastro.DataBind();
    }

    /// <summary>
    /// Adiciona Colunas Valor Dinamicamente
    /// </summary>
    /// <param name="d"></param>
    private void AddColumns(DataTable d) {

        int colunasValor = d.Columns.Count - 4;

        for (int i = 1; i <= colunasValor; i++) {
            GridViewDataSpinEditColumn g = new GridViewDataSpinEditColumn();
            g.FieldName = "Valor" + i;
            g.Caption = "Valor" + i;
            g.VisibleIndex = i + 1;
            if (colunasValor >= 15) {
                g.Width = Unit.Point(70);
            }
            g.Settings.AllowDragDrop = DefaultBoolean.False;
            g.HeaderStyle.HorizontalAlign = HorizontalAlign.Right;
            g.PropertiesSpinEdit.DisplayFormatString = "{0:#,##0.00;(#,##0.00);0.00}";

            this.gridCadastro.Columns.Add(g);
        }

        // Adiciona ScroolBar se colunas > 15
        if (colunasValor >= 15) {
            this.gridCadastro.Settings.HorizontalScrollBarMode = DevExpress.Web.ScrollBarMode.Visible;
        }
    }

    /// <summary>
    /// Adiciona GroupSummary e TotalSummary da coluna Valor Dinamicamente
    /// </summary>
    /// <param name="d"></param>
    private void AddGroupTotalSummary(DataTable d) {

        int colunasValor = d.Columns.Count - 4;

        #region Adiciona Group Summary
        for (int j = 1; j <= colunasValor; j++) {
            ASPxSummaryItem item = new ASPxSummaryItem();
            //
            item.FieldName = "Valor" + j;
            item.SummaryType = SummaryItemType.Sum;
            //
            string format = "Valor" + j;
            //
            item.DisplayFormat = format +"={0:#,##0.00;(#,##0.00);0.00}";

            this.gridCadastro.GroupSummary.Add(item);
        }
        #endregion

        #region Adiciona Total Summary
        for (int j = 1; j <= colunasValor; j++) {
            ASPxSummaryItem item = new ASPxSummaryItem();
            //
            item.FieldName = "Valor" + j;            
            item.SummaryType = SummaryItemType.Sum;
            item.DisplayFormat = "Total={0:#,##0.00;(#,##0.00);0.00}";
                       
            this.gridCadastro.TotalSummary.Add(item);            
        }
        #endregion
    }

    // Dados Ficticios - vem como parametro
    /// <summary>
    /// DataTable no Formato Id, Descricao1, Descricao2, IdCliente, Valor1, Valor2, ...
    /// </summary>
    /// <returns></returns>
    private DataTable CreateData() {
        int colunas = 6; // minimo = 4 por causa dos dados

        DataTable dt = new DataTable();
        dt.Columns.Add("Id", typeof(int)); // Chave do Grid
        dt.Columns.Add("Descricao1", typeof(string));
        dt.Columns.Add("Descricao2", typeof(string));
        dt.Columns.Add("IdCliente", typeof(int));

        // Adiciona Colunas Valor
        for (int i = 1; i <= colunas; i++) {
            dt.Columns.Add("Valor" + i, typeof(decimal));
        }

        // Adiciona Dados
        //for (int i = 0; i < 50; i++) {
        //    //dt.Rows.Add(new object[] { i, "teste", "teste1", 1, 1.0M, 2.0M, 3.0M, 4.0M, 5.0M, 6.0M, 7.0M, 8.0M, 9.0M, 10.0M });
        //    dt.Rows.Add(new object[] { i, "teste", "teste1", 1, 1.0M, 2.0M });
        //}

        dt.Rows.Add(new object[] { 1, "Teste", "Teste1", 13712, 1.00M, 1.00M, 1.00M, 1.00M, });
        dt.Rows.Add(new object[] { 2, "Teste", "Teste2", 14866, 2.00M, 2.00M, 2.00M, 2.00M });
        dt.Rows.Add(new object[] { 3, "Teste", "Teste3", 13900, 2.00M, 2.00M, 2.00M, 2.00M });
        //
        dt.Rows.Add(new object[] { 4, "Teste10", "Teste12", 13900, 3.00M, 3.00M, 3.00M, 3.00M });
        dt.Rows.Add(new object[] { 5, "Teste11", "Teste13", 13712, 4.00M, 4.00M, 4.00M, 4.00M });
        //
        dt.Rows.Add(new object[] { 6, "Teste12", "Teste23", 13712, 1.00M, 1.00M, 1.00M, 1.00M });
        dt.Rows.Add(new object[] { 7, "Teste13", "Teste23", 13900, 2.00M, 2.00M, 2.00M, 2.00M });
        dt.Rows.Add(new object[] { 8, "Teste14", "Teste23", 15000, 3.00M, 3.00M, 3.00M, 3.00M });
        dt.Rows.Add(new object[] { 9, "Teste15", "Teste23", 16000, 4.00M, 4.00M, 4.00M, 4.00M });
        dt.Rows.Add(new object[] { 10, "Teste16", "Teste23", 17000, 5.00M, 5.00M, 5.00M, 5.00M });
        dt.Rows.Add(new object[] { 11, "Teste17", "Teste23", 18000, 6.00M, 6.00M, 6.00M, 6.00M });
        dt.Rows.Add(new object[] { 12, "Teste18", "Teste23", 19000, 7.00M, 7.00M, 7.00M, 7.00M });
        dt.Rows.Add(new object[] { 13, "Teste19", "Teste23", 20000, 8.00M, 8.00M, 8.00M, 8.00M });
        dt.Rows.Add(new object[] { 14, "Teste20", "Teste23", 21000, 9.00M, 9.00M, 9.00M, 9.00M });

        return dt;
    }   

    /* Colapsa o segundo nivel */
    protected void GridOndataBound(Object sender, EventArgs e) {
        this.gridCadastro.ExpandAll();

        for (int i = 0; i < this.gridCadastro.VisibleRowCount; i++) {
            if (this.gridCadastro.GetRowLevel(i) == 0) {
                this.gridCadastro.CollapseRow(i);
            }
        }
    }

    protected void btnExcel_Click1(object sender, EventArgs e) {
        //this.gridExport.WriteXlsToResponse();
        
        DevExpress.XtraPrinting.XlsExportOptions a = new DevExpress.XtraPrinting.XlsExportOptions();
        a.Suppress256ColumnsWarning = true;

        this.gridExport.WriteXlsToResponse("Grid", false, a);
    }
}