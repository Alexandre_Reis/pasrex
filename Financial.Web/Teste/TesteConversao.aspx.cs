﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Financial.Bolsa;
using EntitySpaces.Core;
using EntitySpaces.Interfaces;
using System.Text;

public partial class Teste_TesteConversao : System.Web.UI.Page {
    
    /* Teste Para Conversão - Adicionar na tabela conversao com uma coluna a mais */
    protected void Page_Load(object sender, EventArgs e) {

        #region Conversao

        //ConversaoBolsaCollection c = new ConversaoBolsaCollection();
        //c.CreateColumnsForBinding();
        //c.AddColumn("CodigoIsin", typeof(System.String));
                
        #region Cria Conversao
        //ConversaoBolsa conversaoBolsa = c.AddNew();

        ConversaoBolsa conversaoBolsa = new ConversaoBolsa();

        //conversaoBolsa.AddColumn("CodigoIsin", typeof(System.String));
        //
        conversaoBolsa.DataLancamento = DateTime.Now;
        conversaoBolsa.DataEx = DateTime.Now;
        conversaoBolsa.DataReferencia = DateTime.Now;
        conversaoBolsa.CdAtivoBolsa = "PETR4";
        conversaoBolsa.FatorQuantidade = 1;
        conversaoBolsa.FatorPU = 1;
        conversaoBolsa.CdAtivoBolsaDestino = " ";
        conversaoBolsa.Fonte = 1;
        //conversaoBolsa.CodigoIsin = "teste";
        //conversaoBolsa.SetColumn("CodigoIsin", "teste");
        //
        //conversaoBolsa.es.ModifiedColumns.Add("CodigoIsin");

        string codigoIsin = "teste";

        #endregion

        //c.Save();

        #region Insert

        string[] campos1 = new string[] {         
            ConversaoBolsaMetadata.ColumnNames.DataLancamento,
            ConversaoBolsaMetadata.ColumnNames.DataEx,
            ConversaoBolsaMetadata.ColumnNames.DataReferencia,
            ConversaoBolsaMetadata.ColumnNames.CdAtivoBolsa,
            ConversaoBolsaMetadata.ColumnNames.FatorQuantidade,
            ConversaoBolsaMetadata.ColumnNames.FatorPU,
            ConversaoBolsaMetadata.ColumnNames.CdAtivoBolsaDestino,
            ConversaoBolsaMetadata.ColumnNames.Fonte,
            "CodigoIsin"
        };

        string camposConcat = String.Join(", ", campos1);
        string camposParams = String.Join(",@", campos1);

        #endregion
       
        esParameters esParams = new esParameters();
        esParams.Add("DataLancamento", conversaoBolsa.DataLancamento.Value);
        esParams.Add("DataEx", conversaoBolsa.DataEx.Value);
        esParams.Add("DataReferencia", conversaoBolsa.DataReferencia.Value);
        esParams.Add("CdAtivoBolsa", conversaoBolsa.CdAtivoBolsa);
        esParams.Add("FatorQuantidade", conversaoBolsa.FatorQuantidade.Value);
        esParams.Add("FatorPU", conversaoBolsa.FatorPU.Value);
        esParams.Add("CdAtivoBolsaDestino", conversaoBolsa.CdAtivoBolsaDestino);
        esParams.Add("Fonte", conversaoBolsa.Fonte.Value);
        
        // Parametros que podem ser null
        if (String.IsNullOrEmpty(codigoIsin)) {
            esParams.Add("CodigoIsin", DBNull.Value);
        }
        else {
            esParams.Add("CodigoIsin", codigoIsin);
        }

        //
        StringBuilder sqlBuilder = new StringBuilder();
        //
        sqlBuilder.Append("INSERT INTO ConversaoBolsa (");
        sqlBuilder.Append(camposConcat);
        sqlBuilder.Append(") VALUES ( ");        
        sqlBuilder.Append("@" + camposParams);
        sqlBuilder.Append(")");

        esUtility u = new esUtility();
        u.ExecuteNonQuery(esQueryType.Text, sqlBuilder.ToString(), esParams);

        #endregion

        #region Bonificacao

        BonificacaoBolsa bonificacaoBolsa = new BonificacaoBolsa();

        bonificacaoBolsa.DataLancamento = DateTime.Now;
        bonificacaoBolsa.DataEx = DateTime.Now;
        bonificacaoBolsa.DataReferencia = DateTime.Now;
        bonificacaoBolsa.TipoFracao = 1;
        bonificacaoBolsa.PUBonificacao = 1;
        bonificacaoBolsa.CdAtivoBolsa = "PETR4";
        bonificacaoBolsa.Percentual = 1;        
        bonificacaoBolsa.CdAtivoBolsaDestino = " ";
        bonificacaoBolsa.Fonte = 1;

        codigoIsin = "teste";
        
        campos1 = new string[] {         
            BonificacaoBolsaMetadata.ColumnNames.DataLancamento,
            BonificacaoBolsaMetadata.ColumnNames.DataEx,
            BonificacaoBolsaMetadata.ColumnNames.DataReferencia,
            BonificacaoBolsaMetadata.ColumnNames.TipoFracao,
            BonificacaoBolsaMetadata.ColumnNames.PUBonificacao,
            BonificacaoBolsaMetadata.ColumnNames.CdAtivoBolsa,
            BonificacaoBolsaMetadata.ColumnNames.Percentual,
            BonificacaoBolsaMetadata.ColumnNames.CdAtivoBolsaDestino,
            BonificacaoBolsaMetadata.ColumnNames.Fonte,
            "CodigoIsin"
        };

        camposConcat = String.Join(", ", campos1);
        camposParams = String.Join(",@", campos1);
       
        esParams = new esParameters();
        esParams.Add("DataLancamento", bonificacaoBolsa.DataLancamento.Value);
        esParams.Add("DataEx", bonificacaoBolsa.DataEx.Value);
        esParams.Add("DataReferencia", bonificacaoBolsa.DataReferencia.Value);
        esParams.Add("TipoFracao", bonificacaoBolsa.TipoFracao.Value);
        esParams.Add("PUBonificacao", bonificacaoBolsa.PUBonificacao);
        esParams.Add("CdAtivoBolsa", bonificacaoBolsa.CdAtivoBolsa);
        esParams.Add("Percentual", bonificacaoBolsa.Percentual.Value);
        esParams.Add("CdAtivoBolsaDestino", bonificacaoBolsa.CdAtivoBolsaDestino);
        esParams.Add("Fonte", bonificacaoBolsa.Fonte.Value);

        // Parametros que podem ser null
        if (String.IsNullOrEmpty(codigoIsin)) {
            esParams.Add("CodigoIsin", DBNull.Value);
        }
        else {
            esParams.Add("CodigoIsin", codigoIsin);
        }

        //
        sqlBuilder = new StringBuilder();
        //
        sqlBuilder.Append("INSERT INTO BonificacaoBolsa (");
        sqlBuilder.Append(camposConcat);
        sqlBuilder.Append(") VALUES ( ");
        sqlBuilder.Append("@" + camposParams);
        sqlBuilder.Append(")");

        u = new esUtility();
        u.ExecuteNonQuery(esQueryType.Text, sqlBuilder.ToString(), esParams);

        #endregion

        #region SubscricaoBolsa

        SubscricaoBolsa subscricaoBolsa = new SubscricaoBolsa();

        subscricaoBolsa.DataLancamento = DateTime.Now;
        subscricaoBolsa.DataEx = DateTime.Now;
        subscricaoBolsa.DataReferencia = DateTime.Now;
        subscricaoBolsa.PrecoSubscricao = 1;
        subscricaoBolsa.Fator = 1;
        subscricaoBolsa.CdAtivoBolsaDireito = "PETR4";
        subscricaoBolsa.CdAtivoBolsa = "PETR4";
        subscricaoBolsa.Fonte = 1;
        subscricaoBolsa.DataFimNegociacao = DateTime.Now;
        
        codigoIsin = "teste";
        
        campos1 = new string[] {         
            SubscricaoBolsaMetadata.ColumnNames.DataLancamento,
            SubscricaoBolsaMetadata.ColumnNames.DataEx,
            SubscricaoBolsaMetadata.ColumnNames.DataReferencia,
            SubscricaoBolsaMetadata.ColumnNames.PrecoSubscricao,
            SubscricaoBolsaMetadata.ColumnNames.Fator,
            SubscricaoBolsaMetadata.ColumnNames.CdAtivoBolsaDireito,
            SubscricaoBolsaMetadata.ColumnNames.CdAtivoBolsa,
            SubscricaoBolsaMetadata.ColumnNames.DataFimNegociacao,            
            SubscricaoBolsaMetadata.ColumnNames.Fonte,
            "CodigoIsin"
        };

        camposConcat = String.Join(", ", campos1);
        camposParams = String.Join(",@", campos1);
       
        esParams = new esParameters();
        esParams.Add("DataLancamento", subscricaoBolsa.DataLancamento.Value);
        esParams.Add("DataEx", subscricaoBolsa.DataEx.Value);
        esParams.Add("DataReferencia", subscricaoBolsa.DataReferencia.Value);
        esParams.Add("PrecoSubscricao", subscricaoBolsa.PrecoSubscricao.Value);
        esParams.Add("Fator", subscricaoBolsa.Fator.Value);
        esParams.Add("CdAtivoBolsaDireito", subscricaoBolsa.CdAtivoBolsaDireito);
        esParams.Add("CdAtivoBolsa", subscricaoBolsa.CdAtivoBolsa);
        esParams.Add("DataFimNegociacao", subscricaoBolsa.DataFimNegociacao.Value);
        esParams.Add("Fonte", subscricaoBolsa.Fonte.Value);

        // Parametros que podem ser null
        if (String.IsNullOrEmpty(codigoIsin)) {
            esParams.Add("CodigoIsin", DBNull.Value);
        }
        else {
            esParams.Add("CodigoIsin", codigoIsin);
        }

        //
        sqlBuilder = new StringBuilder();
        //
        sqlBuilder.Append("INSERT INTO SubscricaoBolsa (");
        sqlBuilder.Append(camposConcat);
        sqlBuilder.Append(") VALUES ( ");
        sqlBuilder.Append("@" + camposParams);
        sqlBuilder.Append(")");

        u = new esUtility();
        u.ExecuteNonQuery(esQueryType.Text, sqlBuilder.ToString(), esParams);

        #endregion
    }
}