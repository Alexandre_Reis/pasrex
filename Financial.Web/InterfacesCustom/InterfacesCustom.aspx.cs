using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;

using Financial.WebConfigConfiguration;

public partial class InterfacesCustom_Default : System.Web.UI.Page {
    protected void Page_Load(object sender, EventArgs e) {

        // "Importacao" - "Exportacao"
        string tipo = Request["param"].ToString();
        //
        string cliente = WebConfig.AppSettings.Cliente.Substring(0, 1).ToUpper() + WebConfig.AppSettings.Cliente.Substring(1).ToLower();
        //
        Response.Redirect("~/InterfacesCustom/" + cliente + ".aspx?param=" + tipo);
    }
}
