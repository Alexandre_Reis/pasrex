﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.IO;
using System.Globalization;
using Financial.Export;
using System.Text;
using Bytescout.Spreadsheet;
using System.Collections.Generic;
using System.Drawing;
using Financial.Investidor;
using Financial.Security;
using Financial.Web.Common;
using EntitySpaces.Interfaces;
using Financial.Security.Enums;
using Financial.Bolsa;
using Financial.Fundo;
using Financial.RendaFixa;
using Financial.Util;
using Financial.Fundo.Enums;
using Financial.RendaFixa.Enums;
using Financial.Tributo.Custom;
using Financial.Common;
using Financial.Common.Enums;
using Financial.Integracao.Excel;
using Financial.Integracao.Excel.Util;
using DevExpress.Web;

public partial class InterfacesCustom_Paulista : ImportacaoBasePage {
    // Controla Exibição de Importação ou Exportação
    private Tipo tipo;

    enum Tipo {
        Importacao = 0,
        Exportacao = 1
    }
    /*-----------------------------------------*/

    #region Enum p/ Exportacao
    #endregion

    #region Enum p/ Importacao
    #endregion

    /// <summary>
    /// Controla Exibição de Importação ou Exportação
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    new protected void panelEdicao_Load(object sender, EventArgs e) {
        Control c1 = this.FindControl("divExportacao");
        Control c2 = this.FindControl("divImportacao");

        if (this.tipo == Tipo.Exportacao) {
            if (c1 != null) { c1.Visible = true; }
            if (c2 != null) { c2.Visible = false; }
        }
        else if (this.tipo == Tipo.Importacao) {
            if (c1 != null) { c1.Visible = false; }
            if (c2 != null) { c2.Visible = true; }
        }
    }

    new protected void Page_Load(object sender, EventArgs e) {
        this.tipo = (Tipo)Enum.Parse(typeof(Tipo), Request.QueryString["param"].ToString());
    }

    #region Exportação
    #endregion

    #region Importação


    #region Operação Compromissada
    protected void callbackExcelOperacaoCompromissada_Callback(object source, DevExpress.Web.CallbackEventArgs e) {
        e.Result = "";
    }

    protected void uplOperacaoCompromissada_FileUploadComplete(object sender, FileUploadCompleteEventArgs e) {
        this.uplOperacaoCompromissada_FileUploadComplete(e);
    } 
    #endregion


    #region Liquidacao Paulista
    protected void callbackExcelLiquidacaoPaulista_Callback(object source, DevExpress.Web.CallbackEventArgs e) {
        e.Result = "";
    }

    protected void uplLiquidacaoPaulista_FileUploadComplete(object sender, FileUploadCompleteEventArgs e) {
        this.uplLiquidacaoPaulista_FileUploadComplete(e);
    }
    
    #endregion


    #region OperacaoFundoYMF
    protected void callbackOperacaoFundoYMFPaulista_Callback(object source, DevExpress.Web.CallbackEventArgs e) {
        e.Result = "";
    }

    protected void uplOperacaoFundoYMFPaulista_FileUploadComplete(object sender, FileUploadCompleteEventArgs e) {
        this.uplOperacaoFundoYMFPaulista_FileUploadComplete(e);
    }

    protected void callbackHistoricoCotaYMFPaulista_Callback(object source, DevExpress.Web.CallbackEventArgs e)
    {
        e.Result = "Importação concluída";

        try
        {
            this.importaCotasYMFPaulista();
        }
        catch(Exception exception){
            e.Result = exception.Message;
        }
        
    }

    #endregion


    #endregion
}