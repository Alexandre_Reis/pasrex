﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.IO;
using System.Globalization;
using Financial.Export;
using System.Text;
using Bytescout.Spreadsheet;
using System.Collections.Generic;
using System.Drawing;
using Financial.Investidor;
using Financial.Security;
using Financial.Web.Common;
using EntitySpaces.Interfaces;
using Financial.Security.Enums;
using Financial.Bolsa;
using Financial.Fundo;
using Financial.RendaFixa;
using Financial.Util;
using Financial.Fundo.Enums;
using Financial.RendaFixa.Enums;
using Financial.Tributo.Custom;
using Financial.Common;
using Financial.Common.Enums;

public partial class InterfacesCustom_Quadrante : FiltroReportBasePage {
    // Controla Exibição de Importação ou Exportação
    private Tipo tipo;

    enum Tipo {
        Importacao = 0,
        Exportacao = 1
    }
    /*-----------------------------------------*/

    #region Enum p/ Exportacao
    enum OwnerExportacao {
        Quadrante = 1
    }

    enum TipoExportacao {
        PlanExcel = 0,
    }
    #endregion

    #region Enum p/ Importacao
    #endregion

    /// <summary>
    /// Controla Exibição de Importação ou Exportação
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    new protected void panelEdicao_Load(object sender, EventArgs e) {
        Control c1 = this.FindControl("divExportacao");
        Control c2 = this.FindControl("divImportacao");

        if (this.tipo == Tipo.Exportacao) {
            if (c1 != null) { c1.Visible = true; }
            if (c2 != null) { c2.Visible = false; }
        }
        else if (this.tipo == Tipo.Importacao) {
            if (c1 != null) { c1.Visible = false; }
            if (c2 != null) { c2.Visible = true; }
        }
    }

    new protected void Page_Load(object sender, EventArgs e) {
        //if (!Page.IsPostBack) {
        //    DateTime hoje = DateTime.Now;
        //    this.textData.Value = new DateTime(hoje.Year, hoje.Month, hoje.Day);
        //}
        this.tipo = (Tipo)Enum.Parse(typeof(Tipo), Request.QueryString["param"].ToString());

        if (this.tipo == Tipo.Exportacao) {
            this.btnEditCodigo.Focus();

            this.HasPopupCliente = true;
            base.Page_Load(sender, e);

            this.TrataTravamentoCampos();
        }
    }

    #region Exportação
    /* Armazena os Valores idCliente/Data vindos da tela para Construção do Excel */        
    private int idCliente;
    private DateTime data;

    // Armazena o TipoIndice/RetornoNominal para ser Usado no DataHolder1
    #region Valores Calculados no Cabeçalho e usados no DataHolder1
    private int? tipoIndice = null;
    private decimal? retornoNominal = null;
    #endregion

    //
    private DateTime dataDiaCliente;

    /* Variaveis privates - DataSources dos Dados */
    private List<DataHolder1> dList1 = new List<DataHolder1>();
    private List<DataHolder2> dList2 = new List<DataHolder2>();
    private List<DataHolderEstrategia> dEstrategia = new List<DataHolderEstrategia>();
    private List<DataHolderClasse> dClasse = new List<DataHolderClasse>();
    private List<DataHolderGestor> dGestor = new List<DataHolderGestor>();
    /* */

    #region Dinamic Data
    private class Cabecalho1 {
        public decimal? B2;
        public decimal? B3;
        public decimal? B4;
        public decimal? B5;
        public string B6;
        public decimal? B7;
        //
        public string C2;
        public string C6;

        public Cabecalho1() { }
        public Cabecalho1(decimal? B2, decimal? B3, decimal? B4, decimal? B5, string B6, decimal? B7, string C2, string C6) {
            this.B2 = B2;
            this.B3 = B3;
            this.B4 = B4;
            this.B5 = B5;
            this.B6 = B6;
            this.B7 = B7;
            this.C2 = C2;
            this.C6 = C6;
        }
    }

    //private class Cabecalho2 {
    //    public decimal? I3;
    //    public int? I4;
    //    public int? I5;
    //    public decimal? I6;
    //    public decimal? I7;
    //    //

    //    public Cabecalho2() { }

    //    public Cabecalho2(decimal? I3, int? I4, int? I5, decimal? I6, decimal? I7) {
    //        this.I3 = I3;
    //        this.I4 = I4;
    //        this.I5 = I5;
    //        this.I6 = I6;
    //        this.I7 = I7;
    //    }
    //}

    private class DataHolder1 {
        public string A;
        public string B;
        public string C;
        //
        public DateTime? D;
        public DateTime? E;
        //
        public int? F;
        public int? G;
        public int? H;
        //
        public decimal? I;
        public decimal? J;
        public decimal? K;
        public decimal? L;
        public decimal? M;
        //
        public string N;
        public string O;
        public string P;

        public DataHolder1() { }

        public DataHolder1(string A, string B, string C, 
                           DateTime D, DateTime E, 
                           int? F, int? G, int? H, 
                           decimal? I, decimal? J, decimal? K, decimal? L, decimal? M, 
                           string N, string O, string P) {
            this.A = A;
            this.B = B;
            this.C = C;
            this.D = D;
            this.E = E;
            this.F = F;
            this.G = G;
            this.H = H;
            this.I = I;
            this.J = J;
            this.K = K;
            this.L = L;
            this.M = M;
            this.N = N;
            this.O = O;
            this.P = P;
        }
    }

    private class DataHolder2 {
        public string A;
        public string B;
        public string C;
        //
        public decimal? D;
        public decimal? E;
        public decimal? F;
        public decimal? G;
        public decimal? H;
        public decimal? I;
        //
        public string J;
        //
        public decimal? K;
        //
        public string L;
        //
        public decimal? M;
        public decimal? N;
        public decimal? O;
        //
        // Coluna Auxiliar para poder agrupar por Gestor - Semelhante a coluna gestor do DataSource1
        public string colGestorAuxiliar; 

        public DataHolder2() { }
        //
        public DataHolder2(string A, string B, string C,
                           decimal? D, decimal? E, decimal? F, decimal? G, decimal? H, decimal? I,
                           string J, 
                           decimal? K, 
                           string L,
                           decimal? M, decimal? N, decimal? O,
                           string colGestorAuxiliar
                           ) {
            this.A = A;
            this.B = B;
            this.C = C;
            this.D = D;
            this.E = E;
            this.F = F;
            this.G = G;
            this.H = H;
            this.I = I;
            this.J = J;
            this.K = K;
            this.L = L;
            this.M = M;
            this.N = N;
            this.O = O;
            this.colGestorAuxiliar = colGestorAuxiliar; // Coluna Auxiliar para poder agrupar por Gestor - Semelhante a coluna gestor do DataSource1
        }
    }

    private class DataHolderEstrategia {
        public string C;
        //
        public decimal? D;
        public decimal? E;
        public decimal? F;
        public decimal? G;
        //

        public DataHolderEstrategia() { }

        public DataHolderEstrategia(string C,
                                    decimal? D, decimal? E, decimal? F, decimal? G) {
            this.C = C;
            this.D = D;
            this.E = E;
            this.F = F;
            this.G = G;
        }
    }

    private class DataHolderClasse {
        public string I;
        //
        public decimal? J;
        public decimal? K;
        //
        public DataHolderClasse() { }
        //
        public DataHolderClasse(string I,
                                decimal? J, decimal? K) {
            this.I = I;
            this.J = J;
            this.K = K;
        }
    }

    private class DataHolderGestor {
        public string M;
        //
        public decimal? N;
        public decimal? O;
        //
        public DataHolderGestor() { }
        //
        public DataHolderGestor(string M,
                                decimal? N, decimal? O) {
            this.M = M;
            this.N = N;
            this.O = O;
        }
    }

    #endregion
    
    private void TrataTravamentoCampos() {
        Usuario usuario = new Usuario();
        usuario.BuscaUsuario(HttpContext.Current.User.Identity.Name);
        int tipoTrava = usuario.TipoTrava.Value;

        if (tipoTrava == (int)TipoTravaUsuario.TravaCliente) {
            btnEditCodigo.Enabled = false;

            PermissaoCliente permissaoCliente = new PermissaoCliente();
            int? idCliente = permissaoCliente.RetornaClienteAssociado(HttpContext.Current.User.Identity.Name);

            if (idCliente.HasValue) {
                Cliente cliente = new Cliente();

                List<esQueryItem> campos = new List<esQueryItem>();
                campos.Add(cliente.Query.Apelido);
                cliente.LoadByPrimaryKey(campos, idCliente.Value);
                string apelido = cliente.Apelido;

                btnEditCodigo.Text = Convert.ToString(idCliente.Value);
                textNome.Text = apelido;
            }
        }
    }

    protected void ASPxCallback1_Callback(object source, DevExpress.Web.CallbackEventArgs e) {
        string nome = "";
        if (!String.IsNullOrEmpty(e.Parameter) && e.Parameter != "null") {
            int idCliente = Convert.ToInt32(e.Parameter);
            Cliente cliente = new Cliente();
            cliente.LoadByPrimaryKey(idCliente);

            if (cliente.str.Apelido != "") {
                if (cliente.IsAtivo) {
                    PermissaoCliente permissaoCliente = new PermissaoCliente();
                    nome = permissaoCliente.RetornaAcessoCliente(idCliente, HttpContext.Current.User.Identity.Name)
                            ? cliente.str.Apelido : "no_access";
                }
                else {
                    nome = "no_active";
                }
            }
        }
        e.Result = nome;
    }

    protected void EsDSCliente_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e) {
        ClienteCollection clienteCollection = new ClienteCollection();
        clienteCollection.BuscaClientesComAcesso(HttpContext.Current.User.Identity.Name);

        e.Collection = clienteCollection;
    }

    /// <summary>
    /// Redireciona para ExibeExportacaoCustom para exportar a planilha excel
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnExcel_Click(object sender, EventArgs e) {
        #region Campos obrigatórios
        List<Control> controles = new List<Control>(new Control[] { 
    	    this.btnEditCodigo, this.textData });

        if (base.TestaObrigatorio(controles) != "") {
            throw new Exception("Campos com * são obrigatórios!");
        }
        #endregion

        // Armazena os valores para posterior uso
        this.idCliente = Convert.ToInt32(this.btnEditCodigo.Text.Trim());
        this.data = Convert.ToDateTime(this.textData.Text);
        //
        Cliente cliente = new Cliente();
        List<esQueryItem> campos = new List<esQueryItem>();
        campos.Add(cliente.Query.DataDia);
        cliente.LoadByPrimaryKey(this.idCliente);
        //
        this.dataDiaCliente = cliente.DataDia.Value;

        //
        MemoryStream msExcel = new MemoryStream();
        string nomeArquivoExcel;

        //
        this.ExportaExcel(out msExcel, out nomeArquivoExcel);
        //

        // Salva Memory Stream do Arquivo xls na Session
        Session["streamExcel"] = msExcel;
        Session["nomeArquivoExcel"] = nomeArquivoExcel;
        //
        //Session["textData"] = this.textDataCota.Text;

        string redirect = "~/InterfacesCustom/ExibeExportacaoCustom.aspx?Owner=" + (int)OwnerExportacao.Quadrante;

        Response.Redirect(redirect, true);
    }

    /// <summary>
    /// Controla a montagem dos elementos do Excel
    /// </summary>
    /// <param name="msExcel"></param>
    /// <param name="nomeArquivoExcel"></param>
    private void ExportaExcel(out MemoryStream msExcel, out string nomeArquivoExcel) {
        // out
        msExcel = new MemoryStream();
        nomeArquivoExcel = "Quadrante.xls";
        //

        Spreadsheet document = new Bytescout.Spreadsheet.Spreadsheet();

        string pathModelos = AppDomain.CurrentDomain.BaseDirectory + "\\Relatorios\\Modelos";
        string pathPlanilhaModelo = String.Format("{0}\\ModeloQuadrante.xlsx", pathModelos);

        document.LoadFromFile(pathPlanilhaModelo);

        Worksheet worksheet = document.Workbook.Worksheets[0];
        worksheet.Name = "Plan_1";
        //
        worksheet.ViewOptions.ShowZeroValues = true;

/*-----------------------------------------------------*/
        #region Cabecalho1
        this.SetCabecalho1(worksheet);
        #endregion

        #region DataPosicao
        this.SetDataPosicao(worksheet);
        #endregion

        #region Cabecalho2
        //this.SetCabecalho2(worksheet);
        #endregion

        /* Inicia de baixo para cima a montagem dos dados 
         * Começa de tras para frente por causa da linha inicial de Dados
         */
/*-----------------------------------------------------*/

        /* Queries */
        this.MontaConsultaDataHolder1();
        //
        this.MontaConsultaDataHolder2(); // chamada aqui pois consulta atual depende da anterior
        //
        this.MontaConsultaEstrategiaClasseGestor();
        //

        #region Estrategia/Classe/Gestor
        this.SetEstrategiaClasseGestor(worksheet);
        #endregion

        #region DataHolder2
        this.SetDataHolder2(worksheet);
        #endregion

        #region DataHolder1
        this.SetDataHolder1(worksheet);
        #endregion

        #region Totais Sum Product
        this.SetTotais(worksheet);
        #endregion

/*-----------------------------------------------------*/

        #region Esconde Planilha Demo do componente
        const string BYTESCOUT_WORKSHEET_NAME = "Bytescout Spreadsheet SDK";
        worksheet = document.Workbook.Worksheets.ByName(BYTESCOUT_WORKSHEET_NAME);
        if (worksheet == null) {
            worksheet = document.Workbook.Worksheets.Add(BYTESCOUT_WORKSHEET_NAME);
        }
        worksheet.Visible = Bytescout.Spreadsheet.Constants.SHEETVISIBILITY.Hidden;
        #endregion

        //
        document.SaveToStreamXLS(msExcel);
        //
        document.Close();
    }

    #region Partes do Excel
    private void SetCabecalho1(Worksheet worksheet) {
                
        // Monta Valores Cabecalho
        //Cabecalho1 c = new Cabecalho1(1.0M, 1.12M, 20.358M, 43.56787M, 5.0M, "CDI", "CDI");

        //decimal? d = null;
        //Cabecalho1 c = new Cabecalho1(d, d, d, d, null, "CDI", "CDI");
        //
        //worksheet.Cell("B2").ValueDataTypeByNumberFormatString = Bytescout.Spreadsheet.Constants.NumberFormatType.Percent;
        //worksheet.Cell("B2").ValueDataType = Bytescout.Spreadsheet.Constants.DataType.NUMERIC;        
        //worksheet.Cell("B2").NumberFormatString = "0.00000%";
        /*  -------------------------------------------------------------------------------*/

        Cabecalho1 c = new Cabecalho1();

        #region B2 - RetornoNominal
        TabelaRetornoEsperadoCollection tre = new TabelaRetornoEsperadoCollection();
        tre.Query.Select(tre.Query.Valor)
                .Where(tre.Query.IdAtivo == this.idCliente,
                       tre.Query.TipoAtivo == 4, // TipoAtivo = Carteira
                       tre.Query.DataReferencia <= this.data)
                .OrderBy(tre.Query.DataReferencia.Descending);

        //
        decimal? retornoNominal = null;
        if (tre.Query.Load()) {
            if (tre[0].Valor.HasValue) {
                retornoNominal = tre[0].Valor.Value;
            }
        }
        if (retornoNominal.HasValue) {
            c.B2 = retornoNominal.Value;
        }
        #endregion

        #region B3 - Retorno Stress

        TabelaStressCollection ts = new TabelaStressCollection();
        ts.Query.Select(ts.Query.Stress)
                .Where(ts.Query.IdAtivo == this.idCliente,
                       ts.Query.TipoAtivo == 3, // TipoAtivo = Carteira
                       ts.Query.DataReferencia <= this.data)
                .OrderBy(ts.Query.DataReferencia.Descending);

        //
        decimal? stress = null;
        if (ts.Query.Load()) {
            if (ts[0].Stress.HasValue) {
                stress = ts[0].Stress.Value;
            }
        }
        if (stress.HasValue) {
            c.B3 = stress.Value;
        }
        #endregion
        
        //
        c.C2 = "";        
        //
        #region B4
        TabelaTaxaAdministracaoCollection t = new TabelaTaxaAdministracaoCollection();
        t.Query.Where(t.Query.IdCarteira == this.idCliente,
                      t.Query.Taxa.IsNotNull(),
                      t.Query.DataReferencia <= this.data,
                      t.Query.IdCadastro == (int)TipoCadastroAdministracao.TaxaGestao
                      );
        t.Query.OrderBy(t.Query.DataReferencia.Descending);
        //
        decimal? taxa = 0.00M;
        if (t.Query.Load()) {
            if (t[0].Taxa.HasValue) {
                taxa = t[0].Taxa.Value;
            }
        }
        c.B4 = taxa;
        #endregion        
        //
        #region B5
        TabelaTaxaPerformanceCollection tp = new TabelaTaxaPerformanceCollection();
        tp.Query.Where(tp.Query.IdCarteira == this.idCliente,
                       tp.Query.TaxaPerformance.IsNotNull(),
                       tp.Query.DataReferencia <= this.data
                      );
        tp.Query.OrderBy(tp.Query.DataReferencia.Descending);
        //
        decimal? taxaPerformance = 0.00M;
        //decimal? percentualIndice = null;
        //string descricaoIndice = String.Empty;        
        //
        if (tp.Query.Load()) {
            if (tp[0].TaxaPerformance.HasValue) {
                taxaPerformance = tp[0].TaxaPerformance.Value;
                //percentualIndice = tp[0].PercentualIndice.Value;
                //
                //descricaoIndice = tp[0].UpToIndiceByIdIndice.str.Descricao.Trim();
                //idIndice = tp[0].UpToIndiceByIdIndice.IdIndice.Value;
            }
        }
        c.B5 = taxaPerformance;
        #endregion        
        //
        #region B6
        Carteira cart = new Carteira();
        cart.Query.Select(cart.Query.IdIndiceBenchmark)
            .Where(cart.Query.IdCarteira == this.idCliente);

        cart.Query.Load();

        int idIndice = cart.IdIndiceBenchmark.Value;
        Indice i = cart.UpToIndiceByIdIndiceBenchmark;
        string descricaoIndice = i.Descricao.Trim();
        this.tipoIndice = i.Tipo.Value;

        // Ajusta label Dataholder1
        worksheet.Cell("K12").ValueAsString = this.tipoIndice == (int)TipoIndice.Decimal ? "(-) Benchmark" : "% Benchmark";
        
        c.B6 = descricaoIndice;
        #endregion
        //
        #region B7
        TabelaRetornoEsperadoCollection tr = new TabelaRetornoEsperadoCollection();
        tr.Query.Select(tr.Query.Valor)
                .Where(tr.Query.IdAtivo == idIndice,
                       tr.Query.TipoAtivo == 3, // TipoAtivo = Indice
                       tr.Query.DataReferencia <= this.data)
                .OrderBy(tr.Query.DataReferencia.Descending);

        //        
        if (tr.Query.Load()) {
            if (tr[0].Valor.HasValue) {
                this.retornoNominal = tr[0].Valor.Value;
            }
        }

        if (this.retornoNominal.HasValue) {
            decimal I3 = this.retornoNominal.Value;
            //
            worksheet.Cell("B7").NumberFormatString = "0.00%";
            //worksheet.Cell("B7").Value = String.Format("=( (B2*({0})-({0}) )/(1-B5)+B4+({0})) / ({0})", I3);
            worksheet.Cell("B7").Value = String.Format("=((B2*({0}/{1})-({0}/{1}))/(1-B5)+B4+({0}/{1}) ) /({0}/{1})", I3, 100);

            
        }

        #endregion

        // Celulas no Modelo ja estão com formato percent com 2 casas
        if (c.B2.HasValue) {
            worksheet.Cell("B2").ValueAsDouble = Convert.ToDouble(c.B2 / 100);
        }
        if (c.B3.HasValue) {
            worksheet.Cell("B3").ValueAsDouble = Convert.ToDouble(c.B3 / 100);
        }
        worksheet.Cell("B4").ValueAsDouble = Convert.ToDouble(c.B4/100);
        worksheet.Cell("B5").ValueAsDouble = Convert.ToDouble(c.B5/100);
        worksheet.Cell("B6").ValueAsString = c.B6;
        //worksheet.Cell("B7").ValueAsDouble = Convert.ToDouble(c.B7/100);
        //
        worksheet.Cell("B2").AlignmentHorizontal = Bytescout.Spreadsheet.Constants.AlignmentHorizontal.Right;
        worksheet.Cell("B3").AlignmentHorizontal = Bytescout.Spreadsheet.Constants.AlignmentHorizontal.Right;
        worksheet.Cell("B4").AlignmentHorizontal = Bytescout.Spreadsheet.Constants.AlignmentHorizontal.Right;
        worksheet.Cell("B5").AlignmentHorizontal = Bytescout.Spreadsheet.Constants.AlignmentHorizontal.Right;
        worksheet.Cell("B6").AlignmentHorizontal = Bytescout.Spreadsheet.Constants.AlignmentHorizontal.Right;
        worksheet.Cell("B7").AlignmentHorizontal = Bytescout.Spreadsheet.Constants.AlignmentHorizontal.Right;

        worksheet.Cell("C2").Value = c.C2;
        //worksheet.Cell("C6").Value = c.C6;
    }

    private void SetDataPosicao(Worksheet worksheet) {
        worksheet.Cell("D3").ValueAsDateTime = Convert.ToDateTime(this.textData.Text);
    }

    [Obsolete("Retirado")]
    private void SetCabecalho2(Worksheet worksheet) {
        //// Monta Valores Cabecalho
        ////Cabecalho2 c = new Cabecalho2();        
        ////Cabecalho2 c = new Cabecalho2(1.0M, 72000, 64500, 2.56M, 5.0M);
        
        //CotacaoIndice cotacaoIndice = new CotacaoIndice();
        //DateTime dataCabecalho = this.data.AddDays(-1);
        
        //if (cotacaoIndice.LoadByPrimaryKey(dataCabecalho, ListaIndiceFixo.CDI)) {
        //    c.I3 = cotacaoIndice.Valor.Value;
        //}

        //// Provisório
        //c.I4 = 0;
        //c.I5 = 0;
        //c.I6 = 0;
        //c.I7 = 0;
        ////

        //if (c.I3.HasValue) {
        //    worksheet.Cell("I3").ValueAsDouble = Convert.ToDouble(c.I3 / 100);
        //}

        //worksheet.Cell("I4").Value = c.I4;
        //worksheet.Cell("I5").Value = c.I5;

        //worksheet.Cell("I6").ValueAsDouble = Convert.ToDouble(c.I6/100);
        //worksheet.Cell("I7").ValueAsDouble = Convert.ToDouble(c.I7/100);
    }

    /// <summary>
    /// Presupoe que dList1 variavel private esteja antecipadamente preenchida com os dados
    /// A montagem dos dados é feito na função MontaConsultaDataHolder1
    /// </summary>
    /// <param name="worksheet"></param>
    private void SetDataHolder1(Worksheet worksheet) {        
        int linha = 14;
        for (int i = 0; i < this.dList1.Count; i++) {
            // Add new row
            worksheet.Rows.Insert(linha, 1);
            //
            worksheet.Rows[linha][0].ValueAsString = this.dList1[i].A;
            worksheet.Rows[linha][1].ValueAsString = this.dList1[i].B;
            worksheet.Rows[linha][2].ValueAsString = this.dList1[i].C;

            if (this.dList1[i].D.HasValue) {
                worksheet.Rows[linha][3].ValueAsDateTime = this.dList1[i].D.Value;
            }
            if (this.dList1[i].E.HasValue) {
                worksheet.Rows[linha][4].ValueAsDateTime = this.dList1[i].E.Value;
            }
            
            worksheet.Rows[linha][5].Value = this.dList1[i].F;            
            worksheet.Rows[linha][6].Value = this.dList1[i].G;                        
            worksheet.Rows[linha][7].Value = this.dList1[i].H;
            //
            if (this.dList1[i].I.HasValue) {
                worksheet.Rows[linha][8].NumberFormatString = "0.00%";
                worksheet.Rows[linha][8].Value = Convert.ToDouble(this.dList1[i].I / 100);
            }
            //
            if (this.dList1[i].J.HasValue) {
                worksheet.Rows[linha][9].NumberFormatString = "0.00%";
                worksheet.Rows[linha][9].Value = Convert.ToDouble(this.dList1[i].J / 100);
            }
            //
            if (this.dList1[i].K.HasValue) {
                worksheet.Rows[linha][10].NumberFormatString = "0.00%";
                worksheet.Rows[linha][10].Value = Convert.ToDouble(this.dList1[i].K / 100);
            }
            //
            worksheet.Rows[linha][11].NumberFormatString = "0.00%";
            worksheet.Rows[linha][11].Value = Convert.ToDouble(this.dList1[i].L/100);
            //
            worksheet.Rows[linha][12].NumberFormatString = "0.00%";
            worksheet.Rows[linha][12].ValueAsDouble = Convert.ToDouble(this.dList1[i].M / 100);
            //
            worksheet.Rows[linha][13].Value = this.dList1[i].N;
            worksheet.Rows[linha][14].Value = this.dList1[i].O;
            worksheet.Rows[linha][15].Value = this.dList1[i].P;
            //
            worksheet.Rows[linha][11].AlignmentHorizontal = Bytescout.Spreadsheet.Constants.AlignmentHorizontal.Centered;
            worksheet.Rows[linha][12].AlignmentHorizontal = Bytescout.Spreadsheet.Constants.AlignmentHorizontal.Centered;
            worksheet.Rows[linha][13].AlignmentHorizontal = Bytescout.Spreadsheet.Constants.AlignmentHorizontal.Centered;
            worksheet.Rows[linha][14].AlignmentHorizontal = Bytescout.Spreadsheet.Constants.AlignmentHorizontal.Centered;
            worksheet.Rows[linha][15].AlignmentHorizontal = Bytescout.Spreadsheet.Constants.AlignmentHorizontal.Centered;
            //
            worksheet.Rows[linha].Font = new Font("Calibri", 11, FontStyle.Regular);
            //
            linha++;
        }

        ///* Total */
        //int inicio = 15;
        //int final = 14 + this.dList1.Count;
        ////
        //int inicio1 = 23 + this.dList1.Count;
        //int final1 = 22 + this.dList1.Count + this.dList2.Count;
        //
        //worksheet.Rows[linha][11].NumberFormatString = "0.00%";
        //worksheet.Rows[linha][11].Value = String.Format("=SUMPRODUCT(${0}${1}:${0}${2};${3}${4}:${3}${5})+$B$4", "L", inicio, final,
        //                                                                                                         "E", inicio1, final1);
        //worksheet.Rows[linha][12].NumberFormatString = "0.00%";
        //worksheet.Rows[linha][12].Value = String.Format("=SUMPRODUCT(${0}${1}:${0}${2};${3}${4}:${3}${5})+$B$4", "M", inicio, final,
        //                                                                                                         "E", inicio1, final1);
        //worksheet.Rows[linha][12].NumberFormatString = "0.00%";
        //worksheet.Rows[linha][12].Value = "=SUMPRODUCT($M$15:$M$15;$E$34:$E$34)";

        //worksheet.Rows[linha][12].NumberFormatString = "0.00%";
        //worksheet.Rows[linha][12].Value = String.Format("=SUM({0}{1}:{0}{2})", "E", inicio, final);

        
    }

    /// <summary>
    /// Montagem dos Dados do DataHolder1
    /// </summary>
    /// modifies: this.dList1 private
    private void MontaConsultaDataHolder1() {
       
        bool historico = this.data < this.dataDiaCliente;

        if (historico) 
        {
            #region Bolsa
            PosicaoBolsaHistoricoCollection posicaoBolsaCollection = new PosicaoBolsaHistoricoCollection();
            //
            PosicaoBolsaHistoricoQuery posicaoBolsaQuery = new PosicaoBolsaHistoricoQuery("P");
            AtivoBolsaQuery ativoBolsaQuery = new AtivoBolsaQuery("A");
            EstrategiaQuery estrategiaQuery = new EstrategiaQuery("E");

            posicaoBolsaQuery.Select(estrategiaQuery.IdEstrategia,
                                     estrategiaQuery.Descricao,
                                     posicaoBolsaQuery.CdAtivoBolsa,
                                     ativoBolsaQuery.Descricao.As("DescricaoAtivoBolsa"),
                                     posicaoBolsaQuery.IdAgente
                                    );
            //
            posicaoBolsaQuery.InnerJoin(ativoBolsaQuery).On(ativoBolsaQuery.CdAtivoBolsa == posicaoBolsaQuery.CdAtivoBolsa);
            posicaoBolsaQuery.InnerJoin(estrategiaQuery).On(estrategiaQuery.IdEstrategia == ativoBolsaQuery.IdEstrategia);
            //
            posicaoBolsaQuery.Where(posicaoBolsaQuery.IdCliente == this.idCliente,
                                    posicaoBolsaQuery.DataHistorico == this.data,
                                    posicaoBolsaQuery.Quantidade != 0
                                    );

            //posicaoBolsaQuery.GroupBy(estrategiaQuery.Descricao, posicaoBolsaQuery.CdAtivoBolsa);
            //
            posicaoBolsaCollection.Load(posicaoBolsaQuery);

            foreach (PosicaoBolsaHistorico posicaoBolsa in posicaoBolsaCollection) 
            {
                int idEstrategia = Convert.ToInt32(posicaoBolsa.GetColumn(EstrategiaMetadata.ColumnNames.IdEstrategia));
                string estrategiaDescricao = Convert.ToString(posicaoBolsa.GetColumn(EstrategiaMetadata.ColumnNames.Descricao));
                string cdAtivoBolsa = posicaoBolsa.CdAtivoBolsa;
                string descricaoAtivoBolsa = Convert.ToString(posicaoBolsa.GetColumn("DescricaoAtivoBolsa"));

                DataHolder1 d = new DataHolder1();
                //
                d.A = estrategiaDescricao;
                d.B = cdAtivoBolsa;
                d.C = descricaoAtivoBolsa;
                d.D = null;
                d.E = null;
                d.F = null;
                d.G = null;
                d.H = null;

                #region Stress
                TabelaStressCollection ts = new TabelaStressCollection();
                ts.Query.Select(ts.Query.Stress)
                        .Where(ts.Query.IdAtivo.ToUpper() == idEstrategia.ToString(),
                               ts.Query.TipoAtivo == 1, // TipoAtivo = Estrategia
                               ts.Query.DataReferencia <= this.data)
                        .OrderBy(ts.Query.DataReferencia.Descending);

                //
                decimal? stress = null;
                if (ts.Query.Load()) {
                    if (ts[0].Stress.HasValue) {
                        stress = ts[0].Stress.Value;
                    }
                }
                if (stress.HasValue) {
                    d.I = stress.Value;
                }
                #endregion

                #region RetornoNominal
                TabelaRetornoEsperadoCollection tr = new TabelaRetornoEsperadoCollection();
                tr.Query.Select(tr.Query.Valor)
                        .Where(tr.Query.IdAtivo.ToUpper() == cdAtivoBolsa.Trim(),
                               tr.Query.TipoAtivo == 2, // TipoAtivo = Ação
                               tr.Query.DataReferencia <= this.data)
                        .OrderBy(tr.Query.DataReferencia.Descending);

                //
                decimal? retornoNominal = null;
                if (tr.Query.Load()) {
                    if (tr[0].Valor.HasValue) {
                        retornoNominal = tr[0].Valor.Value;
                    }
                }
                if (retornoNominal.HasValue) {
                    
                    decimal? puMedio = null;
                    CotacaoBolsa c = new CotacaoBolsa();
                    if (c.LoadByPrimaryKey(this.data, cdAtivoBolsa.Trim())) {
                        puMedio = c.PUMedio.Value;
                    }
                    if (puMedio.HasValue && puMedio != 0) {
                        d.J = retornoNominal.Value / puMedio.Value;
                    }                    
                }
                #endregion

                #region BenchMark
                if (this.tipoIndice == (int)TipoIndice.Decimal) {

                    if (d.J.HasValue && this.retornoNominal.HasValue) {                        
                        decimal valorNominalIndice = this.retornoNominal.Value / 100;                        
                        //
                        d.K = d.J.Value /100 - valorNominalIndice;
                    }                      
                }
                else if (this.tipoIndice == (int)TipoIndice.Percentual) {
                    if (d.J.HasValue && this.retornoNominal.HasValue) {
                        decimal valorNominalIndice = this.retornoNominal.Value / 100;
                        //
                        d.K = (d.J.Value / 100) / valorNominalIndice;
                    }
                }
                #endregion

                d.L = null;
                d.M = null;
                //
                d.N = "0,00%";
                //
                d.O = posicaoBolsa.UpToAgenteMercadoByIdAgente.Nome.Trim();
                d.P = posicaoBolsa.UpToAgenteMercadoByIdAgente.Nome.Trim();
                //
                this.dList1.Add(d);
            }
            #endregion

            #region Fundo
            PosicaoFundoHistoricoCollection posicaoFundoCollection = new PosicaoFundoHistoricoCollection();
            PosicaoFundoHistoricoQuery posicaoFundoQuery = new PosicaoFundoHistoricoQuery("P");
            CarteiraQuery carteiraQuery = new CarteiraQuery("C");
            estrategiaQuery = new EstrategiaQuery("E");

            posicaoFundoQuery.Select(estrategiaQuery.Descricao,
                                     posicaoFundoQuery.IdCarteira,
                                     carteiraQuery.Apelido,
                                     posicaoFundoQuery.DataAplicacao,
                                     carteiraQuery.DiasCotizacaoResgate,
                                     carteiraQuery.DiasLiquidacaoResgate,
                                     carteiraQuery.IdAgenteAdministrador,
                                     carteiraQuery.IdAgenteCustodiante
                                    );
            posicaoFundoQuery.InnerJoin(carteiraQuery).On(carteiraQuery.IdCarteira == posicaoFundoQuery.IdCarteira);
            posicaoFundoQuery.InnerJoin(estrategiaQuery).On(estrategiaQuery.IdEstrategia == carteiraQuery.IdEstrategia);
            //
            posicaoFundoQuery.Where(posicaoFundoQuery.IdCliente == this.idCliente,
                                    posicaoFundoQuery.DataHistorico == this.data,
                                    posicaoFundoQuery.Quantidade != 0
                                   );
            //
            posicaoFundoCollection.Load(posicaoFundoQuery);

            foreach (PosicaoFundoHistorico posicaoFundo in posicaoFundoCollection) {
                string estrategiaDescricao = Convert.ToString(posicaoFundo.GetColumn(EstrategiaMetadata.ColumnNames.Descricao));
                string apelido = Convert.ToString(posicaoFundo.GetColumn(CarteiraMetadata.ColumnNames.Apelido));

                DataHolder1 d = new DataHolder1();
                //
                d.A = estrategiaDescricao;
                d.B = posicaoFundo.IdCarteira.Value.ToString();
                d.C = apelido;
                d.D = posicaoFundo.DataAplicacao.Value;
                d.E = null;
                d.F = Calendario.NumeroDias(posicaoFundo.DataAplicacao.Value, this.data);
                d.G = Convert.ToInt32(posicaoFundo.GetColumn(CarteiraMetadata.ColumnNames.DiasCotizacaoResgate));
                d.H = Convert.ToInt32(posicaoFundo.GetColumn(CarteiraMetadata.ColumnNames.DiasLiquidacaoResgate));

                #region Stress
                string idCarteira = posicaoFundo.IdCarteira.Value.ToString();
                TabelaStressCollection ts = new TabelaStressCollection();
                ts.Query.Select(ts.Query.Stress)
                        .Where(ts.Query.IdAtivo == idCarteira,
                               ts.Query.TipoAtivo == 2, // TipoAtivo = Fundo
                               ts.Query.DataReferencia <= this.data)
                        .OrderBy(ts.Query.DataReferencia.Descending);
                                                            
                //
                decimal? stress = null;
                if (ts.Query.Load()) {
                    if (ts[0].Stress.HasValue) {
                        stress = ts[0].Stress.Value;
                    }
                }
                if (stress.HasValue) {
                    d.I = stress.Value;
                }
                #endregion

                #region RetornoNominal
                TabelaRetornoEsperadoCollection tr = new TabelaRetornoEsperadoCollection();
                tr.Query.Select(tr.Query.Valor)
                        .Where(tr.Query.IdAtivo == idCarteira,
                               tr.Query.TipoAtivo == 1, // TipoAtivo = Fundo
                               tr.Query.DataReferencia <= this.data)
                        .OrderBy(tr.Query.DataReferencia.Descending);

                //
                decimal? retornoNominal = null;
                if (tr.Query.Load()) {
                    if (tr[0].Valor.HasValue) {
                        retornoNominal = tr[0].Valor.Value;
                    }
                }
                if (retornoNominal.HasValue) {
                    d.J = retornoNominal.Value;
                }
                #endregion

                #region BenchMark
                if (this.tipoIndice == (int)TipoIndice.Decimal) {

                    if (d.J.HasValue && this.retornoNominal.HasValue) {
                        decimal valorNominalIndice = this.retornoNominal.Value / 100;
                        //
                        d.K = d.J.Value / 100 - valorNominalIndice;
                    }
                }
                else if (this.tipoIndice == (int)TipoIndice.Percentual) {
                    if (d.J.HasValue && this.retornoNominal.HasValue) {
                        decimal valorNominalIndice = this.retornoNominal.Value / 100;
                        //
                        d.K = (d.J.Value / 100) / valorNominalIndice;
                    }
                }
                #endregion

                #region TaxaAdministracao
                TabelaTaxaAdministracaoCollection t = new TabelaTaxaAdministracaoCollection();
                t.Query.Where(t.Query.IdCarteira == posicaoFundo.IdCarteira.Value,
                              t.Query.Taxa.IsNotNull(),
                              t.Query.DataReferencia <= this.data
                              );
                //
                decimal? taxa = 0.00M;
                if (t.Query.Load()) {
                    if (t[0].Taxa.HasValue) {
                        taxa = t[0].Taxa.Value;
                    }
                }

                d.L = taxa;
                d.M = taxa;
                #endregion

                #region TabelaTaxaPerformance
                TabelaTaxaPerformanceCollection tp = new TabelaTaxaPerformanceCollection();
                tp.Query.Where(tp.Query.IdCarteira == posicaoFundo.IdCarteira.Value,
                               tp.Query.TaxaPerformance.IsNotNull(),
                               tp.Query.DataReferencia <= this.data
                              );
                //
                decimal? taxaPerformance = 0.00M;
                if (tp.Query.Load()) {
                    if (tp[0].TaxaPerformance.HasValue) {
                        taxaPerformance = tp[0].TaxaPerformance.Value;
                    }
                }

                d.N = taxaPerformance.Value.ToString("N2") + "%";
                //
                #endregion

                d.O = posicaoFundo.UpToCarteiraByIdCarteira.UpToAgenteMercadoByIdAgenteAdministrador.Nome.Trim();
                d.P = posicaoFundo.UpToCarteiraByIdCarteira.UpToAgenteMercadoByIdAgenteGestor.Nome.Trim();
                //
                this.dList1.Add(d);
            }
            #endregion

            #region RendaFixa
            PosicaoRendaFixaHistoricoCollection posicaoRendaFixaCollection = new PosicaoRendaFixaHistoricoCollection();
            PosicaoRendaFixaHistoricoQuery posicaoRendaFixaQuery = new PosicaoRendaFixaHistoricoQuery("P");
            TituloRendaFixaQuery tituloRendaFixaQuery = new TituloRendaFixaQuery("T");
            estrategiaQuery = new EstrategiaQuery("E");

            posicaoRendaFixaQuery.Select(estrategiaQuery.IdEstrategia,
                                         estrategiaQuery.Descricao,
                                         posicaoRendaFixaQuery.IdTitulo,
                                         tituloRendaFixaQuery.Descricao.As("DescricaoTituloRendaFixa"),
                                         posicaoRendaFixaQuery.DataOperacao,
                                         posicaoRendaFixaQuery.DataVencimento,
                                         tituloRendaFixaQuery.IdEmissor,
                                         tituloRendaFixaQuery.IdIndice,
                                         tituloRendaFixaQuery.Percentual,
                                         tituloRendaFixaQuery.Taxa
                                         );

            posicaoRendaFixaQuery.InnerJoin(tituloRendaFixaQuery).On(tituloRendaFixaQuery.IdTitulo == posicaoRendaFixaQuery.IdTitulo);
            posicaoRendaFixaQuery.InnerJoin(estrategiaQuery).On(estrategiaQuery.IdEstrategia == tituloRendaFixaQuery.IdEstrategia);
            //
            posicaoRendaFixaQuery.Where(posicaoRendaFixaQuery.IdCliente == this.idCliente,
                                        posicaoRendaFixaQuery.DataHistorico == this.data,
                                        posicaoRendaFixaQuery.Quantidade != 0
                                        );
            //
            posicaoRendaFixaCollection.Load(posicaoRendaFixaQuery);

            foreach (PosicaoRendaFixaHistorico posicaoRendaFixa in posicaoRendaFixaCollection) 
            {
                int idEstrategia = Convert.ToInt32(posicaoRendaFixa.GetColumn(EstrategiaMetadata.ColumnNames.IdEstrategia));
                string estrategiaDescricao = Convert.ToString(posicaoRendaFixa.GetColumn(EstrategiaMetadata.ColumnNames.Descricao));
                string descricaoTitulo = Convert.ToString(posicaoRendaFixa.GetColumn("DescricaoTituloRendaFixa"));
                int idTitulo = posicaoRendaFixa.IdTitulo.Value;

                TituloRendaFixa tituloRendaFixa = new TituloRendaFixa();
                tituloRendaFixa.LoadByPrimaryKey(idTitulo);

                short? idIndice = tituloRendaFixa.IdIndice;
                decimal? percentual = tituloRendaFixa.Percentual;
                decimal? taxa = tituloRendaFixa.Taxa;

                DataHolder1 d = new DataHolder1();
                //
                d.A = estrategiaDescricao;
                d.B = posicaoRendaFixa.IdTitulo.Value.ToString();
                d.C = descricaoTitulo;
                d.D = posicaoRendaFixa.DataOperacao.Value;
                d.E = posicaoRendaFixa.DataVencimento.Value;
                d.F = Calendario.NumeroDias(posicaoRendaFixa.DataOperacao.Value, this.data);
                d.G = null;
                d.H = null;

                #region Stress
                TabelaStressCollection ts = new TabelaStressCollection();
                ts.Query.Select(ts.Query.Stress)
                        .Where(ts.Query.IdAtivo == idEstrategia.ToString(),
                               ts.Query.TipoAtivo == 1, // TipoAtivo = Estrategia
                               ts.Query.DataReferencia <= this.data)
                        .OrderBy(ts.Query.DataReferencia.Descending);

                //
                decimal? stress = null;
                if (ts.Query.Load()) {
                    if (ts[0].Stress.HasValue) {
                        stress = ts[0].Stress.Value;
                    }
                }
                if (stress.HasValue) {
                    d.I = stress.Value;
                }
                #endregion

                #region RetornoNominal
                TimeSpan numeroDias = this.data - posicaoRendaFixa.DataOperacao.Value;
                TabelaEscalonamentoRFCollection tabelaEscalonamentoRFCollection = new TabelaEscalonamentoRFCollection();
                tabelaEscalonamentoRFCollection.Query.Select(tabelaEscalonamentoRFCollection.Query.Taxa);
                tabelaEscalonamentoRFCollection.Query.Where(tabelaEscalonamentoRFCollection.Query.IdTitulo.Equal(idTitulo),
                                                            tabelaEscalonamentoRFCollection.Query.Prazo.LessThanOrEqual(numeroDias.Days));
                tabelaEscalonamentoRFCollection.Query.OrderBy(tabelaEscalonamentoRFCollection.Query.Prazo.Descending);
                tabelaEscalonamentoRFCollection.Query.Load();

                if (tabelaEscalonamentoRFCollection.Count > 0)
                {
                    percentual = tabelaEscalonamentoRFCollection[0].Taxa.Value;
                }

                if (idIndice.HasValue || percentual.HasValue)
                {
                    if (!percentual.HasValue)
                    {
                        percentual = 100M;
                    }

                    TabelaRetornoEsperadoCollection tr = new TabelaRetornoEsperadoCollection();
                    tr.Query.Select(tr.Query.Valor);
                    tr.Query.Where(tr.Query.TipoAtivo == 3, // TipoAtivo = RF
                                   tr.Query.DataReferencia <= this.data,
                                   tr.Query.IdAtivo == idIndice.Value.ToString());
                    tr.Query.OrderBy(tr.Query.DataReferencia.Descending);

                    //
                    decimal? retornoNominal = null;
                    if (tr.Query.Load())
                    {
                        if (tr[0].Valor.HasValue)
                        {
                            retornoNominal = tr[0].Valor.Value;
                        }
                    }
                    if (retornoNominal.HasValue)
                    {
                        decimal fatorIndice = (decimal)(retornoNominal.Value / 100M) * (percentual.Value / 100M) + 1M;
                        decimal fatorTaxa = taxa.HasValue ? taxa.Value / 100M + 1M : 1M;

                        decimal retorno = ((fatorIndice * fatorTaxa) - 1M) * 100M;

                        d.J = retorno;
                    }
                }
                else if (posicaoRendaFixa.TaxaOperacao.HasValue)
                {
                    d.J = posicaoRendaFixa.TaxaOperacao.Value;
                }
                else
                {
                    d.J = taxa.Value;
                }
                #endregion

                #region BenchMark
                if (this.tipoIndice == (int)TipoIndice.Decimal) {

                    if (d.J.HasValue && this.retornoNominal.HasValue) {
                        decimal valorNominalIndice = this.retornoNominal.Value / 100;
                        //
                        d.K = d.J.Value / 100 - valorNominalIndice;
                    }
                }
                else if (this.tipoIndice == (int)TipoIndice.Percentual) {
                    if (d.J.HasValue && this.retornoNominal.HasValue) {
                        decimal valorNominalIndice = this.retornoNominal.Value / 100;
                        //
                        d.K = (d.J.Value / 100) / valorNominalIndice;
                    }
                }
                #endregion

                d.L = null;
                d.M = null;
                //
                d.N = "0,00%";
                //
                d.O = posicaoRendaFixa.UpToTituloRendaFixaByIdTitulo.UpToEmissorByIdEmissor.Nome.Trim();
                d.P = posicaoRendaFixa.UpToTituloRendaFixaByIdTitulo.UpToEmissorByIdEmissor.Nome.Trim();
                //
                this.dList1.Add(d);
            }
            #endregion
        }
        else 
        {
            #region Bolsa
            PosicaoBolsaCollection posicaoBolsaCollection = new PosicaoBolsaCollection();
            //
            PosicaoBolsaQuery posicaoBolsaQuery = new PosicaoBolsaQuery("P");
            AtivoBolsaQuery ativoBolsaQuery = new AtivoBolsaQuery("A");
            EstrategiaQuery estrategiaQuery = new EstrategiaQuery("E");

            posicaoBolsaQuery.Select(estrategiaQuery.IdEstrategia,
                                     estrategiaQuery.Descricao,
                                     posicaoBolsaQuery.CdAtivoBolsa,
                                     ativoBolsaQuery.Descricao.As("DescricaoAtivoBolsa"),
                                     posicaoBolsaQuery.IdAgente
                                    );
            //
            posicaoBolsaQuery.InnerJoin(ativoBolsaQuery).On(ativoBolsaQuery.CdAtivoBolsa == posicaoBolsaQuery.CdAtivoBolsa);
            posicaoBolsaQuery.InnerJoin(estrategiaQuery).On(estrategiaQuery.IdEstrategia == ativoBolsaQuery.IdEstrategia);
            //
            posicaoBolsaQuery.Where(posicaoBolsaQuery.IdCliente == this.idCliente,
                                     posicaoBolsaQuery.Quantidade != 0
                                   );

            //posicaoBolsaQuery.GroupBy(estrategiaQuery.Descricao, posicaoBolsaQuery.CdAtivoBolsa);
            //
            posicaoBolsaCollection.Load(posicaoBolsaQuery);

            foreach (PosicaoBolsa posicaoBolsa in posicaoBolsaCollection)
            {
                int idEstrategia = Convert.ToInt32(posicaoBolsa.GetColumn(EstrategiaMetadata.ColumnNames.IdEstrategia));
                string estrategiaDescricao = Convert.ToString(posicaoBolsa.GetColumn(EstrategiaMetadata.ColumnNames.Descricao));
                string cdAtivoBolsa = posicaoBolsa.CdAtivoBolsa;
                string descricaoAtivoBolsa = Convert.ToString(posicaoBolsa.GetColumn("DescricaoAtivoBolsa"));

                DataHolder1 d = new DataHolder1();
                //
                d.A = estrategiaDescricao;
                d.B = cdAtivoBolsa;
                d.C = descricaoAtivoBolsa;
                d.D = null;
                d.E = null;
                d.F = null;
                d.G = null;
                d.H = null;

                #region Stress
                TabelaStressCollection ts = new TabelaStressCollection();
                ts.Query.Select(ts.Query.Stress)
                        .Where(ts.Query.IdAtivo.ToUpper() == idEstrategia.ToString(),
                               ts.Query.TipoAtivo == 1, // TipoAtivo = Estrategia
                               ts.Query.DataReferencia <= this.data)
                        .OrderBy(ts.Query.DataReferencia.Descending);

                //
                decimal? stress = null;
                if (ts.Query.Load()) {
                    if (ts[0].Stress.HasValue) {
                        stress = ts[0].Stress.Value;
                    }
                }
                if (stress.HasValue) {
                    d.I = stress.Value;
                }
                #endregion

                #region RetornoNominal
                TabelaRetornoEsperadoCollection tr = new TabelaRetornoEsperadoCollection();
                tr.Query.Select(tr.Query.Valor)
                        .Where(tr.Query.IdAtivo.ToUpper() == cdAtivoBolsa.Trim(),
                               tr.Query.TipoAtivo == 2, // TipoAtivo = Ação
                               tr.Query.DataReferencia <= this.data)
                        .OrderBy(tr.Query.DataReferencia.Descending);

                //
                decimal? retornoNominal = null;
                if (tr.Query.Load()) {
                    if (tr[0].Valor.HasValue) {
                        retornoNominal = tr[0].Valor.Value;
                    }
                }
                
                if (retornoNominal.HasValue) {

                    decimal? puMedio = null;
                    CotacaoBolsa c = new CotacaoBolsa();
                    if (c.LoadByPrimaryKey(this.data, cdAtivoBolsa.Trim())) {
                        puMedio = c.PUMedio.Value;
                    }
                    if (puMedio.HasValue && puMedio != 0) {
                        d.J = retornoNominal.Value / puMedio.Value;
                    }
                }

                #endregion

                #region BenchMark
                if (this.tipoIndice == (int)TipoIndice.Decimal) {

                    if (d.J.HasValue && this.retornoNominal.HasValue) {
                        decimal valorNominalIndice = this.retornoNominal.Value / 100;
                        //
                        d.K = d.J.Value / 100 - valorNominalIndice;
                    }
                }
                else if (this.tipoIndice == (int)TipoIndice.Percentual) {
                    if (d.J.HasValue && this.retornoNominal.HasValue) {
                        decimal valorNominalIndice = this.retornoNominal.Value / 100;
                        //
                        d.K = (d.J.Value / 100) / valorNominalIndice;
                    }
                }
                #endregion

                d.L = null;
                d.M = null;
                //
                d.N = "0,00%";
                //
                d.O = posicaoBolsa.UpToAgenteMercadoByIdAgente.Nome.Trim();
                d.P = posicaoBolsa.UpToAgenteMercadoByIdAgente.Nome.Trim();
                //
                this.dList1.Add(d);
            }
            #endregion

            #region Fundo
            PosicaoFundoCollection posicaoFundoCollection = new PosicaoFundoCollection();
            PosicaoFundoQuery posicaoFundoQuery = new PosicaoFundoQuery("P");
            CarteiraQuery carteiraQuery = new CarteiraQuery("C");
            estrategiaQuery = new EstrategiaQuery("E");

            posicaoFundoQuery.Select(estrategiaQuery.Descricao,
                                     posicaoFundoQuery.IdCarteira,
                                     carteiraQuery.Apelido,
                                     posicaoFundoQuery.DataAplicacao,
                                     carteiraQuery.DiasCotizacaoResgate,
                                     carteiraQuery.DiasLiquidacaoResgate,
                                     carteiraQuery.IdAgenteAdministrador,
                                     carteiraQuery.IdAgenteCustodiante
                                    );
            posicaoFundoQuery.InnerJoin(carteiraQuery).On(carteiraQuery.IdCarteira == posicaoFundoQuery.IdCarteira);
            posicaoFundoQuery.InnerJoin(estrategiaQuery).On(estrategiaQuery.IdEstrategia == carteiraQuery.IdEstrategia);
            //
            posicaoFundoQuery.Where( posicaoFundoQuery.IdCliente == this.idCliente,
                                     posicaoFundoQuery.Quantidade != 0
                                   );
            //
            posicaoFundoCollection.Load(posicaoFundoQuery);

            foreach (PosicaoFundo posicaoFundo in posicaoFundoCollection) {
                string estrategiaDescricao = Convert.ToString(posicaoFundo.GetColumn(EstrategiaMetadata.ColumnNames.Descricao));
                string apelido = Convert.ToString(posicaoFundo.GetColumn(CarteiraMetadata.ColumnNames.Apelido));

                DataHolder1 d = new DataHolder1();
                //
                d.A = estrategiaDescricao;
                d.B = posicaoFundo.IdCarteira.Value.ToString();
                d.C = apelido;
                d.D = posicaoFundo.DataAplicacao.Value;
                d.E = null;
                d.F = Calendario.NumeroDias(posicaoFundo.DataAplicacao.Value, this.data);
                d.G = Convert.ToInt32(posicaoFundo.GetColumn(CarteiraMetadata.ColumnNames.DiasCotizacaoResgate));
                d.H = Convert.ToInt32(posicaoFundo.GetColumn(CarteiraMetadata.ColumnNames.DiasLiquidacaoResgate));

                #region Stress
                string idCarteira = posicaoFundo.IdCarteira.Value.ToString();
                TabelaStressCollection ts = new TabelaStressCollection();
                ts.Query.Select(ts.Query.Stress)
                        .Where(ts.Query.IdAtivo == idCarteira,
                               ts.Query.TipoAtivo == 2, // TipoAtivo = Fundo
                               ts.Query.DataReferencia <= this.data)
                        .OrderBy(ts.Query.DataReferencia.Descending);

                //
                decimal? stress = null;
                if (ts.Query.Load()) {
                    if (ts[0].Stress.HasValue) {
                        stress = ts[0].Stress.Value;
                    }
                }
                if (stress.HasValue) {
                    d.I = stress.Value;
                }
                #endregion

                #region RetornoNominal
                TabelaRetornoEsperadoCollection tr = new TabelaRetornoEsperadoCollection();
                tr.Query.Select(tr.Query.Valor)
                        .Where(tr.Query.IdAtivo == idCarteira,
                               tr.Query.TipoAtivo == 1, // TipoAtivo = Fundo
                               tr.Query.DataReferencia <= this.data)
                        .OrderBy(tr.Query.DataReferencia.Descending);

                //
                decimal? retornoNominal = null;
                if (tr.Query.Load()) {
                    if (tr[0].Valor.HasValue) {
                        retornoNominal = tr[0].Valor.Value;
                    }
                }
                if (retornoNominal.HasValue) {
                    d.J = retornoNominal.Value;
                }
                #endregion

                #region BenchMark
                if (this.tipoIndice == (int)TipoIndice.Decimal) {

                    if (d.J.HasValue && this.retornoNominal.HasValue) {
                        decimal valorNominalIndice = this.retornoNominal.Value / 100;
                        //
                        d.K = d.J.Value / 100 - valorNominalIndice;
                    }
                }
                else if (this.tipoIndice == (int)TipoIndice.Percentual) {
                    if (d.J.HasValue && this.retornoNominal.HasValue) {
                        decimal valorNominalIndice = this.retornoNominal.Value / 100;
                        //
                        d.K = (d.J.Value / 100) / valorNominalIndice;
                    }
                }
                #endregion

                #region TaxaAdministracao
                TabelaTaxaAdministracaoCollection t = new TabelaTaxaAdministracaoCollection();
                t.Query.Where(t.Query.IdCarteira == posicaoFundo.IdCarteira.Value,
                              t.Query.DataReferencia <= this.data,
                              t.Query.Taxa.IsNotNull());
                //
                decimal? taxa = 0.00M;
                if (t.Query.Load()) {
                    if (t[0].Taxa.HasValue) {
                        taxa = t[0].Taxa.Value;
                    }
                }

                d.L = taxa;
                d.M = taxa;
                #endregion

                #region TabelaTaxaPerformance
                TabelaTaxaPerformanceCollection tp = new TabelaTaxaPerformanceCollection();
                tp.Query.Where(tp.Query.IdCarteira == posicaoFundo.IdCarteira.Value,
                               tp.Query.TaxaPerformance.IsNotNull(),
                               tp.Query.DataReferencia <= this.data
                              );
                //
                decimal? taxaPerformance = 0.00M;
                if (tp.Query.Load()) {
                    if (tp[0].TaxaPerformance.HasValue) {
                        taxaPerformance = tp[0].TaxaPerformance.Value;
                    }
                }

                d.N = taxaPerformance.Value.ToString("N2") + "%";
                #endregion

                //
                d.O = posicaoFundo.UpToCarteiraByIdCarteira.UpToAgenteMercadoByIdAgenteAdministrador.Nome.Trim();
                d.P = posicaoFundo.UpToCarteiraByIdCarteira.UpToAgenteMercadoByIdAgenteGestor.Nome.Trim();
                //
                this.dList1.Add(d);
            }
            #endregion

            #region RendaFixa
            PosicaoRendaFixaCollection posicaoRendaFixaCollection = new PosicaoRendaFixaCollection();
            PosicaoRendaFixaQuery posicaoRendaFixaQuery = new PosicaoRendaFixaQuery("P");
            TituloRendaFixaQuery tituloRendaFixaQuery = new TituloRendaFixaQuery("T");
            estrategiaQuery = new EstrategiaQuery("E");

            posicaoRendaFixaQuery.Select(estrategiaQuery.IdEstrategia,
                                         estrategiaQuery.Descricao,
                                         posicaoRendaFixaQuery.IdTitulo,
                                         tituloRendaFixaQuery.Descricao.As("DescricaoTituloRendaFixa"),
                                         posicaoRendaFixaQuery.DataOperacao,
                                         posicaoRendaFixaQuery.DataVencimento,
                                         tituloRendaFixaQuery.IdIndice,
                                         tituloRendaFixaQuery.Percentual,
                                         tituloRendaFixaQuery.Taxa
                                         );

            posicaoRendaFixaQuery.InnerJoin(tituloRendaFixaQuery).On(tituloRendaFixaQuery.IdTitulo == posicaoRendaFixaQuery.IdTitulo);
            posicaoRendaFixaQuery.InnerJoin(estrategiaQuery).On(estrategiaQuery.IdEstrategia == tituloRendaFixaQuery.IdEstrategia);
            //
            posicaoRendaFixaQuery.Where(posicaoRendaFixaQuery.IdCliente == this.idCliente,
                                         posicaoRendaFixaQuery.Quantidade != 0
                                       );
            //
            posicaoRendaFixaCollection.Load(posicaoRendaFixaQuery);

            foreach (PosicaoRendaFixa posicaoRendaFixa in posicaoRendaFixaCollection) 
            {
                int idEstrategia = Convert.ToInt32(posicaoRendaFixa.GetColumn(EstrategiaMetadata.ColumnNames.IdEstrategia));
                string estrategiaDescricao = Convert.ToString(posicaoRendaFixa.GetColumn(EstrategiaMetadata.ColumnNames.Descricao));
                string descricaoTitulo = Convert.ToString(posicaoRendaFixa.GetColumn("DescricaoTituloRendaFixa"));
                int idTitulo = posicaoRendaFixa.IdTitulo.Value;

                TituloRendaFixa tituloRendaFixa = new TituloRendaFixa();
                tituloRendaFixa.LoadByPrimaryKey(idTitulo);

                short? idIndice = tituloRendaFixa.IdIndice;
                decimal? percentual = tituloRendaFixa.Percentual;
                decimal? taxa = tituloRendaFixa.Taxa;

                DataHolder1 d = new DataHolder1();
                //
                d.A = estrategiaDescricao;
                d.B = posicaoRendaFixa.IdTitulo.Value.ToString();
                d.C = descricaoTitulo;
                d.D = posicaoRendaFixa.DataOperacao.Value;
                d.E = posicaoRendaFixa.DataVencimento.Value;
                d.F = Calendario.NumeroDias(posicaoRendaFixa.DataOperacao.Value, this.data);
                d.G = null;
                d.H = null;

                #region Stress
                TabelaStressCollection ts = new TabelaStressCollection();
                ts.Query.Select(ts.Query.Stress)
                        .Where(ts.Query.IdAtivo == idEstrategia.ToString(),
                               ts.Query.TipoAtivo == 1, // TipoAtivo = Estrategia
                               ts.Query.DataReferencia <= this.data)
                        .OrderBy(ts.Query.DataReferencia.Descending);

                //
                decimal? stress = null;
                if (ts.Query.Load()) {
                    if (ts[0].Stress.HasValue) {
                        stress = ts[0].Stress.Value;
                    }
                }
                if (stress.HasValue) {
                    d.I = stress.Value;
                }
                #endregion

                #region RetornoNominal
                TimeSpan numeroDias = this.data - posicaoRendaFixa.DataOperacao.Value;
                TabelaEscalonamentoRFCollection tabelaEscalonamentoRFCollection = new TabelaEscalonamentoRFCollection();
                tabelaEscalonamentoRFCollection.Query.Select(tabelaEscalonamentoRFCollection.Query.Taxa);
                tabelaEscalonamentoRFCollection.Query.Where(tabelaEscalonamentoRFCollection.Query.IdTitulo.Equal(idTitulo),
                                                            tabelaEscalonamentoRFCollection.Query.Prazo.LessThanOrEqual(numeroDias.Days));
                tabelaEscalonamentoRFCollection.Query.OrderBy(tabelaEscalonamentoRFCollection.Query.Prazo.Descending);
                tabelaEscalonamentoRFCollection.Query.Load();

                if (tabelaEscalonamentoRFCollection.Count > 0)
                {
                    percentual = tabelaEscalonamentoRFCollection[0].Taxa.Value;
                }

                if (idIndice.HasValue)
                {
                    if (!percentual.HasValue)
                    {
                        percentual = 100M;
                    }

                    TabelaRetornoEsperadoCollection tr = new TabelaRetornoEsperadoCollection();
                    tr.Query.Select(tr.Query.Valor);
                    tr.Query.Where(tr.Query.TipoAtivo == 3, // TipoAtivo = RF
                                   tr.Query.DataReferencia <= this.data,
                                   tr.Query.IdAtivo == idIndice.Value.ToString());
                    tr.Query.OrderBy(tr.Query.DataReferencia.Descending);

                    //
                    decimal? retornoNominal = null;
                    if (tr.Query.Load())
                    {
                        if (tr[0].Valor.HasValue)
                        {
                            retornoNominal = tr[0].Valor.Value;
                        }
                    }
                    if (retornoNominal.HasValue)
                    {
                        decimal fatorIndice = (decimal)(retornoNominal.Value / 100M) * (percentual.Value / 100M) + 1M;
                        decimal fatorTaxa = taxa.HasValue ? taxa.Value / 100M + 1M : 1M;

                        decimal retorno = ((fatorIndice * fatorTaxa) - 1M) * 100M;

                        d.J = retorno;
                    }
                }
                else if (posicaoRendaFixa.TaxaOperacao.HasValue)
                {
                    d.J = posicaoRendaFixa.TaxaOperacao.Value;
                }
                else
                {
                    d.J = taxa.HasValue? taxa.Value: 0;
                }
                #endregion

                #region BenchMark
                if (this.tipoIndice == (int)TipoIndice.Decimal) {

                    if (d.J.HasValue && this.retornoNominal.HasValue) {
                        decimal valorNominalIndice = this.retornoNominal.Value / 100;
                        //
                        d.K = d.J.Value / 100 - valorNominalIndice;
                    }
                }
                else if (this.tipoIndice == (int)TipoIndice.Percentual) {
                    if (d.J.HasValue && this.retornoNominal.HasValue) {
                        decimal valorNominalIndice = this.retornoNominal.Value / 100;
                        //
                        d.K = (d.J.Value / 100) / valorNominalIndice;
                    }
                }
                #endregion

                d.L = null;
                d.M = null;
                //
                d.N = "0,00%";
                //
                d.O = posicaoRendaFixa.UpToTituloRendaFixaByIdTitulo.UpToEmissorByIdEmissor.Nome.Trim();
                d.P = posicaoRendaFixa.UpToTituloRendaFixaByIdTitulo.UpToEmissorByIdEmissor.Nome.Trim();
                //
                this.dList1.Add(d);
            }
            #endregion
        }

        // Ordena por Descricao - Campo A
        this.dList1.Sort(delegate(DataHolder1 d1, DataHolder1 d2) {
                                                                    if (d1.A == d2.A) {
                                                                        return d1.B.CompareTo(d2.B);
                                                                    }
                                                                    else {
                                                                        return d1.A.CompareTo(d2.A);
                                                                    }
                                                                  }
                                                                  );

        /* --------------------------------------------------------------------------- */
        // Dados Ficticios
        //this.dList1.Add(new DataHolder1("CDI", "020.028", "PRIVATE EXCLUSIVE REFERENCIADO DI FICFI", DateTime.Now,
        //                           DateTime.Now, 7, 8, 9, 10.00M,
        //                           11.00M, 12.00M, 13.00M, 14.00M, "15", "16", "17"));

        //this.dList1.Add(new DataHolder1("1", "2", "3", "4", DateTime.Now,
        //                           DateTime.Now, 7, 8, 9, 10,
        //                           11, 12, 13, 14, "15", "16", "17"));

        //this.dList1.Add(new DataHolder1("CDI", "CDI Longo Prazo", "020.028", "PRIVATE EXCLUSIVE REFERENCIADO DI FICFI", new DateTime(2000, 1, 1), DateTime.Now,
        //                   80, 0, 0,
        //                   0.67M, 9.70M, 101.05M, 0.30M, 0.30M,
        //                   "não há", "Itaú", "Itaú"));
        /* --------------------------------------------------------------------------- */

    }

    /// <summary>
    /// Presupoe que dList2 variavel private esteja antecipadamente preenchida com os dados
    /// A montagem dos dados é feito na função MontaConsultaDataHolder2
    /// </summary>
    /// <param name="worksheet"></param>
    private void SetDataHolder2(Worksheet worksheet) {
        
        // Total da Coluna D
        decimal sumD = 0.00M;
        for (int j = 0; j < this.dList2.Count; j++) {
            sumD += this.dList2[j].D.Value;
        }
        
        int linha = 22;
        for (int i = 0; i < this.dList2.Count; i++) {
            // Add new row
            worksheet.Rows.Insert(linha, 1);
            //
            worksheet.Rows[linha][0].Value = this.dList2[i].A;
            worksheet.Rows[linha][1].Value = this.dList2[i].B;
            worksheet.Rows[linha][2].Value = this.dList2[i].C;

            worksheet.Rows[linha][3].NumberFormatString = "#,##,#0.00";
            worksheet.Rows[linha][3].ValueAsDouble = Convert.ToDouble(this.dList2[i].D);

            // Calculated Column - Porcentagem da coluna D            
            worksheet.Rows[linha][4].NumberFormatString = "0.00%";            
            worksheet.Rows[linha][4].Value = Convert.ToDouble(this.dList2[i].D / sumD);
            //
            worksheet.Rows[linha][5].NumberFormatString = "#,##,#0.00";
            worksheet.Rows[linha][5].ValueAsDouble = Convert.ToDouble(this.dList2[i].F);
            //
            worksheet.Rows[linha][6].NumberFormatString = "#,##,#0.00";
            worksheet.Rows[linha][6].ValueAsDouble = Convert.ToDouble(this.dList2[i].G);
            //
            worksheet.Rows[linha][7].NumberFormatString = "#,##,#0.00";
            worksheet.Rows[linha][7].ValueAsDouble = Convert.ToDouble(this.dList2[i].H);
            //

            // Calculated Column            
            worksheet.Rows[linha][8].NumberFormatString = "#,##,#0.00";
            worksheet.Rows[linha][8].ValueAsDouble = Convert.ToDouble(this.dList2[i].D - this.dList2[i].H);
            //
            worksheet.Rows[linha][9].Value = this.dList2[i].J;
            //
            
            if (this.dList2[i].K.HasValue) {
                worksheet.Rows[linha][10].NumberFormatString = "0.00%";
                worksheet.Rows[linha][10].Value = Convert.ToDouble(this.dList2[i].K / 100);
            }
            //
            
            worksheet.Rows[linha][11].Value = this.dList2[i].L;

            if (this.dList2[i].M.HasValue) {
                worksheet.Rows[linha][12].NumberFormatString = "#,##,#0.00";
                worksheet.Rows[linha][12].ValueAsDouble = Convert.ToDouble(this.dList2[i].M);
            }
            //

            if (this.dList2[i].N.HasValue) {
                worksheet.Rows[linha][13].NumberFormatString = "0.00%";
                worksheet.Rows[linha][13].Value = Convert.ToDouble(this.dList2[i].N / 100);
            }
            //
            if (this.dList2[i].O.HasValue) {
                worksheet.Rows[linha][14].NumberFormatString = "#,##,#0.00";
                worksheet.Rows[linha][14].ValueAsDouble = Convert.ToDouble(this.dList2[i].O);
            }

            worksheet.Rows[linha][3].AlignmentHorizontal = Bytescout.Spreadsheet.Constants.AlignmentHorizontal.Right;
            worksheet.Rows[linha][4].AlignmentHorizontal = Bytescout.Spreadsheet.Constants.AlignmentHorizontal.Right;
            worksheet.Rows[linha][5].AlignmentHorizontal = Bytescout.Spreadsheet.Constants.AlignmentHorizontal.Right;
            worksheet.Rows[linha][6].AlignmentHorizontal = Bytescout.Spreadsheet.Constants.AlignmentHorizontal.Right;
            worksheet.Rows[linha][7].AlignmentHorizontal = Bytescout.Spreadsheet.Constants.AlignmentHorizontal.Right;            
            worksheet.Rows[linha][8].AlignmentHorizontal = Bytescout.Spreadsheet.Constants.AlignmentHorizontal.Right;
            //
            worksheet.Rows[linha][9].AlignmentHorizontal = Bytescout.Spreadsheet.Constants.AlignmentHorizontal.Centered;
            worksheet.Rows[linha][10].AlignmentHorizontal = Bytescout.Spreadsheet.Constants.AlignmentHorizontal.Centered;
            worksheet.Rows[linha][11].AlignmentHorizontal = Bytescout.Spreadsheet.Constants.AlignmentHorizontal.Centered;
            //
            worksheet.Rows[linha][12].AlignmentHorizontal = Bytescout.Spreadsheet.Constants.AlignmentHorizontal.Right;
            worksheet.Rows[linha][13].AlignmentHorizontal = Bytescout.Spreadsheet.Constants.AlignmentHorizontal.Right;
            worksheet.Rows[linha][14].AlignmentHorizontal = Bytescout.Spreadsheet.Constants.AlignmentHorizontal.Right;
            //
            worksheet.Rows[linha].Font = new Font("Calibri", 11, FontStyle.Regular);
            //
            linha++;
        }

        /* Total */
        int inicio = 23 + this.dList1.Count;
        int final = 22 + this.dList1.Count + this.dList2.Count;
        //        

        if (this.dList2.Count > 0) { // Se tem Dados coloca Total
            worksheet.Rows[linha][3].Value = String.Format("=SUM({0}{1}:{0}{2})", "D", inicio, final);
            //
            worksheet.Rows[linha][4].NumberFormatString = "0.00%";
            worksheet.Rows[linha][4].Value = String.Format("=SUM({0}{1}:{0}{2})", "E", inicio, final);
            //
            worksheet.Rows[linha][7].NumberFormatString = "#,##,#0.00";
            worksheet.Rows[linha][7].Value = String.Format("=SUM({0}{1}:{0}{2})", "H", inicio, final);
            //
            worksheet.Rows[linha][8].NumberFormatString = "#,##,#0.00";
            worksheet.Rows[linha][8].Value = String.Format("=SUM({0}{1}:{0}{2})", "I", inicio, final);
            //
        }
        //worksheet.Rows[linha][10].NumberFormatString = "0.00%";
        //worksheet.Rows[linha][10].Value = String.Format("=SUMPRODUCT({0}{1}:{0}{2};{3}{1}:{3}{2})", "E", inicio, final, "K");
    }

    /// <summary>
    /// Montagem dos Dados do DataHolder2
    /// </summary>
    /// modifies: this.dList2 private
    private void MontaConsultaDataHolder2() {

        bool historico = this.data < this.dataDiaCliente;

        if (historico) {
            #region Bolsa
            PosicaoBolsaHistoricoCollection posicaoBolsaCollection = new PosicaoBolsaHistoricoCollection();
            //
            PosicaoBolsaHistoricoQuery posicaoBolsaQuery = new PosicaoBolsaHistoricoQuery("P");
            AtivoBolsaQuery ativoBolsaQuery = new AtivoBolsaQuery("A");
            EstrategiaQuery estrategiaQuery = new EstrategiaQuery("E");

            posicaoBolsaQuery.Select(estrategiaQuery.Descricao,
                                     posicaoBolsaQuery.CdAtivoBolsa,
                                     ativoBolsaQuery.Descricao.As("DescricaoAtivoBolsa"),
                                     posicaoBolsaQuery.ValorMercado,
                                     posicaoBolsaQuery.Quantidade,
                                     posicaoBolsaQuery.ValorCustoLiquido,
                                     posicaoBolsaQuery.IdAgente
                                    );
            //
            posicaoBolsaQuery.InnerJoin(ativoBolsaQuery).On(ativoBolsaQuery.CdAtivoBolsa == posicaoBolsaQuery.CdAtivoBolsa);
            posicaoBolsaQuery.InnerJoin(estrategiaQuery).On(estrategiaQuery.IdEstrategia == ativoBolsaQuery.IdEstrategia);
            //
            posicaoBolsaQuery.Where(posicaoBolsaQuery.IdCliente == this.idCliente,
                                    posicaoBolsaQuery.DataHistorico == this.data,
                                    posicaoBolsaQuery.Quantidade != 0
                                    );            
            //
            posicaoBolsaCollection.Load(posicaoBolsaQuery);

            foreach (PosicaoBolsaHistorico posicaoBolsa in posicaoBolsaCollection) {
                string estrategiaDescricao = Convert.ToString(posicaoBolsa.GetColumn(EstrategiaMetadata.ColumnNames.Descricao));
                string cdAtivoBolsa = posicaoBolsa.CdAtivoBolsa;
                string descricaoAtivoBolsa = Convert.ToString(posicaoBolsa.GetColumn("DescricaoAtivoBolsa"));

                DataHolder2 d = new DataHolder2();
                //
                d.A = estrategiaDescricao;
                d.B = cdAtivoBolsa;
                d.C = descricaoAtivoBolsa;
                d.D = posicaoBolsa.ValorMercado.Value;
                //d.E = null; // Computada
                d.F = posicaoBolsa.Quantidade.Value;
                d.G = posicaoBolsa.ValorCustoLiquido.Value;
                d.H = 0;
                //d.I = null; // Computada
                d.J = "Não se Aplica";
                d.K = null;
                //
                d.L = "";
                d.M = null;
                d.N = null;
                d.O = null;
                //
                d.colGestorAuxiliar = posicaoBolsa.UpToAgenteMercadoByIdAgente.Nome.Trim();

                this.dList2.Add(d);
            }
            #endregion

            #region Fundo
            PosicaoFundoHistoricoCollection posicaoFundoCollection = new PosicaoFundoHistoricoCollection();
            PosicaoFundoHistoricoQuery posicaoFundoQuery = new PosicaoFundoHistoricoQuery("P");
            CarteiraQuery carteiraQuery = new CarteiraQuery("C");
            estrategiaQuery = new EstrategiaQuery("E");

            posicaoFundoQuery.Select(estrategiaQuery.Descricao,
                                     posicaoFundoQuery.IdCarteira,
                                     carteiraQuery.Apelido,
                                     posicaoFundoQuery.ValorBruto,
                                     posicaoFundoQuery.Quantidade,
                                     (posicaoFundoQuery.Quantidade * posicaoFundoQuery.CotaAplicacao).As("Custo"),
                                     (posicaoFundoQuery.ValorIR + posicaoFundoQuery.ValorIOF).As("ValorIR_IOF"),
                                     carteiraQuery.TipoTributacao,
                                     posicaoFundoQuery.DataAplicacao
                                    );
            posicaoFundoQuery.InnerJoin(carteiraQuery).On(carteiraQuery.IdCarteira == posicaoFundoQuery.IdCarteira);
            posicaoFundoQuery.InnerJoin(estrategiaQuery).On(estrategiaQuery.IdEstrategia == carteiraQuery.IdEstrategia);
            //
            posicaoFundoQuery.Where(posicaoFundoQuery.IdCliente == this.idCliente,
                                    posicaoFundoQuery.DataHistorico == this.data,
                                    posicaoFundoQuery.Quantidade != 0
                                   );
            //
            posicaoFundoCollection.Load(posicaoFundoQuery);

            foreach (PosicaoFundoHistorico posicaoFundo in posicaoFundoCollection) {
                string estrategiaDescricao = Convert.ToString(posicaoFundo.GetColumn(EstrategiaMetadata.ColumnNames.Descricao));
                string apelido = Convert.ToString(posicaoFundo.GetColumn(CarteiraMetadata.ColumnNames.Apelido));

                DataHolder2 d = new DataHolder2();
                //
                d.A = estrategiaDescricao;
                d.B = posicaoFundo.IdCarteira.Value.ToString();
                d.C = apelido;
                d.D = posicaoFundo.ValorBruto.Value;
                //d.E = null; // Computada
                d.F = posicaoFundo.Quantidade.Value;
                d.G = Convert.ToDecimal(posicaoFundo.GetColumn("Custo"));
                d.H = Convert.ToDecimal(posicaoFundo.GetColumn("ValorIR_IOF"));
                //d.I = null; // Computada
                
                int tipoTributacao = Convert.ToInt32(posicaoFundo.GetColumn(CarteiraMetadata.ColumnNames.TipoTributacao));
                d.J = StringEnum.GetStringValue((TipoTributacaoFundo)tipoTributacao);

                decimal? aliquota = null;
                DateTime? dataProximaMudanca = null;
                switch (tipoTributacao) {
                    case (int)TipoTributacaoFundo.Isento:
                        aliquota = 0.00M;
                        break;
                    case (int)TipoTributacaoFundo.Acoes:
                        aliquota = 15.00M;        
                        break;
                    case (int)TipoTributacaoFundo.CurtoPrazo:
                        aliquota = new CalculoTributo().RetornaAliquotaIRCurtoPrazo(posicaoFundo.DataAplicacao.Value, this.data);
                        //
                        // Campo L
                        DateTime dataBase = new DateTime(2004, 12, 30);
                        DateTime dataInicio = posicaoFundo.DataAplicacao.Value;
                        if (dataInicio <= dataBase) {
                            dataInicio = new DateTime(2004, 07, 01);
                        }

                        int diasCorridos = Calendario.NumeroDias(dataInicio, this.data);

                        if (diasCorridos <= 180) {
                            //int numDiasParaProximaMudanca = 180 - diasCorridos + 1;
                            dataProximaMudanca = posicaoFundo.DataAplicacao.Value.AddDays(181);
                        }

                        break;
                    case (int)TipoTributacaoFundo.LongoPrazo:
                        aliquota = new CalculoTributo().RetornaAliquotaIRLongoPrazo(posicaoFundo.DataAplicacao.Value, this.data);

                        // Campo L
                        DateTime dataBase1 = new DateTime(2004, 12, 22);
                        DateTime dataInicio1 = posicaoFundo.DataAplicacao.Value;

                        if (dataInicio1 <= dataBase1) {
                            dataInicio1 = new DateTime(2004, 07, 01);
                        }

                        int diasCorridos1 = Calendario.NumeroDias(dataInicio1, this.data);

                        if (diasCorridos1 <= 180) {
                            //int numDiasParaProximaMudanca = 180 - diasCorridos1 + 1;
                            dataProximaMudanca = posicaoFundo.DataAplicacao.Value.AddDays(181);
                        }
                        else if (diasCorridos1 <= 360) {
                            //int numDiasParaProximaMudanca = 360 - diasCorridos1 + 1;
                            dataProximaMudanca = posicaoFundo.DataAplicacao.Value.AddDays(361);
                        }
                        else if (diasCorridos1 <= 720) {
                            //int numDiasParaProximaMudanca = 720 - diasCorridos1 + 1;
                            dataProximaMudanca = posicaoFundo.DataAplicacao.Value.AddDays(721);
                        }
                        break;
                }
                //
                d.K = aliquota;
                d.L = dataProximaMudanca.HasValue ? dataProximaMudanca.Value.ToString("d") : "Nâo há mudança";
                d.M = null;
                d.N = null;
                d.O = null;
                //
                d.colGestorAuxiliar = posicaoFundo.UpToCarteiraByIdCarteira.UpToAgenteMercadoByIdAgenteGestor.Nome.Trim();
                //
                this.dList2.Add(d);
            }
            #endregion

            #region RendaFixa
            PosicaoRendaFixaHistoricoCollection posicaoRendaFixaCollection = new PosicaoRendaFixaHistoricoCollection();
            PosicaoRendaFixaHistoricoQuery posicaoRendaFixaQuery = new PosicaoRendaFixaHistoricoQuery("P");
            TituloRendaFixaQuery tituloRendaFixaQuery = new TituloRendaFixaQuery("T");
            estrategiaQuery = new EstrategiaQuery("E");

            posicaoRendaFixaQuery.Select(estrategiaQuery.Descricao,
                                         posicaoRendaFixaQuery.IdTitulo,
                                         tituloRendaFixaQuery.Descricao.As("DescricaoTituloRendaFixa"),
                                         posicaoRendaFixaQuery.ValorMercado,
                                         posicaoRendaFixaQuery.Quantidade,
                                         (posicaoRendaFixaQuery.Quantidade * posicaoRendaFixaQuery.PUOperacao).As("Custo"),
                                         (posicaoRendaFixaQuery.ValorIR + posicaoRendaFixaQuery.ValorIOF).As("ValorIR_IOF"),
                                         tituloRendaFixaQuery.IsentoIR,
                                         posicaoRendaFixaQuery.DataOperacao
                                         );

            posicaoRendaFixaQuery.InnerJoin(tituloRendaFixaQuery).On(tituloRendaFixaQuery.IdTitulo == posicaoRendaFixaQuery.IdTitulo);
            posicaoRendaFixaQuery.InnerJoin(estrategiaQuery).On(estrategiaQuery.IdEstrategia == tituloRendaFixaQuery.IdEstrategia);
            //
            posicaoRendaFixaQuery.Where(posicaoRendaFixaQuery.IdCliente == this.idCliente,
                                        posicaoRendaFixaQuery.DataHistorico == this.data,
                                        posicaoRendaFixaQuery.Quantidade != 0
                                        );
            //
            posicaoRendaFixaCollection.Load(posicaoRendaFixaQuery);

            foreach (PosicaoRendaFixaHistorico posicaoRendaFixa in posicaoRendaFixaCollection) {
                string estrategiaDescricao = Convert.ToString(posicaoRendaFixa.GetColumn(EstrategiaMetadata.ColumnNames.Descricao));
                string descricaoTitulo = Convert.ToString(posicaoRendaFixa.GetColumn("DescricaoTituloRendaFixa"));

                DataHolder2 d = new DataHolder2();
                //
                d.A = estrategiaDescricao;
                d.B = posicaoRendaFixa.IdTitulo.Value.ToString();
                d.C = descricaoTitulo;
                d.D = posicaoRendaFixa.ValorMercado.Value;                
                //d.E = null; // Computada
                d.F = posicaoRendaFixa.Quantidade.Value;
                d.G = Convert.ToDecimal(posicaoRendaFixa.GetColumn("Custo"));
                d.H = Convert.ToDecimal(posicaoRendaFixa.GetColumn("ValorIR_IOF"));
                //d.I = null; // Computada

                int isentoIR = Convert.ToInt32(posicaoRendaFixa.GetColumn(TituloRendaFixaMetadata.ColumnNames.IsentoIR));
                d.J = isentoIR == (int)TituloIsentoIR.Isento ? "Isento" : "Longo Prazo";

                d.K = new CalculoTributo().RetornaAliquotaIRLongoPrazo(posicaoRendaFixa.DataOperacao.Value, this.data);

                #region Campo L
                DateTime? dataProximaMudanca = null;
                DateTime dataBase2 = new DateTime(2004, 12, 22);
                DateTime dataInicio2 = posicaoRendaFixa.DataOperacao.Value;

                if (dataInicio2 <= dataBase2) {
                    dataInicio2 = new DateTime(2004, 07, 01);
                }

                int diasCorridos2 = Calendario.NumeroDias(dataInicio2, this.data);

                if (diasCorridos2 <= 180) {                    
                    dataProximaMudanca = posicaoRendaFixa.DataOperacao.Value.AddDays(181);
                }
                else if (diasCorridos2 <= 360) {
                    dataProximaMudanca = posicaoRendaFixa.DataOperacao.Value.AddDays(361);
                }
                else if (diasCorridos2 <= 720) {
                    dataProximaMudanca = posicaoRendaFixa.DataOperacao.Value.AddDays(721);
                }
                #endregion

                d.L = dataProximaMudanca.HasValue ? dataProximaMudanca.Value.ToString("d") : "Nâo há mudança";
                d.M = null;
                d.N = null;
                d.O = null;
                //
                d.colGestorAuxiliar = posicaoRendaFixa.UpToTituloRendaFixaByIdTitulo.UpToEmissorByIdEmissor.Nome.Trim();
                //
                this.dList2.Add(d);
            }
            #endregion
        }
        else {
            #region Bolsa
            PosicaoBolsaCollection posicaoBolsaCollection = new PosicaoBolsaCollection();
            //
            PosicaoBolsaQuery posicaoBolsaQuery = new PosicaoBolsaQuery("P");
            AtivoBolsaQuery ativoBolsaQuery = new AtivoBolsaQuery("A");
            EstrategiaQuery estrategiaQuery = new EstrategiaQuery("E");

            posicaoBolsaQuery.Select(estrategiaQuery.Descricao,
                                     posicaoBolsaQuery.CdAtivoBolsa,
                                     ativoBolsaQuery.Descricao.As("DescricaoAtivoBolsa"),
                                     posicaoBolsaQuery.ValorMercado,
                                     posicaoBolsaQuery.Quantidade,
                                     posicaoBolsaQuery.ValorCustoLiquido,
                                     posicaoBolsaQuery.IdAgente
                                    );
            //
            posicaoBolsaQuery.InnerJoin(ativoBolsaQuery).On(ativoBolsaQuery.CdAtivoBolsa == posicaoBolsaQuery.CdAtivoBolsa);
            posicaoBolsaQuery.InnerJoin(estrategiaQuery).On(estrategiaQuery.IdEstrategia == ativoBolsaQuery.IdEstrategia);
            //
            posicaoBolsaQuery.Where(posicaoBolsaQuery.IdCliente == this.idCliente,
                                     posicaoBolsaQuery.Quantidade != 0
                                   );

            //posicaoBolsaQuery.GroupBy(estrategiaQuery.Descricao, posicaoBolsaQuery.CdAtivoBolsa);
            //
            posicaoBolsaCollection.Load(posicaoBolsaQuery);

            foreach (PosicaoBolsa posicaoBolsa in posicaoBolsaCollection) {
                string estrategiaDescricao = Convert.ToString(posicaoBolsa.GetColumn(EstrategiaMetadata.ColumnNames.Descricao));
                string cdAtivoBolsa = posicaoBolsa.CdAtivoBolsa;
                string descricaoAtivoBolsa = Convert.ToString(posicaoBolsa.GetColumn("DescricaoAtivoBolsa"));

                DataHolder2 d = new DataHolder2();
                //
                d.A = estrategiaDescricao;
                d.B = cdAtivoBolsa;
                d.C = descricaoAtivoBolsa;
                d.D = posicaoBolsa.ValorMercado.Value;
                //d.E = null; // Computada
                d.F = posicaoBolsa.Quantidade.Value;
                d.G = posicaoBolsa.ValorCustoLiquido.Value;
                d.H = 0;
                //d.I = null; // Computada
                d.J = "Não se Aplica";
                d.K = null;
                d.L = "";
                d.M = null;
                d.N = null;
                d.O = null;
                //
                d.colGestorAuxiliar = posicaoBolsa.UpToAgenteMercadoByIdAgente.Nome.Trim();
                //
                this.dList2.Add(d);
            }
            #endregion

            #region Fundo
            PosicaoFundoCollection posicaoFundoCollection = new PosicaoFundoCollection();
            PosicaoFundoQuery posicaoFundoQuery = new PosicaoFundoQuery("P");
            CarteiraQuery carteiraQuery = new CarteiraQuery("C");
            estrategiaQuery = new EstrategiaQuery("E");

            posicaoFundoQuery.Select(estrategiaQuery.Descricao,
                                     posicaoFundoQuery.IdCarteira,
                                     carteiraQuery.Apelido,
                                     posicaoFundoQuery.ValorBruto,
                                     posicaoFundoQuery.Quantidade,
                                     (posicaoFundoQuery.Quantidade * posicaoFundoQuery.CotaAplicacao).As("Custo"),
                                     (posicaoFundoQuery.ValorIR + posicaoFundoQuery.ValorIOF).As("ValorIR_IOF"),
                                     carteiraQuery.TipoTributacao,
                                     posicaoFundoQuery.DataAplicacao
                                    );
            posicaoFundoQuery.InnerJoin(carteiraQuery).On(carteiraQuery.IdCarteira == posicaoFundoQuery.IdCarteira);
            posicaoFundoQuery.InnerJoin(estrategiaQuery).On(estrategiaQuery.IdEstrategia == carteiraQuery.IdEstrategia);
            //
            posicaoFundoQuery.Where(posicaoFundoQuery.IdCliente == this.idCliente,
                                     posicaoFundoQuery.Quantidade != 0
                                   );
            //
            posicaoFundoCollection.Load(posicaoFundoQuery);

            foreach (PosicaoFundo posicaoFundo in posicaoFundoCollection) {
                string estrategiaDescricao = Convert.ToString(posicaoFundo.GetColumn(EstrategiaMetadata.ColumnNames.Descricao));
                string apelido = Convert.ToString(posicaoFundo.GetColumn(CarteiraMetadata.ColumnNames.Apelido));

                DataHolder2 d = new DataHolder2();
                //
                d.A = estrategiaDescricao;
                d.B = posicaoFundo.IdCarteira.Value.ToString();
                d.C = apelido;
                d.D = posicaoFundo.ValorBruto.Value;
                //d.E = null; // Computada
                d.F = posicaoFundo.Quantidade.Value;
                d.G = Convert.ToDecimal(posicaoFundo.GetColumn("Custo"));
                d.H = Convert.ToDecimal(posicaoFundo.GetColumn("ValorIR_IOF"));
                //d.I = null; // Computada

                int tipoTributacao = Convert.ToInt32(posicaoFundo.GetColumn(CarteiraMetadata.ColumnNames.TipoTributacao));
                d.J = StringEnum.GetStringValue((TipoTributacaoFundo)tipoTributacao);

                decimal? aliquota = null;
                DateTime? dataProximaMudanca = null;
                switch (tipoTributacao) {
                    case (int)TipoTributacaoFundo.Isento:
                        aliquota = 0.00M;
                        break;
                    case (int)TipoTributacaoFundo.Acoes:
                        aliquota = 15.00M;
                        break;
                    case (int)TipoTributacaoFundo.CurtoPrazo:
                        aliquota = new CalculoTributo().RetornaAliquotaIRCurtoPrazo(posicaoFundo.DataAplicacao.Value, this.data);
                        //
                        // Campo L
                        DateTime dataBase = new DateTime(2004, 12, 30);
                        DateTime dataInicio = posicaoFundo.DataAplicacao.Value;
                        if (dataInicio <= dataBase) {
                            dataInicio = new DateTime(2004, 07, 01);
                        }

                        int diasCorridos = Calendario.NumeroDias(dataInicio, this.data);

                        if (diasCorridos <= 180) {
                            //int numDiasParaProximaMudanca = 180 - diasCorridos + 1;
                            dataProximaMudanca = posicaoFundo.DataAplicacao.Value.AddDays(181);
                        }

                        break;
                    case (int)TipoTributacaoFundo.LongoPrazo:
                        aliquota = new CalculoTributo().RetornaAliquotaIRLongoPrazo(posicaoFundo.DataAplicacao.Value, this.data);

                        // Campo L
                        DateTime dataBase1 = new DateTime(2004, 12, 22);
                        DateTime dataInicio1 = posicaoFundo.DataAplicacao.Value;

                        if (dataInicio1 <= dataBase1) {
                            dataInicio1 = new DateTime(2004, 07, 01);
                        }

                        int diasCorridos1 = Calendario.NumeroDias(dataInicio1, this.data);

                        if (diasCorridos1 <= 180) {
                            //int numDiasParaProximaMudanca = 180 - diasCorridos1 + 1;
                            dataProximaMudanca = posicaoFundo.DataAplicacao.Value.AddDays(181);
                        }
                        else if (diasCorridos1 <= 360) {
                            //int numDiasParaProximaMudanca = 360 - diasCorridos1 + 1;
                            dataProximaMudanca = posicaoFundo.DataAplicacao.Value.AddDays(361);
                        }
                        else if (diasCorridos1 <= 720) {
                            //int numDiasParaProximaMudanca = 720 - diasCorridos1 + 1;
                            dataProximaMudanca = posicaoFundo.DataAplicacao.Value.AddDays(721);
                        }
                        break;
                }

                d.K = aliquota;
                //
                d.L = dataProximaMudanca.HasValue ? dataProximaMudanca.Value.ToString("d") : "Nâo há mudança";
                d.M = null;
                d.N = null;
                d.O = null;
                //
                d.colGestorAuxiliar = posicaoFundo.UpToCarteiraByIdCarteira.UpToAgenteMercadoByIdAgenteGestor.Nome.Trim();
                //
                this.dList2.Add(d);
            }
            #endregion

            #region RendaFixa
            PosicaoRendaFixaCollection posicaoRendaFixaCollection = new PosicaoRendaFixaCollection();
            PosicaoRendaFixaQuery posicaoRendaFixaQuery = new PosicaoRendaFixaQuery("P");
            TituloRendaFixaQuery tituloRendaFixaQuery = new TituloRendaFixaQuery("T");
            estrategiaQuery = new EstrategiaQuery("E");

            posicaoRendaFixaQuery.Select(estrategiaQuery.Descricao,
                                         posicaoRendaFixaQuery.IdTitulo,
                                         tituloRendaFixaQuery.Descricao.As("DescricaoTituloRendaFixa"),
                                         posicaoRendaFixaQuery.ValorMercado,
                                         posicaoRendaFixaQuery.Quantidade,
                                         (posicaoRendaFixaQuery.Quantidade * posicaoRendaFixaQuery.PUOperacao).As("Custo"),
                                         (posicaoRendaFixaQuery.ValorIR + posicaoRendaFixaQuery.ValorIOF).As("ValorIR_IOF"),
                                         tituloRendaFixaQuery.IsentoIR,
                                         posicaoRendaFixaQuery.DataOperacao
                                         );

            posicaoRendaFixaQuery.InnerJoin(tituloRendaFixaQuery).On(tituloRendaFixaQuery.IdTitulo == posicaoRendaFixaQuery.IdTitulo);
            posicaoRendaFixaQuery.InnerJoin(estrategiaQuery).On(estrategiaQuery.IdEstrategia == tituloRendaFixaQuery.IdEstrategia);
            //
            posicaoRendaFixaQuery.Where(posicaoRendaFixaQuery.IdCliente == this.idCliente,
                                         posicaoRendaFixaQuery.Quantidade != 0
                                       );
            //
            posicaoRendaFixaCollection.Load(posicaoRendaFixaQuery);

            foreach (PosicaoRendaFixa posicaoRendaFixa in posicaoRendaFixaCollection) {
                string estrategiaDescricao = Convert.ToString(posicaoRendaFixa.GetColumn(EstrategiaMetadata.ColumnNames.Descricao));
                string descricaoTitulo = Convert.ToString(posicaoRendaFixa.GetColumn("DescricaoTituloRendaFixa"));

                DataHolder2 d = new DataHolder2();
                //
                d.A = estrategiaDescricao;
                d.B = posicaoRendaFixa.IdTitulo.Value.ToString();
                d.C = descricaoTitulo;
                d.D = posicaoRendaFixa.ValorMercado.Value;
                //d.E = null; // Computada
                d.F = posicaoRendaFixa.Quantidade.Value;
                d.G = Convert.ToDecimal(posicaoRendaFixa.GetColumn("Custo"));
                d.H = Convert.ToDecimal(posicaoRendaFixa.GetColumn("ValorIR_IOF"));
                //d.I = null; // Computada

                int isentoIR = Convert.ToInt32(posicaoRendaFixa.GetColumn(TituloRendaFixaMetadata.ColumnNames.IsentoIR));
                d.J = isentoIR == (int)TituloIsentoIR.Isento ? "Isento" : "Longo Prazo";
                //
                d.K = new CalculoTributo().RetornaAliquotaIRLongoPrazo(posicaoRendaFixa.DataOperacao.Value, this.data);
                //

                #region Campo L
                DateTime? dataProximaMudanca = null;
                DateTime dataBase2 = new DateTime(2004, 12, 22);
                DateTime dataInicio2 = posicaoRendaFixa.DataOperacao.Value;

                if (dataInicio2 <= dataBase2) {
                    dataInicio2 = new DateTime(2004, 07, 01);
                }

                int diasCorridos2 = Calendario.NumeroDias(dataInicio2, this.data);

                if (diasCorridos2 <= 180) {
                    dataProximaMudanca = posicaoRendaFixa.DataOperacao.Value.AddDays(181);
                }
                else if (diasCorridos2 <= 360) {
                    dataProximaMudanca = posicaoRendaFixa.DataOperacao.Value.AddDays(361);
                }
                else if (diasCorridos2 <= 720) {
                    dataProximaMudanca = posicaoRendaFixa.DataOperacao.Value.AddDays(721);
                }
                #endregion

                d.L = dataProximaMudanca.HasValue ? dataProximaMudanca.Value.ToString("d") : "Nâo há mudança";
                //
                d.M = null;
                d.N = null;
                d.O = null;
                //
                d.colGestorAuxiliar = posicaoRendaFixa.UpToTituloRendaFixaByIdTitulo.UpToEmissorByIdEmissor.Nome.Trim();
                //               
                this.dList2.Add(d);
            }
            #endregion
        }

        // Ordena por Descricao - Campo A
        this.dList2.Sort(delegate(DataHolder2 d1, DataHolder2 d2) { 
                                                                  if (d1.A == d2.A) {
                                                                        return d1.B.CompareTo(d2.B);
                                                                  }
                                                                  else {
                                                                        return d1.A.CompareTo(d2.A);
                                                                  }
                                                                  } );
        /* ------------------------------------------------------------------------------------------ */
        //// Dados Ficticios    
        //this.dList2.Add(new DataHolder2("CDI", "020.028", "PRIVATE EXCLUSIVE REFERENCIADO DI FICFI",
        //                   343249.03M, 7.6M, 2332.27M, null, 2681.06M, 340567.97M,
        //                   "Longo Prazo",
        //                   15M,
        //                   "Mínima",
        //                   0.00M, null, 343249.03M));
    }

    /// <summary>
    /// Presupoe que dEstrategia/dClasse/dGestor variaveis privates estejam antecipadamente preenchida com os dados
    /// A montagem dos dados é feito na função MontaConsultaEstrategiaClasseGestor
    /// </summary>
    /// <param name="worksheet"></param>
    private void SetEstrategiaClasseGestor(Worksheet worksheet) {
        
        int numLinhasAdd = this.RetornaMaiorNumRegistros();

        int linha = 29;
        for (int i = 0; i < numLinhasAdd; i++) {
            // Add new row
            worksheet.Rows.Insert(linha, 1);
            //
            worksheet.Rows[linha].Font = new Font("Calibri", 11, FontStyle.Regular);
            //
            linha++;
        }

        /* Estrategia */
        #region Estrategia
        // Total da Coluna D
        decimal sumD = 0.00M;
        for (int j = 0; j < this.dEstrategia.Count; j++) {
            sumD += this.dEstrategia[j].D.Value;
        }

        linha = 29;
        for (int j = 0; j < this.dEstrategia.Count; j++) {
            worksheet.Rows[linha][2].Value = this.dEstrategia[j].C;
            //
            worksheet.Rows[linha][3].NumberFormatString = "0.00";
            worksheet.Rows[linha][3].ValueAsDouble =  Convert.ToDouble(this.dEstrategia[j].D);
            //
            // Calculated Column - Porcentagem da coluna D
            worksheet.Rows[linha][4].NumberFormatString = "0.00%";
            //decimal v = Utilitario.Truncate( (decimal)(this.dEstrategia[j].D / sumD), 2);
            worksheet.Rows[linha][4].Value = Convert.ToDouble(this.dEstrategia[j].D / sumD);

            if (this.dEstrategia[j].F.HasValue) {
                worksheet.Rows[linha][5].NumberFormatString = "0.00";
                worksheet.Rows[linha][5].ValueAsDouble = Convert.ToDouble(this.dEstrategia[j].F);
            }

            //
            if (this.dEstrategia[j].G.HasValue) {
                worksheet.Rows[linha][6].NumberFormatString = "0.00%";
                worksheet.Rows[linha][6].Value = Convert.ToDouble(this.dEstrategia[j].G / 100);
            }
            //
            worksheet.Rows[linha][3].AlignmentHorizontal = Bytescout.Spreadsheet.Constants.AlignmentHorizontal.Right;
            worksheet.Rows[linha][4].AlignmentHorizontal = Bytescout.Spreadsheet.Constants.AlignmentHorizontal.Right;
            worksheet.Rows[linha][5].AlignmentHorizontal = Bytescout.Spreadsheet.Constants.AlignmentHorizontal.Right;
            worksheet.Rows[linha][6].AlignmentHorizontal = Bytescout.Spreadsheet.Constants.AlignmentHorizontal.Right;
            //
            linha++;
        }
        #endregion
        //
        worksheet.Rows[linha][2].ValueAsString = "TOTAL";
        worksheet.Rows[linha][2].Font = new Font("Calibri", 11, FontStyle.Bold);        
        worksheet.Rows[linha + 1][2].ValueAsString = "Confere";
        //
        int inicio = 30 + this.dList1.Count + this.dList2.Count;
        int final = 29 + this.dList1.Count + this.dList2.Count + this.dEstrategia.Count;
        //

        if (this.dEstrategia.Count > 0) {
            string camposSum = "D" + inicio + ":D" + final;
            worksheet.Rows[linha][3].Value = "=SUM(" + camposSum + ")";

            string sumParticipacao = "E" + inicio + ":E" + final;
            worksheet.Rows[linha][4].NumberFormatString = "0.00%";
            worksheet.Rows[linha][4].Value = "=SUM(" + sumParticipacao + ")";
        }

        /* Classe */
        #region Classe
        linha = 29;
        for (int k = 0; k < this.dClasse.Count; k++) {
            worksheet.Rows[linha][8].Value = this.dClasse[k].I;
            //
            worksheet.Rows[linha][9].NumberFormatString = "0.00%";
            worksheet.Rows[linha][9].Value = Convert.ToDouble(this.dClasse[k].J);
            //
            if (this.dClasse[k].K.HasValue) {
                worksheet.Rows[linha][10].NumberFormatString = "0.00%";
                worksheet.Rows[linha][10].Value = Convert.ToDouble(this.dClasse[k].K / 100);
            }
            //
            worksheet.Rows[linha][9].AlignmentHorizontal = Bytescout.Spreadsheet.Constants.AlignmentHorizontal.Right;
            worksheet.Rows[linha][10].AlignmentHorizontal = Bytescout.Spreadsheet.Constants.AlignmentHorizontal.Right;
            //
            linha++;
        }
        worksheet.Rows[linha][8].ValueAsString = "TOTAL";
        worksheet.Rows[linha][8].Font = new Font("Calibri", 11, FontStyle.Bold);

        //
        int inicioClasse = 30 + this.dList1.Count + this.dList2.Count;
        int finalClasse = 29 + this.dList1.Count + this.dList2.Count + this.dClasse.Count;
        //

        if (this.dClasse.Count > 0) {
            string camposSumClasse = "J" + inicioClasse + ":J" + finalClasse;
            worksheet.Rows[linha][9].NumberFormatString = "0.00%";
            worksheet.Rows[linha][9].Value = "=SUM(" + camposSumClasse + ")";
        }
        #endregion

        /* Gestor */
        #region Gestor
        linha = 29;
        for (int p = 0; p < this.dGestor.Count; p++) {
            worksheet.Rows[linha][12].Value = this.dGestor[p].M;
            //
            worksheet.Rows[linha][13].NumberFormatString = "0.00%";
            worksheet.Rows[linha][13].Value = Convert.ToDouble(this.dGestor[p].N);
            //
            if (this.dGestor[p].O.HasValue) {
                worksheet.Rows[linha][14].NumberFormatString = "0.00%";
                worksheet.Rows[linha][14].Value = Convert.ToDouble(this.dGestor[p].O / 100);
            }
            //
            worksheet.Rows[linha][13].AlignmentHorizontal = Bytescout.Spreadsheet.Constants.AlignmentHorizontal.Right;
            worksheet.Rows[linha][14].AlignmentHorizontal = Bytescout.Spreadsheet.Constants.AlignmentHorizontal.Right;
            //
            linha++;
        }
        worksheet.Rows[linha][12].Value = "TOTAL";
        worksheet.Rows[linha][12].Font = new Font("Calibri", 11, FontStyle.Bold);

        //
        int inicioGestor = 30 + this.dList1.Count + this.dList2.Count;
        int finalGestor = 29 + this.dList1.Count + this.dList2.Count + this.dGestor.Count;
        //

        if (this.dGestor.Count > 0) {
            string camposSumGestor = "N" + inicioGestor + ":N" + finalGestor;
            worksheet.Rows[linha][13].NumberFormatString = "0.00%";
            worksheet.Rows[linha][13].Value = "=SUM(" + camposSumGestor + ")";
        }
        #endregion
    }

    /// <summary>
    /// Montagem dos Dados de Estrategia/Classe/Gestor
    /// </summary>
    /// modifies: this.dEstrategia/dClasse/dGestor private
    private void MontaConsultaEstrategiaClasseGestor() {
        
        //Estrategia
        //this.dEstrategia.Add(new DataHolderEstrategia("Ações Valor", 343249.03M, 7.6M, 2332.2M, 10.06M));
        //this.dEstrategia.Add(new DataHolderEstrategia("Ações1", 343249.03M, 7.6M, 2332.2M, 10.06M));
        //this.dEstrategia.Add(new DataHolderEstrategia("Ações2", 343249.03M, 7.6M, 2332.2M, 10.06M));
        //this.dEstrategia.Add(new DataHolderEstrategia("Ações3", 343249.03M, 7.6M, 2332.2M, 10.06M));
        //this.dEstrategia.Add(new DataHolderEstrategia("Ações4", 343249.03M, 7.6M, 2332.2M, 10.06M));
        //this.dEstrategia.Add(new DataHolderEstrategia("Ações5", 343249.03M, 7.6M, 2332.2M, 10.06M));
        //--------------------------------------------------------------------------------------------------
        //
        //Classe
        //this.dClasse.Add(new DataHolderClasse("CDI", 46.5M, 42.6M));
        //this.dClasse.Add(new DataHolderClasse("CDI1", 46.5M, 42.6M));
        //this.dClasse.Add(new DataHolderClasse("CDI2", 46.5M, 42.6M));
        //-------------------------------------------------------------------------------
        //Gestor
        //this.dGestor.Add(new DataHolderGestor("Brookifield",   17.03M, 13.6M));
        //this.dGestor.Add(new DataHolderGestor("Brookifield1", 17.03M, 13.6M));
        //this.dGestor.Add(new DataHolderGestor("Brookifield2", 17.03M, 13.6M));
        //this.dGestor.Add(new DataHolderGestor("Brookifield3", 17.03M, 13.6M));
        //-------------------------------------------------------------------------------

        #region DataSource Estrategia/Classe
        // Total da Coluna D
        decimal sumD = 0.00M;
        for (int w = 0; w < this.dList2.Count; w++) {
            sumD += this.dList2[w].D.Value;
        }
        // Seta coluna E
        for (int p = 0; p < this.dList2.Count; p++) {
            this.dList2[p].E = this.dList2[p].D / sumD;
        }

        //Grupamento da Lista DataHolder2
        List<string> estrategia = new List<string>();
        //
        for (int i = 0; i < this.dList2.Count; i++) {
		    if(!estrategia.Contains(this.dList2[i].A)) {
                estrategia.Add(this.dList2[i].A.Trim());
            }
        }
        //
     
        for (int j = 0; j < estrategia.Count; j++) {
            // Group do DataSource 2 pela Estrategia
            List<DataHolder2> dSource2 = this.dList2.FindAll(
                                                delegate(DataHolder2 item) {
                                                    return item.A == estrategia[j];
                                                });
            
            DataHolderEstrategia d = new DataHolderEstrategia();
            d.C = estrategia[j];
            //
            DataHolderClasse d1 = new DataHolderClasse();
            d1.I = estrategia[j];
            //
            decimal valor = 0;
            decimal valorClasse = 0;
            for (int k = 0; k < dSource2.Count; k++) {
                valor += dSource2[k].D.Value;
                valorClasse += dSource2[k].E.Value;
            }
            d.D = valor;
            d1.J = valorClasse;
            //
            this.dEstrategia.Add(d);
            //
            this.dClasse.Add(d1);
        }
        #endregion
       
        #region DataSource Gestor
        //Grupamento da Lista DataHolder2 pelo campo ColunaGestorAuxiliar
        List<string> gestor = new List<string>();
        //
        for (int i = 0; i < this.dList2.Count; i++) {
            if (!gestor.Contains(this.dList2[i].colGestorAuxiliar)) {
                gestor.Add(this.dList2[i].colGestorAuxiliar.Trim());
            }
        }
        gestor.Sort();
        //

        for (int j = 0; j < gestor.Count; j++) {
            // Group do DataSource 2 pela gestor
            List<DataHolder2> dSourceGestor2 = this.dList2.FindAll(
                                                delegate(DataHolder2 item) {
                                                    return item.colGestorAuxiliar == gestor[j];
                                                });

            DataHolderGestor dhGestor = new DataHolderGestor();
            dhGestor.M = gestor[j];
            //
            decimal valorGestor = 0;
            for (int k = 0; k < dSourceGestor2.Count; k++) {
                valorGestor += dSourceGestor2[k].E.Value;
            }
            dhGestor.N = valorGestor;
            //
            this.dGestor.Add(dhGestor);
        }
        #endregion
    }

    /// <summary>
    /// Retorna o Maior Numero de Registros entre as collections dEstrategia/dClasse/dGestor
    /// </summary>
    /// <returns></returns>
    private int RetornaMaiorNumRegistros() {
       // Cria e Inicializa um Array de Inteiros
       int[] valores = { this.dEstrategia.Count + 2,        // Linha Total e Confere
                         this.dClasse.Count + 1,            // Linha Total
                         this.dGestor.Count + 1             // Linha Total
                       };

       // Ordena o Array
       Array.Sort(valores);

       return valores[2];
    }

    /// <summary>
    /// Realiza a soma ponderada
    /// SUMPRODUCT({1,2;3,4}, {5,6;7,8})
    ///     =(1*5) + (2*6) + (3*7) + (4*8)
    /// 
    /// (fim1 - inicio1) deve ser igual ao tamanho de (fim2 - inicio2)
    /// </summary>
    /// <param name="worksheet"></param>
    /// <param name="coluna1"></param>
    /// <param name="coluna2"></param>
    /// <param name="inicio1">linha inicio do produtorio</param>
    /// <param name="fim1">linha fim do produtorio</param>
    /// <param name="inicio2">linha inicio2 do produtorio</param>
    /// <param name="fim2">linha fim2 do produtorio</param>
    /// <returns></returns>
    private decimal SumProduct(Worksheet worksheet, string coluna1, 
                                                     string coluna2, 
                                                     int inicio1, int fim1, 
                                                     int inicio2, int fim2) {
        List<decimal> array1 = new List<decimal>();
        List<decimal> array2 = new List<decimal>();

        int tam = fim1 - inicio1;

        // Armazena todos os valores da coluna 
        for (int i = 0; i <= tam; i++) {
            int num = inicio1 + i;
            string cell = coluna1 + num; 
            Object obj = worksheet.Cell(cell).Value;

            if (obj != null) {
                array1.Add(Convert.ToDecimal(obj));
            }
            else {
                array1.Add(0.00M); //Adiciona 0 se nao tiver nada na celula
            }

            /* --------------------------------------------------------------*/
            int num1 = inicio2 + i;
            string cell1 = coluna2 + num1; 
            //
            Object obj1 = worksheet.Cell(cell1).Value;

            if (obj1 != null) {
                array2.Add(Convert.ToDecimal(obj1));
            }
            else {
                array2.Add(0.00M); //Adiciona 0 se nao tiver nada na celula
            }
        }
        
        decimal valorRetorno = 0.00M;
        for (int j = 0; j < array1.Count; j++) {
            decimal v = array1[j] * array2[j];
            //
            valorRetorno += v;
        }

        return valorRetorno;
    }

    /// <summary>
    /// Seta os totais de Colunas que possuem SummProduct
    /// </summary>
    /// <param name="worksheet"></param>
    private void SetTotais(Worksheet worksheet) {
        /* Total */
        int inicio = 15;
        int final = 14 + this.dList1.Count;
        //
        int inicio1 = 23 + this.dList1.Count;
        int final1 = 22 + this.dList1.Count + this.dList2.Count;

        /* DataHolder 1
           Sum Product Colunas L e M 
        */
        // Minima
        worksheet.Rows[final][11].NumberFormatString = "0.00%";
        decimal v1 = this.SumProduct(worksheet, "L", "E", inicio, final, inicio1, final1) + Convert.ToDecimal(worksheet.Cell("B4").Value);
        worksheet.Rows[final][11].Value = Convert.ToDouble(v1);
        //
        // Maxima
        worksheet.Rows[final][12].NumberFormatString = "0.00%";
        decimal v2 = this.SumProduct(worksheet, "M", "E", inicio, final, inicio1, final1) + Convert.ToDecimal(worksheet.Cell("B4").Value);
        worksheet.Rows[final][12].Value = Convert.ToDouble(v2);
        //
        // Stress
        worksheet.Rows[final][8].NumberFormatString = "0.00%";
        decimal v4 = this.SumProduct(worksheet, "I", "E", inicio, final, inicio1, final1);
        worksheet.Rows[final][8].Value = Convert.ToDouble(v4);
        worksheet.Rows[final][8].AlignmentHorizontal = Bytescout.Spreadsheet.Constants.AlignmentHorizontal.Right;

        /* DataHolder 2
           Sum Product Coluna K
        */
        worksheet.Rows[final1][10].NumberFormatString = "0.00%";
        decimal v3 = this.SumProduct(worksheet, "K", "E", inicio1, final1, inicio1, final1);
        worksheet.Rows[final1][10].Value = Convert.ToDouble(v3);

        // alinhamento
        //worksheet.Rows[final1][3].AlignmentHorizontal = Bytescout.Spreadsheet.Constants.AlignmentHorizontal.Right;

        /* -----------------------------------------------------------------------------------*/
        // Teste
        //worksheet.Rows[0][0].NumberFormatString = "#,##,#0.00";
        //worksheet.Rows[0][0].Value = String.Format("=SUM({0}{1}:{0}{2})", "I", 30, 32);
        //worksheet.Rows[0][0].Value = String.Format("=SUMPRODUCT(I15:I17;E27:E29)");
        //worksheet.Rows[0][0].Calculate();
        //worksheet.Cell("A1").Formula = String.Format("=SUMPRODUCT($I$15:$I$17;$E$27:$E$29)");
        

    }
    #endregion

    #endregion

    #region Importação
    #endregion
}