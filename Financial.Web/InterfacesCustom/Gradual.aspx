﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Gradual.aspx.cs" Inherits="InterfacesCustom_Gradual" %>

<%@ Register Assembly="System.Web, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a" Namespace="System.Web.UI.HtmlControls" TagPrefix="cc2" %>    
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxcb" %>
<%@ Register Assembly="System.Web, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a" Namespace="System.Web.UI" TagPrefix="cc1" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxnb" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxe" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxwgv" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxpc" %>
<%@ Register Assembly="EntitySpaces.Web" Namespace="EntitySpaces.Web" TagPrefix="cc1" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxlp" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxrp" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxp" %>


<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxtc" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" tagprefix="dxw" %>    


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">

<link href="../css/forms.css" rel="stylesheet" type="text/css" />

<script type="text/javascript" language="Javascript" src="../../js/global.js"></script>
</head>

<body>
    <form id="form1" runat="server">
    
    <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="true" EnableScriptLocalization="true"/>

<asp:Panel ID="panelEdicao" runat="server" OnLoad="panelEdicao_Load">
    
<div id="divExportacao" runat="server" visible="false">
        
        <div class="divPanel divPanelNew">                
        <div id="container_small">
            <div id="header">
                <asp:Label ID="lblHeader" runat="server" Text="Exportação de Arquivos (Gradual)"/>
            </div>
            <div id="mainContent">
                <div class="reportFilter">
                    <div class="dataMessage">
                    </div>    
                    <table border="0">         
                        <tr>
                            <td>                                                      
                             <!-- Histórico de cota -->
                             <asp:UpdatePanel ID="UpdatePanel1" runat="server"> 
                                <ContentTemplate>                                                                                                            
                                    <table>
                                        <tr>
                                            <td>                
                                                <asp:Label ID="label1" runat="server" CssClass="labelNormal labelFloat" Text="Cota Diária"></asp:Label>
                                            </td>                        
                                                    
                                            <td class="td_Label">                
                                                <asp:Label ID="labelData" runat="server" CssClass="labelRequired" Text="Data:"></asp:Label>
                                            </td>                        
                                            
                                            <td>
                                                <dxe:ASPxDateEdit ID="textDataCota" runat="server" ClientInstanceName="textDataCota" EditFormat="Custom" EditFormatString="dd/MM/yyyy"/>
                                            </td>                                                                                                      

                                            <td>                            
                                                <div id="reportLinkButton" class="linkButton linkButtonNoBorder">
                                                    <asp:LinkButton ID="btnCota" runat="server" Font-Overline="false" CssClass="btnRun"
                                                        OnClick="btnCota_Click"><asp:Literal ID="Literal1" runat="server" Text="Exportar"/>
                                                        <div></div>
                                                    </asp:LinkButton>
                                                </div>                        
                                            </td>
                                        </tr>
                                        
                                        <tr>
                                            <td>                
                                                <asp:Label ID="label2" runat="server" CssClass="labelNormal labelFloat" Text="Posições Cotistas"></asp:Label>
                                            </td>                        
                                                    
                                            <td class="td_Label">                
                                                <asp:Label ID="label3" runat="server" CssClass="labelRequired" Text="Data:"></asp:Label>
                                            </td>                        
                                            
                                            <td>
                                                <dxe:ASPxDateEdit ID="textDataPosicao" runat="server" ClientInstanceName="textDataPosicao" EditFormat="Custom" EditFormatString="dd/MM/yyyy"/>
                                            </td>                                                                                                      

                                            <td>                            
                                                <div id="Div1" class="linkButton linkButtonNoBorder">
                                                    <asp:LinkButton ID="btnPosicao" runat="server" Font-Overline="false" CssClass="btnRun" OnClick="btnPosicao_Click" ><asp:Literal ID="Literal2" runat="server" Text="Exportar"/><div></div>
                                                    </asp:LinkButton>
                                                </div>                                                                                                                                                                                                                        
                                            </td>
                                        </tr>
                                    </table>                                                           
                                </ContentTemplate>
                            </asp:UpdatePanel>         
                            </td>
                            
                            <td>
                            </td>
                        </tr>         
                     </table>    
                </div>
            </div>
        </div>                        
        </div>
        
</div>

<div id="divImportacao" runat="server" visible="false">
        
        <div class="divPanel divPanelNew">                
        <div style="background-image: none !important;">
            <div style="white-space: nowrap;
	                    text-align: center;
	                    font: bold 11px Tahoma, Arial;
	                    color: #333333;
	                    background-image: url(  '../imagens/layout/nbHeaderBack.gif' );
	                    background-repeat: repeat-x;
	                    background-position: top;
	                    border-left-style: none;
	                    border-right-style: none;
	                    border-top-style: none;
	                    border-bottom: solid 1px #D5D5D5;
	                    padding: 3px 10px 3px 5px;">	                    
                <asp:Label ID="lblHeader1" runat="server" Text="Importação de Arquivos (Gradual)"/>
            </div>
            <div style="width: 720px; margin: 0 auto;">
                <div class="reportFilter">
                    
                    <table border="0">         
                        <tr>
                            <td>
                             <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                                <ContentTemplate>
                                    <table>
                                        <tr>
                                        </tr>
                                    </table>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                            </td>

                            <td>
                            </td>
                        </tr>
                     </table>
                </div>
            </div>
        </div>
        </div>

</div>

</asp:Panel>
        
    </form>
</body>
</html>