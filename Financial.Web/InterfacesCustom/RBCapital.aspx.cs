﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.IO;
using System.Globalization;
using Financial.Export;
using System.Text;
using Bytescout.Spreadsheet;
using System.Collections.Generic;
using System.Drawing;
using Financial.Investidor;
using Financial.Security;
using Financial.Web.Common;
using EntitySpaces.Interfaces;
using Financial.Security.Enums;
using Financial.Bolsa;
using Financial.Fundo;
using Financial.RendaFixa;
using Financial.Util;
using Financial.Fundo.Enums;
using Financial.RendaFixa.Enums;
using Financial.Tributo.Custom;
using Financial.Common;
using Financial.Common.Enums;
using Financial.Investidor.Enums;
using Financial.WebConfigConfiguration;
using Financial.Relatorio;
using Financial.Web.Util;
using DevExpress.Web;
using Bytescout.Spreadsheet.Constants;
using Financial.CRM;

public partial class InterfacesCustom_RBCapital : CadastroBasePage {
    // Controla Exibição de Importação ou Exportação
    private Tipo tipo;

    enum Tipo
    {
        Importacao = 0,
        Exportacao = 1
    }

    enum OwnerExportacao
    {
        RbCapital = 6
    }

    enum TipoExportacao {
        Fluxo = 0,
        Amortizacao = 1
    }

    /// <summary>
    /// Controla Exibição de Importação ou Exportação
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    new protected void panelEdicao_Load(object sender, EventArgs e)
    {
        Control c1 = this.FindControl("divExportacao");
        Control c2 = this.FindControl("divImportacao");

        if (this.tipo == Tipo.Exportacao)
        {
            if (c1 != null) { c1.Visible = true; }
            if (c2 != null) { c2.Visible = false; }
        }
        else if (this.tipo == Tipo.Importacao)
        {
            if (c1 != null) { c1.Visible = false; }
            if (c2 != null) { c2.Visible = true; }
        }
    }
    
    new protected void Page_Load(object sender, EventArgs e) {                                
        this.tipo = (Tipo)Enum.Parse(typeof(Tipo), Request.QueryString["param"].ToString());

        if (this.tipo == Tipo.Importacao) {
            this.HasFiltro = true;
            this.HasSummary = false; // Não tem Total
            //
            base.Page_Load(sender, e, this.gridCadastro);
        }
        else { // Exportação
            this.btnEditCodigo.Focus();
            this.HasPopupCliente = true;
            this.TrataTravamentoCampos();
            //
            base.Page_Load(sender, e, this.gridCadastro);
        }
    }
    
    #region DataSources

    protected void EsDSCotacaoMercadoFiduciario_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        CotacaoMercadoFiduciarioQuery cotacaoMercadoFiduciarioQuery = new CotacaoMercadoFiduciarioQuery("C");
        TituloRendaFixaQuery tituloRendaFixaQuery = new TituloRendaFixaQuery("T");

        //cotacaoMercadoFiduciarioQuery.InnerJoin(tituloRendaFixaQuery).On(
        //                                    cotacaoMercadoFiduciarioQuery.IdTitulo == tituloRendaFixaQuery.IdTitulo);
        
        if (this.textDataInicio.Text != "")
        {
            cotacaoMercadoFiduciarioQuery.Where(cotacaoMercadoFiduciarioQuery.Data >= this.textDataInicio.Text);
        }

        if (this.textDataFim.Text != "")
        {
            cotacaoMercadoFiduciarioQuery.Where(cotacaoMercadoFiduciarioQuery.Data <= this.textDataFim.Text);
        }

        cotacaoMercadoFiduciarioQuery.OrderBy(cotacaoMercadoFiduciarioQuery.IdTitulo.Ascending, cotacaoMercadoFiduciarioQuery.Data.Ascending);
        //
        CotacaoMercadoFiduciarioCollection coll = new CotacaoMercadoFiduciarioCollection();
        coll.Load(cotacaoMercadoFiduciarioQuery);

        e.Collection = coll;
    }

    protected void EsDSTitulo_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        TituloRendaFixaCollection coll = new TituloRendaFixaCollection();        
        
        coll.LoadAll();

        e.Collection = coll;
    }
    #endregion

    #region Importacao

    /// <summary>
    /// 
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void callbackErro_Callback(object source, DevExpress.Web.CallbackEventArgs e)
    {
        e.Result = "";
        ASPxComboBox dropIdTitulo = gridCadastro.FindEditFormTemplateControl("dropIdTitulo") as ASPxComboBox;
        ASPxDateEdit textData = gridCadastro.FindEditFormTemplateControl("textData") as ASPxDateEdit;
        ASPxSpinEdit textCotacao = gridCadastro.FindEditFormTemplateControl("textCotacao") as ASPxSpinEdit;

        #region Campos obrigatórios
        List<Control> controles = new List<Control>();
        controles.Add(dropIdTitulo);
        controles.Add(textData);
        controles.Add(textCotacao);

        if (base.TestaObrigatorio(controles) != "")
        {
            e.Result = "Campos com * são obrigatórios!";
            return;
        }
        #endregion

        if (gridCadastro.IsNewRowEditing)
        {
            int idTitulo = Convert.ToInt32(dropIdTitulo.SelectedItem.Value);
            DateTime data = Convert.ToDateTime(textData.Text);

            CotacaoMercadoFiduciario cotacaoMercadoFiduciario = new CotacaoMercadoFiduciario();
            if (cotacaoMercadoFiduciario.LoadByPrimaryKey(idTitulo, data))
            {
                e.Result = "Registro já existente.";
            }
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_CustomUnboundColumnData(object sender, DevExpress.Web.ASPxGridViewColumnDataEventArgs e)
    {
        if (e.Column.FieldName == "CompositeKey")
        {
            //Compose a primary key value
            int idTitulo = Convert.ToInt32(e.GetListSourceFieldValue("IdTitulo"));
            string data = Convert.ToString(e.GetListSourceFieldValue("Data"));
            e.Value = idTitulo.ToString() + "|" + data;
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void dropIdTitulo_Load(object sender, EventArgs e)
    {
        if (!gridCadastro.IsNewRowEditing)
        {
            (sender as ASPxComboBox).Enabled = false;
            (sender as ASPxComboBox).BackColor = Color.FromName("#EBEBEB");
        }
    }

    /// <summary>
    /// Bot]ao OK+ desaperece no Update
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnOKAdd_Init(object sender, EventArgs e)
    {
        if (!gridCadastro.IsNewRowEditing)
            (sender as LinkButton).Visible = false;
    }

    protected void callbackAdd_Callback(object source, DevExpress.Web.CallbackEventArgs e)
    {
        SalvarNovo();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void textData_OnInit(object sender, EventArgs e)
    {
        if (!gridCadastro.IsNewRowEditing)
            (sender as ASPxDateEdit).Enabled = false;
    }

    protected void gridCadastro_RowUpdating(object sender, DevExpress.Web.Data.ASPxDataUpdatingEventArgs e)
    {
        ASPxSpinEdit textCotacao = gridCadastro.FindEditFormTemplateControl("textCotacao") as ASPxSpinEdit;

        CotacaoMercadoFiduciario cotacaoMercadoFiduciario = new CotacaoMercadoFiduciario();

        string[] key = e.Keys[0].ToString().Split('|');
        int idTitulo = Convert.ToInt32(key[0]);
        DateTime data = Convert.ToDateTime(key[1]);

        if (cotacaoMercadoFiduciario.LoadByPrimaryKey(idTitulo, data))
        {
            cotacaoMercadoFiduciario.Cotacao = Convert.ToDecimal(textCotacao.Value);
            cotacaoMercadoFiduciario.Save();

            #region Log do Processo
            HistoricoLog historicoLog = new HistoricoLog();
            historicoLog.InsereHistoricoLog(DateTime.Now,
                                            DateTime.Now,
                                            "Cadastro de CotacaoMercadoFiduciario - Operacao: Update CotacaoMercadoFiduciario: "
                                                    + cotacaoMercadoFiduciario.IdTitulo + " - "
                                                    + cotacaoMercadoFiduciario.Data
                                                    + UtilitarioWeb.ToString(cotacaoMercadoFiduciario),
                                            HttpContext.Current.User.Identity.Name,
                                            UtilitarioWeb.GetIP(Request),
                                            "",
                                            HistoricoLogOrigem.Outros);
            #endregion
        }

        e.Cancel = true;
        gridCadastro.CancelEdit();
        gridCadastro.DataBind();
    }

    protected void gridCadastro_RowInserting(object sender, DevExpress.Web.Data.ASPxDataInsertingEventArgs e)
    {
        SalvarNovo();

        e.Cancel = true;
        gridCadastro.CancelEdit();
        gridCadastro.DataBind();
    }

    protected void SalvarNovo()
    {
        ASPxComboBox dropIdTitulo = gridCadastro.FindEditFormTemplateControl("dropIdTitulo") as ASPxComboBox;
        ASPxDateEdit textData = gridCadastro.FindEditFormTemplateControl("textData") as ASPxDateEdit;
        ASPxSpinEdit textCotacao = gridCadastro.FindEditFormTemplateControl("textCotacao") as ASPxSpinEdit;

        CotacaoMercadoFiduciario cotacaoMercadoFiduciario = new CotacaoMercadoFiduciario();

        cotacaoMercadoFiduciario.IdTitulo = Convert.ToInt32(dropIdTitulo.SelectedItem.Value);
        cotacaoMercadoFiduciario.Data = Convert.ToDateTime(textData.Text);
        cotacaoMercadoFiduciario.Cotacao = Convert.ToDecimal(textCotacao.Value);
        cotacaoMercadoFiduciario.Save();

        #region Log do Processo
        HistoricoLog historicoLog = new HistoricoLog();
        historicoLog.InsereHistoricoLog(DateTime.Now,
                                        DateTime.Now,
                                        "Cadastro de CotacaoMercadoFiduciario - Operacao: Insert CotacaoMercadoFiduciario: "
                                                + cotacaoMercadoFiduciario.IdTitulo + " - "
                                                + cotacaoMercadoFiduciario.Data
                                                + UtilitarioWeb.ToString(cotacaoMercadoFiduciario),
                                        HttpContext.Current.User.Identity.Name,
                                        UtilitarioWeb.GetIP(Request),
                                        "",
                                        HistoricoLogOrigem.Outros);
        #endregion
    }

    protected void gridCadastro_CustomCallback(object sender, DevExpress.Web.ASPxGridViewCustomCallbackEventArgs e)
    {
        if (e.Parameters == "btnDelete")
        {
            List<object> keyValuesId = gridCadastro.GetSelectedFieldValues("IdTitulo");
            List<object> keyValuesData = gridCadastro.GetSelectedFieldValues("Data");

            for (int i = 0; i < keyValuesData.Count; i++)
            {
                int idTitulo = Convert.ToInt32(keyValuesId[i]);
                DateTime data = Convert.ToDateTime(keyValuesData[i]);

                CotacaoMercadoFiduciario cotacaoMercadoFiduciario = new CotacaoMercadoFiduciario();
                if (cotacaoMercadoFiduciario.LoadByPrimaryKey(idTitulo, data))
                {
                    //
                    CotacaoMercadoFiduciario cotacaoMercadoFiduciarioClone = (CotacaoMercadoFiduciario)Utilitario.Clone(cotacaoMercadoFiduciario);
                    //                    
                    cotacaoMercadoFiduciario.MarkAsDeleted();
                    cotacaoMercadoFiduciario.Save();

                    #region Log do Processo
                    HistoricoLog historicoLog = new HistoricoLog();
                    historicoLog.InsereHistoricoLog(DateTime.Now,
                                                    DateTime.Now,
                                                    "Cadastro de CotacaoMercadoFiduciario - Operacao: Delete CotacaoMercadoFiduciario: " + idTitulo + "; " + data + UtilitarioWeb.ToString(cotacaoMercadoFiduciarioClone),
                                                    HttpContext.Current.User.Identity.Name,
                                                    UtilitarioWeb.GetIP(Request),
                                                    "",
                                                    HistoricoLogOrigem.Outros);
                    #endregion
                }
            }
        }

        gridCadastro.DataBind();
        gridCadastro.Selection.UnselectAll();
        gridCadastro.CancelEdit();
    }

    #region btnPDF_Click, btnExcel_Click, btnOK_Click,
    protected void btnPDF_Click(object sender, EventArgs e)
    {
        gridExport.WritePdfToResponse();
    }

    protected void btnExcel_Click(object sender, EventArgs e)
    {
        gridExport.WriteXlsToResponse();
    }
    #endregion

    #endregion

    #region Exportação
    /* Armazena os Valores idCliente/Data vindos da tela para Construção do Excel */
    private int idCliente;
    private DateTime data;
     
    private void TrataTravamentoCampos() {
        Usuario usuario = new Usuario();
        usuario.BuscaUsuario(HttpContext.Current.User.Identity.Name);
        int tipoTrava = usuario.TipoTrava.Value;

        if (tipoTrava == (int)TipoTravaUsuario.TravaCliente) {
            btnEditCodigo.Enabled = false;

            PermissaoCliente permissaoCliente = new PermissaoCliente();
            int? idCliente = permissaoCliente.RetornaClienteAssociado(HttpContext.Current.User.Identity.Name);

            if (idCliente.HasValue) {
                Cliente cliente = new Cliente();

                List<esQueryItem> campos = new List<esQueryItem>();
                campos.Add(cliente.Query.Apelido);
                cliente.LoadByPrimaryKey(campos, idCliente.Value);
                string apelido = cliente.Apelido;

                btnEditCodigo.Text = Convert.ToString(idCliente.Value);
                textNome.Text = apelido;
            }
        }
    }

    protected void ASPxCallback1_Callback(object source, DevExpress.Web.CallbackEventArgs e) {
        string nome = "";
        if (!String.IsNullOrEmpty(e.Parameter) && e.Parameter != "null") {
            int idCliente = Convert.ToInt32(e.Parameter);
            Cliente cliente = new Cliente();
            cliente.LoadByPrimaryKey(idCliente);

            if (cliente.str.Apelido != "") {
                if (cliente.IsAtivo) {
                    PermissaoCliente permissaoCliente = new PermissaoCliente();
                    nome = permissaoCliente.RetornaAcessoCliente(idCliente, HttpContext.Current.User.Identity.Name)
                            ? cliente.str.Apelido : "no_access";
                }
                else {
                    nome = "no_active";
                }
            }
        }
        e.Result = nome;
    }

    protected void EsDSCliente_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e) {
        ClienteCollection clienteCollection = new ClienteCollection();
        clienteCollection.BuscaClientesComAcesso(HttpContext.Current.User.Identity.Name);

        e.Collection = clienteCollection;
    }

    /// <summary>
    /// Redireciona para ExibeExportacaoCustom para exportar a planilha excel
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnExcel1_Click(object sender, EventArgs e) {
        #region Campos obrigatórios
        List<Control> controles = new List<Control>(new Control[] { 
    	    this.btnEditCodigo, this.textData });

        if (base.TestaObrigatorio(controles) != "") {
            throw new Exception("Campos com * são obrigatórios!");
        }
        #endregion

        // Armazena os valores para posterior uso
        this.idCliente = Convert.ToInt32(this.btnEditCodigo.Text.Trim());
        this.data = Convert.ToDateTime(this.textData.Text);

        //
        MemoryStream msExcel = new MemoryStream();
        string nomeArquivoExcel;

        //
        this.ExportaExcel(out msExcel, out nomeArquivoExcel);
        //

        // Salva Memory Stream do Arquivo xls na Session
        Session["streamExcel"] = msExcel;
        Session["nomeArquivoExcel"] = nomeArquivoExcel;
        //
        //Session["textData"] = this.textDataCota.Text;

        string redirect = "~/InterfacesCustom/ExibeExportacaoCustom.aspx?Owner=" + (int)OwnerExportacao.RbCapital;
        redirect += "&Tipo=" + (int)TipoExportacao.Fluxo;
                
        Response.Redirect(redirect, true);
    }

    /// <summary>
    /// Controla a montagem dos elementos do Excel
    /// </summary>
    /// <param name="msExcel"></param>
    /// <param name="nomeArquivoExcel"></param>
    private void ExportaExcel(out MemoryStream msExcel, out string nomeArquivoExcel) {
        // out
        msExcel = new MemoryStream();
        nomeArquivoExcel = "RbCapital.xls";
        //

        Spreadsheet document = new Bytescout.Spreadsheet.Spreadsheet();

        string pathModelos = AppDomain.CurrentDomain.BaseDirectory + "\\Relatorios\\Modelos";
        string pathPlanilhaModelo = String.Format("{0}\\FluxoRB.xlsx", pathModelos);

        document.LoadFromFile(pathPlanilhaModelo);

        Worksheet worksheet = document.Workbook.Worksheets[0];

        //worksheet.ViewOptions.ShowZeroValues = true;
        worksheet.ViewOptions.GridLinesColorEnabled = true;
        /*-----------------------------------------------------*/

        this.SetPlanilha(worksheet);

        /*-----------------------------------------------------*/

        #region Esconde Planilha Demo do componente
        const string BYTESCOUT_WORKSHEET_NAME = "Bytescout Spreadsheet SDK";
        worksheet = document.Workbook.Worksheets.ByName(BYTESCOUT_WORKSHEET_NAME);
        if (worksheet == null) {
            worksheet = document.Workbook.Worksheets.Add(BYTESCOUT_WORKSHEET_NAME);
        }
        worksheet.Visible = Bytescout.Spreadsheet.Constants.SHEETVISIBILITY.Hidden;
        #endregion

        //
        document.SaveToStreamXLS(msExcel);
        //
        document.Close();
    }


    private void SetPlanilha(Worksheet worksheet) {
        Cliente cliente = new Cliente();
        List<esQueryItem> campos = new List<esQueryItem>();
        campos.Add(cliente.Query.Nome);
        cliente.LoadByPrimaryKey(this.idCliente);
        //
        worksheet.Cell("D2").ValueAsString = cliente.Nome.Trim();       
        worksheet.Cell("D3").ValueAsString = this.data.ToString("MMMM/yyyy", CultureInfo.CreateSpecificCulture("pt-BR"));
        //
        //
        FluxoRB fluxo = new FluxoRB();
        List<FluxoRB.Fluxo> lista = fluxo.RetornaListaFluxo(this.data, this.idCliente);
        //
        if (!String.IsNullOrEmpty(fluxo.Titulo1)) {
            worksheet.Cell("D6").Value = fluxo.Titulo1;
        }
        if (!String.IsNullOrEmpty(fluxo.Titulo2)) {
            worksheet.Cell("E6").Value = fluxo.Titulo2;
        }
        if (!String.IsNullOrEmpty(fluxo.Titulo3)) {
            worksheet.Cell("F6").Value = fluxo.Titulo3;
        }
        if (!String.IsNullOrEmpty(fluxo.Titulo4)) {
            worksheet.Cell("G6").Value = fluxo.Titulo4;
        }
        if (!String.IsNullOrEmpty(fluxo.Titulo5)) {
            worksheet.Cell("H6").Value = fluxo.Titulo5;
        }
        if (!String.IsNullOrEmpty(fluxo.Titulo6)) {
            worksheet.Cell("I6").Value = fluxo.Titulo6;
        }
        if (!String.IsNullOrEmpty(fluxo.Titulo7)) {
            worksheet.Cell("J6").Value = fluxo.Titulo7;
        }
        if (!String.IsNullOrEmpty(fluxo.Titulo8)) {
            worksheet.Cell("K6").Value = fluxo.Titulo8;
        }

        int linha = 6;
        decimal totalFluxo1 = 0;
        decimal totalFluxo2 = 0;
        decimal totalFluxo3 = 0;
        decimal totalFluxo4 = 0;
        decimal totalFluxo5 = 0;
        decimal totalFluxo6 = 0;
        decimal totalFluxo7 = 0;
        decimal totalFluxo8 = 0;
        decimal totalFluxos = 0;
        decimal totalValorizacao = 0;
        decimal totalAmortizacao = 0;
        for (int i = 0; i < lista.Count; i++) 
        {
            bool escondeLinha = false;
            int anoSeguinte = DateTime.Now.Year + 1;
            if (lista[i].Data.Year > anoSeguinte && lista[i].Data.Month != 12 && i != lista.Count - 1)
            {
                escondeLinha = true;
            }


            // Add new row            
            worksheet.Rows.Insert(linha, 1);            

            // borda esquerda/direita
            worksheet.Rows[linha][1].LeftBorderStyle = LineStyle.Thin;
            worksheet.Rows[linha][3].LeftBorderStyle = LineStyle.Thin;
            worksheet.Rows[linha][14].LeftBorderStyle = LineStyle.Thin;
            //
            worksheet.Rows[linha][1].RightBorderStyle = LineStyle.Thin;
            worksheet.Rows[linha][12].RightBorderStyle = LineStyle.Thin;
            worksheet.Rows[linha][17].RightBorderStyle = LineStyle.Thin;
            //
            //
            worksheet.Rows[linha][3].NumberFormatString = "#,##,#0.00";
            worksheet.Rows[linha][4].NumberFormatString = "#,##,#0.00";
            worksheet.Rows[linha][5].NumberFormatString = "#,##,#0.00";
            worksheet.Rows[linha][6].NumberFormatString = "#,##,#0.00";
            worksheet.Rows[linha][7].NumberFormatString = "#,##,#0.00";
            worksheet.Rows[linha][8].NumberFormatString = "#,##,#0.00";
            worksheet.Rows[linha][9].NumberFormatString = "#,##,#0.00";
            worksheet.Rows[linha][10].NumberFormatString = "#,##,#0.00";
            worksheet.Rows[linha][11].NumberFormatString = "#,##,#0.00";
            worksheet.Rows[linha][12].NumberFormatString = "#,##,#0.00";            
            worksheet.Rows[linha][14].NumberFormatString = "#,##,#0.00";
            worksheet.Rows[linha][15].NumberFormatString = "#,##,#0.00";
            worksheet.Rows[linha][16].NumberFormatString = "#,##,#0.00";
            worksheet.Rows[linha][17].NumberFormatString = "#,##,#0.00";
            //
            //
            
            worksheet.Rows[linha][1].ValueAsString = lista[i].Data.ToString("MMM-yyyy", CultureInfo.CreateSpecificCulture("pt-BR"));


            totalFluxo1 += lista[i].Fluxo1;
            totalFluxo2 += lista[i].Fluxo2;
            totalFluxo3 += lista[i].Fluxo3;
            totalFluxo4 += lista[i].Fluxo4;
            totalFluxo5 += lista[i].Fluxo5;
            totalFluxo6 += lista[i].Fluxo6;
            totalFluxo7 += lista[i].Fluxo7;
            totalFluxo8 += lista[i].Fluxo8;
            totalFluxos += lista[i].Total;
            totalValorizacao += lista[i].Valorizacao;
            totalAmortizacao += lista[i].Amortizacao;
            

            if (escondeLinha)
            {
                worksheet.Rows[linha][3].ValueAsDouble = Convert.ToDouble(lista[i].Fluxo1);
                worksheet.Rows[linha][4].ValueAsDouble = Convert.ToDouble(lista[i].Fluxo2);
                worksheet.Rows[linha][5].ValueAsDouble = Convert.ToDouble(lista[i].Fluxo3);
                worksheet.Rows[linha][6].ValueAsDouble = Convert.ToDouble(lista[i].Fluxo4);
                worksheet.Rows[linha][7].ValueAsDouble = Convert.ToDouble(lista[i].Fluxo5);
                worksheet.Rows[linha][8].ValueAsDouble = Convert.ToDouble(lista[i].Fluxo6);
                worksheet.Rows[linha][9].ValueAsDouble = Convert.ToDouble(lista[i].Fluxo7);
                worksheet.Rows[linha][10].ValueAsDouble = Convert.ToDouble(lista[i].Fluxo8);
                worksheet.Rows[linha][11].ValueAsDouble = Convert.ToDouble(lista[i].Total);
                worksheet.Rows[linha][15].ValueAsDouble = Convert.ToDouble(lista[i].Valorizacao);
                worksheet.Rows[linha][16].ValueAsDouble = Convert.ToDouble(lista[i].Amortizacao);
            }
            else
            {
                worksheet.Rows[linha][3].ValueAsDouble = Convert.ToDouble(totalFluxo1);
                worksheet.Rows[linha][4].ValueAsDouble = Convert.ToDouble(totalFluxo2);
                worksheet.Rows[linha][5].ValueAsDouble = Convert.ToDouble(totalFluxo3);
                worksheet.Rows[linha][6].ValueAsDouble = Convert.ToDouble(totalFluxo4);
                worksheet.Rows[linha][7].ValueAsDouble = Convert.ToDouble(totalFluxo5);
                worksheet.Rows[linha][8].ValueAsDouble = Convert.ToDouble(totalFluxo6);
                worksheet.Rows[linha][9].ValueAsDouble = Convert.ToDouble(totalFluxo7);
                worksheet.Rows[linha][10].ValueAsDouble = Convert.ToDouble(totalFluxo8);
                worksheet.Rows[linha][11].ValueAsDouble = Convert.ToDouble(totalFluxos);
                worksheet.Rows[linha][15].ValueAsDouble = Convert.ToDouble(totalValorizacao);
                worksheet.Rows[linha][16].ValueAsDouble = Convert.ToDouble(totalAmortizacao);                
            }

            //
            
            worksheet.Rows[linha][12].ValueAsDouble = Convert.ToDouble(lista[i].Acumulado);            
            worksheet.Rows[linha][17].ValueAsDouble = Convert.ToDouble(lista[i].PlFinal);

            worksheet.Rows[linha][14].ValueAsDouble = Convert.ToDouble(lista[i].PlFinal + totalAmortizacao);
            //                                    
            worksheet.Rows[linha][1].AlignmentHorizontal = Bytescout.Spreadsheet.Constants.AlignmentHorizontal.Centered;
            
            worksheet.Rows[linha].Hidden = escondeLinha;

            if (!escondeLinha)
            {
                totalFluxo1 = 0;
                totalFluxo2 = 0;
                totalFluxo3 = 0;
                totalFluxo4 = 0;
                totalFluxo5 = 0;
                totalFluxo6 = 0;
                totalFluxo7 = 0;
                totalFluxo8 = 0;
                totalFluxos = 0;
                totalValorizacao = 0;
                totalAmortizacao = 0;
            }

            linha++;

            //borda inferior para a ultima linha
            if (i == lista.Count-1) {

                worksheet.Rows[linha-1][1].BottomBorderStyle = LineStyle.Thin;
                worksheet.Rows[linha-1][2].BottomBorderStyle = LineStyle.Thin;
                worksheet.Rows[linha-1][3].BottomBorderStyle = LineStyle.Thin;
                worksheet.Rows[linha-1][4].BottomBorderStyle = LineStyle.Thin;
                worksheet.Rows[linha-1][5].BottomBorderStyle = LineStyle.Thin;
                worksheet.Rows[linha-1][6].BottomBorderStyle = LineStyle.Thin;
                worksheet.Rows[linha-1][7].BottomBorderStyle = LineStyle.Thin;
                worksheet.Rows[linha-1][8].BottomBorderStyle = LineStyle.Thin;
                worksheet.Rows[linha-1][9].BottomBorderStyle = LineStyle.Thin;
                worksheet.Rows[linha-1][10].BottomBorderStyle = LineStyle.Thin;
                
                worksheet.Rows[linha-1][11].BottomBorderStyle = LineStyle.Thin;
                worksheet.Rows[linha-1][12].BottomBorderStyle = LineStyle.Thin;
                worksheet.Rows[linha-1][14].BottomBorderStyle = LineStyle.Thin;
                worksheet.Rows[linha-1][15].BottomBorderStyle = LineStyle.Thin;
                worksheet.Rows[linha-1][16].BottomBorderStyle = LineStyle.Thin;
                worksheet.Rows[linha-1][17].BottomBorderStyle = LineStyle.Thin;                      
            }
        }
    }

    #endregion

    #region Exportacao1
    protected void EsDSTituloRendaFixaFiltro_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e) {
        TituloRendaFixaQuery tituloRendaFixaQuery = new TituloRendaFixaQuery("T");
        PapelRendaFixaQuery papelRendaFixaQuery = new PapelRendaFixaQuery("P");
        tituloRendaFixaQuery.Select(tituloRendaFixaQuery.IdTitulo,
                                    tituloRendaFixaQuery.Descricao,
                                    tituloRendaFixaQuery.DataEmissao,
                                    tituloRendaFixaQuery.DataVencimento,
                                    tituloRendaFixaQuery.DescricaoCompleta.Trim(),
                                    papelRendaFixaQuery.Descricao.As("DescricaoPapel"));
        tituloRendaFixaQuery.InnerJoin(papelRendaFixaQuery).On(papelRendaFixaQuery.IdPapel == tituloRendaFixaQuery.IdPapel);
        tituloRendaFixaQuery.OrderBy(tituloRendaFixaQuery.IdTitulo.Ascending, tituloRendaFixaQuery.DataVencimento.Ascending);

        TituloRendaFixaCollection coll = new TituloRendaFixaCollection();
        coll.Load(tituloRendaFixaQuery);

        //
        e.Collection = coll;
    }

    /// <summary>
    /// Redireciona para ExibeExportacaoCustom para exportar a planilha excel
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnExcel2_Click(object sender, EventArgs e) {
        #region Campos obrigatórios
        List<Control> controles = new List<Control>(new Control[] { 
    	    this.dropTituloRendaFixa, this.textDataAmortizacao });

        if (base.TestaObrigatorio(controles) != "") {
            throw new Exception("Campos com * são obrigatórios!");
        }
        #endregion

        MemoryStream msExcel = new MemoryStream();
        string nomeArquivoExcel;

        //
        this.ExportaAmortizacaoExcel(out msExcel, out nomeArquivoExcel);
        //

        // Salva Memory Stream do Arquivo xls na Session
        Session["streamExcel"] = msExcel;
        Session["nomeArquivoExcel"] = nomeArquivoExcel;
        //

        string redirect = "~/InterfacesCustom/ExibeExportacaoCustom.aspx?Owner=" + (int)OwnerExportacao.RbCapital;
        redirect += "&Tipo=" + (int)TipoExportacao.Amortizacao;

        Response.Redirect(redirect, true);
    }

    /// <summary>
    /// Controla a montagem dos elementos do Excel
    /// </summary>
    /// <param name="msExcel"></param>
    /// <param name="nomeArquivoExcel"></param>
    private void ExportaAmortizacaoExcel(out MemoryStream msExcel, out string nomeArquivoExcel) {
        // out
        msExcel = new MemoryStream();
        nomeArquivoExcel = "RbCapitalAmortizacao.xls";
        //

        Spreadsheet document = new Bytescout.Spreadsheet.Spreadsheet();

        string pathModelos = AppDomain.CurrentDomain.BaseDirectory + "\\Relatorios\\Modelos";
        string pathPlanilhaModelo = String.Format("{0}\\AmortizacaoRBCapital.xls", pathModelos);

        document.LoadFromFile(pathPlanilhaModelo);

        Worksheet worksheet = document.Workbook.Worksheets[0];

        //worksheet.ViewOptions.ShowZeroValues = true;
        worksheet.ViewOptions.GridLinesColorEnabled = true;
        /*-----------------------------------------------------*/

        this.SetPlanilhaAmortizacao(worksheet);

        /*-----------------------------------------------------*/

        #region Esconde Planilha Demo do componente
        const string BYTESCOUT_WORKSHEET_NAME = "Bytescout Spreadsheet SDK";
        worksheet = document.Workbook.Worksheets.ByName(BYTESCOUT_WORKSHEET_NAME);
        if (worksheet == null) {
            worksheet = document.Workbook.Worksheets.Add(BYTESCOUT_WORKSHEET_NAME);
        }
        worksheet.Visible = Bytescout.Spreadsheet.Constants.SHEETVISIBILITY.Hidden;
        #endregion

        //
        document.SaveToStreamXLS(msExcel);
        
        document.Close();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="worksheet"></param>
    private void SetPlanilhaAmortizacao(Worksheet worksheet) {
        
        int idTitulo = Convert.ToInt32(dropTituloRendaFixa.Value);                
        DateTime dataAmortizacao = Convert.ToDateTime(this.textDataAmortizacao.Text);
        //
        worksheet.Cell("A3").ValueAsDateTime = dataAmortizacao;
        //
        worksheet.Cell("A14").ValueAsDateTime = DateTime.Now;
        //
        TituloRendaFixa tituloRendaFixa = new TituloRendaFixa();
        List<esQueryItem> campos = new List<esQueryItem>();
        campos.Add(tituloRendaFixa.Query.Descricao);
        tituloRendaFixa.LoadByPrimaryKey(campos, idTitulo);
        //
        worksheet.Cell("A5").ValueAsString = tituloRendaFixa.Descricao;
        worksheet.Cell("B7").ValueAsString = tituloRendaFixa.Descricao;

        // Juros/Amortização/Pagamento Total        
        #region Juros
        AgendaRendaFixaCollection agendaRendaFixaCollection = new AgendaRendaFixaCollection();
        agendaRendaFixaCollection.Query.Select(agendaRendaFixaCollection.Query.Valor.Sum());

        agendaRendaFixaCollection.Query.Where(agendaRendaFixaCollection.Query.IdTitulo == idTitulo,
                                              agendaRendaFixaCollection.Query.DataPagamento == dataAmortizacao,
                                              agendaRendaFixaCollection.Query.TipoEvento.In((byte)TipoEventoTitulo.Juros,
                                                                                            (byte)TipoEventoTitulo.JurosCorrecao,
                                                                                            (byte)TipoEventoTitulo.PagamentoPU));
        agendaRendaFixaCollection.Query.Load();
        decimal valor = 0.00M;
        if (agendaRendaFixaCollection.HasData) {
            if (agendaRendaFixaCollection[0].Valor.HasValue) {
                valor = agendaRendaFixaCollection[0].Valor.Value;
            }
        }

        //worksheet.Cell("B5").NumberFormatString = "R$ #.##0,00";
        //worksheet.Cell("B5").Value = valor.ToString("N2");
        worksheet.Cell("B5").Value = valor.ToString("C2");
        #endregion        
        
        #region Amortização
        agendaRendaFixaCollection = new AgendaRendaFixaCollection();
        agendaRendaFixaCollection.Query.Select(agendaRendaFixaCollection.Query.Valor.Sum());

        agendaRendaFixaCollection.Query.Where(agendaRendaFixaCollection.Query.IdTitulo == idTitulo,
                                              agendaRendaFixaCollection.Query.DataPagamento == dataAmortizacao,
                                              agendaRendaFixaCollection.Query.TipoEvento.In((byte)TipoEventoTitulo.Amortizacao,
                                                                                            (byte)TipoEventoTitulo.AmortizacaoCorrigida));
        agendaRendaFixaCollection.Query.Load();
        decimal valor1 = 0.00M;
        if (agendaRendaFixaCollection.HasData) {
            if (agendaRendaFixaCollection[0].Valor.HasValue) {
                valor1 = agendaRendaFixaCollection[0].Valor.Value;
            }
        }
        //worksheet.Cell("C5").NumberFormatString = "R$ #.##0,00";
        //worksheet.Cell("C5").ValueAsString = valor1.ToString("C2");
        worksheet.Cell("C5").Value = valor1.ToString("C2");

        #endregion        
        
        // Total
        decimal valorTotal = valor + valor1;
        worksheet.Cell("D5").Value = valorTotal.ToString("C2");


        #region Dados Detail
        LiquidacaoRendaFixaQuery liquidacaoRendaFixaQuery = new LiquidacaoRendaFixaQuery("L");
        ClienteQuery clienteQuery = new ClienteQuery("C");

        liquidacaoRendaFixaQuery
                .Select(clienteQuery.Nome, 
                        clienteQuery.IdCliente,
                        liquidacaoRendaFixaQuery.ValorLiquido.Sum(),
                        liquidacaoRendaFixaQuery.Quantidade.Sum());

        liquidacaoRendaFixaQuery.InnerJoin(clienteQuery).On(liquidacaoRendaFixaQuery.IdCliente == clienteQuery.IdCliente);
        liquidacaoRendaFixaQuery.Where(liquidacaoRendaFixaQuery.DataLiquidacao == dataAmortizacao,
                                       liquidacaoRendaFixaQuery.IdTitulo == idTitulo);
        //
        liquidacaoRendaFixaQuery.GroupBy(clienteQuery.Nome, clienteQuery.IdCliente);
        //
        liquidacaoRendaFixaQuery.OrderBy(clienteQuery.Nome.Ascending);
        //
        LiquidacaoRendaFixaCollection liquidacaoRendaFixaCollection = new LiquidacaoRendaFixaCollection();        
        #endregion
        liquidacaoRendaFixaCollection.Load(liquidacaoRendaFixaQuery);

        int linha = 7;
        decimal totalLiquido = 0;
        for (int i = 0; i < liquidacaoRendaFixaCollection.Count; i++) 
        {
            LiquidacaoRendaFixaCollection liquidacaoRendaFixaCollectionAux = new LiquidacaoRendaFixaCollection();
            liquidacaoRendaFixaCollectionAux.Query.Select(liquidacaoRendaFixaCollectionAux.Query.Quantidade.Sum(),
                                                          liquidacaoRendaFixaCollectionAux.Query.TipoLancamento);
            liquidacaoRendaFixaCollectionAux.Query.Where(liquidacaoRendaFixaCollectionAux.Query.IdCliente.Equal(liquidacaoRendaFixaCollection[i].IdCliente.Value),
                                                         liquidacaoRendaFixaCollectionAux.Query.IdTitulo.Equal(idTitulo),
                                                         liquidacaoRendaFixaCollectionAux.Query.DataLiquidacao.Equal(dataAmortizacao));
            liquidacaoRendaFixaCollectionAux.Query.GroupBy(liquidacaoRendaFixaCollectionAux.Query.TipoLancamento);
            liquidacaoRendaFixaCollectionAux.Query.Load();
            decimal quantidade = liquidacaoRendaFixaCollectionAux[0].Quantidade.Value;

            // Add new row            
            worksheet.Rows.Insert(linha, 1);
            //
            worksheet.Rows[linha][0].ValueAsString = (string)liquidacaoRendaFixaCollection[i].GetColumn(ClienteMetadata.ColumnNames.Nome);
            worksheet.Rows[linha][1].Value = quantidade.ToString("N2");
            worksheet.Rows[linha][3].Value = liquidacaoRendaFixaCollection[i].ValorLiquido.Value.ToString("C2");
            //
            totalLiquido += liquidacaoRendaFixaCollection[i].ValorLiquido.Value;

            #region cpf
            Pessoa pessoa = new Pessoa();
            pessoa.LoadByPrimaryKey( Convert.ToInt32(liquidacaoRendaFixaCollection[i].GetColumn(ClienteMetadata.ColumnNames.IdCliente) ) );

            string cpf = "";
            if (!String.IsNullOrEmpty(pessoa.str.Cpfcnpj)) {
                cpf = pessoa.str.Cpfcnpj; // Overload já com a mascara Correta de CPF ou CNPJ
            }

            if (!String.IsNullOrEmpty(cpf)) {
                worksheet.Rows[linha][2].Value = cpf;
                worksheet.Rows[linha][2].Font = new Font("Times", 8, FontStyle.Regular);
                worksheet.Rows[linha][2].AlignmentHorizontal = AlignmentHorizontal.Centered; 
            }
            #endregion
            
            #region ContaCorrente/Banco/Agencia
                                    
            ContaCorrenteCollection c = new ContaCorrenteCollection();
            //
            c.Query.Select(c.Query.IdConta, c.Query.IdAgencia, c.Query.IdBanco, c.Query.Numero,c.Query.DigitoConta)
                   .Where(c.Query.IdPessoa == Convert.ToInt32(liquidacaoRendaFixaCollection[i].GetColumn(ClienteMetadata.ColumnNames.IdCliente)),
                          c.Query.ContaDefault == "S");

            if (c.Query.Load()) {

                string digito = c[0].str.DigitoConta.Trim();
                string conta = c[0].str.Numero.Trim();
                if (!String.IsNullOrEmpty(digito)) {
                    if(!String.IsNullOrEmpty(conta)) {
                        conta += "-" + digito;
                    }
                }
                worksheet.Rows[linha][6].Value = conta;
                worksheet.Rows[linha][6].Font = new Font("Times", 8, FontStyle.Regular);
                worksheet.Rows[linha][6].AlignmentHorizontal = AlignmentHorizontal.Centered;

                int? idAgencia = c[0].IdAgencia;
                int? idBanco = c[0].IdBanco;

                if (idAgencia.HasValue) {
                    Agencia agencia = new Agencia();
                    if (agencia.LoadByPrimaryKey(idAgencia.Value)) {
                        worksheet.Rows[linha][5].Value = agencia.str.Codigo.Trim();
                        worksheet.Rows[linha][5].Font = new Font("Times", 8, FontStyle.Regular);
                        worksheet.Rows[linha][5].AlignmentHorizontal = AlignmentHorizontal.Centered;                        
                    }
                }

                if (idBanco.HasValue) {
                    Banco banco = new Banco();
                    if (banco.LoadByPrimaryKey(idBanco.Value)) {
                        worksheet.Rows[linha][4].Value = banco.str.Nome.Trim() + " (" +banco.str.CodigoCompensacao.Trim()+ ")";
                        worksheet.Rows[linha][4].Font = new Font("Times", 8, FontStyle.Regular);
                        worksheet.Rows[linha][4].AlignmentHorizontal = AlignmentHorizontal.Centered;
                    }
                }
            }
            #endregion

            // Formatos            
            worksheet.Rows[linha][0].Font = new Font("Times", 8, FontStyle.Bold);
            worksheet.Rows[linha][1].Font = new Font("Times", 8, FontStyle.Regular);
            worksheet.Rows[linha][1].AlignmentHorizontal = AlignmentHorizontal.Centered; 
            worksheet.Rows[linha][3].Font = new Font("Times", 8, FontStyle.Regular);
            worksheet.Rows[linha][3].AlignmentHorizontal = AlignmentHorizontal.Right;

            // Borda Direita
            worksheet.Rows[linha][6].RightBorderStyle = LineStyle.Thin;

            linha++;
        }

        #region Coloca Total 
        

        if (liquidacaoRendaFixaCollection.Count > 0) {
            int inicio = 8;
            int final = 7 + liquidacaoRendaFixaCollection.Count;

            //string sum = "D" + inicio + ":D" + final;
            //worksheet1.Rows[linha + 1][3].NumberFormatString = "#,##,#0.0000%";
            //worksheet.Rows[linha + 1][3].Value = "=SUM(" + sum + ")";

            int linhaAux  = linha+1;
            //worksheet.Cell("D" + linhaAux).Value = "=SUM(" + sum + ")";            
            worksheet.Cell("D" + linhaAux).Value = totalLiquido.ToString("C2");
            worksheet.Cell("D" + linhaAux).AlignmentHorizontal = AlignmentHorizontal.Right;
            worksheet.Cell("D" + linhaAux).Font = new Font("Times", 8, FontStyle.Bold);
        }
        #endregion

    }
    #endregion
}