﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.IO;
using System.Globalization;
using Financial.Export;
using System.Text;
using Bytescout.Spreadsheet;
using System.Collections.Generic;
using System.Drawing;
using Financial.Investidor;
using Financial.Security;
using Financial.Web.Common;
using EntitySpaces.Interfaces;
using Financial.Security.Enums;
using Financial.Bolsa;
using Financial.Fundo;
using Financial.RendaFixa;
using Financial.Util;
using Financial.Fundo.Enums;
using Financial.RendaFixa.Enums;
using Financial.Tributo.Custom;
using Financial.Common;
using Financial.Common.Enums;
using Financial.Bolsa.Enums;
using Financial.InvestidorCotista;
using DevExpress.Web;
using Financial.Investidor.Enums;
using Financial.Relatorio;
using Financial.InterfacesCustom;

public partial class InterfacesCustom_Masa : FiltroReportBasePage {
    // Controla Exibição de Importação ou Exportação
    private Tipo tipo;

    enum Tipo {
        Importacao = 0,
        Exportacao = 1
    }
    /*-----------------------------------------*/

    /// <summary>
    /// Controla Exibição de Importação ou Exportação
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void panelEdicao_Load(object sender, EventArgs e) {
        Control c1 = this.FindControl("divExportacao");
        Control c2 = this.FindControl("divImportacao");

        if (this.tipo == Tipo.Exportacao) {
            if (c1 != null) { c1.Visible = true; }
            if (c2 != null) { c2.Visible = false; }
        }
        else if (this.tipo == Tipo.Importacao) {
            if (c1 != null) { c1.Visible = false; }
            if (c2 != null) { c2.Visible = true; }
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    new protected void Page_Load(object sender, EventArgs e) {
        
        this.tipo = (Tipo)Enum.Parse(typeof(Tipo), Request.QueryString["param"].ToString());
        this.HasPopupAtivoBolsa = true;
        //
        //
        this.HasPopupCliente = true;
        //
        
        //TrataTravamentoCampos();

        base.Page_Load(sender, e);
    }

    protected void btnSalvar_Click(object sender, EventArgs e)
    {
        try
        {
            if (this.btnEditAtivoBolsa.Text == "")
            {
                throw new Exception("Selecione o Ativo");
            }

            if (this.dropTipo.SelectedIndex == -1)
            {
                throw new Exception("Preencha o Tipo");
            }

            TipoAtivoMasaCollection tipoAtivoMasaCollection = new TipoAtivoMasaCollection();
            AtivoBolsaCollection ativoBolsaCollection = new AtivoBolsaCollection();

            ativoBolsaCollection.Query.Where(ativoBolsaCollection.Query.CdAtivoBolsa.Equal(this.btnEditAtivoBolsa.Value.ToString()));
            if (!ativoBolsaCollection.LoadAll())
            {
                throw new Exception("Ativo não encontrado.");
            }

            tipoAtivoMasaCollection.Query.Where(tipoAtivoMasaCollection.Query.CdAtivo.Equal(this.btnEditAtivoBolsa.Value.ToString()));
            
            if (tipoAtivoMasaCollection.LoadAll())
            {
                tipoAtivoMasaCollection[0].Tipo = Convert.ToByte(this.dropTipo.Value);
            }
            else
            {
                TipoAtivoMasa tipoAtivoMasa = tipoAtivoMasaCollection.AddNew();
                tipoAtivoMasa.CdAtivo = (string)this.btnEditAtivoBolsa.Value;
                tipoAtivoMasa.Tipo = Convert.ToByte(this.dropTipo.Value);
            }
            tipoAtivoMasaCollection.Save();
        }
        catch (Exception ex)
        {
            throw ex;
        }
        throw new Exception("Os dados foram salvos");
    }

    #region Exportação

    #region Enum p/ Exportacao
    enum OwnerExportacao {
        Masa = 2
    }

    enum TipoExportacaoMasa {
        ParticipacaoSocietaria = 0,
        EvolucaoCapitalSocial = 1,
        ControleParticipacaoSocietariaAcoes = 2,
        ControleParticipacaoSocietariaFundos = 3,
        ComposicaoCarteira = 4
    }
    #endregion

    /// <summary>
    /// Redireciona para ExibeExportacaoCustom para Exportar a planilha excel
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void callbackExportaExcel_Callback(object source, DevExpress.Web.CallbackEventArgs e) {
       
        if (e.Parameter == "exp1") {

            #region Participação Societária

            #region Campos obrigatórios
            List<Control> controles = new List<Control>(new Control[] { this.textData, this.dropEmissor1 });

            if (base.TestaObrigatorio(controles) != "") {
                e.Result = "Campos com * são obrigatórios!";
                return;
            }
            #endregion

            try {
                MemoryStream msExcel = new MemoryStream();
                string nomeArquivoExcel;

                int? idEmissor1 = null;
                //
                if (this.dropEmissor1.Text.Trim() != "") {
                    string emissor1 = this.dropEmissor1.Text.Trim();
                    string[] emissorAux1 = emissor1.Split(new Char[] { '-' });
                    //
                    idEmissor1 = Convert.ToInt32(emissorAux1[0].Trim());
                }

                new ParticipacaoSocietariaMasa(Convert.ToDateTime(this.textData.Value), idEmissor1.Value)
                                .ExportaExcel(out msExcel, out nomeArquivoExcel);

                // Salva Memory Stream do Arquivo xls na Session
                Session["streamExcel"] = msExcel;
                Session["nomeArquivoExcel"] = nomeArquivoExcel;
            }
            catch (Exception erro) {
                e.Result = erro.Message;
                return;
            }

            #endregion
        }
        else if (e.Parameter == "exp2") {

            #region Evolução Capital Social

            #region Campos obrigatórios
            List<Control> controles = new List<Control>(new Control[] { this.dropEmissor });

            if (base.TestaObrigatorio(controles) != "") {
                e.Result = "Campos com * são obrigatórios!";
                return;
            }
            #endregion

            try {
                MemoryStream msExcel = new MemoryStream();
                string nomeArquivoExcel;

                int? idEmissor = null;
                //
                if (this.dropEmissor.Text.Trim() != "") {
                    string emissor = this.dropEmissor.Text.Trim();
                    string[] emissorAux = emissor.Split(new Char[] { '-' });
                    //
                    idEmissor = Convert.ToInt32(emissorAux[0].Trim());
                }

                DateTime? dataInicio = null;
                if (this.textDataInicio.Text != "")
                {
                    dataInicio = Convert.ToDateTime(this.textDataInicio.Text);
                }

                DateTime? dataFim = null;
                if (this.textDataFim.Text != "")
                {
                    dataFim = Convert.ToDateTime(this.textDataFim.Text);
                }

                new EvolucaoCapitalSocialMasa(idEmissor, dataInicio, dataFim)
                                .ExportaExcel(out msExcel, out nomeArquivoExcel);

                // Salva Memory Stream do Arquivo xls na Session
                Session["streamExcel"] = msExcel;
                Session["nomeArquivoExcel"] = nomeArquivoExcel;
            }
            catch (Exception erro) {
                e.Result = erro.Message;
                return;
            }

            #endregion
        }

        else if (e.Parameter == "exp3") 
        {
            #region ControleParticipacaoSocietaria

            #region Campos obrigatórios
            List<Control> controles = new List<Control>(new Control[] { this.dropCliente, this.dropCodigoAtivo });

            if (base.TestaObrigatorio(controles) != "") {
                e.Result = "Campos com * são obrigatórios!";
                return;
            }
            #endregion

            try {
                MemoryStream msExcel = new MemoryStream();
                string nomeArquivoExcel;

                
                //
                int idCliente = Convert.ToInt32(this.dropCliente.Text.Trim());

                Cliente clienteData = new Cliente();
                clienteData.LoadByPrimaryKey(idCliente);

                DateTime dataInicio = new DateTime();
                if (this.textDataInicio1.Text != "")
                {
                    dataInicio = Convert.ToDateTime(this.textDataInicio1.Value);

                    if (dataInicio < clienteData.DataImplantacao.Value)
                    {
                        dataInicio = clienteData.DataImplantacao.Value;
                    }
                }
                else
                {
                    OperacaoBolsa operacaoBolsa = new OperacaoBolsa();
                    operacaoBolsa.Query.Select(operacaoBolsa.Query.Data.Min());
                    operacaoBolsa.Query.Where(operacaoBolsa.Query.IdCliente.Equal(idCliente),
                                              operacaoBolsa.Query.CdAtivoBolsa.Equal(this.dropCodigoAtivo.Text.Trim()));
                    operacaoBolsa.Query.Load();

                    if (operacaoBolsa.Data.HasValue)
                    {
                        dataInicio = operacaoBolsa.Data.Value;
                    }
                    else
                    {
                        dataInicio = clienteData.DataImplantacao.Value;
                    }

                    PosicaoBolsaHistorico posicaoBolsaHistorico = new PosicaoBolsaHistorico();
                    posicaoBolsaHistorico.Query.Select(posicaoBolsaHistorico.Query.DataHistorico.Min());
                    posicaoBolsaHistorico.Query.Where(posicaoBolsaHistorico.Query.IdCliente.Equal(idCliente),
                                              posicaoBolsaHistorico.Query.CdAtivoBolsa.Equal(this.dropCodigoAtivo.Text.Trim()));
                    posicaoBolsaHistorico.Query.Load();

                    if (posicaoBolsaHistorico.DataHistorico.HasValue && posicaoBolsaHistorico.DataHistorico.Value < dataInicio)
                    {
                        dataInicio = posicaoBolsaHistorico.DataHistorico.Value;
                    }                    
                }

                if (dataInicio != clienteData.DataImplantacao.Value)
                {
                    dataInicio = Calendario.SubtraiDiaUtil(dataInicio, 1, (int)LocalFeriadoFixo.Bovespa, TipoFeriado.Brasil); //D-1 da data de inicio de saldo
                }

                DateTime dataFim = new DateTime();
                if (this.textDataFim1.Text != "")
                {
                    dataFim = Convert.ToDateTime(this.textDataFim1.Value);
                }
                else
                {
                    dataFim = clienteData.DataDia.Value;
                }

                if (dataFim > clienteData.DataDia.Value)
                {
                    dataFim = clienteData.DataDia.Value;
                }

                new ControleParticipacaoSocietariaMasa(idCliente,
                                              this.dropCodigoAtivo.Text.Trim(),
                                              dataInicio,
                                              dataFim)
                                .ExportaExcel(out msExcel, out nomeArquivoExcel);

                // Salva Memory Stream do Arquivo xls na Session
                Session["streamExcel"] = msExcel;
                Session["nomeArquivoExcel"] = nomeArquivoExcel;
            }
            catch (Exception erro) {
                e.Result = erro.Message;
                return;
            }

            #endregion
        }

        else if (e.Parameter == "exp4")
        {
            #region ControleParticipacaoSocietariaFundos

            #region Campos obrigatórios
            List<Control> controles = new List<Control>(new Control[] { this.dropCliente2, this.dropFundo });

            if (base.TestaObrigatorio(controles) != "")
            {
                e.Result = "Campos com * são obrigatórios!";
                return;
            }
            #endregion

            try
            {
                MemoryStream msExcel = new MemoryStream();
                string nomeArquivoExcel;

                //
                int idCliente = Convert.ToInt32(this.dropCliente2.Text.Trim());

                Cliente clienteData = new Cliente();
                clienteData.LoadByPrimaryKey(idCliente);

                DateTime dataInicio = new DateTime();
                if (this.textDataInicio2.Text != "")
                {
                    dataInicio = Convert.ToDateTime(this.textDataInicio2.Value);

                    if (dataInicio < clienteData.DataImplantacao.Value)
                    {
                        dataInicio = clienteData.DataImplantacao.Value;
                    }
                }
                else
                {
                    OperacaoFundo operacaoFundo = new OperacaoFundo();
                    operacaoFundo.Query.Select(operacaoFundo.Query.DataOperacao.Min());
                    operacaoFundo.Query.Where(operacaoFundo.Query.IdCliente.Equal(idCliente),
                                              operacaoFundo.Query.IdCarteira.Equal(Convert.ToInt32(this.dropFundo.Text.Trim())));
                    operacaoFundo.Query.Load();

                    if (operacaoFundo.DataOperacao.HasValue)
                    {
                        dataInicio = operacaoFundo.DataOperacao.Value;
                    }
                    else
                    {
                        dataInicio = clienteData.DataImplantacao.Value;
                    }
                }

                dataInicio = Calendario.SubtraiDiaUtil(dataInicio, 1);

                DateTime dataFim = new DateTime();
                if (this.textDataFim2.Text != "")
                {
                    dataFim = Convert.ToDateTime(this.textDataFim2.Value);
                }
                else
                {
                    dataFim = clienteData.DataDia.Value;
                }

                if (dataFim > clienteData.DataDia.Value)
                {
                    dataFim = clienteData.DataDia.Value;
                }

                new ControleParticipacaoSocietariaMasaFundos(idCliente,
                                              Convert.ToInt32(this.dropFundo.Text.Trim()),
                                              dataInicio,
                                              dataFim)
                                .ExportaExcel(out msExcel, out nomeArquivoExcel);

                // Salva Memory Stream do Arquivo xls na Session
                Session["streamExcel"] = msExcel;
                Session["nomeArquivoExcel"] = nomeArquivoExcel;
            }
            catch (Exception erro)
            {
                e.Result = erro.Message;
                return;
            }

            #endregion
        }
        else if (e.Parameter == "exp5") {

           #region Composição Carteira
            string msgErro = this.TrataErros();  
              if(!String.IsNullOrEmpty(msgErro)) {
                e.Result = msgErro;
                return;
              }

            try {

                MemoryStream msPDF = new MemoryStream();
                //                
                int idCarteira = Convert.ToInt32(this.btnEditCodigo.Text);
                DateTime dataRef = Convert.ToDateTime(this.textDataComposicaoCarteira.Text);

                ReportComposicaoCarteiraMasa report = new ReportComposicaoCarteiraMasa(idCarteira, dataRef);
                report.ExportToPdf(msPDF);

                // Salva Memory Stream do Arquivo pds na Session
                Session["streamPDF"] = msPDF;
                Session["idCarteira"] = idCarteira;
                Session["dataPDF"] = dataRef;
            }
            catch (Exception erro) {
                e.Result = erro.Message;
                return;
            }

            #endregion
        }
        //

        string redirect = "ExibeExportacaoCustom.aspx?Owner=" + (int)OwnerExportacao.Masa;
        switch (e.Parameter) {
            case "exp1":
                redirect += "&Tipo=" + (int)TipoExportacaoMasa.ParticipacaoSocietaria;
                break;
            case "exp2":
                redirect += "&Tipo=" + (int)TipoExportacaoMasa.EvolucaoCapitalSocial;
                break;
            case "exp3":
                redirect += "&Tipo=" + (int)TipoExportacaoMasa.ControleParticipacaoSocietariaAcoes;
                break;
            case "exp4":
                redirect += "&Tipo=" + (int)TipoExportacaoMasa.ControleParticipacaoSocietariaFundos;
                break;
            case "exp5":
                redirect += "&Tipo=" + (int)TipoExportacaoMasa.ComposicaoCarteira;
                break;
        }
                
        string newLocation = HttpContext.Current.Request.Url.AbsolutePath.Replace("Masa.aspx", redirect);
        e.Result = "redirect:" + newLocation;
    }

    #region 1 Exportacao

    protected void EsDSAtivoBolsa_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e) {
        AtivoBolsaCollection coll = new AtivoBolsaCollection();
        coll.Query.Where(coll.Query.TipoMercado.In(TipoMercadoBolsa.MercadoVista, TipoMercadoBolsa.Imobiliario));
        //
        coll.Query.OrderBy(coll.Query.Descricao.Ascending);
        coll.LoadAll();
        //
        e.Collection = coll;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void ASPxCallback1_Callback(object source, DevExpress.Web.CallbackEventArgs e) {
        string cdAtivoBolsa = "";
        string tipoativomasa = "";
        if (!String.IsNullOrEmpty(e.Parameter) && e.Parameter != "null") {
            AtivoBolsa ativoBolsa = new AtivoBolsa();
            ativoBolsa.LoadByPrimaryKey(Convert.ToString(e.Parameter));

            if (ativoBolsa.str.CdAtivoBolsa != "") {
                cdAtivoBolsa = ativoBolsa.str.CdAtivoBolsa;
            }
            TipoAtivoMasa tipoAtivoMasa = new TipoAtivoMasa();
            if (tipoAtivoMasa.LoadByPrimaryKey(Convert.ToString(e.Parameter)))
            {
                tipoativomasa = tipoAtivoMasa.str.Tipo;
            }
        }
        e.Result = cdAtivoBolsa + "|" + tipoativomasa;
    }

    #endregion

    #region 2 Exportação
    protected void EsDSEmissor_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e) {
        EmissorCollection coll = new EmissorCollection();

        coll.Query.OrderBy(coll.Query.Nome.Ascending);
        coll.LoadAll();
        
        e.Collection = coll;
    }

    protected void dropEmissorOnInit(object sender, EventArgs e) {
        (sender as ASPxGridLookup).GridView.Width = Unit.Point(300);
    }

    #endregion

    #region 3 Exportação
    protected void dropCodigoAtivoOnInit(object sender, EventArgs e) {
        (sender as ASPxGridLookup).GridView.Width = Unit.Point(200);
    }

    protected void EsDSCliente_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e) {
        ClienteCollection coll = new ClienteCollection();
        coll.BuscaClientesComAcesso(Context.User.Identity.Name);

        e.Collection = coll;
    }

    #endregion

    #region 4 Exportação
    protected void EsDSFundo_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        ClienteCollection coll = new ClienteCollection();
        coll.Query.Where(coll.Query.IdTipo.Equal((int)TipoClienteFixo.Fundo));
        coll.Query.Load();
        
        e.Collection = coll;
    }

    #endregion

    #region 5 Exportação

    protected void ASPxCallback2_Callback(object source, DevExpress.Web.CallbackEventArgs e) {
        try {
            string nome = "";
            if (!String.IsNullOrEmpty(e.Parameter) && e.Parameter != "null") {
                int idCliente = Convert.ToInt32(e.Parameter);
                Cliente cliente = new Cliente();
                cliente.LoadByPrimaryKey(idCliente);

                Carteira carteira = new Carteira();
                string cota = "Fech";
                if (carteira.LoadByPrimaryKey(idCliente)) {
                    byte tipoCota = carteira.TipoCota.Value;
                    cota = tipoCota == (byte)TipoCotaFundo.Abertura ? "Abert" : "Fech";
                }

                if (cliente.str.Apelido != "") {
                    if (cliente.IsAtivo) {
                        //O permissionamento da carteira é dado direto no cliente vinculado à carteira
                        PermissaoCliente permissaoCliente = new PermissaoCliente();
                        if (permissaoCliente.RetornaAcessoClienteComControle(idCliente, HttpContext.Current.User.Identity.Name)) {
                            DateTime dataDia = cliente.DataDia.Value;
                            nome = cliente.str.Apelido + "|" + dataDia.ToShortDateString() + "|" + cota + "|" + System.Threading.Thread.CurrentThread.CurrentCulture.Name;
                        }
                        else {
                            nome = "no_access";
                        }
                    }
                    else {
                        nome = "no_active";
                    }
                }
            }
            e.Result = nome;
        }
        catch (Exception exception) {
            new Financial.Web.Util.ErrorHandler().HandleArrayIndexError(exception);
        }
    }

    // <summary>
    /// Trata os Erros de Relatorio
    /// </summary>
    /// <returns>Mensagem de Erro ou branco se não tiver erro</returns>
    private string TrataErros() {
        string msgErro = "";

        #region Campos obrigatórios
        List<Control> controles = new List<Control>(new Control[] { 
    	    this.btnEditCodigo, this.textDataComposicaoCarteira });

        if (base.TestaObrigatorio(controles) != "") {
            return "Campos com * são obrigatórios!";
        }
        #endregion

        if (this.textDataComposicaoCarteira.Text != "" && this.btnEditCodigo.Text != "" && Utilitario.IsDate(this.textDataComposicaoCarteira.Text)) {
            DateTime dataReport = Convert.ToDateTime(this.textDataComposicaoCarteira.Text);

            Cliente cliente = new Cliente();
            cliente.LoadByPrimaryKey(Convert.ToInt32(this.btnEditCodigo.Text));
            
            if (!Calendario.IsDiaUtil(Convert.ToDateTime(this.textDataComposicaoCarteira.Text), LocalFeriadoFixo.Brasil, TipoFeriado.Brasil)) {
                return "Dia não útil";
            }
            
            DateTime dataImplantacao = cliente.DataImplantacao.Value;
            DateTime dataDia = cliente.DataDia.Value;

            if (dataImplantacao > dataReport) {
                return "Data passada anterior à data de implantação: " + dataImplantacao.ToString("d");
            }

            if (dataReport > dataDia) {
                return "Data passada posterior à data atual da carteira: " + dataDia.ToString("d");
            }

            // masa sempre é Relatório de Fechamento
            if (dataReport == dataDia) {
                if (cliente.Status == (byte)StatusCliente.Aberto ||
                    cliente.Status == (byte)StatusCliente.FechadoComErro) {
                    return "Para relatórios de fechamento, o status da carteira deve estar calculado ou fechado.";
                }
            }
        }

        return msgErro;
    }

    #endregion

    #endregion

    #region Importação

    #region Enum p/ Importacao
    #endregion

    #endregion
}