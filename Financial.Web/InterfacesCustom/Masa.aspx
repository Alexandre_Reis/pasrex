﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Masa.aspx.cs" Inherits="InterfacesCustom_Masa" %>

<%@ Register Assembly="System.Web, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a" Namespace="System.Web.UI.HtmlControls" TagPrefix="cc2" %>
<%@ Register Assembly="System.Web, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a" Namespace="System.Web.UI" TagPrefix="cc1" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxe" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxlp" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxcb" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxrp" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxp" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxwgv" %>

<%@ Register Assembly="EntitySpaces.Web" Namespace="EntitySpaces.Web" TagPrefix="cc1" %>



<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">

<link href="../css/forms.css" rel="stylesheet" type="text/css" />

<script type="text/javascript" language="Javascript" src="../js/global.js"></script>

<script type="text/javascript" language="Javascript">    
    var popup = true;
    document.onkeydown=onDocumentKeyDown;
    var operacao = '';
    
    function OnGetDataAtivoBolsa(data) {
        btnEditAtivoBolsa.SetValue(data);
        popupAtivoBolsa.HideWindow();
        ASPxCallback1.SendCallback(btnEditAtivoBolsa.GetValue());
        btnEditAtivoBolsa.Focus();
    }
    
    function OnGetDataCliente(values) {
        btnEditCodigo.SetValue(values);                    
        popupCliente.HideWindow();
        ASPxCallback2.SendCallback(btnEditCodigo.GetValue());        
        btnEditCodigo.Focus();    
    }                
</script>    

<script language="JavaScript" type="text/javascript">
    function btnExportaClick(tipoExportacao){              
       LoadingPanel1.Show();
       callbackExportaExcel.SendCallback(tipoExportacao);
    }    
</script>

</head>

<body>
    <form id="form1" runat="server">
               
    <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="true" EnableScriptLocalization="true"/>

    <dxcb:ASPxCallback ID="callbackExportaExcel" runat="server" OnCallback="callbackExportaExcel_Callback">
        <ClientSideEvents CallbackComplete="function(s, e) 
        {   
            LoadingPanel1.Hide();
            if(e && e.result.length){
                if(e.result.indexOf('redirect') === 0){
                    var newLocation = (e.result.split(':'))[1];
                    
                    var iframeId = 'iframe_download';
                    var iframe = document.getElementById(iframeId);

                    if (!iframe && document.createElement && (iframe = document.createElement('iframe'))) {
               
                        iframe.name = iframe.id = iframeId;
                        iframe.width = 0;
                        iframe.height = 0;
                        iframe.src = 'about:blank';
                        document.body.appendChild(iframe);                                                                    
                    }
                    iframe.src = newLocation;
                   
                } else {
                    alert(e.result);
                }
            }                    
        }" />
    </dxcb:ASPxCallback>

    <dxcb:ASPxCallback ID="ASPxCallback1" runat="server" OnCallback="ASPxCallback1_Callback">
        <ClientSideEvents CallbackComplete="function(s, e) {
            if (e.result == '' && btnEditAtivoBolsa.GetValue() != null)
            {
                popupMensagemAtivoBolsa.SetContentHTML(document.getElementById('textMsgNaoExiste').value);
                popupMensagemAtivoBolsa.ShowAtElementByID(btnEditAtivoBolsa.name);
                btnEditAtivoBolsa.SetValue(''); 
                btnEditAtivoBolsa.Focus();                 
            }
            else
            {
                var resultSplit = e.result.split('|');
                dropTipo.SetValue(resultSplit[1])
            }
        }        
        "/>
    </dxcb:ASPxCallback>

    <dxcb:ASPxCallback ID="ASPxCallback2" runat="server" OnCallback="ASPxCallback2_Callback">
        <ClientSideEvents CallbackComplete="function(s, e) {            
            var resultSplit = e.result.split('|');
                                    
            OnCallBackCompleteCliente(s, e, popupMensagemCliente, btnEditCodigo, document.getElementById('ASPxRoundPanel5_textNome'), textDataComposicaoCarteira);
            
            var data = LocalizedData(resultSplit[1], resultSplit[3]);
            textDataComposicaoCarteira.SetValue(data);
        }        
        "/>
    </dxcb:ASPxCallback>
    
<asp:Panel ID="panelEdicao" runat="server" OnLoad="panelEdicao_Load">
    
<div id="divExportacao" runat="server" visible="false">    
        
<div class="divPanel">
    <table width="100%"><tr><td>
    <div id="container_small">
                       
    <div id="header">
        <asp:Label ID="Label1" runat="server" Text="Exportação de Arquivo Excel (Masa)"/>
    </div>
        
    <div id="mainContentSpace">
    
    <asp:UpdatePanel ID="UpdatePanel3" runat="server">
    <ContentTemplate>
        
        <div class="reportFilter" style="margin-left:20px">
        
        <div style="height:20px"></div>                                  

<table>
<tr>
<td>

<dxrp:ASPxRoundPanel ID="ASPxRoundPanel2" runat="server" HeaderStyle-Font-Bold="true" HeaderText="Participação Societária" Width="400px" HeaderStyle-Font-Size="11px">
                                                        <PanelCollection>
                                                            <dxp:PanelContent runat="server">
                                                            
        <table cellpadding="2" cellspacing="2" border="0">                                                            
            <tr>            
            <td  class="td_Label">
                <asp:Label ID="labelEmissor1" runat="server" CssClass="labelRequired" Text="Emissor:"/>
            </td>
            <td colspan="3">
                <dx:ASPxGridLookup ID="dropEmissor1" ClientInstanceName="dropEmissor1" runat="server" KeyFieldName="IdEmissor" DataSourceID="EsDSEmissor"
                           Width="250px" TextFormatString="{0} - {1}" Font-Size="11px" AllowUserInput="false" OnInit="dropEmissorOnInit">
                 <Columns>
                     <dxwgv:GridViewCommandColumn ShowSelectCheckbox="True" Width="4%" />                        
                     <dxwgv:GridViewDataColumn FieldName="IdEmissor" Caption="Id" Width="7%" CellStyle-Font-Size="X-Small"/>                         
                     <dxwgv:GridViewDataColumn FieldName="Nome" CellStyle-Font-Size="X-Small" Width="90%" CellStyle-Wrap="false"/>                            
                </Columns>
                
                <GridViewProperties>
                    <Settings ShowFilterRow="True" />
                </GridViewProperties>
                
                </dx:ASPxGridLookup>
            
            </td>                      
            </tr>
            <tr>
            <td class="td_Label">                
                <asp:Label ID="labelData" runat="server" CssClass="labelRequired" Text="Data:"/>
            </td>                        
            
            <td>
                <dxe:ASPxDateEdit ID="textData" runat="server" ClientInstanceName="textData"/>
            </td>                                                                                                      
            
            <td> 
            
            <div id="reportLinkButton2" class="linkButton linkButtonNoBorder">
                <asp:LinkButton ID="btnExcel" runat="server" Font-Overline="false" CssClass="btnRun"
                    OnClientClick="btnExportaClick('exp1');return false;">
                    <asp:Literal ID="Literal1" runat="server" Text="Exportar"/><div></div>
                </asp:LinkButton>
            </div>            
            
            </td>
            
            <td><div style="width:170px"></div></td>
            
            </tr>
                                    
        </table> 
        
        
        
                                                          </dxp:PanelContent>
                                                        </PanelCollection>
                                                    </dxrp:ASPxRoundPanel>
</td>          

<td><div style="width:50px"></div></td>
<td>


<dxrp:ASPxRoundPanel ID="ASPxRoundPanel5" runat="server" HeaderStyle-Font-Bold="true" HeaderText="Composição Carteira" Width="400px" HeaderStyle-Font-Size="11px">
                                                        <PanelCollection>
                                                            <dxp:PanelContent runat="server">
                                                            
        <table cellpadding="2" cellspacing="2" border="0">                                                            
            <tr>            
            <td  class="td_Label">
                <asp:Label ID="label13" runat="server" CssClass="labelRequired" Text="Carteira:"/>
            </td>
             <td>
                <dxe:ASPxSpinEdit ID="btnEditCodigo" runat="server" CssClass="textButtonEdit" 
                                            EnableClientSideAPI="True" ClientInstanceName="btnEditCodigo"
                                            MaxLength="9" NumberType="Integer" AllowMouseWheel="false">  
                <Buttons>
                    <dxe:EditButton></dxe:EditButton>                                
                </Buttons>        
                <ClientSideEvents                        
                         KeyPress="function(s, e) {document.getElementById('textNome').value = '';}" 
                         ButtonClick="function(s, e) {popupCliente.ShowAtElementByID(s.name);}"                           
                         LostFocus="function(s, e) {OnLostFocus(popupMensagemCliente, ASPxCallback2, btnEditCodigo);}"
                />                            
                </dxe:ASPxSpinEdit>                
            </td>                           
            
            <td colspan="2" width="300">
                <asp:TextBox ID="textNome" runat="server" CssClass="textNome" Enabled="false"></asp:TextBox>
            </td>
            
            </tr>
            <tr>
            <td class="td_Label">                
                <asp:Label ID="label14" runat="server" CssClass="labelRequired" Text="Data:"/>
            </td>                        
            
            <td>
                <dxe:ASPxDateEdit ID="textDataComposicaoCarteira" runat="server" ClientInstanceName="textDataComposicaoCarteira"/>
            </td>                                                                                                      
            
            <td> 
            
            <div id="Div3" class="linkButton linkButtonNoBorder">
                <asp:LinkButton ID="LinkButton3" runat="server" Font-Overline="false" CssClass="btnPdf" Visible="true"
                    OnClientClick="btnExportaClick('exp5');return false;">
                    <asp:Literal ID="Literal5" runat="server" Text="Gerar PDF"/><div></div>
                </asp:LinkButton>
            </div>            
            
            </td>
            
            <td><div style="width:170px"></div></td>
            
            </tr>
                                    
        </table> 
        
        
        
                                                          </dxp:PanelContent>
                                                        </PanelCollection>
                                                    </dxrp:ASPxRoundPanel>
</td>



</tr>                        
</table>
                                                    
        </div>
                                                                        



<div class="reportFilter" style="margin-left:20px">
        
        <div style="height:20px"></div>                                  


<table>
<tr>
<td width="468px">

<dxrp:ASPxRoundPanel ID="ASPxRoundPanel1" runat="server" HeaderStyle-Font-Bold="true" HeaderText="Evolução Capital Social" Width="400px" HeaderStyle-Font-Size="11px">
                                                        <PanelCollection>
                                                            <dxp:PanelContent runat="server">
                                                            
        <table cellpadding="2" cellspacing="2" border="0">                                                            
            <tr>            
            <td  class="td_Label">
                <asp:Label ID="label2" runat="server" CssClass="labelRequired" Text="Emissor:"/>
            </td>
            <td colspan="4">
            
            <dx:ASPxGridLookup ID="dropEmissor" ClientInstanceName="dropEmissor" runat="server" KeyFieldName="IdEmissor" DataSourceID="EsDSEmissor"
                           Width="250px" TextFormatString="{0} - {1}" Font-Size="11px" AllowUserInput="false" OnInit="dropEmissorOnInit">
             <Columns>
                 <dxwgv:GridViewCommandColumn ShowSelectCheckbox="True" Width="4%" />                        
                 <dxwgv:GridViewDataColumn FieldName="IdEmissor" Caption="Id" Width="7%" CellStyle-Font-Size="X-Small"/>                         
                 <dxwgv:GridViewDataColumn FieldName="Nome" CellStyle-Font-Size="X-Small" Width="90%" CellStyle-Wrap="false"/>                            
            </Columns>
            
            <GridViewProperties>
                <Settings ShowFilterRow="True" />
            </GridViewProperties>
            
            </dx:ASPxGridLookup>

            </td>                      
            </tr>                       
            
            <tr>
            <td class="td_Label">                
                <asp:Label ID="label3" runat="server" CssClass="labelNormal" Text="Data Início:"/>
            </td>                        
            
            <td>
                <dxe:ASPxDateEdit ID="textDataInicio" runat="server" ClientInstanceName="textDataInicio"/>
            </td>                                                                                                      

            <td class="td_Label">                
                <asp:Label ID="label4" runat="server" CssClass="labelNormal" Text="Data Fim:"/>
            </td>                        
            
            <td>
                <dxe:ASPxDateEdit ID="textDataFim" runat="server" ClientInstanceName="textDataFim"/>
            </td>                                                                                                      
            
            <td> 
            
            <div id="reportLinkButton1" class="linkButton linkButtonNoBorder">
                <asp:LinkButton ID="btnExcel1" runat="server" Font-Overline="false" CssClass="btnRun"
                    OnClientClick="btnExportaClick('exp2');return false;">
                    <asp:Literal ID="Literal2" runat="server" Text="Exportar"/><div></div>
                </asp:LinkButton>
            </div>            
            
            </td>            
            </tr>
                                    
        </table> 
        
        
        
                                                          </dxp:PanelContent>
                                                        </PanelCollection>
                                                    </dxrp:ASPxRoundPanel>
                                                    
        </div>

</td>          

<td><div style="width:50px"></div></td>
<td>

<dxrp:ASPxRoundPanel ID="ASPxRoundPanelTipoAtivo" runat="server" HeaderStyle-Font-Bold="true" HeaderText="Tipo Ativo" Width="400px" HeaderStyle-Font-Size="11px">
                                                        <PanelCollection>
                                                            <dxp:PanelContent runat="server">
                                                            
        <table cellpadding="2" cellspacing="2" border="0">                                                            
            <tr>            
            <td  class="td_Label">
                <asp:Label ID="label15" runat="server" CssClass="labelRequired" Text="Ativo:"/>
            </td>
             <td>
                <dxe:ASPxButtonEdit ID="btnEditAtivoBolsa" runat="server" CssClass="textAtivoCurto" MaxLength="20" 
                                    ClientInstanceName="btnEditAtivoBolsa">            
                <Buttons>
                    <dxe:EditButton></dxe:EditButton>                
                </Buttons>           
                <ClientSideEvents                              
                     ButtonClick="function(s, e) {popupAtivoBolsa.ShowAtElementByID(s.name);}" 
                     LostFocus="function(s, e) {popupMensagemAtivoBolsa.HideWindow();
                                                if (btnEditAtivoBolsa.GetValue() != null)
                                                {
                                                    ASPxCallback1.SendCallback(btnEditAtivoBolsa.GetValue());
                                                }
                                                }"
                />                    
                </dxe:ASPxButtonEdit>                
            </td>                           
            
            <td colspan="2" width="300">
                
            </td>
            
            </tr>
            <tr>
            <td class="td_Label">                
                <asp:Label ID="label16" runat="server" CssClass="labelRequired" Text="Tipo:"/>
            </td>                        
            
            <td>
                <dxe:ASPxComboBox ID="dropTipo" runat="server" ClientInstanceName="dropTipo" ShowShadow="false"
                    DropDownStyle="DropDownList" CssClass="dropDownListCurto" >
                    <Items>
                        <dxe:ListEditItem Value="1" Text="Capital Aberto" />
                        <dxe:ListEditItem Value="2" Text="Capital Fechado" />
                        <dxe:ListEditItem Value="3" Text="Do grupo MASA" />
                    </Items>
                </dxe:ASPxComboBox>
            </td>                                                                                                      
            
            <td> 
            
            <div id="Div4" class="linkButton linkButtonNoBorder">
                <asp:LinkButton ID="btnSalvar" runat="server" Font-Overline="false" CssClass="btnOK"
                OnClick="btnSalvar_Click"><asp:Literal ID="Literal6" runat="server" Text="Salvar" /><div></div></asp:LinkButton>
            </div>            
            
            </td>
            
            <td><div style="width:170px"></div></td>
            
            </tr>
                                    
        </table> 
            

      </dxp:PanelContent>
    </PanelCollection>
</dxrp:ASPxRoundPanel>

</td>
</tr>                        
</table>


<div class="reportFilter" style="margin-left:20px">
        
        <div style="height:20px"></div>                                  


<dxrp:ASPxRoundPanel ID="ASPxRoundPanel3" runat="server" HeaderStyle-Font-Bold="true" HeaderText="Controle Participação Societária (Ações)" Width="400px" HeaderStyle-Font-Size="11px">
                                                        <PanelCollection>
                                                            <dxp:PanelContent runat="server">
                                                            
        <table cellpadding="2" cellspacing="2" border="0">                                                            
            <tr>
                <td class="td_Label">
                    <asp:Label ID="label8" runat="server" CssClass="labelRequired" Text="Cliente:"/>
                </td>
                <td colspan="3">
                    <dx:ASPxGridLookup ID="dropCliente" ClientInstanceName="dropCliente" runat="server" KeyFieldName="IdCliente" DataSourceID="EsDSCliente"
                                   Width="250px" TextFormatString="{0}" Font-Size="11px" AllowUserInput="false" OnInit="dropCodigoAtivoOnInit">
                     <Columns>
                         <dxwgv:GridViewCommandColumn ShowSelectCheckbox="True" Width="4%" />                        
                         <dxwgv:GridViewDataColumn FieldName="IdCliente" Caption="Id" Width="7%" CellStyle-Font-Size="X-Small"/>                         
                         <dxwgv:GridViewDataColumn FieldName="Apelido" CellStyle-Font-Size="X-Small" Width="90%" CellStyle-Wrap="false" />                            
                    </Columns>
                    
                    <GridViewProperties>
                        <Settings ShowFilterRow="True" />
                    </GridViewProperties>
                    
                </dx:ASPxGridLookup>
                </td>
            </tr>
            <tr>            
            <td  class="td_Label">
                <asp:Label ID="label5" runat="server" CssClass="labelRequired" Text="Ativo:"/>
            </td>
            <td colspan="4">
                        
            <dx:ASPxGridLookup ID="dropCodigoAtivo" ClientInstanceName="dropCodigoAtivo" runat="server" KeyFieldName="CdAtivoBolsa" DataSourceID="EsDSAtivoBolsa"
                               Width="100px" TextFormatString="{0}" Font-Size="11px"  AllowUserInput="false" OnInit="dropCodigoAtivoOnInit" >
                 <Columns>
                     <dxwgv:GridViewCommandColumn ShowSelectCheckbox="True" Width="4%" />                        
                     <dxwgv:GridViewDataColumn FieldName="CdAtivoBolsa" Width="10%" CellStyle-Font-Size="X-Small"/>                         
                     <dxwgv:GridViewDataColumn FieldName="Descricao" CellStyle-Font-Size="X-Small" Width="25%"/>
                     <dxwgv:GridViewDataColumn FieldName="TipoMercado" CellStyle-Font-Size="X-Small" Width="10%"/>
                </Columns>
                
                <GridViewProperties>
                    <Settings ShowFilterRow="True" />
                </GridViewProperties>
                
            </dx:ASPxGridLookup>

            </td>                      
            </tr>  
            
            <tr>
            <td class="td_Label">                
                <asp:Label ID="label6" runat="server" CssClass="labelNormal" Text="Data Início:"/>
            </td>                        
            
            <td>
                <dxe:ASPxDateEdit ID="textDataInicio1" runat="server" ClientInstanceName="textDataInicio1"/>
            </td>                                                                                                      

            <td class="td_Label">                
                <asp:Label ID="label7" runat="server" CssClass="labelNormal" Text="Data Fim:"/>
            </td>                        
            
            <td>
                <dxe:ASPxDateEdit ID="textDataFim1" runat="server" ClientInstanceName="textDataFim1"/>
            </td>                                                                                                      
            
            <td> 
            
            <div id="Div1" class="linkButton linkButtonNoBorder">
                <asp:LinkButton ID="LinkButton1" runat="server" Font-Overline="false" CssClass="btnRun"
                    OnClientClick="btnExportaClick('exp3');return false;">
                    <asp:Literal ID="Literal3" runat="server" Text="Exportar"/><div></div>
                </asp:LinkButton>
            </div>            
            
            </td>
            
            </tr>
                                    
        </table> 
        
        
        
                                                          </dxp:PanelContent>
                                                        </PanelCollection>
                                                    </dxrp:ASPxRoundPanel>
                                                    
        </div>
 
 
 
 
 
 
 
 
 
 
 
 
 <div class="reportFilter" style="margin-left:20px">
        
        <div style="height:20px"></div>                                  


<dxrp:ASPxRoundPanel ID="ASPxRoundPanel4" runat="server" HeaderStyle-Font-Bold="true" HeaderText="Controle Participação Societária (Fundos)" Width="400px" HeaderStyle-Font-Size="11px">
                                                        <PanelCollection>
                                                            <dxp:PanelContent runat="server">
                                                            
        <table cellpadding="2" cellspacing="2" border="0">                                                            
            <tr>
                <td class="td_Label">
                    <asp:Label ID="label9" runat="server" CssClass="labelRequired" Text="Cliente:"/>
                </td>
                <td colspan="3">
                    <dx:ASPxGridLookup ID="dropCliente2" ClientInstanceName="dropCliente2" runat="server" KeyFieldName="IdCliente" DataSourceID="EsDSCliente"
                                   Width="250px" TextFormatString="{0}" Font-Size="11px" AllowUserInput="false" >
                     <Columns>
                         <dxwgv:GridViewCommandColumn ShowSelectCheckbox="True" Width="4%" />                        
                         <dxwgv:GridViewDataColumn FieldName="IdCliente" Caption="Id" Width="7%" CellStyle-Font-Size="X-Small"/>                         
                         <dxwgv:GridViewDataColumn FieldName="Apelido" CellStyle-Font-Size="X-Small" Width="90%" CellStyle-Wrap="false" />                            
                    </Columns>
                    
                    <GridViewProperties>
                        <Settings ShowFilterRow="True" />
                    </GridViewProperties>
                    
                </dx:ASPxGridLookup>
                </td>
            </tr>
            <tr>            
            <td  class="td_Label">
                <asp:Label ID="label10" runat="server" CssClass="labelRequired" Text="Fundo:"/>
            </td>
            <td colspan="4">
                        
            <dx:ASPxGridLookup ID="dropFundo" ClientInstanceName="dropFundo" runat="server" KeyFieldName="IdCliente" DataSourceID="EsDSFundo"
                                   Width="250px" TextFormatString="{0}" Font-Size="11px" AllowUserInput="false" >
                     <Columns>
                         <dxwgv:GridViewCommandColumn ShowSelectCheckbox="True" Width="4%" />                        
                         <dxwgv:GridViewDataColumn FieldName="IdCliente" Caption="Id" Width="7%" CellStyle-Font-Size="X-Small"/>                         
                         <dxwgv:GridViewDataColumn FieldName="Apelido" CellStyle-Font-Size="X-Small" Width="90%" CellStyle-Wrap="false" />                            
                    </Columns>
                    
                    <GridViewProperties>
                        <Settings ShowFilterRow="True" />
                    </GridViewProperties>
                    
                </dx:ASPxGridLookup>

            </td>                      
            </tr>  
            
            <tr>
            <td class="td_Label">                
                <asp:Label ID="label11" runat="server" CssClass="labelNormal" Text="Data Início:"/>
            </td>                        
            
            <td>
                <dxe:ASPxDateEdit ID="textDataInicio2" runat="server" ClientInstanceName="textDataInicio2"/>
            </td>                                                                                                      

            <td class="td_Label">                
                <asp:Label ID="label12" runat="server" CssClass="labelNormal" Text="Data Fim:"/>
            </td>                        
            
            <td>
                <dxe:ASPxDateEdit ID="textDataFim2" runat="server" ClientInstanceName="textDataFim2"/>
            </td>                                                                                                      
            
            <td> 
            
            <div id="Div2" class="linkButton linkButtonNoBorder">
                <asp:LinkButton ID="LinkButton2" runat="server" Font-Overline="false" CssClass="btnRun"
                    OnClientClick="btnExportaClick('exp4');return false;">
                    <asp:Literal ID="Literal4" runat="server" Text="Exportar"/><div></div>
                </asp:LinkButton>
            </div>            
            
            </td>
            
            </tr>
                                    
        </table> 
        
        
        
                                                          </dxp:PanelContent>
                                                        </PanelCollection>
                                                    </dxrp:ASPxRoundPanel>
                                                    
        </div>
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 


           
        </div>
        
    </ContentTemplate>
    </asp:UpdatePanel>
    
    </div>
    
    </div>
    </td></tr></table>
</div>
</div>

<div id="divImportacao" runat="server" visible="false">
        
        <div class="divPanel divPanelNew">                
        <div style="background-image: none !important;">
            <div style="white-space: nowrap;
	                    text-align: center;
	                    font: bold 11px Tahoma, Arial;
	                    color: #333333;
	                    background-image: url(  '../imagens/layout/nbHeaderBack.gif' );
	                    background-repeat: repeat-x;
	                    background-position: top;
	                    border-left-style: none;
	                    border-right-style: none;
	                    border-top-style: none;
	                    border-bottom: solid 1px #D5D5D5;
	                    padding: 3px 10px 3px 5px;">	                    
                <asp:Label ID="lblHeader1" runat="server" Text="Importação (Masa)"/>
            </div>
            <div style="width: 720px; margin: 0 auto;">
                <div class="reportFilter">
                    
                    <table border="0">         
                        <tr>
                            <td>
                             <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                                <ContentTemplate>
                                    <table>
                                        <tr>
                                        </tr>
                                    </table>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                            </td>

                            <td>
                            </td>
                        </tr>
                     </table>
                </div>
            </div>
        </div>
        </div>
</div>
        
</asp:Panel>        
                
    <dxlp:ASPxLoadingPanel ID="LoadingPanel1" runat="server" Text="Exportando arquivo, aguarde..." ClientInstanceName="LoadingPanel1" Modal="true" />
    
    <cc1:esDataSource ID="EsDSAtivoBolsa" runat="server" OnesSelect="EsDSAtivoBolsa_esSelect" />
    <cc1:esDataSource ID="EsDSEmissor" runat="server" OnesSelect="EsDSEmissor_esSelect" />
    <cc1:esDataSource ID="EsDSCliente" runat="server" OnesSelect="EsDSCliente_esSelect" />
    <cc1:esDataSource ID="EsDSFundo" runat="server" OnesSelect="EsDSFundo_esSelect" />
    
    </form>
    </body>
</html>