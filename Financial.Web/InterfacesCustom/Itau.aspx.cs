﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.IO;
using System.Globalization;
using Financial.Export;
using System.Text;
using Bytescout.Spreadsheet;
using System.Collections.Generic;
using System.Drawing;
using Financial.Investidor;
using Financial.Security;
using Financial.Web.Common;
using EntitySpaces.Interfaces;
using Financial.Security.Enums;
using Financial.Bolsa;
using Financial.Fundo;
using Financial.RendaFixa;
using Financial.Util;
using Financial.Fundo.Enums;
using Financial.RendaFixa.Enums;
using Financial.Tributo.Custom;
using Financial.Common;
using Financial.Common.Enums;
using Financial.Investidor.Enums;
using Financial.WebConfigConfiguration;
using Financial.Relatorio;

public partial class InterfacesCustom_Itau : FiltroReportBasePage
{
    // Controla Exibição de Importação ou Exportação
    private Tipo tipo;

    enum Tipo
    {
        Importacao = 0,
        Exportacao = 1
    }

    enum OwnerExportacao
    {
        Itau = 5
    }

    enum TipoExportacaoItau
    {
        OperacaoRF = 0,
        PosicaoRF = 1
    }

    /// <summary>
    /// Controla Exibição de Importação ou Exportação
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    new protected void panelEdicao_Load(object sender, EventArgs e)
    {
        Control c1 = this.FindControl("divExportacao");
        Control c2 = this.FindControl("divImportacao");

        if (this.tipo == Tipo.Exportacao)
        {
            if (c1 != null) { c1.Visible = true; }
            if (c2 != null) { c2.Visible = false; }
        }
        else if (this.tipo == Tipo.Importacao)
        {
            if (c1 != null) { c1.Visible = false; }
            if (c2 != null) { c2.Visible = true; }
        }
    }
    
    new protected void Page_Load(object sender, EventArgs e)
    {
        //if (!Page.IsPostBack) {
        //    DateTime hoje = DateTime.Now;
        //    this.textData.Value = new DateTime(hoje.Year, hoje.Month, hoje.Day);
        //}
        this.tipo = (Tipo)Enum.Parse(typeof(Tipo), Request.QueryString["param"].ToString());

        if (this.tipo == Tipo.Exportacao)
        {

            base.Page_Load(sender, e);
        }
    }

    protected void callbackExportaExcel_Callback(object source, DevExpress.Web.CallbackEventArgs e)
    {
        Response.BufferOutput = true;
                
        if (e.Parameter == "OperacaoRF")
        {
            #region Campos obrigatórios
            List<Control> controles = new List<Control>(new Control[] { this.textDataReferencia1 });

            if (base.TestaObrigatorio(controles) != "")
            {
                e.Result = "Campos com * são obrigatórios!";
                return;
            }
            #endregion

            try
            {
                DateTime dataReferencia = Convert.ToDateTime(textDataReferencia1.Value);
                                
                MemoryStream msPlanilhaOperacaoRFExcel = new MemoryStream();

                RendaFixaItau rendaFixaItau = new RendaFixaItau();

                if (rendaFixaItau.ExportaOperacaoRF(dataReferencia, out msPlanilhaOperacaoRFExcel))
                {
                    Session["msPlanilhaOperacaoRFExcel"] = msPlanilhaOperacaoRFExcel;
                }                
            }
            catch (Exception erro)
            {
                e.Result = erro.Message;
                return;
            }
        }

        if (e.Parameter == "PosicaoRF")
        {
            #region Campos obrigatórios
            List<Control> controles = new List<Control>(new Control[] { this.textDataReferencia2 });

            if (base.TestaObrigatorio(controles) != "")
            {
                e.Result = "Campos com * são obrigatórios!";
                return;
            }
            #endregion

            try
            {
                DateTime dataReferencia = Convert.ToDateTime(textDataReferencia2.Value);

                MemoryStream msPlanilhaPosicaoRFExcel = new MemoryStream();

                RendaFixaItau rendaFixaItau = new RendaFixaItau();

                if (rendaFixaItau.ExportaPosicaoRF(dataReferencia, out msPlanilhaPosicaoRFExcel))
                {
                    Session["msPlanilhaPosicaoRFExcel"] = msPlanilhaPosicaoRFExcel;
                }                
            }
            catch (Exception erro)
            {
                e.Result = erro.Message;
                return;
            }
        }
        
        string redirect = "ExibeExportacaoCustom.aspx?Owner=" + (int)OwnerExportacao.Itau;
        switch (e.Parameter)
        {
            case "OperacaoRF":
                redirect += "&Tipo=" + (int)TipoExportacaoItau.OperacaoRF;
                break;
            case "PosicaoRF":
                redirect += "&Tipo=" + (int)TipoExportacaoItau.PosicaoRF;
                break;
        }

        string newLocation = HttpContext.Current.Request.Url.AbsolutePath.Replace("Itau.aspx", redirect);
        e.Result = "redirect:" + newLocation;

    }

}