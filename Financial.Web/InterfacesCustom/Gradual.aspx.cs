﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.IO;
using System.Globalization;
using Financial.Export;
using System.Text;

public partial class InterfacesCustom_Gradual : System.Web.UI.Page {
    // Controla Exibição de Importação ou Exportação
    private Tipo tipo;
        
    enum Tipo {
        Importacao = 0,
        Exportacao = 1
    }
    /*-----------------------------------------*/

    #region Enum p/ Exportacao
    enum OwnerExportacao {
        Gradual = 0
    }

    enum TipoExportacao {
        Cota = 0,
        Posicao = 1
    }
    #endregion

    #region Enum p/ Importacao
    #endregion

    /// <summary>
    /// Controla Exibição de Importação ou Exportação
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    new protected void panelEdicao_Load(object sender, EventArgs e) {
        Control c1 = this.FindControl("divExportacao");
        Control c2 = this.FindControl("divImportacao");

        if (this.tipo == Tipo.Exportacao) {
            if (c1 != null) { c1.Visible = true; }
            if (c2 != null) { c2.Visible = false; }
        }
        else if (this.tipo == Tipo.Importacao) {
            if (c1 != null) { c1.Visible = false; }
            if (c2 != null) { c2.Visible = true; }
        }
    }

    protected void Page_Load(object sender, EventArgs e) {
        this.tipo = (Tipo)Enum.Parse(typeof(Tipo), Request.QueryString["param"].ToString());
        
        if (!Page.IsPostBack) {
            DateTime hoje = DateTime.Now;
            this.textDataCota.Value = new DateTime(hoje.Year, hoje.Month, hoje.Day);
            this.textDataPosicao.Value = new DateTime(hoje.Year, hoje.Month, hoje.Day);
        }
    }

    #region Exportação

    /// <summary>
    /// Redireciona para ExibeExportacaoCustom para exportar o arquivo de Cotas
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnCota_Click(object sender, EventArgs e) {
        if (this.textDataCota.Text == "") {
            throw new Exception("Data da cota obrigatória");
        }

        MemoryStream msCotas = new MemoryStream();
        string nomeArquivoCotas;
        //                
        CotaGradual cotaGradual = new CotaGradual();
        cotaGradual.ExportaCota(Convert.ToDateTime(this.textDataCota.Text),
                                                   out msCotas, out nomeArquivoCotas);

        // Salva Memory Stream do Arquivo txt na Session
        Session["streamCotas"] = msCotas;
        Session["nomeArquivoCotas"] = nomeArquivoCotas;
        //
        Session["textData"] = this.textDataCota.Text;

        string redirect = "~/InterfacesCustom/ExibeExportacaoCustom.aspx?Owner=" + (int)OwnerExportacao.Gradual;
        redirect += "&Tipo=" + (int)TipoExportacao.Cota;
                
        Response.Redirect(redirect, true);
    }

    /// <summary>
    /// Redireciona para ExibeExportacaoCustom para exportar o arquivo de Posição
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnPosicao_Click(object sender, EventArgs e) {
        if (this.textDataPosicao.Text == "") {
            throw new Exception("Data da posição obrigatória");
        }

        MemoryStream msPosicao = new MemoryStream();
        string nomeArquivoPosicao;
        //                
        PosicaoCotistaGradual posicaoCotistaGradual = new PosicaoCotistaGradual();
        posicaoCotistaGradual.ExportaPosicaoCotista(Convert.ToDateTime(this.textDataPosicao.Text),
                                                        out msPosicao, out nomeArquivoPosicao);

        // Salva Memory Stream do Arquivo txt na Session
        Session["streamPosicaoCotista"] = msPosicao;
        Session["nomeArquivoPosicao"] = nomeArquivoPosicao;
        //
        Session["textData"] = this.textDataPosicao.Text;

        string redirect = "~/InterfacesCustom/ExibeExportacaoCustom.aspx?Owner=" + (int)OwnerExportacao.Gradual;
        redirect += "&Tipo=" + (int)TipoExportacao.Posicao;

        Response.Redirect(redirect, true);
    }

    #endregion

    #region Importação
    #endregion
}