﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Paulista.aspx.cs" Inherits="InterfacesCustom_Paulista" %>

<%@ Register Assembly="System.Web, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a" Namespace="System.Web.UI.HtmlControls" TagPrefix="cc2" %>    
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxcb" %>
<%@ Register Assembly="System.Web, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a" Namespace="System.Web.UI" TagPrefix="cc1" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxnb" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxe" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxwgv" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxpc" %>
<%@ Register Assembly="EntitySpaces.Web" Namespace="EntitySpaces.Web" TagPrefix="cc1" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxlp" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxp" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxuc" %>



<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxtc" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" tagprefix="dxw" %>    


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">

<link href="~/css/forms.css" rel="stylesheet" type="text/css" />

    <script type="text/javascript" language="Javascript" src="../js/global.js"></script>
    <script type="text/javascript" language="Javascript" src="../js/ext-core.js"></script>
    
    <script type="text/javascript" language="javascript">
                
        function Uploader_OnUploadComplete(args) {                
            var msg = '';
            
            if (args.callbackData != '') { 
                msg = args.callbackData; 
            }
            
            LoadingPanel.Hide();
            
            if (msg == '') {
                alert('Arquivo(s) importado(s) com sucesso.');
            }
            else {               
                // Se Ocorreu erro em alguma importação Concatena mensagem Concluida
                msg += '\n -------------------------------------------------------------------------';
                msg += '\n\t\t\t\tImportação concluída';
                alert(msg);
            }    
        }                
    </script>    

</head>

<body>
    <form id="form1" runat="server">
                     
    <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="true" EnableScriptLocalization="true"/>

    <dxcb:ASPxCallback ID="callbackExcelOperacaoCompromissada" runat="server" OnCallback="callbackExcelOperacaoCompromissada_Callback">
        <ClientSideEvents CallbackComplete="function(s, e) {   
            if (e.result != '') {
                alert(e.result);                              
            }
            else {
                LoadingPanel.Show();

                uplOperacaoCompromissada.UploadFile();
            }
        }
        " />
    </dxcb:ASPxCallback>
    
    <dxcb:ASPxCallback ID="callbackExcelLiquidacaoPaulista" runat="server" OnCallback="callbackExcelLiquidacaoPaulista_Callback">
        <ClientSideEvents CallbackComplete="function(s, e) {   
            if (e.result != '') {
                alert(e.result);                              
            }
            else {
                LoadingPanel.Show();

                uplLiquidacaoPaulista.UploadFile();
            }
        }
        " />
    </dxcb:ASPxCallback>
    
    <dxcb:ASPxCallback ID="callbackOperacaoFundoYMFPaulista" runat="server" OnCallback="callbackOperacaoFundoYMFPaulista_Callback">
        <ClientSideEvents CallbackComplete="function(s, e) {   
            if (e.result != '') {
                alert(e.result);                              
            }
            else {
                LoadingPanel.Show();

                uplOperacaoFundoYMFPaulista.UploadFile();
            }
        }
        " />
    </dxcb:ASPxCallback>
    
    <dxcb:ASPxCallback ID="callbackHistoricoCotaYMFPaulista" runat="server" OnCallback="callbackHistoricoCotaYMFPaulista_Callback">
        <ClientSideEvents CallbackComplete="function(s, e) {   
            alert(e.result);
        }
        " />
    </dxcb:ASPxCallback>

<asp:Panel ID="panelEdicao" runat="server" OnLoad="panelEdicao_Load">
    
<div id="divExportacao" runat="server" visible="false">    
        <div>
        <div style="        
        font: 11px Tahoma, Arial;
	    color: black;
	    padding: 1px 1px 1px 1px;
	    background-color: #f5f5f5;	
	    border: Solid 1px #7EACB1;"                
        >
            <div id="header">
                <center><asp:Label ID="lblHeader" runat="server" Text="Exportação (Paulista)"/></center>
            </div>
            <div id="mainContent">
                <div class="reportFilter">
                    <div class="dataMessage">
                    </div>    
                    <table border="0">         
                        <tr>
                            <td>                                                      
                             
                             <asp:UpdatePanel ID="UpdatePanel1" runat="server"> 
                                <ContentTemplate>                                                                                                            
                                    <table>
                                        <tr>                                            
                                        </tr>
                                    </table>                                                           
                                </ContentTemplate>
                            </asp:UpdatePanel>         
                            </td>
                            
                            <td>
                            </td>
                        </tr>         
                     </table>    
                </div>
            </div>
        </div>
        </div>        
</div>        

<div id="divImportacao" runat="server" visible="false">
                
    <div class="divPanel divPanelNew">
    <table width="100%"><tr><td>
    <div id="container_small">
    
    <div style="white-space: nowrap;
                text-align: center;
                font: bold 11px Tahoma, Arial;
                color: #333333;
                background-image: url(  '../imagens/layout/nbHeaderBack.gif' );
                background-repeat: repeat-x;
                background-position: top;
                border-left-style: none;
                border-right-style: none;
                border-top-style: none;
                border-bottom: solid 1px #D5D5D5;
                padding: 3px 10px 3px 5px;">
                <asp:Label ID="label1" runat="server" Text="Importação (Paulista)" />
    </div>
        
    <div id="mainContentSpace">
    
    <asp:UpdatePanel ID="UpdatePanel3" runat="server">
    <ContentTemplate>
        
        <div class="reportFilter">
        
        <div style="height:20px"></div>                                  
                                        
        <table cellpadding="2" cellspacing="2" style="margin-left:20px" border="0">
            <tr>
            <td>
            
             <asp:Label ID="label2" runat="server" CssClass="labelNormal labelFloat" Font-Bold="true" Text="Operação Compromissada"/>
            <dxuc:ASPxUploadControl ID="uplOperacaoCompromissada" runat="server" ClientInstanceName="uplOperacaoCompromissada"
                    OnFileUploadComplete="uplOperacaoCompromissada_FileUploadComplete" 			                                            
                    CssClass="labelNormal" FileUploadMode="OnPageLoad">
                 <ValidationSettings MaxFileSize="100000000" />
                 <ClientSideEvents FileUploadComplete="function(s, e) { Uploader_OnUploadComplete(e); }" />
            </dxuc:ASPxUploadControl>
            
            </td>
                                                            
            <td>
                <div class="linkButton linkButtonNoBorder" style="margin-left: 5px; margin-top: 20px">
                    <table><tr><td>
                    <asp:LinkButton ID="LinkButton1" runat="server" Font-Overline="false" CssClass="btnRun"
                        OnClientClick="
                                {                                                                
                                    if(uplOperacaoCompromissada.GetText()== '' ) {
                                        alert('Escolha algum arquivo para importação!');
                                    }
                                    else {
                                        callbackExcelOperacaoCompromissada.SendCallback();                                                           
                                    }
                                    return false;
                                }
                                "><asp:Literal ID="Literal1" runat="server" Text="Importar"/><div></div></asp:LinkButton>
                                   </td>
                                   <td width="80px">                                                                                                                                                                                                         
                     </td></tr></table>
                </div>
            </td>
            </tr>
            
            <tr>
            <td>
            
             <asp:Label ID="label3" runat="server" CssClass="labelNormal labelFloat" Font-Bold="true" Text="Liquidação"/>
            <dxuc:ASPxUploadControl ID="uplLiquidacaoPaulista" runat="server" ClientInstanceName="uplLiquidacaoPaulista"
                    OnFileUploadComplete="uplLiquidacaoPaulista_FileUploadComplete"
                    CssClass="labelNormal" FileUploadMode="OnPageLoad">
                 <ValidationSettings MaxFileSize="100000000" />
                 <ClientSideEvents FileUploadComplete="function(s, e) { Uploader_OnUploadComplete(e); }" />
            </dxuc:ASPxUploadControl>
            
            </td>
                                                            
            <td>
                <div class="linkButton linkButtonNoBorder" style="margin-left: 5px; margin-top: 20px">
                    <table><tr><td>
                    <asp:LinkButton ID="LinkButton2" runat="server" Font-Overline="false" CssClass="btnRun"
                        OnClientClick="
                                {                                                                
                                    if(uplLiquidacaoPaulista.GetText()== '' ) {
                                        alert('Escolha algum arquivo para importação!');
                                    }
                                    else {
                                        callbackExcelLiquidacaoPaulista.SendCallback();                                                           
                                    }
                                    return false;
                                }
                                "><asp:Literal ID="Literal2" runat="server" Text="Importar"/><div></div></asp:LinkButton>
                                   </td>
                                   <td width="80px">                                                                                                                                                                                                         
                     </td></tr></table>
                </div>
            </td>
            </tr>
            
            <tr>
            <td>
            
             <asp:Label ID="label4" runat="server" CssClass="labelNormal labelFloat" Font-Bold="true" Text="Oper. Fundo YMF"/>
            <dxuc:ASPxUploadControl ID="uplOperacaoFundoYMFPaulista" runat="server" ClientInstanceName="uplOperacaoFundoYMFPaulista"
                    OnFileUploadComplete="uplOperacaoFundoYMFPaulista_FileUploadComplete"
                    CssClass="labelNormal" FileUploadMode="OnPageLoad">
                 <ValidationSettings MaxFileSize="100000000" />
                 <ClientSideEvents FileUploadComplete="function(s, e) { Uploader_OnUploadComplete(e); }" />
            </dxuc:ASPxUploadControl>
            
            </td>
                                                            
            <td>
                <div class="linkButton linkButtonNoBorder" style="margin-left: 5px; margin-top: 20px">
                    <table><tr><td>
                    <asp:LinkButton ID="LinkButton3" runat="server" Font-Overline="false" CssClass="btnRun"
                        OnClientClick="
                                {                                                                
                                    if(uplOperacaoFundoYMFPaulista.GetText()== '' ) {
                                        alert('Escolha algum arquivo para importação!');
                                    }
                                    else {
                                        callbackOperacaoFundoYMFPaulista.SendCallback();                                                           
                                    }
                                    return false;
                                }
                                "><asp:Literal ID="Literal3" runat="server" Text="Importar"/><div></div></asp:LinkButton>
                                   </td>
                                   <td width="80px">                                                                                                                                                                                                         
                     </td></tr></table>
                </div>
            </td>
            </tr>
            
            <tr>
            <td>
            
             <asp:Label ID="label5" runat="server" CssClass="labelNormal labelFloat" Font-Bold="true" Text="Histórico Cota YMF"/>
            
            </td>
                                                            
            <td>
                <div class="linkButton linkButtonNoBorder" style="margin-left: 5px; margin-top: 20px">
                    <table><tr><td>
                    <asp:LinkButton ID="LinkButton4" runat="server" Font-Overline="false" CssClass="btnRun"
                        OnClientClick="callbackHistoricoCotaYMFPaulista.SendCallback();                                                           
                                     return false;
                                "><asp:Literal ID="Literal4" runat="server" Text="Importar"/><div></div></asp:LinkButton>
                                   </td>
                                   <td width="80px">                                                                                                                                                                                                         
                     </td></tr></table>
                </div>
            </td>
            </tr>
            
            <%--<tr>
            <td>
            
            <asp:Label ID="label3" runat="server" CssClass="labelNormal labelFloat" Font-Bold="true" Text="Planilha2"/>
            <dxuc:ASPxUploadControl ID="uplPlan2" runat="server" ClientInstanceName="uplPlan2" 
                    CssClass="labelNormal" FileUploadMode="OnPageLoad">
                 <ValidationSettings MaxFileSize="100000000" />
                 <ClientSideEvents FileUploadComplete="function(s, e) { Uploader_OnUploadComplete(e); }" />
            </dxuc:ASPxUploadControl>
            
            </td>
                                                            
            <td>
                <div class="linkButton linkButtonNoBorder" style="margin-left: 5px; margin-top: 20px">
                    <table><tr><td>
                    <asp:LinkButton ID="LinkButton2" runat="server" Font-Overline="false" CssClass="btnRun"
                        OnClientClick="
                                {
                                    if(!ChecaExcel()) {
                                        alert('Escolha algum arquivo para importação!');                                                                            
                                    }
                                    else {
                                        callbackExcel.SendCallback();                                                                             
                                    }
                                    return false;
                                }
                                "><asp:Literal ID="Literal2" runat="server" Text="Importar"/><div></div></asp:LinkButton>
                                   </td>
                                   <td width="80px">                                                                                                                                                                                                         
                     </td></tr></table>
                </div>
            </td>
            </tr>--%>
        </table>                                                                                 
        
        </div>
                
    </ContentTemplate>
    </asp:UpdatePanel>
    
    </div>
    
    </div>
    </td></tr></table>
    </div>

</div>

</asp:Panel>        

    <dxlp:ASPxLoadingPanel ID="LoadingPanel" runat="server" Text="Importando arquivos, aguarde..." ClientInstanceName="LoadingPanel" Modal="True"/>
    
    </form>
</body>
</html>