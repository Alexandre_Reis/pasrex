﻿using Financial.Web.Common;
using System;
using System.IO;
using System.Globalization;
using System.Web;
using System.Collections.Generic;
using Dart.PowerTCP.Zip;

public partial class _ExibeExportacaoCustom : BasePage
{

    enum OwnerExportacao
    {
        Gradual = 0,
        //
        Quadrante = 1,
        //
        Masa = 2,
        //
        Fator = 3,
        //
        PortoPar = 4,

        Itau = 5,

        RbCapital = 6
    }

    enum TipoExportacao
    {
        /* Gradual */
        Cota = 0,
        Posicao = 1
        //
    }

    enum TipoExportacaoMasa
    {
        ParticipacaoSocietaria = 0,
        EvolucaoCapitalSocial = 1,
        ControleParticipacaoSocietaria = 2,
        ControleParticipacaoSocietariaFundos = 3,
        ComposicaoCarteira = 4
    }

    enum TipoExportacaoFator
    {
        FluxoCaixaExtol = 0
    }

    enum TipoExportacaoPortoPar {
        IntegralTrust = 0,
        Risco = 1,
        Rentabilidades = 2,
        RiscoExcel = 3,
        RentabilidadesExcel = 4
    }

    enum TipoExportacaoItau
    {
        OperacaoRF = 0,
        PosicaoRF = 1
    }

    enum TipoExportacaoRBCapital {
        Fluxo = 0,
        Amortizacao = 1
    }

    new protected void Page_Load(object sender, EventArgs e) {
        int owner = Convert.ToInt32(Request.QueryString["Owner"]);
        int tipo = Convert.ToInt32(Request.QueryString["Tipo"]);

        DateTime data = Convert.ToDateTime(Session["textData"]);

        switch (owner) {
            case (int)OwnerExportacao.Gradual:
                #region Gradual

                #region Cota
                if (tipo == (int)TipoExportacao.Cota)
                {

                    MemoryStream msCota = (MemoryStream)(Session["streamCotas"]);
                    string nomeArquivoCota = Convert.ToString(Session["nomeArquivoCotas"]);
                    //            
                    Response.ClearContent();
                    Response.ClearHeaders();
                    Response.Buffer = true;
                    Response.Cache.SetCacheability(HttpCacheability.Private);
                    Response.ContentType = "text/plain";
                    Response.AddHeader("Content-Length", msCota.Length.ToString(CultureInfo.CurrentCulture));
                    Response.AppendHeader("Content-Disposition", String.Format("attachment;filename=" + nomeArquivoCota));

                    Response.BinaryWrite(msCota.ToArray());
                    msCota.Close();
                    //
                    HttpContext.Current.ApplicationInstance.CompleteRequest();
                    Response.End();
                }
                #endregion

                #region Posicao
                else if (tipo == (int)TipoExportacao.Posicao)
                {
                    MemoryStream msPosicao = (MemoryStream)(Session["streamPosicaoCotista"]);
                    string nomeArquivoPosicao = Convert.ToString(Session["nomeArquivoPosicao"]);
                    //            
                    Response.ClearContent();
                    Response.ClearHeaders();
                    Response.Buffer = true;
                    Response.Cache.SetCacheability(HttpCacheability.Private);
                    Response.ContentType = "text/plain";
                    Response.AddHeader("Content-Length", msPosicao.Length.ToString(CultureInfo.CurrentCulture));
                    Response.AppendHeader("Content-Disposition", String.Format("attachment;filename=" + nomeArquivoPosicao));

                    Response.BinaryWrite(msPosicao.ToArray());
                    msPosicao.Close();
                    //
                    HttpContext.Current.ApplicationInstance.CompleteRequest();
                    Response.End();
                }
                #endregion

                #endregion
                break;
            case (int)OwnerExportacao.Quadrante:
                #region Quadrante

                #region Excel

                MemoryStream msExcel = (MemoryStream)(Session["streamExcel"]);
                string nomeArquivoExcel = Convert.ToString(Session["nomeArquivoExcel"]);
                //            
                Response.ClearContent();
                Response.ClearHeaders();
                Response.Buffer = true;
                Response.Cache.SetCacheability(HttpCacheability.Private);
                Response.ContentType = "application/xls";
                Response.AddHeader("Content-Length", msExcel.Length.ToString(CultureInfo.CurrentCulture));
                Response.AppendHeader("Content-Disposition", String.Format("attachment;filename=" + nomeArquivoExcel));

                Response.BinaryWrite(msExcel.ToArray());
                msExcel.Close();
                //
                HttpContext.Current.ApplicationInstance.CompleteRequest();
                Response.End();

                #endregion

                #endregion
                break;
            case (int)OwnerExportacao.Masa:
                #region Masa

                #region ParticipacaoSocietaria
                if (tipo == (int)TipoExportacaoMasa.ParticipacaoSocietaria)
                {
                    MemoryStream msExcelMasa = (MemoryStream)(Session["streamExcel"]);
                    string nomeArquivoExcelMasa = Convert.ToString(Session["nomeArquivoExcel"]);
                    //            
                    Response.ClearContent();
                    Response.ClearHeaders();
                    Response.Buffer = true;
                    Response.Cache.SetCacheability(HttpCacheability.Private);
                    Response.ContentType = "application/xls";
                    Response.AddHeader("Content-Length", msExcelMasa.Length.ToString(CultureInfo.CurrentCulture));
                    Response.AppendHeader("Content-Disposition", String.Format("attachment;filename=" + nomeArquivoExcelMasa));

                    Response.BinaryWrite(msExcelMasa.ToArray());
                    msExcelMasa.Close();
                    //
                    HttpContext.Current.ApplicationInstance.CompleteRequest();
                    Response.End();
                }
                #endregion

                #region EvolucaoCapitalSocial
                else if (tipo == (int)TipoExportacaoMasa.EvolucaoCapitalSocial)
                {
                    MemoryStream msExcelMasa = (MemoryStream)(Session["streamExcel"]);
                    string nomeArquivoExcelMasa = Convert.ToString(Session["nomeArquivoExcel"]);
                    //            
                    Response.ClearContent();
                    Response.ClearHeaders();
                    Response.Buffer = true;
                    Response.Cache.SetCacheability(HttpCacheability.Private);
                    Response.ContentType = "application/xls";
                    Response.AddHeader("Content-Length", msExcelMasa.Length.ToString(CultureInfo.CurrentCulture));
                    Response.AppendHeader("Content-Disposition", String.Format("attachment;filename=" + nomeArquivoExcelMasa));

                    Response.BinaryWrite(msExcelMasa.ToArray());
                    msExcelMasa.Close();
                    //
                    HttpContext.Current.ApplicationInstance.CompleteRequest();
                    Response.End();
                }
                #endregion

                #region ControleParticipacaoSocietaria
                else if (tipo == (int)TipoExportacaoMasa.ControleParticipacaoSocietaria)
                {
                    MemoryStream msExcelMasa = (MemoryStream)(Session["streamExcel"]);
                    string nomeArquivoExcelMasa = Convert.ToString(Session["nomeArquivoExcel"]);
                    //            
                    Response.ClearContent();
                    Response.ClearHeaders();
                    Response.Buffer = true;
                    Response.Cache.SetCacheability(HttpCacheability.Private);
                    Response.ContentType = "application/xls";
                    Response.AddHeader("Content-Length", msExcelMasa.Length.ToString(CultureInfo.CurrentCulture));
                    Response.AppendHeader("Content-Disposition", String.Format("attachment;filename=" + nomeArquivoExcelMasa));

                    Response.BinaryWrite(msExcelMasa.ToArray());
                    msExcelMasa.Close();
                    //
                    HttpContext.Current.ApplicationInstance.CompleteRequest();
                    Response.End();
                }

                #region ControleParticipacaoSocietariaFundos
                else if (tipo == (int)TipoExportacaoMasa.ControleParticipacaoSocietariaFundos)
                {
                    MemoryStream msExcelMasa = (MemoryStream)(Session["streamExcel"]);
                    string nomeArquivoExcelMasa = Convert.ToString(Session["nomeArquivoExcel"]);
                    //            
                    Response.ClearContent();
                    Response.ClearHeaders();
                    Response.Buffer = true;
                    Response.Cache.SetCacheability(HttpCacheability.Private);
                    Response.ContentType = "application/xls";
                    Response.AddHeader("Content-Length", msExcelMasa.Length.ToString(CultureInfo.CurrentCulture));
                    Response.AppendHeader("Content-Disposition", String.Format("attachment;filename=" + nomeArquivoExcelMasa));

                    Response.BinaryWrite(msExcelMasa.ToArray());
                    msExcelMasa.Close();
                    //
                    HttpContext.Current.ApplicationInstance.CompleteRequest();
                    Response.End();
                }
                #endregion
                #endregion

                #region ComposicaoCarteira
                else if (tipo == (int)TipoExportacaoMasa.ComposicaoCarteira)
                {

                    DateTime dataPdf = Convert.ToDateTime(Session["dataPDF"]);
                    int idCarteira = Convert.ToInt32(Session["idCarteira"]);

                    string fileName = "Carteira_" + idCarteira.ToString() + "_" +
                                  dataPdf.Day + "-" + dataPdf.Month + "-" + dataPdf.Year;

                    MemoryStream msPDFMasa = (MemoryStream)(Session["streamPDF"]);

                    msPDFMasa.Seek(0, SeekOrigin.Begin);
                    Response.ClearContent();
                    Response.ClearHeaders();
                    Response.Buffer = true;
                    Response.Cache.SetCacheability(HttpCacheability.Private);
                    Response.ContentType = "application/pdf";
                    Response.AddHeader("Content-Length", msPDFMasa.Length.ToString(CultureInfo.CurrentCulture));
                    Response.AddHeader("Content-Disposition", String.Format("attachment;filename=" + fileName + ".pdf"));

                    Response.BinaryWrite(msPDFMasa.ToArray());
                    msPDFMasa.Close();
                    HttpContext.Current.ApplicationInstance.CompleteRequest();
                    Response.End();
                }

                #region ControleParticipacaoSocietariaFundos
                else if (tipo == (int)TipoExportacaoMasa.ControleParticipacaoSocietariaFundos)
                {
                    MemoryStream msExcelMasa = (MemoryStream)(Session["streamExcel"]);
                    string nomeArquivoExcelMasa = Convert.ToString(Session["nomeArquivoExcel"]);
                    //            
                    Response.ClearContent();
                    Response.ClearHeaders();
                    Response.Buffer = true;
                    Response.Cache.SetCacheability(HttpCacheability.Private);
                    Response.ContentType = "application/xls";
                    Response.AddHeader("Content-Length", msExcelMasa.Length.ToString(CultureInfo.CurrentCulture));
                    Response.AppendHeader("Content-Disposition", String.Format("attachment;filename=" + nomeArquivoExcelMasa));

                    Response.BinaryWrite(msExcelMasa.ToArray());
                    msExcelMasa.Close();
                    //
                    HttpContext.Current.ApplicationInstance.CompleteRequest();
                    Response.End();
                }
                #endregion
                #endregion
                break;
                #endregion
            case (int)OwnerExportacao.Fator:
                #region Fator

                #region Excel
                if (tipo == (int)TipoExportacaoFator.FluxoCaixaExtol)
                {
                    string path = HttpUtility.UrlDecode(Request.QueryString["path"]);
                    string dataExportacao = HttpUtility.UrlDecode(Request.QueryString["data"]);
                    string nomeArquivoZip = "Fluxo_de_caixa_FATOR_CORRETORA_" + dataExportacao + ".zip";
                    using (MemoryStream msZip = new MemoryStream())
                    using (Archive arquivoFluxoCaixa = new Archive())
                    {
                        arquivoFluxoCaixa.Add(path);
                        arquivoFluxoCaixa.Zip(msZip);

                        Response.ClearContent();
                        Response.ClearHeaders();
                        Response.Buffer = true;
                        Response.Cache.SetCacheability(HttpCacheability.Private);
                        Response.ContentType = "application/xls";
                        Response.AddHeader("Content-Length", msZip.Length.ToString(CultureInfo.CurrentCulture));
                        Response.AppendHeader("Content-Disposition", String.Format("attachment;filename=" + nomeArquivoZip));

                        Response.BinaryWrite(msZip.ToArray());

                        HttpContext.Current.ApplicationInstance.CompleteRequest();
                    }

                    // Se Diretorio Existe Apaga o Diretorio
                    if (Directory.Exists(path))
                    {
                        Directory.Delete(path, true);
                    }
                }
                #endregion

                #endregion
                break;
            case (int)OwnerExportacao.PortoPar:
                #region PortoPar

                #region IntegralTrust
                if (tipo == (int)TipoExportacaoPortoPar.IntegralTrust) {

                    Dictionary<string, MemoryStream> msPosicao = (Dictionary<string, MemoryStream>)Session["streamIntegralTrust"];

                    #region Zip

                    Archive arquivo = new Archive();
                    MemoryStream msZip = new MemoryStream();
                    //                
                    try {

                        int j = 0;
                        foreach (KeyValuePair<string, MemoryStream> pair in msPosicao) {
                            arquivo.Add(pair.Value); // Memory Stream
                            arquivo[j].Name = pair.Key; // Nome do Arquivo
                            //
                            j++;
                        }

                        //
                        arquivo.Zip(msZip);
                    }
                    catch (Exception e1) {
                        throw new Exception(e1.Message + "Erro ao Zipar o Arquivo");
                    }
                    Response.ClearContent();
                    Response.ClearHeaders();
                    Response.Buffer = true;
                    Response.Cache.SetCacheability(HttpCacheability.Private);
                    Response.ContentType = "application/zip";
                    Response.AddHeader("Content-Length", msZip.Length.ToString(CultureInfo.CurrentCulture));
                    Response.AppendHeader("Content-Disposition", String.Format("attachment;filename=PlanilhasPosicao.zip"));

                    Response.BinaryWrite(msZip.ToArray());
                    msZip.Close();

                    HttpContext.Current.ApplicationInstance.CompleteRequest();
                    Response.End();
                    #endregion                                                                                  
                }
                #endregion

                #region Rentabilidade
                if (tipo == (int)TipoExportacaoPortoPar.Rentabilidades) {
                    MemoryStream msReportRentabilidade = (MemoryStream)(Session["msReportRentabilidade"]);
                    string nomeArquivo = "ReportRentabilidade";

                    Response.ClearContent();
                    Response.ClearHeaders();
                    Response.Buffer = true;
                    Response.Cache.SetCacheability(HttpCacheability.Private);
                    Response.ContentType = "application/pdf";
                    Response.AddHeader("Content-Length", msReportRentabilidade.Length.ToString(CultureInfo.CurrentCulture));
                    Response.AddHeader("Content-Disposition", String.Format("attachment;filename=" + nomeArquivo + ".pdf"));

                    Response.BinaryWrite(msReportRentabilidade.ToArray());
                    msReportRentabilidade.Close();
                    HttpContext.Current.ApplicationInstance.CompleteRequest();
                    Response.End();
                }
                #endregion

                #region RentabilidadeExcel
                if (tipo == (int)TipoExportacaoPortoPar.RentabilidadesExcel)
                {
                    MemoryStream msReportRentabilidadeExcel = (MemoryStream)(Session["msReportRentabilidadeExcel"]);
                    string nomeArquivo = "ReportRentabilidade";

                    Response.ClearContent();
                    Response.ClearHeaders();
                    Response.Buffer = true;
                    Response.Cache.SetCacheability(HttpCacheability.Private);
                    Response.ContentType = "application/xls";
                    Response.AddHeader("Content-Length", msReportRentabilidadeExcel.Length.ToString(CultureInfo.CurrentCulture));
                    Response.AddHeader("Content-Disposition", String.Format("attachment;filename=" + nomeArquivo + ".xls"));

                    Response.BinaryWrite(msReportRentabilidadeExcel.ToArray());
                    msReportRentabilidadeExcel.Close();
                    HttpContext.Current.ApplicationInstance.CompleteRequest();
                    Response.End();
                }
                #endregion

                #region Risco
                if (tipo == (int)TipoExportacaoPortoPar.Risco) {
                    MemoryStream msReportRisco = (MemoryStream)(Session["msReportRisco"]);
                    string nomeArquivo = "ReportRisco";
                                                                                                   
                    Response.ClearContent();
                    Response.ClearHeaders();
                    Response.Buffer = true;
                    Response.Cache.SetCacheability(HttpCacheability.Private);
                    Response.ContentType = "application/pdf";
                    Response.AddHeader("Content-Length", msReportRisco.Length.ToString(CultureInfo.CurrentCulture));
                    Response.AddHeader("Content-Disposition", String.Format("attachment;filename=" + nomeArquivo + ".pdf"));

                    Response.BinaryWrite(msReportRisco.ToArray());
                    msReportRisco.Close();
                    HttpContext.Current.ApplicationInstance.CompleteRequest();
                    Response.End();
                }
                #endregion

                #region RiscoExcel
                if (tipo == (int)TipoExportacaoPortoPar.RiscoExcel)
                {
                    MemoryStream msReportRiscoExcel = (MemoryStream)(Session["msReportRiscoExcel"]);
                    string nomeArquivo = "ReportRisco";

                    Response.ClearContent();
                    Response.ClearHeaders();
                    Response.Buffer = true;
                    Response.Cache.SetCacheability(HttpCacheability.Private);
                    Response.ContentType = "application/xls";
                    Response.AddHeader("Content-Length", msReportRiscoExcel.Length.ToString(CultureInfo.CurrentCulture));
                    Response.AddHeader("Content-Disposition", String.Format("attachment;filename=" + nomeArquivo + ".xls"));

                    Response.BinaryWrite(msReportRiscoExcel.ToArray());
                    msReportRiscoExcel.Close();
                    HttpContext.Current.ApplicationInstance.CompleteRequest();
                    Response.End();
                }
                #endregion

                #endregion
                break;
            case (int)OwnerExportacao.Itau:
                #region Itau

                #region OperacaoRF
                if (tipo == (int)TipoExportacaoItau.OperacaoRF)
                {
                    MemoryStream msPlanilhaOperacaoRFExcel = (MemoryStream)(Session["msPlanilhaOperacaoRFExcel"]);
                    string nomeArquivo = "OperacaoRendaFixa";

                    Response.ClearContent();
                    Response.ClearHeaders();
                    Response.Buffer = true;
                    Response.Cache.SetCacheability(HttpCacheability.Private);
                    Response.ContentType = "application/xls";
                    Response.AddHeader("Content-Length", msPlanilhaOperacaoRFExcel.Length.ToString(CultureInfo.CurrentCulture));
                    Response.AddHeader("Content-Disposition", String.Format("attachment;filename=" + nomeArquivo + ".xls"));

                    Response.BinaryWrite(msPlanilhaOperacaoRFExcel.ToArray());
                    msPlanilhaOperacaoRFExcel.Close();
                    HttpContext.Current.ApplicationInstance.CompleteRequest();
                    Response.End();
                }
                #endregion

                #region PosicaoRF
                if (tipo == (int)TipoExportacaoItau.PosicaoRF)
                {
                    MemoryStream msPlanilhaPosicaoRFExcel = (MemoryStream)(Session["msPlanilhaPosicaoRFExcel"]);
                    string nomeArquivo = "PosicaoRendaFixa";

                    Response.ClearContent();
                    Response.ClearHeaders();
                    Response.Buffer = true;
                    Response.Cache.SetCacheability(HttpCacheability.Private);
                    Response.ContentType = "application/xls";
                    Response.AddHeader("Content-Length", msPlanilhaPosicaoRFExcel.Length.ToString(CultureInfo.CurrentCulture));
                    Response.AddHeader("Content-Disposition", String.Format("attachment;filename=" + nomeArquivo + ".xls"));

                    Response.BinaryWrite(msPlanilhaPosicaoRFExcel.ToArray());
                    msPlanilhaPosicaoRFExcel.Close();
                    HttpContext.Current.ApplicationInstance.CompleteRequest();
                    Response.End();
                }
                #endregion

            #endregion
                break;
            case (int)OwnerExportacao.RbCapital:
                #region RbCapital

                #region Fluxo
                if (tipo == (int)TipoExportacaoRBCapital.Fluxo) {                    
                    MemoryStream msExcel1 = (MemoryStream)(Session["streamExcel"]);
                    string nomeArquivoExcel1 = Convert.ToString(Session["nomeArquivoExcel"]);
                    //            
                    Response.ClearContent();
                    Response.ClearHeaders();
                    Response.Buffer = true;
                    Response.Cache.SetCacheability(HttpCacheability.Private);
                    Response.ContentType = "application/xls";
                    Response.AddHeader("Content-Length", msExcel1.Length.ToString(CultureInfo.CurrentCulture));
                    Response.AppendHeader("Content-Disposition", String.Format("attachment;filename=" + nomeArquivoExcel1));

                    Response.BinaryWrite(msExcel1.ToArray());
                    msExcel1.Close();
                    //
                    HttpContext.Current.ApplicationInstance.CompleteRequest();
                    Response.End();                    
                }
                #endregion

                #region Amortizacao
                else if (tipo == (int)TipoExportacaoRBCapital.Amortizacao) {
                    MemoryStream msExcel1 = (MemoryStream)(Session["streamExcel"]);
                    string nomeArquivoExcel1 = Convert.ToString(Session["nomeArquivoExcel"]);
                    //            
                    Response.ClearContent();
                    Response.ClearHeaders();
                    Response.Buffer = true;
                    Response.Cache.SetCacheability(HttpCacheability.Private);
                    Response.ContentType = "application/xls";
                    Response.AddHeader("Content-Length", msExcel1.Length.ToString(CultureInfo.CurrentCulture));
                    Response.AppendHeader("Content-Disposition", String.Format("attachment;filename=" + nomeArquivoExcel1));

                    Response.BinaryWrite(msExcel1.ToArray());
                    msExcel1.Close();
                    //
                    HttpContext.Current.ApplicationInstance.CompleteRequest();
                    Response.End();
                }
                #endregion

                #endregion
                break;
        }
    }
}