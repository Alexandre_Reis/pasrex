﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="RBCapital.aspx.cs" Inherits="InterfacesCustom_RBCapital" %>

<%@ Register Assembly="System.Web, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a" Namespace="System.Web.UI.HtmlControls" TagPrefix="cc2" %>    
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxcb" %>
<%@ Register Assembly="System.Web, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a" Namespace="System.Web.UI" TagPrefix="cc1" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxnb" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxe" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxwgv" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxpc" %>
<%@ Register Assembly="EntitySpaces.Web" Namespace="EntitySpaces.Web" TagPrefix="cc1" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxlp" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxrp" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxp" %>


<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxtc" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" tagprefix="dxw" %>    

<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <link href="../css/forms.css" rel="stylesheet" type="text/css" />

<script type="text/javascript" language="Javascript" src="../js/global.js"></script>

<script type="text/javascript" language="Javascript">
    popup = true;
    document.onkeydown=onDocumentKeyDownWithCallback;
    var operacao = '';
</script>

<script type="text/javascript" language="JavaScript">
var backgroundColor;
function OnGetDataCliente(values) {
    btnEditCodigo.SetValue(values);                    
    popupCliente.HideWindow();
    ASPxCallback1.SendCallback(btnEditCodigo.GetValue());        
    btnEditCodigo.Focus();    
}
</script>
    
<script type="text/javascript" language="Javascript">    
	var isCustomCallback = false;   // usada para controlar o refresh após um delete feito no grid
</script>

</head>

<body>
    
<dxcb:ASPxCallback ID="callbackErro" runat="server" OnCallback="callbackErro_Callback">
    <ClientSideEvents CallbackComplete="function(s, e) {             
        if (e.result != '') {
            alert(e.result);                              
        }
        else {
            if (operacao == 'salvar' || gridCadastro.cp_EditVisibleIndex != 'new') {             
                gridCadastro.UpdateEdit();
            }
            else {
                callbackAdd.SendCallback();
            }    
        }
        
        operacao = '';
    }
    "/>
</dxcb:ASPxCallback>

<dxcb:ASPxCallback ID="callbackAdd" runat="server" OnCallback="callbackAdd_Callback">
    <ClientSideEvents CallbackComplete="function(s, e) {
        alert('Operação feita com sucesso.');
    }        
    "/>
</dxcb:ASPxCallback>

<dxcb:ASPxCallback ID="ASPxCallback1" runat="server" OnCallback="ASPxCallback1_Callback">
        <ClientSideEvents CallbackComplete="function(s, e) {             
            OnCallBackCompleteCliente(s, e, popupMensagemCliente, btnEditCodigo, document.getElementById('textNome'));
        }        
        "/>
</dxcb:ASPxCallback>

<form id="form1" runat="server">
<asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="true" EnableScriptLocalization="true"/>

<asp:Panel ID="panelEdicao" runat="server" OnLoad="panelEdicao_Load">
<div id="divExportacao" runat="server" visible="false">    
        <div class="divPanel divPanelNew">
        <div id="Div1"        
            style="white-space: nowrap;
                    text-align: center;
                    font: bold 11px Tahoma, Arial;
                    color: #333333;
                    background-image: url(  '../imagens/layout/nbHeaderBack.gif' );
                    background-repeat: repeat-x;
                    background-repeat: repeat-y;
                    background-position: top;
                    border-left-style: none;
                    border-right-style: none;
                    border-top-style: none;
                    border-bottom: solid 1px #D5D5D5;
                    padding: 3px 10px 3px 5px;">
            <div id="header">
                <asp:Label ID="lblHeader" runat="server" Text="Exportação (RbCapital)"/>
            </div>
            <div id="mainContent" style="background-image: none !important;">
                <div class="reportFilter" style="background-image: none !important;">
                    <br />
                    <div style="text-align: left; margin-left:20px"><b>Fluxo RB</b></div>
                    <br />                    
                    <table border="0">         
                        <tr>
                            <td>                                                      
                             
                             <asp:UpdatePanel ID="UpdatePanel1" runat="server"> 
                                <ContentTemplate>                                                                                                            
                                    <table>
                                        <tr>
                                            <td class="td_Label">
                                                <asp:Label ID="labelCliente" runat="server" CssClass="labelRequired" Text="Cliente:"/>
                                            </td>
                                            
                                            <td>
                                                <dxe:ASPxSpinEdit ID="btnEditCodigo" runat="server" CssClass="textButtonEdit"                                                                             
                                                                            ClientInstanceName="btnEditCodigo" SpinButtons-ShowIncrementButtons="false"
                                                                            MaxLength="9" NumberType="Integer" AllowMouseWheel="false">  
                                                <Buttons>
                                                    <dxe:EditButton>
                                                    </dxe:EditButton>                                
                                                </Buttons>        
                                                <ClientSideEvents                        
                                                         KeyPress="function(s, e) {document.getElementById('textNome').value = '';}" 
                                                         ButtonClick="function(s, e) { popupCliente.ShowAtElementByID(s.name); }" 
                                                         LostFocus="function(s, e) {OnLostFocus(popupMensagemCliente, ASPxCallback1, btnEditCodigo);}"
                                                />                            
                                                </dxe:ASPxSpinEdit>                
                                            </td>                          
                                            
                                            <td colspan="2" width="300">
                                                <asp:TextBox ID="textNome" runat="server" CssClass="textNome" Enabled="false"></asp:TextBox>       
                                            </td>
                                            
                                        </tr>
                                        <tr>                                                                                                                     
                                            <td class="td_Label">                
                                                <asp:Label ID="labelData" runat="server" CssClass="labelRequired" Text="Data:"></asp:Label>
                                            </td>                        
                                            
                                            <td>
                                                <dxe:ASPxDateEdit ID="textData" runat="server" ClientInstanceName="textData" EditFormat="Custom" EditFormatString="dd/MM/yyyy"/>
                                            </td>                                                                                                      

                                            <td>                            
                                                <div id="reportLinkButton" class="linkButton linkButtonNoBorder">
                                                    <asp:LinkButton ID="btnExcel1" runat="server" Font-Overline="false" CssClass="btnRun"
                                                        OnClick="btnExcel1_Click"><asp:Literal ID="Literal1" runat="server" Text="Exportar"/>
                                                        <div></div>
                                                    </asp:LinkButton>
                                                </div>                        
                                            </td>
                                        </tr>                                                                                
                                    </table>                                                           
                                </ContentTemplate>
                            </asp:UpdatePanel>         
                            </td>
                            
                            <td>
                            </td>
                        </tr>         
                     </table>    
                    <br />
                    <%--<hr />--%>
                    <div style="text-align: left;margin-left:20px"><b>PAGAMENTO DE RENDIMENTOS E AMORTIZAÇÃO - CRI- Cetip</b></div>    
                    <br />
                    <table border="0">         
                        <tr>
                            <td>                                                      
                             
                             <asp:UpdatePanel ID="UpdatePanel2" runat="server"> 
                                <ContentTemplate>                                                                                                            
                                    <table border="0">
                                        <tr>
                                            <td class="td_Label">
                                                <asp:Label ID="labelTitulo" runat="server" CssClass="labelRequired" Text="Título:" />
                                            </td>
                                            <td colspan="3">
                                                <dx:ASPxGridLookup ID="dropTituloRendaFixa" ClientInstanceName="dropTituloRendaFixa"
                                                    runat="server" SelectionMode="Single" KeyFieldName="IdTitulo" DataSourceID="EsDSTituloRendaFixaFiltro"
                                                    Width="500px" TextFormatString="{5}" Font-Size="11px" IncrementalFilteringMode="contains"
                                                    ClientSideEvents-ValueChanged="Validate" AllowUserInput="false">
                                                    <Columns>
                                                        <dxwgv:GridViewCommandColumn ShowSelectCheckbox="True" Width="4%" />
                                                        <dxwgv:GridViewDataColumn FieldName="IdTitulo" Caption="Id" Width="10%" CellStyle-Font-Size="X-Small" />
                                                        <dxwgv:GridViewDataColumn FieldName="DescricaoPapel" Caption="Papel" CellStyle-Font-Size="X-Small"
                                                            Settings-AutoFilterCondition="Contains" Width="20%" />
                                                        <dxwgv:GridViewDataColumn FieldName="Descricao" Caption="Descrição" CellStyle-Font-Size="X-Small"
                                                            Settings-AutoFilterCondition="Contains" Width="45%" />
                                                        <dxwgv:GridViewDataDateColumn FieldName="DataEmissao" Caption="Data Emissão" CellStyle-Font-Size="X-Small"
                                                            Width="10%" />
                                                        <dxwgv:GridViewDataDateColumn FieldName="DataVencimento" Caption="Data Vencimento"
                                                            CellStyle-Font-Size="X-Small" Width="10%" />
                                                        <dxwgv:GridViewDataTextColumn FieldName="DescricaoCompleta" Caption="Descrição Completa"
                                                            CellStyle-Font-Size="X-Small" Settings-AutoFilterCondition="Contains" Visible="false" />
                                                    </Columns>
                                                    <GridViewProperties>
                                                        <Settings ShowFilterRow="True" />
                                                    </GridViewProperties>
                                                </dx:ASPxGridLookup>
                                            </td>
                                        </tr>
                                        <tr>                                                                                                                     
                                            <td class="td_Label">                
                                                <asp:Label ID="label2" runat="server" CssClass="labelRequired" Text="Data:"></asp:Label>
                                            </td>                        
                                            
                                            <td style="width:50px">
                                                <dxe:ASPxDateEdit ID="textDataAmortizacao" runat="server" ClientInstanceName="textDataAmortizacao" EditFormat="Custom" EditFormatString="dd/MM/yyyy"/>
                                            </td>                                                                                                      

                                            <td colspan="2">                            
                                                <div id="Div2" class="linkButton linkButtonNoBorder">
                                                    <asp:LinkButton ID="btnExcel2" runat="server" Font-Overline="false" CssClass="btnRun"
                                                        OnClick="btnExcel2_Click"><asp:Literal ID="Literal2" runat="server" Text="Exportar"/>
                                                        <div></div>
                                                    </asp:LinkButton>
                                                </div>                        
                                            </td>
                                        </tr>                                                                                
                                    </table>                                                           
                                </ContentTemplate>
                            </asp:UpdatePanel>         
                            </td>
                            
                            <td>
                            </td>
                        </tr>         
                     </table>                     
                </div>
            </div>
        </div>
        </div>        
</div>

<div id="divImportacao" runat="server" visible="false">
    <dxpc:ASPxPopupControl ID="popupFiltro" AllowDragging="true" PopupElementID="popupFiltro"
        EnableClientSideAPI="True" PopupVerticalAlign="Middle" PopupHorizontalAlign="OutsideRight"
        Width="400" Left="250" Top="70" HeaderText="Filtros adicionais para consulta"
        runat="server" HeaderStyle-BackColor="#EBECEE" HeaderStyle-Font-Bold="true" HeaderStyle-Font-Size="11px">
        <ContentCollection>
            <dxpc:PopupControlContentControl ID="PopupControlContentControl2" runat="server">
                <table>
                    <tr>
                        <td>
                            <asp:Label ID="labelDataInicio" runat="server" CssClass="labelNormal" Text="Início:"></asp:Label>
                        </td>
                        <td>
                            <dxe:ASPxDateEdit ID="textDataInicio" runat="server" ClientInstanceName="textDataInicio" />
                        </td>
                        <td>
                            <asp:Label ID="labelDataFim" runat="server" CssClass="labelNormal" Text="Fim:" />
                        </td>
                        <td>
                            <dxe:ASPxDateEdit ID="textDataFim" runat="server" ClientInstanceName="textDataFim" />
                        </td>
                    </tr>
                </table>
                <div class="linkButton linkButtonNoBorder" style="margin-top: 20px">
                    <asp:LinkButton ID="btnOKFilter" runat="server" Font-Overline="false" ForeColor="Black"
                        CssClass="btnOK" OnClientClick="popupFiltro.Hide(); gridCadastro.PerformCallback('btnRefresh'); return false;">
                        <asp:Literal ID="Literal7" runat="server" Text="Aplicar" /><div>
                        </div>
                    </asp:LinkButton>
                    <asp:LinkButton ID="btnFilterCancel" runat="server" Font-Overline="false" ForeColor="Black"
                        CssClass="btnFilterCancel" OnClientClick="OnClearFilterClick_FiltroDatas(); return false;">
                        <asp:Literal ID="Literal8" runat="server" Text="Limpar" /><div>
                        </div>
                    </asp:LinkButton>
                </div>
            </dxpc:PopupControlContentControl>
        </ContentCollection>
    </dxpc:ASPxPopupControl>
            
    
    <div id="container_small">
    <div class="divPanel divPanelNew">
        </div>
        <div style="width: 100%; margin: 0 auto;">
        <dxtc:ASPxPageControl ID="tabRBCapital" runat="server" Width="100%" Height="100%"
        ClientInstanceName="tabRBCapital" ActiveTabIndex="0" TabSpacing="0px">
        <TabPages>
            <dxtc:TabPage Text="Cotações do Mercado Fiduciário">
            <ContentCollection>
                <dxw:ContentControl runat="server">
            <div class="reportFilter">
                <table>
                    <tr>
                        <td>
                            <div class="linkButton">
                                <asp:LinkButton ID="btnAdd" runat="server" Font-Overline="false" CssClass="btnAdd"
                                    OnClientClick="gridCadastro.AddNewRow(); return false;">
                                    <asp:Literal ID="Literal3" runat="server" Text="Novo" /><div>
                                    </div>
                                </asp:LinkButton>
                                <asp:LinkButton ID="btnDelete" runat="server" Font-Overline="false" CssClass="btnDelete"
                                    OnClientClick=" if (confirm('Tem certeza que quer excluir?')==true) gridCadastro.PerformCallback('btnDelete');return false;">
                                    <asp:Literal ID="Literal4" runat="server" Text="Excluir" /><div>
                                    </div>
                                </asp:LinkButton>
                                <asp:LinkButton ID="btnFilter" runat="server" Font-Overline="false" CssClass="btnFilter"
                                    OnClientClick="return OnButtonClick_FiltroDatas()">
                                    <asp:Literal ID="Literal5" runat="server" Text="Filtro" /><div>
                                    </div>
                                </asp:LinkButton>
                                <asp:LinkButton ID="btnPDF" runat="server" Font-Overline="false" CssClass="btnPdf"
                                    OnClick="btnPDF_Click">
                                    <asp:Literal ID="Literal6" runat="server" Text="Gerar PDF" /><div>
                                    </div>
                                </asp:LinkButton>
                                <asp:LinkButton ID="btnExcel" runat="server" Font-Overline="false" CssClass="btnExcel"
                                    OnClick="btnExcel_Click">
                                    <asp:Literal ID="Literal9" runat="server" Text="Gerar Excel" /><div>
                                    </div>
                                </asp:LinkButton>
                                <asp:LinkButton ID="btnRefresh" runat="server" Font-Overline="false" CssClass="btnRefresh"
                                    OnClientClick=" gridCadastro.PerformCallback('btnRefresh'); return false;">
                                    <asp:Literal ID="Literal10" runat="server" Text="Atualizar" /><div>
                                    </div>
                                </asp:LinkButton>
                            </div>
                        </td>
                    </tr> 
                    <tr>
                        <td>
                            <div class="divDataGrid">
                                <dxwgv:ASPxGridView ID="gridCadastro" runat="server" EnableCallBacks="true" ClientInstanceName="gridCadastro"
                                    KeyFieldName="CompositeKey" DataSourceID="EsDSCotacaoMercadoFiduciario" OnCustomUnboundColumnData="gridCadastro_CustomUnboundColumnData"
                                    OnRowUpdating="gridCadastro_RowUpdating" OnRowInserting="gridCadastro_RowInserting"
                                    OnCustomCallback="gridCadastro_CustomCallback">
                                    <Columns>
                                        <dxwgv:GridViewCommandColumn ShowSelectCheckbox="True" VisibleIndex="0" Width="10%" ButtonType="Image" ShowClearFilterButton="True">
                                            <HeaderTemplate>
                                                <dxe:ASPxCheckBox ID="cbAll" ClientInstanceName="cbAll" runat="server" ToolTip="Seleciona todos os CheckBoxs de todas as páginas" BackColor="white" ClientSideEvents-CheckedChanged="function(s, e) {OnAllCheckedChanged(s, e, gridCadastro);}" OnInit="CheckBoxSelectAll"/>
                                            </HeaderTemplate>
                                        </dxwgv:GridViewCommandColumn>
                                        
                                        <dxwgv:GridViewDataComboBoxColumn Caption="Título" FieldName="IdTitulo" VisibleIndex="3" Width="60%">
                                                <PropertiesComboBox DataSourceID="EsDSTitulo" TextField="DescricaoCompleta" ValueField="IdTitulo"></PropertiesComboBox>
                                        </dxwgv:GridViewDataComboBoxColumn>
                                        <dxwgv:GridViewDataDateColumn FieldName="Data" Caption="Data" VisibleIndex="3" Width="20%" />
                                        <dxwgv:GridViewDataSpinEditColumn FieldName="Cotacao" Caption="Cotação"
                                            VisibleIndex="4" Width="20%">
                                            <PropertiesSpinEdit DisplayFormatString="{0:#,##0.0000;(#,##0.0000);0.0000}">
                                            </PropertiesSpinEdit>
                                        </dxwgv:GridViewDataSpinEditColumn>
                                        <dxwgv:GridViewDataTextColumn FieldName="CompositeKey" UnboundType="String" Visible="False" />
                                    </Columns>
                                    <Templates>
                                        <EditForm>
                                            <div class="editForm">
                                                <table>
                                                    <tr>
                                                        <td class="td_Label">
                                                            <asp:Label ID="labelTitulo" runat="server" CssClass="labelRequired" Text="Título:"></asp:Label>
                                                        </td>
                                                        <td>
                                                            <dxe:ASPxComboBox ID="dropIdTitulo" ClientInstanceName="dropIdTitulo" runat="server" DataSourceID="EsDSTitulo" 
                                                            ValueField="IdTitulo" TextField="DescricaoCompleta" CssClass="dropDownListLongo" Text='<%#Eval("IdTitulo")%>' OnLoad="dropIdTitulo_Load">
                                                            </dxe:ASPxComboBox>          
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="td_Label_Curto">
                                                            <asp:Label ID="labelData" runat="server" CssClass="labelRequired" Text="Data:"></asp:Label>
                                                        </td>
                                                        <td colspan="4">
                                                            <dxe:ASPxDateEdit ID="textData" runat="server" ClientInstanceName="textData"
                                                                Value='<%#Eval("Data")%>' OnInit="textData_OnInit" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="td_Label_Curto">
                                                            <asp:Label ID="labelCotacao" runat="server" CssClass="labelRequired" Text="Cotação:"></asp:Label>
                                                        </td>
                                                        <td colspan="4">
                                                            <dxe:ASPxSpinEdit ID="textCotacao" runat="server" CssClass="textValor" ClientInstanceName="textCotacao"
                                                                NumberType="Float" MaxLength="16" DecimalPlaces="8" Text="<%#Bind('Cotacao')%>">
                                                            </dxe:ASPxSpinEdit>
                                                        </td>
                                                    </tr>
                                                </table>
                                                
                                                <div class="linhaH"></div>
                                                
                                                <div class="linkButton linkButtonNoBorder popupFooter">
                                                                                                <asp:LinkButton ID="btnOKAdd" runat="server" Font-Overline="false" CssClass="btnSaveadd"
                                                                                                    OnInit="btnOKAdd_Init" OnClientClick="if (operacao != '') return false; operacao='salvarAdd'; callbackErro.SendCallback(); return false;">
                                                                                                    <asp:Literal ID="Literal10" runat="server" Text="OK+" /><div>
                                                                                                    </div>
                                                                                                </asp:LinkButton>
                                                                                                <asp:LinkButton ID="btnOK" runat="server" Font-Overline="false" CssClass="btnOK"
                                                                                                    OnClientClick="if (operacao != '') return false; operacao='salvar'; callbackErro.SendCallback(); return false;">
                                                                                                    <asp:Literal ID="Literal12" runat="server" Text="OK" /><div>
                                                                                                    </div>
                                                                                                </asp:LinkButton>
                                                                                                <asp:LinkButton ID="btnCancel" runat="server" Font-Overline="false" CssClass="btnCancel"
                                                                                                    OnClientClick="gridCadastro.CancelEdit(); return false;">
                                                                                                    <asp:Literal ID="Literal9" runat="server" Text="Cancelar" /><div>
                                                                                                    </div>
                                                                                                </asp:LinkButton>
                                                                                            </div>
                                                  
                               
                                                    
                                            </div>
                                    <SettingsCommandButton>
                                        <ClearFilterButton Image-Url="../../imagens/funnel--minus.png"/>
                                    </SettingsCommandButton>
                                        </div>
                                    </EditForm>
                                </Templates>                
                                <SettingsPopup EditForm-Width="400px" />
        
                                <ClientSideEvents

                                    BeginCallback="function(s, e) {
                                        if (e.command == 'CUSTOMCALLBACK') {                            
                                            isCustomCallback = true;
                                        }
                                    }"
                                    
                                    EndCallback="function(s, e) {
                                        if (isCustomCallback) {
                                            isCustomCallback = false;
                                            s.Refresh();
                                        }
                                    }" 

                                />
                            </dxwgv:ASPxGridView>
                        </div>      
                    </td>
                </tr>  
                </table> 
                </div>
                </dxw:ContentControl>
            </ContentCollection>
                </dxtc:TabPage>
        </TabPages>
        </dxtc:ASPxPageControl>
            </div>
        </div>
     </div>
</asp:Panel>  

<cc1:esDataSource ID="EsDSCotacaoMercadoFiduciario" runat="server" OnesSelect="EsDSCotacaoMercadoFiduciario_esSelect" LowLevelBind="true" />
<cc1:esDataSource ID="EsDSTitulo" runat="server" OnesSelect="EsDSTitulo_esSelect" />    
<dxwgv:ASPxGridViewExporter ID="gridExport" runat="server" GridViewID="gridCadastro" />


<!-- Exportacao -->
<cc1:esDataSource ID="EsDSCliente" runat="server" OnesSelect="EsDSCliente_esSelect" />
<cc1:esDataSource ID="EsDSTituloRendaFixaFiltro" runat="server" OnesSelect="EsDSTituloRendaFixaFiltro_esSelect" />
    
</form>    
</body>
</html>
