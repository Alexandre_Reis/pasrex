﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Itau.aspx.cs" Inherits="InterfacesCustom_Itau" %>

<%@ Register Assembly="System.Web, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a" Namespace="System.Web.UI.HtmlControls" TagPrefix="cc2" %>    
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxcb" %>
<%@ Register Assembly="System.Web, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a" Namespace="System.Web.UI" TagPrefix="cc1" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxnb" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxe" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxwgv" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxpc" %>
<%@ Register Assembly="EntitySpaces.Web" Namespace="EntitySpaces.Web" TagPrefix="cc1" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxlp" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxrp" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxp" %>


<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxtc" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" tagprefix="dxw" %>    


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">

<link href="../css/forms.css" rel="stylesheet" type="text/css" />

<script type="text/javascript" language="JavaScript">
var backgroundColor;
function OnGetDataCliente(values) {
    btnEditCodigo.SetValue(values);                    
    popupCliente.HideWindow();
    ASPxCallback1.SendCallback(btnEditCodigo.GetValue());        
    btnEditCodigo.Focus();    
}
</script>
<script type="text/javascript" language="Javascript" src="../js/global.js"></script>

<script language="JavaScript" type="text/javascript">
    function btnExportaClick(tipoExportacao){              
       LoadingPanel1.Show();
       callbackExportaExcel.SendCallback(tipoExportacao);
    }    
</script>

</head>

<body>
    <form id="form1" runat="server">
    
      <dxcb:ASPxCallback ID="callbackExportaExcel" runat="server" OnCallback="callbackExportaExcel_Callback">
        <ClientSideEvents CallbackComplete="function(s, e) 
        {   
            LoadingPanel1.Hide();
            if(e && e.result.length){
                if(e.result.indexOf('redirect') === 0){
                    var newLocation = (e.result.split(':'))[1];
                    
                    var iframeId = 'iframe_download';
                    var iframe = document.getElementById(iframeId);

                    if (!iframe && document.createElement && (iframe = document.createElement('iframe'))) {
               
                        iframe.name = iframe.id = iframeId;
                        iframe.width = 0;
                        iframe.height = 0;
                        iframe.src = 'about:blank';
                        document.body.appendChild(iframe);                                                                    
                    }
                    iframe.src = newLocation;
                   
                } else {
                    alert(e.result);
                }
            }                    
        }" />
    </dxcb:ASPxCallback>
           
    <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="true" EnableScriptLocalization="true"/>

<asp:Panel ID="panelEdicao" runat="server" OnLoad="panelEdicao_Load">
    
<div id="divExportacao" runat="server" visible="false">    
        <div class="divPanel divPanelNew">
        <div id="container_small">
            <div id="header">
                <asp:Label ID="lblHeader" runat="server" Text="Exportação de Arquivo Excel"/>
            </div>
            <div id="mainContent">
                <div class="reportFilter">
                    <div class="dataMessage">
                    </div>    
                    <table border="0">         
                        <tr>
                            <td>                                                      
                             
                             <asp:UpdatePanel ID="UpdatePanel1" runat="server"> 
                                <ContentTemplate>                                                                                                            
                                    <table>
                                        <tr height="30px">
                                            <td>&nbsp;</td>
                                        </tr>
                                        <tr height="30px">
                                            <td>&nbsp;</td>
                                        </tr>
                                        <tr height="30px">
                                            <td>&nbsp;</td>
                                        </tr>
                                        <tr>                                                                                                                     
                                            <td class="td_Label_Longo">                
                                                <asp:Label ID="labelOperacao" runat="server" CssClass="labelNormal" Text="Operação Renda Fixa" Font-Bold="true"></asp:Label>
                                            </td>                  
                                                                                                                      
                                            <td width="30px">                   
                                                &nbsp;
                                             </td>   
                                                                                                                                                           
                                            <td class="td_Label">                
                                                <asp:Label ID="labelDataReferencia1" runat="server" CssClass="labelRequired" Text="Mês de Referência:"></asp:Label>
                                            </td>      
                                            <td>
                                                <dxe:ASPxDateEdit ID="textDataReferencia1" runat="server" ClientInstanceName="textDataReferencia1" EditFormat="Custom" EditFormatString="MM/yyyy"/>
                                            </td>                                                                                                      

                                            <td>                            
                                                <div id="reportLinkButton1" class="linkButton linkButtonNoBorder">
                                                    <asp:LinkButton ID="btnExcel1" runat="server" Font-Overline="false" CssClass="btnRun"
                                                        OnClientClick="btnExportaClick('OperacaoRF');return false;"><asp:Literal ID="Literal1" runat="server" Text="Exportar"/>
                                                        <div></div>
                                                    </asp:LinkButton>
                                                </div>                        
                                            </td>
                                        </tr>    
                                        <tr height="30px">
                                            <td>&nbsp;</td>
                                        </tr>
                                        <tr>                                                                                                                     
                                            <td class="td_Label_Longo">                
                                                <asp:Label ID="labelPosicao" runat="server" CssClass="labelNormal" Text="Posição Renda Fixa" Font-Bold="true"></asp:Label>
                                            </td>                  
                                                                                                                      
                                            <td width="30px">                   
                                                &nbsp;
                                             </td>   
                                                                                                                                                           
                                            <td class="td_Label">                
                                                <asp:Label ID="labelDataReferencia2" runat="server" CssClass="labelRequired" Text="Data de Referência:"></asp:Label>
                                            </td>                        
                                            
                                            <td>
                                                <dxe:ASPxDateEdit ID="textDataReferencia2" runat="server" ClientInstanceName="textDataReferencia2" EditFormat="Custom" EditFormatString="dd/MM/yyyy"/>
                                            </td>                                                                                                      

                                            <td>                            
                                                <div id="reportLinkButton2" class="linkButton linkButtonNoBorder">
                                                    <asp:LinkButton ID="btnExcel2" runat="server" Font-Overline="false" CssClass="btnRun"
                                                        OnClientClick="btnExportaClick('PosicaoRF');return false;"><asp:Literal ID="Literal2" runat="server" Text="Exportar"/>
                                                        <div></div>
                                                    </asp:LinkButton>
                                                </div>                        
                                            </td>
                                        </tr>    
                                        <tr height="30px">
                                            <td>&nbsp;</td>
                                        </tr>
                                        <tr height="30px">
                                            <td>&nbsp;</td>
                                        </tr>
                                        <tr height="30px">
                                            <td>&nbsp;</td>
                                        </tr>                                                                             
                                    </table>                                                           
                                </ContentTemplate>
                            </asp:UpdatePanel>         
                            </td>
                            
                            <td>
                            </td>
                        </tr>         
                     </table>    
                </div>
            </div>
        </div>
        </div>        
</div>        

<div id="divImportacao" runat="server" visible="false">
        
        <div class="divPanel divPanelNew">                
        <div style="background-image: none !important;">
            <div style="white-space: nowrap;
	                    text-align: center;
	                    font: bold 11px Tahoma, Arial;
	                    color: #333333;
	                    background-image: url(  '../imagens/layout/nbHeaderBack.gif' );
	                    background-repeat: repeat-x;
	                    background-position: top;
	                    border-left-style: none;
	                    border-right-style: none;
	                    border-top-style: none;
	                    border-bottom: solid 1px #D5D5D5;
	                    padding: 3px 10px 3px 5px;">	                    
                <asp:Label ID="lblHeader1" runat="server" Text="Importação"/>
            </div>
            <div style="width: 720px; margin: 0 auto;">
                <div class="reportFilter">
                    
                    <table border="0">         
                        <tr>
                            <td>
                             <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                                <ContentTemplate>
                                    <table>
                                        <tr>
                                        <td>&nbsp;</td>
                                        </tr>
                                    </table>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                            </td>

                            <td>
                            </td>
                        </tr>
                     </table>
                </div>
            </div>
        </div>
        </div>

</div>
        
</asp:Panel>  
    
        <dxlp:ASPxLoadingPanel ID="LoadingPanel1" runat="server" Text="Exportando arquivo, aguarde..." ClientInstanceName="LoadingPanel1" Modal="true" />      
        
    </form>
</body>
</html>
