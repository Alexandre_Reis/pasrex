﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Web.UI.HtmlControls;
using Financial.Common;
using System.Collections.Generic;
using Financial.Util;
using System.Text;
using DevExpress.Web;
using System.Drawing;
using Financial.Bolsa;
using Financial.Web.Common;
using Financial.Security;
using Financial.Web.Util;
using Financial.Security.Enums;
using Financial.InterfacesCustom;
using Financial.Fundo;
using System.Xml;
using Financial.Export;
using System.IO;
using Bytescout.Spreadsheet;
using Financial.Integracao.Excel.Util;
using Financial.Relatorio;
using DevExpress.Utils;
using DevExpress.Web.Data;
using System.Globalization;
using Financial.Investidor;
using Financial.Investidor.Enums;
using Financial.RendaFixa;
using Financial.RendaFixa.Enums;
using Financial.Bolsa.Enums;
using Financial.Fundo.Enums;
using System.Xml.Serialization;
using System.Net;
using Financial.WebConfigConfiguration;
using Financial.Fundo.Exportacao;
using System.ComponentModel;
using Financial.InterfacesCustom;

public partial class InterfacesCustom_Portopar : CadastroBasePage
{         
    // Controla Exibição de Importação ou Exportação
    private Tipo tipo;
    private ApenasSeguranca apenasSeguranca;

    enum ApenasSeguranca
    {
        Sim = 1
    }

    enum Tipo
    {
        Importacao = 0,
        Exportacao = 1
    }

    enum OwnerExportacao
    {
        PortoPar = 4
    }

    enum TipoExportacaoPortoPar
    {
        IntegralTrust = 0,
        Risco = 1,
        Rentabilidades = 2,
        RiscoExcel = 3,
        RentabilidadesExcel = 4
    }

    /// <summary>
    /// Controla Exibição de Importação ou Exportação
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    new protected void panelEdicao_Load(object sender, EventArgs e)
    {
        Control c1 = this.FindControl("divExportacao");
        Control c2 = this.FindControl("divImportacao");

        if (this.tipo == Tipo.Exportacao)
        {
            if (c1 != null) { c1.Visible = true; }
            if (c2 != null) { c2.Visible = false; }
        }
        else if (this.tipo == Tipo.Importacao)
        {
            if (c1 != null) { c1.Visible = false; }
            if (c2 != null) { c2.Visible = true; }
        }
    }

    new protected void Page_Load(object sender, EventArgs e)
    {
        this.tipo = (Tipo)Enum.Parse(typeof(Tipo), Request.QueryString["param"].ToString());
        if (Request.QueryString["ApenasSeguranca"] != null)
        {
            this.apenasSeguranca = (ApenasSeguranca)Enum.Parse(typeof(ApenasSeguranca), Request.QueryString["ApenasSeguranca"].ToString());
        }

        this.HasFiltro = true;
        this.HasPopupCarteira = true;
        this.HasSummary = false; // Não tem Total

        if (!Page.IsPostBack) 
        {
            CaixaOnline caixaOnline = new CaixaOnline();

            caixaOnline.LimparDataTableCaixaOnline();
            
            caixaOnline.CriarDataTableCaixaOnLine();

            gridCaixaOnline.DataBind();
        }
        //desabilita botão Atualizar
        //this.LinkButton7.Enabled = false;
        
        this.gridCaixaOnline.Styles.AlternatingRow.Enabled = DefaultBoolean.True;
        this.gridCaixaOnline.Styles.Cell.Wrap = DefaultBoolean.False;

        // Grid Cadastro está dentro de um tabControl
        base.Page_Load(sender, e, this.gridCadastro);
        //

        this.gridCadastro.SettingsPager.PageSize = 10;
        this.gridCadastro.Settings.VerticalScrollableHeight = 270;

        //this.gridCadastro.Settings.ShowVerticalScrollBar = true;       
        //this.gridCadastro.Settings.VerticalScrollBarStyle = GridViewVerticalScrollBarStyle.Virtual;
        ////
        //this.gridCadastro.SettingsPager.PageSize = 15;
        //this.gridCadastro.DataSourceForceStandardPaging = true;      

        //base.Page_Load(sender, e, this.gridCadastro1);        
        this.InicializaGridPermissaoPortalCliente();
        //
        //
        //this.tabPortoPar.TabPages[0].Visible = true;
        //this.tabPortoPar.TabPages[1].Visible = true;
        //this.tabPortoPar.TabPages[2].Visible = true;
        //this.tabPortoPar.TabPages[3].Visible = true;
        ////
        //if (this.apenasSeguranca == ApenasSeguranca.Sim) {
        //    this.tabPortoPar.TabPages[0].Visible = false;
        //    this.tabPortoPar.TabPages[1].Visible = false;
        //    this.tabPortoPar.TabPages[2].Visible = false;
        //    this.tabPortoPar.TabPages[3].Visible = true;
        //}

        // Configurações da aba Carteira/Grupo Econômico
        this.InicializaGridCarteiraGrupoEconomico();

        // Configurações da aba DePara_IntegralTrust
        this.InicializaGridDePara_IntegralTrust();

    }

    #region DataSources

    protected void EsDSTabelaBandaCarteira_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        TabelaBandaCarteiraQuery tabelaBandaCarteiraQuery = new TabelaBandaCarteiraQuery("T");
        CarteiraQuery carteiraQuery = new CarteiraQuery("C");

        if (this.textDataInicio.Text != "")
        {
            tabelaBandaCarteiraQuery.Where(tabelaBandaCarteiraQuery.DataReferencia >= this.textDataInicio.Text);
        }

        if (this.textDataFim.Text != "")
        {
            tabelaBandaCarteiraQuery.Where(tabelaBandaCarteiraQuery.DataReferencia <= this.textDataFim.Text);
        }

        if (this.btnEditCodigoCarteiraFiltro.Text != "")
        {
            tabelaBandaCarteiraQuery.Where(tabelaBandaCarteiraQuery.IdCarteira == Convert.ToInt32(this.btnEditCodigoCarteiraFiltro.Text));
        }

        tabelaBandaCarteiraQuery.Select(tabelaBandaCarteiraQuery, carteiraQuery.Apelido);
        tabelaBandaCarteiraQuery.InnerJoin(carteiraQuery).On(tabelaBandaCarteiraQuery.IdCarteira == carteiraQuery.IdCarteira);
        //
        tabelaBandaCarteiraQuery.OrderBy(tabelaBandaCarteiraQuery.IdCarteira.Ascending, tabelaBandaCarteiraQuery.DataReferencia.Descending);
        //
        TabelaBandaCarteiraCollection coll = new TabelaBandaCarteiraCollection();
        coll.Load(tabelaBandaCarteiraQuery);

        e.Collection = coll;
    }

    protected void EsDSCarteira_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        CarteiraCollection coll = new CarteiraCollection();
        coll.BuscaCarteirasComAcessoTotal(Context.User.Identity.Name);

        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }

    protected void EsDSGrupoEconomico_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        GrupoEconomicoCollection coll = new GrupoEconomicoCollection();
        coll.LoadAll();

        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }

    #endregion

    #region TabelaBandaCarteira
    /// <summary>
    /// 
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void ASPxCallback1_Callback(object source, DevExpress.Web.CallbackEventArgs e)
    {
        string nome = "";
        if (!String.IsNullOrEmpty(e.Parameter) && e.Parameter != "null")
        {
            int idCarteira = Convert.ToInt32(e.Parameter);
            Carteira carteira = new Carteira();
            carteira.LoadByPrimaryKey(idCarteira);

            if (carteira.str.Apelido != "")
            {
                if (carteira.IsAtivo)
                {
                    PermissaoCliente permissaoCliente = new PermissaoCliente();
                    if (permissaoCliente.RetornaAcessoCliente(idCarteira, HttpContext.Current.User.Identity.Name))
                    {
                        nome = carteira.str.Apelido;
                    }
                    else
                    {
                        nome = "no_access";
                    }
                }
                else
                {
                    nome = "no_active";
                }
            }
        }
        e.Result = nome;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void callbackErro_Callback(object source, DevExpress.Web.CallbackEventArgs e)
    {
        e.Result = "";
        ASPxSpinEdit btnEditCodigoCarteira = gridCadastro.FindEditFormTemplateControl("btnEditCodigoCarteira") as ASPxSpinEdit;
        ASPxDateEdit textDataReferencia = gridCadastro.FindEditFormTemplateControl("textDataReferencia") as ASPxDateEdit;
        ASPxSpinEdit textLimiteSuperior = gridCadastro.FindEditFormTemplateControl("textLimiteSuperior") as ASPxSpinEdit;
        ASPxSpinEdit textLimiteInferior = gridCadastro.FindEditFormTemplateControl("textLimiteInferior") as ASPxSpinEdit;

        #region Campos obrigatórios
        List<Control> controles = new List<Control>();
        controles.Add(btnEditCodigoCarteira);
        controles.Add(textDataReferencia);
        controles.Add(textLimiteSuperior);
        controles.Add(textLimiteInferior);

        if (base.TestaObrigatorio(controles) != "")
        {
            e.Result = "Campos com * são obrigatórios!";
            return;
        }
        #endregion

        if (gridCadastro.IsNewRowEditing)
        {
            int idCarteira = Convert.ToInt32(btnEditCodigoCarteira.Text);
            DateTime dataReferencia = Convert.ToDateTime(textDataReferencia.Text);

            TabelaBandaCarteira tabelaBandaCarteira = new TabelaBandaCarteira();
            if (tabelaBandaCarteira.LoadByPrimaryKey(idCarteira, dataReferencia))
            {
                e.Result = "Registro já existente.";
            }
        }
    }

    protected void callbackAdd_Callback(object source, DevExpress.Web.CallbackEventArgs e)
    {
        SalvarNovo();
    }


    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void textData_Init(object sender, EventArgs e)
    {
        if (!gridCadastro.IsNewRowEditing)
            (sender as ASPxDateEdit).Enabled = false;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnEditCodigoCarteira_Load(object sender, EventArgs e)
    {
        if (!gridCadastro.IsNewRowEditing)
        {
            (sender as ASPxSpinEdit).Enabled = false;
            (sender as ASPxSpinEdit).BackColor = Color.FromName("#EBEBEB");
        }
    }

    /// <summary>
    /// Bot]ao OK+ desaperece no Update
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnOKAdd_Init(object sender, EventArgs e)
    {
        if (!gridCadastro.IsNewRowEditing)
            (sender as LinkButton).Visible = false;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_CustomUnboundColumnData(object sender, DevExpress.Web.ASPxGridViewColumnDataEventArgs e)
    {
        if (e.Column.FieldName == "CompositeKey")
        {
            //Compose a primary key value
            string data = Convert.ToString(e.GetListSourceFieldValue(TabelaBandaCarteiraMetadata.ColumnNames.DataReferencia));
            int idCarteira = Convert.ToInt32(e.GetListSourceFieldValue(TabelaBandaCarteiraMetadata.ColumnNames.IdCarteira));
            e.Value = data + idCarteira.ToString();
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_RowUpdating(object sender, DevExpress.Web.Data.ASPxDataUpdatingEventArgs e)
    {
        TabelaBandaCarteira tabelaBandaCarteira = new TabelaBandaCarteira();
        //
        ASPxSpinEdit btnEditCodigoCarteira = gridCadastro.FindEditFormTemplateControl("btnEditCodigoCarteira") as ASPxSpinEdit;
        ASPxDateEdit textDataReferencia = gridCadastro.FindEditFormTemplateControl("textDataReferencia") as ASPxDateEdit;
        ASPxSpinEdit textLimiteInferior = gridCadastro.FindEditFormTemplateControl("textLimiteInferior") as ASPxSpinEdit;
        ASPxSpinEdit textLimiteSuperior = gridCadastro.FindEditFormTemplateControl("textLimiteSuperior") as ASPxSpinEdit;

        int idCarteira = Convert.ToInt32(btnEditCodigoCarteira.Text);
        DateTime dataReferencia = Convert.ToDateTime(textDataReferencia.Text);
        decimal limiteInferior = Convert.ToDecimal(textLimiteInferior.Text);
        decimal limiteSuperior = Convert.ToDecimal(textLimiteSuperior.Text);

        if (tabelaBandaCarteira.LoadByPrimaryKey(idCarteira, dataReferencia))
        {
            tabelaBandaCarteira.LimiteInferior = limiteInferior;
            tabelaBandaCarteira.LimiteSuperior = limiteSuperior;
            //
            tabelaBandaCarteira.Save();

            #region Log do Processo
            HistoricoLog historicoLog = new HistoricoLog();
            historicoLog.InsereHistoricoLog(DateTime.Now,
                                            DateTime.Now,
                                            "Cadastro de TabelaBandaCarteira - Operacao: Update TabelaBandaCarteira: " + idCarteira + "; " + dataReferencia + UtilitarioWeb.ToString(tabelaBandaCarteira),
                                            HttpContext.Current.User.Identity.Name,
                                            UtilitarioWeb.GetIP(Request),
                                            "",
                                            HistoricoLogOrigem.Outros);
            #endregion
        }

        e.Cancel = true;
        gridCadastro.CancelEdit();
        gridCadastro.DataBind();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_RowInserting(object sender, DevExpress.Web.Data.ASPxDataInsertingEventArgs e)
    {
        this.SalvarNovo();

        e.Cancel = true;
        gridCadastro.CancelEdit();
    }

    private void SalvarNovo()
    {

        TabelaBandaCarteira tabelaBandaCarteira = new TabelaBandaCarteira();
        //
        ASPxSpinEdit btnEditCodigoCarteira = gridCadastro.FindEditFormTemplateControl("btnEditCodigoCarteira") as ASPxSpinEdit;
        ASPxDateEdit textDataReferencia = gridCadastro.FindEditFormTemplateControl("textDataReferencia") as ASPxDateEdit;
        ASPxSpinEdit textLimiteInferior = gridCadastro.FindEditFormTemplateControl("textLimiteInferior") as ASPxSpinEdit;
        ASPxSpinEdit textLimiteSuperior = gridCadastro.FindEditFormTemplateControl("textLimiteSuperior") as ASPxSpinEdit;

        int idCarteira = Convert.ToInt32(btnEditCodigoCarteira.Text);
        DateTime dataReferencia = Convert.ToDateTime(textDataReferencia.Text);
        decimal limiteInferior = Convert.ToDecimal(textLimiteInferior.Text);
        decimal limiteSuperior = Convert.ToDecimal(textLimiteSuperior.Text);

        tabelaBandaCarteira.IdCarteira = idCarteira;
        tabelaBandaCarteira.DataReferencia = dataReferencia;
        tabelaBandaCarteira.LimiteInferior = limiteInferior;
        tabelaBandaCarteira.LimiteSuperior = limiteSuperior;
        //
        tabelaBandaCarteira.Save();

        #region Log do Processo
        HistoricoLog historicoLog = new HistoricoLog();
        historicoLog.InsereHistoricoLog(DateTime.Now,
                                        DateTime.Now,
                                        "Cadastro de TabelaBandaCarteira - Operacao: Insert TabelaBandaCarteira: " + tabelaBandaCarteira.IdCarteira + "; " + tabelaBandaCarteira.DataReferencia + UtilitarioWeb.ToString(tabelaBandaCarteira),
                                        HttpContext.Current.User.Identity.Name,
                                        UtilitarioWeb.GetIP(Request),
                                        "",
                                        HistoricoLogOrigem.Outros);
        #endregion
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_CustomCallback(object sender, DevExpress.Web.ASPxGridViewCustomCallbackEventArgs e)
    {
        if (e.Parameters == "btnDelete")
        {
            List<object> keyValuesId = gridCadastro.GetSelectedFieldValues(TabelaBandaCarteiraMetadata.ColumnNames.IdCarteira);
            List<object> keyValuesData = gridCadastro.GetSelectedFieldValues(TabelaBandaCarteiraMetadata.ColumnNames.DataReferencia);

            for (int i = 0; i < keyValuesData.Count; i++)
            {
                int idCarteira = Convert.ToInt32(keyValuesId[i]);
                DateTime dataReferencia = Convert.ToDateTime(keyValuesData[i]);

                TabelaBandaCarteira tabelaBandaCarteira = new TabelaBandaCarteira();
                if (tabelaBandaCarteira.LoadByPrimaryKey(idCarteira, dataReferencia))
                {
                    //
                    TabelaBandaCarteira tabelaBandaCarteiraClone = (TabelaBandaCarteira)Utilitario.Clone(tabelaBandaCarteira);
                    //                    
                    tabelaBandaCarteira.MarkAsDeleted();
                    tabelaBandaCarteira.Save();

                    #region Log do Processo
                    HistoricoLog historicoLog = new HistoricoLog();
                    historicoLog.InsereHistoricoLog(DateTime.Now,
                                                    DateTime.Now,
                                                    "Cadastro de TabelaBandaCarteira - Operacao: Delete TabelaBandaCarteira: " + idCarteira + "; " + dataReferencia + UtilitarioWeb.ToString(tabelaBandaCarteiraClone),
                                                    HttpContext.Current.User.Identity.Name,
                                                    UtilitarioWeb.GetIP(Request),
                                                    "",
                                                    HistoricoLogOrigem.Outros);
                    #endregion
                }
            }
        }

        gridCadastro.DataBind();
        gridCadastro.Selection.UnselectAll();
        gridCadastro.CancelEdit();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    new protected void gridCadastro_CustomJSProperties(object sender, ASPxGridViewClientJSPropertiesEventArgs e)
    {
        base.gridCadastro_CustomJSProperties(sender, e);

        ASPxGridView gridView = sender as ASPxGridView;
        if (gridView.IsEditing)
        {
            ASPxDateEdit textData = gridCadastro.FindEditFormTemplateControl("textData") as ASPxDateEdit;
            e.Properties["cpTextData"] = textData.ClientID;

            ASPxSpinEdit textFator = gridCadastro.FindEditFormTemplateControl("textFator") as ASPxSpinEdit;
            e.Properties["cpTextFator"] = textFator.ClientID;
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    new protected void gridCadastro_PreRender(object sender, EventArgs e)
    {
        this.FocaCampoGrid("btnEditCodigoCarteira", "textLimiteInferior");
        base.gridCadastro_PreRender(sender, e);

        Label labelFiltro = gridCadastro.FindStatusBarTemplateControl("labelFiltro") as Label;
        StringBuilder texto = new StringBuilder();
        //
        texto.Append("");

        if (btnEditCodigoCarteiraFiltro != null && btnEditCodigoCarteiraFiltro.Text != "")
        {
            texto.Append(" Carteira: ").Append(btnEditCodigoCarteiraFiltro.Text);
        }
        if (textDataInicio.Text != "")
        {
            if (texto.Length > 0)
            {
                texto.Append(" |");
            }
            texto.Append(" Data Início >= ").Append(this.textDataInicio.Text.Substring(0, 10));
        }
        if (textDataFim.Text != "")
        {
            if (texto.Length > 0)
            {
                texto.Append(" |");
            }

            texto.Append(" Data Fim <= ").Append(this.textDataFim.Text.Substring(0, 10));
        }

        labelFiltro.Text = texto.ToString();
    }
    #endregion

    #region Exportação
    protected void callbackExporta_Callback(object source, DevExpress.Web.CallbackEventArgs e)
    {
        e.Result = "";

        string arquivo1 = "";
        string arquivo2 = "";


        if (e.Parameter == "IntegralTrust")
        {
            if (this.textDataIntegralTrust.Text == "")
            {
                e.Result = "Data Obrigatório.";
                return;
            }
            #region Trata arquivo1 e arquivo2
            arquivo1 = this.Arq1.Text;
            if (String.IsNullOrEmpty(arquivo1))
            {
                arquivo1 = "Arquivo1.xls";
            }
            if (!arquivo1.Contains(".xls") && !arquivo1.Contains(".xlsx"))
            {
                arquivo1 += ".xls";
            }

            arquivo2 = this.Arq2.Text;
            if (String.IsNullOrEmpty(arquivo2))
            {
                arquivo2 = "Arquivo2.xls";
            }
            if (!arquivo2.Contains(".xls") && !arquivo2.Contains(".xlsx"))
            {
                arquivo2 += ".xls";
            }
            #endregion
        }
        else if (e.Parameter == "Risco")
        {
            if (this.textDataRisco.Text == "")
            {
                e.Result = "Data Obrigatório.";
                return;
            }
        }
        else if (e.Parameter == "Rentabilidades")
        {
            if (this.textDataRentabilidade.Text == "")
            {
                e.Result = "Data Obrigatório.";
                return;
            }
        }
        else if (e.Parameter == "RiscoExcel")
        {
            if (this.textDataRisco.Text == "")
            {
                e.Result = "Data Obrigatório.";
                return;
            }
        }
        else if (e.Parameter == "RentabilidadesExcel")
        {
            if (this.textDataRentabilidade.Text == "")
            {
                e.Result = "Data Obrigatório.";
                return;
            }
        }

        try
        {
            this.SelecionaExportacao(e.Parameter, arquivo1, arquivo2);
        }
        catch (Exception erro)
        {
            e.Result = erro.Message;
            return;
        }

        string redirect = "ExibeExportacaoCustom.aspx?Owner=" + (int)OwnerExportacao.PortoPar;
        switch (e.Parameter)
        {
            case "IntegralTrust":
                redirect += "&Tipo=" + (int)TipoExportacaoPortoPar.IntegralTrust;
                break;
            case "Risco":
                redirect += "&Tipo=" + (int)TipoExportacaoPortoPar.Risco;
                break;
            case "Rentabilidades":
                redirect += "&Tipo=" + (int)TipoExportacaoPortoPar.Rentabilidades;
                break;
            case "RiscoExcel":
                redirect += "&Tipo=" + (int)TipoExportacaoPortoPar.RiscoExcel;
                break;
            case "RentabilidadesExcel":
                redirect += "&Tipo=" + (int)TipoExportacaoPortoPar.RentabilidadesExcel;
                break;
        }

        string newLocation = HttpContext.Current.Request.Url.AbsolutePath.Replace("Portopar.aspx", redirect);
        e.Result = "redirect:" + newLocation;
    }

    /// <summary>
    /// Seleciona o relatorio a ser executado
    /// </summary>
    /// <param name="tipo">Arquivo que se quer exportar</param>
    /// <param name="nomeArquivo1">Nome do Arquivo excel 1 que se quer exportar</param>
    /// <param name="nomeArquivo2">Nome do Arquivo excel 2 que se quer exportar</param>
    private void SelecionaExportacao(string tipo, string nomeArquivo1, string nomeArquivo2)
    {
        Response.BufferOutput = true;
        //
        switch (tipo)
        {

            #region IntegralTrust
            case "IntegralTrust":
                Dictionary<string, MemoryStream> retIntegralTrust = new Dictionary<string, MemoryStream>();
                //
                this.ProcessaIntegralTrust(out retIntegralTrust, nomeArquivo1, nomeArquivo2);

                // Salva Memory Stream do Arquivo Excel na Session                
                Session["streamIntegralTrust"] = retIntegralTrust;
                break;
            #endregion

            #region Risco
            case "Risco":
                MemoryStream msReportRisco = new MemoryStream();

                // msReportRisco armazena o report
                try
                {
                    this.ProcessaRisco(out msReportRisco);
                }
                catch (Exception exp1)
                {
                    HistoricoLog historicoLog = new HistoricoLog();
                    string message = exp1.Message + "-" + exp1.Source + "-" + exp1.InnerException + "-" + exp1.StackTrace;
                    historicoLog.InsereHistoricoLog(DateTime.Now, DateTime.Now, message, "", "", "", HistoricoLogOrigem.Outros);
                    throw new Exception(message);
                }

                // Salva Memory Stream do Arquivo Excel na Session                
                Session["msReportRisco"] = msReportRisco;
                break;

            case "RiscoExcel":
                MemoryStream msReportRiscoExcel = new MemoryStream();

                // msReportRisco armazena o report
                try
                {
                    this.ProcessaRiscoExcel(out msReportRiscoExcel);
                }
                catch (Exception exp1)
                {
                    HistoricoLog historicoLog = new HistoricoLog();
                    string message = exp1.Message + "-" + exp1.Source + "-" + exp1.InnerException + "-" + exp1.StackTrace;
                    historicoLog.InsereHistoricoLog(DateTime.Now, DateTime.Now, message, "", "", "", HistoricoLogOrigem.Outros);
                    throw new Exception(message);
                }

                // Salva Memory Stream do Arquivo Excel na Session                
                Session["msReportRiscoExcel"] = msReportRiscoExcel;
                break;
            #endregion

            #region Rentabilidades
            case "Rentabilidades":
                MemoryStream msReportRentabilidade = new MemoryStream(); ;

                this.ProcessaRentabilidade(out msReportRentabilidade);

                //// Salva Memory Stream do Arquivo Excel na Session                
                Session["msReportRentabilidade"] = msReportRentabilidade;
                break;

            case "RentabilidadesExcel":
                MemoryStream msReportRentabilidadeExcel = new MemoryStream(); ;

                this.ProcessaRentabilidadeExcel(out msReportRentabilidadeExcel);

                //// Salva Memory Stream do Arquivo Excel na Session                
                Session["msReportRentabilidadeExcel"] = msReportRentabilidadeExcel;
                break;
            #endregion
        }
    }

    /// <summary>
    /// Processa Exportação de Rentabilidade
    /// </summary>
    /// <param name="msReportRisco"></param>
    private void ProcessaRentabilidade(out MemoryStream msReportRentabilidade)
    {
        msReportRentabilidade = new MemoryStream(); // Retorno com o conteúdo do report

        DateTime dataRentabilidade = Convert.ToDateTime(textDataRentabilidade.Value);

        ReportRentabilPorto report = new ReportRentabilPorto(dataRentabilidade);

        report.ExportToPdf(msReportRentabilidade);
        msReportRentabilidade.Seek(0, SeekOrigin.Begin);
    }

    /// <summary>
    /// Processa Exportação de Rentabilidade
    /// </summary>
    /// <param name="msReportRisco"></param>
    private void ProcessaRentabilidadeExcel(out MemoryStream msReportRentabilidadeExcel)
    {
        msReportRentabilidadeExcel = new MemoryStream(); // Retorno com o conteúdo do report

        DateTime dataRentabilidade = Convert.ToDateTime(textDataRentabilidade.Value);

        ReportRentabilPorto report = new ReportRentabilPorto(dataRentabilidade);

        report.ExportToXls(msReportRentabilidadeExcel);
        msReportRentabilidadeExcel.Seek(0, SeekOrigin.Begin);
    }

    /// <summary>
    /// Processa Exportação de Risco
    /// </summary>
    /// <param name="msReportRisco">Contém o SubReport Risco</param>
    private void ProcessaRisco(out MemoryStream msReportRisco)
    {
        msReportRisco = new MemoryStream(); // Retorno com o conteúdo do report

        int? grupoEconomico = null;
        if (dropGrupoEconomico.SelectedIndex > -1)
        {
            grupoEconomico = Convert.ToInt32(dropGrupoEconomico.SelectedItem.Value);
        }
        int? carteira = null;
        if (textCarteira.Text != "")
        {
            carteira = Convert.ToInt32(textCarteira.Text);
        }
        DateTime dataRisco = Convert.ToDateTime(textDataRisco.Value);

        ReportRiscoPortopar report = new ReportRiscoPortopar(grupoEconomico, dataRisco, carteira);

        report.ExportToPdf(msReportRisco);
        msReportRisco.Seek(0, SeekOrigin.Begin);
    }

    /// <summary>
    /// Processa Exportação de Risco
    /// </summary>
    /// <param name="msReportRisco">Contém o SubReport Risco</param>
    private void ProcessaRiscoExcel(out MemoryStream msReportRisco)
    {
        msReportRisco = new MemoryStream(); // Retorno com o conteúdo do report

        int? grupoEconomico = null;
        if (dropGrupoEconomico.SelectedIndex > -1)
        {
            grupoEconomico = Convert.ToInt32(dropGrupoEconomico.SelectedItem.Value);
        }
        int? carteira = null;
        if (textCarteira.Text != "")
        {
            carteira = Convert.ToInt32(textCarteira.Text);
        }
        DateTime dataRisco = Convert.ToDateTime(textDataRisco.Value);

        ReportRiscoPortopar report = new ReportRiscoPortopar(grupoEconomico, dataRisco, carteira);

        report.ExportToXls(msReportRisco);
        msReportRisco.Seek(0, SeekOrigin.Begin);
    }



    #region Integral Trust
    /// <summary>
    /// Gera duas Planilhas Excel
    /// </summary>
    /// <param name="dicMStream">Dicionario de memory Streams cada uma representando 1 arquivo e seus respectivos nomes de arquivos</param>
    /// <param name="nomeArquivo1">Nome do Arquivo excel 1 que se quer exportar</param>
    /// <param name="nomeArquivo2">Nome do Arquivo excel 2 que se quer exportar</param>
    private void ProcessaIntegralTrust(out Dictionary<string, MemoryStream> dicMStream, string nomeArquivo1, string nomeArquivo2)
    {
        List<int> lstIdCarteira = new List<int>();

        if (!string.IsNullOrEmpty(dropCarteira.Text))
        {
            foreach (string idCarteira in dropCarteira.Text.Split(','))
                lstIdCarteira.Add(Convert.ToInt32(idCarteira));
        }
        
        // Dados da Planilha
        IntegralTrust t = new IntegralTrust(Convert.ToDateTime(this.textDataIntegralTrust.Text), lstIdCarteira);

        string pathModelo = AppDomain.CurrentDomain.BaseDirectory + "\\LayoutCustom\\PortoPar";
        //
        string pathModelo1 = String.Format("{0}\\PosicaoRendaFixa.xls", pathModelo);
        string pathModelo2 = String.Format("{0}\\Posicao_OpcoesAcoes_OpcoesFuturos.xls", pathModelo);

        dicMStream = new Dictionary<string, MemoryStream>();

        #region PosicaoRendaFixa / RendaVariavel, Swap e Futuros
        MemoryStream m1 = this.GeraPosicaoRendaFixa(pathModelo1, t.listaVista, nomeArquivo1);
        //dicMStream.Add(Path.GetFileName(pathModelo1), m1);
        dicMStream.Add(nomeArquivo1, m1);
        #endregion

        #region Posicao Opções_Acoes / Opcões Futuros
        MemoryStream m2 = this.GeraPosicaoOpcoesAcoes(pathModelo2, t.listaOpcao, nomeArquivo2);
        //dicMStream.Add(Path.GetFileName(pathModelo2), m2);        
        dicMStream.Add(nomeArquivo2, m2);
        #endregion
    }

    /// <summary>
    /// Retorna uma Planilha de Posição RendaFixa
    /// </summary>
    /// <param name="pathModelo">modelo da planilha a ser carregado</param>
    /// <param name="dados">dados das linhas do excel</param>
    /// <returns></returns>
    private MemoryStream GeraPosicaoRendaFixa(string pathModelo, List<IntegralTrust.Vista> dados, string nomeArquivo1)
    {
        MemoryStream msExcel = new MemoryStream();

        Spreadsheet document = new Bytescout.Spreadsheet.Spreadsheet();

        document.LoadFromFile(pathModelo);

        Worksheet worksheet = document.Workbook.Worksheets[0];
        //
        worksheet.ViewOptions.ShowZeroValues = true;

        #region Preenche dados Planilha

        for (int i = 0; i < dados.Count; i++)
        {
            int k = i + 2;
            worksheet.Cell("A" + k).ValueAsDateTime = dados[i].data;
            worksheet.Cell("B" + k).ValueAsString = dados[i].empresa.Trim();
            worksheet.Cell("C" + k).ValueAsString = dados[i].AP.Trim();
            worksheet.Cell("D" + k).ValueAsString = dados[i].produto.Trim();
            worksheet.Cell("E" + k).ValueAsString = dados[i].grupo.Trim();
            worksheet.Cell("F" + k).ValueAsString = dados[i].cod_Bovespa.Trim();
            worksheet.Cell("G" + k).ValueAsString = dados[i].venc;
            worksheet.Cell("H" + k).ValueAsString = "";
            worksheet.Cell("I" + k).ValueAsInteger = Convert.ToInt32(dados[i].qtde);
            worksheet.Cell("J" + k).ValueAsString = dados[i].index.Trim();
            worksheet.Cell("K" + k).Value = Convert.ToDecimal(dados[i].abs_MTM.ToString("N2"));
            worksheet.Cell("L" + k).Value = Convert.ToDecimal(dados[i].valor_MTM.ToString("N2"));

            // Alinhamento e Fonte
            worksheet.Cell("A" + k).AlignmentHorizontal = Bytescout.Spreadsheet.Constants.AlignmentHorizontal.Left;
            worksheet.Cell("B" + k).AlignmentHorizontal = Bytescout.Spreadsheet.Constants.AlignmentHorizontal.Left;
            worksheet.Cell("C" + k).AlignmentHorizontal = Bytescout.Spreadsheet.Constants.AlignmentHorizontal.Left;
            worksheet.Cell("D" + k).AlignmentHorizontal = Bytescout.Spreadsheet.Constants.AlignmentHorizontal.Left;
            worksheet.Cell("E" + k).AlignmentHorizontal = Bytescout.Spreadsheet.Constants.AlignmentHorizontal.Left;
            worksheet.Cell("F" + k).AlignmentHorizontal = Bytescout.Spreadsheet.Constants.AlignmentHorizontal.Left;
            worksheet.Cell("G" + k).AlignmentHorizontal = Bytescout.Spreadsheet.Constants.AlignmentHorizontal.Left;
            worksheet.Cell("H" + k).AlignmentHorizontal = Bytescout.Spreadsheet.Constants.AlignmentHorizontal.Left;
            worksheet.Cell("I" + k).AlignmentHorizontal = Bytescout.Spreadsheet.Constants.AlignmentHorizontal.Left;
            worksheet.Cell("J" + k).AlignmentHorizontal = Bytescout.Spreadsheet.Constants.AlignmentHorizontal.Left;
            worksheet.Cell("K" + k).AlignmentHorizontal = Bytescout.Spreadsheet.Constants.AlignmentHorizontal.Left;
            worksheet.Cell("L" + k).AlignmentHorizontal = Bytescout.Spreadsheet.Constants.AlignmentHorizontal.Left;
            //
            worksheet.Rows[k].Font = new Font("Arial", 8, FontStyle.Regular);
        }
        #endregion

        #region Esconde Planilha Demo do componente
        const string BYTESCOUT_WORKSHEET_NAME = "Bytescout Spreadsheet SDK";
        worksheet = document.Workbook.Worksheets.ByName(BYTESCOUT_WORKSHEET_NAME);
        if (worksheet == null)
        {
            worksheet = document.Workbook.Worksheets.Add(BYTESCOUT_WORKSHEET_NAME);
        }
        worksheet.Visible = Bytescout.Spreadsheet.Constants.SHEETVISIBILITY.Hidden;
        #endregion

        //
        document.SaveToStreamXLS(msExcel);

        /* Necessário voltar o ponteiro do arquivo para o Inicio */
        msExcel.Seek(0, SeekOrigin.Begin);
        //
        document.Close();
        //
        return msExcel;
    }

    /// <summary>
    /// Retorna uma Planilha de Posição de Opções de Ações e Opções de Futuros
    /// </summary>
    /// <param name="pathModelo">modelo da planilha a ser carregado</param>
    /// <param name="dados">dados das linhas do excel</param>
    /// <returns></returns>
    private MemoryStream GeraPosicaoOpcoesAcoes(string pathModelo, List<IntegralTrust.Opcao> dados, string nomeArquivo2)
    {
        MemoryStream msExcel = new MemoryStream();

        Spreadsheet document = new Bytescout.Spreadsheet.Spreadsheet();

        document.LoadFromFile(pathModelo);

        Worksheet worksheet = document.Workbook.Worksheets[0];
        //
        worksheet.ViewOptions.ShowZeroValues = true;

        #region Preenche dados Planilha

        for (int i = 0; i < dados.Count; i++)
        {
            int k = i + 2;
            worksheet.Cell("A" + k).ValueAsDateTime = dados[i].data;
            worksheet.Cell("B" + k).ValueAsString = dados[i].empresa.Trim();
            worksheet.Cell("C" + k).ValueAsString = dados[i].cod_Opcao.Trim();
            worksheet.Cell("D" + k).ValueAsString = dados[i].ativo_Obj.Trim();
            worksheet.Cell("E" + k).Value = Convert.ToDecimal(dados[i].spot.ToString("N2"));
            worksheet.Cell("F" + k).ValueAsInteger = Convert.ToInt32(dados[i].lote);
            worksheet.Cell("G" + k).ValueAsString = dados[i].tipo_Opcao.Trim();
            worksheet.Cell("H" + k).ValueAsString = dados[i].tipo_ATPA.Trim();
            worksheet.Cell("I" + k).ValueAsString = dados[i].vencimento.Trim();
            worksheet.Cell("J" + k).ValueAsInteger = Convert.ToInt32(dados[i].qtde);
            worksheet.Cell("K" + k).Value = Convert.ToDecimal(dados[i].premio.ToString("N2"));
            worksheet.Cell("L" + k).Value = Convert.ToDecimal(dados[i].preco_Exerc.ToString("N2"));
            worksheet.Cell("M" + k).Value = Convert.ToDecimal(dados[i].vl_Notacional.ToString("N2"));
            worksheet.Cell("N" + k).Value = Convert.ToDecimal(dados[i].valor_MTM.ToString("N2"));
            worksheet.Cell("O" + k).ValueAsString = dados[i].produto.Trim();

            // Alinhamento e Fonte
            worksheet.Cell("A" + k).AlignmentHorizontal = Bytescout.Spreadsheet.Constants.AlignmentHorizontal.Left;
            worksheet.Cell("B" + k).AlignmentHorizontal = Bytescout.Spreadsheet.Constants.AlignmentHorizontal.Left;
            worksheet.Cell("C" + k).AlignmentHorizontal = Bytescout.Spreadsheet.Constants.AlignmentHorizontal.Left;
            worksheet.Cell("D" + k).AlignmentHorizontal = Bytescout.Spreadsheet.Constants.AlignmentHorizontal.Left;
            worksheet.Cell("E" + k).AlignmentHorizontal = Bytescout.Spreadsheet.Constants.AlignmentHorizontal.Right;
            worksheet.Cell("F" + k).AlignmentHorizontal = Bytescout.Spreadsheet.Constants.AlignmentHorizontal.Right;
            worksheet.Cell("G" + k).AlignmentHorizontal = Bytescout.Spreadsheet.Constants.AlignmentHorizontal.Left;
            worksheet.Cell("H" + k).AlignmentHorizontal = Bytescout.Spreadsheet.Constants.AlignmentHorizontal.Left;
            worksheet.Cell("I" + k).AlignmentHorizontal = Bytescout.Spreadsheet.Constants.AlignmentHorizontal.Left;
            worksheet.Cell("J" + k).AlignmentHorizontal = Bytescout.Spreadsheet.Constants.AlignmentHorizontal.Right;
            worksheet.Cell("K" + k).AlignmentHorizontal = Bytescout.Spreadsheet.Constants.AlignmentHorizontal.Right;
            worksheet.Cell("L" + k).AlignmentHorizontal = Bytescout.Spreadsheet.Constants.AlignmentHorizontal.Right;
            worksheet.Cell("M" + k).AlignmentHorizontal = Bytescout.Spreadsheet.Constants.AlignmentHorizontal.Right;
            worksheet.Cell("N" + k).AlignmentHorizontal = Bytescout.Spreadsheet.Constants.AlignmentHorizontal.Right;
            worksheet.Cell("O" + k).AlignmentHorizontal = Bytescout.Spreadsheet.Constants.AlignmentHorizontal.Left;
            //
            worksheet.Rows[k].Font = new Font("Arial", 8, FontStyle.Regular);
        }
        #endregion

        #region Esconde Planilha Demo do componente
        const string BYTESCOUT_WORKSHEET_NAME = "Bytescout Spreadsheet SDK";
        worksheet = document.Workbook.Worksheets.ByName(BYTESCOUT_WORKSHEET_NAME);
        if (worksheet == null)
        {
            worksheet = document.Workbook.Worksheets.Add(BYTESCOUT_WORKSHEET_NAME);
        }
        worksheet.Visible = Bytescout.Spreadsheet.Constants.SHEETVISIBILITY.Hidden;
        #endregion

        //
        document.SaveToStreamXLS(msExcel);

        /* Necessário voltar o ponteiro do arquivo para o Inicio */
        msExcel.Seek(0, SeekOrigin.Begin);
        //
        document.Close();
        //
        return msExcel;
    }

    #endregion
    #endregion

    #region 3 Aba

    protected readonly static object lockObject = new object();


    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void ASPxGridView1_CellEditorInitialize(object sender, ASPxGridViewEditorEventArgs e)
    {
        e.Editor.ReadOnly = false;
    }

    /// <summary>
    /// Insert no arquivo XML
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void ASPxGridView1_RowInserting(object sender, DevExpress.Web.Data.ASPxDataInsertingEventArgs e)
    {
        int maxID = 0;

        foreach (XmlNode selectedNode in XmlDataSource1.GetXmlDocument().SelectNodes("Lista/DePara"))
        {
            int idAtivo = int.Parse(selectedNode.Attributes["IdAtivo"].Value);
            if (idAtivo > maxID)
                maxID = idAtivo;
        }

        e.NewValues.Insert(0, "IdAtivo", maxID + 1);

        XmlNode node = XmlDataSource1.GetXmlDocument().CreateElement("DePara");
        foreach (DictionaryEntry entry in e.NewValues)
        {
            XmlAttribute attribute = XmlDataSource1.GetXmlDocument().CreateAttribute(entry.Key.ToString());
            attribute.Value = GetValue(entry.Value);
            node.Attributes.Append(attribute);
        }
        XmlDataSource1.GetXmlDocument().SelectSingleNode("Lista").AppendChild(node);
        SaveXml(XmlDataSource1);

        e.Cancel = true;

        ASPxGridView grid = (ASPxGridView)sender;
        grid.CancelEdit();
    }

    /// <summary>
    /// Update no arquivo XML
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void ASPxGridView1_RowUpdating(object sender, DevExpress.Web.Data.ASPxDataUpdatingEventArgs e)
    {
        ASPxGridView grid = (ASPxGridView)sender;

        int idAtivo = int.Parse(e.Keys["IdAtivo"].ToString());

        XmlNode node = XmlDataSource1.GetXmlDocument().SelectSingleNode(string.Format("Lista/DePara[@IdAtivo='{0}']", idAtivo));
        if (node != null)
        {
            foreach (DictionaryEntry entry in e.NewValues)
                node.Attributes[entry.Key.ToString()].Value = entry.Value.ToString();

            SaveXml(XmlDataSource1);
        }
        else
            grid.JSProperties["cpException"] = string.Format("The DataRow with keyValue = '{0}' was deleted by another user.", idAtivo);

        e.Cancel = true;

        grid.CancelEdit();
    }

    /// <summary>
    /// Delete no arquivo XML
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void ASPxGridView1_RowDeleting(object sender, DevExpress.Web.Data.ASPxDataDeletingEventArgs e)
    {

        int idAtivo = int.Parse(e.Keys["IdAtivo"].ToString());

        XmlNode node = XmlDataSource1.GetXmlDocument().SelectSingleNode(string.Format("Lista/DePara[@IdAtivo='{0}']", idAtivo));
        XmlNode parent = node.ParentNode;
        parent.RemoveChild(node);

        SaveXml(XmlDataSource1);

        e.Cancel = true;

        ASPxGridView grid = (ASPxGridView)sender;
        grid.CancelEdit();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="obj"></param>
    /// <returns></returns>
    private string GetValue(object obj)
    {
        return obj == null ? string.Empty : obj.ToString();
    }

    /// <summary>
    /// Salva dados no arquivo XML
    /// </summary>
    /// <param name="xmlDataSource"></param>
    protected void SaveXml(XmlDataSource xmlDataSource)
    {
        lock (lockObject)
        {
            xmlDataSource.Save();
        }
    }
    #endregion

    #region 4Aba
    private void InicializaGridPermissaoPortalCliente()
    {
        this.InicializaValidacaoForm();

        #region Properties básicas
        this.gridCadastro1.ClientInstanceName = "gridCadastro1";
        this.gridCadastro1.AutoGenerateColumns = false;
        this.gridCadastro1.Width = Unit.Percentage(100);
        this.gridCadastro1.EnableViewState = true;

        //Troca de Like para Contains nos campos texto do grid
        UtilitarioGrid.GridFilterContains(gridCadastro1);

        //Pager
        this.gridCadastro1.SettingsPager.PageSize = 30;

        //Settings geral
        this.gridCadastro1.Settings.ShowFilterRow = true;
        this.gridCadastro1.Settings.ShowStatusBar = GridViewStatusBarMode.Visible;
        this.gridCadastro1.Settings.VerticalScrollBarMode = DevExpress.Web.ScrollBarMode.Visible;
        this.gridCadastro1.Settings.VerticalScrollableHeight = 340;
        this.gridCadastro1.Settings.VerticalScrollBarStyle = GridViewVerticalScrollBarStyle.Virtual;

        foreach (GridViewColumn column in this.gridCadastro1.Columns)
        {
            GridViewDataColumn coluna = column as GridViewDataColumn;
            if (coluna != null)
            {
                coluna.Settings.AllowAutoFilterTextInputTimer = DefaultBoolean.False;
            }
        }

        //SettingsText
        this.gridCadastro1.SettingsText.EmptyDataRow = "0 registros";
        this.gridCadastro1.SettingsText.PopupEditFormCaption = " ";
        this.gridCadastro1.SettingsLoadingPanel.Text = "Carregando...";

        //Styles
        this.gridCadastro1.Styles.Header.SortingImageSpacing = Unit.Pixel(5);
        this.gridCadastro1.Styles.AlternatingRow.Enabled = DefaultBoolean.True;
        this.gridCadastro1.Styles.Cell.Wrap = DefaultBoolean.False;
        this.gridCadastro1.Styles.CommandColumn.Cursor = "hand";

        #endregion

        ((GridViewCommandColumn)this.gridCadastro1.Columns[0]).ShowSelectCheckbox = true;

        // Se tem HeaderTemplate procura pelo comboBox cba_all
        if (gridCadastro1.Columns[0].HeaderTemplate != null)
        {
            Control c = gridCadastro1.FindHeaderTemplateControl(gridCadastro1.Columns[0], "cbAll");
            if (c != null && c is ASPxCheckBox)
            {
                // Alinhamento
                gridCadastro1.Columns[0].HeaderStyle.HorizontalAlign = HorizontalAlign.Center;
            }
        }

        ((GridViewCommandColumn)this.gridCadastro1.Columns[0]).ButtonType = GridCommandButtonRenderMode.Image;

        ((GridViewCommandColumn)this.gridCadastro1.Columns[0]).Width = Unit.Percentage(10);

        this.gridCadastro1.SettingsCommandButton.UpdateButton.Image.Url = "~/imagens/ico_form_ok_inline.gif";
        this.gridCadastro1.SettingsCommandButton.CancelButton.Image.Url = "~/imagens/ico_form_back_inline.gif";
        this.gridCadastro1.SettingsCommandButton.UpdateButton.Text = "Salvar";
        this.gridCadastro1.SettingsCommandButton.CancelButton.Text = "Cancelar";         
        //
        this.gridCadastro1.SettingsEditing.Mode = GridViewEditingMode.Inline;

        UtilitarioGrid.SetaCorCombosInline(this.gridCadastro1);
        this.gridCadastro1.CellEditorInitialize += new ASPxGridViewEditorEventHandler(this.gridCadastro1_CellEditorInitialize);

        #region Evento de Duplo Click
        //Client-Side Events
        this.gridCadastro1.ClientSideEvents.RowDblClick = "function(s, e) {editFormOpen=true; gridCadastro1.StartEditRow(e.visibleIndex);}";
        #endregion

        #region Eventos
        //Eventos comuns
        this.gridCadastro1.HtmlRowCreated += new ASPxGridViewTableRowEventHandler(UtilitarioGrid.Grid_HtmlRowCreated);
        //
        this.gridCadastro1.HtmlDataCellPrepared += new ASPxGridViewTableDataCellEventHandler(gridCadastro_HtmlDataCellPrepared);
        //
        this.gridCadastro1.CustomJSProperties += new ASPxGridViewClientJSPropertiesEventHandler(this.gridCadastro1_CustomJSProperties);
        //
        //this.gridCadastro1.PreRender += new EventHandler(this.gridCadastro_PreRender);
        #endregion
    }

    #region btnPDF_Click, btnExcel_Click, btnOK_Click,
    protected void btnPDF_Click1(object sender, EventArgs e)
    {
        gridExport1.WritePdfToResponse();
    }

    protected void btnExcel_Click1(object sender, EventArgs e)
    {
        gridExport1.WriteXlsToResponse();
    }
    #endregion

    private void InicializaValidacaoForm()
    {
        if (!this.gridCadastro1.IsNewRowEditing)
        {
            this.gridCadastro1.StartRowEditing += new ASPxStartRowEditingEventHandler(this.gridCadastro_StartRowEditing1);
        }
    }

    //Controle (via session) para gerar validação somente para novas linhas
    protected void gridCadastro_StartRowEditing1(object sender, DevExpress.Web.Data.ASPxStartRowEditingEventArgs e)
    {
        if (!gridCadastro1.IsNewRowEditing)
            Session["FormLoad"] = "N";
    }

    protected void gridCadastro1_CustomJSProperties(object sender, ASPxGridViewClientJSPropertiesEventArgs e)
    {
        if (gridCadastro1.IsNewRowEditing)
        {
            e.Properties["cp_EditVisibleIndex"] = "new";
        }
        else
        {
            e.Properties["cp_EditVisibleIndex"] = gridCadastro1.EditingRowVisibleIndex.ToString();
        }

        ASPxGridView gridView = sender as ASPxGridView;
        if (gridView.IsEditing)
        {
            bool newRow = false;
            if (gridView.IsNewRowEditing)
            {
                newRow = true;
            }
            e.Properties["cpNewRow"] = newRow;
        }

        //Variáveis para check múltiplo
        Int32 start = gridView.VisibleStartIndex;
        Int32 end = gridView.VisibleStartIndex + gridView.SettingsPager.PageSize;
        Int32 selectNumbers = 0;
        end = (end > gridView.VisibleRowCount ? gridView.VisibleRowCount : end);

        for (int i = start; i < end; i++)
            if (gridView.Selection.IsRowSelected(i))
                selectNumbers++;

        e.Properties["cpSelectedRowsOnPage"] = selectNumbers;
        e.Properties["cpVisibleRowCount"] = gridView.VisibleRowCount;
        //
    }

    protected void gridCadastro1_RowUpdating(object sender, DevExpress.Web.Data.ASPxDataUpdatingEventArgs e)
    {
        PermissaoPortalCliente permissaoPortalCliente = new PermissaoPortalCliente();
        int idGrupo = (Int32)e.Keys[0];

        if (permissaoPortalCliente.LoadByPrimaryKey(idGrupo))
        {
            permissaoPortalCliente.PodeBoletar = Convert.ToString(e.NewValues[PermissaoPortalClienteMetadata.ColumnNames.PodeBoletar]);
            permissaoPortalCliente.PodeDistribuir = Convert.ToString(e.NewValues[PermissaoPortalClienteMetadata.ColumnNames.PodeDistribuir]);
            permissaoPortalCliente.PodeAprovar = Convert.ToString(e.NewValues[PermissaoPortalClienteMetadata.ColumnNames.PodeAprovar]);
            permissaoPortalCliente.AcessoSeguranca = Convert.ToString(e.NewValues[PermissaoPortalClienteMetadata.ColumnNames.AcessoSeguranca]);
            permissaoPortalCliente.AcessoPGBL = Convert.ToString(e.NewValues[PermissaoPortalClienteMetadata.ColumnNames.AcessoPGBL]);

            permissaoPortalCliente.Save();

            #region Log do Processo
            HistoricoLog historicoLog = new HistoricoLog();
            historicoLog.InsereHistoricoLog(DateTime.Now,
                                            DateTime.Now,
                                            "Cadastro de PermissaoPortalCliente - Operacao: Update PermissaoPortalCliente: " + idGrupo + UtilitarioWeb.ToString(permissaoPortalCliente),
                                            HttpContext.Current.User.Identity.Name,
                                            UtilitarioWeb.GetIP(Request),
                                            "",
                                            HistoricoLogOrigem.Outros);
            #endregion
        }

        e.Cancel = true;
        gridCadastro1.CancelEdit();
        gridCadastro1.DataBind();
    }

    protected void gridCadastro1_RowInserting(object sender, DevExpress.Web.Data.ASPxDataInsertingEventArgs e)
    {
        PermissaoPortalCliente permissaoPortalCliente = new PermissaoPortalCliente();

        permissaoPortalCliente.IdGrupo = Convert.ToInt32(e.NewValues["Descricao"]);
        permissaoPortalCliente.PodeBoletar = Convert.ToString(e.NewValues[PermissaoPortalClienteMetadata.ColumnNames.PodeBoletar]);
        permissaoPortalCliente.PodeDistribuir = Convert.ToString(e.NewValues[PermissaoPortalClienteMetadata.ColumnNames.PodeDistribuir]);
        permissaoPortalCliente.PodeAprovar = Convert.ToString(e.NewValues[PermissaoPortalClienteMetadata.ColumnNames.PodeAprovar]);
        permissaoPortalCliente.AcessoSeguranca = Convert.ToString(e.NewValues[PermissaoPortalClienteMetadata.ColumnNames.AcessoSeguranca]);
        permissaoPortalCliente.AcessoPGBL = Convert.ToString(e.NewValues[PermissaoPortalClienteMetadata.ColumnNames.AcessoPGBL]);
        //
        permissaoPortalCliente.Save();

        #region Log do Processo
        HistoricoLog historicoLog = new HistoricoLog();
        historicoLog.InsereHistoricoLog(DateTime.Now,
                                        DateTime.Now,
                                        "Cadastro de PermissaoPortalCliente - Operacao: Insert PermissaoPortalCliente: " + permissaoPortalCliente.IdGrupo + UtilitarioWeb.ToString(permissaoPortalCliente),
                                        HttpContext.Current.User.Identity.Name,
                                        UtilitarioWeb.GetIP(Request),
                                        "",
                                        HistoricoLogOrigem.Outros);
        #endregion

        e.Cancel = true;
        gridCadastro1.CancelEdit();
        gridCadastro1.DataBind();
    }

    protected void gridCadastro1_CustomCallback(object sender, DevExpress.Web.ASPxGridViewCustomCallbackEventArgs e)
    {
        if (e.Parameters == "btnDelete")
        {
            List<object> keyValuesId = gridCadastro1.GetSelectedFieldValues("IdGrupo");
            for (int i = 0; i < keyValuesId.Count; i++)
            {
                int idGrupo = Convert.ToInt32(keyValuesId[i]);

                PermissaoPortalCliente permissaoPortalCliente = new PermissaoPortalCliente();
                if (permissaoPortalCliente.LoadByPrimaryKey(idGrupo))
                {
                    //
                    PermissaoPortalCliente permissaoPortalClienteClone = (PermissaoPortalCliente)Utilitario.Clone(permissaoPortalCliente);
                    //

                    permissaoPortalCliente.MarkAsDeleted();
                    permissaoPortalCliente.Save();

                    #region Log do Processo
                    HistoricoLog historicoLog = new HistoricoLog();
                    historicoLog.InsereHistoricoLog(DateTime.Now,
                                                    DateTime.Now,
                                                    "Cadastro de PermissaoPortalCliente - Operacao: Delete PermissaoPortalCliente: " + idGrupo + UtilitarioWeb.ToString(permissaoPortalClienteClone),
                                                    HttpContext.Current.User.Identity.Name,
                                                    UtilitarioWeb.GetIP(Request),
                                                    "",
                                                    HistoricoLogOrigem.Outros);
                    #endregion
                }
            }
        }

        gridCadastro1.DataBind();
        gridCadastro1.Selection.UnselectAll();
        gridCadastro1.CancelEdit();
    }

    protected void gridCadastro1_CellEditorInitialize(object sender, ASPxGridViewEditorEventArgs e)
    {
        if ((e.Editor as ASPxComboBox).ValidationSettings.RequiredField.ErrorText != "")
        {
            (e.Editor as ASPxComboBox).ValidationSettings.RequiredField.IsRequired = true;
            (e.Editor as ASPxComboBox).ValidationSettings.RequiredField.ErrorText = base.MsgCampoObrigatorio;
        }

        if (gridCadastro1 != null)
        {
            if (gridCadastro1.IsEditing && e.Column.FieldName == GetFirstColumnFieldName(gridCadastro1))
            {
                e.Editor.Focus();
            }
        }

        if (!gridCadastro1.IsNewRowEditing)
        {
            if (e.Column.FieldName == "Descricao")
            {
                //e.Editor.Enabled = false;
                e.Editor.BackColor = Color.FromName("#EBEBEB");
                e.Editor.ReadOnly = true;
            }
        }
    }

    protected void EsDSGrupoUsuario_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {

        PermissaoPortalClienteQuery p = new PermissaoPortalClienteQuery("P");
        p.Select(p.IdGrupo);

        // Todos os grupos menos o 0 e aqueles que já existem em PermissaoPortalCliente
        GrupoUsuarioQuery g = new GrupoUsuarioQuery("G");
        g.Where(g.IdGrupo != 0 &&
                g.IdGrupo.NotIn(p) &&
                g.Descricao.Trim().NotEqual(""));
        g.OrderBy(g.Descricao.Ascending);
        //
        GrupoUsuarioCollection coll = new GrupoUsuarioCollection();
        coll.Load(g);

        e.Collection = coll;
    }

    protected void EsDSPermissaoPortalCliente_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        PermissaoPortalClienteQuery p = new PermissaoPortalClienteQuery("P");
        GrupoUsuarioQuery g = new GrupoUsuarioQuery("G");

        p.Select(p, g.Descricao);
        p.InnerJoin(g).On(p.IdGrupo == g.IdGrupo);
        p.OrderBy(g.Descricao.Ascending);

        PermissaoPortalClienteCollection coll = new PermissaoPortalClienteCollection();
        coll.Load(p);
        //        
        e.Collection = coll;
    }
    #endregion

    #region Aba Caixa on line

    protected void gridCaixaOnline_HtmlRowCreated(object sender, DevExpress.Web.ASPxGridViewTableRowEventArgs e)
    {
        e.Row.Attributes.Add("onmouseover", "backgroundColor = this.style.backgroundColor; this.style.backgroundColor='#E0E9EB';");
        e.Row.Attributes.Add("onmouseout", "this.style.backgroundColor=backgroundColor;");
    }

    protected void callbackCarrega_Callback(object sender, DevExpress.Web.CallbackEventArgs e)
    {
        e.Result = "";

        CaixaOnline caixaOnline = new CaixaOnline();

        if (e.Parameter == "CargaInicCOL")
        {
            caixaOnline.CriarDataTableCaixaOnLine();

            caixaOnline.PopulaDataTableCaixaOnLine((DateTime)this.textDataCaixaOnline.Value);

            this.LinkButton7.Enabled = true;
            
            e.Result = "Dados carregados";

        }

        if (e.Parameter == "AtualizaCOL")
        {
            caixaOnline.ProcessaCaixaOnLine((DateTime)this.textDataCaixaOnline.Value);

            e.Result = "Dados atualizados";

        }
        gridCaixaOnline.DataBind();
    }

    // Coloca Tooltip nas linhas do grid. Colunas com ListBox não são colocadas. Devem ser feitas manualmente
    protected void gridCaixaOnline_HtmlDataCellPrepared(object sender, ASPxGridViewTableDataCellEventArgs e)
    {
        if (e.CellValue != null)
        {
            if (e.DataColumn is GridViewDataComboBoxColumn)
            {
                return;
            }
            else
            {
                string tooltip = e.CellValue is DateTime
                                 ? (Convert.ToDateTime(e.CellValue)).ToString("d")
                                 : e.CellValue.ToString().Trim();

                e.Cell.Attributes.Add("title", tooltip);
            }
        }
        //e.Cell.Attributes.Add("title", Convert.ToString( e.GetValue(e.DataColumn.FieldName) ).Trim() );
    }
    
    // Coloca Tooltip nas linhas do grid. Colunas com ListBox não são colocadas. Devem ser feitas manualmente
    protected void gridCaixaOnline_CustomColumnDisplayText(object sender, ASPxGridViewColumnDisplayTextEventArgs e)
    {
        int TextMaxLength = 48;
        if (e.Column.FieldName == "Fundo")
        {
            if (e.Value != null)
            {
                string cellValue = e.Value.ToString();
                if (cellValue.Length > TextMaxLength)
                    e.DisplayText = cellValue.Substring(0, TextMaxLength) + "...";
            }
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnAtualizar_OnInit(object sender, EventArgs e)
    {
        LinkButton b = sender as LinkButton;

        /* Verifica em Configurações do Sistema se IntegracaoBolsa, IntegracaoBMF ou IntegracaoContaCorrente = Sinacor */
        b.Enabled = false;
    }

    protected void textDataCaixaOnline_OnInit(object sender, EventArgs e)
    {
        textDataCaixaOnline.Value = DateTime.Today;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCaixaOnline_CustomCallback(object sender, DevExpress.Web.ASPxGridViewCustomCallbackEventArgs e)
    {
        gridCaixaOnline.DataBind();
    }

    #endregion

    #region 5 aba
    protected readonly static object lockObject1 = new object();

    /// <summary>
    /// Configurações Iniciais do grid
    /// </summary>
    private void InicializaGridCarteiraGrupoEconomico() {

        #region Cria arquivo XML se não Existir
        string diretorio = DiretorioAplicacao.DiretorioBaseAplicacao;

        if (!System.IO.Directory.Exists(diretorio + "/XMLCustom/PortoPar"))
        {
            System.IO.Directory.CreateDirectory(diretorio + "/XMLCustom/PortoPar");
        }

        if (!System.IO.File.Exists(diretorio + "/XMLCustom/PortoPar/CarteiraGrupoEconomico.xml"))
        {
            XmlDocument xmlDoc = new XmlDocument();
            XmlNode xmlNode = xmlDoc.CreateXmlDeclaration("1.0", "UTF-8", null);
            xmlDoc.AppendChild(xmlNode);
            xmlNode = xmlDoc.CreateElement("Lista");

            xmlDoc.AppendChild(xmlNode);

            xmlDoc.Save(diretorio + "//XMLCustom/PortoPar/CarteiraGrupoEconomico.xml");
        }
        #endregion
                             
        if (!this.gridCadastro2.IsNewRowEditing) {
            this.gridCadastro2.StartRowEditing += new ASPxStartRowEditingEventHandler(this.gridCadastro2_StartRowEditing2);
        }

        #region Properties básicas
        this.gridCadastro2.AutoGenerateColumns = false;
        //this.gridCadastro2.Width = Unit.Percentage(100);
        //this.gridCadastro2.Width = 600
        //this.gridCadastro2.EnableViewState = true;

        //Troca de Like para Contains nos campos texto do grid
        UtilitarioGrid.GridFilterContains(gridCadastro2);

        //Pager
        this.gridCadastro2.SettingsPager.PageSize = 30;

        //Settings geral
        this.gridCadastro2.Settings.ShowFilterRow = true;
        this.gridCadastro2.Settings.ShowStatusBar = GridViewStatusBarMode.Visible;
        this.gridCadastro2.Settings.ShowVerticalScrollBar = true;
        this.gridCadastro2.Settings.VerticalScrollableHeight = 340;
        this.gridCadastro2.Settings.VerticalScrollBarStyle = GridViewVerticalScrollBarStyle.Virtual;

        foreach (GridViewColumn column in this.gridCadastro2.Columns) {
            GridViewDataColumn coluna = column as GridViewDataColumn;
            if (coluna != null) {
                coluna.Settings.AllowAutoFilterTextInputTimer = DefaultBoolean.False;
            }
        }

        //SettingsText
        this.gridCadastro2.SettingsText.EmptyDataRow = "0 registros";
        this.gridCadastro2.SettingsText.PopupEditFormCaption = " ";
        this.gridCadastro2.SettingsLoadingPanel.Text = "Carregando...";

        //Styles
        this.gridCadastro2.Styles.Header.SortingImageSpacing = Unit.Pixel(5);
        this.gridCadastro2.Styles.AlternatingRow.Enabled = DefaultBoolean.True;
        this.gridCadastro2.Styles.Cell.Wrap = DefaultBoolean.False;
        this.gridCadastro2.Styles.CommandColumn.Cursor = "hand";

        #endregion

        ((GridViewCommandColumn)this.gridCadastro2.Columns[0]).ShowSelectCheckbox = true;

        // Se tem HeaderTemplate procura pelo comboBox cba_all
        if (gridCadastro2.Columns[0].HeaderTemplate != null) {
            Control c = gridCadastro2.FindHeaderTemplateControl(gridCadastro2.Columns[0], "cbAll");
            if (c != null && c is ASPxCheckBox) {
                // Alinhamento
                gridCadastro2.Columns[0].HeaderStyle.HorizontalAlign = HorizontalAlign.Center;
            }
        }

        ((GridViewCommandColumn)this.gridCadastro2.Columns[0]).ButtonType = GridCommandButtonRenderMode.Image;
        ((GridViewCommandColumn)this.gridCadastro2.Columns[0]).Width = Unit.Percentage(10);
        //((GridViewCommandColumn)this.gridCadastro2.Columns[0]).UpdateButton.Image.Url = "~/imagens/ico_form_ok_inline.gif";
        //((GridViewCommandColumn)this.gridCadastro2.Columns[0]).CancelButton.Image.Url = "~/imagens/ico_form_back_inline.gif";
        ((GridViewCommandColumn)this.gridCadastro2.Columns[0]).ShowCancelButton = true;        
        ((GridViewCommandColumn)this.gridCadastro2.Columns[0]).ShowUpdateButton = true;

        //((GridViewCommandColumn)this.gridCadastro2.Columns[0]).UpdateButton.Text = "Salvar";
        //((GridViewCommandColumn)this.gridCadastro2.Columns[0]).CancelButton.Text = "Cancelar";

        this.gridCadastro2.SettingsEditing.Mode = GridViewEditingMode.Inline;

        UtilitarioGrid.SetaCorCombosInline(this.gridCadastro2);

        #region Evento de Duplo Click
        //Client-Side Events
        this.gridCadastro2.ClientSideEvents.RowDblClick = "function(s, e) {editFormOpen=true; gridCadastro2.StartEditRow(e.visibleIndex);}";
        #endregion

        #region Eventos
        //Eventos comuns
        this.gridCadastro2.HtmlRowCreated += new ASPxGridViewTableRowEventHandler(UtilitarioGrid.Grid_HtmlRowCreated);
        //
        this.gridCadastro2.HtmlDataCellPrepared += new ASPxGridViewTableDataCellEventHandler(gridCadastro_HtmlDataCellPrepared);
        //
        this.gridCadastro2.CustomJSProperties += new ASPxGridViewClientJSPropertiesEventHandler(this.gridCadastro2_CustomJSProperties);
        //
        #endregion
    }

    //Controle (via session) para gerar validação somente para novas linhas
    protected void gridCadastro2_StartRowEditing2(object sender, DevExpress.Web.Data.ASPxStartRowEditingEventArgs e) {
        //if (!gridCadastro2.IsNewRowEditing)
        //    Session["FormLoad"] = "N";
    }

    protected void gridCadastro2_CustomJSProperties(object sender, ASPxGridViewClientJSPropertiesEventArgs e) {
        //if (gridCadastro2.IsNewRowEditing) {
        //    e.Properties["cp_EditVisibleIndex"] = "new";
        //}
        //else {
        //    e.Properties["cp_EditVisibleIndex"] = gridCadastro2.EditingRowVisibleIndex.ToString();
        //}

        //ASPxGridView gridView = sender as ASPxGridView;
        //if (gridView.IsEditing) {
        //    bool newRow = false;
        //    if (gridView.IsNewRowEditing) {
        //        newRow = true;
        //    }
        //    e.Properties["cpNewRow"] = newRow;
        //}

        ////Variáveis para check múltiplo
        //Int32 start = gridView.VisibleStartIndex;
        //Int32 end = gridView.VisibleStartIndex + gridView.SettingsPager.PageSize;
        //Int32 selectNumbers = 0;
        //end = (end > gridView.VisibleRowCount ? gridView.VisibleRowCount : end);

        //for (int i = start; i < end; i++)
        //    if (gridView.Selection.IsRowSelected(i))
        //        selectNumbers++;

        //e.Properties["cpSelectedRowsOnPage"] = selectNumbers;
        //e.Properties["cpVisibleRowCount"] = gridView.VisibleRowCount;
        //
    }


    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro2_CellEditorInitialize(object sender, ASPxGridViewEditorEventArgs e) {
        if ((e.Editor as ASPxTextEdit).ValidationSettings.RequiredField.ErrorText != "") {
            (e.Editor as ASPxTextEdit).ValidationSettings.RequiredField.IsRequired = true;
            (e.Editor as ASPxTextEdit).ValidationSettings.RequiredField.ErrorText = "Campo obrigatório";
        }

        if (gridCadastro2 != null) {
            if (gridCadastro2.IsEditing && e.Column.FieldName == GetFirstColumnFieldName(gridCadastro2)) {
                e.Editor.Focus();
            }
        }
        //

        e.Editor.ReadOnly = false;

        if (e.Column.Caption == "Id Carteira" || e.Column.Caption == "Id Grupo Econômico") {
            e.Editor.Enabled = false;
            e.Editor.BackColor = Color.FromName("#EBEBEB");
            e.Editor.ReadOnly = true;
        }
    }


    #region DataSources
    protected void EsDSCliente_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        ClienteCollection coll = new ClienteCollection();
        coll.LoadAll();

        e.Collection = coll;
    }

    protected void EsDSCarteira2_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e) {
        Usuario usuario = new Usuario();
        usuario.BuscaUsuario(Context.User.Identity.Name);
        int idUsuario = usuario.IdUsuario.Value;

        CarteiraQuery carteiraQuery = new CarteiraQuery("C");
        ClienteQuery clienteQuery = new ClienteQuery("L");
        PermissaoClienteQuery permissaoClienteQuery = new PermissaoClienteQuery("P");

        carteiraQuery.Select(carteiraQuery, clienteQuery.IdTipo);
        //
        carteiraQuery.InnerJoin(clienteQuery).On(clienteQuery.IdCliente == carteiraQuery.IdCarteira);
        carteiraQuery.InnerJoin(permissaoClienteQuery).On(permissaoClienteQuery.IdCliente == clienteQuery.IdCliente);
        //
        carteiraQuery.Where(permissaoClienteQuery.IdUsuario == idUsuario,
                            clienteQuery.TipoControle.Equal((byte)TipoControleCliente.Completo),
                            clienteQuery.StatusAtivo.Equal((byte)StatusAtivoCliente.Ativo));
        //
        carteiraQuery.OrderBy(carteiraQuery.Apelido.Ascending);

        CarteiraCollection coll = new CarteiraCollection();
        coll.Load(carteiraQuery);

        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }

    protected void EsDSGrupoEconomico2_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e) {
        GrupoEconomicoCollection coll = new GrupoEconomicoCollection();
        coll.Query.Select(coll.Query.IdGrupo.As("IdGrupoEconomico"), coll.Query.Nome);
        coll.LoadAll();
        //
        e.Collection = coll;
    }

    #endregion

    protected void gridCadastro2_CustomUnboundColumnData(object sender, DevExpress.Web.ASPxGridViewColumnDataEventArgs e) {
        if (e.Column.FieldName == "CompositeKey") {
            //Compose a primary key value
            string idCarteira = Convert.ToString(e.GetListSourceFieldValue("IdCarteira"));
            string idGrupoEconomico = Convert.ToString(e.GetListSourceFieldValue("IdGrupoEconomico"));
            e.Value = idCarteira + "|" + idGrupoEconomico;
        }
    }

    /// <summary>
    /// Delete
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro2_CustomCallback(object sender, DevExpress.Web.ASPxGridViewCustomCallbackEventArgs e) {
        if (e.Parameters == "btnDelete") {
            List<object> keyValuesId = gridCadastro2.GetSelectedFieldValues("IdCarteira");
            List<object> keyValuesData = gridCadastro2.GetSelectedFieldValues("IdGrupoEconomico");

            for (int i = 0; i < keyValuesData.Count; i++) {
                int idCarteiraDelete = Convert.ToInt32(keyValuesId[i]);
                int IdGrupoEconomicoDelete = Convert.ToInt32(keyValuesData[i]);

                string search = string.Format("Lista/Carteira[@IdCarteira='{0}' and @IdGrupoEconomico='{1}']", idCarteiraDelete, IdGrupoEconomicoDelete);

                XmlNode node = this.CarteiraGrupoEconomico.GetXmlDocument().SelectSingleNode(search);
                XmlNode parent = node.ParentNode;
                parent.RemoveChild(node);

                this.SaveXml1(this.CarteiraGrupoEconomico);
            }
        }

        gridCadastro2.DataBind();
        gridCadastro2.Selection.UnselectAll();
        gridCadastro2.CancelEdit();
    }

    /// <summary>
    /// Insert no arquivo XML
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro2_RowInserting(object sender, DevExpress.Web.Data.ASPxDataInsertingEventArgs e) {
        ASPxGridView grid = (ASPxGridView)sender;
        
        XmlNode node = this.CarteiraGrupoEconomico.GetXmlDocument().CreateElement("Carteira");
        foreach (DictionaryEntry entry in e.NewValues) {
            XmlAttribute attribute = this.CarteiraGrupoEconomico.GetXmlDocument().CreateAttribute(entry.Key.ToString());
            attribute.Value = entry.Value.ToString();
            node.Attributes.Append(attribute);
        }

        //
        int idCarteiraInsert = Convert.ToInt32(e.NewValues["IdCarteira"]);
        int idGrupoEconomicoInsert = Convert.ToInt32(e.NewValues["IdGrupoEconomico"]);
        //
        string search = string.Format("Lista/Carteira[@IdCarteira='{0}' and @IdGrupoEconomico='{1}']", idCarteiraInsert, idGrupoEconomicoInsert);
        XmlNode node1 = this.CarteiraGrupoEconomico.GetXmlDocument().SelectSingleNode(search);

        if (node1 != null) { // Node ja existe
            grid.JSProperties["cpException"] = "Registro já cadastrado.";
            e.Cancel = true;
            grid.CancelEdit();
            return;
        }
        else {
            this.CarteiraGrupoEconomico.GetXmlDocument().SelectSingleNode("Lista").AppendChild(node);
            this.SaveXml1(CarteiraGrupoEconomico);
        }

        e.Cancel = true;
        gridCadastro2.CancelEdit();
        gridCadastro2.DataBind();
    }

    /// <summary>
    /// Salva dados no arquivo XML
    /// </summary>
    /// <param name="xmlDataSource"></param>
    protected void SaveXml1(XmlDataSource xmlDataSource) {
        lock (lockObject1) {
            xmlDataSource.Save();
        }
    }

    /// <summary>
    /// Update no arquivo XML
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro2_RowUpdating(object sender, DevExpress.Web.Data.ASPxDataUpdatingEventArgs e) {
        ASPxGridView grid = (ASPxGridView)sender;
        
        string compositeKey = e.Keys["CompositeKey"].ToString();
        string[] keys = compositeKey.Split('|');
        //
        int idCarteiraUpdate = Convert.ToInt32(keys[0]);
        int IdGrupoEconomicoUpdate = Convert.ToInt32(keys[1]);
        //
        string search = string.Format("Lista/Carteira[@IdCarteira='{0}' and @IdGrupoEconomico='{1}']", idCarteiraUpdate, IdGrupoEconomicoUpdate);
        //
        XmlNode node = this.CarteiraGrupoEconomico.GetXmlDocument().SelectSingleNode(search);
        //
        int idCarteiraUpdateNovo = Convert.ToInt32(e.NewValues["IdCarteira"]);
        int idGrupoEconomicoUpdateNovo = Convert.ToInt32(e.NewValues["IdGrupoEconomico"]);
        //

        // Se foi alterado algum valor
        if (idCarteiraUpdate != idCarteiraUpdateNovo || IdGrupoEconomicoUpdate != idGrupoEconomicoUpdateNovo) {
            string search1 = string.Format("Lista/Carteira[@IdCarteira='{0}' and @IdGrupoEconomico='{1}']", idCarteiraUpdateNovo, idGrupoEconomicoUpdateNovo);
            XmlNode node1 = this.CarteiraGrupoEconomico.GetXmlDocument().SelectSingleNode(search1);

            if (node1 != null) { // Node já existe
                grid.JSProperties["cpException"] = "Registro já cadastrado.";
                e.Cancel = true;
                grid.CancelEdit();
                return;
            }
            else {
                foreach (DictionaryEntry entry in e.NewValues) {
                    node.Attributes[entry.Key.ToString()].Value = entry.Value.ToString();
                }

                this.SaveXml1(this.CarteiraGrupoEconomico);
            }
        }

        e.Cancel = true;
        gridCadastro2.CancelEdit();
        gridCadastro2.DataBind();
    }


    #endregion

    /// <summary>
    /// TabControl - Controla visibilidade da 4 aba
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    new protected void tabPortoParOnLoad(object sender, EventArgs e)
    {


        // ***** retirar
        //CriarDataTableCaixaOnLine();
        //DateTime dt = new DateTime(2014, 03, 21);
        //CarregaSaldoInicialCaixaOnLine(dt);
        //ProcessaCaixaOnLine(dt);

        ASPxPageControl pageControl = sender as ASPxPageControl;
        //
        if (this.apenasSeguranca == ApenasSeguranca.Sim)
        {
            pageControl.TabPages[0].ClientVisible = false;
            pageControl.TabPages[1].ClientVisible = false;
            pageControl.TabPages[2].ClientVisible = false;
            pageControl.TabPages[3].ClientVisible = true;
            pageControl.TabPages[4].ClientVisible = false;
            pageControl.TabPages[5].ClientVisible = false;
        }
        else
        {
            pageControl.TabPages[0].ClientVisible = true;
            pageControl.TabPages[1].ClientVisible = true;
            pageControl.TabPages[2].ClientVisible = true;
            pageControl.TabPages[3].ClientVisible = false;
            pageControl.TabPages[4].ClientVisible = true;
            pageControl.TabPages[5].ClientVisible = true;
        }
    }

    private void InicializaGridDePara_IntegralTrust()
    {
        #region Cria arquivo XML se não Existir
        string diretorio = DiretorioAplicacao.DiretorioBaseAplicacao;

        if (!System.IO.Directory.Exists(diretorio + "/XMLCustom/PortoPar"))
        {
            System.IO.Directory.CreateDirectory(diretorio + "/XMLCustom/PortoPar");
        }

        if (!System.IO.File.Exists(diretorio + "/XMLCustom/PortoPar/DePara_IntegralTrust.xml"))
        {
            XmlDocument xmlDoc = new XmlDocument();
            XmlNode xmlNode = xmlDoc.CreateXmlDeclaration("1.0", "UTF-8", null);
            xmlDoc.AppendChild(xmlNode);
            xmlNode = xmlDoc.CreateElement("Lista");

            xmlDoc.AppendChild(xmlNode);

            xmlDoc.Save(diretorio + "//XMLCustom/PortoPar/DePara_IntegralTrust.xml");
        }
        #endregion
    }
}