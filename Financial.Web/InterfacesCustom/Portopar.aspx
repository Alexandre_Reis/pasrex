﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Portopar.aspx.cs" Inherits="InterfacesCustom_Portopar" %>

<%@ Register Assembly="System.Web, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"
    Namespace="System.Web.UI.HtmlControls" TagPrefix="cc2" %>
<%@ Register Assembly="EntitySpaces.Web" Namespace="EntitySpaces.Web" TagPrefix="cc1" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxe" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxcb" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxwgv" %>

<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxpc" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxrp" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxp" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxtc" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxw" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" 
    Namespace="DevExpress.Web" TagPrefix="dxlp" %>    
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxgv" %>
    
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <link href="~/css/forms.css" rel="stylesheet" type="text/css" />

    <script type="text/javascript" language="Javascript" src="../js/global.js"></script>

    <script type="text/javascript" language="Javascript">    
    function OnEndCallbackEventHandler(s, e) {
        if (s.cpException) {
            alert(s.cpException.toString());
            delete s.cpException;
        }
    }
    
    function CloseGridLookup() 
    {
        dropCarteira.ConfirmCurrentSelection();
        dropCarteira.HideDropDown();
        dropCarteira.Focus();
    }  
    
    function OnEndCallbackEventHandler1(s, e) {
        if (s.cpException) {
            alert(s.cpException.toString());
            delete s.cpException;
        }
    }
    
    function btnExportaClick(tipoData){
        LoadingPanel.Show();
        callbackExporta.SendCallback(tipoData);
    }
            
    var popup = true;
    document.onkeydown=onDocumentKeyDown;     
    var operacao = '';
    var btnAtualizarCOLEnable = false;
    
    function OnGetDataCarteira(data) {
        btnEditCodigoCarteira.SetValue(data);
        popupCarteira.HideWindow();
        ASPxCallback1.SendCallback(btnEditCodigoCarteira.GetValue());
        btnEditCodigoCarteira.Focus();
    }
    
    function OnGetDataCarteiraFiltro(data) {
        btnEditCodigoCarteiraFiltro.SetValue(data);        
        popupCarteira.HideWindow();
        ASPxCallback1.SendCallback(btnEditCodigoCarteiraFiltro.GetValue());        
        btnEditCodigoCarteiraFiltro.Focus();
    }
    </script>

</head>
<body>
   
   <dxcb:ASPxCallback ID="callbackCarrega" runat="server" OnCallback="callbackCarrega_Callback" ClientInstanceName="callbackCarrega">
        <ClientSideEvents CallbackComplete="function(s, e) 
        {               
            btnAtualizarCOLEnable = true;
            LoadingPanel2.Hide();
            gridCaixaOnline.PerformCallback('btnRefresh');
            alert(e.result);
        }" />
   </dxcb:ASPxCallback>
   <dxcb:ASPxCallback ID="callbackExporta" runat="server" OnCallback="callbackExporta_Callback">
        <ClientSideEvents CallbackComplete="function(s, e) 
        {   
            LoadingPanel.Hide();
            if(e && e.result.length){
                if(e.result.indexOf('redirect') === 0){
                    var newLocation = (e.result.split(':'))[1];
                    var iframeId = 'iframe_download';
                    var iframe = document.getElementById(iframeId);

                    if (!iframe && document.createElement && (iframe =
                        document.createElement('iframe'))) {
               
                        iframe.name = iframe.id = iframeId;
                        iframe.width = 0;
                        iframe.height = 0;
                        iframe.src = 'about:blank';
                        document.body.appendChild(iframe);                                                                    
                    }
                    iframe.src = newLocation;
                   
                }else{
                    alert(e.result);
                }
            }
        }" />
    </dxcb:ASPxCallback>
      
    <form id="form1" runat="server">

<asp:ScriptManager ID="ScriptManager1" AsyncPostBackTimeout="360000" runat="server" EnableScriptGlobalization="true" EnableScriptLocalization="true" />
        
<asp:Panel ID="panelEdicao" runat="server" OnLoad="panelEdicao_Load">            

<div id="divExportacao" runat="server" visible="false">
        <dxcb:ASPxCallback ID="ASPxCallback1" runat="server" OnCallback="ASPxCallback1_Callback">
            <ClientSideEvents CallbackComplete="function(s, e) {    
            if (gridCadastro.cp_EditVisibleIndex == -1) {                                                  
                var textNomeCarteiraFiltro = document.getElementById('popupFiltro_textNomeCarteiraFiltro');
                OnCallBackCompleteCliente(s, e, popupMensagemCarteira, btnEditCodigoCarteiraFiltro, textNomeCarteiraFiltro);
            }
            else {
                var textNomeCarteira = document.getElementById('gridCadastro_DXPEForm_ef' + gridCadastro.cp_EditVisibleIndex + '_textNomeCarteira');
                OnCallBackCompleteCliente(s, e, popupMensagemCarteira, btnEditCodigoCarteira, textNomeCarteira);
            }                                    
         }        
        " />
        </dxcb:ASPxCallback>
        
        <dxcb:ASPxCallback ID="callbackErro" runat="server" OnCallback="callbackErro_Callback">
            <ClientSideEvents CallbackComplete="function(s, e) {             
            if (e.result != '')
            {                   
                alert(e.result);                              
            }
            else
            {
                if (operacao == 'salvar' || gridCadastro.cp_EditVisibleIndex != 'new')
                {             
                    gridCadastro.UpdateEdit();
                }
                else
                {
                    callbackAdd.SendCallback();
                }                          
            }
            operacao = '';
        }        
        " />
        </dxcb:ASPxCallback>
        
        <dxcb:ASPxCallback ID="callbackAdd" runat="server" OnCallback="callbackAdd_Callback">
            <ClientSideEvents CallbackComplete="function(s, e) 
        {
            alert('Operação feita com sucesso.');
        }        
        " />
        </dxcb:ASPxCallback>
        
        <dxpc:ASPxPopupControl ID="popupFiltro" AllowDragging="true" PopupElementID="popupFiltro"
            EnableClientSideAPI="True" PopupVerticalAlign="Middle" PopupHorizontalAlign="OutsideRight"
            Width="400" Left="250" Top="70" HeaderText="Filtros adicionais para consulta"
            runat="server" HeaderStyle-BackColor="#EBECEE" HeaderStyle-Font-Bold="true" HeaderStyle-Font-Size="11px">
            <ContentCollection>
                <dxpc:PopupControlContentControl ID="PopupControlContentControl2" runat="server">
                    <table>
                        <tr>
                            <td>
                                <asp:Label ID="labelDataInicio" runat="server" CssClass="labelNormal" Text="Início:"></asp:Label>
                            </td>
                            <td>
                                <dxe:ASPxDateEdit ID="textDataInicio" runat="server" ClientInstanceName="textDataInicio" />
                            </td>
                            <td>
                                <asp:Label ID="labelDataFim" runat="server" CssClass="labelNormal" Text="Fim:" />
                            </td>
                            <td>
                                <dxe:ASPxDateEdit ID="textDataFim" runat="server" ClientInstanceName="textDataFim" />
                            </td>
                        </tr>
                        <tr>
                            <td class="td_Label_Longo">
                                <asp:Label ID="labelCarteiraFiltro" runat="server" CssClass="labelNormal" Text="Carteira:"></asp:Label>
                            </td>
                            <td>
                                <dxe:ASPxSpinEdit ID="btnEditCodigoCarteiraFiltro" runat="server" CssClass="textButtonEdit"
                                    ClientInstanceName="btnEditCodigoCarteiraFiltro" EnableClientSideAPI="true" CssFilePath="../css/forms.css"
                                    SpinButtons-ShowIncrementButtons="false" MaxLength="10" NumberType="Integer">
                                    <Buttons>
                                        <dxe:EditButton>
                                        </dxe:EditButton>
                                    </Buttons>
                                    <ClientSideEvents KeyPress="function(s, e) {document.getElementById('popupFiltro_textNomeCarteiraFiltro').value = '';} "
                                        ButtonClick="function(s, e) {popupCarteira.ShowAtElementByID(s.name);}" LostFocus="function(s, e) {OnLostFocus(popupMensagemCarteira, ASPxCallback1, btnEditCodigoCarteiraFiltro);}" />
                                </dxe:ASPxSpinEdit>
                            </td>
                            <td colspan="2" width="450">
                                <asp:TextBox ID="textNomeCarteiraFiltro" runat="server" CssClass="textNome" Enabled="false"></asp:TextBox>
                            </td>
                        </tr>
                    </table>
                    <div class="linkButton linkButtonNoBorder" style="margin-top: 20px">
                        <asp:LinkButton ID="btnOKFilter" runat="server" Font-Overline="false" ForeColor="Black"
                            CssClass="btnOK" OnClientClick="popupFiltro.Hide(); gridCadastro.PerformCallback('btnRefresh'); return false;">
                            <asp:Literal ID="Literal7" runat="server" Text="Aplicar" /><div>
                            </div>
                        </asp:LinkButton>
                        <asp:LinkButton ID="btnFilterCancel" runat="server" Font-Overline="false" ForeColor="Black"
                            CssClass="btnFilterCancel" OnClientClick="OnClearFilterClick_FiltroDatas(); return false;">
                            <asp:Literal ID="Literal8" runat="server" Text="Limpar" /><div>
                            </div>
                        </asp:LinkButton>
                    </div>
                </dxpc:PopupControlContentControl>
            </ContentCollection>
        </dxpc:ASPxPopupControl>
        
        <div class="divPanel">
            <div id="container_small">
                <div id="header">
                    <asp:Label ID="lblHeader" runat="server" Text=""></asp:Label>
                </div>
                <div id="mainContent">
                    <div class="reportFilter">
                        <dxrp:ASPxRoundPanel ID="ASPxRoundPanel1" runat="server" ShowHeader="False" BackColor="White"
                            Border-BorderColor="#AECAF0">
                            <Border BorderColor="#AECAF0"></Border>
                            <PanelCollection>
                                <dxp:PanelContent runat="server">
                                    <dxtc:ASPxPageControl ID="tabPortoPar" runat="server"  Height="100%"
                                        ClientInstanceName="tabPortoPar" ActiveTabIndex="0" TabSpacing="0px" OnLoad="tabPortoParOnLoad">
                                        <TabPages>
                                            <dxtc:TabPage Text="Cadastro de Banda Carteira">
                                                <ContentCollection>
                                                    <dxw:ContentControl runat="server">
                                                        <div class="innerPanel">
                                                            <table  border="0">
                                                                <tr>
                                                                    <td>
                                                                        <div class="linkButton">
                                                                            <asp:LinkButton ID="btnAdd" runat="server" Font-Overline="false" CssClass="btnAdd"
                                                                                OnClientClick="gridCadastro.AddNewRow(); return false;">
                                                                                <asp:Literal ID="Literal3" runat="server" Text="Novo" /><div>
                                                                                </div>
                                                                            </asp:LinkButton>
                                                                            <asp:LinkButton ID="btnDelete" runat="server" Font-Overline="false" CssClass="btnDelete"
                                                                                OnClientClick=" if (confirm('Tem certeza que quer excluir?')==true) gridCadastro.PerformCallback('btnDelete');return false;">
                                                                                <asp:Literal ID="Literal4" runat="server" Text="Excluir" /><div>
                                                                                </div>
                                                                            </asp:LinkButton>
                                                                            <asp:LinkButton ID="btnFilter" runat="server" Font-Overline="false" CssClass="btnFilter"
                                                                                OnClientClick="return OnButtonClick_FiltroDatas()">
                                                                                <asp:Literal ID="Literal5" runat="server" Text="Filtro" /><div>
                                                                                </div>
                                                                            </asp:LinkButton>
                                                                            <asp:LinkButton ID="btnPDF" runat="server" Font-Overline="false" CssClass="btnPdf"
                                                                                OnClick="btnPDF_Click">
                                                                                <asp:Literal ID="Literal6" runat="server" Text="Gerar PDF" /><div>
                                                                                </div>
                                                                            </asp:LinkButton>
                                                                            <asp:LinkButton ID="btnExcel" runat="server" Font-Overline="false" CssClass="btnExcel"
                                                                                OnClick="btnExcel_Click">
                                                                                <asp:Literal ID="Literal9" runat="server" Text="Gerar Excel" /><div>
                                                                                </div>
                                                                            </asp:LinkButton>
                                                                            <asp:LinkButton ID="btnRefresh" runat="server" Font-Overline="false" CssClass="btnRefresh"
                                                                                OnClientClick=" gridCadastro.PerformCallback('btnRefresh'); return false;">
                                                                                <asp:Literal ID="Literal10" runat="server" Text="Atualizar" /><div>
                                                                                </div>
                                                                            </asp:LinkButton>
                                                                        </div>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <div class="divDataGrid">
                                                                            <dxwgv:ASPxGridView ID="gridCadastro" runat="server" EnableCallBacks="true" ClientInstanceName="gridCadastro"
                                                                                KeyFieldName="CompositeKey" DataSourceID="EsDSTabelaBandaCarteira" OnCustomUnboundColumnData="gridCadastro_CustomUnboundColumnData"
                                                                                OnRowUpdating="gridCadastro_RowUpdating" OnRowInserting="gridCadastro_RowInserting"
                                                                                OnCustomCallback="gridCadastro_CustomCallback" OnPreRender="gridCadastro_PreRender"
                                                                                OnBeforeGetCallbackResult="gridCadastro_PreRender">
                                                                                <Columns>
                                                                                    <dxwgv:GridViewCommandColumn ShowSelectCheckbox="True" VisibleIndex="0" Width="5%" ButtonType="Image" ShowClearFilterButton="True">
                                                                                        <HeaderTemplate>
                                                                                            <dxe:ASPxCheckBox ID="cbAll" ClientInstanceName="cbAll" runat="server" ToolTip="Seleciona todos os CheckBoxs de todas as páginas" BackColor="white" ClientSideEvents-CheckedChanged="function(s, e) {OnAllCheckedChanged(s, e, gridCadastro);}" OnInit="CheckBoxSelectAll"/>
                                                                                        </HeaderTemplate>
                                                                                    </dxwgv:GridViewCommandColumn>
                                                                                    <dxwgv:GridViewDataColumn FieldName="IdCarteira" VisibleIndex="1" Width="9%" CellStyle-HorizontalAlign="left" />
                                                                                    <dxwgv:GridViewDataColumn FieldName="Apelido" Caption="Nome" VisibleIndex="2" Width="32%" />
                                                                                    <dxwgv:GridViewDataDateColumn FieldName="DataReferencia" Caption="Data Referência"
                                                                                        VisibleIndex="3" Width="9%" />
                                                                                    <dxwgv:GridViewDataSpinEditColumn FieldName="LimiteInferior" Caption="Limite Inferior"
                                                                                        VisibleIndex="4" Width="25%">
                                                                                        <PropertiesSpinEdit DisplayFormatString="{0:#,##0.0000;(#,##0.0000);0.0000}">
                                                                                        </PropertiesSpinEdit>
                                                                                    </dxwgv:GridViewDataSpinEditColumn>
                                                                                    <dxwgv:GridViewDataSpinEditColumn FieldName="LimiteSuperior" Caption="Limite Superior"
                                                                                        VisibleIndex="5" Width="25%">
                                                                                        <PropertiesSpinEdit DisplayFormatString="{0:#,##0.0000;(#,##0.0000);0.0000}">
                                                                                        </PropertiesSpinEdit>
                                                                                    </dxwgv:GridViewDataSpinEditColumn>
                                                                                    <dxwgv:GridViewDataTextColumn FieldName="CompositeKey" UnboundType="String" Visible="False" />
                                                                                </Columns>
                                                                                <Templates>
                                                                                    <EditForm>
                                                                                        <div class="editForm">
                                                                                            <table>
                                                                                                <tr>
                                                                                                    <td class="td_Label">
                                                                                                        <asp:Label ID="labelCarteira" runat="server" CssClass="labelRequired" Text="Carteira:"></asp:Label>
                                                                                                    </td>
                                                                                                    <td>
                                                                                                        <dxe:ASPxSpinEdit ID="btnEditCodigoCarteira" runat="server" CssClass="textButtonEdit1"
                                                                                                            ClientInstanceName="btnEditCodigoCarteira" Text='<%# Eval("IdCarteira") %>' MaxLength="10"
                                                                                                            NumberType="Integer" OnLoad="btnEditCodigoCarteira_Load">
                                                                                                            <Buttons>
                                                                                                                <dxe:EditButton>
                                                                                                                </dxe:EditButton>
                                                                                                            </Buttons>
                                                                                                            <ClientSideEvents KeyPress="function(s, e) {document.getElementById('gridCadastro_DXPEForm_ef' + gridCadastro.cp_EditVisibleIndex + '_textNome').value = '';} "
                                                                                                                ButtonClick="function(s, e) {popupCarteira.ShowAtElementByID(s.name);}" LostFocus="function(s, e) {OnLostFocus(popupMensagemCarteira, ASPxCallback1, btnEditCodigoCarteira);}" />
                                                                                                        </dxe:ASPxSpinEdit>
                                                                                                    </td>
                                                                                                    <td colspan="2">
                                                                                                        <asp:TextBox ID="textNome" runat="server" CssClass="textNome" Enabled="false" Text='<%#Eval("Apelido")%>'></asp:TextBox>
                                                                                                    </td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td class="td_Label_Curto">
                                                                                                        <asp:Label ID="labelData" runat="server" CssClass="labelRequired" Text="Data:"></asp:Label>
                                                                                                    </td>
                                                                                                    <td colspan="4">
                                                                                                        <dxe:ASPxDateEdit ID="textDataReferencia" runat="server" ClientInstanceName="textDataReferencia"
                                                                                                            Value='<%#Eval("DataReferencia")%>' OnInit="textData_Init" />
                                                                                                    </td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td class="td_Label_Curto">
                                                                                                        <asp:Label ID="label1" runat="server" CssClass="labelRequired" Text="Limite Inferior:"></asp:Label>
                                                                                                    </td>
                                                                                                    <td colspan="4">
                                                                                                        <dxe:ASPxSpinEdit ID="textLimiteInferior" runat="server" CssClass="textValor" ClientInstanceName="textLimiteInferior"
                                                                                                            NumberType="Float" MaxLength="16" DecimalPlaces="8" Text="<%#Bind('LimiteInferior')%>">
                                                                                                        </dxe:ASPxSpinEdit>
                                                                                                    </td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td class="td_Label_Curto">
                                                                                                        <asp:Label ID="labelLimiteSuperior" runat="server" CssClass="labelRequired" Text="Limite Superior:"></asp:Label>
                                                                                                    </td>
                                                                                                    <td colspan="4">
                                                                                                        <dxe:ASPxSpinEdit ID="textLimiteSuperior" runat="server" CssClass="textValor" ClientInstanceName="textLimiteSuperior"
                                                                                                            NumberType="Float" MaxLength="16" DecimalPlaces="8" Text="<%#Bind('LimiteSuperior')%>">
                                                                                                        </dxe:ASPxSpinEdit>
                                                                                                    </td>
                                                                                                </tr>
                                                                                            </table>
                                                                                            <div class="linhaH">
                                                                                            </div>
                                                                                            <div class="linkButton linkButtonNoBorder popupFooter">
                                                                                                <asp:LinkButton ID="btnOKAdd" runat="server" Font-Overline="false" CssClass="btnSaveadd"
                                                                                                    OnInit="btnOKAdd_Init" OnClientClick="if (operacao != '') return false; operacao='salvarAdd'; callbackErro.SendCallback(); return false;">
                                                                                                    <asp:Literal ID="Literal10" runat="server" Text="OK+" /><div>
                                                                                                    </div>
                                                                                                </asp:LinkButton>
                                                                                                <asp:LinkButton ID="btnOK" runat="server" Font-Overline="false" CssClass="btnOK"
                                                                                                    OnClientClick="if (operacao != '') return false; operacao='salvar'; callbackErro.SendCallback(); return false;">
                                                                                                    <asp:Literal ID="Literal12" runat="server" Text="OK" /><div>
                                                                                                    </div>
                                                                                                </asp:LinkButton>
                                                                                                <asp:LinkButton ID="btnCancel" runat="server" Font-Overline="false" CssClass="btnCancel"
                                                                                                    OnClientClick="gridCadastro.CancelEdit(); return false;">
                                                                                                    <asp:Literal ID="Literal9" runat="server" Text="Cancelar" /><div>
                                                                                                    </div>
                                                                                                </asp:LinkButton>
                                                                                            </div>
                                                                                        </div>
                                                                                    </EditForm>
                                                                                    <StatusBar>
                                                                                        <asp:Label ID="labelFiltro" runat="server" CssClass="labelNormal" Text="" />
                                                                                    </StatusBar>
                                                                                </Templates>
                                                                                <SettingsPopup EditForm-Width="350px" />
                                                                                <%--<SettingsPager PageSize="15"></SettingsPager>--%>
                                                                                <Templates>
                                                                                    <StatusBar>
                                                                                        <asp:Label ID="labelFiltro" runat="server" CssClass="labelNormal" Text="" />
                                                                                    </StatusBar>
                                                                                </Templates>
                                                                                <SettingsCommandButton>
                                                                                    <ClearFilterButton Image-Url="../../imagens/funnel--minus.png"/>
                                                                                </SettingsCommandButton>
                                                                            </dxwgv:ASPxGridView>
                                                                        </div>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </div>
                                                    </dxw:ContentControl>
                                                </ContentCollection>
                                            </dxtc:TabPage>
                                        </TabPages>
                                        <TabPages>
                                            <dxtc:TabPage Text="Exportação Customizada">
                                                <ContentCollection>
                                                    <dxw:ContentControl runat="server">
                                                        <div id="innerPanel_Tab2">
                                                         <table border="0">
                                                            <tr>
                                                                <td>
                                                                                                                                                                        

                                                            <table border="0">
                                                                <tr>
                                                                    <td width="75px">
                                                                        <asp:Label ID="label4" runat="server" CssClass="labelNormal labelFloat" Text="Integral Trust:  "></asp:Label>
                                                                    </td>
                                                                    <td>
                                                                        <asp:Label ID="label1" runat="server" CssClass="labelRequired labelFloat" Text="Data:"></asp:Label>
                                                                    </td>
                                                                    <td>
                                                                        <dxe:ASPxDateEdit ID="textDataIntegralTrust" runat="server" ClientInstanceName="textDataIntegralTrust" />
                                                                    </td>
                                                                    <td>
                                                                        <asp:Label ID="labelCarteira" runat="server" CssClass="labelNormal" Text="Carteira:"></asp:Label>
                                                                    </td>
                                                                    <td>
                                                                        <dxgv:ASPxGridLookup ID="dropCarteira" runat="server" SelectionMode="Multiple"
                                                                            ClientInstanceName="dropCarteira" Width="400px" TextFormatString="{0}" 
                                                                            MultiTextSeparator="," EnableClientSideAPI="true"
                                                                            KeyFieldName="IdCarteira" DataSourceID="EsDSCarteira">
                                                                            <Columns>
                                                                                <dxwgv:GridViewCommandColumn ShowSelectCheckbox="True" />
                                                                                <dxwgv:GridViewDataColumn FieldName="IdCarteira" Caption="Id.Carteira" />
                                                                                <dxwgv:GridViewDataColumn FieldName="Nome" Caption="Nome" Width="80%" />                                                                                
                                                                            </Columns>
                                                                            <GridViewProperties>
                                                                                <Templates>
                                                                                    <StatusBar>
                                                                                        <table class="OptionsTable" style="float: right">
                                                                                            <tr>
                                                                                                <td>
                                                                                                    <dxe:ASPxButton ID="Fechar" runat="server" AutoPostBack="false" Text="Fechar" ClientSideEvents-Click="CloseGridLookup" />
                                                                                                </td>
                                                                                            </tr>
                                                                                        </table>
                                                                                    </StatusBar>
                                                                                </Templates>
                                                                                <Settings ShowFilterRow="True" ShowStatusBar="Visible" />
                                                                            </GridViewProperties>
                                                                            <ClientSideEvents DropDown="function(s, e) { }" />
                                                                        </dxgv:ASPxGridLookup>
                                                                    </td>
                                                                    <td>
                                                                        <dxe:ASPxTextBox ID="Arq1" runat="server" CssClass="textNormal" MaxLength="25" ClientInstanceName="Arq1" Text="Arquivo1.xls" />
                                                                    </td>
                                                                    <td>
                                                                        <dxe:ASPxTextBox ID="Arq2" runat="server" CssClass="textNormal" MaxLength="25" ClientInstanceName="Arq2" Text="Arquivo2.xls" />
                                                                    </td>                                                                                                                                        
                                                                    <td>
                                                                        <div class="linkButton linkButtonNoBorder linkButtonNoMargin">
                                                                            <asp:LinkButton ID="LinkButton1" runat="server" Font-Overline="false" CssClass="btnRun"
                                                                                OnClientClick="btnExportaClick('IntegralTrust')">
                                                                                <asp:Literal ID="Literal2" runat="server" Text="Exportar" /><div>
                                                                                </div>
                                                                            </asp:LinkButton>
                                                                        </div>
                                                                    </td>
                                                                </tr>
                                                             </table>
                                                            
                                                                </td>
                                                            </tr>

                                                            <tr>
                                                                <td>

                                                            <table border="0">
                                                                <tr>
                                                                    <td width="75px">
                                                                        <asp:Label ID="label5" runat="server" CssClass="labelNormal labelFloat" Text="Análise Risco:"></asp:Label>
                                                                    </td>
                                                                    <td>
                                                                        <asp:Label ID="label2" runat="server" CssClass="labelRequired labelFloat" Text="Data:"></asp:Label>
                                                                    </td>
                                                                    <td>
                                                                        <dxe:ASPxDateEdit ID="textDataRisco" runat="server" ClientInstanceName="textDataRisco" />
                                                                    </td>
                                                                    <td>
                                                                        <asp:Label ID="label8" runat="server" CssClass="labelRequired labelFloat" Text="Carteira:"></asp:Label>
                                                                    </td>
                                                                    <td>
                                                                        <dxe:ASPxSpinEdit ID="textCarteira" runat="server" CssClass="textNormal" MaxLength="15" DecimalPlaces="0" NumberType="Integer" />
                                                                    </td> 
                                                                    <td witdh=100>
                                                                    </td> 
                                                                    <td>
                                                                        <asp:Label ID="label7" runat="server" CssClass="labelNormal labelFloat" Text="Grupo Económico:"></asp:Label>
                                                                    </td>                                                                                                                                                                                               
                                                                    <td>                                                                        
                                                                        <dxe:ASPxComboBox ID="dropGrupoEconomico" runat="server" ClientInstanceName="dropGrupoEconomico"
                                                                            DataSourceID="EsDSGrupoEconomico" ShowShadow="false" DropDownStyle="DropDown"
                                                                            CssClass="dropDownList" TextField="Nome" ValueField="idGrupo">
                                                                        </dxe:ASPxComboBox>
                                                                    </td>
                                                                    <td>
                                                                        <div class="linkButton linkButtonNoBorder linkButtonNoMargin">
                                                                            <asp:LinkButton ID="LinkButton2" runat="server" Font-Overline="false" CssClass="btnPdf"
                                                                               OnClientClick="btnExportaClick('Risco')">
                                                                                <asp:Literal ID="Literal1" runat="server" Text="Exportar PDF" /><div>
                                                                                </div>
                                                                            </asp:LinkButton>
                                                                        </div>
                                                                    </td>
                                                                    <td>
                                                                        <div class="linkButton linkButtonNoBorder linkButtonNoMargin">
                                                                            <asp:LinkButton ID="LinkButton4" runat="server" Font-Overline="false" CssClass="btnExcel"
                                                                               OnClientClick="btnExportaClick('RiscoExcel')">
                                                                                <asp:Literal ID="Literal18" runat="server" Text="Exportar Excel" /><div>
                                                                                </div>
                                                                            </asp:LinkButton>
                                                                        </div>
                                                                    </td>
                                                                </tr>
                                                             </table>
                        
                                                            
                                                                </td>
                                                            </tr>

                                                            <tr>
                                                                <td>

                                                            <table border="0">
                                                                <tr>
                                                                    <td width="75px">
                                                                        <asp:Label ID="label6" runat="server" CssClass="labelNormal labelFloat" Text="Rentabilidades:  "></asp:Label>
                                                                    </td>
                                                                    <td>
                                                                        <asp:Label ID="label3" runat="server" CssClass="labelRequired labelFloat" Text="Data:"></asp:Label>
                                                                    </td>
                                                                    <td>
                                                                        <dxe:ASPxDateEdit ID="textDataRentabilidade" runat="server" ClientInstanceName="textDataRentabilidade" />
                                                                    </td>
                                                                    <td colspan="3">
                                                                        <div class="linkButton linkButtonNoBorder linkButtonNoMargin">
                                                                            <asp:LinkButton ID="LinkButton3" runat="server" Font-Overline="false" CssClass="btnPdf"
                                                                              OnClientClick="btnExportaClick('Rentabilidades')">
                                                                                <asp:Literal ID="Literal11" runat="server" Text="Exportar PDF" /><div>
                                                                                </div>
                                                                            </asp:LinkButton>
                                                                        </div>
                                                                    </td>
                                                                    <td>
                                                                        <div class="linkButton linkButtonNoBorder linkButtonNoMargin">
                                                                            <asp:LinkButton ID="LinkButton5" runat="server" Font-Overline="false" CssClass="btnExcel"
                                                                               OnClientClick="btnExportaClick('RentabilidadesExcel')">
                                                                                <asp:Literal ID="Literal19" runat="server" Text="Exportar Excel" /><div>
                                                                                </div>
                                                                            </asp:LinkButton>
                                                                        </div>
                                                                    </td>
                                                                </tr>
                                                            </table>
  

                                                                </td>
                                                            </tr>
                                                        </table>

                                                          
                                                        </div>
                                                    </dxw:ContentControl>
                                                </ContentCollection>
                                            </dxtc:TabPage>
                                        </TabPages>
                                        <TabPages>
                                            <dxtc:TabPage Text="De-Para Integral Trust">
                                                <ContentCollection>
                                                    <dxw:ContentControl runat="server">
                                                                                                                    
                                                     <div>
                                                        <dxwgv:ASPxGridView ID="ASPxGridView1" runat="server" AutoGenerateColumns="False"
            ClientInstanceName="grid" DataSourceID="XmlDataSource1" KeyFieldName="IdAtivo"
            OnCellEditorInitialize="ASPxGridView1_CellEditorInitialize" Width="700px"
            OnRowInserting="ASPxGridView1_RowInserting" 
            OnRowUpdating="ASPxGridView1_RowUpdating"
            OnRowDeleting="ASPxGridView1_RowDeleting">
            <Columns>
                <dxwgv:GridViewCommandColumn VisibleIndex="0" ShowEditButton="True" ShowDeleteButton="True"/>
                
                <dxwgv:GridViewDataTextColumn FieldName="IdAtivo" VisibleIndex="1" Width="20%">
                    <EditFormSettings Visible="False" />
                </dxwgv:GridViewDataTextColumn>
                
                <dxwgv:GridViewDataTextColumn FieldName="Ativo" VisibleIndex="2" Width="40%"/>                                
                <dxwgv:GridViewDataTextColumn FieldName="Codigo" Caption="Código" Width="40%" VisibleIndex="3"/>                               
            </Columns>
            <Settings ShowStatusBar="Visible" />
            <Templates>
                <StatusBar>
                    <table cellpadding="0" cellspacing="0" width="100%">
                        <tr>
                            <td align="left">
                                <dxe:ASPxButton ID="ASPxButton1" runat="server" Text="Adicione uma Nova Linha" AutoPostBack="false">
                                    <ClientSideEvents Click="function(s, e) { grid.AddNewRow(); }" />
                                </dxe:ASPxButton>
                            </td>
                        </tr>
                    </table>
                </StatusBar>
            </Templates>
            <ClientSideEvents EndCallback="OnEndCallbackEventHandler" />
            <SettingsCommandButton>
                <EditButton Text="Editar"/>
                <DeleteButton Text="Deletar"/>
                <UpdateButton Text="Atualizar"/>
                <CancelButton Text="Cancelar"/>
            </SettingsCommandButton>
        </dxwgv:ASPxGridView>
                                                     </div>
                                                                                
                                                    </dxw:ContentControl>
                                                </ContentCollection>
                                            </dxtc:TabPage>                           
                                         </TabPages>
                                        <TabPages>
                                            <dxtc:TabPage Text="Cadastro de Permissão Portal Cliente">
                                                <ContentCollection>
                                                    <dxw:ContentControl runat="server">
                                                        <div class="innerPanel">
                                                            <table border="0">
                                                                <tr>
                                                                    <td>    
                                                                    <div class="linkButton" >               
               <asp:LinkButton ID="btnAdd1" runat="server" Font-Overline="false" CssClass="btnAdd" OnClientClick="gridCadastro1.AddNewRow(); return false;"><asp:Literal ID="Literal13" runat="server" Text="Novo"/><div></div></asp:LinkButton>
               <asp:LinkButton ID="btnDelete1" runat="server" Font-Overline="false" CssClass="btnDelete" OnClientClick=" if (confirm('Tem certeza que quer excluir?')==true) gridCadastro1.PerformCallback('btnDelete');return false;"><asp:Literal ID="Literal14" runat="server" Text="Excluir"/><div></div></asp:LinkButton>
               <asp:LinkButton ID="btnPdf1" runat="server" Font-Overline="false" CssClass="btnPdf" OnClick="btnPDF_Click1"><asp:Literal ID="Literal15" runat="server" Text="Gerar PDF"/><div></div></asp:LinkButton>
               <asp:LinkButton ID="btnExcel1" runat="server" Font-Overline="false" CssClass="btnExcel" OnClick="btnExcel_Click1"><asp:Literal ID="Literal16" runat="server" Text="Gerar Excel"/><div></div></asp:LinkButton>
               <asp:LinkButton ID="btnRefresh1" runat="server" Font-Overline="false" CssClass="btnRefresh" OnClientClick=" gridCadastro1.PerformCallback('btnRefresh'); return false;"><asp:Literal ID="Literal17" runat="server" Text="Atualizar"/><div></div></asp:LinkButton>
            </div>
                                                                     </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>    
                                                                    <div class="divDataGrid">            
            <dxwgv:ASPxGridView ID="gridCadastro1" runat="server" EnableCallBacks="true" ClientInstanceName="gridCadastro1"
                KeyFieldName="IdGrupo" DataSourceID="EsDSPermissaoPortalCliente"
                OnCustomCallback="gridCadastro1_CustomCallback"
                OnRowInserting="gridCadastro1_RowInserting"
                OnRowUpdating="gridCadastro1_RowUpdating"
                >        
                    
            <Columns>           
                <dxwgv:GridViewCommandColumn VisibleIndex="0" ShowClearFilterButton="True">
                    <HeaderTemplate>
                        <dxe:ASPxCheckBox ID="cbAll" ClientInstanceName="cbAll" runat="server" ToolTip="Seleciona todos os CheckBoxs de todas as páginas" BackColor="white" ClientSideEvents-CheckedChanged="function(s, e) {OnAllCheckedChanged(s, e, gridCadastro1);}" OnInit="CheckBoxSelectAll"/>
                    </HeaderTemplate>
                </dxwgv:GridViewCommandColumn>
                
                <dxwgv:GridViewDataComboBoxColumn Caption="Descrição Grupo" FieldName="Descricao" VisibleIndex="1" Width="20%">
                    <EditFormSettings Visible="False" />
                    <PropertiesComboBox DataSourceID="EsDSGrupoUsuario" TextField="Descricao" ValueField="IdGrupo"/>
                </dxwgv:GridViewDataComboBoxColumn>
                                           
                <dxwgv:GridViewDataComboBoxColumn FieldName="PodeBoletar" Caption="Boleta" VisibleIndex="2" Width="15%" CellStyle-HorizontalAlign="Center">
                    <PropertiesComboBox EncodeHtml="false">
                        <Items>
                            <dxe:ListEditItem Value="S" Text="Sim" />
                            <dxe:ListEditItem Value="N" Text="Não" />
                        </Items>
                    </PropertiesComboBox>
                </dxwgv:GridViewDataComboBoxColumn>
                                           
                <dxwgv:GridViewDataComboBoxColumn FieldName="PodeDistribuir" Caption="Distribui" VisibleIndex="3" Width="15%" CellStyle-HorizontalAlign="Center">
                    <PropertiesComboBox EncodeHtml="false">
                        <Items>
                            <dxe:ListEditItem Value="S" Text="Sim" />
                            <dxe:ListEditItem Value="N" Text="Não" />
                        </Items>
                    </PropertiesComboBox>
                </dxwgv:GridViewDataComboBoxColumn>
                                           
                <dxwgv:GridViewDataComboBoxColumn FieldName="PodeAprovar" Caption="Aprova" VisibleIndex="4" Width="15%" CellStyle-HorizontalAlign="Center">
                    <PropertiesComboBox EncodeHtml="false">
                        <Items>
                            <dxe:ListEditItem Value="S" Text="Sim" />
                            <dxe:ListEditItem Value="N" Text="Não" />
                        </Items>
                    </PropertiesComboBox>
                </dxwgv:GridViewDataComboBoxColumn>

                <dxwgv:GridViewDataComboBoxColumn FieldName="AcessoSeguranca" Caption="Acesso Segurança" VisibleIndex="5" Width="15%" CellStyle-HorizontalAlign="Center">
                    <PropertiesComboBox EncodeHtml="false">
                        <Items>
                            <dxe:ListEditItem Value="S" Text="Sim" />
                            <dxe:ListEditItem Value="N" Text="Não" />
                        </Items>
                    </PropertiesComboBox>
                </dxwgv:GridViewDataComboBoxColumn>
                
                <dxwgv:GridViewDataComboBoxColumn FieldName="AcessoPGBL" Caption="Acessa PGBL" VisibleIndex="6" Width="15%" CellStyle-HorizontalAlign="Center">
                    <PropertiesComboBox EncodeHtml="false">
                        <Items>
                            <dxe:ListEditItem Value="S" Text="Sim" />
                            <dxe:ListEditItem Value="N" Text="Não" />
                        </Items>
                    </PropertiesComboBox>
                </dxwgv:GridViewDataComboBoxColumn>
                                                                                                             
            </Columns>
            <SettingsCommandButton>
                <ClearFilterButton Image-Url="../imagens/funnel--minus.png"/>
                <UpdateButton Image-Url="../imagens/ico_form_ok_inline.gif"/>
                <CancelButton Image-Url="../imagens/ico_form_back_inline.gif"/>
            </SettingsCommandButton>

            </dxwgv:ASPxGridView>
            
        </div>
                                                                     </td>
                                                                </tr>
                                                            </table>
                                                        </div>
                                                    </dxw:ContentControl>
                                                </ContentCollection>
                                            </dxtc:TabPage>         
                                        </TabPages>  
                                        <TabPages>
                                            <dxtc:TabPage Text="Caixa Online">
                                                <ContentCollection>
                                                    <dxw:ContentControl runat="server">
                                                        <div class="innerPanel">
                                                            <table border="0">
                                                                <tr>                                                                    
                                                                    <td>
                                                                        <asp:Label ID="labelDataSrv" runat="server" CssClass="labelRequired labelFloat" Text="Data:"></asp:Label>
                                                                    </td>
                                                                    <td>
                                                                        <dxe:ASPxDateEdit ID="textDataCaixaOnline" OnInit="textDataCaixaOnline_OnInit" runat="server" ClientInstanceName="textDataCaixaOnline" />
                                                                    </td>
                                                                    <td width="300px">
                                                                        
                                                                    </td>
                                                                    
                                                                    <td>
                                                                        <div class="linkButton linkButtonNoBorder linkButtonNoMargin">
                                                                            <asp:LinkButton ID="LinkButton6" runat="server" Font-Overline="false" CssClass="btnRun"
                                                                                OnClientClick="LoadingPanel2.Show();callbackCarrega.SendCallback('CargaInicCOL');return false;">
                                                                                <asp:Literal ID="Literal20" runat="server" Text="Carregar Saldos Iniciais" /><div>
                                                                                </div>
                                                                            </asp:LinkButton>
                                                                        </div>
                                                                    </td>
                                                                    
                                                                    <td>
                                                                        <div class="linkButton linkButtonNoBorder linkButtonNoMargin">
                                                                            <asp:LinkButton ID="LinkButton7" runat="server" Font-Overline="false" CssClass="btnRun"
                                                                                OnClientClick="if (btnAtualizarCOLEnable){LoadingPanel2.Show();callbackCarrega.SendCallback('AtualizaCOL');return false;}">
                                                                                <asp:Literal ID="Literal21" runat="server" Text="Atualizar" /><div>
                                                                                </div>
                                                                            </asp:LinkButton>
                                                                        </div>
                                                                    </td>                                                                    
                                                                </tr>
                                                                <tr>
                                                                    <td colspan="5">
                                                                    <div class="divDataGrid">
                                                                            <dxwgv:ASPxGridView ID="gridCaixaOnline" runat="server" EnableCallBacks="true" AutoGenerateColumns="false" 
                                                                            ClientInstanceName="gridCaixaOnline" DataSourceID="DSCaixaOnline" KeyFieldName="IdCliente" 
                                                                            OnCustomCallback="gridCaixaOnline_CustomCallback" Width="1050px" OnHtmlRowCreated="gridCaixaOnline_HtmlRowCreated"  
                                                                            OnHtmlDataCellPrepared="gridCaixaOnline_HtmlDataCellPrepared" OnCustomColumnDisplayText="gridCaixaOnline_CustomColumnDisplayText">
                                                                                <Columns>
                                                                                    <dxwgv:GridViewDataColumn FieldName="IdCliente" VisibleIndex="0" Visible="false"/>
                                                                                    <dxwgv:GridViewDataColumn CellStyle-Wrap="False" FieldName="Fundo" Caption="Fundo" 
                                                                                    VisibleIndex="1" Width="30%" FixedStyle="Left" />
                                                                                    <dxwgv:GridViewDataSpinEditColumn FieldName="SaldoInicial" Caption="Saldo Inicial" 
                                                                                        VisibleIndex="2" Width="10%" PropertiesSpinEdit-DisplayFormatString="{0:#,##0.00;(#,##0.00);0.00}" CellStyle-BackColor="Lavender" />
                                                                                    <dxwgv:GridViewDataSpinEditColumn FieldName="Aplicacoes" Caption="Aplicações" CellStyle-ForeColor="DarkBlue"
                                                                                        VisibleIndex="3" Width="8%" PropertiesSpinEdit-DisplayFormatString="{0:#,##0.00;(#,##0.00);0.00}" />
                                                                                    <dxwgv:GridViewDataSpinEditColumn FieldName="Resgates" Caption="Resgates" CellStyle-ForeColor="Red"
                                                                                        VisibleIndex="4" Width="8%" PropertiesSpinEdit-DisplayFormatString="{0:#,##0.00;(#,##0.00);0.00}" />
                                                                                    <dxwgv:GridViewDataSpinEditColumn FieldName="Compras" Caption="Compras" CellStyle-ForeColor="Red"
                                                                                        VisibleIndex="5" Width="8%" PropertiesSpinEdit-DisplayFormatString="{0:#,##0.00;(#,##0.00);0.00}" />
                                                                                    <dxwgv:GridViewDataSpinEditColumn FieldName="Vendas" Caption="Vendas" CellStyle-ForeColor="DarkBlue"
                                                                                        VisibleIndex="6" Width="8%" PropertiesSpinEdit-DisplayFormatString="{0:#,##0.00;(#,##0.00);0.00}" />
                                                                                    <dxwgv:GridViewDataSpinEditColumn FieldName="SaldoAtualizado" Caption="Saldo Atualizado"
                                                                                        VisibleIndex="7" Width="10%" PropertiesSpinEdit-DisplayFormatString="{0:#,##0.00;(#,##0.00);0.00}" CellStyle-BackColor="Lavender"/>
                                                                                    <dxwgv:GridViewDataSpinEditColumn FieldName="Over" Caption="Over" 
                                                                                        VisibleIndex="8" Width="8%" PropertiesSpinEdit-DisplayFormatString="{0:#,##0.00;(#,##0.00);0.00}" />
                                                                                    <dxwgv:GridViewDataSpinEditColumn FieldName="SaldoFinal" Caption="Saldo Final"
                                                                                        VisibleIndex="9" Width="10%" PropertiesSpinEdit-DisplayFormatString="{0:#,##0.00;(#,##0.00);0.00}" CellStyle-BackColor="Lavender"/>
                                                                                        
                                                                                </Columns>
                                                                              </dxwgv:ASPxGridView>
                                                                        </div>                                                                    
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </div>
                                                    </dxw:ContentControl>
                                                </ContentCollection>
                                            </dxtc:TabPage>      
                                        </TabPages>  
                                        <TabPages>
                                            <dxtc:TabPage Text="Cadastro de Carteira/Grupo Económico">
                                                <ContentCollection>
                                                    <dxw:ContentControl runat="server">
                                                        <div class="innerPanel">
                                                            <table  border="0">
                                                                <tr>
                                                                    <td>
                                                                         <div class="linkButton">
                                                                           <asp:LinkButton ID="LinkButton8" runat="server" Font-Overline="false" CssClass="btnAdd" OnClientClick="gridCadastro2.AddNewRow(); return false;"><asp:Literal ID="Literal22" runat="server" Text="Novo"/><div></div></asp:LinkButton>
                                                                           <asp:LinkButton ID="LinkButton9" runat="server" Font-Overline="false" CssClass="btnDelete" OnClientClick=" if (confirm('Tem certeza que quer excluir?')==true) gridCadastro2.PerformCallback('btnDelete');return false;"><asp:Literal ID="Literal23" runat="server" Text="Excluir"/><div></div></asp:LinkButton>
                                                                        </div>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <div class="divDataGrid">
                                                                            <dxwgv:ASPxGridView ID="gridCadastro2"  runat="server" 
                                                                            ClientInstanceName="gridCadastro2" DataSourceID="CarteiraGrupoEconomico" KeyFieldName="CompositeKey"
                                                                            EnableCallBacks="true" Width = "750"
                                                                            OnRowInserting="gridCadastro2_RowInserting" 
                                                                            OnRowUpdating="gridCadastro2_RowUpdating"                 
                                                                            OnCustomCallback="gridCadastro2_CustomCallback"
                                                                            OnCustomUnboundColumnData="gridCadastro2_CustomUnboundColumnData"
                                                                            OnCellEditorInitialize="gridCadastro2_CellEditorInitialize"                                                                           
                                                                            >
                                                                            
                                                                            <Columns>
                                                                                    <dxwgv:GridViewCommandColumn VisibleIndex="0" ShowClearFilterButton="True">
                                                                                        <HeaderTemplate>
                                                                                            <dxe:ASPxCheckBox ID="cbAll" ClientInstanceName="cbAll" Width="10%" runat="server" ToolTip="Seleciona todos os CheckBoxs de todas as páginas" BackColor="white" ClientSideEvents-CheckedChanged="function(s, e) {OnAllCheckedChanged(s, e, gridCadastro2);}" OnInit="CheckBoxSelectAll"/>
                                                                                        </HeaderTemplate>
                                                                                    </dxwgv:GridViewCommandColumn>
                                                                                
                                                                                <dxwgv:GridViewDataTextColumn FieldName="CompositeKey" UnboundType="String" Visible="False"></dxwgv:GridViewDataTextColumn>
                                                                                
                                                                                <dxwgv:GridViewDataSpinEditColumn FieldName="IdCarteira" Caption="Id Carteira" VisibleIndex="1" 
                                                                                Width="8%" PropertiesSpinEdit-NumberFormat="Number" PropertiesSpinEdit-NumberType="Integer" 
                                                                                CellStyle-HorizontalAlign="Right" HeaderStyle-HorizontalAlign="Right">
                                                                                 
                                                                                </dxwgv:GridViewDataSpinEditColumn>
                                                                                
                                                                                
                                                                                <dxwgv:GridViewDataSpinEditColumn FieldName="IdGrupoEconomico" Caption="Id Grupo Económico" VisibleIndex="2" 
                                                                                Width="14%" PropertiesSpinEdit-NumberFormat="Number" PropertiesSpinEdit-NumberType="Integer" 
                                                                                CellStyle-HorizontalAlign="Right" HeaderStyle-HorizontalAlign="Right">
                                                                                    
                                                                                </dxwgv:GridViewDataSpinEditColumn>

                                                                                <dxwgv:GridViewDataComboBoxColumn FieldName="IdCarteira" Caption="Carteira" VisibleIndex="3" Width="35%">
                                                                                <PropertiesComboBox DataSourceID="EsDSCarteira2" TextField="Nome" ValueField="IdCarteira"></PropertiesComboBox>
                                                                                </dxwgv:GridViewDataComboBoxColumn>
                                                                                
                                                                                <dxwgv:GridViewDataComboBoxColumn FieldName="IdGrupoEconomico" Caption="Grupo Económico" VisibleIndex="4" Width="24%">
                                                                                <PropertiesComboBox DataSourceID="EsDSGrupoEconomico2" TextField="Nome" ValueField="IdGrupoEconomico"></PropertiesComboBox>
                                                                                </dxwgv:GridViewDataComboBoxColumn>
                                                                                                                                                               
                                                                            </Columns>
                                                                            
                                                                            <ClientSideEvents EndCallback="OnEndCallbackEventHandler1" />
                                                                            <Settings ShowStatusBar="Hidden" />
                                                                            <SettingsCommandButton>
                                                                                <ClearFilterButton>
                                                                                    <Image Url="~/imagens/funnel--minus.png">
                                                                                    </Image>
                                                                                </ClearFilterButton>
                                                                                <UpdateButton>
                                                                                    <Image Url="~/imagens/ico_form_ok_inline.gif">
                                                                                    </Image>
                                                                                </UpdateButton>
                                                                                <CancelButton>
                                                                                    <Image Url="~/imagens/ico_form_back_inline.gif">
                                                                                    </Image>
                                                                                </CancelButton>
                                                                            </SettingsCommandButton>
                                                                        </dxwgv:ASPxGridView>
                                                                        </div>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </div>
                                                    </dxw:ContentControl>
                                                </ContentCollection>
                                            </dxtc:TabPage>      
                                        </TabPages>  
                                    </dxtc:ASPxPageControl>
                                </dxp:PanelContent>
                            </PanelCollection>
                        </dxrp:ASPxRoundPanel>
                    </div>
                </div>
            </div>
        </div>
        
        <dxwgv:ASPxGridViewExporter ID="gridExport" runat="server" GridViewID="gridCadastro" />
        <dxwgv:ASPxGridViewExporter ID="gridExport1" runat="server" GridViewID="gridCadastro1" />
        <asp:ObjectDataSource ID="DSCaixaOnline" runat="server" SelectMethod="CarregaGridCaixaOnline" TypeName="Financial.InterfacesCustom.CaixaOnline" />
        <cc1:esDataSource ID="EsDSTabelaBandaCarteira" runat="server" OnesSelect="EsDSTabelaBandaCarteira_esSelect" LowLevelBind="True" />
        <cc1:esDataSource ID="EsDSCarteira" runat="server" OnesSelect="EsDSCarteira_esSelect" />
        <cc1:esDataSource ID="EsDSGrupoEconomico" runat="server" OnesSelect="EsDSGrupoEconomico_esSelect" />
        
        <cc1:esDataSource ID="EsDSPermissaoPortalCliente" runat="server" OnesSelect="EsDSPermissaoPortalCliente_esSelect" LowLevelBind="true" />
        <cc1:esDataSource ID="EsDSGrupoUsuario" runat="server" OnesSelect="EsDSGrupoUsuario_esSelect" />
        
        <dxlp:ASPxLoadingPanel ID="LoadingPanel" runat="server" Text="Exportando arquivo, aguarde..." ClientInstanceName="LoadingPanel" Modal="true" />
        <dxlp:ASPxLoadingPanel ID="LoadingPanel2" runat="server" Text="Carregando dados, aguarde..." ClientInstanceName="LoadingPanel2" Modal="true" />
        <asp:XmlDataSource ID="XmlDataSource1" runat="server" DataFile="~/XMLCustom/PortoPar/DePara_IntegralTrust.xml"></asp:XmlDataSource>
        
        <!-- 5 aba -->
        <asp:XmlDataSource ID="CarteiraGrupoEconomico" runat="server" DataFile="~/XMLCustom/PortoPar/CarteiraGrupoEconomico.xml"></asp:XmlDataSource>    
        <cc1:esDataSource ID="EsDSCarteira2" runat="server" OnesSelect="EsDSCarteira2_esSelect" LowLevelBind="true" />
        <cc1:esDataSource ID="EsDSGrupoEconomico2" runat="server" OnesSelect="EsDSGrupoEconomico2_esSelect" LowLevelBind="true" />        
        
</div>
        
<div id="divImportacao" runat="server" visible="false">
        
        <div class="divPanel divPanelNew">                
        <div style="background-image: none !important;">
            <div style="white-space: nowrap;
	                    text-align: center;
	                    font: bold 11px Tahoma, Arial;
	                    color: #333333;
	                    background-image: url(  '../imagens/layout/nbHeaderBack.gif' );
	                    background-repeat: repeat-x;
	                    background-position: top;
	                    border-left-style: none;
	                    border-right-style: none;
	                    border-top-style: none;
	                    border-bottom: solid 1px #D5D5D5;
	                    padding: 3px 10px 3px 5px;">	                    
                <asp:Label ID="lblHeader1" runat="server" Text="Importação de Arquivos (PortoPar)"/>
            </div>
            <div style="width: 720px; margin: 0 auto;">
                <div class="reportFilter">
                    
                    <table border="0">         
                        <tr>
                            <td>
                             <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                                <ContentTemplate>
                                    <table>
                                        <tr>
                                        </tr>
                                    </table>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                            </td>

                            <td>
                            </td>
                        </tr>
                     </table>
                </div>
            </div>
        </div>
        </div>

</div>
        
</asp:Panel>        
        
    </form>
</body>
</html>