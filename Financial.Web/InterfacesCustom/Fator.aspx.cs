﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.IO;
using System.Globalization;
using Financial.Export;
using System.Text;
using Bytescout.Spreadsheet;
using System.Collections.Generic;
using System.Drawing;
using Financial.Investidor;
using Financial.Security;
using Financial.Web.Common;
using EntitySpaces.Interfaces;
using Financial.Security.Enums;
using Financial.Bolsa;
using Financial.Fundo;
using Financial.RendaFixa;
using Financial.Util;
using Financial.Fundo.Enums;
using Financial.RendaFixa.Enums;
using Financial.Tributo.Custom;
using Financial.Common;
using Financial.Common.Enums;
//using Dart.PowerTCP.Zip;
using Financial.Investidor.Enums;
using Financial.WebConfigConfiguration;
using Financial.Relatorio;

public partial class InterfacesCustom_Fator : FiltroReportBasePage
{
    // Controla Exibição de Importação ou Exportação
    private Tipo tipo;

    enum Tipo
    {
        Importacao = 0,
        Exportacao = 1
    }
    /*-----------------------------------------*/

    enum TipoExportacaoFator
    {
        FluxoCaixaExtol = 0
    }

    #region Enum p/ Exportacao
    enum OwnerExportacao
    {
        Fator = 3
    }

    enum TipoExportacao
    {
        PlanExcel = 0,
    }
    #endregion

    #region Enum p/ Importacao
    #endregion

    /// <summary>
    /// Controla Exibição de Importação ou Exportação
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    new protected void panelEdicao_Load(object sender, EventArgs e)
    {
        Control c1 = this.FindControl("divExportacao");
        Control c2 = this.FindControl("divImportacao");

        if (this.tipo == Tipo.Exportacao)
        {
            if (c1 != null) { c1.Visible = true; }
            if (c2 != null) { c2.Visible = false; }
        }
        else if (this.tipo == Tipo.Importacao)
        {
            if (c1 != null) { c1.Visible = false; }
            if (c2 != null) { c2.Visible = true; }
        }
    }

    new protected void Page_Load(object sender, EventArgs e)
    {
        //if (!Page.IsPostBack) {
        //    DateTime hoje = DateTime.Now;
        //    this.textData.Value = new DateTime(hoje.Year, hoje.Month, hoje.Day);
        //}
        this.tipo = (Tipo)Enum.Parse(typeof(Tipo), Request.QueryString["param"].ToString());

        if (this.tipo == Tipo.Exportacao)
        {

            base.Page_Load(sender, e);
        }
    }

    #region Exportação

    /// <summary>
    /// Redireciona para ExibeExportacaoCustom para Exportar a planilha excel
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void callbackExportaExcel_Callback(object source, DevExpress.Web.CallbackEventArgs e)
    {
        string queryParams = "";
        if (e.Parameter == "exp1")
        {
            

            #region Fluxo Caixa Extol

            #region Campos obrigatórios
            List<Control> controles = new List<Control>(new Control[] { this.textData });

            if (base.TestaObrigatorio(controles) != "")
            {
                e.Result = "Campos com * são obrigatórios!";
                return;
            }
            #endregion

            try
            {
                ClienteCollection clienteCollection = new ClienteCollection();
                clienteCollection.Query.Select(clienteCollection.Query.IdCliente);
                clienteCollection.Query.Where(clienteCollection.Query.TipoControle.In((byte)TipoControleCliente.Completo),
                                              clienteCollection.Query.StatusAtivo.In((byte)StatusAtivoCliente.Ativo));
                clienteCollection.Query.Load();

                int i = 0;

                string path = DiretorioAplicacao.DiretorioBaseAplicacao + WebConfig.AppSettings.DiretorioDownloads;
                if (!path.Trim().EndsWith("\\"))
                {
                    path += "\\";
                }
                path = path.Replace("\\", "\\\\");

                string login = System.Web.HttpContext.Current.User.Identity.Name;
                path += login + @"\extol\";

                // Se Diretorio Existe Apaga o Diretorio
                if (Directory.Exists(path))
                {
                    Directory.Delete(path, true);
                }

                // Cria o Diretorio
                if (!Directory.Exists(path))
                {
                    Directory.CreateDirectory(path);
                }

                List<string> nomeArquivos = new List<string>();
                foreach (Cliente cliente in clienteCollection)
                {
                    int idCliente = cliente.IdCliente.Value;

                    string nomeArquivoExcel;

                    if (new FluxoCaixaExtol(Convert.ToDateTime(this.textData.Text), idCliente)
                                    .ExportaExcel(path, out nomeArquivoExcel))
                    {
                        nomeArquivos.Add(nomeArquivoExcel);
                        i++;
                    }
                }

                if (i == 0)
                {
                    // Se Diretorio Existe Apaga o Diretorio
                    if (Directory.Exists(path))
                    {
                        Directory.Delete(path, true);
                    }

                    throw new Exception("Não há dados para gerar.");
                }
                                
                queryParams = "&path=" + HttpUtility.UrlEncode(path) + "&data=" + Convert.ToDateTime(this.textData.Text).ToString("yyyyMMdd");
                
            }
            catch (Exception erro)
            {
                e.Result = erro.Message;
                return;
            }

            #endregion
        }

        string redirect = "ExibeExportacaoCustom.aspx?Owner=" + (int)OwnerExportacao.Fator;
        switch (e.Parameter)
        {
            case "exp1":
                redirect += "&Tipo=" + (int)TipoExportacaoFator.FluxoCaixaExtol + queryParams;
                break;
        }

        string newLocation = HttpContext.Current.Request.Url.AbsolutePath.Replace("Fator.aspx", redirect);
        e.Result = "redirect:" + newLocation;
    }

    #endregion

    #region Importação
    #endregion
}