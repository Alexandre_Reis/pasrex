Ext.namespace('FDESK.win.Acao');

FDESK.win.Acao = function(config) {
	config = config || {};
	config.tableDetalheAtivoConfig = {
		xtype : 'basetable',
		ref : 'tableDetalheAtivo',
		tableCls : 'x-table-detalhe-ativo',
		width : '100%',
		readerConfig : {
			fields : [{
						name : 'PuCusto',
						type : 'float',
						header : {
							width : '20%',
							cls : 'centered',
							title : 'PU Custo M�dio'
						},
						cell : {
							cls : 'centered',
							format : '{$n$:number("0.000,00/i")}'
						}
					}, {
						name : 'Quantidade',
						type : 'float',
						header : {
							width : '20%',
							cls : 'centered',
							title : 'Quantidade'
						},
						cell : {
							cls : 'centered',
							format : '{$n$:number("0.000/i")}'
						}
					}, {
						name : 'PuMercado',
						type : 'float',
						header : {
							width : '20%',
							cls : 'centered',
							title : 'PU Mercado'
						},
						cell : {
							cls : 'centered',
							format : '{$n$:number("0.000,00/i")}'
						}
					}, {
						name : 'ValorMercado',
						type : 'float',
						header : {
							width : '20%',
							cls : 'centered',
							title : 'Valor Mercado'
						},
						cell : {
							cls : 'centered',
							format : '{$n$:number("0.000,00/i")}'
						}
					}, {
						name : 'ResultadoRealizar',
						type : 'float',
						header : {
							width : '20%',
							cls : 'centered',
							title : 'Resultado Realizar'
						},
						cell : {
							cls : 'centered',
							format : '{$n$:number("0.000,00/i")}'
						}
					}]
		}
	};

	FDESK.win.Acao.superclass.constructor.call(this, config);

};

Ext.extend(FDESK.win.Acao, FDESK.win.SeletorPeriodo, {
			title : 'A��o',
			fieldDescricao : 'CdAtivoBolsa',
			// tables : 'Acao',
			posicaoSinteticaMethod : 'posicaoBolsaSintetica',
			operacaoSinteticaMethod : 'operacaoBolsaSintetica',
			operacaoAnaliticaMethod : 'operacaoBolsaAnalitica'
		});
