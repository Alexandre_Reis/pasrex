Ext.namespace('FDESK.win.Usuario');
FDESK.win.Usuario = function(config) {
	Ext.apply(this, config);
	
	this.iframe = new Ext.Component({
				autoEl : {
					tag : 'iframe',
					width : '100%',
					height: '130px',
                    style : 'border: 0px none; visibility: hidden;',
					scrolling : 'no'
				}
			});
	
	FDESK.win.Usuario.superclass.constructor.call(this, {
				width : 690,
				height : 196,
				autoScroll : true,
				resizable : false,
				modal : true,
				closeAction : 'hide',
				buttons : [{
							text : this.butOkText,
							id : 'btn-ok-' + this.tipoRegistro.toLowerCase(),
							//iconCls : 'icon-disk',
							scope : this
						}, {
							text : this.butCancelText,
							cls : 'x-btn-plain',
							//iconCls : 'icon-minus-circle',

							handler : this.hide.createDelegate(this, [])
						}],

				items : this.iframe

			});

	var events = {};
	events['aftersave'] = true;
	this.addEvents(events);
	this.on('aftersave', this.aftersave, this);
};

Ext.extend(FDESK.win.Usuario, Ext.Window, {
			title : 'Usu�rio',
			tipoRegistro : 'Usuario',
			butOkText : 'OK',
			butCancelText : 'Cancelar',

			updateForm : function(config, show) {
    			this.show();
                this.updateIframeSrc(FDESK.AS.paths.base + '/login/alterarprofile.aspx?extpanelid=' + this.id);
			},

            aftersave: function(params){
                if(params.errorCode === 0){
                    this.hide();
                }
            },
			
			updateIframeSrc : function(url){

                var iframeDom = this.iframe.getEl().dom;

			    iframeDom.src =  url;
	            iframeDom.style.visibility = 'hidden';
	            
	            this.iframe.getEl().on('load', function(){
	                this.iframe.document = iframeDom.contentDocument || iframeDom.contentWindow.document;
                    this.iframeLoaded();
	            }, this);
            },
            iframeLoaded : function(){
    		    var innerTable = this.iframe.document.getElementById('pnlPerfil_RPC');
    		    if(innerTable){
    		        var containerSmall = this.iframe.document.getElementById('container_small');
    		        containerSmall.innerHTML = innerTable.innerHTML;
    		        containerSmall.style.backgroundColor = '#DFE8F6';
    		        containerSmall.style.height = '120px';
    		    }
    		    this.btnSaveIframe = this.iframe.document.getElementById('pnlPerfil_LinkButton1');
    		    if(this.btnSaveIframe){
    		        this.btnSaveIframe.parentNode.parentNode.style.display = 'none';
    		    }
    		    
    		    var btnOK = Ext.getCmp('btn-ok-' + this.tipoRegistro.toLowerCase());
    		    btnOK.setHandler(function(){this.btnSaveIframe.onclick();}, this);
    		    
    		    this.iframe.getEl().dom.style.visibility = 'visible';
	        }
		});

