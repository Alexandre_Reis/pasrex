Ext.namespace('FDESK.win.Help');

FDESK.win.Help = function(config) {

    this.iframe = new Ext.Component({
				autoEl : {
					tag : 'iframe',
					width : '100%',
					height: '420px',
                    style : 'border: 0px none; visibility: hidden;',
					scrolling : 'no'
				}
			});

	Ext.apply(config, {
	            items : [this.iframe], 
				buttons : [{
							text : this.butOkText,
							handler : this.hide.createDelegate(this, [])
						}]
			});

	FDESK.win.Help.superclass.constructor.call(this, config);

};

Ext.extend(FDESK.win.Help, Ext.Window, {
			title : 'Ajuda',
			butOkText : 'OK',
			width : 966,
			height : 486,
			resizable : false,
			cls : 'solve-window',
			modal : true,
			closeAction : 'hide',

			updateForm : function(config, show, extraParams) {
			    if (!this.rendered) {
					this.show();
				}
				var url = String.format('{0}/help.aspx?topic={1}&app={2}',
				    FDESK.AS.paths.helpBase,
				    config.topic,
				    FDESK.AS.g_AppId);
				this.updateIframeSrc(url);
                this.afterUpdateForm();
			},

			afterUpdateForm : function() {
				this.show();
			},
			
			updateIframeSrc : function(url){

                var iframeDom = this.iframe.getEl().dom;

			    iframeDom.src =  url;
	            iframeDom.style.visibility = 'hidden';
	            
	            this.iframe.getEl().on('load', function(){
	                this.iframe.document = iframeDom.contentDocument || iframeDom.contentWindow.document;
                    this.iframeLoaded();
	            }, this);
	            
            },
			
			iframeLoaded : function(){
    		    this.iframe.getEl().dom.style.visibility = 'visible';
    		    this.getIframePanelContainer().style.overflowY = 'scroll';
    		    this.getIframePanelContainer().style.overflowX = 'hidden';
                this.getIframePanelContainer().style.height = (this.iframe.getEl().getHeight()-10) + 'px';
	        },
	        getIframePanelContainer : function(){
			    return this.iframe.document && this.iframe.document.getElementById('content');
			}
			
		});