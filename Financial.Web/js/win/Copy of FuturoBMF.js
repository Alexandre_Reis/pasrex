Ext.namespace('FDESK.win.FuturoBMF');

FDESK.win.FuturoBMF = function(config) {
	config = config || {};
	config.tableDetalheAtivoConfig = {
		xtype : 'basetable',
		tableCls : 'x-table-detalhe-ativo',		
		ref : 'tableDetalheAtivo',
		width : '100%',
		readerConfig : {
			fields : [{
						name : 'AjusteDiario',
						type : 'float',
						header : {
							width : '20%',
							cls : 'centered',
							title : 'Ajuste Diario'
						},
						cell : {
							cls : 'centered',
							format : '{$n$:number("0.000,00/i")}'
						}
					}, {
						name : 'DataVencimento',
						type : 'string',
						header : {
							width : '20%',
							cls : 'centered',
							title : 'Data Vencimento'
						},
						cell : {
							cls : 'centered data'
						}
					}, {
						name : 'Quantidade',
						type : 'float',
						header : {
							width : '20%',
							cls : 'centered',
							title : 'Quantidade'
						},
						cell : {
							cls : 'centered',
							format : '{$n$:number("0.000/i")}'
						}
					}, {
						name : 'PuMercado',
						type : 'float',
						header : {
							width : '20%',
							cls : 'centered',
							title : 'PU Mercado'
						},
						cell : {
							cls : 'centered',
							format : '{$n$:number("0.000,00/i")}'
						}
					}, {
						name : 'Serie',
						type : 'float',
						header : {
							width : '20%',
							cls : 'centered',
							title : 'Serie'
						},
						cell : {
							cls : 'centered',
							format : '{$n$:number("0.000,00/i")}'
						}
					}]
		}
	};
	
	FDESK.win.FuturoBMF.superclass.constructor.call(this, config);

};

Ext.extend(FDESK.win.FuturoBMF, FDESK.win.SeletorPeriodo, {
			title : 'Futuro BMF',
			fieldDescricao : 'CdAtivoBMF',
			//tables : 'FuturoBMF',
			posicaoSinteticaMethod : 'posicaoFuturoBMFSintetica',			
			operacaoAnaliticaMethod : 'operacaoBMFAnalitica'
					});
		
