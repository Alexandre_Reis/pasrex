﻿$(document).ready(function() {
	$("#spaceused1").progressBar({width: 240});
});

var Financial = {};
Financial.Util = {};
Financial.Util.ProgressBar = {};

Financial.Util.ProgressBar.PrepareProcess = function(config)
{   
    for(prop in config){
        Financial.Util.ProgressBar[prop] = config[prop];
    }
    var grid = Financial.Util.ProgressBar.Grid;
    grid.GetSelectedFieldValues('IdCliente;Apelido', OnGetSelectedFieldValuesDiario);    
}

function OnGetSelectedFieldValuesDiario(selectedValues)
{  
    var idClientes=[], nomeClientes=[];
    if(selectedValues.length == 0) return;
     
    for(i = 0; i < selectedValues.length; i++) 
    {
        idClientes.push(selectedValues[i][0]);
        nomeClientes.push(selectedValues[i][1]);
    }
    Financial.Util.ProgressBar.StartProcess(idClientes, nomeClientes);        
}

Financial.Util.ProgressBar.StopProcess = function() {
    Financial.Util.ProgressBar.halt = true;
    $("#client-info").html('Aguarde... O processamento será interrompido após o cliente atual ser processado');
    
}

Financial.Util.ProgressBar.StartProcess = function(idClientes, nomeClientes) 
{   
    Financial.Util.ProgressBar.IdClientes = idClientes;
    Financial.Util.ProgressBar.NomeClientes = nomeClientes;
    Financial.Util.ProgressBar.ResultadoProcessamentoClientes = {};
    Financial.Util.ProgressBar.ResultadoProcessamentoClientes.Erros = [];
    
    $('#spaceused1').progressBar(0);
    progressWindow.Show();
    $('#progressWindow_btnClose').hide();
    $('#progressWindow_btnStop').show();
    $('#progressWindow_btnErrors').hide();
    
    $('#total-erros').html('0');
    $('#wrapper-erros').hide();
    $('#total-sucessos').html('0');
    
    Financial.Util.ProgressBar.halt = false;
    
    //Ordenar lista de processamento de clientes (regras de processar filhos antes das maes, etc.)
    $.ajax({
        type: 'post',
	    url: '../ashx/OrdenaCarteirasProcessamento.ashx',
	    dataType: 'json',
	    cache: false,
	    data: {idClientes:idClientes.join(',')},
	    success: function(data){
	        var idsOrdenados = data;
	        var idsDesordenados = Financial.Util.ProgressBar.IdClientes;
	        var nomesOrdenados = [];
	        
	        for(var countIdOrdenado = 0; countIdOrdenado < idsOrdenados.length; countIdOrdenado++){
	            var idOrdenado = idsOrdenados[countIdOrdenado];
	            //Encontrar idOrdenado na lista desordenada
	            for(var countIdDesordenado=0; countIdDesordenado < idsDesordenados.length; countIdDesordenado++){
	                var idDesordenado = idsDesordenados[countIdDesordenado];
	                if(idDesordenado === idOrdenado){
	                    nomesOrdenados.push(Financial.Util.ProgressBar.NomeClientes[countIdDesordenado]);
	                    break;
	                }
	            }
	        }
	        Financial.Util.ProgressBar.IdClientes = idsOrdenados;
	        Financial.Util.ProgressBar.NomeClientes = nomesOrdenados;
	        Financial.Util.ProgressBar.ProcessClient(0);        
	    }
	});
    
    
    return true;
}

Financial.Util.ProgressBar.ProcessClient = function(index)
{
    var idCliente = Financial.Util.ProgressBar.IdClientes[index];
    var nomeCliente = Financial.Util.ProgressBar.NomeClientes[index];
    
    $("#client-info").html('Processando: ' + nomeCliente);
  
    var ajaxUrl = Financial.Util.ProgressBar.ProcessaContabil === true ? 
                    "ControllerProcessaContabil.ashx" : 
                    Financial.Util.ProgressBar.ProcessaRentabilidade === true ? 
                        "ControllerProcessaRentabilidade.ashx" :
                        "ControllerProcessaCliente.ashx";
    
	$.ajax({
	    type: 'post',
	    url: ajaxUrl,
	    dataType: 'json',
	    cache: false,
	    data: {idCliente : idCliente, 
	           tipoProcesso : Financial.Util.ProgressBar.TipoProcesso,
	           processaBolsa : Financial.Util.ProgressBar.ProcessaBolsa,
	           processaBMF : Financial.Util.ProgressBar.ProcessaBMF,
	           processaRendaFixa : Financial.Util.ProgressBar.ProcessaRendaFixa,
	           processaSwap : Financial.Util.ProgressBar.ProcessaSwap,
	           processaFundo : Financial.Util.ProgressBar.ProcessaFundo,
	           processaContabil : Financial.Util.ProgressBar.ProcessaContabil,
	           processaCambio : Financial.Util.ProgressBar.ProcessaCambio,
	           integraBolsa : Financial.Util.ProgressBar.IntegraBolsa,
	           integraBMF : Financial.Util.ProgressBar.IntegraBMF,
	           integraRendaFixa : Financial.Util.ProgressBar.IntegraRendaFixa,
	           integraCC : Financial.Util.ProgressBar.IntegraCC,
	           ignoraOperacoes: Financial.Util.ProgressBar.IgnoraOperacoes,
	           dataInicio : Financial.Util.ProgressBar.DataInicio,
	           dataFinal : Financial.Util.ProgressBar.DataFinal,
	           mantemFuturo : Financial.Util.ProgressBar.MantemFuturo,
	           remuneraRF : Financial.Util.ProgressBar.RemuneraRF,
	           cravaCota : Financial.Util.ProgressBar.CravaCota,
	           resetCusto : Financial.Util.ProgressBar.ResetCusto,
	           zeragemContabil : Financial.Util.ProgressBar.ZeragemContabil,
			   perfil: Financial.Util.ProgressBar.Perfil,
			   processaRentabilidade: Financial.Util.ProgressBar.ProcessaRentabilidade,
			   tipoProcessamento: Financial.Util.ProgressBar.TipoProcessamento
	    },
	    //async: false,
	    success: function(data) {
	    
	        if(data.success === false){ 
	            var errorHtml = '<span class="warning">Cliente ' + idCliente + " - " + nomeCliente +': ' + data.errorMessage + '</span>' +
                    '<div class="container"><div class="header"><span>+ Detalhes</span></div><div class="content">' + data.errorDetail + '</div></div>';
	            
	            Financial.Util.ProgressBar.ResultadoProcessamentoClientes.Erros.push(errorHtml);
	            
	            $('#total-erros').html(Financial.Util.ProgressBar.ResultadoProcessamentoClientes.Erros.length);
	            $('#wrapper-erros').show();
	            
	        }else{
	            $('#total-sucessos').html((index+1) - Financial.Util.ProgressBar.ResultadoProcessamentoClientes.Erros.length);
	        }
	    
		    $('#spaceused1').progressBar(((index + 1) / Financial.Util.ProgressBar.IdClientes.length) * 100);
	    
		    //Checar se processamos o ultimo
		    if(index == (Financial.Util.ProgressBar.IdClientes.length - 1)){
		        $("#client-info").html('Fim do Processamento');
		        
		        var numErros = Financial.Util.ProgressBar.ResultadoProcessamentoClientes.Erros.length;
		        if(numErros > 0){
		            var errorHtml = Financial.Util.ProgressBar.ResultadoProcessamentoClientes.Erros.join('');
		        
		            $("#output-info").html(errorHtml);
	            
	                $(".header").click(function () {
                    
                        $header = $(this);
                        $content = $header.next();
                        $content.slideToggle(200, function () {
                            $header.text(function () {
                            return $content.is(":visible") ? "- Detalhes" : "+ Detalhes";
                            });
                        });
                    });
                    $('#progressWindow_btnErrors').show();
		        }else{
		            $('#progressWindow_btnErrors').hide();
		        }
		        
		        $('#progressWindow_btnClose').show();
                $('#progressWindow_btnStop').hide();
                window.processing = false;                
		        return;
		    }
		    
		    //Checar variavel global de interrupcao antes de continuar
            if(Financial.Util.ProgressBar.halt === true){
                progressWindow.Hide();                
                window.processing = false;
                return;
            }
            
            //Encadear chamada do proximo recursivamente
            Financial.Util.ProgressBar.ProcessClient(index + 1);            
        }
    });        
}
