Ext.namespace('FDESK.table.BaseTable');

FDESK.table.BaseTable = function(config) {
	var reader = new Ext.data.JsonReader(config.readerConfig);
	var store = new Ext.data.Store({
				data : config.data || [],
				reader : reader
			});

	var tpl;
	if (config.tpl) {
		tpl = config.tpl;
	} else {
		var ths = [], tds = [];
		var thTemplate = '<th width="{0}" class="{1}">{2}</th>';
		var tdTemplate = '<td class="{0}">{1}</td>';
		for (var i = 0; i < config.readerConfig.fields.length; i++) {
			var field = config.readerConfig.fields[i];
			if (field.header) {
				ths.push(String.format(thTemplate, field.header.width,
						field.header.cls, field.header.title));

				field.cell = field.cell || {
					cls : '',
					format : ''
				};

				tds.push(String.format(tdTemplate, field.cell.cls,
						(field.cell.format && field.cell.format.replace('$n$',
								field.name))
								|| String.format('{{0}}', field.name)));

			};

		}

		tpl = new Ext.XTemplate(
				'<table class="x-table-financial ' + (config.tableCls || "")
						+ '" width="' + config.width
						+ '" cellspacing="0" cellpadding="0">',
				'<thead>',
				(config.tplTopHeader || ''),
				'<tr>',
				ths.join(''),
				'</tr>',
				'</thead>',
				'<tbody height="' + (config.scrollerHeight || 'auto') + '">',
				'<tpl for=".">',
				'<tr class="selector {[xindex % 2 === 0 ? "dark-bg" : "light-bg"]}">',
				tds.join(''), '</tr>', '</tpl>', '</tbody>', '</table>');
	}

	Ext.apply(config, {
				store : store,
				tpl : tpl,
				itemSelector : config.itemSelector || 'tr.selector',
				height : Ext.isIE ? config.scrollerHeight : config.height
			});

	FDESK.table.BaseTable.superclass.constructor.call(this, config);
	this.addEvents({
				'refresh' : true
			});
};

Ext.extend(FDESK.table.BaseTable, Ext.DataView, {
			loadData : function(data) {
				this.store.loadData(data);
			},
			refresh : function() {
				FDESK.table.BaseTable.superclass.refresh.call(this);
				this.fireEvent('refresh', this);
			}
		});

Ext.reg('basetable', FDESK.table.BaseTable);