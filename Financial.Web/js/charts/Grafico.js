Ext.namespace('FDESK.panel.Grafico');
FDESK.panel.Grafico = function(config) {

	this.id = Ext.id();
	if (config) {
		Ext.apply(this, config);
	}

	this.chartDivId = 'chart-div-' + this.id;

	this.panelGrafico = new Ext.Panel({
				region : 'center',
				items : {
					xtype : 'box',
					autoEl : {
						tag : 'div',
						id : this.chartDivId
					}
				}
			});

	this.items = [this.panelGrafico];

	this.menuChartTypesId = 'menu-charttypes-' + this.id;
	var menuChartTypes = {
		id : this.menuChartTypesId,
		items : []
	};

	for (var chartTypeConfigKey in this.chartTypeConfigs) {
		var c = this.chartTypeConfigs[chartTypeConfigKey];

        //Graficos de volatilidade e industria nao existem hoje para carteira simulada - ignora-los
        if((chartTypeConfigKey == 'Industria' || chartTypeConfigKey == 'Volatilidade')
            && FDESK.AS.user.isCarteiraSimulada){
            continue;
        }

		if (c.chartGroup === config.chartGroup) {
			menuChartTypes.items.push({
						text : c.caption,
						scope : this,
						group : 'chartType' + this.id,
						checked : false,
						chartTypeConfigKey : chartTypeConfigKey,
						checkHandler : function(b) {
							if (b.checked) {
								this.updateChart(b.chartTypeConfigKey, {});
							}
						}
					});
		}
	}

	this.menuExibirId = 'menu-exibir-' + this.id;
	var menuExibir = {
		id : this.menuExibirId,
		items : []
	};

	var btnValuesPercent = {
		text : 'Percentual',
		ref : '../btnValuesPercent',
		group : 'ShowValues' + this.id,
		checked : true,
		scope : this,
		checkHandler : function(b) {
			if(b.checked){
				b.parentMenu.ownerCt.setText(b.text);
				this.chartInfo.showPercentageValues = FDESK.AS.onValue;
				this.renderChart();
			}
		}
	};

	var btnValuesAbsolute = {
		text : 'Valores',
		ref : '../btnValuesAbsolute',
		scope : this,
		group : 'ShowValues' + this.id,
		checked : false,
		checkHandler : function(b) {
			if(b.checked){
				b.parentMenu.ownerCt.setText(b.text);
				this.chartInfo.showPercentageValues = FDESK.AS.offValue;
				this.renderChart();
			}
		}
	};
	
	var menuValuesDisplay = {
		items: [btnValuesPercent, btnValuesAbsolute]
	};
	
	this.tbar = [{
				text : 'Gr�fico',
				iconCls : 'icon-chart',
				hidden: this.showMenuChartTypes !== true,
				menu : menuChartTypes
			}, {
				text : 'Exibir',
				iconCls : 'icon-eye',
				menu : menuExibir
			}, '->',  {
				ref : '../menuValuesDisplay',
				text : 'Percentual',
				iconCls : 'icon-pie-chart',
				menu : menuValuesDisplay
			}];


	this.chartInfo = {
		showValues : FDESK.AS.offValue
	};

	FDESK.panel.Grafico.superclass.constructor.call(this, {
				layout : 'border',
				cls : 'panel-grafico'
			});
	this.on('afterlayout', this.onAfterLayout, this);
	this.on('afterrender', function() {
				if (this.toolbarDirty) {
					this.refreshToolbar();
				}
			}, this, {
				single : true
			});

};

Ext.extend(FDESK.panel.Grafico, Ext.Panel, {
	chartTypeConfigs : {
		Alocacao : {
			caption : 'Aloca��o por Estrat�gia',
			renderConfigs : ['Pie', 'Column3D'],
			chartGroup : 'SingleSeries'
		},
		Ativo : {
			caption : 'Aloca��o por Ativo',
			renderConfigs : ['Pie', 'Column3D'],
			chartGroup : 'SingleSeries'
		},
		Gestor : {
			caption : 'Gestor',
			renderConfigs : ['Pie', 'Column3D'],
			chartGroup : 'SingleSeries'
		},
		Industria : {
			caption : 'Ind�stria',
			renderConfigs : ['Pie', 'Column3D'],
			chartGroup : 'SingleSeries'
		},
		/*Risco : {
			caption : 'Risco',
			renderConfigs : ['Column3D', 'Pie'],
			chartGroup : 'SingleSeries'
		},*/
		RetornoAcumulado : {
			caption : 'Retorno Acumulado',
			renderConfigs : ['MSLine', 'MSColumn3D'],
			chartGroup : 'MultiSeries'
		},
		/*RetornoEfetivo : {
			caption : 'Retorno Efetivo',
			renderConfigs : ['MSLine', 'MSColumn3D'],
			chartGroup : 'MultiSeries'
		},*/
		RetornoMeses : {
			caption : 'Retorno 12 Meses',
			renderConfigs : ['MSColumn3D', 'MSLine'],
			chartGroup : 'MultiSeries'
		}/*,
		Volatilidade : {
			caption : 'Vol',
			renderConfigs : ['MSLine', 'MSColumn3D'],
			chartGroup : 'MultiSeries'
		}*/
	},

	chartRenderConfigs : {
		Pie : {
			title : 'Gr�fico Pizza',
			swf : 'Pie2D',
			// attributes : "bgAlpha='100' palette='3' labelSepChar = '&#xD;'
			// attributes : "bgAlpha='100' palette='3' labelSepChar = ': '
			// pieRadius = '100' bgColor='e8f1ff'",
			attributes : "bgAlpha='100' palette='3' labelSepChar = ': ' bgColor='e8f1ff'",
			animation : '1',
			showValues : '1',
			showPercentageValues : '1',
			multiSeries : false
		},
		Column3D : {
			title : 'Gr�fico Colunas',
			swf : 'Column3D',
			attributes : "decimals='1' formatNumberScale='0' bgColor='e8f1ff' bgAlpha='100'  slantLabels='0' rotateLabels='1' ",
			animation : '1',
			showPercentageValues : null,
			showValues : '1',
			multiSeries : false
		},
		MSColumn3D : {
			title : 'Gr�fico Colunas',
			swf : 'MSColumn3D',
			attributes : "shownames='1' decimals='1' numberSuffix='%25' bgColor='e8f1ff' bgAlpha='100'  slantLabels='0' rotateLabels='1' ",
			animation : '1',
			showValues : '0',
			showPercentageValues : null,
			multiSeries : true
		},
		MSLine : {
			title : 'Gr�fico Linhas',
			swf : 'MSLine',
			attributes : "lineThickness='2' showValues='0' formatNumberScale='0' numberSuffix='%25' anchorRadius='1'  bgColor='e8f1ff' bgAlpha='100' divLineColor='BBBBBB' showAlternateHGridColor='1' alternateHGridColor='EEEEEE' shadowAlpha='0' showShadow='0' numvdivlines='0'  chartRightMargin='35' bgColor='FFFFFF' bgAngle='270' legendbgcolor='f0f3ed' legendbordercolor='c6cfb8' slantLabels='0' rotateLabels='1'",
			animation : '1',
			showPercentageValues : null,
			showValues : '1',
			multiSeries : true
		}
	},

	decimalSeparator : ',',
	thousandSeparator : '.',

	updateChart : function(chartType, loadDataParams) {
		if (chartType) {
			this.setChartType(chartType);
		}

		if (loadDataParams) {
			if (!loadDataParams.chartType) {
				loadDataParams.chartType = chartType
						|| this.chartInfo.chartType;
			}

			this.loadChartData(loadDataParams, this.renderChart);
		} else {
			this.renderChart();
		}
	},

	refreshMenuExibir : function() {
		var menu = Ext.getCmp(this.menuExibirId);
		if (!menu) {
			this.toolbarDirty = true;
			return;
		}

		menu.removeAll();
		Ext.each(this.chartInfo.renderConfigs, function(renderConfig, i) {
					var renderConfigObj = this.chartRenderConfigs[renderConfig];
					menu.add({
								text : renderConfigObj.title,
								renderConfigObj : renderConfigObj,
								group : 'renderConfig' + this.id,
								checked : i === 0,
								scope : this,
								checkHandler : function(b) {
									if (b.checked) {
										this.setRenderConfig(b.renderConfigObj);
										this.renderChart();
									}
								}
							});
				}, this);

	},

	refreshMenuChartTypes : function() {
		var menu = Ext.getCmp(this.menuChartTypesId);
		if (!menu) {
			this.toolbarDirty = true;
			return;
		}

		var items = menu.items;
		menu.items.get(0).setChecked(true);
	},

	refreshBtnValues : function() {
		var actionBtnValues = 'hide';
		if (this.chartInfo.showPercentageValues !== null) {
			this.menuValuesDisplay.menu.items.get(0).setChecked(true, true);
			this.menuValuesDisplay.menu.items.get(1).setChecked(false, true);
			actionBtnValues = 'show';
		}
		this.menuValuesDisplay[actionBtnValues]();
	},

	loadChartData : function(params, onLoad) {
		if (!params.date) {
			params.date = FDESK.AS.dataReferencia.format('Y-m-d');
		}

		if (!params.idIndice) {
			params.idIndice = FDESK.AS.idIndice;
		}
		
		params.idCliente = FDESK.AS.user.idCliente;

        if (!params.explodeFundos) {
			params.explodeFundos = FDESK.AS.explodeFundos;
		}

		Ext.Ajax.request({
					scope : this,
					method : 'GET',
					params : params,
					url : FDESK.AS.g_WebDbName + '/ashx/Chart.ashx',
					failure : function(response) {
						var obj = Ext.decode(response.responseText);
						FDESK.AS.errHandler(obj && obj.erros);
					},
					success : function(response) {
						var data = Ext.decode(response.responseText);
						this.setChartData(data);
						if (onLoad) {
							onLoad.call(this);
						}
					}
				});
	},

	refreshToolbar : function() {
		this.refreshMenuExibir();
		this.refreshBtnValues();
	},

	setChartType : function(chartType) {
		// Este m�todo pode ser utilizado tanto para trocar o tipo de grafico
		// (alocacao, gestor, etc.)
		// quanto para o tipo de renderizacao (pie, mscolumn, etc)

		var chartTypeConfig = this.chartTypeConfigs[chartType];

		this.chartInfo.chartType = chartType;
		this.chartInfo.animation = FDESK.AS.onValue;
		Ext.apply(this.chartInfo, chartTypeConfig);
		Ext.apply(this.chartInfo,
				this.chartRenderConfigs[chartTypeConfig.renderConfigs[0]]);
		this.refreshToolbar();
		this.setTitle(chartTypeConfig.caption);
	},

	setRenderConfig : function(renderConfig) {
		this.chartInfo.animation = FDESK.AS.onValue;
		Ext.apply(this.chartInfo, renderConfig);
		this.refreshBtnValues();
	},

	setChartData : function(chartData) {
		this.chartInfo = this.chartInfo || {};
		this.chartInfo.data = {};
		Ext.apply(this.chartInfo.data, chartData);
	},

	onAfterLayout : function() {
		if (!this.chartRendered) {
			return;
		}
		var bWidth = this.panelGrafico.body.getWidth();
		var bHeight = this.panelGrafico.body.getHeight();

		if (bWidth < 50 || bHeight < 50) {
			return;
		} else if (bWidth == this.lastPanelGraficoWidth
				&& bHeight == this.lastPanelGraficoHeight) {
			return;
		}

		this.lastPanelGraficoWidth = bWidth;
		this.lastPanelGraficoHeight = bHeight;

		this.renderChart();
	},

	renderChart : function() {
		/*
		 * if(!(this.chartInfo && this.chartInfo.data)){ return; }
		 */

		if (!this.gridDiv) {
			var gridDivEl = Ext.get(this.chartDivId);
			if (!gridDivEl) {
				return;
			}
			this.gridDiv = Ext.get(this.chartDivId).dom;

		}

		// Desligar evento de resize para que n�o interfira no rendering do
		// chart
		this.panelGrafico.removeListener('resize', this.onAfterLayout);

        var chartWidth = this.lastPanelGraficoWidth || this.panelGrafico.body.getWidth();
        var bodyWidth = Ext.getBody().getWidth();
        var singleColumnWidth = bodyWidth - 70;
        var twoColumnWidth = singleColumnWidth / 2 - 20;
        chartWidth = twoColumnWidth;

		var chart = new FusionCharts(
				String.format("charts/{0}.swf", this.chartInfo.swf),
				("grafico-" + this.id),
				chartWidth,
				this.lastPanelGraficoHeight
						|| this.panelGrafico.body.getHeight(), "0", "0");

		var xml = this.buildChartXML();
		chart.setDataXML(xml);

		this.chartInfo.animation = FDESK.AS.offValue;

		chart.render(this.gridDiv);
		this.panelGrafico.on('resize', this.onAfterLayout, this);

		this.chartRendered = true;
	},

	serializeAttributes : function(o, invalidProps, joinChar) {
		var props = [];
		for (var prop in o) {
			if (!invalidProps || (invalidProps.indexOf(prop) == -1)) {
				props[props.length] = String.format("{0}='{1}'", prop, o[prop]);
			}
		}

		return props.join(joinChar || ' ');
	},

	buildChartXML : function() {
		var data = this.chartInfo.data;
		sbuffer = [];

        var labelStepText = "";		
		if (data.categories){
		    var MAX_CATEGORIES = 14;
		    var numCategories = data.categories.length;
            var labelStep = (numCategories <= MAX_CATEGORIES) ? 1 : Math.ceil(numCategories / MAX_CATEGORIES);
            labelStepText = "labelStep = '" + labelStep + "'";
		}
		
		
		sbuffer[sbuffer.length] = String
				.format(
						"<chart {0} animation='{1}' showValues='{2}' caption='{3}' decimalSeparator='{4}' showPercentageValues = '{5}' thousandSeparator='{6}' {7}>",
						this.chartInfo.attributes, this.chartInfo.animation,
						this.chartInfo.showValues, this.chartInfo.caption,
						this.decimalSeparator,
						this.chartInfo.showPercentageValues, 
						this.thousandSeparator,
						labelStepText
						);

		if (data.categories) {
			sbuffer[sbuffer.length] = "<categories>";
			Ext.each(data.categories, function(category, i) {
						if (!this.chartInfo.multiSeries) {
							data.datasets[0].sets[i]['Label'] = category.label;
						}
						sbuffer[sbuffer.length] = String.format('<{0} {1} />',
								'category', this.serializeAttributes(category));
					}, this);
			sbuffer[sbuffer.length] = "</categories>";

		}

		Ext.each(data.datasets, function(dataset) {
					if (this.chartInfo.multiSeries) {
						sbuffer[sbuffer.length] = String.format('<{0} {1}>',
								'dataset', this.serializeAttributes(dataset,
										['sets']));
					}
					Ext.each(dataset.sets, function(set) {
								sbuffer[sbuffer.length] = String.format(
										'<{0} {1} />', 'set', this
												.serializeAttributes(set));
							}, this);
					if (this.chartInfo.multiSeries) {
						sbuffer[sbuffer.length] = "</dataset>";
					}

				}, this);

		sbuffer[sbuffer.length] = "</chart>";
		return sbuffer.join('');

	},
	toggleTabular : function() {
		this.panelTabular.toggleCollapse(true);
	},

	refreshTabularHTML : function() {

	},

	selectChart : function(itemNumber) {
		var menu = Ext.getCmp(this.menuChartTypesId);
		menu.items.get(itemNumber).setChecked(true, false);
	}
});

Ext.reg('panelgrafico', FDESK.panel.Grafico);