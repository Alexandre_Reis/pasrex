/* Resolutions */
function changeDisplay(){
var myWidth = 700, myHeight = 400;
if( typeof( window.innerWidth ) == 'number' ) {
    //Non-IE
    myWidth = window.innerWidth;
    myHeight = window.innerHeight ;
} else if( document.documentElement && ( document.documentElement.clientWidth || document.documentElement.clientHeight )) {
    //IE 6+
    myWidth = document.documentElement.clientWidth;
    myHeight = document.documentElement.clientHeight;
} else if( document.body && ( document.body.clientWidth || document.body.clientHeight ) ) {
    //compativel IE 4
    myWidth = document.body.clientWidth;
    myHeight = document.body.clientHeight;
}
var x = (myWidth - 118)+'px';
var y = (myHeight - 61)+'px';
document.getElementById('content').style.width = x;
document.getElementById('content').style.height = y;
}

/* Main search control */
function search_focus(input){
    if (input.value==document.getElementById('textTextoSearch').value){
        input.value="";
    }
}
function search_blur(input){
    if (input.value==''){
        input.value=document.getElementById('textTextoSearch').value;
    }
}
function login_edit_submit()
{    
    window.iframePrincipal.location="Login/AlterarProfile.aspx";
}

/* Gridview */
function OnRowClick(e) {
    //Clear the text selection
    _aspxClearSelection();
    if(!e.htmlEvent.ctrlKey && !e.htmlEvent.shiftKey) {
        //Unselect all rows
        gridSearch._selectAllRowsOnPage(false);
        //Select the row
        gridSearch.SelectRow(e.visibleIndex, true);
        lastSelectedIndex = e.visibleIndex;
    } else {
        if(e.htmlEvent.ctrlKey) {
            //Select/Unselect the row
            gridSearch.SelectRowCore(e.visibleIndex);
            lastSelectedIndex = e.visibleIndex;
        } else {
            var startIndex = e.visibleIndex > lastSelectedIndex ? lastSelectedIndex + 1 : e.visibleIndex;
            var endIndex = e.visibleIndex > lastSelectedIndex ? e.visibleIndex : lastSelectedIndex - 1;
            for(var i = gridSearch.visibleStartIndex; i < gridSearch.pageRowCount + gridSearch.visibleStartIndex; i ++) {
                if(i == lastSelectedIndex) continue;
                gridSearch.SelectRow(i, i >= startIndex && i <= endIndex);
            }
        }
    }
}

function onDocumentKeyDownWithCallback(e) {
    if (window.event != null)
    {
        var event = window.event;
        var iKeyCode = event.keyCode;		
    }
    else
    {
        var iKeyCode = e.keyCode;
    }
    if(iKeyCode == 45) {
        gridCadastro.AddNewRow();
        return false;
    }
    if(iKeyCode == 13 && !popup) {
        if (gridCadastro.cpNewRow == true)
        {   callbackErro.SendCallback(); 
            return false;        
        }
        else
        {
            gridCadastro.UpdateEdit();            
            return false;
        }    
    }
    if(iKeyCode == 27) { // ESC
        
        var isChrome = testCSS('WebkitTransform');
        if(isChrome) {                        
            var b = document.getElementById(e.srcElement.id); // Desfoca o campo - tratamento para google chrome
            if(b!=null) {
                b.blur();
            }
        }
                                
        gridCadastro.CancelEdit();
        return false;                        
    }
}   

function onDocumentKeyDown(e) {
    if (window.event != null)
    {
        var event = window.event;
        var iKeyCode = event.keyCode;		
    }
    else
    {
        var iKeyCode = e.keyCode;
    }	
    if(iKeyCode == 45) {
        gridCadastro.AddNewRow();
        return false;
    }
    if(iKeyCode == 13 && !popup) {
        gridCadastro.UpdateEdit();            
        return false;
    }
    if(iKeyCode == 27) { //Esc
//----------------------------------------------------------------------------------------------    
        //gridCadastro.FocusEditor(2);
        //gridCadastro.CancelEdit();
        //FocusField(textQuantidade,textQuantidade);
                
        //textQuantidade.Focus();
        //gridCadastro.CancelEdit();
        
//        for (i = 0; i < document.form1.all.length; i++) {
//            obj = document.form1.all[i];
//            alert(obj.src);            
//        }
                                            
        //var a = gridCadastro.GetColumn(5);
        //gridCadastro.FocusEditor(a);
        
        //alert(a.name);
        
        //$('btnEditCodigoCliente').blur();

//        if (document.getElementById("btnEditCodigoCliente") != null) {
//            btnEditCodigoCliente
//        }

        //var a = document.getElementById('gridCadastro_DXPEForm_ef0_btnEditCodigoCliente');
        //if(a!=null) {
        //    a.onblur();
        //}

        //var b = document.getElementById('gridCadastro_DXPEForm_ef0_textQuantidade');
        //b.onBlur();
        //b.blur();
          
//          var test = document.getElementById('gridCadastro_DXPEForm_ef0_textQuantidade');

//            test.onblur = function() {
//                alert( 'I was blurred with .blur()' );
//            };

//                // give it focus
//            test.focus();

//                // blur it
//            test.blur();
          
//document.getElementById('gridCadastro_DXPEForm_ef0_btnEditCodigoCliente').blur();
//document.getElementById('gridCadastro_DXPEForm_ef0_labelCliente').Focus();

// Funciona parcialmente
//var b = document.getElementById('gridCadastro_DXPEForm_ef0_btnEditCodigoCliente_I')
//if(b!=null) {
//    b.blur();
//}

//GetInputElement()
//----------------------------------------------------------------------------------------------
        //alert(e.srcElement.id);
        var isChrome = testCSS('WebkitTransform');
        if(isChrome) {                        
            var b = document.getElementById(e.srcElement.id); // Desfoca o campo - tratamento para google chrome
            if(b!=null) {
                b.blur();
            }
        }
        
        gridCadastro.CancelEdit();
                      
        return false;                        
    }
}   

function testCSS(prop) {
    return prop in document.documentElement.style;
}

function FocusField(campo1, campo2) 
{   
    if(typeof(campo1) != "undefined")
    {        
        if (gridCadastro.cpNewRow == true || gridCadastro.cpNewRow == undefined)
        {               
            if (document.getElementById(campo1) != null)
            {   
                if (document.getElementById(campo1).focus && document.getElementById(campo1).disabled == false)
                {
                    document.getElementById(campo1).focus();                
                }
            }
            else
            {
                campo1.Focus();
            }
        }
        else
        {
            if (document.getElementById(campo2) != null)            
            {   
                if (document.getElementById(campo2).focus && document.getElementById(campo2).disabled == false)
                {
                    document.getElementById(campo2).focus();                
                }
            }
            else
            {
                campo2.Focus();
            }
        }        
    }    
} 

/* Popup */
function OnButtonClick_FiltroDatas() {
    popupFiltro.ShowWindow(); 
    return false;        
}

function OnClearFilterClick_FiltroDatas() 
{
    textDataInicio.SetValue(null);
    textDataFim.SetValue(null);    
}    

function CalculoTriplo(textPU, textQtde, textValor, fator)
{    
    if (textPU.GetValue() == null && textQtde.GetValue() != null && textValor.GetValue() != null && fator != 0)
    {
        textPU.SetValue(textValor.GetValue() / textQtde.GetValue() * fator);
    }
    else if (textQtde.GetValue() == null && textPU.GetValue() != null && textValor.GetValue() != null && fator != 0)
    {
        textQtde.SetValue((textValor.GetValue() * fator / textPU.GetValue()));
    }
    else if (textValor.GetValue() == null && textPU.GetValue() != null && textQtde.GetValue() != null && fator != 0)
    {        
        textValor.SetValue((textQtde.GetValue() * textPU.GetValue() / fator).toFixed(2));
    }    
}

/* Dado uma Lingua de Pa�s Constroe um Formato de Data 
   Qualquer outra lingua que estiver configurado no browser diferente de en-US e pt-BR retorna vazio
   Se n�o tiver nenhuma lingua no browser � assumido o default do web-config: pt-BR
*/
function LocalizedData(stringData, culturaPais) {
    var localizedData;    
    var dataAux = stringData.split("/");
    
    if(culturaPais != null || culturaPais != '') {                        
        if(culturaPais == 'pt-BR') { // Formato de Data Portugu�s            
             // [0] = dia 
             // [1] = mes 
             // [2] = ano
            localizedData = new Date(dataAux[2], dataAux[1] - 1, dataAux[0]);
        }
        else if(culturaPais == 'en-US') { // Formato de Data Ingl�s 
            // [0] = mes 
            // [1] = dia 
            // [2] = ano
            localizedData = new Date(dataAux[2], dataAux[0] - 1, dataAux[1]);       
        }
    }
    else { // Default que est� no Web-Config - Formato Portugu�s
        localizedData = new Date(dataAux[2], dataAux[1] - 1, dataAux[0]);
    }
    
    return localizedData;    
}

function OnLostFocus(popupMensagem, callBackParam, btnEditCodigo) {

    //alert(btnEditCodigo.name);
    //var b = document.getElementById(btnEditCodigo.name)
    //if(b!=null) {
    //    b.blur();
    //}

    if(popupMensagem) {
        popupMensagem.HideWindow();
    }
        
    if (btnEditCodigo && btnEditCodigo.GetValue()) {
        callBackParam.SendCallback(btnEditCodigo.GetValue());        
    }
}

/* Callback */
function OnCallBackCompleteClienteFiltro(s, e, popupMensagem, btnEditCodigo, textNome, textData)
{
    OnCallBackCompleteCliente (s, e, popupMensagem, btnEditCodigo, textNome, textData, false)
}

function OnCallBackCompleteCliente (s, e, popupMensagem, btnEditCodigo, textNome, textData, testaFechado)
{    
    if (typeof(testaFechado) == "undefined")
    {
        testaFechado = true;
    }
    
    if (e.result.indexOf('status_closed') != -1)
    {
        var resultNome = e.result.split("|");
        if (!testaFechado)
        {        
            e.result = resultNome[0];
        }
        else
        {
            e.result = resultNome[1];
        }
    }
    
    if (typeof(textData) != "undefined" && textData != null)
    {        
        var resultAux = e.result.split("|");
        e.result = resultAux[0];
                        
        if (e.result != '' && e.result != 'no_access' && e.result != 'no_active' && (e.result != 'status_closed' && testaFechado))
        {            
            var lingua = resultAux[2];            
            var newDate = LocalizedData(resultAux[1], lingua)
                     
            textData.SetValue(newDate);            
        }        
        else
        {            
            textData.SetValue(null);
        }
        
    }
    
        
    if (btnEditCodigo && btnEditCodigo.GetValue() != null)
    {      
        if ( (e.result == '' || e.result == 'no_access' || e.result == 'no_active' || (e.result == 'status_closed' && testaFechado)) 
                    && btnEditCodigo.GetValue() != '' && btnEditCodigo.GetValue() != null)
        {   
            if (btnEditCodigo.GetValue() != '' && btnEditCodigo.GetValue() != null)
            {                
                if (e.result == '')
                {                    
                    popupMensagem.SetContentHTML(document.getElementById('textMsgNaoExiste').value);
                }
                else
                {                    
                    if (e.result == 'no_active')
                    {
                        popupMensagem.SetContentHTML(document.getElementById('textMsgInativo').value);
                    }
                    else if (e.result == 'status_closed' && testaFechado)
                    {
                        popupMensagem.SetContentHTML(document.getElementById('textMsgStatusFechado').value);
                    }
                    else if (e.result == 'no_access')
                    {                        
                        popupMensagem.SetContentHTML(document.getElementById('textMsgUsuarioSemAcesso').value);
                    }
                }                        
                popupMensagem.ShowAtElementByID(btnEditCodigo.name);
            }                
            btnEditCodigo.SetValue(''); 
            textNome.value = '';
            btnEditCodigo.Focus();                 
        }
        else
        {
            textNome.value = e.result;
        }
    }
}

var _selectNumber = 0;
var _all = false;
var _handle = true;

function OnAllCheckedChanged(s, e, grid) {

    if (s.GetChecked())
        grid.SelectRows();
    else
        grid.UnselectRows();
}

function OnGridSelectionChanged(s, e) {
    cbAll.SetChecked(s.GetSelectedRowCount() == s.cpVisibleRowCount);

    if (e.isChangedOnServer == false) {
        if (e.isAllRecordsOnPage && e.isSelected)
            _selectNumber = s.GetVisibleRowsOnPage();
        else if (e.isAllRecordsOnPage && !e.isSelected)
            _selectNumber = 0;
        else if (!e.isAllRecordsOnPage && e.isSelected)
            _selectNumber++;
        else if (!e.isAllRecordsOnPage && !e.isSelected)
            _selectNumber--;

        if (_handle) {
            cbPage.SetChecked(_selectNumber == s.GetVisibleRowsOnPage());
            _handle = false;
        }
        _handle = true;
    }
    else {
        cbPage.SetChecked(cbAll.GetChecked());
    }
}

function OnPageCheckedChanged(s, e, grid) {
    _handle = false;
    
    if (s.GetChecked())
    {
        grid.SelectAllRowsOnPage();
    }    
    else
    {
        grid.UnselectAllRowsOnPage();
    }    
}

function OnGridEndCallback(s, e) {
    _selectNumber = s.cpSelectedRowsOnPage;
}