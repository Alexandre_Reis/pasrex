/*
 * Ext JS Library 2.0.2
 * Copyright(c) 2006-2008, Ext JS, LLC.
 * licensing@extjs.com
 * 
 * http://extjs.com/license
 */

Ext.ux.Portlet = function(config) {
    
    var defConfig = {
        anchor: '100%',
        frame:true,
        collapsible:false,
        draggable:true,
        cls:'x-portlet',
        style : 'margin-bottom: 25px'
    };
    
    Ext.applyIf(config, defConfig);
    
    Ext.ux.Portlet.superclass.constructor.call(this, config);
    this.addEvents({'drop': true});
    
};

Ext.extend(Ext.ux.Portlet, Ext.Panel);

Ext.reg('portlet', Ext.ux.Portlet);