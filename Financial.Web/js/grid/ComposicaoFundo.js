Ext.namespace('FDESK.grid.ComposicaoFundo');
FDESK.grid.ComposicaoFundo = function(config) {

	var reader = new Ext.data.JsonReader({
				idProperty : 'IdAtivo',
				fields : [{
							name : 'IdAtivo',
							type : 'string'
						}, {
							name : 'IdTipo',
							type : 'int'
						}, {
							name : 'TipoPlural',
							type : 'string'
						}, {
							name : 'Nome',
							type : 'string'
						}, {
							name : 'SaldoBruto',
							type : 'float'
						}, {
							name : 'PercentualAtivos',
							type : 'int'
						}]

			});

	config = config || {};

	var summary = new Ext.ux.grid.GroupSummary();

	var store = Ext.extend(Ext.data.GroupingStore, {
				loadData : function(o, append) {
					this.fireEvent('beforeload', this);
					var r = this.reader.readRecords(o);
					this.loadRecords(r, {
								add : append
							}, true);
				}

			});

	Ext.apply(config, {
		ds : new store({
					reader : reader,
					data : config.data,
					groupField : 'TipoPlural',
					remoteSort : false,
					remoteGroup : false,
					sortInfo : {
						field : 'Nome',
						direction : 'ASC'
					}
				}),
		columns : [{
					header : 'Ativos',
					width : 300,
					sortable : false,
					dataIndex : 'Nome',
					hideable : false,
					summaryRenderer : function(v, params, data) {
						return 'Sub-total ';
					}
				}, {
					header : 'Tipo',
					width : 20,
					sortable : true,
					dataIndex : 'TipoPlural'
				}, {
					header : 'Saldo Bruto',
					width : 50,
					sortable : true,
					dataIndex : 'SaldoBruto',
					summaryType : 'sum',
					renderer : FDESK.util.Util.formatBRMoneySinal
				}, {
					header : '%PL',
					width : 50,
					sortable : true,
					dataIndex : 'PercentualAtivos',
					summaryType : 'sum',
					renderer : function(v, params, data) {
						return v + "%";
					}
				}, {
				    header: '',
				    width: 10,
				    dataIndex: 'Null'
				}],

		view : new FDESK.grid.GroupingView({
					forceFit : true,
					showGroupName : false,
					enableNoGroups : false,

					enableGroupingMenu : false,
					hideGroupedColumn : true,
					headersDisabled : true,
					disabledGroups : ['Caixa C/C', 'Total Geral']
				}),

		plugins : summary
		})
		
	FDESK.grid.ComposicaoFundo.superclass.constructor.call(this, config);

	this.on('rowclick', this.onRowClick, this);
    this.on('afterrender', this.addColumnHover, this);
};

Ext.extend(FDESK.grid.ComposicaoFundo, Ext.grid.GridPanel, {
			frame : true,
			header: false,
			clicksToEdit : 1,
			collapsible : true,
			animCollapse : false,
			trackMouseOver : false,
			columnLines : true,
			bodyBorder : false,
			border : false,
			cls : 'grid-financial grid-carteira-sintetica',
			onRowClick : function(grid, rowIndex) {

				var row = grid.selModel.getSelected();
				var rec = FDESK.AS.stores.tipoAtivo.getAt(FDESK.AS.stores.tipoAtivo.find(
						'IdTipo', row.data.IdTipo));
				
				if(!FDESK.win[rec.data.Tipo]){
				    Ext.Msg.alert('Portal do Cliente', 'N�o existe detalhamento para este ativo');
				    return;
				}
				
                this.showWindow({
							tipoRegistro : rec.data.Tipo,
							pkRegistro : row.data.IdAtivo
						});
			},

            addColumnHover : function(){
                var el = this.getEl();
                var colEls = el.query('.x-grid3-hd');
                var messages = ['Valores estimados, sujeitos a altera��es', 'Valores estimados, sujeitos a altera��es'];
                for(var i = 0; i < colEls.length; i++){
                    var colEl = colEls[i];
                    if(colEl.innerHTML.indexOf('*') !== -1){
                        var q = new Ext.ToolTip({
	                        target: colEl,
        	                html: messages[0],
        	                showDelay : 0,
        	                trackMouse : true
	                    });
	                    messages[0] = messages[1];
                    }
                }
            },

			showWindow : function(formParams) {
				FDESK.LE.openDocInWin(formParams, true);
			},

			loadData : function(data) {
				this.store.loadData(data);
			}
		});
Ext.reg('composicaofundo', FDESK.grid.ComposicaoFundo);
