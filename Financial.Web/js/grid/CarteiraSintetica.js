Ext.namespace('FDESK.grid.CarteiraSintetica');
FDESK.grid.CarteiraSintetica = function(config) {

	var reader = new Ext.data.JsonReader({
				idProperty : 'IdAtivo',
				fields : [{
							name : 'IdAtivo',
							type : 'string'
						}, {
							name : 'IdTipo',
							type : 'int'
						}, {
							name : 'TipoPlural',
							type : 'string',
							sortType: function(value)
                            {
                               var lowValue = value.toLowerCase();
                               switch (lowValue)
                               {
                                  case 'total geral': return 9999;
                                  default: 1;
                               }
                            }	
							/*,
							sortType: function(value)
                            {
                               var lowValue = value.toLowerCase();
                               switch (lowValue)
                               {
                                  case 'caixa': return 1;
                                  case 'total geral': return 9999;
                                  default: return lowValue.charCodeAt(0);
                               }
                            }*/
						}, {
							name : 'Nome',
							type : 'string'
						}, {
							name : 'SaldoAnterior',
							type : 'float'
						}, {
							name : 'Aplicacoes',
							type : 'float'
						}, {
							name : 'Resgates',
							type : 'float'
						}, {
							name : 'ImpostoPago',
							type : 'float'
						}, {
							name : 'SaldoBruto',
							type : 'float'
						}, {
							name : 'ProvisaoTributo',
							type : 'float'
						}, {
							name : 'SaldoLiquido',
							type : 'float'
						}]

			});

	config = config || {};

	var summary = new Ext.ux.grid.GroupSummary();

	var store = Ext.extend(Ext.data.GroupingStore, {
				loadData : function(o, append) {
					this.fireEvent('beforeload', this);
					var r = this.reader.readRecords(o);
					this.loadRecords(r, {
								add : append
							}, true);
				}

			});

	Ext.apply(config, {
		ds : new store({
					reader : reader,
					data : config.data,
					groupField : 'TipoPlural',
					remoteSort : true,
					remoteGroup : false,
					sortInfo : {
						field : 'Nome',
						direction : 'ASC'
					}
				}),
		columns : [{
					header : 'Ativos',
					width : 250,
					sortable : false,
					dataIndex : 'Nome',
					hideable : false,
					summaryRenderer : function(v, params, data) {
						return 'Sub-total ';
					}
				}, {
					header : 'Tipo',
					width : 20,
					sortable : true,
					dataIndex : 'TipoPlural'
				}, {
					header : 'Saldo<br />Anterior',
					width : 100,
					sortable : true,
					dataIndex : 'SaldoAnterior',
					summaryType : 'sum',
					renderer : FDESK.util.Util.formatBRMoneySinal
					//FDESK.util.Util.formatBRMoneySinal(v, true);
				}, {
					header : 'Entradas',
					width : 100,
					sortable : true,
					dataIndex : 'Aplicacoes',
					summaryType : 'sum',
					renderer : FDESK.util.Util.formatBRMoneySinal
				}, {
					header : 'Sa�das',
					width : 100,
					sortable : true,
					dataIndex : 'Resgates',
					summaryType : 'sum',
					renderer : FDESK.util.Util.formatBRMoneySinal
				}, {
					header : 'Imposto<br />Pago *',
					width : 100,
					sortable : true,
					dataIndex : 'ImpostoPago',
					summaryType : 'sum',
					renderer : FDESK.util.Util.formatBRMoneySinal
				}, {
					header : 'Saldo<br />Bruto',
					width : 100,
					sortable : true,
					dataIndex : 'SaldoBruto',
					summaryType : 'sum',
					renderer : FDESK.util.Util.formatBRMoneySinal
				}, {
					header : 'Provis�o de<br/>IR+IOF *',
					width : 100,
					sortable : true,
					dataIndex : 'ProvisaoTributo',
					summaryType : 'sum',
					renderer : FDESK.util.Util.formatBRMoneySinal
				}, {
					header : 'Saldo L�quido',
					width : 100,
					sortable : true,
					dataIndex : 'SaldoLiquido',
					summaryType : 'sum',
					renderer : FDESK.util.Util.formatBRMoneySinal
				}],

		view : new FDESK.grid.GroupingView({
					forceFit : true,
					showGroupName : false,
					enableNoGroups : false,

					enableGroupingMenu : false,
					hideGroupedColumn : true,
					headersDisabled : true,
					disabledGroups : ['Caixa C/C', 'Total Geral']
				}),

		plugins : summary
/*
 * ,
 * 
 * tbar : [{ text : 'Reload novos valores', tooltip : 'Rload novos valores',
 * scope : this, handler : function() { this.store.loadData([{ IdAtivo : 3,
 * TipoPlural : 'Renda Fixa', IdTipo : 1, Nome : 'LFT - 15/11/2014',
 * SaldoAnterior : 136.44 }, { IdAtivo : 1, TipoPlural : 'A��es', IdTipo : 2,
 * Nome : 'BBDC4', SaldoAnterior : 126 }, { IdAtivo : 2, TipoPlural : 'A��es',
 * IdTipo : 2, Nome : 'CGAS5', SaldoAnterior : 145 }, { IdAtivo : "", IdTipo :
 * 0, TipoPlural : 'Total Geral', Nome : "", SaldoAnterior : 12781850.00 }]);
 *  } }]
 */
		})
		
	FDESK.grid.CarteiraSintetica.superclass.constructor.call(this, config);

	this.on('rowclick', this.onRowClick, this);
    this.on('afterrender', this.addColumnHover, this);
};

Ext.extend(FDESK.grid.CarteiraSintetica, Ext.grid.GridPanel, {
			title : 'Composi��o da Carteira',
			frame : true,
			//autoHeight : true,
			clicksToEdit : 1,
			collapsible : true,
			animCollapse : false,
			trackMouseOver : false,
			columnLines : true,
			bodyBorder : false,
			border : false,
			cls : 'grid-financial grid-carteira-sintetica',
			onRowClick : function(grid, rowIndex) {

				var row = grid.selModel.getSelected();
				var rec = FDESK.AS.stores.tipoAtivo.getAt(FDESK.AS.stores.tipoAtivo.find(
						'IdTipo', row.data.IdTipo));
				
				if(!FDESK.win[rec.data.Tipo]){
				    Ext.Msg.alert('Portal do Cliente', 'N�o existe detalhamento para este ativo');
				    return;
				}
				
                this.showWindow({
							tipoRegistro : rec.data.Tipo,
							pkRegistro : row.data.IdAtivo
						});
			},

            addColumnHover : function(){
                var el = this.getEl();
                var colEls = el.query('.x-grid3-hd');
                var messages = ['Valores estimados, sujeitos a altera��es', 'Valores estimados, sujeitos a altera��es'];
                for(var i = 0; i < colEls.length; i++){
                    var colEl = colEls[i];
                    if(colEl.innerHTML.indexOf('*') !== -1){
                        var q = new Ext.ToolTip({
	                        target: colEl,
        	                html: messages[0],
        	                showDelay : 0,
        	                trackMouse : true
	                    });
	                    messages[0] = messages[1];
                    }
                }
            },

			showWindow : function(formParams) {
				FDESK.LE.openDocInWin(formParams, true);
			},

			loadData : function(data) {
				this.store.loadData(data);
			}
		});
Ext.reg('carteirasintetica', FDESK.grid.CarteiraSintetica);
