﻿using System;
using System.Data;
using System.Text;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Financial.WebConfigConfiguration;
using System.IO;
using System.Collections.Specialized;
using Financial.Security;
using Financial.Security.Enums;
using Financial.Util;
using EntitySpaces.Core;
using EntitySpaces.Interfaces;
using System.Data.SqlClient;

public partial class Default : Financial.Web.Common.BasePage {
    private Usuario usuario = new Usuario();

    private SqlConnectionStringBuilder connectionBuilder = new SqlConnectionStringBuilder(esConfigSettings.DefaultConnection.ConnectionString);

    new protected void Page_Load(object sender, EventArgs e) {
        this.Title = String.IsNullOrEmpty(ConfigurationManager.AppSettings["TituloPagina"]) ? "Financial" : ConfigurationManager.AppSettings["TituloPagina"];

        string userAgent = Request.ServerVariables["HTTP_USER_AGENT"].ToUpper();
        string httpAccept = Request.ServerVariables["HTTP_ACCEPT"].ToUpper();
        MobileDetect mobileDetect = new MobileDetect(userAgent, httpAccept);

        bool ignoraMobile = Request["mobile"] == "false";

        if (!ignoraMobile && (mobileDetect.DetectTierTablet() || mobileDetect.DetectSmartphone())) {
            //Pular para mobile
            const string URL_MOBILE = "~/mobile/index.html";
            Response.Redirect(URL_MOBILE);
            return;
        }

        string queryString = Request.QueryString.ToString();
        if (!String.IsNullOrEmpty(queryString)) {
            queryString = "?" + queryString;
        }

        string URL_FDESK = "~/DefaultFDesk.aspx" + queryString;

        usuario.BuscaUsuario(HttpContext.Current.User.Identity.Name);

        //Examinar se devemos forcar usuario para FinancialDesk
        if (WebConfig.AppSettings.FinancialDesk) {
            if (usuario.IdGrupo.HasValue) {
                GrupoUsuario grupoUsuario = new GrupoUsuario();
                grupoUsuario.LoadByPrimaryKey(usuario.IdGrupo.Value);

                if (grupoUsuario.TipoPerfil.Value == (byte)TipoPerfilGrupo.Externo) {
                    Response.Redirect(URL_FDESK);
                    return;
                }
            }
        }


        Response.Cache.SetCacheability(HttpCacheability.ServerAndNoCache);

        textSearch.Text = textTextoSearch.Text;

        //Carrega página default
        string paginaDefault = RetornaPaginaDefault();
        homePage.Attributes["href"] = paginaDefault;

        hiddenPage.Value = paginaDefault;

        chkJanela.Attributes.Add("onclick", "iterateThroughMenu();");

        this.updatePopReleaseNotes();
    }

    protected void updatePopReleaseNotes()
    {
        string path = DiretorioAplicacao.DiretorioBaseAplicacao;
        string releaseNotesText = "";
        string linha = "";
        int nroLinha = 0;

        using (TextReader sr = new StreamReader(path + "/data/ReleaseNotes/releaseNotes.txt", Encoding.UTF8))
        {
            while ((linha = sr.ReadLine()) != null)
            {
                if (nroLinha == 0) Literal1.Text = linha.Substring(8);
                nroLinha++;

                releaseNotesText += linha + "\n";

            }
        }

        popupReleaseNotes.Text = releaseNotesText;
        
    }


    /// <summary>
    /// Carregamento da imagem de Logotipo
    /// Se estiver Definido na tabela Cliente carrega a imagem do Cliente
    /// Se estiver Definido na tabela Usuário carrega a imagem do Usuário
    /// Se não carrega a imagem logo_cliente.png
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void divLogotipoOnPreRender(object sender, EventArgs e) {

        string img = "imagensPersonalizadas/logo_cliente.png"; // Default
        //string img = "imagensPersonalizadas/logo_clienteSolidus.png";

        Usuario u = new Usuario();
        u.Query.Select(u.Query.IdUsuario, u.Query.Logotipo)
               .Where(u.Query.Login == HttpContext.Current.User.Identity.Name);

        if (u.Query.Load()) {
            if (!String.IsNullOrEmpty(u.Logotipo)) { 
                
                // Usuário tem Logotipo Definido - Carrega o Logotipo do Usuário                                
                string path = AppDomain.CurrentDomain.BaseDirectory + "\\imagensPersonalizadas\\"+ u.Logotipo.Trim();

                if (File.Exists(path)) {
                    img = "imagensPersonalizadas/" + u.Logotipo.Trim();
                }
            }
        }
                       
        string image = String.Format("transparent url({0}) no-repeat", img);

        this.divLogotipo.Style.Add("background", image);
    }

    private string RetornaPaginaDefault() {
        //Busca página default associada ao usuário, se não tiver, pega a página default do web.config
        string pagina = "";
        if (!String.IsNullOrEmpty(usuario.PaginaInicial)) {
            pagina = usuario.PaginaInicial;
        }

        if (pagina == "") {
            pagina = "Home.aspx";
            //pagina = WebConfig.AppSettings.PaginaInicial;
        }

        return pagina;
    }

    protected void NavigationMenu_DataBound(object sender, EventArgs e) {
        DevExpress.Web.ASPxMenu menu = (DevExpress.Web.ASPxMenu)sender;
        if (menu.Items.Count == 1) {
            AjustaMenu(menu);
        }
    }

    private void AjustaMenu(DevExpress.Web.ASPxMenu menu) {
        if (menu.Items[0].Items.Count > 0) {
            DevExpress.Web.MenuItemCollection directChildren = menu.Items[0].Items;

            int directChildrenCount = directChildren.Count;
            for (int childIndex = 0; childIndex < directChildren.Count; ) {
                menu.Items.Add(directChildren[childIndex]);
            }

            directChildren = menu.Items[0].Items;

            menu.Items.RemoveAt(0);

            AjustaMenu(menu);
        }
    }

    protected void callback1_Callback(object source, DevExpress.Web.CallbackEventArgs e) {
        if (HttpContext.Current.User.Identity.IsAuthenticated) {
            string pagina = Convert.ToString(Session["CurrentPage"]);
            Usuario usuario = new Usuario();
            if (usuario.BuscaUsuario(HttpContext.Current.User.Identity.Name)) {
                usuario.PaginaInicial = pagina;
                usuario.Save();

                e.Result = "Página Inicial alterada com sucesso!" + "|" + pagina;
            }
            else {
                e.Result = "Ocorreu algum problema, atualização cancelada.";
            }
        }
    }

    /// <summary>
    /// Coloca Informação sobre o banco sendo executado
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void lblVersao_OnPreRender(object sender, EventArgs e) {
        //
        string sql1 = " SELECT top(1) * FROM VersaoSchema ORDER by 1 DESC ";
        DateTime dataUltimaAtualizacao = Convert.ToDateTime(new esUtility().ExecuteScalar(esQueryType.Text, sql1, ""));
        //        
        this.lblVersao.Text = dataUltimaAtualizacao.ToString("d");
    }

    /// <summary>
    /// Coloca a data da última dll web - Financial.Web_Deploy.dll
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void lblVersaoWeb_OnPreRender(object sender, EventArgs e) {
        //
        string filename = DiretorioAplicacao.DiretorioBaseAplicacao + "Bin/Financial.Web_Deploy.dll";

        DateTime? data = null;

        if (File.Exists(filename)) {
            data = File.GetLastWriteTime(filename);
        }

        this.lblVersaoWeb.Text = data.HasValue ? data.Value.ToString("dd/MM/yyyy HH:mm:ss") : " - ";
    }

    protected void lblServidorNome_Load(object sender, EventArgs e)
    {
        lblServidorNome.Text = connectionBuilder.DataSource;
    }

    protected void lblBancoNome_Load(object sender, EventArgs e)
    {
        lblBancoNome.Text = connectionBuilder.InitialCatalog;       
    }
}