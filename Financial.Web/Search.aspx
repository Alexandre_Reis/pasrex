﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Search.aspx.cs" Inherits="Search" %>

<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxe" %>

<%@ Register Assembly="EntitySpaces.Web" Namespace="EntitySpaces.Web" TagPrefix="cc1" %>

<%@ Register Assembly="System.Web, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"
    Namespace="System.Web.UI.HtmlControls" TagPrefix="cc4" %>
<%@ Register Assembly="System.Web, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"
    Namespace="System.Web.UI" TagPrefix="cc3" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxwgv" %>
    

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Untitled Page</title>
</head>
<body>
    <form id="form1" runat="server">
    
    <div>   
        <dxwgv:ASPxGridView  Width="97%" ID="gridSearch" 
                        ClientInstanceName="gridSearch" runat="server" AutoGenerateColumns="False" 
                        KeyFieldName="IdCliente">                    
                        
            <Settings ShowFilterRow="True" ShowTitlePanel="True" ShowStatusBar="Hidden" />            
            <SettingsBehavior AllowFocusedRow="True" ColumnResizeMode="NextColumn" />              
            <SettingsText EmptyDataRow="0 Registros" Title="Pesquisa Geral" />
            <SettingsDetail ShowDetailButtons="False" />
            
            <SettingsPager PageSize="12">
                <AllButton Text="All">
                </AllButton>
                <NextPageButton Text="Next &gt;">
                </NextPageButton>
                <PrevPageButton Text="&lt; Prev">
                </PrevPageButton>
            </SettingsPager>
            
            <Styles>
                <Header ImageSpacing="5px" SortingImageSpacing="5px">
                </Header>
            </Styles>
            <Images></Images>
            
            <Columns>        
                <dxwgv:GridViewDataTextColumn FieldName="IdCliente" ReadOnly="True" VisibleIndex="0" Width="20%" >
                </dxwgv:GridViewDataTextColumn>
                <dxwgv:GridViewDataTextColumn FieldName="Nome" VisibleIndex="1" Width="80%">                                    
                </dxwgv:GridViewDataTextColumn>                                
            </Columns>       
                
        </dxwgv:ASPxGridView>  
                  
    </div>
        
    <cc1:esDataSource ID="esDSSearch" runat="server" OnesSelect="esDSSearch_esSelect" />
    
    </form>
</body>
</html>
