<%@ Application Language="C#" %>

<script RunAt="server">

    private string DecryptConnectionString(string encryptedConnectionString)
    {
        const string SHARED_SECRET = "financial@$";
        string[] connectionStringTokens = encryptedConnectionString.Split(';');
        System.Collections.Generic.List<string> decryptedConnectionStringList = new System.Collections.Generic.List<string>();

        foreach (string connectionStringToken in connectionStringTokens)
        {
            string token = connectionStringToken.Trim();
            if (token.ToLower().StartsWith("password"))
            {
                int passwordStart = token.IndexOf("=") + 1;
                string password = token.Substring(passwordStart);
                string decryptedPassword = Financial.Util.Crypto.DecryptStringAES(password, SHARED_SECRET);
                token = "Password=" + decryptedPassword;
            }

            decryptedConnectionStringList.Add(token);
        }

        string decryptedConnectionString = String.Join(";", decryptedConnectionStringList.ToArray());
        return decryptedConnectionString;
    }

    private void AddEntitySpacesSqlConnection(string connectionName, string encryptedConnectionString)
    {
        EntitySpaces.Interfaces.esConnectionElement conn = new EntitySpaces.Interfaces.esConnectionElement();

        //Decrypt connection string
        conn.ConnectionString = DecryptConnectionString(encryptedConnectionString);

        conn.Name = connectionName;
        conn.Provider = "EntitySpaces.SqlClientProvider";
        conn.ProviderClass = "DataProvider";
        conn.SqlAccessType = EntitySpaces.Interfaces.esSqlAccessType.DynamicSQL;
        conn.ProviderMetadataKey = "esDefault";
        conn.DatabaseVersion = "2005";   
        conn.CommandTimeout =int.MaxValue;     
        EntitySpaces.Interfaces.esConfigSettings.ConnectionInfo.Connections.Add(conn);
    }

    private void AddEntitySpacesOracleConnection(string connectionName, string encryptedConnectionString)
    {
        EntitySpaces.Interfaces.esConnectionElement conn = new EntitySpaces.Interfaces.esConnectionElement();

        //Decrypt connection string
        conn.ConnectionString = DecryptConnectionString(encryptedConnectionString);

        conn.Name = connectionName;
        conn.Provider = "EntitySpaces.OracleClientProvider";
        conn.ProviderClass = "DataProvider";
        conn.SqlAccessType = EntitySpaces.Interfaces.esSqlAccessType.DynamicSQL;
        conn.ProviderMetadataKey = "esDefault";
        EntitySpaces.Interfaces.esConfigSettings.ConnectionInfo.Connections.Add(conn);
    }
    
    void Application_Start(object sender, EventArgs e)
    {
        bool webconfigEncrypted = ConfigurationManager.AppSettings["CriptografaSenhaConexao"] == "true";
        if (webconfigEncrypted)
        {
            const string FINANCIAL_CONNECTION_NAME = "Financial";
            const string SINACOR_CONNECTION_NAME = "Sinacor";
            const string BSMAPPING_CONNECTION_NAME = "BsMapping";
            const string CONSULTA_CONNECTION_NAME = "Financial_Consulta";

            #region financial
            ConnectionStringSettings connectionSettings = ConfigurationManager.ConnectionStrings[FINANCIAL_CONNECTION_NAME];
            if (connectionSettings == null)
            {
                throw new Exception("N�o foi poss�vel encontrar a conex�o Financial no Web.Config");
            }            
                        
            string encryptedConnectionString = connectionSettings.ConnectionString;
            
            //Hoje consideramos que se estamos encriptando estamos usando o Financial no SQL Server. 
            //Se mudar, precisamos rever esse IF e passar pela connection string o provider
            AddEntitySpacesSqlConnection(FINANCIAL_CONNECTION_NAME, encryptedConnectionString);
            EntitySpaces.Interfaces.esConfigSettings.ConnectionInfo.Default = FINANCIAL_CONNECTION_NAME;
            #endregion
            
            #region sinacor
            connectionSettings = ConfigurationManager.ConnectionStrings[SINACOR_CONNECTION_NAME];
            if (connectionSettings != null)
            {
                encryptedConnectionString = connectionSettings.ConnectionString;
                if (!string.IsNullOrEmpty(encryptedConnectionString))
                {
                    AddEntitySpacesOracleConnection(SINACOR_CONNECTION_NAME, encryptedConnectionString);
                }
            }
            #endregion

            #region bsmapping
            connectionSettings = ConfigurationManager.ConnectionStrings[BSMAPPING_CONNECTION_NAME];
            if (connectionSettings != null)
            {
                encryptedConnectionString = connectionSettings.ConnectionString;
                if (!string.IsNullOrEmpty(encryptedConnectionString))
                {
                    AddEntitySpacesSqlConnection(BSMAPPING_CONNECTION_NAME, encryptedConnectionString);                    
                }
            }
            #endregion

            #region Financial_Consulta
            connectionSettings = ConfigurationManager.ConnectionStrings[CONSULTA_CONNECTION_NAME];
            if (connectionSettings != null)
            {
                encryptedConnectionString = connectionSettings.ConnectionString;
                if (!string.IsNullOrEmpty(encryptedConnectionString))
                {
                    AddEntitySpacesSqlConnection(CONSULTA_CONNECTION_NAME, encryptedConnectionString);
                }
            }
            #endregion
            
        }

        EntitySpaces.Interfaces.esProviderFactory.Factory = new EntitySpaces.LoaderMT.esDataProviderFactory();
        string arquivo = Financial.WebConfigConfiguration.DiretorioAplicacao.DiretorioBaseAplicacao + "web.Config";
        log4net.Config.XmlConfigurator.Configure(new System.IO.FileInfo(arquivo));

        if (EntitySpaces.Interfaces.esConfigSettings.DefaultConnection.Provider == "EntitySpaces.SqlClientProvider")
        {
            EntitySpaces.Core.esUtility u = new EntitySpaces.Core.esUtility();
            string sql = " set language english ";
            u.ExecuteNonQuery(EntitySpaces.Interfaces.esQueryType.Text, sql, "");
        }
    }

    void Application_End(object sender, EventArgs e)
    {
        //  Code that runs on application shutdown

    }

    void Application_Error(object sender, EventArgs e)
    {
        // Code that runs when an unhandled error occurs
        //Response.Redirect("~/Erro/PrincipalErro.aspx");
    }

    void Session_Start(object sender, EventArgs e)
    {

        string path = AppDomain.CurrentDomain.BaseDirectory;
        path += @"Data\financial.xml";

        Korzh.EasyQuery.DataModel model = new Korzh.EasyQuery.DataModel();
        model.UseResourcesForOperators = true;

        model.LoadFromFile(path);

        Korzh.EasyQuery.Query query = new Korzh.EasyQuery.Query();
        //query.Formats.EOL = Korzh.EasyQuery.EOLSymbol.None;
        query.Model = model;
        //query.Formats.AlphaAlias = true;

        query.Formats.DateFormat = "yyyy-MM-dd";
        query.Formats.DateTimeFormat = "yyyy-MM-dd";
        query.Formats.QuoteBool = false;
        query.Formats.QuoteTime = true;
        query.Formats.UseSchema = false;
        query.Formats.SqlQuote1 = '[';
        query.Formats.SqlQuote2 = ']';
        query.Formats.SqlSyntax = Korzh.EasyQuery.SqlSyntax.SQL2;
        query.Formats.TimeFormat = "";
        //
        Session["Query"] = query;
    }

    void Session_End(object sender, EventArgs e)
    {
        // Code that runs when a session ends. 
        // Note: The Session_End event is raised only when the sessionstate mode
        // is set to InProc in the Web.config file. If session mode is set to StateServer 
        // or SQLServer, the event is not raised.

        //Session["Query"] = null;  
    }

    protected void Application_PreSendRequestContent(object sender, EventArgs e)
    {

    }
</script>

