﻿IF NOT EXISTS(SELECT 1 FROM SYSCOLUMNS WHERE ID= OBJECT_ID('Trader') AND NAME = 'IdCarteira')
BEGIN
	ALTER TABLE Trader add IdCarteira int null ;
	alter table Trader add constraint Trader_Carteira_FK FOREIGN KEY ( IdCarteira ) references Carteira(IdCarteira)
END
GO

IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'PortfolioPadrao') = 0
begin
	CREATE TABLE dbo.PortfolioPadrao
	(
		IdPortfolioPadrao  			INT PRIMARY KEY NOT NULL IDENTITY,
		DataVigencia				Datetime	   NOT NULL,
		IdCarteiraTaxaGestao		INT 		   NOT NULL,
		IdCarteiraTaxaPerformance   INT 		   NOT NULL,
		IdCarteiraDespesasReceitas  INT 		   NOT NULL
	)
	ALTER TABLE [dbo].[PortfolioPadrao] ADD CONSTRAINT FK_CarteiraTaxaGestao_Carteira FOREIGN KEY(IdCarteiraTaxaGestao) REFERENCES Carteira(IdCarteira);
	ALTER TABLE [dbo].[PortfolioPadrao] ADD CONSTRAINT FK_CarteiraTaxaPerformance_Carteira FOREIGN KEY(IdCarteiraTaxaPerformance) REFERENCES Carteira(IdCarteira);
	ALTER TABLE [dbo].[PortfolioPadrao] ADD CONSTRAINT FK_CarteiraDespesasReceitas_Carteira FOREIGN KEY(IdCarteiraDespesasReceitas) REFERENCES Carteira(IdCarteira);
END	
GO

if not exists(select 1 from syscolumns where id = object_id('OrdemRendaFixa') and name = 'IdTrader')
BEGIN
	ALTER TABLE OrdemRendaFixa add IdTrader int NULL;	
	alter table OrdemRendaFixa add constraint OrdemRendaFixa_Trader_FK FOREIGN KEY ( IdTrader ) references Trader(IdTrader)
END
GO	

if not exists(select 1 from syscolumns where id = object_id('OrdemFundo') and name = 'IdTrader')
BEGIN
	ALTER TABLE OrdemFundo add IdTrader int NULL;	
	alter table OrdemFundo add constraint OrdemFundo_Trader_FK FOREIGN KEY ( IdTrader ) references Trader(IdTrader)
END
GO	

if not exists(select 1 from syscolumns where id = object_id('OperacaoRendaFixa') and name = 'IdTrader')
BEGIN
	ALTER TABLE OperacaoRendaFixa add IdTrader int NULL;	
	alter table OperacaoRendaFixa add constraint OpRendaFixa_Trader_FK FOREIGN KEY ( IdTrader ) references Trader(IdTrader)
END
GO	

if not exists(select 1 from syscolumns where id = object_id('OperacaoFundo') and name = 'IdTrader')
BEGIN
	ALTER TABLE OperacaoFundo add IdTrader int NULL;	
	alter table OperacaoFundo add constraint OpFundo_Trader_FK FOREIGN KEY ( IdTrader ) references Trader(IdTrader)
END
GO	

if not exists(select 1 from syscolumns where id = object_id('OperacaoFundoAux') and name = 'IdTrader')
BEGIN
	ALTER TABLE OperacaoFundoAux add IdTrader int NULL;	
	alter table OperacaoFundoAux add constraint OpFundoAux_Trader_FK FOREIGN KEY ( IdTrader ) references Trader(IdTrader)
END
GO	

IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'CalculoResultado') = 0
begin
	CREATE TABLE dbo.CalculoResultado
	(
		IdCliente	 	INT				NOT NULL,
		DataReferencia	Datetime		NOT NULL,
		PL				decimal(20,2)	NOT NULL default('0'),
		PnL_Ano			decimal(20,2)	NOT NULL default('0'),
		Custo_Ano		decimal(20,2)	NOT NULL default('0'),
		Resultado_Ano	decimal(20,2)	NOT NULL default('0'),
		PnL_Mes			decimal(20,2)	NOT NULL default('0'),
		Custo_Mes		decimal(20,2)	NOT NULL default('0'),
		Resultado_Mes	decimal(20,2)	NOT NULL default('0'),
		PnL_Dia			decimal(20,2)	NOT NULL default('0'),
		Custo_Dia		decimal(20,2)	NOT NULL default('0'),
		Resultado_Dia	decimal(20,2)	NOT NULL default('0')
		CONSTRAINT PK_CalculoResultado 		primary key(IdCliente, DataReferencia)
	)	
	ALTER TABLE [dbo].[CalculoResultado] ADD CONSTRAINT FK_CalculoResultado_Cliente FOREIGN KEY(IdCliente) REFERENCES Cliente(IdCliente);
END	
GO	

if not exists(select 1 from syscolumns where id = object_id('OrdemCotista') and name = 'IdTrader')
BEGIN
	ALTER TABLE OrdemCotista add IdTrader int NULL;	
	alter table OrdemCotista add constraint OrdemCotista_Trader_FK FOREIGN KEY ( IdTrader ) references Trader(IdTrader)
END
GO	

if not exists(select 1 from syscolumns where id = object_id('OperacaoCotista') and name = 'IdTrader')
BEGIN
	ALTER TABLE OperacaoCotista add IdTrader int NULL;	
	alter table OperacaoCotista add constraint OpCotista_Trader_FK FOREIGN KEY ( IdTrader ) references Trader(IdTrader)
END
GO	

if not exists(select 1 from syscolumns where id = object_id('OperacaoCotistaAux') and name = 'IdTrader')
BEGIN
	ALTER TABLE OperacaoCotistaAux add IdTrader int NULL;	
	alter table OperacaoCotistaAux add constraint OpCotistaAux_Trader_FK FOREIGN KEY ( IdTrader ) references Trader(IdTrader)
END
GO	

if not exists (select 1 from syscolumns where id = object_id('OperacaoBMF') and name = 'TaxaClearingVlFixo')
begin
	alter table OperacaoBMF add TaxaClearingVlFixo [decimal](16, 2) NOT NULL default 0
end 
GO	

if not exists (select 1 from syscolumns where id = object_id('OrdemBMF') and name = 'TaxaClearingVlFixo')
begin
	alter table OrdemBMF add TaxaClearingVlFixo [decimal](16, 2) NOT NULL default 0
end 
GO	

if exists (select 1 from syscolumns where id = object_id('TabelaTaxaClearingBMF') and name = 'serie')
begin
	IF EXISTS (select 1 from sysconstraints where object_name(id) = 'TabelaTaxaClearingBMF' and object_name(constid) = 'TabelaTaxaClearingBMF_FK2')
	begin
		alter table TabelaTaxaClearingBMF drop constraint TabelaTaxaClearingBMF_FK2
	end 
	
	alter table TabelaTaxaClearingBMF drop column serie;
end 
GO		

if exists (select 1 from syscolumns where id = object_id('OperacaoRendaFixa') and name = 'IdBoletaGPS')
begin
	alter table OperacaoRendaFixa drop column IdBoletaGPS;
end 
GO	

if exists (select 1 from syscolumns where id = object_id('TabelaTaxaClearingBMF') and name = 'TipoMercado')
begin
	alter table TabelaTaxaClearingBMF alter column TipoMercado tinyint null;
end 
GO		

--Merge 1.0 -> 1.1 - 2016/01/12

IF NOT EXISTS (SELECT 1 FROM sysindexes WHERE name = 'IDX_PosicaoHistoricoCotista_IdCarteira_IdCotista')
BEGIN
CREATE NONCLUSTERED INDEX
[IDX_PosicaoHistoricoCotista_IdCarteira_IdCotista] ON
[dbo].[PosicaoCotistaHistorico]
(
[IdCotista] ASC,
[IdCarteira] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB
= OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF,
ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]	
END
GO	

IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'Lamina') = 0
begin
	CREATE TABLE [dbo].[Lamina](
		[IdCarteira] [int] NOT NULL,
		[InicioVigencia] [datetime] NOT NULL,
		[EnderecoEletronico] [varchar](200) NULL,
		[DescricaoPublicoAlvo] [varchar](500) NULL,
		[ObjetivoFundo] [varchar](500) NULL,
		[DescricaoPoliticaInvestimento] [varchar](500) NULL,
		[LimiteAplicacaoExterior] [decimal](16, 2) NULL,
		[LimiteCreditoPrivado] [decimal](16, 2) NULL,
		[DerivativosProtecaoCarteira] [char](1) NULL,
		[LimiteAlavancagem] [decimal](16, 2) NULL,
		[PrazoCarencia] [varchar](500) NULL,
		[TaxaEntrada] [varchar](500) NULL,
		[TaxaSaida] [varchar](500) NULL,
		[Risco] [int] NULL,
		[FundoEstruturado] [char](1) NULL,
		[CenariosApuracaoRentabilidade] [varchar](500) NULL,
		[DesempenhoFundo] [varchar](500) NULL,
		[PoliticaDistribuicao] [varchar](500) NULL,
		[Telefone] [varchar](20) NULL,
		[UrlAtendimento] [varchar](100) NULL,
		[Reclamacoes] [varchar](500) NULL,
		[FundosInvestimento] [varchar](500) NULL,
		[FundosInvestimentoCotas] [varchar](500) NULL,
		[EstrategiaPerdas] [char](1) NULL,
		[RegulamentoPerdasPatrimoniais] [varchar](500) NULL,
		[RegulamentoPatrimonioNegativo] [varchar](500) NULL,
	 CONSTRAINT [PK_Lamina] PRIMARY KEY CLUSTERED 
	(
		[IdCarteira] ASC,
		[InicioVigencia] ASC
	)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
	) ON [PRIMARY]

	ALTER TABLE [Lamina]  WITH CHECK ADD  CONSTRAINT [FK_Lamina_Carteira] FOREIGN KEY([IdCarteira])
	REFERENCES [Carteira] ([IdCarteira])

	ALTER TABLE [Lamina] CHECK CONSTRAINT [FK_Lamina_Carteira]
end

if not exists (select 1 from syscolumns where id = object_id('Carteira') and name = 'ProcAutomatico')
begin
	ALTER TABLE Carteira
	ADD ProcAutomatico char(1) NOT NULL DEFAULT 'N'
end

if not exists (select 1 from syscolumns where id = object_id('OrdemRendaFixa') and name = 'IdAgenteContraParte')
begin
	ALTER TABLE OrdemRendaFixa
	ADD IdAgenteContraParte int NULL foreign key references AgenteMercado(IdAgente);
end

if not exists (select 1 from syscolumns where id = object_id('OrdemRendaFixa') and name = 'IdCarteiraContraParte')
begin
	ALTER TABLE OrdemRendaFixa
	ADD IdCarteiraContraParte int NULL foreign key references Carteira(IdCarteira);
end

if not exists (select 1 from syscolumns where id = object_id('OperacaoRendaFixa') and name = 'IdAgenteContraParte')
begin
	ALTER TABLE OperacaoRendaFixa
	ADD IdAgenteContraParte int NULL foreign key references AgenteMercado(IdAgente);
end

if not exists (select 1 from syscolumns where id = object_id('OperacaoRendaFixa') and name = 'IdOperacaoEspelho')
begin
	ALTER TABLE OperacaoRendaFixa
	ADD IdOperacaoEspelho int NULL;
end

if not exists (select 1 from syscolumns where id = object_id('OperacaoRendaFixa') and name = 'DataModificacao')
begin
	ALTER TABLE OperacaoRendaFixa
	ADD DataModificacao datetime not null default GetDAte();
end

IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'InformacoesComplementaresFundo') = 0
begin
	CREATE TABLE [dbo].[InformacoesComplementaresFundo](
		[IdCarteira] [int] NOT NULL,
		[DataInicioVigencia] [datetime] NOT NULL,
		[Periodicidade] [varchar](500) NULL,
		[LocalFormaDivulgacao] [varchar](500) NULL,
		[LocalFormaSolicitacaoCotista] [varchar](500) NULL,
		[FatoresRisco] [varchar](500) NULL,
		[PoliticaExercicioVoto] [varchar](500) NULL,
		[TributacaoAplicavel] [varchar](500) NULL,
		[PoliticaAdministracaoRisco] [varchar](500) NULL,
		[AgenciaClassificacaoRisco] [varchar](500) NULL,
		[RecursosServicosGestor] [varchar](500) NULL,
		[PrestadoresServicos] [varchar](500) NULL,
		[PoliticaDistribuicaoCotas] [varchar](500) NULL,
		[Observacoes] [varchar](500) NULL
	 CONSTRAINT [PK_InformacoesComplementaresFundo] PRIMARY KEY CLUSTERED 
	(
		[IdCarteira] ASC,
		[DataInicioVigencia] ASC
	)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
	) ON [PRIMARY];


	ALTER TABLE [InformacoesComplementaresFundo]  WITH CHECK ADD  CONSTRAINT [FK_InformacoesComplementaresFundo_Carteira] FOREIGN KEY([IdCarteira])
	REFERENCES [Carteira] ([IdCarteira]);

	ALTER TABLE [InformacoesComplementaresFundo] CHECK CONSTRAINT [FK_InformacoesComplementaresFundo_Carteira]
end

IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'SuitabilityValidacao') = 0
begin
	CREATE TABLE dbo.SuitabilityValidacao
	(
		[IdValidacao] int IDENTITY(1,1) NOT NULL,
		[Descricao] varchar(100) NOT NULL
		CONSTRAINT PK_SuitabilityValidacao primary key(IdValidacao)
	)
end

--Adicionar atributo
if not exists(select 1 from syscolumns where id = object_id('SuitabilityQuestao') and name = 'IdValidacao')
BEGIN
	ALTER TABLE SuitabilityQuestao ADD IdValidacao int null
END
GO

--Criar tabelas de historico
IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'SuitabilityQuestaoHistorico') = 0
begin
	CREATE TABLE dbo.SuitabilityQuestaoHistorico
	(
		[DataHistorico] [datetime] NOT NULL,
		[IdQuestao] [int] NOT NULL,
		[Descricao] [varchar](2000) NOT NULL,
		[Fator] [int] NOT NULL,
		[IdValidacao] [int] NULL,
		[Tipo] [varchar](10) NOT NULL,
		CONSTRAINT PK_SuitabilityQuestaoHistorico primary key(DataHistorico, IdQuestao)
	)
end

IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'SuitabilityOpcaoHistorico') = 0
begin
	CREATE TABLE dbo.SuitabilityOpcaoHistorico
	(
		[DataHistorico] [datetime] NOT NULL,
		[IdOpcao] [int] IDENTITY(1,1) NOT NULL,
		[IdQuestao] [int] NOT NULL,
		[Descricao] [varchar](2000) NOT NULL,
		[Pontos] [int] NOT NULL,
		[Tipo] [varchar](10) NOT NULL,
		CONSTRAINT PK_SuitabilityOpcaoHistorico primary key(DataHistorico, IdOpcao)
	)
end

--Criar tabelas Tipo de Dispensa
IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'SuitabilityTipoDispensa') = 0
begin
	CREATE TABLE dbo.SuitabilityTipoDispensa
	(
		[IdDispensa] int IDENTITY(1,1) NOT NULL,
		[Descricao] [varchar](100) NOT NULL,
		CONSTRAINT PK_SuitabilityTipoDispensa primary key(IdDispensa)
	)
end

IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'SuitabilityTipoDispensaHistorico') = 0
begin
	CREATE TABLE dbo.SuitabilityTipoDispensaHistorico
	(
		[DataHistorico] [datetime] NOT NULL,
		[IdDispensa] int NOT NULL,
		[Descricao] [varchar](100) NOT NULL,
		[Tipo] [varchar](10) NOT NULL,
		CONSTRAINT PK_SuitabilityTipoDispensaHistorico primary key(DataHistorico, IdDispensa)
	)
end

IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'SuitabilityPerfilInvestidor') = 0
begin
	CREATE TABLE dbo.SuitabilityPerfilInvestidor
	(
		[IdPerfilInvestidor] int IDENTITY(1,1) NOT NULL,
		[Nivel] int NOT NULL default 0,
		[Perfil] [varchar](20) NOT NULL,
		CONSTRAINT PK_SuitabilityPerfilInvestidor primary key(IdPerfilInvestidor)
	)
end

IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'SuitabilityPerfilInvestidorHistorico') = 0
begin
	CREATE TABLE dbo.SuitabilityPerfilInvestidorHistorico
	(
		[DataHistorico] [datetime] NOT NULL,
		[IdPerfilInvestidor] int NOT NULL,
		[Nivel] int NOT NULL default 0,
		[Perfil] [varchar](20) NOT NULL,
		[Tipo] [varchar](10) NOT NULL,
		CONSTRAINT PK_SuitabilityPerfilInvestidorHistorico primary key(DataHistorico, IdPerfilInvestidor)
	)
end

IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'SuitabilityParametroPerfil') = 0
begin
	CREATE TABLE dbo.SuitabilityParametroPerfil
	(
		IdParametroPerfil int IDENTITY(1,1) NOT NULL,
		IdValidacao int NOT NULL,
		PerfilInvestidor int NOT NULL,
		Definicao text NULL,
		FaixaPontuacao int NULL,
		CONSTRAINT PK_SuitabilityParametroPerfil primary key(IdParametroPerfil)
	)
	alter table SuitabilityParametroPerfil add constraint SuitabilityParametroPerfil_SuitabilityValidacao_FK FOREIGN KEY ( IdValidacao ) references SuitabilityValidacao(IdValidacao) ON DELETE CASCADE; 
	alter table SuitabilityParametroPerfil add constraint SuitabilityParametroPerfil_SuitabilityPerfilInvestidor_FK FOREIGN KEY ( PerfilInvestidor ) references SuitabilityPerfilInvestidor(IdPerfilInvestidor) ON DELETE CASCADE; 
end

IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'SuitabilityParametroPerfilHistorico') = 0
begin
	CREATE TABLE dbo.SuitabilityParametroPerfilHistorico
	(
		[DataHistorico] [datetime] NOT NULL,
		IdParametroPerfil int NOT NULL,
		IdValidacao int NOT NULL,
		PerfilInvestidor int NOT NULL,
		Definicao text NULL,
		FaixaPontuacao int NULL,
		[Tipo] [varchar](10) NOT NULL,
		CONSTRAINT PK_SuitabilityParametroPerfilHistorico primary key(DataHistorico, IdParametroPerfil)
	)
end

IF NOT EXISTS(select 1 from syscolumns where id = object_id('Carteira') and name = 'PerfilRisco')
BEGIN
	Alter Table Carteira add PerfilRisco int NULL;
end

IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'CarteiraSuitabilityHistorico') = 0
begin
	CREATE TABLE dbo.CarteiraSuitabilityHistorico
	(
		[DataHistorico] [datetime] NOT NULL,
		[IdCarteira] int NOT NULL,
		[PerfilRisco] int NOT NULL,
		[Tipo] [varchar](10) NOT NULL,
		CONSTRAINT PK_CarteiraSuitabilityHistorico primary key(DataHistorico, IdCarteira)
	)	
end

IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'SuitabilityApuracaoRisco') = 0
begin
	CREATE TABLE dbo.SuitabilityApuracaoRisco
	(
		[IdApuracaoRisco] int IDENTITY(1,1) NOT NULL,
		[IdValidacao] int NOT NULL,
		[PerfilCarteira] int NOT NULL,
		[PerfilRiscoFundo] varchar(100) NOT NULL,
		[Criterio] int NOT NULL,
		[Percentual] int NOT NULL default 0,
		CONSTRAINT PK_SuitabilityApuracaoRisco primary key(IdApuracaoRisco)
	)
	
	alter table SuitabilityApuracaoRisco add constraint SuitabilityApuracaoRisco_SuitabilityPerfilInvestidor_FK FOREIGN KEY ( PerfilCarteira ) references SuitabilityPerfilInvestidor(IdPerfilInvestidor); 
	alter table SuitabilityApuracaoRisco add constraint SuitabilityApuracaoRisco_SuitabilityValidacao_FK FOREIGN KEY ( IdValidacao ) references SuitabilityValidacao(IdValidacao); 
end

IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'SuitabilityApuracaoRiscoHistorico') = 0
begin
	CREATE TABLE dbo.SuitabilityApuracaoRiscoHistorico
	(
		[DataHistorico] [datetime] NOT NULL,
		[IdApuracaoRisco] int NOT NULL,
		[IdValidacao] int NOT NULL,
		[PerfilCarteira] int NOT NULL,
		[PerfilRiscoFundo] varchar(100) NOT NULL,
		[Criterio] int NOT NULL,
		[Percentual] int NOT NULL default 0,
		[Tipo] [varchar](10) NOT NULL,
		CONSTRAINT PK_SuitabilityApuracaoRiscoHistorico primary key(DataHistorico, IdApuracaoRisco)
	)
end

IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'SuitabilityMensagens') = 0
begin
	CREATE TABLE dbo.SuitabilityMensagens
	(
		[IdMensagem] int IDENTITY(1,1) NOT NULL,
		[Assunto] varchar(100) NOT NULL,
		[Descricao] text NOT NULL,
		[ControlaConcordancia] char(1) NOT NULL default 'N',
		CONSTRAINT PK_SuitabilityMensagens primary key(IdMensagem)
	)
end

IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'SuitabilityMensagensHistorico') = 0
begin
	CREATE TABLE dbo.SuitabilityMensagensHistorico
	(
		[DataHistorico] [datetime] NOT NULL,
		[IdMensagem] int NOT NULL,
		[Assunto] varchar(100) NOT NULL,
		[Descricao] text NOT NULL,
		[ControlaConcordancia] char(1) NOT NULL,
		[Tipo] [varchar](10) NOT NULL,
		CONSTRAINT PK_SuitabilityMensagensHistorico primary key(DataHistorico, IdMensagem)
	)
end

IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'SuitabilityParametrosWorkflow') = 0
begin
	CREATE TABLE dbo.SuitabilityParametrosWorkflow
	(
		[IdParametrosWorkflow] int NOT NULL default 1,
		[AtivaWorkFlow] char(1) NOT NULL default 'N',
		[VerificaEnquadramento] int NULL,
		[ResponsavelNome] varchar(50) NULL,
		[ResponsavelEmail] varchar(100) NULL,
		[ValorPeriodo] int NULL,
		[GrandezaPeriodo] int NULL,
		CONSTRAINT PK_SuitabilityParametrosWorkflow primary key(IdParametrosWorkflow)
	)
	
end

IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'SuitabilityParametrosWorkflowHistorico') = 0
begin
	CREATE TABLE dbo.SuitabilityParametrosWorkflowHistorico
	(
		[DataHistorico] [datetime] NOT NULL,
		[IdParametrosWorkflow] int NOT NULL default 1,
		[AtivaWorkFlow] char(1) NOT NULL default 'N',
		[VerificaEnquadramento] int NULL,
		[ResponsavelNome] varchar(50) NULL,
		[ResponsavelEmail] varchar(100) NULL,
		[ValorPeriodo] int NULL,
		[GrandezaPeriodo] int NULL,
		[Tipo] [varchar](10) NOT NULL,
		CONSTRAINT PK_SuitabilityParametrosWorkflowHistorico primary key(DataHistorico, IdParametrosWorkflow)
	)
	
end
GO

IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'SuitabilityEventos') = 0
begin
	CREATE TABLE dbo.SuitabilityEventos(
		[IdSuitabilityEventos] [int] NOT NULL,
		[Descricao] [varchar](100) NOT NULL,
		[IdMensagem] [int] NULL,
	 CONSTRAINT [PK_SuitabilityEventos] PRIMARY KEY (IdSuitabilityEventos))
	 
	 alter table SuitabilityEventos add constraint SuitabilityEventos_SuitabilityMensagens_FK FOREIGN KEY ( IdMensagem ) references SuitabilityMensagens(IdMensagem); 
end

IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'SuitabilityEventosHistorico') = 0
begin
	CREATE TABLE dbo.SuitabilityEventosHistorico(
		[DataHistorico] [datetime] NOT NULL,
		[IdSuitabilityEventos] [int] NOT NULL,
		[Descricao] [varchar](100) NOT NULL,
		[IdMensagem] [int] NULL,
		[Tipo] [varchar](10) NOT NULL,
	 CONSTRAINT [PK_SuitabilityEventosHistorico] PRIMARY KEY (DataHistorico, IdSuitabilityEventos))
end


IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'SuitabilityRespostaCotista') = 0
begin
	CREATE TABLE dbo.SuitabilityRespostaCotista(
		[Data] datetime NOT NULL,
		[IdCotista] int NOT NULL,
		[Status] int NOT NULL,
	 CONSTRAINT [PK_SuitabilityRespostaCotista] PRIMARY KEY (Data, IdCotista))
	 
	 alter table SuitabilityRespostaCotista add constraint SuitabilityRespostaCotista_Cotista_FK FOREIGN KEY ( IdCotista ) references Cotista(IdCotista) on delete cascade; 
end

IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'SuitabilityRespostaCotistaDetalhe') = 0
begin
	CREATE TABLE dbo.SuitabilityRespostaCotistaDetalhe(
		[Data] datetime NOT NULL,
		[IdCotista] int NOT NULL,
		[IdQuestao] int NOT NULL,
		[IdOpcao] int NOT NULL,
	 CONSTRAINT [PK_SuitabilityRespostaCotistaDetalhe] PRIMARY KEY (Data, IdCotista, IdQuestao))
	 
	 alter table SuitabilityRespostaCotistaDetalhe add constraint SuitabilityRespostaCotistaDetalhe_SuitabilityRespostaCotista_FK FOREIGN KEY ( Data, IdCotista ) references SuitabilityRespostaCotista(Data, IdCotista) on delete cascade; 
	 alter table SuitabilityRespostaCotistaDetalhe add constraint SuitabilityRespostaCotistaDetalhe_Cotista_FK FOREIGN KEY ( IdCotista ) references Cotista(IdCotista); 
	 alter table SuitabilityRespostaCotistaDetalhe add constraint SuitabilityRespostaCotistaDetalhe_SuitabilityQuestao_FK FOREIGN KEY ( IdQuestao ) references SuitabilityQuestao(IdQuestao) on delete cascade; 
	 alter table SuitabilityRespostaCotistaDetalhe add constraint SuitabilityRespostaCotistaDetalhe_SuitabilityOpcao_FK FOREIGN KEY ( IdOpcao ) references SuitabilityOpcao(IdOpcao); 
end


IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'SuitabilityLogMensagens') = 0
begin
	CREATE TABLE dbo.SuitabilityLogMensagens(
		[IdLogMensagem] int IDENTITY(1,1) NOT NULL,
		[Data] datetime NOT NULL,
		[Usuario] varchar(100) NOT NULL,
		[IdCotista] int NOT NULL,
		[IdMensagem] int NOT NULL,
		[Mensagem] text NOT NULL,
		[Resposta] varchar(10) NOT NULL
	 CONSTRAINT [PK_SuitabilityLogMensagens] PRIMARY KEY (IdLogMensagem))
	 
	 alter table SuitabilityLogMensagens add constraint SuitabilityLogMensagens_Cotista_FK FOREIGN KEY ( IdCotista ) references Cotista(IdCotista) on delete cascade; 
	 alter table SuitabilityLogMensagens add constraint SuitabilityLogMensagens_SuitabilityMensagens_FK FOREIGN KEY ( IdMensagem ) references SuitabilityMensagens(IdMensagem) on delete cascade; 
end


IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'PessoaSuitability') = 0
begin
	CREATE TABLE dbo.PessoaSuitability
	(
		[IdPessoa] int NOT NULL,
		[IdValidacao] int NULL,
		[Perfil] int NULL,
		[Recusa] char(1) NOT NULL default 'N',
		[Dispensado] char(1) NOT NULL default 'N',
		[Dispensa] int NULL,
		[UltimaAlteracao] datetime NULL,
		CONSTRAINT PK_PessoaSuitability primary key(IdPessoa)
	)
	
	alter table PessoaSuitability add constraint PessoaSuitability_Pessoa_FK FOREIGN KEY ( IdPessoa ) references Pessoa(IdPessoa) on delete cascade; 
	alter table PessoaSuitability add constraint PessoaSuitability_TipoDispensa_FK FOREIGN KEY ( Dispensa ) references SuitabilityTipoDispensa(IdDispensa); 
	alter table PessoaSuitability add constraint PessoaSuitability_PerfilInvestidor_FK FOREIGN KEY ( Perfil ) references SuitabilityPerfilInvestidor(IdPerfilInvestidor); 
	alter table PessoaSuitability add constraint PessoaSuitability_Validacao_FK FOREIGN KEY ( IdValidacao ) references SuitabilityValidacao(IdValidacao); 
	
end
GO

IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'PessoaSuitabilityHistorico') = 0
begin
	CREATE TABLE dbo.PessoaSuitabilityHistorico
	(
		[DataHistorico] [datetime] NOT NULL,
		[IdPessoa] int NOT NULL,
		[IdValidacao] int NULL,
		[Perfil] int NULL,
		[Recusa] char(1) NOT NULL default 'N',
		[Dispensado] char(1) NOT NULL default 'N',
		[Dispensa] int NULL,
		[UltimaAlteracao] datetime NULL,
		[Tipo] [varchar](10) NOT NULL,
		CONSTRAINT PK_PessoaSuitabilityHistorico primary key(DataHistorico, IdPessoa)
	)	
end
GO


IF NOT EXISTS(select 1 from syscolumns where id = object_id('Carteira') and name = 'ControladoriaAtivo')
BEGIN
	ALTER TABLE CARTEIRA ADD ControladoriaAtivo char(1);
end

IF NOT EXISTS(select 1 from syscolumns where id = object_id('Carteira') and name = 'ControladoriaPassivo')
BEGIN
	ALTER TABLE CARTEIRA ADD ControladoriaPassivo char(1);
end

IF NOT EXISTS(select 1 from syscolumns where id = object_id('Carteira') and name = 'MesmoConglomerado')
BEGIN
	ALTER TABLE CARTEIRA ADD MesmoConglomerado char(1);
end
		
IF NOT EXISTS(select 1 from syscolumns where id = object_id('Carteira') and name = 'CategoriaAnbima')
BEGIN
	ALTER TABLE CARTEIRA ADD CategoriaAnbima int null;
end
		
IF NOT EXISTS(select 1 from syscolumns where id = object_id('Carteira') and name = 'Contratante')
BEGIN
	ALTER TABLE CARTEIRA ADD Contratante int references AgenteMercado(IdAgente);
end


--Adicionar atributo
if not exists(select 1 from syscolumns where id = object_id('SuitabilityQuestaoHistorico') and name = 'Validacao')
BEGIN
	ALTER TABLE SuitabilityQuestaoHistorico ADD Validacao varchar(100) null
END


IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'SuitabilityOpcaoHistorico') > 0
begin
	DROP TABLE SuitabilityOpcaoHistorico;

	CREATE TABLE dbo.SuitabilityOpcaoHistorico
	(
		[DataHistorico] [datetime] NOT NULL,
		[IdQuestao] [int] NOT NULL,
		[Questao] [varchar](2000) NULL,
		[IdOpcao] [int] NOT NULL,
		[Opcao] [varchar](2000) NULL,
		[Pontos] [int] NOT NULL,
		[Tipo] [varchar](10) NOT NULL,
		CONSTRAINT PK_SuitabilityOpcaoHistorico primary key(DataHistorico, IdQuestao, IdOpcao)
	)
end


IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'SuitabilityResposta') > 0
begin
	DROP TABLE SuitabilityResposta;
end


if exists(select 1 from sysconstraints where object_name(id) = 'SuitabilityRespostaCotistaDetalhe' and object_name(constid) = 'SuitabilityRespostaCotistaDetalhe_SuitabilityRespostaCotista_FK')
begin
	ALTER TABLE SuitabilityRespostaCotistaDetalhe DROP CONSTRAINT SuitabilityRespostaCotistaDetalhe_SuitabilityRespostaCotista_FK
end


IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'SuitabilityParametroPerfilHistorico') > 0
begin

	drop table SuitabilityParametroPerfilHistorico;

	CREATE TABLE dbo.SuitabilityParametroPerfilHistorico
	(
		[DataHistorico] [datetime] NOT NULL,
		IdParametroPerfil int NOT NULL,
		IdValidacao int NOT NULL,
		Validacao varchar(100) NULL,
		IdPerfilInvestidor int NOT NULL,
		PerfilInvestidor varchar(20) NOT NULL,
		Definicao text NULL,
		FaixaPontuacao int NULL,
		[Tipo] [varchar](10) NOT NULL,
		CONSTRAINT PK_SuitabilityParametroPerfilHistorico primary key(DataHistorico, IdParametroPerfil)
	)
end


IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'CarteiraSuitabilityHistorico') > 0
begin

	drop TABLE CarteiraSuitabilityHistorico

	CREATE TABLE dbo.CarteiraSuitabilityHistorico
	(
		[DataHistorico] [datetime] NOT NULL,
		[IdCarteira] int NOT NULL,
		[IdPerfilRisco] int NULL,
		[PerfilRisco] varchar(100) NULL,
		[Tipo] [varchar](10) NOT NULL,
		CONSTRAINT PK_CarteiraSuitabilityHistorico primary key(DataHistorico, IdCarteira)
	)	
end


IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'SuitabilityApuracaoRiscoHistorico') > 0
begin

	drop TABLE SuitabilityApuracaoRiscoHistorico

	CREATE TABLE dbo.SuitabilityApuracaoRiscoHistorico
	(
		[DataHistorico] [datetime] NOT NULL,
		[IdApuracaoRisco] int NOT NULL,
		[IdValidacao] int NOT NULL,
		[Validacao] varchar(100) NULL,
		[IdPerfilCarteira] int NOT NULL,
		[PerfilCarteira] varchar(100) NULL,
		[PerfilRiscoFundo] varchar(100) NOT NULL,
		[Criterio] varchar(50) NOT NULL,
		[Percentual] int NOT NULL default 0,
		[Tipo] [varchar](10) NOT NULL,
		CONSTRAINT PK_SuitabilityApuracaoRiscoHistorico primary key(DataHistorico, IdApuracaoRisco)
	)
end


IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'SuitabilityParametrosWorkflowHistorico') > 0
begin

	drop TABLE SuitabilityParametrosWorkflowHistorico

	CREATE TABLE dbo.SuitabilityParametrosWorkflowHistorico
	(
		[DataHistorico] [datetime] NOT NULL,
		[IdParametrosWorkflow] int NOT NULL default 1,
		[AtivaWorkFlow] char(1) NOT NULL default 'N',
		[VerificaEnquadramento] varchar(20) NULL,
		[ResponsavelNome] varchar(50) NULL,
		[ResponsavelEmail] varchar(100) NULL,
		[ValorPeriodo] int NULL,
		[GrandezaPeriodo] varchar(20) NULL,
		[Tipo] [varchar](10) NOT NULL,
		CONSTRAINT PK_SuitabilityParametrosWorkflowHistorico primary key(DataHistorico, IdParametrosWorkflow)
	)
	
end


IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'SuitabilityEventosHistorico') > 0
begin

	drop TABLE SuitabilityEventosHistorico;

	CREATE TABLE dbo.SuitabilityEventosHistorico(
		[DataHistorico] [datetime] NOT NULL,
		[IdSuitabilityEventos] [int] NOT NULL,
		[Descricao] [varchar](100) NOT NULL,
		[IdMensagem] [int] NULL,
		[Mesagem] text NULL,
		[Tipo] [varchar](10) NOT NULL,
	 CONSTRAINT [PK_SuitabilityEventosHistorico] PRIMARY KEY (DataHistorico, IdSuitabilityEventos))
end

IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'PessoaSuitabilityHistorico') > 0
begin

	drop table PessoaSuitabilityHistorico

	CREATE TABLE dbo.PessoaSuitabilityHistorico
	(
		[DataHistorico] [datetime] NOT NULL,
		[IdPessoa] int NOT NULL,
		[IdValidacao] int NULL,
		[Validacao] varchar(100) NULL,
		[IdPerfil] int NULL,
		[Perfil] varchar(100) NULL,
		[Recusa] char(1) NOT NULL default 'N',
		[Dispensado] char(1) NOT NULL default 'N',
		[IdDispensa] int NULL,
		[Dispensa] varchar(100) NULL,
		[UltimaAlteracao] datetime NULL,
		[Tipo] [varchar](10) NOT NULL,
		CONSTRAINT PK_PessoaSuitabilityHistorico primary key(DataHistorico, IdPessoa)
	)	
end

if not exists(select 1 from syscolumns where id = object_id('TabelaPerfilMensalCVM') and name = 'Secao18_CnpjComitente1')
BEGIN
	alter table TabelaPerfilMensalCVM add Secao18_CnpjComitente1 varchar(50) null
END

if not exists(select 1 from syscolumns where id = object_id('TabelaPerfilMensalCVM') and name = 'Secao18_CnpjComitente2')
BEGIN
	alter table TabelaPerfilMensalCVM add Secao18_CnpjComitente2 varchar(50) null
END
	
if not exists(select 1 from syscolumns where id = object_id('TabelaPerfilMensalCVM') and name = 'Secao18_CnpjComitente3')
BEGIN
	alter table TabelaPerfilMensalCVM add Secao18_CnpjComitente3 varchar(50) null
END

if not exists(select 1 from syscolumns where id = object_id('TabelaPerfilMensalCVM') and name = 'Secao18_ParteRelacionada1')
BEGIN
	alter table TabelaPerfilMensalCVM add Secao18_ParteRelacionada1 char(1) null
END
	
if not exists(select 1 from syscolumns where id = object_id('TabelaPerfilMensalCVM') and name = 'Secao18_ParteRelacionada2')
BEGIN
	alter table TabelaPerfilMensalCVM add Secao18_ParteRelacionada2 char(1) null
END
	
if not exists(select 1 from syscolumns where id = object_id('TabelaPerfilMensalCVM') and name = 'Secao18_ParteRelacionada3')
BEGIN
	alter table TabelaPerfilMensalCVM add Secao18_ParteRelacionada3 char(1) null
END

if not exists(select 1 from syscolumns where id = object_id('TabelaPerfilMensalCVM') and name = 'Secao18_ValorTotal1')
BEGIN
	alter table TabelaPerfilMensalCVM add Secao18_ValorTotal1 decimal (16, 2) null
END
	
if not exists(select 1 from syscolumns where id = object_id('TabelaPerfilMensalCVM') and name = 'Secao18_ValorTotal2')
BEGIN
	alter table TabelaPerfilMensalCVM add Secao18_ValorTotal2 decimal (16, 2) null
END
	
if not exists(select 1 from syscolumns where id = object_id('TabelaPerfilMensalCVM') and name = 'Secao18_ValorTotal3')
BEGIN
	alter table TabelaPerfilMensalCVM add Secao18_ValorTotal3 decimal (16, 2) null
END

if not exists(select 1 from syscolumns where id = object_id('TabelaPerfilMensalCVM') and name = 'Secao18_VedadaCobrancaTaxa')
BEGIN
	alter table TabelaPerfilMensalCVM add Secao18_VedadaCobrancaTaxa char(1) null
END
	
if not exists(select 1 from syscolumns where id = object_id('TabelaPerfilMensalCVM') and name = 'Secao18_DataUltimaCobranca')
BEGIN
	alter table TabelaPerfilMensalCVM add Secao18_DataUltimaCobranca datetime NULL
END
	
if not exists(select 1 from syscolumns where id = object_id('TabelaPerfilMensalCVM') and name = 'Secao18_ValorUltimaCota')
BEGIN
	alter table TabelaPerfilMensalCVM add Secao18_ValorUltimaCota decimal (16, 2) null
END

if not exists(select 1 from syscolumns where id = object_id('OperacaoRendaFixa') and name = 'IdCarteiraContraparte')
BEGIN
	ALTER TABLE OperacaoRendaFixa ADD IdCarteiraContraparte int NULL foreign key references Carteira(IdCarteira)
END

IF NOT EXISTS(select 1 from syscolumns where id = object_id('Carteira') and name = 'ExplodeCotasDeFundos')
BEGIN
	ALTER TABLE Carteira ADD ExplodeCotasDeFundos char(1) NOT NULL DEFAULT 'N';
END

UPDATE InformacoesComplementaresFundo SET Periodicidade = SUBSTRING(Periodicidade,1, 250);
UPDATE InformacoesComplementaresFundo SET LocalFormaDivulgacao = SUBSTRING(LocalFormaDivulgacao,1, 300);
UPDATE InformacoesComplementaresFundo SET LocalFormaSolicitacaoCotista = SUBSTRING(LocalFormaSolicitacaoCotista,1, 250);
UPDATE InformacoesComplementaresFundo SET AgenciaClassificacaoRisco = SUBSTRING(AgenciaClassificacaoRisco,1, 50);
UPDATE InformacoesComplementaresFundo SET RecursosServicosGestor = SUBSTRING(RecursosServicosGestor,1, 100);
UPDATE InformacoesComplementaresFundo SET PrestadoresServicos = SUBSTRING(PrestadoresServicos,1, 100);

ALTER TABLE InformacoesComplementaresFundo ALTER COLUMN Periodicidade varchar(250) null;
ALTER TABLE InformacoesComplementaresFundo ALTER COLUMN LocalFormaDivulgacao varchar(300) null;
ALTER TABLE InformacoesComplementaresFundo ALTER COLUMN LocalFormaSolicitacaoCotista varchar(250) null;
ALTER TABLE InformacoesComplementaresFundo ALTER COLUMN FatoresRisco varchar(2000) null;
ALTER TABLE InformacoesComplementaresFundo ALTER COLUMN PoliticaExercicioVoto varchar(1000) null;
ALTER TABLE InformacoesComplementaresFundo ALTER COLUMN TributacaoAplicavel varchar(2500) null;
ALTER TABLE InformacoesComplementaresFundo ALTER COLUMN PoliticaAdministracaoRisco varchar(2500) null;
ALTER TABLE InformacoesComplementaresFundo ALTER COLUMN AgenciaClassificacaoRisco varchar(50) null;
ALTER TABLE InformacoesComplementaresFundo ALTER COLUMN RecursosServicosGestor varchar(100) null;
ALTER TABLE InformacoesComplementaresFundo ALTER COLUMN PrestadoresServicos varchar(100) null;	

IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'OperacaoCotistaAux') = 0
Begin
	CREATE TABLE [dbo].[OperacaoCotistaAux]
	(
		[IdOperacaoCotistaAux] [int] IDENTITY(1,1) PRIMARY KEY NOT NULL,
		[IdOperacao] [int] NOT NULL,
		[IdCotista] [int] NOT NULL,
		[IdCarteira] [int] NOT NULL,
		[DataOperacao] [datetime] NOT NULL,
		[DataConversao] [datetime] NOT NULL,
		[DataLiquidacao] [datetime] NOT NULL,
		[DataAgendamento] [datetime] NOT NULL,
		[TipoOperacao] [tinyint] NOT NULL,
		[TipoResgate] [tinyint] NULL,
		[IdPosicaoResgatada] [int] NULL,
		[IdFormaLiquidacao] [tinyint] NOT NULL,
		[Quantidade] [decimal](28, 12) NOT NULL,
		[CotaOperacao] [decimal](28, 12) NOT NULL,
		[ValorBruto] [decimal](16, 2) NOT NULL,
		[ValorLiquido] [decimal](16, 2) NOT NULL,
		[ValorIR] [decimal](16, 2) NOT NULL,
		[ValorIOF] [decimal](16, 2) NOT NULL,
		[ValorCPMF] [decimal](16, 2) NOT NULL,
		[ValorPerformance] [decimal](16, 2) NOT NULL,
		[PrejuizoUsado] [decimal](16, 2) NOT NULL,
		[RendimentoResgate] [decimal](16, 2) NOT NULL,
		[VariacaoResgate] [decimal](16, 2) NOT NULL,
		[Observacao] [varchar](400) NOT NULL,
		[DadosBancarios] [varchar](500) NULL,
		[Fonte] [tinyint] NOT NULL,
		[IdConta] [int] NULL,
		[CotaInformada] [decimal](28, 12) NULL,
		[IdAgenda] [int] NULL,
		[IdOperacaoResgatada] [int] NULL,
		[IdOperacaoAuxiliar] [int] NULL,
		[IdOrdem] [int] NULL
	)
END	

IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'DetalheResgateCotistaAux') = 0
Begin
	CREATE TABLE [dbo].[DetalheResgateCotistaAux]
	(
		[IdOperacao] [int] NOT NULL,
		[IdPosicaoResgatada] [int] NOT NULL,
		[IdCotista] [int] NOT NULL,
		[IdCarteira] [int] NOT NULL,
		[Quantidade] [decimal](28, 12) NOT NULL,
		[ValorBruto] [decimal](16, 2) NOT NULL,
		[ValorLiquido] [decimal](16, 2) NOT NULL,
		[ValorCPMF] [decimal](16, 2) NOT NULL,
		[ValorPerformance] [decimal](16, 2) NOT NULL,
		[PrejuizoUsado] [decimal](16, 2) NOT NULL,
		[RendimentoResgate] [decimal](16, 2) NOT NULL,
		[VariacaoResgate] [decimal](16, 2) NOT NULL,
		[ValorIR] [decimal](16, 2) NOT NULL,
		[ValorIOF] [decimal](16, 2) NOT NULL,
		[DataOperacao] [datetime] NOT NULL,
		[TipoOperacao] [TinyInt] NOT NULL,
	 CONSTRAINT [PK_DetalheResgateCotistaAux] PRIMARY KEY CLUSTERED 
	(
		[IdOperacao] ASC,
		[IdPosicaoResgatada] ASC
	)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
	) ON [PRIMARY]
END

IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'PosicaoCotistaAux') = 0
Begin
	CREATE TABLE [dbo].[PosicaoCotistaAux]
	(
		[IdPosicaoCotistaAux] [int] IDENTITY(1,1) PRIMARY KEY NOT NULL,
		[DataReferencia] [datetime] NOT NULL,
		[IdPosicao] [int] NOT NULL,
		[IdOperacao] [int] NULL,
		[IdCotista] [int] NOT NULL,
		[IdCarteira] [int] NOT NULL,
		[ValorAplicacao] [decimal](16, 2) NOT NULL,
		[DataAplicacao] [datetime] NOT NULL,
		[DataConversao] [datetime] NOT NULL,
		[CotaAplicacao] [decimal](28, 12) NOT NULL,
		[CotaDia] [decimal](28, 12) NOT NULL,
		[ValorBruto] [decimal](16, 2) NOT NULL,
		[ValorLiquido] [decimal](16, 2) NOT NULL,
		[QuantidadeInicial] [decimal](28, 12) NOT NULL,
		[Quantidade] [decimal](28, 12) NOT NULL,
		[QuantidadeBloqueada] [decimal](28, 12) NOT NULL,
		[DataUltimaCobrancaIR] [datetime] NOT NULL,
		[ValorIR] [decimal](16, 2) NOT NULL,
		[ValorIOF] [decimal](16, 2) NOT NULL,
		[ValorPerformance] [decimal](16, 2) NOT NULL,
		[ValorIOFVirtual] [decimal](16, 2) NOT NULL,
		[QuantidadeAntesCortes] [decimal](28, 12) NOT NULL,
		[ValorRendimento] [decimal](16, 2) NOT NULL,
		[DataUltimoCortePfee] [datetime] NULL,
		[PosicaoIncorporada] [char](1) NOT NULL
	)
END	

IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'OperacaoFundoAux') = 0
Begin
	CREATE TABLE [dbo].[OperacaoFundoAux](
		[IdOperacaoFundoAux] [int] IDENTITY(1,1) PRIMARY KEY NOT NULL,
		[IdOperacao] [int] NOT NULL,
		[IdCliente] [int] NOT NULL,
		[IdCarteira] [int] NOT NULL,
		[DataOperacao] [datetime] NOT NULL,
		[DataConversao] [datetime] NOT NULL,
		[DataLiquidacao] [datetime] NOT NULL,
		[DataAgendamento] [datetime] NOT NULL,
		[TipoOperacao] [tinyint] NOT NULL,
		[TipoResgate] [tinyint] NULL,
		[IdPosicaoResgatada] [int] NULL,
		[IdFormaLiquidacao] [tinyint] NOT NULL,
		[Quantidade] [decimal](28, 12) NOT NULL,
		[CotaOperacao] [decimal](28, 12) NOT NULL,
		[ValorBruto] [decimal](16, 2) NOT NULL,
		[ValorLiquido] [decimal](16, 2) NOT NULL,
		[ValorIR] [decimal](16, 2) NOT NULL,
		[ValorIOF] [decimal](16, 2) NOT NULL,
		[ValorCPMF] [decimal](16, 2) NOT NULL,
		[ValorPerformance] [decimal](16, 2) NOT NULL,
		[PrejuizoUsado] [decimal](16, 2) NOT NULL,
		[RendimentoResgate] [decimal](16, 2) NOT NULL,
		[VariacaoResgate] [decimal](16, 2) NOT NULL,
		[IdConta] [int] NULL,
		[Observacao] [varchar](400) NOT NULL,
		[Fonte] [tinyint] NOT NULL,
		[IdAgenda] [int] NULL,
		[CotaInformada] [decimal](28, 12) NULL,
		[IdOperacaoResgatada] [int] NULL
	)
END	

IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'PosicaoFundoAux') = 0
Begin	
	CREATE TABLE [dbo].[PosicaoFundoAux]
	(
		[IdPosicaoFundoAux] [int] IDENTITY(1,1) PRIMARY KEY NOT NULL,
		[DataReferencia] [datetime] NOT NULL,
		[IdPosicao] [int] NOT NULL,
		[IdOperacao] [int] NULL,
		[IdCliente] [int] NOT NULL,
		[IdCarteira] [int] NOT NULL,
		[ValorAplicacao] [decimal](16, 2) NOT NULL,
		[DataAplicacao] [datetime] NOT NULL,
		[DataConversao] [datetime] NOT NULL,
		[CotaAplicacao] [decimal](28, 12) NOT NULL,
		[CotaDia] [decimal](28, 12) NOT NULL,
		[ValorBruto] [decimal](16, 2) NOT NULL,
		[ValorLiquido] [decimal](16, 2) NOT NULL,
		[QuantidadeInicial] [decimal](28, 12) NOT NULL,
		[Quantidade] [decimal](28, 12) NOT NULL,
		[QuantidadeBloqueada] [decimal](28, 12) NULL,
		[DataUltimaCobrancaIR] [datetime] NOT NULL,
		[ValorIR] [decimal](16, 2) NOT NULL,
		[ValorIOF] [decimal](16, 2) NOT NULL,
		[ValorPerformance] [decimal](16, 2) NOT NULL,
		[ValorIOFVirtual] [decimal](16, 2) NOT NULL,
		[QuantidadeAntesCortes] [decimal](28, 12) NOT NULL,
		[ValorRendimento] [decimal](16, 2) NOT NULL,
		[DataUltimoCortePfee] [datetime] NULL,
		[PosicaoIncorporada] [char](1) NOT NULL
	)
END	

IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'DetalheResgateFundoAux') = 0
Begin
CREATE TABLE [dbo].[DetalheResgateFundoAux]
(
	[IdOperacao] [int] NOT NULL,
	[IdPosicaoResgatada] [int] NOT NULL,
	[IdCliente] [int] NOT NULL,
	[IdCarteira] [int] NOT NULL,
	[Quantidade] [decimal](28, 12) NOT NULL,
	[ValorBruto] [decimal](16, 2) NOT NULL,
	[ValorLiquido] [decimal](16, 2) NOT NULL,
	[ValorCPMF] [decimal](16, 2) NOT NULL,
	[ValorPerformance] [decimal](16, 2) NOT NULL,
	[PrejuizoUsado] [decimal](16, 2) NOT NULL,
	[RendimentoResgate] [decimal](16, 2) NOT NULL,
	[VariacaoResgate] [decimal](16, 2) NOT NULL,
	[ValorIR] [decimal](16, 2) NOT NULL,
	[ValorIOF] [decimal](16, 2) NOT NULL,
	[DataOperacao] [datetime] NOT NULL,
	[TipoOperacao] [TinyInt] NOT NULL,
	CONSTRAINT [PK_DetalheResgateFundoAux] PRIMARY KEY CLUSTERED 
	(
		[IdOperacao] ASC,
		[IdPosicaoResgatada] ASC
	)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
	) ON [PRIMARY]
END

if not exists(select 1 from syscolumns where id = object_id('OperacaoFundo') and name = 'RegistroEditado')
BEGIN
	ALTER TABLE dbo.OperacaoFundo ADD RegistroEditado char(1) NOT NULL DEFAULT 'N'
END
	
if not exists(select 1 from syscolumns where id = object_id('OperacaoFundo') and name = 'ValoresColados')
BEGIN
	ALTER TABLE dbo.OperacaoFundo ADD ValoresColados char(1) NOT NULL DEFAULT 'N' 
END
	
if not exists(select 1 from syscolumns where id = object_id('OperacaoFundoAux') and name = 'RegistroEditado')
BEGIN
	ALTER TABLE dbo.OperacaoFundoAux ADD RegistroEditado char(1) NOT NULL DEFAULT 'N'
END
	
if not exists(select 1 from syscolumns where id = object_id('OperacaoFundoAux') and name = 'ValoresColados')
BEGIN
	ALTER TABLE dbo.OperacaoFundoAux ADD ValoresColados char(1) NOT NULL DEFAULT 'N' 
END

if not exists(select 1 from syscolumns where id = object_id('OperacaoCotista') and name = 'RegistroEditado')
BEGIN
	ALTER TABLE dbo.OperacaoCotista ADD RegistroEditado char(1) NOT NULL DEFAULT 'N'
END	

if not exists(select 1 from syscolumns where id = object_id('OperacaoCotista') and name = 'ValoresColados')
BEGIN
	ALTER TABLE dbo.OperacaoCotista ADD ValoresColados char(1) NOT NULL DEFAULT 'N'
END	

if not exists(select 1 from syscolumns where id = object_id('OperacaoCotistaAux') and name = 'RegistroEditado')
BEGIN
	ALTER TABLE dbo.OperacaoCotistaAux ADD RegistroEditado char(1) NOT NULL DEFAULT 'N'
END	

if not exists(select 1 from syscolumns where id = object_id('OperacaoCotistaAux') and name = 'ValoresColados')
BEGIN
	ALTER TABLE dbo.OperacaoCotistaAux ADD ValoresColados char(1) NOT NULL DEFAULT 'N'
END	

IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'ColagemComeCotas') = 0
Begin
	CREATE TABLE [dbo].[ColagemComeCotas]
	(
		IdColagemComeCotas [int] PRIMARY KEY NOT NULL IDENTITY,
		TipoRegistro char(1) NOT NULL ,
		CodigoExterno char(15) NOT NULL ,
		DataReferencia [datetime] NOT NULL,
		DataOperacao [datetime] NOT NULL,
		IdCliente  [int] NOT NULL,
		IdCarteira  [int] NOT NULL,
		DataConversao [datetime] NULL,
		Quantidade [decimal](28, 12) NOT NULL,
		CotaOperacao [decimal](28, 12) NOT NULL,
		ValorBruto [decimal](28, 12) NOT NULL,
		ValorLiquido [decimal](28, 12) NOT NULL,
		ValorCPMF [decimal](28, 12) NOT NULL,
		ValorPerformance [decimal](28, 12) NOT NULL,
		PrejuizoUsado [decimal](28, 12) NOT NULL,
		RendimentoComeCotas [decimal](28, 12) NOT NULL,
		VariacaoComeCotas [decimal](28, 12) NOT NULL,
		ValorIR [decimal](28, 12) NOT NULL,
		ValorIOF [decimal](28, 12) NOT NULL
	)	
END	

IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'ColagemComeCotasDetalhe') = 0
Begin
	CREATE TABLE [dbo].[ColagemComeCotasDetalhe]
	(
		IdColagemComeCotasDetalhe  [int] PRIMARY KEY NOT NULL IDENTITY,
		CodigoExterno char(15) NOT NULL ,
		DataReferencia [datetime] NOT NULL,
		DataOperacao [datetime] NOT NULL,
		IdCliente  [int] NOT NULL,
		IdCarteira  [int] NOT NULL,
		DataConversao [datetime] NULL,
		Quantidade [decimal](28, 12) NOT NULL,
		CotaOperacao [decimal](28, 12) NOT NULL,
		ValorBruto [decimal](28, 12) NOT NULL,
		ValorLiquido [decimal](28, 12) NOT NULL,
		ValorCPMF [decimal](28, 12) NOT NULL,
		ValorPerformance [decimal](28, 12) NOT NULL,
		PrejuizoUsado [decimal](28, 12) NOT NULL,
		RendimentoComeCotas [decimal](28, 12) NOT NULL,
		VariacaoComeCotas [decimal](28, 12) NOT NULL,
		ValorIR [decimal](28, 12) NOT NULL,
		ValorIOF [decimal](28, 12) NOT NULL,
	)	
END	

IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'ColagemComeCotasDetalhePosicao') = 0
Begin
	CREATE TABLE [dbo].[ColagemComeCotasDetalhePosicao]
	(
		IdColagemComeCotasDetalhePosicao [int] PRIMARY KEY NOT NULL IDENTITY,
		IdColagemComeCotasDetalhe [int] NOT NULL,
		DataReferencia [datetime] NOT NULL,
		QuantidadeInicial [decimal](28, 12) NOT NULL,
		QuantidadeRestante [decimal](28, 12) NOT NULL
	)		
END	

IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'ColagemComeCotasMapaFundo') = 0
Begin
	CREATE TABLE [dbo].[ColagemComeCotasMapaFundo]
	(
		IdColagemComeCotasMapaFundo  [int] PRIMARY KEY NOT NULL IDENTITY,
		DataReferencia [datetime] NOT NULL,
		IdCliente  [int] NOT NULL,
		IdCarteira  [int] NOT NULL,
		DataConversao [datetime] NULL,
		DetalheImportado char(1) NOT NULL DEFAULT 'N',
		IdOperacaoComeCotasVinculada  [int] NOT NULL,	
		Participacao [decimal](28, 12) NOT NULL
	)		
END	

IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'ColagemComeCotasPosFundo') = 0
Begin
	CREATE TABLE [dbo].[ColagemComeCotasPosFundo]
	(
		IdColagemComeCotasPosFundo  [int] PRIMARY KEY NOT NULL IDENTITY,
		IdCarteira  				[int] NOT NULL,
		IdCliente  					[int] NOT NULL,
		DataConversao 				[datetime] NULL,
		DataReferencia 				[datetime] NOT NULL,
		IdPosicaoVinculada  		[int] NOT NULL,
		PrejuizoUsado 				[decimal](28, 12) NOT NULL,
		RendimentoComeCotas 		[decimal](28, 12) NOT NULL,
		ValorBruto 					[decimal](28, 12) NOT NULL,
		ValorCPMF 					[decimal](28, 12) NOT NULL,
		ValorIOF 					[decimal](28, 12) NOT NULL,
		ValorIR 					[decimal](28, 12) NOT NULL,
		ValorLiquido 				[decimal](28, 12) NOT NULL,
		ValorPerformance 			[decimal](28, 12) NOT NULL,
		VariacaoComeCotas 			[decimal](28, 12) NOT NULL,
		Quantidade					[decimal](28, 12) NOT NULL,
		Participacao				[decimal](28, 12) NOT NULL,
		DetalheImportado 			[char](1) NOT NULL DEFAULT 'N'
	)		
END	


IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'ColagemResgateMapaFundo') = 0
Begin
	CREATE TABLE [dbo].[ColagemResgateMapaFundo]
	(
		IdColagemResgateMapaFundo  [int] PRIMARY KEY NOT NULL IDENTITY,
		DataReferencia [datetime] NOT NULL,
		IdCliente  [int] NOT NULL,
		IdCarteira  [int] NOT NULL,
		DataConversao [datetime] NULL,
		DetalheImportado char(1) NOT NULL DEFAULT 'N',
		IdOperacaoResgateVinculada  [int] NOT NULL,	
		Participacao [decimal](28, 12) NOT NULL
	)		
END	

IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'ColagemOperResgPosFundo') = 0
Begin
	CREATE TABLE [dbo].[ColagemOperResgPosFundo]
	(
		IdColagemOperResgPosFundo  [int] PRIMARY KEY NOT NULL IDENTITY,
		IdCarteira  [int] NOT NULL,
		IdCliente  [int] NOT NULL,
		DataReferencia [datetime] NOT NULL,
		IdColagemResgateDetalhe  [int] NOT NULL,
		IdOperacaoResgateVinculada  [int] NOT NULL,
		IdPosicaoVinculada  [int] NOT NULL,
		QuantidadeVinculada [decimal](28, 12) NOT NULL
	)		
END	


IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'ColagemResgate') = 0
Begin
	CREATE TABLE [dbo].[ColagemResgate]
	(
		IdColagemResgate [int] PRIMARY KEY NOT NULL IDENTITY,
		TipoRegistro char(1) NOT NULL ,
		CodigoExterno char(15) NOT NULL ,
		DataReferencia [datetime] NOT NULL,
		DataOperacao [datetime] NOT NULL,
		IdCliente  [int] NOT NULL,
		IdCarteira  [int] NOT NULL,
		DataConversao [datetime] NULL,
		Quantidade [decimal](28, 12) NOT NULL,
		CotaOperacao [decimal](28, 12) NOT NULL,
		ValorBruto [decimal](28, 12) NOT NULL,
		ValorLiquido [decimal](28, 12) NOT NULL,
		ValorCPMF [decimal](28, 12) NOT NULL,
		ValorPerformance [decimal](28, 12) NOT NULL,
		PrejuizoUsado [decimal](28, 12) NOT NULL,
		RendimentoResgate [decimal](28, 12) NOT NULL,
		VariacaoResgate [decimal](28, 12) NOT NULL,
		ValorIR [decimal](28, 12) NOT NULL,
		ValorIOF [decimal](28, 12) NOT NULL
	)	
END	

IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'ColagemResgateDetalhe') = 0
Begin
	CREATE TABLE [dbo].[ColagemResgateDetalhe]
	(
		IdColagemResgateDetalhe  [int] PRIMARY KEY NOT NULL IDENTITY,
		CodigoExterno char(15) NOT NULL ,
		DataReferencia [datetime] NOT NULL,
		DataOperacao [datetime] NOT NULL,
		IdCliente  [int] NOT NULL,
		IdCarteira  [int] NOT NULL,
		DataConversao [datetime] NULL,
		Quantidade [decimal](28, 12) NOT NULL,
		CotaOperacao [decimal](28, 12) NOT NULL,
		ValorBruto [decimal](28, 12) NOT NULL,
		ValorLiquido [decimal](28, 12) NOT NULL,
		ValorCPMF [decimal](28, 12) NOT NULL,
		ValorPerformance [decimal](28, 12) NOT NULL,
		PrejuizoUsado [decimal](28, 12) NOT NULL,
		RendimentoResgate [decimal](28, 12) NOT NULL,
		VariacaoResgate [decimal](28, 12) NOT NULL,
		ValorIR [decimal](28, 12) NOT NULL,
		ValorIOF [decimal](28, 12) NOT NULL,
	)	
END	

 IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'ColagemResgateDetalhePosicao') = 0
Begin
	CREATE TABLE [dbo].[ColagemResgateDetalhePosicao]
	(
		IdColagemResgateDetalhePosicao [int] PRIMARY KEY NOT NULL IDENTITY,
		IdColagemResgateDetalhe [int] NOT NULL,
		DataReferencia [datetime] NOT NULL,
		QuantidadeInicial [decimal](28, 12) NOT NULL,
		QuantidadeRestante [decimal](28, 12) NOT NULL
	)		
END	

IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'ColagemResgateMapaCotista') = 0
Begin
	CREATE TABLE [dbo].[ColagemResgateMapaCotista]
	(
		IdColagemResgateMapaCotista  [int] PRIMARY KEY NOT NULL IDENTITY,
		DataReferencia [datetime] NOT NULL,
		IdCliente  [int] NOT NULL,
		IdCarteira  [int] NOT NULL,
		DataConversao [datetime] NULL,
		DetalheImportado char(1) NOT NULL DEFAULT 'N',
		IdOperacaoResgateVinculada  [int] NOT NULL,	
		Participacao [decimal](28, 12) NOT NULL
	)		
END	

IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'ColagemOperResgPosCotista') = 0
Begin
	CREATE TABLE [dbo].[ColagemOperResgPosCotista]
	(
		IdColagemOperResgPosCotista  [int] PRIMARY KEY NOT NULL IDENTITY,
		IdCarteira  [int] NOT NULL,
		IdCliente  [int] NOT NULL,
		DataReferencia [datetime] NOT NULL,
		IdColagemResgateDetalhe  [int] NOT NULL,
		IdOperacaoResgateVinculada  [int] NOT NULL,
		IdPosicaoVinculada  [int] NOT NULL,
		QuantidadeVinculada [decimal](28, 12) NOT NULL
	)		
END	

if exists(select 1 from syscolumns where id = object_id('ColagemResgateMapaFundo') and name = 'Participacao')
BEGIN	
	ALTER TABLE [dbo].[ColagemResgateMapaFundo] DROP COLUMN Participacao;
END

if NOT exists(select 1 from syscolumns where id = object_id('ColagemResgateMapaFundo') and name = 'IdColagemResgateDetalhe')
BEGIN	
	ALTER TABLE [dbo].[ColagemResgateMapaFundo] ADD IdColagemResgateDetalhe int NULL;
END

if exists(select 1 from syscolumns where id = object_id('ColagemResgate') and name = 'DataOperacao')
BEGIN	
	ALTER TABLE [dbo].[ColagemResgate] DROP COLUMN DataOperacao;
END

if exists(select 1 from syscolumns where id = object_id('ColagemResgateDetalhe') and name = 'DataOperacao')
BEGIN	
	ALTER TABLE [dbo].[ColagemResgateDetalhe] DROP COLUMN DataOperacao;
END

if exists(select 1 from syscolumns where id = object_id('ColagemComeCotas') and name = 'DataOperacao')
BEGIN	
	ALTER TABLE [dbo].[ColagemComeCotas] DROP COLUMN DataOperacao;
END

if exists(select 1 from syscolumns where id = object_id('ColagemComeCotasDetalhe') and name = 'DataOperacao')
BEGIN	
	ALTER TABLE [dbo].[ColagemComeCotasDetalhe] DROP COLUMN DataOperacao;
END

if exists(select 1 from syscolumns where id = object_id('ColagemResgateMapaCotista') and name = 'Participacao')
BEGIN	
	ALTER TABLE [dbo].[ColagemResgateMapaCotista] DROP COLUMN Participacao;
END

if NOT exists(select 1 from syscolumns where id = object_id('ColagemResgateMapaCotista') and name = 'IdColagemResgateDetalhe')
BEGIN	
	ALTER TABLE [dbo].[ColagemResgateMapaCotista] ADD IdColagemResgateDetalhe int NULL;
END

if not exists(select 1 from syscolumns where id = object_id('ColagemResgate') and name = 'Processado')
BEGIN	
	ALTER TABLE [dbo].[ColagemResgate] ADD Processado char(1) NOT NULL DEFAULT 'N';
END

if not exists(select 1 from syscolumns where id = object_id('ColagemComeCotas') and name = 'Processado')
BEGIN	
	ALTER TABLE [dbo].[ColagemComeCotas] ADD Processado char(1) NOT NULL DEFAULT 'N';
END

IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'ColagemComeCotasPosCotista') = 0
Begin
	CREATE TABLE [dbo].[ColagemComeCotasPosCotista]
	(
		IdColagemComeCotasPosCotista  [int] PRIMARY KEY NOT NULL IDENTITY,
		IdCarteira  				[int] NOT NULL,
		IdCliente  					[int] NOT NULL,
		DataConversao 				[datetime] NULL,
		DataReferencia 				[datetime] NOT NULL,
		IdPosicaoVinculada  		[int] NOT NULL,
		PrejuizoUsado 				[decimal](28, 12) NOT NULL,
		RendimentoComeCotas 		[decimal](28, 12) NOT NULL,
		ValorBruto 					[decimal](28, 12) NOT NULL,
		ValorCPMF 					[decimal](28, 12) NOT NULL,
		ValorIOF 					[decimal](28, 12) NOT NULL,
		ValorIR 					[decimal](28, 12) NOT NULL,
		ValorLiquido 				[decimal](28, 12) NOT NULL,
		ValorPerformance 			[decimal](28, 12) NOT NULL,
		VariacaoComeCotas 			[decimal](28, 12) NOT NULL,
		Quantidade					[decimal](28, 12) NOT NULL,
		Participacao				[decimal](28, 12) NOT NULL,
		DetalheImportado 			[char](1) NOT NULL DEFAULT 'N'
	)		
END	

if not exists(select 1 from syscolumns where id = object_id('InformacoesComplementaresFundo') and name = 'DsLocalDivulg')
BEGIN
	ALTER TABLE dbo.InformacoesComplementaresFundo ADD DsLocalDivulg varchar(300) NOT NULL;
END	

if not exists(select 1 from syscolumns where id = object_id('InformacoesComplementaresFundo') and name = 'DsResp')
BEGIN
	ALTER TABLE dbo.InformacoesComplementaresFundo ADD DsResp varchar(250) NOT NULL;
END	

if not exists(select 1 from syscolumns where id = object_id('InformacoesComplementaresFundo') and name = 'CodVotoGestAssemb')
BEGIN
	ALTER TABLE dbo.InformacoesComplementaresFundo ADD CodVotoGestAssemb varchar(1) NOT NULL;
END	

if not exists(select 1 from syscolumns where id = object_id('InformacoesComplementaresFundo') and name = 'AgencClassifRatin')
BEGIN
	ALTER TABLE dbo.InformacoesComplementaresFundo ADD AgencClassifRatin varchar(1) NOT NULL;
END	

if not exists(select 1 from syscolumns where id = object_id('InformacoesComplementaresFundo') and name = 'ApresDetalheAdm')
BEGIN
	ALTER TABLE dbo.InformacoesComplementaresFundo ADD ApresDetalheAdm varchar(2500) NOT NULL;
END	

if not exists(select 1 from syscolumns where id = object_id('InformacoesComplementaresFundo') and name = 'DsServicoPrestado')
BEGIN
	ALTER TABLE dbo.InformacoesComplementaresFundo ADD DsServicoPrestado varchar(100) NOT NULL;
END	

if not exists(select 1 from syscolumns where id = object_id('InformacoesComplementaresFundo') and name = 'CodDistrOfertaPub')
BEGIN
	ALTER TABLE dbo.InformacoesComplementaresFundo ADD CodDistrOfertaPub varchar(1) NOT NULL;
END	

if not exists(select 1 from syscolumns where id = object_id('InformacoesComplementaresFundo') and name = 'InformAutoregulAnbima')
BEGIN
	ALTER TABLE dbo.InformacoesComplementaresFundo ADD InformAutoregulAnbima varchar(2500) NOT NULL;
END	

if not exists(select 1 from syscolumns where id = object_id('InformacoesComplementaresFundo') and name = 'CodMeioDivulg')
BEGIN
	ALTER TABLE dbo.InformacoesComplementaresFundo ADD CodMeioDivulg varchar(1) NOT NULL;
END	

if not exists(select 1 from syscolumns where id = object_id('InformacoesComplementaresFundo') and name = 'CodMeio')
BEGIN
	ALTER TABLE dbo.InformacoesComplementaresFundo ADD CodMeio varchar(1) NOT NULL;
END	

if not exists(select 1 from syscolumns where id = object_id('InformacoesComplementaresFundo') and name = 'DsLocal')
BEGIN
	ALTER TABLE dbo.InformacoesComplementaresFundo ADD DsLocal varchar(100) NOT NULL;
END	

if not exists(select 1 from syscolumns where id = object_id('InformacoesComplementaresFundo') and name = 'NrCnpj')
BEGIN
	ALTER TABLE dbo.InformacoesComplementaresFundo ADD NrCnpj varchar(14) NOT NULL;
END	

if not exists(select 1 from syscolumns where id = object_id('InformacoesComplementaresFundo') and name = 'NmPrest')
BEGIN
	ALTER TABLE dbo.InformacoesComplementaresFundo ADD NmPrest varchar(100) NOT NULL;
END	

if not exists(select 1 from syscolumns where id = object_id('InformacoesComplementaresFundo') and name = 'DisclAdvert')
BEGIN
	ALTER TABLE dbo.InformacoesComplementaresFundo ADD DisclAdvert varchar(100) NOT NULL;
END	

ALTER TABLE GerOperacaoBolsa ALTER COLUMN CdAtivoBolsaOpcao VARCHAR(25) NULL;

if not exists(select 1 from syscolumns where id = object_id('Carteira') and name = 'ComeCotasEntreRegatesConversao')
BEGIN
	ALTER TABLE dbo.Carteira ADD ComeCotasEntreRegatesConversao char(1) NOT NULL DEFAULT 'S'
END

if not exists(select 1 from syscolumns where id = object_id('OperacaoRendaFixa') and name = 'StatusBatimento')
BEGIN
	ALTER TABLE OperacaoRendaFixa add StatusBatimento int NULL;
end

if not exists(select 1 from syscolumns where id = object_id('Lamina') and name = 'ExisteCarencia')
BEGIN
	ALTER TABLE Lamina ADD ExisteCarencia CHAR(1) NULL;
END
	
if not exists(select 1 from syscolumns where id = object_id('Lamina') and name = 'ExisteTaxaEntrada')
BEGIN
	ALTER TABLE Lamina ADD ExisteTaxaEntrada CHAR(1) NULL;
END
	
if not exists(select 1 from syscolumns where id = object_id('Lamina') and name = 'ExisteTaxaSaida')
BEGIN
	ALTER TABLE Lamina ADD ExisteTaxaSaida CHAR(1) NULL;
END

IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'PrazoMedio') = 0
begin
	CREATE TABLE dbo.PrazoMedio
	(
		IdPrazoMedio			 INT 			NOT NULL PRIMARY KEY identity,
		DataAtual 		 		 DateTime		NOT NULL,
		IdCliente		 		 INT 			NOT NULL,
		Descricao 		 		 VARCHAR(255) 	NOT NULL,
		PrazoMedioAtivo		 	 INT 			NOT NULL,
		PrazoMedio		 		 Decimal(10,0)	NOT NULL,
		ValorPosicao	 		 Decimal(25,2)	NOT NULL
	)
END

IF EXISTS(SELECT * FROM sys.columns WHERE NAME = N'IdMercadoTipoPessoa' and Object_ID = Object_ID(N'CadastroComplementar'))
BEGIN
 ALTER TABLE CadastroComplementar ALTER COLUMN IdMercadoTipoPessoa varchar(20)
END

IF EXISTS(SELECT * FROM sys.columns WHERE NAME = N'PrazoMedioAtivo' and Object_ID = Object_ID(N'PrazoMedio'))
BEGIN
 exec sp_rename 'PrazoMedio.PrazoMedioAtivo', 'MercadoAtivo'
END

IF (select count(*) from SYSCOLUMNS WHERE ID = (SELECT ID FROM SYSOBJECTS WHERE  NAME = 'PrazoMedio' ) and name = 'IdPosicao') = 0
begin
 alter table PrazoMedio add IdPosicao int NULL;
END 

IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'SuitabilityParametrosWorkflow') = 0
begin
	CREATE TABLE dbo.SuitabilityParametrosWorkflow
	(
		[IdParametrosWorkflow] int NOT NULL default 1,
		[AtivaWorkFlow] char(1) NOT NULL default 'N',
		[VerificaEnquadramento] int NULL,
		[ResponsavelNome] varchar(50) NULL,
		[ResponsavelEmail] varchar(100) NULL,
		[ValorPeriodo] int NULL,
		[GrandezaPeriodo] int NULL,
		CONSTRAINT PK_SuitabilityParametrosWorkflow primary key(IdParametrosWorkflow)
	)
	
end

IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'SuitabilityParametrosWorkflowHistorico') = 0
begin
	CREATE TABLE dbo.SuitabilityParametrosWorkflowHistorico
	(
		[DataHistorico] [datetime] NOT NULL,
		[IdParametrosWorkflow] int NOT NULL default 1,
		[AtivaWorkFlow] char(1) NOT NULL default 'N',
		[VerificaEnquadramento] int NULL,
		[ResponsavelNome] varchar(50) NULL,
		[ResponsavelEmail] varchar(100) NULL,
		[ValorPeriodo] int NULL,
		[GrandezaPeriodo] int NULL,
		[Tipo] [varchar](10) NOT NULL,
		CONSTRAINT PK_SuitabilityParametrosWorkflowHistorico primary key(DataHistorico, IdParametrosWorkflow)
	)
	
end

if not exists(select count(1) from syscolumns where id = object_id('ControladoriaAtivo'))
begin
alter table carteira
add ControladoriaAtivo char(1)
end
go

if not exists(select count(1) from syscolumns where id = object_id('ControladoriaPassivo'))
begin
alter table carteira
add	ControladoriaPassivo char(1)
end
go
if not exists(select count(1) from syscolumns where id = object_id('MesmoConglomerado'))
begin
alter table carteira
add MesmoConglomerado char(1)
end
go

if not exists(select count(1) from syscolumns where id = object_id('CategoriaAnbima'))
begin
alter table carteira
add CategoriaAnbima int null
end
go

if not exists(select count(1) from syscolumns where id = object_id('Contratante'))
begin
alter table carteira
add Contratante int references AgenteMercado(IdAgente)
end
go


--Adicionar atributo
if not exists(select 1 from syscolumns where id = object_id('SuitabilityQuestaoHistorico') and name = 'Validacao')
BEGIN
	ALTER TABLE SuitabilityQuestaoHistorico ADD Validacao varchar(100) null
END
GO

IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'SuitabilityOpcaoHistorico') > 0
begin
	DROP TABLE SuitabilityOpcaoHistorico;

	CREATE TABLE dbo.SuitabilityOpcaoHistorico
	(
		[DataHistorico] [datetime] NOT NULL,
		[IdQuestao] [int] NOT NULL,
		[Questao] [varchar](2000) NULL,
		[IdOpcao] [int] NOT NULL,
		[Opcao] [varchar](2000) NULL,
		[Pontos] [int] NOT NULL,
		[Tipo] [varchar](10) NOT NULL,
		CONSTRAINT PK_SuitabilityOpcaoHistorico primary key(DataHistorico, IdQuestao, IdOpcao)
	)
end
GO

IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'SuitabilityResposta') > 0
begin
	DROP TABLE SuitabilityResposta;
end
go

if exists(select 1 from sysconstraints where object_name(id) = 'SuitabilityRespostaCotistaDetalhe' and object_name(constid) = 'SuitabilityRespostaCotistaDetalhe_SuitabilityRespostaCotista_FK')
begin
	ALTER TABLE SuitabilityRespostaCotistaDetalhe DROP CONSTRAINT SuitabilityRespostaCotistaDetalhe_SuitabilityRespostaCotista_FK
end
go

IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'SuitabilityParametroPerfilHistorico') > 0
begin

	drop table SuitabilityParametroPerfilHistorico;

	CREATE TABLE dbo.SuitabilityParametroPerfilHistorico
	(
		[DataHistorico] [datetime] NOT NULL,
		IdParametroPerfil int NOT NULL,
		IdValidacao int NOT NULL,
		Validacao varchar(100) NULL,
		IdPerfilInvestidor int NOT NULL,
		PerfilInvestidor varchar(20) NOT NULL,
		Definicao text NULL,
		FaixaPontuacao int NULL,
		[Tipo] [varchar](10) NOT NULL,
		CONSTRAINT PK_SuitabilityParametroPerfilHistorico primary key(DataHistorico, IdParametroPerfil)
	)
end
GO

IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'CarteiraSuitabilityHistorico') > 0
begin

	drop TABLE CarteiraSuitabilityHistorico

	CREATE TABLE dbo.CarteiraSuitabilityHistorico
	(
		[DataHistorico] [datetime] NOT NULL,
		[IdCarteira] int NOT NULL,
		[IdPerfilRisco] int NULL,
		[PerfilRisco] varchar(100) NULL,
		[Tipo] [varchar](10) NOT NULL,
		CONSTRAINT PK_CarteiraSuitabilityHistorico primary key(DataHistorico, IdCarteira)
	)	
end
GO

IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'SuitabilityApuracaoRiscoHistorico') > 0
begin

	drop TABLE SuitabilityApuracaoRiscoHistorico

	CREATE TABLE dbo.SuitabilityApuracaoRiscoHistorico
	(
		[DataHistorico] [datetime] NOT NULL,
		[IdApuracaoRisco] int NOT NULL,
		[IdValidacao] int NOT NULL,
		[Validacao] varchar(100) NULL,
		[IdPerfilCarteira] int NOT NULL,
		[PerfilCarteira] varchar(100) NULL,
		[PerfilRiscoFundo] varchar(100) NOT NULL,
		[Criterio] varchar(50) NOT NULL,
		[Percentual] int NOT NULL default 0,
		[Tipo] [varchar](10) NOT NULL,
		CONSTRAINT PK_SuitabilityApuracaoRiscoHistorico primary key(DataHistorico, IdApuracaoRisco)
	)
end
GO

IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'SuitabilityParametrosWorkflowHistorico') > 0
begin

	drop TABLE SuitabilityParametrosWorkflowHistorico

	CREATE TABLE dbo.SuitabilityParametrosWorkflowHistorico
	(
		[DataHistorico] [datetime] NOT NULL,
		[IdParametrosWorkflow] int NOT NULL default 1,
		[AtivaWorkFlow] char(1) NOT NULL default 'N',
		[VerificaEnquadramento] varchar(20) NULL,
		[ResponsavelNome] varchar(50) NULL,
		[ResponsavelEmail] varchar(100) NULL,
		[ValorPeriodo] int NULL,
		[GrandezaPeriodo] varchar(20) NULL,
		[Tipo] [varchar](10) NOT NULL,
		CONSTRAINT PK_SuitabilityParametrosWorkflowHistorico primary key(DataHistorico, IdParametrosWorkflow)
	)
	
end
GO

IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'SuitabilityEventosHistorico') > 0
begin

	drop TABLE SuitabilityEventosHistorico;

	CREATE TABLE dbo.SuitabilityEventosHistorico(
		[DataHistorico] [datetime] NOT NULL,
		[IdSuitabilityEventos] [int] NOT NULL,
		[Descricao] [varchar](100) NOT NULL,
		[IdMensagem] [int] NULL,
		[Mesagem] text NULL,
		[Tipo] [varchar](10) NOT NULL,
	 CONSTRAINT [PK_SuitabilityEventosHistorico] PRIMARY KEY (DataHistorico, IdSuitabilityEventos))
end
go

IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'PessoaSuitabilityHistorico') > 0
begin

	drop table PessoaSuitabilityHistorico

	CREATE TABLE dbo.PessoaSuitabilityHistorico
	(
		[DataHistorico] [datetime] NOT NULL,
		[IdPessoa] int NOT NULL,
		[IdValidacao] int NULL,
		[Validacao] varchar(100) NULL,
		[IdPerfil] int NULL,
		[Perfil] varchar(100) NULL,
		[Recusa] char(1) NOT NULL default 'N',
		[Dispensado] char(1) NOT NULL default 'N',
		[IdDispensa] int NULL,
		[Dispensa] varchar(100) NULL,
		[UltimaAlteracao] datetime NULL,
		[Tipo] [varchar](10) NOT NULL,
		CONSTRAINT PK_PessoaSuitabilityHistorico primary key(DataHistorico, IdPessoa)
	)	
end
GO


IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'TabelaPerfilMensalCVM') > 0
begin
if not exists(select * from sys.all_columns where name = 'Secao18_CnpjComitente1' and object_id = object_id('TabelaPerfilMensalCVM'))
	alter table TabelaPerfilMensalCVM add Secao18_CnpjComitente1 varchar(50) null
if not exists(select * from sys.all_columns where name = 'Secao18_CnpjComitente1' and object_id = object_id('TabelaPerfilMensalCVM'))
	alter table TabelaPerfilMensalCVM add Secao18_CnpjComitente2 varchar(50) null
if not exists(select * from sys.all_columns where name = 'Secao18_CnpjComitente1' and object_id = object_id('TabelaPerfilMensalCVM'))
	alter table TabelaPerfilMensalCVM add Secao18_CnpjComitente3 varchar(50) null
if not exists(select * from sys.all_columns where name = 'Secao18_CnpjComitente1' and object_id = object_id('TabelaPerfilMensalCVM'))
	alter table TabelaPerfilMensalCVM add Secao18_ParteRelacionada1 char(1) null
if not exists(select * from sys.all_columns where name = 'Secao18_CnpjComitente1' and object_id = object_id('TabelaPerfilMensalCVM'))
	alter table TabelaPerfilMensalCVM add Secao18_ParteRelacionada2 char(1) null
if not exists(select * from sys.all_columns where name = 'Secao18_CnpjComitente1' and object_id = object_id('TabelaPerfilMensalCVM'))
	alter table TabelaPerfilMensalCVM add Secao18_ParteRelacionada3 char(1) null
if not exists(select * from sys.all_columns where name = 'Secao18_CnpjComitente1' and object_id = object_id('TabelaPerfilMensalCVM'))
	alter table TabelaPerfilMensalCVM add Secao18_ValorTotal1 decimal (16, 2) null
if not exists(select * from sys.all_columns where name = 'Secao18_CnpjComitente1' and object_id = object_id('TabelaPerfilMensalCVM'))
	alter table TabelaPerfilMensalCVM add Secao18_ValorTotal2 decimal (16, 2) null
if not exists(select * from sys.all_columns where name = 'Secao18_CnpjComitente1' and object_id = object_id('TabelaPerfilMensalCVM'))
	alter table TabelaPerfilMensalCVM add Secao18_ValorTotal3 decimal (16, 2) null
if not exists(select * from sys.all_columns where name = 'Secao18_CnpjComitente1' and object_id = object_id('TabelaPerfilMensalCVM'))
	alter table TabelaPerfilMensalCVM add Secao18_VedadaCobrancaTaxa char(1) null
if not exists(select * from sys.all_columns where name = 'Secao18_CnpjComitente1' and object_id = object_id('TabelaPerfilMensalCVM'))
	alter table TabelaPerfilMensalCVM add Secao18_DataUltimaCobranca datetime NULL
if not exists(select * from sys.all_columns where name = 'Secao18_CnpjComitente1' and object_id = object_id('TabelaPerfilMensalCVM'))
	alter table TabelaPerfilMensalCVM add Secao18_ValorUltimaCota decimal (16, 2) null
end




IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'OperacaoCotistaAux') = 0
Begin
	CREATE TABLE [dbo].[OperacaoCotistaAux]
	(
		[IdOperacaoCotistaAux] [int] IDENTITY(1,1) PRIMARY KEY NOT NULL,
		[IdOperacao] [int] NOT NULL,
		[IdCotista] [int] NOT NULL,
		[IdCarteira] [int] NOT NULL,
		[DataOperacao] [datetime] NOT NULL,
		[DataConversao] [datetime] NOT NULL,
		[DataLiquidacao] [datetime] NOT NULL,
		[DataAgendamento] [datetime] NOT NULL,
		[TipoOperacao] [tinyint] NOT NULL,
		[TipoResgate] [tinyint] NULL,
		[IdPosicaoResgatada] [int] NULL,
		[IdFormaLiquidacao] [tinyint] NOT NULL,
		[Quantidade] [decimal](28, 12) NOT NULL,
		[CotaOperacao] [decimal](28, 12) NOT NULL,
		[ValorBruto] [decimal](16, 2) NOT NULL,
		[ValorLiquido] [decimal](16, 2) NOT NULL,
		[ValorIR] [decimal](16, 2) NOT NULL,
		[ValorIOF] [decimal](16, 2) NOT NULL,
		[ValorCPMF] [decimal](16, 2) NOT NULL,
		[ValorPerformance] [decimal](16, 2) NOT NULL,
		[PrejuizoUsado] [decimal](16, 2) NOT NULL,
		[RendimentoResgate] [decimal](16, 2) NOT NULL,
		[VariacaoResgate] [decimal](16, 2) NOT NULL,
		[Observacao] [varchar](400) NOT NULL,
		[DadosBancarios] [varchar](500) NULL,
		[Fonte] [tinyint] NOT NULL,
		[IdConta] [int] NULL,
		[CotaInformada] [decimal](28, 12) NULL,
		[IdAgenda] [int] NULL,
		[IdOperacaoResgatada] [int] NULL,
		[IdOperacaoAuxiliar] [int] NULL,
		[IdOrdem] [int] NULL
	)
END	
GO

IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'DetalheResgateCotistaAux') = 0
Begin
	CREATE TABLE [dbo].[DetalheResgateCotistaAux]
	(
		[IdOperacao] [int] NOT NULL,
		[IdPosicaoResgatada] [int] NOT NULL,
		[IdCotista] [int] NOT NULL,
		[IdCarteira] [int] NOT NULL,
		[Quantidade] [decimal](28, 12) NOT NULL,
		[ValorBruto] [decimal](16, 2) NOT NULL,
		[ValorLiquido] [decimal](16, 2) NOT NULL,
		[ValorCPMF] [decimal](16, 2) NOT NULL,
		[ValorPerformance] [decimal](16, 2) NOT NULL,
		[PrejuizoUsado] [decimal](16, 2) NOT NULL,
		[RendimentoResgate] [decimal](16, 2) NOT NULL,
		[VariacaoResgate] [decimal](16, 2) NOT NULL,
		[ValorIR] [decimal](16, 2) NOT NULL,
		[ValorIOF] [decimal](16, 2) NOT NULL,
		[DataOperacao] [datetime] NOT NULL,
		[TipoOperacao] [TinyInt] NOT NULL,
	 CONSTRAINT [PK_DetalheResgateCotistaAux] PRIMARY KEY CLUSTERED 
	(
		[IdOperacao] ASC,
		[IdPosicaoResgatada] ASC
	)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
	) ON [PRIMARY]
END
GO

IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'PosicaoCotistaAux') = 0
Begin
	CREATE TABLE [dbo].[PosicaoCotistaAux]
	(
		[IdPosicaoCotistaAux] [int] IDENTITY(1,1) PRIMARY KEY NOT NULL,
		[DataReferencia] [datetime] NOT NULL,
		[IdPosicao] [int] NOT NULL,
		[IdOperacao] [int] NULL,
		[IdCotista] [int] NOT NULL,
		[IdCarteira] [int] NOT NULL,
		[ValorAplicacao] [decimal](16, 2) NOT NULL,
		[DataAplicacao] [datetime] NOT NULL,
		[DataConversao] [datetime] NOT NULL,
		[CotaAplicacao] [decimal](28, 12) NOT NULL,
		[CotaDia] [decimal](28, 12) NOT NULL,
		[ValorBruto] [decimal](16, 2) NOT NULL,
		[ValorLiquido] [decimal](16, 2) NOT NULL,
		[QuantidadeInicial] [decimal](28, 12) NOT NULL,
		[Quantidade] [decimal](28, 12) NOT NULL,
		[QuantidadeBloqueada] [decimal](28, 12) NOT NULL,
		[DataUltimaCobrancaIR] [datetime] NOT NULL,
		[ValorIR] [decimal](16, 2) NOT NULL,
		[ValorIOF] [decimal](16, 2) NOT NULL,
		[ValorPerformance] [decimal](16, 2) NOT NULL,
		[ValorIOFVirtual] [decimal](16, 2) NOT NULL,
		[QuantidadeAntesCortes] [decimal](28, 12) NOT NULL,
		[ValorRendimento] [decimal](16, 2) NOT NULL,
		[DataUltimoCortePfee] [datetime] NULL,
		[PosicaoIncorporada] [char](1) NOT NULL
	)
END	
GO

IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'OperacaoFundoAux') = 0
Begin
	CREATE TABLE [dbo].[OperacaoFundoAux](
		[IdOperacaoFundoAux] [int] IDENTITY(1,1) PRIMARY KEY NOT NULL,
		[IdOperacao] [int] NOT NULL,
		[IdCliente] [int] NOT NULL,
		[IdCarteira] [int] NOT NULL,
		[DataOperacao] [datetime] NOT NULL,
		[DataConversao] [datetime] NOT NULL,
		[DataLiquidacao] [datetime] NOT NULL,
		[DataAgendamento] [datetime] NOT NULL,
		[TipoOperacao] [tinyint] NOT NULL,
		[TipoResgate] [tinyint] NULL,
		[IdPosicaoResgatada] [int] NULL,
		[IdFormaLiquidacao] [tinyint] NOT NULL,
		[Quantidade] [decimal](28, 12) NOT NULL,
		[CotaOperacao] [decimal](28, 12) NOT NULL,
		[ValorBruto] [decimal](16, 2) NOT NULL,
		[ValorLiquido] [decimal](16, 2) NOT NULL,
		[ValorIR] [decimal](16, 2) NOT NULL,
		[ValorIOF] [decimal](16, 2) NOT NULL,
		[ValorCPMF] [decimal](16, 2) NOT NULL,
		[ValorPerformance] [decimal](16, 2) NOT NULL,
		[PrejuizoUsado] [decimal](16, 2) NOT NULL,
		[RendimentoResgate] [decimal](16, 2) NOT NULL,
		[VariacaoResgate] [decimal](16, 2) NOT NULL,
		[IdConta] [int] NULL,
		[Observacao] [varchar](400) NOT NULL,
		[Fonte] [tinyint] NOT NULL,
		[IdAgenda] [int] NULL,
		[CotaInformada] [decimal](28, 12) NULL,
		[IdOperacaoResgatada] [int] NULL
	)
END	
GO

IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'PosicaoFundoAux') = 0
Begin	
	CREATE TABLE [dbo].[PosicaoFundoAux]
	(
		[IdPosicaoFundoAux] [int] IDENTITY(1,1) PRIMARY KEY NOT NULL,
		[DataReferencia] [datetime] NOT NULL,
		[IdPosicao] [int] NOT NULL,
		[IdOperacao] [int] NULL,
		[IdCliente] [int] NOT NULL,
		[IdCarteira] [int] NOT NULL,
		[ValorAplicacao] [decimal](16, 2) NOT NULL,
		[DataAplicacao] [datetime] NOT NULL,
		[DataConversao] [datetime] NOT NULL,
		[CotaAplicacao] [decimal](28, 12) NOT NULL,
		[CotaDia] [decimal](28, 12) NOT NULL,
		[ValorBruto] [decimal](16, 2) NOT NULL,
		[ValorLiquido] [decimal](16, 2) NOT NULL,
		[QuantidadeInicial] [decimal](28, 12) NOT NULL,
		[Quantidade] [decimal](28, 12) NOT NULL,
		[QuantidadeBloqueada] [decimal](28, 12) NULL,
		[DataUltimaCobrancaIR] [datetime] NOT NULL,
		[ValorIR] [decimal](16, 2) NOT NULL,
		[ValorIOF] [decimal](16, 2) NOT NULL,
		[ValorPerformance] [decimal](16, 2) NOT NULL,
		[ValorIOFVirtual] [decimal](16, 2) NOT NULL,
		[QuantidadeAntesCortes] [decimal](28, 12) NOT NULL,
		[ValorRendimento] [decimal](16, 2) NOT NULL,
		[DataUltimoCortePfee] [datetime] NULL,
		[PosicaoIncorporada] [char](1) NOT NULL
	)
END	
GO

IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'DetalheResgateFundoAux') = 0
Begin
CREATE TABLE [dbo].[DetalheResgateFundoAux]
(
	[IdOperacao] [int] NOT NULL,
	[IdPosicaoResgatada] [int] NOT NULL,
	[IdCliente] [int] NOT NULL,
	[IdCarteira] [int] NOT NULL,
	[Quantidade] [decimal](28, 12) NOT NULL,
	[ValorBruto] [decimal](16, 2) NOT NULL,
	[ValorLiquido] [decimal](16, 2) NOT NULL,
	[ValorCPMF] [decimal](16, 2) NOT NULL,
	[ValorPerformance] [decimal](16, 2) NOT NULL,
	[PrejuizoUsado] [decimal](16, 2) NOT NULL,
	[RendimentoResgate] [decimal](16, 2) NOT NULL,
	[VariacaoResgate] [decimal](16, 2) NOT NULL,
	[ValorIR] [decimal](16, 2) NOT NULL,
	[ValorIOF] [decimal](16, 2) NOT NULL,
	[DataOperacao] [datetime] NOT NULL,
	[TipoOperacao] [TinyInt] NOT NULL,
	CONSTRAINT [PK_DetalheResgateFundoAux] PRIMARY KEY CLUSTERED 
	(
		[IdOperacao] ASC,
		[IdPosicaoResgatada] ASC
	)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
	) ON [PRIMARY]
END
GO

if not exists(select 1 from syscolumns where id = object_id('OperacaoFundo') and name = 'RegistroEditado')
BEGIN
	ALTER TABLE dbo.OperacaoFundo ADD RegistroEditado char(1) NOT NULL DEFAULT 'N'
END
GO
	
if not exists(select 1 from syscolumns where id = object_id('OperacaoFundo') and name = 'ValoresColados')
BEGIN
	ALTER TABLE dbo.OperacaoFundo ADD ValoresColados char(1) NOT NULL DEFAULT 'N' 
END
GO
	
if not exists(select 1 from syscolumns where id = object_id('OperacaoFundoAux') and name = 'RegistroEditado')
BEGIN
	ALTER TABLE dbo.OperacaoFundoAux ADD RegistroEditado char(1) NOT NULL DEFAULT 'N'
END
GO
	
if not exists(select 1 from syscolumns where id = object_id('OperacaoFundoAux') and name = 'ValoresColados')
BEGIN
	ALTER TABLE dbo.OperacaoFundoAux ADD ValoresColados char(1) NOT NULL DEFAULT 'N' 
END
GO	

if not exists(select 1 from syscolumns where id = object_id('OperacaoCotista') and name = 'RegistroEditado')
BEGIN
	ALTER TABLE dbo.OperacaoCotista ADD RegistroEditado char(1) NOT NULL DEFAULT 'N'
END	

if not exists(select 1 from syscolumns where id = object_id('OperacaoCotista') and name = 'ValoresColados')
BEGIN
	ALTER TABLE dbo.OperacaoCotista ADD ValoresColados char(1) NOT NULL DEFAULT 'N'
END	

if not exists(select 1 from syscolumns where id = object_id('OperacaoCotistaAux') and name = 'RegistroEditado')
BEGIN
	ALTER TABLE dbo.OperacaoCotistaAux ADD RegistroEditado char(1) NOT NULL DEFAULT 'N'
END	

if not exists(select 1 from syscolumns where id = object_id('OperacaoCotistaAux') and name = 'ValoresColados')
BEGIN
	ALTER TABLE dbo.OperacaoCotistaAux ADD ValoresColados char(1) NOT NULL DEFAULT 'N'
END	

IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'ColagemComeCotas') = 0
Begin
	CREATE TABLE [dbo].[ColagemComeCotas]
	(
		IdColagemComeCotas [int] PRIMARY KEY NOT NULL IDENTITY,
		TipoRegistro char(1) NOT NULL ,
		CodigoExterno char(15) NOT NULL ,
		DataReferencia [datetime] NOT NULL,
		DataOperacao [datetime] NOT NULL,
		IdCliente  [int] NOT NULL,
		IdCarteira  [int] NOT NULL,
		DataConversao [datetime] NULL,
		Quantidade [decimal](28, 12) NOT NULL,
		CotaOperacao [decimal](28, 12) NOT NULL,
		ValorBruto [decimal](28, 12) NOT NULL,
		ValorLiquido [decimal](28, 12) NOT NULL,
		ValorCPMF [decimal](28, 12) NOT NULL,
		ValorPerformance [decimal](28, 12) NOT NULL,
		PrejuizoUsado [decimal](28, 12) NOT NULL,
		RendimentoComeCotas [decimal](28, 12) NOT NULL,
		VariacaoComeCotas [decimal](28, 12) NOT NULL,
		ValorIR [decimal](28, 12) NOT NULL,
		ValorIOF [decimal](28, 12) NOT NULL
	)	
END	

IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'ColagemComeCotasDetalhe') = 0
Begin
	CREATE TABLE [dbo].[ColagemComeCotasDetalhe]
	(
		IdColagemComeCotasDetalhe  [int] PRIMARY KEY NOT NULL IDENTITY,
		CodigoExterno char(15) NOT NULL ,
		DataReferencia [datetime] NOT NULL,
		DataOperacao [datetime] NOT NULL,
		IdCliente  [int] NOT NULL,
		IdCarteira  [int] NOT NULL,
		DataConversao [datetime] NULL,
		Quantidade [decimal](28, 12) NOT NULL,
		CotaOperacao [decimal](28, 12) NOT NULL,
		ValorBruto [decimal](28, 12) NOT NULL,
		ValorLiquido [decimal](28, 12) NOT NULL,
		ValorCPMF [decimal](28, 12) NOT NULL,
		ValorPerformance [decimal](28, 12) NOT NULL,
		PrejuizoUsado [decimal](28, 12) NOT NULL,
		RendimentoComeCotas [decimal](28, 12) NOT NULL,
		VariacaoComeCotas [decimal](28, 12) NOT NULL,
		ValorIR [decimal](28, 12) NOT NULL,
		ValorIOF [decimal](28, 12) NOT NULL,
	)	
END	

IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'ColagemComeCotasDetalhePosicao') = 0
Begin
	CREATE TABLE [dbo].[ColagemComeCotasDetalhePosicao]
	(
		IdColagemComeCotasDetalhePosicao [int] PRIMARY KEY NOT NULL IDENTITY,
		IdColagemComeCotasDetalhe [int] NOT NULL,
		DataReferencia [datetime] NOT NULL,
		QuantidadeInicial [decimal](28, 12) NOT NULL,
		QuantidadeRestante [decimal](28, 12) NOT NULL
	)		
END	

IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'ColagemComeCotasMapaFundo') = 0
Begin
	CREATE TABLE [dbo].[ColagemComeCotasMapaFundo]
	(
		IdColagemComeCotasMapaFundo  [int] PRIMARY KEY NOT NULL IDENTITY,
		DataReferencia [datetime] NOT NULL,
		IdCliente  [int] NOT NULL,
		IdCarteira  [int] NOT NULL,
		DataConversao [datetime] NULL,
		DetalheImportado char(1) NOT NULL DEFAULT 'N',
		IdOperacaoComeCotasVinculada  [int] NOT NULL,	
		Participacao [decimal](28, 12) NOT NULL
	)		
END	

IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'ColagemComeCotasPosFundo') = 0
Begin
	CREATE TABLE [dbo].[ColagemComeCotasPosFundo]
	(
		IdColagemComeCotasPosFundo  [int] PRIMARY KEY NOT NULL IDENTITY,
		IdCarteira  				[int] NOT NULL,
		IdCliente  					[int] NOT NULL,
		DataConversao 				[datetime] NULL,
		DataReferencia 				[datetime] NOT NULL,
		IdPosicaoVinculada  		[int] NOT NULL,
		PrejuizoUsado 				[decimal](28, 12) NOT NULL,
		RendimentoComeCotas 		[decimal](28, 12) NOT NULL,
		ValorBruto 					[decimal](28, 12) NOT NULL,
		ValorCPMF 					[decimal](28, 12) NOT NULL,
		ValorIOF 					[decimal](28, 12) NOT NULL,
		ValorIR 					[decimal](28, 12) NOT NULL,
		ValorLiquido 				[decimal](28, 12) NOT NULL,
		ValorPerformance 			[decimal](28, 12) NOT NULL,
		VariacaoComeCotas 			[decimal](28, 12) NOT NULL,
		Quantidade					[decimal](28, 12) NOT NULL,
		Participacao				[decimal](28, 12) NOT NULL,
		DetalheImportado 			[char](1) NOT NULL DEFAULT 'N'
	)		
END	


IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'ColagemResgateMapaFundo') = 0
Begin
	CREATE TABLE [dbo].[ColagemResgateMapaFundo]
	(
		IdColagemResgateMapaFundo  [int] PRIMARY KEY NOT NULL IDENTITY,
		DataReferencia [datetime] NOT NULL,
		IdCliente  [int] NOT NULL,
		IdCarteira  [int] NOT NULL,
		DataConversao [datetime] NULL,
		DetalheImportado char(1) NOT NULL DEFAULT 'N',
		IdOperacaoResgateVinculada  [int] NOT NULL,	
		Participacao [decimal](28, 12) NOT NULL
	)		
END	

IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'ColagemOperResgPosFundo') = 0
Begin
	CREATE TABLE [dbo].[ColagemOperResgPosFundo]
	(
		IdColagemOperResgPosFundo  [int] PRIMARY KEY NOT NULL IDENTITY,
		IdCarteira  [int] NOT NULL,
		IdCliente  [int] NOT NULL,
		DataReferencia [datetime] NOT NULL,
		IdColagemResgateDetalhe  [int] NOT NULL,
		IdOperacaoResgateVinculada  [int] NOT NULL,
		IdPosicaoVinculada  [int] NOT NULL,
		QuantidadeVinculada [decimal](28, 12) NOT NULL
	)		
END	


IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'ColagemResgate') = 0
Begin
	CREATE TABLE [dbo].[ColagemResgate]
	(
		IdColagemResgate [int] PRIMARY KEY NOT NULL IDENTITY,
		TipoRegistro char(1) NOT NULL ,
		CodigoExterno char(15) NOT NULL ,
		DataReferencia [datetime] NOT NULL,
		DataOperacao [datetime] NOT NULL,
		IdCliente  [int] NOT NULL,
		IdCarteira  [int] NOT NULL,
		DataConversao [datetime] NULL,
		Quantidade [decimal](28, 12) NOT NULL,
		CotaOperacao [decimal](28, 12) NOT NULL,
		ValorBruto [decimal](28, 12) NOT NULL,
		ValorLiquido [decimal](28, 12) NOT NULL,
		ValorCPMF [decimal](28, 12) NOT NULL,
		ValorPerformance [decimal](28, 12) NOT NULL,
		PrejuizoUsado [decimal](28, 12) NOT NULL,
		RendimentoResgate [decimal](28, 12) NOT NULL,
		VariacaoResgate [decimal](28, 12) NOT NULL,
		ValorIR [decimal](28, 12) NOT NULL,
		ValorIOF [decimal](28, 12) NOT NULL
	)	
END	


IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'ColagemResgateDetalhe') = 0
Begin
	CREATE TABLE [dbo].[ColagemResgateDetalhe]
	(
		IdColagemResgateDetalhe  [int] PRIMARY KEY NOT NULL IDENTITY,
		CodigoExterno char(15) NOT NULL ,
		DataReferencia [datetime] NOT NULL,
		DataOperacao [datetime] NOT NULL,
		IdCliente  [int] NOT NULL,
		IdCarteira  [int] NOT NULL,
		DataConversao [datetime] NULL,
		Quantidade [decimal](28, 12) NOT NULL,
		CotaOperacao [decimal](28, 12) NOT NULL,
		ValorBruto [decimal](28, 12) NOT NULL,
		ValorLiquido [decimal](28, 12) NOT NULL,
		ValorCPMF [decimal](28, 12) NOT NULL,
		ValorPerformance [decimal](28, 12) NOT NULL,
		PrejuizoUsado [decimal](28, 12) NOT NULL,
		RendimentoResgate [decimal](28, 12) NOT NULL,
		VariacaoResgate [decimal](28, 12) NOT NULL,
		ValorIR [decimal](28, 12) NOT NULL,
		ValorIOF [decimal](28, 12) NOT NULL,
	)	
END	

 IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'ColagemResgateDetalhePosicao') = 0
Begin
	CREATE TABLE [dbo].[ColagemResgateDetalhePosicao]
	(
		IdColagemResgateDetalhePosicao [int] PRIMARY KEY NOT NULL IDENTITY,
		IdColagemResgateDetalhe [int] NOT NULL,
		DataReferencia [datetime] NOT NULL,
		QuantidadeInicial [decimal](28, 12) NOT NULL,
		QuantidadeRestante [decimal](28, 12) NOT NULL
	)		
END	

IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'ColagemResgateMapaCotista') = 0
Begin
	CREATE TABLE [dbo].[ColagemResgateMapaCotista]
	(
		IdColagemResgateMapaCotista  [int] PRIMARY KEY NOT NULL IDENTITY,
		DataReferencia [datetime] NOT NULL,
		IdCliente  [int] NOT NULL,
		IdCarteira  [int] NOT NULL,
		DataConversao [datetime] NULL,
		DetalheImportado char(1) NOT NULL DEFAULT 'N',
		IdOperacaoResgateVinculada  [int] NOT NULL,	
		Participacao [decimal](28, 12) NOT NULL
	)		
END	

IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'ColagemOperResgPosCotista') = 0
Begin
	CREATE TABLE [dbo].[ColagemOperResgPosCotista]
	(
		IdColagemOperResgPosCotista  [int] PRIMARY KEY NOT NULL IDENTITY,
		IdCarteira  [int] NOT NULL,
		IdCliente  [int] NOT NULL,
		DataReferencia [datetime] NOT NULL,
		IdColagemResgateDetalhe  [int] NOT NULL,
		IdOperacaoResgateVinculada  [int] NOT NULL,
		IdPosicaoVinculada  [int] NOT NULL,
		QuantidadeVinculada [decimal](28, 12) NOT NULL
	)		
END	

if exists(select 1 from syscolumns where id = object_id('ColagemResgateMapaFundo') and name = 'Participacao')
BEGIN	
	ALTER TABLE [dbo].[ColagemResgateMapaFundo] DROP COLUMN Participacao;
END

if NOT exists(select 1 from syscolumns where id = object_id('ColagemResgateMapaFundo') and name = 'IdColagemResgateDetalhe')
BEGIN	
	ALTER TABLE [dbo].[ColagemResgateMapaFundo] ADD IdColagemResgateDetalhe int NULL;
END

if exists(select 1 from syscolumns where id = object_id('ColagemResgate') and name = 'DataOperacao')
BEGIN	
	ALTER TABLE [dbo].[ColagemResgate] DROP COLUMN DataOperacao;
END

if exists(select 1 from syscolumns where id = object_id('ColagemResgateDetalhe') and name = 'DataOperacao')
BEGIN	
	ALTER TABLE [dbo].[ColagemResgateDetalhe] DROP COLUMN DataOperacao;
END

if exists(select 1 from syscolumns where id = object_id('ColagemComeCotas') and name = 'DataOperacao')
BEGIN	
	ALTER TABLE [dbo].[ColagemComeCotas] DROP COLUMN DataOperacao;
END

if exists(select 1 from syscolumns where id = object_id('ColagemComeCotasDetalhe') and name = 'DataOperacao')
BEGIN	
	ALTER TABLE [dbo].[ColagemComeCotasDetalhe] DROP COLUMN DataOperacao;
END


if exists(select 1 from syscolumns where id = object_id('ColagemResgateMapaCotista') and name = 'Participacao')
BEGIN	
	ALTER TABLE [dbo].[ColagemResgateMapaCotista] DROP COLUMN Participacao;
END

if NOT exists(select 1 from syscolumns where id = object_id('ColagemResgateMapaCotista') and name = 'IdColagemResgateDetalhe')
BEGIN	
	ALTER TABLE [dbo].[ColagemResgateMapaCotista] ADD IdColagemResgateDetalhe int NULL;
END

if not exists(select 1 from syscolumns where id = object_id('ColagemResgate') and name = 'Processado')
BEGIN	
	ALTER TABLE [dbo].[ColagemResgate] ADD Processado char(1) NOT NULL DEFAULT 'N';
END

if not exists(select 1 from syscolumns where id = object_id('ColagemComeCotas') and name = 'Processado')
BEGIN	
	ALTER TABLE [dbo].[ColagemComeCotas] ADD Processado char(1) NOT NULL DEFAULT 'N';
END

IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'ColagemComeCotasPosCotista') = 0
Begin
	CREATE TABLE [dbo].[ColagemComeCotasPosCotista]
	(
		IdColagemComeCotasPosCotista  [int] PRIMARY KEY NOT NULL IDENTITY,
		IdCarteira  				[int] NOT NULL,
		IdCliente  					[int] NOT NULL,
		DataConversao 				[datetime] NULL,
		DataReferencia 				[datetime] NOT NULL,
		IdPosicaoVinculada  		[int] NOT NULL,
		PrejuizoUsado 				[decimal](28, 12) NOT NULL,
		RendimentoComeCotas 		[decimal](28, 12) NOT NULL,
		ValorBruto 					[decimal](28, 12) NOT NULL,
		ValorCPMF 					[decimal](28, 12) NOT NULL,
		ValorIOF 					[decimal](28, 12) NOT NULL,
		ValorIR 					[decimal](28, 12) NOT NULL,
		ValorLiquido 				[decimal](28, 12) NOT NULL,
		ValorPerformance 			[decimal](28, 12) NOT NULL,
		VariacaoComeCotas 			[decimal](28, 12) NOT NULL,
		Quantidade					[decimal](28, 12) NOT NULL,
		Participacao				[decimal](28, 12) NOT NULL,
		DetalheImportado 			[char](1) NOT NULL DEFAULT 'N'
	)		
END	


UPDATE InformacoesComplementaresFundo SET Periodicidade = SUBSTRING(Periodicidade,1, 250);
ALTER TABLE InformacoesComplementaresFundo ALTER COLUMN Periodicidade varchar(250) null;

UPDATE InformacoesComplementaresFundo SET LocalFormaDivulgacao = SUBSTRING(LocalFormaDivulgacao,1, 300);
ALTER TABLE InformacoesComplementaresFundo ALTER COLUMN LocalFormaDivulgacao varchar(300) null;

UPDATE InformacoesComplementaresFundo SET LocalFormaSolicitacaoCotista = SUBSTRING(LocalFormaSolicitacaoCotista,1, 250);
ALTER TABLE InformacoesComplementaresFundo ALTER COLUMN LocalFormaSolicitacaoCotista varchar(250) null;


ALTER TABLE InformacoesComplementaresFundo ALTER COLUMN FatoresRisco varchar(2000) null;
ALTER TABLE InformacoesComplementaresFundo ALTER COLUMN PoliticaExercicioVoto varchar(1000) null;
ALTER TABLE InformacoesComplementaresFundo ALTER COLUMN TributacaoAplicavel varchar(2500) null;
ALTER TABLE InformacoesComplementaresFundo ALTER COLUMN PoliticaAdministracaoRisco varchar(2500) null;

UPDATE InformacoesComplementaresFundo SET AgenciaClassificacaoRisco = SUBSTRING(AgenciaClassificacaoRisco,1, 50);
ALTER TABLE InformacoesComplementaresFundo ALTER COLUMN AgenciaClassificacaoRisco varchar(50) null;

UPDATE InformacoesComplementaresFundo SET RecursosServicosGestor = SUBSTRING(RecursosServicosGestor,1, 100);
ALTER TABLE InformacoesComplementaresFundo ALTER COLUMN RecursosServicosGestor varchar(100) null;

UPDATE InformacoesComplementaresFundo SET PrestadoresServicos = SUBSTRING(PrestadoresServicos,1, 100);
ALTER TABLE InformacoesComplementaresFundo ALTER COLUMN PrestadoresServicos varchar(100) null;	

IF NOT EXISTS(SELECT * FROM SYSCOLUMNS WHERE NAME = 'Secao18_CnpjComitente1' AND ID = OBJECT_ID('TabelaPerfilMensalCVM'))
BEGIN
	alter table TabelaPerfilMensalCVM add Secao18_CnpjComitente1 varchar(50) null
END

IF NOT EXISTS(SELECT * FROM SYSCOLUMNS WHERE NAME = 'Secao18_CnpjComitente2' AND ID = OBJECT_ID('TabelaPerfilMensalCVM'))
BEGIN
	alter table TabelaPerfilMensalCVM add Secao18_CnpjComitente2 varchar(50) null
END

IF NOT EXISTS(SELECT * FROM SYSCOLUMNS WHERE NAME = 'Secao18_CnpjComitente3' AND ID = OBJECT_ID('TabelaPerfilMensalCVM'))
BEGIN
	alter table TabelaPerfilMensalCVM add Secao18_CnpjComitente3 varchar(50) null
END


IF NOT EXISTS(SELECT * FROM SYSCOLUMNS WHERE NAME = 'Secao18_ParteRelacionada1' AND ID = OBJECT_ID('TabelaPerfilMensalCVM'))
BEGIN
	alter table TabelaPerfilMensalCVM add Secao18_ParteRelacionada1 char(1) null
END 

IF NOT EXISTS(SELECT * FROM SYSCOLUMNS WHERE NAME = 'Secao18_ParteRelacionada2' AND ID = OBJECT_ID('TabelaPerfilMensalCVM'))
BEGIN
	alter table TabelaPerfilMensalCVM add Secao18_ParteRelacionada2 char(1) null
END

IF NOT EXISTS(SELECT * FROM SYSCOLUMNS WHERE NAME = 'Secao18_ParteRelacionada3' AND ID = OBJECT_ID('TabelaPerfilMensalCVM'))
BEGIN
	alter table TabelaPerfilMensalCVM add Secao18_ParteRelacionada3 char(1) null
END

IF NOT EXISTS(SELECT * FROM SYSCOLUMNS WHERE NAME = 'Secao18_ValorTotal1' AND ID = OBJECT_ID('TabelaPerfilMensalCVM'))
BEGIN
	alter table TabelaPerfilMensalCVM add Secao18_ValorTotal1 decimal (16, 2) null
END

IF NOT EXISTS(SELECT * FROM SYSCOLUMNS WHERE NAME = 'Secao18_ValorTotal2' AND ID = OBJECT_ID('TabelaPerfilMensalCVM'))
BEGIN
	alter table TabelaPerfilMensalCVM add Secao18_ValorTotal2 decimal (16, 2) null
END 

IF NOT EXISTS(SELECT * FROM SYSCOLUMNS WHERE NAME = 'Secao18_ValorTotal3' AND ID = OBJECT_ID('TabelaPerfilMensalCVM'))
BEGIN
	alter table TabelaPerfilMensalCVM add Secao18_ValorTotal3 decimal (16, 2) null
END 

IF NOT EXISTS(SELECT * FROM SYSCOLUMNS WHERE NAME = 'Secao18_VedadaCobrancaTaxa' AND ID = OBJECT_ID('TabelaPerfilMensalCVM'))
BEGIN
	alter table TabelaPerfilMensalCVM add Secao18_VedadaCobrancaTaxa char(1) null
END 

IF NOT EXISTS(SELECT * FROM SYSCOLUMNS WHERE NAME = 'Secao18_DataUltimaCobranca' AND ID = OBJECT_ID('TabelaPerfilMensalCVM'))
BEGIN
	alter table TabelaPerfilMensalCVM add Secao18_DataUltimaCobranca datetime NULL
END 

IF NOT EXISTS(SELECT * FROM SYSCOLUMNS WHERE NAME = 'Secao18_ValorUltimaCota' AND ID = OBJECT_ID('TabelaPerfilMensalCVM'))
BEGIN
	alter table TabelaPerfilMensalCVM add Secao18_ValorUltimaCota decimal (16, 2) null	
END 

-------------------------------------


IF NOT EXISTS(SELECT * FROM SYSCOLUMNS WHERE NAME = 'ProcAutomatico' AND ID = OBJECT_ID('Carteira'))
BEGIN
	ALTER TABLE Carteira
	ADD ProcAutomatico char(1) NOT NULL DEFAULT 'N';
END

IF NOT EXISTS(SELECT * FROM SYSCOLUMNS WHERE NAME = 'IdAgenteContraParte' AND ID = OBJECT_ID('OrdemRendaFixa'))
BEGIN
	ALTER TABLE OrdemRendaFixa
	ADD IdAgenteContraParte int NULL foreign key references AgenteMercado(IdAgente)
END

IF NOT EXISTS(SELECT * FROM SYSCOLUMNS WHERE NAME = 'IdCarteiraContraParte' AND ID = OBJECT_ID('OrdemRendaFixa'))
BEGIN
	ALTER TABLE OrdemRendaFixa
	ADD IdCarteiraContraParte int NULL foreign key references Carteira(IdCarteira)
END

IF NOT EXISTS(SELECT * FROM SYSCOLUMNS WHERE NAME = 'IdAgenteContraParte' AND ID = OBJECT_ID('OperacaoRendaFixa'))
BEGIN
	ALTER TABLE OperacaoRendaFixa
	ADD IdAgenteContraParte int NULL foreign key references AgenteMercado(IdAgente)
END

IF NOT EXISTS(SELECT * FROM SYSCOLUMNS WHERE NAME = 'IdOperacaoEspelho' AND ID = OBJECT_ID('OperacaoRendaFixa'))
BEGIN
	ALTER TABLE OperacaoRendaFixa	
	ADD IdOperacaoEspelho int NULL		
END

IF NOT EXISTS(SELECT * FROM SYSCOLUMNS WHERE NAME = 'DataModificacao' AND ID = OBJECT_ID('OperacaoRendaFixa'))
BEGIN
	ALTER TABLE OperacaoRendaFixa		
	ADD DataModificacao datetime not null default GetDAte()
END


	
IF NOT EXISTS(SELECT * FROM SYSCOLUMNS WHERE NAME = 'ExplodeCotasDeFundos' AND ID = OBJECT_ID('Carteira'))
BEGIN
	ALTER TABLE Carteira
	ADD ExplodeCotasDeFundos char(1) NOT NULL DEFAULT 'N';
END


IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'PrazoMedio') = 0
begin
	CREATE TABLE dbo.PrazoMedio
	(
		IdPrazoMedio			 INT 			NOT NULL PRIMARY KEY identity,
		DataAtual 		 		 DateTime		NOT NULL,
		IdCliente		 		 INT 			NOT NULL,
		Descricao 		 		 VARCHAR(255) 	NOT NULL,
		PrazoMedioAtivo		 	 INT 			NOT NULL,
		PrazoMedio		 		 Decimal(10,0)	NOT NULL,
		ValorPosicao	 		 Decimal(25,2)	NOT NULL
	)
END

IF EXISTS(SELECT * FROM sys.columns WHERE NAME = N'IdMercadoTipoPessoa' and Object_ID = Object_ID(N'CadastroComplementar'))
BEGIN
 ALTER TABLE CadastroComplementar ALTER COLUMN IdMercadoTipoPessoa varchar(20)
END

IF EXISTS(SELECT * FROM sys.columns WHERE NAME = N'PrazoMedioAtivo' and Object_ID = Object_ID(N'PrazoMedio'))
BEGIN
 exec sp_rename 'PrazoMedio.PrazoMedioAtivo', 'MercadoAtivo'
END

IF (select count(*) from SYSCOLUMNS WHERE ID = (SELECT ID FROM SYSOBJECTS WHERE  NAME = 'PrazoMedio' ) and name = 'IdPosicao') = 0
begin
 alter table PrazoMedio add IdPosicao int NULL;
END 

IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'SuitabilityParametrosWorkflow') = 0
begin
	CREATE TABLE dbo.SuitabilityParametrosWorkflow
	(
		[IdParametrosWorkflow] int NOT NULL default 1,
		[AtivaWorkFlow] char(1) NOT NULL default 'N',
		[VerificaEnquadramento] int NULL,
		[ResponsavelNome] varchar(50) NULL,
		[ResponsavelEmail] varchar(100) NULL,
		[ValorPeriodo] int NULL,
		[GrandezaPeriodo] int NULL,
		CONSTRAINT PK_SuitabilityParametrosWorkflow primary key(IdParametrosWorkflow)
	)
	
end

IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'SuitabilityParametrosWorkflowHistorico') = 0
begin
	CREATE TABLE dbo.SuitabilityParametrosWorkflowHistorico
	(
		[DataHistorico] [datetime] NOT NULL,
		[IdParametrosWorkflow] int NOT NULL default 1,
		[AtivaWorkFlow] char(1) NOT NULL default 'N',
		[VerificaEnquadramento] int NULL,
		[ResponsavelNome] varchar(50) NULL,
		[ResponsavelEmail] varchar(100) NULL,
		[ValorPeriodo] int NULL,
		[GrandezaPeriodo] int NULL,
		[Tipo] [varchar](10) NOT NULL,
		CONSTRAINT PK_SuitabilityParametrosWorkflowHistorico primary key(DataHistorico, IdParametrosWorkflow)
	)
	
end

if(select count(1) from SuitabilityParametrosWorkflow) = 0
begin
	insert into SuitabilityParametrosWorkflow (AtivaWorkFlow) Values('N')
end

if(select count(1) from SuitabilityParametrosWorkflowHistorico) = 0
begin
	insert into SuitabilityParametrosWorkflowHistorico (DataHistorico, AtivaWorkFlow, Tipo) Values (getdate(), 'N', 'Insert')
end

if not exists(select * from syscolumns where name = 'ControladoriaAtivo' and id = object_id('carteira'))
begin
alter table carteira
add ControladoriaAtivo char(1)
end

if not exists(select * from syscolumns where name = 'ControladoriaPassivo' and id = object_id('carteira'))
begin
alter table carteira
add	ControladoriaPassivo char(1)
end

if not exists(select * from syscolumns where name = 'MesmoConglomerado' and id = object_id('carteira'))
begin
alter table carteira
add MesmoConglomerado char(1)
end

if not exists(select * from syscolumns where name = 'CategoriaAnbima' and id = object_id('carteira'))
begin
alter table carteira
add CategoriaAnbima int null
end

if not exists(select * from syscolumns where name = 'Contratante' and id = object_id('carteira'))
begin
alter table carteira
add Contratante int references AgenteMercado(IdAgente)
end


--Adicionar atributo
if not exists(select 1 from syscolumns where id = object_id('SuitabilityQuestaoHistorico') and name = 'Validacao')
BEGIN
	ALTER TABLE SuitabilityQuestaoHistorico ADD Validacao varchar(100) null
END

IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'SuitabilityOpcaoHistorico') > 0
begin
	DROP TABLE SuitabilityOpcaoHistorico;

	CREATE TABLE dbo.SuitabilityOpcaoHistorico
	(
		[DataHistorico] [datetime] NOT NULL,
		[IdQuestao] [int] NOT NULL,
		[Questao] [varchar](2000) NULL,
		[IdOpcao] [int] NOT NULL,
		[Opcao] [varchar](2000) NULL,
		[Pontos] [int] NOT NULL,
		[Tipo] [varchar](10) NOT NULL,
		CONSTRAINT PK_SuitabilityOpcaoHistorico primary key(DataHistorico, IdQuestao, IdOpcao)
	)
end

IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'SuitabilityResposta') > 0
begin
	DROP TABLE SuitabilityResposta;
end

if exists(select 1 from sysconstraints where object_name(id) = 'SuitabilityRespostaCotistaDetalhe' and object_name(constid) = 'SuitabilityRespostaCotistaDetalhe_SuitabilityRespostaCotista_FK')
begin
	ALTER TABLE SuitabilityRespostaCotistaDetalhe DROP CONSTRAINT SuitabilityRespostaCotistaDetalhe_SuitabilityRespostaCotista_FK
end

IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'SuitabilityParametroPerfilHistorico') > 0
begin

	drop table SuitabilityParametroPerfilHistorico;

	CREATE TABLE dbo.SuitabilityParametroPerfilHistorico
	(
		[DataHistorico] [datetime] NOT NULL,
		IdParametroPerfil int NOT NULL,
		IdValidacao int NOT NULL,
		Validacao varchar(100) NULL,
		IdPerfilInvestidor int NOT NULL,
		PerfilInvestidor varchar(20) NOT NULL,
		Definicao text NULL,
		FaixaPontuacao int NULL,
		[Tipo] [varchar](10) NOT NULL,
		CONSTRAINT PK_SuitabilityParametroPerfilHistorico primary key(DataHistorico, IdParametroPerfil)
	)
end

IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'CarteiraSuitabilityHistorico') > 0
begin

	drop TABLE CarteiraSuitabilityHistorico

	CREATE TABLE dbo.CarteiraSuitabilityHistorico
	(
		[DataHistorico] [datetime] NOT NULL,
		[IdCarteira] int NOT NULL,
		[IdPerfilRisco] int NULL,
		[PerfilRisco] varchar(100) NULL,
		[Tipo] [varchar](10) NOT NULL,
		CONSTRAINT PK_CarteiraSuitabilityHistorico primary key(DataHistorico, IdCarteira)
	)	
end

IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'SuitabilityApuracaoRiscoHistorico') > 0
begin

	drop TABLE SuitabilityApuracaoRiscoHistorico

	CREATE TABLE dbo.SuitabilityApuracaoRiscoHistorico
	(
		[DataHistorico] [datetime] NOT NULL,
		[IdApuracaoRisco] int NOT NULL,
		[IdValidacao] int NOT NULL,
		[Validacao] varchar(100) NULL,
		[IdPerfilCarteira] int NOT NULL,
		[PerfilCarteira] varchar(100) NULL,
		[PerfilRiscoFundo] varchar(100) NOT NULL,
		[Criterio] varchar(50) NOT NULL,
		[Percentual] int NOT NULL default 0,
		[Tipo] [varchar](10) NOT NULL,
		CONSTRAINT PK_SuitabilityApuracaoRiscoHistorico primary key(DataHistorico, IdApuracaoRisco)
	)
end

IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'SuitabilityParametrosWorkflowHistorico') > 0
begin

	drop TABLE SuitabilityParametrosWorkflowHistorico

	CREATE TABLE dbo.SuitabilityParametrosWorkflowHistorico
	(
		[DataHistorico] [datetime] NOT NULL,
		[IdParametrosWorkflow] int NOT NULL default 1,
		[AtivaWorkFlow] char(1) NOT NULL default 'N',
		[VerificaEnquadramento] varchar(20) NULL,
		[ResponsavelNome] varchar(50) NULL,
		[ResponsavelEmail] varchar(100) NULL,
		[ValorPeriodo] int NULL,
		[GrandezaPeriodo] varchar(20) NULL,
		[Tipo] [varchar](10) NOT NULL,
		CONSTRAINT PK_SuitabilityParametrosWorkflowHistorico primary key(DataHistorico, IdParametrosWorkflow)
	)
	
end

IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'SuitabilityEventosHistorico') > 0
begin

	drop TABLE SuitabilityEventosHistorico;

	CREATE TABLE dbo.SuitabilityEventosHistorico(
		[DataHistorico] [datetime] NOT NULL,
		[IdSuitabilityEventos] [int] NOT NULL,
		[Descricao] [varchar](100) NOT NULL,
		[IdMensagem] [int] NULL,
		[Mesagem] text NULL,
		[Tipo] [varchar](10) NOT NULL,
	 CONSTRAINT [PK_SuitabilityEventosHistorico] PRIMARY KEY (DataHistorico, IdSuitabilityEventos))
end

IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'TabelaPerfilMensalCVM') > 0
begin
if not exists(select * from sys.all_columns where name = 'Secao18_CnpjComitente1' and object_id = object_id('TabelaPerfilMensalCVM'))
	alter table TabelaPerfilMensalCVM add Secao18_CnpjComitente1 varchar(50) null
if not exists(select * from sys.all_columns where name = 'Secao18_CnpjComitente1' and object_id = object_id('TabelaPerfilMensalCVM'))
	alter table TabelaPerfilMensalCVM add Secao18_CnpjComitente2 varchar(50) null
if not exists(select * from sys.all_columns where name = 'Secao18_CnpjComitente1' and object_id = object_id('TabelaPerfilMensalCVM'))
	alter table TabelaPerfilMensalCVM add Secao18_CnpjComitente3 varchar(50) null
if not exists(select * from sys.all_columns where name = 'Secao18_CnpjComitente1' and object_id = object_id('TabelaPerfilMensalCVM'))
	alter table TabelaPerfilMensalCVM add Secao18_ParteRelacionada1 char(1) null
if not exists(select * from sys.all_columns where name = 'Secao18_CnpjComitente1' and object_id = object_id('TabelaPerfilMensalCVM'))
	alter table TabelaPerfilMensalCVM add Secao18_ParteRelacionada2 char(1) null
if not exists(select * from sys.all_columns where name = 'Secao18_CnpjComitente1' and object_id = object_id('TabelaPerfilMensalCVM'))
	alter table TabelaPerfilMensalCVM add Secao18_ParteRelacionada3 char(1) null
if not exists(select * from sys.all_columns where name = 'Secao18_CnpjComitente1' and object_id = object_id('TabelaPerfilMensalCVM'))
	alter table TabelaPerfilMensalCVM add Secao18_ValorTotal1 decimal (16, 2) null
if not exists(select * from sys.all_columns where name = 'Secao18_CnpjComitente1' and object_id = object_id('TabelaPerfilMensalCVM'))
	alter table TabelaPerfilMensalCVM add Secao18_ValorTotal2 decimal (16, 2) null
if not exists(select * from sys.all_columns where name = 'Secao18_CnpjComitente1' and object_id = object_id('TabelaPerfilMensalCVM'))
	alter table TabelaPerfilMensalCVM add Secao18_ValorTotal3 decimal (16, 2) null
if not exists(select * from sys.all_columns where name = 'Secao18_CnpjComitente1' and object_id = object_id('TabelaPerfilMensalCVM'))
	alter table TabelaPerfilMensalCVM add Secao18_VedadaCobrancaTaxa char(1) null
if not exists(select * from sys.all_columns where name = 'Secao18_CnpjComitente1' and object_id = object_id('TabelaPerfilMensalCVM'))
	alter table TabelaPerfilMensalCVM add Secao18_DataUltimaCobranca datetime NULL
if not exists(select * from sys.all_columns where name = 'Secao18_CnpjComitente1' and object_id = object_id('TabelaPerfilMensalCVM'))
	alter table TabelaPerfilMensalCVM add Secao18_ValorUltimaCota decimal (16, 2) null
end

IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'OperacaoCotistaAux') = 0
Begin
	CREATE TABLE [dbo].[OperacaoCotistaAux]
	(
		[IdOperacaoCotistaAux] [int] IDENTITY(1,1) PRIMARY KEY NOT NULL,
		[IdOperacao] [int] NOT NULL,
		[IdCotista] [int] NOT NULL,
		[IdCarteira] [int] NOT NULL,
		[DataOperacao] [datetime] NOT NULL,
		[DataConversao] [datetime] NOT NULL,
		[DataLiquidacao] [datetime] NOT NULL,
		[DataAgendamento] [datetime] NOT NULL,
		[TipoOperacao] [tinyint] NOT NULL,
		[TipoResgate] [tinyint] NULL,
		[IdPosicaoResgatada] [int] NULL,
		[IdFormaLiquidacao] [tinyint] NOT NULL,
		[Quantidade] [decimal](28, 12) NOT NULL,
		[CotaOperacao] [decimal](28, 12) NOT NULL,
		[ValorBruto] [decimal](16, 2) NOT NULL,
		[ValorLiquido] [decimal](16, 2) NOT NULL,
		[ValorIR] [decimal](16, 2) NOT NULL,
		[ValorIOF] [decimal](16, 2) NOT NULL,
		[ValorCPMF] [decimal](16, 2) NOT NULL,
		[ValorPerformance] [decimal](16, 2) NOT NULL,
		[PrejuizoUsado] [decimal](16, 2) NOT NULL,
		[RendimentoResgate] [decimal](16, 2) NOT NULL,
		[VariacaoResgate] [decimal](16, 2) NOT NULL,
		[Observacao] [varchar](400) NOT NULL,
		[DadosBancarios] [varchar](500) NULL,
		[Fonte] [tinyint] NOT NULL,
		[IdConta] [int] NULL,
		[CotaInformada] [decimal](28, 12) NULL,
		[IdAgenda] [int] NULL,
		[IdOperacaoResgatada] [int] NULL,
		[IdOperacaoAuxiliar] [int] NULL,
		[IdOrdem] [int] NULL
	)
END	

IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'DetalheResgateCotistaAux') = 0
Begin
	CREATE TABLE [dbo].[DetalheResgateCotistaAux]
	(
		[IdOperacao] [int] NOT NULL,
		[IdPosicaoResgatada] [int] NOT NULL,
		[IdCotista] [int] NOT NULL,
		[IdCarteira] [int] NOT NULL,
		[Quantidade] [decimal](28, 12) NOT NULL,
		[ValorBruto] [decimal](16, 2) NOT NULL,
		[ValorLiquido] [decimal](16, 2) NOT NULL,
		[ValorCPMF] [decimal](16, 2) NOT NULL,
		[ValorPerformance] [decimal](16, 2) NOT NULL,
		[PrejuizoUsado] [decimal](16, 2) NOT NULL,
		[RendimentoResgate] [decimal](16, 2) NOT NULL,
		[VariacaoResgate] [decimal](16, 2) NOT NULL,
		[ValorIR] [decimal](16, 2) NOT NULL,
		[ValorIOF] [decimal](16, 2) NOT NULL,
		[DataOperacao] [datetime] NOT NULL,
		[TipoOperacao] [TinyInt] NOT NULL,
	 CONSTRAINT [PK_DetalheResgateCotistaAux] PRIMARY KEY CLUSTERED 
	(
		[IdOperacao] ASC,
		[IdPosicaoResgatada] ASC
	)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
	) ON [PRIMARY]
END

IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'PosicaoCotistaAux') = 0
Begin
	CREATE TABLE [dbo].[PosicaoCotistaAux]
	(
		[IdPosicaoCotistaAux] [int] IDENTITY(1,1) PRIMARY KEY NOT NULL,
		[DataReferencia] [datetime] NOT NULL,
		[IdPosicao] [int] NOT NULL,
		[IdOperacao] [int] NULL,
		[IdCotista] [int] NOT NULL,
		[IdCarteira] [int] NOT NULL,
		[ValorAplicacao] [decimal](16, 2) NOT NULL,
		[DataAplicacao] [datetime] NOT NULL,
		[DataConversao] [datetime] NOT NULL,
		[CotaAplicacao] [decimal](28, 12) NOT NULL,
		[CotaDia] [decimal](28, 12) NOT NULL,
		[ValorBruto] [decimal](16, 2) NOT NULL,
		[ValorLiquido] [decimal](16, 2) NOT NULL,
		[QuantidadeInicial] [decimal](28, 12) NOT NULL,
		[Quantidade] [decimal](28, 12) NOT NULL,
		[QuantidadeBloqueada] [decimal](28, 12) NOT NULL,
		[DataUltimaCobrancaIR] [datetime] NOT NULL,
		[ValorIR] [decimal](16, 2) NOT NULL,
		[ValorIOF] [decimal](16, 2) NOT NULL,
		[ValorPerformance] [decimal](16, 2) NOT NULL,
		[ValorIOFVirtual] [decimal](16, 2) NOT NULL,
		[QuantidadeAntesCortes] [decimal](28, 12) NOT NULL,
		[ValorRendimento] [decimal](16, 2) NOT NULL,
		[DataUltimoCortePfee] [datetime] NULL,
		[PosicaoIncorporada] [char](1) NOT NULL
	)
END	

IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'OperacaoFundoAux') = 0
Begin
	CREATE TABLE [dbo].[OperacaoFundoAux](
		[IdOperacaoFundoAux] [int] IDENTITY(1,1) PRIMARY KEY NOT NULL,
		[IdOperacao] [int] NOT NULL,
		[IdCliente] [int] NOT NULL,
		[IdCarteira] [int] NOT NULL,
		[DataOperacao] [datetime] NOT NULL,
		[DataConversao] [datetime] NOT NULL,
		[DataLiquidacao] [datetime] NOT NULL,
		[DataAgendamento] [datetime] NOT NULL,
		[TipoOperacao] [tinyint] NOT NULL,
		[TipoResgate] [tinyint] NULL,
		[IdPosicaoResgatada] [int] NULL,
		[IdFormaLiquidacao] [tinyint] NOT NULL,
		[Quantidade] [decimal](28, 12) NOT NULL,
		[CotaOperacao] [decimal](28, 12) NOT NULL,
		[ValorBruto] [decimal](16, 2) NOT NULL,
		[ValorLiquido] [decimal](16, 2) NOT NULL,
		[ValorIR] [decimal](16, 2) NOT NULL,
		[ValorIOF] [decimal](16, 2) NOT NULL,
		[ValorCPMF] [decimal](16, 2) NOT NULL,
		[ValorPerformance] [decimal](16, 2) NOT NULL,
		[PrejuizoUsado] [decimal](16, 2) NOT NULL,
		[RendimentoResgate] [decimal](16, 2) NOT NULL,
		[VariacaoResgate] [decimal](16, 2) NOT NULL,
		[IdConta] [int] NULL,
		[Observacao] [varchar](400) NOT NULL,
		[Fonte] [tinyint] NOT NULL,
		[IdAgenda] [int] NULL,
		[CotaInformada] [decimal](28, 12) NULL,
		[IdOperacaoResgatada] [int] NULL
	)
END	

IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'PosicaoFundoAux') = 0
Begin	
	CREATE TABLE [dbo].[PosicaoFundoAux]
	(
		[IdPosicaoFundoAux] [int] IDENTITY(1,1) PRIMARY KEY NOT NULL,
		[DataReferencia] [datetime] NOT NULL,
		[IdPosicao] [int] NOT NULL,
		[IdOperacao] [int] NULL,
		[IdCliente] [int] NOT NULL,
		[IdCarteira] [int] NOT NULL,
		[ValorAplicacao] [decimal](16, 2) NOT NULL,
		[DataAplicacao] [datetime] NOT NULL,
		[DataConversao] [datetime] NOT NULL,
		[CotaAplicacao] [decimal](28, 12) NOT NULL,
		[CotaDia] [decimal](28, 12) NOT NULL,
		[ValorBruto] [decimal](16, 2) NOT NULL,
		[ValorLiquido] [decimal](16, 2) NOT NULL,
		[QuantidadeInicial] [decimal](28, 12) NOT NULL,
		[Quantidade] [decimal](28, 12) NOT NULL,
		[QuantidadeBloqueada] [decimal](28, 12) NULL,
		[DataUltimaCobrancaIR] [datetime] NOT NULL,
		[ValorIR] [decimal](16, 2) NOT NULL,
		[ValorIOF] [decimal](16, 2) NOT NULL,
		[ValorPerformance] [decimal](16, 2) NOT NULL,
		[ValorIOFVirtual] [decimal](16, 2) NOT NULL,
		[QuantidadeAntesCortes] [decimal](28, 12) NOT NULL,
		[ValorRendimento] [decimal](16, 2) NOT NULL,
		[DataUltimoCortePfee] [datetime] NULL,
		[PosicaoIncorporada] [char](1) NOT NULL
	)
END	

IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'DetalheResgateFundoAux') = 0
Begin
CREATE TABLE [dbo].[DetalheResgateFundoAux]
(
	[IdOperacao] [int] NOT NULL,
	[IdPosicaoResgatada] [int] NOT NULL,
	[IdCliente] [int] NOT NULL,
	[IdCarteira] [int] NOT NULL,
	[Quantidade] [decimal](28, 12) NOT NULL,
	[ValorBruto] [decimal](16, 2) NOT NULL,
	[ValorLiquido] [decimal](16, 2) NOT NULL,
	[ValorCPMF] [decimal](16, 2) NOT NULL,
	[ValorPerformance] [decimal](16, 2) NOT NULL,
	[PrejuizoUsado] [decimal](16, 2) NOT NULL,
	[RendimentoResgate] [decimal](16, 2) NOT NULL,
	[VariacaoResgate] [decimal](16, 2) NOT NULL,
	[ValorIR] [decimal](16, 2) NOT NULL,
	[ValorIOF] [decimal](16, 2) NOT NULL,
	[DataOperacao] [datetime] NOT NULL,
	[TipoOperacao] [TinyInt] NOT NULL,
	CONSTRAINT [PK_DetalheResgateFundoAux] PRIMARY KEY CLUSTERED 
	(
		[IdOperacao] ASC,
		[IdPosicaoResgatada] ASC
	)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
	) ON [PRIMARY]
END

if not exists(select 1 from syscolumns where id = object_id('OperacaoFundo') and name = 'RegistroEditado')
BEGIN
	ALTER TABLE dbo.OperacaoFundo ADD RegistroEditado char(1) NOT NULL DEFAULT 'N'
END
	
if not exists(select 1 from syscolumns where id = object_id('OperacaoFundo') and name = 'ValoresColados')
BEGIN
	ALTER TABLE dbo.OperacaoFundo ADD ValoresColados char(1) NOT NULL DEFAULT 'N' 
END
	
if not exists(select 1 from syscolumns where id = object_id('OperacaoFundoAux') and name = 'RegistroEditado')
BEGIN
	ALTER TABLE dbo.OperacaoFundoAux ADD RegistroEditado char(1) NOT NULL DEFAULT 'N'
END
	
if not exists(select 1 from syscolumns where id = object_id('OperacaoFundoAux') and name = 'ValoresColados')
BEGIN
	ALTER TABLE dbo.OperacaoFundoAux ADD ValoresColados char(1) NOT NULL DEFAULT 'N' 
END

if not exists(select 1 from syscolumns where id = object_id('OperacaoCotista') and name = 'RegistroEditado')
BEGIN
	ALTER TABLE dbo.OperacaoCotista ADD RegistroEditado char(1) NOT NULL DEFAULT 'N'
END	

if not exists(select 1 from syscolumns where id = object_id('OperacaoCotista') and name = 'ValoresColados')
BEGIN
	ALTER TABLE dbo.OperacaoCotista ADD ValoresColados char(1) NOT NULL DEFAULT 'N'
END	

if not exists(select 1 from syscolumns where id = object_id('OperacaoCotistaAux') and name = 'RegistroEditado')
BEGIN
	ALTER TABLE dbo.OperacaoCotistaAux ADD RegistroEditado char(1) NOT NULL DEFAULT 'N'
END	

if not exists(select 1 from syscolumns where id = object_id('OperacaoCotistaAux') and name = 'ValoresColados')
BEGIN
	ALTER TABLE dbo.OperacaoCotistaAux ADD ValoresColados char(1) NOT NULL DEFAULT 'N'
END	

IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'ColagemComeCotas') = 0
Begin
	CREATE TABLE [dbo].[ColagemComeCotas]
	(
		IdColagemComeCotas [int] PRIMARY KEY NOT NULL IDENTITY,
		TipoRegistro char(1) NOT NULL ,
		CodigoExterno char(15) NOT NULL ,
		DataReferencia [datetime] NOT NULL,
		DataOperacao [datetime] NOT NULL,
		IdCliente  [int] NOT NULL,
		IdCarteira  [int] NOT NULL,
		DataConversao [datetime] NULL,
		Quantidade [decimal](28, 12) NOT NULL,
		CotaOperacao [decimal](28, 12) NOT NULL,
		ValorBruto [decimal](28, 12) NOT NULL,
		ValorLiquido [decimal](28, 12) NOT NULL,
		ValorCPMF [decimal](28, 12) NOT NULL,
		ValorPerformance [decimal](28, 12) NOT NULL,
		PrejuizoUsado [decimal](28, 12) NOT NULL,
		RendimentoComeCotas [decimal](28, 12) NOT NULL,
		VariacaoComeCotas [decimal](28, 12) NOT NULL,
		ValorIR [decimal](28, 12) NOT NULL,
		ValorIOF [decimal](28, 12) NOT NULL
	)	
END	

IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'ColagemComeCotasDetalhe') = 0
Begin
	CREATE TABLE [dbo].[ColagemComeCotasDetalhe]
	(
		IdColagemComeCotasDetalhe  [int] PRIMARY KEY NOT NULL IDENTITY,
		CodigoExterno char(15) NOT NULL ,
		DataReferencia [datetime] NOT NULL,
		DataOperacao [datetime] NOT NULL,
		IdCliente  [int] NOT NULL,
		IdCarteira  [int] NOT NULL,
		DataConversao [datetime] NULL,
		Quantidade [decimal](28, 12) NOT NULL,
		CotaOperacao [decimal](28, 12) NOT NULL,
		ValorBruto [decimal](28, 12) NOT NULL,
		ValorLiquido [decimal](28, 12) NOT NULL,
		ValorCPMF [decimal](28, 12) NOT NULL,
		ValorPerformance [decimal](28, 12) NOT NULL,
		PrejuizoUsado [decimal](28, 12) NOT NULL,
		RendimentoComeCotas [decimal](28, 12) NOT NULL,
		VariacaoComeCotas [decimal](28, 12) NOT NULL,
		ValorIR [decimal](28, 12) NOT NULL,
		ValorIOF [decimal](28, 12) NOT NULL,
	)	
END	

IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'ColagemComeCotasDetalhePosicao') = 0
Begin
	CREATE TABLE [dbo].[ColagemComeCotasDetalhePosicao]
	(
		IdColagemComeCotasDetalhePosicao [int] PRIMARY KEY NOT NULL IDENTITY,
		IdColagemComeCotasDetalhe [int] NOT NULL,
		DataReferencia [datetime] NOT NULL,
		QuantidadeInicial [decimal](28, 12) NOT NULL,
		QuantidadeRestante [decimal](28, 12) NOT NULL
	)		
END	

IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'ColagemComeCotasMapaFundo') = 0
Begin
	CREATE TABLE [dbo].[ColagemComeCotasMapaFundo]
	(
		IdColagemComeCotasMapaFundo  [int] PRIMARY KEY NOT NULL IDENTITY,
		DataReferencia [datetime] NOT NULL,
		IdCliente  [int] NOT NULL,
		IdCarteira  [int] NOT NULL,
		DataConversao [datetime] NULL,
		DetalheImportado char(1) NOT NULL DEFAULT 'N',
		IdOperacaoComeCotasVinculada  [int] NOT NULL,	
		Participacao [decimal](28, 12) NOT NULL
	)		
END	

IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'ColagemComeCotasPosFundo') = 0
Begin
	CREATE TABLE [dbo].[ColagemComeCotasPosFundo]
	(
		IdColagemComeCotasPosFundo  [int] PRIMARY KEY NOT NULL IDENTITY,
		IdCarteira  				[int] NOT NULL,
		IdCliente  					[int] NOT NULL,
		DataConversao 				[datetime] NULL,
		DataReferencia 				[datetime] NOT NULL,
		IdPosicaoVinculada  		[int] NOT NULL,
		PrejuizoUsado 				[decimal](28, 12) NOT NULL,
		RendimentoComeCotas 		[decimal](28, 12) NOT NULL,
		ValorBruto 					[decimal](28, 12) NOT NULL,
		ValorCPMF 					[decimal](28, 12) NOT NULL,
		ValorIOF 					[decimal](28, 12) NOT NULL,
		ValorIR 					[decimal](28, 12) NOT NULL,
		ValorLiquido 				[decimal](28, 12) NOT NULL,
		ValorPerformance 			[decimal](28, 12) NOT NULL,
		VariacaoComeCotas 			[decimal](28, 12) NOT NULL,
		Quantidade					[decimal](28, 12) NOT NULL,
		Participacao				[decimal](28, 12) NOT NULL,
		DetalheImportado 			[char](1) NOT NULL DEFAULT 'N'
	)		
END	


IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'ColagemResgateMapaFundo') = 0
Begin
	CREATE TABLE [dbo].[ColagemResgateMapaFundo]
	(
		IdColagemResgateMapaFundo  [int] PRIMARY KEY NOT NULL IDENTITY,
		DataReferencia [datetime] NOT NULL,
		IdCliente  [int] NOT NULL,
		IdCarteira  [int] NOT NULL,
		DataConversao [datetime] NULL,
		DetalheImportado char(1) NOT NULL DEFAULT 'N',
		IdOperacaoResgateVinculada  [int] NOT NULL,	
		Participacao [decimal](28, 12) NOT NULL
	)		
END	

IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'ColagemOperResgPosFundo') = 0
Begin
	CREATE TABLE [dbo].[ColagemOperResgPosFundo]
	(
		IdColagemOperResgPosFundo  [int] PRIMARY KEY NOT NULL IDENTITY,
		IdCarteira  [int] NOT NULL,
		IdCliente  [int] NOT NULL,
		DataReferencia [datetime] NOT NULL,
		IdColagemResgateDetalhe  [int] NOT NULL,
		IdOperacaoResgateVinculada  [int] NOT NULL,
		IdPosicaoVinculada  [int] NOT NULL,
		QuantidadeVinculada [decimal](28, 12) NOT NULL
	)		
END	


IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'ColagemResgate') = 0
Begin
	CREATE TABLE [dbo].[ColagemResgate]
	(
		IdColagemResgate [int] PRIMARY KEY NOT NULL IDENTITY,
		TipoRegistro char(1) NOT NULL ,
		CodigoExterno char(15) NOT NULL ,
		DataReferencia [datetime] NOT NULL,
		DataOperacao [datetime] NOT NULL,
		IdCliente  [int] NOT NULL,
		IdCarteira  [int] NOT NULL,
		DataConversao [datetime] NULL,
		Quantidade [decimal](28, 12) NOT NULL,
		CotaOperacao [decimal](28, 12) NOT NULL,
		ValorBruto [decimal](28, 12) NOT NULL,
		ValorLiquido [decimal](28, 12) NOT NULL,
		ValorCPMF [decimal](28, 12) NOT NULL,
		ValorPerformance [decimal](28, 12) NOT NULL,
		PrejuizoUsado [decimal](28, 12) NOT NULL,
		RendimentoResgate [decimal](28, 12) NOT NULL,
		VariacaoResgate [decimal](28, 12) NOT NULL,
		ValorIR [decimal](28, 12) NOT NULL,
		ValorIOF [decimal](28, 12) NOT NULL
	)	
END	

IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'ColagemResgateDetalhe') = 0
Begin
	CREATE TABLE [dbo].[ColagemResgateDetalhe]
	(
		IdColagemResgateDetalhe  [int] PRIMARY KEY NOT NULL IDENTITY,
		CodigoExterno char(15) NOT NULL ,
		DataReferencia [datetime] NOT NULL,
		DataOperacao [datetime] NOT NULL,
		IdCliente  [int] NOT NULL,
		IdCarteira  [int] NOT NULL,
		DataConversao [datetime] NULL,
		Quantidade [decimal](28, 12) NOT NULL,
		CotaOperacao [decimal](28, 12) NOT NULL,
		ValorBruto [decimal](28, 12) NOT NULL,
		ValorLiquido [decimal](28, 12) NOT NULL,
		ValorCPMF [decimal](28, 12) NOT NULL,
		ValorPerformance [decimal](28, 12) NOT NULL,
		PrejuizoUsado [decimal](28, 12) NOT NULL,
		RendimentoResgate [decimal](28, 12) NOT NULL,
		VariacaoResgate [decimal](28, 12) NOT NULL,
		ValorIR [decimal](28, 12) NOT NULL,
		ValorIOF [decimal](28, 12) NOT NULL,
	)	
END	

 IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'ColagemResgateDetalhePosicao') = 0
Begin
	CREATE TABLE [dbo].[ColagemResgateDetalhePosicao]
	(
		IdColagemResgateDetalhePosicao [int] PRIMARY KEY NOT NULL IDENTITY,
		IdColagemResgateDetalhe [int] NOT NULL,
		DataReferencia [datetime] NOT NULL,
		QuantidadeInicial [decimal](28, 12) NOT NULL,
		QuantidadeRestante [decimal](28, 12) NOT NULL
	)		
END	

IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'ColagemResgateMapaCotista') = 0
Begin
	CREATE TABLE [dbo].[ColagemResgateMapaCotista]
	(
		IdColagemResgateMapaCotista  [int] PRIMARY KEY NOT NULL IDENTITY,
		DataReferencia [datetime] NOT NULL,
		IdCliente  [int] NOT NULL,
		IdCarteira  [int] NOT NULL,
		DataConversao [datetime] NULL,
		DetalheImportado char(1) NOT NULL DEFAULT 'N',
		IdOperacaoResgateVinculada  [int] NOT NULL,	
		Participacao [decimal](28, 12) NOT NULL
	)		
END	

IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'ColagemOperResgPosCotista') = 0
Begin
	CREATE TABLE [dbo].[ColagemOperResgPosCotista]
	(
		IdColagemOperResgPosCotista  [int] PRIMARY KEY NOT NULL IDENTITY,
		IdCarteira  [int] NOT NULL,
		IdCliente  [int] NOT NULL,
		DataReferencia [datetime] NOT NULL,
		IdColagemResgateDetalhe  [int] NOT NULL,
		IdOperacaoResgateVinculada  [int] NOT NULL,
		IdPosicaoVinculada  [int] NOT NULL,
		QuantidadeVinculada [decimal](28, 12) NOT NULL
	)		
END	

if exists(select 1 from syscolumns where id = object_id('ColagemResgateMapaFundo') and name = 'Participacao')
BEGIN	
	ALTER TABLE [dbo].[ColagemResgateMapaFundo] DROP COLUMN Participacao;
END
	
if NOT exists(select 1 from syscolumns where id = object_id('ColagemResgateMapaFundo') and name = 'IdColagemResgateDetalhe')
BEGIN	
	ALTER TABLE [dbo].[ColagemResgateMapaFundo] ADD IdColagemResgateDetalhe int NULL;
END

if exists(select 1 from syscolumns where id = object_id('ColagemResgate') and name = 'DataOperacao')
BEGIN	
	ALTER TABLE [dbo].[ColagemResgate] DROP COLUMN DataOperacao;
END

if exists(select 1 from syscolumns where id = object_id('ColagemResgateDetalhe') and name = 'DataOperacao')
BEGIN	
	ALTER TABLE [dbo].[ColagemResgateDetalhe] DROP COLUMN DataOperacao;
END

if exists(select 1 from syscolumns where id = object_id('ColagemComeCotas') and name = 'DataOperacao')
BEGIN	
	ALTER TABLE [dbo].[ColagemComeCotas] DROP COLUMN DataOperacao;
END

if exists(select 1 from syscolumns where id = object_id('ColagemComeCotasDetalhe') and name = 'DataOperacao')
BEGIN	
	ALTER TABLE [dbo].[ColagemComeCotasDetalhe] DROP COLUMN DataOperacao;
END

if exists(select 1 from syscolumns where id = object_id('ColagemResgateMapaCotista') and name = 'Participacao')
BEGIN	
	ALTER TABLE [dbo].[ColagemResgateMapaCotista] DROP COLUMN Participacao;
END

if NOT exists(select 1 from syscolumns where id = object_id('ColagemResgateMapaCotista') and name = 'IdColagemResgateDetalhe')
BEGIN	
	ALTER TABLE [dbo].[ColagemResgateMapaCotista] ADD IdColagemResgateDetalhe int NULL;
END

if not exists(select 1 from syscolumns where id = object_id('ColagemResgate') and name = 'Processado')
BEGIN	
	ALTER TABLE [dbo].[ColagemResgate] ADD Processado char(1) NOT NULL DEFAULT 'N';
END

if not exists(select 1 from syscolumns where id = object_id('ColagemComeCotas') and name = 'Processado')
BEGIN	
	ALTER TABLE [dbo].[ColagemComeCotas] ADD Processado char(1) NOT NULL DEFAULT 'N';
END

IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'ColagemComeCotasPosCotista') = 0
Begin
	CREATE TABLE [dbo].[ColagemComeCotasPosCotista]
	(
		IdColagemComeCotasPosCotista  [int] PRIMARY KEY NOT NULL IDENTITY,
		IdCarteira  				[int] NOT NULL,
		IdCliente  					[int] NOT NULL,
		DataConversao 				[datetime] NULL,
		DataReferencia 				[datetime] NOT NULL,
		IdPosicaoVinculada  		[int] NOT NULL,
		PrejuizoUsado 				[decimal](28, 12) NOT NULL,
		RendimentoComeCotas 		[decimal](28, 12) NOT NULL,
		ValorBruto 					[decimal](28, 12) NOT NULL,
		ValorCPMF 					[decimal](28, 12) NOT NULL,
		ValorIOF 					[decimal](28, 12) NOT NULL,
		ValorIR 					[decimal](28, 12) NOT NULL,
		ValorLiquido 				[decimal](28, 12) NOT NULL,
		ValorPerformance 			[decimal](28, 12) NOT NULL,
		VariacaoComeCotas 			[decimal](28, 12) NOT NULL,
		Quantidade					[decimal](28, 12) NOT NULL,
		Participacao				[decimal](28, 12) NOT NULL,
		DetalheImportado 			[char](1) NOT NULL DEFAULT 'N'
	)		
END	


if not exists(select * from sys.indexes where object_id = OBJECT_ID('CadastroComplementar') and name = 'UQ_CadastroComplementar')
begin
	delete CadastroComplementar
	where
		IdCadastroComplementares not in(
			select  	
				(
					select top(1) 
						IdCadastroComplementares 
					from 
						CadastroComplementar ccInt
					where
						cc.IdCamposComplementares = ccInt.IdCamposComplementares and
						cc.IdMercadoTipoPessoa = ccInt.IdMercadoTipoPessoa
					order by IdCadastroComplementares desc		
				) as IdCadastroComplementares
			from 
				CadastroComplementar cc
			group by 
				IdMercadoTipoPessoa, 
				IdCamposComplementares		
		)

	delete CadastroComplementar where IdCamposComplementares is null

	ALTER TABLE CadastroComplementar
	ADD CONSTRAINT UQ_CadastroComplementar UNIQUE (IdMercadoTipoPessoa, IdCamposComplementares) 

end

IF NOT EXISTS(SELECT * FROM SYSCOLUMNS WHERE NAME = 'TipoCotistaAnbima' AND ID = OBJECT_ID('Cotista'))
BEGIN
	alter table Cotista add TipoCotistaAnbima int null;
end

if (not exists (select * from sys.all_columns where name = 'Corretora' and OBJECT_NAME(object_id) = 'Carteira'))
begin
	alter table carteira
	add Corretora char(1)
end
else
begin
	if(exists(
		select 
			* 
		from 
			sys.all_columns 
		where 
			name = 'Corretora' and 
			OBJECT_NAME(object_id) = 'Carteira' and 
			system_type_id <> 175 and 
			max_length <> 1
	))
		raiserror('A coluna corretora da tabela Carteira esta com tamanho ou formato incorreto. Favor corrigir para prosseguir.',16,1);	
end

IF NOT EXISTS(SELECT * FROM SYSCOLUMNS WHERE NAME = 'LimiteConcentracaoEmissor' AND ID = OBJECT_ID('Lamina'))
BEGIN
	alter table Lamina add LimiteConcentracaoEmissor decimal(16, 2) NULL
END

IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'AliquotaEspecifica') = 0
Begin
	CREATE TABLE dbo.AliquotaEspecifica 
	(
		IdCarteira						INT                 NOT NULL,
		DataValidade					DateTime            NOT NULL,
		Taxa	 						Decimal(5,2)		NOT NULL,  
		CONSTRAINT AliquotaEspecifica_PK 		primary key(IdCarteira, DataValidade)
	)
	
	ALTER TABLE AliquotaEspecifica ADD CONSTRAINT AliquotaEspecifica_Carteira_FK1 FOREIGN KEY (IdCarteira) REFERENCES Carteira(IdCarteira) on delete cascade
	
end 

IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'ExcecoesTributacaoIR') = 0
Begin
	CREATE TABLE dbo.ExcecoesTributacaoIR
	(
		 IdExcecoesTributacaoIR INT PRIMARY KEY identity NOT NULL, 
		 Mercado int NOT NULL,
		 TipoClasseAtivo int NULL,
		 Ativo varchar(50) NULL,
		 TipoInvestidor int NOT NULL,	
		 IsencaoIR int NOT NULL,
		 AliquotaIR varchar(20) NULL,
		 IsencaoGanhoCapital int NOT NULL,
		 AliquotaIRGanhoCapital varchar(20) NULL,
		 CdAtivoBMF varchar(20)  NULL,
		 SerieBMF varchar(5) NULL,
		 IdTituloRendaFixa int FOREIGN KEY REFERENCES TituloRendaFixa(IdTitulo) NULL,
		 IdOperacaoSwap int FOREIGN KEY REFERENCES OperacaoSwap(IdOperacao) NULL,
		 IdCarteira int FOREIGN KEY REFERENCES Carteira(IdCarteira) NULL,
		 CdAtivoBolsa varchar(25) FOREIGN KEY REFERENCES AtivoBolsa(CdAtivoBolsa) NULL,
		 DataInicio DateTime NOT NULL,
		 DataFim DateTime NOT NULL,
		 CONSTRAINT FK_AtivoBMF FOREIGN KEY (CdAtivoBMF, SerieBMF) REFERENCES AtivoBMF(CdAtivoBMF, Serie)
	) 
End
else
Begin
	IF NOT EXISTS(SELECT * FROM SYSCOLUMNS WHERE NAME = 'DataInicio' AND ID = OBJECT_ID('ExcecoesTributacaoIR'))
	BEGIN
		 Alter table ExcecoesTributacaoIR ADD DataInicio DateTime NOT NULL;
	END
	
	IF NOT EXISTS(SELECT * FROM SYSCOLUMNS WHERE NAME = 'DataFim' AND ID = OBJECT_ID('ExcecoesTributacaoIR'))
	BEGIN
		 Alter table ExcecoesTributacaoIR ADD DataFim DateTime NOT NULL;
	END
end		 
		 
		 






IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'IRProventos') = 0
Begin
	CREATE TABLE dbo.IRProventos 
	(
		IdIRProventos					INT PRIMARY KEY identity NOT NULL, 
		IdCotista						INT                 NOT NULL,
		ValorDistribuido				decimal				NOT NULL,
		ValorIR							decimal				NOT NULL,
		IdCarteira						INT					NOT NULL,
		DataLancamento   				DateTime			NOT NULL,
		DataVencimento					DateTime			NOT NULL,
		Descricao						varchar(50)         NOT NULL,
		IsentoIR						varchar(01)			NOT NULL
	)
	
	ALTER TABLE IRProventos ADD CONSTRAINT IRProventos_Carteira_FK1 FOREIGN KEY (IdCarteira) REFERENCES Carteira(IdCarteira) on delete cascade
	ALTER TABLE IRProventos ADD CONSTRAINT IRProventos_Cotista_FK1 FOREIGN KEY (IdCotista) REFERENCES Cotista(IdCotista) on delete cascade
	
end 

if (not exists (select * from sys.all_columns where name = 'Corretora' and OBJECT_NAME(object_id) = 'Carteira'))
begin
	alter table carteira
	add Corretora char(1)
end
else
begin
	if(exists(
		select 
			* 
		from 
			sys.all_columns 
		where 
			name = 'Corretora' and 
			OBJECT_NAME(object_id) = 'Carteira' and 
			system_type_id <> 175 and 
			max_length <> 1
	))
		raiserror('A coluna corretora da tabela Carteira esta com tamanho ou formato incorreto. Favor corrigir para prosseguir.',16,1);	
end

if  not exists (select 'x' from sysobjects o inner join syscolumns c on o.id = c.cdefault inner join sysobjects t on c.id = t.id where o.xtype = 'D' and c.name = 'Corretora' and t.name = 'Carteira')
begin
 	ALTER TABLE Carteira ADD default 'N' for Corretora;
end

if(not exists(select * from sysobjects where id = object_id('LiquidacaoTaxaCustodia')))
begin	
	CREATE TABLE dbo.LiquidacaoTaxaCustodia
	(
		IdLiquidacaoTaxaCustodia int not null primary key identity,
		IdOperacao int NOT NULL,
		IdCliente int NOT NULL,
		DataHistorico Datetime not null,
		CustoCustodia decimal(16,2) NULL,
		TipoCustodia int NOT NULL,
	);
END

if (not exists (select * from sys.all_columns where name = 'DataVencimento' and OBJECT_NAME(object_id) = 'PessoaDocumento'))
begin
	ALTER TABLE PessoaDocumento ADD DataVencimento datetime null
end 

if (not exists (select * from sys.all_columns where name = 'DataVencimentoCadastro' and OBJECT_NAME(object_id) = 'Pessoa'))
begin
	ALTER TABLE Pessoa ADD DataVencimentoCadastro datetime null
end 

if (not exists (select * from sys.all_columns where name = 'AlertaCadastro' and OBJECT_NAME(object_id) = 'Pessoa'))
begin
	ALTER TABLE Pessoa ADD AlertaCadastro char not null default 'N'
end

if (not exists (select * from sys.all_columns where name = 'EmailAlerta' and OBJECT_NAME(object_id) = 'Pessoa'))
begin
	ALTER TABLE Pessoa ADD EmailAlerta varchar(200) null
end

if (not exists (select * from sys.all_columns where name = 'CdIso' and OBJECT_NAME(object_id) = 'Moeda'))
begin
	ALTER TABLE Moeda ADD  CdIso char(3) null
end


if not exists(select * from sys.all_columns where Name = 'DataAtualizacao' and object_id = object_id('Cliente'))
begin
	alter table cliente add DataAtualizacao Datetime not null default GetDAte()
end 
else
begin
	--se a coluna ja existe verificar se é igual a defino senão disparar um erro
	if not exists(
		select * 
		from sys.all_columns a 
		inner join sys.types t 
			on a.user_type_id = t.user_type_id
		where a.object_id = object_id('Cliente')		
			and a.Name = 'DataAtualizacao'				
			and	a.user_type_id = TYPE_ID('datetime')	
			and a.is_nullable = 0
			and object_definition(a.default_object_id) like '%getdate%'
		)
	begin
		raiserror('Coluna Cliente.DataAtualizacao inconsistente, favor verificar',16,1)
	end
end

-- Se existir a trigger, apaga e cria de novo, pois triggers não guardam dados. 
-- Devem ser sempre recriadas para garantir que estão consistente com o script.
if exists(select * from sys.triggers where name = 'trg_cliente_DataAtualizacao')
begin
	drop trigger trg_cliente_DataAtualizacao
end
GO

-- trigger para preencher a data de ultima atualização 
create trigger trg_cliente_DataAtualizacao on dbo.Cliente
for update
as
begin	

	if (@@rowcount = 0)
	  return;

	set nocount on

	update Cliente 
	set 
	   DataAtualizacao = getdate()          
	where
		Cliente.IdCliente IN (select IdCliente from inserted)

end
GO

-- Se existir a trigger, apaga e cria de novo, pois triggers não guardam dados. 
-- Devem ser sempre recriadas para garantir que estão consistente com o script.
if exists(select * from sys.triggers where name = 'trg_Pessoa_DataUltimaAlteracao')
begin
	drop trigger trg_Pessoa_DataUltimaAlteracao
end
GO

-- trigger para preencher a data de ultima atualização 
create trigger trg_Pessoa_DataUltimaAlteracao on dbo.Pessoa
for update
as
begin	

	if (@@rowcount = 0)
	  return;

	set nocount on

	update Pessoa 
	set 
	   DataUltimaAlteracao = getdate()          
	where
		Pessoa.IdPessoa IN (select IdPessoa from inserted)

end
GO

if not exists(select * from sys.all_columns where Name = 'AtivoCetip' and object_id = object_id('ClienteRendaFixa'))
begin
	alter table ClienteRendaFixa add AtivoCetip char not null default 'S'
end 
else
begin
	--se a coluna ja existe verificar se  igual a defini��o sen�o disparar um erro
	if not exists(
		select * 
		from sys.all_columns a 
		inner join sys.types t 
			on a.user_type_id = t.user_type_id
		where a.object_id = object_id('ClienteRendaFixa')		
			and a.Name = 'AtivoCetip'				
			and	a.user_type_id = TYPE_ID('char')	
			and a.is_nullable = 0
		)
	begin
		raiserror('Coluna ClienteRendaFixa.AtivoCetip inconsistente, favor verificar',16,1)
	end
end

IF EXISTS (SELECT NAME FROM SYSOBJECTS WHERE NAME = 'trg_cliente_dataatualizacao' AND TYPE = 'TR')
BEGIN 
	DROP TRIGGER trg_cliente_dataatualizacao
END


IF NOT EXISTS(SELECT * FROM SYSCOLUMNS WHERE NAME = 'DataEnvioCetip' AND ID = OBJECT_ID('Cliente'))
BEGIN
	alter table Cliente add DataEnvioCetip Datetime NULL
END

if (not exists (select * from sys.all_columns where name = 'CodigoSTI' and OBJECT_NAME(object_id) = 'Carteira'))
begin
	ALTER TABLE Carteira ADD  CodigoSTI varchar(50) null
end


IF EXISTS(select * FROM sys.views where name = 'ViewStatusInvestidores')
begin
	DROP VIEW ViewStatusInvestidores;
end	
GO

CREATE VIEW ViewStatusInvestidores AS
  select Cotista.IdCotista, 
		ltrim(rtrim(CONVERT(char(20),Cotista.IdCotista))) + ' - ' + Cotista.Apelido as Cotista,
		pessoaCotista.CPFCNPJ as DocumentoCotista,
		'' as Assessor,
		perfilcotista.Perfil as PerfilRiscoInvestidor, 
		CASE WHEN pessoaCotista.Tipo = 1 
			THEN 'Física'
		ELSE
			'Juridica'
		END AS TipoPessoa,
		OperacaoCotista.DataOperacao as DataUltimaMovimentacao, 
		Carteira.Apelido as FundoQueInveste, 		
		perfilfundo.Perfil as PerfilRiscoFundo, 
		'' AS PerfilRiscoPortifolioCliente, --Calculado em tempo de extra�ção do relatório
		case 
			when perfilcotista.nivel < perfilfundo.nivel then 'Sim' else 'Não' 
		end as CotistaDesenquadrado, 
		case 
			when (select count(1) 
				  from SuitabilityEventos,  
				  SuitabilityLogMensagens 
				  where SuitabilityEventos.IdMensagem = SuitabilityLogMensagens.IdMensagem 
					and SuitabilityEventos.IdSuitabilityEventos in (18, 19) 
					and SuitabilityLogMensagens.Resposta = 'OK') > 0 then 'Sim' else 'Não' 
		end as AssinouIncompatibilidadeDePerfilRisco, 
		case 
			when (	select count(1) 
					from SuitabilityEventos,  
						 SuitabilityLogMensagens 
					where SuitabilityEventos.IdMensagem = SuitabilityLogMensagens.IdMensagem 
						and SuitabilityEventos.IdSuitabilityEventos in (16,17) 
						and SuitabilityLogMensagens.Resposta = 'OK') > 0 then 'Sim' else 'Não' 
		end as AssinouTermoAdesao, 
		PessoaSuitability.UltimaAlteracao AS DataUltimaAlteracao, 
		case suitabilityParametrosWorkflow.GrandezaPeriodo			 
			when 1 then DATEADD(day, suitabilityParametrosWorkflow.ValorPeriodo,PessoaSuitability.UltimaAlteracao) 
			when 2 then DATEADD(day, (suitabilityParametrosWorkflow.ValorPeriodo * 30),PessoaSuitability.UltimaAlteracao) 
			when 3 then DATEADD(day, (suitabilityParametrosWorkflow.ValorPeriodo * 360),PessoaSuitability.UltimaAlteracao) 
		end as DataProximaRenovacao,
		Carteira.IdCarteira,
		pessoaCotista.Tipo
 from OperacaoCotista,  
 PessoaSuitability,  
 SuitabilityPerfilInvestidor as perfilcotista,  
 SuitabilityPerfilInvestidor as perfilfundo, 
 Carteira, 
 suitabilityParametrosWorkflow,
 Cotista,
 Pessoa pessoaCotista
 where OperacaoCotista.IdCotista = PessoaSuitability.IdPessoa 
	and PessoaSuitability.Perfil = perfilcotista.IdPerfilInvestidor 
	and OperacaoCotista.IdCarteira = Carteira.IdCarteira 
	and Carteira.PerfilRisco = perfilfundo.IdPerfilInvestidor 
	and Cotista.IdCotista = OperacaoCotista.IdCotista 
	and cotista.IdCotista = pessoaCotista.IdPessoa
	and operacaoCotista.TipoOperacao in (1,10,11,12);
GO

if not exists(select 1 from syscolumns where id = object_id('TabelaInformeDesempenho') and name = 'ValorAdministracaoPaga')
BEGIN
	ALTER TABLE TabelaInformeDesempenho add ValorAdministracaoPaga decimal (16,2) not null default 0.0
END

if not exists(select 1 from syscolumns where id = object_id('TabelaInformeDesempenho') and name = 'ValorPfeePaga')
BEGIN
	ALTER TABLE TabelaInformeDesempenho add ValorPfeePaga decimal (16,2) not null default 0.0
END
	
if not exists(select 1 from syscolumns where id = object_id('TabelaInformeDesempenho') and name = 'ValorCustodiaPaga')
BEGIN
	ALTER TABLE TabelaInformeDesempenho add ValorCustodiaPaga decimal (16,2) not null default 0.0
END
	
if not exists(select 1 from syscolumns where id = object_id('TabelaInformeDesempenho') and name = 'ValorOutrasDespesasPagas')
BEGIN
	ALTER TABLE TabelaInformeDesempenho add ValorOutrasDespesasPagas decimal (16,2) not null default 0.0
END
	
if not exists(select 1 from syscolumns where id = object_id('TabelaInformeDesempenho') and name = 'ValorAdministracaoPagaGrupoEconomicoADM')
BEGIN
	ALTER TABLE TabelaInformeDesempenho add ValorAdministracaoPagaGrupoEconomicoADM decimal (16,2) not null default 0.0
END
	
if not exists(select 1 from syscolumns where id = object_id('TabelaInformeDesempenho') and name = 'ValorDespesasOperacionaisGrupoEconomicoADM')
BEGIN
	ALTER TABLE TabelaInformeDesempenho add ValorDespesasOperacionaisGrupoEconomicoADM decimal (16,2) not null default 0.0
END

if not exists(select 1 from syscolumns where id = object_id('TabelaInformeDesempenho') and name = 'ValorAdministracaoPagaGrupoEconomicoGestor')
BEGIN
	ALTER TABLE TabelaInformeDesempenho add ValorAdministracaoPagaGrupoEconomicoGestor decimal (16,2) not null default 0.0
END
	
if not exists(select 1 from syscolumns where id = object_id('TabelaInformeDesempenho') and name = 'ValorDespesasOperacionaisGrupoEconomicoGestor')
BEGIN
	ALTER TABLE TabelaInformeDesempenho add ValorDespesasOperacionaisGrupoEconomicoGestor decimal (16,2) not null default 0.0
END

GO

alter table IRProventos alter column ValorDistribuido decimal(18,2) not null;
alter table IRProventos alter column ValorIR decimal(18,2) not null;

if not exists(select 1 from syscolumns where id = object_id('AgendaFundo') and name = 'TipoValorInput')
BEGIN
	ALTER TABLE AgendaFundo ADD TipoValorInput int NOT NULL default 1;
END

if not exists(select 1 from syscolumns where id = object_id('Carteira') and name = 'Rendimento')
BEGIN
	ALTER TABLE Carteira ADD Rendimento int NOT NULL default 1
END

if not exists(select 1 from syscolumns where id = object_id('HistoricoCota') and name = 'CotaEx')
BEGIN
	ALTER TABLE HistoricoCota ADD CotaEx decimal(28,10) NOT NULL default 0
END

if not exists(select 1 from syscolumns where id = object_id('HistoricoCota') and name = 'CotaRendimento')
BEGIN
	ALTER TABLE HistoricoCota ADD CotaRendimento decimal(28,10) NOT NULL default 0
END

ALTER TABLE IRProventos alter column Descricao varchar(200) not null	

if not exists(select 1 from syscolumns where id = object_id('IRProventos') and name = 'ValorLiquido')
BEGIN
	ALTER TABLE IRProventos ADD ValorLiquido decimal(18,2) NOT NULL default 0
END

if not exists(select 1 from syscolumns where id = object_id('ExcecoesTributacaoIR') and name = 'RegimeEspecialTributacao')
BEGIN
	ALTER TABLE ExcecoesTributacaoIR ADD RegimeEspecialTributacao char(1) NOT NULL default 'N'
END

if exists(select 1 from syscolumns where id = object_id('Pessoa') and name = 'Nome')
begin
	ALTER TABLE Pessoa ALTER COLUMN Nome VARCHAR(255) NOT NULL
END

if exists(select 1 from syscolumns where id = object_id('Pessoa') and name = 'Apelido')
begin
	ALTER TABLE Pessoa ALTER COLUMN Apelido VARCHAR(100) NOT NULL
END

if exists(select 1 from syscolumns where id = object_id('Carteira') and name = 'Nome')
begin	
	ALTER TABLE Carteira ALTER COLUMN Nome VARCHAR(255) NOT NULL
END

if exists(select 1 from syscolumns where id = object_id('Carteira') and name = 'Apelido')
begin
	ALTER TABLE Carteira ALTER COLUMN Apelido VARCHAR(100) NULL
END

if exists(select 1 from syscolumns where id = object_id('Cliente') and name = 'Nome')
begin
	ALTER TABLE Cliente ALTER COLUMN Nome VARCHAR(255) NOT NULL
END

if exists(select 1 from syscolumns where id = object_id('Cliente') and name = 'Apelido')
begin
	ALTER TABLE Cliente ALTER COLUMN Apelido VARCHAR(100) NOT NULL
END

if exists(select 1 from syscolumns where id = object_id('Cotista') and name = 'Nome')
begin
	ALTER TABLE Cotista ALTER COLUMN Nome VARCHAR(255) NOT NULL
END

if exists(select 1 from syscolumns where id = object_id('Cotista') and name = 'Apelido')
begin
	ALTER TABLE Cotista ALTER COLUMN Apelido VARCHAR(255) NOT NULL
END

IF EXISTS (select 1 from sysconstraints where object_name(id) = 'TabelaProvisao' and object_name(constid) = 'DF_TabelaProvisao_NumeroDiasPagamento')	
begin
	alter table TabelaProvisao drop constraint DF_TabelaProvisao_NumeroDiasPagamento
END
GO 

if exists(select 1 from syscolumns where id = object_id('TabelaProvisao') and name = 'NumeroDiasPagamento')
begin
	alter table TabelaProvisao alter column NumeroDiasPagamento smallint not null
END
GO

 IF NOT EXISTS (select 1 from sysconstraints where object_name(id) = 'TabelaProvisao' and object_name(constid) = 'DF_TabelaProvisao_NumeroDiasPagamento')	
begin
	alter table TabelaProvisao add constraint DF_TabelaProvisao_NumeroDiasPagamento default 0 for NumeroDiasPagamento
END
GO

if NOT exists(select 1 from syscolumns where id = object_id('CalculoResultado') and name = 'PL_SemResgAplicPfee')
begin
	if NOT exists(select 1 from syscolumns where id = object_id('CalculoResultado') and name = 'PL_CalculoPnL')
	begin
		ALTER TABLE CalculoResultado add PL_SemResgAplicPfee decimal(20,2) NOT NULL default('0')
	END
END

if exists(select 1 from syscolumns where id = object_id('CalculoResultado') and name = 'PL_SemResgAplicPfee')
begin
	EXEC sp_rename 'CalculoResultado.PL_SemResgAplicPfee', 'PL_CalculoPnL';
END

IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'PreProcessamento') = 0
begin
	CREATE TABLE dbo.PreProcessamento
	(
		IdPreProcessamento	INT				Primary key	identity,		
		Descricao			varchar(255)		NOT NULL,
		Pendencia			varchar(50)		NOT NULL,
		Data				DateTime		NOT NULL,
		Chave				INT				NOT NULL			
	)		
END	
GO	

IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'PreProcessamentoCliente') = 0
begin
	CREATE TABLE dbo.PreProcessamentoCliente
	(
		IdPreProcessamentoCliente	INT		Primary key	identity,		
		IdCliente			int				NOT NULL,
		Chave				INT				NOT NULL		
	)		
END	
GO	

if not exists(select 1 from syscolumns where id = object_id('PreProcessamentoCliente') and name = 'DataDia')
begin
	delete from PreProcessamento;
	delete from PreProcessamentoCliente;
	ALTER TABLE PreProcessamentoCliente ADD DataDia DateTime NOT NULL		
END

IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'PreProcessamentoHistoricoCota') = 0
begin
	CREATE TABLE dbo.PreProcessamentoHistoricoCota
	(
		IdPreProcessamentoHistoricoCota	INT				Primary key	identity,		
		IdCarteira 			int 			NOT NULL,
		Data				DateTime		NOT NULL,
		Chave				INT				NOT NULL			
	)		
END	
GO	

/*IF NOT EXISTS (SELECT name FROM sysindexes WHERE name = 'MemoriaCalculoRendaFixa_IdOperacao_Idx')
BEGIN
	CREATE INDEX MemoriaCalculoRendaFixa_IdOperacao_Idx ON MemoriaCalculoRendaFixa (IdOperacao);
END
GO*/

if not exists(select 1 from syscolumns where id = object_id('ExcecoesTributacaoIR') and name = 'DataInicio')
BEGIN
	ALTER TABLE ExcecoesTributacaoIR ADD DataInicio DateTime NOT NULL;
END
GO
		
if not exists(select 1 from syscolumns where id = object_id('ExcecoesTributacaoIR') and name = 'DataFim')
BEGIN		
	ALTER TABLE ExcecoesTributacaoIR ADD DataFim DateTime NOT NULL;
END
GO

if not exists(select 1 from syscolumns where id = object_id('ExcecoesTributacaoIR') and name = 'RegimeEspecialTributacao')
BEGIN
	ALTER TABLE ExcecoesTributacaoIR ADD RegimeEspecialTributacao char(1) NOT NULL default 'N'
END
GO
	
IF NOT EXISTS(select 1 from syscolumns where id = object_id('OperacaoRendaFixa') and name = 'IdConta')
BEGIN
	alter table OperacaoRendaFixa add IdConta int null;

	ALTER TABLE OperacaoRendaFixa ADD CONSTRAINT FK_OperacaoRendaFixa_ContaCorrente FOREIGN KEY(IdConta) 
	REFERENCES ContaCorrente(IdConta) ;
END


IF NOT EXISTS(select 1 from syscolumns where id = object_id('OperacaoBolsa') and name = 'IdConta')
BEGIN
	alter table OperacaoBolsa add IdConta int null;

	ALTER TABLE OperacaoBolsa ADD CONSTRAINT FK_OperacaoBolsa_ContaCorrente FOREIGN KEY(IdConta) 
	REFERENCES ContaCorrente(IdConta) ;
END

IF NOT EXISTS(select 1 from syscolumns where id = object_id('OperacaoBmf') and name = 'IdConta')
BEGIN
	alter table OperacaoBmf add IdConta int null;

	ALTER TABLE OperacaoBmf ADD CONSTRAINT FK_OperacaoBmf_ContaCorrente FOREIGN KEY(IdConta) 
	REFERENCES ContaCorrente(IdConta) ;
END

IF NOT EXISTS(select 1 from syscolumns where id = object_id('OperacaoSwap') and name = 'IdConta')
BEGIN
	alter table OperacaoSwap add IdConta int null;

	ALTER TABLE OperacaoSwap ADD CONSTRAINT FK_OperacaoSwap_ContaCorrente FOREIGN KEY(IdConta) 
	REFERENCES ContaCorrente(IdConta) ;
END


IF NOT EXISTS(select 1 from syscolumns where id = object_id('PosicaoBMF') and name = 'IdConta')
BEGIN
	alter table PosicaoBMF add IdConta int null;

	ALTER TABLE PosicaoBMF ADD CONSTRAINT FK_PosicaoBMF_ContaCorrente FOREIGN KEY(IdConta) 
	REFERENCES ContaCorrente(IdConta) ;
END

IF NOT EXISTS(select 1 from syscolumns where id = object_id('PosicaoBMFAbertura') and name = 'IdConta')
BEGIN
	alter table PosicaoBMFAbertura add IdConta int null;

	ALTER TABLE PosicaoBMFAbertura ADD CONSTRAINT FK_PosicaoBMFAbertura_ContaCorrente FOREIGN KEY(IdConta) 
	REFERENCES ContaCorrente(IdConta) ;
END

IF NOT EXISTS(select 1 from syscolumns where id = object_id('PosicaoBMFHistorico') and name = 'IdConta')
BEGIN
	alter table PosicaoBMFHistorico add IdConta int null;

	ALTER TABLE PosicaoBMFHistorico ADD CONSTRAINT FK_PosicaoBMFHistoricop_ContaCorrente FOREIGN KEY(IdConta) 
	REFERENCES ContaCorrente(IdConta) ;
END

IF NOT EXISTS(select 1 from syscolumns where id = object_id('CustodiaBMF') and name = 'IdConta')
BEGIN
	alter table CustodiaBMF add IdConta int null;

	ALTER TABLE CustodiaBMF ADD CONSTRAINT FK_CustodiaBMF_ContaCorrente FOREIGN KEY(IdConta) 
	REFERENCES ContaCorrente(IdConta) ;
END

--Merge - 04/03/2016
alter table OrdemBMF alter column TipoOrdem char(2) not null;

IF NOT EXISTS(select 1 from syscolumns where id = object_id('OperacaoRendaFixa') and name = 'IdConta')
BEGIN
	alter table OperacaoRendaFixa add IdConta int null;

	ALTER TABLE OperacaoRendaFixa ADD CONSTRAINT FK_OperacaoRendaFixa_ContaCorrente FOREIGN KEY(IdConta) 
	REFERENCES ContaCorrente(IdConta) ;
END


IF NOT EXISTS(select 1 from syscolumns where id = object_id('OperacaoBolsa') and name = 'IdConta')
BEGIN
	alter table OperacaoBolsa add IdConta int null;

	ALTER TABLE OperacaoBolsa ADD CONSTRAINT FK_OperacaoBolsa_ContaCorrente FOREIGN KEY(IdConta) 
	REFERENCES ContaCorrente(IdConta) ;
END

IF NOT EXISTS(select 1 from syscolumns where id = object_id('OperacaoBmf') and name = 'IdConta')
BEGIN
	alter table OperacaoBmf add IdConta int null;

	ALTER TABLE OperacaoBmf ADD CONSTRAINT FK_OperacaoBmf_ContaCorrente FOREIGN KEY(IdConta) 
	REFERENCES ContaCorrente(IdConta) ;
END

IF NOT EXISTS(select 1 from syscolumns where id = object_id('OperacaoSwap') and name = 'IdConta')
BEGIN
	alter table OperacaoSwap add IdConta int null;

	ALTER TABLE OperacaoSwap ADD CONSTRAINT FK_OperacaoSwap_ContaCorrente FOREIGN KEY(IdConta) 
	REFERENCES ContaCorrente(IdConta) ;
END


IF NOT EXISTS(select 1 from syscolumns where id = object_id('PosicaoBMF') and name = 'IdConta')
BEGIN
	alter table PosicaoBMF add IdConta int null;

	ALTER TABLE PosicaoBMF ADD CONSTRAINT FK_PosicaoBMF_ContaCorrente FOREIGN KEY(IdConta) 
	REFERENCES ContaCorrente(IdConta) ;
END

IF NOT EXISTS(select 1 from syscolumns where id = object_id('PosicaoBMFAbertura') and name = 'IdConta')
BEGIN
	alter table PosicaoBMFAbertura add IdConta int null;

	ALTER TABLE PosicaoBMFAbertura ADD CONSTRAINT FK_PosicaoBMFAbertura_ContaCorrente FOREIGN KEY(IdConta) 
	REFERENCES ContaCorrente(IdConta) ;
END

IF NOT EXISTS(select 1 from syscolumns where id = object_id('PosicaoBMFHistorico') and name = 'IdConta')
BEGIN
	alter table PosicaoBMFHistorico add IdConta int null;

	ALTER TABLE PosicaoBMFHistorico ADD CONSTRAINT FK_PosicaoBMFHistoricop_ContaCorrente FOREIGN KEY(IdConta) 
	REFERENCES ContaCorrente(IdConta) ;
END

IF NOT EXISTS(select 1 from syscolumns where id = object_id('CustodiaBMF') and name = 'IdConta')
BEGIN
	alter table CustodiaBMF add IdConta int null;

	ALTER TABLE CustodiaBMF ADD CONSTRAINT FK_CustodiaBMF_ContaCorrente FOREIGN KEY(IdConta) 
	REFERENCES ContaCorrente(IdConta) ;
END

if (not exists (select * from sys.all_columns where name = 'ExportaGalgo' and OBJECT_NAME(object_id) = 'Carteira'))
begin
	ALTER TABLE Carteira ADD  ExportaGalgo char(1) not null default 'N'
end

if(not exists(select * from sysobjects where id = object_id('HistoricoCotaGalgo')))
begin
	
	CREATE TABLE [dbo].[HistoricoCotaGalgo](
		[Data] [datetime] NOT NULL,
		[IdCarteira] [int] NOT NULL,
		[Cota] [decimal](28, 12) NOT NULL,
		[PL] [decimal](16, 2) NOT NULL,
		[DataImportacao] [datetime] NOT NULL,
		[UsuarioImportacao] [varchar](50) NOT NULL, 
		[Aprovar] [int] NOT NULL, 
		[DataAprovacao]  [datetime] NULL,
		[UsuarioAprovacao]  [varchar](50) NULL, 
	 CONSTRAINT [HistoricoCotaGalgo_PK] PRIMARY KEY CLUSTERED 
	(
		[Data] ASC,
		[IdCarteira] ASC
	)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
	) ON [PRIMARY]


	ALTER TABLE [dbo].[HistoricoCotaGalgo]  WITH CHECK ADD  CONSTRAINT [Carteira_HistoricoCotaGalgo_FK1] FOREIGN KEY([IdCarteira])
	REFERENCES [dbo].[Carteira] ([IdCarteira])

end
	
if exists(select 1 from syscolumns where id = object_id('CadastroComplementar') and name = 'ValorCampo' and length < 255)
begin
	ALTER TABLE CadastroComplementar ALTER COLUMN ValorCampo VARCHAR(255) NOT NULL
END

IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'CategoriaMovimentacao') = 0
begin
	CREATE TABLE dbo.CategoriaMovimentacao
	(
		IdCategoriaMovimentacao	INT				NOT NULL	Primary Key Identity,
		CodigoCategoria			varchar(20)		NOT NULL,
		Descricao				varchar(100)	NULL		
	)	
END	


if (not exists (select * from syscolumns where name = 'IdCategoriaMovimentacao' and id = object_id('OperacaoBMF')))
begin
	ALTER TABLE OperacaoBMF ADD IdCategoriaMovimentacao INT null;	
	ALTER TABLE OperacaoBMF	ADD CONSTRAINT OperacaoBMF_CatMovimentacao_FK1 FOREIGN KEY (IdCategoriaMovimentacao) REFERENCES CategoriaMovimentacao(IdCategoriaMovimentacao) on delete SET NULL;
end


if (not exists (select * from syscolumns where name = 'IdCategoriaMovimentacao' and id = object_id('OperacaoRendaFixa')))
begin
	ALTER TABLE OperacaoRendaFixa ADD IdCategoriaMovimentacao INT null;	
	ALTER TABLE OperacaoRendaFixa ADD CONSTRAINT OperacaoRF_CatMovimentacao_FK1 FOREIGN KEY (IdCategoriaMovimentacao) REFERENCES CategoriaMovimentacao(IdCategoriaMovimentacao) on delete SET NULL;
end

if (not exists (select * from syscolumns where name = 'IdCategoriaMovimentacao' and id = object_id('OperacaoSwap')))
begin
	ALTER TABLE OperacaoSwap ADD IdCategoriaMovimentacao INT null;	
	ALTER TABLE OperacaoSwap ADD CONSTRAINT OperacaoSwap_CatMovimentacao_FK1 FOREIGN KEY (IdCategoriaMovimentacao) REFERENCES CategoriaMovimentacao(IdCategoriaMovimentacao) on delete SET NULL;
end

if (not exists (select * from syscolumns where name = 'IdCategoriaMovimentacao' and id = object_id('OperacaoBolsa')))
begin
	ALTER TABLE OperacaoBolsa ADD IdCategoriaMovimentacao INT null;	
	ALTER TABLE OperacaoBolsa ADD CONSTRAINT OperacaoBolsa_CatMovimentacao_FK1 FOREIGN KEY (IdCategoriaMovimentacao) REFERENCES CategoriaMovimentacao(IdCategoriaMovimentacao) on delete SET NULL;
end

if (not exists (select * from syscolumns where name = 'IdCategoriaMovimentacao' and id = object_id('OperacaoFundo')))
begin
	ALTER TABLE OperacaoFundo ADD IdCategoriaMovimentacao INT null;	
	ALTER TABLE OperacaoFundo ADD CONSTRAINT OperacaoFundo_CatMovimentacao_FK1 FOREIGN KEY (IdCategoriaMovimentacao) REFERENCES CategoriaMovimentacao(IdCategoriaMovimentacao) on delete SET NULL;
end

if (not exists (select * from syscolumns where name = 'IdCategoriaMovimentacao' and id = object_id('OperacaoFundoAux')))
begin
	ALTER TABLE OperacaoFundoAux ADD IdCategoriaMovimentacao INT null;	
	ALTER TABLE OperacaoFundoAux ADD CONSTRAINT OperacaoFundoAux_CatMovimentacao_FK1 FOREIGN KEY (IdCategoriaMovimentacao) REFERENCES CategoriaMovimentacao(IdCategoriaMovimentacao) on delete SET NULL;
end

if (not exists (select * from syscolumns where name = 'IdCategoriaMovimentacao' and id = object_id('OrdemBMF')))
begin
	ALTER TABLE OrdemBMF ADD IdCategoriaMovimentacao INT null;	
	ALTER TABLE OrdemBMF ADD CONSTRAINT OrdemBMF_CatMovimentacao_FK1 FOREIGN KEY (IdCategoriaMovimentacao) REFERENCES CategoriaMovimentacao(IdCategoriaMovimentacao) on delete SET NULL;
end


if (not exists (select * from syscolumns where name = 'IdCategoriaMovimentacao' and id = object_id('OrdemRendaFixa')))
begin
	ALTER TABLE OrdemRendaFixa ADD IdCategoriaMovimentacao INT null;	
	ALTER TABLE OrdemRendaFixa ADD CONSTRAINT OrdemRF_CatMovimentacao_FK1 FOREIGN KEY (IdCategoriaMovimentacao) REFERENCES CategoriaMovimentacao(IdCategoriaMovimentacao) on delete SET NULL;
end


if (not exists (select * from syscolumns where name = 'IdCategoriaMovimentacao' and id = object_id('OrdemBolsa')))
begin
	ALTER TABLE OrdemBolsa ADD IdCategoriaMovimentacao INT null;	
	ALTER TABLE OrdemBolsa ADD CONSTRAINT OrdemBolsa_CatMovimentacao_FK1 FOREIGN KEY (IdCategoriaMovimentacao) REFERENCES CategoriaMovimentacao(IdCategoriaMovimentacao) on delete SET NULL;
end

if (not exists (select * from syscolumns where name = 'IdCategoriaMovimentacao' and id = object_id('OrdemFundo')))
begin
	ALTER TABLE OrdemFundo ADD IdCategoriaMovimentacao INT null;	
	ALTER TABLE OrdemFundo ADD CONSTRAINT OrdemFundo_CatMovimentacao_FK1 FOREIGN KEY (IdCategoriaMovimentacao) REFERENCES CategoriaMovimentacao(IdCategoriaMovimentacao) on delete SET NULL;
end

if (select count(1) from CadastroComplementarCampos where TipoCadastro = -8 and NomeCampo = 'GrupoEconomicoLamina') = 0
begin
	INSERT INTO CadastroComplementarCampos (TipoCadastro, NomeCampo, DescricaoCampo, ValorDefault, TipoCampo, CampoObrigatorio, Tamanho, CasasDecimais)
	VALUES(-8, 'GrupoEconomicoLamina', 'Grupo economico no qual a carteira faz parte', null, -2, 'N', 40 /*Alterar caso o limite de char no campo seja menor*/, 0)
END	
	
if (select count(1) from CadastroComplementarCampos where TipoCadastro = -8 and NomeCampo = 'NomeFundoLamina') = 0
begin
	INSERT INTO CadastroComplementarCampos (TipoCadastro, NomeCampo, DescricaoCampo, ValorDefault, TipoCampo, CampoObrigatorio, Tamanho, CasasDecimais)
	VALUES(-8, 'NomeFundoLamina', 'Grupo economico no qual a carteira faz parte', null, -2, 'N', 255 /*Alterar caso o limite de char no campo seja menor*/, 0)	
END	

if (select count(1) from CadastroComplementarCampos where TipoCadastro = -8 and NomeCampo = 'ApelidoReduzidoRentMensal') = 0
begin
	INSERT INTO CadastroComplementarCampos (TipoCadastro, NomeCampo, DescricaoCampo, ValorDefault, TipoCampo, CampoObrigatorio, Tamanho, CasasDecimais)
	VALUES(-8, 'ApelidoReduzidoRentMensal', 'Apelido reduzido que será usado no relatório de rent.mensal', null, -2, 'N', 40 /*Alterar caso o limite de char no campo seja menor*/, 0)	
END		

IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'Rentabilidade') = 0
begin
	CREATE TABLE [dbo].[Rentabilidade]
	(
		[IdRentabilidade] [int] IDENTITY(1,1) NOT NULL,
		[Data] [datetime] NOT NULL,
		[TipoAtivo] [int] NOT NULL,
		[IdCliente] [int] NOT NULL,
		[CodigoAtivo] [varchar](255) NULL,
		[CodigoMoedaAtivo] [varchar](50) NULL,
		[PatrimonioInicialMoedaAtivo] [decimal](25, 12) NULL,
		[QuantidadeInicialAtivo] [decimal](25, 12) NULL,
		[ValorFinanceiroEntradaMoedaAtivo] [decimal](25, 2) NULL,
		[QuantidadeTotalEntradaAtivo] [decimal](25, 12) NULL,
		[ValorFinanceiroSaidaMoedaAtivo] [decimal](25, 2) NULL,
		[QuantidadeTotalSaidaAtivo] [decimal](25, 12) NULL,
		[ValorFinanceiroRendasMoedaAtivo] [decimal](25, 2) NULL,
		[PatrimonioFinalMoedaAtivo] [decimal](25, 12) NULL,
		[QuantidadeFinalAtivo] [decimal](25, 12) NULL,
		[RentabilidadeMoedaAtivo] [decimal](25, 12) NULL,
		[CodigoMoedaPortfolio] [varchar](10) NULL,
		[PatrimonioInicialMoedaPortfolio] [decimal](25, 12) NULL,
		[ValorFinanceiroEntradaMoedaPortfolio] [decimal](25, 2) NULL,
		[ValorFinanceiroSaidaMoedaPortfolio] [decimal](25, 2) NULL,
		[ValorFinanceiroRendasMoedaPortfolio] [decimal](25, 2) NULL,
		[PatrimonioFinalMoedaPortfolio] [decimal](25, 2) NULL,
		[RentabilidadeMoedaPortfolio] [decimal](25, 12) NULL,
		[IdTituloRendaFixa] [int] NULL,
		[IdOperacaoSwap] [int] NULL,
		[IdCarteira] [int] NULL,
		[CdAtivoBolsa] [varchar](20) NULL,
		[CdAtivoBMF] [varchar](20) NULL,
		[SerieBMF] [varchar](5) NULL,
		[PatrimonioInicialBrutoMoedaPortfolio] [decimal](28, 2) NULL,
		[PatrimonioInicialBrutoMoedaAtivo] [decimal](28, 2) NULL,
		[PatrimonioFinalBrutoMoedaPortfolio] [decimal](28, 2) NULL,
		[PatrimonioFinalBrutoMoedaAtivo] [decimal](28, 2) NULL,
		[CotaGerencial] [decimal](28, 12) NULL,
		[RentabilidadeBrutaDiariaMoedaAtivo] [decimal](25, 12) NULL,
		[RentabilidadeBrutaAcumMoedaAtivo] [decimal](25, 12) NULL,
		[RentabilidadeBrutaDiariaMoedaPortfolio] [decimal](25, 12) NULL,
		[RentabilidadeBrutaAcumMoedaPortfolio] [decimal](25, 12) NULL,
		[PatrimonioInicialGrossUpMoedaAtivo] [decimal](28, 2) NULL,
		[PatrimonioInicialGrossUpMoedaPortfolio] [decimal](28, 2) NULL,
		[PatrimonioFinalGrossUpMoedaAtivo] [decimal](28, 2) NULL,
		[PatrimonioFinalGrossUpMoedaPortfolio] [decimal](28, 2) NULL,
		[RentabilidadeAtivosIcentivados] [decimal](25, 12) NULL,
		[RentabilidadeAtivosTributados] [decimal](25, 12) NULL,
		[RentabilidadeGrossUpDiariaMoedaAtivo] [decimal](25, 12) NULL,
		[RentabilidadeGrossUpAcumMoedaAtivo] [decimal](25, 12) NULL,
		[RentabilidadeGrossUpDiariaMoedaPortfolio] [decimal](25, 12) NULL,
		[RentabilidadeGrossUpAcumMoedaPortfolio] [decimal](25, 12) NULL,
	PRIMARY KEY CLUSTERED 
	(
		[IdRentabilidade] ASC
	)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
	) ON [PRIMARY]
END	


IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'AtivoMercadoFiltro') = 0
Begin
	Create Table dbo.AtivoMercadoFiltro
	(
			IdAtivoMercadoFiltro INT 	PRIMARY KEY identity NOT NULL, 
			CompositeKey	VARCHAR(50),
			IdAtivo			VARCHAR(50),
			Descricao		VARCHAR(255),
			IdCliente		INT,
			TipoMercado		INT
	)
END	


if not exists(select 1 from syscolumns where id = object_id('PosicaoRendaFixa') and name = 'ValorBrutoGrossUp')
BEGIN
	ALTER TABLE PosicaoRendaFixa ADD ValorBrutoGrossUp decimal(28,10) NOT NULL default 0;
END


if not exists(select 1 from syscolumns where id = object_id('PosicaoRendaFixaHistorico') and name = 'ValorBrutoGrossUp')
BEGIN
	ALTER TABLE PosicaoRendaFixaHistorico ADD ValorBrutoGrossUp decimal(28,10) NOT NULL default 0;
END


if not exists(select 1 from syscolumns where id = object_id('PosicaoRendaFixaAbertura') and name = 'ValorBrutoGrossUp')
BEGIN
	ALTER TABLE PosicaoRendaFixaAbertura ADD ValorBrutoGrossUp decimal(28,10) NOT NULL default 0;
END

	
if not exists(select 1 from syscolumns where id = object_id('CalculoAdministracao') and name = 'ValorBase')
BEGIN
	ALTER TABLE CalculoAdministracao ADD ValorBase DECIMAL(16,2) NULL
END 


if not exists(select 1 from syscolumns where id = object_id('CalculoAdministracaoHistorico') and name = 'ValorBase')
BEGIN
	ALTER TABLE CalculoAdministracaoHistorico ADD ValorBase DECIMAL(16,2) NULL
END


if exists(select * from sys.triggers where name = 'trg_Pessoa_DataUltimaAlteracao')
begin
	drop trigger trg_Pessoa_DataUltimaAlteracao
end

IF NOT EXISTS(select name from sys.indexes where object_id = object_id('PosicaoCotista') AND name = 'IX_PosicaoCotista_IdCarteira')
BEGIN 
	CREATE NONCLUSTERED INDEX [IX_PosicaoCotista_IdCarteira]
	ON [dbo].PosicaoCotista([IdCarteira] ASC)
END

IF NOT EXISTS(select name from sys.indexes where object_id = object_id('PosicaoFundo') AND name = 'IX_PosicaoFundo_IdCliente')
BEGIN
	CREATE NONCLUSTERED INDEX [IX_PosicaoFundo_IdCliente]
	ON [dbo].PosicaoFundo([IdCliente] ASC)
END

if exists(select 1 from syscolumns where id = object_id('CadastroComplementar') and name = 'ValorCampo')
begin
	ALTER TABLE CadastroComplementar ALTER COLUMN ValorCampo VARCHAR(500) NOT NULL
END

IF NOT EXISTS (SELECT name FROM sysindexes WHERE name = 'MediaMovel_IDX')
BEGIN
	CREATE NONCLUSTERED INDEX MediaMovel_IDX
	ON [dbo].[MediaMovel] ([DataAtual],[TipoMediaMovel])
	INCLUDE ([IdMediaMovel],[IdCliente],[MediaMovel],[idPosicao],[ChaveComposta])
END

IF NOT EXISTS (SELECT name FROM sysindexes WHERE name = 'PosicaoRendaFixaAbertura_IDX')
BEGIN
	CREATE NONCLUSTERED INDEX PosicaoRendaFixaAbertura_IDX
	ON [dbo].[PosicaoRendaFixaAbertura] ([IdCliente],[DataHistorico])
	INCLUDE ([IdPosicao],[IdTitulo],[TipoOperacao],[DataVencimento],[Quantidade],[QuantidadeBloqueada],[DataOperacao],[DataLiquidacao],[PUOperacao],[PUCurva],[ValorCurva],[PUMercado],[ValorMercado],[PUJuros],[ValorJuros],[DataVolta],[TaxaVolta],[PUVolta],[ValorVolta],[QuantidadeInicial],[ValorIR],[ValorIOF],[TipoNegociacao],[PUCorrecao],[ValorCorrecao],[TaxaOperacao],[IdAgente],[IdCustodia],[CustoCustodia],[IdIndiceVolta],[IdOperacao],[OperacaoTermo],[ValorBrutoGrossUp])
END

IF EXISTS (select 1 from sysconstraints where object_name(id) = 'MemoriaCalculoRendaFixa' and object_name(constid) = 'PK_MemoriaCalculoRendaFixa')	
begin
	ALTER TABLE MemoriaCalculoRendaFixa DROP CONSTRAINT PK_MemoriaCalculoRendaFixa
end
go

IF EXISTS (SELECT name FROM sysindexes WHERE name = 'MemoriaCalculoRendaFixa_IdOperacao_Idx' and object_name(id) = 'MemoriaCalculoRendaFixa')
BEGIN
	DROP INDEX MemoriaCalculoRendaFixa_IdOperacao_Idx ON MemoriaCalculoRendaFixa 
END
GO	

IF NOT EXISTS(select 1 from syscolumns where id = object_id('MemoriaCalculoRendaFixa') and name = 'IdMemoriaCalculoRendaFixa')
BEGIN
	CREATE TABLE dbo.MemoriaCalculoRendaFixaAux
	(
		DataAtual 		 		 DateTime		NOT NULL,
		IdOperacao		 		 INT 			NOT NULL,
		TipoPreco 		 		 INT 			NOT NULL,
		TipoProcessamento		 INT 			NOT NULL,
		DataFluxo		 		 DateTime 		NOT NULL,
		TaxaDesconto	 		 Decimal(25,12)	NULL,
		ValorVencimento	 		 Decimal(25,8)	NULL,
		ValorPresente	 		 Decimal(25,8)	NULL,		
		ValorNominalAjustado	 Decimal(25,8)	NULL,
		ValorNominal			 Decimal(25,8)	NULL,
		TipoEventoTitulo		 INT	
	);
	
	EXEC('INSERT INTO MemoriaCalculoRendaFixaAux SELECT * FROM MemoriaCalculoRendaFixa;');
		
	DROP TABLE MemoriaCalculoRendaFixa;
	
	CREATE TABLE dbo.MemoriaCalculoRendaFixa
	(
		IdMemoriaCalculoRendaFixa				INT 			PRIMARY KEY nonclustered identity   NOT NULL , 
		IdCliente								INT				NOT NULL,
		DataAtual 		 		 				DateTime		NOT NULL,
		IdOperacao		 		 				INT 			NOT NULL,
		TipoPreco 		 		 				INT 			NOT NULL,
		TipoProcessamento		 				INT 			NOT NULL,
		DataFluxo		 		 				DateTime 		NOT NULL,
		TaxaDesconto	 		 				Decimal(25,12)	NULL,
		ValorVencimento	 		 				Decimal(25,8)	NULL,
		ValorPresente	 		 				Decimal(25,8)	NULL,		
		ValorNominalAjustado	 				Decimal(25,8)	NULL,
		ValorNominal			 				Decimal(25,8)	NULL,
		TipoEventoTitulo						INT				NOT NULL
	);
	
	ALTER TABLE MemoriaCalculoRendaFixa ADD CONSTRAINT FK_MemCalcOpRF FOREIGN KEY(IdOperacao) REFERENCES OperacaoRendaFixa(IdOperacao) ON DELETE CASCADE;
	ALTER TABLE MemoriaCalculoRendaFixa ADD CONSTRAINT FK_MemCalcCliente FOREIGN KEY(IdCliente) REFERENCES Cliente(IdCliente);
	
	INSERT INTO MemoriaCalculoRendaFixa 
	SELECT 
		op.IdCliente,
		DataAtual, 		 		 		
		op.IdOperacao,	 		 		
		TipoPreco,	 		 		
		TipoProcessamento,	 		
		DataFluxo,		 		 		
		TaxaDesconto,	 		 		
		ValorVencimento,		 		
		ValorPresente,		 		
		ValorNominalAjustado,	 		
		ValorNominal,		 		
		TipoEventoTitulo				
	FROM MemoriaCalculoRendaFixaAux mem
	INNER JOIN OperacaoRendaFixa op ON op.IdOperacao = mem.IdOperacao
	
	DROP TABLE MemoriaCalculoRendaFixaAux;		
END	
GO

IF NOT EXISTS (SELECT name FROM sysindexes WHERE name = 'MemoriaCalculoRendaFixa_IdClienteData_Idx')
BEGIN
	CREATE INDEX MemoriaCalculoRendaFixa_IdClienteData_Idx ON MemoriaCalculoRendaFixa (IdCliente, DataAtual);	
END
GO

-- Se o index abaixo nÃ£o foi criado, significa que o refactoring dos index nÃ£o foi executado
IF NOT EXISTS (SELECT name FROM sysindexes WHERE name = 'PerfilMTM_Data_Papel_Grupo_Idx')
BEGIN
	CREATE NONCLUSTERED INDEX PerfilMTM_Data_Papel_Grupo_Idx ON PerfilMTM (DtReferencia, IdPapel, IdGrupoPerfilMTM);
	

END
GO

IF NOT EXISTS (SELECT name FROM sysindexes WHERE name = 'LiquidacaoFuturo_Data_IdCliente_Idx')
BEGIN
	CREATE NONCLUSTERED INDEX LiquidacaoFuturo_Data_IdCliente_Idx ON LiquidacaoFuturo (DataHistorico, IdCliente)
END	

IF NOT EXISTS (SELECT name FROM sysindexes WHERE name = 'LiquidacaoHistorico_Data_IdCliente_Idx')
BEGIN
	CREATE NONCLUSTERED INDEX LiquidacaoHistorico_Data_IdCliente_Idx ON LiquidacaoHistorico (DataHistorico, IdCliente)
END

IF NOT EXISTS (SELECT name FROM sysindexes WHERE name = 'LiquidacaoAbertura_Data_IdCliente_Idx')
BEGIN
	CREATE NONCLUSTERED INDEX LiquidacaoAbertura_Data_IdCliente_Idx ON LiquidacaoAbertura (DataHistorico, IdCliente)
END
			
IF NOT EXISTS (SELECT name FROM sysindexes WHERE name = 'MediaMovel_Data_Cliente_Idx')
BEGIN
	CREATE NONCLUSTERED INDEX MediaMovel_Data_Cliente_Idx ON MediaMovel (DataAtual, IdCliente);
END
GO

IF NOT EXISTS (SELECT name FROM sysindexes WHERE name = 'CalculoAdminHist_Data_Carteira_PK')
BEGIN
	CREATE NONCLUSTERED INDEX CalculoAdminHist_Data_Carteira_PK ON CalculoAdministracaoHistorico (DataHistorico, IdCarteira)
END
GO
			
IF NOT EXISTS (SELECT name FROM sysindexes WHERE name = 'PosicaoBMF_IdCliente_Idx')
BEGIN
	CREATE NONCLUSTERED INDEX PosicaoBMF_IdCliente_Idx ON PosicaoBMF (IdCliente);
END	

IF NOT EXISTS (SELECT name FROM sysindexes WHERE name = 'PosicaoBolsa_IdCliente_Idx')
BEGIN
	CREATE NONCLUSTERED INDEX PosicaoBolsa_IdCliente_Idx ON PosicaoBolsa (IdCliente);
END	
	
IF NOT EXISTS (SELECT name FROM sysindexes WHERE name = 'PosicaoCotista_IdCarteira_Idx')
BEGIN
	CREATE NONCLUSTERED INDEX PosicaoCotista_IdCarteira_Idx ON PosicaoCotista (IdCarteira);
END		

IF NOT EXISTS (SELECT name FROM sysindexes WHERE name = 'PosicaoRendaFixa_IdCliente_Idx')
BEGIN
	CREATE NONCLUSTERED INDEX PosicaoRendaFixa_IdCliente_Idx ON PosicaoRendaFixa (IdCliente);
END		

IF NOT EXISTS (SELECT name FROM sysindexes WHERE name = 'PosicaoFundo_IdCliente_Idx')
BEGIN
	CREATE NONCLUSTERED INDEX PosicaoFundo_IdCliente_Idx ON PosicaoFundo (IdCliente);
END
					
IF NOT EXISTS (SELECT name FROM sysindexes WHERE name = 'AgendaComeCotas_Posicao_Data_Idx')
BEGIN
	CREATE NONCLUSTERED INDEX AgendaComeCotas_Posicao_Data_Idx ON AgendaComeCotas (idPosicao, DataLancamento);
END			

IF NOT EXISTS (SELECT name FROM sysindexes WHERE name = 'DesenquadramentoTributario_Data_Carteira_Idx')
BEGIN
	CREATE NONCLUSTERED INDEX DesenquadramentoTributario_Data_Carteira_Idx ON DesenquadramentoTributario (Data, IdCarteira);
END

IF NOT EXISTS (SELECT name FROM sysindexes WHERE name = 'PosicaoCotista_Carteira_Cotista_Idx')
BEGIN
	CREATE NONCLUSTERED INDEX PosicaoCotista_Carteira_Cotista_Idx ON PosicaoCotista (IdCarteira, IdCotista);
END

IF NOT EXISTS(select 1 from syscolumns where id = object_id('PosicaoCotistaAlterada') and name = 'IdCarteira')
BEGIN
	alter table PosicaoCotistaAlterada add IdCarteira int null;

	EXEC('UPDATE posAlterada SET posAlterada.IdCarteira = (CASE WHEN posAbeCot.IdCarteira is null THEN  isnull(posCot.IdCarteira,posHisCot.IdCarteira) ELSE posAbeCot.IdCarteira END)
	from PosicaoCotistaAlterada posAlterada
	LEFT OUTER JOIN PosicaoCotista posCot ON posAlterada.IdPosicao = posCot.IdPosicao
	LEFT OUTER JOIN PosicaoCotistaHistorico posHisCot ON isnull(posCot.IdPosicao,posAlterada.idPosicao) = posHisCot.IdPosicao and posAlterada.Data = posHisCot.DataHistorico
	LEFT OUTER JOIN PosicaoCotistaAbertura posAbeCot ON isnull(posCot.IdPosicao,posAlterada.idPosicao) = posAbeCot.IdPosicao and posAlterada.Data = posAbeCot.DataHistorico')

	EXEC('DELETE FROM PosicaoCotistaAlterada WHERE IdCarteira is null');

	alter table PosicaoCotistaAlterada alter column IdCarteira int null;

	ALTER TABLE PosicaoCotistaAlterada ADD CONSTRAINT FK_PosicaoCotistaAlterada_Carteira FOREIGN KEY(IdCarteira) REFERENCES Carteira(IdCarteira) ;
	
	CREATE NONCLUSTERED INDEX PosicaoCotistaAlterada_Data_Carteira_Idx ON PosicaoCotistaAlterada (Data, IdCarteira);
END

if not exists(select 1 from syscolumns where id = object_id('PosicaoCotista') and name = 'AmortizacaoAcumuladaPorCota')
BEGIN
	ALTER TABLE PosicaoCotista ADD AmortizacaoAcumuladaPorCota decimal(28,10) NOT NULL DEFAULT('0') ;
	
	EXEC('UPDATE PosicaoCotista SET AmortizacaoAcumuladaPorCota = 
	(select isnull(sum(agenda.Valor),0)
	from AgendaFundo agenda 
	inner join Cliente cli on (cli.IdCliente = agenda.IdCarteira) 
	where agenda.IdCarteira = posicaoCotista.IdCarteira and agenda.DataEvento >= PosicaoCotista.DataAplicacao and agenda.DataEvento <= cli.dataDia and agenda.TipoEvento = 2)');
END
GO

if not exists(select 1 from syscolumns where id = object_id('PosicaoCotistaHistorico') and name = 'AmortizacaoAcumuladaPorCota')
BEGIN
	ALTER TABLE PosicaoCotistaHistorico ADD AmortizacaoAcumuladaPorCota decimal(28,10) NOT NULL DEFAULT('0') ;
	
	EXEC('UPDATE PosicaoCotistaHistorico SET AmortizacaoAcumuladaPorCota = 
	(select isnull(sum(agenda.Valor),0)
	from AgendaFundo agenda 
	where agenda.IdCarteira = PosicaoCotistaHistorico.IdCarteira and agenda.DataEvento >= PosicaoCotistaHistorico.DataAplicacao and agenda.DataEvento <= PosicaoCotistaHistorico.DataHistorico and agenda.TipoEvento = 2)');
END
GO

if not exists(select 1 from syscolumns where id = object_id('PosicaoCotistaAbertura') and name = 'AmortizacaoAcumuladaPorCota')
BEGIN
	ALTER TABLE PosicaoCotistaAbertura ADD AmortizacaoAcumuladaPorCota decimal(28,10) NOT NULL DEFAULT('0') ;
	
	EXEC('UPDATE PosicaoCotistaAbertura SET AmortizacaoAcumuladaPorCota = 
	(select isnull(sum(agenda.Valor),0)
	from AgendaFundo agenda 
	where agenda.IdCarteira = PosicaoCotistaAbertura.IdCarteira and agenda.DataEvento >= PosicaoCotistaAbertura.DataAplicacao and agenda.DataEvento < PosicaoCotistaAbertura.DataHistorico and agenda.TipoEvento = 2)');
END
GO

if not exists(select 1 from syscolumns where id = object_id('PosicaoFundo') and name = 'AmortizacaoAcumuladaPorCota')
BEGIN
	ALTER TABLE PosicaoFundo ADD AmortizacaoAcumuladaPorCota decimal(28,10) NOT NULL DEFAULT('0') ;
	
	EXEC('UPDATE PosicaoFundo SET AmortizacaoAcumuladaPorCota = 
	(select isnull(sum(agenda.Valor),0)
	from AgendaFundo agenda 
	inner join Cliente cli on (cli.IdCliente = agenda.IdCarteira) 
	where agenda.IdCarteira = PosicaoFundo.IdCarteira and agenda.DataEvento >= PosicaoFundo.DataAplicacao and agenda.DataEvento <= cli.dataDia and agenda.TipoEvento = 2)');
END
GO

if not exists(select 1 from syscolumns where id = object_id('PosicaoFundoHistorico') and name = 'AmortizacaoAcumuladaPorCota')
BEGIN
	ALTER TABLE PosicaoFundoHistorico ADD AmortizacaoAcumuladaPorCota decimal(28,10) NOT NULL DEFAULT('0') ;
	
	EXEC('UPDATE PosicaoFundoHistorico SET AmortizacaoAcumuladaPorCota = 
	(select isnull(sum(agenda.Valor),0)
	from AgendaFundo agenda 
	where agenda.IdCarteira = PosicaoFundoHistorico.IdCarteira and agenda.DataEvento >= PosicaoFundoHistorico.DataAplicacao and agenda.DataEvento <= PosicaoFundoHistorico.DataHistorico and agenda.TipoEvento = 2)');	
END
GO

if not exists(select 1 from syscolumns where id = object_id('PosicaoFundoAbertura') and name = 'AmortizacaoAcumuladaPorCota')
BEGIN
	ALTER TABLE PosicaoFundoAbertura ADD AmortizacaoAcumuladaPorCota decimal(28,10) NOT NULL DEFAULT('0') ;
	
	EXEC('UPDATE PosicaoFundoAbertura SET AmortizacaoAcumuladaPorCota = 
	(select isnull(sum(agenda.Valor),0)
	from AgendaFundo agenda 
	where agenda.IdCarteira = PosicaoFundoAbertura.IdCarteira and agenda.DataEvento >= PosicaoFundoAbertura.DataAplicacao and agenda.DataEvento < PosicaoFundoAbertura.DataHistorico and agenda.TipoEvento = 2)');	
END
GO

if not exists(select 1 from syscolumns where id = object_id('PosicaoFundoAux') and name = 'AmortizacaoAcumuladaPorCota')
BEGIN
	ALTER TABLE PosicaoFundoAux ADD AmortizacaoAcumuladaPorCota decimal(28,10) NOT NULL DEFAULT('0') ;
	
	EXEC('UPDATE PosicaofundoAux SET AmortizacaoAcumuladaPorCota = 
	(select isnull(sum(agenda.Valor),0)
	from AgendaFundo agenda 
	where agenda.IdCarteira = PosicaofundoAux.IdCarteira and agenda.DataEvento >= PosicaofundoAux.DataAplicacao and agenda.DataEvento <= PosicaofundoAux.DataReferencia and agenda.TipoEvento = 2)');
	
END
GO

if not exists(select 1 from syscolumns where id = object_id('PosicaoCotistaAux') and name = 'AmortizacaoAcumuladaPorCota')
BEGIN
	ALTER TABLE PosicaoCotistaAux ADD AmortizacaoAcumuladaPorCota decimal(28,10) NOT NULL DEFAULT('0') ;
	
	EXEC('UPDATE PosicaoCotistaAux SET AmortizacaoAcumuladaPorCota = 
	(select isnull(sum(agenda.Valor),0)
	from AgendaFundo agenda 
	where agenda.IdCarteira = PosicaoCotistaAux.IdCarteira and agenda.DataEvento >= PosicaoCotistaAux.DataAplicacao and agenda.DataEvento <= PosicaoCotistaAux.DataReferencia and agenda.TipoEvento = 2)');
END
GO

if not exists(select 1 from syscolumns where id = object_id('PosicaoCotista') and name = 'JurosAcumuladoPorCota')
BEGIN
	ALTER TABLE PosicaoCotista ADD JurosAcumuladoPorCota decimal(28,10) NOT NULL DEFAULT('0') ;
	
	EXEC('UPDATE PosicaoCotista SET JurosAcumuladoPorCota = 
	(select isnull(sum(agenda.Valor),0)
	from AgendaFundo agenda 
	inner join Cliente cli on (cli.IdCliente = agenda.IdCarteira) 
	where agenda.IdCarteira = posicaoCotista.IdCarteira and agenda.DataEvento >= PosicaoCotista.DataAplicacao and agenda.DataEvento <= cli.dataDia and agenda.TipoEvento = 1)');
END
GO

if not exists(select 1 from syscolumns where id = object_id('PosicaoCotistaHistorico') and name = 'JurosAcumuladoPorCota')
BEGIN
	ALTER TABLE PosicaoCotistaHistorico ADD JurosAcumuladoPorCota decimal(28,10) NOT NULL DEFAULT('0') ;
	
	EXEC('UPDATE PosicaoCotistaHistorico SET JurosAcumuladoPorCota = 
	(select isnull(sum(agenda.Valor),0)
	from AgendaFundo agenda 
	where agenda.IdCarteira = PosicaoCotistaHistorico.IdCarteira and agenda.DataEvento >= PosicaoCotistaHistorico.DataAplicacao and agenda.DataEvento <= PosicaoCotistaHistorico.DataHistorico and agenda.TipoEvento = 1)');
END
GO

if not exists(select 1 from syscolumns where id = object_id('PosicaoCotistaAbertura') and name = 'JurosAcumuladoPorCota')
BEGIN
	ALTER TABLE PosicaoCotistaAbertura ADD JurosAcumuladoPorCota decimal(28,10) NOT NULL DEFAULT('0') ;
	
	EXEC('UPDATE PosicaoCotistaAbertura SET JurosAcumuladoPorCota = 
	(select isnull(sum(agenda.Valor),0)
	from AgendaFundo agenda 
	where agenda.IdCarteira = PosicaoCotistaAbertura.IdCarteira and agenda.DataEvento >= PosicaoCotistaAbertura.DataAplicacao and agenda.DataEvento < PosicaoCotistaAbertura.DataHistorico and agenda.TipoEvento = 1)');
END
GO

if not exists(select 1 from syscolumns where id = object_id('PosicaoFundo') and name = 'JurosAcumuladoPorCota')
BEGIN
	ALTER TABLE PosicaoFundo ADD JurosAcumuladoPorCota decimal(28,10) NOT NULL DEFAULT('0') ;
	
	EXEC('UPDATE PosicaoFundo SET JurosAcumuladoPorCota = 
	(select isnull(sum(agenda.Valor),0)
	from AgendaFundo agenda 
	inner join Cliente cli on (cli.IdCliente = agenda.IdCarteira) 
	where agenda.IdCarteira = PosicaoFundo.IdCarteira and agenda.DataEvento >= PosicaoFundo.DataAplicacao and agenda.DataEvento <= cli.dataDia and agenda.TipoEvento = 1)');
END
GO

if not exists(select 1 from syscolumns where id = object_id('PosicaoFundoHistorico') and name = 'JurosAcumuladoPorCota')
BEGIN
	ALTER TABLE PosicaoFundoHistorico ADD JurosAcumuladoPorCota decimal(28,10) NOT NULL DEFAULT('0') ;
	
	EXEC('UPDATE PosicaoFundoHistorico SET JurosAcumuladoPorCota = 
	(select isnull(sum(agenda.Valor),0)
	from AgendaFundo agenda 
	where agenda.IdCarteira = PosicaoFundoHistorico.IdCarteira and agenda.DataEvento >= PosicaoFundoHistorico.DataAplicacao and agenda.DataEvento <= PosicaoFundoHistorico.DataHistorico and agenda.TipoEvento = 1)');	
END
GO

if not exists(select 1 from syscolumns where id = object_id('PosicaoFundoAbertura') and name = 'JurosAcumuladoPorCota')
BEGIN
	ALTER TABLE PosicaoFundoAbertura ADD JurosAcumuladoPorCota decimal(28,10) NOT NULL DEFAULT('0') ;
	
	EXEC('UPDATE PosicaoFundoAbertura SET JurosAcumuladoPorCota = 
	(select isnull(sum(agenda.Valor),0)
	from AgendaFundo agenda 
	where agenda.IdCarteira = PosicaoFundoAbertura.IdCarteira and agenda.DataEvento >= PosicaoFundoAbertura.DataAplicacao and agenda.DataEvento < PosicaoFundoAbertura.DataHistorico and agenda.TipoEvento = 1)');	
END
GO

if not exists(select 1 from syscolumns where id = object_id('PosicaoFundoAux') and name = 'JurosAcumuladoPorCota')
BEGIN
	ALTER TABLE PosicaoFundoAux ADD JurosAcumuladoPorCota decimal(28,10) NOT NULL DEFAULT('0') ;
	
	EXEC('UPDATE PosicaofundoAux SET JurosAcumuladoPorCota = 
	(select isnull(sum(agenda.Valor),0)
	from AgendaFundo agenda 
	where agenda.IdCarteira = PosicaofundoAux.IdCarteira and agenda.DataEvento >= PosicaofundoAux.DataAplicacao and agenda.DataEvento <= PosicaofundoAux.DataReferencia and agenda.TipoEvento = 1)');
	
END
GO

if not exists(select 1 from syscolumns where id = object_id('PosicaoCotistaAux') and name = 'JurosAcumuladoPorCota')
BEGIN
	ALTER TABLE PosicaoCotistaAux ADD JurosAcumuladoPorCota decimal(28,10) NOT NULL DEFAULT('0') ;
	
	EXEC('UPDATE PosicaoCotistaAux SET JurosAcumuladoPorCota = 
	(select isnull(sum(agenda.Valor),0)
	from AgendaFundo agenda 
	where agenda.IdCarteira = PosicaoCotistaAux.IdCarteira and agenda.DataEvento >= PosicaoCotistaAux.DataAplicacao and agenda.DataEvento <= PosicaoCotistaAux.DataReferencia and agenda.TipoEvento = 1)');
END
GO

if not exists(select 1 from syscolumns where id = object_id('PosicaoCotista') and name = 'AmortizacaoAcumuladaPorValor')
BEGIN
	ALTER TABLE PosicaoCotista ADD AmortizacaoAcumuladaPorValor decimal(28,10) NOT NULL DEFAULT('0') ;
	
	EXEC('UPDATE PosicaoCotista SET AmortizacaoAcumuladaPorValor = AmortizacaoAcumuladaPorCota * quantidade where AmortizacaoAcumuladaPorCota > 0');
END
GO

if not exists(select 1 from syscolumns where id = object_id('PosicaoCotistaHistorico') and name = 'AmortizacaoAcumuladaPorValor')
BEGIN
	ALTER TABLE PosicaoCotistaHistorico ADD AmortizacaoAcumuladaPorValor decimal(28,10) NOT NULL DEFAULT('0') ;
	
	EXEC('UPDATE PosicaoCotistaHistorico SET AmortizacaoAcumuladaPorValor = AmortizacaoAcumuladaPorCota * quantidade where AmortizacaoAcumuladaPorCota > 0');
END
GO

if not exists(select 1 from syscolumns where id = object_id('PosicaoCotistaAbertura') and name = 'AmortizacaoAcumuladaPorValor')
BEGIN
	ALTER TABLE PosicaoCotistaAbertura ADD AmortizacaoAcumuladaPorValor decimal(28,10) NOT NULL DEFAULT('0') ;
	
	EXEC('UPDATE PosicaoCotistaAbertura SET AmortizacaoAcumuladaPorValor = AmortizacaoAcumuladaPorCota * quantidade where AmortizacaoAcumuladaPorCota > 0');
END
GO

if not exists(select 1 from syscolumns where id = object_id('PosicaoFundo') and name = 'AmortizacaoAcumuladaPorValor')
BEGIN
	ALTER TABLE PosicaoFundo ADD AmortizacaoAcumuladaPorValor decimal(28,10) NOT NULL DEFAULT('0') ;
	
	EXEC('UPDATE PosicaoFundo SET AmortizacaoAcumuladaPorValor = AmortizacaoAcumuladaPorCota * quantidade where AmortizacaoAcumuladaPorCota > 0');
END
GO

if not exists(select 1 from syscolumns where id = object_id('PosicaoFundoHistorico') and name = 'AmortizacaoAcumuladaPorValor')
BEGIN
	ALTER TABLE PosicaoFundoHistorico ADD AmortizacaoAcumuladaPorValor decimal(28,10) NOT NULL DEFAULT('0') ;
	
	EXEC('UPDATE PosicaoFundoHistorico SET AmortizacaoAcumuladaPorValor = AmortizacaoAcumuladaPorCota * quantidade where AmortizacaoAcumuladaPorCota > 0');	
END
GO

if not exists(select 1 from syscolumns where id = object_id('PosicaoFundoAbertura') and name = 'AmortizacaoAcumuladaPorValor')
BEGIN
	ALTER TABLE PosicaoFundoAbertura ADD AmortizacaoAcumuladaPorValor decimal(28,10) NOT NULL DEFAULT('0') ;
	
	EXEC('UPDATE PosicaoFundoAbertura SET AmortizacaoAcumuladaPorValor = AmortizacaoAcumuladaPorCota * quantidade where AmortizacaoAcumuladaPorCota > 0');	
END
GO

if not exists(select 1 from syscolumns where id = object_id('PosicaoFundoAux') and name = 'AmortizacaoAcumuladaPorValor')
BEGIN
	ALTER TABLE PosicaoFundoAux ADD AmortizacaoAcumuladaPorValor decimal(28,10) NOT NULL DEFAULT('0') ;
	
	EXEC('UPDATE PosicaofundoAux SET AmortizacaoAcumuladaPorValor = AmortizacaoAcumuladaPorCota * quantidade where AmortizacaoAcumuladaPorCota > 0');
	
END
GO

if not exists(select 1 from syscolumns where id = object_id('PosicaoCotistaAux') and name = 'AmortizacaoAcumuladaPorValor')
BEGIN
	ALTER TABLE PosicaoCotistaAux ADD AmortizacaoAcumuladaPorValor decimal(28,10) NOT NULL DEFAULT('0') ;
	
	EXEC('UPDATE PosicaoCotistaAux SET AmortizacaoAcumuladaPorValor = AmortizacaoAcumuladaPorCota * quantidade where AmortizacaoAcumuladaPorCota > 0');
END
GO

if not exists(select 1 from syscolumns where id = object_id('PosicaoCotista') and name = 'JurosAcumuladoPorValor')
BEGIN
	ALTER TABLE PosicaoCotista ADD JurosAcumuladoPorValor decimal(28,10) NOT NULL DEFAULT('0') ;
	
	EXEC('UPDATE PosicaoCotista SET JurosAcumuladoPorValor = JurosAcumuladoPorCota * quantidade where JurosAcumuladoPorCota > 0');
END
GO

if not exists(select 1 from syscolumns where id = object_id('PosicaoCotistaHistorico') and name = 'JurosAcumuladoPorValor')
BEGIN
	ALTER TABLE PosicaoCotistaHistorico ADD JurosAcumuladoPorValor decimal(28,10) NOT NULL DEFAULT('0') ;
	
	EXEC('UPDATE PosicaoCotistaHistorico SET JurosAcumuladoPorValor = JurosAcumuladoPorCota * quantidade where JurosAcumuladoPorCota > 0');
END
GO

if not exists(select 1 from syscolumns where id = object_id('PosicaoCotistaAbertura') and name = 'JurosAcumuladoPorValor')
BEGIN
	ALTER TABLE PosicaoCotistaAbertura ADD JurosAcumuladoPorValor decimal(28,10) NOT NULL DEFAULT('0') ;
	
	EXEC('UPDATE PosicaoCotistaAbertura SET JurosAcumuladoPorValor = JurosAcumuladoPorCota * quantidade where JurosAcumuladoPorCota > 0');
END
GO

if not exists(select 1 from syscolumns where id = object_id('PosicaoFundo') and name = 'JurosAcumuladoPorValor')
BEGIN
	ALTER TABLE PosicaoFundo ADD JurosAcumuladoPorValor decimal(28,10) NOT NULL DEFAULT('0') ;
	
	EXEC('UPDATE PosicaoFundo SET JurosAcumuladoPorValor = JurosAcumuladoPorCota * quantidade where JurosAcumuladoPorCota > 0');
END
GO

if not exists(select 1 from syscolumns where id = object_id('PosicaoFundoHistorico') and name = 'JurosAcumuladoPorValor')
BEGIN
	ALTER TABLE PosicaoFundoHistorico ADD JurosAcumuladoPorValor decimal(28,10) NOT NULL DEFAULT('0') ;
	
	EXEC('UPDATE PosicaoFundoHistorico SET JurosAcumuladoPorValor = JurosAcumuladoPorCota * quantidade where JurosAcumuladoPorCota > 0');	
END
GO

if not exists(select 1 from syscolumns where id = object_id('PosicaoFundoAbertura') and name = 'JurosAcumuladoPorValor')
BEGIN
	ALTER TABLE PosicaoFundoAbertura ADD JurosAcumuladoPorValor decimal(28,10) NOT NULL DEFAULT('0') ;
	
	EXEC('UPDATE PosicaoFundoAbertura SET JurosAcumuladoPorValor = JurosAcumuladoPorCota * quantidade where JurosAcumuladoPorCota > 0');	
END
GO

if not exists(select 1 from syscolumns where id = object_id('PosicaoFundoAux') and name = 'JurosAcumuladoPorValor')
BEGIN
	ALTER TABLE PosicaoFundoAux ADD JurosAcumuladoPorValor decimal(28,10) NOT NULL DEFAULT('0') ;
	
	EXEC('UPDATE PosicaofundoAux SET JurosAcumuladoPorValor = JurosAcumuladoPorCota * quantidade where JurosAcumuladoPorCota > 0');
	
END
GO

if not exists(select 1 from syscolumns where id = object_id('PosicaoCotistaAux') and name = 'JurosAcumuladoPorValor')
BEGIN
	ALTER TABLE PosicaoCotistaAux ADD JurosAcumuladoPorValor decimal(28,10) NOT NULL DEFAULT('0') ;
	
	EXEC('UPDATE PosicaoCotistaAux SET JurosAcumuladoPorValor = JurosAcumuladoPorCota * quantidade where JurosAcumuladoPorCota > 0');
END
GO

IF NOT EXISTS (select 1 from sysconstraints where object_name(id) = 'CarteiraMae' and object_name(constid) = 'CarteiraMae_Filha_FK')	
begin

	delete from CarteiraMae where idCarteiraFilha not in (select idCarteira from carteira) 

	ALTER TABLE CarteiraMae  WITH CHECK ADD  CONSTRAINT CarteiraMae_Filha_FK FOREIGN KEY(idCarteiraFilha)
	REFERENCES Carteira (Idcarteira)
	
end
go

IF NOT EXISTS (select 1 from sysconstraints where object_name(id) = 'CarteiraMae' and object_name(constid) = 'CarteiraMae_Mae_FK')	
begin

	delete from CarteiraMae where IdCarteiraMae not in (select idCarteira from carteira) 

	ALTER TABLE CarteiraMae  WITH CHECK ADD  CONSTRAINT CarteiraMae_Mae_FK FOREIGN KEY(IdCarteiraMae)
	REFERENCES Carteira (Idcarteira)
	
end
go

if (not exists (select * from sys.all_columns where name = 'ValidaDigitoConta' and OBJECT_NAME(object_id) = 'ContabPlano'))
begin
	ALTER TABLE ContabPlano ADD  ValidaDigitoConta char(1) not null default 'N'
end

if (not exists (select * from sys.all_columns where name = 'Digito' and OBJECT_NAME(object_id) = 'ContabConta'))
begin
	ALTER TABLE ContabConta ADD  Digito char(1) null
end

IF NOT EXISTS(SELECT * FROM SYSCOLUMNS WHERE NAME = 'Usuario' AND ID = OBJECT_ID('PessoaSuitabilityHistorico'))
BEGIN
	alter table PessoaSuitabilityHistorico add Usuario varchar(100) not NULL DEFAULT ''
END

if (not exists (select * from sys.all_columns where name = 'InfluenciaGestorLocalCvm' and OBJECT_NAME(object_id) = 'Carteira'))
	begin
		ALTER TABLE Carteira ADD  InfluenciaGestorLocalCvm char(1) not null default 'N'
	end

if (not exists (select * from sys.all_columns where name = 'InvestimentoColetivoCvm' and OBJECT_NAME(object_id) = 'Carteira'))
	begin
		ALTER TABLE Carteira ADD  InvestimentoColetivoCvm char(1) not null default 'N'
	end

if (not exists (select * from sys.all_columns where name = 'InvestimentoColetivoCvm' and OBJECT_NAME(object_id) = 'PapelRendaFixa'))
	begin
		ALTER TABLE PapelRendaFixa ADD  InvestimentoColetivoCvm char(1) not null default 'N'
	end

if (not exists (select * from sys.all_columns where name = 'InvestimentoColetivoCvm' and OBJECT_NAME(object_id) = 'AtivoBMF'))
	begin
		ALTER TABLE AtivoBMF ADD  InvestimentoColetivoCvm char(1) not null default 'N'
	end

if (not exists (select * from sys.all_columns where name = 'InvestimentoColetivoCvm' and OBJECT_NAME(object_id) = 'AtivoBolsa'))
	begin
		ALTER TABLE AtivoBolsa ADD  InvestimentoColetivoCvm char(1) not null default 'N'
	end

IF NOT EXISTS(SELECT * FROM SYSCOLUMNS WHERE NAME = 'Participacao' AND ID = OBJECT_ID('AtivoBolsa'))
begin
	alter table AtivoBolsa add Participacao char(1) Constraint DF_Participacao DEFAULT 'N';
end  

IF NOT EXISTS (SELECT name FROM sysindexes WHERE name = 'AgendaFundo_Carteira_Data_Idx')
begin
	CREATE NONCLUSTERED INDEX AgendaFundo_Carteira_Data_Idx ON AgendaFundo (DataEvento,IdCarteira);
END		

if not exists(select 1 from syscolumns where id = object_id('AgendaFundo') and name = 'CotaFechamentoCheia')
BEGIN
	ALTER TABLE AgendaFundo ADD CotaFechamentoCheia decimal(28,12) NULL;
END
GO

if not exists(select 1 from syscolumns where id = object_id('Liquidacao') and name = 'CodigoAtivo')
BEGIN
	ALTER TABLE Liquidacao ADD CodigoAtivo varchar(40) null;
END
GO	
	
if not exists(select 1 from syscolumns where id = object_id('LiquidacaoHistorico') and name = 'CodigoAtivo')
BEGIN	
	ALTER TABLE LiquidacaoHistorico ADD CodigoAtivo varchar(40) null;
END
GO	
	
if not exists(select 1 from syscolumns where id = object_id('LiquidacaoAbertura') and name = 'CodigoAtivo')
BEGIN	
	ALTER TABLE LiquidacaoAbertura ADD CodigoAtivo varchar(40) null;
END
GO	
	
if not exists(select 1 from syscolumns where id = object_id('LiquidacaoFuturo') and name = 'CodigoAtivo')
BEGIN	
	ALTER TABLE LiquidacaoFuturo ADD CodigoAtivo varchar(40) null;
END
GO

IF NOT EXISTS(SELECT * FROM SYSCOLUMNS WHERE NAME = 'Participacao' AND ID = OBJECT_ID('TituloRendaFixa'))
begin
	alter table TituloRendaFixa add Participacao char(1) Constraint DF_ParticipacaoTituloRF DEFAULT 'N';
end  

IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'EntidadePerfilFundo') = 0
BEGIN

	CREATE TABLE [dbo].[EntidadePerfilFundo](
		[IdEntidade] [int] IDENTITY(1,1) NOT NULL,
		[Codigo] [varchar](255) NOT NULL,
		[Descricao] [varchar](255) NOT NULL,
	 CONSTRAINT [EntidadePerfilFundo_PK] PRIMARY KEY CLUSTERED 
	(
		[IdEntidade] ASC
	)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
	) ON [PRIMARY]

END

IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'EntidadeClassificacaoPerfilFundo') = 0
BEGIN

	CREATE TABLE [dbo].[EntidadeClassificacaoPerfilFundo](
		[IdEntidadeClassificacao] [int] IDENTITY(1,1) NOT NULL,
		[IdEntidadePerfilFundo] [int] NOT NULL,
		[TipoFundoCarteira] [varchar](100) NOT NULL,
		[DescricaoTipoFundoCarteira] [varchar](255) NOT NULL,
		[InicioVigencia] [datetime] NOT NULL,
	 CONSTRAINT [EntidadeClassificacaoPerfilFundo_PK] PRIMARY KEY CLUSTERED 
	(
		[IdEntidadeClassificacao] ASC
	)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
	) ON [PRIMARY]

	ALTER TABLE [dbo].[EntidadeClassificacaoPerfilFundo]  WITH CHECK ADD  CONSTRAINT [EntidadeClassificacaoPerfilFundo_EntidadePerfilFundo_FK1] FOREIGN KEY([IdEntidadePerfilFundo])
	REFERENCES [dbo].[EntidadePerfilFundo] ([IdEntidade])
	ON DELETE CASCADE

END

IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'PerfilClassificacaoFundo') = 0
BEGIN

	CREATE TABLE [dbo].[PerfilClassificacaoFundo](
		[IdPerfil] [int] IDENTITY(1,1) NOT NULL,
		[Codigo] [varchar](100) NOT NULL,
		[Descricao] [varchar](255) NOT NULL,
		[InicioVigencia] [datetime] NOT NULL,
	 CONSTRAINT [PerfilClassificacaoFundo_PK] PRIMARY KEY CLUSTERED 
	(
		[IdPerfil] ASC
	)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
	) ON [PRIMARY]

END

IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'PerfilClassificacaoFundoEntidade') = 0
BEGIN

	CREATE TABLE [dbo].[PerfilClassificacaoFundoEntidade](
		[IdPerfilClassificacaoFundoEntidade] [int] IDENTITY(1,1) NOT NULL,
		[IdPerfilClassificacao] [int] NOT NULL,
		[IdEntidade] [int] NOT NULL,
		[IdEntidadeClassificacao] [int] NOT NULL,
	 CONSTRAINT [PerfilClassificacaoFundoEntidade_PK] PRIMARY KEY CLUSTERED 
	(
		[IdPerfilClassificacaoFundoEntidade] ASC
	)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
	) ON [PRIMARY]

	ALTER TABLE [dbo].[PerfilClassificacaoFundoEntidade]  WITH CHECK ADD  CONSTRAINT [PerfilClassificacaoFundoEntidade_PerfilClassificacaoFundo_FK1] FOREIGN KEY([IdPerfilClassificacao])
	REFERENCES [dbo].[PerfilClassificacaoFundo] ([IdPerfil])
	ON DELETE CASCADE

	ALTER TABLE [dbo].[PerfilClassificacaoFundoEntidade]  WITH CHECK ADD  CONSTRAINT [PerfilClassificacaoFundoEntidade_EntidadePerfilFundo_FK1] FOREIGN KEY([IdEntidade])
	REFERENCES [dbo].[EntidadePerfilFundo] ([IdEntidade])
	ON DELETE CASCADE

	ALTER TABLE [dbo].[PerfilClassificacaoFundoEntidade]  WITH CHECK ADD  CONSTRAINT [PerfilClassificacaoFundoEntidade_EntidadeClassificacaoPerfilFundo_FK1] FOREIGN KEY([IdEntidadeClassificacao])
	REFERENCES [dbo].[EntidadeClassificacaoPerfilFundo] ([IdEntidadeClassificacao])
	ON DELETE NO ACTION
END

IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'PerfilClassificacaoFundoCarteira') = 0
BEGIN

	CREATE TABLE [dbo].[PerfilClassificacaoFundoCarteira](
		[IdPerfilClassificacaoFundoCarteira] [int] IDENTITY(1,1) NOT NULL,
		[IdPerfilClassificacao] [int] NOT NULL,
		[IdCarteira] [int] NOT NULL,
		[InicioVigenciaPerfil] [datetime] NOT NULL,
	 CONSTRAINT [PerfilClassificacaoFundoCarteira_PK] PRIMARY KEY CLUSTERED 
	(
		[IdPerfilClassificacaoFundoCarteira] ASC
	)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
	) ON [PRIMARY]

	ALTER TABLE [dbo].[PerfilClassificacaoFundoCarteira]  WITH CHECK ADD  CONSTRAINT [PerfilClassificacaoFundoCarteira_PerfilClassificacaoFundo_FK1] FOREIGN KEY([IdPerfilClassificacao])
	REFERENCES [dbo].[PerfilClassificacaoFundo] ([IdPerfil])
	ON DELETE CASCADE

	ALTER TABLE [dbo].[PerfilClassificacaoFundoCarteira]  WITH CHECK ADD  CONSTRAINT [PerfilClassificacaoFundoCarteira_Carteira_FK1] FOREIGN KEY([IdCarteira])
	REFERENCES [dbo].[Carteira] ([IdCarteira])
	ON DELETE CASCADE

END

IF NOT EXISTS(SELECT * FROM SYSCOLUMNS WHERE NAME = 'IdEventoVencimento' AND ID = OBJECT_ID('Liquidacao'))
begin
	ALTER TABLE Liquidacao ADD  IdEventoVencimento int null
end

IF NOT EXISTS(SELECT * FROM SYSCOLUMNS WHERE NAME = 'IdEventoVencimento' AND ID = OBJECT_ID('LiquidacaoAbertura'))
begin
	ALTER TABLE LiquidacaoAbertura ADD  IdEventoVencimento int null
end

IF NOT EXISTS(SELECT * FROM SYSCOLUMNS WHERE NAME = 'IdEventoVencimento' AND ID = OBJECT_ID('LiquidacaoHistorico'))
begin
	ALTER TABLE LiquidacaoHistorico ADD  IdEventoVencimento int null
end

IF NOT EXISTS(SELECT * FROM SYSCOLUMNS WHERE NAME = 'IdEventoVencimento' AND ID = OBJECT_ID('LiquidacaoFuturo'))
begin
	ALTER TABLE LiquidacaoFuturo ADD  IdEventoVencimento int null
end

IF NOT EXISTS (SELECT * FROM SYSCOLUMNS WHERE NAME = 'PossuiResgateAutomatico' AND ID = OBJECT_ID('Carteira'))
BEGIN
	ALTER TABLE Carteira
	ADD PossuiResgateAutomatico CHAR(1) NOT NULL DEFAULT 'N'
END

IF NOT EXISTS(SELECT * FROM SYSCOLUMNS WHERE NAME = 'ParametrosAvancados' AND ID = OBJECT_ID('TituloRendaFixa'))
begin
	ALTER TABLE TituloRendaFixa ADD ParametrosAvancados char(1) not null default('N')
end

IF NOT EXISTS(SELECT * FROM SYSCOLUMNS WHERE NAME = 'ExclusaoLogica' AND ID = OBJECT_ID('OperacaoFundo'))
begin
	ALTER TABLE OperacaoFundo ADD ExclusaoLogica char(1) not null default('N')
end

IF NOT EXISTS(SELECT * FROM SYSCOLUMNS WHERE NAME = 'ExclusaoLogica' AND ID = OBJECT_ID('OperacaoCotista'))
begin
	ALTER TABLE OperacaoCotista ADD ExclusaoLogica char(1) not null default('N')
end

IF NOT EXISTS(SELECT * FROM SYSCOLUMNS WHERE NAME = 'ExclusaoLogica' AND ID = OBJECT_ID('OperacaoFundoAux'))
begin
	ALTER TABLE OperacaoFundoAux ADD ExclusaoLogica char(1) not null default('N')
end

IF NOT EXISTS(SELECT * FROM SYSCOLUMNS WHERE NAME = 'ExclusaoLogica' AND ID = OBJECT_ID('OperacaoCotistaAux'))
begin
	ALTER TABLE OperacaoCotistaAux ADD ExclusaoLogica char(1) not null default('N')
end

IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'V' AND NAME = 'SB_HIST_INDEXES') > 0
BEGIN
	drop view SB_HIST_INDEXES;
END	
go

IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'V' AND NAME = 'SB_HIST_INDEXES') = 0
BEGIN
						
	exec('CREATE VIEW [dbo].[SB_HIST_INDEXES] 
	AS 	
		SELECT 1 ID, 
				CONVERT(INT, dePara.CodigoExterno) as IDINDEX,
				CASE WHEN CAST(dePara.CodigoExterno AS INT) = 127 --IPCA
					THEN Convert(datetime, ''15/'' + convert(varchar(2),DATEPART(month,DATEADD(month,1,Data)))+ ''/'' +convert(varchar(4),DATEPART(year,DATEADD(month,1,Data))), 103)
					  WHEN CAST(dePara.CodigoExterno AS INT) = 126 --IGPM
					THEN Convert(datetime, ''01/'' + convert(varchar(2),DATEPART(month,Data))+ ''/'' +convert(varchar(4),DATEPART(year,Data)), 103)
				ELSE Data
				END as REFERENCEDATE,
				CASE WHEN CAST(dePara.CodigoExterno AS INT) = 127 --IPCA
					THEN Convert(datetime, ''15/'' + convert(varchar(2),DATEPART(month,DATEADD(month,1,Data)))+ ''/'' +convert(varchar(4),DATEPART(year,DATEADD(month,1,Data))), 103)
					 WHEN CAST(dePara.CodigoExterno AS INT) = 126 --IGPM
					THEN Convert(datetime, ''01/'' + convert(varchar(2),DATEPART(month,Data))+ ''/'' +convert(varchar(4),DATEPART(year,Data)), 103)
				ELSE Data
				END as RELEASEDATE,
				CASE WHEN CAST(dePara.CodigoExterno AS INT) IN (126, 127, 3, 4)  --IGPM / IPCA / IGPDI / INPC 
					THEN Valor
					ELSE NULL
				END as INDEXESINDEX,
				CASE WHEN CAST(dePara.CodigoExterno AS INT) NOT IN (126, 127, 3, 4) --IGPM / IPCA / IGPDI / INPC 
					THEN Valor
					ELSE 0
				END as INDEXESVALUE,
				1 as VALID 
			FROM [dbo].[CotacaoIndice] 
			INNER JOIN dePara ON (dePara.IdTipoDePara = (SELECT DISTINCT tpDePAra.IdTipoDePara 
					 FROM  dbo.DePara dePara 
					 INNER JOIN  dbo.TipoDePara tpDePara ON tpDePara.idTipoDePara = dePara.idTipoDePara
					 WHERE tpDePara.EnumTipoDePara = 1 /* Fincs - Indexador */						
						    and tpDePara.TipoCampo = 1) AND dePara.CodigoInterno = idIndice)')
END					
GO 

IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'ImplantacaoPosicaoCotista') = 0
begin
	CREATE TABLE dbo.ImplantacaoPosicaoCotista
	(
		IdImplantacaoPosicaoCotista INT NOT NULL IDENTITY,
		IdCotista					INT not null,
		IdCarteira					INT not null,
		ValorAplicacao				decimal(16,2) not null,
		DataAplicacao				datetime not null,
		DataConversao				datetime not null,
		Quantidade					decimal(28,12) not null,
		CotaAplicacao				decimal(28,12) not null,
		DataUltimaCobrancaIR		datetime not null,
		DataUltimoCortePfee			datetime not null,
		DataLiquidacao				datetime,
		[Status]					char(1),						
		DataImportacao				datetime,
		CONSTRAINT [PK_ImplantacaoPosicaoCotista] PRIMARY KEY CLUSTERED 
		(
			[IdImplantacaoPosicaoCotista] ASC
		)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
		) ON [PRIMARY]

END	
GO

IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'EventoFinanceiro') = 0
begin
	CREATE TABLE [dbo].[EventoFinanceiro](
		[IdEventoFinanceiro] [int] NOT NULL,
		[Descricao] [nvarchar](200) NULL,
		[ClassificacaoAnbima] [int] NULL,
		[IdEventoProvisao] [int] NULL,
		[IdEventoPagamento] [int] NULL,
		CONSTRAINT [PK_EventoFinanceiro] PRIMARY KEY CLUSTERED 
	(
		[IdEventoFinanceiro] ASC
	)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
	) ON [PRIMARY]

	ALTER TABLE [dbo].[EventoFinanceiro]  WITH CHECK ADD  CONSTRAINT [FK_EventoFinanceiro_ContabRoteiro] FOREIGN KEY([IdEventoPagamento])
	REFERENCES [dbo].[ContabRoteiro] ([IdEvento])

	ALTER TABLE [dbo].[EventoFinanceiro] CHECK CONSTRAINT [FK_EventoFinanceiro_ContabRoteiro]

	ALTER TABLE [dbo].[EventoFinanceiro]  WITH CHECK ADD  CONSTRAINT [FK_EventoFinanceiro_ContabRoteiro1] FOREIGN KEY([IdEventoProvisao])
	REFERENCES [dbo].[ContabRoteiro] ([IdEvento])

	ALTER TABLE [dbo].[EventoFinanceiro] CHECK CONSTRAINT [FK_EventoFinanceiro_ContabRoteiro1]

	insert into EventoFinanceiro (IdEventoFinanceiro, Descricao, ClassificacaoAnbima) values (1,'Bolsa - Compra Ações',21)
	insert into EventoFinanceiro (IdEventoFinanceiro, Descricao, ClassificacaoAnbima) values (2,'Bolsa - Venda Ações',21)
	insert into EventoFinanceiro (IdEventoFinanceiro, Descricao, ClassificacaoAnbima) values (3,'Bolsa - Compra Opções',21)
	insert into EventoFinanceiro (IdEventoFinanceiro, Descricao, ClassificacaoAnbima) values (4,'Bolsa - Venda Opções',21)
	insert into EventoFinanceiro (IdEventoFinanceiro, Descricao, ClassificacaoAnbima) values (5,'Bolsa - Exercício Compra',21)
	insert into EventoFinanceiro (IdEventoFinanceiro, Descricao, ClassificacaoAnbima) values (6,'Bolsa - Exercício Venda',21)
	insert into EventoFinanceiro (IdEventoFinanceiro, Descricao, ClassificacaoAnbima) values (7,'Bolsa - Compra Termo',23)
	insert into EventoFinanceiro (IdEventoFinanceiro, Descricao, ClassificacaoAnbima) values (8,'Bolsa - Venda Termo',23)
	insert into EventoFinanceiro (IdEventoFinanceiro, Descricao, ClassificacaoAnbima) values (9,'Bolsa - Antecipação Termo',23)
	insert into EventoFinanceiro (IdEventoFinanceiro, Descricao, ClassificacaoAnbima) values (10,'Bolsa - Empréstimo Doado',31)
	insert into EventoFinanceiro (IdEventoFinanceiro, Descricao, ClassificacaoAnbima) values (11,'Bolsa - Empréstimo Tomado',31)
	insert into EventoFinanceiro (IdEventoFinanceiro, Descricao, ClassificacaoAnbima) values (12,'Bolsa - Antecipação Empréstimo',31)
	insert into EventoFinanceiro (IdEventoFinanceiro, Descricao, ClassificacaoAnbima) values (13,'Bolsa - Ajuste Operação Futuro',21)
	insert into EventoFinanceiro (IdEventoFinanceiro, Descricao, ClassificacaoAnbima) values (14,'Bolsa - Ajuste Posição Futuro',21)
	insert into EventoFinanceiro (IdEventoFinanceiro, Descricao, ClassificacaoAnbima) values (15,'Bolsa - Liquidação Final Futuro',21)
	insert into EventoFinanceiro (IdEventoFinanceiro, Descricao, ClassificacaoAnbima) values (16,'Bolsa - Despesas Taxas',39)
	insert into EventoFinanceiro (IdEventoFinanceiro, Descricao, ClassificacaoAnbima) values (17,'Bolsa - Corretagem',36)
	insert into EventoFinanceiro (IdEventoFinanceiro, Descricao, ClassificacaoAnbima) values (18,'Bolsa - Dividendo',27)
	insert into EventoFinanceiro (IdEventoFinanceiro, Descricao, ClassificacaoAnbima) values (19,'Bolsa - Juros Capital',28)
	insert into EventoFinanceiro (IdEventoFinanceiro, Descricao, ClassificacaoAnbima) values (20,'Bolsa - Rendimento',27)
	insert into EventoFinanceiro (IdEventoFinanceiro, Descricao, ClassificacaoAnbima) values (21,'Bolsa - Restituição Capital',21)
	insert into EventoFinanceiro (IdEventoFinanceiro, Descricao, ClassificacaoAnbima) values (22,'Bolsa - Crédito Frações Ações',21)
	insert into EventoFinanceiro (IdEventoFinanceiro, Descricao, ClassificacaoAnbima) values (23,'Bolsa - IR Proventos',27)
	insert into EventoFinanceiro (IdEventoFinanceiro, Descricao, ClassificacaoAnbima) values (24,'Bolsa - Outros Proventos',27)
	insert into EventoFinanceiro (IdEventoFinanceiro, Descricao, ClassificacaoAnbima) values (25,'Bolsa - Amortização',21)
	insert into EventoFinanceiro (IdEventoFinanceiro, Descricao, ClassificacaoAnbima) values (40,'Bolsa - Taxa Custódia',15)
	insert into EventoFinanceiro (IdEventoFinanceiro, Descricao, ClassificacaoAnbima) values (41,'Bolsa - Dividendo Distribuição',27)
	insert into EventoFinanceiro (IdEventoFinanceiro, Descricao, ClassificacaoAnbima) values (100,'Bolsa - Outros',999)
	insert into EventoFinanceiro (IdEventoFinanceiro, Descricao, ClassificacaoAnbima) values (201,'BMF – Compra à Vista',22)
	insert into EventoFinanceiro (IdEventoFinanceiro, Descricao, ClassificacaoAnbima) values (202,'BMF – Venda à Vista',22)
	insert into EventoFinanceiro (IdEventoFinanceiro, Descricao, ClassificacaoAnbima) values (203,'BMF – Compra Opções',22)
	insert into EventoFinanceiro (IdEventoFinanceiro, Descricao, ClassificacaoAnbima) values (204,'BMF – Venda Opções',22)
	insert into EventoFinanceiro (IdEventoFinanceiro, Descricao, ClassificacaoAnbima) values (205,'BMF – Exercício Compra',22)
	insert into EventoFinanceiro (IdEventoFinanceiro, Descricao, ClassificacaoAnbima) values (206,'BMF – Exercício Venda',22)
	insert into EventoFinanceiro (IdEventoFinanceiro, Descricao, ClassificacaoAnbima) values (220,'BMF – Despesas Taxas',41)
	insert into EventoFinanceiro (IdEventoFinanceiro, Descricao, ClassificacaoAnbima) values (221,'BMF – Corretagem',37)
	insert into EventoFinanceiro (IdEventoFinanceiro, Descricao, ClassificacaoAnbima) values (222,'BMF – Emolumentos',38)
	insert into EventoFinanceiro (IdEventoFinanceiro, Descricao, ClassificacaoAnbima) values (223,'BMF – Registro',41)
	insert into EventoFinanceiro (IdEventoFinanceiro, Descricao, ClassificacaoAnbima) values (224,'BMF – Outros Custos',41)
	insert into EventoFinanceiro (IdEventoFinanceiro, Descricao, ClassificacaoAnbima) values (225,'BMF – Ajuste Posição',22)
	insert into EventoFinanceiro (IdEventoFinanceiro, Descricao, ClassificacaoAnbima) values (250,'BMF – Ajuste Posição',22)
	insert into EventoFinanceiro (IdEventoFinanceiro, Descricao, ClassificacaoAnbima) values (251,'BMF – Ajuste Operação',22)
	insert into EventoFinanceiro (IdEventoFinanceiro, Descricao, ClassificacaoAnbima) values (260,'BMF – Taxa Permanência',41)
	insert into EventoFinanceiro (IdEventoFinanceiro, Descricao, ClassificacaoAnbima) values (270,'BMF – Taxa Clearing',999)
	insert into EventoFinanceiro (IdEventoFinanceiro, Descricao, ClassificacaoAnbima) values (300,'BMF – Outros',999)
	insert into EventoFinanceiro (IdEventoFinanceiro, Descricao) values (501,'Renda Fixa – Compra Final')
	insert into EventoFinanceiro (IdEventoFinanceiro, Descricao) values (502,'Renda Fixa –Venda Final')
	insert into EventoFinanceiro (IdEventoFinanceiro, Descricao) values (503,'Renda Fixa –Compra com Revenda')
	insert into EventoFinanceiro (IdEventoFinanceiro, Descricao) values (504,'Renda Fixa –Venda com Recompra')
	insert into EventoFinanceiro (IdEventoFinanceiro, Descricao) values (601,'Renda Fixa – Exercício Opção')
	insert into EventoFinanceiro (IdEventoFinanceiro, Descricao) values (505,'Renda Fixa – Net Operação Casada')
	insert into EventoFinanceiro (IdEventoFinanceiro, Descricao) values (510,'Renda Fixa – Vencimento')
	insert into EventoFinanceiro (IdEventoFinanceiro, Descricao) values (511,'Renda Fixa – Revenda')
	insert into EventoFinanceiro (IdEventoFinanceiro, Descricao) values (512,'Renda Fixa – Recompra')
	insert into EventoFinanceiro (IdEventoFinanceiro, Descricao) values (520,'Renda Fixa – Juros 30')
	insert into EventoFinanceiro (IdEventoFinanceiro, Descricao) values (521,'Renda Fixa – Amortização')
	insert into EventoFinanceiro (IdEventoFinanceiro, Descricao) values (530,'Renda Fixa – IR')
	insert into EventoFinanceiro (IdEventoFinanceiro, Descricao) values (531,'Renda Fixa –IOF')
	insert into EventoFinanceiro (IdEventoFinanceiro, Descricao) values (540,'Renda Fixa –Corretagem')
	insert into EventoFinanceiro (IdEventoFinanceiro, Descricao) values (550,'Renda Fixa –Taxa Custódia')
	insert into EventoFinanceiro (IdEventoFinanceiro, Descricao, ClassificacaoAnbima) values (600, 'Renda Fixa –Outros', 999)
	insert into EventoFinanceiro (IdEventoFinanceiro, Descricao) values (701,'Swap – Liquidação Vencimento')
	insert into EventoFinanceiro (IdEventoFinanceiro, Descricao) values (702,'Swap – Liquidação Antecipação')
	insert into EventoFinanceiro (IdEventoFinanceiro, Descricao) values (703,'Swap – Despesas Taxas')
	insert into EventoFinanceiro (IdEventoFinanceiro, Descricao, ClassificacaoAnbima) values (801,'Taxa Administração',44)
	insert into EventoFinanceiro (IdEventoFinanceiro, Descricao, ClassificacaoAnbima) values (802,'Pagamento Taxa Administração',44)
	insert into EventoFinanceiro (IdEventoFinanceiro, Descricao) values (810,'Taxa Gestão')
	insert into EventoFinanceiro (IdEventoFinanceiro, Descricao) values (811,'Pagamento Taxa Gestão')
	insert into EventoFinanceiro (IdEventoFinanceiro, Descricao) values (820,'Taxa Performance')
	insert into EventoFinanceiro (IdEventoFinanceiro, Descricao, ClassificacaoAnbima) values (821,'Pagamento Taxa Performance',35)
	insert into EventoFinanceiro (IdEventoFinanceiro, Descricao) values (830,'CPMF')
	insert into EventoFinanceiro (IdEventoFinanceiro, Descricao, ClassificacaoAnbima) values (840,'Taxa Fiscalização CVM',14)
	insert into EventoFinanceiro (IdEventoFinanceiro, Descricao, ClassificacaoAnbima) values (841,'Pagamento Taxa Fiscalização CVM',14)
	insert into EventoFinanceiro (IdEventoFinanceiro, Descricao, ClassificacaoAnbima) values (890,'Provisão Outros',999)
	insert into EventoFinanceiro (IdEventoFinanceiro, Descricao) values (891,'Pagamento Provisão Outros')
	insert into EventoFinanceiro (IdEventoFinanceiro, Descricao) values (851,'Taxa Custódia')
	insert into EventoFinanceiro (IdEventoFinanceiro, Descricao) values (852,'Pagamento Taxa Custódia')
	insert into EventoFinanceiro (IdEventoFinanceiro, Descricao) values (1001,'Fundos – Aplicação')
	insert into EventoFinanceiro (IdEventoFinanceiro, Descricao) values (1002,'Fundos – Resgate')
	insert into EventoFinanceiro (IdEventoFinanceiro, Descricao) values (1003,'Fundos – IR Resgate')
	insert into EventoFinanceiro (IdEventoFinanceiro, Descricao) values (1004,'Fundos – IOF Resgate')
	insert into EventoFinanceiro (IdEventoFinanceiro, Descricao) values (1005,'Fundos – Performance Fee Resgate')
	insert into EventoFinanceiro (IdEventoFinanceiro, Descricao) values (1006,'Fundos – Come Cotas')
	insert into EventoFinanceiro (IdEventoFinanceiro, Descricao) values (1010,'Fundos – Aplicação a Converter')
	insert into EventoFinanceiro (IdEventoFinanceiro, Descricao) values (1020,'Fundos – Transferência Saldo')
	insert into EventoFinanceiro (IdEventoFinanceiro, Descricao) values (1101,'Cotista – Aplicação')
	insert into EventoFinanceiro (IdEventoFinanceiro, Descricao) values (1102,'Cotista – Resgate')
	insert into EventoFinanceiro (IdEventoFinanceiro, Descricao) values (1103,'Cotista – IR Resgate')
	insert into EventoFinanceiro (IdEventoFinanceiro, Descricao) values (1104,'Cotista – IOF Resgate')
	insert into EventoFinanceiro (IdEventoFinanceiro, Descricao) values (1105,'Cotista – Performance Fee Resgate')
	insert into EventoFinanceiro (IdEventoFinanceiro, Descricao) values (1106,'Cotista – Come Cotas')
	insert into EventoFinanceiro (IdEventoFinanceiro, Descricao) values (1110,'Cotista – Aplicação a Converter')
	insert into EventoFinanceiro (IdEventoFinanceiro, Descricao) values (2000,'IR Fonte Operação')
	insert into EventoFinanceiro (IdEventoFinanceiro, Descricao) values (2001,'IR Fonte Daytrade')
	insert into EventoFinanceiro (IdEventoFinanceiro, Descricao) values (2002,'IR Renda Variável')
	insert into EventoFinanceiro (IdEventoFinanceiro, Descricao) values (3000,'Chamada Margem')
	insert into EventoFinanceiro (IdEventoFinanceiro, Descricao) values (3001,'Devolução Margem')
	insert into EventoFinanceiro (IdEventoFinanceiro, Descricao) values (4000,'Cambio – Principal Origem')
	insert into EventoFinanceiro (IdEventoFinanceiro, Descricao) values (4001,'Cambio – Principal Destino')
	insert into EventoFinanceiro (IdEventoFinanceiro, Descricao) values (4002,'Cambio – Tributos')
	insert into EventoFinanceiro (IdEventoFinanceiro, Descricao) values (4003,'Cambio - Custos')
	insert into EventoFinanceiro (IdEventoFinanceiro, Descricao) values (5000,'Ajuste Compensação Cotas')
	insert into EventoFinanceiro (IdEventoFinanceiro, Descricao) values (5001,'Ajuste Compensação Cotas – Evento')
	insert into EventoFinanceiro (IdEventoFinanceiro, Descricao, ClassificacaoAnbima) values (6001,'Valores a Crédito não Identificados',999)
	insert into EventoFinanceiro (IdEventoFinanceiro, Descricao, ClassificacaoAnbima) values (6002,'Valores a Débito não Identificados',999)
	insert into EventoFinanceiro (IdEventoFinanceiro, Descricao, ClassificacaoAnbima) values (100000,'Outros',999)
	insert into EventoFinanceiro (IdEventoFinanceiro, Descricao) values (100001,'Carteira Gerencial')


END	

IF NOT EXISTS(SELECT * FROM SYSCOLUMNS WHERE NAME = 'IdEventoFinanceiro' AND ID = OBJECT_ID('Liquidacao'))
BEGIN
	ALTER TABLE Liquidacao ADD IdEventoFinanceiro INTEGER, FOREIGN KEY(IdEventoFinanceiro) REFERENCES EventoFinanceiro(IdEventoFinanceiro);
END


IF NOT EXISTS(SELECT * FROM SYSCOLUMNS WHERE NAME = 'CotaAtual' AND ID = OBJECT_ID('CalculoPerformance'))
BEGIN
	alter table CalculoPerformance add CotaAtual decimal(28,12) NULL
END
	
IF NOT EXISTS(SELECT * FROM SYSCOLUMNS WHERE NAME = 'CotaBase' AND ID = OBJECT_ID('CalculoPerformance'))
BEGIN
	alter table CalculoPerformance add CotaBase decimal(28,12) NULL
END
	
IF NOT EXISTS(SELECT * FROM SYSCOLUMNS WHERE NAME = 'CotaCorrigida' AND ID = OBJECT_ID('CalculoPerformance'))
BEGIN
	alter table CalculoPerformance add CotaCorrigida decimal(28,12) NULL
END
	
IF NOT EXISTS(SELECT * FROM SYSCOLUMNS WHERE NAME = 'FatorIndice' AND ID = OBJECT_ID('CalculoPerformance'))
BEGIN
	alter table CalculoPerformance add FatorIndice decimal(28,12) NULL
END

IF NOT EXISTS(SELECT * FROM SYSCOLUMNS WHERE NAME = 'FatorJuros' AND ID = OBJECT_ID('CalculoPerformance'))
BEGIN
	alter table CalculoPerformance add FatorJuros decimal(28,12) NULL
END	
	
IF NOT EXISTS(SELECT * FROM SYSCOLUMNS WHERE NAME = 'QuantidadeCotas' AND ID = OBJECT_ID('CalculoPerformance'))
BEGIN
	alter table CalculoPerformance add QuantidadeCotas decimal(28,12) NULL
END	
	
IF NOT EXISTS(SELECT * FROM SYSCOLUMNS WHERE NAME = 'Rendimento' AND ID = OBJECT_ID('CalculoPerformance'))
BEGIN
	alter table CalculoPerformance add Rendimento decimal(28,12) NULL
END
	
IF NOT EXISTS(SELECT * FROM SYSCOLUMNS WHERE NAME = 'CotaAtual' AND ID = OBJECT_ID('CalculoPerformanceHistorico'))
BEGIN
	alter table CalculoPerformanceHistorico add CotaAtual decimal(28,12) NULL
END
	
IF NOT EXISTS(SELECT * FROM SYSCOLUMNS WHERE NAME = 'CotaBase' AND ID = OBJECT_ID('CalculoPerformanceHistorico'))
BEGIN
	alter table CalculoPerformanceHistorico add CotaBase decimal(28,12) NULL
END
	
IF NOT EXISTS(SELECT * FROM SYSCOLUMNS WHERE NAME = 'CotaCorrigida' AND ID = OBJECT_ID('CalculoPerformanceHistorico'))
BEGIN
	alter table CalculoPerformanceHistorico add CotaCorrigida decimal(28,12) NULL
END
	
IF NOT EXISTS(SELECT * FROM SYSCOLUMNS WHERE NAME = 'FatorIndice' AND ID = OBJECT_ID('CalculoPerformanceHistorico'))
BEGIN
	alter table CalculoPerformanceHistorico add FatorIndice decimal(28,12) NULL
END

IF NOT EXISTS(SELECT * FROM SYSCOLUMNS WHERE NAME = 'FatorJuros' AND ID = OBJECT_ID('CalculoPerformanceHistorico'))
BEGIN
	alter table CalculoPerformanceHistorico add FatorJuros decimal(28,12) NULL
END	
	
IF NOT EXISTS(SELECT * FROM SYSCOLUMNS WHERE NAME = 'QuantidadeCotas' AND ID = OBJECT_ID('CalculoPerformanceHistorico'))
BEGIN
	alter table CalculoPerformanceHistorico add QuantidadeCotas decimal(28,12) NULL
END	
	
IF NOT EXISTS(SELECT * FROM SYSCOLUMNS WHERE NAME = 'Rendimento' AND ID = OBJECT_ID('CalculoPerformanceHistorico'))
BEGIN
	alter table CalculoPerformanceHistorico add Rendimento decimal(28,12) NULL
END

