declare @data_versao char(10)
set @data_versao = '2015-06-19'
if exists (select 1 from VersaoSchema where DataVersao = @data_versao) return
insert into VersaoSchema VALUES(@data_versao)

alter table tabelacustosrendafixa add IdTitulo int null

ALTER TABLE tabelacustosrendafixa ADD CONSTRAINT FK_TabelaCustosRendaFixa_TituloRendaFixa FOREIGN KEY(IdTitulo) 
REFERENCES TituloRendaFixa(IdTitulo) ON DELETE CASCADE

ALTER TABLE tabelacustosrendafixa
ALTER COLUMN Classe int