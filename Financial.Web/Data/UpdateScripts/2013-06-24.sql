Declare @data_versao char(10)
set @data_versao = '2013-06-24'
if exists (select 1 from VersaoSchema where DataVersao = @data_versao) return
insert into VersaoSchema VALUES(@data_versao)

alter table operacaorendafixa add ValorCorretagem decimal (16, 2) null
go
update operacaorendafixa set ValorCorretagem = 0
go
alter table operacaorendafixa alter column ValorCorretagem decimal (16, 2) not null
go
ALTER TABLE operacaorendafixa ADD  CONSTRAINT DF_OperacaoRendaFixa_ValorCorretagem  DEFAULT ((0)) FOR ValorCorretagem
go