declare @data_versao char(10)
set @data_versao = '2013-02-14'
if exists (select 1 from VersaoSchema where DataVersao = @data_versao) return
insert into VersaoSchema VALUES(@data_versao)

CREATE TABLE [dbo].[TabelaCarteiraSimulada](
	[IdCarteira] [int] NOT NULL,
	[TipoAtivo] [int] NOT NULL,
	[CodigoAtivo] [varchar](20) NOT NULL,
	[Valor] [decimal](16, 2) NOT NULL,
 CONSTRAINT [PK_TabelaCarteiraSimulada] PRIMARY KEY CLUSTERED 
(
	[IdCarteira] ASC,
	[TipoAtivo] ASC,
	[CodigoAtivo] ASC
)
)
GO
ALTER TABLE [dbo].[TabelaCarteiraSimulada]  WITH CHECK ADD  CONSTRAINT [FK_TabelaCarteiraSimulada_Carteira] FOREIGN KEY([IdCarteira])
REFERENCES [dbo].[Carteira] ([IdCarteira])
GO
ALTER TABLE [dbo].[TabelaCarteiraSimulada] CHECK CONSTRAINT [FK_TabelaCarteiraSimulada_Carteira]

