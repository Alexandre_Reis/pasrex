declare @data_versao char(10)
set @data_versao = '2014-02-07'
if exists (select 1 from VersaoSchema where DataVersao = @data_versao) return
insert into VersaoSchema VALUES(@data_versao)

alter table enquadraitemacao add Bloqueado char(1) null
go
alter table enquadraitemrendafixa add Bloqueado char(1) null
go
