declare @data_versao char(10)
set @data_versao = '2014-04-03'
if exists (select 1 from VersaoSchema where DataVersao = @data_versao) return
insert into VersaoSchema VALUES(@data_versao)

CREATE TABLE [PosicaoRendaFixaDetalhe](
	[IdCliente] [int] NOT NULL,
	[IdPosicao] [int] NOT NULL,
	[DataHistorico] [datetime] NOT NULL,
	[IdTitulo] [int] NOT NULL,
	[QuantidadeBloqueada] [decimal](26, 12) NOT NULL,
	[Motivo] [varchar](500) NOT NULL,
 CONSTRAINT [PK_PosicaoRendaFixaDetalhe] PRIMARY KEY CLUSTERED 
(
	[DataHistorico] ASC,
	[IdCliente] ASC,
	[IdPosicao] ASC
)
)

ALTER TABLE [PosicaoRendaFixaDetalhe]  WITH CHECK ADD  CONSTRAINT [Cliente_PosicaoRendaFixaDetalhe_FK1] FOREIGN KEY([IdCliente])
REFERENCES [Cliente] ([IdCliente])
ON DELETE CASCADE
GO

ALTER TABLE [PosicaoRendaFixaDetalhe] CHECK CONSTRAINT [Cliente_PosicaoRendaFixaDetalhe_FK1]
GO

ALTER TABLE [PosicaoRendaFixaDetalhe]  WITH CHECK ADD  CONSTRAINT [TituloRendaFixa_PosicaoRendaFixaDetalhe_FK1] FOREIGN KEY([IdTitulo])
REFERENCES [TituloRendaFixa] ([IdTitulo])
ON DELETE CASCADE
GO

ALTER TABLE [PosicaoRendaFixaDetalhe] CHECK CONSTRAINT [TituloRendaFixa_PosicaoRendaFixaDetalhe_FK1]
GO


ALTER TABLE BloqueioRendaFixa ADD Motivo varchar(500) NULL
GO