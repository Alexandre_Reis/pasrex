declare @data_versao char(10)
set @data_versao = '2013-08-29'
if exists (select 1 from VersaoSchema where DataVersao = @data_versao) return
insert into VersaoSchema VALUES(@data_versao)

alter table operacaofundo add IdOperacaoResgatada int null
go
alter table operacaocotista add IdOperacaoResgatada int null
go
alter table ordemfundo add IdOperacaoResgatada int null
go
alter table ordemcotista add IdOperacaoResgatada int null
go
