declare @data_versao char(10)
set @data_versao = '2014-01-14'
if exists (select 1 from VersaoSchema where DataVersao = @data_versao) return
insert into VersaoSchema VALUES(@data_versao)

alter table cliente add IdLocal smallint null
go

update cliente set IdLocal = 1
go

alter table cliente alter column IdLocal smallint not null
go

ALTER TABLE cliente ADD  CONSTRAINT DF_IdLocal  DEFAULT ((1)) FOR IdLocal
GO

update cliente set idlocal = 4 where idmoeda = 2 --Atualiza para local NY onde moeda = Dolar
go