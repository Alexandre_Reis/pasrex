declare @data_versao char(10)
set @data_versao = '2015-09-16'
if exists (select 1 from VersaoSchema where DataVersao = @data_versao) return
insert into VersaoSchema VALUES(@data_versao)

--Menu Integracoes / Informacoes Complementares
INSERT INTO [permissaomenu] ([IdGrupo],[IdMenu],[PermissaoLeitura],[PermissaoAlteracao],[PermissaoExclusao],[PermissaoInclusao]) 
SELECT idgrupo,14260,'S','S','S','S' from grupousuario where idgrupo <> 0


CREATE TABLE [InformacoesComplementaresFundo](
	[IdCarteira] [int] NOT NULL,
	[DataInicioVigencia] [datetime] NOT NULL,
	[Periodicidade] [varchar](500) NULL,
	[LocalFormaDivulgacao] [varchar](500) NULL,
	[LocalFormaSolicitacaoCotista] [varchar](500) NULL,
	[FatoresRisco] [varchar](500) NULL,
	[PoliticaExercicioVoto] [varchar](500) NULL,
	[TributacaoAplicavel] [varchar](500) NULL,
	[PoliticaAdministracaoRisco] [varchar](500) NULL,
	[AgenciaClassificacaoRisco] [varchar](500) NULL,
	[RecursosServicosGestor] [varchar](500) NULL,
	[PrestadoresServicos] [varchar](500) NULL,
	[PoliticaDistribuicaoCotas] [varchar](500) NULL,
	[Observacoes] [varchar](500) NULL
 CONSTRAINT [PK_InformacoesComplementaresFundo] PRIMARY KEY CLUSTERED 
(
	[IdCarteira] ASC,
	[DataInicioVigencia] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [InformacoesComplementaresFundo]  WITH CHECK ADD  CONSTRAINT [FK_InformacoesComplementaresFundo_Carteira] FOREIGN KEY([IdCarteira])
REFERENCES [Carteira] ([IdCarteira])
GO

ALTER TABLE [InformacoesComplementaresFundo] CHECK CONSTRAINT [FK_InformacoesComplementaresFundo_Carteira]
GO