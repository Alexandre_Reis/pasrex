declare @data_versao char(10)
set @data_versao = '2014-01-27'
if exists (select 1 from VersaoSchema where DataVersao = @data_versao) return
insert into VersaoSchema VALUES(@data_versao)

CREATE TABLE TabelaInterfaceCliente(
	IdCliente int NOT NULL,
	TipoInterface int NOT NULL,
 CONSTRAINT PK_TabelaInterfaceCliente PRIMARY KEY CLUSTERED 
(
	IdCliente ASC,
	TipoInterface ASC
)
)
GO

ALTER TABLE TabelaInterfaceCliente ADD  CONSTRAINT Cliente_TabelaInterfaceCliente_FK1 FOREIGN KEY(IdCliente)
REFERENCES Cliente (IdCliente)
ON DELETE CASCADE
GO


