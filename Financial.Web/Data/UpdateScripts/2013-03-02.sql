declare @data_versao char(10)
set @data_versao = '2013-03-02'
if exists (select 1 from VersaoSchema where DataVersao = @data_versao) return
insert into VersaoSchema VALUES(@data_versao)

alter table cliente add ContabilLiquidacao tinyint null
go
update cliente set ContabilLiquidacao = 1
go


