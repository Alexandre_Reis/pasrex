declare @data_versao char(10)
set @data_versao = '2014-03-27'
if exists (select 1 from VersaoSchema where DataVersao = @data_versao) return
insert into VersaoSchema VALUES(@data_versao)

CREATE TABLE [dbo].[PermissaoOperacaoFundo](
	[IdUsuario] [int] NOT NULL,
	[IdFundo] [int] NOT NULL,
 CONSTRAINT [PermissaoOperacaoFundo_PK] PRIMARY KEY CLUSTERED 
(
	[IdFundo] ASC,
	[IdUsuario] ASC
)
)

GO

ALTER TABLE [dbo].[PermissaoOperacaoFundo]  WITH CHECK ADD  CONSTRAINT [Cliente_PermissaoOperacaoFundo_FK1] FOREIGN KEY([IdFundo])
REFERENCES [dbo].[Cliente] ([IdCliente])
ON DELETE CASCADE
GO

ALTER TABLE [dbo].[PermissaoOperacaoFundo] CHECK CONSTRAINT [Cliente_PermissaoOperacaoFundo_FK1]
GO

ALTER TABLE [dbo].[PermissaoOperacaoFundo]  WITH CHECK ADD  CONSTRAINT [Usuario_PermissaoOperacaoFundo_FK1] FOREIGN KEY([IdUsuario])
REFERENCES [dbo].[Usuario] ([IdUsuario])
ON DELETE CASCADE
GO

ALTER TABLE [dbo].[PermissaoOperacaoFundo] CHECK CONSTRAINT [Usuario_PermissaoOperacaoFundo_FK1]
GO
