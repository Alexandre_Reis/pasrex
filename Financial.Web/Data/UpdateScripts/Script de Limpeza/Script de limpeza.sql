truncate table TransferenciaBMF
GO
truncate table TransferenciaBolsa
GO
delete from SaldoCaixa
GO
delete from PerfilMTM
go
delete from TabelaEscalonamentoRF
go
delete from ParametroAdministradorFundoInvestimento
go
delete from EmpresaSecuritizada
go
delete from FundoInvestimentoFormaCondominio
go
delete from AgendaComeCotas;
go
truncate table TransferenciaEntreCotista 
GO
truncate table TributacaoCautelaFie  
GO
truncate table PosicaoBMF;
GO
truncate table PosicaoBMFAbertura;
GO
truncate table PosicaoBMFHistorico;
GO
truncate table PosicaoBolsa;
GO
truncate table PosicaoBolsaAbertura;
GO
truncate table PosicaoBolsaDetalhe;
GO
truncate table PosicaoBolsaHistorico;
GO
truncate table PosicaoCotista;
GO
truncate table PosicaoCotistaAbertura;
GO
truncate table PosicaoCotistaHistorico;
GO
truncate table PosicaoEmprestimoBolsa;
GO
truncate table PosicaoEmprestimoBolsaAbertura;
GO
truncate table PosicaoEmprestimoBolsaHistorico;
GO
truncate table PosicaoFundo;
GO
truncate table PosicaoFundoAbertura;
GO
truncate table PosicaoFundoHistorico;
GO
truncate table PosicaoRendaFixa;
GO
truncate table PosicaoRendaFixaAbertura;
GO
truncate table PosicaoRendaFixaDetalhe;
GO
truncate table PosicaoRendaFixaHistorico;
GO
delete from SubscricaoBolsa
go
delete from ConversaoBolsa
go
delete from BonificacaoBolsa
go
delete from Emissor;
go
truncate table PosicaoSwap;
GO
truncate table PosicaoSwapAbertura;
GO
truncate table PosicaoSwapHistorico;
GO
truncate table PosicaoTermoBolsa;
GO
truncate table PosicaoTermoBolsaAbertura;
GO
truncate table PosicaoTermoBolsaHistorico;
GO
truncate table DetalheResgateCotista;
GO
truncate table Ordemcotista;
GO
Truncate table PermissaoCotista;
GO
truncate table PrejuizoCotista;
GO
truncate table PrejuizoCotistaHistorico;
go
truncate table PrejuizoCotistaUsado;
go
truncate table TemplateCotista;
go
truncate table TransferenciaCota
Go
truncate table AplicacaoOfertaDistribuicaoCotas
go
truncate table CalculoAdministracao
go
truncate table CalculoAdministracaoHistorico
go
truncate table CalculoPerformance
go
truncate table CalculoPerformanceHistorico
go
truncate table CalculoProvisao
go
truncate table CalculoProvisaoHistorico
go
truncate table CalculoRebateCarteira
go
truncate table CalculoRebateCotista
go
truncate table DetalheResgateCotista
go
truncate table DetalheResgateFundo
go
truncate table EnquadraItemFundo
go
truncate table EnquadraResultadoDetalhe
go
delete from EnquadraResultado
go
truncate table EnquadraResultadoDetalhe
go
delete from EnquadraRegra
go
truncate table EnquadraResultadoDetalhe
go
truncate table HistoricoCota
go
truncate table ListaBenchmark
go
truncate table OrdemCotista
go
truncate table OrdemFundo
go
truncate table PrejuizoCotista
go
truncate table PrejuizoCotistaHistorico
go
truncate table PrejuizoCotistaUsado
go
truncate table PrejuizoFundo
go
truncate table PrejuizoFundoHistorico
go
truncate table TabelaPerfilMensalCVM
go
truncate table TabelaRebateCarteira
go
truncate table TabelaRebateDistribuidor
go
truncate table TemplateCarteira
go
truncate table TransferenciaCota
go

truncate table eventoFundoMovimentoAtivos
go
truncate table eventoFundoMovimentoCautelas
go
truncate table eventoFundoPosicaoAtivos
go
truncate table eventoFundoPosicaoCautelas
go
truncate table eventoFundoBMF
go
truncate table eventoFundoBolsa
go
truncate table eventoFundoCaixa
go
truncate table eventoFundoCautela
go
truncate table eventoFundoFundo
go
truncate table eventoFundoLiquidacao
go
truncate table eventoFundoRendaFixa
go
truncate table eventoFundoSwap
go
delete from eventoFundo
go
truncate table tributacaoFundoFie
go
truncate table agendaComeCotas
go

delete from carteira;
GO
truncate table AcumuladoCorretagemBMF
GO
truncate table ApuracaoIRImobiliario
GO
truncate table ApuracaoRendaVariavelIR
GO
truncate table BloqueioBolsa
GO
truncate table BloqueioRendaFixa
GO
truncate table BookCliente
GO
truncate table CalculoGerencial
GO
truncate table ClienteBMF
GO
truncate table ClienteBolsa
GO
truncate table ClienteInterface
GO
truncate table ClienteRendaFixa
GO
truncate table CodigoClienteAgente
GO
truncate table ContabLancamento
GO
truncate table ContabSaldo
GO
truncate table CustodiaBMF
GO
truncate table DetalheResgateFundo
GO
truncate table EventoFisicoBolsa
GO
truncate table EventoRendaFixa
GO
truncate table GerOperacaoBMF
GO
truncate table GerOperacaoBolsa
GO
truncate table GerPosicaoBMF
GO
truncate table GerPosicaoBMFAbertura
GO
truncate table GerPosicaoBMFHistorico
GO
truncate table GerPosicaoBolsa
GO
truncate table GerPosicaoBolsaAbertura
GO
truncate table GerPosicaoBolsaHistorico
GO
truncate table IRFonte
GO
truncate table Liquidacao
GO
truncate table LiquidacaoAbertura
GO
truncate table LiquidacaoEmprestimoBolsa
GO
truncate table LiquidacaoFuturo
GO
truncate table LiquidacaoHistorico
GO
truncate table LiquidacaoRendaFixa
GO
truncate table LiquidacaoSwap
GO
truncate table LiquidacaoTermoBolsa
GO
truncate table OrdemBMF
GO
truncate table OrdemTermoBolsa
go
delete from OrdemBolsa
GO
truncate table OrdemFundo
GO
truncate table OrdemRendaFixa
GO
truncate table TabelaCorretagemBMF
go
delete from  PerfilCorretagemBMF
GO
truncate table PerfilCorretagemBolsa
GO
truncate table PermissaoCliente
GO
truncate table PermissaoOperacaoFundo
GO
truncate table ProventoBolsaCliente
GO
truncate table SaldoCaixa
GO
truncate table TabelaCONR
GO
truncate table TabelaCustosRendaFixa
GO
truncate table TabelaExtratoCliente
GO
truncate table TabelaInterfaceCliente
GO
truncate table TabelaMTMRendaFixa
GO
truncate table TabelaProcessamento
GO
truncate table TabelaRebateCorretagem
GO
truncate table TabelaTaxaCustodiaBolsa
GO
truncate table TransferenciaBMF
GO
truncate table TransferenciaBolsa
GO

truncate table operacaoCambio
go
delete from  Usuario where login <> 'admin'
GO
truncate table SaldoCaixa;
go
delete from ContaCorrente;
go
delete from ConversaoBolsa
go
delete from cotista;
go
delete from cliente;
GO
truncate table PessoaDocumento;
go
truncate table PessoaEmail;
go
truncate table PessoaEndereco;
go
truncate table PessoaTelefone;
go
truncate table SuitabilityResposta;
go
truncate table TabelaRebateOfficer;
go
truncate table PerfilMTM
go
truncate table empresaSecuritizada
go
delete from SubscricaoBolsa
go
delete from ConversaoBolsa
go
delete from  Emissor
go
truncate table ParametroAdministradorFundoInvestimento
go
delete from  AgenteMercado
go
delete from banco;
GO
delete from pessoa;
GO
delete from cotista;
go
truncate table historicosenhas
go

delete from OperacaoBMF;
GO
delete from OperacaoBolsa;
GO
delete from OperacaoCotista;
GO
delete from  OperacaoEmprestimoBolsa;
GO
delete from OperacaoFundo;
GO
delete from OperacaoRendaFixa;
GO
delete from OperacaoSwap;
GO
delete from OperacaoTermoBolsa;
GO
truncate table AgendaFundo
go
delete from  Agencia
go
delete from Banco
go
delete from Estrategia
go

truncate table CadastroComplementar
go
delete from indice where IdIndice not in (1,10,20,21,22,23,24,25,26,40,45,60,61,62,63,64,70,71,73,80,81,90,91,93,94,100,101,102,103,104,105,106,107,108,109,200,210,220,230,231,400,500,501,510,520,521,522,523)
go

DELETE FROM ParametroLiqAvancadoAnalitico;
GO
DELETE FROM ParametroLiqAvancado;
GO
DELETE FROM OperacaoRendaFixa;
GO
DELETE FROM OperacaoCotista;
GO
DELETE FROM ColagemComeCotasDetalhe;
GO
delete from ClassificacaoFaixaRating
go
DELETE FROM PerfilFaixaRating;
GO
DELETE FROM OperacaoFundo;
GO
DELETE FROM ColagemComeCotasPosCotista;
GO
DELETE FROM OrdemBolsa;
GO
DELETE FROM AtivoMercadoFiltro;
GO
DELETE FROM PosicaoCotistaAlterada;
GO
DELETE FROM OperacaoEmprestimoBolsa;
GO
DELETE FROM OrdemBMF;
GO
DELETE FROM ColagemComeCotas;
GO
DELETE FROM PessoaVinculo;
GO
DELETE FROM PosicaoCotistaAux;
GO
DELETE FROM TabelaProvisao;
GO
DELETE FROM DePara;
GO
DELETE FROM TabelaExcecaoAdm;
GO
DELETE FROM GrupoProcessamento;
GO
DELETE FROM SerieRendaFixa;
GO
DELETE FROM CadastroComplementarCampos;
GO
DELETE FROM CotacaoSerie;
GO

IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'AADadosFixos') > 0
begin
	DELETE FROM AADadosFixos;
END
GO

IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'AADadosGerados') > 0
begin
	DELETE FROM AADadosGerados;
END	
GO

IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'AADadosFixosCotista') > 0
begin
	DELETE FROM AADadosFixosCotista;
END	
GO

IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'AADadosGeradosCC') > 0
begin
	DELETE FROM AADadosGeradosCC;
END	
GO

IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'AADadosFixosCC') > 0
begin
	DELETE FROM AADadosFixosCC;
END	
GO

DELETE FROM MediaMovel;
GO
DELETE FROM Rentabilidade;
GO