declare @data_versao char(10)
set @data_versao = '2015-03-11'
if exists (select 1 from VersaoSchema where DataVersao = @data_versao) return
insert into VersaoSchema VALUES(@data_versao)

INSERT INTO [permissaomenu] ([IdGrupo],[IdMenu],[PermissaoLeitura],[PermissaoAlteracao],[PermissaoExclusao],[PermissaoInclusao]) 
SELECT idgrupo,390,'S','S','S','S' from grupousuario where idgrupo <> 0
go

INSERT INTO [permissaomenu] ([IdGrupo],[IdMenu],[PermissaoLeitura],[PermissaoAlteracao],[PermissaoExclusao],[PermissaoInclusao]) 
SELECT idgrupo,6140,'S','S','S','S' from grupousuario where idgrupo <> 0
go

CREATE TABLE [dbo].[ParidadeCambial](
	[IdParidade] [int] IDENTITY(1,1) NOT NULL,
	[IdMoedaNumerador] [smallint] NOT NULL,
	[IdMoedaDenominador] [smallint] NOT NULL,
	[Descricao] [varchar](50) NOT NULL,
	[IdIndice] [smallint] NOT NULL,
 CONSTRAINT [PK_ParidadeCambial] PRIMARY KEY CLUSTERED 
(
	[IdParidade] ASC
)
)
GO

ALTER TABLE [dbo].[ParidadeCambial]  WITH CHECK ADD  CONSTRAINT [FK_ParidadeCambial_Indice] FOREIGN KEY([IdIndice])
REFERENCES [dbo].[Indice] ([IdIndice])
GO

ALTER TABLE [dbo].[ParidadeCambial] CHECK CONSTRAINT [FK_ParidadeCambial_Indice]
GO

ALTER TABLE [dbo].[ParidadeCambial]  WITH CHECK ADD  CONSTRAINT [FK_ParidadeCambial_Moeda1] FOREIGN KEY([IdMoedaDenominador])
REFERENCES [dbo].[Moeda] ([IdMoeda])
GO

ALTER TABLE [dbo].[ParidadeCambial] CHECK CONSTRAINT [FK_ParidadeCambial_Moeda1]
GO

ALTER TABLE [dbo].[ParidadeCambial]  WITH CHECK ADD  CONSTRAINT [FK_ParidadeCambial_Moeda2] FOREIGN KEY([IdMoedaNumerador])
REFERENCES [dbo].[Moeda] ([IdMoeda])
GO

ALTER TABLE [dbo].[ParidadeCambial] CHECK CONSTRAINT [FK_ParidadeCambial_Moeda2]
GO




CREATE TABLE [dbo].[OperacaoCambio](
	[IdOperacao] [int] IDENTITY(1,1) NOT NULL,
	[IdCliente] [int] NOT NULL,
	[DataRegistro] [datetime] NOT NULL,
	[DataLiquidacao] [datetime] NOT NULL,
	[TaxaCambio] [decimal](16,4) NOT NULL,
	[IdMoedaOrigem] [smallint] NOT NULL,
	[IdContaOrigem] [int] NOT NULL,
	[PrincipalOrigem] [decimal](30, 2) NOT NULL,
	[CustosOrigem] [decimal](16, 2) NOT NULL,
	[TributosOrigem] [decimal](16, 2) NOT NULL,
	[TotalOrigem] [decimal](30, 2) NOT NULL,
	[IdMoedaDestino] [smallint] NOT NULL,
	[IdContaDestino] [int] NOT NULL,
	[PrincipalDestino] [decimal](30, 2) NOT NULL,
	[CustosDestino] [decimal](16, 2) NOT NULL,
	[TributosDestino] [decimal](16, 2) NOT NULL,
	[TotalDestino] [decimal](30, 2) NOT NULL,
 CONSTRAINT [PK_OperacaoCambio] PRIMARY KEY CLUSTERED 
(
	[IdOperacao] ASC
)
)
GO

ALTER TABLE [dbo].[OperacaoCambio]  WITH CHECK ADD  CONSTRAINT [FK_OperacaoCambio_Cliente] FOREIGN KEY([IdCliente])
REFERENCES [dbo].[Cliente] ([IdCliente])
GO

ALTER TABLE [dbo].[OperacaoCambio] CHECK CONSTRAINT [FK_OperacaoCambio_Cliente]
GO

ALTER TABLE [dbo].[OperacaoCambio]  WITH CHECK ADD  CONSTRAINT [FK_OperacaoCambio_ContaCorrente1] FOREIGN KEY([IdContaOrigem])
REFERENCES [dbo].[ContaCorrente] ([IdConta])
GO

ALTER TABLE [dbo].[OperacaoCambio] CHECK CONSTRAINT [FK_OperacaoCambio_ContaCorrente1]
GO

ALTER TABLE [dbo].[OperacaoCambio]  WITH CHECK ADD  CONSTRAINT [FK_OperacaoCambio_ContaCorrente2] FOREIGN KEY([IdContaDestino])
REFERENCES [dbo].[ContaCorrente] ([IdConta])
GO

ALTER TABLE [dbo].[OperacaoCambio] CHECK CONSTRAINT [FK_OperacaoCambio_ContaCorrente2]
GO

ALTER TABLE [dbo].[OperacaoCambio]  WITH CHECK ADD  CONSTRAINT [FK_OperacaoCambio_Moeda1] FOREIGN KEY([IdMoedaDestino])
REFERENCES [dbo].[Moeda] ([IdMoeda])
GO

ALTER TABLE [dbo].[OperacaoCambio] CHECK CONSTRAINT [FK_OperacaoCambio_Moeda1]
GO

ALTER TABLE [dbo].[OperacaoCambio]  WITH CHECK ADD  CONSTRAINT [FK_OperacaoCambio_Moeda2] FOREIGN KEY([IdMoedaOrigem])
REFERENCES [dbo].[Moeda] ([IdMoeda])
GO

ALTER TABLE [dbo].[OperacaoCambio] CHECK CONSTRAINT [FK_OperacaoCambio_Moeda2]
GO

ALTER TABLE [dbo].[OperacaoCambio] ADD  CONSTRAINT [DF_OperacaoCambio_PrincipalOrigem]  DEFAULT ((0)) FOR [PrincipalOrigem]
GO

ALTER TABLE [dbo].[OperacaoCambio] ADD  CONSTRAINT [DF_OperacaoCambio_CustosOrigem]  DEFAULT ((0)) FOR [CustosOrigem]
GO

ALTER TABLE [dbo].[OperacaoCambio] ADD  CONSTRAINT [DF_OperacaoCambio_TributosOrigem]  DEFAULT ((0)) FOR [TributosOrigem]
GO

ALTER TABLE [dbo].[OperacaoCambio] ADD  CONSTRAINT [DF_OperacaoCambio_TotalOrigem]  DEFAULT ((0)) FOR [TotalOrigem]
GO

ALTER TABLE [dbo].[OperacaoCambio] ADD  CONSTRAINT [DF_OperacaoCambio_PrincipalDestino]  DEFAULT ((0)) FOR [PrincipalDestino]
GO

ALTER TABLE [dbo].[OperacaoCambio] ADD  CONSTRAINT [DF_OperacaoCambio_CustosDestino]  DEFAULT ((0)) FOR [CustosDestino]
GO

ALTER TABLE [dbo].[OperacaoCambio] ADD  CONSTRAINT [DF_OperacaoCambio_TributosDestino]  DEFAULT ((0)) FOR [TributosDestino]
GO

ALTER TABLE [dbo].[OperacaoCambio] ADD  CONSTRAINT [DF_OperacaoCambio_TotalDestino]  DEFAULT ((0)) FOR [TotalDestino]
GO


