declare @data_versao char(10)
set @data_versao = '2015-02-25'
if exists (select 1 from VersaoSchema where DataVersao = @data_versao) return
insert into VersaoSchema VALUES(@data_versao)

alter table operacaorendafixa add IsIPO char(1) null
go

update operacaorendafixa set IsIPO = 'N'
go

alter table operacaorendafixa alter column IsIPO char(1) not null
go

ALTER TABLE operacaorendafixa ADD  CONSTRAINT DF_OperacaoRendaFixa_IsIPO  DEFAULT (('N')) FOR IsIPO
GO

alter table operacaorendafixa add IdOperacaoExterna int null

