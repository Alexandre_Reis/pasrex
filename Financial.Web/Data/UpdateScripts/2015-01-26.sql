declare @data_versao char(10)
set @data_versao = '2015-01-26'
if exists (select 1 from VersaoSchema where DataVersao = @data_versao) return
insert into VersaoSchema VALUES(@data_versao)

ALTER TABLE TituloRendaFixa ALTER COLUMN Taxa decimal(25,16)


--Menu Saldos para informe de cotista
INSERT INTO [permissaomenu] ([IdGrupo],[IdMenu],[PermissaoLeitura],[PermissaoAlteracao],[PermissaoExclusao],[PermissaoInclusao]) 
SELECT idgrupo,10270,'S','S','S','S' from grupousuario where idgrupo <> 0