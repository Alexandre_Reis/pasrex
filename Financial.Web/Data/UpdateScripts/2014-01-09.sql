declare @data_versao char(10)
set @data_versao = '2014-01-09'
if exists (select 1 from VersaoSchema where DataVersao = @data_versao) return
insert into VersaoSchema VALUES(@data_versao)

alter table bloqueiobolsa add TipoCarteiraBloqueada int null
go

alter table PosicaoBolsaDetalhe add QuantidadeAbertura decimal(16, 2) NOT NULL
go