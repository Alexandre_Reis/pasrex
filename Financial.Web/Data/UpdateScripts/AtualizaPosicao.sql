UPDATE Posicaoswap SET idOperacao = null where idOperacao not in (select idoperacao from operacaoswap);
UPDATE PosicaoswapHistorico SET idOperacao = null where idOperacao not in (select idoperacao from operacaoswap);
UPDATE PosicaoswapAbertura SET idOperacao = null where idOperacao not in (select idoperacao from operacaoswap);

--CURSOR SWAP Historico
--Declara as variáveis para armazenar os valores do fetch
DECLARE @IdCliente INT,
		@TipoRegistro TINYINT,
		@NumeroContrato VARCHAR(20),
		@DataEmissao DATETIME,
		@DataVencimento DATETIME, 
		@ValorBase DECIMAL(16,2),
		@TipoPonta TINYINT,                                                                                                                       
		@IdIndice SMALLINT,                                                                                                                        
		@TaxaJuros DECIMAL(10,4),                                                                                                                       
		@Percentual DECIMAL(10,4),                                                                                                                      
		@TipoApropriacao TINYINT,                                                                                                                  
		@ContagemDias TINYINT,                                                                                                                     
		@BaseAno SMALLINT,                                                                                                                         
		@PontaContraParte TINYINT,                                                                                                             
		@IdIndiceContraParte SMALLINT,                                                                                                             
		@TaxaJurosContraParte DECIMAL(10,4), 
		@PercentualContraParte DECIMAL(10,4),                                                                                                           
		@ApropriacaoContraParte TINYINT,                                                                                                      
		@ContagemDiasContraParte TINYINT,
		@BaseAnoContraParte SMALLINT,
		@ValorParte DECIMAL(16,2),
		@ValorContraParte DECIMAL(16,2),
		@Saldo DECIMAL(16,2),
		@IdOperacao INT,
		@IdPosicao INT,
		@ComGarantia CHAR,
		@DiasUteis INT,
		@DiasCorridos INT,
		@IdEstrategia INT

DECLARE SwapCursor CURSOR FOR
SELECT  IdCliente,
		TipoRegistro,
		NumeroContrato,
		DataEmissao,
		DataVencimento, 
		ValorBase,
		TipoPonta,
		IdIndice,
		TaxaJuros,
		Percentual,
		TipoApropriacao,
		ContagemDias,
		BaseAno,
		TipoPontaContraParte,
		IdIndiceContraParte,
		TaxaJurosContraParte,
		PercentualContraParte,
		TipoApropriacaoContraParte, 
		ContagemDiasContraParte,
		BaseAnoContraParte,
		ValorParte,
		ValorContraParte,
		Saldo,
		IdOperacao,
		IdPosicao,
		ComGarantia,
		DiasUteis,
		DiasCorridos,
		IdEstrategia
FROM PosicaoSwapHistorico pos
WHERE pos.IdOperacao is null
	AND pos.DataHistorico = (select min(DataHistorico) from PosicaoSwapHistorico b where b.IdPosicao = pos.IdPosicao);

OPEN SwapCursor;
-- Armazena os valores nas variáveis
FETCH NEXT FROM SwapCursor
INTO  @IdCliente,
	  @TipoRegistro,
	  @NumeroContrato,
	  @DataEmissao,
	  @DataVencimento,
	  @ValorBase,
	  @TipoPonta,
	  @IdIndice,
	  @TaxaJuros,
	  @Percentual,
	  @TipoApropriacao,
	  @ContagemDias,
	  @BaseAno,
	  @PontaContraParte,
	  @IdIndiceContraParte,
	  @TaxaJurosContraParte,
	  @PercentualContraParte, 
	  @ApropriacaoContraParte, 
	  @ContagemDiasContraParte,
	  @BaseAnoContraParte,
	  @ValorParte,
	  @ValorContraParte,
	  @Saldo,
	  @IdOperacao,
	  @IdPosicao,
	  @ComGarantia,
	  @DiasUteis,
	  @DiasCorridos,
	  @IdEstrategia;

--Verifica se há mais linhas a serem percorridas
WHILE @@FETCH_STATUS = 0
BEGIN
	INSERT INTO OperacaoSwap(IdCliente, TipoRegistro, NumeroContrato, DataEmissao, DataVencimento, ValorBase, TipoPonta, IdIndice, TaxaJuros, Percentual, TipoApropriacao, ContagemDias,BaseAno, TipoPontaContraParte, IdIndiceContraParte,TaxaJurosContraParte,PercentualContraParte,TipoApropriacaoContraParte, ContagemDiasContraParte,BaseAnoContraParte, ValorIndicePartida, ValorIndicePartidaContraParte, DataRegistro, ComGarantia, DiasUteis, DiasCorridos, IdEstrategia) 
	VALUES(@IdCliente, @TipoRegistro, @NumeroContrato, @DataEmissao, @DataVencimento, @ValorBase, @TipoPonta,@IdIndice,@TaxaJuros,@Percentual,@TipoApropriacao,@ContagemDias, @BaseAno, @PontaContraParte, @IdIndiceContraParte, @TaxaJurosContraParte, @PercentualContraParte, @ApropriacaoContraParte, @ContagemDiasContraParte, @BaseAnoContraParte, @ValorParte, @ValorContraParte, @DataEmissao, @ComGarantia, @DiasUteis, @DiasCorridos, @IdEstrategia);  

	UPDATE PosicaoSwap 
	SET IdOperacao = (select max(OperacaoSwap.IdOperacao) FROM OperacaoSwap)
	WHERE IdPosicao = @IdPosicao;
	
	UPDATE PosicaoSwapAbertura
	SET IdOperacao = (select max(OperacaoSwap.IdOperacao) FROM OperacaoSwap)
	WHERE IdPosicao = @IdPosicao;
	
	UPDATE PosicaoSwapHistorico 
	SET IdOperacao = (select max(OperacaoSwap.IdOperacao) FROM OperacaoSwap)
	WHERE IdPosicao = @IdPosicao;

   FETCH NEXT FROM SwapCursor
   INTO @IdCliente,
	    @TipoRegistro,
	    @NumeroContrato,
	    @DataEmissao,
	    @DataVencimento,
	    @ValorBase,
	    @TipoPonta,
	    @IdIndice,
	    @TaxaJuros,
	    @Percentual,
	    @TipoApropriacao,
	    @ContagemDias,
	    @BaseAno,
	    @PontaContraParte,
	    @IdIndiceContraParte,
	    @TaxaJurosContraParte,
	    @PercentualContraParte, 
	    @ApropriacaoContraParte, 
	    @ContagemDiasContraParte,
	    @BaseAnoContraParte,
	    @ValorParte,
	    @ValorContraParte,
	    @Saldo,
	    @IdOperacao,
	    @IdPosicao,
	    @ComGarantia,
	    @DiasUteis,
	    @DiasCorridos,
	    @IdEstrategia;
END
CLOSE SwapCursor;
DEALLOCATE SwapCursor;
GO

--CURSOR SWAP Abertura
--Declara as variáveis para armazenar os valores do fetch
DECLARE @IdCliente INT,
		@TipoRegistro TINYINT,
		@NumeroContrato VARCHAR(20),
		@DataEmissao DATETIME,
		@DataVencimento DATETIME, 
		@ValorBase DECIMAL(16,2),
		@TipoPonta TINYINT,                                                                                                                       
		@IdIndice SMALLINT,                                                                                                                        
		@TaxaJuros DECIMAL(10,4),                                                                                                                       
		@Percentual DECIMAL(10,4),                                                                                                                      
		@TipoApropriacao TINYINT,                                                                                                                  
		@ContagemDias TINYINT,                                                                                                                     
		@BaseAno SMALLINT,                                                                                                                         
		@PontaContraParte TINYINT,                                                                                                             
		@IdIndiceContraParte SMALLINT,                                                                                                             
		@TaxaJurosContraParte DECIMAL(10,4), 
		@PercentualContraParte DECIMAL(10,4),                                                                                                           
		@ApropriacaoContraParte TINYINT,                                                                                                      
		@ContagemDiasContraParte TINYINT,
		@BaseAnoContraParte SMALLINT,
		@ValorParte DECIMAL(16,2),
		@ValorContraParte DECIMAL(16,2),
		@Saldo DECIMAL(16,2),
		@IdOperacao INT,
		@IdPosicao INT,
		@ComGarantia CHAR,
		@DiasUteis INT,
		@DiasCorridos INT,
		@IdEstrategia INT

DECLARE SwapCursor CURSOR FOR
SELECT  IdCliente,
		TipoRegistro,
		NumeroContrato,
		DataEmissao,
		DataVencimento, 
		ValorBase,
		TipoPonta,
		IdIndice,
		TaxaJuros,
		Percentual,
		TipoApropriacao,
		ContagemDias,
		BaseAno,
		TipoPontaContraParte,
		IdIndiceContraParte,
		TaxaJurosContraParte,
		PercentualContraParte,
		TipoApropriacaoContraParte, 
		ContagemDiasContraParte,
		BaseAnoContraParte,
		ValorParte,
		ValorContraParte,
		Saldo,
		IdOperacao,
		IdPosicao,
		ComGarantia,
		DiasUteis,
		DiasCorridos,
		IdEstrategia
FROM PosicaoSwapAbertura pos
WHERE pos.IdOperacao is null
	AND pos.DataHistorico = (select min(DataHistorico) from PosicaoSwapAbertura b where b.IdPosicao = pos.IdPosicao);

OPEN SwapCursor;
-- Armazena os valores nas variáveis
FETCH NEXT FROM SwapCursor
INTO  @IdCliente,
	  @TipoRegistro,
	  @NumeroContrato,
	  @DataEmissao,
	  @DataVencimento,
	  @ValorBase,
	  @TipoPonta,
	  @IdIndice,
	  @TaxaJuros,
	  @Percentual,
	  @TipoApropriacao,
	  @ContagemDias,
	  @BaseAno,
	  @PontaContraParte,
	  @IdIndiceContraParte,
	  @TaxaJurosContraParte,
	  @PercentualContraParte, 
	  @ApropriacaoContraParte, 
	  @ContagemDiasContraParte,
	  @BaseAnoContraParte,
	  @ValorParte,
	  @ValorContraParte,
	  @Saldo,
	  @IdOperacao,
	  @IdPosicao,
	  @ComGarantia,
	  @DiasUteis,
	  @DiasCorridos,
	  @IdEstrategia;

--Verifica se há mais linhas a serem percorridas
WHILE @@FETCH_STATUS = 0
BEGIN
	INSERT INTO OperacaoSwap(IdCliente, TipoRegistro, NumeroContrato, DataEmissao, DataVencimento, ValorBase, TipoPonta, IdIndice, TaxaJuros, Percentual, TipoApropriacao, ContagemDias,BaseAno, TipoPontaContraParte, IdIndiceContraParte,TaxaJurosContraParte,PercentualContraParte,TipoApropriacaoContraParte, ContagemDiasContraParte,BaseAnoContraParte, ValorIndicePartida, ValorIndicePartidaContraParte, DataRegistro, ComGarantia, DiasUteis, DiasCorridos, IdEstrategia) 
	VALUES(@IdCliente, @TipoRegistro, @NumeroContrato, @DataEmissao, @DataVencimento, @ValorBase, @TipoPonta,@IdIndice,@TaxaJuros,@Percentual,@TipoApropriacao,@ContagemDias, @BaseAno, @PontaContraParte, @IdIndiceContraParte, @TaxaJurosContraParte, @PercentualContraParte, @ApropriacaoContraParte, @ContagemDiasContraParte, @BaseAnoContraParte, @ValorParte, @ValorContraParte, @DataEmissao, @ComGarantia, @DiasUteis, @DiasCorridos, @IdEstrategia);  

	UPDATE PosicaoSwap 
	SET IdOperacao = (select max(OperacaoSwap.IdOperacao) FROM OperacaoSwap)
	WHERE IdPosicao = @IdPosicao;
	
	UPDATE PosicaoSwapAbertura
	SET IdOperacao = (select max(OperacaoSwap.IdOperacao) FROM OperacaoSwap)
	WHERE IdPosicao = @IdPosicao;
	
	UPDATE PosicaoSwapHistorico 
	SET IdOperacao = (select max(OperacaoSwap.IdOperacao) FROM OperacaoSwap)
	WHERE IdPosicao = @IdPosicao;

   FETCH NEXT FROM SwapCursor
   INTO @IdCliente,
	    @TipoRegistro,
	    @NumeroContrato,
	    @DataEmissao,
	    @DataVencimento,
	    @ValorBase,
	    @TipoPonta,
	    @IdIndice,
	    @TaxaJuros,
	    @Percentual,
	    @TipoApropriacao,
	    @ContagemDias,
	    @BaseAno,
	    @PontaContraParte,
	    @IdIndiceContraParte,
	    @TaxaJurosContraParte,
	    @PercentualContraParte, 
	    @ApropriacaoContraParte, 
	    @ContagemDiasContraParte,
	    @BaseAnoContraParte,
	    @ValorParte,
	    @ValorContraParte,
	    @Saldo,
	    @IdOperacao,
	    @IdPosicao,
	    @ComGarantia,
	    @DiasUteis,
	    @DiasCorridos,
	    @IdEstrategia;
END
CLOSE SwapCursor;
DEALLOCATE SwapCursor;
GO

--CURSOR SWAP
--Declara as variáveis para armazenar os valores do fetch
DECLARE @IdCliente INT,
		@TipoRegistro TINYINT,
		@NumeroContrato VARCHAR(20),
		@DataEmissao DATETIME,
		@DataVencimento DATETIME, 
		@ValorBase DECIMAL(16,2),
		@TipoPonta TINYINT,                                                                                                                       
		@IdIndice SMALLINT,                                                                                                                        
		@TaxaJuros DECIMAL(10,4),                                                                                                                       
		@Percentual DECIMAL(10,4),                                                                                                                      
		@TipoApropriacao TINYINT,                                                                                                                  
		@ContagemDias TINYINT,                                                                                                                     
		@BaseAno SMALLINT,                                                                                                                         
		@PontaContraParte TINYINT,                                                                                                             
		@IdIndiceContraParte SMALLINT,                                                                                                             
		@TaxaJurosContraParte DECIMAL(10,4), 
		@PercentualContraParte DECIMAL(10,4),                                                                                                           
		@ApropriacaoContraParte TINYINT,                                                                                                      
		@ContagemDiasContraParte TINYINT,
		@BaseAnoContraParte SMALLINT,
		@ValorParte DECIMAL(16,2),
		@ValorContraParte DECIMAL(16,2),
		@Saldo DECIMAL(16,2),
		@IdOperacao INT,
		@IdPosicao INT,
		@ComGarantia CHAR,
		@DiasUteis INT,
		@DiasCorridos INT,
		@IdEstrategia INT

DECLARE SwapCursor CURSOR FOR
SELECT  IdCliente,
		TipoRegistro,
		NumeroContrato,
		DataEmissao,
		DataVencimento, 
		ValorBase,
		TipoPonta,
		IdIndice,
		TaxaJuros,
		Percentual,
		TipoApropriacao,
		ContagemDias,
		BaseAno,
		TipoPontaContraParte,
		IdIndiceContraParte,
		TaxaJurosContraParte,
		PercentualContraParte,
		TipoApropriacaoContraParte, 
		ContagemDiasContraParte,
		BaseAnoContraParte,
		ValorParte,
		ValorContraParte,
		Saldo,
		IdOperacao,
		IdPosicao,
		ComGarantia,
		DiasUteis,
		DiasCorridos,
		IdEstrategia
FROM PosicaoSwap pos
WHERE IdOperacao is null;

OPEN SwapCursor;
-- Armazena os valores nas variáveis
FETCH NEXT FROM SwapCursor
INTO  @IdCliente,
	  @TipoRegistro,
	  @NumeroContrato,
	  @DataEmissao,
	  @DataVencimento,
	  @ValorBase,
	  @TipoPonta,
	  @IdIndice,
	  @TaxaJuros,
	  @Percentual,
	  @TipoApropriacao,
	  @ContagemDias,
	  @BaseAno,
	  @PontaContraParte,
	  @IdIndiceContraParte,
	  @TaxaJurosContraParte,
	  @PercentualContraParte, 
	  @ApropriacaoContraParte, 
	  @ContagemDiasContraParte,
	  @BaseAnoContraParte,
	  @ValorParte,
	  @ValorContraParte,
	  @Saldo,
	  @IdOperacao,
	  @IdPosicao,
	  @ComGarantia,
	  @DiasUteis,
	  @DiasCorridos,
	  @IdEstrategia

--Verifica se há mais linhas a serem percorridas
WHILE @@FETCH_STATUS = 0
BEGIN
	INSERT INTO OperacaoSwap(IdCliente, TipoRegistro, NumeroContrato, DataEmissao, DataVencimento, ValorBase, TipoPonta, IdIndice, TaxaJuros, Percentual, TipoApropriacao, ContagemDias,BaseAno, TipoPontaContraParte, IdIndiceContraParte,TaxaJurosContraParte,PercentualContraParte,TipoApropriacaoContraParte, ContagemDiasContraParte,BaseAnoContraParte, ValorIndicePartida, ValorIndicePartidaContraParte, DataRegistro, ComGarantia, DiasUteis, DiasCorridos, IdEstrategia) 
	VALUES(@IdCliente, @TipoRegistro, @NumeroContrato, @DataEmissao, @DataVencimento, @ValorBase, @TipoPonta,@IdIndice,@TaxaJuros,@Percentual,@TipoApropriacao,@ContagemDias, @BaseAno, @PontaContraParte, @IdIndiceContraParte, @TaxaJurosContraParte, @PercentualContraParte, @ApropriacaoContraParte, @ContagemDiasContraParte, @BaseAnoContraParte, @ValorParte, @ValorContraParte, @DataEmissao, @ComGarantia, @DiasUteis, @DiasCorridos, @IdEstrategia);  

	UPDATE PosicaoSwap 
	SET IdOperacao = (select max(OperacaoSwap.IdOperacao) FROM OperacaoSwap)
	WHERE IdPosicao = @IdPosicao;
	
	UPDATE PosicaoSwapAbertura
	SET IdOperacao = (select max(OperacaoSwap.IdOperacao) FROM OperacaoSwap)
	WHERE IdPosicao = @IdPosicao;
	
	UPDATE PosicaoSwapHistorico 
	SET IdOperacao = (select max(OperacaoSwap.IdOperacao) FROM OperacaoSwap)
	WHERE IdPosicao = @IdPosicao;

   FETCH NEXT FROM SwapCursor
   INTO @IdCliente,
	    @TipoRegistro,
	    @NumeroContrato,
	    @DataEmissao,
	    @DataVencimento,
	    @ValorBase,
	    @TipoPonta,
	    @IdIndice,
	    @TaxaJuros,
	    @Percentual,
	    @TipoApropriacao,
	    @ContagemDias,
	    @BaseAno,
	    @PontaContraParte,
	    @IdIndiceContraParte,
	    @TaxaJurosContraParte,
	    @PercentualContraParte, 
	    @ApropriacaoContraParte, 
	    @ContagemDiasContraParte,
	    @BaseAnoContraParte,
	    @ValorParte,
	    @ValorContraParte,
	    @Saldo,
	    @IdOperacao,
	    @IdPosicao,
	    @ComGarantia,
	    @DiasUteis,
	    @DiasCorridos,
	    @IdEstrategia;
END
CLOSE SwapCursor;
DEALLOCATE SwapCursor;
GO

UPDATE PosicaoCotista SET idOperacao = null where idOperacao not in (select idoperacao from operacaocotista);
UPDATE PosicaoCotistaHistorico SET idOperacao = null where idOperacao not in (select idoperacao from operacaocotista);
UPDATE PosicaoCotistaAbertura SET idOperacao = null where idOperacao not in (select idoperacao from operacaocotista);

--CURSOR Cotista Historico
--Declara as variáveis para armazenar os valores do fetch
BEGIN 
	DECLARE @IdCotista	INT, 
			@IdCarteira	INT,
			@ValorAplicacao DECIMAL(16,2),
			@DataAplicacao DATETIME,
			@DataConversao DATETIME,
			@Quantidade	DECIMAL(28,12),
			@CotaAplicacao DECIMAL(28,12),
			@IdOperacao INT,
			@IdPosicao INT,
			@DataDia DATETIME
	
	DECLARE CotistaCursor CURSOR FOR
	SELECT pos.IdCotista,
			pos.IdCarteira,
			pos.ValorAplicacao,
			pos.DataAplicacao,
			pos.DataConversao,
			pos.Quantidade,
			pos.CotaAplicacao,
			pos.IdOperacao,
			pos.IdPosicao,
			pos.dataAplicacao
	FROM PosicaoCotistaHistorico pos
	WHERE pos.IdOperacao is null
		AND pos.DataHistorico = (select min(DataHistorico) from PosicaoCotistaHistorico b where b.IdPosicao = pos.IdPosicao);

	OPEN CotistaCursor;
	-- Armazena os valores nas variáveis
	FETCH NEXT FROM CotistaCursor
	INTO @IdCotista, 
		@IdCarteira,
		@ValorAplicacao,
		@DataAplicacao,
		@DataConversao,
		@Quantidade,
		@CotaAplicacao,
		@IdOperacao,
		@IdPosicao,
		@DataDia 
	
	WHILE @@FETCH_STATUS = 0
	BEGIN
		INSERT INTO OperacaoCotista(IdCotista, IdCarteira, ValorBruto, Quantidade, DataOperacao, DataConversao, CotaOperacao, DataLiquidacao, DataAgendamento, TipoOperacao, IdFormaLiquidacao, Fonte, dataRegistro) 
		VALUES(@IdCotista, @IdCarteira, @ValorAplicacao, @Quantidade, @DataDia, @DataConversao,@CotaAplicacao, GetDate(),@DataAplicacao, 1, 1, 1,@DataAplicacao);  

		UPDATE PosicaoCotista
		SET IdOperacao = (select max(OperacaoCotista.IdOperacao) FROM OperacaoCotista)
		WHERE IdPosicao = @IdPosicao;
		
		UPDATE PosicaoCotistaHistorico
		SET IdOperacao = (select max(OperacaoCotista.IdOperacao) FROM OperacaoCotista)
		WHERE IdPosicao = @IdPosicao;
		
		UPDATE PosicaoCotistaAbertura
		SET IdOperacao = (select max(OperacaoCotista.IdOperacao) FROM OperacaoCotista)
		WHERE IdPosicao = @IdPosicao;

		FETCH NEXT FROM CotistaCursor
		INTO @IdCotista, 
			@IdCarteira,
			@ValorAplicacao,
			@DataAplicacao,
			@DataConversao,
			@Quantidade,
			@CotaAplicacao,
			@IdOperacao,
			@IdPosicao,
			@DataDia 
	END

	CLOSE CotistaCursor;
	DEALLOCATE CotistaCursor;
END 
GO

--CURSOR Cotista Abertura
--Declara as variáveis para armazenar os valores do fetch
BEGIN 
	DECLARE @IdCotista	INT, 
			@IdCarteira	INT,
			@ValorAplicacao DECIMAL(16,2),
			@DataAplicacao DATETIME,
			@DataConversao DATETIME,
			@Quantidade	DECIMAL(28,12),
			@CotaAplicacao DECIMAL(28,12),
			@IdOperacao INT,
			@IdPosicao INT,
			@DataDia DATETIME
	
	DECLARE CotistaCursor CURSOR FOR
	SELECT pos.IdCotista,
			pos.IdCarteira,
			pos.ValorAplicacao,
			pos.DataAplicacao,
			pos.DataConversao,
			pos.Quantidade,
			pos.CotaAplicacao,
			pos.IdOperacao,
			pos.IdPosicao,
			pos.dataAplicacao
	FROM PosicaoCotistaAbertura pos
	WHERE pos.IdOperacao is null
		AND pos.DataHistorico = (select min(DataHistorico) from PosicaoCotistaAbertura b where b.IdPosicao = pos.IdPosicao);

	OPEN CotistaCursor;
	-- Armazena os valores nas variáveis
	FETCH NEXT FROM CotistaCursor
	INTO @IdCotista, 
		@IdCarteira,
		@ValorAplicacao,
		@DataAplicacao,
		@DataConversao,
		@Quantidade,
		@CotaAplicacao,
		@IdOperacao,
		@IdPosicao,
		@DataDia 
	
	WHILE @@FETCH_STATUS = 0
	BEGIN
		INSERT INTO OperacaoCotista(IdCotista, IdCarteira, ValorBruto, Quantidade, DataOperacao, DataConversao, CotaOperacao, DataLiquidacao, DataAgendamento, TipoOperacao, IdFormaLiquidacao, Fonte, dataRegistro) 
		VALUES(@IdCotista, @IdCarteira, @ValorAplicacao, @Quantidade, @DataDia, @DataConversao,@CotaAplicacao, GetDate(),@DataAplicacao, 1, 1, 1,@DataAplicacao);  

		UPDATE PosicaoCotista
		SET IdOperacao = (select max(OperacaoCotista.IdOperacao) FROM OperacaoCotista)
		WHERE IdPosicao = @IdPosicao;
		
		UPDATE PosicaoCotistaHistorico
		SET IdOperacao = (select max(OperacaoCotista.IdOperacao) FROM OperacaoCotista)
		WHERE IdPosicao = @IdPosicao;
		
		UPDATE PosicaoCotistaAbertura
		SET IdOperacao = (select max(OperacaoCotista.IdOperacao) FROM OperacaoCotista)
		WHERE IdPosicao = @IdPosicao;

		FETCH NEXT FROM CotistaCursor
		INTO @IdCotista, 
			@IdCarteira,
			@ValorAplicacao,
			@DataAplicacao,
			@DataConversao,
			@Quantidade,
			@CotaAplicacao,
			@IdOperacao,
			@IdPosicao,
			@DataDia 
	END

	CLOSE CotistaCursor;
	DEALLOCATE CotistaCursor;
END 
GO

--CURSOR Cotista
--Declara as variáveis para armazenar os valores do fetch
BEGIN 
	DECLARE @IdCotista	INT, 
			@IdCarteira	INT,
			@ValorAplicacao DECIMAL(16,2),
			@DataAplicacao DATETIME,
			@DataConversao DATETIME,
			@Quantidade	DECIMAL(28,12),
			@CotaAplicacao DECIMAL(28,12),
			@IdOperacao INT,
			@IdPosicao INT,
			@DataDia DATETIME
	
	DECLARE CotistaCursor CURSOR FOR
	SELECT pos.IdCotista,
			pos.IdCarteira,
			pos.ValorAplicacao,
			pos.DataAplicacao,
			pos.DataConversao,
			pos.Quantidade,
			pos.CotaAplicacao,
			pos.IdOperacao,
			pos.IdPosicao,
			pos.dataAplicacao
	FROM PosicaoCotista pos
	WHERE pos.IdOperacao is null;

	OPEN CotistaCursor;
	-- Armazena os valores nas variáveis
	FETCH NEXT FROM CotistaCursor
	INTO @IdCotista, 
		@IdCarteira,
		@ValorAplicacao,
		@DataAplicacao,
		@DataConversao,
		@Quantidade,
		@CotaAplicacao,
		@IdOperacao,
		@IdPosicao,
		@DataDia 
	
	WHILE @@FETCH_STATUS = 0
	BEGIN
		INSERT INTO OperacaoCotista(IdCotista, IdCarteira, ValorBruto, Quantidade, DataOperacao, DataConversao, CotaOperacao, DataLiquidacao, DataAgendamento, TipoOperacao, IdFormaLiquidacao, Fonte, dataRegistro) 
		VALUES(@IdCotista, @IdCarteira, @ValorAplicacao, @Quantidade, @DataDia, @DataConversao,@CotaAplicacao, GetDate(),@DataAplicacao, 1, 1, 1,@DataAplicacao);  

		UPDATE PosicaoCotista
		SET IdOperacao = (select max(OperacaoCotista.IdOperacao) FROM OperacaoCotista)
		WHERE IdPosicao = @IdPosicao;
		
		UPDATE PosicaoCotistaHistorico
		SET IdOperacao = (select max(OperacaoCotista.IdOperacao) FROM OperacaoCotista)
		WHERE IdPosicao = @IdPosicao;
		
		UPDATE PosicaoCotistaAbertura
		SET IdOperacao = (select max(OperacaoCotista.IdOperacao) FROM OperacaoCotista)
		WHERE IdPosicao = @IdPosicao;

		FETCH NEXT FROM CotistaCursor
		INTO @IdCotista, 
			@IdCarteira,
			@ValorAplicacao,
			@DataAplicacao,
			@DataConversao,
			@Quantidade,
			@CotaAplicacao,
			@IdOperacao,
			@IdPosicao,
			@DataDia 
	END

	CLOSE CotistaCursor;
	DEALLOCATE CotistaCursor;
END 
GO

UPDATE PosicaoFundo SET idOperacao = null where idOperacao not in (select idoperacao from operacaoFundo);
UPDATE PosicaoFundoHistorico SET idOperacao = null where idOperacao not in (select idoperacao from operacaoFundo);
UPDATE PosicaoFundoAbertura SET idOperacao = null where idOperacao not in (select idoperacao from operacaoFundo);

--CURSOR Fundo Historico
--Declara as variáveis para armazenar os valores do fetch
BEGIN 
	DECLARE @IdCliente	INT, 
			@IdCarteira	INT,
			@ValorAplicacao DECIMAL(16,2),
			@DataAplicacao DATETIME,
			@DataConversao DATETIME,
			@Quantidade	DECIMAL(28,12),
			@CotaAplicacao DECIMAL(28,12),
			@IdOperacao INT,
			@IdPosicao INT,
			@DataDia DATETIME
	
	DECLARE FundoCursor CURSOR FOR
	SELECT pos.IdCliente,
			pos.IdCarteira,
			pos.ValorAplicacao,
			pos.DataAplicacao,
			pos.DataConversao,
			pos.Quantidade,
			pos.CotaAplicacao,
			pos.IdOperacao,
			pos.IdPosicao,
			pos.dataAplicacao
	FROM PosicaoFundoHistorico pos
	WHERE pos.IdOperacao is null
		AND pos.DataHistorico = (select min(DataHistorico) from PosicaoFundoHistorico b where b.IdPosicao = pos.IdPosicao);

	OPEN FundoCursor;
	-- Armazena os valores nas variáveis
	FETCH NEXT FROM FundoCursor
	INTO @IdCliente, 
		@IdCarteira,
		@ValorAplicacao,
		@DataAplicacao,
		@DataConversao,
		@Quantidade,
		@CotaAplicacao,
		@IdOperacao,
		@IdPosicao,
		@DataDia 
	
	WHILE @@FETCH_STATUS = 0
	BEGIN
		INSERT INTO OperacaoFundo(IdCliente, IdCarteira, ValorBruto, Quantidade, DataOperacao, DataConversao, CotaOperacao, DataLiquidacao, DataAgendamento, TipoOperacao, IdFormaLiquidacao, Fonte, dataRegistro) 
		VALUES(@IdCliente, @IdCarteira, @ValorAplicacao, @Quantidade, @DataDia, @DataConversao,@CotaAplicacao, GetDate(),@DataAplicacao, 1, 1, 2,@DataAplicacao);  

		UPDATE PosicaoFundo
		SET IdOperacao = (select max(OperacaoFundo.IdOperacao) FROM OperacaoFundo)
		WHERE IdPosicao = @IdPosicao;
		
		UPDATE PosicaoFundoHistorico
		SET IdOperacao = (select max(OperacaoFundo.IdOperacao) FROM OperacaoFundo)
		WHERE IdPosicao = @IdPosicao;
		
		UPDATE PosicaoFundoAbertura
		SET IdOperacao = (select max(OperacaoFundo.IdOperacao) FROM OperacaoFundo)
		WHERE IdPosicao = @IdPosicao;

		FETCH NEXT FROM FundoCursor
		INTO @IdCliente, 
			@IdCarteira,
			@ValorAplicacao,
			@DataAplicacao,
			@DataConversao,
			@Quantidade,
			@CotaAplicacao,
			@IdOperacao,
			@IdPosicao,
			@DataDia 
	END

	CLOSE FundoCursor;
	DEALLOCATE FundoCursor;
END 
GO

--CURSOR Fundo Historico
--Declara as variáveis para armazenar os valores do fetch
BEGIN 
	DECLARE @IdCliente	INT, 
			@IdCarteira	INT,
			@ValorAplicacao DECIMAL(16,2),
			@DataAplicacao DATETIME,
			@DataConversao DATETIME,
			@Quantidade	DECIMAL(28,12),
			@CotaAplicacao DECIMAL(28,12),
			@IdOperacao INT,
			@IdPosicao INT,
			@DataDia DATETIME
	
	DECLARE FundoCursor CURSOR FOR
	SELECT pos.IdCliente,
			pos.IdCarteira,
			pos.ValorAplicacao,
			pos.DataAplicacao,
			pos.DataConversao,
			pos.Quantidade,
			pos.CotaAplicacao,
			pos.IdOperacao,
			pos.IdPosicao,
			pos.dataAplicacao
	FROM PosicaoFundoAbertura pos
	WHERE pos.IdOperacao is null
		AND pos.DataHistorico = (select min(DataHistorico) from PosicaoFundoAbertura b where b.IdPosicao = pos.IdPosicao);

	OPEN FundoCursor;
	-- Armazena os valores nas variáveis
	FETCH NEXT FROM FundoCursor
	INTO @IdCliente, 
		@IdCarteira,
		@ValorAplicacao,
		@DataAplicacao,
		@DataConversao,
		@Quantidade,
		@CotaAplicacao,
		@IdOperacao,
		@IdPosicao,
		@DataDia 
	
	WHILE @@FETCH_STATUS = 0
	BEGIN
		INSERT INTO OperacaoFundo(IdCliente, IdCarteira, ValorBruto, Quantidade, DataOperacao, DataConversao, CotaOperacao, DataLiquidacao, DataAgendamento, TipoOperacao, IdFormaLiquidacao, Fonte, dataRegistro) 
		VALUES(@IdCliente, @IdCarteira, @ValorAplicacao, @Quantidade, @DataDia, @DataConversao,@CotaAplicacao, GetDate(),@DataAplicacao, 1, 1, 2,@DataAplicacao);  

		UPDATE PosicaoFundo
		SET IdOperacao = (select max(OperacaoFundo.IdOperacao) FROM OperacaoFundo)
		WHERE IdPosicao = @IdPosicao;
		
		UPDATE PosicaoFundoHistorico
		SET IdOperacao = (select max(OperacaoFundo.IdOperacao) FROM OperacaoFundo)
		WHERE IdPosicao = @IdPosicao;
		
		UPDATE PosicaoFundoAbertura
		SET IdOperacao = (select max(OperacaoFundo.IdOperacao) FROM OperacaoFundo)
		WHERE IdPosicao = @IdPosicao;

		FETCH NEXT FROM FundoCursor
		INTO @IdCliente, 
			@IdCarteira,
			@ValorAplicacao,
			@DataAplicacao,
			@DataConversao,
			@Quantidade,
			@CotaAplicacao,
			@IdOperacao,
			@IdPosicao,
			@DataDia 
	END

	CLOSE FundoCursor;
	DEALLOCATE FundoCursor;
END 
GO

--CURSOR Fundo
--Declara as variáveis para armazenar os valores do fetch
BEGIN 
	DECLARE @IdCliente	INT, 
			@IdCarteira	INT,
			@ValorAplicacao DECIMAL(16,2),
			@DataAplicacao DATETIME,
			@DataConversao DATETIME,
			@Quantidade	DECIMAL(28,12),
			@CotaAplicacao DECIMAL(28,12),
			@IdOperacao INT,
			@IdPosicao INT,
			@DataDia DATETIME
	
	DECLARE FundoCursor CURSOR FOR
	SELECT pos.IdCliente,
			pos.IdCarteira,
			pos.ValorAplicacao,
			pos.DataAplicacao,
			pos.DataConversao,
			pos.Quantidade,
			pos.CotaAplicacao,
			pos.IdOperacao,
			pos.IdPosicao,
			pos.dataAplicacao
	FROM PosicaoFundo pos
	WHERE pos.IdOperacao is null;

	OPEN FundoCursor;
	-- Armazena os valores nas variáveis
	FETCH NEXT FROM FundoCursor
	INTO @IdCliente, 
		@IdCarteira,
		@ValorAplicacao,
		@DataAplicacao,
		@DataConversao,
		@Quantidade,
		@CotaAplicacao,
		@IdOperacao,
		@IdPosicao,
		@DataDia 
	
	WHILE @@FETCH_STATUS = 0
	BEGIN
		INSERT INTO OperacaoFundo(IdCliente, IdCarteira, ValorBruto, Quantidade, DataOperacao, DataConversao, CotaOperacao, DataLiquidacao, DataAgendamento, TipoOperacao, IdFormaLiquidacao, Fonte, dataRegistro) 
		VALUES(@IdCliente, @IdCarteira, @ValorAplicacao, @Quantidade, @DataDia, @DataConversao,@CotaAplicacao, GetDate(),@DataAplicacao, 1, 1, 2,@DataAplicacao);  

		UPDATE PosicaoFundo
		SET IdOperacao = (select max(OperacaoFundo.IdOperacao) FROM OperacaoFundo)
		WHERE IdPosicao = @IdPosicao;
		
		UPDATE PosicaoFundoHistorico
		SET IdOperacao = (select max(OperacaoFundo.IdOperacao) FROM OperacaoFundo)
		WHERE IdPosicao = @IdPosicao;
		
		UPDATE PosicaoFundoAbertura
		SET IdOperacao = (select max(OperacaoFundo.IdOperacao) FROM OperacaoFundo)
		WHERE IdPosicao = @IdPosicao;

		FETCH NEXT FROM FundoCursor
		INTO @IdCliente, 
			@IdCarteira,
			@ValorAplicacao,
			@DataAplicacao,
			@DataConversao,
			@Quantidade,
			@CotaAplicacao,
			@IdOperacao,
			@IdPosicao,
			@DataDia 
	END

	CLOSE FundoCursor;
	DEALLOCATE FundoCursor;
END 
GO


IF NOT EXISTS(select 1 from syscolumns where id = object_id('PosicaoFundo') and name = 'IdOperacao')
BEGIN
	ALTER TABLE PosicaoFundo ADD IdOperacao int NOT NULL;
END
GO		
	
if not EXISTS(select 1 from sysconstraints where object_name(id) = 'PosicaoFundo' and object_name(constid) = 'Operacao_PosicaoFundo_FK1')
Begin
	ALTER TABLE PosicaoFundo
	ADD CONSTRAINT Operacao_PosicaoFundo_FK1
	FOREIGN KEY (IdOperacao)
	REFERENCES OperacaoFundo(IdOPeracao);
END
GO

IF NOT EXISTS(select 1 from syscolumns where id = object_id('PosicaoSwap') and name = 'IdOperacao')
BEGIN
	ALTER TABLE PosicaoSwap ADD IdOperacao int NOT NULL;
END
GO
	
if not EXISTS(select 1 from sysconstraints where object_name(id) = 'PosicaoSwap' and object_name(constid) = 'Operacao_PosicaoSwap_FK1')
Begin
	ALTER TABLE PosicaoSwap
	ADD CONSTRAINT Operacao_PosicaoSwap_FK1
	FOREIGN KEY (IdOperacao)
	REFERENCES OperacaoSwap(IdOPeracao);
END
GO

if  EXISTS(select 1 from sysconstraints where object_name(id) = 'PosicaoCotista' and object_name(constid) = 'Operacao_PosicaoCotista_FK1')
Begin
	ALTER TABLE PosicaoCotista
	DROP CONSTRAINT Operacao_PosicaoCotista_FK1
END
GO	

IF NOT EXISTS(select 1 from syscolumns where id = object_id('PosicaoCotista') and name = 'IdOperacao')
BEGIN
	ALTER TABLE PosicaoCotista ADD IdOperacao int NOT NULL;
END
	
if not EXISTS(select 1 from sysconstraints where object_name(id) = 'PosicaoCotista' and object_name(constid) = 'Operacao_PosicaoCotista_FK1')
Begin
	ALTER TABLE PosicaoCotista
	ADD CONSTRAINT Operacao_PosicaoCotista_FK1
	FOREIGN KEY (IdOperacao)
	REFERENCES OperacaoCotista(IdOPeracao);
END
GO



UPDATE PosicaoRendaFixa SET idOperacao = null where idOperacao not in (select idoperacao from operacaoRendaFixa);
UPDATE PosicaoRendaFixaHistorico SET idOperacao = null where idOperacao not in (select idoperacao from operacaoRendaFixa);
UPDATE PosicaoRendaFixaAbertura SET idOperacao = null where idOperacao not in (select idoperacao from operacaoRendaFixa);

--CURSOR RENDA FIXA Historico
--Declara as variÃ¡veis para armazenar os valores do fetch
DECLARE @IdCliente	INT, 
		@IdTitulo	INT,
		@TipoOperacao INT,
		@Quantidade DECIMAL(25,12),
		@QuantidadeBloqueada DECIMAL(16,2),
		@DataOperacao	DATETIME,
		@PUOperacao	DECIMAL(25,12),
		@TaxaOperacao	DECIMAL(25,16),
		@PUMercado	DECIMAL(25,12),
		@DataVolta	DATETIME,
		@TaxaVolta	DECIMAL(8,4),
		@PUVolta	DECIMAL(25,12),
		@IdCustodia TINYINT,
		@TipoNegociacao INT,
		@IdPosicao INT,
		@IdOperacao INT,		
		@IdAgenteCorretora int,
		@IdAgenteCustodia int,
		@IdLocalNegociacao INT

DECLARE RendaFixaCursor CURSOR FOR
SELECT pos.IdCliente,
	   pos.IdTitulo, 
	   pos.TipoOperacao,
	   pos.Quantidade,
	   pos.QuantidadeBloqueada,
	   pos.DataOperacao,
	   pos.PUOperacao,
	   pos.TaxaOperacao,
	   pos.PUMercado,
	   pos.DataVolta,
	   pos.TaxaVolta,
	   pos.PUVolta,
	   pos.IdCustodia,
	   pos.TipoNegociacao,
	   pos.IdPosicao,
	   pos.IdOperacao,  
	   pos.IdCorretora,
	   pos.IdAgente,
	   pap.IdLocalNegociacao
FROM PosicaoRendaFixaHistorico pos
inner join titulorendaFixa tit on tit.IdTitulo = pos.IdTitulo
inner join PapelRendaFixa pap on tit.idPapel = pap.idPapel
WHERE
	pos.DataHistorico = (select min(DataHistorico) from PosicaoRendaFixaHistorico b where b.IdPosicao = pos.IdPosicao) AND
	pos.IdOperacao IS NULL;

OPEN RendaFixaCursor;
-- Armazena os valores nas variáveis
FETCH NEXT FROM RendaFixaCursor
INTO @IdCliente, 
	 @IdTitulo,
	 @TipoOperacao,
	 @Quantidade ,
	 @QuantidadeBloqueada,
	 @DataOperacao,
	 @PUOperacao,
	 @TaxaOperacao,
	 @PUMercado,
	 @DataVolta,
	 @TaxaVolta,
	 @PUVolta,
	 @IdCustodia,
	 @TipoNegociacao,
	 @IdPosicao,
	 @IdOperacao,
	 @IdAgenteCorretora,	 
	 @IdAgenteCustodia,
	 @IdLocalNegociacao
	 
--Verifica se há mais linhas a serem percorridas
WHILE @@FETCH_STATUS = 0
BEGIN
	INSERT INTO OperacaoRendaFixa(IdCliente, IdTitulo, TipoOperacao, Quantidade, DataOperacao, PUOperacao, TaxaOperacao, DataVolta, TaxaVolta, PUVolta, IdCustodia, DataLiquidacao, Valor,Fonte, TipoNegociacao, IdLiquidacao, DataRegistro, IdLocalNegociacao, IdAgenteCorretora, IdAgenteCustodia) 
	VALUES(@IdCliente, @IdTitulo, @TipoOperacao, @Quantidade, @DataOperacao, @PUOperacao,@TaxaOperacao,@DataVolta,@TaxaVolta,@PUVolta,@IdCustodia, @DataOperacao, (@Quantidade*@PUOperacao), 1, @TipoNegociacao, 0,@DataOperacao, @IdLocalNegociacao, @IdAgenteCorretora, @IdAgenteCustodia);
	
	UPDATE PosicaoRendaFixa 
	SET IdOperacao = (select max(OperacaoRendaFixa.IdOperacao) FROM OperacaoRendaFixa)
	WHERE IdPosicao = @IdPosicao; 
		
	UPDATE PosicaoRendaFixaHistorico
	SET IdOperacao = (select max(OperacaoRendaFixa.IdOperacao) FROM OperacaoRendaFixa)
	WHERE IdPosicao = @IdPosicao; 

	UPDATE PosicaoRendaFixaAbertura
	SET IdOperacao = (select max(OperacaoRendaFixa.IdOperacao) FROM OperacaoRendaFixa)
	WHERE IdPosicao = @IdPosicao; 	

   FETCH NEXT FROM RendaFixaCursor
   INTO @IdCliente, 
		@IdTitulo,
		@TipoOperacao,
		@Quantidade ,
		@QuantidadeBloqueada,
		@DataOperacao,
		@PUOperacao,
		@TaxaOperacao,
		@PUMercado,
		@DataVolta,
		@TaxaVolta,
		@PUVolta,
		@IdCustodia,
		@TipoNegociacao,
		@IdPosicao,
		@IdOperacao,
		@IdAgenteCorretora,	 
		@IdAgenteCustodia,
		@IdLocalNegociacao;
END
CLOSE RendaFixaCursor;
DEALLOCATE RendaFixaCursor;
GO

--CURSOR RENDA FIXA Abertura
--Declara as variÃ¡veis para armazenar os valores do fetch
DECLARE @IdCliente	INT, 
		@IdTitulo	INT,
		@TipoOperacao INT,
		@Quantidade DECIMAL(25,12),
		@QuantidadeBloqueada DECIMAL(16,2),
		@DataOperacao	DATETIME,
		@PUOperacao	DECIMAL(25,12),
		@TaxaOperacao	DECIMAL(25,16),
		@PUMercado	DECIMAL(25,12),
		@DataVolta	DATETIME,
		@TaxaVolta	DECIMAL(8,4),
		@PUVolta	DECIMAL(25,12),
		@IdCustodia TINYINT,
		@TipoNegociacao INT,
		@IdPosicao INT,
		@IdOperacao INT,		
		@IdAgenteCorretora int,
		@IdAgenteCustodia int,
		@IdLocalNegociacao INT

DECLARE RendaFixaCursor CURSOR FOR
SELECT pos.IdCliente,
	   pos.IdTitulo, 
	   pos.TipoOperacao,
	   pos.Quantidade,
	   pos.QuantidadeBloqueada,
	   pos.DataOperacao,
	   pos.PUOperacao,
	   pos.TaxaOperacao,
	   pos.PUMercado,
	   pos.DataVolta,
	   pos.TaxaVolta,
	   pos.PUVolta,
	   pos.IdCustodia,
	   pos.TipoNegociacao,
	   pos.IdPosicao,
	   pos.IdOperacao,  
	   pos.IdCorretora,
	   pos.IdAgente,
	   pap.IdLocalNegociacao
FROM PosicaoRendaFixaAbertura pos
inner join titulorendaFixa tit on tit.IdTitulo = pos.IdTitulo
inner join PapelRendaFixa pap on tit.idPapel = pap.idPapel
WHERE pos.DataHistorico = (select min(DataHistorico) from PosicaoRendaFixaAbertura b where b.IdPosicao = pos.IdPosicao) AND
	pos.IdOperacao IS NULL;

OPEN RendaFixaCursor;
-- Armazena os valores nas variáveis
FETCH NEXT FROM RendaFixaCursor
INTO @IdCliente, 
	 @IdTitulo,
	 @TipoOperacao,
	 @Quantidade ,
	 @QuantidadeBloqueada,
	 @DataOperacao,
	 @PUOperacao,
	 @TaxaOperacao,
	 @PUMercado,
	 @DataVolta,
	 @TaxaVolta,
	 @PUVolta,
	 @IdCustodia,
	 @TipoNegociacao,
	 @IdPosicao,
	 @IdOperacao,
	 @IdAgenteCorretora,	 
	 @IdAgenteCustodia,
	 @IdLocalNegociacao
	 
--Verifica se há mais linhas a serem percorridas
WHILE @@FETCH_STATUS = 0
BEGIN
	INSERT INTO OperacaoRendaFixa(IdCliente, IdTitulo, TipoOperacao, Quantidade, DataOperacao, PUOperacao, TaxaOperacao, DataVolta, TaxaVolta, PUVolta, IdCustodia, DataLiquidacao, Valor,Fonte, TipoNegociacao, IdLiquidacao, DataRegistro, IdLocalNegociacao, IdAgenteCorretora, IdAgenteCustodia) 
	VALUES(@IdCliente, @IdTitulo, @TipoOperacao, @Quantidade, @DataOperacao, @PUOperacao,@TaxaOperacao,@DataVolta,@TaxaVolta,@PUVolta,@IdCustodia, @DataOperacao, (@Quantidade*@PUOperacao), 1, @TipoNegociacao, 0,@DataOperacao, @IdLocalNegociacao, @IdAgenteCorretora, @IdAgenteCustodia);
	
	UPDATE PosicaoRendaFixa 
	SET IdOperacao = (select max(OperacaoRendaFixa.IdOperacao) FROM OperacaoRendaFixa)
	WHERE IdPosicao = @IdPosicao; 
		
	UPDATE PosicaoRendaFixaHistorico
	SET IdOperacao = (select max(OperacaoRendaFixa.IdOperacao) FROM OperacaoRendaFixa)
	WHERE IdPosicao = @IdPosicao; 

	UPDATE PosicaoRendaFixaAbertura
	SET IdOperacao = (select max(OperacaoRendaFixa.IdOperacao) FROM OperacaoRendaFixa)
	WHERE IdPosicao = @IdPosicao; 	

   FETCH NEXT FROM RendaFixaCursor
   INTO @IdCliente, 
		@IdTitulo,
		@TipoOperacao,
		@Quantidade ,
		@QuantidadeBloqueada,
		@DataOperacao,
		@PUOperacao,
		@TaxaOperacao,
		@PUMercado,
		@DataVolta,
		@TaxaVolta,
		@PUVolta,
		@IdCustodia,
		@TipoNegociacao,
		@IdPosicao,
		@IdOperacao,
		@IdAgenteCorretora,	 
		@IdAgenteCustodia,
		@IdLocalNegociacao;
END
CLOSE RendaFixaCursor;
DEALLOCATE RendaFixaCursor;
GO

--CURSOR RENDA FIXA 
--Declara as variÃ¡veis para armazenar os valores do fetch
DECLARE @IdCliente	INT, 
		@IdTitulo	INT,
		@TipoOperacao INT,
		@Quantidade DECIMAL(25,12),
		@QuantidadeBloqueada DECIMAL(16,2),
		@DataOperacao	DATETIME,
		@PUOperacao	DECIMAL(25,12),
		@TaxaOperacao	DECIMAL(25,16),
		@PUMercado	DECIMAL(25,12),
		@DataVolta	DATETIME,
		@TaxaVolta	DECIMAL(8,4),
		@PUVolta	DECIMAL(25,12),
		@IdCustodia TINYINT,
		@TipoNegociacao INT,
		@IdPosicao INT,
		@IdOperacao INT,		
		@IdAgenteCorretora int,
		@IdAgenteCustodia int,
		@IdLocalNegociacao INT

DECLARE RendaFixaCursor CURSOR FOR
SELECT pos.IdCliente,
	   pos.IdTitulo, 
	   pos.TipoOperacao,
	   pos.Quantidade,
	   pos.QuantidadeBloqueada,
	   pos.DataOperacao,
	   pos.PUOperacao,
	   pos.TaxaOperacao,
	   pos.PUMercado,
	   pos.DataVolta,
	   pos.TaxaVolta,
	   pos.PUVolta,
	   pos.IdCustodia,
	   pos.TipoNegociacao,
	   pos.IdPosicao,
	   pos.IdOperacao,  
	   pos.IdCorretora,
	   pos.IdAgente,
	   pap.IdLocalNegociacao
FROM PosicaoRendaFixa pos
inner join titulorendaFixa tit on tit.IdTitulo = pos.IdTitulo
inner join PapelRendaFixa pap on tit.idPapel = pap.idPapel
WHERE pos.IdOperacao IS NULL;

OPEN RendaFixaCursor;
-- Armazena os valores nas variáveis
FETCH NEXT FROM RendaFixaCursor
INTO @IdCliente, 
	 @IdTitulo,
	 @TipoOperacao,
	 @Quantidade ,
	 @QuantidadeBloqueada,
	 @DataOperacao,
	 @PUOperacao,
	 @TaxaOperacao,
	 @PUMercado,
	 @DataVolta,
	 @TaxaVolta,
	 @PUVolta,
	 @IdCustodia,
	 @TipoNegociacao,
	 @IdPosicao,
	 @IdOperacao,
	 @IdAgenteCorretora,	 
	 @IdAgenteCustodia,
	 @IdLocalNegociacao
	 
--Verifica se há mais linhas a serem percorridas
WHILE @@FETCH_STATUS = 0
BEGIN
	INSERT INTO OperacaoRendaFixa(IdCliente, IdTitulo, TipoOperacao, Quantidade, DataOperacao, PUOperacao, TaxaOperacao, DataVolta, TaxaVolta, PUVolta, IdCustodia, DataLiquidacao, Valor,Fonte, TipoNegociacao, IdLiquidacao, DataRegistro, IdLocalNegociacao, IdAgenteCorretora, IdAgenteCustodia) 
	VALUES(@IdCliente, @IdTitulo, @TipoOperacao, @Quantidade, @DataOperacao, @PUOperacao,@TaxaOperacao,@DataVolta,@TaxaVolta,@PUVolta,@IdCustodia, @DataOperacao, (@Quantidade*@PUOperacao), 1, @TipoNegociacao, 0,@DataOperacao, @IdLocalNegociacao, @IdAgenteCorretora, @IdAgenteCustodia);
	
	UPDATE PosicaoRendaFixa 
	SET IdOperacao = (select max(OperacaoRendaFixa.IdOperacao) FROM OperacaoRendaFixa)
	WHERE IdPosicao = @IdPosicao; 
		
	UPDATE PosicaoRendaFixaHistorico
	SET IdOperacao = (select max(OperacaoRendaFixa.IdOperacao) FROM OperacaoRendaFixa)
	WHERE IdPosicao = @IdPosicao; 

	UPDATE PosicaoRendaFixaAbertura
	SET IdOperacao = (select max(OperacaoRendaFixa.IdOperacao) FROM OperacaoRendaFixa)
	WHERE IdPosicao = @IdPosicao; 	

   FETCH NEXT FROM RendaFixaCursor
   INTO @IdCliente, 
		@IdTitulo,
		@TipoOperacao,
		@Quantidade ,
		@QuantidadeBloqueada,
		@DataOperacao,
		@PUOperacao,
		@TaxaOperacao,
		@PUMercado,
		@DataVolta,
		@TaxaVolta,
		@PUVolta,
		@IdCustodia,
		@TipoNegociacao,
		@IdPosicao,
		@IdOperacao,
		@IdAgenteCorretora,	 
		@IdAgenteCustodia,
		@IdLocalNegociacao;
END
CLOSE RendaFixaCursor;
DEALLOCATE RendaFixaCursor;
GO
