declare @data_versao char(10)
set @data_versao = '2014-10-30'
if exists (select 1 from VersaoSchema where DataVersao = @data_versao) return
insert into VersaoSchema VALUES(@data_versao)

alter table operacaorendafixa add RendimentoNaoTributavel decimal (16, 2) null
go
ALTER TABLE operacaorendafixa ADD  CONSTRAINT DF_OperacaoRendaFixa_RendimentoNaoTributavel  DEFAULT ((0)) FOR RendimentoNaoTributavel
GO
alter table operacaorendafixa add Status tinyint null
go
ALTER TABLE operacaorendafixa ADD  CONSTRAINT DF_OperacaoRendaFixa_Status  DEFAULT ((0)) FOR Status
GO
update operacaorendafixa set RendimentoNaoTributavel = 0
go
update operacaorendafixa set Status = 1
go