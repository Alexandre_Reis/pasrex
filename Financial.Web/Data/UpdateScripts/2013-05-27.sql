declare @data_versao char(10)
set @data_versao = '2013-05-27'
if exists (select 1 from VersaoSchema where DataVersao = @data_versao) return
insert into VersaoSchema VALUES(@data_versao)

alter table cliente add Aux int null
go
update cliente set Aux = 2
go
update cliente set DescontaTributoPL = Aux
go
alter table cliente alter column DescontaTributoPL tinyint not null
go
alter table cliente drop column Aux
go


