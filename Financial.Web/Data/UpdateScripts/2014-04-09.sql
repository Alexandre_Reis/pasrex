declare @data_versao char(10)
set @data_versao = '2014-04-09'
if exists (select 1 from VersaoSchema where DataVersao = @data_versao) return
insert into VersaoSchema VALUES(@data_versao)

ALTER TABLE OperacaoRendaFixa ADD IdOperacaoVinculo int NULL
ALTER TABLE Carteira add HorarioInicioResgate datetime null
ALTER TABLE Carteira add HorarioFimResgate datetime null