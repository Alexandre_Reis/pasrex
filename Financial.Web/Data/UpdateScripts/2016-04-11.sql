declare @data_versao char(10)
set @data_versao = '2016-04-11'

/* 
*  caso este script ja tenha sido executado anteriormente 
*  n�o permite que seja executado novamente, avisa o usuario e 
*  n�o exibe nenhuma outra saida
*/
if exists(select * from VersaoSchema where DataVersao = @data_versao)
begin
	
	-- avisa o usuario em caso de erro
	raiserror('Aten��o este script ja foi executado anteriormente.',16,1)

	
	-- n�o mostra a execu��o do script
	set nocount on 
	
	-- n�o roda o script
	set noexec on 
	

end

begin transaction

	-- n�o permite que a transaction seja commitada em caso de erros
	set xact_abort on	
	
	insert into VersaoSchema values(@data_versao)

	IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'EventoFinanceiro') = 0
	begin
		CREATE TABLE [dbo].[EventoFinanceiro](
	[IdEventoFinanceiro] [int] NOT NULL,
	[Descricao] [nvarchar](200) NULL,
	[ClassificacaoAnbima] [int] NULL,
	[IdEventoProvisao] [int] NULL,
	[IdEventoPagamento] [int] NULL,
 CONSTRAINT [PK_EventoFinanceiro] PRIMARY KEY CLUSTERED 
(
	[IdEventoFinanceiro] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

--GO

ALTER TABLE [dbo].[EventoFinanceiro]  WITH CHECK ADD  CONSTRAINT [FK_EventoFinanceiro_ContabRoteiro] FOREIGN KEY([IdEventoPagamento])
REFERENCES [dbo].[ContabRoteiro] ([IdEvento])
--GO

ALTER TABLE [dbo].[EventoFinanceiro] CHECK CONSTRAINT [FK_EventoFinanceiro_ContabRoteiro]
--GO

ALTER TABLE [dbo].[EventoFinanceiro]  WITH CHECK ADD  CONSTRAINT [FK_EventoFinanceiro_ContabRoteiro1] FOREIGN KEY([IdEventoProvisao])
REFERENCES [dbo].[ContabRoteiro] ([IdEvento])
--GO

ALTER TABLE [dbo].[EventoFinanceiro] CHECK CONSTRAINT [FK_EventoFinanceiro_ContabRoteiro1]
--GO



	insert into EventoFinanceiro (IdEventoFinanceiro, Descricao, ClassificacaoAnbima) values (1,'Bolsa - Compra A��es',21)
insert into EventoFinanceiro (IdEventoFinanceiro, Descricao, ClassificacaoAnbima) values (2,'Bolsa - Venda A��es',21)
insert into EventoFinanceiro (IdEventoFinanceiro, Descricao, ClassificacaoAnbima) values (3,'Bolsa - Compra Op��es',21)
insert into EventoFinanceiro (IdEventoFinanceiro, Descricao, ClassificacaoAnbima) values (4,'Bolsa - Venda Op��es',21)
insert into EventoFinanceiro (IdEventoFinanceiro, Descricao, ClassificacaoAnbima) values (5,'Bolsa - Exerc�cio Compra',21)
insert into EventoFinanceiro (IdEventoFinanceiro, Descricao, ClassificacaoAnbima) values (6,'Bolsa - Exerc�cio Venda',21)
insert into EventoFinanceiro (IdEventoFinanceiro, Descricao, ClassificacaoAnbima) values (7,'Bolsa - Compra Termo',23)
insert into EventoFinanceiro (IdEventoFinanceiro, Descricao, ClassificacaoAnbima) values (8,'Bolsa - Venda Termo',23)
insert into EventoFinanceiro (IdEventoFinanceiro, Descricao, ClassificacaoAnbima) values (9,'Bolsa - Antecipa��o Termo',23)
insert into EventoFinanceiro (IdEventoFinanceiro, Descricao, ClassificacaoAnbima) values (10,'Bolsa - Empr�stimo Doado',31)
insert into EventoFinanceiro (IdEventoFinanceiro, Descricao, ClassificacaoAnbima) values (11,'Bolsa - Empr�stimo Tomado',31)
insert into EventoFinanceiro (IdEventoFinanceiro, Descricao, ClassificacaoAnbima) values (12,'Bolsa - Antecipa��o Empr�stimo',31)
insert into EventoFinanceiro (IdEventoFinanceiro, Descricao, ClassificacaoAnbima) values (13,'Bolsa - Ajuste Opera��o Futuro',21)
insert into EventoFinanceiro (IdEventoFinanceiro, Descricao, ClassificacaoAnbima) values (14,'Bolsa - Ajuste Posi��o Futuro',21)
insert into EventoFinanceiro (IdEventoFinanceiro, Descricao, ClassificacaoAnbima) values (15,'Bolsa - Liquida��o Final Futuro',21)
insert into EventoFinanceiro (IdEventoFinanceiro, Descricao, ClassificacaoAnbima) values (16,'Bolsa - Despesas Taxas',39)
insert into EventoFinanceiro (IdEventoFinanceiro, Descricao, ClassificacaoAnbima) values (17,'Bolsa - Corretagem',36)
insert into EventoFinanceiro (IdEventoFinanceiro, Descricao, ClassificacaoAnbima) values (18,'Bolsa - Dividendo',27)
insert into EventoFinanceiro (IdEventoFinanceiro, Descricao, ClassificacaoAnbima) values (19,'Bolsa - Juros Capital',28)
insert into EventoFinanceiro (IdEventoFinanceiro, Descricao, ClassificacaoAnbima) values (20,'Bolsa - Rendimento',27)
insert into EventoFinanceiro (IdEventoFinanceiro, Descricao, ClassificacaoAnbima) values (21,'Bolsa - Restitui��o Capital',21)
insert into EventoFinanceiro (IdEventoFinanceiro, Descricao, ClassificacaoAnbima) values (22,'Bolsa - Cr�dito Fra��es A��es',21)
insert into EventoFinanceiro (IdEventoFinanceiro, Descricao, ClassificacaoAnbima) values (23,'Bolsa - IR Proventos',27)
insert into EventoFinanceiro (IdEventoFinanceiro, Descricao, ClassificacaoAnbima) values (24,'Bolsa - Outros Proventos',27)
insert into EventoFinanceiro (IdEventoFinanceiro, Descricao, ClassificacaoAnbima) values (25,'Bolsa - Amortiza��o',21)
insert into EventoFinanceiro (IdEventoFinanceiro, Descricao, ClassificacaoAnbima) values (40,'Bolsa - Taxa Cust�dia',15)
insert into EventoFinanceiro (IdEventoFinanceiro, Descricao, ClassificacaoAnbima) values (41,'Bolsa - Dividendo Distribui��o',27)
insert into EventoFinanceiro (IdEventoFinanceiro, Descricao, ClassificacaoAnbima) values (100,'Bolsa - Outros',999)
insert into EventoFinanceiro (IdEventoFinanceiro, Descricao, ClassificacaoAnbima) values (201,'BMF � Compra � Vista',22)
insert into EventoFinanceiro (IdEventoFinanceiro, Descricao, ClassificacaoAnbima) values (202,'BMF � Venda � Vista',22)
insert into EventoFinanceiro (IdEventoFinanceiro, Descricao, ClassificacaoAnbima) values (203,'BMF � Compra Op��es',22)
insert into EventoFinanceiro (IdEventoFinanceiro, Descricao, ClassificacaoAnbima) values (204,'BMF � Venda Op��es',22)
insert into EventoFinanceiro (IdEventoFinanceiro, Descricao, ClassificacaoAnbima) values (205,'BMF � Exerc�cio Compra',22)
insert into EventoFinanceiro (IdEventoFinanceiro, Descricao, ClassificacaoAnbima) values (206,'BMF � Exerc�cio Venda',22)
insert into EventoFinanceiro (IdEventoFinanceiro, Descricao, ClassificacaoAnbima) values (220,'BMF � Despesas Taxas',41)
insert into EventoFinanceiro (IdEventoFinanceiro, Descricao, ClassificacaoAnbima) values (221,'BMF � Corretagem',37)
insert into EventoFinanceiro (IdEventoFinanceiro, Descricao, ClassificacaoAnbima) values (222,'BMF � Emolumentos',38)
insert into EventoFinanceiro (IdEventoFinanceiro, Descricao, ClassificacaoAnbima) values (223,'BMF � Registro',41)
insert into EventoFinanceiro (IdEventoFinanceiro, Descricao, ClassificacaoAnbima) values (224,'BMF � Outros Custos',41)
insert into EventoFinanceiro (IdEventoFinanceiro, Descricao, ClassificacaoAnbima) values (225,'BMF � Ajuste Posi��o',22)
insert into EventoFinanceiro (IdEventoFinanceiro, Descricao, ClassificacaoAnbima) values (250,'BMF � Ajuste Posi��o',22)
insert into EventoFinanceiro (IdEventoFinanceiro, Descricao, ClassificacaoAnbima) values (251,'BMF � Ajuste Opera��o',22)
insert into EventoFinanceiro (IdEventoFinanceiro, Descricao, ClassificacaoAnbima) values (260,'BMF � Taxa Perman�ncia',41)
insert into EventoFinanceiro (IdEventoFinanceiro, Descricao, ClassificacaoAnbima) values (270,'BMF � Taxa Clearing',999)
insert into EventoFinanceiro (IdEventoFinanceiro, Descricao, ClassificacaoAnbima) values (300,'BMF � Outros',999)
insert into EventoFinanceiro (IdEventoFinanceiro, Descricao) values (501,'Renda Fixa � Compra Final')
insert into EventoFinanceiro (IdEventoFinanceiro, Descricao) values (502,'Renda Fixa �Venda Final')
insert into EventoFinanceiro (IdEventoFinanceiro, Descricao) values (503,'Renda Fixa �Compra com Revenda')
insert into EventoFinanceiro (IdEventoFinanceiro, Descricao) values (504,'Renda Fixa �Venda com Recompra')
insert into EventoFinanceiro (IdEventoFinanceiro, Descricao) values (601,'Renda Fixa � Exerc�cio Op��o')
insert into EventoFinanceiro (IdEventoFinanceiro, Descricao) values (505,'Renda Fixa � Net Opera��o Casada')
insert into EventoFinanceiro (IdEventoFinanceiro, Descricao) values (510,'Renda Fixa � Vencimento')
insert into EventoFinanceiro (IdEventoFinanceiro, Descricao) values (511,'Renda Fixa � Revenda')
insert into EventoFinanceiro (IdEventoFinanceiro, Descricao) values (512,'Renda Fixa � Recompra')
insert into EventoFinanceiro (IdEventoFinanceiro, Descricao) values (520,'Renda Fixa � Juros 30')
insert into EventoFinanceiro (IdEventoFinanceiro, Descricao) values (521,'Renda Fixa � Amortiza��o')
insert into EventoFinanceiro (IdEventoFinanceiro, Descricao) values (530,'Renda Fixa � IR')
insert into EventoFinanceiro (IdEventoFinanceiro, Descricao) values (531,'Renda Fixa �IOF')
insert into EventoFinanceiro (IdEventoFinanceiro, Descricao) values (540,'Renda Fixa �Corretagem')
insert into EventoFinanceiro (IdEventoFinanceiro, Descricao) values (550,'Renda Fixa �Taxa Cust�dia')
insert into EventoFinanceiro (IdEventoFinanceiro, Descricao, ClassificacaoAnbima) values (600, 'Renda Fixa �Outros', 999)
insert into EventoFinanceiro (IdEventoFinanceiro, Descricao) values (701,'Swap � Liquida��o Vencimento')
insert into EventoFinanceiro (IdEventoFinanceiro, Descricao) values (702,'Swap � Liquida��o Antecipa��o')
insert into EventoFinanceiro (IdEventoFinanceiro, Descricao) values (703,'Swap � Despesas Taxas')
insert into EventoFinanceiro (IdEventoFinanceiro, Descricao, ClassificacaoAnbima) values (801,'Taxa Administra��o',44)
insert into EventoFinanceiro (IdEventoFinanceiro, Descricao, ClassificacaoAnbima) values (802,'Pagamento Taxa Administra��o',44)
insert into EventoFinanceiro (IdEventoFinanceiro, Descricao) values (810,'Taxa Gest�o')
insert into EventoFinanceiro (IdEventoFinanceiro, Descricao) values (811,'Pagamento Taxa Gest�o')
insert into EventoFinanceiro (IdEventoFinanceiro, Descricao) values (820,'Taxa Performance')
insert into EventoFinanceiro (IdEventoFinanceiro, Descricao, ClassificacaoAnbima) values (821,'Pagamento Taxa Performance',35)
insert into EventoFinanceiro (IdEventoFinanceiro, Descricao) values (830,'CPMF')
insert into EventoFinanceiro (IdEventoFinanceiro, Descricao, ClassificacaoAnbima) values (840,'Taxa Fiscaliza��o CVM',14)
insert into EventoFinanceiro (IdEventoFinanceiro, Descricao, ClassificacaoAnbima) values (841,'Pagamento Taxa Fiscaliza��o CVM',14)
insert into EventoFinanceiro (IdEventoFinanceiro, Descricao, ClassificacaoAnbima) values (890,'Provis�o Outros',999)
insert into EventoFinanceiro (IdEventoFinanceiro, Descricao) values (891,'Pagamento Provis�o Outros')
insert into EventoFinanceiro (IdEventoFinanceiro, Descricao) values (851,'Taxa Cust�dia')
insert into EventoFinanceiro (IdEventoFinanceiro, Descricao) values (852,'Pagamento Taxa Cust�dia')
insert into EventoFinanceiro (IdEventoFinanceiro, Descricao) values (1001,'Fundos � Aplica��o')
insert into EventoFinanceiro (IdEventoFinanceiro, Descricao) values (1002,'Fundos � Resgate')
insert into EventoFinanceiro (IdEventoFinanceiro, Descricao) values (1003,'Fundos � IR Resgate')
insert into EventoFinanceiro (IdEventoFinanceiro, Descricao) values (1004,'Fundos � IOF Resgate')
insert into EventoFinanceiro (IdEventoFinanceiro, Descricao) values (1005,'Fundos � Performance Fee Resgate')
insert into EventoFinanceiro (IdEventoFinanceiro, Descricao) values (1006,'Fundos � Come Cotas')
insert into EventoFinanceiro (IdEventoFinanceiro, Descricao) values (1010,'Fundos � Aplica��o a Converter')
insert into EventoFinanceiro (IdEventoFinanceiro, Descricao) values (1020,'Fundos � Transfer�ncia Saldo')
insert into EventoFinanceiro (IdEventoFinanceiro, Descricao) values (1101,'Cotista � Aplica��o')
insert into EventoFinanceiro (IdEventoFinanceiro, Descricao) values (1102,'Cotista � Resgate')
insert into EventoFinanceiro (IdEventoFinanceiro, Descricao) values (1103,'Cotista � IR Resgate')
insert into EventoFinanceiro (IdEventoFinanceiro, Descricao) values (1104,'Cotista � IOF Resgate')
insert into EventoFinanceiro (IdEventoFinanceiro, Descricao) values (1105,'Cotista � Performance Fee Resgate')
insert into EventoFinanceiro (IdEventoFinanceiro, Descricao) values (1106,'Cotista � Come Cotas')
insert into EventoFinanceiro (IdEventoFinanceiro, Descricao) values (1110,'Cotista � Aplica��o a Converter')
insert into EventoFinanceiro (IdEventoFinanceiro, Descricao) values (2000,'IR Fonte Opera��o')
insert into EventoFinanceiro (IdEventoFinanceiro, Descricao) values (2001,'IR Fonte Daytrade')
insert into EventoFinanceiro (IdEventoFinanceiro, Descricao) values (2002,'IR Renda Vari�vel')
insert into EventoFinanceiro (IdEventoFinanceiro, Descricao) values (3000,'Chamada Margem')
insert into EventoFinanceiro (IdEventoFinanceiro, Descricao) values (3001,'Devolu��o Margem')
insert into EventoFinanceiro (IdEventoFinanceiro, Descricao) values (4000,'Cambio � Principal Origem')
insert into EventoFinanceiro (IdEventoFinanceiro, Descricao) values (4001,'Cambio � Principal Destino')
insert into EventoFinanceiro (IdEventoFinanceiro, Descricao) values (4002,'Cambio � Tributos')
insert into EventoFinanceiro (IdEventoFinanceiro, Descricao) values (4003,'Cambio - Custos')
insert into EventoFinanceiro (IdEventoFinanceiro, Descricao) values (5000,'Ajuste Compensa��o Cotas')
insert into EventoFinanceiro (IdEventoFinanceiro, Descricao) values (5001,'Ajuste Compensa��o Cotas � Evento')
insert into EventoFinanceiro (IdEventoFinanceiro, Descricao, ClassificacaoAnbima) values (6001,'Valores a Cr�dito n�o Identificados',999)
insert into EventoFinanceiro (IdEventoFinanceiro, Descricao, ClassificacaoAnbima) values (6002,'Valores a D�bito n�o Identificados',999)
insert into EventoFinanceiro (IdEventoFinanceiro, Descricao, ClassificacaoAnbima) values (100000,'Outros',999)
insert into EventoFinanceiro (IdEventoFinanceiro, Descricao) values (100001,'Carteira Gerencial')


	END	

	IF NOT EXISTS(SELECT * FROM SYSCOLUMNS WHERE NAME = 'IdEventoFinanceiro' AND ID = OBJECT_ID('Liquidacao'))
	BEGIN
		ALTER TABLE Liquidacao ADD IdEventoFinanceiro INTEGER, FOREIGN KEY(IdEventoFinanceiro) REFERENCES EventoFinanceiro(IdEventoFinanceiro);
	END

	if not exists (select 1 from PermissaoMenu where IdGrupo = 1 and IdMenu = 330) 
	begin
		insert into PermissaoMenu values (1, 330, 'S','S','S','S')
	end

-- commita a transaction e volta as variaveis do sql server para o estado inicial
commit transaction
set noexec off
set nocount off