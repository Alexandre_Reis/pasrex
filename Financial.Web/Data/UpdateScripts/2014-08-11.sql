declare @data_versao char(10)
set @data_versao = '2014-08-11'
if exists (select 1 from VersaoSchema where DataVersao = @data_versao) return
insert into VersaoSchema VALUES(@data_versao)

ALTER TABLE TituloRendaFixa ADD	DebentureInfra char(1) NOT NULL CONSTRAINT DF_TituloRendaFixa_DebentureInfra DEFAULT 'N'
