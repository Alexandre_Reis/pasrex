﻿---
---
---  Ranking Anbima por Segmento
---
--- 
if exists(select * from sys.all_objects where name = 'v_RankingPorSegmento' and type = 'v')
	drop view v_RankingPorSegmento 
go 

create VIEW  v_RankingPorSegmento  as 

  SELECT PosicaoCotistaHistorico.Datahistorico as Datahistorico,
         PosicaoCotistaHistorico.idCarteira as idCarteira,
         (select ValorTexto from Configuracao where Descricao = 'NomeInstituicao' ) as Instituicao,
		 Carteira.codigoAnbid as CodigoFundo,
		 coalesce((select Top 1 ValorCampo from CadastroComplementar where [IdCamposComplementares] = (select IdCamposComplementares  from [CadastroComplementarCampos] where NomeCampo = 'StatusFundo' and TipoCadastro = -8 ) and idMercadoTipoPessoa = PosicaoCotistaHistorico.idCarteira ),'NA') as StatusFundo,  --via Cadastro Complementar ( FF / FI / FC / FM )
		 Cotista.TipoCotistaAnbima as TipoCotistaAnbima,
		 sum(PosicaoCotistaHistorico.ValorBruto)/1000000 as ValorBruto
  FROM  PosicaoCotistaHistorico
         left join Cotista on Cotista.idCotista = PosicaoCotistaHistorico.idCotista
         left join Carteira on Carteira.idCarteira = PosicaoCotistaHistorico.idCarteira
 
  group by 
         PosicaoCotistaHistorico.Datahistorico,
         PosicaoCotistaHistorico.idCarteira,
		 Carteira.codigoAnbid,
		 Cotista.TipoCotistaAnbima


  
GO

if exists(select * from sys.all_objects where name = 'v_RankingPorSegmentoReportView' and type = 'v')
	drop view v_RankingPorSegmentoReportView 
go 

create VIEW  v_RankingPorSegmentoReportView  as 

	select distinct A.Datahistorico, 
	       A.idCarteira, 
		   A.Instituicao, 
		   A.CodigoFundo, 
		   A.StatusFundo, 
		   coalesce((select SUM(B.ValorBruto) 
		       from v_RankingPorSegmento B
			   where A.datahistorico = b.datahistorico and 
			         A.idCarteira = b.idCarteira and
					 B.TipoCotistaAnbima = 1 ),0) as EFPC_Emp_Publica,

		   coalesce((select sum(B.ValorBruto) 
		       from v_RankingPorSegmento B
			   where A.datahistorico = b.datahistorico and 
			         A.idCarteira = b.idCarteira and
					 B.TipoCotistaAnbima = 2 ),0) as EFPC_Emp_Privada,

		   coalesce((select sum(B.ValorBruto) 
		       from v_RankingPorSegmento B
			   where A.datahistorico = b.datahistorico and 
			         A.idCarteira = b.idCarteira and
					 B.TipoCotistaAnbima =  3 ),0) as Seguradora,

		   coalesce((select sum(B.ValorBruto) 
		       from v_RankingPorSegmento B
			   where A.datahistorico = b.datahistorico and 
			         A.idCarteira = b.idCarteira and
					 B.TipoCotistaAnbima = 4 ),0) as EAPC,

		   coalesce((select sum(B.ValorBruto) 
		       from v_RankingPorSegmento B
			   where A.datahistorico = b.datahistorico and 
			         A.idCarteira = b.idCarteira and
					 B.TipoCotistaAnbima = 5 ),0) as Capitalizacao,

		   coalesce((select sum(B.ValorBruto) 
		       from v_RankingPorSegmento B
			   where A.datahistorico = b.datahistorico and 
			         A.idCarteira = b.idCarteira and
					 B.TipoCotistaAnbima =  6 ),0) as Corporate,

		   coalesce((select sum(B.ValorBruto) 
		       from v_RankingPorSegmento B
			   where A.datahistorico = b.datahistorico and 
			         A.idCarteira = b.idCarteira and
					 B.TipoCotistaAnbima = 7 ),0) as Middle_Market,

		   coalesce((select sum(B.ValorBruto) 
		       from v_RankingPorSegmento B
			   where A.datahistorico = b.datahistorico and 
			         A.idCarteira = b.idCarteira and
					 B.TipoCotistaAnbima =  8 ),0) as Private,

		   coalesce((select sum(B.ValorBruto) 
		       from v_RankingPorSegmento B
			   where A.datahistorico = b.datahistorico and 
			         A.idCarteira = b.idCarteira and
					 B.TipoCotistaAnbima = 9 ),0) as Varejo_Alta_Renda,

		   coalesce((select sum(B.ValorBruto) 
		       from v_RankingPorSegmento B
			   where A.datahistorico = b.datahistorico and 
			         A.idCarteira = b.idCarteira and
					 B.TipoCotistaAnbima =  10 ),0) as Varejo,

		   coalesce((select sum(B.ValorBruto) 
		       from v_RankingPorSegmento B
			   where A.datahistorico = b.datahistorico and 
			         A.idCarteira = b.idCarteira and
					 B.TipoCotistaAnbima =  11 ),0) as Poder_Publico,

		   coalesce((select sum(B.ValorBruto) 
		       from v_RankingPorSegmento B
			   where A.datahistorico = b.datahistorico and 
			         A.idCarteira = b.idCarteira and
					 B.TipoCotistaAnbima =  12 ),0) as Regime_Proprio_Previdencia_Social,

		   coalesce((select sum(B.ValorBruto) 
		       from v_RankingPorSegmento B
			   where A.datahistorico = b.datahistorico and 
			         A.idCarteira = b.idCarteira and
					 B.TipoCotistaAnbima =  13 ),0) as Fundos_Investimento,

		   coalesce((select sum(B.ValorBruto) 
		       from v_RankingPorSegmento B
			   where A.datahistorico = b.datahistorico and 
			         A.idCarteira = b.idCarteira and
					 B.TipoCotistaAnbima =  14 ),0) as Estrangeiros,

		   coalesce((select sum(B.ValorBruto) 
		       from v_RankingPorSegmento B
			   where A.datahistorico = b.datahistorico and 
			         A.idCarteira = b.idCarteira and
					 (B.TipoCotistaAnbima =  15 or B.TipoCotistaAnbima is null) ),0) as Outros,

		   (select count(distinct c.idcotista) 
		       from PosicaoCotistaHistorico C
			   where a.Datahistorico = c.DataHistorico and 
					 A.idCarteira = c.IdCarteira ) as QuantidadeTotalCotistas,

		   coalesce((select sum(B.ValorBruto) 
		       from v_RankingPorSegmento B
			   where A.datahistorico = b.datahistorico and 
			         A.idCarteira = b.idCarteira ),0) as PLFundo

	from v_RankingPorSegmento A;

go

--- SCRIPT Exemplo de utilização 

select * from v_RankingPorSegmentoReportView
	where  Datahistorico= '2015-10-30' and idcarteira = 13352 
	order by Datahistorico, idCarteira











