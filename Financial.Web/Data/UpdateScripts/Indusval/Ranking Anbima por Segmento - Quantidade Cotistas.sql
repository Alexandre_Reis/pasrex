﻿---
---
---  Ranking Anbima por Segmento ( Quantidade )
---
--- 
if exists(select * from sys.all_objects where name = 'v_RankingPorSegmentoQuantidade' and type = 'v')
	drop view v_RankingPorSegmentoQuantidade 
go 
create VIEW v_RankingPorSegmentoQuantidade as

   select PosHist.DataHistorico as DataHistorico,
          PosHist.IdCarteira as IdCarteira,
		  o.TipoCotistaAnbima as TipoCotistaAnbima, 
		  count(distinct PosHist.idcotista) as quantidadeCotistas
	      from PosicaoCotistaHistorico PosHist, 
		       Cotista O 
        where  O.IdCotista = PosHist.IdCotista
		group by PosHist.DataHistorico, 
		         PosHist.IdCarteira,
				 o.TipoCotistaAnbima; 
go




if exists(select * from sys.all_objects where name = 'v_RankingPorSegmentoQuantidadeReportView' and type = 'v')
	drop view v_RankingPorSegmentoQuantidadeReportView 
go 

create VIEW  v_RankingPorSegmentoQuantidadeReportView  as 

	select distinct A.Datahistorico, 
	       A.idCarteira, 
		   A.Instituicao, 
		   A.CodigoFundo, 
		   A.StatusFundo, 
		   coalesce((select b.quantidadeCotistas
	                 from v_RankingPorSegmentoQuantidade B
	                 where a.Datahistorico = b.DataHistorico and 
						   A.idCarteira = b.IdCarteira and 
						   b.TipoCotistaAnbima = 1 ),0) as EFPC_Emp_Publica,

		   coalesce((select b.quantidadeCotistas
	                 from v_RankingPorSegmentoQuantidade B
	                 where a.Datahistorico = b.DataHistorico and 
						   A.idCarteira = b.IdCarteira and 
						   b.TipoCotistaAnbima =  2 ),0) as EFPC_Emp_Privada,

		   coalesce((select b.quantidadeCotistas
	                 from v_RankingPorSegmentoQuantidade B
	                 where a.Datahistorico = b.DataHistorico and 
						   A.idCarteira = b.IdCarteira and 
						   b.TipoCotistaAnbima = 3 ),0) as Seguradora,

		   coalesce((select b.quantidadeCotistas
	                 from v_RankingPorSegmentoQuantidade B
	                 where a.Datahistorico = b.DataHistorico and 
						   A.idCarteira = b.IdCarteira and 
						   b.TipoCotistaAnbima = 4 ),0) as EAPC,

		   coalesce((select b.quantidadeCotistas
	                 from v_RankingPorSegmentoQuantidade B
	                 where a.Datahistorico = b.DataHistorico and 
						   A.idCarteira = b.IdCarteira and 
						   b.TipoCotistaAnbima =  5 ),0) as Capitalizacao,

		   coalesce((select b.quantidadeCotistas
	                 from v_RankingPorSegmentoQuantidade B
	                 where a.Datahistorico = b.DataHistorico and 
						   A.idCarteira = b.IdCarteira and 
						   b.TipoCotistaAnbima =  6 ),0) as Corporate,

		   coalesce((select b.quantidadeCotistas
	                 from v_RankingPorSegmentoQuantidade B
	                 where a.Datahistorico = b.DataHistorico and 
						   A.idCarteira = b.IdCarteira and 
						   b.TipoCotistaAnbima = 7 ),0) as Middle_Market,

		   coalesce((select b.quantidadeCotistas
	                 from v_RankingPorSegmentoQuantidade B
	                 where a.Datahistorico = b.DataHistorico and 
						   A.idCarteira = b.IdCarteira and 
						   b.TipoCotistaAnbima =  8 ),0) as Private,

		   coalesce((select b.quantidadeCotistas
	                 from v_RankingPorSegmentoQuantidade B
	                 where a.Datahistorico = b.DataHistorico and 
						   A.idCarteira = b.IdCarteira and 
						   b.TipoCotistaAnbima =  9 ),0) as Varejo_Alta_Renda,

		   coalesce((select b.quantidadeCotistas
	                 from v_RankingPorSegmentoQuantidade B
	                 where a.Datahistorico = b.DataHistorico and 
						   A.idCarteira = b.IdCarteira and 
						   b.TipoCotistaAnbima = 10 ),0) as Varejo,

		   coalesce((select b.quantidadeCotistas
	                 from v_RankingPorSegmentoQuantidade B
	                 where a.Datahistorico = b.DataHistorico and 
						   A.idCarteira = b.IdCarteira and 
						   b.TipoCotistaAnbima =  11 ),0) as Poder_Publico,

		   coalesce((select b.quantidadeCotistas
	                 from v_RankingPorSegmentoQuantidade B
	                 where a.Datahistorico = b.DataHistorico and 
						   A.idCarteira = b.IdCarteira and 
						   b.TipoCotistaAnbima = 12 ),0) as Regime_Proprio_Previdencia_Social,

		   coalesce((select b.quantidadeCotistas
	                 from v_RankingPorSegmentoQuantidade B
	                 where a.Datahistorico = b.DataHistorico and 
						   A.idCarteira = b.IdCarteira and 
						   b.TipoCotistaAnbima =  13 ),0) as Fundos_Investimento,

		   coalesce((select b.quantidadeCotistas
	                 from v_RankingPorSegmentoQuantidade B
	                 where a.Datahistorico = b.DataHistorico and 
						   A.idCarteira = b.IdCarteira and 
						   b.TipoCotistaAnbima =  14 ),0) as Estrangeiros,

		   coalesce((select sum(b.quantidadeCotistas)
	                 from v_RankingPorSegmentoQuantidade B
	                 where a.Datahistorico = b.DataHistorico and 
						   A.idCarteira = b.IdCarteira and 
						   (b.TipoCotistaAnbima = 15 or b.TipoCotistaAnbima is null)),0) as Outros,

		   coalesce((select count(distinct c.idcotista) 
		       from PosicaoCotistaHistorico C
			   where a.Datahistorico = c.DataHistorico and 
					 A.idCarteira = c.IdCarteira ),0) as QuantidadeTotalCotistas

	from v_RankingPorSegmento A;

go

  

--- SCRIPT Exemplo de utilização 
              
select * from v_RankingPorSegmentoQuantidadeReportView
	where  Datahistorico= '2015-10-30'and idcarteira in (17449, 21341, 24326, 24807, 4112736, 4112894, 743259, 746295, 736745, 736744)
	order by Datahistorico, idCarteira

