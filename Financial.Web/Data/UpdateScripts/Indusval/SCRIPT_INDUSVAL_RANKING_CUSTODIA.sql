﻿/****** Criação dos campos de cadastro complementar  ******/

begin transaction 
go

delete CadastroComplementar 
 where IdCamposComplementares in (select IdCamposComplementares from CadastroComplementarCampos 
                                     where NomeCampo in 
									       ('MercadoInternoExterno')
								 )
go

delete CadastroComplementarCampos 
 where NomeCampo in ('MercadoInternoExterno')
go

delete CadastroComplementarItemLista 
 where IdTipoLista in 
    (select IdTipoLista 
	  from [CadastroComplementarTipoLista] 
	   where nomeLista in ('TipoListaMercIntExt'))
go 

delete [CadastroComplementarTipoLista] where nomeLista in ('TipoListaMercIntExt')
go


/**** CRIAÇÃO DA LISTA ****/

INSERT INTO [CadastroComplementarTipoLista]  ([NomeLista]  ,[TipoLista])  VALUES  ( 'TipoListaMercIntExt',-2) ;


/**** INCLUSÃO DOS ITENS NA LISTA ****/

INSERT INTO [CadastroComplementarItemLista]  ([IdTipoLista]  ,[ItemLista])  VALUES  ( (select IdTipoLista from [CadastroComplementarTipoLista] where NomeLista='TipoListaMercIntExt'),'Doméstico') ;
INSERT INTO [CadastroComplementarItemLista]  ([IdTipoLista]  ,[ItemLista])  VALUES  ( (select IdTipoLista from [CadastroComplementarTipoLista] where NomeLista='TipoListaMercIntExt'),'Resol.2689') ;
INSERT INTO [CadastroComplementarItemLista]  ([IdTipoLista]  ,[ItemLista])  VALUES  ( (select IdTipoLista from [CadastroComplementarTipoLista] where NomeLista='TipoListaMercIntExt'),'ADR') ;
INSERT INTO [CadastroComplementarItemLista]  ([IdTipoLista]  ,[ItemLista])  VALUES  ( (select IdTipoLista from [CadastroComplementarTipoLista] where NomeLista='TipoListaMercIntExt'),'Outros') ;

/**** INCLUSÃO DO CAMPO DO CADASTRO COMPLEMENTAR ****/

--CarteiraFundo
delete CadastroComplementar where IdCamposComplementares in(select IdCamposComplementares from CadastroComplementarCampos where TipoCadastro = -8 AND NomeCampo ='MercadoInternoExterno') delete CadastroComplementarCampos where TipoCadastro = -8 AND NomeCampo='MercadoInternoExterno'  INSERT INTO [CadastroComplementarCampos]  ([TipoCadastro]    ,[NomeCampo]         ,[DescricaoCampo]       ,[ValorDefault]      ,[TipoCampo]       ,[CampoObrigatorio]       ,[Tamanho]          ,[CasasDecimais])  VALUES  (-8 , 'MercadoInternoExterno','Mercado Interno Externo' ,NULL ,(select IdTipoLista from CadastroComplementarTipoLista where NomeLista = 'TipoListaMercIntExt') ,'N' ,30  ,0) ;

--AtivoBolsa
delete CadastroComplementar where IdCamposComplementares in(select IdCamposComplementares from CadastroComplementarCampos where TipoCadastro = -10 AND NomeCampo ='MercadoInternoExterno') delete CadastroComplementarCampos where TipoCadastro = -10 AND NomeCampo='MercadoInternoExterno'  INSERT INTO [CadastroComplementarCampos]  ([TipoCadastro]    ,[NomeCampo]         ,[DescricaoCampo]       ,[ValorDefault]      ,[TipoCampo]       ,[CampoObrigatorio]       ,[Tamanho]          ,[CasasDecimais])  VALUES  (-10 , 'MercadoInternoExterno','Mercado Interno Externo' ,NULL ,(select IdTipoLista from CadastroComplementarTipoLista where NomeLista = 'TipoListaMercIntExt') ,'N' ,30  ,0) ;

--AtivoBMF
delete CadastroComplementar where IdCamposComplementares in(select IdCamposComplementares from CadastroComplementarCampos where TipoCadastro = -11 AND NomeCampo ='MercadoInternoExterno') delete CadastroComplementarCampos where TipoCadastro = -11 AND NomeCampo='MercadoInternoExterno'  INSERT INTO [CadastroComplementarCampos]  ([TipoCadastro]    ,[NomeCampo]         ,[DescricaoCampo]       ,[ValorDefault]      ,[TipoCampo]       ,[CampoObrigatorio]       ,[Tamanho]          ,[CasasDecimais])  VALUES  (-11 , 'MercadoInternoExterno','Mercado Interno Externo' ,NULL ,(select IdTipoLista from CadastroComplementarTipoLista where NomeLista = 'TipoListaMercIntExt') ,'N' ,30  ,0) ;

--RendaFixaTitulo
delete CadastroComplementar where IdCamposComplementares in(select IdCamposComplementares from CadastroComplementarCampos where TipoCadastro = -12 AND NomeCampo ='MercadoInternoExterno') delete CadastroComplementarCampos where TipoCadastro = -12 AND NomeCampo='MercadoInternoExterno'  INSERT INTO [CadastroComplementarCampos]  ([TipoCadastro]    ,[NomeCampo]         ,[DescricaoCampo]       ,[ValorDefault]      ,[TipoCampo]       ,[CampoObrigatorio]       ,[Tamanho]          ,[CasasDecimais])  VALUES  (-12 , 'MercadoInternoExterno','Mercado Interno Externo' ,NULL ,(select IdTipoLista from CadastroComplementarTipoLista where NomeLista = 'TipoListaMercIntExt') ,'N' ,30  ,0) 

/**** INCLUSÃO DO CONTEÚDO DO CADASTRO COMPLEMENTAR ****/

DELETE FROM [CadastroComplementar]
where  exists (select 'X' 
                   from [CadastroComplementarCampos] A
				 where a.IdCamposComplementares = [CadastroComplementar].IdCamposComplementares 
				       and a.NomeCampo in ('MercadoInternoExterno')
			  )
go

--FUNDOS -8
INSERT INTO [CadastroComplementar]
           ([IdCamposComplementares]
           ,[IdMercadoTipoPessoa]
           ,[DescricaoMercadoTipoPessoa]
           ,[ValorCampo])
	select (select IdCamposComplementares  from [CadastroComplementarCampos] where NomeCampo = 'MercadoInternoExterno' and TipoCadastro = -8),
           Carteira.idCarteira,
           Convert(Varchar(50),Carteira.Nome), 
           'Outros'
	 from Carteira
	 where Carteira.idCarteira in (368199)
GO

--BMF -11
INSERT INTO [CadastroComplementar]
           ([IdCamposComplementares]
           ,[IdMercadoTipoPessoa]
           ,[DescricaoMercadoTipoPessoa]
           ,[ValorCampo])
	select (select IdCamposComplementares  from [CadastroComplementarCampos] where NomeCampo = 'MercadoInternoExterno' and TipoCadastro = -11),
           'DI1',
           Convert(Varchar(50),'DI1'), 
           'Resol.2689'
GO



--Renda Fixa -12
INSERT INTO [CadastroComplementar]
           ([IdCamposComplementares]
           ,[IdMercadoTipoPessoa]
           ,[DescricaoMercadoTipoPessoa]
           ,[ValorCampo])
	select (select IdCamposComplementares  from [CadastroComplementarCampos] where NomeCampo = 'MercadoInternoExterno' and TipoCadastro = -12),
           TituloRendaFixa.IdTitulo,
           Convert(Varchar(50),TituloRendaFixa.Descricao), 
           'Resol.2689'
	 from TituloRendaFixa
	 where IdTitulo in (40069,40066,40024,40346)
GO


INSERT INTO [CadastroComplementar]
           ([IdCamposComplementares]
           ,[IdMercadoTipoPessoa]
           ,[DescricaoMercadoTipoPessoa]
           ,[ValorCampo])
	select (select IdCamposComplementares  from [CadastroComplementarCampos] where NomeCampo = 'MercadoInternoExterno' and TipoCadastro = -12),
           TituloRendaFixa.IdTitulo,
           Convert(Varchar(50),TituloRendaFixa.Descricao), 
           'ADR'
	 from TituloRendaFixa
	 where IdTitulo in (40050,40047,40041,40023,40020,40069,40066)
GO

INSERT INTO [CadastroComplementar]
           ([IdCamposComplementares]
           ,[IdMercadoTipoPessoa]
           ,[DescricaoMercadoTipoPessoa]
           ,[ValorCampo])
	select (select IdCamposComplementares  from [CadastroComplementarCampos] where NomeCampo = 'MercadoInternoExterno' and TipoCadastro = -12),
           TituloRendaFixa.IdTitulo,
           Convert(Varchar(50),TituloRendaFixa.Descricao), 
           'Outros'
	 from TituloRendaFixa
	 where IdTitulo in (40024,40346,40050,40047,40041,40023)
GO

INSERT INTO [CadastroComplementar]
           ([IdCamposComplementares]
           ,[IdMercadoTipoPessoa]
           ,[DescricaoMercadoTipoPessoa]
           ,[ValorCampo])
	select (select IdCamposComplementares  from [CadastroComplementarCampos] where NomeCampo = 'MercadoInternoExterno' and TipoCadastro = -12),
           TituloRendaFixa.IdTitulo,
           Convert(Varchar(50),TituloRendaFixa.Descricao), 
           'Doméstico'
	 from TituloRendaFixa
	 where IdTitulo in (40020,40040,40000,40322,40049,40034,40006,40095,40092,40039,40027,40005,40001,40291,40029,40325,40063,40033,40012,40007,40004,40018,40013,40002,40290,40059,40409)
GO

commit transaction
go


select * from CadastroComplementar

--- Alternativa criando os campos na tabela

--if not exists(select 1 from syscolumns where id = object_id('AtivoBolsa') and name = 'MercadoInternoExterno')
--BEGIN
--	ALTER TABLE AtivoBolsa ADD MercadoInternoExterno varchar(100) null
--END
--GO

--if not exists(select 1 from syscolumns where id = object_id('AtivoBMF') and name = 'MercadoInternoExterno')
--BEGIN
--	ALTER TABLE AtivoBMF ADD MercadoInternoExterno varchar(100) null
--END
--GO

--if not exists(select 1 from syscolumns where id = object_id('TituloRendaFixa') and name = 'MercadoInternoExterno')
--BEGIN
--	ALTER TABLE TituloRendaFixa ADD MercadoInternoExterno varchar(100) null
--END
--GO

--if not exists(select 1 from syscolumns where id = object_id('Carteira') and name = 'MercadoInternoExterno')
--BEGIN
--	ALTER TABLE Carteira ADD MercadoInternoExterno varchar(100) null
--END
--GO