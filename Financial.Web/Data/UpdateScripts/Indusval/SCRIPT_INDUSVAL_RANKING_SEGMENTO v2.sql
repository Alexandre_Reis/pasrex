﻿/****** Criação dos campos de cadastro complementar  ******/

begin transaction 
go

 -- não permite que a transaction seja commitada em caso de erros
    set xact_abort on

delete CadastroComplementar 
 where IdCamposComplementares in (select IdCamposComplementares from CadastroComplementarCampos 
                                     where NomeCampo in 
									       ('StatusFundo')
								 )
go

delete CadastroComplementarCampos 
 where NomeCampo in ('StatusFundo')
go

delete CadastroComplementarItemLista 
 where IdTipoLista in 
    (select IdTipoLista 
	  from [CadastroComplementarTipoLista] 
	   where nomeLista in ('TipoListaStatusFundoFFFIFCFM'))
go 

delete [CadastroComplementarTipoLista] where nomeLista in ('TipoListaStatusFundoFFFIFCFM')
go


/**** CRIAÇÃO DA LISTA ****/

INSERT INTO [CadastroComplementarTipoLista]  ([NomeLista]  ,[TipoLista])  VALUES  ( 'TipoListaStatusFundoFFFIFCFM',-2) ;


/**** INCLUSÃO DOS ITENS NA LISTA ****/

INSERT INTO [CadastroComplementarItemLista]  ([IdTipoLista]  ,[ItemLista])  VALUES  ( (select IdTipoLista from [CadastroComplementarTipoLista] where NomeLista='TipoListaStatusFundoFFFIFCFM'),'FF') ;
INSERT INTO [CadastroComplementarItemLista]  ([IdTipoLista]  ,[ItemLista])  VALUES  ( (select IdTipoLista from [CadastroComplementarTipoLista] where NomeLista='TipoListaStatusFundoFFFIFCFM'),'FI') ;
INSERT INTO [CadastroComplementarItemLista]  ([IdTipoLista]  ,[ItemLista])  VALUES  ( (select IdTipoLista from [CadastroComplementarTipoLista] where NomeLista='TipoListaStatusFundoFFFIFCFM'),'FC') ;
INSERT INTO [CadastroComplementarItemLista]  ([IdTipoLista]  ,[ItemLista])  VALUES  ( (select IdTipoLista from [CadastroComplementarTipoLista] where NomeLista='TipoListaStatusFundoFFFIFCFM'),'FM') ;

/**** INCLUSÃO DO CAMPO DO CADASTRO COMPLEMENTAR ****/

--CarteiraFundo
delete CadastroComplementar where IdCamposComplementares in(select IdCamposComplementares from CadastroComplementarCampos where TipoCadastro = -8 AND NomeCampo ='StatusFundo') delete CadastroComplementarCampos where TipoCadastro = -8 AND NomeCampo='StatusFundo'  INSERT INTO [CadastroComplementarCampos]  ([TipoCadastro]    ,[NomeCampo]         ,[DescricaoCampo]       ,[ValorDefault]      ,[TipoCampo]       ,[CampoObrigatorio]       ,[Tamanho]          ,[CasasDecimais])  VALUES  (-8 , 'StatusFundo','Status do Fundo' ,NULL ,(select IdTipoLista from CadastroComplementarTipoLista where NomeLista = 'TipoListaStatusFundoFFFIFCFM') ,'N' ,30  ,0) ;

/**** INCLUSÃO DO CONTEÚDO DO CADASTRO COMPLEMENTAR ****/

DELETE FROM [CadastroComplementar]
where  exists (select 'X' 
                   from [CadastroComplementarCampos] A
				 where a.IdCamposComplementares = [CadastroComplementar].IdCamposComplementares 
				       and a.NomeCampo in ('StatusFundo')
			  )
go

--FUNDOS -8
INSERT INTO [CadastroComplementar]
           ([IdCamposComplementares]
           ,[IdMercadoTipoPessoa]
           ,[DescricaoMercadoTipoPessoa]
           ,[ValorCampo])
	select (select IdCamposComplementares  from [CadastroComplementarCampos] where NomeCampo = 'StatusFundo' and TipoCadastro = -8),
           Carteira.idCarteira,
           Convert(Varchar(50),Carteira.Nome), 
           'FF'
	 from Carteira
	 where Carteira.idCarteira in ()  --- Indicar aqui a lista de carteiras cujo Status do Fundo seja FF
GO

INSERT INTO [CadastroComplementar]
           ([IdCamposComplementares]
           ,[IdMercadoTipoPessoa]
           ,[DescricaoMercadoTipoPessoa]
           ,[ValorCampo])
	select (select IdCamposComplementares  from [CadastroComplementarCampos] where NomeCampo = 'StatusFundo' and TipoCadastro = -8),
           Carteira.idCarteira,
           Convert(Varchar(50),Carteira.Nome), 
           'FI'
	 from Carteira
	 where Carteira.idCarteira in (17449, 21341, 24326, 24807, 4112736, 4112894)  --- Indicar aqui a lista de carteiras cujo Status do Fundo seja FI
GO

INSERT INTO [CadastroComplementar]
           ([IdCamposComplementares]
           ,[IdMercadoTipoPessoa]
           ,[DescricaoMercadoTipoPessoa]
           ,[ValorCampo])
	select (select IdCamposComplementares  from [CadastroComplementarCampos] where NomeCampo = 'StatusFundo' and TipoCadastro = -8),
           Carteira.idCarteira,
           Convert(Varchar(50),Carteira.Nome), 
           'FC'
	 from Carteira
	 where Carteira.idCarteira in (743259, 746295, 736745, 736744)  --- Indicar aqui a lista de carteiras cujo Status do Fundo seja FC
GO

INSERT INTO [CadastroComplementar]
           ([IdCamposComplementares]
           ,[IdMercadoTipoPessoa]
           ,[DescricaoMercadoTipoPessoa]
           ,[ValorCampo])
	select (select IdCamposComplementares  from [CadastroComplementarCampos] where NomeCampo = 'StatusFundo' and TipoCadastro = -8),
           Carteira.idCarteira,
           Convert(Varchar(50),Carteira.Nome), 
           'FM'
	 from Carteira
	 where Carteira.idCarteira in () --- Indicar aqui a lista de carteiras cujo Status do Fundo seja FM
GO


commit transaction
go


