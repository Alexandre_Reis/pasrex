﻿----- SCRIPT COMPATIVEL COM VERSAO 1.0.x
---
---
--- Script depende que sejam criados os seguintes campos na base de dados (ou usar cadastro complementar)
--- 
---    AtivoBMF.MercadoInternoExterno
---    AtivoBolsa.MercadoInternoExterno
---    Carteira.MercadoInternoExterno
---    RendaFixa.MercadoInternoExterno
if exists(select * from sys.all_objects where name = 'v_RankingCustodiaAtivos' and type = 'v')
	drop view v_RankingCustodiaAtivos 
go 

create VIEW  v_RankingCustodiaAtivos  as 

  SELECT 'BMF' as Mercado,
          PosicaoBMFHistorico.Datahistorico as Data,
		 CONCAT(CAST(MONTH(PosicaoBMFHistorico.Datahistorico) as VARCHAR),'/',CAST(year(PosicaoBMFHistorico.Datahistorico) as VARCHAR)) as DataRef,
         Carteira.idCarteira as Fundo, 
		 AgenteMercado.nome as Instituicao, 
		 AgenteMercado.NomeResponsavel as Responsavel, 
		 AgenteMercado.Telefone as TelefoneContato,
		 AgenteMercado.email as email, 
		 'Domestico' as TipoMercadoInternoExterno,    --- AtivoBMF.MercadoInternoExterno 
		 Carteira.MesmoConglomerado as PortfolioMesmoConglomerado,         
		 Carteira.CategoriaAnbima as Categoria,   	 
          sum(ValorMercado)/1000000 Saldo
  FROM  PosicaoBMFHistorico
  left join Carteira on PosicaoBMFHistorico.idcliente = Carteira.idCarteira
  left join AgenteMercado on AgenteMercado.IdAgente =  Carteira.idAgenteCustodiante
  left join AtivoBMF on AtivoBMF.CdAtivoBMF = PosicaoBMFHistorico.CdAtivoBMF 
  group by PosicaoBMFHistorico.Datahistorico,
           Carteira.idCarteira,
		   AtivoBMF.CdAtivoBMF,
		   Carteira.MesmoConglomerado,
		   Carteira.CategoriaAnbima,
           AgenteMercado.nome,
		   AgenteMercado.NomeResponsavel, 
		   AgenteMercado.Telefone ,
		   AgenteMercado.email
  
  Union all
  
SELECT   'BOLSA' as Mercado,   
		  PosicaoBolsaHistorico.Datahistorico as Data,
		 CONCAT(CAST(MONTH(PosicaoBolsaHistorico.Datahistorico) as VARCHAR),'/',CAST(year(PosicaoBolsaHistorico.Datahistorico) as VARCHAR)) as DataRef,
         Carteira.idCarteira as Fundo, 
		 AgenteMercado.nome as Instituicao, 
		 AgenteMercado.NomeResponsavel as Responsavel, 
		 AgenteMercado.Telefone as TelefoneContato,
		 AgenteMercado.email as email, 
		 'Domestico' as TipoMercadoInternoExterno,   --- AtivoBolsa.MercadoInternoExterno
		 Carteira.MesmoConglomerado as PortfolioMesmoConglomerado,    
		 Carteira.CategoriaAnbima as Categoria,  		       
         sum(ValorMercado)/1000000 Saldo
  FROM  PosicaoBolsaHistorico 
  left join Carteira on PosicaoBolsaHistorico.idcliente = Carteira.idCarteira
  left join AgenteMercado on AgenteMercado.IdAgente =  Carteira.idAgenteCustodiante
  left join AtivoBolsa on AtivoBolsa.CdAtivoBolsa = PosicaoBolsaHistorico.CdAtivoBolsa 
  group by PosicaoBolsaHistorico.Datahistorico ,
         Carteira.idCarteira, 
		 AtivoBolsa.CdAtivoBolsa,
		 Carteira.MesmoConglomerado,
		 Carteira.CategoriaAnbima,
		 AgenteMercado.nome,
		 AgenteMercado.NomeResponsavel, 
		 AgenteMercado.Telefone ,
		 AgenteMercado.email

    Union all

SELECT   'Fundo' as Mercado,
          PosicaoFundoHistorico.Datahistorico as Data,
		 CONCAT(CAST(MONTH(PosicaoFundoHistorico.Datahistorico) as VARCHAR),'/',CAST(year(PosicaoFundoHistorico.Datahistorico) as VARCHAR)) as DataRef,
		 Cliente.idCarteira as Fundo, 
		 AgenteMercado.nome as Instituicao,
		 AgenteMercado.NomeResponsavel as Responsavel,  
		 AgenteMercado.Telefone as TelefoneContato,
		 AgenteMercado.email as email, 
		 'Domestico' as TipoMercadoInternoExterno,    --- Cliente.MercadoInternoExterno
		 Cliente.MesmoConglomerado as PortfolioMesmoConglomerado,         
		 Cliente.CategoriaAnbima as Categoria,                      
         sum(ValorLiquido)/1000000 Saldo
   FROM PosicaoFundoHistorico
  left join Carteira as Cliente on PosicaoFundoHistorico.idcliente = Cliente.idCarteira
  left join AgenteMercado on AgenteMercado.IdAgente =  Cliente.idAgenteCustodiante
  left join Carteira on Carteira.IdCarteira = PosicaoFundoHistorico.IdCarteira
  group by PosicaoFundoHistorico.Datahistorico ,
           Cliente.idCarteira,
		   Cliente.MesmoConglomerado,
		   Cliente.CategoriaAnbima,
		   AgenteMercado.nome,
		   AgenteMercado.NomeResponsavel, 
		   AgenteMercado.Telefone ,
		   AgenteMercado.email
		   
      Union all

SELECT   'Renda Fixa' as Mercado,
          PosicaoRendaFixaHistorico.Datahistorico as Data,
		 CONCAT(CAST(MONTH(PosicaoRendaFixaHistorico.Datahistorico) as VARCHAR),'/',CAST(year(PosicaoRendaFixaHistorico.Datahistorico) as VARCHAR)) as DataRef,
	     Carteira.idCarteira as Fundo, 
		 AgenteMercado.nome as Instituicao, 
		 AgenteMercado.NomeResponsavel as Responsavel, 
		 AgenteMercado.Telefone as TelefoneContato,
		 AgenteMercado.email as email, 
		 'Domestico' as TipoMercadoInternoExterno,    --- TituloRendaFixa.MercadoInternoExterno
		 Carteira.MesmoConglomerado as PortfolioMesmoConglomerado,    
		 Carteira.CategoriaAnbima as Categoria,                   
         sum(ValorMercado)/1000000 Saldo
  FROM  PosicaoRendaFixaHistorico
  left join Carteira on PosicaoRendaFixaHistorico.idcliente = Carteira.idCarteira
  left join AgenteMercado on AgenteMercado.IdAgente =  Carteira.idAgenteCustodiante
  left join TituloRendaFixa on TituloRendaFixa.idTitulo = PosicaoRendaFixaHistorico.IdTitulo
  group by PosicaoRendaFixaHistorico.Datahistorico,
  	       Carteira.idCarteira, 
		   TituloRendaFixa.idTitulo,
		   Carteira.MesmoConglomerado,
		   Carteira.CategoriaAnbima,
		   AgenteMercado.nome,
		   AgenteMercado.NomeResponsavel, 
		   AgenteMercado.Telefone ,
		   AgenteMercado.email

GO

--- VIEW PARA O REPORT - RANKING CUSTODIA

if exists(select * from sys.all_objects where name = 'v_RankingCustodiaReportView' and type = 'v')
	drop view v_RankingCustodiaReportView 
go 

 Create View v_RankingCustodiaReportView as 

  select Data, DataRef, Fundo, Instituicao, Responsavel, TelefoneContato, email, '1.' as CODCONTA, 'Total Custodiado' DESCRIÇÃO_DA_CONTA,  sum(saldo) as SALDO
  from v_RankingCustodiaAtivos
  group by Data, DataRef, Fundo, Instituicao, Responsavel, TelefoneContato, email

  UNION ALL 
  
 select Data, DataRef, Fundo, Instituicao, Responsavel, TelefoneContato, email, '1.1.' as CODCONTA, 'Mercado Doméstico' DESCRIÇÃO_DA_CONTA,  sum(saldo) as SALDO
  from v_RankingCustodiaAtivos
  where TipoMercadoInternoExterno = 'Domestico' 
  group by Data, DataRef, Fundo, Instituicao, Responsavel, TelefoneContato, email

  UNION ALL 

 select Data, DataRef, Fundo, Instituicao, Responsavel, TelefoneContato, email, '1.1.1.', 'Ativos de Origem da Própria Instituição', sum(saldo)
  from v_RankingCustodiaAtivos
  where TipoMercadoInternoExterno = 'Domestico' and 
        PortfolioMesmoConglomerado = 'S'  
   group by Data, DataRef, Fundo, Instituicao, Responsavel, TelefoneContato, email

  UNION ALL 

 select Data, DataRef, Fundo, Instituicao, Responsavel, TelefoneContato, email, '1.1.1.1.', 'Administrador/Gestor', sum(saldo)
  from v_RankingCustodiaAtivos
  where TipoMercadoInternoExterno = 'Domestico' and 
        PortfolioMesmoConglomerado = 'S'  and 
		Categoria = 1 -- 'Administrador/Gestor' 
   group by Data, DataRef, Fundo, Instituicao, Responsavel, TelefoneContato, email

  UNION ALL 

 select Data, DataRef, Fundo, Instituicao, Responsavel, TelefoneContato, email, '1.1.1.2.', 'EFPC', sum(saldo)
  from v_RankingCustodiaAtivos
  where TipoMercadoInternoExterno = 'Domestico' and 
        PortfolioMesmoConglomerado = 'S'  and 
		Categoria = 2 ---'EFPC' 
   group by Data, DataRef, Fundo, Instituicao, Responsavel, TelefoneContato, email

  UNION ALL 

 select Data, DataRef, Fundo, Instituicao, Responsavel, TelefoneContato, email, '1.1.1.3.', 'Empresas', sum(saldo)
  from v_RankingCustodiaAtivos
  where TipoMercadoInternoExterno = 'Domestico' and 
        PortfolioMesmoConglomerado = 'S'  and 
		Categoria = 3  ---'Empresas' 
   group by Data, DataRef, Fundo, Instituicao, Responsavel, TelefoneContato, email

  UNION ALL 

 select Data, DataRef, Fundo, Instituicao, Responsavel, TelefoneContato, email, '1.1.1.4.', 'Seguradoras', sum(saldo)
  from v_RankingCustodiaAtivos
  where TipoMercadoInternoExterno = 'Domestico' and 
        PortfolioMesmoConglomerado = 'S'  and 
		Categoria = 4 ---'Seguradoras' 
   group by Data, DataRef, Fundo, Instituicao, Responsavel, TelefoneContato, email

  UNION ALL 

 select Data, DataRef, Fundo, Instituicao, Responsavel, TelefoneContato, email, '1.1.1.6.', 'Outros Investidores Institucionais', sum(saldo)
  from v_RankingCustodiaAtivos
  where TipoMercadoInternoExterno = 'Domestico' and 
        PortfolioMesmoConglomerado = 'S'  and 
		Categoria in (6, 7, 8)  --- 'Outros Investidores Institucionais' 
   group by Data, DataRef, Fundo, Instituicao, Responsavel, TelefoneContato, email

  UNION ALL 

 select Data, DataRef, Fundo, Instituicao, Responsavel, TelefoneContato, email, '1.1.1.7.', 'FIDC', sum(saldo)
  from v_RankingCustodiaAtivos
  where TipoMercadoInternoExterno = 'Domestico' and 
        PortfolioMesmoConglomerado = 'S'  and 
		Categoria = 5 ----'FIDC' 
   group by Data, DataRef, Fundo, Instituicao, Responsavel, TelefoneContato, email

 UNION ALL

 select Data, DataRef, Fundo, Instituicao, Responsavel, TelefoneContato, email, '1.2.' as CODCONTA, 'Mercado Externo' DESCRIÇÃO_DA_CONTA,  sum(saldo) as SALDO
  from v_RankingCustodiaAtivos
  where TipoMercadoInternoExterno <> 'Domestico' 
  group by Data, DataRef, Fundo, Instituicao, Responsavel, TelefoneContato, email

  UNION ALL

 select Data, DataRef, Fundo, Instituicao, Responsavel, TelefoneContato, email, '1.2.1.' as CODCONTA, 'Ativos de Origem da Própria Instituição' DESCRIÇÃO_DA_CONTA,  sum(saldo) as SALDO
  from v_RankingCustodiaAtivos
  where TipoMercadoInternoExterno <> 'Domestico' and 
        TipoMercadoInternoExterno <> 'Outros' and 
        PortfolioMesmoConglomerado = 'S' 
  group by Data, DataRef, Fundo, Instituicao, Responsavel, TelefoneContato, email

    UNION ALL

 select Data, DataRef, Fundo, Instituicao, Responsavel, TelefoneContato, email, '1.2.1.1.' as CODCONTA, 'ADR' DESCRIÇÃO_DA_CONTA,  sum(saldo) as SALDO
  from v_RankingCustodiaAtivos
  where TipoMercadoInternoExterno = 'ADR' and 
        PortfolioMesmoConglomerado = 'S' 
  group by Data, DataRef, Fundo, Instituicao, Responsavel, TelefoneContato, email

      UNION ALL

 select Data, DataRef, Fundo, Instituicao, Responsavel, TelefoneContato, email, '1.2.1.2.' as CODCONTA, 'Resol. 2689' DESCRIÇÃO_DA_CONTA,  sum(saldo) as SALDO
  from v_RankingCustodiaAtivos
  where TipoMercadoInternoExterno = 'Resol. 2689' and 
        PortfolioMesmoConglomerado = 'S' 
  group by Data, DataRef, Fundo, Instituicao, Responsavel, TelefoneContato, email

    UNION ALL 

 select Data, DataRef, Fundo, Instituicao, Responsavel, TelefoneContato, email, '1.2.2.' as CODCONTA, 'Ativos de Origem de Outra Instituição' DESCRIÇÃO_DA_CONTA,  sum(saldo) as SALDO
  from v_RankingCustodiaAtivos
  where TipoMercadoInternoExterno <> 'Doméstico' and 
        TipoMercadoInternoExterno <> 'Outros' and 
        PortfolioMesmoConglomerado = 'N' 
  group by Data, DataRef, Fundo, Instituicao, Responsavel, TelefoneContato, email

    UNION ALL

 select Data, DataRef, Fundo, Instituicao, Responsavel, TelefoneContato, email, '1.2.2.1.' as CODCONTA, 'ADR' DESCRIÇÃO_DA_CONTA,  sum(saldo) as SALDO
  from v_RankingCustodiaAtivos
  where TipoMercadoInternoExterno = 'ADR' and 
        PortfolioMesmoConglomerado = 'N'
  group by Data, DataRef, Fundo, Instituicao, Responsavel, TelefoneContato, email 

      UNION ALL

 select Data, DataRef, Fundo, Instituicao, Responsavel, TelefoneContato, email, '1.2.2.2.' as CODCONTA, 'Resol. 2689' DESCRIÇÃO_DA_CONTA,  sum(saldo) as SALDO
  from v_RankingCustodiaAtivos
  where TipoMercadoInternoExterno = 'Resol. 2689' and 
        PortfolioMesmoConglomerado = 'N' 
  group by Data, DataRef, Fundo, Instituicao, Responsavel, TelefoneContato, email
  
  UNION ALL

 select Data, DataRef, Fundo, Instituicao, Responsavel, TelefoneContato, email, '1.2.3.' as CODCONTA, 'Outros' DESCRIÇÃO_DA_CONTA,  sum(saldo) as SALDO
  from v_RankingCustodiaAtivos
  where TipoMercadoInternoExterno = 'Outros' 
  group by Data, DataRef, Fundo, Instituicao, Responsavel, TelefoneContato, email

Go

-- Exemplos de utilização 
 
 select * 
  from v_RankingCustodiaAtivos 
  where Fundo = 24326 and Data = '2015-10-30'
  --order by data, CLIENTE, Mercado

-- select Cliente,Ativo, count(*)
--  from [v_ativos] 
--  where Data = '2014-06-18' 
--  group by Cliente,Ativo
--  order by Cliente, Ativo
  


select * from v_RankingCustodiaReportView 
 where Data = '2015-10-30'  and Fundo = 24326
 order by Data, fundo , CODCONTA 


 select * from PosicaoFUNDOHistorico
 where DataHistorico = '2015-10-30' and IdCliente = 24326

--  select * 
--  from   v_RankingCustodiaAtivos
--  where   Data = '2015-02-02' and Fundo = 5053 
--  order by Fundo