declare @data_versao char(10)
set @data_versao = '2013-04-15'
if exists (select 1 from VersaoSchema where DataVersao = @data_versao) return
insert into VersaoSchema VALUES(@data_versao)

alter table carteira add TipoCalculoRetorno tinyint null
go
update carteira set TipoCalculoRetorno = 1 --Calculo por cota
go
alter table carteira alter column TipoCalculoRetorno tinyint not null
go
