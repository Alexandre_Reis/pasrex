CREATE TABLE [TipoAtivoMasa](
	[CdAtivo] [varchar](20) NOT NULL,
	[Tipo] [tinyint] NOT NULL,
 CONSTRAINT [PK_TipoAtivoMasa] PRIMARY KEY CLUSTERED 
(
	[CdAtivo] ASC
)
)