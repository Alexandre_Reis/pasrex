declare @data_versao char(10)
set @data_versao = '2014-03-06'
if exists (select 1 from VersaoSchema where DataVersao = @data_versao) return
insert into VersaoSchema VALUES(@data_versao)

update Carteira set IdAgenteCustodiante = IdAgenteAdministrador
go