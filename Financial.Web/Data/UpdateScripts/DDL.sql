-- Se existir a trigger, apaga e cria de novo, pois triggers não guardam dados. 
-- Devem ser sempre recriadas para garantir que estão consistente com o script.
if exists(select * from sys.triggers where name = 'trg_cliente_DataAtualizacao')
begin
	drop trigger trg_cliente_DataAtualizacao
end

go 

-- trigger para preencher a data de ultima atualização 
create trigger trg_cliente_DataAtualizacao on dbo.Cliente
for update
as
begin	

	if (@@rowcount = 0)
	  return;

	set nocount on

	update Cliente 
	set 
	   DataAtualizacao = getdate()          
	where
		Cliente.IdCliente IN (select IdCliente from inserted)

end
go

-- Se existir a trigger, apaga e cria de novo, pois triggers não guardam dados. 
-- Devem ser sempre recriadas para garantir que estão consistente com o script.
if exists(select * from sys.triggers where name = 'trg_Pessoa_DataUltimaAlteracao')
begin
	drop trigger trg_Pessoa_DataUltimaAlteracao
end

go

-- trigger para preencher a data de ultima atualização 
create trigger trg_Pessoa_DataUltimaAlteracao on dbo.Pessoa
for update
as
begin	

	if (@@rowcount = 0)
	  return;

	set nocount on

	update Pessoa 
	set 
	   DataUltimaAlteracao = getdate()          
	where
		Pessoa.IdPessoa IN (select IdPessoa from inserted)

end
go



if exists (select 1 from information_schema.columns  
		   where table_name = 'AtivoBolsa' and column_name = 'CdAtivoBolsa' and character_maximum_length = 20)
begin

	/*
		Drop Constraint Feferenciadas na Ativobolsa (CdAtivoBolsa) 
	*/
	IF (OBJECT_ID('AtivoBolsa_PosicaoBolsaDetalhe_FK1', 'F') IS NOT NULL)
	BEGIN
		 ALTER TABLE [dbo].[PosicaoBolsaDetalhe] DROP CONSTRAINT [AtivoBolsa_PosicaoBolsaDetalhe_FK1];
	END
	 
	IF (OBJECT_ID('AtivoBolsa_ProventoBolsa_FK2', 'F') IS NOT NULL)
	BEGIN
		 ALTER TABLE [dbo].[ProventoBolsa] DROP CONSTRAINT [AtivoBolsa_ProventoBolsa_FK2];
	END
	 
	IF (OBJECT_ID('AtivoBolsa_ProventoBolsa_FK1', 'F') IS NOT NULL)
	BEGIN
		 ALTER TABLE [dbo].[ProventoBolsaCliente] DROP CONSTRAINT [AtivoBolsa_ProventoBolsa_FK1];
	END
	 
	IF (OBJECT_ID('AtivoBolsa_SubscricaoBolsa_FK1', 'F') IS NOT NULL)
	BEGIN
		 ALTER TABLE [dbo].[SubscricaoBolsa] DROP CONSTRAINT [AtivoBolsa_SubscricaoBolsa_FK1];
	END
	 
	IF (OBJECT_ID('AtivoBolsa_PosicaoEmprestimoBolsa_FK1', 'F') IS NOT NULL)
	BEGIN
		 ALTER TABLE [dbo].[PosicaoEmprestimoBolsa] DROP CONSTRAINT [AtivoBolsa_PosicaoEmprestimoBolsa_FK1];
	END
	 
	IF (OBJECT_ID('AtivoBolsa_SubscricaoBolsa_FK2', 'F') IS NOT NULL)
	BEGIN
		 ALTER TABLE [dbo].[SubscricaoBolsa] DROP CONSTRAINT [AtivoBolsa_SubscricaoBolsa_FK2];
	END
	 
	IF (OBJECT_ID('AtivoBolsa_GerPosicaoBolsa_FK1', 'F') IS NOT NULL)
	BEGIN
		 ALTER TABLE [dbo].[GerPosicaoBolsa] DROP CONSTRAINT [AtivoBolsa_GerPosicaoBolsa_FK1];
	END
	 
	IF (OBJECT_ID('AtivoBolsa_GerPosicaoBolsaAbertura_FK1', 'F') IS NOT NULL)
	BEGIN
		 ALTER TABLE [dbo].[GerPosicaoBolsaAbertura] DROP CONSTRAINT [AtivoBolsa_GerPosicaoBolsaAbertura_FK1];
	END
	 
	IF (OBJECT_ID('AtivoBolsa_TabelaCONR_FK1', 'F') IS NOT NULL)
	BEGIN
		 ALTER TABLE [dbo].[TabelaCONR] DROP CONSTRAINT [AtivoBolsa_TabelaCONR_FK1];
	END
	 
	IF (OBJECT_ID('AtivoBolsa_GerPosicaoBolsaHistorico_FK1', 'F') IS NOT NULL)
	BEGIN
		 ALTER TABLE [dbo].[GerPosicaoBolsaHistorico] DROP CONSTRAINT [AtivoBolsa_GerPosicaoBolsaHistorico_FK1];
	END
	 
	IF (OBJECT_ID('AtivoBolsa_PosicaoEmprestimoBolsaAbertura_FK1', 'F') IS NOT NULL)
	BEGIN
		 ALTER TABLE [dbo].[PosicaoEmprestimoBolsaAbertura] DROP CONSTRAINT [AtivoBolsa_PosicaoEmprestimoBolsaAbertura_FK1];
	END
	 
	IF (OBJECT_ID('AtivoBolsa_Grupamento_FK1', 'F') IS NOT NULL)
	BEGIN
		 ALTER TABLE [dbo].[GrupamentoBolsa] DROP CONSTRAINT [AtivoBolsa_Grupamento_FK1];
	END
	 
	IF (OBJECT_ID('AtivoBolsa_OrdemBolsa_FK1', 'F') IS NOT NULL)
	BEGIN
		 ALTER TABLE [dbo].[OrdemBolsa] DROP CONSTRAINT [AtivoBolsa_OrdemBolsa_FK1];
	END
	 
	IF (OBJECT_ID('AtivoBolsa_OrdemBolsa_FK2', 'F') IS NOT NULL)
	BEGIN
		 ALTER TABLE [dbo].[OrdemBolsa] DROP CONSTRAINT [AtivoBolsa_OrdemBolsa_FK2];
	END
	 
	IF (OBJECT_ID('AtivoBolsa_PosicaoEmprestimoBolsaHistorico_FK1', 'F') IS NOT NULL)
	BEGIN
		 ALTER TABLE [dbo].[PosicaoEmprestimoBolsaHistorico] DROP CONSTRAINT [AtivoBolsa_PosicaoEmprestimoBolsaHistorico_FK1];
	END
	 
	IF (OBJECT_ID('AtivoBolsa_AntecipacaoEmprestimoBolsa_FK1', 'F') IS NOT NULL)
	BEGIN
		 ALTER TABLE [dbo].[LiquidacaoEmprestimoBolsa] DROP CONSTRAINT [AtivoBolsa_AntecipacaoEmprestimoBolsa_FK1];
	END
	 
	IF (OBJECT_ID('AtivoBolsa_ConversaoBolsa_FK1', 'F') IS NOT NULL)
	BEGIN
		 ALTER TABLE [dbo].[ConversaoBolsa] DROP CONSTRAINT [AtivoBolsa_ConversaoBolsa_FK1];
	END
	 
	IF (OBJECT_ID('AtivoBolsa_ConversaoBolsa_FK2', 'F') IS NOT NULL)
	BEGIN
		 ALTER TABLE [dbo].[ConversaoBolsa] DROP CONSTRAINT [AtivoBolsa_ConversaoBolsa_FK2];
	END
	 
	IF (OBJECT_ID('AtivoBolsa_CotacaoBolsa_FK1', 'F') IS NOT NULL)
	BEGIN
		 ALTER TABLE [dbo].[CotacaoBolsa] DROP CONSTRAINT [AtivoBolsa_CotacaoBolsa_FK1];
	END
	 
	IF (OBJECT_ID('AtivoBolsa_LiquidacaoTermoBolsa_FK1', 'F') IS NOT NULL)
	BEGIN
		 ALTER TABLE [dbo].[LiquidacaoTermoBolsa] DROP CONSTRAINT [AtivoBolsa_LiquidacaoTermoBolsa_FK1];
	END
	 
	IF (OBJECT_ID('AtivoBolsa_DistribuicaoBolsa_FK1', 'F') IS NOT NULL)
	BEGIN
		 ALTER TABLE [dbo].[DistribuicaoBolsa] DROP CONSTRAINT [AtivoBolsa_DistribuicaoBolsa_FK1];
	END
	 
	IF (OBJECT_ID('AtivoBolsa_TransferenciaBolsa_FK1', 'F') IS NOT NULL)
	BEGIN
		 ALTER TABLE [dbo].[TransferenciaBolsa] DROP CONSTRAINT [AtivoBolsa_TransferenciaBolsa_FK1];
	END
	 
	IF (OBJECT_ID('AtivoBolsa_OperacaoBolsa_FK1', 'F') IS NOT NULL)
	BEGIN
		 ALTER TABLE [dbo].[OperacaoBolsa] DROP CONSTRAINT [AtivoBolsa_OperacaoBolsa_FK1];
	END
	 
	IF (OBJECT_ID('AtivoBolsa_OperacaoBolsa_FK2', 'F') IS NOT NULL)
	BEGIN
		 ALTER TABLE [dbo].[OperacaoBolsa] DROP CONSTRAINT [AtivoBolsa_OperacaoBolsa_FK2];
	END
	 
	IF (OBJECT_ID('AtivoBolsa_PosicaoBolsa_FK1', 'F') IS NOT NULL)
	BEGIN
		 ALTER TABLE [dbo].[PosicaoBolsa] DROP CONSTRAINT [AtivoBolsa_PosicaoBolsa_FK1];
	END
	 
	IF (OBJECT_ID('AtivoBolsa_PosicaoTermoBolsa_FK1', 'F') IS NOT NULL)
	BEGIN
		 ALTER TABLE [dbo].[PosicaoTermoBolsa] DROP CONSTRAINT [AtivoBolsa_PosicaoTermoBolsa_FK1];
	END
	 
	IF (OBJECT_ID('AtivoBolsa_PosicaoBolsaAbertura_FK1', 'F') IS NOT NULL)
	BEGIN
		 ALTER TABLE [dbo].[PosicaoBolsaAbertura] DROP CONSTRAINT [AtivoBolsa_PosicaoBolsaAbertura_FK1];
	END
	 
	IF (OBJECT_ID('AtivoBolsa_AtivoBolsaHistorico_FK1', 'F') IS NOT NULL)
	BEGIN
		 ALTER TABLE [dbo].[AtivoBolsaHistorico] DROP CONSTRAINT [AtivoBolsa_AtivoBolsaHistorico_FK1];
	END
	 
	IF (OBJECT_ID('AtivoBolsa_BloqueioBolsa_FK1', 'F') IS NOT NULL)
	BEGIN
		 ALTER TABLE [dbo].[BloqueioBolsa] DROP CONSTRAINT [AtivoBolsa_BloqueioBolsa_FK1];
	END
	 
	IF (OBJECT_ID('AtivoBolsa_EventoFisicoBolsa_FK1', 'F') IS NOT NULL)
	BEGIN
		 ALTER TABLE [dbo].[EventoFisicoBolsa] DROP CONSTRAINT [AtivoBolsa_EventoFisicoBolsa_FK1];
	END
	 
	IF (OBJECT_ID('AtivoBolsa_PosicaoTermoBolsaAbertura_FK1', 'F') IS NOT NULL)
	BEGIN
		 ALTER TABLE [dbo].[PosicaoTermoBolsaAbertura] DROP CONSTRAINT [AtivoBolsa_PosicaoTermoBolsaAbertura_FK1];
	END
	 
	IF (OBJECT_ID('AtivoBolsa_BonificacaoBolsa_FK1', 'F') IS NOT NULL)
	BEGIN
		 ALTER TABLE [dbo].[BonificacaoBolsa] DROP CONSTRAINT [AtivoBolsa_BonificacaoBolsa_FK1];
	END
	 
	IF (OBJECT_ID('FK_EvolucaoCapitalSocial_AtivoBolsa', 'F') IS NOT NULL)
	BEGIN
		 ALTER TABLE [dbo].[EvolucaoCapitalSocial] DROP CONSTRAINT [FK_EvolucaoCapitalSocial_AtivoBolsa];
	END
	 
	IF (OBJECT_ID('AtivoBolsa_BonificacaoBolsa_FK2', 'F') IS NOT NULL)
	BEGIN
		 ALTER TABLE [dbo].[BonificacaoBolsa] DROP CONSTRAINT [AtivoBolsa_BonificacaoBolsa_FK2];
	END
	 
	IF (OBJECT_ID('AtivoBolsa_PosicaoBolsaHistorico_FK1', 'F') IS NOT NULL)
	BEGIN
		 ALTER TABLE [dbo].[PosicaoBolsaHistorico] DROP CONSTRAINT [AtivoBolsa_PosicaoBolsaHistorico_FK1];
	END
	 
	IF (OBJECT_ID('AtivoBolsa_FatorCotacaoBolsa_FK1', 'F') IS NOT NULL)
	BEGIN
		 ALTER TABLE [dbo].[FatorCotacaoBolsa] DROP CONSTRAINT [AtivoBolsa_FatorCotacaoBolsa_FK1];
	END
	 
	IF (OBJECT_ID('AtivoBolsa_PosicaoTermoBolsaHistorico_FK1', 'F') IS NOT NULL)
	BEGIN
		 ALTER TABLE [dbo].[PosicaoTermoBolsaHistorico] DROP CONSTRAINT [AtivoBolsa_PosicaoTermoBolsaHistorico_FK1];
	END
	 
	IF (OBJECT_ID('AtivoBolsa_GerOperacaoBolsa_FK1', 'F') IS NOT NULL)
	BEGIN
		 ALTER TABLE [dbo].[GerOperacaoBolsa] DROP CONSTRAINT [AtivoBolsa_GerOperacaoBolsa_FK1];
	END
	 
	IF (OBJECT_ID('AtivoBolsaHistorico_PK', 'PK') IS NOT NULL)
	BEGIN
		 ALTER TABLE [dbo].[AtivoBolsaHistorico] DROP CONSTRAINT [AtivoBolsaHistorico_PK];
	END
	 
	IF (OBJECT_ID('CotacaoBolsa_PK', 'PK') IS NOT NULL)
	BEGIN
		 ALTER TABLE [dbo].[CotacaoBolsa] DROP CONSTRAINT [CotacaoBolsa_PK];
	END
	 
	IF (OBJECT_ID('DistribuicaoBolsa_PK', 'PK') IS NOT NULL)
	BEGIN
		 ALTER TABLE [dbo].[DistribuicaoBolsa] DROP CONSTRAINT [DistribuicaoBolsa_PK] ;
	END
	 
	IF (OBJECT_ID('PK_EvolucaoCapitalSocial', 'PK') IS NOT NULL)
	BEGIN
		 ALTER TABLE [dbo].[EvolucaoCapitalSocial] DROP CONSTRAINT [PK_EvolucaoCapitalSocial] ;
	END
	 
	IF (OBJECT_ID('FatorCotacaoBolsa_PK', 'PK') IS NOT NULL)
	BEGIN
		 ALTER TABLE [dbo].[FatorCotacaoBolsa] DROP CONSTRAINT [FatorCotacaoBolsa_PK] ;
	END
	 
	IF (OBJECT_ID('PosicaoBolsaDetalhe_PK', 'PK') IS NOT NULL)
	BEGIN
		 ALTER TABLE [dbo].[PosicaoBolsaDetalhe] DROP CONSTRAINT [PosicaoBolsaDetalhe_PK]; 
	END
	 


		IF (OBJECT_ID('OperacaoSwap_AtivoBolsa_FK', 'F') IS NOT NULL)
		BEGIN
			 ALTER TABLE [dbo].OperacaoSwap DROP CONSTRAINT OperacaoSwap_AtivoBolsa_FK;
		END

		IF (OBJECT_ID('OperacaoSwap_AtivoBolsaCP_FK', 'F') IS NOT NULL)
		BEGIN
				ALTER TABLE [dbo].OperacaoSwap DROP CONSTRAINT OperacaoSwap_AtivoBolsaCP_FK;
		END

		IF (OBJECT_ID('PosicaoSwap_AtivoBolsa_FK', 'F') IS NOT NULL)
		BEGIN
				ALTER TABLE [dbo].PosicaoSwap DROP CONSTRAINT PosicaoSwap_AtivoBolsa_FK;
		END

		IF (OBJECT_ID('PosicaoSwap_AtivoBolsaCP_FK', 'F') IS NOT NULL)
		BEGIN
				ALTER TABLE [dbo].PosicaoSwap DROP CONSTRAINT PosicaoSwap_AtivoBolsaCP_FK;
		END

		IF (OBJECT_ID('PosicaoSwapAbertura_AtivoBolsa_FK', 'F') IS NOT NULL)
		BEGIN
				ALTER TABLE [dbo].PosicaoSwapAbertura DROP CONSTRAINT PosicaoSwapAbertura_AtivoBolsa_FK;
		END

		IF (OBJECT_ID('PosicaoSwapAber_AtivoBolsaCP_FK', 'F') IS NOT NULL)
		BEGIN
				ALTER TABLE [dbo].PosicaoSwapAbertura DROP CONSTRAINT PosicaoSwapAber_AtivoBolsaCP_FK;
		END

		IF (OBJECT_ID('PosicaoSwapHist_AtivoBolsa_FK', 'F') IS NOT NULL)
		BEGIN
				ALTER TABLE [dbo].PosicaoSwapHistorico DROP CONSTRAINT PosicaoSwapHist_AtivoBolsa_FK;
		END

		IF (OBJECT_ID('PosicaoSwapHist_AtivoBolsaCP_FK', 'F') IS NOT NULL)
		BEGIN
				ALTER TABLE [dbo].PosicaoSwapHistorico DROP CONSTRAINT PosicaoSwapHist_AtivoBolsaCP_FK;
		END

		IF (OBJECT_ID('IncorporacaoCisaoFusaoBolsaDetalhe_PK', 'PK') IS NOT NULL)
		BEGIN
				ALTER TABLE [dbo].IncorporacaoCisaoFusaoBolsaDetalhe DROP CONSTRAINT IncorporacaoCisaoFusaoBolsaDetalhe_PK;
		END

		IF (OBJECT_ID('EventoFundoBolsa_PK', 'PK') IS NOT NULL)
		BEGIN
				ALTER TABLE [dbo].EventoFundoBolsa DROP CONSTRAINT EventoFundoBolsa_PK;
		END

		IF (OBJECT_ID('IncorporacaoCisaoFusaoBolsa_AtivoBolsa_FK1', 'F') IS NOT NULL)
		BEGIN
				ALTER TABLE [dbo].IncorporacaoCisaoFusaoBolsa DROP CONSTRAINT IncorporacaoCisaoFusaoBolsa_AtivoBolsa_FK1;
		END

		IF (OBJECT_ID('IncorporacaoCisaoFusaoBolsa_PK', 'PK') IS NOT NULL)
		BEGIN
				ALTER TABLE [dbo].IncorporacaoCisaoFusaoBolsa DROP CONSTRAINT IncorporacaoCisaoFusaoBolsa_PK;
		END

		IF (OBJECT_ID('IncorporacaoCisaoFusaoBolsaDetalhe_AtivoBolsa_FK1', 'F') IS NOT NULL)
		BEGIN
				ALTER TABLE [dbo].IncorporacaoCisaoFusaoBolsaDetalhe DROP CONSTRAINT IncorporacaoCisaoFusaoBolsaDetalhe_AtivoBolsa_FK1;
		END

		IF (OBJECT_ID('IncorporacaoCisaoFusaoBolsaDetalhe_PK', 'PK') IS NOT NULL)
		BEGIN
				ALTER TABLE [dbo].IncorporacaoCisaoFusaoBolsaDetalhe DROP CONSTRAINT IncorporacaoCisaoFusaoBolsaDetalhe_PK;
		END


	 IF EXISTS (SELECT CONSTRAINT_NAME FROM information_schema.KEY_COLUMN_USAGE WHERE table_name = 'ExcecoesTributacaoIR' AND COLUMN_NAME = 'CdAtivoBolsa') 
	 BEGIN
	  DECLARE @constraintName nvarchar(2000);
	  SET @constraintName = (SELECT CONSTRAINT_NAME FROM information_schema.KEY_COLUMN_USAGE WHERE table_name = 'ExcecoesTributacaoIR' AND COLUMN_NAME = 'CdAtivoBolsa');

	  EXEC('ALTER TABLE ExcecoesTributacaoIR DROP CONSTRAINT ' + @constraintName);
	 END 








	IF (OBJECT_ID('AtivoBolsa_PK', 'PK') IS NOT NULL)
	BEGIN
		 ALTER TABLE [dbo].[AtivoBolsa] DROP CONSTRAINT [AtivoBolsa_PK] ;
	END
	 

	/*
		altera tamanho da coluna CdAtivoBolsa para 25
	*/

	ALTER TABLE AtivoBolsa ALTER COLUMN CdAtivoBolsa VARCHAR(25) NOT NULL;
	 
	ALTER TABLE AtivoBolsa ALTER COLUMN CdAtivoBolsaObjeto VARCHAR(25) ;
	 
	ALTER TABLE PosicaoBolsaDetalhe ALTER COLUMN CdAtivoBolsa VARCHAR(25) NOT NULL;
	 
	ALTER TABLE AtivoBolsaHistorico ALTER COLUMN CdAtivoBolsa VARCHAR(25) NOT NULL;
	 
	ALTER TABLE BloqueioBolsa ALTER COLUMN CdAtivoBolsa VARCHAR(25) NOT NULL;
	 
	ALTER TABLE PosicaoBolsa ALTER COLUMN CdAtivoBolsa VARCHAR(25) NOT NULL;
	 
	ALTER TABLE PosicaoBolsaAbertura ALTER COLUMN CdAtivoBolsa VARCHAR(25) NOT NULL;
	 
	ALTER TABLE EventoFisicoBolsa ALTER COLUMN CdAtivoBolsa VARCHAR(25) NOT NULL;
	 
	ALTER TABLE BonificacaoBolsa ALTER COLUMN CdAtivoBolsa VARCHAR(25) NOT NULL;
	 
	ALTER TABLE BonificacaoBolsa ALTER COLUMN CdAtivoBolsaDestino VARCHAR(25) NOT NULL;
	 
	ALTER TABLE PosicaoBolsaHistorico ALTER COLUMN CdAtivoBolsa VARCHAR(25) NOT NULL;
	 
	ALTER TABLE EvolucaoCapitalSocial ALTER COLUMN CdAtivoBolsa VARCHAR(25) NOT NULL;
	 
	ALTER TABLE FatorCotacaoBolsa ALTER COLUMN CdAtivoBolsa VARCHAR(25) NOT NULL;
	 
	ALTER TABLE PosicaoEmprestimoBolsa ALTER COLUMN CdAtivoBolsa VARCHAR(25) NOT NULL;
	 
	ALTER TABLE PosicaoEmprestimoBolsaAbertura ALTER COLUMN CdAtivoBolsa VARCHAR(25) NOT NULL;
	 
	ALTER TABLE GerOperacaoBolsa ALTER COLUMN CdAtivoBolsa VARCHAR(25) NOT NULL;
	 
	ALTER TABLE GerOperacaoBolsa ALTER COLUMN CdAtivoBolsaOpcao VARCHAR(25) NULL;
	 
	ALTER TABLE PosicaoEmprestimoBolsaHistorico ALTER COLUMN CdAtivoBolsa VARCHAR(25) NOT NULL;
	 
	ALTER TABLE TransferenciaBolsa ALTER COLUMN CdAtivoBolsa VARCHAR(25) NOT NULL;
	 
	ALTER TABLE GerPosicaoBolsa ALTER COLUMN CdAtivoBolsa VARCHAR(25) NOT NULL;
	 
	ALTER TABLE GerPosicaoBolsaAbertura ALTER COLUMN CdAtivoBolsa VARCHAR(25) NOT NULL;
	 
	ALTER TABLE GerPosicaoBolsaHistorico ALTER COLUMN CdAtivoBolsa VARCHAR(25) NOT NULL;
	 
	ALTER TABLE GrupamentoBolsa ALTER COLUMN CdAtivoBolsa VARCHAR(25) NOT NULL;
	 
	ALTER TABLE PosicaoTermoBolsa ALTER COLUMN CdAtivoBolsa VARCHAR(25) NOT NULL;
	 
	ALTER TABLE PosicaoTermoBolsaAbertura ALTER COLUMN CdAtivoBolsa VARCHAR(25) NOT NULL;
	 
	ALTER TABLE PosicaoTermoBolsaHistorico ALTER COLUMN CdAtivoBolsa VARCHAR(25) NOT NULL;
	 
	ALTER TABLE ProventoBolsa ALTER COLUMN CdAtivoBolsa VARCHAR(25) NOT NULL;
	 
	ALTER TABLE ProventoBolsaCliente ALTER COLUMN CdAtivoBolsa VARCHAR(25) NOT NULL;
	 
	ALTER TABLE LiquidacaoEmprestimoBolsa ALTER COLUMN CdAtivoBolsa VARCHAR(25) NOT NULL;
	 
	ALTER TABLE SubscricaoBolsa ALTER COLUMN CdAtivoBolsa VARCHAR(25) NOT NULL;
	 
	ALTER TABLE SubscricaoBolsa ALTER COLUMN CdAtivoBolsaDireito VARCHAR(25) NOT NULL;
	 
	ALTER TABLE ConversaoBolsa ALTER COLUMN CdAtivoBolsa VARCHAR(25) NOT NULL;
	 
	ALTER TABLE ConversaoBolsa ALTER COLUMN CdAtivoBolsaDestino VARCHAR(25) NOT NULL;
	 
	ALTER TABLE LiquidacaoTermoBolsa ALTER COLUMN CdAtivoBolsa VARCHAR(25) NOT NULL;
	 
	ALTER TABLE TabelaCONR ALTER COLUMN CdAtivoBolsa VARCHAR(25) NOT NULL;
	 
	ALTER TABLE CotacaoBolsa ALTER COLUMN CdAtivoBolsa VARCHAR(25) NOT NULL;
	 
	ALTER TABLE OperacaoBolsa ALTER COLUMN CdAtivoBolsa VARCHAR(25) NOT NULL;
	 
	ALTER TABLE OperacaoBolsa ALTER COLUMN CdAtivoBolsaOpcao VARCHAR(25) ;
	 
	ALTER TABLE OperacaoEmprestimoBolsa ALTER COLUMN CdAtivoBolsa VARCHAR(25) NOT NULL;
	 
	ALTER TABLE OrdemBolsa ALTER COLUMN CdAtivoBolsa VARCHAR(25) NOT NULL;
	 
	ALTER TABLE OrdemBolsa ALTER COLUMN CdAtivoBolsaOpcao VARCHAR(25);
	 
	ALTER TABLE DistribuicaoBolsa ALTER COLUMN CdAtivoBolsa VARCHAR(25) NOT NULL;
	 

	/*
		Recria as constraints com AtivoBolsa (CdAtivoBolsa) 
	*/
	IF (OBJECT_ID('AtivoBolsa_PK', 'PK') IS NULL)
	BEGIN
		ALTER TABLE [dbo].[AtivoBolsa]  ADD CONSTRAINT [AtivoBolsa_PK] PRIMARY KEY (CdAtivoBolsa);
	END
	 
	IF (OBJECT_ID('AtivoBolsaHistorico_PK', 'PK') IS NULL)
	BEGIN
		ALTER TABLE [dbo].[AtivoBolsaHistorico]  ADD CONSTRAINT [AtivoBolsaHistorico_PK] PRIMARY KEY (DataReferencia,CdAtivoBolsa);
	END
	  
	IF (OBJECT_ID('CotacaoBolsa_PK', 'PK') IS NULL)
	BEGIN
		ALTER TABLE [dbo].[CotacaoBolsa]	ADD CONSTRAINT	[CotacaoBolsa_PK] PRIMARY KEY 	(Data,CdAtivoBolsa);
	END
	  
	IF (OBJECT_ID('DistribuicaoBolsa_PK', 'PK') IS NULL)
	BEGIN
		ALTER TABLE [dbo].[DistribuicaoBolsa]	ADD CONSTRAINT	[DistribuicaoBolsa_PK] PRIMARY KEY (DataReferencia,CdAtivoBolsa);
	END
	  
	IF (OBJECT_ID('PK_EvolucaoCapitalSocial', 'PK') IS NULL)
	BEGIN
	   ALTER TABLE [dbo].[EvolucaoCapitalSocial]	ADD CONSTRAINT	[PK_EvolucaoCapitalSocial] PRIMARY KEY CLUSTERED	(CdAtivoBolsa,Data);
	END
	 
	IF (OBJECT_ID('FatorCotacaoBolsa_PK', 'PK') IS NULL)
	BEGIN
		ALTER TABLE [dbo].[FatorCotacaoBolsa]	ADD CONSTRAINT	[FatorCotacaoBolsa_PK]	 PRIMARY KEY (DataReferencia,CdAtivoBolsa);
	END
	 
	IF (OBJECT_ID('PosicaoBolsaDetalhe_PK', 'PK') IS NULL)
	BEGIN
		ALTER TABLE [dbo].[PosicaoBolsaDetalhe]	ADD CONSTRAINT	[PosicaoBolsaDetalhe_PK] PRIMARY KEY CLUSTERED (DataHistorico, IdCliente, IdAgente, CdAtivoBolsa, TipoCarteira);
	END
	 
	IF (OBJECT_ID('AtivoBolsa_AntecipacaoEmprestimoBolsa_FK1', 'F') IS NULL)
	BEGIN
		ALTER TABLE [dbo].[LiquidacaoEmprestimoBolsa] ADD CONSTRAINT [AtivoBolsa_AntecipacaoEmprestimoBolsa_FK1] FOREIGN KEY ([CdAtivoBolsa]) REFERENCES [dbo].[AtivoBolsa]([CdAtivoBolsa]) ON DELETE CASCADE;
	END
	  
	IF (OBJECT_ID('AtivoBolsa_AtivoBolsaHistorico_FK1', 'F') IS NULL)
	BEGIN
		ALTER TABLE [dbo].[AtivoBolsaHistorico] ADD CONSTRAINT [AtivoBolsa_AtivoBolsaHistorico_FK1] FOREIGN KEY ([CdAtivoBolsa]) REFERENCES [dbo].[AtivoBolsa]([CdAtivoBolsa]) ON DELETE CASCADE;
	END
	  
	IF (OBJECT_ID('AtivoBolsa_BloqueioBolsa_FK1', 'F') IS NULL)
	BEGIN
		ALTER TABLE [dbo].[BloqueioBolsa] ADD CONSTRAINT [AtivoBolsa_BloqueioBolsa_FK1] FOREIGN KEY ([CdAtivoBolsa]) REFERENCES [dbo].[AtivoBolsa]([CdAtivoBolsa]) ON DELETE CASCADE;
	END
	  
	IF (OBJECT_ID('AtivoBolsa_CotacaoBolsa_FK1', 'F') IS NULL)
	BEGIN
		ALTER TABLE [dbo].[CotacaoBolsa] ADD CONSTRAINT [AtivoBolsa_CotacaoBolsa_FK1] FOREIGN KEY ([CdAtivoBolsa]) REFERENCES [dbo].[AtivoBolsa]([CdAtivoBolsa]) ON DELETE CASCADE;
	END
	  
	IF (OBJECT_ID('AtivoBolsa_DistribuicaoBolsa_FK1', 'F') IS NULL)
	BEGIN
		ALTER TABLE [dbo].[DistribuicaoBolsa] ADD CONSTRAINT [AtivoBolsa_DistribuicaoBolsa_FK1] FOREIGN KEY ([CdAtivoBolsa]) REFERENCES [dbo].[AtivoBolsa]([CdAtivoBolsa]) ON DELETE CASCADE;
	END
	  
	IF (OBJECT_ID('AtivoBolsa_EventoFisicoBolsa_FK1', 'F') IS NULL)
	BEGIN
		ALTER TABLE [dbo].[EventoFisicoBolsa] ADD CONSTRAINT [AtivoBolsa_EventoFisicoBolsa_FK1] FOREIGN KEY ([CdAtivoBolsa]) REFERENCES [dbo].[AtivoBolsa]([CdAtivoBolsa]) ON DELETE CASCADE;
	END
	  
	IF (OBJECT_ID('AtivoBolsa_FatorCotacaoBolsa_FK1', 'F') IS NULL)
	BEGIN
		ALTER TABLE [dbo].[FatorCotacaoBolsa] ADD CONSTRAINT [AtivoBolsa_FatorCotacaoBolsa_FK1] FOREIGN KEY ([CdAtivoBolsa]) REFERENCES [dbo].[AtivoBolsa]([CdAtivoBolsa]) ON DELETE CASCADE;
	END
	  
	IF (OBJECT_ID('AtivoBolsa_Grupamento_FK1', 'F') IS NULL)
	BEGIN
		ALTER TABLE [dbo].[GrupamentoBolsa] ADD CONSTRAINT [AtivoBolsa_Grupamento_FK1] FOREIGN KEY ([CdAtivoBolsa]) REFERENCES [dbo].[AtivoBolsa]([CdAtivoBolsa]) ON DELETE CASCADE;
	END
	  
	IF (OBJECT_ID('AtivoBolsa_LiquidacaoTermoBolsa_FK1', 'F') IS NULL)
	BEGIN
		ALTER TABLE [dbo].[LiquidacaoTermoBolsa] ADD CONSTRAINT [AtivoBolsa_LiquidacaoTermoBolsa_FK1] FOREIGN KEY ([CdAtivoBolsa]) REFERENCES [dbo].[AtivoBolsa]([CdAtivoBolsa]) ON DELETE CASCADE;
	END
	  
	IF (OBJECT_ID('AtivoBolsa_OperacaoBolsa_FK1', 'F') IS NULL)
	BEGIN
		ALTER TABLE [dbo].[OperacaoBolsa] ADD CONSTRAINT [AtivoBolsa_OperacaoBolsa_FK1] FOREIGN KEY ([CdAtivoBolsa]) REFERENCES [dbo].[AtivoBolsa]([CdAtivoBolsa]) ON DELETE CASCADE;
	END
	  
	IF (OBJECT_ID('AtivoBolsa_OrdemBolsa_FK1', 'F') IS NULL)
	BEGIN
		ALTER TABLE [dbo].[OrdemBolsa] ADD CONSTRAINT [AtivoBolsa_OrdemBolsa_FK1] FOREIGN KEY ([CdAtivoBolsa]) REFERENCES [dbo].[AtivoBolsa]([CdAtivoBolsa]) ON DELETE CASCADE;
	END
	  
	IF (OBJECT_ID('AtivoBolsa_PosicaoBolsa_FK1', 'F') IS NULL)
	BEGIN
		ALTER TABLE [dbo].[PosicaoBolsa] ADD CONSTRAINT [AtivoBolsa_PosicaoBolsa_FK1] FOREIGN KEY ([CdAtivoBolsa]) REFERENCES [dbo].[AtivoBolsa]([CdAtivoBolsa]) ON DELETE CASCADE;
	END
	  
	IF (OBJECT_ID('AtivoBolsa_PosicaoBolsaAbertura_FK1', 'F') IS NULL)
	BEGIN
		ALTER TABLE [dbo].[PosicaoBolsaAbertura] ADD CONSTRAINT [AtivoBolsa_PosicaoBolsaAbertura_FK1] FOREIGN KEY ([CdAtivoBolsa]) REFERENCES [dbo].[AtivoBolsa]([CdAtivoBolsa]) ON DELETE CASCADE;
	END
	  
	IF (OBJECT_ID('AtivoBolsa_PosicaoBolsaDetalhe_FK1', 'F') IS NULL)
	BEGIN
		ALTER TABLE [dbo].[PosicaoBolsaDetalhe] ADD CONSTRAINT [AtivoBolsa_PosicaoBolsaDetalhe_FK1] FOREIGN KEY ([CdAtivoBolsa]) REFERENCES [dbo].[AtivoBolsa]([CdAtivoBolsa]) ON DELETE CASCADE;
	END
	  
	IF (OBJECT_ID('AtivoBolsa_PosicaoBolsaHistorico_FK1', 'F') IS NULL)
	BEGIN
		ALTER TABLE [dbo].[PosicaoBolsaHistorico] ADD CONSTRAINT [AtivoBolsa_PosicaoBolsaHistorico_FK1] FOREIGN KEY ([CdAtivoBolsa]) REFERENCES [dbo].[AtivoBolsa]([CdAtivoBolsa]) ON DELETE CASCADE;
	END
	  
	IF (OBJECT_ID('AtivoBolsa_PosicaoEmprestimoBolsaAbertura_FK1', 'F') IS NULL)
	BEGIN
		ALTER TABLE [dbo].[PosicaoEmprestimoBolsaAbertura] ADD CONSTRAINT [AtivoBolsa_PosicaoEmprestimoBolsaAbertura_FK1] FOREIGN KEY ([CdAtivoBolsa]) REFERENCES [dbo].[AtivoBolsa]([CdAtivoBolsa]) ON DELETE CASCADE;
	END
	  
	IF (OBJECT_ID('AtivoBolsa_PosicaoEmprestimoBolsaHistorico_FK1', 'F') IS NULL)
	BEGIN
		ALTER TABLE [dbo].[PosicaoEmprestimoBolsaHistorico] ADD CONSTRAINT [AtivoBolsa_PosicaoEmprestimoBolsaHistorico_FK1] FOREIGN KEY ([CdAtivoBolsa]) REFERENCES [dbo].[AtivoBolsa]([CdAtivoBolsa]) ON DELETE CASCADE;
	END
	  
	IF (OBJECT_ID('AtivoBolsa_PosicaoTermoBolsaAbertura_FK1', 'F') IS NULL)
	BEGIN
		ALTER TABLE [dbo].[PosicaoTermoBolsaAbertura] ADD CONSTRAINT [AtivoBolsa_PosicaoTermoBolsaAbertura_FK1] FOREIGN KEY ([CdAtivoBolsa]) REFERENCES [dbo].[AtivoBolsa]([CdAtivoBolsa]) ON DELETE CASCADE;
	END
	  
	IF (OBJECT_ID('AtivoBolsa_PosicaoTermoBolsaHistorico_FK1', 'F') IS NULL)
	BEGIN
		ALTER TABLE [dbo].[PosicaoTermoBolsaHistorico] ADD CONSTRAINT [AtivoBolsa_PosicaoTermoBolsaHistorico_FK1] FOREIGN KEY ([CdAtivoBolsa]) REFERENCES [dbo].[AtivoBolsa]([CdAtivoBolsa]) ON DELETE CASCADE;
	END
	  
	IF (OBJECT_ID('AtivoBolsa_ProventoBolsa_FK1', 'F') IS NULL)
	BEGIN
		ALTER TABLE [dbo].[ProventoBolsaCliente] ADD CONSTRAINT [AtivoBolsa_ProventoBolsa_FK1] FOREIGN KEY ([CdAtivoBolsa]) REFERENCES [dbo].[AtivoBolsa]([CdAtivoBolsa]) ON DELETE CASCADE;
	END
	  
	IF (OBJECT_ID('AtivoBolsa_ProventoBolsa_FK2', 'F') IS NULL)
	BEGIN
		ALTER TABLE [dbo].[ProventoBolsa] ADD CONSTRAINT [AtivoBolsa_ProventoBolsa_FK2] FOREIGN KEY ([CdAtivoBolsa]) REFERENCES [dbo].[AtivoBolsa]([CdAtivoBolsa]) ON DELETE CASCADE;
	END
	  
	IF (OBJECT_ID('AtivoBolsa_SubscricaoBolsa_FK1', 'F') IS NULL)
	BEGIN
		ALTER TABLE [dbo].[SubscricaoBolsa] ADD CONSTRAINT [AtivoBolsa_SubscricaoBolsa_FK1] FOREIGN KEY ([CdAtivoBolsa]) REFERENCES [dbo].[AtivoBolsa]([CdAtivoBolsa]) ON DELETE CASCADE;
	END
	  
	IF (OBJECT_ID('AtivoBolsa_TransferenciaBolsa_FK1', 'F') IS NULL)
	BEGIN
		ALTER TABLE [dbo].[TransferenciaBolsa] ADD CONSTRAINT [AtivoBolsa_TransferenciaBolsa_FK1] FOREIGN KEY ([CdAtivoBolsa]) REFERENCES [dbo].[AtivoBolsa]([CdAtivoBolsa]) ON DELETE CASCADE;
	END
	  
	IF (OBJECT_ID('AtivoBolsa_BonificacaoBolsa_FK1', 'F') IS NULL)
	BEGIN
		ALTER TABLE [dbo].[BonificacaoBolsa] ADD CONSTRAINT [AtivoBolsa_BonificacaoBolsa_FK1] FOREIGN KEY ([CdAtivoBolsa]) REFERENCES [dbo].[AtivoBolsa]([CdAtivoBolsa]);
	END
	  
	IF (OBJECT_ID('AtivoBolsa_BonificacaoBolsa_FK2', 'F') IS NULL)
	BEGIN
		ALTER TABLE [dbo].[BonificacaoBolsa] ADD CONSTRAINT [AtivoBolsa_BonificacaoBolsa_FK2] FOREIGN KEY ([CdAtivoBolsaDestino]) REFERENCES [dbo].[AtivoBolsa]([CdAtivoBolsa]);
	END
	  
	IF (OBJECT_ID('AtivoBolsa_ConversaoBolsa_FK1', 'F') IS NULL)
	BEGIN
		ALTER TABLE [dbo].[ConversaoBolsa] ADD CONSTRAINT [AtivoBolsa_ConversaoBolsa_FK1] FOREIGN KEY ([CdAtivoBolsa]) REFERENCES [dbo].[AtivoBolsa]([CdAtivoBolsa]);
	END
	  
	IF (OBJECT_ID('AtivoBolsa_ConversaoBolsa_FK2', 'F') IS NULL)
	BEGIN
		ALTER TABLE [dbo].[ConversaoBolsa] ADD CONSTRAINT [AtivoBolsa_ConversaoBolsa_FK2] FOREIGN KEY ([CdAtivoBolsaDestino]) REFERENCES [dbo].[AtivoBolsa]([CdAtivoBolsa]);
	END
	  
	IF (OBJECT_ID('AtivoBolsa_GerOperacaoBolsa_FK1', 'F') IS NULL)
	BEGIN
		ALTER TABLE [dbo].[GerOperacaoBolsa] ADD CONSTRAINT [AtivoBolsa_GerOperacaoBolsa_FK1] FOREIGN KEY ([CdAtivoBolsa]) REFERENCES [dbo].[AtivoBolsa]([CdAtivoBolsa]);
	END
	  
	IF (OBJECT_ID('AtivoBolsa_GerPosicaoBolsa_FK1', 'F') IS NULL)
	BEGIN
		ALTER TABLE [dbo].[GerPosicaoBolsa] ADD CONSTRAINT [AtivoBolsa_GerPosicaoBolsa_FK1] FOREIGN KEY ([CdAtivoBolsa]) REFERENCES [dbo].[AtivoBolsa]([CdAtivoBolsa]);
	END
	  
	IF (OBJECT_ID('AtivoBolsa_GerPosicaoBolsaAbertura_FK1', 'F') IS NULL)
	BEGIN
		ALTER TABLE [dbo].[GerPosicaoBolsaAbertura] ADD CONSTRAINT [AtivoBolsa_GerPosicaoBolsaAbertura_FK1] FOREIGN KEY ([CdAtivoBolsa]) REFERENCES [dbo].[AtivoBolsa]([CdAtivoBolsa]);
	END
	  
	IF (OBJECT_ID('AtivoBolsa_GerPosicaoBolsaHistorico_FK1', 'F') IS NULL)
	BEGIN
		ALTER TABLE [dbo].[GerPosicaoBolsaHistorico] ADD CONSTRAINT [AtivoBolsa_GerPosicaoBolsaHistorico_FK1] FOREIGN KEY ([CdAtivoBolsa]) REFERENCES [dbo].[AtivoBolsa]([CdAtivoBolsa]);
	END
	  
	IF (OBJECT_ID('AtivoBolsa_OperacaoBolsa_FK2', 'F') IS NULL)
	BEGIN
		ALTER TABLE [dbo].[OperacaoBolsa] ADD CONSTRAINT [AtivoBolsa_OperacaoBolsa_FK2] FOREIGN KEY ([CdAtivoBolsaOpcao]) REFERENCES [dbo].[AtivoBolsa]([CdAtivoBolsa]);
	END
	  
	IF (OBJECT_ID('AtivoBolsa_OrdemBolsa_FK2', 'F') IS NULL)
	BEGIN
		ALTER TABLE [dbo].[OrdemBolsa] ADD CONSTRAINT [AtivoBolsa_OrdemBolsa_FK2] FOREIGN KEY ([CdAtivoBolsaOpcao]) REFERENCES [dbo].[AtivoBolsa]([CdAtivoBolsa]);
	END
	  
	IF (OBJECT_ID('AtivoBolsa_PosicaoEmprestimoBolsa_FK1', 'F') IS NULL)
	BEGIN
		ALTER TABLE [dbo].[PosicaoEmprestimoBolsa] ADD CONSTRAINT [AtivoBolsa_PosicaoEmprestimoBolsa_FK1] FOREIGN KEY ([CdAtivoBolsa]) REFERENCES [dbo].[AtivoBolsa]([CdAtivoBolsa]);
	END
	  
	IF (OBJECT_ID('AtivoBolsa_PosicaoTermoBolsa_FK1', 'F') IS NULL)
	BEGIN
		ALTER TABLE [dbo].[PosicaoTermoBolsa] ADD CONSTRAINT [AtivoBolsa_PosicaoTermoBolsa_FK1] FOREIGN KEY ([CdAtivoBolsa]) REFERENCES [dbo].[AtivoBolsa]([CdAtivoBolsa]);
	END
	  
	IF (OBJECT_ID('AtivoBolsa_SubscricaoBolsa_FK2', 'F') IS NULL)
	BEGIN
		ALTER TABLE [dbo].[SubscricaoBolsa] ADD CONSTRAINT [AtivoBolsa_SubscricaoBolsa_FK2] FOREIGN KEY ([CdAtivoBolsaDireito]) REFERENCES [dbo].[AtivoBolsa]([CdAtivoBolsa]);
	END
	  
	IF (OBJECT_ID('AtivoBolsa_TabelaCONR_FK1', 'F') IS NULL)
	BEGIN
		ALTER TABLE [dbo].[TabelaCONR] ADD CONSTRAINT [AtivoBolsa_TabelaCONR_FK1] FOREIGN KEY ([CdAtivoBolsa]) REFERENCES [dbo].[AtivoBolsa]([CdAtivoBolsa]);
	END
	  
	IF (OBJECT_ID('FK_EvolucaoCapitalSocial_AtivoBolsa', 'F') IS NULL)
	BEGIN
		ALTER TABLE [dbo].[EvolucaoCapitalSocial] ADD CONSTRAINT [FK_EvolucaoCapitalSocial_AtivoBolsa] FOREIGN KEY ([CdAtivoBolsa]) REFERENCES [dbo].[AtivoBolsa]([CdAtivoBolsa]);
	END
	
	
	
	
	--Tabelas versão 1.1
	IF EXISTS(SELECT 1 FROM SYSCOLUMNS WHERE ID= OBJECT_ID('OperacaoSwap') AND NAME = 'CdAtivoBolsa')
	BEGIN

		ALTER TABLE OperacaoSwap ALTER COLUMN CdAtivoBolsa varchar(25) null ;
		alter table OperacaoSwap add constraint OperacaoSwap_AtivoBolsa_FK FOREIGN KEY ( CdAtivoBolsa ) references AtivoBolsa(CdAtivoBolsa);
	END

	IF EXISTS(SELECT 1 FROM SYSCOLUMNS WHERE ID= OBJECT_ID('OperacaoSwap') AND NAME = 'CdAtivoBolsaContraParte')
	BEGIN



		ALTER TABLE OperacaoSwap ALTER COLUMN CdAtivoBolsaContraParte varchar(25) null ;
		alter table OperacaoSwap add constraint OperacaoSwap_AtivoBolsaCP_FK FOREIGN KEY ( CdAtivoBolsaContraParte ) references AtivoBolsa(CdAtivoBolsa)
	END

	IF EXISTS(SELECT 1 FROM SYSCOLUMNS WHERE ID= OBJECT_ID('PosicaoSwap') AND NAME = 'CdAtivoBolsa')
	BEGIN



		ALTER TABLE PosicaoSwap ALTER COLUMN CdAtivoBolsa varchar(25) null ;
		alter table PosicaoSwap add constraint PosicaoSwap_AtivoBolsa_FK FOREIGN KEY ( CdAtivoBolsa ) references AtivoBolsa(CdAtivoBolsa)
	END

	IF EXISTS(SELECT 1 FROM SYSCOLUMNS WHERE ID= OBJECT_ID('PosicaoSwap') AND NAME = 'CdAtivoBolsaContraParte')
	BEGIN



		ALTER TABLE PosicaoSwap ALTER COLUMN CdAtivoBolsaContraParte varchar(25) null ;
		alter table PosicaoSwap add constraint PosicaoSwap_AtivoBolsaCP_FK FOREIGN KEY ( CdAtivoBolsaContraParte ) references AtivoBolsa(CdAtivoBolsa)
	END

	IF EXISTS(SELECT 1 FROM SYSCOLUMNS WHERE ID= OBJECT_ID('PosicaoSwapAbertura') AND NAME = 'CdAtivoBolsa')
	BEGIN



		ALTER TABLE PosicaoSwapAbertura ALTER COLUMN CdAtivoBolsa varchar(25) null ;
		alter table PosicaoSwapAbertura add constraint PosicaoSwapAbertura_AtivoBolsa_FK FOREIGN KEY ( CdAtivoBolsa ) references AtivoBolsa(CdAtivoBolsa)
	END

	IF EXISTS(SELECT 1 FROM SYSCOLUMNS WHERE ID= OBJECT_ID('PosicaoSwapAbertura') AND NAME = 'CdAtivoBolsaContraParte')
	BEGIN


		ALTER TABLE PosicaoSwapAbertura ALTER COLUMN CdAtivoBolsaContraParte varchar(25) null ;
		alter table PosicaoSwapAbertura add constraint PosicaoSwapAber_AtivoBolsaCP_FK FOREIGN KEY ( CdAtivoBolsaContraParte ) references AtivoBolsa(CdAtivoBolsa)
	END

	IF EXISTS(SELECT 1 FROM SYSCOLUMNS WHERE ID= OBJECT_ID('PosicaoSwapHistorico') AND NAME = 'CdAtivoBolsa')
	BEGIN

		ALTER TABLE PosicaoSwapHistorico ALTER COLUMN CdAtivoBolsa varchar(25) null ;
		alter table PosicaoSwapHistorico add constraint PosicaoSwapHist_AtivoBolsa_FK FOREIGN KEY ( CdAtivoBolsa ) references AtivoBolsa(CdAtivoBolsa)
	END

	IF EXISTS(SELECT 1 FROM SYSCOLUMNS WHERE ID= OBJECT_ID('PosicaoSwapHistorico') AND NAME = 'CdAtivoBolsaContraParte')
	BEGIN


		ALTER TABLE PosicaoSwapHistorico ALTER COLUMN CdAtivoBolsaContraParte varchar(25) null ;
		alter table PosicaoSwapHistorico add constraint PosicaoSwapHist_AtivoBolsaCP_FK FOREIGN KEY ( CdAtivoBolsaContraParte ) references AtivoBolsa(CdAtivoBolsa)
	END

	IF EXISTS(SELECT 1 FROM SYSCOLUMNS WHERE ID= OBJECT_ID('Rentabilidade') AND NAME = 'CdAtivoBolsa')
	BEGIN
		ALTER TABLE Rentabilidade ALTER COLUMN CdAtivoBolsa varchar(25) null ;
	END

	IF EXISTS(SELECT 1 FROM SYSCOLUMNS WHERE ID= OBJECT_ID('IncorporacaoCisaoFusaoBolsaDetalhe') AND NAME = 'CdAtivoBolsa')
	BEGIN

		ALTER TABLE IncorporacaoCisaoFusaoBolsaDetalhe ALTER COLUMN CdAtivoBolsa varchar(25) NOT NULL ;
		alter table IncorporacaoCisaoFusaoBolsaDetalhe add constraint IncorporacaoCisaoFusaoBolsaDetalhe_PK PRIMARY KEY CLUSTERED  ( IdIncorporacaoCisaoFusaoBolsa ASC, CdAtivoBolsa ASC ) 
	END

	IF EXISTS(SELECT 1 FROM SYSCOLUMNS WHERE ID= OBJECT_ID('EventoFundoBolsa') AND NAME = 'CdAtivoBolsa')
	BEGIN

		ALTER TABLE EventoFundoBolsa ALTER COLUMN CdAtivoBolsa varchar(25) NOT NULL ;
		alter table EventoFundoBolsa add constraint EventoFundoBolsa_PK PRIMARY KEY CLUSTERED  ( IdEventoFundo ASC, TipoMercado Asc, CdAtivoBolsa ASC ) 
	END

	IF EXISTS(SELECT 1 FROM SYSCOLUMNS WHERE ID= OBJECT_ID('ExcecoesTributacaoIR') AND NAME = 'CdAtivoBolsa')
	BEGIN
		ALTER TABLE ExcecoesTributacaoIR ALTER COLUMN CdAtivoBolsa varchar(25) null ;
	END


	IF not EXISTS (select 1 from sysconstraints where object_name(id) = 'IncorporacaoCisaoFusaoBolsa' and object_name(constid) = 'IncorporacaoCisaoFusaoBolsa_AtivoBolsa_FK1')
	BEGIN
		ALTER TABLE IncorporacaoCisaoFusaoBolsa ALTER COLUMN CdAtivoBolsa varchar(25) null ;

		ALTER TABLE IncorporacaoCisaoFusaoBolsa  WITH CHECK ADD  CONSTRAINT IncorporacaoCisaoFusaoBolsa_AtivoBolsa_FK1 FOREIGN KEY(CdAtivoBolsa)
		REFERENCES AtivoBolsa (CdAtivoBolsa)
	END
		
	IF not EXISTS (select 1 from sysconstraints where object_name(id) = 'IncorporacaoCisaoFusaoBolsaDetalhe' and object_name(constid) = 'IncorporacaoCisaoFusaoBolsaDetalhe_AtivoBolsa_FK1')
	BEGIN
		
		ALTER TABLE IncorporacaoCisaoFusaoBolsaDetalhe  WITH CHECK ADD  CONSTRAINT IncorporacaoCisaoFusaoBolsaDetalhe_AtivoBolsa_FK1 FOREIGN KEY(CdAtivoBolsa)
		REFERENCES AtivoBolsa (CdAtivoBolsa)
	END
	
	IF not NOT EXISTS (select 1 from sysconstraints where object_name(id) = 'ExcecoesTributacaoIR' and object_name(constid) = 'ExcecoesTributacaoIR_AtivoBolsa_FK') 
	BEGIN  


	 ALTER TABLE ExcecoesTributacaoIR ALTER COLUMN CdAtivoBolsa VARCHAR(25) NULL;

	 ALTER TABLE ExcecoesTributacaoIR ADD CONSTRAINT ExcecoesTributacaoIR_AtivoBolsa_FK FOREIGN KEY(CdAtivoBolsa)  REFERENCES AtivoBolsa (CdAtivoBolsa)  
	END
	
end

----------------------------------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------------------------------






IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'Feeder') > 0
Begin
	IF NOT EXISTS (select 1 from sysconstraints where object_name(id) = 'SerieRendaFixa' and object_name(constid) = 'SerieRF_Feeder_FK')
	BEGIN
		DROP TABLE Feeder;
	END
end
GO	

IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'Feeder') = 0
Begin
	CREATE TABLE Feeder 
	( 	IdFeeder INT PRIMARY KEY NOT NULL IDENTITY , 
		Descricao VARCHAR(50) NOT NULL, 
		Url VARCHAR(255) NULL,
		LocalJarFile VARCHAR(255) NULL,
		LocalDados VARCHAR(255) NULL
	) 
	
	INSERT INTO Feeder (descricao,url) VALUES ('BVMF'	,'http://www.bmfbovespa.com.br/shared/iframeBoletim.aspx?altura=3800&idioma=pt-br&url=www2.bmf.com.br/pages/portal/bmfbovespa/boletim1/TxRef1.asp');
	INSERT INTO Feeder (descricao) VALUES ('ANBIMA');
	INSERT INTO Feeder (descricao) VALUES ('SND'	);
	INSERT INTO Feeder (descricao) VALUES ('CETIP'	);
	INSERT INTO Feeder (descricao) VALUES ('Area Interna'	);
	INSERT INTO Feeder (descricao) VALUES ('BACEN'	);
end
GO

IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'CurvaRendaFixa') = 0
Begin
	CREATE TABLE CurvaRendaFixa 
	( 	IdCurvaRendaFixa 					INT PRIMARY KEY NOT NULL, 
		Descricao 							VARCHAR(100) NOT NULL, 
		CriterioInterpolacao 				INT NOT NULL, 
		ExpressaoTaxaZero 					INT NOT NULL, 	
		PermiteInterpolacao			 		INT NOT NULL, 
		PermiteExtrapolacao		 			INT NOT NULL,
		TipoCurva				 			INT NOT NULL,
		IdCurvaBase  						INT NULL,
		IdCurvaSpread  						INT NULL,
		TipoComposicao						INT NULL,
		IdIndice							smallint NULL
	) 	
end
GO

IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'PerfilMTM') = 0
Begin
	CREATE TABLE PerfilMTM 
	( 	IdPerfilMTM 					INT 	PRIMARY KEY identity NOT NULL, 
		DtVigencia						DateTime			NOT NULL, 
		IdPapel							INT 				NULL, 
		IdSerie							INT 				NULL, 
		IdTitulo 						INT 				NULL, 	
		IdOperacao 						INT 				NULL
	) 	
end
go

IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'MTMManual') = 0
Begin
	CREATE TABLE MTMManual 
	( 	
		IdOperacao						INT 				NOT NULL,
		DtVigencia						DateTime 			NOT NULL, 
		Taxa252 						Decimal(25,12)		NULL, 
		PuMTM							Decimal(25,12)		NULL	
		CONSTRAINT PK_MTMManual 		primary key(DtVigencia, IdOperacao)	
	) 
end	
GO

IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'TaxaSerie') = 0
Begin
	CREATE TABLE TaxaSerie 
	( 	
		IdSerie							INT 				NOT NULL,
		Data							DateTime 			NOT NULL, 
		Taxa	 						Decimal(25,12)		NOT NULL,  
		DigitadoImportado 				INT					NOT NULL	
		CONSTRAINT TaxaSerie_PK 		primary key(Data, IdSerie)	
	) 
end 
GO

IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'VigenciaCategoria') = 0
Begin
	CREATE TABLE VigenciaCategoria 
	( 	
		IdVigenciaCategoria				 INT 	PRIMARY KEY identity NOT NULL, 
		DataVigencia					 DateTime 			NOT NULL, 
		IdOperacao						 INT 				NOT NULL, 
		TipoVigenciaCategoria 			 INT				NOT NULL	
	) 
end	
GO

IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'TaxaCurva') = 0
Begin
	CREATE TABLE TaxaCurva 
	( 	
		IdCurvaRendaFixa				INT 				NOT NULL,
		"DataBase"						DateTime 			NOT NULL, 
		DataVertice						DateTime 			NOT NULL, 
		CodigoVertice					VARCHAR(100) 		NULL, 
		PrazoDU							INT 				NOT NULL, 
		PrazoDC							INT 				NOT NULL, 
		Taxa	 						Decimal(25,12)		NOT NULL	
		CONSTRAINT PK_TaxaCurva 		primary key("DataBase", IdCurvaRendaFixa,DataVertice)	
	) 		
end	
GO

IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'MemoriaCalculoRendaFixa') = 0
Begin
	CREATE TABLE MemoriaCalculoRendaFixa
	(
		DataAtual 		 		 DateTime		NOT NULL,
		IdOperacao		 		 INT 			NOT NULL,
		TipoPreco 		 		 INT 			NOT NULL,
		TipoProcessamento		 INT 			NOT NULL,
		DataFluxo		 		 DateTime 		NOT NULL,
		TaxaDesconto	 		 Decimal(25,12)	NULL,
		ValorVencimento	 		 Decimal(25,8)	NULL,
		ValorPresente	 		 Decimal(25,8)	NULL,		
		ValorNominalAjustado	 Decimal(25,8)	NULL,
		ValorNominal			 Decimal(25,8)	NULL
		CONSTRAINT PK_MemoriaCalculoRendaFixa 		primary key(DataAtual, IdOperacao, TipoPreco, TipoProcessamento, DataFluxo)	
	)
end	
GO

IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'LocalCustodia') = 0
Begin
	CREATE TABLE [dbo].[LocalCustodia]
	(
		[IdLocalCustodia] [int] PRIMARY KEY  IDENTITY(1,1) NOT NULL,
		[Descricao] [varchar](60) NOT NULL,
		[Codigo] [varchar](250) NOT NULL,
		[Cnpj] [varchar](250) NOT NULL,
	)
	
	INSERT INTO LocalCustodia VALUES ('SELIC', 'SELIC', '00038166000105');
	INSERT INTO LocalCustodia VALUES ('CETIP', 'CETIP', '09358105000191');
	INSERT INTO LocalCustodia VALUES ('CBLC', 'CBLC', '60777661000150');
end
GO

IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'TipoDePara') = 0
begin
	CREATE TABLE TipoDePara 
	( 	IdTipoDePara 					INT 				PRIMARY KEY identity NOT NULL, 
		TipoCampo						INT 				NOT NULL, 
		Descricao 						VARCHAR(100) 		NOT NULL, 
		Observacao 						VARCHAR(255) 		NULL,
		EnumTipoDePara					INT 				NULL	
	) 	

end
GO

IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'DePara') = 0
begin
	CREATE TABLE DePara 
	( 	IdDePara	 					INT 				PRIMARY KEY identity NOT NULL, 
		IdTipoDePara 					INT 				NOT NULL, 
		CodigoInterno	 				VARCHAR(100) 		NOT NULL, 
		CodigoExterno					VARCHAR(100) 		NOT NULL	
	) 	

end
GO

IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'MemoriaCalculoRendaFixa') = 0
begin
	CREATE TABLE MemoriaCalculoRendaFixa
	(
		DataAtual 		 		 DateTime		NOT NULL,
		IdOperacao		 		 INT 			NOT NULL,
		TipoPreco 		 		 INT 			NOT NULL,
		TipoProcessamento		 INT 			NOT NULL,
		DataFluxo		 		 DateTime 		NOT NULL,
		TaxaDesconto	 		 Decimal(25,12)	NULL,
		ValorVencimento	 		 Decimal(25,8)	NULL,
		ValorPresente	 		 Decimal(25,8)	NULL,		
		ValorNominalAjustado	 Decimal(25,8)	NULL,
		ValorNominal			 Decimal(25,8)	NULL
		CONSTRAINT PK_MemoriaCalculoRendaFixa 		primary key(DataAtual, IdOperacao, TipoPreco, TipoProcessamento, DataFluxo)	
	)	
end
GO

IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'PrazoMedio') = 0
begin
	CREATE TABLE PrazoMedio
	(
		IdPrazoMedio			 INT 			NOT NULL PRIMARY KEY identity,
		DataAtual 		 		 DateTime		NOT NULL,
		IdCliente		 		 INT 			NOT NULL,
		Descricao 		 		 VARCHAR(255) 	NOT NULL,
		PrazoMedioAtivo		 	 INT 			NOT NULL,
		PrazoMedio		 		 Decimal(10,0)	NOT NULL,
		ValorPosicao	 		 Decimal(25,2)	NOT NULL
	)
END	
GO

IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'RepactuacaoRendaFixa') = 0
Begin
	CREATE TABLE RepactuacaoRendaFixa
	(
		IdRepactuacao			 INT 			NOT NULL PRIMARY KEY identity,
		IdTituloOriginal		 INT 			NOT NULL,
		IdTituloNovo		     INT 			NOT NULL,
		DataEvento			 	 DateTime 		NOT NULL
	)

END	
GO

IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'TipoParametroLiqAvancado') = 0
Begin
	Create Table TipoParametroLiqAvancado
	(
			IdParametroLiqAvancado 			VARCHAR(10) 	PRIMARY KEY NOT NULL, 
			SignificadoParametro			VARCHAR(50)		NOT NULL,
			VariacaoComplemento				VARCHAR(50)		NOT NULL,
			SignificadoComplemento			VARCHAR(255)	NOT NULL,
			Exemplo							VARCHAR(255)	NOT NULL
	)

	INSERT INTO TipoParametroLiqAvancado VALUES ('D','Dias úteis','+ ou - N','Quantidade de dias úteis','D+3 Representa 3 dias úteis')
	INSERT INTO TipoParametroLiqAvancado VALUES ('C','Dias Corridos','+ ou - N','Quantidade de dias corridos','D+3 Representa 3 dias corridos')
	INSERT INTO TipoParametroLiqAvancado VALUES ('W','Ocorrência periódica em contagem semanal','N/M','N indica o início da ocorrência, onde 0 (zero) representaria o período vigente, 1 representaria o próximo período, e assim por diante. M indica a quantidade de dias dentro do período.','W0/4 Representa o quarto dia útil da semana vigente')
	INSERT INTO TipoParametroLiqAvancado VALUES ('F','Ocorrência periódica em contagem quinzenal','N/M','N indica o início da ocorrência, onde 0 (zero) representaria o período vigente, 1 representaria o próximo período, e assim por diante. M indica a quantidade de dias dentro do período.','F1/5 Representa o quinto dia útil da quinzena seguinte')
	INSERT INTO TipoParametroLiqAvancado VALUES ('M','Ocorrência periódica em contagem mensal','N/M','N indica o início da ocorrência, onde 0 (zero) representaria o período vigente, 1 representaria o próximo período, e assim por diante. M indica a quantidade de dias dentro do período.','M0/31 Representa o último dia útil do mês vigente (31 representa o último dia útil, por convenção)')
	INSERT INTO TipoParametroLiqAvancado VALUES ('T','Ocorrência periódica em contagem trimestral','N/M','N indica o início da ocorrência, onde 0 (zero) representaria o período vigente, 1 representaria o próximo período, e assim por diante. M indica a quantidade de dias dentro do período.','T0/31 Representa o último dia útil do trimestre vigente (31 representa o último dia útil, por convenção)')
	INSERT INTO TipoParametroLiqAvancado VALUES ('S','Ocorrência periódica em contagem semestral','N/M','N indica o início da ocorrência, onde 0 (zero) representaria o período vigente, 1 representaria o próximo período, e assim por diante. M indica a quantidade de dias dentro do período.','S0/10 Representa o décimo dia útil do semestre vigente ')	
END	
GO

IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'ParametroLiqAvancadoAnalitico') = 0
Begin
	Create Table ParametroLiqAvancadoAnalitico
	(
			IdParametroLiqAvancado 			VARCHAR(10) 	NOT NULL, 
			IdCarteira						INT				NOT NULL,
			DataVigencia 					datetime		NOT NULL,
			TipoData						int				NOT NULL,
			TipoOperacao					int				NOT NULL,
			ParametroUm						VARCHAR(50)		NULL,
			ParametroDois					VARCHAR(50)		NULL,
			ParametroComposto				VARCHAR(50)		NULL,
			OrdemProcessamento 				int				NOT NULL
			CONSTRAINT PK_ParametroLiqAvancadoAnalico	primary key(IdParametroLiqAvancado, IdCarteira, DataVigencia, TipoData, TipoOperacao)	
	)	
END	
GO
 
IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'ParametroLiqAvancado') = 0
Begin
	Create Table ParametroLiqAvancado
	(
			IdCarteira						INT				NOT NULL,
			DataVigencia 					datetime		NOT NULL,
			TipoData						int				NOT NULL,
			TipoOperacao					int				NOT NULL,			
			Parametro						VARCHAR(150)	NOT NULL,
			ParametroValido					char(1) 		NOT NULL DEFAULT 'N'			
			CONSTRAINT PK_ParametroLiqAvancado	primary key(IdCarteira, DataVigencia, TipoData, TipoOperacao)	
	)	
END	
GO

IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'DetalhePosicaoAfetadaRF') = 0
Begin
	CREATE TABLE [dbo].DetalhePosicaoAfetadaRF
	(
		[DataOperacao] [datetime] NOT NULL,
		[IdOperacao] [int] NOT NULL,
		[IdPosicaoAfetada] [int] NOT NULL,		
		[IdCliente] [int] NOT NULL,
		[QtdeMovimentada] [decimal](28, 12) NOT NULL,
		[PuOperacao] [decimal](28, 12) NOT NULL		
	 )
	 alter table DetalhePosicaoAfetadaRF add primary key(DataOperacao, IdOperacao, IdPosicaoAfetada)	
	 alter table DetalhePosicaoAfetadaRF add constraint DetalhePosAfetadaRF_OpRF_FK FOREIGN KEY ( IdOperacao ) references OperacaoRendaFixa(IdOperacao)
	 alter table DetalhePosicaoAfetadaRF add constraint DetalhePosAfetadaRF_PosRF_FK FOREIGN KEY ( IdPosicaoAfetada ) references PosicaoRendaFixa(IdPosicao)
 END	
GO

 IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'Rentabilidade') = 0
Begin
	CREATE TABLE Rentabilidade 
	( 	
		IdRentabilidade							INT 				PRIMARY KEY identity NOT NULL, 
		Data	 		 		 				DateTime		    NOT NULL,						
		TipoAtivo		 	 					INT 				NOT NULL,
		IdCliente								INT					NOT NULL,
		CodigoAtivo 							VARCHAR(255) 		NULL, 
		CodigoMoedaAtivo 						VARCHAR(10) 		NULL,
		PatrimonioInicialMoedaAtivo				Decimal(25,12)		NULL,
		QuantidadeInicialAtivo					Decimal(25,12)		NULL,
		ValorFinanceiroEntradaMoedaAtivo		Decimal(25,2)		NULL,
		QuantidadeTotalEntradaAtivo				Decimal(25,12)		NULL,
		ValorFinanceiroSaidaMoedaAtivo			Decimal(25,2)		NULL,
		QuantidadeTotalSaidaAtivo				Decimal(25,12)		NULL,
		ValorFinanceiroRendasMoedaAtivo			Decimal(25,2)		NULL,	
		PatrimonioFinalMoedaAtivo				Decimal(25,12)		NULL,	
		QuantidadeFinalAtivo					Decimal(25,12)		NULL,
		RentabilidadeMoedaAtivo					Decimal(25,12)		NULL,		
		CodigoMoedaPortfolio					VARCHAR(10) 		NULL,
		PatrimonioInicialMoedaPortfolio			Decimal(25,12)		NULL,
		ValorFinanceiroEntradaMoedaPortfolio	Decimal(25,2)		NULL,
		ValorFinanceiroSaidaMoedaPortfolio      Decimal(25,2)		NULL,
		ValorFinanceiroRendasMoedaPortfolio     Decimal(25,2)		NULL,
		PatrimonioFinalMoedaPortfolio           Decimal(25,2)		NULL,
		RentabilidadeMoedaPortfolio             Decimal(25,12)		NULL,
		IdTituloRendaFixa						INT					NULL,
		IdOperacaoSwap							INT					NULL,
		IdCarteira								INT					NULL,
		CdAtivoBolsa							varchar(25)			NULL,
		CdAtivoBMF								varchar(20)			NULL,
		SerieBMF								varchar(5)			NULL
	) 
END
GO

IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'AtivoMercadoFiltro') = 0
Begin
	Create Table AtivoMercadoFiltro
	(
			IdAtivoMercadoFiltro INT 	PRIMARY KEY identity NOT NULL, 
			CompositeKey	VARCHAR(50),
			IdAtivo			VARCHAR(50),
			Descricao		VARCHAR(255),
			IdCliente		INT,
			TipoMercado		INT
	)
END	
GO

IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'OperacaoCotistaAux') = 0
Begin
	CREATE TABLE [dbo].[OperacaoCotistaAux]
	(
		[IdOperacaoCotistaAux] [int] NOT NULL IDENTITY PRIMARY KEY,
		[IdOperacao] [int] NOT NULL,
		[IdCotista] [int] NOT NULL,
		[IdCarteira] [int] NOT NULL,
		[DataOperacao] [datetime] NOT NULL,
		[DataConversao] [datetime] NOT NULL,
		[DataLiquidacao] [datetime] NOT NULL,
		[DataAgendamento] [datetime] NOT NULL,
		[TipoOperacao] [tinyint] NOT NULL,
		[TipoResgate] [tinyint] NULL,
		[IdPosicaoResgatada] [int] NULL,
		[IdFormaLiquidacao] [tinyint] NOT NULL,
		[Quantidade] [decimal](28, 12) NOT NULL,
		[CotaOperacao] [decimal](28, 12) NOT NULL,
		[ValorBruto] [decimal](16, 2) NOT NULL,
		[ValorLiquido] [decimal](16, 2) NOT NULL,
		[ValorIR] [decimal](16, 2) NOT NULL,
		[ValorIOF] [decimal](16, 2) NOT NULL,
		[ValorCPMF] [decimal](16, 2) NOT NULL,
		[ValorPerformance] [decimal](16, 2) NOT NULL,
		[PrejuizoUsado] [decimal](16, 2) NOT NULL,
		[RendimentoResgate] [decimal](16, 2) NOT NULL,
		[VariacaoResgate] [decimal](16, 2) NOT NULL,
		[Observacao] [varchar](400) NOT NULL,
		[DadosBancarios] [varchar](500) NULL,
		[Fonte] [tinyint] NOT NULL,
		[IdConta] [int] NULL,
		[CotaInformada] [decimal](28, 12) NULL,
		[IdAgenda] [int] NULL,
		[IdOperacaoResgatada] [int] NULL,
		[IdOperacaoAuxiliar] [int] NULL
	)
END
GO

IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'DetalheResgateCotistaAux') = 0
Begin
	CREATE TABLE [dbo].[DetalheResgateCotistaAux]
	(
		[IdOperacao] [int] NOT NULL,
		[IdPosicaoResgatada] [int] NOT NULL,
		[IdCotista] [int] NOT NULL,
		[IdCarteira] [int] NOT NULL,
		[Quantidade] [decimal](28, 12) NOT NULL,
		[ValorBruto] [decimal](16, 2) NOT NULL,
		[ValorLiquido] [decimal](16, 2) NOT NULL,
		[ValorCPMF] [decimal](16, 2) NOT NULL,
		[ValorPerformance] [decimal](16, 2) NOT NULL,
		[PrejuizoUsado] [decimal](16, 2) NOT NULL,
		[RendimentoResgate] [decimal](16, 2) NOT NULL,
		[VariacaoResgate] [decimal](16, 2) NOT NULL,
		[ValorIR] [decimal](16, 2) NOT NULL,
		[ValorIOF] [decimal](16, 2) NOT NULL,
		[DataOperacao] [datetime] NOT NULL,
		[TipoOperacao] [tinyint] NOT NULL,
	 CONSTRAINT [PK_DetalheResgateCotistaAux] PRIMARY KEY CLUSTERED 
	(
		[IdOperacao] ASC,
		[IdPosicaoResgatada] ASC
	)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
	) ON [PRIMARY]
END
GO

IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'ColagemResgate') = 0
Begin
	CREATE TABLE [dbo].[ColagemResgate]
	(
		IdColagemResgate [int] PRIMARY KEY NOT NULL IDENTITY,
		TipoRegistro char(1) NOT NULL ,
		CodigoExterno char(15) NOT NULL ,
		DataReferencia [datetime] NOT NULL,
		DataOperacao [datetime] NOT NULL,
		IdCliente  [int] NOT NULL,
		IdCarteira  [int] NOT NULL,
		DataConversao [datetime] NULL,
		Quantidade [decimal](28, 12) NOT NULL,
		CotaOperacao [decimal](28, 12) NOT NULL,
		ValorBruto [decimal](28, 12) NOT NULL,
		ValorLiquido [decimal](28, 12) NOT NULL,
		ValorCPMF [decimal](28, 12) NOT NULL,
		ValorPerformance [decimal](28, 12) NOT NULL,
		PrejuizoUsado [decimal](28, 12) NOT NULL,
		RendimentoResgate [decimal](28, 12) NOT NULL,
		VariacaoResgate [decimal](28, 12) NOT NULL,
		ValorIR [decimal](28, 12) NOT NULL,
		ValorIOF [decimal](28, 12) NOT NULL
	)	
END	
GO

IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'ColagemResgateDetalhe') = 0
Begin
	CREATE TABLE [dbo].[ColagemResgateDetalhe]
	(
		IdColagemResgateDetalhe  [int] PRIMARY KEY NOT NULL IDENTITY,
		CodigoExterno char(15) NOT NULL ,
		DataReferencia [datetime] NOT NULL,
		DataOperacao [datetime] NOT NULL,
		IdCliente  [int] NOT NULL,
		IdCarteira  [int] NOT NULL,
		DataConversao [datetime] NULL,
		Quantidade [decimal](28, 12) NOT NULL,
		CotaOperacao [decimal](28, 12) NOT NULL,
		ValorBruto [decimal](28, 12) NOT NULL,
		ValorLiquido [decimal](28, 12) NOT NULL,
		ValorCPMF [decimal](28, 12) NOT NULL,
		ValorPerformance [decimal](28, 12) NOT NULL,
		PrejuizoUsado [decimal](28, 12) NOT NULL,
		RendimentoResgate [decimal](28, 12) NOT NULL,
		VariacaoResgate [decimal](28, 12) NOT NULL,
		ValorIR [decimal](28, 12) NOT NULL,
		ValorIOF [decimal](28, 12) NOT NULL,
	)	
END	
GO

 IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'ColagemResgateDetalhePosicao') = 0
Begin
	CREATE TABLE [dbo].[ColagemResgateDetalhePosicao]
	(
		IdColagemResgateDetalhePosicao [int] PRIMARY KEY NOT NULL IDENTITY,
		IdColagemResgateDetalhe [int] NOT NULL,
		DataReferencia [datetime] NOT NULL,
		QuantidadeInicial [decimal](28, 12) NOT NULL,
		QuantidadeRestante [decimal](28, 12) NOT NULL
	)		
END	
GO

IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'ColagemResgateMapaCotista') = 0
Begin
	CREATE TABLE [dbo].[ColagemResgateMapaCotista]
	(
		IdColagemResgateMapaCotista  [int] PRIMARY KEY NOT NULL IDENTITY,
		DataReferencia [datetime] NOT NULL,
		IdCliente  [int] NOT NULL,
		IdCarteira  [int] NOT NULL,
		DataConversao [datetime] NULL,
		DetalheImportado char(1) NOT NULL DEFAULT 'N',
		IdOperacaoResgateVinculada  [int] NOT NULL,	
		Participacao [decimal](28, 12) NOT NULL
	)		
END	
GO

IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'ColagemOperResgPosCotista') = 0
Begin
	CREATE TABLE [dbo].[ColagemOperResgPosCotista]
	(
		IdColagemOperResgPosCotista  [int] PRIMARY KEY NOT NULL IDENTITY,
		IdCarteira  [int] NOT NULL,
		IdCliente  [int] NOT NULL,
		DataReferencia [datetime] NOT NULL,
		IdColagemResgateDetalhe  [int] NOT NULL,
		IdOperacaoResgateVinculada  [int] NOT NULL,
		IdPosicaoVinculada  [int] NOT NULL,
		QuantidadeVinculada [decimal](28, 12) NOT NULL
	)		
END	
GO

IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'DetalheResgateFundoAux') = 0
Begin
	CREATE TABLE [dbo].[DetalheResgateFundoAux]
	(
		[IdOperacao] [int] NOT NULL,
		[IdPosicaoResgatada] [int] NOT NULL,
		[IdCliente] [int] NOT NULL,
		[IdCarteira] [int] NOT NULL,
		[Quantidade] [decimal](28, 12) NOT NULL,
		[ValorBruto] [decimal](16, 2) NOT NULL,
		[ValorLiquido] [decimal](16, 2) NOT NULL,
		[ValorCPMF] [decimal](16, 2) NOT NULL,
		[ValorPerformance] [decimal](16, 2) NOT NULL,
		[PrejuizoUsado] [decimal](16, 2) NOT NULL,
		[RendimentoResgate] [decimal](16, 2) NOT NULL,
		[VariacaoResgate] [decimal](16, 2) NOT NULL,
		[ValorIR] [decimal](16, 2) NOT NULL,
		[ValorIOF] [decimal](16, 2) NOT NULL,		
		[DataOperacao] [datetime] NOT NULL,
		[TipoOperacao] [tinyint] NOT NULL,
	 CONSTRAINT [PK_DetalheResgateFundoAux] PRIMARY KEY CLUSTERED 
	(
		[IdOperacao] ASC,
		[IdPosicaoResgatada] ASC
	)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
	) ON [PRIMARY]
END
GO

IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'OperacaoFundoAux') = 0
Begin
	CREATE TABLE [dbo].[OperacaoFundoAux]
	(
		[IdOperacaoFundoAux] [int] NOT NULL IDENTITY PRIMARY KEY,
		[IdOperacao] [int] NOT NULL,
		[IdCliente] [int] NOT NULL,
		[IdCarteira] [int] NOT NULL,
		[DataOperacao] [datetime] NOT NULL,
		[DataConversao] [datetime] NOT NULL,
		[DataLiquidacao] [datetime] NOT NULL,
		[DataAgendamento] [datetime] NOT NULL,
		[TipoOperacao] [tinyint] NOT NULL,
		[TipoResgate] [tinyint] NULL,
		[IdPosicaoResgatada] [int] NULL,
		[IdFormaLiquidacao] [tinyint] NOT NULL,
		[Quantidade] [decimal](28, 12) NOT NULL,
		[CotaOperacao] [decimal](28, 12) NOT NULL,
		[ValorBruto] [decimal](16, 2) NOT NULL,
		[ValorLiquido] [decimal](16, 2) NOT NULL,
		[ValorIR] [decimal](16, 2) NOT NULL,
		[ValorIOF] [decimal](16, 2) NOT NULL,
		[ValorCPMF] [decimal](16, 2) NOT NULL,
		[ValorPerformance] [decimal](16, 2) NOT NULL,
		[PrejuizoUsado] [decimal](16, 2) NOT NULL,
		[RendimentoResgate] [decimal](16, 2) NOT NULL,
		[VariacaoResgate] [decimal](16, 2) NOT NULL,
		[IdConta] [int] NULL,
		[Observacao] [varchar](400) NOT NULL,
		[Fonte] [tinyint] NOT NULL,
		[IdAgenda] [int] NULL,
		[CotaInformada] [decimal](28, 12) NULL,
		[IdOperacaoResgatada] [int] NULL
	)
END
GO

IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'ColagemResgateMapaFundo') = 0
Begin
	CREATE TABLE [dbo].[ColagemResgateMapaFundo]
	(
		IdColagemResgateMapaFundo  [int] PRIMARY KEY NOT NULL IDENTITY,
		DataReferencia [datetime] NOT NULL,
		IdCliente  [int] NOT NULL,
		IdCarteira  [int] NOT NULL,
		DataConversao [datetime] NULL,
		DetalheImportado char(1) NOT NULL DEFAULT 'N',
		IdOperacaoResgateVinculada  [int] NOT NULL,	
		Participacao [decimal](28, 12) NOT NULL
	)		
END	
GO

IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'ColagemOperResgPosFundo') = 0
Begin
	CREATE TABLE [dbo].[ColagemOperResgPosFundo]
	(
		IdColagemOperResgPosFundo  [int] PRIMARY KEY NOT NULL IDENTITY,
		IdCarteira  [int] NOT NULL,
		IdCliente  [int] NOT NULL,
		DataReferencia [datetime] NOT NULL,
		IdColagemResgateDetalhe  [int] NOT NULL,
		IdOperacaoResgateVinculada  [int] NOT NULL,
		IdPosicaoVinculada  [int] NOT NULL,
		QuantidadeVinculada [decimal](28, 12) NOT NULL
	)		
END	
GO

IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'ColagemComeCotas') = 0
Begin
	CREATE TABLE [dbo].[ColagemComeCotas]
	(
		IdColagemComeCotas [int] PRIMARY KEY NOT NULL IDENTITY,
		TipoRegistro char(1) NOT NULL ,
		CodigoExterno char(15) NOT NULL ,
		DataReferencia [datetime] NOT NULL,
		DataOperacao [datetime] NOT NULL,
		IdCliente  [int] NOT NULL,
		IdCarteira  [int] NOT NULL,
		DataConversao [datetime] NULL,
		Quantidade [decimal](28, 12) NOT NULL,
		CotaOperacao [decimal](28, 12) NOT NULL,
		ValorBruto [decimal](28, 12) NOT NULL,
		ValorLiquido [decimal](28, 12) NOT NULL,
		ValorCPMF [decimal](28, 12) NOT NULL,
		ValorPerformance [decimal](28, 12) NOT NULL,
		PrejuizoUsado [decimal](28, 12) NOT NULL,
		RendimentoComeCotas [decimal](28, 12) NOT NULL,
		VariacaoComeCotas [decimal](28, 12) NOT NULL,
		ValorIR [decimal](28, 12) NOT NULL,
		ValorIOF [decimal](28, 12) NOT NULL
	)	
END	
GO

IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'ColagemComeCotasDetalhe') = 0
Begin
	CREATE TABLE [dbo].[ColagemComeCotasDetalhe]
	(
		IdColagemComeCotasDetalhe  [int] PRIMARY KEY NOT NULL IDENTITY,
		CodigoExterno char(15) NOT NULL ,
		DataReferencia [datetime] NOT NULL,
		DataOperacao [datetime] NOT NULL,
		IdCliente  [int] NOT NULL,
		IdCarteira  [int] NOT NULL,
		DataConversao [datetime] NULL,
		Quantidade [decimal](28, 12) NOT NULL,
		CotaOperacao [decimal](28, 12) NOT NULL,
		ValorBruto [decimal](28, 12) NOT NULL,
		ValorLiquido [decimal](28, 12) NOT NULL,
		ValorCPMF [decimal](28, 12) NOT NULL,
		ValorPerformance [decimal](28, 12) NOT NULL,
		PrejuizoUsado [decimal](28, 12) NOT NULL,
		RendimentoComeCotas [decimal](28, 12) NOT NULL,
		VariacaoComeCotas [decimal](28, 12) NOT NULL,
		ValorIR [decimal](28, 12) NOT NULL,
		ValorIOF [decimal](28, 12) NOT NULL,
	)	
END	
GO

IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'ColagemComeCotasDetalhePosicao') = 0
Begin
	CREATE TABLE [dbo].[ColagemComeCotasDetalhePosicao]
	(
		IdColagemComeCotasDetalhePosicao [int] PRIMARY KEY NOT NULL IDENTITY,
		IdColagemComeCotasDetalhe [int] NOT NULL,
		DataReferencia [datetime] NOT NULL,
		QuantidadeInicial [decimal](28, 12) NOT NULL,
		QuantidadeRestante [decimal](28, 12) NOT NULL
	)		
END	
GO

IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'ColagemComeCotasMapaFundo') = 0
Begin
	CREATE TABLE [dbo].[ColagemComeCotasMapaFundo]
	(
		IdColagemComeCotasMapaFundo  [int] PRIMARY KEY NOT NULL IDENTITY,
		DataReferencia [datetime] NOT NULL,
		IdCliente  [int] NOT NULL,
		IdCarteira  [int] NOT NULL,
		DataConversao [datetime] NULL,
		DetalheImportado char(1) NOT NULL DEFAULT 'N',
		IdOperacaoComeCotasVinculada  [int] NOT NULL,	
		Participacao [decimal](28, 12) NOT NULL
	)		
END	
GO

IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'ColagemComeCotasPosFundo') = 0
Begin
	CREATE TABLE [dbo].[ColagemComeCotasPosFundo]
	(
		IdColagemComeCotasPosFundo  [int] PRIMARY KEY NOT NULL IDENTITY,
		IdCarteira  				[int] NOT NULL,
		IdCliente  					[int] NOT NULL,
		DataConversao 				[datetime] NULL,
		DataReferencia 				[datetime] NOT NULL,
		IdPosicaoVinculada  		[int] NOT NULL,
		PrejuizoUsado 				[decimal](28, 12) NOT NULL,
		RendimentoComeCotas 		[decimal](28, 12) NOT NULL,
		ValorBruto 					[decimal](28, 12) NOT NULL,
		ValorCPMF 					[decimal](28, 12) NOT NULL,
		ValorIOF 					[decimal](28, 12) NOT NULL,
		ValorIR 					[decimal](28, 12) NOT NULL,
		ValorLiquido 				[decimal](28, 12) NOT NULL,
		ValorPerformance 			[decimal](28, 12) NOT NULL,
		VariacaoComeCotas 			[decimal](28, 12) NOT NULL,
		Quantidade					[decimal](28, 12) NOT NULL,
		Participacao				[decimal](28, 12) NOT NULL,
		DetalheImportado 			[char](1) NOT NULL DEFAULT 'N'
	)		
END	
GO

IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'PosicaoFundoAux') = 0
Begin
	CREATE TABLE [dbo].[PosicaoFundoAux]
	(
		[IdPosicaoFundoAux] [int] NOT NULL IDENTITY PRIMARY KEY,
		[DataReferencia] [datetime] NOT NULL,
		[IdPosicao] [int] NOT NULL,
		[IdOperacao] [int] NULL,
		[IdCliente] [int] NOT NULL,
		[IdCarteira] [int] NOT NULL,
		[ValorAplicacao] [decimal](16, 2) NOT NULL,
		[DataAplicacao] [datetime] NOT NULL,
		[DataConversao] [datetime] NOT NULL,
		[CotaAplicacao] [decimal](28, 12) NOT NULL,
		[CotaDia] [decimal](28, 12) NOT NULL,
		[ValorBruto] [decimal](16, 2) NOT NULL,
		[ValorLiquido] [decimal](16, 2) NOT NULL,
		[QuantidadeInicial] [decimal](28, 12) NOT NULL,
		[Quantidade] [decimal](28, 12) NOT NULL,
		[QuantidadeBloqueada] [decimal](28, 12) NULL,
		[DataUltimaCobrancaIR] [datetime] NOT NULL,
		[ValorIR] [decimal](16, 2) NOT NULL,
		[ValorIOF] [decimal](16, 2) NOT NULL,
		[ValorPerformance] [decimal](16, 2) NOT NULL,
		[ValorIOFVirtual] [decimal](16, 2) NOT NULL,
		[QuantidadeAntesCortes] [decimal](28, 12) NOT NULL,
		[ValorRendimento] [decimal](16, 2) NOT NULL,
		[DataUltimoCortePfee] [datetime] NULL,
		[PosicaoIncorporada] [char](1) NOT NULL,
	)
END	
GO

IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'ColagemComeCotasPosCotista') = 0
Begin
	CREATE TABLE [dbo].[ColagemComeCotasPosCotista]
	(
		IdColagemComeCotasPosCotista  [int] PRIMARY KEY NOT NULL IDENTITY,
		IdCarteira  				[int] NOT NULL,
		IdCliente  					[int] NOT NULL,
		DataConversao 				[datetime] NULL,
		DataReferencia 				[datetime] NOT NULL,
		IdPosicaoVinculada  		[int] NOT NULL,
		PrejuizoUsado 				[decimal](28, 12) NOT NULL,
		RendimentoComeCotas 		[decimal](28, 12) NOT NULL,
		ValorBruto 					[decimal](28, 12) NOT NULL,
		ValorCPMF 					[decimal](28, 12) NOT NULL,
		ValorIOF 					[decimal](28, 12) NOT NULL,
		ValorIR 					[decimal](28, 12) NOT NULL,
		ValorLiquido 				[decimal](28, 12) NOT NULL,
		ValorPerformance 			[decimal](28, 12) NOT NULL,
		VariacaoComeCotas 			[decimal](28, 12) NOT NULL,
		Quantidade					[decimal](28, 12) NOT NULL,
		Participacao				[decimal](28, 12) NOT NULL,
		DetalheImportado 			[char](1) NOT NULL DEFAULT 'N'
	)		
END	
 GO

IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'PosicaoCotistaAux') = 0
Begin
	CREATE TABLE [dbo].[PosicaoCotistaAux]
	(
		[IdPosicaoCotistaAux] [int] NOT NULL IDENTITY PRIMARY KEY,
		[DataReferencia] [datetime] NOT NULL,
		[IdPosicao] [int] NOT NULL,
		[IdOperacao] [int] NULL,
		[IdCotista] [int] NOT NULL,
		[IdCarteira] [int] NOT NULL,
		[ValorAplicacao] [decimal](16, 2) NOT NULL,
		[DataAplicacao] [datetime] NOT NULL,
		[DataConversao] [datetime] NOT NULL,
		[CotaAplicacao] [decimal](28, 12) NOT NULL,
		[CotaDia] [decimal](28, 12) NOT NULL,
		[ValorBruto] [decimal](16, 2) NOT NULL,
		[ValorLiquido] [decimal](16, 2) NOT NULL,
		[QuantidadeInicial] [decimal](28, 12) NOT NULL,
		[Quantidade] [decimal](28, 12) NOT NULL,
		[QuantidadeBloqueada] [decimal](28, 12) NOT NULL,
		[DataUltimaCobrancaIR] [datetime] NOT NULL,
		[ValorIR] [decimal](16, 2) NOT NULL,
		[ValorIOF] [decimal](16, 2) NOT NULL,
		[ValorPerformance] [decimal](16, 2) NOT NULL,
		[ValorIOFVirtual] [decimal](16, 2) NOT NULL,
		[QuantidadeAntesCortes] [decimal](28, 12) NOT NULL,
		[ValorRendimento] [decimal](16, 2) NOT NULL,
		[DataUltimoCortePfee] [datetime] NULL,
		[PosicaoIncorporada] [char](1) NOT NULL,
	 )
 END	
GO

IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'Clearing') = 0
Begin
	CREATE TABLE [dbo].Clearing
	(
		[IdClearing]				int NOT NULL IDENTITY PRIMARY KEY,
		[Codigo]					VARCHAR(30) NOT NULL, 
		[Descricao]					VARCHAR(100) NOT NULL, 
		[CNPJ]						VARCHAR(30) NOT NULL, 
		[IdFormaLiquidacao]			tinyint NOT NULL,
		[IdLocalFeriado]			smallint NOT NULL,
		[CodigoCETIP]				VARCHAR(30) NULL,
		[CodigoSELIC]				VARCHAR(30) NULL		
	 )
	 
	 INSERT INTO Clearing (Codigo, Descricao, CNPJ, IdFormaLiquidacao, IdLocalFeriado, CodigoCETIP, CodigoSELIC) VALUES('SELIC', 'SELIC', '00038166000105', 2, 1, null, null)
	 INSERT INTO Clearing (Codigo, Descricao, CNPJ, IdFormaLiquidacao, IdLocalFeriado, CodigoCETIP, CodigoSELIC) VALUES('CETIP', 'CETIP', '09358105000191', 2, 1, null, null)
	 INSERT INTO Clearing (Codigo, Descricao, CNPJ, IdFormaLiquidacao, IdLocalFeriado, CodigoCETIP, CodigoSELIC) VALUES('CBLC', 'CBLC', '60777661000150', 2, 1, null, null)
	 INSERT INTO Clearing (Codigo, Descricao, CNPJ, IdFormaLiquidacao, IdLocalFeriado, CodigoCETIP, CodigoSELIC) VALUES('BOVESPA', 'BOVESPA', '09346601000125', 2, 1, null, null)		
 END	
 GO
 			
 IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'LocalNegociacao') = 0
Begin
	CREATE TABLE [dbo].LocalNegociacao
	(
		[IdLocalNegociacao]				int NOT NULL IDENTITY PRIMARY KEY,
		[Codigo]						VARCHAR(30) NOT NULL, 
		[Descricao]						VARCHAR(100) NOT NULL, 
		[Pais]							VARCHAR(30) NOT NULL, 
		[IdMoeda]						smallint NOT NULL,
		[IdLocalFeriado]				smallint NOT NULL,
		[ParaisoFiscal] 				[char](1) NOT NULL DEFAULT 'N'				
	 )	 
	 
	INSERT INTO LocalNegociacao VALUES ('Brasil', 'Brasil', 'Brasil', 1, 1, 'N');
	INSERT INTO LocalNegociacao VALUES ('SELIC', 'SELIC', 'Brasil', 1, 1, 'N');
	INSERT INTO LocalNegociacao VALUES ('CETIP', 'CETIP', 'Brasil', 1, 1, 'N');
	INSERT INTO LocalNegociacao VALUES ('BMF', 'BMF', 'Brasil', 1, 1, 'N');
	INSERT INTO LocalNegociacao VALUES ('BOVESPA', 'BOVESPA', 'Brasil', 1, 1, 'N');	 
 END	
GO

IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'TransferenciaSerie') = 0
Begin
	CREATE TABLE [dbo].TransferenciaSerie
	(
		[IdTransferenciaSerie] [int] PRIMARY KEY NOT NULL IDENTITY , 
		[DataExecucao] [datetime] NOT NULL,
		[DataPosicao] [datetime] NOT NULL,
		[IdCarteira] [int] NOT NULL,
		[IdSerieOrigem] [int] NOT NULL,		
		[IdSerieDestino] [int] NOT NULL,		
		[IdPosicao] [int] NOT NULL,	
		[IdEventoRollUp] [int] NULL	
	 )
 END	
 GO

IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'EventoRollUp') = 0
Begin
	CREATE TABLE [dbo].EventoRollUp
	(
		[IdEventoRollUp] [int] PRIMARY KEY NOT NULL IDENTITY , 
		[DataExecucao] [datetime] NOT NULL,
		[DataNav] [datetime] NOT NULL,
		[IdFundoOffShore] [int] NOT NULL,
		[IdSerieOrigem] [int] NOT NULL,		
		[IdSerieDestino] [int] NOT NULL,		
	 )
 END	
GO

IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'RentabilidadeSerie') = 0
Begin
	CREATE TABLE RentabilidadeSerie 
	( 	
		IdRentabilidadeSerie					INT 				PRIMARY KEY identity NOT NULL, 
		Data	 		 		 				DateTime		    NOT NULL,						
		IdFundo									INT					NOT NULL,
		IdSerie									INT					NOT NULL,
		QuantidadeSerie							Decimal(25,12)		NULL,
		Rentabilidade							Decimal(25,12)		NULL,
		RentabilidadeAcumuluda					Decimal(25,12)		NULL
	) 
END
GO

IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'RentabilidadeFundoOffShore') = 0
Begin
	CREATE TABLE RentabilidadeFundoOffShore 
	( 	
		IdRentabilidadeFundoOffShore			INT 				PRIMARY KEY identity NOT NULL, 
		Data	 		 		 				DateTime		    NOT NULL,						
		IdFundo									INT					NOT NULL,
		Patrimonio								Decimal(25,12)		NULL,
		ValorCota								Decimal(25,12)		NULL,
		PatrimonioAnterior						Decimal(25,12)		NULL,
		ValorCotaAnterior						Decimal(25,12)		NULL,		
		Rentabilidade							Decimal(25,12)		NULL,
		RentabilidadePercentual					Decimal(25,12)		NULL,
		RentabilidadeAcumuluda					Decimal(25,12)		NULL
	) 
END
GO

IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'TabelaExcecaoAdm') = 0
BEGIN
CREATE TABLE TabelaExcecaoAdm(
	IdTabelaExcecao int IDENTITY(1,1) NOT NULL,
	IdTabela int NOT NULL,
	TipoGrupo int NOT NULL,
	CodigoAtivo varchar(100),
 CONSTRAINT TabelaExcecaoAdm_PK PRIMARY KEY CLUSTERED 
(
	IdTabelaExcecao ASC
)
)
END
GO

IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'TaxaGlobal') = 0
BEGIN
CREATE TABLE TaxaGlobal(
	IdTaxaGlobal int IDENTITY(1,1) NOT NULL,
	IdTabelaGlobal int NOT NULL,
	IdTabelaAssociada int NOT NULL,
	Prioridade int NOT NULL,
 CONSTRAINT TaxaGlobal_PK PRIMARY KEY CLUSTERED 
(
	IdTaxaGlobal ASC
)
)
END
GO

IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'CalculoAdministracaoAssociada') = 0
BEGIN
CREATE TABLE [dbo].[CalculoAdministracaoAssociada](
	[IdTabela] [int] NOT NULL,
	[IdCarteira] [int] NOT NULL,
	[ValorDia] [decimal](16, 2) NOT NULL,
	[ValorAcumulado] [decimal](16, 2) NOT NULL,
	[DataFimApropriacao] [datetime] NOT NULL,
	[DataPagamento] [datetime] NOT NULL,
	[ValorCPMFDia] [decimal](8, 2) NOT NULL,
	[ValorCPMFAcumulado] [decimal](8, 2) NOT NULL,
 CONSTRAINT [CalculoAdministracaoAssociada_PK] PRIMARY KEY CLUSTERED 
(
	[IdTabela] ASC
)
)
END
GO

IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'CalculoAdministracaoAssociadaHistorico') = 0
BEGIN
CREATE TABLE [dbo].[CalculoAdministracaoAssociadaHistorico](
	[DataHistorico] [datetime] NOT NULL,
	[IdTabela] [int] NOT NULL,
	[IdCarteira] [int] NOT NULL,
	[ValorDia] [decimal](16, 2) NOT NULL,
	[ValorAcumulado] [decimal](16, 2) NOT NULL,
	[DataFimApropriacao] [datetime] NOT NULL,
	[DataPagamento] [datetime] NOT NULL,
	[ValorCPMFDia] [decimal](8, 2) NOT NULL,
	[ValorCPMFAcumulado] [decimal](8, 2) NOT NULL,
 CONSTRAINT [CalculoAdministracaoAssociadaHistorico_PK] PRIMARY KEY CLUSTERED 
(
	[DataHistorico] ASC,
	[IdTabela] ASC
)
)
END
GO

IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'PerfilProcessamento') = 0
BEGIN
CREATE TABLE PerfilProcessamento(
	IdPerfil int IDENTITY(1,1) NOT NULL,
	Descricao varchar(40) NOT NULL,
	Tipo int NOT NULL,
	DiasFechamento int NULL,
	DiasRetroagir int NULL,
	AberturaIndexada char(1) NULL,
 CONSTRAINT PerfilProcessamento_PK PRIMARY KEY CLUSTERED 
(
	IdPerfil ASC
)
)
END
GO

IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'ClientePerfil') = 0
BEGIN
CREATE TABLE ClientePerfil(
	IdPerfil int NOT NULL,
	IdCliente int NOT NULL,
	Sequencia int NOT NULL,
 CONSTRAINT CarteiraPerfil_PK PRIMARY KEY CLUSTERED 
(
	IdPerfil ASC, IdCliente ASC
)
)
END
GO

IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'LogProcessamento') = 0
BEGIN
CREATE TABLE LogProcessamento(
	IdCliente int NOT NULL,
	Data datetime NOt NULL,
	Login varchar(50) NOT NULL,
	Tipo int NOT NULL,
	DataInicio datetime NOT NULL,
	DataFim datetime NOT NULL,
	DataInicialPeriodo datetime NOT NULL,
	DataFinalPeriodo datetime NOT NULL,
	Erro int NOT NULL,
	Mensagem varchar(8000) NOT NULL,
 CONSTRAINT LogProcessamento_PK PRIMARY KEY CLUSTERED 
(
	IdCliente ASC, Data ASC
)
)
END
GO

IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'BoletoRetroativo') = 0
BEGIN
CREATE TABLE BoletoRetroativo(
	IdCliente int NOT NULL,
	TipoMercado int NOT NULL,
	DataBoleto datetime NOT NULL,
 CONSTRAINT BoletoRetroativo_PK PRIMARY KEY CLUSTERED 
(
	IdCliente ASC
)
)
END
GO

IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'AgendaProcessos') = 0
BEGIN
CREATE TABLE AgendaProcessos(
	IdAgendaProcesso int  IDENTITY(1,1) NOT NULL,
	IdPerfil int NOt NULL,
	HoraExecucao datetime NOT NULL,
	DataFinal datetime NULL,
	Ativo char(1) NOT NULL,
 CONSTRAINT AgendaProcessos_PK PRIMARY KEY CLUSTERED 
(
	IdAgendaProcesso ASC
)
)
END
GO

IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'LogProcessamentoAutomatico') = 0
BEGIN
CREATE TABLE LogProcessamentoAutomatico(
	idLog int  IDENTITY(1,1) NOT NULL,
	idAgendaProcesso int NOT NULL,
	DataInicio datetime NOT NULL,
	DataFim datetime NOT NULL,
	Erro char(1) NOT NULL,
	Mensagem varchar(8000) NULL,
 CONSTRAINT LogProcessamentoAutomatico_PK PRIMARY KEY CLUSTERED 
(
	idLog ASC
)
)
END
GO

IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'CalculoRebateImpactaPL') = 0
BEGIN
create TABLE CalculoRebateImpactaPL
(
	IdCliente int NOT NULL,
	IdCarteira int NOT NULL,
	DataLancamento datetime NOT NULL,
	DataPagamento datetime NOT NULL,
	ValorDia decimal(16,2) NOT NUll,
	ValorAcumulado decimal(16,2) NOT NULL,
 CONSTRAINT CalculoRebateImpactaPL_PK PRIMARY KEY CLUSTERED 
(
	IdCliente ASC, IdCarteira ASC, DataLancamento ASC
)
)
END
GO

IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'IncorporacaoCisaoFusaoBolsa') = 0
BEGIN
CREATE TABLE IncorporacaoCisaoFusaoBolsa(
	IdIncorporacaoCisaoFusaoBolsa int IDENTITY(1,1) NOT NULL,
	Tipo int NOT NULL,
	CdAtivoBolsa varchar(25) NOT NULL,
	DataPosicao datetime NOT NULL,
	DataEx datetime NOT NULL,
 CONSTRAINT IncorporacaoCisaoFusaoBolsa_PK PRIMARY KEY CLUSTERED 
(
	IdIncorporacaoCisaoFusaoBolsa ASC
)
)
END
GO

IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'IncorporacaoCisaoFusaoBolsaDetalhe') = 0
BEGIN
CREATE TABLE IncorporacaoCisaoFusaoBolsaDetalhe(
	IdIncorporacaoCisaoFusaoBolsa int NOT NULL,
	CdAtivoBolsa varchar(25) NOT NULL,
	Percentual decimal(5,2) NULL,
	Fator decimal(5,2) NULL,
	Custo decimal(8,2) NOT NULL,
 CONSTRAINT IncorporacaoCisaoFusaoBolsaDetalhe_PK PRIMARY KEY CLUSTERED 
(
	IdIncorporacaoCisaoFusaoBolsa ASC, CdAtivoBolsa ASC
)
)
END
GO

IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'EventoFundo') = 0
BEGIN
CREATE TABLE EventoFundo(
	IdEventoFundo int IDENTITY(1,1) NOT NULL,
	IdCarteiraOrigem int NOT NULL,
	IdCarteiraDestino int NOT NULL,
	TipoEvento int NOT NULL,
	Percentual decimal(5,2) NULL,
	DataPosicao datetime NOT NULL,
	Status varchar(10) NOT NULL,
 CONSTRAINT EventoFundo_PK PRIMARY KEY CLUSTERED 
(
	IdEventoFundo ASC
)
)
END
GO

IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'EventoFundoCautela') = 0
BEGIN
CREATE TABLE EventoFundoCautela(
	IdEventoFundo int NOT NULL,
	IdCotista int NOT NULL,
	DataAplicacao datetime NOT NULL,
	IdOperacao int NOT NULL,
	Quantidade decimal(28,12) NOT NULL,
 CONSTRAINT EventoFundoCautela_PK PRIMARY KEY CLUSTERED 
(
	IdEventoFundo ASC, IdCotista ASC, DataAplicacao ASC, IdOperacao ASc
)
)
END
GO

IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'EventoFundoBolsa') = 0
BEGIN
CREATE TABLE EventoFundoBolsa(
	IdEventoFundo int NOT NULL,
	TipoMercado int NOT NULL,
	CdAtivoBolsa varchar(25) NOT NULL,
	Quantidade decimal(28,12) NOT NULL,
 CONSTRAINT EventoFundoBolsa_PK PRIMARY KEY CLUSTERED 
(
	IdEventoFundo ASC, TipoMercado Asc, CdAtivoBolsa ASC
)
)
END
GO

IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'EventoFundoBMF') = 0
BEGIN
CREATE TABLE EventoFundoBMF(
	IdEventoFundo int NOT NULL,
	TipoMercado int NOT NULL,
	CdAtivoBMF varchar(20) NOT NULL,
	Serie varchar(5) NOT NULL,
	Quantidade decimal(28,12) NOT NULL,
	ValorLiquido decimal(16,2) NOT NULL,
 CONSTRAINT EventoFundoBMF_PK PRIMARY KEY CLUSTERED 
(
	IdEventoFundo ASC, TipoMercado Asc, CdAtivoBMF ASC, Serie ASC
)
)
END
GO

IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'EventoFundoSwap') = 0
BEGIN
CREATE TABLE EventoFundoSwap(
	IdEventoFundo int NOT NULL,
	TipoMercado int NOT NULL,
	NumeroContrato varchar(20) NOT NULL,
	DataEmissao datetime NOT NULL,
	DataVencimento datetime NOT NULL,
	TipoPonta tinyint NOT NULL,
	IdIndice smallint NULL,
	TipoPontaContraParte tinyint NOT NULL,
	IdIndiceContraParte smallint NULL,
	Quantidade decimal(28,12) NOT NULL,
	QuantidadeOriginal decimal(28,12) NOT NULL,
 CONSTRAINT EventoFundoSwap_PK PRIMARY KEY CLUSTERED 
(
	IdEventoFundo ASC, TipoMercado Asc, NumeroContrato ASC, DataEmissao ASC, DataVencimento ASC, TipoPonta ASC, TipoPontaContraParte ASC
)
)
END
GO

IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'EventoFundoRendaFixa') = 0
BEGIN
CREATE TABLE EventoFundoRendaFixa(
	IdEventoFundo int NOT NULL,
	TipoMercado int NOT NULL,
	IdTitulo int NOT NULL,
	TipoOperacao tinyint NOT NULL,
	DataOperacao datetime NOT NULL,
	DataLiquidacao datetime NOT NULL,
	Quantidade decimal(28,12) NOT NULL,
 CONSTRAINT EventoFundoRendaFixa_PK PRIMARY KEY CLUSTERED 
(
	IdEventoFundo ASC, TipoMercado Asc, IdTitulo ASC, TipoOperacao ASC, DataOperacao ASC, DataLiquidacao ASC
)
)
END
GO

IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'EventoFundoFundo') = 0
BEGIN
CREATE TABLE EventoFundoFundo(
	IdEventoFundo int NOT NULL,
	TipoMercado int NOT NULL,
	IdCarteira int NOT NULL,
	DataAplicacao datetime NOT NULL,
	DataConversao datetime NOT NULL,
	Quantidade decimal(28,12) NOT NULL,
 CONSTRAINT EventoFundoFundo_PK PRIMARY KEY CLUSTERED 
(
	IdEventoFundo ASC, TipoMercado Asc, IdCarteira ASC, DataAplicacao ASC, DataConversao ASC
)
)
END
GO

IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'EventoFundoLiquidacao') = 0
BEGIN
CREATE TABLE EventoFundoLiquidacao(
	IdEventoFundo int Not NULL,
	TipoMercado int NOT NULL,
	DataLancamento Datetime NOT NULL,
	DataVencimento Datetime NOT NULL,
	Descricao varchar(600) NOT NULL,
	Valor  decimal(16,2) NOT NULL,
	Quantidade decimal(28,12) NOT NULL,
	QuantidadeConstante int NOT NULL,
	Contador int NOT NULL,
 CONSTRAINT EventoFundoLiquidacao_PK PRIMARY KEY CLUSTERED 
(
	IdEventoFundo ASC, TipoMercado ASC, DataLancamento ASC, DataVencimento ASC, Descricao ASC
)
)
END
GO

IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'EventoFundoCaixa') = 0
BEGIN
CREATE TABLE EventoFundoCaixa(
	IdEventoFundo int Not NULL,
	TipoMercado int NOT NULL,
	Data Datetime NOT NULL,
	IdConta int NOT NULL,
	SaldoAbertura decimal(16,2) NOT NULL,
	Valor  decimal(16,2) NOT NULL,
	QuantidadeConstante int NOT NULL,
	Descricao varchar(20) NOT NULL,
 CONSTRAINT EventoFundoCaixa_PK PRIMARY KEY CLUSTERED 
(
	IdEventoFundo ASC, TipoMercado ASC, Data ASC, IdConta ASC, SaldoAbertura ASC
)
)
END
GO

IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'EventoFundoMovimentoCautelas') = 0
BEGIN
CREATE TABLE EventoFundoMovimentoCautelas(
	IdEventoFundo int Not NULL,
	IdOperacao int NOT NULL,
 CONSTRAINT EventoFundoMovimentoCautelas_PK PRIMARY KEY CLUSTERED 
(
	IdEventoFundo ASC, IdOperacao ASC
)
)
END
GO

IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'EventoFundoMovimentoAtivos') = 0
BEGIN
	CREATE TABLE EventoFundoMovimentoAtivos
	(
		IdEventoFundo int Not NULL,
		TipoMercado int NOT NULL,
		IdOperacao int NOT NULL,
		CONSTRAINT EventoFundoMovimentoAtivos_PK PRIMARY KEY CLUSTERED 
		(
			IdEventoFundo ASC, TipoMercado ASC, IdOperacao ASC
		)
	)
END
GO

IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'EventoFundoPosicaoCautelas') = 0
BEGIN
	CREATE TABLE EventoFundoPosicaoCautelas
	(
		IdEventoFundo int Not NULL,
		IdPosicao int NOT NULL,
		CONSTRAINT EventoFundoPosicaoCautelas_PK PRIMARY KEY CLUSTERED 
		(
			IdEventoFundo ASC, IdPosicao ASC
		)
	)
END
GO

IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'EventoFundoPosicaoAtivos') = 0
BEGIN
	CREATE TABLE EventoFundoPosicaoAtivos
	(
		IdEventoFundo int Not NULL,
		TipoMercado int NOT NULL,
		IdPosicao int NOT NULL,
		CONSTRAINT EventoFundoPosicaoAtivos_PK PRIMARY KEY CLUSTERED 
		(
			IdEventoFundo ASC, TipoMercado ASC, IdPosicao ASC
		)
	)
END
GO

IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'CadastroComplementarTipoLista') = 0
Begin
 CREATE TABLE CadastroComplementarTipoLista
 (  
   IdTipoLista INT PRIMARY KEY identity NOT NULL, 
   NomeLista varchar(50),
   TipoLista int NOT NULL,   
   CONSTRAINT TipoUnico UNIQUE (NomeLista,TipoLista)
 ) 
End
GO

IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'CadastroComplementarItemLista') = 0
Begin
 CREATE TABLE CadastroComplementarItemLista
 (  
   IdLista INT PRIMARY KEY identity NOT NULL, 
   IdTipoLista int FOREIGN KEY REFERENCES CadastroComplementarTipoLista(IdTipoLista) NOT NULL,
   ItemLista varchar(50) NOT NULL,   
 ) 
End
GO

IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'CadastroComplementarCampos') = 0
Begin
 CREATE TABLE CadastroComplementarCampos
 (  
   IdCamposComplementares INT  PRIMARY KEY identity NOT NULL, 
   TipoCadastro int           NOT NULL, 
   NomeCampo varchar(50)      NOT NULL, 
   DescricaoCampo varchar(50) NULL, 
   ValorDefault varchar(20)   NULL,
   TipoCampo int              NOT NULL,
   CampoObrigatorio Char      NOT NULL,
   Tamanho int                NULL,
   CasasDecimais int          NULL
 ) 
End
GO

IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'CadastroComplementar') = 0
Begin
 CREATE TABLE CadastroComplementar
 (  
   IdCadastroComplementares INT PRIMARY KEY identity NOT NULL, 
   IdCamposComplementares int FOREIGN KEY REFERENCES CadastroComplementarCampos(IdCamposComplementares),
   IdMercadoTipoPessoa int NOT NULL,
   DescricaoMercadoTipoPessoa varchar(50)    NOT NULL, 
   ValorCampo varchar(50)            NOT NULL
 ) 
End
GO

IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'EmpresaSecuritizada') = 0
Begin
	CREATE TABLE EmpresaSecuritizada
	(  
		 IdEmpresaSecuritizada INT PRIMARY KEY identity NOT NULL, 
		 IdAgenteMercado int FOREIGN KEY REFERENCES AgenteMercado(IdAgente) NOT NULL,
		 IdTitulo int FOREIGN KEY REFERENCES TituloRendaFixa(IdTitulo) NOT NULL
	) 
End
GO

IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'ExcecoesTributacaoIR') = 0
Begin
	CREATE TABLE ExcecoesTributacaoIR
	(
		 IdExcecoesTributacaoIR INT PRIMARY KEY identity NOT NULL, 
		 Mercado int NOT NULL,
		 TipoClasseAtivo int NULL,
	     Ativo varchar(50) NULL,
		 TipoInvestidor int NOT NULL,	
		 IsencaoIR int NOT NULL,
		 AliquotaIR varchar(20) NULL,
		 IsencaoGanhoCapital int NOT NULL,
		 AliquotaIRGanhoCapital varchar(20) NULL,
		 CdAtivoBMF varchar(20)  NULL,
		 SerieBMF varchar(5) NULL,
		 IdTituloRendaFixa int FOREIGN KEY REFERENCES TituloRendaFixa(IdTitulo) NULL,
		 IdOperacaoSwap int FOREIGN KEY REFERENCES OperacaoSwap(IdOperacao) NULL,
		 IdCarteira int FOREIGN KEY REFERENCES Carteira(IdCarteira) NULL,
		 CdAtivoBolsa varchar(25) FOREIGN KEY REFERENCES AtivoBolsa(CdAtivoBolsa) NULL,
		 CONSTRAINT FK_AtivoBMF FOREIGN KEY (CdAtivoBMF, SerieBMF) REFERENCES AtivoBMF(CdAtivoBMF, Serie)
	) 
End
GO

IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'CadastroComplementarTipoLista') = 0
Begin
 CREATE TABLE CadastroComplementarTipoLista
 (  
   IdTipoLista INT PRIMARY KEY identity NOT NULL, 
   NomeLista varchar(50),
   TipoLista int NOT NULL,   
   CONSTRAINT TipoUnico UNIQUE (NomeLista,TipoLista)
 ) 
End
GO

IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'CadastroComplementarItemLista') = 0
Begin
 CREATE TABLE CadastroComplementarItemLista
 (  
   IdLista INT PRIMARY KEY identity NOT NULL, 
   IdTipoLista int FOREIGN KEY REFERENCES CadastroComplementarTipoLista(IdTipoLista) NOT NULL,
   ItemLista varchar(50) NOT NULL,   
 ) 
End
GO

IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'CadastroComplementarCampos') = 0
Begin
 CREATE TABLE CadastroComplementarCampos
 (  
   IdCamposComplementares INT  PRIMARY KEY identity NOT NULL, 
   TipoCadastro int           NOT NULL, 
   NomeCampo varchar(50)      NOT NULL, 
   DescricaoCampo varchar(50) NULL, 
   ValorDefault varchar(20)   NULL,
   TipoCampo int              NOT NULL,
   CampoObrigatorio Char      NOT NULL,
   Tamanho int                NULL,
   CasasDecimais int          NULL
 ) 
End
GO

IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'CadastroComplementar') = 0
Begin
 CREATE TABLE CadastroComplementar
 (  
   IdCadastroComplementares INT PRIMARY KEY identity NOT NULL, 
   IdCamposComplementares int FOREIGN KEY REFERENCES CadastroComplementarCampos(IdCamposComplementares),
   IdMercadoTipoPessoa int NOT NULL,
   DescricaoMercadoTipoPessoa varchar(50)    NOT NULL, 
   ValorCampo varchar(50)            NOT NULL
 ) 
End
GO

IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'CotacaoASEL007') = 0
BEGIN
	CREATE TABLE CotacaoASEL007
	(
		 Data DATETIME NULL,
		 CodigoTitulo VARCHAR(6) NULL,
	     DataVencimento DATETIME NULL,
		 PU DECIMAL(16,8) NULL	
	) 
END
GO

IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'ParametroAdministradorFundoInvestimento') = 0
BEGIN
	CREATE TABLE ParametroAdministradorFundoInvestimento
	(
		 IdParametro INT PRIMARY KEY identity NOT NULL, 
		 ExecucaoRecolhimento INT NOT NULL,
		 AliquotaIR INT NOT NULL,
		 DataRecolhimento DATETIME NULL,
		 Administrador INT FOREIGN KEY REFERENCES AgenteMercado(IdAgente) NOT NULL
	) 
END
GO

IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'FundoInvestimentoFormaCondominio') = 0
BEGIN
	CREATE TABLE FundoInvestimentoFormaCondominio
	(
		 IdFormaCondominio INT PRIMARY KEY identity NOT NULL, 
		 DataInicioVigencia DATETIME NOT NULL,
		 FormaCondominio INT NOT NULL,
		 ExecucaoRecolhimento INT NOT NULL,
		 AliquotaIR INT NOT NULL,
		 DataRecolhimento DATETIME NULL,
		 FundoInvestimento INT FOREIGN KEY REFERENCES Carteira(IdCarteira) NOT NULL,
	) 
END
GO

IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'DesenquadramentoTributario') = 0
BEGIN
	CREATE TABLE DesenquadramentoTributario
	(
		 IdDesenquadramentoTributario INT PRIMARY KEY identity NOT NULL, 
		 TipoOcorrencia INT NOT NULL,
		 Data DATETIME NOT NULL,
		 ClassificacaoTributariaAtual INT NULL,
		 MotivoMudancaOcorrencia VARCHAR(200),
		 ExecucaoRecolhimento INT NULL,
		 AliquotaIR INT NULL,
		 DataRecolhimento DATETIME NULL,
		 FundoInvestimento INT FOREIGN KEY REFERENCES Carteira(IdCarteira) NOT NULL,
	) 
END
GO

IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'AgenciaClassificadora') = 0
Begin
	CREATE TABLE AgenciaClassificadora
	(  
		 IdAgenciaClassificadora INT PRIMARY KEY identity NOT NULL, 
		 CodigoAgencia int NOT NULL,
		 DescricaoAgencia varchar(255) NOT NULL,
		 CNPJ varchar(30) NULL,
		 CodigoExterno int NULL
	) 
End
GO

IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'PadraoNotas') = 0
Begin
	CREATE TABLE PadraoNotas
	(
		 IdPadraoNotas INT PRIMARY KEY identity NOT NULL, 
		 ItemAvaliado int NULL,
	     DataInicial DateTime NOT NULL,
		 DataFinal DateTime NOT NULL,	
		 CodigoNota Varchar(50) NOT NULL,
		 DescricaoNota varchar(200) NULL,
		 Sequencia int NOT NULL,
		 IdAgencia int FOREIGN KEY REFERENCES AgenciaClassificadora(IdAgenciaClassificadora) NULL
	) 
End
GO

IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'PerfilFaixaRating') = 0
Begin
	CREATE TABLE PerfilFaixaRating
	(
		 IdPerfilFaixaRating INT PRIMARY KEY identity NOT NULL, 
		 CodigoPerfil varchar(50) NOT NULL,
	     DescricaoPerfil varchar(200) NULL
	) 
End
GO

IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'PerfilClienteFaixaRating') = 0
Begin
	CREATE TABLE PerfilClienteFaixaRating
	(
		 IdPerfilClienteFaixaRating INT PRIMARY KEY identity NOT NULL, 
		 Descricao varchar(200) NULL,
		 IdPerfilFaixaRating int FOREIGN KEY REFERENCES PerfilFaixaRating(IdPerfilFaixaRating) NULL
	) 
End
GO

IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'PerfilCliente') = 0
Begin
	CREATE TABLE PerfilCliente
	(
		 IdPerfilCliente INT PRIMARY KEY identity NOT NULL, 
		 IdPerfilClienteFaixaRating int FOREIGN KEY REFERENCES PerfilClienteFaixaRating(IdPerfilClienteFaixaRating) NOT NULL,
		 IdCarteira int FOREIGN KEY REFERENCES Carteira(IdCarteira) NOT NULL
	) 
End
GO

IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'ClassificacaoFaixaRating') = 0
Begin
	CREATE TABLE ClassificacaoFaixaRating
	(
		 IdClassificacaoFaixaRating INT PRIMARY KEY identity NOT NULL, 
		 SequenciaAgencia int NOT NULL,
		 DataVigencia DateTime NOT NULL,
		 Classificacao int NULL,
		 IdPerfilFaixaRating int FOREIGN KEY REFERENCES PerfilFaixaRating(IdPerfilFaixaRating) NOT NULL,
		 IdAgenciaClassificadora int FOREIGN KEY REFERENCES AgenciaClassificadora(IdAgenciaClassificadora) NOT NULL,
		 IdPadraoNotas int FOREIGN KEY REFERENCES PadraoNotas(IdPadraoNotas) NULL
	) 
End
GO

IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'ClassificacaoAtivo') = 0
Begin
	CREATE TABLE ClassificacaoAtivo
	(
		 IdClassificacaoAtivo INT PRIMARY KEY identity NOT NULL, 
		 ItemAvaliado int NOT NULL,
		 Ativo int NOT NULL,
		 IdAgenciaClassificadora int FOREIGN KEY REFERENCES AgenciaClassificadora(IdAgenciaClassificadora) NOT NULL,
		 IdPadraoNotas int FOREIGN KEY REFERENCES PadraoNotas(IdPadraoNotas) NULL
	) 
End
GO

IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'AgendaComeCotas') = 0
BEGIN
	CREATE TABLE AgendaComeCotas
	(
		 IdAgendamentoComeCotas INT PRIMARY KEY identity NOT NULL,
		 IdCarteira int FOREIGN KEY REFERENCES Carteira(IdCarteira) NOT NULL,
		 DataLancamento datetime NOT NULL,
		 DataVencimento datetime NOT NULL,
		 TipoEvento int NOT NULL,
		 IdPosicao int NOT NULL,
		 DataUltimoIR datetime NOT NULL,
		 Quantidade decimal(28,12) NOT NULL,
		 QuantidadeAntesCortes decimal(28,12) NOT NULL,
		 ValorCotaAplic decimal(28,12) NOT NULL,
		 ValorCotaUltimoPagamentoIR decimal(28,12) NOT NULL,
		 ValorCota decimal(28,12) NOT NULL,
		 AliquotaCC decimal(5,2) NOT NULL,
		 RendimentoBrutoDesdeAplicacao decimal(16,2) NOT NULL,
 		 RendimentoDesdeUltimoPagamentoIR decimal(16,2) NOT NULL,
		 NumDiasCorridosDesdeAquisicao int NOT NULL,
		 PrazoIOF int NOT NULL,
		 AliquotaIOF decimal(5,2) NOT NULL,
		 ValorIOF  decimal(28,12) NOT NULL,
		 ValorIOFVirtual decimal(28,12) NOT NULL,
		 PrejuizoUsado decimal(16,2) NOT NULL,
		 RendimentoCompensado decimal(16,2) NOT NULL,
		 ValorIRAgendado decimal(28,12) NOT NULL,
		 ValorIRPago decimal(16,2) NOT NULL,
		 Residuo15 decimal(28,12) NOT NULL,
		 Residuo175 decimal(28,12) NOT NULL,
		 Residuo20 decimal(28,12) NOT NULL,
		 Residuo225 decimal(28,12) NOT NULL,
		 QuantidadeComida decimal(28,12) NOT NULL,
		 QuantidadeFinal decimal(28,12) NOT NULL,
		 ExecucaoRecolhimento int NOT NULL,
		 TipoAliquotaIR int NOT NULL
	) 
END
GO

IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'ClassesOffShore') = 0
BEGIN

	CREATE TABLE ClassesOffShore
	(
		 IdClassesOffShore INT PRIMARY KEY NOT NULL,
		 Nome varchar(100) NOT NULL,
		 DataInicio datetime NOT NULL,
		 SerieUnica char(1) NOT NULL,
		 FrequenciaSerie int NOT NULL,
		 PoliticaInvestimentos varchar(200) NULL,
		 Moeda smallint FOREIGN KEY REFERENCES Moeda(IdMoeda) NULL,
		 Domicilio int FOREIGN KEY REFERENCES LocalNegociacao(IdLocalNegociacao) NULL,
		 TaxaAdm decimal(16,2) NULL,
		 TaxaCustodia decimal(16,2) NULL,
		 TaxaPerformance decimal(16,2) NULL,
		 IndexadorPerf smallint FOREIGN KEY REFERENCES Indice(IdIndice) NULL,
		 FreqCalcPerformance int NULL,
		 LockUp int NULL,
		 HardSoft int NULL,
		 PenalidadeResgate decimal(5,2) NULL,
		 VlMinimoAplicacao decimal(16,2) NULL,
		 VlMinimoResgate decimal(16,2) NULL,
		 VlMinimoPermanencia decimal(16,2) NULL,
		 QtdDiasConvResgate int NULL,
		 QtdDiasLiqFin int NULL,
		 HoldBack decimal(5,2) NULL,
		 AnivHoldBack datetime NULL,
		 SidePocket char(1) NOT NULL default 'N'
	) 
END
GO

IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'SeriesOffShore') = 0
BEGIN

	CREATE TABLE SeriesOffShore
	(
		 IdSeriesOffShore INT PRIMARY KEY identity NOT NULL,
		 IdClassesOffShore int  NOT NULL,
		 Mnemonico varchar(100) NOT NULL,
		 Descricao varchar(100) NULL,
		 VlCotaInicial decimal(28,12) NULL,
		 ISIN varchar(12) NULL,
		 DataRollUp datetime NULL,
		 Inativa char(1) NOT NULL DEFAULT 'N'
	)
END
GO

	
if NOT exists(select 1 from syscolumns where id = object_id('CotacaoSerie') and name = 'DigitadoImportado')
BEGIN
	ALTER TABLE CotacaoSerie ADD DigitadoImportado INT NOT NULL default '2';
END		
GO

if NOT exists(select 1 from syscolumns where id = object_id('Indice') and name = 'IdFeeder')
BEGIN
	ALTER TABLE Indice ADD IdFeeder INT not NULL default '5';
END	
GO

if NOT exists(select 1 from syscolumns where id = object_id('PosicaoRendaFixa') and name = 'ValorCurvaVencimento')
BEGIN
	ALTER TABLE PosicaoRendaFixa ADD ValorCurvaVencimento Decimal(25,2) NULL default '0';
end
GO
	
if NOT exists(select 1 from syscolumns where id = object_id('PosicaoRendaFixa') and name = 'PUCurvaVencimento')
BEGIN	
	ALTER TABLE PosicaoRendaFixa ADD PUCurvaVencimento Decimal(25,8) NULL default '0';
end
GO

if NOT exists(select 1 from syscolumns where id = object_id('PosicaoRendaFixa') and name = 'AjusteMTM')
BEGIN
	ALTER TABLE PosicaoRendaFixa ADD AjusteMTM Decimal(25,8) NULL default '0';
END	
GO

if NOT exists(select 1 from syscolumns where id = object_id('PosicaoRendaFixa') and name = 'AjusteVencimento')
BEGIN
	ALTER TABLE PosicaoRendaFixa ADD AjusteVencimento Decimal(25,8) NULL default '0';
END	
GO

if NOT exists(select 1 from syscolumns where id = object_id('PosicaoRendaFixa') and name = 'TaxaMTM')
BEGIN
	ALTER TABLE PosicaoRendaFixa ADD TaxaMTM Decimal(25,8) NULL default '0';
END	
GO

if NOT exists(select 1 from syscolumns where id = object_id('PosicaoRendaFixaAbertura') and name = 'ValorCurvaVencimento')
BEGIN
	ALTER TABLE PosicaoRendaFixaAbertura ADD ValorCurvaVencimento Decimal(25,2) NULL default '0';
END
GO

if NOT exists(select 1 from syscolumns where id = object_id('PosicaoRendaFixaAbertura') and name = 'PUCurvaVencimento')
BEGIN	
	ALTER TABLE PosicaoRendaFixaAbertura ADD PUCurvaVencimento Decimal(25,8) NULL default '0';
END
GO

if NOT exists(select 1 from syscolumns where id = object_id('PosicaoRendaFixaAbertura') and name = 'AjusteMTM')
BEGIN	
	ALTER TABLE PosicaoRendaFixaAbertura ADD AjusteMTM Decimal(25,8) NULL default '0';
END	
GO

if NOT exists(select 1 from syscolumns where id = object_id('PosicaoRendaFixaAbertura') and name = 'AjusteVencimento')
BEGIN
	ALTER TABLE PosicaoRendaFixaAbertura ADD AjusteVencimento Decimal(25,8) NULL default '0';
END
GO

if NOT exists(select 1 from syscolumns where id = object_id('PosicaoRendaFixaAbertura') and name = 'TaxaMTM')
BEGIN	
	ALTER TABLE PosicaoRendaFixaAbertura ADD TaxaMTM Decimal(25,8) NULL default '0';
END
GO

if NOT exists(select 1 from syscolumns where id = object_id('PosicaoRendaFixaHistorico') and name = 'ValorCurvaVencimento')
BEGIN
	ALTER TABLE PosicaoRendaFixaHistorico ADD ValorCurvaVencimento Decimal(25,2) NULL default '0';
END
GO

if NOT exists(select 1 from syscolumns where id = object_id('PosicaoRendaFixaHistorico') and name = 'PUCurvaVencimento')
BEGIN
	ALTER TABLE PosicaoRendaFixaHistorico ADD PUCurvaVencimento Decimal(25,8) NULL default '0';
END
GO

if NOT exists(select 1 from syscolumns where id = object_id('PosicaoRendaFixaHistorico') and name = 'AjusteMTM')
BEGIN	
	ALTER TABLE PosicaoRendaFixaHistorico ADD AjusteMTM Decimal(25,8) NULL default '0';
END
GO

if NOT exists(select 1 from syscolumns where id = object_id('PosicaoRendaFixaHistorico') and name = 'AjusteVencimento')
BEGIN	
	ALTER TABLE PosicaoRendaFixaHistorico ADD AjusteVencimento Decimal(25,8) NULL default '0';
END
GO

if NOT exists(select 1 from syscolumns where id = object_id('PosicaoRendaFixaHistorico') and name = 'TaxaMTM')
BEGIN	
	ALTER TABLE PosicaoRendaFixaHistorico ADD TaxaMTM Decimal(25,8) NULL default '0';
END
GO

if NOT exists(select 1 from syscolumns where id = object_id('TituloRendaFixa') and name = 'PremioRebate')
BEGIN
	ALTER TABLE TituloRendaFixa ADD PremioRebate decimal(25,8)
END
GO
	
if NOT exists(select 1 from syscolumns where id = object_id('TituloRendaFixa') and name = 'ExpTaxaEmissao')
BEGIN
	ALTER TABLE TituloRendaFixa ADD ExpTaxaEmissao int
END
GO
	
if NOT exists(select 1 from syscolumns where id = object_id('TituloRendaFixa') and name = 'Periodicidade')
BEGIN
	ALTER TABLE TituloRendaFixa ADD Periodicidade int
END
GO
	
if NOT exists(select 1 from syscolumns where id = object_id('TituloRendaFixa') and name = 'CriterioAmortizacao')
BEGIN
	ALTER TABLE TituloRendaFixa ADD CriterioAmortizacao INT NOT NULL default '2';
END
GO
	
if not exists(select 1 from syscolumns where id = object_id('Carteira') and name = 'CalculaPrazoMedio')
BEGIN
	ALTER TABLE dbo.Carteira ADD CalculaPrazoMedio char(1) NOT NULL DEFAULT 'N'
END
GO
	
if not exists(select 1 from syscolumns where id = object_id('TituloRendaFixa') and name = 'PermiteRepactuacao')
BEGIN
	ALTER TABLE dbo.TituloRendaFixa ADD	PermiteRepactuacao char(1) NOT NULL DEFAULT 'N'
END
GO
	
if not exists(select 1 from syscolumns where id = object_id('TituloRendaFixa') and name = 'ExecutaProRataEmissao')
BEGIN
	ALTER TABLE TituloRendaFixa ADD ExecutaProRataEmissao char(1) NOT NULL DEFAULT 'N'
END
GO
	
if not exists(select 1 from syscolumns where id = object_id('TituloRendaFixa') and name = 'TipoProRata')
BEGIN
	ALTER TABLE TituloRendaFixa ADD TipoProRata int NOT NULL DEFAULT '0'
END
GO
	
if exists(select 1 from syscolumns where id = object_id('PerfilProcessamento') and name = 'AberturaIndexada')
BEGIN
	ALTER TABLE PerfilProcessamento DROP COLUMN AberturaIndexada
END
GO
	
if not exists(select 1 from syscolumns where id = object_id('PerfilProcessamento') and name = 'AberturaAutomatica')
BEGIN
	ALTER TABLE PerfilProcessamento ADD AberturaAutomatica char(1) NOT NULL DEFAULT 'N'
END
GO
	
if not exists(select 1 from syscolumns where id = object_id('MemoriaCalculoRendaFixa') and name = 'TipoEventoTitulo')
BEGIN
	ALTER TABLE MemoriaCalculoRendaFixa ADD TipoEventoTitulo int NOT NULL DEFAULT '1'
END
GO
	
if not exists(select 1 from syscolumns where id = object_id('Carteira') and name = 'DiasAposResgate')
BEGIN
	if not exists(select 1 from syscolumns where id = object_id('Carteira') and name = 'DiasAposResgateIR')
	begin
		ALTER TABLE Carteira ADD DiasAposResgate int NOT NULL DEFAULT '0'
	end
END
GO
	
if not exists(select 1 from syscolumns where id = object_id('Carteira') and name = 'ProjecaoIRComeCotas')
BEGIN
	ALTER TABLE Carteira ADD ProjecaoIRComeCotas tinyint NOT NULL DEFAULT '2'
END
GO
	
if not exists(select 1 from syscolumns where id = object_id('Carteira') and name = 'DiasAposComeCotas')
BEGIN
	if not exists(select 1 from syscolumns where id = object_id('Carteira') and name = 'DiasAposComeCotasIR')
	begin
		ALTER TABLE Carteira ADD DiasAposComeCotas int NOT NULL DEFAULT '0'
	end
END
GO
	
if not exists(select 1 from syscolumns where id = object_id('Carteira') and name = 'RealizaCompensacaoDePrejuizo')
BEGIN
	ALTER TABLE Carteira ADD RealizaCompensacaoDePrejuizo char(1) NOT NULL DEFAULT 'N'
END
GO	

if not exists(select 1 from syscolumns where id = object_id('TituloRendaFixa') and name = 'DefasagemLiquidacao')
BEGIN
	ALTER TABLE TituloRendaFixa ADD DefasagemLiquidacao int NOT NULL DEFAULT '0'
END
GO
	
if not exists(select 1 from syscolumns where id = object_id('OperacaoRendaFixa') and name = 'PercentualPuFace')
BEGIN
	ALTER TABLE OperacaoRendaFixa ADD PercentualPuFace Decimal(25,12) NOT NULL DEFAULT '0'
END
GO
	
if not exists(select 1 from syscolumns where id = object_id('TituloRendaFixa') and name = 'ProRataLiquidacao')
BEGIN
	ALTER TABLE dbo.TituloRendaFixa ADD	ProRataLiquidacao char(1) NOT NULL DEFAULT 'N'
END
GO
	
if not exists(select 1 from syscolumns where id = object_id('TituloRendaFixa') and name = 'ProRataEmissaoDia')
BEGIN
	ALTER TABLE TituloRendaFixa ADD ProRataEmissaoDia int NULL DEFAULT '0'
END
GO
	
if not exists(select 1 from syscolumns where id = object_id('TituloRendaFixa') and name = 'AtivoRegra')
BEGIN
	ALTER TABLE TituloRendaFixa ADD AtivoRegra VARCHAR(10) NULL 
END
GO
	
if not exists(select 1 from syscolumns where id = object_id('TituloRendaFixa') and name = 'DataInicioCorrecao')
BEGIN
	ALTER TABLE TituloRendaFixa ADD DataInicioCorrecao datetime NULL 
END
GO
	
if not exists(select 1 from syscolumns where id = object_id('TituloRendaFixa') and name = 'DefasagemMeses')
BEGIN
	ALTER TABLE TituloRendaFixa ADD DefasagemMeses int NULL DEFAULT '0'
END
GO
	
if not exists(select 1 from syscolumns where id = object_id('TituloRendaFixa') and name = 'EJuros')
BEGIN
	ALTER TABLE TituloRendaFixa ADD EJuros int NULL DEFAULT '6'
END
GO
	
if not exists(select 1 from syscolumns where id = object_id('OperacaoFundo') and name = 'RegistroEditado')
BEGIN
	ALTER TABLE dbo.OperacaoFundo ADD RegistroEditado char(1) NOT NULL DEFAULT 'N'
END
GO
	
if not exists(select 1 from syscolumns where id = object_id('OperacaoFundo') and name = 'ValoresColados')
BEGIN
	ALTER TABLE dbo.OperacaoFundo ADD ValoresColados char(1) NOT NULL DEFAULT 'N' 
END
GO
	
if not exists(select 1 from syscolumns where id = object_id('OperacaoFundoAux') and name = 'RegistroEditado')
BEGIN
	ALTER TABLE dbo.OperacaoFundoAux ADD RegistroEditado char(1) NOT NULL DEFAULT 'N'
END
GO
	
if not exists(select 1 from syscolumns where id = object_id('OperacaoFundoAux') and name = 'ValoresColados')
BEGIN
	ALTER TABLE dbo.OperacaoFundoAux ADD ValoresColados char(1) NOT NULL DEFAULT 'N' 
END
GO	

if not exists(select 1 from syscolumns where id = object_id('OperacaoCotista') and name = 'RegistroEditado')
BEGIN
	ALTER TABLE dbo.OperacaoCotista ADD RegistroEditado char(1) NOT NULL DEFAULT 'N'
END	

if not exists(select 1 from syscolumns where id = object_id('OperacaoCotista') and name = 'ValoresColados')
BEGIN
	ALTER TABLE dbo.OperacaoCotista ADD ValoresColados char(1) NOT NULL DEFAULT 'N'
END	

if not exists(select 1 from syscolumns where id = object_id('OperacaoCotistaAux') and name = 'RegistroEditado')
BEGIN
	ALTER TABLE dbo.OperacaoCotistaAux ADD RegistroEditado char(1) NOT NULL DEFAULT 'N'
END	

if not exists(select 1 from syscolumns where id = object_id('OperacaoCotistaAux') and name = 'ValoresColados')
BEGIN
	ALTER TABLE dbo.OperacaoCotistaAux ADD ValoresColados char(1) NOT NULL DEFAULT 'N'
END	

if exists(select 1 from syscolumns where id = object_id('ColagemResgateDetalhe') and name = 'CodigoExterno')
BEGIN
	ALTER TABLE [dbo].[ColagemResgateDetalhe] ALTER COLUMN CodigoExterno varchar(30) NOT NULL 
END
GO		

if exists(select 1 from syscolumns where id = object_id('ColagemComeCotas') and name = 'CodigoExterno')
BEGIN	
	ALTER TABLE [dbo].[ColagemComeCotas] ALTER COLUMN CodigoExterno varchar(30) NOT NULL 
END
GO	
 
if exists(select 1 from syscolumns where id = object_id('ColagemComeCotasDetalhe') and name = 'CodigoExterno')
BEGIN	
	ALTER TABLE [dbo].[ColagemComeCotasDetalhe] ALTER COLUMN CodigoExterno varchar(30) NOT NULL 
END
GO		

if exists(select 1 from syscolumns where id = object_id('ColagemResgate') and name = 'CodigoExterno')
BEGIN	
	ALTER TABLE [dbo].[ColagemResgate] ALTER COLUMN CodigoExterno varchar(30) NOT NULL
END
GO		

if not exists(select 1 from syscolumns where id = object_id('ColagemResgate') and name = 'Processado')
BEGIN	
	ALTER TABLE [dbo].[ColagemResgate] ADD Processado char(1) NOT NULL DEFAULT 'N';
END
GO		

if not exists(select 1 from syscolumns where id = object_id('ColagemComeCotas') and name = 'Processado')
BEGIN	
	ALTER TABLE [dbo].[ColagemComeCotas] ADD Processado char(1) NOT NULL DEFAULT 'N';
END
GO		

if not exists(select 1 from syscolumns where id = object_id('AtivoBolsa') and name = 'IdLocalCustodia')
BEGIN
	ALTER TABLE dbo.AtivoBolsa ADD IdLocalCustodia int NOT NULL DEFAULT '3'	
END
GO	

if not exists(select 1 from syscolumns where id = object_id('AtivoBolsa') and name = 'IdClearing')
BEGIN
	ALTER TABLE dbo.AtivoBolsa ADD IdClearing int NOT NULL DEFAULT('4')
END
GO	

if not exists(select 1 from syscolumns where id = object_id('AtivoBolsa') and name = 'IdLocalNegociacao')
BEGIN
	ALTER TABLE dbo.AtivoBolsa ADD IdLocalNegociacao int NOT NULL DEFAULT('5')
END
GO	

if not exists(select 1 from syscolumns where id = object_id('AtivoBMF') and name = 'IdLocalCustodia')
BEGIN
	ALTER TABLE dbo.AtivoBMF ADD IdLocalCustodia int NOT NULL DEFAULT '3'
END
GO	

if not exists(select 1 from syscolumns where id = object_id('AtivoBMF') and name = 'IdClearing')
BEGIN
	ALTER TABLE dbo.AtivoBMF ADD IdClearing int NOT NULL DEFAULT('4')
END
GO	

if not exists(select 1 from syscolumns where id = object_id('AtivoBMF') and name = 'IdLocalNegociacao')
BEGIN
	ALTER TABLE dbo.AtivoBMF ADD IdLocalNegociacao int NOT NULL DEFAULT('5')
END
GO	

if not exists(select 1 from syscolumns where id = object_id('PapelRendaFixa') and name = 'IdLocalCustodia')
BEGIN
	ALTER TABLE dbo.PapelRendaFixa ADD IdLocalCustodia int NULL;
	EXEC('UPDATE PapelRendaFixa SET IdLocalCustodia = (CASE WHEN TipoPapel <> 1 THEN 2 ELSE 1 END)');
	ALTER TABLE PapelRendaFixa ALTER COLUMN IdLocalCustodia int NOT NULL;
END
GO	

if not exists(select 1 from syscolumns where id = object_id('PapelRendaFixa') and name = 'IdClearing')
BEGIN
	ALTER TABLE dbo.PapelRendaFixa ADD IdClearing int NULL
	EXEC('UPDATE PapelRendaFixa SET IdClearing = (CASE WHEN TipoPapel <> 1 THEN 2 ELSE 1 END)');
	ALTER TABLE PapelRendaFixa ALTER COLUMN IdClearing int NOT NULL;	
END
GO	

if not exists(select 1 from syscolumns where id = object_id('PapelRendaFixa') and name = 'IdLocalNegociacao')
BEGIN
	ALTER TABLE dbo.PapelRendaFixa ADD IdLocalNegociacao int NULL;
	EXEC('UPDATE PapelRendaFixa SET IdLocalNegociacao = (CASE WHEN TipoPapel <> 1 THEN 3 ELSE 2 END)');
	ALTER TABLE PapelRendaFixa ALTER COLUMN IdLocalNegociacao int NOT NULL;
END
GO	

if not exists(select 1 from syscolumns where id = object_id('Cliente') and name = 'IdLocalNegociacao')
BEGIN
	ALTER TABLE dbo.Cliente ADD IdLocalNegociacao int NULL;
	EXEC('UPDATE Cliente SET IdLocalNegociacao = 1 WHERE idTipo = 500');
END
GO	

if not exists(select 1 from syscolumns where id = object_id('OperacaoBolsa') and name = 'IdCorretora')
BEGIN
	ALTER TABLE dbo.OperacaoBolsa ADD IdCorretora int NULL
	EXEC('UPDATE OperacaoBolsa SET IdCorretora = idAgenteCorretora');
	ALTER TABLE OperacaoBolsa ALTER COLUMN IdCorretora int NOT NULL;
END
GO	

if not exists(select 1 from syscolumns where id = object_id('OperacaoBolsa') and name = 'IdLocalCustodia')
BEGIN
	ALTER TABLE dbo.OperacaoBolsa ADD IdLocalCustodia int NULL
	EXEC('UPDATE OperacaoBolsa SET IdLocalCustodia = 3');
	ALTER TABLE OperacaoBolsa ALTER COLUMN IdLocalCustodia int NOT NULL;
END
GO	

if not exists(select 1 from syscolumns where id = object_id('OperacaoBolsa') and name = 'IdLocalNegociacao')
BEGIN
	ALTER TABLE dbo.OperacaoBolsa ADD IdLocalNegociacao int NULL
	EXEC('UPDATE OperacaoBolsa SET IdLocalNegociacao = 5');
	ALTER TABLE OperacaoBolsa ALTER COLUMN IdLocalNegociacao int NOT NULL;
END
GO	

if not exists(select 1 from syscolumns where id = object_id('OperacaoBolsa') and name = 'IdClearing')
BEGIN
	ALTER TABLE dbo.OperacaoBolsa ADD IdClearing int NULL
	EXEC('UPDATE OperacaoBolsa SET IdClearing = 4');
	ALTER TABLE OperacaoBolsa ALTER COLUMN IdClearing int NOT NULL;
END
GO	

if not exists(select 1 from syscolumns where id = object_id('OperacaoBMF') and name = 'IdCorretora')
BEGIN
	ALTER TABLE dbo.OperacaoBMF ADD IdCorretora int NULL
	EXEC('UPDATE OperacaoBMF SET IdCorretora = idAgenteCorretora');
	ALTER TABLE OperacaoBMF ALTER COLUMN IdCorretora int NOT NULL;
END
GO	

if not exists(select 1 from syscolumns where id = object_id('OperacaoBMF') and name = 'IdLocalCustodia')
BEGIN
	ALTER TABLE dbo.OperacaoBMF ADD IdLocalCustodia int NULL
	EXEC('UPDATE OperacaoBMF SET IdLocalCustodia = 3');
	ALTER TABLE OperacaoBMF ALTER COLUMN IdLocalCustodia int NOT NULL;
END
GO	

if not exists(select 1 from syscolumns where id = object_id('OperacaoBMF') and name = 'IdLocalNegociacao')
BEGIN
	ALTER TABLE dbo.OperacaoBMF ADD IdLocalNegociacao int NULL
	EXEC('UPDATE OperacaoBMF SET IdLocalNegociacao = 5');
	ALTER TABLE OperacaoBMF ALTER COLUMN IdLocalNegociacao int NOT NULL;
END
GO	

if not exists(select 1 from syscolumns where id = object_id('OperacaoBMF') and name = 'IdClearing')
BEGIN
	ALTER TABLE dbo.OperacaoBMF ADD IdClearing int NULL
	EXEC('UPDATE OperacaoBMF SET IdClearing = 4');
	ALTER TABLE OperacaoBMF ALTER COLUMN IdClearing int NOT NULL;
END
GO	

if not exists(select 1 from syscolumns where id = object_id('OperacaoFundo') and name = 'IdLocalNegociacao')
BEGIN
	ALTER TABLE dbo.OperacaoFundo ADD IdLocalNegociacao int NOT NULL DEFAULT('1')
END
GO	

if not exists(select 1 from syscolumns where id = object_id('OperacaoCotista') and name = 'IdLocalNegociacao')
BEGIN
	ALTER TABLE dbo.OperacaoCotista ADD IdLocalNegociacao int NOT NULL DEFAULT('1')
END
GO	

if not exists(select 1 from syscolumns where id = object_id('OperacaoFundoAux') and name = 'IdLocalNegociacao')
BEGIN
	ALTER TABLE dbo.OperacaoFundoAux ADD IdLocalNegociacao int NOT NULL DEFAULT('1')
END
GO	

if not exists(select 1 from syscolumns where id = object_id('OperacaoCotistaAux') and name = 'IdLocalNegociacao')
BEGIN
	ALTER TABLE dbo.OperacaoCotistaAux ADD IdLocalNegociacao int NOT NULL DEFAULT('1')
END
GO	

if not exists(select 1 from syscolumns where id = object_id('OperacaoRendaFixa') and name = 'IdLocalNegociacao')
BEGIN
	ALTER TABLE dbo.OperacaoRendaFixa ADD IdLocalNegociacao int NULL
	EXEC('UPDATE op SET op.IdLocalNegociacao = (CASE WHEN pap.TipoPapel <> 1 THEN 3 ELSE 2 END)
	FROM OperacaoRendaFixa op
	INNER JOIN TituloRendaFixa tit ON op.IdTitulo = tit.IdTitulo
	INNER JOIN PapelRendafixa pap ON pap.idPapel = tit.idPapel');
	ALTER TABLE OperacaoRendaFixa ALTER COLUMN IdLocalNegociacao int NOT NULL;
END
GO	

if not exists(select 1 from syscolumns where id = object_id('OperacaoRendaFixa') and name = 'IdCorretora')
BEGIN
	ALTER TABLE dbo.OperacaoRendaFixa ADD IdCorretora int NULL
	EXEC('UPDATE OperacaoRendaFixa SET IdCorretora = IdAgenteCorretora');
END
GO	

if not exists(select 1 from syscolumns where id = object_id('OrdemBolsa') and name = 'IdCorretora')
BEGIN
	ALTER TABLE dbo.OrdemBolsa ADD IdCorretora int NULL
	EXEC('UPDATE OrdemBolsa SET IdCorretora = idAgenteCorretora');
	ALTER TABLE OrdemBolsa ALTER COLUMN IdCorretora int NOT NULL;
END
GO	

if not exists(select 1 from syscolumns where id = object_id('OrdemBolsa') and name = 'IdLocalCustodia')
BEGIN
	ALTER TABLE dbo.OrdemBolsa ADD IdLocalCustodia int NULL
	EXEC('UPDATE OrdemBolsa SET IdLocalCustodia = 3');
	ALTER TABLE OrdemBolsa ALTER COLUMN IdLocalCustodia int NOT NULL;
END
GO	

if not exists(select 1 from syscolumns where id = object_id('OrdemBolsa') and name = 'IdLocalNegociacao')
BEGIN
	ALTER TABLE dbo.OrdemBolsa ADD IdLocalNegociacao int NULL
	EXEC('UPDATE OrdemBolsa SET IdLocalNegociacao = 5');
	ALTER TABLE OrdemBolsa ALTER COLUMN IdLocalNegociacao int NOT NULL;
END
GO	

if not exists(select 1 from syscolumns where id = object_id('OrdemBolsa') and name = 'IdClearing')
BEGIN
	ALTER TABLE dbo.OrdemBolsa ADD IdClearing int NULL
	EXEC('UPDATE OrdemBolsa SET IdClearing = 4');
	ALTER TABLE OrdemBolsa ALTER COLUMN IdClearing int NOT NULL;
END
GO	

if not exists(select 1 from syscolumns where id = object_id('OrdemBMF') and name = 'IdCorretora')
BEGIN
	ALTER TABLE dbo.OrdemBMF ADD IdCorretora int NULL
	EXEC('UPDATE OrdemBMF SET IdCorretora = idAgenteCorretora');
	ALTER TABLE OrdemBMF ALTER COLUMN IdCorretora int NOT NULL;
END
GO	

if not exists(select 1 from syscolumns where id = object_id('OrdemBMF') and name = 'IdLocalCustodia')
BEGIN
	ALTER TABLE dbo.OrdemBMF ADD IdLocalCustodia int NULL
	EXEC('UPDATE OrdemBMF SET IdLocalCustodia = 3');
	ALTER TABLE OrdemBMF ALTER COLUMN IdLocalCustodia int NOT NULL;
END
GO	

if not exists(select 1 from syscolumns where id = object_id('OrdemBMF') and name = 'IdLocalNegociacao')
BEGIN
	ALTER TABLE dbo.OrdemBMF ADD IdLocalNegociacao int NULL
	EXEC('UPDATE OrdemBMF SET IdLocalNegociacao = 5');
	ALTER TABLE OrdemBMF ALTER COLUMN IdLocalNegociacao int NOT NULL;
END
GO	

if not exists(select 1 from syscolumns where id = object_id('OrdemBMF') and name = 'IdClearing')
BEGIN
	ALTER TABLE dbo.OrdemBMF ADD IdClearing int NULL
	EXEC('UPDATE OrdemBMF SET IdClearing = 4');
	ALTER TABLE OrdemBMF ALTER COLUMN IdClearing int NOT NULL;
END
GO	

IF EXISTS (select * from sys.columns where Name = N'IdFormaLiquidacao' and Object_ID = Object_ID(N'OperacaoFundo') and is_nullable = 0)
BEGIN
	ALTER TABLE OperacaoFundo Alter Column IdFormaLiquidacao tinyint NULL;	
END
GO	

IF EXISTS (select * from sys.columns where Name = N'IdFormaLiquidacao' and Object_ID = Object_ID(N'OperacaoCotista') and is_nullable = 0)
BEGIN
	ALTER TABLE OperacaoCotista Alter Column IdFormaLiquidacao tinyint NULL;	
END
GO	

if  exists(select 1 from syscolumns where id = object_id('OperacaoRendaFixa') and name = 'IdCorretora')
BEGIN
	ALTER TABLE dbo.OperacaoRendaFixa drop column IdCorretora;
	
END
GO	

if not exists(select 1 from syscolumns where id = object_id('OperacaoRendaFixa') and name = 'IdAgenteCustodia')
BEGIN
	ALTER TABLE dbo.OperacaoRendaFixa ADD IdAgenteCustodia int NULL;
END
GO
	
if  exists(select 1 from syscolumns where id = object_id('OrdemBolsa') and name = 'IdCorretora')
BEGIN
	ALTER TABLE dbo.OrdemBolsa drop column IdCorretora;
END
GO	

if not exists(select 1 from syscolumns where id = object_id('OrdemBolsa') and name = 'IdAgenteCustodia')
BEGIN
	ALTER TABLE dbo.OrdemBolsa ADD IdAgenteCustodia int NULL;
END
GO

if  exists(select 1 from syscolumns where id = object_id('OrdemBMF') and name = 'IdCorretora')
BEGIN
	ALTER TABLE dbo.OrdemBMF drop column IdCorretora;	
END
GO	

if not exists(select 1 from syscolumns where id = object_id('OrdemBMF') and name = 'IdAgenteCustodia')
BEGIN
	ALTER TABLE dbo.OrdemBMF ADD IdAgenteCustodia int NULL;
END
GO

if  exists(select 1 from syscolumns where id = object_id('OperacaoBolsa') and name = 'IdCorretora')
BEGIN
	ALTER TABLE dbo.OperacaoBolsa drop column IdCorretora;	
END
GO	

if not exists(select 1 from syscolumns where id = object_id('OperacaoBolsa') and name = 'IdAgenteCustodia')
BEGIN
	ALTER TABLE dbo.OperacaoBolsa ADD IdAgenteCustodia int NULL;
END
GO

if  exists(select 1 from syscolumns where id = object_id('OperacaoBMF') and name = 'IdCorretora')
BEGIN
	ALTER TABLE dbo.OperacaoBMF drop column IdCorretora;
END
GO

if not exists(select 1 from syscolumns where id = object_id('OperacaoBMF') and name = 'IdAgenteCustodia')
BEGIN
	ALTER TABLE dbo.OperacaoBMF ADD IdAgenteCustodia int NULL;
END
GO

if not exists(select 1 from syscolumns where id = object_id('PosicaoCotista') and name = 'IdSeriesOffShore')
BEGIN
	ALTER TABLE dbo.PosicaoCotista ADD IdSeriesOffShore int NULL;
END
GO	

if not exists(select 1 from syscolumns where id = object_id('PosicaoCotistaHistorico') and name = 'IdSeriesOffShore')
BEGIN
	ALTER TABLE dbo.PosicaoCotistaHistorico ADD IdSeriesOffShore int NULL;
END
GO	

if not exists(select 1 from syscolumns where id = object_id('PosicaoCotistaAbertura') and name = 'IdSeriesOffShore')
BEGIN
	ALTER TABLE dbo.PosicaoCotistaAbertura ADD IdSeriesOffShore int NULL;
END
GO	

if not exists(select 1 from syscolumns where id = object_id('OperacaoCotista') and name = 'IdSeriesOffShore')
BEGIN
	ALTER TABLE dbo.OperacaoCotista ADD IdSeriesOffShore int NULL;
END
GO	

if not exists(select 1 from syscolumns where id = object_id('OperacaoCotistaAux') and name = 'IdSeriesOffShore')
BEGIN
	ALTER TABLE dbo.OperacaoCotistaAux ADD IdSeriesOffShore int NULL;
END
GO	

if not exists(select 1 from syscolumns where id = object_id('PosicaoCotistaAux') and name = 'IdSeriesOffShore')
BEGIN
	ALTER TABLE dbo.PosicaoCotistaAux ADD IdSeriesOffShore int NULL;
END
GO	

if not exists(select 1 from syscolumns where id = object_id('SeriesOffShore') and name = 'PrazoRepeticaoCotas')
BEGIN
	ALTER TABLE dbo.SeriesOffShore ADD PrazoRepeticaoCotas int NULL;	
END
GO

if not exists(select 1 from syscolumns where id = object_id('SeriesOffShore') and name = 'PrazoValidadePrevia')
BEGIN
	ALTER TABLE dbo.SeriesOffShore ADD PrazoValidadePrevia int NULL;	
END
GO	

if not exists(select 1 from syscolumns where id = object_id('SeriesOffShore') and name = 'IdIndicePrevia')
BEGIN
	ALTER TABLE dbo.SeriesOffShore ADD IdIndicePrevia smallint NULL;	
END
GO	

if not exists(select 1 from syscolumns where id = object_id('ClassesOffShore') and name = 'PrazoValidadePrevia')
BEGIN
	ALTER TABLE dbo.ClassesOffShore ADD PrazoValidadePrevia int NULL;	
END
GO	

if not exists(select 1 from syscolumns where id = object_id('ClassesOffShore') and name = 'PrazoRepeticaoCotas')
BEGIN
	ALTER TABLE dbo.ClassesOffShore ADD PrazoRepeticaoCotas int NULL;	
END
GO	

if not exists(select 1 from syscolumns where id = object_id('ClassesOffShore') and name = 'IdIndicePrevia')
BEGIN
	ALTER TABLE dbo.ClassesOffShore ADD IdIndicePrevia smallint NULL;	
END
GO	

if exists(select 1 from syscolumns where id = object_id('OperacaoBolsa') and name = 'IdAgenteCustodia')
BEGIN
	ALTER TABLE OperacaoBolsa DROP COLUMN IdAgenteCustodia;
END
GO

if exists(select 1 from syscolumns where id = object_id('OperacaoBMF') and name = 'IdAgenteCustodia')
BEGIN
	ALTER TABLE OperacaoBMF DROP COLUMN IdAgenteCustodia;
END
GO

if exists(select 1 from syscolumns where id = object_id('OrdemBolsa') and name = 'IdAgenteCustodia')
BEGIN
	ALTER TABLE OrdemBolsa DROP COLUMN IdAgenteCustodia;
END
GO

if exists(select 1 from syscolumns where id = object_id('OrdemBMF') and name = 'IdAgenteCustodia')
BEGIN
	ALTER TABLE OrdemBMF DROP COLUMN IdAgenteCustodia;
END
GO

if not exists(select 1 from syscolumns where id = object_id('OperacaoCotista') and name = 'IdTransferenciaSerie')
BEGIN
	ALTER TABLE dbo.OperacaoCotista ADD IdTransferenciaSerie int NULL;	
END
GO	

IF NOT EXISTS(select 1 from syscolumns where id = object_id('TabelaTaxaAdministracao') and name = 'EscalonaProporcional')
BEGIN
	alter table TabelaTaxaAdministracao add EscalonaProporcional char(1) null
END
GO

IF NOT EXISTS(select 1 from syscolumns where id = object_id('TabelaTaxaAdministracao') and name = 'Consolidadora')
BEGIN
	alter table TabelaTaxaAdministracao add Consolidadora char(1) null
END
GO

IF NOT EXISTS(select 1 from syscolumns where id = object_id('Carteira') and name = 'BuscaCotaAnterior')
BEGIN
	ALTER TABLE dbo.Carteira ADD BuscaCotaAnterior char(1) NOT NULL DEFAULT 'N'
END
GO

IF NOT EXISTS(select 1 from syscolumns where id = object_id('Carteira') and name = 'NumeroDiasBuscaCota')
BEGIN
	ALTER TABLE dbo.Carteira ADD NumeroDiasBuscaCota int
END
GO

IF NOT EXISTS(select 1 from syscolumns where id = object_id('Carteira') and name = 'BandaVariacao')
BEGIN
	ALTER TABLE dbo.Carteira ADD BandaVariacao decimal(16, 2) NULL DEFAULT 0.00
END
GO

IF NOT EXISTS(select 1 from syscolumns where id = object_id('Carteira') and name = 'ExcecaoRegraTxAdm')
BEGIN
	ALTER TABLE dbo.Carteira ADD ExcecaoRegraTxAdm char(1) NOT NULL DEFAULT 'N'
END
GO

IF NOT EXISTS(select 1 from syscolumns where id = object_id('Carteira') and name = 'RebateImpactaPL')
BEGIN
	ALTER TABLE dbo.Carteira ADD RebateImpactaPL char(1) NOT NULL DEFAULT 'N'
END
GO

IF NOT EXISTS(SELECT * FROM sys.columns WHERE NAME = N'CodigoCusip' and Object_ID = Object_ID(N'TituloRendaFixa'))
BEGIN
    ALTER TABLE dbo.TituloRendaFixa	ADD CodigoCusip VARCHAR(9)
END
GO

IF NOT EXISTS(SELECT * FROM sys.columns WHERE NAME = N'CodigoCusip' and Object_ID = Object_ID(N'AtivoBMF'))
BEGIN
    ALTER TABLE dbo.AtivoBMF ADD CodigoCusip varchar(9)
END
GO

IF NOT EXISTS(SELECT * FROM sys.columns WHERE NAME = N'CodigoCusip' and Object_ID = Object_ID(N'AtivoBolsa'))
BEGIN
    ALTER TABLE dbo.AtivoBolsa ADD CodigoCusip VARCHAR(9)	
END
GO

IF NOT EXISTS(SELECT * FROM sys.columns WHERE NAME = N'CodigoIsin' and Object_ID = Object_ID(N'ClienteInterface'))
BEGIN
    ALTER TABLE dbo.ClienteInterface ADD CodigoIsin varchar(12)
END
GO

IF NOT EXISTS(SELECT * FROM sys.columns WHERE NAME = N'CodigoCusip' and Object_ID = Object_ID(N'ClienteInterface'))
BEGIN
	ALTER TABLE dbo.ClienteInterface ADD CodigoCusip varchar(9)
END
GO

IF NOT EXISTS(SELECT * FROM sys.columns WHERE NAME = N'EmpSecuritizada' and Object_ID = Object_ID(N'AgenteMercado'))
BEGIN
    ALTER TABLE AgenteMercado ADD EmpSecuritizada CHAR;
	EXEC('Update AgenteMercado set EmpSecuritizada = ''S''');
END
GO

IF NOT EXISTS(SELECT * FROM sys.columns WHERE NAME = N'IsentoIR' and Object_ID = Object_ID(N'PapelRendaFixa'))
BEGIN
    ALTER TABLE PapelRendaFixa ADD IsentoIR int
END
GO

IF EXISTS(SELECT * FROM sys.columns WHERE NAME = N'IdMercadoTipoPessoa' and Object_ID = Object_ID(N'CadastroComplementar'))
BEGIN
	ALTER TABLE CadastroComplementar ALTER COLUMN IdMercadoTipoPessoa varchar(20)
END
GO	

IF NOT EXISTS(SELECT * FROM sys.columns  WHERE NAME = N'RegimeEspecialTributacao' and Object_ID = Object_ID(N'Cliente'))
BEGIN
    ALTER TABLE Cliente ADD RegimeEspecialTributacao CHAR
END
GO

if not exists(select 1 from syscolumns where id = object_id('EventoFundoMovimentoAtivos') and name = 'IdCliente')
BEGIN
	ALTER TABLE EventoFundoMovimentoAtivos ADD IdCliente INT NOT NULL;
END
GO 

if not exists(select 1 from syscolumns where id = object_id('EventoFundoMovimentoCautelas') and name = 'IdCliente')
BEGIN
	ALTER TABLE EventoFundoMovimentoCautelas ADD IdCliente INT NOT NULL;
END
GO 

if not exists(select 1 from syscolumns where id = object_id('EventoFundoPosicaoAtivos') and name = 'IdCliente')
BEGIN
	ALTER TABLE EventoFundoPosicaoAtivos ADD IdCliente INT NOT NULL;
END
GO 

if not exists(select 1 from syscolumns where id = object_id('EventoFundoPosicaoCautelas') and name = 'IdCliente')
BEGIN
	ALTER TABLE EventoFundoPosicaoCautelas ADD IdCliente INT NOT NULL;
END
GO 

IF EXISTS(select 1 from syscolumns where id = object_id('ParametroAdministradorFundoInvestimento') and name = 'DataRecolhimento')
BEGIN
    ALTER TABLE ParametroAdministradorFundoInvestimento
	DROP COLUMN DataRecolhimento 
END
GO

IF NOT EXISTS(select 1 from syscolumns where id = object_id('EventoFundo') and name = 'ExecucaoRecolhimento')
BEGIN
	alter table EventoFundo add ExecucaoRecolhimento int null
END
GO

IF NOT EXISTS(select 1 from syscolumns where id = object_id('EventoFundo') and name = 'AliquotaIR')
BEGIN
	alter table EventoFundo add AliquotaIR int null
END
GO

IF NOT EXISTS(select 1 from syscolumns where id = object_id('EventoFundo') and name = 'DataRecolhimento')
BEGIN
	alter table EventoFundo add DataRecolhimento DateTime null
END
GO

IF NOT EXISTS(select 1 from syscolumns where id = object_id('ParametroAdministradorFundoInvestimento') and name = 'AliquotaIR')
BEGIN
	ALTER TABLE ParametroAdministradorFundoInvestimento ALTER COLUMN AliquotaIR int NULL
END
GO

IF NOT EXISTS(SELECT * FROM sys.columns WHERE NAME = N'TipoFundo' and Object_ID = Object_ID(N'Carteira'))
BEGIN
	ALTER TABLE Carteira ADD TipoFundo int not null DEFAULT 1
END
GO

IF EXISTS(SELECT * FROM sys.columns WHERE NAME = N'FundoInvestimento' and Object_ID = Object_ID(N'FundoInvestimentoFormaCondominio'))
BEGIN
	exec sp_rename 'FundoInvestimentoFormaCondominio.FundoInvestimento', 'IdCarteira'
END
GO

IF EXISTS(SELECT * FROM sys.columns WHERE NAME = N'FundoInvestimento' and Object_ID = Object_ID(N'DesenquadramentoTributario'))
BEGIN
	exec sp_rename 'DesenquadramentoTributario.FundoInvestimento', 'IdCarteira'	
END
GO

IF EXISTS(select 1 from syscolumns where id = object_id('PadraoNotas') and name = 'DataFinal')
BEGIN
	ALTER TABLE PadraoNotas DROP COLUMN DataFinal
END
GO

IF EXISTS(select 1 from syscolumns where id = object_id('PadraoNotas') and name = 'DataInicial')
BEGIN
	exec sp_rename 'PadraoNotas.DataInicial', 'DataVigencia'
END
GO

IF EXISTS(select 1 from syscolumns where id = object_id('PosicaoRendaFixa') and name = 'IdOperacao')
BEGIN
	alter table PosicaoRendaFixa alter column IdOperacao int NULL;
END
GO

IF EXISTS(select 1 from syscolumns where id = object_id('PosicaoSwap') and name = 'IdOperacao')
BEGIN
	alter table PosicaoSwap alter column IdOperacao int NULL;
END
GO	

IF EXISTS(select 1 from syscolumns where id = object_id('PosicaoFundo') and name = 'IdOperacao')
BEGIN
	alter table PosicaoFundo alter column IdOperacao int NULL;
END
GO	

IF EXISTS(select 1 from syscolumns where id = object_id('PosicaoCotista') and name = 'IdOperacao')
BEGIN
	alter table PosicaoCotista alter column IdOperacao int NULL;
END
GO

IF NOT EXISTS(SELECT * FROM sys.columns WHERE NAME = N'AliquotaIR' and Object_ID = Object_ID(N'AgendaComeCotas'))
BEGIN
	ALTER TABLE AgendaComeCotas ADD AliquotaIR int NOT NULL default 0
END
GO

IF NOT EXISTS(SELECT * FROM sys.columns WHERE NAME = N'TipoPosicao' and Object_ID = Object_ID(N'AgendaComeCotas'))
BEGIN
	ALTER TABLE AgendaComeCotas ADD TipoPosicao int
END
GO

if not exists(select 1 from syscolumns where id = object_id('Cotista') and name = 'IdPessoa')
BEGIN
	ALTER TABLE Cotista ADD IdPessoa int;
	EXEC('UPDATE Cotista SET IdPessoa = IdCotista')
END
GO

if not exists(select 1 from syscolumns where id = object_id('Cliente') and name = 'IdPessoa')
BEGIN
	ALTER TABLE Cliente ADD IdPessoa int;
	EXEC('UPDATE Cliente SET IdPessoa = IdCliente')
END
GO	

if not exists(select 1 from syscolumns where id = object_id('Cotista') and name = 'IdCarteira')
BEGIN
	ALTER TABLE Cotista ADD IdCarteira INT NULL;
	
	EXEC('BEGIN TRANSACTION T1; update Cotista
							set Cotista.IdCarteira = OperacaoCotista.IdCarteira 
							from OperacaoCotista 
							inner join Cotista on (OperacaoCotista.IdCotista = Cotista.IdCotista) 
							where OperacaoCotista.IdCotista = OperacaoCotista.IdCarteira 
									and OperacaoCotista.IdCarteira in(select IdCarteira 
																      from Carteira 
																	  left join Cliente on(Carteira.IdCarteira = Cliente.IdCliente) 
																	  where Cliente.TipoControle != 1 and Cliente.StatusAtivo = 1) 
	  COMMIT TRANSACTION T1;');

END
GO	

if not exists(select 1 from syscolumns where id = object_id('SeriesOffShore') and name = 'DataInicioAplicacao')
BEGIN
	ALTER TABLE SeriesOffShore ADD DataInicioAplicacao datetime NULL 
END
GO	

if not exists(select 1 from syscolumns where id = object_id('SeriesOffShore') and name = 'DataFimAplicacao')
BEGIN
	ALTER TABLE SeriesOffShore ADD DataFimAplicacao datetime NULL 
END
GO	

if not exists(select 1 from syscolumns where id = object_id('SeriesOffShore') and name = 'SerieBase')
BEGIN
	ALTER TABLE SeriesOffShore ADD SerieBase char(1) NULL
END
GO	

IF EXISTS(select 1 from syscolumns where id = object_id('Pessoa') and name = 'Tipo')
BEGIN
	ALTER TABLE Pessoa ALTER COLUMN Tipo tinyint NULL;
END
GO

if not exists(select 1 from syscolumns where id = object_id('OperacaoCotista') and name = 'IdContaCorrenteOrigem')
BEGIN
	ALTER TABLE OperacaoCotista ADD IdContaCorrenteOrigem int NULL
END
GO

if not exists(select 1 from syscolumns where id = object_id('OperacaoCotista') and name = 'IdContaCorrenteDestino')
BEGIN
	ALTER TABLE OperacaoCotista ADD IdContaCorrenteDestino int NULL
END
GO	

if not exists(select 1 from syscolumns where id = object_id('OperacaoCotista') and name = 'ValorDespesas')
BEGIN
	ALTER TABLE OperacaoCotista ADD ValorDespesas decimal(16,2) NULL
END
GO	

if not exists(select 1 from syscolumns where id = object_id('OperacaoCotista') and name = 'ValorTaxas')
BEGIN
	ALTER TABLE OperacaoCotista ADD ValorTaxas decimal(16,2) NULL
END
GO	

if not exists(select 1 from syscolumns where id = object_id('OperacaoCotista') and name = 'ValorTributos')
BEGIN
	ALTER TABLE OperacaoCotista ADD ValorTributos decimal(16,2) NULL
END
GO	

if not exists(select 1 from syscolumns where id = object_id('OperacaoCotista') and name = 'PenalidadeLockUp')
BEGIN
	ALTER TABLE OperacaoCotista ADD PenalidadeLockUp decimal(16,2) NULL
END
GO	

if not exists(select 1 from syscolumns where id = object_id('OperacaoCotista') and name = 'ValorHoldBack')
BEGIN
	ALTER TABLE OperacaoCotista ADD ValorHoldBack decimal(16,2) NULL
END
GO	

IF NOT EXISTS (select 1 from sysconstraints where object_name(id) = 'TaxaGlobal' and object_name(constid) = 'TaxaGlobal_TabelaTaxaAdministracao_FK1')
BEGIN
	ALTER TABLE TaxaGlobal  WITH CHECK ADD  CONSTRAINT TaxaGlobal_TabelaTaxaAdministracao_FK1 FOREIGN KEY(IdTabelaGlobal)
	REFERENCES TabelaTaxaAdministracao (IdTabela)
END
GO

IF NOT EXISTS (select 1 from sysconstraints where object_name(id) = 'TaxaGlobal' and object_name(constid) = 'TaxaGlobal_TabelaTaxaAdministracao_FK2')
BEGIN
	ALTER TABLE TaxaGlobal  WITH CHECK ADD  CONSTRAINT TaxaGlobal_TabelaTaxaAdministracao_FK2 FOREIGN KEY(IdTabelaAssociada)
	REFERENCES TabelaTaxaAdministracao (IdTabela)
END
GO

IF NOT EXISTS (select 1 from sysconstraints where object_name(id) = 'EventoFundoBolsa' and object_name(constid) = 'EventoFundoBolsa_EventoFundo_FK1')
BEGIN
	ALTER TABLE EventoFundoBolsa  WITH CHECK ADD  CONSTRAINT EventoFundoBolsa_EventoFundo_FK1 FOREIGN KEY(IdEventoFundo)
	REFERENCES EventoFundo (IdEventoFundo)
END
GO

IF NOT EXISTS (select 1 from sysconstraints where object_name(id) = 'EventoFundoBMF' and object_name(constid) = 'EventoFundoBMF_EventoFundo_FK1')
BEGIN
	ALTER TABLE EventoFundoBMF  WITH CHECK ADD  CONSTRAINT EventoFundoBMF_EventoFundo_FK1 FOREIGN KEY(IdEventoFundo)
	REFERENCES EventoFundo (IdEventoFundo)
END
GO

IF EXISTS(SELECT 1 FROM SYSCOLUMNS WHERE ID = OBJECT_ID('OperacaoBolsa') AND NAME = 'DataRegistro')
BEGIN
	ALTER TABLE OperacaoBolsa DROP COLUMN DataRegistro
END
GO

IF NOT EXISTS(select 1 from syscolumns where id = object_id('OperacaoBolsa') and name = 'DataOperacao')
BEGIN    

	alter table OperacaoBolsa add DataOperacao Datetime;
    
	EXEC sp_executesql
            N'update OperacaoBolsa set DataOperacao = Data'

	alter table OperacaoBolsa alter column DataOperacao datetime not null;
	
END
GO

IF EXISTS(SELECT 1 FROM SYSCOLUMNS WHERE ID = OBJECT_ID('OperacaoBMF') AND NAME = 'DataRegistro')
BEGIN
	ALTER TABLE OperacaoBMF
	DROP COLUMN DataRegistro
END
GO

IF NOT EXISTS(select 1 from syscolumns where id = object_id('OperacaoBMF') and name = 'DataOperacao')
BEGIN    

	alter table OperacaoBMF add DataOperacao Datetime;
    
	EXEC sp_executesql
            N'update OperacaoBMF set DataOperacao = Data'

	alter table OperacaoBMF alter column DataOperacao datetime not null;
	
END
GO

IF NOT EXISTS(SELECT 1 FROM SYSCOLUMNS WHERE ID= OBJECT_ID('OperacaoFundo') AND NAME = 'DataRegistro')
BEGIN
	ALTER TABLE OperacaoFundo add DataRegistro Datetime;

	EXEC sp_executesql
            N'update OperacaoFundo set DataRegistro = DataOperacao'

	alter table OperacaoFundo alter column DataRegistro datetime not null;
END
GO


IF NOT EXISTS(SELECT 1 FROM SYSCOLUMNS WHERE ID=OBJECT_ID('OperacaoFundo') AND NAME = 'DepositoRetirada')
BEGIN
	ALTER TABLE OperacaoFundo add DepositoRetirada bit not null default 0;
END
GO

IF NOT EXISTS(SELECT 1 FROM SYSCOLUMNS WHERE ID= OBJECT_ID('OrdemBolsa') AND NAME = 'DataOperacao')
BEGIN
	ALTER TABLE OrdemBolsa add DataOperacao Datetime;

	EXEC sp_executesql
            N'update OrdemBolsa set DataOperacao = Data'

	ALTER TABLE OrdemBolsa ALTER COLUMN DataOperacao Datetime NOT NULL ;
END
GO

IF NOT EXISTS(SELECT 1 FROM SYSCOLUMNS WHERE ID= OBJECT_ID('OrdemBMF') AND NAME = 'DataOperacao')
BEGIN
	ALTER TABLE OrdemBmf add DataOperacao Datetime;

	EXEC sp_executesql
            N'update OrdemBmf set DataOperacao = Data'

	ALTER TABLE OrdemBmf ALTER COLUMN DataOperacao Datetime NOT NULL ;
END
GO

IF NOT EXISTS(SELECT 1 FROM SYSCOLUMNS WHERE ID= OBJECT_ID('OperacaoCotista') AND NAME = 'DataRegistro')
BEGIN
	ALTER TABLE OperacaoCotista add DataRegistro Datetime;

	EXEC sp_executesql
            N'update OperacaoCotista set DataRegistro = DataOperacao'

	ALTER TABLE OperacaoCotista ALTER COLUMN DataRegistro Datetime NOT NULL ;
END
GO

IF NOT EXISTS(SELECT 1 FROM SYSCOLUMNS WHERE ID=OBJECT_ID('OperacaoCotista') AND NAME = 'DepositoRetirada')
BEGIN
	ALTER TABLE OperacaoCotista add DepositoRetirada bit not null default 0;
END
GO

if not exists(select 1 from syscolumns where id = object_id('ClassesOffShore') and name = 'QtdDiasConvAplicacao')
BEGIN
 ALTER TABLE ClassesOffShore ADD QtdDiasConvAplicacao int NULL
END
GO 

if not exists(select 1 from syscolumns where id = object_id('ClassesOffShore') and name = 'QtdDiasLiqFinAplic')
BEGIN
 ALTER TABLE ClassesOffShore ADD QtdDiasLiqFinAplic int NULL
END
GO 

if not exists(select 1 from syscolumns where id = object_id('OperacaoCotista') and name = 'IdContaCorrente')
BEGIN
 ALTER TABLE OperacaoCotista ADD IdContaCorrente int NULL;
END
GO 

IF NOT EXISTS (select 1 from sysconstraints where object_name(id) = 'CurvaRendaFixa' and object_name(constid) = 'CurvaRF_CurvaBase_FK')
BEGIN		
	alter table CurvaRendaFixa add constraint CurvaRF_CurvaBase_FK FOREIGN KEY ( IdCurvaBase ) references CurvaRendaFixa(IdCurvaRendaFixa)
END
GO
	
IF NOT EXISTS (select 1 from sysconstraints where object_name(id) = 'CurvaRendaFixa' and object_name(constid) = 'CurvaRF_CurvaSpread_FK')
BEGIN	
	alter table CurvaRendaFixa add constraint CurvaRF_CurvaSpread_FK FOREIGN KEY ( IdCurvaSpread ) references CurvaRendaFixa(IdCurvaRendaFixa)
END
GO
	
IF NOT EXISTS (select 1 from sysconstraints where object_name(id) = 'CurvaRendaFixa' and object_name(constid) = 'Curva_Indice_FK')
BEGIN	
	alter table CurvaRendaFixa add constraint Curva_Indice_FK FOREIGN KEY ( IdIndice ) references Indice(IdIndice);	
END
GO
		
IF NOT EXISTS (select 1 from sysconstraints where object_name(id) = 'TaxaCurva' and object_name(constid) = 'TaxaCurva_CurvaRF_FK')
BEGIN	
	alter table TaxaCurva add constraint TaxaCurva_CurvaRF_FK FOREIGN KEY ( IdCurvaRendaFixa ) references CurvaRendaFixa(IdCurvaRendaFixa);
END
GO
		
IF NOT EXISTS (select 1 from sysconstraints where object_name(id) = 'Indice' and object_name(constid) = 'Indice_Feeder_FK')
BEGIN
	alter table Indice add constraint Indice_Feeder_FK FOREIGN KEY ( IdFeeder ) references Feeder(IdFeeder);
END
GO

if not exists(select 1 from syscolumns where id = object_id('SerieRendaFixa') and name = 'IdFeeder')
BEGIN	
	ALTER TABLE SerieRendaFixa ADD IdFeeder	INT NOT NULL default '5';
END 	
GO
	
IF NOT EXISTS (select 1 from sysconstraints where object_name(id) = 'SerieRendaFixa' and object_name(constid) = 'SerieRF_Feeder_FK')
BEGIN
	alter table SerieRendaFixa add constraint SerieRF_Feeder_FK FOREIGN KEY ( IdFeeder ) references Feeder(IdFeeder)
END
GO	

IF NOT EXISTS (select 1 from sysconstraints where object_name(id) = 'PerfilMTM' and object_name(constid) = 'PerfilMTM_Papel_FK')
BEGIN
	alter table PerfilMTM add constraint PerfilMTM_Papel_FK FOREIGN KEY ( IdPapel ) references PapelRendaFixa(IdPapel);
END
GO
	
IF NOT EXISTS (select 1 from sysconstraints where object_name(id) = 'PerfilMTM' and object_name(constid) = 'PerfilMTM_Serie_FK')
BEGIN	
	alter table PerfilMTM add constraint PerfilMTM_Serie_FK FOREIGN KEY ( IdSerie ) references SerieRendaFixa(IdSerie);
END
GO
		
IF NOT EXISTS (select 1 from sysconstraints where object_name(id) = 'PerfilMTM' and object_name(constid) = 'PerfilMTM_Titulo_FK')
BEGIN	
	alter table PerfilMTM add constraint PerfilMTM_Titulo_FK FOREIGN KEY ( IdTitulo ) references TituloRendaFixa(IdTitulo);
END
GO
		
IF NOT EXISTS (select 1 from sysconstraints where object_name(id) = 'PerfilMTM' and object_name(constid) = 'PerfilMTM_Operacao_FK')
BEGIN	
	alter table PerfilMTM add constraint PerfilMTM_Operacao_FK FOREIGN KEY ( IdOperacao ) references OperacaoRendaFixa(IdOperacao)
END
GO
		
IF EXISTS (select 1 from sysconstraints where object_name(id) = 'TituloRendaFixa' and object_name(constid) = 'SerieRendaFixa_TituloRendaFixa_FK1')
BEGIN	
	ALTER TABLE TituloRendaFixa DROP CONSTRAINT SerieRendaFixa_TituloRendaFixa_FK1;
END
GO	

IF NOT EXISTS (select 1 from sysconstraints where object_name(id) = 'MemoriaCalculoRendaFixa' and object_name(constid) = 'MemCalRF_Operacao_FK')
BEGIN
	alter table MemoriaCalculoRendaFixa add constraint MemCalRF_Operacao_FK FOREIGN KEY ( IdOperacao ) references OperacaoRendaFixa(IdOperacao);
END
GO

--Exclui linhas sem referencia
if exists (select 1 from DePara)
Begin
	truncate table DEPARA;
	delete from TipoDePara;

	delete DEPARA WHERE IDTIPODEPARA IN(SELECT IDTIPODEPARA FROM TIPODEPARA WHERE DESCRICAO = 'SituacaoLancamentoLiquidacao'); delete TipoDePara where descricao = 'SituacaoLancamentoLiquidacao' ; insert into TipoDepara(TipoCampo,Descricao,Observacao,EnumTipoDePara) values(1,'SituacaoLancamentoLiquidacao','Tabela Liquidacao',null)
	delete DEPARA WHERE IDTIPODEPARA IN(SELECT IDTIPODEPARA FROM TIPODEPARA WHERE DESCRICAO = 'FonteLancamentoLiquidacao'); delete TipoDePara where descricao = 'FonteLancamentoLiquidacao' ; insert into TipoDepara(TipoCampo,Descricao,Observacao,EnumTipoDePara) values(1,'FonteLancamentoLiquidacao','Tabela Liquidacao',null)
	delete DEPARA WHERE IDTIPODEPARA IN(SELECT IDTIPODEPARA FROM TIPODEPARA WHERE DESCRICAO = 'Origem_MT_CAIXA'); delete TipoDePara where descricao = 'Origem_MT_CAIXA' ; insert into TipoDepara(TipoCampo,Descricao,Observacao,EnumTipoDePara) values(1,'Origem_MT_CAIXA','Tabela Liquidacao',null)
	delete DEPARA WHERE IDTIPODEPARA IN(SELECT IDTIPODEPARA FROM TIPODEPARA WHERE DESCRICAO = 'Tipo_MT_CAIXA'); delete TipoDePara where descricao = 'Tipo_MT_CAIXA' ; insert into TipoDepara(TipoCampo,Descricao,Observacao,EnumTipoDePara) values(1,'Tipo_MT_CAIXA','Tabela Liquidacao',null)
	delete DEPARA WHERE IDTIPODEPARA IN(SELECT IDTIPODEPARA FROM TIPODEPARA WHERE DESCRICAO = 'Tipo Operacao Fundo'); delete TipoDePara where descricao = 'Tipo Operacao Fundo' ; insert into TipoDepara(TipoCampo,Descricao,Observacao,EnumTipoDePara) values(1,'Tipo Operacao Fundo','Tabela OperacaoFundo',null)
	delete DEPARA WHERE IDTIPODEPARA IN(SELECT IDTIPODEPARA FROM TIPODEPARA WHERE DESCRICAO = 'TipoTributacaoFundo'); delete TipoDePara where descricao = 'TipoTributacaoFundo' ; insert into TipoDepara(TipoCampo,Descricao,Observacao,EnumTipoDePara) values(1,'TipoTributacaoFundo','Tabela Carteira',null)
	delete DEPARA WHERE IDTIPODEPARA IN(SELECT IDTIPODEPARA FROM TIPODEPARA WHERE DESCRICAO = 'TipoCarteiraFundo'); delete TipoDePara where descricao = 'TipoCarteiraFundo' ; insert into TipoDepara(TipoCampo,Descricao,Observacao,EnumTipoDePara) values(1,'TipoCarteiraFundo','Tabela Carteira',null)
	delete DEPARA WHERE IDTIPODEPARA IN(SELECT IDTIPODEPARA FROM TIPODEPARA WHERE DESCRICAO = 'TipoOperacaoTitulo'); delete TipoDePara where descricao = 'TipoOperacaoTitulo' ; insert into TipoDepara(TipoCampo,Descricao,Observacao,EnumTipoDePara) values(1,'TipoOperacaoTitulo','Tabela OperacaoRendaFixa',null)
	delete DEPARA WHERE IDTIPODEPARA IN(SELECT IDTIPODEPARA FROM TIPODEPARA WHERE DESCRICAO = 'PontaSwap'); delete TipoDePara where descricao = 'PontaSwap' ; insert into TipoDepara(TipoCampo,Descricao,Observacao,EnumTipoDePara) values(1,'PontaSwap','Tabela OperacaoSwap',null)
	delete DEPARA WHERE IDTIPODEPARA IN(SELECT IDTIPODEPARA FROM TIPODEPARA WHERE DESCRICAO = 'TipoMercadoAtivoBolsa'); delete TipoDePara where descricao = 'TipoMercadoAtivoBolsa' ; insert into TipoDepara(TipoCampo,Descricao,Observacao,EnumTipoDePara) values(1,'TipoMercadoAtivoBolsa','Tabela AtivoBolsa',null)
	delete DEPARA WHERE IDTIPODEPARA IN(SELECT IDTIPODEPARA FROM TIPODEPARA WHERE DESCRICAO = 'PontaEmprestimoBolsa'); delete TipoDePara where descricao = 'PontaEmprestimoBolsa' ; insert into TipoDepara(TipoCampo,Descricao,Observacao,EnumTipoDePara) values(1,'PontaEmprestimoBolsa','Tabela  PosicaoEmprestimoBolsaHistorico',null)
	delete DEPARA WHERE IDTIPODEPARA IN(SELECT IDTIPODEPARA FROM TIPODEPARA WHERE DESCRICAO = 'TipoApropriacaoSwap'); delete TipoDePara where descricao = 'TipoApropriacaoSwap' ; insert into TipoDepara(TipoCampo,Descricao,Observacao,EnumTipoDePara) values(1,'TipoApropriacaoSwap','Tipo de curva usada (exponencial / Linear)',null)
	delete DEPARA WHERE IDTIPODEPARA IN(SELECT IDTIPODEPARA FROM TIPODEPARA WHERE DESCRICAO = 'ContagemDiasSwap'); delete TipoDePara where descricao = 'ContagemDiasSwap' ; insert into TipoDepara(TipoCampo,Descricao,Observacao,EnumTipoDePara) values(1,'ContagemDiasSwap','Dias úteis ou corridos',null)
	delete DEPARA WHERE IDTIPODEPARA IN(SELECT IDTIPODEPARA FROM TIPODEPARA WHERE DESCRICAO = 'TipoAtivoAuxiliar'); delete TipoDePara where descricao = 'TipoAtivoAuxiliar' ; insert into TipoDepara(TipoCampo,Descricao,Observacao,EnumTipoDePara) values(1,'TipoAtivoAuxiliar','Tabela de Prazo Médio',null)
	delete DEPARA WHERE IDTIPODEPARA IN(SELECT IDTIPODEPARA FROM TIPODEPARA WHERE DESCRICAO = 'TipoOperacaoCotista'); delete TipoDePara where descricao = 'TipoOperacaoCotista' ; insert into TipoDepara(TipoCampo,Descricao,Observacao,EnumTipoDePara) values(1,'TipoOperacaoCotista','Tipo de Operações do Cotista',null)
	delete DEPARA WHERE IDTIPODEPARA IN(SELECT IDTIPODEPARA FROM TIPODEPARA WHERE DESCRICAO = 'TipoOperacaoCotistaNota'); delete TipoDePara where descricao = 'TipoOperacaoCotistaNota' ; insert into TipoDepara(TipoCampo,Descricao,Observacao,EnumTipoDePara) values(1,'TipoOperacaoCotistaNota','Tipo de Operações do Cotista resgate por nota,',null)
	delete DEPARA WHERE IDTIPODEPARA IN(SELECT IDTIPODEPARA FROM TIPODEPARA WHERE DESCRICAO = 'TipoResgateCotista'); delete TipoDePara where descricao = 'TipoResgateCotista' ; insert into TipoDepara(TipoCampo,Descricao,Observacao,EnumTipoDePara) values(1,'TipoResgateCotista','Tipo de Resgate de Cotista',null)
	delete DEPARA WHERE IDTIPODEPARA IN(SELECT IDTIPODEPARA FROM TIPODEPARA WHERE DESCRICAO = 'ListaCadastro'); delete TipoDePara where descricao = 'ListaCadastro' ; insert into TipoDepara(TipoCampo,Descricao,Observacao,EnumTipoDePara) values(1,'ListaCadastro','Links dos cadastros complementares',null)
	delete DEPARA WHERE IDTIPODEPARA IN(SELECT IDTIPODEPARA FROM TIPODEPARA WHERE DESCRICAO = 'TipoOperacaoBolsa'); delete TipoDePara where descricao = 'TipoOperacaoBolsa' ; insert into TipoDepara(TipoCampo,Descricao,Observacao,EnumTipoDePara) values(1,'TipoOperacaoBolsa','Lista dos tipos de operaçao',null)
	delete DEPARA WHERE IDTIPODEPARA IN(SELECT IDTIPODEPARA FROM TIPODEPARA WHERE DESCRICAO = 'TipoDivulgacaoIndice'); delete TipoDePara where descricao = 'TipoDivulgacaoIndice' ; insert into TipoDepara(TipoCampo,Descricao,Observacao,EnumTipoDePara) values(1,'TipoDivulgacaoIndice','Tipo de Divulgação do Indice D, M',null)
	delete DEPARA WHERE IDTIPODEPARA IN(SELECT IDTIPODEPARA FROM TIPODEPARA WHERE DESCRICAO = 'TipoIndice'); delete TipoDePara where descricao = 'TipoIndice' ; insert into TipoDepara(TipoCampo,Descricao,Observacao,EnumTipoDePara) values(1,'TipoIndice','Tipo do Indice Percentual ou NroIndice',null)
	delete DEPARA WHERE IDTIPODEPARA IN(SELECT IDTIPODEPARA FROM TIPODEPARA WHERE DESCRICAO = 'TipoPapelAtivo'); delete TipoDePara where descricao = 'TipoPapelAtivo' ; insert into TipoDepara(TipoCampo,Descricao,Observacao,EnumTipoDePara) values(1,'TipoPapelAtivo','Tipo papel Ativo',null)
	delete DEPARA WHERE IDTIPODEPARA IN(SELECT IDTIPODEPARA FROM TIPODEPARA WHERE DESCRICAO = 'ClassificacaoCVMFundo'); delete TipoDePara where descricao = 'ClassificacaoCVMFundo' ; insert into TipoDepara(TipoCampo,Descricao,Observacao,EnumTipoDePara) values(1,'ClassificacaoCVMFundo','Classificação CVM do fundo',null)
	delete DEPARA WHERE IDTIPODEPARA IN(SELECT IDTIPODEPARA FROM TIPODEPARA WHERE DESCRICAO = 'ClassePapelRendaFixa'); delete TipoDePara where descricao = 'ClassePapelRendaFixa' ; insert into TipoDepara(TipoCampo,Descricao,Observacao,EnumTipoDePara) values(1,'ClassePapelRendaFixa','Classe Papel Renda Fixa',null)
	delete DEPARA WHERE IDTIPODEPARA IN(SELECT IDTIPODEPARA FROM TIPODEPARA WHERE DESCRICAO = 'TipoClassificacaoCliente'); delete TipoDePara where descricao = 'TipoClassificacaoCliente' ; insert into TipoDepara(TipoCampo,Descricao,Observacao,EnumTipoDePara) values(1,'TipoClassificacaoCliente','Tipo Classificacao Cliente',null)
	delete DEPARA WHERE IDTIPODEPARA IN(SELECT IDTIPODEPARA FROM TIPODEPARA WHERE DESCRICAO = 'TipoMovimentoRF'); delete TipoDePara where descricao = 'TipoMovimentoRF' ; insert into TipoDepara(TipoCampo,Descricao,Observacao,EnumTipoDePara) values(1,'TipoMovimentoRF','Tipo de Movimentação Renda Fixa',null)
	delete DEPARA WHERE IDTIPODEPARA IN(SELECT IDTIPODEPARA FROM TIPODEPARA WHERE DESCRICAO = 'CategoriaMovimentoRF'); delete TipoDePara where descricao = 'CategoriaMovimentoRF' ; insert into TipoDepara(TipoCampo,Descricao,Observacao,EnumTipoDePara) values(1,'CategoriaMovimentoRF','Categoria de Movimento de Renda Fixa',null)
	delete DEPARA WHERE IDTIPODEPARA IN(SELECT IDTIPODEPARA FROM TIPODEPARA WHERE DESCRICAO = 'TipoOperacaoBolsa'); delete TipoDePara where descricao = 'TipoOperacaoBolsa' ; insert into TipoDepara(TipoCampo,Descricao,Observacao,EnumTipoDePara) values(1,'TipoOperacaoBolsa','Tipo Operação Bolsa',null)
	delete DEPARA WHERE IDTIPODEPARA IN(SELECT IDTIPODEPARA FROM TIPODEPARA WHERE DESCRICAO = 'TipoPontaEA'); delete TipoDePara where descricao = 'TipoPontaEA' ; insert into TipoDepara(TipoCampo,Descricao,Observacao,EnumTipoDePara) values(1,'TipoPontaEA','Tipo Ponta Emprestimo de Ações',null)
	delete DEPARA WHERE IDTIPODEPARA IN(SELECT IDTIPODEPARA FROM TIPODEPARA WHERE DESCRICAO = 'TipoEA'); delete TipoDePara where descricao = 'TipoEA' ; insert into TipoDepara(TipoCampo,Descricao,Observacao,EnumTipoDePara) values(1,'TipoEA','Tipo Emprestimo de Ações',null)
	delete DEPARA WHERE IDTIPODEPARA IN(SELECT IDTIPODEPARA FROM TIPODEPARA WHERE DESCRICAO = 'OrigemOperacaoBolsa'); delete TipoDePara where descricao = 'OrigemOperacaoBolsa' ; insert into TipoDepara(TipoCampo,Descricao,Observacao,EnumTipoDePara) values(1,'OrigemOperacaoBolsa','Origem Operação Bolsa',null)
	delete DEPARA WHERE IDTIPODEPARA IN(SELECT IDTIPODEPARA FROM TIPODEPARA WHERE DESCRICAO = 'TipoPontaSwap'); delete TipoDePara where descricao = 'TipoPontaSwap' ; insert into TipoDepara(TipoCampo,Descricao,Observacao,EnumTipoDePara) values(1,'TipoPontaSwap','Tipo Ponta SWAP',null)
	delete DEPARA WHERE IDTIPODEPARA IN(SELECT IDTIPODEPARA FROM TIPODEPARA WHERE DESCRICAO = 'TipoApropriacaoSwap'); delete TipoDePara where descricao = 'TipoApropriacaoSwap' ; insert into TipoDepara(TipoCampo,Descricao,Observacao,EnumTipoDePara) values(1,'TipoApropriacaoSwap','Tipo Apropriação SWAP',null)
	delete DEPARA WHERE IDTIPODEPARA IN(SELECT IDTIPODEPARA FROM TIPODEPARA WHERE DESCRICAO = 'ContagemDiasSwap'); delete TipoDePara where descricao = 'ContagemDiasSwap' ; insert into TipoDepara(TipoCampo,Descricao,Observacao,EnumTipoDePara) values(1,'ContagemDiasSwap','Contagem de Dias Swap',null)
	delete DEPARA WHERE IDTIPODEPARA IN(SELECT IDTIPODEPARA FROM TIPODEPARA WHERE DESCRICAO = 'BaseCalculoSwap'); delete TipoDePara where descricao = 'BaseCalculoSwap' ; insert into TipoDepara(TipoCampo,Descricao,Observacao,EnumTipoDePara) values(1,'BaseCalculoSwap','Base de Cálculo do SWAP',null)
	delete DEPARA WHERE IDTIPODEPARA IN(SELECT IDTIPODEPARA FROM TIPODEPARA WHERE DESCRICAO = 'Eventos'); delete TipoDePara where descricao = 'Eventos' ; insert into TipoDepara(TipoCampo,Descricao,Observacao,EnumTipoDePara) values(1,'Eventos','Eventos',null)
	delete DEPARA WHERE IDTIPODEPARA IN(SELECT IDTIPODEPARA FROM TIPODEPARA WHERE DESCRICAO = 'TipoOperacaoBMF'); delete TipoDePara where descricao = 'TipoOperacaoBMF' ; insert into TipoDepara(TipoCampo,Descricao,Observacao,EnumTipoDePara) values(1,'TipoOperacaoBMF','Tipo Operação BMF',null)
	delete DEPARA WHERE IDTIPODEPARA IN(SELECT IDTIPODEPARA FROM TIPODEPARA WHERE DESCRICAO = 'TipoMercadoBMF'); delete TipoDePara where descricao = 'TipoMercadoBMF' ; insert into TipoDepara(TipoCampo,Descricao,Observacao,EnumTipoDePara) values(1,'TipoMercadoBMF','Tipo Mercado BMF',null)
	delete DEPARA WHERE IDTIPODEPARA IN(SELECT IDTIPODEPARA FROM TIPODEPARA WHERE DESCRICAO = 'LiquidacaoTituloRF'); delete TipoDePara where descricao = 'LiquidacaoTituloRF' ; insert into TipoDepara(TipoCampo,Descricao,Observacao,EnumTipoDePara) values(1,'LiquidacaoTituloRF','Liquidação Título de Renda Fixa',null)
	delete DEPARA WHERE IDTIPODEPARA IN(SELECT IDTIPODEPARA FROM TIPODEPARA WHERE DESCRICAO = 'TipoRegistroSwap'); delete TipoDePara where descricao = 'TipoRegistroSwap' ; insert into TipoDepara(TipoCampo,Descricao,Observacao,EnumTipoDePara) values(1,'TipoRegistroSwap','Tipo Registro Swap',null)
	delete DEPARA WHERE IDTIPODEPARA IN(SELECT IDTIPODEPARA FROM TIPODEPARA WHERE DESCRICAO = 'TipoLiquidacaoSwap'); delete TipoDePara where descricao = 'TipoLiquidacaoSwap' ; insert into TipoDepara(TipoCampo,Descricao,Observacao,EnumTipoDePara) values(1,'TipoLiquidacaoSwap','Tipo Liquidação Swap (não tem no PAS)',null)
	delete DEPARA WHERE IDTIPODEPARA IN(SELECT IDTIPODEPARA FROM TIPODEPARA WHERE DESCRICAO = 'TipoCaixaSwap'); delete TipoDePara where descricao = 'TipoCaixaSwap' ; insert into TipoDepara(TipoCampo,Descricao,Observacao,EnumTipoDePara) values(1,'TipoCaixaSwap','Tipo Caixa Swap (não tem no PAS)',null)
	delete DEPARA WHERE IDTIPODEPARA IN(SELECT IDTIPODEPARA FROM TIPODEPARA WHERE DESCRICAO = 'TipoSistemaSwap'); delete TipoDePara where descricao = 'TipoSistemaSwap' ; insert into TipoDepara(TipoCampo,Descricao,Observacao,EnumTipoDePara) values(1,'TipoSistemaSwap','Tipo Sistema Swap (não tem no PAS)',null)
	delete DEPARA WHERE IDTIPODEPARA IN(SELECT IDTIPODEPARA FROM TIPODEPARA WHERE DESCRICAO = 'TipoLiquidacaoFundo'); delete TipoDePara where descricao = 'TipoLiquidacaoFundo' ; insert into TipoDepara(TipoCampo,Descricao,Observacao,EnumTipoDePara) values(1,'TipoLiquidacaoFundo','Tipo Liquidação Fundo (não tem no PAS)',null)





	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'SituacaoLancamentoLiquidacao'),'1','1','Normal')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'SituacaoLancamentoLiquidacao'),'2','2','Pendente')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'SituacaoLancamentoLiquidacao'),'3','3','Compensacao')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'FonteLancamentoLiquidacao'),'1','1','Interno')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'FonteLancamentoLiquidacao'),'2','2','Manual')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'FonteLancamentoLiquidacao'),'3','3','Sinacor')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'FonteLancamentoLiquidacao'),'4','4','ArquivoCMDF')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'Tipo_MT_CAIXA'),'0','None','Bolsa-None')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'Tipo_MT_CAIXA'),'1','Compra Acoes','Bolsa-CompraAcoes')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'Tipo_MT_CAIXA'),'2','Venda Acoes','Bolsa-VendaAcoes')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'Tipo_MT_CAIXA'),'3','Compra Opcoes','Bolsa-CompraOpcoes')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'Tipo_MT_CAIXA'),'4','Venda Opcoes','Bolsa-VendaOpcoes')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'Tipo_MT_CAIXA'),'5','Exercicio Compra','Bolsa-ExercicioCompra')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'Tipo_MT_CAIXA'),'6','Exercicio Venda','Bolsa-ExercicioVenda')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'Tipo_MT_CAIXA'),'7','Compra Termo','Bolsa-CompraTermo')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'Tipo_MT_CAIXA'),'8','Venda Termo','Bolsa-VendaTermo')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'Tipo_MT_CAIXA'),'9','Antecipacao Termo','Bolsa-AntecipacaoTermo')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'Tipo_MT_CAIXA'),'10','Emprestimo Doado','Bolsa-EmprestimoDoado')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'Tipo_MT_CAIXA'),'11','Emprestimo Tomado','Bolsa-EmprestimoTomado')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'Tipo_MT_CAIXA'),'12','Antecipacao Emprestimo','Bolsa-AntecipacaoEmprestimo')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'Tipo_MT_CAIXA'),'13','Ajuste Operacao Futuro','Bolsa-AjusteOperacaoFuturo')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'Tipo_MT_CAIXA'),'14','Ajuste Posicao Futuro','Bolsa-AjustePosicaoFuturo')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'Tipo_MT_CAIXA'),'15','Liquidacao Final Futuro','Bolsa-LiquidacaoFinalFuturo')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'Tipo_MT_CAIXA'),'16','Despesas Taxas','Bolsa-DespesasTaxas')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'Tipo_MT_CAIXA'),'17','Corretagem','Bolsa-Corretagem')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'Tipo_MT_CAIXA'),'18','Dividendo','Bolsa-Dividendo')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'Tipo_MT_CAIXA'),'19','Juros Capital','Bolsa-JurosCapital')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'Tipo_MT_CAIXA'),'20','Rendimento','Bolsa-Rendimento')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'Tipo_MT_CAIXA'),'21','Restituicao Capital','Bolsa-RestituicaoCapital')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'Tipo_MT_CAIXA'),'22','Credito Fracoes Acoes','Bolsa-CreditoFracoesAcoes')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'Tipo_MT_CAIXA'),'23','IR Proventos','Bolsa-IRProventos')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'Tipo_MT_CAIXA'),'24','Outros Proventos','Bolsa-OutrosProventos')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'Tipo_MT_CAIXA'),'25','Amortizacao','Bolsa-Amortizacao')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'Tipo_MT_CAIXA'),'40','Taxa Custodia','Bolsa-TaxaCustodia')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'Tipo_MT_CAIXA'),'100','Outros','Bolsa-Outros')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'Tipo_MT_CAIXA'),'200','None','BMF-None')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'Tipo_MT_CAIXA'),'201','Compra Vista','BMF-CompraVista')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'Tipo_MT_CAIXA'),'202','Venda Vista','BMF-VendaVista')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'Tipo_MT_CAIXA'),'203','Compra Opcoes','BMF-CompraOpcoes')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'Tipo_MT_CAIXA'),'204','Venda Opcoes','BMF-VendaOpcoes')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'Tipo_MT_CAIXA'),'205','Exercicio Compra','BMF-ExercicioCompra')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'Tipo_MT_CAIXA'),'206','Exercicio Venda','BMF-ExercicioVenda')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'Tipo_MT_CAIXA'),'220','Despesas Taxas','BMF-DespesasTaxas')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'Tipo_MT_CAIXA'),'221','Corretagem','BMF-Corretagem')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'Tipo_MT_CAIXA'),'222','Emolumento','BMF-Emolumento')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'Tipo_MT_CAIXA'),'223','Registro','BMF-Registro')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'Tipo_MT_CAIXA'),'224','Outros Custos','BMF-OutrosCustos')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'Tipo_MT_CAIXA'),'250','Ajuste Posicao','BMF-AjustePosicao')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'Tipo_MT_CAIXA'),'251','Ajuste Operacao','BMF-AjusteOperacao')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'Tipo_MT_CAIXA'),'260','Taxa Permanencia','BMF-TaxaPermanencia')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'Tipo_MT_CAIXA'),'300','Outros','BMF-Outros')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'Tipo_MT_CAIXA'),'500','None','RendaFixa-None')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'Tipo_MT_CAIXA'),'501','Compra Final','RendaFixa-CompraFinal')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'Tipo_MT_CAIXA'),'502','Venda Final','RendaFixa-VendaFinal')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'Tipo_MT_CAIXA'),'503','Compra Revenda','RendaFixa-CompraRevenda')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'Tipo_MT_CAIXA'),'504','Venda Recompra','RendaFixa-VendaRecompra')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'Tipo_MT_CAIXA'),'505','Net Operacao Casada','RendaFixa-NetOperacaoCasada')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'Tipo_MT_CAIXA'),'510','Vencimento','RendaFixa-Vencimento')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'Tipo_MT_CAIXA'),'511','Revenda','RendaFixa-Revenda')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'Tipo_MT_CAIXA'),'512','Recompra','RendaFixa-Recompra')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'Tipo_MT_CAIXA'),'520','Juros','RendaFixa-Juros')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'Tipo_MT_CAIXA'),'521','Amortizacao','RendaFixa-Amortizacao')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'Tipo_MT_CAIXA'),'530','IR','RendaFixa-IR')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'Tipo_MT_CAIXA'),'531','IOF','RendaFixa-IOF')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'Tipo_MT_CAIXA'),'540','Corretagem','RendaFixa-Corretagem')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'Tipo_MT_CAIXA'),'550','TaxaCustodia','RendaFixa-TaxaCustodia')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'Tipo_MT_CAIXA'),'600','Outros','RendaFixa-Outros')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'Tipo_MT_CAIXA'),'701','Liquidacao Vencimento','Swap-LiquidacaoVencimento')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'Tipo_MT_CAIXA'),'702','Liquidacao Antecipacao','Swap-LiquidacaoAntecipacao')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'Tipo_MT_CAIXA'),'703','Despesas Taxas','Swap-DespesasTaxas')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'Tipo_MT_CAIXA'),'801','Taxa Administracao','Provisao-TaxaAdministracao')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'Tipo_MT_CAIXA'),'802','Pagto Taxa Administracao','Provisao-PagtoTaxaAdministracao')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'Tipo_MT_CAIXA'),'810','Taxa Gestao','Provisao-TaxaGestao')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'Tipo_MT_CAIXA'),'811','Pagto Taxa Gestao','Provisao-PagtoTaxaGestao')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'Tipo_MT_CAIXA'),'820','Taxa Performance','Provisao-TaxaPerformance')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'Tipo_MT_CAIXA'),'821','Pagto Taxa Performance','Provisao-PagtoTaxaPerformance')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'Tipo_MT_CAIXA'),'830','CPMF','Provisao-CPMF')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'Tipo_MT_CAIXA'),'840','Taxa Fiscalizacao CVM','Provisao-TaxaFiscalizacaoCVM')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'Tipo_MT_CAIXA'),'841','Pagto TaxaFiscalizacao CVM','Provisao-PagtoTaxaFiscalizacaoCVM')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'Tipo_MT_CAIXA'),'890','Provisao Outros','Provisao-ProvisaoOutros')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'Tipo_MT_CAIXA'),'891','Pagto Provisao Outros','Provisao-PagtoProvisaoOutros')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'Tipo_MT_CAIXA'),'851','Taxa Custodia','Provisao-TaxaCustodia')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'Tipo_MT_CAIXA'),'852','Pagto Taxa Custodia','Provisao-PagtoTaxaCustodia')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'Tipo_MT_CAIXA'),'1000','None','Fundo-None')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'Tipo_MT_CAIXA'),'1001','Aplicacao','Fundo-Aplicacao')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'Tipo_MT_CAIXA'),'1002','Resgate','Fundo-Resgate')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'Tipo_MT_CAIXA'),'1003','IR Resgate','Fundo-IRResgate')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'Tipo_MT_CAIXA'),'1004','IOF Resgate','Fundo-IOFResgate')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'Tipo_MT_CAIXA'),'1005','PfeeResgate','Fundo-PfeeResgate')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'Tipo_MT_CAIXA'),'1006','Come Cotas','Fundo-ComeCotas')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'Tipo_MT_CAIXA'),'1010','Aplicacao Converter','Fundo-AplicacaoConverter')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'Tipo_MT_CAIXA'),'1020','Transferencia Saldo','Fundo-TransferenciaSaldo')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'Tipo_MT_CAIXA'),'1100','None','Cotista-None')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'Tipo_MT_CAIXA'),'1101','Aplicacao','Cotista-Aplicacao')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'Tipo_MT_CAIXA'),'1102','Resgate','Cotista-Resgate')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'Tipo_MT_CAIXA'),'1103','IRResgate','Cotista-IRResgate')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'Tipo_MT_CAIXA'),'1104','IOFResgate','Cotista-IOFResgate')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'Tipo_MT_CAIXA'),'1105','PfeeResgate','Cotista-PfeeResgate')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'Tipo_MT_CAIXA'),'1106','ComeCotas','Cotista-ComeCotas')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'Tipo_MT_CAIXA'),'1110','Aplicacao Converter','Cotista-AplicacaoConverter')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'Tipo_MT_CAIXA'),'2000','IR Fonte Operacao','IR-IRFonteOperacao')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'Tipo_MT_CAIXA'),'2001','IR Fonte Day Trade','IR-IRFonteDayTrade')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'Tipo_MT_CAIXA'),'2002','IR Renda Variavel','IR-IRRendaVariavel')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'Tipo_MT_CAIXA'),'3000','Chamada Margem','Margem-ChamadaMargem')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'Tipo_MT_CAIXA'),'3001','Devolucao Margem','Margem-DevolucaoMargem')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'Tipo_MT_CAIXA'),'5000','Ajuste Compensacao Cota','-AjusteCompensacaoCota')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'Tipo_MT_CAIXA'),'100000','Outros','-Outros')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'Origem_MT_CAIXA'),'0','Bolsa','Bolsa-None')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'Origem_MT_CAIXA'),'1','Bolsa','Bolsa-CompraAcoes')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'Origem_MT_CAIXA'),'2','Bolsa','Bolsa-VendaAcoes')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'Origem_MT_CAIXA'),'3','Bolsa','Bolsa-CompraOpcoes')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'Origem_MT_CAIXA'),'4','Bolsa','Bolsa-VendaOpcoes')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'Origem_MT_CAIXA'),'5','Bolsa','Bolsa-ExercicioCompra')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'Origem_MT_CAIXA'),'6','Bolsa','Bolsa-ExercicioVenda')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'Origem_MT_CAIXA'),'7','Bolsa','Bolsa-CompraTermo')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'Origem_MT_CAIXA'),'8','Bolsa','Bolsa-VendaTermo')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'Origem_MT_CAIXA'),'9','Bolsa','Bolsa-AntecipacaoTermo')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'Origem_MT_CAIXA'),'10','Bolsa','Bolsa-EmprestimoDoado')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'Origem_MT_CAIXA'),'11','Bolsa','Bolsa-EmprestimoTomado')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'Origem_MT_CAIXA'),'12','Bolsa','Bolsa-AntecipacaoEmprestimo')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'Origem_MT_CAIXA'),'13','Bolsa','Bolsa-AjusteOperacaoFuturo')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'Origem_MT_CAIXA'),'14','Bolsa','Bolsa-AjustePosicaoFuturo')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'Origem_MT_CAIXA'),'15','Bolsa','Bolsa-LiquidacaoFinalFuturo')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'Origem_MT_CAIXA'),'16','Bolsa','Bolsa-DespesasTaxas')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'Origem_MT_CAIXA'),'17','Bolsa','Bolsa-Corretagem')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'Origem_MT_CAIXA'),'18','Bolsa','Bolsa-Dividendo')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'Origem_MT_CAIXA'),'19','Bolsa','Bolsa-JurosCapital')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'Origem_MT_CAIXA'),'20','Bolsa','Bolsa-Rendimento')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'Origem_MT_CAIXA'),'21','Bolsa','Bolsa-RestituicaoCapital')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'Origem_MT_CAIXA'),'22','Bolsa','Bolsa-CreditoFracoesAcoes')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'Origem_MT_CAIXA'),'23','Bolsa','Bolsa-IRProventos')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'Origem_MT_CAIXA'),'24','Bolsa','Bolsa-OutrosProventos')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'Origem_MT_CAIXA'),'25','Bolsa','Bolsa-Amortizacao')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'Origem_MT_CAIXA'),'40','Bolsa','Bolsa-TaxaCustodia')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'Origem_MT_CAIXA'),'100','Bolsa','Bolsa-Outros')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'Origem_MT_CAIXA'),'200','BMF','BMF-None')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'Origem_MT_CAIXA'),'201','BMF','BMF-CompraVista')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'Origem_MT_CAIXA'),'202','BMF','BMF-VendaVista')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'Origem_MT_CAIXA'),'203','BMF','BMF-CompraOpcoes')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'Origem_MT_CAIXA'),'204','BMF','BMF-VendaOpcoes')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'Origem_MT_CAIXA'),'205','BMF','BMF-ExercicioCompra')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'Origem_MT_CAIXA'),'206','BMF','BMF-ExercicioVenda')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'Origem_MT_CAIXA'),'220','BMF','BMF-DespesasTaxas')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'Origem_MT_CAIXA'),'221','BMF','BMF-Corretagem')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'Origem_MT_CAIXA'),'222','BMF','BMF-Emolumento')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'Origem_MT_CAIXA'),'223','BMF','BMF-Registro')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'Origem_MT_CAIXA'),'224','BMF','BMF-OutrosCustos')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'Origem_MT_CAIXA'),'250','BMF','BMF-AjustePosicao')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'Origem_MT_CAIXA'),'251','BMF','BMF-AjusteOperacao')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'Origem_MT_CAIXA'),'260','BMF','BMF-TaxaPermanencia')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'Origem_MT_CAIXA'),'300','BMF','BMF-Outros')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'Origem_MT_CAIXA'),'500','RendaFixa','RendaFixa-None')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'Origem_MT_CAIXA'),'501','RendaFixa','RendaFixa-CompraFinal')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'Origem_MT_CAIXA'),'502','RendaFixa','RendaFixa-VendaFinal')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'Origem_MT_CAIXA'),'503','RendaFixa','RendaFixa-CompraRevenda')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'Origem_MT_CAIXA'),'504','RendaFixa','RendaFixa-VendaRecompra')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'Origem_MT_CAIXA'),'505','RendaFixa','RendaFixa-NetOperacaoCasada')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'Origem_MT_CAIXA'),'510','RendaFixa','RendaFixa-Vencimento')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'Origem_MT_CAIXA'),'511','RendaFixa','RendaFixa-Revenda')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'Origem_MT_CAIXA'),'512','RendaFixa','RendaFixa-Recompra')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'Origem_MT_CAIXA'),'520','RendaFixa','RendaFixa-Juros')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'Origem_MT_CAIXA'),'521','RendaFixa','RendaFixa-Amortizacao')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'Origem_MT_CAIXA'),'530','RendaFixa','RendaFixa-IR')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'Origem_MT_CAIXA'),'531','RendaFixa','RendaFixa-IOF')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'Origem_MT_CAIXA'),'540','RendaFixa','RendaFixa-Corretagem')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'Origem_MT_CAIXA'),'550','RendaFixa','RendaFixa-TaxaCustodia')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'Origem_MT_CAIXA'),'600','RendaFixa','RendaFixa-Outros')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'Origem_MT_CAIXA'),'701','Swap','Swap-LiquidacaoVencimento')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'Origem_MT_CAIXA'),'702','Swap','Swap-LiquidacaoAntecipacao')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'Origem_MT_CAIXA'),'703','Swap','Swap-DespesasTaxas')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'Origem_MT_CAIXA'),'801','Provisao','Provisao-TaxaAdministracao')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'Origem_MT_CAIXA'),'802','Provisao','Provisao-PagtoTaxaAdministracao')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'Origem_MT_CAIXA'),'810','Provisao','Provisao-TaxaGestao')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'Origem_MT_CAIXA'),'811','Provisao','Provisao-PagtoTaxaGestao')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'Origem_MT_CAIXA'),'820','Provisao','Provisao-TaxaPerformance')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'Origem_MT_CAIXA'),'821','Provisao','Provisao-PagtoTaxaPerformance')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'Origem_MT_CAIXA'),'830','Provisao','Provisao-CPMF')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'Origem_MT_CAIXA'),'840','Provisao','Provisao-TaxaFiscalizacaoCVM')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'Origem_MT_CAIXA'),'841','Provisao','Provisao-PagtoTaxaFiscalizacaoCVM')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'Origem_MT_CAIXA'),'890','Provisao','Provisao-ProvisaoOutros')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'Origem_MT_CAIXA'),'891','Provisao','Provisao-PagtoProvisaoOutros')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'Origem_MT_CAIXA'),'851','Provisao','Provisao-TaxaCustodia')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'Origem_MT_CAIXA'),'852','Provisao','Provisao-PagtoTaxaCustodia')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'Origem_MT_CAIXA'),'1000','Fundo','Fundo-None')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'Origem_MT_CAIXA'),'1001','Fundo','Fundo-Aplicacao')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'Origem_MT_CAIXA'),'1002','Fundo','Fundo-Resgate')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'Origem_MT_CAIXA'),'1003','Fundo','Fundo-IRResgate')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'Origem_MT_CAIXA'),'1004','Fundo','Fundo-IOFResgate')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'Origem_MT_CAIXA'),'1005','Fundo','Fundo-PfeeResgate')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'Origem_MT_CAIXA'),'1006','Fundo','Fundo-ComeCotas')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'Origem_MT_CAIXA'),'1010','Fundo','Fundo-AplicacaoConverter')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'Origem_MT_CAIXA'),'1020','Fundo','Fundo-TransferenciaSaldo')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'Origem_MT_CAIXA'),'1100','Cotista','Cotista-None')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'Origem_MT_CAIXA'),'1101','Cotista','Cotista-Aplicacao')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'Origem_MT_CAIXA'),'1102','Cotista','Cotista-Resgate')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'Origem_MT_CAIXA'),'1103','Cotista','Cotista-IRResgate')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'Origem_MT_CAIXA'),'1104','Cotista','Cotista-IOFResgate')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'Origem_MT_CAIXA'),'1105','Cotista','Cotista-PfeeResgate')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'Origem_MT_CAIXA'),'1106','Cotista','Cotista-ComeCotas')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'Origem_MT_CAIXA'),'1110','Cotista','Cotista-AplicacaoConverter')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'Origem_MT_CAIXA'),'2000','IR','IR-IRFonteOperacao')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'Origem_MT_CAIXA'),'2001','IR','IR-IRFonteDayTrade')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'Origem_MT_CAIXA'),'2002','IR','IR-IRRendaVariavel')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'Origem_MT_CAIXA'),'3000','Margem','Margem-ChamadaMargem')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'Origem_MT_CAIXA'),'3001','Margem','Margem-DevolucaoMargem')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'Origem_MT_CAIXA'),'5000','Fundo','AjusteCompensacaoCota')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'Origem_MT_CAIXA'),'100000','Outros','Outros')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'Tipo Operacao Fundo'),'1','A','Aplicacao')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'Tipo Operacao Fundo'),'2','R','ResgateBruto')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'Tipo Operacao Fundo'),'3','R','ResgateLiquido')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'Tipo Operacao Fundo'),'4','R','ResgateCotas')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'Tipo Operacao Fundo'),'5','R','ResgateTotal')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'Tipo Operacao Fundo'),'10','A','AplicacaoCotasEspecial')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'Tipo Operacao Fundo'),'11','A','AplicacaoAcoesEspecial')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'Tipo Operacao Fundo'),'12','R','ResgateCotasEspecial')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'Tipo Operacao Fundo'),'20','','ComeCotas')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'Tipo Operacao Fundo'),'40','','AjustePosicao')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'Tipo Operacao Fundo'),'80','','Amortizacao')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'Tipo Operacao Fundo'),'82','','Juros')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'Tipo Operacao Fundo'),'84','','AmortizacaoJuros')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'Tipo Operacao Fundo'),'100','','IncorporacaoResgate')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'Tipo Operacao Fundo'),'101','','IncorporacaoAplicacao')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'TipoTributacaoFundo'),'1','RF','CurtoPrazo')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'TipoTributacaoFundo'),'2','RF','LongoPrazo')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'TipoTributacaoFundo'),'3','RV','Acoes')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'TipoTributacaoFundo'),'4','RF','CPrazo_SemComeCotas')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'TipoTributacaoFundo'),'5','RF','LPrazo_SemComeCotas')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'TipoTributacaoFundo'),'20','Isento','Isento')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'TipoCarteiraFundo'),'1','RF','Renda Fixa')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'TipoCarteiraFundo'),'2','RV','Renda Variável')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'TipoOperacaoTitulo'),'1','C','CompraFinal')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'TipoOperacaoTitulo'),'2','V','VendaFinal')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'TipoOperacaoTitulo'),'3','C','CompraRevenda')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'TipoOperacaoTitulo'),'4','V','VendaRecompra')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'TipoOperacaoTitulo'),'6','V','VendaTotal')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'TipoOperacaoTitulo'),'10','C','CompraCasada')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'TipoOperacaoTitulo'),'11','V','VendaCasada')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'TipoOperacaoTitulo'),'12','V','AntecipacaoRevenda')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'TipoOperacaoTitulo'),'13','C','AntecipacaoRecompra')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'TipoOperacaoTitulo'),'20','D','Deposito')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'TipoOperacaoTitulo'),'21','R','Retirada')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'TipoOperacaoTitulo'),'22','D','IngressoAtivoImpactoQtde')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'TipoOperacaoTitulo'),'23','D','IngressoAtivoImpactoCota')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'TipoOperacaoTitulo'),'24','R','RetiradaAtivoImpactoQtde')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'TipoOperacaoTitulo'),'25','R','RetiradaAtivoImpactoCota')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'PontaSwap'),'1','C','Parte')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'PontaSwap'),'2','V','ContraParte')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'TipoMercadoAtivoBolsa'),'OPV','P','Opção de Venda')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'TipoMercadoAtivoBolsa'),'OPC','O','Opção de Compra')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'TipoMercadoAtivoBolsa'),'VIS','V','Papel a Vista')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'PontaEmprestimoBolsa'),'1','D','Doador')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'PontaEmprestimoBolsa'),'2','T','Tomador')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'TipoAtivoAuxiliar'),'1','OperacaoBolsa','')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'TipoAtivoAuxiliar'),'2','OperacaoRendaFixa','')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'TipoAtivoAuxiliar'),'3','OperacaoFundos','')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'TipoAtivoAuxiliar'),'4','ContaCorrente','')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'TipoAtivoAuxiliar'),'5','OperacaoBMF','')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'TipoAtivoAuxiliar'),'6','OperacaoSwap','')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'TipoAtivoAuxiliar'),'7','Fundo','')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'TipoOperacaoCotista'),'1','A','Aplicacao')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'TipoOperacaoCotista'),'2','RB','ResgateBruto')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'TipoOperacaoCotista'),'3','RL','ResgateLiquido')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'TipoOperacaoCotista'),'4','RC','ResgateCotas')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'TipoOperacaoCotista'),'5','RT','ResgateTotal')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'TipoOperacaoCotista'),'10','não mapeada','AplicacaoCotasEspecial')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'TipoOperacaoCotista'),'11','não mapeada','AplicacaoAcoesEspecial')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'TipoOperacaoCotista'),'12','não mapeada','ResgateCotasEspecial')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'TipoOperacaoCotista'),'20','RL','ComeCotas')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'TipoOperacaoCotista'),'80','não mapeada','Amortizacao')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'TipoOperacaoCotista'),'82','não mapeada','Juros')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'TipoOperacaoCotista'),'84','não mapeada','AmortizacaoJuros')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'TipoOperacaoCotista'),'100','não mapeada','IncorporacaoResgate')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'TipoOperacaoCotista'),'101','não mapeada','IncorporacaoAplicacao')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'TipoOperacaoCotistaNota'),'2','NB','Resgate por NOTA - Valor Bruto')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'TipoOperacaoCotistaNota'),'3','NL','Resgate por NOTA -  Valor Líquido')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'TipoOperacaoCotistaNota'),'5','NT','Resgate por NOTA - Total')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'TipoResgateCotista'),'1','não existe','Especifico')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'TipoResgateCotista'),'2','F','FIFO')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'TipoResgateCotista'),'3','não existe','LIFO')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'TipoResgateCotista'),'4','J','MenorImposto')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'ListaCadastro'),'-1','','Emissores')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'ListaCadastro'),'-2','','Administradores')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'ListaCadastro'),'-3','','Custodiantes')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'ListaCadastro'),'-4','','Corretora')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'ListaCadastro'),'-5','','Gestor')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'ListaCadastro'),'-6','','Liquidante')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'ListaCadastro'),'-7','','Distribuidor')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'ListaCadastro'),'-8','','CarteiraFundo')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'ListaCadastro'),'-9','','Cotista')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'ListaCadastro'),'-10','','AtivoBolsa')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'ListaCadastro'),'-11','','AtivoBMF')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'ListaCadastro'),'-12','','RendaFixaTitulo')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'ListaCadastro'),'-13','','RendaFixaPapel')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'TipoDivulgacaoIndice'),'1','D','Diario')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'TipoDivulgacaoIndice'),'2','M','Mensal')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'TipoDivulgacaoIndice'),'3','M','Mensal_1')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'TipoDivulgacaoIndice'),'4','M','Mensal_2')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'TipoDivulgacaoIndice'),'5','M','Mensal_3')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'TipoDivulgacaoIndice'),'6','D','Diario_1')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'TipoDivulgacaoIndice'),'7','D','Diario_2')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'TipoDivulgacaoIndice'),'8','D','Diario_3')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'TipoIndice'),'1','N','Percentual')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'TipoIndice'),'2','I','Decimal')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'TipoPapelAtivo'),'1','Normal','Normal')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'TipoPapelAtivo'),'2','Bonus de Subscrição','BonusSubscricao')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'TipoPapelAtivo'),'3','Recibo de Subscrição','ReciboSubscricao')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'TipoPapelAtivo'),'4','ETF','ETF')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'TipoPapelAtivo'),'5','Certificado de Deposito em Ações','CertificadoDepositoAcoes')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'TipoPapelAtivo'),'6','Recibo de Deposito em Ações','ReciboDepositoAcoes')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'TipoPapelAtivo'),'7','BDR Nível I','BDRNivelI')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'TipoPapelAtivo'),'8','BDR Nível II','BDRNivelII')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'TipoPapelAtivo'),'9','BDR Nível III','BDRNivelIII')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'ClassificacaoCVMFundo'),'1','Companhia Aberta','Companhia Aberta')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'ClassificacaoCVMFundo'),'2','Sociedade Beneficiaria Incentivo Fiscal','Sociedade Beneficiaria Incentivo Fiscal')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'ClassificacaoCVMFundo'),'3','Instituicao Financeira','Instituicao Financeira')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'ClassificacaoCVMFundo'),'4','Investidor Estrangeiro','Investidor Estrangeiro')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'ClassificacaoCVMFundo'),'5','Fundo Investimento','Fundo Investimento')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'ClassificacaoCVMFundo'),'6','Fundo Aplicacao Cotas','Fundo Aplicacao Cotas')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'ClassePapelRendaFixa'),'1','LFT','LFT')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'ClassePapelRendaFixa'),'2','LTN','LTN')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'ClassePapelRendaFixa'),'10','Pre_Descontado','Pre_Descontado')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'ClassePapelRendaFixa'),'12','Pre_Descontado_Fluxo','Pre_Descontado_Fluxo')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'ClassePapelRendaFixa'),'15','Pre_Atualizado','Pre_Atualizado')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'ClassePapelRendaFixa'),'20','NTN','NTN')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'ClassePapelRendaFixa'),'30','PosFixado','PosFixado')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'ClassePapelRendaFixa'),'35','PosFixado_Truncado','PosFixado_Truncado')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'ClassePapelRendaFixa'),'45','PosFixado_FluxoCorrigido','PosFixado_FluxoCorrigido')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'ClassePapelRendaFixa'),'1000','CCB','CCB')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'ClassePapelRendaFixa'),'1005','CCE','CCE')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'ClassePapelRendaFixa'),'1010','NCE','NCE')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'ClassePapelRendaFixa'),'1015','ExportNote','ExportNote')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'ClassePapelRendaFixa'),'1100','CDB','CDB')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'ClassePapelRendaFixa'),'1105','DPGE','DPGE')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'ClassePapelRendaFixa'),'1110','LF','LF')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'ClassePapelRendaFixa'),'1200','CRI','CRI')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'ClassePapelRendaFixa'),'1205','CCI','CCI')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'ClassePapelRendaFixa'),'1300','LCI','LCI')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'ClassePapelRendaFixa'),'1400','LH','LH')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'ClassePapelRendaFixa'),'1500','NotaComercial','NotaComercial')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'ClassePapelRendaFixa'),'1700','LC','LC')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'ClassePapelRendaFixa'),'2000','Debenture','Debenture')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'ClassePapelRendaFixa'),'2100','LCA','LCA')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'ClassePapelRendaFixa'),'2105','CRA','CRA')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'ClassePapelRendaFixa'),'2110','CDCA','CDCA')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'ClassePapelRendaFixa'),'2115','CPR','CPR')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'ClassePapelRendaFixa'),'2120','CDA','CDA')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'ClassePapelRendaFixa'),'5000','TDA','TDA')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'TipoClassificacaoCliente'),'001','Cotista','')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'TipoClassificacaoCliente'),'002','Cotista gerenciado','')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'TipoClassificacaoCliente'),'003','Carteiras administradas pessoas físicas','')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'TipoClassificacaoCliente'),'004','Carteiras administradas pessoas jurídicas','')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'TipoClassificacaoCliente'),'005','Fundos PS Exclusivos','')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'TipoClassificacaoCliente'),'006','Fundos PS FIC','')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'TipoClassificacaoCliente'),'007','Fundos PS FIF','')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'TipoClassificacaoCliente'),'008','Fundo Terceiros FIF','')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'TipoClassificacaoCliente'),'009','Fundo Terceiros FIC','')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'TipoClassificacaoCliente'),'010','Fundos Terceiros Exclusivos','')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'TipoClassificacaoCliente'),'011','Outras carteiras simuladas','')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'TipoClassificacaoCliente'),'012','Carteira Master','')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'TipoMovimentoRF'),'1','Compra Final','')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'TipoMovimentoRF'),'2','Venda Final','')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'TipoMovimentoRF'),'3','Compra c/ Rev.','')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'TipoMovimentoRF'),'4','Venda c/ Rec.','')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'TipoMovimentoRF'),'6','Venda Total','')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'TipoMovimentoRF'),'10','Compra Casada','')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'TipoMovimentoRF'),'11','Venda Casada','')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'TipoMovimentoRF'),'12','Antecip. Rev.','')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'TipoMovimentoRF'),'13','Antecip. Rec.','')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'TipoMovimentoRF'),'20','Depósito','')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'TipoMovimentoRF'),'21','Retirada','')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'CategoriaMovimentoRF'),'1','Negociação','')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'CategoriaMovimentoRF'),'2','Disp. Venda','')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'CategoriaMovimentoRF'),'3','Vencimento','')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'TipoOperacaoBolsa'),'C','Compra','')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'TipoOperacaoBolsa'),'V','Venda','')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'TipoOperacaoBolsa'),'CD','CompraDaytrade','')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'TipoOperacaoBolsa'),'VD','VendaDaytrade','')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'TipoOperacaoBolsa'),'DE','Deposito','')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'TipoOperacaoBolsa'),'RE','Retirada','')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'TipoOperacaoBolsa'),'1','Bloqueio','')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'TipoOperacaoBolsa'),'2','Desbloqueio','')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'TipoPontaEA'),'1','Doador','')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'TipoPontaEA'),'2','Tomador','')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'TipoEA'),'1','Voluntário','')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'TipoEA'),'2','Compulsório','')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'OrigemOperacaoBolsa'),'1','','')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'OrigemOperacaoBolsa'),'2','','')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'OrigemOperacaoBolsa'),'3','','')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'OrigemOperacaoBolsa'),'4','','')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'OrigemOperacaoBolsa'),'5','','')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'OrigemOperacaoBolsa'),'6','','')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'OrigemOperacaoBolsa'),'7','','')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'OrigemOperacaoBolsa'),'8','','')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'OrigemOperacaoBolsa'),'9','','')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'OrigemOperacaoBolsa'),'10','','')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'OrigemOperacaoBolsa'),'20','Subscrição','')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'OrigemOperacaoBolsa'),'30','','')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'OrigemOperacaoBolsa'),'80','Conversão','')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'OrigemOperacaoBolsa'),'81','','')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'OrigemOperacaoBolsa'),'82','Bonificação','')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'OrigemOperacaoBolsa'),'100','','')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'TipoPontaSwap'),'1','PREFIXADO','')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'TipoPontaSwap'),'2','INDEXADO','')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'TipoPontaSwap'),'3','INDEXADOD0','')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'TipoApropriacaoSwap'),'1','EXPONENCIAL','')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'TipoApropriacaoSwap'),'2','LINEAR','')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'ContagemDiasSwap'),'1','UTEIS','')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'ContagemDiasSwap'),'2','CORRIDOS','')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'BaseCalculoSwap'),'252','252','')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'BaseCalculoSwap'),'360','360','')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'BaseCalculoSwap'),'365','365','')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'Eventos'),'18','Dividendo','Bolsa-Dividendo')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'Eventos'),'19','Juros Capital','Bolsa-JurosCapital')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'Eventos'),'20','Rendimento','Bolsa-Rendimento')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'Eventos'),'25','Amortizacao','Bolsa-Amortizacao')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'Eventos'),'520','Juros','RendaFixa-Juros')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'Eventos'),'521','Amortizacao','RendaFixa-Amortizacao')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'TipoOperacaoBMF'),'C','Compra','')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'TipoOperacaoBMF'),'V','Venda','')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'TipoOperacaoBMF'),'CD','CompraDaytrade','')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'TipoOperacaoBMF'),'VD','VendaDaytrade','')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'TipoOperacaoBMF'),'DE','Deposito','')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'TipoOperacaoBMF'),'RE','Retirada','')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'TipoMercadoBMF'),'1','Disponível','')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'TipoMercadoBMF'),'2','Futuro','')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'TipoMercadoBMF'),'3','Opçao Disponível','')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'TipoMercadoBMF'),'4','Opção Futuro','')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'TipoMercadoBMF'),'5','Termo','')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'LiquidacaoTituloRF'),'0','NaoInformado','')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'LiquidacaoTituloRF'),'1','SELIC','')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'LiquidacaoTituloRF'),'2','CETIP','')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'LiquidacaoTituloRF'),'3','CBLC','')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'TipoRegistroSwap'),'1','BMF','')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'TipoRegistroSwap'),'2','CETIP','')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'TipoLiquidacaoSwap'),'0','Nenhum','')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'TipoLiquidacaoSwap'),'1','CETIP','')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'TipoLiquidacaoSwap'),'2','Interno','')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'TipoCaixaSwap'),'1','Com Caixa','')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'TipoCaixaSwap'),'2','Sem Caixa','')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'TipoSistemaSwap'),'0','Indeterminado','')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'TipoSistemaSwap'),'1','SPR','')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'TipoSistemaSwap'),'2','Interno','')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'TipoLiquidacaoFundo'),'1','TED','')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values((select IdTipoDePara from TipoDePara where Descricao = 'TipoLiquidacaoFundo'),'2','CETIP','')

end
	
IF NOT EXISTS (select 1 from sysconstraints where object_name(id) = 'DePara' and object_name(constid) = 'DePara_TipoDePara_FK')
BEGIN	
	alter table DePara add constraint DePara_TipoDePara_FK FOREIGN KEY ( IdTipoDePara ) references TipoDePara(IdTipoDePara)		
END
GO
		
IF NOT EXISTS (select 1 from sysconstraints where object_name(id) = 'MemoriaCalculoRendaFixa' and object_name(constid) = 'MemCalRF_Operacao_FK')
BEGIN	
	alter table MemoriaCalculoRendaFixa add constraint MemCalRF_Operacao_FK FOREIGN KEY ( IdOperacao ) references OperacaoRendaFixa(IdOperacao);		
END
GO
		
IF NOT EXISTS (select 1 from sysconstraints where object_name(id) = 'RepactuacaoRendaFixa' and object_name(constid) = 'Repac_PapelOrig_FK')
BEGIN	
	alter table RepactuacaoRendaFixa add constraint Repac_PapelOrig_FK FOREIGN KEY ( IdTituloOriginal ) references TituloRendaFixa(IdTitulo);
END
GO
		
IF NOT EXISTS (select 1 from sysconstraints where object_name(id) = 'RepactuacaoRendaFixa' and object_name(constid) = 'Repac_PapelNovo_FK')
BEGIN	
	alter table RepactuacaoRendaFixa add constraint Repac_PapelNovo_FK FOREIGN KEY ( IdTituloNovo ) references TituloRendaFixa(IdTitulo);
END
GO
		
IF EXISTS (select 1 from sysconstraints where object_name(id) = 'MemoriaCalculoRendaFixa' and object_name(constid) = 'PK_MemoriaCalculoRendaFixa')
BEGIN
	alter table MemoriaCalculoRendaFixa drop constraint PK_MemoriaCalculoRendaFixa;
	alter table MemoriaCalculoRendaFixa add constraint PK_MemoriaCalculoRendaFixa primary key(DataAtual, IdOperacao, TipoPreco, TipoProcessamento, TipoEventoTitulo, DataFluxo)	
END
GO
		
IF NOT EXISTS (select 1 from sysconstraints where object_name(id) = 'ColagemResgateDetalhePosicao' and object_name(constid) = 'ColagemResgates_Det_Pos_FK')
BEGIN
	alter table ColagemResgateDetalhePosicao add constraint ColagemResgates_Det_Pos_FK FOREIGN KEY ( IdColagemResgateDetalhe ) references ColagemResgateDetalhe(IdColagemResgateDetalhe)
END
GO
	
IF NOT EXISTS (select 1 from sysconstraints where object_name(id) = 'ColagemComeCotasDetalhePosicao' and object_name(constid) = 'ColagemComeCotass_Det_Pos_FK')
BEGIN	
	alter table ColagemComeCotasDetalhePosicao add constraint ColagemComeCotass_Det_Pos_FK FOREIGN KEY ( IdColagemComeCotasDetalhe ) references ColagemComeCotasDetalhe(IdColagemComeCotasDetalhe)
END
GO
	
IF NOT EXISTS (select 1 from sysconstraints where object_name(id) = 'Clearing' and object_name(constid) = 'Clearing_FormaLiq_FK')
BEGIN	
	alter table Clearing add constraint Clearing_FormaLiq_FK FOREIGN KEY ( IdFormaLiquidacao ) references FormaLiquidacao(IdFormaLiquidacao)
END
GO
		
IF NOT EXISTS (select 1 from sysconstraints where object_name(id) = 'Clearing' and object_name(constid) = 'Clearing_LocalFer_FK')
BEGIN	
	alter table Clearing add constraint Clearing_LocalFer_FK FOREIGN KEY ( IdLocalFeriado ) references LocalFeriado(IdLocal)
END
GO
		 
IF NOT EXISTS (select 1 from sysconstraints where object_name(id) = 'LocalNegociacao' and object_name(constid) = 'LocalNegociacao_Moeda_FK')
BEGIN	 
	alter table LocalNegociacao add constraint LocalNegociacao_Moeda_FK FOREIGN KEY ( IdMoeda ) references Moeda(IdMoeda)
END
GO
		
IF NOT EXISTS (select 1 from sysconstraints where object_name(id) = 'LocalNegociacao' and object_name(constid) = 'LocalNegociacao_LocalFer_FK')
BEGIN	
	alter table LocalNegociacao add constraint LocalNegociacao_LocalFer_FK FOREIGN KEY ( IdLocalFeriado ) references LocalFeriado(IdLocal)	 
END
GO
		 
IF EXISTS (select 1 from sysconstraints where object_name(id) = 'cliente' and object_name(constid) = 'Pessoa_Officer_FK1')
BEGIN
	alter table cliente drop constraint Pessoa_Officer_FK1;
	alter table cliente add constraint Cliente_Pessoa_FK1 FOREIGN KEY ( IdPessoa ) references Pessoa(IdPessoa)
END
GO
	
IF NOT EXISTS (select 1 from sysconstraints where object_name(id) = 'TransferenciaSerie' and object_name(constid) = 'TransSerie_SerieDest_FK')
BEGIN
	alter table TransferenciaSerie add constraint TransSerie_SerieDest_FK FOREIGN KEY ( IdSerieOrigem )  references SeriesOffShore(IdSeriesOffShore)
END
GO
		
IF NOT EXISTS (select 1 from sysconstraints where object_name(id) = 'TransferenciaSerie' and object_name(constid) = 'TransSerie_SerieOrig_FK')
BEGIN	
	alter table TransferenciaSerie add constraint TransSerie_SerieOrig_FK FOREIGN KEY ( IdSerieDestino ) references SeriesOffShore(IdSeriesOffShore)
END
GO
	 
if EXISTS(select 1 from sysconstraints where object_name(id) = 'DetalhePosicaoAfetadaRF' and object_name(constid) = 'DetalhePosAfetadaRF_PosRF_FK')
Begin
	alter table DetalhePosicaoAfetadaRF drop constraint DetalhePosAfetadaRF_PosRF_FK
END
GO	

IF NOT EXISTS (select 1 from sysconstraints where object_name(id) = 'EventoRollUp' and object_name(constid) = 'EventoRollUp_SerieDest_FK')
BEGIN
	alter table EventoRollUp add constraint EventoRollUp_SerieDest_FK FOREIGN KEY ( IdSerieOrigem )  references SeriesOffShore(IdSeriesOffShore)
END
GO
		
IF NOT EXISTS (select 1 from sysconstraints where object_name(id) = 'EventoRollUp' and object_name(constid) = 'EventoRollUp_SerieOrig_FK')
BEGIN	
	alter table EventoRollUp add constraint EventoRollUp_SerieOrig_FK FOREIGN KEY ( IdSerieDestino ) references SeriesOffShore(IdSeriesOffShore)
END
GO
		 
IF NOT EXISTS (select 1 from sysconstraints where object_name(id) = 'OperacaoCotista' and object_name(constid) = 'DF_ValorDespesas')
BEGIN
	ALTER TABLE OperacaoCotista ADD CONSTRAINT DF_ValorDespesas DEFAULT 0 FOR ValorDespesas;
END
GO
	
IF NOT EXISTS (select 1 from sysconstraints where object_name(id) = 'OperacaoCotista' and object_name(constid) = 'DF_ValorTaxas')
BEGIN
	ALTER TABLE OperacaoCotista ADD CONSTRAINT DF_ValorTaxas DEFAULT 0 FOR ValorTaxas;
END
GO	

IF NOT EXISTS (select 1 from sysconstraints where object_name(id) = 'OperacaoCotista' and object_name(constid) = 'DF_ValorTributos')
BEGIN
	ALTER TABLE OperacaoCotista ADD CONSTRAINT DF_ValorTributos DEFAULT 0 FOR ValorTributos;
END
GO
	
IF NOT EXISTS (select 1 from sysconstraints where object_name(id) = 'OperacaoCotista' and object_name(constid) = 'DF_PenalidadeLockUp')
BEGIN
	ALTER TABLE OperacaoCotista ADD CONSTRAINT DF_PenalidadeLockUp DEFAULT 0 FOR PenalidadeLockUp;
END
GO
	
IF NOT EXISTS (select 1 from sysconstraints where object_name(id) = 'OperacaoCotista' and object_name(constid) = 'DF_ValorHoldBack')
BEGIN
	ALTER TABLE OperacaoCotista ADD CONSTRAINT DF_ValorHoldBack DEFAULT 0 FOR ValorHoldBack;
END
GO
	
IF NOT EXISTS (select 1 from sysconstraints where object_name(id) = 'TabelaExcecaoAdm' and object_name(constid) = 'TabelaExcecaoAdm_TabelaTaxaAdministracao_FK1')
BEGIN
	ALTER TABLE TabelaExcecaoAdm  WITH CHECK ADD  CONSTRAINT TabelaExcecaoAdm_TabelaTaxaAdministracao_FK1 FOREIGN KEY(IdTabela)
	REFERENCES TabelaTaxaAdministracao (IdTabela)
END
GO
		 
IF NOT EXISTS (select 1 from sysconstraints where object_name(id) = 'CalculoAdministracaoAssociada' and object_name(constid) = 'Carteira_CalculoAdministracaoAssociada_FK1')
BEGIN
	ALTER TABLE [dbo].[CalculoAdministracaoAssociada]  WITH CHECK ADD  CONSTRAINT [Carteira_CalculoAdministracaoAssociada_FK1] FOREIGN KEY([IdCarteira])
	REFERENCES [dbo].[Carteira] ([IdCarteira])
END
GO
	
IF NOT EXISTS (select 1 from sysconstraints where object_name(id) = 'CalculoAdministracaoAssociada' and object_name(constid) = 'TabelaTaxaAdministracao_CalculoAdministracaoAssociada_FK1')
BEGIN
	ALTER TABLE [dbo].[CalculoAdministracaoAssociada]  WITH CHECK ADD  CONSTRAINT [TabelaTaxaAdministracao_CalculoAdministracaoAssociada_FK1] FOREIGN KEY([IdTabela])
	REFERENCES [dbo].[TabelaTaxaAdministracao] ([IdTabela])
END
GO
	 	 
IF NOT EXISTS (select 1 from sysconstraints where object_name(id) = 'CalculoAdministracaoAssociadaHistorico' and object_name(constid) = 'Carteira_CalculoAdministracaoAssociadaHistorico_FK1')
BEGIN
	ALTER TABLE [dbo].[CalculoAdministracaoAssociadaHistorico]  WITH CHECK ADD  CONSTRAINT [Carteira_CalculoAdministracaoAssociadaHistorico_FK1] FOREIGN KEY([IdCarteira])
	REFERENCES [dbo].[Carteira] ([IdCarteira])
END
GO
	
IF NOT EXISTS (select 1 from sysconstraints where object_name(id) = 'CalculoAdministracaoAssociadaHistorico' and object_name(constid) = 'TabelaTaxaAdministracao_CalculoAdministracaoAssociadaHistorico_FK1')
BEGIN
	ALTER TABLE [dbo].[CalculoAdministracaoAssociadaHistorico]  WITH CHECK ADD  CONSTRAINT [TabelaTaxaAdministracao_CalculoAdministracaoAssociadaHistorico_FK1] FOREIGN KEY([IdTabela])
	REFERENCES [dbo].[TabelaTaxaAdministracao] ([IdTabela])
END
GO
	 	 
IF NOT EXISTS (select 1 from sysconstraints where object_name(id) = 'ClientePerfil' and object_name(constid) = 'Perfil_ClientePerfil_FK1')
BEGIN
	ALTER TABLE ClientePerfil  WITH CHECK ADD  CONSTRAINT [Perfil_ClientePerfil_FK1] FOREIGN KEY(IdPerfil)
	REFERENCES PerfilProcessamento (IdPerfil)
END
GO
	
IF NOT EXISTS (select 1 from sysconstraints where object_name(id) = 'ClientePerfil' and object_name(constid) = 'Cliente_ClientePerfil_FK1')
BEGIN
	ALTER TABLE ClientePerfil  WITH CHECK ADD  CONSTRAINT [Cliente_ClientePerfil_FK1] FOREIGN KEY(IdCliente)
	REFERENCES Cliente (IdCliente)
END
GO
		 	 
IF NOT EXISTS (select 1 from sysconstraints where object_name(id) = 'LogProcessamento' and object_name(constid) = 'Cliente_LogProcessamento_FK1')
BEGIN
	ALTER TABLE LogProcessamento  WITH CHECK ADD  CONSTRAINT [Cliente_LogProcessamento_FK1] FOREIGN KEY(IdCliente)
	REFERENCES Cliente (IdCliente)
END
GO
	
IF NOT EXISTS (select 1 from sysconstraints where object_name(id) = 'BoletoRetroativo' and object_name(constid) = 'Cliente_BoletoRetroativo_FK1')
BEGIN
	ALTER TABLE BoletoRetroativo  WITH CHECK ADD  CONSTRAINT [Cliente_BoletoRetroativo_FK1] FOREIGN KEY(IdCliente)
	REFERENCES Cliente (IdCliente)
END
GO
	
IF NOT EXISTS (select 1 from sysconstraints where object_name(id) = 'AgendaProcessos' and object_name(constid) = 'PerfilProcessamento_AgendaProcessos_FK1')
BEGIN
	ALTER TABLE AgendaProcessos  WITH CHECK ADD  CONSTRAINT [PerfilProcessamento_AgendaProcessos_FK1] FOREIGN KEY(IdPerfil)
	REFERENCES PerfilProcessamento (IdPerfil)
END
GO
	
IF NOT EXISTS (select 1 from sysconstraints where object_name(id) = 'LogProcessamentoAutomatico' and object_name(constid) = 'AgendaProcessos_LogProcessamentoAutomatico_FK1')
BEGIN
	ALTER TABLE LogProcessamentoAutomatico  WITH CHECK ADD  CONSTRAINT [AgendaProcessos_LogProcessamentoAutomatico_FK1] FOREIGN KEY(IdAgendaProcesso)
	REFERENCES AgendaProcessos (IdAgendaProcesso)
END
GO
	
IF NOT EXISTS(select 1 from syscolumns where id = object_id('HistoricoCota') and name = 'CotaImportada')
BEGIN
	ALTER TABLE dbo.HistoricoCota ADD CotaImportada CHAR(1)
	CONSTRAINT DF_HistoricoCota_CotaImportada DEFAULT 'N' NOT NULL
END
GO
	
IF NOT EXISTS (select 1 from sysconstraints where object_name(id) = 'CalculoRebateImpactaPL' and object_name(constid) = 'CalculoRebateImpactaPL_Carteira_FK1')
BEGIN
	ALTER TABLE CalculoRebateImpactaPL  WITH CHECK ADD  CONSTRAINT CalculoRebateImpactaPL_Carteira_FK1 FOREIGN KEY(IdCarteira)
	REFERENCES Carteira (IdCarteira)
END
GO
	
IF NOT EXISTS (select 1 from sysconstraints where object_name(id) = 'CalculoRebateImpactaPL' and object_name(constid) = 'CalculoRebateImpactaPL_Cliente_FK1')
BEGIN
	ALTER TABLE CalculoRebateImpactaPL  WITH CHECK ADD  CONSTRAINT CalculoRebateImpactaPL_Cliente_FK1 FOREIGN KEY(IdCliente)
	REFERENCES Cliente (IdCliente)
END
GO
	
IF NOT EXISTS (select 1 from sysconstraints where object_name(id) = 'IncorporacaoCisaoFusaoBolsa' and object_name(constid) = 'IncorporacaoCisaoFusaoBolsa_AtivoBolsa_FK1')
BEGIN
	ALTER TABLE IncorporacaoCisaoFusaoBolsa  WITH CHECK ADD  CONSTRAINT IncorporacaoCisaoFusaoBolsa_AtivoBolsa_FK1 FOREIGN KEY(CdAtivoBolsa)
	REFERENCES AtivoBolsa (CdAtivoBolsa)
END
GO
	
IF NOT EXISTS (select 1 from sysconstraints where object_name(id) = 'IncorporacaoCisaoFusaoBolsaDetalhe' and object_name(constid) = 'IncorporacaoCisaoFusaoBolsaDetalhe_AtivoBolsa_FK1')
BEGIN
	ALTER TABLE IncorporacaoCisaoFusaoBolsaDetalhe  WITH CHECK ADD  CONSTRAINT IncorporacaoCisaoFusaoBolsaDetalhe_AtivoBolsa_FK1 FOREIGN KEY(CdAtivoBolsa)
	REFERENCES AtivoBolsa (CdAtivoBolsa)
END
GO
	
IF NOT EXISTS (select 1 from sysconstraints where object_name(id) = 'EventoFundo' and object_name(constid) = 'EventoFundo_Carteira_FK1')
BEGIN
	ALTER TABLE EventoFundo  WITH CHECK ADD  CONSTRAINT EventoFundo_Carteira_FK1 FOREIGN KEY(IdCarteiraOrigem)
	REFERENCES Carteira (IdCarteira)	
END
GO
	
IF NOT EXISTS (select 1 from sysconstraints where object_name(id) = 'EventoFundo' and object_name(constid) = 'EventoFundo_Carteira_FK2')
BEGIN
	ALTER TABLE EventoFundo  WITH CHECK ADD  CONSTRAINT EventoFundo_Carteira_FK2 FOREIGN KEY(IdCarteiraDestino)
	REFERENCES Carteira (IdCarteira)
END
GO
	
IF NOT EXISTS (select 1 from sysconstraints where object_name(id) = 'EventoFundoCautela' and object_name(constid) = 'EventoFundoCautela_EventoFundo_FK1')
BEGIN
	ALTER TABLE EventoFundoCautela  WITH CHECK ADD  CONSTRAINT EventoFundoCautela_EventoFundo_FK1 FOREIGN KEY(IdEventoFundo)	
	REFERENCES EventoFundo (IdEventoFundo)
END
GO
	
IF NOT EXISTS (select 1 from sysconstraints where object_name(id) = 'EventoFundoSwap' and object_name(constid) = 'EventoFundoSwap_EventoFundo_FK1')
BEGIN
	ALTER TABLE EventoFundoSwap  WITH CHECK ADD  CONSTRAINT EventoFundoSwap_EventoFundo_FK1 FOREIGN KEY(IdEventoFundo)
	REFERENCES EventoFundo (IdEventoFundo)
END
GO
	
IF NOT EXISTS (select 1 from sysconstraints where object_name(id) = 'EventoFundoRendaFixa' and object_name(constid) = 'EventoFundoRendaFixa_EventoFundo_FK1')
BEGIN
	ALTER TABLE EventoFundoRendaFixa  WITH CHECK ADD  CONSTRAINT EventoFundoRendaFixa_EventoFundo_FK1 FOREIGN KEY(IdEventoFundo)
	REFERENCES EventoFundo (IdEventoFundo)
END
GO
	
IF NOT EXISTS (select 1 from sysconstraints where object_name(id) = 'EventoFundoFundo' and object_name(constid) = 'EventoFundoFundo_EventoFundo_FK1')
BEGIN
	ALTER TABLE EventoFundoFundo  WITH CHECK ADD  CONSTRAINT EventoFundoFundo_EventoFundo_FK1 FOREIGN KEY(IdEventoFundo)
	REFERENCES EventoFundo (IdEventoFundo)
END
GO
	
IF NOT EXISTS (select 1 from sysconstraints where object_name(id) = 'EventoFundoLiquidacao' and object_name(constid) = 'EventoFundoLiquidacao_EventoFundo_FK1')
BEGIN 
	ALTER TABLE EventoFundoLiquidacao  WITH CHECK ADD  CONSTRAINT EventoFundoLiquidacao_EventoFundo_FK1 FOREIGN KEY(IdEventoFundo)
	REFERENCES EventoFundo (IdEventoFundo)
END
GO
	
IF NOT EXISTS (select 1 from sysconstraints where object_name(id) = 'EventoFundoCaixa' and object_name(constid) = 'EventoFundoCaixa_EventoFundo_FK1')
BEGIN 
	ALTER TABLE EventoFundoCaixa  WITH CHECK ADD  CONSTRAINT EventoFundoCaixa_EventoFundo_FK1 FOREIGN KEY(IdEventoFundo)
	REFERENCES EventoFundo (IdEventoFundo)
END
GO
	
IF NOT EXISTS (select 1 from sysconstraints where object_name(id) = 'EventoFundoMovimentoCautelas' and object_name(constid) = 'EventoFundoMovimentoCautelas_EventoFundo_FK1')
BEGIN 
	ALTER TABLE EventoFundoMovimentoCautelas  WITH CHECK ADD  CONSTRAINT EventoFundoMovimentoCautelas_EventoFundo_FK1 FOREIGN KEY(IdEventoFundo)
	REFERENCES EventoFundo (IdEventoFundo)
END
GO
	
IF NOT EXISTS (select 1 from sysconstraints where object_name(id) = 'EventoFundoMovimentoAtivos' and object_name(constid) = 'EventoFundoMovimentoAtivos_EventoFundo_FK1')
BEGIN 
	ALTER TABLE EventoFundoMovimentoAtivos  WITH CHECK ADD  CONSTRAINT EventoFundoMovimentoAtivos_EventoFundo_FK1 FOREIGN KEY(IdEventoFundo)
	REFERENCES EventoFundo (IdEventoFundo)
END
GO
	
IF NOT EXISTS (select 1 from sysconstraints where object_name(id) = 'EventoFundoPosicaoCautelas' and object_name(constid) = 'EventoFundoPosicaoCautelas_EventoFundo_FK1')
BEGIN
	ALTER TABLE EventoFundoPosicaoCautelas  WITH CHECK ADD  CONSTRAINT EventoFundoPosicaoCautelas_EventoFundo_FK1 FOREIGN KEY(IdEventoFundo) REFERENCES EventoFundo (IdEventoFundo)
END
GO
	
IF NOT EXISTS (select 1 from sysconstraints where object_name(id) = 'EventoFundoPosicaoAtivos' and object_name(constid) = 'EventoFundoPosicaoAtivos_EventoFundo_FK1')
BEGIN
	ALTER TABLE EventoFundoPosicaoAtivos  WITH CHECK ADD  CONSTRAINT EventoFundoPosicaoAtivos_EventoFundo_FK1 FOREIGN KEY(IdEventoFundo) REFERENCES EventoFundo (IdEventoFundo)
END
GO
	
IF EXISTS (select 1 from sysconstraints where object_name(id) = 'Cotista' and object_name(constid) = 'Pessoa_Cotista_FK1')
BEGIN
	ALTER TABLE Cotista DROP CONSTRAINT Pessoa_Cotista_FK1
END
GO
	
IF NOT EXISTS (select 1 from sysconstraints where object_name(id) = 'Cotista' and object_name(constid) = 'Cotista_IdPessoa_FK1')
BEGIN
	ALTER TABLE Cotista ADD CONSTRAINT Cotista_IdPessoa_FK1 FOREIGN KEY (IdPessoa) REFERENCES Pessoa(IdPessoa) on delete cascade
END
GO
	
IF EXISTS (select 1 from sysconstraints where object_name(id) = 'Cliente' and object_name(constid) = 'Pessoa_Cliente_FK1')
BEGIN
	ALTER TABLE Cliente DROP CONSTRAINT Pessoa_Cliente_FK1
END
GO
	
IF NOT EXISTS (select 1 from sysconstraints where object_name(id) = 'Cliente' and object_name(constid) = 'Cliente_IdPessoa_FK1')
BEGIN
	ALTER TABLE Cliente ADD CONSTRAINT Cliente_IdPessoa_FK1 FOREIGN KEY (IdPessoa) REFERENCES Pessoa(IdPessoa) on delete cascade
END
GO
	
IF NOT EXISTS (select 1 from sysconstraints where object_name(id) = 'Cotista' and object_name(constid) = 'Cotista_IdCarteira_FK1')
BEGIN
	ALTER TABLE Cotista ADD CONSTRAINT Cotista_IdCarteira_FK1 FOREIGN KEY (IdCarteira) REFERENCES Carteira(IdCarteira) on delete set null
END
GO
	
IF NOT EXISTS (select 1 from sysconstraints where object_name(id) = 'ClassesOffShore' and object_name(constid) = 'Classes_Carteira_FK1')
BEGIN
	ALTER TABLE [dbo].ClassesOffShore  WITH CHECK ADD  CONSTRAINT [Classes_Carteira_FK1] FOREIGN KEY([IdClassesOffShore])
	REFERENCES [dbo].[Carteira] ([IdCarteira])
END
GO
	
IF NOT EXISTS (select 1 from sysconstraints where object_name(id) = 'SeriesOffShore' and object_name(constid) = 'Serie_Classes_FK1')
BEGIN
	ALTER TABLE [dbo].SeriesOffShore  WITH CHECK ADD  CONSTRAINT [Serie_Classes_FK1] FOREIGN KEY([IdClassesOffShore])
	REFERENCES [dbo].[ClassesOffShore] ([IdClassesOffShore])
END
GO
	
if not exists(select 1 from syscolumns where id = object_id('OperacaoCotista') and name = 'IdContaCorrenteOrigem')
BEGIN
	ALTER TABLE OperacaoCotista ADD IdContaCorrenteOrigem int NULL
END	
	
IF NOT EXISTS (select 1 from sysconstraints where object_name(id) = 'OperacaoCotista' and object_name(constid) = 'Operacao_ContaCorrenteOrigem_FK1')
BEGIN
	ALTER TABLE [dbo].OperacaoCotista  WITH CHECK ADD  CONSTRAINT [Operacao_ContaCorrenteOrigem_FK1] FOREIGN KEY([IdContaCorrenteOrigem])
	REFERENCES [dbo].[ContaCorrente] ([IdConta])
END
GO
	
IF NOT EXISTS (select 1 from sysconstraints where object_name(id) = 'OperacaoCotista' and object_name(constid) = 'Operacao_ContaCorrenteDestino_FK1')
BEGIN
	ALTER TABLE [dbo].OperacaoCotista  WITH CHECK ADD  CONSTRAINT [Operacao_ContaCorrenteDestino_FK1] FOREIGN KEY([IdContaCorrenteDestino])
	REFERENCES [dbo].[ContaCorrente] ([IdConta])
END
GO
		
if exists(select 1 from syscolumns where id = object_id('OperacaoCotista') and name = 'IdContaCorrenteDestino')
BEGIN
 ALTER TABLE [dbo].[OperacaoCotista] DROP CONSTRAINT [Operacao_ContaCorrenteDestino_FK1]
 ALTER TABLE OperacaoCotista DROP COLUMN IdContaCorrenteDestino;
END 
GO

if exists(select 1 from syscolumns where id = object_id('OperacaoCotista') and name = 'IdContaCorrenteOrigem')
BEGIN
 ALTER TABLE [dbo].[OperacaoCotista] DROP CONSTRAINT [Operacao_ContaCorrenteOrigem_FK1]
 ALTER TABLE OperacaoCotista DROP COLUMN IdContaCorrenteOrigem;
END 
GO

IF NOT EXISTS (select 1 from sysconstraints where object_name(id) = 'OperacaoCotista' and object_name(constid) = 'Operacao_ContaCorrente_FK1')
BEGIN
 ALTER TABLE [dbo].OperacaoCotista  WITH CHECK ADD  CONSTRAINT [Operacao_ContaCorrente_FK1] FOREIGN KEY([IdContaCorrente])
 REFERENCES [dbo].[ContaCorrente] ([IdConta])
END
GO

if not exists(select 1 FROM sys.all_columns WHERE upper(name) = 'Codigo' AND default_object_id <> 0 AND OBJECT_NAME(object_id) = 'LocalCustodia')
BEGIN
	ALTER TABLE [dbo].[LocalCustodia] ADD CONSTRAINT DF_Codigo DEFAULT ('') FOR [Codigo]
END
GO
	
if not exists(select 1 FROM sys.all_columns WHERE upper(name) = 'CNPJ' AND default_object_id <> 0 AND OBJECT_NAME(object_id) = 'LocalCustodia')
BEGIN
	ALTER TABLE [dbo].[LocalCustodia] ADD CONSTRAINT DF_Cnpj DEFAULT ('') FOR [Cnpj]
END
GO

IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'V' AND NAME = 'view__GPS__Agenda_Fluxo_Ativos_Creditos') > 0
BEGIN
	drop view view__GPS__Agenda_Fluxo_Ativos_Creditos;
END	
go

CREATE VIEW [dbo].[view__GPS__Agenda_Fluxo_Ativos_Creditos] AS
SELECT Cl.Apelido, Cl.DataDia, HC.Data, HC.PLFechamento, TRF.Descricao, E.Nome, 0 RiscoFinal, PRF.Quantidade, PRF.ValorMercado, ARF.DataEvento, 
	CASE WHEN ARF.TipoEvento = 1 THEN 'Juros'
		 WHEN ARF.TipoEvento = 2 THEN 'Juros Correção'
		 WHEN ARF.TipoEvento = 3 THEN 'Amortização'
		 WHEN ARF.TipoEvento = 4 THEN 'Pagamento Principal'
		 WHEN ARF.TipoEvento = 8 THEN 'Amortização Corrigida'
		 ELSE 'Outros Eventos'
	END AS TipoEvento,
	ARF.Taxa, LRF.ValorBruto, '' Comentario, LRF.ValorIR, LRF.ValorIOF, LRF.ValorLiquido, cl.IdCliente
FROM Cliente Cl
	INNER JOIN HistoricoCota HC ON HC.IdCarteira = Cl.IdCliente
	INNER JOIN PosicaoRendaFixaHistorico PRF ON PRF.IdCliente = HC.IdCarteira AND PRF.DataHistorico = HC.Data
	INNER JOIN AgendaRendaFixa ARF ON ARF.IdTitulo = PRF.IdTitulo AND ARF.DataEvento >= PRF.DataHistorico
	INNER JOIN LiquidacaoRendaFixa LRF ON LRF.IdCliente = PRF.IdCliente AND LRF.IdTitulo = PRF.IdTitulo AND LRF.DataLiquidacao >= PRF.DataHistorico AND LRF.TipoLancamento = ARF.TipoEvento AND LRF.DataLiquidacao = ARF.DataPagamento
	INNER JOIN TituloRendaFixa TRF ON TRF.IdTitulo = LRF.IdTitulo
	INNER JOIN Emissor E ON E.IdEmissor = TRF.IdEmissor

UNION

SELECT Cl.Apelido, Cl.DataDia, HC.Data, HC.PLFechamento, PBC.CdAtivoBolsa, E.Nome, 0 RiscoFinal, PBC.Quantidade, PB.ValorMercado, PBC.DataLancamento, 
	CASE WHEN PBC.TipoProvento = 10 THEN 'Dividendo'
		 WHEN PBC.TipoProvento = 11 THEN 'Restituição Capital'
		 WHEN PBC.TipoProvento = 12 THEN 'Bonificação Financeira'
		 WHEN PBC.TipoProvento = 13 THEN 'Juros sobre Capital'
		 WHEN PBC.TipoProvento = 14 THEN 'Rendimento'
		 WHEN PBC.TipoProvento = 16 THEN 'Juros'
		 ELSE 'Outros Eventos'
	END AS TipoEvento,
	0 Percentual, PBC.Valor, '' Comentario, (PBC.Valor * PBC.PercentualIR) ValorIR, 0 OutrasTaxas, (PBC.Valor * (1 - PBC.PercentualIR)) ValorLiquido, Cl.IdCliente
FROM Cliente Cl
	INNER JOIN HistoricoCota HC ON HC.IdCarteira = Cl.IdCliente
	INNER JOIN PosicaoBolsaHistorico PB ON PB.IdCliente = HC.IdCarteira AND PB.DataHistorico = HC.Data
	INNER JOIN ProventoBolsaCliente PBC ON PBC.IdCliente = PB.IdCliente AND PBC.DataPagamento >= PB.DataHistorico
	INNER JOIN AtivoBolsa AB ON AB.CdAtivoBolsa = PBC.CdAtivoBolsa
	INNER JOIN Emissor E ON E.IdEmissor = AB.IdEmissor;

GO

--TRIGGER FUNDO
IF EXISTS (select * from sys.objects where [type] = 'TR' and [name] = 'trgPosicaoFundo')
begin
	drop trigger trgPosicaoFundo;
END
	
--TRIGGER SWAP	
IF EXISTS (select * from sys.objects where [type] = 'TR' and [name] = 'trgPosicaoSwap')
begin
	drop trigger trgPosicaoSwap;
END

--TRIGGER COTISTA
IF EXISTS (select * from sys.objects where [type] = 'TR' and [name] = 'trgPosicaoCotista')
begin
	drop trigger trgPosicaoCotista;	
END

if not exists(select 1 from syscolumns where id = object_id('Indice') and name = 'IdCurva')
BEGIN	
	ALTER TABLE Indice ADD IdCurva	INT NULL;
	ALTER TABLE [dbo].Indice  WITH CHECK ADD  CONSTRAINT [Indice_Curva_FK] FOREIGN KEY([IdCurva])  REFERENCES [dbo].[CurvaRendaFixa] ([IdCurvaRendaFixa]);
END 
GO
	
IF NOT EXISTS(SELECT 1 FROM SYSCOLUMNS WHERE ID= OBJECT_ID('SeriesOffShore') AND NAME = 'VlCotaBase')
BEGIN
	ALTER TABLE SeriesOffShore add VlCotaBase Decimal(16,2);
END
GO

if (select count(*) from SYSCOLUMNS WHERE ID = (SELECT ID FROM SYSOBJECTS WHERE  NAME = 'Indice' ) and name = 'TabelaBDS') = 0
begin
alter table indice
add TabelaBDS varchar(50) 
end 
GO

if (select count(*) from SYSCOLUMNS WHERE ID = (SELECT ID FROM SYSOBJECTS WHERE  NAME = 'Indice' ) and name = 'IntervaloBDS') = 0
begin
alter table indice
add IntervaloBDS tinyint 
end 
GO

if (select count(*) from SYSCOLUMNS WHERE ID = (SELECT ID FROM SYSOBJECTS WHERE  NAME = 'Indice' ) and name = 'AtributoBDS') = 0
begin
alter table indice
add AtributoBDS int
end 
GO
IF EXISTS (select 1 from sysconstraints where object_name(id) = 'PosicaoRendaFixa' and object_name(constid) = 'Cliente_PosicaoRendaFixa_FK1')
BEGIN
	alter table PosicaoRendaFixa DROP constraint Cliente_PosicaoRendaFixa_FK1
END
GO	

IF EXISTS (select 1 from sysconstraints where object_name(id) = 'DetalheResgateFundo' and object_name(constid) = 'Cliente_DetalheResgateFundo_FK1')
BEGIN
	alter table DetalheResgateFundo DROP constraint Cliente_DetalheResgateFundo_FK1
END
GO	

IF EXISTS (select 1 from sysconstraints where object_name(id) = 'MemoriaCalculoRendaFixa' and object_name(constid) = 'MemCalRF_Operacao_FK')
BEGIN
	alter table MemoriaCalculoRendaFixa DROP constraint MemCalRF_Operacao_FK
END
GO	

IF EXISTS (select 1 from sysconstraints where object_name(id) = 'LogProcessamento' and object_name(constid) = 'Cliente_LogProcessamento_FK1')
BEGIN
	alter table LogProcessamento DROP constraint Cliente_LogProcessamento_FK1
END
GO	

IF EXISTS (select 1 from sysconstraints where object_name(id) = 'PerfilCliente' and object_name(constid) = 'FK__PerfilCli__IdCar__4A78EF25')
BEGIN
	alter table PerfilCliente DROP constraint FK__PerfilCli__IdCar__4A78EF25
END
GO	

IF EXISTS (select 1 from sysconstraints where object_name(id) = 'FundoInvestimentoFormaCondominio' and object_name(constid) = 'FK__FundoInve__Fundo__163A3110')
BEGIN
	alter table FundoInvestimentoFormaCondominio DROP constraint FK__FundoInve__Fundo__163A3110
END
GO	


IF NOT EXISTS (select 1 from sysconstraints where object_name(id) = 'PosicaoRendaFixa' and object_name(constid) = 'Cliente_PosicaoRendaFixa_FK1')
BEGIN
	alter table PosicaoRendaFixa add constraint Cliente_PosicaoRendaFixa_FK1 FOREIGN KEY ( IdCliente ) references Cliente(IdCliente) ON DELETE CASCADE;
END
GO

IF NOT EXISTS (select 1 from sysconstraints where object_name(id) = 'DetalheResgateFundo' and object_name(constid) = 'Cliente_DetalheResgateFundo_FK1')
BEGIN
	alter table DetalheResgateFundo add constraint Cliente_DetalheResgateFundo_FK1 FOREIGN KEY ( IdCliente ) references Cliente(IdCliente) ON DELETE CASCADE;
END
GO

IF NOT EXISTS (select 1 from sysconstraints where object_name(id) = 'MemoriaCalculoRendaFixa' and object_name(constid) = 'MemCalRF_Operacao_FK')
BEGIN
	alter table MemoriaCalculoRendaFixa add constraint MemCalRF_Operacao_FK FOREIGN KEY ( IdOperacao ) references OperacaoRendaFixa(IdOperacao) ON DELETE CASCADE;
END
GO

IF NOT EXISTS (select 1 from sysconstraints where object_name(id) = 'LogProcessamento' and object_name(constid) = 'Cliente_LogProcessamento_FK1')
BEGIN
	ALTER TABLE LogProcessamento  WITH CHECK ADD  CONSTRAINT [Cliente_LogProcessamento_FK1] FOREIGN KEY(IdCliente)
	REFERENCES Cliente (IdCliente) ON DELETE CASCADE
END
GO

IF NOT EXISTS (SELECT 1 FROM sys.tables t INNER JOIN sys.sysconstraints dc ON t.object_id = dc.id INNER JOIN sys.columns c ON dc.id = c.object_id AND c.column_id = dc.colid where  object_name(c.object_id) = 'PerfilCliente' and c.Name = 'IdCarteira')
BEGIN 
	ALTER TABLE PerfilCliente  WITH CHECK ADD  CONSTRAINT [PerfilCliente_Carteira_FK1] FOREIGN KEY(IdCarteira)
	REFERENCES Carteira (IdCarteira) ON DELETE CASCADE
END	

IF EXISTS (select 1 from sysconstraints where object_name(id) = 'FundoInvestimentoFormaCondominio' and object_name(constid) = 'FK__FundoInve__Fundo__163A3110')
BEGIN
	alter table FundoInvestimentoFormaCondominio DROP constraint FK__FundoInve__Fundo__163A3110
END
GO	

IF NOT EXISTS (SELECT 1 FROM sys.tables t INNER JOIN sys.sysconstraints dc ON t.object_id = dc.id INNER JOIN sys.columns c ON dc.id = c.object_id AND c.column_id = dc.colid where  object_name(c.object_id) = 'FundoInvestimentoFormaCondominio' and c.Name = 'IdCarteira')
BEGIN 
	ALTER TABLE FundoInvestimentoFormaCondominio  WITH CHECK ADD  CONSTRAINT [FundoInvestimentoFormaCondominio_Carteira_FK1] FOREIGN KEY(IdCarteira)
	REFERENCES Carteira (IdCarteira) ON DELETE CASCADE
END	
go

IF EXISTS (select 1 from sysconstraints where object_name(id) = 'AgendaComeCotas' and object_name(constid) = 'FK__AgendaCom__IdCar__481C70BE')
BEGIN
	alter table AgendaComeCotas DROP constraint FK__AgendaCom__IdCar__481C70BE
END
GO	

IF NOT EXISTS (SELECT 1 FROM sys.tables t INNER JOIN sys.sysconstraints dc ON t.object_id = dc.id INNER JOIN sys.columns c ON dc.id = c.object_id AND c.column_id = dc.colid where  object_name(c.object_id) = 'AgendaComeCotas' and c.Name = 'IdCarteira')
BEGIN 
	ALTER TABLE AgendaComeCotas  WITH CHECK ADD  CONSTRAINT [AgendaComeCotas_Carteira_FK1] FOREIGN KEY(IdCarteira)
	REFERENCES Carteira (IdCarteira) ON DELETE CASCADE
END	
go



IF EXISTS (select 1 from sysconstraints where object_name(id) = 'CalculoAdministracaoAssociada' and object_name(constid) = 'Carteira_CalculoAdministracaoAssociada_FK1')
BEGIN
	alter table CalculoAdministracaoAssociada DROP constraint Carteira_CalculoAdministracaoAssociada_FK1
END
GO	


IF NOT EXISTS (select 1 from sysconstraints where object_name(id) = 'CalculoAdministracaoAssociada' and object_name(constid) = 'Carteira_CalculoAdministracaoAssociada_FK1')
BEGIN 
	ALTER TABLE CalculoAdministracaoAssociada  WITH CHECK ADD  CONSTRAINT Carteira_CalculoAdministracaoAssociada_FK1 FOREIGN KEY(IdCarteira)
	REFERENCES Carteira (IdCarteira) ON DELETE CASCADE
END	
go

IF EXISTS (select 1 from sysconstraints where object_name(id) = 'CalculoAdministracaoAssociadaHistorico' and object_name(constid) = 'Carteira_CalculoAdministracaoAssociadaHistorico_FK1')
BEGIN
	alter table CalculoAdministracaoAssociadaHistorico DROP constraint Carteira_CalculoAdministracaoAssociadaHistorico_FK1
END
GO	


IF NOT EXISTS (select 1 from sysconstraints where object_name(id) = 'CalculoAdministracaoAssociadaHistorico' and object_name(constid) = 'Carteira_CalculoAdministracaoAssociadaHistorico_FK1')
BEGIN 
	ALTER TABLE CalculoAdministracaoAssociadaHistorico  WITH CHECK ADD  CONSTRAINT Carteira_CalculoAdministracaoAssociadaHistorico_FK1 FOREIGN KEY(IdCarteira)
	REFERENCES Carteira (IdCarteira) ON DELETE CASCADE
END	
go

IF EXISTS (select 1 from sysconstraints where object_name(id) = 'DesenquadramentoTributario' and object_name(constid) = 'FK__Desenquad__Fundo__19169DBB')
BEGIN
	alter table DesenquadramentoTributario DROP constraint FK__Desenquad__Fundo__19169DBB
END
GO	

IF NOT EXISTS (SELECT 1 FROM sys.tables t INNER JOIN sys.sysconstraints dc ON t.object_id = dc.id INNER JOIN sys.columns c ON dc.id = c.object_id AND c.column_id = dc.colid where  object_name(c.object_id) = 'DesenquadramentoTributario' and c.Name = 'IdCarteira')
BEGIN 
	ALTER TABLE DesenquadramentoTributario  WITH CHECK ADD  CONSTRAINT [DesenquadramentoTributario_Carteira_FK1] FOREIGN KEY(IdCarteira)
	REFERENCES Carteira (IdCarteira) ON DELETE CASCADE
END	
go


IF EXISTS (select 1 from sysconstraints where object_name(id) = 'CalculoRebateImpactaPL' and object_name(constid) = 'CalculoRebateImpactaPL_Carteira_FK1')
BEGIN
	alter table CalculoRebateImpactaPL DROP constraint CalculoRebateImpactaPL_Carteira_FK1
END
GO	

IF not EXISTS (select 1 from sysconstraints where object_name(id) = 'CalculoRebateImpactaPL' and object_name(constid) = 'CalculoRebateImpactaPL_Carteira_FK1')
BEGIN 
	ALTER TABLE CalculoRebateImpactaPL  WITH CHECK ADD  CONSTRAINT CalculoRebateImpactaPL_Carteira_FK1 FOREIGN KEY(IdCarteira)
	REFERENCES Carteira (IdCarteira) ON DELETE CASCADE
END	
go

IF EXISTS (select 1 from sysconstraints where object_name(id) = 'EventoFundo' and object_name(constid) = 'EventoFundo_Carteira_FK1')
BEGIN
	alter table EventoFundo DROP constraint EventoFundo_Carteira_FK1
END
GO	

IF not EXISTS (select 1 from sysconstraints where object_name(id) = 'EventoFundo' and object_name(constid) = 'EventoFundo_Carteira_FK1')
BEGIN 
	ALTER TABLE EventoFundo  WITH CHECK ADD  CONSTRAINT EventoFundo_Carteira_FK1 FOREIGN KEY(IdCarteiraOrigem)
	REFERENCES Carteira (IdCarteira) ON DELETE CASCADE
END	
go


IF EXISTS (select 1 from sysconstraints where object_name(id) = 'EventoFundoCautela' and object_name(constid) = 'EventoFundoCautela_EventoFundo_FK1')
BEGIN
	alter table EventoFundoCautela DROP constraint EventoFundoCautela_EventoFundo_FK1
END
GO	

IF not EXISTS (select 1 from sysconstraints where object_name(id) = 'EventoFundoCautela' and object_name(constid) = 'EventoFundoCautela_EventoFundo_FK1')
BEGIN 
	ALTER TABLE EventoFundoCautela  WITH CHECK ADD  CONSTRAINT EventoFundoCautela_EventoFundo_FK1 FOREIGN KEY(IdEventoFundo)
	REFERENCES EventoFundo (IdEventoFundo) ON DELETE CASCADE
END	
go


IF EXISTS (select 1 from sysconstraints where object_name(id) = 'EventoFundoFundo' and object_name(constid) = 'EventoFundoFundo_EventoFundo_FK1')
BEGIN
	alter table EventoFundoFundo DROP constraint EventoFundoFundo_EventoFundo_FK1
END
GO	

IF not EXISTS (select 1 from sysconstraints where object_name(id) = 'EventoFundoFundo' and object_name(constid) = 'EventoFundoFundo_EventoFundo_FK1')
BEGIN 
	ALTER TABLE EventoFundoFundo  WITH CHECK ADD  CONSTRAINT EventoFundoFundo_EventoFundo_FK1 FOREIGN KEY(IdEventoFundo)
	REFERENCES EventoFundo (IdEventoFundo) ON DELETE CASCADE
END	
go

IF EXISTS (select 1 from sysconstraints where object_name(id) = 'EventoFundoLiquidacao' and object_name(constid) = 'EventoFundoLiquidacao_EventoFundo_FK1')
BEGIN
	alter table EventoFundoLiquidacao DROP constraint EventoFundoLiquidacao_EventoFundo_FK1
END
GO	

IF not EXISTS (select 1 from sysconstraints where object_name(id) = 'EventoFundoLiquidacao' and object_name(constid) = 'EventoFundoLiquidacao_EventoFundo_FK1')
BEGIN 
	ALTER TABLE EventoFundoLiquidacao  WITH CHECK ADD  CONSTRAINT EventoFundoLiquidacao_EventoFundo_FK1 FOREIGN KEY(IdEventoFundo)
	REFERENCES EventoFundo (IdEventoFundo) ON DELETE CASCADE
END	
go

IF EXISTS (select 1 from sysconstraints where object_name(id) = 'EventoFundoCaixa' and object_name(constid) = 'EventoFundoCaixa_EventoFundo_FK1')
BEGIN
	alter table EventoFundoCaixa DROP constraint EventoFundoCaixa_EventoFundo_FK1
END
GO	

IF not EXISTS (select 1 from sysconstraints where object_name(id) = 'EventoFundoCaixa' and object_name(constid) = 'EventoFundoCaixa_EventoFundo_FK1')
BEGIN 
	ALTER TABLE EventoFundoCaixa  WITH CHECK ADD  CONSTRAINT EventoFundoCaixa_EventoFundo_FK1 FOREIGN KEY(IdEventoFundo)
	REFERENCES EventoFundo (IdEventoFundo) ON DELETE CASCADE
END	
go

IF EXISTS (select 1 from sysconstraints where object_name(id) = 'ClassesOffShore' and object_name(constid) = 'Classes_Carteira_FK1')
BEGIN
	alter table ClassesOffShore DROP constraint Classes_Carteira_FK1
END
GO	

IF not EXISTS (select 1 from sysconstraints where object_name(id) = 'ClassesOffShore' and object_name(constid) = 'Classes_Carteira_FK1')
BEGIN 
	ALTER TABLE ClassesOffShore  WITH CHECK ADD  CONSTRAINT Classes_Carteira_FK1 FOREIGN KEY(IdClassesOffShore)
	REFERENCES Carteira (IdCarteira) ON DELETE CASCADE
END	
go

IF EXISTS (select 1 from sysconstraints where object_name(id) = 'DetalhePosicaoAfetadaRF' and object_name(constid) = 'DetalhePosAfetadaRF_OpRF_FK')
BEGIN
	alter table DetalhePosicaoAfetadaRF DROP constraint DetalhePosAfetadaRF_OpRF_FK
END
GO	

IF not EXISTS (select 1 from sysconstraints where object_name(id) = 'DetalhePosicaoAfetadaRF' and object_name(constid) = 'DetalhePosAfetadaRF_OpRF_FK')
BEGIN 
	ALTER TABLE DetalhePosicaoAfetadaRF  WITH CHECK ADD  CONSTRAINT DetalhePosAfetadaRF_OpRF_FK FOREIGN KEY(IdOperacao)
	REFERENCES OperacaoRendaFixa (IdOperacao) ON DELETE CASCADE
END	
go

IF EXISTS (select 1 from sysconstraints where object_name(id) = 'ClientePerfil' and object_name(constid) = 'Cliente_ClientePerfil_FK1')
BEGIN
	alter table ClientePerfil DROP constraint Cliente_ClientePerfil_FK1
END
GO	

IF not EXISTS (select 1 from sysconstraints where object_name(id) = 'ClientePerfil' and object_name(constid) = 'Cliente_ClientePerfil_FK1')
BEGIN 
	ALTER TABLE ClientePerfil  WITH CHECK ADD  CONSTRAINT Cliente_ClientePerfil_FK1 FOREIGN KEY(IdCliente)
	REFERENCES Cliente (IdCliente) ON DELETE CASCADE
END	
go

IF EXISTS (select 1 from sysconstraints where object_name(id) = 'BoletoRetroativo' and object_name(constid) = 'Cliente_BoletoRetroativo_FK1')
BEGIN
	alter table BoletoRetroativo DROP constraint Cliente_BoletoRetroativo_FK1
END
GO	

IF not EXISTS (select 1 from sysconstraints where object_name(id) = 'BoletoRetroativo' and object_name(constid) = 'Cliente_BoletoRetroativo_FK1')
BEGIN 
	ALTER TABLE BoletoRetroativo  WITH CHECK ADD  CONSTRAINT Cliente_BoletoRetroativo_FK1 FOREIGN KEY(IdCliente)
	REFERENCES Cliente (IdCliente) ON DELETE CASCADE
END	
go

IF EXISTS (select 1 from sysconstraints where object_name(id) = 'TemplateCotista' and object_name(constid) = 'Cotista_TemplateCotista_FK1')
BEGIN
	alter table TemplateCotista DROP constraint Cotista_TemplateCotista_FK1
END
GO	

IF not EXISTS (select 1 from sysconstraints where object_name(id) = 'TemplateCotista' and object_name(constid) = 'Cotista_TemplateCotista_FK1')
BEGIN 
	ALTER TABLE TemplateCotista  WITH CHECK ADD  CONSTRAINT Cotista_TemplateCotista_FK1 FOREIGN KEY(IdCotista)
	REFERENCES Cotista (IdCotista) ON DELETE CASCADE
END	
go

if exists(select 1 from syscolumns where id = object_id('TituloRendaFixa') and name = 'ExpTaxaEmissao')
BEGIN
	ALTER TABLE TituloRendaFixa drop column ExpTaxaEmissao
END
GO
	
if  EXISTS(select 1 from sysconstraints where object_name(id) = 'PosicaoCotistaHistorico' and object_name(constid) = 'Operacao_PosicaoCotistaHis_FK1')
Begin
	ALTER TABLE PosicaoCotistaHistorico
	DROP CONSTRAINT Operacao_PosicaoCotistaHis_FK1
END
GO	
	
if EXISTS(select 1 from sysconstraints where object_name(id) = 'PosicaoCotistaAbertura' and object_name(constid) = 'Operacao_PosicaoCotistaAbe_FK1')
Begin
	ALTER TABLE PosicaoCotistaAbertura
	DROP CONSTRAINT Operacao_PosicaoCotistaAbe_FK1
END
GO	
		
IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'TravamentoCotas') = 0
Begin
	CREATE TABLE TravamentoCotas
	(
		IdTravamentoCota int IDENTITY(1,1) NOT NULL,
		IdCarteira int NOT NULL,
		DataProcessamento datetime NOT NULL,
		TipoBloqueio int NOT NULL,
		ValorCota decimal (28,12) NOT NULL,
		ValorCotaCalculada decimal (28,12) NOT NULL,
		AjusteCompensacao decimal (16,2) NULL,
		CONSTRAINT TravamentoCotas_PK PRIMARY KEY CLUSTERED 
		(
			IdTravamentoCota ASC
		)
	)
END
GO

IF EXISTS (select 1 from sysconstraints where object_name(id) = 'TravamentoCotas' and object_name(constid) = 'Carteira_TravamentoCotas_FK1')
BEGIN
	alter table TravamentoCotas DROP constraint Carteira_TravamentoCotas_FK1
END
GO	


IF NOT EXISTS (select 1 from sysconstraints where object_name(id) = 'TravamentoCotas' and object_name(constid) = 'Carteira_TravamentoCotas_FK1')
BEGIN 
	ALTER TABLE TravamentoCotas  WITH CHECK ADD  CONSTRAINT Carteira_TravamentoCotas_FK1 FOREIGN KEY(IdCarteira)
	REFERENCES Carteira (IdCarteira) ON DELETE CASCADE
END	
go

IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'TransferenciaEntreCotista') = 0
Begin
	CREATE TABLE TransferenciaEntreCotista 
	( 	IdTransferenciaEntreCotista			INT PRIMARY KEY IDENTITY(1,1) NOT NULL, 	
		IdCarteira							INT NOT NULL,
		IdCotistaOrigem						INT NOT NULL,
		IdCotistaDestino	 				INT NOT NULL, 
		DataProcessamento 					datetime NOT NULL,
		IdPosicao		 					INT NOT NULL, 	
		IdOperacaoDeposito			 		INT NULL, 
		IdOperacaoRetirada		 			INT NULL
	) 		
	alter table TransferenciaEntreCotista add constraint TransEntreCotista_Carteira_FK FOREIGN KEY ( IdCarteira ) references Carteira(IdCarteira);
	alter table TransferenciaEntreCotista add constraint TransEntreCotista_CotOri_FK FOREIGN KEY ( IdCotistaOrigem ) references Cotista(IdCotista);
	alter table TransferenciaEntreCotista add constraint TransEntreCotista_CotDes_FK FOREIGN KEY ( IdCotistaDestino ) references Cotista(IdCotista);
end
GO

IF NOT EXISTS(select 1 from syscolumns where id = object_id('OperacaoEmprestimoBolsa') and name = 'PermiteDevolucaoAntecipada')
BEGIN
	ALTER TABLE dbo.OperacaoEmprestimoBolsa ADD PermiteDevolucaoAntecipada CHAR(1)
	CONSTRAINT DF_OpEmprestimoBolsa_DevolucaoAntecipada DEFAULT 'S' NOT NULL
END
GO

IF NOT EXISTS(select 1 from syscolumns where id = object_id('OperacaoEmprestimoBolsa') and name = 'DataInicialDevolucaoAntecipada')
BEGIN
	alter table OperacaoEmprestimoBolsa add DataInicialDevolucaoAntecipada datetime null;
	EXEC('Update OperacaoEmprestimoBolsa set DataInicialDevolucaoAntecipada = DataRegistro');
END
GO	

 IF NOT EXISTS(select 1 from syscolumns where id = object_id('PosicaoEmprestimoBolsa') and name = 'PermiteDevolucaoAntecipada')
BEGIN
	ALTER TABLE dbo.PosicaoEmprestimoBolsa ADD PermiteDevolucaoAntecipada CHAR(1)
	CONSTRAINT DF_PosEmprestimoBolsa_DevolucaoAntecipada DEFAULT 'S' NOT NULL
END
GO

IF NOT EXISTS(select 1 from syscolumns where id = object_id('PosicaoEmprestimoBolsa') and name = 'DataInicialDevolucaoAntecipada')
BEGIN
	alter table PosicaoEmprestimoBolsa add DataInicialDevolucaoAntecipada datetime null;
	EXEC('Update PosicaoEmprestimoBolsa set DataInicialDevolucaoAntecipada = DataRegistro');
END
GO	

 IF NOT EXISTS(select 1 from syscolumns where id = object_id('PosicaoEmprestimoBolsaHistorico') and name = 'PermiteDevolucaoAntecipada')
BEGIN
	ALTER TABLE dbo.PosicaoEmprestimoBolsaHistorico ADD PermiteDevolucaoAntecipada CHAR(1)
	CONSTRAINT DF_PosEmprestimoBolsaHistorico_DevolucaoAntecipada DEFAULT 'S' NOT NULL
END
GO

IF NOT EXISTS(select 1 from syscolumns where id = object_id('PosicaoEmprestimoBolsaHistorico') and name = 'DataInicialDevolucaoAntecipada')
BEGIN
	alter table PosicaoEmprestimoBolsaHistorico add DataInicialDevolucaoAntecipada datetime null;
	EXEC('Update PosicaoEmprestimoBolsaHistorico set DataInicialDevolucaoAntecipada = DataRegistro');
END
GO	

 IF NOT EXISTS(select 1 from syscolumns where id = object_id('PosicaoEmprestimoBolsaAbertura') and name = 'PermiteDevolucaoAntecipada')
BEGIN
	ALTER TABLE dbo.PosicaoEmprestimoBolsaAbertura ADD PermiteDevolucaoAntecipada CHAR(1)
	CONSTRAINT PosicaoEmprestimoBolsaAbertura_DevolucaoAntecipada DEFAULT 'S' NOT NULL
END
GO

IF NOT EXISTS(select 1 from syscolumns where id = object_id('PosicaoEmprestimoBolsaAbertura') and name = 'DataInicialDevolucaoAntecipada')
BEGIN
	alter table PosicaoEmprestimoBolsaAbertura add DataInicialDevolucaoAntecipada datetime null;
	EXEC('Update PosicaoEmprestimoBolsaAbertura set DataInicialDevolucaoAntecipada = DataRegistro');
END
GO	


IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'TabelaProgressivaVigencia') = 0
BEGIN

	CREATE TABLE TabelaProgressivaVigencia
	(
		 IdTabelaProgressiva INT identity NOT NULL,
		 Data datetime NOT NULL
		 CONSTRAINT PK_TabelaProgressivaVigencia primary key(IdTabelaProgressiva)	
	)
END
GO

IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'TabelaProgressivaValores') = 0
BEGIN

	CREATE TABLE TabelaProgressivaValores
	(
		 IdTabelaProgressiva INT NOT NULL,
		 Valor decimal(16,2) NOT NULL,
		 Aliquota decimal(5,2) NOT NULL
		 CONSTRAINT PK_TabelaProgressivaValores primary key(IdTabelaProgressiva, Valor)	
	)
	
	alter table TabelaProgressivaValores add constraint TabelaProgressivaValores_TabelaProgressivaVigencia_FK FOREIGN KEY ( IdTabelaProgressiva ) references TabelaProgressivaVigencia(IdTabelaProgressiva) ON DELETE CASCADE
END
GO

IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'TabelaRegressivaVigencia') = 0
BEGIN

	CREATE TABLE TabelaRegressivaVigencia
	(
		 IdTabelaRegressiva INT identity NOT NULL,
		 Data datetime NOT NULL
		 CONSTRAINT PK_TabelaRegressivaVigencia primary key(IdTabelaRegressiva)	
	)
END
GO

IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'TabelaRegressivaValores') = 0
BEGIN

	CREATE TABLE TabelaRegressivaValores
	(
		 IdTabelaRegressiva INT NOT NULL,
		 Periodo INT NOT NULL,
		 Aliquota decimal(5,2) NOT NULL
		 CONSTRAINT PK_TabelaRegressivaValores primary key(IdTabelaRegressiva, Periodo)	
	)
	
	alter table TabelaRegressivaValores add constraint TabelaRegressivaValores_TabelaRegressivaVigencia_FK FOREIGN KEY ( IdTabelaRegressiva ) references TabelaRegressivaVigencia(IdTabelaRegressiva) ON DELETE CASCADE
END
GO

IF EXISTS (select 1 from sysconstraints where object_name(id) = 'cliente' and object_name(constid) = 'Cliente_Pessoa_FK1')
BEGIN
	alter table cliente drop constraint Cliente_Pessoa_FK1;
END
GO

IF NOT EXISTS(select 1 from syscolumns where id = object_id('Indice') and name = 'CodigoBDS')
BEGIN
	ALTER TABLE Indice ADD CodigoBDS int NULL
END
GO

IF NOT EXISTS(select 1 from syscolumns where id = object_id('OperacaoBMF') and name = 'IdBoletaExterna')
BEGIN
	ALTER TABLE dbo.OperacaoBMF ADD IdBoletaExterna int NULL;
END
GO

IF NOT EXISTS(select 1 from syscolumns where id = object_id('OperacaoBolsa') and name = 'IdBoletaExterna')
BEGIN
	ALTER TABLE dbo.OperacaoBolsa ADD IdBoletaExterna int NULL;
END
GO

IF NOT EXISTS(select 1 from syscolumns where id = object_id('OperacaoFundo') and name = 'IdBoletaExterna')
BEGIN
	ALTER TABLE dbo.OperacaoFundo ADD IdBoletaExterna int NULL;
END
GO

IF NOT EXISTS(select 1 from syscolumns where id = object_id('OperacaoRendaFixa') and name = 'IdBoletaExterna')
BEGIN
	ALTER TABLE dbo.OperacaoRendaFixa ADD IdBoletaExterna int NULL;
END
GO

IF NOT EXISTS(select 1 from syscolumns where id = object_id('OperacaoEmprestimoBolsa') and name = 'IdBoletaExterna')
BEGIN
	ALTER TABLE dbo.OperacaoEmprestimoBolsa ADD IdBoletaExterna int NULL;
END
GO

IF NOT EXISTS(select 1 from syscolumns where id = object_id('OperacaoCotista') and name = 'IdBoletaExterna')
BEGIN
	ALTER TABLE dbo.OperacaoCotista ADD IdBoletaExterna int NULL;
END
GO


IF EXISTS (select 1 from sysconstraints where object_name(id) = 'OperacaoRendaFixa' and object_name(constid) = 'DF_OperacaoRendaFixa_Status')
BEGIN
	ALTER TABLE [dbo].[OperacaoRendaFixa] DROP  CONSTRAINT [DF_OperacaoRendaFixa_Status]
End
go

IF NOT EXISTS (select 1 from sysconstraints where object_name(id) = 'OperacaoRendaFixa' and object_name(constid) = 'DF_OperacaoRendaFixa_Status')
BEGIN
	ALTER TABLE [dbo].[OperacaoRendaFixa] ADD CONSTRAINT [DF_OperacaoRendaFixa_Status] DEFAULT ((1)) FOR [Status]
End
go

IF EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND name = 'CopyTables')
BEGIN
	DROP PROCEDURE CopyTables
end
GO

	CREATE PROCEDURE CopyTables
		@Banco_Origem varchar(200), @Banco_Destino nvarchar(200),
		@idClienteOrigem int, @idClienteDestino int,
		@mercadoBolsa bit, @mercadoBMF bit,
		@mercadoRendaFixa bit, @mercadoLiquidacao bit,
		@mercadoFundo bit, @mercadoHistoricoCota bit
	AS

	DECLARE @sql nvarchar(2000)

	BEGIN TRY
		BEGIN TRANSACTION

	--#region Cria Pessoa Destino se não Existir
	-- /*************************  Cria Pessoa Destino se não Existir ******************************************************************/
	SET @sql = ' IF NOT EXISTS (SELECT * FROM [' + @Banco_Destino + '].dbo.Pessoa where idpessoa = @idClienteDestino) ' + CHAR(13) +
			   ' BEGIN ' + CHAR(13) +
				   ' SELECT * INTO #TempTable FROM [' + @Banco_Origem + '].dbo.Pessoa where IdPessoa = @idClienteOrigem ' + CHAR(13) +
				   ' Update #TempTable set idPessoa = @idClienteDestino ' + CHAR(13) +
				   ' INSERT INTO [' + @Banco_Destino + '].dbo.Pessoa SELECT * FROM #TempTable ' + CHAR(13) +
				   ' DROP TABLE #TempTable ' + CHAR(13) +
			   ' END '
	print 'Pessoa'
	EXECUTE sp_executesql @sql, 
						  N'@idClienteOrigem int, @idClienteDestino int',
						  @idClienteOrigem = @idClienteOrigem, 
						  @idClienteDestino = @idClienteDestino
	---------------------------------------------------------------------------------------------------------------------------------------------------

	--#endregion
	--#region Cria Cliente Destino se não Existir 
	-- /*************************  Cria Cliente Destino se não Existir ******************************************************************/
	SET @sql = ' IF NOT EXISTS (SELECT * FROM [' + @Banco_Destino + '].dbo.Cliente where idcliente = @idClienteDestino) ' + CHAR(13) +
			   ' BEGIN ' + CHAR(13) +
				   ' SELECT * INTO #TempTable FROM [' + @Banco_Origem + '].dbo.Cliente where IdCliente = @idClienteOrigem ' + CHAR(13) +
				   ' Update #TempTable set idCliente = @idClienteDestino ' + CHAR(13) +
				   ' INSERT INTO [' + @Banco_Destino + '].dbo.Cliente SELECT * FROM #TempTable ' + CHAR(13) +
				   ' DROP TABLE #TempTable ' + CHAR(13) +
			   ' END '
	print 'cliente'
	EXECUTE sp_executesql @sql, 
						  N'@idClienteOrigem int, @idClienteDestino int',
						  @idClienteOrigem = @idClienteOrigem, 
						  @idClienteDestino = @idClienteDestino
	-----------------------------------------------------------------------------------------------------------------------------------------------------

	--#endregion
	--#region Cria Carteira Destino se não Existir
	-- /*************************  Cria Carteira Destino se não Existir ******************************************************************/
	SET @sql = ' IF NOT EXISTS (SELECT * FROM [' + @Banco_Destino + '].dbo.Carteira where idCarteira = @idClienteDestino) ' + CHAR(13) +
			   ' BEGIN ' + CHAR(13) +
				   ' SELECT * INTO #TempTable FROM [' + @Banco_Origem + '].dbo.Carteira where IdCarteira = @idClienteOrigem ' + CHAR(13) +
				   ' Update #TempTable set idCarteira = @idClienteDestino ' + CHAR(13) +
				   ' INSERT INTO [' + @Banco_Destino + '].dbo.Carteira SELECT * FROM #TempTable ' + CHAR(13) +
				   ' DROP TABLE #TempTable ' + CHAR(13) +
			   ' END '
	print 'Carteira'
	EXECUTE sp_executesql @sql, 
						  N'@idClienteOrigem int, @idClienteDestino int',
						  @idClienteOrigem = @idClienteOrigem, 
						  @idClienteDestino = @idClienteDestino
	----------------------------------------------------------------------------------------------------------------------------------------------------

	--#endregion
	--#region Cria ClienteBolsa Destino se não Existir
	-- /*************************  Cria ClienteBolsa Destino se não Existir ******************************************************************/
	SET @sql = ' IF NOT EXISTS (SELECT * FROM [' + @Banco_Destino + '].dbo.ClienteBolsa where idCliente = @idClienteDestino) ' + CHAR(13) +
			   ' BEGIN ' + CHAR(13) +
				   ' SELECT * INTO #TempTable FROM [' + @Banco_Origem + '].dbo.ClienteBolsa where idCliente = @idClienteOrigem ' + CHAR(13) +
				   ' Update #TempTable set idCliente = @idClienteDestino ' + CHAR(13) +
				   ' INSERT INTO [' + @Banco_Destino + '].dbo.ClienteBolsa SELECT * FROM #TempTable ' + CHAR(13) +
				   ' DROP TABLE #TempTable ' + CHAR(13) +
			   ' END '
	print 'ClienteBolsa'
	EXECUTE sp_executesql @sql, 
						  N'@idClienteOrigem int, @idClienteDestino int',
						  @idClienteOrigem = @idClienteOrigem, 
						  @idClienteDestino = @idClienteDestino
	----------------------------------------------------------------------------------------------------------------------------------------------------

	--#endregion
	--#region Cria ClienteBMF Destino se não Existir
	-- /*************************  Cria ClienteBMF Destino se não Existir ******************************************************************/
	SET @sql = ' IF NOT EXISTS (SELECT * FROM [' + @Banco_Destino + '].dbo.ClienteBMF where idCliente = @idClienteDestino) ' + CHAR(13) +
			   ' BEGIN ' + CHAR(13) +
				   ' SELECT * INTO #TempTable FROM [' + @Banco_Origem + '].dbo.ClienteBMF where idCliente = @idClienteOrigem ' + CHAR(13) +
				   ' Update #TempTable set idCliente = @idClienteDestino ' + CHAR(13) +
				   ' INSERT INTO [' + @Banco_Destino + '].dbo.ClienteBMF SELECT * FROM #TempTable ' + CHAR(13) +
				   ' DROP TABLE #TempTable ' + CHAR(13) +
			   ' END '
	print 'ClienteBMF'
	EXECUTE sp_executesql @sql, 
						  N'@idClienteOrigem int, @idClienteDestino int',
						  @idClienteOrigem = @idClienteOrigem, 
						  @idClienteDestino = @idClienteDestino
	----------------------------------------------------------------------------------------------------------------------------------------------------

	--#endregion
	--#region  Cria ClienteRendaFixa Destino se não Existir
	-- /*************************  Cria ClienteRendaFixa Destino se não Existir ******************************************************************/
	SET @sql = ' IF NOT EXISTS (SELECT * FROM [' + @Banco_Destino + '].dbo.ClienteRendaFixa where idCliente = @idClienteDestino) ' + CHAR(13) +
			   ' BEGIN ' + CHAR(13) +
				   ' SELECT * INTO #TempTable FROM [' + @Banco_Origem + '].dbo.ClienteRendaFixa where idCliente = @idClienteOrigem ' + CHAR(13) +
				   ' Update #TempTable set idCliente = @idClienteDestino ' + CHAR(13) +
				   ' INSERT INTO [' + @Banco_Destino + '].dbo.ClienteRendaFixa SELECT * FROM #TempTable ' + CHAR(13) +
				   ' DROP TABLE #TempTable ' + CHAR(13) +
			   ' END '
	print 'ClienteRendaFixa'
	EXECUTE sp_executesql @sql, 
						  N'@idClienteOrigem int, @idClienteDestino int',
						  @idClienteOrigem = @idClienteOrigem, 
						  @idClienteDestino = @idClienteDestino
	---------------------------------------------------------------------------------------------------------------------------------------------------

	--#endregion
	--#region Cria ClienteInterface Destino se não Existir
	-- /*************************  Cria ClienteInterface Destino se não Existir ******************************************************************/
	SET @sql = ' IF NOT EXISTS (SELECT * FROM [' + @Banco_Destino + '].dbo.ClienteInterface where idCliente = @idClienteDestino) ' + CHAR(13) +
			   ' BEGIN ' + CHAR(13) +
				   ' SELECT * INTO #TempTable FROM [' + @Banco_Origem + '].dbo.ClienteInterface where idCliente = @idClienteOrigem ' + CHAR(13) +
				   ' Update #TempTable set idCliente = @idClienteDestino ' + CHAR(13) +
				   ' INSERT INTO [' + @Banco_Destino + '].dbo.ClienteInterface SELECT * FROM #TempTable ' + CHAR(13) +
				   ' DROP TABLE #TempTable ' + CHAR(13) +
			   ' END '
	print 'ClienteInterface'
	EXECUTE sp_executesql @sql, 
						  N'@idClienteOrigem int, @idClienteDestino int',
						  @idClienteOrigem = @idClienteOrigem, 
						  @idClienteDestino = @idClienteDestino
	---------------------------------------------------------------------------------------------------------------------------------------------------

	--#endregion
	--#region Data Dia e Status do Cliente Destino
	-- /*************************  Data Dia e Status do Cliente Destino ******************************************************************/
	-- Status:
	-- 1 - Aberto
	-- 2 - Fechado
	-- 3 - Fechado
	-- 4 - FechadoComErro      
	---------------------------------------------------------------------------------------
	DECLARE @dataDiaCliente DateTime, @statusCliente int

	SET @sql = ' SELECT @dataDiaClienteOut = datadia, ' + CHAR(13) +
			   '		@statusClienteOut = status ' + CHAR(13) +
			   ' FROM [' + @Banco_Destino + '].dbo.Cliente where idCliente = @idClienteDestino ' + CHAR(13)
			   
	EXECUTE sp_executesql @sql, 
						  N'@idClienteDestino int, @dataDiaClienteOut DateTime OUTPUT, @statusClienteOut int OUTPUT',
						  @idClienteDestino = @idClienteDestino,
						  @dataDiaClienteOut=@dataDiaCliente OUTPUT, @statusClienteOut=@statusCliente OUTPUT
											  
	set @dataDiaCliente=CONVERT(varchar(8), @dataDiaCliente, 112) -- Tira o Time
	print @dataDiaCliente
	print @statusCliente
	--#endregion
	---------------------------------------------------------------------------------------------------------------------------------------------------

	--#region Mercado Bolsa
	-- /************************* Bolsa	******************************************************************/
	IF @mercadoBolsa = 1
	BEGIN

	-- /*************************  PosicaoBolsa	******************************************************************/
	SET @sql = ' SELECT * INTO #TempTable FROM [' + @Banco_Origem + '].dbo.PosicaoBolsa where IdCliente = @idClienteOrigem ' +
			   ' ALTER TABLE #TempTable DROP COLUMN idPosicao ' +
			   ' Update #TempTable set idCliente = @idClienteDestino ' +
			   ' INSERT INTO [' + @Banco_Destino + '].dbo.PosicaoBolsa SELECT * FROM #TempTable ' +		   
			   ' DROP TABLE #TempTable '
	print 'PosicaoBolsa'
	EXECUTE sp_executesql @sql, 
						  N'@idClienteOrigem int, @idClienteDestino int',
						  @idClienteOrigem = @idClienteOrigem, 
						  @idClienteDestino = @idClienteDestino
	-- /********************************************************************************************************************/
	-- /************************* PosicaoBolsaAbertura	******************************************************************/
	SET @sql = ' DECLARE @i int ' + CHAR(13) +
			   ' SELECT @i = isnull( max(idposicao),0) from [' + @Banco_Destino + '].dbo.PosicaoBolsaAbertura' + CHAR(13) +
							   
			   ' SELECT * INTO #TempTable FROM ( ' + CHAR(13) +
			   
			   ' SELECT ( ROW_NUMBER() OVER(ORDER BY P.idPosicao ASC)) + @i as Row, P.* 
				 from [' + @Banco_Origem + '].dbo.PosicaoBolsaAbertura P, [' + @Banco_Origem + '].dbo.PosicaoBolsaAbertura P1
				 where P.idPosicao = P1.idPosicao and P.datahistorico = P1.datahistorico and P.IdCliente = @idClienteOrigem'
				 
	IF @statusCliente = 3 -- Fechado
	BEGIN
		SET @sql = @sql + ' and cast(CONVERT(varchar(8), P.datahistorico, 112) AS Datetime) <= @dataDiaCliente '
	END

	ELSE	-- Nao Fechado
	BEGIN
		SET @sql = @sql + ' and cast(CONVERT(varchar(8), P.datahistorico, 112) AS Datetime) < @dataDiaCliente '
	END

	SET @sql = @sql + 
	 
			   ') as T' + CHAR(13) +
			   
			   ' ALTER TABLE #TempTable DROP COLUMN idPosicao ' + CHAR(13) +
			   ' EXEC tempdb..sp_rename ''#TempTable.Row'', ''IdPosicao'', ''COLUMN'' ' + CHAR(13) +
			   ' Update #TempTable set idCliente = @idClienteDestino ' +
			   ' INSERT INTO [' + @Banco_Destino + '].dbo.PosicaoBolsaAbertura SELECT * FROM #TempTable ' + CHAR(13) +
			   ' DROP TABLE #TempTable '
	print 'PosicaoBolsaAbertura'  		   
	EXECUTE sp_executesql @sql, 
						  N'@idClienteOrigem int, @idClienteDestino int, @dataDiaCliente DateTime',
						  @idClienteOrigem = @idClienteOrigem, 
						  @idClienteDestino = @idClienteDestino,
						  @dataDiaCliente = @dataDiaCliente
	-- /********************************************************************************************************************/
	-- /************************* PosicaoBolsaHistorico	******************************************************************/
	SET @sql = ' DECLARE @i int ' + CHAR(13) +
			   ' SELECT @i = isnull( max(idposicao),0) from [' + @Banco_Destino + '].dbo.PosicaoBolsaHistorico' + CHAR(13) +
							   
			   ' SELECT * INTO #TempTable FROM ( ' + CHAR(13) +
			   
			   ' SELECT ( ROW_NUMBER() OVER(ORDER BY P.idPosicao ASC)) + @i as Row, P.* 
				 from [' + @Banco_Origem + '].dbo.PosicaoBolsaHistorico P, [' + @Banco_Origem + '].dbo.PosicaoBolsaHistorico P1
				 where P.idPosicao = P1.idPosicao and P.datahistorico = P1.datahistorico and P.IdCliente = @idClienteOrigem'
				 
	IF @statusCliente = 3 -- Fechado
	BEGIN
		SET @sql = @sql + ' and cast(CONVERT(varchar(8), P.datahistorico, 112) AS Datetime) <= @dataDiaCliente '
	END

	ELSE	-- Nao Fechado
	BEGIN
		SET @sql = @sql + ' and cast(CONVERT(varchar(8), P.datahistorico, 112) AS Datetime) < @dataDiaCliente '
	END

	SET @sql = @sql + 			 
				 
			   ') as T' + CHAR(13) +
			   
			   ' ALTER TABLE #TempTable DROP COLUMN idPosicao ' + CHAR(13) +
			   ' EXEC tempdb..sp_rename ''#TempTable.Row'', ''IdPosicao'', ''COLUMN'' ' + CHAR(13) +
			   ' Update #TempTable set idCliente = @idClienteDestino ' +
			   ' INSERT INTO [' + @Banco_Destino + '].dbo.PosicaoBolsaHistorico SELECT * FROM #TempTable ' + CHAR(13) +
			   ' DROP TABLE #TempTable '
	print 'PosicaoBolsaHistorico'		  		   
	EXECUTE sp_executesql @sql, 
						  N'@idClienteOrigem int, @idClienteDestino int, @dataDiaCliente DateTime',
						  @idClienteOrigem = @idClienteOrigem, 
						  @idClienteDestino = @idClienteDestino,
						  @dataDiaCliente = @dataDiaCliente
	-- /********************************************************************************************************************


	--/*************************  PosicaoEmprestimoBolsa ******************************************************************/
	SET @sql = ' SELECT * INTO #TempTable FROM [' + @Banco_Origem + '].dbo.PosicaoEmprestimoBolsa where IdCliente = @idClienteOrigem ' +
			   ' ALTER TABLE #TempTable DROP COLUMN idPosicao ' +
			   ' Update #TempTable set idCliente = @idClienteDestino ' +
			   ' INSERT INTO [' + @Banco_Destino + '].dbo.PosicaoEmprestimoBolsa SELECT * FROM #TempTable ' +
			   ' DROP TABLE #TempTable '
	print 'PosicaoEmprestimoBolsa'
	EXECUTE sp_executesql @sql, 
						  N'@idClienteOrigem int, @idClienteDestino int',
						  @idClienteOrigem = @idClienteOrigem, 
						  @idClienteDestino = @idClienteDestino
	--/********************************************************************************************************************/
	--/*************************  PosicaoEmprestimoBolsaAbertura  *****************************************************/
	SET @sql = ' DECLARE @i int ' + CHAR(13) +
			   ' SELECT @i = isnull( max(idposicao),0) from [' + @Banco_Destino + '].dbo.PosicaoEmprestimoBolsaAbertura' + CHAR(13) +
							   
			   ' SELECT * INTO #TempTable FROM ( ' + CHAR(13) +
			   
			   ' SELECT ( ROW_NUMBER() OVER(ORDER BY P.idPosicao ASC)) + @i as Row, P.* 
				 from [' + @Banco_Origem + '].dbo.PosicaoEmprestimoBolsaAbertura P, [' + @Banco_Origem + '].dbo.PosicaoEmprestimoBolsaAbertura P1
				 where P.idPosicao = P1.idPosicao and P.datahistorico = P1.datahistorico and P.IdCliente = @idClienteOrigem'

	IF @statusCliente = 3 -- Fechado
	BEGIN
		SET @sql = @sql + ' and cast(CONVERT(varchar(8), P.datahistorico, 112) AS Datetime) <= @dataDiaCliente '
	END

	ELSE	-- Nao Fechado
	BEGIN
		SET @sql = @sql + ' and cast(CONVERT(varchar(8), P.datahistorico, 112) AS Datetime) < @dataDiaCliente '
	END

	SET @sql = @sql + 

			   ') as T' + CHAR(13) +
			   
			   ' ALTER TABLE #TempTable DROP COLUMN idPosicao ' + CHAR(13) +
			   ' EXEC tempdb..sp_rename ''#TempTable.Row'', ''IdPosicao'', ''COLUMN'' ' + CHAR(13) +
			   ' Update #TempTable set idCliente = @idClienteDestino ' +
			   ' INSERT INTO [' + @Banco_Destino + '].dbo.PosicaoEmprestimoBolsaAbertura SELECT * FROM #TempTable ' + CHAR(13) +
			   ' DROP TABLE #TempTable '
	print 'PosicaoEmprestimoBolsaAbertura'		  		   
	EXECUTE sp_executesql @sql, 
						  N'@idClienteOrigem int, @idClienteDestino int, @dataDiaCliente DateTime',
						  @idClienteOrigem = @idClienteOrigem, 
						  @idClienteDestino = @idClienteDestino,
						  @dataDiaCliente = @dataDiaCliente
	--/********************************************************************************************************************/
	--/*************************    PosicaoEmprestimoBolsaHistorico	*****************************************************/
	SET @sql = ' DECLARE @i int ' + CHAR(13) +
			   ' SELECT @i = isnull( max(idposicao),0) from [' + @Banco_Destino + '].dbo.PosicaoEmprestimoBolsaHistorico' + CHAR(13) +
							   
			   ' SELECT * INTO #TempTable FROM ( ' + CHAR(13) +
			   
			   ' SELECT ( ROW_NUMBER() OVER(ORDER BY P.idPosicao ASC)) + @i as Row, P.* 
				 from [' + @Banco_Origem + '].dbo.PosicaoEmprestimoBolsaHistorico P, [' + @Banco_Origem + '].dbo.PosicaoEmprestimoBolsaHistorico P1
				 where P.idPosicao = P1.idPosicao and P.datahistorico = P1.datahistorico and P.IdCliente =  @idClienteOrigem'
				 
	IF @statusCliente = 3 -- Fechado
	BEGIN
		SET @sql = @sql + ' and cast(CONVERT(varchar(8), P.datahistorico, 112) AS Datetime) <= @dataDiaCliente '
	END

	ELSE	-- Nao Fechado
	BEGIN
		SET @sql = @sql + ' and cast(CONVERT(varchar(8), P.datahistorico, 112) AS Datetime) < @dataDiaCliente '
	END

	SET @sql = @sql + 
							 
			   ') as T' + CHAR(13) +
			   
			   ' ALTER TABLE #TempTable DROP COLUMN idPosicao ' + CHAR(13) +
			   ' EXEC tempdb..sp_rename ''#TempTable.Row'', ''IdPosicao'', ''COLUMN'' ' + CHAR(13) +
			   ' Update #TempTable set idCliente = @idClienteDestino ' +
			   ' INSERT INTO [' + @Banco_Destino + '].dbo.PosicaoEmprestimoBolsaHistorico SELECT * FROM #TempTable ' + CHAR(13) +
			   ' DROP TABLE #TempTable '
	print 'PosicaoEmprestimoBolsaHistorico'		  		   
	EXECUTE sp_executesql @sql, 
						  N'@idClienteOrigem int, @idClienteDestino int, @dataDiaCliente DateTime',
						  @idClienteOrigem = @idClienteOrigem, 
						  @idClienteDestino = @idClienteDestino,
						  @dataDiaCliente = @dataDiaCliente
	--/********************************************************************************************************************


	--//*************************    OperacaoBolsa	******************************************************************/
	SET @sql = ' SELECT * INTO #TempTable FROM [' + @Banco_Origem + '].dbo.OperacaoBolsa where IdCliente = @idClienteOrigem ' +
			   ' ALTER TABLE #TempTable DROP COLUMN idOperacao ' +
			   ' Update #TempTable set idCliente = @idClienteDestino ' +
			   ' INSERT INTO [' + @Banco_Destino + '].dbo.OperacaoBolsa SELECT * FROM #TempTable ' +
			   ' DROP TABLE #TempTable '
	print 'OperacaoBolsa'
	EXECUTE sp_executesql @sql, 
						  N'@idClienteOrigem int, @idClienteDestino int',
						  @idClienteOrigem = @idClienteOrigem, 
						  @idClienteDestino = @idClienteDestino
	--/********************************************************************************************************************

	--//*************************    OperacaoEmprestimoBolsa	******************************************************************/
	SET @sql = ' SELECT * INTO #TempTable FROM [' + @Banco_Origem + '].dbo.OperacaoEmprestimoBolsa where IdCliente = @idClienteOrigem ' +
			   ' ALTER TABLE #TempTable DROP COLUMN idOperacao ' +
			   ' Update #TempTable set idCliente = @idClienteDestino ' +
			   ' INSERT INTO [' + @Banco_Destino + '].dbo.OperacaoEmprestimoBolsa SELECT * FROM #TempTable ' +
			   ' DROP TABLE #TempTable '
	print 'OperacaoEmprestimoBolsa'
	EXECUTE sp_executesql @sql, 
						  N'@idClienteOrigem int, @idClienteDestino int',
						  @idClienteOrigem = @idClienteOrigem, 
						  @idClienteDestino = @idClienteDestino
	--/********************************************************************************************************************

	--//*************************     OrdemBolsa	******************************************************************/
	SET @sql = ' SELECT * INTO #TempTable FROM [' + @Banco_Origem + '].dbo.OrdemBolsa where IdCliente = @idClienteOrigem ' +
			   ' ALTER TABLE #TempTable DROP COLUMN idOrdem ' +
			   ' Update #TempTable set idCliente = @idClienteDestino ' +
			   ' INSERT INTO [' + @Banco_Destino + '].dbo.OrdemBolsa SELECT * FROM #TempTable ' +
			   ' DROP TABLE #TempTable '
	print 'OrdemBolsa'		   
	EXECUTE sp_executesql @sql, 
						  N'@idClienteOrigem int, @idClienteDestino int',
						  @idClienteOrigem = @idClienteOrigem, 
						  @idClienteDestino = @idClienteDestino
	--/********************************************************************************************************************
	--//*************************     PerfilCorretagemBolsa	******************************************************************/
	SET @sql = ' DELETE FROM [' + @Banco_Destino + '].dbo.PerfilCorretagemBolsa where idCliente = @idClienteDestino ' +
			   ' SELECT * INTO #TempTable FROM [' + @Banco_Origem + '].dbo.PerfilCorretagemBolsa where idCliente = @idClienteOrigem ' +
			   ' Update #TempTable set idCliente = @idClienteDestino ' +
			   ' INSERT INTO [' + @Banco_Destino + '].dbo.PerfilCorretagemBolsa SELECT * FROM #TempTable ' +
			   ' DROP TABLE #TempTable '
	print 'PerfilCorretagemBolsa'
	EXECUTE sp_executesql @sql, 
						  N'@idClienteOrigem int, @idClienteDestino int',
						  @idClienteOrigem = @idClienteOrigem, 
						  @idClienteDestino = @idClienteDestino
	--/********************************************************************************************************************

	--//*************************     ProventoBolsaCliente	******************************************************************/
	SET @sql = ' SELECT * INTO #TempTable FROM [' + @Banco_Origem + '].dbo.ProventoBolsaCliente where IdCliente = @idClienteOrigem ' +
			   ' ALTER TABLE #TempTable DROP COLUMN idProvento ' +
			   ' Update #TempTable set idCliente = @idClienteDestino ' +
			   ' INSERT INTO [' + @Banco_Destino + '].dbo.ProventoBolsaCliente SELECT * FROM #TempTable ' +
			   ' DROP TABLE #TempTable '
	print 'ProventoBolsaCliente'		   
	EXECUTE sp_executesql @sql, 
						  N'@idClienteOrigem int, @idClienteDestino int',
						  @idClienteOrigem = @idClienteOrigem, 
						  @idClienteDestino = @idClienteDestino
	--/********************************************************************************************************************/
	END
	-- /*******************************************************************************************/

	--#endregion
	--#region Mercado BMF
	-- /************************* BMF	******************************************************************/
	IF @mercadoBMF = 1
	BEGIN

	-- /************************* PosicaoBMF	******************************************************************/
	SET @sql = ' SELECT * INTO #TempTable FROM [' + @Banco_Origem + '].dbo.PosicaoBMF where IdCliente = @idClienteOrigem ' +
			   ' ALTER TABLE #TempTable DROP COLUMN idPosicao ' +
			   ' Update #TempTable set idCliente = @idClienteDestino ' +
			   ' INSERT INTO [' + @Banco_Destino + '].dbo.PosicaoBMF SELECT * FROM #TempTable ' +
			   ' DROP TABLE #TempTable '
	print 'PosicaoBMF'
	EXECUTE sp_executesql @sql, 
						  N'@idClienteOrigem int, @idClienteDestino int',
						  @idClienteOrigem = @idClienteOrigem, 
						  @idClienteDestino = @idClienteDestino
	-- /********************************************************************************************************************/
	-- /************************* PosicaoBMFAbertura	******************************************************************/
	SET @sql = ' DECLARE @i int ' + CHAR(13) +
			   ' SELECT @i = isnull( max(idposicao),0) from [' + @Banco_Destino + '].dbo.PosicaoBMFAbertura' + CHAR(13) +
							   
			   ' SELECT * INTO #TempTable FROM ( ' + CHAR(13) +
			   
			   ' SELECT ( ROW_NUMBER() OVER(ORDER BY P.idPosicao ASC)) + @i as Row, P.* 
				 from [' + @Banco_Origem + '].dbo.PosicaoBMFAbertura P, [' + @Banco_Origem + '].dbo.PosicaoBMFAbertura P1
				 where P.idPosicao = P1.idPosicao and P.datahistorico = P1.datahistorico and P.IdCliente = @idClienteOrigem'
				 
	IF @statusCliente = 3 -- Fechado
	BEGIN
		SET @sql = @sql + ' and cast(CONVERT(varchar(8), P.datahistorico, 112) AS Datetime) <= @dataDiaCliente '
	END

	ELSE	-- Nao Fechado
	BEGIN
		SET @sql = @sql + ' and cast(CONVERT(varchar(8), P.datahistorico, 112) AS Datetime) < @dataDiaCliente '
	END

	SET @sql = @sql + 			 
							 
			   ') as T' + CHAR(13) +
			   
			   ' ALTER TABLE #TempTable DROP COLUMN idPosicao ' + CHAR(13) +
			   ' EXEC tempdb..sp_rename ''#TempTable.Row'', ''IdPosicao'', ''COLUMN'' ' + CHAR(13) +
			   ' Update #TempTable set idCliente = @idClienteDestino ' +
			   ' INSERT INTO [' + @Banco_Destino + '].dbo.PosicaoBMFAbertura SELECT * FROM #TempTable ' + CHAR(13) +
			   ' DROP TABLE #TempTable '
	print 'PosicaoBMFAbertura'
	EXECUTE sp_executesql @sql, 
						  N'@idClienteOrigem int, @idClienteDestino int, @dataDiaCliente DateTime',
						  @idClienteOrigem = @idClienteOrigem, 
						  @idClienteDestino = @idClienteDestino,
						  @dataDiaCliente = @dataDiaCliente
	-- /********************************************************************************************************************/
	-- /************************* PosicaoBMFHistorico	******************************************************************/
	SET @sql = ' DECLARE @i int ' + CHAR(13) +
			   ' SELECT @i = isnull( max(idposicao),0) from [' + @Banco_Destino + '].dbo.PosicaoBMFHistorico' + CHAR(13) +
							   
			   ' SELECT * INTO #TempTable FROM ( ' + CHAR(13) +
			   
			   ' SELECT ( ROW_NUMBER() OVER(ORDER BY P.idPosicao ASC)) + @i as Row, P.* 
				 from [' + @Banco_Origem + '].dbo.PosicaoBMFHistorico P, [' + @Banco_Origem + '].dbo.PosicaoBMFHistorico P1
				 where P.idPosicao = P1.idPosicao and P.datahistorico = P1.datahistorico and P.IdCliente = @idClienteOrigem'
				 
	IF @statusCliente = 3 -- Fechado
	BEGIN
		SET @sql = @sql + ' and cast(CONVERT(varchar(8), P.datahistorico, 112) AS Datetime) <= @dataDiaCliente '
	END

	ELSE	-- Nao Fechado
	BEGIN
		SET @sql = @sql + ' and cast(CONVERT(varchar(8), P.datahistorico, 112) AS Datetime) < @dataDiaCliente '
	END

	SET @sql = @sql + 			 
						
			   ') as T' + CHAR(13) +
			   
			   ' ALTER TABLE #TempTable DROP COLUMN idPosicao ' + CHAR(13) +
			   ' EXEC tempdb..sp_rename ''#TempTable.Row'', ''IdPosicao'', ''COLUMN'' ' + CHAR(13) +
			   ' Update #TempTable set idCliente = @idClienteDestino ' +
			   ' INSERT INTO [' + @Banco_Destino + '].dbo.PosicaoBMFHistorico SELECT * FROM #TempTable ' + CHAR(13) +
			   ' DROP TABLE #TempTable '
	print 'PosicaoBMFHistorico'
	EXECUTE sp_executesql @sql, 
						  N'@idClienteOrigem int, @idClienteDestino int, @dataDiaCliente DateTime',
						  @idClienteOrigem = @idClienteOrigem, 
						  @idClienteDestino = @idClienteDestino,
						  @dataDiaCliente = @dataDiaCliente
	-- /********************************************************************************************************************/

	--/*************************    OperacaoBMF	******************************************************************/
	SET @sql = ' SELECT * INTO #TempTable FROM [' + @Banco_Origem + '].dbo.OperacaoBMF where IdCliente = @idClienteOrigem ' +
			   ' ALTER TABLE #TempTable DROP COLUMN idOperacao ' +
			   ' Update #TempTable set idCliente = @idClienteDestino ' +
			   ' INSERT INTO [' + @Banco_Destino + '].dbo.OperacaoBMF SELECT * FROM #TempTable ' +
			   ' DROP TABLE #TempTable '
	print 'OperacaoBMF'
	EXECUTE sp_executesql @sql, 
						  N'@idClienteOrigem int, @idClienteDestino int',
						  @idClienteOrigem = @idClienteOrigem, 
						  @idClienteDestino = @idClienteDestino
	--/********************************************************************************************************************

	--//*************************     PerfilCorretagemBMF	******************************************************************/
	SET @sql = ' SELECT * INTO #TempTable FROM [' + @Banco_Origem + '].dbo.PerfilCorretagemBMF where IdCliente = @idClienteOrigem ' +
			   ' ALTER TABLE #TempTable DROP COLUMN idPerfilCorretagem ' +
			   ' Update #TempTable set idCliente = @idClienteDestino ' +
			   ' INSERT INTO [' + @Banco_Destino + '].dbo.PerfilCorretagemBMF SELECT * FROM #TempTable ' +
			   ' DROP TABLE #TempTable '
	print 'PerfilCorretagemBMF'		   
	EXECUTE sp_executesql @sql, 
						  N'@idClienteOrigem int, @idClienteDestino int',
						  @idClienteOrigem = @idClienteOrigem, 
						  @idClienteDestino = @idClienteDestino
	--/********************************************************************************************************************

	--//*************************     OrdemBMF	 ******************************************************************/
	SET @sql = ' SELECT * INTO #TempTable FROM [' + @Banco_Origem + '].dbo.OrdemBMF where IdCliente = @idClienteOrigem ' +
			   ' ALTER TABLE #TempTable DROP COLUMN idOrdem ' +
			   ' Update #TempTable set idCliente = @idClienteDestino ' +
			   ' INSERT INTO [' + @Banco_Destino + '].dbo.OrdemBMF SELECT * FROM #TempTable ' +
			   ' DROP TABLE #TempTable '
	print 'OrdemBMF'
	EXECUTE sp_executesql @sql, 
						  N'@idClienteOrigem int, @idClienteDestino int',
						  @idClienteOrigem = @idClienteOrigem, 
						  @idClienteDestino = @idClienteDestino
	--/********************************************************************************************************************
	END
	-- /*******************************************************************************************/

	--#endregion
	--#region Mercado RendaFixa
	-- /************************* RendaFixa	******************************************************************/
	IF @mercadoRendaFixa = 1
	BEGIN

	--/*************************  PosicaoRendaFixa	******************************************************************/
	SET @sql = ' SELECT * INTO #TempTable FROM [' + @Banco_Origem + '].dbo.PosicaoRendaFixa where IdCliente = @idClienteOrigem ' +
			   ' ALTER TABLE #TempTable DROP COLUMN idPosicao ' +
			   ' Update #TempTable set idCliente = @idClienteDestino ' +
			   ' INSERT INTO [' + @Banco_Destino + '].dbo.PosicaoRendaFixa SELECT * FROM #TempTable ' +
			   ' DROP TABLE #TempTable '
	print 'PosicaoRendaFixa'
	EXECUTE sp_executesql @sql, 
						  N'@idClienteOrigem int, @idClienteDestino int',
						  @idClienteOrigem = @idClienteOrigem, 
						  @idClienteDestino = @idClienteDestino
	--/********************************************************************************************************************/
	--/*************************  PosicaoRendaFixaAbertura	******************************************************************/
	SET @sql = ' DECLARE @i int ' + CHAR(13) +
			   ' SELECT @i = isnull( max(idposicao),0) from [' + @Banco_Destino + '].dbo.PosicaoRendaFixaAbertura' + CHAR(13) +
							   
			   ' SELECT * INTO #TempTable FROM ( ' + CHAR(13) +
			   
			   ' SELECT ( ROW_NUMBER() OVER(ORDER BY P.idPosicao ASC)) + @i as Row, P.* 
				 from [' + @Banco_Origem + '].dbo.PosicaoRendaFixaAbertura P, [' + @Banco_Origem + '].dbo.PosicaoRendaFixaAbertura P1
				 where P.idPosicao = P1.idPosicao and P.datahistorico = P1.datahistorico and P.IdCliente = @idClienteOrigem'
				 
	IF @statusCliente = 3 -- Fechado
	BEGIN
		SET @sql = @sql + ' and cast(CONVERT(varchar(8), P.datahistorico, 112) AS Datetime) <= @dataDiaCliente '
	END

	ELSE	-- Nao Fechado
	BEGIN
		SET @sql = @sql + ' and cast(CONVERT(varchar(8), P.datahistorico, 112) AS Datetime) < @dataDiaCliente '
	END

	SET @sql = @sql + 			 
				 
			   ') as T' + CHAR(13) +
			   
			   ' ALTER TABLE #TempTable DROP COLUMN idPosicao ' + CHAR(13) +
			   ' EXEC tempdb..sp_rename ''#TempTable.Row'', ''IdPosicao'', ''COLUMN'' ' + CHAR(13) +
			   ' Update #TempTable set idCliente = @idClienteDestino ' +
			   ' INSERT INTO [' + @Banco_Destino + '].dbo.PosicaoRendaFixaAbertura SELECT * FROM #TempTable ' + CHAR(13) +
			   ' DROP TABLE #TempTable '
	print 'PosicaoRendaFixaAbertura'
	EXECUTE sp_executesql @sql, 
						  N'@idClienteOrigem int, @idClienteDestino int, @dataDiaCliente DateTime',
						  @idClienteOrigem = @idClienteOrigem, 
						  @idClienteDestino = @idClienteDestino,
						  @dataDiaCliente = @dataDiaCliente
	--/********************************************************************************************************************/
	--/*************************  PosicaoRendaFixaHistorico	******************************************************************/
	SET @sql = ' DECLARE @i int ' + CHAR(13) +
			   ' SELECT @i = isnull( max(idposicao),0) from [' + @Banco_Destino + '].dbo.PosicaoRendaFixaHistorico' + CHAR(13) +
							   
			   ' SELECT * INTO #TempTable FROM ( ' + CHAR(13) +
			   
			   ' SELECT ( ROW_NUMBER() OVER(ORDER BY P.idPosicao ASC)) + @i as Row, P.* 
				 from [' + @Banco_Origem + '].dbo.PosicaoRendaFixaHistorico P, [' + @Banco_Origem + '].dbo.PosicaoRendaFixaHistorico P1
				 where P.idPosicao = P1.idPosicao and P.datahistorico = P1.datahistorico and P.IdCliente = @idClienteOrigem'
				 
	IF @statusCliente = 3 -- Fechado
	BEGIN
		SET @sql = @sql + ' and cast(CONVERT(varchar(8), P.datahistorico, 112) AS Datetime) <= @dataDiaCliente '
	END

	ELSE	-- Nao Fechado
	BEGIN
		SET @sql = @sql + ' and cast(CONVERT(varchar(8), P.datahistorico, 112) AS Datetime) < @dataDiaCliente '
	END

	SET @sql = @sql + 
										 
			   ') as T' + CHAR(13) +
			   
			   ' ALTER TABLE #TempTable DROP COLUMN idPosicao ' + CHAR(13) +
			   ' EXEC tempdb..sp_rename ''#TempTable.Row'', ''IdPosicao'', ''COLUMN'' ' + CHAR(13) +
			   ' Update #TempTable set idCliente = @idClienteDestino ' +
			   ' INSERT INTO [' + @Banco_Destino + '].dbo.PosicaoRendaFixaHistorico SELECT * FROM #TempTable ' + CHAR(13) +
			   ' DROP TABLE #TempTable '
	print 'PosicaoRendaFixaHistorico'		  		   
	EXECUTE sp_executesql @sql, 
						  N'@idClienteOrigem int, @idClienteDestino int, @dataDiaCliente DateTime',
						  @idClienteOrigem = @idClienteOrigem, 
						  @idClienteDestino = @idClienteDestino,
						  @dataDiaCliente = @dataDiaCliente
	--/********************************************************************************************************************/

	--//*************************    OperacaoRendaFixa	******************************************************************/
	SET @sql = ' SELECT * INTO #TempTable FROM [' + @Banco_Origem + '].dbo.OperacaoRendaFixa where IdCliente = @idClienteOrigem ' +
			   ' ALTER TABLE #TempTable DROP COLUMN idOperacao ' +
			   ' Update #TempTable set idCliente = @idClienteDestino ' +
			   ' INSERT INTO [' + @Banco_Destino + '].dbo.OperacaoRendaFixa SELECT * FROM #TempTable ' +
			   ' DROP TABLE #TempTable '
	print 'OperacaoRendaFixa'
	EXECUTE sp_executesql @sql, 
						  N'@idClienteOrigem int, @idClienteDestino int',
						  @idClienteOrigem = @idClienteOrigem, 
						  @idClienteDestino = @idClienteDestino
	--/********************************************************************************************************************/
	END
	-- /*******************************************************************************************/

	--#endregion
	--#region Liquidacao
	-- /************************* Liquidacao	******************************************************************/
	IF @mercadoLiquidacao = 1
	BEGIN

	--#region Cria ContaCorrente ClienteDestino se não houver e retorna o IdConta do cliente Destino
	DECLARE @idConta int

	--INSERT INTO contacorrente
	--SELECT null, null, 110, 110, 1, 'S', 1 
	--WHERE NOT EXISTS (select top 1 1 from contacorrente where idpessoa = 110)
	--SELECT top 1 idConta FROM contacorrente WHERE idpessoa = 110 order by contadefault desc

	SET @sql = ' INSERT INTO [' + @Banco_Destino + '].dbo.ContaCorrente ' + CHAR(13) +
			   ' SELECT null, null, @idClienteDestino, @idClienteDestino, ''S'', 1, null, null, null, null, null ' + CHAR(13) +
			   ' WHERE NOT EXISTS (SELECT top 1 1 FROM [' + @Banco_Destino + '].dbo.ContaCorrente where idpessoa = @idClienteDestino) ' + CHAR(13) +
			   ' SELECT TOP 1 @idContaOut = idConta ' + CHAR(13) +	
			   ' FROM [' + @Banco_Destino + '].dbo.ContaCorrente where idPessoa = @idClienteDestino ORDER BY ContaDefault DESC' + CHAR(13)
			   
	EXECUTE sp_executesql @sql, 
						  N'@idClienteDestino int, @idContaOut int OUTPUT',
						  @idClienteDestino = @idClienteDestino,
						  @idContaOut=@idConta OUTPUT
											  
	print @idConta
	--#endregion

	--#region Liquidacao
	SET @sql = ' SELECT * INTO #TempTable FROM [' + @Banco_Origem + '].dbo.Liquidacao where IdCliente = @idClienteOrigem ' +
			   ' ALTER TABLE #TempTable DROP COLUMN idLiquidacao ' +
			   ' Update #TempTable set idCliente = @idClienteDestino, IdConta = @idConta ' +
			   ' INSERT INTO [' + @Banco_Destino + '].dbo.Liquidacao SELECT * FROM #TempTable ' +
			   ' DROP TABLE #TempTable '
	print 'Liquidacao'		   
	EXECUTE sp_executesql @sql, 
						  N'@idClienteOrigem int, @idClienteDestino int, @idConta int', 
						  @idClienteOrigem = @idClienteOrigem, 
						  @idClienteDestino = @idClienteDestino,
						  @idConta = @idConta
	--#endregion

	--#region LiquidacaoAbertura
	--/*************************    LiquidacaoAbertura	******************************************************************/
	SET @sql = ' DECLARE @i int ' + CHAR(13) +
			   ' SELECT @i = isnull( max(idliquidacao),0) from [' + @Banco_Destino + '].dbo.LiquidacaoAbertura' + CHAR(13) +
							   
			   ' SELECT * INTO #TempTable FROM ( ' + CHAR(13) +
			   
			   ' SELECT ( ROW_NUMBER() OVER(ORDER BY L.idliquidacao ASC)) + @i as Row, L.* 
				 from [' + @Banco_Origem + '].dbo.LiquidacaoAbertura L, [' + @Banco_Origem + '].dbo.LiquidacaoAbertura L1
				 where L.idliquidacao = L1.idliquidacao and L.datahistorico = L1.datahistorico and L.IdCliente = @idClienteOrigem'
				 
	IF @statusCliente = 3 -- Fechado
	BEGIN
		SET @sql = @sql + ' and cast(CONVERT(varchar(8), L.datahistorico, 112) AS Datetime) <= @dataDiaCliente '
	END

	ELSE	-- Nao Fechado
	BEGIN
		SET @sql = @sql + ' and cast(CONVERT(varchar(8), L.datahistorico, 112) AS Datetime) < @dataDiaCliente '
	END

	SET @sql = @sql + 			 
							 
			   ') as T' + CHAR(13) +
			   
			   ' ALTER TABLE #TempTable DROP COLUMN idLiquidacao ' + CHAR(13) +
			   ' EXEC tempdb..sp_rename ''#TempTable.Row'', ''IdLiquidacao'', ''COLUMN'' ' + CHAR(13) +
			   ' Update #TempTable set idCliente = @idClienteDestino, IdConta = @idConta ' +
			   ' INSERT INTO [' + @Banco_Destino + '].dbo.LiquidacaoAbertura SELECT * FROM #TempTable ' + CHAR(13) +
			   ' DROP TABLE #TempTable '
			   
	print 'LiquidacaoAbertura'		  		   
	EXECUTE sp_executesql @sql, 
						  N'@idClienteOrigem int, @idClienteDestino int, @dataDiaCliente DateTime, @idConta int',
						  @idClienteOrigem = @idClienteOrigem, 
						  @idClienteDestino = @idClienteDestino,
						  @dataDiaCliente = @dataDiaCliente,
						  @idConta = @idConta
																  
	--/********************************************************************************************************************/

	--#endregion

	--#region LiquidacaoHistorico
	--/*************************    LiquidacaoHistorico	******************************************************************/
	SET @sql = ' DECLARE @i int ' + CHAR(13) +
			   ' SELECT @i = isnull( max(idliquidacao),0) from [' + @Banco_Destino + '].dbo.LiquidacaoHistorico' + CHAR(13) +
							   
			   ' SELECT * INTO #TempTable FROM ( ' + CHAR(13) +
			   
			   ' SELECT ( ROW_NUMBER() OVER(ORDER BY L.idliquidacao ASC)) + @i as Row, L.* 
				 from [' + @Banco_Origem + '].dbo.LiquidacaoHistorico L, [' + @Banco_Origem + '].dbo.LiquidacaoHistorico L1
				 where L.idliquidacao = L1.idliquidacao and L.datahistorico = L1.datahistorico and L.IdCliente = @idClienteOrigem'
				 
	IF @statusCliente = 3 -- Fechado
	BEGIN
		SET @sql = @sql + ' and cast(CONVERT(varchar(8), L.datahistorico, 112) AS Datetime) <= @dataDiaCliente '
	END

	ELSE	-- Nao Fechado
	BEGIN
		SET @sql = @sql + ' and cast(CONVERT(varchar(8), L.datahistorico, 112) AS Datetime) < @dataDiaCliente '
	END

	SET @sql = @sql + 			 
				 
			   ') as T' + CHAR(13) +
			   
			   ' ALTER TABLE #TempTable DROP COLUMN idLiquidacao ' + CHAR(13) +
			   ' EXEC tempdb..sp_rename ''#TempTable.Row'', ''IdLiquidacao'', ''COLUMN'' ' + CHAR(13) +
			   ' Update #TempTable set idCliente = @idClienteDestino, IdConta = @idConta ' +
			   ' INSERT INTO [' + @Banco_Destino + '].dbo.LiquidacaoHistorico SELECT * FROM #TempTable ' + CHAR(13) +
			   ' DROP TABLE #TempTable '

	print 'LiquidacaoHistorico'
	EXECUTE sp_executesql @sql, 
						  N'@idClienteOrigem int, @idClienteDestino int, @dataDiaCliente DateTime, @idConta int',
						  @idClienteOrigem = @idClienteOrigem, 
						  @idClienteDestino = @idClienteDestino,
						  @dataDiaCliente = @dataDiaCliente,
						  @idConta = @idConta                     
	--/********************************************************************************************************************/

	--#endregion

	--#region LiquidacaoFuturo
	--/*************************      LiquidacaoFuturo	*****************************************************************/
	SET @sql = ' DECLARE @i int ' + CHAR(13) +
			   ' SELECT @i = isnull( max(idliquidacao),0) from [' + @Banco_Destino + '].dbo.LiquidacaoFuturo' + CHAR(13) +
							   
			   ' SELECT * INTO #TempTable FROM ( ' + CHAR(13) +
			   
			   ' SELECT ( ROW_NUMBER() OVER(ORDER BY L.idliquidacao ASC)) + @i as Row, L.* 
				 from [' + @Banco_Origem + '].dbo.LiquidacaoFuturo L, [' + @Banco_Origem + '].dbo.LiquidacaoFuturo L1
				 where L.idliquidacao = L1.idliquidacao and L.datahistorico = L1.datahistorico and L.IdCliente = @idClienteOrigem'
				 
	IF @statusCliente = 3 -- Fechado
	BEGIN
		SET @sql = @sql + ' and cast(CONVERT(varchar(8), L.datahistorico, 112) AS Datetime) <= @dataDiaCliente '
	END

	ELSE	-- Nao Fechado
	BEGIN
		SET @sql = @sql + ' and cast(CONVERT(varchar(8), L.datahistorico, 112) AS Datetime) < @dataDiaCliente '
	END

	SET @sql = @sql + 			 
				 
			   ') as T' + CHAR(13) +
			   
			   ' ALTER TABLE #TempTable DROP COLUMN idLiquidacao ' + CHAR(13) +
			   ' EXEC tempdb..sp_rename ''#TempTable.Row'', ''IdLiquidacao'', ''COLUMN'' ' + CHAR(13) +
			   ' Update #TempTable set idCliente = @idClienteDestino, IdConta = @idConta ' +
			   ' INSERT INTO [' + @Banco_Destino + '].dbo.LiquidacaoFuturo SELECT * FROM #TempTable ' + CHAR(13) +
			   ' DROP TABLE #TempTable '

	print 'LiquidacaoFuturo'		  		   
	EXECUTE sp_executesql @sql, 
						  N'@idClienteOrigem int, @idClienteDestino int, @dataDiaCliente DateTime, @idConta int',
						  @idClienteOrigem = @idClienteOrigem, 
						  @idClienteDestino = @idClienteDestino,
						  @dataDiaCliente = @dataDiaCliente,
						  @idConta = @idConta                    
	--/********************************************************************************************************************

	--#endregion

	--#region SaldoCaixa
	--/*************************     SaldoCaixa	******************************************************************/
	IF @statusCliente = 3 -- Fechado
	BEGIN
		SET @sql = ' DELETE FROM [' + @Banco_Destino + '].dbo.SaldoCaixa where idCliente = @idClienteDestino ' +
				   ' and cast(CONVERT(varchar(8), data, 112) AS Datetime) <= @dataDiaCliente ' +
				   ' SELECT * INTO #TempTable FROM [' + @Banco_Origem + '].dbo.SaldoCaixa where idCliente = @idClienteOrigem ' +
				   ' and cast(CONVERT(varchar(8), data, 112) AS Datetime) <= @dataDiaCliente '
	END

	ELSE	-- Nao Fechado
	BEGIN
		SET @sql = ' DELETE FROM [' + @Banco_Destino + '].dbo.SaldoCaixa where idCliente = @idClienteDestino ' +
				   ' and cast(CONVERT(varchar(8), data, 112) AS Datetime) < @dataDiaCliente ' +
				   ' SELECT * INTO #TempTable FROM [' + @Banco_Origem + '].dbo.SaldoCaixa where idCliente = @idClienteOrigem ' +
				   ' and cast(CONVERT(varchar(8), data, 112) AS Datetime) < @dataDiaCliente '
	END

	SET @sql = @sql + 		   
					   
			   ' Update #TempTable set idCliente = @idClienteDestino, IdConta = @idConta ' +
			   ' INSERT INTO [' + @Banco_Destino + '].dbo.SaldoCaixa SELECT * FROM #TempTable ' +
			   ' DROP TABLE #TempTable '

	print 'SaldoCaixa'
	EXECUTE sp_executesql @sql, 
						  N'@idClienteOrigem int, @idClienteDestino int, @dataDiaCliente DateTime, @idConta int',
						  @idClienteOrigem = @idClienteOrigem, 
						  @idClienteDestino = @idClienteDestino,
						  @dataDiaCliente = @dataDiaCliente,
						  @idConta = @idConta
	--/********************************************************************************************************************/
	--#endregion

	END
	-- /*******************************************************************************************/

	--#endregion
	--#region Mercado Fundo
	-- /************************* Fundo	******************************************************************/
	IF @mercadoFundo = 1
	BEGIN

	--//*************************  PosicaoFundo	******************************************************************/
	SET @sql = ' SELECT * INTO #TempTable FROM [' + @Banco_Origem + '].dbo.PosicaoFundo where IdCliente = @idClienteOrigem ' +
			   ' ALTER TABLE #TempTable DROP COLUMN idPosicao ' +
			   ' Update #TempTable set IdCliente = @idClienteDestino ' +
			   ' INSERT INTO [' + @Banco_Destino + '].dbo.PosicaoFundo SELECT * FROM #TempTable ' +
			   ' DROP TABLE #TempTable '
	print 'PosicaoFundo'		   
	EXECUTE sp_executesql @sql, 
						  N'@idClienteOrigem int, @idClienteDestino int',
						  @idClienteOrigem = @idClienteOrigem, 
						  @idClienteDestino = @idClienteDestino
	--/********************************************************************************************************************/
	--/*************************  PosicaoFundoAbertura	******************************************************************/
	SET @sql = ' DECLARE @i int ' + CHAR(13) +
			   ' SELECT @i = isnull( max(idposicao),0) from [' + @Banco_Destino + '].dbo.PosicaoFundoAbertura' + CHAR(13) +
							   
			   ' SELECT * INTO #TempTable FROM ( ' + CHAR(13) +
			   
			   ' SELECT ( ROW_NUMBER() OVER(ORDER BY P.idPosicao ASC)) + @i as Row, P.* 
				 from [' + @Banco_Origem + '].dbo.PosicaoFundoAbertura P, [' + @Banco_Origem + '].dbo.PosicaoFundoAbertura P1
				 where P.idPosicao = P1.idPosicao and P.datahistorico = P1.datahistorico and P.IdCliente = @idClienteOrigem'
				 
	IF @statusCliente = 3 -- Fechado
	BEGIN
		SET @sql = @sql + ' and cast(CONVERT(varchar(8), P.datahistorico, 112) AS Datetime) <= @dataDiaCliente '
	END

	ELSE	-- Nao Fechado
	BEGIN
		SET @sql = @sql + ' and cast(CONVERT(varchar(8), P.datahistorico, 112) AS Datetime) < @dataDiaCliente '
	END

	SET @sql = @sql + 			 
				 
			   ') as T' + CHAR(13) +
			   
			   ' ALTER TABLE #TempTable DROP COLUMN idPosicao ' + CHAR(13) +
			   ' EXEC tempdb..sp_rename ''#TempTable.Row'', ''IdPosicao'', ''COLUMN'' ' + CHAR(13) +
			   ' Update #TempTable set IdCliente = @idClienteDestino ' +
			   ' INSERT INTO [' + @Banco_Destino + '].dbo.PosicaoFundoAbertura SELECT * FROM #TempTable ' + CHAR(13) +
			   ' DROP TABLE #TempTable '
	print 'PosicaoFundoAbertura'		  		   
	EXECUTE sp_executesql @sql, 
						  N'@idClienteOrigem int, @idClienteDestino int, @dataDiaCliente DateTime',
						  @idClienteOrigem = @idClienteOrigem, 
						  @idClienteDestino = @idClienteDestino,
						  @dataDiaCliente = @dataDiaCliente
	--/********************************************************************************************************************/
	--/*************************  PosicaoFundoHistorico	******************************************************************/
	SET @sql = ' DECLARE @i int ' + CHAR(13) +
			   ' SELECT @i = isnull( max(idposicao),0) from [' + @Banco_Destino + '].dbo.PosicaoFundoHistorico' + CHAR(13) +
							   
			   ' SELECT * INTO #TempTable FROM ( ' + CHAR(13) +
			   
			   ' SELECT ( ROW_NUMBER() OVER(ORDER BY P.idPosicao ASC)) + @i as Row, P.* 
				 from [' + @Banco_Origem + '].dbo.PosicaoFundoHistorico P, [' + @Banco_Origem + '].dbo.PosicaoFundoHistorico P1
				 where P.idPosicao = P1.idPosicao and P.datahistorico = P1.datahistorico and P.IdCliente = @idClienteOrigem'

	IF @statusCliente = 3 -- Fechado
	BEGIN
		SET @sql = @sql + ' and cast(CONVERT(varchar(8), P.datahistorico, 112) AS Datetime) <= @dataDiaCliente '
	END

	ELSE	-- Nao Fechado
	BEGIN
		SET @sql = @sql + ' and cast(CONVERT(varchar(8), P.datahistorico, 112) AS Datetime) < @dataDiaCliente '
	END

	SET @sql = @sql + 			 
				 
			   ') as T' + CHAR(13) +
			   
			   ' ALTER TABLE #TempTable DROP COLUMN idPosicao ' + CHAR(13) +
			   ' EXEC tempdb..sp_rename ''#TempTable.Row'', ''IdPosicao'', ''COLUMN'' ' + CHAR(13) +
			   ' Update #TempTable set IdCliente = @idClienteDestino ' +
			   ' INSERT INTO [' + @Banco_Destino + '].dbo.PosicaoFundoHistorico SELECT * FROM #TempTable ' + CHAR(13) +
			   ' DROP TABLE #TempTable '
	print 'PosicaoFundoHistorico'		  		   
	EXECUTE sp_executesql @sql, 
						  N'@idClienteOrigem int, @idClienteDestino int, @dataDiaCliente DateTime',
						  @idClienteOrigem = @idClienteOrigem, 
						  @idClienteDestino = @idClienteDestino,
						  @dataDiaCliente = @dataDiaCliente
	--/********************************************************************************************************************

	--//*************************    OperacaoFundo	******************************************************************/
	SET @sql = ' SELECT * INTO #TempTable FROM [' + @Banco_Origem + '].dbo.OperacaoFundo where IdCliente = @idClienteOrigem ' +
			   ' ALTER TABLE #TempTable DROP COLUMN idOperacao ' +
			   ' Update #TempTable set idCliente = @idClienteDestino ' +
			   ' INSERT INTO [' + @Banco_Destino + '].dbo.OperacaoFundo SELECT * FROM #TempTable ' +
			   ' DROP TABLE #TempTable '
	print 'OperacaoFundo'
	EXECUTE sp_executesql @sql, 
						  N'@idClienteOrigem int, @idClienteDestino int',
						  @idClienteOrigem = @idClienteOrigem, 
						  @idClienteDestino = @idClienteDestino
	--/********************************************************************************************************************

	--//*************************      PrejuizoFundo	******************************************************************/
	SET @sql = ' DELETE FROM [' + @Banco_Destino + '].dbo.PrejuizoFundo where idCliente = @idClienteDestino ' +
			   ' SELECT * INTO #TempTable FROM [' + @Banco_Origem + '].dbo.PrejuizoFundo where idCliente = @idClienteOrigem ' +
			   ' Update #TempTable set idCliente = @idClienteDestino ' +
			   ' INSERT INTO [' + @Banco_Destino + '].dbo.PrejuizoFundo SELECT * FROM #TempTable ' +
			   ' DROP TABLE #TempTable '
	print 'PrejuizoFundo'
	EXECUTE sp_executesql @sql, 
						  N'@idClienteOrigem int, @idClienteDestino int',
						  @idClienteOrigem = @idClienteOrigem, 
						  @idClienteDestino = @idClienteDestino
	--/********************************************************************************************************************
	--//*************************      PrejuizoFundoHistorico	******************************************************************/
	IF @statusCliente = 3 -- Fechado
	BEGIN
		SET @sql = ' DELETE FROM [' + @Banco_Destino + '].dbo.PrejuizoFundoHistorico where idCliente = @idClienteDestino ' +
				   ' and cast(CONVERT(varchar(8), datahistorico, 112) AS Datetime) <= @dataDiaCliente ' +
				   ' SELECT * INTO #TempTable FROM [' + @Banco_Origem + '].dbo.PrejuizoFundoHistorico where idCliente = @idClienteOrigem ' +
				   ' and cast(CONVERT(varchar(8), datahistorico, 112) AS Datetime) <= @dataDiaCliente '
	END

	ELSE	-- Nao Fechado
	BEGIN
		SET @sql = ' DELETE FROM [' + @Banco_Destino + '].dbo.PrejuizoFundoHistorico where idCliente = @idClienteDestino ' +
				   ' and cast(CONVERT(varchar(8), datahistorico, 112) AS Datetime) < @dataDiaCliente ' +
				   ' SELECT * INTO #TempTable FROM [' + @Banco_Origem + '].dbo.PrejuizoFundoHistorico where idCliente = @idClienteOrigem ' +
				   ' and cast(CONVERT(varchar(8), datahistorico, 112) AS Datetime) < @dataDiaCliente '
	END

	SET @sql = @sql + 		   
					   
			   ' Update #TempTable set idCliente = @idClienteDestino ' +
			   ' INSERT INTO [' + @Banco_Destino + '].dbo.PrejuizoFundoHistorico SELECT * FROM #TempTable ' +
			   ' DROP TABLE #TempTable '

	print 'PrejuizoFundoHistorico'
	EXECUTE sp_executesql @sql, 
						  N'@idClienteOrigem int, @idClienteDestino int, @dataDiaCliente DateTime',
						  @idClienteOrigem = @idClienteOrigem, 
						  @idClienteDestino = @idClienteDestino,
						  @dataDiaCliente = @dataDiaCliente
	--/********************************************************************************************************************
	END
	-- /*******************************************************************************************/

	--#endregion
	--#region HistoricoCota
	-- /************************* HistoricoCota	******************************************************************/
	IF @mercadoHistoricoCota = 1
	BEGIN

	--/*************************      HistoricoCota	******************************************************************/
	IF @statusCliente = 3 -- Fechado
	BEGIN
		SET @sql = ' DELETE FROM [' + @Banco_Destino + '].dbo.HistoricoCota where idcarteira = @idClienteDestino ' +
				   ' and cast(CONVERT(varchar(8), data, 112) AS Datetime) <= @dataDiaCliente ' +
				   ' SELECT * INTO #TempTable FROM [' + @Banco_Origem + '].dbo.HistoricoCota where IdCarteira = @idClienteOrigem ' +
				   ' and cast(CONVERT(varchar(8), data, 112) AS Datetime) <= @dataDiaCliente '
	END

	ELSE	-- Nao Fechado
	BEGIN
		SET @sql = ' DELETE FROM [' + @Banco_Destino + '].dbo.HistoricoCota where idcarteira = @idClienteDestino ' +
				   ' and cast(CONVERT(varchar(8), data, 112) AS Datetime) < @dataDiaCliente ' +
				   ' SELECT * INTO #TempTable FROM [' + @Banco_Origem + '].dbo.HistoricoCota where IdCarteira = @idClienteOrigem ' +
				   ' and cast(CONVERT(varchar(8), data, 112) AS Datetime) < @dataDiaCliente '
	END

	SET @sql = @sql + 		   
					   
			   ' Update #TempTable set idCarteira = @idClienteDestino ' +
			   ' INSERT INTO [' + @Banco_Destino + '].dbo.HistoricoCota SELECT * FROM #TempTable ' +
			   ' DROP TABLE #TempTable '
	print 'HistoricoCota'
	EXECUTE sp_executesql @sql, 
						  N'@idClienteOrigem int, @idClienteDestino int, @dataDiaCliente DateTime',
						  @idClienteOrigem = @idClienteOrigem, 
						  @idClienteDestino = @idClienteDestino,
						  @dataDiaCliente = @dataDiaCliente
	--/********************************************************************************************************************

	-- /************************* PosicaoCotista ******************************************************************/
	SET @sql = ' SELECT * INTO #TempTable FROM [' + @Banco_Origem + '].dbo.PosicaoCotista where IdCarteira = @idClienteOrigem ' +
			   ' ALTER TABLE #TempTable DROP COLUMN idPosicao ' +
			   ' Update #TempTable set idCarteira = @idClienteDestino ' +
			   ' INSERT INTO [' + @Banco_Destino + '].dbo.PosicaoCotista SELECT * FROM #TempTable ' +
			   ' DROP TABLE #TempTable '
	print 'PosicaoCotista'		   
	EXECUTE sp_executesql @sql, 
						  N'@idClienteOrigem int, @idClienteDestino int',
						  @idClienteOrigem = @idClienteOrigem, 
						  @idClienteDestino = @idClienteDestino
	--/********************************************************************************************************************/
	--/*************************  PosicaoCotistaAbertura ******************************************************************/
	SET @sql = ' DECLARE @i int ' + CHAR(13) +
			   ' SELECT @i = isnull( max(idposicao),0) from [' + @Banco_Destino + '].dbo.PosicaoCotistaAbertura' + CHAR(13) +
							   
			   ' SELECT * INTO #TempTable FROM ( ' + CHAR(13) +
			   
			   ' SELECT ( ROW_NUMBER() OVER(ORDER BY P.idPosicao ASC)) + @i as Row, P.* 
				 from [' + @Banco_Origem + '].dbo.PosicaoCotistaAbertura P, [' + @Banco_Origem + '].dbo.PosicaoCotistaAbertura P1
				 where P.idPosicao = P1.idPosicao and P.datahistorico = P1.datahistorico and P.IdCarteira = @idClienteOrigem'
				 
	IF @statusCliente = 3 -- Fechado
	BEGIN
		SET @sql = @sql + ' and cast(CONVERT(varchar(8), P.datahistorico, 112) AS Datetime) <= @dataDiaCliente '
	END

	ELSE	-- Nao Fechado
	BEGIN
		SET @sql = @sql + ' and cast(CONVERT(varchar(8), P.datahistorico, 112) AS Datetime) < @dataDiaCliente '
	END

	SET @sql = @sql + 			 
				 
			   ') as T' + CHAR(13) +
			   
			   ' ALTER TABLE #TempTable DROP COLUMN idPosicao ' + CHAR(13) +
			   ' EXEC tempdb..sp_rename ''#TempTable.Row'', ''IdPosicao'', ''COLUMN'' ' + CHAR(13) +
			   ' Update #TempTable set idCarteira = @idClienteDestino ' +
			   ' INSERT INTO [' + @Banco_Destino + '].dbo.PosicaoCotistaAbertura SELECT * FROM #TempTable ' + CHAR(13) +
			   ' DROP TABLE #TempTable '
	print 'PosicaoCotistaAbertura'		  		   
	EXECUTE sp_executesql @sql, 
						  N'@idClienteOrigem int, @idClienteDestino int, @dataDiaCliente DateTime',
						  @idClienteOrigem = @idClienteOrigem, 
						  @idClienteDestino = @idClienteDestino,
						  @dataDiaCliente = @dataDiaCliente
	--/********************************************************************************************************************/
	--/*************************  PosicaoCotistaHistorico ******************************************************************/
	SET @sql = ' DECLARE @i int ' + CHAR(13) +
			   ' SELECT @i = isnull( max(idposicao),0) from [' + @Banco_Destino + '].dbo.PosicaoCotistaHistorico' + CHAR(13) +
							   
			   ' SELECT * INTO #TempTable FROM ( ' + CHAR(13) +
			   
			   ' SELECT ( ROW_NUMBER() OVER(ORDER BY P.idPosicao ASC)) + @i as Row, P.* 
				 from [' + @Banco_Origem + '].dbo.PosicaoCotistaHistorico P, [' + @Banco_Origem + '].dbo.PosicaoCotistaHistorico P1
				 where P.idPosicao = P1.idPosicao and P.datahistorico = P1.datahistorico and P.IdCarteira = @idClienteOrigem'

	IF @statusCliente = 3 -- Fechado
	BEGIN
		SET @sql = @sql + ' and cast(CONVERT(varchar(8), P.datahistorico, 112) AS Datetime) <= @dataDiaCliente '
	END

	ELSE	-- Nao Fechado
	BEGIN
		SET @sql = @sql + ' and cast(CONVERT(varchar(8), P.datahistorico, 112) AS Datetime) < @dataDiaCliente '
	END

	SET @sql = @sql + 
				 
			   ') as T' + CHAR(13) +
			   
			   ' ALTER TABLE #TempTable DROP COLUMN idPosicao ' + CHAR(13) +
			   ' EXEC tempdb..sp_rename ''#TempTable.Row'', ''IdPosicao'', ''COLUMN'' ' + CHAR(13) +
			   ' Update #TempTable set idCarteira = @idClienteDestino ' +
			   ' INSERT INTO [' + @Banco_Destino + '].dbo.PosicaoCotistaHistorico SELECT * FROM #TempTable ' + CHAR(13) +
			   ' DROP TABLE #TempTable '
	print 'PosicaoCotistaHistorico'		  		   
	EXECUTE sp_executesql @sql, 
						  N'@idClienteOrigem int, @idClienteDestino int, @dataDiaCliente DateTime',
						  @idClienteOrigem = @idClienteOrigem, 
						  @idClienteDestino = @idClienteDestino,
						  @dataDiaCliente = @dataDiaCliente
	--/********************************************************************************************************************

	--//*************************    OperacaoCotista	******************************************************************/
	SET @sql = ' SELECT * INTO #TempTable FROM [' + @Banco_Origem + '].dbo.OperacaoCotista where IdCarteira = @idClienteOrigem ' +
			   ' ALTER TABLE #TempTable DROP COLUMN idOperacao ' +
			   ' Update #TempTable set idCarteira = @idClienteDestino ' +
			   ' INSERT INTO [' + @Banco_Destino + '].dbo.OperacaoCotista SELECT * FROM #TempTable ' +
			   ' DROP TABLE #TempTable '
	print 'OperacaoCotista'
	EXECUTE sp_executesql @sql, 
						  N'@idClienteOrigem int, @idClienteDestino int',
						  @idClienteOrigem = @idClienteOrigem, 
						  @idClienteDestino = @idClienteDestino
	--/********************************************************************************************************************
	END
	-- /*******************************************************************************************/

	--#endregion
	print 'Commit'
	COMMIT TRAN

	END TRY
	BEGIN CATCH		
		IF @@TRANCOUNT > 0
		begin
			print '------- ROLLBACK Transaction pois houve ERRO'		
			print ERROR_MESSAGE()
			ROLLBACK TRAN --RollBack if error occured
		end        
	END CATCH
GO

IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'Recon') = 0
BEGIN
	CREATE TABLE [dbo].[Recon](
		[IdRecon] [int] IDENTITY(1,1) NOT NULL,
		[DataRegistro] [datetime] NOT NULL,
		[DataInicioProcesso] [datetime] NULL,
		[DataFimProcesso] [datetime] NULL,
		[IdUsuario] [int] NOT NULL,
		[Observacao] [varchar](200) NULL,
		[DataCancelamento] [datetime] NULL,
		[Status] [varchar](8000) NULL,
		[MessageId] [varchar](50) NULL,
	 CONSTRAINT [PK_Recon] PRIMARY KEY CLUSTERED 
	(
		[IdRecon] ASC
	))
END
GO

DELETE FROM permissaomenu WHERE IdMenu = 19200;
INSERT INTO [permissaomenu] ([IdGrupo],[IdMenu],[PermissaoLeitura],[PermissaoAlteracao],[PermissaoExclusao],[PermissaoInclusao]) 
SELECT idgrupo,19200,'S','S','S','S' from grupousuario where idgrupo <> 0
go

DELETE FROM permissaomenu WHERE IdMenu = 390;
INSERT INTO [permissaomenu] ([IdGrupo],[IdMenu],[PermissaoLeitura],[PermissaoAlteracao],[PermissaoExclusao],[PermissaoInclusao]) 
SELECT idgrupo,390,'S','S','S','S' from grupousuario where idgrupo <> 0
go

DELETE FROM permissaomenu WHERE IdMenu = 6140;
INSERT INTO [permissaomenu] ([IdGrupo],[IdMenu],[PermissaoLeitura],[PermissaoAlteracao],[PermissaoExclusao],[PermissaoInclusao]) 
SELECT idgrupo,6140,'S','S','S','S' from grupousuario where idgrupo <> 0
go

IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'ParidadeCambial') = 0
BEGIN
	CREATE TABLE [dbo].[ParidadeCambial](
		[IdParidade] [int] IDENTITY(1,1) NOT NULL,
		[IdMoedaNumerador] [smallint] NOT NULL,
		[IdMoedaDenominador] [smallint] NOT NULL,
		[Descricao] [varchar](50) NOT NULL,
		[IdIndice] [smallint] NOT NULL,
	 CONSTRAINT [PK_ParidadeCambial] PRIMARY KEY CLUSTERED 
	(
		[IdParidade] ASC
	)
	)
END
GO

IF NOT EXISTS (select 1 from sysconstraints where object_name(id) = 'ParidadeCambial' and object_name(constid) = 'FK_ParidadeCambial_Indice')
BEGIN
	ALTER TABLE [dbo].[ParidadeCambial]  WITH CHECK ADD  CONSTRAINT [FK_ParidadeCambial_Indice] FOREIGN KEY([IdIndice])
	REFERENCES [dbo].[Indice] ([IdIndice]);
	ALTER TABLE [dbo].[ParidadeCambial] CHECK CONSTRAINT [FK_ParidadeCambial_Indice];
END
GO

IF NOT EXISTS (select 1 from sysconstraints where object_name(id) = 'ParidadeCambial' and object_name(constid) = 'FK_ParidadeCambial_Moeda1')
BEGIN
	ALTER TABLE [dbo].[ParidadeCambial]  WITH CHECK ADD  CONSTRAINT [FK_ParidadeCambial_Moeda1] FOREIGN KEY([IdMoedaDenominador])
	REFERENCES [dbo].[Moeda] ([IdMoeda])
	ALTER TABLE [dbo].[ParidadeCambial] CHECK CONSTRAINT [FK_ParidadeCambial_Moeda1]
END
GO

IF NOT EXISTS (select 1 from sysconstraints where object_name(id) = 'ParidadeCambial' and object_name(constid) = 'FK_ParidadeCambial_Moeda2')
BEGIN
	ALTER TABLE [dbo].[ParidadeCambial]  WITH CHECK ADD  CONSTRAINT [FK_ParidadeCambial_Moeda2] FOREIGN KEY([IdMoedaNumerador])
	REFERENCES [dbo].[Moeda] ([IdMoeda])
	ALTER TABLE [dbo].[ParidadeCambial] CHECK CONSTRAINT [FK_ParidadeCambial_Moeda2]
END
GO

IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'OperacaoCambio') = 0
BEGIN
	CREATE TABLE [dbo].[OperacaoCambio](
		[IdOperacao] [int] IDENTITY(1,1) NOT NULL,
		[IdCliente] [int] NOT NULL,
		[DataRegistro] [datetime] NOT NULL,
		[DataLiquidacao] [datetime] NOT NULL,
		[TaxaCambio] [decimal](16,4) NOT NULL,
		[IdMoedaOrigem] [smallint] NOT NULL,
		[IdContaOrigem] [int] NOT NULL,
		[PrincipalOrigem] [decimal](30, 2) NOT NULL,
		[CustosOrigem] [decimal](16, 2) NOT NULL,
		[TributosOrigem] [decimal](16, 2) NOT NULL,
		[TotalOrigem] [decimal](30, 2) NOT NULL,
		[IdMoedaDestino] [smallint] NOT NULL,
		[IdContaDestino] [int] NOT NULL,
		[PrincipalDestino] [decimal](30, 2) NOT NULL,
		[CustosDestino] [decimal](16, 2) NOT NULL,
		[TributosDestino] [decimal](16, 2) NOT NULL,
		[TotalDestino] [decimal](30, 2) NOT NULL,
	 CONSTRAINT [PK_OperacaoCambio] PRIMARY KEY CLUSTERED 
	(
		[IdOperacao] ASC
	)
	)
END
GO

IF NOT EXISTS (select 1 from sysconstraints where object_name(id) = 'OperacaoCambio' and object_name(constid) = 'FK_OperacaoCambio_Cliente')
BEGIN
	ALTER TABLE [dbo].[OperacaoCambio]  WITH CHECK ADD  CONSTRAINT [FK_OperacaoCambio_Cliente] FOREIGN KEY([IdCliente])	REFERENCES [dbo].[Cliente] ([IdCliente]);
	ALTER TABLE [dbo].[OperacaoCambio] CHECK CONSTRAINT [FK_OperacaoCambio_Cliente];
END
GO

IF NOT EXISTS (select 1 from sysconstraints where object_name(id) = 'OperacaoCambio' and object_name(constid) = 'FK_OperacaoCambio_ContaCorrente1')
BEGIN
	ALTER TABLE [dbo].[OperacaoCambio]  WITH CHECK ADD  CONSTRAINT [FK_OperacaoCambio_ContaCorrente1] FOREIGN KEY([IdContaOrigem]) REFERENCES [dbo].[ContaCorrente] ([IdConta])
	ALTER TABLE [dbo].[OperacaoCambio] CHECK CONSTRAINT [FK_OperacaoCambio_ContaCorrente1]
END
GO

IF NOT EXISTS (select 1 from sysconstraints where object_name(id) = 'OperacaoCambio' and object_name(constid) = 'FK_OperacaoCambio_ContaCorrente2')
BEGIN
	ALTER TABLE [dbo].[OperacaoCambio]  WITH CHECK ADD  CONSTRAINT [FK_OperacaoCambio_ContaCorrente2] FOREIGN KEY([IdContaDestino]) REFERENCES [dbo].[ContaCorrente] ([IdConta])
	ALTER TABLE [dbo].[OperacaoCambio] CHECK CONSTRAINT [FK_OperacaoCambio_ContaCorrente2]
END
GO

IF NOT EXISTS (select 1 from sysconstraints where object_name(id) = 'OperacaoCambio' and object_name(constid) = 'FK_OperacaoCambio_Moeda1')
BEGIN
	ALTER TABLE [dbo].[OperacaoCambio]  WITH CHECK ADD  CONSTRAINT [FK_OperacaoCambio_Moeda1] FOREIGN KEY([IdMoedaDestino]) REFERENCES [dbo].[Moeda] ([IdMoeda])
	ALTER TABLE [dbo].[OperacaoCambio] CHECK CONSTRAINT [FK_OperacaoCambio_Moeda1]
END
GO

IF NOT EXISTS (select 1 from sysconstraints where object_name(id) = 'OperacaoCambio' and object_name(constid) = 'FK_OperacaoCambio_Moeda2')
BEGIN
	ALTER TABLE [dbo].[OperacaoCambio]  WITH CHECK ADD  CONSTRAINT [FK_OperacaoCambio_Moeda2] FOREIGN KEY([IdMoedaOrigem]) REFERENCES [dbo].[Moeda] ([IdMoeda])
	ALTER TABLE [dbo].[OperacaoCambio] CHECK CONSTRAINT [FK_OperacaoCambio_Moeda2]
END	
GO

IF NOT EXISTS (select 1 from sysconstraints where object_name(id) = 'OperacaoCambio' and object_name(constid) = 'DF_OperacaoCambio_PrincipalOrigem')
BEGIN
	ALTER TABLE [dbo].[OperacaoCambio] ADD  CONSTRAINT [DF_OperacaoCambio_PrincipalOrigem]  DEFAULT ((0)) FOR [PrincipalOrigem]
END	
GO

IF NOT EXISTS (select 1 from sysconstraints where object_name(id) = 'OperacaoCambio' and object_name(constid) = 'DF_OperacaoCambio_CustosOrigem')
BEGIN
	ALTER TABLE [dbo].[OperacaoCambio] ADD  CONSTRAINT [DF_OperacaoCambio_CustosOrigem]  DEFAULT ((0)) FOR [CustosOrigem]
END	
GO

IF NOT EXISTS (select 1 from sysconstraints where object_name(id) = 'OperacaoCambio' and object_name(constid) = 'DF_OperacaoCambio_TributosOrigem')
BEGIN
	ALTER TABLE [dbo].[OperacaoCambio] ADD  CONSTRAINT [DF_OperacaoCambio_TributosOrigem]  DEFAULT ((0)) FOR [TributosOrigem]
END	
GO

IF NOT EXISTS (select 1 from sysconstraints where object_name(id) = 'OperacaoCambio' and object_name(constid) = 'DF_OperacaoCambio_TotalOrigem')
BEGIN
	ALTER TABLE [dbo].[OperacaoCambio] ADD  CONSTRAINT [DF_OperacaoCambio_TotalOrigem]  DEFAULT ((0)) FOR [TotalOrigem]
END	
GO

IF NOT EXISTS (select 1 from sysconstraints where object_name(id) = 'OperacaoCambio' and object_name(constid) = 'DF_OperacaoCambio_PrincipalDestino')
BEGIN
	ALTER TABLE [dbo].[OperacaoCambio] ADD  CONSTRAINT [DF_OperacaoCambio_PrincipalDestino]  DEFAULT ((0)) FOR [PrincipalDestino]
END	
GO

IF NOT EXISTS (select 1 from sysconstraints where object_name(id) = 'OperacaoCambio' and object_name(constid) = 'DF_OperacaoCambio_CustosDestino')
BEGIN
	ALTER TABLE [dbo].[OperacaoCambio] ADD  CONSTRAINT [DF_OperacaoCambio_CustosDestino]  DEFAULT ((0)) FOR [CustosDestino]
END	
GO

IF NOT EXISTS (select 1 from sysconstraints where object_name(id) = 'OperacaoCambio' and object_name(constid) = 'DF_OperacaoCambio_TributosDestino')
BEGIN
	ALTER TABLE [dbo].[OperacaoCambio] ADD  CONSTRAINT [DF_OperacaoCambio_TributosDestino]  DEFAULT ((0)) FOR [TributosDestino]
END	
GO

IF NOT EXISTS (select 1 from sysconstraints where object_name(id) = 'OperacaoCambio' and object_name(constid) = 'DF_OperacaoCambio_TotalDestino')
BEGIN
	ALTER TABLE [dbo].[OperacaoCambio] ADD  CONSTRAINT [DF_OperacaoCambio_TotalDestino]  DEFAULT ((0)) FOR [TotalDestino]
END	
GO

if (select count(*) from SYSCOLUMNS WHERE ID = (SELECT ID FROM SYSOBJECTS WHERE  NAME = 'operacaocotista' ) and name = 'IdOrdem') = 0
begin
	alter table operacaocotista add IdOrdem int null
END	
GO

if (select count(*) from SYSCOLUMNS WHERE ID = (SELECT ID FROM SYSOBJECTS WHERE  NAME = 'operacaorendafixa' ) and name = 'IsIPO') = 0
begin
	alter table operacaorendafixa add IsIPO char(1) null
	EXEC('update operacaorendafixa set IsIPO = ''N''');
	alter table operacaorendafixa alter column IsIPO char(1) not null
	ALTER TABLE operacaorendafixa ADD  CONSTRAINT DF_OperacaoRendaFixa_IsIPO  DEFAULT (('N')) FOR IsIPO
END	
GO

if (select count(*) from SYSCOLUMNS WHERE ID = (SELECT ID FROM SYSOBJECTS WHERE  NAME = 'operacaorendafixa' ) and name = 'IdOperacaoExterna') = 0
begin
	alter table operacaorendafixa add IdOperacaoExterna int null;
END	
GO

if (select count(*) from SYSCOLUMNS WHERE ID = (SELECT ID FROM SYSOBJECTS WHERE  NAME = 'permissaooperacaofundo' ) and name = 'SituacaoEnvio') = 0
begin
	alter table permissaooperacaofundo add SituacaoEnvio tinyint null;
	EXEC('update permissaooperacaofundo set SituacaoEnvio = 1');
	alter table permissaooperacaofundo alter column SituacaoEnvio tinyint not null;
	ALTER TABLE PermissaoOperacaoFundo ADD CONSTRAINT DF_SituacaoEnvio DEFAULT 1 FOR SituacaoEnvio;
END	
GO

DELETE FROM permissaomenu WHERE IdMenu = 25215;
INSERT INTO [permissaomenu] ([IdGrupo],[IdMenu],[PermissaoLeitura],[PermissaoAlteracao],[PermissaoExclusao],[PermissaoInclusao]) 
SELECT idgrupo,25215,'S','S','S','S' from grupousuario where idgrupo <> 0;
go

if (select count(*) from SYSCOLUMNS WHERE ID = (SELECT ID FROM SYSOBJECTS WHERE  NAME = 'clienteinterface' ) and name = 'InterfaceSinacorCC') = 0
begin
	alter table clienteinterface add InterfaceSinacorCC varchar(2000) null;
	EXEC('update clienteinterface set InterfaceSinacorCC = ''''');
END	
GO

if (select count(*) from SYSCOLUMNS WHERE ID = (SELECT ID FROM SYSOBJECTS WHERE  NAME = 'TituloRendaFixa' ) and name = 'Taxa') > 0
begin
	ALTER TABLE TituloRendaFixa ALTER COLUMN Taxa decimal(25,16);
END	
GO

DELETE FROM permissaomenu WHERE IdMenu = 10270;
INSERT INTO [permissaomenu] ([IdGrupo],[IdMenu],[PermissaoLeitura],[PermissaoAlteracao],[PermissaoExclusao],[PermissaoInclusao]) 
SELECT idgrupo,10270,'S','S','S','S' from grupousuario where idgrupo <> 0
GO

if (select count(*) from SYSCOLUMNS WHERE ID = (SELECT ID FROM SYSOBJECTS WHERE  NAME = 'carteira' ) and name = 'LiberaAplicacao') = 0
begin
	alter table carteira add LiberaAplicacao char(1) null;
	EXEC('update carteira set LiberaAplicacao = ''S''');
	alter table carteira alter column LiberaAplicacao char(1) not null;
	ALTER TABLE carteira ADD  CONSTRAINT DF_Carteira_LiberaAplicacao  DEFAULT (('S')) FOR LiberaAplicacao;
END	
GO

if (select count(*) from SYSCOLUMNS WHERE ID = (SELECT ID FROM SYSOBJECTS WHERE  NAME = 'carteira' ) and name = 'LiberaResgate') = 0
begin
	alter table carteira add LiberaResgate char(1) null;
	EXEC('update carteira set LiberaResgate = ''S''');
	alter table carteira alter column LiberaResgate char(1) not null;
	ALTER TABLE carteira ADD  CONSTRAINT DF_Carteira_LiberaResgate  DEFAULT (('S')) FOR LiberaResgate;
END	
GO

DELETE FROM permissaomenu WHERE IdMenu = 1150;
INSERT INTO [permissaomenu] ([IdGrupo],[IdMenu],[PermissaoLeitura],[PermissaoAlteracao],[PermissaoExclusao],[PermissaoInclusao]) 
SELECT idgrupo,1150,'S','S','S','S' from grupousuario where idgrupo <> 0
GO

DELETE FROM permissaomenu WHERE IdMenu = 1155;
INSERT INTO [permissaomenu] ([IdGrupo],[IdMenu],[PermissaoLeitura],[PermissaoAlteracao],[PermissaoExclusao],[PermissaoInclusao]) 
SELECT idgrupo,1155,'S','S','S','S' from grupousuario where idgrupo <> 0
GO

DELETE FROM permissaomenu WHERE IdMenu = 1160;
INSERT INTO [permissaomenu] ([IdGrupo],[IdMenu],[PermissaoLeitura],[PermissaoAlteracao],[PermissaoExclusao],[PermissaoInclusao]) 
SELECT idgrupo,1160,'S','S','S','S' from grupousuario where idgrupo <> 0
GO

if (select count(*) from SYSCOLUMNS WHERE ID = (SELECT ID FROM SYSOBJECTS WHERE  NAME = 'agentemercado' ) and name = 'nomeresponsavel') > 0
begin
	alter table agentemercado alter column nomeresponsavel varchar(100)
END	
GO

IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'TipoConta') = 0
BEGIN
	CREATE TABLE TipoConta(
		IdTipoConta int IDENTITY(1,1) NOT NULL,
		Descricao varchar(100) NOT NULL,
	 CONSTRAINT PK_TipoConta PRIMARY KEY CLUSTERED 
	(
		IdTipoConta ASC
	)
	)
END	
GO

IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'GrupoConta') = 0
BEGIN
	CREATE TABLE GrupoConta(
		IdGrupo int IDENTITY(1,1) NOT NULL,
		Descricao varchar(100) NOT NULL,
	 CONSTRAINT PK_GrupoConta PRIMARY KEY CLUSTERED 
	(
		IdGrupo ASC
	)
	)
END	
GO

if (select count(*) from SYSCOLUMNS WHERE ID = (SELECT ID FROM SYSOBJECTS WHERE  NAME = 'banco' ) and name = 'CodigoExterno') = 0
begin
	alter table banco add CodigoExterno varchar(12) null;
END	
GO

if (select count(*) from SYSCOLUMNS WHERE ID = (SELECT ID FROM SYSOBJECTS WHERE  NAME = 'banco' ) and name = 'DescricaoCodigoExterno') = 0
begin
	alter table banco add DescricaoCodigoExterno varchar(20) null;
END	
GO

if (select count(*) from SYSCOLUMNS WHERE ID = (SELECT ID FROM SYSOBJECTS WHERE  NAME = 'agencia' ) and name = 'IdLocal') = 0
begin
	alter table agencia add IdLocal smallint null;
	EXEC('update agencia set IdLocal = 1');
	alter table agencia alter column IdLocal smallint not null
END	
GO

if (select count(*) from SYSCOLUMNS WHERE ID = (SELECT ID FROM SYSOBJECTS WHERE  NAME = 'contacorrente' ) and name = 'numero') > 0
begin
	alter table contacorrente alter column numero varchar(50)
END	
GO

if (select count(*) from SYSCOLUMNS WHERE ID = (SELECT ID FROM SYSOBJECTS WHERE  NAME = 'contacorrente' ) and name = 'IdLocal') = 0
begin
	alter table contacorrente add IdLocal smallint null
END	
GO

if (select count(*) from SYSCOLUMNS WHERE ID = (SELECT ID FROM SYSOBJECTS WHERE  NAME = 'contacorrente' ) and name = 'DescricaoCodigo') = 0
begin
	alter table contacorrente add DescricaoCodigo varchar(20) null
END	
GO

if (select count(*) from SYSCOLUMNS WHERE ID = (SELECT ID FROM SYSOBJECTS WHERE  NAME = 'contacorrente' ) and name = 'TipoConta') > 0
begin
	alter table contacorrente drop column TipoConta
END	
GO
	
if (select count(*) from SYSCOLUMNS WHERE ID = (SELECT ID FROM SYSOBJECTS WHERE  NAME = 'contacorrente' ) and name = 'IdTipoConta') = 0
begin	
	alter table contacorrente add IdTipoConta int null
END	
GO

if (select count(*) from SYSCOLUMNS WHERE ID = (SELECT ID FROM SYSOBJECTS WHERE  NAME = 'contacorrente' ) and name = 'IdGrupo') = 0
begin
	alter table contacorrente add IdGrupo int null
END	
GO

if (select count(*) from SYSCOLUMNS WHERE ID = (SELECT ID FROM SYSOBJECTS WHERE  NAME = 'liquidacaorendafixa' ) and name = 'IdOperacaoVenda') = 0
begin
	alter table liquidacaorendafixa add IdOperacaoVenda int null
END	
GO

if (select count(*) from SYSCOLUMNS WHERE ID = (SELECT ID FROM SYSOBJECTS WHERE  NAME = 'liquidacaorendafixa' ) and name = 'Quantidade') > 0
begin
	alter table liquidacaorendafixa alter column Quantidade decimal (25, 12) not null
END	
GO

if (select count(*) from SYSCOLUMNS WHERE ID = (SELECT ID FROM SYSOBJECTS WHERE  NAME = 'liquidacaorendafixa' ) and name = 'Status') = 0
begin
	alter table liquidacaorendafixa add Status tinyint null
	EXEC('update liquidacaorendafixa set Status = 1');
	ALTER TABLE liquidacaorendafixa ADD  CONSTRAINT DF_LiquidacaoRendaFixa_Status  DEFAULT ((1)) FOR Status	
END	
GO

if (select count(*) from SYSCOLUMNS WHERE ID = (SELECT ID FROM SYSOBJECTS WHERE  NAME = 'LiquidacaoRendaFixa' ) and name = 'RendimentoNaoTributavel') = 0
begin
	ALTER TABLE LiquidacaoRendaFixa ADD RendimentoNaoTributavel decimal(16,2) null
	EXEC('update LiquidacaoRendaFixa set RendimentoNaoTributavel = 0');
	ALTER TABLE LiquidacaoRendaFixa alter column RendimentoNaoTributavel decimal(16,2) not null
	ALTER TABLE LiquidacaoRendaFixa ADD  CONSTRAINT DF_LiquidacaoRendaFixa_RendimentoNaoTributavel  DEFAULT ((0)) FOR RendimentoNaoTributavel
END	
GO

DELETE FROM CONFIGURACAO WHERE Id = 109;
INSERT INTO CONFIGURACAO(Id,Descricao,ValorNumerico,ValorTexto)VALUES(109,'PfeeCotasFundos',null,'N')
GO

DELETE FROM CONFIGURACAO WHERE Id = 1040;
INSERT INTO CONFIGURACAO(Id,Descricao,ValorNumerico,ValorTexto)VALUES(1040,'UsuarioIntegracao',null,'')
GO

DELETE FROM CONFIGURACAO WHERE Id = 1041;
INSERT INTO CONFIGURACAO(Id,Descricao,ValorNumerico,ValorTexto)VALUES(1041,'SenhaIntegracao',null,'')
GO

DELETE FROM permissaomenu WHERE IdMenu = 3895;
INSERT INTO [permissaomenu] ([IdGrupo],[IdMenu],[PermissaoLeitura],[PermissaoAlteracao],[PermissaoExclusao],[PermissaoInclusao]) 
SELECT idgrupo,3895,'S','S','S','S' from grupousuario where idgrupo <> 0
GO

if not exists(select 1 from syscolumns where id = object_id('Cliente') and name = 'AberturaIndexada')
BEGIN
	ALTER TABLE Cliente ADD AberturaIndexada smallint NOT NULL DEFAULT '0'
END
GO

if Not exists(select 1 from syscolumns where id = object_id('Cliente') and name = 'IdIndiceAbertura')
BEGIN
	ALTER TABLE dbo.Cliente ADD IdIndiceAbertura smallint NULL;
	ALTER TABLE Cliente  WITH CHECK ADD  CONSTRAINT Cliente_Indice_FK1 FOREIGN KEY(IdIndiceAbertura)
	REFERENCES Indice (IdIndice) ON DELETE SET NULL;
END
GO	

if (select count(*) from SYSCOLUMNS WHERE ID = (SELECT ID FROM SYSOBJECTS WHERE  NAME = 'PapelRendaFixa' ) and name = 'CasasDecimaisPU') > 0
begin
	alter table PapelRendaFixa alter column CasasDecimaisPU smallint not null
END	
GO

if (select count(*) from SYSCOLUMNS WHERE ID = (SELECT ID FROM SYSOBJECTS WHERE  NAME = 'PrazoMedio' ) and name = 'IdPosicao') = 0
begin
	alter table PrazoMedio add IdPosicao int NULL;
END	
GO

IF EXISTS(SELECT * FROM sys.columns WHERE NAME = N'PrazoMedioAtivo' and Object_ID = Object_ID(N'PrazoMedio'))
BEGIN
	exec sp_rename 'PrazoMedio.PrazoMedioAtivo', 'MercadoAtivo'
END
GO

if (select count(*) from SYSCOLUMNS WHERE ID = (SELECT ID FROM SYSOBJECTS WHERE  NAME = 'Indice' ) and name = 'AtributoBDS') > 0
begin
	alter table Indice alter column AtributoBDS varchar(20) NULL;
END	
GO

if not exists(select 1 from syscolumns where id = object_id('Carteira') and name = 'FIE')
BEGIN	
	ALTER TABLE [dbo].[Carteira] ADD FIE char(1) NOT NULL DEFAULT 'N';
END
GO	

if not exists(select 1 from syscolumns where id = object_id('OperacaoCotista') and name = 'FieModalidade')
BEGIN	
	ALTER TABLE [dbo].[OperacaoCotista] ADD FieModalidade int NULL;
END
GO	

if not exists(select 1 from syscolumns where id = object_id('OperacaoCotista') and name = 'FieTabelaIr')
BEGIN	
	ALTER TABLE [dbo].[OperacaoCotista] ADD FieTabelaIr int NULL;
END
GO


IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'TributacaoCautelaFie') = 0
BEGIN
	CREATE TABLE TributacaoCautelaFie(
		Data datetime NOT NULL,
		IdOperacao int NOT NULL,
		FieTabelaIr int NOT NULL,
	 CONSTRAINT PK_TributacaoCautelaFie PRIMARY KEY CLUSTERED (Data ASC, IdOperacao ASC)
	)
	alter table TributacaoCautelaFie add constraint TributacaoCautelaFie_OperacaoCotista_FK FOREIGN KEY ( IdOperacao ) references OperacaoCotista(IdOperacao);
END	
GO

if not exists(select 1 from syscolumns where id = object_id('PosicaoCotista') and name = 'FieTabelaIr')
BEGIN	
	ALTER TABLE [dbo].[PosicaoCotista] ADD FieTabelaIr int NULL;
END
GO

if not exists(select 1 from syscolumns where id = object_id('PosicaoCotistaHistorico') and name = 'FieTabelaIr')
BEGIN	
	ALTER TABLE [dbo].[PosicaoCotistaHistorico] ADD FieTabelaIr int NULL;
END
GO

if not exists(select 1 from syscolumns where id = object_id('PosicaoCotistaAbertura') and name = 'FieTabelaIr')
BEGIN	
	ALTER TABLE [dbo].[PosicaoCotistaAbertura] ADD FieTabelaIr int NULL;
END
GO

if not exists(select 1 from syscolumns where id = object_id('PosicaoCotistaAux') and name = 'FieTabelaIr')
BEGIN	
	ALTER TABLE [dbo].[PosicaoCotistaAux] ADD FieTabelaIr int NULL;
END
GO

IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'DeParaTitRendaFixaJuros') = 0
Begin
 CREATE TABLE DeParaTitRendaFixaJuros
 (  
	TipoCurvaPAS		tinyint not null,
	ContagemDiasPAS		tinyint not null,
	BaseAnoPAS			int		not null, 	
	PagamentoJurosPAS	tinyint	not null,
	EJurosFincs			tinyint not null,
	EJurosTipoFincs		int not null 
	CONSTRAINT PK_DeParaTitRendaFixaJuros 		primary key(TipoCurvaPAS, ContagemDiasPAS, BaseAnoPAS, PagamentoJurosPAS)	
 ) 
End
GO

if not exists(select 1 from syscolumns where id = object_id('TituloRendaFixa') and name = 'EJurosTipo')
BEGIN	
	ALTER TABLE [dbo].[TituloRendaFixa] ADD EJurosTipo int NULL;
END
GO


if not exists(select 1 from syscolumns where id = object_id('OperacaoFundo') and name = 'FieModalidade')
BEGIN	
	ALTER TABLE [dbo].[OperacaoFundo] ADD FieModalidade int NULL;
END
GO	

if not exists(select 1 from syscolumns where id = object_id('OperacaoFundo') and name = 'FieTabelaIr')
BEGIN	
	ALTER TABLE [dbo].[OperacaoFundo] ADD FieTabelaIr int NULL;
END
GO


IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'TributacaoFundoFie') = 0
BEGIN
	CREATE TABLE TributacaoFundoFie(
		Data datetime NOT NULL,
		IdOperacao int NOT NULL,
		FieTabelaIr int NOT NULL,
	 CONSTRAINT PK_TributacaoFundoFie PRIMARY KEY CLUSTERED (Data ASC, IdOperacao ASC)
	)
	alter table TributacaoFundoFie add constraint TributacaoFundoFie_OperacaoFundo_FK FOREIGN KEY ( IdOperacao ) references OperacaoFundo(IdOperacao);
END	
GO

if not exists(select 1 from syscolumns where id = object_id('PosicaoFundo') and name = 'FieTabelaIr')
BEGIN	
	ALTER TABLE [dbo].[PosicaoFundo] ADD FieTabelaIr int NULL;
END
GO

if not exists(select 1 from syscolumns where id = object_id('PosicaoFundoHistorico') and name = 'FieTabelaIr')
BEGIN	
	ALTER TABLE [dbo].[PosicaoFundoHistorico] ADD FieTabelaIr int NULL;
END
GO

if not exists(select 1 from syscolumns where id = object_id('PosicaoFundoAbertura') and name = 'FieTabelaIr')
BEGIN	
	ALTER TABLE [dbo].[PosicaoFundoAbertura] ADD FieTabelaIr int NULL;
END
GO

if not exists(select 1 from syscolumns where id = object_id('PosicaoFundoAux') and name = 'FieTabelaIr')
BEGIN	
	ALTER TABLE [dbo].[PosicaoFundoAux] ADD FieTabelaIr int NULL;
END
GO


if not exists(select 1 from syscolumns where id = object_id('OperacaoFundo') and name = 'FieModalidade')
BEGIN	
	ALTER TABLE [dbo].[OperacaoFundo] ADD FieModalidade int NULL;
END
GO	

if not exists(select 1 from syscolumns where id = object_id('OperacaoFundo') and name = 'FieTabelaIr')
BEGIN	
	ALTER TABLE [dbo].[OperacaoFundo] ADD FieTabelaIr int NULL;
END
GO


IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'TributacaoFundoFie') = 0
BEGIN
	CREATE TABLE TributacaoFundoFie(
		Data datetime NOT NULL,
		IdOperacao int NOT NULL,
		FieTabelaIr int NOT NULL,
	 CONSTRAINT PK_TributacaoFundoFie PRIMARY KEY CLUSTERED (Data ASC, IdOperacao ASC)
	)
	alter table TributacaoFundoFie add constraint TributacaoFundoFie_OperacaoFundo_FK FOREIGN KEY ( IdOperacao ) references OperacaoFundo(IdOperacao);
END	
GO

if not exists(select 1 from syscolumns where id = object_id('PosicaoFundo') and name = 'FieTabelaIr')
BEGIN	
	ALTER TABLE [dbo].[PosicaoFundo] ADD FieTabelaIr int NULL;
END
GO

if not exists(select 1 from syscolumns where id = object_id('PosicaoFundoHistorico') and name = 'FieTabelaIr')
BEGIN	
	ALTER TABLE [dbo].[PosicaoFundoHistorico] ADD FieTabelaIr int NULL;
END
GO

if not exists(select 1 from syscolumns where id = object_id('PosicaoFundoAbertura') and name = 'FieTabelaIr')
BEGIN	
	ALTER TABLE [dbo].[PosicaoFundoAbertura] ADD FieTabelaIr int NULL;
END
GO

if not exists(select 1 from syscolumns where id = object_id('PosicaoFundoAux') and name = 'FieTabelaIr')
BEGIN	
	ALTER TABLE [dbo].[PosicaoFundoAux] ADD FieTabelaIr int NULL;
END
GO



/*Diferencial*/
if exists(select 1 from syscolumns where id = object_id('operacaorendafixa') and name = 'IdOperacaoExterna')
BEGIN	
	alter table operacaorendafixa alter column [IdOperacaoExterna] bigint null;
END
GO


IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'MediaMovelRV') > 0
begin
	DROP TABLE MediaMovelRV;
END	
GO

IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'MediaMovel') = 0
begin
	CREATE TABLE MediaMovel
	(
		IdMediaMovel			 INT 			NOT NULL PRIMARY KEY identity,
		DataAtual 		 		 DateTime		NOT NULL,
		IdCliente		 		 INT 			NOT NULL,
		TipoMediaMovel		 	 INT 			NOT NULL,
		MediaMovel		 		 Decimal(25,8)	NOT NULL,
		idPosicao				 int            NULL,
		ChaveComposta			 varchar(100) 	NULL				
	)
END	
GO

if Not exists(select 1 from syscolumns where id = object_id('MediaMovel') and name = 'IdCliente')
BEGIN
	ALTER TABLE dbo.MediaMovel ADD IdCliente smallint NULL;
	ALTER TABLE Cliente  WITH CHECK ADD  CONSTRAINT MediaMovel_Cliente_FK1 FOREIGN KEY(IdCliente)
	REFERENCES Cliente (IdCliente) ON DELETE Cascade;
END
GO	

IF EXISTS(SELECT * FROM sys.columns WHERE NAME = N'IdMercadoTipoPessoa' and Object_ID = Object_ID(N'CadastroComplementar'))
BEGIN
	ALTER TABLE CadastroComplementar ALTER COLUMN IdMercadoTipoPessoa varchar(20)
END
GO	

if not exists(select 1 from syscolumns where id = object_id('ProventoBolsaCliente') and name = 'TipoPosicaoBolsa')
BEGIN
	ALTER TABLE ProventoBolsaCliente add TipoPosicaoBolsa smallint 
END
GO	
/*Diferencial*/

if NOT exists(select 1 from syscolumns where id = object_id('PosicaoRendaFixa') and name = 'DataRegistro')
BEGIN
	ALTER TABLE PosicaoRendaFixa ADD DataRegistro [datetime] NULL;
	EXEC('UPDATE PosicaoRendaFixa SET DataRegistro = DataOperacao');
	ALTER TABLE PosicaoRendaFixa ALTER COLUMN DataRegistro [datetime] NOT NULL;	
end
GO
		

if NOT exists(select 1 from syscolumns where id = object_id('PosicaoRendaFixaAbertura') and name = 'DataRegistro')
BEGIN
	ALTER TABLE PosicaoRendaFixaAbertura ADD DataRegistro [datetime] NULL;
	EXEC('UPDATE PosicaoRendaFixaAbertura SET DataRegistro = DataOperacao');
	ALTER TABLE PosicaoRendaFixaAbertura ALTER COLUMN DataRegistro [datetime] NOT NULL;
end
GO
	
if NOT exists(select 1 from syscolumns where id = object_id('PosicaoRendaFixaHistorico') and name = 'DataRegistro')
BEGIN
	ALTER TABLE PosicaoRendaFixaHistorico ADD DataRegistro [datetime] NULL;
	EXEC('UPDATE PosicaoRendaFixaHistorico SET DataRegistro = DataOperacao');
	ALTER TABLE PosicaoRendaFixaHistorico ALTER COLUMN DataRegistro [datetime] NOT NULL;
end
GO

if not exists(select 1 from syscolumns where id = object_id('PosicaoRendaFixa') and name = 'IdLocalNegociacao')
BEGIN
	ALTER TABLE dbo.PosicaoRendaFixa ADD IdLocalNegociacao int NULL;
	
	EXEC('	UPDATE pos SET  pos.IdLocalNegociacao = (CASE WHEN pap.TipoPapel <> 1 THEN 3 ELSE 2 END) 
			FROM PosicaoRendaFixa pos 
			INNER JOIN TituloRendaFixa tit ON pos.IdTitulo = tit.IdTitulo
			INNER JOIN PapelRendaFixa pap ON tit.IdPapel = pap.IdPapel');

	ALTER TABLE PosicaoRendaFixa ALTER COLUMN IdLocalNegociacao int NOT NULL;
	alter table PosicaoRendaFixa add constraint PosRendaFixa_LocalNeg_FK FOREIGN KEY ( IdLocalNegociacao ) references LocalNegociacao(IdLocalNegociacao)
END
GO	

if not exists(select 1 from syscolumns where id = object_id('PosicaoRendaFixaAbertura') and name = 'IdLocalNegociacao')
BEGIN
	ALTER TABLE dbo.PosicaoRendaFixaAbertura ADD IdLocalNegociacao int NULL;
	EXEC('	UPDATE pos SET  pos.IdLocalNegociacao = (CASE WHEN pap.TipoPapel <> 1 THEN 3 ELSE 2 END) 
			FROM PosicaoRendaFixaAbertura pos 
			INNER JOIN TituloRendaFixa tit ON pos.IdTitulo = tit.IdTitulo
			INNER JOIN PapelRendaFixa pap ON tit.IdPapel = pap.IdPapel');
	ALTER TABLE PosicaoRendaFixaAbertura ALTER COLUMN IdLocalNegociacao int NOT NULL;
	alter table PosicaoRendaFixaAbertura add constraint PosRendaFixaAbe_LocalNeg_FK FOREIGN KEY ( IdLocalNegociacao ) references LocalNegociacao(IdLocalNegociacao)
END
GO	

if not exists(select 1 from syscolumns where id = object_id('PosicaoRendaFixaHistorico') and name = 'IdLocalNegociacao')
BEGIN
	ALTER TABLE dbo.PosicaoRendaFixaHistorico ADD IdLocalNegociacao int NULL;
	EXEC('	UPDATE pos SET  pos.IdLocalNegociacao = (CASE WHEN pap.TipoPapel <> 1 THEN 3 ELSE 2 END) 
			FROM PosicaoRendaFixaHistorico pos 
			INNER JOIN TituloRendaFixa tit ON pos.IdTitulo = tit.IdTitulo
			INNER JOIN PapelRendaFixa pap ON tit.IdPapel = pap.IdPapel');
	ALTER TABLE PosicaoRendaFixaHistorico ALTER COLUMN IdLocalNegociacao int NOT NULL;
	alter table PosicaoRendaFixaHistorico add constraint PosRendaFixaHis_LocalNeg_FK FOREIGN KEY ( IdLocalNegociacao ) references LocalNegociacao(IdLocalNegociacao)
END
GO	

if not exists(select 1 from syscolumns where id = object_id('PosicaoRendaFixa') and name = 'IdCorretora')
BEGIN
	ALTER TABLE dbo.PosicaoRendaFixa ADD IdCorretora int NULL
	EXEC('UPDATE PosicaoRendaFixa SET IdCorretora = IdAgente');
END
GO	

if not exists(select 1 from syscolumns where id = object_id('PosicaoRendaFixaAbertura') and name = 'IdCorretora')
BEGIN
	ALTER TABLE dbo.PosicaoRendaFixaAbertura ADD IdCorretora int NULL
	EXEC('UPDATE PosicaoRendaFixaAbertura SET IdCorretora = IdAgente');
END
GO	

if not exists(select 1 from syscolumns where id = object_id('PosicaoRendaFixaHistorico') and name = 'IdCorretora')
BEGIN
	ALTER TABLE dbo.PosicaoRendaFixaHistorico ADD IdCorretora int NULL
	EXEC('UPDATE PosicaoRendaFixaHistorico SET IdCorretora = IdAgente');
END
GO	

if exists(select 1 from syscolumns where id = object_id('PosicaoRendaFixa') and name = 'DataRegistro')		
BEGIN
	ALTER TABLE PosicaoRendaFixa DROP COLUMN DataRegistro;
end
GO

if exists(select 1 from syscolumns where id = object_id('PosicaoRendaFixaAbertura') and name = 'DataRegistro')		
BEGIN
	ALTER TABLE PosicaoRendaFixaAbertura DROP COLUMN DataRegistro;
end
GO

if exists(select 1 from syscolumns where id = object_id('PosicaoRendaFixaHistorico') and name = 'DataRegistro')		
BEGIN
	ALTER TABLE PosicaoRendaFixaHistorico DROP COLUMN DataRegistro;
end
GO

if exists(select 1 from syscolumns where id = object_id('PosicaoRendaFixa') and name = 'IdLocalNegociacao')
BEGIN
	alter table PosicaoRendaFixa DROP constraint PosRendaFixa_LocalNeg_FK;
	alter table PosicaoRendaFixa DROP COLUMN IdLocalNegociacao;
end
GO	

if exists(select 1 from syscolumns where id = object_id('PosicaoRendaFixaAbertura') and name = 'IdLocalNegociacao')
BEGIN
	alter table PosicaoRendaFixaAbertura DROP constraint PosRendaFixaAbe_LocalNeg_FK;
	alter table PosicaoRendaFixaAbertura DROP COLUMN IdLocalNegociacao;
end
GO	

if exists(select 1 from syscolumns where id = object_id('PosicaoRendaFixaHistorico') and name = 'IdLocalNegociacao')
BEGIN
	alter table PosicaoRendaFixaHistorico DROP constraint PosRendaFixaHis_LocalNeg_FK;
	alter table PosicaoRendaFixaHistorico DROP COLUMN IdLocalNegociacao;
end
GO	

IF EXISTS (select * from sys.objects where [type] = 'TR' and [name] = 'trgPosicaoRendaFixa')
begin
	drop trigger trgPosicaoRendaFixa;
END
	
--IF NOT EXISTS (select * from sys.objects where [type] = 'TR' and [name] = 'trgPosicaoRendaFixa')
--EXEC('
--	CREATE TRIGGER trgPosicaoRendaFixa ON PosicaoRendaFixa 
--	AFTER INSERT AS 
--	BEGIN 
--		DECLARE @IdCliente	INT, 
--				@IdTitulo	INT,
--				@TipoOperacao INT,
--				@Quantidade DECIMAL(25,12),
--				@QuantidadeBloqueada DECIMAL(16,2),
--				@DataOperacao	DATETIME,
--				@PUOperacao	DECIMAL(25,12),
--				@TaxaOperacao	DECIMAL(25,16),
--				@PUMercado	DECIMAL(25,12),
--				@DataVolta	DATETIME,
--				@TaxaVolta	DECIMAL(8,4),
--				@PUVolta	DECIMAL(25,12),
--				@IdCustodia tinyint,
--				@TipoNegociacao int,
--				@IdPosicao int,
--				@IdOperacao int,
--				@DataDia DATETIME,		
--				@IdAgenteCorretora int,
--				@IdAgenteCustodia int,
--				@IdLocalNegociacao int
--		
--		SELECT @IdCliente = IdCliente from inserted; 
--		SELECT @IdTitulo =	IdTitulo from inserted;
--		SELECT @TipoOperacao = TipoOperacao from inserted;
--		SELECT @Quantidade = Quantidade from inserted;
--		SELECT @DataOperacao = DataOperacao from inserted;
--		SELECT @PUOperacao = PUOperacao from inserted;
--		SELECT @TaxaOperacao = TaxaOperacao from inserted;
--		SELECT @DataVolta = DataVolta from inserted;
--		SELECT @TaxaVolta = TaxaVolta from inserted;
--		SELECT @PUVolta = PUVolta from inserted;
--		SELECT @IdCustodia = IdCustodia from inserted; 
--		SELECT @TipoNegociacao = TipoNegociacao from inserted; 
--		SELECT @IdPosicao = IdPosicao from inserted; 
--		SELECT @IdOperacao = IdOperacao from inserted; 
--		SELECT @DataDia = DataDia from Cliente where IdCliente = @IdCliente;
--		SELECT @IdAgenteCustodia = IdAgente from inserted; 
--		SELECT @IdAgenteCorretora = IdCorretora from inserted; 
--		SELECT @IdLocalNegociacao = IdLocalNegociacao from TituloRendaFixa tit inner join PapelRendaFixa pap on pap.IdPapel = tit.IdPapel where tit.idTitulo = @IdTitulo;
--		
--		IF(@IdOperacao IS NULL)
--		BEGIN	
--			INSERT INTO OperacaoRendaFixa(IdCliente, IdTitulo, TipoOperacao, Quantidade, DataOperacao, PUOperacao, TaxaOperacao, DataVolta, TaxaVolta, PUVolta, IdCustodia, DataLiquidacao, Valor,Fonte, TipoNegociacao, IdLiquidacao, DataRegistro, IdAgenteCustodia, IdAgenteCorretora, IdLocalNegociacao)
--			VALUES(@IdCliente, @IdTitulo, @TipoOperacao, @Quantidade, @DataDia, @PUOperacao,@TaxaOperacao,@DataVolta,@TaxaVolta,@PUVolta,@IdCustodia, @DataOperacao, (@Quantidade*@PUOperacao), 0, @TipoNegociacao, 0, @DataOperacao, @IdAgenteCustodia, @IdAgenteCorretora, @IdLocalNegociacao);
--
--			UPDATE PosicaoRendaFixa 
--			SET IdOperacao = (select max(OperacaoRendaFixa.IdOperacao) FROM OperacaoRendaFixa)
--			WHERE IdPosicao = @IdPosicao;
--			
--			UPDATE PosicaoRendaFixaHistorico
--			SET IdOperacao = (select max(OperacaoRendaFixa.IdOperacao) FROM OperacaoRendaFixa)
--			WHERE IdPosicao = @IdPosicao;
--			
--			UPDATE PosicaoRendaFixaAbertura
--			SET IdOperacao = (select max(OperacaoRendaFixa.IdOperacao) FROM OperacaoRendaFixa)
--			WHERE IdPosicao = @IdPosicao;
--		END
--	END 
--');
--go

--Diferencial
IF EXISTS(SELECT * FROM sys.columns WHERE NAME = N'IdMercadoTipoPessoa' and Object_ID = Object_ID(N'CadastroComplementar'))
BEGIN
 ALTER TABLE CadastroComplementar ALTER COLUMN IdMercadoTipoPessoa varchar(20)
END
GO

IF EXISTS(SELECT * FROM sys.columns WHERE NAME = N'PrazoMedioAtivo' and Object_ID = Object_ID(N'PrazoMedio'))
BEGIN
 exec sp_rename 'PrazoMedio.PrazoMedioAtivo', 'MercadoAtivo'
END
GO


IF (select count(*) from SYSCOLUMNS WHERE ID = (SELECT ID FROM SYSOBJECTS WHERE  NAME = 'PrazoMedio' ) and name = 'IdPosicao') = 0
begin
 alter table PrazoMedio add IdPosicao int NULL;
END 
GO

IF EXISTS (select 1 from sysconstraints where object_name(id) = 'LiquidacaoRendaFixa' and object_name(constid) = 'DF_LiquidacaoRendaFixa_Status')
BEGIN
	ALTER TABLE [dbo].[LiquidacaoRendaFixa] DROP  CONSTRAINT [DF_LiquidacaoRendaFixa_Status];
END 
GO	

IF NOT EXISTS (select 1 from sysconstraints where object_name(id) = 'LiquidacaoRendaFixa' and object_name(constid) = 'DF_LiquidacaoRendaFixa_Status')
BEGIN
	ALTER TABLE [dbo].[LiquidacaoRendaFixa] ADD CONSTRAINT [DF_LiquidacaoRendaFixa_Status] DEFAULT ((1)) FOR [Status];
END 
GO	
	
IF (select count(*) from SYSCOLUMNS WHERE ID = (SELECT ID FROM SYSOBJECTS WHERE  NAME = 'operacaoswap' ) and name = 'CodigoIsin') = 0
begin
	alter table operacaoswap add CodigoIsin varchar(12) null;
END
GO

IF (select count(*) from SYSCOLUMNS WHERE ID = (SELECT ID FROM SYSOBJECTS WHERE  NAME = 'operacaoswap' ) and name = 'CpfcnpjContraParte') = 0
begin
	alter table operacaoswap add CpfcnpjContraParte varchar(30) null
END
GO


IF NOT EXISTS (SELECT name FROM sysindexes WHERE name = 'PosicaoFundoHistorico_Idx')
BEGIN
	CREATE INDEX PosicaoFundoHistorico_Idx ON PosicaoFundoHistorico (DataHistorico, IdCliente);
END
GO	

IF NOT EXISTS (SELECT name FROM sysindexes WHERE name = 'PosicaoFundoAbertura_Idx')
BEGIN
	CREATE INDEX PosicaoFundoAbertura_Idx ON PosicaoFundoAbertura (DataHistorico, IdCliente);
END
GO		

IF NOT EXISTS (SELECT name FROM sysindexes WHERE name = 'PosicaoCotistaHistorico_Idx')
BEGIN
	CREATE INDEX PosicaoCotistaHistorico_Idx ON PosicaoCotistaHistorico (DataHistorico, IdCarteira);
END
GO		

IF NOT EXISTS (SELECT name FROM sysindexes WHERE name = 'PosicaoCotistaAbertura_Idx')
BEGIN
	CREATE INDEX PosicaoCotistaAbertura_Idx ON PosicaoCotistaAbertura (DataHistorico, IdCarteira);
END
GO

IF NOT EXISTS(select 1 from syscolumns where id = object_id('DePara') and name = 'DescricaoOrigem')
BEGIN
	alter table DePara add DescricaoOrigem [varchar](200) NULL;
END
GO
--Diferencial

IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'ImportacaoComeCotas') = 0
begin
	CREATE TABLE ImportacaoComeCotas
	(
		IdPosicao   			 INT 			NOT NULL,
		IdPosicaoComeCotas 		 varchar(100)	NOT NULL,
		TipoPosicao		 		 INT 			NOT NULL
		CONSTRAINT PK_ImportacaoComeCotas 		primary key(IdPosicao, TipoPosicao)
	)
END	
GO

IF NOT EXISTS (SELECT name FROM sysindexes WHERE name = 'ImportacaoComeCotas_Idx')
BEGIN
	CREATE INDEX ImportacaoComeCotas_Idx ON ImportacaoComeCotas (IdPosicaoComeCotas);
END
GO	

IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'PosicaoCotistaAlterada') = 0
begin
	CREATE TABLE PosicaoCotistaAlterada
	(
		Data						Datetime	   NOT NULL,
		IdPosicao   				INT 		   NOT NULL,
		ValorIOFAntigo				Decimal(16,2)  NULL,
		ValorIOFNovo				Decimal(16,2)  NULL,
		ValorIRAntigo				Decimal(16,2)  NULL,
		ValorIRNovo					Decimal(16,2)  NULL,
		QuantidadeAntigo			Decimal(28,12) NULL,
		QuantidadeNovo				Decimal(28,12) NULL,
		QuantidadeAntesCortesNovo	Decimal(28,12) NULL,
		QuantidadeAntesCortesAntigo	Decimal(28,12) NULL,
		CONSTRAINT PK_PosicaoCotistaAlterada 		primary key(Data, IdPosicao)
	)
END	
GO

if not exists(select 1 from syscolumns where id = object_id('Carteira') and name = 'DataInicioContIOF')
BEGIN
	ALTER TABLE dbo.Carteira ADD DataInicioContIOF smallint NULL;	
END
GO	

if not exists(select 1 from syscolumns where id = object_id('Carteira') and name = 'DataFimContIOF')
BEGIN
	ALTER TABLE dbo.Carteira ADD DataFimContIOF smallint NULL;	
END
GO	

if not exists(select 1 from syscolumns where id = object_id('Carteira') and name = 'ContaPrzIOFVirtual')
BEGIN
	ALTER TABLE dbo.Carteira ADD ContaPrzIOFVirtual smallint NOT NULL DEFAULT '2'
END
GO	

if not exists(select 1 from syscolumns where id = object_id('Carteira') and name = 'ProjecaoIOFResgate')
BEGIN
	ALTER TABLE Carteira ADD ProjecaoIOFResgate tinyint NOT NULL DEFAULT '2'
END
GO
	
if not exists(select 1 from syscolumns where id = object_id('Carteira') and name = 'DiasAposResgateIOF')
BEGIN
	ALTER TABLE Carteira ADD DiasAposResgateIOF int NOT NULL DEFAULT '0'
END
GO

IF EXISTS(SELECT * FROM sys.columns WHERE NAME = N'ProjecaoIRCotista' and Object_ID = Object_ID(N'Carteira'))
BEGIN
	exec sp_rename 'Carteira.ProjecaoIRCotista', 'ProjecaoIRResgate'	
END
GO

IF EXISTS(SELECT * FROM sys.columns WHERE NAME = N'DiasAposResgate' and Object_ID = Object_ID(N'Carteira'))
BEGIN
	if not exists(select 1 from syscolumns where id = object_id('Carteira') and name = 'DiasAposResgateIR')
	begin
		exec sp_rename 'Carteira.DiasAposResgate', 'DiasAposResgateIR'	
	end
	else
	begin
		DECLARE @ConstraintName nvarchar(200)
		SELECT @ConstraintName = Name FROM SYS.DEFAULT_CONSTRAINTS
		WHERE PARENT_OBJECT_ID = OBJECT_ID('CARTEIRA')
		AND PARENT_COLUMN_ID = (SELECT column_id FROM sys.columns
								WHERE NAME = N'DiasAposResgate'
								AND object_id = OBJECT_ID(N'CARTEIRA'))
		IF @ConstraintName IS NOT NULL
		EXEC('ALTER TABLE CARTEIRA DROP CONSTRAINT ' + @ConstraintName)	
		ALTER TABLE CARTEIRA DROP COLUMN DiasAposResgate
	end
END
GO
	
IF EXISTS(SELECT * FROM sys.columns WHERE NAME = N'DiasAposComeCotas' and Object_ID = Object_ID(N'Carteira'))
BEGIN
	if not exists(select 1 from syscolumns where id = object_id('Carteira') and name = 'DiasAposComeCotasIR')
	begin
		exec sp_rename 'Carteira.DiasAposComeCotas', 'DiasAposComeCotasIR'	
	end	
	else
	begin	
		DECLARE @ConstraintName nvarchar(200)
		SELECT @ConstraintName = Name FROM SYS.DEFAULT_CONSTRAINTS
		WHERE PARENT_OBJECT_ID = OBJECT_ID('CARTEIRA')
		AND PARENT_COLUMN_ID = (SELECT column_id FROM sys.columns
								WHERE NAME = N'DiasAposComeCotas'
								AND object_id = OBJECT_ID(N'CARTEIRA'))
		IF @ConstraintName IS NOT NULL
		EXEC('ALTER TABLE CARTEIRA DROP CONSTRAINT ' + @ConstraintName)		
		ALTER TABLE CARTEIRA DROP COLUMN DiasAposComeCotas
	end
END
GO	



if not exists(select 1 from syscolumns where id = object_id('TabelaCustosRendaFixa') and name = 'IdTitulo')
BEGIN
	alter table TabelaCustosRendaFixa add IdTitulo int null;
	ALTER TABLE tabelacustosrendafixa ADD CONSTRAINT FK_TabelaCustosRendaFixa_TituloRendaFixa FOREIGN KEY(IdTitulo) REFERENCES TituloRendaFixa(IdTitulo) ON DELETE CASCADE
END
GO	

if not exists(select 1 from syscolumns where id = object_id('TabelaCustosRendaFixa') and name = 'Classe')
BEGIN
	ALTER TABLE tabelacustosrendafixa ALTER COLUMN Classe int
END
GO		

IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'QueryCustom') = 0
BEGIN
CREATE TABLE QueryCustom
(
	IdQuery int PRIMARY KEY IDENTITY,
	Nome Varchar(40) NOT NULL,
	Descricao varchar(255),
	QueryCustom varchar(8000) NOT NULL
)
END
GO
	
if not exists(select 1 from syscolumns where id = object_id('Pessoa') and name = 'OffShore')
BEGIN
	ALTER TABLE dbo.Pessoa ADD OffShore char(1) NOT NULL DEFAULT 'N'
END
GO
	
if not exists(select 1 from syscolumns where id = object_id('AgenteMercado') and name = 'IdPessoa')
BEGIN
	ALTER TABLE AgenteMercado ADD IdPessoa int;
	alter table AgenteMercado add constraint AgenteMercado_Pessoa_FK FOREIGN KEY ( IdPessoa ) references Pessoa(IdPessoa)
END
GO	

if exists(select 1 from syscolumns where id = object_id('AgenteMercado') and name = 'Nome')
BEGIN
	ALTER TABLE dbo.AgenteMercado ALTER COLUMN Nome varchar(255) NOT NULL;
END
GO

if exists(select 1 from syscolumns where id = object_id('AgenteMercado') and name = 'Apelido')
BEGIN
	ALTER TABLE dbo.AgenteMercado ALTER COLUMN Apelido varchar(40) NULL;
END
GO

if exists(select 1 from syscolumns where id = object_id('Emissor') and name = 'Nome')
BEGIN
	ALTER TABLE dbo.Emissor ALTER COLUMN Nome varchar(255) NOT NULL;
END
GO

if not exists(select 1 from syscolumns where id = object_id('Emissor') and name = 'IdPessoa')
BEGIN
	ALTER TABLE Emissor ADD IdPessoa int;
	alter table Emissor add constraint Emissor_Pessoa_FK FOREIGN KEY ( IdPessoa ) references Pessoa(IdPessoa)
END
GO	

if exists(select 1 from syscolumns where id = object_id('Banco') and name = 'Nome')
BEGIN
	ALTER TABLE dbo.Banco ALTER COLUMN Nome varchar(255) NOT NULL;
END
GO

if not exists(select 1 from syscolumns where id = object_id('Banco') and name = 'IdPessoa')
BEGIN
	ALTER TABLE Banco ADD IdPessoa int;
	alter table Banco add constraint Banco_Pessoa_FK FOREIGN KEY ( IdPessoa ) references Pessoa(IdPessoa)
END
GO	

if not exists(select 1 from syscolumns where id = object_id('ContaCorrente') and name = 'IdCliente')
BEGIN
	ALTER TABLE ContaCorrente ADD IdCliente int;
	alter table ContaCorrente add constraint Pessoa_Cliente_FK FOREIGN KEY ( IdCliente ) references Cliente (IdCliente) 
END
GO	

if not exists(select 1 from syscolumns where id = object_id('ContaCorrente') and name = 'IdCotista')
BEGIN
	ALTER TABLE ContaCorrente ADD IdCotista int;
	alter table ContaCorrente add constraint Pessoa_Cotista_FK FOREIGN KEY ( IdCotista ) references Cotista(IdCotista) 
END
GO	

if not exists(select 1 from syscolumns where id = object_id('ContaCorrente') and name = 'ContaDefaultCliente')
BEGIN
	ALTER TABLE ContaCorrente ADD ContaDefaultCliente char NOT NULL default 'N';
END
GO	

if not exists(select 1 from syscolumns where id = object_id('ContaCorrente') and name = 'ContaDefaultCotista')
BEGIN
	ALTER TABLE ContaCorrente ADD ContaDefaultCotista char NOT NULL default 'N';
END
GO	

if not exists(select 1 from syscolumns where id = object_id('Cotista') and name = 'OffShore')
BEGIN
	ALTER TABLE dbo.Cotista ADD OffShore char(1) NOT NULL DEFAULT 'N'
END
GO

if not exists(select 1 from syscolumns where id = object_id('Cotista') and name = 'CodigoConsolidacaoExterno')
BEGIN
	ALTER TABLE dbo.Cotista Add CodigoConsolidacaoExterno varchar(60) NULL;
END
GO	

if not exists(select 1 from syscolumns where id = object_id('Carteira') and name = 'CodigoConsolidacaoExterno')
BEGIN
	ALTER TABLE dbo.Carteira Add CodigoConsolidacaoExterno varchar(60) NULL;
END
GO	

IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'Diretor') = 0
begin
	CREATE TABLE Diretor
	(
		IdDiretor				 INT 			NOT NULL PRIMARY KEY identity,
		Nome	 		 		 varchar(255)   NOT NULL,
		IdPessoa		 		 INT 			NULL,		
	)
	ALTER TABLE Diretor WITH CHECK ADD CONSTRAINT Pessoa_Diretor_FK1 FOREIGN KEY(IdPessoa)	REFERENCES Pessoa (IdPessoa) ON DELETE Cascade;
END	
GO


IF NOT EXISTS(select 1 from syscolumns where id = object_id('SerieRendaFixa') and name = 'CodigoBDS')
BEGIN
	ALTER TABLE SerieRendaFixa ADD CodigoBDS varchar(25) NULL
END
GO

IF NOT EXISTS(select 1 from syscolumns where id = object_id('AgendaComeCotas') and name = 'Residuo15')
BEGIN
	alter table AgendaComeCotas alter column Residuo15 decimal(38,28) NOT NULL
end
GO

IF NOT EXISTS(select 1 from syscolumns where id = object_id('AgendaComeCotas') and name = 'Residuo175')
BEGIN
	alter table AgendaComeCotas alter column Residuo175 decimal(38,28) NOT NULL
end
GO

IF NOT EXISTS(select 1 from syscolumns where id = object_id('AgendaComeCotas') and name = 'Residuo20')
BEGIN
	alter table AgendaComeCotas alter column Residuo20 decimal(38,28) NOT NULL
end
GO

IF NOT EXISTS(select 1 from syscolumns where id = object_id('AgendaComeCotas') and name = 'Residuo225')
BEGIN
	alter table AgendaComeCotas alter column Residuo225 decimal(38,28) NOT NULL
end
GO

IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'AgendaComeCotasAlterada') = 0
begin
	CREATE TABLE AgendaComeCotasAlterada
	(
		IdAgendaComeCotas   		INT 		   NOT NULL,
		ValorIrAgendadadoAntigo		Decimal(38,28) NULL,
		ValorIOFVirtualAntigo		Decimal(38,28) NULL,
		Residual15Antigo			Decimal(38,28) NULL,
		Residual175Antigo			Decimal(38,28) NULL,
		Residual20Antigo			Decimal(38,28) NULL,
		Residual225Antigo			Decimal(38,28) NULL,
		CONSTRAINT PK_AgendaComeCotasAlterada 		primary key(IdAgendaComeCotas)
	)
END	

IF NOT EXISTS(select 1 from syscolumns where id = object_id('PosicaoFundo') and name = 'QtdePendenteLiquidacao')
BEGIN
	ALTER TABLE PosicaoFundo ADD QtdePendenteLiquidacao decimal(28,12) NOT NULL DEFAULT('0');
	ALTER TABLE PosicaoFundoHistorico ADD QtdePendenteLiquidacao decimal(28,12) NOT NULL DEFAULT('0');
	ALTER TABLE PosicaoFundoAbertura ADD QtdePendenteLiquidacao decimal(28,12) NOT NULL DEFAULT('0');
END
GO

IF NOT EXISTS(select 1 from syscolumns where id = object_id('PosicaoCotista') and name = 'QtdePendenteLiquidacao')
BEGIN
	ALTER TABLE PosicaoCotista ADD QtdePendenteLiquidacao decimal(28,12) NOT NULL DEFAULT('0');
	ALTER TABLE PosicaoCotistaHistorico ADD QtdePendenteLiquidacao decimal(28,12) NOT NULL DEFAULT('0');
	ALTER TABLE PosicaoCotistaAbertura ADD QtdePendenteLiquidacao decimal(28,12) NOT NULL DEFAULT('0');
END
GO

IF NOT EXISTS(select 1 from syscolumns where id = object_id('PosicaoFundo') and name = 'ValorPendenteLiquidacao')
BEGIN
	ALTER TABLE PosicaoFundo ADD ValorPendenteLiquidacao decimal(16,2) NOT NULL DEFAULT('0');
	ALTER TABLE PosicaoFundoHistorico ADD ValorPendenteLiquidacao decimal(16,2) NOT NULL DEFAULT('0');
	ALTER TABLE PosicaoFundoAbertura ADD ValorPendenteLiquidacao decimal(16,2) NOT NULL DEFAULT('0');
END
GO

IF NOT EXISTS(select 1 from syscolumns where id = object_id('PosicaoCotista') and name = 'ValorPendenteLiquidacao')
BEGIN
	ALTER TABLE PosicaoCotista ADD ValorPendenteLiquidacao decimal(16,2) NOT NULL DEFAULT('0');
	ALTER TABLE PosicaoCotistaHistorico ADD ValorPendenteLiquidacao decimal(16,2) NOT NULL DEFAULT('0');
	ALTER TABLE PosicaoCotistaAbertura ADD ValorPendenteLiquidacao decimal(16,2) NOT NULL DEFAULT('0');
END
GO

IF NOT EXISTS(select 1 from syscolumns where id = object_id('EventoFundoFundo') and name = 'IdOperacao')
BEGIN
	delete from EventoFundoFundo;
	alter table EventoFundoFundo add IdOperacao int NOT NULL;
END
GO

if exists (select 1 from sysobjects where name = 'EventoFundoFundo_PK')
begin
	alter table EventoFundoFundo drop constraint EventoFundoFundo_PK
	IF EXISTS(select 1 from syscolumns where id = object_id('EventoFundoFundo') and name = 'DataAplicacao')
	BEGIN
		alter table EventoFundoFundo add constraint EventoFundoFundo_PK Primary Key CLUSTERED 
		(
			IdEventoFundo ASC, TipoMercado Asc, IdCarteira ASC, DataAplicacao ASC, DataConversao ASC, IdOperacao
		)
	END
END
go

IF NOT EXISTS(select 1 from syscolumns where id = object_id('TituloRendaFixa') and name = 'CodigoBDS')
BEGIN
	ALTER TABLE TituloRendaFixa ADD CodigoBDS int NULL
END
GO

if not exists(select 1 from syscolumns where id = object_id('AtivoBMF') and name = 'CodigoBDS')
BEGIN
	ALTER TABLE AtivoBMF ADD CodigoBDS	INT;
END
GO

if not exists(select 1 from syscolumns where id = object_id('Carteira') and name = 'CodigoBDS')
BEGIN
	ALTER TABLE Carteira ADD CodigoBDS	INT;
END
GO

if not exists(select 1 from syscolumns where id = object_id('AtivoBolsa') and name = 'CodigoBDS')
BEGIN
	ALTER TABLE AtivoBolsa ADD CodigoBDS	INT;
END
GO
	
IF EXISTS(select 1 from syscolumns where id = object_id('AgendaComeCotas') and name = 'Residuo15')
BEGIN
	alter table AgendaComeCotas alter column Residuo15 decimal(28,12) NOT NULL
end
GO

IF EXISTS(select 1 from syscolumns where id = object_id('AgendaComeCotas') and name = 'Residuo175')
BEGIN
	alter table AgendaComeCotas alter column Residuo175 decimal(28,12) NOT NULL
end
GO

IF EXISTS(select 1 from syscolumns where id = object_id('AgendaComeCotas') and name = 'Residuo20')
BEGIN
	alter table AgendaComeCotas alter column Residuo20 decimal(28,12) NOT NULL
end
GO

IF EXISTS(select 1 from syscolumns where id = object_id('AgendaComeCotas') and name = 'Residuo225')
BEGIN
	alter table AgendaComeCotas alter column Residuo225 decimal(28,12) NOT NULL
end
GO

IF EXISTS(select 1 from syscolumns where id = object_id('AgendaComeCotasAlterada') and name = 'ValorIrAgendadadoAntigo')
BEGIN
	exec sp_rename 'AgendaComeCotasAlterada.ValorIrAgendadadoAntigo', 'ValorIrAgendadoAntigo'
END
GO

IF EXISTS(select 1 from syscolumns where id = object_id('AgendaComeCotasAlterada') and name = 'ValorIrAgendadoAntigo')
BEGIN
	alter table AgendaComeCotasAlterada alter column ValorIrAgendadoAntigo decimal(28,12) NOT NULL
end
GO
IF EXISTS(select 1 from syscolumns where id = object_id('AgendaComeCotasAlterada') and name = 'ValorIOFVirtualAntigo')
BEGIN
	alter table AgendaComeCotasAlterada alter column ValorIOFVirtualAntigo decimal(28,12) NOT NULL
end
GO
IF EXISTS(select 1 from syscolumns where id = object_id('AgendaComeCotasAlterada') and name = 'Residual15Antigo')
BEGIN
	alter table AgendaComeCotasAlterada alter column Residual15Antigo decimal(28,12) NOT NULL
end
GO
IF EXISTS(select 1 from syscolumns where id = object_id('AgendaComeCotasAlterada') and name = 'Residual175Antigo')
BEGIN
	alter table AgendaComeCotasAlterada alter column Residual175Antigo decimal(28,12) NOT NULL
end
GO
IF EXISTS(select 1 from syscolumns where id = object_id('AgendaComeCotasAlterada') and name = 'Residual20Antigo')
BEGIN
	alter table AgendaComeCotasAlterada alter column Residual20Antigo decimal(28,12) NOT NULL
end
GO

IF EXISTS(select 1 from syscolumns where id = object_id('AgendaComeCotasAlterada') and name = 'Residual225Antigo')
BEGIN
	alter table AgendaComeCotasAlterada alter column Residual225Antigo decimal(28,12) NOT NULL
end
GO

IF (select count(*) from SYSCOLUMNS WHERE ID = (SELECT ID FROM SYSOBJECTS WHERE  NAME = 'TransferenciaEntreCotista' ) and name = 'IdAplicacao') = 0
begin
	alter table TransferenciaEntreCotista add IdAplicacao int NULL;
	alter table TransferenciaEntreCotista add constraint TransEntreCotista_Operacao_FK FOREIGN KEY ( IdAplicacao ) references OperacaoCotista(IdOperacao) ON DELETE CASCADE; 
END 
GO

IF NOT EXISTS(select 1 from syscolumns where id = object_id('EventoFundoBolsa') and name = 'IdAgente')
BEGIN
	delete from EventoFundoBolsa;
	alter table EventoFundoBolsa add IdAgente int NOT NULL;
END
GO

if exists (select 1 from sysobjects where name = 'EventoFundoBolsa_PK')
begin
	alter table EventoFundoBolsa drop constraint EventoFundoBolsa_PK
	alter table EventoFundoBolsa add constraint EventoFundoBolsa_PK Primary Key CLUSTERED 
	(
		IdEventoFundo ASC, TipoMercado Asc, CdAtivoBolsa ASC, IdAgente ASC
	)
end

IF NOT EXISTS(select 1 from syscolumns where id = object_id('EventoFundoBMF') and name = 'IdAgente')
BEGIN
	delete from EventoFundoBMF;
	alter table EventoFundoBMF add IdAgente int NOT NULL;
END
GO

if exists (select 1 from sysobjects where name = 'EventoFundoBMF_PK')
begin
	alter table EventoFundoBMF drop constraint EventoFundoBMF_PK
	alter table EventoFundoBMF add constraint EventoFundoBMF_PK Primary Key CLUSTERED 
	(
		IdEventoFundo ASC, TipoMercado Asc, CdAtivoBMF ASC, Serie ASC, IdAgente ASC
	)
end
GO

IF EXISTS(select 1 from syscolumns where id = object_id('Rentabilidade') and name = 'CodigoMoedaAtivo')
BEGIN
	Alter Table Rentabilidade alter column CodigoMoedaAtivo varchar(50);
end
GO

IF NOT EXISTS(select 1 from syscolumns where id = object_id('Carteira') and name = 'CodigoSTI')
BEGIN
	Alter Table Carteira add  CodigoSTI varchar(50);
end
GO

IF NOT EXISTS(select 1 from syscolumns where id = object_id('Moeda') and name = 'CodigoISO')
BEGIN
	Alter Table Moeda add  CodigoISO char(3);
end
GO

if not exists(select 1 from syscolumns where id = object_id('Carteira') and name = 'TipoVisualizacaoResgCotista')
BEGIN
	ALTER TABLE dbo.Carteira ADD TipoVisualizacaoResgCotista char(1) NOT NULL DEFAULT 'C'
END
GO

IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'V' AND NAME = 'Cadastros') > 0
BEGIN
	drop view Cadastros;
END	
go

create view [dbo].[Cadastros] as
select
	null as 'Nome',
	null as 'CNPJ',
	ContaCorrente.IdPessoa,
	ContaCorrente.IdBanco,
	Banco.Nome as 'BancoNome',
	ContaCorrente.IdAgencia,
	Agencia.Nome as 'AgenciaNome',
	Numero,
	DigitoConta,
	ContaCorrente.IdTipoConta,
	IdMoeda,
	null as 'CodigoBovespa',
	null as 'CodigoBMF',
	null as 'CodigoCetip',
	null as 'CorretagemAdicional',
	null as 'Apelido',
	null as 'IdSetor',
	null as 'SetorNome',
	null as 'IdAgente',
	null as 'AgenteNome',
	null as 'CodigoInterface',
	null as 'TipoEmissor'
from ContaCorrente 
left outer join Banco on ContaCorrente.IdBanco = Banco.IdBanco
left outer  join Agencia on ContaCorrente.IdAgencia = Agencia.IdAgencia
union
select 
	Nome,
	CNPJ,
	null, 
	null, 
	null, 
	null, 
	null, 
	null, 
	null, 
	null, 
	null, 
	CodigoBovespa,
	CodigoBMF,
	CodigoCetip,
	CorretagemAdicional,
	Apelido,
	null, 
	null, 
	null, 
	null, 
	null, 
	null
from AgenteMercado 

union

select
	Emissor.Nome,
	Emissor.CNPJ,
	null, 
	null, 
	null, 
	null, 
	null, 
	null, 
	null, 
	null, 
	null, 
	null, 
	null, 
	null, 
	null, 
	null, 
	Emissor.IdSetor,
	Setor.Nome,
	Emissor.IdAgente,
	AgenteMercado.Nome,
	CodigoInterface,
	TipoEmissor
from Emissor left join Setor
on Emissor.IdSetor = Setor.IdSetor
left join AgenteMercado
on Emissor.IdAgente = AgenteMercado.IdAgente;
go

IF EXISTS(select 1 from syscolumns where id = object_id('SerieRendaFixa') and name = 'CodigoBDS')
BEGIN
	ALTER TABLE SerieRendaFixa ALTER COLUMN CodigoBDS INT NULL
END
GO

IF NOT EXISTS(select 1 from syscolumns where id = object_id('Carteira') and name = 'ApenasInvestidorProfissional')
BEGIN
	Alter Table Carteira add ApenasInvestidorProfissional char(1) NOT NULL default 'N';
end
GO

IF NOT EXISTS(select 1 from syscolumns where id = object_id('Carteira') and name = 'ApenasInvestidorQualificado')
BEGIN
	Alter Table Carteira add ApenasInvestidorQualificado char(1) NOT NULL default 'N';
end
GO

IF NOT EXISTS(select 1 from syscolumns where id = object_id('Carteira') and name = 'RealizaOfertaSubscricao')
BEGIN
	Alter Table Carteira add RealizaOfertaSubscricao char(1) NOT NULL default 'N';
end
GO

IF NOT EXISTS(select 1 from syscolumns where id = object_id('Cliente') and name = 'InvestidorProfissional')
BEGIN
	Alter Table Cliente add InvestidorProfissional char(1) NOT NULL default 'N';
end
GO

IF NOT EXISTS(select 1 from syscolumns where id = object_id('Cliente') and name = 'InvestidorQualificado')
BEGIN
	Alter Table Cliente add InvestidorQualificado char(1) NOT NULL default 'N';
end
GO

IF NOT EXISTS(select 1 from syscolumns where id = object_id('Cotista') and name = 'InvestidorProfissional')
BEGIN
	Alter Table Cotista add InvestidorProfissional char(1) NOT NULL default 'N';
end
GO

IF NOT EXISTS(select 1 from syscolumns where id = object_id('Cotista') and name = 'InvestidorQualificado')
BEGIN
	Alter Table Cotista add InvestidorQualificado char(1) NOT NULL default 'N';
end
GO

IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'OfertaDistribuicaoCotas') = 0
begin
	CREATE TABLE OfertaDistribuicaoCotas
	(
		IdOfertaDistribuicaoCotas		INT				NOT NULL PRIMARY KEY identity,
		IdCarteira				   		INT				NOT NULL,
		PrimeiraEmissao					Char(1)			NOT NULL,
		ApenasInvestidorProfissional	Char(1)			NOT NULL,
		ApenasInvestidorQualificado		Char(1)			NOT NULL,
		DataInicialOferta				DateTime		NOT NULL,
		DataFinalOferta					DateTime		NULL,
		QuantidadeTotalCotas			int				NOT NULL,
		CotaEspecifica					char(1)			NOT NULL,
		ValorCota						Decimal(28,12)  NOT NULL
	)
	ALTER TABLE OfertaDistribuicaoCotas WITH CHECK ADD CONSTRAINT Carteira_OfertaDistribuicaoCotas_FK1 FOREIGN KEY(IdCarteira) REFERENCES Carteira (IdCarteira) ON DELETE Cascade;
END	
GO

IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'AplicacaoOfertaDistribuicaoCotas') = 0
begin
	CREATE TABLE AplicacaoOfertaDistribuicaoCotas
	(
		IdOfertaDistribuicaoCotas		INT NOT NULL,
		IdOperacao						INT NOT NULL,
		CONSTRAINT PK_AplicacaoOfertaDistribuicaoCotas 		primary key(IdOfertaDistribuicaoCotas, IdOperacao)
	)
	ALTER TABLE AplicacaoOfertaDistribuicaoCotas WITH CHECK ADD CONSTRAINT AplicacaoOfertaDistribuicaoCotas_OfertaDistribuicaoCotas_FK1 FOREIGN KEY(IdOfertaDistribuicaoCotas)	REFERENCES OfertaDistribuicaoCotas (IdOfertaDistribuicaoCotas);
	ALTER TABLE AplicacaoOfertaDistribuicaoCotas WITH CHECK ADD CONSTRAINT AplicacaoOfertaDistribuicaoCotas_OperacaoCotista_FK1 FOREIGN KEY(IdOperacao)	REFERENCES OperacaoCotista (IdOperacao);
end
GO

if not exists(select 1 from syscolumns where id = object_id('TituloRendaFixa') and name = 'OpcaoEmbutida')
BEGIN
	ALTER TABLE dbo.TituloRendaFixa ADD OpcaoEmbutida char(1) NOT NULL DEFAULT 'N'
END
GO

if not exists(select 1 from syscolumns where id = object_id('TituloRendaFixa') and name = 'TipoOpcao')
BEGIN
	ALTER TABLE dbo.TituloRendaFixa ADD TipoOpcao smallint null 
END
GO
	
if not exists(select 1 from syscolumns where id = object_id('TituloRendaFixa') and name = 'TipoExercicio')
BEGIN
	ALTER TABLE dbo.TituloRendaFixa ADD TipoExercicio smallint null 
END
GO	

if not exists(select 1 from syscolumns where id = object_id('TituloRendaFixa') and name = 'TipoPremio')
BEGIN
	ALTER TABLE dbo.TituloRendaFixa ADD TipoPremio smallint null 
END
GO	

if not exists(select 1 from syscolumns where id = object_id('TituloRendaFixa') and name = 'DataInicioExercicio')
BEGIN
	ALTER TABLE dbo.TituloRendaFixa ADD DataInicioExercicio datetime null 
END
GO	

if not exists(select 1 from syscolumns where id = object_id('TituloRendaFixa') and name = 'DataVencimentoOpcao')
BEGIN
	ALTER TABLE dbo.TituloRendaFixa ADD DataVencimentoOpcao datetime null 
END
GO	

if not exists(select 1 from syscolumns where id = object_id('TituloRendaFixa') and name = 'ValorPremio')
BEGIN
	ALTER TABLE dbo.TituloRendaFixa ADD ValorPremio decimal(16,8) null 
END
GO	

if exists(select 1 from syscolumns where id = object_id('Pessoa') and name = 'CodigoInterface')
BEGIN
	ALTER TABLE Pessoa ALTER COLUMN CodigoInterface varchar(40) NULL; 
END
GO		

if exists(select 1 from syscolumns where id = object_id('PapelRendaFixa') and name = 'CodigoInterface')
BEGIN
	ALTER TABLE PapelRendaFixa ALTER COLUMN CodigoInterface varchar(40) NULL ;
END
GO		

if exists(select 1 from syscolumns where id = object_id('TituloRendaFixa') and name = 'CodigoInterface')
BEGIN
	ALTER TABLE TituloRendaFixa ALTER COLUMN CodigoInterface varchar(40) NULL ;
END
GO		

if exists(select 1 from syscolumns where id = object_id('Emissor') and name = 'CodigoInterface')
BEGIN
	ALTER TABLE Emissor ALTER COLUMN CodigoInterface varchar(40) NULL ;
END
GO		

if exists(select 1 from syscolumns where id = object_id('Banco') and name = 'DescricaoCodigoExterno')
BEGIN
	ALTER TABLE Banco ALTER COLUMN DescricaoCodigoExterno varchar(40) NULL ;
END
GO		

if exists(select 1 from syscolumns where id = object_id('Banco') and name = 'CodigoExterno')
BEGIN	
	ALTER TABLE Banco ALTER COLUMN CodigoExterno varchar(36) NULL ;
END
GO		

if exists(select 1 from syscolumns where id = object_id('Cotista') and name = 'CodigoInterface')
BEGIN
	ALTER TABLE Cotista ALTER COLUMN CodigoInterface varchar(40) NULL ;
END
GO		

IF EXISTS(select 1 from syscolumns where id = object_id('CadastroComplementarCampos') and name = 'DescricaoCampo')
BEGIN
	alter table CadastroComplementarCampos alter column DescricaoCampo varchar(160);
END
GO	

if exists(select 1 from syscolumns where id = object_id('ContaCorrente') and name = 'DescricaoCodigo')
BEGIN
	ALTER TABLE ContaCorrente ALTER COLUMN DescricaoCodigo varchar(50) NULL ;
END
GO

if not exists(select 1 from syscolumns where id = object_id('AgendaFundo') and name = 'TipoValorInput')
BEGIN
	ALTER TABLE AgendaFundo ADD TipoValorInput int NOT NULL default 1;
END
GO		

if not exists(select 1 from syscolumns where id = object_id('Cliente') and name = 'AmortizacaoRendimentoJuros')
BEGIN
	ALTER TABLE dbo.Cliente ADD AmortizacaoRendimentoJuros char(1) NOT NULL default 'N'
END
GO		

IF NOT EXISTS(select 1 from syscolumns where id = object_id('PosicaoRendaFixa') and name = 'IdOperacao')
BEGIN
	ALTER TABLE PosicaoRendaFixa ADD IdOperacao int NOT NULL;
END
GO		
	
if not EXISTS(select 1 from sysconstraints where object_name(id) = 'PosicaoRendaFixa' and object_name(constid) = 'Operacao_PosicaoRendaFixa_FK1')
Begin
	ALTER TABLE PosicaoRendaFixa
	ADD CONSTRAINT Operacao_PosicaoRendaFixa_FK1
	FOREIGN KEY (IdOperacao)
	REFERENCES OperacaoRendaFixa(IdOPeracao);
END
GO

IF  EXISTS (select 1 from sysconstraints where object_name(id) = 'EventoFundoMovimentoAtivos' and object_name(constid) = 'EventoFundoMovimentoAtivos_EventoFundo_FK1')
BEGIN 
	alter table EventoFundoMovimentoAtivos drop constraint EventoFundoMovimentoAtivos_EventoFundo_FK1;
END
GO

IF  EXISTS (select 1 from sysconstraints where object_name(id) = 'EventoFundoMovimentoCautelas' and object_name(constid) = 'EventoFundoMovimentoCautelas_EventoFundo_FK1')
BEGIN 
	alter table EventoFundoMovimentoCautelas drop constraint EventoFundoMovimentoCautelas_EventoFundo_FK1;
END
GO

IF EXISTS (select 1 from sysconstraints where object_name(id) = 'TributacaoFundoFie' and object_name(constid) = 'TributacaoFundoFie_OperacaoFundo_FK')
BEGIN 
	alter table TributacaoFundoFie drop constraint TributacaoFundoFie_OperacaoFundo_FK;
END
GO

IF  EXISTS (select 1 from sysconstraints where object_name(id) = 'EventoFundoPosicaoAtivos' and object_name(constid) = 'EventoFundoPosicaoAtivos_EventoFundo_FK1')
BEGIN 
	alter table EventoFundoPosicaoAtivos drop constraint EventoFundoPosicaoAtivos_EventoFundo_FK1;
END
GO

IF  EXISTS (select 1 from sysconstraints where object_name(id) = 'EventoFundoRendaFixa' and object_name(constid) = 'EventoFundoRendaFixa_EventoFundo_FK1')
BEGIN 
	alter table EventoFundoRendaFixa drop constraint EventoFundoRendaFixa_EventoFundo_FK1;
END
GO

IF  EXISTS (select 1 from sysconstraints where object_name(id) = 'EventoFundoBolsa' and object_name(constid) = 'EventoFundoBolsa_EventoFundo_FK1')
BEGIN 
	alter table EventoFundoBolsa drop constraint EventoFundoBolsa_EventoFundo_FK1;
END
GO

IF  EXISTS (select 1 from sysconstraints where object_name(id) = 'EventoFundoPosicaoCautelas' and object_name(constid) = 'EventoFundoPosicaoCautelas_EventoFundo_FK1')
BEGIN 
	alter table EventoFundoPosicaoCautelas drop constraint EventoFundoPosicaoCautelas_EventoFundo_FK1;
END
GO

IF  EXISTS (select 1 from sysconstraints where object_name(id) = 'AplicacaoOfertaDistribuicaoCotas' and object_name(constid) = 'AplicacaoOfertaDistribuicaoCotas_OfertaDistribuicaoCotas_FK1')
BEGIN 
	alter table AplicacaoOfertaDistribuicaoCotas drop constraint AplicacaoOfertaDistribuicaoCotas_OfertaDistribuicaoCotas_FK1;
END
GO

IF  EXISTS (select 1 from sysconstraints where object_name(id) = 'TributacaoCautelaFie' and object_name(constid) = 'TributacaoCautelaFie_OperacaoCotista_FK')
BEGIN 
	alter table TributacaoCautelaFie drop constraint TributacaoCautelaFie_OperacaoCotista_FK;
END
GO

IF NOT EXISTS (select 1 from sysconstraints where object_name(id) = 'EventoFundoMovimentoAtivos' and object_name(constid) = 'EventoFundoMovimentoAtivos_EventoFundo_FK1')
BEGIN 
	ALTER TABLE EventoFundoMovimentoAtivos
	ADD CONSTRAINT EventoFundoMovimentoAtivos_EventoFundo_FK1
	FOREIGN KEY (IdEventoFundo)
	REFERENCES EventoFundo(IdEventoFundo) on delete cascade;
END
GO
		
IF NOT EXISTS (select 1 from sysconstraints where object_name(id) = 'EventoFundoMovimentoCautelas' and object_name(constid) = 'EventoFundoMovimentoCautelas_EventoFundo_FK1')
BEGIN 
	ALTER TABLE EventoFundoMovimentoCautelas
	ADD CONSTRAINT EventoFundoMovimentoCautelas_EventoFundo_FK1
	FOREIGN KEY (IdEventoFundo)
	REFERENCES EventoFundo(IdEventoFundo) on delete cascade;
END
GO

IF NOT EXISTS (select 1 from sysconstraints where object_name(id) = 'EventoFundoPosicaoAtivos' and object_name(constid) = 'EventoFundoPosicaoAtivos_EventoFundo_FK1')
BEGIN 
	ALTER TABLE EventoFundoPosicaoAtivos
	ADD CONSTRAINT EventoFundoPosicaoAtivos_EventoFundo_FK1
	FOREIGN KEY (IdEventoFundo)
	REFERENCES EventoFundo(IdEventoFundo) on delete cascade;
END
GO

IF NOT EXISTS (select 1 from sysconstraints where object_name(id) = 'EventoFundoRendaFixa' and object_name(constid) = 'EventoFundoRendaFixa_EventoFundo_FK1')
BEGIN 
	ALTER TABLE EventoFundoRendaFixa
	ADD CONSTRAINT EventoFundoRendaFixa_EventoFundo_FK1
	FOREIGN KEY (IdEventoFundo)
	REFERENCES EventoFundo(IdEventoFundo) on delete cascade;
END
GO
	
IF NOT EXISTS (select 1 from sysconstraints where object_name(id) = 'EventoFundoBolsa' and object_name(constid) = 'EventoFundoBolsa_EventoFundo_FK1')
BEGIN 
	ALTER TABLE EventoFundoBolsa
	ADD CONSTRAINT EventoFundoBolsa_EventoFundo_FK1
	FOREIGN KEY (IdEventoFundo)
	REFERENCES EventoFundo(IdEventoFundo) on delete cascade;
END
GO

IF NOT EXISTS (select 1 from sysconstraints where object_name(id) = 'EventoFundoPosicaoCautelas' and object_name(constid) = 'EventoFundoPosicaoCautelas_EventoFundo_FK1')
BEGIN 
	ALTER TABLE EventoFundoPosicaoCautelas
	ADD CONSTRAINT EventoFundoPosicaoCautelas_EventoFundo_FK1
	FOREIGN KEY (IdEventoFundo)
	REFERENCES EventoFundo(IdEventoFundo) on delete cascade;
END
GO
	
IF NOT EXISTS (select 1 from sysconstraints where object_name(id) = 'TributacaoFundoFie' and object_name(constid) = 'TributacaoFundoFie_OperacaoFundo_FK')
BEGIN 
	ALTER TABLE TributacaoFundoFie
	ADD CONSTRAINT TributacaoFundoFie_OperacaoFundo_FK
	FOREIGN KEY (IdOperacao)
	REFERENCES OperacaoFundo(IdOperacao) on delete cascade;
END
GO

IF NOT EXISTS (select 1 from sysconstraints where object_name(id) = 'TributacaoCautelaFie' and object_name(constid) = 'TributacaoCautelaFie_OperacaoCotista_FK')
BEGIN 
	ALTER TABLE TributacaoCautelaFie
	ADD CONSTRAINT TributacaoCautelaFie_OperacaoCotista_FK
	FOREIGN KEY (IdOperacao)
	REFERENCES OperacaoCotista(IdOperacao) on delete cascade;
END
GO

IF NOT EXISTS (select 1 from sysconstraints where object_name(id) = 'AplicacaoOfertaDistribuicaoCotas' and object_name(constid) = 'AplicacaoOfertaDistribuicaoCotas_OfertaDistribuicaoCotas_FK1')
BEGIN 
	ALTER TABLE AplicacaoOfertaDistribuicaoCotas
	ADD CONSTRAINT AplicacaoOfertaDistribuicaoCotas_OfertaDistribuicaoCotas_FK1
	FOREIGN KEY (IdOfertaDistribuicaoCotas)
	REFERENCES OfertaDistribuicaoCotas(IdOfertaDistribuicaoCotas) on delete cascade;	
END
GO

if not exists(select 1 from syscolumns where id = object_id('Cliente') and name = 'AmortizacaoRendimentoJuros')
BEGIN
	ALTER TABLE dbo.Cliente ADD AmortizacaoRendimentoJuros char(1) NOT NULL default 'N'
END
GO

IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'ClasseCota') = 0
begin
	CREATE TABLE ClasseCota
	(
		IdClasseCota	int identity NOT NULL,
		Descricao		varchar(100) NOT NULL,
		CONSTRAINT PK_ClasseCota 		primary key(IdClasseCota)
	)
end
GO

IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'ClasseCota') = 0
begin
	CREATE TABLE ClasseCota
	(
		IdClasseCota	int identity NOT NULL,
		Descricao		varchar(100) NOT NULL,
		CONSTRAINT PK_ClasseCota 		primary key(IdClasseCota)
	)
end
GO

IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'CarteiraComplemento') = 0
begin
	CREATE TABLE CarteiraComplemento
	(
		IdCarteiraComplemento			INT				NOT NULL identity,
		IdCarteira						INT				NOT NULL,
		NomeEmissaoSerie				varchar(100)	NOT NULL,
		IdCarteiraFundoPai				INT				NULL,
		CodigoTituloBovespa				varchar(20)		NULL,
		MultiplasEmissoes				char(1)         NOT NULL default 'N',
		MultiplasSeries					char(1)         NOT NULL default 'N',
		NumeroEmissao					int             NOT NULL default 0,
		NumeroSerie						int             NOT NULL default 0,
		DataBaseInicioEmissaoSerie		DateTime        NOT NULL,
		EmissaoEncerravel				char(1)			NOT NULL default 'S',
		DataFimEmissao					DateTime		NULL,
		IdClasseCota					int				NOT NULL,
		QuantidadeCotasAportadas		Decimal(18,12)  NULL,
		PermiteRendimento				char(1)			NOT NULL default 'S',
		PermiteAmortizacao				char(1)         NOT NULL default 'S',
		PeriodicidadeCota				int				NULL,
		PeriodicidadeRendimento			int				NULL,
		InicioPeriodicidadeRendimento	DateTime        NULL,
		PeriodicidadeAmortizacao		int				NULL,
		InicioPeriodicidadeAmortizacao	DateTime        NULL,
		RetemIRFonteRendimento			char(1)			NOT NULL default 'N',
		ExecutaResgateFimEmissao		char(1)			NOT NULL default 'N',
		CONSTRAINT PK_CarteiraComplemento 		primary key(IdCarteiraComplemento)
	)
	ALTER TABLE CarteiraComplemento WITH CHECK ADD CONSTRAINT CarteiraComplemento_Carteira_FK1 FOREIGN KEY(IdCarteira) REFERENCES Carteira (IdCarteira);
	ALTER TABLE CarteiraComplemento WITH CHECK ADD CONSTRAINT CarteiraComplemento_Carteira_FK2 FOREIGN KEY(IdCarteiraFundoPai) REFERENCES Carteira (IdCarteira);
	ALTER TABLE CarteiraComplemento WITH CHECK ADD CONSTRAINT CarteiraComplemento_ClasseCota_FK1 FOREIGN KEY(IdClasseCota) REFERENCES ClasseCota (IdClasseCota);
end
GO

IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'PatrimonioFundoEmissaoSerie') = 0
begin
	CREATE TABLE PatrimonioFundoEmissaoSerie
	(
		IdCarteira				int NOT NULL,
		Competencia				DateTime NOT NULL,
		DataBaseCota			DateTime NOT NULL,
		DataCotaEx				DateTime NOT NULL,
		PatrimonioLiquido		Decimal(16,2) NOT NULL,
		QuantidadeCotas			Decimal(28,12) NOT NULL,
		VlCotaPatrimonial		Decimal(16,2) NOT NULL,
		VlRendimentoDistribuido Decimal(16,2) NOT NULL,
		VlRendimentoPorCota		Decimal(16,2) NOT NULL,
		VlAmortizacao			Decimal(16,2) NOT NULL,
		VlAmortizacaoPorCota	Decimal(16,2) NOT NULL,
		VlDividendoDistribuido	Decimal(16,2) NOT NULL,
		VlDividendoPorCota		Decimal(16,2) NOT NULL,
		CotaRendimento			Decimal(28,12) NOT NULL,
		CONSTRAINT PK_PatrimonioFundoEmissaoSerie 		primary key(IdCarteira, Competencia)
	)
	ALTER TABLE PatrimonioFundoEmissaoSerie WITH CHECK ADD CONSTRAINT PatrimonioFundoEmissaoSerie_Carteira_FK1 FOREIGN KEY(IdCarteira) REFERENCES Carteira (IdCarteira);
end
GO

IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'ComplementoProventosCotista') = 0
begin
	CREATE TABLE ComplementoProventosCotista
	(
		IdOperacao				int NOT NULL,
		IdOperacaoReferencia	int NOT NULL,
		Rendimento				Decimal(16,2) NOT NULL default 0,
		Quantidade				Decimal(28,12) NOT NULL default 0,
		DataAplicacao			DateTime NOT NULL,
		CONSTRAINT PK_ComplementoProventosCotista 		primary key(IdOperacao)
	)
	ALTER TABLE ComplementoProventosCotista WITH CHECK ADD CONSTRAINT ComplementoProventosCotista_OperacaoCotista_FK FOREIGN KEY(IdOperacao) REFERENCES OperacaoCotista (IdOperacao) on delete cascade;
end
GO

if not exists(select 1 from syscolumns where id = object_id('Carteira') and name = 'Rendimento')
BEGIN
	ALTER TABLE Carteira ADD Rendimento int NOT NULL default 1
END
GO

if not exists(select 1 from syscolumns where id = object_id('HistoricoCota') and name = 'CotaEx')
BEGIN
	ALTER TABLE HistoricoCota ADD CotaEx decimal(28,10) NOT NULL default 0
END
GO

if not exists(select 1 from syscolumns where id = object_id('HistoricoCota') and name = 'CotaRendimento')
BEGIN
	ALTER TABLE HistoricoCota ADD CotaRendimento decimal(28,10) NOT NULL default 0
END
GO

if not exists(select 1 from syscolumns where id = object_id('Rentabilidade') and name = 'ValorBrutoGrossUpMoedaAtivo')
BEGIN
	ALTER TABLE Rentabilidade ADD ValorBrutoGrossUpMoedaAtivo Decimal(25,2) NULL ;
END
GO

--if not exists(select 1 from syscolumns where id = object_id('Rentabilidade') and name = 'RentabilidadeGrossUpMoedaAtivo')
--BEGIN	
--	ALTER TABLE Rentabilidade ADD RentabilidadeGrossUpMoedaAtivo Decimal(25,12) NULL ;
--END
--GO	
--
--if not exists(select 1 from syscolumns where id = object_id('Rentabilidade') and name = 'RentabilidadeSemGrossUpMoedaAtivo')
--BEGIN	
--	ALTER TABLE Rentabilidade ADD RentabilidadeSemGrossUpMoedaAtivo Decimal(25,12) NULL;
--END
--GO		
	
if not exists(select 1 from syscolumns where id = object_id('Rentabilidade') and name = 'ValorBrutoGrossUpMoedaPortfolio')
BEGIN	
	ALTER TABLE Rentabilidade ADD ValorBrutoGrossUpMoedaPortfolio Decimal(25,2) NULL ;	
END
GO	
	
if not exists(select 1 from syscolumns where id = object_id('Rentabilidade') and name = 'RentabilidadeGrossUpMoedaPortfolio')
BEGIN	
	ALTER TABLE Rentabilidade ADD RentabilidadeGrossUpMoedaPortfolio Decimal(25,12) NULL;
END
GO		

if not exists(select 1 from syscolumns where id = object_id('Rentabilidade') and name = 'RentabilidadeSemGrossUpMoedaPortfolio')
BEGIN	
	ALTER TABLE Rentabilidade ADD RentabilidadeSemGrossUpMoedaPortfolio Decimal(25,12) NULL;
END
GO		
--
--if not exists(select 1 from syscolumns where id = object_id('Rentabilidade') and name = 'CotaAtivoIncentivado')
--BEGIN	
--	ALTER TABLE Rentabilidade ADD CotaAtivoIncentivado Decimal(28,12) NULL;
--END
--GO	
--
--if not exists(select 1 from syscolumns where id = object_id('Rentabilidade') and name = 'CotaDemaisAtivos')
--BEGIN	
--	ALTER TABLE Rentabilidade ADD CotaDemaisAtivos Decimal(28,12) NULL;
--END
--GO			
--
if not exists(select 1 from syscolumns where id = object_id('PosicaoRendaFixa') and name = 'ValorBrutoGrossUp')
BEGIN
	ALTER TABLE PosicaoRendaFixa ADD ValorBrutoGrossUp decimal(28,10) NULL default 0;
END
GO

if not exists(select 1 from syscolumns where id = object_id('PosicaoRendaFixaHistorico') and name = 'ValorBrutoGrossUp')
BEGIN
	ALTER TABLE PosicaoRendaFixaHistorico ADD ValorBrutoGrossUp decimal(28,10) NOT NULL default 0;
END
GO

if not exists(select 1 from syscolumns where id = object_id('PosicaoRendaFixaAbertura') and name = 'ValorBrutoGrossUp')
BEGIN
	ALTER TABLE PosicaoRendaFixaAbertura ADD ValorBrutoGrossUp decimal(28,10) NOT NULL default 0;
END
GO


if not exists(select 1 from syscolumns where id = object_id('PosicaoRendaFixa') and name = 'AliquotaIR')
BEGIN
	ALTER TABLE PosicaoRendaFixaHistorico ADD AliquotaIR decimal(28,10) NOT NULL default 0;
	ALTER TABLE PosicaoRendaFixaAbertura ADD AliquotaIR decimal(28,10) NOT NULL default 0;
	ALTER TABLE PosicaoRendaFixa ADD AliquotaIR decimal(28,10) NOT NULL default 0;
END
GO

if not exists(select 1 from syscolumns where id = object_id('PosicaoRendaFixaHistorico') and name = 'AliquotaIOF')
BEGIN
	ALTER TABLE PosicaoRendaFixaHistorico ADD AliquotaIOF decimal(28,10) NOT NULL default 0;
	ALTER TABLE PosicaoRendaFixaAbertura ADD AliquotaIOF decimal(28,10) NOT NULL default 0;
	ALTER TABLE PosicaoRendaFixa ADD AliquotaIOF decimal(28,10) NOT NULL default 0;
END
GO

if not exists(select 1 from syscolumns where id = object_id('PosicaoRendaFixaHistorico') and name = 'PrazoDecorridoDC')
BEGIN
	ALTER TABLE PosicaoRendaFixaHistorico ADD PrazoDecorridoDC int NOT NULL default 0;
	ALTER TABLE PosicaoRendaFixaAbertura ADD PrazoDecorridoDC int NOT NULL default 0;
	ALTER TABLE PosicaoRendaFixa ADD PrazoDecorridoDC int NOT NULL default 0;
END
GO

if not exists(select 1 from syscolumns where id = object_id('PosicaoRendaFixaHistorico') and name = 'PrazoDecorridoDU')
BEGIN
	ALTER TABLE PosicaoRendaFixaHistorico ADD PrazoDecorridoDU int NOT NULL default 0;
	ALTER TABLE PosicaoRendaFixaAbertura ADD PrazoDecorridoDU int NOT NULL default 0;
	ALTER TABLE PosicaoRendaFixa ADD PrazoDecorridoDU int NOT NULL default 0;
END
GO

if (select count(*) from SYSCOLUMNS WHERE ID = (SELECT ID FROM SYSOBJECTS WHERE  NAME = 'OperacaoFundo' ) and name = 'Observacao') > 0
begin
	ALTER TABLE OperacaoFundo ALTER COLUMN Observacao varchar(400) null;
	ALTER TABLE OperacaoFundoAux ALTER COLUMN Observacao varchar(400) null;
END	
GO

if (select count(*) from SYSCOLUMNS WHERE ID = (SELECT ID FROM SYSOBJECTS WHERE  NAME = 'OperacaoCotista' ) and name = 'Observacao') > 0
begin
	ALTER TABLE OperacaoCotista ALTER COLUMN Observacao varchar(400) null;
	ALTER TABLE OperacaoCotistaAux ALTER COLUMN Observacao varchar(400) null;
END	
GO

if exists (select 1 from sysobjects where name = 'EventoFundoFundo_PK')
begin
	delete from EventoFundoFundo;
	alter table EventoFundoFundo drop constraint EventoFundoFundo_PK
	alter table EventoFundoFundo add constraint EventoFundoFundo_PK Primary Key CLUSTERED 
	(
		IdEventoFundo ASC, TipoMercado Asc, IdCarteira ASC
	)
end

if exists(select 1 from syscolumns where id = object_id('EventoFundoFundo') and name = 'DataAplicacao')
BEGIN
	ALTER TABLE EventoFundoFundo DROP COLUMN DataAplicacao;
END
GO

if exists(select 1 from syscolumns where id = object_id('EventoFundoFundo') and name = 'DataConversao')
BEGIN
	ALTER TABLE EventoFundoFundo DROP COLUMN DataConversao;
END
GO

if exists(select 1 from syscolumns where id = object_id('EventoFundoFundo') and name = 'IdOperacao')
BEGIN
	ALTER TABLE EventoFundoFundo DROP COLUMN IdOperacao;
END
GO

if exists(select 1 from syscolumns where id = object_id('OperacaoFundoAux') and name = 'IdFormaLiquidacao')
BEGIN
	ALTER TABLE OperacaoFundoAux alter column IdFormaLiquidacao	tinyint NULL;
END
GO

IF NOT EXISTS(SELECT 1 FROM SYSCOLUMNS WHERE ID= OBJECT_ID('OperacaoFundoAux') AND NAME = 'DataRegistro')
BEGIN
	ALTER TABLE OperacaoFundoAux add DataRegistro Datetime;

	EXEC sp_executesql
            N'update OperacaoFundoAux set DataRegistro = DataOperacao'

	alter table OperacaoFundoAux alter column DataRegistro datetime not null;
END
GO

IF NOT EXISTS(SELECT 1 FROM SYSCOLUMNS WHERE ID=OBJECT_ID('OperacaoFundoAux') AND NAME = 'DepositoRetirada')
BEGIN
	ALTER TABLE OperacaoFundoAux add DepositoRetirada bit not null default 0;
END
GO

if not exists(select 1 from syscolumns where id = object_id('OperacaoFundoAux') and name = 'FieModalidade')
BEGIN	
	ALTER TABLE [dbo].[OperacaoFundoAux] ADD FieModalidade int NULL;
END
GO	

if not exists(select 1 from syscolumns where id = object_id('OperacaoFundoAux') and name = 'FieTabelaIr')
BEGIN	
	ALTER TABLE [dbo].[OperacaoFundoAux] ADD FieTabelaIr int NULL;
END
GO

if exists(select 1 from syscolumns where id = object_id('ColagemResgateMapaFundo') and name = 'Participacao')
BEGIN	
	ALTER TABLE [dbo].[ColagemResgateMapaFundo] DROP COLUMN Participacao;
END
GO

if NOT exists(select 1 from syscolumns where id = object_id('ColagemResgateMapaFundo') and name = 'IdColagemResgateDetalhe')
BEGIN	
	ALTER TABLE [dbo].[ColagemResgateMapaFundo] ADD IdColagemResgateDetalhe int NULL;
END
GO

if exists(select 1 from syscolumns where id = object_id('ColagemResgate') and name = 'DataOperacao')
BEGIN	
	ALTER TABLE [dbo].[ColagemResgate] DROP COLUMN DataOperacao;
END
GO

if exists(select 1 from syscolumns where id = object_id('ColagemResgateDetalhe') and name = 'DataOperacao')
BEGIN	
	ALTER TABLE [dbo].[ColagemResgateDetalhe] DROP COLUMN DataOperacao;
END
GO

if exists(select 1 from syscolumns where id = object_id('ColagemComeCotas') and name = 'DataOperacao')
BEGIN	
	ALTER TABLE [dbo].[ColagemComeCotas] DROP COLUMN DataOperacao;
END
GO

if exists(select 1 from syscolumns where id = object_id('ColagemComeCotasDetalhe') and name = 'DataOperacao')
BEGIN	
	ALTER TABLE [dbo].[ColagemComeCotasDetalhe] DROP COLUMN DataOperacao;
END
GO


if exists(select 1 from syscolumns where id = object_id('ColagemResgateMapaCotista') and name = 'Participacao')
BEGIN	
	ALTER TABLE [dbo].[ColagemResgateMapaCotista] DROP COLUMN Participacao;
END
GO

if NOT exists(select 1 from syscolumns where id = object_id('ColagemResgateMapaCotista') and name = 'IdColagemResgateDetalhe')
BEGIN	
	ALTER TABLE [dbo].[ColagemResgateMapaCotista] ADD IdColagemResgateDetalhe int NULL;
END
GO


IF NOT EXISTS(SELECT 1 FROM SYSCOLUMNS WHERE ID=OBJECT_ID('OperacaoCotistaAux') AND NAME = 'DepositoRetirada')
BEGIN
	ALTER TABLE OperacaoCotistaAux add DepositoRetirada bit not null default 0;
END
GO

if not exists(select 1 from syscolumns where id = object_id('OperacaoCotistaAux') and name = 'FieModalidade')
BEGIN	
	ALTER TABLE [dbo].[OperacaoCotistaAux] ADD FieModalidade int NULL;
END
GO	

if not exists(select 1 from syscolumns where id = object_id('OperacaoCotistaAux') and name = 'FieTabelaIr')
BEGIN	
	ALTER TABLE [dbo].[OperacaoCotistaAux] ADD FieTabelaIr int NULL;
END
GO

if not exists(select 1 from syscolumns where id = object_id('OperacaoCotistaAux') and name = 'ValorDespesas')
BEGIN
	ALTER TABLE OperacaoCotistaAux ADD ValorDespesas decimal(16,2) NULL
END
GO	

if not exists(select 1 from syscolumns where id = object_id('OperacaoCotistaAux') and name = 'ValorTaxas')
BEGIN
	ALTER TABLE OperacaoCotistaAux ADD ValorTaxas decimal(16,2) NULL
END
GO	

if not exists(select 1 from syscolumns where id = object_id('OperacaoCotistaAux') and name = 'ValorTributos')
BEGIN
	ALTER TABLE OperacaoCotistaAux ADD ValorTributos decimal(16,2) NULL
END
GO	

if not exists(select 1 from syscolumns where id = object_id('OperacaoCotistaAux') and name = 'PenalidadeLockUp')
BEGIN
	ALTER TABLE OperacaoCotistaAux ADD PenalidadeLockUp decimal(16,2) NULL
END
GO	

if not exists(select 1 from syscolumns where id = object_id('OperacaoCotistaAux') and name = 'ValorHoldBack')
BEGIN
	ALTER TABLE OperacaoCotistaAux ADD ValorHoldBack decimal(16,2) NULL
END
GO	

if not exists(select 1 from syscolumns where id = object_id('OperacaoCotistaAux') and name = 'IdTransferenciaSerie')
BEGIN
	ALTER TABLE dbo.OperacaoCotistaAux ADD IdTransferenciaSerie int NULL;	
END
GO	

if (select count(*) from SYSCOLUMNS WHERE ID = (SELECT ID FROM SYSOBJECTS WHERE  NAME = 'OperacaoCotistaAux' ) and name = 'IdOrdem') = 0
begin
	alter table OperacaoCotistaAux add IdOrdem int null
END	
GO

if not exists(select 1 from syscolumns where id = object_id('OperacaoCotistaAux') and name = 'IdContaCorrente')
BEGIN
 ALTER TABLE OperacaoCotistaAux ADD IdContaCorrente int NULL;
END
GO 

IF NOT EXISTS(SELECT 1 FROM SYSCOLUMNS WHERE ID= OBJECT_ID('OperacaoCotistaAux') AND NAME = 'DataRegistro')
BEGIN
	ALTER TABLE OperacaoCotistaAux add DataRegistro Datetime;

	EXEC sp_executesql
            N'update OperacaoCotistaAux set DataRegistro = DataOperacao'

	alter table OperacaoCotistaAux alter column DataRegistro datetime not null;
END
GO

IF NOT EXISTS(SELECT 1 FROM SYSCOLUMNS WHERE ID= OBJECT_ID('OperacaoCotista') AND NAME = 'DataAplicCautelaResgatada')
BEGIN
	ALTER TABLE OperacaoCotista add DataAplicCautelaResgatada Datetime;
END
GO

IF NOT EXISTS(SELECT 1 FROM SYSCOLUMNS WHERE ID= OBJECT_ID('OperacaoCotistaAux') AND NAME = 'DataAplicCautelaResgatada')
BEGIN
	ALTER TABLE OperacaoCotistaAux add DataAplicCautelaResgatada Datetime;
END
GO

IF NOT EXISTS(SELECT 1 FROM SYSCOLUMNS WHERE ID= OBJECT_ID('OperacaoFundo') AND NAME = 'DataAplicCautelaResgatada')
BEGIN
	ALTER TABLE OperacaoFundo add DataAplicCautelaResgatada Datetime;
END
GO

IF NOT EXISTS(SELECT 1 FROM SYSCOLUMNS WHERE ID= OBJECT_ID('OperacaoFundoAux') AND NAME = 'DataAplicCautelaResgatada')
BEGIN
	ALTER TABLE OperacaoFundoAux add DataAplicCautelaResgatada Datetime;
END
GO

if  exists(select 1 from syscolumns where id = object_id('Rentabilidade') and name = 'CotaAtivoIncentivado')
BEGIN	
	ALTER TABLE Rentabilidade DROP COLUMN CotaAtivoIncentivado;
END
GO	

if  exists(select 1 from syscolumns where id = object_id('Rentabilidade') and name = 'CotaDemaisAtivos')
BEGIN	
	ALTER TABLE Rentabilidade DROP COLUMN CotaDemaisAtivos;
END
GO			

if  exists(select 1 from syscolumns where id = object_id('Rentabilidade') and name = 'ValorBrutoGrossUpMoedaPortfolio')
BEGIN	
	ALTER TABLE Rentabilidade DROP COLUMN ValorBrutoGrossUpMoedaPortfolio;
END
GO		

if  exists(select 1 from syscolumns where id = object_id('Rentabilidade') and name = 'ValorBrutoGrossUpMoedaAtivo')
BEGIN	
	ALTER TABLE Rentabilidade DROP COLUMN ValorBrutoGrossUpMoedaAtivo;
END
GO		

if  exists(select 1 from syscolumns where id = object_id('Rentabilidade') and name = 'RentabilidadeGrossUpMoedaPortfolio')
BEGIN	
	ALTER TABLE Rentabilidade DROP COLUMN RentabilidadeGrossUpMoedaPortfolio;
END
GO		

if exists(select 1 from syscolumns where id = object_id('Rentabilidade') and name = 'RentabilidadeGrossUpMoedaAtivo')
BEGIN	
	ALTER TABLE Rentabilidade DROP COLUMN RentabilidadeGrossUpMoedaAtivo;
END
GO		

if exists(select 1 from syscolumns where id = object_id('Rentabilidade') and name = 'RentabilidadeSemGrossUpMoedaPortfolio')
BEGIN	
	ALTER TABLE Rentabilidade DROP COLUMN RentabilidadeSemGrossUpMoedaPortfolio;
END
GO		

if exists(select 1 from syscolumns where id = object_id('Rentabilidade') and name = 'RentabilidadeSemGrossUpMoedaAtivo')
BEGIN	
	ALTER TABLE Rentabilidade DROP COLUMN RentabilidadeSemGrossUpMoedaAtivo;
END
GO		

if not exists(select 1 from syscolumns where id = object_id('Rentabilidade') and name = 'PatrimonioInicialBrutoMoedaPortfolio')
BEGIN	
	ALTER TABLE Rentabilidade ADD PatrimonioInicialBrutoMoedaPortfolio Decimal(28,2) NULL;
END
GO	

if not exists(select 1 from syscolumns where id = object_id('Rentabilidade') and name = 'PatrimonioInicialBrutoMoedaAtivo')
BEGIN	
	ALTER TABLE Rentabilidade ADD PatrimonioInicialBrutoMoedaAtivo Decimal(28,2) NULL;
END
GO	

if not exists(select 1 from syscolumns where id = object_id('Rentabilidade') and name = 'PatrimonioFinalBrutoMoedaPortfolio')
BEGIN	
	ALTER TABLE Rentabilidade ADD PatrimonioFinalBrutoMoedaPortfolio Decimal(28,2) NULL;
END
GO	

if not exists(select 1 from syscolumns where id = object_id('Rentabilidade') and name = 'PatrimonioFinalBrutoMoedaAtivo')
BEGIN	
	ALTER TABLE Rentabilidade ADD PatrimonioFinalBrutoMoedaAtivo Decimal(28,2) NULL;
END
GO	


if not exists(select 1 from syscolumns where id = object_id('Rentabilidade') and name = 'CotaGerencial')
BEGIN	
	ALTER TABLE Rentabilidade ADD CotaGerencial Decimal(28,12) NULL;
END
GO	

if not exists(select 1 from syscolumns where id = object_id('Rentabilidade') and name = 'RentabilidadeBrutaDiariaMoedaAtivo')
BEGIN	
	ALTER TABLE Rentabilidade ADD RentabilidadeBrutaDiariaMoedaAtivo Decimal(25,12) NULL ;
END
GO	

if not exists(select 1 from syscolumns where id = object_id('Rentabilidade') and name = 'RentabilidadeBrutaAcumMoedaAtivo')
BEGIN	
	ALTER TABLE Rentabilidade ADD RentabilidadeBrutaAcumMoedaAtivo Decimal(25,12) NULL ;
END
GO	

if not exists(select 1 from syscolumns where id = object_id('Rentabilidade') and name = 'RentabilidadeBrutaDiariaMoedaPortfolio')
BEGIN	
	ALTER TABLE Rentabilidade ADD RentabilidadeBrutaDiariaMoedaPortfolio Decimal(25,12) NULL ;
END
GO	

if not exists(select 1 from syscolumns where id = object_id('Rentabilidade') and name = 'RentabilidadeBrutaAcumMoedaPortfolio')
BEGIN	
	ALTER TABLE Rentabilidade ADD RentabilidadeBrutaAcumMoedaPortfolio Decimal(25,12) NULL ;
END
GO	

if not exists(select 1 from syscolumns where id = object_id('Rentabilidade') and name = 'PatrimonioInicialGrossUpMoedaAtivo')
BEGIN	
	ALTER TABLE Rentabilidade ADD PatrimonioInicialGrossUpMoedaAtivo Decimal(28,2) NULL;
END
GO	

if not exists(select 1 from syscolumns where id = object_id('Rentabilidade') and name = 'PatrimonioInicialGrossUpMoedaPortfolio')
BEGIN	
	ALTER TABLE Rentabilidade ADD PatrimonioInicialGrossUpMoedaPortfolio Decimal(28,2) NULL;
END
GO	

if not exists(select 1 from syscolumns where id = object_id('Rentabilidade') and name = 'PatrimonioFinalGrossUpMoedaAtivo')
BEGIN	
	ALTER TABLE Rentabilidade ADD PatrimonioFinalGrossUpMoedaAtivo Decimal(28,2) NULL;
END
GO	

if not exists(select 1 from syscolumns where id = object_id('Rentabilidade') and name = 'PatrimonioFinalGrossUpMoedaPortfolio')
BEGIN	
	ALTER TABLE Rentabilidade ADD PatrimonioFinalGrossUpMoedaPortfolio Decimal(28,2) NULL;
END
GO	

if not exists(select 1 from syscolumns where id = object_id('Rentabilidade') and name = 'RentabilidadeAtivosIcentivados')
BEGIN	
	ALTER TABLE Rentabilidade ADD RentabilidadeAtivosIcentivados Decimal(25,12) NULL ;
END
GO	

BEGIN TRANSACTION
set xact_abort on
delete CadastroComplementar
where
	IdCadastroComplementares not in(
		select  	
			(
				select top(1) 
					IdCadastroComplementares 
				from 
					CadastroComplementar ccInt
				where
					cc.IdCamposComplementares = ccInt.IdCamposComplementares and
					cc.IdMercadoTipoPessoa = ccInt.IdMercadoTipoPessoa
				order by IdCadastroComplementares desc		
			) as IdCadastroComplementares
		from 
			CadastroComplementar cc
		group by 
			IdMercadoTipoPessoa, 
			IdCamposComplementares		
	)

ALTER TABLE CadastroComplementar
ADD UNIQUE (IdMercadoTipoPessoa, IdCamposComplementares)

IF NOT EXISTS(SELECT 1 FROM SYSCOLUMNS WHERE ID= OBJECT_ID('EventoFundoCautela') AND NAME = 'Processar')
BEGIN
	ALTER TABLE EventoFundoCautela add Processar tinyint default 0;
END

IF NOT EXISTS(SELECT 1 FROM SYSCOLUMNS WHERE ID= OBJECT_ID('EventoFundoBMF') AND NAME = 'Processar')
BEGIN
	ALTER TABLE EventoFundoBMF add Processar tinyint default 0;
END

IF NOT EXISTS(SELECT 1 FROM SYSCOLUMNS WHERE ID= OBJECT_ID('EventoFundoBolsa') AND NAME = 'Processar')
BEGIN
	ALTER TABLE EventoFundoBolsa add Processar tinyint default 0;
END

IF NOT EXISTS(SELECT 1 FROM SYSCOLUMNS WHERE ID= OBJECT_ID('EventoFundoRendaFixa') AND NAME = 'Processar')
BEGIN
	ALTER TABLE EventoFundoRendaFixa add Processar tinyint default 0;
END

IF NOT EXISTS(SELECT 1 FROM SYSCOLUMNS WHERE ID= OBJECT_ID('EventoFundoSwap') AND NAME = 'Processar')
BEGIN
	ALTER TABLE EventoFundoSwap add Processar tinyint default 0;
END

IF NOT EXISTS(SELECT 1 FROM SYSCOLUMNS WHERE ID= OBJECT_ID('EventoFundoFundo') AND NAME = 'Processar')
BEGIN
	ALTER TABLE EventoFundoFundo add Processar tinyint default 0;
END

IF NOT EXISTS(SELECT 1 FROM SYSCOLUMNS WHERE ID= OBJECT_ID('EventoFundoLiquidacao') AND NAME = 'Processar')
BEGIN
	ALTER TABLE EventoFundoLiquidacao add Processar tinyint default 0;
END

IF NOT EXISTS(SELECT 1 FROM SYSCOLUMNS WHERE ID= OBJECT_ID('EventoFundoCaixa') AND NAME = 'Processar')
BEGIN
	ALTER TABLE EventoFundoCaixa add Processar tinyint default 0;
END

if not exists(select 1 from syscolumns where id = object_id('Rentabilidade') and name = 'RentabilidadeAtivosTributados')
BEGIN	
	ALTER TABLE Rentabilidade ADD RentabilidadeAtivosTributados Decimal(25,12) NULL ;
END

if not exists(select 1 from syscolumns where id = object_id('Rentabilidade') and name = 'RentabilidadeGrossUpDiariaMoedaAtivo')
BEGIN	
	ALTER TABLE Rentabilidade ADD RentabilidadeGrossUpDiariaMoedaAtivo Decimal(25,12) NULL ;
END

if not exists(select 1 from syscolumns where id = object_id('Rentabilidade') and name = 'RentabilidadeGrossUpAcumMoedaAtivo')
BEGIN	
	ALTER TABLE Rentabilidade ADD RentabilidadeGrossUpAcumMoedaAtivo Decimal(25,12) NULL ;
END

if not exists(select 1 from syscolumns where id = object_id('Rentabilidade') and name = 'RentabilidadeGrossUpDiariaMoedaPortfolio')
BEGIN	
	ALTER TABLE Rentabilidade ADD RentabilidadeGrossUpDiariaMoedaPortfolio Decimal(25,12) NULL ;
END

if not exists(select 1 from syscolumns where id = object_id('Rentabilidade') and name = 'RentabilidadeGrossUpAcumMoedaPortfolio')
BEGIN	
	ALTER TABLE Rentabilidade ADD RentabilidadeGrossUpAcumMoedaPortfolio Decimal(25,12) NULL ;
END

IF NOT EXISTS(SELECT 1 FROM SYSCOLUMNS WHERE ID= OBJECT_ID('OperacaoFundo') AND NAME = 'UserTimeStamp')
BEGIN
	ALTER TABLE OperacaoFundo add UserTimeStamp datetime default CURRENT_TIMESTAMP
	ALTER TABLE OperacaoFundoAux add UserTimeStamp datetime;
END
	
IF NOT EXISTS(SELECT 1 FROM SYSCOLUMNS WHERE ID= OBJECT_ID('OperacaoCotista') AND NAME = 'UserTimeStamp')
BEGIN
	ALTER TABLE OperacaoCotista add UserTimeStamp datetime default CURRENT_TIMESTAMP
	ALTER TABLE OperacaoCotistaAux add UserTimeStamp datetime;
END

IF NOT EXISTS(SELECT 1 FROM SYSCOLUMNS WHERE ID= OBJECT_ID('OperacaoBMF') AND NAME = 'UserTimeStamp')
BEGIN
	ALTER TABLE OperacaoBMF add UserTimeStamp datetime default CURRENT_TIMESTAMP
END

IF NOT EXISTS(SELECT 1 FROM SYSCOLUMNS WHERE ID= OBJECT_ID('OperacaoBOlsa') AND NAME = 'UserTimeStamp')
BEGIN
	ALTER TABLE OperacaoBOlsa add UserTimeStamp datetime default CURRENT_TIMESTAMP
END

IF NOT EXISTS(SELECT 1 FROM SYSCOLUMNS WHERE ID= OBJECT_ID('OperacaoCambio') AND NAME = 'UserTimeStamp')
BEGIN
	ALTER TABLE OperacaoCambio add UserTimeStamp datetime default CURRENT_TIMESTAMP
END

IF NOT EXISTS(SELECT 1 FROM SYSCOLUMNS WHERE ID= OBJECT_ID('OperacaoEmprestimoBolsa') AND NAME = 'UserTimeStamp')
BEGIN
	ALTER TABLE OperacaoEmprestimoBolsa add UserTimeStamp datetime default CURRENT_TIMESTAMP
END

IF NOT EXISTS(SELECT 1 FROM SYSCOLUMNS WHERE ID= OBJECT_ID('OperacaoSwap') AND NAME = 'UserTimeStamp')
BEGIN
	ALTER TABLE OperacaoSwap add UserTimeStamp datetime default CURRENT_TIMESTAMP
END

IF NOT EXISTS(SELECT 1 FROM SYSCOLUMNS WHERE ID= OBJECT_ID('OperacaoTermoBolsa') AND NAME = 'UserTimeStamp')
BEGIN
	ALTER TABLE OperacaoTermoBolsa add UserTimeStamp datetime default CURRENT_TIMESTAMP
END

IF NOT EXISTS(SELECT 1 FROM SYSCOLUMNS WHERE ID= OBJECT_ID('OrdemBMF') AND NAME = 'UserTimeStamp')
BEGIN
	ALTER TABLE OrdemBMF add UserTimeStamp datetime default CURRENT_TIMESTAMP
END

IF NOT EXISTS(SELECT 1 FROM SYSCOLUMNS WHERE ID= OBJECT_ID('OrdemBolsa') AND NAME = 'UserTimeStamp')
BEGIN
	ALTER TABLE OrdemBolsa add UserTimeStamp datetime default CURRENT_TIMESTAMP
END

IF NOT EXISTS(SELECT 1 FROM SYSCOLUMNS WHERE ID= OBJECT_ID('OrdemCotista') AND NAME = 'UserTimeStamp')
BEGIN
	ALTER TABLE OrdemCotista add UserTimeStamp datetime default CURRENT_TIMESTAMP
END

IF NOT EXISTS(SELECT 1 FROM SYSCOLUMNS WHERE ID= OBJECT_ID('OrdemFundo') AND NAME = 'UserTimeStamp')
BEGIN
	ALTER TABLE OrdemFundo add UserTimeStamp datetime default CURRENT_TIMESTAMP
END

IF NOT EXISTS(SELECT 1 FROM SYSCOLUMNS WHERE ID= OBJECT_ID('OrdemTermoBolsa') AND NAME = 'UserTimeStamp')
BEGIN
	ALTER TABLE OrdemTermoBolsa add UserTimeStamp datetime default CURRENT_TIMESTAMP
END

IF NOT EXISTS(SELECT 1 FROM SYSCOLUMNS WHERE ID= OBJECT_ID('AgendaFundo') AND NAME = 'ImpactarCota')
BEGIN
	ALTER TABLE AgendaFundo add ImpactarCota char(1) not null default 'S'
END

COMMIT TRANSACTION

IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'GrupoPerfilMTM') = 0
Begin
	CREATE TABLE GrupoPerfilMTM 
	( 	IdGrupoPerfilMTM 				INT 	PRIMARY KEY identity	NOT NULL, 
		Descricao						varchar(100)					NOT NULL
	) 	
end
go

IF NOT EXISTS(SELECT 1 FROM SYSCOLUMNS WHERE ID= OBJECT_ID('Carteira') AND NAME = 'IdGrupoPerfilMTM')
BEGIN
	ALTER TABLE Carteira add IdGrupoPerfilMTM INT null ;
	alter table Carteira add constraint Carteira_GrupoPerfil_FK FOREIGN KEY ( IdGrupoPerfilMTM ) references GrupoPerfilMTM(IdGrupoPerfilMTM)
END
GO

IF NOT EXISTS(SELECT 1 FROM SYSCOLUMNS WHERE ID= OBJECT_ID('PerfilMTM') AND NAME = 'IdGrupoPerfilMTM')
BEGIN
	ALTER TABLE PerfilMTM add IdGrupoPerfilMTM INT null ;
	alter table PerfilMTM add constraint PerfilMTM_GrupoPerfil_FK FOREIGN KEY ( IdGrupoPerfilMTM ) references GrupoPerfilMTM(IdGrupoPerfilMTM)
END
GO

IF NOT EXISTS(SELECT 1 FROM SYSCOLUMNS WHERE ID= OBJECT_ID('MTMManual') AND NAME = 'IdMTMManual')
BEGIN
	--Cria tabela auxiliar
	CREATE TABLE MTMManualAux 
	( 	
		IdOperacao						INT 				NOT NULL,
		DtVigencia						DateTime 			NOT NULL, 
		Taxa252 						Decimal(25,12)		NULL, 
		PuMTM							Decimal(25,12)		NULL	
	)
	
	--Transfere informações
	INSERT INTO MTMManualAux SELECT * FROM MTMManual;
	
	--Deleta da tabela de origem
	DROP TABLE MTMManual;
	
	--Cria nova estrutura da tabela
	CREATE TABLE MTMManual 
	( 	
	    IdMTMManual 					INT PRIMARY KEY identity NOT NULL,
		IdOperacao						INT 				NULL,
		DtVigencia						DateTime 			NOT NULL, 
		Taxa252 						Decimal(25,12)		NULL, 
		PuMTM							Decimal(25,12)		NULL	
	) 
	
	--Transfere informações para a nova tabela
	INSERT INTO MTMManual SELECT * FROM MTMManualAux;
	
	--drop Tabela Aux
	DROP TABLE MTMManualAux;
END
GO

IF NOT EXISTS(SELECT 1 FROM SYSCOLUMNS WHERE ID= OBJECT_ID('MTMManual') AND NAME = 'IdTitulo')
BEGIN
	ALTER TABLE MTMManual add IdTitulo INT null;
END
GO

IF NOT EXISTS(SELECT 1 FROM SYSCOLUMNS WHERE ID= OBJECT_ID('MTMManual') AND NAME = 'IdCliente')
BEGIN
	ALTER TABLE MTMManual add IdCliente INT null ;
END
GO

IF NOT EXISTS (SELECT name FROM sysindexes WHERE name = 'PosicaoRendaFixaHistorico_Idx')
BEGIN
	CREATE INDEX PosicaoRendaFixaHistorico_Idx ON PosicaoRendaFixaHistorico (DataHistorico, IdCliente);
END
GO	

IF NOT EXISTS (SELECT name FROM sysindexes WHERE name = 'PosicaoRendaFixaAbertura_Idx')
BEGIN
	CREATE INDEX PosicaoRendaFixaAbertura_Idx ON PosicaoRendaFixaAbertura (DataHistorico, IdCliente);
END
GO		

IF NOT EXISTS (SELECT name FROM sysindexes WHERE name = 'PosicaoBolsaHistorico_Idx')
BEGIN
	CREATE INDEX PosicaoBolsaHistorico_Idx ON PosicaoBolsaHistorico (DataHistorico, IdCliente);
END
GO	

IF NOT EXISTS (SELECT name FROM sysindexes WHERE name = 'PosicaoBolsaAbertura_Idx')
BEGIN
	CREATE INDEX PosicaoBolsaAbertura_Idx ON PosicaoBolsaAbertura (DataHistorico, IdCliente);
END
GO		

IF NOT EXISTS (SELECT name FROM sysindexes WHERE name = 'PosicaoBMFHistorico_Idx')
BEGIN
	CREATE INDEX PosicaoBMFHistorico_Idx ON PosicaoBMFHistorico (DataHistorico, IdCliente);
END
GO	

IF NOT EXISTS (SELECT name FROM sysindexes WHERE name = 'PosicaoBMFAbertura_Idx')
BEGIN
	CREATE INDEX PosicaoBMFAbertura_Idx ON PosicaoBMFAbertura (DataHistorico, IdCliente);
END
GO		

IF NOT EXISTS (SELECT name FROM sysindexes WHERE name = 'PosicaoSwapHistorico_Idx')
BEGIN
	CREATE INDEX PosicaoSwapHistorico_Idx ON PosicaoSwapHistorico (DataHistorico, IdCliente);
END
GO	

IF NOT EXISTS (SELECT name FROM sysindexes WHERE name = 'PosicaoSwapAbertura_Idx')
BEGIN
	CREATE INDEX PosicaoSwapAbertura_Idx ON PosicaoSwapAbertura (DataHistorico, IdCliente);
END
GO		

IF NOT EXISTS (SELECT name FROM sysindexes WHERE name = 'PosicaoEmprestimoBolsaHistorico_Idx')
BEGIN
	CREATE INDEX PosicaoEmprestimoBolsaHistorico_Idx ON PosicaoEmprestimoBolsaHistorico (DataHistorico, IdCliente);
END
GO	

IF NOT EXISTS (SELECT name FROM sysindexes WHERE name = 'PosicaoEmprestimoBolsaAbertura_Idx')
BEGIN
	CREATE INDEX PosicaoEmprestimoBolsaAbertura_Idx ON PosicaoEmprestimoBolsaAbertura (DataHistorico, IdCliente);
END
GO		

IF NOT EXISTS (SELECT name FROM sysindexes WHERE name = 'PosicaoTermoBolsaHistorico_Idx')
BEGIN
	CREATE INDEX PosicaoTermoBolsaHistorico_Idx ON PosicaoTermoBolsaHistorico (DataHistorico, IdCliente);
END
GO	

IF NOT EXISTS (SELECT name FROM sysindexes WHERE name = 'PosicaoTermoBolsaAbertura_Idx')
BEGIN
	CREATE INDEX PosicaoTermoBolsaAbertura_Idx ON PosicaoTermoBolsaAbertura (DataHistorico, IdCliente);
END
GO		

IF NOT EXISTS (SELECT name FROM sysindexes WHERE name = 'Rentabilidade_Idx')
BEGIN
	CREATE INDEX Rentabilidade_Idx ON Rentabilidade (IdCliente, data);
END
GO	

IF NOT EXISTS(SELECT 1 FROM SYSCOLUMNS WHERE ID= OBJECT_ID('OperacaoSwap') AND NAME = 'CdAtivoBolsa')
BEGIN
	ALTER TABLE OperacaoSwap add CdAtivoBolsa varchar(25) null ;
	alter table OperacaoSwap add constraint OperacaoSwap_AtivoBolsa_FK FOREIGN KEY ( CdAtivoBolsa ) references AtivoBolsa(CdAtivoBolsa)
END
GO


IF NOT EXISTS(SELECT 1 FROM SYSCOLUMNS WHERE ID= OBJECT_ID('OperacaoSwap') AND NAME = 'CdAtivoBolsaContraParte')
BEGIN
	ALTER TABLE OperacaoSwap add CdAtivoBolsaContraParte varchar(25) null ;
	alter table OperacaoSwap add constraint OperacaoSwap_AtivoBolsaCP_FK FOREIGN KEY ( CdAtivoBolsaContraParte ) references AtivoBolsa(CdAtivoBolsa)
END
GO


IF NOT EXISTS(SELECT 1 FROM SYSCOLUMNS WHERE ID= OBJECT_ID('OperacaoSwap') AND NAME = 'IdAtivoCarteira')
BEGIN
	ALTER TABLE OperacaoSwap add IdAtivoCarteira int null ;
	alter table OperacaoSwap add constraint OperacaoSwap_Carteira_FK FOREIGN KEY ( IdAtivoCarteira ) references Carteira(IdCarteira)
END
GO


IF NOT EXISTS(SELECT 1 FROM SYSCOLUMNS WHERE ID= OBJECT_ID('OperacaoSwap') AND NAME = 'IdAtivoCarteiraContraParte')
BEGIN
	ALTER TABLE OperacaoSwap add IdAtivoCarteiraContraParte int null ;
	alter table OperacaoSwap add constraint OperacaoSwap_CarteiraCP_FK FOREIGN KEY ( IdAtivoCarteiraContraParte ) references Carteira(IdCarteira)
END
GO

IF EXISTS(select 1 from syscolumns where id = object_id('OperacaoSwap') and name = 'TaxaJuros')
BEGIN
	ALTER TABLE OperacaoSwap ALTER COLUMN TaxaJuros decimal(10,4) null;
END
GO
	
IF EXISTS(select 1 from syscolumns where id = object_id('OperacaoSwap') and name = 'Percentual')		
BEGIN
	ALTER TABLE OperacaoSwap ALTER COLUMN Percentual decimal(10,4) null;
END
GO
	
IF EXISTS(select 1 from syscolumns where id = object_id('OperacaoSwap') and name = 'TaxaJurosContraParte')	
BEGIN
	ALTER TABLE OperacaoSwap ALTER COLUMN TaxaJurosContraParte decimal(10,4) null;
END
GO

IF EXISTS(select 1 from syscolumns where id = object_id('OperacaoSwap') and name = 'PercentualContraParte')	
BEGIN
	ALTER TABLE OperacaoSwap ALTER COLUMN PercentualContraParte decimal(10,4) null;
END
GO

IF NOT EXISTS(select 1 from syscolumns where id = object_id('OperacaoSwap') and name = 'RealizaReset')
BEGIN
	ALTER TABLE dbo.OperacaoSwap ADD RealizaReset CHAR(1) not null default ('N')
END
GO

IF NOT EXISTS(select 1 from syscolumns where id = object_id('OperacaoSwap') and name = 'PeriodicidadeReset')
BEGIN
	ALTER TABLE dbo.OperacaoSwap ADD PeriodicidadeReset int
END
GO

IF NOT EXISTS(select 1 from syscolumns where id = object_id('OperacaoSwap') and name = 'QtdePeriodicidade')
BEGIN
	ALTER TABLE dbo.OperacaoSwap ADD QtdePeriodicidade int
END
GO

IF NOT EXISTS(SELECT 1 FROM SYSCOLUMNS WHERE ID= OBJECT_ID('OperacaoSwap') AND NAME = 'DataInicioReset')
BEGIN
	ALTER TABLE OperacaoSwap add DataInicioReset Datetime;
END
GO

IF NOT EXISTS(SELECT 1 FROM SYSCOLUMNS WHERE ID= OBJECT_ID('PosicaoSwap') AND NAME = 'CdAtivoBolsa')
BEGIN
	ALTER TABLE PosicaoSwap add CdAtivoBolsa varchar(25) null ;
	alter table PosicaoSwap add constraint PosicaoSwap_AtivoBolsa_FK FOREIGN KEY ( CdAtivoBolsa ) references AtivoBolsa(CdAtivoBolsa)
END
GO


IF NOT EXISTS(SELECT 1 FROM SYSCOLUMNS WHERE ID= OBJECT_ID('PosicaoSwap') AND NAME = 'CdAtivoBolsaContraParte')
BEGIN
	ALTER TABLE PosicaoSwap add CdAtivoBolsaContraParte varchar(25) null ;
	alter table PosicaoSwap add constraint PosicaoSwap_AtivoBolsaCP_FK FOREIGN KEY ( CdAtivoBolsaContraParte ) references AtivoBolsa(CdAtivoBolsa)
END
GO


IF NOT EXISTS(SELECT 1 FROM SYSCOLUMNS WHERE ID= OBJECT_ID('PosicaoSwap') AND NAME = 'IdAtivoCarteira')
BEGIN
	ALTER TABLE PosicaoSwap add IdAtivoCarteira int null ;
	alter table PosicaoSwap add constraint PosicaoSwap_Carteira_FK FOREIGN KEY ( IdAtivoCarteira ) references Carteira(IdCarteira)
END
GO


IF NOT EXISTS(SELECT 1 FROM SYSCOLUMNS WHERE ID= OBJECT_ID('PosicaoSwap') AND NAME = 'IdAtivoCarteiraContraParte')
BEGIN
	ALTER TABLE PosicaoSwap add IdAtivoCarteiraContraParte int null ;
	alter table PosicaoSwap add constraint PosicaoSwap_CarteiraCP_FK FOREIGN KEY ( IdAtivoCarteiraContraParte ) references Carteira(IdCarteira)
END
GO

IF NOT EXISTS(SELECT 1 FROM SYSCOLUMNS WHERE ID= OBJECT_ID('PosicaoSwapAbertura') AND NAME = 'CdAtivoBolsa')
BEGIN
	ALTER TABLE PosicaoSwapAbertura add CdAtivoBolsa varchar(25) null ;
	alter table PosicaoSwapAbertura add constraint PosicaoSwapAbertura_AtivoBolsa_FK FOREIGN KEY ( CdAtivoBolsa ) references AtivoBolsa(CdAtivoBolsa)
END
GO


IF NOT EXISTS(SELECT 1 FROM SYSCOLUMNS WHERE ID= OBJECT_ID('PosicaoSwapAbertura') AND NAME = 'CdAtivoBolsaContraParte')
BEGIN
	ALTER TABLE PosicaoSwapAbertura add CdAtivoBolsaContraParte varchar(25) null ;
	alter table PosicaoSwapAbertura add constraint PosicaoSwapAber_AtivoBolsaCP_FK FOREIGN KEY ( CdAtivoBolsaContraParte ) references AtivoBolsa(CdAtivoBolsa)
END
GO


IF NOT EXISTS(SELECT 1 FROM SYSCOLUMNS WHERE ID= OBJECT_ID('PosicaoSwapAbertura') AND NAME = 'IdAtivoCarteira')
BEGIN
	ALTER TABLE PosicaoSwapAbertura add IdAtivoCarteira int null ;
	alter table PosicaoSwapAbertura add constraint PosicaoSwapAber_Carteira_FK FOREIGN KEY ( IdAtivoCarteira ) references Carteira(IdCarteira)
END
GO


IF NOT EXISTS(SELECT 1 FROM SYSCOLUMNS WHERE ID= OBJECT_ID('PosicaoSwapAbertura') AND NAME = 'IdAtivoCarteiraContraParte')
BEGIN
	ALTER TABLE PosicaoSwapAbertura add IdAtivoCarteiraContraParte int null ;
	alter table PosicaoSwapAbertura add constraint PosicaoSwapAber_CarteiraCP_FK FOREIGN KEY ( IdAtivoCarteiraContraParte ) references Carteira(IdCarteira)
END
GO

IF NOT EXISTS(SELECT 1 FROM SYSCOLUMNS WHERE ID= OBJECT_ID('PosicaoSwapHistorico') AND NAME = 'CdAtivoBolsa')
BEGIN
	ALTER TABLE PosicaoSwapHistorico add CdAtivoBolsa varchar(25) null ;
	alter table PosicaoSwapHistorico add constraint PosicaoSwapHist_AtivoBolsa_FK FOREIGN KEY ( CdAtivoBolsa ) references AtivoBolsa(CdAtivoBolsa)
END
GO


IF NOT EXISTS(SELECT 1 FROM SYSCOLUMNS WHERE ID= OBJECT_ID('PosicaoSwapHistorico') AND NAME = 'CdAtivoBolsaContraParte')
BEGIN
	ALTER TABLE PosicaoSwapHistorico add CdAtivoBolsaContraParte varchar(25) null ;
	alter table PosicaoSwapHistorico add constraint PosicaoSwapHist_AtivoBolsaCP_FK FOREIGN KEY ( CdAtivoBolsaContraParte ) references AtivoBolsa(CdAtivoBolsa)
END
GO


IF NOT EXISTS(SELECT 1 FROM SYSCOLUMNS WHERE ID= OBJECT_ID('PosicaoSwapHistorico') AND NAME = 'IdAtivoCarteira')
BEGIN
	ALTER TABLE PosicaoSwapHistorico add IdAtivoCarteira int null ;
	alter table PosicaoSwapHistorico add constraint PosicaoSwapHist_Carteira_FK FOREIGN KEY ( IdAtivoCarteira ) references Carteira(IdCarteira)
END
GO


IF NOT EXISTS(SELECT 1 FROM SYSCOLUMNS WHERE ID= OBJECT_ID('PosicaoSwapHistorico') AND NAME = 'IdAtivoCarteiraContraParte')
BEGIN
	ALTER TABLE PosicaoSwapHistorico add IdAtivoCarteiraContraParte int null ;
	alter table PosicaoSwapHistorico add constraint PosicaoSwapHist_CarteiraCP_FK FOREIGN KEY ( IdAtivoCarteiraContraParte ) references Carteira(IdCarteira)
END
GO

IF EXISTS(select 1 from syscolumns where id = object_id('OperacaoSwap') and name = 'TaxaJuros')
BEGIN
	ALTER TABLE OperacaoSwap ALTER COLUMN TaxaJuros decimal(10,4) null;
	ALTER TABLE PosicaoSwap ALTER COLUMN TaxaJuros decimal(10,4) null;
	ALTER TABLE PosicaoSwapAbertura ALTER COLUMN TaxaJuros decimal(10,4) null;
	ALTER TABLE PosicaoSwapHistorico ALTER COLUMN TaxaJuros decimal(10,4) null;
END
GO
	
IF EXISTS(select 1 from syscolumns where id = object_id('OperacaoSwap') and name = 'Percentual')		
BEGIN
	ALTER TABLE OperacaoSwap ALTER COLUMN Percentual decimal(10,4) null;
	ALTER TABLE PosicaoSwap ALTER COLUMN Percentual decimal(10,4) null;
	ALTER TABLE PosicaoSwapAbertura ALTER COLUMN Percentual decimal(10,4) null;
	ALTER TABLE PosicaoSwapHistorico ALTER COLUMN Percentual decimal(10,4) null	
END
GO
	
IF EXISTS(select 1 from syscolumns where id = object_id('OperacaoSwap') and name = 'TaxaJurosContraParte')	
BEGIN
	ALTER TABLE OperacaoSwap ALTER COLUMN TaxaJurosContraParte decimal(10,4) null;
	ALTER TABLE PosicaoSwap ALTER COLUMN TaxaJurosContraParte decimal(10,4) null;
	ALTER TABLE PosicaoSwapAbertura ALTER COLUMN TaxaJurosContraParte decimal(10,4) null;
	ALTER TABLE PosicaoSwapHistorico ALTER COLUMN TaxaJurosContraParte decimal(10,4) null	
END
GO

IF EXISTS(select 1 from syscolumns where id = object_id('OperacaoSwap') and name = 'PercentualContraParte')	
BEGIN
	ALTER TABLE OperacaoSwap ALTER COLUMN PercentualContraParte decimal(10,4) null;
	ALTER TABLE PosicaoSwap ALTER COLUMN PercentualContraParte decimal(10,4) null;
	ALTER TABLE PosicaoSwapAbertura ALTER COLUMN PercentualContraParte decimal(10,4) null;
	ALTER TABLE PosicaoSwapHistorico ALTER COLUMN PercentualContraParte decimal(10,4) null	
END
GO

IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'EventoSwap') = 0
Begin
	CREATE TABLE EventoSwap 
	( 	IdEventoSwap INT PRIMARY KEY NOT NULL IDENTITY , 
		TipoEvento smallint NOT NULL, 
		DataVigencia DateTime NOT NULL,
		DataEvento DateTime NOT NULL,
		IdOperacao int NOT NULL,
		PontaSwap smallint NULL,
		Valor decimal(10,2) NULL
	) 

	alter table EventoSwap add constraint EventoSwap_OpSwap_FK FOREIGN KEY ( IdOperacao ) references OperacaoSwap(IdOperacao)
end
go

IF NOT EXISTS(select 1 from syscolumns where id = object_id('PosicaoSwap') and name = 'DataUltimoEvento')	
BEGIN
	ALTER TABLE PosicaoSwap ADD DataUltimoEvento Datetime null;
	ALTER TABLE PosicaoSwapAbertura ADD DataUltimoEvento Datetime null;
	ALTER TABLE PosicaoSwapHistorico ADD DataUltimoEvento Datetime null;
END
GO

IF EXISTS( select OBJECT_ID('view__GPS__Agenda_Fluxo_Ativos_Creditos', 'V') )
BEGIN
	DROP VIEW view__GPS__Agenda_Fluxo_Ativos_Creditos
END
go 

CREATE VIEW [dbo].[view__GPS__Agenda_Fluxo_Ativos_Creditos] AS
SELECT Cl.Apelido, Cl.DataDia, HC.Data, HC.PLFechamento, TRF.Descricao, E.Nome, 0 RiscoFinal, PRF.Quantidade, PRF.ValorMercado, ARF.DataEvento, 
	CASE WHEN ARF.TipoEvento = 1 THEN 'Juros'
		 WHEN ARF.TipoEvento = 2 THEN 'Juros Correção'
		 WHEN ARF.TipoEvento = 3 THEN 'Amortização'
		 WHEN ARF.TipoEvento = 4 THEN 'Pagamento Principal'
		 WHEN ARF.TipoEvento = 8 THEN 'Amortização Corrigida'
		 ELSE 'Outros Eventos'
	END AS TipoEvento,
	ARF.Taxa, LRF.ValorBruto, '' Comentario, LRF.ValorIR, LRF.ValorIOF, LRF.ValorLiquido, cl.IdCliente
FROM Cliente Cl
	INNER JOIN HistoricoCota HC ON HC.IdCarteira = Cl.IdCliente
	INNER JOIN PosicaoRendaFixaHistorico PRF ON PRF.IdCliente = HC.IdCarteira AND PRF.DataHistorico = HC.Data
	INNER JOIN AgendaRendaFixa ARF ON ARF.IdTitulo = PRF.IdTitulo AND ARF.DataEvento >= PRF.DataHistorico
	INNER JOIN LiquidacaoRendaFixa LRF ON LRF.IdCliente = PRF.IdCliente AND LRF.IdTitulo = PRF.IdTitulo AND LRF.DataLiquidacao >= PRF.DataHistorico AND LRF.TipoLancamento = ARF.TipoEvento AND LRF.DataLiquidacao = ARF.DataPagamento
	INNER JOIN TituloRendaFixa TRF ON TRF.IdTitulo = LRF.IdTitulo
	INNER JOIN Emissor E ON E.IdEmissor = TRF.IdEmissor
UNION
SELECT Cl.Apelido, Cl.DataDia, HC.Data, HC.PLFechamento, PBC.CdAtivoBolsa, E.Nome, 0 RiscoFinal, PBC.Quantidade, PB.ValorMercado, PBC.DataLancamento, 
	CASE WHEN PBC.TipoProvento = 10 THEN 'Dividendo'
		 WHEN PBC.TipoProvento = 11 THEN 'Restituição Capital'
		 WHEN PBC.TipoProvento = 12 THEN 'Bonificação Financeira'
		 WHEN PBC.TipoProvento = 13 THEN 'Juros sobre Capital'
		 WHEN PBC.TipoProvento = 14 THEN 'Rendimento'
		 WHEN PBC.TipoProvento = 16 THEN 'Juros'
		 ELSE 'Outros Eventos'
	END AS TipoEvento,
	0 Percentual, PBC.Valor, '' Comentario, (PBC.Valor * PBC.PercentualIR) ValorIR, 0 OutrasTaxas, (PBC.Valor * (1 - PBC.PercentualIR)) ValorLiquido, Cl.IdCliente
FROM Cliente Cl
	INNER JOIN HistoricoCota HC ON HC.IdCarteira = Cl.IdCliente
	INNER JOIN PosicaoBolsaHistorico PB ON PB.IdCliente = HC.IdCarteira AND PB.DataHistorico = HC.Data
	INNER JOIN ProventoBolsaCliente PBC ON PBC.IdCliente = PB.IdCliente AND PBC.DataPagamento >= PB.DataHistorico
	INNER JOIN AtivoBolsa AB ON AB.CdAtivoBolsa = PBC.CdAtivoBolsa
	INNER JOIN Emissor E ON E.IdEmissor = AB.IdEmissor
UNION
SELECT Cl.Apelido, Cl.DataDia, Cl.DataDia, ps.Saldo as PLFechamento, 'Swap' as Descricao, '' as Nome, 0 RiscoFinal, 0 as Quantidade, 0 as ValorMercado, es.DataEvento, 
	CASE WHEN ES.TipoEvento = 1 THEN 'Reset de Swap'
	     WHEN ES.TipoEvento = 2 and ES.PontaSwap = 1 THEN 'Ajuste - Parte'
		 WHEN ES.TipoEvento = 2 and ES.PontaSwap = 2 THEN 'Ajuste - Contra Parte'
		 ELSE 'Outros Eventos'
	END AS TipoEvento,
	0 Percentual, es.valor AS ValorBruto, '' as Comentario, 0 as ValorIR, 0 as OutrasTaxas, es.valor as ValorLiquido, Cl.IdCliente
FROM Cliente Cl	
	INNER JOIN OperacaoSwap OS ON OS.IdCliente = Cl.IdCliente 	
	INNER JOIN PosicaoSwap PS ON OS.IdOperacao = PS.IdOperacao
	INNER JOIN EventoSwap ES ON ES.IdOperacao = OS.IdOperacao 
								and ES.DataVigencia in (select max(esAux.DataVigencia) 
													from EventoSwap esAux
													where esAux.IdOperacao = ES.IdOperacao 
													and esAux.DataEvento = ES.DataEvento
													and esAux.TipoEvento = ES.TipoEvento
													and esAux.PontaSwap = ES.PontaSwap )		
GO

if not exists(select 1 from syscolumns where id = object_id('IndicadoresCarteira') and name = 'RetornoDiaCotaRendimento')
BEGIN
	ALTER TABLE IndicadoresCarteira ADD RetornoDiaCotaRendimento decimal(20,12) NULL ;
END
GO

if not exists(select 1 from syscolumns where id = object_id('IndicadoresCarteira') and name = 'CotaPatrimonial')
BEGIN
	ALTER TABLE IndicadoresCarteira ADD CotaPatrimonial decimal(28,10) NULL ;
END
GO

if not exists(select 1 from syscolumns where id = object_id('IndicadoresCarteira') and name = 'CotaRendimento')
BEGIN
	ALTER TABLE IndicadoresCarteira ADD CotaRendimento decimal(28,10) NULL ;
END
GO

if not exists(select 1 from syscolumns where id = object_id('HistoricoCota') and name = 'ProventoAcumulado')
BEGIN
	ALTER TABLE HistoricoCota ADD ProventoAcumulado decimal(28,10) NULL ;
END
GO

IF NOT EXISTS(SELECT 1 FROM SYSCOLUMNS WHERE ID= OBJECT_ID('TituloRendaFixa') AND NAME = 'ETaxa')
BEGIN
	ALTER TABLE TituloRendaFixa add ETaxa INT not null default('5');
END
GO

if not exists(select 1 from syscolumns where id = object_id('OperacaoBMF') and name = 'TaxaClearingVlFixo')
	begin
		alter table OperacaoBMF add TaxaClearingVlFixo [decimal](16, 2) NOT NULL default 0
	end 
GO

if not exists(select 1 from syscolumns where id = object_id('TabelaCorretagemBMF') and name = 'FaixaVencimentoValidoPosteriores')
	begin
		alter table TabelaCorretagemBMF add FaixaVencimentoValidoPosteriores char(1) not null default 'N'
	end 
GO

if not exists(select 1 from syscolumns where id = object_id('ClienteBMF') and name = 'IdCustodianteBmf')
	begin
		alter table ClienteBMF add IdCustodianteBmf int null
	end 
GO

if (not exists (SELECT * FROM INFORMATION_SCHEMA.REFERENTIAL_CONSTRAINTS WHERE CONSTRAINT_NAME ='FK_ClienteBMFCustodianteBmf_AgenteMercado'))
	begin
		ALTER TABLE ClienteBMF  WITH CHECK ADD  CONSTRAINT FK_ClienteBMFCustodianteBmf_AgenteMercado FOREIGN KEY(IdCustodianteBmf)
		REFERENCES AgenteMercado (IdAgente)
	end
GO

IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'PerfilTaxaClearingBMF') = 0
	begin
		create table PerfilTaxaClearingBMF
		( IdPerfil int not null, 
		  Descricao varchar(50) not null
		 CONSTRAINT [PK_PerfilTaxaClearingBMF] PRIMARY KEY CLUSTERED 
		(
			IdPerfil ASC
		)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
		) ON [PRIMARY]
	end
go
	
IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'TabelaTaxaClearingBMF') = 0
	begin
		create table TabelaTaxaClearingBMF
		(	IdTabela int IDENTITY(1,1),
			IdPerfilTaxaClearingBMF int not null, 
			TipoMercado	tinyint not null,
			CdAtivoBMF varchar (20) not null,
			Serie varchar (5) not null,
			FaixaVencimento int not null,
			UltimoVencimento char(1) not null,
			ValorFixoAgenteCustodia [decimal](16, 4) NOT NULL,
			ValorFixoCorretora [decimal](16, 4) NOT NULL
		 CONSTRAINT [PK_TabelaTaxaClearingBMF] PRIMARY KEY CLUSTERED 
		(
			IdTabela ASC
		)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
		) ON [PRIMARY]
end
go

IF NOT EXISTS (select 1 from sysconstraints where object_name(id) = 'TabelaTaxaClearingBMF' and object_name(constid) = 'TabelaTaxaClearingBMF_FK1')	
begin
	ALTER TABLE TabelaTaxaClearingBMF  WITH CHECK ADD  CONSTRAINT TabelaTaxaClearingBMF_FK1 FOREIGN KEY(IdPerfilTaxaClearingBMF)
	REFERENCES PerfilTaxaClearingBMF (IdPerfil)
	ON DELETE CASCADE
end
go

IF NOT EXISTS (select 1 from sysconstraints where object_name(id) = 'TabelaTaxaClearingBMF' and object_name(constid) = 'TabelaTaxaClearingBMF_FK2')	
begin
	if exists (select 1 from syscolumns where id = object_id('TabelaTaxaClearingBMF') and name = 'serie')
	begin
		ALTER TABLE TabelaTaxaClearingBMF  WITH CHECK ADD  CONSTRAINT TabelaTaxaClearingBMF_FK2 FOREIGN KEY(CdAtivoBMF, Serie)
		REFERENCES AtivoBMF (CdAtivoBMF, Serie)
		ON DELETE CASCADE
	end	
end
go

IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'PerfilTaxaClearingBMFCliente') = 0
	begin
		create table PerfilTaxaClearingBMFCliente
		(	IdPerfilTaxaClearingBMF int not null,
			IdCliente int not null
		 CONSTRAINT [PK_PerfilTaxaClearingBMFCliente] PRIMARY KEY CLUSTERED 
		(
			IdPerfilTaxaClearingBMF ASC,
			IdCliente ASC
		)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
		) ON [PRIMARY]
	end
	go
	
IF NOT EXISTS (select 1 from sysconstraints where object_name(id) = 'PerfilTaxaClearingBMFCliente' and object_name(constid) = 'PerfilTaxaClearingBMFCliente_FK1')	
begin
	ALTER TABLE PerfilTaxaClearingBMFCliente  WITH CHECK ADD  CONSTRAINT PerfilTaxaClearingBMFCliente_FK1 FOREIGN KEY(IdPerfilTaxaClearingBMF)
	REFERENCES PerfilTaxaClearingBMF (IdPerfil)
	ON DELETE CASCADE
end
go

IF NOT EXISTS (select 1 from sysconstraints where object_name(id) = 'PerfilTaxaClearingBMFCliente' and object_name(constid) = 'PerfilTaxaClearingBMFCliente_FK2')	
begin
	ALTER TABLE PerfilTaxaClearingBMFCliente  WITH CHECK ADD  CONSTRAINT PerfilTaxaClearingBMFCliente_FK2 FOREIGN KEY([IdCliente])
	REFERENCES Cliente (IdCliente)
end
go

if not exists(select 1 from syscolumns where id = object_id('TabelaCorretagemBMF') and name = 'TaxaVencimento')
begin
	alter table TabelaCorretagemBMF add TaxaVencimento decimal(16, 8) not null default('0')
end
go

if not exists(select 1 from syscolumns where id = object_id('SerieRendaFixa') and name = 'TipoTaxa')
BEGIN
	ALTER TABLE SerieRendaFixa ADD TipoTaxa	INT NOT NULL default '1'; --Não faz
	
	exec ('UPDATE SerieRendaFixa
	SET TipoTaxa = isnull(titulo.TipoMTM, 1)
	FROM SerieRendaFixa serie
	INNER JOIN TituloRendaFixa titulo ON serie.IdSerie = titulo.IdSerie')
END 	
GO

if not exists(select 1 from syscolumns where id = object_id('SerieRendaFixa') and name = 'IdFeeder')
BEGIN	
	ALTER TABLE SerieRendaFixa ADD IdFeeder	INT NOT NULL default '5';
END 	
GO

if exists(select 1 from syscolumns where id = object_id('TituloRendaFixa') and name = 'TipoMTM')
Begin
	create table DataAux
	(
		idDataAux 					INT 	PRIMARY KEY identity NOT NULL, 
		dataAtual						DateTime			NOT NULL, 
	)
	
	INSERT INTO DataAux (DataAtual) Values (Convert(DateTime, '01/06/2015', 103));
	INSERT INTO DataAux (DataAtual) Values (Convert(DateTime, '02/06/2015', 103));
	INSERT INTO DataAux (DataAtual) Values (Convert(DateTime, '03/06/2015', 103));
	INSERT INTO DataAux (DataAtual) Values (Convert(DateTime, '04/06/2015', 103));
	INSERT INTO DataAux (DataAtual) Values (Convert(DateTime, '05/06/2015', 103));
	INSERT INTO DataAux (DataAtual) Values (Convert(DateTime, '08/06/2015', 103));
	INSERT INTO DataAux (DataAtual) Values (Convert(DateTime, '09/06/2015', 103));
	INSERT INTO DataAux (DataAtual) Values (Convert(DateTime, '10/06/2015', 103));
	INSERT INTO DataAux (DataAtual) Values (Convert(DateTime, '11/06/2015', 103));
	INSERT INTO DataAux (DataAtual) Values (Convert(DateTime, '12/06/2015', 103));
	INSERT INTO DataAux (DataAtual) Values (Convert(DateTime, '15/06/2015', 103));
	INSERT INTO DataAux (DataAtual) Values (Convert(DateTime, '16/06/2015', 103));
	INSERT INTO DataAux (DataAtual) Values (Convert(DateTime, '17/06/2015', 103));
	INSERT INTO DataAux (DataAtual) Values (Convert(DateTime, '18/06/2015', 103));
	INSERT INTO DataAux (DataAtual) Values (Convert(DateTime, '19/06/2015', 103));
	INSERT INTO DataAux (DataAtual) Values (Convert(DateTime, '22/06/2015', 103));
	INSERT INTO DataAux (DataAtual) Values (Convert(DateTime, '23/06/2015', 103));
	INSERT INTO DataAux (DataAtual) Values (Convert(DateTime, '24/06/2015', 103));
	INSERT INTO DataAux (DataAtual) Values (Convert(DateTime, '25/06/2015', 103));
	INSERT INTO DataAux (DataAtual) Values (Convert(DateTime, '26/06/2015', 103));
	INSERT INTO DataAux (DataAtual) Values (Convert(DateTime, '29/06/2015', 103));
	INSERT INTO DataAux (DataAtual) Values (Convert(DateTime, '30/06/2015', 103));
	INSERT INTO DataAux (DataAtual) Values (Convert(DateTime, '01/07/2015', 103));
	INSERT INTO DataAux (DataAtual) Values (Convert(DateTime, '02/07/2015', 103));
	INSERT INTO DataAux (DataAtual) Values (Convert(DateTime, '03/07/2015', 103));
	INSERT INTO DataAux (DataAtual) Values (Convert(DateTime, '06/07/2015', 103));
	INSERT INTO DataAux (DataAtual) Values (Convert(DateTime, '07/07/2015', 103));
	INSERT INTO DataAux (DataAtual) Values (Convert(DateTime, '08/07/2015', 103));
	INSERT INTO DataAux (DataAtual) Values (Convert(DateTime, '09/07/2015', 103));
	INSERT INTO DataAux (DataAtual) Values (Convert(DateTime, '10/07/2015', 103));
	INSERT INTO DataAux (DataAtual) Values (Convert(DateTime, '13/07/2015', 103));
	INSERT INTO DataAux (DataAtual) Values (Convert(DateTime, '14/07/2015', 103));
	INSERT INTO DataAux (DataAtual) Values (Convert(DateTime, '15/07/2015', 103));
	INSERT INTO DataAux (DataAtual) Values (Convert(DateTime, '16/07/2015', 103));
	INSERT INTO DataAux (DataAtual) Values (Convert(DateTime, '17/07/2015', 103));
	INSERT INTO DataAux (DataAtual) Values (Convert(DateTime, '20/07/2015', 103));
	INSERT INTO DataAux (DataAtual) Values (Convert(DateTime, '21/07/2015', 103));
	INSERT INTO DataAux (DataAtual) Values (Convert(DateTime, '22/07/2015', 103));
	INSERT INTO DataAux (DataAtual) Values (Convert(DateTime, '23/07/2015', 103));
	INSERT INTO DataAux (DataAtual) Values (Convert(DateTime, '24/07/2015', 103));
	INSERT INTO DataAux (DataAtual) Values (Convert(DateTime, '27/07/2015', 103));
	INSERT INTO DataAux (DataAtual) Values (Convert(DateTime, '28/07/2015', 103));
	INSERT INTO DataAux (DataAtual) Values (Convert(DateTime, '29/07/2015', 103));
	INSERT INTO DataAux (DataAtual) Values (Convert(DateTime, '30/07/2015', 103));
	INSERT INTO DataAux (DataAtual) Values (Convert(DateTime, '31/07/2015', 103));
	INSERT INTO DataAux (DataAtual) Values (Convert(DateTime, '03/08/2015', 103));
	INSERT INTO DataAux (DataAtual) Values (Convert(DateTime, '04/08/2015', 103));
	INSERT INTO DataAux (DataAtual) Values (Convert(DateTime, '05/08/2015', 103));
	INSERT INTO DataAux (DataAtual) Values (Convert(DateTime, '06/08/2015', 103));
	INSERT INTO DataAux (DataAtual) Values (Convert(DateTime, '07/08/2015', 103));
	INSERT INTO DataAux (DataAtual) Values (Convert(DateTime, '10/08/2015', 103));
	INSERT INTO DataAux (DataAtual) Values (Convert(DateTime, '11/08/2015', 103));
	INSERT INTO DataAux (DataAtual) Values (Convert(DateTime, '12/08/2015', 103));
	INSERT INTO DataAux (DataAtual) Values (Convert(DateTime, '13/08/2015', 103));
	INSERT INTO DataAux (DataAtual) Values (Convert(DateTime, '14/08/2015', 103));
	INSERT INTO DataAux (DataAtual) Values (Convert(DateTime, '17/08/2015', 103));
	INSERT INTO DataAux (DataAtual) Values (Convert(DateTime, '18/08/2015', 103));
	INSERT INTO DataAux (DataAtual) Values (Convert(DateTime, '19/08/2015', 103));
	INSERT INTO DataAux (DataAtual) Values (Convert(DateTime, '20/08/2015', 103));
	INSERT INTO DataAux (DataAtual) Values (Convert(DateTime, '21/08/2015', 103));
	INSERT INTO DataAux (DataAtual) Values (Convert(DateTime, '24/08/2015', 103));
	INSERT INTO DataAux (DataAtual) Values (Convert(DateTime, '25/08/2015', 103));
	INSERT INTO DataAux (DataAtual) Values (Convert(DateTime, '26/08/2015', 103));
	INSERT INTO DataAux (DataAtual) Values (Convert(DateTime, '27/08/2015', 103));
	INSERT INTO DataAux (DataAtual) Values (Convert(DateTime, '28/08/2015', 103));
	INSERT INTO DataAux (DataAtual) Values (Convert(DateTime, '31/08/2015', 103));
	INSERT INTO DataAux (DataAtual) Values (Convert(DateTime, '01/09/2015', 103));
	INSERT INTO DataAux (DataAtual) Values (Convert(DateTime, '02/09/2015', 103));
	INSERT INTO DataAux (DataAtual) Values (Convert(DateTime, '03/09/2015', 103));
	INSERT INTO DataAux (DataAtual) Values (Convert(DateTime, '04/09/2015', 103));
	INSERT INTO DataAux (DataAtual) Values (Convert(DateTime, '07/09/2015', 103));
	INSERT INTO DataAux (DataAtual) Values (Convert(DateTime, '08/09/2015', 103));
	INSERT INTO DataAux (DataAtual) Values (Convert(DateTime, '09/09/2015', 103));
	INSERT INTO DataAux (DataAtual) Values (Convert(DateTime, '10/09/2015', 103));
	INSERT INTO DataAux (DataAtual) Values (Convert(DateTime, '11/09/2015', 103));
	INSERT INTO DataAux (DataAtual) Values (Convert(DateTime, '14/09/2015', 103));
	INSERT INTO DataAux (DataAtual) Values (Convert(DateTime, '15/09/2015', 103));
	INSERT INTO DataAux (DataAtual) Values (Convert(DateTime, '16/09/2015', 103));
	INSERT INTO DataAux (DataAtual) Values (Convert(DateTime, '17/09/2015', 103));
	INSERT INTO DataAux (DataAtual) Values (Convert(DateTime, '18/09/2015', 103));
	INSERT INTO DataAux (DataAtual) Values (Convert(DateTime, '21/09/2015', 103));
	INSERT INTO DataAux (DataAtual) Values (Convert(DateTime, '22/09/2015', 103));
	INSERT INTO DataAux (DataAtual) Values (Convert(DateTime, '23/09/2015', 103));
	INSERT INTO DataAux (DataAtual) Values (Convert(DateTime, '24/09/2015', 103));
	INSERT INTO DataAux (DataAtual) Values (Convert(DateTime, '25/09/2015', 103));
	INSERT INTO DataAux (DataAtual) Values (Convert(DateTime, '28/09/2015', 103));
	INSERT INTO DataAux (DataAtual) Values (Convert(DateTime, '29/09/2015', 103));
	INSERT INTO DataAux (DataAtual) Values (Convert(DateTime, '30/09/2015', 103));
	INSERT INTO DataAux (DataAtual) Values (Convert(DateTime, '01/10/2015', 103));
	INSERT INTO DataAux (DataAtual) Values (Convert(DateTime, '02/10/2015', 103));
	INSERT INTO DataAux (DataAtual) Values (Convert(DateTime, '05/10/2015', 103));
	INSERT INTO DataAux (DataAtual) Values (Convert(DateTime, '06/10/2015', 103));
	INSERT INTO DataAux (DataAtual) Values (Convert(DateTime, '07/10/2015', 103));
	INSERT INTO DataAux (DataAtual) Values (Convert(DateTime, '08/10/2015', 103));
	INSERT INTO DataAux (DataAtual) Values (Convert(DateTime, '09/10/2015', 103));
	INSERT INTO DataAux (DataAtual) Values (Convert(DateTime, '12/10/2015', 103));
	INSERT INTO DataAux (DataAtual) Values (Convert(DateTime, '13/10/2015', 103));
	INSERT INTO DataAux (DataAtual) Values (Convert(DateTime, '14/10/2015', 103));
	INSERT INTO DataAux (DataAtual) Values (Convert(DateTime, '15/10/2015', 103));
	INSERT INTO DataAux (DataAtual) Values (Convert(DateTime, '16/10/2015', 103));
	INSERT INTO DataAux (DataAtual) Values (Convert(DateTime, '19/10/2015', 103));
	INSERT INTO DataAux (DataAtual) Values (Convert(DateTime, '20/10/2015', 103));
	INSERT INTO DataAux (DataAtual) Values (Convert(DateTime, '21/10/2015', 103));
	INSERT INTO DataAux (DataAtual) Values (Convert(DateTime, '22/10/2015', 103));
	INSERT INTO DataAux (DataAtual) Values (Convert(DateTime, '23/10/2015', 103));
	INSERT INTO DataAux (DataAtual) Values (Convert(DateTime, '26/10/2015', 103));
	INSERT INTO DataAux (DataAtual) Values (Convert(DateTime, '27/10/2015', 103));
	INSERT INTO DataAux (DataAtual) Values (Convert(DateTime, '28/10/2015', 103));
	INSERT INTO DataAux (DataAtual) Values (Convert(DateTime, '29/10/2015', 103));
	INSERT INTO DataAux (DataAtual) Values (Convert(DateTime, '30/10/2015', 103));
	INSERT INTO DataAux (DataAtual) Values (Convert(DateTime, '02/11/2015', 103));
	INSERT INTO DataAux (DataAtual) Values (Convert(DateTime, '03/11/2015', 103));
	INSERT INTO DataAux (DataAtual) Values (Convert(DateTime, '04/11/2015', 103));
	INSERT INTO DataAux (DataAtual) Values (Convert(DateTime, '05/11/2015', 103));
	INSERT INTO DataAux (DataAtual) Values (Convert(DateTime, '06/11/2015', 103));
	INSERT INTO DataAux (DataAtual) Values (Convert(DateTime, '09/11/2015', 103));
	INSERT INTO DataAux (DataAtual) Values (Convert(DateTime, '10/11/2015', 103));
	INSERT INTO DataAux (DataAtual) Values (Convert(DateTime, '11/11/2015', 103));
	INSERT INTO DataAux (DataAtual) Values (Convert(DateTime, '12/11/2015', 103));
	INSERT INTO DataAux (DataAtual) Values (Convert(DateTime, '13/11/2015', 103));
	INSERT INTO DataAux (DataAtual) Values (Convert(DateTime, '16/11/2015', 103));
	INSERT INTO DataAux (DataAtual) Values (Convert(DateTime, '17/11/2015', 103));
	INSERT INTO DataAux (DataAtual) Values (Convert(DateTime, '18/11/2015', 103));
	INSERT INTO DataAux (DataAtual) Values (Convert(DateTime, '19/11/2015', 103));
	INSERT INTO DataAux (DataAtual) Values (Convert(DateTime, '20/11/2015', 103));
	INSERT INTO DataAux (DataAtual) Values (Convert(DateTime, '23/11/2015', 103));
	INSERT INTO DataAux (DataAtual) Values (Convert(DateTime, '24/11/2015', 103));
	INSERT INTO DataAux (DataAtual) Values (Convert(DateTime, '25/11/2015', 103));
	INSERT INTO DataAux (DataAtual) Values (Convert(DateTime, '26/11/2015', 103));
	INSERT INTO DataAux (DataAtual) Values (Convert(DateTime, '27/11/2015', 103));
	INSERT INTO DataAux (DataAtual) Values (Convert(DateTime, '30/11/2015', 103));
	INSERT INTO DataAux (DataAtual) Values (Convert(DateTime, '01/12/2015', 103));
	INSERT INTO DataAux (DataAtual) Values (Convert(DateTime, '02/12/2015', 103));
	INSERT INTO DataAux (DataAtual) Values (Convert(DateTime, '03/12/2015', 103));
	INSERT INTO DataAux (DataAtual) Values (Convert(DateTime, '04/12/2015', 103));
	INSERT INTO DataAux (DataAtual) Values (Convert(DateTime, '07/12/2015', 103));
	INSERT INTO DataAux (DataAtual) Values (Convert(DateTime, '08/12/2015', 103));
	INSERT INTO DataAux (DataAtual) Values (Convert(DateTime, '09/12/2015', 103));
	INSERT INTO DataAux (DataAtual) Values (Convert(DateTime, '10/12/2015', 103));
	INSERT INTO DataAux (DataAtual) Values (Convert(DateTime, '11/12/2015', 103));
	INSERT INTO DataAux (DataAtual) Values (Convert(DateTime, '14/12/2015', 103));
	INSERT INTO DataAux (DataAtual) Values (Convert(DateTime, '15/12/2015', 103));
	INSERT INTO DataAux (DataAtual) Values (Convert(DateTime, '16/12/2015', 103));
	INSERT INTO DataAux (DataAtual) Values (Convert(DateTime, '17/12/2015', 103));
	INSERT INTO DataAux (DataAtual) Values (Convert(DateTime, '18/12/2015', 103));
	INSERT INTO DataAux (DataAtual) Values (Convert(DateTime, '21/12/2015', 103));
	INSERT INTO DataAux (DataAtual) Values (Convert(DateTime, '22/12/2015', 103));
	INSERT INTO DataAux (DataAtual) Values (Convert(DateTime, '23/12/2015', 103));
	INSERT INTO DataAux (DataAtual) Values (Convert(DateTime, '24/12/2015', 103));
	INSERT INTO DataAux (DataAtual) Values (Convert(DateTime, '25/12/2015', 103));
	INSERT INTO DataAux (DataAtual) Values (Convert(DateTime, '28/12/2015', 103));
	INSERT INTO DataAux (DataAtual) Values (Convert(DateTime, '29/12/2015', 103));
	INSERT INTO DataAux (DataAtual) Values (Convert(DateTime, '30/12/2015', 103));
	INSERT INTO DataAux (DataAtual) Values (Convert(DateTime, '31/12/2015', 103));
	INSERT INTO DataAux (DataAtual) Values (Convert(DateTime, '01/01/2016', 103));
	INSERT INTO DataAux (DataAtual) Values (Convert(DateTime, '04/01/2016', 103));
	INSERT INTO DataAux (DataAtual) Values (Convert(DateTime, '05/01/2016', 103));
	INSERT INTO DataAux (DataAtual) Values (Convert(DateTime, '06/01/2016', 103));
	INSERT INTO DataAux (DataAtual) Values (Convert(DateTime, '07/01/2016', 103));
	INSERT INTO DataAux (DataAtual) Values (Convert(DateTime, '08/01/2016', 103));
	INSERT INTO DataAux (DataAtual) Values (Convert(DateTime, '11/01/2016', 103));
	INSERT INTO DataAux (DataAtual) Values (Convert(DateTime, '12/01/2016', 103));
	INSERT INTO DataAux (DataAtual) Values (Convert(DateTime, '13/01/2016', 103));
	INSERT INTO DataAux (DataAtual) Values (Convert(DateTime, '14/01/2016', 103));
	INSERT INTO DataAux (DataAtual) Values (Convert(DateTime, '15/01/2016', 103));
	INSERT INTO DataAux (DataAtual) Values (Convert(DateTime, '18/01/2016', 103));
	INSERT INTO DataAux (DataAtual) Values (Convert(DateTime, '19/01/2016', 103));
	INSERT INTO DataAux (DataAtual) Values (Convert(DateTime, '20/01/2016', 103));
	INSERT INTO DataAux (DataAtual) Values (Convert(DateTime, '21/01/2016', 103));
	INSERT INTO DataAux (DataAtual) Values (Convert(DateTime, '22/01/2016', 103));
	INSERT INTO DataAux (DataAtual) Values (Convert(DateTime, '25/01/2016', 103));
	INSERT INTO DataAux (DataAtual) Values (Convert(DateTime, '26/01/2016', 103));
	INSERT INTO DataAux (DataAtual) Values (Convert(DateTime, '27/01/2016', 103));
	INSERT INTO DataAux (DataAtual) Values (Convert(DateTime, '28/01/2016', 103));
	INSERT INTO DataAux (DataAtual) Values (Convert(DateTime, '29/01/2016', 103));
	INSERT INTO DataAux (DataAtual) Values (Convert(DateTime, '01/02/2016', 103));
	INSERT INTO DataAux (DataAtual) Values (Convert(DateTime, '02/02/2016', 103));
	INSERT INTO DataAux (DataAtual) Values (Convert(DateTime, '03/02/2016', 103));
	INSERT INTO DataAux (DataAtual) Values (Convert(DateTime, '04/02/2016', 103));
	INSERT INTO DataAux (DataAtual) Values (Convert(DateTime, '05/02/2016', 103));
	INSERT INTO DataAux (DataAtual) Values (Convert(DateTime, '08/02/2016', 103));
	INSERT INTO DataAux (DataAtual) Values (Convert(DateTime, '09/02/2016', 103));
	INSERT INTO DataAux (DataAtual) Values (Convert(DateTime, '10/02/2016', 103));
	INSERT INTO DataAux (DataAtual) Values (Convert(DateTime, '11/02/2016', 103));
	INSERT INTO DataAux (DataAtual) Values (Convert(DateTime, '12/02/2016', 103));
	INSERT INTO DataAux (DataAtual) Values (Convert(DateTime, '15/02/2016', 103));
	INSERT INTO DataAux (DataAtual) Values (Convert(DateTime, '16/02/2016', 103));
	INSERT INTO DataAux (DataAtual) Values (Convert(DateTime, '17/02/2016', 103));
	INSERT INTO DataAux (DataAtual) Values (Convert(DateTime, '18/02/2016', 103));
	INSERT INTO DataAux (DataAtual) Values (Convert(DateTime, '19/02/2016', 103));
	INSERT INTO DataAux (DataAtual) Values (Convert(DateTime, '22/02/2016', 103));
	INSERT INTO DataAux (DataAtual) Values (Convert(DateTime, '23/02/2016', 103));
	INSERT INTO DataAux (DataAtual) Values (Convert(DateTime, '24/02/2016', 103));
	INSERT INTO DataAux (DataAtual) Values (Convert(DateTime, '25/02/2016', 103));
	INSERT INTO DataAux (DataAtual) Values (Convert(DateTime, '26/02/2016', 103));
	INSERT INTO DataAux (DataAtual) Values (Convert(DateTime, '29/02/2016', 103));
	INSERT INTO DataAux (DataAtual) Values (Convert(DateTime, '1/3/2016', 103));
	INSERT INTO DataAux (DataAtual) Values (Convert(DateTime, '2/3/2016', 103));
	INSERT INTO DataAux (DataAtual) Values (Convert(DateTime, '3/3/2016', 103));
	INSERT INTO DataAux (DataAtual) Values (Convert(DateTime, '4/3/2016', 103));
	INSERT INTO DataAux (DataAtual) Values (Convert(DateTime, '7/3/2016', 103));
	INSERT INTO DataAux (DataAtual) Values (Convert(DateTime, '8/3/2016', 103));
	INSERT INTO DataAux (DataAtual) Values (Convert(DateTime, '9/3/2016', 103));
	INSERT INTO DataAux (DataAtual) Values (Convert(DateTime, '10/3/2016', 103));
	INSERT INTO DataAux (DataAtual) Values (Convert(DateTime, '11/3/2016', 103));
	INSERT INTO DataAux (DataAtual) Values (Convert(DateTime, '14/3/2016', 103));
	INSERT INTO DataAux (DataAtual) Values (Convert(DateTime, '15/3/2016', 103));
	INSERT INTO DataAux (DataAtual) Values (Convert(DateTime, '16/3/2016', 103));
	INSERT INTO DataAux (DataAtual) Values (Convert(DateTime, '17/3/2016', 103));
	INSERT INTO DataAux (DataAtual) Values (Convert(DateTime, '18/3/2016', 103));
	INSERT INTO DataAux (DataAtual) Values (Convert(DateTime, '21/3/2016', 103));
	INSERT INTO DataAux (DataAtual) Values (Convert(DateTime, '22/3/2016', 103));
	INSERT INTO DataAux (DataAtual) Values (Convert(DateTime, '23/3/2016', 103));
	INSERT INTO DataAux (DataAtual) Values (Convert(DateTime, '24/3/2016', 103));
	INSERT INTO DataAux (DataAtual) Values (Convert(DateTime, '28/3/2016', 103));
	INSERT INTO DataAux (DataAtual) Values (Convert(DateTime, '29/3/2016', 103));
	INSERT INTO DataAux (DataAtual) Values (Convert(DateTime, '30/3/2016', 103));
	INSERT INTO DataAux (DataAtual) Values (Convert(DateTime, '31/3/2016', 103));
	INSERT INTO DataAux (DataAtual) Values (Convert(DateTime, '1/4/2016', 103));
	INSERT INTO DataAux (DataAtual) Values (Convert(DateTime, '4/4/2016', 103));
	INSERT INTO DataAux (DataAtual) Values (Convert(DateTime, '5/4/2016', 103));
	INSERT INTO DataAux (DataAtual) Values (Convert(DateTime, '6/4/2016', 103));
	INSERT INTO DataAux (DataAtual) Values (Convert(DateTime, '7/4/2016', 103));
	INSERT INTO DataAux (DataAtual) Values (Convert(DateTime, '8/4/2016', 103));
	INSERT INTO DataAux (DataAtual) Values (Convert(DateTime, '11/4/2016', 103));
	INSERT INTO DataAux (DataAtual) Values (Convert(DateTime, '12/4/2016', 103));
	INSERT INTO DataAux (DataAtual) Values (Convert(DateTime, '13/4/2016', 103));
	INSERT INTO DataAux (DataAtual) Values (Convert(DateTime, '14/4/2016', 103));
	INSERT INTO DataAux (DataAtual) Values (Convert(DateTime, '15/4/2016', 103));
	INSERT INTO DataAux (DataAtual) Values (Convert(DateTime, '18/4/2016', 103));
	INSERT INTO DataAux (DataAtual) Values (Convert(DateTime, '19/4/2016', 103));
	INSERT INTO DataAux (DataAtual) Values (Convert(DateTime, '20/4/2016', 103));
	INSERT INTO DataAux (DataAtual) Values (Convert(DateTime, '22/4/2016', 103));
	INSERT INTO DataAux (DataAtual) Values (Convert(DateTime, '25/4/2016', 103));
	INSERT INTO DataAux (DataAtual) Values (Convert(DateTime, '26/4/2016', 103));
	INSERT INTO DataAux (DataAtual) Values (Convert(DateTime, '27/4/2016', 103));
	INSERT INTO DataAux (DataAtual) Values (Convert(DateTime, '28/4/2016', 103));

	
	SET IDENTITY_INSERT GrupoPerfilMTM ON
	if (select count(1) from GrupoPerfilMTM where IdGrupoPerfilMTM = 1) = 0	
	BEGIN
		insert into GrupoPerfilMTM (IdGrupoPerfilMTM, Descricao) values (1, 'Grupo Default');
	END 
	SET IDENTITY_INSERT GrupoPerfilMTM OFF

	--Não Faz
	INSERT INTO SerieRendaFixa (Descricao, TipoTaxa, IdFeeder)
	VALUES('Não faz', 1, 1);

	DECLARE @NaoFaz int;
	SET @NaoFaz = (SELECT top 1 (IdSerie) FROM SerieRendaFixa WHERE Descricao = 'Não faz' AND TipoTaxa = 1 AND IdFeeder = 1 )

	--Anbima Tit.Publico
	INSERT INTO SerieRendaFixa (Descricao, TipoTaxa, IdFeeder)
	VALUES('Anbima Títulos Públicos', 2, 1);

	DECLARE @Andima_TituloPublico int;
	SET @Andima_TituloPublico = (SELECT top 1 (IdSerie) FROM SerieRendaFixa WHERE Descricao = 'Anbima Títulos Públicos' AND TipoTaxa = 2 AND IdFeeder = 1 )

	--Mercado Debentures
	INSERT INTO SerieRendaFixa (Descricao, TipoTaxa, IdFeeder)
	VALUES('Mercado Debêntures', 3, 1);

	DECLARE @Mercado_Debenture int;
	SET @Mercado_Debenture = (SELECT top 1 (IdSerie) FROM SerieRendaFixa WHERE Descricao = 'Mercado Debêntures' AND TipoTaxa = 3 AND IdFeeder = 1 )

	--Tesouro Direto
	INSERT INTO SerieRendaFixa (Descricao, TipoTaxa, IdFeeder)
	VALUES('Tesouro Direto', 5, 1);

	DECLARE @Tesouro int;
	SET @Tesouro = (SELECT top 1 (IdSerie) FROM SerieRendaFixa WHERE Descricao = 'Tesouro Direto' AND TipoTaxa = 5 AND IdFeeder = 1 )

	--Fundo Anbima
	INSERT INTO SerieRendaFixa (Descricao, TipoTaxa, IdFeeder)
	VALUES('Fundo Anbima', 7, 1);

	DECLARE @CotaAnbima int;
	SET @CotaAnbima = (SELECT top 1 (IdSerie) FROM SerieRendaFixa WHERE Descricao = 'Fundo Anbima' AND TipoTaxa = 7 AND IdFeeder = 1 )

	--Par Debentures
	INSERT INTO SerieRendaFixa (Descricao, TipoTaxa, IdFeeder)
	VALUES('Par Debêntures', 8, 1);

	DECLARE @Par_Debenture int;
	SET @Par_Debenture = (SELECT top 1 (IdSerie) FROM SerieRendaFixa WHERE Descricao = 'Par Debêntures' AND TipoTaxa = 8 AND IdFeeder = 1 )

	--Bovespa BMF
	INSERT INTO SerieRendaFixa (Descricao, TipoTaxa, IdFeeder)
	VALUES('Bovespa BMF', 9, 1);

	DECLARE @BovespaBMF int;
	SET @BovespaBMF = (SELECT top 1 (IdSerie) FROM SerieRendaFixa WHERE Descricao = 'Bovespa BMF' AND TipoTaxa = 9 AND IdFeeder = 1 )

	if exists(select 1 from syscolumns where id = object_id('TituloRendaFixa') and name = 'idSerie')
	BEGIN
		EXEC('INSERT INTO PerfilMTM (DtVigencia, IdPapel, IdTitulo, IdOperacao, IdSerie, IdGrupoPerfilMTM)
				select distinct dataRef.dataAtual,
					titulo.IdPapel,
					titulo.IdTitulo,
					null,
					CASE	WHEN titulo.TipoMTM = 1 THEN ' + @NaoFaz + '
							WHEN titulo.TipoMTM = 2 THEN ' + @Andima_TituloPublico + '
							WHEN titulo.TipoMTM = 3 THEN ' + @Mercado_Debenture + '
							WHEN titulo.TipoMTM = 4 THEN isnull(titulo.IdSerie,' + @NaoFaz + ')
							WHEN titulo.TipoMTM = 5 THEN isnull(titulo.IdSerie,' + @Tesouro + ')
							WHEN titulo.TipoMTM = 6 THEN titulo.IdSerie
							WHEN titulo.TipoMTM = 7 THEN ' + @CotaAnbima + '
							WHEN titulo.TipoMTM = 8 THEN ' + @Par_Debenture + '
							WHEN titulo.TipoMTM = 9 THEN ' + @BovespaBMF + '
				    END,
					1				
				From TituloRendaFixa titulo, dataAux dataRef');
	END
		
	drop table DataAux;
END
GO	

if exists(select 1 from syscolumns where id = object_id('PerfilMTM') and name = 'DtVigencia')
BEGIN
	exec sp_rename 'PerfilMTM.[DtVigencia]', 'DtReferencia', 'column'
END 
GO

if exists(select 1 from syscolumns where id = object_id('TituloRendaFixa') and name = 'TipoMTM')
BEGIN
	ALTER TABLE TituloRendaFixa Drop Column TipoMTM;
END
GO

IF (OBJECT_ID('SerieRendaFixa_TituloRendaFixa_FK1', 'F') IS NOT NULL)
BEGIN
	 ALTER TABLE TituloRendaFixa Drop constraint SerieRendaFixa_TituloRendaFixa_FK1;
END

if exists(select 1 from syscolumns where id = object_id('TituloRendaFixa') and name = 'IdSerie')
BEGIN
	ALTER TABLE TituloRendaFixa Drop Column IdSerie;
END	
GO
	
if NOT exists(select 1 from syscolumns where id = object_id('Carteira') and name = 'CalculaMTM')
BEGIN
	ALTER TABLE Carteira ADD CalculaMTM INT NULL;
	exec('UPDATE Carteira SET CalculaMTM = 1');
	
	SET IDENTITY_INSERT GrupoPerfilMTM ON
	if (select count(1) from GrupoPerfilMTM where IdGrupoPerfilMTM = 1) = 0	
	BEGIN
		insert into GrupoPerfilMTM (IdGrupoPerfilMTM, Descricao) values (1, 'Grupo Default');
	END 
	SET IDENTITY_INSERT GrupoPerfilMTM OFF
	
	UPDATE Carteira SET IdGrupoPerfilMTM = 1;
END
GO	

IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'V' AND NAME = 'SB_HIST_INDEXES') > 0
BEGIN
	drop view SB_HIST_INDEXES;
END	
go

IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'V' AND NAME = 'SB_HIST_INDEXES') = 0
BEGIN
						
	exec('CREATE VIEW [dbo].[SB_HIST_INDEXES] 
	AS 	
		SELECT 1 ID, 
				CONVERT(INT, dePara.CodigoExterno) as IDINDEX,
				CASE WHEN CAST(dePara.CodigoExterno AS INT) = 127 --IPCA
					THEN Convert(datetime, ''15/'' + convert(varchar(2),DATEPART(month,DATEADD(month,1,Data)))+ ''/'' +convert(varchar(4),DATEPART(year,DATEADD(month,1,Data))), 103)
					  WHEN CAST(dePara.CodigoExterno AS INT) = 126 --IGPM
					THEN Convert(datetime, ''01/'' + convert(varchar(2),DATEPART(month,Data))+ ''/'' +convert(varchar(4),DATEPART(year,Data)), 103)
				ELSE Data
				END as REFERENCEDATE,
				CASE WHEN CAST(dePara.CodigoExterno AS INT) = 127 --IPCA
					THEN Convert(datetime, ''15/'' + convert(varchar(2),DATEPART(month,DATEADD(month,1,Data)))+ ''/'' +convert(varchar(4),DATEPART(year,DATEADD(month,1,Data))), 103)
					 WHEN CAST(dePara.CodigoExterno AS INT) = 126 --IGPM
					THEN Convert(datetime, ''01/'' + convert(varchar(2),DATEPART(month,Data))+ ''/'' +convert(varchar(4),DATEPART(year,Data)), 103)
				ELSE Data
				END as RELEASEDATE,
				CASE WHEN CAST(dePara.CodigoExterno AS INT) IN (126, 127, 3, 4)  --IGPM / IPCA / IGPDI / INPC 
					THEN Valor
					ELSE NULL
				END as INDEXESINDEX,
				CASE WHEN CAST(dePara.CodigoExterno AS INT) NOT IN (126, 127, 3, 4) --IGPM / IPCA / IGPDI / INPC 
					THEN Valor
					ELSE 0
				END as INDEXESVALUE,
				1 as VALID 
			FROM [dbo].[CotacaoIndice] 
			INNER JOIN dePara ON (dePara.IdTipoDePara = (SELECT DISTINCT tpDePAra.IdTipoDePara 
					 FROM  dbo.DePara dePara 
					 INNER JOIN  dbo.TipoDePara tpDePara ON tpDePara.idTipoDePara = dePara.idTipoDePara
					 WHERE tpDePara.EnumTipoDePara = 1 /* Fincs - Indexador */						
						    and tpDePara.TipoCampo = 1) AND dePara.CodigoInterno = idIndice)')
END					
GO 

