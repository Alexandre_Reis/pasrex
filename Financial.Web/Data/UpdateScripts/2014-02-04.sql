﻿declare @data_versao char(10)
set @data_versao = '2014-02-04'
if exists (select 1 from VersaoSchema where DataVersao = @data_versao) return
insert into VersaoSchema VALUES(@data_versao)

insert into tipocarteirabolsa values (21016, 'Livre')
go
insert into tipocarteirabolsa values (23019, 'Garantias Gerais')
go
insert into tipocarteirabolsa values (23906, 'Garantia BMF')
go
insert into tipocarteirabolsa values (25018, 'Termos Flexiveis')
go
insert into tipocarteirabolsa values (27014, 'Cobertura Opções')
go
insert into tipocarteirabolsa values (22012, 'BTC Tomado')
go
insert into tipocarteirabolsa values (28010, 'BTC Doado')
go
insert into tipocarteirabolsa values (21059, 'Conta Margem')
go

--Menu de Detalhe de Tipo de Carteira de Bolsa
INSERT INTO [permissaomenu] ([IdGrupo],[IdMenu],[PermissaoLeitura],[PermissaoAlteracao],[PermissaoExclusao],[PermissaoInclusao]) SELECT idgrupo,2342,'S','S','S','S' from grupousuario where idgrupo <> 0
go



 