declare @data_versao char(10)
set @data_versao = '2013-08-26'
if exists (select 1 from VersaoSchema where DataVersao = @data_versao) return
insert into VersaoSchema VALUES(@data_versao)


alter table agentemercado add Website varchar(200) null
go
alter table agentemercado add TelOuvidoria varchar(25) null
go
alter table agentemercado add EmailOuvidoria varchar(100)
go
alter table agentemercado alter column Email varchar(100)
go
alter table agentemercado alter column Telefone varchar(25) null
go
alter table agentemercado alter column Fax varchar(25) null
go






