﻿
BEGIN /*Cursor para preencher o campo de idoperação, tanto na tabela posicaorendafixa quando na posicaorendafixahistorico*/
		
	IF EXISTS(
		SELECT 1   
		FROM
			PosicaoRendaFixaHistorico hist
		LEFT JOIN PosicaoRendaFixa p ON
			p.IdPosicao = hist.IdPosicao
		WHERE 
			hist.IdOperacao is null AND 
			p.idposicao is not null AND
			p.IdOperacao is not null
		UNION
		SELECT 1   
		FROM
			PosicaoRendaFixaHistorico hist
		LEFT JOIN PosicaoRendaFixaAbertura p ON
			p.IdPosicao = hist.IdPosicao
		WHERE 
			hist.IdOperacao is null AND 
			p.idposicao is not null AND
			p.IdOperacao is not null		
	)
	BEGIN
		RAISERROR('Base possui posições inconsistentes, conserte para prosseguir',15,1)
	END


	DECLARE 
		@IdCliente	INT, @IdTitulo	INT, @TipoOperacao INT, @Quantidade DECIMAL(25,12), @DataOperacao	DATETIME,
		@PUOperacao	DECIMAL(25,12), @TaxaOperacao	DECIMAL(25,16), @DataVolta	DATETIME, 
		@TaxaVolta	DECIMAL(8,4), @PUVolta	DECIMAL(25,12), @IdCustodia tinyint, @TipoNegociacao int,
		@IdPosicao int,	@IdOperacao int, @DataDia DATETIME

	--tem que declarar como table senão não tem como usar no output
	DECLARE @OperacaoInserted table( IdOperacao int )

	DECLARE RendaFixaCursor CURSOR FOR
		SELECT 
			IdCliente, IdTitulo, TipoOperacao, Quantidade, DataOperacao, PUOperacao, TaxaOperacao, 	
			DataVolta, TaxaVolta, PUVolta, IdCustodia, TipoNegociacao, IdPosicao, IdOperacao
		FROM 
			PosicaoRendaFixaHistorico a
		WHERE
			a.DataHistorico = (select min(DataHistorico) from PosicaoRendaFixaHistorico b where b.IdPosicao = a.IdPosicao) AND
			a.IdOperacao IS NULL
		FOR UPDATE OF IdOperacao
	
	OPEN RendaFixaCursor

	FETCH NEXT FROM RendaFixaCursor	
	INTO
		@IdCliente, @IdTitulo, @TipoOperacao, @Quantidade, @DataOperacao,
		@PUOperacao, @TaxaOperacao,		@DataVolta,	@TaxaVolta,	@PUVolta, @IdCustodia, 
		@TipoNegociacao, @IdPosicao, @IdOperacao	

	SELECT @DataDia = DataDia from Cliente where IdCliente = @IdCliente;
	WHILE(@@FETCH_STATUS = 0)
	BEGIN
		
		--garante que nesta tabela tenha sempre apenas o ultimo valor inserido na tabela de operação
		DELETE @OperacaoInserted
		
		INSERT INTO OperacaoRendaFixa(
			IdCliente,IdTitulo,TipoOperacao,Quantidade,DataOperacao,PUOperacao,TaxaOperacao,DataVolta, 
			TaxaVolta,PUVolta,IdCustodia,DataLiquidacao, Valor,	Fonte, TipoNegociacao, IdLiquidacao, 
			DataRegistro
		) 
		OUTPUT 
			inserted.IdOperacao
		INTO 
			@OperacaoInserted  
		VALUES(
			@IdCliente,	@IdTitulo, @TipoOperacao, @Quantidade, @DataDia, @PUOperacao, @TaxaOperacao, @DataVolta,
			@TaxaVolta,	@PUVolta, @IdCustodia, @DataOperacao, (@Quantidade*@PUOperacao), 0, @TipoNegociacao, 0, 
			getDate()
		)	

		UPDATE PosicaoRendaFixaAbertura 
		SET 
			IdOperacao = (select IdOperacao from @OperacaoInserted)
		WHERE 
			IdPosicao = @IdPosicao

		UPDATE PosicaoRendaFixa
		SET 
			IdOperacao = (select IdOperacao from @OperacaoInserted)
		WHERE 
			IdPosicao = @IdPosicao;

		UPDATE PosicaoRendaFixaHistorico 
		SET 
			IdOperacao = (select IdOperacao from @OperacaoInserted)
		WHERE 
			IdPosicao = @IdPosicao	


		FETCH NEXT FROM RendaFixaCursor	
		INTO
			@IdCliente,	@IdTitulo, @TipoOperacao, @Quantidade, @DataOperacao, @PUOperacao, @TaxaOperacao,	
			@DataVolta,	@TaxaVolta,	@PUVolta, @IdCustodia, @TipoNegociacao, @IdPosicao, @IdOperacao		
		
		SELECT 
			@DataDia = DataDia 
		FROM 
			Cliente 
		WHERE 
			IdCliente = @IdCliente;

	END	

	CLOSE RendaFixaCursor;
	DEALLOCATE RendaFixaCursor;

END

