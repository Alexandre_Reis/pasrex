declare @data_versao char(10)
set @data_versao = '2014-12-02'
if exists (select 1 from VersaoSchema where DataVersao = @data_versao) return
insert into VersaoSchema VALUES(@data_versao)

ALTER TABLE LiquidacaoRendaFixa ADD RendimentoNaoTributavel decimal(16,2) null
go
update LiquidacaoRendaFixa set RendimentoNaoTributavel = 0
go
ALTER TABLE LiquidacaoRendaFixa alter column RendimentoNaoTributavel decimal(16,2) not null
go
ALTER TABLE LiquidacaoRendaFixa ADD  CONSTRAINT DF_LiquidacaoRendaFixa_RendimentoNaoTributavel  DEFAULT ((0)) FOR RendimentoNaoTributavel
GO


