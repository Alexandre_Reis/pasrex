declare @data_versao char(10)
set @data_versao = '2013-10-15'
if exists (select 1 from VersaoSchema where DataVersao = @data_versao) return
insert into VersaoSchema VALUES(@data_versao)

alter table PrejuizoCotista add ValorPrejuizoOriginal decimal (16,2) null
go
update PrejuizoCotista set ValorPrejuizoOriginal = ValorPrejuizo
go
alter table PrejuizoCotista alter column ValorPrejuizoOriginal decimal (16,2) not null
go

alter table PrejuizoCotistaHistorico add ValorPrejuizoOriginal decimal (16,2) null
go
update PrejuizoCotistaHistorico set ValorPrejuizoOriginal = ValorPrejuizo
go
alter table PrejuizoCotistaHistorico alter column ValorPrejuizoOriginal decimal (16,2) not null
go


CREATE TABLE PrejuizoCotistaUsado(
	IdCarteira int NOT NULL,
	IdCarteiraCompensada int NOT NULL,
	IdCotista int NOT NULL,
	DataOriginal datetime NOT NULL,
	DataCompensacao datetime NOT NULL,		
	PrejuizoUsado decimal(16, 2) NOT NULL,
 CONSTRAINT PK_PrejuizoCotistaUsado PRIMARY KEY CLUSTERED 
(
	IdCarteira ASC,
	IdCarteiraCompensada ASC,
	IdCotista ASC,
	DataCompensacao ASC,
	DataOriginal ASC
)
)
GO

ALTER TABLE PrejuizoCotistaUsado  WITH CHECK ADD  CONSTRAINT FK_PrejuizoCotistaUsado_Carteira FOREIGN KEY(IdCarteira)
REFERENCES Carteira (IdCarteira)
ON DELETE CASCADE
GO

ALTER TABLE PrejuizoCotistaUsado  WITH CHECK ADD  CONSTRAINT FK_PrejuizoCotistaUsado_Cotista FOREIGN KEY(IdCotista)
REFERENCES Cotista (IdCotista)
ON DELETE CASCADE
GO

