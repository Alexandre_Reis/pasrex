declare @data_versao char(10)
set @data_versao = '2014-02-03'
if exists (select 1 from VersaoSchema where DataVersao = @data_versao) return
insert into VersaoSchema VALUES(@data_versao)

CREATE TABLE TabelaMTMRendaFixa(
	IdCliente int NOT NULL,
	IdTitulo int NOT NULL,
 CONSTRAINT PK_TabelaMTMRendaFixa PRIMARY KEY CLUSTERED 
(
	IdCliente ASC,
	IdTitulo ASC
)
)

GO

ALTER TABLE TabelaMTMRendaFixa  ADD  CONSTRAINT FK_TabelaMTMRendaFixa_Cliente FOREIGN KEY(IdCliente)
REFERENCES Cliente (IdCliente)
ON DELETE CASCADE
GO

ALTER TABLE TabelaMTMRendaFixa  ADD  CONSTRAINT FK_TabelaMTMRendaFixa_TituloRendaFixa FOREIGN KEY(IdTitulo)
REFERENCES TituloRendaFixa (IdTitulo)
ON DELETE CASCADE
GO

alter table tabelataxaadministracao add TipoGrupo int null
go

alter table tabelataxaadministracao add CodigoAtivo varchar(8)
go

drop table tipocarteirabolsa
go

CREATE TABLE TipoCarteiraBolsa
(
	IdCarteiraBolsa int NOT NULL,
	Descricao varchar(100)
 CONSTRAINT PK_TipoCarteiraBolsa PRIMARY KEY CLUSTERED 
(
	IdCarteiraBolsa ASC
)
)
go