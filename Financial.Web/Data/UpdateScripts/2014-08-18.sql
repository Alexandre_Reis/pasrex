declare @data_versao char(10)
set @data_versao = '2014-08-18'
if exists (select 1 from VersaoSchema where DataVersao = @data_versao) return
insert into VersaoSchema VALUES(@data_versao)

alter table TabelaEscalonamentoAdm alter column Percentual decimal (16,6) not null
go
