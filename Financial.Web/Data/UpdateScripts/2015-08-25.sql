declare @data_versao char(10)
set @data_versao = '2015-08-25'
if exists (select 1 from VersaoSchema where DataVersao = @data_versao) return
insert into VersaoSchema VALUES(@data_versao)

--Criacao Indice Portopar (relatorios gerenciais)

BEGIN TRANSACTION
IF EXISTS(select 1 from syscolumns where id = object_id('ST_RELATORIO'))
BEGIN
CREATE NONCLUSTERED INDEX
[IDX_PosicaoHistoricoCotista_IdCarteira_IdCotista] ON
[dbo].[PosicaoCotistaHistorico]
(
[IdCotista] ASC,
[IdCarteira] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB
= OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF,
ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]	
END
GO	
COMMIT TRANSACTION
