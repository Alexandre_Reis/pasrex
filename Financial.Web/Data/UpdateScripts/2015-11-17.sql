﻿declare @data_versao char(10)
set @data_versao = '2015-11-17'

/* 
*  caso este script ja tenha sido executado anteriormente 
*  não permite que seja executado novamente, avisa o usuario e 
*  não exibe nenhuma outra saida
*/
if exists(select * from VersaoSchema where DataVersao = @data_versao)
begin

    -- avisa o usuario em caso de erro
    raiserror('Atenção este script ja foi executado anteriormente.',16,1)

    -- não mostra a execução do script
    set nocount on

    -- não roda o script
    set noexec on 

end

begin transaction

     -- não permite que a transaction seja commitada em caso de erros
    set xact_abort on

    insert into VersaoSchema values(@data_versao)

    -- DDL	
	IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'AliquotaEspecifica') = 0
	Begin
		CREATE TABLE AliquotaEspecifica 
		(
			IdCarteira						INT                 NOT NULL,
			DataValidade					DateTime            NOT NULL,
			Taxa	 						Decimal(5,2)		NOT NULL,  
			CONSTRAINT PK_TaxaSerie 		primary key(IdCarteira, DataValidade)
		)
		
		ALTER TABLE AliquotaEspecifica ADD CONSTRAINT AliquotaEspecifica_Carteira_FK1 FOREIGN KEY (IdCarteira) REFERENCES Carteira(IdCarteira) on delete cascade
		
	end 
	
	IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'ExcecoesTributacaoIR') = 0
	Begin
		CREATE TABLE ExcecoesTributacaoIR
		(
			 IdExcecoesTributacaoIR INT PRIMARY KEY identity NOT NULL, 
			 Mercado int NOT NULL,
			 TipoClasseAtivo int NULL,
			 Ativo varchar(50) NULL,
			 TipoInvestidor int NOT NULL,	
			 IsencaoIR int NOT NULL,
			 AliquotaIR varchar(20) NULL,
			 IsencaoGanhoCapital int NOT NULL,
			 AliquotaIRGanhoCapital varchar(20) NULL,
			 CdAtivoBMF varchar(20)  NULL,
			 SerieBMF varchar(5) NULL,
			 IdTituloRendaFixa int FOREIGN KEY REFERENCES TituloRendaFixa(IdTitulo) NULL,
			 IdOperacaoSwap int FOREIGN KEY REFERENCES OperacaoSwap(IdOperacao) NULL,
			 IdCarteira int FOREIGN KEY REFERENCES Carteira(IdCarteira) NULL,
			 CdAtivoBolsa varchar(25) FOREIGN KEY REFERENCES AtivoBolsa(CdAtivoBolsa) NULL,
			 DataInicio DateTime NOT NULL,
			 DataFim DateTime NOT NULL,
			 CONSTRAINT FK_AtivoBMF FOREIGN KEY (CdAtivoBMF, SerieBMF) REFERENCES AtivoBMF(CdAtivoBMF, Serie)
		) 
	End
    --

    -- DML
	delete from PermissaoMenu where idMEnu = 7716
	insert into [dbo].[PermissaoMenu]
	SELECT [IdGrupo]
		  ,7716
		  ,[PermissaoLeitura]
		  ,[PermissaoAlteracao]
		  ,[PermissaoExclusao]
		  ,[PermissaoInclusao]
	  FROM [dbo].[PermissaoMenu]
	  where IdMenu = 7160;
	GO
	
	DELETE FROM PermissaoMenu WHERE IdMenu = 530
		INSERT INTO [dbo].[PermissaoMenu]
		SELECT [IdGrupo]
		  ,530
		  ,[PermissaoLeitura]
		  ,[PermissaoAlteracao]
		  ,[PermissaoExclusao]
		  ,[PermissaoInclusao]
		FROM [dbo].[PermissaoMenu]
	 WHERE IdMenu = 100
	 GO
    --

-- commita a transaction e volta as variaveis do sql server para o estado inicial
commit transaction
set noexec off
set nocount off
