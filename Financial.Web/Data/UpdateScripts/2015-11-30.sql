﻿declare @data_versao char(10)
set @data_versao = '2015-11-30'

/* 
*  caso este script ja tenha sido executado anteriormente 
*  não permite que seja executado novamente, avisa o usuario e 
*  não exibe nenhuma outra saida
*/
if exists(select * from VersaoSchema where DataVersao = @data_versao)
begin

    -- avisa o usuario em caso de erro
    raiserror('Atenção este script ja foi executado anteriormente.',16,1)

    -- não mostra a execução do script
    set nocount on

    -- não roda o script
    set noexec on 

end

begin transaction

     -- não permite que a transaction seja commitada em caso de erros
    set xact_abort on

    insert into VersaoSchema values(@data_versao)

    -- DDL	
	if (not exists (select * from sys.all_columns where name = 'Corretora' and OBJECT_NAME(object_id) = 'Carteira'))
	begin
		alter table carteira
		add Corretora char(1)
	end
	else
	begin
		if(exists(
			select 
				* 
			from 
				sys.all_columns 
			where 
				name = 'Corretora' and 
				OBJECT_NAME(object_id) = 'Carteira' and 
				system_type_id <> 175 and 
				max_length <> 1
		))
			raiserror('A coluna corretora da tabela Carteira esta com tamanho ou formato incorreto. Favor corrigir para prosseguir.',16,1);	
	end

	if  not exists (select 'x' from sysobjects o inner join syscolumns c on o.id = c.cdefault inner join sysobjects t on c.id = t.id where o.xtype = 'D' and c.name = 'Corretora' and t.name = 'Carteira')
	begin
	 	ALTER TABLE Carteira ADD default 'N' for Corretora;
	end

	if(not exists(select * from sysobjects where id = object_id('LiquidacaoTaxaCustodia')))
	begin	
		CREATE TABLE dbo.LiquidacaoTaxaCustodia
		(
			IdLiquidacaoTaxaCustodia int not null primary key identity,
			IdOperacao int NOT NULL,
			IdCliente int NOT NULL,
			DataHistorico Datetime not null,
			CustoCustodia decimal(16,2) NULL,
			TipoCustodia int NOT NULL,
		);
	END

	if (not exists (select * from sys.all_columns where name = 'DataVencimento' and OBJECT_NAME(object_id) = 'PessoaDocumento'))
	begin
		ALTER TABLE PessoaDocumento ADD DataVencimento datetime null
	end 

	if (not exists (select * from sys.all_columns where name = 'DataVencimentoCadastro' and OBJECT_NAME(object_id) = 'Pessoa'))
	begin
		ALTER TABLE Pessoa ADD DataVencimentoCadastro datetime null
	end 

	if (not exists (select * from sys.all_columns where name = 'AlertaCadastro' and OBJECT_NAME(object_id) = 'Pessoa'))
	begin
		ALTER TABLE Pessoa ADD AlertaCadastro char not null default 'N'
	end

	if (not exists (select * from sys.all_columns where name = 'EmailAlerta' and OBJECT_NAME(object_id) = 'Pessoa'))
	begin
		ALTER TABLE Pessoa ADD EmailAlerta varchar(200) null
	end
	
	if (not exists (select * from sys.all_columns where name = 'CdIso' and OBJECT_NAME(object_id) = 'Moeda'))
	begin
		ALTER TABLE Moeda ADD  CdIso char(3) null
	end

	
	if not exists(select * from sys.all_columns where Name = 'DataAtualizacao' and object_id = object_id('Cliente'))
	begin
		alter table cliente add DataAtualizacao Datetime not null default GetDAte()
	end 
	else
	begin
		--se a coluna ja existe verificar se é igual a definição senão disparar um erro
		if not exists(
			select * 
			from sys.all_columns a 
			inner join sys.types t 
				on a.user_type_id = t.user_type_id
			where a.object_id = object_id('Cliente')		
				and a.Name = 'DataAtualizacao'				
				and	a.user_type_id = TYPE_ID('datetime')	
				and a.is_nullable = 0
				and object_definition(a.default_object_id) like '%getdate%'
			)
		begin
			raiserror('Coluna Cliente.DataAtualizacao inconsistente, favor verificar',16,1)
		end
	end

	-- Se existir a trigger, apaga e cria de novo, pois triggers não guardam dados. 
	-- Devem ser sempre recriadas para garantir que estão consistente com o script.
	if exists(select * from sys.triggers where name = 'trg_cliente_DataAtualizacao')
	begin
		drop trigger trg_cliente_DataAtualizacao
	end

	go 

	-- trigger para preencher a data de ultima atualização 
	create trigger trg_cliente_DataAtualizacao on dbo.Cliente
	for update
	as
	begin	

		if (@@rowcount = 0)
		  return;

		set nocount on

		update Cliente 
		set 
		   DataAtualizacao = getdate()          
		where
			Cliente.IdCliente = (select IdCliente from inserted)

	end
	go

	-- Se existir a trigger, apaga e cria de novo, pois triggers não guardam dados. 
	-- Devem ser sempre recriadas para garantir que estão consistente com o script.
	if exists(select * from sys.triggers where name = 'trg_Pessoa_DataUltimaAlteracao')
	begin
		drop trigger trg_Pessoa_DataUltimaAlteracao
	end

	go

	-- trigger para preencher a data de ultima atualização 
	create trigger trg_Pessoa_DataUltimaAlteracao on dbo.Pessoa
	for update
	as
	begin	

		if (@@rowcount = 0)
		  return;

		set nocount on

		update Pessoa 
		set 
		   DataUltimaAlteracao = getdate()          
		where
			Pessoa.IdPessoa = (select IdPessoa from inserted)

	end
	go




	if not exists(select * from sys.all_columns where Name = 'AtivoCetip' and object_id = object_id('ClienteRendaFixa'))
	begin
		alter table ClienteRendaFixa add AtivoCetip char not null default 'S'
	end 
	else
	begin
		--se a coluna ja existe verificar se é igual a definição senão disparar um erro
		if not exists(
			select * 
			from sys.all_columns a 
			inner join sys.types t 
				on a.user_type_id = t.user_type_id
			where a.object_id = object_id('ClienteRendaFixa')		
				and a.Name = 'AtivoCetip'				
				and	a.user_type_id = TYPE_ID('char')	
				and a.is_nullable = 0
			)
		begin
			raiserror('Coluna ClienteRendaFixa.AtivoCetip inconsistente, favor verificar',16,1)
		end
	end
    --

    -- DML
	update Carteira set Corretora = 'N' where Corretora is null;
    --

-- commita a transaction e volta as variaveis do sql server para o estado inicial
commit transaction
set noexec off
set nocount off
