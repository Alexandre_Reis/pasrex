declare @data_versao char(10)
set @data_versao = '2013-12-23'
if exists (select 1 from VersaoSchema where DataVersao = @data_versao) return
insert into VersaoSchema VALUES(@data_versao)

alter table agentemercado add CodigoCetip varchar(9) null
go
