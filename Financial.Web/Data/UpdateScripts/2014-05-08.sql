declare @data_versao char(10)
set @data_versao = '2014-05-08'
if exists (select 1 from VersaoSchema where DataVersao = @data_versao) return
insert into VersaoSchema VALUES(@data_versao)

IF NOT EXISTS(select name from sys.indexes where object_id = object_id('PosicaoRendaFixa') AND name = 'IDX_IdTitulo')
CREATE NONCLUSTERED INDEX IDX_IdTitulo ON PosicaoRendaFixa 
(
	IdTitulo ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON)
GO

IF NOT EXISTS(select name from sys.indexes where object_id = object_id('PosicaoRendaFixaHistorico') AND name = 'IDX_IdTitulo')
CREATE NONCLUSTERED INDEX IDX_IdTitulo ON PosicaoRendaFixaHistorico 
(
	IdTitulo ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON)
GO

IF NOT EXISTS(select name from sys.indexes where object_id = object_id('PosicaoRendaFixaAbertura') AND name = 'IDX_IdTitulo')
CREATE NONCLUSTERED INDEX IDX_IdTitulo ON PosicaoRendaFixaAbertura 
(
	IdTitulo ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON)
GO