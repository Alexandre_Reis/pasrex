declare @data_versao char(10)
set @data_versao = '2015-01-06'
if exists (select 1 from VersaoSchema where DataVersao = @data_versao) return
insert into VersaoSchema VALUES(@data_versao)

CREATE TABLE TipoConta(
	IdTipoConta int IDENTITY(1,1) NOT NULL,
	Descricao varchar(100) NOT NULL,
 CONSTRAINT PK_TipoConta PRIMARY KEY CLUSTERED 
(
	IdTipoConta ASC
)
)
go

CREATE TABLE GrupoConta(
	IdGrupo int IDENTITY(1,1) NOT NULL,
	Descricao varchar(100) NOT NULL,
 CONSTRAINT PK_GrupoConta PRIMARY KEY CLUSTERED 
(
	IdGrupo ASC
)
)
go

alter table banco add CodigoExterno varchar(12) null
go
alter table banco add DescricaoCodigoExterno varchar(20) null
go

alter table agencia add IdLocal smallint null
go
update agencia set IdLocal = 1
go
alter table agencia alter column IdLocal smallint not null
go

alter table contacorrente alter column numero varchar(50)
go
alter table contacorrente add IdLocal smallint null
go
alter table contacorrente add DescricaoCodigo varchar(20) null
go

alter table contacorrente drop column TipoConta
go
alter table contacorrente add IdTipoConta int null
go
alter table contacorrente add IdGrupo int null
go




