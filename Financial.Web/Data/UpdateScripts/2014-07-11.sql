declare @data_versao char(10)
set @data_versao = '2014-07-11'
if exists (select 1 from VersaoSchema where DataVersao = @data_versao) return
insert into VersaoSchema VALUES(@data_versao)


CREATE TABLE CarteiraMae(
	IdCarteiraFilha int NOT NULL,
	IdCarteiraMae int NOT NULL,
 CONSTRAINT PK_CarteiraMae PRIMARY KEY CLUSTERED 
(
	IdCarteiraFilha ASC,
	IdCarteiraMae ASC
)
)