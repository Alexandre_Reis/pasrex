declare @data_versao char(10)
set @data_versao = '2014-05-26'
if exists (select 1 from VersaoSchema where DataVersao = @data_versao) return
insert into VersaoSchema VALUES(@data_versao)

--Menu Tabela Pessoa Vinculada
INSERT INTO [permissaomenu] ([IdGrupo],[IdMenu],[PermissaoLeitura],[PermissaoAlteracao],[PermissaoExclusao],[PermissaoInclusao]) 
SELECT idgrupo,1220,'S','S','S','S' from grupousuario where idgrupo <> 0