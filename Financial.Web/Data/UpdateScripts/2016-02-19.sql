﻿declare @data_versao char(10)
set @data_versao = '2016-02-19'

/* 
*  caso este script ja tenha sido executado anteriormente 
*  não permite que seja executado novamente, avisa o usuario e 
*  não exibe nenhuma outra saida
*/
if exists(select * from VersaoSchema where DataVersao = @data_versao)
begin
	
	-- avisa o usuario em caso de erro
	raiserror('Atenção este script ja foi executado anteriormente.',16,1)

	
	-- não mostra a execução do script
	set nocount on 
	
	-- não roda o script
	set noexec on 
	

end

BEGIN TRANSACTION
	
	--garante que só sera commitado caso não aconteça nenhum erro
	set xact_abort on	
	
	insert into VersaoSchema values(@data_versao)
	
	IF NOT EXISTS(SELECT ID FROM CONFIGURACAO WHERE ID = 1060 )
	BEGIN
		INSERT INTO CONFIGURACAO(Id,Descricao,ValorNumerico,ValorTexto)VALUES(1060,'IntegraGalgoPlCota',0,'')
	END  

	if (not exists (select * from sys.all_columns where name = 'ExportaGalgo' and OBJECT_NAME(object_id) = 'Carteira'))
	begin
		ALTER TABLE Carteira ADD  ExportaGalgo char(1) not null default 'N'
	end
	
	if(not exists(select * from sysobjects where id = object_id('HistoricoCotaGalgo')))
	begin
		
		CREATE TABLE [dbo].[HistoricoCotaGalgo](
			[Data] [datetime] NOT NULL,
			[IdCarteira] [int] NOT NULL,
			[Cota] [decimal](28, 12) NOT NULL,
			[PL] [decimal](16, 2) NOT NULL,
			[DataImportacao] [datetime] NOT NULL,
			[UsuarioImportacao] [varchar](50) NOT NULL, 
			[Aprovar] [int] NOT NULL, 
			[DataAprovacao]  [datetime] NULL,
			[UsuarioAprovacao]  [varchar](50) NULL, 
		 CONSTRAINT [HistoricoCotaGalgo_PK] PRIMARY KEY CLUSTERED 
		(
			[Data] ASC,
			[IdCarteira] ASC
		)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
		) ON [PRIMARY]

	
		ALTER TABLE [dbo].[HistoricoCotaGalgo]  WITH CHECK ADD  CONSTRAINT [Carteira_HistoricoCotaGalgo_FK1] FOREIGN KEY([IdCarteira])
		REFERENCES [dbo].[Carteira] ([IdCarteira])
	
	end
		
	if exists(select 1 from syscolumns where id = object_id('CadastroComplementar') and name = 'ValorCampo')
	begin
		ALTER TABLE CadastroComplementar ALTER COLUMN ValorCampo VARCHAR(255) NOT NULL
	END

	IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'CategoriaMovimentacao') = 0
	begin
		CREATE TABLE CategoriaMovimentacao
		(
			IdCategoriaMovimentacao	INT				NOT NULL	Primary Key Identity,
			CodigoCategoria			varchar(20)		NOT NULL,
			Descricao				varchar(100)	NULL		
		)	
	END	


	if (not exists (select * from syscolumns where name = 'IdCategoriaMovimentacao' and id = object_id('OperacaoBMF')))
	begin
		ALTER TABLE OperacaoBMF ADD IdCategoriaMovimentacao INT null;	
		ALTER TABLE OperacaoBMF	ADD CONSTRAINT OperacaoBMF_CatMovimentacao_FK1 FOREIGN KEY (IdCategoriaMovimentacao) REFERENCES CategoriaMovimentacao(IdCategoriaMovimentacao) on delete SET NULL;
	end


	if (not exists (select * from syscolumns where name = 'IdCategoriaMovimentacao' and id = object_id('OperacaoRendaFixa')))
	begin
		ALTER TABLE OperacaoRendaFixa ADD IdCategoriaMovimentacao INT null;	
		ALTER TABLE OperacaoRendaFixa ADD CONSTRAINT OperacaoRF_CatMovimentacao_FK1 FOREIGN KEY (IdCategoriaMovimentacao) REFERENCES CategoriaMovimentacao(IdCategoriaMovimentacao) on delete SET NULL;
	end

	if (not exists (select * from syscolumns where name = 'IdCategoriaMovimentacao' and id = object_id('OperacaoSwap')))
	begin
		ALTER TABLE OperacaoSwap ADD IdCategoriaMovimentacao INT null;	
		ALTER TABLE OperacaoSwap ADD CONSTRAINT OperacaoSwap_CatMovimentacao_FK1 FOREIGN KEY (IdCategoriaMovimentacao) REFERENCES CategoriaMovimentacao(IdCategoriaMovimentacao) on delete SET NULL;
	end

	if (not exists (select * from syscolumns where name = 'IdCategoriaMovimentacao' and id = object_id('OperacaoBolsa')))
	begin
		ALTER TABLE OperacaoBolsa ADD IdCategoriaMovimentacao INT null;	
		ALTER TABLE OperacaoBolsa ADD CONSTRAINT OperacaoBolsa_CatMovimentacao_FK1 FOREIGN KEY (IdCategoriaMovimentacao) REFERENCES CategoriaMovimentacao(IdCategoriaMovimentacao) on delete SET NULL;
	end

	if (not exists (select * from syscolumns where name = 'IdCategoriaMovimentacao' and id = object_id('OperacaoFundo')))
	begin
		ALTER TABLE OperacaoFundo ADD IdCategoriaMovimentacao INT null;	
		ALTER TABLE OperacaoFundo ADD CONSTRAINT OperacaoFundo_CatMovimentacao_FK1 FOREIGN KEY (IdCategoriaMovimentacao) REFERENCES CategoriaMovimentacao(IdCategoriaMovimentacao) on delete SET NULL;
	end

	if (not exists (select * from syscolumns where name = 'IdCategoriaMovimentacao' and id = object_id('OperacaoFundoAux')))
	begin
		ALTER TABLE OperacaoFundoAux ADD IdCategoriaMovimentacao INT null;	
		ALTER TABLE OperacaoFundoAux ADD CONSTRAINT OperacaoFundoAux_CatMovimentacao_FK1 FOREIGN KEY (IdCategoriaMovimentacao) REFERENCES CategoriaMovimentacao(IdCategoriaMovimentacao) on delete SET NULL;
	end

	if (not exists (select * from syscolumns where name = 'IdCategoriaMovimentacao' and id = object_id('OrdemBMF')))
	begin
		ALTER TABLE OrdemBMF ADD IdCategoriaMovimentacao INT null;	
		ALTER TABLE OrdemBMF ADD CONSTRAINT OrdemBMF_CatMovimentacao_FK1 FOREIGN KEY (IdCategoriaMovimentacao) REFERENCES CategoriaMovimentacao(IdCategoriaMovimentacao) on delete SET NULL;
	end


	if (not exists (select * from syscolumns where name = 'IdCategoriaMovimentacao' and id = object_id('OrdemRendaFixa')))
	begin
		ALTER TABLE OrdemRendaFixa ADD IdCategoriaMovimentacao INT null;	
		ALTER TABLE OrdemRendaFixa ADD CONSTRAINT OrdemRF_CatMovimentacao_FK1 FOREIGN KEY (IdCategoriaMovimentacao) REFERENCES CategoriaMovimentacao(IdCategoriaMovimentacao) on delete SET NULL;
	end


	if (not exists (select * from syscolumns where name = 'IdCategoriaMovimentacao' and id = object_id('OrdemBolsa')))
	begin
		ALTER TABLE OrdemBolsa ADD IdCategoriaMovimentacao INT null;	
		ALTER TABLE OrdemBolsa ADD CONSTRAINT OrdemBolsa_CatMovimentacao_FK1 FOREIGN KEY (IdCategoriaMovimentacao) REFERENCES CategoriaMovimentacao(IdCategoriaMovimentacao) on delete SET NULL;
	end

	if (not exists (select * from syscolumns where name = 'IdCategoriaMovimentacao' and id = object_id('OrdemFundo')))
	begin
		ALTER TABLE OrdemFundo ADD IdCategoriaMovimentacao INT null;	
		ALTER TABLE OrdemFundo ADD CONSTRAINT OrdemFundo_CatMovimentacao_FK1 FOREIGN KEY (IdCategoriaMovimentacao) REFERENCES CategoriaMovimentacao(IdCategoriaMovimentacao) on delete SET NULL;
	end

	if (select count(1) from CadastroComplementarCampos where TipoCadastro = -8 and NomeCampo = 'GrupoEconomicoLamina') = 0
	begin
		INSERT INTO CadastroComplementarCampos (TipoCadastro, NomeCampo, DescricaoCampo, ValorDefault, TipoCampo, CampoObrigatorio, Tamanho, CasasDecimais)
		VALUES(-8, 'GrupoEconomicoLamina', 'Grupo economico no qual a carteira faz parte', null, -2, 'N', 40 /*Alterar caso o limite de char no campo seja menor*/, 0)
	END	
		
	if (select count(1) from CadastroComplementarCampos where TipoCadastro = -8 and NomeCampo = 'NomeFundoLamina') = 0
	begin
		INSERT INTO CadastroComplementarCampos (TipoCadastro, NomeCampo, DescricaoCampo, ValorDefault, TipoCampo, CampoObrigatorio, Tamanho, CasasDecimais)
		VALUES(-8, 'NomeFundoLamina', 'Grupo economico no qual a carteira faz parte', null, -2, 'N', 255 /*Alterar caso o limite de char no campo seja menor*/, 0)	
	END	

	if (select count(1) from CadastroComplementarCampos where TipoCadastro = -8 and NomeCampo = 'ApelidoReduzidoRentMensal') = 0
	begin
		INSERT INTO CadastroComplementarCampos (TipoCadastro, NomeCampo, DescricaoCampo, ValorDefault, TipoCampo, CampoObrigatorio, Tamanho, CasasDecimais)
		VALUES(-8, 'ApelidoReduzidoRentMensal', 'Apelido reduzido que será usado no relatório de rent.mensal', null, -2, 'N', 40 /*Alterar caso o limite de char no campo seja menor*/, 0)	
	END		
	
	IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'Rentabilidade') = 0
	begin
		CREATE TABLE [Rentabilidade]
		(
			[IdRentabilidade] [int] IDENTITY(1,1) NOT NULL,
			[Data] [datetime] NOT NULL,
			[TipoAtivo] [int] NOT NULL,
			[IdCliente] [int] NOT NULL,
			[CodigoAtivo] [varchar](255) NULL,
			[CodigoMoedaAtivo] [varchar](50) NULL,
			[PatrimonioInicialMoedaAtivo] [decimal](25, 12) NULL,
			[QuantidadeInicialAtivo] [decimal](25, 12) NULL,
			[ValorFinanceiroEntradaMoedaAtivo] [decimal](25, 2) NULL,
			[QuantidadeTotalEntradaAtivo] [decimal](25, 12) NULL,
			[ValorFinanceiroSaidaMoedaAtivo] [decimal](25, 2) NULL,
			[QuantidadeTotalSaidaAtivo] [decimal](25, 12) NULL,
			[ValorFinanceiroRendasMoedaAtivo] [decimal](25, 2) NULL,
			[PatrimonioFinalMoedaAtivo] [decimal](25, 12) NULL,
			[QuantidadeFinalAtivo] [decimal](25, 12) NULL,
			[RentabilidadeMoedaAtivo] [decimal](25, 12) NULL,
			[CodigoMoedaPortfolio] [varchar](10) NULL,
			[PatrimonioInicialMoedaPortfolio] [decimal](25, 12) NULL,
			[ValorFinanceiroEntradaMoedaPortfolio] [decimal](25, 2) NULL,
			[ValorFinanceiroSaidaMoedaPortfolio] [decimal](25, 2) NULL,
			[ValorFinanceiroRendasMoedaPortfolio] [decimal](25, 2) NULL,
			[PatrimonioFinalMoedaPortfolio] [decimal](25, 2) NULL,
			[RentabilidadeMoedaPortfolio] [decimal](25, 12) NULL,
			[IdTituloRendaFixa] [int] NULL,
			[IdOperacaoSwap] [int] NULL,
			[IdCarteira] [int] NULL,
			[CdAtivoBolsa] [varchar](20) NULL,
			[CdAtivoBMF] [varchar](20) NULL,
			[SerieBMF] [varchar](5) NULL,
			[PatrimonioInicialBrutoMoedaPortfolio] [decimal](28, 2) NULL,
			[PatrimonioInicialBrutoMoedaAtivo] [decimal](28, 2) NULL,
			[PatrimonioFinalBrutoMoedaPortfolio] [decimal](28, 2) NULL,
			[PatrimonioFinalBrutoMoedaAtivo] [decimal](28, 2) NULL,
			[CotaGerencial] [decimal](28, 12) NULL,
			[RentabilidadeBrutaDiariaMoedaAtivo] [decimal](25, 12) NULL,
			[RentabilidadeBrutaAcumMoedaAtivo] [decimal](25, 12) NULL,
			[RentabilidadeBrutaDiariaMoedaPortfolio] [decimal](25, 12) NULL,
			[RentabilidadeBrutaAcumMoedaPortfolio] [decimal](25, 12) NULL,
			[PatrimonioInicialGrossUpMoedaAtivo] [decimal](28, 2) NULL,
			[PatrimonioInicialGrossUpMoedaPortfolio] [decimal](28, 2) NULL,
			[PatrimonioFinalGrossUpMoedaAtivo] [decimal](28, 2) NULL,
			[PatrimonioFinalGrossUpMoedaPortfolio] [decimal](28, 2) NULL,
			[RentabilidadeAtivosIcentivados] [decimal](25, 12) NULL,
			[RentabilidadeAtivosTributados] [decimal](25, 12) NULL,
			[RentabilidadeGrossUpDiariaMoedaAtivo] [decimal](25, 12) NULL,
			[RentabilidadeGrossUpAcumMoedaAtivo] [decimal](25, 12) NULL,
			[RentabilidadeGrossUpDiariaMoedaPortfolio] [decimal](25, 12) NULL,
			[RentabilidadeGrossUpAcumMoedaPortfolio] [decimal](25, 12) NULL,
		PRIMARY KEY CLUSTERED 
		(
			[IdRentabilidade] ASC
		)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
		) ON [PRIMARY]
	END	


	IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'AtivoMercadoFiltro') = 0
	Begin
		Create Table AtivoMercadoFiltro
		(
				IdAtivoMercadoFiltro INT 	PRIMARY KEY identity NOT NULL, 
				CompositeKey	VARCHAR(50),
				IdAtivo			VARCHAR(50),
				Descricao		VARCHAR(255),
				IdCliente		INT,
				TipoMercado		INT
		)
	END	


	if not exists(select 1 from syscolumns where id = object_id('PosicaoRendaFixa') and name = 'ValorBrutoGrossUp')
	BEGIN
		ALTER TABLE PosicaoRendaFixa ADD ValorBrutoGrossUp decimal(28,10) NULL default 0;
	END


	if not exists(select 1 from syscolumns where id = object_id('PosicaoRendaFixaHistorico') and name = 'ValorBrutoGrossUp')
	BEGIN
		ALTER TABLE PosicaoRendaFixaHistorico ADD ValorBrutoGrossUp decimal(28,10) NOT NULL default 0;
	END


	if not exists(select 1 from syscolumns where id = object_id('PosicaoRendaFixaAbertura') and name = 'ValorBrutoGrossUp')
	BEGIN
		ALTER TABLE PosicaoRendaFixaAbertura ADD ValorBrutoGrossUp decimal(28,10) NOT NULL default 0;
	END

		
	if not exists(select 1 from syscolumns where id = object_id('CalculoAdministracao') and name = 'ValorBase')
	BEGIN
		ALTER TABLE CalculoAdministracao ADD ValorBase DECIMAL(16,2) NULL
	END 

	
	if not exists(select 1 from syscolumns where id = object_id('CalculoAdministracaoHistorico') and name = 'ValorBase')
	BEGIN
		ALTER TABLE CalculoAdministracaoHistorico ADD ValorBase DECIMAL(16,2) NULL
	END

	
	if exists(select * from sys.triggers where name = 'trg_Pessoa_DataUltimaAlteracao')
	begin
		drop trigger trg_Pessoa_DataUltimaAlteracao
	end

	
	UPDATE pessoa set CPFCNPJ = SUBSTRING(CPFCNPJ, len(CPFCNPJ) - 10, 11) where tipo =1 and len(CPFCNPJ) > 11
	UPDATE pessoa set CPFCNPJ = SUBSTRING(CPFCNPJ, len(CPFCNPJ) - 13, 14) where tipo =2 and len(CPFCNPJ) > 14	
	
-- commita a transaction e volta as variaveis do sql server para o estado inicial
commit transaction
set noexec off
set nocount off
