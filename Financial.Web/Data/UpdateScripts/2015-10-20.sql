﻿declare @data_versao char(10)
set @data_versao = '2015-10-20'
if exists (select 1 from VersaoSchema where DataVersao = @data_versao) return
BEGIN TRANSACTION


	--garante que só sera commitado caso não aconteça nenhum erro
	set xact_abort on
	insert into VersaoSchema values(@data_versao);

	ALTER TABLE Lamina ADD ExisteCarencia CHAR(1) NULL
	ALTER TABLE Lamina ADD ExisteTaxaEntrada CHAR(1) NULL
	ALTER TABLE Lamina ADD ExisteTaxaSaida CHAR(1) NULL
	
COMMIT TRANSACTION

GO	
