declare @data_versao char(10)
set @data_versao = '2013-12-19'
if exists (select 1 from VersaoSchema where DataVersao = @data_versao) return
insert into VersaoSchema VALUES(@data_versao)

--Menu de Batimento
INSERT INTO [dbo].[permissaomenu] ([IdGrupo],[IdMenu],[PermissaoLeitura],[PermissaoAlteracao],[PermissaoExclusao],[PermissaoInclusao])VALUES(1,3910,'S','S','S','S')

