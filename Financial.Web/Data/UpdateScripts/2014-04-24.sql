declare @data_versao char(10)
set @data_versao = '2014-04-24'
if exists (select 1 from VersaoSchema where DataVersao = @data_versao) return
insert into VersaoSchema VALUES(@data_versao)

alter table pessoa add SituacaoLegal tinyint null
go

CREATE TABLE [PessoaDocumento](
	[IdDocumento] [int] IDENTITY(1,1) NOT NULL,
	[IdPessoa] [int] NULL,
	[TipoDocumento] [tinyint] NULL,
	[NumeroDocumento] [varchar](13) NOT NULL,
	[DataExpedicao] [datetime] NULL,
	[OrgaoEmissor] [varchar](7) NULL,
	[UFEmissor] [char](2) NULL,
 CONSTRAINT [PK_PessoaDocumento] PRIMARY KEY CLUSTERED 
(
	[IdDocumento] ASC
)
)

ALTER TABLE [PessoaDocumento]  WITH CHECK ADD  CONSTRAINT [FK_PessoaDocumento_Pessoa] FOREIGN KEY([IdPessoa])
REFERENCES [Pessoa] ([IdPessoa])


ALTER TABLE [PessoaDocumento] CHECK CONSTRAINT [FK_PessoaDocumento_Pessoa]


ALTER TABLE [PessoaDocumento]  WITH CHECK ADD  CONSTRAINT [FK_PessoaDocumento_PessoaDocumento] FOREIGN KEY([IdDocumento])
REFERENCES [PessoaDocumento] ([IdDocumento])


ALTER TABLE [PessoaDocumento] CHECK CONSTRAINT [FK_PessoaDocumento_PessoaDocumento]

insert into pessoadocumento
(idpessoa, tipodocumento, numerodocumento, DataExpedicao, orgaoemissor, ufemissor)
select idpessoa, 12, substring(numeroRG, 0, 12), dataemissaoRG, 'SSP', 'SP'
from pessoa where numeroRG is not null and numeroRG <> ''
go


alter table pessoa drop column numeroRG
go
alter table pessoa drop column emissorRG
go
alter table pessoa drop column dataemissaoRG
go

drop table SuitabilityResposta
go

CREATE TABLE SuitabilityResposta(
	IdResposta int IDENTITY(1,1) NOT NULL,
	Data datetime NOT NULL,
	IdPessoa int NOT NULL,
	IdQuestao int NOT NULL,
	IdOpcaoEscolhida int NOT NULL,
	RiskPoints int NOT NULL,
 CONSTRAINT PK_SuitabilityResposta PRIMARY KEY CLUSTERED 
(
	IdResposta ASC
)
)

GO

ALTER TABLE SuitabilityResposta  WITH CHECK ADD  CONSTRAINT FK_SuitabilityResposta_SuitabilityOpcao FOREIGN KEY(IdOpcaoEscolhida)
REFERENCES SuitabilityOpcao (IdOpcao)
ON DELETE CASCADE
GO

ALTER TABLE SuitabilityResposta  WITH CHECK ADD  CONSTRAINT FK_SuitabilityResposta_SuitabilityQuestao FOREIGN KEY(IdQuestao)
REFERENCES SuitabilityQuestao (IdQuestao)
GO

ALTER TABLE SuitabilityResposta  WITH CHECK ADD  CONSTRAINT FK_SuitabilityResposta_Pessoa FOREIGN KEY(IdPessoa)
REFERENCES Pessoa (IdPessoa)
ON DELETE CASCADE
GO


