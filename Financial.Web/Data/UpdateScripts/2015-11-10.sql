﻿declare @data_versao char(10)
set @data_versao = '2015-11-10'

/* 
*  caso este script ja tenha sido executado anteriormente 
*  não permite que seja executado novamente, avisa o usuario e 
*  não exibe nenhuma outra saida
*/
if exists(select * from VersaoSchema where DataVersao = @data_versao)
begin

    -- avisa o usuario em caso de erro
    raiserror('Atenção este script ja foi executado anteriormente.',16,1)

    -- não mostra a execução do script
    set nocount on

    -- não roda o script
    set noexec on 

end

begin transaction

     -- não permite que a transaction seja commitada em caso de erros
    set xact_abort on

    insert into VersaoSchema values(@data_versao)

    -- DDL	
	if (not exists (select * from sys.all_columns where name = 'Corretora' and OBJECT_NAME(object_id) = 'Carteira'))
	begin
		alter table carteira
		add Corretora char(1)
	end
	else
	begin
		if(exists(
			select 
				* 
			from 
				sys.all_columns 
			where 
				name = 'Corretora' and 
				OBJECT_NAME(object_id) = 'Carteira' and 
				system_type_id <> 175 and 
				max_length <> 1
		))
			raiserror('A coluna corretora da tabela Carteira esta com tamanho ou formato incorreto. Favor corrigir para prosseguir.',16,1);	
	end
    --

    -- DML
    --

-- commita a transaction e volta as variaveis do sql server para o estado inicial
commit transaction
set noexec off
set nocount off
