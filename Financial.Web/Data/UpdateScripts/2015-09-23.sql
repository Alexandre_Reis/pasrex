﻿declare @data_versao char(10)
set @data_versao = '2015-09-23'
if exists (select 1 from VersaoSchema where DataVersao = @data_versao) return
BEGIN TRANSACTION


	--garante que só sera commitado caso não aconteça nenhum erro
	set xact_abort on
	insert into VersaoSchema values(@data_versao);

	--insere dados de configuração para que esses possam ser salvos posteriormente na tela
	insert into Configuracao(Id,Descricao,ValorTexto) values(1050,'NomeInstituicao','');
	insert into Configuracao(Id,Descricao,ValorTexto) values(1051,'CodInstituicao','');
	insert into Configuracao(Id,Descricao,ValorTexto) values(1052,'ResponsavelInstituicao','');
	insert into Configuracao(Id,Descricao,ValorTexto) values(1053,'TelefoneInstituicao','');
	insert into Configuracao(Id,Descricao,ValorTexto) values(1054,'EmailInstituicao','');
	
	--adiciona os campos ne necessarios para preencher o ranking anbima
	ALTER TABLE CARTEIRA
	ADD 
		ControladoriaAtivo char(1),
		ControladoriaPassivo char(1),
		MesmoConglomerado char(1),
		CategoriaAnbima int null,
		Contratante int references AgenteMercado(IdAgente)


--Adicionar atributo
if not exists(select 1 from syscolumns where id = object_id('SuitabilityQuestaoHistorico') and name = 'Validacao')
BEGIN
	ALTER TABLE SuitabilityQuestaoHistorico ADD Validacao varchar(100) null
END
GO

IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'SuitabilityOpcaoHistorico') > 0
begin
	DROP TABLE SuitabilityOpcaoHistorico;

	CREATE TABLE SuitabilityOpcaoHistorico
	(
		[DataHistorico] [datetime] NOT NULL,
		[IdQuestao] [int] NOT NULL,
		[Questao] [varchar](2000) NULL,
		[IdOpcao] [int] NOT NULL,
		[Opcao] [varchar](2000) NULL,
		[Pontos] [int] NOT NULL,
		[Tipo] [varchar](10) NOT NULL,
		CONSTRAINT PK_SuitabilityOpcaoHistorico primary key(DataHistorico, IdQuestao, IdOpcao)
	)
end
GO

IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'SuitabilityResposta') > 0
begin
	DROP TABLE SuitabilityResposta;
end
go

if exists(select 1 from sysconstraints where object_name(id) = 'SuitabilityRespostaCotistaDetalhe' and object_name(constid) = 'SuitabilityRespostaCotistaDetalhe_SuitabilityRespostaCotista_FK')
begin
	ALTER TABLE SuitabilityRespostaCotistaDetalhe DROP CONSTRAINT SuitabilityRespostaCotistaDetalhe_SuitabilityRespostaCotista_FK
end
go

IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'SuitabilityParametroPerfilHistorico') > 0
begin

	drop table SuitabilityParametroPerfilHistorico;

	CREATE TABLE SuitabilityParametroPerfilHistorico
	(
		[DataHistorico] [datetime] NOT NULL,
		IdParametroPerfil int NOT NULL,
		IdValidacao int NOT NULL,
		Validacao varchar(100) NULL,
		IdPerfilInvestidor int NOT NULL,
		PerfilInvestidor varchar(20) NOT NULL,
		Definicao text NULL,
		FaixaPontuacao int NULL,
		[Tipo] [varchar](10) NOT NULL,
		CONSTRAINT PK_SuitabilityParametroPerfilHistorico primary key(DataHistorico, IdParametroPerfil)
	)
end
GO

IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'CarteiraSuitabilityHistorico') > 0
begin

	drop TABLE CarteiraSuitabilityHistorico

	CREATE TABLE CarteiraSuitabilityHistorico
	(
		[DataHistorico] [datetime] NOT NULL,
		[IdCarteira] int NOT NULL,
		[IdPerfilRisco] int NULL,
		[PerfilRisco] varchar(100) NULL,
		[Tipo] [varchar](10) NOT NULL,
		CONSTRAINT PK_CarteiraSuitabilityHistorico primary key(DataHistorico, IdCarteira)
	)	
end
GO

IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'SuitabilityApuracaoRiscoHistorico') > 0
begin

	drop TABLE SuitabilityApuracaoRiscoHistorico

	CREATE TABLE SuitabilityApuracaoRiscoHistorico
	(
		[DataHistorico] [datetime] NOT NULL,
		[IdApuracaoRisco] int NOT NULL,
		[IdValidacao] int NOT NULL,
		[Validacao] varchar(100) NULL,
		[IdPerfilCarteira] int NOT NULL,
		[PerfilCarteira] varchar(100) NULL,
		[PerfilRiscoFundo] varchar(100) NOT NULL,
		[Criterio] varchar(50) NOT NULL,
		[Percentual] int NOT NULL default 0,
		[Tipo] [varchar](10) NOT NULL,
		CONSTRAINT PK_SuitabilityApuracaoRiscoHistorico primary key(DataHistorico, IdApuracaoRisco)
	)
end
GO

IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'SuitabilityParametrosWorkflowHistorico') > 0
begin

	drop TABLE SuitabilityParametrosWorkflowHistorico

	CREATE TABLE SuitabilityParametrosWorkflowHistorico
	(
		[DataHistorico] [datetime] NOT NULL,
		[IdParametrosWorkflow] int NOT NULL default 1,
		[AtivaWorkFlow] char(1) NOT NULL default 'N',
		[VerificaEnquadramento] varchar(20) NULL,
		[ResponsavelNome] varchar(50) NULL,
		[ResponsavelEmail] varchar(100) NULL,
		[ValorPeriodo] int NULL,
		[GrandezaPeriodo] varchar(20) NULL,
		[Tipo] [varchar](10) NOT NULL,
		CONSTRAINT PK_SuitabilityParametrosWorkflowHistorico primary key(DataHistorico, IdParametrosWorkflow)
	)
	
end
GO

IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'SuitabilityEventosHistorico') > 0
begin

	drop TABLE SuitabilityEventosHistorico;

	CREATE TABLE SuitabilityEventosHistorico(
		[DataHistorico] [datetime] NOT NULL,
		[IdSuitabilityEventos] [int] NOT NULL,
		[Descricao] [varchar](100) NOT NULL,
		[IdMensagem] [int] NULL,
		[Mesagem] text NULL,
		[Tipo] [varchar](10) NOT NULL,
	 CONSTRAINT [PK_SuitabilityEventosHistorico] PRIMARY KEY (DataHistorico, IdSuitabilityEventos))
end
go

if (select count(1) from SuitabilityEventosHistorico) = 0
begin
	insert into SuitabilityEventosHistorico values (getdate(),1, 'Apuração de novo perfil de Investidor (Portal)', null, null, 'Insert');
	insert into SuitabilityEventosHistorico values (getdate(),2, 'Declaração sobre as informações prestadas (Portal)', null, null, 'Insert');
	insert into SuitabilityEventosHistorico values (getdate(),3, 'Aceite do perfil apurado', null, null, 'Insert');
	insert into SuitabilityEventosHistorico values (getdate(),4, 'Aceite do perfil apurado (Portal)', null, null, 'Insert');
	insert into SuitabilityEventosHistorico values (getdate(),5, 'Aceite do tipo de dispensa', null, null, 'Insert');
	insert into SuitabilityEventosHistorico values (getdate(),6, 'Aceite do tipo de dispensa (Portal)', null, null, 'Insert');
	insert into SuitabilityEventosHistorico values (getdate(),7, 'Recusa do preenchimento do formulário”', null, null, 'Insert');
	insert into SuitabilityEventosHistorico values (getdate(),8, 'Recusa do preenchimento do formulário (Portal)', null, null, 'Insert');
	insert into SuitabilityEventosHistorico values (getdate(),9, 'Questionário não preenchido (Portal)', null, null, 'Insert');
	insert into SuitabilityEventosHistorico values (getdate(),10, 'Atualizar perfil (Portal)', null, null, 'Insert');
	insert into SuitabilityEventosHistorico values (getdate(),11, 'Inexistência de Perfil (Portal)', null, null, 'Insert');
	insert into SuitabilityEventosHistorico values (getdate(),12, 'Perfil desatualizado (Portal)', null, null, 'Insert');
	insert into SuitabilityEventosHistorico values (getdate(),13, 'Desenquadramento - Alteração de perfil', null, null, 'Insert');
	insert into SuitabilityEventosHistorico values (getdate(),14, 'Desenquadramento - Alteração de perfil (Portal)', null, null, 'Insert');
	insert into SuitabilityEventosHistorico values (getdate(),15, 'Termo de Ciência (Portal)', null, null, 'Insert');
	insert into SuitabilityEventosHistorico values (getdate(),16, 'Termo de Ciência de Risco e Adesão ao Fundo', null, null, 'Insert');
	insert into SuitabilityEventosHistorico values (getdate(),17, 'Termo de Ciência de Risco e Adesão ao Fundo (Portal)', null, null, 'Insert');
	insert into SuitabilityEventosHistorico values (getdate(),18, 'Termo de Inadequação', null, null, 'Insert');
	insert into SuitabilityEventosHistorico values (getdate(),19, 'Termo de Inadequação (Portal)', null, null, 'Insert');
end
go

IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'PessoaSuitabilityHistorico') > 0
begin

	drop table PessoaSuitabilityHistorico

	CREATE TABLE PessoaSuitabilityHistorico
	(
		[DataHistorico] [datetime] NOT NULL,
		[IdPessoa] int NOT NULL,
		[IdValidacao] int NULL,
		[Validacao] varchar(100) NULL,
		[IdPerfil] int NULL,
		[Perfil] varchar(100) NULL,
		[Recusa] char(1) NOT NULL default 'N',
		[Dispensado] char(1) NOT NULL default 'N',
		[IdDispensa] int NULL,
		[Dispensa] varchar(100) NULL,
		[UltimaAlteracao] datetime NULL,
		[Tipo] [varchar](10) NOT NULL,
		CONSTRAINT PK_PessoaSuitabilityHistorico primary key(DataHistorico, IdPessoa)
	)	
end
GO

--Implantação Suitability no cadastro de pessoas.
--TODO alterar processo para versão 1.1.N (GPS) devido relação 1xn de pessoa para cliente
insert into PessoaSuitability (IdPessoa, Dispensado, Recusa, IdValidacao, UltimaAlteracao)
select IdCliente, 'N', 'N', 1, getdate()
from Cliente
where IdTipo = 1
and not exists (select 1 from PessoaSuitability where cliente.IdCliente = PessoaSuitability.IdPessoa);
go

insert into PessoaSuitability (IdPessoa, Dispensado, Recusa, IdValidacao, UltimaAlteracao)
select IdCliente, 'N', 'N', 2, getdate()
from Cliente
where IdTipo = 10
and not exists (select 1 from PessoaSuitability where cliente.IdCliente = PessoaSuitability.IdPessoa);
go

insert into PessoaSuitability (IdPessoa, Dispensado, Recusa, IdValidacao, UltimaAlteracao, Dispensa)
select IdCliente, 'S', 'N', 3, getdate(), 1
from Cliente
where Cliente.IdTipo in (300,310,320,500,510)
and not exists (select 1 from PessoaSuitability where cliente.IdCliente = PessoaSuitability.IdPessoa);
go

insert into PessoaSuitability (IdPessoa, Dispensado, Recusa, IdValidacao, UltimaAlteracao, Dispensa)
select IdCliente, 'S', 'N', 4, getdate(), 1
from Cliente
where Cliente.IdTipo in (200)
and not exists (select 1 from PessoaSuitability where cliente.IdCliente = PessoaSuitability.IdPessoa);
go

insert into PessoaSuitability (IdPessoa, Dispensado, Recusa, IdValidacao, UltimaAlteracao)
select idCotista, 'N', 'N', 1, getdate()
from Cotista
where TipoTributacao = 1
and not exists (select 1 from PessoaSuitability where cotista.IdCotista = PessoaSuitability.IdPessoa);
go

insert into PessoaSuitability (IdPessoa, Dispensado, Recusa, IdValidacao, UltimaAlteracao)
select idCotista, 'N', 'N', 2, getdate()
from Cotista
where TipoTributacao = 2
and not exists (select 1 from PessoaSuitability where cotista.IdCotista = PessoaSuitability.IdPessoa);
go

alter table TabelaPerfilMensalCVM add Secao18_CnpjComitente1 varchar(50) null
alter table TabelaPerfilMensalCVM add Secao18_CnpjComitente2 varchar(50) null
alter table TabelaPerfilMensalCVM add Secao18_CnpjComitente3 varchar(50) null

alter table TabelaPerfilMensalCVM add Secao18_ParteRelacionada1 char(1) null
alter table TabelaPerfilMensalCVM add Secao18_ParteRelacionada2 char(1) null
alter table TabelaPerfilMensalCVM add Secao18_ParteRelacionada3 char(1) null

alter table TabelaPerfilMensalCVM add Secao18_ValorTotal1 decimal (16, 2) null
alter table TabelaPerfilMensalCVM add Secao18_ValorTotal2 decimal (16, 2) null
alter table TabelaPerfilMensalCVM add Secao18_ValorTotal3 decimal (16, 2) null

alter table TabelaPerfilMensalCVM add Secao18_VedadaCobrancaTaxa char(1) null
alter table TabelaPerfilMensalCVM add Secao18_DataUltimaCobranca datetime NULL
alter table TabelaPerfilMensalCVM add Secao18_ValorUltimaCota decimal (16, 2) null

COMMIT TRANSACTION

GO	
