declare @data_versao char(10)
set @data_versao = '2013-05-13'
if exists (select 1 from VersaoSchema where DataVersao = @data_versao) return
insert into VersaoSchema VALUES(@data_versao)

alter table operacaocotista add IdAgenda int null
go
alter table operacaofundo add IdAgenda int null
go