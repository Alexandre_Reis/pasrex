declare @data_versao char(10)
set @data_versao = '2013-12-11'
if exists (select 1 from VersaoSchema where DataVersao = @data_versao) return
insert into VersaoSchema VALUES(@data_versao)

alter table pessoaendereco add TipoEndereco char(1) null
go
alter table pessoaendereco add CodigoMunicipio int null
go
alter table pessoa add FiliacaoNomePai varchar(100) null
go
alter table pessoa add FiliacaoNomeMae varchar(100) null
go
alter table pessoa add UFNaturalidade char(2) null
go
alter table pessoa add PessoaPoliticamenteExposta char(1) null
go

