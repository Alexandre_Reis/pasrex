declare @data_versao char(10)
set @data_versao = '2015-02-11'
if exists (select 1 from VersaoSchema where DataVersao = @data_versao) return
insert into VersaoSchema VALUES(@data_versao)

INSERT INTO [permissaomenu] ([IdGrupo],[IdMenu],[PermissaoLeitura],[PermissaoAlteracao],[PermissaoExclusao],[PermissaoInclusao]) 
SELECT idgrupo,25215,'S','S','S','S' from grupousuario where idgrupo <> 0
go

alter table clienteinterface add InterfaceSinacorCC varchar(2000) null
go

update clienteinterface set InterfaceSinacorCC = ''
go