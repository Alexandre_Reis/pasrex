declare @data_versao char(10)
set @data_versao = '2014-07-10'
if exists (select 1 from VersaoSchema where DataVersao = @data_versao) return
insert into VersaoSchema VALUES(@data_versao)


update dbo.PermissaoMenu set idmenu = 14530 where idmenu = 1160 and idgrupo <> 0
go

alter table indicadorescarteira add RetornoInicio decimal (20, 12) null
go