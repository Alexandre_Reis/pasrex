declare @data_versao char(10)
set @data_versao = '2016-04-15'

/* 
*  caso este script ja tenha sido executado anteriormente 
*  n�o permite que seja executado novamente, avisa o usuario e 
*  n�o exibe nenhuma outra saida
*/
if exists(select * from VersaoSchema where DataVersao = @data_versao)
begin
	
	-- avisa o usuario em caso de erro
	raiserror('Aten��o este script ja foi executado anteriormente.',16,1)

	
	-- n�o mostra a execu��o do script
	set nocount on 
	
	-- n�o roda o script
	set noexec on 
	

end

begin transaction

	-- n�o permite que a transaction seja commitada em caso de erros
	set xact_abort on

	insert into VersaoSchema values(@data_versao)

	IF EXISTS(select * FROM sys.views where name = 'ViewStatusInvestidores')
	begin
		DROP VIEW ViewStatusInvestidores;
	end	
	GO

	CREATE VIEW ViewStatusInvestidores AS
	 select Cotista.IdCotista, 
  			Carteira.IdCarteira,
			PosicaoCotistaHistorico.DataHistorico,
			ltrim(rtrim(CONVERT(char(20),Cotista.IdCotista))) + ' - ' + Cotista.Apelido as Cotista,
			pessoaCotista.CPFCNPJ as DocumentoCotista,
			'' as Assessor,
			perfilcotista.Perfil as PerfilRiscoInvestidor, 
			CASE WHEN pessoaCotista.Tipo = 1 
				THEN 'F�sica'
			ELSE
				'Juridica'
			END AS TipoPessoa,
			PosicaoCotistaHistorico.DataAplicacao as DataUltimaMovimentacao, 
			Carteira.Apelido as FundoQueInveste, 		
			perfilfundo.Perfil as PerfilRiscoFundo, 
			'' AS PerfilRiscoPortifolioCliente, --Calculado em tempo de extra��o do relat�rio
			case 
				when perfilcotista.nivel < perfilfundo.nivel then 'Sim' else 'N�o' 
			end as CotistaDesenquadrado, 
			case 
				when (select count(1) 
					  from SuitabilityEventos,  
					  SuitabilityLogMensagens 
					  where SuitabilityEventos.IdMensagem = SuitabilityLogMensagens.IdMensagem 
						and SuitabilityEventos.IdSuitabilityEventos in (18, 19) 
						and SuitabilityLogMensagens.Resposta = 'OK') > 0 then 'Sim' else 'N�o' 
			end as AssinouIncompatibilidadeDePerfilRisco, 
			case 
				when (	select count(1) 
						from SuitabilityEventos,  
							 SuitabilityLogMensagens 
						where SuitabilityEventos.IdMensagem = SuitabilityLogMensagens.IdMensagem 
							and SuitabilityEventos.IdSuitabilityEventos in (16,17) 
							and SuitabilityLogMensagens.Resposta = 'OK') > 0 then 'Sim' else 'N�o' 
			end as AssinouTermoAdesao, 
			PessoaSuitability.UltimaAlteracao AS DataUltimaAlteracao, 
			case suitabilityParametrosWorkflow.GrandezaPeriodo			 
				when 1 then DATEADD(day, suitabilityParametrosWorkflow.ValorPeriodo,PessoaSuitability.UltimaAlteracao) 
				when 2 then DATEADD(day, (suitabilityParametrosWorkflow.ValorPeriodo * 30),PessoaSuitability.UltimaAlteracao) 
				when 3 then DATEADD(day, (suitabilityParametrosWorkflow.ValorPeriodo * 360),PessoaSuitability.UltimaAlteracao) 
			end as DataProximaRenovacao,
			pessoaCotista.Tipo
	 from PosicaoCotistaHistorico,  
	 PessoaSuitability,  
	 SuitabilityPerfilInvestidor as perfilcotista,  
	 SuitabilityPerfilInvestidor as perfilfundo, 
	 Carteira, 
	 suitabilityParametrosWorkflow,
	 Cotista,
	 Pessoa pessoaCotista
	 where PosicaoCotistaHistorico.IdCotista = PessoaSuitability.IdPessoa 
		and PessoaSuitability.Perfil = perfilcotista.IdPerfilInvestidor 
		and PosicaoCotistaHistorico.IdCarteira = Carteira.IdCarteira 
		and Carteira.PerfilRisco = perfilfundo.IdPerfilInvestidor 
		and Cotista.IdCotista = PosicaoCotistaHistorico.IdCotista 
		and cotista.IdCotista = pessoaCotista.IdPessoa
	GO
	--

	--DML
	--

-- commita a transaction e volta as variaveis do sql server para o estado inicial
commit transaction
set noexec off
set nocount off


