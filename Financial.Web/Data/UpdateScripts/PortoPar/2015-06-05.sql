﻿

delete [CadastroComplementarItemLista];
delete [CadastroComplementarTipoLista];
delete [CadastroComplementar];
delete [CadastroComplementarCampos];


INSERT INTO [CadastroComplementarTipoLista]  ([NomeLista]  ,[TipoLista])  VALUES  ( 'Cia Aberta ou não',-2) ;
INSERT INTO [CadastroComplementarTipoLista]  ([NomeLista]  ,[TipoLista])  VALUES  ( 'Publica ou Restrita',-2) ;
INSERT INTO [CadastroComplementarTipoLista]  ([NomeLista]  ,[TipoLista])  VALUES  ( 'Qualificado ou Profissional',-2) ;
INSERT INTO [CadastroComplementarTipoLista]  ([NomeLista]  ,[TipoLista])  VALUES  ( 'Sim ou Não',-2) ;
INSERT INTO [CadastroComplementarTipoLista]  ([NomeLista]  ,[TipoLista])  VALUES  ( 'AbertoFechado',-2) ;
INSERT INTO [CadastroComplementarTipoLista]  ([NomeLista]  ,[TipoLista])  VALUES  ( 'AltaMediaBaixa',-2) ;
INSERT INTO [CadastroComplementarTipoLista]  ([NomeLista]  ,[TipoLista])  VALUES  ( 'TipoFundoComplementar',-2) ;
INSERT INTO [CadastroComplementarTipoLista]  ([NomeLista]  ,[TipoLista])  VALUES  ( 'TipoInstFin',-2) ;
INSERT INTO [CadastroComplementarTipoLista]  ([NomeLista]  ,[TipoLista])  VALUES  ( 'NivelAção',-2) ;




INSERT INTO [CadastroComplementarItemLista]  ([IdTipoLista]  ,[ItemLista])  VALUES  ( (select IdTipoLista from [CadastroComplementarTipoLista] where NomeLista='Sim ou Não'),'S') 
INSERT INTO [CadastroComplementarItemLista]  ([IdTipoLista]  ,[ItemLista])  VALUES  ( (select IdTipoLista from [CadastroComplementarTipoLista] where NomeLista='Sim ou Não'),'N') 
INSERT INTO [CadastroComplementarItemLista]  ([IdTipoLista]  ,[ItemLista])  VALUES  ( (select IdTipoLista from [CadastroComplementarTipoLista] where NomeLista='Qualificado ou Profissional'),'Q') 
INSERT INTO [CadastroComplementarItemLista]  ([IdTipoLista]  ,[ItemLista])  VALUES  ( (select IdTipoLista from [CadastroComplementarTipoLista] where NomeLista='Qualificado ou Profissional'),'P') 
INSERT INTO [CadastroComplementarItemLista]  ([IdTipoLista]  ,[ItemLista])  VALUES  ( (select IdTipoLista from [CadastroComplementarTipoLista] where NomeLista='Cia Aberta ou não'),'A') 
INSERT INTO [CadastroComplementarItemLista]  ([IdTipoLista]  ,[ItemLista])  VALUES  ( (select IdTipoLista from [CadastroComplementarTipoLista] where NomeLista='Cia Aberta ou não'),'N') 
INSERT INTO [CadastroComplementarItemLista]  ([IdTipoLista]  ,[ItemLista])  VALUES  ( (select IdTipoLista from [CadastroComplementarTipoLista] where NomeLista='Publica ou Restrita'),'P') 
INSERT INTO [CadastroComplementarItemLista]  ([IdTipoLista]  ,[ItemLista])  VALUES  ( (select IdTipoLista from [CadastroComplementarTipoLista] where NomeLista='Publica ou Restrita'),'R') 
INSERT INTO [CadastroComplementarItemLista]  ([IdTipoLista]  ,[ItemLista])  VALUES  ( (select IdTipoLista from [CadastroComplementarTipoLista] where NomeLista='AbertoFechado'),'Aberto') 
INSERT INTO [CadastroComplementarItemLista]  ([IdTipoLista]  ,[ItemLista])  VALUES  ( (select IdTipoLista from [CadastroComplementarTipoLista] where NomeLista='AbertoFechado'),'Fechado') 
INSERT INTO [CadastroComplementarItemLista]  ([IdTipoLista]  ,[ItemLista])  VALUES  ( (select IdTipoLista from [CadastroComplementarTipoLista] where NomeLista='AltaMediaBaixa'),'Alta') 
INSERT INTO [CadastroComplementarItemLista]  ([IdTipoLista]  ,[ItemLista])  VALUES  ( (select IdTipoLista from [CadastroComplementarTipoLista] where NomeLista='AltaMediaBaixa'),'Média') 
INSERT INTO [CadastroComplementarItemLista]  ([IdTipoLista]  ,[ItemLista])  VALUES  ( (select IdTipoLista from [CadastroComplementarTipoLista] where NomeLista='AltaMediaBaixa'),'Baixa') 
INSERT INTO [CadastroComplementarItemLista]  ([IdTipoLista]  ,[ItemLista])  VALUES  ( (select IdTipoLista from [CadastroComplementarTipoLista] where NomeLista='TipoFundoComplementar'),'FIEE') 
INSERT INTO [CadastroComplementarItemLista]  ([IdTipoLista]  ,[ItemLista])  VALUES  ( (select IdTipoLista from [CadastroComplementarTipoLista] where NomeLista='TipoFundoComplementar'),'FIP') 
INSERT INTO [CadastroComplementarItemLista]  ([IdTipoLista]  ,[ItemLista])  VALUES  ( (select IdTipoLista from [CadastroComplementarTipoLista] where NomeLista='TipoFundoComplementar'),'FIA') 
INSERT INTO [CadastroComplementarItemLista]  ([IdTipoLista]  ,[ItemLista])  VALUES  ( (select IdTipoLista from [CadastroComplementarTipoLista] where NomeLista='TipoFundoComplementar'),'FIC FI') 
INSERT INTO [CadastroComplementarItemLista]  ([IdTipoLista]  ,[ItemLista])  VALUES  ( (select IdTipoLista from [CadastroComplementarTipoLista] where NomeLista='TipoFundoComplementar'),'FIA REFERENCIADO') 
INSERT INTO [CadastroComplementarItemLista]  ([IdTipoLista]  ,[ItemLista])  VALUES  ( (select IdTipoLista from [CadastroComplementarTipoLista] where NomeLista='TipoFundoComplementar'),'FIC FIA REFERENCIADO') 
INSERT INTO [CadastroComplementarItemLista]  ([IdTipoLista]  ,[ItemLista])  VALUES  ( (select IdTipoLista from [CadastroComplementarTipoLista] where NomeLista='TipoFundoComplementar'),'FII') 
INSERT INTO [CadastroComplementarItemLista]  ([IdTipoLista]  ,[ItemLista])  VALUES  ( (select IdTipoLista from [CadastroComplementarTipoLista] where NomeLista='TipoFundoComplementar'),'FIDC') 
INSERT INTO [CadastroComplementarItemLista]  ([IdTipoLista]  ,[ItemLista])  VALUES  ( (select IdTipoLista from [CadastroComplementarTipoLista] where NomeLista='TipoFundoComplementar'),'FIC FIDC') 
INSERT INTO [CadastroComplementarItemLista]  ([IdTipoLista]  ,[ItemLista])  VALUES  ( (select IdTipoLista from [CadastroComplementarTipoLista] where NomeLista='TipoFundoComplementar'),'FIP') 
INSERT INTO [CadastroComplementarItemLista]  ([IdTipoLista]  ,[ItemLista])  VALUES  ( (select IdTipoLista from [CadastroComplementarTipoLista] where NomeLista='TipoFundoComplementar'),'FMIEE') 
INSERT INTO [CadastroComplementarItemLista]  ([IdTipoLista]  ,[ItemLista])  VALUES  ( (select IdTipoLista from [CadastroComplementarTipoLista] where NomeLista='TipoFundoComplementar'),'FIEEI') 
INSERT INTO [CadastroComplementarItemLista]  ([IdTipoLista]  ,[ItemLista])  VALUES  ( (select IdTipoLista from [CadastroComplementarTipoLista] where NomeLista='TipoFundoComplementar'),'FIDC NÃO PADRONIZADO') 
INSERT INTO [CadastroComplementarItemLista]  ([IdTipoLista]  ,[ItemLista])  VALUES  ( (select IdTipoLista from [CadastroComplementarTipoLista] where NomeLista='TipoFundoComplementar'),'FICA (POSIÇÃO/TÍTULO)') 
INSERT INTO [CadastroComplementarItemLista]  ([IdTipoLista]  ,[ItemLista])  VALUES  ( (select IdTipoLista from [CadastroComplementarTipoLista] where NomeLista='TipoInstFin'),'Instituição Financeira ') 
INSERT INTO [CadastroComplementarItemLista]  ([IdTipoLista]  ,[ItemLista])  VALUES  ( (select IdTipoLista from [CadastroComplementarTipoLista] where NomeLista='TipoInstFin'),'Instituição não Financeira Aberta ') 
INSERT INTO [CadastroComplementarItemLista]  ([IdTipoLista]  ,[ItemLista])  VALUES  ( (select IdTipoLista from [CadastroComplementarTipoLista] where NomeLista='TipoInstFin'),'Instituição não Financeira Fechada') 
INSERT INTO [CadastroComplementarItemLista]  ([IdTipoLista]  ,[ItemLista])  VALUES  ( (select IdTipoLista from [CadastroComplementarTipoLista] where NomeLista='NivelAção'),'1') 
INSERT INTO [CadastroComplementarItemLista]  ([IdTipoLista]  ,[ItemLista])  VALUES  ( (select IdTipoLista from [CadastroComplementarTipoLista] where NomeLista='NivelAção'),'2') 
INSERT INTO [CadastroComplementarItemLista]  ([IdTipoLista]  ,[ItemLista])  VALUES  ( (select IdTipoLista from [CadastroComplementarTipoLista] where NomeLista='NivelAção'),'3') 
INSERT INTO [CadastroComplementarItemLista]  ([IdTipoLista]  ,[ItemLista])  VALUES  ( (select IdTipoLista from [CadastroComplementarTipoLista] where NomeLista='NivelAção'),'Balcão') 



INSERT INTO [CadastroComplementarCampos]  ([TipoCadastro]    ,[NomeCampo]         ,[DescricaoCampo]       ,[ValorDefault]      ,[TipoCampo]       ,[CampoObrigatorio]       ,[Tamanho]          ,[CasasDecimais])  VALUES  (-1 , 'CD RATING','Rating do Emissor' ,NULL , (select  idTipoLista from CadastroComplementarTipoLista where NomeLista = 'AltaMediaBaixa') ,'S' ,8  ,0) 
INSERT INTO [CadastroComplementarCampos]  ([TipoCadastro]    ,[NomeCampo]         ,[DescricaoCampo]       ,[ValorDefault]      ,[TipoCampo]       ,[CampoObrigatorio]       ,[Tamanho]          ,[CasasDecimais])  VALUES  (-10 , 'CAPITAL VOTANTE','Capital Votante do papel' ,NULL ,-1 ,'N' ,10  ,2) 
INSERT INTO [CadastroComplementarCampos]  ([TipoCadastro]    ,[NomeCampo]         ,[DescricaoCampo]       ,[ValorDefault]      ,[TipoCampo]       ,[CampoObrigatorio]       ,[Tamanho]          ,[CasasDecimais])  VALUES  (-10 , 'CAPITAL TOTAL','Capital Total do papel' ,NULL ,-1 ,'N' ,10  ,2) 
INSERT INTO [CadastroComplementarCampos]  ([TipoCadastro]    ,[NomeCampo]         ,[DescricaoCampo]       ,[ValorDefault]      ,[TipoCampo]       ,[CampoObrigatorio]       ,[Tamanho]          ,[CasasDecimais])  VALUES  (-10 , 'SOC CONTROLADA','Sociedade Controlada' ,NULL ,-2 ,'N' ,15  ,0) 
INSERT INTO [CadastroComplementarCampos]  ([TipoCadastro]    ,[NomeCampo]         ,[DescricaoCampo]       ,[ValorDefault]      ,[TipoCampo]       ,[CampoObrigatorio]       ,[Tamanho]          ,[CasasDecimais])  VALUES  (-10 , 'CIA NOVO MERCADO','Ativo Emitido de Companhia Novo Mercado' ,NULL , (select  idTipoLista from CadastroComplementarTipoLista where NomeLista = 'Sim ou Não') ,'N' ,1  ,0) 
INSERT INTO [CadastroComplementarCampos]  ([TipoCadastro]    ,[NomeCampo]         ,[DescricaoCampo]       ,[ValorDefault]      ,[TipoCampo]       ,[CampoObrigatorio]       ,[Tamanho]          ,[CasasDecimais])  VALUES  (-8 , 'TIPO FUNDO COMPL','Tipo Fundo Complementar' ,NULL , (select  idTipoLista from CadastroComplementarTipoLista where NomeLista = 'TipoFundoComplementar') ,'N' ,30  ,0) 
INSERT INTO [CadastroComplementarCampos]  ([TipoCadastro]    ,[NomeCampo]         ,[DescricaoCampo]       ,[ValorDefault]      ,[TipoCampo]       ,[CampoObrigatorio]       ,[Tamanho]          ,[CasasDecimais])  VALUES  (-8 , 'TIPO FUNDO QUAL PROF','domínio "Q"ualificado ou Profissional' ,NULL , (select  idTipoLista from CadastroComplementarTipoLista where NomeLista = 'Qualificado ou Profissional') ,'N' ,1  ,0) 
INSERT INTO [CadastroComplementarCampos]  ([TipoCadastro]    ,[NomeCampo]         ,[DescricaoCampo]       ,[ValorDefault]      ,[TipoCampo]       ,[CampoObrigatorio]       ,[Tamanho]          ,[CasasDecimais])  VALUES  (-1 , 'SPE','Sociedade Propósito Específico' ,NULL , (select  idTipoLista from CadastroComplementarTipoLista where NomeLista = 'Sim ou Não') ,'S' ,1  ,0) 
INSERT INTO [CadastroComplementarCampos]  ([TipoCadastro]    ,[NomeCampo]         ,[DescricaoCampo]       ,[ValorDefault]      ,[TipoCampo]       ,[CampoObrigatorio]       ,[Tamanho]          ,[CasasDecimais])  VALUES  (-8 , 'APLICA TX PERFORMANCE ','Campo indicativo se' ,NULL , (select  idTipoLista from CadastroComplementarTipoLista where NomeLista = 'Sim ou Não') ,'N' ,1  ,0) 
INSERT INTO [CadastroComplementarCampos]  ([TipoCadastro]    ,[NomeCampo]         ,[DescricaoCampo]       ,[ValorDefault]      ,[TipoCampo]       ,[CampoObrigatorio]       ,[Tamanho]          ,[CasasDecimais])  VALUES  (-8 , 'CD RATING ','Classificação interna' ,NULL , (select  idTipoLista from CadastroComplementarTipoLista where NomeLista = 'AltaMediaBaixa') ,'N' ,8  ,0) 
INSERT INTO [CadastroComplementarCampos]  ([TipoCadastro]    ,[NomeCampo]         ,[DescricaoCampo]       ,[ValorDefault]      ,[TipoCampo]       ,[CampoObrigatorio]       ,[Tamanho]          ,[CasasDecimais])  VALUES  (-11 , 'IC GARANTIA','Buscar de Cadastro complementar Ativo BMF' ,NULL , (select  idTipoLista from CadastroComplementarTipoLista where NomeLista = 'Sim ou Não') ,'N' ,1  ,0) 



INSERT INTO [CadastroComplementarCampos]  ([TipoCadastro]    ,[NomeCampo]         ,[DescricaoCampo]       ,[ValorDefault]      ,[TipoCampo]       ,[CampoObrigatorio]       ,[Tamanho]          ,[CasasDecimais])  VALUES  (-10 , 'IC COMPANHIA','Indica se trata-se de companhia aberta ou não' ,NULL , (select  idTipoLista from CadastroComplementarTipoLista where NomeLista = 'Cia Aberta ou não') ,'N' ,0  ,0) 
INSERT INTO [CadastroComplementarCampos]  ([TipoCadastro]    ,[NomeCampo]         ,[DescricaoCampo]       ,[ValorDefault]      ,[TipoCampo]       ,[CampoObrigatorio]       ,[Tamanho]          ,[CasasDecimais])  VALUES  (-10 , 'NIVEL','Indica o Nível da ação na BM&FBOVESPA.' ,NULL ,18 ,'N' ,0  ,0) 
INSERT INTO [CadastroComplementarCampos]  ([TipoCadastro]    ,[NomeCampo]         ,[DescricaoCampo]       ,[ValorDefault]      ,[TipoCampo]       ,[CampoObrigatorio]       ,[Tamanho]          ,[CasasDecimais])  VALUES  (-10 , 'DT IPO','Data de IPO (initial public offering)' ,NULL ,-3 ,'N' ,0  ,0) 
INSERT INTO [CadastroComplementarCampos]  ([TipoCadastro]    ,[NomeCampo]         ,[DescricaoCampo]       ,[ValorDefault]      ,[TipoCampo]       ,[CampoObrigatorio]       ,[Tamanho]          ,[CasasDecimais])  VALUES  (-10 , 'IC OFERTA','Pública ou "R"estrita ' ,NULL , (select  idTipoLista from CadastroComplementarTipoLista where NomeLista = 'Publica ou Restrita') ,'N' ,1  ,0) 
INSERT INTO [CadastroComplementarCampos]  ([TipoCadastro]    ,[NomeCampo]         ,[DescricaoCampo]       ,[ValorDefault]      ,[TipoCampo]       ,[CampoObrigatorio]       ,[Tamanho]          ,[CasasDecimais])  VALUES  (-11 , 'LOCAL CUSTODIA','Local de Custodia' ,NULL ,-2 ,'N' ,50  ,0) 
INSERT INTO [CadastroComplementarCampos]  ([TipoCadastro]    ,[NomeCampo]         ,[DescricaoCampo]       ,[ValorDefault]      ,[TipoCampo]       ,[CampoObrigatorio]       ,[Tamanho]          ,[CasasDecimais])  VALUES  (-10 , 'LOCAL CUSTODIA','Local de Custodia' ,NULL ,-2 ,'N' ,50  ,0) 
INSERT INTO [CadastroComplementarCampos]  ([TipoCadastro]    ,[NomeCampo]         ,[DescricaoCampo]       ,[ValorDefault]      ,[TipoCampo]       ,[CampoObrigatorio]       ,[Tamanho]          ,[CasasDecimais])  VALUES  (-8 , 'TIPO CONDOMINIO','Condominio do Fundo Aberto ou Fechado' ,NULL , (select  idTipoLista from CadastroComplementarTipoLista where NomeLista = 'AbertoFechado') ,'S' ,10  ,0) 
INSERT INTO [CadastroComplementarCampos]  ([TipoCadastro]    ,[NomeCampo]         ,[DescricaoCampo]       ,[ValorDefault]      ,[TipoCampo]       ,[CampoObrigatorio]       ,[Tamanho]          ,[CasasDecimais])  VALUES  (-8 , 'IN 3792','Regido pela IN 3792' ,NULL , (select  idTipoLista from CadastroComplementarTipoLista where NomeLista = 'Sim ou Não') ,'S' ,1  ,0) 
INSERT INTO [CadastroComplementarCampos]  ([TipoCadastro]    ,[NomeCampo]         ,[DescricaoCampo]       ,[ValorDefault]      ,[TipoCampo]       ,[CampoObrigatorio]       ,[Tamanho]          ,[CasasDecimais])  VALUES  (-11 , 'IC HEDGE','Usado em Operações de HEDGE' ,NULL , (select  idTipoLista from CadastroComplementarTipoLista where NomeLista = 'Sim ou Não') ,'S' ,1  ,0) 
INSERT INTO [CadastroComplementarCampos]  ([TipoCadastro]    ,[NomeCampo]         ,[DescricaoCampo]       ,[ValorDefault]      ,[TipoCampo]       ,[CampoObrigatorio]       ,[Tamanho]          ,[CasasDecimais])  VALUES  (-8 , 'IC MERCOSUL','Indica se faz parte do Mercosul Sim ou Não' ,NULL , (select  idTipoLista from CadastroComplementarTipoLista where NomeLista = 'Sim ou Não') ,'S' ,1  ,0) 
INSERT INTO [CadastroComplementarCampos]  ([TipoCadastro]    ,[NomeCampo]         ,[DescricaoCampo]       ,[ValorDefault]      ,[TipoCampo]       ,[CampoObrigatorio]       ,[Tamanho]          ,[CasasDecimais])  VALUES  (-12 , 'CD RATING ','Classificação interna' ,NULL , (select  idTipoLista from CadastroComplementarTipoLista where NomeLista = 'AltaMediaBaixa') ,'N' ,8  ,0) 

INSERT INTO [CadastroComplementarCampos]  ([TipoCadastro]    ,[NomeCampo]         ,[DescricaoCampo]       ,[ValorDefault]      ,[TipoCampo]       ,[CampoObrigatorio]       ,[Tamanho]          ,[CasasDecimais])  VALUES  (-13 , 'IC SECURITIZADA','Indica se o papel é securitizado Sim ou Não' ,NULL , (select  idTipoLista from CadastroComplementarTipoLista where NomeLista = 'Sim ou Não') ,'N' ,8  ,0) 
INSERT INTO [CadastroComplementarCampos]  ([TipoCadastro]    ,[NomeCampo]         ,[DescricaoCampo]       ,[ValorDefault]      ,[TipoCampo]       ,[CampoObrigatorio]       ,[Tamanho]          ,[CasasDecimais])  VALUES  (-1 , 'TIPO INST FIN','Indica o tipo de Instituição Financeira' ,NULL , (select  idTipoLista from CadastroComplementarTipoLista where NomeLista = 'TipoInstFin') ,'N' ,40  ,0) 



--AtivoBolsa
 INSERT INTO [CadastroComplementar] (IdCamposComplementares, IdMercadoTipoPessoa, DescricaoMercadoTipoPessoa, ValorCampo) 
 SELECT A.IdCamposComplementares,  B.CdAtivoBolsa, 'Ativo bolsa: '+B.CdAtivoBolsa, '' 
 FROM CadastroComplementarCampos A,   AtivoBolsa B, (select distinct CdAtivoBolsa from PosicaoBolsa) C
 where A.TipoCadastro = -10 and B.CdAtivoBolsa = c.CdAtivoBolsa order by IdCamposComplementares;

--AtivoBMF
 INSERT INTO [CadastroComplementar] (IdCamposComplementares, IdMercadoTipoPessoa, DescricaoMercadoTipoPessoa, ValorCampo) 
 SELECT A.IdCamposComplementares, B.CdAtivoBMF ,'Derivativo: '+B.CdAtivoBMF, '' 
 FROM CadastroComplementarCampos A, AtivoBMF B, (select distinct CdAtivoBMF ativo from PosicaoBMF) C
 where A.TipoCadastro = -11 and B.CdAtivoBMF = C.ativo
 
 
 --Carteira/Fundo
 INSERT INTO [CadastroComplementar] (IdCamposComplementares, IdMercadoTipoPessoa, DescricaoMercadoTipoPessoa, ValorCampo) 
 SELECT A.IdCamposComplementares, B.IdCliente ,B.Apelido, '' 
 FROM CadastroComplementarCampos A, Cliente B, (select distinct IdCarteira from PosicaoFundo) C
 where A.TipoCadastro = -8 and B.IdCliente = C.IdCarteira
 
  --Emissores
 INSERT INTO [CadastroComplementar] (IdCamposComplementares, IdMercadoTipoPessoa, DescricaoMercadoTipoPessoa, ValorCampo) 
 SELECT A.IdCamposComplementares, B.IdAgente ,B.Nome, '' 
 FROM CadastroComplementarCampos A, AgenteMercado B
 where A.TipoCadastro = -1 
 
 
