DROP TABLE [dbo].[OrdemTesouraria]

CREATE TABLE [dbo].[OrdemTesouraria](
	[IdOperacao] [int] IDENTITY(1,1) NOT NULL,
	[IdOperacaoMae] [int] NULL,
	[IdCarteira] [int] NULL,
	[IdCotista] [int] NOT NULL,
	[DataOperacao] [datetime] NOT NULL,
	[TipoOperacao] [tinyint] NOT NULL,
	[Status] [tinyint] NOT NULL,
	[Valor] [decimal](16, 2) NOT NULL,
	[IdFormaLiquidacao] [tinyint] NOT NULL,
	[IdConta] [int] NULL,
 CONSTRAINT [PK_OrdemTesouraria] PRIMARY KEY CLUSTERED 
(
	[IdOperacao] ASC
)
)
GO

ALTER TABLE [dbo].[OrdemTesouraria]  ADD  CONSTRAINT [FK_OrdemTesouraria_OrdemTesouraria1] FOREIGN KEY([IdOperacaoMae])
REFERENCES [dbo].[OrdemTesouraria] ([IdOperacao])
GO

DROP TABLE TabelaOrdemTesouraria
go

