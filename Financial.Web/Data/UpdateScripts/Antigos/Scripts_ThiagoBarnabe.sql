﻿----------------------------------------------------------------------------------------------------------

--06/02/2015

--ATLAS-PASPAS-214
--30365 - AGENDA E FLUXOS DE ATIVOS DE CRÉDITO


--cria a view
IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'V' AND NAME = 'view__GPS__Agenda_Fluxo_Ativos_Creditos') > 0
drop view view__GPS__Agenda_Fluxo_Ativos_Creditos;
go
CREATE VIEW [dbo].[view__GPS__Agenda_Fluxo_Ativos_Creditos] AS
SELECT Cl.Apelido, Cl.DataDia, HC.Data, HC.PLFechamento, TRF.Descricao, E.Nome, 0 RiscoFinal, PRF.Quantidade, PRF.ValorMercado, ARF.DataEvento, 
	CASE WHEN ARF.TipoEvento = 1 THEN 'Juros'
		 WHEN ARF.TipoEvento = 2 THEN 'Juros Correção'
		 WHEN ARF.TipoEvento = 3 THEN 'Amortização'
		 WHEN ARF.TipoEvento = 4 THEN 'Pagamento Principal'
		 WHEN ARF.TipoEvento = 8 THEN 'Amortização Corrigida'
		 ELSE 'Outros Eventos'
	END AS TipoEvento,
	ARF.Taxa, LRF.ValorBruto, '' Comentario, LRF.ValorIR, LRF.ValorIOF, LRF.ValorLiquido, cl.IdCliente
FROM Cliente Cl
	INNER JOIN HistoricoCota HC ON HC.IdCarteira = Cl.IdCliente
	INNER JOIN PosicaoRendaFixaHistorico PRF ON PRF.IdCliente = HC.IdCarteira AND PRF.DataHistorico = HC.Data
	INNER JOIN AgendaRendaFixa ARF ON ARF.IdTitulo = PRF.IdTitulo AND ARF.DataEvento >= PRF.DataHistorico
	INNER JOIN LiquidacaoRendaFixa LRF ON LRF.IdCliente = PRF.IdCliente AND LRF.IdTitulo = PRF.IdTitulo AND LRF.DataLiquidacao >= PRF.DataHistorico AND LRF.TipoLancamento = ARF.TipoEvento AND LRF.DataLiquidacao = ARF.DataPagamento
	INNER JOIN TituloRendaFixa TRF ON TRF.IdTitulo = LRF.IdTitulo
	INNER JOIN Emissor E ON E.IdEmissor = TRF.IdEmissor

UNION

SELECT Cl.Apelido, Cl.DataDia, HC.Data, HC.PLFechamento, PBC.CdAtivoBolsa, E.Nome, 0 RiscoFinal, PBC.Quantidade, PB.ValorMercado, PBC.DataLancamento, 
	CASE WHEN PBC.TipoProvento = 10 THEN 'Dividendo'
		 WHEN PBC.TipoProvento = 11 THEN 'Restituição Capital'
		 WHEN PBC.TipoProvento = 12 THEN 'Bonificação Financeira'
		 WHEN PBC.TipoProvento = 13 THEN 'Juros sobre Capital'
		 WHEN PBC.TipoProvento = 14 THEN 'Rendimento'
		 WHEN PBC.TipoProvento = 16 THEN 'Juros'
		 ELSE 'Outros Eventos'
	END AS TipoEvento,
	0 Percentual, PBC.Valor, '' Comentario, (PBC.Valor * PBC.PercentualIR) ValorIR, 0 OutrasTaxas, (PBC.Valor * (1 - PBC.PercentualIR)) ValorLiquido, Cl.IdCliente
FROM Cliente Cl
	INNER JOIN HistoricoCota HC ON HC.IdCarteira = Cl.IdCliente
	INNER JOIN PosicaoBolsaHistorico PB ON PB.IdCliente = HC.IdCarteira AND PB.DataHistorico = HC.Data
	INNER JOIN ProventoBolsaCliente PBC ON PBC.IdCliente = PB.IdCliente AND PBC.DataPagamento >= PB.DataHistorico
	INNER JOIN AtivoBolsa AB ON AB.CdAtivoBolsa = PBC.CdAtivoBolsa
	INNER JOIN Emissor E ON E.IdEmissor = AB.IdEmissor;
GO

--cria a permissão para o menu	
delete from PermissaoMenu where IdMEnu =  15040
insert into [dbo]. [PermissaoMenu]
SELECT [IdGrupo]
      ,15040
      ,[PermissaoLeitura]
      ,[PermissaoAlteracao]
      ,[PermissaoExclusao]
      ,[PermissaoInclusao]
  FROM [dbo].[PermissaoMenu]
  where IdMenu = 30000;

GO



--Criar VIEWs na base do PAS
/*

-DATA:10/02/2015

-JIRA
ATLAS-PASPAS-352
Criar VIEWs na base do PAS

*/
GO

--MovimentosBMF
IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'V' AND NAME = 'MovimentosBMF') > 0
DROP VIEW MovimentosBMF;

GO
create view MovimentosBMF as
select
OperacaoBMF.IdCliente,
Cliente.Nome as 'ClienteNome',
AgenteMercado.Nome as 'Corretora',
OperacaoBMF.Data,
OperacaoBMF.TipoOperacao as 'Tipo',
OperacaoBMF.CdAtivoBMF as 'Ativo',
OperacaoBMF.TipoMercado as 'Mercado',
Quantidade,
PU,
Valor,
CalculaDespesas as 'InformaCustos',
AtivoBMF.Peso,
null as 'IdAgenteOrigem',
null as 'AgenteOrigemDescricao',
null as 'IdAgenteDestino',
null as 'AgenteDestinoDescricao',
OperacaoBMF.Serie,
OperacaoBMF.IdOperacao,
OperacaoBMF.ValorLiquido,
AtivoBMF.DataVencimento
from 
OperacaoBMF inner join Cliente
on OperacaoBMF.IdCliente =  Cliente.IdCliente 
inner join AgenteMercado 
on OperacaoBMF.IdAgenteCorretora = AgenteMercado.IdAgente
inner join AtivoBMF
on OperacaoBMF.CdAtivoBMF = AtivoBMF.CdAtivoBMF

union 

select
TransferenciaBMF.IdCliente,
Cliente.Nome,
null,
Data,
null,
TransferenciaBMF.CdAtivoBMF,
null,
Quantidade,
PU,
null,
null,
null,
IdAgenteOrigem,
A.Nome,
IdAgenteDestino,
B.Nome,
TransferenciaBMF.Serie,
TransferenciaBMF.IdTransferencia,
null,
AtivoBMF.DataVencimento
from TransferenciaBMF left join Cliente
on TransferenciaBMF.IdCliente = Cliente.IdCliente
left join AgenteMercado A
on TransferenciaBMF.IdAgenteOrigem = A.IdAgente
left join AgenteMercado B
on TransferenciaBMF.IdAgenteDestino = B.IdAgente
inner join AtivoBMF 
on TransferenciaBMF.CdAtivoBMF = AtivoBMF.CdAtivoBMF



GO

--CadastroIndexadores
IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'V' AND NAME = 'Indexadores') > 0
DROP VIEW Indexadores;

GO
create view Indexadores as
select 
Indice.IdIndice,
Indice.Descricao,
Feeder.Descricao as 'Feeder',
Indice.Tipo,
Indice.TipoDivulgacao,
Indice.IdIndiceBase,
Indice.Percentual,
Indice.Taxa
from Indice inner join Feeder
on Indice.IdFeeder = Feeder.IdFeeder;


GO

--MovimentosFundoCotistas
IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'V' AND NAME = 'MovimentosFundoCotistas') > 0
DROP VIEW MovimentosFundoCotistas;

GO
create view MovimentosFundoCotistas as
select 
OperacaoFundo.IdCliente,
Cliente.Nome as Cotista,
OperacaoFundo.IdCarteira,
Carteira.Nome as Carteira,
DataOperacao,
--dataregistro
DataConversao,
DataLiquidacao,
TipoOperacao,
ValorBruto,
ValorIR,
ValorIOF,
(ValorIOF + ValorIR + ValorCPMF) as 'ValorTotalTributos',
ValorLiquido,
Quantidade
from OperacaoFundo left join Cliente
on OperacaoFundo.IdCliente = Cliente.IdCliente
left join Carteira 
on OperacaoFundo.IdCarteira = Carteira.IdCarteira
union
select
OperacaoCotista.IdCotista,
Cotista.Nome as Cotista,
OperacaoCotista.IdCarteira,
Carteira.Nome as Carteira,
DataOperacao,
--dataregistro
DataConversao,
DataLiquidacao,
TipoOperacao,
ValorBruto,
ValorIR,
ValorIOF,
(ValorIOF + ValorIR + ValorCPMF) as 'ValorTotalTributos',
ValorLiquido,
Quantidade
from OperacaoCotista left join Cotista
on OperacaoCotista.IdCotista = Cotista.IdCotista
left join Carteira 
on OperacaoCotista.IdCarteira = Carteira.IdCarteira

GO

--MovimentosSwap
IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'V' AND NAME = 'MovimentosSwap') > 0
DROP VIEW MovimentosSwap;

GO 
create view MovimentosSwap as
select
TipoRegistro as 'Tipo',
OperacaoSwap.IdAgente as 'Agente',
AgenteMercado.Nome 'AgenteNome',
NumeroContrato as 'Contrato',
ComGarantia as 'Com Garantia',
DataEmissao as 'Emissão',
DataVencimento as 'Vencimento',
ValorBase as 'ValorBase',
OperacaoSwap.IdEstrategia as 'Estratégia',
Estrategia.Descricao as 'EstrategiaDescrição',
TipoPonta as 'Ponta',
TipoApropriacao as 'Apropriação',
ContagemDias as 'Contagem',
BaseAno as 'Base Ano',
--calculo
OperacaoSwap.Percentual,
--spread
OperacaoSwap.IdIndice as 'ÍndicePartida',
Indice.Descricao as 'ÍndicePartidaDescrição'
from OperacaoSwap left join AgenteMercado
on OperacaoSwap.IdAgente =  AgenteMercado.IdAgente
left join Estrategia
on OperacaoSwap.IdEstrategia = Estrategia.IdEstrategia
left join Indice
on OperacaoSwap.IdIndice = Indice.IdIndice

go

--CotacoesEPrecos
IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'V' AND NAME = 'CotacoesEPrecos') > 0
DROP VIEW CotacoesEPrecos;
go
create view CotacoesEPrecos as
select
Data,
CdAtivoBolsa,
PUMedio,
PUFechamento,
PUAbertura,
null as 'PUCorrigido',
null as 'Serie',
null as 'IdSerie',
null as 'Cotacao',
null as 'Descricao',
null as 'CodigoSELIC',
null as 'DataEmissao',
null as 'DataVencimento',
null as 'TaxaIndicativa',
null as 'PU',
null as 'CodigoTitulo',
null as 'Fundo',
null as 'ValorCota'
from CotacaoBolsa

union

select
Data,
CdAtivoBMF,
PUMedio,
PUFechamento,
null,
PUCorrigido,
Serie,
null,
null,
null,
null,
null,
null,
null,
null,
null,
null,
null
from CotacaoBMF

union

select
Data,
null,
null,
null,
null,
null,
null,
IdSerie,
Cotacao,
null,
null,
null,
null,
null,
null,
null,
null,
null
from CotacaoSerie

union

select
DataReferencia,
null,
null,
null,
null,
null,
null,
null,
null,
Descricao,
CodigoSELIC,
DataEmissao,
DataVencimento,
TaxaIndicativa,
PU,
null,
null,
null
from CotacaoMercadoAndima

union

select 
DataReferencia,
null,
null,
null,
null,
null,
null,
null,
null,
null,
null,
null,
DataVencimento,
TaxaIndicativa,
PU,
CodigoPapel as 'CodigoTitulo',
null,
null
from CotacaoMercadoDebenture

union

select
Data,
null,
null,
null,
null,
null,
null,
null,
null,
null,
null,
null,
null,
null,
PU,
CodigoPapel as 'CodigoTitulo',
null,
null
from CotacaoDebenture

union

select
DataReferencia,
null,
null,
null,
null,
null,
null,
null,
null,
Descricao,
null,
null,
DataVencimento,
null,
PU,
null,
null,
null
from CotacaoMercadoTesouro

union

select
HistoricoCota.Data,
null,
null,
null,
null,
null,
null,
null,
null,
null,
null,
null,
null,
null,
null,
null,
Carteira.Nome as 'Fundo',
HistoricoCota.CotaFechamento as 'ValorCota'
from HistoricoCota 
inner join Carteira
on HistoricoCota.IdCarteira = Carteira.IdCarteira
inner join Cliente
on HistoricoCota.IdCarteira = Cliente.IdCliente
where Cliente.TipoControle in (1,4,7)
and Cliente.StatusAtivo = 1


go

--Cadastros
IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'V' AND NAME = 'Cadastros') > 0
DROP VIEW Cadastros;
go
create view Cadastros as
select
null as 'Nome',
null as 'CNPJ',
IdPessoa,
ContaCorrente.IdBanco,
Banco.Nome as 'BancoNome',
ContaCorrente.IdAgencia,
Agencia.Nome as 'AgenciaNome',
Numero,
DigitoConta,
TipoConta,
IdMoeda,
null as 'CodigoBovespa',
null as 'CodigoBMF',
null as 'CodigoCetip',
null as 'CorretagemAdicional',
null as 'Apelido',
null as 'IdSetor',
null as 'SetorNome',
null as 'IdAgente',
null as 'AgenteNome',
null as 'CodigoInterface',
null as 'TipoEmissor'
from ContaCorrente left join Banco
on ContaCorrente.IdBanco = Banco.IdBanco
left join Agencia
on ContaCorrente.IdAgencia = Agencia.IdAgencia

union

select 
Nome,
CNPJ,
null, 
null, 
null, 
null, 
null, 
null, 
null, 
null, 
null, 
CodigoBovespa,
CodigoBMF,
CodigoCetip,
CorretagemAdicional,
Apelido,
null, 
null, 
null, 
null, 
null, 
null
from AgenteMercado 

union

select
Emissor.Nome,
Emissor.CNPJ,
null, 
null, 
null, 
null, 
null, 
null, 
null, 
null, 
null, 
null, 
null, 
null, 
null, 
null, 
Emissor.IdSetor,
Setor.Nome,
Emissor.IdAgente,
AgenteMercado.Nome,
CodigoInterface,
TipoEmissor
from Emissor left join Setor
on Emissor.IdSetor = Setor.IdSetor
left join AgenteMercado
on Emissor.IdAgente = AgenteMercado.IdAgente

go

--MovimentosBolsa
IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'V' AND NAME = 'MovimentosBolsa') > 0
DROP VIEW MovimentosBolsa;

go
create view MovimentosBolsa as
select
OperacaoBolsa.IdCliente as 'Cliente',
Cliente.Nome as 'ClienteNome',
IdAgenteCorretora as 'Corretora',
AgenteMercado.Nome as 'CorretoraNome',
Data as 'Lançamento',
OperacaoBolsa.TipoMercado as 'Tipo',
OperacaoBolsa.CdAtivoBolsa as 'Ativo',
FatorCotacaoBolsa.Fator as 'Ft.Cotação',
PercentualDesconto as '%Desconto',
DataLiquidacao, 
Quantidade,
AtivoBolsa.DataVencimento as 'VctoTermo',
PU,
OperacaoTermoBolsa.Taxa as 'TaxaTermo', 
Valor,
OperacaoTermoBolsa.NumeroContrato 'NrContrato',
Origem as 'Exercicio',
OperacaoTermoBolsa.IdIndice as 'IndiceTermo',
Observacao as 'Observação',
CalculaDespesas as 'InformaCustos',
null as 'Tipo Carteira',
null as 'Ponta',
null as 'Espec.',
null as 'TxOperação',
null as 'TxComissão',
null as 'TipoMovimento',
null as 'TipoOperação',
null as 'TipoEmprestimo'
from OperacaoBolsa left join OperacaoTermoBolsa
on OperacaoBolsa.IdOperacao = OperacaoTermoBolsa.IdOperacao
left join FatorCotacaoBolsa
on OperacaoBolsa.CdAtivoBolsa = fatorCotacaoBolsa.CdAtivoBolsa
left join AtivoBolsa
on OperacaoBolsa.CdAtivoBolsa = AtivoBolsa.CdAtivoBolsa
left join Cliente
on OperacaoBolsa.IdCliente = Cliente.IdCliente
left join AgenteMercado
on OperacaoBolsa.IdAgenteCorretora = AgenteMercado.IdAgente

union 

select
TransferenciaBolsa.IdCliente,
Cliente.Nome,
null,
null,
Data,
null,
CdAtivoBolsa,
null,
null,
null,
Quantidade,
null,
pu,
null,
null,
null,
null,
null,
null,
null,
null,
null,
null,
null,
null,
null, --Tipo de Movimento 
null,
null
from TransferenciaBolsa left join Cliente
on TransferenciaBolsa.idCliente = Cliente.IdCliente

union 

select
BloqueioBolsa.IdCliente,
Cliente.Nome,
BloqueioBolsa.IdAgente,
AgenteMercado.Nome,
DataOperacao,
null,
CdAtivoBolsa,
null,
null,
null,
Quantidade,
null,
null,
null,
null,
null,
null,
null,
Observacao,
null,
TipoCarteiraBloqueada,
null,
null,
null,
null,
null,
TipoOperacao,
null
from BloqueioBolsa left join Cliente
on BloqueioBolsa.idCliente = Cliente.IdCliente
left join AgenteMercado
on BloqueioBolsa.IdAgente = AgenteMercado.IdAgente

union

select
OperacaoEmprestimoBolsa.IdCliente,
Cliente.Nome,
AgenteMercado.IdAgente,
AgenteMercado.Nome,
DataRegistro,
null,
OperacaoEmprestimoBolsa.CdAtivoBolsa,
FatorCotacaoBolsa.Fator,
null,
null,
Quantidade,
OperacaoEmprestimoBolsa.DataVencimento,
PU,
null,
Valor,
NumeroContrato,
null,
null,
null,
null,
null,
PontaEmprestimo,
AtivoBolsa.Especificacao,
TaxaOperacao, 
TaxaComissao,
null,
null,
TipoEmprestimo
from OperacaoEmprestimoBolsa left join Cliente
on OperacaoEmprestimoBolsa.IdCliente = Cliente.IdCliente
left join AgenteMercado
on OperacaoEmprestimoBolsa.IdAgente = AgenteMercado.IdAgente
left join AtivoBolsa
on OperacaoEmprestimoBolsa.CdAtivoBolsa = AtivoBolsa.CdAtivoBolsa
left join FatorCotacaoBolsa
on OperacaoEmprestimoBolsa.CdAtivoBolsa = FatorCotacaoBolsa.CdAtivoBolsa
go


--MovimentosRendaFixa
IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'V' AND NAME = 'MovimentosRendaFixa') > 0
DROP VIEW MovimentosRendaFixa;
GO
CREATE VIEW MovimentosRendaFixa AS
SELECT 
BloqueioRendaFixa.IdCliente,
Cliente.Nome,
posicaoRendaFixa.DataOperacao,
Motivo,
BloqueioRendaFixa.TipoOperacao as 'TipoOperacaoBloqueio',
BloqueioRendaFixa.Quantidade as 'QuantidadeBloqueio',
BloqueioRendaFixa.IdTitulo as 'IdTituloBloqueio',
TituloRendaFixa.DescricaoCompleta,
BloqueioRendaFixa.DataOperacao as 'DataBloqueio',
posicaoRendaFixa.DataVencimento,
posicaoRendaFixa.Quantidade,
BloqueioRendaFixa.IdPosicao,
null as 'DataRegistro',
null as 'TaxaOperacao',
null as 'IdAgenteCorretora',
null as 'AgenteMercadoNome',
null as 'DataLiquidacao',
null as 'DataVolta',
null as 'TipoNegociacao',
null as 'IdIndiceVolta',
null as 'TaxaVolta',
null as 'PUOperacao',
null as 'PUVolta',
null as 'Valor',
null as 'ValorVolta',
null as 'OperacaoTermo',
null as 'ValorCorretagem',
null as 'Observacao',
null as 'QtdeOperacaoRendaFixa'
from BloqueioRendaFixa left join posicaoRendaFixa
on BloqueioRendaFixa.IdPosicao = posicaoRendaFixa.IdPosicao
left join Cliente
on BloqueioRendaFixa.IdCliente = Cliente.IdCliente
left join TituloRendaFixa
on BloqueioRendaFixa.IdTitulo = TituloRendaFixa.IdTitulo

union

select
OperacaoRendaFixa.IdCliente,
Cliente.Nome,
DataOperacao,
null,
TipoOperacao,
null,
OperacaoRendaFixa.IdTitulo,
TituloRendaFixa.DescricaoCompleta,
null,
null,
null,
IdPosicaoResgatada,
DataRegistro,
TaxaOperacao,
IdAgenteCorretora,
AgenteMercado.Nome,
DataLiquidacao,
DataVolta,
TipoNegociacao,
IdIndiceVolta,
TaxaVolta,
PUOperacao,
PUVolta,
Valor,
ValorVolta,
OperacaoTermo,
ValorCorretagem,
Observacao,
Quantidade
from OperacaoRendaFixa left join Cliente
on OperacaoRendaFixa.IdCliente = Cliente.IdCliente
left join TituloRendaFixa
on OperacaoRendaFixa.IdTitulo = TituloRendaFixa.IdTitulo
left join AgenteMercado
on OperacaoRendaFixa.IdAgenteCorretora = AgenteMercado.IdAgente


GO


--Posicao
IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'V' AND NAME = 'Posicao') > 0
DROP VIEW Posicao;
GO

create view Posicao as
select
PosicaoBolsaHistorico.IdCliente,
Cliente.Nome as 'ClienteNome',
DataHistorico,
CdAtivoBolsa as 'Código',
PuMercado,
Quantidade as 'QtdeTotal',
null as 'Tipo',
null as 'Registro',
null as 'Vencimento',
null as 'TaxaOperacao',
null as 'ValorBase',
null as 'ValorCorrigidoJuros',
PosicaoBolsaHistorico.IdAgente as 'Corretora',
AgenteMercado.Nome as 'AgenteNome',
PUCustoLiquido as 'PUCusto',
ValorCustoLiquido as 'ValorCusto',
ValorMercado,
ResultadoRealizar,
QuantidadeBloqueada,
null as 'Série',
null as 'ValorIR',
null as 'ValorIOF',
null as 'Operação',
null as 'IdTitulo',
null as 'TituloNome',
null as 'ValorLiquido',
null as 'Emissão',
null as 'Saldo',
null as 'IdIndiceContraParte',
null as 'TaxaJurosContraParte',
null as 'ValorContraParte',
null as 'ValorParte',
null as 'Índice',
null as 'Taxa',
null as 'Aplicação',
null as 'ValorBruto',
null as 'Nome',
null as 'Lançamento',
null as 'IdConta',
null as 'Valor',
null as 'Fonte',
null as 'Descricao',
null as 'QtdeBloqueada',
null as 'IdCotista',
null as 'CotistaNome',
null as 'IdCarteira',
null as 'CarteiraNome',
null as 'CotaDia',
null as 'LiquidacaoOrigem',
'Bolsa' as 'Mercado'
from PosicaoBolsaHistorico left join Cliente
on PosicaoBolsaHistorico.IdCliente = Cliente.IdCliente
left join AgenteMercado 
on PosicaoBolsaHistorico.IdAgente = AgenteMercado.IdAgente 

union

select

PosicaoEmprestimoBolsaHistorico.IdCliente,
Cliente.Nome,
DataHistorico,
CdAtivoBolsa,
PUMercado,
Quantidade,
PontaEmprestimo,
DataRegistro,
DataVencimento,
TaxaOperacao,
ValorBase,
ValorCorrigidoJuros,
null,
null,
PosicaoEmprestimoBolsaHistorico.PULiquidoOriginal,
null,
null,
null,
null,
null,
null,
null,
null,
null,
null,
null,
null,
null,
null,
null,
null,
null,
null,
null,
null,
null,
null,
null,
null,
null,
null,
null,
null,
null,
null,
null,
null,
null,
null,
'EA'
from PosicaoEmprestimoBolsaHistorico left join Cliente
on PosicaoEmprestimoBolsaHistorico.IdCliente = Cliente.IdCliente 

union

select
PosicaoBMFHistorico.IdCliente,
Cliente.Nome,
DataHistorico,
CdAtivoBMF,
PUMercado,
Quantidade,
null,
null,
null,
null,
null,
null,
PosicaoBMFHistorico.IdAgente,
AgenteMercado.Nome,
null,
PUCustoLiquido,
ValorMercado,
ResultadoRealizar,
null,
Serie,
null,
null,
null,
null,
null,
null,
null,
null,
null,
null,
null,
null,
null,
null,
null,
null,
null,
null,
null,
null,
null,
null,
null,
null,
null,
null,
null,
null,
null,
'BMF'
from PosicaoBMFHistorico left join Cliente
on PosicaoBMFHistorico.IdCliente = Cliente.IdCliente
left join AgenteMercado
on PosicaoBMFHistorico.IdAgente = AgenteMercado.IdAgente


union

select
PosicaoRendaFixaHistorico.IdCliente,
Cliente.Nome,
DataHistorico,
null,
PUMercado,
Quantidade,
null,
null,
PosicaoRendaFixaHistorico.DataVencimento,
TaxaOperacao,
null,
null,
null,
null,
PUOperacao,
PUMercado,
ValorMercado,
null,
QuantidadeBloqueada,
null,
ValorIR,
ValorIOF,
DataOperacao,
PosicaoRendaFixaHistorico.IdTitulo,
TituloRendaFixa.DescricaoCompleta,
(ValorMercado - ValorIR - ValorIOF),
null,
null,
IdIndiceVolta,
null,
null,
null,
null,
null,
null,
null,
null,
null,
null,
null,
null,
null,
null,
null,
null,
null,
null,
null,
null,
'RendaFixa'
from PosicaoRendaFixaHistorico left join Cliente
on PosicaoRendaFixaHistorico.IdCliente = Cliente.IdCliente 
left join TituloRendaFixa
on PosicaoRendaFixaHistorico.IdTitulo = TituloRendaFixa.IdTitulo

union

select
PosicaoSwapHistorico.IdCliente,
Cliente.Nome,
DataHistorico,
null,
null,
null,
null,
null,
DataVencimento,
null,
PosicaoSwapHistorico.ValorBase,
null,
null,
null,
null,
null,
null,
null,
null,
null,
null,
null,
null,
null,
null,
null,
DataEmissao,
Saldo,
IdIndiceContraParte,
TaxaJurosContraParte,
ValorContraParte,
ValorParte,
IdIndice,
TaxaJuros,
null,
null,
null,
null,
null,
null,
null,
null,
null,
null,
null,
null,
null,
null,
null,
'SWAP'
from PosicaoSwapHistorico left join Cliente
on PosicaoSwapHistorico.IdCliente = Cliente.IdCliente 

union 

select 

null,
null,
null,
null,
null,
Quantidade,
null,
null,
null,
null,
null,
null,
null,
null,
null,
null,
null,
null,
null,
null,
ValorIR,
ValorIOF,
null,
null,
null,
ValorLiquido,
null,
null,
null,
null,
null,
null,
null,
null,
DataAplicacao,
ValorBruto,
null,
null,
null,
null,
null,
null,
QuantidadeBloqueada,
PosicaoCotista.IdCotista,
Cotista.Nome, 
PosicaoCotista.IdCarteira,
Carteira.Nome,
CotaDia,
null,
'FUNDO'
from PosicaoCotista left join Cotista
on PosicaoCotista.IdCotista = Cotista.IdCotista
left join Carteira
on PosicaoCotista.IdCarteira = Carteira.IdCarteira

union

select 
Liquidacao.IdCliente,
Cliente.Nome,
null,
null,
null,
null,
null,
null,
Liquidacao.DataVencimento,
null,
null,
null,
null,
null,
null,
null,
null,
null,
null,
null,
null,
null,
null,
null,
null,
null,
null,
null,
null,
null,
null,
null,
null,
null,
null,
null,
Cliente.Apelido,
Liquidacao.DataLancamento,
Liquidacao.IdConta,
Liquidacao.Valor,
Liquidacao.Fonte,
Liquidacao.Descricao,
null,
null,
null,
null,
null,
null,
Liquidacao.Origem,
'CAIXA'
from Liquidacao inner join Cliente
on Liquidacao.IdCliente = Cliente.IdCliente

go

IF EXISTS(SELECT 1 FROM SYSCOLUMNS WHERE ID = OBJECT_ID('OperacaoBolsa') AND NAME = 'DataRegistro')
BEGIN
	ALTER TABLE OperacaoBolsa
	DROP COLUMN DataRegistro
END
GO

IF NOT EXISTS(select 1 from syscolumns where id = object_id('OperacaoBolsa') and name = 'DataOperacao')
BEGIN    

	alter table OperacaoBolsa add DataOperacao Datetime;
    
	EXEC sp_executesql
            N'update OperacaoBolsa set DataOperacao = Data'

	alter table OperacaoBolsa alter column DataOperacao datetime not null;
	
END

IF EXISTS(SELECT 1 FROM SYSCOLUMNS WHERE ID = OBJECT_ID('OperacaoBMF') AND NAME = 'DataRegistro')
BEGIN
	ALTER TABLE OperacaoBMF
	DROP COLUMN DataRegistro
END
GO

IF NOT EXISTS(select 1 from syscolumns where id = object_id('OperacaoBMF') and name = 'DataOperacao')
BEGIN    

	alter table OperacaoBMF add DataOperacao Datetime;
    
	EXEC sp_executesql
            N'update OperacaoBMF set DataOperacao = Data'

	alter table OperacaoBMF alter column DataOperacao datetime not null;
	
END

IF NOT EXISTS(SELECT 1 FROM SYSCOLUMNS WHERE ID= OBJECT_ID('OperacaoFundo') AND NAME = 'DataRegistro')
BEGIN
	ALTER TABLE OperacaoFundo add DataRegistro Datetime;

	EXEC sp_executesql
            N'update OperacaoFundo set DataRegistro = DataOperacao'

	alter table OperacaoFundo alter column DataRegistro datetime not null;
END


IF NOT EXISTS(SELECT 1 FROM SYSCOLUMNS WHERE ID=OBJECT_ID('OperacaoFundo') AND NAME = 'DepositoRetirada')
BEGIN
	ALTER TABLE OperacaoFundo add DepositoRetirada bit not null default 0;
END




/*
*
*
*
*
*
*
*
*/


IF EXISTS (
    SELECT * FROM sysobjects WHERE id = object_id(N'DiasUteis') 
    AND xtype IN (N'FN', N'IF', N'TF')
)
DROP FUNCTION DiasUteis
GO

CREATE FUNCTION DiasUteis (@StartDate DATE,	@EndDate DATE, @IdLocal int) RETURNS TABLE
AS
RETURN(	
	SELECT 
   (DATEDIFF(dd, @StartDate, @EndDate) +1)
  -(DATEDIFF(wk, @StartDate, @EndDate) * 2)
  -1
  -(CASE WHEN DATENAME(dw, @StartDate) = 'Sunday' THEN 1 ELSE 0 END)  
  -(CASE WHEN DATENAME(dw, @EndDate) = 'Saturday' THEN 1 ELSE 0 END) 
  -(CASE WHEN EXISTS (SELECT 1 FROM Feriado f WHERE f.Data = @StartDate) THEN 1 ELSE 0 END)
  -(SELECT COUNT(*) FROM Feriado f WHERE f.Data BETWEEN @StartDate and @EndDate and IdLocal = @IdLocal  and DATENAME(dw,f.Data) != 'Sunday' and DATENAME(dw,f.Data) != 'Saturday')
  AS QtdDias
)

GO

IF EXISTS (
    SELECT * FROM sysobjects WHERE id = object_id(N'LOAD_BS_POS_RF') 
    AND xtype IN (N'P', N'PC')
)
DROP PROCEDURE LOAD_BS_POS_RF
GO

CREATE PROCEDURE[dbo].[LOAD_BS_POS_RF]
      @DataRef date --Data em que a procedure vai rodar
AS
BEGIN

SELECT
CONVERT(DATETIME,posRF.DataHistorico)								AS BS_DT,
CONVERT(CHAR(10),posRF.IdOperacao)									AS BS_RFOP_CD,
CONVERT(DATETIME,posRF.dataOperacao)								AS BS_DT_AQUISICAO,
CONVERT(FLOAT,posRF.PuOperacao)										AS BS_VL_PU_AQUISICAO,
CONVERT(FLOAT,posRF.Quantidade - posRF.QuantidadeBloqueada)			AS BS_QT_DISPONIVEL,
CONVERT(FLOAT,posRF.QuantidadeBloqueada)							AS BS_QT_BLOQUEADA,
CONVERT(FLOAT,posRF.PuMercado)										AS BS_VL_PU,
CONVERT(FLOAT,TitRF.ValorNominal * posRF.Quantidade)				AS BS_VL_PRINCIPAL,
CONVERT(FLOAT,posRF.ValorCorrecao)									AS BS_VL_CM,
CONVERT(FLOAT,posRF.ValorJuros)										AS BS_VL_JUROS,
CONVERT(FLOAT,0)													AS BS_VL_PREMIO,
CONVERT(FLOAT,0)													AS BS_VL_AGIO,
CONVERT(FLOAT,posRF.ValorMercado)									AS BS_VL_BRUTOPOS,
CONVERT(FLOAT,posRF.ValorIR)										AS BS_VL_PROV_IRRF,
CONVERT(FLOAT,posRF.ValorIOF)										AS BS_VL_PROV_IOF,
CONVERT(FLOAT,posRF.ValorMercado - posRF.ValorIR - posRF.ValorIOF)	AS BS_VL_LIQ_POS,
CONVERT(FLOAT,posRF.ValorMercado)									AS BS_VL_BRUTO_GER,
CONVERT(FLOAT,posRF.ValorIR)										AS BS_VL_PROV_IRRFGER,
CONVERT(FLOAT,posRF.ValorIOF)										AS BS_VL_PROV_IOFGER,
CONVERT(FLOAT,posRF.ValorMercado - posRF.ValorIR - posRF.ValorIOF)	AS BS_VL_LIQUID_GER,
CONVERT(FLOAT,1)													AS BS_VL_FATOR_CM,
CONVERT(FLOAT,1)													AS BS_VL_FATOR_JUROS,
CONVERT(FLOAT,1)													AS BS_VL_FATOR_PREMIO,
CONVERT(FLOAT,1)													AS BS_VL_FATOR_AGIO,
CONVERT(FLOAT,1)													AS BS_VL_FATOR_AJUSTE,
CONVERT(FLOAT,0)													AS BS_VL_PUEM_COR,
CONVERT(FLOAT,0)													AS BS_VL_PUEM_PRINC,
CONVERT(FLOAT,0)													AS BS_VL_PUEM_CM,
CONVERT(FLOAT,0)													AS BS_VL_PUEM_JUROS,
CONVERT(FLOAT,0)													AS BS_VL_PUEM_PREMIO,
CONVERT(FLOAT,0)													AS BS_VL_PROV_IRRF_UNIT,
CONVERT(FLOAT,1)													AS BS_VL_AP,
CONVERT(FLOAT,posRF.AjusteMTM)										AS BS_VL_AJUSTE,

--((ValorMercado /  ValorMercado(D-1))^(252)-1)*100
CONVERT(FLOAT,														--
(																	--
	(																--
		POWER(														--
		(															--
			ValorMercado											--
			/														--
			(														--
				SELECT TOP(1) ValorMercado 							--
				FROM  PosicaoRendaFixaHistorico posRFint 			--
				WHERE 												--
				posRFint.IdPosicao = posRF.IdPosicao AND			--
				posRFint.DataHistorico < posRF.DataHistorico		--
				ORDER BY posRFint.DataHistorico DESC				--
			)														--
		),252) - 1													--
	) * 100															--
))																	AS BS_PC_TAXA_OVER, 

--'nrDiasUteis(@DataRef, TituloRendaFixa.DataVencimento))',			--
CONVERT(FLOAT,														--
(SELECT QtdDias FROM dbo.DiasUteis(														--
	@DataRef,														--
	TitRF.DataVencimento,
	1
)))																	AS BS_DD_UTEIS_ATE_VCTO,


CONVERT(FLOAT,														--
(																	--
	select top(1) TaxaDesconto from MemoriaCalculoRendaFixa where 	--
	MemoriaCalculoRendaFixa.DataAtual = posRF.DataHistorico and 	--
	MemoriaCalculoRendaFixa.IdOperacao = posRF.IdOperacao and		--
	MemoriaCalculoRendaFixa.TipoPreco in(2,3)						--
	order by TipoPreco desc											--
))																	AS BS_VL_TIR,

CONVERT(FLOAT,0)													AS BS_VL_DURATION,
CONVERT(FLOAT,0)													AS BS_VL_COUPON,
CONVERT(FLOAT,0)													AS BS_VL_M_DURATION,
CONVERT(FLOAT,0)													AS BS_VL_PU_AGIO,
CONVERT(FLOAT,posRF.AjusteMTM /  posRF.Quantidade)					AS BS_VL_PU_AJUSTE_MTM,

--nrDiasUteis(BS_DT_AQUISICAO, TituloRendaFixa.DataVencimento)),
CONVERT(FLOAT,														--
(SELECT QtdDias FROM dbo.DiasUteis(														--
	posRF.dataOperacao,														--
	TitRF.DataVencimento,
	1
)))																	AS BS_DD_UTEIS_AQUISICAO_VENCTO, 

CONVERT(FLOAT,0)													AS BS_VL_PU_NAO_DESCONTADO,
CONVERT(FLOAT,0)													AS BS_VL_PU_AO_PAR,
CONVERT(FLOAT,PUCurva)												AS BS_VL_PU_CURVA,

CONVERT(FLOAT,														--
(																	--
	select top(1) TaxaDesconto from MemoriaCalculoRendaFixa where	--
	MemoriaCalculoRendaFixa.DataAtual = posRF.DataHistorico and 	--
	MemoriaCalculoRendaFixa.IdOperacao = posRF.IdOperacao and 		--
	MemoriaCalculoRendaFixa.TipoPreco =2							--
))																	AS BS_VL_TIR_CURVA,

CONVERT(FLOAT,TaxaMTM)												AS BS_VL_TIR_MERCADO,
CONVERT(FLOAT,PUMercado)											AS BS_VL_PU_MERCADO,
CONVERT(FLOAT,TitRF.ValorNominal)									AS BS_VL_PU_NOMINAL,
CONVERT(CHAR(1),'S')												AS BS_IC_LOG_LIBERADO,
CONVERT(INT,0)														AS BS_ID_LOG_LIBERADO,
CONVERT(FLOAT,posRF.AjusteVencimento)								AS BS_VL_AJUSTE_NEG_VENC,
CONVERT(CHAR(1),'N')												AS BS_SG_TIPO_MTM,
CONVERT(CHAR(1),'')													AS BS_CD_PROVEDOR_MTM,
CONVERT(FLOAT,0)													AS BS_QT_BLOQ_01,
CONVERT(FLOAT,0)													AS BS_QT_BLOQ_03,
CONVERT(FLOAT,0)													AS BS_QT_BLOQ_05,
CONVERT(FLOAT,0)													AS BS_QT_BLOQ_14,
CONVERT(FLOAT,0)													AS BS_QT_VENDA_TERMO,
CONVERT(CHAR(1),0)													AS BS_SG_ORIGEM_CAD
FROM 
PosicaoRendaFixaHistorico posRF
LEFT JOIN TituloRendaFixa TitRF on TitRF.IdTitulo = posRF.IdTitulo
WHERE CONVERT(DATE,posRF.DataHistorico) = @DataRef

END


GO

IF NOT EXISTS(SELECT 1 FROM SYSCOLUMNS WHERE ID= OBJECT_ID('OrdemBolsa') AND NAME = 'DataOperacao')
BEGIN
	ALTER TABLE OrdemBolsa add DataOperacao Datetime;

	EXEC sp_executesql
            N'update OrdemBolsa set DataOperacao = Data'

	ALTER TABLE OrdemBolsa ALTER COLUMN DataOperacao Datetime NOT NULL ;
END

GO

IF NOT EXISTS(SELECT 1 FROM SYSCOLUMNS WHERE ID= OBJECT_ID('OrdemBMF') AND NAME = 'DataOperacao')
BEGIN
	ALTER TABLE OrdemBmf add DataOperacao Datetime;

	EXEC sp_executesql
            N'update OrdemBmf set DataOperacao = Data'

	ALTER TABLE OrdemBmf ALTER COLUMN DataOperacao Datetime NOT NULL ;
END

GO

IF NOT EXISTS(SELECT 1 FROM SYSCOLUMNS WHERE ID= OBJECT_ID('OperacaoCotista') AND NAME = 'DataRegistro')
BEGIN
	ALTER TABLE OperacaoCotista add DataRegistro Datetime;

	EXEC sp_executesql
            N'update OperacaoCotista set DataRegistro = DataOperacao'

	ALTER TABLE OperacaoCotista ALTER COLUMN DataRegistro Datetime NOT NULL ;
END

IF NOT EXISTS(SELECT 1 FROM SYSCOLUMNS WHERE ID=OBJECT_ID('OperacaoCotista') AND NAME = 'DepositoRetirada')
BEGIN
	ALTER TABLE OperacaoCotista add DepositoRetirada bit not null default 0;
END

GO

