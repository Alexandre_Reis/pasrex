
IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'Feeder') = 0
Begin
	CREATE TABLE Feeder 
	( 	IdFeeder INT PRIMARY KEY NOT NULL IDENTITY , 
		Descricao VARCHAR(50) NOT NULL, 
		Url VARCHAR(255) NULL,
		LocalJarFile VARCHAR(255) NULL,
		LocalDados VARCHAR(255) NULL
	) 

	INSERT INTO Feeder (descricao,url) VALUES ('BVMF'	,'http://www.bmfbovespa.com.br/shared/iframeBoletim.aspx?altura=3800&idioma=pt-br&url=www2.bmf.com.br/pages/portal/bmfbovespa/boletim1/TxRef1.asp');
	INSERT INTO Feeder (descricao) VALUES ('ANBIMA');
	INSERT INTO Feeder (descricao) VALUES ('SND'	);
	INSERT INTO Feeder (descricao) VALUES ('CETIP'	);
	INSERT INTO Feeder (descricao) VALUES ('Area Interna'	);
	INSERT INTO Feeder (descricao) VALUES ('BACEN'	);
end

delete from PermissaoMenu where idMEnu = 310;
insert into [dbo].[PermissaoMenu]
SELECT [IdGrupo]
	  ,310
	  ,[PermissaoLeitura]
	  ,[PermissaoAlteracao]
	  ,[PermissaoExclusao]
	  ,[PermissaoInclusao]
  FROM [dbo].[PermissaoMenu]
where IdMenu = 22280;

-- Tabela Curva RF INICIO
IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'CurvaRendaFixa') = 0
Begin
	CREATE TABLE CurvaRendaFixa 
	( 	IdCurvaRendaFixa 					INT PRIMARY KEY NOT NULL, 
		Descricao 							VARCHAR(100) NOT NULL, 
		CriterioInterpolacao 				INT NOT NULL, 
		ExpressaoTaxaZero 					INT NOT NULL, 	
		PermiteInterpolacao			 		INT NOT NULL, 
		PermiteExtrapolacao		 			INT NOT NULL,
		TipoCurva				 			INT NOT NULL,
		IdCurvaBase  						INT NULL,
		IdCurvaSpread  						INT NULL,
		TipoComposicao						INT NULL,
		IdIndice							smallint NULL
	) 	
	alter table CurvaRendaFixa add constraint CurvaRF_CurvaBase_FK FOREIGN KEY ( IdCurvaBase ) references CurvaRendaFixa(IdCurvaRendaFixa)
	alter table CurvaRendaFixa add constraint CurvaRF_CurvaSpread_FK FOREIGN KEY ( IdCurvaSpread ) references CurvaRendaFixa(IdCurvaRendaFixa)
	alter table CurvaRendaFixa add constraint Curva_Indice_FK FOREIGN KEY ( IdIndice ) references Indice(IdIndice);
end

if not exists(select 1 from syscolumns where id = object_id('SerieRendaFixa') and name = 'TipoTaxa')
BEGIN
	ALTER TABLE SerieRendaFixa ADD TipoTaxa	INT NOT NULL default '0';
	
	exec ('UPDATE SerieRendaFixa
	SET TipoTaxa = isnull(titulo.TipoMTM, 1)
	FROM SerieRendaFixa serie
	INNER JOIN TituloRendaFixa titulo ON serie.IdSerie = titulo.IdSerie')
END 	

if not exists(select 1 from syscolumns where id = object_id('SerieRendaFixa') and name = 'IdFeeder')
BEGIN	
	ALTER TABLE SerieRendaFixa ADD IdFeeder	INT NOT NULL default '5';
END 	
	
IF NOT EXISTS (select 1 from sysconstraints where object_name(id) = 'SerieRendaFixa' and object_name(constid) = 'SerieRF_Feeder_FK')
BEGIN
	alter table SerieRendaFixa add constraint SerieRF_Feeder_FK FOREIGN KEY ( IdFeeder ) references Feeder(IdFeeder)
END	

if exists(select 1 from syscolumns where id = object_id('TituloRendaFixa') and name = 'TipoMTM')
BEGIN
	ALTER TABLE TituloRendaFixa Drop Column TipoMTM;
END

IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'DataAux') = 0
Begin
	create table DataAux
	(
		idDataAux 					INT 	PRIMARY KEY identity NOT NULL, 
		dataAtual						DateTime			NOT NULL, 
	)
	insert into dataAux ( dataAtual ) values (Convert(DateTime, '01/11/2013', 103)); 
	insert into dataAux ( dataAtual ) values (Convert(DateTime, '04/11/2013', 103)); 
	insert into dataAux ( dataAtual ) values (Convert(DateTime, '05/11/2013', 103)); 
	insert into dataAux ( dataAtual ) values (Convert(DateTime, '06/11/2013', 103)); 
	insert into dataAux ( dataAtual ) values (Convert(DateTime, '07/11/2013', 103)); 
	insert into dataAux ( dataAtual ) values (Convert(DateTime, '08/11/2013', 103)); 
	insert into dataAux ( dataAtual ) values (Convert(DateTime, '11/11/2013', 103)); 
	insert into dataAux ( dataAtual ) values (Convert(DateTime, '12/11/2013', 103)); 
	insert into dataAux ( dataAtual ) values (Convert(DateTime, '13/11/2013', 103)); 
	insert into dataAux ( dataAtual ) values (Convert(DateTime, '14/11/2013', 103)); 
	insert into dataAux ( dataAtual ) values (Convert(DateTime, '15/11/2013', 103)); 
	insert into dataAux ( dataAtual ) values (Convert(DateTime, '18/11/2013', 103)); 
	insert into dataAux ( dataAtual ) values (Convert(DateTime, '19/11/2013', 103)); 
	insert into dataAux ( dataAtual ) values (Convert(DateTime, '20/11/2013', 103)); 
	insert into dataAux ( dataAtual ) values (Convert(DateTime, '21/11/2013', 103)); 
	insert into dataAux ( dataAtual ) values (Convert(DateTime, '22/11/2013', 103)); 
	insert into dataAux ( dataAtual ) values (Convert(DateTime, '25/11/2013', 103)); 
	insert into dataAux ( dataAtual ) values (Convert(DateTime, '26/11/2013', 103)); 
	insert into dataAux ( dataAtual ) values (Convert(DateTime, '27/11/2013', 103)); 
	insert into dataAux ( dataAtual ) values (Convert(DateTime, '28/11/2013', 103)); 
	insert into dataAux ( dataAtual ) values (Convert(DateTime, '29/11/2013', 103)); 
	insert into dataAux ( dataAtual ) values (Convert(DateTime, '02/12/2013', 103)); 
	insert into dataAux ( dataAtual ) values (Convert(DateTime, '03/12/2013', 103)); 
	insert into dataAux ( dataAtual ) values (Convert(DateTime, '04/12/2013', 103)); 
	insert into dataAux ( dataAtual ) values (Convert(DateTime, '05/12/2013', 103)); 
	insert into dataAux ( dataAtual ) values (Convert(DateTime, '06/12/2013', 103)); 
	insert into dataAux ( dataAtual ) values (Convert(DateTime, '09/12/2013', 103)); 
	insert into dataAux ( dataAtual ) values (Convert(DateTime, '10/12/2013', 103)); 
	insert into dataAux ( dataAtual ) values (Convert(DateTime, '11/12/2013', 103)); 
	insert into dataAux ( dataAtual ) values (Convert(DateTime, '12/12/2013', 103)); 
	insert into dataAux ( dataAtual ) values (Convert(DateTime, '13/12/2013', 103)); 
	insert into dataAux ( dataAtual ) values (Convert(DateTime, '16/12/2013', 103)); 
	insert into dataAux ( dataAtual ) values (Convert(DateTime, '17/12/2013', 103)); 
	insert into dataAux ( dataAtual ) values (Convert(DateTime, '18/12/2013', 103)); 
	insert into dataAux ( dataAtual ) values (Convert(DateTime, '19/12/2013', 103)); 
	insert into dataAux ( dataAtual ) values (Convert(DateTime, '20/12/2013', 103)); 
	insert into dataAux ( dataAtual ) values (Convert(DateTime, '23/12/2013', 103)); 
	insert into dataAux ( dataAtual ) values (Convert(DateTime, '24/12/2013', 103)); 
	insert into dataAux ( dataAtual ) values (Convert(DateTime, '25/12/2013', 103)); 
	insert into dataAux ( dataAtual ) values (Convert(DateTime, '26/12/2013', 103)); 
	insert into dataAux ( dataAtual ) values (Convert(DateTime, '27/12/2013', 103)); 
	insert into dataAux ( dataAtual ) values (Convert(DateTime, '30/12/2013', 103)); 
	insert into dataAux ( dataAtual ) values (Convert(DateTime, '31/12/2013', 103)); 
	insert into dataAux ( dataAtual ) values (Convert(DateTime, '01/01/2014', 103)); 
	insert into dataAux ( dataAtual ) values (Convert(DateTime, '02/01/2014', 103)); 
	insert into dataAux ( dataAtual ) values (Convert(DateTime, '03/01/2014', 103)); 
	insert into dataAux ( dataAtual ) values (Convert(DateTime, '06/01/2014', 103)); 
	insert into dataAux ( dataAtual ) values (Convert(DateTime, '07/01/2014', 103)); 
	insert into dataAux ( dataAtual ) values (Convert(DateTime, '08/01/2014', 103)); 
	insert into dataAux ( dataAtual ) values (Convert(DateTime, '09/01/2014', 103)); 
	insert into dataAux ( dataAtual ) values (Convert(DateTime, '10/01/2014', 103)); 
	insert into dataAux ( dataAtual ) values (Convert(DateTime, '13/01/2014', 103)); 
	insert into dataAux ( dataAtual ) values (Convert(DateTime, '14/01/2014', 103)); 
	insert into dataAux ( dataAtual ) values (Convert(DateTime, '15/01/2014', 103)); 
	insert into dataAux ( dataAtual ) values (Convert(DateTime, '16/01/2014', 103)); 
	insert into dataAux ( dataAtual ) values (Convert(DateTime, '17/01/2014', 103)); 
	insert into dataAux ( dataAtual ) values (Convert(DateTime, '20/01/2014', 103)); 
	insert into dataAux ( dataAtual ) values (Convert(DateTime, '21/01/2014', 103)); 
	insert into dataAux ( dataAtual ) values (Convert(DateTime, '22/01/2014', 103)); 
	insert into dataAux ( dataAtual ) values (Convert(DateTime, '23/01/2014', 103)); 
	insert into dataAux ( dataAtual ) values (Convert(DateTime, '24/01/2014', 103)); 
	insert into dataAux ( dataAtual ) values (Convert(DateTime, '27/01/2014', 103)); 
	insert into dataAux ( dataAtual ) values (Convert(DateTime, '28/01/2014', 103)); 
	insert into dataAux ( dataAtual ) values (Convert(DateTime, '29/01/2014', 103)); 
	insert into dataAux ( dataAtual ) values (Convert(DateTime, '30/01/2014', 103)); 
	insert into dataAux ( dataAtual ) values (Convert(DateTime, '31/01/2014', 103)); 
	insert into dataAux ( dataAtual ) values (Convert(DateTime, '03/02/2014', 103)); 
	insert into dataAux ( dataAtual ) values (Convert(DateTime, '04/02/2014', 103)); 
	insert into dataAux ( dataAtual ) values (Convert(DateTime, '05/02/2014', 103)); 
	insert into dataAux ( dataAtual ) values (Convert(DateTime, '06/02/2014', 103)); 
	insert into dataAux ( dataAtual ) values (Convert(DateTime, '07/02/2014', 103)); 
	insert into dataAux ( dataAtual ) values (Convert(DateTime, '10/02/2014', 103)); 
	insert into dataAux ( dataAtual ) values (Convert(DateTime, '11/02/2014', 103)); 
	insert into dataAux ( dataAtual ) values (Convert(DateTime, '12/02/2014', 103)); 
	insert into dataAux ( dataAtual ) values (Convert(DateTime, '13/02/2014', 103)); 
	insert into dataAux ( dataAtual ) values (Convert(DateTime, '14/02/2014', 103)); 
	insert into dataAux ( dataAtual ) values (Convert(DateTime, '17/02/2014', 103)); 
	insert into dataAux ( dataAtual ) values (Convert(DateTime, '18/02/2014', 103)); 
	insert into dataAux ( dataAtual ) values (Convert(DateTime, '19/02/2014', 103)); 
	insert into dataAux ( dataAtual ) values (Convert(DateTime, '20/02/2014', 103)); 
	insert into dataAux ( dataAtual ) values (Convert(DateTime, '21/02/2014', 103)); 
	insert into dataAux ( dataAtual ) values (Convert(DateTime, '24/02/2014', 103)); 
	insert into dataAux ( dataAtual ) values (Convert(DateTime, '25/02/2014', 103)); 
	insert into dataAux ( dataAtual ) values (Convert(DateTime, '26/02/2014', 103)); 
	insert into dataAux ( dataAtual ) values (Convert(DateTime, '27/02/2014', 103)); 
	insert into dataAux ( dataAtual ) values (Convert(DateTime, '28/02/2014', 103)); 
	insert into dataAux ( dataAtual ) values (Convert(DateTime, '03/03/2014', 103)); 
	insert into dataAux ( dataAtual ) values (Convert(DateTime, '04/03/2014', 103)); 
	insert into dataAux ( dataAtual ) values (Convert(DateTime, '05/03/2014', 103)); 
	insert into dataAux ( dataAtual ) values (Convert(DateTime, '06/03/2014', 103)); 
	insert into dataAux ( dataAtual ) values (Convert(DateTime, '07/03/2014', 103)); 
	insert into dataAux ( dataAtual ) values (Convert(DateTime, '10/03/2014', 103)); 
	insert into dataAux ( dataAtual ) values (Convert(DateTime, '11/03/2014', 103)); 
	insert into dataAux ( dataAtual ) values (Convert(DateTime, '12/03/2014', 103)); 
	insert into dataAux ( dataAtual ) values (Convert(DateTime, '13/03/2014', 103)); 
	insert into dataAux ( dataAtual ) values (Convert(DateTime, '14/03/2014', 103)); 
	insert into dataAux ( dataAtual ) values (Convert(DateTime, '17/03/2014', 103)); 
	insert into dataAux ( dataAtual ) values (Convert(DateTime, '18/03/2014', 103)); 
	insert into dataAux ( dataAtual ) values (Convert(DateTime, '19/03/2014', 103)); 
	insert into dataAux ( dataAtual ) values (Convert(DateTime, '20/03/2014', 103)); 
	insert into dataAux ( dataAtual ) values (Convert(DateTime, '21/03/2014', 103)); 
	insert into dataAux ( dataAtual ) values (Convert(DateTime, '24/03/2014', 103)); 
	insert into dataAux ( dataAtual ) values (Convert(DateTime, '25/03/2014', 103)); 
	insert into dataAux ( dataAtual ) values (Convert(DateTime, '26/03/2014', 103)); 
	insert into dataAux ( dataAtual ) values (Convert(DateTime, '27/03/2014', 103)); 
	insert into dataAux ( dataAtual ) values (Convert(DateTime, '28/03/2014', 103)); 
	insert into dataAux ( dataAtual ) values (Convert(DateTime, '31/03/2014', 103)); 
	insert into dataAux ( dataAtual ) values (Convert(DateTime, '01/04/2014', 103)); 
	insert into dataAux ( dataAtual ) values (Convert(DateTime, '02/04/2014', 103)); 
	insert into dataAux ( dataAtual ) values (Convert(DateTime, '03/04/2014', 103)); 
	insert into dataAux ( dataAtual ) values (Convert(DateTime, '04/04/2014', 103)); 
	insert into dataAux ( dataAtual ) values (Convert(DateTime, '07/04/2014', 103)); 
	insert into dataAux ( dataAtual ) values (Convert(DateTime, '08/04/2014', 103)); 
	insert into dataAux ( dataAtual ) values (Convert(DateTime, '09/04/2014', 103)); 
	insert into dataAux ( dataAtual ) values (Convert(DateTime, '10/04/2014', 103)); 
	insert into dataAux ( dataAtual ) values (Convert(DateTime, '11/04/2014', 103)); 
	insert into dataAux ( dataAtual ) values (Convert(DateTime, '14/04/2014', 103)); 
	insert into dataAux ( dataAtual ) values (Convert(DateTime, '15/04/2014', 103)); 
	insert into dataAux ( dataAtual ) values (Convert(DateTime, '16/04/2014', 103)); 
	insert into dataAux ( dataAtual ) values (Convert(DateTime, '17/04/2014', 103)); 
	insert into dataAux ( dataAtual ) values (Convert(DateTime, '18/04/2014', 103)); 
	insert into dataAux ( dataAtual ) values (Convert(DateTime, '21/04/2014', 103)); 
	insert into dataAux ( dataAtual ) values (Convert(DateTime, '22/04/2014', 103)); 
	insert into dataAux ( dataAtual ) values (Convert(DateTime, '23/04/2014', 103)); 
	insert into dataAux ( dataAtual ) values (Convert(DateTime, '24/04/2014', 103)); 
	insert into dataAux ( dataAtual ) values (Convert(DateTime, '25/04/2014', 103)); 
	insert into dataAux ( dataAtual ) values (Convert(DateTime, '28/04/2014', 103)); 
	insert into dataAux ( dataAtual ) values (Convert(DateTime, '29/04/2014', 103)); 
	insert into dataAux ( dataAtual ) values (Convert(DateTime, '30/04/2014', 103)); 
	insert into dataAux ( dataAtual ) values (Convert(DateTime, '01/05/2014', 103)); 
	insert into dataAux ( dataAtual ) values (Convert(DateTime, '02/05/2014', 103)); 
	insert into dataAux ( dataAtual ) values (Convert(DateTime, '05/05/2014', 103)); 
	insert into dataAux ( dataAtual ) values (Convert(DateTime, '06/05/2014', 103)); 
	insert into dataAux ( dataAtual ) values (Convert(DateTime, '07/05/2014', 103)); 
	insert into dataAux ( dataAtual ) values (Convert(DateTime, '08/05/2014', 103)); 
	insert into dataAux ( dataAtual ) values (Convert(DateTime, '09/05/2014', 103)); 
	insert into dataAux ( dataAtual ) values (Convert(DateTime, '12/05/2014', 103)); 
	insert into dataAux ( dataAtual ) values (Convert(DateTime, '13/05/2014', 103)); 
	insert into dataAux ( dataAtual ) values (Convert(DateTime, '14/05/2014', 103)); 
	insert into dataAux ( dataAtual ) values (Convert(DateTime, '15/05/2014', 103)); 
	insert into dataAux ( dataAtual ) values (Convert(DateTime, '16/05/2014', 103)); 
	insert into dataAux ( dataAtual ) values (Convert(DateTime, '19/05/2014', 103)); 
	insert into dataAux ( dataAtual ) values (Convert(DateTime, '20/05/2014', 103)); 
	insert into dataAux ( dataAtual ) values (Convert(DateTime, '21/05/2014', 103)); 
	insert into dataAux ( dataAtual ) values (Convert(DateTime, '22/05/2014', 103)); 
	insert into dataAux ( dataAtual ) values (Convert(DateTime, '23/05/2014', 103)); 
	insert into dataAux ( dataAtual ) values (Convert(DateTime, '26/05/2014', 103)); 
	insert into dataAux ( dataAtual ) values (Convert(DateTime, '27/05/2014', 103)); 
	insert into dataAux ( dataAtual ) values (Convert(DateTime, '28/05/2014', 103)); 
	insert into dataAux ( dataAtual ) values (Convert(DateTime, '29/05/2014', 103)); 
	insert into dataAux ( dataAtual ) values (Convert(DateTime, '30/05/2014', 103)); 
	insert into dataAux ( dataAtual ) values (Convert(DateTime, '02/06/2014', 103)); 
	insert into dataAux ( dataAtual ) values (Convert(DateTime, '03/06/2014', 103)); 
	insert into dataAux ( dataAtual ) values (Convert(DateTime, '04/06/2014', 103)); 
	insert into dataAux ( dataAtual ) values (Convert(DateTime, '05/06/2014', 103)); 
	insert into dataAux ( dataAtual ) values (Convert(DateTime, '06/06/2014', 103)); 
	insert into dataAux ( dataAtual ) values (Convert(DateTime, '09/06/2014', 103)); 
	insert into dataAux ( dataAtual ) values (Convert(DateTime, '10/06/2014', 103)); 
	insert into dataAux ( dataAtual ) values (Convert(DateTime, '11/06/2014', 103)); 
	insert into dataAux ( dataAtual ) values (Convert(DateTime, '12/06/2014', 103)); 
	insert into dataAux ( dataAtual ) values (Convert(DateTime, '13/06/2014', 103)); 
	insert into dataAux ( dataAtual ) values (Convert(DateTime, '16/06/2014', 103)); 
	insert into dataAux ( dataAtual ) values (Convert(DateTime, '17/06/2014', 103)); 
	insert into dataAux ( dataAtual ) values (Convert(DateTime, '18/06/2014', 103)); 
	insert into dataAux ( dataAtual ) values (Convert(DateTime, '19/06/2014', 103)); 
	insert into dataAux ( dataAtual ) values (Convert(DateTime, '20/06/2014', 103)); 
	insert into dataAux ( dataAtual ) values (Convert(DateTime, '23/06/2014', 103)); 
	insert into dataAux ( dataAtual ) values (Convert(DateTime, '24/06/2014', 103)); 
	insert into dataAux ( dataAtual ) values (Convert(DateTime, '25/06/2014', 103)); 
	insert into dataAux ( dataAtual ) values (Convert(DateTime, '26/06/2014', 103)); 
	insert into dataAux ( dataAtual ) values (Convert(DateTime, '27/06/2014', 103)); 
	insert into dataAux ( dataAtual ) values (Convert(DateTime, '30/06/2014', 103)); 
	insert into dataAux ( dataAtual ) values (Convert(DateTime, '01/07/2014', 103)); 
	insert into dataAux ( dataAtual ) values (Convert(DateTime, '02/07/2014', 103)); 
	insert into dataAux ( dataAtual ) values (Convert(DateTime, '03/07/2014', 103)); 
	insert into dataAux ( dataAtual ) values (Convert(DateTime, '04/07/2014', 103)); 
	insert into dataAux ( dataAtual ) values (Convert(DateTime, '07/07/2014', 103)); 
	insert into dataAux ( dataAtual ) values (Convert(DateTime, '08/07/2014', 103)); 
	insert into dataAux ( dataAtual ) values (Convert(DateTime, '09/07/2014', 103)); 
	insert into dataAux ( dataAtual ) values (Convert(DateTime, '10/07/2014', 103)); 
	insert into dataAux ( dataAtual ) values (Convert(DateTime, '11/07/2014', 103)); 
	insert into dataAux ( dataAtual ) values (Convert(DateTime, '14/07/2014', 103)); 
	insert into dataAux ( dataAtual ) values (Convert(DateTime, '15/07/2014', 103)); 
	insert into dataAux ( dataAtual ) values (Convert(DateTime, '16/07/2014', 103)); 
	insert into dataAux ( dataAtual ) values (Convert(DateTime, '17/07/2014', 103)); 
	insert into dataAux ( dataAtual ) values (Convert(DateTime, '18/07/2014', 103)); 
	insert into dataAux ( dataAtual ) values (Convert(DateTime, '21/07/2014', 103)); 
	insert into dataAux ( dataAtual ) values (Convert(DateTime, '22/07/2014', 103)); 
	insert into dataAux ( dataAtual ) values (Convert(DateTime, '23/07/2014', 103)); 
	insert into dataAux ( dataAtual ) values (Convert(DateTime, '24/07/2014', 103)); 
	insert into dataAux ( dataAtual ) values (Convert(DateTime, '25/07/2014', 103)); 
	insert into dataAux ( dataAtual ) values (Convert(DateTime, '28/07/2014', 103)); 
	insert into dataAux ( dataAtual ) values (Convert(DateTime, '29/07/2014', 103)); 
	insert into dataAux ( dataAtual ) values (Convert(DateTime, '30/07/2014', 103)); 
	insert into dataAux ( dataAtual ) values (Convert(DateTime, '31/07/2014', 103)); 
	insert into dataAux ( dataAtual ) values (Convert(DateTime, '01/08/2014', 103)); 
	insert into dataAux ( dataAtual ) values (Convert(DateTime, '04/08/2014', 103)); 
	insert into dataAux ( dataAtual ) values (Convert(DateTime, '05/08/2014', 103)); 
	insert into dataAux ( dataAtual ) values (Convert(DateTime, '06/08/2014', 103)); 
	insert into dataAux ( dataAtual ) values (Convert(DateTime, '07/08/2014', 103)); 
	insert into dataAux ( dataAtual ) values (Convert(DateTime, '08/08/2014', 103)); 
	insert into dataAux ( dataAtual ) values (Convert(DateTime, '11/08/2014', 103)); 
	insert into dataAux ( dataAtual ) values (Convert(DateTime, '12/08/2014', 103)); 
	insert into dataAux ( dataAtual ) values (Convert(DateTime, '13/08/2014', 103)); 
	insert into dataAux ( dataAtual ) values (Convert(DateTime, '14/08/2014', 103)); 
	insert into dataAux ( dataAtual ) values (Convert(DateTime, '15/08/2014', 103)); 
	insert into dataAux ( dataAtual ) values (Convert(DateTime, '18/08/2014', 103)); 
	insert into dataAux ( dataAtual ) values (Convert(DateTime, '19/08/2014', 103)); 
	insert into dataAux ( dataAtual ) values (Convert(DateTime, '20/08/2014', 103)); 
	insert into dataAux ( dataAtual ) values (Convert(DateTime, '21/08/2014', 103)); 
	insert into dataAux ( dataAtual ) values (Convert(DateTime, '22/08/2014', 103)); 
	insert into dataAux ( dataAtual ) values (Convert(DateTime, '25/08/2014', 103)); 
	insert into dataAux ( dataAtual ) values (Convert(DateTime, '26/08/2014', 103)); 
	insert into dataAux ( dataAtual ) values (Convert(DateTime, '27/08/2014', 103)); 
	insert into dataAux ( dataAtual ) values (Convert(DateTime, '28/08/2014', 103)); 
	insert into dataAux ( dataAtual ) values (Convert(DateTime, '29/08/2014', 103)); 
	insert into dataAux ( dataAtual ) values (Convert(DateTime, '01/09/2014', 103)); 
	insert into dataAux ( dataAtual ) values (Convert(DateTime, '02/09/2014', 103)); 
	insert into dataAux ( dataAtual ) values (Convert(DateTime, '03/09/2014', 103)); 
	insert into dataAux ( dataAtual ) values (Convert(DateTime, '04/09/2014', 103)); 
	insert into dataAux ( dataAtual ) values (Convert(DateTime, '05/09/2014', 103)); 
	insert into dataAux ( dataAtual ) values (Convert(DateTime, '08/09/2014', 103)); 
	insert into dataAux ( dataAtual ) values (Convert(DateTime, '09/09/2014', 103)); 
	insert into dataAux ( dataAtual ) values (Convert(DateTime, '10/09/2014', 103)); 
	insert into dataAux ( dataAtual ) values (Convert(DateTime, '11/09/2014', 103)); 
	insert into dataAux ( dataAtual ) values (Convert(DateTime, '12/09/2014', 103)); 
	insert into dataAux ( dataAtual ) values (Convert(DateTime, '15/09/2014', 103)); 
	insert into dataAux ( dataAtual ) values (Convert(DateTime, '16/09/2014', 103)); 
	insert into dataAux ( dataAtual ) values (Convert(DateTime, '17/09/2014', 103)); 
	insert into dataAux ( dataAtual ) values (Convert(DateTime, '18/09/2014', 103)); 
	insert into dataAux ( dataAtual ) values (Convert(DateTime, '19/09/2014', 103)); 
	insert into dataAux ( dataAtual ) values (Convert(DateTime, '22/09/2014', 103)); 
	insert into dataAux ( dataAtual ) values (Convert(DateTime, '23/09/2014', 103)); 
	insert into dataAux ( dataAtual ) values (Convert(DateTime, '24/09/2014', 103)); 
	insert into dataAux ( dataAtual ) values (Convert(DateTime, '25/09/2014', 103)); 
	insert into dataAux ( dataAtual ) values (Convert(DateTime, '26/09/2014', 103)); 
	insert into dataAux ( dataAtual ) values (Convert(DateTime, '29/09/2014', 103)); 
	insert into dataAux ( dataAtual ) values (Convert(DateTime, '30/09/2014', 103)); 
	insert into dataAux ( dataAtual ) values (Convert(DateTime, '01/10/2014', 103)); 
	insert into dataAux ( dataAtual ) values (Convert(DateTime, '02/10/2014', 103)); 
	insert into dataAux ( dataAtual ) values (Convert(DateTime, '03/10/2014', 103)); 
	insert into dataAux ( dataAtual ) values (Convert(DateTime, '06/10/2014', 103)); 
	insert into dataAux ( dataAtual ) values (Convert(DateTime, '07/10/2014', 103)); 
	insert into dataAux ( dataAtual ) values (Convert(DateTime, '08/10/2014', 103)); 
	insert into dataAux ( dataAtual ) values (Convert(DateTime, '09/10/2014', 103)); 
	insert into dataAux ( dataAtual ) values (Convert(DateTime, '10/10/2014', 103)); 
	insert into dataAux ( dataAtual ) values (Convert(DateTime, '13/10/2014', 103)); 
	insert into dataAux ( dataAtual ) values (Convert(DateTime, '14/10/2014', 103)); 
	insert into dataAux ( dataAtual ) values (Convert(DateTime, '15/10/2014', 103)); 
	insert into dataAux ( dataAtual ) values (Convert(DateTime, '16/10/2014', 103)); 
	insert into dataAux ( dataAtual ) values (Convert(DateTime, '17/10/2014', 103)); 
	insert into dataAux ( dataAtual ) values (Convert(DateTime, '20/10/2014', 103)); 
	insert into dataAux ( dataAtual ) values (Convert(DateTime, '21/10/2014', 103)); 
	insert into dataAux ( dataAtual ) values (Convert(DateTime, '22/10/2014', 103)); 
	insert into dataAux ( dataAtual ) values (Convert(DateTime, '23/10/2014', 103)); 
	insert into dataAux ( dataAtual ) values (Convert(DateTime, '24/10/2014', 103)); 
	insert into dataAux ( dataAtual ) values (Convert(DateTime, '27/10/2014', 103)); 
END

IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'PerfilMTM') = 0
Begin
	CREATE TABLE PerfilMTM 
	( 	IdPerfilMTM 					INT 	PRIMARY KEY identity NOT NULL, 
		DtVigencia						DateTime			NOT NULL, 
		IdPapel							INT 				NULL, 
		IdSerie							INT 				NULL, 
		IdTitulo 						INT 				NULL, 	
		IdOperacao 						INT 				NULL
	) 
	
	alter table PerfilMTM add constraint PerfilMTM_Papel_FK FOREIGN KEY ( IdPapel ) references PapelRendaFixa(IdPapel);
	alter table PerfilMTM add constraint PerfilMTM_Serie_FK FOREIGN KEY ( IdSerie ) references SerieRendaFixa(IdSerie);
	alter table PerfilMTM add constraint PerfilMTM_Titulo_FK FOREIGN KEY ( IdTitulo ) references TituloRendaFixa(IdTitulo);
	alter table PerfilMTM add constraint PerfilMTM_Operacao_FK FOREIGN KEY ( IdOperacao ) references OperacaoRendaFixa(IdOperacao)
	
	Insert into PerfilMTM ( DtVigencia, IdPapel, IdTitulo, IdOperacao,IdSerie)
	select distinct dataRef.dataAtual,
		titulo.IdPapel,
		titulo.IdTitulo,
		 null,
		titulo.idSerie  
	From TituloRendaFixa titulo, dataAux dataRef
	where titulo.idSerie is not null;
end

IF OBJECT_ID('DataAux') Is Not Null
Begin
	drop table DataAux;
End

delete from PermissaoMenu where idMEnu = 3960;
insert into [dbo].[PermissaoMenu]
SELECT [IdGrupo]
      ,3960
      ,[PermissaoLeitura]
      ,[PermissaoAlteracao]
      ,[PermissaoExclusao]
      ,[PermissaoInclusao]
  FROM [dbo].[PermissaoMenu]
  where IdMenu = 22280;
GO

IF OBJECT_ID('SerieRendaFixa_TituloRendaFixa_FK1') Is Not Null
Begin
	ALTER TABLE TituloRendaFixa 
	DROP CONSTRAINT SerieRendaFixa_TituloRendaFixa_FK1;
End

if exists(select 1 from syscolumns where id = object_id('TituloRendaFixa') and name = 'IdSerie')
BEGIN
	ALTER TABLE TituloRendaFixa Drop Column IdSerie;
END

IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'MTMManual') = 0
Begin
	CREATE TABLE MTMManual 
	( 	
		IdOperacao						INT 				NOT NULL,
		DtVigencia						DateTime 			NOT NULL, 
		Taxa252 						Decimal(25,12)		NULL, 
		PuMTM							Decimal(25,12)		NULL	
		CONSTRAINT PK_MTMManual 		primary key(DtVigencia, IdOperacao)	
	) 
end	

delete from PermissaoMenu where idMEnu = 3940;
insert into [dbo].[PermissaoMenu]
SELECT [IdGrupo]
      ,3940
      ,[PermissaoLeitura]
      ,[PermissaoAlteracao]
      ,[PermissaoExclusao]
      ,[PermissaoInclusao]
  FROM [dbo].[PermissaoMenu]
  where IdMenu = 22280;
GO
--Tabela MTMManual FIM

IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'TaxaSerie') = 0
Begin
	CREATE TABLE TaxaSerie 
	( 	
		IdSerie							INT 				NOT NULL,
		Data							DateTime 			NOT NULL, 
		Taxa	 						Decimal(25,12)		NOT NULL,  
		DigitadoImportado 				INT					NOT NULL	
		CONSTRAINT PK_TaxaSerie 		primary key(Data, IdSerie)	
	) 
end 

delete from PermissaoMenu where idMEnu = 3770;
insert into [dbo].[PermissaoMenu]
SELECT [IdGrupo]
      ,3770
      ,[PermissaoLeitura]
      ,[PermissaoAlteracao]
      ,[PermissaoExclusao]
      ,[PermissaoInclusao]
  FROM [dbo].[PermissaoMenu]
  where IdMenu = 22280;
GO

if NOT exists(select 1 from syscolumns where id = object_id('CotacaoSerie') and name = 'DigitadoImportado')
BEGIN
	ALTER TABLE CotacaoSerie ADD DigitadoImportado INT NOT NULL default '2';
END	

IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'VigenciaCategoria') = 0
Begin
	CREATE TABLE VigenciaCategoria 
	( 	
		IdVigenciaCategoria				 INT 	PRIMARY KEY identity NOT NULL, 
		DataVigencia					 DateTime 			NOT NULL, 
		IdOperacao						 INT 				NOT NULL, 
		TipoVigenciaCategoria 			 INT				NOT NULL	
	) 
end	

delete from PermissaoMenu where idMEnu = 3980;
insert into [dbo].[PermissaoMenu]
SELECT [IdGrupo]
      ,3980
      ,[PermissaoLeitura]
      ,[PermissaoAlteracao]
      ,[PermissaoExclusao]
      ,[PermissaoInclusao]
  FROM [dbo].[PermissaoMenu]
  where IdMenu = 22280;
GO

if NOT exists(select 1 from syscolumns where id = object_id('Carteira') and name = 'CalculaMTM')
BEGIN
	ALTER TABLE Carteira ADD CalculaMTM INT NULL;
END

IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'TaxaCurva') = 0
Begin
	CREATE TABLE TaxaCurva 
	( 	
		IdCurvaRendaFixa				INT 				NOT NULL,
		"DataBase"						DateTime 			NOT NULL, 
		DataVertice						DateTime 			NOT NULL, 
		CodigoVertice					VARCHAR(100) 		NULL, 
		PrazoDU							INT 				NOT NULL, 
		PrazoDC							INT 				NOT NULL, 
		Taxa	 						Decimal(25,12)		NOT NULL	
		CONSTRAINT PK_TaxaCurva 		primary key("DataBase", IdCurvaRendaFixa,DataVertice)	
	) 	
	alter table TaxaCurva add constraint TaxaCurva_CurvaRF_FK FOREIGN KEY ( IdCurvaRendaFixa ) references CurvaRendaFixa(IdCurvaRendaFixa);
end	

delete from PermissaoMenu where idMEnu = 3970;
insert into [dbo].[PermissaoMenu]
SELECT [IdGrupo]
      ,3970
      ,[PermissaoLeitura]
      ,[PermissaoAlteracao]
      ,[PermissaoExclusao]
      ,[PermissaoInclusao]
  FROM [dbo].[PermissaoMenu]
  where IdMenu = 22280;
GO

if NOT exists(select 1 from syscolumns where id = object_id('Indice') and name = 'IdFeeder')
BEGIN
	ALTER TABLE Indice ADD IdFeeder INT not NULL default '5';
END	

IF NOT EXISTS (select 1 from sysconstraints where object_name(id) = 'Indice' and object_name(constid) = 'Indice_Feeder_FK')
BEGIN
	alter table Indice add constraint Indice_Feeder_FK FOREIGN KEY ( IdFeeder ) references Feeder(IdFeeder);
END

IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'MemoriaCalculoRendaFixa') = 0
Begin
	CREATE TABLE MemoriaCalculoRendaFixa
	(
		DataAtual 		 		 DateTime		NOT NULL,
		IdOperacao		 		 INT 			NOT NULL,
		TipoPreco 		 		 INT 			NOT NULL,
		TipoProcessamento		 INT 			NOT NULL,
		DataFluxo		 		 DateTime 		NOT NULL,
		TaxaDesconto	 		 Decimal(25,12)	NULL,
		ValorVencimento	 		 Decimal(25,8)	NULL,
		ValorPresente	 		 Decimal(25,8)	NULL,		
		ValorNominalAjustado	 Decimal(25,8)	NULL,
		ValorNominal			 Decimal(25,8)	NULL
		CONSTRAINT PK_MemoriaCalculoRendaFixa 		primary key(DataAtual, IdOperacao, TipoPreco, TipoProcessamento, DataFluxo)	
	)
	alter table MemoriaCalculoRendaFixa add constraint MemCalRF_Operacao_FK FOREIGN KEY ( IdOperacao ) references OperacaoRendaFixa(IdOperacao);
end	

delete from PermissaoMenu where idMEnu = 3990;
insert into [dbo].[PermissaoMenu]
SELECT [IdGrupo]
      ,3990
      ,[PermissaoLeitura]
      ,[PermissaoAlteracao]
      ,[PermissaoExclusao]
      ,[PermissaoInclusao]
  FROM [dbo].[PermissaoMenu]
  where IdMenu = 22280;
GO

delete from PermissaoMenu where idMEnu = 390;
insert into [dbo].[PermissaoMenu]
SELECT [IdGrupo]
      ,390
      ,[PermissaoLeitura]
      ,[PermissaoAlteracao]
      ,[PermissaoExclusao]
      ,[PermissaoInclusao]
  FROM [dbo].[PermissaoMenu]
  where IdMenu = 22280;
GO

IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'LocalCustodia') = 0
Begin
	CREATE TABLE [dbo].[LocalCustodia]
	(
		[IdLocalCustodia] [int] PRIMARY KEY  IDENTITY(1,1) NOT NULL,
		[Descricao] [varchar](60) NOT NULL,
		[Codigo] [varchar](250) NOT NULL,
		[Cnpj] [varchar](250) NOT NULL,
	)
	ALTER TABLE [dbo].[LocalCustodia] ADD  DEFAULT ('') FOR [Codigo]
	ALTER TABLE [dbo].[LocalCustodia] ADD  DEFAULT ('') FOR [Cnpj]
end

if NOT exists(select 1 from syscolumns where id = object_id('PosicaoRendaFixa') and name = 'ValorCurvaVencimento')
BEGIN
	ALTER TABLE PosicaoRendaFixa ADD ValorCurvaVencimento Decimal(25,2) NULL default '0';
end
	
if NOT exists(select 1 from syscolumns where id = object_id('PosicaoRendaFixa') and name = 'PUCurvaVencimento')
BEGIN	
	ALTER TABLE PosicaoRendaFixa ADD PUCurvaVencimento Decimal(25,8) NULL default '0';
end

if NOT exists(select 1 from syscolumns where id = object_id('PosicaoRendaFixa') and name = 'AjusteMTM')
BEGIN
	ALTER TABLE PosicaoRendaFixa ADD AjusteMTM Decimal(25,8) NULL default '0';
END	

if NOT exists(select 1 from syscolumns where id = object_id('PosicaoRendaFixa') and name = 'AjusteVencimento')
BEGIN
	ALTER TABLE PosicaoRendaFixa ADD AjusteVencimento Decimal(25,8) NULL default '0';
END	

if NOT exists(select 1 from syscolumns where id = object_id('PosicaoRendaFixa') and name = 'TaxaMTM')
BEGIN
	ALTER TABLE PosicaoRendaFixa ADD TaxaMTM Decimal(25,8) NULL default '0';
END	

if NOT exists(select 1 from syscolumns where id = object_id('PosicaoRendaFixaAbertura') and name = 'ValorCurvaVencimento')
BEGIN
	ALTER TABLE PosicaoRendaFixaAbertura ADD ValorCurvaVencimento Decimal(25,2) NULL default '0';
END

if NOT exists(select 1 from syscolumns where id = object_id('PosicaoRendaFixaAbertura') and name = 'PUCurvaVencimento')
BEGIN	
	ALTER TABLE PosicaoRendaFixaAbertura ADD PUCurvaVencimento Decimal(25,8) NULL default '0';
END

if NOT exists(select 1 from syscolumns where id = object_id('PosicaoRendaFixaAbertura') and name = 'AjusteMTM')
BEGIN	
	ALTER TABLE PosicaoRendaFixaAbertura ADD AjusteMTM Decimal(25,8) NULL default '0';
END	

if NOT exists(select 1 from syscolumns where id = object_id('PosicaoRendaFixaAbertura') and name = 'AjusteVencimento')
BEGIN
	ALTER TABLE PosicaoRendaFixaAbertura ADD AjusteVencimento Decimal(25,8) NULL default '0';
END

if NOT exists(select 1 from syscolumns where id = object_id('PosicaoRendaFixaAbertura') and name = 'TaxaMTM')
BEGIN	
	ALTER TABLE PosicaoRendaFixaAbertura ADD TaxaMTM Decimal(25,8) NULL default '0';
END	

if NOT exists(select 1 from syscolumns where id = object_id('PosicaoRendaFixaHistorico') and name = 'ValorCurvaVencimento')
BEGIN
	ALTER TABLE PosicaoRendaFixaHistorico ADD ValorCurvaVencimento Decimal(25,2) NULL default '0';
END

if NOT exists(select 1 from syscolumns where id = object_id('PosicaoRendaFixaHistorico') and name = 'PUCurvaVencimento')
BEGIN
	ALTER TABLE PosicaoRendaFixaHistorico ADD PUCurvaVencimento Decimal(25,8) NULL default '0';
END

if NOT exists(select 1 from syscolumns where id = object_id('PosicaoRendaFixaHistorico') and name = 'AjusteMTM')
BEGIN	
	ALTER TABLE PosicaoRendaFixaHistorico ADD AjusteMTM Decimal(25,8) NULL default '0';
END

if NOT exists(select 1 from syscolumns where id = object_id('PosicaoRendaFixaHistorico') and name = 'AjusteVencimento')
BEGIN	
	ALTER TABLE PosicaoRendaFixaHistorico ADD AjusteVencimento Decimal(25,8) NULL default '0';
END

if NOT exists(select 1 from syscolumns where id = object_id('PosicaoRendaFixaHistorico') and name = 'TaxaMTM')
BEGIN	
	ALTER TABLE PosicaoRendaFixaHistorico ADD TaxaMTM Decimal(25,8) NULL default '0';
END

IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'TipoDePara') = 0
begin
	CREATE TABLE TipoDePara 
	( 	IdTipoDePara 					INT 				PRIMARY KEY identity NOT NULL, 
		TipoCampo						INT 				NOT NULL, 
		Descricao 						VARCHAR(100) 		NOT NULL, 
		Observacao 						VARCHAR(255) 		NULL,
		EnumTipoDePara					INT 				NULL	
	) 
end
--Tabela TipoDePara Fim

IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'DePara') = 0
begin
	CREATE TABLE DePara 
	( 	IdDePara	 					INT 				PRIMARY KEY identity NOT NULL, 
		IdTipoDePara 					INT 				NOT NULL, 
		CodigoInterno	 				VARCHAR(100) 		NOT NULL, 
		CodigoExterno					VARCHAR(100) 		NOT NULL	
	) 
	alter table DePara add constraint DePara_TipoDePara_FK FOREIGN KEY ( IdTipoDePara ) references TipoDePara(IdTipoDePara)
end

delete from PermissaoMenu where idMEnu = 14260;
insert into [dbo].[PermissaoMenu]
SELECT [IdGrupo]
      ,14260
      ,[PermissaoLeitura]
      ,[PermissaoAlteracao]
      ,[PermissaoExclusao]
      ,[PermissaoInclusao]
  FROM [dbo].[PermissaoMenu]
  where IdMenu = 14200;
GO

delete from PermissaoMenu where idMEnu = 14261;
insert into [dbo].[PermissaoMenu]
SELECT [IdGrupo]
      ,14261
      ,[PermissaoLeitura]
      ,[PermissaoAlteracao]
      ,[PermissaoExclusao]
      ,[PermissaoInclusao]
  FROM [dbo].[PermissaoMenu]
  where IdMenu = 22280;
GO

delete from PermissaoMenu where idMEnu = 14262;
insert into [dbo].[PermissaoMenu]
SELECT [IdGrupo]
      ,14262
      ,[PermissaoLeitura]
      ,[PermissaoAlteracao]
      ,[PermissaoExclusao]
      ,[PermissaoInclusao]
  FROM [dbo].[PermissaoMenu]
  where IdMenu = 22280;
GO
--Tabela DePara Fim

if NOT exists(select 1 from syscolumns where id = object_id('TituloRendaFixa') and name = 'PremioRebate')
BEGIN
	ALTER TABLE TituloRendaFixa ADD PremioRebate decimal(25,8)
END	

if NOT exists(select 1 from syscolumns where id = object_id('TituloRendaFixa') and name = 'ExpTaxaEmissao')
BEGIN
	ALTER TABLE TituloRendaFixa ADD ExpTaxaEmissao int
END	

if NOT exists(select 1 from syscolumns where id = object_id('TituloRendaFixa') and name = 'Periodicidade')
BEGIN
	ALTER TABLE TituloRendaFixa ADD Periodicidade int
END	

if NOT exists(select 1 from syscolumns where id = object_id('TituloRendaFixa') and name = 'CriterioAmortizacao')
BEGIN
	ALTER TABLE TituloRendaFixa ADD CriterioAmortizacao INT NOT NULL default '2';
END

IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'MemoriaCalculoRendaFixa') = 0
begin
	CREATE TABLE MemoriaCalculoRendaFixa
	(
		DataAtual 		 		 DateTime		NOT NULL,
		IdOperacao		 		 INT 			NOT NULL,
		TipoPreco 		 		 INT 			NOT NULL,
		TipoProcessamento		 INT 			NOT NULL,
		DataFluxo		 		 DateTime 		NOT NULL,
		TaxaDesconto	 		 Decimal(25,12)	NULL,
		ValorVencimento	 		 Decimal(25,8)	NULL,
		ValorPresente	 		 Decimal(25,8)	NULL,		
		ValorNominalAjustado	 Decimal(25,8)	NULL,
		ValorNominal			 Decimal(25,8)	NULL
		CONSTRAINT PK_MemoriaCalculoRendaFixa 		primary key(DataAtual, IdOperacao, TipoPreco, TipoProcessamento, DataFluxo)	
	)
	alter table MemoriaCalculoRendaFixa add constraint MemCalRF_Operacao_FK FOREIGN KEY ( IdOperacao ) references OperacaoRendaFixa(IdOperacao);

	delete from PermissaoMenu where idMEnu = 3990;
	insert into [dbo].[PermissaoMenu]
	SELECT [IdGrupo]
		  ,3990
		  ,[PermissaoLeitura]
		  ,[PermissaoAlteracao]
		  ,[PermissaoExclusao]
		  ,[PermissaoInclusao]
	  FROM [dbo].[PermissaoMenu]
	  where IdMenu = 22280;
end

IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'PrazoMedio') = 0
begin
	CREATE TABLE PrazoMedio
	(
		IdPrazoMedio			 INT 			NOT NULL PRIMARY KEY identity,
		DataAtual 		 		 DateTime		NOT NULL,
		IdCliente		 		 INT 			NOT NULL,
		Descricao 		 		 VARCHAR(255) 	NOT NULL,
		PrazoMedioAtivo		 	 INT 			NOT NULL,
		PrazoMedio		 		 Decimal(10,0)	NOT NULL,
		ValorPosicao	 		 Decimal(25,2)	NOT NULL
	)
END	

if not exists(select 1 from syscolumns where id = object_id('Carteira') and name = 'CalculaPrazoMedio')
BEGIN
	ALTER TABLE dbo.Carteira ADD CalculaPrazoMedio char(1) NOT NULL DEFAULT 'N'
END	
	
--CREATE VIEW [dbo].[SB_HIST_INDEXES] 
--AS 	
--	SELECT 1 ID, 
--			dePara.CodigoExterno as IDINDEX,
--			CASE WHEN CAST(dePara.CodigoExterno AS INT) = 127 --IPCA
--				THEN Convert(datetime, '15/' + convert(varchar(2),DATEPART(month,DATEADD(month,1,Data)))+ '/' +convert(varchar(4),DATEPART(year,DATEADD(month,1,Data))), 103)
--				  WHEN CAST(dePara.CodigoExterno AS INT) = 126 --IGPM
--				THEN Convert(datetime, '01/' + convert(varchar(2),DATEPART(month,Data))+ '/' +convert(varchar(4),DATEPART(year,Data)), 103)
--			ELSE Data
--			END as REFERENCEDATE,
--			CASE WHEN CAST(dePara.CodigoExterno AS INT) = 127 --IPCA
--				THEN Convert(datetime, '15/' + convert(varchar(2),DATEPART(month,DATEADD(month,1,Data)))+ '/' +convert(varchar(4),DATEPART(year,DATEADD(month,1,Data))), 103)
--				 WHEN CAST(dePara.CodigoExterno AS INT) = 126 --IGPM
--				THEN Convert(datetime, '01/' + convert(varchar(2),DATEPART(month,Data))+ '/' +convert(varchar(4),DATEPART(year,Data)), 103)
--			ELSE Data
--			END as RELEASEDATE,
--			CASE WHEN CAST(dePara.CodigoExterno AS INT) IN (126, 127, 3, 4)  --IGPM / IPCA / IGPDI / INPC 
--				THEN Valor
--				ELSE NULL
--			END as INDEXESINDEX,
--			CASE WHEN CAST(dePara.CodigoExterno AS INT) NOT IN (126, 127, 3, 4) --IGPM / IPCA / IGPDI / INPC 
--				THEN Valor
--				ELSE 0
--			END as INDEXESVALUE,
--			1 as VALID 
--		FROM FIN_GPS.[dbo].[CotacaoIndice] 
--		INNER JOIN (SELECT dePara.* 
--					FROM  FIN_GPS.dbo.DePara dePara
--					INNER JOIN  FIN_GPS.dbo.TipoDePara tpDePara ON tpDePara.idTipoDePara = dePara.idTipoDePara
--					WHERE tpDePara.EnumTipoDePara = 1 /* Fincs - Indexador */
--					and tpDePara.TipoCampo = 1 /* Inteiro */) dePara 
--					ON (dePara.CodigoInterno = idIndice)
--GO 

IF (SELECT count(*) FROM TipoDePara WHERE TipoCampo = 1 AND EnumTipoDePara = 1) = 0
Begin
	Insert into TipoDePara values( 1, 'Fincs - Indexador', null, 1);
	INSERT INTO DePara VALUES ( 1, 1, 123); --CDI
	INSERT INTO DePara VALUES ( 1, 10, 130); --SELIC
	INSERT INTO DePara VALUES ( 1, 61,127); --IPCA)
	INSERT INTO DePara VALUES ( 1, 60, 126); --IGPM)
	INSERT INTO DePara VALUES ( 1, 62, 4); --INPC)
	INSERT INTO DePara VALUES ( 1, 63, 3); --IGPDI)
End

IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'RepactuacaoRendaFixa') = 0
Begin
	CREATE TABLE RepactuacaoRendaFixa
	(
		IdRepactuacao			 INT 			NOT NULL PRIMARY KEY identity,
		IdTituloOriginal		 INT 			NOT NULL,
		IdTituloNovo		     INT 			NOT NULL,
		DataEvento			 DateTime 		NOT NULL
	)
	alter table RepactuacaoRendaFixa add constraint Repac_PapelOrig_FK FOREIGN KEY ( IdTituloOriginal ) references TituloRendaFixa(IdTitulo);
	alter table RepactuacaoRendaFixa add constraint Repac_PapelNovo_FK FOREIGN KEY ( IdTituloNovo ) references TituloRendaFixa(IdTitulo);
END	

delete from PermissaoMenu where idMEnu = 3950;
insert into [dbo].[PermissaoMenu]
SELECT [IdGrupo]
      ,3950
      ,[PermissaoLeitura]
      ,[PermissaoAlteracao]
      ,[PermissaoExclusao]
      ,[PermissaoInclusao]
  FROM [dbo].[PermissaoMenu]
  where IdMenu = 14200;
GO

if not exists(select 1 from syscolumns where id = object_id('TituloRendaFixa') and name = 'PermiteRepactuacao')
BEGIN
	ALTER TABLE dbo.TituloRendaFixa ADD	PermiteRepactuacao char(1) NOT NULL DEFAULT 'N'
END	

IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'Rentabilidade') = 0
Begin
	CREATE TABLE Rentabilidade 
	( 	
		IdRentabilidade							INT 				PRIMARY KEY identity NOT NULL, 
		Data	 		 		 				DateTime		    NOT NULL,						
		TipoAtivo		 	 					INT 				NOT NULL,
		IdCliente								INT					NOT NULL,
		CodigoAtivo 							VARCHAR(255) 		NULL, 
		CodigoMoedaAtivo 						VARCHAR(10) 		NULL,
		PatrimonioInicialMoedaAtivo				Decimal(25,12)		NULL,
		QuantidadeInicialAtivo					Decimal(25,12)		NULL,
		ValorFinanceiroEntradaMoedaAtivo		Decimal(25,2)		NULL,
		QuantidadeTotalEntradaAtivo				Decimal(25,12)		NULL,
		ValorFinanceiroSaidaMoedaAtivo			Decimal(25,2)		NULL,
		QuantidadeTotalSaidaAtivo				Decimal(25,12)		NULL,
		ValorFinanceiroRendasMoedaAtivo			Decimal(25,2)		NULL,	
		PatrimonioFinalMoedaAtivo				Decimal(25,12)		NULL,	
		QuantidadeFinalAtivo					Decimal(25,12)		NULL,
		RentabilidadeMoedaAtivo					Decimal(25,12)		NULL,		
		CodigoMoedaPortfolio					VARCHAR(10) 		NULL,
		PatrimonioInicialMoedaPortfolio			Decimal(25,12)		NULL,
		ValorFinanceiroEntradaMoedaPortfolio	Decimal(25,2)		NULL,
		ValorFinanceiroSaidaMoedaPortfolio      Decimal(25,2)		NULL,
		ValorFinanceiroRendasMoedaPortfolio     Decimal(25,2)		NULL,
		PatrimonioFinalMoedaPortfolio           Decimal(25,2)		NULL,
		RentabilidadeMoedaPortfolio             Decimal(25,12)		NULL,
		IdTituloRendaFixa						INT					NULL,
		IdOperacaoSwap							INT					NULL,
		IdCarteira								INT					NULL,
		CdAtivoBolsa							varchar(20)			NULL,
		CdAtivoBMF								varchar(20)			NULL,
		SerieBMF								varchar(5)			NULL
	) 
END


if not exists(select 1 from syscolumns where id = object_id('TituloRendaFixa') and name = 'ExecutaProRataEmissao')
BEGIN
	ALTER TABLE TituloRendaFixa ADD ExecutaProRataEmissao char(1) NOT NULL DEFAULT 'N'
END

if not exists(select 1 from syscolumns where id = object_id('TituloRendaFixa') and name = 'TipoProRata')
BEGIN
	ALTER TABLE TituloRendaFixa ADD TipoProRata int NOT NULL DEFAULT '0'
END

if exists(select 1 from syscolumns where id = object_id('PerfilProcessamento') and name = 'AberturaIndexada')
BEGIN
	ALTER TABLE PerfilProcessamento DROP COLUMN AberturaIndexada
END	

if not exists(select 1 from syscolumns where id = object_id('PerfilProcessamento') and name = 'AberturaAutomatica')
BEGIN
	ALTER TABLE PerfilProcessamento ADD AberturaAutomatica char(1) NOT NULL DEFAULT 'N'
END

IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'AtivoMercadoFiltro') = 0
Begin
	Create Table AtivoMercadoFiltro
	(
			IdAtivoMercadoFiltro INT 	PRIMARY KEY identity NOT NULL, 
			CompositeKey	VARCHAR(50),
			IdAtivo			VARCHAR(50),
			Descricao		VARCHAR(255),
			IdCliente		INT,
			TipoMercado		INT
	)
END	

delete from PermissaoMenu where idMEnu = 15250;
insert into [dbo].[PermissaoMenu]
SELECT [IdGrupo]
      ,15250
      ,[PermissaoLeitura]
      ,[PermissaoAlteracao]
      ,[PermissaoExclusao]
      ,[PermissaoInclusao]
  FROM [dbo].[PermissaoMenu]
  where IdMenu = 22280;
GO

delete from PermissaoMenu where idMEnu = 15260;
insert into [dbo].[PermissaoMenu]
SELECT [IdGrupo]
      ,15260
      ,[PermissaoLeitura]
      ,[PermissaoAlteracao]
      ,[PermissaoExclusao]
      ,[PermissaoInclusao]
  FROM [dbo].[PermissaoMenu]
  where IdMenu = 22280;
GO

if not exists(select 1 from syscolumns where id = object_id('MemoriaCalculoRendaFixa') and name = 'TipoEventoTitulo')
BEGIN
	ALTER TABLE MemoriaCalculoRendaFixa ADD TipoEventoTitulo int NOT NULL DEFAULT '1'
END

IF EXISTS (select 1 from sysconstraints where object_name(id) = 'MemoriaCalculoRendaFixa' and object_name(constid) = 'PK_MemoriaCalculoRendaFixa')
BEGIN
	alter table MemoriaCalculoRendaFixa drop constraint PK_MemoriaCalculoRendaFixa;
	alter table MemoriaCalculoRendaFixa add constraint PK_MemoriaCalculoRendaFixa primary key(DataAtual, IdOperacao, TipoPreco, TipoProcessamento, TipoEventoTitulo, DataFluxo)	
END	

IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'TipoParametroLiqAvancado') = 0
Begin
	Create Table TipoParametroLiqAvancado
	(
			IdParametroLiqAvancado 			VARCHAR(10) 	PRIMARY KEY NOT NULL, 
			SignificadoParametro			VARCHAR(50)		NOT NULL,
			VariacaoComplemento				VARCHAR(50)		NOT NULL,
			SignificadoComplemento			VARCHAR(255)	NOT NULL,
			Exemplo							VARCHAR(255)	NOT NULL
	)
	INSERT INTO TipoParametroLiqAvancado VALUES ('D','Dias úteis','+ ou - N','Quantidade de dias úteis','D+3 Representa 3 dias úteis')
	INSERT INTO TipoParametroLiqAvancado VALUES ('C','Dias Corridos','+ ou - N','Quantidade de dias corridos','D+3 Representa 3 dias corridos')
	INSERT INTO TipoParametroLiqAvancado VALUES ('W','Ocorrência periódica em contagem semanal','N/M','N indica o início da ocorrência, onde 0 (zero) representaria o período vigente, 1 representaria o próximo período, e assim por diante. M indica a quantidade de dias dentro do período.','W0/4 Representa o quarto dia útil da semana vigente')
	INSERT INTO TipoParametroLiqAvancado VALUES ('F','Ocorrência periódica em contagem quinzenal','N/M','N indica o início da ocorrência, onde 0 (zero) representaria o período vigente, 1 representaria o próximo período, e assim por diante. M indica a quantidade de dias dentro do período.','F1/5 Representa o quinto dia útil da quinzena seguinte')
	INSERT INTO TipoParametroLiqAvancado VALUES ('M','Ocorrência periódica em contagem mensal','N/M','N indica o início da ocorrência, onde 0 (zero) representaria o período vigente, 1 representaria o próximo período, e assim por diante. M indica a quantidade de dias dentro do período.','M0/31 Representa o último dia útil do mês vigente (31 representa o último dia útil, por convenção)')
	INSERT INTO TipoParametroLiqAvancado VALUES ('T','Ocorrência periódica em contagem trimestral','N/M','N indica o início da ocorrência, onde 0 (zero) representaria o período vigente, 1 representaria o próximo período, e assim por diante. M indica a quantidade de dias dentro do período.','T0/31 Representa o último dia útil do trimestre vigente (31 representa o último dia útil, por convenção)')
	INSERT INTO TipoParametroLiqAvancado VALUES ('S','Ocorrência periódica em contagem semestral','N/M','N indica o início da ocorrência, onde 0 (zero) representaria o período vigente, 1 representaria o próximo período, e assim por diante. M indica a quantidade de dias dentro do período.','S0/10 Representa o décimo dia útil do semestre vigente ')	
END	

IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'ParametroLiqAvancadoAnalitico') = 0
Begin
	Create Table ParametroLiqAvancadoAnalitico
	(
			IdParametroLiqAvancado 			VARCHAR(10) 	NOT NULL, 
			IdCarteira						INT				NOT NULL,
			DataVigencia 					datetime		NOT NULL,
			TipoData						int				NOT NULL,
			TipoOperacao					int				NOT NULL,
			ParametroUm						VARCHAR(50)		NULL,
			ParametroDois					VARCHAR(50)		NULL,
			ParametroComposto				VARCHAR(50)		NULL,
			OrdemProcessamento 				int				NOT NULL
			CONSTRAINT PK_ParametroLiqAvancadoAnalico	primary key(IdParametroLiqAvancado, IdCarteira, DataVigencia, TipoData, TipoOperacao)	
	)	
END	

IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'ParametroLiqAvancado') = 0
Begin
	Create Table ParametroLiqAvancado
	(
			IdCarteira						INT				NOT NULL,
			DataVigencia 					datetime		NOT NULL,
			TipoData						int				NOT NULL,
			TipoOperacao					int				NOT NULL,			
			Parametro						VARCHAR(150)	NOT NULL,
			ParametroValido					char(1) 		NOT NULL DEFAULT 'N'			
			CONSTRAINT PK_ParametroLiqAvancado	primary key(IdCarteira, DataVigencia, TipoData, TipoOperacao)	
	)	
END	

delete from PermissaoMenu where idMEnu = 7725;
insert into [dbo].[PermissaoMenu]
SELECT [IdGrupo]
      ,7725
      ,[PermissaoLeitura]
      ,[PermissaoAlteracao]
      ,[PermissaoExclusao]
      ,[PermissaoInclusao]
  FROM [dbo].[PermissaoMenu]
  where IdMenu = 22280;
GO

if not exists(select 1 from syscolumns where id = object_id('Carteira') and name = 'DiasAposResgate')
BEGIN
	ALTER TABLE Carteira ADD DiasAposResgate int NOT NULL DEFAULT '0'
END

if not exists(select 1 from syscolumns where id = object_id('Carteira') and name = 'ProjecaoIRComeCotas')
BEGIN
	ALTER TABLE Carteira ADD ProjecaoIRComeCotas tinyint NOT NULL DEFAULT '2'
END

if not exists(select 1 from syscolumns where id = object_id('Carteira') and name = 'DiasAposComeCotas')
BEGIN
	ALTER TABLE Carteira ADD DiasAposComeCotas int NOT NULL DEFAULT '0'
END

if not exists(select 1 from syscolumns where id = object_id('Carteira') and name = 'RealizaCompensacaoDePrejuizo')
BEGIN
	ALTER TABLE Carteira ADD RealizaCompensacaoDePrejuizo char(1) NOT NULL DEFAULT 'N'
END

if not exists(select 1 from syscolumns where id = object_id('TituloRendaFixa') and name = 'DefasagemLiquidacao')
BEGIN
	ALTER TABLE TituloRendaFixa ADD DefasagemLiquidacao int NOT NULL DEFAULT '0'
END

if not exists(select 1 from syscolumns where id = object_id('OperacaoRendaFixa') and name = 'PercentualPuFace')
BEGIN
	ALTER TABLE OperacaoRendaFixa ADD PercentualPuFace Decimal(25,12) NOT NULL DEFAULT '0'
END

if not exists(select 1 from syscolumns where id = object_id('TituloRendaFixa') and name = 'ProRataLiquidacao')
BEGIN
	ALTER TABLE dbo.TituloRendaFixa ADD	ProRataLiquidacao char(1) NOT NULL DEFAULT 'N'
END	

if not exists(select 1 from syscolumns where id = object_id('TituloRendaFixa') and name = 'ProRataEmissaoDia')
BEGIN
	ALTER TABLE TituloRendaFixa ADD ProRataEmissaoDia int NULL DEFAULT '0'
END

if not exists(select 1 from syscolumns where id = object_id('TituloRendaFixa') and name = 'AtivoRegra')
BEGIN
	ALTER TABLE TituloRendaFixa ADD AtivoRegra VARCHAR(10) NULL 
END

if not exists(select 1 from syscolumns where id = object_id('TituloRendaFixa') and name = 'DataInicioCorrecao')
BEGIN
	ALTER TABLE TituloRendaFixa ADD DataInicioCorrecao datetime NULL 
END

if not exists(select 1 from syscolumns where id = object_id('TituloRendaFixa') and name = 'DefasagemMeses')
BEGIN
	ALTER TABLE TituloRendaFixa ADD DefasagemMeses int NULL DEFAULT '0'
END

if not exists(select 1 from syscolumns where id = object_id('TituloRendaFixa') and name = 'EJuros')
BEGIN
	ALTER TABLE TituloRendaFixa ADD EJuros int NULL DEFAULT '6'
END

IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'OperacaoCotistaAux') = 0
Begin
	CREATE TABLE [dbo].[OperacaoCotistaAux]
	(
		[IdOperacaoCotistaAux] [int] NOT NULL IDENTITY PRIMARY KEY,
		[IdOperacao] [int] NOT NULL,
		[IdCotista] [int] NOT NULL,
		[IdCarteira] [int] NOT NULL,
		[DataOperacao] [datetime] NOT NULL,
		[DataConversao] [datetime] NOT NULL,
		[DataLiquidacao] [datetime] NOT NULL,
		[DataAgendamento] [datetime] NOT NULL,
		[TipoOperacao] [tinyint] NOT NULL,
		[TipoResgate] [tinyint] NULL,
		[IdPosicaoResgatada] [int] NULL,
		[IdFormaLiquidacao] [tinyint] NOT NULL,
		[Quantidade] [decimal](28, 12) NOT NULL,
		[CotaOperacao] [decimal](28, 12) NOT NULL,
		[ValorBruto] [decimal](16, 2) NOT NULL,
		[ValorLiquido] [decimal](16, 2) NOT NULL,
		[ValorIR] [decimal](16, 2) NOT NULL,
		[ValorIOF] [decimal](16, 2) NOT NULL,
		[ValorCPMF] [decimal](16, 2) NOT NULL,
		[ValorPerformance] [decimal](16, 2) NOT NULL,
		[PrejuizoUsado] [decimal](16, 2) NOT NULL,
		[RendimentoResgate] [decimal](16, 2) NOT NULL,
		[VariacaoResgate] [decimal](16, 2) NOT NULL,
		[Observacao] [varchar](400) NOT NULL,
		[DadosBancarios] [varchar](500) NULL,
		[Fonte] [tinyint] NOT NULL,
		[IdConta] [int] NULL,
		[CotaInformada] [decimal](28, 12) NULL,
		[IdAgenda] [int] NULL,
		[IdOperacaoResgatada] [int] NULL,
		[IdOperacaoAuxiliar] [int] NULL
	)
END

IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'DetalheResgateCotistaAux') = 0
Begin
	CREATE TABLE [dbo].[DetalheResgateCotistaAux]
	(
		[IdOperacao] [int] NOT NULL,
		[IdPosicaoResgatada] [int] NOT NULL,
		[IdCotista] [int] NOT NULL,
		[IdCarteira] [int] NOT NULL,
		[Quantidade] [decimal](28, 12) NOT NULL,
		[ValorBruto] [decimal](16, 2) NOT NULL,
		[ValorLiquido] [decimal](16, 2) NOT NULL,
		[ValorCPMF] [decimal](16, 2) NOT NULL,
		[ValorPerformance] [decimal](16, 2) NOT NULL,
		[PrejuizoUsado] [decimal](16, 2) NOT NULL,
		[RendimentoResgate] [decimal](16, 2) NOT NULL,
		[VariacaoResgate] [decimal](16, 2) NOT NULL,
		[ValorIR] [decimal](16, 2) NOT NULL,
		[ValorIOF] [decimal](16, 2) NOT NULL,
		[DataOperacao] [datetime] NOT NULL,
		[TipoOperacao] [tinyint] NOT NULL,
	 CONSTRAINT [PK_DetalheResgateCotistaAux] PRIMARY KEY CLUSTERED 
	(
		[IdOperacao] ASC,
		[IdPosicaoResgatada] ASC
	)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
	) ON [PRIMARY]
END

if not exists(select 1 from syscolumns where id = object_id('OperacaoCotista') and name = 'RegistroEditado')
BEGIN
	ALTER TABLE dbo.OperacaoCotista ADD RegistroEditado char(1) NOT NULL DEFAULT 'N'
END	

if not exists(select 1 from syscolumns where id = object_id('OperacaoCotista') and name = 'ValoresColados')
BEGIN
	ALTER TABLE dbo.OperacaoCotista ADD ValoresColados char(1) NOT NULL DEFAULT 'N'
END	

if not exists(select 1 from syscolumns where id = object_id('OperacaoCotistaAux') and name = 'RegistroEditado')
BEGIN
	ALTER TABLE dbo.OperacaoCotistaAux ADD RegistroEditado char(1) NOT NULL DEFAULT 'N'
END	

if not exists(select 1 from syscolumns where id = object_id('OperacaoCotistaAux') and name = 'ValoresColados')
BEGIN
	ALTER TABLE dbo.OperacaoCotistaAux ADD ValoresColados char(1) NOT NULL DEFAULT 'N'
END	

IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'ColagemResgate') = 0
Begin
	CREATE TABLE [dbo].[ColagemResgate]
	(
		IdColagemResgate [int] PRIMARY KEY NOT NULL IDENTITY,
		TipoRegistro char(1) NOT NULL ,
		CodigoExterno char(15) NOT NULL ,
		DataReferencia [datetime] NOT NULL,
		DataOperacao [datetime] NOT NULL,
		IdCliente  [int] NOT NULL,
		IdCarteira  [int] NOT NULL,
		DataConversao [datetime] NULL,
		Quantidade [decimal](28, 12) NOT NULL,
		CotaOperacao [decimal](28, 12) NOT NULL,
		ValorBruto [decimal](28, 12) NOT NULL,
		ValorLiquido [decimal](28, 12) NOT NULL,
		ValorCPMF [decimal](28, 12) NOT NULL,
		ValorPerformance [decimal](28, 12) NOT NULL,
		PrejuizoUsado [decimal](28, 12) NOT NULL,
		RendimentoResgate [decimal](28, 12) NOT NULL,
		VariacaoResgate [decimal](28, 12) NOT NULL,
		ValorIR [decimal](28, 12) NOT NULL,
		ValorIOF [decimal](28, 12) NOT NULL
	)	
END	

IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'ColagemResgateDetalhe') = 0
Begin
	CREATE TABLE [dbo].[ColagemResgateDetalhe]
	(
		IdColagemResgateDetalhe  [int] PRIMARY KEY NOT NULL IDENTITY,
		CodigoExterno char(15) NOT NULL ,
		DataReferencia [datetime] NOT NULL,
		DataOperacao [datetime] NOT NULL,
		IdCliente  [int] NOT NULL,
		IdCarteira  [int] NOT NULL,
		DataConversao [datetime] NULL,
		Quantidade [decimal](28, 12) NOT NULL,
		CotaOperacao [decimal](28, 12) NOT NULL,
		ValorBruto [decimal](28, 12) NOT NULL,
		ValorLiquido [decimal](28, 12) NOT NULL,
		ValorCPMF [decimal](28, 12) NOT NULL,
		ValorPerformance [decimal](28, 12) NOT NULL,
		PrejuizoUsado [decimal](28, 12) NOT NULL,
		RendimentoResgate [decimal](28, 12) NOT NULL,
		VariacaoResgate [decimal](28, 12) NOT NULL,
		ValorIR [decimal](28, 12) NOT NULL,
		ValorIOF [decimal](28, 12) NOT NULL,
	)	
END	

IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'ColagemResgateDetalhePosicao') = 0
Begin
	CREATE TABLE [dbo].[ColagemResgateDetalhePosicao]
	(
		IdColagemResgateDetalhePosicao [int] PRIMARY KEY NOT NULL IDENTITY,
		IdColagemResgateDetalhe [int] NOT NULL,
		DataReferencia [datetime] NOT NULL,
		QuantidadeInicial [decimal](28, 12) NOT NULL,
		QuantidadeRestante [decimal](28, 12) NOT NULL
	)	
	alter table ColagemResgateDetalhePosicao add constraint ColagemResgates_Det_Pos_FK FOREIGN KEY ( IdColagemResgateDetalhe ) references ColagemResgateDetalhe(IdColagemResgateDetalhe)
END	

IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'ColagemResgateMapaCotista') = 0
Begin
	CREATE TABLE [dbo].[ColagemResgateMapaCotista]
	(
		IdColagemResgateMapaCotista  [int] PRIMARY KEY NOT NULL IDENTITY,
		DataReferencia [datetime] NOT NULL,
		IdCliente  [int] NOT NULL,
		IdCarteira  [int] NOT NULL,
		DataConversao [datetime] NULL,
		DetalheImportado char(1) NOT NULL DEFAULT 'N',
		IdOperacaoResgateVinculada  [int] NOT NULL,	
		Participacao [decimal](28, 12) NOT NULL
	)		
END	

IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'ColagemOperResgPosCotista') = 0
Begin
	CREATE TABLE [dbo].[ColagemOperResgPosCotista]
	(
		IdColagemOperResgPosCotista  [int] PRIMARY KEY NOT NULL IDENTITY,
		IdCarteira  [int] NOT NULL,
		IdCliente  [int] NOT NULL,
		DataReferencia [datetime] NOT NULL,
		IdColagemResgateDetalhe  [int] NOT NULL,
		IdOperacaoResgateVinculada  [int] NOT NULL,
		IdPosicaoVinculada  [int] NOT NULL,
		QuantidadeVinculada [decimal](28, 12) NOT NULL
	)		
END	

IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'DetalheResgateFundoAux') = 0
Begin
	CREATE TABLE [dbo].[DetalheResgateFundoAux]
	(
		[IdOperacao] [int] NOT NULL,
		[IdPosicaoResgatada] [int] NOT NULL,
		[IdCliente] [int] NOT NULL,
		[IdCarteira] [int] NOT NULL,
		[Quantidade] [decimal](28, 12) NOT NULL,
		[ValorBruto] [decimal](16, 2) NOT NULL,
		[ValorLiquido] [decimal](16, 2) NOT NULL,
		[ValorCPMF] [decimal](16, 2) NOT NULL,
		[ValorPerformance] [decimal](16, 2) NOT NULL,
		[PrejuizoUsado] [decimal](16, 2) NOT NULL,
		[RendimentoResgate] [decimal](16, 2) NOT NULL,
		[VariacaoResgate] [decimal](16, 2) NOT NULL,
		[ValorIR] [decimal](16, 2) NOT NULL,
		[ValorIOF] [decimal](16, 2) NOT NULL,		
		[DataOperacao] [datetime] NOT NULL,
		[TipoOperacao] [tinyint] NOT NULL,
	 CONSTRAINT [PK_DetalheResgateFundoAux] PRIMARY KEY CLUSTERED 
	(
		[IdOperacao] ASC,
		[IdPosicaoResgatada] ASC
	)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
	) ON [PRIMARY]
END


IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'OperacaoFundoAux') = 0
Begin
	CREATE TABLE [dbo].[OperacaoFundoAux]
	(
		[IdOperacaoFundoAux] [int] NOT NULL IDENTITY PRIMARY KEY,
		[IdOperacao] [int] NOT NULL,
		[IdCliente] [int] NOT NULL,
		[IdCarteira] [int] NOT NULL,
		[DataOperacao] [datetime] NOT NULL,
		[DataConversao] [datetime] NOT NULL,
		[DataLiquidacao] [datetime] NOT NULL,
		[DataAgendamento] [datetime] NOT NULL,
		[TipoOperacao] [tinyint] NOT NULL,
		[TipoResgate] [tinyint] NULL,
		[IdPosicaoResgatada] [int] NULL,
		[IdFormaLiquidacao] [tinyint] NOT NULL,
		[Quantidade] [decimal](28, 12) NOT NULL,
		[CotaOperacao] [decimal](28, 12) NOT NULL,
		[ValorBruto] [decimal](16, 2) NOT NULL,
		[ValorLiquido] [decimal](16, 2) NOT NULL,
		[ValorIR] [decimal](16, 2) NOT NULL,
		[ValorIOF] [decimal](16, 2) NOT NULL,
		[ValorCPMF] [decimal](16, 2) NOT NULL,
		[ValorPerformance] [decimal](16, 2) NOT NULL,
		[PrejuizoUsado] [decimal](16, 2) NOT NULL,
		[RendimentoResgate] [decimal](16, 2) NOT NULL,
		[VariacaoResgate] [decimal](16, 2) NOT NULL,
		[IdConta] [int] NULL,
		[Observacao] [varchar](400) NOT NULL,
		[Fonte] [tinyint] NOT NULL,
		[IdAgenda] [int] NULL,
		[CotaInformada] [decimal](28, 12) NULL,
		[IdOperacaoResgatada] [int] NULL
	)
END

if not exists(select 1 from syscolumns where id = object_id('OperacaoFundo') and name = 'RegistroEditado')
BEGIN
	ALTER TABLE dbo.OperacaoFundo ADD RegistroEditado char(1) NOT NULL DEFAULT 'N'
END	

if not exists(select 1 from syscolumns where id = object_id('OperacaoFundo') and name = 'ValoresColados')
BEGIN
	ALTER TABLE dbo.OperacaoFundo ADD ValoresColados char(1) NOT NULL DEFAULT 'N' 
END	

if not exists(select 1 from syscolumns where id = object_id('OperacaoFundoAux') and name = 'RegistroEditado')
BEGIN
	ALTER TABLE dbo.OperacaoFundoAux ADD RegistroEditado char(1) NOT NULL DEFAULT 'N'
END	

if not exists(select 1 from syscolumns where id = object_id('OperacaoFundoAux') and name = 'ValoresColados')
BEGIN
	ALTER TABLE dbo.OperacaoFundoAux ADD ValoresColados char(1) NOT NULL DEFAULT 'N' 
END	

IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'ColagemResgateMapaFundo') = 0
Begin
	CREATE TABLE [dbo].[ColagemResgateMapaFundo]
	(
		IdColagemResgateMapaFundo  [int] PRIMARY KEY NOT NULL IDENTITY,
		DataReferencia [datetime] NOT NULL,
		IdCliente  [int] NOT NULL,
		IdCarteira  [int] NOT NULL,
		DataConversao [datetime] NULL,
		DetalheImportado char(1) NOT NULL DEFAULT 'N',
		IdOperacaoResgateVinculada  [int] NOT NULL,	
		Participacao [decimal](28, 12) NOT NULL
	)		
END	

IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'ColagemOperResgPosFundo') = 0
Begin
	CREATE TABLE [dbo].[ColagemOperResgPosFundo]
	(
		IdColagemOperResgPosFundo  [int] PRIMARY KEY NOT NULL IDENTITY,
		IdCarteira  [int] NOT NULL,
		IdCliente  [int] NOT NULL,
		DataReferencia [datetime] NOT NULL,
		IdColagemResgateDetalhe  [int] NOT NULL,
		IdOperacaoResgateVinculada  [int] NOT NULL,
		IdPosicaoVinculada  [int] NOT NULL,
		QuantidadeVinculada [decimal](28, 12) NOT NULL
	)		
END	


IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'ColagemComeCotas') = 0
Begin
	CREATE TABLE [dbo].[ColagemComeCotas]
	(
		IdColagemComeCotas [int] PRIMARY KEY NOT NULL IDENTITY,
		TipoRegistro char(1) NOT NULL ,
		CodigoExterno char(15) NOT NULL ,
		DataReferencia [datetime] NOT NULL,
		DataOperacao [datetime] NOT NULL,
		IdCliente  [int] NOT NULL,
		IdCarteira  [int] NOT NULL,
		DataConversao [datetime] NULL,
		Quantidade [decimal](28, 12) NOT NULL,
		CotaOperacao [decimal](28, 12) NOT NULL,
		ValorBruto [decimal](28, 12) NOT NULL,
		ValorLiquido [decimal](28, 12) NOT NULL,
		ValorCPMF [decimal](28, 12) NOT NULL,
		ValorPerformance [decimal](28, 12) NOT NULL,
		PrejuizoUsado [decimal](28, 12) NOT NULL,
		RendimentoComeCotas [decimal](28, 12) NOT NULL,
		VariacaoComeCotas [decimal](28, 12) NOT NULL,
		ValorIR [decimal](28, 12) NOT NULL,
		ValorIOF [decimal](28, 12) NOT NULL
	)	
END	

IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'ColagemComeCotasDetalhe') = 0
Begin
	CREATE TABLE [dbo].[ColagemComeCotasDetalhe]
	(
		IdColagemComeCotasDetalhe  [int] PRIMARY KEY NOT NULL IDENTITY,
		CodigoExterno char(15) NOT NULL ,
		DataReferencia [datetime] NOT NULL,
		DataOperacao [datetime] NOT NULL,
		IdCliente  [int] NOT NULL,
		IdCarteira  [int] NOT NULL,
		DataConversao [datetime] NULL,
		Quantidade [decimal](28, 12) NOT NULL,
		CotaOperacao [decimal](28, 12) NOT NULL,
		ValorBruto [decimal](28, 12) NOT NULL,
		ValorLiquido [decimal](28, 12) NOT NULL,
		ValorCPMF [decimal](28, 12) NOT NULL,
		ValorPerformance [decimal](28, 12) NOT NULL,
		PrejuizoUsado [decimal](28, 12) NOT NULL,
		RendimentoComeCotas [decimal](28, 12) NOT NULL,
		VariacaoComeCotas [decimal](28, 12) NOT NULL,
		ValorIR [decimal](28, 12) NOT NULL,
		ValorIOF [decimal](28, 12) NOT NULL,
	)	
END	

IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'ColagemComeCotasDetalhePosicao') = 0
Begin
	CREATE TABLE [dbo].[ColagemComeCotasDetalhePosicao]
	(
		IdColagemComeCotasDetalhePosicao [int] PRIMARY KEY NOT NULL IDENTITY,
		IdColagemComeCotasDetalhe [int] NOT NULL,
		DataReferencia [datetime] NOT NULL,
		QuantidadeInicial [decimal](28, 12) NOT NULL,
		QuantidadeRestante [decimal](28, 12) NOT NULL
	)	
	alter table ColagemComeCotasDetalhePosicao add constraint ColagemComeCotass_Det_Pos_FK FOREIGN KEY ( IdColagemComeCotasDetalhe ) references ColagemComeCotasDetalhe(IdColagemComeCotasDetalhe)
END	

IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'ColagemComeCotasMapaFundo') = 0
Begin
	CREATE TABLE [dbo].[ColagemComeCotasMapaFundo]
	(
		IdColagemComeCotasMapaFundo  [int] PRIMARY KEY NOT NULL IDENTITY,
		DataReferencia [datetime] NOT NULL,
		IdCliente  [int] NOT NULL,
		IdCarteira  [int] NOT NULL,
		DataConversao [datetime] NULL,
		DetalheImportado char(1) NOT NULL DEFAULT 'N',
		IdOperacaoComeCotasVinculada  [int] NOT NULL,	
		Participacao [decimal](28, 12) NOT NULL
	)		
END	

IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'ColagemComeCotasPosFundo') = 0
Begin
	CREATE TABLE [dbo].[ColagemComeCotasPosFundo]
	(
		IdColagemComeCotasPosFundo  [int] PRIMARY KEY NOT NULL IDENTITY,
		IdCarteira  				[int] NOT NULL,
		IdCliente  					[int] NOT NULL,
		DataConversao 				[datetime] NULL,
		DataReferencia 				[datetime] NOT NULL,
		IdPosicaoVinculada  		[int] NOT NULL,
		PrejuizoUsado 				[decimal](28, 12) NOT NULL,
		RendimentoComeCotas 		[decimal](28, 12) NOT NULL,
		ValorBruto 					[decimal](28, 12) NOT NULL,
		ValorCPMF 					[decimal](28, 12) NOT NULL,
		ValorIOF 					[decimal](28, 12) NOT NULL,
		ValorIR 					[decimal](28, 12) NOT NULL,
		ValorLiquido 				[decimal](28, 12) NOT NULL,
		ValorPerformance 			[decimal](28, 12) NOT NULL,
		VariacaoComeCotas 			[decimal](28, 12) NOT NULL,
		Quantidade					[decimal](28, 12) NOT NULL,
		Participacao				[decimal](28, 12) NOT NULL,
		DetalheImportado 			[char](1) NOT NULL DEFAULT 'N'
	)		
END	

IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'PosicaoFundoAux') = 0
Begin
	CREATE TABLE [dbo].[PosicaoFundoAux]
	(
		[IdPosicaoFundoAux] [int] NOT NULL IDENTITY PRIMARY KEY,
		[DataReferencia] [datetime] NOT NULL,
		[IdPosicao] [int] NOT NULL,
		[IdOperacao] [int] NULL,
		[IdCliente] [int] NOT NULL,
		[IdCarteira] [int] NOT NULL,
		[ValorAplicacao] [decimal](16, 2) NOT NULL,
		[DataAplicacao] [datetime] NOT NULL,
		[DataConversao] [datetime] NOT NULL,
		[CotaAplicacao] [decimal](28, 12) NOT NULL,
		[CotaDia] [decimal](28, 12) NOT NULL,
		[ValorBruto] [decimal](16, 2) NOT NULL,
		[ValorLiquido] [decimal](16, 2) NOT NULL,
		[QuantidadeInicial] [decimal](28, 12) NOT NULL,
		[Quantidade] [decimal](28, 12) NOT NULL,
		[QuantidadeBloqueada] [decimal](28, 12) NULL,
		[DataUltimaCobrancaIR] [datetime] NOT NULL,
		[ValorIR] [decimal](16, 2) NOT NULL,
		[ValorIOF] [decimal](16, 2) NOT NULL,
		[ValorPerformance] [decimal](16, 2) NOT NULL,
		[ValorIOFVirtual] [decimal](16, 2) NOT NULL,
		[QuantidadeAntesCortes] [decimal](28, 12) NOT NULL,
		[ValorRendimento] [decimal](16, 2) NOT NULL,
		[DataUltimoCortePfee] [datetime] NULL,
		[PosicaoIncorporada] [char](1) NOT NULL,
	)
END	

IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'ColagemComeCotasPosCotista') = 0
Begin
	CREATE TABLE [dbo].[ColagemComeCotasPosCotista]
	(
		IdColagemComeCotasPosCotista  [int] PRIMARY KEY NOT NULL IDENTITY,
		IdCarteira  				[int] NOT NULL,
		IdCliente  					[int] NOT NULL,
		DataConversao 				[datetime] NULL,
		DataReferencia 				[datetime] NOT NULL,
		IdPosicaoVinculada  		[int] NOT NULL,
		PrejuizoUsado 				[decimal](28, 12) NOT NULL,
		RendimentoComeCotas 		[decimal](28, 12) NOT NULL,
		ValorBruto 					[decimal](28, 12) NOT NULL,
		ValorCPMF 					[decimal](28, 12) NOT NULL,
		ValorIOF 					[decimal](28, 12) NOT NULL,
		ValorIR 					[decimal](28, 12) NOT NULL,
		ValorLiquido 				[decimal](28, 12) NOT NULL,
		ValorPerformance 			[decimal](28, 12) NOT NULL,
		VariacaoComeCotas 			[decimal](28, 12) NOT NULL,
		Quantidade					[decimal](28, 12) NOT NULL,
		Participacao				[decimal](28, 12) NOT NULL,
		DetalheImportado 			[char](1) NOT NULL DEFAULT 'N'
	)		
END	
 
IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'PosicaoCotistaAux') = 0
Begin
	CREATE TABLE [dbo].[PosicaoCotistaAux]
	(
		[IdPosicaoCotistaAux] [int] NOT NULL IDENTITY PRIMARY KEY,
		[DataReferencia] [datetime] NOT NULL,
		[IdPosicao] [int] NOT NULL,
		[IdOperacao] [int] NULL,
		[IdCotista] [int] NOT NULL,
		[IdCarteira] [int] NOT NULL,
		[ValorAplicacao] [decimal](16, 2) NOT NULL,
		[DataAplicacao] [datetime] NOT NULL,
		[DataConversao] [datetime] NOT NULL,
		[CotaAplicacao] [decimal](28, 12) NOT NULL,
		[CotaDia] [decimal](28, 12) NOT NULL,
		[ValorBruto] [decimal](16, 2) NOT NULL,
		[ValorLiquido] [decimal](16, 2) NOT NULL,
		[QuantidadeInicial] [decimal](28, 12) NOT NULL,
		[Quantidade] [decimal](28, 12) NOT NULL,
		[QuantidadeBloqueada] [decimal](28, 12) NOT NULL,
		[DataUltimaCobrancaIR] [datetime] NOT NULL,
		[ValorIR] [decimal](16, 2) NOT NULL,
		[ValorIOF] [decimal](16, 2) NOT NULL,
		[ValorPerformance] [decimal](16, 2) NOT NULL,
		[ValorIOFVirtual] [decimal](16, 2) NOT NULL,
		[QuantidadeAntesCortes] [decimal](28, 12) NOT NULL,
		[ValorRendimento] [decimal](16, 2) NOT NULL,
		[DataUltimoCortePfee] [datetime] NULL,
		[PosicaoIncorporada] [char](1) NOT NULL,
	 )
 END	
 
if exists(select 1 from syscolumns where id = object_id('ColagemResgateDetalhe') and name = 'CodigoExterno')
BEGIN
	ALTER TABLE [dbo].[ColagemResgateDetalhe] ALTER COLUMN CodigoExterno varchar(30) NOT NULL 
END		

if exists(select 1 from syscolumns where id = object_id('ColagemComeCotas') and name = 'CodigoExterno')
BEGIN	
	ALTER TABLE [dbo].[ColagemComeCotas] ALTER COLUMN CodigoExterno varchar(30) NOT NULL 
END	
 
if exists(select 1 from syscolumns where id = object_id('ColagemComeCotasDetalhe') and name = 'CodigoExterno')
BEGIN	
	ALTER TABLE [dbo].[ColagemComeCotasDetalhe] ALTER COLUMN CodigoExterno varchar(30) NOT NULL 
END		

if exists(select 1 from syscolumns where id = object_id('ColagemResgate') and name = 'CodigoExterno')
BEGIN	
	ALTER TABLE [dbo].[ColagemResgate] ALTER COLUMN CodigoExterno varchar(30) NOT NULL
END		

if not exists(select 1 from syscolumns where id = object_id('ColagemResgate') and name = 'Processado')
BEGIN	
	ALTER TABLE [dbo].[ColagemResgate] ADD Processado char(1) NOT NULL DEFAULT 'N';
END		

if not exists(select 1 from syscolumns where id = object_id('ColagemComeCotas') and name = 'Processado')
BEGIN	
	ALTER TABLE [dbo].[ColagemComeCotas] ADD Processado char(1) NOT NULL DEFAULT 'N';
END		

IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'Clearing') = 0
Begin
	CREATE TABLE [dbo].Clearing
	(
		[IdClearing]				int NOT NULL IDENTITY PRIMARY KEY,
		[Codigo]					VARCHAR(30) NOT NULL, 
		[Descricao]					VARCHAR(100) NOT NULL, 
		[CNPJ]						VARCHAR(30) NOT NULL, 
		[IdFormaLiquidacao]			tinyint NOT NULL,
		[IdLocalFeriado]			smallint NOT NULL,
		[CodigoCETIP]				VARCHAR(30) NULL,
		[CodigoSELIC]				VARCHAR(30) NULL		
	 )
	 alter table Clearing add constraint Clearing_FormaLiq_FK FOREIGN KEY ( IdFormaLiquidacao ) references FormaLiquidacao(IdFormaLiquidacao)
	 alter table Clearing add constraint Clearing_LocalFer_FK FOREIGN KEY ( IdLocalFeriado ) references LocalFeriado(IdLocal)
	 INSERT INTO Clearing (Codigo, Descricao, CNPJ, IdFormaLiquidacao, IdLocalFeriado, CodigoCETIP, CodigoSELIC) VALUES('SELIC', 'SELIC', '00038166000105', 2, 1, null, null)
	 INSERT INTO Clearing (Codigo, Descricao, CNPJ, IdFormaLiquidacao, IdLocalFeriado, CodigoCETIP, CodigoSELIC) VALUES('CETIP', 'CETIP', '09358105000191', 2, 1, null, null)
	 INSERT INTO Clearing (Codigo, Descricao, CNPJ, IdFormaLiquidacao, IdLocalFeriado, CodigoCETIP, CodigoSELIC) VALUES('CBLC', 'CBLC', '60777661000150', 2, 1, null, null)
	 INSERT INTO Clearing (Codigo, Descricao, CNPJ, IdFormaLiquidacao, IdLocalFeriado, CodigoCETIP, CodigoSELIC) VALUES('BOVESPA', 'BOVESPA', '09346601000125', 2, 1, null, null)
 END	
  			
 IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'LocalNegociacao') = 0
Begin
	CREATE TABLE [dbo].LocalNegociacao
	(
		[IdLocalNegociacao]				int NOT NULL IDENTITY PRIMARY KEY,
		[Codigo]						VARCHAR(30) NOT NULL, 
		[Descricao]						VARCHAR(100) NOT NULL, 
		[Pais]							VARCHAR(30) NOT NULL, 
		[IdMoeda]						smallint NOT NULL,
		[IdLocalFeriado]				smallint NOT NULL,
		[ParaisoFiscal] 				[char](1) NOT NULL DEFAULT 'N'				
	 )	 
	 alter table LocalNegociacao add constraint LocalNegociacao_Moeda_FK FOREIGN KEY ( IdMoeda ) references Moeda(IdMoeda)
	 alter table LocalNegociacao add constraint LocalNegociacao_LocalFer_FK FOREIGN KEY ( IdLocalFeriado ) references LocalFeriado(IdLocal)
	 INSERT INTO LocalNegociacao VALUES ('Brasil', 'Brasil', 'Brasil', 1, 1, 'N');
	 INSERT INTO LocalNegociacao VALUES ('SELIC', 'SELIC', 'Brasil', 1, 1, 'N');
	 INSERT INTO LocalNegociacao VALUES ('CETIP', 'CETIP', 'Brasil', 1, 1, 'N');
	 INSERT INTO LocalNegociacao VALUES ('BMF', 'BMF', 'Brasil', 1, 1, 'N');
	 INSERT INTO LocalNegociacao VALUES ('BOVESPA', 'BOVESPA', 'Brasil', 1, 1, 'N');
 END	

delete from PermissaoMenu where idMEnu = 630;
insert into [dbo].[PermissaoMenu]
SELECT [IdGrupo]
      ,630
      ,[PermissaoLeitura]
      ,[PermissaoAlteracao]
      ,[PermissaoExclusao]
      ,[PermissaoInclusao]
  FROM [dbo].[PermissaoMenu]
  where IdMenu = 22280;
GO

delete from PermissaoMenu where idMEnu = 640;
insert into [dbo].[PermissaoMenu]
SELECT [IdGrupo]
      ,640
      ,[PermissaoLeitura]
      ,[PermissaoAlteracao]
      ,[PermissaoExclusao]
      ,[PermissaoInclusao]
  FROM [dbo].[PermissaoMenu]
  where IdMenu = 22280;
GO

if NOT exists(select 1 from LocalCustodia WHERE idLocalCustodia = 1 and upper(CODIGO) = 'SELIC')
BEGIN
	TRUNCATE TABLE LocalCustodia;
	INSERT INTO LocalCustodia VALUES ('SELIC', 'SELIC', '00038166000105');
	INSERT INTO LocalCustodia VALUES ('CETIP', 'CETIP', '09358105000191');
	INSERT INTO LocalCustodia VALUES ('CBLC', 'CBLC', '60777661000150');
END

if not exists(select 1 from syscolumns where id = object_id('AtivoBolsa') and name = 'IdLocalCustodia')
BEGIN
	ALTER TABLE dbo.AtivoBolsa ADD IdLocalCustodia int NOT NULL DEFAULT '3'	
END	

if not exists(select 1 from syscolumns where id = object_id('AtivoBolsa') and name = 'IdClearing')
BEGIN
	ALTER TABLE dbo.AtivoBolsa ADD IdClearing int NOT NULL DEFAULT('4')
END	

if not exists(select 1 from syscolumns where id = object_id('AtivoBolsa') and name = 'IdLocalNegociacao')
BEGIN
	ALTER TABLE dbo.AtivoBolsa ADD IdLocalNegociacao int NOT NULL DEFAULT('5')
END	

if not exists(select 1 from syscolumns where id = object_id('AtivoBMF') and name = 'IdLocalCustodia')
BEGIN
	ALTER TABLE dbo.AtivoBMF ADD IdLocalCustodia int NOT NULL DEFAULT '3'
END	

if not exists(select 1 from syscolumns where id = object_id('AtivoBMF') and name = 'IdClearing')
BEGIN
	ALTER TABLE dbo.AtivoBMF ADD IdClearing int NOT NULL DEFAULT('4')
END	

if not exists(select 1 from syscolumns where id = object_id('AtivoBMF') and name = 'IdLocalNegociacao')
BEGIN
	ALTER TABLE dbo.AtivoBMF ADD IdLocalNegociacao int NOT NULL DEFAULT('5')
END	

if not exists(select 1 from syscolumns where id = object_id('PapelRendaFixa') and name = 'IdLocalCustodia')
BEGIN
	ALTER TABLE dbo.PapelRendaFixa ADD IdLocalCustodia int NULL;
	EXEC('UPDATE PapelRendaFixa SET IdLocalCustodia = (CASE WHEN TipoPapel <> 1 THEN 2 ELSE 1 END)');
	ALTER TABLE PapelRendaFixa ALTER COLUMN IdLocalCustodia int NOT NULL;
END	

if not exists(select 1 from syscolumns where id = object_id('PapelRendaFixa') and name = 'IdClearing')
BEGIN
	ALTER TABLE dbo.PapelRendaFixa ADD IdClearing int NULL
	EXEC('UPDATE PapelRendaFixa SET IdClearing = (CASE WHEN TipoPapel <> 1 THEN 2 ELSE 1 END)');
	ALTER TABLE PapelRendaFixa ALTER COLUMN IdClearing int NOT NULL;	
END	

if not exists(select 1 from syscolumns where id = object_id('PapelRendaFixa') and name = 'IdLocalNegociacao')
BEGIN
	ALTER TABLE dbo.PapelRendaFixa ADD IdLocalNegociacao int NULL;
	EXEC('UPDATE PapelRendaFixa SET IdLocalNegociacao = (CASE WHEN TipoPapel <> 1 THEN 3 ELSE 2 END)');
	ALTER TABLE PapelRendaFixa ALTER COLUMN IdLocalNegociacao int NOT NULL;
END	

if not exists(select 1 from syscolumns where id = object_id('Cliente') and name = 'IdLocalNegociacao')
BEGIN
	ALTER TABLE dbo.Cliente ADD IdLocalNegociacao int NULL;
	EXEC('UPDATE Cliente SET IdLocalNegociacao = 1 WHERE idTipo = 500');
END	

if not exists(select 1 from syscolumns where id = object_id('OperacaoBolsa') and name = 'IdCorretora')
BEGIN
	ALTER TABLE dbo.OperacaoBolsa ADD IdCorretora int NULL
	EXEC('UPDATE OperacaoBolsa SET IdCorretora = idAgenteCorretora');
	ALTER TABLE OperacaoBolsa ALTER COLUMN IdCorretora int NOT NULL;
END	

if not exists(select 1 from syscolumns where id = object_id('OperacaoBolsa') and name = 'IdLocalCustodia')
BEGIN
	ALTER TABLE dbo.OperacaoBolsa ADD IdLocalCustodia int NULL
	EXEC('UPDATE OperacaoBolsa SET IdLocalCustodia = 3');
	ALTER TABLE OperacaoBolsa ALTER COLUMN IdLocalCustodia int NOT NULL;
END	

if not exists(select 1 from syscolumns where id = object_id('OperacaoBolsa') and name = 'IdLocalNegociacao')
BEGIN
	ALTER TABLE dbo.OperacaoBolsa ADD IdLocalNegociacao int NULL
	EXEC('UPDATE OperacaoBolsa SET IdLocalNegociacao = 5');
	ALTER TABLE OperacaoBolsa ALTER COLUMN IdLocalNegociacao int NOT NULL;
END	

if not exists(select 1 from syscolumns where id = object_id('OperacaoBolsa') and name = 'IdClearing')
BEGIN
	ALTER TABLE dbo.OperacaoBolsa ADD IdClearing int NULL
	EXEC('UPDATE OperacaoBolsa SET IdClearing = 4');
	ALTER TABLE OperacaoBolsa ALTER COLUMN IdClearing int NOT NULL;
END	

if not exists(select 1 from syscolumns where id = object_id('OperacaoBMF') and name = 'IdCorretora')
BEGIN
	ALTER TABLE dbo.OperacaoBMF ADD IdCorretora int NULL
	EXEC('UPDATE OperacaoBMF SET IdCorretora = idAgenteCorretora');
	ALTER TABLE OperacaoBMF ALTER COLUMN IdCorretora int NOT NULL;
END	

if not exists(select 1 from syscolumns where id = object_id('OperacaoBMF') and name = 'IdLocalCustodia')
BEGIN
	ALTER TABLE dbo.OperacaoBMF ADD IdLocalCustodia int NULL
	EXEC('UPDATE OperacaoBMF SET IdLocalCustodia = 3');
	ALTER TABLE OperacaoBMF ALTER COLUMN IdLocalCustodia int NOT NULL;
END	

if not exists(select 1 from syscolumns where id = object_id('OperacaoBMF') and name = 'IdLocalNegociacao')
BEGIN
	ALTER TABLE dbo.OperacaoBMF ADD IdLocalNegociacao int NULL
	EXEC('UPDATE OperacaoBMF SET IdLocalNegociacao = 5');
	ALTER TABLE OperacaoBMF ALTER COLUMN IdLocalNegociacao int NOT NULL;
END	

if not exists(select 1 from syscolumns where id = object_id('OperacaoBMF') and name = 'IdClearing')
BEGIN
	ALTER TABLE dbo.OperacaoBMF ADD IdClearing int NULL
	EXEC('UPDATE OperacaoBMF SET IdClearing = 4');
	ALTER TABLE OperacaoBMF ALTER COLUMN IdClearing int NOT NULL;
END	

if not exists(select 1 from syscolumns where id = object_id('OperacaoFundo') and name = 'IdLocalNegociacao')
BEGIN
	ALTER TABLE dbo.OperacaoFundo ADD IdLocalNegociacao int NOT NULL DEFAULT('1')
END	

if not exists(select 1 from syscolumns where id = object_id('OperacaoCotista') and name = 'IdLocalNegociacao')
BEGIN
	ALTER TABLE dbo.OperacaoCotista ADD IdLocalNegociacao int NOT NULL DEFAULT('1')
END	

if not exists(select 1 from syscolumns where id = object_id('OperacaoFundoAux') and name = 'IdLocalNegociacao')
BEGIN
	ALTER TABLE dbo.OperacaoFundoAux ADD IdLocalNegociacao int NOT NULL DEFAULT('1')
END	

if not exists(select 1 from syscolumns where id = object_id('OperacaoCotistaAux') and name = 'IdLocalNegociacao')
BEGIN
	ALTER TABLE dbo.OperacaoCotistaAux ADD IdLocalNegociacao int NOT NULL DEFAULT('1')
END	

if exists(select 1 from OperacaoRendaFixa where IdLiquidacao = 0)
BEGIN
	EXEC('UPDATE op SET op.IdLiquidacao = (CASE WHEN pap.TipoPapel <> 1 THEN 2 ELSE 1 END)
	FROM OperacaoRendaFixa op
	INNER JOIN TituloRendaFixa tit ON op.IdTitulo = tit.IdTitulo
	INNER JOIN PapelRendafixa pap ON pap.idPapel = tit.idPapel')
END

if exists(select 1 from OperacaoRendaFixa where IdCustodia = 0)
BEGIN
	EXEC('UPDATE op SET op.IdCustodia = (CASE WHEN pap.TipoPapel <> 1 THEN 2 ELSE 1 END)
	FROM OperacaoRendaFixa op
	INNER JOIN TituloRendaFixa tit ON op.IdTitulo = tit.IdTitulo
	INNER JOIN PapelRendafixa pap ON pap.idPapel = tit.idPapel')
END

if not exists(select 1 from syscolumns where id = object_id('OperacaoRendaFixa') and name = 'IdLocalNegociacao')
BEGIN
	ALTER TABLE dbo.OperacaoRendaFixa ADD IdLocalNegociacao int NULL
	EXEC('UPDATE op SET op.IdLocalNegociacao = (CASE WHEN pap.TipoPapel <> 1 THEN 3 ELSE 2 END)
	FROM OperacaoRendaFixa op
	INNER JOIN TituloRendaFixa tit ON op.IdTitulo = tit.IdTitulo
	INNER JOIN PapelRendafixa pap ON pap.idPapel = tit.idPapel');
	ALTER TABLE OperacaoRendaFixa ALTER COLUMN IdLocalNegociacao int NOT NULL;
END	

if not exists(select 1 from syscolumns where id = object_id('OperacaoRendaFixa') and name = 'IdCorretora')
BEGIN
	ALTER TABLE dbo.OperacaoRendaFixa ADD IdCorretora int NULL
	EXEC('UPDATE OperacaoRendaFixa SET IdCorretora = IdAgenteCorretora');
END	

if not exists(select 1 from syscolumns where id = object_id('OrdemBolsa') and name = 'IdCorretora')
BEGIN
	ALTER TABLE dbo.OrdemBolsa ADD IdCorretora int NULL
	EXEC('UPDATE OrdemBolsa SET IdCorretora = idAgenteCorretora');
	ALTER TABLE OrdemBolsa ALTER COLUMN IdCorretora int NOT NULL;
END	

if not exists(select 1 from syscolumns where id = object_id('OrdemBolsa') and name = 'IdLocalCustodia')
BEGIN
	ALTER TABLE dbo.OrdemBolsa ADD IdLocalCustodia int NULL
	EXEC('UPDATE OrdemBolsa SET IdLocalCustodia = 3');
	ALTER TABLE OrdemBolsa ALTER COLUMN IdLocalCustodia int NOT NULL;
END	

if not exists(select 1 from syscolumns where id = object_id('OrdemBolsa') and name = 'IdLocalNegociacao')
BEGIN
	ALTER TABLE dbo.OrdemBolsa ADD IdLocalNegociacao int NULL
	EXEC('UPDATE OrdemBolsa SET IdLocalNegociacao = 5');
	ALTER TABLE OrdemBolsa ALTER COLUMN IdLocalNegociacao int NOT NULL;
END	

if not exists(select 1 from syscolumns where id = object_id('OrdemBolsa') and name = 'IdClearing')
BEGIN
	ALTER TABLE dbo.OrdemBolsa ADD IdClearing int NULL
	EXEC('UPDATE OrdemBolsa SET IdClearing = 4');
	ALTER TABLE OrdemBolsa ALTER COLUMN IdClearing int NOT NULL;
END	

if not exists(select 1 from syscolumns where id = object_id('OrdemBMF') and name = 'IdCorretora')
BEGIN
	ALTER TABLE dbo.OrdemBMF ADD IdCorretora int NULL
	EXEC('UPDATE OrdemBMF SET IdCorretora = idAgenteCorretora');
	ALTER TABLE OrdemBMF ALTER COLUMN IdCorretora int NOT NULL;
END	

if not exists(select 1 from syscolumns where id = object_id('OrdemBMF') and name = 'IdLocalCustodia')
BEGIN
	ALTER TABLE dbo.OrdemBMF ADD IdLocalCustodia int NULL
	EXEC('UPDATE OrdemBMF SET IdLocalCustodia = 3');
	ALTER TABLE OrdemBMF ALTER COLUMN IdLocalCustodia int NOT NULL;
END	

if not exists(select 1 from syscolumns where id = object_id('OrdemBMF') and name = 'IdLocalNegociacao')
BEGIN
	ALTER TABLE dbo.OrdemBMF ADD IdLocalNegociacao int NULL
	EXEC('UPDATE OrdemBMF SET IdLocalNegociacao = 5');
	ALTER TABLE OrdemBMF ALTER COLUMN IdLocalNegociacao int NOT NULL;
END	

if not exists(select 1 from syscolumns where id = object_id('OrdemBMF') and name = 'IdClearing')
BEGIN
	ALTER TABLE dbo.OrdemBMF ADD IdClearing int NULL
	EXEC('UPDATE OrdemBMF SET IdClearing = 4');
	ALTER TABLE OrdemBMF ALTER COLUMN IdClearing int NOT NULL;
END	

IF EXISTS (select 1 from sysconstraints where object_name(id) = 'cliente' and object_name(constid) = 'Pessoa_Officer_FK1')
BEGIN
	alter table cliente drop constraint Pessoa_Officer_FK1;
	alter table cliente add constraint Cliente_Pessoa_FK1 FOREIGN KEY ( IdPessoa ) references Pessoa(IdPessoa)
END	

IF EXISTS (select * from sys.columns where Name = N'IdFormaLiquidacao' and Object_ID = Object_ID(N'OperacaoFundo') and is_nullable = 0)
BEGIN
	ALTER TABLE OperacaoFundo Alter Column IdFormaLiquidacao tinyint NULL;	
END	

IF EXISTS (select * from sys.columns where Name = N'IdFormaLiquidacao' and Object_ID = Object_ID(N'OperacaoCotista') and is_nullable = 0)
BEGIN
	ALTER TABLE OperacaoCotista Alter Column IdFormaLiquidacao tinyint NULL;	
END	

if not EXISTS(select 1 from sysconstraints where object_name(id) = 'PosicaoCotistaHistorico' and object_name(constid) = 'Operacao_PosicaoCotistaHis_FK1')
Begin
	ALTER TABLE PosicaoCotistaHistorico
	ADD CONSTRAINT Operacao_PosicaoCotistaHis_FK1
	FOREIGN KEY (IdOperacao)
	REFERENCES OperacaoCotista(IdOPeracao);
End

if not EXISTS(select 1 from sysconstraints where object_name(id) = 'PosicaoCotistaAbertura' and object_name(constid) = 'Operacao_PosicaoCotistaAbe_FK1')
Begin
	ALTER TABLE PosicaoCotistaAbertura
	ADD CONSTRAINT Operacao_PosicaoCotistaAbe_FK1
	FOREIGN KEY (IdOperacao)
	REFERENCES OperacaoCotista(IdOPeracao);
End

--correção na estrutura da tabela
IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'DetalhePosicaoAfetadaRF') > 0
Begin
	if  exists(select 1 from syscolumns where id = object_id('DetalhePosicaoAfetadaRF') and name = 'puAtualPosicao')
	BEGIN
		drop table DetalhePosicaoAfetadaRF;
	END	
END	

IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'DetalhePosicaoAfetadaRF') = 0
Begin
	CREATE TABLE [dbo].DetalhePosicaoAfetadaRF
	(
		[DataOperacao] [datetime] NOT NULL,
		[IdOperacao] [int] NOT NULL,
		[IdPosicaoAfetada] [int] NOT NULL,		
		[IdCliente] [int] NOT NULL,
		[QtdeMovimentada] [decimal](28, 12) NOT NULL,
		[PuOperacao] [decimal](28, 12) NOT NULL		
	 )
	 alter table DetalhePosicaoAfetadaRF add primary key(DataOperacao, IdOperacao, IdPosicaoAfetada)	
	 alter table DetalhePosicaoAfetadaRF add constraint DetalhePosAfetadaRF_OpRF_FK FOREIGN KEY ( IdOperacao ) references OperacaoRendaFixa(IdOperacao)
	 alter table DetalhePosicaoAfetadaRF add constraint DetalhePosAfetadaRF_PosRF_FK FOREIGN KEY ( IdPosicaoAfetada ) references PosicaoRendaFixa(IdPosicao)
 END	
 
if  exists(select 1 from syscolumns where id = object_id('OperacaoRendaFixa') and name = 'IdCorretora')
BEGIN
	ALTER TABLE dbo.OperacaoRendaFixa drop column IdCorretora;
	
END	

if not exists(select 1 from syscolumns where id = object_id('OperacaoRendaFixa') and name = 'IdAgenteCustodia')
BEGIN
	ALTER TABLE dbo.OperacaoRendaFixa ADD IdAgenteCustodia int NULL;
END
	
if  exists(select 1 from syscolumns where id = object_id('OrdemBolsa') and name = 'IdCorretora')
BEGIN
	ALTER TABLE dbo.OrdemBolsa drop column IdCorretora;
END	

if not exists(select 1 from syscolumns where id = object_id('OrdemBolsa') and name = 'IdAgenteCustodia')
BEGIN
	ALTER TABLE dbo.OrdemBolsa ADD IdAgenteCustodia int NULL;
END

if  exists(select 1 from syscolumns where id = object_id('OrdemBMF') and name = 'IdCorretora')
BEGIN
	ALTER TABLE dbo.OrdemBMF drop column IdCorretora;
	
END	

if not exists(select 1 from syscolumns where id = object_id('OrdemBMF') and name = 'IdAgenteCustodia')
BEGIN
	ALTER TABLE dbo.OrdemBMF ADD IdAgenteCustodia int NULL;
END

if  exists(select 1 from syscolumns where id = object_id('OperacaoBolsa') and name = 'IdCorretora')
BEGIN
	ALTER TABLE dbo.OperacaoBolsa drop column IdCorretora;	
END	

if not exists(select 1 from syscolumns where id = object_id('OperacaoBolsa') and name = 'IdAgenteCustodia')
BEGIN
	ALTER TABLE dbo.OperacaoBolsa ADD IdAgenteCustodia int NULL;
END

if  exists(select 1 from syscolumns where id = object_id('OperacaoBMF') and name = 'IdCorretora')
BEGIN
	ALTER TABLE dbo.OperacaoBMF drop column IdCorretora;
	
END	

if not exists(select 1 from syscolumns where id = object_id('OperacaoBMF') and name = 'IdAgenteCustodia')
BEGIN
	ALTER TABLE dbo.OperacaoBMF ADD IdAgenteCustodia int NULL;
END

--correção na estrutura da tabela
IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'TransferenciaSerie') > 0
Begin
	if not exists(select 1 from syscolumns where id = object_id('TransferenciaSerie') and name = 'IdEventoRollUp')
	BEGIN
		drop table TransferenciaSerie;
	END	
END	

IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'TransferenciaSerie') = 0
Begin
	CREATE TABLE [dbo].TransferenciaSerie
	(
		[IdTransferenciaSerie] [int] PRIMARY KEY NOT NULL IDENTITY , 
		[DataExecucao] [datetime] NOT NULL,
		[DataPosicao] [datetime] NOT NULL,
		[IdCarteira] [int] NOT NULL,
		[IdSerieOrigem] [int] NOT NULL,		
		[IdSerieDestino] [int] NOT NULL,		
		[IdPosicao] [int] NOT NULL,	
		[IdEventoRollUp] [int] NULL	
	 )
	 alter table TransferenciaSerie add constraint TransSerie_SerieDest_FK FOREIGN KEY ( IdSerieOrigem )  references SeriesOffShore(IdSeriesOffShore)
	 alter table TransferenciaSerie add constraint TransSerie_SerieOrig_FK FOREIGN KEY ( IdSerieDestino ) references SeriesOffShore(IdSeriesOffShore)
 END	
 
if not exists(select 1 from syscolumns where id = object_id('PosicaoCotista') and name = 'IdSeriesOffShore')
BEGIN
	ALTER TABLE dbo.PosicaoCotista ADD IdSeriesOffShore int NULL;
END	

if not exists(select 1 from syscolumns where id = object_id('PosicaoCotistaHistorico') and name = 'IdSeriesOffShore')
BEGIN
	ALTER TABLE dbo.PosicaoCotistaHistorico ADD IdSeriesOffShore int NULL;
END	

if not exists(select 1 from syscolumns where id = object_id('PosicaoCotistaAbertura') and name = 'IdSeriesOffShore')
BEGIN
	ALTER TABLE dbo.PosicaoCotistaAbertura ADD IdSeriesOffShore int NULL;
END	

if not exists(select 1 from syscolumns where id = object_id('OperacaoCotista') and name = 'IdSeriesOffShore')
BEGIN
	ALTER TABLE dbo.OperacaoCotista ADD IdSeriesOffShore int NULL;
END	

if not exists(select 1 from syscolumns where id = object_id('OperacaoCotistaAux') and name = 'IdSeriesOffShore')
BEGIN
	ALTER TABLE dbo.OperacaoCotistaAux ADD IdSeriesOffShore int NULL;
END	

if not exists(select 1 from syscolumns where id = object_id('PosicaoCotistaAux') and name = 'IdSeriesOffShore')
BEGIN
	ALTER TABLE dbo.PosicaoCotistaAux ADD IdSeriesOffShore int NULL;
END	

delete from PermissaoMenu where idMEnu = 10270;
insert into [dbo].[PermissaoMenu]
SELECT [IdGrupo]
	  ,10270
	  ,[PermissaoLeitura]
	  ,[PermissaoAlteracao]
	  ,[PermissaoExclusao]
	  ,[PermissaoInclusao]
  FROM [dbo].[PermissaoMenu]
where IdMenu = 22280;

if EXISTS(select 1 from sysconstraints where object_name(id) = 'DetalhePosicaoAfetadaRF' and object_name(constid) = 'DetalhePosAfetadaRF_PosRF_FK')
Begin
	alter table DetalhePosicaoAfetadaRF drop constraint DetalhePosAfetadaRF_PosRF_FK
End

IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'EventoRollUp') = 0
Begin
	CREATE TABLE [dbo].EventoRollUp
	(
		[IdEventoRollUp] [int] PRIMARY KEY NOT NULL IDENTITY , 
		[DataExecucao] [datetime] NOT NULL,
		[DataNav] [datetime] NOT NULL,
		[IdFundoOffShore] [int] NOT NULL,
		[IdSerieOrigem] [int] NOT NULL,		
		[IdSerieDestino] [int] NOT NULL,		
	 )
	 alter table EventoRollUp add constraint EventoRollUp_SerieDest_FK FOREIGN KEY ( IdSerieOrigem )  references SeriesOffShore(IdSeriesOffShore)
	 alter table EventoRollUp add constraint EventoRollUp_SerieOrig_FK FOREIGN KEY ( IdSerieDestino ) references SeriesOffShore(IdSeriesOffShore)
 END	

if not exists(select 1 from syscolumns where id = object_id('OperacaoCotista') and name = 'IdTransferenciaSerie')
BEGIN
	ALTER TABLE dbo.OperacaoCotista ADD IdTransferenciaSerie int NULL;	
END	
	
delete from PermissaoMenu where idMEnu = 7735;
insert into [dbo].[PermissaoMenu]
SELECT [IdGrupo]
	  ,7735
	  ,[PermissaoLeitura]
	  ,[PermissaoAlteracao]
	  ,[PermissaoExclusao]
	  ,[PermissaoInclusao]
  FROM [dbo].[PermissaoMenu]
where IdMenu = 22280;

	
delete from PermissaoMenu where idMEnu = 7730;
insert into [dbo].[PermissaoMenu]
SELECT [IdGrupo]
	  ,7730
	  ,[PermissaoLeitura]
	  ,[PermissaoAlteracao]
	  ,[PermissaoExclusao]
	  ,[PermissaoInclusao]
  FROM [dbo].[PermissaoMenu]
where IdMenu = 22280;

delete from PermissaoMenu where idMEnu = 7740;
insert into [dbo].[PermissaoMenu]
SELECT [IdGrupo]
	  ,7740
	  ,[PermissaoLeitura]
	  ,[PermissaoAlteracao]
	  ,[PermissaoExclusao]
	  ,[PermissaoInclusao]
  FROM [dbo].[PermissaoMenu]
where IdMenu = 22280;

IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'RentabilidadeSerie') = 0
Begin
	CREATE TABLE RentabilidadeSerie 
	( 	
		IdRentabilidadeSerie					INT 				PRIMARY KEY identity NOT NULL, 
		Data	 		 		 				DateTime		    NOT NULL,						
		IdFundo									INT					NOT NULL,
		IdSerie									INT					NOT NULL,
		QuantidadeSerie							Decimal(25,12)		NULL,
		Rentabilidade							Decimal(25,12)		NULL,
		RentabilidadeAcumuluda					Decimal(25,12)		NULL
	) 
END

IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'RentabilidadeFundoOffShore') = 0
Begin
	CREATE TABLE RentabilidadeFundoOffShore 
	( 	
		IdRentabilidadeFundoOffShore			INT 				PRIMARY KEY identity NOT NULL, 
		Data	 		 		 				DateTime		    NOT NULL,						
		IdFundo									INT					NOT NULL,
		Patrimonio								Decimal(25,12)		NULL,
		ValorCota								Decimal(25,12)		NULL,
		PatrimonioAnterior						Decimal(25,12)		NULL,
		ValorCotaAnterior						Decimal(25,12)		NULL,		
		Rentabilidade							Decimal(25,12)		NULL,
		RentabilidadePercentual					Decimal(25,12)		NULL,
		RentabilidadeAcumuluda					Decimal(25,12)		NULL
	) 
END

delete from PermissaoMenu where idMEnu = 10280;
insert into [dbo].[PermissaoMenu]
SELECT [IdGrupo]
	  ,10280
	  ,[PermissaoLeitura]
	  ,[PermissaoAlteracao]
	  ,[PermissaoExclusao]
	  ,[PermissaoInclusao]
  FROM [dbo].[PermissaoMenu]
where IdMenu = 22280;

if not exists(select 1 from syscolumns where id = object_id('SeriesOffShore') and name = 'PrazoRepeticaoCotas')
BEGIN
	ALTER TABLE dbo.SeriesOffShore ADD PrazoRepeticaoCotas int NULL;	
END	

if not exists(select 1 from syscolumns where id = object_id('SeriesOffShore') and name = 'PrazoValidadePrevia')
BEGIN
	ALTER TABLE dbo.SeriesOffShore ADD PrazoValidadePrevia int NULL;	
END	

if not exists(select 1 from syscolumns where id = object_id('SeriesOffShore') and name = 'IdIndicePrevia')
BEGIN
	ALTER TABLE dbo.SeriesOffShore ADD IdIndicePrevia smallint NULL;	
END	

if not exists(select 1 from syscolumns where id = object_id('ClassesOffShore') and name = 'PrazoValidadePrevia')
BEGIN
	ALTER TABLE dbo.ClassesOffShore ADD PrazoValidadePrevia int NULL;	
END	

if not exists(select 1 from syscolumns where id = object_id('ClassesOffShore') and name = 'PrazoRepeticaoCotas')
BEGIN
	ALTER TABLE dbo.ClassesOffShore ADD PrazoRepeticaoCotas int NULL;	
END	

if not exists(select 1 from syscolumns where id = object_id('ClassesOffShore') and name = 'IdIndicePrevia')
BEGIN
	ALTER TABLE dbo.ClassesOffShore ADD IdIndicePrevia smallint NULL;	
END	


if exists(select 1 from syscolumns where id = object_id('OperacaoBolsa') and name = 'IdAgenteCustodia')
BEGIN
	ALTER TABLE OperacaoBolsa DROP COLUMN IdAgenteCustodia;
END

if exists(select 1 from syscolumns where id = object_id('OperacaoBMF') and name = 'IdAgenteCustodia')
BEGIN
	ALTER TABLE OperacaoBMF DROP COLUMN IdAgenteCustodia;
END

if exists(select 1 from syscolumns where id = object_id('OrdemBolsa') and name = 'IdAgenteCustodia')
BEGIN
	ALTER TABLE OrdemBolsa DROP COLUMN IdAgenteCustodia;
END

if exists(select 1 from syscolumns where id = object_id('OrdemBMF') and name = 'IdAgenteCustodia')
BEGIN
	ALTER TABLE OrdemBMF DROP COLUMN IdAgenteCustodia;
END

if exists(select 1 from syscolumns where id = object_id('PerfilMTM') and name = 'DtVigencia')
BEGIN
	exec sp_rename 'PerfilMTM.[DtVigencia]', 'DtReferencia', 'column'
END

