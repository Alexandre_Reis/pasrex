declare @data_versao char(10)
set @data_versao = '2013-03-01'
if exists (select 1 from VersaoSchema where DataVersao = @data_versao) return
insert into VersaoSchema VALUES(@data_versao)

insert into permissaomenu (IdGrupo, IdMenu, PermissaoLeitura, PermissaoAlteracao, PermissaoExclusao, PermissaoInclusao) select idgrupo,	14570, 'N', 'N', 'N', 'N' from GrupoUsuario where idgrupo <> 0

