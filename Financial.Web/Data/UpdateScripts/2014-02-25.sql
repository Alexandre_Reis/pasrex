declare @data_versao char(10)
set @data_versao = '2014-02-25'
if exists (select 1 from VersaoSchema where DataVersao = @data_versao) return
insert into VersaoSchema VALUES(@data_versao)

CREATE TABLE TabelaProcessamento(
	IdCliente int NOT NULL,
	IdGrupoProcessamento smallint NOT NULL,
 CONSTRAINT PK_TabelaProcessamento PRIMARY KEY CLUSTERED 
(
	IdCliente ASC,
	IdGrupoProcessamento ASC
)
)
GO

ALTER TABLE TabelaProcessamento  WITH CHECK ADD  CONSTRAINT FK_TabelaProcessamento_Cliente FOREIGN KEY(IdCliente)
REFERENCES Cliente (IdCliente)
ON DELETE CASCADE
GO

ALTER TABLE TabelaProcessamento  WITH CHECK ADD  CONSTRAINT FK_TabelaProcessamento_GrupoProcessamento FOREIGN KEY(IdGrupoProcessamento)
REFERENCES GrupoProcessamento (IdGrupoProcessamento)
GO
