declare @data_versao char(10)
set @data_versao = '2013-08-01'
if exists (select 1 from VersaoSchema where DataVersao = @data_versao) return
insert into VersaoSchema VALUES(@data_versao)


alter table posicaocotista add PosicaoIncorporada char(1) null
go
update posicaocotista set PosicaoIncorporada = 'N'
go
alter table posicaocotista alter column PosicaoIncorporada char(1) not null
go

alter table posicaocotistahistorico add PosicaoIncorporada char(1) null
go
update posicaocotistahistorico set PosicaoIncorporada = 'N'
go
alter table posicaocotistahistorico alter column PosicaoIncorporada char(1) not null
go

alter table posicaocotistaabertura add PosicaoIncorporada char(1) null
go
update posicaocotistaabertura set PosicaoIncorporada = 'N'
go
alter table posicaocotistaabertura alter column PosicaoIncorporada char(1) not null
go

ALTER TABLE PosicaoCotista ADD CONSTRAINT DF_PosicaoCotista_PosicaoIncorporada  DEFAULT (('N')) FOR PosicaoIncorporada
GO
ALTER TABLE PosicaoCotistaHistorico ADD CONSTRAINT DF_PosicaoCotistaHistorico_PosicaoIncorporada  DEFAULT (('N')) FOR PosicaoIncorporada
GO
ALTER TABLE PosicaoCotistaAbertura ADD CONSTRAINT DF_PosicaoCotistaAbertura_PosicaoIncorporada  DEFAULT (('N')) FOR PosicaoIncorporada
GO


alter table posicaofundo add PosicaoIncorporada char(1) null
go
update posicaofundo set PosicaoIncorporada = 'N'
go
alter table posicaofundo alter column PosicaoIncorporada char(1) not null
go

alter table posicaofundohistorico add PosicaoIncorporada char(1) null
go
update posicaofundohistorico set PosicaoIncorporada = 'N'
go
alter table posicaofundohistorico alter column PosicaoIncorporada char(1) not null
go

alter table posicaofundoabertura add PosicaoIncorporada char(1) null
go
update posicaofundoabertura set PosicaoIncorporada = 'N'
go
alter table posicaofundoabertura alter column PosicaoIncorporada char(1) not null
go

ALTER TABLE PosicaoFundo ADD CONSTRAINT DF_PosicaoFundo_PosicaoIncorporada  DEFAULT (('N')) FOR PosicaoIncorporada
GO
ALTER TABLE PosicaoFundoHistorico ADD CONSTRAINT DF_PosicaoFundoHistorico_PosicaoIncorporada  DEFAULT (('N')) FOR PosicaoIncorporada
GO
ALTER TABLE PosicaoFundoAbertura ADD CONSTRAINT DF_PosicaoFundoAbertura_PosicaoIncorporada  DEFAULT (('N')) FOR PosicaoIncorporada
GO
