declare @data_versao char(10)
set @data_versao = '2013-09-30'
if exists (select 1 from VersaoSchema where DataVersao = @data_versao) return
insert into VersaoSchema VALUES(@data_versao)

alter table fatorcotacaobolsa add fatoraux decimal (16,8) null
go

update fatorcotacaobolsa set fatoraux = fator
go

alter table fatorcotacaobolsa drop column fator
go

alter table fatorcotacaobolsa add Fator decimal (16,8) null
go

update fatorcotacaobolsa set fator = fatoraux
go

alter table fatorcotacaobolsa drop column fatoraux
go

alter table fatorcotacaobolsa alter column Fator decimal (16, 8) not null
go

