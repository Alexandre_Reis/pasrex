﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Financial.Investidor;
using Financial.Investidor.Enums;
using Financial.Util;
using Financial.Web.Common;
using DevExpress.Web;
using Financial.Bolsa;
using Financial.BMF;
using Financial.Fundo;
using Financial.RendaFixa;
using Financial.Security;
using System.IO;

public partial class DefaultFDesk : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        string userAgent = Request.ServerVariables["HTTP_USER_AGENT"].ToUpper();
        string httpAccept = Request.ServerVariables["HTTP_ACCEPT"].ToUpper();
        MobileDetect mobileDetect = new MobileDetect(userAgent, httpAccept);

        bool ignoraMobile = Request["mobile"] == "false";

        if (!ignoraMobile && (mobileDetect.DetectTierTablet() || mobileDetect.DetectSmartphone()))
        {
            //Pular para mobile
            const string URL_MOBILE = "~/mobile/index.html";
            Response.Redirect(URL_MOBILE);
            return;
        }

        PlaceHolder ph1 = new PlaceHolder();
        this.FindControl("form1").Controls.Add(ph1);
        BrowserPopups popups = new BrowserPopups();

        ASPxPopupControl popupCliente = popups.PopupCliente(false);
        ASPxPopupControl popupCarteira = popups.PopupCarteira(false);
        ASPxPopupControl popupAtivoBolsa = popups.PopupAtivoBolsa(false);
        ASPxPopupControl popupAtivoBMF = popups.PopupAtivoBMF();
        ASPxPopupControl popupTituloRF = popups.PopupTituloRF();

        ph1.Controls.Add(popupCliente);
        ph1.Controls.Add(popupCarteira);
        ph1.Controls.Add(popupAtivoBMF);
        ph1.Controls.Add(popupAtivoBolsa);
        ph1.Controls.Add(popupTituloRF);

        //DiretorioBaseHelp.HRef = ConfigurationManager.AppSettings["DiretorioBaseHelp"] + "/fdesk/index.htm";
        DiretorioBaseHelp.HRef = "help/fdesk/application.htm";

        string cliente = ConfigurationManager.AppSettings["Cliente"];
        if (!string.IsNullOrEmpty(cliente))
        {
            //Checar se todos os web.configs estao corretos antes de implementar a linha abaixo:
            //Title = cliente + ": Portal do Cliente";
        }

    }

    // <summary>
    /// Carregamento da imagem de Logotipo    
    /// Se estiver Definido na tabela Usuário carrega a imagem do Usuário
    /// Se não carrega a imagem logo_cliente.png
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void divLogotipoOnPreRender(object sender, EventArgs e) {

        string img = "imagensPersonalizadas/logo_cliente.png"; // Default
        //string img = "imagensPersonalizadas/logo_clienteSolidus.png";

        Usuario u = new Usuario();
        u.Query.Select(u.Query.IdUsuario, u.Query.Logotipo)
               .Where(u.Query.Login == HttpContext.Current.User.Identity.Name);

        if (u.Query.Load()) {
            if (!String.IsNullOrEmpty(u.Logotipo)) {

                // Usuário tem Logotipo Definido - Carrega o Logotipo do Usuário                                
                string path = AppDomain.CurrentDomain.BaseDirectory + "\\imagensPersonalizadas\\" + u.Logotipo.Trim();

                if (File.Exists(path)) {
                    img = "imagensPersonalizadas/" + u.Logotipo.Trim();
                }
            }
        }

        string image = String.Format("transparent url({0}) no-repeat", img);

        this.divLogoClient.Style.Add("background", image);
    }

    protected void EsDSCliente_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        ClienteCollection clienteCollection = new ClienteCollection();
        clienteCollection.BuscaClientesComAcesso(HttpContext.Current.User.Identity.Name);

        e.Collection = clienteCollection;
    }

    protected void EsDSCarteira_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        CarteiraCollection carteiraCollection = new CarteiraCollection();
        carteiraCollection.BuscaCarteirasFundos();

        e.Collection = carteiraCollection;
    }

    protected void EsDSAtivoBolsa_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        AtivoBolsaCollection coll = new AtivoBolsaCollection();
        coll.Query.OrderBy(coll.Query.CdAtivoBolsa.Ascending);
        coll.LoadAll();

        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }

    protected void EsDSAtivoBMF_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        AtivoBMFCollection coll = new AtivoBMFCollection();
        coll.Query.OrderBy(coll.Query.CdAtivoBMF.Ascending, coll.Query.Serie.Ascending);
        coll.LoadAll();

        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }

    protected void EsDSTituloRF_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        TituloRendaFixaCollection coll = new TituloRendaFixaCollection();
        coll.Query.OrderBy(coll.Query.DataVencimento.Ascending);

        coll.Query.Load();

        e.Collection = coll;

    }
}