<%@ WebService Language="C#" Class="InvestidorDebug" %>

using System;
using System.Data;
using System.Web;
using System.Collections;
using System.Web.Services;
using System.Web.Services.Protocols;
using System.ComponentModel;
using Financial.Investidor.Enums;
using Financial.Investidor.Controller;
using Financial.Processamento;
using EntitySpaces.Interfaces;
using System.Configuration;
using Financial.Security;
using System.Collections.Specialized;
using Financial.Investidor;
using Financial.Common.Enums;
using Financial.Util;
using Financial.CRM;
using Financial.InvestidorCotista;
using Financial.InvestidorCotista.Enums;
using System.Collections.Generic;

[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
public class InvestidorDebug  : System.Web.Services.WebService {

    public ValidateLogin Authentication;

    [SoapHeader("Authentication")]
    [WebMethod]
    public void ProcessaDiario(int idCliente, Financial.Investidor.Enums.TipoProcessamento tipoProcessamento, Financial.Investidor.Controller.ParametrosProcessamento parametrosProcessamento)
    {
        ProcessoDiario processoDiario = new ProcessoDiario();
        processoDiario.ProcessaDiario(idCliente, tipoProcessamento, parametrosProcessamento);

        Cliente cliente = new Cliente();
        if (cliente.LoadByPrimaryKey(idCliente))
        {
            InserirLancamentosContaGraficaFundos(idCliente, cliente.DataDia.Value);
        }
    }

    private void InserirLancamentosContaGraficaFundos(int idCliente, DateTime dataProcessamento)
    {
        WSFinanceiroItau.Financeiro ws = new WSFinanceiroItau.Financeiro();

        OperacaoCotistaCollection operacoes = new OperacaoCotistaCollection();
        OperacaoCotistaQuery operacaoQuery = new OperacaoCotistaQuery("O");
        CotistaQuery cotistaQuery = new CotistaQuery("C");
        PessoaQuery pessoaQuery = new PessoaQuery("P");
        operacaoQuery.Select(operacaoQuery.IdCotista, operacaoQuery.ValorIOF.Sum(), operacaoQuery.ValorIR.Sum(),
            operacaoQuery.ValorLiquido.Sum(), pessoaQuery.Cpfcnpj);
        operacaoQuery.Where(operacaoQuery.DataLiquidacao.Equal(dataProcessamento),
            operacaoQuery.IdCarteira.Equal(idCliente),
            operacaoQuery.TipoOperacao.In((byte)TipoOperacaoCotista.ComeCotas, (byte)TipoOperacaoCotista.ResgateBruto, (byte)TipoOperacaoCotista.ResgateCotas,
                (byte)TipoOperacaoCotista.ResgateCotasEspecial, (byte)TipoOperacaoCotista.ResgateLiquido, (byte)TipoOperacaoCotista.ResgateTotal));
        operacaoQuery.LeftJoin(cotistaQuery).On(cotistaQuery.IdCotista.Equal(operacaoQuery.IdCotista));
        operacaoQuery.LeftJoin(pessoaQuery).On(pessoaQuery.IdPessoa.Equal(cotistaQuery.IdPessoa));
        operacaoQuery.GroupBy(operacaoQuery.IdCotista, pessoaQuery.Cpfcnpj);
        operacoes.Load(operacaoQuery);

        foreach (OperacaoCotista operacao in operacoes)
        {
            string cpf = Utilitario.RemoveCaracteresEspeciais(Convert.ToString(operacao.GetColumn("Cpfcnpj")));
            decimal valor = operacao.ValorIOF.Value + operacao.ValorIR.Value + operacao.ValorLiquido.Value;

            /*ws.InserirLancametosContaGraficaFundos(cpf, valor, true,
                WSFinanceiroItau.TipoLanctoFundosContaGraficaEnum.ResgateFundos, true, null, 0, true);*/
        }
    }

    [SoapHeader("Authentication")]
    [WebMethod]
    public void ProcessaPeriodo()
    //public void ProcessaPeriodo(int idCliente, Financial.Investidor.Enums.TipoProcessamento tipoProcessamento, Financial.Investidor.Controller.ParametrosProcessamento parametrosProcessamento, DateTime dataFinal)
    {
        int idCliente = 5064;
        Financial.Investidor.Enums.TipoProcessamento tipoProcessamento = TipoProcessamento.Retroacao;
        Financial.Investidor.Controller.ParametrosProcessamento parametrosProcessamento = new ParametrosProcessamento();
        DateTime dataFinal = new DateTime(2014, 6, 3);
        //ProcessoPeriodo processoPeriodo = new ProcessoPeriodo();
        //processoPeriodo.ProcessaPeriodo(idCliente, tipoProcessamento, parametrosProcessamento, dataFinal);

        #region Inserir Lancamentos Conta Grafica
        Cliente cliente = new Cliente();
        List<esQueryItem> campos = new List<esQueryItem>();
        campos.Add(cliente.Query.IdCliente);
        campos.Add(cliente.Query.Status);
        campos.Add(cliente.Query.DataDia);
        campos.Add(cliente.Query.IsProcessando);
        campos.Add(cliente.Query.IdLocal);

        if (cliente.LoadByPrimaryKey(campos, idCliente))
        {
            if (tipoProcessamento == TipoProcessamento.AvancoPeriodo)
            {

                short idLocal = cliente.IdLocal.Value;
                DateTime dataDia = cliente.DataDia.Value;
                int status = cliente.Status.Value;
                DateTime dataAux = dataDia;

                //Garante que se estiver fechado, o 1o c�lculo di�rio ser� feito com o status aberto
                if (status == (int)StatusCliente.Divulgado)
                {
                    if (idLocal == LocalFeriadoFixo.Brasil)
                    {
                        dataAux = Calendario.AdicionaDiaUtil(dataAux, 1, LocalFeriadoFixo.Brasil, TipoFeriado.Brasil);
                    }
                    else
                    {
                        dataAux = Calendario.AdicionaDiaUtil(dataAux, 1, idLocal, TipoFeriado.Outros);
                    }
                }

                while (DateTime.Compare(dataFinal, dataAux) >= 0)
                {
                    InserirLancamentosContaGraficaFundos(idCliente, dataAux);

                    if (idLocal == LocalFeriadoFixo.Brasil)
                    {
                        dataAux = Calendario.AdicionaDiaUtil(dataAux, 1, LocalFeriadoFixo.Brasil, TipoFeriado.Brasil);
                    }
                    else
                    {
                        dataAux = Calendario.AdicionaDiaUtil(dataAux, 1, idLocal, TipoFeriado.Outros);
                    }
                }
            }
            else if (tipoProcessamento == TipoProcessamento.Retroacao)
            {
                InserirLancamentosContaGraficaFundos(idCliente, dataFinal);
            }
        }
        #endregion
    }

    public class ValidateLogin : SoapHeader
    {
        public string Username;
        public string Password;
    }

    private bool CheckAuthentication(string username, string password)
    {
        FinancialMembershipProvider financialMembershipProvider = new FinancialMembershipProvider();
        NameValueCollection listaValores = new NameValueCollection();
        listaValores.Add("passwordFormat", "Encrypted");
        financialMembershipProvider.Initialize(null, listaValores);

        bool autenticado = financialMembershipProvider.ValidateUser(username, password);
        return autenticado;
    }

    
}

