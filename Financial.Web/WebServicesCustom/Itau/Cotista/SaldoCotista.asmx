<%@ WebService Language="C#" Class="SaldoCotistaWS" %>

using System;
using System.Web;
using System.Web.Services;
using System.Web.Services.Protocols;
using System.Data;
using Financial.InvestidorCotista;
using Financial.CRM;
using Financial.Investidor;

[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
public class SaldoCotistaWS  : System.Web.Services.WebService {

    [WebMethod]
    public DataTable Exporta(DateTime dataReferencia)
    {
        
        DataTable result = new DataTable();
        PosicaoCotistaHistoricoQuery posicaoQuery = new PosicaoCotistaHistoricoQuery("P");
        PessoaQuery pessoaCotistaQuery = new PessoaQuery("C");
        PessoaQuery pessoaFundoQuery = new PessoaQuery("F");
        CotistaQuery cotistaQuery = new CotistaQuery("O");

        posicaoQuery.Select(
            pessoaCotistaQuery.Cpfcnpj.Cast(EntitySpaces.Interfaces.esCastType.Int64).As("CPFCNPJ_Cotista"), 
            posicaoQuery.IdCotista, 
            cotistaQuery.CodigoInterface.As("CodigoInterfaceCotista"),
            pessoaFundoQuery.Cpfcnpj.Cast(EntitySpaces.Interfaces.esCastType.Int64).As("CPFCNPJ_Fundo"),
            posicaoQuery.DataHistorico.Cast(EntitySpaces.Interfaces.esCastType.String).As("DataReferencia"),
            posicaoQuery.ValorBruto.Sum(),
            posicaoQuery.Quantidade.Sum());
        
        posicaoQuery.Where(posicaoQuery.DataHistorico == dataReferencia,
                           posicaoQuery.Quantidade.NotEqual(0));

        posicaoQuery.LeftJoin(cotistaQuery).On(posicaoQuery.IdCotista == cotistaQuery.IdCotista);
        posicaoQuery.LeftJoin(pessoaCotistaQuery).On(cotistaQuery.IdPessoa == pessoaCotistaQuery.IdPessoa);
        posicaoQuery.LeftJoin(pessoaFundoQuery).On(cotistaQuery.IdPessoa == pessoaFundoQuery.IdPessoa);

        pessoaCotistaQuery.GroupBy(pessoaCotistaQuery.Cpfcnpj.As("CPFCNPJ_Cotista"),
                                   posicaoQuery.IdCotista,
                                   cotistaQuery.CodigoInterface.As("CodigoInterfaceCotista"),
                                   pessoaFundoQuery.Cpfcnpj.As("CPFCNPJ_Fundo"),
                                   posicaoQuery.DataHistorico.As("DataReferencia"));
        
        result = posicaoQuery.LoadDataTable();

        foreach (DataRow row in result.Rows)
        {
            row["DataReferencia"] = dataReferencia.ToString("yyyyMMdd");
            row["ValorBruto"] = Convert.ToInt64(Convert.ToDouble(row["ValorBruto"]) * 100);
            row["Quantidade"] = Convert.ToInt64(Convert.ToDouble(row["Quantidade"]) * 1000000);
        }
                        
        return result;
    }
    
    
}

