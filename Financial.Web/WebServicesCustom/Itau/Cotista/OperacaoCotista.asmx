<%@ WebService Language="C#" Class="OperacaoCotistaWSItau" %>

using System;
using System.Web;
using System.Web.Services;
using System.Web.Services.Protocols;
using System.Data;
using Financial.InvestidorCotista; 
using Financial.CRM;
using Financial.Investidor;
using Financial.Fundo;

[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
public class OperacaoCotistaWSItau : System.Web.Services.WebService
{

    [WebMethod]
    public DataTable Exporta(DateTime dataReferencia)
    {

        OperacaoCotistaQuery operacaoQuery = new OperacaoCotistaQuery("P");
        PessoaQuery pessoaCotistaQuery = new PessoaQuery("C");
        PessoaQuery pessoaFundoQuery = new PessoaQuery("F");
        CotistaQuery cotistaQuery = new CotistaQuery("O");
        CarteiraQuery carteiraQuery = new CarteiraQuery("A");

        operacaoQuery.Select(
            pessoaCotistaQuery.Cpfcnpj.Cast(EntitySpaces.Interfaces.esCastType.Int64).As("CPFCNPJ_Cotista"),
            operacaoQuery.IdCotista,
            cotistaQuery.CodigoInterface.As("CodigoInterfaceCotista"),
            pessoaFundoQuery.Cpfcnpj.Cast(EntitySpaces.Interfaces.esCastType.Int64).As("CPFCNPJ_Fundo"),
            carteiraQuery.TipoCota.Cast(EntitySpaces.Interfaces.esCastType.String),//Checar se � tipo cota (fechamento/abertura) ou tipo de fundo (aberto/fechado - fixa, variavel)!
            operacaoQuery.RendimentoResgate.As("Rendimento"),
            operacaoQuery.RendimentoResgate.As("ValorBaseCalculo"),
            operacaoQuery.Observacao.As("TipoImposto"),
            operacaoQuery.ValorIR.As("ValorImposto"),
            operacaoQuery.DataOperacao.Cast(EntitySpaces.Interfaces.esCastType.String),
            operacaoQuery.DataLiquidacao.Cast(EntitySpaces.Interfaces.esCastType.String));

        operacaoQuery.Where(operacaoQuery.DataOperacao == dataReferencia & operacaoQuery.ValorIR > 0);

        operacaoQuery.LeftJoin(pessoaCotistaQuery).On(cotistaQuery.IdPessoa == pessoaCotistaQuery.IdPessoa);
        operacaoQuery.LeftJoin(pessoaFundoQuery).On(cotistaQuery.IdPessoa == pessoaFundoQuery.IdPessoa);
        operacaoQuery.LeftJoin(cotistaQuery).On(operacaoQuery.IdCotista == cotistaQuery.IdCotista);
        operacaoQuery.LeftJoin(carteiraQuery).On(operacaoQuery.IdCarteira == carteiraQuery.IdCarteira);

        DataTable dataTableIR = operacaoQuery.LoadDataTable();

        foreach (DataRow row in dataTableIR.Rows)
        {
            int tipoCota = Convert.ToInt16(row["TipoCota"]);
            row["TipoCota"] = tipoCota == (int)Financial.Fundo.Enums.TipoCotaFundo.Abertura ? "A" : "F";
            
            row["Rendimento"] = Convert.ToInt64(Convert.ToDouble(row["Rendimento"]) * 100);
            row["ValorBaseCalculo"] = Convert.ToInt64(Convert.ToDouble(row["ValorBaseCalculo"]) * 100);
            row["DataOperacao"] = Convert.ToDateTime(row["DataOperacao"]).ToString("yyyyMMdd");
            row["DataLiquidacao"] = Convert.ToDateTime(row["DataLiquidacao"]).ToString("yyyyMMdd");
            row["TipoImposto"] = "IR";
        }


        operacaoQuery = new OperacaoCotistaQuery("P");
        pessoaCotistaQuery = new PessoaQuery("C");
        pessoaFundoQuery = new PessoaQuery("F");
        cotistaQuery = new CotistaQuery("O");
        carteiraQuery = new CarteiraQuery("A");

        operacaoQuery.Select(
           pessoaCotistaQuery.Cpfcnpj.Cast(EntitySpaces.Interfaces.esCastType.Int64).As("CPFCNPJ_Cotista"),
           operacaoQuery.IdCotista,
           cotistaQuery.CodigoInterface.As("CodigoInterfaceCotista"),
           pessoaFundoQuery.Cpfcnpj.Cast(EntitySpaces.Interfaces.esCastType.Int64).As("CPFCNPJ_Fundo"),
           carteiraQuery.TipoCota.Cast(EntitySpaces.Interfaces.esCastType.String),//Checar se � tipo cota (fechamento/abertura) ou tipo de fundo (aberto/fechado - fixa, variavel)!
           operacaoQuery.RendimentoResgate.As("Rendimento"),
           operacaoQuery.RendimentoResgate.As("ValorBaseCalculo"),
           operacaoQuery.Observacao.As("TipoImposto"),
           operacaoQuery.ValorIOF.As("ValorImposto"),
           operacaoQuery.DataOperacao.Cast(EntitySpaces.Interfaces.esCastType.String),
           operacaoQuery.DataLiquidacao.Cast(EntitySpaces.Interfaces.esCastType.String));

        operacaoQuery.Where(operacaoQuery.DataOperacao == dataReferencia & operacaoQuery.ValorIOF > 0);

        operacaoQuery.LeftJoin(cotistaQuery).On(operacaoQuery.IdCotista == cotistaQuery.IdCotista);
        operacaoQuery.LeftJoin(pessoaCotistaQuery).On(cotistaQuery.IdPessoa == pessoaCotistaQuery.IdPessoa);
        operacaoQuery.LeftJoin(pessoaFundoQuery).On(cotistaQuery.IdPessoa == pessoaFundoQuery.IdPessoa);
        operacaoQuery.LeftJoin(carteiraQuery).On(operacaoQuery.IdCarteira == carteiraQuery.IdCarteira);

        DataTable dataTableIOF = operacaoQuery.LoadDataTable();

        foreach (DataRow row in dataTableIOF.Rows)
        {
            int tipoCota = Convert.ToInt16(row["TipoCota"]);
            row["TipoCota"] = tipoCota == (int)Financial.Fundo.Enums.TipoCotaFundo.Abertura ? "A" : "F";
            
            row["Rendimento"] = Convert.ToInt64(Convert.ToDouble(row["Rendimento"]) * 100);
            row["ValorBaseCalculo"] = Convert.ToInt64(Convert.ToDouble(row["ValorBaseCalculo"]) * 100);
            row["DataOperacao"] = Convert.ToDateTime(row["DataOperacao"]).ToString("yyyyMMdd");
            row["DataLiquidacao"] = Convert.ToDateTime(row["DataLiquidacao"]).ToString("yyyyMMdd");
            row["TipoImposto"] = "IOF";
        }

        dataTableIR.Merge(dataTableIOF);
        dataTableIR.AcceptChanges();
        return dataTableIR;
    }
}

