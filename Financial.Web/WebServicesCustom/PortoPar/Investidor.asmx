<%@ WebService Language="C#" Class="Financial.WebServicesCustom.PortoPar.Investidor" %>

using System;
using System.Web;
using System.Web.Services;
using System.Web.Services.Protocols;
using Financial.Investidor;
using Financial.Investidor.Controller;
using System.Collections.Generic;
using Financial.Processamento;
using Financial.Investidor.Enums;
using Financial.Util;
using Financial.Common.Enums;
using Financial.Fundo;
using Financial.InvestidorCotista;
using Financial.InvestidorCotista.Enums;
using Financial.Interfaces.Export;
using System.Configuration;
using Financial.Security;
using Financial.Web.Util;
using Financial.Security.Enums;

namespace Financial.WebServicesCustom.PortoPar
{
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    public class Investidor : System.Web.Services.WebService
    {
        [WebMethod]
        public DateTime CalculaDataLiquidacao(string TipoOperacao, DateTime Data, int? IdCarteira)
        {
            Carteira carteira = new Carteira();
            if (IdCarteira.HasValue)
            {
                carteira.LoadByPrimaryKey(IdCarteira.Value);
            }

            DateTime dataAjustada = Data;
            if (TipoOperacao == "FUTURO" || TipoOperacao == "BOLSA" || TipoOperacao == "BMF")
            {
                dataAjustada = Calendario.AdicionaDiaUtil(Data, 1, LocalFeriadoFixo.Bovespa, TipoFeriado.Brasil);
            }
            else if (TipoOperacao == "RF" || TipoOperacao == "COMPROMISSADA-COMPRA")
            {
                //D0 - nao mudar data a nao ser que nao seja dia util
                if (!Calendario.IsDiaUtil(Data))
                {
                    dataAjustada = Calendario.AdicionaDiaUtil(Data, 1);
                }
            }
            else if (TipoOperacao == "FUNDO-APLICACAO")
            {
                DateTime dataLiquidacao = new DateTime();

                dataAjustada = Calendario.AdicionaDiaUtil(Data, carteira.DiasLiquidacaoAplicacao.Value);

            }
            else if (TipoOperacao == "FUNDO-RESGATE")
            {
                if (carteira.ContagemDiasConversaoResgate.Value == (byte)Financial.Fundo.Enums.ContagemDiasLiquidacaoResgate.DiasUteis)
                {
                    dataAjustada = Calendario.AdicionaDiaUtil(Data, carteira.DiasLiquidacaoResgate.Value);
                }
                else
                {
                    //Conta por Dias �teis em cima da data de convers�o
                    dataAjustada = Calendario.AdicionaDiaUtil(Data, carteira.DiasLiquidacaoResgate.Value);
                }
            }

            return dataAjustada;
        }

        [WebMethod]
        public DateTime CalculaDataDiaUtil(DateTime Data, int NumeroDias)
        {
            DateTime dataAjustada;
            if (NumeroDias == 0 && !Calendario.IsDiaUtil(Data))
            {
                dataAjustada = Calendario.AdicionaDiaUtil(Data, 1);
            }
            else
            {
                dataAjustada = Calendario.AdicionaDiaUtil(Data, NumeroDias);
            }
            return dataAjustada;
        }

        [WebMethod]
        public void IntegraOperacaoItau()
        {
            //Enviar todas as ordemcotista com dataoperacao = hoje e reenviar aquelas com erro de saldo insuficiente
            OrdemCotistaCollection ordens = new OrdemCotistaCollection();
            ordens.Query.Where(
                (ordens.Query.DataOperacao.LessThanOrEqual(DateTime.Today) & ordens.Query.StatusIntegracao.Equal((byte)StatusOrdemIntegracao.Agendado)) |
                ordens.Query.StatusIntegracao.Equal((byte)StatusOrdemIntegracao.SemSaldo));

            ordens.Load(ordens.Query);
            
            foreach (OrdemCotista ordemCotista in ordens)
            {
                OrdemCotista ordemLoop = new OrdemCotista();
                ordemLoop.LoadByPrimaryKey(ordemCotista.IdOperacao.Value);
                ordemLoop.IntegraItau(2);
            }
            
        }

        [WebMethod]
        public decimal RetornaPUConvertido(decimal taxa, DateTime dataOperacao, DateTime dataVencimento)
        {
            return new Financial.BMF.PosicaoBMF().RetornaPUConvertido(taxa, dataOperacao, dataVencimento);
        }

        [WebMethod]
        public decimal RetornaPUConvertidoDDI(decimal taxa, DateTime dataOperacao, DateTime dataVencimento)
        {
            return new Financial.BMF.PosicaoBMF().RetornaPUConvertidoDDI(taxa, dataOperacao, dataVencimento);
        }
        
        [WebMethod]
        public void ProcessaDiario(int IdGrupoProcessamento, DateTime DataDia, int TipoProcessamento)
        {
            ClienteCollection clientes = new ClienteCollection();
            clientes.Query.Where(clientes.Query.IdGrupoProcessamento.Equal(IdGrupoProcessamento),
                clientes.Query.DataDia.Equal(DataDia.Date));

            clientes.Load(clientes.Query);

            ControllerInvestidor controllerInvestidor = new ControllerInvestidor();
            List<Cliente> clientesOrdenados = controllerInvestidor.OrdenaProcessamentoClientes(clientes);
            IntegracaoRendaFixa integracaoRendaFixa = (IntegracaoRendaFixa)ParametrosConfiguracaoSistema.Integracoes.IntegracaoRendaFixa;


            foreach (Cliente cliente in clientesOrdenados)
            {
                int idCliente = cliente.IdCliente.Value;
                ParametrosProcessamento parametrosProcessamento = new ParametrosProcessamento();
                parametrosProcessamento.ProcessaBolsa = true;
                parametrosProcessamento.ProcessaBMF = true;
                parametrosProcessamento.ProcessaRendaFixa = true;
                parametrosProcessamento.ProcessaSwap = true;
                parametrosProcessamento.IntegraBolsa = false;
                parametrosProcessamento.IntegraBTC = false;
                parametrosProcessamento.IntegraBMF = false;
                parametrosProcessamento.IntegracaoRendaFixa = (int)integracaoRendaFixa;
                parametrosProcessamento.IntegraCC = false;
                parametrosProcessamento.IgnoraOperacoes = false;
                parametrosProcessamento.MantemFuturo = false;
                parametrosProcessamento.RemuneraRF = false;
                parametrosProcessamento.CravaCota = false;

                TabelaInterfaceClienteCollection tabelaInterfaceClienteCollection = new TabelaInterfaceClienteCollection();
                tabelaInterfaceClienteCollection.Query.Select(tabelaInterfaceClienteCollection.Query.TipoInterface);
                tabelaInterfaceClienteCollection.Query.Where(tabelaInterfaceClienteCollection.Query.IdCliente.Equal(idCliente));
                tabelaInterfaceClienteCollection.Query.Load();
                List<int> listaTabelaInterfaceCliente = new List<int>();

                foreach (TabelaInterfaceCliente tabelaInterfaceCliente in tabelaInterfaceClienteCollection)
                {
                    listaTabelaInterfaceCliente.Add(tabelaInterfaceCliente.TipoInterface.Value);
                }
                parametrosProcessamento.ListaTabelaInterfaceCliente = listaTabelaInterfaceCliente;


                Financial.Investidor.Enums.TipoProcessamento tipoProcessamento = (Financial.Investidor.Enums.TipoProcessamento)TipoProcessamento;

                ProcessoDiario processoDiario = new ProcessoDiario();
                processoDiario.ProcessaDiario(idCliente, tipoProcessamento, parametrosProcessamento);

            }

        }
    }
}