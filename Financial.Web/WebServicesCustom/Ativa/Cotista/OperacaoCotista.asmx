<%@ WebService Language="C#" Class="OperacaoCotistaWSAtiva" %>

using System;
using System.Web;
using System.Web.Services;
using System.Web.Services.Protocols;
using System.Collections.Generic;
using Financial.Integracao.Excel;
using Financial.InvestidorCotista;
using Financial.Security;
using System.Collections.Specialized;
using Financial.Fundo;
using Financial.Investidor;
using Financial.Investidor.Enums;
using Financial.Util;

[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
public class OperacaoCotistaWSAtiva : System.Web.Services.WebService
{

    public ValidateLogin Authentication;
    [SoapHeader("Authentication")]
    [WebMethod]
    public void Importa(int IdCotista, int IdCarteira, DateTime DataOperacao,
        DateTime? DataConversao, DateTime? DataLiquidacao, byte TipoOperacao, byte TipoResgate, decimal Quantidade, decimal CotaOperacao, decimal ValorBruto,
        decimal ValorLiquido, decimal ValorIR, decimal ValorIOF, decimal ValorPerformance, decimal RendimentoResgate,
        int? idOperacaoAuxiliar)
    {
        
        if(!CheckAuthentication(Authentication.Username, Authentication.Password)){
            string msgErro = "Usu�rio n�o autenticado";
            throw new Exception(msgErro);
        }
        
        ValoresExcelOperacaoCotista item = new ValoresExcelOperacaoCotista();
        item.IdCotista = IdCotista;
        item.IdCarteira = IdCarteira;
        item.DataOperacao = DataOperacao;
        item.DataConversao = DataConversao;
        item.DataLiquidacao = DataLiquidacao;

        item.TipoOperacao = TipoOperacao;
        item.TipoResgate = TipoResgate;
        item.Quantidade = Quantidade;
        item.CotaOperacao = CotaOperacao;
        item.ValorBruto = ValorBruto;
        item.ValorLiquido = ValorLiquido;
        item.ValorIR = ValorIR;
        item.ValorIOF = ValorIOF;
        item.ValorPerformance = ValorPerformance;
        item.RendimentoResgate = RendimentoResgate;
        item.IdOperacaoAuxiliar = idOperacaoAuxiliar;

        //Pelo codigoYMF para pegar o Id do clube e fundo
        ClienteQuery clienteQuery = new ClienteQuery("C");
        ClienteInterfaceQuery clienteInterfaceQuery = new ClienteInterfaceQuery("I");
                
        clienteInterfaceQuery.Select(clienteInterfaceQuery.CodigoYMF,
                                     clienteInterfaceQuery.IdCliente);
        clienteInterfaceQuery.InnerJoin(clienteQuery).On(clienteQuery.IdCliente == clienteInterfaceQuery.IdCliente);
        clienteInterfaceQuery.Where(clienteQuery.IdTipo.In((int)TipoClienteFixo.Clube, (int)TipoClienteFixo.Fundo, (int)TipoClienteFixo.FDIC),
                                    clienteInterfaceQuery.CodigoYMF.IsNotNull());

        ClienteInterfaceCollection clienteInterfaceCollection = new ClienteInterfaceCollection();
        clienteInterfaceCollection.Load(clienteInterfaceQuery);

        item.IdCarteira = -1;
        foreach (ClienteInterface clienteInterface in clienteInterfaceCollection)
        {
            if (Utilitario.IsInteger(clienteInterface.CodigoYMF.Replace("AT", "")))
            {
                if (Convert.ToInt32(clienteInterface.CodigoYMF.Replace("AT", "")) == IdCarteira)
                {
                    item.IdCarteira = clienteInterface.IdCliente.Value;
                    break;
                }
            }
        }
        //************************************************************

        //Pelo codigoInterface para pegar o Id do cotista
        CotistaCollection cotistaCollection = new CotistaCollection();
        cotistaCollection.Query.Select(cotistaCollection.Query.CodigoInterface,
                                       cotistaCollection.Query.IdCotista);
        cotistaCollection.Query.Where(cotistaCollection.Query.CodigoInterface.IsNotNull());
        cotistaCollection.Query.Load();

        item.IdCotista = -1;
        foreach (Cotista cotista in cotistaCollection)
        {
            if (Utilitario.IsInteger(cotista.CodigoInterface.Replace("AT", "")))
            {
                if (Convert.ToInt32(cotista.CodigoInterface.Replace("AT", "")) == IdCotista)
                {
                    item.IdCotista = cotista.IdCotista.Value;
                    break;
                }
            }
        }
        //************************************************************

        ImportacaoBasePage basePage = new ImportacaoBasePage();
        basePage.valoresExcelOperacaoCotista = new List<ValoresExcelOperacaoCotista>();
        basePage.valoresExcelOperacaoCotista.Add(item);

        basePage.CarregaOperacaoCotista(false);
    }

    public class ValidateLogin : SoapHeader
    {
        public string Username;
        public string Password;
    }

    private bool CheckAuthentication(string username, string password)
    {
        FinancialMembershipProvider financialMembershipProvider = new FinancialMembershipProvider();
        NameValueCollection listaValores = new NameValueCollection();
        listaValores.Add("passwordFormat", "Encrypted");
        financialMembershipProvider.Initialize(null, listaValores);

        bool autenticado = financialMembershipProvider.ValidateUser(username, password);
        return autenticado;
    }
    
    [WebMethod]
    public OperacaoCotistaCollection Exporta(int? IdOperacao, int? IdCotista, int? IdCarteira, DateTime? DataOperacao)
    {
        OperacaoCotistaCollection operacoes = new OperacaoCotistaCollection();

        if (IdOperacao.HasValue)
        {
            operacoes.Query.Where(operacoes.Query.IdOperacao.Equal(IdOperacao));
        }
        
        if (IdCotista.HasValue)
        {
            operacoes.Query.Where(operacoes.Query.IdCotista.Equal(IdCotista));
        }

        if (IdCarteira.HasValue)
        {
            operacoes.Query.Where(operacoes.Query.IdCarteira.Equal(IdCarteira));
        }

        if (DataOperacao.HasValue)
        {
            operacoes.Query.Where(operacoes.Query.DataOperacao.Equal(DataOperacao.Value.Date));
        }
        
        operacoes.Load(operacoes.Query);
        return operacoes;
    }
}