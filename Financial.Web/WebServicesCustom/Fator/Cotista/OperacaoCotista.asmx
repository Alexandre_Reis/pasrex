<%@ WebService Language="C#" Class="OperacaoCotistaWSFator" %>

using System;
using System.Web;
using System.Web.Services;
using System.Web.Services.Protocols;
using System.Collections.Specialized;
using System.Collections.Generic;
using System.Text.RegularExpressions;

using Financial.InvestidorCotista;
using Financial.Security;
using Financial.Investidor;
using Financial.CRM;
using Financial.Fundo;
using Financial.Integracao.Excel;

using Financial.InvestidorCotista.Enums;

[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
public class OperacaoCotistaWSFator : System.Web.Services.WebService
{

    public ValidateLogin Authentication;

    [SoapHeader("Authentication")]
    [WebMethod]
    public OperacaoCotistaViewModel Importa(int IdCotista, int IdCarteira, DateTime DataOperacao,
        DateTime? DataConversao, DateTime? DataLiquidacao, byte TipoOperacao, byte TipoResgate, decimal Quantidade, decimal CotaOperacao, decimal ValorBruto,
        decimal ValorLiquido, decimal ValorIR, decimal ValorIOF, decimal ValorPerformance, decimal RendimentoResgate, byte Status)
    {

        if (!CheckAuthentication(Authentication.Username, Authentication.Password))
        {
            string msgErro = "Usu�rio n�o autenticado";
            throw new Exception(msgErro);
        }

        Cliente cliente = new Cliente();
        bool isContaOrdem = false;
        if (cliente.LoadByPrimaryKey(IdCarteira))
        {
            isContaOrdem = cliente.TipoControle == (byte)Financial.Investidor.Enums.TipoControleCliente.Cotista;
        }

        OperacaoCotistaViewModel operacaoCotistaViewModel = null;
        if (isContaOrdem)
        {
            ValoresExcelOrdemCotista item = new ValoresExcelOrdemCotista();
            item.IdCotista = IdCotista;
            item.IdCarteira = IdCarteira;
            item.DataOperacao = DataOperacao;
            item.DataConversao = DataConversao;
            item.DataLiquidacao = DataLiquidacao;
            item.TipoOperacao = TipoOperacao;
            item.TipoResgate = TipoResgate;
            item.Quantidade = Quantidade;
            item.ValorBruto = ValorBruto;
            item.ValorLiquido = ValorLiquido;
            item.Status = Status;

            ImportacaoBasePage basePage = new ImportacaoBasePage();
            basePage.valoresExcelOrdemCotista = new List<ValoresExcelOrdemCotista>();
            basePage.valoresExcelOrdemCotista.Add(item);

            OrdemCotistaCollection ordens = basePage.CarregaOrdemCotista(false);

            if (ordens.Count > 0)
            {
                OrdemCotista ordemCotista = ordens[0];
                operacaoCotistaViewModel = new OperacaoCotistaViewModel(ordemCotista);
            }
        }
        else
        {
            ValoresExcelOperacaoCotista item = new ValoresExcelOperacaoCotista();
            item.IdCotista = IdCotista;
            item.IdCarteira = IdCarteira;
            item.DataOperacao = DataOperacao;
            item.DataConversao = DataConversao;
            item.DataLiquidacao = DataLiquidacao;

            item.TipoOperacao = TipoOperacao;
            item.TipoResgate = TipoResgate;
            item.Quantidade = Quantidade;
            item.CotaOperacao = CotaOperacao;
            item.ValorBruto = ValorBruto;
            item.ValorLiquido = ValorLiquido;
            item.ValorIR = ValorIR;
            item.ValorIOF = ValorIOF;
            item.ValorPerformance = ValorPerformance;
            item.RendimentoResgate = RendimentoResgate;

            ImportacaoBasePage basePage = new ImportacaoBasePage();
            basePage.valoresExcelOperacaoCotista = new List<ValoresExcelOperacaoCotista>();
            basePage.valoresExcelOperacaoCotista.Add(item);

            OperacaoCotistaCollection operacoes = basePage.CarregaOperacaoCotista(false);

            if (operacoes.Count > 0)
            {
                OperacaoCotista operacaoCotista = operacoes[0];
                operacaoCotistaViewModel = new OperacaoCotistaViewModel(operacaoCotista);
            }
        }

        return operacaoCotistaViewModel;

    }

    [SoapHeader("Authentication")]
    [WebMethod]
    public List<OperacaoCotistaViewModel> Exporta(int? IdOperacao, int? IdCotista, int? IdCarteira, DateTime? DataOperacao,
        string CpfcnpjCotista, string CpfcnpjCarteira, byte TipoControle)
    {

        bool isContaOrdem = TipoControle == (byte)Financial.Investidor.Enums.TipoControleCliente.Cotista;

        List<OperacaoCotistaViewModel> listaOperacaoCotistaViewModel = new List<OperacaoCotistaViewModel>();

        if (!CheckAuthentication(Authentication.Username, Authentication.Password))
        {
            string msgErro = "Usu�rio n�o autenticado";
            throw new Exception(msgErro);
        }

        OperacaoCotistaQuery operacaoQuery = new OperacaoCotistaQuery("O");
        ClienteQuery clienteQuery = new ClienteQuery("C");

        if (!string.IsNullOrEmpty(CpfcnpjCotista))
        {
            Pessoa pessoaCotista = BuscaCotistaPorCpfCnpj(CpfcnpjCotista, TipoControle);
            if (pessoaCotista == null)
            {
                throw new Exception("N�o foi poss�vel encontrar o cotista com CpfCnpj: " + CpfcnpjCotista);
            }

            Cotista cotista = new Cotista();
            cotista.Query.Where(cotista.Query.IdPessoa.Equal(pessoaCotista.IdPessoa.Value));
            if (cotista.Query.Load())
            {
                IdCotista = cotista.IdPessoa.Value;
            }
            else
            {
                throw new Exception("N�o foi poss�vel encontrar o cotista com CpfCnpj: " + CpfcnpjCotista);
            }
        }

        if (!string.IsNullOrEmpty(CpfcnpjCarteira))
        {
            Pessoa pessoaCarteira = new PessoaCollection().BuscaPessoaPorCPFCNPJ(CpfcnpjCarteira);
            if (pessoaCarteira == null)
            {
                throw new Exception("N�o foi poss�vel encontrar a carteira com CpfCnpj: " + CpfcnpjCarteira);
            }

            Cliente cliente = new Cliente();
            cliente.Query.Where(cliente.Query.IdPessoa.Equal(pessoaCarteira.IdPessoa.Value));
            if (cliente.Query.Load())
            {
                IdCarteira = cliente.IdCliente.Value;
            }
            else
            {
                throw new Exception("N�o foi poss�vel encontrar a carteira com CpfCnpj: " + CpfcnpjCarteira);
            }
        }

        if (IdOperacao.HasValue)
        {
            if (isContaOrdem)
            {
                //Se for conta e ordem precisamos encontrar operacao pelo IdOrdem
                operacaoQuery.Where(operacaoQuery.IdOrdem.Equal(IdOperacao));
            }
            else
            {
                operacaoQuery.Where(operacaoQuery.IdOperacao.Equal(IdOperacao));
            }
        }

        if (IdCotista.HasValue)
        {
            operacaoQuery.Where(operacaoQuery.IdCotista.Equal(IdCotista));
        }

        if (IdCarteira.HasValue)
        {
            operacaoQuery.Where(operacaoQuery.IdCarteira.Equal(IdCarteira));
        }

        if (DataOperacao.HasValue)
        {
            operacaoQuery.Where(operacaoQuery.DataOperacao.Equal(DataOperacao.Value.Date));
        }

        operacaoQuery.InnerJoin(clienteQuery).On(operacaoQuery.IdCarteira.Equal(clienteQuery.IdCliente));
        operacaoQuery.Where(clienteQuery.TipoControle.Equal(TipoControle));

        OperacaoCotistaCollection operacoes = new OperacaoCotistaCollection();
        operacoes.Load(operacaoQuery);

        foreach (OperacaoCotista operacao in operacoes)
        {
            listaOperacaoCotistaViewModel.Add(new OperacaoCotistaViewModel(operacao));
        }

        if (isContaOrdem)
        {

            OrdemCotistaQuery ordemQuery = new OrdemCotistaQuery("O");
            clienteQuery = new ClienteQuery("C");

            if (!string.IsNullOrEmpty(CpfcnpjCotista))
            {
                Pessoa pessoaCotista = BuscaCotistaPorCpfCnpj(CpfcnpjCotista, TipoControle);
                if (pessoaCotista == null)
                {
                    throw new Exception("N�o foi poss�vel encontrar o cotista com CpfCnpj: " + CpfcnpjCotista);
                }

                Cotista cotista = new Cotista();
                cotista.Query.Where(cotista.Query.IdPessoa.Equal(pessoaCotista.IdPessoa.Value));
                if (cotista.Query.Load())
                {
                    IdCotista = cotista.IdPessoa.Value;
                }
                else
                {
                    throw new Exception("N�o foi poss�vel encontrar o cotista com CpfCnpj: " + CpfcnpjCotista);
                }
            }

            if (!string.IsNullOrEmpty(CpfcnpjCarteira))
            {
                Pessoa pessoaCarteira = new PessoaCollection().BuscaPessoaPorCPFCNPJ(CpfcnpjCarteira);
                if (pessoaCarteira == null)
                {
                    throw new Exception("N�o foi poss�vel encontrar a carteira com CpfCnpj: " + CpfcnpjCarteira);
                }

                Cliente cliente = new Cliente();
                cliente.Query.Where(cliente.Query.IdPessoa.Equal(pessoaCarteira.IdPessoa.Value));
                if (cliente.Query.Load())
                {
                    IdCarteira = cliente.IdCliente.Value;
                }
                else
                {
                    throw new Exception("N�o foi poss�vel encontrar a carteira com CpfCnpj: " + CpfcnpjCarteira);
                }
            }

            if (IdOperacao.HasValue)
            {
                ordemQuery.Where(ordemQuery.IdOperacao.Equal(IdOperacao));
            }

            if (IdCotista.HasValue)
            {
                ordemQuery.Where(ordemQuery.IdCotista.Equal(IdCotista));
            }

            if (IdCarteira.HasValue)
            {
                ordemQuery.Where(ordemQuery.IdCarteira.Equal(IdCarteira));
            }

            if (DataOperacao.HasValue)
            {
                ordemQuery.Where(ordemQuery.DataOperacao.Equal(DataOperacao.Value.Date));
            }

            //Nao buscar nenhuma ordem processada pois ja vieram como operacao
            ordemQuery.Where(ordemQuery.Status.NotEqual((byte)StatusOrdemCotista.Processado));

            ordemQuery.InnerJoin(clienteQuery).On(ordemQuery.IdCarteira.Equal(clienteQuery.IdCliente));
            ordemQuery.Where(clienteQuery.TipoControle.Equal(TipoControle));

            OrdemCotistaCollection ordens = new OrdemCotistaCollection();
            ordens.Load(ordemQuery);

            foreach (OrdemCotista ordem in ordens)
            {
                listaOperacaoCotistaViewModel.Add(new OperacaoCotistaViewModel(ordem));
            }
        }

        return listaOperacaoCotistaViewModel;
    }

    public class ValidateLogin : SoapHeader
    {
        public string Username;
        public string Password;
    }

    private bool CheckAuthentication(string username, string password)
    {
        FinancialMembershipProvider financialMembershipProvider = new FinancialMembershipProvider();
        NameValueCollection listaValores = new NameValueCollection();
        listaValores.Add("passwordFormat", "Encrypted");
        financialMembershipProvider.Initialize(null, listaValores);

        bool autenticado = financialMembershipProvider.ValidateUser(username, password);
        return autenticado;
    }

    public class DetalheResgateCotistaViewModel
    {
        public int NotaAplicacaoOperacao;
        public int NotaAplicacaoPosicao;
        public DateTime DataCotizacao;
        public DateTime DataAplicacao;

        public int IdOperacao;
        public int IdPosicaoResgatada;
        public int IdCotista;
        public int IdCarteira;
        public decimal Quantidade;
        public decimal ValorBruto;
        public decimal ValorLiquido;
        public decimal ValorCPMF;
        public decimal ValorPerformance;
        public decimal PrejuizoUsado;
        public decimal RendimentoResgate;
        public decimal VariacaoResgate;
        public decimal ValorIR;
        public decimal ValorIOF;

        public DetalheResgateCotistaViewModel()
        {
        }

        public DetalheResgateCotistaViewModel(DetalheResgateCotista detalheResgate)
        {
            int idPosicao = detalheResgate.IdPosicaoResgatada.Value;
            int idOperacao = 0;
            this.NotaAplicacaoPosicao = idPosicao;

            DateTime dataConversao = new DateTime();
            DateTime dataAplicacao = new DateTime();

            PosicaoCotista posicaoCotista = new PosicaoCotista();
            if (posicaoCotista.LoadByPrimaryKey(idPosicao))
            {
                dataConversao = posicaoCotista.DataConversao.Value;
                dataAplicacao = posicaoCotista.DataAplicacao.Value;
                idOperacao = posicaoCotista.IdOperacao.GetValueOrDefault(0);
            }
            else
            {
                PosicaoCotistaHistoricoCollection posicaoCotistaHistoricoColl = new PosicaoCotistaHistoricoCollection();
                posicaoCotistaHistoricoColl.Query.Where(posicaoCotistaHistoricoColl.Query.IdPosicao.Equal(idPosicao));

                if (posicaoCotistaHistoricoColl.Query.Load())
                {
                    dataConversao = posicaoCotistaHistoricoColl[0].DataConversao.Value;
                    dataAplicacao = posicaoCotistaHistoricoColl[0].DataAplicacao.Value;
                    idOperacao = posicaoCotistaHistoricoColl[0].IdOperacao.GetValueOrDefault(0);
                }
            }

            this.DataAplicacao = dataAplicacao;
            this.DataCotizacao = dataConversao;
            this.NotaAplicacaoOperacao = idOperacao;
            this.NotaAplicacaoPosicao = idPosicao;

            this.IdOperacao = detalheResgate.IdOperacao.Value;
            this.IdPosicaoResgatada = detalheResgate.IdPosicaoResgatada.Value;
            this.IdCotista = detalheResgate.IdCotista.Value;
            this.IdCarteira = detalheResgate.IdCarteira.Value;
            this.Quantidade = detalheResgate.Quantidade.Value;
            this.ValorBruto = detalheResgate.ValorBruto.Value;
            this.ValorLiquido = detalheResgate.ValorLiquido.Value;
            this.ValorCPMF = detalheResgate.ValorCPMF.Value;
            this.ValorPerformance = detalheResgate.ValorPerformance.Value;
            this.PrejuizoUsado = detalheResgate.PrejuizoUsado.Value;
            this.RendimentoResgate = detalheResgate.RendimentoResgate.Value;
            this.VariacaoResgate = detalheResgate.VariacaoResgate.Value;
            this.ValorIR = detalheResgate.ValorIR.Value;
            this.ValorIOF = detalheResgate.ValorIOF.Value;

        }
    }

    public class OperacaoCotistaViewModel
    {
        public int IdOperacao;
        public int IdCotista;
        public int IdCarteira;
        public DateTime DataOperacao;
        public DateTime DataConversao;
        public DateTime DataLiquidacao;
        public DateTime DataAgendamento;
        public byte TipoOperacao;
        public string TipoOperacaoString;
        public byte? TipoResgate;
        public string TipoResgateString;
        public int? IdPosicaoResgatada;
        public byte IdFormaLiquidacao;
        public string FormaLiquidacao;
        public decimal Quantidade;
        public decimal CotaOperacao;
        public decimal ValorBruto;
        public decimal ValorLiquido;
        public decimal ValorIR;
        public decimal ValorIOF;
        public decimal ValorCPMF;
        public decimal ValorPerformance;
        public decimal PrejuizoUsado;
        public decimal RendimentoResgate;
        public decimal VariacaoResgate;
        public string Observacao;
        public string DadosBancarios;
        public byte Fonte;
        public byte? Status;
        public int? IdConta;
        public decimal? CotaInformada;
        public int? IdAgenda;
        public int? IdOperacaoResgatada;
        public int? IdOperacaoProcessada;
        
        public List<DetalheResgateCotistaViewModel> lstDetalheCotista;

        public string CpfCnpjCotista;
        public string CpfCnpjCarteira;
        public string CodigoCarteira;
        public string NomeCarteira;

        public OperacaoCotistaViewModel()
        {
        }

        public OperacaoCotistaViewModel(OperacaoCotista operacaoCotista)
        {

            this.CotaInformada = operacaoCotista.CotaInformada;
            this.CotaOperacao = operacaoCotista.CotaOperacao.Value;
            this.DadosBancarios = operacaoCotista.DadosBancarios;
            this.DataAgendamento = operacaoCotista.DataAgendamento.Value;
            this.DataConversao = operacaoCotista.DataConversao.Value;
            this.DataLiquidacao = operacaoCotista.DataLiquidacao.Value;
            this.DataOperacao = operacaoCotista.DataOperacao.Value;
            this.Fonte = operacaoCotista.Fonte.Value;
            this.IdAgenda = operacaoCotista.IdAgenda;
            this.IdCarteira = operacaoCotista.IdCarteira.Value;
            this.IdConta = operacaoCotista.IdConta;
            this.IdCotista = operacaoCotista.IdCotista.Value;
            this.IdFormaLiquidacao = operacaoCotista.IdFormaLiquidacao.Value;
            //Se for uma operacao derivada de uma ordem, sempre referenciar o Id da Ordem que a originou
            this.IdOperacao = operacaoCotista.IdOrdem.HasValue ? operacaoCotista.IdOrdem.Value : operacaoCotista.IdOperacao.Value;
            this.IdOperacaoProcessada = operacaoCotista.IdOperacao.Value;
            this.IdOperacaoResgatada = operacaoCotista.IdOperacaoResgatada;
            this.IdPosicaoResgatada = operacaoCotista.IdPosicaoResgatada;
            this.Observacao = operacaoCotista.Observacao;
            this.PrejuizoUsado = operacaoCotista.PrejuizoUsado.Value;
            this.Quantidade = operacaoCotista.Quantidade.Value;
            this.RendimentoResgate = operacaoCotista.RendimentoResgate.Value;
            this.TipoOperacao = operacaoCotista.TipoOperacao.Value;
            this.TipoResgate = operacaoCotista.TipoResgate;
            this.ValorBruto = operacaoCotista.ValorBruto.Value;
            this.ValorCPMF = operacaoCotista.ValorCPMF.Value;
            this.ValorIOF = operacaoCotista.ValorIOF.Value;
            this.ValorIR = operacaoCotista.ValorIR.Value;
            this.ValorLiquido = operacaoCotista.ValorLiquido.Value;
            this.ValorPerformance = operacaoCotista.ValorPerformance.Value;
            this.VariacaoResgate = operacaoCotista.VariacaoResgate.Value;

            if (this.TipoOperacao > 0)
            {
                TipoOperacaoCotista tipoOperacaoCotista = (TipoOperacaoCotista)Enum.Parse(typeof(TipoOperacaoCotista), this.TipoOperacao.ToString());
                this.TipoOperacaoString = Financial.Util.StringEnum.GetStringValue(tipoOperacaoCotista);
            }
            else
            {
                this.TipoOperacaoString = "";
            }

            if (this.TipoResgate.HasValue)
            {
                TipoResgateCotista tipoResgateCotista = (TipoResgateCotista)Enum.Parse(typeof(TipoResgateCotista), this.TipoResgate.ToString());
                this.TipoResgateString = Financial.Util.StringEnum.GetStringValue(tipoResgateCotista);
            }

            Financial.ContaCorrente.FormaLiquidacao formaLiquidacao = new Financial.ContaCorrente.FormaLiquidacao();
            formaLiquidacao.LoadByPrimaryKey(this.IdFormaLiquidacao);
            this.FormaLiquidacao = formaLiquidacao.Descricao;

            Cotista cotista = new Cotista();
            cotista.LoadByPrimaryKey(this.IdCotista);
            
            Pessoa pessoaCotista = new Pessoa();
            pessoaCotista.LoadByPrimaryKey(cotista.IdPessoa.Value);
            this.CpfCnpjCotista = Regex.Replace(pessoaCotista.Cpfcnpj, "[^0-9]", "");

            Cliente cliente = new Cliente();
            cliente.LoadByPrimaryKey(this.IdCarteira);
            
            Pessoa pessoaCarteira = new Pessoa();
            pessoaCarteira.LoadByPrimaryKey(cliente.IdPessoa.Value);
            this.CpfCnpjCarteira = Regex.Replace(pessoaCarteira.Cpfcnpj, "[^0-9]", "");

            ClienteInterface clienteInterface = new ClienteInterface();
            bool loadedClienteInterface = clienteInterface.LoadByPrimaryKey(this.IdCarteira);

            this.CodigoCarteira = loadedClienteInterface && !String.IsNullOrEmpty(clienteInterface.CodigoYMF) ?
                clienteInterface.CodigoYMF : this.IdCarteira.ToString();

            Carteira carteira = new Carteira();
            carteira.LoadByPrimaryKey(this.IdCarteira);
            this.NomeCarteira = carteira.Nome;

            this.Status = (byte)StatusOrdemCotista.Processado;

            if (operacaoCotista.TipoResgate.HasValue)
            {
                
                if (operacaoCotista.TipoOperacao == (byte) TipoOperacaoCotista.ComeCotas)
                {
                    
                    DetalheResgateCotista operacaoComeCotas = new DetalheResgateCotista();

                    operacaoComeCotas.IdOperacao = operacaoCotista.IdOperacao;
                    operacaoComeCotas.IdPosicaoResgatada = operacaoCotista.IdPosicaoResgatada;
                    operacaoComeCotas.IdCotista = operacaoCotista.IdCotista;
                    operacaoComeCotas.IdCarteira = operacaoCotista.IdCarteira;
                    operacaoComeCotas.Quantidade = operacaoCotista.Quantidade;
                    operacaoComeCotas.ValorBruto = operacaoCotista.ValorBruto;
                    operacaoComeCotas.ValorLiquido = operacaoCotista.ValorLiquido;
                    operacaoComeCotas.ValorCPMF = operacaoCotista.ValorCPMF;
                    operacaoComeCotas.ValorPerformance = operacaoCotista.ValorPerformance;
                    operacaoComeCotas.PrejuizoUsado = operacaoCotista.PrejuizoUsado;
                    operacaoComeCotas.RendimentoResgate = operacaoCotista.RendimentoResgate;
                    operacaoComeCotas.VariacaoResgate = operacaoCotista.VariacaoResgate;
                    operacaoComeCotas.ValorIR = operacaoCotista.ValorIR;
                    operacaoComeCotas.ValorIOF = operacaoCotista.ValorIOF;
                    lstDetalheCotista = new List<DetalheResgateCotistaViewModel>();
                    lstDetalheCotista.Add(new DetalheResgateCotistaViewModel(operacaoComeCotas));  
                    
                }
                else
                {
                

                    DetalheResgateCotistaCollection detalheCotistaColl = new DetalheResgateCotistaCollection();
                    detalheCotistaColl.Query.Where(detalheCotistaColl.Query.IdOperacao.Equal(operacaoCotista.IdOperacao.Value));
                    detalheCotistaColl.Query.Load();
                    lstDetalheCotista = new List<DetalheResgateCotistaViewModel>();
                    foreach (DetalheResgateCotista detalhe in detalheCotistaColl)
                        lstDetalheCotista.Add(new DetalheResgateCotistaViewModel(detalhe));
                }
                
            }
        }

        public OperacaoCotistaViewModel(OrdemCotista ordemCotista)
        {
            this.DataAgendamento = ordemCotista.DataAgendamento.Value;
            this.DataConversao = ordemCotista.DataConversao.Value;
            this.DataLiquidacao = ordemCotista.DataLiquidacao.Value;
            this.DataOperacao = ordemCotista.DataOperacao.Value;
            this.IdCarteira = ordemCotista.IdCarteira.Value;
            this.IdConta = ordemCotista.IdConta;
            this.IdCotista = ordemCotista.IdCotista.Value;
            this.IdFormaLiquidacao = ordemCotista.IdFormaLiquidacao.Value;
            this.IdOperacao = ordemCotista.IdOperacao.Value;
            this.IdOperacaoResgatada = ordemCotista.IdOperacaoResgatada;
            this.IdPosicaoResgatada = ordemCotista.IdPosicaoResgatada;
            this.Observacao = ordemCotista.Observacao;
            this.Quantidade = ordemCotista.Quantidade.Value;
            this.TipoOperacao = ordemCotista.TipoOperacao.Value;
            this.TipoResgate = ordemCotista.TipoResgate;
            this.ValorBruto = ordemCotista.ValorBruto.Value;
            this.ValorLiquido = ordemCotista.ValorLiquido.Value;

            if (this.TipoOperacao > 0)
            {
                TipoOperacaoCotista tipoOperacaoCotista = (TipoOperacaoCotista)Enum.Parse(typeof(TipoOperacaoCotista), this.TipoOperacao.ToString());
                this.TipoOperacaoString = Financial.Util.StringEnum.GetStringValue(tipoOperacaoCotista);
            }
            else
            {
                this.TipoOperacaoString = "";
            }

            if (this.TipoResgate.HasValue)
            {
                TipoResgateCotista tipoResgateCotista = (TipoResgateCotista)Enum.Parse(typeof(TipoResgateCotista), this.TipoResgate.ToString());
                this.TipoResgateString = Financial.Util.StringEnum.GetStringValue(tipoResgateCotista);
            }

            Financial.ContaCorrente.FormaLiquidacao formaLiquidacao = new Financial.ContaCorrente.FormaLiquidacao();
            formaLiquidacao.LoadByPrimaryKey(this.IdFormaLiquidacao);
            this.FormaLiquidacao = formaLiquidacao.Descricao;

            Cotista cotista = new Cotista();
            cotista.LoadByPrimaryKey(this.IdCotista);
            
            Pessoa pessoaCotista = new Pessoa();
            pessoaCotista.LoadByPrimaryKey(cotista.IdPessoa.Value);
            this.CpfCnpjCotista = Regex.Replace(pessoaCotista.Cpfcnpj, "[^0-9]", "");

            Cliente cliente = new Cliente();
            cliente.LoadByPrimaryKey(this.IdCarteira);
            
            Pessoa pessoaCarteira = new Pessoa();
            pessoaCarteira.LoadByPrimaryKey(cliente.IdPessoa.Value);
            this.CpfCnpjCarteira = Regex.Replace(pessoaCarteira.Cpfcnpj, "[^0-9]", "");

            ClienteInterface clienteInterface = new ClienteInterface();
            bool loadedClienteInterface = clienteInterface.LoadByPrimaryKey(this.IdCarteira);

            this.CodigoCarteira = loadedClienteInterface && !String.IsNullOrEmpty(clienteInterface.CodigoYMF) ?
                clienteInterface.CodigoYMF : this.IdCarteira.ToString();

            Carteira carteira = new Carteira();
            carteira.LoadByPrimaryKey(this.IdCarteira);
            this.NomeCarteira = carteira.Nome;

            this.Status = ordemCotista.Status;
        }
    }

    private Pessoa BuscaCotistaPorCpfCnpj(string cpfcnpj, byte? tipoControle)
    {

        Pessoa pessoaEncontrada = null;

        PessoaCollection pessoas = new PessoaCollection();
        pessoas.Query.Where(pessoas.Query.Cpfcnpj == cpfcnpj);

        pessoas.Load(pessoas.Query);

        if (pessoas.Count == 0)
        {
            pessoaEncontrada = null;
        }
        else if (pessoas.Count == 1 || !tipoControle.HasValue)
        {
            pessoaEncontrada = pessoas[0];
        }
        else
        {
            foreach (Pessoa pessoa in pessoas)
            {

                if (tipoControle == (byte)Financial.Investidor.Enums.TipoControleCliente.Cotista)
                {
                    //Se tiver cliente, precisamos ignorar pois soh queremos cotistas
                    Cliente cliente = new Cliente();
                    cliente.Query.Where(cliente.Query.IdPessoa.Equal(pessoa.IdPessoa.Value));
                    cliente.Query.es.Top = 1;
                    cliente.Query.OrderBy(cliente.Query.IdPessoa.Descending);
                    if (cliente.Query.Load())
                    {
                        continue;
                    }

                    Cotista cotista = new Cotista();
                    cotista.Query.Where(cotista.Query.IdPessoa.Equal(pessoa.IdPessoa.Value));
                    cotista.Query.es.Top = 1;
                    cotista.Query.OrderBy(cotista.Query.IdCotista.Descending);
                    if (cotista.Query.Load() && cotista.IsAtivo)
                    {
                        pessoaEncontrada = pessoa;
                        break;
                    }
                }
                else
                {
                    Cliente cliente = new Cliente();
                    cliente.Query.Where(cliente.Query.IdPessoa.Equal(pessoa.IdPessoa));
                    cliente.Query.es.Top = 1;
                    cliente.Query.OrderBy(cliente.Query.IdPessoa.Descending);
                    if (cliente.Query.Load() && cliente.IsAtivo)
                    {
                        pessoaEncontrada = pessoa;
                        break;
                    }
                }
            }
        }

        return pessoaEncontrada;
    }

}

