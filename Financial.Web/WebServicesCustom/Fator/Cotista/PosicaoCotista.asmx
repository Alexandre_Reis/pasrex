<%@ WebService Language="C#" Class="PosicaoCotistaWSFator" %>

using System;
using System.Web;
using System.Web.Services;
using System.Web.Services.Protocols;
using System.Collections.Specialized;
using System.Collections.Generic;
using System.Text.RegularExpressions;

using Financial.InvestidorCotista;
using Financial.Security;
using Financial.Investidor;
using Financial.CRM;
using Financial.Fundo;

[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
public class PosicaoCotistaWSFator : System.Web.Services.WebService
{

    public ValidateLogin Authentication;

    [SoapHeader("Authentication")]
    [WebMethod]
    public List<PosicaoCotistaViewModel> Exporta(int? IdPosicao, int? IdCotista, int? IdCarteira,
        DateTime? DataHistorico, string CpfcnpjCotista, string CpfcnpjCarteira, byte? TipoControle)
    {

        string debugExporta = "MetodoExporta:";
        debugExporta += "IdPosicao: " + IdPosicao;
        debugExporta += ";IdCotista: " + IdCotista;
        debugExporta += ";IdCarteira: " + IdCarteira;
        debugExporta += ";DataHistorico: " + DataHistorico;
        debugExporta += ";CpfcnpjCotista: " + CpfcnpjCotista;
        debugExporta += ";CpfcnpjCarteira: " + CpfcnpjCarteira;
        debugExporta += ";TipoControle: " + TipoControle;


        if (!CheckAuthentication(Authentication.Username, Authentication.Password))
        {
            string msgErro = "Usu�rio n�o autenticado";
            throw new Exception(msgErro);
        }

        List<PosicaoCotistaViewModel> listaPosicaoCotistaViewModel = new List<PosicaoCotistaViewModel>();

        if (!string.IsNullOrEmpty(CpfcnpjCotista))
        {
            Pessoa pessoaCotista = BuscaCotistaPorCpfCnpj(CpfcnpjCotista);
            if (pessoaCotista == null)
            {
                throw new Exception("N�o foi poss�vel encontrar o cotista com CpfCnpj: " + CpfcnpjCotista);
            }

            IdCotista = pessoaCotista.IdPessoa.Value;
            debugExporta += ";IdCotista encontrado: " + IdCotista;
        }

        if (!string.IsNullOrEmpty(CpfcnpjCarteira))
        {
            Pessoa pessoaCarteira = new PessoaCollection().BuscaPessoaPorCPFCNPJ(CpfcnpjCarteira);
            if (pessoaCarteira == null)
            {
                throw new Exception("N�o foi poss�vel encontrar a carteira com CpfCnpj: " + CpfcnpjCarteira);
            }

            IdCarteira = pessoaCarteira.IdPessoa.Value;
            debugExporta += ";IdCarteira encontrado: " + IdCarteira;
        }

        //Se nao for informada uma data de posicao, vamos utilizar a posicao mais recente
        if (!DataHistorico.HasValue)
        {
            PosicaoCotistaCollection posicoes = new PosicaoCotistaCollection();
            PosicaoCotistaQuery posicaoQuery = new PosicaoCotistaQuery("P");
            ClienteQuery clienteQuery = new ClienteQuery("C");

            if (IdPosicao.HasValue)
            {
                posicaoQuery.Where(posicaoQuery.IdPosicao.Equal(IdPosicao.Value));
            }

            if (IdCotista.HasValue)
            {
                posicaoQuery.Where(posicaoQuery.IdCotista.Equal(IdCotista.Value));
            }

            if (IdCarteira.HasValue)
            {
                posicaoQuery.Where(posicaoQuery.IdCarteira.Equal(IdCarteira.Value));
            }

            if (TipoControle.HasValue)
            {
                posicaoQuery.InnerJoin(clienteQuery).On(posicaoQuery.IdCarteira.Equal(clienteQuery.IdCliente));
                posicaoQuery.Where(clienteQuery.TipoControle.Equal(TipoControle.Value));
            }

            posicaoQuery.Where(posicaoQuery.Quantidade.GreaterThan(0));

            posicoes.Load(posicaoQuery);
            debugExporta += ";PosicoesSemHIstoricoEncontradas:" + posicoes.Count;

            foreach (PosicaoCotista posicao in posicoes)
            {
                listaPosicaoCotistaViewModel.Add(new PosicaoCotistaViewModel(posicao));
            }
        }
        else
        {
            //Foi passada uma data e nenhuma carteira... Entao precisamos encontrar a data de cada uma dessas carteiras
            PosicaoCotistaHistoricoCollection posicoes = new PosicaoCotistaHistoricoCollection();
            PosicaoCotistaHistoricoQuery posicaoQuery = new PosicaoCotistaHistoricoQuery("P");
            ClienteQuery clienteQuery = new ClienteQuery("C");

            if (IdPosicao.HasValue)
            {
                posicaoQuery.Where(posicaoQuery.IdPosicao.Equal(IdPosicao.Value));
            }

            if (IdCotista.HasValue)
            {
                posicaoQuery.Where(posicaoQuery.IdCotista.Equal(IdCotista.Value));
            }

            if (IdCarteira.HasValue)
            {
                posicaoQuery.Where(posicaoQuery.IdCarteira.Equal(IdCarteira.Value));
            }

            if (TipoControle.HasValue)
            {
                posicaoQuery.InnerJoin(clienteQuery).On(posicaoQuery.IdCarteira.Equal(clienteQuery.IdCliente));
                posicaoQuery.Where(clienteQuery.TipoControle.Equal(TipoControle.Value));
            }

            posicaoQuery.Where(posicaoQuery.DataHistorico.Equal(DataHistorico));

            posicoes.Load(posicaoQuery);
            debugExporta += ";PosicoesComHistEncontradas:" + posicoes.Count;


            foreach (PosicaoCotistaHistorico posicao in posicoes)
            {
                listaPosicaoCotistaViewModel.Add(new PosicaoCotistaViewModel(posicao));
            }
        }

        debugExporta += ";ListViewModelCount:" + listaPosicaoCotistaViewModel.Count;

        HistoricoLog historicoLog = new HistoricoLog();
        historicoLog.Descricao = debugExporta;
        historicoLog.Login = HttpContext.Current.User.Identity.Name;
        historicoLog.Maquina = Financial.Util.Utilitario.GetLocalIp();
        historicoLog.Cultura = "";
        historicoLog.DataInicio = DateTime.Now;
        historicoLog.DataFim = DateTime.Now;
        historicoLog.Origem = (int)Financial.Security.Enums.HistoricoLogOrigem.Outros;
        historicoLog.Save();

        return listaPosicaoCotistaViewModel;
    }

    public class PosicaoCotistaViewModel
    {
        public int IdPosicao;
        public int? IdOperacao;
        public int IdCotista;
        public int IdCarteira;
        public decimal ValorAplicacao;
        public DateTime DataHistorico;
        public DateTime DataAplicacao;
        public DateTime DataConversao;
        public decimal CotaAplicacao;
        public decimal CotaDia;
        public decimal ValorBruto;
        public decimal ValorLiquido;
        public decimal QuantidadeInicial;
        public decimal Quantidade;
        public decimal QuantidadeBloqueada;
        public DateTime DataUltimaCobrancaIR;
        public decimal ValorIR;
        public decimal ValorIOF;
        public decimal ValorPerformance;
        public decimal ValorIOFVirtual;
        public decimal QuantidadeAntesCortes;
        public decimal ValorRendimento;
        public DateTime? DataUltimoCortePfee;
        public string PosicaoIncorporada;
        public string CpfCnpjCotista;
        public string CpfCnpjCarteira;
        public string CodigoCarteira;
        public string NomeCarteira;

        public PosicaoCotistaViewModel()
        {
        }

        public PosicaoCotistaViewModel(PosicaoCotista posicaoCotista)
        {
            this.CotaAplicacao = posicaoCotista.CotaAplicacao.Value;
            this.CotaDia = posicaoCotista.CotaDia.Value;
            this.DataAplicacao = posicaoCotista.DataAplicacao.Value;
            this.DataConversao = posicaoCotista.DataConversao.Value;
            this.DataUltimaCobrancaIR = posicaoCotista.DataUltimaCobrancaIR.Value;
            this.DataUltimoCortePfee = posicaoCotista.DataUltimoCortePfee;
            this.IdCarteira = posicaoCotista.IdCarteira.Value;
            this.IdCotista = posicaoCotista.IdCotista.Value;
            this.IdOperacao = posicaoCotista.IdOperacao;
            this.IdPosicao = posicaoCotista.IdPosicao.Value;
            this.PosicaoIncorporada = posicaoCotista.PosicaoIncorporada;
            this.Quantidade = posicaoCotista.Quantidade.Value;
            this.QuantidadeAntesCortes = posicaoCotista.QuantidadeAntesCortes.Value;
            this.QuantidadeBloqueada = posicaoCotista.QuantidadeBloqueada.Value;
            this.QuantidadeInicial = posicaoCotista.QuantidadeInicial.Value;
            this.ValorAplicacao = posicaoCotista.ValorAplicacao.Value;
            this.ValorBruto = posicaoCotista.ValorBruto.Value;
            this.ValorIOF = posicaoCotista.ValorIOF.Value;
            this.ValorIOFVirtual = posicaoCotista.ValorIOFVirtual.Value;
            this.ValorIR = posicaoCotista.ValorIR.Value;
            this.ValorLiquido = posicaoCotista.ValorLiquido.Value;
            this.ValorPerformance = posicaoCotista.ValorPerformance.Value;
            this.ValorRendimento = posicaoCotista.ValorRendimento.Value;

            Pessoa pessoaCotista = new Pessoa();
            pessoaCotista.LoadByPrimaryKey(this.IdCotista);
            this.CpfCnpjCotista = Regex.Replace(pessoaCotista.Cpfcnpj, "[^0-9]", "");

            Pessoa pessoaCarteira = new Pessoa();
            pessoaCarteira.LoadByPrimaryKey(this.IdCarteira);
            this.CpfCnpjCarteira = Regex.Replace(pessoaCarteira.Cpfcnpj, "[^0-9]", "");

            ClienteInterface clienteInterface = new ClienteInterface();
            bool loadedClienteInterface = clienteInterface.LoadByPrimaryKey(this.IdCarteira);

            this.CodigoCarteira = loadedClienteInterface && !String.IsNullOrEmpty(clienteInterface.CodigoYMF) ?
                clienteInterface.CodigoYMF : this.IdCarteira.ToString();

            Carteira carteira = new Carteira();
            carteira.LoadByPrimaryKey(this.IdCarteira);
            this.NomeCarteira = carteira.Nome;

            //LINHAS DIFERENTES PARA HISTORICO:
            Cliente cliente = new Cliente();
            cliente.LoadByPrimaryKey(posicaoCotista.IdCarteira.Value);
            this.DataHistorico = cliente.DataDia.Value;
        }

        public PosicaoCotistaViewModel(PosicaoCotistaHistorico posicaoCotista)
        {
            this.CotaAplicacao = posicaoCotista.CotaAplicacao.Value;
            this.CotaDia = posicaoCotista.CotaDia.Value;
            this.DataAplicacao = posicaoCotista.DataAplicacao.Value;
            this.DataConversao = posicaoCotista.DataConversao.Value;
            this.DataUltimaCobrancaIR = posicaoCotista.DataUltimaCobrancaIR.Value;
            this.DataUltimoCortePfee = posicaoCotista.DataUltimoCortePfee;
            this.IdCarteira = posicaoCotista.IdCarteira.Value;
            this.IdCotista = posicaoCotista.IdCotista.Value;
            this.IdOperacao = posicaoCotista.IdOperacao;
            this.IdPosicao = posicaoCotista.IdPosicao.Value;
            this.PosicaoIncorporada = posicaoCotista.PosicaoIncorporada;
            this.Quantidade = posicaoCotista.Quantidade.Value;
            this.QuantidadeAntesCortes = posicaoCotista.QuantidadeAntesCortes.Value;
            this.QuantidadeBloqueada = posicaoCotista.QuantidadeBloqueada.Value;
            this.QuantidadeInicial = posicaoCotista.QuantidadeInicial.Value;
            this.ValorAplicacao = posicaoCotista.ValorAplicacao.Value;
            this.ValorBruto = posicaoCotista.ValorBruto.Value;
            this.ValorIOF = posicaoCotista.ValorIOF.Value;
            this.ValorIOFVirtual = posicaoCotista.ValorIOFVirtual.Value;
            this.ValorIR = posicaoCotista.ValorIR.Value;
            this.ValorLiquido = posicaoCotista.ValorLiquido.Value;
            this.ValorPerformance = posicaoCotista.ValorPerformance.Value;
            this.ValorRendimento = posicaoCotista.ValorRendimento.Value;

            Pessoa pessoaCotista = new Pessoa();
            pessoaCotista.LoadByPrimaryKey(this.IdCotista);
            this.CpfCnpjCotista = Regex.Replace(pessoaCotista.Cpfcnpj, "[^0-9]", "");

            Pessoa pessoaCarteira = new Pessoa();
            pessoaCarteira.LoadByPrimaryKey(this.IdCarteira);
            this.CpfCnpjCarteira = Regex.Replace(pessoaCarteira.Cpfcnpj, "[^0-9]", "");

            ClienteInterface clienteInterface = new ClienteInterface();
            bool loadedClienteInterface = clienteInterface.LoadByPrimaryKey(this.IdCarteira);

            this.CodigoCarteira = loadedClienteInterface && !String.IsNullOrEmpty(clienteInterface.CodigoYMF) ?
                clienteInterface.CodigoYMF : this.IdCarteira.ToString();

            Carteira carteira = new Carteira();
            carteira.LoadByPrimaryKey(this.IdCarteira);
            this.NomeCarteira = carteira.Nome;

            //LINHAS DIFERENTES PARA HISTORICO:
            this.DataHistorico = posicaoCotista.DataHistorico.Value;
        }
    }

    public class ValidateLogin : SoapHeader
    {
        public string Username;
        public string Password;
    }

    private bool CheckAuthentication(string username, string password)
    {
        FinancialMembershipProvider financialMembershipProvider = new FinancialMembershipProvider();
        NameValueCollection listaValores = new NameValueCollection();
        listaValores.Add("passwordFormat", "Encrypted");
        financialMembershipProvider.Initialize(null, listaValores);

        bool autenticado = financialMembershipProvider.ValidateUser(username, password);
        return autenticado;
    }

    private Pessoa BuscaCotistaPorCpfCnpj(string cpfcnpj)
    {

        string debugExporta = "BuscaCotistaPorCpfCnpj";

        Pessoa pessoaEncontrada = null;

        PessoaCollection pessoas = new PessoaCollection();
        pessoas.Query.Where(pessoas.Query.Cpfcnpj == cpfcnpj);

        pessoas.Load(pessoas.Query);

        debugExporta += ";cpfcnpj: " + cpfcnpj;
        debugExporta += ";pessoas encontradas: " + pessoas.Count;

        if (pessoas.Count == 0)
        {
            pessoaEncontrada = null;
        }
        else if (pessoas.Count == 1)
        {
            debugExporta += ";count = 1";
            pessoaEncontrada = pessoas[0];
        }
        else
        {
            foreach (Pessoa pessoa in pessoas)
            {
                debugExporta += ";analisando pessoa:" + pessoa.IdPessoa.Value;

                debugExporta += ";if controle cotista";
                //Se tiver cliente, precisamos ignorar pois soh queremos cotistas
                Cliente cliente = new Cliente();
                cliente.Query.Where(cliente.Query.IdPessoa.Equal(pessoa.IdPessoa.Value));
                if (cliente.Query.Load())
                {
                    debugExporta += ";tinha cliente, continuei";
                    continue;
                }

                debugExporta += ";nao tinha cliente, procurar cotista";

                Cotista cotista = new Cotista();
                cotista.Query.Where(cotista.Query.IdPessoa.Equal(pessoa.IdPessoa.Value));
                if (cotista.Query.Load() && cotista.IsAtivo)
                {
                    debugExporta += ";encontrei cotista ativo, break";
                    pessoaEncontrada = pessoa;
                    break;
                }


            }
        }

        if (pessoaEncontrada == null)
        {
            debugExporta += ";pessoa nao encontrada";
        }
        else
        {
            debugExporta += ";pessoa encontrada:" + pessoaEncontrada.IdPessoa;
        }

        HistoricoLog historicoLog = new HistoricoLog();
        historicoLog.Descricao = debugExporta;
        historicoLog.Login = HttpContext.Current.User.Identity.Name;
        historicoLog.Maquina = Financial.Util.Utilitario.GetLocalIp();
        historicoLog.Cultura = "";
        historicoLog.DataInicio = DateTime.Now;
        historicoLog.DataFim = DateTime.Now;
        historicoLog.Origem = (int)Financial.Security.Enums.HistoricoLogOrigem.Outros;
        historicoLog.Save();

        return pessoaEncontrada;
    }

}

