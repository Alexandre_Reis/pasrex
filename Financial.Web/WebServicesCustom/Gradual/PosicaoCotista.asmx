<%@ WebService Language="C#" Class="PosicaoCotistaWSGradual" %>

using System;
using System.Web;
using System.Web.Services;
using System.Web.Services.Protocols;
using Financial.InvestidorCotista;
using Financial.Security;
using System.Collections.Specialized;
using System.Collections.Generic;
using Financial.Fundo;

[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
public class PosicaoCotistaWSGradual : System.Web.Services.WebService
{

    public ValidateLogin Authentication;
    [SoapHeader("Authentication")]
    [WebMethod]
    public List<PosicaoCotistaViewModel> Exporta(int? IdPosicao, int? IdCotista, int? IdCarteira)
    {
        if (!CheckAuthentication(Authentication.Username, Authentication.Password))
        {
            string msgErro = "Usu�rio n�o autenticado";
            throw new Exception(msgErro);
        }

        List<PosicaoCotistaViewModel> listaRetorno = new List<PosicaoCotistaViewModel>();
        
        PosicaoCotistaCollection posicoes = new PosicaoCotistaCollection();
        if (IdPosicao.HasValue)
        {
            posicoes.Query.Where(posicoes.Query.IdPosicao.Equal(IdPosicao.Value));
        }

        if (IdCotista.HasValue)
        {
            posicoes.Query.Where(posicoes.Query.IdCotista.Equal(IdCotista.Value));
        }

        if (IdCarteira.HasValue)
        {
            posicoes.Query.Where(posicoes.Query.IdCarteira.Equal(IdCarteira.Value));
        }

        if (posicoes.Load(posicoes.Query))
        {
            foreach (PosicaoCotista pc in posicoes)
            {
                PosicaoCotistaViewModel p = new PosicaoCotistaViewModel(pc);

                listaRetorno.Add(p);
            }
        }     
                
        return listaRetorno;
    }

    [SoapHeader("Authentication")]
    [WebMethod]
    public List<PosicaoCotistaViewModel> ExportaPorDataAplicacao(DateTime DataInicial, DateTime DataFinal)
    {
        if (!CheckAuthentication(Authentication.Username, Authentication.Password))
        {
            string msgErro = "Usu�rio n�o autenticado";
            throw new Exception(msgErro);
        }

        List<PosicaoCotistaViewModel> listaRetorno = new List<PosicaoCotistaViewModel>();

        PosicaoCotistaCollection posicoes = new PosicaoCotistaCollection();
        posicoes.Query.Where(posicoes.Query.DataAplicacao.GreaterThanOrEqual(DataInicial.Date),
            posicoes.Query.DataAplicacao.LessThanOrEqual(DataFinal.Date));
        
        if (posicoes.Load(posicoes.Query))
        {
            foreach (PosicaoCotista pc in posicoes)
            {
                PosicaoCotistaViewModel p = new PosicaoCotistaViewModel(pc);

                listaRetorno.Add(p);
            }
        }

        return listaRetorno;
    }

    public class PosicaoCotistaViewModel
    {
        private int idPosicao;
        private int? idOperacao;
        private int idCotista;
        private int idCarteira;
        private decimal valorAplicacao;
        private DateTime dataAplicacao;
        private DateTime dataConversao;
        private decimal cotaAplicacao;
        private decimal cotaDia;
        private decimal valorBruto;
        private decimal valorLiquido;
        private decimal quantidadeInicial;
        private decimal quantidade;
        private decimal quantidadeBloqueada;
        private DateTime dataUltimaCobrancaIR;
        private decimal valorIR;
        private decimal valorIOF;
        private decimal valorPerformance;
        private decimal valorIOFVirtual;
        private decimal quantidadeAntesCortes;
        private decimal valorRendimento;
        private DateTime? dataUltimoCortePfee;
        private string posicaoIncorporada;
        private string codigoAnbima;

        public int IdPosicao
        {
            get { return idPosicao; }
            set { idPosicao = value; }
        }

        public int? IdOperacao
        {
            get { return idOperacao; }
            set { idOperacao = value; }
        }
        public int IdCotista
        {
            get { return idCotista; }
            set { idCotista = value; }
        }
        public int IdCarteira
        {
            get { return idCarteira; }
            set { idCarteira = value; }
        }
        public decimal ValorAplicacao
        {
            get { return valorAplicacao; }
            set { valorAplicacao = value; }
        }
        public DateTime DataAplicacao
        {
            get { return dataAplicacao; }
            set { dataAplicacao = value; }
        }
        public DateTime DataConversao
        {
            get { return dataConversao; }
            set { dataConversao = value; }
        }
        public decimal CotaAplicacao
        {
            get { return cotaAplicacao; }
            set { cotaAplicacao = value; }
        }
        public decimal CotaDia
        {
            get { return cotaDia; }
            set { cotaDia = value; }
        }
        public decimal ValorBruto
        {
            get { return valorBruto; }
            set { valorBruto = value; }
        }
        public decimal ValorLiquido
        {
            get { return valorLiquido; }
            set { valorLiquido = value; }
        }
        public decimal QuantidadeInicial
        {
            get { return quantidadeInicial; }
            set { quantidadeInicial = value; }
        }
        public decimal Quantidade
        {
            get { return quantidade; }
            set { quantidade = value; }
        }
        public decimal QuantidadeBloqueada
        {
            get { return quantidadeBloqueada; }
            set { quantidadeBloqueada = value; }
        }
        public DateTime DataUltimaCobrancaIR
        {
            get { return dataUltimaCobrancaIR; }
            set { dataUltimaCobrancaIR = value; }
        }
        public decimal ValorIR
        {
            get { return valorIR; }
            set { valorIR = value; }
        }
        public decimal ValorIOF
        {
            get { return valorIOF; }
            set { valorIOF = value; }
        }
        public decimal ValorPerformance
        {
            get { return valorPerformance; }
            set { valorPerformance = value; }
        }
        public decimal ValorIOFVirtual
        {
            get { return valorIOFVirtual; }
            set { valorIOFVirtual = value; }
        }
        public decimal QuantidadeAntesCortes
        {
            get { return quantidadeAntesCortes; }
            set { quantidadeAntesCortes = value; }
        }
        public decimal ValorRendimento
        {
            get { return valorRendimento; }
            set { valorRendimento = value; }
        }
        public DateTime? DataUltimoCortePfee
        {
            get { return dataUltimoCortePfee; }
            set { dataUltimoCortePfee = value; }
        }
        public string PosicaoIncorporada
        {
            get { return posicaoIncorporada; }
            set { posicaoIncorporada = value; }
        }
        public string CodigoAnbima
        {
            get { return codigoAnbima; }
            set { codigoAnbima = value; }
        }

        public PosicaoCotistaViewModel()
        {
        }

        public PosicaoCotistaViewModel(PosicaoCotista posicaoCotista)
        {
            this.IdPosicao = posicaoCotista.IdPosicao.Value;
            this.IdOperacao = posicaoCotista.IdOperacao;
            this.IdCotista = posicaoCotista.IdCotista.Value;
            this.IdCarteira = posicaoCotista.IdCarteira.Value;
            this.ValorAplicacao = posicaoCotista.ValorAplicacao.Value;
            this.DataAplicacao = posicaoCotista.DataAplicacao.Value;
            this.DataConversao = posicaoCotista.DataConversao.Value;            
            this.CotaAplicacao = posicaoCotista.CotaAplicacao.Value;
            this.CotaDia = posicaoCotista.CotaDia.Value;
            this.ValorBruto = posicaoCotista.ValorBruto.Value;
            this.ValorLiquido = posicaoCotista.ValorLiquido.Value;
            this.QuantidadeInicial = posicaoCotista.QuantidadeInicial.Value;
            this.Quantidade = posicaoCotista.Quantidade.Value;
            this.QuantidadeBloqueada = posicaoCotista.QuantidadeBloqueada.Value;
            this.DataUltimaCobrancaIR = posicaoCotista.DataUltimaCobrancaIR.Value;
            this.ValorIR = posicaoCotista.ValorIR.Value;
            this.ValorIOF = posicaoCotista.ValorIOF.Value;
            this.ValorPerformance = posicaoCotista.ValorPerformance.Value;
            this.ValorIOFVirtual = posicaoCotista.ValorIOFVirtual.Value;
            this.QuantidadeAntesCortes = posicaoCotista.QuantidadeAntesCortes.Value;
            this.ValorRendimento = posicaoCotista.ValorRendimento.Value;
            this.DataUltimoCortePfee = posicaoCotista.DataUltimoCortePfee;            
            this.PosicaoIncorporada = posicaoCotista.PosicaoIncorporada;

            Carteira carteira = new Carteira();
            if (carteira.LoadByPrimaryKey(this.IdCarteira))
            {
                this.CodigoAnbima = carteira.CodigoAnbid;
            }
        }
    }

    public class ValidateLogin : SoapHeader
    {
        public string Username;
        public string Password;
    }

    private bool CheckAuthentication(string username, string password)
    {
        FinancialMembershipProvider financialMembershipProvider = new FinancialMembershipProvider();
        NameValueCollection listaValores = new NameValueCollection();
        listaValores.Add("passwordFormat", "Encrypted");
        financialMembershipProvider.Initialize(null, listaValores);

        bool autenticado = financialMembershipProvider.ValidateUser(username, password);
        return autenticado;
    }
    
}

