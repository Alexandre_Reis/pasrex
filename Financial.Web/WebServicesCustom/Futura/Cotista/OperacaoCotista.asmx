<%@ WebService Language="C#" Class="OperacaoCotistaWSFutura" %>

using System;
using System.Web;
using System.Web.Services;
using System.Web.Services.Protocols;
using System.Collections.Generic;
using Financial.Integracao.Excel;
using Financial.InvestidorCotista;
using Financial.Security;
using Financial.CRM;
using Financial.Fundo;
using System.Collections.Specialized;
using Financial.Investidor;

[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
public class OperacaoCotistaWSFutura : System.Web.Services.WebService
{

    public ValidateLogin Authentication;
    [SoapHeader("Authentication")]
    [WebMethod]
    public OperacaoCotistaViewModel Importa(int IdCotista, int IdCarteira, DateTime DataOperacao,
        DateTime? DataConversao, DateTime? DataLiquidacao, byte TipoOperacao, byte TipoResgate, decimal Quantidade, decimal CotaOperacao, decimal ValorBruto,
        decimal ValorLiquido, decimal ValorIR, decimal ValorIOF, decimal ValorPerformance, decimal RendimentoResgate)
    {
        
        if(!CheckAuthentication(Authentication.Username, Authentication.Password)){
            string msgErro = "Usu�rio n�o autenticado";
            throw new Exception(msgErro);
        }
        
        ValoresExcelOperacaoCotista item = new ValoresExcelOperacaoCotista();
        item.IdCotista = IdCotista;
        item.IdCarteira = IdCarteira;
        item.DataOperacao = DataOperacao;
        item.DataConversao = DataConversao;
        item.DataLiquidacao = DataLiquidacao;

        item.TipoOperacao = TipoOperacao;
        item.TipoResgate = TipoResgate;
        item.Quantidade = Quantidade;
        item.CotaOperacao = CotaOperacao;
        item.ValorBruto = ValorBruto;
        item.ValorLiquido = ValorLiquido;
        item.ValorIR = ValorIR;
        item.ValorIOF = ValorIOF;
        item.ValorPerformance = ValorPerformance;
        item.RendimentoResgate = RendimentoResgate;

        ImportacaoBasePage basePage = new ImportacaoBasePage();
        basePage.valoresExcelOperacaoCotista = new List<ValoresExcelOperacaoCotista>();
        basePage.valoresExcelOperacaoCotista.Add(item);

        OperacaoCotistaCollection operacoes = basePage.CarregaOperacaoCotista(false);

        OperacaoCotistaViewModel operacaoCotistaViewModel = null;
        if (operacoes.Count > 0)
        {
            OperacaoCotista operacaoCotista = operacoes[0];
            operacaoCotistaViewModel = new OperacaoCotistaViewModel(operacaoCotista);
        }
        return operacaoCotistaViewModel;

    }

    public class OperacaoCotistaViewModel
    {
        public int IdOperacao;
        public int IdCotista;
        public int IdCarteira;
        public DateTime DataOperacao;
        public DateTime DataConversao;
        public DateTime DataLiquidacao;
        public DateTime DataAgendamento;
        public byte TipoOperacao;
        public byte? TipoResgate;
        public int? IdPosicaoResgatada;
        public byte IdFormaLiquidacao;
        public decimal Quantidade;
        public decimal CotaOperacao;
        public decimal ValorBruto;
        public decimal ValorLiquido;
        public decimal ValorIR;
        public decimal ValorIOF;
        public decimal ValorCPMF;
        public decimal ValorPerformance;
        public decimal PrejuizoUsado;
        public decimal RendimentoResgate;
        public decimal VariacaoResgate;
        public string Observacao;
        public string DadosBancarios;
        public string CpfcnpjCarteira;
        public string CpfcnpjCotista;
        public byte Fonte;
        public int? IdConta;
        public decimal? CotaInformada;
        public int? IdAgenda;
        public int? IdOperacaoResgatada;
        public string CodigoAnbima;

        public OperacaoCotistaViewModel()
        {
        }
        public OperacaoCotistaViewModel(OperacaoCotista operacaoCotista)
        {
            Cliente cliente = new Cliente();
            cliente.LoadByPrimaryKey(operacaoCotista.IdCarteira.Value);
            
            Pessoa pessoaCarteira = new Pessoa();
            pessoaCarteira.LoadByPrimaryKey(cliente.IdPessoa.Value);
            this.CpfcnpjCarteira = pessoaCarteira.Cpfcnpj;

            Cotista cotista = new Cotista();
            cotista.LoadByPrimaryKey(operacaoCotista.IdCotista.Value);
            
            Pessoa pessoaCotista = new Pessoa();
            pessoaCotista.LoadByPrimaryKey(cotista.IdPessoa.Value);
            this.CpfcnpjCotista = pessoaCotista.Cpfcnpj;

            this.CotaInformada = operacaoCotista.CotaInformada;
            this.CotaOperacao = operacaoCotista.CotaOperacao.Value;
            this.DadosBancarios = operacaoCotista.DadosBancarios;
            this.DataAgendamento = operacaoCotista.DataAgendamento.Value;
            this.DataConversao = operacaoCotista.DataConversao.Value;
            this.DataLiquidacao = operacaoCotista.DataLiquidacao.Value;
            this.DataOperacao = operacaoCotista.DataOperacao.Value;
            this.Fonte = operacaoCotista.Fonte.Value;
            this.IdAgenda = operacaoCotista.IdAgenda;
            this.IdCarteira = operacaoCotista.IdCarteira.Value;
            this.IdConta = operacaoCotista.IdConta;
            this.IdCotista = operacaoCotista.IdCotista.Value;
            this.IdFormaLiquidacao = operacaoCotista.IdFormaLiquidacao.Value;
            this.IdOperacao = operacaoCotista.IdOperacao.Value;
            this.IdOperacaoResgatada = operacaoCotista.IdOperacaoResgatada;
            this.IdPosicaoResgatada = operacaoCotista.IdPosicaoResgatada;
            if (this.IdOperacaoResgatada == null && this.IdPosicaoResgatada != null)
            {
                PosicaoCotista posicaoResgatada = new PosicaoCotista();
                if (posicaoResgatada.LoadByPrimaryKey(this.IdPosicaoResgatada.Value))
                {
                    this.IdOperacaoResgatada = posicaoResgatada.IdOperacao;
                }
            }
            
            this.Observacao = operacaoCotista.Observacao;
            this.PrejuizoUsado = operacaoCotista.PrejuizoUsado.Value;
            this.Quantidade = operacaoCotista.Quantidade.Value;
            this.RendimentoResgate = operacaoCotista.RendimentoResgate.Value;
            this.TipoOperacao = operacaoCotista.TipoOperacao.Value;
            this.ValorBruto = operacaoCotista.ValorBruto.Value;
            this.ValorCPMF = operacaoCotista.ValorCPMF.Value;
            this.ValorIOF = operacaoCotista.ValorIOF.Value;
            this.ValorIR = operacaoCotista.ValorIR.Value;
            this.ValorLiquido = operacaoCotista.ValorLiquido.Value;
            this.ValorPerformance = operacaoCotista.ValorPerformance.Value;
            this.VariacaoResgate = operacaoCotista.VariacaoResgate.Value;

            Carteira carteira = new Carteira();
            if (carteira.LoadByPrimaryKey(this.IdCarteira))
            {
                this.CodigoAnbima = carteira.CodigoAnbid;
            }
        }
    }
    
    public class ValidateLogin : SoapHeader
    {
        public string Username;
        public string Password;
    }

    private bool CheckAuthentication(string username, string password)
    {
        FinancialMembershipProvider financialMembershipProvider = new FinancialMembershipProvider();
        NameValueCollection listaValores = new NameValueCollection();
        listaValores.Add("passwordFormat", "Encrypted");
        financialMembershipProvider.Initialize(null, listaValores);

        bool autenticado = financialMembershipProvider.ValidateUser(username, password);
        return autenticado;
    }

    [SoapHeader("Authentication")]
    [WebMethod]
    public List<OperacaoCotistaViewModel> Exporta(int? IdOperacao, int? IdCotista, int? IdCarteira, DateTime? DataOperacao)
    {
        if (!CheckAuthentication(Authentication.Username, Authentication.Password))
        {
            string msgErro = "Usu�rio n�o autenticado";
            throw new Exception(msgErro);
        }
        
        OperacaoCotistaCollection operacoes = new OperacaoCotistaCollection();

        if (IdOperacao.HasValue)
        {
            operacoes.Query.Where(operacoes.Query.IdOperacao.Equal(IdOperacao));
        }
        
        if (IdCotista.HasValue)
        {
            operacoes.Query.Where(operacoes.Query.IdCotista.Equal(IdCotista));
        }

        if (IdCarteira.HasValue)
        {
            operacoes.Query.Where(operacoes.Query.IdCarteira.Equal(IdCarteira));
        }

        if (DataOperacao.HasValue)
        {
            operacoes.Query.Where(operacoes.Query.DataOperacao.Equal(DataOperacao.Value.Date));
        }

        List<OperacaoCotistaViewModel> operacoesViewModel = new List<OperacaoCotistaViewModel>();
        if (operacoes.Load(operacoes.Query))
        {
            foreach (OperacaoCotista operacao in operacoes)
            {
                operacoesViewModel.Add(new OperacaoCotistaViewModel(operacao));
            }
        }
        
        return operacoesViewModel;
    }
}