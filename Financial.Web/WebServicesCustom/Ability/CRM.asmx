<%@ WebService Language="C#" Class="CRM" %>

using System;
using System.Web;
using System.Web.Services;
using System.Web.Services.Protocols;
using Financial.InvestidorCotista;
using System.Collections.Generic;
using Financial.Integracao.Excel;
using Financial.Security;
using System.Collections.Specialized;
using Financial.Fundo;
using Financial.CRM;
using Financial.Interfaces.Export;
using System.Web.Configuration;
using System.IO;
using  Financial.Web.Util;

[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
public class CRM : System.Web.Services.WebService
{
    private class key_Carteira_Cotista
    {
        public int IdCarteira;
        public int IdCotista;        
    }
    
    public ValidateLogin Authentication;

    [SoapHeader("Authentication")]
    [TraceExtensionAttribute]
    [WebMethod]
    public CotistaViewModel ImportaCotista(int? IdCotista, string Nome, byte Tipo, string Cpfcnpj, string IsentoIR, string IsentoIOF,
       byte StatusAtivo, byte TipoTributacao, byte TipoCotistaCVM, string CodigoInterface, string Endereco, string Numero,
        string Complemento, string Bairro, string Cidade, string CEP, string UF, string Pais, string EnderecoCom,
        string NumeroCom, string ComplementoCom, string BairroCom, string CidadeCom, string CEPCom, string UFCom,
        string PaisCom, string FoneDDD, string FoneNumero, string FoneRamal,
        string Email, string FoneDDDCom, string FoneNumeroCom, string FoneRamalCom,
        string EmailCom, byte EstadoCivil, string NumeroRG,
        string EmissorRG, DateTime DataEmissaoRG, string EmissorUF, string Sexo, DateTime DataNascimento, string Profissao,
        List<Financial.Integracao.Excel.ValoresExcelCadastroCotista.InformacaoBancaria> InformacoesBancarias,
        byte SituacaoLegal, decimal ValorRendaFamiliar, decimal ValorPatrimonio, string CodigoDistribuidor,
        string FiliacaoNomePai, string FiliacaoNomeMae, string NomeConjuge, string PessoaVinculada,
        short NaturezaJuridica, string TipoRenda, string IndicadorFatca, string GiinTin, string JustificativaGiin,
        byte CnaeDivisao, byte CnaeGrupo, byte CnaeClasse, byte CnaeSubClasse, string CodigoPaisNascimento,
        int RankingAnbima, string EmpresaTrabalha, int NIRE, string CodigoNacionalidade, string CodigoNaturalidade,
        string PessoaPoliticamenteExposta, string ContaOrdem, string CodigoPaisResidenciaFiscal, int? IdTitular,
        out string MsgErro)
    {
        MsgErro = "";

        if (Authentication == null)
        {
            string msgErro = "Credenciais de autentica��o n�o foram informadas";
            throw new Exception(msgErro);
        }
        else if (!CheckAuthentication(Authentication.Username, Authentication.Password))
        {
            string msgErro = "Usu�rio n�o autenticado";
            throw new Exception(msgErro);
        }

        ValoresExcelCadastroCotista item = new ValoresExcelCadastroCotista();

        if (!IdCotista.HasValue)
        {
            IdCotista = new Pessoa().GeraIdPessoa();
        }

        item.IdCotista = IdCotista.Value;
        item.Nome = Nome;
        item.TipoPessoa = (Financial.CRM.Enums.TipoPessoa)Tipo;
        item.Cnpj = Cpfcnpj;
        //
        item.IsentoIR = IsentoIR;
        item.IsentoIOF = IsentoIOF;
        item.StatusCotista = (Financial.InvestidorCotista.Enums.StatusAtivoCotista)StatusAtivo;
        //
        item.TipoTributacaoCotista = (Financial.InvestidorCotista.Enums.TipoTributacaoCotista)TipoTributacao;
        //
        item.TipoCotistaCVM = (Financial.InvestidorCotista.Enums.TipoCotista)TipoCotistaCVM;
        //
        item.CodigoInterface = CodigoInterface;
        //
        item.EstadoCivilCotista = (Financial.CRM.Enums.EstadoCivilPessoa)EstadoCivil;
        item.Rg = NumeroRG;
        item.EmissorRg = EmissorRG;
        //
        item.DataEmissaoRg = DataEmissaoRG;

        //
        item.Sexo = Sexo;
        item.DataNascimento = DataNascimento;
        item.Profissao = Profissao;
        //                                                                              
        ValoresExcelCadastroCotista.Endereco[] endereco = {
                            new ValoresExcelCadastroCotista.Endereco(
                                Endereco,  //Endereco
                                Numero,  //Numero
                                Complemento, //Complemento 
                                Bairro, //Bairro
                                Cidade, //Cidade
                                CEP, //CEP
                                UF, //UF
                                Pais, //Pais
                                FoneDDD, //Fone
                                FoneNumero, //Fone
                                FoneRamal, //Fone
                                Email  //Email
                                ),
                            new ValoresExcelCadastroCotista.Endereco(
                                EnderecoCom, //EnderecoCom
                                NumeroCom, //NumeroCom
                                ComplementoCom, //ComplementoCom
                                BairroCom, //BairroCom
                                CidadeCom, //CidadeCom
                                CEPCom, //CEPCom
                                UFCom, //UFCom
                                PaisCom, //Pais
            FoneDDDCom, //Fone                    
            FoneNumeroCom, //Fone
            FoneRamalCom, //Fone
                                EmailCom  //Email
                                )
                        };

        item.EnderecoPessoa = endereco[0];
        item.EnderecoComercialPessoa = endereco[1];
        item.CriadoPor = Authentication.Username;
        item.IdTitular = IdTitular;
        item.InformacoesBancarias = InformacoesBancarias;

        ImportacaoBasePage basePage = new ImportacaoBasePage();
        basePage.valoresExcelCadastroCotista = new List<ValoresExcelCadastroCotista>();
        basePage.valoresExcelCadastroCotista.Add(item);

        Cotista cotista = new Cotista();

        bool integraCotistaItau = WebConfigurationManager.AppSettings["IntegraCotistaItau"] != null ?
            Convert.ToBoolean(WebConfigurationManager.AppSettings["IntegraCotistaItau"]) : false;

        if (cotista.LoadByPrimaryKey(item.IdCotista))
        {
            basePage.AtualizaCadastroCotista();

            cotista = new Cotista();
            cotista.LoadByPrimaryKey(IdCotista.Value);

            if (integraCotistaItau && IdTitular.HasValue)
            {
                Itau itau = new Itau();
                itau.EnviaCco_XML(IdCotista.Value, "A");
            }
        }
        else
        {
            basePage.CarregaCadastroCotista();
            cotista = new Cotista();
            cotista.LoadByPrimaryKey(IdCotista.Value);

            if (integraCotistaItau)
            {
                Itau itau = new Itau();
                if (IdTitular.HasValue)
                {
                    itau.EnviaCco_XML(IdCotista.Value, "I");
                }
                else
                {
                    cotista.CodigoInterface = itau.EnviaCac_XML(IdCotista.Value);
                }
                cotista.Save();
            }

        }

        cotista = new Cotista();
        cotista.LoadByPrimaryKey(IdCotista.Value);
        return new CotistaViewModel(cotista);
    }

    public class ValidateLogin : SoapHeader
    {
        public string Username;
        public string Password;
    }

    private bool CheckAuthentication(string username, string password)
    {
        FinancialMembershipProvider financialMembershipProvider = new FinancialMembershipProvider();
        NameValueCollection listaValores = new NameValueCollection();
        listaValores.Add("passwordFormat", "Encrypted");
        financialMembershipProvider.Initialize(null, listaValores);

        bool autenticado = financialMembershipProvider.ValidateUser(username, password);
        return autenticado;
    }


    //USANDO IDTITULAR PREENCHIDO NO METODO IMPORTACOTISTA
    /*[SoapHeader("Authentication")]
    [WebMethod]
    public int ImportaCotitular(
        int? IdPessoa, int IdTitular, string CodigoInterface,
        string Nome, string Cpf,
        string Endereco, string Numero, string Complemento, string Bairro, string Cidade, string CEP, string UF, string Pais,
        string FoneDDD, string FoneNumero, string FoneRamal, string Email,
        string FoneDDDCom, string FoneNumeroCom, string FoneRamalCom,
        byte EstadoCivil, string NumeroRG, string EmissorRG, DateTime DataEmissaoRG, string EmissorUF,
        string Sexo, DateTime DataNascimento, string Profissao,
        byte SituacaoLegal, decimal ValorRendaFamiliar, decimal ValorPatrimonio,
        string FiliacaoNomePai, string FiliacaoNomeMae, string NomeConjuge,
        string GiinTin,
        string CodigoPaisNascimento,
        string EmpresaTrabalha, int NIRE, string CodigoNacionalidade, string CodigoNaturalidade,
        string CodigoPaisResidenciaFiscal,
        out string MsgErro)
    {

        MsgErro = "";

        if (Authentication == null)
        {
            string msgErro = "Credenciais de autentica��o n�o foram informadas";
            throw new Exception(msgErro);
        }
        else if (!CheckAuthentication(Authentication.Username, Authentication.Password))
        {
            string msgErro = "Usu�rio n�o autenticado";
            throw new Exception(msgErro);
        }

        if(insert){
            //Criar documento de pessoa

            
            
            //Chamar metodo do itau para inclusao
            
        }else{
            //Atualizar documento de pessoa

            //Chamar metodo do itau para inclusao ou alteracao de CCO
        }
       
    }*/


    //NAO IMPLEMENTADO
    /*[SoapHeader("Authentication")]
    [WebMethod]
    public int ImportaPessoaVinculada(
        int? IdPessoa, string CodigoInterface,
        string CodigoPesquisa, byte TipoVinculo,
        string Nome, byte Tipo, string IndicadorPEP,
        string IndicadorTitularidade, string SituacaoResponsavel,
        string Sexo, string Cpfcnpj,
        string NumeroRG, string EmissorRG, DateTime DataEmissaoRG, string EmissorUF,
        string Endereco, string Numero, string Complemento, string Bairro, string Cidade, string CEP, string UF, string Pais,
        int PorcentagemParticipacao,
        string NomeContato, string FoneDDD, string FoneNumero, string FoneRamal, string Email,
        string NomeContatoCom, string FoneDDDCom, string FoneNumeroCom, string FoneRamalCom, string EmailCom,
        string CodigoNacionalidade, string CodigoPaisResidenciaFiscal,
        string GiinTin, DateTime DataNascimento,
        string CodigoNaturalidade,
        DateTime DataVencimentoProcuracao,
        string AutorizaTransmissaoOrdens,
        string FiliacaoNomePai, string FiliacaoNomeMae, string NomeConjuge,
        byte EstadoCivil,
        out string MsgErro)
    {
        MsgErro = "";

        if (Authentication == null)
        {
            string msgErro = "Credenciais de autentica��o n�o foram informadas";
            throw new Exception(msgErro);
        }
        else if (!CheckAuthentication(Authentication.Username, Authentication.Password))
        {
            string msgErro = "Usu�rio n�o autenticado";
            throw new Exception(msgErro);
        }

        return 1;
    }*/


    [SoapHeader("Authentication")]
    [WebMethod]
    public CotistaViewModel ExportaCotista(int IdCotista, out string MsgErro)
    {

        MsgErro = "";

        if (Authentication == null)
        {
            string msgErro = "Credenciais de autentica��o n�o foram informadas";
            throw new Exception(msgErro);
        }
        else if (!CheckAuthentication(Authentication.Username, Authentication.Password))
        {
            string msgErro = "Usu�rio n�o autenticado";
            throw new Exception(msgErro);
        }

        Cotista cotista = new Cotista();
        if (!cotista.LoadByPrimaryKey(IdCotista))
        {
            MsgErro = "N�o foi poss�vel encontrar o cotista com ID: " + IdCotista;
            return null;
        }
        CotistaViewModel cotistaViewModel = new CotistaViewModel(cotista);

        return cotistaViewModel;

    }

    [SoapHeader("Authentication")]
    [WebMethod]
    public List<CotistaViewModel> ExportaListaCotista()
    {

        if (Authentication == null)
        {
            string msgErro = "Credenciais de autentica��o n�o foram informadas";
            throw new Exception(msgErro);
        }
        else if (!CheckAuthentication(Authentication.Username, Authentication.Password))
        {
            string msgErro = "Usu�rio n�o autenticado";
            throw new Exception(msgErro);
        }

        CotistaCollection cotistas = new CotistaCollection();
        cotistas.LoadAll();

        List<CotistaViewModel> cotistasViewModel = new List<CotistaViewModel>();
        foreach (Cotista cotista in cotistas)
        {
            CotistaViewModel cotistaViewModel = new CotistaViewModel(cotista);
            cotistasViewModel.Add(cotistaViewModel);
        }

        return cotistasViewModel;

    }

    [SoapHeader("Authentication")]
    [WebMethod]
    public List<PosicaoCotistaViewModel> ExportaPosicaoCotista(int IdCotista, out string MsgErro)
    {

        MsgErro = "";

        if (Authentication == null)
        {
            string msgErro = "Credenciais de autentica��o n�o foram informadas";
            throw new Exception(msgErro);
        }
        else if (!CheckAuthentication(Authentication.Username, Authentication.Password))
        {
            string msgErro = "Usu�rio n�o autenticado";
            throw new Exception(msgErro);
        }

        PosicaoCotistaCollection posicoes = new PosicaoCotistaCollection();

        posicoes.Query.Where(posicoes.Query.IdCotista.Equal(IdCotista));

        posicoes.Load(posicoes.Query);

        List<PosicaoCotistaViewModel> posicoesViewModel = new List<PosicaoCotistaViewModel>();

        Dictionary<string, PosicaoCotista> distinctCotistaCarteira = new Dictionary<string, PosicaoCotista>();
        foreach (PosicaoCotista posicaoCotista in posicoes)
        {
            string keyCotistaCarteira = String.Format("{0}|{1}", posicaoCotista.IdCotista, posicaoCotista.IdCarteira);
            if (!distinctCotistaCarteira.ContainsKey(keyCotistaCarteira))
            {
                distinctCotistaCarteira.Add(keyCotistaCarteira, posicaoCotista);
            }
        }

        foreach (KeyValuePair<string, PosicaoCotista> posicaoDistinct in distinctCotistaCarteira)
        {
            string[] splittedKey = posicaoDistinct.Key.Split('|');
            int idCotista = Convert.ToInt32(splittedKey[0]);
            int idCarteira = Convert.ToInt32(splittedKey[1]);

            //Descobrir data mais recente da abertura
            DateTime dataHistorico = DateTime.MinValue;
            PosicaoCotistaAbertura posicaoMaisRecente = new PosicaoCotistaAbertura();
            posicaoMaisRecente.Query.Select(posicaoMaisRecente.Query.DataHistorico.Max());
            posicaoMaisRecente.Query.Where(posicaoMaisRecente.Query.IdCarteira.Equal(idCarteira),
                    posicaoMaisRecente.Query.IdCotista.Equal(idCotista));

            if (posicaoMaisRecente.Query.Load())
            {
                if (posicaoMaisRecente.DataHistorico.HasValue)
                {
                    dataHistorico = posicaoMaisRecente.DataHistorico.Value;
                }
            }

            PosicaoCotista posicaoLivre = new PosicaoCotista().RetornaPosicaoLivre(idCarteira, idCotista, dataHistorico, null);
            posicoesViewModel.Add(new PosicaoCotistaViewModel(posicaoLivre, dataHistorico));
        }

        return posicoesViewModel;
    }

    [SoapHeader("Authentication")]
    [WebMethod]
    public List<PosicaoCotistaViewModel> ExportaPosicaoCotistaPorData(int? IdCotista, DateTime DataHistorico, out string MsgErro)
    {
        MsgErro = "";

        if (Authentication == null)
        {
            string msgErro = "Credenciais de autentica��o n�o foram informadas";
            throw new Exception(msgErro);
        }
        else if (!CheckAuthentication(Authentication.Username, Authentication.Password))
        {
            string msgErro = "Usu�rio n�o autenticado";
            throw new Exception(msgErro);
        }

        //Ajustar data posicao para dia util (Crava zero na hora, minuto e segundo)
        DataHistorico = Financial.Util.Calendario.IsDiaUtil(DataHistorico) ? DataHistorico : Financial.Util.Calendario.SubtraiDiaUtil(DataHistorico, 1);
        DataHistorico = DataHistorico.Date;

        Dictionary<string, key_Carteira_Cotista> dic_Key_Carteira_Cotista = new Dictionary<string, key_Carteira_Cotista>();
        
        #region Posicao Cotista Hist�rico
        PosicaoCotistaHistoricoCollection posicoesHistorico = new PosicaoCotistaHistoricoCollection();
        posicoesHistorico.Query.es.Distinct = true;
        posicoesHistorico.Query.Select(posicoesHistorico.Query.IdCarteira, posicoesHistorico.Query.IdCotista);
        posicoesHistorico.Query.Where(posicoesHistorico.Query.DataHistorico.Equal(DataHistorico));
 
        if (IdCotista.HasValue)
        {
            posicoesHistorico.Query.Where(posicoesHistorico.Query.IdCotista.Equal(IdCotista.Value));
        }

        posicoesHistorico.Load(posicoesHistorico.Query);
                                
        foreach (PosicaoCotistaHistorico posicaoCotista in posicoesHistorico)
        {
            string keyCotistaCarteira = String.Format("{0}|{1}", posicaoCotista.IdCotista, posicaoCotista.IdCarteira);
            if (!dic_Key_Carteira_Cotista.ContainsKey(keyCotistaCarteira))
            {
                key_Carteira_Cotista key = new key_Carteira_Cotista();
                key.IdCarteira = posicaoCotista.IdCarteira.Value;
                key.IdCotista= posicaoCotista.IdCotista.Value;
                dic_Key_Carteira_Cotista.Add(keyCotistaCarteira, key);
            }
        }
        #endregion

        #region Posicao Cotista Hist�rico
        PosicaoCotistaAberturaCollection posicoesAbertura = new PosicaoCotistaAberturaCollection();
        posicoesAbertura.Query.es.Distinct = true;
        posicoesAbertura.Query.Select(posicoesAbertura.Query.IdCarteira, posicoesAbertura.Query.IdCotista);
        posicoesAbertura.Query.Where(posicoesAbertura.Query.DataHistorico.Equal(DataHistorico));

        if (IdCotista.HasValue)
        {
            posicoesAbertura.Query.Where(posicoesAbertura.Query.IdCotista.Equal(IdCotista.Value));
        }

        posicoesAbertura.Load(posicoesAbertura.Query);

        foreach (PosicaoCotistaAbertura posicaoCotista in posicoesAbertura)
        {
            string keyCotistaCarteira = String.Format("{0}|{1}", posicaoCotista.IdCotista, posicaoCotista.IdCarteira);
            if (!dic_Key_Carteira_Cotista.ContainsKey(keyCotistaCarteira))
            {
                key_Carteira_Cotista key = new key_Carteira_Cotista();
                key.IdCarteira = posicaoCotista.IdCarteira.Value;
                key.IdCotista = posicaoCotista.IdCotista.Value;
                dic_Key_Carteira_Cotista.Add(keyCotistaCarteira, key);
            }
        }
        #endregion        
        
        List<PosicaoCotistaViewModel> posicoesViewModel = new List<PosicaoCotistaViewModel>();
        foreach (KeyValuePair<string, key_Carteira_Cotista> key_Carteira_Cotista in dic_Key_Carteira_Cotista)
        {
            int idCotista = key_Carteira_Cotista.Value.IdCotista;
            int idCarteira = key_Carteira_Cotista.Value.IdCarteira;

            PosicaoCotista posicaoLivre = new PosicaoCotista().RetornaPosicaoLivreHistorico(idCarteira, idCotista, DataHistorico, null);

            if (posicaoLivre.IdCarteira.HasValue)
            {
                posicoesViewModel.Add(new PosicaoCotistaViewModel(posicaoLivre, DataHistorico));
            }
            else
            {
                posicaoLivre = new PosicaoCotista().RetornaPosicaoLivre(idCarteira, idCotista, DataHistorico, null, false);
                if (posicaoLivre.IdCarteira.HasValue)
                {
                    posicoesViewModel.Add(new PosicaoCotistaViewModel(posicaoLivre, DataHistorico));
                }
            }
        }

        return posicoesViewModel;
    }

    public class PosicaoCotistaViewModel
    {
        public DateTime? DataHistorico;
        public int? IdPosicao;
        public int? IdOperacao;
        public int? IdCotista;
        public int? IdCarteira;
        public string CpfcnpjCarteira;
        public string CpfcnpjCotista;
        public decimal? ValorAplicacao;
        public DateTime? DataAplicacao;
        public DateTime? DataConversao;
        public decimal? CotaAplicacao;
        public decimal? CotaDia;
        public decimal? ValorBruto;
        public decimal? ValorLiquido;
        public decimal? QuantidadeInicial;
        public decimal? Quantidade;
        public decimal? QuantidadeBloqueada;
        public DateTime? DataUltimaCobrancaIR;
        public decimal? ValorIR;
        public decimal? ValorIOF;
        public decimal? ValorPerformance;
        public decimal? ValorIOFVirtual;
        public decimal? QuantidadeAntesCortes;
        public decimal? ValorRendimento;
        public DateTime? DataUltimoCortePfee;
        public string PosicaoIncorporada;
        public string NomeCarteira;
        public decimal? ValorIRMaio;
        public decimal? ValorIRNovembro;
        public string CodigoInterfaceCotista;

        public decimal? AplicacoesMes;
        public decimal? DepositosMes;
        public decimal? ResgatesMes;
        public decimal? RetiradasMes;
        public decimal? AplicacoesAno;
        public decimal? DepositosAno;
        public decimal? ResgatesAno;
        public decimal? RetiradasAno;

        public PosicaoCotistaViewModel()
        {
        }
        public PosicaoCotistaViewModel(PosicaoCotista posicaoCotista, DateTime DataHistorico)
        {
            this.DataHistorico = DataHistorico;
            this.CotaAplicacao = posicaoCotista.CotaAplicacao;
            this.CotaDia = posicaoCotista.CotaDia;
            this.DataAplicacao = posicaoCotista.DataAplicacao;
            this.DataConversao = posicaoCotista.DataConversao;
            this.DataUltimaCobrancaIR = posicaoCotista.DataUltimaCobrancaIR;
            this.DataUltimoCortePfee = posicaoCotista.DataUltimoCortePfee;
            this.IdCarteira = posicaoCotista.IdCarteira;
            this.IdCotista = posicaoCotista.IdCotista;
            this.IdOperacao = posicaoCotista.IdOperacao;
            this.IdPosicao = posicaoCotista.IdPosicao;
            this.PosicaoIncorporada = posicaoCotista.PosicaoIncorporada;
            this.Quantidade = posicaoCotista.Quantidade;
            this.QuantidadeAntesCortes = posicaoCotista.QuantidadeAntesCortes;
            this.QuantidadeBloqueada = posicaoCotista.QuantidadeBloqueada;
            this.QuantidadeInicial = posicaoCotista.QuantidadeInicial;
            this.ValorAplicacao = posicaoCotista.ValorAplicacao;
            this.ValorBruto = posicaoCotista.ValorBruto;
            this.ValorIOF = posicaoCotista.ValorIOF;
            this.ValorIOFVirtual = posicaoCotista.ValorIOFVirtual;
            this.ValorIR = posicaoCotista.ValorIR;
            this.ValorLiquido = posicaoCotista.ValorLiquido;
            this.ValorPerformance = posicaoCotista.ValorPerformance;
            this.ValorRendimento = posicaoCotista.ValorRendimento;

            BuscaMovimento(this, DataHistorico);

            Carteira carteira = new Carteira();
            carteira.LoadByPrimaryKey(this.IdCarteira.Value);
            this.NomeCarteira = carteira.Nome;

            Pessoa pessoa = new Pessoa();
            pessoa.LoadByPrimaryKey(this.IdCarteira.Value);
            this.CpfcnpjCarteira = pessoa.Cpfcnpj;

            pessoa = new Pessoa();
            pessoa.LoadByPrimaryKey(this.IdCotista.Value);
            this.CpfcnpjCotista = pessoa.Cpfcnpj;

            Financial.InvestidorCotista.Cotista cotista = new Financial.InvestidorCotista.Cotista();
            cotista.LoadByPrimaryKey(posicaoCotista.IdCotista.Value);
            this.CodigoInterfaceCotista = cotista.CodigoInterface;

            this.ValorIRMaio = 0;
            this.ValorIRNovembro = 0;
        }

        public PosicaoCotistaViewModel(PosicaoCotistaHistorico posicaoCotista, string x)
        {
            this.CotaAplicacao = posicaoCotista.CotaAplicacao;
            this.CotaDia = posicaoCotista.CotaDia;
            this.DataAplicacao = posicaoCotista.DataAplicacao;
            this.DataConversao = posicaoCotista.DataConversao;
            this.DataUltimaCobrancaIR = posicaoCotista.DataUltimaCobrancaIR;
            this.DataUltimoCortePfee = posicaoCotista.DataUltimoCortePfee;
            this.IdCarteira = posicaoCotista.IdCarteira;
            this.IdCotista = posicaoCotista.IdCotista;
            this.IdOperacao = posicaoCotista.IdOperacao;
            this.IdPosicao = posicaoCotista.IdPosicao;
            this.PosicaoIncorporada = posicaoCotista.PosicaoIncorporada;
            this.Quantidade = posicaoCotista.Quantidade;
            this.QuantidadeAntesCortes = posicaoCotista.QuantidadeAntesCortes;
            this.QuantidadeBloqueada = posicaoCotista.QuantidadeBloqueada;
            this.QuantidadeInicial = posicaoCotista.QuantidadeInicial;
            this.ValorAplicacao = posicaoCotista.ValorAplicacao;
            this.ValorBruto = posicaoCotista.ValorBruto;
            this.ValorIOF = posicaoCotista.ValorIOF;
            this.ValorIOFVirtual = posicaoCotista.ValorIOFVirtual;
            this.ValorIR = posicaoCotista.ValorIR;
            this.ValorLiquido = posicaoCotista.ValorLiquido;
            this.ValorPerformance = posicaoCotista.ValorPerformance;
            this.ValorRendimento = posicaoCotista.ValorRendimento;



            Carteira carteira = new Carteira();
            carteira.LoadByPrimaryKey(this.IdCarteira.Value);
            this.NomeCarteira = carteira.Nome;

            Pessoa pessoa = new Pessoa();
            pessoa.LoadByPrimaryKey(this.IdCarteira.Value);
            this.CpfcnpjCarteira = pessoa.Cpfcnpj;

            pessoa = new Pessoa();
            pessoa.LoadByPrimaryKey(this.IdCotista.Value);
            this.CpfcnpjCotista = pessoa.Cpfcnpj;

            Financial.InvestidorCotista.Cotista cotista = new Financial.InvestidorCotista.Cotista();
            cotista.LoadByPrimaryKey(posicaoCotista.IdCotista.Value);
            this.CodigoInterfaceCotista = cotista.CodigoInterface;

            this.ValorIRMaio = 0;
            this.ValorIRNovembro = 0;
        }

        private void BuscaMovimento(PosicaoCotistaViewModel viewModel, DateTime data)
        {
            viewModel.AplicacoesMes = 0;
            viewModel.DepositosMes = 0;
            viewModel.ResgatesMes = 0;
            viewModel.RetiradasMes = 0;
            viewModel.AplicacoesAno = 0;
            viewModel.DepositosAno = 0;
            viewModel.ResgatesAno = 0;
            viewModel.RetiradasAno = 0;

            DateTime dtMesInicio = Financial.Util.Calendario.RetornaPrimeiroDiaUtilMes(data);
            DateTime dtMesFim = Financial.Util.Calendario.RetornaUltimoDiaUtilMes(data);
            DateTime dtAnoInicio = Financial.Util.Calendario.RetornaPrimeiroDiaUtilAno(data);
            DateTime dtAnoFim = Financial.Util.Calendario.RetornaUltimoDiaUtilAno(data);

            // mes
            OperacaoCotistaQuery operacaoCotistaQuery = new OperacaoCotistaQuery("O");
            operacaoCotistaQuery.Select(operacaoCotistaQuery.TipoOperacao,
                                        operacaoCotistaQuery.ValorBruto.Sum());
            operacaoCotistaQuery.Where(operacaoCotistaQuery.IdCotista.Equal(viewModel.IdCotista.Value),
                                       operacaoCotistaQuery.DataConversao.Between(dtMesInicio, dtMesFim));
            operacaoCotistaQuery.GroupBy(operacaoCotistaQuery.TipoOperacao);

            OperacaoCotistaCollection operacaoCotistaCollection = new OperacaoCotistaCollection();
            if (operacaoCotistaCollection.Load(operacaoCotistaQuery))
            {
                foreach (OperacaoCotista operacaoCotista in operacaoCotistaCollection)
                {
                    switch (operacaoCotista.TipoOperacao)
                    {
                        case (byte)Financial.InvestidorCotista.Enums.TipoOperacaoCotista.Aplicacao:
                        case (byte)Financial.InvestidorCotista.Enums.TipoOperacaoCotista.AplicacaoCotasEspecial:
                        case (byte)Financial.InvestidorCotista.Enums.TipoOperacaoCotista.AplicacaoAcoesEspecial:

                            viewModel.AplicacoesMes += operacaoCotista.ValorBruto.Value;
                            break;

                        case (byte)Financial.InvestidorCotista.Enums.TipoOperacaoCotista.ResgateBruto:
                        case (byte)Financial.InvestidorCotista.Enums.TipoOperacaoCotista.ResgateLiquido:
                        case (byte)Financial.InvestidorCotista.Enums.TipoOperacaoCotista.ResgateCotas:
                        case (byte)Financial.InvestidorCotista.Enums.TipoOperacaoCotista.ResgateTotal:
                        case (byte)Financial.InvestidorCotista.Enums.TipoOperacaoCotista.ResgateCotasEspecial:
                        case (byte)Financial.InvestidorCotista.Enums.TipoOperacaoCotista.ComeCotas:

                            viewModel.ResgatesMes += operacaoCotista.ValorBruto.Value;

                            break;


                        //(byte)Financial.InvestidorCotista.Enums.TipoOperacaoCotista.Amortizacao 
                        //(byte)Financial.InvestidorCotista.Enums.TipoOperacaoCotista.Juros 
                        //(byte)Financial.InvestidorCotista.Enums.TipoOperacaoCotista.AmortizacaoJuros 
                        //(byte)Financial.InvestidorCotista.Enums.TipoOperacaoCotista.IncorporacaoResgate 
                        //(byte)Financial.InvestidorCotista.Enums.TipoOperacaoCotista.IncorporacaoAplicacao 
                        //(byte)Financial.InvestidorCotista.Enums.TipoOperacaoCotista.Dividendo 


                        default:
                            break;
                    }
                }
            }

            // ano
            operacaoCotistaQuery = new OperacaoCotistaQuery("O");
            operacaoCotistaQuery.Select(operacaoCotistaQuery.TipoOperacao,
                                        operacaoCotistaQuery.ValorBruto.Sum());
            operacaoCotistaQuery.Where(operacaoCotistaQuery.IdCotista.Equal(viewModel.IdCotista.Value),
                                       operacaoCotistaQuery.DataConversao.Between(dtAnoInicio, dtAnoFim));
            operacaoCotistaQuery.GroupBy(operacaoCotistaQuery.TipoOperacao);

            operacaoCotistaCollection = new OperacaoCotistaCollection();
            if (operacaoCotistaCollection.Load(operacaoCotistaQuery))
            {
                foreach (OperacaoCotista operacaoCotista in operacaoCotistaCollection)
                {
                    switch (operacaoCotista.TipoOperacao)
                    {
                        case (byte)Financial.InvestidorCotista.Enums.TipoOperacaoCotista.Aplicacao:
                        case (byte)Financial.InvestidorCotista.Enums.TipoOperacaoCotista.AplicacaoCotasEspecial:
                        case (byte)Financial.InvestidorCotista.Enums.TipoOperacaoCotista.AplicacaoAcoesEspecial:

                            viewModel.AplicacoesAno += operacaoCotista.ValorBruto.Value;
                            break;

                        case (byte)Financial.InvestidorCotista.Enums.TipoOperacaoCotista.ResgateBruto:
                        case (byte)Financial.InvestidorCotista.Enums.TipoOperacaoCotista.ResgateLiquido:
                        case (byte)Financial.InvestidorCotista.Enums.TipoOperacaoCotista.ResgateCotas:
                        case (byte)Financial.InvestidorCotista.Enums.TipoOperacaoCotista.ResgateTotal:
                        case (byte)Financial.InvestidorCotista.Enums.TipoOperacaoCotista.ResgateCotasEspecial:
                        case (byte)Financial.InvestidorCotista.Enums.TipoOperacaoCotista.ComeCotas:

                            viewModel.ResgatesAno += operacaoCotista.ValorBruto.Value;

                            break;

                        default:
                            break;

                    }
                }
            }
        }
    }

    public class CotistaViewModel
    {
        public string Cpfcnpj;
        public int IdCotista;
        public string Nome;
        public string Apelido;
        public string IsentoIR;
        public string IsentoIOF;
        public byte StatusAtivo;
        public int? TipoCotistaCVM;
        public string CodigoInterface;
        public byte TipoTributacao;
        public DateTime? DataExpiracao;
        public string PendenciaCadastral;

        public CotistaViewModel()
        {
        }

        public CotistaViewModel(Financial.InvestidorCotista.Cotista cotista)
        {
            this.Apelido = cotista.Apelido;
            this.CodigoInterface = cotista.CodigoInterface;
            this.DataExpiracao = cotista.DataExpiracao;
            this.IdCotista = cotista.IdCotista.Value;
            this.IsentoIOF = cotista.IsentoIOF;
            this.IsentoIR = cotista.IsentoIR;
            this.Nome = cotista.Nome;
            this.PendenciaCadastral = cotista.PendenciaCadastral;
            this.StatusAtivo = cotista.StatusAtivo.Value;
            this.TipoCotistaCVM = cotista.TipoCotistaCVM;
            this.TipoTributacao = cotista.TipoTributacao.Value;
            Pessoa pessoa = new Pessoa();
            pessoa.LoadByPrimaryKey(cotista.IdCotista.Value);
            this.Cpfcnpj = pessoa.Cpfcnpj;
            this.CodigoInterface = cotista.CodigoInterface;
        }



    }

}

