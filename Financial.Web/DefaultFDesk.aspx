﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="DefaultFDesk.aspx.cs" Inherits="DefaultFDesk" %>

<%@ Register Assembly="System.Web, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"
    Namespace="System.Web.UI" TagPrefix="cc1" %>
<%@ Register Assembly="EntitySpaces.Web" Namespace="EntitySpaces.Web" TagPrefix="cc1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<meta http-equiv="X-UA-Compatible" content="IE=EmulateIE8" />
<head id="Head1" runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
    <link href="js/ext/resources/css/ext-all.css" rel="stylesheet" type="text/css" />
    <link href="css/financialdesk.css" rel="stylesheet" type="text/css" />
    <link href="cssCustom/financialdesk.css" rel="stylesheet" type="text/css" />
    <link rel="SHORTCUT ICON" href="~/imagensPersonalizadas/favicon.ico" type="image/x-icon" />
    <title id="Title1" runat="server">Financial: Portal do Cliente</title>

    <script type="text/javascript">
            var FDESK = {};
            
            function OnGetDataCliente(values) {
                popupCliente.HideWindow();
                window.location = window.location.pathname + '?' + 'idCliente=' + values;
                //FDESK.AS.loadEnvironment(null,null,{idCliente : values});
            }
            
            function OnGetDataCarteira(values) {
                popupCarteira.HideWindow();
                FDESK.LE.publishEvent('ativoselected', values);
            }
            
            function OnGetDataAtivoBolsa(values) {
                popupAtivoBolsa.HideWindow();
                FDESK.LE.publishEvent('ativoselected', values);
            }
            
            function OnGetDataAtivoBMF(values) {
                popupAtivoBMF.HideWindow();
                FDESK.LE.publishEvent('ativoselected', values);
            }
            
            function OnGetDataTituloRF(values) {
                popupTituloRF.HideWindow();
                FDESK.LE.publishEvent('ativoselected', values);
            }

    </script>

    <style type="text/css">
            /*INLINE Style*/ /*Loading*/
            #loading-mask {
                background-color: transparent;
                height: 100%;
                left: 0pt;
                position: absolute;
                top: 0pt;
                width: 100%;
                /*z-index: 20000;*/
            }
            
            #loading-outer {
                background: #fff;
                left: 40%;
                position: absolute;
                top: 40%;
                z-index: 20001;
            }
            
            .loading-progress {
                background: #e8eef6 url(imagens/layout/progress-off.gif) repeat-x;
                font-family: Verdana, arial, helvetica;
                font-size: 13px;
                font-size-adjust: none;
                font-stretch: normal;
                font-style: normal;
                font-variant: normal;
                font-weight: bold;
                line-height: normal;
                margin: 0pt;
                padding: 0pt;
                text-align: left;
                border: 1px solid #6593cf;
                width: 200px !important;
                height: 18px;
                overflow: hidden;
            }
            
            #loading-progress-on {
                background: transparent url(imagens/layout/progress-on.gif) repeat-x;
                height: 18px;
                width: 60px;
                padding: 0pt;
                margin: 0pt;
            }
            
            #loading-msg {
                background: #fff url(js/ext/resources/imagens/default/grid/loading.gif) no-repeat;
                margin: 4px 0 0 3px;
                height: 18px;
                padding: 1px 0 0 20px;
                font-family: Verdana, arial, helvetica;
                font-size: 12px;
                color: #a5a5a5;
            }
        </style>
</head>
<body id="dae-layout-container">
    <div id="loading-mask">
    </div>
    <div id="loading-outer">
        <!--
            <div class="loading-progress">
            <div id="loading-progress-on">
            </div>
            </div>
            <div id="loading-msg">
            Carregando...
            </div>
            -->
    </div>

    <script type="text/javascript" src="js/ext/adapter/ext/ext-base.js" charset="iso-8859-1">
    </script>

    <script type="text/javascript" src="js/ext/ext-all.js" charset="iso-8859-1">
    </script>

    <script type="text/javascript" src="js/util/Util.js?v=1.0" charset="iso-8859-1"></script>

    <script type="text/javascript" src="js/charts/FusionCharts.js?v=1.0" charset="iso-8859-1"></script>

    <script type="text/javascript" src="js/charts/Grafico.js?v=1.0" charset="iso-8859-1"></script>

    <script type="text/javascript" src="js/app/AppSettings.js?v=1.0" charset="iso-8859-1">
    </script>

    <script type="text/javascript" src="js/grid/GroupSummary.js?v=1.0" charset="iso-8859-1">
    </script>

    <script type="text/javascript" src="js/grid/GroupingView.js?v=1.0" charset="iso-8859-1">
    </script>

    <script type="text/javascript" src="js/grid/CarteiraSintetica.js?v=1.0" charset="iso-8859-1">
    </script>

    <script type="text/javascript" src="js/grid/ComposicaoFundo.js?v=1.0" charset="iso-8859-1">
    </script>

    <script type="text/javascript" src="js/grid/CarteiraOnline.js?v=1.0" charset="iso-8859-1">
    </script>

    <script type="text/javascript" src="js/grid/RetornoAtivos.js?v=1.0" charset="iso-8859-1">
    </script>

    <script type="text/javascript" src="js/table/BaseTable.js?v=1.0" charset="iso-8859-1">
    </script>

    <script type="text/javascript" src="js/panel/ZoomCards.js?v=1.0" charset="iso-8859-1">
    </script>

    <script type="text/javascript" src="js/panel/ZoomPosicaoCarteira.js?v=1.0" charset="iso-8859-1">
    </script>

    <script type="text/javascript" src="js/panel/ZoomIndicadores.js?v=1.0" charset="iso-8859-1">
    </script>

    <script type="text/javascript" src="js/panel/ZoomRelatorios.js?v=1.0" charset="iso-8859-1">
    </script>

    <script type="text/javascript" src="js/panel/ZoomCarteiraOnline.js?v=1.0" charset="iso-8859-1">
    </script>

    <script type="text/javascript" src="js/win/Ativo.js?v=1.0" charset="iso-8859-1">
    </script>

    <script type="text/javascript" src="js/win/SeletorPeriodo.js?v=1.0" charset="iso-8859-1">
    </script>

    <script type="text/javascript" src="js/win/Acao.js?v=1.0" charset="iso-8859-1">
    </script>

    <script type="text/javascript" src="js/win/OpcaoBolsa.js?v=1.0" charset="iso-8859-1">
    </script>

    <script type="text/javascript" src="js/win/FuturoBMF.js?v=1.0" charset="iso-8859-1">
    </script>

    <script type="text/javascript" src="js/win/TermoBolsa.js?v=1.0" charset="iso-8859-1">
    </script>

    <script type="text/javascript" src="js/win/RendaFixa.js?v=1.0" charset="iso-8859-1">
    </script>

    <script type="text/javascript" src="js/win/CotaInvestimento.js?v=1.0" charset="iso-8859-1">
    </script>

    <script type="text/javascript" src="js/win/Liquidar.js?v=1.0" charset="iso-8859-1">
    </script>

    <script type="text/javascript" src="js/win/Help.js?v=1.0" charset="iso-8859-1">
    </script>

    <script type="text/javascript" src="js/win/Usuario.js?v=1.0" charset="iso-8859-1">
    </script>

    <script type="text/javascript" src="js/win/CarteiraSimulada.js?v=1.0" charset="iso-8859-1">
    </script>

    <script type="text/javascript" src="js/win/AtivoCarteiraSimulada.js?v=1.0" charset="iso-8859-1">
    </script>

    <script type="text/javascript" src="js/layout/LayoutEnginePortal.js?v=1.0" charset="iso-8859-1">
    </script>

    <script type="text/javascript" src="js/locale/ext-lang-pt_BR.js" charset="iso-8859-1">
    </script>

    <!-- DEBUG                        
        <script type="text/javascript" src="js/build/financialdesk_portal.js?v=1.0" charset="iso-8859-1">
        </script>
        -->

    <script type="text/javascript" src="jsCustom/app/AppSettings.js?v=1.0" charset="iso-8859-1">
    </script>

    <script type="text/javascript" src="js/app/Launcher.js?v=1.0" charset="iso-8859-1">
    </script>

    <div id="x-taskbar">
       <%-- <div id="logo-client"></div> --%>
       
       <div runat="server" id="divLogoClient" class="logoClient" onprerender="divLogotipoOnPreRender"></div>

       <div id="x-header-info">
            <div id="x-info-user">
                <img alt="Usuário" src="imagens/icons/card-address.png" /><span id="x-nome-user"></span>
            </div>
            <div id="x-help">
                <a runat="server" target="_blank" id="DiretorioBaseHelp" href="">
                    <img alt="Ajuda" src="imagens/icons/question.png" />Ajuda</a>
            </div>
            <div id="x-logoff">
                <img alt="Logout" src="imagens/icons/lock--minus.png" />Logout
            </div>
        </div>
    </div>
    <form id="form1" runat="server">
        <div>
        </div>
        <cc1:esDataSource ID="EsDSCliente" runat="server" OnesSelect="EsDSCliente_esSelect" />
        <cc1:esDataSource ID="EsDSTituloRF" runat="server" OnesSelect="EsDSTituloRF_esSelect" />
        <cc1:esDataSource ID="EsDSCarteira" runat="server" OnesSelect="EsDSCarteira_esSelect" />
        <cc1:esDataSource ID="EsDSAtivoBolsa" runat="server" OnesSelect="EsDSAtivoBolsa_esSelect" />
        <cc1:esDataSource ID="EsDSAtivoBMF" runat="server" OnesSelect="EsDSAtivoBMF_esSelect" />
    </form>
</body>
</html>
