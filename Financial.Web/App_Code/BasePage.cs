﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Text.RegularExpressions;
using System.Text;
using System.Drawing;
using System.Collections.Generic;

using Financial.Web.Util;

using DevExpress.Web.Data;
using DevExpress.Web;

using Financial.Util;
using Financial.Security;
using Financial.WebConfigConfiguration;
using DevExpress.Data;
using DevExpress.Utils;
using System.Net.Mail;
using System.Web.Configuration;
using System.Net.Configuration;
using System.Globalization;
using Financial.Security.Enums;

namespace Financial.Web.Common {

    /// <summary>
    ///     Classe Base para Todas as Paginas Web
    /// </summary>
    public class BasePage : System.Web.UI.Page {
        
        /// <summary>
        /// Adiciona o script js/alertError.js em todas as paginas que tiverem ScriptManager
        /// Função: javascript.Alert para update Panel no Net Framework 4.0
        /// 
        /// Para paginas do tipo Filtro faz os buttons PDF e excel ser um postBack para resolver bug 
        /// surgido depois que clica no botão ocasiona perda dos javascripts
        /// 
        /// http://stackoverflow.com/questions/5858978/response-redirect-in-net-4-0
        /// http://forums.asp.net/t/1539851.aspx?Response+Redirect+not+working+on+an+UpdatePanel+if+redirecting+to+a+ClickOnce+application+in+some+cases+
        /// http://forums.asp.net/t/1392827.aspx
        /// http://stackoverflow.com/questions/1554728/the-message-received-from-the-server-could-not-be-parsed-common-causes-for-this
        /// http://msdn.microsoft.com/en-us/library/system.web.ui.postbacktrigger.aspx
        /// http://msdn.microsoft.com/en-us/library/system.web.ui.scriptmanager.registerasyncpostbackcontrol%28v=vs.110%29.aspx
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_PreRender(object sender, EventArgs e) {

            // If there is a ScriptManager on the page, use it.
            ScriptManager smgr = ScriptManager.GetCurrent(Page);
            if (smgr != null) {

                ScriptReference sRef = new ScriptReference();
                sRef.Path = "~/js/alertError.js";
                smgr.Scripts.Add(sRef);

                if (sender is FiltroReportBasePage) {
                    Control buttonPDF = smgr.FindControl("btnPDF");
                    if (buttonPDF != null) {
                        //smgr.RegisterPostBackControl(buttonPDF);
                    }
                    Control buttonExcel = smgr.FindControl("btnExcel");
                    if (buttonExcel != null) {
                        //smgr.RegisterPostBackControl(buttonExcel);
                    }
                }

                //smgr.AsyncPostBackErrorMessage = "teste";

            }
        }

        protected void Page_Load(object sender, EventArgs e) 
        {
            HtmlLink link = new HtmlLink();
            link.Href = "~/imagensPersonalizadas/favicon.ico";
            link.Attributes.Add("rel", "SHORTCUT ICON");
            link.Attributes.Add("type", "image/x-icon");
            this.Page.Header.Controls.Add(link);

            bool skipAuthentication = Page.Request["skipauthentication"] == "1";//usado pra abrir paginas anonimas
            if (!HttpContext.Current.User.Identity.IsAuthenticated && !skipAuthentication) {
                Response.Redirect("~/Login/LoginInit.aspx");
                return;
            }
            else {

                // Se login = master passa reto
                //if (new FinancialMembershipProvider().IsUserMaster(HttpContext.Current.User.Identity.Name)) {
                if ( Convert.ToBoolean( Session["IsUserMaster"] ) == true || skipAuthentication) {                    
                    string dirApp = DiretorioAplicacao.DiretorioBaseAplicacao.Replace("/", "");
                    Session["CurrentPage"] = dirApp == HttpContext.Current.Request.Url.Segments[1]
                                             ? HttpContext.Current.Request.Url.LocalPath.Replace(HttpContext.Current.Request.Url.Segments[0] + HttpContext.Current.Request.Url.Segments[1], "")
                                             : HttpContext.Current.Request.Url.LocalPath;
                }
                else {

                    Usuario usuario = new Usuario();
                    usuario.Query.Where(usuario.Query.Login.Equal(HttpContext.Current.User.Identity.Name));

                    if (!usuario.Query.Load()) 
                    {
                        Response.Redirect("~/Login/LoginInit.aspx");
                        return;
                    }
                    else if (usuario.DataLogout.Value > usuario.DataUltimoLogin.Value)
                    {
                        GrupoUsuario grupoUsuario = new GrupoUsuario();
                        grupoUsuario.LoadByPrimaryKey(usuario.IdGrupo.Value);

                        if (grupoUsuario.TipoPerfil.Value != (byte)TipoPerfilGrupo.Administrador)
                        {
                            Response.Redirect("~/Login/LoginInit.aspx");
                            return;
                        }
                    }
                    else
                    {
                        string dirApp = DiretorioAplicacao.DiretorioBaseAplicacao.Replace("/", "");
                        if (dirApp == HttpContext.Current.Request.Url.Segments[1]) {
                            Session["CurrentPage"] = HttpContext.Current.Request.Url.LocalPath.Replace(HttpContext.Current.Request.Url.Segments[0] + HttpContext.Current.Request.Url.Segments[1], "");
                        }
                        else {
                            Session["CurrentPage"] = HttpContext.Current.Request.Url.LocalPath;
                        }
                    }
                }

            }

            InitializeCulture();
        }

        /// <summary>
        ///     Inicializa Configurações especificas de Lingua
        ///     Função Automaticamente chamada pelo FrameWork
        /// </summary>
        protected override void InitializeCulture() {
            PersonalizeCulture.InicializaCulturePersonalizada();
            base.InitializeCulture();
        }

        /// <summary>
        /// Testa a Obrigatoriedade dos Controles
        /// </summary>
        /// <param name="controles"></param>
        /// <returns></returns>
        protected string TestaObrigatorio(List<Control> controles) {
            string retorno = "";
            foreach (Control c in controles) {
                if (c.GetType() == typeof(ASPxComboBox)) {
                    ASPxComboBox combo = c as ASPxComboBox;

                    if (combo.SelectedIndex == -1) {
                        retorno = "dev|" + combo.ID;
                        return retorno;
                    }
                }
                else if (c.GetType() == typeof(ASPxDateEdit)) {
                    ASPxDateEdit dateEdit = c as ASPxDateEdit;

                    if (dateEdit.Text.Trim() == "") {
                        retorno = "dev|" + dateEdit.ID;
                        return retorno;
                    }
                }
                else if (c.GetType() == typeof(ASPxSpinEdit)) {
                    ASPxSpinEdit spin = c as ASPxSpinEdit;

                    if (spin.Text.Trim() == "") {
                        retorno = "dev|" + spin.ID;
                        return retorno;
                    }
                }
                else if (c.GetType() == typeof(ASPxButtonEdit)) {
                    ASPxButtonEdit btnEdit = c as ASPxButtonEdit;

                    if (btnEdit.Text.Trim() == "") {
                        retorno = "dev|" + btnEdit.ID;
                        return retorno;
                    }
                }
                else if (c.GetType() == typeof(ASPxTextBox)) {
                    ASPxTextBox btnAspxTextBox = c as ASPxTextBox;

                    if (btnAspxTextBox.Text.Trim() == "") {
                        retorno = "dev|" + btnAspxTextBox.ID;
                        return retorno;
                    }
                }
                else if (c.GetType() == typeof(System.Web.UI.WebControls.TextBox)) {
                    System.Web.UI.WebControls.TextBox text = c as System.Web.UI.WebControls.TextBox;

                    if (text.Text.Trim() == "") {
                        retorno = "reg|" + text.ID;
                        return retorno;
                    }
                }
                else if (c.GetType() == typeof(ASPxGridLookup)) {
                    ASPxGridLookup btnAspxGridLookup = c as ASPxGridLookup;

                    if (btnAspxGridLookup.Text.Trim() == "") {
                        retorno = "dev|" + btnAspxGridLookup.ID;
                        return retorno;
                    }
                }
            }

            return retorno;
        }

        public void PopulaPeriodoDatas(ASPxComboBox combo)
        {
            combo.Items.Add("");
            combo.Items.Add("No Mês");
            combo.Items.Add("No Ano");
            combo.Items.Add("3 Meses");
            combo.Items.Add("6 Meses");
            combo.Items.Add("12 Meses");
            combo.Items.Add("24 Meses");

            int ano_1 = DateTime.Now.Year - 1;
            combo.Items.Add("Ano " + ano_1.ToString());

            int ano_2 = DateTime.Now.Year - 2;
            combo.Items.Add("Ano " + ano_2.ToString());

            int ano_3 = DateTime.Now.Year - 3;
            combo.Items.Add("Ano " + ano_3.ToString());

            combo.Items.Add("Desde Início");        
        }

        public void PopulaEventoContabil(ASPxComboBox combo)
        {
            combo.Items.Add("Bolsa - Valorização Ação", 100);
            combo.Items.Add("Bolsa - Reversão Valorização Ação", 101);
            combo.Items.Add("Bolsa - Desvalorização Ação", 102);
            combo.Items.Add("Bolsa - Reversão Desvalorização Ação", 103);
            combo.Items.Add("Bolsa - Valorização Ação Short", 200);
            combo.Items.Add("Bolsa - Reversão Valorização Ação Short", 201);
            combo.Items.Add("Bolsa - Desvalorização Ação Short", 202);
            combo.Items.Add("Bolsa - Reversão Desvalorização Ação Short", 203);
            combo.Items.Add("Bolsa - Valorização OpçãoCompra Comprada", 500);
            combo.Items.Add("Bolsa - Reversão Valorização OpçãoCompra Comprada", 501);
            combo.Items.Add("Bolsa - Desvalorização OpçãoCompra Comprada", 502);
            combo.Items.Add("Bolsa - Reversão Desvalorização OpçãoCompra Comprada", 503);
            combo.Items.Add("Bolsa - Valorização OpçãoCompra Vendida", 600);
            combo.Items.Add("Bolsa - Reversão Valorização OpçãoCompra Vendida", 601);
            combo.Items.Add("Bolsa - Desvalorização OpçãoCompra Vendida", 602);
            combo.Items.Add("Bolsa - Reversão Desvalorização OpçãoCompra Vendida", 603);
            combo.Items.Add("Bolsa - Valorização OpçãoVenda Comprada", 1000);
            combo.Items.Add("Bolsa - Reversão Valorização OpçãoVenda Comprada", 1001);
            combo.Items.Add("Bolsa - Desvalorização OpçãoVenda Comprada", 1002);
            combo.Items.Add("Bolsa - Reversão Desvalorização OpçãoVenda Comprada", 1003);
            combo.Items.Add("Bolsa - Valorização OpçãoVenda Vendida", 1100);
            combo.Items.Add("Bolsa - Reversão Valorização OpçãoVenda Vendida", 1101);
            combo.Items.Add("Bolsa - Desvalorização OpçãoVenda Vendida", 1102);
            combo.Items.Add("Bolsa - Reversão Desvalorização OpçãoVenda Vendida", 1103);
            combo.Items.Add("Bolsa - Compra Ação", 1500);
            combo.Items.Add("Bolsa - Compra Ação (Corretagem)", 1501);
            combo.Items.Add("Bolsa - Compra Ação (Emolumento)", 1502);
            combo.Items.Add("Bolsa - Compra Ação (Taxas CBLC)", 1503);
            combo.Items.Add("Bolsa - Venda Ação", 2000);
            combo.Items.Add("Bolsa - Venda Ação (Corretagem)", 2001);
            combo.Items.Add("Bolsa - Venda Ação (Emolumento)", 2002);
            combo.Items.Add("Bolsa - Venda Ação (Taxas CBLC)", 2003);
            combo.Items.Add("Bolsa - Compra OpçãoCompra", 2500);
            combo.Items.Add("Bolsa - Compra OpçãoCompra (Corretagem)", 2501);
            combo.Items.Add("Bolsa - Compra OpçãoCompra (Emolumento)", 2502);
            combo.Items.Add("Bolsa - Compra OpçãoCompra (Taxas CBLC)", 2503);
            combo.Items.Add("Bolsa - Venda OpçãoCompra", 3000);
            combo.Items.Add("Bolsa - Venda OpçãoCompra (Corretagem)", 3001);
            combo.Items.Add("Bolsa - Venda OpçãoCompra (Emolumento)", 3002);
            combo.Items.Add("Bolsa - Venda OpçãoCompra (Taxas CBLC)", 3003);
            combo.Items.Add("Bolsa - Compra OpçãoVenda", 3500);
            combo.Items.Add("Bolsa - Compra OpçãoVenda (Corretagem)", 3501);
            combo.Items.Add("Bolsa - Compra OpçãoVenda (Emolumento)", 3502);
            combo.Items.Add("Bolsa - Compra OpçãoVenda (Taxas CBLC)", 3503);
            combo.Items.Add("Bolsa - Venda OpçãoVenda", 4000);
            combo.Items.Add("Bolsa - Venda OpçãoVenda (Corretagem)", 4001);
            combo.Items.Add("Bolsa - Venda OpçãoVenda (Emolumento)", 4002);
            combo.Items.Add("Bolsa - Venda OpçãoVenda (Taxas CBLC)", 4003);
            combo.Items.Add("Bolsa - Lucro Ação", 4500);
            combo.Items.Add("Bolsa - Prejuízo Ação", 4501);
            combo.Items.Add("Bolsa - Lucro Compra OpçãoCompra", 4600);
            combo.Items.Add("Bolsa - Prejuízo Compra OpçãoCompra", 4601);
            combo.Items.Add("Bolsa - Lucro Venda OpçãoCompra", 4650);
            combo.Items.Add("Bolsa - Prejuízo Venda OpçãoCompra                            ", 4651);
            combo.Items.Add("Bolsa - Lucro Compra OpçãoVenda", 4700);
            combo.Items.Add("Bolsa - Prejuízo Compra OpçãoVenda", 4701);
            combo.Items.Add("Bolsa - Lucro Venda OpçãoVenda", 4750);
            combo.Items.Add("Bolsa - Prejuízo Venda OpçãoVenda", 4751);
            combo.Items.Add("Bolsa - Lucro Ação DayTrade", 5000);
            combo.Items.Add("Bolsa - Prejuízo Ação DayTrade", 5001);
            combo.Items.Add("Bolsa - Lucro Opção DayTrade", 5100);
            combo.Items.Add("Bolsa - Prejuízo Opção DayTrade", 5101);
            combo.Items.Add("Bolsa - Expiração OpçãoCompra Comprada", 5500);
            combo.Items.Add("Bolsa - Expiração OpçãoCompra Vendida", 5600);
            combo.Items.Add("Bolsa - Expiração OpçãoVenda Comprada", 5700);
            combo.Items.Add("Bolsa - Expiração OpçãoVenda Vendida                                    ", 5800);
            combo.Items.Add("Bolsa - Exercício OpçãoCompra Comprada", 6000);
            combo.Items.Add("Bolsa - Exercício OpçãoCompra Vendida", 6100);
            combo.Items.Add("Bolsa - Exercício OpçãoVenda Comprada", 6200);
            combo.Items.Add("Bolsa - Expiração OpçãoVenda Vendida                                    ", 6300);
            combo.Items.Add("Bolsa - Liquidação a Pagar", 7000);
            combo.Items.Add("Bolsa - Liquidação a Receber                        ", 7001);
            combo.Items.Add("Bolsa - Provisão Dividendos Receber", 8000);
            combo.Items.Add("Bolsa - Pagamento Dividendos Receber", 8100);
            combo.Items.Add("Bolsa - Provisão Dividendos Pagar", 8200);
            combo.Items.Add("Bolsa - Pagamento Dividendos Pagar                            ", 8300);
            combo.Items.Add("Bolsa - Provisão Juros s/ Capital Receber", 8400);
            combo.Items.Add("Bolsa - Pagamento Juros s/ Capital Receber", 8500);
            combo.Items.Add("Bolsa - Provisão Juros s/ Capital Pagar", 8600);
            combo.Items.Add("Bolsa - Pagamento Juros s/ Capital Pagar                                    ", 8700);
            combo.Items.Add("Bolsa - Provisão Rendimentos Receber", 8800);
            combo.Items.Add("Bolsa - Pagamento Rendimentos Receber", 8900);
            combo.Items.Add("Bolsa - Provisão Rendimentos Pagar", 9000);
            combo.Items.Add("Bolsa - Pagamento Rendimentos Pagar                                    ", 9100);
            combo.Items.Add("Bolsa - BTC Tomado", 10000);
            combo.Items.Add("Bolsa - BTC Tomado Devolução", 10100);
            combo.Items.Add("Bolsa - BTC Doado", 10200);
            combo.Items.Add("Bolsa - BTC Doado Devolução", 10300);
            combo.Items.Add("Bolsa - BTC Tomado Variação Positiva", 10400);
            combo.Items.Add("Bolsa - BTC Tomado Variação Positiva (Reversão)", 10401);
            combo.Items.Add("Bolsa - BTC Tomado Variação Negativa", 10500);
            combo.Items.Add("Bolsa - BTC Tomado Variação Negativa (Reversão)", 10501);
            combo.Items.Add("Bolsa - BTC Doado Variação Positiva", 10600);
            combo.Items.Add("Bolsa - BTC Doado Variação Positiva (Reversão)", 10601);
            combo.Items.Add("Bolsa - BTC Doado Variação Negativa", 10700);
            combo.Items.Add("Bolsa - BTC Doado Variação Negativa (Reversão)", 10701);
            combo.Items.Add("Bolsa - BTC Tomado Provisão Taxas", 10800);
            combo.Items.Add("Bolsa - BTC Tomado Pagto Taxas", 10900);
            combo.Items.Add("Bolsa - BTC Doado Provisão Taxas", 11000);
            combo.Items.Add("Bolsa - BTC Doado Pagto Taxas                                    ", 11100);
            combo.Items.Add("Bolsa - Compra Termo", 14000);
            combo.Items.Add("Bolsa - Compra Termo (Corretagem)", 14001);
            combo.Items.Add("Bolsa - Compra Termo (Emolumento)", 14002);
            combo.Items.Add("Bolsa - Compra Termo (Taxas CBLC)                                    ", 14003);
            combo.Items.Add("Bolsa - Venda Termo", 14500);
            combo.Items.Add("Bolsa - Venda Termo (Corretagem)", 14501);
            combo.Items.Add("Bolsa - Venda Termo (Emolumento)", 14502);
            combo.Items.Add("Bolsa - Venda Termo (Taxas CBLC)", 14503);
            combo.Items.Add("Bolsa - Liquidação Termo Comprado", 14800);
            combo.Items.Add("Bolsa - Liquidação Termo Vendido                                    ", 14850);
            combo.Items.Add("Bolsa - Valorização Termo Comprado", 15000);
            combo.Items.Add("Bolsa - Desvalorização Termo Comprado", 15010);
            combo.Items.Add("Bolsa - Valorização Termo Vendido", 15050);
            combo.Items.Add("Bolsa - Desvalorização Termo Vendido                                    ", 15060);
            combo.Items.Add("BMF - Valorização OpçãoCompra Comprada", 20000);
            combo.Items.Add("BMF - Reversão Valorização OpçãoCompra Comprada", 20001);
            combo.Items.Add("BMF - Desvalorização OpçãoCompra Comprada", 20002);
            combo.Items.Add("BMF - Reversão Desvalorização OpçãoCompra Comprada", 20003);
            combo.Items.Add("BMF - Valorização OpçãoCompra Vendida", 20100);
            combo.Items.Add("BMF - Reversão Valorização OpçãoCompra Vendida", 20101);
            combo.Items.Add("BMF - Desvalorização OpçãoCompra Vendida", 20102);
            combo.Items.Add("BMF - Reversão Desvalorização OpçãoCompra Vendida", 20103);
            combo.Items.Add("BMF - Valorização OpçãoVenda Comprada", 20500);
            combo.Items.Add("BMF - Reversão Valorização OpçãoVenda Comprada", 20501);
            combo.Items.Add("BMF - Desvalorização OpçãoVenda Comprada", 20502);
            combo.Items.Add("BMF - Reversão Desvalorização OpçãoVenda Comprada", 20503);
            combo.Items.Add("BMF - Valorização OpçãoVenda Vendida", 20600);
            combo.Items.Add("BMF - Reversão Valorização OpçãoVenda Vendida", 20601);
            combo.Items.Add("BMF - Desvalorização OpçãoVenda Vendida", 20602);
            combo.Items.Add("BMF - Reversão Desvalorização OpçãoVenda Vendida", 20603);
            combo.Items.Add("BMF - Ajuste Positivo Futuros", 21000);
            combo.Items.Add("BMF - Ajuste Negativo Futuros", 21001);
            combo.Items.Add("BMF - Futuros (Corretagem)", 21100);
            combo.Items.Add("BMF - Futuros (Emolumento)", 21101);
            combo.Items.Add("BMF - Futuros (Registro)", 21102);
            combo.Items.Add("BMF - Compra OpçãoCompra", 21500);
            combo.Items.Add("BMF - Compra OpçãoCompra (Corretagem)", 21600);
            combo.Items.Add("BMF - Compra OpçãoCompra (Emolumento)", 21601);
            combo.Items.Add("BMF - Compra OpçãoCompra (Registro)", 21602);
            combo.Items.Add("BMF - Venda OpçãoCompra", 22000);
            combo.Items.Add("BMF - Venda OpçãoCompra (Corretagem)", 22100);
            combo.Items.Add("BMF - Venda OpçãoCompra (Emolumento)", 22101);
            combo.Items.Add("BMF - Venda OpçãoCompra (Registro)", 22102);
            combo.Items.Add("BMF - Compra OpçãoVenda", 22500);
            combo.Items.Add("BMF - Compra OpçãoVenda (Corretagem)", 22600);
            combo.Items.Add("BMF - Compra OpçãoVenda (Emolumento)", 22601);
            combo.Items.Add("BMF - Compra OpçãoVenda (Registro)", 22602);
            combo.Items.Add("BMF - Venda OpçãoVenda", 23000);
            combo.Items.Add("BMF - Venda OpçãoVenda (Corretagem)", 23100);
            combo.Items.Add("BMF - Venda OpçãoVenda (Emolumento)", 23101);
            combo.Items.Add("BMF - Venda OpçãoVenda (Registro)", 23102);
            combo.Items.Add("BMF - Lucro Compra OpçãoCompra", 23500);
            combo.Items.Add("BMF - Prejuízo Compra OpçãoCompra", 23501);
            combo.Items.Add("BMF - Lucro Venda OpçãoCompra", 23550);
            combo.Items.Add("BMF - Prejuízo Venda OpçãoCompra                            ", 23551);
            combo.Items.Add("BMF - Lucro Compra OpçãoVenda", 24000);
            combo.Items.Add("BMF - Prejuízo Compra OpçãoVenda", 24001);
            combo.Items.Add("BMF - Lucro Venda OpçãoVenda", 24050);
            combo.Items.Add("BMF - Prejuízo Venda OpçãoVenda                                    ", 24051);
            combo.Items.Add("BMF - Lucro Futuro DayTrade", 25000);
            combo.Items.Add("BMF - Prejuízo Futuro DayTrade", 25001);
            combo.Items.Add("BMF - Lucro Opção DayTrade", 25100);
            combo.Items.Add("BMF - Prejuízo Opção DayTrade", 25101);
            combo.Items.Add("BMF - Liquidação a Pagar", 26000);
            combo.Items.Add("BMF - Liquidação a Receber", 26001);
            combo.Items.Add("Renda Fixa - Compra Final", 30000);
            combo.Items.Add("Renda Fixa - Venda Final", 30500);
            combo.Items.Add("Renda Fixa - Lucro Venda Final", 30600);
            combo.Items.Add("Renda Fixa - Prejuízo Venda Final", 30601);
            combo.Items.Add("Renda Fixa - Compra Compromissada", 31000);
            combo.Items.Add("Renda Fixa - Revenda", 31100);
            combo.Items.Add("Renda Fixa - Lucro Revenda", 31200);
            combo.Items.Add("Renda Fixa - Ajuste MTM Positivo", 32000);
            combo.Items.Add("Renda Fixa - Ajuste MTM Negativo", 32001);
            combo.Items.Add("Renda Fixa - Reversão Ajuste MTM Positivo", 32002);
            combo.Items.Add("Renda Fixa - Reversão Ajuste MTM Negativo", 32003);
            combo.Items.Add("Renda Fixa - Renda Positiva", 32500);
            combo.Items.Add("Renda Fixa - Renda Negativa", 32501);
            combo.Items.Add("Renda Fixa - Vencimento", 33000);
            combo.Items.Add("Renda Fixa - Pagamento Juros", 34000);
            combo.Items.Add("Renda Fixa - Pagamento Amortização", 34100);
            combo.Items.Add("Swap - Ajuste Positivo", 40000);
            combo.Items.Add("Swap - Reversão Ajuste Positivo", 40001);
            combo.Items.Add("Swap - Ajuste Negativo", 40100);
            combo.Items.Add("Swap - Reversão Ajuste Negativo", 40101);
            combo.Items.Add("Swap - Liquidação Pagar", 41000);
            combo.Items.Add("Swap - Liquidação Receber", 41001);
            combo.Items.Add("Fundos - Aplicação", 50000);
            combo.Items.Add("Fundos - Resgate", 51000);
            combo.Items.Add("Fundos - Liquidação Resgate", 51100);
            combo.Items.Add("Fundos - Valorização", 52000);
            combo.Items.Add("Fundos - Desvalorização", 52001);
            combo.Items.Add("Cotista - Aplicação Pessoa Física", 60000);
            combo.Items.Add("Cotista - Resgate (Custo) Pessoa Física", 60500);
            combo.Items.Add("Cotista - Resgate (Variação Positiva) Pessoa Física", 60600);
            combo.Items.Add("Cotista - Resgate (Variação Negativa) Pessoa Física", 60601);
            combo.Items.Add("Cotista - Resgate (Provisão IR) Pessoa Física", 60700);
            combo.Items.Add("Cotista - Resgate (Provisão IOF) Pessoa Física", 60800);
            combo.Items.Add("Cotista - Aplicação Pessoa Jurídica", 62000);
            combo.Items.Add("Cotista - Resgate (Custo) Pessoa Jurídica", 62500);
            combo.Items.Add("Cotista - Resgate (Variação Positiva) Pessoa Jurídica", 62600);
            combo.Items.Add("Cotista - Resgate (Variação Negativa) Pessoa Jurídica", 62601);
            combo.Items.Add("Cotista - Resgate (Provisão IR) Pessoa Jurídica", 62700);
            combo.Items.Add("Cotista - Resgate (Provisão IOF) Pessoa Jurídica", 62800);
            combo.Items.Add("Cotista - Liquidação Resgate", 64000);
            combo.Items.Add("Cotista - Liquidação Resgate IR", 64100);
            combo.Items.Add("Cotista - Liquidação Resgate IOF", 64200);
            combo.Items.Add("Despesas - Provisão", 80000);
            combo.Items.Add("Despesas - Pagamento", 80100);
            combo.Items.Add("Zeragem Lucro", 200000);
            combo.Items.Add("Zeragem Prejuizo", 200001);
            combo.Items.Add("Outros", 999999);
        }
    }

    /// <summary>
    ///     Classe Base para Todas as Paginas Web
    /// </summary>
    public class FiltroReportBasePage : BasePage {
        #region Atributos e Properties da classe
        protected class VisaoRelatorio {
            public static string visaoHTML = "Report";
            public static string visaoPDF = "PDF";
            public static string visaoExcel = "Excel";
        }

        private PlaceHolder ph1; //Cria PlaceHolder genérico para abrigar qq tipo de controle gerado em run-time        
        #region Atributos e properties para controle de popups
        bool hasPopupPessoa = false;
        bool hasPopupCliente = false;
        bool hasPopupCarteira = false;
        bool hasPopupCotista = false;
        bool hasPopupAtivoBolsa = false;
        bool hasPopupAtivoBMF = false;
        bool hasPopupTituloRF = false;
        bool hasPopupContabConta = false;


        public bool HasPopupPessoa {
            get { return hasPopupPessoa; }
            set { hasPopupPessoa = value; }
        }

        public bool HasPopupCliente {
            get { return hasPopupCliente; }
            set { hasPopupCliente = value; }
        }

        public bool HasPopupCarteira {
            get { return hasPopupCarteira; }
            set { hasPopupCarteira = value; }
        }

        public bool HasPopupCotista {
            get { return hasPopupCotista; }
            set { hasPopupCotista = value; }
        }

        public bool HasPopupAtivoBolsa {
            get { return hasPopupAtivoBolsa; }
            set { hasPopupAtivoBolsa = value; }
        }

        public bool HasPopupAtivoBMF {
            get { return hasPopupAtivoBMF; }
            set { hasPopupAtivoBMF = value; }
        }

        public bool HasPopupTituloRF {
            get { return hasPopupTituloRF; }
            set { hasPopupTituloRF = value; }
        }

        public bool HasPopupContabConta
        {
            get { return hasPopupContabConta; }
            set { hasPopupContabConta = value; }
        }
        #endregion

        #endregion

        new protected void Page_Load(object sender, EventArgs e) {
            base.Page_Load(sender, e);

            //Cria o placeholder no form
            this.ph1 = new PlaceHolder();
            this.FindControl("form1").Controls.Add(this.ph1);
            //

            CriaBrowserPopups();
        }

        #region gridConsulta, gridAtivo, gridCliente, gridCarteira, gridCotista
        protected void gridAtivo_HtmlRowCreated(object sender, ASPxGridViewTableRowEventArgs e) {
            this.SetaCoresGrid(e);
        }

        protected void gridCliente_HtmlRowCreated(object sender, ASPxGridViewTableRowEventArgs e) {
            this.SetaCoresGrid(e);
        }

        protected void gridCliente_CustomDataCallback(object sender, ASPxGridViewCustomDataCallbackEventArgs e) {
            ASPxGridView gridView = sender as ASPxGridView;
            e.Result = gridView.GetRowValues(Convert.ToInt32(e.Parameters), "IdCliente");
        }

        protected void gridCarteira_HtmlRowCreated(object sender, ASPxGridViewTableRowEventArgs e) {
            this.SetaCoresGrid(e);
        }

        protected void gridCarteira_CustomDataCallback(object sender, ASPxGridViewCustomDataCallbackEventArgs e) {
            ASPxGridView gridView = sender as ASPxGridView;
            e.Result = gridView.GetRowValues(Convert.ToInt32(e.Parameters), "IdCarteira");
        }

        protected void gridCotista_HtmlRowCreated(object sender, ASPxGridViewTableRowEventArgs e) {
            this.SetaCoresGrid(e);
        }

        protected void gridCotista_CustomDataCallback(object sender, ASPxGridViewCustomDataCallbackEventArgs e) {
            ASPxGridView gridView = sender as ASPxGridView;
            e.Result = gridView.GetRowValues(Convert.ToInt32(e.Parameters), "IdCotista");
        }

        
        #endregion

        protected void SetaCoresGrid(ASPxGridViewTableRowEventArgs e) {
            e.Row.Attributes.Add("onmouseover", "backgroundColor = this.style.backgroundColor; this.style.backgroundColor='#E0E9EB';");
            e.Row.Attributes.Add("onmouseout", "this.style.backgroundColor=backgroundColor;");
        }

        private void CriaBrowserPopups() {
            BrowserPopups popups = new BrowserPopups();

            if (this.hasPopupPessoa) {
                ASPxPopupControl popup = popups.PopupPessoa(false);
                ASPxPopupControl popupMensagem = popups.PopupMensagemPessoa();
                ph1.Controls.Add(popupMensagem);
                ph1.Controls.Add(popup);
            }
            if (this.hasPopupCliente) {
                ASPxPopupControl popup = popups.PopupCliente(false);
                ASPxPopupControl popupMensagem = popups.PopupMensagemCliente();
                ph1.Controls.Add(popupMensagem);
                ph1.Controls.Add(popup);
            }
            if (this.hasPopupCarteira) {
                ASPxPopupControl popup = popups.PopupCarteira(false);
                ASPxPopupControl popupMensagem = popups.PopupMensagemCarteira();
                ph1.Controls.Add(popupMensagem);
                ph1.Controls.Add(popup);
            }
            if (this.hasPopupCotista) {
                ASPxPopupControl popup = popups.PopupCotista(false);
                ASPxPopupControl popupMensagem = popups.PopupMensagemCotista();
                ph1.Controls.Add(popupMensagem);
                ph1.Controls.Add(popup);
            }
            if (this.hasPopupAtivoBolsa) {
                ASPxPopupControl popup = popups.PopupAtivoBolsa(false);
                ASPxPopupControl popupMensagem = popups.PopupMensagemAtivoBolsa();
                ph1.Controls.Add(popupMensagem);
                ph1.Controls.Add(popup);
            }
            if (this.hasPopupAtivoBMF) {
                ASPxPopupControl popup = popups.PopupAtivoBMF();
                ASPxPopupControl popupMensagem = popups.PopupMensagemAtivoBMF();
                ph1.Controls.Add(popupMensagem);
                ph1.Controls.Add(popup);
            }
            if (this.hasPopupTituloRF) {
                ASPxPopupControl popup = popups.PopupTituloRF();
                ASPxPopupControl popupMensagem = popups.PopupMensagemTituloRF();
                ph1.Controls.Add(popupMensagem);
                ph1.Controls.Add(popup);
            }
            if (this.hasPopupContabConta)
            {
                ASPxPopupControl popup = popups.PopupContabConta();
                ASPxPopupControl popupMensagem = popups.PopupMensagemContabConta();
                ph1.Controls.Add(popupMensagem);
                ph1.Controls.Add(popup);
            }

            List<System.Web.UI.WebControls.TextBox> listaMensagens = popups.ListaMensagens();
            this.ph1.Controls.Add((System.Web.UI.WebControls.TextBox)listaMensagens[0]);
            this.ph1.Controls.Add((System.Web.UI.WebControls.TextBox)listaMensagens[1]);
            this.ph1.Controls.Add((System.Web.UI.WebControls.TextBox)listaMensagens[2]);
            this.ph1.Controls.Add((System.Web.UI.WebControls.TextBox)listaMensagens[3]);
        }
    }

    /// <summary>
    ///     Classe Base para Todas as Paginas Web
    /// </summary>
    public class ConsultaBasePage : BasePage {

        [Obsolete("Uso depreciado pois agora é feito Usado estaticamente")]
        public class CommandColumnHeaderTemplate : ITemplate {
            ASPxGridView gridView = null;

            //public CommandColumnHeaderTemplate(ASPxGridView gridView) {
            //this.gridView = gridView;
            //}

            public void InstantiateIn(Control container) {
                //ASPxCheckBox checkBox2 = new ASPxCheckBox();
                //checkBox2.ClientInstanceName = "cbAll";
                //checkBox2.BackColor = Color.White;
                //checkBox2.Init += new EventHandler(cbAll_Init);
                //checkBox2.ClientSideEvents.CheckedChanged = "function(s, e) {OnAllCheckedChanged(s, e, gridConsulta);}";
                //container.Controls.Add(checkBox2);
            }

            #region Eventos para check múltiplo
            //protected void cbAll_Init(object sender, EventArgs e)
            //{
            //    ASPxCheckBox chk = sender as ASPxCheckBox;
            //    ASPxGridView grid = (chk.NamingContainer as GridViewHeaderTemplateContainer).Grid;

            //    chk.Checked = grid.VisibleRowCount == 0
            //                  ? false
            //                  : (grid.Selection.Count == grid.VisibleRowCount);
            //}

            //protected void cbPage_Init(object sender, EventArgs e)
            //{
            //    ASPxCheckBox chk = sender as ASPxCheckBox;
            //    ASPxGridView grid = (chk.NamingContainer as GridViewHeaderTemplateContainer).Grid;

            //    Boolean cbChecked = true;
            //    Int32 start = grid.VisibleStartIndex;
            //    Int32 end = grid.VisibleStartIndex + grid.SettingsPager.PageSize;
            //    end = (end > grid.VisibleRowCount ? grid.VisibleRowCount : end);

            //    for (int i = start; i < end; i++)
            //        if (!grid.Selection.IsRowSelected(i))
            //        {
            //            cbChecked = false;
            //            break;
            //        }

            //    chk.Checked = cbChecked;
            //}
            #endregion
        }

        #region Atributos e Properties da classe
        protected class VisaoRelatorio {
            public static string visaoHTML = "Report";
            public static string visaoPDF = "PDF";
            public static string visaoExcel = "Excel";
        }

        private PlaceHolder ph1; //Cria PlaceHolder genérico para abrigar qq tipo de controle gerado em run-time
        private ASPxGridView gridConsulta; //Busca a partir de FindControl na página
        private ASPxGridViewExporter gridExport; //Busca a partir de FindControl na página

        #region Atributos e properties para controle de popups
        bool hasPopupPessoa = false;
        bool hasPopupCliente = false;
        bool hasPopupCarteira = false;
        bool hasPopupCotista = false;
        bool hasPopupAtivoBolsa = false;
        bool hasPopupAtivoBolsa2 = false;
        bool hasPopupAtivoBMF = false;
        bool hasPopupTituloRF = false;

        public bool HasPopupPessoa {
            get { return hasPopupPessoa; }
            set { hasPopupPessoa = value; }
        }

        public bool HasPopupCliente {
            get { return hasPopupCliente; }
            set { hasPopupCliente = value; }
        }

        public bool HasPopupCarteira {
            get { return hasPopupCarteira; }
            set { hasPopupCarteira = value; }
        }

        public bool HasPopupCotista {
            get { return hasPopupCotista; }
            set { hasPopupCotista = value; }
        }

        public bool HasPopupAtivoBolsa {
            get { return hasPopupAtivoBolsa; }
            set { hasPopupAtivoBolsa = value; }
        }

        public bool HasPopupAtivoBolsa2 {
            get { return hasPopupAtivoBolsa2; }
            set { hasPopupAtivoBolsa2 = value; }
        }

        public bool HasPopupAtivoBMF {
            get { return hasPopupAtivoBMF; }
            set { hasPopupAtivoBMF = value; }
        }

        public bool HasPopupTituloRF {
            get { return hasPopupTituloRF; }
            set { hasPopupTituloRF = value; }
        }
        #endregion
        #endregion

        new protected void Page_Load(object sender, EventArgs e) {
            base.Page_Load(sender, e);

            ASPxGridView gridConsulta = this.FindControl("gridConsulta") as ASPxGridView;
            ASPxGridViewExporter gridExport = this.FindControl("gridExport") as ASPxGridViewExporter;

            if (gridConsulta != null) {

                this.gridExport = gridExport;
                this.gridConsulta = gridConsulta;

                //Cria o placeholder no form
                this.ph1 = new PlaceHolder();
                this.FindControl("form1").Controls.Add(this.ph1);
                //

                InicializaGrid();
                CriaBrowserPopups();
            }
        }

        #region gridConsulta, gridAtivo, gridCliente, gridCarteira, gridCotista
        protected void gridConsulta_HtmlRowCreated(object sender, ASPxGridViewTableRowEventArgs e) {
            this.SetaCoresGrid(e);
        }

        // Coloca Tooltip nas linhas do grid. Colunas com ListBox não são colocadas. Devem ser feitas manualmente
        protected void gridConsulta_HtmlDataCellPrepared(object sender, ASPxGridViewTableDataCellEventArgs e)
        {
            if (e.CellValue != null)
            {
                if (e.DataColumn is GridViewDataComboBoxColumn)
                {
                    return;
                }
                else
                {
                    string tooltip = e.CellValue is DateTime
                                     ? (Convert.ToDateTime(e.CellValue)).ToString("d")
                                     : e.CellValue.ToString().Trim();

                    e.Cell.Attributes.Add("title", tooltip);
                }
            }            
        }

        protected void gridAtivo_HtmlRowCreated(object sender, ASPxGridViewTableRowEventArgs e) {
            this.SetaCoresGrid(e);
        }

        protected void gridCliente_HtmlRowCreated(object sender, ASPxGridViewTableRowEventArgs e) {
            this.SetaCoresGrid(e);
        }

        protected void gridCliente_CustomDataCallback(object sender, ASPxGridViewCustomDataCallbackEventArgs e) {
            ASPxGridView gridView = sender as ASPxGridView;
            e.Result = gridView.GetRowValues(Convert.ToInt32(e.Parameters), "IdCliente");
        }

        protected void gridCarteira_HtmlRowCreated(object sender, ASPxGridViewTableRowEventArgs e) {
            this.SetaCoresGrid(e);
        }

        protected void gridCarteira_CustomDataCallback(object sender, ASPxGridViewCustomDataCallbackEventArgs e) {
            ASPxGridView gridView = sender as ASPxGridView;
            e.Result = gridView.GetRowValues(Convert.ToInt32(e.Parameters), "IdCarteira");
        }

        protected void gridCotista_HtmlRowCreated(object sender, ASPxGridViewTableRowEventArgs e) {
            this.SetaCoresGrid(e);
        }

        protected void gridCotista_CustomDataCallback(object sender, ASPxGridViewCustomDataCallbackEventArgs e) {
            ASPxGridView gridView = sender as ASPxGridView;
            e.Result = gridView.GetRowValues(Convert.ToInt32(e.Parameters), "IdCotista");
        }


        #endregion

        protected void SetaCoresGrid(ASPxGridViewTableRowEventArgs e) {
            e.Row.Attributes.Add("onmouseover", "backgroundColor = this.style.backgroundColor; this.style.backgroundColor='#E0E9EB';");
            e.Row.Attributes.Add("onmouseout", "this.style.backgroundColor=backgroundColor;");
        }

        private void InicializaGrid() {
            #region Properties básicas
            this.gridConsulta.ClientInstanceName = "gridConsulta";
            this.gridConsulta.AutoGenerateColumns = false;
            this.gridConsulta.Width = Unit.Percentage(100);
            this.gridConsulta.EnableViewState = false;

            //Troca de Like para Contains nos campos texto do grid
            UtilitarioGrid.GridFilterContains(gridConsulta);

            //Pager
            this.gridConsulta.SettingsPager.PageSize = 30;

            //Settings geral
            this.gridConsulta.Settings.ShowFilterRow = true;
            this.gridConsulta.Settings.ShowStatusBar = GridViewStatusBarMode.Visible;
            this.gridConsulta.Settings.VerticalScrollBarMode = ScrollBarMode.Visible;
            this.gridConsulta.Settings.VerticalScrollableHeight = 260;

            foreach (GridViewColumn column in this.gridConsulta.Columns) {

                // Se for Coluna ComboBox coloca a possibilidade de digitar
                if (column is GridViewDataComboBoxColumn) {
                    ((GridViewDataComboBoxColumn)column).PropertiesComboBox.DropDownStyle = DropDownStyle.DropDown;
                    ((GridViewDataComboBoxColumn)column).PropertiesComboBox.IncrementalFilteringMode = IncrementalFilteringMode.Contains;
                }

                if (column is GridViewDataDateColumn)
                {
                    ((GridViewDataDateColumn)column).PropertiesDateEdit.ClearButton.DisplayMode = ClearButtonDisplayMode.Never;
                }

                GridViewDataColumn coluna = column as GridViewDataColumn;
                if (coluna != null) {                                                                               
                    coluna.Settings.AllowAutoFilterTextInputTimer = DefaultBoolean.False;
                }
            }

            this.gridConsulta.Settings.VerticalScrollBarStyle = WebConfig.AppSettings.RolagemVirtual
                                                                ? GridViewVerticalScrollBarStyle.Virtual
                                                                : GridViewVerticalScrollBarStyle.Standard;

            //SettingsText
            this.gridConsulta.SettingsText.EmptyDataRow = "0 registros";
            this.gridConsulta.SettingsText.PopupEditFormCaption = " ";
            this.gridConsulta.SettingsLoadingPanel.Text = "Carregando...";

            //Images/Styles
            //this.gridConsulta.Images.ImageFolder = "~/App_Themes/Glass/{0}/";
            //this.gridConsulta.Styles.CssFilePath = "~/App_Themes/Glass/{0}/styles.css";
            //this.gridConsulta.Styles.CssPostfix = "Glass";

            //this.gridConsulta.EnableCallBacks = false;            
            //this.gridConsulta.CssFilePath = "~/App_Themes/Glass/{0}/styles.css";
            //this.gridConsulta.CssPostfix = "Glass";
            //this.gridConsulta.DataSourceForceStandardPaging = true; 

            this.gridConsulta.Styles.Header.SortingImageSpacing = Unit.Pixel(5);
            this.gridConsulta.Styles.AlternatingRow.Enabled = DefaultBoolean.True;
            this.gridConsulta.Styles.Cell.Wrap = DefaultBoolean.False;
            this.gridConsulta.Styles.CommandColumn.Cursor = "hand";

            #endregion

            //Eventos comuns
            this.gridConsulta.HtmlRowCreated += new ASPxGridViewTableRowEventHandler(UtilitarioGrid.Grid_HtmlRowCreated);

            this.gridConsulta.HtmlDataCellPrepared += new ASPxGridViewTableDataCellEventHandler(gridConsulta_HtmlDataCellPrepared);
        }

        private void CriaBrowserPopups() {
            BrowserPopups popups = new BrowserPopups();

            if (this.hasPopupPessoa) {
                ASPxPopupControl popup = popups.PopupPessoa(false);
                ASPxPopupControl popupMensagem = popups.PopupMensagemPessoa();
                ph1.Controls.Add(popupMensagem);
                ph1.Controls.Add(popup);
            }
            if (this.hasPopupCliente) {
                ASPxPopupControl popup = popups.PopupCliente(false);
                ASPxPopupControl popupMensagem = popups.PopupMensagemCliente();
                ph1.Controls.Add(popupMensagem);
                ph1.Controls.Add(popup);
            }
            if (this.hasPopupCarteira) {
                ASPxPopupControl popup = popups.PopupCarteira(false);
                ASPxPopupControl popupMensagem = popups.PopupMensagemCarteira();
                ph1.Controls.Add(popupMensagem);
                ph1.Controls.Add(popup);
            }
            if (this.hasPopupCotista) {
                ASPxPopupControl popup = popups.PopupCotista(false);
                ASPxPopupControl popupMensagem = popups.PopupMensagemCotista();
                ph1.Controls.Add(popupMensagem);
                ph1.Controls.Add(popup);
            }
            if (this.hasPopupAtivoBolsa) {
                ASPxPopupControl popup = popups.PopupAtivoBolsa(this.hasPopupAtivoBolsa2);
                ASPxPopupControl popupMensagem = popups.PopupMensagemAtivoBolsa();
                ph1.Controls.Add(popupMensagem);
                ph1.Controls.Add(popup);
            }
            if (this.hasPopupAtivoBMF) {
                ASPxPopupControl popup = popups.PopupAtivoBMF();
                ASPxPopupControl popupMensagem = popups.PopupMensagemAtivoBMF();
                ph1.Controls.Add(popupMensagem);
                ph1.Controls.Add(popup);
            }
            if (this.hasPopupTituloRF) {
                ASPxPopupControl popup = popups.PopupTituloRF();
                ASPxPopupControl popupMensagem = popups.PopupMensagemTituloRF();
                ph1.Controls.Add(popupMensagem);
                ph1.Controls.Add(popup);
            }

            List<System.Web.UI.WebControls.TextBox> listaMensagens = popups.ListaMensagens();
            this.ph1.Controls.Add((System.Web.UI.WebControls.TextBox)listaMensagens[0]);
            this.ph1.Controls.Add((System.Web.UI.WebControls.TextBox)listaMensagens[1]);
            this.ph1.Controls.Add((System.Web.UI.WebControls.TextBox)listaMensagens[2]);
            this.ph1.Controls.Add((System.Web.UI.WebControls.TextBox)listaMensagens[3]);
        }

        #region Evento do Combo para selecionar todos os CheckBoxs
        /// <summary>
        /// Função para selecionar todos os checkboxs de um Grid 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void CheckBoxSelectAll(object sender, EventArgs e) {
            ASPxCheckBox chk = sender as ASPxCheckBox;
            ASPxGridView grid = (chk.NamingContainer as GridViewHeaderTemplateContainer).Grid;
            chk.Checked = (grid.Selection.Count == grid.VisibleRowCount);
        }
        #endregion

        #region btnRun_Click, btnPDF_Click, btnExcel_Click
        protected void btnRun_Click(object sender, EventArgs e) {
            gridConsulta.DataBind();
        }

        protected void btnPDF_Click(object sender, EventArgs e) {
            gridExport.WritePdfToResponse();
        }

        protected void btnExcel_Click(object sender, EventArgs e) {
            gridExport.WriteXlsToResponse();
        }
        #endregion
    }

    /// <summary>
    ///     Classe Base para Todas as Paginas Web
    /// </summary>
    public class CadastroBasePage : BasePage {

        [Obsolete("Uso depreciado pois agora é feito Usado estaticamente")]
        public class CommandColumnHeaderTemplate : ITemplate {
            ASPxGridView gridView = null;

            [Obsolete("Não mais usado")]
            public CommandColumnHeaderTemplate(ASPxGridView gridView) {
                //this.gridView = gridView;
            }

            public void InstantiateIn(Control container) {
                //                ASPxCheckBox checkBox2 = new ASPxCheckBox();
                //                checkBox2.ClientInstanceName = "cbAll";
                //                checkBox2.BackColor = Color.White;
                //checkBox2.Init += new EventHandler(cbAll_Init);
                //checkBox2.ClientSideEvents.CheckedChanged = "function(s, e) {OnAllCheckedChanged(s, e, gridCadastro);}";
                //                container.Controls.Add(checkBox2);
            }

            //#region Eventos para check múltiplo
            //protected void cbAll_Init(object sender, EventArgs e)
            //{
            //    ASPxCheckBox chk = sender as ASPxCheckBox;
            //    ASPxGridView grid = (chk.NamingContainer as GridViewHeaderTemplateContainer).Grid;
            //    chk.Checked = (grid.Selection.Count == grid.VisibleRowCount);
            //}

            //protected void cbPage_Init(object sender, EventArgs e)
            //{
            //    ASPxCheckBox chk = sender as ASPxCheckBox;
            //    ASPxGridView grid = (chk.NamingContainer as GridViewHeaderTemplateContainer).Grid;

            //    Boolean cbChecked = true;
            //    Int32 start = grid.VisibleStartIndex;
            //    Int32 end = grid.VisibleStartIndex + grid.SettingsPager.PageSize;
            //    end = (end > grid.VisibleRowCount ? grid.VisibleRowCount : end);

            //    for (int i = start; i < end; i++)
            //        if (!grid.Selection.IsRowSelected(i))
            //        {
            //            cbChecked = false;
            //            break;
            //        }

            //    chk.Checked = cbChecked;
            //}
            //#endregion
        }

        #region Atributos e Properties da classe
        private PlaceHolder ph1; //Cria PlaceHolder genérico para abrigar qq tipo de controle gerado em run-time
        private ASPxGridView gridCadastro; //Busca a partir de FindControl na página
        private ASPxGridViewExporter gridExport; //Busca a partir de FindControl na página

        private decimal gridTotalSummary = 0.00M; // Guarda o Total para o Sum do Grid

        #region Atributo e Enum - tipoCadastro
        private enum TipoCadastroPagina {
            Inline = 1,
            Popup = 2
        }

        private byte tipoCadastro;
        #endregion

        #region Properties para Foco no Grid
        /// <summary>
        /// Id do campo que recebe o Foco no Grid
        /// </summary>
        public class FocoGrid {
            private string campo1 = String.Empty;
            private string campo2 = String.Empty;

            /* Indica se Campo do Foco está contido num TabPage */
            private bool tabPage = false;

            // Construtor
            public FocoGrid() {
            }

            // Construtor
            public FocoGrid(string campo1) {
                this.campo1 = campo1;
                this.tabPage = false;
            }

            public FocoGrid(string campo1, string campo2) {
                this.campo1 = campo1;
                this.campo2 = campo2;
                this.tabPage = false;
            }

            public string Campo1 {
                get { return this.campo1; }
            }

            public string Campo2 {
                get { return this.campo2; }
            }

            public bool TabPage {
                get { return this.tabPage; }
                set { tabPage = value; }
            }
        }
        private FocoGrid focoGrid = new FocoGrid();
        #endregion

        #region Atributos e properties para controle de popups
        bool hasPopupPessoa = false;
        bool hasPopupCliente = false;
        bool hasPopupCliente1 = false;
        bool hasPopupCarteira = false;
        bool hasPopupCarteira1 = false;
        bool hasPopupCarteira2= false;
        bool hasPopupCotista = false;
        bool hasPopupAtivoBolsa = false;
        bool hasPopupAtivoBolsa2 = false;
        bool hasPopupAtivoBMF = false;
        bool hasPopupTituloRF = false;
        bool hasPopupEmissor = false;
        bool hasPopupContabConta = false;
        bool hasPopupAgenteMercado = false;

        public bool HasPopupPessoa {
            get { return hasPopupPessoa; }
            set { hasPopupPessoa = value; }
        }

        public bool HasPopupCliente {
            get { return hasPopupCliente; }
            set { hasPopupCliente = value; }
        }

        public bool HasPopupCliente1 {
            get { return hasPopupCliente1; }
            set { hasPopupCliente1 = value; }
        }

        public bool HasPopupCarteira {
            get { return hasPopupCarteira; }
            set { hasPopupCarteira = value; }
        }

        public bool HasPopupCarteira1 {
            get { return hasPopupCarteira1; }
            set { hasPopupCarteira1 = value; }
        }

        public bool HasPopupCarteira2
        {
            get { return hasPopupCarteira2; }
            set { hasPopupCarteira2 = value; }
        }

        public bool HasPopupCotista {
            get { return hasPopupCotista; }
            set { hasPopupCotista = value; }
        }

        public bool HasPopupAtivoBolsa {
            get { return hasPopupAtivoBolsa; }
            set { hasPopupAtivoBolsa = value; }
        }

        public bool HasPopupAtivoBolsa2 {
            get { return hasPopupAtivoBolsa2; }
            set { hasPopupAtivoBolsa2 = value; }
        }

        public bool HasPopupAtivoBMF {
            get { return hasPopupAtivoBMF; }
            set { hasPopupAtivoBMF = value; }
        }

        public bool HasPopupTituloRF {
            get { return hasPopupTituloRF; }
            set { hasPopupTituloRF = value; }
        }

        public bool HasPopupEmissor {
            get { return hasPopupEmissor; }
            set { hasPopupEmissor = value; }
        }

        public bool HasPopupContabConta
        {
            get { return hasPopupContabConta; }
            set { hasPopupContabConta = value; }
        }

        public bool HasPopupAgenteMercado
        {
            get { return hasPopupAgenteMercado; }
            set { hasPopupAgenteMercado = value; }
        }
        #endregion

        #region Atributo e propery - MsgCampoObrigatorio
        private string msgCampoObrigatorio = "Campo obrigatório";
        public string MsgCampoObrigatorio {
            get { return msgCampoObrigatorio; }
            set { msgCampoObrigatorio = value; }
        }
        #endregion

        #region Atributo e propery - HasFiltro
        private bool hasFiltro = false;
        public bool HasFiltro {
            get { return hasFiltro; }
            set { hasFiltro = value; }
        }
        #endregion

        #region Atributo e propery - HasPanelFieldsLoading
        private bool hasPanelFieldsLoading = false;
        public bool HasPanelFieldsLoading {
            get { return hasPanelFieldsLoading; }
            set { hasPanelFieldsLoading = value; }
        }
        #endregion

        #region Atributo e propery - HasEditControl (default = true)
        private bool hasEditControl = true;
        public bool HasEditControl {
            get { return hasEditControl; }
            set { hasEditControl = value; }
        }
        #endregion

        #region Atributo e Property AllowUpdate
        private bool allowUpdate = true;
        public bool AllowUpdate {
            get { return allowUpdate; }
            set { allowUpdate = value; }
        }
        #endregion

        #region Atributo e Property AllowDelete
        private bool allowDelete = true;
        public bool AllowDelete {
            get { return allowDelete; }
            set { allowDelete = value; }
        }
        #endregion

        #region Atributo e Property AllowDelete
        private bool hasSummary = false;
        public bool HasSummary
        {
            get { return hasSummary; }
            set { hasSummary = value; }
        }
        #endregion
        #endregion

        /// <summary>
        /// Usado quando a pagina de cadastro está dentro de um tabPage
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// <param name="gridCadastro">GridCadastro</param>
        protected void Page_Load(object sender, EventArgs e, ASPxGridView gridCadastro) {
            base.Page_Load(sender, e);
            
            // Procura o grid dentro do pageControl                                 
            ASPxGridViewExporter gridExport = this.FindControl("gridExport") as ASPxGridViewExporter;

            this.gridExport = gridExport;
            this.gridCadastro = gridCadastro;

            this.tipoCadastro = this.gridCadastro.Templates.EditForm != null
                                ? (byte)TipoCadastroPagina.Popup
                                : (byte)TipoCadastroPagina.Inline;

            //Cria o placeholder no form
            this.ph1 = new PlaceHolder();
            this.FindControl("form1").Controls.Add(this.ph1);
            //

            this.InicializaGrid();
            this.InicializaValidacaoForm();
            this.CriaBrowserPopups();
            //
            //this.RepairGridViewTemplateColumns();
            //
            if (!IsPostBack && !IsCallback) {
                InicializaFiltro();
            }

            if (this.hasSummary) {
                this.AddGridTotalSummary();
            }
        }

        new protected void Page_Load(object sender, EventArgs e) {
            base.Page_Load(sender, e);

            ASPxGridView gridCadastro = this.FindControl("gridCadastro") as ASPxGridView;
            ASPxGridViewExporter gridExport = this.FindControl("gridExport") as ASPxGridViewExporter;

            this.gridExport = gridExport;
            this.gridCadastro = gridCadastro;

            this.tipoCadastro = this.gridCadastro.Templates.EditForm != null
                                ? (byte)TipoCadastroPagina.Popup
                                : (byte)TipoCadastroPagina.Inline;

            //Cria o placeholder no form
            this.ph1 = new PlaceHolder();
            this.FindControl("form1").Controls.Add(this.ph1);
            //

            this.InicializaGrid();
            this.InicializaValidacaoForm();
            this.CriaBrowserPopups();
            //
            //this.RepairGridViewTemplateColumns();
            //
            if (!IsPostBack && !IsCallback) {
                InicializaFiltro();
            }

            if (this.hasSummary)
            {
                this.AddGridTotalSummary();
            }
        }

        #region gridCadastro Events
        //Joga variáveis de server-side para o client-side
        protected void gridCadastro_CustomJSProperties(object sender, ASPxGridViewClientJSPropertiesEventArgs e) {
            if (gridCadastro.IsNewRowEditing) {
                e.Properties["cp_EditVisibleIndex"] = "new";
            }
            else {
                e.Properties["cp_EditVisibleIndex"] = gridCadastro.EditingRowVisibleIndex.ToString();
            }

            ASPxGridView gridView = sender as ASPxGridView;
            if (gridView.IsEditing) {
                bool newRow = false;
                if (gridView.IsNewRowEditing) {
                    newRow = true;
                }
                e.Properties["cpNewRow"] = newRow;
            }

            //Variáveis para check múltiplo
            Int32 start = gridView.VisibleStartIndex;
            Int32 end = gridView.VisibleStartIndex + gridView.SettingsPager.PageSize;
            Int32 selectNumbers = 0;
            end = (end > gridView.VisibleRowCount ? gridView.VisibleRowCount : end);

            for (int i = start; i < end; i++)
                if (gridView.Selection.IsRowSelected(i))
                    selectNumbers++;

            e.Properties["cpSelectedRowsOnPage"] = selectNumbers;
            e.Properties["cpVisibleRowCount"] = gridView.VisibleRowCount;
            //
        }

        //JScript p pintar linhas alternadas
        protected void gridCadastro_HtmlRowCreated(object sender, ASPxGridViewTableRowEventArgs e) {
            UtilitarioGrid.SetaCoresGrid(e);
        }

        //Trata validação de campos obrigatórios
        protected void gridCadastro_CellEditorInitialize(object sender, ASPxGridViewEditorEventArgs e) {
            if ((e.Editor as ASPxTextEdit).ValidationSettings.RequiredField.ErrorText != "") {
                (e.Editor as ASPxTextEdit).ValidationSettings.RequiredField.IsRequired = true;
                (e.Editor as ASPxTextEdit).ValidationSettings.RequiredField.ErrorText = "Campo obrigatório";
            }

            if (gridCadastro != null) {
                if (gridCadastro.IsEditing && e.Column.FieldName == GetFirstColumnFieldName(gridCadastro)) {
                    e.Editor.Focus();
                }
            }
        }

        // Coloca Tooltip nas linhas do grid. Colunas com ListBox não são colocadas. Devem ser feitas manualmente
        protected void gridCadastro_HtmlDataCellPrepared(object sender, ASPxGridViewTableDataCellEventArgs e) {
            if (e.CellValue != null) {
                if (e.DataColumn is GridViewDataComboBoxColumn) {
                    return;
                }
                else {
                    string tooltip = e.CellValue is DateTime
                                     ? (Convert.ToDateTime(e.CellValue)).ToString("d")
                                     : e.CellValue.ToString().Trim();

                    e.Cell.Attributes.Add("title", tooltip);
                }
            }
            //e.Cell.Attributes.Add("title", Convert.ToString( e.GetValue(e.DataColumn.FieldName) ).Trim() );
        }

        /// <summary>
        /// Baseado no modo do grid: Insert ou Update
        /// Define qual o campo do Grid que vai receber o Foco
        /// Se o campo do Foco estiver num tabPage é necessario setar o parametro TabPage na classe FocoGrid
        /// O ASPxPageControl deve se chamar "tabCadastro"
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void gridCadastro_PreRender(object sender, EventArgs e) {
            if (!String.IsNullOrEmpty(this.focoGrid.Campo1) ||
               !String.IsNullOrEmpty(this.focoGrid.Campo2)) {

                if (String.IsNullOrEmpty(this.focoGrid.Campo2)) {
                    // Só existe um campo - Foca ele
                    if (!this.focoGrid.TabPage) {
                        Control c1 = this.gridCadastro.FindEditFormTemplateControl(this.focoGrid.Campo1);
                        if (c1 != null) {
                            c1.Focus();
                        }
                    }
                    else {
                        // Procura o Controle no TabControl
                        ASPxPageControl pageControl = this.gridCadastro.FindEditFormTemplateControl("tabCadastro") as ASPxPageControl;
                        if (pageControl != null) {
                            Control c1 = pageControl.FindControl(this.focoGrid.Campo1);
                            if (c1 != null) {
                                c1.Focus();
                            }
                        }
                    }
                }
                else {
                    // Existe os dois campos
                    // Analisa o Modo: UpdatePanel ou Insert                
                    // Insert
                    if (this.gridCadastro.IsNewRowEditing) {
                        if (!this.focoGrid.TabPage) {
                            Control c1 = this.gridCadastro.FindEditFormTemplateControl(this.focoGrid.Campo1);
                            if (c1 != null) {
                                c1.Focus();
                            }
                        }
                        else {
                            // Procura o Controle no TabControl
                            ASPxPageControl pageControl = this.gridCadastro.FindEditFormTemplateControl("tabCadastro") as ASPxPageControl;
                            if (pageControl != null) {
                                Control c1 = pageControl.FindControl(this.focoGrid.Campo1);
                                if (c1 != null) {
                                    c1.Focus();
                                }
                            }
                        }
                    }
                    // Update
                    else if (this.gridCadastro.IsEditing) {
                        if (!this.focoGrid.TabPage) {
                            Control c2 = this.gridCadastro.FindEditFormTemplateControl(this.focoGrid.Campo2);
                            if (c2 != null) {
                                c2.Focus();
                            }
                        }
                        else {
                            // Procura o Controle no TabControl
                            ASPxPageControl pageControl = this.gridCadastro.FindEditFormTemplateControl("tabCadastro") as ASPxPageControl;
                            if (pageControl != null) {
                                Control c2 = pageControl.FindControl(this.focoGrid.Campo2);
                                if (c2 != null) {
                                    c2.Focus();
                                }
                            }
                        }

                    }
                }
            }
        }

        /// <summary>
        /// Evento para fazer Sum do Grid
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void gridCadastro_CustomSummaryCalculate(object sender, CustomSummaryEventArgs e) {
            // Initialization.
            if (e.SummaryProcess == DevExpress.Data.CustomSummaryProcess.Start) {
                this.gridTotalSummary = 0.00M;
            }

            // Calculation.
            if (e.SummaryProcess == DevExpress.Data.CustomSummaryProcess.Calculate) {
                if (this.gridCadastro.Selection.IsRowSelectedByKey(e.GetValue(this.gridCadastro.KeyFieldName))) {

                    if (!Convert.IsDBNull(e.FieldValue)) {
                        this.gridTotalSummary += Convert.ToDecimal(e.FieldValue);
                    }
                }
            }

            // Finalization.
            if (e.SummaryProcess == DevExpress.Data.CustomSummaryProcess.Finalize) {
                if (this.gridTotalSummary != 0.00M) {
                    e.TotalValue = this.gridTotalSummary;
                }
                else {
                    e.TotalValue = "";
                }
            }
        }

        //Controle (via session) para gerar validação somente para novas linhas
        protected void gridCadastro_StartRowEditing(object sender, DevExpress.Web.Data.ASPxStartRowEditingEventArgs e) {
            if (!gridCadastro.IsNewRowEditing)
                Session["FormLoad"] = "N";
        }
        #endregion

        #region Inicializa os campos para foco no Grid
        /// <summary>
        /// 
        /// </summary>
        /// <param name="campo1">id do campo a ser focado</param>
        public void FocaCampoGrid(string campo1) {
            this.focoGrid = new FocoGrid(campo1);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="campo1">id do campo a ser focado</param>
        /// <param name="tabPage">campo do foco está contido num TabPage</param>
        public void FocaCampoGrid(string campo1, bool tabPage) {
            this.focoGrid = new FocoGrid(campo1);
            this.focoGrid.TabPage = tabPage;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="campo1">id do campo a ser focado</param>
        /// <param name="campo2">id do campo a ser focado</param>
        public void FocaCampoGrid(string campo1, string campo2) {
            this.focoGrid = new FocoGrid(campo1, campo2);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="campo1">id do campo a ser focado</param>
        /// <param name="campo2">id do campo a ser focado</param>
        /// <param name="tabPage">campo do foco está contido num TabPage</param>
        public void FocaCampoGrid(string campo1, string campo2, bool tabPage) {
            this.focoGrid = new FocoGrid(campo1, campo2);
            this.focoGrid.TabPage = tabPage;
        }

        #endregion

        private void InicializaGrid() {
            #region Properties básicas
            this.gridCadastro.ClientInstanceName = "gridCadastro";
            this.gridCadastro.AutoGenerateColumns = false;
            this.gridCadastro.Width = Unit.Percentage(100);
            this.gridCadastro.EnableViewState = true;

            //Troca de Like para Contains nos campos texto do grid
            UtilitarioGrid.GridFilterContains(gridCadastro);

            //Pager
            this.gridCadastro.SettingsPager.PageSize = 30;

            //Settings geral
            this.gridCadastro.Settings.ShowFilterRow = true;
            this.gridCadastro.Settings.ShowStatusBar = GridViewStatusBarMode.Visible;
            this.gridCadastro.Settings.VerticalScrollBarMode = ScrollBarMode.Visible;
            this.gridCadastro.Settings.VerticalScrollableHeight = 340;

            foreach (GridViewColumn column in this.gridCadastro.Columns) {
                GridViewDataColumn coluna = column as GridViewDataColumn;
                if (coluna != null)
                {
                    if (column is GridViewDataDateColumn)
                    {
                        ((GridViewDataDateColumn)column).PropertiesDateEdit.ClearButton.DisplayMode = ClearButtonDisplayMode.Never;
                    }

                    coluna.Settings.AllowAutoFilterTextInputTimer = DefaultBoolean.False;
                }
            }

            if (WebConfig.AppSettings.RolagemVirtual) {
                this.gridCadastro.Settings.VerticalScrollBarStyle = GridViewVerticalScrollBarStyle.Virtual;
            }
            else {
                this.gridCadastro.Settings.VerticalScrollBarStyle = GridViewVerticalScrollBarStyle.Standard;
            }

            //this.gridCadastro.CssFilePath = "~/App_Themes/Glass/{0}/styles.css";
            //this.gridCadastro.CssPostfix = "Glass";
            //this.gridCadastro.SettingsBehavior.AllowMultiSelection = true;
            //this.gridCadastro.Settings.ShowFilterRowMenu = true;
            //this.gridCadastro.EnableRowsCache = false;
            //this.gridCadastro.DataSourceForceStandardPaging = true;

            //Images
            //this.gridCadastro.Images.ImageFolder = "~/App_Themes/Glass/{0}/";
            //this.gridCadastro.Styles.CssFilePath = "~/App_Themes/Glass/{0}/styles.css";
            //this.gridCadastro.Styles.CssPostfix = "Glass";

            //SettingsText
            this.gridCadastro.SettingsText.EmptyDataRow = "0 registros";
            this.gridCadastro.SettingsText.PopupEditFormCaption = " ";
            this.gridCadastro.SettingsLoadingPanel.Text = "Carregando...";

            //Styles
            this.gridCadastro.Styles.Header.SortingImageSpacing = Unit.Pixel(5);
            this.gridCadastro.Styles.AlternatingRow.Enabled = DefaultBoolean.True;
            this.gridCadastro.Styles.Cell.Wrap = DefaultBoolean.False;
            this.gridCadastro.Styles.CommandColumn.Cursor = "hand";

            //A SER RECOLOCADO EM UMA VERSÃO MAIS ESTÁVEL, TESTAR BEM...SERVE PARA ADICIONAR DE FORMA AUTOMÁTICA TOTAIS EM QQ GRID DE CADASTRO!!!!

            //this.gridCadastro.Settings.ShowFooter = true;
            //foreach (GridViewColumn col in gridCadastro.Columns)
            //{
            //    if (col is GridViewDataTextColumn)
            //    {
            //        GridViewDataTextColumn colAux = (GridViewDataTextColumn)col;
            //        if (colAux.FieldName.Contains("Valor") || colAux.FieldName.Contains("Quantidade"))
            //        {
            //            ASPxSummaryItem summaryItem = new ASPxSummaryItem(col.Name, DevExpress.Data.SummaryItemType.Sum);
            //            summaryItem.ShowInColumn = colAux.FieldName;
            //            summaryItem.FieldName = colAux.FieldName;
            //            summaryItem.DisplayFormat = "{0:c0}";
            //            summaryItem.SummaryType = SummaryItemType.Sum;
            //            gridCadastro.TotalSummary.Add(summaryItem);
            //        }
            //    }               
            //}
            //*************************************************************************************************************************************
            #endregion

            //Command Column  (only for EditControl = true)           
            if (this.HasEditControl) {
                #region quando EditControl estiver ligado
                if (this.allowDelete) {
                    ((GridViewCommandColumn)this.gridCadastro.Columns[0]).ShowSelectCheckbox = true;

                    //gridCadastro.Columns[0].HeaderTemplate = new CommandColumnHeaderTemplate(gridCadastro);
                    //gridCadastro.Columns[0].HeaderStyle.HorizontalAlign = HorizontalAlign.Center;

                    // Se tem HeaderTemplate procura pelo comboBox cba_all
                    if (gridCadastro.Columns[0].HeaderTemplate != null) {
                        Control c = gridCadastro.FindHeaderTemplateControl(gridCadastro.Columns[0], "cbAll");
                        if (c != null && c is ASPxCheckBox) {
                            //((ASPxCheckBox)c).BackColor = Color.White;
                            //((ASPxCheckBox)c).ToolTip = "Select/Unselect all rows on the page";
                            //gridCadastro.Columns[0].ToolTip = "Select/Unselect all rows on the page";
                            //                            
                            // Cadastra eventos
                            //c.Init += new EventHandler(cbAll_Init);
                            //((ASPxCheckBox)c).Init += new EventHandler(teste_Init);

                            // Verificar - gridCadastro ou gridConsulta
                            //((ASPxCheckBox)c).ClientSideEvents.CheckedChanged = "function(s, e) {OnAllCheckedChanged(s, e, gridCadastro);}";                            

                            // Alinhamento
                            gridCadastro.Columns[0].HeaderStyle.HorizontalAlign = HorizontalAlign.Center;
                        }
                    }
                }

                ((GridViewCommandColumn)this.gridCadastro.Columns[0]).ButtonType = GridCommandButtonRenderMode.Image;

                #region Popup vs Inline
                if (this.tipoCadastro == (byte)TipoCadastroPagina.Popup) {
                    ((GridViewCommandColumn)this.gridCadastro.Columns[0]).Width = Unit.Percentage(5);
                    //
                    this.gridCadastro.SettingsEditing.Mode = GridViewEditingMode.PopupEditForm;                    
                    this.gridCadastro.SettingsPopup.EditForm.Modal = true;                    
                    this.gridCadastro.SettingsPopup.EditForm.HorizontalAlign = PopupHorizontalAlign.WindowCenter;
                    this.gridCadastro.SettingsPopup.EditForm.VerticalAlign = PopupVerticalAlign.WindowCenter;
                }
                else if (this.tipoCadastro == (byte)TipoCadastroPagina.Inline) {
                    ((GridViewCommandColumn)this.gridCadastro.Columns[0]).Width = Unit.Percentage(10);
                    this.gridCadastro.SettingsCommandButton.UpdateButton.Image.Url = "~/imagens/ico_form_ok_inline.gif";
                    this.gridCadastro.SettingsCommandButton.CancelButton.Image.Url = "~/imagens/ico_form_back_inline.gif";
                    this.gridCadastro.SettingsCommandButton.UpdateButton.Text = "Salvar";
                    this.gridCadastro.SettingsCommandButton.CancelButton.Text = "Cancelar";                                        
                    //
                    this.gridCadastro.SettingsEditing.Mode = GridViewEditingMode.Inline;
                    UtilitarioGrid.SetaCorCombosInline(this.gridCadastro);
                    this.gridCadastro.CellEditorInitialize += new ASPxGridViewEditorEventHandler(this.gridCadastro_CellEditorInitialize);
                }
                #endregion

                #region Evento de Duplo Click
                //Client-Side Events
                this.gridCadastro.ClientSideEvents.RowDblClick = this.allowUpdate
                                        ? "function(s, e) {editFormOpen=true; gridCadastro.StartEditRow(e.visibleIndex);}"
                                        : "function(s, e) { alert('Apenas operações de inserção e deleção são permitidas.'); }";
                #endregion

                #endregion
            }

            #region Eventos
            //Eventos comuns
            this.gridCadastro.HtmlRowCreated += new ASPxGridViewTableRowEventHandler(UtilitarioGrid.Grid_HtmlRowCreated);
            //
            this.gridCadastro.HtmlDataCellPrepared += new ASPxGridViewTableDataCellEventHandler(gridCadastro_HtmlDataCellPrepared);
            //
            this.gridCadastro.CustomJSProperties += new ASPxGridViewClientJSPropertiesEventHandler(this.gridCadastro_CustomJSProperties);
            //
            this.gridCadastro.PreRender += new EventHandler(this.gridCadastro_PreRender);
            #endregion
        }

        /// <summary>
        /// Adiciona Total no Rodapé do Grid
        /// </summary>
        private void AddGridTotalSummary() {

            #region Colunas para ASPxSummaryItem
            List<string> colunasSummary = new List<string>();
            List<string> displayFormat = new List<string>();
            //
            for (int i = 0; i < this.gridCadastro.VisibleColumns.Count; i++) {
                GridViewColumn column = this.gridCadastro.VisibleColumns[i] as GridViewColumn;

                if (column is GridViewDataTextColumn) {
                    GridViewDataTextColumn c1 = column as GridViewDataTextColumn;
                    if (!String.IsNullOrEmpty(c1.PropertiesTextEdit.DisplayFormatString)) {
                        if (!c1.FieldName.StartsWith("Id", true, CultureInfo.InvariantCulture)) {
                            colunasSummary.Add(c1.FieldName);
                            displayFormat.Add(c1.PropertiesTextEdit.DisplayFormatString);
                        }
                    }
                }

                if (column is GridViewDataSpinEditColumn) {
                    GridViewDataSpinEditColumn c2 = column as GridViewDataSpinEditColumn;
                    if (!String.IsNullOrEmpty(c2.PropertiesSpinEdit.DisplayFormatString)) {
                        if (!c2.FieldName.StartsWith("Id", true, CultureInfo.InvariantCulture)) {
                            colunasSummary.Add(c2.FieldName);
                            displayFormat.Add(c2.PropertiesSpinEdit.DisplayFormatString);
                        }
                    }
                }
            }
            #endregion

            #region Adiciona Summary
            for (int j = 0; j < colunasSummary.Count; j++) {
                ASPxSummaryItem item = new ASPxSummaryItem();
                //
                item.FieldName = colunasSummary[j];
                item.ShowInColumn = colunasSummary[j];
                item.SummaryType = SummaryItemType.Custom;
                //item.DisplayFormat = "<b>{0:N2}</b>";
                //item.DisplayFormat = "{0:#,##0.00;(#,##0.00);0.00}";
                //item.ValueDisplayFormat = "{0:#,##0.00;(#,##0.00);0.00}";
                //
                item.DisplayFormat = displayFormat[j];
                item.ValueDisplayFormat = displayFormat[j];
                //

                if (this.gridCadastro.TotalSummary.Find(delegate(ASPxSummaryItem item1) {
                    return item1.FieldName.Contains(item.FieldName);
                }) == null) {
                    this.gridCadastro.TotalSummary.Add(item);
                }
            }
            #endregion

            // Determina se FooterSummary vai aparecer            
            this.gridCadastro.Settings.ShowFooter = colunasSummary.Count != 0;

            #region Selection Changed do Grid e Evento ServerSide CustomSummaryCalculate            
            if (colunasSummary.Count != 0) 
            {
                string fn = "";
                fn = " function(s, e){";
                fn += "    gridCadastro.Refresh();";
                fn += "    return false;";
                fn += "} ";
                //
                // Client JavaScript
                this.gridCadastro.ClientSideEvents.SelectionChanged = fn;

                // Server Side oara Calcular o Sum do grid
                this.gridCadastro.CustomSummaryCalculate += new CustomSummaryEventHandler(this.gridCadastro_CustomSummaryCalculate);
            }
            #endregion
        }

        #region Evento do Combo para selecionar todos os CheckBoxs
        /// <summary>
        /// Função para selecionar todos os checkboxs de um Grid 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void CheckBoxSelectAll(object sender, EventArgs e) {
            ASPxCheckBox chk = sender as ASPxCheckBox;
            ASPxGridView grid = (chk.NamingContainer as GridViewHeaderTemplateContainer).Grid;
            chk.Checked = (grid.Selection.Count == grid.VisibleRowCount);
        }
        #endregion

        private void CriaBrowserPopups() {
            BrowserPopups popups = new BrowserPopups();

            if (this.hasPopupPessoa) {
                ASPxPopupControl popup = popups.PopupPessoa(this.hasFiltro);
                ASPxPopupControl popupMensagem = popups.PopupMensagemPessoa();
                ph1.Controls.Add(popupMensagem);
                ph1.Controls.Add(popup);
            }
            if (this.hasPopupCliente) {
                ASPxPopupControl popup = popups.PopupCliente(this.hasFiltro);
                ASPxPopupControl popupMensagem = popups.PopupMensagemCliente();
                ph1.Controls.Add(popupMensagem);
                ph1.Controls.Add(popup);
            }
            if (this.hasPopupCliente1) {
                ASPxPopupControl popup = popups.PopupCliente1(this.hasFiltro);
                ASPxPopupControl popupMensagem = popups.PopupMensagemCliente1();
                ph1.Controls.Add(popupMensagem);
                ph1.Controls.Add(popup);
            }
            if (this.hasPopupCarteira) {
                ASPxPopupControl popup = popups.PopupCarteira(this.hasFiltro);
                ASPxPopupControl popupMensagem = popups.PopupMensagemCarteira();
                ph1.Controls.Add(popupMensagem);
                ph1.Controls.Add(popup);
            }

            if (this.hasPopupCarteira1) {
                ASPxPopupControl popup = popups.PopupCarteira1(this.hasFiltro);
                ASPxPopupControl popupMensagem = popups.PopupMensagemCarteira1();
                ph1.Controls.Add(popupMensagem);
                ph1.Controls.Add(popup);
            }

            if (this.hasPopupCarteira2)
            {
                ASPxPopupControl popup = popups.PopupCarteira2(this.hasFiltro);
                ASPxPopupControl popupMensagem = popups.PopupMensagemCarteira2();
                ph1.Controls.Add(popupMensagem);
                ph1.Controls.Add(popup);
            }

            if (this.hasPopupCotista) {
                ASPxPopupControl popup = popups.PopupCotista(this.hasFiltro);
                ASPxPopupControl popupMensagem = popups.PopupMensagemCotista();
                ph1.Controls.Add(popupMensagem);
                ph1.Controls.Add(popup);
            }
            if (this.hasPopupAtivoBolsa) {
                ASPxPopupControl popup = popups.PopupAtivoBolsa(this.hasPopupAtivoBolsa2);
                ASPxPopupControl popupMensagem = popups.PopupMensagemAtivoBolsa();
                ph1.Controls.Add(popupMensagem);
                ph1.Controls.Add(popup);
            }
            if (this.hasPopupAtivoBMF) {
                ASPxPopupControl popup = popups.PopupAtivoBMF();
                ASPxPopupControl popupMensagem = popups.PopupMensagemAtivoBMF();
                ph1.Controls.Add(popupMensagem);
                ph1.Controls.Add(popup);
            }
            if (this.hasPopupTituloRF) {
                ASPxPopupControl popup = popups.PopupTituloRF();
                ASPxPopupControl popupMensagem = popups.PopupMensagemTituloRF();
                ph1.Controls.Add(popupMensagem);
                ph1.Controls.Add(popup);
            }
            if (this.hasPopupEmissor) {
                ASPxPopupControl popup = popups.PopupEmissor();
                ASPxPopupControl popupMensagem = popups.PopupMensagemEmissor();
                ph1.Controls.Add(popupMensagem);
                ph1.Controls.Add(popup);
            }
            if (this.hasPopupContabConta)
            {
                ASPxPopupControl popup = popups.PopupContabConta();
                ASPxPopupControl popupMensagem = popups.PopupMensagemContabConta();
                ph1.Controls.Add(popupMensagem);
                ph1.Controls.Add(popup);
            }
            if (this.hasPopupAgenteMercado)
            {
                ASPxPopupControl popup = popups.PopupAgenteMercado(this.hasFiltro);
                ASPxPopupControl popupMensagem = popups.PopupMensagemAgenteMercado();
                ph1.Controls.Add(popupMensagem);
                ph1.Controls.Add(popup);
            }

            List<System.Web.UI.WebControls.TextBox> listaMensagens = popups.ListaMensagens();
            this.ph1.Controls.Add((System.Web.UI.WebControls.TextBox)listaMensagens[0]);
            this.ph1.Controls.Add((System.Web.UI.WebControls.TextBox)listaMensagens[1]);
            this.ph1.Controls.Add((System.Web.UI.WebControls.TextBox)listaMensagens[2]);
            this.ph1.Controls.Add((System.Web.UI.WebControls.TextBox)listaMensagens[3]);
        }

        private void InicializaFiltro() {
            if (this.hasFiltro && !Page.IsCallback) {
                DateTime hoje = DateTime.Now;
                //Retorna 3 dias corridos anteriores
                DateTime diaAnterior = hoje.Subtract(TimeSpan.FromDays(3));

                ASPxPopupControl popupFiltro = this.FindControl("popupFiltro") as ASPxPopupControl;

                ASPxDateEdit textDataInicio = popupFiltro.FindControl("textDataInicio") as ASPxDateEdit;
                ASPxDateEdit textDataFim = popupFiltro.FindControl("textDataFim") as ASPxDateEdit;

                textDataFim.Text = hoje.ToShortDateString().ToString();
                textDataInicio.Text = diaAnterior.ToShortDateString().ToString();
            }
        }

        private void InicializaValidacaoForm() {
            if (this.hasPanelFieldsLoading && !this.gridCadastro.IsNewRowEditing) {
                this.gridCadastro.StartRowEditing += new ASPxStartRowEditingEventHandler(this.gridCadastro_StartRowEditing);
            }
        }

        protected void panelEdicao_Load(object sender, EventArgs e) {
            if (gridCadastro.Templates.EditForm != null) //Template editing
            {
                UtilitarioGrid.SetaCorCombosPopup(sender as Control);
            }
        }

        #region btnPDF_Click, btnExcel_Click, btnOK_Click, btnOKFilter_Click
        protected void btnPDF_Click(object sender, EventArgs e) {
            gridExport.WritePdfToResponse();
        }

        protected void btnExcel_Click(object sender, EventArgs e) {
            gridExport.WriteXlsToResponse();
        }

        protected void btnOK_Click(object sender, EventArgs e) {
            if (this.tipoCadastro == (byte)TipoCadastroPagina.Inline) {
                this.gridCadastro.DataBind();
            }
        }

        protected void btnOKFilter_Click(object sender, EventArgs e) {
            gridCadastro.DataBind();
        }
        #endregion

        protected string GetFirstColumnFieldName(ASPxGridView grid) {
            for (int i = 0; i < grid.Columns.Count; i++) {
                GridViewDataColumn column = grid.Columns[i] as GridViewDataColumn;
                if (column != null && !column.ReadOnly && column.Visible)
                    return column.FieldName;
            }
            throw new InvalidOperationException("All columns are readonly or invisible.");
        }

        /// <summary>
        /// Realiza o Foco no Controle baseado no Modo Insert/Update
        /// </summary>
        /// <param name="idControleFocoInsert"></param>
        /// <param name="idControleFocoUpdate"></param>
        protected void Focus(string idControleFocoInsert, string idControleFocoUpdate) {
            //Insert
            if (this.gridCadastro.IsNewRowEditing) {
                Control c1 = this.gridCadastro.FindEditFormTemplateControl(idControleFocoInsert);
                if (c1 != null) {
                    c1.Focus();
                }
            }

            // Update
            else if (this.gridCadastro.IsEditing) {
                Control c2 = this.gridCadastro.FindEditFormTemplateControl(idControleFocoUpdate);
                if (c2 != null) {
                    c2.Focus();
                }
            }
        }

        /// <summary>
        /// Realiza o Foco no Controle
        /// </summary>
        /// <param name="idControleFoco"></param>
        protected void Focus(string idControleFoco) {
            Control c = this.gridCadastro.FindEditFormTemplateControl(idControleFoco);
            if (c != null) {
                c.Focus();
            }
        }
    }

    /// <summary>
    /// Classe utilitária para o AspxGridView
    /// </summary>
    public static class UtilitarioGrid {
        public static void GridFilterContains(ASPxGridView grid) {
            //Default para colunas com Contains
            GridViewDataColumn colNome = grid.Columns["Nome"] as GridViewDataColumn;
            if (colNome != null) {
                colNome.Settings.AutoFilterCondition = AutoFilterCondition.Contains;
            }
            GridViewDataColumn colApelido = grid.Columns["Apelido"] as GridViewDataColumn;
            if (colApelido != null) {
                colApelido.Settings.AutoFilterCondition = AutoFilterCondition.Contains;
            }
            GridViewDataColumn colDescricao = grid.Columns["Descricao"] as GridViewDataColumn;
            if (colDescricao != null) {
                colDescricao.Settings.AutoFilterCondition = AutoFilterCondition.Contains;
            }
            GridViewDataColumn colCdAtivoBolsa = grid.Columns["CdAtivoBolsa"] as GridViewDataColumn;
            if (colCdAtivoBolsa != null) {
                colCdAtivoBolsa.Settings.AutoFilterCondition = AutoFilterCondition.Contains;
            }
            GridViewDataColumn colApelidoCarteira = grid.Columns["ApelidoCarteira"] as GridViewDataColumn;
            if (colApelidoCarteira != null) {
                colApelidoCarteira.Settings.AutoFilterCondition = AutoFilterCondition.Contains;
            }
            GridViewDataColumn colApelidoCliente = grid.Columns["ApelidoCliente"] as GridViewDataColumn;
            if (colApelidoCliente != null) {
                colApelidoCliente.Settings.AutoFilterCondition = AutoFilterCondition.Contains;
            }
            GridViewDataColumn colApelidoCotista = grid.Columns["ApelidoCotista"] as GridViewDataColumn;
            if (colApelidoCotista != null) {
                colApelidoCotista.Settings.AutoFilterCondition = AutoFilterCondition.Contains;
            }
        }

        public static void SetaCoresGrid(ASPxGridViewTableRowEventArgs e) {
            e.Row.Attributes.Add("onmouseover", "backgroundColor = this.style.backgroundColor; this.style.backgroundColor='#E0E9EB';");
            e.Row.Attributes.Add("onmouseout", "this.style.backgroundColor=backgroundColor;");
        }

        public static void Grid_HtmlRowCreated(object sender, ASPxGridViewTableRowEventArgs e) {
            UtilitarioGrid.SetaCoresGrid(e);
        }

        private const string ClientSideEventHandlerFormat = "function(s, e) {{ {0} }}";

        private static readonly Regex AnonymEventHandlerRegex = new Regex(@"^\s*?(?<HANDLER_SIGNATURE>function\s*?\([^\)]*?\))\s*?\{(?<HANDLER_CONTENT>.*)\}\s*?$",
            RegexOptions.IgnoreCase | RegexOptions.Singleline | RegexOptions.CultureInvariant);
        private static readonly Regex NamedEventHandlerRegex = new Regex(@"^(?<HANDLER_NAME>\w+)$",
            RegexOptions.IgnoreCase | RegexOptions.Singleline | RegexOptions.CultureInvariant);

        private static string GetUpdatedClientInitHandler(string eventHandler, string bgColor) {
            string newBehavior = "s.GetMainElement().style.backgroundColor = '" + bgColor + "';";
            if (string.IsNullOrEmpty(eventHandler))
                return string.Format(ClientSideEventHandlerFormat, newBehavior);
            else {
                Match match;
                if ((match = NamedEventHandlerRegex.Match(eventHandler)).Success)
                    return string.Format("function(s, e) {{ {0} {1}(s, e); }}", newBehavior, match.Groups["HANDLER_NAME"]);
                else if ((match = AnonymEventHandlerRegex.Match(eventHandler)).Success)
                    return string.Format("{0} {{ {1} {2} }}", match.Groups["HANDLER_SIGNATURE"], newBehavior, match.Groups["HANDLER_CONTENT"]);
                else
                    throw new ArgumentException("eventHandler");
            }
        }

        public static void SetaCorCombosInline(ASPxGridView gridCadastro) {
            foreach (GridViewColumn column in gridCadastro.Columns) {
                if (column is GridViewDataComboBoxColumn) {
                    ((GridViewDataComboBoxColumn)column).PropertiesComboBox.ClientSideEvents.GotFocus = "function(s,e){s.GetMainElement().style.backgroundColor = '#ffffcc';}";
                    ((GridViewDataComboBoxColumn)column).PropertiesComboBox.ClientSideEvents.LostFocus = "function(s,e){s.GetMainElement().style.backgroundColor = 'White';}";
                }
            }
        }

        public static void SetaCorCombosPopup(Control container) {
            if (container == null)
                throw new ArgumentNullException();
            foreach (Control child in container.Controls) {
                ASPxComboBox comboBox = child as ASPxComboBox;
                if (comboBox != null) {
                    if (comboBox.EnableIncrementalFiltering == true) {
                        comboBox.ClientSideEvents.LostFocus = "function(s, e) {if (s.GetSelectedIndex() == -1) s.SetText(null);}";
                    }

                    // Client-side event modifications
                    comboBox.ClientSideEvents.GotFocus = GetUpdatedClientInitHandler(comboBox.ClientSideEvents.GotFocus, "#ffffcc");
                    comboBox.ClientSideEvents.LostFocus = GetUpdatedClientInitHandler(comboBox.ClientSideEvents.LostFocus, "White");

                }
                SetaCorCombosPopup(child);
            }
        }

        /// <summary>
        /// Cria items num ListEditItem com suporte a tooltip
        /// </summary>
        /// <param name="grid">GridCadastro</param>
        /// <param name="colunaGrid">Nome da Coluna do tipo GridViewDataComboBoxColumn que se deseja inserir itens</param>
        /// <param name="textos">lista de textos do ListEditItem</param>
        /// <param name="keys">lista de chaves do ListEditItem</param>
        /// <exception cref="">ArgmumentException se textos.lenght diferente de keys.lenght</exception>
        public static void CarregaTooltipListEditItem(ASPxGridView grid, string colunaGrid, List<string> textos, List<Object> keys) {
            if (textos.Count != keys.Count) {
                throw new ArgumentException("Parâmetro Invalido");
            }

            ListEditItemCollection l = new ListEditItemCollection();

            List<string> textosTooltip = new List<string>(textos.Count);
            // Cria elementos numa div oculta
            for (int i = 0; i < textos.Count; i++) {
                textosTooltip.Add("<div title='" + textos[i] + "'>" + textos[i] + "</div>");
            }

            ListEditItem item;
            for (int i = 0; i < textosTooltip.Count; i++) {
                item = new ListEditItem(textosTooltip[i], keys[i]);
                l.Add(item);
            }
            ((GridViewDataComboBoxColumn)grid.Columns[colunaGrid]).PropertiesComboBox.EncodeHtml = false; // Não interpreta Codigo HTML
            ((GridViewDataComboBoxColumn)grid.Columns[colunaGrid]).PropertiesComboBox.Items.AddRange(l); // Adiciona itens criados
        }

        /// <summary>
        /// Retira tag div dos textos de uma coluna GridViewDataComboBoxColumn
        /// Usado nas exportações excel e PDF para consertar colunas com tooltip
        /// </summary>
        /// <param name="gridExport">Grid de Exportação</param>
        /// <param name="colunasExport">colunas do tipo GridViewDataComboBoxColumn</param>
        public static void ExportacaoPDFExcel(ASPxGridViewExporter gridExport, List<string> colunasExport) {

            gridExport.RenderBrick += delegate(object sender, ASPxGridViewExportRenderingEventArgs e) {
                                          exporter_RenderBrick(sender, e, colunasExport);
                                      };

            //gridExport.RenderBrick += new ASPxGridViewExportRenderingEventHandler(UtilitarioGrid.exporter_RenderBrick);
        }

        /// <summary>
        /// Trata Textos com div em Exportações Excel e PDF
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// <param name="colunasExport">colunas do tipo GridViewDataComboBoxColumn</param> 
        public static void exporter_RenderBrick(object sender, ASPxGridViewExportRenderingEventArgs e, List<string> colunasExport) {
            GridViewDataColumn dataColumn = e.Column as GridViewDataColumn;

            if (e.RowType == GridViewRowType.Data && dataColumn != null) {

                for (int i = 0; i < colunasExport.Count; i++) {

                    if (dataColumn.FieldName == colunasExport[i]) {

                        string t = e.Text.Trim();
                        if (t.Contains("<div title='")) {

                            int inicio = t.IndexOf("'>") + 2;
                            //               
                            string texto = t.Substring(inicio);
                            texto = texto.Replace("</div>", "");

                            e.Text = texto;
                        }
                    }
                }
            }
        }
    }

    /// <summary>
    /// Classe para criação da popup, com o grid dentro.
    /// </summary>
    public class BrowserPopups {
        private void SetaCoresGrid(ASPxGridViewTableRowEventArgs e) {
            e.Row.Attributes.Add("onmouseover", "backgroundColor = this.style.backgroundColor; this.style.backgroundColor='#E0E9EB';");
            e.Row.Attributes.Add("onmouseout", "this.style.backgroundColor=backgroundColor;");
        }

        private void Grid_HtmlRowCreated(object sender, ASPxGridViewTableRowEventArgs e) {
            this.SetaCoresGrid(e);
        }

        #region Eventos de callback de grid
        public void gridPessoa_CustomDataCallBack(object sender, ASPxGridViewCustomDataCallbackEventArgs e) {
            ASPxGridView gridView = sender as ASPxGridView;
            e.Result = gridView.GetRowValues(Convert.ToInt32(e.Parameters), "IdPessoa");
        }

        public void gridCliente_CustomDataCallBack(object sender, ASPxGridViewCustomDataCallbackEventArgs e) {
            ASPxGridView gridView = sender as ASPxGridView;
            e.Result = gridView.GetRowValues(Convert.ToInt32(e.Parameters), "IdCliente");
        }

        public void gridCarteira_CustomDataCallBack(object sender, ASPxGridViewCustomDataCallbackEventArgs e) {
            ASPxGridView gridView = sender as ASPxGridView;
            e.Result = gridView.GetRowValues(Convert.ToInt32(e.Parameters), "IdCarteira");
        }

        public void gridCotista_CustomDataCallBack(object sender, ASPxGridViewCustomDataCallbackEventArgs e) {
            ASPxGridView gridView = sender as ASPxGridView;
            e.Result = gridView.GetRowValues(Convert.ToInt32(e.Parameters), "IdCotista");
        }

        public void gridAtivoBolsa_CustomDataCallBack(object sender, ASPxGridViewCustomDataCallbackEventArgs e) {
            ASPxGridView gridView = sender as ASPxGridView;
            e.Result = gridView.GetRowValues(Convert.ToInt32(e.Parameters), "CdAtivoBolsa");
        }

        public void gridAtivoBMF_CustomDataCallBack(object sender, ASPxGridViewCustomDataCallbackEventArgs e) {
            ASPxGridView gridView = sender as ASPxGridView;
            e.Result = Convert.ToString(gridView.GetRowValues(Convert.ToInt32(e.Parameters), "CdAtivoBMF")) + "-" +
                       Convert.ToString(gridView.GetRowValues(Convert.ToInt32(e.Parameters), "Serie"));
        }

        public void gridTituloRF_CustomDataCallBack(object sender, ASPxGridViewCustomDataCallbackEventArgs e) {
            ASPxGridView gridView = sender as ASPxGridView;
            e.Result = gridView.GetRowValues(Convert.ToInt32(e.Parameters), "IdTitulo");
        }

        public void gridTituloRF_CustomCallBack(object sender, ASPxGridViewCustomCallbackEventArgs e) {
            ASPxGridView gridView = sender as ASPxGridView;
            gridView.DataBind();
        }

        public void gridCarteira_CustomCallBack(object sender, ASPxGridViewCustomCallbackEventArgs e)
        {
            ASPxGridView gridView = sender as ASPxGridView;
            gridView.DataBind();
        }

        public void gridEmissor_CustomDataCallBack(object sender, ASPxGridViewCustomDataCallbackEventArgs e) {
            ASPxGridView gridView = sender as ASPxGridView;
            e.Result = gridView.GetRowValues(Convert.ToInt32(e.Parameters), "IdEmissor");
        }

        public void gridEmissor_CustomCallBack(object sender, ASPxGridViewCustomCallbackEventArgs e) {
            ASPxGridView gridView = sender as ASPxGridView;
            gridView.DataBind();
        }

        public void gridContabConta_CustomDataCallBack(object sender, ASPxGridViewCustomDataCallbackEventArgs e)
        {
            ASPxGridView gridView = sender as ASPxGridView;
            e.Result = gridView.GetRowValues(Convert.ToInt32(e.Parameters), "IdConta");
        }

        public void gridContabConta_CustomCallBack(object sender, ASPxGridViewCustomCallbackEventArgs e)
        {
            ASPxGridView gridView = sender as ASPxGridView;
            gridView.DataBind();
        }

        protected void gridAgenteMercado_CustomDataCallBack(object sender, ASPxGridViewCustomDataCallbackEventArgs e)
        {
            ASPxGridView gridView = sender as ASPxGridView;
            e.Result = gridView.GetRowValues(Convert.ToInt32(e.Parameters), "IdAgente");
        }
        public void gridAgenteMercado_CustomCallBack(object sender, ASPxGridViewCustomCallbackEventArgs e)
        {
            ASPxGridView gridView = sender as ASPxGridView;
            gridView.DataBind();
        }
        #endregion

        #region Properties comuns a grids e popups
        private ASPxGridView SetCommonSettings(ASPxGridView grid) {
            grid.Width = Unit.Percentage(100);
            grid.AutoGenerateColumns = false;
            //grid.CssFilePath = "~/App_Themes/Glass/{0}/styles.css";
            //grid.CssPostfix = "Glass";
            grid.Settings.ShowFilterRow = true;
            grid.Settings.ShowTitlePanel = true;
            grid.SettingsBehavior.ColumnResizeMode = ColumnResizeMode.Disabled;
            grid.SettingsDetail.ShowDetailButtons = false;
            grid.Styles.AlternatingRow.Enabled = DefaultBoolean.True;
            grid.Styles.Cell.Wrap = DefaultBoolean.False;
            //grid.Styles.CssFilePath = "~/App_Themes/Glass/{0}/styles.css";
            //grid.Styles.CssPostfix = "Glass";
            //grid.Images.ImageFolder = "~/App_Themes/Glass/{0}/";
            grid.Images.PopupEditFormWindowClose.Height = Unit.Pixel(17);
            grid.Images.PopupEditFormWindowClose.Width = Unit.Pixel(17);
            grid.SettingsText.EmptyDataRow = "0 Registros";
            grid.ClientSideEvents.Init = "function(s, e) {e.cancel = true;} ";

            return grid;
        }

        private ASPxPopupControl SetCommonSettings(ASPxPopupControl popup) {
            popup.Width = Unit.Pixel(500);
            popup.ContentStyle.VerticalAlign = VerticalAlign.Top;
            popup.PopupVerticalAlign = PopupVerticalAlign.Middle;
            popup.PopupHorizontalAlign = PopupHorizontalAlign.OutsideRight;
            popup.AllowDragging = true;

            return popup;
        }

        private ASPxPopupControl SetCommonSettingsMensagem(ASPxPopupControl popup) {
            popup.CloseAction = CloseAction.MouseOut;
            popup.ForeColor = Color.Red;
            popup.PopupVerticalAlign = PopupVerticalAlign.Middle;
            popup.PopupHorizontalAlign = PopupHorizontalAlign.OutsideRight;
            popup.Left = 100;
            popup.PopupAction = PopupAction.None;
            popup.HeaderText = "";

            return popup;
        }
        #endregion

        #region Popups de browser e popus de mensagem
        public ASPxPopupControl PopupPessoa(bool hasFiltro) {
            ASPxPopupControl popupPessoa = new ASPxPopupControl();
            popupPessoa = SetCommonSettings(popupPessoa); //Seta properties comuns a todos as popups
            popupPessoa.ClientInstanceName = "popupPessoa";
            popupPessoa.HeaderText = " ";
            popupPessoa.ClientSideEvents.CloseUp = "function(s, e) {gridPessoa.ClearFilter(); }";

            ASPxGridView gridPessoa = new ASPxGridView();
            gridPessoa = SetCommonSettings(gridPessoa); //Seta properties comuns a todos os grids
            gridPessoa.ClientInstanceName = "gridPessoa";
            gridPessoa.DataSourceID = "EsDSPessoa";
            gridPessoa.KeyFieldName = "IdPessoa";
            gridPessoa.SettingsText.Title = "Pesquisa Pessoa";

            if (hasFiltro) {
                StringBuilder str = new StringBuilder();
                str.Append("function(s, e) { if (gridCadastro.cp_EditVisibleIndex == -1) ");
                str.Append(" gridPessoa.GetValuesOnCustomCallback(e.visibleIndex, OnGetDataPessoaFiltro); ");
                str.Append(" else ");
                str.Append("  gridPessoa.GetValuesOnCustomCallback(e.visibleIndex, OnGetDataPessoa); }");
                gridPessoa.ClientSideEvents.RowDblClick = str.ToString();
            }
            else {
                gridPessoa.ClientSideEvents.RowDblClick = "function(s, e) {gridPessoa.GetValuesOnCustomCallback(e.visibleIndex, OnGetDataPessoa);}";
            }

            //Colunas do grid
            GridViewDataTextColumn IdColumn = new GridViewDataTextColumn();
            IdColumn.FieldName = "IdPessoa";
            IdColumn.ReadOnly = true;
            IdColumn.VisibleIndex = 0;
            IdColumn.Width = Unit.Percentage(20);

            GridViewDataTextColumn ApelidoColumn = new GridViewDataTextColumn();
            ApelidoColumn.FieldName = "Apelido";
            ApelidoColumn.VisibleIndex = 1;
            ApelidoColumn.Width = Unit.Percentage(80);

            gridPessoa.Columns.Add(IdColumn);
            gridPessoa.Columns.Add(ApelidoColumn);
            //

            //Troca de Like para Contains no filtro
            UtilitarioGrid.GridFilterContains(gridPessoa);

            //Eventos de server-side
            gridPessoa.HtmlRowCreated += new ASPxGridViewTableRowEventHandler(this.Grid_HtmlRowCreated);
            gridPessoa.CustomDataCallback += new ASPxGridViewCustomDataCallbackEventHandler(this.gridPessoa_CustomDataCallBack);

            //Adiciona na popup e retorna a a popup
            popupPessoa.Controls.Add(gridPessoa);
            return popupPessoa;
        }
        public ASPxPopupControl PopupMensagemPessoa() {
            ASPxPopupControl popupMensagem = new ASPxPopupControl();
            popupMensagem.ID = "popupMensagemPessoa";
            popupMensagem = SetCommonSettingsMensagem(popupMensagem); //Seta properties comuns a todos as popups de msg
            popupMensagem.PopupElementID = "btnEditCodigoPessoa";

            return popupMensagem;
        }
        //
        public ASPxPopupControl PopupCliente(bool hasFiltro) {
            ASPxPopupControl popupCliente = new ASPxPopupControl();
            popupCliente = SetCommonSettings(popupCliente); //Seta properties comuns a todos as popups
            popupCliente.ClientInstanceName = "popupCliente";
            popupCliente.ID = "PopupCliente";
            popupCliente.HeaderText = " ";
            popupCliente.ClientSideEvents.CloseUp = "function(s, e) {gridCliente.ClearFilter(); }";

            ASPxGridView gridCliente = new ASPxGridView();
            gridCliente = SetCommonSettings(gridCliente); //Seta properties comuns a todos os grids
            gridCliente.ClientInstanceName = "gridCliente";
            gridCliente.ID = "PopupClienteGridCliente";
            gridCliente.DataSourceID = "EsDSCliente";
            gridCliente.KeyFieldName = "IdCliente";
            gridCliente.SettingsText.Title = "Pesquisa Cliente";

            if (hasFiltro) {
                StringBuilder str = new StringBuilder();
                str.Append("function(s, e) { if (gridCadastro.cp_EditVisibleIndex == -1) ");
                str.Append(" gridCliente.GetValuesOnCustomCallback(e.visibleIndex, OnGetDataClienteFiltro); ");
                str.Append(" else ");
                str.Append("  gridCliente.GetValuesOnCustomCallback(e.visibleIndex, OnGetDataCliente); }");
                gridCliente.ClientSideEvents.RowDblClick = str.ToString();
            }
            else {
                gridCliente.ClientSideEvents.RowDblClick = "function(s, e) {gridCliente.GetValuesOnCustomCallback(e.visibleIndex, OnGetDataCliente);}";
            }

            //Colunas do grid
            GridViewDataTextColumn IdColumn = new GridViewDataTextColumn();
            IdColumn.FieldName = "IdCliente";
            IdColumn.ReadOnly = true;
            IdColumn.VisibleIndex = 0;
            IdColumn.Width = Unit.Percentage(20);

            GridViewDataTextColumn ApelidoColumn = new GridViewDataTextColumn();
            ApelidoColumn.FieldName = "Apelido";
            ApelidoColumn.VisibleIndex = 1;
            ApelidoColumn.Width = Unit.Percentage(80);

            gridCliente.Columns.Add(IdColumn);
            gridCliente.Columns.Add(ApelidoColumn);
            //

            //Troca de Like pelo Contains no filtro
            UtilitarioGrid.GridFilterContains(gridCliente);

            //Eventos de server-side
            gridCliente.HtmlRowCreated += new ASPxGridViewTableRowEventHandler(this.Grid_HtmlRowCreated);
            gridCliente.CustomDataCallback += new ASPxGridViewCustomDataCallbackEventHandler(this.gridCliente_CustomDataCallBack);

            //Adiciona na popup e retorna a a popup
            popupCliente.Controls.Add(gridCliente);
            return popupCliente;
        }
        public ASPxPopupControl PopupMensagemCliente() {
            ASPxPopupControl popupMensagem = new ASPxPopupControl();
            popupMensagem.ID = "popupMensagemCliente";
            popupMensagem = SetCommonSettingsMensagem(popupMensagem); //Seta properties comuns a todos as popups de msg
            popupMensagem.PopupElementID = "btnEditCodigoCliente";

            return popupMensagem;
        }
        //
        public ASPxPopupControl PopupCliente1(bool hasFiltro) {
            ASPxPopupControl popupCliente = new ASPxPopupControl();
            popupCliente = SetCommonSettings(popupCliente); //Seta properties comuns a todos as popups
            popupCliente.ClientInstanceName = "popupCliente1";
            popupCliente.HeaderText = " ";
            popupCliente.ClientSideEvents.CloseUp = "function(s, e) {gridCliente1.ClearFilter(); }";

            ASPxGridView gridCliente = new ASPxGridView();
            gridCliente = SetCommonSettings(gridCliente); //Seta properties comuns a todos os grids
            gridCliente.ClientInstanceName = "gridCliente1";
            gridCliente.DataSourceID = "EsDSCliente1";
            gridCliente.KeyFieldName = "IdCliente";
            gridCliente.SettingsText.Title = "Pesquisa Cliente";

            if (hasFiltro) {
                StringBuilder str = new StringBuilder();
                str.Append("function(s, e) { if (gridCadastro.cp_EditVisibleIndex == -1) ");
                str.Append(" gridCliente1.GetValuesOnCustomCallback(e.visibleIndex, OnGetDataClienteFiltro1); ");
                str.Append(" else ");
                str.Append("  gridCliente1.GetValuesOnCustomCallback(e.visibleIndex, OnGetDataCliente1); }");
                gridCliente.ClientSideEvents.RowDblClick = str.ToString();
            }
            else {
                gridCliente.ClientSideEvents.RowDblClick = "function(s, e) {gridCliente1.GetValuesOnCustomCallback(e.visibleIndex, OnGetDataCliente1);}";
            }

            //Colunas do grid
            GridViewDataTextColumn IdColumn = new GridViewDataTextColumn();
            IdColumn.FieldName = "IdCliente";
            IdColumn.ReadOnly = true;
            IdColumn.VisibleIndex = 0;
            IdColumn.Width = Unit.Percentage(20);

            GridViewDataTextColumn ApelidoColumn = new GridViewDataTextColumn();
            ApelidoColumn.FieldName = "Apelido";
            ApelidoColumn.VisibleIndex = 1;
            ApelidoColumn.Width = Unit.Percentage(80);

            gridCliente.Columns.Add(IdColumn);
            gridCliente.Columns.Add(ApelidoColumn);
            //

            //Troca de Like pelo Contains no filtro
            UtilitarioGrid.GridFilterContains(gridCliente);

            //Eventos de server-side
            gridCliente.HtmlRowCreated += new ASPxGridViewTableRowEventHandler(this.Grid_HtmlRowCreated);
            gridCliente.CustomDataCallback += new ASPxGridViewCustomDataCallbackEventHandler(this.gridCliente_CustomDataCallBack);

            //Adiciona na popup e retorna a a popup
            popupCliente.Controls.Add(gridCliente);
            return popupCliente;
        }
        public ASPxPopupControl PopupMensagemCliente1() {
            ASPxPopupControl popupMensagem = new ASPxPopupControl();
            popupMensagem.ID = "popupMensagemCliente1";
            popupMensagem = SetCommonSettingsMensagem(popupMensagem); //Seta properties comuns a todos as popups de msg
            popupMensagem.PopupElementID = "btnEditCodigoCliente1";

            return popupMensagem;
        }
        //
        public ASPxPopupControl PopupCarteira(bool hasFiltro) {
            ASPxPopupControl popupCarteira = new ASPxPopupControl();
            popupCarteira = SetCommonSettings(popupCarteira); //Seta properties comuns a todos as popups
            popupCarteira.ClientInstanceName = "popupCarteira";
            popupCarteira.HeaderText = " ";
            popupCarteira.ClientSideEvents.CloseUp = "function(s, e) {gridCarteira.ClearFilter(); }";

            ASPxGridView gridCarteira = new ASPxGridView();
            gridCarteira = SetCommonSettings(gridCarteira); //Seta properties comuns a todos os grids
            gridCarteira.ClientInstanceName = "gridCarteira";
            gridCarteira.DataSourceID = "EsDSCarteira";
            gridCarteira.KeyFieldName = "IdCarteira";
            gridCarteira.SettingsText.Title = "Pesquisa Carteira";

            if (hasFiltro) {
                StringBuilder str = new StringBuilder();
                str.Append("function(s, e) { if (gridCadastro.cp_EditVisibleIndex == -1) ");
                str.Append(" gridCarteira.GetValuesOnCustomCallback(e.visibleIndex, OnGetDataCarteiraFiltro); ");
                str.Append(" else ");
                str.Append("  gridCarteira.GetValuesOnCustomCallback(e.visibleIndex, OnGetDataCarteira); }");
                gridCarteira.ClientSideEvents.RowDblClick = str.ToString();
            }
            else {
                gridCarteira.ClientSideEvents.RowDblClick = "function(s, e) {gridCarteira.GetValuesOnCustomCallback(e.visibleIndex, OnGetDataCarteira);}";
            }

            //Colunas do grid
            GridViewDataTextColumn IdColumn = new GridViewDataTextColumn();
            IdColumn.FieldName = "IdCarteira";
            IdColumn.ReadOnly = true;
            IdColumn.VisibleIndex = 0;
            IdColumn.Width = Unit.Percentage(20);

            GridViewDataTextColumn ApelidoColumn = new GridViewDataTextColumn();
            ApelidoColumn.FieldName = "Apelido";
            ApelidoColumn.VisibleIndex = 1;
            ApelidoColumn.Width = Unit.Percentage(80);

            gridCarteira.Columns.Add(IdColumn);
            gridCarteira.Columns.Add(ApelidoColumn);


            //Troca de Like pelo Contains no filtro
            UtilitarioGrid.GridFilterContains(gridCarteira);

            //Eventos de server-side
            gridCarteira.HtmlRowCreated += new ASPxGridViewTableRowEventHandler(this.Grid_HtmlRowCreated);
            gridCarteira.CustomDataCallback += new ASPxGridViewCustomDataCallbackEventHandler(this.gridCarteira_CustomDataCallBack);
            gridCarteira.CustomCallback += new ASPxGridViewCustomCallbackEventHandler(this.gridCarteira_CustomCallBack);

            //Adiciona na popup e retorna a a popup
            popupCarteira.Controls.Add(gridCarteira);
            return popupCarteira;
        }
        public ASPxPopupControl PopupMensagemCarteira() {
            ASPxPopupControl popupMensagem = new ASPxPopupControl();
            popupMensagem.ID = "popupMensagemCarteira";
            popupMensagem = SetCommonSettingsMensagem(popupMensagem); //Seta properties comuns a todos as popups de msg
            popupMensagem.PopupElementID = "btnEditCodigoCarteira";

            return popupMensagem;
        }
        //
        public ASPxPopupControl PopupCarteira1(bool hasFiltro) {
            ASPxPopupControl popupCarteira = new ASPxPopupControl();
            popupCarteira = SetCommonSettings(popupCarteira); //Seta properties comuns a todos as popups
            popupCarteira.ClientInstanceName = "popupCarteira1";
            popupCarteira.HeaderText = " ";
            popupCarteira.ClientSideEvents.CloseUp = "function(s, e) {gridCarteira1.ClearFilter(); }";

            ASPxGridView gridCarteira = new ASPxGridView();
            gridCarteira = SetCommonSettings(gridCarteira); //Seta properties comuns a todos os grids
            gridCarteira.ClientInstanceName = "gridCarteira1";
            gridCarteira.DataSourceID = "EsDSCarteira1";
            gridCarteira.KeyFieldName = "IdCarteira";
            gridCarteira.SettingsText.Title = "Pesquisa Carteira";

            if (hasFiltro) {
                StringBuilder str = new StringBuilder();
                str.Append("function(s, e) { if (gridCadastro.cp_EditVisibleIndex == -1) ");
                str.Append(" gridCarteira1.GetValuesOnCustomCallback(e.visibleIndex, OnGetDataCarteiraFiltro1); ");
                str.Append(" else ");
                str.Append("  gridCarteira1.GetValuesOnCustomCallback(e.visibleIndex, OnGetDataCarteira1); }");
                gridCarteira.ClientSideEvents.RowDblClick = str.ToString();
            }
            else {
                gridCarteira.ClientSideEvents.RowDblClick = "function(s, e) {gridCarteira1.GetValuesOnCustomCallback(e.visibleIndex, OnGetDataCarteira1);}";
            }

            //Colunas do grid
            GridViewDataTextColumn IdColumn = new GridViewDataTextColumn();
            IdColumn.FieldName = "IdCarteira";
            IdColumn.ReadOnly = true;
            IdColumn.VisibleIndex = 0;
            IdColumn.Width = Unit.Percentage(20);

            GridViewDataTextColumn ApelidoColumn = new GridViewDataTextColumn();
            ApelidoColumn.FieldName = "Apelido";
            ApelidoColumn.VisibleIndex = 1;
            ApelidoColumn.Width = Unit.Percentage(80);

            gridCarteira.Columns.Add(IdColumn);
            gridCarteira.Columns.Add(ApelidoColumn);


            //Troca de Like pelo Contains no filtro
            UtilitarioGrid.GridFilterContains(gridCarteira);

            //Eventos de server-side
            gridCarteira.HtmlRowCreated += new ASPxGridViewTableRowEventHandler(this.Grid_HtmlRowCreated);
            gridCarteira.CustomDataCallback += new ASPxGridViewCustomDataCallbackEventHandler(this.gridCarteira_CustomDataCallBack);

            //Adiciona na popup e retorna a a popup
            popupCarteira.Controls.Add(gridCarteira);
            return popupCarteira;
        }
        public ASPxPopupControl PopupMensagemCarteira1() {
            ASPxPopupControl popupMensagem = new ASPxPopupControl();
            popupMensagem.ID = "popupMensagemCarteira1";
            popupMensagem = SetCommonSettingsMensagem(popupMensagem); //Seta properties comuns a todos as popups de msg
            popupMensagem.PopupElementID = "btnEditCodigoCarteira1";

            return popupMensagem;
        }

        public ASPxPopupControl PopupCarteira2(bool hasFiltro)
        {
            ASPxPopupControl popupCarteira = new ASPxPopupControl();
            popupCarteira = SetCommonSettings(popupCarteira); //Seta properties comuns a todos as popups
            popupCarteira.ClientInstanceName = "popupCarteira2";
            popupCarteira.HeaderText = " ";
            popupCarteira.ClientSideEvents.CloseUp = "function(s, e) {gridCarteira2.ClearFilter(); }";

            ASPxGridView gridCarteira = new ASPxGridView();
            gridCarteira = SetCommonSettings(gridCarteira); //Seta properties comuns a todos os grids
            gridCarteira.ClientInstanceName = "gridCarteira2";
            gridCarteira.DataSourceID = "EsDSCarteira2";
            gridCarteira.KeyFieldName = "IdCarteira";
            gridCarteira.SettingsText.Title = "Pesquisa Carteira";

            if (hasFiltro)
            {
                StringBuilder str = new StringBuilder();
                str.Append("function(s, e) { if (gridCadastro.cp_EditVisibleIndex == -1) ");
                str.Append(" gridCarteira2.GetValuesOnCustomCallback(e.visibleIndex, OnGetDataCarteiraFiltro2); ");
                str.Append(" else ");
                str.Append("  gridCarteira2.GetValuesOnCustomCallback(e.visibleIndex, OnGetDataCarteira2); }");
                gridCarteira.ClientSideEvents.RowDblClick = str.ToString();
            }
            else
            {
                gridCarteira.ClientSideEvents.RowDblClick = "function(s, e) {gridCarteira2.GetValuesOnCustomCallback(e.visibleIndex, OnGetDataCarteira2);}";
            }

            //Colunas do grid
            GridViewDataTextColumn IdColumn = new GridViewDataTextColumn();
            IdColumn.FieldName = "IdCarteira";
            IdColumn.ReadOnly = true;
            IdColumn.VisibleIndex = 0;
            IdColumn.Width = Unit.Percentage(20);

            GridViewDataTextColumn ApelidoColumn = new GridViewDataTextColumn();
            ApelidoColumn.FieldName = "Apelido";
            ApelidoColumn.VisibleIndex = 1;
            ApelidoColumn.Width = Unit.Percentage(80);

            gridCarteira.Columns.Add(IdColumn);
            gridCarteira.Columns.Add(ApelidoColumn);


            //Troca de Like pelo Contains no filtro
            UtilitarioGrid.GridFilterContains(gridCarteira);

            //Eventos de server-side
            gridCarteira.HtmlRowCreated += new ASPxGridViewTableRowEventHandler(this.Grid_HtmlRowCreated);
            gridCarteira.CustomDataCallback += new ASPxGridViewCustomDataCallbackEventHandler(this.gridCarteira_CustomDataCallBack);

            //Adiciona na popup e retorna a a popup
            popupCarteira.Controls.Add(gridCarteira);
            return popupCarteira;
        }
        public ASPxPopupControl PopupMensagemCarteira2()
        {
            ASPxPopupControl popupMensagem = new ASPxPopupControl();
            popupMensagem.ID = "popupMensagemCarteira2";
            popupMensagem = SetCommonSettingsMensagem(popupMensagem); //Seta properties comuns a todos as popups de msg
            popupMensagem.PopupElementID = "btnEditCodigoCarteira2";

            return popupMensagem;
        }

        public ASPxPopupControl PopupCotista(bool hasFiltro) {
            ASPxPopupControl popupCotista = new ASPxPopupControl();
            popupCotista = SetCommonSettings(popupCotista); //Seta properties comuns a todos as popups
            popupCotista.ClientInstanceName = "popupCotista";
            popupCotista.HeaderText = " ";
            popupCotista.ClientSideEvents.CloseUp = "function(s, e) {gridCotista.ClearFilter(); }";

            ASPxGridView gridCotista = new ASPxGridView();
            gridCotista = SetCommonSettings(gridCotista); //Seta properties comuns a todos os grids
            gridCotista.ClientInstanceName = "gridCotista";
            gridCotista.DataSourceID = "EsDSCotista";
            gridCotista.KeyFieldName = "IdCotista";
            gridCotista.SettingsText.Title = "Pesquisa Cotista";

            if (hasFiltro) {
                StringBuilder str = new StringBuilder();
                str.Append("function(s, e) { if (gridCadastro.cp_EditVisibleIndex == -1) ");
                str.Append(" gridCotista.GetValuesOnCustomCallback(e.visibleIndex, OnGetDataCotistaFiltro); ");
                str.Append(" else ");
                str.Append("  gridCotista.GetValuesOnCustomCallback(e.visibleIndex, OnGetDataCotista); }");
                gridCotista.ClientSideEvents.RowDblClick = str.ToString();
            }
            else {
                gridCotista.ClientSideEvents.RowDblClick = "function(s, e) {gridCotista.GetValuesOnCustomCallback(e.visibleIndex, OnGetDataCotista);}";
            }

            //Colunas do grid
            GridViewDataTextColumn IdColumn = new GridViewDataTextColumn();
            IdColumn.FieldName = "IdCotista";
            IdColumn.ReadOnly = true;
            IdColumn.VisibleIndex = 0;
            IdColumn.Width = Unit.Percentage(20);

            GridViewDataTextColumn ApelidoColumn = new GridViewDataTextColumn();
            ApelidoColumn.FieldName = "Apelido";
            ApelidoColumn.VisibleIndex = 1;
            ApelidoColumn.Width = Unit.Percentage(80);

            gridCotista.Columns.Add(IdColumn);
            gridCotista.Columns.Add(ApelidoColumn);
            //

            //Troca de Like pelo Contains no filtro
            UtilitarioGrid.GridFilterContains(gridCotista);

            //Eventos de server-side
            gridCotista.HtmlRowCreated += new ASPxGridViewTableRowEventHandler(this.Grid_HtmlRowCreated);
            gridCotista.CustomDataCallback += new ASPxGridViewCustomDataCallbackEventHandler(this.gridCotista_CustomDataCallBack);

            //Adiciona na popup e retorna a a popup
            popupCotista.Controls.Add(gridCotista);
            return popupCotista;
        }
        public ASPxPopupControl PopupMensagemCotista() {
            ASPxPopupControl popupMensagem = new ASPxPopupControl();
            popupMensagem.ID = "popupMensagemCotista";
            popupMensagem = SetCommonSettingsMensagem(popupMensagem); //Seta properties comuns a todos as popups de msg
            popupMensagem.PopupElementID = "btnEditCodigoCotista";

            return popupMensagem;
        }
        //
        public ASPxPopupControl PopupAtivoBolsa(bool hasPopupAtivoBolsa2) {
            ASPxPopupControl popupAtivoBolsa = new ASPxPopupControl();
            popupAtivoBolsa = SetCommonSettings(popupAtivoBolsa); //Seta properties comuns a todos as popups
            popupAtivoBolsa.ClientInstanceName = "popupAtivoBolsa";
            popupAtivoBolsa.HeaderText = " ";
            popupAtivoBolsa.ClientSideEvents.CloseUp = "function(s, e) {gridAtivoBolsa.ClearFilter(); }";

            ASPxGridView gridAtivoBolsa = new ASPxGridView();
            gridAtivoBolsa = SetCommonSettings(gridAtivoBolsa); //Seta properties comuns a todos os grids
            gridAtivoBolsa.ClientInstanceName = "gridAtivoBolsa";
            gridAtivoBolsa.DataSourceID = "EsDSAtivoBolsa";
            gridAtivoBolsa.KeyFieldName = "CdAtivoBolsa";
            gridAtivoBolsa.SettingsText.Title = "Pesquisa Ativos de Bolsa";

            if (hasPopupAtivoBolsa2) {
                StringBuilder str = new StringBuilder();
                str.Append("function(s, e) { if (browseAtivoBolsa2 == false) { ");
                str.Append("  gridAtivoBolsa.GetValuesOnCustomCallback(e.visibleIndex, OnGetDataAtivoBolsa); ");
                str.Append(" } ");
                str.Append(" else { ");
                str.Append(" browseAtivoBolsa2 == false; ");
                str.Append("  gridAtivoBolsa.GetValuesOnCustomCallback(e.visibleIndex, OnGetDataAtivoBolsa2); }");
                str.Append(" } ");
                gridAtivoBolsa.ClientSideEvents.RowDblClick = str.ToString();
            }
            else {
                gridAtivoBolsa.ClientSideEvents.RowDblClick = "function(s, e) {gridAtivoBolsa.GetValuesOnCustomCallback(e.visibleIndex, OnGetDataAtivoBolsa);}";
            }

            //Colunas do grid
            GridViewDataTextColumn IdColumn = new GridViewDataTextColumn();
            IdColumn.FieldName = "CdAtivoBolsa";
            IdColumn.Caption = "Código";
            IdColumn.ReadOnly = true;
            IdColumn.VisibleIndex = 0;
            IdColumn.Width = Unit.Percentage(20);

            GridViewDataTextColumn DescricaoColumn = new GridViewDataTextColumn();
            DescricaoColumn.FieldName = "Descricao";
            DescricaoColumn.Caption = "Descrição";
            DescricaoColumn.VisibleIndex = 1;
            DescricaoColumn.Width = Unit.Percentage(80);

            gridAtivoBolsa.Columns.Add(IdColumn);
            gridAtivoBolsa.Columns.Add(DescricaoColumn);
            //

            //Troca de Like pelo Contains no filtro
            UtilitarioGrid.GridFilterContains(gridAtivoBolsa);

            //Eventos de server-side
            gridAtivoBolsa.HtmlRowCreated += new ASPxGridViewTableRowEventHandler(this.Grid_HtmlRowCreated);
            gridAtivoBolsa.CustomDataCallback += new ASPxGridViewCustomDataCallbackEventHandler(this.gridAtivoBolsa_CustomDataCallBack);

            //Adiciona na popup e retorna a a popup
            popupAtivoBolsa.Controls.Add(gridAtivoBolsa);
            return popupAtivoBolsa;
        }
        public ASPxPopupControl PopupMensagemAtivoBolsa() {
            ASPxPopupControl popupMensagem = new ASPxPopupControl();
            popupMensagem.ID = "popupMensagemAtivoBolsa";
            popupMensagem = SetCommonSettingsMensagem(popupMensagem); //Seta properties comuns a todos as popups de msg
            popupMensagem.PopupElementID = "btnEditAtivoBolsa";

            return popupMensagem;
        }
        //
        public ASPxPopupControl PopupAtivoBMF() {
            ASPxPopupControl popupAtivoBMF = new ASPxPopupControl();
            popupAtivoBMF = SetCommonSettings(popupAtivoBMF); //Seta properties comuns a todos as popups
            popupAtivoBMF.ClientInstanceName = "popupAtivoBMF";
            popupAtivoBMF.HeaderText = " ";
            popupAtivoBMF.ClientSideEvents.CloseUp = "function(s, e) {gridAtivoBMF.ClearFilter(); }";

            ASPxGridView gridAtivoBMF = new ASPxGridView();
            gridAtivoBMF = SetCommonSettings(gridAtivoBMF); //Seta properties comuns a todos os grids
            gridAtivoBMF.ClientInstanceName = "gridAtivoBMF";
            gridAtivoBMF.DataSourceID = "EsDSAtivoBMF";
            gridAtivoBMF.KeyFieldName = "CdAtivoBMF";
            gridAtivoBMF.SettingsText.Title = "Pesquisa Ativos de BMF";
            gridAtivoBMF.ClientSideEvents.RowDblClick = "function(s, e) {gridAtivoBMF.GetValuesOnCustomCallback(e.visibleIndex, OnGetDataAtivoBMF);}";

            //Colunas do grid
            GridViewDataTextColumn IdColumn = new GridViewDataTextColumn();
            IdColumn.FieldName = "CdAtivoBMF";
            IdColumn.Caption = "Código";
            IdColumn.ReadOnly = true;
            IdColumn.VisibleIndex = 0;
            IdColumn.Width = Unit.Percentage(15);

            GridViewDataTextColumn DescricaoColumn = new GridViewDataTextColumn();
            DescricaoColumn.FieldName = "Serie";
            DescricaoColumn.Caption = "Série";
            DescricaoColumn.VisibleIndex = 1;
            DescricaoColumn.Width = Unit.Percentage(85);

            gridAtivoBMF.Columns.Add(IdColumn);
            gridAtivoBMF.Columns.Add(DescricaoColumn);
            //

            //Troca de Like pelo Contains no filtro
            UtilitarioGrid.GridFilterContains(gridAtivoBMF);

            //Eventos de server-side
            gridAtivoBMF.HtmlRowCreated += new ASPxGridViewTableRowEventHandler(this.Grid_HtmlRowCreated);
            gridAtivoBMF.CustomDataCallback += new ASPxGridViewCustomDataCallbackEventHandler(this.gridAtivoBMF_CustomDataCallBack);

            //Adiciona na popup e retorna a a popup
            popupAtivoBMF.Controls.Add(gridAtivoBMF);
            return popupAtivoBMF;
        }
        public ASPxPopupControl PopupMensagemAtivoBMF() {
            ASPxPopupControl popupMensagem = new ASPxPopupControl();
            popupMensagem.ID = "popupMensagemAtivoBMF";
            popupMensagem = SetCommonSettingsMensagem(popupMensagem); //Seta properties comuns a todos as popups de msg
            popupMensagem.PopupElementID = "btnEditAtivoBMF";

            return popupMensagem;
        }
        //
        public ASPxPopupControl PopupTituloRF() {
            ASPxPopupControl popupTituloRF = new ASPxPopupControl();
            popupTituloRF = SetCommonSettings(popupTituloRF); //Seta properties comuns a todos as popups
            popupTituloRF.ClientInstanceName = "popupTituloRF";
            popupTituloRF.HeaderText = " ";
            popupTituloRF.ClientSideEvents.CloseUp = "function(s, e) {gridTituloRF.ClearFilter(); }";
            popupTituloRF.ClientSideEvents.PopUp = "function(s, e) {gridTituloRF.PerformCallback(); }";

            ASPxGridView gridTituloRF = new ASPxGridView();
            gridTituloRF = SetCommonSettings(gridTituloRF); //Seta properties comuns a todos os grids
            gridTituloRF.ClientInstanceName = "gridTituloRF";
            gridTituloRF.DataSourceID = "EsDSTituloRF";
            gridTituloRF.KeyFieldName = "CdTituloRF";
            gridTituloRF.SettingsText.Title = "Pesquisa Títulos de Renda Fixa";
            gridTituloRF.ClientSideEvents.RowDblClick = "function(s, e) {gridTituloRF.GetValuesOnCustomCallback(e.visibleIndex, OnGetDataTituloRF);}";

            //Colunas do grid
            GridViewDataTextColumn IdColumn = new GridViewDataTextColumn();
            IdColumn.FieldName = "IdTitulo";
            IdColumn.Caption = "Id Título";
            IdColumn.VisibleIndex = 0;
            IdColumn.Width = Unit.Percentage(10);

            GridViewDataTextColumn DescricaoColumn = new GridViewDataTextColumn();
            DescricaoColumn.FieldName = "Descricao";
            DescricaoColumn.Caption = "Descrição";
            DescricaoColumn.VisibleIndex = 1;
            DescricaoColumn.Width = Unit.Percentage(30);
            DescricaoColumn.Settings.AutoFilterCondition = AutoFilterCondition.Contains;

            GridViewDataDateColumn DataEmissaoColumn = new GridViewDataDateColumn();
            DataEmissaoColumn.FieldName = "DataEmissao";
            DataEmissaoColumn.Caption = "Emissão";
            DataEmissaoColumn.VisibleIndex = 2;
            DataEmissaoColumn.Width = Unit.Percentage(12);

            GridViewDataDateColumn DataVencimentoColumn = new GridViewDataDateColumn();
            DataVencimentoColumn.FieldName = "DataVencimento";
            DataVencimentoColumn.Caption = "Vencimento";
            DataVencimentoColumn.VisibleIndex = 3;
            DataVencimentoColumn.Width = Unit.Percentage(12);

            GridViewDataTextColumn CodigoCustodiaColumn = new GridViewDataTextColumn();
            CodigoCustodiaColumn.FieldName = "CodigoCustodia";
            CodigoCustodiaColumn.Caption = "Cod.Anbima";
            CodigoCustodiaColumn.VisibleIndex = 4;
            CodigoCustodiaColumn.Width = Unit.Percentage(8);
            CodigoCustodiaColumn.Settings.AutoFilterCondition = AutoFilterCondition.Contains;

            GridViewDataTextColumn CodigoCBLCColumn = new GridViewDataTextColumn();
            CodigoCBLCColumn.FieldName = "CodigoCBLC";
            CodigoCBLCColumn.Caption = "Cod.CBLC";
            CodigoCBLCColumn.VisibleIndex = 5;
            CodigoCBLCColumn.Width = Unit.Percentage(8);
            CodigoCBLCColumn.Settings.AutoFilterCondition = AutoFilterCondition.Contains;

            GridViewDataTextColumn CodigoCetipColumn = new GridViewDataTextColumn();
            CodigoCetipColumn.FieldName = "CodigoCetip";
            CodigoCetipColumn.Caption = "Cod.Cetip";
            CodigoCetipColumn.VisibleIndex = 6;
            CodigoCetipColumn.Width = Unit.Percentage(8);
            CodigoCetipColumn.Settings.AutoFilterCondition = AutoFilterCondition.Contains;

            GridViewDataTextColumn CodigoISINColumn = new GridViewDataTextColumn();
            CodigoISINColumn.FieldName = "CodigoIsin";
            CodigoISINColumn.Caption = "Cod.ISIN";
            CodigoISINColumn.VisibleIndex = 7;
            CodigoISINColumn.Width = Unit.Percentage(12);
            CodigoISINColumn.Settings.AutoFilterCondition = AutoFilterCondition.Contains;

            gridTituloRF.Columns.Add(IdColumn);
            gridTituloRF.Columns.Add(DescricaoColumn);
            gridTituloRF.Columns.Add(DataEmissaoColumn);
            gridTituloRF.Columns.Add(DataVencimentoColumn);
            gridTituloRF.Columns.Add(CodigoCustodiaColumn);
            gridTituloRF.Columns.Add(CodigoCBLCColumn);
            gridTituloRF.Columns.Add(CodigoCetipColumn);
            gridTituloRF.Columns.Add(CodigoISINColumn);
            //

            //Eventos de server-side
            gridTituloRF.HtmlRowCreated += new ASPxGridViewTableRowEventHandler(this.Grid_HtmlRowCreated);
            gridTituloRF.CustomDataCallback += new ASPxGridViewCustomDataCallbackEventHandler(this.gridTituloRF_CustomDataCallBack);
            gridTituloRF.CustomCallback += new ASPxGridViewCustomCallbackEventHandler(this.gridTituloRF_CustomCallBack);

            //Adiciona na popup e retorna a a popup
            popupTituloRF.Controls.Add(gridTituloRF);
            return popupTituloRF;
        }
        public ASPxPopupControl PopupMensagemTituloRF() {
            ASPxPopupControl popupMensagem = new ASPxPopupControl();
            popupMensagem.ID = "popupMensagemTituloRF";
            popupMensagem = SetCommonSettingsMensagem(popupMensagem); //Seta properties comuns a todos as popups de msg
            popupMensagem.PopupElementID = "btnEditTituloRF";

            return popupMensagem;
        }
        //
        public ASPxPopupControl PopupEmissor() {
            ASPxPopupControl popupEmissor = new ASPxPopupControl();
            popupEmissor = SetCommonSettings(popupEmissor); //Seta properties comuns a todos as popups
            popupEmissor.ClientInstanceName = "popupEmissor";
            popupEmissor.HeaderText = " ";
            popupEmissor.ClientSideEvents.CloseUp = "function(s, e) {gridEmissor.ClearFilter(); }";
            popupEmissor.ClientSideEvents.PopUp = "function(s, e) {gridEmissor.PerformCallback(); }";

            ASPxGridView gridEmissor = new ASPxGridView();
            gridEmissor = SetCommonSettings(gridEmissor); //Seta properties comuns a todos os grids
            gridEmissor.ClientInstanceName = "gridEmissor";
            gridEmissor.DataSourceID = "EsDSEmissor";
            gridEmissor.KeyFieldName = "IdEmissor";
            gridEmissor.SettingsText.Title = "Pesquisa De Emissor";
            gridEmissor.ClientSideEvents.RowDblClick = "function(s, e) {gridEmissor.GetValuesOnCustomCallback(e.visibleIndex, OnGetDataEmissor);}";

            //Colunas do grid
            GridViewDataTextColumn IdColumn = new GridViewDataTextColumn();
            IdColumn.FieldName = "IdEmissor";
            IdColumn.VisibleIndex = 0;
            IdColumn.Width = Unit.Percentage(15);

            GridViewDataTextColumn NomeColumn = new GridViewDataTextColumn();
            NomeColumn.FieldName = "Nome";
            NomeColumn.Caption = "Nome";
            NomeColumn.VisibleIndex = 1;
            NomeColumn.Width = Unit.Percentage(50);

            gridEmissor.Columns.Add(IdColumn);
            gridEmissor.Columns.Add(NomeColumn);
            //

            //Eventos de server-side
            gridEmissor.HtmlRowCreated += new ASPxGridViewTableRowEventHandler(this.Grid_HtmlRowCreated);
            gridEmissor.CustomDataCallback += new ASPxGridViewCustomDataCallbackEventHandler(this.gridEmissor_CustomDataCallBack);
            gridEmissor.CustomCallback += new ASPxGridViewCustomCallbackEventHandler(this.gridEmissor_CustomCallBack);

            //Adiciona na popup e retorna a popup
            popupEmissor.Controls.Add(gridEmissor);
            return popupEmissor;
        }
        public ASPxPopupControl PopupMensagemEmissor() {
            ASPxPopupControl popupMensagem = new ASPxPopupControl();
            popupMensagem.ID = "popupMensagemEmissor";
            popupMensagem = SetCommonSettingsMensagem(popupMensagem); //Seta properties comuns a todos as popups de msg
            popupMensagem.PopupElementID = "btnEditEmissor";

            return popupMensagem;
        }

        public ASPxPopupControl PopupContabConta()
        {
            ASPxPopupControl popupContabConta = new ASPxPopupControl();
            popupContabConta = SetCommonSettings(popupContabConta); //Seta properties comuns a todos as popups
            popupContabConta.ClientInstanceName = "popupContabConta";
            popupContabConta.HeaderText = " ";
            popupContabConta.ClientSideEvents.CloseUp = "function(s, e) {gridContabConta.ClearFilter(); }";
            popupContabConta.ClientSideEvents.PopUp = "function(s, e) {gridContabConta.PerformCallback(); }";

            ASPxGridView gridContabConta = new ASPxGridView();
            gridContabConta = SetCommonSettings(gridContabConta); //Seta properties comuns a todos os grids
            gridContabConta.ClientInstanceName = "gridContabConta";
            gridContabConta.DataSourceID = "EsDSContabConta";
            gridContabConta.KeyFieldName = "IdConta";
            gridContabConta.SettingsText.Title = "Pesquisa Contas Contábeis";
            gridContabConta.ClientSideEvents.RowDblClick = "function(s, e) {gridContabConta.GetValuesOnCustomCallback(e.visibleIndex, OnGetData);}";

            //Colunas do grid
            GridViewDataTextColumn PlanoColumn = new GridViewDataTextColumn();
            PlanoColumn.FieldName = "DescricaoPlano";
            PlanoColumn.Caption = "Plano Contábil";
            PlanoColumn.VisibleIndex = 0;
            PlanoColumn.Width = Unit.Percentage(20);

            GridViewDataTextColumn IdColumn = new GridViewDataTextColumn();
            IdColumn.FieldName = "IdConta";
            IdColumn.VisibleIndex = 1;
            IdColumn.Width = Unit.Percentage(10);

            GridViewDataTextColumn DescricaoColumn = new GridViewDataTextColumn();
            DescricaoColumn.FieldName = "Descricao";
            DescricaoColumn.Caption = "Descrição";
            DescricaoColumn.VisibleIndex = 2;
            DescricaoColumn.Settings.AutoFilterCondition = AutoFilterCondition.Contains;
            DescricaoColumn.Width = Unit.Percentage(35);

            GridViewDataTextColumn CodigoColumn = new GridViewDataTextColumn();
            CodigoColumn.FieldName = "Codigo";
            CodigoColumn.Caption = "Código";
            CodigoColumn.VisibleIndex = 3;
            CodigoColumn.Settings.AutoFilterCondition = AutoFilterCondition.Contains;
            CodigoColumn.Width = Unit.Percentage(25);

            GridViewDataTextColumn CodigoReduzidaColumn = new GridViewDataTextColumn();
            CodigoReduzidaColumn.FieldName = "CodigoReduzida";
            CodigoReduzidaColumn.Caption = "Código Reduzida";
            CodigoReduzidaColumn.VisibleIndex = 4;
            CodigoReduzidaColumn.Width = Unit.Percentage(15);

            gridContabConta.Columns.Add(PlanoColumn);
            gridContabConta.Columns.Add(IdColumn);
            gridContabConta.Columns.Add(DescricaoColumn);
            gridContabConta.Columns.Add(CodigoColumn);
            gridContabConta.Columns.Add(CodigoReduzidaColumn);
            //

            //Eventos de server-side
            gridContabConta.HtmlRowCreated += new ASPxGridViewTableRowEventHandler(this.Grid_HtmlRowCreated);
            gridContabConta.CustomDataCallback += new ASPxGridViewCustomDataCallbackEventHandler(this.gridContabConta_CustomDataCallBack);
            gridContabConta.CustomCallback += new ASPxGridViewCustomCallbackEventHandler(this.gridContabConta_CustomCallBack);

            //Adiciona na popup e retorna a a popup
            popupContabConta.Controls.Add(gridContabConta);
            return popupContabConta;
        }
        public ASPxPopupControl PopupMensagemContabConta()
        {
            ASPxPopupControl popupMensagem = new ASPxPopupControl();
            popupMensagem.ID = "popupMensagemContabConta";
            popupMensagem = SetCommonSettingsMensagem(popupMensagem); //Seta properties comuns a todos as popups de msg
            popupMensagem.PopupElementID = "btnEditConta";

            return popupMensagem;
        }

        public ASPxPopupControl PopupAgenteMercado(bool hasFiltro)
        {
            ASPxPopupControl popupAgenteMercado = new ASPxPopupControl();
            popupAgenteMercado = SetCommonSettings(popupAgenteMercado); //Seta properties comuns a todos as popups
            popupAgenteMercado.ClientInstanceName = "popupAgenteMercado";
            popupAgenteMercado.HeaderText = " ";
            popupAgenteMercado.ClientSideEvents.CloseUp = "function(s, e) {gridAgenteMercado.ClearFilter(); }";

            ASPxGridView gridAgenteMercado = new ASPxGridView();
            gridAgenteMercado = SetCommonSettings(gridAgenteMercado); //Seta properties comuns a todos os grids
            gridAgenteMercado.ClientInstanceName = "gridAgenteMercado";
            gridAgenteMercado.DataSourceID = "EsDSAgenteMercado";
            gridAgenteMercado.KeyFieldName = "IdAgente";
            gridAgenteMercado.SettingsText.Title = "Pesquisa Agente Mercado";

            if (hasFiltro)
            {
                StringBuilder str = new StringBuilder();
                str.Append("function(s, e) { if (gridCadastro.cp_EditVisibleIndex == -1) ");
                str.Append(" gridAgenteMercado.GetValuesOnCustomCallback(e.visibleIndex, OnGetDataAgenteMercadoFiltro); ");
                str.Append(" else ");
                str.Append("  gridAgenteMercado.GetValuesOnCustomCallback(e.visibleIndex, OnGetDataAgenteMercado); }");
                gridAgenteMercado.ClientSideEvents.RowDblClick = str.ToString();
            }
            else
            {
                gridAgenteMercado.ClientSideEvents.RowDblClick = "function(s, e) {gridAgenteMercado.GetValuesOnCustomCallback(e.visibleIndex, OnGetDataAgenteMercado);}";
            }

            //Colunas do grid
            GridViewDataTextColumn IdColumn = new GridViewDataTextColumn();
            IdColumn.FieldName = "IdAgente";
            IdColumn.ReadOnly = true;
            IdColumn.VisibleIndex = 0;
            IdColumn.Width = Unit.Percentage(20);

            GridViewDataTextColumn ApelidoColumn = new GridViewDataTextColumn();
            ApelidoColumn.FieldName = "Apelido";
            ApelidoColumn.VisibleIndex = 1;
            ApelidoColumn.Width = Unit.Percentage(80);

            gridAgenteMercado.Columns.Add(IdColumn);
            gridAgenteMercado.Columns.Add(ApelidoColumn);


            //Troca de Like pelo Contains no filtro
            UtilitarioGrid.GridFilterContains(gridAgenteMercado);

            //Eventos de server-side
            gridAgenteMercado.HtmlRowCreated += new ASPxGridViewTableRowEventHandler(this.Grid_HtmlRowCreated);
            gridAgenteMercado.CustomDataCallback += new ASPxGridViewCustomDataCallbackEventHandler(this.gridAgenteMercado_CustomDataCallBack);

            //Adiciona na popup e retorna a a popup
            popupAgenteMercado.Controls.Add(gridAgenteMercado);
            return popupAgenteMercado;
        }
        public ASPxPopupControl PopupMensagemAgenteMercado()
        {
            ASPxPopupControl popupMensagem = new ASPxPopupControl();
            popupMensagem.ID = "popupMensagemAgenteMercado";
            popupMensagem = SetCommonSettingsMensagem(popupMensagem); //Seta properties comuns a todos as popups de msg
            popupMensagem.PopupElementID = "btnEditAgenteMercado";

            return popupMensagem;
        }

        #endregion

        /// <summary>
        /// TextBoxes de mensagem (NaoExiste, Inativo, Inexistente)
        /// </summary>
        /// <returns></returns>
        public List<System.Web.UI.WebControls.TextBox> ListaMensagens() {
            List<System.Web.UI.WebControls.TextBox> listaMensagens = new List<System.Web.UI.WebControls.TextBox>();
            const string styleMsg = "display:none";

            System.Web.UI.WebControls.TextBox textMsgNaoExiste = new System.Web.UI.WebControls.TextBox();
            textMsgNaoExiste.Attributes["style"] = styleMsg;
            textMsgNaoExiste.ID = "textMsgNaoExiste";
            textMsgNaoExiste.Text = "Inexistente!";

            System.Web.UI.WebControls.TextBox textMsgInativo = new System.Web.UI.WebControls.TextBox();
            textMsgInativo.Attributes["style"] = styleMsg;
            textMsgInativo.ID = "textMsgInativo";
            textMsgInativo.Text = "Inativo!";

            System.Web.UI.WebControls.TextBox textMsgUsuarioSemAcesso = new System.Web.UI.WebControls.TextBox();
            textMsgUsuarioSemAcesso.Attributes["style"] = styleMsg;
            textMsgUsuarioSemAcesso.ID = "textMsgUsuarioSemAcesso";
            textMsgUsuarioSemAcesso.Text = "Usuário sem acesso ou código inválido para este tipo de operação!";

            System.Web.UI.WebControls.TextBox textMsgStatusFechado = new System.Web.UI.WebControls.TextBox();
            textMsgStatusFechado.Attributes["style"] = styleMsg;
            textMsgStatusFechado.ID = "textMsgStatusFechado";
            textMsgStatusFechado.Text = "Status está fechado!";

            listaMensagens.Add(textMsgNaoExiste);
            listaMensagens.Add(textMsgInativo);
            listaMensagens.Add(textMsgUsuarioSemAcesso);
            listaMensagens.Add(textMsgStatusFechado);

            return listaMensagens;
        }
    }
}