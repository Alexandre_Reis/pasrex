using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Financial.Security;
using Financial.Web.Util;

/// <summary>
/// Summary description for UsuarioUI
/// </summary>

namespace Financial.Web
{
    public class UsuarioUI
    {
        private Financial.Security.Usuario usuario;
        public UsuarioUI()
        {
            this.usuario = new Financial.Security.Usuario();
        }

        public bool LoadByPrimaryKey(int idUsuario)
        {
            return this.usuario.LoadByPrimaryKey(idUsuario);
        }

        public ActionFromUIOutput SaveFromUI(System.Collections.Specialized.NameValueCollection webParams)
        {
            //string debug = "here";
            return new ActionFromUIOutput(0, null, null);
        }

    }
}