﻿using System;
using System.IO;
using System.Data;
using System.Xml;
using System.Text;
using Financial.Bolsa;
using Financial.CRM;
using System.Globalization;
using Financial.Common;
using Financial.Util;
using Financial.BMF;
using Financial.ContaCorrente;
using System.Xml.XPath;
using Financial.Bolsa.Enums;
using Financial.Fundo;

namespace Financial.Batimentos {
    public class XmlAnbid1 {

        /* Sessões do Arquivo XML */
        #region Sessões
        const string NODE_HEADER = "/arquivoposicao_4_01/carteira/header";
        const string NODE_TITULO_PUBLICO = "/arquivoposicao_4_01/carteira/titpublico";
        const string NODE_TITULO_PRIVADO = "/arquivoposicao_4_01/carteira/titprivado";
        const string NODE_DEBENTURE = "/arquivoposicao_4_01/carteira/debenture";
        const string NODE_ACOES = "/arquivoposicao_4_01/carteira/acoes";
        const string NODE_OPCOESACOES = "/arquivoposicao_4_01/carteira/opcoesacoes";
        const string NODE_OPCOESFLX = "/arquivoposicao_4_01/carteira/opcoesflx";
        const string NODE_TERMO_RV = "/arquivoposicao_4_01/carteira/termorv";
        const string NODE_FUTUROS = "/arquivoposicao_4_01/carteira/futuros";
        const string NODE_SWAP = "/arquivoposicao_4_01/carteira/swap";
        const string NODE_CAIXA = "/arquivoposicao_4_01/carteira/caixa";
        const string NODE_COTAS = "/arquivoposicao_4_01/carteira/cotas";
        const string NODE_DESPESAS = "/arquivoposicao_4_01/carteira/despesas";
        const string NODE_OUTRAS_DESPESAS = "/arquivoposicao_4_01/carteira/outrasdespesas";
        const string NODE_PROVISAO = "/arquivoposicao_4_01/carteira/provisao";
        #endregion

        /* Nomes Dos DataTables */
        #region DataTables Names
        const string HEADER = "header";
        const string TITULO_PUBLICO = "titpublico";
        const string TITULO_PRIVADO = "titprivado";
        const string DEBENTURE = "debenture";
        const string ACOES = "acoes";
        const string OPCOESACOES = "opcoesacoes";
        const string OPCOESFLX = "opcoesflx";
        const string TERMO_RV = "termorv";
        const string FUTUROS = "futuros";
        const string SWAP = "swap";
        const string CAIXA = "caixa";
        const string COTAS = "cotas";
        const string DESPESAS = "despesas";
        const string OUTRAS_DESPESAS = "outrasdespesas";
        const string PROVISAO = "provisao";
        #endregion

        // DataSet Com os DataTables do ArquivoXML
        private DataSet dataSet = new DataSet();
         
        #region Secoes do ArquivoXML
        public class Secoes {
            private XmlAnbid1 x;

            /* Construtor */
            public Secoes(XmlAnbid1 x) {
                this.x = x;
            }

            public DataTable GetHeader {
                get { return this.x.dataSet.Tables[HEADER] != null ? this.x.dataSet.Tables[HEADER] : new DataTable(); }
            }

            public DataTable GetTituloPublico {
                get { return this.x.dataSet.Tables[TITULO_PUBLICO] != null ? this.x.dataSet.Tables[TITULO_PUBLICO] : new DataTable(); }
            }

            public DataTable GetTituloPrivado {
                get { return this.x.dataSet.Tables[TITULO_PRIVADO] != null ? this.x.dataSet.Tables[TITULO_PRIVADO] : new DataTable(); }
            }

            public DataTable GetDebentures {
                get { return this.x.dataSet.Tables[DEBENTURE] != null ? this.x.dataSet.Tables[DEBENTURE] : new DataTable(); }
            }

            public DataTable GetAcoes {
                get { return this.x.dataSet.Tables[ACOES] != null ? this.x.dataSet.Tables[ACOES] : new DataTable(); }
            }

            public DataTable GetOpcoesAcoes {
                get { return this.x.dataSet.Tables[OPCOESACOES] != null ? this.x.dataSet.Tables[OPCOESACOES] : new DataTable(); }
            }

            public DataTable GetOpcoesFlx {
                get { return this.x.dataSet.Tables[OPCOESFLX] != null ? this.x.dataSet.Tables[OPCOESFLX] : new DataTable(); }
            }

            public DataTable GetFuturos {
                get { return this.x.dataSet.Tables[FUTUROS] != null ? this.x.dataSet.Tables[FUTUROS] : new DataTable(); }
            }

            public DataTable GetSwap {
                get { return this.x.dataSet.Tables[SWAP] != null ? this.x.dataSet.Tables[SWAP] : new DataTable(); }
            }

            public DataTable GetCaixa {
                get { return this.x.dataSet.Tables[CAIXA] != null ? this.x.dataSet.Tables[CAIXA] : new DataTable(); }
            }

            public DataTable GetCotas {
                get { return this.x.dataSet.Tables[COTAS] != null ? this.x.dataSet.Tables[COTAS] : new DataTable(); }
            }

            public DataTable GetDespesas {
                get { return this.x.dataSet.Tables[DESPESAS] != null ? this.x.dataSet.Tables[DESPESAS] : new DataTable(); }
            }

            public DataTable GetOutrasDespesas {
                get { return this.x.dataSet.Tables[OUTRAS_DESPESAS] != null ? this.x.dataSet.Tables[OUTRAS_DESPESAS] : new DataTable(); }
            }

            public DataTable GetProvisao {
                get { return this.x.dataSet.Tables[PROVISAO] != null ? this.x.dataSet.Tables[PROVISAO] : new DataTable(); }
            }

            public DataTable GetTermos {
                get { return this.x.dataSet.Tables[TERMO_RV] != null ? this.x.dataSet.Tables[TERMO_RV] : new DataTable(); }
            }
        }
        #endregion

        /* Guarda a Seção de Ações do Arquivo XML */
        //private PosicaoBolsaCollection posicaoBolsaCollection = new PosicaoBolsaCollection();

        //public PosicaoBolsaCollection CollectionPosicaoBolsa {
        //    get { return posicaoBolsaCollection; }            
        //}

        public Secoes Secao { 
            get { return new Secoes(this); }
        }

        /// <summary>
        /// Lê um arquivo XML com as seguintes seções:
        /// Header, TituloPublico, TituloPrivado, Debenture, Acões, OpçõesAcoes, OpçõesFlx, Futuros, Swap, 
        /// Caixa, Cotas, Despesas, OutrasDespesas, Provisão, TermoRV
        /// </summary>
        /// <param name="nomeArquivoCompleto"></param>
        /// <exception cref="">
        /// throws Exception Se Arquivo não existir,
        /// Se o arquivo nao for um arquivo XML
        /// Se não existir o nó /arquivoposicao_4_01/carteira/header no arquivo XML
        /// </exception>
        public void LerArquivoXML(StreamReader sr) {
            
            XmlDocument xml = new XmlDocument();
            xml.LoadXml(sr.ReadToEnd());
            
            // Volta o Ponteiro para o Começo 
            sr.BaseStream.Seek(0, SeekOrigin.Begin);

            #region Header
            XmlNodeList xnList = xml.SelectNodes(NODE_HEADER);

            if (xnList.Count ==0) {
                throw new Exception("Arquivo XML no formato Incorreto: Node Header Incorreto.");
            }

            foreach (XmlNode xn in xnList) {
                byte[] xmlBuffer = ASCIIEncoding.ASCII.GetBytes(xn.OuterXml);
                MemoryStream reader = new MemoryStream(xmlBuffer);

                this.dataSet.ReadXml(reader, XmlReadMode.InferSchema);
            }
            #endregion
            
            #region Titulo Publico
            xnList = xml.SelectNodes(NODE_TITULO_PUBLICO);
            foreach (XmlNode xn in xnList) {
                byte[] xmlBuffer = ASCIIEncoding.ASCII.GetBytes(xn.OuterXml);
                MemoryStream reader = new MemoryStream(xmlBuffer);
                //
                this.dataSet.ReadXml(reader, XmlReadMode.InferSchema);
            }
            #endregion

            #region Titulo Privado
            xnList = xml.SelectNodes(NODE_TITULO_PRIVADO);
            foreach (XmlNode xn in xnList) {
                byte[] xmlBuffer = ASCIIEncoding.ASCII.GetBytes(xn.OuterXml);
                MemoryStream reader = new MemoryStream(xmlBuffer);
                //
                this.dataSet.ReadXml(reader, XmlReadMode.InferSchema);
            }
            #endregion

            #region Debenture
            xnList = xml.SelectNodes(NODE_DEBENTURE);
            foreach (XmlNode xn in xnList) {
                byte[] xmlBuffer = ASCIIEncoding.ASCII.GetBytes(xn.OuterXml);
                MemoryStream reader = new MemoryStream(xmlBuffer);
                //
                 this.dataSet.ReadXml(reader, XmlReadMode.InferSchema);
            }
            #endregion

            #region Acoes
            xnList = xml.SelectNodes(NODE_ACOES);
            foreach (XmlNode xn in xnList) {
                byte[] xmlBuffer = ASCIIEncoding.ASCII.GetBytes(xn.OuterXml);
                MemoryStream reader = new MemoryStream(xmlBuffer);
                //
                 this.dataSet.ReadXml(reader, XmlReadMode.InferSchema);
             }
            #endregion

            #region OpcoesAcoes
             xnList = xml.SelectNodes(NODE_OPCOESACOES);
             foreach (XmlNode xn in xnList) {
                 byte[] xmlBuffer = ASCIIEncoding.ASCII.GetBytes(xn.OuterXml);
                 MemoryStream reader = new MemoryStream(xmlBuffer);
                 //
                 this.dataSet.ReadXml(reader, XmlReadMode.InferSchema);
             }
             #endregion

            #region Opcoes flx 
             xnList = xml.SelectNodes(NODE_OPCOESFLX);
            foreach (XmlNode xn in xnList) {
                byte[] xmlBuffer = ASCIIEncoding.ASCII.GetBytes(xn.OuterXml);
                MemoryStream reader = new MemoryStream(xmlBuffer);
                //
                 this.dataSet.ReadXml(reader, XmlReadMode.InferSchema);
             }
            #endregion

            #region Futuros
             xnList = xml.SelectNodes(NODE_FUTUROS);
            foreach (XmlNode xn in xnList) {
                byte[] xmlBuffer = ASCIIEncoding.ASCII.GetBytes(xn.OuterXml);
                MemoryStream reader = new MemoryStream(xmlBuffer);
                //
                this.dataSet.ReadXml(reader, XmlReadMode.InferSchema);
            }
            #endregion

            #region Swap
            xnList = xml.SelectNodes(NODE_SWAP);
            foreach (XmlNode xn in xnList) {
                byte[] xmlBuffer = ASCIIEncoding.ASCII.GetBytes(xn.OuterXml);
                MemoryStream reader = new MemoryStream(xmlBuffer);
                //
                this.dataSet.ReadXml(reader, XmlReadMode.InferSchema);
            }
            #endregion
            
            #region Caixa
            xnList = xml.SelectNodes(NODE_CAIXA);
            foreach (XmlNode xn in xnList) {
                byte[] xmlBuffer = ASCIIEncoding.ASCII.GetBytes(xn.OuterXml);
                MemoryStream reader = new MemoryStream(xmlBuffer);
                //
                this.dataSet.ReadXml(reader, XmlReadMode.InferSchema);
            }
            #endregion

            #region Cotas
            xnList = xml.SelectNodes(NODE_COTAS);
            foreach (XmlNode xn in xnList) {
                byte[] xmlBuffer = ASCIIEncoding.ASCII.GetBytes(xn.OuterXml);
                MemoryStream reader = new MemoryStream(xmlBuffer);
                //
                this.dataSet.ReadXml(reader, XmlReadMode.InferSchema);
            }
            #endregion
            
            #region Cotas Despesas
            xnList = xml.SelectNodes(NODE_DESPESAS);
            foreach (XmlNode xn in xnList) {
                byte[] xmlBuffer = ASCIIEncoding.ASCII.GetBytes(xn.OuterXml);
                MemoryStream reader = new MemoryStream(xmlBuffer);
                //
                this.dataSet.ReadXml(reader, XmlReadMode.InferSchema);
            }
            #endregion

            #region Outras Despesas
            xnList = xml.SelectNodes(NODE_OUTRAS_DESPESAS);
            foreach (XmlNode xn in xnList) {
                byte[] xmlBuffer = ASCIIEncoding.ASCII.GetBytes(xn.OuterXml);
                MemoryStream reader = new MemoryStream(xmlBuffer);
                //
                this.dataSet.ReadXml(reader, XmlReadMode.InferSchema);
            }
            #endregion

            #region Provisao
            xnList = xml.SelectNodes(NODE_PROVISAO);
            foreach (XmlNode xn in xnList) {
                byte[] xmlBuffer = ASCIIEncoding.ASCII.GetBytes(xn.OuterXml);
                MemoryStream reader = new MemoryStream(xmlBuffer);
                //
                this.dataSet.ReadXml(reader, XmlReadMode.InferSchema);
            }
            #endregion

            #region TermoRV
            xnList = xml.SelectNodes(NODE_TERMO_RV);
            foreach (XmlNode xn in xnList) {
                byte[] xmlBuffer = ASCIIEncoding.ASCII.GetBytes(xn.OuterXml);
                MemoryStream reader = new MemoryStream(xmlBuffer);
                //
                this.dataSet.ReadXml(reader, XmlReadMode.InferSchema);
            }
            #endregion
        }

        /// <summary>
        /// Realiza um De-Para das Seções DataTables para as Entidades EntitySpaces
        /// </summary>
        /// <param name="nomeArquivoCompleto"></param>
        /// <param name="posicaoBolsaCollection"></param>
        /// <param name="posicaoEmprestimoBolsaCollection"></param>
        /// <param name="posicaoBMFCollection"></param>
        /// <param name="posicaoTermoBolsaCollection"></param>
        /// <param name="posicaoFundoCollection"></param>
        /// <param name="posicaoBolsaOpcoesCollection">PosicaoBolsa que guarda as opções</param>
        /// <param name="saldoCaixa"></param>
        /// <exception cref="">throws Exception se não encontrar Pessoa com o CPF</exception> 
        public void MontaEntidadesComparacao(StreamReader sr, out PosicaoBolsaCollection posicaoBolsaCollection,
                                             out PosicaoEmprestimoBolsaCollection posicaoEmprestimoBolsaCollection,
                                             out PosicaoBMFCollection posicaoBMFCollection,
                                             out PosicaoTermoBolsaCollection posicaoTermoBolsaCollection,
                                             out PosicaoFundoCollection posicaoFundoCollection,
                                             out PosicaoBolsaCollection posicaoBolsaOpcoesCollection,
                                             out SaldoCaixa saldoCaixa) {

            posicaoBolsaCollection = new PosicaoBolsaCollection();
            posicaoEmprestimoBolsaCollection = new PosicaoEmprestimoBolsaCollection();
            posicaoBMFCollection = new PosicaoBMFCollection();
            posicaoTermoBolsaCollection = new PosicaoTermoBolsaCollection();
            posicaoFundoCollection = new PosicaoFundoCollection();
            posicaoBolsaOpcoesCollection = new PosicaoBolsaCollection();
            saldoCaixa = new SaldoCaixa();
                                               
            XmlAnbid1 x = new XmlAnbid1();
            x.LerArquivoXML(sr);
            //
            int? idClienteCarteira = null;
            
            DataTable header = x.Secao.GetHeader;
            //
            DataTable acoes = x.Secao.GetAcoes;
            DataTable opcoesAcoes = x.Secao.GetOpcoesAcoes;
            DataTable futuros = x.Secao.GetFuturos;
            DataTable termos = x.Secao.GetTermos;
            DataTable cotas = x.Secao.GetCotas;
            DataTable caixa = x.Secao.GetCaixa;
            //

            if (header.Rows[0]["cnpjcpf"] != null) {
                string cnpjcpf = ((string)header.Rows[0]["cnpjcpf"]).Trim();

                PessoaCollection p = new PessoaCollection();
                p.Query.Select(p.Query.IdPessoa).Where(p.Query.Cpfcnpj == cnpjcpf).OrderBy(p.Query.IdPessoa.Ascending);
                
                if (p.Query.Load()) {
                    idClienteCarteira = p[0].IdPessoa.Value;
                }
                else {
                    throw new Exception("CPF/CNPJ não cadastrado: " + cnpjcpf);
                }

                // String Decimal aparecem com formato de ponto no arquivo XML
                NumberFormatInfo provider = new NumberFormatInfo();
                provider.NumberDecimalSeparator = ".";

                #region Monta PosicaoBolsa p/Acoes e PosicaoEmprestimoBolsa
                foreach (DataRow row in acoes.Rows) {

                    string classeOperacao = ((string)row["classeoperacao"]).Trim();

                    switch (classeOperacao) {
                        case "C": 
                        case "V":
                            #region Carrega em PosicaoBolsa
                            PosicaoBolsa posicao = posicaoBolsaCollection.AddNew();
                            posicao.IdCliente = idClienteCarteira;
                            posicao.IdAgente = this.RetornaIdAgenteMercadoDefault(); // idAgente Default
                            posicao.CdAtivoBolsa = ((string)row["codativo"]).Trim();
                            posicao.QuantidadeBloqueada = Convert.ToDecimal(row["qtgarantia"], provider);
                            //
                            posicao.ValorMercado = classeOperacao == "V"
                                                 ? -1 * (Convert.ToDecimal(row["valorfindisp"], provider))
                                                 : Convert.ToDecimal(row["valorfindisp"], provider);
                            //                                                                 
                            posicao.PUMercado = Convert.ToDecimal(row["puposicao"], provider);
                            //
                            posicao.Quantidade = classeOperacao == "V"
                                                 ? -1 * (Convert.ToDecimal(row["qtdisponivel"], provider) + Convert.ToDecimal(row["qtgarantia"], provider))
                                                 : Convert.ToDecimal(row["qtdisponivel"], provider) + Convert.ToDecimal(row["qtgarantia"], provider);
                            #endregion
                            break;
                        case "D":
                        case "T":
                            int? idAgentePosEmprestimoBolsa = null;
                            #region Carrega em PosicaoEmprestimoBolsa

                            /* Pega a Corretora por CNPJ  */
                            string cnpjinter = ((string)row["cnpjinter"]).Trim();
                            //
                            Int64 cnjpZerado = Convert.ToInt64(cnpjinter);

                            if (cnjpZerado == 0) {
                                //Busca o IdAgenteMercado default
                                idAgentePosEmprestimoBolsa = this.RetornaIdAgenteMercadoDefault();
                            }
                            else {
                                // Procura no AgenteMercado por CNPJ
                                AgenteMercadoCollection a = new AgenteMercadoCollection();
                                a.Query.Select(a.Query.IdAgente).Where(a.Query.Cnpj == cnpjinter).OrderBy(a.Query.IdAgente.Ascending);
                                //
                                idAgentePosEmprestimoBolsa = a.Query.Load() ? a[0].IdAgente.Value : this.RetornaIdAgenteMercadoDefault();
                            }

                            PosicaoEmprestimoBolsa posicaoEmprestimoBolsa = posicaoEmprestimoBolsaCollection.AddNew();
                            posicaoEmprestimoBolsa.IdCliente = idClienteCarteira;
                            posicaoEmprestimoBolsa.IdAgente = idAgentePosEmprestimoBolsa;
                            posicaoEmprestimoBolsa.CdAtivoBolsa = ((string)row["codativo"]).Trim();
                            posicaoEmprestimoBolsa.Quantidade = Convert.ToDecimal(row["qtdisponivel"], provider) + Convert.ToDecimal(row["qtgarantia"], provider);
                            posicaoEmprestimoBolsa.ValorMercado = Convert.ToDecimal(row["valorfindisp"], provider) + Convert.ToDecimal(row["valorfinemgar"], provider);
                            posicaoEmprestimoBolsa.PUMercado = Convert.ToDecimal(row["puposicao"], provider);
                            
                            //
                            // Armazena D ou T

                            int tipo = classeOperacao == "D" ? (int)PontaEmprestimoBolsa.Doador : (int)PontaEmprestimoBolsa.Tomador;
                            posicaoEmprestimoBolsa.TipoEmprestimo = (byte)tipo;
                            //
                            if (!String.IsNullOrEmpty(Convert.ToString(row["dtvencalug"]))) {
                                posicaoEmprestimoBolsa.DataVencimento = this.GetDate(Convert.ToString(row["dtvencalug"]));
                            }

                            posicaoEmprestimoBolsa.TaxaOperacao = Convert.ToDecimal(row["txalug"], provider);                            
                            #endregion
                            break;
                    }
                }                
                #endregion

                #region Monta PosicaoBolsa p/ Opcoes
                foreach (DataRow row in opcoesAcoes.Rows) {

                    string classeOperacao = ((string)row["classeoperacao"]).Trim();

                    #region Carrega em PosicaoBolsaOpcoes
                    PosicaoBolsa posicao = posicaoBolsaOpcoesCollection.AddNew();
                    posicao.IdCliente = idClienteCarteira;
                    posicao.IdAgente = this.RetornaIdAgenteMercadoDefault(); // idAgente Default
                    posicao.CdAtivoBolsa = ((string)row["codativo"]).Trim();
                    //
                    posicao.ValorMercado = classeOperacao == "V"
                                         ? -1 * (Convert.ToDecimal(row["valorfinanceiro"], provider))
                                         : Convert.ToDecimal(row["valorfinanceiro"], provider);
                    //                                                                 
                    posicao.PUMercado = Convert.ToDecimal(row["puposicao"], provider);
                    //
                    posicao.Quantidade = classeOperacao == "V"
                                         ? -1 * (Convert.ToDecimal(row["qtdisponivel"], provider) )
                                         : Convert.ToDecimal(row["qtdisponivel"], provider);
                    #endregion
                }
                #endregion

                #region Monta PosicaoBMF
                int? idAgentePosicaoBMF = null;
                foreach (DataRow row in futuros.Rows) {

                    /* Pega a Corretora por CNPJ  */
                    string cnpjCorretora = ((string)row["cnpjcorretora"]).Trim();
                    //
                    Int64 cnjpZerado = Convert.ToInt64(cnpjCorretora);

                    if (cnjpZerado == 0) {
                        //Busca o IdAgenteMercado default
                        idAgentePosicaoBMF = this.RetornaIdAgenteMercadoDefault();
                    }
                    else {
                        // Procura no AgenteMercado por CNPJ
                        AgenteMercadoCollection a = new AgenteMercadoCollection();
                        a.Query.Select(a.Query.IdAgente).Where(a.Query.Cnpj == cnpjCorretora).OrderBy(a.Query.IdAgente.Ascending);
                        //
                        idAgentePosicaoBMF = a.Query.Load() ? a[0].IdAgente.Value : this.RetornaIdAgenteMercadoDefault();
                    }

                    string classeOperacao = ((string)row["classeoperacao"]).Trim();
                    //        
                    PosicaoBMF posicao = posicaoBMFCollection.AddNew();
                    posicao.IdCliente = idClienteCarteira;
                    posicao.IdAgente = idAgentePosicaoBMF;
                    posicao.CdAtivoBMF = ((string)row["ativo"]).Trim();
                    posicao.Serie = ((string)row["serie"]).Trim();

                    decimal quantidade = Convert.ToDecimal(row["quantidade"], provider);
                    posicao.Quantidade = classeOperacao == "V"
                                         ? -1 * Convert.ToInt32( Math.Truncate(quantidade) )
                                         : Convert.ToInt32( Math.Truncate(quantidade) );
                    //
                    posicao.ValorMercado = classeOperacao == "V"
                                         ? -1 * (Convert.ToDecimal(row["vltotalpos"], provider))
                                         : Convert.ToDecimal(row["vltotalpos"], provider);
                    //

                    posicao.DataVencimento = this.GetDate(Convert.ToString(row["dtvencimento"]));
                }
                #endregion

                #region Monta PosicaoFundo
                int? idCarteiraFundo = null;
                //PosicaoFundoCollection posicaoFundoAuxCollection = new PosicaoFundoCollection();
                foreach (DataRow row in cotas.Rows) {
                    
                    // Pega IdCarteira por CNPJ
                    string cnpjFundo = ((string)row["cnpjfundo"]).Trim();
                    //
                    PessoaCollection pessoa = new PessoaCollection();
                    pessoa.Query.Select(pessoa.Query.IdPessoa).Where(pessoa.Query.Cpfcnpj == cnpjFundo).OrderBy(pessoa.Query.IdPessoa.Ascending);
                    if (pessoa.Query.Load()) {
                        idCarteiraFundo = pessoa[0].IdPessoa.Value;
                    }
                    else { // Se nao Achou o IdCarteira - Desconsidera o Registro
                        continue;
                    }
                    //                    
                    //PosicaoFundo posicao = posicaoFundoAuxCollection.AddNew();
                    PosicaoFundo posicao = posicaoFundoCollection.AddNew();
                    posicao.IdCliente = idClienteCarteira;
                    posicao.IdCarteira = idCarteiraFundo;
                    //
                    posicao.Quantidade = Convert.ToDecimal(row["qtdisponivel"], provider);
                    posicao.CotaDia = Convert.ToDecimal(row["puposicao"], provider);
                    posicao.ValorBruto = posicao.Quantidade * posicao.CotaDia; 
                }
                //// posicaoFundoAuxCollection tem as Posicao Fundo Separado por idCarteira
                //// Unifica Total por IdCliente
                //decimal valorBrutoTotal = 0;
                //for (int i = 0; i < posicaoFundoAuxCollection.Count; i++) {
                //      // Soma o Valor Bruto
                //    valorBrutoTotal += posicaoFundoAuxCollection[i].ValorBruto.Value; 
                //}
                //// Adiciona na Collection de PosicaoFundo
                //PosicaoFundo posicaoAux = posicaoFundoCollection.AddNew();
                //posicaoAux.IdCliente = idClienteCarteira;
                //posicaoAux.ValorBruto = valorBrutoTotal;
                ////
                #endregion

                #region Monta SaldoCaixa
                decimal sumTotalCaixa = 0.0M;
                foreach (DataRow row in caixa.Rows) {                  
                    sumTotalCaixa += Convert.ToDecimal(row["saldo"], provider);
                }

                saldoCaixa.IdCliente = idClienteCarteira;
                saldoCaixa.SaldoAbertura = sumTotalCaixa;
                saldoCaixa.SaldoFechamento = sumTotalCaixa;
                saldoCaixa.Data = this.GetDate(Convert.ToString(header.Rows[0]["dtposicao"]).Trim());
                //                 
                #endregion

                #region Monta PosicaoTermoBolsa
                foreach (DataRow row in termos.Rows) {
                    string classeOperacao = ((string)row["classeoperacao"]).Trim();
                    //
                    PosicaoTermoBolsa posicao = posicaoTermoBolsaCollection.AddNew();
                    posicao.IdCliente = idClienteCarteira;
                    //
                    posicao.IdAgente = this.RetornaIdAgenteMercadoDefault(); // idAgente Default
                    //
                    DateTime dataVencimento = this.GetDate(Convert.ToString(row["dtvencimento"]));
                    //                    
                    posicao.CdAtivoBolsa = ((string)row["ativo"]).Trim().ToUpper() + dataVencimento.ToString("ddMMyyyy");
                    //
                    posicao.DataOperacao = this.GetDate(Convert.ToString(row["dtoperacao"]));
                    posicao.DataVencimento = dataVencimento;
                    posicao.PUMercado = Convert.ToDecimal(row["puposicao"], provider);
                    //                    
                    posicao.Quantidade = classeOperacao == "V"
                                        ? -1 * (Convert.ToDecimal(row["quantidade"], provider))
                                        : Convert.ToDecimal(row["quantidade"], provider);
                    //                    
                    posicao.ValorMercado = classeOperacao == "V"
                                            ? -1 * (Convert.ToDecimal(row["valorfinanceiro"], provider))
                                            : Convert.ToDecimal(row["valorfinanceiro"], provider);
                    //
                    posicao.PUTermo = Convert.ToDecimal(row["puvenc"], provider);
                }
                #endregion
            }
        }
        
        /// <summary>
        /// 
        /// </summary>
        /// <param name="nomeArquivoXmlCompleto">Arquivo XML Anbid</param>
        /// <returns>Codigo da Carteira/Cliente presente na sessão Header do arquivo Xml Anbid</returns>
        /// <exception cref="">
        /// throws Exception Se Arquivo não existir,
        /// Se arquivo XML estiver incorreto - não existir o node /arquivoposicao_4_01/carteira/header/cnpjcpf
        /// Se o arquivo nao for um arquivo XML
        /// Se não existir uma pessoa com o CPF/CNPJ presente no arquivo xml
        /// </exception>
        public int GetIdCarteiraXmlAnbid(StreamReader sr) {

            XmlDocument xml = new XmlDocument();
            xml.LoadXml(sr.ReadToEnd());

            // Volta o Ponteiro para o Começo 
            sr.BaseStream.Seek(0, SeekOrigin.Begin);

            XmlNode xnList;
            try {
                xnList = xml.SelectSingleNode("/arquivoposicao_4_01/carteira/header/cnpjcpf");
            }
            catch (XPathException e) {
                throw new Exception("Node /arquivoposicao_4_01/carteira/header/cnpjcpf não existe");
            }
                        
            string cnpjcpf = xnList.InnerText;

            PessoaCollection p = new PessoaCollection();
            p.Query.Select(p.Query.IdPessoa).Where(p.Query.Cpfcnpj == cnpjcpf).OrderBy(p.Query.IdPessoa.Ascending);
            if (p.Query.Load()) {
                return p[0].IdPessoa.Value;
            }
            else {
                throw new Exception("CPF/CNPJ não cadastrado");
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="nomeArquivoXmlCompleto">Arquivo XML Anbid</param>
        /// <returns>Data Posição presente na sessão Header do arquivo Xml Anbid</returns>
        /// <exception cref="">
        /// throws Exception Se Arquivo não existir,
        /// Se arquivo XML estiver incorreto - não existir o node /arquivoposicao_4_01/carteira/header/dtposicao
        /// Se o arquivo nao for um arquivo XML
        /// </exception>
        public DateTime GetDataPosicaoXmlAnbid(StreamReader sr) {

            XmlDocument xml = new XmlDocument();
            xml.LoadXml(sr.ReadToEnd());
            //

            // Volta o Ponteiro para o Começo 
            sr.BaseStream.Seek(0, SeekOrigin.Begin);

            XmlNode xnList;
            try {
                xnList = xml.SelectSingleNode("/arquivoposicao_4_01/carteira/header/dtposicao");
            }
            catch (XPathException e) {
                throw new Exception("Node /arquivoposicao_4_01/carteira/header/dtposicao não existe");
            }

            string dataPosicao = xnList.InnerText;
            return this.GetDate(dataPosicao);            
        }

        /// <summary>
        /// Retorna o IdAgente Mercado Default de Acordo com o Parametro de Configuração presente no banco
        /// </summary>
        /// <returns></returns>
        private int RetornaIdAgenteMercadoDefault() {
            int codigoBovespaDefault = Convert.ToInt32(ParametrosConfiguracaoSistema.Bolsa.CodigoAgenteDefault);
            AgenteMercado agenteMercado = new AgenteMercado();
            //
            return agenteMercado.BuscaIdAgenteMercadoBovespa(codigoBovespaDefault);
        }

        /// <summary>
        /// Transforma uma string no formato Ano-Mes-Dia para um Datetime
        /// </summary>
        /// <param name="data">string no formato Ano-Mes-Dia</param>
        /// <returns></returns>
        /// <exception cref="">throws Exception se data nao tiver tamanho 8</exception>
        private DateTime GetDate(string data) {
            if (data.Length != 8) {
                throw new Exception("Formato de Data do arquivo XML inválido");
            }
            
            return new DateTime( Convert.ToInt32(data.Substring(0,4)), 
                                 Convert.ToInt32(data.Substring(4,2)), 
                                 Convert.ToInt32(data.Substring(6,2)) );
        }

        /// <summary>
        /// false se arquivo não possui extensão XML(.xml)
        /// true se arquivo possui extensão XML(.xml)
        /// </summary>
        /// <param name="arquivo">Nome do arquivo</param>
        /// <returns></returns>
        [Obsolete("Desnecessario")]
        public bool isExtensaoXml(string arquivo) {
            string extensao = new FileInfo(arquivo).Extension;
            if (extensao != ".xml") {
                return false;
            }
            return true;
        }
    }
}