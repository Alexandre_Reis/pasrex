﻿using System;
using System.Text;
using System.Collections.Generic;
using System.IO;

using EntitySpaces.Core;
using EntitySpaces.Interfaces;
using Financial.Bolsa;
using Financial.Common;
using Financial.Common.Exceptions;
using Financial.Investidor;
using Financial.Interfaces.Import.Bolsa;

namespace Financial.Batimentos {
        
    /// <summary>
    /// Classe Interna para Batimento do Front-Office
    /// </summary>
    public class BatimentoFrontOffice {

        /// <summary>
        /// Carrega As Divergências entre OrdemBolsa e o Arquivo Negs 
        /// </summary>
        /// <param name="idClienteArquivo">IdCliente Contido no Nome Do Arquivo Negs</param>
        /// <param name="sr">StreamReader com o Conteudo do Arquivo Negs</param>
        /// <param name="dataReferencia"></param>
        /// <returns>Lista com os Elementos para Bind do Grid</returns>
        public List<OrdemBolsa.BatimentoOrdemBolsa> CarregaGridFrontOffice(int idClienteArquivo, StreamReader sr, DateTime dataReferencia) {
            // Objeto para o Bind
            List<OrdemBolsa.BatimentoOrdemBolsa> listaBatimentoFrontOffice = new List<OrdemBolsa.BatimentoOrdemBolsa>();

            string linha = sr.ReadLine().Trim();

            // Volta o Ponteiro para o Começo 
            sr.DiscardBufferedData();
            sr.BaseStream.Seek(0, SeekOrigin.Begin);

            int codigoBovespaAgente = Convert.ToInt32(linha.Substring(6, 4));
            AgenteMercado agenteMercado = new AgenteMercado();
            // Da exceção se Agente Mercado não existir

            try {
                int idAgenteCorretora = agenteMercado.BuscaIdAgenteMercadoBovespa(codigoBovespaAgente);
            }
            catch (IdAgenteNaoCadastradoException e) {
                throw new Exception(e.Message);
            }

            #region Confere o IdCliente Interno
            int codigoClienteBovespa = idClienteArquivo;
            CodigoClienteAgente codigoClienteAgente = new CodigoClienteAgente();
            bool idClienteExiste = codigoClienteAgente.BuscaIdClienteInternoBovespa(codigoBovespaAgente, codigoClienteBovespa);
            if (!idClienteExiste) {
                throw new Exception("Processando Arquivo Negs: Não Existe Cliente com o Código Cliente Bovespa Informado: " + codigoClienteBovespa);
            }
            #endregion

            #region Monta Collection de Negs com os Valores das Ordens do Arquivo Negs
            Negs negs = new Negs();
            NegsCollection negsCollection = new NegsCollection();
            //
            try {
                negsCollection = negs.ProcessaNegs(sr, dataReferencia);
                // Remove a Linha de Header do Negs
                negsCollection.CollectionNegs.RemoveAt(0);
            }
            catch (Exception e) {
                throw new Exception(e.Message);
            }

            // Seta o IdCliente em Todos os Itens da Collection
            negsCollection.SetaIdCliente(codigoClienteAgente.IdCliente.Value);
            //

            // Remove CdAtivo Fracionário se Existir de Todos os itens da Collection de Negs
            negsCollection.CorrigeAtivosFracionarios();
            //
            NegsCollection negsCollectionAgrupada = negs.GroupBy(negsCollection);
            //
            #endregion

            #region Monta Collection de OrdemBolsa
            OrdemBolsaCollection ordemBolsaCollection = new OrdemBolsaCollection();
            //
            ordemBolsaCollection.Query
                                .Select(ordemBolsaCollection.Query.IdCliente,
                                        ordemBolsaCollection.Query.CdAtivoBolsa,
                                        ordemBolsaCollection.Query.TipoOrdem,
                                        ordemBolsaCollection.Query.Quantidade.Sum().As("Quantidade"),
                                        ordemBolsaCollection.Query.Valor.Sum().As("Valor"))
                                .Where(ordemBolsaCollection.Query.IdCliente == codigoClienteAgente.IdCliente.Value,
                                       ordemBolsaCollection.Query.Data.Equal(dataReferencia))
                                .GroupBy(ordemBolsaCollection.Query.IdCliente,
                                         ordemBolsaCollection.Query.CdAtivoBolsa,
                                         ordemBolsaCollection.Query.TipoOrdem)
                                .OrderBy(ordemBolsaCollection.Query.CdAtivoBolsa.Ascending,
                                         ordemBolsaCollection.Query.TipoOrdem.Ascending);
            //
            ordemBolsaCollection.Query.Load();
            //
            #endregion

            OrdemBolsa ordemBolsa = new OrdemBolsa();

            // Carrega Lista de Divergências
            List<OrdemBolsa.BatimentoOrdemBolsa> listaBatimento = ordemBolsa.ProcessaBatimentoNegs(ordemBolsaCollection, negsCollectionAgrupada);
            //

            // Ordena por CdAtivo
            OrdemBolsa.BatimentoOrdemBolsa b = new OrdemBolsa.BatimentoOrdemBolsa();
            listaBatimento.Sort(b.OrderBatimentoOrdemBolsaByCdAtivoBolsa);

            // Seta o Apelido Do Cliente em todos os Itens do DataSource
            for (int i = 0; i < listaBatimento.Count; i++) {
                Cliente cliente = new Cliente();
                List<esQueryItem> campos = new List<esQueryItem>();
                campos.Add(cliente.Query.Apelido);
                //
                cliente.LoadByPrimaryKey(campos, codigoClienteAgente.IdCliente.Value);
                //
                listaBatimento[i].Apelido = cliente.str.Apelido.Trim();
            }

            return listaBatimento;
        }

        /// <summary>
        /// Copia uma Stream para Outra
        /// </summary>
        /// <param name="input"></param>
        /// <param name="output"></param>
        [Obsolete("Não Precisou - não testado")]
        public void CopyStream(Stream input, Stream output) {
            using (StreamReader reader = new StreamReader(input)) {
                using (StreamWriter writer = new StreamWriter(output)) {
                    writer.Write(reader.ReadToEnd());
                }
            }
        }
    }
}