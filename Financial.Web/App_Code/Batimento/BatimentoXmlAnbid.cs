﻿using System;
using System.Text;
using System.Collections.Generic;

using EntitySpaces.Core;
using EntitySpaces.Interfaces;

using Financial.CRM;
using Financial.Bolsa;
using Financial.BMF;
using Financial.Fundo;
using Financial.ContaCorrente;
using Financial.Investidor;

using Financial.Bolsa.Enums;
using System.IO;

namespace Financial.Batimentos {
        
        /// <summary>
        /// Classe Interna para Batimento do Arquivo Xml Anbid
        /// </summary>
        public class BatimentoXmlAnbid {

            public BatimentoXmlAnbid() { }

            public BatimentoXmlAnbid(int idCliente, string apelido, string cdAtivo,
                           decimal valorFinancial, decimal valorXmlAnbid, decimal diferenca) {

                this.idCliente = idCliente;
                this.apelido = apelido;
                this.cdAtivo = cdAtivo;
                this.valorFinancial = valorFinancial;
                this.valorXmlAnbid = valorXmlAnbid;
                this.diferenca = diferenca;
            }

            // Carrega Lista de Divergências do XMLAnbid
            private List<BatimentoXmlAnbid> listaBatimentoXML = new List<BatimentoXmlAnbid>();

            private int idCliente;
            private string apelido;
            private string cdAtivo;
            private decimal valorFinancial;
            private decimal valorXmlAnbid;
            private decimal diferenca;

            public int IdCliente {
                get { return idCliente; }
                set { idCliente = value; }
            }

            public string Apelido {
                get { return apelido; }
                set { apelido = value; }
            }

            public string CdAtivo {
                get { return cdAtivo; }
                set { cdAtivo = value; }
            }

            public decimal ValorFinancial {
                get { return valorFinancial; }
                set { valorFinancial = value; }
            }

            public decimal ValorXmlAnbid {
                get { return valorXmlAnbid; }
                set { valorXmlAnbid = value; }
            }

            public decimal Diferenca {
                get { return diferenca; }
                set { diferenca = value; }
            }

            /// <summary>
            ///  Função Delegate que Indica se um BatimentoXmlAnbid Existe
            /// </summary>
            /// <param name="d"></param>
            /// <returns>  </returns>
            public bool Exists(BatimentoXmlAnbid b) {
                return this.idCliente == b.idCliente &&
                       this.cdAtivo.Trim() == b.cdAtivo.Trim() &&
                       this.valorFinancial == b.valorFinancial &&
                       this.valorXmlAnbid == b.ValorXmlAnbid;
            }

            /// <summary>
            ///  Função Delegate que Serve para Ordenar a Collection de BatimentoXmlAnbid pelo CdAtivoBolsa
            /// </summary>
            /// <param name="b1"></param>
            /// <param name="b2"></param>
            /// <returns> 0 se CdAtivos são iguais
            ///           1 se b1 maior b2  
            ///          -1 se b1 menor b2
            /// </returns>
            public int OrderBatimentoXmlAnbidByCdAtivoBolsa(BatimentoXmlAnbid b1, BatimentoXmlAnbid b2) {
                return b1.CdAtivo.Trim().CompareTo(b2.CdAtivo.Trim());
            }
        
            /// <summary>
            /// Carrega As Divergências entre XML Anbid e Financial
            /// </summary>
            /// <param name="path"></param>
            public List<BatimentoXmlAnbid> CarregaGridXmlAnbid(StreamReader sr) {
                // Limpa o DataSource
                this.listaBatimentoXML = new List<BatimentoXmlAnbid>();
                //
                PosicaoBolsaCollection posicaoBolsaCollection = new PosicaoBolsaCollection();
                PosicaoEmprestimoBolsaCollection posicaoEmprestimoBolsaCollection = new PosicaoEmprestimoBolsaCollection();
                PosicaoBMFCollection posicaoBMFCollection = new PosicaoBMFCollection();
                PosicaoTermoBolsaCollection posicaoTermoBolsaCollection = new PosicaoTermoBolsaCollection();
                PosicaoFundoCollection posicaoFundoCollection = new PosicaoFundoCollection();
                PosicaoBolsaCollection posicaoBolsaOpcoesCollection = new PosicaoBolsaCollection();
                SaldoCaixa saldoCaixa = new SaldoCaixa();

                XmlAnbid1 xmlAnbid = new XmlAnbid1();
                try {
                    xmlAnbid.MontaEntidadesComparacao(sr, out posicaoBolsaCollection,
                                                  out posicaoEmprestimoBolsaCollection,
                                                  out posicaoBMFCollection,
                                                  out posicaoTermoBolsaCollection,
                                                  out posicaoFundoCollection,
                                                  out posicaoBolsaOpcoesCollection,
                                                  out saldoCaixa);
                }
                catch (Exception e) {
                    throw new Exception(e.Message);
                }

                int? idCarteira = null;
                try {
                    idCarteira = xmlAnbid.GetIdCarteiraXmlAnbid(sr);
                }
                catch (Exception e1) {
                    throw new Exception(e1.Message);
                }

                #region Monta Collections Financial
                #region Monta PosicaoBolsa
                PosicaoBolsaCollection posBolsaFinancialCollection = new PosicaoBolsaCollection();
                //
                posBolsaFinancialCollection.Query
                                    .Select(posBolsaFinancialCollection.Query.IdCliente,
                                            posBolsaFinancialCollection.Query.CdAtivoBolsa,
                                            posBolsaFinancialCollection.Query.ValorMercado.Sum())
                                    .Where(posBolsaFinancialCollection.Query.IdCliente == idCarteira.Value &&
                                           posBolsaFinancialCollection.Query.TipoMercado == TipoMercadoBolsa.MercadoVista)
                                    .GroupBy(posBolsaFinancialCollection.Query.IdCliente,
                                             posBolsaFinancialCollection.Query.CdAtivoBolsa)
                                    .OrderBy(posBolsaFinancialCollection.Query.CdAtivoBolsa.Ascending);
                //
                posBolsaFinancialCollection.Query.Load();
                #endregion

                #region Monta PosicaoBolsa p/ Opções
                PosicaoBolsaCollection posBolsaOpcoesFinancialCollection = new PosicaoBolsaCollection();
                //
                posBolsaOpcoesFinancialCollection.Query
                                    .Select(posBolsaOpcoesFinancialCollection.Query.IdCliente,
                                            posBolsaOpcoesFinancialCollection.Query.CdAtivoBolsa,
                                            posBolsaOpcoesFinancialCollection.Query.ValorMercado.Sum())
                                    .Where(posBolsaOpcoesFinancialCollection.Query.IdCliente == idCarteira.Value &&
                                           posBolsaOpcoesFinancialCollection.Query.TipoMercado.In(TipoMercadoBolsa.OpcaoCompra, TipoMercadoBolsa.OpcaoVenda))
                                    .GroupBy(posBolsaOpcoesFinancialCollection.Query.IdCliente,
                                             posBolsaOpcoesFinancialCollection.Query.CdAtivoBolsa,
                                             posBolsaOpcoesFinancialCollection.Query.TipoMercado)
                                    .OrderBy(posBolsaOpcoesFinancialCollection.Query.CdAtivoBolsa.Ascending);
                //
                posBolsaOpcoesFinancialCollection.Query.Load();
                #endregion

                #region Monta PosicaoEmprestimoBolsa

                PosicaoEmprestimoBolsaCollection posEmprestimoBolsaFinancialCollection = new PosicaoEmprestimoBolsaCollection();
                //
                posEmprestimoBolsaFinancialCollection.Query
                                    .Select(posEmprestimoBolsaFinancialCollection.Query.IdCliente,
                                            posEmprestimoBolsaFinancialCollection.Query.CdAtivoBolsa,
                                            posEmprestimoBolsaFinancialCollection.Query.PontaEmprestimo,
                                            posEmprestimoBolsaFinancialCollection.Query.ValorMercado.Sum())
                                    .Where(posEmprestimoBolsaFinancialCollection.Query.IdCliente == idCarteira.Value)
                                    .GroupBy(posEmprestimoBolsaFinancialCollection.Query.IdCliente,
                                             posEmprestimoBolsaFinancialCollection.Query.CdAtivoBolsa,
                                             posEmprestimoBolsaFinancialCollection.Query.PontaEmprestimo)
                                    .OrderBy(posEmprestimoBolsaFinancialCollection.Query.CdAtivoBolsa.Ascending);
                //
                posEmprestimoBolsaFinancialCollection.Query.Load();
                #endregion

                #region Monta PosicaoBMFCollection
                PosicaoBMFCollection posBMFFinancialCollection = new PosicaoBMFCollection();
                //
                posBMFFinancialCollection.Query
                                    .Select(posBMFFinancialCollection.Query.IdCliente,
                                            posBMFFinancialCollection.Query.CdAtivoBMF,
                                            posBMFFinancialCollection.Query.Serie,
                                            posBMFFinancialCollection.Query.ValorMercado.Sum())
                                    .Where(posBMFFinancialCollection.Query.IdCliente == idCarteira.Value)
                                    .GroupBy(posBMFFinancialCollection.Query.IdCliente,
                                             posBMFFinancialCollection.Query.CdAtivoBMF,
                                             posBMFFinancialCollection.Query.Serie)
                                    .OrderBy(posBMFFinancialCollection.Query.CdAtivoBMF.Ascending,
                                             posBMFFinancialCollection.Query.Serie.Ascending);
                //
                posBMFFinancialCollection.Query.Load();
                #endregion

                #region Monta PosicaoTermoBolsa
                PosicaoTermoBolsaCollection posTermoBolsaFinancialCollection = new PosicaoTermoBolsaCollection();
                //
                posTermoBolsaFinancialCollection.Query
                                    .Select(posTermoBolsaFinancialCollection.Query.IdCliente,
                                            posTermoBolsaFinancialCollection.Query.CdAtivoBolsa,
                                            posTermoBolsaFinancialCollection.Query.ValorMercado.Sum())
                                    .Where(posTermoBolsaFinancialCollection.Query.IdCliente == idCarteira.Value)
                                    .GroupBy(posTermoBolsaFinancialCollection.Query.IdCliente,
                                             posTermoBolsaFinancialCollection.Query.CdAtivoBolsa)
                                    .OrderBy(posTermoBolsaFinancialCollection.Query.CdAtivoBolsa.Ascending);
                //
                posTermoBolsaFinancialCollection.Query.Load();
                #endregion

                #region Monta PosicaoFundo

                //// Pega a Lista de Carteiras presente no arquivo XML
                //List<int> idCarteirasFundo = new List<int>(posicaoFundoCollection.Count);
                //for (int i = 0; i < posicaoFundoCollection.Count; i++) {
                //    idCarteirasFundo.Add(posicaoFundoCollection[i].IdCarteira.Value);
                //}
                //
                PosicaoFundoCollection posFundoFinancialCollection = new PosicaoFundoCollection();
                //
                posFundoFinancialCollection.Query
                                    .Select(posFundoFinancialCollection.Query.IdCliente,
                                            posFundoFinancialCollection.Query.IdCarteira,
                                           (posFundoFinancialCollection.Query.Quantidade * posFundoFinancialCollection.Query.CotaDia).As("ValorBruto").Sum())
                                    .Where(posFundoFinancialCollection.Query.IdCliente == idCarteira.Value)
                    //posFundoFinancialCollection.Query.IdCarteira.In(idCarteirasFundo))
                                    .GroupBy(posFundoFinancialCollection.Query.IdCliente, posFundoFinancialCollection.Query.IdCarteira);
                //.OrderBy(posFundoFinancialCollection.Query.IdCarteira.Ascending);
                //
                posFundoFinancialCollection.Query.Load();
                #endregion

                #region Monta SaldoCaixa
                DateTime? dataPosicao = null;
                try {
                    dataPosicao = xmlAnbid.GetDataPosicaoXmlAnbid(sr);
                }
                catch (Exception e1) {
                    throw new Exception(e1.Message);
                }

                SaldoCaixa s = new SaldoCaixa();
                //
                s.BuscaSaldoCaixa(idCarteira.Value, dataPosicao.Value);
                //                    
                //decimal saldoFechamento = s.SaldoFechamento.HasValue ? s.SaldoFechamento.Value : 0;
                #endregion
                #endregion

                /* Divergencias */
                this.MontaDivergenciasPosicaoBolsa(posicaoBolsaCollection, posBolsaFinancialCollection);
                this.MontaDivergenciasPosicaoBolsaOpcoes(posicaoBolsaOpcoesCollection, posBolsaOpcoesFinancialCollection);
                this.MontaDivergenciasPosicaoEmprestimoBolsa(posicaoEmprestimoBolsaCollection, posEmprestimoBolsaFinancialCollection);
                this.MontaDivergenciasPosicaoBMF(posicaoBMFCollection, posBMFFinancialCollection);
                this.MontaDivergenciasPosicaoTermoBolsa(posicaoTermoBolsaCollection, posTermoBolsaFinancialCollection);
                this.MontaDivergenciasPosicaoFundo(posicaoFundoCollection, posFundoFinancialCollection);
                this.MontaDivergenciasSaldoCaixa(saldoCaixa, s);

                if (this.listaBatimentoXML.Count == 0) {                    
                   throw new Exception("Não há Inconsistências");
                }
                else {
                    BatimentoXmlAnbid b = new BatimentoXmlAnbid();
                    // Ordena por CdAtivoBolsa
                    this.listaBatimentoXML.Sort(b.OrderBatimentoXmlAnbidByCdAtivoBolsa);
                    //
                    //gridBatimentoXmlAnbid.DataSource = this.listaBatimentoXML;
                }
                //------------------------------------------------------------------------------------------------------
                //--------------- Teste ------------------------------------------------------
                //listaBatimentoXML.Add(new BatimentoXmlAnbid(1, "apelido", "Petr4", 1, 1, 1));
                //listaBatimentoXML.Add(new BatimentoXmlAnbid(2, "apelido2", "Vale5", 2, 2, 2));
                // End Teste ------------------------------------------------------
               
                return this.listaBatimentoXML;
            }

            #region Trata Divergencias entre XML Anbid e Financial
            /// <summary>
            /// 
            /// </summary>
            /// <param name="posicaoBolsaXmlCollection"></param>
            /// <param name="posBolsaFinancialCollection"></param>
            private void MontaDivergenciasPosicaoBolsa(PosicaoBolsaCollection posicaoBolsaXmlCollection, PosicaoBolsaCollection posBolsaFinancialCollection) {

                foreach (PosicaoBolsa posicaoBolsa in posicaoBolsaXmlCollection) {
                    #region Determina se Collection Financial Está na Collection de XML

                    #region Find
                    esEntityCollectionView<PosicaoBolsa> view1 = new esEntityCollectionView<PosicaoBolsa>(posBolsaFinancialCollection);

                    // Procura por IdCliente, CdAtivo
                    StringBuilder filtro = new StringBuilder();
                    filtro = new StringBuilder();
                    filtro.Append("" + PosicaoBolsaMetadata.ColumnNames.IdCliente + " = '" + posicaoBolsa.IdCliente + "'");
                    filtro.Append(" AND " + PosicaoBolsaMetadata.ColumnNames.CdAtivoBolsa + " = '" + posicaoBolsa.CdAtivoBolsa + "'");
                    //                         
                    view1.Filter = filtro.ToString();

                    // Financial não está no XML
                    if (view1.Count == 0) {
                        #region Cria BatimentoXmlAnbid
                        BatimentoXmlAnbid batimentoXmlAnbid = new BatimentoXmlAnbid();
                        batimentoXmlAnbid.CdAtivo = posicaoBolsa.CdAtivoBolsa;
                        batimentoXmlAnbid.IdCliente = posicaoBolsa.IdCliente.Value;
                        batimentoXmlAnbid.Apelido = this.BuscaNomeCliente(posicaoBolsa.IdCliente.Value);
                        //
                        batimentoXmlAnbid.ValorXmlAnbid = posicaoBolsa.ValorMercado.Value;
                        batimentoXmlAnbid.ValorFinancial = 0;
                        //
                        batimentoXmlAnbid.Diferenca = batimentoXmlAnbid.ValorFinancial - batimentoXmlAnbid.ValorXmlAnbid;
                        //
                        // Adiciona na Lista de Inconsistência somente se não existir Registro Igual
                        if (this.listaBatimentoXML.FindAll(batimentoXmlAnbid.Exists).Count == 0) {
                            this.listaBatimentoXML.Add(batimentoXmlAnbid);
                        }
                        #endregion
                    }
                    // Financial Está no XML
                    else {
                        // Se existe Diferença
                        if (view1[0].ValorMercado.HasValue && view1[0].ValorMercado != posicaoBolsa.ValorMercado) {

                            #region Cria BatimentoXmlAnbid
                            BatimentoXmlAnbid batimentoXmlAnbid = new BatimentoXmlAnbid();
                            batimentoXmlAnbid.CdAtivo = posicaoBolsa.CdAtivoBolsa;
                            batimentoXmlAnbid.IdCliente = posicaoBolsa.IdCliente.Value;
                            batimentoXmlAnbid.Apelido = this.BuscaNomeCliente(posicaoBolsa.IdCliente.Value);
                            //
                            batimentoXmlAnbid.ValorXmlAnbid = posicaoBolsa.ValorMercado.Value;
                            batimentoXmlAnbid.ValorFinancial = view1[0].ValorMercado.Value;
                            //
                            batimentoXmlAnbid.Diferenca = batimentoXmlAnbid.ValorFinancial - batimentoXmlAnbid.ValorXmlAnbid;
                            //                            
                            // Adiciona na Lista de Inconsistência somente se não existir Registro Igual
                            if (this.listaBatimentoXML.FindAll(batimentoXmlAnbid.Exists).Count == 0) {
                                this.listaBatimentoXML.Add(batimentoXmlAnbid);
                            }
                            #endregion
                        }
                    }
                                      
                    // Zera o Filtro
                    view1.Filter = "";
                    #endregion
                    
                    #endregion
                }

                for (int i = 0; i < posBolsaFinancialCollection.Count; i++) {
                    #region Determina se XML Está na Collection do Financial

                    int idCliente = posBolsaFinancialCollection[i].IdCliente.Value;
                    string cdAtivoBolsa = posBolsaFinancialCollection[i].CdAtivoBolsa.Trim();
                    decimal valor = posBolsaFinancialCollection[i].ValorMercado.Value;
                    //                    
                    esEntityCollectionView<PosicaoBolsa> view1 = new esEntityCollectionView<PosicaoBolsa>(posicaoBolsaXmlCollection);

                    // Procura por IdCliente, CdAtivo e IdAgente
                    StringBuilder filtro = new StringBuilder();
                    filtro = new StringBuilder();
                    filtro.Append("" + PosicaoBolsaMetadata.ColumnNames.IdCliente + " = '" + idCliente + "'");
                    filtro.Append(" AND " + PosicaoBolsaMetadata.ColumnNames.CdAtivoBolsa + " = '" + cdAtivoBolsa + "'");
                    //                         
                    view1.Filter = filtro.ToString();

                    // XML não está no Financial
                    if (view1.Count == 0) {

                        #region Cria BatimentoXmlAnbid
                        // Adiciona Informações de Financial
                        BatimentoXmlAnbid batimentoXmlAnbid = new BatimentoXmlAnbid();
                        batimentoXmlAnbid.CdAtivo = cdAtivoBolsa;
                        batimentoXmlAnbid.IdCliente = idCliente;
                        batimentoXmlAnbid.Apelido = this.BuscaNomeCliente(idCliente);
                        //
                        batimentoXmlAnbid.ValorXmlAnbid = 0;
                        batimentoXmlAnbid.ValorFinancial = valor;
                        //
                        batimentoXmlAnbid.Diferenca = batimentoXmlAnbid.ValorFinancial - batimentoXmlAnbid.ValorXmlAnbid;
                        //
                        // Adiciona na Lista de Inconsistência somente se não existir Registro Igual
                        if (this.listaBatimentoXML.FindAll(batimentoXmlAnbid.Exists).Count == 0) {
                            this.listaBatimentoXML.Add(batimentoXmlAnbid);
                        }
                        #endregion                                                                                                                                                                                                                             
                    }
                    // XML Está na Collection do Financial
                    else  {
                        // Se existe Diferença
                        if (view1[0].ValorMercado.HasValue && view1[0].ValorMercado != valor) {

                            #region Cria BatimentoXmlAnbid
                            BatimentoXmlAnbid batimentoXmlAnbid = new BatimentoXmlAnbid();
                            batimentoXmlAnbid.CdAtivo = cdAtivoBolsa;
                            batimentoXmlAnbid.IdCliente = idCliente;
                            batimentoXmlAnbid.Apelido = this.BuscaNomeCliente(idCliente);
                            //                            
                            batimentoXmlAnbid.ValorXmlAnbid = view1[0].ValorMercado.Value;
                            batimentoXmlAnbid.ValorFinancial = valor;
                            //
                            batimentoXmlAnbid.Diferenca = batimentoXmlAnbid.ValorFinancial - batimentoXmlAnbid.ValorXmlAnbid;
                            //
                            // Adiciona na Lista de Inconsistência somente se não existir Registro Igual
                            if (this.listaBatimentoXML.FindAll(batimentoXmlAnbid.Exists).Count == 0) {
                                this.listaBatimentoXML.Add(batimentoXmlAnbid);
                            }
                            #endregion
                        }
                    }

                    // Zera o Filtro
                    view1.Filter = "";

                    #endregion
                }
            }

            /// <summary>
            /// 
            /// </summary>
            /// <param name="posicaoBolsaOpcoesXmlCollection"></param>
            /// <param name="posBolsaOpcoesFinancialCollection"></param>
            private void MontaDivergenciasPosicaoBolsaOpcoes(PosicaoBolsaCollection posicaoBolsaOpcoesXmlCollection, PosicaoBolsaCollection posBolsaOpcoesFinancialCollection) {

                foreach (PosicaoBolsa posicaoBolsa in posicaoBolsaOpcoesXmlCollection) {
                    #region Determina se Collection Financial Está na Collection de XML

                    #region Find
                    esEntityCollectionView<PosicaoBolsa> view1 = new esEntityCollectionView<PosicaoBolsa>(posBolsaOpcoesFinancialCollection);

                    // Procura por IdCliente, CdAtivo
                    StringBuilder filtro = new StringBuilder();
                    filtro = new StringBuilder();
                    filtro.Append("" + PosicaoBolsaMetadata.ColumnNames.IdCliente + " = '" + posicaoBolsa.IdCliente + "'");
                    filtro.Append(" AND " + PosicaoBolsaMetadata.ColumnNames.CdAtivoBolsa + " = '" + posicaoBolsa.CdAtivoBolsa + "'");
                    //                         
                    view1.Filter = filtro.ToString();

                    // Financial não está no XML
                    if (view1.Count == 0) {
                        #region Cria BatimentoXmlAnbid
                        BatimentoXmlAnbid batimentoXmlAnbid = new BatimentoXmlAnbid();
                        batimentoXmlAnbid.CdAtivo = posicaoBolsa.CdAtivoBolsa;
                        batimentoXmlAnbid.IdCliente = posicaoBolsa.IdCliente.Value;
                        batimentoXmlAnbid.Apelido = this.BuscaNomeCliente(posicaoBolsa.IdCliente.Value);
                        //
                        batimentoXmlAnbid.ValorXmlAnbid = posicaoBolsa.ValorMercado.Value;
                        batimentoXmlAnbid.ValorFinancial = 0;
                        //
                        batimentoXmlAnbid.Diferenca = batimentoXmlAnbid.ValorFinancial - batimentoXmlAnbid.ValorXmlAnbid;
                        //
                        // Adiciona na Lista de Inconsistência somente se não existir Registro Igual
                        if (this.listaBatimentoXML.FindAll(batimentoXmlAnbid.Exists).Count == 0) {
                            this.listaBatimentoXML.Add(batimentoXmlAnbid);
                        }
                        #endregion
                    }
                    // Financial Está no XML
                    else {
                        // Se existe Diferença
                        if (view1[0].ValorMercado.HasValue && view1[0].ValorMercado != posicaoBolsa.ValorMercado) {

                            #region Cria BatimentoXmlAnbid
                            BatimentoXmlAnbid batimentoXmlAnbid = new BatimentoXmlAnbid();
                            batimentoXmlAnbid.CdAtivo = posicaoBolsa.CdAtivoBolsa;
                            batimentoXmlAnbid.IdCliente = posicaoBolsa.IdCliente.Value;
                            batimentoXmlAnbid.Apelido = this.BuscaNomeCliente(posicaoBolsa.IdCliente.Value);
                            //
                            batimentoXmlAnbid.ValorXmlAnbid = posicaoBolsa.ValorMercado.Value;
                            batimentoXmlAnbid.ValorFinancial = view1[0].ValorMercado.Value;
                            //
                            batimentoXmlAnbid.Diferenca = batimentoXmlAnbid.ValorFinancial - batimentoXmlAnbid.ValorXmlAnbid;
                            //                            
                            // Adiciona na Lista de Inconsistência somente se não existir Registro Igual
                            if (this.listaBatimentoXML.FindAll(batimentoXmlAnbid.Exists).Count == 0) {
                                this.listaBatimentoXML.Add(batimentoXmlAnbid);
                            }
                            #endregion
                        }
                    }

                    // Zera o Filtro
                    view1.Filter = "";
                    #endregion

                    #endregion
                }

                for (int i = 0; i < posBolsaOpcoesFinancialCollection.Count; i++) {
                    #region Determina se XML Está na Collection do Financial

                    int idCliente = posBolsaOpcoesFinancialCollection[i].IdCliente.Value;
                    string cdAtivoBolsa = posBolsaOpcoesFinancialCollection[i].CdAtivoBolsa.Trim();
                    decimal valor = posBolsaOpcoesFinancialCollection[i].ValorMercado.Value;
                    //                    
                    esEntityCollectionView<PosicaoBolsa> view1 = new esEntityCollectionView<PosicaoBolsa>(posicaoBolsaOpcoesXmlCollection);

                    // Procura por IdCliente, CdAtivo e IdAgente
                    StringBuilder filtro = new StringBuilder();
                    filtro = new StringBuilder();
                    filtro.Append("" + PosicaoBolsaMetadata.ColumnNames.IdCliente + " = '" + idCliente + "'");
                    filtro.Append(" AND " + PosicaoBolsaMetadata.ColumnNames.CdAtivoBolsa + " = '" + cdAtivoBolsa + "'");
                    //                         
                    view1.Filter = filtro.ToString();

                    // XML não está no Financial
                    if (view1.Count == 0) {

                        #region Cria BatimentoXmlAnbid
                        // Adiciona Informações de Financial
                        BatimentoXmlAnbid batimentoXmlAnbid = new BatimentoXmlAnbid();
                        batimentoXmlAnbid.CdAtivo = cdAtivoBolsa;
                        batimentoXmlAnbid.IdCliente = idCliente;
                        batimentoXmlAnbid.Apelido = this.BuscaNomeCliente(idCliente);
                        //
                        batimentoXmlAnbid.ValorXmlAnbid = 0;
                        batimentoXmlAnbid.ValorFinancial = valor;
                        //
                        batimentoXmlAnbid.Diferenca = batimentoXmlAnbid.ValorFinancial - batimentoXmlAnbid.ValorXmlAnbid;
                        //
                        // Adiciona na Lista de Inconsistência somente se não existir Registro Igual
                        if (this.listaBatimentoXML.FindAll(batimentoXmlAnbid.Exists).Count == 0) {
                            this.listaBatimentoXML.Add(batimentoXmlAnbid);
                        }
                        #endregion
                    }
                    // XML Está na Collection do Financial
                    else {
                        // Se existe Diferença
                        if (view1[0].ValorMercado.HasValue && view1[0].ValorMercado != valor) {

                            #region Cria BatimentoXmlAnbid
                            BatimentoXmlAnbid batimentoXmlAnbid = new BatimentoXmlAnbid();
                            batimentoXmlAnbid.CdAtivo = cdAtivoBolsa;
                            batimentoXmlAnbid.IdCliente = idCliente;
                            batimentoXmlAnbid.Apelido = this.BuscaNomeCliente(idCliente);                            
                            //                            
                            batimentoXmlAnbid.ValorXmlAnbid = view1[0].ValorMercado.Value;
                            batimentoXmlAnbid.ValorFinancial = valor;
                            //
                            batimentoXmlAnbid.Diferenca = batimentoXmlAnbid.ValorFinancial - batimentoXmlAnbid.ValorXmlAnbid;
                            //
                            // Adiciona na Lista de Inconsistência somente se não existir Registro Igual
                            if (this.listaBatimentoXML.FindAll(batimentoXmlAnbid.Exists).Count == 0) {
                                this.listaBatimentoXML.Add(batimentoXmlAnbid);
                            }
                            #endregion
                        }
                    }

                    // Zera o Filtro
                    view1.Filter = "";

                    #endregion
                }
            }

            /// <summary>
            /// 
            /// </summary>
            /// <param name="posicaoEmprestimoBolsaXmlCollection"></param>
            /// <param name="posEmprestimoBolsaFinancialCollection"></param>
            private void MontaDivergenciasPosicaoEmprestimoBolsa(PosicaoEmprestimoBolsaCollection posicaoEmprestimoBolsaXmlCollection, PosicaoEmprestimoBolsaCollection posEmprestimoBolsaFinancialCollection) {
                foreach (PosicaoEmprestimoBolsa posicaoEmprestimoBolsa in posicaoEmprestimoBolsaXmlCollection) {
                    #region Determina se Collection Financial Está na Collection de XML

                    #region Find
                    esEntityCollectionView<PosicaoEmprestimoBolsa> view1 = new esEntityCollectionView<PosicaoEmprestimoBolsa>(posEmprestimoBolsaFinancialCollection);

                    // Procura por IdCliente, CdAtivo
                    StringBuilder filtro = new StringBuilder();
                    filtro = new StringBuilder();
                    filtro.Append("" + PosicaoEmprestimoBolsaMetadata.ColumnNames.IdCliente + " = '" + posicaoEmprestimoBolsa.IdCliente + "'");
                    filtro.Append(" AND " + PosicaoEmprestimoBolsaMetadata.ColumnNames.CdAtivoBolsa + " = '" + posicaoEmprestimoBolsa.CdAtivoBolsa + "'");
                    //                         
                    view1.Filter = filtro.ToString();

                    // Financial não está no XML
                    if (view1.Count == 0) {
                        #region Cria BatimentoXmlAnbid
                        BatimentoXmlAnbid batimentoXmlAnbid = new BatimentoXmlAnbid();

                        string aux = posicaoEmprestimoBolsa.TipoEmprestimo == (byte)PontaEmprestimoBolsa.Doador ? " Doado" : " Tomado";

                        batimentoXmlAnbid.CdAtivo = posicaoEmprestimoBolsa.CdAtivoBolsa + " - " +aux;
                        batimentoXmlAnbid.IdCliente = posicaoEmprestimoBolsa.IdCliente.Value;
                        batimentoXmlAnbid.Apelido = this.BuscaNomeCliente(posicaoEmprestimoBolsa.IdCliente.Value);
                        //
                        batimentoXmlAnbid.ValorXmlAnbid = posicaoEmprestimoBolsa.ValorMercado.Value;
                        batimentoXmlAnbid.ValorFinancial = 0;
                        //
                        batimentoXmlAnbid.Diferenca = batimentoXmlAnbid.ValorFinancial - batimentoXmlAnbid.ValorXmlAnbid;
                        //
                        // Adiciona na Lista de Inconsistência somente se não existir Registro Igual
                        if (this.listaBatimentoXML.FindAll(batimentoXmlAnbid.Exists).Count == 0) {
                            this.listaBatimentoXML.Add(batimentoXmlAnbid);
                        }
                        #endregion
                    }
                    // Financial Está no XML
                    else {
                        // Se existe Diferença
                        if (view1[0].ValorMercado.HasValue && view1[0].ValorMercado != posicaoEmprestimoBolsa.ValorMercado) {

                            #region Cria BatimentoXmlAnbid
                            BatimentoXmlAnbid batimentoXmlAnbid = new BatimentoXmlAnbid();

                            string aux = posicaoEmprestimoBolsa.TipoEmprestimo == (byte)PontaEmprestimoBolsa.Doador ? " Doado" : " Tomado";

                            batimentoXmlAnbid.CdAtivo = posicaoEmprestimoBolsa.CdAtivoBolsa + " - " +aux;
                            batimentoXmlAnbid.IdCliente = posicaoEmprestimoBolsa.IdCliente.Value;
                            batimentoXmlAnbid.Apelido = this.BuscaNomeCliente(posicaoEmprestimoBolsa.IdCliente.Value);
                            //
                            batimentoXmlAnbid.ValorXmlAnbid = posicaoEmprestimoBolsa.ValorMercado.Value;
                            batimentoXmlAnbid.ValorFinancial = view1[0].ValorMercado.Value;
                            //
                            batimentoXmlAnbid.Diferenca = batimentoXmlAnbid.ValorFinancial - batimentoXmlAnbid.ValorXmlAnbid;
                            //                            
                            // Adiciona na Lista de Inconsistência somente se não existir Registro Igual
                            if (this.listaBatimentoXML.FindAll(batimentoXmlAnbid.Exists).Count == 0) {
                                this.listaBatimentoXML.Add(batimentoXmlAnbid);
                            }
                            #endregion
                        }
                    }

                    // Zera o Filtro
                    view1.Filter = "";
                    #endregion

                    #endregion
                }

                for (int i = 0; i < posEmprestimoBolsaFinancialCollection.Count; i++) {
                    #region Determina se XML Está na Collection do Financial

                    int idCliente = posEmprestimoBolsaFinancialCollection[i].IdCliente.Value;
                    string cdAtivoBolsa = posEmprestimoBolsaFinancialCollection[i].CdAtivoBolsa.Trim();
                    decimal valor = posEmprestimoBolsaFinancialCollection[i].ValorMercado.Value;
                    //                    
                    esEntityCollectionView<PosicaoEmprestimoBolsa> view1 = new esEntityCollectionView<PosicaoEmprestimoBolsa>(posicaoEmprestimoBolsaXmlCollection);

                    // Procura por IdCliente, CdAtivo e IdAgente
                    StringBuilder filtro = new StringBuilder();
                    filtro = new StringBuilder();
                    filtro.Append("" + PosicaoEmprestimoBolsaMetadata.ColumnNames.IdCliente + " = '" + idCliente + "'");
                    filtro.Append(" AND " + PosicaoEmprestimoBolsaMetadata.ColumnNames.CdAtivoBolsa + " = '" + cdAtivoBolsa + "'");
                    //                         
                    view1.Filter = filtro.ToString();

                    // XML não está no Financial
                    if (view1.Count == 0) {

                        #region Cria BatimentoXmlAnbid
                        // Adiciona Informações de Financial
                        BatimentoXmlAnbid batimentoXmlAnbid = new BatimentoXmlAnbid();

                        string aux = posEmprestimoBolsaFinancialCollection[i].TipoEmprestimo == (byte)PontaEmprestimoBolsa.Doador ? " Doado" : " Tomado";

                        batimentoXmlAnbid.CdAtivo = cdAtivoBolsa + " - " + aux;
                        batimentoXmlAnbid.IdCliente = idCliente;
                        batimentoXmlAnbid.Apelido = this.BuscaNomeCliente(idCliente);
                        //
                        batimentoXmlAnbid.ValorXmlAnbid = 0;
                        batimentoXmlAnbid.ValorFinancial = valor;
                        //
                        batimentoXmlAnbid.Diferenca = batimentoXmlAnbid.ValorFinancial - batimentoXmlAnbid.ValorXmlAnbid;
                        //
                        // Adiciona na Lista de Inconsistência somente se não existir Registro Igual
                        if (this.listaBatimentoXML.FindAll(batimentoXmlAnbid.Exists).Count == 0) {
                            this.listaBatimentoXML.Add(batimentoXmlAnbid);
                        }
                        #endregion
                    }
                    // XML Está na Collection do Financial
                    else {
                        // Se existe Diferença
                        if (view1[0].ValorMercado.HasValue && view1[0].ValorMercado != valor) {

                            string aux = view1[0].TipoEmprestimo == (byte)PontaEmprestimoBolsa.Doador ? " Doado" : " Tomado";

                            #region Cria BatimentoXmlAnbid
                            BatimentoXmlAnbid batimentoXmlAnbid = new BatimentoXmlAnbid();
                            batimentoXmlAnbid.CdAtivo = cdAtivoBolsa + " - " +aux;
                            batimentoXmlAnbid.IdCliente = idCliente;
                            batimentoXmlAnbid.Apelido = this.BuscaNomeCliente(idCliente);
                            //                            
                            batimentoXmlAnbid.ValorXmlAnbid = view1[0].ValorMercado.Value;
                            batimentoXmlAnbid.ValorFinancial = valor;
                            //
                            batimentoXmlAnbid.Diferenca = batimentoXmlAnbid.ValorFinancial - batimentoXmlAnbid.ValorXmlAnbid;
                            //
                            // Adiciona na Lista de Inconsistência somente se não existir Registro Igual
                            if (this.listaBatimentoXML.FindAll(batimentoXmlAnbid.Exists).Count == 0) {
                                this.listaBatimentoXML.Add(batimentoXmlAnbid);
                            }
                            #endregion
                        }
                    }

                    // Zera o Filtro
                    view1.Filter = "";

                    #endregion
                }
            }

            /// <summary>
            /// 
            /// </summary>
            /// <param name="posicaoBMFXmlCollection"></param>
            /// <param name="posBMFFinancialCollection"></param>
            private void MontaDivergenciasPosicaoBMF(PosicaoBMFCollection posicaoBMFXmlCollection, PosicaoBMFCollection posBMFFinancialCollection) {
                foreach (PosicaoBMF posicaoBMF in posicaoBMFXmlCollection) {
                    #region Determina se Collection Financial Está na Collection de XML

                    #region Find
                    esEntityCollectionView<PosicaoBMF> view1 = new esEntityCollectionView<PosicaoBMF>(posBMFFinancialCollection);

                    // Procura por IdCliente, CdAtivo
                    StringBuilder filtro = new StringBuilder();
                    filtro = new StringBuilder();
                    filtro.Append("" + PosicaoBMFMetadata.ColumnNames.IdCliente + " = '" + posicaoBMF.IdCliente + "'");
                    filtro.Append(" AND " + PosicaoBMFMetadata.ColumnNames.CdAtivoBMF + " = '" + posicaoBMF.CdAtivoBMF + "'");
                    filtro.Append(" AND " + PosicaoBMFMetadata.ColumnNames.Serie + " = '" + posicaoBMF.Serie + "'");
                    //                         
                    view1.Filter = filtro.ToString();

                    // Financial não está no XML
                    if (view1.Count == 0) {
                        #region Cria BatimentoXmlAnbid
                        BatimentoXmlAnbid batimentoXmlAnbid = new BatimentoXmlAnbid();
                        batimentoXmlAnbid.CdAtivo = posicaoBMF.CdAtivoBMF + " "+posicaoBMF.Serie;
                        batimentoXmlAnbid.IdCliente = posicaoBMF.IdCliente.Value;
                        batimentoXmlAnbid.Apelido = this.BuscaNomeCliente(posicaoBMF.IdCliente.Value);
                        //
                        batimentoXmlAnbid.ValorXmlAnbid = posicaoBMF.ValorMercado.Value;
                        batimentoXmlAnbid.ValorFinancial = 0;
                        //
                        batimentoXmlAnbid.Diferenca = batimentoXmlAnbid.ValorFinancial - batimentoXmlAnbid.ValorXmlAnbid;
                        //
                        // Adiciona na Lista de Inconsistência somente se não existir Registro Igual
                        if (this.listaBatimentoXML.FindAll(batimentoXmlAnbid.Exists).Count == 0) {
                            this.listaBatimentoXML.Add(batimentoXmlAnbid);
                        }
                        #endregion
                    }
                    // Financial Está no XML
                    else {
                        // Se existe Diferença
                        if (view1[0].ValorMercado.HasValue && view1[0].ValorMercado != posicaoBMF.ValorMercado) {

                            #region Cria BatimentoXmlAnbid
                            BatimentoXmlAnbid batimentoXmlAnbid = new BatimentoXmlAnbid();
                            batimentoXmlAnbid.CdAtivo = posicaoBMF.CdAtivoBMF + " " + posicaoBMF.Serie;
                            batimentoXmlAnbid.IdCliente = posicaoBMF.IdCliente.Value;
                            batimentoXmlAnbid.Apelido = this.BuscaNomeCliente(posicaoBMF.IdCliente.Value);
                            //
                            batimentoXmlAnbid.ValorXmlAnbid = posicaoBMF.ValorMercado.Value;
                            batimentoXmlAnbid.ValorFinancial = view1[0].ValorMercado.Value;
                            //
                            batimentoXmlAnbid.Diferenca = batimentoXmlAnbid.ValorFinancial - batimentoXmlAnbid.ValorXmlAnbid;
                            //                            
                            // Adiciona na Lista de Inconsistência somente se não existir Registro Igual
                            if (this.listaBatimentoXML.FindAll(batimentoXmlAnbid.Exists).Count == 0) {
                                this.listaBatimentoXML.Add(batimentoXmlAnbid);
                            }
                            #endregion
                        }
                    }

                    // Zera o Filtro
                    view1.Filter = "";
                    #endregion

                    #endregion
                }

                for (int i = 0; i < posBMFFinancialCollection.Count; i++) {
                    #region Determina se XML Está na Collection do Financial

                    int idCliente = posBMFFinancialCollection[i].IdCliente.Value;
                    string cdAtivoBMF = posBMFFinancialCollection[i].CdAtivoBMF.Trim();
                    string serie = posBMFFinancialCollection[i].Serie.Trim();
                    decimal valor = posBMFFinancialCollection[i].ValorMercado.Value;
                    //                    
                    esEntityCollectionView<PosicaoBMF> view1 = new esEntityCollectionView<PosicaoBMF>(posicaoBMFXmlCollection);

                    // Procura por IdCliente, CdAtivo e IdAgente
                    StringBuilder filtro = new StringBuilder();
                    filtro = new StringBuilder();
                    filtro.Append("" + PosicaoBMFMetadata.ColumnNames.IdCliente + " = '" + idCliente + "'");
                    filtro.Append(" AND " + PosicaoBMFMetadata.ColumnNames.CdAtivoBMF + " = '" + cdAtivoBMF + "'");
                    filtro.Append(" AND " + PosicaoBMFMetadata.ColumnNames.Serie + " = '" + serie + "'");                    
                    //                         
                    view1.Filter = filtro.ToString();

                    // XML não está no Financial
                    if (view1.Count == 0) {

                        #region Cria BatimentoXmlAnbid
                        // Adiciona Informações de Financial
                        BatimentoXmlAnbid batimentoXmlAnbid = new BatimentoXmlAnbid();
                        batimentoXmlAnbid.CdAtivo = cdAtivoBMF + " " + serie;
                        batimentoXmlAnbid.IdCliente = idCliente;
                        batimentoXmlAnbid.Apelido = this.BuscaNomeCliente(idCliente);
                        //
                        batimentoXmlAnbid.ValorXmlAnbid = 0;
                        batimentoXmlAnbid.ValorFinancial = valor;
                        //
                        batimentoXmlAnbid.Diferenca = batimentoXmlAnbid.ValorFinancial - batimentoXmlAnbid.ValorXmlAnbid;
                        //
                        // Adiciona na Lista de Inconsistência somente se não existir Registro Igual
                        if (this.listaBatimentoXML.FindAll(batimentoXmlAnbid.Exists).Count == 0) {
                            this.listaBatimentoXML.Add(batimentoXmlAnbid);
                        }
                        #endregion
                    }
                    // XML Está na Collection do Financial
                    else {
                        // Se existe Diferença
                        if (view1[0].ValorMercado.HasValue && view1[0].ValorMercado != valor) {

                            #region Cria BatimentoXmlAnbid
                            BatimentoXmlAnbid batimentoXmlAnbid = new BatimentoXmlAnbid();
                            batimentoXmlAnbid.CdAtivo = cdAtivoBMF + " " + serie;
                            batimentoXmlAnbid.IdCliente = idCliente;
                            batimentoXmlAnbid.Apelido = this.BuscaNomeCliente(idCliente);
                            //                            
                            batimentoXmlAnbid.ValorXmlAnbid = view1[0].ValorMercado.Value;
                            batimentoXmlAnbid.ValorFinancial = valor;
                            //
                            batimentoXmlAnbid.Diferenca = batimentoXmlAnbid.ValorFinancial - batimentoXmlAnbid.ValorXmlAnbid;
                            //
                            // Adiciona na Lista de Inconsistência somente se não existir Registro Igual
                            if (this.listaBatimentoXML.FindAll(batimentoXmlAnbid.Exists).Count == 0) {
                                this.listaBatimentoXML.Add(batimentoXmlAnbid);
                            }
                            #endregion
                        }
                    }

                    // Zera o Filtro
                    view1.Filter = "";

                    #endregion
                }
            }

            /// <summary>
            /// 
            /// </summary>
            /// <param name="posicaoTermoBolsaXmlCollection"></param>
            /// <param name="posTermoBolsaFinancialCollection"></param>
            private void MontaDivergenciasPosicaoTermoBolsa(PosicaoTermoBolsaCollection posicaoTermoBolsaXmlCollection, PosicaoTermoBolsaCollection posTermoBolsaFinancialCollection) {
                foreach (PosicaoTermoBolsa posicaoTermoBolsa in posicaoTermoBolsaXmlCollection) {
                    #region Determina se Collection Financial Está na Collection de XML

                    #region Find
                    esEntityCollectionView<PosicaoTermoBolsa> view1 = new esEntityCollectionView<PosicaoTermoBolsa>(posTermoBolsaFinancialCollection);

                    // Procura por IdCliente, CdAtivo
                    StringBuilder filtro = new StringBuilder();
                    filtro = new StringBuilder();
                    filtro.Append("" + PosicaoTermoBolsaMetadata.ColumnNames.IdCliente + " = '" + posicaoTermoBolsa.IdCliente + "'");
                    filtro.Append(" AND " + PosicaoTermoBolsaMetadata.ColumnNames.CdAtivoBolsa + " = '" + posicaoTermoBolsa.CdAtivoBolsa + "'");
                    //                         
                    view1.Filter = filtro.ToString();

                    // Financial não está no XML
                    if (view1.Count == 0) {
                        #region Cria BatimentoXmlAnbid
                        BatimentoXmlAnbid batimentoXmlAnbid = new BatimentoXmlAnbid();
                        batimentoXmlAnbid.CdAtivo = posicaoTermoBolsa.CdAtivoBolsa;
                        batimentoXmlAnbid.IdCliente = posicaoTermoBolsa.IdCliente.Value;
                        batimentoXmlAnbid.Apelido = this.BuscaNomeCliente(posicaoTermoBolsa.IdCliente.Value);                        
                        //
                        batimentoXmlAnbid.ValorXmlAnbid = posicaoTermoBolsa.ValorMercado.Value;
                        batimentoXmlAnbid.ValorFinancial = 0;
                        //
                        batimentoXmlAnbid.Diferenca = batimentoXmlAnbid.ValorFinancial - batimentoXmlAnbid.ValorXmlAnbid;
                        //
                        // Adiciona na Lista de Inconsistência somente se não existir Registro Igual
                        if (this.listaBatimentoXML.FindAll(batimentoXmlAnbid.Exists).Count == 0) {
                            this.listaBatimentoXML.Add(batimentoXmlAnbid);
                        }
                        #endregion
                    }
                    // Financial Está no XML
                    else {
                        // Se existe Diferença
                        if (view1[0].ValorMercado.HasValue && view1[0].ValorMercado != posicaoTermoBolsa.ValorMercado) {

                            #region Cria BatimentoXmlAnbid
                            BatimentoXmlAnbid batimentoXmlAnbid = new BatimentoXmlAnbid();                            
                            batimentoXmlAnbid.CdAtivo = posicaoTermoBolsa.CdAtivoBolsa;
                            batimentoXmlAnbid.IdCliente = posicaoTermoBolsa.IdCliente.Value;
                            batimentoXmlAnbid.Apelido = this.BuscaNomeCliente(posicaoTermoBolsa.IdCliente.Value);
                            //
                            batimentoXmlAnbid.ValorXmlAnbid = posicaoTermoBolsa.ValorMercado.Value;
                            batimentoXmlAnbid.ValorFinancial = view1[0].ValorMercado.Value;
                            //
                            batimentoXmlAnbid.Diferenca = batimentoXmlAnbid.ValorFinancial - batimentoXmlAnbid.ValorXmlAnbid;
                            //                            
                            // Adiciona na Lista de Inconsistência somente se não existir Registro Igual
                            if (this.listaBatimentoXML.FindAll(batimentoXmlAnbid.Exists).Count == 0) {
                                this.listaBatimentoXML.Add(batimentoXmlAnbid);
                            }
                            #endregion
                        }
                    }

                    // Zera o Filtro
                    view1.Filter = "";
                    #endregion

                    #endregion
                }

                for (int i = 0; i < posTermoBolsaFinancialCollection.Count; i++) {
                    #region Determina se XML Está na Collection do Financial

                    int idCliente = posTermoBolsaFinancialCollection[i].IdCliente.Value;
                    string cdAtivoBolsa = posTermoBolsaFinancialCollection[i].CdAtivoBolsa.Trim();
                    decimal valor = posTermoBolsaFinancialCollection[i].ValorMercado.Value;
                    //                    
                    esEntityCollectionView<PosicaoTermoBolsa> view1 = new esEntityCollectionView<PosicaoTermoBolsa>(posicaoTermoBolsaXmlCollection);

                    // Procura por IdCliente, CdAtivo e IdAgente
                    StringBuilder filtro = new StringBuilder();
                    filtro = new StringBuilder();
                    filtro.Append("" + PosicaoTermoBolsaMetadata.ColumnNames.IdCliente + " = '" + idCliente + "'");
                    filtro.Append(" AND " + PosicaoTermoBolsaMetadata.ColumnNames.CdAtivoBolsa + " = '" + cdAtivoBolsa + "'");
                    //                         
                    view1.Filter = filtro.ToString();

                    // XML não está no Financial
                    if (view1.Count == 0) {

                        #region Cria BatimentoXmlAnbid
                        // Adiciona Informações de Financial
                        BatimentoXmlAnbid batimentoXmlAnbid = new BatimentoXmlAnbid();
                        batimentoXmlAnbid.CdAtivo = cdAtivoBolsa;
                        batimentoXmlAnbid.IdCliente = idCliente;
                        batimentoXmlAnbid.Apelido = this.BuscaNomeCliente(idCliente);
                        //
                        batimentoXmlAnbid.ValorXmlAnbid = 0;
                        batimentoXmlAnbid.ValorFinancial = valor;
                        //
                        batimentoXmlAnbid.Diferenca = batimentoXmlAnbid.ValorFinancial - batimentoXmlAnbid.ValorXmlAnbid;
                        //
                        // Adiciona na Lista de Inconsistência somente se não existir Registro Igual
                        if (this.listaBatimentoXML.FindAll(batimentoXmlAnbid.Exists).Count == 0) {
                            this.listaBatimentoXML.Add(batimentoXmlAnbid);
                        }
                        #endregion
                    }
                    // XML Está na Collection do Financial
                    else {
                        // Se existe Diferença
                        if (view1[0].ValorMercado.HasValue && view1[0].ValorMercado != valor) {                           
                            #region Cria BatimentoXmlAnbid
                            BatimentoXmlAnbid batimentoXmlAnbid = new BatimentoXmlAnbid();
                            batimentoXmlAnbid.CdAtivo = cdAtivoBolsa;
                            batimentoXmlAnbid.IdCliente = idCliente;
                            batimentoXmlAnbid.Apelido = this.BuscaNomeCliente(idCliente);
                            //                            
                            batimentoXmlAnbid.ValorXmlAnbid = view1[0].ValorMercado.Value;
                            batimentoXmlAnbid.ValorFinancial = valor;
                            //
                            batimentoXmlAnbid.Diferenca = batimentoXmlAnbid.ValorFinancial - batimentoXmlAnbid.ValorXmlAnbid;
                            //
                            // Adiciona na Lista de Inconsistência somente se não existir Registro Igual
                            if (this.listaBatimentoXML.FindAll(batimentoXmlAnbid.Exists).Count == 0) {
                                this.listaBatimentoXML.Add(batimentoXmlAnbid);
                            }
                            #endregion
                        }
                    }

                    // Zera o Filtro
                    view1.Filter = "";

                    #endregion
                }
            }

            /// <summary>
            /// 
            /// </summary>
            /// <param name="posicaoFundoXmlCollection"></param>
            /// <param name="posFundoFinancialCollection"></param>
            private void MontaDivergenciasPosicaoFundo(PosicaoFundoCollection posicaoFundoXmlCollection, PosicaoFundoCollection posFundoFinancialCollection) {
                foreach (PosicaoFundo posicaoFundo in posicaoFundoXmlCollection) {
                    #region Determina se Collection Financial Está na Collection de XML

                    #region Find
                    esEntityCollectionView<PosicaoFundo> view1 = new esEntityCollectionView<PosicaoFundo>(posFundoFinancialCollection);

                    // Procura por IdCliente, CdAtivo
                    StringBuilder filtro = new StringBuilder();
                    filtro = new StringBuilder();
                    filtro.Append("" + PosicaoFundoMetadata.ColumnNames.IdCliente + " = '" + posicaoFundo.IdCliente + "'");
                    filtro.Append(" AND " + PosicaoFundoMetadata.ColumnNames.IdCarteira + " = '" + posicaoFundo.IdCarteira + "'");
                    //                         
                    view1.Filter = filtro.ToString();

                    // Financial não está no XML
                    if (view1.Count == 0) {
                        #region Cria BatimentoXmlAnbid
                        BatimentoXmlAnbid batimentoXmlAnbid = new BatimentoXmlAnbid();
                        //
                        batimentoXmlAnbid.IdCliente = posicaoFundo.IdCliente.Value;
                        batimentoXmlAnbid.Apelido = this.BuscaNomeCliente(posicaoFundo.IdCliente.Value);
                        //
                        batimentoXmlAnbid.CdAtivo = " >> "+this.BuscaNomeCarteira(posicaoFundo.IdCarteira.Value);
                        //
                        batimentoXmlAnbid.ValorXmlAnbid = posicaoFundo.ValorBruto.Value;
                        batimentoXmlAnbid.ValorFinancial = 0;
                        //
                        batimentoXmlAnbid.Diferenca = batimentoXmlAnbid.ValorFinancial - batimentoXmlAnbid.ValorXmlAnbid;
                        //
                        // Adiciona na Lista de Inconsistência somente se não existir Registro Igual
                        if (this.listaBatimentoXML.FindAll(batimentoXmlAnbid.Exists).Count == 0) {
                            this.listaBatimentoXML.Add(batimentoXmlAnbid);
                        }
                        #endregion
                    }
                    // Financial Está no XML
                    else {
                        // Se existe Diferença
                        if (view1[0].ValorBruto.HasValue && view1[0].ValorBruto != posicaoFundo.ValorBruto) {

                            #region Cria BatimentoXmlAnbid
                            BatimentoXmlAnbid batimentoXmlAnbid = new BatimentoXmlAnbid();
                            //
                            batimentoXmlAnbid.IdCliente = posicaoFundo.IdCliente.Value;
                            batimentoXmlAnbid.Apelido = this.BuscaNomeCliente(posicaoFundo.IdCliente.Value);
                            //
                            batimentoXmlAnbid.CdAtivo = " >> " + this.BuscaNomeCarteira(posicaoFundo.IdCarteira.Value);
                            //
                            batimentoXmlAnbid.ValorXmlAnbid = posicaoFundo.ValorBruto.Value;
                            batimentoXmlAnbid.ValorFinancial = view1[0].ValorBruto.Value;
                            //
                            batimentoXmlAnbid.Diferenca = batimentoXmlAnbid.ValorFinancial - batimentoXmlAnbid.ValorXmlAnbid;
                            //                            
                            // Adiciona na Lista de Inconsistência somente se não existir Registro Igual
                            if (this.listaBatimentoXML.FindAll(batimentoXmlAnbid.Exists).Count == 0) {
                                this.listaBatimentoXML.Add(batimentoXmlAnbid);
                            }
                            #endregion
                        }
                    }

                    // Zera o Filtro
                    view1.Filter = "";
                    #endregion

                    #endregion
                }

                for (int i = 0; i < posFundoFinancialCollection.Count; i++) {
                    #region Determina se XML Está na Collection do Financial

                    int idCliente = posFundoFinancialCollection[i].IdCliente.Value;
                    int idCarteira = posFundoFinancialCollection[i].IdCarteira.Value;
                    decimal valorBruto = posFundoFinancialCollection[i].ValorBruto.Value;
                    //                    
                    esEntityCollectionView<PosicaoFundo> view1 = new esEntityCollectionView<PosicaoFundo>(posicaoFundoXmlCollection);

                    // Procura por IdCliente
                    StringBuilder filtro = new StringBuilder();
                    filtro = new StringBuilder();
                    filtro.Append("" + PosicaoFundoMetadata.ColumnNames.IdCliente + " = '" + idCliente + "'");
                    filtro.Append(" AND " + PosicaoFundoMetadata.ColumnNames.IdCarteira + " = '" + idCarteira + "'");
                    //                         
                    view1.Filter = filtro.ToString();

                    // XML não está no Financial
                    if (view1.Count == 0) {

                        #region Cria BatimentoXmlAnbid
                        // Adiciona Informações de Financial
                        BatimentoXmlAnbid batimentoXmlAnbid = new BatimentoXmlAnbid();
                        //
                        batimentoXmlAnbid.CdAtivo = " >> " +this.BuscaNomeCarteira(idCarteira);
                        //
                        batimentoXmlAnbid.IdCliente = idCliente;
                        batimentoXmlAnbid.Apelido = this.BuscaNomeCliente(idCliente);
                        //
                        batimentoXmlAnbid.ValorXmlAnbid = 0;
                        batimentoXmlAnbid.ValorFinancial = valorBruto;
                        //
                        batimentoXmlAnbid.Diferenca = batimentoXmlAnbid.ValorFinancial - batimentoXmlAnbid.ValorXmlAnbid;
                        //
                        // Adiciona na Lista de Inconsistência somente se não existir Registro Igual
                        if (this.listaBatimentoXML.FindAll(batimentoXmlAnbid.Exists).Count == 0) {
                            this.listaBatimentoXML.Add(batimentoXmlAnbid);
                        }
                        #endregion
                    }
                    // XML Está na Collection do Financial
                    else {
                        // Se existe Diferença
                        if (view1[0].ValorBruto.HasValue && view1[0].ValorBruto != valorBruto) {

                            #region Cria BatimentoXmlAnbid
                            BatimentoXmlAnbid batimentoXmlAnbid = new BatimentoXmlAnbid();
                            //
                            batimentoXmlAnbid.CdAtivo = " >> " + this.BuscaNomeCarteira(idCarteira);
                            //
                            batimentoXmlAnbid.IdCliente = idCliente;                            
                            batimentoXmlAnbid.Apelido = this.BuscaNomeCliente(idCliente);
                            //                            
                            batimentoXmlAnbid.ValorXmlAnbid = view1[0].ValorBruto.Value;
                            batimentoXmlAnbid.ValorFinancial = valorBruto;
                            //
                            batimentoXmlAnbid.Diferenca = batimentoXmlAnbid.ValorFinancial - batimentoXmlAnbid.ValorXmlAnbid;
                            //
                            // Adiciona na Lista de Inconsistência somente se não existir Registro Igual
                            if (this.listaBatimentoXML.FindAll(batimentoXmlAnbid.Exists).Count == 0) {
                                this.listaBatimentoXML.Add(batimentoXmlAnbid);
                            }
                            #endregion
                        }
                    }

                    // Zera o Filtro
                    view1.Filter = "";

                    #endregion
                }
            }

            /// <summary>
            /// 
            /// </summary>
            /// <param name="saldoCaixaXml"></param>
            /// <param name="saldoCaixaFinancial"></param>
            private void MontaDivergenciasSaldoCaixa(SaldoCaixa saldoCaixaXml, SaldoCaixa saldoCaixaFinancial) {
                
                // Compara Saldo Fechamento
                decimal saldoFechamentoFinancial = saldoCaixaFinancial.SaldoFechamento.HasValue ? saldoCaixaFinancial.SaldoFechamento.Value : 0;

                if (saldoCaixaXml.SaldoFechamento.Value != saldoFechamentoFinancial) {
                    #region Cria BatimentoXmlAnbid
                    BatimentoXmlAnbid batimentoXmlAnbid = new BatimentoXmlAnbid();
                    batimentoXmlAnbid.CdAtivo = " >> Saldo C/C";
                    batimentoXmlAnbid.IdCliente = saldoCaixaXml.IdCliente.Value;
                    batimentoXmlAnbid.Apelido = this.BuscaNomeCliente(saldoCaixaXml.IdCliente.Value);
                    //
                    batimentoXmlAnbid.ValorXmlAnbid = saldoCaixaXml.SaldoFechamento.Value;
                    batimentoXmlAnbid.ValorFinancial = saldoFechamentoFinancial;
                    //
                    batimentoXmlAnbid.Diferenca = batimentoXmlAnbid.ValorFinancial - batimentoXmlAnbid.ValorXmlAnbid;
                    //
                    // Adiciona na Lista de Inconsistência somente se não existir Registro Igual
                    if (this.listaBatimentoXML.FindAll(batimentoXmlAnbid.Exists).Count == 0) {
                        this.listaBatimentoXML.Add(batimentoXmlAnbid);
                    }
                    #endregion
                }
            }

            #endregion

            #region Busca Apelido
            /// <summary>
            /// Busca Apelido na tabela Cliente, Caso não Encontrar busca na Tabela Pessoa
            /// </summary>
            /// <param name="idCliente"></param>
            /// <returns></returns>
            private string BuscaNomeCliente(int idCliente) {                
                Cliente cliente = new Cliente();
                List<esQueryItem> campos = new List<esQueryItem>();
                campos.Add(cliente.Query.Apelido);

                string apelido = "";

                if (!cliente.LoadByPrimaryKey(campos, idCliente)) {
                    Pessoa pessoa = new Pessoa();
                    List<esQueryItem> camposAux = new List<esQueryItem>();
                    camposAux.Add(pessoa.Query.Apelido);
                    //
                    if (pessoa.LoadByPrimaryKey(camposAux, idCliente)) {
                        apelido = pessoa.str.Apelido.Trim();
                    }                    
                }
                else {
                    apelido = cliente.str.Apelido.Trim();
                }

                return apelido;
            }

            /// <summary>
            /// Busca Apelido na tabela Carteira, Caso não Encontrar busca na Tabela Pessoa
            /// </summary>
            /// <param name="idCarteira"></param>
            /// <returns></returns>
            private string BuscaNomeCarteira(int idCarteira) {
                Carteira carteira = new Carteira();
                List<esQueryItem> campos = new List<esQueryItem>();
                campos.Add(carteira.Query.Apelido);

                string apelido = "";

                if (!carteira.LoadByPrimaryKey(campos, idCarteira)) {
                    Pessoa pessoa = new Pessoa();
                    List<esQueryItem> camposAux = new List<esQueryItem>();
                    camposAux.Add(pessoa.Query.Apelido);
                    //
                    if (pessoa.LoadByPrimaryKey(camposAux, idCarteira)) {
                        apelido = pessoa.str.Apelido.Trim();
                    }
                }
                else {
                    apelido = carteira.str.Apelido.Trim();
                }

                return apelido;
            }
            #endregion
    }
}