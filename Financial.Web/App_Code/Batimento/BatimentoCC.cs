﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel;
using Financial.Investidor;
using Financial.Investidor.Enums;
using Financial.Bolsa;
using EntitySpaces.Interfaces;
using Financial.ContaCorrente;
using Financial.Interfaces.Sinacor;
using Financial.WebConfigConfiguration;
using Financial.Bolsa.Exceptions;
using Financial.Util;
using Financial.Security;
using System.Web;

namespace Financial.Batimentos {
    
    /// <summary>
    /// Binding para Batimento de Conta Corrente X Sinacor
    /// </summary>
    #region Composicao BatimentoCC

    public class BatimentoCC {

        public BatimentoCC() { }

        public BatimentoCC(int idCliente, string apelido, DateTime dataDia,
                           decimal? saldoFinancial, decimal? saldoSinacor, decimal? diferenca) {
            this.idCliente = idCliente;
            this.apelido = apelido;
            this.dataDia = dataDia;
            this.saldoFinancial = saldoFinancial;
            this.saldoSinacor = saldoSinacor;
            this.diferenca = diferenca;
        }

        private int idCliente;
        private string apelido;
        private DateTime dataDia;
        private decimal? saldoFinancial;
        private decimal? saldoSinacor;
        private decimal? diferenca;

        public int IdCliente {
            get { return idCliente; }
            set { idCliente = value; }
        }
        public string Apelido {
            get { return apelido; }
            set { apelido = value; }
        }
        public DateTime DataDia {
            get { return dataDia; }
            set { dataDia = value; }
        }
        public decimal? SaldoFinancial {
            get { return saldoFinancial; }
            set { saldoFinancial = value; }
        }
        public decimal? SaldoSinacor {
            get { return saldoSinacor; }
            set { saldoSinacor = value; }
        }
        public decimal? Diferenca {
            get { return diferenca; }
            set { diferenca = value; }
        }

        /* Armazena o idCliente/DataDia do Erro se existir */
        private int? errorIdCliente = null;
        private DateTime dataErrorCliente = DateTime.Now;

        /// <summary>
        /// Binding Do grid
        /// </summary>
        /// <param name="idGrupo"></param>
        /// <returns></returns>
        public BindingList<BatimentoCC> CarregaGridCCSinacor(int idGrupo) {
            this.errorIdCliente = null;

            ClienteCollection clienteCollection = new ClienteCollection();

            ClienteQuery clienteQuery = new ClienteQuery("C");
            PermissaoClienteQuery permissaoClienteQuery = new PermissaoClienteQuery("P");
            UsuarioQuery usuarioQuery = new UsuarioQuery("U");

            clienteQuery.InnerJoin(permissaoClienteQuery).On(permissaoClienteQuery.IdCliente == clienteQuery.IdCliente);
            clienteQuery.InnerJoin(usuarioQuery).On(usuarioQuery.IdUsuario == permissaoClienteQuery.IdUsuario);

            if (idGrupo > 0)
            { // Branco - Sem nenhum Grupo escolhido
                clienteQuery.Where(clienteQuery.IdGrupoProcessamento == idGrupo);
            }

            clienteQuery.Where(clienteQuery.TipoControle != TipoControleCliente.ApenasCotacao,
                             clienteQuery.TipoControle != TipoControleCliente.Cotista,
                             clienteQuery.TipoControle != TipoControleCliente.CarteiraImportada,
                             clienteQuery.TipoControle != TipoControleCliente.CarteiraSimulada,
                             clienteQuery.StatusAtivo == StatusAtivoCliente.Ativo,
                             clienteQuery.IsProcessando == "N",
                             usuarioQuery.Login.Equal(HttpContext.Current.User.Identity.Name));
            //
            clienteQuery.OrderBy(clienteQuery.Apelido.Ascending);
            clienteCollection.Load(clienteQuery);
            clienteCollection.Query.Load();

            BindingList<BatimentoCC> listaBatimentoCC = new BindingList<BatimentoCC>();

            try 
            {
                #region Loop For

            foreach (Cliente cliente in clienteCollection) {
                int idCliente = cliente.IdCliente.Value;
                string apelido = cliente.Apelido;
                DateTime dataDia = cliente.DataDia.Value;

                #region Busca códigos do cliente (codigoCliente, codigoCliente2 na Bovespa)
                int? codigoSinacor = null;
                int? codigoSinacor2 = null;

                ClienteBolsa clienteBolsa = new ClienteBolsa();

                //Se não achar pelo menos o código do cliente associado, sai da função
                if (!clienteBolsa.LoadByPrimaryKey(idCliente)) {
                    this.errorIdCliente = idCliente;
                    this.dataErrorCliente = cliente.DataDia.Value;
                    throw new CodigoClienteBovespaInvalido("Cód. Bovespa Inválido/Inexistente p/ Cliente ");
                }

                //Se não tiver código associado, lança exception
                if (String.IsNullOrEmpty(clienteBolsa.CodigoSinacor) && String.IsNullOrEmpty(clienteBolsa.CodigoSinacor2)) {
                    this.errorIdCliente = idCliente;
                    this.dataErrorCliente = cliente.DataDia.Value;
                    throw new CodigoClienteBovespaInvalido("Cód. Bovespa Inválido/Inexistente p/ IdCliente ");
                }

                //Se nenhum dos 2 códigos é número, lança exception
                if (!Utilitario.IsInteger(clienteBolsa.CodigoSinacor) && !Utilitario.IsInteger(clienteBolsa.CodigoSinacor2)) {
                    this.errorIdCliente = idCliente;
                    this.dataErrorCliente = cliente.DataDia.Value;
                    throw new CodigoClienteBovespaInvalido("Cód. Bovespa Inválido/Inexistente p/ o IdCliente ");
                }

                if (!String.IsNullOrEmpty(clienteBolsa.CodigoSinacor)) {
                    codigoSinacor = Convert.ToInt32(clienteBolsa.CodigoSinacor);
                }

                if (!String.IsNullOrEmpty(clienteBolsa.CodigoSinacor2)) {
                    codigoSinacor2 = Convert.ToInt32(clienteBolsa.CodigoSinacor2);
                }
                #endregion

                SaldoCaixa saldoCaixa = new SaldoCaixa();
                saldoCaixa.BuscaSaldoCaixa(idCliente, dataDia);
                decimal saldoFinancial = 0;
                if (saldoCaixa.SaldoFechamento.HasValue) {
                    saldoFinancial = saldoCaixa.SaldoFechamento.Value;
                }

                //Trago o valor de liquidação no Sinacor na TCCMOVTO
                //Esse valor será abatido, já que no Financial o valor liquidando em D0 não deve ser levado em conta
                decimal liquidacaoDia = 0;
                Tccmovto tccmovto = new Tccmovto();
                tccmovto.es.Connection.Name = "Sinacor";
                tccmovto.es.Connection.Schema = ParametrosConfiguracaoSistema.Integracoes.SchemaSinacor;
                tccmovto.Query.Select(tccmovto.Query.VlLancamento.Sum());

                if (codigoSinacor.HasValue && codigoSinacor2.HasValue) {
                    tccmovto.Query.Where(tccmovto.Query.CdCliente.In(codigoSinacor.Value, codigoSinacor2.Value));
                }
                else if (codigoSinacor.HasValue) {
                    tccmovto.Query.Where(tccmovto.Query.CdCliente == codigoSinacor.Value);
                }

                DateTime dataHoje = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day);
                tccmovto.Query.Where(tccmovto.Query.DtLiquidacao == dataHoje);
                if (tccmovto.Query.Load()) {
                    if (tccmovto.VlLancamento.HasValue) {
                        liquidacaoDia = tccmovto.VlLancamento.Value;
                    }
                }
                //                

                // Pega o Total de Valores por historico, para excluir aqueles presentes na lista
                decimal liquidacaoExcluir = 0;
                //
                // Pega do WebConfig
                string listaNumeros = WebConfig.AppSettings.ListaNumeros.Trim();

                if (!String.IsNullOrEmpty(listaNumeros)) {
                    #region Converte Lista Do TextEdit em Decimal
                    string[] lista = listaNumeros.Split(new Char[] { ',' });
                    
                    List<decimal> listaDecimal = new List<decimal>(lista.Length);

                    for (int i = 0; i < lista.Length; i++) {
                        listaDecimal.Add(Convert.ToDecimal(lista[i]));
                    }
                    #endregion

                    VTccmovto tccmovtoExcluir = new VTccmovto();
                    tccmovtoExcluir.es.Connection.Name = "Sinacor";
                    tccmovtoExcluir.es.Connection.Schema = ParametrosConfiguracaoSistema.Integracoes.SchemaSinacor;
                    tccmovtoExcluir.Query.Select(tccmovtoExcluir.Query.VlLancamento.Sum());

                    if (codigoSinacor.HasValue && codigoSinacor2.HasValue) {
                        tccmovtoExcluir.Query.Where(tccmovtoExcluir.Query.CdCliente.In(codigoSinacor.Value, codigoSinacor2.Value));
                    }
                    else if (codigoSinacor.HasValue) {
                        tccmovtoExcluir.Query.Where(tccmovtoExcluir.Query.CdCliente.Equal(codigoSinacor.Value));
                    }

                    tccmovtoExcluir.Query.Where(tccmovtoExcluir.Query.CdHistorico.In(listaDecimal));

                    if (tccmovtoExcluir.Query.Load() && tccmovtoExcluir.VlLancamento.HasValue) {
                        if (tccmovtoExcluir.VlLancamento.HasValue) {
                            liquidacaoExcluir = tccmovtoExcluir.VlLancamento.Value;
                        }
                    }
                }

                Tccsaldo tccsaldo = new Tccsaldo();
                decimal saldoSinacor = 0;
                if (codigoSinacor.HasValue && codigoSinacor2.HasValue) {                    
                    saldoSinacor = tccsaldo.RetornaSaldo(codigoSinacor.Value) +
                                   tccsaldo.RetornaSaldo(codigoSinacor2.Value); //Soma da conta deposito e investimento
                    saldoSinacor -= liquidacaoDia; //Pq o Sinacor trabalha com D0!
                    saldoSinacor -= liquidacaoExcluir;
                }
                else if (codigoSinacor.HasValue) {
                    saldoSinacor = tccsaldo.RetornaSaldo(codigoSinacor.Value);
                    saldoSinacor -= liquidacaoDia; //Pq o Sinacor trabalha com D0!
                    saldoSinacor -= liquidacaoExcluir;
                }

                decimal diferenca = saldoFinancial - saldoSinacor;

                if (diferenca != 0) {
                    listaBatimentoCC.Add(new BatimentoCC(idCliente, apelido, dataDia, saldoFinancial, saldoSinacor, diferenca));
                }
            }

            #endregion
            }
            catch (Exception e) {

                if (!this.errorIdCliente.HasValue) {
                    this.errorIdCliente = -1;
                }
                
                listaBatimentoCC.Add(new BatimentoCC(this.errorIdCliente.Value, e.Message, this.dataErrorCliente, null, null, null));
                //throw new Exception(e.Message);
            }

            return listaBatimentoCC;
        }
    }

    #endregion
}