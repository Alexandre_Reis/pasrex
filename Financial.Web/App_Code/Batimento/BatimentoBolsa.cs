﻿using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.ComponentModel;
using Financial.Investidor;
using Financial.Investidor.Enums;
using Financial.Bolsa;
using Financial.Util;
using Financial.Bolsa.Exceptions;
using Financial.Interfaces.Sinacor;
using Financial.Bolsa.Enums;
using Financial.Security;
using Financial.BMF;
using Financial.BMF.Enums;
using Financial.BMF.Exceptions;
using Financial.Tributo.Enums;

namespace Financial.Batimentos {				

    public class BatimentoBolsa : BatimentoCC {

        public BatimentoBolsa() { }

        public BatimentoBolsa(int idCliente, string apelido, DateTime dataDia, string cdAtivoBolsa,
                              decimal? saldoFinancial, decimal? saldoSinacor, decimal? diferenca)                                  
                : base(idCliente, apelido, dataDia, saldoFinancial, saldoSinacor, diferenca) {
           
            this.cdAtivoBolsa = cdAtivoBolsa;                
        }

        private string cdAtivoBolsa;

        public string CdAtivoBolsa {
            get { return cdAtivoBolsa; }
            set { cdAtivoBolsa = value; }
        }

        /* Armazena o idCliente/DataDia do Erro se existir */
        private int? errorIdCliente = null;
        private DateTime dataErrorCliente = DateTime.Now;

        /// <summary>
        /// Binding do Grid
        /// </summary>
        /// <param name="idGrupo"></param>
        /// <param name="tipoMercado">-1 = Branco </param>
        /// <returns></returns>
        public BindingList<BatimentoBolsa> CarregaGridBolsaSinacor(int idGrupo, int tipoMercado) {
            this.errorIdCliente = null;

            BindingList<BatimentoBolsa> listaBatimentoBolsa = new BindingList<BatimentoBolsa>();
            //
            ClienteCollection clienteCollection = new ClienteCollection();
            //
            ClienteQuery clienteQuery = new ClienteQuery("C");
            PermissaoClienteQuery permissaoClienteQuery = new PermissaoClienteQuery("P");
            UsuarioQuery usuarioQuery = new UsuarioQuery("U");

            clienteQuery.InnerJoin(permissaoClienteQuery).On(permissaoClienteQuery.IdCliente == clienteQuery.IdCliente);
            clienteQuery.InnerJoin(usuarioQuery).On(usuarioQuery.IdUsuario == permissaoClienteQuery.IdUsuario);

            if (idGrupo >= 0) 
            { // Branco - Sem nenhum Grupo escolhido
                clienteQuery.Where(clienteQuery.IdGrupoProcessamento == idGrupo);
            }

            clienteQuery.Where(clienteQuery.TipoControle != TipoControleCliente.ApenasCotacao,
                             clienteQuery.TipoControle != TipoControleCliente.Cotista,
                             clienteQuery.TipoControle != TipoControleCliente.CarteiraImportada,
                             clienteQuery.TipoControle != TipoControleCliente.CarteiraSimulada,
                             clienteQuery.StatusAtivo == StatusAtivoCliente.Ativo,
                             clienteQuery.IsProcessando == "N",
                             usuarioQuery.Login.Equal(HttpContext.Current.User.Identity.Name));
            //
            clienteQuery.OrderBy(clienteQuery.Apelido.Ascending);
            clienteCollection.Load(clienteQuery);

             try {

                #region Loop For
                foreach (Cliente cliente in clienteCollection) 
                {
                    int idCliente = cliente.IdCliente.Value;

                    #region Busca códigos do cliente (codigoCliente, codigoCliente2 na Bovespa)
                    int codigoCliente = 0;
                    int codigoCliente2 = 0;

                    ClienteBolsa clienteBolsa = new ClienteBolsa();
                    
                    //Se não achar pelo menos o código do cliente associado, sai da função
                    if (!clienteBolsa.LoadByPrimaryKey(idCliente)) {
                        this.errorIdCliente = idCliente;
                        this.dataErrorCliente = cliente.DataDia.Value;
                        throw new CodigoClienteBovespaInvalido("Cód. Bovespa Inválido/Inexistente p/ Cliente ");
                    }

                    //Se não tiver código associado, lança exception
                    if (String.IsNullOrEmpty(clienteBolsa.CodigoSinacor) && String.IsNullOrEmpty(clienteBolsa.CodigoSinacor2)) {
                        this.errorIdCliente = idCliente;
                        this.dataErrorCliente = cliente.DataDia.Value;
                        throw new CodigoClienteBovespaInvalido("Cód. Bovespa Inválido/Inexistente p/ IdCliente ");
                    }

                    //Se nenhum dos 2 códigos é número, lança exception
                    if (!Utilitario.IsInteger(clienteBolsa.CodigoSinacor) && !Utilitario.IsInteger(clienteBolsa.CodigoSinacor2)) {
                        this.errorIdCliente = idCliente;
                        this.dataErrorCliente = cliente.DataDia.Value;
                        throw new CodigoClienteBovespaInvalido("Cód. Bovespa Inválido/Inexistente p/ o IdCliente ");
                    }

                    if (!String.IsNullOrEmpty(clienteBolsa.CodigoSinacor)) {
                        codigoCliente = Convert.ToInt32(clienteBolsa.CodigoSinacor);
                    }

                    if (!String.IsNullOrEmpty(clienteBolsa.CodigoSinacor2)) {
                        codigoCliente2 = Convert.ToInt32(clienteBolsa.CodigoSinacor2);
                    }

                    //Se algum dos 2 códigos for nulo, repete o código de um para ao outro
                    if (String.IsNullOrEmpty(clienteBolsa.CodigoSinacor)) {
                        codigoCliente = codigoCliente2;
                    }

                    if (String.IsNullOrEmpty(clienteBolsa.CodigoSinacor2)) {
                        codigoCliente2 = codigoCliente;
                    }
                    #endregion

                    string apelido = cliente.Apelido;
                    DateTime dataDia = cliente.DataDia.Value;
                    
                    // Acões/Opções
                    if (tipoMercado == 0 || tipoMercado == -1) 
                    {
                        #region Batimento Ações e Opções
                        VcfposicaoCollection vcfposicaoCollection = new VcfposicaoCollection();
                        vcfposicaoCollection.es.Connection.Name = "Sinacor";
                        vcfposicaoCollection.es.Connection.Schema = ParametrosConfiguracaoSistema.Integracoes.SchemaSinacor;

                        vcfposicaoCollection.Query.Select(vcfposicaoCollection.Query.CodNeg,
                                                          vcfposicaoCollection.Query.QtdeTot.Sum());

                        vcfposicaoCollection.Query.Where(
                                             (vcfposicaoCollection.Query.CodCli == codigoCliente | vcfposicaoCollection.Query.CodCli == codigoCliente2) &
                                             (vcfposicaoCollection.Query.TipoMerc.Like("VIS%") |
                                              vcfposicaoCollection.Query.TipoMerc.Like("OPC%") |
                                              vcfposicaoCollection.Query.TipoMerc.Like("OPV%"))
                                            );
                        //
                        vcfposicaoCollection.Query.Where(vcfposicaoCollection.Query.TipoGrup == "ACAO");
                        //
                        vcfposicaoCollection.Query.GroupBy(vcfposicaoCollection.Query.CodNeg);
                        //
                        vcfposicaoCollection.Query.OrderBy(vcfposicaoCollection.Query.CodNeg.Ascending);
                        vcfposicaoCollection.Query.Load();

                        foreach (Vcfposicao vcfposicao in vcfposicaoCollection) {
                            string cdAtivoBolsa = vcfposicao.CodNeg;
                            decimal quantidadeSinacor = vcfposicao.QtdeTot.Value;

                            PosicaoBolsa posicaoBolsa = new PosicaoBolsa();
                            decimal quantidadeFinancial = posicaoBolsa.RetornaQuantidadeAtivo(idCliente, cdAtivoBolsa);

                            decimal diferenca = quantidadeFinancial - quantidadeSinacor;

                            if (diferenca != 0)
                            {
                                if (ParametrosConfiguracaoSistema.Bolsa.CustodiaBTC == (int)CustodiaBTC.NaoAltera ||
                                   (ParametrosConfiguracaoSistema.Bolsa.CustodiaBTC == (int)CustodiaBTC.AlteraClientesSemIR && cliente.ApuraGanhoRV != TipoApuracaoGanhoRV.NaoApura))
                                {
                                    PosicaoEmprestimoBolsa pEmprestimo = new PosicaoEmprestimoBolsa();
                                    decimal quantidadeDoada = pEmprestimo.RetornaQuantidade(idCliente, cdAtivoBolsa, PontaEmprestimoBolsa.Doador);
                                    decimal quantidadeTomada = pEmprestimo.RetornaQuantidade(idCliente, cdAtivoBolsa, PontaEmprestimoBolsa.Tomador);

                                    quantidadeFinancial = quantidadeFinancial - quantidadeDoada + quantidadeTomada;

                                    diferenca = quantidadeFinancial - quantidadeSinacor;
                                }
                            }

                            if (diferenca != 0) 
                            {
                                listaBatimentoBolsa.Add(new BatimentoBolsa(idCliente, apelido, dataDia, cdAtivoBolsa,
                                                                           quantidadeFinancial, quantidadeSinacor, diferenca));
                            }
                        }

                        PosicaoBolsaCollection posicaoBolsaCollection = new PosicaoBolsaCollection();
                        //
                        posicaoBolsaCollection.Query.Select(posicaoBolsaCollection.Query.CdAtivoBolsa,
                                                            posicaoBolsaCollection.Query.Quantidade.Sum());

                        posicaoBolsaCollection.Query.Where(posicaoBolsaCollection.Query.IdCliente == idCliente,
                                                           posicaoBolsaCollection.Query.TipoMercado != TipoMercadoBolsa.Termo);

                        posicaoBolsaCollection.Query.GroupBy(posicaoBolsaCollection.Query.CdAtivoBolsa);

                        posicaoBolsaCollection.Query.OrderBy(posicaoBolsaCollection.Query.CdAtivoBolsa.Ascending);

                        posicaoBolsaCollection.Query.Load();

                        foreach (PosicaoBolsa posicaoBolsa in posicaoBolsaCollection) {
                            string cdAtivoBolsa = posicaoBolsa.CdAtivoBolsa;
                            decimal quantidadeFinancial = posicaoBolsa.Quantidade.Value;

                            if (ParametrosConfiguracaoSistema.Bolsa.CustodiaBTC == (int)CustodiaBTC.NaoAltera ||
                                   (ParametrosConfiguracaoSistema.Bolsa.CustodiaBTC == (int)CustodiaBTC.AlteraClientesSemIR && cliente.ApuraGanhoRV != TipoApuracaoGanhoRV.NaoApura))
                            {
                                PosicaoEmprestimoBolsa pEmprestimo = new PosicaoEmprestimoBolsa();
                                decimal quantidadeDoada = pEmprestimo.RetornaQuantidade(idCliente, cdAtivoBolsa, PontaEmprestimoBolsa.Doador);
                                decimal quantidadeTomada = pEmprestimo.RetornaQuantidade(idCliente, cdAtivoBolsa, PontaEmprestimoBolsa.Tomador);

                                quantidadeFinancial = quantidadeFinancial - quantidadeDoada + quantidadeTomada;
                            }

                            Vcfposicao vcfposicao = new Vcfposicao();
                            vcfposicao.es.Connection.Name = "Sinacor";
                            vcfposicao.es.Connection.Schema = ParametrosConfiguracaoSistema.Integracoes.SchemaSinacor;
                            vcfposicao.Query.Select(vcfposicao.Query.QtdeTot.Sum());
                            vcfposicao.Query.Where((vcfposicao.Query.CodCli == codigoCliente | vcfposicao.Query.CodCli == codigoCliente2) &
                                                     vcfposicao.Query.CodNeg == cdAtivoBolsa);
                            vcfposicao.Query.Load();

                            if (quantidadeFinancial != 0)
                            {
                                if (!vcfposicao.es.HasData || !vcfposicao.QtdeTot.HasValue)
                                {
                                    listaBatimentoBolsa.Add(new BatimentoBolsa(idCliente, apelido, dataDia, cdAtivoBolsa,
                                                                               quantidadeFinancial, 0, quantidadeFinancial));
                                }
                            }
                        }
                        #endregion
                    }

                    // Termos
                    if (tipoMercado == 1 || tipoMercado == -1) {

                        #region Batimento Termos de Ações
                        VcfposicaoCollection vcfposicaoCollection = new VcfposicaoCollection();
                        vcfposicaoCollection.es.Connection.Name = "Sinacor";
                        vcfposicaoCollection.es.Connection.Schema = ParametrosConfiguracaoSistema.Integracoes.SchemaSinacor;

                        vcfposicaoCollection.Query.Select(vcfposicaoCollection.Query.CodNeg,
                                                          vcfposicaoCollection.Query.DataVenc,
                                                          vcfposicaoCollection.Query.QtdeTot.Sum());
                        vcfposicaoCollection.Query.Where(
                                            (vcfposicaoCollection.Query.CodCli == codigoCliente | vcfposicaoCollection.Query.CodCli == codigoCliente2) &
                                                         (vcfposicaoCollection.Query.TipoMerc.Like("TER%")));

                        vcfposicaoCollection.Query.Where(vcfposicaoCollection.Query.TipoGrup == "ACAO");

                        vcfposicaoCollection.Query.GroupBy(vcfposicaoCollection.Query.CodNeg, vcfposicaoCollection.Query.DataVenc);

                        vcfposicaoCollection.Query.OrderBy(vcfposicaoCollection.Query.CodNeg.Ascending);

                        vcfposicaoCollection.Query.Load();

                        foreach (Vcfposicao vcfposicao in vcfposicaoCollection) {
                            string cdAtivoBolsa = vcfposicao.CodNeg;
                            decimal quantidadeSinacor = vcfposicao.QtdeTot.Value;
                            DateTime dataVencimento = vcfposicao.DataVenc.Value;

                            string cdAtivoBolsaTermo = AtivoBolsa.RetornaCdAtivoBolsaConcatenado(cdAtivoBolsa, dataVencimento);

                            PosicaoTermoBolsa posicaoTermoBolsa = new PosicaoTermoBolsa();
                            decimal quantidadeFinancial = posicaoTermoBolsa.RetornaQuantidadeAtivo(idCliente, cdAtivoBolsaTermo);

                            decimal diferenca = quantidadeFinancial - quantidadeSinacor;
                            if (diferenca != 0) {
                                listaBatimentoBolsa.Add(new BatimentoBolsa(idCliente, apelido, dataDia, cdAtivoBolsa + " - " + dataVencimento.ToShortDateString(),
                                                                           quantidadeFinancial, quantidadeSinacor, diferenca));
                            }
                        }

                        PosicaoTermoBolsaCollection posicaoTermoBolsaCollection = new PosicaoTermoBolsaCollection();
                        posicaoTermoBolsaCollection.Query.Select(posicaoTermoBolsaCollection.Query.CdAtivoBolsa,
                                                                 posicaoTermoBolsaCollection.Query.DataVencimento,
                                                                 posicaoTermoBolsaCollection.Query.Quantidade.Sum());

                        posicaoTermoBolsaCollection.Query.Where(posicaoTermoBolsaCollection.Query.IdCliente == idCliente,
                                                                posicaoTermoBolsaCollection.Query.Quantidade != 0);

                        posicaoTermoBolsaCollection.Query.GroupBy(posicaoTermoBolsaCollection.Query.CdAtivoBolsa,
                                                                  posicaoTermoBolsaCollection.Query.DataVencimento);

                        posicaoTermoBolsaCollection.Query.OrderBy(posicaoTermoBolsaCollection.Query.CdAtivoBolsa.Ascending);

                        posicaoTermoBolsaCollection.Query.Load();

                        foreach (PosicaoTermoBolsa posicaoTermoBolsa in posicaoTermoBolsaCollection) {
                            string cdAtivoBolsa = posicaoTermoBolsa.CdAtivoBolsa;
                            decimal quantidadeFinancial = posicaoTermoBolsa.Quantidade.Value;
                            DateTime dataVencimento = posicaoTermoBolsa.DataVencimento.Value;
                            string cdAtivoTermo = AtivoBolsa.RetornaCdAtivoBolsaAcao(cdAtivoBolsa) + "T";

                            Vcfposicao vcfposicao = new Vcfposicao();
                            vcfposicao.es.Connection.Name = "Sinacor";
                            vcfposicao.es.Connection.Schema = ParametrosConfiguracaoSistema.Integracoes.SchemaSinacor;
                            vcfposicao.Query.Select(vcfposicao.Query.QtdeDisp.Sum());
                            vcfposicao.Query.Where((vcfposicao.Query.CodCli == codigoCliente | vcfposicao.Query.CodCli == codigoCliente2) &
                                                    vcfposicao.Query.CodNeg == cdAtivoTermo,
                                                    vcfposicao.Query.DataVenc == dataVencimento);

                            vcfposicao.Query.Load();

                            decimal quantidadeSinacor = 0;
                            decimal diferenca = 0;

                            if (!vcfposicao.es.HasData || !vcfposicao.QtdeDisp.HasValue) {
                                diferenca = quantidadeFinancial - quantidadeSinacor;
                            }

                            if (diferenca != 0) {
                                listaBatimentoBolsa.Add(new BatimentoBolsa(idCliente, apelido, dataDia, cdAtivoTermo + " - " + dataVencimento.ToShortDateString(),
                                                                           quantidadeFinancial, quantidadeSinacor, diferenca));
                            }
                        }
                        #endregion
                    }

                    // BTC
                    if (tipoMercado == 2 || tipoMercado == -1)
                    {
                        #region Batimento BTC (Tomador)
                        VcfposiBtcCollection vcfposiBtcCollection = new VcfposiBtcCollection();
                        vcfposiBtcCollection.es.Connection.Name = "Sinacor";
                        vcfposiBtcCollection.es.Connection.Schema = ParametrosConfiguracaoSistema.Integracoes.SchemaSinacor;

                        vcfposiBtcCollection.Query.Select(vcfposiBtcCollection.Query.CodNeg,
                                                          vcfposiBtcCollection.Query.DataVenc,
                                                          vcfposiBtcCollection.Query.QtdeAcoe.Sum());
                        vcfposiBtcCollection.Query.Where(
                                            (vcfposiBtcCollection.Query.CodCli == codigoCliente | vcfposiBtcCollection.Query.CodCli == codigoCliente2));

                        vcfposiBtcCollection.Query.Where(vcfposiBtcCollection.Query.TipoCotr == "T");

                        vcfposiBtcCollection.Query.GroupBy(vcfposiBtcCollection.Query.CodNeg, vcfposiBtcCollection.Query.DataVenc);

                        vcfposiBtcCollection.Query.OrderBy(vcfposiBtcCollection.Query.CodNeg.Ascending, vcfposiBtcCollection.Query.DataVenc.Ascending);

                        vcfposiBtcCollection.Query.Load();

                        foreach (VcfposiBtc vcfposiBtc in vcfposiBtcCollection)
                        {
                            string cdAtivoBolsa = vcfposiBtc.CodNeg;
                            decimal quantidadeSinacor = vcfposiBtc.QtdeAcoe.Value;
                            DateTime dataVencimento = vcfposiBtc.DataVenc.Value;
                            
                            PosicaoEmprestimoBolsa posicaoEmprestimoBolsa = new PosicaoEmprestimoBolsa();
                            posicaoEmprestimoBolsa.Query.Select(posicaoEmprestimoBolsa.Query.Quantidade.Sum());
                            posicaoEmprestimoBolsa.Query.Where(posicaoEmprestimoBolsa.Query.IdCliente.Equal(idCliente),
                                                               posicaoEmprestimoBolsa.Query.CdAtivoBolsa.Equal(cdAtivoBolsa), 
                                                               posicaoEmprestimoBolsa.Query.PontaEmprestimo.Equal((byte)PontaEmprestimoBolsa.Tomador),
                                                               posicaoEmprestimoBolsa.Query.DataVencimento.Equal(dataVencimento));
                            posicaoEmprestimoBolsa.Query.Load();

                            decimal quantidadeFinancial = posicaoEmprestimoBolsa.Quantidade.HasValue ? posicaoEmprestimoBolsa.Quantidade.Value : 0;

                            cdAtivoBolsa = "Tomado - " + cdAtivoBolsa + " - " + dataVencimento.ToShortDateString();

                            decimal diferenca = quantidadeFinancial - quantidadeSinacor;
                            if (diferenca != 0)
                            {
                                listaBatimentoBolsa.Add(new BatimentoBolsa(idCliente, apelido, dataDia, cdAtivoBolsa,
                                                                           quantidadeFinancial, quantidadeSinacor, diferenca));
                            }
                        }

                        PosicaoEmprestimoBolsaCollection posicaoEmprestimoBolsaCollection = new PosicaoEmprestimoBolsaCollection();
                        posicaoEmprestimoBolsaCollection.Query.Select(posicaoEmprestimoBolsaCollection.Query.CdAtivoBolsa,
                                                                 posicaoEmprestimoBolsaCollection.Query.DataVencimento,
                                                                 posicaoEmprestimoBolsaCollection.Query.Quantidade.Sum());

                        posicaoEmprestimoBolsaCollection.Query.Where(posicaoEmprestimoBolsaCollection.Query.IdCliente == idCliente,
                                                                     posicaoEmprestimoBolsaCollection.Query.Quantidade != 0,
                                                                     posicaoEmprestimoBolsaCollection.Query.PontaEmprestimo.Equal((byte)PontaEmprestimoBolsa.Tomador));

                        posicaoEmprestimoBolsaCollection.Query.GroupBy(posicaoEmprestimoBolsaCollection.Query.CdAtivoBolsa,
                                                                       posicaoEmprestimoBolsaCollection.Query.DataVencimento);

                        posicaoEmprestimoBolsaCollection.Query.OrderBy(posicaoEmprestimoBolsaCollection.Query.CdAtivoBolsa.Ascending,
                                                                       posicaoEmprestimoBolsaCollection.Query.DataVencimento.Ascending);

                        posicaoEmprestimoBolsaCollection.Query.Load();

                        foreach (PosicaoEmprestimoBolsa posicaoEmprestimoBolsa in posicaoEmprestimoBolsaCollection)
                        {
                            string cdAtivoBolsa = posicaoEmprestimoBolsa.CdAtivoBolsa;
                            decimal quantidadeFinancial = posicaoEmprestimoBolsa.Quantidade.Value;
                            DateTime dataVencimento = posicaoEmprestimoBolsa.DataVencimento.Value;                        

                            VcfposiBtc vcfposiBtc = new VcfposiBtc();
                            vcfposiBtc.es.Connection.Name = "Sinacor";
                            vcfposiBtc.es.Connection.Schema = ParametrosConfiguracaoSistema.Integracoes.SchemaSinacor;
                            vcfposiBtc.Query.Select(vcfposiBtc.Query.QtdeAcoe.Sum());
                            vcfposiBtc.Query.Where((vcfposiBtc.Query.CodCli == codigoCliente | vcfposiBtc.Query.CodCli == codigoCliente2) &
                                                    vcfposiBtc.Query.CodNeg == cdAtivoBolsa,
                                                    vcfposiBtc.Query.DataVenc == dataVencimento,
                                                    vcfposiBtc.Query.TipoCotr == "T");

                            vcfposiBtc.Query.Load();

                            cdAtivoBolsa = "Tomado - " + cdAtivoBolsa + " - " + dataVencimento.ToShortDateString();

                            decimal quantidadeSinacor = 0;
                            decimal diferenca = 0;
                            if (!vcfposiBtc.es.HasData || !vcfposiBtc.QtdeAcoe.HasValue)
                            {
                                diferenca = quantidadeFinancial - quantidadeSinacor;
                            }

                            if (diferenca != 0)
                            {
                                listaBatimentoBolsa.Add(new BatimentoBolsa(idCliente, apelido, dataDia, cdAtivoBolsa,
                                                                           quantidadeFinancial, quantidadeSinacor, diferenca));
                            }
                        }
                        #endregion

                        #region Batimento BTC (Doador)
                        vcfposiBtcCollection = new VcfposiBtcCollection();
                        vcfposiBtcCollection.es.Connection.Name = "Sinacor";
                        vcfposiBtcCollection.es.Connection.Schema = ParametrosConfiguracaoSistema.Integracoes.SchemaSinacor;

                        vcfposiBtcCollection.Query.Select(vcfposiBtcCollection.Query.CodNeg,
                                                          vcfposiBtcCollection.Query.DataVenc,
                                                          vcfposiBtcCollection.Query.QtdeAcoe.Sum());
                        vcfposiBtcCollection.Query.Where(
                                            (vcfposiBtcCollection.Query.CodCli == codigoCliente | vcfposiBtcCollection.Query.CodCli == codigoCliente2));

                        vcfposiBtcCollection.Query.Where(vcfposiBtcCollection.Query.TipoCotr == "D");

                        vcfposiBtcCollection.Query.GroupBy(vcfposiBtcCollection.Query.CodNeg, vcfposiBtcCollection.Query.DataVenc);

                        vcfposiBtcCollection.Query.OrderBy(vcfposiBtcCollection.Query.CodNeg.Ascending, vcfposiBtcCollection.Query.DataVenc.Ascending);

                        vcfposiBtcCollection.Query.Load();

                        foreach (VcfposiBtc vcfposiBtc in vcfposiBtcCollection)
                        {
                            string cdAtivoBolsa = vcfposiBtc.CodNeg;
                            decimal quantidadeSinacor = vcfposiBtc.QtdeAcoe.Value;
                            DateTime dataVencimento = vcfposiBtc.DataVenc.Value;

                            PosicaoEmprestimoBolsa posicaoEmprestimoBolsa = new PosicaoEmprestimoBolsa();
                            posicaoEmprestimoBolsa.Query.Select(posicaoEmprestimoBolsa.Query.Quantidade.Sum());
                            posicaoEmprestimoBolsa.Query.Where(posicaoEmprestimoBolsa.Query.IdCliente.Equal(idCliente),
                                                               posicaoEmprestimoBolsa.Query.CdAtivoBolsa.Equal(cdAtivoBolsa),
                                                               posicaoEmprestimoBolsa.Query.PontaEmprestimo.Equal((byte)PontaEmprestimoBolsa.Doador),
                                                               posicaoEmprestimoBolsa.Query.DataVencimento.Equal(dataVencimento));
                            posicaoEmprestimoBolsa.Query.Load();

                            decimal quantidadeFinancial = posicaoEmprestimoBolsa.Quantidade.HasValue ? posicaoEmprestimoBolsa.Quantidade.Value : 0;

                            cdAtivoBolsa = "Doado - " + cdAtivoBolsa + " - " + dataVencimento.ToShortDateString();

                            decimal diferenca = quantidadeFinancial - quantidadeSinacor;
                            if (diferenca != 0)
                            {
                                listaBatimentoBolsa.Add(new BatimentoBolsa(idCliente, apelido, dataDia, cdAtivoBolsa,
                                                                           quantidadeFinancial, quantidadeSinacor, diferenca));
                            }
                        }

                        posicaoEmprestimoBolsaCollection = new PosicaoEmprestimoBolsaCollection();
                        posicaoEmprestimoBolsaCollection.Query.Select(posicaoEmprestimoBolsaCollection.Query.CdAtivoBolsa,
                                                                 posicaoEmprestimoBolsaCollection.Query.DataVencimento,
                                                                 posicaoEmprestimoBolsaCollection.Query.Quantidade.Sum());

                        posicaoEmprestimoBolsaCollection.Query.Where(posicaoEmprestimoBolsaCollection.Query.IdCliente == idCliente,
                                                                     posicaoEmprestimoBolsaCollection.Query.Quantidade != 0,
                                                                     posicaoEmprestimoBolsaCollection.Query.PontaEmprestimo.Equal((byte)PontaEmprestimoBolsa.Doador));

                        posicaoEmprestimoBolsaCollection.Query.GroupBy(posicaoEmprestimoBolsaCollection.Query.CdAtivoBolsa,
                                                                       posicaoEmprestimoBolsaCollection.Query.DataVencimento);

                        posicaoEmprestimoBolsaCollection.Query.OrderBy(posicaoEmprestimoBolsaCollection.Query.CdAtivoBolsa.Ascending,
                                                                       posicaoEmprestimoBolsaCollection.Query.DataVencimento.Ascending);

                        posicaoEmprestimoBolsaCollection.Query.Load();

                        foreach (PosicaoEmprestimoBolsa posicaoEmprestimoBolsa in posicaoEmprestimoBolsaCollection)
                        {
                            string cdAtivoBolsa = posicaoEmprestimoBolsa.CdAtivoBolsa;
                            decimal quantidadeFinancial = posicaoEmprestimoBolsa.Quantidade.Value;
                            DateTime dataVencimento = posicaoEmprestimoBolsa.DataVencimento.Value;

                            VcfposiBtc vcfposiBtc = new VcfposiBtc();
                            vcfposiBtc.es.Connection.Name = "Sinacor";
                            vcfposiBtc.es.Connection.Schema = ParametrosConfiguracaoSistema.Integracoes.SchemaSinacor;
                            vcfposiBtc.Query.Select(vcfposiBtc.Query.QtdeAcoe.Sum());
                            vcfposiBtc.Query.Where((vcfposiBtc.Query.CodCli == codigoCliente | vcfposiBtc.Query.CodCli == codigoCliente2) &
                                                    vcfposiBtc.Query.CodNeg == cdAtivoBolsa,
                                                    vcfposiBtc.Query.DataVenc == dataVencimento,
                                                    vcfposiBtc.Query.TipoCotr == "D");

                            vcfposiBtc.Query.Load();

                            cdAtivoBolsa = "Doado - " + cdAtivoBolsa + " - " + dataVencimento.ToShortDateString();

                            decimal quantidadeSinacor = 0;
                            decimal diferenca = 0;
                            if (!vcfposiBtc.es.HasData || !vcfposiBtc.QtdeAcoe.HasValue)
                            {
                                diferenca = quantidadeFinancial - quantidadeSinacor;
                            }

                            if (diferenca != 0)
                            {
                                listaBatimentoBolsa.Add(new BatimentoBolsa(idCliente, apelido, dataDia, cdAtivoBolsa,
                                                                           quantidadeFinancial, quantidadeSinacor, diferenca));
                            }
                        }
                        #endregion
                    }

                    //Futuros BMF
                    if (tipoMercado == 3 || tipoMercado == -1)
                    {
                        #region Busca códigos do cliente (codigoCliente, codigoCliente2 na Bovespa)
                        int codigoClienteBMF = 0;
                        
                        ClienteBMF clienteBMF = new ClienteBMF();

                        //Se não achar pelo menos o código do cliente associado, sai da função
                        if (!clienteBMF.LoadByPrimaryKey(idCliente))
                        {
                            this.errorIdCliente = idCliente;
                            this.dataErrorCliente = cliente.DataDia.Value;
                            throw new CodigoClienteBMFInvalido("Cód. BMF Inválido/Inexistente p/ Cliente ");
                        }

                        //Se não tiver código associado, lança exception
                        if (String.IsNullOrEmpty(clienteBMF.CodigoSinacor))
                        {
                            this.errorIdCliente = idCliente;
                            this.dataErrorCliente = cliente.DataDia.Value;
                            throw new CodigoClienteBMFInvalido("Cód. BMF Inválido/Inexistente p/ IdCliente ");
                        }

                        //Se nenhum código é número, lança exception
                        if (!Utilitario.IsInteger(clienteBMF.CodigoSinacor))
                        {
                            this.errorIdCliente = idCliente;
                            this.dataErrorCliente = cliente.DataDia.Value;
                            throw new CodigoClienteBMFInvalido("Cód. BMF Inválido/Inexistente p/ o IdCliente ");
                        }

                        if (!String.IsNullOrEmpty(clienteBMF.CodigoSinacor))
                        {
                            codigoClienteBMF = Convert.ToInt32(clienteBMF.CodigoSinacor);
                        }
                        #endregion

                        if (codigoClienteBMF != 0)
                        {
                            #region Batimento Futuros BMF
                            TmfposicCollection tmfposicCollection = new TmfposicCollection();
                            tmfposicCollection.es.Connection.Name = "Sinacor";
                            tmfposicCollection.es.Connection.Schema = ParametrosConfiguracaoSistema.Integracoes.SchemaSinacor;
                            tmfposicCollection.Query.Select(tmfposicCollection.Query.CdCommod,
                                                            tmfposicCollection.Query.CdSerie,
                                                            tmfposicCollection.Query.QtDiaAtu.Sum());
                            tmfposicCollection.Query.Where(tmfposicCollection.Query.CdCliente.Equal(codigoClienteBMF),
                                                         tmfposicCollection.Query.CdMercad.Like("FUT%"),
                                                         tmfposicCollection.Query.DtDatmov.Equal(dataDia));
                            tmfposicCollection.Query.GroupBy(tmfposicCollection.Query.CdCommod,
                                                             tmfposicCollection.Query.CdSerie);
                            tmfposicCollection.Query.OrderBy(tmfposicCollection.Query.CdCommod.Ascending,
                                                             tmfposicCollection.Query.CdSerie.Ascending);
                            tmfposicCollection.Query.Load();

                            foreach (Tmfposic tmfposic in tmfposicCollection)
                            {
                                string cdAtivoBMF = tmfposic.CdCommod.Trim().ToUpper();
                                string serie = tmfposic.CdSerie.Trim().ToUpper();
                                decimal quantidadeSinacor = tmfposic.QtDiaAtu.Value;

                                PosicaoBMF posicaoBMF = new PosicaoBMF();
                                decimal quantidadeFinancial = posicaoBMF.RetornaQuantidadeAtivo(idCliente, cdAtivoBMF, serie);

                                decimal diferenca = quantidadeFinancial - quantidadeSinacor;
                                if (diferenca != 0)
                                {
                                    listaBatimentoBolsa.Add(new BatimentoBolsa(idCliente, apelido, dataDia, cdAtivoBMF + serie,
                                                                               quantidadeFinancial, quantidadeSinacor, diferenca));
                                }
                            }

                            PosicaoBMFCollection posicaoBMFCollection = new PosicaoBMFCollection();
                            posicaoBMFCollection.Query.Select(posicaoBMFCollection.Query.CdAtivoBMF,
                                                              posicaoBMFCollection.Query.Serie,
                                                              posicaoBMFCollection.Query.Quantidade.Sum());
                            posicaoBMFCollection.Query.Where(posicaoBMFCollection.Query.IdCliente.Equal(idCliente),
                                                             posicaoBMFCollection.Query.TipoMercado.Equal((byte)TipoMercadoBMF.Futuro));
                            posicaoBMFCollection.Query.GroupBy(posicaoBMFCollection.Query.CdAtivoBMF, posicaoBMFCollection.Query.Serie);
                            posicaoBMFCollection.Query.OrderBy(posicaoBMFCollection.Query.CdAtivoBMF.Ascending, posicaoBMFCollection.Query.Serie.Ascending);
                            posicaoBMFCollection.Query.Load();

                            foreach (PosicaoBMF posicaoBMF in posicaoBMFCollection)
                            {
                                string cdAtivoBMF = posicaoBMF.CdAtivoBMF.ToUpper();
                                string serie = posicaoBMF.Serie.ToUpper();
                                decimal quantidadeFinancial = posicaoBMF.Quantidade.Value;

                                Tmfposic tmfposic = new Tmfposic();
                                tmfposic.es.Connection.Name = "Sinacor";
                                tmfposic.es.Connection.Schema = ParametrosConfiguracaoSistema.Integracoes.SchemaSinacor;
                                tmfposic.Query.Select(tmfposic.Query.QtDiaAtu.Sum());
                                tmfposic.Query.Where(tmfposic.Query.CdCliente.Equal(codigoClienteBMF),
                                                      tmfposic.Query.CdCommod.Equal(cdAtivoBMF),
                                                      tmfposic.Query.CdSerie.Equal(serie),
                                                      tmfposic.Query.DtDatmov.Equal(dataDia));
                                tmfposic.Query.Load();

                                decimal diferenca = 0;
                                if (!tmfposic.es.HasData || !tmfposic.QtDiaAtu.HasValue)
                                {
                                    diferenca = quantidadeFinancial;
                                }
                                if (diferenca != 0)
                                {
                                    listaBatimentoBolsa.Add(new BatimentoBolsa(idCliente, apelido, dataDia, cdAtivoBMF + serie,
                                                                               quantidadeFinancial, 0, diferenca));
                                }
                            }
                            #endregion
                        }
                    }
                    
                }
                #endregion

                }
                catch (Exception e) {
                    if (!this.errorIdCliente.HasValue) {
                        this.errorIdCliente = -1;
                    }

                    listaBatimentoBolsa.Add(new BatimentoBolsa(this.errorIdCliente.Value, e.Message, this.dataErrorCliente, "", null, null, null));                
                }
                
                return listaBatimentoBolsa;
            }
    }     
}