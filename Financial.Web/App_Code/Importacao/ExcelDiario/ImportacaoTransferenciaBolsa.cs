﻿using System;
using System.Collections.Generic;
using System.IO;
using Financial.Integracao.Excel;
using Financial.Integracao.Excel.Util;
using Financial.RendaFixa;
using Financial.Web.Common;
using EntitySpaces.Interfaces;
using DevExpress.Web;
using Financial.Security;
using System.Web;
using Financial.Web.Util;
using Financial.Security.Enums;
using Financial.Util;
using Financial.Common.Enums;
using Financial.Fundo;
using Financial.Investidor;
using Financial.InvestidorCotista;
using Financial.BMF;
using Financial.Bolsa;

public partial class ImportacaoBasePage : BasePage
{
    /* Estrutura do Excel */
    private List<ValoresExcelTransferenciaBolsa> valoresExcelTransferenciaBolsa = new List<ValoresExcelTransferenciaBolsa>();

    /// <summary>
    /// Leitura de um arquivo Excel
    /// Saida: List<ValoresExcel> com os valores armazenados no arquivo
    /// </summary>
    /// <param name="streamExcel">Stream de bytes de um arquivo Excel</param>
    /// <exception cref="Exception">Se ocorreu problemas</exception>
    private void LerArquivoTransferenciaBolsa(Stream streamExcel)
    {

        #region Leitura do Arquivo Excel

        // Open Spreadsheet
        Bytescout.Spreadsheet.Spreadsheet document = new Bytescout.Spreadsheet.Spreadsheet();
        document.LoadFromStream(streamExcel);

        Bytescout.Spreadsheet.Worksheet workSheet = document.Workbook.Worksheets[0];

        //
        #region Confere Formato Arquivo
        string[] colunasConferencias = new string[] { "IdCliente", "CdAtivoBolsa", "IdAgenteOrigem", "IdAgenteDestino", "Data", "Quantidade", "Pu" };

        bool formato = ValoresExcelUtil.isExcelFormatoValido(colunasConferencias, workSheet);

        if (!formato)
        {
            string mensagem = "Formato Interno do Arquivo Inválido.\n";
            mensagem += "Linha 1 do Arquivo deve ser: \n";
            for (int i = 0; i < colunasConferencias.Length; i++)
            {
                int coluna = i + 1;
                mensagem += "\tColuna " + coluna + ": " + colunasConferencias[i] + "\n";
            }

            document.Close();
            document.Dispose();

            throw new Exception(mensagem);
        }
        #endregion

        //
        int index = 0;
        // This row,column index should be changed as per your need.
        // i.e. which cell in the excel you are interesting to read.
        //
        /* Formato: -0)IdCliente -1)CdAtivoBolsa -2)IdAgenteOrigem -3)IdAgenteDestino -4)Data -5)Quantidade -6)Pu */

        int linha = 1;
        int coluna0 = 0, coluna1 = 1, coluna2 = 2, coluna3 = 3, coluna4 = 4, coluna5 = 5, coluna6 = 6;

        //            
        try
        {
            // Enquanto TipoRegistro tiver valor
            while (workSheet.Cell(linha, coluna0).Value != null)
            {
                ValoresExcelTransferenciaBolsa item = new ValoresExcelTransferenciaBolsa();

                #region IdCliente
                if (workSheet.Cell(linha, coluna0).Value == null)
                    throw new Exception("IdCliente não informada: linha " + (linha + 1));

                try
                {
                    item.IdCliente = workSheet.Cell(linha, coluna0).ValueAsInteger;
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message + " - IdCliente: linha " + (linha + 1));
                }
                #endregion

                #region CdAtivoBolsa
                if (workSheet.Cell(linha, coluna1).Value == null)
                    throw new Exception("CdAtivoBolsa não informada: linha " + (linha + 1));

                try
                {
                    item.CdAtivoBolsa = workSheet.Cell(linha, coluna1).ValueAsString;
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message + " - CdAtivoBolsa: linha " + (linha + 1));
                }
                #endregion

                #region IdAgenteOrigem
                if (workSheet.Cell(linha, coluna2).Value == null)
                    throw new Exception("IdAgenteOrigem não informada: linha " + (linha + 1));

                try
                {
                    item.IdAgenteOrigem = workSheet.Cell(linha, coluna2).ValueAsInteger;
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message + " - IdAgenteOrigem: linha " + (linha + 1));
                }
                #endregion

                #region IdAgenteDestino
                if (workSheet.Cell(linha, coluna3).Value == null)
                    throw new Exception("IdAgenteDestino não informada: linha " + (linha + 1));

                try
                {
                    item.IdAgenteDestino = workSheet.Cell(linha, coluna3).ValueAsInteger;
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message + " - IdAgenteDestino: linha " + (linha + 1));
                }
                #endregion

                #region Data
                if (workSheet.Cell(linha, coluna4).Value == null)
                    throw new Exception("Data não informada: linha " + (linha + 1));

                try
                {
                    item.Data = workSheet.Cell(linha, coluna4).ValueAsDateTime;
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message + " - Data: linha " + (linha + 1));
                }
                #endregion

                #region Quantidade
                if (workSheet.Cell(linha, coluna5).Value == null)
                    throw new Exception("Quantidade não informada: linha " + (linha + 1));
                try
                {
                    item.Quantidade = Convert.ToDecimal(workSheet.Cell(linha, coluna5).Value);
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message + " - Quantidade: linha " + (linha + 1));
                }
                #endregion

                #region Pu
                if (workSheet.Cell(linha, coluna6).Value == null)
                    throw new Exception("Pu não informada: linha " + (linha + 1));
                try
                {
                    item.Pu = Convert.ToDecimal(workSheet.Cell(linha, coluna6).Value);
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message + " - Pu: linha " + (linha + 1));
                }
                #endregion

                this.valoresExcelTransferenciaBolsa.Add(item);

                index++;
                linha = 1 + index;


                Console.WriteLine("{0},{1},{2},{3},{4},{5},{6}", item.IdCliente, item.CdAtivoBolsa, item.IdAgenteOrigem, item.IdAgenteDestino, item.Data, item.Quantidade, item.Pu);
            }
        }
        catch (Exception ex)
        {
            document.Close();
            document.Dispose();

            throw new Exception(ex.Message);
        }
        #endregion

        document.Close();
        document.Dispose();
    }

    /// <summary>
    /// Gera e Carrega as Operações de ComeCotas co de acordo com o objeto List<ValoresExcel>
    /// </summary>     
    private void CarregaTransferenciaBolsa()
    {
        TransferenciaBolsaCollection transferenciaBolsaCollection = new TransferenciaBolsaCollection();
        //    
        for (int i = 0; i < this.valoresExcelTransferenciaBolsa.Count; i++)
        {
            ValoresExcelTransferenciaBolsa valoresExcel = valoresExcelTransferenciaBolsa[i];
            #region Verifica se cliente
            Cliente cliente = new Cliente();
            if (!cliente.LoadByPrimaryKey(valoresExcel.IdCliente))
            {
                throw new Exception("Cliente não existente: " + valoresExcel.IdCliente);
            }            
            #endregion

            #region Ativo Bolsa
            AtivoBolsa ativo = new AtivoBolsa();
            if (!ativo.LoadByPrimaryKey(valoresExcel.CdAtivoBolsa))
            {
                throw new Exception("Ativo não existente: " + valoresExcel.CdAtivoBolsa);
            }
            #endregion

            #region TransferenciaBolsa
            // Dados do Arquivo            
            TransferenciaBolsa transferenciaBolsa = transferenciaBolsaCollection.AddNew();
            transferenciaBolsa.IdCliente = valoresExcel.IdCliente;
            transferenciaBolsa.CdAtivoBolsa = valoresExcel.CdAtivoBolsa;
            transferenciaBolsa.IdAgenteOrigem = valoresExcel.IdAgenteOrigem;
            transferenciaBolsa.IdAgenteDestino = valoresExcel.IdAgenteDestino;
            transferenciaBolsa.Data = valoresExcel.Data;
            transferenciaBolsa.Quantidade = valoresExcel.Quantidade;
            transferenciaBolsa.Pu = valoresExcel.Pu;           
            //
            #endregion
        }

        transferenciaBolsaCollection.Save();
    }

    /// <summary>
    /// Processa a Planilha de CotacaoIndice após ter acabado o Upload
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void uplTransferenciaBolsa_FileUploadComplete(object sender, FileUploadCompleteEventArgs e)
    {
        e.CallbackData = "";

        #region Trata Extensão Válida
        if (!ValoresExcelUtil.isExtensaoExcel(e.UploadedFile.FileName.Trim()))
        {
            e.CallbackData = "Importação Transferência - BMF - Formato de Arquivo Inválido. Formato permitido: .xls ou .xlsx\n\n";
            return;
        }
        #endregion

        // Stream de bytes com o Conteudo do Arquivo Excel
        Stream sr = e.UploadedFile.FileContent;

        #region Processamento Arquivo Excel

        //Reseta Vetor de Dados
        this.valoresExcelTransferenciaBolsa = new List<ValoresExcelTransferenciaBolsa>();
        //
        try
        {
            // Ler Arquivo
            this.LerArquivoTransferenciaBolsa(sr);
            // Carrega Arquivo
            this.CarregaTransferenciaBolsa();
        }
        catch (Exception e2)
        {
            e.CallbackData = "Importação Transferência - BMF - " + e2.Message;
            return;
        }

        new HistoricoLog().InsereHistoricoLog(DateTime.Now, DateTime.Now,
            "Importação Planilha Transferência - BMF",
            HttpContext.Current.User.Identity.Name, UtilitarioWeb.GetIP(Request), "", HistoricoLogOrigem.Outros);

        #endregion
    }
}
