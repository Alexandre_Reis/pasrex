﻿using System;
using System.Collections.Generic;
using System.IO;
using Financial.Integracao.Excel;
using Financial.Integracao.Excel.Util;
using Financial.RendaFixa;
using Financial.Web.Common;
using EntitySpaces.Interfaces;
using DevExpress.Web;
using Financial.Security;
using System.Web;
using Financial.Web.Util;
using Financial.Security.Enums;
using Financial.Util;
using Financial.Common.Enums;
using Financial.Fundo;
using Financial.Investidor;
using Financial.InvestidorCotista;

public partial class ImportacaoBasePage : BasePage
{
    /* Estrutura do Excel */
    private List<ValoresExcelColagemResgate> valoresExcelColagemResgate = new List<ValoresExcelColagemResgate>();

    private List<DateTime> dataReferenciaDeletarColagemResgateDetalhe = new List<DateTime>();
    private List<int> idClienteDeletarColagemResgateDetalhe = new List<int>();
    private List<int> idCarteiraDeletarColagemResgateDetalhe = new List<int>();
    /// <summary>
    /// Leitura de um arquivo Excel
    /// Saida: List<ValoresExcel> com os valores armazenados no arquivo
    /// </summary>
    /// <param name="streamExcel">Stream de bytes de um arquivo Excel</param>
    /// <exception cref="Exception">Se ocorreu problemas</exception>
    private void LerArquivoColagemResgates(Stream streamExcel)
    {

        #region Leitura do Arquivo Excel

        // Open Spreadsheet
        Bytescout.Spreadsheet.Spreadsheet document = new Bytescout.Spreadsheet.Spreadsheet();
        document.LoadFromStream(streamExcel);

        Bytescout.Spreadsheet.Worksheet workSheet = document.Workbook.Worksheets[0];

        //
        #region Confere Formato Arquivo
        string[] colunasConferencias = new string[] { "TipoRegistro", "CodigoExterno", "DataReferencia", "IdCliente", "IdCarteira", "DataConversaoCautela", "Quantidade", "CotaOperacao", "ValorBruto", "ValorLiquido", "ValorCPMF", "ValorPerformance", "PrejuizoUsado", "RendimentoResgate", "VariacaoResgate", "ValorIR", "ValorIOF" };

        bool formato = ValoresExcelUtil.isExcelFormatoValido(colunasConferencias, workSheet);

        if (!formato)
        {
            string mensagem = "Formato Interno do Arquivo Inválido.\n";
            mensagem += "Linha 1 do Arquivo deve ser: \n";
            for (int i = 0; i < colunasConferencias.Length; i++)
            {
                int coluna = i + 1;
                mensagem += "\tColuna " + coluna + ": " + colunasConferencias[i] + "\n";
            }

            document.Close();
            document.Dispose();

            throw new Exception(mensagem);
        }
        #endregion

        //
        int index = 0;
        // This row,column index should be changed as per your need.
        // i.e. which cell in the excel you are interesting to read.
        //
        /* Formato: -0)TipoRegistro     -1)CodigoExterno        -2)DataReferencia 
         *          -3)IdCliente        -4)IdCarteira           -5)DataConversaoCautela 
         *          -6)Quantidade       -7)CotaOperacao         -8)ValorBruto           
         *          -9)ValorLiquido     -10)ValorCPMF           -11)ValorPerformance    
         *          -12)PrejuizoUsado   -13)RendimentoResgate   -14)VariacaoResgate     
         *          -15)ValorIR         -16)ValorIOF  */

        int linha = 1;
        int coluna0 = 0,   coluna1 = 1,   coluna2 = 2, 
            coluna3 = 3,   coluna4 = 4,   coluna5 = 5, 
            coluna6 = 6,   coluna7 = 7,   coluna8 = 8, 
            coluna9 = 9,   coluna10 = 10, coluna11 = 11, 
            coluna12 = 12, coluna13 = 13, coluna14 = 14, 
            coluna15 = 15, coluna16 = 16;

        //            
        try
        {
            // Enquanto TipoRegistro tiver valor
            while (workSheet.Cell(linha, coluna0).Value != null)
            {
                ValoresExcelColagemResgate item = new ValoresExcelColagemResgate();

                #region TipoRegistro
                if (workSheet.Cell(linha, coluna0).Value == null)
                    throw new Exception("TipoRegistro não informada: linha " + (linha + 1));

                try
                {
                    item.TipoRegistro = workSheet.Cell(linha, coluna0).ValueAsString;
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message + " - TipoRegistro: linha " + (linha + 1));
                }
                #endregion

                #region CodigoExterno
                if (workSheet.Cell(linha, coluna1).Value == null)
                    throw new Exception("CodigoExterno não informada: linha " + (linha + 1));

                try
                {
                    item.CodigoExterno = workSheet.Cell(linha, coluna1).ValueAsString;
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message + " - CodigoExterno: linha " + (linha + 1));
                }
                #endregion

                #region DataReferencia
                if (workSheet.Cell(linha, coluna2).Value == null)
                    throw new Exception("DataReferencia não informada: linha " + (linha + 1));

                try
                {
                    item.DataReferencia = workSheet.Cell(linha, coluna2).ValueAsDateTime;
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message + " - DataReferencia: linha " + (linha + 1));
                }
                #endregion

                #region IdCliente
                if (workSheet.Cell(linha, coluna3).Value == null)
                    throw new Exception("IdCliente não informado: linha " + (linha + 1));

                try
                {
                    item.IdCliente = workSheet.Cell(linha, coluna3).ValueAsInteger;
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message + " - IdCliente: linha " + (linha + 1));
                }
                #endregion

                #region IdCarteira
                if (workSheet.Cell(linha, coluna4).Value == null)
                    throw new Exception("IdCarteira não informado: linha " + (linha + 1));

                try
                {
                    item.IdCarteira = workSheet.Cell(linha, coluna4).ValueAsInteger;
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message + " - IdCarteira: linha " + (linha + 1));
                }
                #endregion

                #region DataConversao
                if (workSheet.Cell(linha, coluna5).Value == null)
                {
                    item.DataConversao = new DateTime();
                }
                else
                {
                    try
                    {
                        item.DataConversao = workSheet.Cell(linha, coluna5).ValueAsDateTime;
                    }
                    catch (Exception ex)
                    {
                        throw new Exception(ex.Message + " - DataConversaoCautela: linha " + (linha + 1));
                    }
                }
                #endregion

                #region Quantidade
                if (workSheet.Cell(linha, coluna6).Value == null)
                    throw new Exception("Quantidade não informada: linha " + (linha + 1));
                try
                {
                    item.Quantidade = Convert.ToDecimal(workSheet.Cell(linha, coluna6).Value);
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message + " - Quantidade: linha " + (linha + 1));
                }
                #endregion

                #region CotaOperacao
                if (workSheet.Cell(linha, coluna7).Value == null)
                {
                    item.CotaOperacao = 0;
                }
                else
                {
                    try
                    {
                        item.CotaOperacao = Convert.ToDecimal(workSheet.Cell(linha, coluna7).Value);
                    }
                    catch (Exception ex)
                    {
                        throw new Exception(ex.Message + " - CotaOperacao: linha " + (linha + 1));
                    }
                }
                #endregion

                #region ValorBruto
                if (workSheet.Cell(linha, coluna8).Value == null)                
                    throw new Exception("ValorBruto não informada: linha " + (linha + 1));
                try
                {
                    item.ValorBruto = Convert.ToDecimal(workSheet.Cell(linha, coluna8).Value);
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message + " - ValorBruto: linha " + (linha + 1));
                }                
                #endregion

                #region ValorLiquido
                if (workSheet.Cell(linha, coluna9).Value == null)
                    throw new Exception("ValorLiquido não informada: linha " + (linha + 1));
                try
                {
                    item.ValorLiquido = Convert.ToDecimal(workSheet.Cell(linha, coluna9).Value);
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message + " - ValorLiquido: linha " + (linha + 1));
                }
                #endregion

                #region ValorCPMF
                if (workSheet.Cell(linha, coluna10).Value == null)
                    item.ValorCPMF = 0;
                else
                {
                    try
                    {
                        item.ValorCPMF = Convert.ToDecimal(workSheet.Cell(linha, coluna10).Value);
                    }
                    catch (Exception ex)
                    {
                        throw new Exception(ex.Message + " - ValorCPMF: linha " + (linha + 1));
                    }
                }
                #endregion

                #region ValorPerformance
                if (workSheet.Cell(linha, coluna11).Value == null)
                    item.ValorPerformance = 0;
                else
                {
                    try
                    {
                        item.ValorPerformance = Convert.ToDecimal(workSheet.Cell(linha, coluna11).Value);
                    }
                    catch (Exception ex)
                    {
                        throw new Exception(ex.Message + " - ValorPerformance: linha " + (linha + 1));
                    }
                }
                #endregion

                #region PrejuizoUsado
                if (workSheet.Cell(linha, coluna12).Value == null)
                    item.PrejuizoUsado = 0;
                else
                {
                    try
                    {
                        item.PrejuizoUsado = Convert.ToDecimal(workSheet.Cell(linha, coluna12).Value);
                    }
                    catch (Exception ex)
                    {
                        throw new Exception(ex.Message + " - PrejuizoUsado: linha " + (linha + 1));
                    }
                }
                #endregion

                #region RendimentoResgate
                if (workSheet.Cell(linha, coluna13).Value == null)
                    item.RendimentoResgate = 0;
                else
                {
                    try
                    {
                        item.RendimentoResgate = Convert.ToDecimal(workSheet.Cell(linha, coluna13).Value);
                    }
                    catch (Exception ex)
                    {
                        throw new Exception(ex.Message + " - RendimentoResgate: linha " + (linha + 1));
                    }
                }
                #endregion

                #region VariacaoResgate
                if (workSheet.Cell(linha, coluna14).Value == null)
                    item.RendimentoResgate = 0;
                else
                {
                    try
                    {
                        item.RendimentoResgate = Convert.ToDecimal(workSheet.Cell(linha, coluna14).Value);
                    }
                    catch (Exception ex)
                    {
                        throw new Exception(ex.Message + " - VariacaoResgate: linha " + (linha + 1));
                    }
                }
                #endregion

                #region ValorIR
                if (workSheet.Cell(linha, coluna15).Value == null)
                    throw new Exception("ValorIR não informada: linha " + (linha + 1));
                try
                {
                    item.ValorIR = Convert.ToDecimal(workSheet.Cell(linha, coluna15).Value);
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message + " - ValorIR: linha " + (linha + 1));
                }
                #endregion

                #region ValorIOF
                if (workSheet.Cell(linha, coluna16).Value == null)
                    throw new Exception("ValorIOF não informada: linha " + (linha + 1));
                try
                {
                    item.ValorIOF = Convert.ToDecimal(workSheet.Cell(linha, coluna16).Value);
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message + " - ValorIOF: linha " + (linha + 1));
                }
                #endregion

                this.valoresExcelColagemResgate.Add(item);

                index++;
                linha = 1 + index;


                Console.WriteLine("{0},{1},{2},{3},{4},{5},{6},{7},{8},{9},{10},{11},{12},{13},{14},{15},{16}", item.TipoRegistro, item.CodigoExterno, item.DataReferencia, item.IdCliente, item.IdCarteira, item.DataConversao, item.Quantidade, item.CotaOperacao, item.ValorBruto, item.ValorLiquido, item.ValorCPMF, item.ValorPerformance, item.PrejuizoUsado, item.RendimentoResgate, item.VariacaoResgate, item.ValorIR, item.ValorIOF);
            }
        }
        catch (Exception ex)
        {
            document.Close();
            document.Dispose();

            throw new Exception(ex.Message);
        }
        #endregion

        document.Close();
        document.Dispose();
    }

    /// <summary>
    /// Gera e Carrega as Operações de Resgate co de acordo com o objeto List<ValoresExcel>
    /// </summary>     
    private void CarregaColagemResgate()
    {
        ColagemResgateCollection colagemResgateCollection = new ColagemResgateCollection();
        ColagemResgateCollection colagemResgateDeletarCollection = new ColagemResgateCollection();
        ColagemResgateDetalheCollection colagemResgateDetalheCollection = new ColagemResgateDetalheCollection();
        ColagemResgateDetalheCollection colagemResgateDetalheDeletarCollection = new ColagemResgateDetalheCollection();
        //    

        List<string> lstChave = new List<string>();
        for (int i = 0; i < this.valoresExcelColagemResgate.Count; i++)
        {
            ValoresExcelColagemResgate valoresExcel = this.valoresExcelColagemResgate[i];

            //    
            string chave = valoresExcel.IdCarteira.ToString() + "|" + valoresExcel.IdCliente.ToString() + "|" + valoresExcel.DataReferencia.ToString("dd/MM/yyyy");

            // Chave para poder Deletar 
            if (!lstChave.Contains(chave))
            {
                this.idCarteiraDeletarColagemResgateDetalhe.Add(valoresExcel.IdCarteira);
                this.idClienteDeletarColagemResgateDetalhe.Add(valoresExcel.IdCliente);
                this.dataReferenciaDeletarColagemResgateDetalhe.Add(valoresExcel.DataReferencia);
                lstChave.Add(chave);
            }

            #region Verifica se Existe carteira
            Carteira carteira = new Carteira();
            if (!carteira.LoadByPrimaryKey(valoresExcel.IdCarteira))
            {
                throw new Exception("Carteira não existente: " + valoresExcel.IdCarteira);
            }
            #endregion

            #region Verifica se Existe cliente/cotista
            if (valoresExcel.TipoRegistro.Equals("A"))
            {
                Cliente cliente = new Cliente();
                if (!cliente.LoadByPrimaryKey(valoresExcel.IdCliente))
                {
                    throw new Exception("Cliente não existente: " + valoresExcel.IdCliente);
                }
            }
            else if (valoresExcel.TipoRegistro.Equals("P"))
            {
                Cotista cotista = new Cotista();
                if (!cotista.LoadByPrimaryKey(valoresExcel.IdCliente))
                {
                    throw new Exception("Cotista não existente: " + valoresExcel.IdCliente);
                }
            }
            #endregion

            #region ColagemResgate
            
            // Dados do Arquivo
            if (valoresExcel.TipoRegistro.Equals("D"))
            {
                ColagemResgateDetalhe colagemResgateDetalhe = colagemResgateDetalheCollection.AddNew();
                colagemResgateDetalhe.CodigoExterno = valoresExcel.CodigoExterno.Trim();
                colagemResgateDetalhe.CotaOperacao = valoresExcel.CotaOperacao;
                if (valoresExcel.DataConversao != new DateTime())
                    colagemResgateDetalhe.DataConversao = valoresExcel.DataConversao;
                else
                    colagemResgateDetalhe.DataConversao = valoresExcel.DataReferencia;
                colagemResgateDetalhe.DataReferencia = valoresExcel.DataReferencia;
                colagemResgateDetalhe.IdCarteira = valoresExcel.IdCarteira;
                colagemResgateDetalhe.IdCliente = valoresExcel.IdCliente;
                colagemResgateDetalhe.PrejuizoUsado = valoresExcel.PrejuizoUsado;
                colagemResgateDetalhe.Quantidade = valoresExcel.Quantidade;
                colagemResgateDetalhe.RendimentoResgate = valoresExcel.RendimentoResgate;
                colagemResgateDetalhe.ValorBruto = valoresExcel.ValorBruto;
                colagemResgateDetalhe.ValorCPMF = valoresExcel.ValorCPMF;
                colagemResgateDetalhe.ValorIOF = valoresExcel.ValorIOF;
                colagemResgateDetalhe.ValorIR = valoresExcel.ValorIR;
                colagemResgateDetalhe.ValorLiquido = valoresExcel.ValorLiquido;
                colagemResgateDetalhe.ValorPerformance = valoresExcel.ValorPerformance;
                colagemResgateDetalhe.VariacaoResgate = valoresExcel.VariacaoResgate;
            }
            else
            {
                ColagemResgate colagemResgate = colagemResgateCollection.AddNew();
                colagemResgate.CodigoExterno = valoresExcel.CodigoExterno.Trim();
                colagemResgate.TipoRegistro = valoresExcel.TipoRegistro;
                colagemResgate.CotaOperacao = valoresExcel.CotaOperacao;
                if (valoresExcel.DataConversao != new DateTime())
                    colagemResgate.DataConversao = valoresExcel.DataConversao;
                else
                    colagemResgate.DataConversao = valoresExcel.DataReferencia;
                colagemResgate.DataReferencia = valoresExcel.DataReferencia;
                colagemResgate.IdCarteira = valoresExcel.IdCarteira;
                colagemResgate.IdCliente = valoresExcel.IdCliente;
                colagemResgate.PrejuizoUsado = valoresExcel.PrejuizoUsado;
                colagemResgate.Quantidade = valoresExcel.Quantidade;
                colagemResgate.RendimentoResgate = valoresExcel.RendimentoResgate;
                colagemResgate.ValorBruto = valoresExcel.ValorBruto;
                colagemResgate.ValorCPMF = valoresExcel.ValorCPMF;
                colagemResgate.ValorIOF = valoresExcel.ValorIOF;
                colagemResgate.ValorIR = valoresExcel.ValorIR;
                colagemResgate.ValorLiquido = valoresExcel.ValorLiquido;
                colagemResgate.ValorPerformance = valoresExcel.ValorPerformance;
                colagemResgate.VariacaoResgate = valoresExcel.VariacaoResgate;
            }
            //
            #endregion


        }

        using (esTransactionScope scope = new esTransactionScope())
        {
            #region Deleta ColagemResgate por Codigo
            for (int i = 0; i < lstChave.Count; i++)
            {
                DateTime dataReferencia = this.dataReferenciaDeletarColagemResgateDetalhe[i];
                int idCarteira = Convert.ToInt32(this.idCarteiraDeletarColagemResgateDetalhe[i]);

                int idCliente = Convert.ToInt32(this.idClienteDeletarColagemResgateDetalhe[i]);

                colagemResgateDeletarCollection = new ColagemResgateCollection();
                colagemResgateDeletarCollection.Query.Where(colagemResgateDeletarCollection.Query.DataReferencia.Equal(dataReferencia)
                                                            & colagemResgateDeletarCollection.Query.IdCarteira.Equal(idCarteira)
                                                            & colagemResgateDeletarCollection.Query.IdCliente.Equal(idCliente));

                if (colagemResgateDeletarCollection.Query.Load())
                {
                    colagemResgateDeletarCollection.MarkAllAsDeleted();
                    colagemResgateDeletarCollection.Save();
                }

                //Detalhe
                colagemResgateDetalheDeletarCollection = new ColagemResgateDetalheCollection();
                colagemResgateDetalheDeletarCollection.Query.Where(colagemResgateDetalheDeletarCollection.Query.IdCarteira.Equal(idCarteira)
                                                                   & colagemResgateDetalheDeletarCollection.Query.IdCliente.Equal(idCliente)
                                                                   & colagemResgateDetalheDeletarCollection.Query.DataReferencia.Equal(dataReferencia));

                if (colagemResgateDetalheDeletarCollection.Query.Load())
                {

                    foreach (ColagemResgateDetalhe colagemDetalhe in colagemResgateDetalheDeletarCollection)

                    {
                        ColagemResgateDetalhePosicaoCollection colaResgateDtlPosColl = new ColagemResgateDetalhePosicaoCollection();
                        colaResgateDtlPosColl.Query.Where(colaResgateDtlPosColl.Query.IdColagemResgateDetalhe.Equal(colagemDetalhe.IdColagemResgateDetalhe));

                        if (colaResgateDtlPosColl.Query.Load())
                        {
                            colaResgateDtlPosColl.MarkAllAsDeleted();
                            colaResgateDtlPosColl.Save();
                        }

                    }
                    colagemResgateDetalheDeletarCollection.MarkAllAsDeleted();
                    colagemResgateDetalheDeletarCollection.Save();
                }
            }
            #endregion

            // Salva Cotações Índices presentes no Excel                
            colagemResgateCollection.Save();
            colagemResgateDetalheCollection.Save();
            scope.Complete();
        }
    }

    /// <summary>
    /// Processa a Planilha de CotacaoIndice após ter acabado o Upload
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void uplColagemResgates_FileUploadComplete(object sender, FileUploadCompleteEventArgs e)
    {
        e.CallbackData = "";

        #region Trata Extensão Válida
        if (!ValoresExcelUtil.isExtensaoExcel(e.UploadedFile.FileName.Trim()))
        {
            e.CallbackData = "Importação Colagem - Resgates - Formato de Arquivo Inválido. Formato permitido: .xls ou .xlsx\n\n";
            return;
        }
        #endregion

        // Stream de bytes com o Conteudo do Arquivo Excel
        Stream sr = e.UploadedFile.FileContent;

        #region Processamento Arquivo Excel

        //Reseta Vetor de Dados
        this.valoresExcelColagemResgate = new List<ValoresExcelColagemResgate>();
        this.dataReferenciaDeletarColagemResgateDetalhe = new List<DateTime>();
        this.idClienteDeletarColagemResgateDetalhe = new List<int>();
        this.idCarteiraDeletarColagemResgateDetalhe = new List<int>();
        //
        try
        {
            // Ler Arquivo
            this.LerArquivoColagemResgates(sr);
            // Carrega Arquivo
            this.CarregaColagemResgate();
        }
        catch (Exception e2)
        {
            e.CallbackData = "Importação Colagem Resgates- " + e2.Message;
            return;
        }

        new HistoricoLog().InsereHistoricoLog(DateTime.Now, DateTime.Now,
            "Importação Planilha Colagem Resgates",
            HttpContext.Current.User.Identity.Name, UtilitarioWeb.GetIP(Request), "", HistoricoLogOrigem.Outros);

        #endregion
    }
}
