﻿using System;
using System.Collections.Generic;
using System.IO;
using Financial.Integracao.Excel;
using Financial.Integracao.Excel.Util;
using Financial.Web.Common;
using EntitySpaces.Interfaces;
using DevExpress.Web;
using Financial.CRM;
using Financial.InvestidorCotista;
using Financial.RendaFixa;
using Financial.Investidor;
using Financial.Bolsa;
using Financial.BMF;
using Financial.Fundo;
using Financial.Common;
using Financial.CRM.Enums;
using Financial.InvestidorCotista.Enums;
using Financial.Security;
using System.Web;
using Financial.Web.Util;
using Financial.Security.Enums;
using Financial.Util;

public partial class ImportacaoBasePage : BasePage
{

    /* Estrutura do Excel */
    public List<ValoresExcelCadastroCotistaComIdPessoa> valoresExcelCadastroCotistaComIdPessoa = new List<ValoresExcelCadastroCotistaComIdPessoa>();

    /// <summary>
    /// 
    /// </summary>
    /// <param name="streamExcel"></param>
    private void LerArquivoCadastroCotistaComIdPessoa(Stream streamExcel)
    {

        #region Leitura do Arquivo Excel
        // Open Spreadsheet
        Bytescout.Spreadsheet.Spreadsheet document = new Bytescout.Spreadsheet.Spreadsheet();
        document.LoadFromStream(streamExcel);

        Bytescout.Spreadsheet.Worksheet workSheet = document.Workbook.Worksheets[0];
        //
        #region Confere Formato Arquivo
        string[] colunasConferencias = new string[] {"IdPessoa","IdCarteira","Nome",
                                                     "Apelido","IsentoIR","IsentoIOF",
                                                     "StatusAtivo","TipoCotistaCVM","CodigoInterface",
                                                     "TipoTributacao"};

        bool formato = ValoresExcelUtil.isExcelFormatoValido(colunasConferencias, workSheet);
        if (!formato)
        {
            string mensagem = "Formato Interno do Arquivo Inválido.\n";
            mensagem += "Linha 1 do Arquivo deve ser: \n";
            for (int i = 0; i < colunasConferencias.Length; i++)
            {
                int coluna = i + 1;
                mensagem += "\tColuna " + coluna + ": " + colunasConferencias[i] + "\n";
            }

            document.Close();
            document.Dispose();

            throw new Exception(mensagem);
        }
        #endregion

        //
        int index = 0;
        // This row,column index should be changed as per your need.
        // i.e. which cell in the excel you are interesting to read.
        //
        /* Formato: 1)IdPessoa	- 2)IdCarteira	- 3)Nome	- 4)Apelido
                    5)IsentoIR	- 6)IsentoIOF	- 7)StatusAtivo	- 8)TipoCotistaCVM
                    9)CodigoInterface	- 10)TipoTributacao
         */
        int linha = 1;
        int coluna1 = 0,coluna2 = 1,coluna3 = 2,
            coluna4 = 3,coluna5 = 4,coluna6 = 5,
            coluna7 = 6,coluna8 = 7,coluna9 = 8,
            coluna10 = 9;

        try
        {
            // Enquanto idCliente tiver valor
            while (workSheet.Cell(linha, coluna1).Value != null)
            {
                ValoresExcelCadastroCotistaComIdPessoa item = new ValoresExcelCadastroCotistaComIdPessoa();
                //

                #region IdPessoa
                if (workSheet.Cell(linha, coluna1).Value == null)
                    throw new Exception("IdPessoa não informado: linha " + (linha + 1));
                else
                {
                    try
                    {
                        item.IdPessoa = Convert.ToInt32(workSheet.Cell(linha, coluna1).Value);
                    }
                    catch (Exception ex)
                    {
                        throw new Exception(ex.Message + " - IdPessoa: linha " + (linha + 1));
                    }
                }
                #endregion

                #region IdCarteira
                if (workSheet.Cell(linha, coluna2).Value != null)
                {
                    try
                    {
                        item.IdCarteira = Convert.ToInt32(workSheet.Cell(linha, coluna2).Value);
                    }
                    catch (Exception ex)
                    {
                        throw new Exception(ex.Message + " - IdCarteira: linha " + (linha + 1));
                    }
                }
                else
                {
                    item.IdCarteira = null;
                }
                #endregion

                #region Nome
                if (workSheet.Cell(linha, coluna3).Value == null)
                    throw new Exception("Nome não informado: linha " + (linha + 1));
                try
                {
                    item.Nome = Convert.ToString(workSheet.Cell(linha, coluna3).Value);
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message + " - Nome: linha " + (linha + 1));
                }
                #endregion

                #region Apelido
                if (workSheet.Cell(linha, coluna4).Value != null)
                {
                    try
                    {
                        item.Apelido = Convert.ToString(workSheet.Cell(linha, coluna4).Value);
                    }
                    catch (Exception ex)
                    {
                        throw new Exception(ex.Message + " - Apelido: linha " + (linha + 1));
                    }
                }
                else
                {
                    item.Apelido = item.Nome;
                }
                #endregion

                #region IsentoIR
                if (workSheet.Cell(linha, coluna5).Value == null)
                    throw new Exception("IsentoIR não informado: linha " + (linha + 1));
                try
                {
                    item.IsentoIR = Convert.ToString(workSheet.Cell(linha, coluna5).Value);
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message + " - IsentoIR: linha " + (linha + 1));
                }
                #endregion

                #region IsentoIOF
                if (workSheet.Cell(linha, coluna6).Value == null)
                    throw new Exception("IsentoIOF não informado: linha " + (linha + 1));
                try
                {
                    item.IsentoIOF = Convert.ToString(workSheet.Cell(linha, coluna6).Value);
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message + " - IsentoIOF: linha " + (linha + 1));
                }
                #endregion

                #region StatusCotista
                if (workSheet.Cell(linha, coluna7).Value == null)
                    throw new Exception("StatusCotista não informado: linha " + (linha + 1));
                try
                {
                    int status = Convert.ToInt32(workSheet.Cell(linha, coluna7).Value);
                    item.StatusCotista = (StatusAtivoCotista)status;
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message + " - StatusCotista: linha " + (linha + 1));
                }
                #endregion

                #region TipoCotistaCVM
                if (workSheet.Cell(linha, coluna8).Value != null)
                {
                    try
                    {
                        item.TipoCotistaCVM = (TipoCotista)Convert.ToInt32(workSheet.Cell(linha, coluna8).Value);
                    }
                    catch (Exception ex)
                    {
                        throw new Exception(ex.Message + " - TipoCotistaCVM: linha " + (linha + 1));
                    }
                }
                #endregion

                #region CodigoInterface
                if (workSheet.Cell(linha, coluna9).Value != null)
                {
                    try
                    {
                        item.CodigoInterface = workSheet.Cell(linha, coluna9).ValueAsString;
                    }
                    catch (Exception ex)
                    {
                        throw new Exception(ex.Message + " - TipoCotistaCVM: linha " + (linha + 1));
                    }
                }
                else
                    item.CodigoInterface = null;
                #endregion

                #region TipoTributacaoCotista
                if (workSheet.Cell(linha, coluna10).Value == null)
                    throw new Exception("TipoTributacaoCotista não informado: linha " + (linha + 1));
                try
                {
                    int tipoTributacao = Convert.ToInt32(workSheet.Cell(linha, coluna10).Value);
                    item.TipoTributacaoCotista = (TipoTributacaoCotista)tipoTributacao;
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message + " - TipoTributacaoCotista: linha " + (linha + 1));
                }
                #endregion

                this.valoresExcelCadastroCotistaComIdPessoa.Add(item);
                //
                index++;
                linha = 1 + index;
                //                
                Console.WriteLine("{0},{1},{2},{3},{4},{5},{6},{7},{8},{9}",
                                    item.IdPessoa,item.IdCarteira,item.Nome,
                                    item.Apelido,item.IsentoIR,item.IsentoIOF,
                                    item.StatusCotista, item.TipoCotistaCVM, item.CodigoInterface,
                                    item.TipoTributacaoCotista);
            }
        }

        catch (Exception ex)
        {
            document.Close();
            document.Dispose();

            throw new Exception(ex.Message);
        }
        #endregion

        document.Close();
        document.Dispose();
    }

    /// <summary>
    /// 
    /// </summary>
    public void CarregaCadastroCotistaComIdPessoa()
    {
        CotistaCollection cotistaCollection = new CotistaCollection();
        PermissaoCotistaCollection permissaoCotistaCollection = new PermissaoCotistaCollection();

        ContaCorrenteCollection contaCorrenteCollection = new ContaCorrenteCollection();

        Usuario usuario = new Usuario();

        string login = HttpContext.Current.User.Identity.Name;
        if (String.IsNullOrEmpty(login))
        {
            //Quando carregando de web service não temos o Identity - olhar para o campo CriadoPor
            if (this.valoresExcelCadastroCotistaComIdPessoa.Count > 0)
            {
                login = this.valoresExcelCadastroCotistaComIdPessoa[0].CriadoPor;
            }
        }

        int idCotista = 1;
        cotistaCollection.Query.Select(cotistaCollection.Query.IdCotista.Max());
        if (cotistaCollection.Query.Load())
            idCotista += cotistaCollection[0].IdCotista.GetValueOrDefault(0);

        cotistaCollection = new CotistaCollection();

        bool usuarioEncontrado = usuario.BuscaUsuario(login);
        for (int i = 0; i < this.valoresExcelCadastroCotistaComIdPessoa.Count; i++, idCotista++)
        {
            ValoresExcelCadastroCotistaComIdPessoa valoresExcel = this.valoresExcelCadastroCotistaComIdPessoa[i];
            //                    
            #region Valida Pessoa
            Pessoa pessoaAux = new Pessoa();
            List<esQueryItem> campos = new List<esQueryItem>();
            campos.Add(pessoaAux.Query.IdPessoa);
            campos.Add(pessoaAux.Query.Tipo);
            campos.Add(pessoaAux.Query.CodigoInterface);
            if (!pessoaAux.LoadByPrimaryKey(campos, valoresExcel.IdPessoa))
            {
                throw new Exception("Id Pessoa não existente : " + valoresExcel.IdPessoa);
            }
            #endregion

            #region Valida Carteira
            if (valoresExcel.IdCarteira.HasValue)
            {
                Carteira carteiraAux = new Carteira();
                if (!carteiraAux.LoadByPrimaryKey(valoresExcel.IdCarteira.Value))
                {
                    throw new Exception("Id carteira não existente : " + valoresExcel.IdCarteira.Value);
                }
            }
            #endregion

            #region Verifica se Campos Obrigatórios foram Preenchidos
            if (String.IsNullOrEmpty(valoresExcel.Nome.Trim()))
            {
                throw new Exception("Nome Cotista não pode ser em Branco");
            }
            if (String.IsNullOrEmpty(valoresExcel.IsentoIR.Trim()))
            {
                throw new Exception("Campo IsentoIR é Obrigatório");
            }
            if (String.IsNullOrEmpty(valoresExcel.IsentoIOF.Trim()))
            {
                throw new Exception("Campo IsentoIOF é Obrigatório");
            }
            if (valoresExcel.StatusCotista == 0)
            { // Conversão do Enum resultou em 0
                throw new Exception("Status Cotista (Ativo/Inativo) deve ser Preenchido");
            }
            if (valoresExcel.TipoTributacaoCotista == 0)
            { // Conversão do Enum resultou em 0
                throw new Exception("Tipo Tributação Cotista deve ser Preenchido");
            }
            #endregion

            #region Cotista
            Cotista cotista = cotistaCollection.AddNew();
            cotista.IdCotista = idCotista;
            cotista.IdPessoa = valoresExcel.IdPessoa;
            cotista.Nome = valoresExcel.Nome.Trim();
            int maxlength = (int)CotistaMetadata.Meta().Columns.FindByColumnName(CotistaMetadata.ColumnNames.Apelido).CharacterMaxLength;
            cotista.Apelido = (cotista.Nome.Length <= maxlength) ? cotista.Nome : cotista.Nome.Substring(0, maxlength - 1);

            cotista.IsentoIR = valoresExcel.IsentoIR.ToUpper();
            cotista.IsentoIOF = valoresExcel.IsentoIOF.ToUpper();
            cotista.StatusAtivo = (byte)valoresExcel.StatusCotista;

            cotista.TipoTributacao = (byte)valoresExcel.TipoTributacaoCotista;

            if (valoresExcel.TipoCotistaCVM != null)
            {
                cotista.TipoCotistaCVM = (int)valoresExcel.TipoCotistaCVM;
            }

            if (valoresExcel.IdCarteira.HasValue)
                cotista.IdCarteira = valoresExcel.IdCarteira.Value;
            else
                cotista.IdCarteira = null;

            if (!String.IsNullOrEmpty(valoresExcel.CodigoInterface))
            {
                cotista.CodigoInterface = valoresExcel.CodigoInterface;
            }
            #endregion

            #region Adiciona em PermissaoCotista
            if (usuarioEncontrado)
            {
                PermissaoCotista permissaoCotista = permissaoCotistaCollection.AddNew();
                permissaoCotista.IdCotista = cotista.IdCotista.Value;
                permissaoCotista.IdUsuario = usuario.IdUsuario.Value;
            }
            #endregion            
        }

        using (esTransactionScope scope = new esTransactionScope())
        {
            cotistaCollection.Save();
            permissaoCotistaCollection.Save();

            //
            scope.Complete();
        }
    }
   
    /// <summary>
    /// Processa a Planilha de Cotista após ter acabado o Upload
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void uplCadastroCotistaComIdPessoa_FileUploadComplete(object sender, FileUploadCompleteEventArgs e)
    {
        e.CallbackData = "";

        #region Trata Extensão Válida
        if (!ValoresExcelUtil.isExtensaoExcel(e.UploadedFile.FileName.Trim()))
        {
            e.CallbackData = "Importação Cadastro Cotista - Formato de Arquivo Inválido. Formato permitido: .xls ou .xlsx\n\n";
            return;
        }
        #endregion

        // Stream de bytes com o Conteudo do Arquivo Excel
        Stream sr = e.UploadedFile.FileContent;

        #region Processamento Arquivo Excel

        //Reseta vetor de dados
        this.valoresExcelCadastroCotistaComIdPessoa = new List<ValoresExcelCadastroCotistaComIdPessoa>();

        try
        {
            // Ler Arquivo
            this.LerArquivoCadastroCotistaComIdPessoa(sr);
            // Carrega Arquivo
            this.CarregaCadastroCotistaComIdPessoa();
        }
        catch (Exception e2)
        {
            e.CallbackData = "Importação Cadastro Cotista - " + e2.Message;
            return;
        }

        new HistoricoLog().InsereHistoricoLog(DateTime.Now, DateTime.Now,
                        "Importação Planilha Cadastro Cotista",
                        HttpContext.Current.User.Identity.Name, UtilitarioWeb.GetIP(Request), "", HistoricoLogOrigem.Outros);

        #endregion
    }
}