﻿using System;
using System.Collections.Generic;
using System.IO;

using Financial.Integracao.Excel;
using Financial.Integracao.Excel.Util;
using Financial.Web.Common;

using EntitySpaces.Interfaces;

using DevExpress.Web;
using Financial.Common;
using Financial.Security;
using System.Web;
using Financial.Web.Util;
using Financial.Security.Enums;
using Financial.Util;

public partial class ImportacaoBasePage : BasePage {

    /* Estrutura do Excel */
    private List<ValoresExcelCotacaoIndice> valoresExcelCotacaoIndice = new List<ValoresExcelCotacaoIndice>();
    //
    List<int> idDeletarCotacaoIndice = new List<int>();
    List<DateTime> dataDeletarCotacaoIndice = new List<DateTime>();
   
    /// <summary>
    /// Leitura de um arquivo Excel
    /// Saida: List<ValoresExcel> com os valores armazenados no arquivo
    /// </summary>
    /// <param name="streamExcel">Stream de bytes de um arquivo Excel</param>
    /// <exception cref="Exception">Se ocorreu problemas</exception>
    private void LerArquivoCotacaoIndice(Stream streamExcel) {

        #region Leitura do Arquivo Excel

        // Open Spreadsheet
        Bytescout.Spreadsheet.Spreadsheet document = new Bytescout.Spreadsheet.Spreadsheet();
        document.LoadFromStream(streamExcel);

        Bytescout.Spreadsheet.Worksheet workSheet = document.Workbook.Worksheets[0];

        //
        #region Confere Formato Arquivo
        string[] colunasConferencias = new string[] { "Data", "Indice", "Valor" };

        bool formato = ValoresExcelUtil.isExcelFormatoValido(colunasConferencias, workSheet);

        if (!formato) {
            string mensagem = "Formato Interno do Arquivo Inválido.\n";
            mensagem += "Linha 1 do Arquivo deve ser: \n";
            for (int i = 0; i < colunasConferencias.Length; i++) {
                int coluna = i + 1;
                mensagem += "\tColuna " + coluna + ": " + colunasConferencias[i] + "\n";
            }

            document.Close();
            document.Dispose();

            throw new Exception(mensagem);
        }
        #endregion

        //
        int index = 0;
        // This row,column index should be changed as per your need.
        // i.e. which cell in the excel you are interesting to read.
        //
        /* Formato: 1)Data  - 2)IdIndice    - 3)Valor */

        int linha = 1;
        int coluna1 = 0, coluna2 = 1, coluna3 = 2;

        //            
        try {
            // Enquanto Data tiver valor
            while (workSheet.Cell(linha, coluna1).Value != null) {
                ValoresExcelCotacaoIndice item = new ValoresExcelCotacaoIndice();

                try {
                    item.Data = workSheet.Cell(linha, coluna1).ValueAsDateTime;
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message + " - Data: linha " + (linha + 1));
                }

                if (workSheet.Cell(linha, coluna2).Value == null)
                    throw new Exception("IdIndice não informado: linha " + (linha + 1));
                try {
                    item.IdIndice = workSheet.Cell(linha, coluna2).ValueAsInteger;
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message + " - IdIndice: linha " + (linha + 1));
                }

                if (workSheet.Cell(linha, coluna3).Value == null)
                    throw new Exception("Valor não informado: linha " + (linha + 1));
                try {
                    item.Valor = Convert.ToDecimal(workSheet.Cell(linha, coluna3).Value);
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message + " - Valor: linha " + (linha + 1));
                }

                this.valoresExcelCotacaoIndice.Add(item);
                
                index++;
                linha = 1 + index;
                                
                Console.WriteLine("{0},{1},{2}", item.Data, item.IdIndice, item.Valor);
            }
        }
        catch (Exception ex) {
            document.Close();
            document.Dispose();

            throw new Exception(ex.Message);
        }
        #endregion

        document.Close();
        document.Dispose();
    }

    /// <summary>
    /// Gera e Carrega as Cotações Indices de acordo com o objeto List<ValoresExcel>
    /// </summary>
    private void CarregaCotacaoIndice() {
        List<short> listIdIndice = new List<short>();
        CotacaoIndiceCollection cotacaoIndiceCollection = new CotacaoIndiceCollection();
        CotacaoIndiceCollection cotacaoIndiceDeletarCollection = new CotacaoIndiceCollection();
        //    
        for (int i = 0; i < this.valoresExcelCotacaoIndice.Count; i++) {
            ValoresExcelCotacaoIndice valoresExcel = this.valoresExcelCotacaoIndice[i];
            //
            this.idDeletarCotacaoIndice.Add(valoresExcel.IdIndice);

            #region Verifica se Existe Indice
            Indice indice = new Indice();
            if (!indice.LoadByPrimaryKey((short)valoresExcel.IdIndice))
            {
                throw new Exception("IdIndice Não existente: " + valoresExcel.IdIndice + "\n Cadastre o Índice " + valoresExcel.IdIndice);
            }
            else
            {
                if (ParametrosConfiguracaoSistema.Outras.CalculaFatorIndice)
                {
                    bool poeNaLista = true;
                    foreach (short idIndice in listIdIndice)
                    {
                        if (idIndice == indice.IdIndice)
                        {
                            poeNaLista = false;
                        }
                    }
                    if (poeNaLista)
                    {
                        listIdIndice.Add((short)indice.IdIndice);
                    }
                }
            }
            #endregion

            // Salva a Data para poder Deletar
            this.dataDeletarCotacaoIndice.Add(valoresExcel.Data);

            #region CotacaoIndice
            CotacaoIndice cotacaoIndice = cotacaoIndiceCollection.AddNew();
            // Dados do Arquivo
            cotacaoIndice.Data = valoresExcel.Data;

            // Maximo é 32767
            cotacaoIndice.IdIndice = (short)valoresExcel.IdIndice;
            cotacaoIndice.Valor = valoresExcel.Valor;
            //
            #endregion
        }

        using (esTransactionScope scope = new esTransactionScope()) {

            #region Deleta CotacaoIndice por Data/Indice
            for (int i = 0; i < this.idDeletarCotacaoIndice.Count; i++) {
                cotacaoIndiceDeletarCollection = new CotacaoIndiceCollection();

                cotacaoIndiceDeletarCollection.Query
                .Where(cotacaoIndiceDeletarCollection.Query.IdIndice == this.idDeletarCotacaoIndice[i],
                       cotacaoIndiceDeletarCollection.Query.Data == this.dataDeletarCotacaoIndice[i]);

                cotacaoIndiceDeletarCollection.Query.Load();
                cotacaoIndiceDeletarCollection.MarkAllAsDeleted();
                cotacaoIndiceDeletarCollection.Save();
            }
            #endregion

            // Salva Cotações Índices presentes no Excel                
            cotacaoIndiceCollection.Save();

            scope.Complete();
        }

        using (esTransactionScope scope = new esTransactionScope()) {

            if (ParametrosConfiguracaoSistema.Outras.CalculaFatorIndice)
            {
                CotacaoIndice cotacaoIndice = new CotacaoIndice();
                foreach (short idIndice in listIdIndice)
                {
                    cotacaoIndice.AtualizaFatorIndice(idIndice);
                }
            }

            scope.Complete();
        }
    }

    /// <summary>
    /// Processa a Planilha de CotacaoIndice após ter acabado o Upload
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void uplCotacaoIndice_FileUploadComplete(object sender, FileUploadCompleteEventArgs e) {
        e.CallbackData = "";

        #region Trata Extensão Válida
        if (!ValoresExcelUtil.isExtensaoExcel(e.UploadedFile.FileName.Trim())) {
            e.CallbackData = "Importação Cotação Índice - Formato de Arquivo Inválido. Formato permitido: .xls ou .xlsx\n\n";
            return;
        }
        #endregion

        // Stream de bytes com o Conteudo do Arquivo Excel
        Stream sr = e.UploadedFile.FileContent;

        #region Processamento Arquivo Excel

        //Reseta Vetor de Dados
        this.valoresExcelCotacaoIndice = new List<ValoresExcelCotacaoIndice>();
        this.idDeletarCotacaoIndice = new List<int>();
        this.dataDeletarCotacaoIndice = new List<DateTime>();
        //
        try {
            // Ler Arquivo
            this.LerArquivoCotacaoIndice(sr);
            // Carrega Arquivo
            this.CarregaCotacaoIndice();
        }
        catch (Exception e2) {
            e.CallbackData = "Importação Cotação Índice - " + e2.Message;
            return;
        }

        new HistoricoLog().InsereHistoricoLog(DateTime.Now, DateTime.Now,
            "Importação Planilha Cotação Índice",
            HttpContext.Current.User.Identity.Name, UtilitarioWeb.GetIP(Request), "", HistoricoLogOrigem.Outros);

        #endregion

    }
}
