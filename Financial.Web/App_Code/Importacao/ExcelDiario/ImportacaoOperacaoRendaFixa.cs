using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Financial.Web.Common;
using System.IO;
using Financial.Integracao.Excel.Util;
using Financial.Integracao.Excel;
using EntitySpaces.Interfaces;
using Financial.Common;
using Financial.Investidor;
using System.Collections.Generic;
using Financial.Investidor.Enums;
using DevExpress.Web;
using Financial.Util;
using Financial.Util.Enums;
using Financial.RendaFixa;
using Financial.RendaFixa.Enums;
using Financial.Security;
using Financial.Web.Util;
using Financial.Security.Enums;
using Financial.Common.Exceptions;
using Financial.Common.Enums;
using Financial.Fundo;
using Financial.RendaFixa.Controller;

public partial class ImportacaoBasePage : BasePage {

    /* Estrutura do Excel */
    public List<ValoresExcelOperacaoRendaFixa> valoresExcelOperacaoRendaFixa = new List<ValoresExcelOperacaoRendaFixa>();

    /// <summary>
    /// Leitura de um arquivo Excel
    /// Saida: List<ValoresExcel> com os valores armazenados no arquivo
    /// </summary>
    /// <param name="streamExcel">Stream de bytes de um arquivo Excel</param>
    /// <exception cref="Exception">Se ocorreu problemas</exception>   
    private void LerArquivoOperacaoRendaFixa(Stream streamExcel) {

        #region Leitura do Arquivo Excel

        // Open Spreadsheet
        Bytescout.Spreadsheet.Spreadsheet document = new Bytescout.Spreadsheet.Spreadsheet();
        document.LoadFromStream(streamExcel);

        Bytescout.Spreadsheet.Worksheet workSheet = document.Workbook.Worksheets[0];

        //
        #region Confere Formato Arquivo
        string[] colunasConferencias = new string[] 
                            {"IdCliente","IdTitulo","DataOperacao",
                             "TipoOperacao","Quantidade", 
                             "PuOperacao", "Valor", "TaxaOperacao", "Categoria",
                             "DataVolta", "TaxaVolta","PuVolta", "ValorVolta", "IdIndiceVolta", "Custodia",
                             "CodigoBovespa", "DataOperacaoOriginal", "IPO"};

        bool formato = ValoresExcelUtil.isExcelFormatoValido(colunasConferencias, workSheet);

        if (!formato) {
            string mensagem = "Formato Interno do Arquivo Inválido.\n";
            mensagem += "Linha 1 do Arquivo deve ser: \n";
            for (int i = 0; i < colunasConferencias.Length; i++) {
                int coluna = i + 1;
                mensagem += "\tColuna " + coluna + ": " + colunasConferencias[i] + "\n";
            }

            document.Close();
            document.Dispose();

            throw new Exception(mensagem);
        }
        #endregion

        //
        int index = 0;
        // This row,column index should be changed as per your need.
        // i.e. which cell in the excel you are interesting to read.
        //
        /* Formato: 1)IdCliente      - 2)IdTitulo       - 3)DataOperacao
         *          4)TipoOperacao   - 5)Quantidade     - 6)PuOperacao
         *          7)Valor          - 8)TaxaOperacao   - 9)Categoria
         *          10)DataVolta     - 11)TaxaVolta     - 12)PuVolta       
         *          13)ValorVolta   - 14)IdIndiceVolta - 15)IdCustodia
         *          16)CodigoBovespa  17)DataOriginal  - 18)IPO
         *          19)LocalNegociacao 20)AgenteCustodia 21)Clearing
         *          22)Trader          23)IdAgenteContraparte 24)IdClienteContraparte		 
         *          25)ValorCorretagem  26)IdOperacaoExterna 27)IdCategoriaMovimentacao
         *          28)OperacaoTermo

         */
        int linha = 1;
        int coluna1 = 0, coluna2 = 1, coluna3 = 2,
            coluna4 = 3, coluna5 = 4, coluna6 = 5,
            coluna7 = 6, coluna8 = 7, coluna9 = 8,
            coluna10 = 9, coluna11 = 10, coluna12 = 11,
            coluna13 = 12, coluna14 = 13, coluna15 = 14,
            coluna16 = 15, coluna17 = 16, coluna18 = 17,
            coluna19 = 18, coluna20 = 19, coluna21 = 20,
            coluna22 = 21, coluna23 = 22, coluna24 = 23,
            coluna25 = 24, coluna26 = 25, coluna27 = 26,
            coluna28 = 27, coluna29 = 28;

        //            
        try {
            // Enquanto idCliente tiver valor
            while (workSheet.Cell(linha, coluna1).Value != null) {
                ValoresExcelOperacaoRendaFixa item = new ValoresExcelOperacaoRendaFixa();
                
                try {
                    item.IdCliente = workSheet.Cell(linha, coluna1).ValueAsInteger;
                }
                catch (Exception ex) {
                    throw new Exception(ex.Message + " - IdCliente: linha " + (linha + 1));
                }

                if (workSheet.Cell(linha, coluna2).Value == null)
                    throw new Exception("IdTitulo não informado: linha " + (linha + 1));
                try {
                    item.IdTitulo = workSheet.Cell(linha, coluna2).ValueAsInteger;
                }
                catch (Exception ex) {
                    throw new Exception(ex.Message + " - IdTitulo: linha " + (linha + 1));
                }

                if (workSheet.Cell(linha, coluna3).Value == null)
                    throw new Exception("DataOperacao não informada: linha " + (linha + 1));
                try {
                    item.DataOperacao = workSheet.Cell(linha, coluna3).ValueAsDateTime;
                }
                catch (Exception ex) {
                    throw new Exception(ex.Message + " - DataOperacao: linha " + (linha + 1));
                }

                if (workSheet.Cell(linha, coluna4).Value != null) {
                    try {
                        item.TipoOperacao = workSheet.Cell(linha, coluna4).Value.ToString().Trim();
                    }
                    catch (Exception ex) {
                        throw new Exception(ex.Message + " - TipoOperacao: linha " + (linha + 1));
                    }
                }

                if (workSheet.Cell(linha, coluna5).Value == null)
                    throw new Exception("Quantidade não informada: linha " + (linha + 1));
                try {
                    item.Quantidade = Convert.ToDecimal(workSheet.Cell(linha, coluna5).Value);
                }
                catch (Exception ex) {
                    throw new Exception(ex.Message + " - Quantidade: linha " + (linha + 1));
                }

                if (workSheet.Cell(linha, coluna6).Value == null)
                    throw new Exception("PuOperacao não informado: linha " + (linha + 1));
                try {
                    item.PuOperacao = Convert.ToDecimal(workSheet.Cell(linha, coluna6).Value);
                }
                catch (Exception ex) {
                    throw new Exception(ex.Message + " - PuOperacao: linha " + (linha + 1));
                }

                if (workSheet.Cell(linha, coluna7).Value == null)
                    throw new Exception("Valor não informado: linha " + (linha + 1));
                try {
                    item.Valor = Convert.ToDecimal(workSheet.Cell(linha, coluna7).Value);
                }
                catch (Exception ex) {
                    throw new Exception(ex.Message + " - Valor: linha " + (linha + 1));
                }

                if (workSheet.Cell(linha, coluna8).Value == null)
                    throw new Exception("TaxaOperacao não informada: linha " + (linha + 1));
                try {
                    item.TaxaOperacao = Convert.ToDecimal(workSheet.Cell(linha, coluna8).Value);
                }
                catch (Exception ex) {
                    throw new Exception(ex.Message + " - TaxaOperacao: linha " + (linha + 1));
                }

                if (workSheet.Cell(linha, coluna9).Value != null) {
                    try {
                        item.Categoria = workSheet.Cell(linha, coluna9).Value.ToString().Trim();
                    }
                    catch (Exception ex) {
                        throw new Exception(ex.Message + " - Categoria: linha " + (linha + 1));
                    }
                }

                if (workSheet.Cell(linha, coluna10).Value != null) {
                    try {
                        item.DataVolta = workSheet.Cell(linha, coluna10).ValueAsDateTime;
                    }
                    catch (Exception ex) {
                        throw new Exception(ex.Message + " - DataVolta: linha " + (linha + 1));
                    }
                }
                
                if (workSheet.Cell(linha, coluna11).Value != null) {
                    try {
                        item.TaxaVolta = Convert.ToDecimal(workSheet.Cell(linha, coluna11).Value);
                    }
                    catch (Exception ex) {
                        throw new Exception(ex.Message + " - TaxaVolta: linha " + (linha + 1));
                    }
                }
                
                if (workSheet.Cell(linha, coluna12).Value != null) {
                    try {
                        item.PuVolta = Convert.ToDecimal(workSheet.Cell(linha, coluna12).Value);
                    }
                    catch (Exception ex) {
                        throw new Exception(ex.Message + " - PuVolta: linha " + (linha + 1));
                    }
                }
                
                if (workSheet.Cell(linha, coluna13).Value != null) {
                    try {
                        item.ValorVolta = Convert.ToDecimal(workSheet.Cell(linha, coluna13).Value);
                    }
                    catch (Exception ex) {
                        throw new Exception(ex.Message + " - ValorVolta: linha " + (linha + 1));
                    }
                }

                if (workSheet.Cell(linha, coluna14).Value != null) {
                    try {
                        item.IdIndiceVolta = (short)workSheet.Cell(linha, coluna14).ValueAsInteger;
                    }
                    catch (Exception ex) {
                        throw new Exception(ex.Message + " - IdIndiceVolta: linha " + (linha + 1));
                    }
                }

                if (workSheet.Cell(linha, coluna15).Value != null)
                {
                    try {
                        item.Custodia = workSheet.Cell(linha, coluna15).Value.ToString().Trim();
                    }
                    catch (Exception ex) {
                        throw new Exception(ex.Message + " - Custodia: linha " + (linha + 1));
                    }
                }

                if (workSheet.Cell(linha, coluna16).Value != null)
                {
                    try {
                        item.CodigoBovespa = workSheet.Cell(linha, coluna16).ValueAsInteger;
                    }
                    catch (Exception ex) {
                        throw new Exception(ex.Message + " - CodigoBovespa: linha " + (linha + 1));
                    }
                }

                #region Data operação original
                if (workSheet.Cell(linha, coluna17).Value != null)
                {
                    try
                    {
                        item.DataOriginal = workSheet.Cell(linha, coluna17).ValueAsDateTime;
                    }
                    catch (Exception ex)
                    {
                        throw new Exception(ex.Message + " - DataOriginal: linha " + (linha + 1));
                    }
                }
                else
                {
                    item.DataOriginal = item.DataOperacao;
                }
                #endregion

				#region IPO
                if (workSheet.Cell(linha, coluna18).Value != null)
                {
                    try
                    {
                        item.IsIPO = workSheet.Cell(linha, coluna18).Value.ToString().Trim();
                    }
                    catch (Exception ex)
                    {
                        throw new Exception(ex.Message + " - IPO: linha " + (linha + 1));
                    }
                }
				#endregion 

                #region Local Negociação
                if (workSheet.Cell(linha, coluna19).Value == null)
                {
                    item.LocalNegociacao = null;
                }
                else
                {
                    try
                    {
                        item.LocalNegociacao = workSheet.Cell(linha, coluna19).ValueAsString;
                    }
                    catch (Exception ex)
                    {
                        throw new Exception(ex.Message + " - Local Negociação: linha " + (linha + 1));
                    }
                }
                #endregion

                #region Corretora
                if (workSheet.Cell(linha, coluna20).Value == null)
                {
                    item.IdAgenteCustodia = null;
                }
                else
                {
                    try
                    {
                        item.IdAgenteCustodia = workSheet.Cell(linha, coluna20).ValueAsString;
                    }
                    catch (Exception ex)
                    {
                        throw new Exception(ex.Message + " - AgenteCustodia: linha " + (linha + 1));
                    }
                }
                #endregion

                #region Clearing
                if (workSheet.Cell(linha, coluna21).Value == null)
                {
                    item.Clearing = null;
                }
                else
                {
                    try
                    {
                        item.Clearing = workSheet.Cell(linha, coluna21).ValueAsString;
                    }
                    catch (Exception ex)
                    {
                        throw new Exception(ex.Message + " - Clearing: linha " + (linha + 1));
                    }
                }
                #endregion

                if (workSheet.Cell(linha, coluna22).Value != null &&
                     workSheet.Cell(linha, coluna22).Value.ToString().Trim() != "")
                {
                    try
                    {
                        item.Trader = workSheet.Cell(linha, coluna22).ValueAsString;
                    }
                    catch (Exception ex)
                    {
                        throw new Exception(ex.Message + " - Trader: linha " + (linha + 1));
                    }
                }

                if (workSheet.Cell(linha, coluna23).Value != null)
                {
                    try
                    {
                        item.IdAgenteContraparte = workSheet.Cell(linha, coluna23).ValueAsInteger;
                    }
                    catch (Exception ex)
                    {
                        throw new Exception(ex.Message + " - IdAgenteContraparte: linha " + (linha + 1));
                    }
                }

                if (workSheet.Cell(linha, coluna24).Value != null)
                {
                    try
                    {
                        item.IdClienteContraparte = workSheet.Cell(linha, coluna24).ValueAsInteger;
                    }
                    catch (Exception ex)
                    {
                        throw new Exception(ex.Message + " - IdClienteContraparte" + (linha + 1));
                    }
                }

                if (workSheet.Cell(linha, coluna25).Value != null)
                {
                    try
                    {
                        item.DataLiquidacao = workSheet.Cell(linha, coluna25).ValueAsDateTime;
                    }
                    catch (Exception ex)
                    {
                        throw new Exception(ex.Message + " - DataLiquidacao" + (linha + 1));
                    }
                }


                if (workSheet.Cell(linha, coluna26).Value != null)
                {
                    try
                    {
                        item.ValorCorretagem = Convert.ToDecimal(workSheet.Cell(linha, coluna26).Value);
                    }
                    catch (Exception ex)
                    {
                        throw new Exception(ex.Message + " - ValorCorretagem" + (linha + 1));
                    }
                }

                if (workSheet.Cell(linha, coluna27).Value != null)
                {
                    try
                    {
                        item.IdOperacaoResgatada = workSheet.Cell(linha, coluna27).ValueAsInteger;
                    }
                    catch (Exception ex)
                    {
                        throw new Exception(ex.Message + " - IdOperacaoResgatada" + (linha + 1));
                    }
                }

                if (workSheet.Cell(linha, coluna28).Value != null)
                {
                    try
                    {
                        item.IdCategoriaMovimentacao = workSheet.Cell(linha, coluna28).ValueAsInteger;
                    }
                    catch (Exception ex)
                    {
                        throw new Exception(ex.Message + " - IdCategoriaMovimentacao" + (linha + 1));
                    }
                }

                if (workSheet.Cell(linha, coluna29).Value != null)
                {
                    try
                    {
                        item.OperacaoTermo = workSheet.Cell(linha, coluna29).ToString();
                    }
                    catch (Exception ex)
                    {
                        throw new Exception(ex.Message + " - OperacaoTermo" + (linha + 1));
                    }
                }
               
                this.valoresExcelOperacaoRendaFixa.Add(item);
                
                index++;
                linha = 1 + index;

                Console.WriteLine("{0},{1},{2},{3},{4},{5},{6},{7},{8},{9},{10},{11},{12},{13},{14},{15},{16},{17},{18},{19},{20},{21},{22},{23}",
                    item.IdCliente, item.IdTitulo, item.DataOperacao,
                    item.TipoOperacao, item.Quantidade,
                    item.PuOperacao, item.Valor, item.TaxaOperacao, item.Categoria,
                    item.DataVolta, item.TaxaVolta, item.PuVolta, item.ValorVolta, item.IdIndiceVolta, item.Custodia, item.CodigoBovespa, item.DataOriginal,
                    item.IsIPO, item.LocalNegociacao, item.IdAgenteCustodia, item.Clearing, item.Trader, item.IdAgenteContraparte, item.IdClienteContraparte);
            }
        }
        catch (Exception ex) {
            document.Close();
            document.Dispose();

            throw new Exception(ex.Message);
        }
        #endregion

        document.Close();
        document.Dispose();
    }

    /// <summary>
    /// Gera e carrega as Operações RendaFixa de acordo com o objeto List<ValoresExcel>
    /// </summary>
    /// <param name="ignoraCarteiraInexistente">ignora cliente e carteira inexistentes</param> 
    /// <exception cref="Exception">Se ocorreu problemas</exception>
    public void CarregaOperacaoRendaFixa(bool ignoraCarteiraInexistente) {
        OperacaoRendaFixaCollection operacaoRendaFixaCollection = new OperacaoRendaFixaCollection();
        //
        Dictionary<string, decimal> dicQuantidadePosicao = new Dictionary<string, decimal>();
        int idCarteiraPosicao = 0;
        decimal quantidadeCarteiraIntermediacao = 0; 
        for (int i = 0; i < this.valoresExcelOperacaoRendaFixa.Count; i++) {
            ValoresExcelOperacaoRendaFixa valoresExcel = this.valoresExcelOperacaoRendaFixa[i];
            //                  


            #region Testa TipoOperacao Nulo
            if (String.IsNullOrEmpty(valoresExcel.TipoOperacao)) {
                throw new Exception("Tipo Operação não pode ser nulo ");
            }
            #endregion

            #region Testa TipoOperacaoTitulo
            int? tipo = null;
            try {
                tipo = (int)Enum.Parse(typeof(TipoOperacaoTitulo), valoresExcel.TipoOperacao);
            }
            catch (Exception e) {
                string tipos = "";
                for (int j = 0; j < Enum.GetNames(typeof(TipoOperacaoTitulo)).Length; j++) {
                    tipos = tipos == ""
                        ? tipos + Enum.GetNames(typeof(TipoOperacaoTitulo))[j]
                        : tipos + ", " + Enum.GetNames(typeof(TipoOperacaoTitulo))[j];
                }
                throw new Exception(e.Message + "TipoOperacao deve ser: " + tipos);
            }
            #endregion

            #region Testa Cliente existente
            Cliente cliente = new Cliente();
            List<esQueryItem> campos = new List<esQueryItem>();
            campos.Add(cliente.Query.IdCliente);
            campos.Add(cliente.Query.DataDia);
            campos.Add(cliente.Query.Status);
            if (!cliente.LoadByPrimaryKey(campos, valoresExcel.IdCliente)) {
                if (ignoraCarteiraInexistente) {
                    continue;
                }
                else {
                    throw new Exception("Cliente Não existente : " + valoresExcel.IdCliente);
                }
            }

            
            
            #endregion
        


            #region Verifica DataDia
            if (DateTime.Compare(valoresExcel.DataOperacao, cliente.DataDia.Value) < 0)
            {
                throw new Exception("Cliente: " + cliente.IdCliente.Value + " - Data: " + valoresExcel.DataOperacao.ToString("dd/MM/yyyy") + " menor que a data dia do Cliente: " + cliente.DataDia.Value.ToString("dd/MM/yyyy"));
            }

            // Se Status está Fechado
            if (cliente.Status == (byte)StatusCliente.Divulgado)
            {
                // Pode importar somente depois da DataDia
                if (valoresExcel.DataOperacao == cliente.DataDia.Value)
                {
                    throw new Exception("Cliente: " + cliente.IdCliente.Value + " - Para Clientes Com Status Fechado, Data: " + valoresExcel.DataOperacao.ToString("dd/MM/yyyy") + " deve ser maior que data dia do Cliente: " + cliente.DataDia.Value.ToString("dd/MM/yyyy"));
                }
            }
            #endregion

            #region Testa Titulo 
            TituloRendaFixa titulo = new TituloRendaFixa();

            if (!titulo.LoadByPrimaryKey(valoresExcel.IdTitulo)) {
                throw new Exception("Titulo Não existente : " + valoresExcel.IdTitulo);
            }

            if (tipo == (int)TipoOperacaoTitulo.ExercicioOpcao)
                if (titulo.DataInicioExercicio > valoresExcel.DataOperacao)
                    throw new Exception("A data de operação de exercício deve ser maior ou igual à data mínima cadastrada no título!!!");
            #endregion

            #region testa contraparte
            if (valoresExcel.IdClienteContraparte.HasValue)
            {
                Cliente clienteContraparte = new Cliente();
                if (!clienteContraparte.LoadByPrimaryKey(valoresExcel.IdClienteContraparte.Value))
                {
                    if (ignoraCarteiraInexistente)
                    {
                        continue;
                    }
                    else
                    {
                        throw new Exception("Cliente Não existente : " + valoresExcel.IdClienteContraparte);
                    }
                }
            }

            if (valoresExcel.IdAgenteContraparte.HasValue)
            {
                AgenteMercado agenteMercado = new AgenteMercado();
                if (!agenteMercado.LoadByPrimaryKey(valoresExcel.IdAgenteContraparte.Value))
                {
                    throw new Exception("Agente não existente: " + valoresExcel.IdAgenteContraparte);
                }
            }
            #endregion

            #region OperacaoRendaFixa
            OperacaoRendaFixa operacao = new OperacaoRendaFixa();
            int tipoPapel = titulo.UpToPapelRendaFixaByIdPapel.TipoPapel.Value;

            // Valores vindos do arquivo
            operacao.IdCliente = valoresExcel.IdCliente;
            operacao.IdTitulo = valoresExcel.IdTitulo;
            operacao.DataOperacao = valoresExcel.DataOriginal;
            operacao.DataRegistro = valoresExcel.DataOperacao;
            operacao.DataLiquidacao = valoresExcel.DataLiquidacao.HasValue ? valoresExcel.DataLiquidacao.Value : valoresExcel.DataOperacao;
            operacao.OperacaoTermo = valoresExcel.OperacaoTermo != null  ? valoresExcel.OperacaoTermo : "N";            

            operacao.TipoOperacao = (byte)tipo;

            if (valoresExcel.Quantidade < 0)
            {
                throw new Exception("Quantidade " + valoresExcel.Quantidade.ToString() + " não pode ser negativa.");
            }

            operacao.Quantidade = valoresExcel.Quantidade;
            operacao.PUOperacao = valoresExcel.PuOperacao;

            operacao.Valor = valoresExcel.Valor;
            operacao.TaxaOperacao = valoresExcel.TaxaOperacao;

            if (tipo == (int)TipoOperacaoTitulo.CompraRevenda || tipo == (int)TipoOperacaoTitulo.VendaRecompra) 
            {
                operacao.DataVolta = valoresExcel.DataVolta;
                operacao.TaxaVolta = valoresExcel.TaxaVolta;                
                operacao.PUVolta = valoresExcel.PuVolta;
                operacao.ValorVolta = valoresExcel.ValorVolta;
                operacao.IdIndiceVolta = valoresExcel.IdIndiceVolta;                
            }

            //            
            operacao.Fonte = (byte)FonteOperacaoTitulo.Manual;
            //

            if(String.IsNullOrEmpty(valoresExcel.Categoria)) {
                operacao.TipoNegociacao = (byte)TipoNegociacaoTitulo.Negociacao;
            }
            else {
                int tipoNegociacao = (int)Enum.Parse(typeof(TipoNegociacaoTitulo), valoresExcel.Categoria);
                operacao.TipoNegociacao = Convert.ToByte(tipoNegociacao);
            }
          


            operacao.Rendimento = 0;
            operacao.ValorIR = 0;
            operacao.ValorIOF = 0;
            //

            operacao.IdAgenteCorretora = null;
            if (valoresExcel.IdAgenteCorretora.HasValue)
            {
                AgenteMercado agenteMercado = new AgenteMercado();
                int idAgenteMercado = agenteMercado.BuscaIdAgenteMercadoBovespa(Convert.ToInt32(valoresExcel.IdAgenteCorretora));

                operacao.IdAgenteCorretora = idAgenteMercado;
            }

            operacao.IdAgenteCustodia = null;
            if (!string.IsNullOrEmpty(valoresExcel.IdAgenteCustodia))
            {
                AgenteMercado agenteMercado = new AgenteMercado();
                int idAgenteMercado = agenteMercado.BuscaIdAgenteMercadoBovespa(Convert.ToInt32(valoresExcel.IdAgenteCustodia));

                operacao.IdAgenteCustodia = idAgenteMercado;
            }

            if (!valoresExcel.IdLiquidacao.HasValue)
            {

                if (String.IsNullOrEmpty(valoresExcel.Clearing))
                {
                    operacao.IdLiquidacao = (byte)LiquidacaoTitulo.NaoInformado;
                }
                else
                {
                    int idliquidacao = (int)Enum.Parse(typeof(LiquidacaoTitulo), valoresExcel.Clearing);
                    operacao.IdLiquidacao = Convert.ToByte(idliquidacao);
                }
            }
            else
            {
                operacao.IdLiquidacao = (byte?)valoresExcel.IdLiquidacao;
            }

           operacao.IdLiquidacao = (tipoPapel == (int)TipoPapelTitulo.Privado ? (byte)ClearingFixo.Cetip : (byte)ClearingFixo.Selic);
           if (!string.IsNullOrEmpty(valoresExcel.Clearing))
            {
                Clearing clearing = new Clearing();
                clearing.Query.Where(clearing.Query.Codigo.ToUpper().Equal(valoresExcel.Clearing.ToUpper()));

                if (clearing.Query.Load())
                    operacao.IdLiquidacao = Convert.ToByte(clearing.IdClearing.Value);
                else
                    throw new Exception("Clearing não cadastrada: " + valoresExcel.LocalNegociacao);
            }

            operacao.IdCustodia = (tipoPapel == (int)TipoPapelTitulo.Privado ? (byte)LocalCustodiaFixo.Cetip : (byte)LocalCustodiaFixo.Selic);
            if (!string.IsNullOrEmpty(valoresExcel.Custodia))
            {
                LocalCustodia localCustodia = new LocalCustodia();
                localCustodia.Query.Where(localCustodia.Query.Codigo.ToUpper().Equal(valoresExcel.Custodia.ToUpper()));

                if (localCustodia.Query.Load())
                    operacao.IdCustodia = Convert.ToByte(localCustodia.IdLocalCustodia.Value);
                else
                    throw new Exception("Local de Custódia não cadastrado: " + valoresExcel.Custodia);
            }

            operacao.IdLocalNegociacao = (int)Financial.Common.Enums.LocalNegociacaoFixo.Brasil;
            if (!string.IsNullOrEmpty(valoresExcel.LocalNegociacao))
            {
                LocalNegociacao localNegociacao = new LocalNegociacao();
                localNegociacao.Query.Where(localNegociacao.Query.Codigo.ToUpper().Equal(valoresExcel.LocalNegociacao.ToUpper()));

                if (localNegociacao.Query.Load())
                    operacao.IdLocalNegociacao = localNegociacao.IdLocalNegociacao.Value;
                else
                    throw new Exception("Local de Negociação não cadastrado: " + valoresExcel.LocalNegociacao);
            }

            operacao.ValorCorretagem = valoresExcel.ValorCorretagem.HasValue ? valoresExcel.ValorCorretagem.Value : 0;
            operacao.Emolumento = valoresExcel.Emolumento.HasValue ? valoresExcel.Emolumento.Value : 0;
            operacao.ValorISS = valoresExcel.ValorISS.HasValue ? valoresExcel.ValorISS.Value : 0;
            operacao.IsIPO = valoresExcel.IsIPO == "S" ? "S" : "N";
            operacao.IdAgenteContraParte = valoresExcel.IdAgenteContraparte;
            operacao.IdCarteiraContraparte = valoresExcel.IdClienteContraparte;

			operacao.IdCategoriaMovimentacao = valoresExcel.IdCategoriaMovimentacao;
			operacao.IdOperacaoExterna = valoresExcel.IdOperacaoResgatada.HasValue ? valoresExcel.IdOperacaoResgatada.Value : 0;			

            if (valoresExcel.NumeroNota.HasValue)
            {
                operacao.NumeroNota = valoresExcel.NumeroNota.Value;
            }

            #region Verifica se Existe Trader
            Trader trader = new Trader();
            if (!String.IsNullOrEmpty(valoresExcel.Trader))
            {
                trader.Query.Select(trader.Query.IdTrader)
                             .Where(trader.Query.Nome == valoresExcel.Trader.Trim());
                //
                if (!trader.Query.Load())
                {
                    throw new Exception("Trader Não existente: " + valoresExcel.Trader.Trim());
                }
            }
            operacao.IdTrader = trader.IdTrader;
            #endregion

            #region Testa AgenteMercado
            if (valoresExcel.CodigoBovespa.HasValue)
            {
                AgenteMercado agenteMercado = new AgenteMercado();
                int? idAgenteMercado = null;
                try
                {
                    idAgenteMercado = (int)agenteMercado.BuscaIdAgenteMercadoBovespa(valoresExcel.CodigoBovespa.Value);
                }
                catch (IdAgenteNaoCadastradoException e)
                {
                    throw new IdAgenteNaoCadastradoException(e.Message);
                }

                operacao.IdAgenteCorretora = idAgenteMercado.Value;
            }            
            
            #endregion

            #region Operação retroativa - Verifica se deve marcar o cliente para efetuar o reprocessamento
            if (DateTime.Compare(cliente.DataDia.Value, operacao.DataRegistro.Value) > 0)
            {
                BoletoRetroativo boletoRetroativo = new BoletoRetroativo();
                if (boletoRetroativo.LoadByPrimaryKey(valoresExcel.IdCliente))
                {
                    if (DateTime.Compare(boletoRetroativo.DataBoleto.Value, operacao.DataRegistro.Value) > 0)
                    {
                        boletoRetroativo.DataBoleto = operacao.DataRegistro.Value;
                    }
                }
                else
                {
                    boletoRetroativo.IdCliente = valoresExcel.IdCliente;
                    boletoRetroativo.DataBoleto = operacao.DataRegistro.Value;
                    boletoRetroativo.TipoMercado = (int)TipoMercado.RendaFixa;
                }

                boletoRetroativo.Save();
            }
            #endregion

            idCarteiraPosicao = 0;
            Carteira carteira = new Carteira();

            if (!carteira.LoadByPrimaryKey(operacao.IdCliente.Value))
            {
                throw new Exception("Carteira " + operacao.IdCliente.Value + " não encontrada");
            }
            if (carteira.ProcAutomatico == "S")
            {
                idCarteiraPosicao = carteira.IdCarteira.Value;
            }

            #region operação espelho
            if (valoresExcel.IdClienteContraparte.HasValue || idCarteiraPosicao !=0 )
            {  
                bool isCarteiraContraparte = false;                
                quantidadeCarteiraIntermediacao = 0;    

                if (idCarteiraPosicao == 0 ) 
                {
                    Carteira carteiraContraParte = new Carteira();

                    if (!carteiraContraParte.LoadByPrimaryKey(Convert.ToInt32(valoresExcel.IdClienteContraparte.Value)))
                    {
                        throw new Exception("Carteira " + valoresExcel.IdClienteContraparte.Value + " não encontrada");
                    }

                    if (carteiraContraParte.ProcAutomatico == "S")
                    {
                        idCarteiraPosicao = carteiraContraParte.IdCarteira.Value;
                        isCarteiraContraparte = true;
                    }
                }
                string chave = idCarteiraPosicao.ToString() + "#" + operacao.IdTitulo.ToString();
                if ( !dicQuantidadePosicao.ContainsKey(chave) && idCarteiraPosicao != 0)
                {
                    //primeira vez... processa o dia para montar posicao
                    ProcessamentoAutomatico(idCarteiraPosicao);

                    //recupera posição 
                    PosicaoRendaFixa posicaoRendaFixa = new PosicaoRendaFixa();
                    PosicaoRendaFixaQuery posicaoRendaFixaQuery = new PosicaoRendaFixaQuery("P");
                    posicaoRendaFixaQuery.Select(posicaoRendaFixaQuery.Quantidade.Sum());
                    posicaoRendaFixaQuery.Where(posicaoRendaFixaQuery.IdCliente.Equal(idCarteiraPosicao),
                                                posicaoRendaFixaQuery.IdTitulo.Equal(operacao.IdTitulo.Value));

                    if (posicaoRendaFixa.Load(posicaoRendaFixaQuery))
                    {
                        if (posicaoRendaFixa.Quantidade.HasValue)
                        {
                            quantidadeCarteiraIntermediacao = posicaoRendaFixa.Quantidade.Value;
                        }                        
                    }
                    dicQuantidadePosicao.Add(chave, quantidadeCarteiraIntermediacao);
                }
                if (dicQuantidadePosicao.ContainsKey(chave))
                {
                    decimal qtd = dicQuantidadePosicao[chave];
                    if (EhCompra(operacao.TipoOperacao.ToString()))
                    {
                        if (isCarteiraContraparte)
                        {                            
                            qtd -= operacao.Quantidade.Value;
                        }
                        else
                        {
                            qtd += operacao.Quantidade.Value;
                        }
                    }
                    else
                    {
                        if (isCarteiraContraparte)
                        {
                            qtd += operacao.Quantidade.Value;
                        }
                        else
                        {
                           qtd -= operacao.Quantidade.Value;
                        }
                    }
                    dicQuantidadePosicao[chave] = qtd;                   
                }
            }

            if (valoresExcel.IdClienteContraparte.HasValue) 
            {
                OperacaoRendaFixa operacaoEspelho = new OperacaoRendaFixa();
                operacaoEspelho.IdCliente = Convert.ToInt32(valoresExcel.IdClienteContraparte.Value);
                operacaoEspelho.IdTitulo = operacao.IdTitulo;
                operacaoEspelho.IdPosicaoResgatada = operacao.IdPosicaoResgatada;
                operacaoEspelho.IdOperacaoResgatada = operacao.IdOperacaoResgatada;
                operacaoEspelho.DataRegistro = operacao.DataRegistro;
                operacaoEspelho.TaxaOperacao = operacao.TaxaOperacao;
                operacaoEspelho.DataOperacao = operacao.DataOperacao;
                operacaoEspelho.DataLiquidacao = operacao.DataLiquidacao;
                operacaoEspelho.TipoOperacao = TipoOperacaoContraParte(operacao.TipoOperacao);
                operacaoEspelho.DataVolta = operacao.DataVolta;
                operacaoEspelho.TipoNegociacao = operacao.TipoNegociacao;
                operacaoEspelho.IdIndiceVolta = operacao.IdIndiceVolta;
                operacaoEspelho.Quantidade = operacao.Quantidade;
                operacaoEspelho.TaxaVolta = operacao.TaxaVolta;
                operacaoEspelho.PUOperacao = operacao.PUOperacao;
                operacaoEspelho.PUVolta = operacao.PUVolta;
                operacaoEspelho.Valor = operacao.Valor;
                operacaoEspelho.ValorVolta = operacao.ValorVolta;
                operacaoEspelho.ValorCorretagem = operacao.ValorCorretagem;
                operacaoEspelho.Observacao = operacao.Observacao;
                operacaoEspelho.Fonte = operacao.Fonte;
                operacaoEspelho.IdCustodia = operacao.IdCustodia;
                operacaoEspelho.IdLiquidacao = operacao.IdLiquidacao;
                operacaoEspelho.IsIPO = operacao.IsIPO;
                operacaoEspelho.OperacaoTermo = operacao.OperacaoTermo;
                operacaoEspelho.IdOperacaoEspelho = operacao.IdOperacao;                
                operacaoEspelho.IdCarteiraContraparte = operacao.IdCliente;                

                operacao.IdCarteiraContraparte = operacaoEspelho.IdCliente;
                operacao.IdOperacaoEspelho = operacaoEspelho.IdOperacao;

                operacaoRendaFixaCollection.AttachEntity(operacaoEspelho);
               
            }
            #endregion 

            //          
            // Attach the object
            operacaoRendaFixaCollection.AttachEntity(operacao);  

            #endregion
            

        }

        foreach (KeyValuePair<string, decimal> dicQtd in dicQuantidadePosicao)
        {
            if (dicQtd.Value < 0)
            {
                throw new Exception("A quantidade movimentada excede o disponível na Carteira#Titulo " +  dicQtd.Key);
                return;
            }            
        }       


        // Salva Operações presentes no Excel                                
        operacaoRendaFixaCollection.Save();


        
        //adicionando os clientes
        List<int> idClienteLista = this.valoresExcelOperacaoRendaFixa.ConvertAll(
            new Converter<ValoresExcelOperacaoRendaFixa, int>(
                delegate(ValoresExcelOperacaoRendaFixa valor) 
                {
                    return valor.IdCliente;
                }
            )
        );

        //adicionando os clientes contraparte
        idClienteLista.AddRange(
            this.valoresExcelOperacaoRendaFixa
                .FindAll(
                    new Predicate<ValoresExcelOperacaoRendaFixa>(
                        delegate(ValoresExcelOperacaoRendaFixa valor)
                        {
                            return valor.IdClienteContraparte.HasValue;
                        }
                    )
                )
                .ConvertAll(
                    new Converter<ValoresExcelOperacaoRendaFixa, int>(
                        delegate(ValoresExcelOperacaoRendaFixa valor)
                        {
                            return valor.IdClienteContraparte.Value;
                        }
                    )
                )
        );

        CarteiraQuery carteiraQuery = new CarteiraQuery("CA");
        ClienteQuery clienteQuery = new ClienteQuery("CLI");
        carteiraQuery.LeftJoin(clienteQuery).On(carteiraQuery.IdCarteira == clienteQuery.IdCliente);
        carteiraQuery.Where(carteiraQuery.IdCarteira.In(idClienteLista) && carteiraQuery.ProcAutomatico == "S");
        CarteiraCollection coll = new CarteiraCollection();
        coll.Load(carteiraQuery);

        

        Financial.Investidor.Controller.ControllerInvestidor controller = new Financial.Investidor.Controller.ControllerInvestidor();

        Financial.Investidor.Controller.ParametrosProcessamento parametros = new Financial.Investidor.Controller.ParametrosProcessamento();
        parametros.CravaCota = false;
        parametros.IgnoraOperacoes = false;
        parametros.IntegraBMF = false;
        parametros.IntegraBolsa = false;
        parametros.IntegraBTC = false;
        parametros.IntegracaoRendaFixa = 0;
        parametros.IntegraCC = false;
        parametros.MantemFuturo = false;
        parametros.ProcessaBMF = false;
        parametros.ProcessaBolsa = false;
        parametros.ProcessaCambio = false;
        parametros.ProcessaFundo = false;
        parametros.ProcessaRendaFixa = true;
        parametros.ProcessaSwap = false;
        parametros.RemuneraRF = false;
        parametros.ResetCusto = false;
        parametros.ListaTabelaInterfaceCliente = new List<int>();
        

        foreach (Carteira c in coll)
        {
            DateTime? datadia = c.GetColumn(ClienteMetadata.PropertyNames.DataDia) as DateTime?;
            if(c.IdCarteira.HasValue && datadia.HasValue)
                controller.ExecutaFechamento(c.IdCarteira.Value, datadia.Value, parametros);
                
        }

        
    }

    protected bool EhCompra(string tipoOperacao)
    {
        switch (tipoOperacao)
        {
            case "1": return true;
            case "3": return true;
            case "10": return true;
            case "20": return true;
            default: return false;
        }
    }

    protected byte? TipoOperacaoContraParte(byte? id)
    {
        switch (id)
        {
            case 1: return 2;
            case 2: return 1;
            case 3: return 4;
            case 4: return 3;
            case 6: return 1;
            case 10: return 11;
            case 11: return 10;
            case 12: return 13;
            case 13: return 12;
            case 20: return 21;
            case 21: return 20;
            default: return 0;
                break;
        }
    }

    public void ProcessamentoAutomatico(int? idCliente)
    {
        Carteira carteira = new Carteira();
        Cliente cliente = new Cliente();

        #region validações
        if (!idCliente.HasValue)
            return;

        if (!carteira.LoadByPrimaryKey(idCliente.Value))
            throw new Exception("Carteira " + idCliente.Value + " não encontrada");

        if (!cliente.LoadByPrimaryKey(idCliente.Value))
            throw new Exception("Cliente: " + idCliente.Value + " não encontrado");

        if (!cliente.DataDia.HasValue)
            throw new Exception("Cliente sem DataDia preenchida");

        //if (cliente.Status == (byte)StatusCliente.FechadoComErro)
        //    throw new Exception("Ultimo calculo do cliente possui erros favor verificar antes de prosseguir");

        if (carteira.ProcAutomatico != "S")
            return;
        #endregion

        //Financial.Investidor.Controller.ControllerInvestidor controller = new Financial.Investidor.Controller.ControllerInvestidor();
        Financial.Investidor.Controller.ParametrosProcessamento parametros = new Financial.Investidor.Controller.ParametrosProcessamento();
        parametros.CravaCota = false;
        parametros.IgnoraOperacoes = false;
        parametros.IntegraBMF = false;
        parametros.IntegraBolsa = false;
        parametros.IntegraBTC = false;
        parametros.IntegracaoRendaFixa = 0;
        parametros.IntegraCC = false;
        parametros.MantemFuturo = false;
        parametros.ProcessaBMF = false;
        parametros.ProcessaBolsa = false;
        parametros.ProcessaCambio = false;
        parametros.ProcessaFundo = false;
        parametros.ProcessaRendaFixa = true;
        parametros.ProcessaSwap = false;
        parametros.RemuneraRF = false;
        parametros.ResetCusto = false;
        parametros.ListaTabelaInterfaceCliente = new List<int>();

        //controller.ExecutaFechamento(idCliente.Value, cliente.DataDia.Value, parametros);

        Financial.Investidor.Controller.ControllerInvestidor controller = new Financial.Investidor.Controller.ControllerInvestidor();
        controller.ExecutaFechamento(idCliente.Value, cliente.DataDia.Value, parametros);

        //Financial.RendaFixa.Controller.ControllerRendaFixa controller = new Financial.RendaFixa.Controller.ControllerRendaFixa();
        //controller.ExecutaCalculoAutomatico(idCliente.Value, cliente.DataDia.Value);

    }

    /// <summary>
    /// Processa a Planilha de OperacaoRendaFixa após ter acabado o Upload
    /// </summary>
    /// <param name="e"></param>
    /// <param name="checkIgnora">Se Checado ignora os erros de importação de codigos inválidos </param>
    protected void uplOperacaoRendaFixa_FileUploadComplete(FileUploadCompleteEventArgs e, ASPxCheckBox checkIgnora) {
        e.CallbackData = "";

        #region Trata Extensão Válida
        if (!ValoresExcelUtil.isExtensaoExcel(e.UploadedFile.FileName.Trim())) {
            e.CallbackData = "Importação Operação Renda Fixa - Formato de Arquivo Inválido. Formato permitido: .xls ou .xlsx\n\n";
            return;
        }
        #endregion

        // Stream de bytes com o Conteudo do Arquivo Excel
        Stream sr = e.UploadedFile.FileContent;

        #region Processamento Arquivo Excel

        //Reseta Vetor de Dados
        this.valoresExcelOperacaoRendaFixa = new List<ValoresExcelOperacaoRendaFixa>();
        //
        try {
            // Ler Arquivo
            this.LerArquivoOperacaoRendaFixa(sr);
            // Carrega Arquivo
            this.CarregaOperacaoRendaFixa(checkIgnora.Checked);
        }
        catch (Exception e2) {
            e.CallbackData = "Importação Operação RendaFixa - " + e2.Message;
            return;
        }

        new HistoricoLog().InsereHistoricoLog(DateTime.Now, DateTime.Now,
            "Importação Planilha Operação Renda Fixa",
            HttpContext.Current.User.Identity.Name, UtilitarioWeb.GetIP(Request), "", HistoricoLogOrigem.Outros);

        #endregion
    }
}