﻿using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Financial.Web.Common;
using System.IO;
using Financial.Integracao.Excel.Util;
using Financial.Integracao.Excel;
using EntitySpaces.Interfaces;
using Financial.Common;
using System.Collections.Generic;
using Financial.Investidor.Enums;
using DevExpress.Web;
using Financial.Bolsa;
using Financial.Common.Exceptions;
using Financial.Bolsa.Enums;
using Financial.Bolsa.Exceptions;
using Financial.Investidor;
using Financial.Common.Enums;
using Financial.Web.Util;
using Financial.Security.Enums;
using Financial.Security;

public partial class ImportacaoBasePage : BasePage {

    /* Estrutura do Excel */
    private List<ValoresExcelOperacaoEmprestimoBolsa> valoresExcelOperacaoEmprestimoBolsa = new List<ValoresExcelOperacaoEmprestimoBolsa>();

    /// <summary>
    /// Leitura de um Arquivo Excel
    /// Saida: List<ValoresExcel>Com os valores Armazenados no Arquivo
    /// </summary>
    /// <param name="streamExcel">Stream de bytes de um arquivo Excel</param>
    /// <exception cref="Exception">Se ocorreu problemas</exception>
    private void LerArquivoOperacaoEmprestimoBolsa(Stream streamExcel) {
        #region Leitura do Arquivo Excel

        // Open Spreadsheet
        Bytescout.Spreadsheet.Spreadsheet document = new Bytescout.Spreadsheet.Spreadsheet();
        document.LoadFromStream(streamExcel);

        Bytescout.Spreadsheet.Worksheet workSheet = document.Workbook.Worksheets[0];
        //
        #region Confere Formato Arquivo
        string[] colunasConferencias = new string[] 
                                {"IdCliente","CdAtivoBolsa","CodigoBovespa","PontaEmprestimo","DataRegistro",
                                 "DataVencimento","TaxaOperacao","Quantidade","Pu","NumeroContrato","TipoEmprestimo",
                                 "Trader"
                                };

        bool formato = ValoresExcelUtil.isExcelFormatoValido(colunasConferencias, workSheet);

        if (!formato) {
            string mensagem = "Formato Interno do Arquivo Inválido.\n";
            mensagem += "Linha 1 do Arquivo deve ser: \n";
            for (int i = 0; i < colunasConferencias.Length; i++) {
                int coluna = i + 1;
                mensagem += "\tColuna " + coluna + ": " + colunasConferencias[i] + "\n";
            }

            document.Close();
            document.Dispose();

            throw new Exception(mensagem);
        }
        #endregion

        int index = 0;
        // This row,column index should be changed as per your need.
        // i.e. which cell in the excel you are interesting to read.
        //
        /* Formato: 1)IdCliente         - 2)CdAtivoBolsa   - 3)CodigoBovespa - 4)PontaEmprestimo   - 5)DataRegistro
         *          6)DataVencimento    - 7)TaxaOperacao   - 8)Quantidade    - 9)Pu                - 10)NumeroContrato
         *          11)TipoEmprestimo   - 12)Trader
         */

        int linha = 1;
        int coluna1 = 0, coluna2 = 1, coluna3 = 2,
           coluna4 = 3, coluna5 = 4, coluna6 = 5,
           coluna7 = 6, coluna8 = 7, coluna9 = 8,
           coluna10 = 9, coluna11 = 10, coluna12 = 11;

        //            
        try {
            // Enquanto idCliente tiver valor
            while (workSheet.Cell(linha, coluna1).Value != null) {
                ValoresExcelOperacaoEmprestimoBolsa item = new ValoresExcelOperacaoEmprestimoBolsa();
                
                try {
                    item.IdCliente = workSheet.Cell(linha, coluna1).ValueAsInteger;
                }
                catch (Exception ex) {
                    throw new Exception(ex.Message + " - IdCliente: linha " + (linha + 1));
                }

                if (workSheet.Cell(linha, coluna2).Value == null)
                    throw new Exception("CdAtivoBolsa não informado: linha " + (linha + 1));
                try {
                    item.CdAtivoBolsa = workSheet.Cell(linha, coluna2).ValueAsString;
                }
                catch (Exception ex) {
                    throw new Exception(ex.Message + " - CdAtivoBolsa: linha " + (linha + 1));
                }

                if (workSheet.Cell(linha, coluna3).Value == null)
                    throw new Exception("CodigoBovespa não informado: linha " + (linha + 1));
                try {
                    item.CodigoBovespa = workSheet.Cell(linha, coluna3).ValueAsInteger;
                }
                catch (Exception ex) {
                    throw new Exception(ex.Message + " - CodigoBovespa: linha " + (linha + 1));
                }

                if (workSheet.Cell(linha, coluna4).Value == null)
                    throw new Exception("PontaEmprestimo não informada: linha " + (linha + 1));
                try {
                    int pontaEmprestimo = workSheet.Cell(linha, coluna4).ValueAsInteger;
                    item.PontaEmprestimo = (PontaEmprestimoBolsa)pontaEmprestimo;
                }
                catch (Exception ex) {
                    throw new Exception(ex.Message + " - pontaEmprestimo: linha " + (linha + 1));
                }
                
                if (workSheet.Cell(linha, coluna5).Value == null)
                    throw new Exception("DataRegistro não informada: linha " + (linha + 1));
                try {
                    item.DataRegistro = workSheet.Cell(linha, coluna5).ValueAsDateTime;
                }
                catch (Exception ex) {
                    throw new Exception(ex.Message + " - DataRegistro: linha " + (linha + 1));
                }

                if (workSheet.Cell(linha, coluna6).Value == null)
                    throw new Exception("DataVencimento não informada: linha " + (linha + 1));
                try {
                    item.DataVencimento = workSheet.Cell(linha, coluna6).ValueAsDateTime;
                }
                catch (Exception ex) {
                    throw new Exception(ex.Message + " - DataVencimento: linha " + (linha + 1));
                }

                if (workSheet.Cell(linha, coluna7).Value == null)
                    throw new Exception("TaxaOperacao não informado: linha " + (linha + 1));
                try {
                    item.TaxaOperacao = Convert.ToDecimal(workSheet.Cell(linha, coluna7).Value);
                }
                catch (Exception ex) {
                    throw new Exception(ex.Message + " - TaxaOperacao: linha " + (linha + 1));
                }

                if (workSheet.Cell(linha, coluna8).Value == null)
                    throw new Exception("Quantidade não informada: linha " + (linha + 1));
                try {
                    item.Quantidade = Convert.ToDecimal(workSheet.Cell(linha, coluna8).Value);
                }
                catch (Exception ex) {
                    throw new Exception(ex.Message + " - Quantidade: linha " + (linha + 1));
                }

                if (workSheet.Cell(linha, coluna9).Value == null)
                    throw new Exception("Pu não informado: linha " + (linha + 1));
                try {
                    item.Pu = Convert.ToDecimal(workSheet.Cell(linha, coluna9).Value);
                }
                catch (Exception ex) {
                    throw new Exception(ex.Message + " - Pu: linha " + (linha + 1));
                }

                item.NumeroContrato = 0;
                if (workSheet.Cell(linha, coluna10).Value != null) {
                    try {
                        item.NumeroContrato = workSheet.Cell(linha, coluna10).ValueAsInteger;
                    }
                    catch (Exception ex) {
                        throw new Exception(ex.Message + " - NumeroContrato: linha " + (linha + 1));
                    }
                }

                if (workSheet.Cell(linha, coluna11).Value == null)
                    throw new Exception("tipoEmprestimo não informado: linha " + (linha + 1));
                try {
                    int tipoEmprestimo = workSheet.Cell(linha, coluna11).ValueAsInteger;
                    item.TipoEmprestimo = (TipoEmprestimoBolsa)tipoEmprestimo;
                }
                catch (Exception ex) {
                    throw new Exception(ex.Message + " - tipoEmprestimo: linha " + (linha + 1));
                }

                if (workSheet.Cell(linha, coluna12).Value != null) {
                    try {
                        item.Trader = workSheet.Cell(linha, coluna12).ValueAsString;
                    }
                    catch (Exception ex) {
                        throw new Exception(ex.Message + " - Trader: linha " + (linha + 1));
                    }
                }
                                  
                this.valoresExcelOperacaoEmprestimoBolsa.Add(item);
                
                index++;
                linha = 1 + index;
                
                Console.WriteLine("{0},{1},{2},{3},{4},{5},{6},{7},{8},{9}",
                    item.IdCliente, item.CdAtivoBolsa, item.CodigoBovespa, item.PontaEmprestimo, item.DataRegistro.ToString("dd/MM/yyyy"),
                    item.DataVencimento.ToString("dd/MM/yyyy"), item.TaxaOperacao, item.Quantidade, item.Pu,
                    item.NumeroContrato, item.TipoEmprestimo);
            }
        }
        catch (Exception ex) {
            document.Close();
            document.Dispose();

            throw new Exception(ex.Message);
        }
        #endregion

        document.Close();
        document.Dispose();
    }

    /// <summary>
    /// Gera e Carrega as Operações Empréstimo Bolsa de Acordo com o objeto List<ValoresExcelOperacaoEmprestimoBolsa>
    /// </summary>
    /// <param name="ignoraCarteiraInexistente">ignora cliente e carteira inexistentes</param>
    private void CarregaOperacaoEmprestimoBolsa(bool ignoraCarteiraInexistente) {
        OperacaoEmprestimoBolsaCollection operacaoEmprestimoBolsaCollection = new OperacaoEmprestimoBolsaCollection();

        for (int i = 0; i < this.valoresExcelOperacaoEmprestimoBolsa.Count; i++) {
            ValoresExcelOperacaoEmprestimoBolsa valoresExcel = this.valoresExcelOperacaoEmprestimoBolsa[i];

            #region Verifica se Existe Cliente
            Cliente cliente = new Cliente();
            List<esQueryItem> campos = new List<esQueryItem>();
            campos.Add(cliente.Query.IdCliente);
            campos.Add(cliente.Query.DataDia);
            campos.Add(cliente.Query.Status);

            if (!cliente.LoadByPrimaryKey(campos, valoresExcel.IdCliente)) {
                if (!cliente.LoadByPrimaryKey(campos, valoresExcel.IdCliente)) {
                    if (ignoraCarteiraInexistente) {
                        continue;
                    }
                    else {
                        throw new Exception("Cliente Não existente: " + valoresExcel.IdCliente);
                    }
                }
            }
            #endregion

            #region Verifica se Existe AtivoBolsa
            AtivoBolsa ativoBolsa = new AtivoBolsa();
            if (!ativoBolsa.LoadByPrimaryKey(valoresExcel.CdAtivoBolsa.Trim())) {
                throw new Exception("AtivoBolsa Não existente: " + valoresExcel.CdAtivoBolsa.Trim());
            }
            #endregion

            #region Verifica se Existe Corretora
            AgenteMercado agenteMercado = new AgenteMercado();
            int? idAgenteMercado = null;
            try {
                idAgenteMercado = (int)agenteMercado.BuscaIdAgenteMercadoBovespa(valoresExcel.CodigoBovespa);
            }
            catch (IdAgenteNaoCadastradoException e) {
                throw new IdAgenteNaoCadastradoException(e.Message);
            }
            #endregion

            #region Verifica DataDia
            if (DateTime.Compare(valoresExcel.DataRegistro, cliente.DataDia.Value) < 0) 
            {
                throw new Exception("Data Registro: " + valoresExcel.DataRegistro.ToString("dd/MM/yyyy") + " menor que a data dia do Cliente: " + cliente.DataDia.Value.ToString("dd/MM/yyyy"));
            }

            // Se Status está Fechado
            if (cliente.Status == (byte)StatusCliente.Divulgado)
            {
                // Pode importar somente depois da DataDia
                if (valoresExcel.DataRegistro == cliente.DataDia.Value)
                {
                    throw new Exception("Cliente: " + cliente.IdCliente.Value + " - Para Clientes Com Status Fechado, Data Registro: " + valoresExcel.DataRegistro.ToString("dd/MM/yyyy") + " deve ser maior que data dia do Cliente: " + cliente.DataDia.Value.ToString("dd/MM/yyyy"));
                }
            }
            #endregion

            #region Verifica se PontaEmprestimo é Consistente
            if (valoresExcel.PontaEmprestimo != PontaEmprestimoBolsa.Doador &&
                valoresExcel.PontaEmprestimo != PontaEmprestimoBolsa.Tomador) {
                throw new Exception("Ponta Empréstimo deve ser 1=Doador ou 2=Tomador");
            }
            #endregion

            #region Verifica se TipoEmprestimo é Consistente
            if (valoresExcel.TipoEmprestimo != TipoEmprestimoBolsa.Compulsorio &&
                valoresExcel.TipoEmprestimo != TipoEmprestimoBolsa.Voluntario) {
                throw new Exception("Tipo Empréstimo deve ser 1=Voluntario ou 2=Compulsorio");
            }
            #endregion

            #region Verifica se Existe Trader
            Trader trader = new Trader();
            if (!String.IsNullOrEmpty(valoresExcel.Trader)) {
                trader.Query.Select(trader.Query.IdTrader)
                             .Where(trader.Query.Nome == valoresExcel.Trader.Trim());
                //
                if (!trader.Query.Load()) {
                    throw new Exception("Trader Não existente: " + valoresExcel.Trader.Trim());
                }
            }
            #endregion

            #region OperacaoEmprestimoBolsa
            OperacaoEmprestimoBolsa operacaoEmprestimoBolsa = operacaoEmprestimoBolsaCollection.AddNew();

            // Dados do Arquivo
            operacaoEmprestimoBolsa.IdCliente = valoresExcel.IdCliente;
            operacaoEmprestimoBolsa.CdAtivoBolsa = valoresExcel.CdAtivoBolsa.ToUpper();
            operacaoEmprestimoBolsa.IdAgente = idAgenteMercado;
            operacaoEmprestimoBolsa.PontaEmprestimo = (byte)valoresExcel.PontaEmprestimo;
            operacaoEmprestimoBolsa.DataRegistro = valoresExcel.DataRegistro;
            operacaoEmprestimoBolsa.DataVencimento = valoresExcel.DataVencimento;
            operacaoEmprestimoBolsa.TaxaOperacao = valoresExcel.TaxaOperacao;
            operacaoEmprestimoBolsa.Quantidade = valoresExcel.Quantidade;
            operacaoEmprestimoBolsa.Pu = valoresExcel.Pu;
            operacaoEmprestimoBolsa.NumeroContrato = valoresExcel.NumeroContrato;
            operacaoEmprestimoBolsa.TipoEmprestimo = (byte)valoresExcel.TipoEmprestimo;

            if (!String.IsNullOrEmpty(valoresExcel.Trader)) {
                operacaoEmprestimoBolsa.IdTrader = trader.IdTrader.Value;
            }

            // Valores Forçados

            #region Seta ValorMercado
            FatorCotacaoBolsa fatorCotacaoBolsa = new FatorCotacaoBolsa();
            if (!fatorCotacaoBolsa.BuscaFatorCotacaoBolsa(valoresExcel.CdAtivoBolsa.ToUpper(), cliente.DataDia.Value)) {
                throw new FatorCotacaoNaoCadastradoException("FatorCotacaoBolsa Não existente : " + cliente.DataDia.Value.ToString("d") + ", " + valoresExcel.CdAtivoBolsa.ToUpper());
            }
            operacaoEmprestimoBolsa.Valor = ((valoresExcel.Quantidade * valoresExcel.Pu) / fatorCotacaoBolsa.Fator.Value);
            //
            #endregion

            operacaoEmprestimoBolsa.TaxaComissao = 0;
            operacaoEmprestimoBolsa.Fonte = (byte)FonteEmprestimoBolsa.Manual;
            operacaoEmprestimoBolsa.IdLocal = LocalFeriadoFixo.Bovespa;
            //
            #endregion
        }

        // Salva Posicões presentes no Excel
        operacaoEmprestimoBolsaCollection.Save();
    }

    /// <summary>
    /// Processa a Planilha de OperacaoEmprestimoBolsa após ter acabado o Upload
    /// </summary>
    /// <param name="e"></param>
    /// <param name="checkIgnora">Se Checado ignora os erros de importação de codigos inválidos </param>    
    protected void uplOperacaoEmprestimoBolsa_FileUploadComplete(FileUploadCompleteEventArgs e, ASPxCheckBox checkIgnora) {
        e.CallbackData = "";

        #region Trata Extensão Válida
        if (!ValoresExcelUtil.isExtensaoExcel(e.UploadedFile.FileName.Trim())) {
            e.CallbackData = "Importação Operação Empréstimo Bolsa - Formato de Arquivo Inválido. Formato permitido: .xls ou .xlsx\n\n";
            return;
        }
        #endregion

        // Stream de bytes com o Conteudo do Arquivo Excel
        Stream sr = e.UploadedFile.FileContent;

        #region Processamento Arquivo Excel

        //Reseta Vetor de Dados
        this.valoresExcelOperacaoEmprestimoBolsa = new List<ValoresExcelOperacaoEmprestimoBolsa>();
        //
        try {
            // Ler Arquivo
            this.LerArquivoOperacaoEmprestimoBolsa(sr);
            // Carrega Arquivo
            this.CarregaOperacaoEmprestimoBolsa(checkIgnora.Checked);
        }
        catch (Exception e2) {
            e.CallbackData = "Importação Operação Empréstimo Bolsa - " + e2.Message;
            return;
        }

        new HistoricoLog().InsereHistoricoLog(DateTime.Now, DateTime.Now,
            "Importação Planilha Operação Empréstimo Bolsa",
            HttpContext.Current.User.Identity.Name, UtilitarioWeb.GetIP(Request), "", HistoricoLogOrigem.Outros);

        #endregion
    }
}