﻿using System;
using System.Collections.Generic;
using System.IO;
using Financial.Integracao.Excel;
using Financial.Integracao.Excel.Util;
using Financial.RendaFixa;
using Financial.Web.Common;
using EntitySpaces.Interfaces;
using DevExpress.Web;
using Financial.Security;
using System.Web;
using Financial.Web.Util;
using Financial.Security.Enums;
using Financial.Util;
using Financial.Common.Enums;
using Financial.Fundo;
using Financial.Investidor;
using Financial.InvestidorCotista;
using Financial.BMF;

public partial class ImportacaoBasePage : BasePage
{
    /* Estrutura do Excel */
    private List<ValoresExcelTransferenciaBMF> valoresExcelTransferenciaBMF = new List<ValoresExcelTransferenciaBMF>();

    private List<string> codigoExternoDeletarTransferenciaBMFDetalhe = new List<string>();
    /// <summary>
    /// Leitura de um arquivo Excel
    /// Saida: List<ValoresExcel> com os valores armazenados no arquivo
    /// </summary>
    /// <param name="streamExcel">Stream de bytes de um arquivo Excel</param>
    /// <exception cref="Exception">Se ocorreu problemas</exception>
    private void LerArquivoTransferenciaBMF(Stream streamExcel)
    {

        #region Leitura do Arquivo Excel

        // Open Spreadsheet
        Bytescout.Spreadsheet.Spreadsheet document = new Bytescout.Spreadsheet.Spreadsheet();
        document.LoadFromStream(streamExcel);

        Bytescout.Spreadsheet.Worksheet workSheet = document.Workbook.Worksheets[0];

        //
        #region Confere Formato Arquivo
        string[] colunasConferencias = new string[] { "IdCliente", "CdAtivoBMF", "Serie", "IdAgenteOrigem", "IdAgenteDestino", "Data", "Quantidade", "Pu" };

        bool formato = ValoresExcelUtil.isExcelFormatoValido(colunasConferencias, workSheet);

        if (!formato)
        {
            string mensagem = "Formato Interno do Arquivo Inválido.\n";
            mensagem += "Linha 1 do Arquivo deve ser: \n";
            for (int i = 0; i < colunasConferencias.Length; i++)
            {
                int coluna = i + 1;
                mensagem += "\tColuna " + coluna + ": " + colunasConferencias[i] + "\n";
            }

            document.Close();
            document.Dispose();

            throw new Exception(mensagem);
        }
        #endregion

        //
        int index = 0;
        // This row,column index should be changed as per your need.
        // i.e. which cell in the excel you are interesting to read.
        //
        /* Formato: -0)IdCliente -1)CdAtivoBMF -2)Serie -3)IdAgenteOrigem -4)IdAgenteDestino -5)Data -6)Quantidade -7)Pu */

        int linha = 1;
        int coluna0 = 0, coluna1 = 1, coluna2 = 2, coluna3 = 3, coluna4 = 4, coluna5 = 5, coluna6 = 6, coluna7 = 7;

        //            
        try
        {
            // Enquanto TipoRegistro tiver valor
            while (workSheet.Cell(linha, coluna0).Value != null)
            {
                ValoresExcelTransferenciaBMF item = new ValoresExcelTransferenciaBMF();

                #region IdCliente
                if (workSheet.Cell(linha, coluna0).Value == null)
                    throw new Exception("IdCliente não informada: linha " + (linha + 1));

                try
                {
                    item.IdCliente = workSheet.Cell(linha, coluna0).ValueAsInteger;
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message + " - IdCliente: linha " + (linha + 1));
                }
                #endregion

                #region CdAtivoBMF
                if (workSheet.Cell(linha, coluna1).Value == null)
                    throw new Exception("CdAtivoBMF não informada: linha " + (linha + 1));

                try
                {
                    item.CdAtivoBMF = workSheet.Cell(linha, coluna1).ValueAsString;
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message + " - CdAtivoBMF: linha " + (linha + 1));
                }
                #endregion

                #region Serie
                if (workSheet.Cell(linha, coluna2).Value == null)
                    throw new Exception("Serie não informada: linha " + (linha + 1));

                try
                {
                    item.Serie = workSheet.Cell(linha, coluna2).ValueAsString;
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message + " - Serie: linha " + (linha + 1));
                }
                #endregion

                #region IdAgenteOrigem
                if (workSheet.Cell(linha, coluna3).Value == null)
                    throw new Exception("IdAgenteOrigem não informada: linha " + (linha + 1));

                try
                {
                    item.IdAgenteOrigem = workSheet.Cell(linha, coluna3).ValueAsInteger;
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message + " - IdAgenteOrigem: linha " + (linha + 1));
                }
                #endregion

                #region IdAgenteDestino
                if (workSheet.Cell(linha, coluna4).Value == null)
                    throw new Exception("IdAgenteDestino não informada: linha " + (linha + 1));

                try
                {
                    item.IdAgenteDestino = workSheet.Cell(linha, coluna4).ValueAsInteger;
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message + " - IdAgenteDestino: linha " + (linha + 1));
                }
                #endregion

                #region Data
                if (workSheet.Cell(linha, coluna5).Value == null)
                    throw new Exception("Data não informada: linha " + (linha + 1));

                try
                {
                    item.Data = workSheet.Cell(linha, coluna5).ValueAsDateTime;
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message + " - Data: linha " + (linha + 1));
                }
                #endregion

                #region Quantidade
                if (workSheet.Cell(linha, coluna6).Value == null)
                    throw new Exception("Quantidade não informada: linha " + (linha + 1));
                try
                {
                    item.Quantidade = workSheet.Cell(linha, coluna6).ValueAsInteger;
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message + " - Quantidade: linha " + (linha + 1));
                }
                #endregion

                #region Pu
                if (workSheet.Cell(linha, coluna7).Value == null)
                    throw new Exception("Pu não informada: linha " + (linha + 1));
                try
                {
                    item.Pu = Convert.ToDecimal(workSheet.Cell(linha, coluna7).Value);
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message + " - Pu: linha " + (linha + 1));
                }
                #endregion

                this.valoresExcelTransferenciaBMF.Add(item);

                index++;
                linha = 1 + index;


                Console.WriteLine("{0},{1},{2},{3},{4},{5},{6},{7}", item.IdCliente, item.CdAtivoBMF, item.Serie, item.IdAgenteOrigem, item.IdAgenteDestino, item.Data, item.Quantidade, item.Pu);
            }
        }
        catch (Exception ex)
        {
            document.Close();
            document.Dispose();

            throw new Exception(ex.Message);
        }
        #endregion

        document.Close();
        document.Dispose();
    }

    /// <summary>
    /// Gera e Carrega as Operações de ComeCotas co de acordo com o objeto List<ValoresExcel>
    /// </summary>     
    private void CarregaTransferenciaBMF()
    {
        TransferenciaBMFCollection transferenciaBMFCollection = new TransferenciaBMFCollection();
        //    
        for (int i = 0; i < this.valoresExcelTransferenciaBMF.Count; i++)
        {
            ValoresExcelTransferenciaBMF valoresExcel = this.valoresExcelTransferenciaBMF[i];

            #region Verifica se cliente
            Cliente cliente = new Cliente();
            if (!cliente.LoadByPrimaryKey(valoresExcel.IdCliente))
            {
                throw new Exception("Cliente não existente: " + valoresExcel.IdCliente);
            }            
            #endregion

            #region Ativo BMF
            AtivoBMF ativo = new AtivoBMF();
            if (!ativo.LoadByPrimaryKey(valoresExcel.CdAtivoBMF, valoresExcel.Serie))
            {
                throw new Exception("Ativo não existente: " + valoresExcel.CdAtivoBMF + "-" + valoresExcel.Serie);
            }
            #endregion

            #region TransferenciaBMF            
            // Dados do Arquivo            
            TransferenciaBMF transferenciaBMF = transferenciaBMFCollection.AddNew();
            transferenciaBMF.IdCliente = valoresExcel.IdCliente;
            transferenciaBMF.CdAtivoBMF = valoresExcel.CdAtivoBMF;
            transferenciaBMF.Serie = valoresExcel.Serie;
            transferenciaBMF.IdAgenteOrigem = valoresExcel.IdAgenteOrigem;
            transferenciaBMF.IdAgenteDestino = valoresExcel.IdAgenteDestino;
            transferenciaBMF.Data = valoresExcel.Data;
            transferenciaBMF.Quantidade = valoresExcel.Quantidade;
            transferenciaBMF.Pu = valoresExcel.Pu;           
            //
            #endregion
        }

        transferenciaBMFCollection.Save();
    }

    /// <summary>
    /// Processa a Planilha de CotacaoIndice após ter acabado o Upload
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void uplTransferenciaBMF_FileUploadComplete(object sender, FileUploadCompleteEventArgs e)
    {
        e.CallbackData = "";

        #region Trata Extensão Válida
        if (!ValoresExcelUtil.isExtensaoExcel(e.UploadedFile.FileName.Trim()))
        {
            e.CallbackData = "Importação Transferência - BMF - Formato de Arquivo Inválido. Formato permitido: .xls ou .xlsx\n\n";
            return;
        }
        #endregion

        // Stream de bytes com o Conteudo do Arquivo Excel
        Stream sr = e.UploadedFile.FileContent;

        #region Processamento Arquivo Excel

        //Reseta Vetor de Dados
        this.valoresExcelTransferenciaBMF = new List<ValoresExcelTransferenciaBMF>();
        this.codigoExternoDeletarTransferenciaBMFDetalhe = new List<string>();
        //
        try
        {
            // Ler Arquivo
            this.LerArquivoTransferenciaBMF(sr);
            // Carrega Arquivo
            this.CarregaTransferenciaBMF();
        }
        catch (Exception e2)
        {
            e.CallbackData = "Importação Transferência - BMF - " + e2.Message;
            return;
        }

        new HistoricoLog().InsereHistoricoLog(DateTime.Now, DateTime.Now,
            "Importação Planilha Transferência - BMF",
            HttpContext.Current.User.Identity.Name, UtilitarioWeb.GetIP(Request), "", HistoricoLogOrigem.Outros);

        #endregion
    }
}
