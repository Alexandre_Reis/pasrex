﻿using System;
using System.Collections.Generic;
using System.IO;

using Financial.Integracao.Excel;
using Financial.Integracao.Excel.Util;
using Financial.Web.Common;
using Financial.Bolsa;

using EntitySpaces.Interfaces;

using DevExpress.Web;
using Financial.Common.Enums;
using Financial.Fundo;
using Financial.Bolsa.Enums;
using Financial.Security;
using System.Web;
using Financial.Web.Util;
using Financial.Security.Enums;

public partial class ImportacaoBasePage : BasePage {

    #region Estrutura AtivoBolsa

    /// <summary>
    /// Armazena os Valores de AtivoBolsa Bolsa presentes num arquivo Excel
    /// </summary>
    internal class ValoresExcelAtivoBolsa {
        private string cdAtivoBolsa;
        public string CdAtivoBolsa {
            get { return cdAtivoBolsa; }
            set { cdAtivoBolsa = value; }
        }

        private string especificao;
        public string Especificao {
            get { return especificao; }
            set { especificao = value; }
        }

        private string descricao;
        public string Descricao {
            get { return descricao; }
            set { descricao = value; }
        }

        private string tipoMercado;
        public string TipoMercado {
            get { return tipoMercado; }
            set { tipoMercado = value; }
        }
        
        private decimal? puExercicio;
        public decimal? PuExercicio {
            get { return puExercicio; }
            set { puExercicio = value; }
        }

        private DateTime? dataVencimento;
        public DateTime? DataVencimento {
            get { return dataVencimento; }
            set { dataVencimento = value; }
        }

        private string codigoAtivoObjeto = null;
        public string CodigoAtivoObjeto {
            get { return codigoAtivoObjeto; }
            set { codigoAtivoObjeto = value; }
        }

        private string codigoIsin = "";
        public string CodigoIsin {
            get { return codigoIsin; }
            set { codigoIsin = value; }
        }

        private int numeroSerie;
        public int NumeroSerie
        {
            get { return numeroSerie; }
            set { numeroSerie = value; }
        }
    }
    #endregion

    /* Estrutura do Excel */
    private List<ValoresExcelAtivoBolsa> valoresExcelAtivoBolsa = new List<ValoresExcelAtivoBolsa>();
    
    /// <summary>
    /// Leitura de um arquivo Excel
    /// Saida: List<ValoresExcel> com os valores armazenados no arquivo
    /// </summary>
    /// <param name="streamExcel">Stream de bytes de um arquivo Excel</param>
    /// <exception cref="Exception">Se ocorreu problemas</exception>
    private void LerArquivoAtivoBolsa(Stream streamExcel) {

        #region Leitura do Arquivo Excel

        // Open Spreadsheet
        Bytescout.Spreadsheet.Spreadsheet document = new Bytescout.Spreadsheet.Spreadsheet();
        document.LoadFromStream(streamExcel);

        Bytescout.Spreadsheet.Worksheet workSheet = document.Workbook.Worksheets[0];

        //
        #region Confere Formato Arquivo
       
        string[] colunasConferencias = new string[] { "CdAtivoBolsa", "Especificação", "Descrição",
                                                      "TipoMercado",  "PuExercicio",   "DataVencimento", 	
                                                      "CodigoAtivoObjeto","CodigoIsin","NumeroSerie"
                                                    };

        bool formato = ValoresExcelUtil.isExcelFormatoValido(colunasConferencias, workSheet);

        if (!formato) {
            string mensagem = "Formato Interno do Arquivo Inválido.\n";
            mensagem += "Linha 1 do Arquivo deve ser: \n";
            for (int i = 0; i < colunasConferencias.Length; i++) {
                int coluna = i + 1;
                mensagem += "\tColuna " + coluna + ": " + colunasConferencias[i] + "\n";
            }

            document.Close();
            document.Dispose();

            throw new Exception(mensagem);
        }
        #endregion

        //
        int index = 0;
        // This row,column index should be changed as per your need.
        // i.e. which cell in the excel you are interesting to read.
        //
        /* Formato: 1) CdAtivoBolsa      - 2) Especificação    - 3) Descrição 
         *          4) TipoMercado       - 5) PuExercicio      - 6) DataVencimento 
         *          7) CodigoAtivoObjeto - 8) CodigoIsin       - 8) NumeroSerie
         */

        int linha = 1;
        int coluna1 = 0, coluna2 = 1, coluna3 = 2;
        int coluna4 = 3, coluna5 = 4, coluna6 = 5;
        int coluna7 = 6, coluna8 = 7, coluna9 =8;

        //            
        try {
            // Enquanto Data tiver valor
            while (workSheet.Cell(linha, coluna1).Value != null) {
                ValoresExcelAtivoBolsa item = new ValoresExcelAtivoBolsa();

                try {
                    item.CdAtivoBolsa = workSheet.Cell(linha, coluna1).ValueAsString.Trim().ToUpper();                             
                }
                catch (Exception ex) {
                    throw new Exception(ex.Message + " - CdAtivoBolsa: linha " + (linha + 1));
                }

                if (workSheet.Cell(linha, coluna2).Value == null)
                    throw new Exception("Especificao não informada: linha " + (linha + 1));
                try {
                    item.Especificao = workSheet.Cell(linha, coluna2).ValueAsString.Trim().ToUpper();                
                }
                catch (Exception ex) {
                    throw new Exception(ex.Message + " - Especificao: linha " + (linha + 1));
                }

                try {
                    item.Descricao = workSheet.Cell(linha, coluna3).ValueAsString.Trim().ToUpper();
                }
                catch (Exception ex) {
                    throw new Exception(ex.Message + " - Descricao: linha " + (linha + 1));
                }

                if (workSheet.Cell(linha, coluna4).Value == null)
                    throw new Exception("TipoMercado não informado: linha " + (linha + 1));
                try {
                    item.TipoMercado = workSheet.Cell(linha, coluna4).ValueAsString.Trim().ToUpper();
                }
                catch (Exception ex) {
                    throw new Exception(ex.Message + " - TipoMercado: linha " + (linha + 1));
                }          

                if (workSheet.Cell(linha, coluna5).Value != null) {
                    try {
                        item.PuExercicio = Convert.ToDecimal(workSheet.Cell(linha, coluna5).Value);
                    }
                    catch (Exception ex) {
                        throw new Exception(ex.Message + " - PuExercicio: linha " + (linha + 1));
                    }
                }

                if (workSheet.Cell(linha, coluna6).Value != null) {
                    try {
                        item.DataVencimento  = workSheet.Cell(linha, coluna6).ValueAsDateTime;
                    }
                    catch (Exception ex) {
                        throw new Exception(ex.Message + " - DataVencimento: linha " + (linha + 1));
                    }
                }
                
                if (workSheet.Cell(linha, coluna7).Value != null) {
                    try {
                        item.CodigoAtivoObjeto = workSheet.Cell(linha, coluna7).ValueAsString.Trim().ToUpper();
                    }
                    catch (Exception ex) {
                        throw new Exception(ex.Message + " - CodigoAtivoObjeto: linha " + (linha + 1));
                    }
                }

                if (workSheet.Cell(linha, coluna8).Value != null) {
                    try {
                        item.CodigoIsin = workSheet.Cell(linha, coluna8).ValueAsString.Trim().ToUpper();
                    }
                    catch (Exception ex) {
                        throw new Exception(ex.Message + " - CodigoIsin: linha " + (linha + 1));
                    }
                }

                if (workSheet.Cell(linha, coluna9).Value == null)
                    throw new Exception("NumeroSerie não informada: linha " + (linha + 1));
                try
                {
                    item.NumeroSerie = workSheet.Cell(linha, coluna9).ValueAsInteger;
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message + " - NumeroSerie: linha " + (linha + 1));
                }
                
                this.valoresExcelAtivoBolsa.Add(item);
                //
                index++;
                linha = 1 + index;
                //                
                Console.WriteLine("{0},{1},{2},{3},{4},{5},{6},{7},{8}", 
                            item.CdAtivoBolsa, item.Especificao,
                            item.Descricao, item.TipoMercado,
                            item.PuExercicio, item.DataVencimento,
                            item.CodigoAtivoObjeto, item.CodigoIsin, item.NumeroSerie);
            }
        }
        catch (Exception ex) {
            document.Close();
            document.Dispose();

            throw new Exception(ex.Message);
        }
        #endregion

        document.Close();
        document.Dispose();
    }

    /// <summary>
    /// Gera e Carrega os Ativos Bolsa de acordo com o objeto List<ValoresExcel>
    /// </summary>
    private void CarregaAtivoBolsa() {
        AtivoBolsaCollection ativoBolsaCollection = new AtivoBolsaCollection();        
        //    
        for (int i = 0; i < this.valoresExcelAtivoBolsa.Count; i++) {
            ValoresExcelAtivoBolsa valoresExcel = this.valoresExcelAtivoBolsa[i];
            //

            #region Confere Null
            if (String.IsNullOrEmpty(valoresExcel.Especificao)) {
                throw new Exception("Especificação não pode ser nulo.");
            }
            else if (String.IsNullOrEmpty(valoresExcel.TipoMercado)) {
                throw new Exception("TipoMercado não pode ser nulo.");
            }
            #endregion
           
            #region AtivoBolsa

            if (!new AtivoBolsa().LoadByPrimaryKey(valoresExcel.CdAtivoBolsa)) {

                AtivoBolsa ativoBolsa = ativoBolsaCollection.AddNew();
                // Dados do Arquivo
                ativoBolsa.CdAtivoBolsa = valoresExcel.CdAtivoBolsa;
                ativoBolsa.Especificacao = valoresExcel.Especificao;
                ativoBolsa.Descricao = valoresExcel.Descricao;
                ativoBolsa.TipoMercado = valoresExcel.TipoMercado;
                ativoBolsa.PUExercicio = valoresExcel.PuExercicio;
                ativoBolsa.DataVencimento = valoresExcel.DataVencimento;
                ativoBolsa.CdAtivoBolsaObjeto = valoresExcel.CodigoAtivoObjeto;
                ativoBolsa.CodigoIsin = valoresExcel.CodigoIsin;
                ativoBolsa.NumeroSerie = valoresExcel.NumeroSerie;
                //
                ativoBolsa.IdEmissor = (int)ListaEmissorFixo.Padrao;
                ativoBolsa.IdMoeda = (int)ListaMoedaFixo.Real;
                //ativoBolsa.OpcaoIndice = null;
                //
                EstrategiaCollection estrategia = new EstrategiaCollection();
                estrategia.Query.Where(estrategia.Query.Descricao.Like("Ações%"));
                //
                int? idEstrategia = null;
                //
                if (estrategia.Query.Load()) {
                    idEstrategia = estrategia[0].IdEstrategia.Value;
                }
                ativoBolsa.IdEstrategia = idEstrategia;
                ativoBolsa.TipoPapel = (byte)TipoPapelAtivo.Normal;
                //
            }

            #endregion
        }

        // Salva AtivosBolsa presentes no Excel                
        ativoBolsaCollection.Save();
    }

    /// <summary>
    /// Processa a Planilha de AtivoBolsa após ter acabado o Upload
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void uplAtivoBolsa_FileUploadComplete(object sender, FileUploadCompleteEventArgs e) {
        e.CallbackData = "";

        #region Trata Extensão Válida
        if (!ValoresExcelUtil.isExtensaoExcel(e.UploadedFile.FileName.Trim())) {
            e.CallbackData = "Importação Ativo Bolsa - Formato de Arquivo Inválido. Formato permitido: .xls ou .xlsx\n\n";
            return;
        }
        #endregion

        // Stream de bytes com o Conteudo do Arquivo Excel
        Stream sr = e.UploadedFile.FileContent;

        #region Processamento Arquivo Excel

        // Reseta Vetor de Dados
        this.valoresExcelAtivoBolsa = new List<ValoresExcelAtivoBolsa>();

        //
        try {
            // Ler Arquivo
            this.LerArquivoAtivoBolsa(sr);
            // Carrega Arquivo
            this.CarregaAtivoBolsa();
        }
        catch (Exception e2) {
            e.CallbackData = "Importação Ativo Bolsa - " + e2.Message;
            return;
        }

        new HistoricoLog().InsereHistoricoLog(DateTime.Now, DateTime.Now,
                                "Importação Planilha Ativo Bolsa",
                                HttpContext.Current.User.Identity.Name, UtilitarioWeb.GetIP(Request), "", HistoricoLogOrigem.Outros);

        #endregion

    }
}