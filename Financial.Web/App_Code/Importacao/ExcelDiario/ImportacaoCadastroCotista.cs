﻿using System;
using System.Collections.Generic;
using System.IO;

using Financial.Integracao.Excel;
using Financial.Integracao.Excel.Util;
using Financial.Web.Common;

using EntitySpaces.Interfaces;

using DevExpress.Web;
using Financial.CRM;
using Financial.InvestidorCotista;
using Financial.RendaFixa;
using Financial.Investidor;
using Financial.Bolsa;
using Financial.BMF;
using Financial.Fundo;
using Financial.Common;
using Financial.CRM.Enums;
using Financial.InvestidorCotista.Enums;
using Financial.Security;
using System.Web;
using Financial.Web.Util;
using Financial.Security.Enums;
using Financial.Util;

public partial class ImportacaoBasePage : BasePage
{

    /* Estrutura do Excel */
    public List<ValoresExcelCadastroCotista> valoresExcelCadastroCotista = new List<ValoresExcelCadastroCotista>();

    /// <summary>
    /// 
    /// </summary>
    /// <param name="streamExcel"></param>
    private void LerArquivoCadastroCotista(Stream streamExcel)
    {

        #region Leitura do Arquivo Excel
        // Open Spreadsheet
        Bytescout.Spreadsheet.Spreadsheet document = new Bytescout.Spreadsheet.Spreadsheet();
        document.LoadFromStream(streamExcel);

        Bytescout.Spreadsheet.Worksheet workSheet = document.Workbook.Worksheets[0];
        //
        #region Confere Formato Arquivo
        string[] colunasConferencias = new string[] {"IdCliente","Nome","Apelido","Tipo","Cpfcnpj",
                                                     "IsentoIR", "IsentoIOF", "StatusAtivo",
                                                     "TipoTributacao", "PessoaVinculada", "SituacaoLegal", "TipoCotistaCVM", "CodigoInterface", "EstadoCivil",
                                                     "RG", "EmissorRG", "DataEmissaoRG", "UFEmissor", "Sexo", "DataNascimento", "Profissão", "UFNaturalidade",
                          "Endereco","Numero","Complemento","Bairro","Cidade","CEP","UF","Pais",
                        "EnderecoCom","NumeroCom","ComplementoCom","BairroCom","CidadeCom","CEPCom","UFCom",
                        "Fone", "Email", "FoneCom", "EmailCom"
                };

        bool formato = ValoresExcelUtil.isExcelFormatoValido(colunasConferencias, workSheet);
        if (!formato)
        {
            string mensagem = "Formato Interno do Arquivo Inválido.\n";
            mensagem += "Linha 1 do Arquivo deve ser: \n";
            for (int i = 0; i < colunasConferencias.Length; i++)
            {
                int coluna = i + 1;
                mensagem += "\tColuna " + coluna + ": " + colunasConferencias[i] + "\n";
            }

            document.Close();
            document.Dispose();

            throw new Exception(mensagem);
        }
        #endregion

        //
        int index = 0;
        // This row,column index should be changed as per your need.
        // i.e. which cell in the excel you are interesting to read.
        //
        /* Formato: 1)IdCliente  - 2)Nome - 3) Apelido, 4)Tipo - 5)Cpfcnpj
         *          6)IsentoIR   - 7)IsentoIOF  - 8)StatusAtivo
         *          9)TipoTributacao - 10) PessoaVinculada - 11) SituacaoLegal 12)TipoCotistaCVM - 13)CodigoInterface  - 14)EstadoCivil
         *          15)RG   -  16)EmissorRG -  17)DataEmissaoRG - 18)UFEmissor - 19)Sexo -  20)DataNascimento  - 21)Profissão - 22)UFNaturalidade
         *          23)Endereco  - 24)Numero  - 25) Complemento - 26)Bairro  - 27)Cidade - 28)CEP - 29)UF  - 30)Pais
         *          31)EnderecoCom - 32)NumeroCom - 33)ComplementoCom  - 34)BairroCom - 35)CidadeCom - 36)CEPCom - 37)UFCom
         *          38)Fone - 39)Email - 40)FoneCom - 41)EmailCom
         */
        int linha = 1;
        int coluna1 = 0, coluna2 = 1, coluna3 = 2,
            coluna4 = 3, coluna5 = 4, coluna6 = 5,
            coluna7 = 6, coluna8 = 7, coluna9 = 8,
            coluna10 = 9, coluna11 = 10, coluna12 = 11,
            coluna13 = 12, coluna14 = 13, coluna15 = 14,
            coluna16 = 15, coluna17 = 16, coluna18 = 17,
            coluna19 = 18, coluna20 = 19, coluna21 = 20,
            coluna22 = 21, coluna23 = 22, coluna24 = 23,
            coluna25 = 24, coluna26 = 25, coluna27 = 26,
            coluna28 = 27, coluna29 = 28, coluna30 = 29,
            coluna31 = 30, coluna32 = 31, coluna33 = 32,
            coluna34 = 33, coluna35 = 34, coluna36 = 35,
            coluna37 = 36, coluna38 = 37, coluna39 = 38,
            coluna40 = 39, coluna41 = 40;

        try
        {
            // Enquanto idCliente tiver valor
            while (workSheet.Cell(linha, coluna1).Value != null)
            {
                ValoresExcelCadastroCotista item = new ValoresExcelCadastroCotista();
                //                                                                                  

                try
                {
                    item.IdCotista = Convert.ToInt32(workSheet.Cell(linha, coluna1).Value);
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message + " - IdCotista: linha " + (linha + 1));
                }

                if (workSheet.Cell(linha, coluna2).Value == null)
                    throw new Exception("Nome não informado: linha " + (linha + 1));
                try
                {
                    item.Nome = Convert.ToString(workSheet.Cell(linha, coluna2).Value);
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message + " - Nome: linha " + (linha + 1));
                }

                if (workSheet.Cell(linha, coluna3).Value == null)
                    throw new Exception("Apelido não informado: linha " + (linha + 1));
                try
                {
                    item.Apelido = Convert.ToString(workSheet.Cell(linha, coluna3).Value);
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message + " - Apelido: linha " + (linha + 1));
                }

                if (workSheet.Cell(linha, coluna4).Value == null)
                    throw new Exception("TipoPessoa não informado: linha " + (linha + 1));
                try
                {
                    int tipo = Convert.ToInt32(workSheet.Cell(linha, coluna4).Value);
                    item.TipoPessoa = (TipoPessoa)tipo;
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message + " - TipoPessoa: linha " + (linha + 1));
                }

                try
                {
                    item.Cnpj = Convert.ToString(workSheet.Cell(linha, coluna5).Value);
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message + " - Cnpj: linha " + (linha + 1));
                }

                if (workSheet.Cell(linha, coluna6).Value == null)
                    throw new Exception("IsentoIR não informado: linha " + (linha + 1));
                try
                {
                    item.IsentoIR = Convert.ToString(workSheet.Cell(linha, coluna6).Value);
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message + " - IsentoIR: linha " + (linha + 1));
                }

                if (workSheet.Cell(linha, coluna7).Value == null)
                    throw new Exception("IsentoIOF não informado: linha " + (linha + 1));
                try
                {
                    item.IsentoIOF = Convert.ToString(workSheet.Cell(linha, coluna7).Value);
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message + " - IsentoIOF: linha " + (linha + 1));
                }

                if (workSheet.Cell(linha, coluna8).Value == null)
                    throw new Exception("StatusCotista não informado: linha " + (linha + 1));
                try
                {
                    int status = Convert.ToInt32(workSheet.Cell(linha, coluna8).Value);
                    item.StatusCotista = (StatusAtivoCotista)status;
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message + " - StatusCotista: linha " + (linha + 1));
                }

                if (workSheet.Cell(linha, coluna9).Value == null)
                    throw new Exception("TipoTributacaoCotista não informado: linha " + (linha + 1));
                try
                {
                    int tipoTributacao = Convert.ToInt32(workSheet.Cell(linha, coluna9).Value);
                    item.TipoTributacaoCotista = (TipoTributacaoCotista)tipoTributacao;
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message + " - TipoTributacaoCotista: linha " + (linha + 1));
                }

                item.PessoaVinculada = "N";
                if (workSheet.Cell(linha, coluna10).Value != null)
                {
                    try
                    {
                        item.PessoaVinculada = workSheet.Cell(linha, coluna10).ValueAsString;
                    }
                    catch (Exception ex)
                    {
                        throw new Exception(ex.Message + " - PessoaVinculada: linha " + (linha + 1));
                    }
                }
                if (workSheet.Cell(linha, coluna11).Value != null)
                {
                    try
                    {
                        item.SituacaoLegal = (SituacaoLegalPessoa)Convert.ToInt32(workSheet.Cell(linha, coluna11).Value);
                    }
                    catch (Exception ex)
                    {
                        throw new Exception(ex.Message + " - SituacaoLegal: linha " + (linha + 1));
                    }
                }
                if (workSheet.Cell(linha, coluna12).Value != null)
                {
                    try
                    {
                        item.TipoCotistaCVM = (TipoCotista)Convert.ToInt32(workSheet.Cell(linha, coluna12).Value);
                    }
                    catch (Exception ex)
                    {
                        throw new Exception(ex.Message + " - TipoCotistaCVM: linha " + (linha + 1));
                    }
                }
                //
                if (workSheet.Cell(linha, coluna13).Value != null)
                {
                    try
                    {
                        item.CodigoInterface = workSheet.Cell(linha, coluna13).ValueAsString;
                    }
                    catch (Exception ex)
                    {
                        throw new Exception(ex.Message + " - CodigoInterface: linha " + (linha + 1));
                    }
                }
                //
                if (workSheet.Cell(linha, coluna14).Value != null)
                {
                    try
                    {
                        item.EstadoCivilCotista = (EstadoCivilPessoa)Convert.ToInt32(workSheet.Cell(linha, coluna14).Value);
                    }
                    catch (Exception ex)
                    {
                        throw new Exception(ex.Message + " - EstadoCivilCotista: linha " + (linha + 1));
                    }
                }
                //
                if (workSheet.Cell(linha, coluna15).Value != null)
                {
                    try
                    {
                        item.Rg = workSheet.Cell(linha, coluna15).ValueAsString;
                    }
                    catch (Exception ex)
                    {
                        throw new Exception(ex.Message + " - Rg: linha " + (linha + 1));
                    }
                }
                //
                if (workSheet.Cell(linha, coluna16).Value != null)
                {
                    try
                    {
                        item.EmissorRg = workSheet.Cell(linha, coluna16).ValueAsString;
                    }
                    catch (Exception ex)
                    {
                        throw new Exception(ex.Message + " - EmissorRg: linha " + (linha + 1));
                    }
                }
                //
                if (workSheet.Cell(linha, coluna17).Value != null)
                {
                    try
                    {
                        item.DataEmissaoRg = workSheet.Cell(linha, coluna17).ValueAsDateTime;
                    }
                    catch (Exception ex)
                    {
                        throw new Exception(ex.Message + " - DataEmissaoRg: linha " + (linha + 1));
                    }
                }

                if (workSheet.Cell(linha, coluna18).Value != null)
                {
                    try
                    {
                        item.UFEmissor = workSheet.Cell(linha, coluna18).ValueAsString;
                    }
                    catch (Exception ex)
                    {
                        throw new Exception(ex.Message + " - UFEmissor: linha " + (linha + 1));
                    }
                }
                //
                if (workSheet.Cell(linha, coluna19).Value != null)
                {
                    try
                    {
                        item.Sexo = workSheet.Cell(linha, coluna19).ValueAsString;
                    }
                    catch (Exception ex)
                    {
                        throw new Exception(ex.Message + " - Sexo: linha " + (linha + 1));
                    }
                }
                //
                if (workSheet.Cell(linha, coluna20).Value != null)
                {
                    try
                    {
                        item.DataNascimento = workSheet.Cell(linha, coluna20).ValueAsDateTime;
                    }
                    catch (Exception ex)
                    {
                        throw new Exception(ex.Message + " - DataNascimento: linha " + (linha + 1));
                    }
                }
                if (workSheet.Cell(linha, coluna21).Value != null)
                {
                    try
                    {
                        item.Profissao = workSheet.Cell(linha, coluna21).ValueAsString;
                    }
                    catch (Exception ex)
                    {
                        throw new Exception(ex.Message + " - Profissao: linha " + (linha + 1));
                    }
                }
                //   
                if (workSheet.Cell(linha, coluna22).Value != null)
                {
                    try
                    {
                        item.UFNaturalidade = workSheet.Cell(linha, coluna22).ValueAsString;
                    }
                    catch (Exception ex)
                    {
                        throw new Exception(ex.Message + " - UFNaturalidade: linha " + (linha + 1));
                    }
                }
                ValoresExcelCadastroCotista.Endereco[] endereco = {
                            new ValoresExcelCadastroCotista.Endereco(
                                Convert.ToString(workSheet.Cell(linha, coluna23).Value),  //Endereco
                                Convert.ToString(workSheet.Cell(linha, coluna24).Value),  //Numero
                                Convert.ToString(workSheet.Cell(linha, coluna25).Value), //Complemento 
                                Convert.ToString(workSheet.Cell(linha, coluna26).Value), //Bairro
                                Convert.ToString(workSheet.Cell(linha, coluna27).Value), //Cidade
                                Convert.ToString(workSheet.Cell(linha, coluna28).Value), //CEP
                                Convert.ToString(workSheet.Cell(linha, coluna29).Value), //UF
                                Convert.ToString(workSheet.Cell(linha, coluna30).Value), //Pais
                                "", //DDD nao exite na planilha
                                Convert.ToString(workSheet.Cell(linha, coluna38).Value), //Fone
                                "", //Ramal nao existe na planilha
                                Convert.ToString(workSheet.Cell(linha, coluna39).Value)  //Email
                                ),
                            new ValoresExcelCadastroCotista.Endereco(
                                Convert.ToString(workSheet.Cell(linha, coluna31).Value), //EnderecoCom
                                Convert.ToString(workSheet.Cell(linha, coluna32).Value), //NumeroCom
                                Convert.ToString(workSheet.Cell(linha, coluna33).Value), //ComplementoCom
                                Convert.ToString(workSheet.Cell(linha, coluna34).Value), //BairroCom
                                Convert.ToString(workSheet.Cell(linha, coluna35).Value), //CidadeCom
                                Convert.ToString(workSheet.Cell(linha, coluna36).Value), //CEPCom
                                Convert.ToString(workSheet.Cell(linha, coluna37).Value), //UFCom
                                Convert.ToString(workSheet.Cell(linha, coluna30).Value), //Pais
                                "", //DDD nao exite na planilha
                                Convert.ToString(workSheet.Cell(linha, coluna40).Value), //Fone
                                "", //Ramal nao existe na planilha
                                Convert.ToString(workSheet.Cell(linha, coluna41).Value)  //Email
                                )
                        };

                item.EnderecoPessoa = endereco[0];
                item.EnderecoComercialPessoa = endereco[1];

                //
                this.valoresExcelCadastroCotista.Add(item);
                //
                index++;
                linha = 1 + index;
                //                
                //Console.WriteLine("{0},{1},{2},{3},{4},{5},{6},{7},{8},{9},{10},{11},{12},{13},{14},{15},{16}",
                //    item.IdCotista, item.Nome, item.TipoPessoa, item.Cnpj,
                //    item.IsentoIR, item.IsentoIOF, item.StatusCotista,
                //    item.TipoTributacaoCotista, item.TipoCotistaCVM, item.CodigoInterface, item.EstadoCivilCotista,
                //    item.Rg, item.EmissorRg, item.DataEmissaoRg, item.Sexo, item.DataNascimento, item.Profissao,
                //    item.EnderecoPessoa, item.EnderecoComercialPessoa);
            }
        }

        catch (Exception ex)
        {
            document.Close();
            document.Dispose();

            throw new Exception(ex.Message);
        }
        #endregion

        document.Close();
        document.Dispose();
    }

    /// <summary>
    ///  Carrega somente o Cotista e PermissaoCotista
    /// </summary>
    /// <param name="somenteCotista">indica que deve carregar somente o cotista e não carregar a Pessoa</param>
    public void CarregaCadastroCotista(bool somenteCotista)
    {
        CotistaCollection cotistaCollection = new CotistaCollection();
        PermissaoCotistaCollection permissaoCotistaCollection = new PermissaoCotistaCollection();

        Usuario usuario = new Usuario();

        string login = HttpContext.Current.User.Identity.Name;
        if (String.IsNullOrEmpty(login))
        {
            //Quando carregando de web service não temos o Identity - olhar para o campo CriadoPor
            if (this.valoresExcelCadastroCotista.Count > 0)
            {
                login = this.valoresExcelCadastroCotista[0].CriadoPor;
            }
        }

        bool usuarioEncontrado = usuario.BuscaUsuario(login);
        for (int i = 0; i < this.valoresExcelCadastroCotista.Count; i++)
        {
            ValoresExcelCadastroCotista valoresExcel = this.valoresExcelCadastroCotista[i];
            //
            if (!new Cotista().LoadByPrimaryKey(valoresExcel.IdCotista))
            { // Para Garantir

                #region Cotista
                Cotista cotista = cotistaCollection.AddNew();
                cotista.IdCotista = valoresExcel.IdCotista;
                cotista.Nome = valoresExcel.Nome.Trim();
                cotista.Apelido = valoresExcel.Apelido == null ? cotista.Nome : valoresExcel.Apelido.Trim();

                cotista.IsentoIR = valoresExcel.IsentoIR.ToUpper();
                cotista.IsentoIOF = valoresExcel.IsentoIOF.ToUpper();
                cotista.StatusAtivo = (byte)valoresExcel.StatusCotista;

                cotista.TipoTributacao = (byte)valoresExcel.TipoTributacaoCotista;

                if (valoresExcel.TipoCotistaCVM != null)
                {
                    cotista.TipoCotistaCVM = (int)valoresExcel.TipoCotistaCVM;
                }

                if (valoresExcel.TipoCotistaAnbima != null)
                {
                    cotista.TipoCotistaAnbima = (int)valoresExcel.TipoCotistaAnbima;
                }

                if (!String.IsNullOrEmpty(valoresExcel.CodigoInterface))
                {
                    cotista.CodigoInterface = valoresExcel.CodigoInterface;
                }
                #endregion

                #region Adiciona em PermissaoCotista
                if (usuarioEncontrado)
                {
                    PermissaoCotista permissaoCotista = permissaoCotistaCollection.AddNew();
                    permissaoCotista.IdCotista = cotista.IdCotista.Value;
                    permissaoCotista.IdUsuario = usuario.IdUsuario.Value;
                }
                #endregion
            }
        }

        using (esTransactionScope scope = new esTransactionScope())
        {
            cotistaCollection.Save();
            permissaoCotistaCollection.Save();

            //
            scope.Complete();
        }
    }

    /// <summary>
    /// Salva pessoa, pessoaEndereco, pessoaTelefone, pessoaEmail e pessoaDocumento, cotista e pemrmissaoCotista
    /// </summary>
    /// <param name="isCotista"></param>
    public void CarregaCadastroCotista()
    {
        PessoaCollection pessoaCollection = new PessoaCollection();
        PessoaEnderecoCollection pessoaEnderecoCollection = new PessoaEnderecoCollection();
        PessoaTelefoneCollection pessoaTelefoneCollection = new PessoaTelefoneCollection();
        PessoaEmailCollection pessoaEmailCollection = new PessoaEmailCollection();
        PessoaDocumentoCollection pessoaDocumentoCollection = new PessoaDocumentoCollection();

        CotistaCollection cotistaCollection = new CotistaCollection();
        PermissaoCotistaCollection permissaoCotistaCollection = new PermissaoCotistaCollection();

        PessoaVinculoCollection pessoaVinculoCollection = new PessoaVinculoCollection();

        ContaCorrenteCollection contaCorrenteCollection = new ContaCorrenteCollection();

        Usuario usuario = new Usuario();

        string login = HttpContext.Current.User.Identity.Name;
        if (String.IsNullOrEmpty(login))
        {
            //Quando carregando de web service não temos o Identity - olhar para o campo CriadoPor
            if (this.valoresExcelCadastroCotista.Count > 0)
            {
                login = this.valoresExcelCadastroCotista[0].CriadoPor;
            }
        }

        bool usuarioEncontrado = usuario.BuscaUsuario(login);
        for (int i = 0; i < this.valoresExcelCadastroCotista.Count; i++)
        {
            ValoresExcelCadastroCotista valoresExcel = this.valoresExcelCadastroCotista[i];
            //                    
            #region Verifica se já Existe Pessoa ou se já está na collection
            Pessoa pessoaAux = new Pessoa();
            List<esQueryItem> campos = new List<esQueryItem>();
            campos.Add(pessoaAux.Query.IdPessoa);

            var cotistaAux = new Cotista();

            if (pessoaAux.LoadByPrimaryKey(campos, valoresExcel.IdCotista) || cotistaAux.LoadByPrimaryKey(valoresExcel.IdCotista))
            {
                AtualizaCadastroCotista();
                continue;
            }

            Pessoa pessoaExiste = new Pessoa();
            pessoaExiste = pessoaCollection.FindByPrimaryKey(valoresExcel.IdCotista);

            if (pessoaExiste != null)
            {
                continue;
            }
            #endregion

            #region Verifica se Campos Obrigatários foram Preenchidos
            if (valoresExcel.Nome == null || String.IsNullOrEmpty(valoresExcel.Nome.Trim()))
            {
                throw new Exception("Nome Cotista não pode ser em Branco");
            }
            if (valoresExcel.TipoPessoa == 0)
            { // Conversão do Enum resultou em 0
                throw new Exception("Tipo Pessoa(Física/Jurídica) deve ser Preenchido");
            }
            if (String.IsNullOrEmpty(valoresExcel.IsentoIR))
            {
                valoresExcel.IsentoIR = "S";
            }
            if (String.IsNullOrEmpty(valoresExcel.IsentoIOF))
            {
                valoresExcel.IsentoIOF = "S";
            }
            if (valoresExcel.StatusCotista == 0)
            { // Conversão do Enum resultou em 0
                valoresExcel.StatusCotista = StatusAtivoCotista.Ativo;
            }
            if (valoresExcel.TipoTributacaoCotista == 0)
            { // Conversão do Enum resultou em 0
                valoresExcel.TipoTributacaoCotista = TipoTributacaoCotista.Residente;
            }
            #endregion

            #region Pessoa
            Pessoa pessoa = pessoaCollection.AddNew();
            pessoa.IdPessoa = pessoa.GeraIdPessoa();
            pessoa.Nome = valoresExcel.Nome.Trim();
            pessoa.Apelido = valoresExcel.Apelido == null ? pessoa.Nome : valoresExcel.Apelido.Trim();
            pessoa.Tipo = (byte)valoresExcel.TipoPessoa;
            pessoa.DataCadatro = DateTime.Now;
            pessoa.DataUltimaAlteracao = DateTime.Now;

            if (!string.IsNullOrEmpty(valoresExcel.Cnpj))
            {
                int lengthCnpj = pessoa.Tipo == (byte)TipoPessoa.Fisica ? 11 : 14;

                string cnpjNumOnly = string.Join(null, System.Text.RegularExpressions.Regex.Split(valoresExcel.Cnpj, "[^\\d]")).PadLeft(lengthCnpj, '0');

                if (pessoa.Tipo == (byte)TipoPessoa.Fisica)
                {
                    if (!Utilitario.ValidaCPF(cnpjNumOnly))
                    {
                        throw new Exception("CPF inválido! " + valoresExcel.Cnpj);
                    }
                }
                else
                {
                    if (!Utilitario.ValidaCNPJ(cnpjNumOnly))
                    {
                        throw new Exception("CNPJ inválido! " + valoresExcel.Cnpj);
                    }
                }

                pessoa.Cpfcnpj = cnpjNumOnly;
            }
            else
            {
                pessoa.Cpfcnpj = null;
            }

            pessoa.PessoaVinculada = valoresExcel.PessoaVinculada;
            if (valoresExcel.SituacaoLegal != null)
            {
                pessoa.SituacaoLegal = (byte)valoresExcel.SituacaoLegal;
            }
            //
            if (valoresExcel.EstadoCivilCotista != null)
            {
                pessoa.EstadoCivil = (byte)valoresExcel.EstadoCivilCotista;
            }
            if (!String.IsNullOrEmpty(valoresExcel.Sexo))
            {
                pessoa.Sexo = valoresExcel.Sexo.Trim().ToUpper();
            }
            if (valoresExcel.DataNascimento.HasValue)
            {
                pessoa.DataNascimento = valoresExcel.DataNascimento;
            }
            if (!String.IsNullOrEmpty(valoresExcel.Profissao))
            {
                pessoa.Profissao = valoresExcel.Profissao.Trim();
            }
            if (!String.IsNullOrEmpty(valoresExcel.UFNaturalidade))
            {
                pessoa.UFNaturalidade = valoresExcel.UFNaturalidade.Trim();
            }
            if (!String.IsNullOrEmpty(valoresExcel.CodigoInterface))
            {
                pessoa.CodigoInterface = valoresExcel.CodigoInterface;
            }

            if (valoresExcel.PaisNacionalidade != null)
            {
                pessoa.Nacionalidade = valoresExcel.PaisNacionalidade;
            }
            if (!string.IsNullOrEmpty(valoresExcel.PoliticamenteExposta))
            {
                pessoa.PessoaPoliticamenteExposta = valoresExcel.PoliticamenteExposta;
            }
            if (valoresExcel.DataVencimentoCadastro != null)
            {
                pessoa.DataVencimentoCadastro = valoresExcel.DataVencimentoCadastro;
            }
            if (!string.IsNullOrEmpty(valoresExcel.NomePai))
            {
                pessoa.FiliacaoNomePai = valoresExcel.NomePai;
            }
            if (!string.IsNullOrEmpty(valoresExcel.NomeMae))
            {
                pessoa.FiliacaoNomeMae = valoresExcel.NomeMae;
            }

            #endregion

            #region PessoaDocumento
            if (!String.IsNullOrEmpty(valoresExcel.Rg))
            {

                PessoaDocumento p1 = pessoaDocumentoCollection.AddNew();
                p1.IdPessoa = pessoa.IdPessoa;
                p1.TipoDocumento = (byte)TipoDocumentoPessoa.CedulaIdentidade_RG;

                if (!string.IsNullOrEmpty(valoresExcel.Rg))
                    p1.NumeroDocumento = StringExt.Truncate(valoresExcel.Rg.Trim(), 13);

                if (!String.IsNullOrEmpty(valoresExcel.EmissorRg))
                {
                    p1.OrgaoEmissor = StringExt.Truncate(valoresExcel.EmissorRg.Trim(), 7);
                }
                if (valoresExcel.DataEmissaoRg.HasValue)
                {
                    p1.DataExpedicao = valoresExcel.DataEmissaoRg;
                }
                if (!String.IsNullOrEmpty(valoresExcel.UFEmissor))
                {
                    p1.UFEmissor = StringExt.Truncate(valoresExcel.UFEmissor.Trim(), 7);
                }
            }

            #endregion

            #region PessoaEndereco

            if (valoresExcel.PessoaEnderecoList != null && valoresExcel.PessoaEnderecoList.Count > 0)
            {
                valoresExcel.PessoaEnderecoList.ForEach(t =>
                {
                    var pessoaEndereco = pessoaEnderecoCollection.AddNew();
                    pessoaEndereco.IdPessoa = pessoa.IdPessoa.Value;
                    pessoaEndereco.RecebeCorrespondencia = t.RecebeCorrespondencia;
                    pessoaEndereco.Endereco = t.Endereco;
                    pessoaEndereco.Numero = t.Numero;
                    pessoaEndereco.Complemento = t.Complemento;
                    pessoaEndereco.Bairro = t.Bairro;
                    pessoaEndereco.Cidade = t.Cidade;
                    pessoaEndereco.Cep = t.Cep;
                    pessoaEndereco.Uf = t.Uf;
                    pessoaEndereco.Pais = t.Pais;
                    pessoaEndereco.TipoEndereco = t.TipoEndereco;

                });
            }
            else
            {
                if (valoresExcel.EnderecoPessoa.HasValue() && !String.IsNullOrEmpty(valoresExcel.EnderecoPessoa.Rua))
                {
                    PessoaEndereco pessoaEndereco = pessoaEnderecoCollection.AddNew();
                    pessoaEndereco.IdPessoa = pessoa.IdPessoa.Value;
                    pessoaEndereco.RecebeCorrespondencia = "S";
                    pessoaEndereco.Endereco = valoresExcel.EnderecoPessoa.Rua == null ? "" : valoresExcel.EnderecoPessoa.Rua.Trim();
                    pessoaEndereco.Numero = valoresExcel.EnderecoPessoa.Numero == null ? "" : valoresExcel.EnderecoPessoa.Numero.Trim();
                    pessoaEndereco.Complemento = valoresExcel.EnderecoPessoa.Complemento == null ? "" : valoresExcel.EnderecoPessoa.Complemento.Trim();
                    pessoaEndereco.Bairro = valoresExcel.EnderecoPessoa.Bairro == null ? "" : valoresExcel.EnderecoPessoa.Bairro.Trim();
                    pessoaEndereco.Cidade = valoresExcel.EnderecoPessoa.Cidade == null ? "" : valoresExcel.EnderecoPessoa.Cidade.Trim();
                    pessoaEndereco.Cep = valoresExcel.EnderecoPessoa.Cep == null ? "" : valoresExcel.EnderecoPessoa.Cep.Trim();
                    pessoaEndereco.Uf = valoresExcel.EnderecoPessoa.Uf == null ? "" : valoresExcel.EnderecoPessoa.Uf.Trim().ToUpper();
                    pessoaEndereco.Pais = valoresExcel.EnderecoPessoa.Pais == null ? "" : valoresExcel.EnderecoPessoa.Pais.Trim();
                    pessoaEndereco.TipoEndereco = TipoEnderecoPessoa.Residencial;
                }

                if (valoresExcel.EnderecoComercialPessoa.HasValue() && !String.IsNullOrEmpty(valoresExcel.EnderecoComercialPessoa.Rua))
                {
                    PessoaEndereco pessoaEnderecoComercial = pessoaEnderecoCollection.AddNew();
                    pessoaEnderecoComercial.IdPessoa = pessoa.IdPessoa.Value;
                    pessoaEnderecoComercial.RecebeCorrespondencia = "S";
                    pessoaEnderecoComercial.Endereco = valoresExcel.EnderecoComercialPessoa.Rua == null ? "" : valoresExcel.EnderecoComercialPessoa.Rua.Trim();
                    pessoaEnderecoComercial.Numero = valoresExcel.EnderecoComercialPessoa.Numero == null ? "" : valoresExcel.EnderecoComercialPessoa.Numero.Trim();
                    pessoaEnderecoComercial.Complemento = valoresExcel.EnderecoComercialPessoa.Complemento == null ? "" : valoresExcel.EnderecoComercialPessoa.Complemento.Trim();
                    pessoaEnderecoComercial.Bairro = valoresExcel.EnderecoComercialPessoa.Bairro == null ? "" : valoresExcel.EnderecoComercialPessoa.Bairro.Trim();
                    pessoaEnderecoComercial.Cidade = valoresExcel.EnderecoComercialPessoa.Cidade == null ? "" : valoresExcel.EnderecoComercialPessoa.Cidade.Trim();
                    pessoaEnderecoComercial.Cep = valoresExcel.EnderecoComercialPessoa.Cep == null ? "" : valoresExcel.EnderecoComercialPessoa.Cep.Trim();
                    pessoaEnderecoComercial.Uf = valoresExcel.EnderecoComercialPessoa.Uf == null ? "" : valoresExcel.EnderecoComercialPessoa.Uf.Trim().ToUpper();
                    pessoaEnderecoComercial.Pais = valoresExcel.EnderecoComercialPessoa.Pais == null ? "" : valoresExcel.EnderecoComercialPessoa.Pais.Trim();

                    pessoaEnderecoComercial.TipoEndereco = TipoEnderecoPessoa.Comercial;
                    //
                    // Marcar RecebeCorrespondencia do endereço Normal se existir para N
                    if (valoresExcel.EnderecoPessoa.HasValue())
                    {
                        pessoaEnderecoCollection[pessoaEnderecoCollection.Count - 1].RecebeCorrespondencia = "N";
                    }
                }
            }
            #endregion

            #region PessoaTelefone

            if (valoresExcel.PessoaTelefoneList != null && valoresExcel.PessoaTelefoneList.Count > 0)
            {
                valoresExcel.PessoaTelefoneList.ForEach(t =>
                {
                    var pessoaTelefone = pessoaTelefoneCollection.AddNew();
                    pessoaTelefone.IdPessoa = pessoa.IdPessoa.Value;
                    pessoaTelefone.Ddi = t.Ddi;
                    pessoaTelefone.Ddd = t.Ddd;
                    pessoaTelefone.Numero = t.Numero;
                    pessoaTelefone.TipoTelefone = t.TipoTelefone;
                    pessoaTelefone.Ramal = t.Ramal;
                });
            }
            else {

                if (!String.IsNullOrEmpty(valoresExcel.EnderecoPessoa.FoneNumero))
                {
                    PessoaTelefone pessoaTelefone = pessoaTelefoneCollection.AddNew();
                    //
                    pessoaTelefone.IdPessoa = pessoa.IdPessoa.Value;
                    pessoaTelefone.Ddi = "";
                    pessoaTelefone.Ddd = "";
                    pessoaTelefone.Numero = valoresExcel.EnderecoPessoa.FoneNumero;
                    pessoaTelefone.TipoTelefone = (byte)TipoTelefonePessoa.Residencial;
                }
                if (!String.IsNullOrEmpty(valoresExcel.EnderecoComercialPessoa.FoneNumero))
                {
                    PessoaTelefone pessoaTelefone = pessoaTelefoneCollection.AddNew();
                    //
                    pessoaTelefone.IdPessoa = pessoa.IdPessoa.Value;
                    pessoaTelefone.Ddi = "";
                    pessoaTelefone.Ddd = "";
                    pessoaTelefone.Numero = valoresExcel.EnderecoComercialPessoa.FoneNumero;
                    pessoaTelefone.TipoTelefone = (byte)TipoTelefonePessoa.Comercial;
                }
            }
            #endregion

            #region PessoaEmail
            if (valoresExcel.PessoaEmailList != null && valoresExcel.PessoaEmailList.Count > 0)
            {
                valoresExcel.PessoaEmailList.ForEach(t =>
                {
                    var pessoaEmail = pessoaEmailCollection.AddNew();
                    pessoaEmail.IdPessoa = pessoa.IdPessoa.Value;
                    pessoaEmail.Email = t.Email;
                }
                );
            }
            else {
                if (!String.IsNullOrEmpty(valoresExcel.EnderecoPessoa.Email))
                {
                    PessoaEmail pessoaEmail = pessoaEmailCollection.AddNew();
                    //
                    pessoaEmail.IdPessoa = pessoa.IdPessoa.Value;
                    pessoaEmail.Email = valoresExcel.EnderecoPessoa.Email;
                }

                if (!String.IsNullOrEmpty(valoresExcel.EnderecoComercialPessoa.Email))
                {
                    PessoaEmail pessoaEmail = pessoaEmailCollection.AddNew();
                    //
                    pessoaEmail.IdPessoa = pessoa.IdPessoa.Value;
                    pessoaEmail.Email = valoresExcel.EnderecoComercialPessoa.Email;
                }
            }
            #endregion

            #region Cotista
            Cotista cotista = cotistaCollection.AddNew();

            cotista.IdCotista = valoresExcel.IdCotista;
            cotista.IdPessoa = pessoa.IdPessoa.Value;
            cotista.Nome = valoresExcel.Nome.Trim();
            cotista.Apelido = valoresExcel.Apelido == null ? cotista.Nome : valoresExcel.Apelido.Trim();

            cotista.IsentoIR = valoresExcel.IsentoIR.ToUpper();
            cotista.IsentoIOF = valoresExcel.IsentoIOF.ToUpper();
            cotista.StatusAtivo = (byte)valoresExcel.StatusCotista;

            cotista.TipoTributacao = (byte)valoresExcel.TipoTributacaoCotista;

            if (valoresExcel.TipoCotistaCVM != null)
            {
                cotista.TipoCotistaCVM = (int)valoresExcel.TipoCotistaCVM;
            }

            if (!String.IsNullOrEmpty(valoresExcel.CodigoInterfaceCotista))
            {
                cotista.CodigoInterface = valoresExcel.CodigoInterfaceCotista;
            }
            else {
                if (!String.IsNullOrEmpty(valoresExcel.CodigoInterface))
                {
                    cotista.CodigoInterface = valoresExcel.CodigoInterface;
                }
            }
            if (!String.IsNullOrEmpty(valoresExcel.PendenciaCadastral))
            {
                cotista.PendenciaCadastral = valoresExcel.PendenciaCadastral.Trim();
            }
            #endregion

            #region Adiciona em PermissaoCotista
            if (usuarioEncontrado)
            {
                PermissaoCotista permissaoCotista = permissaoCotistaCollection.AddNew();
                permissaoCotista.IdCotista = cotista.IdCotista.HasValue ? cotista.IdCotista.Value : valoresExcel.IdCotista;
                permissaoCotista.IdUsuario = usuario.IdUsuario.Value;
            }
            #endregion

            #region Cotitular
            if (valoresExcel.IdTitular.HasValue)
            {

                PessoaVinculo pessoaVinculo = pessoaVinculoCollection.AddNew();
                pessoaVinculo.IdPessoa = valoresExcel.IdTitular;

                pessoaVinculo.IdPessoaVinculada = pessoa.IdPessoa.Value; ;

                pessoaVinculo.TipoVinculo = Convert.ToByte(TipoVinculoPessoa.Cotitular);


            }
            #endregion

            #region ContaCorrente
            if (valoresExcel.InformacoesBancarias != null && valoresExcel.InformacoesBancarias.Count > 0)
            {
                foreach (Financial.Integracao.Excel.ValoresExcelCadastroCotista.InformacaoBancaria informacaoBancaria in valoresExcel.InformacoesBancarias)
                {
                    Banco banco = new BancoCollection().BuscaBancoPorCodigoCompensacao(informacaoBancaria.CodigoCompensacaoBanco);
                    if (banco == null)
                    {
                        banco = new Banco();
                        banco.CodigoCompensacao = informacaoBancaria.CodigoCompensacaoBanco;
                        banco.Nome = informacaoBancaria.CodigoCompensacaoBanco;
                        banco.Save();
                    }

                    Agencia agencia = new AgenciaCollection().BuscaAgenciaPorBancoECodigo(banco.IdBanco.Value, informacaoBancaria.CodigoAgencia);
                    if (agencia == null)
                    {
                        agencia = new Agencia();
                        agencia.IdBanco = banco.IdBanco;
                        agencia.Nome = informacaoBancaria.NomeAgencia;
                        agencia.Codigo = informacaoBancaria.CodigoAgencia;
                        agencia.IdLocal = informacaoBancaria.LocalAgencia;
                        agencia.DigitoAgencia = informacaoBancaria.DigitoAgencia;
                        agencia.Save();
                    }

                    ContaCorrente conta = contaCorrenteCollection.AddNew();
                    conta.IdAgencia = agencia.IdAgencia;
                    conta.IdBanco = banco.IdBanco;
                    conta.DigitoConta = informacaoBancaria.DigitoConta;
                    conta.IdMoeda = informacaoBancaria.MoedaConta;
                    conta.IdPessoa = pessoa.IdPessoa.Value;
                    conta.Numero = informacaoBancaria.NumeroConta;
                    conta.ContaDefault = String.IsNullOrEmpty(informacaoBancaria.ContaDefault) ? "N" : informacaoBancaria.ContaDefault;
                }

            }
            #endregion
        }

        using (esTransactionScope scope = new esTransactionScope())
        {
            pessoaCollection.Save();
            pessoaEnderecoCollection.Save();
            pessoaTelefoneCollection.Save();
            pessoaEmailCollection.Save();
            pessoaDocumentoCollection.Save();
            cotistaCollection.Save();
            permissaoCotistaCollection.Save();
            pessoaVinculoCollection.Save();
            contaCorrenteCollection.Save();

            //
            scope.Complete();
        }
    }

    public void AtualizaCadastroCotista()
    {
        PessoaCollection pessoaCollection = new PessoaCollection();
        PessoaEnderecoCollection pessoaEnderecoCollection = new PessoaEnderecoCollection();
        PessoaTelefoneCollection pessoaTelefoneCollection = new PessoaTelefoneCollection();
        PessoaEmailCollection pessoaEmailCollection = new PessoaEmailCollection();
        PessoaDocumentoCollection pessoaDocumentoCollection = new PessoaDocumentoCollection();

        CotistaCollection cotistaCollection = new CotistaCollection();
        PermissaoCotistaCollection permissaoCotistaCollection = new PermissaoCotistaCollection();

        PessoaVinculoCollection pessoaVinculoCollection = new PessoaVinculoCollection();

        ContaCorrenteCollection contaCorrenteCollection = new ContaCorrenteCollection();

        Usuario usuario = new Usuario();

        string login = HttpContext.Current.User.Identity.Name;
        if (String.IsNullOrEmpty(login))
        {
            //Quando carregando de web service não temos o Identity - olhar para o campo CriadoPor
            if (this.valoresExcelCadastroCotista.Count > 0)
            {
                login = this.valoresExcelCadastroCotista[0].CriadoPor;
            }
        }

        bool usuarioEncontrado = usuario.BuscaUsuario(login);
        for (int i = 0; i < this.valoresExcelCadastroCotista.Count; i++)
        {
            ValoresExcelCadastroCotista valoresExcel = this.valoresExcelCadastroCotista[i];
            //                    
            #region Verifica se já Existe Pessoa ou se já está na collection
            Pessoa pessoa = new Pessoa();

            Cotista cotista = new Cotista();
            cotista.LoadByPrimaryKey(valoresExcel.IdCotista);

            if (!pessoa.LoadByPrimaryKey(cotista.IdPessoa.Value))
            {
                throw new Exception("Pessoa não encontrada. Id: " + valoresExcel.IdCotista);
            }

            #endregion

            #region Pessoa
            pessoa = pessoaCollection.AttachEntity(pessoa);
            pessoa.Nome = valoresExcel.Nome.Trim();
            pessoa.Apelido = valoresExcel.Apelido == null ? pessoa.Nome : valoresExcel.Apelido.Trim();
            pessoa.Tipo = (byte)valoresExcel.TipoPessoa;
            pessoa.DataUltimaAlteracao = DateTime.Now;
            int lengthCnpj = pessoa.Tipo == (byte)TipoPessoa.Fisica ? 11 : 14;
            string cnpjNumOnly = string.Join(null, System.Text.RegularExpressions.Regex.Split(valoresExcel.Cnpj, "[^\\d]"))
                .PadLeft(lengthCnpj, '0');

            pessoa.Cpfcnpj = cnpjNumOnly;
            pessoa.PessoaVinculada = valoresExcel.PessoaVinculada;
            if (valoresExcel.SituacaoLegal != null)
            {
                pessoa.SituacaoLegal = (byte)valoresExcel.SituacaoLegal;
            }
            else
            {
                pessoa.SituacaoLegal = null;
            }
            //
            if (valoresExcel.EstadoCivilCotista != null)
            {
                pessoa.EstadoCivil = (byte)valoresExcel.EstadoCivilCotista;
            }
            else
            {
                pessoa.EstadoCivil = null;
            }
            if (!String.IsNullOrEmpty(valoresExcel.Sexo))
            {
                pessoa.Sexo = valoresExcel.Sexo.Trim().ToUpper();
            }
            else
            {
                pessoa.Sexo = null;
            }
            if (valoresExcel.DataNascimento.HasValue)
            {
                pessoa.DataNascimento = valoresExcel.DataNascimento;
            }
            else
            {
                pessoa.DataNascimento = null;
            }
            if (!String.IsNullOrEmpty(valoresExcel.Profissao))
            {
                pessoa.Profissao = valoresExcel.Profissao.Trim();
            }
            else
            {
                pessoa.Profissao = null;
            }
            if (!String.IsNullOrEmpty(valoresExcel.UFNaturalidade))
            {
                pessoa.UFNaturalidade = valoresExcel.UFNaturalidade.Trim();
            }
            else
            {
                pessoa.UFNaturalidade = null;
            }
            if (!String.IsNullOrEmpty(valoresExcel.CodigoInterface))
            {
                pessoa.CodigoInterface = valoresExcel.CodigoInterface;
            }
            else
            {
                pessoa.CodigoInterface = null;
            }

            if (valoresExcel.PaisNacionalidade != null)
            {
                pessoa.Nacionalidade = valoresExcel.PaisNacionalidade;
            }
            if (!string.IsNullOrEmpty(valoresExcel.PoliticamenteExposta))
            {
                pessoa.PessoaPoliticamenteExposta = valoresExcel.PoliticamenteExposta;
            }
            if (valoresExcel.DataVencimentoCadastro != null)
            {
                pessoa.DataVencimentoCadastro = valoresExcel.DataVencimentoCadastro;
            }
            if (!string.IsNullOrEmpty(valoresExcel.NomePai))
            {
                pessoa.FiliacaoNomePai = valoresExcel.NomePai;
            }
            if (!string.IsNullOrEmpty(valoresExcel.NomeMae))
            {
                pessoa.FiliacaoNomeMae = valoresExcel.NomeMae;
            }

            #endregion

            #region PessoaDocumento
            if (!String.IsNullOrEmpty(valoresExcel.Rg))
            {
                //Remover e atualizar se ja existir
                PessoaDocumentoCollection rgs = new PessoaDocumentoCollection();
                rgs.Query.Where(rgs.Query.IdPessoa.Equal(pessoa.IdPessoa),
                    rgs.Query.TipoDocumento.Equal((byte)TipoDocumentoPessoa.CedulaIdentidade_RG));

                if (rgs.Load(rgs.Query))
                {
                    rgs.MarkAllAsDeleted();
                    rgs.Save();
                }

                PessoaDocumento p1 = pessoaDocumentoCollection.AddNew();
                p1.IdPessoa = pessoa.IdPessoa;
                p1.TipoDocumento = (byte)TipoDocumentoPessoa.CedulaIdentidade_RG;

                if (!string.IsNullOrEmpty(valoresExcel.Rg))
                    p1.NumeroDocumento = StringExt.Truncate(valoresExcel.Rg.Trim(), 13);

                if (!String.IsNullOrEmpty(valoresExcel.EmissorRg))
                {
                    p1.OrgaoEmissor = StringExt.Truncate(valoresExcel.EmissorRg.Trim(), 7);
                }
                if (valoresExcel.DataEmissaoRg.HasValue)
                {
                    p1.DataExpedicao = valoresExcel.DataEmissaoRg;
                }
                if (!String.IsNullOrEmpty(valoresExcel.UFEmissor))
                {
                    p1.UFEmissor = StringExt.Truncate(valoresExcel.UFEmissor.Trim(), 7);
                }
            }

            #endregion

            #region PessoaEndereco
            if (valoresExcel.PessoaEnderecoList != null && valoresExcel.PessoaEnderecoList.Count > 0)
            {
                //Remover e atualizar se ja existir
                var enderecos = new PessoaEnderecoCollection();
                enderecos.Query.Where(enderecos.Query.IdPessoa.Equal(pessoa.IdPessoa));

                if (enderecos.Load(enderecos.Query))
                {
                    enderecos.MarkAllAsDeleted();
                    enderecos.Save();
                }

                valoresExcel.PessoaEnderecoList.ForEach(t =>
                {
                    var pessoaEndereco = pessoaEnderecoCollection.AddNew();
                    pessoaEndereco.IdPessoa = pessoa.IdPessoa.Value;
                    pessoaEndereco.RecebeCorrespondencia = t.RecebeCorrespondencia;
                    pessoaEndereco.Endereco = t.Endereco;
                    pessoaEndereco.Numero = t.Numero;
                    pessoaEndereco.Complemento = t.Complemento;
                    pessoaEndereco.Bairro = t.Bairro;
                    pessoaEndereco.Cidade = t.Cidade;
                    pessoaEndereco.Cep = t.Cep;
                    pessoaEndereco.Uf = t.Uf;
                    pessoaEndereco.Pais = t.Pais;
                    pessoaEndereco.TipoEndereco = t.TipoEndereco;

                });
            }
            else
            {
                if (valoresExcel.EnderecoPessoa.HasValue())
                {
                    //Remover e atualizar se ja existir
                    PessoaEnderecoCollection enderecos = new PessoaEnderecoCollection();
                    enderecos.Query.Where(enderecos.Query.IdPessoa.Equal(pessoa.IdPessoa),
                        enderecos.Query.TipoEndereco.Equal(TipoEnderecoPessoa.Residencial));

                    if (enderecos.Load(enderecos.Query))
                    {
                        enderecos.MarkAllAsDeleted();
                        enderecos.Save();
                    }

                    PessoaEndereco pessoaEndereco = pessoaEnderecoCollection.AddNew();
                    pessoaEndereco.IdPessoa = valoresExcel.IdCotista;
                    pessoaEndereco.RecebeCorrespondencia = "S";
                    pessoaEndereco.Endereco = valoresExcel.EnderecoPessoa.Rua == null ? "" : valoresExcel.EnderecoPessoa.Rua.Trim();
                    pessoaEndereco.Numero = valoresExcel.EnderecoPessoa.Numero == null ? "" : valoresExcel.EnderecoPessoa.Numero.Trim();
                    pessoaEndereco.Complemento = valoresExcel.EnderecoPessoa.Complemento == null ? "" : valoresExcel.EnderecoPessoa.Complemento.Trim();
                    pessoaEndereco.Bairro = valoresExcel.EnderecoPessoa.Bairro == null ? "" : valoresExcel.EnderecoPessoa.Bairro.Trim();
                    pessoaEndereco.Cidade = valoresExcel.EnderecoPessoa.Cidade == null ? "" : valoresExcel.EnderecoPessoa.Cidade.Trim();
                    pessoaEndereco.Cep = valoresExcel.EnderecoPessoa.Cep == null ? "" : valoresExcel.EnderecoPessoa.Cep.Trim();
                    pessoaEndereco.Uf = valoresExcel.EnderecoPessoa.Uf == null ? "" : valoresExcel.EnderecoPessoa.Uf.Trim().ToUpper();
                    pessoaEndereco.Pais = valoresExcel.EnderecoPessoa.Pais == null ? "" : valoresExcel.EnderecoPessoa.Pais.Trim();

                    pessoaEndereco.TipoEndereco = TipoEnderecoPessoa.Residencial;
                }

                if (valoresExcel.EnderecoComercialPessoa.HasValue())
                {
                    //Remover e atualizar se ja existir
                    PessoaEnderecoCollection enderecos = new PessoaEnderecoCollection();
                    enderecos.Query.Where(enderecos.Query.IdPessoa.Equal(pessoa.IdPessoa),
                        enderecos.Query.TipoEndereco.Equal(TipoEnderecoPessoa.Comercial));

                    if (enderecos.Load(enderecos.Query))
                    {
                        enderecos.MarkAllAsDeleted();
                        enderecos.Save();
                    }

                    PessoaEndereco pessoaEnderecoComercial = pessoaEnderecoCollection.AddNew();
                    pessoaEnderecoComercial.IdPessoa = valoresExcel.IdCotista;
                    pessoaEnderecoComercial.RecebeCorrespondencia = "S";
                    pessoaEnderecoComercial.Endereco = valoresExcel.EnderecoComercialPessoa.Rua == null ? "" : valoresExcel.EnderecoComercialPessoa.Rua.Trim();
                    pessoaEnderecoComercial.Numero = valoresExcel.EnderecoComercialPessoa.Numero == null ? "" : valoresExcel.EnderecoComercialPessoa.Numero.Trim();
                    pessoaEnderecoComercial.Complemento = valoresExcel.EnderecoComercialPessoa.Complemento == null ? "" : valoresExcel.EnderecoComercialPessoa.Complemento.Trim();
                    pessoaEnderecoComercial.Bairro = valoresExcel.EnderecoComercialPessoa.Bairro == null ? "" : valoresExcel.EnderecoComercialPessoa.Bairro.Trim();
                    pessoaEnderecoComercial.Cidade = valoresExcel.EnderecoComercialPessoa.Cidade == null ? "" : valoresExcel.EnderecoComercialPessoa.Cidade.Trim();
                    pessoaEnderecoComercial.Cep = valoresExcel.EnderecoComercialPessoa.Cep == null ? "" : valoresExcel.EnderecoComercialPessoa.Cep.Trim();
                    pessoaEnderecoComercial.Uf = valoresExcel.EnderecoComercialPessoa.Uf == null ? "" : valoresExcel.EnderecoComercialPessoa.Uf.Trim().ToUpper();
                    pessoaEnderecoComercial.Pais = valoresExcel.EnderecoComercialPessoa.Pais == null ? "" : valoresExcel.EnderecoComercialPessoa.Pais.Trim();

                    pessoaEnderecoComercial.TipoEndereco = TipoEnderecoPessoa.Comercial;
                    //
                    // Marcar RecebeCorrespondencia do endereço Normal se existir para N
                    if (valoresExcel.EnderecoPessoa.HasValue())
                    {
                        pessoaEnderecoCollection[pessoaEnderecoCollection.Count - 1].RecebeCorrespondencia = "N";
                    }
                }
            }
            #endregion

            #region PessoaTelefone
            if (valoresExcel.PessoaTelefoneList != null && valoresExcel.PessoaTelefoneList.Count > 0)
            {
                //Remover e atualizar se ja existir
                var telefones = new PessoaTelefoneCollection();
                telefones.Query.Where(telefones.Query.IdPessoa.Equal(pessoa.IdPessoa));

                if (telefones.Load(telefones.Query))
                {
                    telefones.MarkAllAsDeleted();
                    telefones.Save();
                }

                valoresExcel.PessoaTelefoneList.ForEach(t =>
                {
                    var pessoaTelefone = pessoaTelefoneCollection.AddNew();
                    pessoaTelefone.IdPessoa = pessoa.IdPessoa.Value;
                    pessoaTelefone.Ddi = t.Ddi;
                    pessoaTelefone.Ddd = t.Ddd;
                    pessoaTelefone.Numero = t.Numero;
                    pessoaTelefone.TipoTelefone = t.TipoTelefone;
                    pessoaTelefone.Ramal = t.Ramal;
                });
            }
            else {
                if (!String.IsNullOrEmpty(valoresExcel.EnderecoPessoa.FoneNumero))
                {
                    //Remover e atualizar se ja existir
                    PessoaTelefoneCollection telefones = new PessoaTelefoneCollection();
                    telefones.Query.Where(telefones.Query.IdPessoa.Equal(pessoa.IdPessoa),
                        telefones.Query.TipoTelefone.Equal((byte)TipoTelefonePessoa.Residencial));

                    if (telefones.Load(telefones.Query))
                    {
                        telefones.MarkAllAsDeleted();
                        telefones.Save();
                    }

                    PessoaTelefone pessoaTelefone = pessoaTelefoneCollection.AddNew();
                    //
                    pessoaTelefone.IdPessoa = pessoa.IdPessoa.Value;
                    pessoaTelefone.Ddi = "";
                    pessoaTelefone.Ddd = "";
                    pessoaTelefone.Numero = valoresExcel.EnderecoPessoa.FoneNumero;
                    pessoaTelefone.TipoTelefone = (byte)TipoTelefonePessoa.Residencial;
                }
                if (!String.IsNullOrEmpty(valoresExcel.EnderecoComercialPessoa.FoneNumero))
                {
                    //Remover e atualizar se ja existir
                    PessoaTelefoneCollection telefones = new PessoaTelefoneCollection();
                    telefones.Query.Where(telefones.Query.IdPessoa.Equal(pessoa.IdPessoa),
                        telefones.Query.TipoTelefone.Equal((byte)TipoTelefonePessoa.Comercial));

                    if (telefones.Load(telefones.Query))
                    {
                        telefones.MarkAllAsDeleted();
                        telefones.Save();
                    }

                    PessoaTelefone pessoaTelefone = pessoaTelefoneCollection.AddNew();
                    //
                    pessoaTelefone.IdPessoa = pessoa.IdPessoa.Value;
                    pessoaTelefone.Ddi = "";
                    pessoaTelefone.Ddd = "";
                    pessoaTelefone.Numero = valoresExcel.EnderecoComercialPessoa.FoneNumero;
                    pessoaTelefone.TipoTelefone = (byte)TipoTelefonePessoa.Comercial;
                }
            }
            #endregion

            #region PessoaEmail
            if (valoresExcel.PessoaEmailList != null && valoresExcel.PessoaEmailList.Count > 0)
            {
                //Remover e atualizar se ja existir
                PessoaEmailCollection emails = new PessoaEmailCollection();
                emails.Query.Where(emails.Query.IdPessoa.Equal(pessoa.IdPessoa));

                if (emails.Load(emails.Query))
                {
                    emails.MarkAllAsDeleted();
                    emails.Save();
                }
                valoresExcel.PessoaEmailList.ForEach(t =>
                {
                    var pessoaEmail = pessoaEmailCollection.AddNew();
                    pessoaEmail.IdPessoa = pessoa.IdPessoa.Value;
                    pessoaEmail.Email = t.Email;
                }
                );
            }
            else {
                if (!String.IsNullOrEmpty(valoresExcel.EnderecoPessoa.Email) ||
                    !String.IsNullOrEmpty(valoresExcel.EnderecoComercialPessoa.Email))
                {
                    //Remover e atualizar se ja existir
                    PessoaEmailCollection emails = new PessoaEmailCollection();
                    emails.Query.Where(emails.Query.IdPessoa.Equal(pessoa.IdPessoa));

                    if (emails.Load(emails.Query))
                    {
                        emails.MarkAllAsDeleted();
                        emails.Save();
                    }
                }

                if (!String.IsNullOrEmpty(valoresExcel.EnderecoPessoa.Email))
                {
                    PessoaEmail pessoaEmail = pessoaEmailCollection.AddNew();
                    //
                    pessoaEmail.IdPessoa = pessoa.IdPessoa.Value;
                    pessoaEmail.Email = valoresExcel.EnderecoPessoa.Email;
                }

                if (!String.IsNullOrEmpty(valoresExcel.EnderecoComercialPessoa.Email))
                {
                    PessoaEmail pessoaEmail = pessoaEmailCollection.AddNew();
                    //
                    pessoaEmail.IdPessoa = pessoa.IdPessoa.Value;
                    pessoaEmail.Email = valoresExcel.EnderecoComercialPessoa.Email;
                }
            }
            #endregion

            #region Cotista
            cotista = cotistaCollection.AttachEntity(cotista);
            cotista.Nome = valoresExcel.Nome.Trim();
            cotista.Apelido = valoresExcel.Apelido == null ? cotista.Nome : valoresExcel.Apelido.Trim();

            cotista.IsentoIR = valoresExcel.IsentoIR.ToUpper();
            cotista.IsentoIOF = valoresExcel.IsentoIOF.ToUpper();
            cotista.StatusAtivo = (byte)valoresExcel.StatusCotista;

            cotista.TipoTributacao = (byte)valoresExcel.TipoTributacaoCotista;

            if (valoresExcel.TipoCotistaCVM != null)
            {
                cotista.TipoCotistaCVM = (int)valoresExcel.TipoCotistaCVM;
            }

            if (valoresExcel.TipoCotistaAnbima != null)
            {
                cotista.TipoCotistaAnbima = (int)valoresExcel.TipoCotistaAnbima;
            }

            if (!String.IsNullOrEmpty(valoresExcel.CodigoInterfaceCotista))
            {
                cotista.CodigoInterface = valoresExcel.CodigoInterfaceCotista;
            }
            else {
                if (!String.IsNullOrEmpty(valoresExcel.CodigoInterface))
                {
                    cotista.CodigoInterface = valoresExcel.CodigoInterface;
                }
            }
            if (!String.IsNullOrEmpty(valoresExcel.PendenciaCadastral))
            {
                cotista.PendenciaCadastral = valoresExcel.PendenciaCadastral.Trim();
            }
            #endregion

            #region Adiciona em PermissaoCotista
            if (usuarioEncontrado)
            {
                PermissaoCotista permissaoExistente = new PermissaoCotista();
                if (!permissaoExistente.LoadByPrimaryKey(usuario.IdUsuario.Value, cotista.IdCotista.Value))
                {
                    PermissaoCotista permissaoCotista = permissaoCotistaCollection.AddNew();
                    permissaoCotista.IdCotista = cotista.IdCotista.Value;
                    permissaoCotista.IdUsuario = usuario.IdUsuario.Value;
                }
            }
            #endregion

            #region Cotitular
            if (valoresExcel.IdTitular.HasValue)
            {
                PessoaVinculo pessoaVinculoExistente = new PessoaVinculo();
                if (!pessoaVinculoExistente.LoadByPrimaryKey(valoresExcel.IdTitular.Value, valoresExcel.IdCotista))
                {
                    PessoaVinculo pessoaVinculo = pessoaVinculoCollection.AddNew();
                    pessoaVinculo.IdPessoa = valoresExcel.IdTitular;
                    pessoaVinculo.IdPessoaVinculada = valoresExcel.IdCotista;
                    pessoaVinculo.TipoVinculo = Convert.ToByte(TipoVinculoPessoa.Cotitular);
                }
            }
            #endregion

            #region ContaCorrente
            if (valoresExcel.InformacoesBancarias != null && valoresExcel.InformacoesBancarias.Count > 0)
            {
                ContaCorrenteCollection contasDeletar = new ContaCorrenteCollection();
                contasDeletar.Query.Where(contasDeletar.Query.IdPessoa.Equal(pessoa.IdPessoa)
                    );

                if (contasDeletar.Load(contasDeletar.Query))
                {
                    contasDeletar.MarkAllAsDeleted();
                    contasDeletar.Save();
                }

                foreach (Financial.Integracao.Excel.ValoresExcelCadastroCotista.InformacaoBancaria informacaoBancaria in valoresExcel.InformacoesBancarias)
                {
                    Banco banco = new BancoCollection().BuscaBancoPorCodigoCompensacao(informacaoBancaria.CodigoCompensacaoBanco);
                    if (banco == null)
                    {
                        banco = new Banco();
                        banco.CodigoCompensacao = informacaoBancaria.CodigoCompensacaoBanco;
                        banco.Nome = informacaoBancaria.CodigoCompensacaoBanco;
                        banco.Save();
                    }

                    Agencia agencia = new AgenciaCollection().BuscaAgenciaPorBancoECodigo(banco.IdBanco.Value, informacaoBancaria.CodigoAgencia);
                    if (agencia == null)
                    {
                        agencia = new Agencia();
                        agencia.IdBanco = banco.IdBanco;
                        agencia.Nome = informacaoBancaria.NomeAgencia;
                        agencia.Codigo = informacaoBancaria.CodigoAgencia;
                        agencia.DigitoAgencia = informacaoBancaria.DigitoAgencia;
                        agencia.Save();
                    }

                    ContaCorrente conta = contaCorrenteCollection.AddNew();
                    conta.IdAgencia = agencia.IdAgencia;
                    conta.IdBanco = banco.IdBanco;
                    conta.DigitoConta = informacaoBancaria.DigitoConta;
                    conta.IdMoeda = informacaoBancaria.MoedaConta;
                    conta.IdPessoa = valoresExcel.IdCotista;
                    conta.Numero = informacaoBancaria.NumeroConta;
                }

            }
            #endregion
        }

        using (esTransactionScope scope = new esTransactionScope())
        {
            pessoaCollection.Save();
            pessoaEnderecoCollection.Save();
            pessoaTelefoneCollection.Save();
            pessoaEmailCollection.Save();
            pessoaDocumentoCollection.Save();
            cotistaCollection.Save();
            permissaoCotistaCollection.Save();
            pessoaVinculoCollection.Save();
            //
            scope.Complete();
        }
    }

    /// <summary>
    /// Processa a Planilha de Cotista após ter acabado o Upload
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void uplCadastroCotista_FileUploadComplete(object sender, FileUploadCompleteEventArgs e)
    {
        e.CallbackData = "";

        #region Trata Extensão Válida
        if (!ValoresExcelUtil.isExtensaoExcel(e.UploadedFile.FileName.Trim()))
        {
            e.CallbackData = "Importação Cadastro Cotista - Formato de Arquivo Inválido. Formato permitido: .xls ou .xlsx\n\n";
            return;
        }
        #endregion

        // Stream de bytes com o Conteudo do Arquivo Excel
        Stream sr = e.UploadedFile.FileContent;

        #region Processamento Arquivo Excel

        //Reseta vetor de dados
        this.valoresExcelCadastroCotista = new List<ValoresExcelCadastroCotista>();

        try
        {
            // Ler Arquivo
            this.LerArquivoCadastroCotista(sr);
            // Carrega Arquivo
            this.CarregaCadastroCotista();
        }
        catch (Exception e2)
        {
            e.CallbackData = "Importação Cadastro Cotista - " + e2.Message;
            return;
        }

        new HistoricoLog().InsereHistoricoLog(DateTime.Now, DateTime.Now,
                        "Importação Planilha Cadastro Cotista",
                        HttpContext.Current.User.Identity.Name, UtilitarioWeb.GetIP(Request), "", HistoricoLogOrigem.Outros);

        #endregion
    }
}