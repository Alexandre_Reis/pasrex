﻿using System;
using System.Collections.Generic;
using System.IO;

using Financial.Integracao.Excel;
using Financial.Integracao.Excel.Util;
using Financial.Web.Common;
using Financial.Bolsa;

using EntitySpaces.Interfaces;

using DevExpress.Web;
using Financial.Common.Enums;
using Financial.Fundo;
using Financial.Bolsa.Enums;
using Financial.Security;
using System.Web;
using Financial.Web.Util;
using Financial.Security.Enums;
using Financial.Contabil;
using Financial.Investidor;
using Financial.Contabil.Enums;

public partial class ImportacaoBasePage : BasePage {

    #region Estrutura ContabLancamento

    /// <summary>
    /// Armazena os Valores de ContabLancamento presentes num arquivo Excel
    /// </summary>
    internal class ValoresExcelContabLancamento : ContabLancamento {
    }
    #endregion

    /* Estrutura do Excel */
    private List<ValoresExcelContabLancamento> valoresExcelContabLancamento = new List<ValoresExcelContabLancamento>();
    
    /// <summary>
    /// Leitura de um arquivo Excel
    /// Saida: List<ValoresExcel> com os valores armazenados no arquivo
    /// </summary>
    /// <param name="streamExcel">Stream de bytes de um arquivo Excel</param>
    /// <exception cref="Exception">Se ocorreu problemas</exception>
    private void LerArquivoContabLancamento(Stream streamExcel) {

        #region Leitura do Arquivo Excel

        // Open Spreadsheet
        Bytescout.Spreadsheet.Spreadsheet document = new Bytescout.Spreadsheet.Spreadsheet();
        document.LoadFromStream(streamExcel);

        Bytescout.Spreadsheet.Worksheet workSheet = document.Workbook.Worksheets[0];

        //
        #region Confere Formato Arquivo
       
        string[] colunasConferencias = new string[] { "IdCliente", "DataLancamento", "DataRegistro",
                                                      "Valor", "ContaDebito","ContaCredito", 
                                                      "Origem", "Descricao", "IdPlano"
                                                    };

        bool formato = ValoresExcelUtil.isExcelFormatoValido(colunasConferencias, workSheet);

        if (!formato) {
            string mensagem = "Formato Interno do Arquivo Inválido.\n";
            mensagem += "Linha 1 do Arquivo deve ser: \n";
            for (int i = 0; i < colunasConferencias.Length; i++) {
                int coluna = i + 1;
                mensagem += "\tColuna " + coluna + ": " + colunasConferencias[i] + "\n";
            }

            document.Close();
            document.Dispose();

            throw new Exception(mensagem);
        }
        #endregion

        //
        int index = 0;
        // This row,column index should be changed as per your need.
        // i.e. which cell in the excel you are interesting to read.
        //
        /* Formato: 1) IdCliente      - 2) DataLancamento    - 3) DataRegistro
         *          4) Valor          - 5) ContaDebito       - 6) ContaCredito 
         *          7) Origem         - 8) Descricao         - 9) IdPlano
         */

        int linha = 1;
        int coluna1 = 0, coluna2 = 1, coluna3 = 2;
        int coluna4 = 3, coluna5 = 4, coluna6 = 5;
        int coluna7 = 6, coluna8 = 7, coluna9 = 8;
        int coluna10 = 9, coluna11 = 10;

        //
        try { 
            // Enquanto Data tiver valor
            while (workSheet.Cell(linha, coluna1).Value != null) {
                ValoresExcelContabLancamento item = new ValoresExcelContabLancamento();

                try
                {
                    item.IdCliente = workSheet.Cell(linha, coluna1).ValueAsInteger;
                }
                catch (Exception ex) {
                    throw new Exception(ex.Message + " - IdCliente: linha " + (linha + 1));
                }

                if (workSheet.Cell(linha, coluna2).Value == null)
                    throw new Exception("DataLancamento não informada: linha " + (linha + 1));
                try
                {
                    item.DataLancamento = workSheet.Cell(linha, coluna2).ValueAsDateTime;
                }
                catch (Exception ex) {
                    throw new Exception(ex.Message + " - DataLancamento: linha " + (linha + 1));
                }

                if (workSheet.Cell(linha, coluna3).Value == null)
                    throw new Exception("DataRegistro não informada: linha " + (linha + 1));
                try
                {
                    item.DataRegistro = workSheet.Cell(linha, coluna3).ValueAsDateTime;
                }
                catch (Exception ex) {
                    throw new Exception(ex.Message + " - DataRegistro: linha " + (linha + 1));
                }

                if (workSheet.Cell(linha, coluna4).Value == null)
                    throw new Exception("Valor não informada: linha " + (linha + 1));
                try
                {
                    item.Valor = Convert.ToDecimal(workSheet.Cell(linha, coluna4).Value);
                }
                catch (Exception ex) {
                    throw new Exception(ex.Message + " - Valor: linha " + (linha + 1));
                }

                if (workSheet.Cell(linha, coluna5).Value == null)
                    throw new Exception("ContaDebito não informada: linha " + (linha + 1));
                try
                {
                    item.ContaDebito = workSheet.Cell(linha, coluna5).ValueAsString.Trim();
                }
                catch (Exception ex) {
                    throw new Exception(ex.Message + " - ContaDebito: linha " + (linha + 1));
                }

                if (workSheet.Cell(linha, coluna6).Value == null)
                    throw new Exception("ContaCredito não informada: linha " + (linha + 1));
                try
                {
                    item.ContaCredito = workSheet.Cell(linha, coluna6).ValueAsString.Trim();                
                }
                catch (Exception ex) {
                    throw new Exception(ex.Message + " - ContaCredito: linha " + (linha + 1));
                }

                if (workSheet.Cell(linha, coluna7).Value == null)
                    throw new Exception("Origem não informada: linha " + (linha + 1));
                try
                {
                    item.Origem = workSheet.Cell(linha, coluna7).ValueAsInteger;
                }
                catch (Exception ex) {
                    throw new Exception(ex.Message + " - Origem: linha " + (linha + 1));
                }

                if (workSheet.Cell(linha, coluna8).Value == null)
                    throw new Exception("Descricao não informada: linha " + (linha + 1));
                try
                {
                    item.Descricao = workSheet.Cell(linha, coluna8).ValueAsString.Trim();
                }
                catch (Exception ex) {
                    throw new Exception(ex.Message + " - Descricao: linha " + (linha + 1));
                }

                if (workSheet.Cell(linha, coluna9).Value == null)
                    throw new Exception("IdPlano não informada: linha " + (linha + 1));
                try
                {
                    item.IdPlano = workSheet.Cell(linha, coluna9).ValueAsInteger;
                }
                catch (Exception ex) {
                    throw new Exception(ex.Message + " - IdPlano: linha " + (linha + 1));
                }

                this.valoresExcelContabLancamento.Add(item);
                
                index++;
                linha = 1 + index;
                //                
                //Console.WriteLine("{0},{1},{2},{3},{4},{5},{6},{7}, {8}, {9}, {10}, {11}, {12}", 
                //            item.CdAtivoBolsa, item.Especificao,
                //            item.Descricao, item.TipoMercado,
                //            item.PuExercicio, item.DataVencimento,
                //            item.CodigoAtivoObjeto, item.CodigoIsin);
            }
        }
        catch (Exception ex) {
            document.Close();
            document.Dispose();

            throw new Exception(ex.Message);
        }
        #endregion

        document.Close();
        document.Dispose();
    }

    /// <summary>
    /// Gera e Carrega os contabLancamentos de acordo com o objeto List<ValoresExcel>
    /// </summary>
    private void CarregaContabLancamento() {
        ContabLancamentoCollection contabLancamentoCollection = new ContabLancamentoCollection();
        //    
        for (int i = 0; i < this.valoresExcelContabLancamento.Count; i++) {
            ValoresExcelContabLancamento valoresExcel = this.valoresExcelContabLancamento[i];
            //

            #region Testa Cliente Existente
            Cliente cliente = new Cliente();
            List<esQueryItem> campos = new List<esQueryItem>();
            campos.Add(cliente.Query.IdCliente);
            if (!cliente.LoadByPrimaryKey(campos, valoresExcel.IdCliente.Value)) {
                throw new Exception("Cliente não existente : " + valoresExcel.IdCliente.Value);
            }
            #endregion

            #region Testa idPlano
            ContabPlano c = new ContabPlano();
            
            if (!c.LoadByPrimaryKey(valoresExcel.IdPlano.Value)) {
                throw new Exception("Contab Plano não existente : " + valoresExcel.IdPlano.Value);
            }
            #endregion

            #region Testa idContaDebito
            
            ContabContaCollection contabContaCollection = new ContabContaCollection();
            contabContaCollection.Query.Select(contabContaCollection.Query.IdConta);
            contabContaCollection.Query.Where(contabContaCollection.Query.Codigo == valoresExcel.ContaDebito);

            contabContaCollection.Query.Load();

            int? idContaDebito = null;
            if (contabContaCollection.HasData) {
                idContaDebito = contabContaCollection[0].IdConta;
            }
            else {
                throw new Exception("Conta Débito não existente : " + valoresExcel.ContaDebito);
            }
            #endregion

            #region Testa idContaCredito

            contabContaCollection = new ContabContaCollection();
            contabContaCollection.Query.Select(contabContaCollection.Query.IdConta);
            contabContaCollection.Query.Where(contabContaCollection.Query.Codigo == valoresExcel.ContaCredito);

            contabContaCollection.Query.Load();

            int? idContaCredito = null;
            if (contabContaCollection.HasData) {
                idContaCredito = contabContaCollection[0].IdConta;
            }
            else {
                throw new Exception("Conta Crédito não existente : " + valoresExcel.ContaCredito);
            }
            #endregion

            //#region Testa Origem
            //List<int> valoresPossiveis = this.GetValuesOrigem();

            //if (!valoresPossiveis.Contains(valoresExcel.Origem.Value)) {
            //    int linha = i + 2;
            //    throw new Exception("Origem Inválida. Linha: " + linha);
            //}            
            //#endregion
            
            #region ContabLancamento
            
            ContabLancamento contabLancamento = contabLancamentoCollection.AddNew();
            
            // Dados do Arquivo
            contabLancamento.IdCliente = valoresExcel.IdCliente;
            contabLancamento.DataLancamento = valoresExcel.DataLancamento;
            contabLancamento.DataRegistro = valoresExcel.DataRegistro;
            //
            contabLancamento.IdContaDebito = idContaDebito;
            contabLancamento.IdContaCredito = idContaCredito;
            //
            contabLancamento.Valor = valoresExcel.Valor;
            contabLancamento.ContaDebito = valoresExcel.ContaDebito;
            contabLancamento.ContaCredito = valoresExcel.ContaCredito;
            contabLancamento.Origem = valoresExcel.Origem;
            contabLancamento.Descricao = valoresExcel.Descricao;
            contabLancamento.IdPlano = valoresExcel.IdPlano;
            contabLancamento.Fonte = (byte)FonteLancamentoContabil.Manual;
            //
            #endregion
        }
        
        // Salva ContabLancamentoCollection presentes no Excel                
        contabLancamentoCollection.Save();
    }

    /// <summary>
    /// Processa a Planilha de Contab Lancamento após ter acabado o Upload
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void uplContabLancamento_FileUploadComplete(FileUploadCompleteEventArgs e, ASPxCheckBox checkIgnora) {
        e.CallbackData = "";

        #region Trata Extensão Válida
        if (!ValoresExcelUtil.isExtensaoExcel(e.UploadedFile.FileName.Trim())) {
            e.CallbackData = "Importação Contab Lançamento - Formato de Arquivo Inválido. Formato permitido: .xls ou .xlsx\n\n";
            return;
        }
        #endregion

        // Stream de bytes com o Conteudo do Arquivo Excel
        Stream sr = e.UploadedFile.FileContent;

        #region Processamento Arquivo Excel

        // Reseta Vetor de Dados
        this.valoresExcelContabLancamento = new List<ValoresExcelContabLancamento>();

        //
        try {
            // Ler Arquivo
            this.LerArquivoContabLancamento(sr);
            // Carrega Arquivo
            this.CarregaContabLancamento();
        }
        catch (Exception e2) {
            e.CallbackData = "Importação Contab Lançamento com problemas - " + e2.Message;
            return;
        }

        new HistoricoLog().InsereHistoricoLog(DateTime.Now, DateTime.Now,
                                "Importação Planilha ContabLancamento",
                                HttpContext.Current.User.Identity.Name, UtilitarioWeb.GetIP(Request), "", HistoricoLogOrigem.Outros);

        #endregion

    }

    /// <summary>
    /// Retorna a Lista de Valores Possíveis para a Origem
    /// </summary>
    public List<int> GetValuesOrigem() {

        List<int> origem = new List<int>(new int[] {
100,101,102,103,200,201,202,203,500,501,502,503,600,601,602,603,
1000,1001,1002,1003,1100,1101,1102,1103,1500,1501,1502,1503,
2000,2001,2002,2003,2500,2501,2502,2503,
3000,3001,3002,3003,3500,3501,3502,3503,
4000,4001,4002,4003,4500,4501,4600,4601,4650,4651,4700,4701,4750,4751,
5000,5001,5100,5101,5200,5201,5500,5600,5700,5800,
6000,6100,6200,6300,
7000,7001,
8000,8100,8200,8300,8400,8500,8600,8700,8800,8900,
9000,9100,                     
10000,10100,10200,10300,10400,10401,10500,10501,10600,10601,
10700,10701,10800,10900,
11000,11100,
14000,14001,14002,14003,14500,14501,14502,14503,14700,14750,14800,14850,       
15000,15010,15050,15060,15100,15110,
20000,20001,20002,20003,
20100,20101,20102,20103,20500,20501,20502,20503,20600,20601,20602,20603,
21000,21001,21100,21101,21102,21500,21600,21601,21602,
22000,22100,22101,22102,22500,22600,22601,22602,
23000,23100,23101,23102,23500,23501,23550,23551,
24000,24001,24050,24051,
25000,25001,25100,25101,25200,25201,26000,26001,27000,27001,
30000,30500,30600,30601,31000,31100,31200,32000,32001,32002,32003,32500,32501,33000,34000,34100,
40000,40001,40100,40101,41000,41001,
50000,51000,51100,52000,52001,
60000,60500,60600,60601,60700,60800,62000,62500,62600,62601,62700,62800,64000,64100,64200,
80000,80100,
999999 
});

        return origem;

    }
}