﻿using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Financial.Web.Common;
using System.IO;
using Financial.Integracao.Excel.Util;
using Financial.Integracao.Excel;
using EntitySpaces.Interfaces;
using Financial.Common;
using Financial.Bolsa;
using Financial.Bolsa.Enums;
using Financial.Investidor;
using System.Collections.Generic;
using Financial.Investidor.Enums;
using DevExpress.Web;
using Financial.Common.Exceptions;
using Financial.Security;
using Financial.Web.Util;
using Financial.Security.Enums;
using Financial.Common.Enums;

public partial class ImportacaoBasePage : BasePage {

    /* Estrutura do Excel */
    public List<ValoresExcelOrdemBolsa> valoresExcelOrdemBolsa = new List<ValoresExcelOrdemBolsa>();

    /// <summary>
    /// Leitura de um arquivo Excel
    /// Saida: List<ValoresExcel> Com os valores armazenados no arquivo
    /// </summary>
    /// <param name="streamExcel">Stream de bytes de um arquivo Excel</param>
    /// <exception cref="Exception">Se ocorreu problemas</exception>
    public void LerArquivoOrdemBolsa(Stream streamExcel) {

        #region Leitura do Arquivo Excel

        // Open Spreadsheet
        Bytescout.Spreadsheet.Spreadsheet document = new Bytescout.Spreadsheet.Spreadsheet();
        document.LoadFromStream(streamExcel);

        Bytescout.Spreadsheet.Worksheet workSheet = document.Workbook.Worksheets[0];

        #region Confere Formato Arquivo
        string[] colunasConferencias = new string[] 
                                {"IdCliente","CdAtivoBolsa","CodigoBovespa","TipoOrdem","Data",
                                 "PU","Valor","Quantidade","PercentualDesconto","Trader","CalculaDespesas"
                                };

        bool formato = ValoresExcelUtil.isExcelFormatoValido(colunasConferencias, workSheet);

        if (!formato) {
            string mensagem = "Formato Interno do Arquivo Inválido.\n";
            mensagem += "Linha 1 do Arquivo deve ser: \n";
            for (int i = 0; i < colunasConferencias.Length; i++) {
                int coluna = i + 1;
                mensagem += "\tColuna " + coluna + ": " + colunasConferencias[i] + "\n";
            }

            document.Close();
            document.Dispose();
            throw new Exception(mensagem);
        }
        #endregion

        int index = 0;
        //This row,column index should be changed as per your need.
        // i.e. which cell in the excel you are interesting to read.

        /* Formato: 1)IdCliente  - 2)CdAtivoBolsa    - 3)CodigoBovespa   - 4)TipoOrdem           - 5)Data
         *          6)PU         - 7)Valor           - 8)Quantidade      - 9)PercentualDesconto  - 10) Trader
         *          11)CalculaDespesas  - 12)LocalNegociacao    - 13)Clearing    - 14)LocalCustodia    - 14)Corretora    
         *          15)DataRegistro
         */

        int linha = 1;
        int coluna1 = 0, coluna2 = 1, coluna3 = 2,
            coluna4 = 3, coluna5 = 4, coluna6 = 5,
            coluna7 = 6, coluna8 = 7, coluna9 = 8,
            coluna10 = 9, coluna11 = 10, coluna12 = 11,
            coluna13 = 12, coluna14 = 13, coluna15 = 14,
            coluna16 =15;

        try {
            // Enquanto idCliente Tiver valor
            while (workSheet.Cell(linha, coluna1).Value != null) {
                ValoresExcelOrdemBolsa item = new ValoresExcelOrdemBolsa();

                item.IdCliente = workSheet.Cell(linha, coluna1).ValueAsInteger;
                
                if (workSheet.Cell(linha, coluna2).Value == null)
                    throw new Exception("CdAtivoBolsa não informado: linha " + (linha + 1));
                try {
                    item.CdAtivoBolsa = workSheet.Cell(linha, coluna2).ValueAsString;
                }
                catch (Exception ex) {
                    throw new Exception(ex.Message + " - CdAtivoBolsa: linha " + (linha + 1));
                }

                if (workSheet.Cell(linha, coluna3).Value == null)
                    throw new Exception("CodigoBovespa não informado: linha " + (linha + 1));
                try {
                    item.CodigoBovespa = workSheet.Cell(linha, coluna3).ValueAsInteger;
                }
                catch (Exception ex) {
                    throw new Exception(ex.Message + " - CodigoBovespa: linha " + (linha + 1));
                }

                if (workSheet.Cell(linha, coluna4).Value == null)
                    throw new Exception("TipoOrdem não informado: linha " + (linha + 1));
                try {
                    item.TipoOrdem = workSheet.Cell(linha, coluna4).ValueAsString;
                }
                catch (Exception ex) {
                    throw new Exception(ex.Message + " - TipoOrdem: linha " + (linha + 1));
                }

                if (workSheet.Cell(linha, coluna5).Value == null)
                    throw new Exception("DataOrdem não informada: linha " + (linha + 1));
                try {
                    item.DataOrdem = workSheet.Cell(linha, coluna5).ValueAsDateTime;
                }
                catch (Exception ex) {
                    throw new Exception(ex.Message + " - DataOrdem: linha " + (linha + 1));
                }

                if (workSheet.Cell(linha, coluna6).Value == null)
                    throw new Exception("Pu não informado: linha " + (linha + 1));
                try {
                    item.Pu = Convert.ToDecimal(workSheet.Cell(linha, coluna6).Value);
                }
                catch (Exception ex) {
                    throw new Exception(ex.Message + " - Pu: linha " + (linha + 1));
                }

                if (workSheet.Cell(linha, coluna7).Value != null &&
                    workSheet.Cell(linha, coluna7).Value.ToString().Trim() != "") {
                    try {
                        item.Valor = Convert.ToDecimal(workSheet.Cell(linha, coluna7).Value);
                    }
                    catch (Exception ex) {
                        throw new Exception(ex.Message + " - Valor: linha " + (linha + 1));
                    }
                }
                else {
                    item.Valor = 0.0M;
                }

                if (workSheet.Cell(linha, coluna8).Value == null)
                    throw new Exception("Quantidade não informada: linha " + (linha + 1));
                try {
                    item.Quantidade = Convert.ToDecimal(workSheet.Cell(linha, coluna8).Value);
                }
                catch (Exception ex) {
                    throw new Exception(ex.Message + " - Quantidade: linha " + (linha + 1));
                }

                if (workSheet.Cell(linha, coluna9).Value != null &&
                     workSheet.Cell(linha, coluna9).Value.ToString().Trim() != "") {
                    try {
                        item.PercentualDesconto = Convert.ToDecimal(workSheet.Cell(linha, coluna9).Value);
                    }
                    catch (Exception ex) {
                        throw new Exception(ex.Message + " - PercentualDesconto: linha " + (linha + 1));
                    }
                }

                if (workSheet.Cell(linha, coluna10).Value != null &&
                     workSheet.Cell(linha, coluna10).Value.ToString().Trim() != "") {
                    try {
                        item.Trader = workSheet.Cell(linha, coluna10).ValueAsString;
                    }
                    catch (Exception ex) {
                        throw new Exception(ex.Message + " - Trader: linha " + (linha + 1));
                    }
                }
                if (workSheet.Cell(linha, coluna11).Value != null) {
                    try {
                        item.CalculaDespesas = workSheet.Cell(linha, coluna11).ValueAsString;
                    }
                    catch (Exception ex) {
                        throw new Exception(ex.Message + " - CalculaDespesas: linha " + (linha + 1));
                    }
                }

                #region Local Negociação
                if (workSheet.Cell(linha, coluna12).Value == null)
                {
                    item.LocalNegociacao = null;
                }
                else
                {
                    try
                    {
                        item.LocalNegociacao = workSheet.Cell(linha, coluna12).ValueAsString;
                    }
                    catch (Exception ex)
                    {
                        throw new Exception(ex.Message + " - LocalNegociacao: linha " + (linha + 1));
                    }
                }
                #endregion

                #region Clearing
                if (workSheet.Cell(linha, coluna13).Value == null)
                {
                    item.Clearing = null;
                }
                else
                {
                    try
                    {
                        item.Clearing = workSheet.Cell(linha, coluna13).ValueAsString;
                    }
                    catch (Exception ex)
                    {
                        throw new Exception(ex.Message + " - Clearing: linha " + (linha + 1));
                    }
                }
                #endregion

                #region Local Custódia
                if (workSheet.Cell(linha, coluna14).Value == null)
                {
                    item.LocalCustodia = null;
                }
                else
                {
                    try
                    {
                        item.LocalCustodia = workSheet.Cell(linha, coluna14).ValueAsString;
                    }
                    catch (Exception ex)
                    {
                        throw new Exception(ex.Message + " - LocalCustodia: linha " + (linha + 1));
                    }
                }
                #endregion

                #region Corretora
                if (workSheet.Cell(linha, coluna15).Value == null)
                {
                    item.AgenteCustodia = null;
                }
                else
                {
                    try
                    {
                        item.AgenteCustodia = workSheet.Cell(linha, coluna15).ValueAsString;
                    }
                    catch (Exception ex)
                    {
                        throw new Exception(ex.Message + " - Corretora: linha " + (linha + 1));
                    }
                }
                #endregion

                #region DataRegistro
                if (workSheet.Cell(linha, coluna16).Value == null)
                {
                    item.DataOperacao = item.DataOrdem;
                }
                else
                {
                    try
                    {
                        item.DataOperacao = workSheet.Cell(linha, coluna16).ValueAsDateTime;
                    }
                    catch (Exception ex)
                    {
                        throw new Exception(ex.Message + " - Data Registro: linha " + (linha + 1));
                    }
                }
                #endregion


                this.valoresExcelOrdemBolsa.Add(item);

                index++;
                linha = 1 + index;

                Console.WriteLine("{0},{1},{2},{3},{4},{5},{6},{7},{8},{9},{10},{11},{12},{13}",
                    item.IdCliente, item.CdAtivoBolsa, item.CodigoBovespa, item.TipoOrdem, item.DataOrdem.ToString("dd/MM/yyyy"),
                    item.Pu, item.Valor, item.Quantidade,item.PercentualDesconto, item.Trader, item.CalculaDespesas,
                    item.LocalNegociacao, item.Clearing, item.LocalCustodia, item.AgenteCustodia);
            }
        }
        catch (Exception ex) {
            document.Close();
            document.Dispose();
            throw new Exception(ex.Message);
        }
        #endregion

        document.Close();
        document.Dispose();
    }

    /// <summary>
    /// Gera e Carrega as Ordens Bolsa de Acordo com o objeto List<ValoresExcelOrdemBolsa>
    /// </summary>
    /// <param name="ignoraCarteiraInexistente">ignora cliente e carteira inexistentes</param>     
    /// <exception cref="Exception">Se ocorreu problemas</exception>
    public void CarregaOrdemBolsa(bool ignoraCarteiraInexistente) {
        //OrdemBolsaCollection ordemBolsaDeletarCollection = new OrdemBolsaCollection();
        //
        int cont = 0;
        using (esTransactionScope scope = new esTransactionScope()) {
            for (int i = 0; i < this.valoresExcelOrdemBolsa.Count; i++) {
                ValoresExcelOrdemBolsa valoresExcel = this.valoresExcelOrdemBolsa[i];
                //                    
                #region Verifica se Existe Cliente
                Cliente cliente = new Cliente();
                List<esQueryItem> campos = new List<esQueryItem>();
                campos.Add(cliente.Query.IdCliente);
                campos.Add(cliente.Query.Status);
                campos.Add(cliente.Query.DataDia);

                if (!cliente.LoadByPrimaryKey(campos, valoresExcel.IdCliente)) {
                    if (ignoraCarteiraInexistente) {
                        continue;
                    }
                    else {
                        throw new Exception("Cliente Não existente: " + valoresExcel.IdCliente);
                    }                                       
                }
                #endregion

                #region Verifica se Existe AtivoBolsa
                AtivoBolsa ativoBolsa = new AtivoBolsa();
                if (!ativoBolsa.LoadByPrimaryKey(valoresExcel.CdAtivoBolsa.Trim())) {
                    throw new Exception("AtivoBolsa Não existente: " + valoresExcel.CdAtivoBolsa.Trim());
                }
                #endregion                

                #region Testa AgenteMercado

                AgenteMercado agenteMercado = new AgenteMercado();
                int? idAgenteMercado = null;
                try {
                    idAgenteMercado = (int)agenteMercado.BuscaIdAgenteMercadoBovespa(valoresExcel.CodigoBovespa);
                }
                catch (IdAgenteNaoCadastradoException e) {
                    throw new IdAgenteNaoCadastradoException(e.Message);
                }
                #endregion

                #region Testa AgenteMercado Liquidante ( caso tenha )               
                int? idAgenteMercadoLiquidante = null;
                if (!string.IsNullOrEmpty(valoresExcel.AgenteCustodia))
                {
                    AgenteMercado agenteMercadoLiquidante = new AgenteMercado();
                    try
                    {
                        idAgenteMercadoLiquidante = (int)agenteMercadoLiquidante.BuscaIdAgenteMercadoBovespa(Convert.ToInt32(valoresExcel.AgenteCustodia));
                    }
                    catch (IdAgenteNaoCadastradoException e)
                    {
                        throw new IdAgenteNaoCadastradoException(e.Message);
                    }
                }
                #endregion

                if (idAgenteMercado.HasValue) 
                {
                    #region Verifica DataDia
                    if (valoresExcel.DataOrdem < cliente.DataDia.Value) {
                        throw new Exception("Cliente: " + cliente.IdCliente.Value + " - Data da Ordem: " + valoresExcel.DataOrdem.ToString("dd/MM/yyyy") + " menor que a data dia do Cliente: " + cliente.DataDia.Value.ToString("dd/MM/yyyy"));
                    }

                    // Se Status está Fechado
                    if (cliente.Status == (byte)StatusCliente.Divulgado) 
                    {
                        // Pode importar somente depois da DataDia
                        if (valoresExcel.DataOrdem == cliente.DataDia.Value) 
                        {
                            throw new Exception("Cliente: " + cliente.IdCliente.Value + " - Para Clientes Com Status Fechado, Data da Ordem: " + valoresExcel.DataOrdem.ToString("dd/MM/yyyy") + " deve ser maior que data dia do Cliente: " + cliente.DataDia.Value.ToString("dd/MM/yyyy"));
                        }
                    }
                    #endregion

                    #region Verifica se TipoOrdem é Consistente
                    if (valoresExcel.TipoOrdem.ToString().ToUpper() != TipoOrdemBolsa.Compra &&
                        valoresExcel.TipoOrdem.ToString().ToUpper() != TipoOrdemBolsa.Venda &&
                        valoresExcel.TipoOrdem.ToString().ToUpper() != TipoOrdemBolsa.Ingresso &&
                        valoresExcel.TipoOrdem.ToString().ToUpper() != TipoOrdemBolsa.Retirada &&
                        valoresExcel.TipoOrdem.ToString().ToUpper() != "CE" && //Exercicio de opcao (Compra)
                        valoresExcel.TipoOrdem.ToString().ToUpper() != "VE") //Exercicio de opcao (Venda)
                    {
                        throw new Exception("TipoOrdem Deve ser C=Compra, V=Venda, CE=ExercicioComprado, VE=ExercicioVendido");
                    }
                    #endregion

                    #region Verifica se Existe Trader
                    Trader trader = new Trader();
                    if (!String.IsNullOrEmpty(valoresExcel.Trader)) {
                        trader.Query.Select(trader.Query.IdTrader)
                                     .Where(trader.Query.Nome == valoresExcel.Trader.Trim());
                        //
                        if (!trader.Query.Load()) {
                            throw new Exception("Trader Não existente: " + valoresExcel.Trader.Trim());
                        }
                    }
                    #endregion
                                        
                    #region OrdemBolsa
                    OrdemBolsa ordemBolsa = new OrdemBolsa();

                    // Dados do Arquivo
                    ordemBolsa.IdCliente = valoresExcel.IdCliente;
                    ordemBolsa.CdAtivoBolsa = valoresExcel.CdAtivoBolsa.Trim().ToUpper();
                    ordemBolsa.IdAgenteLiquidacao = (idAgenteMercadoLiquidante.HasValue? idAgenteMercadoLiquidante.Value : idAgenteMercado.Value);
                    ordemBolsa.IdAgenteCorretora = idAgenteMercado.Value;
                    ordemBolsa.TipoMercado = ativoBolsa.TipoMercado.Trim().ToUpper();

                    if (valoresExcel.TipoOrdem.ToString().ToUpper() == "CE" ||
                        valoresExcel.TipoOrdem.ToString().ToUpper() == "VE") {
                        if (ativoBolsa.TipoMercado == TipoMercadoBolsa.OpcaoCompra) {
                            ordemBolsa.Origem = (byte)OrigemOperacaoBolsa.ExercicioOpcaoCompra;
                        }
                        else if (ativoBolsa.TipoMercado == TipoMercadoBolsa.OpcaoVenda) {
                            ordemBolsa.Origem = (byte)OrigemOperacaoBolsa.ExercicioOpcaoVenda;
                        }

                        ordemBolsa.CdAtivoBolsaOpcao = valoresExcel.CdAtivoBolsa.Trim().ToUpper();
                        ordemBolsa.CdAtivoBolsa = ativoBolsa.CdAtivoBolsaObjeto;

                        ordemBolsa.TipoMercado = TipoMercadoBolsa.MercadoVista;
                        ordemBolsa.TipoOrdem = valoresExcel.TipoOrdem.ToString().ToUpper().Substring(0, 1); //Transforma em C ou V
                    }
                    else {
                        ordemBolsa.TipoOrdem = valoresExcel.TipoOrdem.ToString().ToUpper();
                        ordemBolsa.Origem = (byte)OrigemOrdemBolsa.Primaria;
                    }

                    ordemBolsa.Data = valoresExcel.DataOrdem;
                    ordemBolsa.Pu = valoresExcel.Pu;
                    ordemBolsa.Valor = valoresExcel.Valor;

                    if (valoresExcel.Quantidade < 0)
                    {
                        throw new Exception("Quantidade " + valoresExcel.Quantidade.ToString() + " não pode ser negativa.");
                    }
                    ordemBolsa.Quantidade = valoresExcel.Quantidade;
                    ordemBolsa.PercentualDesconto = valoresExcel.PercentualDesconto;
                    if (!String.IsNullOrEmpty(valoresExcel.Trader)) {
                        ordemBolsa.IdTrader = trader.IdTrader.Value;
                    }

                    ordemBolsa.CalculaDespesas = !String.IsNullOrEmpty(valoresExcel.CalculaDespesas)
                                                 ? valoresExcel.CalculaDespesas.Trim().ToUpper()
                                                 : "N";
                    // Valores Forçados
                    ordemBolsa.Corretagem = 0;
                    ordemBolsa.Emolumento = 0;
                    ordemBolsa.LiquidacaoCBLC = 0;
                    ordemBolsa.RegistroBolsa = 0;
                    ordemBolsa.RegistroCBLC = 0;

                    ordemBolsa.Fonte = (byte)FonteOrdemBolsa.Manual;
                    ordemBolsa.Desconto = 0;
                    ordemBolsa.QuantidadeDayTrade = 0;
                    ordemBolsa.IdMoeda = (int)ativoBolsa.IdMoeda.Value;

                    ordemBolsa.IdLocalNegociacao = (int)LocalNegociacaoFixo.BOVESPA;
                    if (!string.IsNullOrEmpty(valoresExcel.LocalNegociacao))
                    {
                        LocalNegociacao localNegociacao = new LocalNegociacao();
                        localNegociacao.Query.Where(localNegociacao.Query.Codigo.ToUpper().Equal(valoresExcel.LocalNegociacao.ToUpper()));

                        if (localNegociacao.Query.Load())
                            ordemBolsa.IdLocalNegociacao = localNegociacao.IdLocalNegociacao.Value;
                        else
                            throw new Exception("Local de Negociação não cadastrado: " + valoresExcel.LocalNegociacao);
                    }

                    ordemBolsa.IdLocalCustodia = (int)LocalCustodiaFixo.CBLC;
                    if (!string.IsNullOrEmpty(valoresExcel.LocalCustodia))
                    {
                        LocalCustodia localCustodia = new LocalCustodia();
                        localCustodia.Query.Where(localCustodia.Query.Codigo.ToUpper().Equal(valoresExcel.LocalCustodia.ToUpper()));

                        if (localCustodia.Query.Load())
                            ordemBolsa.IdLocalCustodia = localCustodia.IdLocalCustodia.Value;
                        else
                            throw new Exception("Local de Custódia não cadastrado: " + valoresExcel.LocalCustodia);
                    }

                    ordemBolsa.IdClearing = (int)ClearingFixo.CBLC;
                    if (!string.IsNullOrEmpty(valoresExcel.Clearing))
                    {
                        Clearing clearing = new Clearing();
                        clearing.Query.Where(clearing.Query.Codigo.ToUpper().Equal(valoresExcel.Clearing.ToUpper()));

                        if (clearing.Query.Load())
                            ordemBolsa.IdClearing = clearing.IdClearing.Value;
                        else
                            throw new Exception("Clearing não cadastrada: " + valoresExcel.Clearing);
                    }

                    ordemBolsa.DataOperacao = valoresExcel.DataOperacao;
                    ordemBolsa.Save();
                    //

                    if (ativoBolsa.TipoMercado == TipoMercadoBolsa.Termo) {
                        OrdemTermoBolsa ordemTermoBolsa = new OrdemTermoBolsa();
                        ordemTermoBolsa.IdOrdem = ordemBolsa.IdOrdem.Value;
                        ordemTermoBolsa.NumeroContrato = "0";

                        ordemTermoBolsa.Save();
                    }
                    #endregion
                }

                cont += 1;
            }

            scope.Complete();
        }
    }

    /// <summary>
    /// Processa a Planilha de OrdemBolsa após ter acabado o Upload
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    /// <param name="checkIgnora">Se Checado ignora os erros de importação de codigos inválidos </param>
    protected void uplOrdemBolsa_FileUploadComplete(FileUploadCompleteEventArgs e, ASPxCheckBox checkIgnora) {
        e.CallbackData = "";

        #region Trata Extensão Válida
        if (!ValoresExcelUtil.isExtensaoExcel(e.UploadedFile.FileName.Trim())) {
            e.CallbackData = "Importação Ordem Bolsa - Formato de Arquivo Inválido. Formato permitido: .xls ou .xlsx\n\n";
            return;
        }
        #endregion

        // Stream de bytes com o Conteudo do Arquivo Excel
        Stream sr = e.UploadedFile.FileContent;

        #region Processamento Arquivo Excel

        //Reseta Vetor de Dados        
        this.valoresExcelOrdemBolsa = new List<ValoresExcelOrdemBolsa>();

        try {
            // Ler Arquivo
            this.LerArquivoOrdemBolsa(sr);
            // Carrega Arquivo
            this.CarregaOrdemBolsa(checkIgnora.Checked);
        }
        catch (Exception e2) {
            e.CallbackData = "Importação Ordem Bolsa - " + e2.Message;
            return;
        }

        new HistoricoLog().InsereHistoricoLog(DateTime.Now, DateTime.Now,
                                        "Importação Planilha Ordem Bolsa",
                                        HttpContext.Current.User.Identity.Name, UtilitarioWeb.GetIP(Request), "", HistoricoLogOrigem.Outros);

        #endregion
    }
}
