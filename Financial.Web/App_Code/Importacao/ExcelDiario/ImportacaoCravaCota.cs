﻿using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Financial.Web.Common;
using System.IO;
using Financial.Integracao.Excel.Util;
using Financial.Integracao.Excel;
using EntitySpaces.Interfaces;
using Financial.Common;
using System.Collections.Generic;
using Financial.Investidor;
using DevExpress.Web;
using Financial.Fundo;
using Financial.Security;
using Financial.Web.Util;
using Financial.Security.Enums;
using Financial.Util;
using Financial.Util.Enums;  
using Financial.Investidor.Enums;
using Financial.Investidor.Controller;

public partial class ImportacaoBasePage : BasePage {

    /* Estrutura do Excel */
    private List<ValoresExcelCravaCota> valoresExcelCravaCota = new List<ValoresExcelCravaCota>();
    
    /// <summary>
    /// Leitura de um arquivo Excel
    /// Saida: List<ValoresExcel> com os valores armazenados no arquivo
    /// </summary>
    /// <param name="streamExcel">Stream de bytes de um arquivo Excel</param>
    /// <exception cref="Exception">Se ocorreu problemas</exception>
    private void LerArquivoCravaCota(Stream streamExcel)
    {
        #region Leitura do Arquivo Excel

        // Open Spreadsheet
        Bytescout.Spreadsheet.Spreadsheet document = new Bytescout.Spreadsheet.Spreadsheet();
        document.LoadFromStream(streamExcel);

        Bytescout.Spreadsheet.Worksheet workSheet = document.Workbook.Worksheets[0];

        //
        #region Confere Formato Arquivo
        string[] colunasConferencias = new string[] {"IdCarteira","Data","Cota","TipoBloqueio"};

        bool formato = ValoresExcelUtil.isExcelFormatoValido(colunasConferencias, workSheet);

        if (!formato)
        {
            string mensagem = "Formato Interno do Arquivo Inválido.\n";
            mensagem += "Linha 1 do Arquivo deve ser: \n";
            for (int i = 0; i < colunasConferencias.Length; i++)
            {
                int coluna = i + 1;
                mensagem += "\tColuna " + coluna + ": " + colunasConferencias[i] + "\n";
            }

            document.Close();
            document.Dispose();

            throw new Exception(mensagem);
        }
        #endregion

        //
        int index = 0;
        // This row,column index should be changed as per your need.
        // i.e. which cell in the excel you are interesting to read.
        //
        /* Formato: 1)IdCarteira  - 2)Data - 3)Cota - 4)TipoBloqueio
         */
        int linha = 1;
        int coluna1 = 0, coluna2 = 1, coluna3 = 2, coluna4 = 3;

        //            
        try
        {
            // Enquanto idCotista tiver valor
            while (workSheet.Cell(linha, coluna1).Value != null)
            {
                ValoresExcelCravaCota item = new ValoresExcelCravaCota();
                
                try {
                    item.IdCarteira = Convert.ToInt32(workSheet.Cell(linha, coluna1).ValueAsString.Trim());
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message + " - IdCarteira: linha " + (linha + 1));
                }

                if (workSheet.Cell(linha, coluna2).Value == null)
                    throw new Exception("Data não informada: linha " + (linha + 1));
                try {
                    item.Data = workSheet.Cell(linha, coluna2).ValueAsDateTime;
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message + " - Data: linha " + (linha + 1));
                }
                    
                if (workSheet.Cell(linha, coluna3) == null)
                    throw new Exception("Cota não informada: linha " + (linha + 1));
                try
                {
                    item.Cota = Convert.ToDecimal(workSheet.Cell(linha, coluna3).Value);
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message + " - Cota: linha " + (linha + 1));
                }

                if (workSheet.Cell(linha, coluna4) == null)
                    throw new Exception("TipoBloqueio não informado: linha " + (linha + 1));
                try
                {
                    item.TipoBloqueio = Convert.ToInt16(workSheet.Cell(linha, coluna4).Value);
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message + " - TipoBloqueio: linha " + (linha + 1));
                }

                //
                this.valoresExcelCravaCota.Add(item);
                //
                index++;
                linha = 1 + index;
                //                
                Console.WriteLine("{0},{1},{2},{3}",
                    item.IdCarteira, item.Data, item.Cota, item.TipoBloqueio);
            }
        }
        catch (Exception ex)
        {
            document.Close();
            document.Dispose();

            throw new Exception(ex.Message);
        }
        #endregion

        // Fecha o arquivo
        document.Close();
        document.Dispose();
    }
    
    /// <summary>
    /// Gera e carrega as posições Cotista de acordo com o objeto List<ValoresExcel>
    /// </summary>
    private void CarregaCravaCota()
    {
   
        for (int i = 0; i < this.valoresExcelCravaCota.Count; i++)
        {
            ValoresExcelCravaCota valoresExcel = this.valoresExcelCravaCota[i];

            #region Verifica se já existe HistoricoCota com o mesmo IdCarteira/Data
            TravamentoCotas travamentoCotas = new TravamentoCotas();

            // Se não existe é feito um insert, caso contrario é feito um update
            travamentoCotas.Query.Where(travamentoCotas.Query.IdCarteira.Equal(valoresExcel.IdCarteira));
            travamentoCotas.Query.Where(travamentoCotas.Query.DataProcessamento.Equal(valoresExcel.Data));
            travamentoCotas.Query.Load();
            #endregion

            #region Verifica se Existe Carteira
            Carteira carteira = new Carteira();
            if (!carteira.LoadByPrimaryKey(valoresExcel.IdCarteira))
            {
                throw new Exception("Carteira Não existente : " + valoresExcel.IdCarteira);
            }
            #endregion

            #region Verifica se já existe cota calculada para a carteira na data solicitada
            HistoricoCota historicoCota = new HistoricoCota();
            if (historicoCota.LoadByPrimaryKey(valoresExcel.Data, valoresExcel.IdCarteira))
                travamentoCotas.ValorCotaCalculada = historicoCota.CotaFechamento;
            else
                travamentoCotas.ValorCotaCalculada = valoresExcel.Cota;
            #endregion

            // Dados do Arquivo
            travamentoCotas.DataProcessamento = valoresExcel.Data;
            travamentoCotas.IdCarteira = valoresExcel.IdCarteira;
            travamentoCotas.TipoBloqueio = valoresExcel.TipoBloqueio;
            travamentoCotas.ValorCota = valoresExcel.Cota;

            // Attach the object
            travamentoCotas.Save();
        }



    }

    /// <summary>
    /// Processa a Planilha de HistoricoCota após ter acabado o Upload
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void uplCravaCota_FileUploadComplete(object sender, FileUploadCompleteEventArgs e)
    {
        e.CallbackData = "";

        #region Trata Extensão Válida
        if (!ValoresExcelUtil.isExtensaoExcel(e.UploadedFile.FileName.Trim()))
        {
            e.CallbackData = "Importação Crava Cota - Formato de Arquivo Inválido. Formato permitido: .xls ou .xlsx\n\n";
            return;
        }
        #endregion

        // Stream de bytes com o Conteudo do Arquivo Excel
        Stream sr = e.UploadedFile.FileContent;

        #region Processamento Arquivo Excel

        //Reseta Vetor de Dados
        this.valoresExcelCravaCota = new List<ValoresExcelCravaCota>();
        //
        try
        {
            // Ler Arquivo
            this.LerArquivoCravaCota(sr);
            // Carrega Arquivo
            this.CarregaCravaCota();
        }
        catch (Exception e2)
        {
            e.CallbackData = "Importação Crava Cota - " + e2.Message;
            return;
        }

        new HistoricoLog().InsereHistoricoLog(DateTime.Now, DateTime.Now,
            "Importação Planilha Crava Cota",
            HttpContext.Current.User.Identity.Name, UtilitarioWeb.GetIP(Request), "", HistoricoLogOrigem.Outros);

        #endregion
    }
}
