﻿using System;
using System.Collections.Generic;
using System.IO;

using Financial.Integracao.Excel;
using Financial.Integracao.Excel.Util;
using Financial.RendaFixa;
using Financial.Web.Common;

using EntitySpaces.Interfaces;

using DevExpress.Web;
using Financial.Security;
using System.Web;
using Financial.Web.Util;
using Financial.Security.Enums;

public partial class ImportacaoBasePage : BasePage {

    /* Estrutura do Excel */
    private List<ValoresExcelCotacaoSerie> valoresExcelCotacaoSerie = new List<ValoresExcelCotacaoSerie>();
    
    //
    List<int> idSerieDeletarCotacaoSerie = new List<int>();
    List<DateTime> dataDeletarCotacaoSerie = new List<DateTime>();
    List<int> TipoSerieDeletar = new List<int>();
    //

    /// <summary>
    /// Leitura de um arquivo Excel
    /// Saida: List<ValoresExcel> com os valores armazenados no arquivo
    /// </summary>
    /// <param name="streamExcel">Stream de bytes de um arquivo Excel</param>
    /// <exception cref="Exception">Se ocorreu problemas</exception>
    private void LerArquivoCotacaoSerie(Stream streamExcel) {

        #region Leitura do Arquivo Excel

        // Open Spreadsheet
        Bytescout.Spreadsheet.Spreadsheet document = new Bytescout.Spreadsheet.Spreadsheet();
        document.LoadFromStream(streamExcel);

        Bytescout.Spreadsheet.Worksheet workSheet = document.Workbook.Worksheets[0];

        //
        #region Confere Formato Arquivo
        string[] colunasConferencias = new string[] { "Data", "IdSerie", "Valor" };

        bool formato = ValoresExcelUtil.isExcelFormatoValido(colunasConferencias, workSheet);

        if (!formato) {
            string mensagem = "Formato Interno do Arquivo Inválido.\n";
            mensagem += "Linha 1 do Arquivo deve ser: \n";
            for (int i = 0; i < colunasConferencias.Length; i++) {
                int coluna = i + 1;
                mensagem += "\tColuna " + coluna + ": " + colunasConferencias[i] + "\n";
            }

            document.Close();
            document.Dispose();

            throw new Exception(mensagem);
        }
        #endregion

        //
        int index = 0;
        // This row,column index should be changed as per your need.
        // i.e. which cell in the excel you are interesting to read.
        //
        /* Formato: 1)Data  - 2)IdSerie    - 3)Valor */

        int linha = 1;
        int coluna1 = 0, coluna2 = 1, coluna3 = 2;

        //            
        try {
            // Enquanto Data tiver valor
            while (workSheet.Cell(linha, coluna1).Value != null) {
                ValoresExcelCotacaoSerie item = new ValoresExcelCotacaoSerie();
        
                try {
                    item.Data = workSheet.Cell(linha, coluna1).ValueAsDateTime;
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message + " - Data: linha " + (linha + 1));
                }

                if (workSheet.Cell(linha, coluna2).Value == null)
                    throw new Exception("IdSerie não informado: linha " + (linha + 1));
                try {
                    item.IdSerie = workSheet.Cell(linha, coluna2).ValueAsInteger;
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message + " - IdSerie: linha " + (linha + 1));
                }

                if (workSheet.Cell(linha, coluna3).Value == null)
                    throw new Exception("Valor não informado: linha " + (linha + 1));
                try {
                    item.Valor = Convert.ToDecimal(workSheet.Cell(linha, coluna3).Value);
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message + " - Valor: linha " + (linha + 1));
                }

                this.valoresExcelCotacaoSerie.Add(item);
                
                index++;
                linha = 1 + index;
                
                Console.WriteLine("{0},{1},{2}", item.Data, item.IdSerie, item.Valor);
            }
        }
        catch (Exception ex) {
            document.Close();
            document.Dispose();

            throw new Exception(ex.Message);
        }
        #endregion

        document.Close();
        document.Dispose();
    }

    /// <summary>
    /// Gera e Carrega as Cotações Series de acordo com o objeto List<ValoresExcel>
    /// </summary>     
    private void CarregaCotacaoSerie() {
        CotacaoSerieCollection cotacaoSerieCollection = new CotacaoSerieCollection();        
        CotacaoSerieCollection cotacaoSerieDeletarCollection = new CotacaoSerieCollection();

        TaxaSerieCollection taxaSerieCollection = new TaxaSerieCollection();
        TaxaSerieCollection taxaSerieDeletarCollection;

        List<int> tipoSerieImportacaoTaxa = new List<int>();
        tipoSerieImportacaoTaxa.Add((int)Financial.RendaFixa.Enums.TipoMTMTitulo.SerieTaxa);        
        tipoSerieImportacaoTaxa.Add((int)Financial.RendaFixa.Enums.TipoMTMTitulo.SerieTaxaPuFace);

        List<int> tipoSerieImportacaoCotacao = new List<int>();
        tipoSerieImportacaoCotacao.Add((int)Financial.RendaFixa.Enums.TipoMTMTitulo.Serie);
        //    
        for (int i = 0; i < this.valoresExcelCotacaoSerie.Count; i++) 
        {
            ValoresExcelCotacaoSerie valoresExcel = this.valoresExcelCotacaoSerie[i];
            //
            #region Verifica se Existe Serie
            SerieRendaFixa serie = new SerieRendaFixa();
            if (!serie.LoadByPrimaryKey(valoresExcel.IdSerie)) 
            {
                throw new Exception("IdSerie não existente: " + valoresExcel.IdSerie + "\n Cadastre a Serie Renda Fixa " + valoresExcel.IdSerie);
            }
            #endregion

            if(!tipoSerieImportacaoCotacao.Contains(serie.TipoTaxa.Value) && !tipoSerieImportacaoTaxa.Contains(serie.TipoTaxa.Value))
            {
                throw new Exception("Importação permitida apenas para séries do Tipo Série-Cotação, Série-Taxa e Série-Taxa - % pu de Face! (ID - " + valoresExcel.IdSerie + ")");
            }

            // Salva id e Data para poder Deletar
            this.idSerieDeletarCotacaoSerie.Add(valoresExcel.IdSerie);
            this.dataDeletarCotacaoSerie.Add(valoresExcel.Data);

            if (tipoSerieImportacaoTaxa.Contains(serie.TipoTaxa.Value))
            {
                this.TipoSerieDeletar.Add(serie.TipoTaxa.Value);

                #region CotacaoSerie
                TaxaSerie taxaSerie = taxaSerieCollection.AddNew();
                // Dados do Arquivo
                taxaSerie.Data = valoresExcel.Data;

                taxaSerie.IdSerie = valoresExcel.IdSerie;
                taxaSerie.Taxa = valoresExcel.Valor;
                taxaSerie.DigitadoImportado = (int)Financial.RendaFixa.Enums.DigitadoImportado.Importado;
                //
                #endregion
            }
            else
            {
                this.TipoSerieDeletar.Add(serie.TipoTaxa.Value);

                #region CotacaoSerie
                CotacaoSerie cotacaoSerie = cotacaoSerieCollection.AddNew();
                // Dados do Arquivo
                cotacaoSerie.Data = valoresExcel.Data;

                cotacaoSerie.IdSerie = valoresExcel.IdSerie;
                cotacaoSerie.Cotacao = valoresExcel.Valor;
                cotacaoSerie.DigitadoImportado = (int)Financial.RendaFixa.Enums.DigitadoImportado.Importado;
                //
                #endregion
            }
            
        }

        using (esTransactionScope scope = new esTransactionScope()) {

            #region Deleta CotacaoSerie por Data/IdSerie
            for (int i = 0; i < this.idSerieDeletarCotacaoSerie.Count; i++) {

                if (tipoSerieImportacaoTaxa.Contains(this.TipoSerieDeletar[i]))
                {
                    taxaSerieDeletarCollection = new TaxaSerieCollection();

                    taxaSerieDeletarCollection.Query
                    .Where(taxaSerieDeletarCollection.Query.IdSerie == this.idSerieDeletarCotacaoSerie[i],
                           taxaSerieDeletarCollection.Query.Data == this.dataDeletarCotacaoSerie[i]);

                    taxaSerieDeletarCollection.Query.Load();
                    taxaSerieDeletarCollection.MarkAllAsDeleted();
                    taxaSerieDeletarCollection.Save();
                }
                else
                { 
                    cotacaoSerieDeletarCollection = new CotacaoSerieCollection();

                    cotacaoSerieDeletarCollection.Query
                    .Where(cotacaoSerieDeletarCollection.Query.IdSerie == this.idSerieDeletarCotacaoSerie[i],
                           cotacaoSerieDeletarCollection.Query.Data == this.dataDeletarCotacaoSerie[i]);

                    cotacaoSerieDeletarCollection.Query.Load();
                    cotacaoSerieDeletarCollection.MarkAllAsDeleted();
                    cotacaoSerieDeletarCollection.Save();
                }
            }
            #endregion

            // Salva Cotações Índices presentes no Excel                
            cotacaoSerieCollection.Save();
            taxaSerieCollection.Save();

            scope.Complete();
        }
    }

    /// <summary>
    /// Processa a Planilha de CotacaoIndice após ter acabado o Upload
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void uplCotacaoSerie_FileUploadComplete(object sender, FileUploadCompleteEventArgs e) {
        e.CallbackData = "";

        #region Trata Extensão Válida
        if (!ValoresExcelUtil.isExtensaoExcel(e.UploadedFile.FileName.Trim())) {
            e.CallbackData = "Importação Cotação Serie - Formato de Arquivo Inválido. Formato permitido: .xls ou .xlsx\n\n";
            return;
        }
        #endregion

        // Stream de bytes com o Conteudo do Arquivo Excel
        Stream sr = e.UploadedFile.FileContent;

        #region Processamento Arquivo Excel

        //Reseta Vetor de Dados
        this.valoresExcelCotacaoSerie = new List<ValoresExcelCotacaoSerie>();
        this.idSerieDeletarCotacaoSerie = new List<int>();
        this.TipoSerieDeletar = new List<int>();
        this.dataDeletarCotacaoSerie = new List<DateTime>();
        //
        try {
            // Ler Arquivo
            this.LerArquivoCotacaoSerie(sr);
            // Carrega Arquivo
            this.CarregaCotacaoSerie();
        }
        catch (Exception e2) {
            if (e2.Message.Contains("Violation of PRIMARY KEY")) {
                e.CallbackData = "Importação não Concluída: 2 registros com a mesma data e série";
            }
            else {
                e.CallbackData = "Importação Cotação Série - " + e2.Message;
            }
            return;
        }

        new HistoricoLog().InsereHistoricoLog(DateTime.Now, DateTime.Now,
            "Importação Planilha Cotação Série",
            HttpContext.Current.User.Identity.Name, UtilitarioWeb.GetIP(Request), "", HistoricoLogOrigem.Outros);

        #endregion
    }
}
