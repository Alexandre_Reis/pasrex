﻿using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Financial.Web.Common;
using System.IO;
using Financial.Integracao.Excel.Util;
using Financial.Integracao.Excel;
using EntitySpaces.Interfaces;
using Financial.Common;
using System.Collections.Generic;
using Financial.Investidor.Enums;
using DevExpress.Web;
using Financial.InvestidorCotista;
using Financial.Fundo;
using Financial.Investidor;
using Financial.InvestidorCotista.Enums;
using Financial.Util;
using Financial.Security;
using Financial.Web.Util;
using Financial.Security.Enums;
using Financial.Fundo.Enums;

public partial class ImportacaoBasePage : BasePage
{

    /* Estrutura do Excel */
    public List<ValoresExcelOrdemCotista> valoresExcelOrdemCotista = new List<ValoresExcelOrdemCotista>();

    /// <summary>
    /// Leitura de um arquivo Excel
    /// Saida: List<ValoresExcel> com os valores armazenados no arquivo
    /// </summary>
    /// <param name="streamExcel">Stream de bytes de um arquivo Excel</param>
    /// <exception cref="Exception">Se ocorreu problemas</exception>
    //private void LerArquivoOrdemCotista(Stream streamExcel)
    //{

    //    #region Leitura do Arquivo Excel
    //    // Open Spreadsheet
    //    Bytescout.Spreadsheet.Spreadsheet document = new Bytescout.Spreadsheet.Spreadsheet();
    //    document.LoadFromStream(streamExcel);

    //    Bytescout.Spreadsheet.Worksheet workSheet = document.Workbook.Worksheets[0];
    //    //
    //    //
    //    #region Confere Formato Arquivo
    //    string[] colunasConferencias = new string[] 
    //                            {"IdCotista", "IdCarteira", "DataOperacao", "DataConversao", "DataLiquidacao",
    //                             "TipoOperacao", "TipoResgate", "Quantidade", "CotaOperacao", 
    //                             "ValorBruto", "ValorLiquido", "ValorIR", "ValorIOF",
    //                             "ValorPerformance", "RendimentoResgate"
    //                            };

    //    bool formato = ValoresExcelUtil.isExcelFormatoValido(colunasConferencias, workSheet);

    //    if (!formato)
    //    {
    //        string mensagem = "Formato Interno do Arquivo Inválido.\n";
    //        mensagem += "Linha 1 do Arquivo deve ser: \n";
    //        for (int i = 0; i < colunasConferencias.Length; i++)
    //        {
    //            int coluna = i + 1;
    //            mensagem += "\tColuna " + coluna + ": " + colunasConferencias[i] + "\n";
    //        }

    //        document.Close();
    //        document.Dispose();

    //        throw new Exception(mensagem);
    //    }
    //    #endregion

    //    int index = 0;
    //    // This row,column index should be changed as per your need.
    //    // i.e. which cell in the excel you are interesting to read.
    //    //
    //    /* Formato: 1)IdCotista  - 2)IdCarteira  - 3)DataOperacao  - 4)DataConversao
    //                5)DataLiquidacao - 6)TipoOperacao - 7)TipoResgate - 8)Quantidade - 9)CotaOperacao
    //     *          10)ValorBruto - 11)ValorLiquido - 12)ValorIR - 13)ValorIOF - 14)ValorPerformance
    //     *          15)RendimentoResgate - 16)IdOperacaoAuxiliar
    //     */

    //    int linha = 1;
    //    int coluna1 = 0, coluna2 = 1, coluna3 = 2,
    //        coluna4 = 3, coluna5 = 4, coluna6 = 5,
    //        coluna7 = 6, coluna8 = 7, coluna9 = 8,
    //        coluna10 = 9, coluna11 = 10, coluna12 = 11,
    //        coluna13 = 12, coluna14 = 13, coluna15 = 14,
    //        coluna16 = 15;

    //    //            
    //    try
    //    {
    //        // Enquanto idCotista tiver valor
    //        while (workSheet.Cell(linha, coluna1).Value != null)
    //        {
    //            ValoresExcelOrdemCotista item = new ValoresExcelOrdemCotista();
                
    //            try {
    //                item.IdCotista = workSheet.Cell(linha, coluna1).ValueAsInteger;
    //            }
    //            catch (Exception ex) {
    //                throw new Exception(ex.Message + " - IdCotista: linha " + (linha + 1));
    //            }

    //            if (workSheet.Cell(linha, coluna2).Value == null)
    //                throw new Exception("IdCarteira não informado: linha " + (linha + 1));
    //            try {
    //                item.IdCarteira = workSheet.Cell(linha, coluna2).ValueAsInteger;
    //            }
    //            catch (Exception ex) {
    //                throw new Exception(ex.Message + " - IdCarteira: linha " + (linha + 1));
    //            }

    //            if (workSheet.Cell(linha, coluna3).Value == null)
    //                throw new Exception("DataOperacao não informada: linha " + (linha + 1));
    //            try {
    //                item.DataOperacao = workSheet.Cell(linha, coluna3).ValueAsDateTime;
    //            }
    //            catch (Exception ex) {
    //                throw new Exception(ex.Message + " - DataOperacao: linha " + (linha + 1));
    //            }

    //            if (workSheet.Cell(linha, coluna4).Value != null && workSheet.Cell(linha, coluna4).Value.ToString().Trim() != "")
    //            {
    //                try {
    //                    item.DataConversao = workSheet.Cell(linha, coluna4).ValueAsDateTime;
    //                }
    //                catch (Exception ex) {
    //                    throw new Exception(ex.Message + " - DataConversao: linha " + (linha + 1));
    //                }
    //            }
                
    //            if (workSheet.Cell(linha, coluna5).Value != null && workSheet.Cell(linha, coluna5).Value.ToString().Trim() != "")
    //            {
    //                try {
    //                    item.DataLiquidacao = workSheet.Cell(linha, coluna5).ValueAsDateTime;
    //                }
    //                catch (Exception ex) {
    //                    throw new Exception(ex.Message + " - DataLiquidacao: linha " + (linha + 1));
    //                }
    //            }

    //            if (workSheet.Cell(linha, coluna6).Value == null)
    //                throw new Exception("TipoOperacao não informado: linha " + (linha + 1));
    //            try {
    //                item.TipoOperacao = Convert.ToByte(workSheet.Cell(linha, coluna6).Value);
    //            }
    //            catch (Exception ex) {
    //                throw new Exception(ex.Message + " - TipoOperacao: linha " + (linha + 1));
    //            }

    //            if (workSheet.Cell(linha, coluna7).Value != null && workSheet.Cell(linha, coluna7).Value.ToString().Trim() != "")
    //            {
    //                try {
    //                    item.TipoResgate = Convert.ToByte(workSheet.Cell(linha, coluna7).Value);
    //                }
    //                catch (Exception ex) {
    //                    throw new Exception(ex.Message + " - TipoResgate: linha " + (linha + 1));
    //                }
    //            }
    //            else
    //            {
    //                item.TipoResgate = 0;
    //            }

    //            if (workSheet.Cell(linha, coluna8).Value == null)
    //                throw new Exception("Quantidade não informada: linha " + (linha + 1));
    //            try {
    //                item.Quantidade = Convert.ToDecimal(workSheet.Cell(linha, coluna8).Value);
    //            }
    //            catch (Exception ex) {
    //                throw new Exception(ex.Message + " - Quantidade: linha " + (linha + 1));
    //            }

    //            if (workSheet.Cell(linha, coluna9).Value == null)
    //                throw new Exception("CotaOperacao não informada: linha " + (linha + 1));
    //            try {
    //                item.CotaOperacao = Convert.ToDecimal(workSheet.Cell(linha, coluna9).Value);
    //            }
    //            catch (Exception ex) {
    //                throw new Exception(ex.Message + " - CotaOperacao: linha " + (linha + 1));
    //            }

    //            if (workSheet.Cell(linha, coluna10).Value == null)
    //                throw new Exception("ValorBruto não informado: linha " + (linha + 1));
    //            try {
    //                item.ValorBruto = Convert.ToDecimal(workSheet.Cell(linha, coluna10).Value);
    //            }
    //            catch (Exception ex) {
    //                throw new Exception(ex.Message + " - ValorBruto: linha " + (linha + 1));
    //            }

    //            if (workSheet.Cell(linha, coluna11).Value == null)
    //                throw new Exception("ValorLiquido não informado: linha " + (linha + 1));
    //            try {
    //                item.ValorLiquido = Convert.ToDecimal(workSheet.Cell(linha, coluna11).Value);
    //            }
    //            catch (Exception ex) {
    //                throw new Exception(ex.Message + " - ValorLiquido: linha " + (linha + 1));
    //            }

    //            if (workSheet.Cell(linha, coluna12).Value == null)
    //                throw new Exception("ValorIR não informado: linha " + (linha + 1));
    //            try {
    //                item.ValorIR = Convert.ToDecimal(workSheet.Cell(linha, coluna12).Value);
    //            }
    //            catch (Exception ex) {
    //                throw new Exception(ex.Message + " - ValorIR: linha " + (linha + 1));
    //            }

    //            if (workSheet.Cell(linha, coluna13).Value == null)
    //                throw new Exception("ValorIOF não informado: linha " + (linha + 1));
    //            try {
    //                item.ValorIOF = Convert.ToDecimal(workSheet.Cell(linha, coluna13).Value);
    //            }
    //            catch (Exception ex) {
    //                throw new Exception(ex.Message + " - ValorIOF: linha " + (linha + 1));
    //            }

    //            if (workSheet.Cell(linha, coluna14).Value == null)
    //                throw new Exception("ValorPerformance não informado: linha " + (linha + 1));
    //            try {
    //                item.ValorPerformance = Convert.ToDecimal(workSheet.Cell(linha, coluna14).Value);
    //            }
    //            catch (Exception ex) {
    //                throw new Exception(ex.Message + " - ValorPerformance: linha " + (linha + 1));
    //            }

    //            if (workSheet.Cell(linha, coluna15).Value == null)
    //                throw new Exception("RendimentoResgate não informado: linha " + (linha + 1));
    //            try {
    //                item.RendimentoResgate = Convert.ToDecimal(workSheet.Cell(linha, coluna15).Value);
    //            }
    //            catch (Exception ex) {
    //                throw new Exception(ex.Message + " - RendimentoResgate: linha " + (linha + 1));
    //            }
                
    //            if (workSheet.Cell(linha, coluna16).Value != null && workSheet.Cell(linha, coluna16).Value.ToString().Trim() != "")
    //            {
    //                try {
    //                    item.IdOperacaoAuxiliar = Convert.ToInt32(workSheet.Cell(linha, coluna16).Value);
    //                }
    //                catch (Exception ex) {
    //                    throw new Exception(ex.Message + " - IdOperacaoAuxiliar: linha " + (linha + 1));
    //                }
    //            }                
                
    //            this.valoresExcelOrdemCotista.Add(item);
                
    //            index++;
    //            linha = 1 + index;
                
    //            Console.WriteLine("{0},{1},{2},{3},{4},{5},{6},{7},{8},{9},{10},{11},{12},{13},{14},{15}",
    //                item.IdCotista, item.IdCarteira, item.DataOperacao, item.DataConversao,
    //                item.DataLiquidacao, item.TipoOperacao, item.TipoResgate, item.Quantidade,
    //                item.CotaOperacao, item.ValorBruto, item.ValorLiquido, item.ValorIR, item.ValorIOF,
    //                item.ValorPerformance, item.RendimentoResgate, item.IdOperacaoAuxiliar);
    //        }
    //    }
    //    catch (Exception ex)
    //    {
    //        document.Close();
    //        document.Dispose();

    //        throw new Exception(ex.Message);
    //    }
    //    #endregion

    //    document.Close();
    //    document.Dispose();
    //}

    /// <summary>
    /// Gera e carrega as operações de Cotista de acordo com o objeto List<ValoresExcel>
    /// </summary>
    /// </summary>
    /// <param name="ignoraCarteiraInexistente">ignora cliente e carteira inexistentes</param>    
    public OrdemCotistaCollection CarregaOrdemCotista(bool ignoraCarteiraInexistente)
    {
        OrdemCotistaCollection ordemCotistaCollection = new OrdemCotistaCollection();
        OrdemCotistaCollection ordemCotistaDeletarCollection = new OrdemCotistaCollection();
        //    
        for (int i = 0; i < this.valoresExcelOrdemCotista.Count; i++)
        {
            ValoresExcelOrdemCotista valoresExcel = this.valoresExcelOrdemCotista[i];
            //

            #region Verifica se Existe Carteira
            Carteira carteira = new Carteira();
            if (!carteira.LoadByPrimaryKey(valoresExcel.IdCarteira))
            {
                if (ignoraCarteiraInexistente)
                {
                    continue;
                }
                else
                {
                    Cliente clienteCodigo = new Cliente();

                    throw new Exception("Fundo Não existente : " + valoresExcel.IdCarteira);
                }
            }
            #endregion

            #region DataDia do Cliente
            Cliente cliente = new Cliente();
            List<esQueryItem> campos = new List<esQueryItem>();
            campos.Add(cliente.Query.IdCliente);
            campos.Add(cliente.Query.DataDia);
            if (!cliente.LoadByPrimaryKey(campos, valoresExcel.IdCarteira))
            {
                if (ignoraCarteiraInexistente)
                {
                    continue;
                }
                else
                {
                    throw new Exception("Fundo Não existente : " + valoresExcel.IdCarteira);
                }
            }

            DateTime dataDia = cliente.DataDia.Value;
            #endregion

            #region OrdemCotista
            OrdemCotista ordemCotista = new OrdemCotista();
            // Dados do Arquivo
            Cotista cotista = new Cotista();
            if (!cotista.LoadByPrimaryKey(valoresExcel.IdCotista))
            {
                continue;

                //if (ignoraCarteiraInexistente)
                //{

                //}
                //else
                //{
                //    throw new Exception("Cotista Não existente : " + valoresExcel.IdCotista);
                //}
            }
            ordemCotista.IdCotista = valoresExcel.IdCotista;

            ordemCotista.IdCarteira = valoresExcel.IdCarteira;
            DateTime DataOperacaoDateAndTime = valoresExcel.DataOperacao;
            ordemCotista.DataOperacao = valoresExcel.DataOperacao.Date;
            ordemCotista.TipoOperacao = valoresExcel.TipoOperacao;

            if (ordemCotista.DataOperacao.Value >= dataDia)
            {
                //Para operações na data ou superiores à data dia, é possível informar apenas a data da operação
                carteira = new Carteira();
                campos = new List<esQueryItem>();
                campos.Add(carteira.Query.DiasCotizacaoAplicacao);
                campos.Add(carteira.Query.DiasCotizacaoResgate);
                campos.Add(carteira.Query.DiasLiquidacaoAplicacao);
                campos.Add(carteira.Query.DiasLiquidacaoResgate);
                campos.Add(carteira.Query.ContagemDiasConversaoResgate);
                carteira.LoadByPrimaryKey(campos, ordemCotista.IdCarteira.Value);

                if (valoresExcel.DataConversao == null)
                {
                    DateTime dataConversao = new DateTime();
                    if (ordemCotista.TipoOperacao.Value == (byte)TipoOperacaoCotista.Aplicacao ||
                        ordemCotista.TipoOperacao.Value == (byte)TipoOperacaoCotista.AplicacaoAcoesEspecial)
                    {
                        dataConversao = Calendario.AdicionaDiaUtil(ordemCotista.DataOperacao.Value, carteira.DiasCotizacaoAplicacao.Value);
                    }
                    else if (ordemCotista.TipoOperacao.Value != (byte)TipoOperacaoCotista.AplicacaoCotasEspecial)
                    {
                        if (carteira.ContagemDiasConversaoResgate.Value == (byte)ContagemDiasLiquidacaoResgate.DiasUteis)
                        {
                            dataConversao = Calendario.AdicionaDiaUtil(ordemCotista.DataOperacao.Value, carteira.DiasCotizacaoResgate.Value);
                        }
                        else
                        {
                            dataConversao = ordemCotista.DataOperacao.Value.AddDays(carteira.DiasCotizacaoResgate.Value);

                            if (!Calendario.IsDiaUtil(dataConversao))
                            {
                                dataConversao = Calendario.AdicionaDiaUtil(dataConversao, 1);
                            }
                        }
                    }
                    ordemCotista.DataConversao = dataConversao;
                }
                else
                {
                    ordemCotista.DataConversao = valoresExcel.DataConversao.Value.Date;
                }

                if (valoresExcel.DataLiquidacao == null)
                {
                    DateTime dataLiquidacao = new DateTime();
                    if (ordemCotista.TipoOperacao.Value == (byte)TipoOperacaoCotista.Aplicacao ||
                        ordemCotista.TipoOperacao.Value == (byte)TipoOperacaoCotista.AplicacaoAcoesEspecial)
                    {
                        dataLiquidacao = Calendario.AdicionaDiaUtil(ordemCotista.DataOperacao.Value, carteira.DiasLiquidacaoAplicacao.Value);
                    }
                    else if (ordemCotista.TipoOperacao.Value != (byte)TipoOperacaoCotista.AplicacaoCotasEspecial)
                    {
                        if (carteira.ContagemDiasConversaoResgate.Value == (byte)ContagemDiasLiquidacaoResgate.DiasUteis)
                        {
                            dataLiquidacao = Calendario.AdicionaDiaUtil(ordemCotista.DataOperacao.Value, carteira.DiasLiquidacaoResgate.Value);
                        }
                        else
                        {
                            //Conta por Dias Úteis em cima da data de conversão
                            dataLiquidacao = Calendario.AdicionaDiaUtil(ordemCotista.DataConversao.Value, carteira.DiasLiquidacaoResgate.Value);
                        }
                    }
                    ordemCotista.DataLiquidacao = dataLiquidacao;
                }
                else
                {
                    ordemCotista.DataLiquidacao = valoresExcel.DataLiquidacao.Value.Date;
                }
            }
            else
            {
                if (valoresExcel.DataConversao == null || valoresExcel.DataLiquidacao == null)
                {
                    throw new Exception("Data da conversão e Data da liquidação devem ser informadas para Carteira " + ordemCotista.IdCarteira.Value.ToString());
                }
                else
                {
                    ordemCotista.DataConversao = valoresExcel.DataConversao.Value.Date;
                    ordemCotista.DataLiquidacao = valoresExcel.DataLiquidacao.Value.Date;
                }
            }

            ordemCotista.DataAgendamento = valoresExcel.DataAgendamento.HasValue ?
                valoresExcel.DataAgendamento : DataOperacaoDateAndTime;

            if (valoresExcel.TipoResgate != 0)
            {
                ordemCotista.TipoResgate = valoresExcel.TipoResgate;
            }

            ordemCotista.IdFormaLiquidacao = 1; //TODO ajustar para enum fixo depois
            ordemCotista.Quantidade = valoresExcel.Quantidade;
            ordemCotista.ValorBruto = valoresExcel.ValorBruto;
            ordemCotista.ValorLiquido = valoresExcel.ValorLiquido;
            ordemCotista.Status = valoresExcel.Status;

            // Attach the object
            ordemCotista.Save();
            ordemCotistaCollection.AttachEntity(ordemCotista);

            #endregion

        }

        // Salva Operações presentes no Excel
        return ordemCotistaCollection;
    }

    /// <summary>
    /// Processa a Planilha de OrdemCotista após ter acabado o Upload
    /// </summary>
    /// <param name="e"></param>
    /// <param name="checkIgnora">Se Checado ignora os erros de importação de codigos inválidos </param>
    //protected void uplOrdemCotista_FileUploadComplete(FileUploadCompleteEventArgs e, ASPxCheckBox checkIgnora)
    //{
    //    e.CallbackData = "";

    //    #region Trata Extensão Válida
    //    if (!ValoresExcelUtil.isExtensaoExcel(e.UploadedFile.FileName.Trim()))
    //    {
    //        e.CallbackData = "Importação Operação Cotista - Formato de Arquivo Inválido. Formato permitido: .xls ou .xlsx\n\n";
    //        return;
    //    }
    //    #endregion

    //    // Stream de bytes com o Conteudo do Arquivo Excel
    //    Stream sr = e.UploadedFile.FileContent;

    //    #region Processamento Arquivo Excel

    //    //Reseta Vetor de Dados
    //    this.valoresExcelOrdemCotista = new List<ValoresExcelOrdemCotista>();
    //    //
    //    try
    //    {
    //        // Ler Arquivo
    //        this.LerArquivoOrdemCotista(sr);
    //        // Carrega Arquivo
    //        this.CarregaOrdemCotista(checkIgnora.Checked);
    //    }
    //    catch (Exception e2)
    //    {
    //        e.CallbackData = "Importação Operação Cotista - " + e2.Message;
    //        return;
    //    }

    //    new HistoricoLog().InsereHistoricoLog(DateTime.Now, DateTime.Now,
    //        "Importação Planilha Operação Cotista",
    //        HttpContext.Current.User.Identity.Name, UtilitarioWeb.GetIP(Request), "", HistoricoLogOrigem.Outros);

    //    #endregion
    //}
}