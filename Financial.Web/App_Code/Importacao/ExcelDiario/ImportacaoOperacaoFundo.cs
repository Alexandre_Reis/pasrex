﻿using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Financial.Web.Common;
using System.IO;
using Financial.Integracao.Excel.Util;
using Financial.Integracao.Excel;
using EntitySpaces.Interfaces;
using Financial.Common;
using Financial.Investidor;
using System.Collections.Generic;
using Financial.Investidor.Enums;
using DevExpress.Web;
using Financial.Fundo;
using Financial.Fundo.Enums;
using Financial.Util;
using Financial.Util.Enums;
using Financial.Web.Util;
using Financial.Security;
using Financial.Security.Enums;

public partial class ImportacaoBasePage : BasePage
{

    /* Estrutura do Excel */
    private List<ValoresExcelOperacaoFundo> valoresExcelOperacaoFundo = new List<ValoresExcelOperacaoFundo>();

    /// <summary>
    /// Leitura de um arquivo Excel
    /// Saida: List<ValoresExcel> com os valores armazenados no arquivo
    /// </summary>
    /// <param name="streamExcel">Stream de bytes de um arquivo Excel</param>
    /// <exception cref="Exception">Se ocorreu problemas</exception>
    private void LerArquivoOperacaoFundo(Stream streamExcel)
    {
        #region Leitura do Arquivo Excel

        // Open Spreadsheet
        Bytescout.Spreadsheet.Spreadsheet document = new Bytescout.Spreadsheet.Spreadsheet();
        document.LoadFromStream(streamExcel);

        Bytescout.Spreadsheet.Worksheet workSheet = document.Workbook.Worksheets[0];

        //
        #region Confere Formato Arquivo
        string[] colunasConferencias = new string[] 
                                {"IdCliente", "IdCarteira", "DataOperacao", "DataConversao", "DataLiquidacao",
                                 "TipoOperacao", "TipoResgate", "Quantidade", "CotaOperacao", 
                                 "ValorBruto", "ValorLiquido", "ValorIR", "ValorIOF",
                                 "ValorPerformance", "RendimentoResgate", "LocalNegociacao", 
                                 "FieModalidade", "FieTabelaIr","DataAplicacaoCautelaResgate","CotaInformada","DataRegistro","Trader"
                                };

        bool formato = ValoresExcelUtil.isExcelFormatoValido(colunasConferencias, workSheet);

        if (!formato)
        {
            string mensagem = "Formato Interno do Arquivo Inválido.\n";
            mensagem += "Linha 1 do Arquivo deve ser: \n";
            for (int i = 0; i < colunasConferencias.Length; i++)
            {
                int coluna = i + 1;
                mensagem += "\tColuna " + coluna + ": " + colunasConferencias[i] + "\n";
            }

            document.Close();
            document.Dispose();

            throw new Exception(mensagem);
        }
        #endregion

        int index = 0;
        // This row,column index should be changed as per your need.
        // i.e. which cell in the excel you are interesting to read.
        //
        /* Formato: 1)IdCliente  - 2)IdCarteira  - 3)DataOperacao  - 4)DataConversao
                    5)DataLiquidacao - 6)TipoOperacao - 7)TipoResgate - 8)Quantidade - 9)CotaOperacao
         *          10)ValorBruto - 11)ValorLiquido - 12)ValorIR - 13)ValorIOF - 14)ValorPerformance
         *          15)RendimentoResgate - 16)LocalNegociacao
         *          17)FieModalidade - 18)FieTabelaIr - 19)DataAplicacaoCautelaResgate - 20)CotaInformada
         */

        int linha = 1;
        int coluna1 = 0, coluna2 = 1, coluna3 = 2,
            coluna4 = 3, coluna5 = 4, coluna6 = 5,
            coluna7 = 6, coluna8 = 7, coluna9 = 8,
            coluna10 = 9, coluna11 = 10, coluna12 = 11,
            coluna13 = 12, coluna14 = 13, coluna15 = 14,
            coluna16 = 15, coluna17 = 16, coluna18 = 17,
            coluna19 = 18, coluna20 = 19, coluna21 = 20,
            coluna22 = 21;
        //            
        try
        {
            // Enquanto idCliente tiver valor
            while (workSheet.Cell(linha, coluna1).Value != null)
            {
                ValoresExcelOperacaoFundo item = new ValoresExcelOperacaoFundo();
                //

                try
                {
                    item.IdCliente = workSheet.Cell(linha, coluna1).ValueAsInteger;
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message + " - IdCliente: linha " + (linha + 1));
                }

                if (workSheet.Cell(linha, coluna2).Value == null)
                    throw new Exception("IdCarteira não informado: linha " + (linha + 1));
                try
                {
                    item.IdCarteira = workSheet.Cell(linha, coluna2).ValueAsInteger;
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message + " - IdCarteira: linha " + (linha + 1));
                }

                if (workSheet.Cell(linha, coluna3).Value == null)
                    throw new Exception("DataOperacao não informada: linha " + (linha + 1));
                try
                {
                    item.DataOperacao = workSheet.Cell(linha, coluna3).ValueAsDateTime;
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message + " - DataOperacao: linha " + (linha + 1));
                }

                if (workSheet.Cell(linha, coluna4).Value != null)
                {
                    try
                    {
                        item.DataConversao = workSheet.Cell(linha, coluna4).ValueAsDateTime;
                    }
                    catch (Exception ex)
                    {
                        throw new Exception(ex.Message + " - DataConversao: linha " + (linha + 1));
                    }
                }

                if (workSheet.Cell(linha, coluna5).Value != null)
                {
                    try
                    {
                        item.DataLiquidacao = workSheet.Cell(linha, coluna5).ValueAsDateTime;
                    }
                    catch (Exception ex)
                    {
                        throw new Exception(ex.Message + " - DataLiquidacao: linha " + (linha + 1));
                    }
                }

                if (workSheet.Cell(linha, coluna6).Value == null)
                    throw new Exception("TipoOperacao não informado: linha " + (linha + 1));
                try
                {
                    item.TipoOperacao = Convert.ToByte(workSheet.Cell(linha, coluna6).Value);
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message + " - TipoOperacao: linha " + (linha + 1));
                }

                if (workSheet.Cell(linha, coluna7).Value != null && workSheet.Cell(linha, coluna7).Value.ToString().Trim() != "")
                {
                    try
                    {
                        item.TipoResgate = Convert.ToByte(workSheet.Cell(linha, coluna7).Value);
                    }
                    catch (Exception ex)
                    {
                        throw new Exception(ex.Message + " - TipoResgate: linha " + (linha + 1));
                    }
                }

                if (workSheet.Cell(linha, coluna8).Value == null)
                    throw new Exception("Quantidade não informada: linha " + (linha + 1));
                try
                {
                    item.Quantidade = Convert.ToDecimal(workSheet.Cell(linha, coluna8).Value);
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message + " - Quantidade: linha " + (linha + 1));
                }

                if (workSheet.Cell(linha, coluna9).Value == null)
                    throw new Exception("CotaOperacao não informada: linha " + (linha + 1));
                try
                {
                    item.CotaOperacao = Convert.ToDecimal(workSheet.Cell(linha, coluna9).Value);
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message + " - CotaOperacao: linha " + (linha + 1));
                }

                if (workSheet.Cell(linha, coluna10).Value == null)
                    throw new Exception("ValorBruto não informado: linha " + (linha + 1));
                try
                {
                    item.ValorBruto = Convert.ToDecimal(workSheet.Cell(linha, coluna10).Value);
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message + " - ValorBruto: linha " + (linha + 1));
                }

                if (workSheet.Cell(linha, coluna11).Value == null)
                    throw new Exception("ValorLiquido não informado: linha " + (linha + 1));
                try
                {
                    item.ValorLiquido = Convert.ToDecimal(workSheet.Cell(linha, coluna11).Value);
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message + " - ValorLiquido: linha " + (linha + 1));
                }

                if (workSheet.Cell(linha, coluna12).Value == null)
                    throw new Exception("ValorIR não informado: linha " + (linha + 1));
                try
                {
                    item.ValorIR = Convert.ToDecimal(workSheet.Cell(linha, coluna12).Value);
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message + " - ValorIR: linha " + (linha + 1));
                }

                if (workSheet.Cell(linha, coluna13).Value == null)
                    throw new Exception("ValorIOF não informado: linha " + (linha + 1));
                try
                {
                    item.ValorIOF = Convert.ToDecimal(workSheet.Cell(linha, coluna13).Value);
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message + " - ValorIOF: linha " + (linha + 1));
                }

                if (workSheet.Cell(linha, coluna14).Value == null)
                    throw new Exception("ValorPerformance não informado: linha " + (linha + 1));
                try
                {
                    item.ValorPerformance = Convert.ToDecimal(workSheet.Cell(linha, coluna14).Value);
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message + " - ValorPerformance: linha " + (linha + 1));
                }

                if (workSheet.Cell(linha, coluna15).Value == null)
                    throw new Exception("RendimentoResgate não informado: linha " + (linha + 1));
                try
                {
                    item.RendimentoResgate = Convert.ToDecimal(workSheet.Cell(linha, coluna15).Value);
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message + " - RendimentoResgate: linha " + (linha + 1));
                }

                if (workSheet.Cell(linha, coluna16).Value == null)
                {
                    item.LocalNegociacao = null;
                }
                else
                {
                    try
                    {
                        item.LocalNegociacao = workSheet.Cell(linha, coluna16).ValueAsString;
                    }
                    catch (Exception ex)
                    {
                        throw new Exception(ex.Message + " - LocalNegociacao: linha " + (linha + 1));
                    }
                }

                Carteira carteira = new Carteira();
                if (carteira.LoadByPrimaryKey(item.IdCarteira))
                {
                    if (carteira.Fie.Equals("S"))
                    {
                        if (workSheet.Cell(linha, coluna17).Value != null && workSheet.Cell(linha, coluna17).Value.ToString().Trim() != "")
                        {
                            try
                            {
                                item.FieModalidade = Convert.ToInt32(workSheet.Cell(linha, coluna17).Value);
                            }
                            catch (Exception ex)
                            {
                                throw new Exception(ex.Message + " - FieModalidade: linha " + (linha + 1));
                            }
                        }
                        else
                        {
                            throw new Exception(" Para fundos de previdência, obrigatório informar modalidade: linha " + (linha + 1));
                        }

                        if (workSheet.Cell(linha, coluna18).Value != null && workSheet.Cell(linha, coluna18).Value.ToString().Trim() != "")
                        {
                            try
                            {
                                item.FieTabelaIr = Convert.ToInt32(workSheet.Cell(linha, coluna18).Value);
                            }
                            catch (Exception ex)
                            {
                                throw new Exception(ex.Message + " - FieTabelaIr: linha " + (linha + 1));
                            }
                        }
                        else
                        {
                            throw new Exception(" Para fundos de previdência, obrigatório informar tabela de IR: linha " + (linha + 1));
                        }
                    }
                }

                if (item.TipoResgate == 1)
                {
                    if (workSheet.Cell(linha, coluna19).Value == null)
                        throw new Exception("DataAplicacaoCautelaResgate não informada: linha " + (linha + 1));

                    try
                    {
                        item.DataAplicacaoCautelaResgate = workSheet.Cell(linha, coluna19).ValueAsDateTime;
                    }
                    catch (Exception ex)
                    {
                        throw new Exception(ex.Message + " - DataAplicacaoCautelaResgate: linha " + (linha + 1));
                    }
                }
                else
                {
                    item.DataAplicacaoCautelaResgate = null;
                }

                try
                {
                    if (workSheet.Cell(linha, coluna20).Value == null)
                        item.CotaInformada = null;
                    else
                        item.CotaInformada = Convert.ToDecimal(workSheet.Cell(linha, coluna20).Value);
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message + " - CotaInformada: linha " + (linha + 1));
                }

                if (workSheet.Cell(linha, coluna21).Value != null)
                {
                    try
                    {
                        item.DataRegistro = workSheet.Cell(linha, coluna21).ValueAsDateTime;
                    }
                    catch (Exception ex)
                    {
                        throw new Exception(ex.Message + " - DataRegistro: linha " + (linha + 1));
                    }
                }
                else
                {
                    item.DataRegistro = null;
                }

                if (workSheet.Cell(linha, coluna22).Value != null &&
                     workSheet.Cell(linha, coluna22).Value.ToString().Trim() != "")
                {
                    try
                    {
                        item.Trader = workSheet.Cell(linha, coluna22).ValueAsString;
                    }
                    catch (Exception ex)
                    {
                        throw new Exception(ex.Message + " - Trader: linha " + (linha + 1));
                    }
                }

                this.valoresExcelOperacaoFundo.Add(item);

                index++;
                linha = 1 + index;

                Console.WriteLine("{0},{1},{2},{3},{4},{5},{6},{7},{8},{9},{10},{11},{12},{13},{14},{15},{16},{17}",
                    item.IdCliente, item.IdCarteira, item.DataOperacao, item.DataConversao,
                    item.DataLiquidacao, item.TipoOperacao, item.TipoResgate, item.Quantidade,
                    item.CotaOperacao, item.ValorBruto, item.ValorLiquido, item.ValorIR, item.ValorIOF,
                    item.ValorPerformance, item.RendimentoResgate, item.LocalNegociacao,
                    item.FieModalidade, item.FieTabelaIr);
            }
        }
        catch (Exception ex)
        {
            document.Close();
            document.Dispose();

            throw new Exception(ex.Message);
        }
        #endregion

        document.Close();
        document.Dispose();
    }

    /// <summary>
    /// Gera e carrega as operações de Cliente em Fundos de acordo 
    /// com o objeto List<ValoresExcel>
    /// </summary>
    private void CarregaOperacaoFundo(bool ignoraCarteiraInexistente)
    {
        OperacaoFundoCollection operacaoFundoCollection = new OperacaoFundoCollection();

        //    
        for (int i = 0; i < this.valoresExcelOperacaoFundo.Count; i++)
        {
            ValoresExcelOperacaoFundo valoresExcel = this.valoresExcelOperacaoFundo[i];

            #region Verifica se Existe Carteira
            Carteira carteira = new Carteira();
            if (!carteira.LoadByPrimaryKey(valoresExcel.IdCarteira))
            {
                if (ignoraCarteiraInexistente)
                {
                    continue;
                }
                else
                {
                    throw new Exception("Carteira Não existente : " + valoresExcel.IdCarteira);
                }
            }
            #endregion

            #region Verifica se Existe Cliente
            Cliente cliente = new Cliente();
            List<esQueryItem> campos = new List<esQueryItem>();
            campos.Add(cliente.Query.IdCliente);
            campos.Add(cliente.Query.DataDia);
            if (!cliente.LoadByPrimaryKey(campos, valoresExcel.IdCliente))
            {
                if (ignoraCarteiraInexistente)
                {
                    continue;
                }
                else
                {
                    throw new Exception("Cliente Não existente : " + valoresExcel.IdCliente);
                }
            }

            DateTime dataDia = cliente.DataDia.Value;
            #endregion

            #region OperacaoFundo
            OperacaoFundo operacaoFundo = new OperacaoFundo();
            // Dados do Arquivo
            operacaoFundo.IdCliente = valoresExcel.IdCliente;
            operacaoFundo.IdCarteira = valoresExcel.IdCarteira;
            operacaoFundo.DataOperacao = valoresExcel.DataOperacao;
            operacaoFundo.TipoOperacao = valoresExcel.TipoOperacao;

            if (DateTime.Compare(dataDia, operacaoFundo.DataOperacao.Value) >= 0)
            {
                //Para operações na data ou superiores à data dia, é possível informar apenas a data da operação
                carteira = new Carteira();
                campos = new List<esQueryItem>();
                campos.Add(carteira.Query.DiasCotizacaoAplicacao);
                campos.Add(carteira.Query.DiasCotizacaoResgate);
                campos.Add(carteira.Query.DiasLiquidacaoAplicacao);
                campos.Add(carteira.Query.DiasLiquidacaoResgate);
                carteira.LoadByPrimaryKey(campos, operacaoFundo.IdCarteira.Value);

                if (String.IsNullOrEmpty(valoresExcel.DataConversao.ToString()))
                {
                    DateTime dataConversao = new DateTime();
                    if (operacaoFundo.TipoOperacao.Value == (byte)TipoOperacaoFundo.Aplicacao ||
                        operacaoFundo.TipoOperacao.Value == (byte)TipoOperacaoFundo.AplicacaoAcoesEspecial)
                    {
                        dataConversao = Calendario.AdicionaDiaUtil(operacaoFundo.DataOperacao.Value, carteira.DiasCotizacaoAplicacao.Value);
                    }
                    if (operacaoFundo.TipoOperacao.Value != (byte)TipoOperacaoFundo.AplicacaoCotasEspecial)
                    {
                        dataConversao = Calendario.AdicionaDiaUtil(operacaoFundo.DataOperacao.Value, carteira.DiasCotizacaoResgate.Value);
                    }
                    operacaoFundo.DataConversao = dataConversao;
                }
                else
                {
                    operacaoFundo.DataConversao = valoresExcel.DataConversao;
                }
                if (String.IsNullOrEmpty(valoresExcel.DataLiquidacao.ToString()))
                {
                    DateTime dataLiquidacao = new DateTime();
                    if (operacaoFundo.TipoOperacao.Value == (byte)TipoOperacaoFundo.Aplicacao ||
                        operacaoFundo.TipoOperacao.Value == (byte)TipoOperacaoFundo.AplicacaoAcoesEspecial)
                    {
                        dataLiquidacao = Calendario.AdicionaDiaUtil(operacaoFundo.DataOperacao.Value, carteira.DiasCotizacaoAplicacao.Value);
                    }
                    if (operacaoFundo.TipoOperacao.Value != (byte)TipoOperacaoFundo.AplicacaoCotasEspecial)
                    {
                        dataLiquidacao = Calendario.AdicionaDiaUtil(operacaoFundo.DataOperacao.Value, carteira.DiasCotizacaoResgate.Value);
                    }
                    operacaoFundo.DataLiquidacao = dataLiquidacao;
                }
                else
                {
                    operacaoFundo.DataLiquidacao = valoresExcel.DataLiquidacao;
                }
            }
            else
            {
                if (String.IsNullOrEmpty(valoresExcel.DataConversao.ToString()) || String.IsNullOrEmpty(valoresExcel.DataLiquidacao.ToString()))
                {
                    throw new Exception("Data da conversão e Data da liquidação devem ser informadas para Carteira " + operacaoFundo.IdCarteira.Value.ToString());
                }
                else
                {
                    operacaoFundo.DataConversao = valoresExcel.DataConversao;
                    operacaoFundo.DataLiquidacao = valoresExcel.DataLiquidacao;
                }
            }

            operacaoFundo.DataAgendamento = valoresExcel.DataOperacao; //Repete data operação                    
            operacaoFundo.TipoResgate = valoresExcel.TipoResgate;
            operacaoFundo.IdFormaLiquidacao = 1; //TODO ajustar para enum fixo depois
            operacaoFundo.Quantidade = valoresExcel.Quantidade;
            operacaoFundo.CotaOperacao = valoresExcel.CotaOperacao;
            operacaoFundo.ValorIR = valoresExcel.ValorIR;
            operacaoFundo.ValorIOF = valoresExcel.ValorIOF;
            operacaoFundo.ValorPerformance = valoresExcel.ValorPerformance;
            operacaoFundo.ValorBruto = valoresExcel.ValorBruto;
            operacaoFundo.ValorLiquido = valoresExcel.ValorLiquido;
            operacaoFundo.ValorCPMF = 0;
            operacaoFundo.PrejuizoUsado = 0; //Quase certo de não ser necessário mesmo!
            operacaoFundo.RendimentoResgate = valoresExcel.RendimentoResgate;
            operacaoFundo.VariacaoResgate = 0; //Quase certo de não ser necessário mesmo!
            operacaoFundo.Fonte = (byte)FonteOperacaoFundo.Manual;
            operacaoFundo.IdPosicaoResgatada = valoresExcel.IdPosicaoResgatadaCotista;           

            if (valoresExcel.DataAplicacaoCautelaResgate.HasValue)
            {
                operacaoFundo.DataAplicCautelaResgatada = valoresExcel.DataAplicacaoCautelaResgate;

                if (!string.IsNullOrEmpty(operacaoFundo.Observacao))
                    operacaoFundo.Observacao += " / ";
                else
                    operacaoFundo.Observacao += string.Empty;

                operacaoFundo.Observacao += "Op. de Resgate importada especificando a data de aplicação a ser impactada - Dt.Aplic = " + String.Format("{0:dd/MM/yyyy}", valoresExcel.DataAplicacaoCautelaResgate.Value);
            }

            operacaoFundo.IdLocalNegociacao = (int)Financial.Common.Enums.LocalNegociacaoFixo.Brasil;
            if (!string.IsNullOrEmpty(valoresExcel.LocalNegociacao))
            {
                LocalNegociacao localNegociacao = new LocalNegociacao();
                localNegociacao.Query.Where(localNegociacao.Query.Codigo.ToUpper().Equal(valoresExcel.LocalNegociacao.ToUpper()));

                if (localNegociacao.Query.Load())
                    operacaoFundo.IdLocalNegociacao = localNegociacao.IdLocalNegociacao.Value;
                else
                    throw new Exception("Local de Negociação não cadastrado: " + valoresExcel.LocalNegociacao);
            }

            #region Operação retroativa - Verifica se deve marcar o cliente para efetuar o reprocessamento
            if (DateTime.Compare(cliente.DataDia.Value, operacaoFundo.DataOperacao.Value) > 0)
            {
                BoletoRetroativo boletoRetroativo = new BoletoRetroativo();
                if (boletoRetroativo.LoadByPrimaryKey(valoresExcel.IdCliente))
                {
                    if (DateTime.Compare(boletoRetroativo.DataBoleto.Value, operacaoFundo.DataOperacao.Value) > 0)
                    {
                        boletoRetroativo.DataBoleto = operacaoFundo.DataOperacao.Value;
                    }
                }
                else
                {
                    boletoRetroativo.IdCliente = valoresExcel.IdCliente;
                    boletoRetroativo.DataBoleto = operacaoFundo.DataOperacao.Value;
                    boletoRetroativo.TipoMercado = (int)TipoMercado.Fundos;
                }

                boletoRetroativo.Save();
            }
            #endregion

            operacaoFundo.DataRegistro = valoresExcel.DataRegistro.HasValue ? valoresExcel.DataRegistro.Value : dataDia;
            operacaoFundo.FieModalidade = valoresExcel.FieModalidade;
            operacaoFundo.FieTabelaIr = valoresExcel.FieTabelaIr;

            #region Verifica se Existe Trader
            Trader trader = new Trader();
            if (!String.IsNullOrEmpty(valoresExcel.Trader))
            {
                trader.Query.Select(trader.Query.IdTrader)
                             .Where(trader.Query.Nome == valoresExcel.Trader.Trim());
                //
                if (!trader.Query.Load())
                {
                    throw new Exception("Trader Não existente: " + valoresExcel.Trader.Trim());
                }
            }
            operacaoFundo.IdTrader = trader.IdTrader;
            #endregion

            // Attach the object
            operacaoFundoCollection.AttachEntity(operacaoFundo);
            #endregion
        }

        // Salva Operações presentes no Excel
        operacaoFundoCollection.Save();
    }

    /// <summary>
    /// Processa a Planilha de OperacaoFundo após ter acabado o Upload
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void uplOperacaoFundo_FileUploadComplete(FileUploadCompleteEventArgs e, DevExpress.Web.ASPxCheckBox checkIgnora)
    {
        e.CallbackData = "";

        #region Trata Extensão Válida
        if (!ValoresExcelUtil.isExtensaoExcel(e.UploadedFile.FileName.Trim()))
        {
            e.CallbackData = "Importação Operação Fundo - Formato de Arquivo Inválido. Formato permitido: .xls ou .xlsx\n\n";
            return;
        }
        #endregion

        // Stream de bytes com o Conteudo do Arquivo Excel
        Stream sr = e.UploadedFile.FileContent;

        #region Processamento Arquivo Excel

        //Reseta Vetor de Dados
        this.valoresExcelOperacaoFundo = new List<ValoresExcelOperacaoFundo>();
        //
        try
        {
            // Ler Arquivo
            this.LerArquivoOperacaoFundo(sr);
            this.CarregaOperacaoFundo(checkIgnora.Checked);
        }
        catch (Exception e2)
        {
            e.CallbackData = "Importação Operação Fundo - " + e2.Message;
            return;
        }

        new HistoricoLog().InsereHistoricoLog(DateTime.Now, DateTime.Now,
            "Importação Planilha Operação Fundo",
            HttpContext.Current.User.Identity.Name, UtilitarioWeb.GetIP(Request), "", HistoricoLogOrigem.Outros);

        #endregion
    }
}