﻿using System;
using System.Collections.Generic;
using System.IO;
using Financial.Integracao.Excel;
using Financial.Integracao.Excel.Util;
using Financial.RendaFixa;
using Financial.Web.Common;
using EntitySpaces.Interfaces;
using DevExpress.Web;
using Financial.Security;
using System.Web;
using Financial.Web.Util;
using Financial.Security.Enums;
using Financial.Util;
using Financial.Common.Enums;
using System.Collections;
using Financial.Investidor;

public partial class ImportacaoBasePage : BasePage
{

    /* Estrutura do Excel */
    private List<ValoresExcelMTMManual> valoresExcelMTMManual = new List<ValoresExcelMTMManual>();

    //
    List<int> idClienteDeletar = new List<int>();
    List<DateTime> dataVigenciaDeletar = new List<DateTime>();
    //

    /// <summary>
    /// Leitura de um arquivo Excel
    /// Saida: List<ValoresExcel> com os valores armazenados no arquivo
    /// </summary>
    /// <param name="streamExcel">Stream de bytes de um arquivo Excel</param>
    /// <exception cref="Exception">Se ocorreu problemas</exception>
    private void LerArquivoMTMManual(Stream streamExcel)
    {

        #region Leitura do Arquivo Excel

        // Open Spreadsheet
        Bytescout.Spreadsheet.Spreadsheet document = new Bytescout.Spreadsheet.Spreadsheet();
        document.LoadFromStream(streamExcel);

        Bytescout.Spreadsheet.Worksheet workSheet = document.Workbook.Worksheets[0];

        //
        #region Confere Formato Arquivo
        string[] colunasConferencias = new string[] { "IdCliente", "IdTitulo", "IdOperacao", "DtVigencia", "Taxa252", "PuMTM" };

        bool formato = ValoresExcelUtil.isExcelFormatoValido(colunasConferencias, workSheet);

        if (!formato)
        {
            string mensagem = "Formato Interno do Arquivo Inválido.\n";
            mensagem += "Linha 1 do Arquivo deve ser: \n";
            for (int i = 0; i < colunasConferencias.Length; i++)
            {
                int coluna = i + 1;
                mensagem += "\tColuna " + coluna + ": " + colunasConferencias[i] + "\n";
            }

            document.Close();
            document.Dispose();

            throw new Exception(mensagem);
        }
        #endregion

        //
        int index = 0;
        // This row,column index should be changed as per your need.
        // i.e. which cell in the excel you are interesting to read.
        //
        /* Formato: 1)IdCliente       -2)IdTitulo     
         *          3)IdOperacao      -4)DtVigencia     
         *          5)Taxa252         -6)PuMTM */

        int linha = 1;
        int coluna1 = 0, coluna2 = 1, coluna3 = 2, coluna4 = 3, coluna5 = 4, coluna6 = 5;

        //            
        try
        {
            // Enquanto Data tiver valor
            while (workSheet.Cell(linha, coluna1).Value != null)
            {
                ValoresExcelMTMManual item = new ValoresExcelMTMManual();

                #region IdCliente
                if (workSheet.Cell(linha, coluna1).Value == null)
                    throw new Exception("IdCliente não informado: linha " + (linha + 1));

                try
                {
                    item.IdCliente = workSheet.Cell(linha, coluna1).ValueAsInteger;
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message + " - IdCliente: linha " + (linha + 1));
                }
                #endregion

                #region IdTitulo
                if (workSheet.Cell(linha, coluna2).Value == null)
                    throw new Exception("IdTitulo não informado: linha " + (linha + 1));

                try
                {
                    item.IdTitulo = workSheet.Cell(linha, coluna2).ValueAsInteger;
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message + " - IdTitulo: linha " + (linha + 1));
                }
                #endregion

                #region IdOperacao
                if (workSheet.Cell(linha, coluna3).Value != null)
                {

                    try
                    {
                        item.IdOperacao = workSheet.Cell(linha, coluna3).ValueAsInteger;
                    }
                    catch (Exception ex)
                    {
                        throw new Exception(ex.Message + " - IdOperacao: linha " + (linha + 1));
                    }
                }
                else
                {
                    item.IdOperacao = null;
                }
                #endregion

                #region DtVigencia
                if (workSheet.Cell(linha, coluna4).Value == null)
                    throw new Exception("DtVigencia não informado: linha " + (linha + 1));

                try
                {
                    item.DtVigencia = workSheet.Cell(linha, coluna4).ValueAsDateTime;
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message + " - DtVigencia: linha " + (linha + 1));
                }
                #endregion

                #region Taxa252
                if (workSheet.Cell(linha, coluna5).Value != null)
                {
                    try
                    {
                        item.Taxa252 = Convert.ToDecimal(workSheet.Cell(linha, coluna5).Value);
                    }
                    catch (Exception ex)
                    {
                        throw new Exception(ex.Message + " - Taxa252: linha " + (linha + 1));
                    }
                }
                else
                {
                    item.Taxa252 = 0;
                }
                #endregion

                #region PuMTM
                if (workSheet.Cell(linha, coluna6).Value != null)
                {
                    try
                    {
                        item.PuMTM = Convert.ToDecimal(workSheet.Cell(linha, coluna6).Value);
                    }
                    catch (Exception ex)
                    {
                        throw new Exception(ex.Message + " - PuMTM: linha " + (linha + 1));
                    }
                }
                else
                {
                    item.PuMTM = 0;
                }
                #endregion

                this.valoresExcelMTMManual.Add(item);

                index++;
                linha = 1 + index;

                Console.WriteLine("{0},{1},{2},{3},{4},{5}", item.IdCliente, item.IdTitulo, item.IdOperacao, item.DtVigencia, item.Taxa252, item.PuMTM);
            }
        }
        catch (Exception ex)
        {
            document.Close();
            document.Dispose();

            throw new Exception(ex.Message);
        }
        #endregion

        document.Close();
        document.Dispose();
    }

    /// <summary>
    /// Gera e Carrega as Cotações Series de acordo com o objeto List<ValoresExcel>
    /// </summary>     
    private void CarregaMTMManual()
    {
        MTMManualCollection mTMManualCollection = new MTMManualCollection();
        MTMManualCollection mTMManualDeletarCollection = new MTMManualCollection();

        List<int> lstTitulosValidados = new List<int>();
        List<int> lstClientesValidados = new List<int>();
        Hashtable hsChaveDelete = new Hashtable(); //Usado para diminuir a quantidade de interações no momento da deleção dos registros.
        //    
        for (int i = 0; i < this.valoresExcelMTMManual.Count; i++)
        {
            ValoresExcelMTMManual valoresExcel = this.valoresExcelMTMManual[i];
            //
            #region Verifica se Existe Operacao Renda Fixa
            if (valoresExcel.IdOperacao.HasValue)
            {
                OperacaoRendaFixa operacaoRendaFixa = new OperacaoRendaFixa();
                if (!operacaoRendaFixa.LoadByPrimaryKey(valoresExcel.IdOperacao.Value))
                {
                    throw new Exception("IdOperacao não existente: " + valoresExcel.IdOperacao);
                }

                if (operacaoRendaFixa.IdTitulo.Value != valoresExcel.IdTitulo)
                    throw new Exception("Id.Título não é igual ao Id.Título da Operação: Id.Operação - " + valoresExcel.IdOperacao + " Id.Título - " + valoresExcel.IdTitulo);
            }
            #endregion

            if (!lstClientesValidados.Contains(valoresExcel.IdCliente))
            {
                Cliente cliente = new Cliente();
                if (cliente.LoadByPrimaryKey(valoresExcel.IdCliente))
                    lstClientesValidados.Add(valoresExcel.IdCliente);
                else
                    throw new Exception("Cliente não existente: " + valoresExcel.IdCliente);
            }

            if (!lstTitulosValidados.Contains(valoresExcel.IdTitulo))
            {
                TituloRendaFixa titulo = new TituloRendaFixa();
                if (titulo.LoadByPrimaryKey(valoresExcel.IdTitulo))
                    lstTitulosValidados.Add(valoresExcel.IdTitulo);
                else
                    throw new Exception("Título não existente: " + valoresExcel.IdTitulo);
            }

            if ((valoresExcel.Taxa252 + valoresExcel.PuMTM == 0) || (valoresExcel.Taxa252 != 0 && valoresExcel.PuMTM != 0))
            {
                if (valoresExcel.IdOperacao.HasValue)
                    throw new Exception("Taxa ou PU devem ser preenchidos!! IdOperacao: " + valoresExcel.IdOperacao);
                else
                    throw new Exception("Taxa ou PU devem ser preenchidos!! Cliente: " + valoresExcel.IdCliente + " Titulo: " + valoresExcel.IdTitulo);
            }            

            // Salva id e Data para poder Deletar
            string chave = valoresExcel.IdCliente + "|" + valoresExcel.DtVigencia.ToShortDateString();
            if (!hsChaveDelete.Contains(chave))
            {
                this.idClienteDeletar.Add(valoresExcel.IdCliente);
                this.dataVigenciaDeletar.Add(valoresExcel.DtVigencia);
                hsChaveDelete.Add(chave, valoresExcel.IdCliente);
            }

            #region MTMManual
            MTMManual mtmManual = mTMManualCollection.AddNew();
            // Dados do Arquivo
            mtmManual.IdCliente = valoresExcel.IdCliente;
            mtmManual.IdTitulo = valoresExcel.IdTitulo;
            mtmManual.IdOperacao = valoresExcel.IdOperacao;
            mtmManual.DtVigencia = valoresExcel.DtVigencia;
            mtmManual.Taxa252 = valoresExcel.Taxa252;
            mtmManual.PuMTM = valoresExcel.PuMTM;
            //
            #endregion

        }

        using (esTransactionScope scope = new esTransactionScope())
        {
            #region Deleta MTMManual
            
            for (int i = 0; i < this.idClienteDeletar.Count; i++)
            {
                mTMManualDeletarCollection = new MTMManualCollection();

                mTMManualDeletarCollection.Query
                .Where(mTMManualDeletarCollection.Query.IdCliente.Equal(this.idClienteDeletar[i]) &
                       mTMManualDeletarCollection.Query.DtVigencia.Equal(this.dataVigenciaDeletar[i]));

                mTMManualDeletarCollection.Query.Load();
                mTMManualDeletarCollection.MarkAllAsDeleted();
                mTMManualDeletarCollection.Save();
            }
            #endregion

            // Salva MTM Manual presentes no Excel                
            mTMManualCollection.Save();

            scope.Complete();
        }
    }

    /// <summary>
    /// Processa a Planilha de MTM Manual após ter acabado o Upload
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void uplMTMManual_FileUploadComplete(object sender, FileUploadCompleteEventArgs e)
    {
        e.CallbackData = "";

        #region Trata Extensão Válida
        if (!ValoresExcelUtil.isExtensaoExcel(e.UploadedFile.FileName.Trim()))
        {
            e.CallbackData = "Importação MTM Manual - Formato de Arquivo Inválido. Formato permitido: .xls ou .xlsx\n\n";
            return;
        }
        #endregion

        // Stream de bytes com o Conteudo do Arquivo Excel
        Stream sr = e.UploadedFile.FileContent;

        #region Processamento Arquivo Excel

        //Reseta Vetor de Dados
        this.valoresExcelMTMManual = new List<ValoresExcelMTMManual>();
        this.idClienteDeletar = new List<int>();
        this.dataVigenciaDeletar = new List<DateTime>();
        //
        try
        {
            // Ler Arquivo
            this.LerArquivoMTMManual(sr);
            // Carrega Arquivo
            this.CarregaMTMManual();
        }
        catch (Exception e2)
        {
            if (e2.Message.Contains("Violation of PRIMARY KEY"))
            {
                e.CallbackData = "Importação não Concluída: 2 registros com a mesma IdOperacao/DtVigencia";
            }
            else
            {
                e.CallbackData = "Importação MTM Manual - " + e2.Message;
            }
            return;
        }

        new HistoricoLog().InsereHistoricoLog(DateTime.Now, DateTime.Now,
            "Importação Planilha MTM Manual",
            HttpContext.Current.User.Identity.Name, UtilitarioWeb.GetIP(Request), "", HistoricoLogOrigem.Outros);

        #endregion
    }
}
