﻿using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Financial.Web.Common;
using System.IO;
using Financial.Integracao.Excel.Util;
using Financial.Integracao.Excel;
using EntitySpaces.Interfaces;
using Financial.Common;
using Financial.Investidor;
using System.Collections.Generic;
using Financial.Investidor.Enums;
using DevExpress.Web;
using Financial.BMF;
using Financial.BMF.Enums;

using Financial.Common.Exceptions;
using Financial.Security;
using Financial.Web.Util;
using Financial.Security.Enums;
using Financial.Common.Enums;

public partial class ImportacaoBasePage : BasePage {

    /* Estrutura do Excel */
    private List<ValoresExcelOrdemBMF> valoresExcelOrdemBMF = new List<ValoresExcelOrdemBMF>();

    /// <summary>
    /// Leitura de um arquivo Excel
    /// Saida: List<ValoresExcel>Com os Valores Armazenados no arquivo
    /// </summary>
    /// <param name="streamExcel">Stream de bytes de um arquivo Excel</param>
    /// <exception cref="Exception">Se ocorreu problemas</exception>
    private void LerArquivoOrdemBMF(Stream streamExcel) {

        #region Leitura do Arquivo Excel

        // Open Spreadsheet
        Bytescout.Spreadsheet.Spreadsheet document = new Bytescout.Spreadsheet.Spreadsheet();
        document.LoadFromStream(streamExcel);

        Bytescout.Spreadsheet.Worksheet workSheet = document.Workbook.Worksheets[0];
        //
        #region Confere Formato Arquivo
        string[] colunasConferencias = new string[] 
                                {"IdCliente","CdAtivoBMF","Serie","CodigoBovespa","TipoOrdem","Data",
                                 "PU","Valor","Quantidade","PercentualDesconto","Trader","CalculaDespesas",
                                "Taxa", "TipoEstrategia", "LocalNegociacao", "Clearing", "LocalCustodia",
                                "AgenteCustodia_Liquidacao", "Emolumentos", "TaxaRegistro", "ValorCorretagem", "TaxaClearing"
                                };

        bool formato = ValoresExcelUtil.isExcelFormatoValido(colunasConferencias, workSheet);
        if (!formato) {
            string mensagem = "Formato Interno do Arquivo Inválido.\n";
            mensagem += "Linha 1 do Arquivo deve ser: \n";
            for (int i = 0; i < colunasConferencias.Length; i++) {
                int coluna = i + 1;
                mensagem += "\tColuna " + coluna + ": " + colunasConferencias[i] + "\n";
            }

            document.Close();
            document.Dispose();

            throw new Exception(mensagem);
        }
        #endregion

        int index = 0;
        // This row,column index should be changed as per your need.
        // i.e. which cell in the excel you are interesting to read.
        //
        /* Formato: 1)IdCliente     - 2)CdAtivoBMF               - 3) Serie                       - 4)Corretora       - 5)TipoOrdem
         *          6)Data          - 7)PU                      - 8)Valor                        - 9)Quantidade      - 10)PercentualDesconto
         *          11)Trader       - 12)CalculaDespesas        - 13)Taxa                        - 14)TipoEstrategia - 15)LocalNegociacao
         *          16)Clearing     - 17)LocalCustodia          - 18)AgenteCustodia_Liquidacao   - 19)Emolumentos
         *          20)TaxaRegistro - 21)ValorCorretagem        - 22)TaxaClearing
         */

        int linha = 1;
        int coluna1 = 0, coluna2 = 1, coluna3 = 2,
            coluna4 = 3, coluna5 = 4, coluna6 = 5,
            coluna7 = 6, coluna8 = 7, coluna9 = 8,
            coluna10 = 9, coluna11 = 10, coluna12 = 11,
            coluna13 = 12, coluna14 = 13, coluna15 = 14,
            coluna16 = 15, coluna17 = 16, coluna18 = 17, 
            coluna19 = 18, coluna20 = 19, coluna21 = 20,
            coluna22 = 21;

        //            
        try {
            // Enquanto idCliente tiver valor
            while (workSheet.Cell(linha, coluna1).Value != null) {
                ValoresExcelOrdemBMF item = new ValoresExcelOrdemBMF();
                
                try {
                    item.IdCliente = workSheet.Cell(linha, coluna1).ValueAsInteger;
                }
                catch (Exception ex) {
                    throw new Exception(ex.Message + " - IdCliente: linha " + (linha + 1));
                }

                if (workSheet.Cell(linha, coluna2).Value == null)
                    throw new Exception("CdAtivoBMF não informado: linha " + (linha + 1));
                try {
                    item.CdAtivoBMF = workSheet.Cell(linha, coluna2).ValueAsString;
                }
                catch (Exception ex) {
                    throw new Exception(ex.Message + " - CdAtivoBMF: linha " + (linha + 1));
                }

                if (workSheet.Cell(linha, coluna3).Value == null)
                    throw new Exception("Serie não informada: linha " + (linha + 1));
                try {
                    item.Serie = workSheet.Cell(linha, coluna3).ValueAsString;
                }
                catch (Exception ex) {
                    throw new Exception(ex.Message + " - Serie: linha " + (linha + 1));
                }

                if (workSheet.Cell(linha, coluna4).Value == null)
                    throw new Exception("CodigoBovespa não informado: linha " + (linha + 1));
                try {
                    item.CodigoBovespa = workSheet.Cell(linha, coluna4).ValueAsInteger;
                }
                catch (Exception ex) {
                    throw new Exception(ex.Message + " - CodigoBovespa: linha " + (linha + 1));
                }

                if (workSheet.Cell(linha, coluna5).Value == null)
                    throw new Exception("TipoOrdem não informado: linha " + (linha + 1));
                try {
                    item.TipoOrdem = workSheet.Cell(linha, coluna5).ValueAsString;
                }
                catch (Exception ex) {
                    throw new Exception(ex.Message + " - TipoOrdem: linha " + (linha + 1));
                }

                if (workSheet.Cell(linha, coluna6).Value == null)
                    throw new Exception("DataOrdem não informada: linha " + (linha + 1));
                try {
                    item.DataOrdem = workSheet.Cell(linha, coluna6).ValueAsDateTime;
                }
                catch (Exception ex) {
                    throw new Exception(ex.Message + " - DataOrdem: linha " + (linha + 1));
                }
                
                item.Pu = 0;
                if (workSheet.Cell(linha, coluna7).Value != null && workSheet.Cell(linha, coluna7).ValueAsString != "") {
                    try {
                        item.Pu = Convert.ToDecimal(workSheet.Cell(linha, coluna7).Value);
                    }
                    catch (Exception ex) {
                        throw new Exception(ex.Message + " - Pu: linha " + (linha + 1));
                    }
                }

                if (workSheet.Cell(linha, coluna8).Value != null && workSheet.Cell(linha, coluna8).ValueAsString != "") {
                    try {
                        item.Valor = Convert.ToDecimal(workSheet.Cell(linha, coluna8).Value);
                    }
                    catch (Exception ex) {
                        throw new Exception(ex.Message + " - Valor: linha " + (linha + 1));
                    }
                }
                else {
                    item.Valor = 0.0M;
                }

                if (workSheet.Cell(linha, coluna9).Value == null)
                    throw new Exception("Quantidade não informada: linha " + (linha + 1));
                try {
                    item.Quantidade = Convert.ToDecimal(workSheet.Cell(linha, coluna9).Value);
                }
                catch (Exception ex) {
                    throw new Exception(ex.Message + " - Quantidade: linha " + (linha + 1));
                }

                if (workSheet.Cell(linha, coluna10).Value == null)
                    throw new Exception("PercentualDesconto não informado: linha " + (linha + 1));
                try {
                    item.PercentualDesconto = Convert.ToDecimal(workSheet.Cell(linha, coluna10).Value);
                }
                catch (Exception ex) {
                    throw new Exception(ex.Message + " - PercentualDesconto: linha " + (linha + 1));
                }
                
                if (workSheet.Cell(linha, coluna11).Value != null) {
                    try {
                        item.Trader = workSheet.Cell(linha, coluna11).ValueAsString;
                    }
                    catch (Exception ex) {
                        throw new Exception(ex.Message + " - Trader: linha " + (linha + 1));
                    }
                }
                
                if (workSheet.Cell(linha, coluna12).Value != null) {
                    try {
                        item.CalculaDespesas = workSheet.Cell(linha, coluna12).ValueAsString;
                    }
                    catch (Exception ex) {
                        throw new Exception(ex.Message + " - CalculaDespesas: linha " + (linha + 1));
                    }
                }

                item.Taxa = 0;
                if (workSheet.Cell(linha, coluna13).Value != null && workSheet.Cell(linha, coluna13).ValueAsString != "") {
                    try {
                        item.Taxa = Convert.ToDecimal(workSheet.Cell(linha, coluna13).Value);
                    }
                    catch (Exception ex) {
                        throw new Exception(ex.Message + " - Taxa: linha " + (linha + 1));
                    }
                }

                item.TipoEstrategia = 0;
                if (workSheet.Cell(linha, coluna14).Value != null) {
                    try {
                        item.TipoEstrategia = workSheet.Cell(linha, coluna14).ValueAsInteger;
                    }
                    catch (Exception ex) {
                        throw new Exception(ex.Message + " - TipoEstrategia: linha " + (linha + 1));
                    }
                }

                

                #region Local Negociação
                if (workSheet.Cell(linha, coluna15).Value == null)
                {
                    item.LocalNegociacao = null;
                }
                else
                {
                    try
                    {
                        item.LocalNegociacao = workSheet.Cell(linha, coluna15).ValueAsString;
                    }
                    catch (Exception ex)
                    {
                        throw new Exception(ex.Message + " - LocalNegociacao: linha " + (linha + 1));
                    }
                }
                #endregion

                #region Clearing
                if (workSheet.Cell(linha, coluna16).Value == null)
                {
                    item.Clearing = null;
                }
                else
                {
                    try
                    {
                        item.Clearing = workSheet.Cell(linha, coluna16).ValueAsString;
                    }
                    catch (Exception ex)
                    {
                        throw new Exception(ex.Message + " - Clearing: linha " + (linha + 1));
                    }
                }
                #endregion

                #region Local Custódia
                if (workSheet.Cell(linha, coluna17).Value == null)
                {
                    item.LocalCustodia = null;
                }
                else
                {
                    try
                    {
                        item.LocalCustodia = workSheet.Cell(linha, coluna17).ValueAsString;
                    }
                    catch (Exception ex)
                    {
                        throw new Exception(ex.Message + " - LocalCustodia: linha " + (linha + 1));
                    }
                }
                #endregion

                #region AgenteCustodia_Liquidacao
                if (workSheet.Cell(linha, coluna18).Value == null)
                {
                    item.AgenteCustodia = null;
                }
                else
                {
                    try
                    {
                        item.AgenteCustodia = workSheet.Cell(linha, coluna18).ValueAsString;
                    }
                    catch (Exception ex)
                    {
                        throw new Exception(ex.Message + " - Corretora: linha " + (linha + 1));
                    }
                }
                #endregion

                if (workSheet.Cell(linha, coluna19).Value != null)
                {
                    try
                    {
                        item.Emolumentos = Convert.ToDecimal(workSheet.Cell(linha, coluna19).Value);
                    }
                    catch (Exception ex)
                    {
                        throw new Exception(ex.Message + " - Emolumentos: linha " + (linha + 1));
                    }
                }

                if (workSheet.Cell(linha, coluna20).Value != null)
                {
                    try
                    {
                        item.TaxaRegistro = Convert.ToDecimal(workSheet.Cell(linha, coluna20).Value);
                    }
                    catch (Exception ex)
                    {
                        throw new Exception(ex.Message + " - TaxaRegistro: linha " + (linha + 1));
                    }
                }

                if (workSheet.Cell(linha, coluna21).Value != null)
                {
                    try
                    {
                        item.ValorCorretagem = Convert.ToDecimal(workSheet.Cell(linha, coluna21).Value);
                    }
                    catch (Exception ex)
                    {
                        throw new Exception(ex.Message + " - ValorCorretagem: linha " + (linha + 1));
                    }
                }

                if (workSheet.Cell(linha, coluna22).Value != null)
                {
                    try
                    {
                        item.TaxaClearing = Convert.ToDecimal(workSheet.Cell(linha, coluna22).Value);
                    }
                    catch (Exception ex)
                    {
                        throw new Exception(ex.Message + " - TaxaClearing: linha " + (linha + 1));
                    }
                }

                item.DataOperacao = item.DataOrdem;

                this.valoresExcelOrdemBMF.Add(item);
                //
                index++;
                linha = 1 + index;
                //     
                Console.WriteLine("{0},{1},{2},{3},{4},{5},{6},{7},{8},{9},{10},{11},{12},{13},{14},{15},{16},{17},{18},{19},{20},{21}",
                    item.IdCliente, item.CdAtivoBMF, item.Serie, item.CodigoBovespa, item.TipoOrdem, item.DataOrdem.ToString("dd/MM/yyyy"),
                    item.Pu, item.Valor, item.Quantidade, item.PercentualDesconto, item.Trader, item.CalculaDespesas, item.Taxa,
                    item.TipoEstrategia, item.LocalNegociacao, item.Clearing, item.LocalCustodia, item.AgenteCustodia,
                    item.Emolumentos, item.TaxaRegistro, item.ValorCorretagem, item.TaxaClearing);
            }
        }
        catch (Exception ex) {
            document.Close();
            document.Dispose();

            throw new Exception(ex.Message);
        }
        #endregion

        document.Close();
        document.Dispose();
    }

    /// <summary>
    /// Gera e Carrega as Ordens BMF de Acordo com o objeto List<ValoresExcelOrdemBMF>
    /// </summary>
    /// <param name="ignoraCarteiraInexistente">ignora cliente e carteira inexistentes</param>     
    private void CarregaOrdemBMF(bool ignoraCarteiraInexistente) {
        OrdemBMFCollection ordemBMFCollection = new OrdemBMFCollection();
        //        
        for (int i = 0; i < this.valoresExcelOrdemBMF.Count; i++) {
            ValoresExcelOrdemBMF valoresExcel = this.valoresExcelOrdemBMF[i];
            //                    
            #region Verifica se Existe Cliente
            Cliente cliente = new Cliente();
            List<esQueryItem> campos = new List<esQueryItem>();
            campos.Add(cliente.Query.IdCliente);
            campos.Add(cliente.Query.Status);
            campos.Add(cliente.Query.DataDia);
           
            if (!cliente.LoadByPrimaryKey(campos, valoresExcel.IdCliente)) {
                if (!cliente.LoadByPrimaryKey(campos, valoresExcel.IdCliente)) {
                    if (ignoraCarteiraInexistente) {
                        continue;
                    }
                    else {
                        throw new Exception("Cliente Não existente: " + valoresExcel.IdCliente);
                    }
                }
            }
            #endregion

            #region Verifica se Existe AtivoBMF
            AtivoBMF ativoBMF = new AtivoBMF();
            if (!ativoBMF.LoadByPrimaryKey(valoresExcel.CdAtivoBMF.Trim(), valoresExcel.Serie.Trim())) {
                throw new Exception("AtivoBMF Não existente: " + valoresExcel.CdAtivoBMF.Trim() + " " + valoresExcel.Serie.Trim());
            }
            #endregion

            #region Testa AgenteMercado

            AgenteMercado agenteMercado = new AgenteMercado();
            int? idAgenteMercado = null;
            try {
                idAgenteMercado = (int)agenteMercado.BuscaIdAgenteMercadoBovespa(valoresExcel.CodigoBovespa);
            }
            catch (IdAgenteNaoCadastradoException e) {
                throw new IdAgenteNaoCadastradoException(e.Message);
            }
            #endregion

            #region Testa AgenteMercado Liquidante ( caso tenha )
            int? idAgenteMercadoLiquidante = null;
            if (!string.IsNullOrEmpty(valoresExcel.AgenteCustodia))
            {
                AgenteMercado agenteMercadoLiquidante = new AgenteMercado();
                try
                {
                    idAgenteMercadoLiquidante = (int)agenteMercadoLiquidante.BuscaIdAgenteMercadoBovespa(Convert.ToInt32(valoresExcel.AgenteCustodia));
                }
                catch (IdAgenteNaoCadastradoException e)
                {
                    throw new IdAgenteNaoCadastradoException(e.Message);
                }
            }
            #endregion

            if (idAgenteMercado.HasValue) {

                #region Verifica DataDia
                if (valoresExcel.DataOrdem < cliente.DataDia.Value) 
                {
                    throw new Exception("Cliente: " + cliente.IdCliente.Value + " - Data da Ordem: " + valoresExcel.DataOrdem.ToString("dd/MM/yyyy") + " menor que a data dia do Cliente: " + cliente.DataDia.Value.ToString("dd/MM/yyyy"));
                }

                // Se Status está Fechado
                if (cliente.Status == (byte)StatusCliente.Divulgado) 
                {
                    // Pode importar somente depois da DataDia
                    if (valoresExcel.DataOrdem == cliente.DataDia.Value) 
                    {
                        throw new Exception("Cliente: " + cliente.IdCliente.Value + " - Para Clientes Com Status Fechado, Data da Ordem: " + valoresExcel.DataOrdem.ToString("dd/MM/yyyy") + " deve ser maior que data dia do Cliente: " + cliente.DataDia.Value.ToString("dd/MM/yyyy"));
                    }
                }
                #endregion

                #region Verifica se TipoOrdem é Consistente
                if (valoresExcel.TipoOrdem.ToString().ToUpper() != TipoOrdemBMF.Compra &&
                    valoresExcel.TipoOrdem.ToString().ToUpper() != TipoOrdemBMF.Venda) {
                    throw new Exception("TipoOrdem deve ser C=Compra ou V=Venda");
                }
                #endregion

                #region Verifica se Existe Trader
                Trader trader = new Trader();
                if (!String.IsNullOrEmpty(valoresExcel.Trader)) {
                    trader.Query.Select(trader.Query.IdTrader)
                                 .Where(trader.Query.Nome == valoresExcel.Trader.Trim());
                    //
                    if (!trader.Query.Load()) {
                        throw new Exception("Trader Não existente: " + valoresExcel.Trader.Trim());
                    }
                }
                #endregion

                #region Confere Ativo Vencido
                if (ativoBMF.DataVencimento.HasValue && ativoBMF.DataVencimento < Convert.ToDateTime(valoresExcel.DataOrdem)) {
                    throw new Exception("Ativo: " + ativoBMF.CdAtivoBMF.Trim().ToUpper() + " " + ativoBMF.Serie.Trim().ToUpper() + " Vencido em: " + ativoBMF.DataVencimento.Value.ToString("d"));
                }
                #endregion

                #region OrdemBMF
                OrdemBMF ordemBMF = ordemBMFCollection.AddNew();

                // Dados do Arquivo
                ordemBMF.IdCliente = valoresExcel.IdCliente;
                ordemBMF.CdAtivoBMF = valoresExcel.CdAtivoBMF.Trim().ToUpper();
                ordemBMF.Serie = valoresExcel.Serie.Trim().ToUpper();
                ordemBMF.IdAgenteLiquidacao = (idAgenteMercadoLiquidante.HasValue ? idAgenteMercadoLiquidante.Value : idAgenteMercado.Value);
                ordemBMF.IdAgenteCorretora = idAgenteMercado.Value;
                ordemBMF.TipoMercado = ativoBMF.TipoMercado.Value;
                ordemBMF.TipoOrdem = valoresExcel.TipoOrdem.ToString().ToUpper();
                ordemBMF.Data = valoresExcel.DataOrdem;
                ordemBMF.Taxa = valoresExcel.Taxa;
                ordemBMF.PontaEstrategia = (short)valoresExcel.TipoEstrategia;

                if (ativoBMF.TipoMercado == (byte)TipoMercadoBMF.Futuro && ordemBMF.Taxa.Value != 0 &&
                                    (ativoBMF.CdAtivoBMF == "DI1" || ativoBMF.CdAtivoBMF == "DDI")) {

                    if (ativoBMF.CdAtivoBMF == "DI1") {
                        AtivoBMF ativoBMFPrazo = new AtivoBMF();
                        int numeroDiasVencimento = ativoBMFPrazo.RetornaPrazoDiasUteisBrasil(ordemBMF.CdAtivoBMF, ordemBMF.Serie, ordemBMF.Data.Value);
                        decimal fator = 1M + (ordemBMF.Taxa.Value / 100M);
                        decimal expoente = numeroDiasVencimento / 252M;
                        decimal fatorDesconto = (decimal)Math.Pow((double)fator, (double)expoente);
                        decimal puCalculado = Math.Round(100000M / fatorDesconto, 2);
                        ordemBMF.Pu = puCalculado;
                    }
                    else if (ativoBMF.CdAtivoBMF == "DDI") {
                        AtivoBMF ativoBMFPrazo = new AtivoBMF();
                        int numeroDiasVencimento = ativoBMFPrazo.RetornaPrazoDiasCorridos(ordemBMF.CdAtivoBMF, ordemBMF.Serie, ordemBMF.Data.Value);

                        decimal puCalculado = Math.Round(100000M / (1M + (ordemBMF.Taxa.Value * numeroDiasVencimento / 36000M)), 2);
                        ordemBMF.Pu = puCalculado;
                    }

                    ordemBMF.TipoOrdem = ordemBMF.TipoOrdem == TipoOrdemBMF.Compra ? TipoOrdemBMF.Venda : TipoOrdemBMF.Compra;
                }
                else {
                    ordemBMF.Pu = valoresExcel.Pu;
                }

                ordemBMF.Valor = valoresExcel.Valor;

                if (valoresExcel.Quantidade < 0)
                {
                    throw new Exception("Quantidade " + valoresExcel.Quantidade.ToString() + " não pode ser negativa.");
                }

                ordemBMF.Quantidade = (int)valoresExcel.Quantidade;
                ordemBMF.PercentualDesconto = valoresExcel.PercentualDesconto;

                if (!String.IsNullOrEmpty(valoresExcel.Trader)) {
                    ordemBMF.IdTrader = trader.IdTrader.Value;
                }

                ordemBMF.CalculaDespesas = !String.IsNullOrEmpty(valoresExcel.CalculaDespesas)
                         ? valoresExcel.CalculaDespesas.Trim().ToUpper() : "N";

                ordemBMF.IdLocalNegociacao = (int)LocalNegociacaoFixo.BOVESPA;
                if (!string.IsNullOrEmpty(valoresExcel.LocalNegociacao))
                {
                    LocalNegociacao localNegociacao = new LocalNegociacao();
                    localNegociacao.Query.Where(localNegociacao.Query.Codigo.ToUpper().Equal(valoresExcel.LocalNegociacao.ToUpper()));

                    if (localNegociacao.Query.Load())
                        ordemBMF.IdLocalNegociacao = localNegociacao.IdLocalNegociacao.Value;
                    else
                        throw new Exception("Local de Negociação não cadastrado: " + valoresExcel.LocalNegociacao);
                }

                ordemBMF.IdLocalCustodia = (int)LocalCustodiaFixo.CBLC;
                if (!string.IsNullOrEmpty(valoresExcel.LocalCustodia))
                {
                    LocalCustodia localCustodia = new LocalCustodia();
                    localCustodia.Query.Where(localCustodia.Query.Codigo.ToUpper().Equal(valoresExcel.LocalCustodia.ToUpper()));

                    if (localCustodia.Query.Load())
                        ordemBMF.IdLocalCustodia = localCustodia.IdLocalCustodia.Value;
                    else
                        throw new Exception("Local de Custódia não cadastrado: " + valoresExcel.LocalCustodia);
                }

                ordemBMF.IdClearing = (int)ClearingFixo.CBLC;
                if (!string.IsNullOrEmpty(valoresExcel.Clearing))
                {
                    Clearing clearing = new Clearing();
                    clearing.Query.Where(clearing.Query.Codigo.ToUpper().Equal(valoresExcel.Clearing.ToUpper()));

                    if (clearing.Query.Load())
                        ordemBMF.IdClearing = clearing.IdClearing.Value;
                    else
                        throw new Exception("Clearing não cadastrada: " + valoresExcel.Clearing);
                }

                ordemBMF.DataOperacao = valoresExcel.DataOperacao;

                // Valores Forçados
                ordemBMF.Corretagem = 0;
                ordemBMF.Emolumento = 0;
                ordemBMF.Registro = 0;
                ordemBMF.CustoWtr = 0;

                ordemBMF.Origem = (byte)OrigemOrdemBMF.Primaria;
                ordemBMF.Fonte = (byte)FonteOrdemBMF.Manual;
                ordemBMF.QuantidadeDayTrade = 0;
                ordemBMF.IdMoeda = (int)ativoBMF.IdMoeda.Value;

                ordemBMF.Emolumento = valoresExcel.Emolumentos;
                ordemBMF.Registro = valoresExcel.TaxaRegistro;
                ordemBMF.Corretagem = valoresExcel.ValorCorretagem;
                ordemBMF.TaxaClearingVlFixo = valoresExcel.TaxaClearing;
                //
                #endregion
            }
        }

        // Salva Ordens Presentes no Excel
        ordemBMFCollection.Save();
    }

    /// <summary>
    /// Processa a Planilha de OrdemBMF após ter acabado o Upload
    /// </summary>
    /// <param name="e"></param>
    /// <param name="checkIgnora">Se Checado ignora os erros de importação de codigos inválidos </param>
    protected void uplOrdemBMF_FileUploadComplete(FileUploadCompleteEventArgs e, ASPxCheckBox checkIgnora) {
        e.CallbackData = "";

        #region Trata Extensão Válida
        if (!ValoresExcelUtil.isExtensaoExcel(e.UploadedFile.FileName.Trim())) {
            e.CallbackData = "Importação Ordem BMF - Formato de Arquivo Inválido. Formato permitido: .xls ou .xlsx\n\n";
            return;
        }
        #endregion

        // Stream de bytes com o Conteudo do Arquivo Excel
        Stream sr = e.UploadedFile.FileContent;

        #region Processamento Arquivo Excel

        //Reseta Vetor de Dados
        this.valoresExcelOrdemBMF = new List<ValoresExcelOrdemBMF>();
        //

        try {
            // Ler Arquivo
            this.LerArquivoOrdemBMF(sr);
            // Carrega Arquivo
            this.CarregaOrdemBMF(checkIgnora.Checked);
        }
        catch (Exception e2) {
            e.CallbackData = "Importação Ordem BMF - " + e2.Message;
            return;
        }
        #endregion

        new HistoricoLog().InsereHistoricoLog(DateTime.Now, DateTime.Now,
            "Importação Planilha Ordem BMF",
            HttpContext.Current.User.Identity.Name, UtilitarioWeb.GetIP(Request), "", HistoricoLogOrigem.Outros);

    }
}
