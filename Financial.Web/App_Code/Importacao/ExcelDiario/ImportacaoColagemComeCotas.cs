﻿using System;
using System.Collections.Generic;
using System.IO;
using Financial.Integracao.Excel;
using Financial.Integracao.Excel.Util;
using Financial.RendaFixa;
using Financial.Web.Common;
using EntitySpaces.Interfaces;
using DevExpress.Web;
using Financial.Security;
using System.Web;
using Financial.Web.Util;
using Financial.Security.Enums;
using Financial.Util;
using Financial.Common.Enums;
using Financial.Fundo;
using Financial.Investidor;
using Financial.InvestidorCotista;

public partial class ImportacaoBasePage : BasePage
{
    /* Estrutura do Excel */
    private List<ValoresExcelColagemComeCotas> valoresExcelColagemComeCotas = new List<ValoresExcelColagemComeCotas>();

    private List<int> idClienteDeletarColagemComeCotasDetalhe = new List<int>();
    private List<int> idCarteiraDeletarColagemComeCotasDetalhe = new List<int>();
    private List<DateTime> dataReferenciaDeletarColagemComeCotasDetalhe = new List<DateTime>();
    /// <summary>
    /// Leitura de um arquivo Excel
    /// Saida: List<ValoresExcel> com os valores armazenados no arquivo
    /// </summary>
    /// <param name="streamExcel">Stream de bytes de um arquivo Excel</param>
    /// <exception cref="Exception">Se ocorreu problemas</exception>
    private void LerArquivoColagemComeCotas(Stream streamExcel)
    {

        #region Leitura do Arquivo Excel

        // Open Spreadsheet
        Bytescout.Spreadsheet.Spreadsheet document = new Bytescout.Spreadsheet.Spreadsheet();
        document.LoadFromStream(streamExcel);

        Bytescout.Spreadsheet.Worksheet workSheet = document.Workbook.Worksheets[0];

        //
        #region Confere Formato Arquivo
        string[] colunasConferencias = new string[] { "TipoRegistro", "CodigoExterno", "DataReferencia", "IdCliente", "IdCarteira", "DataConversaoCautela", "Quantidade", "CotaOperacao", "ValorBruto", "ValorLiquido", "ValorCPMF", "ValorPerformance", "PrejuizoUsado", "RendimentoComeCotas", "ValorIR", "ValorIOF" };

        bool formato = ValoresExcelUtil.isExcelFormatoValido(colunasConferencias, workSheet);

        if (!formato)
        {
            string mensagem = "Formato Interno do Arquivo Inválido.\n";
            mensagem += "Linha 1 do Arquivo deve ser: \n";
            for (int i = 0; i < colunasConferencias.Length; i++)
            {
                int coluna = i + 1;
                mensagem += "\tColuna " + coluna + ": " + colunasConferencias[i] + "\n";
            }

            document.Close();
            document.Dispose();

            throw new Exception(mensagem);
        }
        #endregion

        //
        int index = 0;
        // This row,column index should be changed as per your need.
        // i.e. which cell in the excel you are interesting to read.
        //
        /* Formato: -0)TipoRegistro     -1)CodigoExterno        -2)DataReferencia 
         *          -3)IdCliente        -4)IdCarteira           -5)DataConversaoCautela 
         *          -6)Quantidade       -7)CotaOperacao         -8)ValorBruto 
         *          -9)ValorLiquido     -10)ValorCPMF           -11)ValorPerformance  
         *          -12)PrejuizoUsado   -13)RendimentoComeCotas -14)ValorIR 
         *          -15)ValorIOF  */

        int linha = 1;
        int coluna0 = 0,    coluna1 = 1,    coluna2 = 2, 
            coluna3 = 3,    coluna4 = 4,    coluna5 = 5, 
            coluna6 = 6,    coluna7 = 7,    coluna8 = 8, 
            coluna9 = 9,    coluna10 = 10, coluna11 = 11, 
            coluna12 = 12,  coluna13 = 13, coluna14 = 14, 
            coluna15 = 15;

        //            
        try
        {
            // Enquanto TipoRegistro tiver valor
            while (workSheet.Cell(linha, coluna0).Value != null)
            {
                ValoresExcelColagemComeCotas item = new ValoresExcelColagemComeCotas();

                #region TipoRegistro
                if (workSheet.Cell(linha, coluna0).Value == null)
                    throw new Exception("TipoRegistro não informada: linha " + (linha + 1));

                try
                {
                    item.TipoRegistro = workSheet.Cell(linha, coluna0).ValueAsString;
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message + " - TipoRegistro: linha " + (linha + 1));
                }
                #endregion

                #region CodigoExterno
                if (workSheet.Cell(linha, coluna1).Value == null)
                    throw new Exception("CodigoExterno não informada: linha " + (linha + 1));

                try
                {
                    item.CodigoExterno = workSheet.Cell(linha, coluna1).ValueAsString;
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message + " - CodigoExterno: linha " + (linha + 1));
                }
                #endregion

                #region DataReferencia
                if (workSheet.Cell(linha, coluna2).Value == null)
                    throw new Exception("DataReferencia não informada: linha " + (linha + 1));

                try
                {
                    item.DataReferencia = workSheet.Cell(linha, coluna2).ValueAsDateTime;
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message + " - DataReferencia: linha " + (linha + 1));
                }
                #endregion

                #region IdCliente
                if (workSheet.Cell(linha, coluna3).Value == null)
                    throw new Exception("IdCliente não informado: linha " + (linha + 1));

                try
                {
                    item.IdCliente = workSheet.Cell(linha, coluna3).ValueAsInteger;
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message + " - IdCliente: linha " + (linha + 1));
                }
                #endregion

                #region IdCarteira
                if (workSheet.Cell(linha, coluna4).Value == null)
                    throw new Exception("IdCarteira não informado: linha " + (linha + 1));

                try
                {
                    item.IdCarteira = workSheet.Cell(linha, coluna4).ValueAsInteger;
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message + " - IdCarteira: linha " + (linha + 1));
                }
                #endregion

                #region DataConversao
                if (workSheet.Cell(linha, coluna5).Value == null)
                {
                    item.DataConversao = new DateTime();
                }
                else
                {
                    try
                    {
                        item.DataConversao = workSheet.Cell(linha, coluna5).ValueAsDateTime;
                    }
                    catch (Exception ex)
                    {
                        throw new Exception(ex.Message + " - DataConversaoCautela: linha " + (linha + 1));
                    }
                }
                #endregion

                #region Quantidade
                if (workSheet.Cell(linha, coluna6).Value == null)
                    throw new Exception("Quantidade não informada: linha " + (linha + 1));
                try
                {
                    item.Quantidade = Convert.ToDecimal(workSheet.Cell(linha, coluna6).Value);
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message + " - Quantidade: linha " + (linha + 1));
                }
                #endregion

                #region CotaOperacao
                if (workSheet.Cell(linha, coluna7).Value == null)
                {
                    item.CotaOperacao = 0;
                }
                else
                {
                    try
                    {
                        item.CotaOperacao = Convert.ToDecimal(workSheet.Cell(linha, coluna7).Value);
                    }
                    catch (Exception ex)
                    {
                        throw new Exception(ex.Message + " - CotaOperacao: linha " + (linha + 1));
                    }
                }
                #endregion

                #region ValorBruto
                if (workSheet.Cell(linha, coluna8).Value == null)                
                    throw new Exception("ValorBruto não informada: linha " + (linha + 1));
                try
                {
                    item.ValorBruto = Convert.ToDecimal(workSheet.Cell(linha, coluna8).Value);
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message + " - ValorBruto: linha " + (linha + 1));
                }                
                #endregion

                #region ValorLiquido
                if (workSheet.Cell(linha, coluna9).Value == null)
                    throw new Exception("ValorLiquido não informada: linha " + (linha + 1));
                try
                {
                    item.ValorLiquido = Convert.ToDecimal(workSheet.Cell(linha, coluna9).Value);
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message + " - ValorLiquido: linha " + (linha + 1));
                }
                #endregion

                #region ValorCPMF
                if (workSheet.Cell(linha, coluna10).Value == null)
                    item.ValorCPMF = 0;
                else
                {
                    try
                    {
                        item.ValorCPMF = Convert.ToDecimal(workSheet.Cell(linha, coluna10).Value);
                    }
                    catch (Exception ex)
                    {
                        throw new Exception(ex.Message + " - ValorCPMF: linha " + (linha + 1));
                    }
                }
                #endregion

                #region ValorPerformance
                if (workSheet.Cell(linha, coluna11).Value == null)
                    item.ValorPerformance = 0;
                else
                {
                    try
                    {
                        item.ValorPerformance = Convert.ToDecimal(workSheet.Cell(linha, coluna11).Value);
                    }
                    catch (Exception ex)
                    {
                        throw new Exception(ex.Message + " - ValorPerformance: linha " + (linha + 1));
                    }
                }
                #endregion

                #region PrejuizoUsado
                if (workSheet.Cell(linha, coluna12).Value == null)
                    item.PrejuizoUsado = 0;
                else
                {
                    try
                    {
                        item.PrejuizoUsado = Convert.ToDecimal(workSheet.Cell(linha, coluna12).Value);
                    }
                    catch (Exception ex)
                    {
                        throw new Exception(ex.Message + " - PrejuizoUsado: linha " + (linha + 1));
                    }
                }
                #endregion

                #region RendimentoComeCotas
                if (workSheet.Cell(linha, coluna13).Value == null)
                    item.RendimentoComeCotas = 0;
                else
                {
                    try
                    {
                        item.RendimentoComeCotas = Convert.ToDecimal(workSheet.Cell(linha, coluna13).Value);
                    }
                    catch (Exception ex)
                    {
                        throw new Exception(ex.Message + " - RendimentoComeCotas: linha " + (linha + 1));
                    }
                }
                #endregion

                #region ValorIR
                if (workSheet.Cell(linha, coluna14).Value == null)
                    throw new Exception("ValorIR não informada: linha " + (linha + 1));
                try
                {
                    item.ValorIR = Convert.ToDecimal(workSheet.Cell(linha, coluna14).Value);
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message + " - ValorIR: linha " + (linha + 1));
                }
                #endregion

                #region ValorIOF
                if (workSheet.Cell(linha, coluna15).Value == null)
                    throw new Exception("ValorIOF não informada: linha " + (linha + 1));
                try
                {
                    item.ValorIOF = Convert.ToDecimal(workSheet.Cell(linha, coluna15).Value);
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message + " - ValorIOF: linha " + (linha + 1));
                }
                #endregion

                this.valoresExcelColagemComeCotas.Add(item);

                index++;
                linha = 1 + index;


                Console.WriteLine("{0},{1},{2},{3},{4},{5},{6},{7},{8},{9},{10},{11},{12},{13},{14},{15}", item.TipoRegistro, item.CodigoExterno, item.DataReferencia, item.IdCliente, item.IdCarteira, item.DataConversao, item.Quantidade, item.CotaOperacao, item.ValorBruto, item.ValorLiquido, item.ValorCPMF, item.ValorPerformance, item.PrejuizoUsado, item.RendimentoComeCotas, item.ValorIR, item.ValorIOF);
            }
        }
        catch (Exception ex)
        {
            document.Close();
            document.Dispose();

            throw new Exception(ex.Message);
        }
        #endregion

        document.Close();
        document.Dispose();
    }

    /// <summary>
    /// Gera e Carrega as Operações de ComeCotas co de acordo com o objeto List<ValoresExcel>
    /// </summary>     
    private void CarregaColagemComeCotas()
    {
        ColagemComeCotasCollection colagemComeCotasCollection = new ColagemComeCotasCollection();
        ColagemComeCotasCollection colagemComeCotasDeletarCollection = new ColagemComeCotasCollection();
        ColagemComeCotasDetalheCollection colagemComeCotasDetalheCollection = new ColagemComeCotasDetalheCollection();
        ColagemComeCotasDetalheCollection colagemComeCotasDetalheDeletarCollection = new ColagemComeCotasDetalheCollection();
        //    

        List<string> lstChave = new List<string>();
        for (int i = 0; i < this.valoresExcelColagemComeCotas.Count; i++)
        {
            ValoresExcelColagemComeCotas valoresExcel = this.valoresExcelColagemComeCotas[i];

            string chave = valoresExcel.IdCarteira.ToString() + "|" + valoresExcel.IdCliente.ToString() + "|" + valoresExcel.DataReferencia.ToString("dd/MM/yyyy");
            // Chave para poder Deletar 
            if (!lstChave.Contains(chave))
            {
                this.idCarteiraDeletarColagemComeCotasDetalhe.Add(valoresExcel.IdCarteira);
                this.idClienteDeletarColagemComeCotasDetalhe.Add(valoresExcel.IdCliente);
                this.dataReferenciaDeletarColagemComeCotasDetalhe.Add(valoresExcel.DataReferencia);
                lstChave.Add(chave);
            }

            #region Verifica se Existe carteira
            Carteira carteira = new Carteira();
            if (!carteira.LoadByPrimaryKey(valoresExcel.IdCarteira))
            {
                throw new Exception("Carteira não existente: " + valoresExcel.IdCarteira);
            }
            #endregion

            #region Verifica se Existe cliente/cotista
            if (valoresExcel.TipoRegistro.Equals("A"))
            {
                Cliente cliente = new Cliente();
                if (!cliente.LoadByPrimaryKey(valoresExcel.IdCliente))
                {
                    throw new Exception("Cliente não existente: " + valoresExcel.IdCliente);
                }
            }
            else if (valoresExcel.TipoRegistro.Equals("P"))
            {
                Cotista cotista = new Cotista();
                if (!cotista.LoadByPrimaryKey(valoresExcel.IdCliente))
                {
                    throw new Exception("Cotista não existente: " + valoresExcel.IdCliente);
                }
            }
            #endregion

            #region ColagemComeCotas
            
            // Dados do Arquivo
            if (valoresExcel.TipoRegistro.Equals("D"))
            {
                ColagemComeCotasDetalhe colagemComeCotasDetalhe = colagemComeCotasDetalheCollection.AddNew();
                colagemComeCotasDetalhe.CodigoExterno = valoresExcel.CodigoExterno.Trim();
                colagemComeCotasDetalhe.CotaOperacao = valoresExcel.CotaOperacao;
                if (valoresExcel.DataConversao != new DateTime())
                    colagemComeCotasDetalhe.DataConversao = valoresExcel.DataConversao;
                else
                    colagemComeCotasDetalhe.DataConversao = valoresExcel.DataReferencia;
                colagemComeCotasDetalhe.DataReferencia = valoresExcel.DataReferencia;
                colagemComeCotasDetalhe.IdCarteira = valoresExcel.IdCarteira;
                colagemComeCotasDetalhe.IdCliente = valoresExcel.IdCliente;
                colagemComeCotasDetalhe.PrejuizoUsado = valoresExcel.PrejuizoUsado;
                colagemComeCotasDetalhe.Quantidade = valoresExcel.Quantidade;
                colagemComeCotasDetalhe.RendimentoComeCotas = valoresExcel.RendimentoComeCotas;
                colagemComeCotasDetalhe.ValorBruto = valoresExcel.ValorBruto;
                colagemComeCotasDetalhe.ValorCPMF = valoresExcel.ValorCPMF;
                colagemComeCotasDetalhe.ValorIOF = valoresExcel.ValorIOF;
                colagemComeCotasDetalhe.ValorIR = valoresExcel.ValorIR;
                colagemComeCotasDetalhe.ValorLiquido = valoresExcel.ValorLiquido;
                colagemComeCotasDetalhe.ValorPerformance = valoresExcel.ValorPerformance;
                colagemComeCotasDetalhe.VariacaoComeCotas = valoresExcel.RendimentoComeCotas;
            }
            else
            {
                ColagemComeCotas colagemComeCotas = colagemComeCotasCollection.AddNew();
                colagemComeCotas.CodigoExterno = valoresExcel.CodigoExterno.Trim();
                colagemComeCotas.TipoRegistro = valoresExcel.TipoRegistro;
                colagemComeCotas.CotaOperacao = valoresExcel.CotaOperacao;
                if (valoresExcel.DataConversao != new DateTime())
                    colagemComeCotas.DataConversao = valoresExcel.DataConversao;
                else
                    colagemComeCotas.DataConversao = valoresExcel.DataReferencia;
                colagemComeCotas.DataReferencia = valoresExcel.DataReferencia;
                colagemComeCotas.IdCarteira = valoresExcel.IdCarteira;
                colagemComeCotas.IdCliente = valoresExcel.IdCliente;
                colagemComeCotas.PrejuizoUsado = valoresExcel.PrejuizoUsado;
                colagemComeCotas.Quantidade = valoresExcel.Quantidade;
                colagemComeCotas.RendimentoComeCotas = valoresExcel.RendimentoComeCotas;
                colagemComeCotas.ValorBruto = valoresExcel.ValorBruto;
                colagemComeCotas.ValorCPMF = valoresExcel.ValorCPMF;
                colagemComeCotas.ValorIOF = valoresExcel.ValorIOF;
                colagemComeCotas.ValorIR = valoresExcel.ValorIR;
                colagemComeCotas.ValorLiquido = valoresExcel.ValorLiquido;
                colagemComeCotas.ValorPerformance = valoresExcel.ValorPerformance;
                colagemComeCotas.VariacaoComeCotas = valoresExcel.RendimentoComeCotas;
            }
            //
            #endregion


        }

        using (esTransactionScope scope = new esTransactionScope())
        {
            #region Deleta ColagemComeCotas por Codigo
            for (int i = 0; i < lstChave.Count; i++)
            {
                DateTime dataReferencia = this.dataReferenciaDeletarColagemComeCotasDetalhe[i];
                int idCarteira = Convert.ToInt32(this.idCarteiraDeletarColagemComeCotasDetalhe[i]);
                int idCliente = Convert.ToInt32(this.idClienteDeletarColagemComeCotasDetalhe[i]);

                colagemComeCotasDeletarCollection = new ColagemComeCotasCollection();
                colagemComeCotasDeletarCollection.Query.Where(colagemComeCotasDeletarCollection.Query.IdCarteira.Equal(idCarteira)
                                                              & colagemComeCotasDeletarCollection.Query.IdCliente.Equal(idCliente)
                                                              & colagemComeCotasDeletarCollection.Query.DataReferencia.Equal(dataReferencia));

                if (colagemComeCotasDeletarCollection.Query.Load())
                {
                    colagemComeCotasDeletarCollection.MarkAllAsDeleted();
                    colagemComeCotasDeletarCollection.Save();
                }

                //Detalhe
                colagemComeCotasDetalheDeletarCollection = new ColagemComeCotasDetalheCollection();
                colagemComeCotasDetalheDeletarCollection.Query.Where(colagemComeCotasDetalheDeletarCollection.Query.IdCarteira.Equal(idCarteira)
                                                                     & colagemComeCotasDetalheDeletarCollection.Query.IdCliente.Equal(idCliente)
                                                                     & colagemComeCotasDetalheDeletarCollection.Query.DataReferencia.Equal(dataReferencia));

                if (colagemComeCotasDetalheDeletarCollection.Query.Load())
                {

                    foreach (ColagemComeCotasDetalhe colagemDetalhe in colagemComeCotasDetalheDeletarCollection)

                    {
                        ColagemComeCotasDetalhePosicaoCollection colaComeCotasDtlPosColl = new ColagemComeCotasDetalhePosicaoCollection();
                        colaComeCotasDtlPosColl.Query.Where(colaComeCotasDtlPosColl.Query.IdColagemComeCotasDetalhe.Equal(colagemDetalhe.IdColagemComeCotasDetalhe));

                        if (colaComeCotasDtlPosColl.Query.Load())
                        {
                            colaComeCotasDtlPosColl.MarkAllAsDeleted();
                            colaComeCotasDtlPosColl.Save();
                        }

                    }
                    colagemComeCotasDetalheDeletarCollection.MarkAllAsDeleted();
                    colagemComeCotasDetalheDeletarCollection.Save();
                }
            }
            #endregion

            // Salva Cotações Índices presentes no Excel                
            colagemComeCotasCollection.Save();
            colagemComeCotasDetalheCollection.Save();
            scope.Complete();
        }
    }

    /// <summary>
    /// Processa a Planilha de CotacaoIndice após ter acabado o Upload
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void uplColagemComeCotas_FileUploadComplete(object sender, FileUploadCompleteEventArgs e)
    {
        e.CallbackData = "";

        #region Trata Extensão Válida
        if (!ValoresExcelUtil.isExtensaoExcel(e.UploadedFile.FileName.Trim()))
        {
            e.CallbackData = "Importação Colagem - ComeCotas - Formato de Arquivo Inválido. Formato permitido: .xls ou .xlsx\n\n";
            return;
        }
        #endregion

        // Stream de bytes com o Conteudo do Arquivo Excel
        Stream sr = e.UploadedFile.FileContent;

        #region Processamento Arquivo Excel

        //Reseta Vetor de Dados
        this.valoresExcelColagemComeCotas = new List<ValoresExcelColagemComeCotas>();
        this.idClienteDeletarColagemComeCotasDetalhe = new List<int>();
        this.idCarteiraDeletarColagemComeCotasDetalhe = new List<int>();
        this.dataReferenciaDeletarColagemComeCotasDetalhe = new List<DateTime>();
        //
        try
        {
            // Ler Arquivo
            this.LerArquivoColagemComeCotas(sr);
            // Carrega Arquivo
            this.CarregaColagemComeCotas();
        }
        catch (Exception e2)
        {
            e.CallbackData = "Importação Colagem ComeCotas- " + e2.Message;
            return;
        }

        new HistoricoLog().InsereHistoricoLog(DateTime.Now, DateTime.Now,
            "Importação Planilha Colagem ComeCotas",
            HttpContext.Current.User.Identity.Name, UtilitarioWeb.GetIP(Request), "", HistoricoLogOrigem.Outros);

        #endregion
    }
}
