﻿using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Financial.Web.Common;
using System.IO;
using Financial.Integracao.Excel.Util;
using Financial.Integracao.Excel;
using EntitySpaces.Interfaces;
using Financial.Common;
using Financial.Bolsa;
using Financial.Bolsa.Enums;
using Financial.Investidor;
using System.Collections.Generic;
using Financial.Investidor.Enums;
using DevExpress.Web;
using Financial.Common.Exceptions;
using Financial.Bolsa.Exceptions;
using Financial.ContaCorrente;
using Financial.ContaCorrente.Enums;
using Financial.Security;
using Financial.Web.Util;
using Financial.Security.Enums;
using System.Collections;
using DevExpress.Web;

public partial class ImportacaoBasePage : BasePage
{
    /* Estrutura do Excel */
    private List<ValoresExcelLiquidacao> valoresExcelLiquidacaoDiaria = new List<ValoresExcelLiquidacao>();

    /// <summary>
    /// Leitura de um arquivo Excel
    /// Saida: List<ValoresExcel> com os valores armazenados no arquivo
    /// </summary>
    /// <param name="streamExcel">Stream de bytes de um arquivo Excel</param>
    /// <exception cref="Exception">Se ocorreu problemas</exception>
    private void LerArquivoLiquidacaoDiaria(Stream streamExcel)
    {
        #region Leitura do Arquivo Excel
        // Open Spreadsheet
        Bytescout.Spreadsheet.Spreadsheet document = new Bytescout.Spreadsheet.Spreadsheet();
        document.LoadFromStream(streamExcel);

        Bytescout.Spreadsheet.Worksheet workSheet = document.Workbook.Worksheets[0];

        //
        #region Confere Formato Arquivo
        string[] colunasConferencias = new string[] { "IdCliente", "DataVencimento", "Descricao", "Valor", "IdConta" };

        bool formato = ValoresExcelUtil.isExcelFormatoValido(colunasConferencias, workSheet);

        if (!formato)
        {
            string mensagem = "Formato Interno do Arquivo Inválido.\n";
            mensagem += "Linha 1 do Arquivo deve ser: \n";
            for (int i = 0; i < colunasConferencias.Length; i++)
            {
                int coluna = i + 1;
                mensagem += "\tColuna " + coluna + ": " + colunasConferencias[i] + "\n";
            }

            document.Close();
            document.Dispose();

            throw new Exception(mensagem);
        }
        #endregion

        int index = 0;
        // This row,column index should be changed as per your need.
        // i.e. which cell in the excel you are interesting to read.
        //
        /* Formato: 1)IdCliente  - 2)DataVencimento  - 3)Descricao  - 4)Valor  - 5)IdConta
         */

        int linha = 1;
        int coluna1 = 0, coluna2 = 1, coluna3 = 2,
            coluna4 = 3, coluna5 = 4;

        try
        {
            // Enquanto IdCliente tiver valor
            while (workSheet.Cell(linha, coluna1).Value != null)
            {
                ValoresExcelLiquidacao item = new ValoresExcelLiquidacao();

                #region Cliente
                try
                {
                    item.IdCliente = workSheet.Cell(linha, coluna1).ValueAsInteger;
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message + " - IdCliente: linha " + (linha + 1));
                }
                #endregion

                #region Data de Vencimento
                if (workSheet.Cell(linha, coluna2).Value == null)
                    throw new Exception("DataVencimento não informada: linha " + (linha + 1));
                try
                {
                    item.DataVencimento = workSheet.Cell(linha, coluna2).ValueAsDateTime;
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message + " - DataVencimento: linha " + (linha + 1));
                }
                #endregion

                #region Descição
                if (workSheet.Cell(linha, coluna3).Value == null)
                    throw new Exception("Descricao não informada: linha " + (linha + 1));
                try
                {
                    item.Descricao = workSheet.Cell(linha, coluna3).Value.ToString();
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message + " - Descricao: linha " + (linha + 1));
                }
                #endregion

                #region Valor
                if (workSheet.Cell(linha, coluna4).Value == null)
                    throw new Exception("Valor não informado: linha " + (linha + 1));
                try
                {
                    item.Valor = Convert.ToDecimal(workSheet.Cell(linha, coluna4).Value);
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message + " - Valor: linha " + (linha + 1));
                }
                #endregion

                #region Id.Conta Corrente
                if (workSheet.Cell(linha, coluna5).Value != null)
                {
                    try
                    {
                        item.IdConta = Convert.ToInt32(workSheet.Cell(linha, coluna5).Value);
                    }
                    catch (Exception ex)
                    {
                        throw new Exception(ex.Message + " - Valor: linha " + (linha + 1));
                    }
                }
                else
                {
                    item.IdConta = null;
                }
                #endregion

                this.valoresExcelLiquidacaoDiaria.Add(item);
                //
                index++;
                linha = 1 + index;
                //                
                Console.WriteLine("{0},{1},{2},{3},{4}", item.IdCliente, item.DataVencimento, item.Descricao, item.Valor, item.IdConta);
            }
        }
        catch (Exception ex)
        {
            document.Close();
            document.Dispose();

            throw new Exception(ex.Message);
        }

        document.Close();
        document.Dispose();

        #endregion
    }

    /// <summary>
    /// 
    /// de acordo com o objeto List<ValoresExcel>
    /// </summary>
    /// <exception cref="Exception">Se ocorreu problemas</exception>
    private void CarregaLiquidacaoDiaria()
    {
        LiquidacaoCollection liquidacaoCollection = new LiquidacaoCollection();
        LiquidacaoAberturaCollection liquidacaoAberturaCollection = new LiquidacaoAberturaCollection();

        Financial.Investidor.ContaCorrente contaCorrente = new Financial.Investidor.ContaCorrente();
        Hashtable hsIdContaValido = new Hashtable();
        Hashtable hsCliente = new Hashtable();

        //    
        for (int i = 0; i < this.valoresExcelLiquidacaoDiaria.Count; i++)
        {
            ValoresExcelLiquidacao valoresExcel = this.valoresExcelLiquidacaoDiaria[i];
            //

            int idCliente = valoresExcel.IdCliente;
            Cliente cliente = new Cliente();
            List<esQueryItem> campos = new List<esQueryItem>();
            campos.Add(cliente.Query.IdCliente);
            campos.Add(cliente.Query.DataDia);

            if (hsCliente.Contains(idCliente))
                cliente = (Cliente)hsCliente[idCliente];
            else
            {
                if (!cliente.LoadByPrimaryKey(campos, valoresExcel.IdCliente))
                    throw new Exception("Cliente Não existente : " + valoresExcel.IdCliente);

                hsCliente.Add(idCliente, cliente);
            }

            if (cliente.DataDia.Value.Date > valoresExcel.DataVencimento.Date)
            {
                throw new Exception("A data de vencimento " + valoresExcel.DataVencimento.ToString("dd/MM/yyyy") + " deve ser maior ou igual que a data do cliente: Id:" + valoresExcel.IdCliente + " - Data:" + cliente.DataDia.Value.ToString("dd/MM/yyyy"));
            }


            if (valoresExcel.IdConta.HasValue && !hsIdContaValido.Contains(valoresExcel.IdConta.Value))
            {
                contaCorrente = new ContaCorrente();

                if (!contaCorrente.LoadByPrimaryKey(valoresExcel.IdConta.Value))
                    throw new Exception("Id.Conta não encontrada/cadastrada!! Id.Conta - " + valoresExcel.IdConta.Value);
                else
                {
                    if (idCliente != contaCorrente.IdPessoa.Value)
                        throw new Exception("Id.Conta não pertence a pessoa!! Id.Conta - " + valoresExcel.IdConta.Value + "; Pessoa - " + idCliente);

                    hsIdContaValido.Add(valoresExcel.IdConta.Value, valoresExcel.IdConta.Value);
                }
            }

            Liquidacao liquidacao = liquidacaoCollection.AddNew();
            LiquidacaoAbertura liquidacaoAbertura = liquidacaoAberturaCollection.AddNew();
            liquidacaoAbertura.IdCliente = liquidacao.IdCliente = idCliente;
            liquidacaoAbertura.DataHistorico = liquidacaoAbertura.DataLancamento = liquidacao.DataLancamento = cliente.DataDia.Value;
            liquidacaoAbertura.DataVencimento = liquidacao.DataVencimento = valoresExcel.DataVencimento;
            liquidacaoAbertura.Descricao = liquidacao.Descricao = valoresExcel.Descricao;
            liquidacaoAbertura.Valor = liquidacao.Valor = valoresExcel.Valor;
            liquidacaoAbertura.Situacao = liquidacao.Situacao = (byte)SituacaoLancamentoLiquidacao.Normal;
            liquidacaoAbertura.Origem = liquidacao.Origem = OrigemLancamentoLiquidacao.Outros;
            liquidacaoAbertura.Fonte = liquidacao.Fonte = (byte)FonteLancamentoLiquidacao.Manual;

            // Lança exceção se conta não encontrada
            if (valoresExcel.IdConta.HasValue)
                liquidacaoAbertura.IdConta = liquidacao.IdConta = valoresExcel.IdConta.Value;
            else
                liquidacaoAbertura.IdConta = liquidacao.IdConta = contaCorrente.RetornaContaDefault(valoresExcel.IdCliente);
            //
        }

        #region Salva as Liquidações
        using (esTransactionScope scope = new esTransactionScope())
        {
            // Salva as Liquidações presentes no Excel
            liquidacaoCollection.Save();

            for (int i = 0; i < liquidacaoAberturaCollection.Count; i++)            
                liquidacaoAberturaCollection[i].IdLiquidacao = liquidacaoCollection[i].IdLiquidacao;            

            liquidacaoAberturaCollection.Save();

            scope.Complete();
        }
        #endregion
    }

    /// <summary>
    /// Processa a Planilha de Liquidação após ter acabado o Upload
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void uplLiquidacaoDiaria_FileUploadComplete(object sender, FileUploadCompleteEventArgs e)
    {
        e.CallbackData = "";

        #region Trata Extensão Válida
        if (!ValoresExcelUtil.isExtensaoExcel(e.UploadedFile.FileName.Trim()))
        {
            e.CallbackData = "Importação Liquidação Diária - Formato de Arquivo Inválido. Formato permitido: .xls ou .xlsx\n\n";
            return;
        }
        #endregion

        // Stream de bytes com o Conteudo do Arquivo Excel
        Stream sr = e.UploadedFile.FileContent;

        #region Processamento Arquivo Excel

        try
        {
            // Ler Arquivo
            this.LerArquivoLiquidacaoDiaria(sr);
            // Carrega Arquivo
            this.CarregaLiquidacaoDiaria();
        }
        catch (Exception e2)
        {
            e.CallbackData = "Importação Liquidação Diária - " + e2.Message;
            return;
        }

        new HistoricoLog().InsereHistoricoLog(DateTime.Now, DateTime.Now,
            "Importação Planilha Liquidação Diária",
            HttpContext.Current.User.Identity.Name, UtilitarioWeb.GetIP(Request), "", HistoricoLogOrigem.Outros);

        #endregion
    }
}