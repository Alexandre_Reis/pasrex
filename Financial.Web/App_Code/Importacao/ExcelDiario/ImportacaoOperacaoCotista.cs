﻿using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Financial.Web.Common;
using System.IO;
using Financial.Integracao.Excel.Util;
using Financial.Integracao.Excel;
using EntitySpaces.Interfaces;
using Financial.Common;
using System.Collections.Generic;
using Financial.Investidor.Enums;
using DevExpress.Web;
using Financial.InvestidorCotista;
using Financial.Fundo;
using Financial.Investidor;
using Financial.InvestidorCotista.Enums;
using Financial.Util;
using Financial.Security;
using Financial.Web.Util;
using Financial.Security.Enums;
using Financial.Fundo.Enums;

public partial class ImportacaoBasePage : BasePage
{

    /* Estrutura do Excel */
    public List<ValoresExcelOperacaoCotista> valoresExcelOperacaoCotista = new List<ValoresExcelOperacaoCotista>();

    /// <summary>
    /// Leitura de um arquivo Excel
    /// Saida: List<ValoresExcel> com os valores armazenados no arquivo
    /// </summary>
    /// <param name="streamExcel">Stream de bytes de um arquivo Excel</param>
    /// <exception cref="Exception">Se ocorreu problemas</exception>
    private void LerArquivoOperacaoCotista(Stream streamExcel)
    {

        #region Leitura do Arquivo Excel
        // Open Spreadsheet
        Bytescout.Spreadsheet.Spreadsheet document = new Bytescout.Spreadsheet.Spreadsheet();
        document.LoadFromStream(streamExcel);

        Bytescout.Spreadsheet.Worksheet workSheet = document.Workbook.Worksheets[0];
        //
        //
        #region Confere Formato Arquivo
        string[] colunasConferencias = new string[] 
                                {"IdCotista", "IdCarteira", "DataOperacao", "DataConversao", "DataLiquidacao", 
                                 "TipoOperacao", "TipoResgate", "Quantidade", "CotaOperacao", 
                                 "ValorBruto", "ValorLiquido", "ValorIR", "ValorIOF",
                                 "ValorPerformance", "RendimentoResgate",
                                 "LocalNegociacao", "FieModalidade", "FieTabelaIr", 
                                 "DataAplicacaoCautelaResgate","CotaInformada","Trader"

                                };

        bool formato = ValoresExcelUtil.isExcelFormatoValido(colunasConferencias, workSheet);

        if (!formato)
        {
            string mensagem = "Formato Interno do Arquivo Inválido.\n";
            mensagem += "Linha 1 do Arquivo deve ser: \n";
            for (int i = 0; i < colunasConferencias.Length; i++)
            {
                int coluna = i + 1;
                mensagem += "\tColuna " + coluna + ": " + colunasConferencias[i] + "\n";
            }

            document.Close();
            document.Dispose();

            throw new Exception(mensagem);
        }
        #endregion

        int index = 0;
        // This row,column index should be changed as per your need.
        // i.e. which cell in the excel you are interesting to read.
        //
        /* Formato: 1)IdCotista  - 2)IdCarteira  - 3)DataOperacao  - 4)DataConversao
                    5)DataLiquidacao - 6)TipoOperacao - 7)TipoResgate - 8)Quantidade - 9)CotaOperacao
         *          10)ValorBruto - 11)ValorLiquido - 12)ValorIR - 13)ValorIOF - 14)ValorPerformance
         *          15)RendimentoResgate - 16)LocalNegociacao - 17)FieModalidade
         *          18)FieTabelaIr - 19)DataAplicacaoCautelaResgate - 20)CotaInformada - 21)DataRegistro
         */

        int linha = 1;
        int coluna1 = 0, coluna2 = 1, coluna3 = 2,
            coluna4 = 3, coluna5 = 4, coluna6 = 5,
            coluna7 = 6, coluna8 = 7, coluna9 = 8,
            coluna10 = 9, coluna11 = 10, coluna12 = 11,
            coluna13 = 12, coluna14 = 13, coluna15 = 14,
            coluna16 = 15, coluna17 = 16, coluna18 = 17,
            coluna19 = 18, coluna20 = 19, coluna21 = 20, 
            coluna22 = 21;


        //            
        try
        {
            // Enquanto idCotista tiver valor
            while (workSheet.Cell(linha, coluna1).Value != null)
            {
                ValoresExcelOperacaoCotista item = new ValoresExcelOperacaoCotista();

                try
                {
                    item.IdCotista = workSheet.Cell(linha, coluna1).ValueAsInteger;
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message + " - IdCotista: linha " + (linha + 1));
                }

                if (workSheet.Cell(linha, coluna2).Value == null)
                    throw new Exception("IdCarteira não informado: linha " + (linha + 1));
                try
                {
                    item.IdCarteira = workSheet.Cell(linha, coluna2).ValueAsInteger;
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message + " - IdCarteira: linha " + (linha + 1));
                }

                if (workSheet.Cell(linha, coluna3).Value == null)
                    throw new Exception("DataOperacao não informada: linha " + (linha + 1));
                try
                {
                    item.DataOperacao = workSheet.Cell(linha, coluna3).ValueAsDateTime;

                    if (!Calendario.IsDiaUtil(item.DataOperacao))
                        throw new Exception("Não é dia útil - DataOperacao: linha " + (linha + 1));

                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message + " - DataOperacao: linha " + (linha + 1));
                }

                if (workSheet.Cell(linha, coluna4).Value != null && workSheet.Cell(linha, coluna4).Value.ToString().Trim() != "")
                {
                    try
                    {
                        item.DataConversao = workSheet.Cell(linha, coluna4).ValueAsDateTime;
                    }
                    catch (Exception ex)
                    {
                        throw new Exception(ex.Message + " - DataConversao: linha " + (linha + 1));
                    }
                }

                if (workSheet.Cell(linha, coluna5).Value != null && workSheet.Cell(linha, coluna5).Value.ToString().Trim() != "")
                {
                    try
                    {
                        item.DataLiquidacao = workSheet.Cell(linha, coluna5).ValueAsDateTime;
                    }
                    catch (Exception ex)
                    {
                        throw new Exception(ex.Message + " - DataLiquidacao: linha " + (linha + 1));
                    }
                }

                if (workSheet.Cell(linha, coluna6).Value == null)
                    throw new Exception("TipoOperacao não informado: linha " + (linha + 1));
                try
                {
                    item.TipoOperacao = Convert.ToByte(workSheet.Cell(linha, coluna6).Value);
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message + " - TipoOperacao: linha " + (linha + 1));
                }

                if (workSheet.Cell(linha, coluna7).Value != null && workSheet.Cell(linha, coluna7).Value.ToString().Trim() != "")
                {
                    try
                    {
                        item.TipoResgate = Convert.ToByte(workSheet.Cell(linha, coluna7).Value);
                    }
                    catch (Exception ex)
                    {
                        throw new Exception(ex.Message + " - TipoResgate: linha " + (linha + 1));
                    }
                }
                else
                {
                    item.TipoResgate = 0;
                }

                if (workSheet.Cell(linha, coluna8).Value == null)
                    throw new Exception("Quantidade não informada: linha " + (linha + 1));
                try
                {
                    item.Quantidade = Convert.ToDecimal(workSheet.Cell(linha, coluna8).Value);
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message + " - Quantidade: linha " + (linha + 1));
                }

                try
                {
                    if (workSheet.Cell(linha, coluna9).Value == null)
                    {
                        item.CotaOperacao = 0;
                    }
                    else
                    {
                        item.CotaOperacao = Convert.ToDecimal(workSheet.Cell(linha, coluna9).Value);
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message + " - Cota Operacao: linha " + (linha + 1));
                }

                if (workSheet.Cell(linha, coluna10).Value == null)
                    throw new Exception("ValorBruto não informado: linha " + (linha + 1));
                try
                {
                    item.ValorBruto = Convert.ToDecimal(workSheet.Cell(linha, coluna10).Value);
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message + " - ValorBruto: linha " + (linha + 1));
                }

                if (workSheet.Cell(linha, coluna11).Value == null)
                    throw new Exception("ValorLiquido não informado: linha " + (linha + 1));
                try
                {
                    item.ValorLiquido = Convert.ToDecimal(workSheet.Cell(linha, coluna11).Value);
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message + " - ValorLiquido: linha " + (linha + 1));
                }

                if (workSheet.Cell(linha, coluna12).Value == null)
                    throw new Exception("ValorIR não informado: linha " + (linha + 1));
                try
                {
                    item.ValorIR = Convert.ToDecimal(workSheet.Cell(linha, coluna12).Value);
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message + " - ValorIR: linha " + (linha + 1));
                }

                if (workSheet.Cell(linha, coluna13).Value == null)
                    throw new Exception("ValorIOF não informado: linha " + (linha + 1));
                try
                {
                    item.ValorIOF = Convert.ToDecimal(workSheet.Cell(linha, coluna13).Value);
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message + " - ValorIOF: linha " + (linha + 1));
                }

                if (workSheet.Cell(linha, coluna14).Value == null)
                    throw new Exception("ValorPerformance não informado: linha " + (linha + 1));
                try
                {
                    item.ValorPerformance = Convert.ToDecimal(workSheet.Cell(linha, coluna14).Value);
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message + " - ValorPerformance: linha " + (linha + 1));
                }

                if (workSheet.Cell(linha, coluna15).Value == null)
                    throw new Exception("RendimentoResgate não informado: linha " + (linha + 1));
                try
                {
                    item.RendimentoResgate = Convert.ToDecimal(workSheet.Cell(linha, coluna15).Value);
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message + " - RendimentoResgate: linha " + (linha + 1));
                }


                if (workSheet.Cell(linha, coluna16).Value == null)
                {
                    item.LocalNegociacao = null;
                }
                else
                {
                    try
                    {
                        item.LocalNegociacao = workSheet.Cell(linha, coluna16).ValueAsString;
                    }
                    catch (Exception ex)
                    {
                        throw new Exception(ex.Message + " - LocalNegociacao: linha " + (linha + 1));
                    }
                }

                Carteira carteira = new Carteira();
                if (carteira.LoadByPrimaryKey(item.IdCarteira))
                {
                    if (carteira.Fie.Equals("S"))
                    {
                        if (workSheet.Cell(linha, coluna17).Value != null && workSheet.Cell(linha, coluna17).Value.ToString().Trim() != "")
                        {
                            try
                            {
                                item.FieModalidade = Convert.ToInt32(workSheet.Cell(linha, coluna17).Value);
                            }
                            catch (Exception ex)
                            {
                                throw new Exception(ex.Message + " - FieModalidade: linha " + (linha + 1));
                            }
                        }
                        else
                        {
                            throw new Exception(" Para fundos de previdência, obrigatório informar modalidade: linha " + (linha + 1));
                        }

                        if (workSheet.Cell(linha, coluna18).Value != null && workSheet.Cell(linha, coluna18).Value.ToString().Trim() != "")
                        {
                            try
                            {
                                item.FieTabelaIr = Convert.ToInt32(workSheet.Cell(linha, coluna18).Value);
                            }
                            catch (Exception ex)
                            {
                                throw new Exception(ex.Message + " - FieTabelaIr: linha " + (linha + 1));
                            }
                        }
                        else
                        {
                            throw new Exception(" Para fundos de previdência, obrigatório informar tabela de IR: linha " + (linha + 1));
                        }
                    }
                }

                item.ObservacaoCotista = "";
                if (item.TipoResgate == 1)
                {

                    if (workSheet.Cell(linha, coluna19).Value == null)

                        throw new Exception("DataAplicacaoCautelaResgate não informada: linha " + (linha + 1));

                    try
                    {
                        item.DataAplicacaoCautelaResgate = workSheet.Cell(linha, coluna19).ValueAsDateTime;
                    }
                    catch (Exception ex)
                    {
                        throw new Exception(ex.Message + " - DataAplicacaoCautelaResgate: linha " + (linha + 1));
                    }
                }
                else
                {
                    item.DataAplicacaoCautelaResgate = null;
                }

                try
                {
                    if (workSheet.Cell(linha, coluna20).Value == null)
                    {
                        item.CotaInformada = null;
                    }
                    else
                    {
                        item.CotaInformada = Convert.ToDecimal(workSheet.Cell(linha, coluna20).Value);
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message + " - Cota Informada: linha " + (linha + 1));
                }

                if (workSheet.Cell(linha, coluna21).Value != null)
                {
                    try
                    {
                        item.DataRegistro = workSheet.Cell(linha, coluna21).ValueAsDateTime;
                    }
                    catch (Exception ex)
                    {
                        throw new Exception(ex.Message + " - DataRegistro: linha " + (linha + 1));
                    }
                }
                else
                {
                    item.DataRegistro = null;
                }

                if (workSheet.Cell(linha, coluna22).Value != null &&
                     workSheet.Cell(linha, coluna22).Value.ToString().Trim() != "")
                {
                    try
                    {
                        item.Trader = workSheet.Cell(linha, coluna22).ValueAsString;
                    }
                    catch (Exception ex)
                    {
                        throw new Exception(ex.Message + " - Trader: linha " + (linha + 1));
                    }
                }

                this.valoresExcelOperacaoCotista.Add(item);

                index++;
                linha = 1 + index;

                Console.WriteLine("{0},{1},{2},{3},{4},{5},{6},{7},{8},{9},{10},{11},{12},{13},{14},{15},{16},{17},{18},{19},{20},{21}",
                    item.IdCotista, item.IdCarteira, item.DataOperacao, item.DataConversao,
                    item.DataLiquidacao, item.TipoOperacao, item.TipoResgate, item.Quantidade,
                    item.CotaOperacao, item.ValorBruto, item.ValorLiquido, item.ValorIR, item.ValorIOF,
                    item.ValorPerformance, item.RendimentoResgate, item.IdOperacaoAuxiliar, item.LocalNegociacao,
                    item.FieModalidade, item.FieTabelaIr, item.DataAplicacaoCautelaResgate, item.CotaInformada, item.DataRegistro);

            }
        }
        catch (Exception ex)
        {
            document.Close();
            document.Dispose();

            throw new Exception(ex.Message);
        }
        #endregion

        document.Close();
        document.Dispose();
    }

    /// <summary>
    /// Gera e carrega as operações de Cotista de acordo com o objeto List<ValoresExcel>
    /// </summary>
    /// </summary>
    /// <param name="ignoraCarteiraInexistente">ignora cliente e carteira inexistentes</param>    
    public OperacaoCotistaCollection CarregaOperacaoCotista(bool ignoraCarteiraInexistente)
    {
        OperacaoCotistaCollection operacaoCotistaCollection = new OperacaoCotistaCollection();
        OperacaoCotistaCollection operacaoCotistaDeletarCollection = new OperacaoCotistaCollection();
        //    
        for (int i = 0; i < this.valoresExcelOperacaoCotista.Count; i++)
        {
            ValoresExcelOperacaoCotista valoresExcel = this.valoresExcelOperacaoCotista[i];
            //

            #region Verifica se Existe Carteira
            Carteira carteira = new Carteira();
            if (!carteira.LoadByPrimaryKey(valoresExcel.IdCarteira))
            {
                if (ignoraCarteiraInexistente)
                {
                    continue;
                }
                else
                {
                    Cliente clienteCodigo = new Cliente();

                    throw new Exception("Fundo Não existente : " + valoresExcel.IdCarteira);
                }
            }
            #endregion

            #region DataDia do Cliente
            Cliente cliente = new Cliente();
            List<esQueryItem> campos = new List<esQueryItem>();
            campos.Add(cliente.Query.IdCliente);
            campos.Add(cliente.Query.DataDia);
            if (!cliente.LoadByPrimaryKey(campos, valoresExcel.IdCarteira))
            {
                if (ignoraCarteiraInexistente)
                {
                    continue;
                }
                else
                {
                    throw new Exception("Fundo Não existente : " + valoresExcel.IdCarteira);
                }
            }

            DateTime dataDia = cliente.DataDia.Value;
            #endregion

            #region OperacaoCotista
            OperacaoCotista operacaoCotista = new OperacaoCotista();
            // Dados do Arquivo
            Cotista cotista = new Cotista();
            if (!cotista.LoadByPrimaryKey(valoresExcel.IdCotista))
            {
                continue;

                //if (ignoraCarteiraInexistente)
                //{

                //}
                //else
                //{
                //    throw new Exception("Cotista Não existente : " + valoresExcel.IdCotista);
                //}
            }

            if(carteira.CarteiraECotistaMesmaPessoa(carteira, cotista))
            {
                throw new Exception("Id.Carteira - " + carteira.IdCarteira.Value + " e Id.Cotista - " + cotista.IdCotista.Value + " são a mesma pessoa, importação abortada");
            }

            operacaoCotista.IdCotista = valoresExcel.IdCotista;

            operacaoCotista.IdCarteira = valoresExcel.IdCarteira;
            DateTime DataOperacaoDateAndTime = valoresExcel.DataOperacao;
            operacaoCotista.DataOperacao = valoresExcel.DataOperacao.Date;
            operacaoCotista.TipoOperacao = valoresExcel.TipoOperacao;

            if (valoresExcel.IdConta != null) operacaoCotista.IdConta = valoresExcel.IdConta;

            if (operacaoCotista.DataOperacao.Value >= dataDia)
            {
                //Para operações na data ou superiores à data dia, é possível informar apenas a data da operação
                carteira = new Carteira();
                campos = new List<esQueryItem>();
                campos.Add(carteira.Query.DiasCotizacaoAplicacao);
                campos.Add(carteira.Query.DiasCotizacaoResgate);
                campos.Add(carteira.Query.DiasLiquidacaoAplicacao);
                campos.Add(carteira.Query.DiasLiquidacaoResgate);
                campos.Add(carteira.Query.ContagemDiasConversaoResgate);
                carteira.LoadByPrimaryKey(campos, operacaoCotista.IdCarteira.Value);

                if (valoresExcel.DataConversao == null)
                {
                    DateTime dataConversao = new DateTime();
                    if (operacaoCotista.TipoOperacao.Value == (byte)TipoOperacaoCotista.Aplicacao ||
                        operacaoCotista.TipoOperacao.Value == (byte)TipoOperacaoCotista.AplicacaoAcoesEspecial)
                    {
                        dataConversao = Calendario.AdicionaDiaUtil(operacaoCotista.DataOperacao.Value, carteira.DiasCotizacaoAplicacao.Value);
                    }
                    else if (operacaoCotista.TipoOperacao.Value != (byte)TipoOperacaoCotista.AplicacaoCotasEspecial)
                    {
                        if (carteira.ContagemDiasConversaoResgate.Value == (byte)ContagemDiasLiquidacaoResgate.DiasUteis)
                        {
                            dataConversao = Calendario.AdicionaDiaUtil(operacaoCotista.DataOperacao.Value, carteira.DiasCotizacaoResgate.Value);
                        }
                        else
                        {
                            dataConversao = operacaoCotista.DataOperacao.Value.AddDays(carteira.DiasCotizacaoResgate.Value);

                            if (!Calendario.IsDiaUtil(dataConversao))
                            {
                                dataConversao = Calendario.AdicionaDiaUtil(dataConversao, 1);
                            }
                        }
                    }
                    operacaoCotista.DataConversao = dataConversao;
                }
                else
                {
                    operacaoCotista.DataConversao = valoresExcel.DataConversao.Value.Date;
                }

                if (valoresExcel.DataLiquidacao == null)
                {
                    DateTime dataLiquidacao = new DateTime();
                    if (operacaoCotista.TipoOperacao.Value == (byte)TipoOperacaoCotista.Aplicacao ||
                        operacaoCotista.TipoOperacao.Value == (byte)TipoOperacaoCotista.AplicacaoAcoesEspecial)
                    {
                        dataLiquidacao = Calendario.AdicionaDiaUtil(operacaoCotista.DataOperacao.Value, carteira.DiasLiquidacaoAplicacao.Value);
                    }
                    else if (operacaoCotista.TipoOperacao.Value != (byte)TipoOperacaoCotista.AplicacaoCotasEspecial)
                    {
                        if (carteira.ContagemDiasConversaoResgate.Value == (byte)ContagemDiasLiquidacaoResgate.DiasUteis)
                        {
                            dataLiquidacao = Calendario.AdicionaDiaUtil(operacaoCotista.DataOperacao.Value, carteira.DiasLiquidacaoResgate.Value);
                        }
                        else
                        {
                            //Conta por Dias Úteis em cima da data de conversão
                            dataLiquidacao = Calendario.AdicionaDiaUtil(operacaoCotista.DataConversao.Value, carteira.DiasLiquidacaoResgate.Value);
                        }
                    }
                    operacaoCotista.DataLiquidacao = dataLiquidacao;
                }
                else
                {
                    operacaoCotista.DataLiquidacao = valoresExcel.DataLiquidacao.Value.Date;
                }
            }
            else
            {
                if (valoresExcel.DataConversao == null || valoresExcel.DataLiquidacao == null)
                {
                    throw new Exception("Data da conversão e Data da liquidação devem ser informadas para Carteira " + operacaoCotista.IdCarteira.Value.ToString());
                }
                else
                {
                    operacaoCotista.DataConversao = valoresExcel.DataConversao.Value.Date;
                    operacaoCotista.DataLiquidacao = valoresExcel.DataLiquidacao.Value.Date;
                }
            }

            operacaoCotista.DataAgendamento = valoresExcel.DataAgendamento.HasValue ? valoresExcel.DataAgendamento : DataOperacaoDateAndTime;

            if (valoresExcel.TipoResgate != 0)
            {
                operacaoCotista.TipoResgate = valoresExcel.TipoResgate;
            }

            operacaoCotista.IdFormaLiquidacao = 1; //TODO ajustar para enum fixo depois
            operacaoCotista.Quantidade = valoresExcel.Quantidade;
            operacaoCotista.CotaOperacao = valoresExcel.CotaOperacao;
            operacaoCotista.ValorIR = valoresExcel.ValorIR;
            operacaoCotista.ValorIOF = valoresExcel.ValorIOF;
            operacaoCotista.ValorPerformance = valoresExcel.ValorPerformance;
            operacaoCotista.ValorBruto = valoresExcel.ValorBruto;
            operacaoCotista.ValorLiquido = valoresExcel.ValorLiquido;
            operacaoCotista.ValorCPMF = 0;
            operacaoCotista.PrejuizoUsado = 0; //Quase certo de não ser necessário mesmo!
            operacaoCotista.RendimentoResgate = valoresExcel.RendimentoResgate;
            operacaoCotista.VariacaoResgate = 0; //Quase certo de não ser necessário mesmo!
            operacaoCotista.Fonte = (byte)FonteOperacaoCotista.Manual;
            operacaoCotista.IdOperacaoAuxiliar = valoresExcel.IdOperacaoAuxiliar;
            operacaoCotista.IdPosicaoResgatada = valoresExcel.IdPosicaoResgatadaCotista;
            operacaoCotista.Observacao = valoresExcel.ObservacaoCotista;

            #region Verifica se Existe Trader
            Trader trader = new Trader();
            if (!String.IsNullOrEmpty(valoresExcel.Trader))
            {
                trader.Query.Select(trader.Query.IdTrader)
                             .Where(trader.Query.Nome == valoresExcel.Trader.Trim());
                //
                if (!trader.Query.Load())
                {
                    throw new Exception("Trader Não existente: " + valoresExcel.Trader.Trim());
                }
            }
            operacaoCotista.IdTrader = trader.IdTrader;
            #endregion

            if (valoresExcel.DataAplicacaoCautelaResgate.HasValue)
            {
                operacaoCotista.DataAplicCautelaResgatada = valoresExcel.DataAplicacaoCautelaResgate;

                if (!string.IsNullOrEmpty(operacaoCotista.Observacao))
                    operacaoCotista.Observacao += " / ";
                else
                    operacaoCotista.Observacao += string.Empty;

                operacaoCotista.Observacao += "Op de Resgate especificando a data de aplicação a ser impactada - Dt.Aplic = " + String.Format("{0:dd/MM/yyyy}", valoresExcel.DataAplicacaoCautelaResgate.Value);
            }

            operacaoCotista.IdLocalNegociacao = (int)Financial.Common.Enums.LocalNegociacaoFixo.Brasil;
            if (!string.IsNullOrEmpty(valoresExcel.LocalNegociacao))
            {
                LocalNegociacao localNegociacao = new LocalNegociacao();
                localNegociacao.Query.Where(localNegociacao.Query.Codigo.ToUpper().Equal(valoresExcel.LocalNegociacao.ToUpper()));

                if (localNegociacao.Query.Load())
                    operacaoCotista.IdLocalNegociacao = localNegociacao.IdLocalNegociacao.Value;
                else
                    throw new Exception("Local de Negociação não cadastrado: " + valoresExcel.LocalNegociacao);
            }

            operacaoCotista.FieModalidade = valoresExcel.FieModalidade;
            operacaoCotista.FieTabelaIr = valoresExcel.FieTabelaIr;
            operacaoCotista.DataRegistro = valoresExcel.DataRegistro.HasValue ? valoresExcel.DataRegistro.Value : dataDia;

            // Attach the object
            operacaoCotista.Save();
            operacaoCotistaCollection.AttachEntity(operacaoCotista);

            #endregion

            #region Faz uma inserção tb em OperacaoFundo se for o caso

            //Operacoes de aplic/resgate em carteira adm não pode entrar!
            if (operacaoCotista.IdCarteira.Value != operacaoCotista.IdCotista.Value && !operacaoCotista.TipoResgate.Equals((byte)TipoResgateCotista.Especifico))
            {


                if (cotista.IdClienteEspelho.HasValue)
                {

                    int idCliente = 0;
                    Cliente clienteFundo = new Cliente();
                    
                    if (clienteFundo.LoadByPrimaryKey(cotista.IdClienteEspelho.Value))
                    {
                        if (operacaoCotista.DataConversao.Value > clienteFundo.DataDia.Value ||
                           (operacaoCotista.DataConversao.Value == clienteFundo.DataDia.Value && clienteFundo.Status != (byte)StatusCliente.Divulgado))
                        {
                            idCliente = clienteFundo.IdCliente.Value;
                            OperacaoFundo operacaoFundo = new OperacaoFundo();
                            operacaoFundo.DataAgendamento = operacaoCotista.DataAgendamento.Value;
                            operacaoFundo.DataConversao = operacaoCotista.DataConversao.Value;
                            operacaoFundo.DataLiquidacao = operacaoCotista.DataLiquidacao.Value;
                            operacaoFundo.DataOperacao = operacaoCotista.DataOperacao.Value;
                            operacaoFundo.TipoOperacao = operacaoCotista.TipoOperacao.Value;
                            operacaoFundo.IdCarteira = operacaoCotista.IdCarteira.Value;
                            operacaoFundo.IdCliente = idCliente;
                            operacaoFundo.IdFormaLiquidacao = operacaoCotista.IdFormaLiquidacao.Value;
                            operacaoFundo.Observacao = "";
                            operacaoFundo.TipoResgate = operacaoCotista.TipoResgate;
                            operacaoFundo.ValorBruto = operacaoCotista.ValorBruto.Value;
                            operacaoFundo.ValorLiquido = operacaoCotista.ValorLiquido.Value;
                            operacaoFundo.Quantidade = operacaoCotista.Quantidade.Value;
                            //operacaoFundo.IdPosicaoResgatada = valoresExcel.IdPosicaoResgatadaFundo;
                            //operacaoFundo.Observacao = valoresExcel.ObservacaoFundo;

                            operacaoFundo.Fonte = (byte)FonteOperacaoFundo.Manual;
                            operacaoFundo.DataRegistro = operacaoCotista.DataRegistro.Value;

                            operacaoFundo.IdTrader = operacaoCotista.IdTrader;

                            operacaoFundo.IdLocalNegociacao = (int)Financial.Common.Enums.LocalNegociacaoFixo.Brasil;
                            if (!string.IsNullOrEmpty(valoresExcel.LocalNegociacao))
                            {
                                LocalNegociacao localNegociacao = new LocalNegociacao();
                                localNegociacao.Query.Where(localNegociacao.Query.Codigo.ToUpper().Equal(valoresExcel.LocalNegociacao.ToUpper()));

                                if (localNegociacao.Query.Load())
                                    operacaoFundo.IdLocalNegociacao = localNegociacao.IdLocalNegociacao.Value;
                                else
                                    throw new Exception("Local de Negociação não cadastrado: " + valoresExcel.LocalNegociacao);
                            }

                            operacaoFundo.Save();
                        }
                    }
                }
            }
            #endregion

        }

        // Salva Operações presentes no Excel
        return operacaoCotistaCollection;

    }

    /// <summary>
    /// Processa a Planilha de OperacaoCotista após ter acabado o Upload
    /// </summary>
    /// <param name="e"></param>
    /// <param name="checkIgnora">Se Checado ignora os erros de importação de codigos inválidos </param>
    protected void uplOperacaoCotista_FileUploadComplete(FileUploadCompleteEventArgs e, ASPxCheckBox checkIgnora)
    {
        e.CallbackData = "";

        #region Trata Extensão Válida
        if (!ValoresExcelUtil.isExtensaoExcel(e.UploadedFile.FileName.Trim()))
        {
            e.CallbackData = "Importação Operação Cotista - Formato de Arquivo Inválido. Formato permitido: .xls ou .xlsx\n\n";
            return;
        }
        #endregion

        // Stream de bytes com o Conteudo do Arquivo Excel
        Stream sr = e.UploadedFile.FileContent;

        #region Processamento Arquivo Excel

        //Reseta Vetor de Dados
        this.valoresExcelOperacaoCotista = new List<ValoresExcelOperacaoCotista>();
        //
        try
        {
            // Ler Arquivo
            this.LerArquivoOperacaoCotista(sr);
            // Carrega Arquivo
            this.CarregaOperacaoCotista(checkIgnora.Checked);
        }
        catch (Exception e2)
        {
            e.CallbackData = "Importação Operação Cotista - " + e2.Message;
            return;
        }

        new HistoricoLog().InsereHistoricoLog(DateTime.Now, DateTime.Now,
            "Importação Planilha Operação Cotista",
            HttpContext.Current.User.Identity.Name, UtilitarioWeb.GetIP(Request), "", HistoricoLogOrigem.Outros);

        #endregion
    }
}