﻿using System;
using System.Collections.Generic;
using System.IO;

using Financial.Integracao.Excel;
using Financial.Integracao.Excel.Util;
using Financial.Web.Common;
using Financial.BMF;

using EntitySpaces.Interfaces;

using DevExpress.Web;
using Financial.Security;
using System.Web;
using Financial.Web.Util;
using Financial.Security.Enums;

public partial class ImportacaoBasePage : BasePage {

    /* Estrutura do Excel */
    private List<ValoresExcelCotacaoBMF> valoresExcelCotacaoBMF = new List<ValoresExcelCotacaoBMF>();
    //
    List<DateTime> dataDeletarCotacaoBMF = new List<DateTime>();
    List<string> cdAtivoBMFDeletarCotacaoBMF = new List<string>();
    List<string> serieBMFDeletarCotacaoBMF = new List<string>();
    
    /// <summary>
    /// Leitura de um arquivo Excel
    /// Saida: List<ValoresExcel> com os valores armazenados no arquivo
    /// </summary>
    /// <param name="streamExcel">Stream de bytes de um arquivo Excel</param>
    /// <exception cref="Exception">Se ocorreu problemas</exception>
    private void LerArquivoCotacaoBMF(Stream streamExcel) {

        #region Leitura do Arquivo Excel

        // Open Spreadsheet
        Bytescout.Spreadsheet.Spreadsheet document = new Bytescout.Spreadsheet.Spreadsheet();
        document.LoadFromStream(streamExcel);

        Bytescout.Spreadsheet.Worksheet workSheet = document.Workbook.Worksheets[0];

        //
        #region Confere Formato Arquivo
        string[] colunasConferencias = new string[] { "Data", "CodigoAtivo", "Serie", "PU", "PUCorrigido" };

        bool formato = ValoresExcelUtil.isExcelFormatoValido(colunasConferencias, workSheet);

        if (!formato) {
            string mensagem = "Formato Interno do Arquivo Inválido.\n";
            mensagem += "Linha 1 do Arquivo deve ser: \n";
            for (int i = 0; i < colunasConferencias.Length; i++) {
                int coluna = i + 1;
                mensagem += "\tColuna " + coluna + ": " + colunasConferencias[i] + "\n";
            }

            document.Close();
            document.Dispose();

            throw new Exception(mensagem);
        }
        #endregion

        //
        int index = 0;
        // This row,column index should be changed as per your need.
        // i.e. which cell in the excel you are interesting to read.
        //
        /* Formato: 1)Data  - 2)CodigoAtivo  - 3)Serie  - 4)PU  - 5)PUCorrigido */

        int linha = 1;
        int coluna1 = 0, coluna2 = 1, coluna3 = 2, coluna4 = 3, coluna5 = 4;

        //            
        try 
        {
            // Enquanto Data tiver valor
            while (workSheet.Cell(linha, coluna1).Value != null) 
            {
                ValoresExcelCotacaoBMF item = new ValoresExcelCotacaoBMF();

                if (workSheet.Cell(linha, coluna1).Value == null)
                    throw new Exception("Data não informada: linha " + (linha + 1));
                try {
                    item.Data = workSheet.Cell(linha, coluna1).ValueAsDateTime;
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message + " - Data: linha " + (linha + 1));
                }

                if (workSheet.Cell(linha, coluna2).Value == null)
                    throw new Exception("CdAtivoBMF não informado: linha " + (linha + 1));
                try {
                    item.CdAtivoBMF = workSheet.Cell(linha, coluna2).ValueAsString.Trim().ToUpper();
                }

                catch (Exception ex)
                {
                    throw new Exception(ex.Message + " - CdAtivoBMF: linha " + (linha + 1));
                }

                if (workSheet.Cell(linha, coluna3).Value == null)
                    throw new Exception("Serie não informada: linha " + (linha + 1));
                try {
                    item.Serie = workSheet.Cell(linha, coluna3).ValueAsString.Trim().ToUpper();
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message + " - Serie: linha " + (linha + 1));
                }

                if (workSheet.Cell(linha, coluna4).Value == null)
                    throw new Exception("Pu não informado: linha " + (linha + 1));
                try {
                    item.Pu = Convert.ToDecimal(workSheet.Cell(linha, coluna4).Value);
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message + " - Pu: linha " + (linha + 1));
                }

                if (workSheet.Cell(linha, coluna5).Value == null)
                    throw new Exception("PuCorrigido não informado: linha " + (linha + 1));
                try
                {
                    item.PuCorrigido = Convert.ToDecimal(workSheet.Cell(linha, coluna5).Value);
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message + " - Pu: linha " + (linha + 1));
                }

                this.valoresExcelCotacaoBMF.Add(item);
                //
                index++;
                linha = 1 + index;
                //                
                Console.WriteLine("{0},{1},{2},{3}", item.Data, item.CdAtivoBMF, item.Serie, item.Pu);
            }
        }
        catch (Exception ex) {
            document.Close();
            document.Dispose();

            throw new Exception(ex.Message);
        }
        #endregion

        document.Close();
        document.Dispose();
    }

    /// <summary>
    /// Gera e Carrega as Cotações BMF de acordo com o objeto List<ValoresExcel>
    /// </summary>
    private void CarregaCotacaoBMF() {
        CotacaoBMFCollection cotacaoBMFCollection = new CotacaoBMFCollection();
        CotacaoBMFCollection cotacaoBMFDeletarCollection = new CotacaoBMFCollection();
        //    
        for (int i = 0; i < this.valoresExcelCotacaoBMF.Count; i++) {
            ValoresExcelCotacaoBMF valoresExcel = this.valoresExcelCotacaoBMF[i];
            //
            #region Verifica se Existe Ativo
            AtivoBMF ativoBMF = new AtivoBMF();
            if (!ativoBMF.LoadByPrimaryKey(valoresExcel.CdAtivoBMF, valoresExcel.Serie))
            {
                throw new Exception("Ativo Não existente: " + valoresExcel.CdAtivoBMF + valoresExcel.Serie + "\n Cadastre o Ativo.");
            }
            #endregion
            // Salva a Data e o Ativo para poder Deletar            
            this.dataDeletarCotacaoBMF.Add(valoresExcel.Data);
            this.cdAtivoBMFDeletarCotacaoBMF.Add(valoresExcel.CdAtivoBMF);
            this.serieBMFDeletarCotacaoBMF.Add(valoresExcel.Serie);

            #region CotacaoBMF
            CotacaoBMF cotacaoBMF = cotacaoBMFCollection.AddNew();
            // Dados do Arquivo
            cotacaoBMF.Data = valoresExcel.Data;
            cotacaoBMF.CdAtivoBMF = valoresExcel.CdAtivoBMF;
            cotacaoBMF.Serie = valoresExcel.Serie;
            cotacaoBMF.PUMedio = valoresExcel.Pu;
            cotacaoBMF.PUFechamento = valoresExcel.Pu;
            cotacaoBMF.PUCorrigido = valoresExcel.PuCorrigido;
            //
            #endregion
        }

        using (esTransactionScope scope = new esTransactionScope()) {

            #region Deleta CotacaoBMF por Data/CdAtivoBMF
            for (int i = 0; i < this.dataDeletarCotacaoBMF.Count; i++) {
                cotacaoBMFDeletarCollection = new CotacaoBMFCollection();

                cotacaoBMFDeletarCollection.Query
                .Where(cotacaoBMFDeletarCollection.Query.Data == this.dataDeletarCotacaoBMF[i],
                       cotacaoBMFDeletarCollection.Query.CdAtivoBMF == this.cdAtivoBMFDeletarCotacaoBMF[i],
                       cotacaoBMFDeletarCollection.Query.Serie == this.serieBMFDeletarCotacaoBMF[i]);

                cotacaoBMFDeletarCollection.Query.Load();
                cotacaoBMFDeletarCollection.MarkAllAsDeleted();
                cotacaoBMFDeletarCollection.Save();
            }
            #endregion

            // Salva Cotações Índices presentes no Excel                
            cotacaoBMFCollection.Save();

            scope.Complete();
        }
    }

    /// <summary>
    /// Processa a Planilha de CotacaoBMF após ter acabado o Upload
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void uplCotacaoBMF_FileUploadComplete(object sender, FileUploadCompleteEventArgs e) {
        e.CallbackData = "";

        #region Trata Extensão Válida
        if (!ValoresExcelUtil.isExtensaoExcel(e.UploadedFile.FileName.Trim())) {
            e.CallbackData = "Importação Cotação BMF - Formato de Arquivo Inválido. Formato permitido: .xls ou .xlsx\n\n";
            return;
        }
        #endregion

        // Stream de bytes com o Conteudo do Arquivo Excel
        Stream sr = e.UploadedFile.FileContent;

        #region Processamento Arquivo Excel

        // Reseta Vetor de Dados
        this.valoresExcelCotacaoBMF = new List<ValoresExcelCotacaoBMF>();
        this.dataDeletarCotacaoBMF = new List<DateTime>();
        this.cdAtivoBMFDeletarCotacaoBMF = new List<string>();
        this.serieBMFDeletarCotacaoBMF = new List<string>();

        //
        try {
            // Ler Arquivo
            this.LerArquivoCotacaoBMF(sr);
            // Carrega Arquivo
            this.CarregaCotacaoBMF();
        }
        catch (Exception e2) {
            e.CallbackData = "Importação Cotação BMF - " + e2.Message;
            return;
        }

        new HistoricoLog().InsereHistoricoLog(DateTime.Now, DateTime.Now,
                "Importação Planilha Cotação BMF",
                HttpContext.Current.User.Identity.Name, UtilitarioWeb.GetIP(Request), "", HistoricoLogOrigem.Outros);

        #endregion

    }

}
