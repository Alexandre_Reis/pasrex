﻿using System;
using System.Collections.Generic;
using System.IO;

using Financial.Integracao.Excel;
using Financial.Integracao.Excel.Util;
using Financial.Web.Common;
using Financial.Bolsa;

using EntitySpaces.Interfaces;

using DevExpress.Web;
using Financial.Security;
using System.Web;
using Financial.Web.Util;
using Financial.Security.Enums;
using Financial.Interfaces.Import.RendaFixa;
using Financial.RendaFixa;

public partial class ImportacaoBasePage : BasePage {

    /* Estrutura do Excel */
    private List<ValoresExcelCotacaoAnbimaPublico> valoresExcelCotacaoAnbimaPublico = new List<ValoresExcelCotacaoAnbimaPublico>();
    //
    /// <summary>
    /// Leitura de um arquivo Excel
    /// Saida: List<ValoresExcel> com os valores armazenados no arquivo
    /// </summary>
    /// <param name="streamExcel">Stream de bytes de um arquivo Excel</param>
    /// <exception cref="Exception">Se ocorreu problemas</exception>
    private void LerArquivoCotacaoAnbimaPublico(Stream streamExcel) {

        #region Leitura do Arquivo Excel

        // Open Spreadsheet
        Bytescout.Spreadsheet.Spreadsheet document = new Bytescout.Spreadsheet.Spreadsheet();
        document.LoadFromStream(streamExcel);

        Bytescout.Spreadsheet.Worksheet workSheet = document.Workbook.Worksheets[0];

        //
        #region Confere Formato Arquivo
        string[] colunasConferencias = new string[] { "Data", "Descricao", "CodigoSelic", "DataEmissao", "DataVencimento", "TaxaIndicativa", "PU" };

        bool formato = ValoresExcelUtil.isExcelFormatoValido(colunasConferencias, workSheet);

        if (!formato) {
            string mensagem = "Formato Interno do Arquivo Inválido.\n";
            mensagem += "Linha 1 do Arquivo deve ser: \n";
            for (int i = 0; i < colunasConferencias.Length; i++) {
                int coluna = i + 1;
                mensagem += "\tColuna " + coluna + ": " + colunasConferencias[i] + "\n";
            }

            document.Close();
            document.Dispose();

            throw new Exception(mensagem);
        }
        #endregion

        //
        int index = 0;
        // This row,column index should be changed as per your need.
        // i.e. which cell in the excel you are interesting to read.
        //
        /* Formato: 1)Data  - 2)Descricao - 3)CodigoSELIC - 4)DataEmissao - 5)DataVencimento - 6)TaxaIndicativa - 7)PU */

        int linha = 1;
        int coluna1 = 0, coluna2 = 1, coluna3 = 2, coluna4 = 3, coluna5 = 4, coluna6 = 5, coluna7 = 6;

        //            
        try {
            // Enquanto Data tiver valor
            while (workSheet.Cell(linha, coluna1).Value != null) {
                ValoresExcelCotacaoAnbimaPublico item = new ValoresExcelCotacaoAnbimaPublico();
                
                try {
                    item.Data = workSheet.Cell(linha, coluna1).ValueAsDateTime;
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message + " - Data: linha " + (linha + 1));
                }

                if (workSheet.Cell(linha, coluna2).Value == null)
                    throw new Exception("Descricao não informada: linha " + (linha + 1));
                try {
                    item.Descricao = workSheet.Cell(linha, coluna2).ValueAsString;
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message + " - Descricao: linha " + (linha + 1));
                }

                if (workSheet.Cell(linha, coluna3).Value == null)
                    throw new Exception("CodigoSelic não informado: linha " + (linha + 1));
                try {
                    item.CodigoSelic = workSheet.Cell(linha, coluna3).ValueAsString;
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message + " - CodigoSelic: linha " + (linha + 1));
                }

                if (workSheet.Cell(linha, coluna4).Value == null)
                    throw new Exception("DataEmissao não informado: linha " + (linha + 1));
                try {
                    item.DataEmissao = workSheet.Cell(linha, coluna4).ValueAsDateTime;
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message + " - DataEmissao: linha " + (linha + 1));
                }

                if (workSheet.Cell(linha, coluna5).Value == null)
                    throw new Exception("DataVencimento não informada: linha " + (linha + 1));
                try {
                    item.DataVencimento = workSheet.Cell(linha, coluna5).ValueAsDateTime;
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message + " - DataVencimento: linha " + (linha + 1));
                }

                if (workSheet.Cell(linha, coluna6).Value == null)
                    throw new Exception("TaxaIndicativa não informada: linha " + (linha + 1));
                try {
                    item.TaxaIndicativa = Convert.ToDecimal(workSheet.Cell(linha, coluna6).Value);
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message + " - TaxaIndicativa: linha " + (linha + 1));
                }

                if (workSheet.Cell(linha, coluna7).Value == null)
                    throw new Exception("Pu não informado: linha " + (linha + 1));
                try {
                    item.Pu = Convert.ToDecimal(workSheet.Cell(linha, coluna7).Value);
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message + " - Pu: linha " + (linha + 1));
                }

                this.valoresExcelCotacaoAnbimaPublico.Add(item);
                
                index++;
                linha = 1 + index;
                
                Console.WriteLine("{0},{1},{2},{3},{4},{5},{6},{7}", item.Data, item.Descricao, item.CodigoSelic, item.DataEmissao, item.DataEmissao, item.DataVencimento, item.TaxaIndicativa, item.Pu);
            }
        }
        catch (Exception ex) {
            document.Close();
            document.Dispose();

            throw new Exception(ex.Message);
        }
        #endregion

        document.Close();
        document.Dispose();
    }

    /// <summary>
    /// Gera e Carrega as Cotações Bolsa de acordo com o objeto List<ValoresExcel>
    /// </summary>
    private void CarregaCotacaoAnbimaPublico()
    {
        AndimaCollection andimaCollection = new AndimaCollection();
        //    
        for (int i = 0; i < this.valoresExcelCotacaoAnbimaPublico.Count; i++) 
        {
            ValoresExcelCotacaoAnbimaPublico valoresExcel = this.valoresExcelCotacaoAnbimaPublico[i];

            Andima andima = new Andima();

            andima.CodigoSelic = valoresExcel.CodigoSelic;
            andima.DataEmissao = valoresExcel.DataEmissao;
            andima.DataVencimento = valoresExcel.DataVencimento;
            andima.DataReferencia = valoresExcel.Data;
            andima.Descricao = valoresExcel.Descricao;
            andima.Pu = valoresExcel.Pu;
            andima.TaxaIndicativa = valoresExcel.TaxaIndicativa;

            andimaCollection.Add(andima);            
        }

        CotacaoMercadoAndima cotacaoMercadoAndima = new CotacaoMercadoAndima();
        cotacaoMercadoAndima.CarregaAndima(andimaCollection);
    }

    /// <summary>
    /// Processa a Planilha de CotacaoAnbimaPublico após ter acabado o Upload
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void uplCotacaoAnbimaPublico_FileUploadComplete(object sender, FileUploadCompleteEventArgs e)
    {
        e.CallbackData = "";

        #region Trata Extensão Válida
        if (!ValoresExcelUtil.isExtensaoExcel(e.UploadedFile.FileName.Trim())) {
            e.CallbackData = "Importação Cotação Anbima Publico - Formato de Arquivo Inválido. Formato permitido: .xls ou .xlsx\n\n";
            return;
        }
        #endregion

        // Stream de bytes com o Conteudo do Arquivo Excel
        Stream sr = e.UploadedFile.FileContent;

        #region Processamento Arquivo Excel

        // Reseta Vetor de Dados
        this.valoresExcelCotacaoAnbimaPublico = new List<ValoresExcelCotacaoAnbimaPublico>();
        
        //
        try {
            // Ler Arquivo
            this.LerArquivoCotacaoAnbimaPublico(sr);
            // Carrega Arquivo
            this.CarregaCotacaoAnbimaPublico();
        }
        catch (Exception e2) {
            e.CallbackData = "Importação Cotação Anbima Publico - " + e2.Message;
            return;
        }

        new HistoricoLog().InsereHistoricoLog(DateTime.Now, DateTime.Now,
            "Importação Planilha Cotação Anbima Publico",
            HttpContext.Current.User.Identity.Name, UtilitarioWeb.GetIP(Request), "", HistoricoLogOrigem.Outros);

        #endregion

    }

}
