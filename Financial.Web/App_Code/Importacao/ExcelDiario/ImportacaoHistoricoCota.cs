﻿using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Financial.Web.Common;
using System.IO;
using Financial.Integracao.Excel.Util;
using Financial.Integracao.Excel;
using EntitySpaces.Interfaces;
using Financial.Common;
using System.Collections.Generic;
using Financial.Investidor;
using DevExpress.Web;
using Financial.Fundo;
using Financial.Security;
using Financial.Web.Util;
using Financial.Security.Enums;
using Financial.Util;
using Financial.Util.Enums;  
using Financial.Investidor.Enums;
using Financial.Investidor.Controller;

public partial class ImportacaoBasePage : BasePage {

    /* Estrutura do Excel */
    private List<ValoresExcelHistoricoCota> valoresExcelHistoricoCota = new List<ValoresExcelHistoricoCota>();
    
    /// <summary>
    /// Leitura de um arquivo Excel
    /// Saida: List<ValoresExcel> com os valores armazenados no arquivo
    /// </summary>
    /// <param name="streamExcel">Stream de bytes de um arquivo Excel</param>
    /// <exception cref="Exception">Se ocorreu problemas</exception>
    private void LerArquivoHistoricoCota(Stream streamExcel)
    {
        #region Leitura do Arquivo Excel

        // Open Spreadsheet
        Bytescout.Spreadsheet.Spreadsheet document = new Bytescout.Spreadsheet.Spreadsheet();
        document.LoadFromStream(streamExcel);

        Bytescout.Spreadsheet.Worksheet workSheet = document.Workbook.Worksheets[0];

        //
        #region Confere Formato Arquivo
        string[] colunasConferencias = new string[] {"IdCarteira","Data","Cota","PL",
                                                         "QuantidadeFechamento"};

        bool formato = ValoresExcelUtil.isExcelFormatoValido(colunasConferencias, workSheet);

        if (!formato)
        {
            string mensagem = "Formato Interno do Arquivo Inválido.\n";
            mensagem += "Linha 1 do Arquivo deve ser: \n";
            for (int i = 0; i < colunasConferencias.Length; i++)
            {
                int coluna = i + 1;
                mensagem += "\tColuna " + coluna + ": " + colunasConferencias[i] + "\n";
            }

            document.Close();
            document.Dispose();

            throw new Exception(mensagem);
        }
        #endregion

        //
        int index = 0;
        // This row,column index should be changed as per your need.
        // i.e. which cell in the excel you are interesting to read.
        //
        /* Formato: 1)IdCarteira  - 2)Data - 3)Cota - 4)PL - 5)QuantidadeFechamento            
         */
        int linha = 1;
        int coluna1 = 0, coluna2 = 1, coluna3 = 2,
            coluna4 = 3, coluna5 = 4;

        //            
        try
        {
            // Enquanto idCotista tiver valor
            while (workSheet.Cell(linha, coluna1).Value != null)
            {
                ValoresExcelHistoricoCota item = new ValoresExcelHistoricoCota();
                
                try {
                    item.IdCarteira = workSheet.Cell(linha, coluna1).ValueAsInteger;
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message + " - IdCarteira: linha " + (linha + 1));
                }

                if (workSheet.Cell(linha, coluna2).Value == null)
                    throw new Exception("Data não informada: linha " + (linha + 1));
                try {
                    item.Data = workSheet.Cell(linha, coluna2).ValueAsDateTime;
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message + " - Data: linha " + (linha + 1));
                }
                    
                decimal valorCota = 0;
                decimal valorPL = 0;
                decimal quantidadeCotas = 0;

                if (workSheet.Cell(linha, coluna3) != null)
                {
                    try {
                        valorCota = Convert.ToDecimal(workSheet.Cell(linha, coluna3).Value);
                    }
                    catch (Exception ex)
                    {
                        throw new Exception(ex.Message + " - Cota: linha " + (linha + 1));
                    }
                }
                if (workSheet.Cell(linha, coluna4) != null)
                {
                    try {
                        valorPL = Convert.ToDecimal(workSheet.Cell(linha, coluna4).Value);
                    }
                    catch (Exception ex)
                    {
                        throw new Exception(ex.Message + " - PL: linha " + (linha + 1));
                    }
                }
                if (workSheet.Cell(linha, coluna5) != null)
                {
                    try {
                        quantidadeCotas = Convert.ToDecimal(workSheet.Cell(linha, coluna5).Value);
                    }
                    catch (Exception ex)
                    {
                        throw new Exception(ex.Message + " - QuantidadeFechamento: linha " + (linha + 1));
                    }
                }

                item.Cota = valorCota;
                item.PL = valorPL;
                item.QuantidadeFechamento = quantidadeCotas;
                //
                this.valoresExcelHistoricoCota.Add(item);
                //
                index++;
                linha = 1 + index;
                //                
                Console.WriteLine("{0},{1},{2},{3},{4}",
                    item.IdCarteira, item.Data, item.Cota, item.PL, item.QuantidadeFechamento);
            }
        }
        catch (Exception ex)
        {
            document.Close();
            document.Dispose();

            throw new Exception(ex.Message);
        }
        #endregion

        // Fecha o arquivo
        document.Close();
        document.Dispose();
    }
    
    /// <summary>
    /// Gera e carrega as posições Cotista de acordo com o objeto List<ValoresExcel>
    /// </summary>
    private void CarregaHistoricoCota()
    {
        HistoricoCotaCollection historicoCotaCollection = new HistoricoCotaCollection();
        //    
        for (int i = 0; i < this.valoresExcelHistoricoCota.Count; i++)
        {
            ValoresExcelHistoricoCota valoresExcel = this.valoresExcelHistoricoCota[i];

            #region Verifica se já existe HistoricoCota com o mesmo IdCarteira/Data
            HistoricoCota historicoCota = new HistoricoCota();

            // Se não existe é feito um insert, caso contrario é feito um update
            if (!historicoCota.LoadByPrimaryKey(valoresExcel.Data, valoresExcel.IdCarteira))
            {
                historicoCota = new HistoricoCota();
            }

            #region Update/Insert

            #region Verifica se Existe Carteira
            Carteira carteira = new Carteira();
            if (!carteira.LoadByPrimaryKey(valoresExcel.IdCarteira))
            {
                throw new Exception("Carteira Não existente : " + valoresExcel.IdCarteira);
            }
            #endregion

            // Dados do Arquivo
            historicoCota.Data = valoresExcel.Data;
            historicoCota.IdCarteira = valoresExcel.IdCarteira;
            //
            historicoCota.CotaAbertura = valoresExcel.Cota;
            historicoCota.CotaFechamento = valoresExcel.Cota;
            historicoCota.CotaBruta = valoresExcel.Cota;
            //
            historicoCota.PLAbertura = valoresExcel.PL;
            historicoCota.PLFechamento = valoresExcel.PL;
            historicoCota.PatrimonioBruto = valoresExcel.PL;
            //
            historicoCota.QuantidadeFechamento = valoresExcel.QuantidadeFechamento;
            historicoCota.AjustePL = 0;
            //
            historicoCota.CotaImportada = "S";
            //
            // Attach the object
            historicoCotaCollection.AttachEntity(historicoCota);

            #endregion
            #endregion
        }

        historicoCotaCollection.Save();

        //#region Verifica se precisa retroagir carteiras que compraram cotas do fundo importado
        //if (ParametrosConfiguracaoSistema.Outras.RetroagirCarteirasDependentes)
        //{

        //    foreach (HistoricoCota historicoCota in historicoCotaCollection)
        //    {
        //        ControllerInvestidor controllerInvestidor = new ControllerInvestidor();
        //        ClienteQuery clienteQuery = new ClienteQuery("CQ");
        //        PosicaoFundoHistoricoQuery posicaoFundoHistoricoQuery = new PosicaoFundoHistoricoQuery("PFHQ");
        //        posicaoFundoHistoricoQuery.Select(posicaoFundoHistoricoQuery.IdCliente);
        //        posicaoFundoHistoricoQuery.InnerJoin(clienteQuery).On(posicaoFundoHistoricoQuery.IdCliente == clienteQuery.IdCliente);
        //        posicaoFundoHistoricoQuery.Where(posicaoFundoHistoricoQuery.IdCarteira.Equal(historicoCota.IdCarteira));
        //        posicaoFundoHistoricoQuery.Where(posicaoFundoHistoricoQuery.DataHistorico.Equal(historicoCota.Data));
        //        posicaoFundoHistoricoQuery.Where(clienteQuery.DataDia.GreaterThan(historicoCota.Data));
        //        posicaoFundoHistoricoQuery.es.Distinct = true;

        //        PosicaoFundoHistoricoCollection posicaoFundoHistoricoCollection = new PosicaoFundoHistoricoCollection();
        //        posicaoFundoHistoricoCollection.Load(posicaoFundoHistoricoQuery);

        //        foreach (PosicaoFundoHistorico posicaoFundoHistorico in posicaoFundoHistoricoCollection)
        //        {
        //            //Volta processamento do carteira que teve cota de fundo comprada da carteira que foi processada. 
        //            controllerInvestidor.ReprocessaPosicao(posicaoFundoHistorico.IdCliente.Value, historicoCota.Data.Value, TipoReprocessamento.Fechamento, false);
        //        }
        //    }
        //}
        //#endregion


        #region Verifica se precisa marcar carteiras para reprocessamento, que compraram cotas do fundo 
        if (ParametrosConfiguracaoSistema.Outras.RetroagirCarteirasDependentes)
        {

            Cliente cliente = new Cliente();

            foreach (HistoricoCota historicoCota in historicoCotaCollection)
            {
                ControllerInvestidor controllerInvestidor = new ControllerInvestidor();
                ClienteQuery clienteQuery = new ClienteQuery("CQ");
                PosicaoFundoHistoricoQuery posicaoFundoHistoricoQuery = new PosicaoFundoHistoricoQuery("PFHQ");
                posicaoFundoHistoricoQuery.Select(posicaoFundoHistoricoQuery.IdCliente);
                posicaoFundoHistoricoQuery.InnerJoin(clienteQuery).On(posicaoFundoHistoricoQuery.IdCliente == clienteQuery.IdCliente);
                posicaoFundoHistoricoQuery.Where(posicaoFundoHistoricoQuery.IdCarteira.Equal(historicoCota.IdCarteira));
                posicaoFundoHistoricoQuery.Where(posicaoFundoHistoricoQuery.DataHistorico.Equal(historicoCota.Data));
                posicaoFundoHistoricoQuery.Where(clienteQuery.DataDia.GreaterThan(historicoCota.Data));
                posicaoFundoHistoricoQuery.es.Distinct = true;

                PosicaoFundoHistoricoCollection posicaoFundoHistoricoCollection = new PosicaoFundoHistoricoCollection();
                posicaoFundoHistoricoCollection.Load(posicaoFundoHistoricoQuery);

                foreach (PosicaoFundoHistorico posicaoFundoHistorico in posicaoFundoHistoricoCollection)
                {
                    cliente.LoadByPrimaryKey(posicaoFundoHistorico.IdCliente.Value);
                    if (DateTime.Compare(cliente.DataDia.Value, historicoCota.Data.Value) > 0)
                    {
                        BoletoRetroativo boletoRetroativo = new BoletoRetroativo();
                        if (boletoRetroativo.LoadByPrimaryKey(posicaoFundoHistorico.IdCliente.Value))
                        {
                            if (DateTime.Compare(boletoRetroativo.DataBoleto.Value, historicoCota.Data.Value) > 0)
                            {
                                boletoRetroativo.DataBoleto = historicoCota.Data.Value;
                            }
                        }
                        else
                        {
                            boletoRetroativo.IdCliente = posicaoFundoHistorico.IdCliente.Value;
                            boletoRetroativo.DataBoleto = historicoCota.Data.Value;
                            boletoRetroativo.TipoMercado = (int)TipoMercado.Fundos;
                        }

                        boletoRetroativo.Save();
                    }
                }
            }
        }
        #endregion

    }

    /// <summary>
    /// Processa a Planilha de HistoricoCota após ter acabado o Upload
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void uplHistoricoCota_FileUploadComplete(object sender, FileUploadCompleteEventArgs e)
    {
        e.CallbackData = "";

        #region Trata Extensão Válida
        if (!ValoresExcelUtil.isExtensaoExcel(e.UploadedFile.FileName.Trim()))
        {
            e.CallbackData = "Importação Histórico Cota - Formato de Arquivo Inválido. Formato permitido: .xls ou .xlsx\n\n";
            return;
        }
        #endregion

        // Stream de bytes com o Conteudo do Arquivo Excel
        Stream sr = e.UploadedFile.FileContent;

        #region Processamento Arquivo Excel

        //Reseta Vetor de Dados
        this.valoresExcelHistoricoCota = new List<ValoresExcelHistoricoCota>();
        //
        try
        {
            // Ler Arquivo
            this.LerArquivoHistoricoCota(sr);
            // Carrega Arquivo
            this.CarregaHistoricoCota();
        }
        catch (Exception e2)
        {
            e.CallbackData = "Importação Histórico Cota - " + e2.Message;
            return;
        }

        new HistoricoLog().InsereHistoricoLog(DateTime.Now, DateTime.Now,
            "Importação Planilha Histórico Cota",
            HttpContext.Current.User.Identity.Name, UtilitarioWeb.GetIP(Request), "", HistoricoLogOrigem.Outros);

        #endregion
    }
}
