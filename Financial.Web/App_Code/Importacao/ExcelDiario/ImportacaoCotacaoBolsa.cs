﻿using System;
using System.Collections.Generic;
using System.IO;

using Financial.Integracao.Excel;
using Financial.Integracao.Excel.Util;
using Financial.Web.Common;
using Financial.Bolsa;

using EntitySpaces.Interfaces;

using DevExpress.Web;
using Financial.Security;
using System.Web;
using Financial.Web.Util;
using Financial.Security.Enums;

public partial class ImportacaoBasePage : BasePage {

    /* Estrutura do Excel */
    private List<ValoresExcelCotacaoBolsa> valoresExcelCotacaoBolsa = new List<ValoresExcelCotacaoBolsa>();
    //
    List<DateTime> dataDeletarCotacaoBolsa = new List<DateTime>();
    List<string> cdAtivoBolsaDeletarCotacaoBolsa = new List<string>();
    
    /// <summary>
    /// Leitura de um arquivo Excel
    /// Saida: List<ValoresExcel> com os valores armazenados no arquivo
    /// </summary>
    /// <param name="streamExcel">Stream de bytes de um arquivo Excel</param>
    /// <exception cref="Exception">Se ocorreu problemas</exception>
    private void LerArquivoCotacaoBolsa(Stream streamExcel) {

        #region Leitura do Arquivo Excel

        // Open Spreadsheet
        Bytescout.Spreadsheet.Spreadsheet document = new Bytescout.Spreadsheet.Spreadsheet();
        document.LoadFromStream(streamExcel);

        Bytescout.Spreadsheet.Worksheet workSheet = document.Workbook.Worksheets[0];

        //
        #region Confere Formato Arquivo
        string[] colunasConferencias = new string[] { "Data", "CodigoAtivo", "PUMedio", "PUFechamento" };

        bool formato = ValoresExcelUtil.isExcelFormatoValido(colunasConferencias, workSheet);

        if (!formato) {
            string mensagem = "Formato Interno do Arquivo Inválido.\n";
            mensagem += "Linha 1 do Arquivo deve ser: \n";
            for (int i = 0; i < colunasConferencias.Length; i++) {
                int coluna = i + 1;
                mensagem += "\tColuna " + coluna + ": " + colunasConferencias[i] + "\n";
            }

            document.Close();
            document.Dispose();

            throw new Exception(mensagem);
        }
        #endregion

        //
        int index = 0;
        // This row,column index should be changed as per your need.
        // i.e. which cell in the excel you are interesting to read.
        //
        /* Formato: 1)Data  - 2)CodigoAtivo - 3)PUMedio - 4)PUFechamento */

        int linha = 1;
        int coluna1 = 0, coluna2 = 1, coluna3 = 2, coluna4 = 3;

        //            
        try {
            // Enquanto Data tiver valor
            while (workSheet.Cell(linha, coluna1).Value != null) {
                ValoresExcelCotacaoBolsa item = new ValoresExcelCotacaoBolsa();

                if (workSheet.Cell(linha, coluna1).Value == null)
                    throw new Exception("Data não informado: linha " + (linha + 1));
                try {
                    item.Data = workSheet.Cell(linha, coluna1).ValueAsDateTime;
                }
                catch (Exception ex) {
                    throw new Exception(ex.Message + " - Data: linha " + (linha + 1));
                }

                if (workSheet.Cell(linha, coluna2).Value == null)
                    throw new Exception("CdAtivoBolsa não informado: linha " + (linha + 1));
                try {
                    item.CdAtivoBolsa = workSheet.Cell(linha, coluna2).ValueAsString.Trim().ToUpper();
                }
                catch (Exception ex) {
                    throw new Exception(ex.Message + " - CdAtivoBolsa: linha " + (linha + 1));
                }

                if (workSheet.Cell(linha, coluna3).Value == null)
                    throw new Exception("PuMedio não informado: linha " + (linha + 1));
                try {
                    item.PuMedio = Convert.ToDecimal(workSheet.Cell(linha, coluna3).Value);
                }
                catch (Exception ex) {
                    throw new Exception(ex.Message + " - PuMedio: linha " + (linha + 1));
                }

                if (workSheet.Cell(linha, coluna4).Value == null)
                    throw new Exception("PuFechamento não informado: linha " + (linha + 1));
                try {
                    item.PuFechamento = Convert.ToDecimal(workSheet.Cell(linha, coluna4).Value);
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message + " - PuFechamento: linha " + (linha + 1));
                }

                this.valoresExcelCotacaoBolsa.Add(item);
                
                index++;
                linha = 1 + index;
                
                Console.WriteLine("{0},{1},{2}", item.Data, item.CdAtivoBolsa, item.PuMedio, item.PuFechamento);
            }
        }
        catch (Exception ex) {
            document.Close();
            document.Dispose();

            throw new Exception(ex.Message);
        }
        #endregion

        document.Close();
        document.Dispose();
    }

    /// <summary>
    /// Gera e Carrega as Cotações Bolsa de acordo com o objeto List<ValoresExcel>
    /// </summary>
    private void CarregaCotacaoBolsa() {
        CotacaoBolsaCollection cotacaoBolsaCollection = new CotacaoBolsaCollection();
        CotacaoBolsaCollection cotacaoBolsaDeletarCollection = new CotacaoBolsaCollection();
        //    
        for (int i = 0; i < this.valoresExcelCotacaoBolsa.Count; i++) {
            ValoresExcelCotacaoBolsa valoresExcel = this.valoresExcelCotacaoBolsa[i];
            //
            AtivoBolsa ativoBolsa = new AtivoBolsa();
            if (!ativoBolsa.LoadByPrimaryKey(valoresExcel.CdAtivoBolsa)) 
            {
                continue;
            }
            
            // Salva a Data e o Ativo para poder Deletar            
            this.dataDeletarCotacaoBolsa.Add(valoresExcel.Data);
            this.cdAtivoBolsaDeletarCotacaoBolsa.Add(valoresExcel.CdAtivoBolsa);

            #region CotacaoBolsa
            CotacaoBolsa cotacaoBolsa = cotacaoBolsaCollection.AddNew();
            // Dados do Arquivo
            cotacaoBolsa.Data = valoresExcel.Data;
            cotacaoBolsa.CdAtivoBolsa = valoresExcel.CdAtivoBolsa;
            cotacaoBolsa.PUAbertura = valoresExcel.PuMedio;
            cotacaoBolsa.PUMedio = valoresExcel.PuMedio;
            cotacaoBolsa.PUFechamento = valoresExcel.PuFechamento;            
            //
            #endregion
        }

        using (esTransactionScope scope = new esTransactionScope()) {

            #region Deleta CotacaoBolsa por Data/CdAtivoBolsa
            for (int i = 0; i < this.dataDeletarCotacaoBolsa.Count; i++) {
                cotacaoBolsaDeletarCollection = new CotacaoBolsaCollection();

                cotacaoBolsaDeletarCollection.Query
                .Where(cotacaoBolsaDeletarCollection.Query.Data == this.dataDeletarCotacaoBolsa[i],
                       cotacaoBolsaDeletarCollection.Query.CdAtivoBolsa == this.cdAtivoBolsaDeletarCotacaoBolsa[i]);

                cotacaoBolsaDeletarCollection.Query.Load();
                cotacaoBolsaDeletarCollection.MarkAllAsDeleted();
                cotacaoBolsaDeletarCollection.Save();
            }
            #endregion

            // Salva Cotações Índices presentes no Excel                
            cotacaoBolsaCollection.Save();

            scope.Complete();
        }
    }

    /// <summary>
    /// Processa a Planilha de CotacaoBolsa após ter acabado o Upload
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void uplCotacaoBolsa_FileUploadComplete(object sender, FileUploadCompleteEventArgs e) {
        e.CallbackData = "";

        #region Trata Extensão Válida
        if (!ValoresExcelUtil.isExtensaoExcel(e.UploadedFile.FileName.Trim())) {
            e.CallbackData = "Importação Cotação Bolsa - Formato de Arquivo Inválido. Formato permitido: .xls ou .xlsx\n\n";
            return;
        }
        #endregion

        // Stream de bytes com o Conteudo do Arquivo Excel
        Stream sr = e.UploadedFile.FileContent;

        #region Processamento Arquivo Excel

        // Reseta Vetor de Dados
        this.valoresExcelCotacaoBolsa = new List<ValoresExcelCotacaoBolsa>();
        this.dataDeletarCotacaoBolsa = new List<DateTime>();
        this.cdAtivoBolsaDeletarCotacaoBolsa = new List<string>();

        //
        try {
            // Ler Arquivo
            this.LerArquivoCotacaoBolsa(sr);
            // Carrega Arquivo
            this.CarregaCotacaoBolsa();
        }
        catch (Exception e2) {
            e.CallbackData = "Importação Cotação Bolsa - " + e2.Message;
            return;
        }

        new HistoricoLog().InsereHistoricoLog(DateTime.Now, DateTime.Now,
            "Importação Planilha Cotação Bolsa",
            HttpContext.Current.User.Identity.Name, UtilitarioWeb.GetIP(Request), "", HistoricoLogOrigem.Outros);

        #endregion

    }

}
