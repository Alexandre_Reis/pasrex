﻿using System;
using System.Collections.Generic;
using System.IO;
using Financial.Integracao.Excel;
using Financial.Integracao.Excel.Util;
using Financial.RendaFixa;
using Financial.Web.Common;
using EntitySpaces.Interfaces;
using DevExpress.Web;
using Financial.Security;
using System.Web;
using Financial.Web.Util;
using Financial.Security.Enums;
using Financial.Util;
using Financial.Common.Enums;

public partial class ImportacaoBasePage : BasePage
{

    /* Estrutura do Excel */
    private List<ValoresExcelTaxaCurva> valoresExcelTaxaCurva = new List<ValoresExcelTaxaCurva>();

    //
    List<int> idCurvaRendaFixaDeletarTaxaCurva = new List<int>();
    List<DateTime> dataBaseDeletarTaxaCurva = new List<DateTime>();
    List<DateTime> dataVerticeDeletarTaxaCurva = new List<DateTime>();
    //

    /// <summary>
    /// Leitura de um arquivo Excel
    /// Saida: List<ValoresExcel> com os valores armazenados no arquivo
    /// </summary>
    /// <param name="streamExcel">Stream de bytes de um arquivo Excel</param>
    /// <exception cref="Exception">Se ocorreu problemas</exception>
    private void LerArquivoTaxaCurva(Stream streamExcel)
    {

        #region Leitura do Arquivo Excel

        // Open Spreadsheet
        Bytescout.Spreadsheet.Spreadsheet document = new Bytescout.Spreadsheet.Spreadsheet();
        document.LoadFromStream(streamExcel);

        Bytescout.Spreadsheet.Worksheet workSheet = document.Workbook.Worksheets[0];

        //
        #region Confere Formato Arquivo
        string[] colunasConferencias = new string[] { "DataBase", "IdCurvaRendaFixa", "DataVertice", "CodigoVertice", "PrazoDU", "PrazoDC", "Taxa" };

        bool formato = ValoresExcelUtil.isExcelFormatoValido(colunasConferencias, workSheet);

        if (!formato)
        {
            string mensagem = "Formato Interno do Arquivo Inválido.\n";
            mensagem += "Linha 1 do Arquivo deve ser: \n";
            for (int i = 0; i < colunasConferencias.Length; i++)
            {
                int coluna = i + 1;
                mensagem += "\tColuna " + coluna + ": " + colunasConferencias[i] + "\n";
            }

            document.Close();
            document.Dispose();

            throw new Exception(mensagem);
        }
        #endregion

        //
        int index = 0;
        // This row,column index should be changed as per your need.
        // i.e. which cell in the excel you are interesting to read.
        //
        /* Formato: 1)DataBase      -2)IdCurvaRendaFixa     -3)DataVertice      -4)CodigoVertice	-5)PrazoDU      -6)PrazoDC      -7)Taxa */

        int linha = 1;
        int coluna1 = 0, coluna2 = 1, coluna3 = 2, coluna4 = 3, coluna5 = 4, coluna6 = 5, coluna7 = 6;

        //            
        try
        {
            // Enquanto Data tiver valor
            while (workSheet.Cell(linha, coluna1).Value != null)
            {
                ValoresExcelTaxaCurva item = new ValoresExcelTaxaCurva();

                #region DataBase
                if (workSheet.Cell(linha, coluna1).Value == null)
                    throw new Exception("DataBase não informada: linha " + (linha + 1));

                try
                {
                    item.DataBase = workSheet.Cell(linha, coluna1).ValueAsDateTime;
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message + " - DataBase: linha " + (linha + 1));
                }
                #endregion

                #region IdCurvaRendaFixa
                if (workSheet.Cell(linha, coluna2).Value == null)
                    throw new Exception("IdCurvaRendaFixa não informado: linha " + (linha + 1));

                try
                {
                    item.IdCurvaRendaFixa = workSheet.Cell(linha, coluna2).ValueAsInteger;
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message + " - IdCurvaRendaFixa: linha " + (linha + 1));
                }
                #endregion

                #region DataVertice
                if (workSheet.Cell(linha, coluna3).Value == null)
                    throw new Exception("DataVertice não informada: linha " + (linha + 1));

                try
                {
                    item.DataVertice = workSheet.Cell(linha, coluna3).ValueAsDateTime;
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message + " - DataVertice: linha " + (linha + 1));
                }
                #endregion

                #region CodigoVertice
                try
                {
                    item.CodigoVertice = (workSheet.Cell(linha, coluna4).Value == null ? null : workSheet.Cell(linha, coluna4).ValueAsString);
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message + " - CodigoVertice: linha " + (linha + 1));
                }
                #endregion

                #region PrazoDU
                try
                {
                    item.PrazoDU = (workSheet.Cell(linha, coluna5).Value == null ? 0 : workSheet.Cell(linha, coluna5).ValueAsInteger);
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message + " - PrazoDU: linha " + (linha + 1));
                }
                #endregion

                #region PrazoDC
                try
                {
                    item.PrazoDC = (workSheet.Cell(linha, coluna6).Value == null ? 0 : workSheet.Cell(linha, coluna6).ValueAsInteger);
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message + " - PrazoDC: linha " + (linha + 1));
                }
                #endregion

                #region Taxa
                if (workSheet.Cell(linha, coluna7).Value == null)
                    throw new Exception("Taxa não informada: linha " + (linha + 1));
                try
                {
                    item.Taxa = Convert.ToDecimal(workSheet.Cell(linha, coluna7).Value);
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message + " - Taxa: linha " + (linha + 1));
                }
                #endregion

                this.valoresExcelTaxaCurva.Add(item);

                index++;
                linha = 1 + index;

                string codigoVertice = (string.IsNullOrEmpty(item.CodigoVertice) ? string.Empty : item.CodigoVertice);
                Console.WriteLine("{0},{1},{2},{3},{4},{5},{6}", item.DataBase, item.IdCurvaRendaFixa, item.DataVertice, codigoVertice, item.PrazoDU, item.PrazoDC, item.Taxa);
            }
        }
        catch (Exception ex)
        {
            document.Close();
            document.Dispose();

            throw new Exception(ex.Message);
        }
        #endregion

        document.Close();
        document.Dispose();
    }

    /// <summary>
    /// Gera e Carrega as Cotações Series de acordo com o objeto List<ValoresExcel>
    /// </summary>     
    private void CarregaTaxaCurva()
    {
        TaxaCurvaCollection taxaCurvaCollection = new TaxaCurvaCollection();
        TaxaCurvaCollection taxaCurvaDeletarCollection = new TaxaCurvaCollection();

        //    
        for (int i = 0; i < this.valoresExcelTaxaCurva.Count; i++)
        {
            ValoresExcelTaxaCurva valoresExcel = this.valoresExcelTaxaCurva[i];
            //
            #region Verifica se Existe Curva Renda Fixa
            CurvaRendaFixa curvaRendaFixa = new CurvaRendaFixa();
            if (!curvaRendaFixa.LoadByPrimaryKey(valoresExcel.IdCurvaRendaFixa))
            {
                throw new Exception("IdCurvaRendaFixa não existente: " + valoresExcel.IdCurvaRendaFixa + "\n Cadastre a Curva Renda Fixa " + valoresExcel.IdCurvaRendaFixa);
            }
            #endregion

            if(valoresExcel.DataVertice <= valoresExcel.DataBase)
            {
                throw new Exception(" DataVertice deve ser maior que a DataBase - IdCurvaRendaFixa - " + valoresExcel.IdCurvaRendaFixa);
            }

            // Salva id e Data para poder Deletar
            this.idCurvaRendaFixaDeletarTaxaCurva.Add(valoresExcel.IdCurvaRendaFixa);
            this.dataBaseDeletarTaxaCurva.Add(valoresExcel.DataBase);
            this.dataVerticeDeletarTaxaCurva.Add(valoresExcel.DataVertice);

            #region TaxaCurva
            TaxaCurva taxaCurva = taxaCurvaCollection.AddNew();
            // Dados do Arquivo
            taxaCurva.DataBase = valoresExcel.DataBase;
            taxaCurva.IdCurvaRendaFixa = valoresExcel.IdCurvaRendaFixa;
            taxaCurva.DataVertice = valoresExcel.DataVertice;
            taxaCurva.CodigoVertice = valoresExcel.CodigoVertice;

            if (valoresExcel.PrazoDC > 0)
                taxaCurva.PrazoDC = valoresExcel.PrazoDC;
            else
                taxaCurva.PrazoDC = Calendario.NumeroDias(taxaCurva.DataBase.Value, taxaCurva.DataVertice.Value);

            if (valoresExcel.PrazoDU > 0)
                taxaCurva.PrazoDU = valoresExcel.PrazoDU;
            else
                taxaCurva.PrazoDU = Calendario.NumeroDias(taxaCurva.DataBase.Value, taxaCurva.DataVertice.Value, (byte)LocalFeriadoFixo.Brasil, TipoFeriado.Brasil);

            taxaCurva.Taxa = valoresExcel.Taxa;
            //
            #endregion


        }

        using (esTransactionScope scope = new esTransactionScope())
        {
            #region Deleta TaxaCurva por DataBase/DataVertice/IdCurvarendaFIxa
            for (int i = 0; i < this.idCurvaRendaFixaDeletarTaxaCurva.Count; i++)
            {
                taxaCurvaDeletarCollection = new TaxaCurvaCollection();

                taxaCurvaDeletarCollection.Query
                .Where(taxaCurvaDeletarCollection.Query.IdCurvaRendaFixa == this.idCurvaRendaFixaDeletarTaxaCurva[i],
                       taxaCurvaDeletarCollection.Query.DataBase == this.dataBaseDeletarTaxaCurva[i],
                       taxaCurvaDeletarCollection.Query.DataVertice == this.dataVerticeDeletarTaxaCurva[i]);

                taxaCurvaDeletarCollection.Query.Load();
                taxaCurvaDeletarCollection.MarkAllAsDeleted();
                taxaCurvaDeletarCollection.Save();
            }
            #endregion

            // Salva Cotações Índices presentes no Excel                
            taxaCurvaCollection.Save();

            scope.Complete();
        }
    }

    /// <summary>
    /// Processa a Planilha de CotacaoIndice após ter acabado o Upload
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void uplTaxaCurvaRendaFixa_FileUploadComplete(object sender, FileUploadCompleteEventArgs e)
    {
        e.CallbackData = "";

        #region Trata Extensão Válida
        if (!ValoresExcelUtil.isExtensaoExcel(e.UploadedFile.FileName.Trim()))
        {
            e.CallbackData = "Importação Taxa Curva Renda Fixa - Formato de Arquivo Inválido. Formato permitido: .xls ou .xlsx\n\n";
            return;
        }
        #endregion

        // Stream de bytes com o Conteudo do Arquivo Excel
        Stream sr = e.UploadedFile.FileContent;

        #region Processamento Arquivo Excel

        //Reseta Vetor de Dados
        this.valoresExcelTaxaCurva = new List<ValoresExcelTaxaCurva>();
        this.idCurvaRendaFixaDeletarTaxaCurva = new List<int>();
        this.dataBaseDeletarTaxaCurva = new List<DateTime>();
        this.dataVerticeDeletarTaxaCurva = new List<DateTime>();
        //
        try
        {
            // Ler Arquivo
            this.LerArquivoTaxaCurva(sr);
            // Carrega Arquivo
            this.CarregaTaxaCurva();
        }
        catch (Exception e2)
        {
            if (e2.Message.Contains("Violation of PRIMARY KEY"))
            {
                e.CallbackData = "Importação não Concluída: 2 registros com a mesma database/vertice e curva";
            }
            else
            {
                e.CallbackData = "Importação Taxa Curva Renda Fixa - " + e2.Message;
            }
            return;
        }

        new HistoricoLog().InsereHistoricoLog(DateTime.Now, DateTime.Now,
            "Importação Planilha Taxa Curva Renda Fixa",
            HttpContext.Current.User.Identity.Name, UtilitarioWeb.GetIP(Request), "", HistoricoLogOrigem.Outros);

        #endregion
    }
}
