﻿using System;
using System.Collections.Generic;
using System.IO;

using EntitySpaces.Interfaces;

using DevExpress.Web;
using Financial.Web.Common;
using Financial.Bolsa;
using Financial.Security;
using Financial.Security.Enums;
using System.Web;
using Financial.Util;
using Financial.Common;
using Financial.Interfaces.Import.Bolsa;

public partial class ImportacaoBasePage : BasePage {

    /// <summary>
    /// Em Desenvolvimento
    /// </summary>
    /// <param name="e"></param>
    /// <param name="dataImportacao"></param>
    [TipoImportacao("ImportaPosr", Prefixo = "POSR")]
    protected void ImportaPosr(FileUploadCompleteEventArgs e, ASPxDateEdit dataImportacao) {

        ASPxDateEdit textDataArquivos = dataImportacao;

        #region Trata Extensão Valida
        if (!this.isExtensaoValida(e.UploadedFile.FileName.Trim()))
        {
            e.CallbackData = "Arquivo Posr inválido: Extensão do arquivo deve ser .txt, .dat ou nula\n\n";
            return;
        }
        #endregion

        StreamReader sr = new StreamReader(e.UploadedFile.FileContent);
        //
        bool erro = false;

        string msg = "";
        DateTime dataInicio = DateTime.Now;
        Posr posr = new Posr();
        DateTime data = Convert.ToDateTime(textDataArquivos.Text);
        PosicaoBolsa posicaoBolsa = new PosicaoBolsa();

        try
        {
            //PosrCollection posrCollection = posr.ProcessaPosr(sr, data);

            // TODO: Posr em Desenvolvimento            
            //posicaoBolsa.CarregaPosicaoPosr(posrCollection, data. idCliente);
        }
        catch (Exception ex)
        {
            erro = true;
            msg = ex.Message;
        }
        finally
        {
            erro = true;
            msg = "Posr em Desenvolvimento!\n\n";
        }

        #region Log
        if (!erro)
        {
            //Log do Processo            
            DateTime dataFim = DateTime.Now;
            //
            HistoricoLog historicoLog = new HistoricoLog();
            historicoLog.Descricao = "Importação POSR - " + data.ToShortDateString();
            historicoLog.Login = HttpContext.Current.User.Identity.Name;
            historicoLog.Maquina = Utilitario.GetLocalIp();
            historicoLog.Cultura = "";
            historicoLog.DataInicio = dataInicio;
            historicoLog.DataFim = dataFim;
            historicoLog.Origem = (int)HistoricoLogOrigem.Outros;
            historicoLog.Save();
        }
        #endregion

        e.CallbackData = msg;
    }
}