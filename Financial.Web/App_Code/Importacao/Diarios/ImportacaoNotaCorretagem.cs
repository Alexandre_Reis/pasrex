﻿using System;
using System.Collections.Generic;
using System.IO;

using EntitySpaces.Interfaces;

using DevExpress.Web;
using Financial.Web.Common;
using Financial.Bolsa;
using Financial.Security;
using Financial.Security.Enums;
using System.Web;
using Financial.Util;
using Financial.Common;
using Financial.Interfaces.Import.Bolsa;
using Financial.WebConfigConfiguration;
using System.Text;
using Dart.PowerTCP.Zip;

public partial class ImportacaoBasePage : BasePage {

    /// <summary>
    /// 
    /// </summary>
    /// <param name="e"></param>
    /// <param name="dataImportacao"></param>
    [TipoImportacao("ImportaNotaCorretagem", Prefixo = "NotasZip")]
    protected void ImportaNotaCorretagem(FileUploadCompleteEventArgs e, ASPxDateEdit dataImportacao) {

        ASPxDateEdit textDataArquivos = dataImportacao;

        e.CallbackData = "";

        #region Trata Extensão Válida
        if (!this.isExtensaoValidaNotaCorretagem(e.UploadedFile.FileName.Trim()))
        {
            e.CallbackData = "Importação Nota Corretagem - Formato de Arquivo Inválido. Formato permitido: .html/htm/mht ou zip \n\n";
            return;
        }
        #endregion

        #region Trata path do Arquivo zip
        string path = DiretorioAplicacao.DiretorioBaseAplicacao + WebConfig.AppSettings.DiretorioDownloads;
        if (!path.Trim().EndsWith("\\"))
        {
            path += "\\";
        }
        path = path.Replace("\\", "\\\\");
        #endregion
        //
        #region Pega a Data para Processamento das notas de Corretagem
        DateTime data = new DateTime();
        //
        if (!String.IsNullOrEmpty(textDataArquivos.Text))
        {
            data = Convert.ToDateTime(textDataArquivos.Text);
        }
        #endregion

        // Stream de bytes com o Conteudo do Arquivo zip - html ou htm
        Stream sr = e.UploadedFile.FileContent;

        List<NotaCorretagem.InfoArquivoHTML> infoArquivosHTML = new List<NotaCorretagem.InfoArquivoHTML>();

        #region Arquivo Zip
        string nomeArquivo = e.UploadedFile.FileName.Trim();
        string extensao = new FileInfo(nomeArquivo).Extension;
        extensao = extensao.ToLower();
        if (extensao == ".zip")
        {
            // Descompacta
            Archive arquivo = new Archive();
            arquivo.PreservePath = true;
            arquivo.Clear();
            arquivo.Overwrite = Overwrite.Always;

            string login = HttpContext.Current.User.Identity.Name;
            path += login + @"\";

            // Se Diretorio Existe Apaga o Diretorio
            if (Directory.Exists(path))
            {
                Directory.Delete(path, true);
            }

            // Cria o Diretorio
            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }

            try
            {
                arquivo.QuickUnzip(sr, path);

                // Pegar Todos os Arquivos .html e .htm do Diretorio
                DirectoryInfo dir = new DirectoryInfo(path);
                //Console.WriteLine("Directory: {0}", dir.FullName);

                foreach (FileInfo file in dir.GetFiles("*.htm*"))
                { // pega htm e html
                    NotaCorretagem.InfoArquivoHTML infoArquivoHTML = new NotaCorretagem.InfoArquivoHTML(file);
                    infoArquivosHTML.Add(infoArquivoHTML);
                }

                foreach (FileInfo file in dir.GetFiles("*.mht"))
                {
                    NotaCorretagem.InfoArquivoHTML infoArquivoHTML = new NotaCorretagem.InfoArquivoHTML(file);
                    infoArquivosHTML.Add(infoArquivoHTML);
                }

                // Para Cada Arquivo - Pega o Conteudo do Arquivo
                foreach (NotaCorretagem.InfoArquivoHTML infoArquivoHTML in infoArquivosHTML)
                {
                    StreamReader srReader = new StreamReader(infoArquivoHTML.fileInfo.FullName, Encoding.GetEncoding("ISO-8859-1"));
                    string conteudo = srReader.ReadToEnd();
                    srReader.Close();
                    infoArquivoHTML.conteudo = conteudo;
                }
            }
            catch (Exception e3)
            {
                e.CallbackData = "Importação Nota Corretagem - " + e3.Message;
                // Apaga o Diretorio e todos os arquivos criados no Unzip
                if (Directory.Exists(path))
                {
                    Directory.Delete(path, true);
                }
                return;
            }
        }
        #endregion
        #region Arquivo Html
        else if (extensao == ".html" || extensao == ".htm" || extensao == ".mht")
        {
            StreamReader srReader = new StreamReader(sr, Encoding.GetEncoding("ISO-8859-1"));
            NotaCorretagem.InfoArquivoHTML infoArquivoHTML = new NotaCorretagem.InfoArquivoHTML(new FileInfo(nomeArquivo));

            infoArquivoHTML.conteudo = srReader.ReadToEnd();
            srReader.Close();
            infoArquivosHTML.Add(infoArquivoHTML);
        }
        #endregion

        #region Processamento Arquivo
        OrdemBolsa ordem = new OrdemBolsa();
        try
        {
            NotaCorretagem notaCorretagem = new NotaCorretagem();
            List<NotaCorretagem.ResumoOperacoes> r = notaCorretagem.ProcessNotaCorretagem(data, infoArquivosHTML);
            // Carrega Ordem Bolsa
            OrdemBolsa ordemBolsa = new OrdemBolsa();
            ordemBolsa.CarregaOrdemNotaCorretagem(r, data);
        }
        catch (Exception e2)
        {
            e.CallbackData = "Importação Nota Corretagem - " + e2.Message;
            return;
        }
        finally
        { //
            if (extensao == ".zip")
            {
                // Apaga o Diretorio e todos os arquivos criados no Unzip
                if (Directory.Exists(path))
                {
                    Directory.Delete(path, true);
                }
            }
        }
        #endregion
    }
}