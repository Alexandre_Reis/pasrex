﻿using System;
using System.Collections.Generic;
using System.IO;

using EntitySpaces.Interfaces;

using DevExpress.Web;
using Financial.Web.Common;
using Financial.Bolsa;
using Financial.Security;
using Financial.Security.Enums;
using System.Web;
using Financial.Util;
using Financial.Common;
using Financial.Investidor.Enums;
using Financial.Interfaces.Import.Bolsa;

public partial class ImportacaoBasePage : BasePage {

    /// <summary>
    /// 
    /// </summary>
    /// <param name="e"></param>
    /// <param name="dataImportacao"></param>
    [TipoImportacao("ImportaConr", Prefixo = "CONR")]
    protected void ImportaConr(FileUploadCompleteEventArgs e, ASPxDateEdit dataImportacao) {

        ASPxDateEdit textDataArquivos = dataImportacao;

        #region Trata Extensão Valida
        if (!this.isExtensaoValida(e.UploadedFile.FileName.Trim()))
        {
            e.CallbackData = "Arquivo Conr inválido: Extensão do arquivo deve ser .txt, .dat ou nula\n\n";
            return;
        }
        #endregion

        StreamReader sr = new StreamReader(e.UploadedFile.FileContent);
        //
        bool erro = false;

        string msg = "";
        DateTime dataInicio = DateTime.Now;
        //
        Conr conr = new Conr();
        TabelaCONR tabelaCONR = new TabelaCONR();
        DateTime data = Convert.ToDateTime(textDataArquivos.Text);

        // Se estiver integrando de algum sistema, não considera o CONR para importar para ordemBolsa, somente para a carga da TabelaCONR
        IntegracaoBolsa integracaoBolsa = (IntegracaoBolsa)ParametrosConfiguracaoSistema.Integracoes.IntegracaoBolsa;
        bool integraBolsa = integracaoBolsa != IntegracaoBolsa.NaoIntegra;

        OrdemTermoBolsa ordemTermoBolsa = new OrdemTermoBolsa();

        try
        {
            ConrCollection conrCollection = conr.ProcessaConr(sr, data);
            //
            if (!integraBolsa)
            {
                ordemTermoBolsa.CarregaOrdemTermoConr(conrCollection, data);
            }

            tabelaCONR.CarregaTabelaConr(conrCollection, data);
        }
        catch (Exception ex)
        {
            erro = true;
            msg = "Conr com problemas - Dados não Importados: " + ex.Message + "\n\n";
        }

        #region Log
        if (!erro)
        {
            //Log do Processo            
            DateTime dataFim = DateTime.Now;
            //
            HistoricoLog historicoLog = new HistoricoLog();
            historicoLog.Descricao = "Importação CONR - " + data.ToShortDateString();
            historicoLog.Login = HttpContext.Current.User.Identity.Name;
            historicoLog.Maquina = Utilitario.GetLocalIp();
            historicoLog.Cultura = "";
            historicoLog.DataInicio = dataInicio;
            historicoLog.DataFim = dataFim;
            historicoLog.Origem = (int)HistoricoLogOrigem.Outros;
            historicoLog.Save();
        }
        #endregion

        e.CallbackData = msg;
    }
}