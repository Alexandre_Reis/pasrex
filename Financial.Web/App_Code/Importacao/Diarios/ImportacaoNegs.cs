﻿using System;
using System.Collections.Generic;
using System.IO;

using EntitySpaces.Interfaces;

using DevExpress.Web;
using Financial.Web.Common;
using Financial.Bolsa;
using Financial.Security;
using Financial.Security.Enums;
using System.Web;
using Financial.Util;
using Financial.Common;
using Financial.Interfaces.Import.Bolsa;

public partial class ImportacaoBasePage : BasePage {

    /// <summary>
    /// 
    /// </summary>
    /// <param name="e"></param>
    /// <param name="dataImportacao"></param>
    [TipoImportacao("ImportaNegs", Prefixo = "NEGS")]
    protected void ImportaNegs(FileUploadCompleteEventArgs e, ASPxDateEdit dataImportacao) {

        ASPxDateEdit textDataArquivos = dataImportacao;

        #region Trata Extensão Valida
        if (!this.isExtensaoValida(e.UploadedFile.FileName.Trim()))
        {
            e.CallbackData = "Arquivo Negs inválido: Extensão do arquivo deve ser .txt, .dat ou nula\n\n";
            return;
        }
        #endregion

        bool erro = false;
        string msg = "";
        //
        string arquivoNegs = e.UploadedFile.FileName.Trim();
        char[] arquivoCliente = arquivoNegs.ToCharArray();

        string idClienteString = "";
        for (int i = 0; char.IsDigit(arquivoCliente[i]); i++)
        {
            idClienteString += arquivoCliente[i];
        }

        if (String.IsNullOrEmpty(idClienteString))
        {
            erro = true;
            msg = "Negs com problemas - Nome do Arquivo deve conter o Identificador do Cliente. ";
            msg += "\nPor favor Renomear o Arquivo. \n\n";
            //
            e.CallbackData = msg;
            return;
        }
        else
        {
            StreamReader sr = new StreamReader(e.UploadedFile.FileContent);
            //            
            DateTime dataInicio = DateTime.Now;
            Negs negs = new Negs();
            DateTime data = Convert.ToDateTime(textDataArquivos.Text);
            OrdemBolsa ordemBolsa = new OrdemBolsa();

            int idCliente = Convert.ToInt32(idClienteString);

            try
            {
                NegsCollection negsCollection = negs.ProcessaNegs(sr, data);
                ordemBolsa.CarregaOrdemNegs(negsCollection, data, idCliente);
            }
            catch (Exception ex)
            {
                erro = true;
                msg = "Negs com problemas - Dados não Importados: " + ex.Message + "\n\n";
            }

            #region Log
            if (!erro)
            {
                //Log do Processo            
                DateTime dataFim = DateTime.Now;
                //
                HistoricoLog historicoLog = new HistoricoLog();
                historicoLog.Descricao = "Importação NEGS - " + data.ToShortDateString();
                historicoLog.Login = HttpContext.Current.User.Identity.Name;
                historicoLog.Maquina = Utilitario.GetLocalIp();
                historicoLog.Cultura = "";
                historicoLog.DataInicio = dataInicio;
                historicoLog.DataFim = dataFim;
                historicoLog.Origem = (int)HistoricoLogOrigem.Outros;
                historicoLog.Save();
            }
            #endregion

            e.CallbackData = msg;
        }
    }
}