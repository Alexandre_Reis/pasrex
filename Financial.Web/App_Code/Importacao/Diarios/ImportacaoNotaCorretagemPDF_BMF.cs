﻿using System;
using System.Collections.Generic;
using System.IO;

using EntitySpaces.Interfaces;

using DevExpress.Web;
using Financial.Web.Common;
using Financial.Bolsa;
using Financial.Security;
using Financial.Security.Enums;
using System.Web;
using Financial.Util;
using Financial.Common;
using Financial.Interfaces.Import.Bolsa;
using Financial.WebConfigConfiguration;
using System.Text;
using Dart.PowerTCP.Zip;
using Financial.BMF;

public partial class ImportacaoBasePage : BasePage {

    /// <summary>
    /// 
    /// </summary>
    /// <param name="e"></param>
    /// <param name="dataImportacao">Não usada - Desconsiderar</param>
    [TipoImportacao("ImportaNotaCorretagemPDF_BMF", Prefixo = "Notas_Bmf")]
    protected void ImportaNotaCorretagemPDF_BMF(FileUploadCompleteEventArgs e, ASPxDateEdit dataImportacao) {

        e.CallbackData = "";

        #region Trata Extensão Válida
        //if (!this.isExtensaoValidaNotaCorretagemPDF(e.UploadedFile.FileName.Trim())) {
        if (!this.isExtensaoPDF(e.UploadedFile.FileName.Trim())) {
            //e.CallbackData = "Importação Nota Corretagem PDF - Formato de Arquivo Inválido. Formato permitido: .pdf ou zip \n\n";
            e.CallbackData = "Importação Nota Corretagem PDF - Formato de Arquivo Inválido. Formato permitido: .pdf \n\n";
            return;
        }
        #endregion

        #region Trata path do Arquivo zip
        string path = DiretorioAplicacao.DiretorioBaseAplicacao + WebConfig.AppSettings.DiretorioDownloads;
        if (!path.Trim().EndsWith("\\")) {
            path += "\\";
        }
        path = path.Replace("\\", "\\\\");
        #endregion
        
        // Stream de bytes com o Conteudo do Arquivo zip - pdf
        Stream sr = e.UploadedFile.FileContent;

        string nomeArquivo = e.UploadedFile.FileName.Trim();
        string extensao = new FileInfo(nomeArquivo).Extension;
        extensao = extensao.ToLower();
        
        #region Arquivo Zip
        if (extensao == ".zip") {
        //    // Descompacta
        //    Archive arquivo = new Archive();
        //    arquivo.PreservePath = true;
        //    arquivo.Clear();
        //    arquivo.Overwrite = Overwrite.Always;

        //    string login = HttpContext.Current.User.Identity.Name;
        //    path += login + @"\";

        //    // Se Diretorio Existe Apaga o Diretorio
        //    if (Directory.Exists(path)) {
        //        Directory.Delete(path, true);
        //    }

        //    // Cria o Diretorio
        //    if (!Directory.Exists(path)) {
        //        Directory.CreateDirectory(path);
        //    }

        //    try {
        //        arquivo.QuickUnzip(sr, path);

        //        // Pegar Todos os Arquivos .html e .htm do Diretorio
        //        DirectoryInfo dir = new DirectoryInfo(path);
        //        //Console.WriteLine("Directory: {0}", dir.FullName);

        //        foreach (FileInfo file in dir.GetFiles("*.htm*")) { // pega htm e html
        //            NotaCorretagem.InfoArquivoHTML infoArquivoHTML = new NotaCorretagem.InfoArquivoHTML(file);
        //            infoArquivosHTML.Add(infoArquivoHTML);
        //        }

        //        foreach (FileInfo file in dir.GetFiles("*.mht")) {
        //            NotaCorretagem.InfoArquivoHTML infoArquivoHTML = new NotaCorretagem.InfoArquivoHTML(file);
        //            infoArquivosHTML.Add(infoArquivoHTML);
        //        }

        //        // Para Cada Arquivo - Pega o Conteudo do Arquivo
        //        foreach (NotaCorretagem.InfoArquivoHTML infoArquivoHTML in infoArquivosHTML) {
        //            StreamReader srReader = new StreamReader(infoArquivoHTML.fileInfo.FullName, Encoding.GetEncoding("ISO-8859-1"));
        //            string conteudo = srReader.ReadToEnd();
        //            srReader.Close();
        //            infoArquivoHTML.conteudo = conteudo;
        //        }
        //    }
        //    catch (Exception e3) {
        //        e.CallbackData = "Importação Nota Corretagem - " + e3.Message;
        //        // Apaga o Diretorio e todos os arquivos criados no Unzip
        //        if (Directory.Exists(path)) {
        //            Directory.Delete(path, true);
        //        }
        //        return;
        //    }
        }
        #endregion

        #region Arquivo PDF
        else if (extensao == ".pdf") {
        }
        #endregion

        #region Processamento Arquivo
        try {
            new OrdemBMF().CarregaNotaCorretagemPDF(sr);
        }
        catch (Exception e2) {
            e.CallbackData = "Importação Nota Corretagem PDF - " + e2.Message;
            return;
        }
        finally { //
            //if (extensao == ".zip") {
            //    // Apaga o Diretorio e todos os arquivos criados no Unzip
            //    if (Directory.Exists(path)) {
            //        Directory.Delete(path, true);
            //    }
            //}

            // Apaga diretorio arquivo PDF
            //if (Directory.Exists(path)) {
            //    Directory.Delete(path, true);
            //}
        }
        #endregion
    }
    
}