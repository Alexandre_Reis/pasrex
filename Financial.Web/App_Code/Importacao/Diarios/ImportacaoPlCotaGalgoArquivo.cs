﻿using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Financial.Web.Common;
using System.IO;
using Financial.Integracao.Excel.Util;
using Financial.Integracao.Excel;
using EntitySpaces.Interfaces;
using Financial.Common;
using System.Collections.Generic;
using Financial.Investidor.Enums;
using DevExpress.Web;
using Financial.Fundo;
using Financial.Security;
using Financial.Web.Util;
using Financial.Security.Enums;

public partial class ImportacaoBasePage : BasePage {

    /// <summary>
    /// Processa a Planilha de HistoricoCota após ter acabado o Upload
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void ImportaArquivoPlCota(FileUploadCompleteEventArgs e)
    {
        e.CallbackData = "";

        #region Trata Extensão Válida
        if (!this.isExtensaoXML(e.UploadedFile.FileName.Trim()))
        {
            e.CallbackData = "Arquivo PL/Cota Galgo inválido: Extensão do arquivo deve ser .xml. \n\n";
            return;
        }
        #endregion

        StreamReader sr = new StreamReader(e.UploadedFile.FileContent);

        #region Processamento Arquivo XML
        
        try
        {
            Financial.Fundo.Galgo.InterfaceGalgo galgo = new Financial.Fundo.Galgo.InterfaceGalgo();
            galgo.ImportaPlCota(sr, HttpContext.Current.User.Identity.Name);
        }
        catch (Exception e2)
        {
            e.CallbackData = "Importação arquivo PL/Cota Galgo - " + e2.Message;
            return;
        }

        new HistoricoLog().InsereHistoricoLog(DateTime.Now, DateTime.Now,
            "Importação Arquivo PL/Cota Galgo",
            HttpContext.Current.User.Identity.Name, UtilitarioWeb.GetIP(Request), "", HistoricoLogOrigem.Outros);

        #endregion
    }
}
