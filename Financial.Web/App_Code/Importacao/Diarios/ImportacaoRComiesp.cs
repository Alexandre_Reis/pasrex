﻿using System;
using System.Collections.Generic;
using System.IO;

using EntitySpaces.Interfaces;

using DevExpress.Web;
using Financial.Web.Common;
using Financial.Bolsa;
using Financial.Security;
using Financial.Security.Enums;
using System.Web;
using Financial.Util;
using Financial.Common;
using Financial.BMF;
using Financial.Interfaces.Import.BMF;

public partial class ImportacaoBasePage : BasePage {

    /// <summary>
    /// Em Desenvolvimento
    /// </summary>
    /// <param name="e"></param>
    /// <param name="data"></param>    
    [TipoImportacao("ImportaRComiesp", Prefixo = "RComiesp")]
    protected void ImportaRComiesp(FileUploadCompleteEventArgs e, ASPxDateEdit dataImportacao) {

        ASPxDateEdit textDataArquivos = dataImportacao;

        #region Trata Extensão Valida
        if (!this.isExtensaoValida(e.UploadedFile.FileName.Trim()))
        {
            e.CallbackData = "Arquivo RComiesp inválido: Extensão do arquivo deve ser .txt, .dat ou nula\n\n";
            return;
        }
        #endregion

        StreamReader sr = new StreamReader(e.UploadedFile.FileContent);
        //
        bool erro = false;

        string msg = "";
        DateTime dataInicio = DateTime.Now;
        RComiesp rComiesp = new RComiesp();
        DateTime data = Convert.ToDateTime(textDataArquivos.Text);
        OrdemBMF ordemBMF = new OrdemBMF();

        try
        {
            //RComiespCollection rComiespCollection = rComiesp.ProcessaRComiesp(sr, data);
            // Em Desenvolvimento
            //ordemBMF.CarregaOrdemRComiesp(rComiespCollection, data, idCliente);
        }
        catch (Exception ex)
        {
            erro = true;
            msg = ex.Message;
        }
        finally
        {
            erro = true;
            msg = "RComiesp em Desenvolvimento!\n\n";
        }

        #region Log
        if (!erro)
        {
            //Log do Processo            
            DateTime dataFim = DateTime.Now;
            //
            HistoricoLog historicoLog = new HistoricoLog();
            historicoLog.Descricao = "Importação RComiesp - " + data.ToShortDateString();
            historicoLog.Login = HttpContext.Current.User.Identity.Name;
            historicoLog.Maquina = Utilitario.GetLocalIp();
            historicoLog.Cultura = "";
            historicoLog.DataInicio = dataInicio;
            historicoLog.DataFim = dataFim;
            historicoLog.Origem = (int)HistoricoLogOrigem.Outros;
            historicoLog.Save();
        }
        #endregion

        e.CallbackData = msg;
    }
}