﻿using System;
using System.Collections.Generic;
using System.IO;

using EntitySpaces.Interfaces;

using DevExpress.Web;
using Financial.Web.Common;
using Financial.Bolsa;
using Financial.Security;
using Financial.Security.Enums;
using System.Web;
using Financial.Util;
using Financial.Common;
using Financial.Interfaces.Import.Bolsa;

public partial class ImportacaoBasePage : BasePage {

    /// <summary>
    /// 
    /// </summary>
    /// <param name="e"></param>
    /// <param name="data"></param>
    [TipoImportacao("ImportaDbtcPosicao", Prefixo = "DBTCPos")]
    protected void ImportaDbtcPosicao(FileUploadCompleteEventArgs e, ASPxDateEdit dataImportacao) {

        ASPxDateEdit textDataArquivos = dataImportacao;

        #region Trata Extensão Valida
        if (!this.isExtensaoValida(e.UploadedFile.FileName.Trim()))
        {
            e.CallbackData = "Arquivo Dbtc inválido: Extensão do arquivo deve ser .txt, .dat ou nula\n\n";
            return;
        }
        #endregion

        StreamReader sr = new StreamReader(e.UploadedFile.FileContent);
        //
        bool erro = false;

        string msg = "";
        DateTime dataInicio = DateTime.Now;

        Dbtc dbtc = new Dbtc();
        PosicaoEmprestimoBolsa posicaoEmprestimoBolsa = new PosicaoEmprestimoBolsa();
        //
        DateTime data = Convert.ToDateTime(textDataArquivos.Text);

        try
        {
            DbtcCollection dbtcCollection = dbtc.ProcessaDbtc(sr, data);
            posicaoEmprestimoBolsa.CarregaPosicaoEmprestimoDbtc(dbtcCollection, data);
        }
        catch (Exception ex)
        {
            erro = true;
            msg = "Dbtc com problemas - Dados não Importados: " + ex.Message + "\n\n";
        }

        #region
        if (!erro)
        {
            //Log do Processo            
            DateTime dataFim = DateTime.Now;
            //
            HistoricoLog historicoLog = new HistoricoLog();
            historicoLog.Descricao = "Importação DBTC - " + data.ToShortDateString();
            historicoLog.Login = HttpContext.Current.User.Identity.Name;
            historicoLog.Maquina = Utilitario.GetLocalIp();
            historicoLog.Cultura = "";
            historicoLog.DataInicio = dataInicio;
            historicoLog.DataFim = dataFim;
            historicoLog.Origem = (int)HistoricoLogOrigem.Outros;
            historicoLog.Save();
        }
        #endregion

        e.CallbackData = msg;
    }
}