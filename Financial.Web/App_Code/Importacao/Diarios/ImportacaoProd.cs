﻿using DevExpress.Web;
using Financial.Bolsa;
using Financial.Interfaces.Import.Bolsa;
using Financial.Security;
using Financial.Security.Enums;
using Financial.Util;
using Financial.Web.Common;
using System;
using System.IO;
using System.Web;

public partial class ImportacaoBasePage : BasePage
{
    /// <summary>
    /// 
    /// </summary>
    /// <param name="e"></param>
    /// <param name="dataImportacao"></param>
    [TipoImportacao("ImportaProd", Prefixo = "PROD")]
    protected void ImportaProd(FileUploadCompleteEventArgs e, ASPxDateEdit dataImportacao)
    {
        ImportaProd(e, dataImportacao, false);
    }
    /// <summary>
    /// Importas the product.
    /// </summary>
    /// <param name="e">The <see cref="FileUploadCompleteEventArgs"/> instance containing the event data.</param>
    /// <param name="dataImportacao">The data importacao.</param>
    /// <param name="geraException">if set to <c>true</c> [gera exception].</param>
    protected void ImportaProd(FileUploadCompleteEventArgs e, ASPxDateEdit dataImportacao, bool geraException)
    {
        ASPxDateEdit textDataArquivos = dataImportacao;

        #region Trata Extensão Valida
        if (!this.isExtensaoValida(e.UploadedFile.FileName.Trim()))
        {
            e.CallbackData = "Arquivo Prod inválido: Extensão do arquivo deve ser .txt, .dat ou nula\n\n";
            return;
        }
        #endregion

        StreamReader sr = new StreamReader(e.UploadedFile.FileContent);
        //
        bool erro = false;

        string msg = "";
        DateTime dataInicio = DateTime.Now;

        Prod prod = new Prod();
        DateTime data = Convert.ToDateTime(textDataArquivos.Text);
        ProventoBolsa proventoBolsa = new ProventoBolsa();
        GrupamentoBolsa grupamentoBolsa = new GrupamentoBolsa();
        ConversaoBolsa conversaoBolsa = new ConversaoBolsa();
        BonificacaoBolsa bonificacaoBolsa = new BonificacaoBolsa();
        SubscricaoBolsa subscricaoBolsa = new SubscricaoBolsa();

        try
        {
            ProdCollection prodCollection = prod.ProcessaProd(sr, data);
            //
            grupamentoBolsa.CarregaProdGrupamento(prodCollection, data);
            //
            conversaoBolsa.CarregaProdConversao(prodCollection, data, geraException);
            //
            bonificacaoBolsa.CarregaProdBonificacao(prodCollection, data);
            //
            subscricaoBolsa.CarregaProdSubscricao(prodCollection, data, geraException);
            //
            proventoBolsa.CarregaProdProvento(prodCollection, data);
        }
        catch (Exception ex)
        {
            erro = true;
            msg = "Prod com problemas - Dados não Importados: " + ex.Message + "\n\n";
        }

        #region Log
        if (!erro)
        {
            //Log do Processo            
            DateTime dataFim = DateTime.Now;
            //
            HistoricoLog historicoLog = new HistoricoLog();
            historicoLog.Descricao = "Importação PROD - " + data.ToShortDateString();
            historicoLog.Login = HttpContext.Current.User.Identity.Name;
            historicoLog.Maquina = Utilitario.GetLocalIp();
            historicoLog.Cultura = "";
            historicoLog.DataInicio = dataInicio;
            historicoLog.DataFim = dataFim;
            historicoLog.Origem = (int)HistoricoLogOrigem.Outros;
            historicoLog.Save();
        }
        #endregion

        e.CallbackData = msg;
    }
}