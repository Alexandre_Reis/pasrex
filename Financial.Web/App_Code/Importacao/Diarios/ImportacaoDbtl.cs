﻿using System;
using System.Collections.Generic;
using System.IO;

using EntitySpaces.Interfaces;

using DevExpress.Web;
using Financial.Web.Common;
using Financial.Bolsa;
using Financial.Security;
using Financial.Security.Enums;
using System.Web;
using Financial.Util;
using Financial.Common;
using Financial.Interfaces.Import.Bolsa;

public partial class ImportacaoBasePage : BasePage {

    /// <summary>
    /// 
    /// </summary>
    /// <param name="e"></param>
    /// <param name="dataImportacao"></param>
     [TipoImportacao("ImportaDbtl", Prefixo = "DBTL")]
    protected void ImportaDbtl(FileUploadCompleteEventArgs e, ASPxDateEdit dataImportacao) {

        ASPxDateEdit textDataArquivos = dataImportacao;

        #region Trata Extensão Valida
        if (!this.isExtensaoValida(e.UploadedFile.FileName.Trim()))
        {
            e.CallbackData = "Arquivo Dbtl inválido: Extensão do arquivo deve ser .txt, .dat ou nula\n\n";
            return;
        }
        #endregion

        StreamReader sr = new StreamReader(e.UploadedFile.FileContent);
        //
        bool erro = false;

        string msg = "";
        DateTime dataInicio = DateTime.Now;
        //
        Dbtl dbtl = new Dbtl();
        DateTime data = Convert.ToDateTime(textDataArquivos.Text);
        LiquidacaoEmprestimoBolsa liquidacaoEmprestimoBolsa = new LiquidacaoEmprestimoBolsa();
        //
        try
        {
            DbtlCollection dbtlCollection = dbtl.ProcessaDbtl(sr, data);
            liquidacaoEmprestimoBolsa.CarregaLiquidacaoEmprestimoDbtl(dbtlCollection, data);
        }
        catch (Exception ex)
        {
            erro = true;
            msg = "Dbtl com problemas - Dados não Importados: " + ex.Message + "\n\n";
        }

        #region Log
        if (!erro)
        {
            //Log do Processo            
            DateTime dataFim = DateTime.Now;
            //
            HistoricoLog historicoLog = new HistoricoLog();
            historicoLog.Descricao = "Importação DBTL - " + data.ToShortDateString();
            historicoLog.Login = HttpContext.Current.User.Identity.Name;
            historicoLog.Maquina = Utilitario.GetLocalIp();
            historicoLog.Cultura = "";
            historicoLog.DataInicio = dataInicio;
            historicoLog.DataFim = dataFim;
            historicoLog.Origem = (int)HistoricoLogOrigem.Outros;
            historicoLog.Save();
        }
        #endregion

        e.CallbackData = msg;
    }
}