﻿using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Financial.Web.Common;
using System.IO;
using Financial.Integracao.Excel.Util;
using Financial.Integracao.Excel;
using EntitySpaces.Interfaces;
using Financial.Common;
using Financial.Investidor;
using System.Collections.Generic;
using Financial.Investidor.Enums;
using DevExpress.Web;
using Financial.Util;
using Financial.RendaFixa;
using Financial.RendaFixa.Enums;
using Financial.Common.Enums;
using Financial.Fundo;
using System.Globalization;
using Financial.CRM;
using Financial.Fundo.Enums;
using Financial.ContaCorrente;
using System.Data.SqlClient;
using Financial.Interfaces.Import.YMF;

public partial class ImportacaoBasePage : BasePage {
    
    /// <summary>
    /// Processa o arquivo YMF após ter acabado o Upload
    /// </summary>
    /// <param name="e"></param>
    protected void importaCotasYMFPaulista()
    {
        CarteiraCollection fundos = new CarteiraCollection();
        fundos.BuscaCarteirasFundosYMF(true);

        foreach (Carteira fundo in fundos)
        {
            fundo.ImportaCotasYMF();
        }
    }
}