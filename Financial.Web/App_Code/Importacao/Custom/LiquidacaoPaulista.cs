﻿using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Financial.Web.Common;
using System.IO;
using Financial.Integracao.Excel.Util;
using Financial.Integracao.Excel;
using EntitySpaces.Interfaces;
using Financial.Common;
using Financial.Bolsa;
using Financial.Bolsa.Enums;
using Financial.Investidor;
using System.Collections.Generic;
using Financial.Investidor.Enums;
using DevExpress.Web;
using Financial.Common.Exceptions;
using Financial.Bolsa.Exceptions;
using Financial.ContaCorrente;
using Financial.ContaCorrente.Enums;
using Financial.Util;


public partial class ImportacaoBasePage : BasePage
{

    private enum TipoDataVencimento
    {
        DataFutura,
        DataLancamento
    }

    /// <summary>
    /// Leitura de um arquivo Excel
    /// Saida: List<ValoresExcel> com os valores armazenados no arquivo
    /// </summary>
    /// <param name="streamExcel">Stream de bytes de um arquivo Excel</param>
    /// <exception cref="Exception">Se ocorreu problemas</exception>   
    private void LerArquivoLiquidacaoPaulista(Stream streamExcel)
    {
        int LINHA_INICIAL = 24;

        #region Leitura do Arquivo Excel

        // Open Spreadsheet
        Bytescout.Spreadsheet.Spreadsheet document = new Bytescout.Spreadsheet.Spreadsheet();
        document.LoadFromStream(streamExcel);

        Bytescout.Spreadsheet.Worksheet workSheet = document.Workbook.Worksheets[0];

        ColunaLiquidacaoPaulista[] colunasLiquidacaoPaulista = new ColunaLiquidacaoPaulista[15];
        colunasLiquidacaoPaulista[0] = new ColunaLiquidacaoPaulista("Id Cliente", "", false, false, 0, false, 0, 0);
        colunasLiquidacaoPaulista[1] = new ColunaLiquidacaoPaulista("Data Lançamento", "", false, false, 0, false, 0, 0);
        colunasLiquidacaoPaulista[2] = new ColunaLiquidacaoPaulista("Cheques Custódia", "Cheques Custódia", true, true, (int)TipoDataVencimento.DataFutura,
            true, 0, 0);
        colunasLiquidacaoPaulista[3] = new ColunaLiquidacaoPaulista("Cheques Devolvidos", "Cheques Devolvidos", true, true, (int)TipoDataVencimento.DataFutura,
            true, 0, 0);
        colunasLiquidacaoPaulista[4] = new ColunaLiquidacaoPaulista("Cobrança D+0", "Cobrança D+0", false, false, 0, false, 0, 0);
        colunasLiquidacaoPaulista[5] = new ColunaLiquidacaoPaulista("Cobrança D+1", "Cobrança D+1", false, false, 0, false, 0, 0);
        colunasLiquidacaoPaulista[6] = new ColunaLiquidacaoPaulista("Depósitos", "Depósitos", true, false, (int)TipoDataVencimento.DataFutura, true, 0, 0);

        colunasLiquidacaoPaulista[7] = new ColunaLiquidacaoPaulista("Depósito em Cheque", "Depósito em Cheque", true, true, (int)TipoDataVencimento.DataFutura,
            true, 0, 0);
        colunasLiquidacaoPaulista[8] = new ColunaLiquidacaoPaulista("transf.ctas", "", false, false, 0, false, 0, 0);
        colunasLiquidacaoPaulista[9] = new ColunaLiquidacaoPaulista("Outros", "", false, false, 0, false, 0, 0);
        colunasLiquidacaoPaulista[10] = new ColunaLiquidacaoPaulista("Tarifas ( Desp.Bancárias)", "Tarifas / Despesas Bancárias", true, true,
            (int)TipoDataVencimento.DataLancamento, false, 0, 0);
        colunasLiquidacaoPaulista[11] = new ColunaLiquidacaoPaulista("Teds Compra do dia", "", false, false, 0, false, 0, 0);
        colunasLiquidacaoPaulista[12] = new ColunaLiquidacaoPaulista("Teds Compra anterior", "", false, false, 0, false, 0, 0);
        colunasLiquidacaoPaulista[13] = new ColunaLiquidacaoPaulista("Diferença na Remessa", "Diferença na Remessa", true, true,
            (int)TipoDataVencimento.DataFutura, true, 0, 0);
        colunasLiquidacaoPaulista[14] = new ColunaLiquidacaoPaulista("Diferença na Baixa", "Diferença na Baixa", true, true,
            (int)TipoDataVencimento.DataFutura, true, 0, 0);

        #region Le descricao do header a ser utilizada no lançamento
        for (int i = 0; i < colunasLiquidacaoPaulista.Length; i++)
        {
            colunasLiquidacaoPaulista[i].Descricao = workSheet.Cell(LINHA_INICIAL, i).ValueAsString;
        }
        #endregion

        DateTime DATA_VENCIMENTO_FUTURA = new DateTime(4000, 12, 31);

        int linha = LINHA_INICIAL + 1;
        int coluna1 = 0;
        try
        {
            // Enquanto IdCliente tiver valor
            while (workSheet.Cell(linha, coluna1).Value != null)
            {
                int idCliente = workSheet.Cell(linha, coluna1).ValueAsInteger;
                DateTime dataLancamento = workSheet.Cell(linha, coluna1 + 1).ValueAsDateTime;

                //fazer loop por colunas pois cada coluna representa um lançamento !
                for (int countColuna = 0; countColuna < colunasLiquidacaoPaulista.Length; countColuna++)
                {
                    ColunaLiquidacaoPaulista colunaLiquidacaoPaulista = colunasLiquidacaoPaulista[countColuna];
                    if (!colunaLiquidacaoPaulista.Importa)
                    {
                        continue;
                    }

                    object valor = workSheet.Cell(linha, countColuna).Value;
                    if (valor == null)
                    {
                        continue;
                    }

                    ValoresExcelLiquidacao item;

                    //Descobrir se este é um lançamento avulso ou que se totaliza
                    if (colunaLiquidacaoPaulista.ConsolidaValores)
                    {
                        //Se ele se totaliza, vamos utilizar uma estrutura temporária de totalização da própria coluna
                        item = colunaLiquidacaoPaulista.ValoresExcelLiquidacaoAcumulado;
                        item.Descricao = colunaLiquidacaoPaulista.Descricao;
                    }
                    else
                    {
                        item = new ValoresExcelLiquidacao();
                        //Se são lançamentos avulsos, precisamos usar um número sequencial para numerar e identificar
                        item.Descricao = colunaLiquidacaoPaulista.Descricao + " " + (++colunaLiquidacaoPaulista.Sequencial);
                    }

                    item.IdCliente = idCliente;
                    item.DataLancamento = dataLancamento;
                    item.TipoSaldo = EnumTipoSaldo.Normal;
                    item.DataVencimento = item.DataLancamento;
                    item.Situacao = (byte)SituacaoLancamentoLiquidacao.Pendente;

                    Decimal valorDecimal = Math.Abs(Convert.ToDecimal(valor));
                    if (colunaLiquidacaoPaulista.ContraPartida)
                    {
                        //Se tem contra partida o valor do dia do lançamento é sempre positivo
                        item.Valor += valorDecimal;
                    }
                    else
                    {
                        //Se não tem contra partida precisamos lançar um valor negativo no dia do lançamento
                        item.Valor -= valorDecimal;
                    }

                    if (!colunaLiquidacaoPaulista.ConsolidaValores)
                    {
                        //Se é um lançamento analitico (avulso), inclui-lo na colecao
                        this.valoresExcelLiquidacao.Add(item);

                        //Precisamos lançar a contra partida futura
                        if (colunaLiquidacaoPaulista.ContraPartida)
                        {
                            ValoresExcelLiquidacao itemClone = (ValoresExcelLiquidacao)Utilitario.Clone(item);
                            itemClone.Valor = -(item.Valor);
                            itemClone.DataVencimento = DATA_VENCIMENTO_FUTURA;
                            this.valoresExcelLiquidacao.Add(itemClone);
                        }
                    }
                }

                linha++;
            }

            //Precisamos agora incluir na lista todos os lançamentos que foram acumulados
            foreach (ColunaLiquidacaoPaulista colunaLiquidacaoPaulista in colunasLiquidacaoPaulista)
            {
                if (colunaLiquidacaoPaulista.Importa)
                {
                    ValoresExcelLiquidacao item = colunaLiquidacaoPaulista.ValoresExcelLiquidacaoAcumulado;
                    if (item.IdCliente > 0)
                    {
                        this.valoresExcelLiquidacao.Add(item);
                        
                        //Precisamos lançar a contra partida futura
                        if (colunaLiquidacaoPaulista.ContraPartida)
                        {
                            ValoresExcelLiquidacao itemClone = (ValoresExcelLiquidacao)Utilitario.Clone(item);
                            itemClone.Valor = -(item.Valor);
                            itemClone.DataVencimento = DATA_VENCIMENTO_FUTURA;
                            this.valoresExcelLiquidacao.Add(itemClone);
                        }
                    }
                }
            }

        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
        finally
        {
            document.Close();
            document.Dispose();
        }
        #endregion


    }

    protected void uplLiquidacaoPaulista_FileUploadComplete(FileUploadCompleteEventArgs e)
    {
        e.CallbackData = "";

        #region Trata Extensão Válida
        //if (!this.isExtensaoExcelCSV(e.UploadedFile.FileName.Trim())) {
        if (!ValoresExcelUtil.isExtensaoExcel(e.UploadedFile.FileName.Trim()))
        {
            e.CallbackData = "Importação Liquidação - Formato de Arquivo Inválido. Formato permitido: .xls ou .xlsx\n\n";
            return;
        }
        #endregion

        // Stream de bytes com o Conteudo do Arquivo Excel
        Stream sr = e.UploadedFile.FileContent;

        #region Processamento Arquivo Excel

        //Reseta Vetor de Dados
        this.valoresExcelLiquidacao = new List<ValoresExcelLiquidacao>();
        //
        try
        {
            // Ler Arquivo
            this.LerArquivoLiquidacaoPaulista(sr);
            // Vamos usar o metodo de liquidacao genérico (nao precisamos duplicar o codigo da insercao na tabela de liquia
            this.CarregaLiquidacao(false);
        }
        catch (Exception e2)
        {
            e.CallbackData = "Importação Liquidação - " + e2.Message;
            return;
        }
        #endregion
    }


    private class ColunaLiquidacaoPaulista
    {
        public string Header;
        public string Descricao;
        public bool Importa;
        public bool ConsolidaValores; //Indica se vamos ter 1 linha pra cada lançamento ou sumarizar em um único lançamento
        public int Sequencial; //Variavel temporaria para acumular o numero sequencial caso nao estejamos consolidando valores (ex.: Cheque 1, Cheque 2, etc.)
        public int TipoDataVencimento;
        public bool ContraPartida;
        public int CodigoContabilCredor;
        public int CodigoContabilDevedor;
        public ValoresExcelLiquidacao ValoresExcelLiquidacaoAcumulado; //Estrutura para manter o valor acumulado para os que consolidam valores

        public ColunaLiquidacaoPaulista(string header, string descricao, bool importa, bool consolidaValores,
            int tipoDataVencimento, bool contraPartida, int codigoContabilCredor, int codigoContabilDevedor)
        {
            this.Header = header;
            this.Descricao = descricao;
            this.Importa = importa;
            this.ConsolidaValores = consolidaValores;
            this.TipoDataVencimento = tipoDataVencimento;
            this.ContraPartida = contraPartida;
            this.CodigoContabilCredor = codigoContabilCredor;
            this.CodigoContabilDevedor = codigoContabilDevedor;
            this.ValoresExcelLiquidacaoAcumulado = new ValoresExcelLiquidacao();
        }
    }

}