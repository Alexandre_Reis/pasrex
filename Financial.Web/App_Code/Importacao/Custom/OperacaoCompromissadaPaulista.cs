﻿using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Financial.Web.Common;
using System.IO;
using Financial.Integracao.Excel.Util;
using Financial.Integracao.Excel;
using EntitySpaces.Interfaces;
using Financial.Common;
using Financial.Investidor;
using System.Collections.Generic;
using Financial.Investidor.Enums;
using DevExpress.Web;
using Financial.Util;
using Financial.RendaFixa;
using Financial.RendaFixa.Enums;
using Financial.Common.Enums;
using Financial.Fundo;

public partial class ImportacaoBasePage : BasePage {

    /* Estrutura do Excel */
    private List<ValoresExcelOperacaoCompromissada> valoresExcelOperacaoCompromissada = new List<ValoresExcelOperacaoCompromissada>();

    /// <summary>
    /// Leitura de um arquivo Excel
    /// Saida: List<ValoresExcel> com os valores armazenados no arquivo
    /// </summary>
    /// <param name="streamExcel">Stream de bytes de um arquivo Excel</param>
    /// <exception cref="Exception">Se ocorreu problemas</exception>   
    private void LerArquivoOperacaoCompromissada(Stream streamExcel) {

        #region Leitura do Arquivo Excel

        // Open Spreadsheet
        Bytescout.Spreadsheet.Spreadsheet document = new Bytescout.Spreadsheet.Spreadsheet();
        document.LoadFromStream(streamExcel);

        Bytescout.Spreadsheet.Worksheet workSheet = document.Workbook.Worksheets[0];

        //
        #region Confere Formato Arquivo
        string[] colunasConferencias = new string[] 
                            {"nIdOper",
                             "sTipoOperComp",
                             "nNumEmpresa",
                             "sNomeEmpresa",
                             "nNumContraparte", 
                             "sNomeContraparte", 
                             "sCodTit", 
                             "sCodAtivo", 
                             "dtDtVctoPapel",
                             "dtDtEmissao", 
                             "dtDtIni",
                             "dtDtVctoOper", 
                             "nPrazoDu",
                             "sIdTipoIndexador",
                             "nPctIndexador",
                             "sCodCarteira",
                             "nTaxa",
                             "nQtd",
                             "nVlrAplicado",
                             "nDespRendaApropTotal",
                             "nResgateBruto",
                             "nDespRendaApropEfetiva",
                             "nVlrAtual",
                             "nDespRendaAprop",
                             "nOrdem",
                             "dtDtInicio",
                             "dtDtFim",
                             "dtDtRef",
                             "sIdSitOper"
                            };

        bool formato = ValoresExcelUtil.isExcelFormatoValido(colunasConferencias, workSheet);

        if (!formato) {
            string mensagem = "Formato Interno do Arquivo Inválido.\n";
            mensagem += "Linha 1 do Arquivo deve ser: \n";
            for (int i = 0; i < colunasConferencias.Length; i++) {
                int coluna = i + 1;
                mensagem += "\tColuna " + coluna + ": " + colunasConferencias[i] + "\n";
            }

            document.Close();
            document.Dispose();

            throw new Exception(mensagem);
        }
        #endregion

        //
        int index = 0;
        // This row,column index should be changed as per your need.
        // i.e. which cell in the excel you are interesting to read.
        //
        int linha = 1;
        int coluna1 = 0, coluna2 = 1, coluna3 = 2,
            coluna4 = 3, coluna5 = 4, coluna6 = 5,
            coluna7 = 6, coluna8 = 7, coluna9 = 8,
            coluna10 = 9, coluna11 = 10, coluna12 = 11,
            coluna13 = 12, coluna14 = 13, coluna15 = 14,
            coluna16 = 15, coluna17 = 16, coluna18 = 17,
            coluna19 = 18, coluna20 = 19, coluna21 = 20,
            coluna22 = 21, coluna23 = 22, coluna24 = 23,
            coluna25 = 24, coluna26 = 25, coluna27 = 26,
            coluna28 = 27, coluna29 = 28, coluna30 = 29;
            
        //            
        try {
            // Enquanto idCliente tiver valor
            while (workSheet.Cell(linha, coluna1).Value != null) {
                ValoresExcelOperacaoCompromissada item = new ValoresExcelOperacaoCompromissada();
                //
                item.nIdOper = workSheet.Cell(linha, coluna1).ValueAsInteger;
                item.sTipoOperComp = workSheet.Cell(linha, coluna2).ValueAsString.Trim();
                item.nNumEmpresa = workSheet.Cell(linha, coluna3).ValueAsInteger;
                item.sNomeEmpresa = workSheet.Cell(linha, coluna4).ValueAsString.Trim();
                item.nNumContraparte = workSheet.Cell(linha, coluna5).ValueAsInteger;
                item.sNomeContraparte = workSheet.Cell(linha, coluna6).ValueAsString.Trim();
                item.sCodTit = workSheet.Cell(linha, coluna7).ValueAsString.Trim();
                item.sCodAtivo = workSheet.Cell(linha, coluna8).ValueAsString.Trim();
                item.dtDtVctoPapel = workSheet.Cell(linha, coluna9).ValueAsDateTime;
                item.dtDtEmissao = workSheet.Cell(linha, coluna10).ValueAsDateTime;
                item.dtDtIni = workSheet.Cell(linha, coluna11).ValueAsDateTime;
                item.dtDtVctoOper = workSheet.Cell(linha, coluna12).ValueAsDateTime;
                item.nPrazoDu = workSheet.Cell(linha, coluna13).ValueAsInteger;
                item.sIdTipoIndexador = workSheet.Cell(linha, coluna14).ValueAsString.Trim();
                item.nPctIndexador = workSheet.Cell(linha, coluna15).ValueAsInteger;
                item.sCodCarteira = workSheet.Cell(linha, coluna16).ValueAsString.Trim();
                item.nTaxa = Convert.ToDecimal(workSheet.Cell(linha, coluna17).Value);
                item.nQtd = workSheet.Cell(linha, coluna18).ValueAsInteger;                
                item.nVlrAplicado = Convert.ToDecimal(workSheet.Cell(linha, coluna19).Value);
                item.nDespRendaApropTotal = Convert.ToDecimal(workSheet.Cell(linha, coluna20).Value);
                item.nResgateBruto = Convert.ToDecimal(workSheet.Cell(linha, coluna21).Value);

                if (workSheet.Cell(linha, coluna22).Value != null) {
                    item.nDespRendaApropEfetiva = workSheet.Cell(linha, coluna22).ValueAsInteger;
                }
                                
                item.nVlrAtual = Convert.ToDecimal(workSheet.Cell(linha, coluna23).Value);
                item.nDespRendaAprop = Convert.ToDecimal(workSheet.Cell(linha, coluna24).Value);
                item.nOrdem = workSheet.Cell(linha, coluna25).ValueAsInteger;
                item.dtDtInicio = workSheet.Cell(linha, coluna26).ValueAsDateTime;
                item.dtDtFim = workSheet.Cell(linha, coluna27).ValueAsDateTime;
                item.dtDtRef = workSheet.Cell(linha, coluna28).ValueAsDateTime;
                item.sIdSitOper = workSheet.Cell(linha, coluna29).ValueAsString.Trim();                
                //
                this.valoresExcelOperacaoCompromissada.Add(item);
                //
                index++;
                linha = 1 + index;
                //                
                //Console.WriteLine("{0},{1},{2},{3},{4},{5},{6},{7},{8},{9},{10},{11},{12}",
                //    item.IdCliente, item.IdTitulo, item.DataOperacao,
                //    item.TipoOperacao, item.Quantidade,
                //    item.PuOperacao, item.Valor, item.TaxaOperacao, item.Categoria,
                //    item.DataVolta, item.TaxaVolta, item.PuVolta, item.ValorVolta);
            }
        }
        catch (Exception ex) {
            document.Close();
            document.Dispose();

            throw new Exception(ex.Message);
        }
        #endregion

        document.Close();
        document.Dispose();
    }

    /// <summary>
    /// Gera e carrega as Operações RendaFixa de acordo com o objeto List<ValoresExcel>
    /// </summary>
    /// <exception cref="Exception">Se ocorreu problemas</exception>
    private void CarregaOperacaoCompromissada() 
    {
        OperacaoRendaFixaCollection operacaoRendaFixaCollection = new OperacaoRendaFixaCollection();
        TituloRendaFixaCollection tituloRendaFixaCollection = new TituloRendaFixaCollection();
        //                
        List<ValoresExcelOperacaoCompromissada> l = this.valoresExcelOperacaoCompromissada.FindAll(
                                                            delegate(ValoresExcelOperacaoCompromissada a) {
                                                                return a.sTipoOperComp.ToUpper() != "CRV";
                                                            }
                                                    );

        for (int i = 0; i < l.Count; i++) 
        {
            ValoresExcelOperacaoCompromissada valoresExcel = l[i];

            if (valoresExcel.sIdTipoIndexador.Trim().ToUpper() != "PRE") 
            {
                continue;
            }
            
            #region OperacaoRendaFixa
                       
            #region Cliente Inexistente

            ClienteRendaFixa c = new ClienteRendaFixa();
            c.Query.Select(c.Query.IdCliente).Where(c.Query.CodigoInterface == valoresExcel.nNumContraparte.ToString());

            int? idCliente = null;
            if (c.Query.Load()) 
            {
                idCliente = c.IdCliente.Value;
            }
            else 
            {
                continue;
            } 

            #endregion

            #region Confere dataDia Cliente
            Cliente cliente = new Cliente();
            List<esQueryItem> campos = new List<esQueryItem>();
            campos.Add(cliente.Query.IdCliente);
            campos.Add(cliente.Query.DataDia);
            campos.Add(cliente.Query.Status);

            //
            cliente.LoadByPrimaryKey(campos, idCliente.Value);
            DateTime dataDiaCliente = cliente.DataDia.Value;

            if (valoresExcel.dtDtIni < dataDiaCliente) {
                throw new Exception("Data Operação (" + valoresExcel.dtDtIni.ToShortDateString() + ") menor que Data Dia (" + dataDiaCliente.ToShortDateString() + ") do Cliente");
            }
            else {
                if (valoresExcel.dtDtIni == dataDiaCliente) {
                    if (cliente.Status == (byte)StatusCliente.Divulgado) {
                        throw new Exception("Cliente " + idCliente + " com status Fechado em " + dataDiaCliente.ToShortDateString());
                    }
                }
            }
            #endregion

            //
            OperacaoRendaFixa operacao = operacaoRendaFixaCollection.AddNew();
            //

            #region Testa Titulo existente

            TituloRendaFixaCollection titulo = new TituloRendaFixaCollection();
            titulo.Query.Select(titulo.Query.IdTitulo)
                        .Where(titulo.Query.CodigoCustodia == valoresExcel.sCodAtivo,
                               titulo.Query.DataVencimento == valoresExcel.dtDtVctoPapel,
                               titulo.Query.DataEmissao == valoresExcel.dtDtEmissao);

            //
            if (!titulo.Query.Load()) {
                #region Titulo não existente - Cadastra o Titulo

                TituloRendaFixa tituloRendaFixa = tituloRendaFixaCollection.AddNew();
                //tituloRendaFixa.IdIndice =
                //tituloRendaFixa.IdSerie =
                //tituloRendaFixa.CodigoCDA =

                #region Emissor
                EmissorCollection emissorCollection = new EmissorCollection();
                emissorCollection.Query.Select(emissorCollection.Query.IdEmissor);
                //
                emissorCollection.Query.Where(emissorCollection.Query.Nome.Like("%TESOURO%"));
                emissorCollection.Query.Load();
                //
                #endregion

                tituloRendaFixa.IdEmissor = emissorCollection[0].IdEmissor.Value;
                tituloRendaFixa.Descricao = valoresExcel.sCodTit;
                tituloRendaFixa.Taxa = 0;
                tituloRendaFixa.Percentual = 0;
                tituloRendaFixa.DataEmissao = valoresExcel.dtDtEmissao;
                tituloRendaFixa.DataVencimento = valoresExcel.dtDtVctoPapel;
                tituloRendaFixa.ValorNominal = 1000;
                tituloRendaFixa.CodigoCustodia = valoresExcel.sCodAtivo;
                tituloRendaFixa.PUNominal = 1000;
                //
                tituloRendaFixa.DescricaoCompleta = tituloRendaFixa.Descricao.Trim() + " - Vcto: " + tituloRendaFixa.DataVencimento.Value.ToString("dd-MM-yyyy");
                tituloRendaFixa.IsentoIR = (byte)TituloIsentoIR.NaoIsento;
                tituloRendaFixa.IsentoIOF = (byte)TituloIsentoIOF.NaoIsento;
                tituloRendaFixa.IdMoeda = (int)ListaMoedaFixo.Real;
                //                                               
                #region Estrategia
                EstrategiaCollection estrategiaCollection = new EstrategiaCollection();
                estrategiaCollection.Query.Select(estrategiaCollection.Query.IdEstrategia);
                estrategiaCollection.Query.Where(estrategiaCollection.Query.Descricao.Like("%Renda Fixa%"));
                estrategiaCollection.Query.Load();

                if (estrategiaCollection.Count > 0 && estrategiaCollection[0].IdEstrategia.HasValue) {
                    tituloRendaFixa.IdEstrategia = estrategiaCollection[0].IdEstrategia.Value;
                }
                #endregion
                //
                
                #region IdPapel
                int classe = this.RetornaClasse(Convert.ToInt32(tituloRendaFixa.CodigoCustodia));
                string descricao = "";
                if(classe == (int)ClasseRendaFixa.LFT) {
                    descricao = "LFT";
                }
                else if(classe == (int)ClasseRendaFixa.LTN) {
                    descricao = "LTN";
                }
                else if(classe == (int)ClasseRendaFixa.NTN) {
                    if(Utilitario.Right(valoresExcel.sCodTit,1).ToUpper() == "B") {
                        descricao = "NTN-B";
                    }
                    else if(Utilitario.Right(valoresExcel.sCodTit,1).ToUpper() == "C") {
                        descricao = "NTN-C";
                    }
                    else if(Utilitario.Right(valoresExcel.sCodTit,1).ToUpper() == "F") {
                        descricao = "NTN-F";
                    }
                }

                PapelRendaFixaCollection pCollection = new PapelRendaFixaCollection();
                pCollection.Query.Select(pCollection.Query.IdPapel);
                pCollection.Query.Where(pCollection.Query.Descricao.ToUpper() == descricao);
                pCollection.Query.Load();

                if (pCollection.Count > 0 && pCollection[0].IdPapel.HasValue) {
                    tituloRendaFixa.IdPapel = pCollection[0].IdPapel.Value;
                }
                #endregion

                #endregion
            }
            else {
                operacao.IdTitulo = titulo[0].IdTitulo.Value;
            }                           
            #endregion

            operacao.IdCliente = idCliente;            
            operacao.DataOperacao = valoresExcel.dtDtIni;
            operacao.DataLiquidacao = operacao.DataOperacao.Value;
            operacao.TipoOperacao = (byte)TipoOperacaoTitulo.CompraRevenda;
            operacao.Quantidade = valoresExcel.nQtd;
            operacao.Valor = valoresExcel.nVlrAplicado;
            operacao.ValorLiquido = operacao.Valor.Value;
            operacao.PUOperacao = operacao.Valor.Value / operacao.Quantidade.Value;
            //
            operacao.DataVolta = valoresExcel.dtDtVctoOper;
            operacao.TaxaVolta = valoresExcel.nTaxa;
            operacao.ValorVolta = valoresExcel.nResgateBruto;
            operacao.PUVolta = Utilitario.Truncate( (operacao.ValorVolta.Value / operacao.Quantidade.Value), 8);
            //
            operacao.TipoNegociacao = (byte)TipoNegociacaoTitulo.Negociacao;
            //           
            //   
            operacao.Fonte = (byte)FonteOperacaoTitulo.Manual;
            //            
            operacao.TaxaOperacao = 0;
            operacao.TaxaNegociacao = 0;
            //
            operacao.Rendimento = 0;
            operacao.ValorIR = 0;
            operacao.ValorIOF = 0;
            //

            int tipoPapel = titulo[0].UpToPapelRendaFixaByIdPapel.TipoPapel.Value;
            operacao.IdLiquidacao = (tipoPapel == (int)TipoPapelTitulo.Privado ? (byte)ClearingFixo.Cetip : (byte)ClearingFixo.Selic);
            operacao.IdCustodia = (tipoPapel == (int)TipoPapelTitulo.Privado ? (byte)LocalCustodiaFixo.Cetip : (byte)LocalCustodiaFixo.Selic);
            //          
            #endregion
        }

        using (esTransactionScope scope = new esTransactionScope()) {
           
            // Salva Titulos - vai ser gerado um novo IdTitulo
            tituloRendaFixaCollection.Save();

            int j = 0;
            // Seta os novos idtitulo na Operação Renda Fixa
            for (int i = 0; i < operacaoRendaFixaCollection.Count; i++) {
                if (!operacaoRendaFixaCollection[i].IdTitulo.HasValue) {
                    operacaoRendaFixaCollection[i].IdTitulo = tituloRendaFixaCollection[j].IdTitulo.Value;
                    j++;
                }                
            }
            operacaoRendaFixaCollection.Save();
            //
           
            scope.Complete();
        }
    }

    /// <summary>
    /// Baseado no codigo de Custodia do Titulo retorna a classe do papel.
    /// </summary>
    /// <param name="codigocustodia"></param>
    public int RetornaClasse(int codigocustodia) {
        if (codigocustodia == 210100) return (int)ClasseRendaFixa.LFT;
        if (codigocustodia == 100000) return (int)ClasseRendaFixa.LTN;
        if (codigocustodia == 760199 || codigocustodia == 950199 || codigocustodia == 770100) return (int)ClasseRendaFixa.NTN;

        return 0;
    }

    /// <summary>
    /// Processa a Planilha de OperacaoCompromissada após ter acabado o Upload
    /// </summary>
    /// <param name="e"></param>
    protected void uplOperacaoCompromissada_FileUploadComplete(FileUploadCompleteEventArgs e) {
        e.CallbackData = "";

        #region Trata Extensão Válida
        //if (!this.isExtensaoExcelCSV(e.UploadedFile.FileName.Trim())) {
        if (!ValoresExcelUtil.isExtensaoExcel(e.UploadedFile.FileName.Trim())) {
            e.CallbackData = "Importação Operação Compromissada - Formato de Arquivo Inválido. Formato permitido: .xls ou .xlsx\n\n";
            return;
        }
        #endregion

        // Stream de bytes com o Conteudo do Arquivo Excel
        Stream sr = e.UploadedFile.FileContent;

        #region Processamento Arquivo Excel

        //Reseta Vetor de Dados
        this.valoresExcelOperacaoCompromissada = new List<ValoresExcelOperacaoCompromissada>();
        //
        try {
            // Ler Arquivo
            this.LerArquivoOperacaoCompromissada(sr);
            // Carrega Arquivo
            this.CarregaOperacaoCompromissada();
        }
        catch (Exception e2) {
            e.CallbackData = "Importação Operação Compromissada - " + e2.Message;
            return;
        }
        #endregion
    }
}