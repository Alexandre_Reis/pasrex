﻿using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Financial.Web.Common;
using System.IO;
using Financial.Integracao.Excel.Util;
using Financial.Integracao.Excel;
using EntitySpaces.Interfaces;
using Financial.Common;
using Financial.Investidor;
using System.Collections.Generic;
using Financial.Investidor.Enums;
using DevExpress.Web;
using Financial.Util;
using Financial.RendaFixa;
using Financial.RendaFixa.Enums;
using Financial.Common.Enums;
using Financial.Fundo;
using System.Globalization;
using Financial.CRM;
using Financial.Fundo.Enums;
using Financial.ContaCorrente;

public partial class ImportacaoBasePage : BasePage {
    
    /// <summary>
    /// Processa o arquivo YMF após ter acabado o Upload
    /// </summary>
    /// <param name="e"></param>
    protected void uplOperacaoFundoYMFPaulista_FileUploadComplete(FileUploadCompleteEventArgs e) {
        e.CallbackData = "";

        #region Trata Extensão Válida
        if (!this.isExtensaoTXT(e.UploadedFile.FileName.Trim())) {        
            e.CallbackData = "Importação Operação Fundo YMF - Formato de Arquivo Inválido. Formato permitido: .txt \n\n";
            return;
        }
        #endregion

        // Stream de bytes com o Conteudo do Arquivo TXT
        StreamReader sr = new StreamReader(e.UploadedFile.FileContent);
        
        #region Processamento Arquivo txt

        try {
            // Ler e Carrega Arquivo
            this.CarregaOperacaoFundoYMF(sr);
        }
        catch (Exception e2) {
            e.CallbackData = "Importação Operação Fundo YMF - " + e2.Message;
            return;
        }
        #endregion
    }

    private void CarregaOperacaoFundoYMF(StreamReader sr) {
        OperacaoFundoCollection oper = new OperacaoFundoCollection();
        
        try {
            #region Lê arquivo / Carrega Arquivo

            // String Decimal aparecem com formato de ponto no arquivo XML
            NumberFormatInfo provider = new NumberFormatInfo();
            provider.NumberDecimalSeparator = ".";

            string linha = "";
            while ((linha = sr.ReadLine()) != null) {
                if (!String.IsNullOrEmpty(linha)) {

                    string[] colunas = linha.Split(new Char[] { '\t' }); // delimitado por tab

                    if (colunas[0] == "07" && colunas[7].ToString().Trim() != "N") { 
                        #region Carrega Arquivo

                        DateTime dataOperacao = new DateTime( Convert.ToInt32( colunas[1].Substring(6, 4) ),
                                                              Convert.ToInt32( colunas[1].Substring(3, 2) ),
                                                              Convert.ToInt32( colunas[1].Substring(0, 2) )
                                                            );
                        //
                        string _idCliente = colunas[3].ToString().Trim();
                        Pessoa p = new PessoaCollection().BuscaPessoaPorCodigoInterface(_idCliente);
                        if (p == null) {
                            throw new Exception("Código Interface " +_idCliente + " não cadastrado na tabela Pessoa");
                        }

                        string _idCarteira = colunas[4].ToString().Trim();
                        Pessoa p1 = new PessoaCollection().BuscaPessoaPorCodigoInterface(_idCarteira);
                        if (p1 == null) {
                            throw new Exception("Código Interface " + _idCarteira + " não cadastrado na tabela Pessoa");
                        }

                        string tipoOperacao = colunas[7].ToString().Trim();
                        //
                        decimal valor = Convert.ToDecimal(colunas[10], provider);
                        decimal quantidade = Convert.ToDecimal(colunas[11], provider);
                        //
                        //
                        OperacaoFundo operacaoFundo = oper.AddNew();
                        //
                        operacaoFundo.IdCliente = p.IdPessoa.Value;
                        operacaoFundo.IdCarteira = p1.IdPessoa.Value;
                        operacaoFundo.DataOperacao = dataOperacao;
                        operacaoFundo.DataConversao = dataOperacao;
                        operacaoFundo.DataLiquidacao = dataOperacao;
                        operacaoFundo.DataAgendamento = dataOperacao;

                        if (tipoOperacao == "A") {
                            operacaoFundo.TipoOperacao = (byte)TipoOperacaoFundo.Aplicacao;
                        }
                        else if (tipoOperacao == "R") {
                            operacaoFundo.TipoOperacao = (byte)TipoOperacaoFundo.ResgateBruto;
                        }
                        else if (tipoOperacao == "Q") {
                            operacaoFundo.TipoOperacao = (byte)TipoOperacaoFundo.ResgateCotas;
                        }
                                                                                
                        operacaoFundo.ValorBruto = valor;
                        operacaoFundo.Quantidade = quantidade;

                        #region Tipo Resgate
                        if (operacaoFundo.TipoOperacao.Value == (byte)TipoOperacaoFundo.Aplicacao ||
                                            operacaoFundo.TipoOperacao.Value == (byte)TipoOperacaoFundo.AplicacaoCotasEspecial ||
                                            operacaoFundo.TipoOperacao.Value == (byte)TipoOperacaoFundo.AplicacaoAcoesEspecial ||
                                            operacaoFundo.TipoOperacao.Value == (byte)TipoOperacaoFundo.ComeCotas ||
                                            operacaoFundo.TipoOperacao.Value == (byte)TipoOperacaoFundo.AjustePosicao) {

                            operacaoFundo.TipoResgate = null;
                        }
                        else {
                            operacaoFundo.TipoResgate = (byte)TipoResgateFundo.FIFO;
                        } 
                        #endregion

                        operacaoFundo.IdFormaLiquidacao = 1;                        
                        operacaoFundo.Fonte = (byte)FonteOperacaoFundo.Manual;
                        //
                        #endregion
                    }
                }
            }
            sr.Close();

            #endregion

            //
            oper.Save();
        }
        catch (Exception e) {            
            throw new Exception(e.Message);
        }
    }
}