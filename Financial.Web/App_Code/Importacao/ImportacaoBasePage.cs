﻿using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Financial.Web.Common;
using Financial.Integracao.Excel;
using System.Collections.Generic;
using System.IO;
using System.Text;
using DevExpress.Web;
using System.Linq;

public partial class ImportacaoBasePage : BasePage {

    /* Guarda a mensagem de erro de Importação Internet */
    private StringBuilder mensagem = new StringBuilder();

    public StringBuilder MensagemErroInternet {
        get { return mensagem; }
        set { mensagem = value; }
    }

    /// <summary>
    /// false se arquivo não possui extensão html/html/mht ou zip
    /// true se arquivo possui extensão html/html/mht ou zip
    /// </summary>
    /// <param name="arquivo">Nome do arqquivo</param>
    /// <returns></returns>
    public bool isExtensaoValidaNotaCorretagem(string arquivo) {
        
        string extensao = new FileInfo(arquivo).Extension;
        extensao = extensao.ToLower();
        if (extensao != ".html" && extensao != ".htm" && extensao != ".mht" && extensao != ".zip") {
            return false;
        }
        return true;
    }

    /// <summary>
    /// false se arquivo não possui extensão pdf ou zip
    /// true se arquivo possui extensão pdf ou zip
    /// </summary>
    /// <param name="arquivo">Nome do arqquivo</param>
    /// <returns></returns>
    public bool isExtensaoValidaNotaCorretagemPDF(string arquivo) {
        string extensao = new FileInfo(arquivo).Extension;
        extensao = extensao.ToLower();
        if (extensao != ".pdf" && extensao != ".zip") {
            return false;
        }
        return true;
    }

    /// <summary>
    /// false se arquivo não possui extensão txt ou dat
    /// true se arquivo possui extensão txt, dat ou nenhuma
    /// </summary>
    /// <param name="arquivo">Nome do arqquivo</param>
    /// <returns></returns>
    public bool isExtensaoValida(string arquivo) {
        string extensao = new FileInfo(arquivo).Extension;
        extensao = extensao.ToLower();
        if (extensao != ".txt" && extensao != ".dat" && extensao != "") {
            return false;
        }
        return true;
    }

    /// <summary>
    /// false se arquivo não possui extensão txt
    /// true se arquivo possui extensão txt ou nenhuma
    /// </summary>
    /// <param name="arquivo">Nome do arqquivo</param>
    /// <returns></returns>
    public bool isExtensaoTXT(string arquivo) {
        string extensao = new FileInfo(arquivo).Extension;
        extensao = extensao.ToLower();
        if (extensao != ".txt" && extensao != "") {
            return false;
        }
        return true;
    }

    /// <summary>
    /// false se arquivo não possui extensão pdf
    /// true se arquivo possui extensão pdf
    /// </summary>
    /// <param name="arquivo">Nome do arquivo</param>
    /// <returns></returns>
    public bool isExtensaoPDF(string arquivo) {
        string extensao = new FileInfo(arquivo).Extension;
        extensao = extensao.ToLower();
        if (extensao != ".pdf") {
            return false;
        }
        return true;
    }

    /// <summary>
    /// false se arquivo não possui extensão xml
    /// true se arquivo possui extensão xml
    /// </summary>
    /// <param name="arquivo">Nome do arquivo</param>
    /// <returns></returns>
    public bool isExtensaoXML(string arquivo)
    {
        string extensao = new FileInfo(arquivo).Extension;
        extensao = extensao.ToLower();
        if (extensao != ".xml")
        {
            return false;
        }
        return true;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="arquivo"></param>
    /// <returns></returns>
    public bool isExtensaoExcelCSV(string arquivo) {
        string extensao = new FileInfo(arquivo).Extension;
        if (extensao != ".xls" && extensao != ".xlsx" && extensao != ".csv") {
            return false;
        }
        return true;
    }

    public bool isExtensaoZIP(string arquivo)
    {
        string extensao = new FileInfo(arquivo).Extension;
        if (extensao != ".zip")
        {
            return false;
        }
        return true;
    }

    #region Attribute
    /// <summary>
    /// TipoImportacao
    /// </summary>
    [System.AttributeUsage(System.AttributeTargets.Method |
                           System.AttributeTargets.Struct)
    ]
    public class TipoImportacao : System.Attribute
    {
        #region Properties
        public string Metodo;
        public string Prefixo { get; set; }
        #endregion

        #region constructor
        public TipoImportacao(string metodo)
        {
            Metodo = metodo;
            Prefixo = "";
        }
        #endregion
    }
    #endregion
}

