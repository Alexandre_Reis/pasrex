﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Financial.Integracao.Excel
{

    /// <summary>
    /// Armazena os Valores de Cotação Anbima Debenture presentes num arquivo Excel
    /// </summary>
    public class ValoresExcelCotacaoAnbimaDebenture
    {
        private DateTime data;
        private string codigoPapel;
        private DateTime dataEmissao;
        private DateTime dataVencimento;
        private decimal taxaIndicativa;
        private decimal pu;
        

        public DateTime Data {
            get { return data; }
            set { data = value; }
        }

        public string CodigoPapel
        {
            get { return codigoPapel; }
            set { codigoPapel = value; }
        }

        public DateTime DataVencimento
        {
            get { return dataVencimento; }
            set { dataVencimento = value; }
        }

        public decimal TaxaIndicativa
        {
            get { return taxaIndicativa; }
            set { taxaIndicativa = value; }
        }

        public decimal Pu
        {
            get { return pu; }
            set { pu = value; }
        }
        
    }
}