﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Financial.Integracao.Excel
{

    /// <summary>
    /// Armazena os Valores de Cotação Índice presentes num arquivo Excel
    /// </summary>
    public class ValoresExcelCotacaoIndice {
        private DateTime data;
        private int idIndice;
        private decimal valor;

        public DateTime Data {
            get { return data; }
            set { data = value; }
        }

        public int IdIndice {
            get { return idIndice; }
            set { idIndice = value; }
        }

        public decimal Valor {
            get { return valor; }
            set { valor = value; }
        }
    }
}