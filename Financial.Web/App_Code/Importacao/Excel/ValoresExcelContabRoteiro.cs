using System;
using System.Collections.Generic;
using System.Text;
using Financial.CRM.Enums;
using Financial.Investidor.Enums;

namespace Financial.Integracao.Excel
{

    /// <summary>
    /// 
    /// </summary>
    public class ValoresExcelContabRoteiro
    {
        public int IdPlano;
        public string ContaDebito;
        public string ContaCredito;
        public int Origem;
        public string Descricao;
        public string Identificador;
    }
}