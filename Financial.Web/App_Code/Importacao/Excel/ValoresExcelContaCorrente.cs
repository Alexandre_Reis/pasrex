using System;
using System.Collections.Generic;
using System.Text;
using Financial.Bolsa.Enums;

namespace Financial.Integracao.Excel {
    
    /// <summary>
    /// Armazena os Valores de Emissor presentes num arquivo Excel
    /// </summary>
    public class ValoresExcelContaCorrente {
        private int idPessoa;
        private string codigoBanco;
        private string numeroAgencia;
        private string dvAgencia;
        private string numeroConta;
        private string dvConta;
        private string idLocal;

        public int IdPessoa
        {
            get { return idPessoa; }
            set { idPessoa = value; }
        }

        public string CodigoBanco
        {
            get { return codigoBanco; }
            set { codigoBanco = value; }
        }

        public string NumeroAgencia
        {
            get { return numeroAgencia; }
            set { numeroAgencia = value; }
        }

        public string DvAgencia
        {
            get { return dvAgencia; }
            set { dvAgencia = value; }
        }

        public string NumeroConta
        {
            get { return numeroConta; }
            set { numeroConta = value; }
        }

        public string DvConta
        {
            get { return dvConta; }
            set { dvConta = value; }
        }

        public string IdLocal
        {
            get { return idLocal; }
            set { idLocal = value; }
        }
    }
}