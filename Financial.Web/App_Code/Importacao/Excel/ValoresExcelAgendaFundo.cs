using System;
using System.Collections.Generic;
using System.Text;
using Financial.Fundo.Enums;

namespace Financial.Integracao.Excel {
    
    /// <summary>
    /// Armazena os Valores de AgendaEventosRendaFixa Presentes num Arquivo Excel
    /// </summary>
    public class ValoresExcelAgendaEventosFundo{
        
        private int idCarteira;
        private DateTime dataEvento;
        private TipoEventoFundo tipo;
        private decimal taxa;
        private decimal valor;

        public int IdCarteira {
            get { return idCarteira; }
            set { idCarteira = value; }
        }

        public DateTime DataEvento
        {
            get { return dataEvento; }
            set { dataEvento = value; }
        }

        public TipoEventoFundo Tipo
        {
            get { return tipo; }
            set { tipo = value; }
        }

        public decimal Taxa
        {
            get { return taxa; }
            set { taxa = value; }
        }

        public decimal Valor
        {
            get { return valor; }
            set { valor = value; }
        }
    }
}
