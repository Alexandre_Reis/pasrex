using System;
using System.Collections.Generic;
using System.Text;

namespace Financial.Integracao.Excel {
    
    /// <summary>
    /// Armazena os Valores de MTMManual presentes num arquivo Excel
    /// </summary>
    public class ValoresExcelMTMManual {
        private int idCliente;
        private int idTitulo;
        private int? idOperacao;
        private DateTime dtVigencia;
        private decimal taxa252;
        private decimal puMTM;

        public int IdCliente
        {
            get { return idCliente; }
            set { idCliente = value; }
        }

        public int IdTitulo
        {
            get { return idTitulo; }
            set { idTitulo = value; }
        }

        public int? IdOperacao
        {
            get { return idOperacao; }
            set { idOperacao = value; }
        }

        public DateTime DtVigencia
        {
            get { return dtVigencia; }
            set { dtVigencia = value; }
        }

        public decimal Taxa252
        {
            get { return taxa252; }
            set { taxa252 = value; }
        }

        public decimal PuMTM
        {
            get { return puMTM; }
            set { puMTM = value; }
        }
    }
}