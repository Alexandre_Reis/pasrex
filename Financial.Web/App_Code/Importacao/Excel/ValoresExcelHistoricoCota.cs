using System;
using System.Collections.Generic;
using System.Text;

namespace Financial.Integracao.Excel {
    
    /// <summary>
    /// Armazena os valores de HistoricoCota presentes num arquivo Excel
    /// </summary>
    public class ValoresExcelHistoricoCota {
        private int idCarteira;
        private DateTime data;
        private decimal cota;
        private decimal pl;
        private decimal quantidadeFechamento;

        public int IdCarteira {
            get { return idCarteira; }
            set { idCarteira = value; }
        }

        public DateTime Data {
            get { return data; }
            set { data = value; }
        }

        public decimal Cota {
            get { return cota; }
            set { cota = value; }
        }

        public decimal PL {
            get { return pl; }
            set { pl = value; }
        }

        public decimal QuantidadeFechamento {
            get { return quantidadeFechamento; }
            set { quantidadeFechamento = value; }
        }
    }
}
