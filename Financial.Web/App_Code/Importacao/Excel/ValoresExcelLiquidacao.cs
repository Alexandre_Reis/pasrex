using System;
using System.Collections.Generic;
using System.Text;
using Financial.ContaCorrente.Enums;

namespace Financial.Integracao.Excel {
    public enum EnumTipoSaldo {
        SaldoAbertura = 0,
        SaldoFechamento = 1,
        Normal = 2
    }

    /// <summary>
    /// Armazena os valores de Liquidacao presentes num arquivo Excel
    /// </summary>
    [Serializable]
    public class ValoresExcelLiquidacao {
        private int idCliente;
        private EnumTipoSaldo tipoSaldo;
        private DateTime dataLancamento;
        private DateTime dataVencimento;
        private string descricao;
        private decimal valor;
        private byte situacao;
        private int? idConta;

        public int IdCliente {
            get { return idCliente; }
            set { idCliente = value; }
        }

        public EnumTipoSaldo TipoSaldo {
            get { return tipoSaldo; }
            set { tipoSaldo = value; }
        }

        public DateTime DataLancamento {
            get { return dataLancamento; }
            set { dataLancamento = value; }
        }

        public DateTime DataVencimento {
            get { return dataVencimento; }
            set { dataVencimento = value; }
        }

        public string Descricao {
            get { return descricao; }
            set { descricao = value; }
        }

        public decimal Valor {
            get { return valor; }
            set { valor = value; }
        }

        public byte Situacao {
            get { return situacao; }
            set { situacao = value; }
        }

        public int? IdConta
        {
            get { return idConta; }
            set { idConta = value; }
        }
    }
}
