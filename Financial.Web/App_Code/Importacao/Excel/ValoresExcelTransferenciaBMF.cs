using System;
using System.Collections.Generic;
using System.Text;

namespace Financial.Integracao.Excel
{

    /// <summary>
    /// Armazena os Valores de Colagem de ComeCotas
    /// </summary>
    public class ValoresExcelTransferenciaBMF
    {
        private int idCliente;
        private string cdAtivoBMF;
        private string serie;
        private int idAgenteOrigem;
        private int idAgenteDestino;        
        private DateTime data;
        private int quantidade;
        private decimal pu;

        public int IdCliente
        {
            get { return idCliente; }
            set { idCliente = value; }
        }

        public string CdAtivoBMF
        {
            get { return cdAtivoBMF; }
            set { cdAtivoBMF = value; }
        }

        public string Serie
        {
            get { return serie; }
            set { serie = value; }
        }

        public int IdAgenteOrigem
        {
            get { return idAgenteOrigem; }
            set { idAgenteOrigem = value; }
        }

        public int IdAgenteDestino
        {
            get { return idAgenteDestino; }
            set { idAgenteDestino = value; }
        }

        public DateTime Data
        {
            get { return data; }
            set { data = value; }
        }

        public int Quantidade
        {
            get { return quantidade; }
            set { quantidade = value; }
        }

        public decimal Pu
        {
            get { return pu; }
            set { pu = value; }
        }        
    }
}