﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Financial.Integracao.Excel
{

    /// <summary>
    /// Armazena os Valores de Cotação Serie presentes num arquivo Excel
    /// </summary>
    public class ValoresExcelCotacaoSerie
    {
        private DateTime data;
        private int idSerie;
        private decimal valor;

        public DateTime Data
        {
            get { return data; }
            set { data = value; }
        }

        public int IdSerie
        {
            get { return idSerie; }
            set { idSerie = value; }
        }

        public decimal Valor
        {
            get { return valor; }
            set { valor = value; }
        }
    }
}