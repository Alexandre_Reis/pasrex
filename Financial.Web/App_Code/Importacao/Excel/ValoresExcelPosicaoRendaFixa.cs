using System;
using System.Collections.Generic;
using System.Text;

namespace Financial.Integracao.Excel {
    /// <summary>
    /// Armazena os valores de PosicaoRendaFixa presentes num arquivo Excel
    /// </summary>
    public class ValoresExcelPosicaoRendaFixa {
        private int idCliente;
        private int idTitulo;
        private string tipoOperacao;
        private decimal quantidade;
        private decimal quantidadeBloqueada;
        private DateTime dataOperacao;
        private decimal puOperacao;
        private decimal taxaOperacao;
        private decimal puMercado;
        private DateTime? dataVolta;
        private decimal? taxaVolta;
        private decimal? puVolta;
        private int? idCustodia;
        private string tipoNegociacao;
        private int? idLocalNegociacao;
        private int? idAgenteCustodia;
        private int? idCorretora;

        public int IdCliente {
            get { return idCliente; }
            set { idCliente = value; }
        }

        public int IdTitulo {
            get { return idTitulo; }
            set { idTitulo = value; }
        }

        public string TipoOperacao {
            get { return tipoOperacao; }
            set { tipoOperacao = value; }
        }

        public decimal Quantidade {
            get { return quantidade; }
            set { quantidade = value; }
        }

        public decimal QuantidadeBloqueada {
            get { return quantidadeBloqueada; }
            set { quantidadeBloqueada = value; }
        }

        public DateTime DataOperacao {
            get { return dataOperacao; }
            set { dataOperacao = value; }
        }

        public decimal PuOperacao {
            get { return puOperacao; }
            set { puOperacao = value; }
        }

        public decimal TaxaOperacao {
            get { return taxaOperacao; }
            set { taxaOperacao = value; }
        }

        public decimal PuMercado {
            get { return puMercado; }
            set { puMercado = value; }
        }

        public DateTime? DataVolta {
            get { return dataVolta; }
            set { dataVolta = value; }
        }

        public decimal? TaxaVolta {
            get { return taxaVolta; }
            set { taxaVolta = value; }
        }

        public decimal? PuVolta {
            get { return puVolta; }
            set { puVolta = value; }
        }

        public int? IdCustodia
        {
            get { return idCustodia; }
            set { idCustodia = value; }
        }

        public string TipoNegociacao
        {
            get { return tipoNegociacao; }
            set { tipoNegociacao = value; }
        }

        public int? IdLocalNegociacao
        {
            get { return idLocalNegociacao; }
            set { idLocalNegociacao = value; }
        }

        public int? IdAgenteCustodia
        {
            get { return idAgenteCustodia; }
            set { idAgenteCustodia = value; }
        }

        public int? IdCorretora
        {
            get { return idCorretora; }
            set { idCorretora = value; }
        }
    }    
}
