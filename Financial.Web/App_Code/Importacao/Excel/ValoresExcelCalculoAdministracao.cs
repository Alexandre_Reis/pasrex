﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Financial.Integracao.Excel {
    
    /// <summary>
    /// Armazena os valores de Cálculo Administração presentes num arquivo Excel
    /// </summary>
    public class ValoresExcelCalculoAdministracao {
        private int idTabela;
        private int idCarteira;
        private decimal valorDia;
        private decimal valorAcumulado;
        private DateTime dataFimApropriacao;
        private DateTime dataPagamento;

        public int IdTabela {
            get { return idTabela; }
            set { idTabela = value; }
        }

        public int IdCarteira {
            get { return idCarteira; }
            set { idCarteira = value; }
        }

        public decimal ValorDia {
            get { return valorDia; }
            set { valorDia = value; }
        }
        
        public decimal ValorAcumulado {
            get { return valorAcumulado; }
            set { valorAcumulado = value; }
        }
        
        public DateTime DataFimApropriacao {
            get { return dataFimApropriacao; }
            set { dataFimApropriacao = value; }
        }
        
        public DateTime DataPagamento {
            get { return dataPagamento; }
            set { dataPagamento = value; }
        }        
    }
}
