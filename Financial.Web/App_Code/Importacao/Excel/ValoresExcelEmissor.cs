using System;
using System.Collections.Generic;
using System.Text;
using Financial.Bolsa.Enums;

namespace Financial.Integracao.Excel {
    
    /// <summary>
    /// Armazena os Valores de Emissor presentes num arquivo Excel
    /// </summary>
    public class ValoresExcelEmissor {
        private string nomeEmissor;
        private string cnpjEmissor;

        public string NomeEmissor {
            get { return nomeEmissor; }
            set { nomeEmissor = value; }
        }

        public string CnpjEmissor {
            get { return cnpjEmissor; }
            set { cnpjEmissor = value; }
        }
    }
}