using System;
using System.Collections.Generic;
using System.Text;

namespace Financial.Integracao.Excel
{

    /// <summary>
    /// Armazena os Valores de Colagem de ComeCotas
    /// </summary>
    public class ValoresExcelColagemComeCotas
    {
        private string tipoRegistro;
        private string codigoExterno;
        private DateTime dataReferencia;
        private int idCliente;
        private int idCarteira;
        private DateTime dataConversao;
        private Decimal quantidade;
        private Decimal cotaOperacao;
        private Decimal valorBruto;
        private Decimal valorLiquido;
		private Decimal valorCPMF;
        private Decimal valorPerformance;
		private Decimal prejuizoUsado;
        private Decimal rendimentoComeCotas;
        private Decimal variacaoComeCotas;
        private Decimal valorIR;
        private Decimal valorIOF;

        public string TipoRegistro
        {
            get { return tipoRegistro; }
            set { tipoRegistro = value; }
        }

        public string CodigoExterno
        {
            get { return codigoExterno; }
            set { codigoExterno = value; }
        }

        public DateTime DataReferencia
        {
            get { return dataReferencia; }
            set { dataReferencia = value; }
        }

        public int IdCliente
        {
            get { return idCliente; }
            set { idCliente = value; }
        }

        public int IdCarteira
        {
            get { return idCarteira; }
            set { idCarteira = value; }
        }

        public DateTime DataConversao
        {
            get { return dataConversao; }
            set { dataConversao = value; }
        }

        public decimal Quantidade
        {
            get { return quantidade; }
            set { quantidade = value; }
        }

        public decimal CotaOperacao
        {
            get { return cotaOperacao; }
            set { cotaOperacao = value; }
        }

        public decimal ValorBruto
        {
            get { return valorBruto; }
            set { valorBruto = value; }
        }

        public decimal ValorLiquido
        {
            get { return valorLiquido; }
            set { valorLiquido = value; }
        }

        public decimal ValorCPMF
        {
            get { return valorCPMF; }
            set { valorCPMF = value; }
        }

        public decimal ValorPerformance
        {
            get { return valorPerformance; }
            set { valorPerformance = value; }
        }

        public decimal PrejuizoUsado
        {
            get { return prejuizoUsado; }
            set { prejuizoUsado = value; }
        }

        public decimal RendimentoComeCotas
        {
            get { return rendimentoComeCotas; }
            set { rendimentoComeCotas = value; }
        }

        public decimal VariacaoComeCotas
        {
            get { return variacaoComeCotas; }
            set { variacaoComeCotas = value; }
        }

        public decimal ValorIR
        {
            get { return valorIR; }
            set { valorIR = value; }
        }

        public decimal ValorIOF
        {
            get { return valorIOF; }
            set { valorIOF = value; }
        }
    }
}