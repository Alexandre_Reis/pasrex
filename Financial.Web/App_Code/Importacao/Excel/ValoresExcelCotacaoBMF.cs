﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Financial.Integracao.Excel
{

    /// <summary>
    /// Armazena os Valores de Cotação BMF presentes num arquivo Excel
    /// </summary>
    public class ValoresExcelCotacaoBMF {
        private DateTime data;
        private string cdAtivoBMF;
        private string serie;
        private decimal pu;
        private decimal puCorrigido;

        public DateTime Data {
            get { return data; }
            set { data = value; }
        }

        public string CdAtivoBMF {
            get { return cdAtivoBMF; }
            set { cdAtivoBMF = value; }
        }

        public string Serie
        {
            get { return serie; }
            set { serie = value; }
        }

        public decimal Pu {
            get { return pu; }
            set { pu = value; }
        }

        public decimal PuCorrigido
        {
            get { return puCorrigido; }
            set { puCorrigido = value; }
        }
    }
}