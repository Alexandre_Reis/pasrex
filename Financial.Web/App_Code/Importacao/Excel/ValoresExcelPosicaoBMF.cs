using System;
using System.Collections.Generic;
using System.Text;

namespace Financial.Integracao.Excel {
    /// <summary>
    /// Armazena os valores de PosicaoBMF presentes num arquivo Excel
    /// </summary>
    public class ValoresExcelPosicaoBMF {
        private int idCliente;
        private int codigoBMF;
        private string cdAtivoBMF;
        private string serie;
        private int quantidade;
        private decimal puCusto;

        public int IdCliente {
            get { return idCliente; }
            set { idCliente = value; }
        }

        public int CodigoBMF {
            get { return codigoBMF; }
            set { codigoBMF = value; }
        }

        public string CdAtivoBMF {
            get { return cdAtivoBMF; }
            set { cdAtivoBMF = value; }
        }

        public string Serie {
            get { return serie; }
            set { serie = value; }
        }

        public int Quantidade {
            get { return quantidade; }
            set { quantidade = value; }
        }

        public decimal PuCusto {
            get { return puCusto; }
            set { puCusto = value; }
        }
    }    
}
