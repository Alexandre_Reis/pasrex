﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
//using Microsoft.Office.Core;
//using Excel;
using System.Diagnostics;
using System.Runtime.InteropServices;
using System.Threading;
using System.Globalization;
using System.Reflection;
using System.Data;
using System.Windows.Forms;
//using Excel;
//using Microsoft.Office.Interop.Excel;

namespace Financial.Integracao.Excel.Util {
    
    /// <summary>
    /// Classe Utilitária do Excel
    /// </summary>
    public class ValoresExcelUtil {

        /// <summary>
        /// Pega o ID de Um Processo
        /// </summary>
        /// <param name="hwnd"></param>
        /// <param name="lpdwProcessId"></param>
        /// <returns></returns>
        [DllImport("user32")]
        public static extern int GetWindowThreadProcessId(int hwnd, ref int lpdwProcessId);

        /// <summary>
        /// false se arquivo não possui extensão excel(.xls ou .xlsx)
        /// true se arquivo possui extensão excel(.xls ou .xlsx)
        /// </summary>
        /// <param name="arquivo">Nome do arqquivo</param>
        /// <returns></returns>
        public static bool isExtensaoExcel(string arquivo) {
            string extensao = new FileInfo(arquivo).Extension;
            if (extensao != ".xls" && extensao != ".xlsx") {                
                return false;
            }
            return true;
        }

        public static bool isExcelFormatoValido(int linhaInicial, string[] colunasConferencias, Bytescout.Spreadsheet.Worksheet workSheet)
        {
            bool formato = true;

            for (int i = 0; i < colunasConferencias.Length; i++)
            {
                string valoresColuna = null;

                if (workSheet.Cell(linhaInicial, i).Value != null)
                {
                    valoresColuna = workSheet.Cell(linhaInicial, i).ValueAsString;
                }

                if (!String.IsNullOrEmpty(valoresColuna))
                {
                    if (colunasConferencias[i].Trim().ToLower() != valoresColuna.Trim().ToLower())
                    {
                        formato = false;
                        break;
                    }
                }
                else
                {
                    formato = false;
                    break;
                }
            }

            return formato;
        }

        /// <summary>
        /// Identifica se um arquivo Excel tem o formato valido baseado nas colunas da 
        /// primeira linha do arquivo. 
        /// Os textos das colunas são Case Insensitive
        /// </summary>
        /// <param name="colunasConferencias">Colunas da Primeira Linha do Excel</param>
        /// <param name="workSheet">Planilha excel ja aberta</param>
        /// <returns></returns>
        public static bool isExcelFormatoValido(string[] colunasConferencias, Bytescout.Spreadsheet.Worksheet workSheet) {
            return isExcelFormatoValido(0, colunasConferencias, workSheet);
        }

        /// <summary>
        /// Identifica se um arquivo Excel tem o formato valido baseado nas colunas da 
        /// primeira linha do arquivo. 
        /// Os textos das colunas são Case Insensitive
        /// </summary>
        /// <param name="colunasConferencias">Colunas da Primeira Linha do Excel</param>
        /// <param name="workSheet">Planilha excel ja aberta</param>
        /// <returns></returns>
        //[Obsolete("Devido a DLL Excel da Microsoft")]
        //public static bool isFormatoValido(string[] colunasConferencias, Worksheet workSheet) {
        //    bool formato = true;

        //    for (int i = 0; i < colunasConferencias.Length; i++) {
        //        string valoresColuna = null;
        //        //
        //        if (((Range)workSheet.Cells[1, i+1]).Value2 != null) {
        //            valoresColuna = ((Range)workSheet.Cells[1, i+1]).Value2.ToString();
        //        }

        //        if (!String.IsNullOrEmpty(valoresColuna)) {
        //            if (colunasConferencias[i].Trim().ToLower() != valoresColuna.Trim().ToLower()) {
        //                formato = false;
        //                break;
        //            }
        //        }
        //        else {
        //            formato = false;
        //            break;
        //        }
        //    }

        //    return formato;
        //}

        /// <summary>
        /// Identifica se um arquivo Excel tem o formato valido baseado nas colunas da 
        /// de uma linha inicial(passada como parametro) do arquivo. 
        /// Os textos das colunas são Case Insensitive
        /// </summary>
        /// <param name="linha">Linha do Excel que se deseja fazer a comparação das Colunas</param>
        /// <param name="colunasConferencias">Colunas da Linha do Excel
        /// Se colunasConferencias = "ColunaOculta" Desconsiderar Coluna para Comparação
        /// </param>
        /// <param name="workSheet">Planilha excel ja aberta</param>
        /// <returns></returns>
        //[Obsolete("Devido a DLL Excel da Microsoft")]
        //public static bool isFormatoValido(int linha, string[] colunasConferencias, Worksheet workSheet) {
        //    bool formato = true;
        //    const string COLUNA_OCULTA_NAO_USADA = "ColunaOculta";

        //    for (int i = 0; i < colunasConferencias.Length; i++) {
        //        string valoresColuna = null;
        //        //
        //        if (((Range)workSheet.Cells[linha, i + 1]).Value2 != null) {
        //            valoresColuna = ((Range)workSheet.Cells[linha, i + 1]).Value2.ToString();
        //            valoresColuna = valoresColuna.Replace("\n", ""); // Retira Caracter Pula Linha se existir
        //        }

        //        if (colunasConferencias[i] == COLUNA_OCULTA_NAO_USADA) {
        //            continue;
        //        }

        //        else if (!String.IsNullOrEmpty(valoresColuna)) {                
        //            if (colunasConferencias[i].Trim().ToLower() != valoresColuna.Trim().ToLower()) {
        //                formato = false;
        //                break;
        //            }
        //        }
        //        else {
        //            formato = false;
        //            break;
        //        }
        //    }

        //    return formato;
        //}

        /// <summary>
        /// Força o Kill da ultima instancia do Excel no TaskManager
        /// </summary>
        public static void KillAllExcels() {
            const string PROGRAM = "EXCEL";
            //foreach (Process proc in Process.GetProcessesByName(PROGRAM)) {
            //    proc.Kill();
            //}
            Process[] proc = Process.GetProcessesByName(PROGRAM);
            proc[0].Kill();
        }

        /// <summary>
        /// Kill do Processo Excell Associado a App
        /// </summary>
        //[Obsolete("Devido a DLL Excel da Microsoft")]
        //public static void KillExcellApp(ApplicationClass app) {

        //    // Pode ser feito desse jeito mas demora mais para matar o Processo
        //    /*
        //    try {
        //        System.Runtime.InteropServices.Marshal.ReleaseComObject(app);
        //        GC.Collect();
        //        GC.WaitForPendingFinalizers();
        //    }

        //    catch (Exception ex) {
        //        app = null;
        //    }*/

        //    int processId = 0;

        //    GetWindowThreadProcessId(app.Hwnd, ref processId);

        //    Process excellProcess = Process.GetProcessById(processId);
        //    excellProcess.Kill();
        //}

        /// <summary>
        /// Exporta um DataTable para um Arquivo Excel
        /// </summary>
        /// <param name="dt"></param>
        /// <param name="filePath">Caminho Completo do arquivo Excel</param>
        //[Obsolete("Devido a DLL Excel da Microsoft")]
        //public static void ExportDataTableToExcel(System.Data.DataTable dt, string filePath) {
        //    // Indica Quantidade de Planilhas - Cada planilha só pode ter 256 Colunas   
        //    int quantidadePlanilhas = 0;

        //    // Trata Numero de Colunas > 256
        //    if (dt.Rows.Count >= 1) {
        //        int colunas = dt.Columns.Count;
        //        // Cada 256 colunas representa uma planilha
        //        quantidadePlanilhas = (int)Math.Ceiling(colunas / 256M);
        //    }

        //    // Seta Thread para Inglês
        //    Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture("en-US");

        //    // Start Excel and get Application object.
        //    ApplicationClass oXL = new ApplicationClass();

        //    // Set some properties            
        //    oXL.DisplayAlerts = false;
        //    //
        //    oXL.SheetsInNewWorkbook = 3;

        //    // Get a new workbook.
        //    Workbook oWB = oXL.Workbooks.Add(Missing.Value);

        //    // Para Cada Planilha
        //    for (int j = 1; j <= quantidadePlanilhas; j++) {
        //        Sheets xlSheets = oWB.Sheets as Sheets;
        //        //                
        //        Worksheet xlNewSheet = (Worksheet)xlSheets.Add(xlSheets[j], Type.Missing, Type.Missing, Type.Missing);
        //        //xlNewSheet.Name = "MatrizAtivos" + j;
        //        xlNewSheet.Name = "Ativos" + j;

        //        int rowCount = 1;
        //        foreach (DataRow dr in dt.Rows) {  // Linhas
        //            #region Para cada Linha
        //            rowCount++;

        //            // Se não for Ultima Planilha Imprime 256 colunas - Planilhas Cheias
        //            if (j != quantidadePlanilhas) {
        //                #region Planilhas Intermediarias
        //                //
        //                int inicio = 1 + (256 * (j - 1));
        //                int fim = inicio + 255; // Colunas
        //                //
        //                int k = 1;
        //                for (int i = inicio; i <= fim; i++) { // Colunas
        //                    /* i serve para indices do DataTable 
        //                       k serve para indices das Celulas do Planilha 
        //                     */

        //                    // Add the header the first time through                                               
        //                    if (rowCount == 2) {
        //                        xlNewSheet.get_Range(xlNewSheet.Cells[1, k], xlNewSheet.Cells[1, k]).Font.Bold = true; // Negrito                        
        //                        xlNewSheet.Cells[1, k] = dt.Columns[i - 1].ColumnName;
        //                    }

        //                    xlNewSheet.Cells[rowCount, k] = dr[i - 1].ToString();
        //                    //
        //                    k++;
        //                }
        //                #endregion
        //            }
        //            else { // Ultima Planilha
        //                // Modulo de 256 indica quantas colunas restam para imprimir
        //                /* 1 % 256 = 1 
        //                 * 10 % 256 = 10;
        //                 * 255 % 256 = 255
        //                 * 256 % 256 = 0 - Se Der Zero Prenche total a planilha
        //                 * 257 % 256 = 1
        //                 * 511 % 256 = 255
        //                 * 512 % 256 = 0 - Se Der Zero Prenche total a planilha
        //                 */
        //                #region Ultima Planilha

        //                int numeroColunas = dt.Columns.Count % 256;
        //                if (numeroColunas == 0) {
        //                    numeroColunas = 256;
        //                }

        //                //
        //                int inicio = 1 + (256 * (j - 1));
        //                int fim = inicio + (numeroColunas - 1); // Colunas
        //                //
        //                int k = 1;
        //                for (int i = inicio; i <= fim; i++) { // Colunas
        //                    /* i serve para indices do DataTable 
        //                       k serve para indices das Celulas do Planilha 
        //                    */

        //                    // Add the header the first time through
        //                    if (rowCount == 2) {
        //                        xlNewSheet.get_Range(xlNewSheet.Cells[1, k], xlNewSheet.Cells[1, k]).Font.Bold = true; // Negrito                        
        //                        xlNewSheet.Cells[1, k] = dt.Columns[i - 1].ColumnName;
        //                    }

        //                    xlNewSheet.Cells[rowCount, k] = dr[i - 1].ToString();
        //                    //
        //                    k++;
        //                }
        //                #endregion
        //            }
        //            #endregion
        //        }

        //        #region AutoFit De cada Planilha
        //        Range oRange;
        //        if (j != quantidadePlanilhas) { // Planilhas Intermidiarias - 256 Colunas
        //            oRange = xlNewSheet.get_Range(xlNewSheet.Cells[1, 1], xlNewSheet.Cells[rowCount, 256]);
        //            oRange.EntireColumn.AutoFit();
        //        }

        //        else { // Ultima Planilha - Parcialmente Cheia
        //            //int fimAux = k - 1;

        //            int numeroColunas = dt.Columns.Count % 256;
        //            if (numeroColunas == 0) {
        //                numeroColunas = 256;
        //            }

        //            oRange = xlNewSheet.get_Range(xlNewSheet.Cells[1, 1], xlNewSheet.Cells[rowCount, numeroColunas]);
        //            oRange.EntireColumn.AutoFit();

        //        }
        //        #endregion

        //        // Save the sheet and close
        //        xlNewSheet = null;
        //        oRange = null;
        //    }

        //    #region Deleta Sheet1/Sheet2/Sheet3 de Acordo com o numero de Planilhas Inseridas
        //    Sheets xlSheetsAux = oWB.Sheets as Sheets;
        //    //                
        //    if (quantidadePlanilhas == 1) {
        //        Worksheet deletaSheet = (Worksheet)xlSheetsAux.get_Item(4);
        //        deletaSheet.Delete();
        //    }
        //    else if (quantidadePlanilhas == 2) {
        //        Worksheet deletaSheet = (Worksheet)xlSheetsAux.get_Item(4);
        //        deletaSheet.Delete();
        //        //
        //        deletaSheet = (Worksheet)xlSheetsAux.get_Item(4);
        //        deletaSheet.Delete();
        //    }
        //    else if (quantidadePlanilhas >= 3) { // Deleta as Ultimas 3
        //        //int fim = xlSheetsAux.Count;
        //        //for (int i = fim; i > fim-3; i--) {
        //        //    Worksheet deletaSheet = (Worksheet)xlSheetsAux.get_Item(i);
        //        //    //string name = deletaSheet.Name;
        //        //}

        //        int ultima = xlSheetsAux.Count;
        //        Worksheet deletaSheet = (Worksheet)xlSheetsAux.get_Item(ultima);
        //        deletaSheet.Delete();

        //        ultima = xlSheetsAux.Count;
        //        deletaSheet = (Worksheet)xlSheetsAux.get_Item(ultima);
        //        deletaSheet.Delete();

        //        ultima = xlSheetsAux.Count;
        //        deletaSheet = (Worksheet)xlSheetsAux.get_Item(ultima);
        //        deletaSheet.Delete();
        //    }
        //    #endregion

        //    // Seta Active Plan como a primeira           
        //    Worksheet activePlan = (Worksheet)oWB.Sheets[1];
        //    activePlan.Select(Missing.Value);

        //    try {
        //        oWB.SaveAs(filePath, XlFileFormat.xlWorkbookNormal,
        //        Missing.Value, Missing.Value, Missing.Value, Missing.Value,
        //        XlSaveAsAccessMode.xlExclusive,
        //        Missing.Value, Missing.Value, Missing.Value,
        //        Missing.Value, Missing.Value);
        //    }
        //    catch (Exception) {
        //        ValoresExcelUtil.KillExcellApp(oXL);
        //        //MessageBox.Show("Erro: " + e.Message);
        //    }

        //    //                       
        //    oWB.Close(Missing.Value, Missing.Value, Missing.Value);
        //    oWB = null;
        //    oXL.Quit();

        //    //MessageBox.Show("Arquivo Excel Gerado.");
            
        //    ValoresExcelUtil.KillExcellApp(oXL);
        //}
    }
}