using System;
using System.Collections.Generic;
using System.Text;

namespace Financial.Integracao.Excel
{

    /// <summary>
    /// Armazena os Valores de Colagem de ComeCotas
    /// </summary>
    public class ValoresExcelTransferenciaBolsa
    {
        private int idCliente;
        private string cdAtivoBolsa;
        
        private int idAgenteOrigem;
        private int idAgenteDestino;        
        private DateTime data;
        private Decimal quantidade;
        private Decimal pu;

        public int IdCliente
        {
            get { return idCliente; }
            set { idCliente = value; }
        }

        public string CdAtivoBolsa
        {
            get { return cdAtivoBolsa; }
            set { cdAtivoBolsa = value; }
        }

        public int IdAgenteOrigem
        {
            get { return idAgenteOrigem; }
            set { idAgenteOrigem = value; }
        }

        public int IdAgenteDestino
        {
            get { return idAgenteDestino; }
            set { idAgenteDestino = value; }
        }

        public DateTime Data
        {
            get { return data; }
            set { data = value; }
        }

        public decimal Quantidade
        {
            get { return quantidade; }
            set { quantidade = value; }
        }

        public decimal Pu
        {
            get { return pu; }
            set { pu = value; }
        }        
    }
}