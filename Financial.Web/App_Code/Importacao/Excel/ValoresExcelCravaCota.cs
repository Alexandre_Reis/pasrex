using System;
using System.Collections.Generic;
using System.Text;

namespace Financial.Integracao.Excel {
    
    /// <summary>
    /// Armazena os valores de CravaCota presentes num arquivo Excel
    /// </summary>
    public class ValoresExcelCravaCota {
        private int idCarteira;
        private DateTime data;
        private decimal cota;
        private int tipoBloqueio;

        public int IdCarteira {
            get { return idCarteira; }
            set { idCarteira = value; }
        }

        public DateTime Data {
            get { return data; }
            set { data = value; }
        }

        public decimal Cota {
            get { return cota; }
            set { cota = value; }
        }

        public int TipoBloqueio {
            get { return tipoBloqueio; }
            set { tipoBloqueio = value; }
        }
    }
}
