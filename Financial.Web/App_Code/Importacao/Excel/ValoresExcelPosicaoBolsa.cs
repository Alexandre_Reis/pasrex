using System;
using System.Collections.Generic;
using System.Text;

namespace Financial.Integracao.Excel {
    
    /// <summary>
    /// Armazena os valores de PosicaoBolsa presentes num arquivo Excel
    /// </summary>
    public class ValoresExcelPosicaoBolsa {
        private int idCliente;
        private int codigoBovespa;
        private string cdAtivoBolsa;
        private int quantidade;
        private decimal puCusto;
        private int quantidadeBloqueada;
        private decimal resultadoRealizar;

        public int IdCliente {
            get { return idCliente; }
            set { idCliente = value; }
        }

        public int CodigoBovespa {
            get { return codigoBovespa; }
            set { codigoBovespa = value; }
        }

        public string CdAtivoBolsa {
            get { return cdAtivoBolsa; }
            set { cdAtivoBolsa = value; }
        }

        public int Quantidade {
            get { return quantidade; }
            set { quantidade = value; }
        }

        public decimal PuCusto {
            get { return puCusto; }
            set { puCusto = value; }
        }

        public int QuantidadeBloqueada
        {
            get { return quantidadeBloqueada; }
            set { quantidadeBloqueada = value; }
        }
    }
}
