﻿using System;
using System.Collections.Generic;
using System.Text;

using Financial.Util;

using Financial.Bolsa.Enums;

namespace Financial.Integracao.Excel
{

    /// <summary>
    /// Armazena os Valores de OperacaoEmprestimoBolsa presentes num arquivo Excel
    /// </summary>
    public class ValoresExcelOperacaoEmprestimoBolsa
    {
        private int idCliente;
        private string cdAtivoBolsa;
        private int codigoBovespa;
        private DateTime dataRegistro;
        private DateTime dataVencimento;
        private decimal taxaOperacao;
        private decimal quantidade;
        private decimal pu;
        private int numeroContrato;
        private string trader;
        //
        private decimal? valor; // Usado para planilha Mellon
        private string apelidoCliente; // Usado para planilha Mellon
        //
        private PontaEmprestimoBolsa pontaEmprestimo;
        private TipoEmprestimoBolsa tipoEmprestimo;
        //
        private string tipoOperacao; // Usado para planilha Mellon

        public int IdCliente
        {
            get { return idCliente; }
            set { idCliente = value; }
        }

        public string CdAtivoBolsa
        {
            get { return cdAtivoBolsa; }
            set { cdAtivoBolsa = value; }
        }

        public int CodigoBovespa
        {
            get { return codigoBovespa; }
            set { codigoBovespa = value; }
        }

        public DateTime DataRegistro
        {
            get { return dataRegistro; }
            set { dataRegistro = value; }
        }

        public DateTime DataVencimento
        {
            get { return dataVencimento; }
            set { dataVencimento = value; }
        }

        public decimal TaxaOperacao
        {
            get { return taxaOperacao; }
            set { taxaOperacao = value; }
        }

        public decimal Quantidade
        {
            get { return quantidade; }
            set { quantidade = value; }
        }

        public decimal Pu
        {
            get { return pu; }
            set { pu = value; }
        }

        public int NumeroContrato
        {
            get { return numeroContrato; }
            set { numeroContrato = value; }
        }

        public PontaEmprestimoBolsa PontaEmprestimo
        {
            get { return pontaEmprestimo; }
            set { pontaEmprestimo = value; }
        }

        public TipoEmprestimoBolsa TipoEmprestimo
        {
            get { return tipoEmprestimo; }
            set { tipoEmprestimo = value; }
        }

        public decimal? Valor
        {
            get { return valor; }
            set { valor = value; }
        }
        public string ApelidoCliente
        {
            get { return apelidoCliente; }
            set { apelidoCliente = value; }
        }

        public string TipoOperacao
        {
            get { return tipoOperacao; }
            set { tipoOperacao = value; }
        }

        public string Trader
        {
            get { return trader; }
            set { trader = value; }
        }
    }
}
