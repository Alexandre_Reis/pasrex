using System;
using System.Collections.Generic;
using System.Text;
using Financial.Bolsa.Enums;

namespace Financial.Integracao.Excel {
    
    /// <summary>
    /// Armazena os Valores de OrdemBMF presentes num arquivo Excel
    /// </summary>
    public class ValoresExcelOrdemBMF {
        private int idCliente;
        private string cdAtivoBMF;
        private string serie;
        
        
        private int codigoBovespa;
        private string trader;
        private string tipoOrdem;
        //
        private DateTime dataOperacao;
        private DateTime dataOrdem;
        //                       
        private decimal pu;
        private decimal valor;
        private decimal quantidade;
        private decimal percentualDesconto;
        private string calculaDespesas;
        private decimal taxa;
        private int tipoEstrategia;
        private string localCustodia;
        private string localNegociacao;
        private string clearing;
        private string agenteCustodia;

        private decimal emolumentos;
        private decimal taxaRegistro;
        private decimal valorCorretagem;
        private decimal taxaClearing;

        public int IdCliente {
            get { return idCliente; }
            set { idCliente = value; }
        }
        
        public string CdAtivoBMF {
            get { return cdAtivoBMF; }
            set { cdAtivoBMF = value; }
        }

        public string Serie {
            get { return serie; }
            set { serie = value; }
        }

        public int CodigoBovespa {
            get { return codigoBovespa; }
            set { codigoBovespa = value; }
        }

        public string Trader {
            get { return trader; }
            set { trader = value; }
        }

        public string TipoOrdem {
            get { return tipoOrdem; }
            set { tipoOrdem = value; }
        }

        public DateTime DataOperacao
        {
            get { return dataOperacao; }
            set { dataOperacao = value;}
        }

        public DateTime DataOrdem {
            get { return dataOrdem; }
            set { dataOrdem = value; }
        }

        public decimal Pu {
            get { return pu; }
            set { pu = value; }
        }

        public decimal Valor {
            get { return valor; }
            set { valor = value; }
        }

        public decimal Quantidade {
            get { return quantidade; }
            set { quantidade = value; }
        }

        public decimal PercentualDesconto {
            get { return percentualDesconto; }
            set { percentualDesconto = value; }
        }

        public string CalculaDespesas {
            get { return calculaDespesas; }
            set { calculaDespesas = value; }
        }

        public decimal Taxa
        {
            get { return taxa; }
            set { taxa = value; }
        }

        public int TipoEstrategia
        {
            get { return tipoEstrategia; }
            set { tipoEstrategia = value; }
        }

        public string LocalCustodia
        {
            get { return localCustodia; }
            set { localCustodia = value; }
        }

        public string LocalNegociacao
        {
            get { return localNegociacao; }
            set { localNegociacao = value; }
        }

        public string Clearing
        {
            get { return clearing; }
            set { clearing = value; }
        }

        public string AgenteCustodia
        {
            get { return agenteCustodia; }
            set { agenteCustodia = value; }
        }

        public decimal Emolumentos
        {
            get { return emolumentos; }
            set { emolumentos = value; }
        }

        public decimal TaxaRegistro
        {
            get { return taxaRegistro; }
            set { taxaRegistro = value; }
        }

        public decimal ValorCorretagem
        {
            get { return valorCorretagem; }
            set { valorCorretagem = value; }
        }

        public decimal TaxaClearing
        {
            get { return taxaClearing; }
            set { taxaClearing = value; }
        }

    }
}