using System;
using System.Collections.Generic;
using System.Text;

namespace Financial.Integracao.Excel {
    
    /// <summary>
    /// Armazena os Valores de PosicaoTermoBolsa presentes num arquivo Excel
    /// </summary>
    public class ValoresExcelPosicaoTermoBolsa {
        private DateTime dataPosicao;
        private short? idIndice;
        private int idCliente;
        private int codigoBovespa;
        private string cdAtivoBolsa;
        private int quantidade;
        private decimal puTermo;
        private decimal puTermoLiquido;
        private decimal valorTermo;
        private decimal valorTermoLiquido;
        private DateTime dataOperacao;
        private DateTime dataVencimento;
        private decimal puCustoLiquidoAcao;
        private string numeroContrato;

        public DateTime DataPosicao
        {
            get { return dataPosicao; }
            set { dataPosicao = value; }
        }

        public short? IdIndice
        {
            get { return idIndice; }
            set { idIndice = value; }
        }

        public int IdCliente {
            get { return idCliente; }
            set { idCliente = value; }
        }

        public int CodigoBovespa {
            get { return codigoBovespa; }
            set { codigoBovespa = value; }
        }

        public string CdAtivoBolsa {
            get { return cdAtivoBolsa; }
            set { cdAtivoBolsa = value; }
        }

        public int Quantidade {
            get { return quantidade; }
            set { quantidade = value; }
        }

        public decimal PuTermo {
            get { return puTermo; }
            set { puTermo = value; }
        }

        public decimal PuTermoLiquido
        {
            get { return puTermoLiquido; }
            set { puTermoLiquido = value; }
        }

        public decimal ValorTermo
        {
            get { return valorTermo; }
            set { valorTermo = value; }
        }

        public decimal ValorTermoLiquido
        {
            get { return valorTermoLiquido; }
            set { valorTermoLiquido = value; }
        }

        public DateTime DataOperacao
        {
            get { return dataOperacao; }
            set { dataOperacao = value; }
        }

        public DateTime DataVencimento
        {
            get { return dataVencimento; }
            set { dataVencimento = value; }
        }

        public decimal PuCustoLiquidoAcao
        {
            get { return puCustoLiquidoAcao; }
            set { puCustoLiquidoAcao = value; }
        }

        public string NumeroContrato
        {
            get { return numeroContrato; }
            set { numeroContrato = value; }
        }
    }
}
