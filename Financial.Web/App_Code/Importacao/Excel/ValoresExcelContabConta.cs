using System;
using System.Collections.Generic;
using System.Text;
using Financial.CRM.Enums;
using Financial.Investidor.Enums;

namespace Financial.Integracao.Excel
{
    /// <summary>
    /// 
    /// </summary>
    public class ValoresExcelContabConta
    {
        public int IdPlano;
        public int IdConta;
        public string Codigo;
        public string Descricao;
        public string CodigoReduzida;
    }
    
}