using System;
using System.Collections.Generic;
using System.Text;

namespace Financial.Integracao.Excel
{
    
    /// <summary>
    /// Armazena os valores de OrdemCotista presentes num arquivo Excel
    /// </summary>
    public class ValoresExcelOrdemCotista {
        private int idCotista;
        private int idCarteira;        
        private DateTime dataOperacao;
        private DateTime? dataConversao;
        private DateTime? dataLiquidacao;
        private DateTime? dataAgendamento;
        private byte tipoOperacao;
        private byte tipoResgate;        
        private decimal quantidade;
        private decimal valorBruto;
        private decimal valorLiquido;
        private byte status;   

        public int IdCotista {
            get { return idCotista; }
            set { idCotista = value; }
        }

        public int IdCarteira {
            get { return idCarteira; }
            set { idCarteira = value; }
        }

        public DateTime DataOperacao {
            get { return dataOperacao; }
            set { dataOperacao = value; }
        }

        public DateTime? DataConversao {
            get { return dataConversao; }
            set { dataConversao = value; }
        }

        public DateTime? DataLiquidacao
        {
            get { return dataLiquidacao; }
            set { dataLiquidacao = value; }
        }

        public DateTime? DataAgendamento
        {
            get { return dataAgendamento; }
            set { dataAgendamento = value; }
        }

        public byte TipoOperacao
        {
            get { return tipoOperacao; }
            set { tipoOperacao = value; }
        }

        public byte TipoResgate
        {
            get { return tipoResgate; }
            set { tipoResgate = value; }
        }

        public decimal Quantidade {
            get { return quantidade; }
            set { quantidade = value; }
        }


        public decimal ValorBruto
        {
            get { return valorBruto; }
            set { valorBruto = value; }
        }

        public decimal ValorLiquido
        {
            get { return valorLiquido; }
            set { valorLiquido = value; }
        }

        public byte Status
        {
            get { return status; }
            set { status = value; }
        }
       
    }
}
