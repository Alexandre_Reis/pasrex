using System;
using System.Collections.Generic;
using System.Text;

namespace Financial.Integracao.Excel {
    /// <summary>
    /// Armazena os valores de OperacaoRendaFixa presentes num arquivo Excel
    /// </summary>
    public class ValoresExcelOperacaoCompromissada {
        public int nIdOper;
        public string sTipoOperComp;
        public int nNumEmpresa;
        public string sNomeEmpresa;
        public int nNumContraparte;
        public string sNomeContraparte;
        public string sCodTit;
        public string sCodAtivo;
        public DateTime dtDtVctoPapel;
        public DateTime dtDtEmissao;
        public DateTime dtDtIni;
        public DateTime dtDtVctoOper;        
        public int nPrazoDu;
        public string sIdTipoIndexador;
        public int nPctIndexador;
        public string sCodCarteira;
        public decimal nTaxa;
        public int nQtd;
        public decimal nVlrAplicado;
        public decimal nDespRendaApropTotal;
        public decimal nResgateBruto;        
        public int? nDespRendaApropEfetiva;
        public decimal nVlrAtual;
        public decimal nDespRendaAprop;
        public int nOrdem;
        public DateTime dtDtInicio;
        public DateTime dtDtFim;        
        public DateTime dtDtRef;        
        public string sIdSitOper;
    }    
}