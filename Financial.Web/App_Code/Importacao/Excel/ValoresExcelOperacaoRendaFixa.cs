using System;
using System.Collections.Generic;
using System.Text;

namespace Financial.Integracao.Excel {
    /// <summary>
    /// Armazena os valores de OperacaoRendaFixa presentes num arquivo Excel
    /// </summary>
    public class ValoresExcelOperacaoRendaFixa {
        private int idCliente;
        private int idTitulo;
        private DateTime dataOperacao;
        private DateTime? dataLiquidacao;
        private string tipoOperacao;
        private decimal quantidade;               
        private decimal puOperacao;
        private decimal valor;
        private decimal taxaOperacao;
        private string categoria;
        private DateTime? dataVolta;
        private decimal? taxaVolta;
        private decimal? puVolta;
        private decimal? valorVolta;

        private int? idAgenteCorretora;
        private int? idLiquidacao;
        private byte? idCustodia;
        private string custodia;
        private decimal? valorCorretagem;
        private decimal? emolumento;
        private int? numeroNota;
        private decimal? valorISS;
        private short? idIndiceVolta;
        private int? codigoBovespa;
        private DateTime dataOriginal;
        private string isIPO;

        private string localNegociacao;
        private string idAgenteCustodia;
        private string clearing;
        private string trader;
     
        private int? idAgenteContraparte;
        private int? idClienteContraparte;
        private int? idOperacaoResgatada;
        private int? idCategoriaMovimentacao;
        private string operacaoTermo;

        public int IdCliente {
            get { return idCliente; }
            set { idCliente = value; }
        }

        public int IdTitulo {
            get { return idTitulo; }
            set { idTitulo = value; }
        }

        public DateTime DataOperacao {
            get { return dataOperacao; }
            set { dataOperacao = value; }
        }

        public DateTime? DataLiquidacao
        {
            get { return dataLiquidacao; }
            set { dataLiquidacao = value; }
        }

        public string TipoOperacao {
            get { return tipoOperacao; }
            set { tipoOperacao = value; }
        }

        public decimal Quantidade {
            get { return quantidade; }
            set { quantidade = value; }
        }
        
        public decimal PuOperacao {
            get { return puOperacao; }
            set { puOperacao = value; }
        }

        public decimal Valor {
            get { return valor; }
            set { valor = value; }
        }

        public decimal TaxaOperacao {
            get { return taxaOperacao; }
            set { taxaOperacao = value; }
        }

        public string Categoria {
            get { return categoria; }
            set { categoria = value; }
        }

        public DateTime? DataVolta {
            get { return dataVolta; }
            set { dataVolta = value; }
        }

        public decimal? TaxaVolta {
            get { return taxaVolta; }
            set { taxaVolta = value; }
        }

        public decimal? PuVolta {
            get { return puVolta; }
            set { puVolta = value; }
        }

        public decimal? ValorVolta {
            get { return valorVolta; }
            set { valorVolta = value; }
        }

        public int? IdAgenteCorretora
        {
            get { return idAgenteCorretora; }
            set { idAgenteCorretora = value; }
        }

        public int? IdLiquidacao
        {
            get { return idLiquidacao; }
            set { idLiquidacao = value; }
        }

        public byte? IdCustodia
        {
            get { return idCustodia; }
            set { idCustodia = value; }
        }

        public short? IdIndiceVolta {
            get { return idIndiceVolta; }
            set { idIndiceVolta = value; }
        }

        public string Custodia
        {
            get { return custodia; }
            set { custodia = value; }
        }

        public decimal? ValorCorretagem
        {
            get { return valorCorretagem; }
            set { valorCorretagem = value; }
        }

        public decimal? Emolumento
        {
            get { return emolumento; }
            set { emolumento = value; }
        }


        public decimal? ValorISS
        {
            get { return valorISS; }
            set { valorISS = value; }
        }

        public int? NumeroNota
        {
            get { return numeroNota; }
            set { numeroNota = value; }
        }

        public int? CodigoBovespa
        {
            get { return codigoBovespa; }
            set { codigoBovespa = value; }
        }

        public DateTime DataOriginal
        {
            get { return dataOriginal; }
            set { dataOriginal = value; }
        }

        public string IsIPO
        {
            get { return isIPO; }
            set { isIPO = value; }        
		}

        public string LocalNegociacao
        {
            get { return localNegociacao; }
            set { localNegociacao = value; }
        }

        public string IdAgenteCustodia
        {
            get { return idAgenteCustodia; }
            set { idAgenteCustodia = value; }
        }

        public string Clearing
        {
            get { return clearing; }
            set { clearing = value; }
        }

        public string Trader
        {
            get { return trader; }
            set { trader = value; }
        }

        

        public int? IdAgenteContraparte
        {
            get { return idAgenteContraparte; }
            set { idAgenteContraparte = value; }
        }

        

        public int? IdClienteContraparte
        {
            get { return idClienteContraparte; }
            set { idClienteContraparte = value; }
        }

        public int? IdOperacaoResgatada
        {
            get { return idOperacaoResgatada; }
            set { idOperacaoResgatada = value; }
        }

        public int? IdCategoriaMovimentacao
        {
            get { return idCategoriaMovimentacao; }
            set { idCategoriaMovimentacao = value; }
        }
        public string OperacaoTermo
        {
            get { return operacaoTermo; }
            set { operacaoTermo = value; } 
        }

        	
    }    
}

