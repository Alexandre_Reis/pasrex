using System;
using System.Collections.Generic;
using System.Text;

namespace Financial.Integracao.Excel {
    
    /// <summary>
    /// Armazena os Valores de PrejuizoCotista Presentes num Arquivo Excel
    /// </summary>
    public class ValoresExcelApuracaoRendaVariavel {
        private int idCliente;
        private int mes;
        private int ano;
        private decimal prejuizoCompensarNormal;
        private decimal prejuizoCompensarDT;
        private decimal irFonteCompensar;
        private decimal prejuizoCompensarFII;

        public int IdCliente {
            get { return idCliente; }
            set { idCliente = value; }
        }

        public int Mes {
            get { return mes; }
            set { mes = value; }
        }

        public int Ano {
            get { return ano; }
            set { ano = value; }
        }

        public decimal PrejuizoCompensarNormal {
            get { return prejuizoCompensarNormal; }
            set { prejuizoCompensarNormal = value; }
        }

        public decimal PrejuizoCompensarDT {
            get { return prejuizoCompensarDT; }
            set { prejuizoCompensarDT = value; }
        }

        public decimal IRFonteCompensar {
            get { return irFonteCompensar; }
            set { irFonteCompensar = value; }
        }

        public decimal PrejuizoCompensarFII
        {
            get { return prejuizoCompensarFII; }
            set { prejuizoCompensarFII = value; }
        }
    }
}
