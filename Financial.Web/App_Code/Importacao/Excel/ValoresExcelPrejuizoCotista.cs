using System;
using System.Collections.Generic;
using System.Text;

namespace Financial.Integracao.Excel {
    
    /// <summary>
    /// Armazena os Valores de PrejuizoCotista Presentes num Arquivo Excel
    /// </summary>
    public class ValoresExcelPrejuizoCotista {
        private int idCotista;
        private int idCarteira;        
        private DateTime data;
        private decimal valorPrejuizo;
        private DateTime? dataLimiteCompensacao;

        public int IdCotista {
            get { return idCotista; }
            set { idCotista = value; }
        }

        public int IdCarteira {
            get { return idCarteira; }
            set { idCarteira = value; }
        }

        public DateTime Data {
            get { return data; }
            set { data = value; }
        }

        public decimal ValorPrejuizo {
            get { return valorPrejuizo; }
            set { valorPrejuizo = value; }
        }

        public DateTime? DataLimiteCompensacao {
            get { return dataLimiteCompensacao; }
            set { dataLimiteCompensacao = value; }
        }        
    }
}
