﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Financial.Integracao.Excel
{

    /// <summary>
    /// Armazena os Valores de Cotação Bolsa presentes num arquivo Excel
    /// </summary>
    public class ValoresExcelCotacaoBolsa {
        private DateTime data;
        private string cdAtivoBolsa;
        private decimal puMedio;
        private decimal puFechamento;        

        public DateTime Data {
            get { return data; }
            set { data = value; }
        }

        public string CdAtivoBolsa {
            get { return cdAtivoBolsa; }
            set { cdAtivoBolsa = value; }
        }
        
        public decimal PuMedio
        {
            get { return puMedio; }
            set { puMedio = value; }
        }

        public decimal PuFechamento
        {
            get { return puFechamento; }
            set { puFechamento = value; }
        }
    }
}