﻿using System;
using System.Collections.Generic;
using System.Text;
using Financial.CRM.Enums;
using Financial.Investidor.Enums;

namespace Financial.Integracao.Excel
{

    /// <summary>
    /// Armazena os valores de Cliente/Pessoa/PessoEndereco presentes num arquivo Excel
    /// </summary>
    public class ValoresExcelCadastroPessoa
    {
        public class Endereco
        {
            protected string endereco;
            protected string numero;
            protected string complemento;
            protected string bairro;
            protected string cidade;
            protected string cep;
            protected string uf;
            protected string pais;
            protected string fone;
            protected string email;

            #region Getters
            public string Rua
            {
                get { return endereco; }
            }
            public string Numero
            {
                get { return numero; }
            }
            public string Complemento
            {
                get { return complemento; }
            }
            public string Bairro
            {
                get { return bairro; }
            }
            public string Cidade
            {
                get { return cidade; }
            }
            public string Cep
            {
                get { return cep; }
            }
            public string Uf
            {
                get { return uf; }
            }
            public string Pais
            {
                get { return pais; }
            }
            public string Fone
            {
                get { return fone; }
            }
            public string Email
            {
                get { return email; }
            }
            #endregion

            /// <summary>
            /// Construtor
            /// </summary>
            /// <param name="endereco"></param>
            /// <param name="numero"></param>
            /// <param name="complemento"></param>
            /// <param name="bairro"></param>
            /// <param name="cidade"></param>
            /// <param name="cep"></param>
            /// <param name="uf"></param>
            /// <param name="pais"></param>
            public Endereco(string endereco, string numero, string complemento,
                            string bairro, string cidade, string cep, string uf, string pais,
                            string fone, string email)
            {

                this.endereco = endereco;
                this.numero = numero;
                this.complemento = complemento;
                this.bairro = bairro;
                this.cidade = cidade;
                this.cep = cep;
                this.uf = uf;
                this.pais = pais;
                this.fone = fone;
                this.email = email;
            }

            /// <summary>
            /// Imprime os valores do Objeto this
            /// </summary>
            /// <returns></returns>
            public override string ToString()
            {
                return this.endereco + " " +
                       this.numero + " " +
                       this.complemento + " " +
                       this.bairro + " " +
                       this.cidade + " " +
                       this.cep + " " +
                       this.uf + " " +
                       this.pais  + " " +
                       this.fone + " " +
                       this.email;
            }

            /// <summary>
            /// Retorna true se existe algum valor em Endereço
            /// campo pais não é considerado
            /// </summary>
            /// <returns></returns>
            public bool HasValue()
            {
                return this.endereco != "" ||
                       this.numero != "" ||
                       this.complemento != "" ||
                       this.bairro != "" ||
                       this.cidade != "" ||
                       this.cep != "" ||
                       this.uf != "" ||
                       this.pais != "";
            }
        }

        private int idPessoa;
        private string nome;
        private string apelido;
        private TipoPessoa tipoPessoa;
        private string documento;
        private string codigoInterface;
        private Endereco endereco;
        private Endereco enderecoComercial;

        public int IdPessoa
        {
            get { return idPessoa; }
            set { idPessoa = value; }
        }

        public string Nome
        {
            get { return nome; }
            set { nome = value; }
        }

        public string Apelido
        {
            get { return apelido; }
            set { apelido = value; }
        }

        public string Documento
        {
            get { return documento; }
            set { documento = value; }
        }        

        public CRM.Enums.TipoPessoa TipoPessoa
        {
            get { return tipoPessoa; }
            set { tipoPessoa = value; }
        }

        public Endereco EnderecoPessoa
        {
            get { return endereco; }
            set { endereco = value; }
        }

        public Endereco EnderecoComercialPessoa
        {
            get { return enderecoComercial; }
            set { enderecoComercial = value; }
        }

        public string CodigoInterface
        {
            get { return codigoInterface; }
            set { codigoInterface = value; }
        }

    }
}