using System;
using System.Collections.Generic;
using System.Text;

namespace Financial.Integracao.Excel
{

    /// <summary>
    /// Armazena os valores de PosicaoFundo presentes num arquivo Excel
    /// </summary>
    public class ValoresExcelPosicaoFundo
    {
        private int idCliente;
        private int idCarteira;
        private decimal valorAplicacao;
        private DateTime dataAplicacao;
        private DateTime dataConversao;
        private decimal quantidade;
        private decimal cotaAplicacao;
        private DateTime? dataUltimaCobrancaIR;
        private DateTime? dataUltimoCortePfee;
        private decimal? quantidadePreCortes;
        private string idPosicaoComeCotas;
        private DateTime? dataLiquidacao;
        private decimal? valorIOFVirtual;

        public int IdCliente
        {
            get { return idCliente; }
            set { idCliente = value; }
        }

        public int IdCarteira
        {
            get { return idCarteira; }
            set { idCarteira = value; }
        }

        public decimal ValorAplicacao
        {
            get { return valorAplicacao; }
            set { valorAplicacao = value; }
        }

        public DateTime DataAplicacao
        {
            get { return dataAplicacao; }
            set { dataAplicacao = value; }
        }

        public DateTime DataConversao
        {
            get { return dataConversao; }
            set { dataConversao = value; }
        }

        public decimal Quantidade
        {
            get { return quantidade; }
            set { quantidade = value; }
        }

        public decimal CotaAplicacao
        {
            get { return cotaAplicacao; }
            set { cotaAplicacao = value; }
        }

        public DateTime? DataUltimaCobrancaIR
        {
            get { return dataUltimaCobrancaIR; }
            set { dataUltimaCobrancaIR = value; }
        }

        public DateTime? DataUltimoCortePfee
        {
            get { return dataUltimoCortePfee; }
            set { dataUltimoCortePfee = value; }
        }

        public decimal? QuantidadePreCortes
        {
            get { return quantidadePreCortes; }
            set { quantidadePreCortes = value; }
        }


        public string IdPosicaoComeCotas
        {
            get { return idPosicaoComeCotas; }
            set { idPosicaoComeCotas = value; }
        }

        public DateTime? DataLiquidacao
        {
            get { return dataLiquidacao; }
            set { dataLiquidacao = value; }
        }

        public decimal? ValorIOFVirtual
        {
            get { return valorIOFVirtual; }
            set { valorIOFVirtual = value; }
        }
    }
}