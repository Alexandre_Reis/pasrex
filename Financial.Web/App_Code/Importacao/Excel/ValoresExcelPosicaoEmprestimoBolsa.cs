using System;
using System.Collections.Generic;
using System.Text;
using Financial.Bolsa.Enums;

namespace Financial.Integracao.Excel {

    /// <summary>
    /// Armazena os valores de PosicaoEmprestimoBolsa presentes num arquivo Excel
    /// </summary>
    public class ValoresExcelPosicaoEmprestimoBolsa {
        private int idCliente;
        private int codigoBovespa;
        private string cdAtivoBolsa;
        private int quantidade;
        private decimal puLiquidoOriginal;
        private decimal valorBase;
        private PontaEmprestimoBolsa tipoEmprestimo;
        private DateTime dataRegistro;
        private DateTime dataVencimento;
        private decimal taxaOperacao;
        private decimal taxaComissao;
        private int numeroContrato;
        private DateTime dataInicialDevolucaoAntecipada;

        public int IdCliente {
            get { return idCliente; }
            set { idCliente = value; }
        }

        public int CodigoBovespa {
            get { return codigoBovespa; }
            set { codigoBovespa = value; }
        }

        public string CdAtivoBolsa {
            get { return cdAtivoBolsa; }
            set { cdAtivoBolsa = value; }
        }

        public int Quantidade {
            get { return quantidade; }
            set { quantidade = value; }
        }

        public decimal PuLiquidoOriginal {
            get { return puLiquidoOriginal; }
            set { puLiquidoOriginal = value; }
        }

        public decimal ValorBase {
            get { return valorBase; }
            set { valorBase = value; }
        }

        public PontaEmprestimoBolsa TipoEmprestimo {
            get { return tipoEmprestimo; }
            set { tipoEmprestimo = value; }
        }

        public DateTime DataRegistro {
            get { return dataRegistro; }
            set { dataRegistro = value; }
        }

        public DateTime DataVencimento {
            get { return dataVencimento; }
            set { dataVencimento = value; }
        }

        public decimal TaxaOperacao {
            get { return taxaOperacao; }
            set { taxaOperacao = value; }
        }

        public decimal TaxaComissao {
            get { return taxaComissao; }
            set { taxaComissao = value; }
        }

        public int NumeroContrato {
            get { return numeroContrato; }
            set { numeroContrato = value; }
        }

        public DateTime DataInicialDevolucaoAntecipada
        {
            get { return dataInicialDevolucaoAntecipada; }
            set { dataInicialDevolucaoAntecipada = value; }
        }
    }
}
