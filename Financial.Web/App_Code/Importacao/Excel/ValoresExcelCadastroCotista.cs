﻿using System;
using System.Collections.Generic;
using System.Text;
using Financial.CRM;
using Financial.CRM.Enums;
using Financial.InvestidorCotista.Enums;


namespace Financial.Integracao.Excel
{

    /// <summary>
    /// Armazena os Valores de Cotista/Pessoa/PessoEndereco presentes num arquivo Excel
    /// </summary>
    public class ValoresExcelCadastroCotista
    {
        public class Endereco
        {
            protected string endereco;
            protected string numero;
            protected string complemento;
            protected string bairro;
            protected string cidade;
            protected string cep;
            protected string uf;
            protected string pais;
            protected string foneDDD;
            protected string foneNumero;
            protected string foneRamal;
            protected string email;

            #region Getters
            public string Rua
            {
                get { return endereco; }
            }
            public string Numero
            {
                get { return numero; }
            }
            public string Complemento
            {
                get { return complemento; }
            }
            public string Bairro
            {
                get { return bairro; }
            }
            public string Cidade
            {
                get { return cidade; }
            }
            public string Cep
            {
                get { return cep; }
            }
            public string Uf
            {
                get { return uf; }
            }
            public string Pais
            {
                get { return pais; }
            }
            public string FoneDDD
            {
                get { return foneDDD; }
            }
            public string FoneNumero
            {
                get { return foneNumero; }
            }
            public string FoneRamal
            {
                get { return foneRamal; }
            }
            public string Email
            {
                get { return email; }
            }
            #endregion

            /// <summary>
            /// Construtor
            /// </summary>
            /// <param name="endereco"></param>
            /// <param name="numero"></param>
            /// <param name="complemento"></param>
            /// <param name="bairro"></param>
            /// <param name="cidade"></param>
            /// <param name="cep"></param>
            /// <param name="uf"></param>
            /// <param name="pais"></param>
            public Endereco(string endereco, string numero, string complemento,
                             string bairro, string cidade, string cep, string uf, string pais,
                             string foneDDD, string foneNumero, string foneRamal, string email)
            {

                this.endereco = endereco;
                this.numero = numero;
                this.complemento = complemento;
                this.bairro = bairro;
                this.cidade = cidade;
                this.cep = cep;
                this.uf = uf;
                this.pais = pais;
                this.foneDDD = foneDDD;
                this.foneNumero = foneNumero;
                this.foneRamal = foneRamal;
                this.email = email;
            }

            /// <summary>
            /// Imprime os valores do Objeto this
            /// </summary>
            /// <returns></returns>
            public override string ToString()
            {
                return this.endereco + " " +
                       this.numero + " " +
                       this.complemento + " " +
                       this.bairro + " " +
                       this.cidade + " " +
                       this.cep + " " +
                       this.uf + " " +
                       this.pais + " " +
                       this.foneDDD + " " +
                       this.foneNumero + " " +
                        this.foneRamal + " " +
                       this.email;
            }

            /// <summary>
            /// Retorna true se existe algum valor em Endereço
            /// campo pais não é considerado
            /// </summary>
            /// <returns></returns>
            public bool HasValue()
            {
                return this.endereco != "" ||
                       this.numero != "" ||
                       this.complemento != "" ||
                       this.bairro != "" ||
                       this.cidade != "" ||
                       this.cep != "" ||
                       this.uf != "" ||
                       this.pais != "";
            }
        }

        public class InformacaoBancaria
        {
            public string CodigoCompensacaoBanco;
            public string CodigoAgencia;
            public string NomeAgencia;
            public string DigitoAgencia;
            public short? LocalAgencia;
            public string NumeroConta;
            public string DigitoConta;
            public int MoedaConta;
            public string ContaDefault;

            /// <summary>
            /// Construtor
            /// </summary>
            public InformacaoBancaria()
            {

            }
        }

        private int idCotista;
        private string nome;
        private string apelido;
        private TipoPessoa tipoPessoa;
        private string cnpj;
        private string isentoIR;
        private string isentoIOF;
        private StatusAtivoCotista statusCotista;
        private TipoTributacaoCotista tipoTributacaoCotista;
        private string pessoaVinculada;
        private SituacaoLegalPessoa situacaoLegal;
        private string ufNaturalidade;
        //
        private TipoCotista? tipoCotistaCVM;
        private string codigoInterface;
        private EstadoCivilPessoa? estadoCivilCotista;
        private string rg;
        private string emissorRg;
        private DateTime? dataEmissaoRg;
        private string ufEmissor;
        private string sexo;
        private DateTime? dataNascimento;
        private string profissao;
        //
        private Endereco endereco = null;
        private Endereco enderecoComercial = null;
        private string criadoPor;

        private int? idTitular;

        private List<InformacaoBancaria> informacoesBancarias = null;

        #region Atributos Publics
        public int IdCotista
        {
            get { return idCotista; }
            set { idCotista = value; }
        }

        public string Nome
        {
            get { return nome; }
            set { nome = value; }
        }

        public string Apelido
        {
            get { return apelido; }
            set { apelido = value; }
        }

        public string Cnpj
        {
            get { return cnpj; }
            set { cnpj = value; }
        }

        public string CriadoPor
        {
            get { return criadoPor; }
            set { criadoPor = value; }
        }

        public string IsentoIOF
        {
            get { return isentoIOF; }
            set { isentoIOF = value; }
        }

        public string IsentoIR
        {
            get { return isentoIR; }
            set { isentoIR = value; }
        }

        public StatusAtivoCotista StatusCotista
        {
            get { return statusCotista; }
            set { statusCotista = value; }
        }

        public TipoTributacaoCotista TipoTributacaoCotista
        {
            get { return tipoTributacaoCotista; }
            set { tipoTributacaoCotista = value; }
        }

        public string PessoaVinculada
        {
            get { return pessoaVinculada; }
            set { pessoaVinculada = value; }
        }

        public SituacaoLegalPessoa SituacaoLegal
        {
            get { return situacaoLegal; }
            set { situacaoLegal = value; }
        }

        public string UFNaturalidade
        {
            get { return ufNaturalidade; }
            set { ufNaturalidade = value; }
        }

        public TipoCotista? TipoCotistaCVM
        {
            get { return tipoCotistaCVM; }
            set { tipoCotistaCVM = value; }
        }

        public int? IdTitular
        {
            get { return idTitular; }
            set { idTitular = value; }
        }

        public TipoPessoa TipoPessoa
        {
            get { return tipoPessoa; }
            set { tipoPessoa = value; }
        }

        public EstadoCivilPessoa? EstadoCivilCotista
        {
            get { return estadoCivilCotista; }
            set { estadoCivilCotista = value; }
        }

        public string CodigoInterface
        {
            get { return codigoInterface; }
            set { codigoInterface = value; }
        }

        public string Rg
        {
            get { return rg; }
            set { rg = value; }
        }

        public string EmissorRg
        {
            get { return emissorRg; }
            set { emissorRg = value; }
        }

        public DateTime? DataEmissaoRg
        {
            get { return dataEmissaoRg; }
            set { dataEmissaoRg = value; }
        }

        public string UFEmissor
        {
            get { return ufEmissor; }
            set { ufEmissor = value; }
        }

        public string Sexo
        {
            get { return sexo; }
            set { sexo = value; }
        }

        public DateTime? DataNascimento
        {
            get { return dataNascimento; }
            set { dataNascimento = value; }
        }

        public string Profissao
        {
            get { return profissao; }
            set { profissao = value; }
        }

        public Endereco EnderecoPessoa
        {
            get { return endereco; }
            set { endereco = value; }
        }

        public Endereco EnderecoComercialPessoa
        {
            get { return enderecoComercial; }
            set { enderecoComercial = value; }
        }

        public List<InformacaoBancaria> InformacoesBancarias
        {
            get { return informacoesBancarias; }
            set { informacoesBancarias = value; }
        }

        public TipoCotistaAnbima? TipoCotistaAnbima { get; set; }
        public byte? PaisNacionalidade { get; set; }
        public string PoliticamenteExposta { get; set; }
        public DateTime? DataVencimentoCadastro { get; set; }
        public DateTime? DataExpiracaoCotista { get; set; }
        public string NomePai { get; set; }
        public string NomeMae { get; set; }
        public string CodigoInterfaceCotista { get; set; }
        public string PendenciaCadastral { get; set; }
        public List<PessoaEndereco> PessoaEnderecoList { get; set; }
        public List<PessoaEmail> PessoaEmailList { get; set; }
        public List<PessoaTelefone> PessoaTelefoneList { get; set; }
        public string Opcao { get; set; }

        #endregion
    }
}