using System;
using System.Collections.Generic;
using System.Text;
using Financial.Bolsa.Enums;

namespace Financial.Integracao.Excel {
    
    /// <summary>
    /// Armazena os Valores de OrdemBolsa presentes num arquivo Excel
    /// </summary>
    public class ValoresExcelOrdemBolsa {
        private int idCliente;
        private string cdAtivoBolsa;
        
        private int codigoBovespa; 
        private string trader;
        //
        private string tipoOrdem;
        //
        private DateTime dataOrdem;
        private DateTime dataOperacao;
        //                       
        private decimal pu;
        private decimal valor;
        private decimal quantidade;
        private decimal percentualDesconto;
        private string localCustodia;
        private string localNegociacao;
        private string clearing;
        private string agenteCustodia;

        private string calculaDespesas;
              
        public int IdCliente {
            get { return idCliente; }
            set { idCliente = value; }
        }
        
        public string CdAtivoBolsa {
            get { return cdAtivoBolsa; }
            set { cdAtivoBolsa = value; }
        }

        public int CodigoBovespa {
            get { return codigoBovespa; }
            set { codigoBovespa = value; }
        }

        public string Trader {
            get { return trader; }
            set { trader = value; }
        }

        public string TipoOrdem {
            get { return tipoOrdem; }
            set { tipoOrdem = value; }
        }

        public DateTime DataOrdem {
            get { return dataOrdem; }
            set { dataOrdem = value; }
        }

        public DateTime DataOperacao
        {
            get { return dataOperacao; }
            set { dataOperacao = value; }
        }

        public decimal Pu {
            get { return pu; }
            set { pu = value; }
        }

        public decimal Valor {
            get { return valor; }
            set { valor = value; }
        }

        public decimal Quantidade {
            get { return quantidade; }
            set { quantidade = value; }
        }

        public decimal PercentualDesconto {
            get { return percentualDesconto; }
            set { percentualDesconto = value; }
        }

        public string CalculaDespesas {
            get { return calculaDespesas; }
            set { calculaDespesas = value; }
        }

        public string LocalCustodia
        {
            get { return localCustodia; }
            set { localCustodia = value; }
        }

        public string LocalNegociacao
        {
            get { return localNegociacao; }
            set { localNegociacao = value; }
        }

        public string Clearing
        {
            get { return clearing; }
            set { clearing = value; }
        }

        public string AgenteCustodia
        {
            get { return agenteCustodia; }
            set { agenteCustodia = value; }
        }

    }
}