﻿using System;
using System.Collections.Generic;
using System.Text;
using Financial.CRM.Enums;
using Financial.Investidor.Enums;

namespace Financial.Integracao.Excel
{

    /// <summary>
    /// Armazena os valores de Cliente/Pessoa/PessoEndereco presentes num arquivo Excel
    /// </summary>
    public class ValoresExcelCadastroCliente
    {
        public class Endereco
        {
            protected string endereco;
            protected string numero;
            protected string complemento;
            protected string bairro;
            protected string cidade;
            protected string cep;
            protected string uf;
            protected string pais;
            protected string fone;
            protected string email;

            #region Getters
            public string Rua
            {
                get { return endereco; }
            }
            public string Numero
            {
                get { return numero; }
            }
            public string Complemento
            {
                get { return complemento; }
            }
            public string Bairro
            {
                get { return bairro; }
            }
            public string Cidade
            {
                get { return cidade; }
            }
            public string Cep
            {
                get { return cep; }
            }
            public string Uf
            {
                get { return uf; }
            }
            public string Pais
            {
                get { return pais; }
            }
            public string Fone
            {
                get { return fone; }
            }
            public string Email
            {
                get { return email; }
            }
            #endregion

            /// <summary>
            /// Construtor
            /// </summary>
            /// <param name="endereco"></param>
            /// <param name="numero"></param>
            /// <param name="complemento"></param>
            /// <param name="bairro"></param>
            /// <param name="cidade"></param>
            /// <param name="cep"></param>
            /// <param name="uf"></param>
            /// <param name="pais"></param>
            public Endereco(string endereco, string numero, string complemento,
                            string bairro, string cidade, string cep, string uf, string pais,
                            string fone, string email)
            {

                this.endereco = endereco;
                this.numero = numero;
                this.complemento = complemento;
                this.bairro = bairro;
                this.cidade = cidade;
                this.cep = cep;
                this.uf = uf;
                this.pais = pais;
                this.fone = fone;
                this.email = email;
            }

            /// <summary>
            /// Imprime os valores do Objeto this
            /// </summary>
            /// <returns></returns>
            public override string ToString()
            {
                return this.endereco + " " +
                       this.numero + " " +
                       this.complemento + " " +
                       this.bairro + " " +
                       this.cidade + " " +
                       this.cep + " " +
                       this.uf + " " +
                       this.pais  + " " +
                       this.fone + " " +
                       this.email;
            }

            /// <summary>
            /// Retorna true se existe algum valor em Endereço
            /// campo pais não é considerado
            /// </summary>
            /// <returns></returns>
            public bool HasValue()
            {
                return this.endereco != "" ||
                       this.numero != "" ||
                       this.complemento != "" ||
                       this.bairro != "" ||
                       this.cidade != "" ||
                       this.cep != "" ||
                       this.uf != "" ||
                       this.pais != "";
            }
        }

        public class CadastroBolsa
        {
            protected byte? tipoCotacao;
            protected int? idAssessor;
            protected string isentoIR;
            protected string pessoaVinculada;

            #region Getters
            public byte? TipoCotacao
            {
                get { return tipoCotacao; }
            }
            public int? IdAssessor
            {
                get { return idAssessor; }
            }
            public string IsentoIR
            {
                get { return isentoIR; }
            }
            public string PessoaVinculada
            {
                get { return pessoaVinculada; }
            }
            #endregion

            /// <summary>
            /// Construtor
            /// </summary>
            public CadastroBolsa(byte tipoCotacao, int? idAssessor, string isentoIR, string pessoaVinculada)
            {
                this.tipoCotacao = tipoCotacao;
                this.idAssessor = idAssessor;
                this.isentoIR = isentoIR;
                this.pessoaVinculada = pessoaVinculada;
            }
                        
            public bool HasValue()
            {
                return this.tipoCotacao.HasValue &&
                       this.isentoIR != "";
            }
        }

        public class CadastroBMF
        {
            protected byte? tipoCotacao;
            protected int? idAssessor;
            
            #region Getters
            public byte? TipoCotacao
            {
                get { return tipoCotacao; }
            }
            public int? IdAssessor
            {
                get { return idAssessor; }
            }
            #endregion

            /// <summary>
            /// Construtor
            /// </summary>
            public CadastroBMF(byte tipoCotacao, int? idAssessor)
            {
                this.tipoCotacao = tipoCotacao;
                this.idAssessor = idAssessor;                
            }
                        
            public bool HasValue()
            {
                return this.tipoCotacao.HasValue;
            }
        }

        public class CadastroRendaFixa
        {
            protected int? idAssessor;
            protected string isentoIR;
            protected string isentoIOF;

            #region Getters
            public int? IdAssessor
            {
                get { return idAssessor; }
            }
            public string IsentoIR
            {
                get { return isentoIR; }
            }
            public string IsentoIOF
            {
                get { return isentoIOF; }
            }
            #endregion

            /// <summary>
            /// Construtor
            /// </summary>
            public CadastroRendaFixa(int? idAssessor, string isentoIR, string isentoIOF)
            {
                this.idAssessor = idAssessor;
                this.isentoIR = isentoIR;
                this.isentoIOF = isentoIOF;
            }

            public bool HasValue()
            {
                return this.isentoIR != "" &&
                       this.isentoIOF != "";
            }
        }

        private int idCliente;
        private string nome;
        private string apelido;
        private TipoPessoa tipoPessoa;
        private string cnpj;
        private string isentoIR;
        private string isentoIOF;
        private StatusAtivoCliente statusCliente;
        private DateTime? dataImplantacao;
        private string codigoInterface;
        private Endereco endereco = null;
        private Endereco enderecoComercial = null;
        private CadastroBolsa cadastroBolsa = null;
        private CadastroBMF cadastroBMF = null;
        private CadastroRendaFixa cadastroRendaFixa = null;
        private string localNegociacao;
        public int IdCliente
        {
            get { return idCliente; }
            set { idCliente = value; }
        }

        public string Nome
        {
            get { return nome; }
            set { nome = value; }
        }

        public string Apelido
        {
            get { return apelido; }
            set { apelido = value; }
        }

        public string Cnpj
        {
            get { return cnpj; }
            set { cnpj = value; }
        }

        public string IsentoIOF
        {
            get { return isentoIOF; }
            set { isentoIOF = value; }
        }

        public string IsentoIR
        {
            get { return isentoIR; }
            set { isentoIR = value; }
        }

        public string CodigoInterface
        {
            get { return codigoInterface; }
            set { codigoInterface = value; }
        }

        public DateTime? DataImplantacao
        {
            get { return dataImplantacao; }
            set { dataImplantacao = value; }
        }

        public CRM.Enums.TipoPessoa TipoPessoa
        {
            get { return tipoPessoa; }
            set { tipoPessoa = value; }
        }

        public StatusAtivoCliente StatusCliente
        {
            get { return statusCliente; }
            set { statusCliente = value; }
        }

        public Endereco EnderecoPessoa
        {
            get { return endereco; }
            set { endereco = value; }
        }

        public Endereco EnderecoComercialPessoa
        {
            get { return enderecoComercial; }
            set { enderecoComercial = value; }
        }

        public CadastroBolsa Bolsa
        {
            get { return cadastroBolsa; }
            set { cadastroBolsa = value; }
        }

        public CadastroBMF BMF
        {
            get { return cadastroBMF; }
            set { cadastroBMF = value; }
        }

        public CadastroRendaFixa RendaFixa
        {
            get { return cadastroRendaFixa; }
            set { cadastroRendaFixa = value; }
        }

        public string LocalNegociacao
        {
            get { return localNegociacao; }
            set { localNegociacao = value; }
        }
    }
}