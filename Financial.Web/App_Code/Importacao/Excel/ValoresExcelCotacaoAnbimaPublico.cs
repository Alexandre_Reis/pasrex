﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Financial.Integracao.Excel
{

    /// <summary>
    /// Armazena os Valores de Cotação Anbima Publico presentes num arquivo Excel
    /// </summary>
    public class ValoresExcelCotacaoAnbimaPublico {
        private DateTime data;
        private string descricao;
        private string codigoSelic;
        private DateTime dataEmissao;
        private DateTime dataVencimento;
        private decimal taxaIndicativa;
        private decimal pu;
        

        public DateTime Data {
            get { return data; }
            set { data = value; }
        }

        public string Descricao
        {
            get { return descricao; }
            set { descricao = value; }
        }

        public string CodigoSelic
        {
            get { return codigoSelic; }
            set { codigoSelic = value; }
        }

        public DateTime DataVencimento
        {
            get { return dataVencimento; }
            set { dataVencimento = value; }
        }

        public DateTime DataEmissao
        {
            get { return dataEmissao; }
            set { dataEmissao = value; }
        }

        public decimal TaxaIndicativa
        {
            get { return taxaIndicativa; }
            set { taxaIndicativa = value; }
        }

        public decimal Pu
        {
            get { return pu; }
            set { pu = value; }
        }
        
    }
}