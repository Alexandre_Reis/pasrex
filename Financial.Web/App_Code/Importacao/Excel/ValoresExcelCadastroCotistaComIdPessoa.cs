using System;
using System.Collections.Generic;
using System.Text;
using Financial.CRM.Enums;
using Financial.InvestidorCotista.Enums;

namespace Financial.Integracao.Excel
{

    /// <summary>
    /// Armazena os Valores de Cotista/Pessoa/PessoEndereco presentes num arquivo Excel
    /// </summary>
    public class ValoresExcelCadastroCotistaComIdPessoa
    {
        private string nome;
        private string apelido;
        private string isentoIR;
        private string isentoIOF;
        private StatusAtivoCotista statusCotista;
        private TipoCotista? tipoCotistaCVM;
        private string codigoInterface;
        private TipoTributacaoCotista tipoTributacaoCotista;
        private DateTime? dataExpiracao;
        private int idClienteEspelho;
        private int idPessoa;
        private int? idCarteira;
        private string criadoPor;

        public string Nome { get { return nome; } set { nome = value; } }
        public string Apelido { get { return apelido; } set { apelido = value; } }
        public string IsentoIR { get { return isentoIR; } set { isentoIR = value; } }
        public string IsentoIOF { get { return isentoIOF; } set { isentoIOF = value; } }
        public StatusAtivoCotista StatusCotista { get { return statusCotista; } set { statusCotista = value; } }
        public TipoCotista? TipoCotistaCVM { get { return tipoCotistaCVM; } set { tipoCotistaCVM = value; } }
        public string CodigoInterface { get { return codigoInterface; } set { codigoInterface = value; } }
        public TipoTributacaoCotista TipoTributacaoCotista { get { return tipoTributacaoCotista; } set { tipoTributacaoCotista = value; } }
        public int IdClienteEspelho { get { return idClienteEspelho; } set { idClienteEspelho = value; } }
        public int IdPessoa { get { return idPessoa; } set { idPessoa = value; } }
        public int? IdCarteira { get { return idCarteira; } set { idCarteira = value; } }
        public string CriadoPor { get { return criadoPor; } set { criadoPor = value; } }

    }
}