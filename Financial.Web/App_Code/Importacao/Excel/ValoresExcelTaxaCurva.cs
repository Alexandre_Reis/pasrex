using System;
using System.Collections.Generic;
using System.Text;

namespace Financial.Integracao.Excel
{

    /// <summary>
    /// Armazena os Valores de Taxa Cuva Renda Fixa presentes num arquivo Excel
    /// </summary>
    public class ValoresExcelTaxaCurva
    {
        private int idCurvaRendaFixa;
        private DateTime dataBase;
        private DateTime dataVertice;        
        private string codigoVertice;
        private int prazoDU;
        private int prazoDC;
        private decimal taxa;

        public int IdCurvaRendaFixa
        {
            get { return idCurvaRendaFixa; }
            set { idCurvaRendaFixa = value; }
        }

        public DateTime DataBase
        {
            get { return dataBase; }
            set { dataBase = value; }
        }

        public DateTime DataVertice
        {
            get { return dataVertice; }
            set { dataVertice = value; }
        }

        public string CodigoVertice
        {
            get { return codigoVertice; }
            set { codigoVertice = value; }
        }

        public int PrazoDU
        {
            get { return prazoDU; }
            set { prazoDU = value; }
        }

        public int PrazoDC
        {
            get { return prazoDC; }
            set { prazoDC = value; }
        }

        public decimal Taxa
        {
            get { return taxa; }
            set { taxa = value; }
        }
    }
}