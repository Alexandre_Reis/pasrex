using System;
using System.Collections.Generic;
using System.Text;
using Financial.RendaFixa.Enums;

namespace Financial.Integracao.Excel {
    
    /// <summary>
    /// Armazena os Valores de AgendaEventosRendaFixaReduzida Presentes num Arquivo Excel
    /// </summary>
    public class ValoresExcelAgendaRendaFixaReduzida {
        private DateTime dataAgenda;
        private DateTime dataEvento;
        private DateTime dataPagamento;

        private TipoEventoTitulo tipo;
        private decimal taxa;
        private decimal valor;

        public DateTime DataAgenda {
            get { return dataAgenda; }
            set { dataAgenda = value; }
        }

        public DateTime DataEvento {
            get { return dataEvento; }
            set { dataEvento = value; }
        }

        public DateTime DataPagamento {
            get { return dataPagamento; }
            set { dataPagamento = value; }
        }

        public TipoEventoTitulo Tipo {
            get { return tipo; }
            set { tipo = value; }
        }

        public decimal Taxa {
            get { return taxa; }
            set { taxa = value; }
        }

        public decimal Valor {
            get { return valor; }
            set { valor = value; }
        }
    }
}
