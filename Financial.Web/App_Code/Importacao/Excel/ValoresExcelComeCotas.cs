using System;  
using System.Collections.Generic;
using System.Text;

namespace Financial.Integracao.Excel
{

    /// <summary>
    /// Armazena os valores de PosicaoFundo presentes num arquivo Excel
    /// </summary>
    public class ValoresExcelComeCotas
    {
        private string idPosicaoComeCotas;
        private int idCarteira;
        private int tipoEvento;
        private DateTime dataAgendamentoEvento;
        private DateTime dataPagamentoIR_Evento;
        private DateTime dataUltimaCobrancaIR;
        private decimal quantidadeCotas;
        private decimal quantidadeAntesCortes;
        private decimal valorCotaAplicacao;
        private decimal valorCotaUltimoPagamentoIR;
        private decimal valorCotaDataAgendamento;
        private decimal aliquotaIR_ComeCotas;
        private decimal rendimentoBrutoDesdeAplicacao;
        private decimal rendimentoDesdeUltimoPagamentoIR;
        private int prazoCautela;
        private int prazoIOF;
        private decimal aliquotaIOF;
        private decimal valorIOF;
        private decimal valorIOFVirtual;
        private decimal prejuizoUtilizado;
        private decimal rendimentoCompensado;
        private decimal valorIR_Agendado;
        private decimal valorIR_Pago;
        private decimal residuo_15;
        private decimal residuo_175;
        private decimal residuo_20;
        private decimal residuo_225;
        private decimal quantidadeCotasComeCotas;
        private decimal quantidadeCotasFechamento;
        private int codExecucaoRecolhimentoIR;
        private int codFormaUsoAliquotaIR;
        private int tipoPosicao;

        public string IdPosicaoComeCotas { get { return idPosicaoComeCotas;} set { idPosicaoComeCotas= value; } } 
        public int IdCarteira { get { return idCarteira;} set { idCarteira= value; } } 
        public int TipoEvento { get { return tipoEvento;} set { tipoEvento= value; } } 
        public DateTime DataAgendamentoEvento { get { return dataAgendamentoEvento;} set { dataAgendamentoEvento= value; } } 
        public DateTime DataPagamentoIR_Evento { get { return dataPagamentoIR_Evento;} set { dataPagamentoIR_Evento= value; } } 
        public DateTime DataUltimaCobrancaIR { get { return dataUltimaCobrancaIR;} set { dataUltimaCobrancaIR= value; } } 
        public decimal QuantidadeCotas { get { return quantidadeCotas;} set { quantidadeCotas= value; } } 
        public decimal QuantidadeAntesCortes { get { return quantidadeAntesCortes;} set { quantidadeAntesCortes= value; } } 
        public decimal ValorCotaAplicacao { get { return valorCotaAplicacao;} set { valorCotaAplicacao= value; } } 
        public decimal ValorCotaUltimoPagamentoIR { get { return valorCotaUltimoPagamentoIR;} set { valorCotaUltimoPagamentoIR= value; } } 
        public decimal ValorCotaDataAgendamento { get { return valorCotaDataAgendamento;} set { valorCotaDataAgendamento= value; } } 
        public decimal AliquotaIR_ComeCotas { get { return aliquotaIR_ComeCotas;} set { aliquotaIR_ComeCotas= value; } } 
        public decimal RendimentoBrutoDesdeAplicacao { get { return rendimentoBrutoDesdeAplicacao;} set { rendimentoBrutoDesdeAplicacao= value; } } 
        public decimal RendimentoDesdeUltimoPagamentoIR { get { return rendimentoDesdeUltimoPagamentoIR;} set { rendimentoDesdeUltimoPagamentoIR= value; } } 
        public int PrazoCautela { get { return prazoCautela;} set { prazoCautela= value; } } 
        public int PrazoIOF { get { return prazoIOF;} set { prazoIOF= value; } } 
        public decimal AliquotaIOF { get { return aliquotaIOF;} set { aliquotaIOF= value; } } 
        public decimal ValorIOF { get { return valorIOF;} set { valorIOF= value; } } 
        public decimal ValorIOFVirtual { get { return valorIOFVirtual;} set { valorIOFVirtual= value; } } 
        public decimal PrejuizoUtilizado { get { return prejuizoUtilizado;} set { prejuizoUtilizado= value; } } 
        public decimal RendimentoCompensado { get { return rendimentoCompensado;} set { rendimentoCompensado= value; } } 
        public decimal ValorIR_Agendado { get { return valorIR_Agendado;} set { valorIR_Agendado= value; } } 
        public decimal ValorIR_Pago { get { return valorIR_Pago;} set { valorIR_Pago= value; } } 
        public decimal Residuo_15 { get { return residuo_15;} set { residuo_15= value; } } 
        public decimal Residuo_175 { get { return residuo_175;} set { residuo_175= value; } } 
        public decimal Residuo_20 { get { return residuo_20;} set { residuo_20= value; } } 
        public decimal Residuo_225 { get { return residuo_225;} set { residuo_225= value; } } 
        public decimal QuantidadeCotasComeCotas { get { return quantidadeCotasComeCotas;} set { quantidadeCotasComeCotas= value; } } 
        public decimal QuantidadeCotasFechamento { get { return quantidadeCotasFechamento;} set { quantidadeCotasFechamento= value; } } 
        public int CodExecucaoRecolhimentoIR { get { return codExecucaoRecolhimentoIR;} set { codExecucaoRecolhimentoIR= value; } } 
        public int CodFormaUsoAliquotaIR { get { return codFormaUsoAliquotaIR;} set { codFormaUsoAliquotaIR= value; } } 
        public int TipoPosicao { get { return tipoPosicao;} set { tipoPosicao= value; } } 


    }
}
