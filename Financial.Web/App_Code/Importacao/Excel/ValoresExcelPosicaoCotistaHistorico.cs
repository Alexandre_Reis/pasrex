using System;
using System.Collections.Generic;
using System.Text;

namespace Financial.Integracao.Excel
{

    /// <summary>
    /// Armazena os valores de PosicaoCotistaHistorico presentes num arquivo Excel
    /// </summary>
    public class ValoresExcelPosicaoCotistaHistorico
    {
        private DateTime dataHistorico;
        private int idCotista;
        private int idCarteira;
        private decimal valorAplicacao;
        private DateTime dataAplicacao;
        private DateTime dataConversao;
        private decimal quantidade;
        private decimal cotaAplicacao;
        private DateTime? dataUltimaCobrancaIR;
        private int? idOperacao;

        public DateTime DataHistorico {
            get { return dataHistorico; }
            set { dataHistorico = value; }
        }

        public int IdCotista
        {
            get { return idCotista; }
            set { idCotista = value; }
        }

        public int IdCarteira
        {
            get { return idCarteira; }
            set { idCarteira = value; }
        }

        public decimal ValorAplicacao
        {
            get { return valorAplicacao; }
            set { valorAplicacao = value; }
        }

        public DateTime DataAplicacao
        {
            get { return dataAplicacao; }
            set { dataAplicacao = value; }
        }

        public DateTime DataConversao
        {
            get { return dataConversao; }
            set { dataConversao = value; }
        }

        public decimal Quantidade
        {
            get { return quantidade; }
            set { quantidade = value; }
        }

        public decimal CotaAplicacao
        {
            get { return cotaAplicacao; }
            set { cotaAplicacao = value; }
        }

        public DateTime? DataUltimaCobrancaIR
        {
            get { return dataUltimaCobrancaIR; }
            set { dataUltimaCobrancaIR = value; }
        }

        public int? IdOperacao
        {
            get { return idOperacao; }
            set { idOperacao = value; }
        }
    }
}