using System;
using System.Collections.Generic;
using System.Text;
using Financial.RendaFixa.Enums;

namespace Financial.Integracao.Excel {
    public enum EnumIndice {
        CDI = 1,
        SELIC = 10,
        Dolar = 21,
        TR = 40,
        IGPM = 60,
        IPCA = 61,
        INPC = 62,
        IGPDI = 63,
        IBOVESPA = 70,
        IBRX = 80,
        IBRX50 = 90,
        TJLP = 200,
        ANBID = 210,
        UFIR = 220,
        EURO = 500,
        YENE = 510,
        NONE = 999999 // Para indices Nulos
    }

    /// <summary>
    /// Armazena os valores de Liquidacao presentes num arquivo Excel
    /// </summary>
    public class ValoresExcelTituloRendaFixa {
        private int? idTitulo;
        private ClasseRendaFixa idClasse;
        private EnumIndice idIndice;
        private string descricao;
        private decimal? taxa;
        private decimal? percentual;
        private decimal puNominal;
        private DateTime dataEmissao;
        private DateTime dataVencimento;
        private string isentoIR;
        private string isentoIOF;
        private string codigoIsin;
        private string codigoCetip;
        private int idEmissor;
        private string codigoCustodia;
        private int idMoeda;
        private int? idEstrategia;
        private int? codigoCDA;
        private string codigoCBLC;
        private char debentureConversivel;
        private string codigoInterface;
        private char debentureInfra;
        private decimal? premioRebate;
        private int? periodicidade;
        private int criterioAmortizacao;
        private char permiteRepactuacao;
        private char executaProRataEmissao;
        private int tipoProRata;
        private int defasagemLiquidacao;
        private char proRataLiquidacao;
        private int? proRataEmissaoDia;
        private string ativoRegra;
        private DateTime? dataInicioCorrecao;
        private int? defasagemMeses;
        private int? eJuros;
        private int? eJurosTipo;
        private string codigoCusip;
        private string empSecuritizada;
        private int? codigoBDS;
        private string opcaoEmbutida;
        private int? tipoOpcao;
        private int? tipoExercicio;
        private int? tipoPremio;
        private DateTime? dataInicioExercicio;
        private DateTime? dataVencimentoOpcao;
        private decimal? valorPremio;

        public int? IdTitulo {
            get { return idTitulo; }
            set { idTitulo = value; }
        }

        public ClasseRendaFixa IdClasse {
            get { return idClasse; }
            set { idClasse = value; }
        }

        public EnumIndice IdIndice {
            get { return idIndice; }
            set { idIndice = value; }
        }

        public string Descricao {
            get { return descricao; }
            set { descricao = value; }
        }

        public decimal? Taxa {
            get { return taxa; }
            set { taxa = value; }
        }

        public decimal? Percentual {
            get { return percentual; }
            set { percentual = value; }
        }

        public decimal PuNominal {
            get { return puNominal; }
            set { puNominal = value; }
        }

        public DateTime DataEmissao {
            get { return dataEmissao; }
            set { dataEmissao = value; }
        }

        public DateTime DataVencimento {
            get { return dataVencimento; }
            set { dataVencimento = value; }
        }

        public string IsentoIR {
            get { return isentoIR; }
            set { isentoIR = value; }
        }

        public string IsentoIOF {
            get { return isentoIOF; }
            set { isentoIOF = value; }
        }

        public int IdEmissor
        {
            get { return idEmissor; }
            set { idEmissor = value; }
        }

        public int IdMoeda
        {
            get { return idMoeda; }
            set { idMoeda = value; }
        }

        public int? IdEstrategia
        {
            get { return idEstrategia; }
            set { idEstrategia = value; }
        }

        public int? CodigoCDA
        {
            get { return codigoCDA; }
            set { codigoCDA = value; }
        }

        public string CodigoIsin
        {
            get { return codigoIsin; }
            set { codigoIsin = value; }
        }

        public string CodigoCetip
        {
            get { return codigoCetip; }
            set { codigoCetip = value; }
        }

        public string CodigoCBLC
        {
            get { return codigoCBLC; }
            set { codigoCBLC = value; }
        }

        public char DebentureConversivel
        {
            get { return debentureConversivel; }
            set { debentureConversivel = value; }
        }


        public string CodigoInterface
        {
            get { return codigoInterface; }
            set { codigoInterface = value; }
        }

        public char DebentureInfra
        {
            get { return debentureInfra; }
            set { debentureInfra = value; }
        }

        public decimal? PremioRebate
        {
            get { return premioRebate; }
            set { premioRebate = value; }
        }

        public int? Periodicidade
        {
            get { return periodicidade; }
            set { periodicidade = value; }
        }

        public int CriterioAmortizacao
        {
            get { return criterioAmortizacao; }
            set { criterioAmortizacao = value; }
        }

        public char PermiteRepactuacao
        {
            get { return permiteRepactuacao; }
            set { permiteRepactuacao = value; }
        }

        public char ExecutaProRataEmissao
        {
            get { return executaProRataEmissao; }
            set { executaProRataEmissao = value; }
        }

        public int TipoProRata
        {
            get { return tipoProRata; }
            set { tipoProRata = value; }
        }

        public int DefasagemLiquidacao
        {
            get { return defasagemLiquidacao; }
            set { defasagemLiquidacao = value; }
        }

        public char ProRataLiquidacao
        {
            get { return proRataLiquidacao; }
            set { proRataLiquidacao = value; }
        }

        public int? ProRataEmissaoDia
        {
            get { return proRataEmissaoDia; }
            set { proRataEmissaoDia = value; }
        }

        public string AtivoRegra
        {
            get { return ativoRegra; }
            set { ativoRegra = value; }
        }

        public DateTime? DataInicioCorrecao
        {
            get { return dataInicioCorrecao; }
            set { dataInicioCorrecao = value; }
        }

        public int? DefasagemMeses
        {
            get { return defasagemMeses; }
            set { defasagemMeses = value; }
        }

        public int? EJuros
        {
            get { return eJuros; }
            set { eJuros = value; }
        }

        public string CodigoCusip
        {
            get { return codigoCusip; }
            set { codigoCusip = value; }
        }


        public string CodigoCustodia
        {
            get { return codigoCustodia; }
            set { codigoCustodia = value; }
        }

        public int? EJurosTipo
        {
            get { return eJurosTipo; }
            set { eJurosTipo = value; }
        }

        public string EmpSecuritizada
        {
            get { return empSecuritizada; }
            set { empSecuritizada = value; }
        }

        public int? CodigoBDS
        {
            get { return codigoBDS; }
            set { codigoBDS = value; }
        }

        public string OpcaoEmbutida
        {
            get { return opcaoEmbutida; }
            set { opcaoEmbutida = value; }
        }

        public int? TipoOpcao
        {
            get { return tipoOpcao; }
            set { tipoOpcao = value; }
        }

        public int? TipoExercicio
        {
            get { return tipoExercicio; }
            set { tipoExercicio = value; }
        }

        public int? TipoPremio
        {
            get { return tipoPremio; }
            set { tipoPremio = value; }
        }

        public DateTime? DataInicioExercicio
        {
            get { return dataInicioExercicio; }
            set { dataInicioExercicio = value; }
        }

        public DateTime? DataVencimentoOpcao
        {
            get { return dataVencimentoOpcao; }
            set { dataVencimentoOpcao = value; }
        }

        public Decimal? ValorPremio
        {
            get { return valorPremio; }
            set { valorPremio = value; }
        }		
		
    }
}
