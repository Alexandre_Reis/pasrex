using System;
using System.Collections.Generic;
using System.Text;
using Financial.RendaFixa.Enums;

namespace Financial.Integracao.Excel {
    
    /// <summary>
    /// Armazena os Valores de AgendaEventosRendaFixa Presentes num Arquivo Excel
    /// </summary>
    public class ValoresExcelAgendaEventosRendaFixa : ValoresExcelAgendaRendaFixaReduzida {
        
        // Construtor
        public ValoresExcelAgendaEventosRendaFixa() : base() {
        }

        private int idTitulo;

        public int IdTitulo {
            get { return idTitulo; }
            set { idTitulo = value; }
        }
    }
}
