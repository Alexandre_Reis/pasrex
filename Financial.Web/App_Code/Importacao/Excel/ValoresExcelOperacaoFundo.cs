using System;
using System.Collections.Generic;
using System.Text;

namespace Financial.Integracao.Excel
{

    /// <summary>
    /// Armazena os valores de OperacaoFundo presentes num arquivo Excel
    /// </summary>
    public class ValoresExcelOperacaoFundo
    {
        private int idCliente;
        private int idCarteira;
        private DateTime dataOperacao;
        private DateTime? dataRegistro;
        private DateTime? dataConversao;
        private DateTime? dataLiquidacao;
        private byte tipoOperacao;
        private byte tipoResgate;
        private decimal quantidade;
        private decimal cotaOperacao;
        private decimal valorBruto;
        private decimal valorLiquido;
        private decimal valorIR;
        private decimal valorIOF;
        private decimal valorPerformance;
        private decimal rendimentoResgate;
        private string localNegociacao;
        private int? fieModalidade;
        private int? fieTabelaIr;
        private DateTime? dataAplicacaoCautelaResgate;
        private int? idPosicaoResgatadaCotista;
        private int? idPosicaoResgatadaFundo;
        private string observacaoCotista;
        private string observacaoFundo;
        private decimal? cotaInformada;
        private string trader;

        public decimal? CotaInformada
        {
            get { return cotaInformada; }
            set { cotaInformada = value; }
        }

        public int IdCliente
        {
            get { return idCliente; }
            set { idCliente = value; }
        }

        public int IdCarteira
        {
            get { return idCarteira; }
            set { idCarteira = value; }
        }

        public DateTime DataOperacao
        {
            get { return dataOperacao; }
            set { dataOperacao = value; }
        }

        public DateTime? DataConversao
        {
            get { return dataConversao; }
            set { dataConversao = value; }
        }

        public DateTime? DataLiquidacao
        {
            get { return dataLiquidacao; }
            set { dataLiquidacao = value; }
        }

        public byte TipoOperacao
        {
            get { return tipoOperacao; }
            set { tipoOperacao = value; }
        }

        public byte TipoResgate
        {
            get { return tipoResgate; }
            set { tipoResgate = value; }
        }

        public decimal Quantidade
        {
            get { return quantidade; }
            set { quantidade = value; }
        }

        public decimal CotaOperacao
        {
            get { return cotaOperacao; }
            set { cotaOperacao = value; }
        }

        public decimal ValorBruto
        {
            get { return valorBruto; }
            set { valorBruto = value; }
        }

        public decimal ValorLiquido
        {
            get { return valorLiquido; }
            set { valorLiquido = value; }
        }

        public decimal ValorIR
        {
            get { return valorIR; }
            set { valorIR = value; }
        }

        public decimal ValorIOF
        {
            get { return valorIOF; }
            set { valorIOF = value; }
        }

        public decimal ValorPerformance
        {
            get { return valorPerformance; }
            set { valorPerformance = value; }
        }

        public decimal RendimentoResgate
        {
            get { return rendimentoResgate; }
            set { rendimentoResgate = value; }
        }

        public string LocalNegociacao
        {
            get { return localNegociacao; }
            set { localNegociacao = value; }
        }

        public int? FieModalidade
        {
            get { return fieModalidade; }
            set { fieModalidade = value; }
        }

        public int? FieTabelaIr
        {
            get { return fieTabelaIr; }
            set { fieTabelaIr = value; }
        }

        public DateTime? DataAplicacaoCautelaResgate
        {
            get { return dataAplicacaoCautelaResgate; }
            set { dataAplicacaoCautelaResgate = value; }
        }

        public int? IdPosicaoResgatadaCotista
        {
            get { return idPosicaoResgatadaCotista; }
            set { idPosicaoResgatadaCotista = value; }

        }

        public int? IdPosicaoResgatadaFundo
        {
            get { return idPosicaoResgatadaFundo; }
            set { idPosicaoResgatadaFundo = value; }
        }

        public string ObservacaoCotista
        {
            get { return observacaoCotista; }
            set { observacaoCotista = value; }
        }

        public string ObservacaoFundo
        {
            get { return observacaoFundo; }
            set { observacaoFundo = value; }
        }

        public DateTime? DataRegistro
        {
            get { return dataRegistro; }
            set { dataRegistro = value; }
        }

        public string Trader
        {
            get { return trader; }
            set { trader = value; }
        }
    }
}
