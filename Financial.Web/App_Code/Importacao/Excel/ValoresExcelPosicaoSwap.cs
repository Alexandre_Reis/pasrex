using System;
using System.Collections.Generic;
using System.Text;
using Financial.Swap.Enums;

namespace Financial.Integracao.Excel {

    /// <summary>
    /// Armazena os valores de PosicaoSwap presentes num arquivo Excel
    /// </summary>
    public class ValoresExcelPosicaoSwap {
        private int idCliente;
        private TipoRegistroSwap tipoRegistro;
        private string numeroContrato;
        private DateTime dataEmissao;
        private DateTime dataVencimento;
        private decimal valorBase;
        private TipoPontaSwap tipoPonta;
        private int? idIndice;
        private decimal taxaJuros;
        private decimal percentual;
        private TipoApropriacaoSwap tipoApropriacao;
        private ContagemDiasSwap contagemDias;
        private BaseCalculoSwap baseAno;
        private PontaSwap pontaContraParte;
        private int? idIndiceContraParte;
        private decimal taxaJurosContraParte;
        private decimal percentualContraParte;
        private TipoApropriacaoSwap tipoApropriacaoContraParte;
        private ContagemDiasSwap contagemDiasContraParte;
        private BaseCalculoSwap baseAnoContraParte;
        private decimal valorParte;
        private decimal valorContraParte;
        private decimal saldo;
        //
        public int IdCliente {
            get { return idCliente; }
            set { idCliente = value; }
        }
        public TipoRegistroSwap TipoRegistro {
            get { return tipoRegistro; }
            set { tipoRegistro = value; }
        }
        public string NumeroContrato {
            get { return numeroContrato; }
            set { numeroContrato = value; }
        }
        public DateTime DataEmissao {
            get { return dataEmissao; }
            set { dataEmissao = value; }
        }
        public DateTime DataVencimento {
            get { return dataVencimento; }
            set { dataVencimento = value; }
        }
        public decimal ValorBase {
            get { return valorBase; }
            set { valorBase = value; }
        }
        public TipoPontaSwap TipoPonta {
            get { return tipoPonta; }
            set { tipoPonta = value; }
        }
        public int? IdIndice {
            get { return idIndice; }
            set { idIndice = value; }
        }
        public decimal TaxaJuros {
            get { return taxaJuros; }
            set { taxaJuros = value; }
        }
        public decimal Percentual {
            get { return percentual; }
            set { percentual = value; }
        }
        public TipoApropriacaoSwap TipoApropriacao {
            get { return tipoApropriacao; }
            set { tipoApropriacao = value; }
        }
        public ContagemDiasSwap ContagemDias {
            get { return contagemDias; }
            set { contagemDias = value; }
        }
        public BaseCalculoSwap BaseAno {
            get { return baseAno; }
            set { baseAno = value; }
        }
        public PontaSwap PontaContraParte {
            get { return pontaContraParte; }
            set { pontaContraParte = value; }
        }
        public int? IdIndiceContraParte {
            get { return idIndiceContraParte; }
            set { idIndiceContraParte = value; }
        }
        public decimal TaxaJurosContraParte {
            get { return taxaJurosContraParte; }
            set { taxaJurosContraParte = value; }
        }
        public decimal PercentualContraParte {
            get { return percentualContraParte; }
            set { percentualContraParte = value; }
        }
        public TipoApropriacaoSwap TipoApropriacaoContraParte {
            get { return tipoApropriacaoContraParte; }
            set { tipoApropriacaoContraParte = value; }
        }
        public ContagemDiasSwap ContagemDiasContraParte {
            get { return contagemDiasContraParte; }
            set { contagemDiasContraParte = value; }
        }
        public BaseCalculoSwap BaseAnoContraParte {
            get { return baseAnoContraParte; }
            set { baseAnoContraParte = value; }
        }
        public decimal ValorParte {
            get { return valorParte; }
            set { valorParte = value; }
        }
        public decimal ValorContraParte {
            get { return valorContraParte; }
            set { valorContraParte = value; }
        }
        public decimal Saldo {
            get { return saldo; }
            set { saldo = value; }
        }
    }
}
