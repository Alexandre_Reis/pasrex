﻿using System;
using System.Collections.Generic;
using System.Text;
using Financial.CRM.Enums;
using Financial.Investidor.Enums;

namespace Financial.Integracao.Excel
{

    /// <summary>
    /// Armazena os valores de Cliente/Pessoa/PessoEndereco presentes num arquivo Excel
    /// </summary>
    public class ValoresExcelCadastroClienteCarteira
    {
        public class CadastroBolsa
        {
            protected byte? tipoCotacao;
            protected int? idAssessor;
            protected string isentoIR;
            protected string pessoaVinculada;

            #region Getters
            public byte? TipoCotacao
            {
                get { return tipoCotacao; }
            }
            public int? IdAssessor
            {
                get { return idAssessor; }
            }
            public string IsentoIR
            {
                get { return isentoIR; }
            }
            public string PessoaVinculada
            {
                get { return pessoaVinculada; }
            }
            #endregion

            /// <summary>
            /// Construtor
            /// </summary>
            public CadastroBolsa(byte tipoCotacao, int? idAssessor, string isentoIR, string pessoaVinculada)
            {
                this.tipoCotacao = tipoCotacao;
                this.idAssessor = idAssessor;
                this.isentoIR = isentoIR;
                this.pessoaVinculada = pessoaVinculada;
            }
                        
            public bool HasValue()
            {
                return this.tipoCotacao.HasValue &&
                       this.isentoIR != "";
            }
        }

        public class CadastroBMF
        {
            protected byte? tipoCotacao;
            protected int? idAssessor;
            
            #region Getters
            public byte? TipoCotacao
            {
                get { return tipoCotacao; }
            }
            public int? IdAssessor
            {
                get { return idAssessor; }
            }
            #endregion

            /// <summary>
            /// Construtor
            /// </summary>
            public CadastroBMF(byte tipoCotacao, int? idAssessor)
            {
                this.tipoCotacao = tipoCotacao;
                this.idAssessor = idAssessor;                
            }
                        
            public bool HasValue()
            {
                return this.tipoCotacao.HasValue;
            }
        }

        public class CadastroRendaFixa
        {
            protected int? idAssessor;
            protected string isentoIR;
            protected string isentoIOF;

            #region Getters
            public int? IdAssessor
            {
                get { return idAssessor; }
            }
            public string IsentoIR
            {
                get { return isentoIR; }
            }
            public string IsentoIOF
            {
                get { return isentoIOF; }
            }
            #endregion

            /// <summary>
            /// Construtor
            /// </summary>
            public CadastroRendaFixa(int? idAssessor, string isentoIR, string isentoIOF)
            {
                this.idAssessor = idAssessor;
                this.isentoIR = isentoIR;
                this.isentoIOF = isentoIOF;
            }

            public bool HasValue()
            {
                return this.isentoIR != "" &&
                       this.isentoIOF != "";
            }
        }

        private int idPessoa;
        private string nome;
        private string apelido;
        private string isentoIR;
        private string isentoIOF;
		private int? idTipo;
		private byte tipoCota;
		private string buscaCotaAnterior;
        private StatusAtivoCliente statusAtivo;
        private DateTime? dataImplantacao;
        private byte tipoCarteira;
        private decimal cotaInicial;
        private string truncaCota;
        private int? idIndiceBenchmark;
        private byte casasDecimaisCota;
        private byte casasDecimaisQuantidade;
        private int? idCategoria;
        private int? idSubCategoria;
        private int idAgenteAdministrador;
        private int idAgenteCustodiante;
        private int idAgenteGestor;
        private string cobraTaxaFiscalizacaoCVM;
        private string calculaEnquadra;
        private string calculaMTM;
        private byte diasCotizacaoAplicacao;
        private byte diasLiquidacaoAplicacao;
        private string calculaIOF;
        private byte tipoCusto;
        private byte diasCotizacaoResgate;
        private byte diasLiquidacaoResgate;
        private string calculaPrazoMedio;
        private byte contagemDiasConversaoResgate;
        private byte prioridadeOperacao;
        private byte tipoTributacao;
        private byte tipoFundo;
        private byte projecaoIRComeCotas;
        private byte projecaoIRCotista;
        private byte diasAposComeCotas;
        private byte diasAposResgate;
        private string localNegociacao;
        private CadastroBolsa cadastroBolsa = null;
        private CadastroBMF cadastroBMF = null;
        private CadastroRendaFixa cadastroRendaFixa = null;
        private byte? tipoControle;
        private int? idGrupoProcessamento;

        public int IdPessoa { get { return idPessoa; } set { idPessoa = value; } }
        public string Nome { get { return nome; } set { nome = value; } }
        public string Apelido { get { return apelido; } set { apelido = value; } }
        public string IsentoIR { get { return isentoIR; } set { isentoIR = value; } }
        public string IsentoIOF { get { return isentoIOF; } set { isentoIOF = value; } }
		public int? IdTipo { get { return idTipo;} set { idTipo = value; } } 
		public byte TipoCota { get { return tipoCota;} set { tipoCota = value; } } 
		public string BuscaCotaAnterior { get { return buscaCotaAnterior;} set { buscaCotaAnterior = value; } } 
        public StatusAtivoCliente StatusAtivo { get { return statusAtivo; } set { statusAtivo = value; } }
        public DateTime? DataImplantacao { get { return dataImplantacao; } set { dataImplantacao = value; } }
        public byte TipoCarteira { get { return tipoCarteira; } set { tipoCarteira = value; } }
        public decimal CotaInicial { get { return cotaInicial; } set { cotaInicial = value; } }
        public string TruncaCota { get { return truncaCota; } set { truncaCota = value; } }
        public int? IdIndiceBenchmark { get { return idIndiceBenchmark; } set { idIndiceBenchmark = value; } }
        public byte CasasDecimaisCota { get { return casasDecimaisCota; } set { casasDecimaisCota = value; } }
        public byte CasasDecimaisQuantidade { get { return casasDecimaisQuantidade; } set { casasDecimaisQuantidade = value; } }
        public int? IdCategoria { get { return idCategoria; } set { idCategoria = value; } }
        public int? IdSubCategoria { get { return idSubCategoria; } set { idSubCategoria = value; } }
        public int IdAgenteAdministrador { get { return idAgenteAdministrador; } set { idAgenteAdministrador = value; } }
        public int IdAgenteCustodiante { get { return idAgenteCustodiante; } set { idAgenteCustodiante = value; } }
        public int IdAgenteGestor { get { return idAgenteGestor; } set { idAgenteGestor = value; } }
        public string CobraTaxaFiscalizacaoCVM { get { return cobraTaxaFiscalizacaoCVM; } set { cobraTaxaFiscalizacaoCVM = value; } }
        public string CalculaEnquadra { get { return calculaEnquadra; } set { calculaEnquadra = value; } }
        public string CalculaMTM { get { return calculaMTM; } set { calculaMTM = value; } }
        public byte DiasCotizacaoAplicacao { get { return diasCotizacaoAplicacao; } set { diasCotizacaoAplicacao = value; } }
        public byte DiasLiquidacaoAplicacao { get { return diasLiquidacaoAplicacao; } set { diasLiquidacaoAplicacao = value; } }
        public string CalculaIOF { get { return calculaIOF; } set { calculaIOF = value; } }
        public byte TipoCusto { get { return tipoCusto; } set { tipoCusto = value; } }
        public byte DiasCotizacaoResgate { get { return diasCotizacaoResgate; } set { diasCotizacaoResgate = value; } }
        public byte DiasLiquidacaoResgate { get { return diasLiquidacaoResgate; } set { diasLiquidacaoResgate = value; } }
        public string CalculaPrazoMedio { get { return calculaPrazoMedio; } set { calculaPrazoMedio = value; } }
        public byte ContagemDiasConversaoResgate { get { return contagemDiasConversaoResgate; } set { contagemDiasConversaoResgate = value; } }
        public byte PrioridadeOperacao { get { return prioridadeOperacao; } set { prioridadeOperacao = value; } }
        public byte TipoTributacao { get { return tipoTributacao; } set { tipoTributacao = value; } }
        public byte TipoFundo { get { return tipoFundo; } set { tipoFundo = value; } }
        public byte ProjecaoIRComeCotas { get { return projecaoIRComeCotas; } set { projecaoIRComeCotas = value; } }
        public byte ProjecaoIRCotista { get { return projecaoIRCotista; } set { projecaoIRCotista = value; } }
        public byte DiasAposComeCotas { get { return diasAposComeCotas; } set { diasAposComeCotas = value; } }
        public byte DiasAposResgate { get { return diasAposResgate; } set { diasAposResgate = value; } }
        public string LocalNegociacao { get { return localNegociacao; } set { localNegociacao = value; } }
        public byte? TipoControle { get { return tipoControle; } set { tipoControle = value; } }
        public int? IdGrupoProcessamento { get { return idGrupoProcessamento; } set { idGrupoProcessamento = value; } }
        
        public CadastroBolsa Bolsa
        {
            get { return cadastroBolsa; }
            set { cadastroBolsa = value; }
        }

        public CadastroBMF BMF
        {
            get { return cadastroBMF; }
            set { cadastroBMF = value; }
        }

        public CadastroRendaFixa RendaFixa
        {
            get { return cadastroRendaFixa; }
            set { cadastroRendaFixa = value; }
        }
    }
}