﻿using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Financial.Web.Common;
using System.IO;
using Financial.Integracao.Excel.Util;
using Financial.Integracao.Excel;
using EntitySpaces.Interfaces;
using Financial.Common;
using Financial.Bolsa;
using Financial.Bolsa.Enums;
using Financial.Investidor;
using System.Collections.Generic;
using Financial.Investidor.Enums;
using DevExpress.Web;
using Financial.Fundo;
using Financial.Util;
using Financial.Security;
using Financial.Web.Util;
using Financial.Security.Enums;
using Financial.Util.Enums;
using Financial.Fundo.Enums;

public partial class ImportacaoBasePage : BasePage {

    /* Estrutura do Excel */
    private List<ValoresExcelPosicaoFundo> valoresExcelPosicaoFundo = new List<ValoresExcelPosicaoFundo>();
    List<int> idClienteDeletarPosicaoFundo = new List<int>();
    List<DateTime> dataHistoricoDeletarPosicaoFundo = new List<DateTime>();

    /// <summary>
    /// Leitura de um arquivo Excel
    /// Saida: List<ValoresExcel> com os valores armazenados no arquivo
    /// </summary>
    /// <param name="streamExcel">Stream de bytes de um arquivo Excel</param>
    /// <exception cref="Exception">Se ocorreu problemas</exception>
    private void LerArquivoPosicaoFundo(Stream streamExcel) {

        #region Leitura do Arquivo Excel

        // Open Spreadsheet
        Bytescout.Spreadsheet.Spreadsheet document = new Bytescout.Spreadsheet.Spreadsheet();
        document.LoadFromStream(streamExcel);

        Bytescout.Spreadsheet.Worksheet workSheet = document.Workbook.Worksheets[0];
        //
        #region Confere Formato Arquivo
        string[] colunasConferencias = new string[] 
                            {"IdCliente","IdCarteira","ValorAplicacao","DataAplicacao","DataConversao",
					         "Quantidade","CotaAplicacao","DataUltimaCobrancaIR","DataUltimoCortePfee","DataLiquidacao"};

        bool formato = ValoresExcelUtil.isExcelFormatoValido(colunasConferencias, workSheet);

        if (!formato) {
            string mensagem = "Formato Interno do Arquivo Inválido.\n";
            mensagem += "Linha 1 do Arquivo deve ser: \n";
            for (int i = 0; i < colunasConferencias.Length; i++) {
                int coluna = i + 1;
                mensagem += "\tColuna " + coluna + ": " + colunasConferencias[i] + "\n";
            }

            document.Close();
            document.Dispose();

            throw new Exception(mensagem);
        }
        #endregion

        //
        int index = 0;
        // This row,column index should be changed as per your need.
        // i.e. which cell in the excel you are interesting to read.
        //
        /* Formato: 1)IdCliente  - 2)IdCarteira    - 3)ValorAplicacao       - 4)DataAplicacao    - 5)DataConversao
         *          6)Quantidade - 7)CotaAplicacao - 8)DataUltimaCobrancaIR - 9)DataUltimoCortePfee - 10)DataLiquidacao
         */
        int linha = 1;
        int coluna1 = 0, coluna2 = 1, coluna3 = 2,
            coluna4 = 3, coluna5 = 4, coluna6 = 5,
            coluna7 = 6, coluna8 = 7, coluna9 = 8,
            coluna10 = 9;

        //            
        try {
            // Enquanto idCliente tiver valor
            while (workSheet.Cell(linha, coluna1).Value != null) {
                ValoresExcelPosicaoFundo item = new ValoresExcelPosicaoFundo();
                
                try {
                    item.IdCliente = workSheet.Cell(linha, coluna1).ValueAsInteger;
                }
                catch (Exception ex) {
                    throw new Exception(ex.Message + " - IdCliente: linha " + (linha + 1));
                }

                if (workSheet.Cell(linha, coluna2).Value == null)
                    throw new Exception("IdCarteira não informado: linha " + (linha + 1));
                try
                {
                    item.IdCarteira = workSheet.Cell(linha, coluna2).ValueAsInteger;
                }
                catch (Exception ex) {
                    throw new Exception(ex.Message + " - IdCarteira: linha " + (linha + 1));
                }

                if (workSheet.Cell(linha, coluna3).Value == null)
                    throw new Exception("ValorAplicacao não informado: linha " + (linha + 1));
                try
                {
                    item.ValorAplicacao = Convert.ToDecimal(workSheet.Cell(linha, coluna3).Value);
                }
                catch (Exception ex) {
                    throw new Exception(ex.Message + " - ValorAplicacao: linha " + (linha + 1));
                }

                if (workSheet.Cell(linha, coluna4).Value == null)
                    throw new Exception("DataAplicacao não informada: linha " + (linha + 1));
                try
                {
                    item.DataAplicacao = workSheet.Cell(linha, coluna4).ValueAsDateTime;
                }
                catch (Exception ex) {
                    throw new Exception(ex.Message + " - DataAplicacao: linha " + (linha + 1));
                }

                if (workSheet.Cell(linha, coluna5).Value == null)
                    throw new Exception("DataConversao não informada: linha " + (linha + 1));
                try
                {
                    item.DataConversao = workSheet.Cell(linha, coluna5).ValueAsDateTime;
                }
                catch (Exception ex) {
                    throw new Exception(ex.Message + " - DataConversao: linha " + (linha + 1));
                }

                if (workSheet.Cell(linha, coluna6).Value == null)
                    throw new Exception("Quantidade não informada: linha " + (linha + 1));
                try
                {
                    item.Quantidade = Convert.ToDecimal(workSheet.Cell(linha, coluna6).Value);
                }
                catch (Exception ex) {
                    throw new Exception(ex.Message + " - Quantidade: linha " + (linha + 1));
                }

                if (workSheet.Cell(linha, coluna7).Value == null)
                    throw new Exception("CotaAplicacao não informada: linha " + (linha + 1));
                try
                {
                    item.CotaAplicacao = Convert.ToDecimal(workSheet.Cell(linha, coluna7).Value);
                }
                catch (Exception ex) {
                    throw new Exception(ex.Message + " - CotaAplicacao: linha " + (linha + 1));
                }

                if (workSheet.Cell(linha, coluna8).Value != null) {
                    try {
                        item.DataUltimaCobrancaIR = workSheet.Cell(linha, coluna8).ValueAsDateTime;
                    }
                    catch (Exception ex) {
                        throw new Exception(ex.Message + " - DataUltimaCobrancaIR: linha " + (linha + 1));
                    }
                }

                if (workSheet.Cell(linha, coluna9).Value != null)
                {
                    try {
                        item.DataUltimoCortePfee = workSheet.Cell(linha, coluna9).ValueAsDateTime;
                    }
                    catch (Exception ex) {
                        throw new Exception(ex.Message + " - DataUltimoCortePfee: linha " + (linha + 1));
                    }
                }

                if (workSheet.Cell(linha, coluna10).Value != null)
                {
                    try
                    {
                        item.DataLiquidacao = workSheet.Cell(linha, coluna10).ValueAsDateTime;
                    }
                    catch (Exception ex)
                    {
                        throw new Exception(ex.Message + " - DataLiquidacao: linha " + (linha + 1));
                    }
                }
                else
                    item.DataLiquidacao = item.DataAplicacao;


                this.valoresExcelPosicaoFundo.Add(item);
                //
                index++;
                linha = 1 + index;
                //                

                Console.WriteLine("{0},{1},{2},{3},{4},{5},{6},{7},{8},{9}",
                    item.IdCliente, item.IdCarteira, item.ValorAplicacao, item.DataAplicacao, item.DataConversao,
                    item.Quantidade, item.CotaAplicacao, item.DataUltimaCobrancaIR, item.DataUltimoCortePfee,
                    item.DataLiquidacao);

            }
        }
        catch (Exception ex) {
            document.Close();
            document.Dispose();

            throw new Exception(ex.Message);
        }
        #endregion

        document.Close();
        document.Dispose();
    }

    /// <summary>
    /// Gera e carrega as posições Fundo e Posições Fundo Abertura de acordo 
    /// com o objeto List<ValoresExcel>
    /// </summary>
    /// <exception cref="Exception">Se ocorreu problemas</exception>
    private void CarregaPosicaoFundo() {
        OperacaoFundoCollection operacaoFundoCollection = new OperacaoFundoCollection();
        OperacaoFundoCollection operacaoFundoDeletarCollection = new OperacaoFundoCollection();

        PosicaoFundoCollection posicaoFundoCollection = new PosicaoFundoCollection();
        PosicaoFundoCollection posicaoFundoDeletarCollection = new PosicaoFundoCollection();
        //            
        PosicaoFundoAberturaCollection posicaoFundoAberturaCollection = new PosicaoFundoAberturaCollection();
        PosicaoFundoAberturaCollection posicaoFundoAberturaDeletarCollection = new PosicaoFundoAberturaCollection();
        //
        //ImportacaoComeCotasCollection importacaoComeCotasCollection = new ImportacaoComeCotasCollection();    

        for (int i = 0; i < this.valoresExcelPosicaoFundo.Count; i++) {
            ValoresExcelPosicaoFundo valoresExcel = this.valoresExcelPosicaoFundo[i];
            //
            this.idClienteDeletarPosicaoFundo.Add(valoresExcel.IdCliente);

            #region Verifica se Existe Carteira
            Carteira carteira = new Carteira();
            if (!carteira.LoadByPrimaryKey(valoresExcel.IdCarteira)) {
                throw new Exception("Carteira Não existente : " + valoresExcel.IdCarteira);
            }
            #endregion

            #region DataDia do Cliente
            Cliente cliente = new Cliente();
            List<esQueryItem> campos = new List<esQueryItem>();
            campos.Add(cliente.Query.IdCliente);
            campos.Add(cliente.Query.DataDia);
            campos.Add(cliente.Query.DataImplantacao);
            if (!cliente.LoadByPrimaryKey(campos, valoresExcel.IdCliente)) {
                throw new Exception("Cliente Não existente : " + valoresExcel.IdCliente);
            }
            // Salva a Data Historico para poder Deletar
            this.dataHistoricoDeletarPosicaoFundo.Add(cliente.DataDia.Value);
            #endregion

            if (valoresExcel.DataConversao >= cliente.DataImplantacao.Value)
                throw new Exception(" Data de Conversão deve ser menor que a data de Implantação do Cliente: " + valoresExcel.IdCarteira);

            if (valoresExcel.CotaAplicacao == 0)
            {
                HistoricoCota historicoCotaAplicacao = new HistoricoCota();
                if (historicoCotaAplicacao.BuscaValorCota(valoresExcel.IdCarteira, valoresExcel.DataConversao) && historicoCotaAplicacao.CotaFechamento.HasValue)
                {
                    valoresExcel.CotaAplicacao = historicoCotaAplicacao.CotaFechamento.Value;
                }
            }

            #region PosicaoFundo
            PosicaoFundo posicao = new PosicaoFundo();
            // Dados do Arquivo
            posicao.IdCliente = valoresExcel.IdCliente;
            posicao.IdCarteira = valoresExcel.IdCarteira;
            posicao.ValorAplicacao = valoresExcel.ValorAplicacao;
            posicao.DataAplicacao = valoresExcel.DataAplicacao;
            posicao.DataConversao = valoresExcel.DataConversao;
            posicao.Quantidade = valoresExcel.Quantidade;
            posicao.CotaAplicacao = valoresExcel.CotaAplicacao;
            posicao.DataUltimaCobrancaIR = valoresExcel.DataUltimaCobrancaIR != null
                                            ? valoresExcel.DataUltimaCobrancaIR
                                            : cliente.DataDia;
            if (valoresExcel.DataUltimoCortePfee != null)
            {
                posicao.DataUltimoCortePfee = valoresExcel.DataUltimoCortePfee.Value;
            }
            posicao.QuantidadeAntesCortes = valoresExcel.Quantidade;
            posicao.ValorIOFVirtual = valoresExcel.ValorIOFVirtual.GetValueOrDefault(0);
            posicao.ValorRendimento = 0;
            posicao.QuantidadeBloqueada = 0;
            posicao.ValorIR = 0;
            posicao.ValorIOF = 0;
            posicao.ValorPerformance = 0;
            posicao.PosicaoIncorporada = "N";            
            //
            posicao.QuantidadeInicial = valoresExcel.Quantidade;
            posicao.QuantidadeAntesCortes = valoresExcel.Quantidade * AgendaComeCotas.fatorQuantidadeFundo(posicao, cliente.DataImplantacao.Value);

            #region CotaDia
            HistoricoCota historicoCota = new HistoricoCota();
            List<esQueryItem> camposAux = new List<esQueryItem>();
            camposAux.Add(historicoCota.Query.CotaFechamento);
            //
            posicao.CotaDia = historicoCota.LoadByPrimaryKey(camposAux, cliente.DataDia.Value, valoresExcel.IdCarteira)
                              ? historicoCota.CotaFechamento.Value
                              : 0;
            #endregion
            //
            posicao.ValorBruto = carteira.IsTruncaFinanceiro()
                        ? Utilitario.Truncate((posicao.Quantidade.Value * posicao.CotaDia.Value), 2)
                        : Math.Round((posicao.Quantidade.Value * posicao.CotaDia.Value), 2);

            posicao.ValorLiquido = posicao.ValorBruto.Value - posicao.ValorIR.Value - posicao.ValorIOF.Value;
            //
            // Attach the object
            posicaoFundoCollection.AttachEntity(posicao);

            #endregion

            #region Operacao Fundo
            OperacaoFundo operacaoFundo = operacaoFundoCollection.AddNew();
            operacaoFundo.IdCliente = posicao.IdCliente.Value;  
            operacaoFundo.IdCarteira = posicao.IdCarteira.Value;
            operacaoFundo.DataOperacao = posicao.DataAplicacao.Value;
            operacaoFundo.DataConversao = posicao.DataConversao.Value;
            operacaoFundo.DataLiquidacao = valoresExcel.DataLiquidacao.Value;
            operacaoFundo.DataAgendamento = posicao.DataAplicacao.Value;
            operacaoFundo.TipoOperacao = 1;
            operacaoFundo.TipoResgate = null;
            operacaoFundo.IdPosicaoResgatada = null;
            operacaoFundo.IdFormaLiquidacao = 1;
            operacaoFundo.Quantidade = posicao.QuantidadeInicial.Value;
            operacaoFundo.CotaOperacao = posicao.CotaAplicacao.Value;
            operacaoFundo.ValorBruto = posicao.ValorBruto.Value;
            operacaoFundo.ValorLiquido = posicao.ValorLiquido.Value;
            operacaoFundo.ValorIR = posicao.ValorIR.Value;
            operacaoFundo.ValorIOF = posicao.ValorIOF.Value;
            operacaoFundo.ValorCPMF = 0;
            operacaoFundo.ValorPerformance = posicao.ValorPerformance.Value;
            operacaoFundo.PrejuizoUsado = 0;
            operacaoFundo.RendimentoResgate = 0;
            operacaoFundo.VariacaoResgate = 0;
            operacaoFundo.IdConta = null;
            operacaoFundo.Observacao = string.Empty;
            operacaoFundo.Fonte = (byte)FonteOperacaoFundo.Manual;
            operacaoFundo.IdAgenda = null;
            operacaoFundo.CotaInformada = posicao.CotaAplicacao.Value;
            operacaoFundo.IdOperacaoResgatada = null;
            operacaoFundo.RegistroEditado = "N";
            operacaoFundo.ValoresColados = "N";
            operacaoFundo.IdLocalNegociacao = 1;
            operacaoFundo.DataRegistro = posicao.DataConversao.Value;
            #endregion

            #region PosicaoFundoAbertura
            PosicaoFundoAbertura posicaoAbertura = new PosicaoFundoAbertura();
            posicaoAbertura.DataHistorico = cliente.DataDia.Value;
            posicaoAbertura.IdCliente = posicao.IdCliente;
            posicaoAbertura.IdCarteira = posicao.IdCarteira;
            posicaoAbertura.ValorAplicacao = posicao.ValorAplicacao;
            posicaoAbertura.DataAplicacao = posicao.DataAplicacao;
            posicaoAbertura.DataConversao = posicao.DataConversao;
            posicaoAbertura.Quantidade = posicao.Quantidade;
            posicaoAbertura.CotaAplicacao = posicao.CotaAplicacao;
            posicaoAbertura.DataUltimaCobrancaIR = posicao.DataUltimaCobrancaIR;
            posicaoAbertura.DataUltimoCortePfee = posicao.DataUltimoCortePfee;
            posicaoAbertura.QuantidadeAntesCortes = posicao.QuantidadeAntesCortes;
            posicaoAbertura.ValorIOFVirtual = posicao.ValorIOFVirtual;
            posicaoAbertura.ValorLiquido = posicao.ValorLiquido;
            posicaoAbertura.QuantidadeBloqueada = posicao.QuantidadeBloqueada;
            posicaoAbertura.PosicaoIncorporada = "N";
            posicaoAbertura.ValorIR = posicao.ValorIR;
            posicaoAbertura.ValorIOF = posicao.ValorIOF;
            posicaoAbertura.ValorPerformance = posicao.ValorPerformance;
            posicaoAbertura.QuantidadeInicial = posicao.QuantidadeInicial;
            posicaoAbertura.CotaDia = posicao.CotaDia;
            posicaoAbertura.ValorBruto = posicao.ValorBruto;
            posicaoAbertura.ValorRendimento = 0;
            posicaoAbertura.QuantidadeAntesCortes = posicao.QuantidadeAntesCortes;
            //
            // Attach the object
            posicaoFundoAberturaCollection.AttachEntity(posicaoAbertura);
            #endregion

            //#region Importação Come Cotas
            //ImportacaoComeCotas importacaoComeCotas = new ImportacaoComeCotas();
            //importacaoComeCotas.IdPosicaoComeCotas = valoresExcel.IdPosicaoComeCotas;
            //importacaoComeCotas.TipoPosicao = TipoComeCotas.Fundo.GetHashCode();

            //importacaoComeCotasCollection.AttachEntity(importacaoComeCotas);
            //#endregion
        }

        using (esTransactionScope scope = new esTransactionScope())
        {
            operacaoFundoCollection.Save();
            for (int i = 0; i < operacaoFundoCollection.Count; i++)
            {
                OperacaoFundo operacaoFundo = operacaoFundoCollection[i];
                PosicaoFundo posicaoFundo = posicaoFundoCollection[i];
                PosicaoFundoAbertura posicaoFundoAbertura = posicaoFundoAberturaCollection[i];

                posicaoFundo.IdOperacao = operacaoFundo.IdOperacao.Value;
                posicaoFundoAbertura.IdOperacao = operacaoFundo.IdOperacao.Value;
            }

           
            ImportacaoComeCotasCollection importacaoCCCollection = new ImportacaoComeCotasCollection();
            operacaoFundoDeletarCollection = new OperacaoFundoCollection();
            List<int?> idOperacaoFundo = new List<int?>();

            posicaoFundoDeletarCollection.Query.Where(posicaoFundoDeletarCollection.Query.IdCliente.In(this.idClienteDeletarPosicaoFundo));
            posicaoFundoDeletarCollection.Query.Load();

            #region Exclui tabela de importacao de come cotas
            foreach (PosicaoFundo posicaoFundo in posicaoFundoDeletarCollection)
            {
                AgendaComeCotasCollection agendaComeCotasCollection = new AgendaComeCotasCollection();
                agendaComeCotasCollection.Query.Where(agendaComeCotasCollection.Query.IdPosicao.Equal(posicaoFundo.IdPosicao.Value),
                                                        agendaComeCotasCollection.Query.TipoPosicao.Equal(TipoComeCotas.Fundo.GetHashCode()));
                agendaComeCotasCollection.Query.Load();
                agendaComeCotasCollection.MarkAllAsDeleted();
                agendaComeCotasCollection.Save();
            }
            #endregion

            #region Deleta PosicaoFundoAbertura por IdCliente/DataHistorico
            for (int i = 0; i < this.idClienteDeletarPosicaoFundo.Count; i++)
            {
                posicaoFundoAberturaDeletarCollection = new PosicaoFundoAberturaCollection();
                posicaoFundoAberturaDeletarCollection.Query
                .Where(posicaoFundoAberturaDeletarCollection.Query.IdCliente == this.idClienteDeletarPosicaoFundo[i],
                       posicaoFundoAberturaDeletarCollection.Query.DataHistorico == this.dataHistoricoDeletarPosicaoFundo[i]);
                posicaoFundoAberturaDeletarCollection.Query.Load();
                posicaoFundoAberturaDeletarCollection.MarkAllAsDeleted();
                posicaoFundoAberturaDeletarCollection.Save();
            }            

            operacaoFundoDeletarCollection.MarkAllAsDeleted();
            posicaoFundoDeletarCollection.MarkAllAsDeleted();
            operacaoFundoDeletarCollection.Save();
            posicaoFundoDeletarCollection.Save();
            #endregion

            if (idOperacaoFundo.Count > 0)
            {
                operacaoFundoDeletarCollection.Query.Where(operacaoFundoDeletarCollection.Query.IdOperacao.In(idOperacaoFundo));
                operacaoFundoDeletarCollection.Query.Load();
            }

            posicaoFundoDeletarCollection.MarkAllAsDeleted();
            operacaoFundoDeletarCollection.MarkAllAsDeleted();
            posicaoFundoDeletarCollection.Save();
            operacaoFundoDeletarCollection.Save();

            operacaoFundoCollection.Save();
            posicaoFundoCollection.Save();

            importacaoCCCollection.MarkAllAsDeleted();
            importacaoCCCollection.Save();
            

            for (int i = 0; i < operacaoFundoCollection.Count; i++)
            {
                PosicaoFundo posicaoFundo = posicaoFundoCollection[i];
                PosicaoFundoAbertura posicaoFundoAbertura = posicaoFundoAberturaCollection[i];

                posicaoFundoAbertura.IdPosicao = posicaoFundo.IdPosicao.Value;
            }

            posicaoFundoAberturaCollection.Save();

            scope.Complete();
        }
    }

    /// <summary>
    /// Processa a Planilha de PosicaoFundo após ter acabado o Upload
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void uplPosicaoFundo_FileUploadComplete(object sender, FileUploadCompleteEventArgs e) {
        e.CallbackData = "";

        #region Trata Extensão Válida
        if (!ValoresExcelUtil.isExtensaoExcel(e.UploadedFile.FileName.Trim())) {
            e.CallbackData = "Importação Posição Fundo - Formato de Arquivo Inválido. Formato permitido: .xls ou .xlsx\n\n";
            return;
        }
        #endregion

        // Stream de bytes com o Conteudo do Arquivo Excel
        Stream sr = e.UploadedFile.FileContent;

        #region Processamento Arquivo Excel

        //Reseta Vetor de Dados
        this.valoresExcelPosicaoFundo = new List<ValoresExcelPosicaoFundo>();
        //
        this.idClienteDeletarPosicaoFundo = new List<int>();
        this.dataHistoricoDeletarPosicaoFundo = new List<DateTime>();

        try {
            // Ler Arquivo
            this.LerArquivoPosicaoFundo(sr);
            // Carrega Arquivo
            this.CarregaPosicaoFundo();
        }
        catch (Exception e2) {
            e.CallbackData = "Importação Posição Fundo - " + e2.Message;
            return;
        }

        new HistoricoLog().InsereHistoricoLog(DateTime.Now, DateTime.Now,
                "Importação Planilha Posição Fundo",
                HttpContext.Current.User.Identity.Name, UtilitarioWeb.GetIP(Request), "", HistoricoLogOrigem.Outros);

        #endregion
    }
}