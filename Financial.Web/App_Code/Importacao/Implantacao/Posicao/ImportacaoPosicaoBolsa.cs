﻿using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Financial.Web.Common;
using System.IO;
using Financial.Integracao.Excel.Util;
using Financial.Integracao.Excel;
using EntitySpaces.Interfaces;
using Financial.Common;
using Financial.Bolsa;
using Financial.Bolsa.Enums;
using Financial.Investidor;
using System.Collections.Generic;
using Financial.Investidor.Enums;
using DevExpress.Web;
using Financial.Common.Exceptions;
using Financial.Bolsa.Exceptions;
using Financial.Security;
using Financial.Web.Util;
using Financial.Security.Enums;

public partial class ImportacaoBasePage : BasePage {

    /* Estrutura do Excel */
    private List<ValoresExcelPosicaoBolsa> valoresExcelPosicaoBolsa = new List<ValoresExcelPosicaoBolsa>();
    List<int> idClienteDeletarPosicaoBolsa = new List<int>();
    List<DateTime> dataHistoricoDeletarPosicaoBolsa = new List<DateTime>();

    /// <summary>
    /// Leitura de um arquivo Excel
    /// Saida: List<ValoresExcel> com os valores armazenados no arquivo
    /// </summary>
    /// <param name="streamExcel">Stream de bytes de um arquivo Excel</param>
    /// <exception cref="Exception">Se ocorreu problemas</exception>
    private void LerArquivoPosicaoBolsa(Stream streamExcel) {

        #region Leitura do Arquivo Excel
        // Open Spreadsheet
        Bytescout.Spreadsheet.Spreadsheet document = new Bytescout.Spreadsheet.Spreadsheet();
        document.LoadFromStream(streamExcel);

        Bytescout.Spreadsheet.Worksheet workSheet = document.Workbook.Worksheets[0];

        #region Confere Formato Arquivo
        string[] colunasConferencias = new string[] {"IdCliente","CodigoBovespa","CdAtivoBolsa",
                                                        "Quantidade","PUCusto"};

        bool formato = ValoresExcelUtil.isExcelFormatoValido(colunasConferencias, workSheet);

        if (!formato) {
            string mensagem = "Formato Interno do Arquivo Inválido.\n";
            mensagem += "Linha 1 do Arquivo deve ser: \n";
            for (int i = 0; i < colunasConferencias.Length; i++) {
                int coluna = i + 1;
                mensagem += "\tColuna " + coluna + ": " + colunasConferencias[i] + "\n";
            }

            document.Close();
            document.Dispose();

            throw new Exception(mensagem);
        }
        #endregion

        //
        int index = 0;
        // This row,column index should be changed as per your need.
        // i.e. which cell in the excel you are interesting to read.
        //
        /* Formato: 1)IdCliente -  2)CodigoBovespa - 3)CdAtivoBolsa - 4)Quantidade - 5)PuCusto - 6)QuantidadeBloqueada             
         */
        int linha = 1;
        int coluna1 = 0, coluna2 = 1, coluna3 = 2,
            coluna4 = 3, coluna5 = 4, coluna6 = 5;

        //
        try {
            // Enquanto idCliente tiver valor
            while (workSheet.Cell(linha, coluna1).Value != null) {
                ValoresExcelPosicaoBolsa item = new ValoresExcelPosicaoBolsa();
                
                try {
                    item.IdCliente = workSheet.Cell(linha, coluna1).ValueAsInteger;
                }
                catch (Exception ex) {
                    throw new Exception(ex.Message + " - IdCliente: linha " + (linha + 1));
                }

                if (workSheet.Cell(linha, coluna2).Value == null)
                    throw new Exception("CodigoBovespa não informado: linha " + (linha + 1));
                try
                {
                    item.CodigoBovespa = workSheet.Cell(linha, coluna2).ValueAsInteger;
                }
                catch (Exception ex) {
                    throw new Exception(ex.Message + " - CodigoBovespa: linha " + (linha + 1));
                }

                if (workSheet.Cell(linha, coluna3).Value == null)
                    throw new Exception("CdAtivoBolsa não informado: linha " + (linha + 1));
                try
                {
                    item.CdAtivoBolsa = workSheet.Cell(linha, coluna3).ValueAsString;
                }
                catch (Exception ex) {
                    throw new Exception(ex.Message + " - CdAtivoBolsa: linha " + (linha + 1));
                }

                if (workSheet.Cell(linha, coluna4).Value == null)
                    throw new Exception("Quantidade não informado: linha " + (linha + 1));
                try
                {
                    item.Quantidade = Convert.ToInt32(workSheet.Cell(linha, coluna4).Value);
                }
                catch (Exception ex) {
                    throw new Exception(ex.Message + " - Quantidade: linha " + (linha + 1));
                }

                if (workSheet.Cell(linha, coluna5).Value == null)
                    throw new Exception("PuCusto não informado: linha " + (linha + 1));
                try
                {
                    item.PuCusto = Convert.ToDecimal(workSheet.Cell(linha, coluna5).Value);
                }
                catch (Exception ex) {
                    throw new Exception(ex.Message + " - PuCusto: linha " + (linha + 1));
                }

                #region QuantidadeBloqueada
                if (workSheet.Cell(linha, coluna6).Value != null)
                {
                    try
                    {
                        item.QuantidadeBloqueada = workSheet.Cell(linha, coluna6).ValueAsInteger;
                    }
                    catch (Exception ex)
                    {
                        throw new Exception(ex.Message + " - QuantidadeBloqueada: linha " + (linha + 1));
                    }
                }
                else
                {
                    item.QuantidadeBloqueada = 0;
                }
                #endregion

                this.valoresExcelPosicaoBolsa.Add(item);
                //
                index++;
                linha = 1 + index;
                //                
                Console.WriteLine("{0},{1},{2},{3},{4},{5} ", item.IdCliente, item.CodigoBovespa, item.CdAtivoBolsa, item.Quantidade, item.PuCusto, item.QuantidadeBloqueada);
            }
        }
        catch (Exception ex) {
            document.Close();
            document.Dispose();

            throw new Exception(ex.Message);
        }
        #endregion

        document.Close();
        document.Dispose();
    }

    /// <summary>
    /// Gera e carrega as posições Bolsa e Posição Bolsa Abertura de acordo 
    /// com o objeto List<ValoresExcel>
    /// </summary>
    /// <exception cref="Exception">Se ocorreu problemas</exception>
    private void CarregaPosicaoBolsa() {
        PosicaoBolsaCollection posicaoBolsaCollection = new PosicaoBolsaCollection();
        PosicaoBolsaCollection posicaoBolsaDeletarCollection = new PosicaoBolsaCollection();
        //
        PosicaoBolsaAberturaCollection posicaoBolsaAberturaCollection = new PosicaoBolsaAberturaCollection();
        PosicaoBolsaAberturaCollection posicaoBolsaAberturaDeletarCollection = new PosicaoBolsaAberturaCollection();
        //    
        for (int i = 0; i < this.valoresExcelPosicaoBolsa.Count; i++) {
            ValoresExcelPosicaoBolsa valoresExcel = this.valoresExcelPosicaoBolsa[i];
            // Salva o Cliente para poder Deletar
            this.idClienteDeletarPosicaoBolsa.Add(valoresExcel.IdCliente);
            //               
            #region Testa AgenteMercado
            AgenteMercado agenteMercado = new AgenteMercado();
            int? idAgenteMercado = null;
            try {
                idAgenteMercado = (int)agenteMercado.BuscaIdAgenteMercadoBovespa(valoresExcel.CodigoBovespa);
            }
            catch (IdAgenteNaoCadastradoException e) {
                throw new IdAgenteNaoCadastradoException(e.Message);
            }
            #endregion

            if (idAgenteMercado.HasValue) {
                #region PosicaoBolsa
                //
                PosicaoBolsa posicao = new PosicaoBolsa();
                posicao.IdCliente = valoresExcel.IdCliente;
                posicao.IdAgente = idAgenteMercado.Value;
                posicao.CdAtivoBolsa = valoresExcel.CdAtivoBolsa.ToUpper().Trim();
                posicao.Quantidade = valoresExcel.Quantidade;
                posicao.PUCusto = valoresExcel.PuCusto;
                posicao.PUCustoLiquido = valoresExcel.PuCusto;

                AtivoBolsa ativoBolsa = new AtivoBolsa();
                if (!ativoBolsa.LoadByPrimaryKey(posicao.CdAtivoBolsa))
                {
                    throw new Exception("Ativo bolsa não cadastrado : " + posicao.CdAtivoBolsa);
                }
                //
                posicao.TipoMercado = ativoBolsa.TipoMercado;
                //
                #region PUMercado/ValorMercado/ValorCustoLiquido
                Cliente cliente = new Cliente();
                List<esQueryItem> campos = new List<esQueryItem>();
                campos.Add(cliente.Query.IdCliente);
                campos.Add(cliente.Query.DataDia);
                if (!cliente.LoadByPrimaryKey(campos, valoresExcel.IdCliente)) {
                    throw new Exception("Cliente Não existente : " + valoresExcel.IdCliente);
                }
                else 
                {
                    if (ativoBolsa.DataVencimento.HasValue)
                    {
                        if (posicao.CdAtivoBolsa.Contains("IBOV"))
                        {
                            posicao.DataVencimento = ativoBolsa.RetornaDataVencimentoOpcaoIBOV(cliente.DataDia.Value, ativoBolsa.DataVencimento.Value);
                        }
                        else
                        {
                            posicao.DataVencimento = ativoBolsa.RetornaDataVencimentoOpcao(cliente.DataDia.Value, ativoBolsa.DataVencimento.Value);
                        }
                    }

                    // Salva a Data Historico para poder Deletar
                    this.dataHistoricoDeletarPosicaoBolsa.Add(cliente.DataDia.Value);
                    //                        
                    CotacaoBolsa cotacaoBolsa = new CotacaoBolsa();
                    if (!cotacaoBolsa.LoadByPrimaryKey(cliente.DataDia.Value, valoresExcel.CdAtivoBolsa.ToUpper())) 
                    {
                        posicao.PUMercado = posicao.PUCusto.Value;
                    }
                    else {
                        // Seta PuMercado
                        posicao.PUMercado = cotacaoBolsa.PUFechamento;
                    }

                    #region Seta ValorMercado/ValorCustoLiquido
                    FatorCotacaoBolsa fatorCotacaoBolsa = new FatorCotacaoBolsa();
                    if (!fatorCotacaoBolsa.BuscaFatorCotacaoBolsa(valoresExcel.CdAtivoBolsa.ToUpper(), cliente.DataDia.Value)) {
                        throw new FatorCotacaoNaoCadastradoException("FatorCotacaoBolsa Não existente : " + cliente.DataDia.Value.ToString("d") + ", " + valoresExcel.CdAtivoBolsa.ToUpper());
                    }
                    else {
                        posicao.ValorMercado = ((valoresExcel.Quantidade * posicao.PUMercado) / fatorCotacaoBolsa.Fator.Value);
                        posicao.ValorCustoLiquido = ((valoresExcel.Quantidade * posicao.PUCustoLiquido) / fatorCotacaoBolsa.Fator.Value);
                    }
                    #endregion
                }
                #endregion
                //
                posicao.QuantidadeInicial = valoresExcel.Quantidade;
                posicao.QuantidadeBloqueada = valoresExcel.QuantidadeBloqueada;
                //
                posicao.ResultadoRealizar = posicao.ValorMercado - posicao.ValorCustoLiquido;
                //
                // Attach the entity
                posicaoBolsaCollection.AttachEntity(posicao);

                #endregion
                //
                #region PosicaoBolsaAbertura
                //
                PosicaoBolsaAbertura posicaoAbertura = new PosicaoBolsaAbertura();
                posicaoAbertura.IdCliente = posicao.IdCliente;
                posicaoAbertura.DataHistorico = cliente.DataDia.Value;
                posicaoAbertura.IdAgente = posicao.IdAgente;
                posicaoAbertura.CdAtivoBolsa = posicao.CdAtivoBolsa;
                posicaoAbertura.Quantidade = posicao.Quantidade;
                posicaoAbertura.PUCusto = posicao.PUCusto;
                posicaoAbertura.PUCustoLiquido = posicao.PUCustoLiquido;
                posicaoAbertura.TipoMercado = posicao.TipoMercado;
                posicaoAbertura.DataVencimento = posicao.DataVencimento;
                posicaoAbertura.PUMercado = posicao.PUMercado;
                posicaoAbertura.ValorMercado = posicao.ValorMercado;
                posicaoAbertura.ValorCustoLiquido = posicao.ValorCustoLiquido;
                posicaoAbertura.QuantidadeInicial = posicao.QuantidadeInicial;
                posicaoAbertura.QuantidadeBloqueada = posicao.QuantidadeBloqueada;
                posicaoAbertura.ResultadoRealizar = posicao.ResultadoRealizar;

                // Attach the object
                posicaoBolsaAberturaCollection.AttachEntity(posicaoAbertura);
                #endregion
            }
        }

        using (esTransactionScope scope = new esTransactionScope()) {

            #region Deleta PosicaoBolsa por IdCliente
            posicaoBolsaDeletarCollection.Query.Where(posicaoBolsaDeletarCollection.Query.IdCliente.In(this.idClienteDeletarPosicaoBolsa));
            posicaoBolsaDeletarCollection.Query.Load();
            posicaoBolsaDeletarCollection.MarkAllAsDeleted();
            posicaoBolsaDeletarCollection.Save();
            #endregion

            #region Deleta PosicaoBolsaAbertura por IdCliente/DataHistorico
            for (int i = 0; i < this.idClienteDeletarPosicaoBolsa.Count; i++) {
                posicaoBolsaAberturaDeletarCollection = new PosicaoBolsaAberturaCollection();
                posicaoBolsaAberturaDeletarCollection.Query
                    .Where(posicaoBolsaAberturaDeletarCollection.Query.IdCliente == this.idClienteDeletarPosicaoBolsa[i],
                           posicaoBolsaAberturaDeletarCollection.Query.DataHistorico == this.dataHistoricoDeletarPosicaoBolsa[i]);
                posicaoBolsaAberturaDeletarCollection.Query.Load();
                posicaoBolsaAberturaDeletarCollection.MarkAllAsDeleted();
                posicaoBolsaAberturaDeletarCollection.Save();
            }
            #endregion

            // Salva Posicões presentes no Excel
            posicaoBolsaCollection.Save();

            // Seta os novos idPosicao na PosicaoAbertura
            for (int i = 0; i < posicaoBolsaCollection.Count; i++) {
                posicaoBolsaAberturaCollection[i].IdPosicao = posicaoBolsaCollection[i].IdPosicao;
            }
            posicaoBolsaAberturaCollection.Save();
            //
            scope.Complete();
        }
    }

    /// <summary>
    /// Processa a Planilha de PosicaoBolsa após ter acabado o Upload
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void uplPosicaoBolsa_FileUploadComplete(object sender, FileUploadCompleteEventArgs e) {
        e.CallbackData = "";

        #region Trata Extensão Válida
        if (!ValoresExcelUtil.isExtensaoExcel(e.UploadedFile.FileName.Trim())) {
            e.CallbackData = "Importação Posição Bolsa - Formato de Arquivo Inválido. Formato permitido: .xls ou .xlsx\n\n";
            return;
        }
        #endregion

        // Stream de bytes com o Conteudo do Arquivo Excel
        Stream sr = e.UploadedFile.FileContent;

        #region Processamento Arquivo Excel

        //Reseta Vetor de Dados
        this.valoresExcelPosicaoBolsa = new List<ValoresExcelPosicaoBolsa>();
        //
        this.idClienteDeletarPosicaoBolsa = new List<int>();
        this.dataHistoricoDeletarPosicaoBolsa = new List<DateTime>();

        try {
            // Ler Arquivo
            this.LerArquivoPosicaoBolsa(sr);
            // Carrega Arquivo
            this.CarregaPosicaoBolsa();
        }
        catch (Exception e2) {
            e.CallbackData = "Importação Posição Bolsa - " + e2.Message;
            return;
        }

        new HistoricoLog().InsereHistoricoLog(DateTime.Now, DateTime.Now,
                "Importação Planilha Posição Bolsa",
                HttpContext.Current.User.Identity.Name, UtilitarioWeb.GetIP(Request), "", HistoricoLogOrigem.Outros);

        #endregion
    }
}