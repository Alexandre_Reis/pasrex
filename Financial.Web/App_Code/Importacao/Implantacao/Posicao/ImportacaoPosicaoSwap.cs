﻿using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Collections.Generic;

using System.IO;
using Financial.Integracao.Excel.Util;
using Financial.Integracao.Excel;
using Financial.Web.Common;

using EntitySpaces.Interfaces;

using DevExpress.Web;
using Financial.Swap.Enums;
using Financial.Swap;
using Financial.Investidor;
using Financial.Security;
using Financial.Web.Util;
using Financial.Security.Enums;

public partial class ImportacaoBasePage : BasePage {

    /* Estrutura do Excel */
    private List<ValoresExcelPosicaoSwap> valoresExcelPosicaoSwap = new List<ValoresExcelPosicaoSwap>();
    //
    List<int> idClienteDeletarPosicaoSwap = new List<int>();
    List<DateTime> dataHistoricoDeletarPosicaoSwap = new List<DateTime>();

    /// <summary>
    /// Leitura de um arquivo Excel
    /// Saida: List<ValoresExcel> com os valores armazenados no arquivo
    /// </summary>
    /// <param name="streamExcel">Stream de bytes de um arquivo Excel</param>
    /// <exception cref="Exception">Se ocorreu problemas</exception>
    private void LerArquivoPosicaoSwap(Stream streamExcel) {

        #region Leitura do Arquivo Excel

        // Open Spreadsheet
        Bytescout.Spreadsheet.Spreadsheet document = new Bytescout.Spreadsheet.Spreadsheet();
        document.LoadFromStream(streamExcel);

        Bytescout.Spreadsheet.Worksheet workSheet = document.Workbook.Worksheets[0];

        //
        #region Confere Formato Arquivo
        string[] colunasConferencias = new string[] 
                       {"IdCliente","Tipo Registro","Numero Contrato",
                        "Data Emissao","Data Venc.","Valor Base",
                        "Tipo Ponta","IdIndice","Taxa Juros",
                        "Percentual","Tipo Apropriacao","Contagem Dias",
                        "Base Ano","Ponta Contra Parte","IdIndice Contra Parte",
                        "Taxa Juros Contra Parte","Percentual Contra Parte","Apropriacao Contra Parte",
                        "Contagem Dias Contra Parte","Base Ano Contra Parte","Valor Parte",
                        "Valor Contra Parte","Saldo"
                       };

        bool formato = ValoresExcelUtil.isExcelFormatoValido(colunasConferencias, workSheet);

        if (!formato) {
            string mensagem = "Formato Interno do Arquivo Inválido.\n";
            mensagem += "Linha 1 do Arquivo deve ser: \n";
            for (int i = 0; i < colunasConferencias.Length; i++) {
                int coluna = i + 1;
                mensagem += "\tColuna " + coluna + ": " + colunasConferencias[i] + "\n";
            }

            document.Close();
            document.Dispose();

            throw new Exception(mensagem);
        }
        #endregion

        int index = 0;
        // This row,column index should be changed as per your need.
        // i.e. which cell in the excel you are interesting to read.
        //
        /* Formato: 1)IdCliente               
         *          2)TipoRegistro                
         *          3)NumeroContrato             
         *          4)DataEmissao
         *          5)DataVencimento          
         *          6)ValorBase               
         *          7)TipoPonta               
         *          8)IdIndice                   
         *          9)TaxaJuros               
         *          10)Percentual                   
         *          11)TipoApropriacao            
         *          12)ContagemDias
         *          13)BaseAno                
         *          14)TipoPontaContraParte         
         *          15)IdIndiceContraParte        
         *          16)TaxaJurosContraParte
         *          17)PercentualContraParte  
         *          18)TipoApropriacaoContraParte   
         *          19) ContagemDiasContraParte   
         *          20) BaseAnoContraParte        
         *          21)ValorParte            
         *          22)ValorContraParte       
         *          23)Saldo
         */

        int linha = 1;
        int coluna1 = 0, coluna2 = 1, coluna3 = 2,
            coluna4 = 3, coluna5 = 4, coluna6 = 5,
            coluna7 = 6, coluna8 = 7, coluna9 = 8,
            coluna10 = 9, coluna11 = 10, coluna12 = 11,
            coluna13 = 12, coluna14 = 13, coluna15 = 14,
            coluna16 = 15, coluna17 = 16, coluna18 = 17,
            coluna19 = 18, coluna20 = 19, coluna21 = 20,
            coluna22 = 21, coluna23 = 22;

        //
        try {
            // Enquanto idCliente tiver valor
            while (workSheet.Cell(linha, coluna1).Value != null) {
                ValoresExcelPosicaoSwap item = new ValoresExcelPosicaoSwap();
                
                try {
                    item.IdCliente = workSheet.Cell(linha, coluna1).ValueAsInteger;
                }
                catch (Exception ex) {
                    throw new Exception(ex.Message + " - IdCliente: linha " + (linha + 1));
                }

                if (workSheet.Cell(linha, coluna2).Value == null)
                    throw new Exception("TipoRegistro não informado: linha " + (linha + 1));
                try
                {
                    int tipoRegistro = workSheet.Cell(linha, coluna2).ValueAsInteger;
                    item.TipoRegistro = (TipoRegistroSwap)tipoRegistro;
                }
                catch (Exception ex) {
                    throw new Exception(ex.Message + " - TipoRegistro: linha " + (linha + 1));
                }

                if (workSheet.Cell(linha, coluna3).Value == null)
                    throw new Exception("NumeroContrato não informado: linha " + (linha + 1));
                try
                {
                    string numeroContrato = workSheet.Cell(linha, coluna3).Value.ToString().Trim();
                    if (numeroContrato.Length > 20)
                    {
                        numeroContrato = numeroContrato.Substring(0, 20);
                    }
                    item.NumeroContrato = numeroContrato;
                }
                catch (Exception ex) {
                    throw new Exception(ex.Message + " - NumeroContrato: linha " + (linha + 1));
                }

                if (workSheet.Cell(linha, coluna4).Value == null)
                    throw new Exception("DataEmissao não informada: linha " + (linha + 1));
                try
                {
                    item.DataEmissao = workSheet.Cell(linha, coluna4).ValueAsDateTime;
                }
                catch (Exception ex) {
                    throw new Exception(ex.Message + " - DataEmissao: linha " + (linha + 1));
                }

                if (workSheet.Cell(linha, coluna5).Value == null)
                    throw new Exception("DataVencimento não informada: linha " + (linha + 1));
                try
                {
                    item.DataVencimento = workSheet.Cell(linha, coluna5).ValueAsDateTime;
                }
                catch (Exception ex) {
                    throw new Exception(ex.Message + " - DataVencimento: linha " + (linha + 1));
                }

                if (workSheet.Cell(linha, coluna6).Value == null)
                    throw new Exception("ValorBase não informado: linha " + (linha + 1));
                try
                {
                    item.ValorBase = Convert.ToDecimal(workSheet.Cell(linha, coluna6).Value);
                }
                catch (Exception ex) {
                    throw new Exception(ex.Message + " - ValorBase: linha " + (linha + 1));
                }

                if (workSheet.Cell(linha, coluna7).Value == null)
                    throw new Exception("TipoPonta não informado: linha " + (linha + 1));
                try
                {                    
                    int tipoPonta = workSheet.Cell(linha, coluna7).ValueAsInteger;
                    item.TipoPonta = (TipoPontaSwap)tipoPonta;
                }
                catch (Exception ex) {
                    throw new Exception(ex.Message + " - TipoPonta: linha " + (linha + 1));
                }

                if (workSheet.Cell(linha, coluna8).Value != null) {
                    try {
                        item.IdIndice = Convert.ToInt16(workSheet.Cell(linha, coluna8).Value);
                    }
                    catch (Exception ex) {
                        throw new Exception(ex.Message + " - IdIndice: linha " + (linha + 1));
                    }
                }

                if (workSheet.Cell(linha, coluna9).Value == null)
                    throw new Exception("TaxaJuros não informada: linha " + (linha + 1));
                try
                {                    
                    item.TaxaJuros = Convert.ToDecimal(workSheet.Cell(linha, coluna9).Value);
                }
                catch (Exception ex) {
                    throw new Exception(ex.Message + " - TaxaJuros: linha " + (linha + 1));
                }

                if (workSheet.Cell(linha, coluna10).Value == null)
                    throw new Exception("Percentual não informado: linha " + (linha + 1));
                try
                {                    
                    item.Percentual = Convert.ToDecimal(workSheet.Cell(linha, coluna10).Value);
                }
                catch (Exception ex) {
                    throw new Exception(ex.Message + " - Percentual: linha " + (linha + 1));
                }

                if (workSheet.Cell(linha, coluna11).Value == null)
                    throw new Exception("TipoApropriacao não informado: linha " + (linha + 1));
                try
                {                    
                    int tipoApropriacao = workSheet.Cell(linha, coluna11).ValueAsInteger;
                    item.TipoApropriacao = (TipoApropriacaoSwap)tipoApropriacao;
                }
                catch (Exception ex) {
                    throw new Exception(ex.Message + " - TipoApropriacao: linha " + (linha + 1));
                }

                if (workSheet.Cell(linha, coluna12).Value == null)
                    throw new Exception("ContagemDias não informada: linha " + (linha + 1));
                try
                {                    
                    int contagemDias = workSheet.Cell(linha, coluna12).ValueAsInteger;
                    item.ContagemDias = (ContagemDiasSwap)contagemDias;
                }
                catch (Exception ex) {
                    throw new Exception(ex.Message + " - ContagemDias: linha " + (linha + 1));
                }

                if (workSheet.Cell(linha, coluna13).Value == null)
                    throw new Exception("BaseAno não informada: linha " + (linha + 1));
                try
                {                    
                    int baseAno = workSheet.Cell(linha, coluna13).ValueAsInteger;
                    item.BaseAno = (BaseCalculoSwap)baseAno;
                }
                catch (Exception ex) {
                    throw new Exception(ex.Message + " - BaseAno: linha " + (linha + 1));
                }

                if (workSheet.Cell(linha, coluna14).Value == null)
                    throw new Exception("PontaContraParte não informada: linha " + (linha + 1));
                try
                {                    
                    int pontaContraParte = workSheet.Cell(linha, coluna14).ValueAsInteger;
                    item.PontaContraParte = (PontaSwap)pontaContraParte;
                }
                catch (Exception ex) {
                    throw new Exception(ex.Message + " - PontaContraParte: linha " + (linha + 1));
                }

                if (workSheet.Cell(linha, coluna15).Value != null) {
                    try {
                        item.IdIndiceContraParte = Convert.ToInt16(workSheet.Cell(linha, coluna15).Value);
                    }
                    catch (Exception ex) {
                        throw new Exception(ex.Message + " - IdIndiceContraParte: linha " + (linha + 1));
                    }
                }

                if (workSheet.Cell(linha, coluna16).Value == null)
                    throw new Exception("TaxaJurosContraParte não informada: linha " + (linha + 1));
                try
                {                    
                    item.TaxaJurosContraParte = Convert.ToDecimal(workSheet.Cell(linha, coluna16).Value);
                }
                catch (Exception ex) {
                    throw new Exception(ex.Message + " - TaxaJurosContraParte: linha " + (linha + 1));
                }

                if (workSheet.Cell(linha, coluna17).Value == null)
                    throw new Exception("PercentualContraParte não informado: linha " + (linha + 1));
                try
                {                    
                    item.PercentualContraParte = Convert.ToDecimal(workSheet.Cell(linha, coluna17).Value);
                }
                catch (Exception ex) {
                    throw new Exception(ex.Message + " - PercentualContraParte: linha " + (linha + 1));
                }

                if (workSheet.Cell(linha, coluna18).Value == null)
                    throw new Exception("TipoApropriacaoContraParte não informado: linha " + (linha + 1));
                try
                {                    
                    int tipoApropriacaoContraParte = workSheet.Cell(linha, coluna18).ValueAsInteger;
                    item.TipoApropriacaoContraParte = (TipoApropriacaoSwap)tipoApropriacaoContraParte;
                }
                catch (Exception ex) {
                    throw new Exception(ex.Message + " - TipoApropriacaoContraParte: linha " + (linha + 1));
                }

                if (workSheet.Cell(linha, coluna19).Value == null)
                    throw new Exception("ContagemDiasContraParte não informada: linha " + (linha + 1));
                try
                {                    
                    int contagemDiasContraParte = workSheet.Cell(linha, coluna19).ValueAsInteger;
                    item.ContagemDiasContraParte = (ContagemDiasSwap)contagemDiasContraParte;
                }
                catch (Exception ex) {
                    throw new Exception(ex.Message + " - ContagemDiasContraParte: linha " + (linha + 1));
                }

                if (workSheet.Cell(linha, coluna20).Value == null)
                    throw new Exception("BaseAnoContraParte não informada: linha " + (linha + 1));
                try
                {                    
                    int baseAnoContraParte = workSheet.Cell(linha, coluna20).ValueAsInteger;
                    item.BaseAnoContraParte = (BaseCalculoSwap)baseAnoContraParte;
                }
                catch (Exception ex) {
                    throw new Exception(ex.Message + " - BaseAnoContraParte: linha " + (linha + 1));
                }

                if (workSheet.Cell(linha, coluna21).Value == null)
                    throw new Exception("ValorParte não informado: linha " + (linha + 1));
                try
                {                    
                    item.ValorParte = Convert.ToDecimal(workSheet.Cell(linha, coluna21).Value);
                }
                catch (Exception ex) {
                    throw new Exception(ex.Message + " - ValorParte: linha " + (linha + 1));
                }

                if (workSheet.Cell(linha, coluna22).Value == null)
                    throw new Exception("ValorContraParte não informado: linha " + (linha + 1));
                try
                {                    
                    item.ValorContraParte = Convert.ToDecimal(workSheet.Cell(linha, coluna22).Value);
                }
                catch (Exception ex) {
                    throw new Exception(ex.Message + " - ValorContraParte: linha " + (linha + 1));
                }

                if (workSheet.Cell(linha, coluna23).Value == null)
                    throw new Exception("Saldo não informado: linha " + (linha + 1));
                try
                {                    
                    item.Saldo = Convert.ToDecimal(workSheet.Cell(linha, coluna23).Value);
                }
                catch (Exception ex) {
                    throw new Exception(ex.Message + " - Saldo: linha " + (linha + 1));
                }
                                
                this.valoresExcelPosicaoSwap.Add(item);
                //
                index++;
                linha = 1 + index;
                //                
                Console.WriteLine("{0},{1},{2},{3},{4},{5},{6},{7},{8},{9},{10},{11},{12},{13},{14},{15},{16},{17},{18},{19},{20},{21},{22}",
                    item.IdCliente, item.TipoRegistro, item.NumeroContrato, item.DataEmissao.ToString("dd/MM/yyyy"),
                    item.DataVencimento.ToString("dd/MM/yyyy"), item.ValorBase, item.TipoPonta, item.IdIndice,
                    item.TaxaJuros, item.Percentual, item.TipoApropriacao, item.ContagemDias,
                    item.BaseAno, item.PontaContraParte, item.IdIndiceContraParte, item.TaxaJurosContraParte,
                    item.PercentualContraParte, item.TipoApropriacaoContraParte, item.ContagemDiasContraParte, item.BaseAnoContraParte,
                    item.ValorParte, item.ValorContraParte, item.Saldo);
            }
        }
        catch (Exception ex) {
            document.Close();
            document.Dispose();

            throw new Exception(ex.Message);
        }
        #endregion

        document.Close();
        document.Dispose();
    }

    /// <summary>
    /// Gera e Carrega as posições EmprestimoSwap e as Posições EmprestimoSwapAbertura 
    /// de acordo com o objeto List<ValoresExcel>
    /// </summary>
    /// <exception cref="Exception">Se ocorreu problemas</exception>
    private void CarregaPosicaoSwap() {
        PosicaoSwapCollection posicaoSwapCollection = new PosicaoSwapCollection();
        PosicaoSwapCollection posicaoSwapDeletarCollection = new PosicaoSwapCollection();
        //
        PosicaoSwapAberturaCollection posicaoSwapAberturaCollection = new PosicaoSwapAberturaCollection();
        PosicaoSwapAberturaCollection posicaoSwapAberturaDeletarCollection = new PosicaoSwapAberturaCollection();
        //    
        for (int i = 0; i < this.valoresExcelPosicaoSwap.Count; i++) {
            ValoresExcelPosicaoSwap valoresExcel = this.valoresExcelPosicaoSwap[i];
            this.idClienteDeletarPosicaoSwap.Add(valoresExcel.IdCliente);

            #region Verifica Valores Incosistentes

            #region Verifica se Existe Cliente
            Cliente cliente = new Cliente();
            List<esQueryItem> campos = new List<esQueryItem>();
            campos.Add(cliente.Query.IdCliente);
            campos.Add(cliente.Query.DataDia);
            if (!cliente.LoadByPrimaryKey(campos, valoresExcel.IdCliente)) {
                throw new Exception("Cliente Não existente : " + valoresExcel.IdCliente);
            }
            // Salva a Data Historico para poder Deletar
            this.dataHistoricoDeletarPosicaoSwap.Add(cliente.DataDia.Value);
            #endregion

            #region TipoRegistro Inconsistente
            if ((int)valoresExcel.TipoRegistro != 1 && (int)valoresExcel.TipoRegistro != 2) {
                throw new Exception("TipoRegistro Inconsistente. Valores Possíveis: 1, 2");
            }
            #endregion

            #region TipoPonta Inconsistente
            if ((int)valoresExcel.TipoPonta != 1 && (int)valoresExcel.TipoPonta != 2 && (int)valoresExcel.TipoPonta != 3) {
                throw new Exception("TipoPonta Inconsistente. Valores Possíveis: 1, 2, 3");
            }
            #endregion

            #region TipoApropriacao/TipoApropriacaoContraParte Inconsistente
            // Se for = 0, entao valor veio nulo
            if ((int)valoresExcel.TipoApropriacao != 1 && (int)valoresExcel.TipoApropriacao != 2 &&
                (int)valoresExcel.TipoApropriacao != 0) {
                throw new Exception("TipoApropriacao Inconsistente. Valores Possíveis: 1, 2");
            }

            // Se for = 0, entao valor veio nulo
            if ((int)valoresExcel.TipoApropriacaoContraParte != 1 && (int)valoresExcel.TipoApropriacaoContraParte != 2 &&
                (int)valoresExcel.TipoApropriacaoContraParte != 0) {
                throw new Exception("TipoApropriacaoContraParte Inconsistente. Valores Possíveis: 1, 2");
            }
            #endregion

            #region ContagemDias/ContagemDiasContraParte Inconsistente
            // Se for = 0, entao valor veio nulo
            if ((int)valoresExcel.ContagemDias != 1 && (int)valoresExcel.ContagemDias != 2 &&
                (int)valoresExcel.ContagemDias != 0) {
                throw new Exception("ContagemDias Inconsistente. Valores Possíveis: 1, 2");
            }

            // Se for = 0, entao valor veio nulo
            if ((int)valoresExcel.ContagemDiasContraParte != 1 && (int)valoresExcel.ContagemDiasContraParte != 2 &&
                (int)valoresExcel.ContagemDiasContraParte != 0) {
                throw new Exception("ContagemDiasContraParte Inconsistente. Valores Possíveis: 1, 2");
            }
            #endregion

            #region BaseAno/BaseAnoContraParte Inconsistente
            // Se for = 0, entao valor veio nulo
            if ((int)valoresExcel.BaseAno != 252 && (int)valoresExcel.BaseAno != 360 && (int)valoresExcel.BaseAno != 365 &&
                (int)valoresExcel.BaseAno != 0) {
                throw new Exception("BaseAno Inconsistente. Valores Possíveis: 252, 360, 365");
            }

            // Se for = 0, entao valor veio nulo
            if ((int)valoresExcel.BaseAnoContraParte != 252 && (int)valoresExcel.BaseAnoContraParte != 360 && (int)valoresExcel.BaseAnoContraParte != 365 &&
                (int)valoresExcel.BaseAnoContraParte != 0) {
                throw new Exception("BaseAnoContraParte Inconsistente. Valores Possíveis: 252, 360, 365");
            }
            #endregion

            #region PontaContraParte Inconsistente
            if ((int)valoresExcel.PontaContraParte != 1 && (int)valoresExcel.PontaContraParte != 2 && (int)valoresExcel.PontaContraParte != 3)
            {
                throw new Exception("PontaContraParte Inconsistente. Valores Possíveis: 1, 2, 3");
            }
            #endregion

            #endregion

            #region PosicaoEmprestimo
            PosicaoSwap posicao = new PosicaoSwap();
            // Dados do Arquivo
            posicao.IdCliente = valoresExcel.IdCliente;
            posicao.TipoRegistro = (byte)valoresExcel.TipoRegistro;
            posicao.NumeroContrato = valoresExcel.NumeroContrato;
            posicao.DataEmissao = valoresExcel.DataEmissao;
            posicao.DataVencimento = valoresExcel.DataVencimento;
            posicao.ValorBase = valoresExcel.ValorBase;
            posicao.TipoPonta = (byte)valoresExcel.TipoPonta;
            posicao.IdIndice = (short?)valoresExcel.IdIndice;
            posicao.TaxaJuros = valoresExcel.TaxaJuros;
            posicao.Percentual = valoresExcel.Percentual;
            //
            if (valoresExcel.TipoApropriacao != 0) {
                posicao.TipoApropriacao = (byte)valoresExcel.TipoApropriacao;
            }
            if (valoresExcel.ContagemDias != 0) {
                posicao.ContagemDias = (byte)valoresExcel.ContagemDias;
            }
            if (valoresExcel.BaseAno != 0) {
                posicao.BaseAno = (byte)valoresExcel.BaseAno;
            }
            posicao.TipoPontaContraParte = (byte)valoresExcel.PontaContraParte;
            posicao.IdIndiceContraParte = (short?)valoresExcel.IdIndiceContraParte;
            posicao.TaxaJurosContraParte = valoresExcel.TaxaJurosContraParte;
            posicao.PercentualContraParte = valoresExcel.PercentualContraParte;
            //
            if (valoresExcel.TipoApropriacaoContraParte != 0) {
                posicao.TipoApropriacaoContraParte = (byte)valoresExcel.TipoApropriacaoContraParte;
            }
            if (valoresExcel.ContagemDiasContraParte != 0) {
                posicao.ContagemDiasContraParte = (byte)valoresExcel.ContagemDiasContraParte;
            }
            if (valoresExcel.BaseAnoContraParte != 0) {
                posicao.BaseAnoContraParte = (byte)valoresExcel.BaseAnoContraParte;
            }
            posicao.ValorParte = valoresExcel.ValorParte;
            posicao.ValorContraParte = valoresExcel.ValorContraParte;
            posicao.Saldo = valoresExcel.Saldo;
            //                               
            posicao.IdAgente = null;
            posicao.IdOperacao = null;
            posicao.IdAssessor = null;
            //
            posicao.ComGarantia = "S";
            posicao.DiasUteis = 0;
            posicao.DiasCorridos = 0;

            // Attach the object
            posicaoSwapCollection.AttachEntity(posicao);
            #endregion

            #region PosicaoEmprestimoAbertura
            PosicaoSwapAbertura posicaoAbertura = new PosicaoSwapAbertura();
            posicaoAbertura.IdCliente = posicao.IdCliente;
            posicaoAbertura.DataHistorico = cliente.DataDia.Value;
            posicaoAbertura.TipoRegistro = posicao.TipoRegistro;
            posicaoAbertura.NumeroContrato = posicao.NumeroContrato;
            posicaoAbertura.DataEmissao = posicao.DataEmissao;
            posicaoAbertura.DataVencimento = posicao.DataVencimento;
            posicaoAbertura.ValorBase = posicao.ValorBase;
            posicaoAbertura.TipoPonta = posicao.TipoPonta;
            posicaoAbertura.IdIndice = posicao.IdIndice;
            posicaoAbertura.TaxaJuros = posicao.TaxaJuros;
            posicaoAbertura.Percentual = posicao.Percentual;
            posicaoAbertura.TipoApropriacao = posicao.TipoApropriacao;
            posicaoAbertura.ContagemDias = posicao.ContagemDias;
            posicaoAbertura.BaseAno = posicao.BaseAno;
            posicaoAbertura.TipoPontaContraParte = posicao.TipoPontaContraParte;
            posicaoAbertura.IdIndiceContraParte = posicao.IdIndiceContraParte;
            posicaoAbertura.TaxaJurosContraParte = posicao.TaxaJurosContraParte;
            posicaoAbertura.PercentualContraParte = posicao.PercentualContraParte;
            posicaoAbertura.TipoApropriacaoContraParte = posicao.TipoApropriacaoContraParte;
            posicaoAbertura.ContagemDiasContraParte = posicao.ContagemDiasContraParte;
            posicaoAbertura.BaseAnoContraParte = posicao.BaseAnoContraParte;
            posicaoAbertura.ValorParte = posicao.ValorParte;
            posicaoAbertura.ValorContraParte = posicao.ValorContraParte;
            posicaoAbertura.Saldo = posicao.Saldo;
            posicaoAbertura.IdAgente = posicao.IdAgente;
            posicaoAbertura.IdOperacao = posicao.IdOperacao;
            posicaoAbertura.IdAssessor = posicao.IdAssessor;
            posicaoAbertura.ComGarantia = posicao.ComGarantia;
            posicaoAbertura.DiasUteis = posicao.DiasUteis;
            posicaoAbertura.DiasCorridos = posicao.DiasCorridos;

            // Attach the object
            posicaoSwapAberturaCollection.AttachEntity(posicaoAbertura);
            #endregion
        }

        using (esTransactionScope scope = new esTransactionScope()) {
            #region Deleta PosicaoSwap por IdCliente
            OperacaoSwapCollection operacaoSwapDeletarCollection = new OperacaoSwapCollection();
            List<int?> idPosicaoSwap = new List<int?>();

            posicaoSwapDeletarCollection.Query.Where(posicaoSwapDeletarCollection.Query.IdCliente.In(this.idClienteDeletarPosicaoSwap));
            posicaoSwapDeletarCollection.Query.Load();

            //Operações Swap
            foreach (PosicaoSwap item in posicaoSwapDeletarCollection)
            {
                if (item.IdOperacao != null)
                    idPosicaoSwap.Add(item.IdOperacao);
            }
            if (idPosicaoSwap.Count > 0)
            {
                operacaoSwapDeletarCollection.Query.Where(operacaoSwapDeletarCollection.Query.IdOperacao.In(idPosicaoSwap));
                operacaoSwapDeletarCollection.Query.Load();
            }
            //

            operacaoSwapDeletarCollection.MarkAllAsDeleted();
            posicaoSwapDeletarCollection.MarkAllAsDeleted();
            operacaoSwapDeletarCollection.Save();
            posicaoSwapDeletarCollection.Save();
            #endregion

            #region Deleta PosicaoSwapAbertura por IdCliente/DataHistorico
            for (int i = 0; i < this.idClienteDeletarPosicaoSwap.Count; i++) {
                posicaoSwapAberturaDeletarCollection = new PosicaoSwapAberturaCollection();
                posicaoSwapAberturaDeletarCollection.Query
                .Where(posicaoSwapAberturaDeletarCollection.Query.IdCliente == this.idClienteDeletarPosicaoSwap[i],
                       posicaoSwapAberturaDeletarCollection.Query.DataHistorico == this.dataHistoricoDeletarPosicaoSwap[i]);
                posicaoSwapAberturaDeletarCollection.Query.Load();
                posicaoSwapAberturaDeletarCollection.MarkAllAsDeleted();
                posicaoSwapAberturaDeletarCollection.Save();
            }
            #endregion

            // Salva Posicões presentes no Excel                                                
            posicaoSwapCollection.Save();

            // Seta os novos idPosicao na PosicaoAbertura
            for (int i = 0; i < posicaoSwapCollection.Count; i++) {
                posicaoSwapAberturaCollection[i].IdPosicao = posicaoSwapCollection[i].IdPosicao;
            }
            posicaoSwapAberturaCollection.Save();
            scope.Complete();
        }
    }
    
    /// <summary>
    /// Processa a Planilha de PosicaoSwap após ter acabado o Upload
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void uplPosicaoSwap_FileUploadComplete(object sender, FileUploadCompleteEventArgs e) {
        e.CallbackData = "";

        #region Trata Extensão Válida
        if (!ValoresExcelUtil.isExtensaoExcel(e.UploadedFile.FileName.Trim())) {
            e.CallbackData = "Importação Posição Swap - Formato de Arquivo Inválido. Formato permitido: .xls ou .xlsx\n\n";
            return;
        }
        #endregion

        // Stream de bytes com o Conteudo do Arquivo Excel
        Stream sr = e.UploadedFile.FileContent;

        #region Processamento Arquivo Excel

        //Reseta Vetor de Dados
        this.valoresExcelPosicaoSwap = new List<ValoresExcelPosicaoSwap>();
        //
        this.idClienteDeletarPosicaoSwap = new List<int>();
        this.dataHistoricoDeletarPosicaoSwap = new List<DateTime>();

        try {
            // Ler Arquivo
            this.LerArquivoPosicaoSwap(sr);
            // Carrega Arquivo
            this.CarregaPosicaoSwap();
        }
        catch (Exception e2) {
            e.CallbackData = "Importação Posição Swap - " + e2.Message;
            return;
        }

        new HistoricoLog().InsereHistoricoLog(DateTime.Now, DateTime.Now,
            "Importação Planilha Posição Swap",
            HttpContext.Current.User.Identity.Name, UtilitarioWeb.GetIP(Request), "", HistoricoLogOrigem.Outros);

        #endregion
    }
}