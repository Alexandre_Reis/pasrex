﻿using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Collections.Generic;

using System.IO;
using Financial.Integracao.Excel.Util;
using Financial.Integracao.Excel;
using Financial.Web.Common;

using EntitySpaces.Interfaces;

using DevExpress.Web;
using Financial.RendaFixa;
using Financial.RendaFixa.Enums;
using Financial.Investidor;
using Financial.Util;
using Financial.Web.Util;
using Financial.Security.Enums;
using Financial.Security;
using Financial.Common.Enums;
using Financial.Common;

public partial class ImportacaoBasePage : BasePage
{

    /* Estrutura do Excel */
    private List<ValoresExcelPosicaoRendaFixa> valoresExcelPosicaoRendaFixa = new List<ValoresExcelPosicaoRendaFixa>();
    //
    List<int> idClienteDeletarPosicaoRendaFixa = new List<int>();
    List<DateTime> dataHistoricoDeletarPosicaoRendaFixa = new List<DateTime>();

    /// <summary>
    /// Leitura de um arquivo Excel
    /// Saida: List<ValoresExcel> com os valores armazenados no arquivo
    /// </summary>
    /// <param name="streamExcel">Stream de bytes de um arquivo Excel</param>
    /// <exception cref="Exception">Se ocorreu problemas</exception>   
    private void LerArquivoPosicaoRendaFixa(Stream streamExcel)
    {

        #region Leitura do Arquivo Excel

        // Open Spreadsheet
        Bytescout.Spreadsheet.Spreadsheet document = new Bytescout.Spreadsheet.Spreadsheet();
        document.LoadFromStream(streamExcel);

        Bytescout.Spreadsheet.Worksheet workSheet = document.Workbook.Worksheets[0];

        //
        #region Confere Formato Arquivo
        string[] colunasConferencias = new string[] 
                            {"IdCliente","IdTitulo","TipoOperacao",
					         "Quantidade","QuantidadeBloqueada","DataOperacao", "PUOperacao",
                             "TaxaOperacao", "PUMercado","DataVolta", "TaxaVolta",
                             "PUVolta", "Custodia"};

        bool formato = ValoresExcelUtil.isExcelFormatoValido(colunasConferencias, workSheet);

        if (!formato)
        {
            string mensagem = "Formato Interno do Arquivo Inválido.\n";
            mensagem += "Linha 1 do Arquivo deve ser: \n";
            for (int i = 0; i < colunasConferencias.Length; i++)
            {
                int coluna = i + 1;
                mensagem += "\tColuna " + coluna + ": " + colunasConferencias[i] + "\n";
            }

            document.Close();
            document.Dispose();

            throw new Exception(mensagem);
        }
        #endregion

        //
        int index = 0;
        // This row,column index should be changed as per your need.
        // i.e. which cell in the excel you are interesting to read.
        //
        /* Formato: 1)IdCliente  - 2)IdTitulo  - 3)TipoOperacao - 4)Quantidade - 5) QuantidadeBloqueada
         *          6)DataOperacao - 7)PuOperacao - 8)TaxaOperacao - 9)PuMercado - 10)DataVolta  - 11)TaxaVolta
         *          12)PuVolta - 13)Custodia - 14)AgenteCustodia - 15)AgenteCorretora - 16)LocalNegociacao
         *          17)Categoria
         */
        int linha = 1;
        int coluna1 = 0, coluna2 = 1, coluna3 = 2,
            coluna4 = 3, coluna5 = 4, coluna6 = 5,
            coluna7 = 6, coluna8 = 7, coluna9 = 8,
            coluna10 = 9, coluna11 = 10, coluna12 = 11, coluna13 = 12,
            coluna14 = 13, coluna15 = 14, coluna16 = 15, coluna17 = 16;            
        //            
        try
        {
            // Enquanto idCliente tiver valor
            while (workSheet.Cell(linha, coluna1).Value != null)
            {
                ValoresExcelPosicaoRendaFixa item = new ValoresExcelPosicaoRendaFixa();
                
                try {
                    item.IdCliente = workSheet.Cell(linha, coluna1).ValueAsInteger;
                }
                catch (Exception ex) {
                    throw new Exception(ex.Message + " - IdCliente: linha " + (linha + 1));
                }

                if (workSheet.Cell(linha, coluna2).Value == null)
                    throw new Exception("IdTitulo não informado: linha " + (linha + 1));
                try
                {
                    item.IdTitulo = workSheet.Cell(linha, coluna2).ValueAsInteger;
                }
                catch (Exception ex) {
                    throw new Exception(ex.Message + " - IdTitulo: linha " + (linha + 1));
                }

                if (workSheet.Cell(linha, coluna3).Value != null)
                {
                    try {
                        item.TipoOperacao = workSheet.Cell(linha, coluna3).Value.ToString().Trim();
                    }
                    catch (Exception ex) {
                        throw new Exception(ex.Message + " - TipoOperacao: linha " + (linha + 1));
                    }
                }

                if (workSheet.Cell(linha, coluna4).Value == null)
                    throw new Exception("Quantidade não informada: linha " + (linha + 1));
                try
                {
                    item.Quantidade = Convert.ToDecimal(workSheet.Cell(linha, coluna4).Value);
                }
                catch (Exception ex) {
                    throw new Exception(ex.Message + " - Quantidade: linha " + (linha + 1));
                }

                if (workSheet.Cell(linha, coluna5).Value == null)
                    throw new Exception("QuantidadeBloqueada não informada: linha " + (linha + 1));
                try
                {
                    item.QuantidadeBloqueada = Convert.ToDecimal(workSheet.Cell(linha, coluna5).Value);
                }
                catch (Exception ex) {
                    throw new Exception(ex.Message + " - QuantidadeBloqueada: linha " + (linha + 1));
                }

                if (workSheet.Cell(linha, coluna6).Value == null)
                    throw new Exception("DataOperacao não informada: linha " + (linha + 1));
                try
                {
                    item.DataOperacao = workSheet.Cell(linha, coluna6).ValueAsDateTime;
                }
                catch (Exception ex) {
                    throw new Exception(ex.Message + " - DataOperacao: linha " + (linha + 1));
                }

                if (workSheet.Cell(linha, coluna7).Value == null)
                    throw new Exception("PuOperacao não informado: linha " + (linha + 1));
                try
                {
                    item.PuOperacao = Convert.ToDecimal(workSheet.Cell(linha, coluna7).Value);
                }
                catch (Exception ex) {
                    throw new Exception(ex.Message + " - PuOperacao: linha " + (linha + 1));
                }

                if (workSheet.Cell(linha, coluna8).Value == null)
                    throw new Exception("TaxaOperacao não informada: linha " + (linha + 1));
                try
                {
                    item.TaxaOperacao = Convert.ToDecimal(workSheet.Cell(linha, coluna8).Value);
                }
                catch (Exception ex) {
                    throw new Exception(ex.Message + " - TaxaOperacao: linha " + (linha + 1));
                }

                if (workSheet.Cell(linha, coluna9).Value == null)
                    throw new Exception("PuMercado não informado: linha " + (linha + 1));
                try
                {
                    item.PuMercado = Convert.ToDecimal(workSheet.Cell(linha, coluna9).Value);
                }
                catch (Exception ex) {
                    throw new Exception(ex.Message + " - PuMercado: linha " + (linha + 1));
                }
                                
                if (workSheet.Cell(linha, coluna10).Value != null)
                {
                    try {
                        item.DataVolta = workSheet.Cell(linha, coluna10).ValueAsDateTime;
                    }
                    catch (Exception ex) {
                        throw new Exception(ex.Message + " - DataVolta: linha " + (linha + 1));
                    }
                }
                
                if (workSheet.Cell(linha, coluna11).Value != null)
                {
                    try {
                        item.TaxaVolta = Convert.ToDecimal(workSheet.Cell(linha, coluna11).Value);
                    }
                    catch (Exception ex) {
                        throw new Exception(ex.Message + " - TaxaVolta: linha " + (linha + 1));
                    }
                }

                if (workSheet.Cell(linha, coluna12).Value != null)
                {
                    try {
                        item.PuVolta = Convert.ToDecimal(workSheet.Cell(linha, coluna12).Value);
                    }
                    catch (Exception ex) {
                        throw new Exception(ex.Message + " - PuVolta: linha " + (linha + 1));
                    }
                }

                #region Custodia
                if (workSheet.Cell(linha, coluna13).Value != null)
                {
                    try {
                        item.IdCustodia = workSheet.Cell(linha, coluna13).ValueAsInteger;
                    }
                    catch (Exception ex) {
                        throw new Exception(ex.Message + " - Custodia: linha " + (linha + 1));
                    }
                }
                #endregion

                #region AgenteCustodia
                if (workSheet.Cell(linha, coluna14).Value != null)
                {
                    try {
                        item.IdAgenteCustodia = workSheet.Cell(linha, coluna14).ValueAsInteger;
                    }
                    catch (Exception ex) {
                        throw new Exception(ex.Message + " - AgenteCustodia: linha " + (linha + 1));
                    }
                }
                #endregion


                #region AgenteCorretora
                if (workSheet.Cell(linha, coluna15).Value != null)
                {
                    try {
                        item.IdCorretora = workSheet.Cell(linha, coluna15).ValueAsInteger;
                    }
                    catch (Exception ex) {
                        throw new Exception(ex.Message + " - AgenteCorretora: linha " + (linha + 1));
                    }
                }
                #endregion


                #region LocalNegociacao
                if (workSheet.Cell(linha, coluna16).Value != null)
                {
                    try {
                        item.IdLocalNegociacao = workSheet.Cell(linha, coluna16).ValueAsInteger;
                    }
                    catch (Exception ex) {
                        throw new Exception(ex.Message + " - LocalNegociacao: linha " + (linha + 1));
                    }
                }
                #endregion


                #region Categoria
                if (workSheet.Cell(linha, coluna17).Value != null)
                {
                    try {
                        item.TipoNegociacao = workSheet.Cell(linha, coluna17).ValueAsString;
                    }
                    catch (Exception ex) {
                        throw new Exception(ex.Message + " - TipoNegociacao: linha " + (linha + 1));
                    }
                }
                #endregion

                this.valoresExcelPosicaoRendaFixa.Add(item);
                //
                index++;
                linha = 1 + index;
                //                
                Console.WriteLine("{0},{1},{2},{3},{4},{5},{6},{7},{8},{9},{10},{11}",
                    item.IdCliente, item.IdTitulo, item.TipoOperacao, item.Quantidade, item.QuantidadeBloqueada,
                    item.DataOperacao, item.PuOperacao, item.TaxaOperacao, item.PuMercado, item.DataVolta,
                    item.TaxaVolta, item.PuVolta);
            }
        }
        catch (Exception ex)
        {
            document.Close();
            document.Dispose();

            throw new Exception(ex.Message);
        }
        #endregion

        document.Close();
        document.Dispose();
    }

    /// <summary>
    /// Gera e carrega as posições RendaFixa e PosicaoRendaFixaAbertura de acordo com o objeto List<ValoresExcel>
    /// </summary>
    /// <exception cref="Exception">Se ocorreu problemas</exception>/// 
    private void CarregaPosicaoRendaFixa()
    {
        OperacaoRendaFixaCollection operacaoRendaFixaCollection = new OperacaoRendaFixaCollection();
        OperacaoRendaFixaCollection operacaoRendaFixaDeletarCollection = new OperacaoRendaFixaCollection();

        PosicaoRendaFixaCollection posicaoRendaFixaCollection = new PosicaoRendaFixaCollection();
        PosicaoRendaFixaCollection posicaoRendaFixaDeletarCollection = new PosicaoRendaFixaCollection();
        //
        PosicaoRendaFixaAberturaCollection posicaoRendaFixaAberturaCollection = new PosicaoRendaFixaAberturaCollection();
        PosicaoRendaFixaAberturaCollection posicaoRendaFixaAberturaDeletarCollection = new PosicaoRendaFixaAberturaCollection();
        //    
        for (int i = 0; i < this.valoresExcelPosicaoRendaFixa.Count; i++)
        {
            ValoresExcelPosicaoRendaFixa valoresExcel = this.valoresExcelPosicaoRendaFixa[i];
            //
            this.idClienteDeletarPosicaoRendaFixa.Add(valoresExcel.IdCliente);

            #region Testa TipoOperacao Nulo
            if (String.IsNullOrEmpty(valoresExcel.TipoOperacao))
            {
                throw new Exception("Tipo Operação não pode ser nulo: ");
            }
            #endregion

            #region Testa TipoOperacaoTitulo
            int? tipo = null;
            try
            {
                tipo = (int)Enum.Parse(typeof(TipoOperacaoTitulo), valoresExcel.TipoOperacao);
            }
            catch (Exception e)
            {
                string tipos = "";
                for (int j = 0; j < Enum.GetNames(typeof(TipoOperacaoTitulo)).Length; j++)
                {
                    tipos = tipos == ""
                        ? tipos + Enum.GetNames(typeof(TipoOperacaoTitulo))[j]
                        : tipos + ", " + Enum.GetNames(typeof(TipoOperacaoTitulo))[j];
                }
                throw new Exception(e.Message + "TipoOperacao deve ser: " + tipos);
            }
            #endregion

            #region Testa Cliente existente
            Cliente cliente = new Cliente();
            List<esQueryItem> campos = new List<esQueryItem>();
            campos.Add(cliente.Query.IdCliente);
            campos.Add(cliente.Query.DataDia);
            if (!cliente.LoadByPrimaryKey(campos, valoresExcel.IdCliente))
            {
                throw new Exception("Cliente Não existente : " + valoresExcel.IdCliente);
            }
            // Salva a Data Historico para poder Deletar
            this.dataHistoricoDeletarPosicaoRendaFixa.Add(cliente.DataDia.Value);
            #endregion

            #region Testa Cliente existente
            TituloRendaFixa titulo = new TituloRendaFixa();
            if (!titulo.LoadByPrimaryKey(valoresExcel.IdTitulo))
            {
                throw new Exception("Titulo Não existente : " + valoresExcel.IdTitulo);
            }
            #endregion

            #region PosicaoRendaFixa
            PosicaoRendaFixa posicao = new PosicaoRendaFixa();
            // valores vindos do arquivo
            posicao.IdCliente = valoresExcel.IdCliente;
            posicao.IdTitulo = valoresExcel.IdTitulo;
            posicao.TipoOperacao = (byte)tipo;
            posicao.Quantidade = valoresExcel.Quantidade;
            posicao.QuantidadeBloqueada = valoresExcel.QuantidadeBloqueada;

            if (valoresExcel.DataOperacao > cliente.DataDia.Value)
            {
                throw new Exception("Data de operação " + valoresExcel.DataOperacao.ToShortDateString() + " posterior à data atual (" +
                                            cliente.DataDia.Value.ToShortDateString() + ") do cliente " + valoresExcel.IdCliente.ToString());
            }

            posicao.DataOperacao = valoresExcel.DataOperacao;

            posicao.PUOperacao = valoresExcel.PuOperacao;
            posicao.TaxaOperacao = valoresExcel.TaxaOperacao;
            posicao.PUMercado = valoresExcel.PuMercado;
            posicao.PUCurva = valoresExcel.PuOperacao;

            //
            if (tipo == (int)TipoOperacaoTitulo.CompraRevenda || tipo == (int)TipoOperacaoTitulo.VendaRecompra)
            {
                posicao.DataVolta = valoresExcel.DataVolta;
                posicao.TaxaVolta = valoresExcel.TaxaVolta;
                posicao.PUVolta = valoresExcel.PuVolta;

                #region Confere PuVolta - Não pode ser nulo para TipoOperação = CompraRevenda ou TipoOperação = VendaRecompra
                string tipoOperacaoString = StringEnum.GetStringValue((TipoOperacaoTitulo)tipo);

                if (!posicao.PUVolta.HasValue)
                {
                    throw new Exception("PUVolta não pode ser nulo para TipoOperação: " + tipoOperacaoString);
                }
                #endregion

                posicao.ValorVolta = Utilitario.Truncate((posicao.Quantidade.Value * posicao.PUVolta.Value), 2);
            }
            //
            posicao.DataVencimento = posicao.UpToTituloRendaFixaByIdTitulo.DataVencimento;
            posicao.QuantidadeInicial = valoresExcel.Quantidade;
            //
            posicao.QuantidadeBloqueada = valoresExcel.QuantidadeBloqueada;
            posicao.PUJuros = valoresExcel.PuOperacao;
            posicao.ValorJuros = 0;
            posicao.ValorIR = 0;
            posicao.ValorIOF = 0;
            //

            if (!string.IsNullOrEmpty(valoresExcel.TipoNegociacao))
            {
                posicao.TipoNegociacao = Convert.ToByte((int)Enum.Parse(typeof(TipoNegociacaoTitulo), valoresExcel.TipoNegociacao));
            }
            else
            {
                posicao.TipoNegociacao = (byte)TipoNegociacaoTitulo.Negociacao;
            }

            if(valoresExcel.IdAgenteCustodia.HasValue)
            {
                AgenteMercadoCollection coll = new AgenteMercadoCollection();
                coll.Query.Where(coll.Query.IdAgente.Equal(valoresExcel.IdAgenteCustodia.Value));
                
                if(coll.Query.Load())
                    posicao.IdAgente = coll[0].IdAgente.Value;
                else
                    throw new Exception("Agente de Mercado Não existente : " + valoresExcel.IdAgenteCustodia);

                posicao.IdAgente = valoresExcel.IdAgenteCustodia.Value;
            }

            if (valoresExcel.IdCorretora.HasValue)
            {
                AgenteMercadoCollection coll = new AgenteMercadoCollection();
                coll.Query.Where(coll.Query.IdAgente.Equal(valoresExcel.IdCorretora.Value));

                if (coll.Query.Load())
                    posicao.IdCorretora = coll[0].IdAgente.Value;
                else
                    throw new Exception("Corretora Não existente : " + valoresExcel.IdCorretora);

                posicao.IdCorretora = valoresExcel.IdCorretora.Value;
            }

            posicao.DataLiquidacao = valoresExcel.DataOperacao;
            //
            //Por ora curva = mercado
            posicao.ValorCurva = 0;
            //
            posicao.ValorMercado = Math.Round((posicao.Quantidade.Value * posicao.PUMercado.Value), 2);
            //
            posicao.PUCorrecao = valoresExcel.PuOperacao;
            posicao.ValorCorrecao = 0;
            posicao.TaxaOperacao = valoresExcel.TaxaOperacao;

            if (valoresExcel.IdCustodia.HasValue)
            {
                LocalCustodiaCollection localCustodiaColl = new LocalCustodiaCollection();
                localCustodiaColl.Query.Where(localCustodiaColl.Query.IdLocalCustodia.Equal(valoresExcel.IdCustodia.Value));

                if(!localCustodiaColl.Query.Load())
                    throw new Exception("Local de Custódia Não existente : " + valoresExcel.IdCustodia);

                posicao.IdCustodia = Convert.ToByte(valoresExcel.IdCustodia.Value);
            }
            else
            {
                int tipoPapel = titulo.UpToPapelRendaFixaByIdPapel.TipoPapel.Value;
                posicao.IdCustodia = (tipoPapel == (int)TipoPapelTitulo.Privado ? (byte)LocalCustodiaFixo.Cetip : (byte)LocalCustodiaFixo.Selic);
            }

            // Attach the object
            posicaoRendaFixaCollection.AttachEntity(posicao);
            #endregion

            #region OperacaoRendaFixa
            OperacaoRendaFixa operacaoRendaFixa = operacaoRendaFixaCollection.AddNew();
            operacaoRendaFixa.IdCliente = posicao.IdCliente;
            operacaoRendaFixa.IdTitulo = posicao.IdTitulo;
            operacaoRendaFixa.TipoOperacao = posicao.TipoOperacao;
            operacaoRendaFixa.Quantidade = posicao.Quantidade;
            operacaoRendaFixa.DataOperacao = posicao.DataOperacao;
            operacaoRendaFixa.PUOperacao = posicao.PUOperacao;
            operacaoRendaFixa.TaxaOperacao = posicao.TaxaOperacao;
            operacaoRendaFixa.DataVolta = posicao.DataVolta;
            operacaoRendaFixa.TaxaVolta = posicao.TaxaVolta;
            operacaoRendaFixa.PUVolta = posicao.PUVolta;
            operacaoRendaFixa.IdCustodia = posicao.IdCustodia;
            operacaoRendaFixa.TipoNegociacao = posicao.TipoNegociacao;
            operacaoRendaFixa.IdAgenteCorretora = posicao.IdCorretora;
            operacaoRendaFixa.IdAgenteCustodia = posicao.IdAgente;
            operacaoRendaFixa.DataRegistro = operacaoRendaFixa.DataOperacao;
            operacaoRendaFixa.DataLiquidacao = posicao.DataLiquidacao;
            operacaoRendaFixa.Valor = posicao.ValorMercado;
            operacaoRendaFixa.Fonte = 0;
            operacaoRendaFixa.IdLiquidacao = 0;            

            if (valoresExcel.IdLocalNegociacao.HasValue)
            {
                LocalNegociacaoCollection localNegociacaoColl = new LocalNegociacaoCollection();
                localNegociacaoColl.Query.Where(localNegociacaoColl.Query.IdLocalNegociacao.Equal(valoresExcel.IdLocalNegociacao.Value));

                if (!localNegociacaoColl.Query.Load())
                    throw new Exception("Local de Negociação Não existente : " + valoresExcel.IdLocalNegociacao.HasValue);

                operacaoRendaFixa.IdLocalNegociacao = valoresExcel.IdLocalNegociacao.Value;
            }
            else
            {
                int tipoPapel = titulo.UpToPapelRendaFixaByIdPapel.TipoPapel.Value;
                operacaoRendaFixa.IdLocalNegociacao = (tipoPapel == (int)TipoPapelTitulo.Privado ? (byte)LocalNegociacaoFixo.CETIP : (byte)LocalNegociacaoFixo.SELIC);
            }
            #endregion

            #region PosicaoRendaFixaAbertura
            PosicaoRendaFixaAbertura posicaoAbertura = new PosicaoRendaFixaAbertura();
            //
            posicaoAbertura.IdCliente = posicao.IdCliente;
            posicaoAbertura.DataHistorico = cliente.DataDia.Value;
            posicaoAbertura.IdTitulo = posicao.IdTitulo;
            posicaoAbertura.TipoOperacao = posicao.TipoOperacao;
            posicaoAbertura.Quantidade = posicao.Quantidade;
            posicaoAbertura.QuantidadeBloqueada = posicao.QuantidadeBloqueada;
            posicaoAbertura.DataOperacao = posicao.DataOperacao;
            posicaoAbertura.PUOperacao = posicao.PUOperacao;
            posicaoAbertura.TaxaOperacao = posicao.TaxaOperacao;
            posicaoAbertura.PUMercado = posicao.PUMercado;
            posicaoAbertura.DataVolta = posicao.DataVolta;
            posicaoAbertura.TaxaVolta = posicao.TaxaVolta;
            posicaoAbertura.PUVolta = posicao.PUVolta;
            posicaoAbertura.ValorVolta = posicao.ValorVolta;
            posicaoAbertura.DataVencimento = posicao.DataVencimento;
            posicaoAbertura.QuantidadeInicial = posicao.QuantidadeInicial;
            posicaoAbertura.QuantidadeBloqueada = posicao.QuantidadeBloqueada;
            posicaoAbertura.PUCurva = posicao.PUCurva;
            posicaoAbertura.ValorCurva = posicao.ValorCurva;
            posicaoAbertura.PUJuros = posicao.PUJuros;
            posicaoAbertura.ValorJuros = posicao.ValorJuros;
            posicaoAbertura.ValorIR = posicao.ValorIR;
            posicaoAbertura.ValorIOF = posicao.ValorIOF;
            posicaoAbertura.TipoNegociacao = posicao.TipoNegociacao;
            posicaoAbertura.DataLiquidacao = posicao.DataLiquidacao;
            posicaoAbertura.ValorMercado = posicao.ValorMercado;
            posicaoAbertura.IdCustodia = posicao.IdCustodia;
            posicaoAbertura.IdCorretora = posicao.IdCorretora;
            posicaoAbertura.IdAgente = posicao.IdAgente;
                       
            //            
            posicaoAbertura.PUCorrecao = posicao.PUCorrecao;
            posicaoAbertura.ValorCorrecao = posicao.ValorCorrecao;
            //
            // Attach the object
            posicaoRendaFixaAberturaCollection.AttachEntity(posicaoAbertura);
            #endregion*/
        }

        using (esTransactionScope scope = new esTransactionScope())
        {
            operacaoRendaFixaCollection.Save();
            for (int i = 0; i < operacaoRendaFixaCollection.Count; i++)
            {
                OperacaoRendaFixa operacaoRendaFixa = operacaoRendaFixaCollection[i];
                PosicaoRendaFixa posicaoRendaFixa = posicaoRendaFixaCollection[i];
                PosicaoRendaFixaAbertura posicaoRendaFixaAbertura = posicaoRendaFixaAberturaCollection[i];

                posicaoRendaFixa.IdOperacao = operacaoRendaFixa.IdOperacao.Value;
                posicaoRendaFixaAbertura.IdOperacao = operacaoRendaFixa.IdOperacao.Value;
            }


            #region Deleta PosicaoRendaFixa por IdCliente
            operacaoRendaFixaDeletarCollection = new OperacaoRendaFixaCollection();
            List<int?> idOperacaoRendaFixa = new List<int?>();

            posicaoRendaFixaDeletarCollection.Query.Where(posicaoRendaFixaDeletarCollection.Query.IdCliente.In(this.idClienteDeletarPosicaoRendaFixa));
            posicaoRendaFixaDeletarCollection.Query.Load();

            foreach (PosicaoRendaFixa item in posicaoRendaFixaDeletarCollection)
            {
                if (item.IdOperacao != null)
                    idOperacaoRendaFixa.Add(item.IdOperacao);
            }
            if (idOperacaoRendaFixa.Count > 0)
            {
                operacaoRendaFixaDeletarCollection.Query.Where(operacaoRendaFixaDeletarCollection.Query.IdOperacao.In(idOperacaoRendaFixa));
                operacaoRendaFixaDeletarCollection.Query.Load();
            }
            //
            #endregion

            #region Deleta PosicaoRendaFixaAbertura por IdCliente/DataHistorico
            for (int i = 0; i < this.idClienteDeletarPosicaoRendaFixa.Count; i++)
            {
                posicaoRendaFixaAberturaDeletarCollection = new PosicaoRendaFixaAberturaCollection();
                posicaoRendaFixaAberturaDeletarCollection.Query
                .Where(posicaoRendaFixaAberturaDeletarCollection.Query.IdCliente == this.idClienteDeletarPosicaoRendaFixa[i],
                       posicaoRendaFixaAberturaDeletarCollection.Query.DataHistorico == this.dataHistoricoDeletarPosicaoRendaFixa[i]);
                posicaoRendaFixaAberturaDeletarCollection.Query.Load();
                posicaoRendaFixaAberturaDeletarCollection.MarkAllAsDeleted();
                posicaoRendaFixaAberturaDeletarCollection.Save();

                PosicaoRendaFixaHistoricoCollection posicaoRendaFixaHistoricoDeletarCollection = new PosicaoRendaFixaHistoricoCollection();
                posicaoRendaFixaHistoricoDeletarCollection.Query
                .Where(posicaoRendaFixaHistoricoDeletarCollection.Query.IdCliente == this.idClienteDeletarPosicaoRendaFixa[i],
                       posicaoRendaFixaHistoricoDeletarCollection.Query.DataHistorico == this.dataHistoricoDeletarPosicaoRendaFixa[i]);
                posicaoRendaFixaHistoricoDeletarCollection.Query.Load();
                posicaoRendaFixaHistoricoDeletarCollection.MarkAllAsDeleted();
                posicaoRendaFixaHistoricoDeletarCollection.Save();
            }

            operacaoRendaFixaDeletarCollection.MarkAllAsDeleted();
            posicaoRendaFixaDeletarCollection.MarkAllAsDeleted();            
            posicaoRendaFixaDeletarCollection.Save();
            operacaoRendaFixaDeletarCollection.Save();
            #endregion

            operacaoRendaFixaCollection.Save();
            posicaoRendaFixaCollection.Save();

            for (int i = 0; i < operacaoRendaFixaCollection.Count; i++)
            {
                PosicaoRendaFixa posicaoRendaFixa = posicaoRendaFixaCollection[i];
                PosicaoRendaFixaAbertura posicaoRendaFixaAbertura = posicaoRendaFixaAberturaCollection[i];

                posicaoRendaFixaAbertura.IdPosicao = posicaoRendaFixa.IdPosicao.Value;
            }

            posicaoRendaFixaAberturaCollection.Save();

            scope.Complete();
        }
    }

    /// <summary>
    /// Processa a Planilha de PosicaoRendaFixa após ter acabado o Upload
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void uplPosicaoRendaFixa_FileUploadComplete(object sender, FileUploadCompleteEventArgs e)
    {
        e.CallbackData = "";

        #region Trata Extensão Válida
        if (!ValoresExcelUtil.isExtensaoExcel(e.UploadedFile.FileName.Trim()))
        {
            e.CallbackData = "Importação Posição Renda Fixa - Formato de Arquivo Inválido. Formato permitido: .xls ou .xlsx\n\n";
            return;
        }
        #endregion

        // Stream de bytes com o Conteudo do Arquivo Excel
        Stream sr = e.UploadedFile.FileContent;

        #region Processamento Arquivo Excel

        //Reseta Vetor de Dados
        this.valoresExcelPosicaoRendaFixa = new List<ValoresExcelPosicaoRendaFixa>();
        //
        this.idClienteDeletarPosicaoRendaFixa = new List<int>();
        this.dataHistoricoDeletarPosicaoRendaFixa = new List<DateTime>();

        try
        {
            // Ler Arquivo
            this.LerArquivoPosicaoRendaFixa(sr);
            // Carrega Arquivo
            this.CarregaPosicaoRendaFixa();
        }
        catch (Exception e2)
        {
            e.CallbackData = "Importação Posição Renda Fixa - " + e2.Message;
            return;
        }

        new HistoricoLog().InsereHistoricoLog(DateTime.Now, DateTime.Now,
            "Importação Planilha Posição Renda Fixa",
            HttpContext.Current.User.Identity.Name, UtilitarioWeb.GetIP(Request), "", HistoricoLogOrigem.Outros);

        #endregion
    }
}