﻿using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Collections.Generic;

using System.IO;
using Financial.Integracao.Excel.Util;
using Financial.Integracao.Excel;
using Financial.Web.Common;

using EntitySpaces.Interfaces;

using DevExpress.Web;
using Financial.InvestidorCotista;
using Financial.Investidor;
using Financial.Fundo;
using Financial.Util;
using Financial.Web.Util;
using Financial.Security.Enums;
using Financial.Security;

public partial class ImportacaoBasePage : BasePage {

    /* Estrutura do Excel */
    private List<ValoresExcelPosicaoCotistaHistorico> valoresExcelPosicaoCotistaH = new List<ValoresExcelPosicaoCotistaHistorico>();
    //    
    /// <summary>
    /// Leitura de um arquivo Excel
    /// Saida: List<ValoresExcel> com os valores armazenados no arquivo
    /// </summary>
    /// <param name="streamExcel">Stream de bytes de um arquivo Excel</param>
    /// <exception cref="Exception">Se ocorreu problemas</exception>
    private void LerArquivoPosicaoCotistaH(Stream streamExcel) {
        #region Leitura do Arquivo Excel

        // Open Spreadsheet
        Bytescout.Spreadsheet.Spreadsheet document = new Bytescout.Spreadsheet.Spreadsheet();
        document.LoadFromStream(streamExcel);

        Bytescout.Spreadsheet.Worksheet workSheet = document.Workbook.Worksheets[0];
        //

        #region Confere Formato Arquivo
        string[] colunasConferencias = new string[] 
                                {"DataHistorico","IdCotista","IdCarteira","ValorAplicacao","DataAplicacao","DataConversao",
					             "Quantidade","CotaAplicacao","DataUltimaCobrancaIR"};

        bool formato = ValoresExcelUtil.isExcelFormatoValido(colunasConferencias, workSheet);

        if (!formato) {
            string mensagem = "Formato Interno do Arquivo Inválido.\n";
            mensagem += "Linha 1 do Arquivo deve ser: \n";
            for (int i = 0; i < colunasConferencias.Length; i++) {
                int coluna = i + 1;
                mensagem += "\tColuna " + coluna + ": " + colunasConferencias[i] + "\n";
            }

            document.Close();
            document.Dispose();

            throw new Exception(mensagem);
        }
        #endregion

        int index = 0;
        // This row,column index should be changed as per your need.
        // i.e. which cell in the excel you are interesting to read.
        //
        /* Formato: 1)IdCotista  - 2)IdCarteira    - 3)ValorAplicacao       - 4)DataAplicacao  - 5)DataConversao
         *          6)Quantidade - 7)CotaAplicacao - 8)DataUltimaCobrancaIR - 10)IdOperacao
         */

        int linha = 1;
        int coluna1 = 0, coluna2 = 1, coluna3 = 2,
            coluna4 = 3, coluna5 = 4, coluna6 = 5,
            coluna7 = 6, coluna8 = 7, coluna9 = 8,
            coluna10 = 9;

        try {
            // Enquanto idCotista tiver valor
            while (workSheet.Cell(linha, coluna1).Value != null) {
                ValoresExcelPosicaoCotistaHistorico item = new ValoresExcelPosicaoCotistaHistorico();
                
                try {                    
                    item.DataHistorico = workSheet.Cell(linha, coluna1).ValueAsDateTime;
                }
                catch (Exception ex) {
                    throw new Exception(ex.Message + " - DataHistorico: linha " + (linha + 1));
                }

                if (workSheet.Cell(linha, coluna2).Value == null)
                    throw new Exception("IdCotista não informado: linha " + (linha + 1));
                try
                {                    
                    item.IdCotista = workSheet.Cell(linha, coluna2).ValueAsInteger;
                }
                catch (Exception ex) {
                    throw new Exception(ex.Message + " - IdCotista: linha " + (linha + 1));
                }

                if (workSheet.Cell(linha, coluna3).Value == null)
                    throw new Exception("IdCarteira não informado: linha " + (linha + 1));
                try
                {                    
                    item.IdCarteira = workSheet.Cell(linha, coluna3).ValueAsInteger;
                }
                catch (Exception ex) {
                    throw new Exception(ex.Message + " - IdCarteira: linha " + (linha + 1));
                }

                if (workSheet.Cell(linha, coluna4).Value == null)
                    throw new Exception("ValorAplicacao não informado: linha " + (linha + 1));
                try
                {                    
                    item.ValorAplicacao = Convert.ToDecimal(workSheet.Cell(linha, coluna4).Value);
                }
                catch (Exception ex) {
                    throw new Exception(ex.Message + " - ValorAplicacao: linha " + (linha + 1));
                }

                if (workSheet.Cell(linha, coluna5).Value == null)
                    throw new Exception("DataAplicacao não informado: linha " + (linha + 1));
                try
                {                    
                    item.DataAplicacao = workSheet.Cell(linha, coluna5).ValueAsDateTime;
                }
                catch (Exception ex) {
                    throw new Exception(ex.Message + " - DataAplicacao: linha " + (linha + 1));
                }

                if (workSheet.Cell(linha, coluna6).Value == null)
                    throw new Exception("DataConversao não informada: linha " + (linha + 1));
                try
                {                    
                    item.DataConversao = workSheet.Cell(linha, coluna6).ValueAsDateTime;
                }
                catch (Exception ex) {
                    throw new Exception(ex.Message + " - DataConversao: linha " + (linha + 1));
                }

                if (workSheet.Cell(linha, coluna7).Value == null)
                    throw new Exception("Quantidade não informada: linha " + (linha + 1));
                try
                {                    
                    item.Quantidade = Convert.ToDecimal(workSheet.Cell(linha, coluna7).Value);
                }
                catch (Exception ex) {
                    throw new Exception(ex.Message + " - Quantidade: linha " + (linha + 1));
                }

                if (workSheet.Cell(linha, coluna8).Value == null)
                    throw new Exception("CotaAplicacao não informada: linha " + (linha + 1));
                try
                {                    
                    item.CotaAplicacao = Convert.ToDecimal(workSheet.Cell(linha, coluna8).Value);
                }
                catch (Exception ex) {
                    throw new Exception(ex.Message + " - CotaAplicacao: linha " + (linha + 1));
                }

                if (workSheet.Cell(linha, coluna9).Value != null) {
                    try {
                        item.DataUltimaCobrancaIR = workSheet.Cell(linha, coluna9).ValueAsDateTime;
                    }
                    catch (Exception ex) {
                        throw new Exception(ex.Message + " - DataUltimaCobrancaIR: linha " + (linha + 1));
                    }
                }

                if (workSheet.Cell(linha, coluna10).Value != null)
                {
                    try
                    {
                        item.IdOperacao = workSheet.Cell(linha, coluna10).ValueAsInteger;
                    }
                    catch (Exception ex)
                    {
                        throw new Exception(ex.Message + " - IdOperacao: linha " + (linha + 1));
                    }
                }
                else
                {
                    item.IdOperacao = null;
                }
                //

                this.valoresExcelPosicaoCotistaH.Add(item);
                //
                index++;
                linha = 1 + index;
                //                
                Console.WriteLine("{0},{1},{2},{3},{4},{5},{6},{7},{8},{9}",
                    item.DataHistorico, item.IdCotista, item.IdCarteira, item.ValorAplicacao, item.DataAplicacao, item.DataConversao,
                    item.Quantidade, item.CotaAplicacao, item.DataUltimaCobrancaIR, item.IdOperacao);
            }
        }
        catch (Exception ex) {
            document.Close();
            document.Dispose();

            throw new Exception(ex.Message);
        }
        #endregion

        document.Close();
        document.Dispose();
    }

    /// <summary>
    /// Gera e Carrega PosicaoCotistaHistorico de acordo com o objeto List<ValoresExcel>
    /// </summary>
    /// <exception cref="Exception">Se ocorreu problemas</exception>
    private void CarregaPosicaoCotistaH() {
        PosicaoCotistaHistoricoCollection posicaoCotistaHCollection = new PosicaoCotistaHistoricoCollection();        
        //

        #region Seleciona o Minimo Negativo
        int minimo = -2147483648;
        PosicaoCotistaHistorico p1 = new PosicaoCotistaHistorico();
        p1.Query.Select(p1.Query.IdPosicao.Max()).Where(p1.Query.IdPosicao < 0);
        if (p1.Query.Load()) {
            if (p1.IdPosicao.HasValue) {
                minimo = p1.IdPosicao.Value;
            }
        }
        #endregion

        for (int i = 0; i < this.valoresExcelPosicaoCotistaH.Count; i++) {
            minimo++;
            ValoresExcelPosicaoCotistaHistorico valoresExcel = this.valoresExcelPosicaoCotistaH[i];
            //
            #region Verifica se Existe Carteira
            Carteira carteira = new Carteira();
            if (!carteira.LoadByPrimaryKey(valoresExcel.IdCarteira)) {
                throw new Exception("Carteira Não existente : " + valoresExcel.IdCarteira);
            }
            #endregion

            #region Verifica se Existe Cotista
            Cotista cotista = new Cotista();
            if (!cotista.LoadByPrimaryKey(valoresExcel.IdCotista))
            {
                throw new Exception("Cotista Não existente : " + valoresExcel.IdCotista);
            }
            #endregion

            #region DataDia do Cliente
            Cliente cliente = new Cliente();
            List<esQueryItem> campos = new List<esQueryItem>();
            campos.Add(cliente.Query.IdCliente);
            campos.Add(cliente.Query.DataDia);
            campos.Add(cliente.Query.DataImplantacao);
            if (!cliente.LoadByPrimaryKey(campos, valoresExcel.IdCarteira)) {
                throw new Exception("Cliente Não existente : " + valoresExcel.IdCarteira);
            }
            #endregion

            if (valoresExcel.DataConversao >= cliente.DataImplantacao.Value)
                throw new Exception(" Data de Conversão deve ser menor que a data de Implantação do Cliente: " + valoresExcel.IdCarteira);

            if (valoresExcel.CotaAplicacao == 0) {
                HistoricoCota historicoCotaAplicacao = new HistoricoCota();
                if (historicoCotaAplicacao.BuscaValorCota(valoresExcel.IdCarteira, valoresExcel.DataConversao) && historicoCotaAplicacao.CotaFechamento.HasValue) {
                    valoresExcel.CotaAplicacao = historicoCotaAplicacao.CotaFechamento.Value;
                }
            }

            #region PosicaoCotistaHistorico
            PosicaoCotistaHistorico posicao = posicaoCotistaHCollection.AddNew();

            // Dados do Arquivo
            posicao.DataHistorico = valoresExcel.DataHistorico;
            posicao.IdPosicao = minimo;
            posicao.IdCotista = valoresExcel.IdCotista;
            posicao.IdCarteira = valoresExcel.IdCarteira;
            posicao.ValorAplicacao = valoresExcel.ValorAplicacao;
            posicao.DataAplicacao = valoresExcel.DataAplicacao;
            posicao.DataConversao = valoresExcel.DataConversao;
            posicao.Quantidade = valoresExcel.Quantidade;
            posicao.CotaAplicacao = valoresExcel.CotaAplicacao;
            posicao.DataUltimaCobrancaIR = valoresExcel.DataUltimaCobrancaIR != null ? valoresExcel.DataUltimaCobrancaIR : cliente.DataDia;
            //
            posicao.QuantidadeAntesCortes = valoresExcel.Quantidade;
            posicao.ValorIOFVirtual = 0;
            //
            posicao.QuantidadeBloqueada = 0;
            posicao.ValorIR = 0;
            posicao.ValorIOF = 0;
            posicao.ValorPerformance = 0;
            posicao.ValorRendimento = 0;
            posicao.PosicaoIncorporada = "N";
            //
            posicao.QuantidadeInicial = valoresExcel.Quantidade;
            posicao.IdOperacao = valoresExcel.IdOperacao;

            #region CotaDia
            HistoricoCota historicoCota = new HistoricoCota();
            List<esQueryItem> camposAux = new List<esQueryItem>();
            camposAux.Add(historicoCota.Query.CotaFechamento);
            //
            posicao.CotaDia = historicoCota.LoadByPrimaryKey(camposAux, cliente.DataDia.Value, valoresExcel.IdCarteira)
                              ? historicoCota.CotaFechamento.Value : 0;
            #endregion

            posicao.ValorBruto = Utilitario.Truncate((posicao.Quantidade.Value * posicao.CotaDia.Value), 2);
            posicao.ValorLiquido = posicao.ValorBruto.Value - posicao.ValorIR.Value - posicao.ValorIOF.Value;
            #endregion
        }

        // Salva Posicões presentes no Excel
        posicaoCotistaHCollection.Save();
    }
    
    /// <summary>
    /// Processa a Planilha de PosicaoCotistaHistórico após ter acabado o Upload
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void uplPosicaoCotistaHistorico_FileUploadComplete(object sender, FileUploadCompleteEventArgs e) {
        e.CallbackData = "";

        #region Trata Extensão Válida
        if (!ValoresExcelUtil.isExtensaoExcel(e.UploadedFile.FileName.Trim())) {
            e.CallbackData = "Importação Posição Cotista Histórico - Formato de Arquivo Inválido. Formato permitido: .xls ou .xlsx\n\n";
            return;
        }
        #endregion

        // Stream de bytes com o Conteudo do Arquivo Excel
        Stream sr = e.UploadedFile.FileContent;

        #region Processamento Arquivo Excel

        //Reseta Vetor de Dados
        this.valoresExcelPosicaoCotistaH = new List<ValoresExcelPosicaoCotistaHistorico>();
        //

        try {
            // Ler Arquivo
            this.LerArquivoPosicaoCotistaH(sr);
            // Carrega Arquivo
            this.CarregaPosicaoCotistaH();
        }
        catch (Exception e2) {
            e.CallbackData = "Importação Posição Cotista Histórico - " + e2.Message;
            return;
        }

        new HistoricoLog().InsereHistoricoLog(DateTime.Now, DateTime.Now,
                "Importação Planilha Posição Cotista Histórico",
                HttpContext.Current.User.Identity.Name, UtilitarioWeb.GetIP(Request), "", HistoricoLogOrigem.Outros);

        #endregion
    }
}