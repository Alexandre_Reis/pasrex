﻿using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Collections.Generic;

using System.IO;
using Financial.Integracao.Excel.Util;
using Financial.Integracao.Excel;
using Financial.Web.Common;

using EntitySpaces.Interfaces;

using DevExpress.Web;
using Financial.Bolsa;
using Financial.ContaCorrente;
using Financial.Investidor;
using Financial.Common;
using Financial.Common.Exceptions;
using Financial.Util;
using Financial.Bolsa.Exceptions;
using Financial.Bolsa.Enums;
using Financial.ContaCorrente.Enums;
using Financial.Common.Enums;
using Financial.Security;
using Financial.Web.Util;
using Financial.Security.Enums;

public partial class ImportacaoBasePage : BasePage {

    /* Estrutura do Excel */
    private List<ValoresExcelPosicaoTermoBolsa> valoresExcelPosicaoTermoBolsa = new List<ValoresExcelPosicaoTermoBolsa>();
    //
    List<int> idClienteDeletarPosicaoTermoBolsa = new List<int>();
    List<DateTime> dataHistoricoDeletarPosicaoTermoBolsa = new List<DateTime>();

    /// <summary>
    /// Leitura de um arquivo Excel
    /// Saida: List<ValoresExcel> com os valores armazenados no arquivo
    /// </summary>
    /// <param name="streamExcel">Stream de bytes de um arquivo Excel</param>
    /// <exception cref="Exception">Se ocorreu problemas</exception>
    private void LerArquivoPosicaoTermoBolsa(Stream streamExcel) {

        #region Leitura do Arquivo Excel
        // Open Spreadsheet
        Bytescout.Spreadsheet.Spreadsheet document = new Bytescout.Spreadsheet.Spreadsheet();
        document.LoadFromStream(streamExcel);

        Bytescout.Spreadsheet.Worksheet workSheet = document.Workbook.Worksheets[0];

        #region Confere Formato Arquivo
        string[] colunasConferencias = new string[] {"DataPosicao", "IdIndice", "IdCliente","CodigoBovespa",
                                                        "CdAtivoBolsa", "Quantidade", "PUTermo", "PUTermoLiquido",
                                                        "ValorTermo", "ValorTermoLiquido",
                                                        "DataOperacao", "DataVencimento",
                                                        "PuCustoLiquidoAcao", "NumeroContrato"};

        bool formato = ValoresExcelUtil.isExcelFormatoValido(colunasConferencias, workSheet);

        if (!formato) {
            string mensagem = "Formato Interno do Arquivo Inválido.\n";
            mensagem += "Linha 1 do Arquivo deve ser: \n";
            for (int i = 0; i < colunasConferencias.Length; i++) {
                int coluna = i + 1;
                mensagem += "\tColuna " + coluna + ": " + colunasConferencias[i] + "\n";
            }

            document.Close();
            document.Dispose();

            throw new Exception(mensagem);
        }
        #endregion

        //
        int index = 0;
        // This row,column index should be changed as per your need.
        // i.e. which cell in the excel you are interesting to read.
        //
        /* Formato: 1) dataPosicao - 2) IdIndice - 3)IdCliente -  4)CodigoBovespa - 5)CdAtivoBolsa - 6)Quantidade
         * 7)PuTermo - 8)PuTermoLiquido - 9)ValorTermo - 10)ValorTermoLiquido - 11)DataOperacao - 12)DataVencimento
         * 13)PuCustoLiquidoAcao - 14)NumeroContrato
         */

        int linha = 1;
        int coluna1 = 0, coluna2 = 1, coluna3 = 2,
            coluna4 = 3, coluna5 = 4, coluna6 = 5,
            coluna7 = 6, coluna8 = 7, coluna9 = 8,
            coluna10 = 9, coluna11 = 10, coluna12 = 11,
            coluna13 = 12, coluna14 = 13;

        //
        try {
            // Enquanto idCliente tiver valor
            while (workSheet.Cell(linha, coluna1).Value != null) {
                ValoresExcelPosicaoTermoBolsa item = new ValoresExcelPosicaoTermoBolsa();
                
                try {
                    item.DataPosicao = workSheet.Cell(linha, coluna1).ValueAsDateTime;
                }
                catch (Exception ex) {
                    throw new Exception(ex.Message + " - DataPosicao: linha " + (linha + 1));
                }

                if (workSheet.Cell(linha, coluna2).Value == null)
                    throw new Exception("IdIndice não informado: linha " + (linha + 1));
                try
                {
                    item.IdIndice = Convert.ToInt16(workSheet.Cell(linha, coluna2).Value);
                }
                catch (Exception ex) {
                    throw new Exception(ex.Message + " - IdIndice: linha " + (linha + 1));
                }

                if (item.IdIndice == 0) {
                    item.IdIndice = null;
                }

                if (workSheet.Cell(linha, coluna3).Value == null)
                    throw new Exception("IdCliente não informado: linha " + (linha + 1));
                try
                {
                    item.IdCliente = workSheet.Cell(linha, coluna3).ValueAsInteger;
                }
                catch (Exception ex) {
                    throw new Exception(ex.Message + " - IdCliente: linha " + (linha + 1));
                }

                if (workSheet.Cell(linha, coluna4).Value == null)
                    throw new Exception("CodigoBovespa não informado: linha " + (linha + 1));
                try
                {
                    item.CodigoBovespa = workSheet.Cell(linha, coluna4).ValueAsInteger;
                }
                catch (Exception ex) {
                    throw new Exception(ex.Message + " - CodigoBovespa: linha " + (linha + 1));
                }

                if (workSheet.Cell(linha, coluna5).Value == null)
                    throw new Exception("CdAtivoBolsa não informado: linha " + (linha + 1));
                try
                {
                    item.CdAtivoBolsa = workSheet.Cell(linha, coluna5).Value.ToString();
                }
                catch (Exception ex) {
                    throw new Exception(ex.Message + " - CdAtivoBolsa: linha " + (linha + 1));
                }

                if (workSheet.Cell(linha, coluna6).Value == null)
                    throw new Exception("Quantidade não informada: linha " + (linha + 1));
                try
                {
                    item.Quantidade = workSheet.Cell(linha, coluna6).ValueAsInteger;
                }
                catch (Exception ex) {
                    throw new Exception(ex.Message + " - Quantidade: linha " + (linha + 1));
                }

                if (workSheet.Cell(linha, coluna7).Value == null)
                    throw new Exception("PuTermo não informado: linha " + (linha + 1));
                try
                {
                    item.PuTermo = Convert.ToDecimal(workSheet.Cell(linha, coluna7).Value);
                }
                catch (Exception ex) {
                    throw new Exception(ex.Message + " - PuTermo: linha " + (linha + 1));
                }

                if (workSheet.Cell(linha, coluna8).Value == null)
                    throw new Exception("PuTermoLiquido não informado: linha " + (linha + 1));
                try
                {
                    item.PuTermoLiquido = Convert.ToDecimal(workSheet.Cell(linha, coluna8).Value);
                }
                catch (Exception ex) {
                    throw new Exception(ex.Message + " - PuTermoLiquido: linha " + (linha + 1));
                }

                if (workSheet.Cell(linha, coluna9).Value == null)
                    throw new Exception("ValorTermo não informado: linha " + (linha + 1));
                try
                {
                    item.ValorTermo = Convert.ToDecimal(workSheet.Cell(linha, coluna9).Value);
                }
                catch (Exception ex) {
                    throw new Exception(ex.Message + " - ValorTermo: linha " + (linha + 1));
                }

                if (workSheet.Cell(linha, coluna10).Value == null)
                    throw new Exception("ValorTermoLiquido não informado: linha " + (linha + 1));
                try
                {
                    item.ValorTermoLiquido = Convert.ToDecimal(workSheet.Cell(linha, coluna10).Value);
                }
                catch (Exception ex) {
                    throw new Exception(ex.Message + " - ValorTermoLiquido: linha " + (linha + 1));
                }

                if (workSheet.Cell(linha, coluna11).Value == null)
                    throw new Exception("DataOperacao não informada: linha " + (linha + 1));
                try
                {
                    item.DataOperacao = workSheet.Cell(linha, coluna11).ValueAsDateTime;
                }
                catch (Exception ex) {
                    throw new Exception(ex.Message + " - DataOperacao: linha " + (linha + 1));
                }

                if (workSheet.Cell(linha, coluna12).Value == null)
                    throw new Exception("DataVencimento não informada: linha " + (linha + 1));
                try
                {
                    item.DataVencimento = workSheet.Cell(linha, coluna12).ValueAsDateTime;
                }
                catch (Exception ex) {
                    throw new Exception(ex.Message + " - DataVencimento: linha " + (linha + 1));
                }

                if (workSheet.Cell(linha, coluna13).Value == null)
                    throw new Exception("PuCustoLiquidoAcao não informado: linha " + (linha + 1));
                try
                {
                    item.PuCustoLiquidoAcao = Convert.ToDecimal(workSheet.Cell(linha, coluna13).Value);
                }
                catch (Exception ex) {
                    throw new Exception(ex.Message + " - PuCustoLiquidoAcao: linha " + (linha + 1));
                }

                if (workSheet.Cell(linha, coluna14).Value == null)
                    throw new Exception("NumeroContrato não informado: linha " + (linha + 1));
                try
                {
                    item.NumeroContrato = Convert.ToString(workSheet.Cell(linha, coluna14).Value);
                }
                catch (Exception ex) {
                    throw new Exception(ex.Message + " - NumeroContrato: linha " + (linha + 1));
                }

                this.valoresExcelPosicaoTermoBolsa.Add(item);
                //
                index++;
                linha = 1 + index;
                //                
                Console.WriteLine("{0},{1},{2},{3},{4},{5},{6},{7},{8},{9},{10},{11},{12},{13} ",
                        item.DataPosicao, item.IdIndice, item.IdCliente, item.CodigoBovespa, item.CdAtivoBolsa,
                        item.Quantidade, item.PuTermo, item.PuTermoLiquido, item.ValorTermo, item.ValorTermoLiquido,
                        item.DataOperacao, item.DataVencimento, item.PuCustoLiquidoAcao, item.NumeroContrato);
            }
        }
        catch (Exception ex) {
            document.Close();
            document.Dispose();

            throw new Exception(ex.Message);
        }
        #endregion

        document.Close();
        document.Dispose();
    }

    /// <summary>
    /// Gera e Carrega as Posições Bolsa e Posição Bolsa Abertura de acordo
    /// com o objeto List<ValoresExcel>
    /// </summary>
    /// <exception cref="Exception">Se ocorreu problemas</exception>
    private void CarregaPosicaoTermoBolsa() {
        PosicaoTermoBolsaCollection posicaoTermoBolsaCollection = new PosicaoTermoBolsaCollection();
        PosicaoTermoBolsaCollection posicaoTermoBolsaDeletarCollection = new PosicaoTermoBolsaCollection();
        //
        PosicaoTermoBolsaAberturaCollection posicaoTermoBolsaAberturaCollection = new PosicaoTermoBolsaAberturaCollection();
        PosicaoTermoBolsaAberturaCollection posicaoTermoBolsaAberturaDeletarCollection = new PosicaoTermoBolsaAberturaCollection();
        //
        LiquidacaoCollection liquidacaoCollection = new LiquidacaoCollection();
        LiquidacaoCollection liquidacaoDeletarCollection = new LiquidacaoCollection();
        //
        LiquidacaoAberturaCollection liquidacaoAberturaCollection = new LiquidacaoAberturaCollection();
        LiquidacaoAberturaCollection liquidacaoAberturaDeletarCollection = new LiquidacaoAberturaCollection();

        //    
        for (int i = 0; i < this.valoresExcelPosicaoTermoBolsa.Count; i++) {
            ValoresExcelPosicaoTermoBolsa valoresExcel = this.valoresExcelPosicaoTermoBolsa[i];
            // Salva o Cliente para poder Deletar
            this.idClienteDeletarPosicaoTermoBolsa.Add(valoresExcel.IdCliente);
            // Salva a Data Historico para poder Deletar
            this.dataHistoricoDeletarPosicaoTermoBolsa.Add(valoresExcel.DataPosicao);
            //               

            #region Verifica se Existe Cliente
            Cliente cliente = new Cliente();
            List<esQueryItem> campos = new List<esQueryItem>();
            campos.Add(cliente.Query.IdCliente);
            campos.Add(cliente.Query.DataDia);
            if (!cliente.LoadByPrimaryKey(campos, valoresExcel.IdCliente)) {
                throw new Exception("Cliente Não existente : " + valoresExcel.IdCliente);
            }
            #endregion

            #region Testa AgenteMercado
            AgenteMercado agenteMercado = new AgenteMercado();
            int? idAgenteMercado = null;
            try {
                idAgenteMercado = (int)agenteMercado.BuscaIdAgenteMercadoBovespa(valoresExcel.CodigoBovespa);
            }
            catch (IdAgenteNaoCadastradoException e) {
                throw new IdAgenteNaoCadastradoException(e.Message);
            }
            #endregion

            if (idAgenteMercado.HasValue) {
                DateTime dataPosicao = valoresExcel.DataPosicao;

                #region PosicaoTermoBolsa
                //
                PosicaoTermoBolsa posicao = new PosicaoTermoBolsa();
                posicao.IdIndice = valoresExcel.IdIndice; //Admite null
                posicao.IdCliente = valoresExcel.IdCliente;
                posicao.IdAgente = idAgenteMercado.Value;
                posicao.Quantidade = valoresExcel.Quantidade;
                posicao.QuantidadeInicial = valoresExcel.Quantidade;
                //Colocar o PU medio da ação para termos comprados (p/ termos vendidos será usado p/ compor a RF)
                posicao.PUCustoLiquidoAcao = valoresExcel.PuCustoLiquidoAcao;
                //
                posicao.DataOperacao = valoresExcel.DataOperacao;
                posicao.DataVencimento = valoresExcel.DataVencimento;
                posicao.NumeroContrato = valoresExcel.NumeroContrato;
                //

                //Prazos calculados                    
                short prazoTotal = (short)Calendario.NumeroDias(valoresExcel.DataOperacao, valoresExcel.DataVencimento, LocalFeriadoFixo.Brasil, TipoFeriado.Brasil);
                short prazoDecorrer = (short)Calendario.NumeroDias(dataPosicao, valoresExcel.DataVencimento, LocalFeriadoFixo.Brasil, TipoFeriado.Brasil);

                posicao.PrazoTotal = prazoTotal;
                posicao.PrazoDecorrer = prazoDecorrer;
                //

                //Ativo objeto (ação) e ativo Termo
                valoresExcel.CdAtivoBolsa.ToUpper().Trim();
                string cdAtivoBolsaAcao = valoresExcel.CdAtivoBolsa.ToUpper().Trim(); ;
                string cdAtivoBolsaTermo = AtivoBolsa.RetornaCdAtivoBolsaConcatenado(cdAtivoBolsaAcao + "T", valoresExcel.DataVencimento);

                AtivoBolsa ativoBolsaExiste = new AtivoBolsa();
                if (!ativoBolsaExiste.LoadByPrimaryKey(cdAtivoBolsaTermo)) {
                    AtivoBolsa ativoBolsaAcao = new AtivoBolsa();
                    if (!ativoBolsaAcao.LoadByPrimaryKey(cdAtivoBolsaAcao)) {
                        throw new FatorCotacaoNaoCadastradoException("Ativo objeto não cadastrado : " + cdAtivoBolsaAcao);
                    }
                    string especificacao = ativoBolsaAcao.Especificacao;

                    AtivoBolsa ativoBolsaTermo = new AtivoBolsa();
                    ativoBolsaTermo.CdAtivoBolsa = cdAtivoBolsaTermo;
                    ativoBolsaTermo.CdAtivoBolsaObjeto = cdAtivoBolsaAcao;
                    ativoBolsaTermo.DataVencimento = valoresExcel.DataVencimento;
                    ativoBolsaTermo.Descricao = cdAtivoBolsaTermo;
                    ativoBolsaTermo.Especificacao = especificacao;
                    ativoBolsaTermo.IdEmissor = 1; //Default
                    ativoBolsaTermo.IdMoeda = 1; //Default
                    ativoBolsaTermo.TipoMercado = TipoMercadoBolsa.Termo;
                    ativoBolsaTermo.OpcaoIndice = 0;
                    ativoBolsaTermo.CodigoIsin = "";
                    ativoBolsaTermo.NumeroSerie = 0;
                    ativoBolsaTermo.TipoPapel = (byte)TipoPapelAtivo.Normal;

                    ativoBolsaTermo.Save();
                }
                //

                posicao.CdAtivoBolsa = cdAtivoBolsaTermo;

                FatorCotacaoBolsa fatorCotacao = new FatorCotacaoBolsa();
                if (!fatorCotacao.BuscaFatorCotacaoBolsa(cdAtivoBolsaAcao, dataPosicao)) {
                    throw new FatorCotacaoNaoCadastradoException("FatorCotacao não cadastrado para a data : " + cdAtivoBolsaAcao + " " + dataPosicao.ToString("d"));
                }

                #region PUMercado, ValorMercado
                CotacaoBolsa cotacaoBolsa = new CotacaoBolsa();
                cotacaoBolsa.BuscaCotacaoBolsa(cdAtivoBolsaAcao, dataPosicao);

                decimal puMercado = 0;
                if (cotacaoBolsa.es.HasData && cotacaoBolsa.PUMedio.HasValue) {
                    puMercado = cotacaoBolsa.PUMedio.Value;
                }

                decimal valorMercado = Utilitario.Truncate((valoresExcel.Quantidade * puMercado) / fatorCotacao.Fator.Value, 2);
                #endregion

                #region PUTermo, PUTermoLiquido, ValorTermo, ValorTermoLiquido
                decimal puTermo = valoresExcel.PuTermo;
                decimal puTermoLiquido = valoresExcel.PuTermoLiquido;
                decimal valorTermo = valoresExcel.ValorTermo;
                decimal valorTermoLiquido = valoresExcel.ValorTermoLiquido;

                bool puTermoZerado = (puTermo == 0 && puTermoLiquido == 0);
                bool valorTermoZerado = (valorTermo == 0 && valorTermoLiquido == 0);

                if (puTermoZerado && valorTermoZerado) {
                    throw new Exception("PU e Valor Termo (bruto e liquido) não podem estar todos zerados na linha!");
                }

                if (puTermoZerado) {
                    if (valorTermo == 0) {
                        valorTermo = valorTermoLiquido;
                    }
                    else if (valorTermoLiquido == 0) {
                        valorTermoLiquido = valorTermo;
                    }
                    puTermo = Utilitario.Truncate(valorTermo / (valoresExcel.Quantidade / fatorCotacao.Fator.Value), 2);
                    puTermoLiquido = puTermo;
                }
                else if (valorTermoZerado) {
                    if (puTermo == 0) {
                        puTermo = puTermoLiquido;
                    }
                    else if (puTermoLiquido == 0) {
                        puTermoLiquido = puTermo;
                    }
                    valorTermo = Utilitario.Truncate((valoresExcel.Quantidade * puTermo) / fatorCotacao.Fator.Value, 2);
                    valorTermoLiquido = valorTermo;
                }
                else if (puTermo == 0) {
                    puTermo = puTermoLiquido;
                }
                else if (puTermoLiquido == 0) {
                    puTermoLiquido = puTermo;
                }
                if (valorTermo == 0) {
                    valorTermo = valorTermoLiquido;
                }
                else if (valorTermoLiquido == 0) {
                    valorTermoLiquido = valorTermo;
                }
                #endregion

                decimal valorCurva = valorTermo;
                decimal valorCorrigido = valorTermo;

                posicao.PUTermo = puTermo;
                posicao.PUTermoLiquido = puTermoLiquido;
                posicao.ValorTermo = valorTermo;
                posicao.ValorTermoLiquido = valorTermoLiquido;
                posicao.PUMercado = puMercado;
                posicao.ValorMercado = valorMercado;
                posicao.ValorCurva = valorCurva;
                posicao.ValorCorrigido = valorCorrigido;
                posicao.TaxaAno = 0;
                posicao.TaxaMTM = 0;

                // Attach the entity
                posicaoTermoBolsaCollection.AttachEntity(posicao);

                #endregion
                //
                #region PosicaoTermoBolsaAbertura
                //
                PosicaoTermoBolsaAbertura posicaoAbertura = new PosicaoTermoBolsaAbertura();
                posicaoAbertura.IdIndice = posicao.IdIndice;
                posicaoAbertura.IdCliente = posicao.IdCliente;
                posicaoAbertura.DataHistorico = dataPosicao;
                posicaoAbertura.IdAgente = posicao.IdAgente;
                posicaoAbertura.CdAtivoBolsa = posicao.CdAtivoBolsa;
                posicaoAbertura.DataOperacao = posicao.DataOperacao;
                posicaoAbertura.DataVencimento = posicao.DataVencimento;
                posicaoAbertura.Quantidade = posicao.Quantidade;
                posicaoAbertura.QuantidadeInicial = posicao.QuantidadeInicial;
                posicaoAbertura.PrazoTotal = posicao.PrazoTotal;
                posicaoAbertura.PrazoDecorrer = posicao.PrazoDecorrer;
                posicaoAbertura.TaxaAno = posicao.TaxaAno;
                posicaoAbertura.TaxaMTM = posicao.TaxaMTM;
                posicaoAbertura.ValorTermo = posicao.ValorTermo;
                posicaoAbertura.ValorTermoLiquido = posicao.ValorTermoLiquido;
                posicaoAbertura.ValorMercado = posicao.ValorMercado;
                posicaoAbertura.ValorCurva = posicao.ValorCurva;
                posicaoAbertura.ValorCorrigido = posicao.ValorCorrigido;
                posicaoAbertura.PUCustoLiquidoAcao = posicao.PUCustoLiquidoAcao;
                posicaoAbertura.PUTermo = posicao.PUTermo;
                posicaoAbertura.PUTermoLiquido = posicao.PUTermoLiquido;
                posicaoAbertura.PUMercado = posicao.PUMercado;
                posicaoAbertura.NumeroContrato = posicao.NumeroContrato;

                // Attach the object
                posicaoTermoBolsaAberturaCollection.AttachEntity(posicaoAbertura);
                #endregion
                //
                #region Liquidação
                Liquidacao l = new Liquidacao();
                l.IdCliente = valoresExcel.IdCliente;
                l.DataLancamento = dataPosicao;
                l.DataVencimento = valoresExcel.DataVencimento;
                //                
                l.Descricao = valoresExcel.Quantidade >= 0 ? "Compra Termo - " : "Venda Termo - ";
                l.Descricao += "Quantidade: " + valoresExcel.Quantidade + " Ativo: " + valoresExcel.CdAtivoBolsa;
                //
                // Se Quantidade Positiva ValorTermo Negativo. - Se Quantidade Negativa ValorTermo Positivo
                l.Valor = valoresExcel.Quantidade > 0 ? (valoresExcel.ValorTermo * -1) : valoresExcel.ValorTermo;
                l.Situacao = (byte)SituacaoLancamentoLiquidacao.Normal;
                l.Origem = valoresExcel.Quantidade >= 0 ? OrigemLancamentoLiquidacao.Bolsa.CompraTermo : OrigemLancamentoLiquidacao.Bolsa.VendaTermo;
                l.Fonte = (byte)FonteLancamentoLiquidacao.Manual;

                ContaCorrente contaCorrente = new ContaCorrente();
                // Lança exceção se conta não encontrada
                l.IdConta = contaCorrente.RetornaContaDefault(valoresExcel.IdCliente);

                // Attach the entity
                liquidacaoCollection.AttachEntity(l);
                #endregion

                #region LiquidaçãoAbertura
                LiquidacaoAbertura la = new LiquidacaoAbertura();
                la.IdCliente = l.IdCliente;
                la.DataHistorico = dataPosicao;
                la.DataLancamento = l.DataLancamento;
                la.DataVencimento = l.DataVencimento;
                la.Descricao = l.Descricao;
                la.Valor = l.Valor;
                la.Situacao = l.Situacao;
                la.Origem = l.Origem;
                la.Fonte = l.Fonte;
                la.IdConta = l.IdConta;

                // Attach the entity
                liquidacaoAberturaCollection.AttachEntity(la);
                #endregion
            }
        }

        using (esTransactionScope scope = new esTransactionScope()) {

            #region Deleta PosicaoTermoBolsa por IdCliente
            posicaoTermoBolsaDeletarCollection.Query.Where(posicaoTermoBolsaDeletarCollection.Query.IdCliente.In(this.idClienteDeletarPosicaoTermoBolsa));
            posicaoTermoBolsaDeletarCollection.Query.Load();
            posicaoTermoBolsaDeletarCollection.MarkAllAsDeleted();
            posicaoTermoBolsaDeletarCollection.Save();
            #endregion

            #region Deleta PosicaoTermoBolsaAbertura por IdCliente/DataHistorico
            for (int i = 0; i < this.idClienteDeletarPosicaoTermoBolsa.Count; i++) {
                posicaoTermoBolsaAberturaDeletarCollection = new PosicaoTermoBolsaAberturaCollection();
                posicaoTermoBolsaAberturaDeletarCollection.Query
                    .Where(posicaoTermoBolsaAberturaDeletarCollection.Query.IdCliente == this.idClienteDeletarPosicaoTermoBolsa[i],
                           posicaoTermoBolsaAberturaDeletarCollection.Query.DataHistorico == this.dataHistoricoDeletarPosicaoTermoBolsa[i]);
                posicaoTermoBolsaAberturaDeletarCollection.Query.Load();
                posicaoTermoBolsaAberturaDeletarCollection.MarkAllAsDeleted();
                posicaoTermoBolsaAberturaDeletarCollection.Save();
            }
            #endregion

            // Salva Posicões presentes no Excel
            posicaoTermoBolsaCollection.Save();

            // Seta os novos idPosicao na PosicaoAbertura
            for (int i = 0; i < posicaoTermoBolsaCollection.Count; i++) {
                posicaoTermoBolsaAberturaCollection[i].IdPosicao = posicaoTermoBolsaCollection[i].IdPosicao;
            }
            posicaoTermoBolsaAberturaCollection.Save();
            //

            /* Liquidação */
            #region Deleta Liquidacao por IdCliente
            liquidacaoDeletarCollection.Query
                        .Where(liquidacaoDeletarCollection.Query.IdCliente.In(this.idClienteDeletarPosicaoTermoBolsa),
                               liquidacaoDeletarCollection.Query.Origem.In((int)OrigemLancamentoLiquidacao.Bolsa.CompraTermo, (int)OrigemLancamentoLiquidacao.Bolsa.VendaTermo),
                               liquidacaoDeletarCollection.Query.Fonte == (byte)FonteLancamentoLiquidacao.Manual);
            //
            liquidacaoDeletarCollection.Query.Load();
            liquidacaoDeletarCollection.MarkAllAsDeleted();
            liquidacaoDeletarCollection.Save();
            #endregion

            #region Deleta LiquidacaoAbertura por IdCliente/DataHistorico
            for (int i = 0; i < this.idClienteDeletarPosicaoTermoBolsa.Count; i++) {
                liquidacaoAberturaDeletarCollection = new LiquidacaoAberturaCollection();
                liquidacaoAberturaDeletarCollection.Query
                    .Where(liquidacaoAberturaDeletarCollection.Query.IdCliente == this.idClienteDeletarPosicaoTermoBolsa[i],
                           liquidacaoAberturaDeletarCollection.Query.DataHistorico == this.dataHistoricoDeletarPosicaoTermoBolsa[i],
                           liquidacaoAberturaDeletarCollection.Query.Origem.In((int)OrigemLancamentoLiquidacao.Bolsa.CompraTermo, (int)OrigemLancamentoLiquidacao.Bolsa.VendaTermo),
                           liquidacaoAberturaDeletarCollection.Query.Fonte == (byte)FonteLancamentoLiquidacao.Manual);

                liquidacaoAberturaDeletarCollection.Query.Load();
                liquidacaoAberturaDeletarCollection.MarkAllAsDeleted();
                liquidacaoAberturaDeletarCollection.Save();
            }
            #endregion

            // Salva Liquidações presentes no Excel
            liquidacaoCollection.Save();

            // Seta os novos idLiquidacao na LiquidacaoAbertura
            for (int i = 0; i < liquidacaoCollection.Count; i++) {
                liquidacaoAberturaCollection[i].IdLiquidacao = liquidacaoCollection[i].IdLiquidacao;
            }
            liquidacaoAberturaCollection.Save();
            //

            scope.Complete();
        }
    }

    /// <summary>
    /// Processa a Planilha de PosicaoTermo após ter acabado o Upload
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void uplPosicaoTermo_FileUploadComplete(object sender, FileUploadCompleteEventArgs e) {
        e.CallbackData = "";

        #region Trata Extensão Válida
        if (!ValoresExcelUtil.isExtensaoExcel(e.UploadedFile.FileName.Trim())) {
            e.CallbackData = "Importação Posição Termo - Formato de Arquivo Inválido. Formato permitido: .xls ou .xlsx\n\n";
            return;
        }
        #endregion

        // Stream de bytes com o Conteudo do Arquivo Excel
        Stream sr = e.UploadedFile.FileContent;

        #region Processamento Arquivo Excel

        //Reseta Vetor de Dados
        this.valoresExcelPosicaoTermoBolsa = new List<ValoresExcelPosicaoTermoBolsa>();
        //
        this.idClienteDeletarPosicaoTermoBolsa = new List<int>();
        this.dataHistoricoDeletarPosicaoTermoBolsa = new List<DateTime>();

        try {
            // Ler Arquivo
            this.LerArquivoPosicaoTermoBolsa(sr);
            // Carrega Arquivo
            this.CarregaPosicaoTermoBolsa();
        }
        catch (Exception e2) {
            e.CallbackData = "Importação Posição Termo - " + e2.Message;
            return;
        }

        new HistoricoLog().InsereHistoricoLog(DateTime.Now, DateTime.Now,
            "Importação Planilha Posição Termo Bolsa",
            HttpContext.Current.User.Identity.Name, UtilitarioWeb.GetIP(Request), "", HistoricoLogOrigem.Outros);

        #endregion
    }
}