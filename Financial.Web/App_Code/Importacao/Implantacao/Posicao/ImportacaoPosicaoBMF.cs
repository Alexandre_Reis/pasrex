﻿using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Financial.Web.Common;
using System.IO;
using Financial.Integracao.Excel.Util;
using Financial.Integracao.Excel;
using EntitySpaces.Interfaces;
using Financial.Common;
using Financial.Investidor;
using System.Collections.Generic;
using Financial.Investidor.Enums;
using DevExpress.Web;
using Financial.BMF;
using Financial.Common.Exceptions;
using Financial.Bolsa.Exceptions;
using Financial.BMF.Enums;
using Financial.Security;
using Financial.Web.Util;
using Financial.Security.Enums;

public partial class ImportacaoBasePage : BasePage {

    /* Estrutura do Excel */
    private List<ValoresExcelPosicaoBMF> valoresExcelPosicaoBMF = new List<ValoresExcelPosicaoBMF>();
    List<int> idClienteDeletarPosicaoBMF = new List<int>();
    List<DateTime> dataHistoricoDeletarPosicaoBMF = new List<DateTime>();

    /// <summary>
    /// Leitura de um arquivo Excel
    /// Saida: List<ValoresExcel> com os valores armazenados no arquivo
    /// </summary>    
    /// <param name="streamExcel">Stream de bytes de um arquivo Excel</param>
    /// <exception cref="Exception">Se ocorreu problemas</exception>
    private void LerArquivoPosicaoBMF(Stream streamExcel) {

        #region Leitura do Arquivo Excel

        // Open Spreadsheet
        Bytescout.Spreadsheet.Spreadsheet document = new Bytescout.Spreadsheet.Spreadsheet();
        document.LoadFromStream(streamExcel);

        Bytescout.Spreadsheet.Worksheet workSheet = document.Workbook.Worksheets[0];

        #region Confere Formato Arquivo
        string[] colunasConferencias = new string[] {"IdCliente","CodigoBMF","CdAtivoBMF",
                                                        "Serie","Quantidade","PUCusto"};

        bool formato = ValoresExcelUtil.isExcelFormatoValido(colunasConferencias, workSheet);

        if (!formato) {
            string mensagem = "Formato Interno do Arquivo Inválido.\n";
            mensagem += "Linha 1 do Arquivo deve ser: \n";
            for (int i = 0; i < colunasConferencias.Length; i++) {
                int coluna = i + 1;
                mensagem += "\tColuna " + coluna + ": " + colunasConferencias[i] + "\n";
            }

            document.Close();
            document.Dispose();

            throw new Exception(mensagem);
        }
        #endregion

        int index = 0;
        // This row,column index should be changed as per your need.
        // i.e. which cell in the excel you are interesting to read.
        //
        /* Formato: 1)IdCliente -  2)CodigoBMF - 3)CdAtivoBMF - 4)Serie - 5)Quantidade - 5)PuCusto             
         */
        int linha = 1;
        int coluna1 = 0, coluna2 = 1, coluna3 = 2,
            coluna4 = 3, coluna5 = 4, coluna6 = 5;

        //
        try {
            // Enquanto idCliente tiver valor
            while (workSheet.Cell(linha, coluna1).Value != null) {
                ValoresExcelPosicaoBMF item = new ValoresExcelPosicaoBMF();
                
                try {
                    item.IdCliente = workSheet.Cell(linha, coluna1).ValueAsInteger;
                }
                catch (Exception ex) {
                    throw new Exception(ex.Message + " - IdCliente: linha " + (linha + 1));
                }

                if (workSheet.Cell(linha, coluna2).Value == null)
                    throw new Exception("CodigoBMF não informado: linha " + (linha + 1));
                try {
                    item.CodigoBMF = workSheet.Cell(linha, coluna2).ValueAsInteger;
                }
                catch (Exception ex) {
                    throw new Exception(ex.Message + " - CodigoBMF: linha " + (linha + 1));
                }

                if (workSheet.Cell(linha, coluna3).Value == null)
                    throw new Exception("CdAtivoBMF não informado: linha " + (linha + 1));
                try
                {
                    item.CdAtivoBMF = workSheet.Cell(linha, coluna3).ValueAsString;
                }
                catch (Exception ex) {
                    throw new Exception(ex.Message + " - CdAtivoBMF: linha " + (linha + 1));
                }

                if (workSheet.Cell(linha, coluna4).Value == null)
                    throw new Exception("Serie não informado: linha " + (linha + 1));
                try
                {
                    item.Serie = workSheet.Cell(linha, coluna4).ValueAsString;
                }
                catch (Exception ex) {
                    throw new Exception(ex.Message + " - Serie: linha " + (linha + 1));
                }

                if (workSheet.Cell(linha, coluna5).Value == null)
                    throw new Exception("Quantidade não informado: linha " + (linha + 1));
                try
                {
                    item.Quantidade = Convert.ToInt32(workSheet.Cell(linha, coluna5).Value);
                }
                catch (Exception ex) {
                    throw new Exception(ex.Message + " - Quantidade: linha " + (linha + 1));
                }

                if (workSheet.Cell(linha, coluna6).Value == null)
                    throw new Exception("PuCusto não informado: linha " + (linha + 1));
                try
                {
                    item.PuCusto = Convert.ToDecimal(workSheet.Cell(linha, coluna6).Value);
                }
                catch (Exception ex) {
                    throw new Exception(ex.Message + " - PuCusto: linha " + (linha + 1));
                }

                this.valoresExcelPosicaoBMF.Add(item);
                //
                index++;
                linha = 1 + index;
                //                
                Console.WriteLine("{0},{1},{2},{3},{4},{5} ", item.IdCliente, item.CodigoBMF,
                                            item.CdAtivoBMF, item.Serie, item.Quantidade, item.PuCusto);
            }
        }
        catch (Exception ex) {
            document.Close();
            document.Dispose();

            throw new Exception(ex.Message);
        }
        #endregion

        document.Close();
        document.Dispose();

    }

    /// <summary>
    /// Gera e carrega as posiçõesBMF e PosicaoBMFAbertura de acordo com o objeto List<ValoresExcel>
    /// </summary>
    /// <exception cref="Exception">Se ocorreu problemas</exception>
    private void CarregaPosicaoBMF() {
        PosicaoBMFCollection posicaoBMFCollection = new PosicaoBMFCollection();
        PosicaoBMFCollection posicaoBMFDeletarCollection = new PosicaoBMFCollection();
        //
        PosicaoBMFAberturaCollection posicaoBMFAberturaCollection = new PosicaoBMFAberturaCollection();
        PosicaoBMFAberturaCollection posicaoBMFAberturaDeletarCollection = new PosicaoBMFAberturaCollection();
        //
        for (int i = 0; i < this.valoresExcelPosicaoBMF.Count; i++) {
            ValoresExcelPosicaoBMF valoresExcel = this.valoresExcelPosicaoBMF[i];
            // Salva o Cliente para poder Deletar
            this.idClienteDeletarPosicaoBMF.Add(valoresExcel.IdCliente);

            #region Testa AgenteMercado
            AgenteMercado agenteMercado = new AgenteMercado();
            try {
                agenteMercado.BuscaIdAgenteMercadoBMF(valoresExcel.CodigoBMF);
            }
            catch (IdAgenteNaoCadastradoException e) {
                throw new IdAgenteNaoCadastradoException(e.Message);
            }
            #endregion

            if (agenteMercado.IdAgente.HasValue) {
                #region PosicaoBMF
                PosicaoBMF posicao = new PosicaoBMF();
                posicao.IdCliente = valoresExcel.IdCliente;
                posicao.IdAgente = agenteMercado.IdAgente.Value;
                posicao.CdAtivoBMF = valoresExcel.CdAtivoBMF.ToUpper().Trim();
                posicao.Serie = valoresExcel.Serie.Trim();
                posicao.Quantidade = valoresExcel.Quantidade;
                posicao.PUCusto = valoresExcel.PuCusto;
                posicao.PUCustoLiquido = valoresExcel.PuCusto;
                //
                posicao.TipoMercado = posicao.UpToAtivoBMFByCdAtivoBMF.TipoMercado;
                posicao.DataVencimento = posicao.UpToAtivoBMFByCdAtivoBMF.DataVencimento;
                //
                #region PUMercado/ValorMercado/ValorCustoLiquido
                Cliente cliente = new Cliente();
                List<esQueryItem> campos = new List<esQueryItem>();
                campos.Add(cliente.Query.IdCliente);
                campos.Add(cliente.Query.DataDia);
                if (!cliente.LoadByPrimaryKey(campos, valoresExcel.IdCliente)) {
                    throw new Exception("Cliente Não existente : " + valoresExcel.IdCliente);
                }
                else {
                    // Salva a Data Historico para poder Deletar
                    this.dataHistoricoDeletarPosicaoBMF.Add(cliente.DataDia.Value);

                    CotacaoBMF cotacaoBMF = new CotacaoBMF();
                    if (!cotacaoBMF.LoadByPrimaryKey(cliente.DataDia.Value, valoresExcel.CdAtivoBMF.ToUpper(), valoresExcel.Serie)) {
                        DateTime data = cliente.DataDia.Value;
                        throw new CotacaoNaoCadastradaException("CotacaoBMF Não existente : " + data.ToString("dd/MM/yyyy") + ", " + valoresExcel.CdAtivoBMF.ToUpper() + ", " + valoresExcel.Serie);
                    }
                    else {
                        // Seta PuMercado
                        posicao.PUMercado = cotacaoBMF.PUFechamento;
                    }

                    #region Seta ValorMercado/ValorCustoLiquido
                    decimal peso = posicao.UpToAtivoBMFByCdAtivoBMF.Peso.Value;
                    posicao.ValorMercado = valoresExcel.Quantidade * posicao.PUMercado * peso;
                    posicao.ValorCustoLiquido = valoresExcel.Quantidade * posicao.PUCustoLiquido * peso;
                    #endregion
                }
                #endregion
                //
                posicao.QuantidadeInicial = valoresExcel.Quantidade;
                posicao.AjusteAcumulado = 0;
                posicao.AjusteDiario = 0;
                //
                posicao.ResultadoRealizar = posicao.TipoMercado == (byte)TipoMercadoBMF.Futuro
                                ? 0 : posicao.ValorMercado - posicao.ValorCustoLiquido;

                // Attach the object
                posicaoBMFCollection.AttachEntity(posicao);
                #endregion
                //
                #region PosicaoBMFAbertura
                PosicaoBMFAbertura posicaoAbertura = new PosicaoBMFAbertura();
                posicaoAbertura.IdCliente = posicao.IdCliente;
                posicaoAbertura.DataHistorico = cliente.DataDia.Value;
                posicaoAbertura.IdAgente = posicao.IdAgente;
                posicaoAbertura.CdAtivoBMF = posicao.CdAtivoBMF;
                posicaoAbertura.Serie = posicao.Serie;
                posicaoAbertura.Quantidade = posicao.Quantidade;
                posicaoAbertura.PUCusto = posicao.PUCusto;
                posicaoAbertura.PUCustoLiquido = posicao.PUCustoLiquido;
                posicaoAbertura.TipoMercado = posicao.TipoMercado;
                posicaoAbertura.DataVencimento = posicao.DataVencimento;
                posicaoAbertura.PUMercado = posicao.PUMercado;
                posicaoAbertura.ValorMercado = posicao.ValorMercado;
                posicaoAbertura.ValorCustoLiquido = posicao.ValorCustoLiquido;
                posicaoAbertura.QuantidadeInicial = posicao.QuantidadeInicial;
                posicaoAbertura.AjusteAcumulado = posicao.AjusteAcumulado;
                posicaoAbertura.AjusteDiario = posicao.AjusteDiario;
                posicaoAbertura.ResultadoRealizar = posicao.ResultadoRealizar;
                //
                // Attach the object
                posicaoBMFAberturaCollection.AttachEntity(posicaoAbertura);
                #endregion
            }
        }

        using (esTransactionScope scope = new esTransactionScope()) {
            #region Deleta PosicaoBMF por IdCliente
            posicaoBMFDeletarCollection.Query.Where(posicaoBMFDeletarCollection.Query.IdCliente.In(this.idClienteDeletarPosicaoBMF));
            posicaoBMFDeletarCollection.Query.Load();
            posicaoBMFDeletarCollection.MarkAllAsDeleted();
            posicaoBMFDeletarCollection.Save();
            #endregion

            #region Deleta PosicaoBMFAbertura por IdCliente/DataHistorico
            for (int i = 0; i < this.idClienteDeletarPosicaoBMF.Count; i++) {
                posicaoBMFAberturaDeletarCollection = new PosicaoBMFAberturaCollection();
                posicaoBMFAberturaDeletarCollection.Query
                    .Where(posicaoBMFAberturaDeletarCollection.Query.IdCliente == this.idClienteDeletarPosicaoBMF[i],
                           posicaoBMFAberturaDeletarCollection.Query.DataHistorico == this.dataHistoricoDeletarPosicaoBMF[i]);
                posicaoBMFAberturaDeletarCollection.Query.Load();
                posicaoBMFAberturaDeletarCollection.MarkAllAsDeleted();
                posicaoBMFAberturaDeletarCollection.Save();
            }
            #endregion

            // Salva Posicões presentes no Excel
            posicaoBMFCollection.Save();

            // Seta os novos idPosicao na PosicaoAbertura
            for (int i = 0; i < posicaoBMFCollection.Count; i++) {
                posicaoBMFAberturaCollection[i].IdPosicao = posicaoBMFCollection[i].IdPosicao;
            }
            posicaoBMFAberturaCollection.Save();
            scope.Complete();
        }
    }

    /// <summary>
    /// Processa a Planilha de PosicaoBMF após ter acabado o Upload
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void uplPosicaoBMF_FileUploadComplete(object sender, FileUploadCompleteEventArgs e) {
        e.CallbackData = "";

        #region Trata Extensão Válida
        if (!ValoresExcelUtil.isExtensaoExcel(e.UploadedFile.FileName.Trim())) {
            e.CallbackData = "Importação Posição BMF - Formato de Arquivo Inválido. Formato permitido: .xls ou .xlsx\n\n";
            return;
        }
        #endregion

        // Stream de bytes com o Conteudo do Arquivo Excel
        Stream sr = e.UploadedFile.FileContent;

        #region Processamento Arquivo Excel

        //Reseta Vetor de Dados
        this.valoresExcelPosicaoBMF = new List<ValoresExcelPosicaoBMF>();
        //
        this.idClienteDeletarPosicaoBMF = new List<int>();
        this.dataHistoricoDeletarPosicaoBMF = new List<DateTime>();

        try {
            // Ler Arquivo
            this.LerArquivoPosicaoBMF(sr);
            // Carrega Arquivo
            this.CarregaPosicaoBMF();
        }
        catch (Exception e2) {
            e.CallbackData = "Importação Posição BMF - " + e2.Message;
            return;
        }

        new HistoricoLog().InsereHistoricoLog(DateTime.Now, DateTime.Now,
                        "Importação Planilha Posição BMF",
                        HttpContext.Current.User.Identity.Name, UtilitarioWeb.GetIP(Request), "", HistoricoLogOrigem.Outros);

        #endregion
    }
}
