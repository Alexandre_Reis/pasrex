﻿using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Collections.Generic;

using System.IO;
using Financial.Integracao.Excel.Util;
using Financial.Integracao.Excel;
using Financial.Web.Common;

using EntitySpaces.Interfaces;

using DevExpress.Web;
using Financial.InvestidorCotista;
using Financial.Investidor;
using Financial.Fundo;
using Financial.Fundo.Enums;
using Financial.Util;
using Financial.Security;
using Financial.Security.Enums;
using Financial.Web.Util;
using Financial.Util.Enums;
using Financial.InvestidorCotista.Enums;
using Financial.Tributo;
using Financial.Tributo.Enums;
using Financial.Common.Enums;
using Financial.Tributo.Custom;
using Financial.Interfaces;
using Financial.Interfaces.Import.Fundo;


public partial class ImportacaoBasePage : BasePage {

    /* Estrutura do Excel */
    private List<ValoresExcelPosicaoCotista> valoresExcelPosicaoCotista = new List<ValoresExcelPosicaoCotista>();
    //
    List<int> idClienteDeletarPosicaoCotista = new List<int>();
    List<int> idClienteDeletarPosicaoCotistaIdCarteira = new List<int>();
    List<DateTime> dataHistoricoDeletarPosicaoCotista = new List<DateTime>();
    
    /// <summary>
    /// Leitura de um arquivo Excel
    /// Saida: List<ValoresExcel> com os valores armazenados no arquivo
    /// </summary>
    /// <param name="streamExcel">Stream de bytes de um arquivo Excel</param>
    /// <exception cref="Exception">Se ocorreu problemas</exception>
    private void LerArquivoPosicaoCotista(Stream streamExcel)
    {
        #region Leitura do Arquivo Excel

        // Open Spreadsheet
        Bytescout.Spreadsheet.Spreadsheet document = new Bytescout.Spreadsheet.Spreadsheet();
        document.LoadFromStream(streamExcel);

        Bytescout.Spreadsheet.Worksheet workSheet = document.Workbook.Worksheets[0];
        //

        #region Confere Formato Arquivo
        string[] colunasConferencias = new string[] 
                                {"IdCotista","IdCarteira","ValorAplicacao","DataAplicacao","DataConversao",
					             "Quantidade","CotaAplicacao","DataUltimaCobrancaIR", "DataUltimoCortePfee", "DataLiquidacao"};

        bool formato = ValoresExcelUtil.isExcelFormatoValido(colunasConferencias, workSheet);

        if (!formato) {
            string mensagem = "Formato Interno do Arquivo Inválido.\n";
            mensagem += "Linha 1 do Arquivo deve ser: \n";
            for (int i = 0; i < colunasConferencias.Length; i++) {
                int coluna = i + 1;
                mensagem += "\tColuna " + coluna + ": " + colunasConferencias[i] + "\n";
            }

            document.Close();
            document.Dispose();

            throw new Exception(mensagem);
        }
        #endregion

        int index = 0;
        // This row,column index should be changed as per your need.
        // i.e. which cell in the excel you are interesting to read.
        //
        /* Formato: 1)IdCotista  - 2)IdCarteira    - 3)ValorAplicacao - 4)DataAplicacao  - 5)DataConversao
         *          6)Quantidade - 7)CotaAplicacao - 8)DataUltimaCobrancaIR - 9)DataUltimoCortePfee - 10)DataLiquidacao
         */

        int linha = 1;
        int coluna1 = 0, coluna2 = 1, coluna3 = 2,
            coluna4 = 3, coluna5 = 4, coluna6 = 5,
            coluna7 = 6, coluna8 = 7,
            coluna9 = 8, coluna10 = 9;

        try {
            // Enquanto idCotista tiver valor
            while (workSheet.Cell(linha, coluna1).Value != null) {
                ValoresExcelPosicaoCotista item = new ValoresExcelPosicaoCotista();

                try {                    
                    item.IdCotista = Convert.ToInt32(workSheet.Cell(linha, coluna1).Value);
                }
                catch (Exception ex) {
                    throw new Exception(ex.Message + " - IdCotista: linha " + (linha + 1));
                }

                if (workSheet.Cell(linha, coluna2).Value == null)
                    throw new Exception("IdCarteira não informada: linha " + (linha + 1));
                try
                {                    
                    item.IdCarteira = Convert.ToInt32(workSheet.Cell(linha, coluna2).Value);
                }
                catch (Exception ex) {
                    throw new Exception(ex.Message + " - IdCarteira: linha " + (linha + 1));
                }

                if (workSheet.Cell(linha, coluna3).Value == null)
                    throw new Exception("ValorAplicacao não informado: linha " + (linha + 1));
                try
                {                    
                    item.ValorAplicacao = Convert.ToDecimal(workSheet.Cell(linha, coluna3).Value);
                }
                catch (Exception ex) {
                    throw new Exception(ex.Message + " - ValorAplicacao: linha " + (linha + 1));
                }

                if (workSheet.Cell(linha, coluna4).Value == null)
                    throw new Exception("DataAplicacao não informado: linha " + (linha + 1));
                try
                {                    
                    item.DataAplicacao = workSheet.Cell(linha, coluna4).ValueAsDateTime;
                }
                catch (Exception ex) {
                    throw new Exception(ex.Message + " - DataAplicacao: linha " + (linha + 1));
                }

                if (workSheet.Cell(linha, coluna5).Value == null)
                    throw new Exception("DataConversao não informado: linha " + (linha + 1));
                try
                {                    
                    item.DataConversao = workSheet.Cell(linha, coluna5).ValueAsDateTime;
                }
                catch (Exception ex) {
                    throw new Exception(ex.Message + " - DataConversao: linha " + (linha + 1));
                }

                if(DateTime.Compare(item.DataAplicacao, item.DataConversao) > 0)
                {
                    throw new Exception("Data aplicação não pode ser superior a data conversão: linha " + (linha + 1));
                }

                if (workSheet.Cell(linha, coluna6).Value == null)
                    throw new Exception("Quantidade não informada: linha " + (linha + 1));
                try
                {                    
                    item.Quantidade = Convert.ToDecimal(workSheet.Cell(linha, coluna6).Value);
                }
                catch (Exception ex) {
                    throw new Exception(ex.Message + " - Quantidade: linha " + (linha + 1));
                }

                if (workSheet.Cell(linha, coluna7).Value == null)
                    throw new Exception("CotaAplicacao não informada: linha " + (linha + 1));
                try
                {                    
                    item.CotaAplicacao = Convert.ToDecimal(workSheet.Cell(linha, coluna7).Value);
                }
                catch (Exception ex) {
                    throw new Exception(ex.Message + " - Quantidade: linha " + (linha + 1));
                }

                if (workSheet.Cell(linha, coluna8).Value != null) {
                    try {
                        item.DataUltimaCobrancaIR = workSheet.Cell(linha, coluna8).ValueAsDateTime;
                    }
                    catch (Exception ex) {
                        throw new Exception(ex.Message + " - DataUltimaCobrancaIR: linha " + (linha + 1));
                    }
                }

                if (workSheet.Cell(linha, coluna9).Value != null)
                {
                    try
                    {
                        item.DataUltimoCortePfee = workSheet.Cell(linha, coluna9).ValueAsDateTime;
                    }
                    catch (Exception ex)
                    {
                        throw new Exception(ex.Message + " - DataUltimoCortePfee: linha " + (linha + 1));
                    }
                }

                if (workSheet.Cell(linha, coluna10).Value != null)
                {
                    try
                    {
                        item.DataLiquidacao = workSheet.Cell(linha, coluna10).ValueAsDateTime;
                    }
                    catch (Exception ex)
                    {
                        throw new Exception(ex.Message + " - DataLiquidacao: linha " + (linha + 1));
                    }
                }

                this.valoresExcelPosicaoCotista.Add(item);
                //
                index++;
                linha = 1 + index;
                //                

                Console.WriteLine("{0},{1},{2},{3},{4},{5},{6},{7},{8},{9}",
                    item.IdCotista, item.IdCarteira, item.ValorAplicacao, item.DataAplicacao, item.DataConversao,
                    item.Quantidade, item.CotaAplicacao, item.DataUltimaCobrancaIR, item.DataUltimoCortePfee, item.DataLiquidacao);

            }
        }
        catch (Exception ex) {
            document.Close();
            document.Dispose();

            throw new Exception(ex.Message);
        }
        #endregion

        document.Close();
        document.Dispose();
    }

    /// <summary>
    /// Gera e carrega as posições Cotista e Posições Cotista Abertura de acordo 
    /// com o objeto List<ValoresExcel>
    /// </summary>
    /// <exception cref="Exception">Se ocorreu problemas</exception>
    private void CarregaPosicaoCotista() {
        OperacaoCotistaCollection operacaoCotistaCollection = new OperacaoCotistaCollection();
        OperacaoCotistaCollection operacaoCotistaDeletarCollection = new OperacaoCotistaCollection();

        PosicaoCotistaCollection posicaoCotistaCollection = new PosicaoCotistaCollection();
        PosicaoCotistaCollection posicaoCotistaDeletarCollection = new PosicaoCotistaCollection();
        //
        PosicaoCotistaAberturaCollection posicaoCotistaAberturaCollection = new PosicaoCotistaAberturaCollection();
        PosicaoCotistaAberturaCollection posicaoCotistaAberturaDeletarCollection = new PosicaoCotistaAberturaCollection();
        PosicaoCotistaHistoricoCollection posicaoCotistaHistoricoDeletarCollection = new PosicaoCotistaHistoricoCollection();
        //
        ImportacaoComeCotasCollection importacaoComeCotasCollection = new ImportacaoComeCotasCollection();

        for (int i = 0; i < this.valoresExcelPosicaoCotista.Count; i++) {
            ValoresExcelPosicaoCotista valoresExcel = this.valoresExcelPosicaoCotista[i];
            //
            this.idClienteDeletarPosicaoCotista.Add(valoresExcel.IdCotista);
            this.idClienteDeletarPosicaoCotistaIdCarteira.Add(valoresExcel.IdCarteira);

            #region Verifica se Existe Carteira
            Carteira carteira = new Carteira();
            if (!carteira.LoadByPrimaryKey(valoresExcel.IdCarteira)) {
                throw new Exception("Carteira Não existente : " + valoresExcel.IdCarteira);
            }
            #endregion

            #region DataDia do Cliente
            Cliente cliente = new Cliente();
            List<esQueryItem> campos = new List<esQueryItem>();
            campos.Add(cliente.Query.IdCliente);
            campos.Add(cliente.Query.DataDia);
            campos.Add(cliente.Query.DataImplantacao);
            if (!cliente.LoadByPrimaryKey(campos, valoresExcel.IdCarteira)) {
                throw new Exception("Cliente Não existente : " + valoresExcel.IdCarteira);
            }

            // Salva a Data Historico para poder Deletar
            this.dataHistoricoDeletarPosicaoCotista.Add(cliente.DataImplantacao.Value);
            #endregion

            if (valoresExcel.CotaAplicacao == 0)
            {
                HistoricoCota historicoCotaAplicacao = new HistoricoCota();
                if (historicoCotaAplicacao.BuscaValorCota(valoresExcel.IdCarteira, valoresExcel.DataConversao) && historicoCotaAplicacao.CotaFechamento.HasValue)
                {
                    valoresExcel.CotaAplicacao = historicoCotaAplicacao.CotaFechamento.Value;
                }
            }

            if(valoresExcel.DataAplicacao > cliente.DataImplantacao.Value)
                throw new Exception(" Data de Conversão deve ser menor ou igual que a data de Implantação do Cliente: " + valoresExcel.IdCarteira);

            if (valoresExcel.DataAplicacao < carteira.DataInicioCota.Value)
                throw new Exception(" Data de Aplicação deve ser maior ou igual que a data de Início da Carteira: " + valoresExcel.IdCarteira);

            #region Verifica se Existe Cotista
            Cotista cotista = new Cotista();
            if (!cotista.LoadByPrimaryKey(valoresExcel.IdCotista))
            {
                throw new Exception("Cotista Não existente : " + valoresExcel.IdCotista);
            }
            #endregion

            #region PosicaoCotista
            PosicaoCotista posicao = new PosicaoCotista();
            // Dados do Arquivo
            posicao.IdCotista = valoresExcel.IdCotista;
            posicao.IdCarteira = valoresExcel.IdCarteira;
            posicao.ValorAplicacao = valoresExcel.ValorAplicacao;
            posicao.DataAplicacao = valoresExcel.DataAplicacao;
            posicao.DataConversao = valoresExcel.DataConversao;
            posicao.Quantidade = valoresExcel.Quantidade;
            posicao.CotaAplicacao = valoresExcel.CotaAplicacao;
            posicao.DataUltimaCobrancaIR = valoresExcel.DataUltimaCobrancaIR != null
                                            ? valoresExcel.DataUltimaCobrancaIR
                                            : cliente.DataImplantacao;

            if (valoresExcel.DataUltimoCortePfee != null)
            {
                posicao.DataUltimoCortePfee = valoresExcel.DataUltimoCortePfee.Value;
            }
            //
            posicao.QuantidadeBloqueada = 0;
            posicao.ValorIR = 0;
            posicao.ValorIOF = 0;
            posicao.ValorPerformance = 0;
            posicao.ValorRendimento = 0;
            posicao.PosicaoIncorporada = "N";
            //
            posicao.QuantidadeInicial = valoresExcel.Quantidade;

            #region CotaDia
            HistoricoCota historicoCota = new HistoricoCota();
            List<esQueryItem> camposAux = new List<esQueryItem>();
            camposAux.Add(historicoCota.Query.CotaFechamento);
            //
            posicao.CotaDia = historicoCota.LoadByPrimaryKey(camposAux, cliente.DataImplantacao.Value, valoresExcel.IdCarteira)
                              ? historicoCota.CotaFechamento.Value
                              : 0;
            #endregion

            posicao.ValorBruto = Utilitario.Truncate((posicao.Quantidade.Value * posicao.CotaDia.Value), 2);
            posicao.ValorLiquido = posicao.ValorBruto.Value - posicao.ValorIR.Value - posicao.ValorIOF.Value;
            //

            posicao.QuantidadeAntesCortes = valoresExcel.Quantidade * AgendaComeCotas.fatorQuantidadeCotista(posicao, cliente.DataImplantacao.Value);
            //posicao.ValorIOFVirtual = valoresExcel.ValorIOFVirtual.GetValueOrDefault(0);

            // Attach the object
            posicaoCotistaCollection.AttachEntity(posicao);
            #endregion

            #region Operacao Cotista
            OperacaoCotista operacaoCotista = operacaoCotistaCollection.AddNew();
            operacaoCotista.IdCotista = posicao.IdCotista.Value;
            operacaoCotista.IdCarteira = posicao.IdCarteira.Value;
            operacaoCotista.DataOperacao = posicao.DataAplicacao.Value;
            operacaoCotista.DataConversao = posicao.DataConversao.Value;
            operacaoCotista.DataLiquidacao = valoresExcel.DataLiquidacao.Value;
            operacaoCotista.DataAgendamento = posicao.DataAplicacao.Value;
            operacaoCotista.TipoOperacao = 1;
            operacaoCotista.TipoResgate = null;
            operacaoCotista.IdPosicaoResgatada = null;
            operacaoCotista.IdFormaLiquidacao = 1;
            operacaoCotista.Quantidade = posicao.QuantidadeInicial.Value;
            operacaoCotista.CotaOperacao = posicao.CotaAplicacao.Value;
            operacaoCotista.ValorBruto = posicao.ValorBruto.Value;
            operacaoCotista.ValorLiquido = posicao.ValorLiquido.Value;
            operacaoCotista.ValorIR = posicao.ValorIR.Value;
            operacaoCotista.ValorIOF = posicao.ValorIOF.Value;
            operacaoCotista.ValorCPMF = 0;
            operacaoCotista.ValorPerformance = posicao.ValorPerformance.Value;
            operacaoCotista.PrejuizoUsado = 0;
            operacaoCotista.RendimentoResgate = 0;
            operacaoCotista.VariacaoResgate = 0;
            operacaoCotista.IdConta = null;
            operacaoCotista.Observacao = string.Empty;
            operacaoCotista.Fonte = (byte)FonteOperacaoCotista.Manual;
            operacaoCotista.IdAgenda = null;
            operacaoCotista.CotaInformada = posicao.CotaAplicacao.Value;
            operacaoCotista.IdOperacaoResgatada = null;
            operacaoCotista.RegistroEditado = "N";
            operacaoCotista.ValoresColados = "N";
            operacaoCotista.IdLocalNegociacao = 1;
            operacaoCotista.DataRegistro = posicao.DataConversao.Value;
            #endregion

            #region PosicaoCotistaAbertura
            PosicaoCotistaAbertura posicaoAbertura = new PosicaoCotistaAbertura();
            posicaoAbertura.IdCotista = posicao.IdCotista;
            posicaoAbertura.IdCarteira = posicao.IdCarteira;
            posicaoAbertura.DataHistorico = cliente.DataImplantacao.Value;
            posicaoAbertura.ValorAplicacao = posicao.ValorAplicacao;
            posicaoAbertura.DataAplicacao = posicao.DataAplicacao;
            posicaoAbertura.DataConversao = posicao.DataConversao;
            posicaoAbertura.Quantidade = posicao.Quantidade;
            posicaoAbertura.CotaAplicacao = posicao.CotaAplicacao;
            posicaoAbertura.DataUltimaCobrancaIR = posicao.DataUltimaCobrancaIR;
            posicaoAbertura.DataUltimoCortePfee = posicao.DataUltimoCortePfee;
            posicaoAbertura.ValorIOFVirtual = posicao.ValorIOFVirtual;
            posicaoAbertura.ValorLiquido = posicao.ValorLiquido;
            posicaoAbertura.QuantidadeBloqueada = posicao.QuantidadeBloqueada;
            posicaoAbertura.ValorIR = posicao.ValorIR;
            posicaoAbertura.QuantidadeAntesCortes = posicao.QuantidadeAntesCortes;
            posicaoAbertura.ValorIOF = 0;
            posicaoAbertura.PosicaoIncorporada = "N";
            posicaoAbertura.ValorPerformance = posicao.ValorPerformance;
            posicaoAbertura.QuantidadeInicial = posicao.QuantidadeInicial;
            posicaoAbertura.CotaDia = posicao.CotaDia;
            posicaoAbertura.ValorBruto = posicao.ValorBruto;
            posicaoAbertura.ValorRendimento = posicao.ValorRendimento;
            //
            // Attach the object
            posicaoCotistaAberturaCollection.AttachEntity(posicaoAbertura);
            #endregion

        }

        using (esTransactionScope scope = new esTransactionScope())
        {
            operacaoCotistaCollection.Save();
            for (int i = 0; i < operacaoCotistaCollection.Count; i++)
            {
                OperacaoCotista operacaoCotista = operacaoCotistaCollection[i];
                PosicaoCotista posicaoCotista = posicaoCotistaCollection[i];
                PosicaoCotistaAbertura posicaoCotistaAbertura = posicaoCotistaAberturaCollection[i];

                posicaoCotista.IdOperacao = operacaoCotista.IdOperacao.Value;
                posicaoCotistaAbertura.IdOperacao = operacaoCotista.IdOperacao.Value;
            }

            #region Deleta PosicaoCotistaAbertura/PosicaoCotistaHistorico por IdCliente/DataHistorico
            for (int i = 0; i < this.idClienteDeletarPosicaoCotista.Count; i++)
            {
                posicaoCotistaAberturaDeletarCollection = new PosicaoCotistaAberturaCollection();
                posicaoCotistaAberturaDeletarCollection.Query
                .Where(posicaoCotistaAberturaDeletarCollection.Query.IdCarteira == this.idClienteDeletarPosicaoCotistaIdCarteira[i],
                       posicaoCotistaAberturaDeletarCollection.Query.DataHistorico == this.dataHistoricoDeletarPosicaoCotista[i]);
                posicaoCotistaAberturaDeletarCollection.Query.Load();
                posicaoCotistaAberturaDeletarCollection.MarkAllAsDeleted();
                posicaoCotistaAberturaDeletarCollection.Save();
            }

            for (int i = 0; i < this.idClienteDeletarPosicaoCotista.Count; i++)
            {
                posicaoCotistaHistoricoDeletarCollection = new PosicaoCotistaHistoricoCollection();
                posicaoCotistaHistoricoDeletarCollection.Query
                .Where(posicaoCotistaHistoricoDeletarCollection.Query.IdCarteira == this.idClienteDeletarPosicaoCotistaIdCarteira[i],
                       posicaoCotistaHistoricoDeletarCollection.Query.DataHistorico >= this.dataHistoricoDeletarPosicaoCotista[i]);
                posicaoCotistaHistoricoDeletarCollection.Query.Load();
                posicaoCotistaHistoricoDeletarCollection.MarkAllAsDeleted();
                posicaoCotistaHistoricoDeletarCollection.Save();
            }

            #region Deleta PosicaoCotista/ComeCotas por IdCliente
            operacaoCotistaDeletarCollection = new OperacaoCotistaCollection();
            posicaoCotistaDeletarCollection.Query.Where(posicaoCotistaDeletarCollection.Query.IdCarteira.In(this.idClienteDeletarPosicaoCotistaIdCarteira));
            posicaoCotistaDeletarCollection.Query.Load();
            List<int?> idOperacaoCotista = new List<int?>();
            foreach (PosicaoCotista posicaoCotista in posicaoCotistaDeletarCollection)
            {
                AgendaComeCotasCollection agendaComeCotasCollection = new AgendaComeCotasCollection();
                agendaComeCotasCollection.Query.Where(agendaComeCotasCollection.Query.IdPosicao.Equal(posicaoCotista.IdPosicao.Value),
                                                        agendaComeCotasCollection.Query.TipoPosicao.Equal(TipoComeCotas.Cotista.GetHashCode()));
                agendaComeCotasCollection.Query.Load();
                agendaComeCotasCollection.MarkAllAsDeleted();
                agendaComeCotasCollection.Save();

                //ImportacaoComeCotas importacaoComeCotas = new ImportacaoComeCotas();
                //if (importacaoComeCotas.LoadByPrimaryKey(posicaoCotista.IdPosicao.Value, TipoComeCotas.Cotista.GetHashCode()))
                //{
                //    importacaoComeCotas.MarkAsDeleted();
                //    importacaoComeCotas.Save();
                //}

                if (posicaoCotista.IdOperacao.Value != null)
                    idOperacaoCotista.Add(posicaoCotista.IdOperacao.Value);
            }
            //
            #endregion

            if (idOperacaoCotista.Count > 0)
            {
                operacaoCotistaDeletarCollection.Query.Where(operacaoCotistaDeletarCollection.Query.IdOperacao.In(idOperacaoCotista));
                operacaoCotistaDeletarCollection.Query.Load();
            }

            operacaoCotistaDeletarCollection.MarkAllAsDeleted();
            posicaoCotistaDeletarCollection.MarkAllAsDeleted();
            posicaoCotistaDeletarCollection.Save();
            operacaoCotistaDeletarCollection.Save();
            #endregion

            operacaoCotistaCollection.Save();
            posicaoCotistaCollection.Save();

            //Gera agendaComeCotas para as posicões importadas
            for (int i = 0; i < posicaoCotistaCollection.Count; i++)
            {
                PosicaoCotista posicaoCotista = posicaoCotistaCollection[i];
                AgendaComeCotas agendaComeCotas = AgendaComeCotas.agendaComeCotasCotista(posicaoCotista, posicaoCotistaAberturaCollection[i].DataHistorico.Value);

                if (agendaComeCotas == null)
                    posicaoCotistaAberturaCollection[i].ValorIOFVirtual = 0;
                else
                    posicaoCotistaAberturaCollection[i].ValorIOFVirtual = agendaComeCotas.ValorIOFVirtual.Value * agendaComeCotas.QuantidadeAntesCortes;
            }

            // Seta os novos idPosicao na PosicaoAbertura
            for (int i = 0; i < posicaoCotistaCollection.Count; i++)
            {
                posicaoCotistaAberturaCollection[i].IdPosicao = posicaoCotistaCollection[i].IdPosicao;
            }

            posicaoCotistaAberturaCollection.Save();
            
            for (int i = 0; i < operacaoCotistaCollection.Count; i++)
            {
                PosicaoCotista posicaoCotista = posicaoCotistaCollection[i];
                PosicaoCotistaAbertura posicaoCotistaAbertura = posicaoCotistaAberturaCollection[i];

                posicaoCotistaAbertura.IdPosicao = posicaoCotista.IdPosicao.Value;
                //importacaoComeCotasCollection[i].IdPosicao = posicaoCotistaAbertura.IdPosicao.Value;
            }

            posicaoCotistaAberturaCollection.Save();
            //importacaoComeCotasCollection.Save();

            scope.Complete();
        }
    }
    
    /// <summary>
    /// Processa a Planilha de PosicaoCotista após ter acabado o Upload
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void uplPosicaoCotista_FileUploadComplete(object sender, FileUploadCompleteEventArgs e) {
        e.CallbackData = "";

        #region Trata Extensão Válida
        if (!ValoresExcelUtil.isExtensaoExcel(e.UploadedFile.FileName.Trim())) {
            e.CallbackData = "Importação Posição Cotista - Formato de Arquivo Inválido. Formato permitido: .xls ou .xlsx\n\n";
            return;
        }
        #endregion

        // Stream de bytes com o Conteudo do Arquivo Excel
        Stream sr = e.UploadedFile.FileContent;

        #region Processamento Arquivo Excel

        //Reseta Vetor de Dados
        this.valoresExcelPosicaoCotista = new List<ValoresExcelPosicaoCotista>();
        //
        this.idClienteDeletarPosicaoCotista = new List<int>();
        this.dataHistoricoDeletarPosicaoCotista = new List<DateTime>();

        try {
            // Ler Arquivo
            this.LerArquivoPosicaoCotista(sr);
            // Carrega Arquivo
            this.CarregaPosicaoCotista();
        }
        catch (Exception e2) {
            e.CallbackData = "Importação Posição Cotista - " + e2.Message;
            return;
        }

        new HistoricoLog().InsereHistoricoLog(DateTime.Now, DateTime.Now,
                "Importação Planilha Posição Cotista",
                HttpContext.Current.User.Identity.Name, UtilitarioWeb.GetIP(Request), "", HistoricoLogOrigem.Outros);

        #endregion
    }

    /// <summary>
    /// Dispara processo para leitura de tabela auxiliar de importação de posição cotista
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void callbackImportaTabelaPosicao_Callback(object source, DevExpress.Web.CallbackEventArgs e)
    {
        e.Result = "Ok";

        ImplantacaoPosicaoCotistaCollection implantacaoPosicaoCotistaCollection = new ImplantacaoPosicaoCotistaCollection();

        implantacaoPosicaoCotistaCollection.Query.Where(implantacaoPosicaoCotistaCollection.Query.Status.IsNull());

        implantacaoPosicaoCotistaCollection.LoadAll();

        foreach(ImplantacaoPosicaoCotista implantacaoPosicaoCotista in implantacaoPosicaoCotistaCollection)
        {

            ValoresExcelPosicaoCotista item = new ValoresExcelPosicaoCotista();

            if (!implantacaoPosicaoCotista.IdCotista.HasValue)
                e.Result = "Favor informar Id do cotista - " + implantacaoPosicaoCotista.IdImplantacaoPosicaoCotista;

            if(!implantacaoPosicaoCotista.IdCarteira.HasValue)
                e.Result = "Favor informar Id do carteira - " + implantacaoPosicaoCotista.IdImplantacaoPosicaoCotista;

            if (!implantacaoPosicaoCotista.ValorAplicacao.HasValue)
                e.Result = "Favor informar valor da aplicação - " + implantacaoPosicaoCotista.IdImplantacaoPosicaoCotista;

            if (!implantacaoPosicaoCotista.DataAplicacao.HasValue)
                e.Result = "Favor informar data da aplicação - " + implantacaoPosicaoCotista.IdImplantacaoPosicaoCotista;

            if (!implantacaoPosicaoCotista.DataConversao.HasValue)
                e.Result = "Favor informar data da conversão - " + implantacaoPosicaoCotista.IdImplantacaoPosicaoCotista;

            if (DateTime.Compare(implantacaoPosicaoCotista.DataAplicacao.Value, implantacaoPosicaoCotista.DataConversao.Value) > 0)
            {
                e.Result = "Data aplicação não pode ser superior a data conversão - " + implantacaoPosicaoCotista.IdImplantacaoPosicaoCotista;
            }

            if (!implantacaoPosicaoCotista.Quantidade.HasValue)
                e.Result = "Favor informar quantidade da posição - " + implantacaoPosicaoCotista.IdImplantacaoPosicaoCotista;

            if (!implantacaoPosicaoCotista.CotaAplicacao.HasValue)
                e.Result = "Favor informar cota da aplicação - " + implantacaoPosicaoCotista.IdImplantacaoPosicaoCotista;


            if (e.Result.Equals("Ok"))
            {
                item.IdCotista = implantacaoPosicaoCotista.IdCotista.Value;
                item.IdCarteira = implantacaoPosicaoCotista.IdCarteira.Value;
                item.ValorAplicacao = implantacaoPosicaoCotista.ValorAplicacao.Value;
                item.DataAplicacao = implantacaoPosicaoCotista.DataAplicacao.Value;
                item.DataConversao = implantacaoPosicaoCotista.DataConversao.Value;
                item.Quantidade = implantacaoPosicaoCotista.Quantidade.Value;
                item.CotaAplicacao = implantacaoPosicaoCotista.CotaAplicacao.Value;
                if (implantacaoPosicaoCotista.DataUltimaCobrancaIR.HasValue) item.DataUltimaCobrancaIR = implantacaoPosicaoCotista.DataUltimaCobrancaIR;
                if (implantacaoPosicaoCotista.DataUltimoCortePfee.HasValue) item.DataUltimoCortePfee = implantacaoPosicaoCotista.DataUltimoCortePfee;
                if (implantacaoPosicaoCotista.DataLiquidacao.HasValue) item.DataLiquidacao = implantacaoPosicaoCotista.DataLiquidacao;

                implantacaoPosicaoCotista.Status = "I";
                implantacaoPosicaoCotista.DataImportacao = DateTime.Now;

                this.valoresExcelPosicaoCotista.Add(item);
            }
            else
            {
                return;
            }


        }

        try
        {
            if (this.valoresExcelPosicaoCotista.Count > 0)
            {
                // Carrega Arquivo
                this.CarregaPosicaoCotista();
                implantacaoPosicaoCotistaCollection.Save();

                e.Result = "Tabela importada com sucesso.";
            }
            else
            {
                e.Result = "Não existem posições para importar na tabela.";
            }


        }
        catch (Exception e2)
        {
            e.Result = "Importação Posição Cotista - " + e2.Message;
        }

    }

}