﻿using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Collections.Generic;

using System.IO;
using Financial.Integracao.Excel.Util;
using Financial.Integracao.Excel;
using Financial.Web.Common;

using EntitySpaces.Interfaces;

using DevExpress.Web;
using Financial.Bolsa.Enums;
using Financial.Bolsa;
using Financial.Common;
using Financial.Common.Exceptions;
using Financial.Investidor;
using Financial.Bolsa.Exceptions;
using Financial.Security;
using Financial.Web.Util;
using Financial.Security.Enums;

public partial class ImportacaoBasePage : BasePage {

    /* Estrutura do Excel */
    private List<ValoresExcelPosicaoEmprestimoBolsa> valoresExcelPosicaoEmprestimoBolsa = new List<ValoresExcelPosicaoEmprestimoBolsa>();
    //
    List<int> idClienteDeletarPosicaoEmprestimoBolsa = new List<int>();
    List<DateTime> dataHistoricoDeletarPosicaoEmprestimoBolsa = new List<DateTime>();

    /// <summary>
    /// Leitura de um arquivo Excel
    /// Saida: List<ValoresExcel> com os valores armazenados no arquivo
    /// </summary>
    /// <param name="streamExcel">Stream de bytes de um arquivo Excel</param>
    /// <exception cref="Exception">Se ocorreu problemas</exception>
    private void LerArquivoPosicaoEmprestimoBolsa(Stream streamExcel) {

        #region Leitura do Arquivo Excel

        // Open Spreadsheet
        Bytescout.Spreadsheet.Spreadsheet document = new Bytescout.Spreadsheet.Spreadsheet();
        document.LoadFromStream(streamExcel);

        Bytescout.Spreadsheet.Worksheet workSheet = document.Workbook.Worksheets[0];
        //

        #region Confere Formato Arquivo
        string[] colunasConferencias = new string[] 
                    {"IdCliente","CodigoBovespa","CdAtivoBolsa",
					 "Quantidade","PULiquidoOriginal","ValorBase",
                     "PontaEmprestimo","DataRegistro","DataVencimento",
                     "TaxaOperacao","TaxaComissao","NumeroContrato",
                     "DataInicialDevolucaoAntecipada"
                    };

        bool formato = ValoresExcelUtil.isExcelFormatoValido(colunasConferencias, workSheet);

        if (!formato) {
            string mensagem = "Formato Interno do Arquivo Inválido.\n";
            mensagem += "Linha 1 do Arquivo deve ser: \n";
            for (int i = 0; i < colunasConferencias.Length; i++) {
                int coluna = i + 1;
                mensagem += "\tColuna " + coluna + ": " + colunasConferencias[i] + "\n";
            }

            document.Close();
            document.Dispose();

            throw new Exception(mensagem);
        }
        #endregion

        //
        int index = 0;
        // This row,column index should be changed as per your need.
        // i.e. which cell in the excel you are interesting to read.
        //
        /* Formato: 1)IdCliente    -  2)CodigoBovespa   - 3)CdAtivoBolsa - 4)Quantidade     - 5)PuLiquidoOriginal
         *          6)ValorBase    -  7)PontaEmprestimo - 8)DataRegistro - 9)DataVencimento - 10)TaxaOperacao
         *         11)TaxaComissao -  12)NumeroContrato - 14)DataInicialDevolucaoAntecipada
         */

        int linha = 1;
        int coluna1 = 0, coluna2 = 1, coluna3 = 2,
            coluna4 = 3, coluna5 = 4, coluna6 = 5,
            coluna7 = 6, coluna8 = 7, coluna9 = 8,
            coluna10 = 9, coluna11 = 10, coluna12 = 11,
            coluna13 = 12;

        //
        try {
            // Enquanto idCliente tiver valor
            while (workSheet.Cell(linha, coluna1).Value != null) {
                ValoresExcelPosicaoEmprestimoBolsa item = new ValoresExcelPosicaoEmprestimoBolsa();
                
                try {
                    item.IdCliente = workSheet.Cell(linha, coluna1).ValueAsInteger;
                }
                catch (Exception ex) {
                    throw new Exception(ex.Message + " - IdCliente: linha " + (linha + 1));
                }

                if (workSheet.Cell(linha, coluna2).Value == null)
                    throw new Exception("CodigoBovespa não informado: linha " + (linha + 1));
                try
                {
                    item.CodigoBovespa = workSheet.Cell(linha, coluna2).ValueAsInteger;
                }
                catch (Exception ex) {
                    throw new Exception(ex.Message + " - CodigoBovespa: linha " + (linha + 1));
                }

                if (workSheet.Cell(linha, coluna3).Value == null)
                    throw new Exception("CdAtivoBolsa não informado: linha " + (linha + 1));
                try
                {
                    item.CdAtivoBolsa = workSheet.Cell(linha, coluna3).Value.ToString();
                }
                catch (Exception ex) {
                    throw new Exception(ex.Message + " - CdAtivoBolsa: linha " + (linha + 1));
                }

                if (workSheet.Cell(linha, coluna4).Value == null)
                    throw new Exception("Quantidade não informada: linha " + (linha + 1));
                try
                {
                    item.Quantidade = workSheet.Cell(linha, coluna4).ValueAsInteger;
                }
                catch (Exception ex) {
                    throw new Exception(ex.Message + " - Quantidade: linha " + (linha + 1));
                }

                if (workSheet.Cell(linha, coluna5).Value == null)
                    throw new Exception("PuLiquidoOriginal não informado: linha " + (linha + 1));
                try
                {
                    item.PuLiquidoOriginal = Convert.ToDecimal(workSheet.Cell(linha, coluna5).Value);
                }
                catch (Exception ex) {
                    throw new Exception(ex.Message + " - PuLiquidoOriginal: linha " + (linha + 1));
                }

                if (workSheet.Cell(linha, coluna6).Value == null)
                    throw new Exception("ValorBase não informado: linha " + (linha + 1));
                try
                {
                    item.ValorBase = Convert.ToDecimal(workSheet.Cell(linha, coluna6).Value);
                }
                catch (Exception ex) {
                    throw new Exception(ex.Message + " - ValorBase: linha " + (linha + 1));
                }

                if (workSheet.Cell(linha, coluna7).Value == null)
                    throw new Exception("TipoEmprestimo não informado: linha " + (linha + 1));
                try
                {
                    int pontaEmprestimo = workSheet.Cell(linha, coluna7).ValueAsInteger;
                    item.TipoEmprestimo = (PontaEmprestimoBolsa)pontaEmprestimo;
                }
                catch (Exception ex) {
                    throw new Exception(ex.Message + " - TipoEmprestimo: linha " + (linha + 1));
                }

                if (workSheet.Cell(linha, coluna8).Value == null)
                    throw new Exception("DataRegistro não informada: linha " + (linha + 1));
                try
                {
                    item.DataRegistro = workSheet.Cell(linha, coluna8).ValueAsDateTime;
                }
                catch (Exception ex) {
                    throw new Exception(ex.Message + " - DataRegistro: linha " + (linha + 1));
                }

                if (workSheet.Cell(linha, coluna9).Value == null)
                    throw new Exception("DataVencimento não informada: linha " + (linha + 1));
                try
                {
                    item.DataVencimento = workSheet.Cell(linha, coluna9).ValueAsDateTime;
                }
                catch (Exception ex) {
                    throw new Exception(ex.Message + " - DataVencimento: linha " + (linha + 1));
                }

                if (workSheet.Cell(linha, coluna10).Value == null)
                    throw new Exception("TaxaOperacao não informada: linha " + (linha + 1));
                try
                {
                    item.TaxaOperacao = Convert.ToDecimal(workSheet.Cell(linha, coluna10).Value);
                }
                catch (Exception ex) {
                    throw new Exception(ex.Message + " - TaxaOperacao: linha " + (linha + 1));
                }

                if (workSheet.Cell(linha, coluna11).Value == null)
                    throw new Exception("TaxaComissao não informada: linha " + (linha + 1));
                try
                {
                    item.TaxaComissao = Convert.ToDecimal(workSheet.Cell(linha, coluna11).Value);
                }
                catch (Exception ex) {
                    throw new Exception(ex.Message + " - TaxaComissao: linha " + (linha + 1));
                }

                if (workSheet.Cell(linha, coluna12).Value == null)
                    throw new Exception("NumeroContrato não informado: linha " + (linha + 1));
                try
                {
                    item.NumeroContrato = workSheet.Cell(linha, coluna12).ValueAsInteger;
                }
                catch (Exception ex) {
                    throw new Exception(ex.Message + " - NumeroContrato: linha " + (linha + 1));
                }

                #region DataInicialDevolucaoAntecipada
                if (workSheet.Cell(linha, coluna13).Value != null)
                {
                    try
                    {
                        item.DataInicialDevolucaoAntecipada = workSheet.Cell(linha, coluna13).ValueAsDateTime;
                    }
                    catch (Exception ex)
                    {
                        throw new Exception(ex.Message + " - DataInicialDevolucaoAntecipada: linha " + (linha + 1));
                    }
                }
                else
                {
                    item.DataInicialDevolucaoAntecipada = new DateTime();

                }
                #endregion

                this.valoresExcelPosicaoEmprestimoBolsa.Add(item);
                //
                index++;
                linha = 1 + index;
                //                
                Console.WriteLine("{0},{1},{2},{3},{4},{5},{6},{7},{8},{9},{10},{11},{12}",
                    item.IdCliente, item.CodigoBovespa, item.CdAtivoBolsa, item.Quantidade,
                    item.PuLiquidoOriginal, item.ValorBase, item.TipoEmprestimo, item.DataRegistro,
                    item.DataVencimento, item.TaxaOperacao, item.TaxaComissao, item.NumeroContrato,
                    item.DataInicialDevolucaoAntecipada);
            }
        }
        catch (Exception ex) {
            document.Close();
            document.Dispose();

            throw new Exception(ex.Message);
        }
        #endregion

        document.Close();
        document.Dispose();
    }

    /// <summary>
    /// Gera e Carrega as posições EmprestimoBolsa  e Posições EmprestimoBolsa Abertura 
    /// de acordo com o objeto List<ValoresExcel>
    /// </summary>
    /// <exception cref="Exception">Se ocorreu problemas</exception>
    private void CarregaPosicaoEmprestimoBolsa() {
        PosicaoEmprestimoBolsaCollection posicaoEmprestimoBolsaCollection = new PosicaoEmprestimoBolsaCollection();
        PosicaoEmprestimoBolsaCollection posicaoEmprestimoBolsaDeletarCollection = new PosicaoEmprestimoBolsaCollection();
        //
        PosicaoEmprestimoBolsaAberturaCollection posicaoEmprestimoBolsaAberturaCollection = new PosicaoEmprestimoBolsaAberturaCollection();
        PosicaoEmprestimoBolsaAberturaCollection posicaoEmprestimoBolsaAberturaDeletarCollection = new PosicaoEmprestimoBolsaAberturaCollection();
        //    
        for (int i = 0; i < this.valoresExcelPosicaoEmprestimoBolsa.Count; i++) {
            ValoresExcelPosicaoEmprestimoBolsa valoresExcel = this.valoresExcelPosicaoEmprestimoBolsa[i];
            //
            this.idClienteDeletarPosicaoEmprestimoBolsa.Add(valoresExcel.IdCliente);

            #region TipoEmprestimo Inconsistente
            if ((int)valoresExcel.TipoEmprestimo != 1 && (int)valoresExcel.TipoEmprestimo != 2) {
                throw new Exception("PontaEmprestimo Inconsistente. Valores Possíveis: 1, 2");
            }
            #endregion

            #region Testa AgenteMercado
            AgenteMercado agenteMercado = new AgenteMercado();
            int? idAgenteMercado = null;
            try {
                idAgenteMercado = (int)agenteMercado.BuscaIdAgenteMercadoBovespa(valoresExcel.CodigoBovespa);
            }
            catch (IdAgenteNaoCadastradoException e) {
                throw new IdAgenteNaoCadastradoException(e.Message);
            }
            #endregion

            #region Verifica se Existe Cliente
            Cliente cliente = new Cliente();
            List<esQueryItem> campos = new List<esQueryItem>();
            campos.Add(cliente.Query.IdCliente);
            campos.Add(cliente.Query.DataDia);
            if (!cliente.LoadByPrimaryKey(campos, valoresExcel.IdCliente)) {
                throw new Exception("Cliente Não existente : " + valoresExcel.IdCliente);
            }
            // Salva a Data Historico para poder Deletar
            this.dataHistoricoDeletarPosicaoEmprestimoBolsa.Add(cliente.DataDia.Value);
            //
            #endregion

            #region PosicaoEmprestimoBolsa
            // Dados do Arquivo
            PosicaoEmprestimoBolsa posicao = new PosicaoEmprestimoBolsa();
            posicao.IdCliente = valoresExcel.IdCliente;
            posicao.IdAgente = idAgenteMercado.Value;
            posicao.CdAtivoBolsa = valoresExcel.CdAtivoBolsa.ToUpper().Trim();
            posicao.Quantidade = valoresExcel.Quantidade;
            posicao.PULiquidoOriginal = valoresExcel.PuLiquidoOriginal;
            posicao.ValorBase = valoresExcel.ValorBase;
            posicao.PontaEmprestimo = (byte)valoresExcel.TipoEmprestimo;
            posicao.DataRegistro = valoresExcel.DataRegistro;
            posicao.DataVencimento = valoresExcel.DataVencimento;
            posicao.TaxaOperacao = valoresExcel.TaxaOperacao;
            posicao.TaxaComissao = valoresExcel.TaxaComissao;
            posicao.NumeroContrato = valoresExcel.NumeroContrato;
            posicao.IdOperacao = null;
            posicao.TipoEmprestimo = (byte)valoresExcel.TipoEmprestimo;

            if (valoresExcel.DataInicialDevolucaoAntecipada != new DateTime())
            {
                if (DateTime.Compare(valoresExcel.DataRegistro, valoresExcel.DataInicialDevolucaoAntecipada) > 0)                
                    throw new Exception("Data Ínicial deve ser maior ou ígual a data de registro! Operação não pode ser realizada.");                

                posicao.DataInicialDevolucaoAntecipada = valoresExcel.DataInicialDevolucaoAntecipada;
                posicao.PermiteDevolucaoAntecipada = "S";
            }
            else
            {
                posicao.DataInicialDevolucaoAntecipada = null;
                posicao.PermiteDevolucaoAntecipada = "N";
            }

            #region PUMercado/ValorMercado
            CotacaoBolsa cotacaoBolsa = new CotacaoBolsa();
            if (!cotacaoBolsa.LoadByPrimaryKey(cliente.DataDia.Value, valoresExcel.CdAtivoBolsa.ToUpper())) {
                throw new CotacaoNaoCadastradaException("CotacaoBolsa Não existente : " + cliente.DataDia.Value.ToString("dd/MM/yyyy") + ", " + valoresExcel.CdAtivoBolsa.ToUpper());
            }
            // Seta PuMercado
            posicao.PUMercado = cotacaoBolsa.PUFechamento;

            #region Seta ValorMercado
            FatorCotacaoBolsa fatorCotacaoBolsa = new FatorCotacaoBolsa();
            if (!fatorCotacaoBolsa.BuscaFatorCotacaoBolsa(valoresExcel.CdAtivoBolsa.ToUpper(), cliente.DataDia.Value)) {
                throw new FatorCotacaoNaoCadastradoException("FatorCotacaoBolsa Não existente : " + cliente.DataDia.Value.ToString("d") + ", " + valoresExcel.CdAtivoBolsa.ToUpper());
            }
            posicao.ValorMercado = ((valoresExcel.Quantidade * posicao.PUMercado) / fatorCotacaoBolsa.Fator.Value);
            //
            #endregion
            #endregion
            //               
            posicao.ValorCorrigidoJuros = 0;
            posicao.ValorCorrigidoComissao = 0;
            posicao.ValorCorrigidoCBLC = 0;

            // Attach the object
            posicaoEmprestimoBolsaCollection.AttachEntity(posicao);
            #endregion

            #region PosicaoEmprestimoBolsaAbertura
            //
            PosicaoEmprestimoBolsaAbertura posicaoAbertura = new PosicaoEmprestimoBolsaAbertura();
            posicaoAbertura.IdCliente = posicao.IdCliente;
            posicaoAbertura.IdAgente = posicao.IdAgente;
            posicaoAbertura.DataHistorico = cliente.DataDia.Value;
            posicaoAbertura.CdAtivoBolsa = posicao.CdAtivoBolsa;
            posicaoAbertura.Quantidade = posicao.Quantidade;
            posicaoAbertura.PULiquidoOriginal = posicao.PULiquidoOriginal;
            posicaoAbertura.ValorBase = posicao.ValorBase;
            posicaoAbertura.PontaEmprestimo = posicao.PontaEmprestimo;
            posicaoAbertura.DataRegistro = posicao.DataRegistro;
            posicaoAbertura.DataVencimento = posicao.DataVencimento;
            posicaoAbertura.TaxaOperacao = posicao.TaxaOperacao;
            posicaoAbertura.TaxaComissao = posicao.TaxaComissao;
            posicaoAbertura.NumeroContrato = posicao.NumeroContrato;
            posicaoAbertura.IdOperacao = posicao.IdOperacao;
            posicaoAbertura.TipoEmprestimo = posicao.TipoEmprestimo;
            posicaoAbertura.PUMercado = posicao.PUMercado;
            posicaoAbertura.ValorMercado = posicao.ValorMercado;
            posicaoAbertura.ValorCorrigidoJuros = posicao.ValorCorrigidoJuros;
            posicaoAbertura.ValorCorrigidoComissao = posicao.ValorCorrigidoComissao;
            posicaoAbertura.ValorCorrigidoCBLC = posicao.ValorCorrigidoCBLC;
            posicaoAbertura.PermiteDevolucaoAntecipada = posicao.PermiteDevolucaoAntecipada;
            posicaoAbertura.DataInicialDevolucaoAntecipada = posicao.DataInicialDevolucaoAntecipada;

            // Attach the object
            posicaoEmprestimoBolsaAberturaCollection.AttachEntity(posicaoAbertura);
            #endregion
        }

        using (esTransactionScope scope = new esTransactionScope()) {
            #region Deleta PosicaoEmprestimoBolsa por IdCliente
            posicaoEmprestimoBolsaDeletarCollection.Query.Where(posicaoEmprestimoBolsaDeletarCollection.Query.IdCliente.In(this.idClienteDeletarPosicaoEmprestimoBolsa));
            posicaoEmprestimoBolsaDeletarCollection.Query.Load();
            posicaoEmprestimoBolsaDeletarCollection.MarkAllAsDeleted();
            posicaoEmprestimoBolsaDeletarCollection.Save();
            #endregion

            #region Deleta PosicaoEmprestimoBolsaAbertura por IdCliente/DataHistorico
            for (int i = 0; i < this.idClienteDeletarPosicaoEmprestimoBolsa.Count; i++) {
                posicaoEmprestimoBolsaAberturaDeletarCollection = new PosicaoEmprestimoBolsaAberturaCollection();
                posicaoEmprestimoBolsaAberturaDeletarCollection.Query
                .Where(posicaoEmprestimoBolsaAberturaDeletarCollection.Query.IdCliente == this.idClienteDeletarPosicaoEmprestimoBolsa[i],
                       posicaoEmprestimoBolsaAberturaDeletarCollection.Query.DataHistorico == this.dataHistoricoDeletarPosicaoEmprestimoBolsa[i]);
                posicaoEmprestimoBolsaAberturaDeletarCollection.Query.Load();
                posicaoEmprestimoBolsaAberturaDeletarCollection.MarkAllAsDeleted();
                posicaoEmprestimoBolsaAberturaDeletarCollection.Save();
            }
            #endregion

            // Salva Posicões presentes no Excel                                
            posicaoEmprestimoBolsaCollection.Save();

            // Seta os novos idPosicao na PosicaoAbertura
            for (int i = 0; i < posicaoEmprestimoBolsaCollection.Count; i++) {
                posicaoEmprestimoBolsaAberturaCollection[i].IdPosicao = posicaoEmprestimoBolsaCollection[i].IdPosicao;
            }
            posicaoEmprestimoBolsaAberturaCollection.Save();
            scope.Complete();
        }
    }

    /// <summary>
    /// Processa a Planilha de PosicaoEmprestimoBolsa após ter acabado o Upload
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void uplPosicaoEmprestimoBolsa_FileUploadComplete(object sender, FileUploadCompleteEventArgs e) {
        e.CallbackData = "";

        #region Trata Extensão Válida
        if (!ValoresExcelUtil.isExtensaoExcel(e.UploadedFile.FileName.Trim())) {
            e.CallbackData = "Importação Posição Empréstimo Bolsa - Formato de Arquivo Inválido. Formato permitido: .xls ou .xlsx\n\n";
            return;
        }
        #endregion

        // Stream de bytes com o Conteudo do Arquivo Excel
        Stream sr = e.UploadedFile.FileContent;

        #region Processamento Arquivo Excel

        //Reseta Vetor de Dados
        this.valoresExcelPosicaoEmprestimoBolsa = new List<ValoresExcelPosicaoEmprestimoBolsa>();
        //
        this.idClienteDeletarPosicaoEmprestimoBolsa = new List<int>();
        this.dataHistoricoDeletarPosicaoEmprestimoBolsa = new List<DateTime>();

        try {
            // Ler Arquivo
            this.LerArquivoPosicaoEmprestimoBolsa(sr);
            // Carrega Arquivo
            this.CarregaPosicaoEmprestimoBolsa();
        }
        catch (Exception e2) {
            e.CallbackData = "Importação Posição Empréstimo Bolsa - " + e2.Message;
            return;
        }

        new HistoricoLog().InsereHistoricoLog(DateTime.Now, DateTime.Now,
                "Importação Planilha Posição Empréstimo Bolsa",
                HttpContext.Current.User.Identity.Name, UtilitarioWeb.GetIP(Request), "", HistoricoLogOrigem.Outros);

        #endregion
    }
}