﻿using System;
using System.Collections.Generic;
using System.IO;

using Financial.Integracao.Excel;
using Financial.Integracao.Excel.Util;
using Financial.Web.Common;
using Financial.Bolsa;

using EntitySpaces.Interfaces;

using DevExpress.Web;
using Financial.Common.Enums;
using Financial.Fundo;
using Financial.Bolsa.Enums;
using Financial.Security;
using System.Web;
using Financial.Web.Util;
using Financial.Security.Enums;
using Financial.Investidor;
using Financial.Investidor.Enums;

public partial class ImportacaoBasePage : BasePage {

    #region Estrutura TabelaInformeDesempenho

    public enum Tipo { 
        Clube = 1,
        Fundo = 2
    }
   
    /// <summary>
    /// Armazena os Valores de TabelaInformeDesempenho presentes num arquivo Excel
    /// </summary>
    internal class ValoresExcelTabelaInformeDesempenho {
        private int? idCarteira;
        public int? IdCarteira {
            get { return idCarteira; }
            set { idCarteira = value; }
        }

        private DateTime? data;
        public DateTime? Data {
            get { return data; }
            set { data = value; }
        }

        private int? tipoCarteira; // Fundo ou Clube
        public int? TipoCarteira {
            get { return tipoCarteira; }
            set { tipoCarteira = value; }       
        }

        private decimal? valorAdministracaoPagaAdministrador = null;
        public decimal? ValorAdministracaoPagaAdministrador {
            get { return valorAdministracaoPagaAdministrador; }
            set { valorAdministracaoPagaAdministrador = value; }
        }

        private decimal? valorPFeePagaAdministrador = null;
        public decimal? ValorPFeePagaAdministrador {
            get { return valorPFeePagaAdministrador; }
            set { valorPFeePagaAdministrador = value; }
        }

        private decimal? valorCustodiaPagaAdministrador = null;
        public decimal? ValorCustodiaPagaAdministrador {
            get { return valorCustodiaPagaAdministrador; }
            set { valorCustodiaPagaAdministrador = value; }
        }

        private decimal? valorOutrasDespPagasAdministrador = null;
        public decimal? ValorOutrasDespPagasAdministrador {
            get { return valorOutrasDespPagasAdministrador; }
            set { valorOutrasDespPagasAdministrador = value; }
        }

        private decimal? valorAdministracaoPagaGrupoEconomicoADM = null;
        public decimal? ValorAdministracaoPagaGrupoEconomicoADM {
            get { return valorAdministracaoPagaGrupoEconomicoADM; }
            set { valorAdministracaoPagaGrupoEconomicoADM = value; }
        }

        private decimal? valorDespesasOperacionaisServicosGrupoADM = null;
        public decimal? ValorDespesasOperacionaisServicosGrupoADM {
            get { return valorDespesasOperacionaisServicosGrupoADM; }
            set { valorDespesasOperacionaisServicosGrupoADM = value; }
        }

        private decimal? valorAdministracaoPagaGrupoGestor = null;
        public decimal? ValorAdministracaoPagaGrupoGestor {
            get { return valorAdministracaoPagaGrupoGestor; }
            set { valorAdministracaoPagaGrupoGestor = value; }
        }

        private decimal? valorPFeePagaGrupoGestor = null;
        public decimal? ValorPFeePagaGrupoGestor {
            get { return valorPFeePagaGrupoGestor; }
            set { valorPFeePagaGrupoGestor = value; }
        }

        private decimal? valorDespesasOperacionaisServiçosGrupoGestor = null;
        public decimal? ValorDespesasOperacionaisServicosGrupoGestor {
            get { return valorDespesasOperacionaisServiçosGrupoGestor; }
            set { valorDespesasOperacionaisServiçosGrupoGestor = value; }
        }        
    }
    #endregion

    /* Estrutura do Excel */
    private List<ValoresExcelTabelaInformeDesempenho> valoresExcelTabelaInformeDesempenho = new List<ValoresExcelTabelaInformeDesempenho>();

    /// <summary>
    /// Leitura de um arquivo Excel
    /// Saida: List<ValoresExcel> com os valores armazenados no arquivo
    /// </summary>
    /// <param name="streamExcel">Stream de bytes de um arquivo Excel</param>
    /// <exception cref="Exception">Se ocorreu problemas</exception>
    private void LerArquivoTabelaInformeDesempenho(Stream streamExcel) {

        #region Leitura do Arquivo Excel

        // Open Spreadsheet
        Bytescout.Spreadsheet.Spreadsheet document = new Bytescout.Spreadsheet.Spreadsheet();
        document.LoadFromStream(streamExcel);

        Bytescout.Spreadsheet.Worksheet workSheet = document.Workbook.Worksheets[0];

        //
        #region Confere Formato Arquivo

        string[] colunasConferencias = new string[] { 
            "Data",
            "IdCarteira",
            "Fundo ou Clube",
            "Valor Administração Paga (Administrador)",
            "Valor PFee Paga (Administrador)",
            "Valor Custódia Paga (Administrador)",
            "Valor Outras Desp. Pagas (Administrador)",
            "Valor Administração Paga (Grupo Econômico ADM)",
            "Valor Despesas operacionais e de serviços (Grupo Econômico ADM)",
            "Valor Administração Paga (Grupo Econômico Gestor)",
            "Valor PFee Paga (Grupo Econômico Gestor)",
            "Valor Despesas operacionais e de serviços (Grupo Econômico Gestor)"
        };

        bool formato = ValoresExcelUtil.isExcelFormatoValido(colunasConferencias, workSheet);

        if (!formato) {
            string mensagem = "Formato Interno do Arquivo Inválido.\n";
            mensagem += "Linha 1 do Arquivo deve ser: \n";
            for (int i = 0; i < colunasConferencias.Length; i++) {
                int coluna = i + 1;
                mensagem += "\tColuna " + coluna + ": " + colunasConferencias[i] + "\n";
            }

            document.Close();
            document.Dispose();

            throw new Exception(mensagem);
        }
        #endregion

        //
        int index = 0;
        // This row,column index should be changed as per your need.
        // i.e. which cell in the excel you are interesting to read.
        //
        /* Formato: 
         *          
         *          
         *          
         *          
         */
        int linha = 1;
        int coluna1 = 0, coluna2 = 1, coluna3 = 2;
        int coluna4 = 3, coluna5 = 4, coluna6 = 5;
        int coluna7 = 6, coluna8 = 7, coluna9 = 8;
        int coluna10 = 9, coluna11 = 10, coluna12 = 11;
        //            
        try {
            // Enquanto Data tiver valor
            while (workSheet.Cell(linha, coluna1).Value != null) {
                ValoresExcelTabelaInformeDesempenho item = new ValoresExcelTabelaInformeDesempenho();

                if (workSheet.Cell(linha, coluna1).Value != null) {
                    item.Data = workSheet.Cell(linha, coluna1).ValueAsDateTime;
                }
                if (workSheet.Cell(linha, coluna2).Value != null) {
                    item.IdCarteira = workSheet.Cell(linha, coluna2).ValueAsInteger;
                }
                if (workSheet.Cell(linha, coluna3).Value != null) {
                    item.TipoCarteira = workSheet.Cell(linha, coluna3).ValueAsInteger;
                }
                if (workSheet.Cell(linha, coluna4).Value != null) {
                    item.ValorAdministracaoPagaAdministrador = Convert.ToDecimal(workSheet.Cell(linha, coluna4).Value);
                }
                if (workSheet.Cell(linha, coluna5).Value != null) {
                    item.ValorPFeePagaAdministrador = Convert.ToDecimal(workSheet.Cell(linha, coluna5).Value);
                }
                if (workSheet.Cell(linha, coluna6).Value != null) {
                    item.ValorCustodiaPagaAdministrador = Convert.ToDecimal(workSheet.Cell(linha, coluna6).Value);
                }
                if (workSheet.Cell(linha, coluna7).Value != null) {
                    item.ValorOutrasDespPagasAdministrador = Convert.ToDecimal(workSheet.Cell(linha, coluna7).Value);
                }
                if (workSheet.Cell(linha, coluna8).Value != null) {
                    item.ValorAdministracaoPagaGrupoEconomicoADM = Convert.ToDecimal(workSheet.Cell(linha, coluna8).Value);
                }
                if (workSheet.Cell(linha, coluna9).Value != null) {
                    item.ValorDespesasOperacionaisServicosGrupoADM = Convert.ToDecimal(workSheet.Cell(linha, coluna9).Value);
                }
                if (workSheet.Cell(linha, coluna10).Value != null) {
                    item.ValorAdministracaoPagaGrupoGestor = Convert.ToDecimal(workSheet.Cell(linha, coluna10).Value);
                }
                if (workSheet.Cell(linha, coluna11).Value != null) {
                    item.ValorPFeePagaGrupoGestor = Convert.ToDecimal(workSheet.Cell(linha, coluna11).Value);
                }
                if (workSheet.Cell(linha, coluna12).Value != null) {
                    item.ValorDespesasOperacionaisServicosGrupoGestor = Convert.ToDecimal(workSheet.Cell(linha, coluna12).Value);
                }

                this.valoresExcelTabelaInformeDesempenho.Add(item);
                //
                index++;
                linha = 1 + index;
                //                
                //Console.WriteLine("{0},{1},{2},{3},{4},{5},{6},{7}", 
                //            item.CdAtivoBolsa, item.Especificao,
                //            item.Descricao, item.TipoMercado,
                //            item.PuExercicio, item.DataVencimento,
                //            item.CodigoAtivoObjeto, item.CodigoIsin);
            }
        }
        catch (Exception ex) {
            document.Close();
            document.Dispose();

            throw new Exception(ex.Message);
        }
        #endregion

        document.Close();
        document.Dispose();
    }

    /// <summary>
    /// Gera e Carrega a TabelaPerfil de acordo com o objeto List<ValoresExcel>
    /// </summary>
    private void CarregaTabelaInformeDesempenho() {
        TabelaInformeDesempenhoCollection infoCollection = new TabelaInformeDesempenhoCollection();        
        //    
        for (int i = 0; i < this.valoresExcelTabelaInformeDesempenho.Count; i++) {
            ValoresExcelTabelaInformeDesempenho valoresExcel = this.valoresExcelTabelaInformeDesempenho[i];
            //
            int j = i + 2;

            #region Confere Null
            if (!valoresExcel.Data.HasValue) {
                throw new Exception("Data não pode ser nulo. " + "Linha: " + j);
            }
            else if (!valoresExcel.IdCarteira.HasValue) {
                throw new Exception("IdCarteira não pode ser nulo. " + "Linha: " + j);
            }
            else if (!valoresExcel.TipoCarteira.HasValue) {
                throw new Exception("Fundo ou Clube não pode ser nulo. " + "Linha: " + j);
            }
            
            Tipo tipoEnum = (Tipo)valoresExcel.TipoCarteira.Value;

            #region Verifica se Existe Carteira
            Carteira carteira = new Carteira();
            if (!carteira.LoadByPrimaryKey(valoresExcel.IdCarteira.Value)) {
                throw new Exception("Carteira Não existente : " + valoresExcel.IdCarteira.Value);
            }
            #endregion

            Cliente cliente = new Cliente();
            cliente.LoadByPrimaryKey(valoresExcel.IdCarteira.Value);

            if (tipoEnum == Tipo.Clube) {
                #region Validacoes Clube

                if (cliente.IdTipo.Value != TipoClienteFixo.Clube) {
                    throw new Exception("Carteira " + valoresExcel.IdCarteira.Value + " deve ser um Clube. " + "Linha: " + j);
                }

                if (!valoresExcel.ValorAdministracaoPagaAdministrador.HasValue) {
                    throw new Exception("Valor Administração Paga (Administrador) não pode ser nulo. " + "Linha: " + j);
                }
                else if (!valoresExcel.ValorPFeePagaAdministrador.HasValue) {
                    throw new Exception("Valor PFee Paga (Administrador) não pode ser nulo. " + "Linha: " + j);
                }
                else if (!valoresExcel.ValorOutrasDespPagasAdministrador.HasValue) {
                    throw new Exception("Valor Outras Desp. Pagas (Administrador) não pode ser nulo. " + "Linha: " + j);
                }
                else if (!valoresExcel.ValorAdministracaoPagaGrupoGestor.HasValue) {
                    throw new Exception("Valor Administração Paga (Grupo Econômico Gestor) não pode ser nulo. " + "Linha: " + j);
                }
                else if (!valoresExcel.ValorPFeePagaGrupoGestor.HasValue) {
                    throw new Exception("Valor PFee Paga (Grupo Econômico Gestor) não pode ser nulo. " + "Linha: " + j);
                }
                else if (!valoresExcel.ValorDespesasOperacionaisServicosGrupoGestor.HasValue) {
                    throw new Exception("Valor Despesas operacionais e de serviços (Grupo Econômico Gestor) não pode ser nulo. " + "Linha: " + j);
                }                                      
                #endregion
            }
            else if (tipoEnum == Tipo.Fundo) {
                #region Validacoes Fundo

                if (cliente.IdTipo.Value != TipoClienteFixo.Fundo) {
                    throw new Exception("Carteira " + valoresExcel.IdCarteira.Value + " deve ser um Fundo. " + "Linha: " + j);
                }
                
                if (!valoresExcel.ValorAdministracaoPagaAdministrador.HasValue) {
                    throw new Exception("Valor Administração Paga (Administrador) não pode ser nulo. " + "Linha: " + j);
                }
                else if (!valoresExcel.ValorPFeePagaAdministrador.HasValue) {
                    throw new Exception("Valor PFee Paga (Administrador) não pode ser nulo. " + "Linha: " + j);
                }
                else if (!valoresExcel.ValorCustodiaPagaAdministrador.HasValue) {
                    throw new Exception("Valor Custódia Paga (Administrador) não pode ser nulo. " + "Linha: " + j);
                }
                else if (!valoresExcel.ValorOutrasDespPagasAdministrador.HasValue) {
                    throw new Exception("Valor Outras Desp. Pagas (Administrador) não pode ser nulo. " + "Linha: " + j);
                }
                else if (!valoresExcel.ValorAdministracaoPagaGrupoEconomicoADM.HasValue) {
                    throw new Exception("Valor Administração Paga (Grupo Econômico ADM) não pode ser nulo. " + "Linha: " + j);
                }
                else if (!valoresExcel.ValorDespesasOperacionaisServicosGrupoADM.HasValue) {
                    throw new Exception("Valor Despesas operacionais e de serviços (Grupo Econômico ADM) não pode ser nulo. " + "Linha: " + j);
                }
                else if (!valoresExcel.ValorAdministracaoPagaGrupoGestor.HasValue) {
                    throw new Exception("Valor Administração Paga (Grupo Econômico Gestor) não pode ser nulo. " + "Linha: " + j);
                }
                else if (!valoresExcel.ValorDespesasOperacionaisServicosGrupoGestor.HasValue) {
                    throw new Exception("Valor Despesas operacionais e de serviços (Grupo Econômico Gestor) não pode ser nulo. " + "Linha: " + j);
                }
                #endregion
            }
            #endregion

            #region Verifica se já existe TabelaInforme com o mesmo IdCarteira/Data
            TabelaInformeDesempenho info = new TabelaInformeDesempenho();

            // Se não existe é feito um insert, caso contrario é feito um update
            if (!info.LoadByPrimaryKey(valoresExcel.IdCarteira.Value, valoresExcel.Data.Value)) {
                info = new TabelaInformeDesempenho();
            }

            #region Update/Insert
            // Dados do Arquivo
            info.Data = valoresExcel.Data.Value;
            info.IdCarteira = valoresExcel.IdCarteira.Value;
                        
            if (tipoEnum == Tipo.Clube) {
                info.ValorAdmAdministrador = valoresExcel.ValorAdministracaoPagaAdministrador.Value;
                info.ValorPfeeAdministrador = valoresExcel.ValorPFeePagaAdministrador.Value;
                info.ValorOutrasAdministrador = valoresExcel.ValorOutrasDespPagasAdministrador.Value;
                info.ValorAdmGestor = valoresExcel.ValorAdministracaoPagaGrupoGestor.Value;
                info.ValorPfeeGestor = valoresExcel.ValorPFeePagaGrupoGestor.Value;
                info.ValorOutrasGestor = valoresExcel.ValorDespesasOperacionaisServicosGrupoGestor.Value;

                info.ValorAdministracaoPaga = 0;
                info.ValorPfeePaga = 0;
                info.ValorCustodiaPaga = 0;
                info.ValorOutrasDespesasPagas = 0;
                info.ValorAdministracaoPagaGrupoEconomicoADM = 0;
                info.ValorDespesasOperacionaisGrupoEconomicoADM = 0;
                info.ValorAdministracaoPagaGrupoEconomicoGestor = 0;
                info.ValorDespesasOperacionaisGrupoEconomicoGestor = 0;

            }
            if (tipoEnum == Tipo.Fundo) {
                info.ValorAdministracaoPaga = valoresExcel.ValorAdministracaoPagaAdministrador.Value;
                info.ValorPfeePaga = valoresExcel.ValorPFeePagaAdministrador.Value;
                info.ValorCustodiaPaga = valoresExcel.ValorCustodiaPagaAdministrador.Value;
                info.ValorOutrasDespesasPagas = valoresExcel.ValorOutrasDespPagasAdministrador.Value;
                info.ValorAdministracaoPagaGrupoEconomicoADM = valoresExcel.ValorAdministracaoPagaGrupoEconomicoADM.Value;
                info.ValorDespesasOperacionaisGrupoEconomicoADM = valoresExcel.ValorDespesasOperacionaisServicosGrupoADM.Value;
                info.ValorAdministracaoPagaGrupoEconomicoGestor = valoresExcel.ValorAdministracaoPagaGrupoGestor.Value;
                info.ValorDespesasOperacionaisGrupoEconomicoGestor = valoresExcel.ValorDespesasOperacionaisServicosGrupoGestor.Value;
                //
                info.ValorAdmAdministrador = 0;
                info.ValorPfeeAdministrador = 0;
                info.ValorOutrasAdministrador = 0;
                info.ValorAdmGestor = 0;
                info.ValorPfeeGestor = 0;
                info.ValorOutrasGestor = 0;
            }                       
            // Attach the object
            infoCollection.AttachEntity(info);

            #endregion
            #endregion           
        }

        // Salva Info Complementar presentes no Excel                
        infoCollection.Save();
    }

    /// <summary>
    /// Processa a Planilha de Info Complementar após ter acabado o Upload
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void uplInformeDesempenho_FileUploadComplete(object sender, FileUploadCompleteEventArgs e) {
        e.CallbackData = "";

        #region Trata Extensão Válida
        if (!ValoresExcelUtil.isExtensaoExcel(e.UploadedFile.FileName.Trim())) {
            e.CallbackData = "Tabela Informe Desempenho - Formato de Arquivo Inválido. Formato permitido: .xls ou .xlsx\n\n";
            return;
        }
        #endregion

        // Stream de bytes com o Conteudo do Arquivo Excel
        Stream sr = e.UploadedFile.FileContent;

        #region Processamento Arquivo Excel

        // Reseta Vetor de Dados
        this.valoresExcelTabelaInformeDesempenho = new List<ValoresExcelTabelaInformeDesempenho>();

        //
        try {
            // Ler Arquivo
            this.LerArquivoTabelaInformeDesempenho(sr);
            // Carrega Arquivo
            this.CarregaTabelaInformeDesempenho();
        }
        catch (Exception e2) {
            e.CallbackData = "Importação Tabela Informe Desempenho - " + e2.Message;
            return;
        }

        new HistoricoLog().InsereHistoricoLog(DateTime.Now, DateTime.Now,
                                "Importação Planilha Tabela Informe Desempenho",
                                HttpContext.Current.User.Identity.Name, UtilitarioWeb.GetIP(Request), "", HistoricoLogOrigem.Outros);

        #endregion
    }
}