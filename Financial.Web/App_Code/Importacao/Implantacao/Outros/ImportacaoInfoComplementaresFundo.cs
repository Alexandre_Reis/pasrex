﻿using System;
using System.Collections.Generic;
using System.IO;

using Financial.Integracao.Excel;
using Financial.Integracao.Excel.Util;
using Financial.Web.Common;
using Financial.Bolsa;

using EntitySpaces.Interfaces;

using DevExpress.Web;
using Financial.Common.Enums;
using Financial.Fundo;
using Financial.Bolsa.Enums;
using Financial.Security;
using System.Web;
using Financial.Web.Util;
using Financial.Security.Enums;

public partial class ImportacaoBasePage : BasePage {

    #region Estrutura Informacoes Complementares Fundo

    /// <summary>
    /// Armazena os Valores de Informacoes Complementares Fundo presentes num arquivo Excel
    /// </summary>
    internal class ValoresExcelInfoComplementarFundo {
        private int? idCarteira;
        public int? IdCarteira {
            get { return idCarteira; }
            set { idCarteira = value; }
        }

        private DateTime? dataInicio;
        public DateTime? DataInicio {
            get { return dataInicio; }
            set { dataInicio = value; }
        }

        private string periodicidade = null;
        public string Periodicidade {
            get { return periodicidade; }
            set { periodicidade = value; }
        }

        private string localFormaDivulgacao = null;
        public string LocalFormaDivulgacao {
            get { return localFormaDivulgacao; }
            set { localFormaDivulgacao = value; }
        }

        private string localFormaSolicitacaoCotista = null;
        public string LocalFormaSolicitacaoCotista {
            get { return localFormaSolicitacaoCotista; }
            set { localFormaSolicitacaoCotista = value; }
        }

        private string fatoresRisco = null;
        public string FatoresRisco {
            get { return fatoresRisco; }
            set { fatoresRisco = value; }
        }

        private string politicaExercicioVoto = null;
        public string PoliticaExercicioVoto {
            get { return politicaExercicioVoto; }
            set { politicaExercicioVoto = value; }
        }

        private string tributacaoAplicavel = null;
        public string TributacaoAplicavel {
            get { return tributacaoAplicavel; }
            set { tributacaoAplicavel = value; }
        }

        private string politicaAdministracaoRisco = null;
        public string PoliticaAdministracaoRisco {
            get { return politicaAdministracaoRisco; }
            set { politicaAdministracaoRisco = value; }
        }

        private string agenciaClassificacaoRisco = null;
        public string AgenciaClassificacaoRisco {
            get { return agenciaClassificacaoRisco; }
            set { agenciaClassificacaoRisco = value; }
        }

        private string recursosServicosGestor = null;
        public string RecursosServicosGestor {
            get { return recursosServicosGestor; }
            set { recursosServicosGestor = value; }
        }

        private string prestadoresServicos = null;
        public string PrestadoresServicos {
            get { return prestadoresServicos; }
            set { prestadoresServicos = value; }
        }

        private string politicaDistribuicaoCotas = null;
        public string PoliticaDistribuicaoCotas {
            get { return politicaDistribuicaoCotas; }
            set { politicaDistribuicaoCotas = value; }
        }

        private string observacoes = null;
        public string Observacoes {
            get { return observacoes; }
            set { observacoes = value; }
        }
    }
    #endregion

    /* Estrutura do Excel */
    private List<ValoresExcelInfoComplementarFundo> valoresExcelInfoComplementarFundo = new List<ValoresExcelInfoComplementarFundo>();
    
    /// <summary>
    /// Leitura de um arquivo Excel
    /// Saida: List<ValoresExcel> com os valores armazenados no arquivo
    /// </summary>
    /// <param name="streamExcel">Stream de bytes de um arquivo Excel</param>
    /// <exception cref="Exception">Se ocorreu problemas</exception>
    private void LerArquivoInfoComplementarFundo(Stream streamExcel) {

        #region Leitura do Arquivo Excel

        // Open Spreadsheet
        Bytescout.Spreadsheet.Spreadsheet document = new Bytescout.Spreadsheet.Spreadsheet();
        document.LoadFromStream(streamExcel);

        Bytescout.Spreadsheet.Worksheet workSheet = document.Workbook.Worksheets[0];

        //
        #region Confere Formato Arquivo

        string[] colunasConferencias = new string[] { "IdCarteira", "DataInicio", 
                                                      "Periodicidade", "LocalFormaDivulgacao",   "LocalFormaSolicitacaoCotista",
                                                      "FatoresRisco",  "PoliticaExercicioVoto",   "TributacaoAplicavel", 	
                                                      "PoliticaAdministracaoRisco",  "AgenciaClassificacaoRisco",   "RecursosServicosGestor", 	
                                                      "PrestadoresServicos","PoliticaDistribuicaoCotas", "Observacoes"
                                                    };

        bool formato = ValoresExcelUtil.isExcelFormatoValido(colunasConferencias, workSheet);

        if (!formato) {
            string mensagem = "Formato Interno do Arquivo Inválido.\n";
            mensagem += "Linha 1 do Arquivo deve ser: \n";
            for (int i = 0; i < colunasConferencias.Length; i++) {
                int coluna = i + 1;
                mensagem += "\tColuna " + coluna + ": " + colunasConferencias[i] + "\n";
            }

            document.Close();
            document.Dispose();

            throw new Exception(mensagem);
        }
        #endregion

        //
        int index = 0;
        // This row,column index should be changed as per your need.
        // i.e. which cell in the excel you are interesting to read.
        //
        /* Formato: 1)  idCarteira                  - 2)  DataInicio             
         *          3)  Periodicidade               - 4)  LocalFormaDivulgacao        - 5)  LocalFormaSolicitacaoCotista      
         *          6)  FatoresRisco                - 7)  PoliticaExercicioVoto       - 8)  TributacaoAplicavel
         *          9)  PoliticaAdministracaoRisco  - 10) AgenciaClassificacaoRisco   - 11) RecursosServicosGestor
         *          12) PrestadoresServicos         - 13) PoliticaDistribuicaoCotas   - 14) Observacoes
         */

        int linha = 1;
        int coluna1 = 0, coluna2 = 1, coluna3 = 2;
        int coluna4 = 3, coluna5 = 4, coluna6 = 5;
        int coluna7 = 6, coluna8 = 7, coluna9 = 8;
        int coluna10 = 9, coluna11 = 10, coluna12 = 11;
        int coluna13 = 12, coluna14 = 13;

        //            
        try {
            // Enquanto Data tiver valor
            while (workSheet.Cell(linha, coluna1).Value != null) {
                ValoresExcelInfoComplementarFundo item = new ValoresExcelInfoComplementarFundo();

                if (workSheet.Cell(linha, coluna1).Value != null) {
                    item.IdCarteira = workSheet.Cell(linha, coluna1).ValueAsInteger;
                }
                if (workSheet.Cell(linha, coluna2).Value != null) {                    
                    item.DataInicio = workSheet.Cell(linha, coluna2).ValueAsDateTime;
                }
                if (workSheet.Cell(linha, coluna3).Value != null) {
                    item.Periodicidade = workSheet.Cell(linha, coluna3).ValueAsString.Trim();
                }
                if (workSheet.Cell(linha, coluna4).Value != null) {
                    item.LocalFormaDivulgacao = workSheet.Cell(linha, coluna4).ValueAsString.Trim();
                }
                if (workSheet.Cell(linha, coluna5).Value != null) {
                    item.LocalFormaSolicitacaoCotista = workSheet.Cell(linha, coluna5).ValueAsString.Trim();
                }
                if (workSheet.Cell(linha, coluna6).Value != null) {
                    item.FatoresRisco = workSheet.Cell(linha, coluna6).ValueAsString.Trim();
                }
                if (workSheet.Cell(linha, coluna7).Value != null) {
                    item.PoliticaExercicioVoto = workSheet.Cell(linha, coluna7).ValueAsString.Trim();
                }
                if (workSheet.Cell(linha, coluna8).Value != null) {
                    item.TributacaoAplicavel = workSheet.Cell(linha, coluna8).ValueAsString.Trim();
                }
                if (workSheet.Cell(linha, coluna9).Value != null) {
                    item.PoliticaAdministracaoRisco = workSheet.Cell(linha, coluna9).ValueAsString.Trim();
                }
                if (workSheet.Cell(linha, coluna10).Value != null) {
                    item.AgenciaClassificacaoRisco = workSheet.Cell(linha, coluna10).ValueAsString.Trim();
                }
                if (workSheet.Cell(linha, coluna11).Value != null) {
                    item.RecursosServicosGestor = workSheet.Cell(linha, coluna11).ValueAsString.Trim();
                }
                if (workSheet.Cell(linha, coluna12).Value != null) {
                    item.PrestadoresServicos = workSheet.Cell(linha, coluna12).ValueAsString.Trim();
                }
                if (workSheet.Cell(linha, coluna13).Value != null) {
                    item.PoliticaDistribuicaoCotas = workSheet.Cell(linha, coluna13).ValueAsString.Trim();
                }
                if (workSheet.Cell(linha, coluna14).Value != null) {
                    item.Observacoes = workSheet.Cell(linha, coluna14).ValueAsString.Trim();
                }
                               
                this.valoresExcelInfoComplementarFundo.Add(item);
                //
                index++;
                linha = 1 + index;
                //                
                //Console.WriteLine("{0},{1},{2},{3},{4},{5},{6},{7}", 
                //            item.CdAtivoBolsa, item.Especificao,
                //            item.Descricao, item.TipoMercado,
                //            item.PuExercicio, item.DataVencimento,
                //            item.CodigoAtivoObjeto, item.CodigoIsin);
            }
        }
        catch (Exception ex) {
            document.Close();
            document.Dispose();

            throw new Exception(ex.Message);
        }
        #endregion

        document.Close();
        document.Dispose();
    }

    /// <summary>
    /// Gera e Carrega as InfoComplementarFundo de acordo com o objeto List<ValoresExcel>
    /// </summary>
    private void CarregaInfoComplementarFundo() {
        InformacoesComplementaresFundoCollection infoCollection = new InformacoesComplementaresFundoCollection();        
        //    
        for (int i = 0; i < this.valoresExcelInfoComplementarFundo.Count; i++) {
            ValoresExcelInfoComplementarFundo valoresExcel = this.valoresExcelInfoComplementarFundo[i];
            //

            #region Confere Null
            if (!valoresExcel.IdCarteira.HasValue) {
                throw new Exception("IdCarteira não pode ser nulo.");
            }
            else if (!valoresExcel.DataInicio.HasValue) {
                throw new Exception("Data Início não pode ser nulo.");
            }
            #endregion

            #region Verifica se já existe InfoComplementar com o mesmo IdCarteira/Data
            InformacoesComplementaresFundo info = new InformacoesComplementaresFundo();

            // Se não existe é feito um insert, caso contrario é feito um update
            if (!info.LoadByPrimaryKey(valoresExcel.IdCarteira.Value, valoresExcel.DataInicio.Value)) {
                info = new InformacoesComplementaresFundo();
            }

            #region Update/Insert

            #region Verifica se Existe Carteira
            Carteira carteira = new Carteira();
            if (!carteira.LoadByPrimaryKey(valoresExcel.IdCarteira.Value)) {
                throw new Exception("Carteira Não existente : " + valoresExcel.IdCarteira.Value);
            }
            #endregion

            // Dados do Arquivo
            info.IdCarteira = valoresExcel.IdCarteira.Value;
            info.DataInicioVigencia = valoresExcel.DataInicio.Value;
            info.Periodicidade = valoresExcel.Periodicidade;
            info.LocalFormaDivulgacao = valoresExcel.LocalFormaDivulgacao;
            info.LocalFormaSolicitacaoCotista = valoresExcel.LocalFormaSolicitacaoCotista;
            info.FatoresRisco = valoresExcel.FatoresRisco;
            info.PoliticaExercicioVoto = valoresExcel.PoliticaExercicioVoto;
            info.TributacaoAplicavel = valoresExcel.TributacaoAplicavel;
            info.PoliticaAdministracaoRisco = valoresExcel.PoliticaAdministracaoRisco;
            info.AgenciaClassificacaoRisco = valoresExcel.AgenciaClassificacaoRisco;
            info.RecursosServicosGestor = valoresExcel.RecursosServicosGestor;
            info.PrestadoresServicos = valoresExcel.PrestadoresServicos;
            info.PoliticaDistribuicaoCotas = valoresExcel.PoliticaDistribuicaoCotas;
            info.Observacoes = valoresExcel.Observacoes;

            // Attach the object
            infoCollection.AttachEntity(info);

            #endregion
            #endregion           
        }

        // Salva Info Complementar presentes no Excel                
        infoCollection.Save();
    }

    /// <summary>
    /// Processa a Planilha de Info Complementar após ter acabado o Upload
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void uplInfoComplementarFundo_FileUploadComplete(object sender, FileUploadCompleteEventArgs e) {
        e.CallbackData = "";

        #region Trata Extensão Válida
        if (!ValoresExcelUtil.isExtensaoExcel(e.UploadedFile.FileName.Trim())) {
            e.CallbackData = "Importação Informações Complementares Fundo - Formato de Arquivo Inválido. Formato permitido: .xls ou .xlsx\n\n";
            return;
        }
        #endregion

        // Stream de bytes com o Conteudo do Arquivo Excel
        Stream sr = e.UploadedFile.FileContent;

        #region Processamento Arquivo Excel

        // Reseta Vetor de Dados
        this.valoresExcelInfoComplementarFundo = new List<ValoresExcelInfoComplementarFundo>();

        //
        try {
            // Ler Arquivo
            this.LerArquivoInfoComplementarFundo(sr);
            // Carrega Arquivo
            this.CarregaInfoComplementarFundo();
        }
        catch (Exception e2) {
            e.CallbackData = "Importação Info Complementar Fundo - " + e2.Message;
            return;
        }

        new HistoricoLog().InsereHistoricoLog(DateTime.Now, DateTime.Now,
                                "Importação Planilha Informação Complementar Fundo",
                                HttpContext.Current.User.Identity.Name, UtilitarioWeb.GetIP(Request), "", HistoricoLogOrigem.Outros);

        #endregion

    }
}