﻿using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Financial.Web.Common;
using System.IO;
using Financial.Integracao.Excel.Util;
using Financial.Integracao.Excel;
using EntitySpaces.Interfaces;
using Financial.Common;
using Financial.Bolsa;
using Financial.Bolsa.Enums;
using Financial.Investidor;
using System.Collections.Generic;
using Financial.Investidor.Enums;
using DevExpress.Web;
using Financial.Common.Exceptions;
using Financial.Bolsa.Exceptions;
using Financial.InvestidorCotista;
using Financial.Fundo;
using Financial.Util;
using Financial.Security;
using Financial.Web.Util;
using Financial.Security.Enums;

public partial class ImportacaoBasePage : BasePage {

    /* Estrutura do Excel */
    private List<ValoresExcelPrejuizoCotista> valoresExcelPrejuizoCotista = new List<ValoresExcelPrejuizoCotista>();
    //
    List<int> idCarteiraDeletarPrejuizo = new List<int>();
    List<int> idCotistaDeletarPrejuizo = new List<int>();
    
    /// <summary>
    /// Leitura de um Arquivo Excel
    /// Saida: List<ValoresExcelPrejuizoCotista> com os valores armazenados no arquivo
    /// </summary>
    /// <param name="streamExcel">Stream de bytes de um arquivo Excel</param>
    /// <exception cref="Exception">Se ocorreu problemas</exception>
    private void LerArquivoPrejuizoCotista(Stream streamExcel) {

        #region Leitura do Arquivo Excel
        // Open Spreadsheet
        Bytescout.Spreadsheet.Spreadsheet document = new Bytescout.Spreadsheet.Spreadsheet();
        document.LoadFromStream(streamExcel);

        Bytescout.Spreadsheet.Worksheet workSheet = document.Workbook.Worksheets[0];
        //
        #region Confere Formato Arquivo
        string[] colunasConferencias = new string[] { "IdCotista", "IdCarteira", "Data", "ValorPrejuizo", "DataLimiteCompensação" };

        bool formato = ValoresExcelUtil.isExcelFormatoValido(colunasConferencias, workSheet);
        if (!formato) {
            string mensagem = "Formato Interno do Arquivo Inválido.\n";
            mensagem += "Linha 1 do Arquivo deve ser: \n";
            for (int i = 0; i < colunasConferencias.Length; i++) {
                int coluna = i + 1;
                mensagem += "\tColuna " + coluna + ": " + colunasConferencias[i] + "\n";
            }

            document.Close();
            document.Dispose();

            throw new Exception(mensagem);
        }
        #endregion

        int index = 0;
        // This row, column index should be changed as per your need.
        // i.e. which cell in the excel you are interesting to read.
        //
        /* Formato: 1)IdCotista - 2)IdCarteira - 3)Data - 4)ValorPrejuizo - 5)DataLimiteCompensação
         */

        int linha = 1;
        int coluna1 = 0, coluna2 = 1, coluna3 = 2,
            coluna4 = 3, coluna5 = 4;
        //
        //            
        try {
            // Enquanto idCotista tiver valor
            while (workSheet.Cell(linha, coluna1).Value != null) {
                ValoresExcelPrejuizoCotista item = new ValoresExcelPrejuizoCotista();
                
                try {
                    item.IdCotista = workSheet.Cell(linha, coluna1).ValueAsInteger;
                }
                catch (Exception ex) {
                    throw new Exception(ex.Message + " - IdCotista: linha " + (linha + 1));
                }

                if (workSheet.Cell(linha, coluna2).Value == null)
                    throw new Exception("IdCarteira não informado: linha " + (linha + 1));
                try
                {
                    item.IdCarteira = workSheet.Cell(linha, coluna2).ValueAsInteger;
                }
                catch (Exception ex) {
                    throw new Exception(ex.Message + " - IdCarteira: linha " + (linha + 1));
                }

                if (workSheet.Cell(linha, coluna3).Value == null)
                    throw new Exception("Data não informada: linha " + (linha + 1));
                try
                {
                    item.Data = workSheet.Cell(linha, coluna3).ValueAsDateTime;
                }
                catch (Exception ex) {
                    throw new Exception(ex.Message + " - Data: linha " + (linha + 1));
                }

                if (workSheet.Cell(linha, coluna4).Value == null)
                    throw new Exception("ValorPrejuizo não informado: linha " + (linha + 1));
                try
                {
                    item.ValorPrejuizo = Convert.ToDecimal(workSheet.Cell(linha, coluna4).Value);
                }
                catch (Exception ex) {
                    throw new Exception(ex.Message + " - ValorPrejuizo: linha " + (linha + 1));
                }
                                
                if (workSheet.Cell(linha, coluna5).Value != null) {
                    try {
                        item.DataLimiteCompensacao = workSheet.Cell(linha, coluna5).ValueAsDateTime;
                    }
                    catch (Exception ex) {
                        throw new Exception(ex.Message + " - DataLimiteCompensacao: linha " + (linha + 1));
                    }
                }
                //
                this.valoresExcelPrejuizoCotista.Add(item);
                //
                index++;
                linha = 1 + index;
                //                
                Console.WriteLine("{0},{1},{2},{3},{4}",
                    item.IdCotista, item.IdCarteira, item.Data, item.ValorPrejuizo, item.DataLimiteCompensacao);
            }
        }
        catch (Exception ex) {
            document.Close();
            document.Dispose();

            throw new Exception(ex.Message);
        }
        #endregion

        document.Close();
        document.Dispose();
    }

    /// <summary>
    /// Gera e Carrega os Prejuízos Cotista de Acordo com o objeto List<ValoresExcelPrejuizoCotista>
    /// </summary>
    /// <exception cref="Exception">Se ocorreu problemas</exception>
    private void CarregaPrejuizoCotista() {
        PrejuizoCotistaCollection prejuizoCotistaCollection = new PrejuizoCotistaCollection();
        PrejuizoCotistaCollection prejuizoCotistaDeletarCollection = new PrejuizoCotistaCollection();

        PrejuizoFundoCollection prejuizoFundoCollection = new PrejuizoFundoCollection();
        PrejuizoFundoCollection prejuizoFundoDeletarCollection = new PrejuizoFundoCollection();
        //
        PrejuizoCotistaHistoricoCollection prejuizoCotistaHistoricoCollection = new PrejuizoCotistaHistoricoCollection();
        PrejuizoCotistaHistoricoCollection prejuizoCotistaHistoricoDeletarCollection = new PrejuizoCotistaHistoricoCollection();

        PrejuizoFundoHistoricoCollection prejuizoFundoHistoricoCollection = new PrejuizoFundoHistoricoCollection();
        PrejuizoFundoHistoricoCollection prejuizoFundoHistoricoDeletarCollection = new PrejuizoFundoHistoricoCollection();
        //    
        for (int i = 0; i < this.valoresExcelPrejuizoCotista.Count; i++) {
            ValoresExcelPrejuizoCotista valoresExcel = this.valoresExcelPrejuizoCotista[i];
            //
            this.idCarteiraDeletarPrejuizo.Add(valoresExcel.IdCarteira);
            this.idCotistaDeletarPrejuizo.Add(valoresExcel.IdCotista);
            
            #region Verifica se Existe Carteira
            Carteira carteira = new Carteira();
            if (!carteira.LoadByPrimaryKey(valoresExcel.IdCarteira)) {
                throw new Exception("Carteira Não existente : " + valoresExcel.IdCarteira);
            }
            #endregion

            #region Verifica se Existe Cotista
            bool existeCotista = false;
            bool existeCliente = false;
            
            Cotista cotista = new Cotista();
            if (cotista.LoadByPrimaryKey(valoresExcel.IdCotista)) 
            {
                existeCotista = true;
            }

            Cliente clienteExiste = new Cliente();
            if (clienteExiste.LoadByPrimaryKey(valoresExcel.IdCotista))
            {
                existeCliente = true;
            }

            if (!existeCotista && !existeCliente)
            {
                throw new Exception("Cotista ou Cliente Não existente : " + valoresExcel.IdCotista);
            }
            #endregion
                        
            if (existeCotista)
            {
                #region DataDia da Carteira
                Cliente cliente = new Cliente();
                List<esQueryItem> campos = new List<esQueryItem>();
                campos.Add(cliente.Query.DataDia);
                if (!cliente.LoadByPrimaryKey(campos, valoresExcel.IdCarteira))
                {
                    throw new Exception("Cliente Não existente : " + valoresExcel.IdCarteira);
                }
                #endregion

                #region PrejuizoCotista

                PrejuizoCotista prejuizo = prejuizoCotistaCollection.AddNew();
                // Dados do Arquivo
                prejuizo.IdCotista = valoresExcel.IdCotista;
                prejuizo.IdCarteira = valoresExcel.IdCarteira;
                prejuizo.Data = valoresExcel.Data;
                prejuizo.ValorPrejuizo = valoresExcel.ValorPrejuizo;
                prejuizo.ValorPrejuizoOriginal = valoresExcel.ValorPrejuizo;
                if (valoresExcel.DataLimiteCompensacao.HasValue)
                {
                    prejuizo.DataLimiteCompensacao = valoresExcel.DataLimiteCompensacao.Value;
                }
                #endregion

                #region PrejuizoCotistaHistorico
                DateTime dataAux = Calendario.SubtraiDiaUtil(cliente.DataDia.Value, 1);

                while (dataAux >= prejuizo.Data)
                {
                    PrejuizoCotistaHistorico prejuizoHistorico = prejuizoCotistaHistoricoCollection.AddNew();
                    prejuizoHistorico.IdCotista = prejuizo.IdCotista;
                    prejuizoHistorico.IdCarteira = prejuizo.IdCarteira;
                    prejuizoHistorico.Data = prejuizo.Data;
                    prejuizoHistorico.DataHistorico = dataAux;
                    prejuizoHistorico.ValorPrejuizo = prejuizo.ValorPrejuizo;
                    prejuizoHistorico.ValorPrejuizoOriginal = prejuizo.ValorPrejuizoOriginal;
                    prejuizoHistorico.DataLimiteCompensacao = prejuizo.DataLimiteCompensacao;

                    dataAux = Calendario.SubtraiDiaUtil(dataAux, 1);
                }
                #endregion
            }

            if (existeCliente)
            {
                #region DataDia do Cliente
                Cliente cliente = new Cliente();
                List<esQueryItem> campos = new List<esQueryItem>();
                campos.Add(cliente.Query.DataDia);
                if (!cliente.LoadByPrimaryKey(campos, valoresExcel.IdCotista))
                {
                    throw new Exception("Cotista Não existente : " + valoresExcel.IdCotista);
                }
                #endregion

                #region PrejuizoFundo

                PrejuizoFundo prejuizo = prejuizoFundoCollection.AddNew();
                // Dados do Arquivo
                prejuizo.IdCliente = valoresExcel.IdCotista;
                prejuizo.IdCarteira = valoresExcel.IdCarteira;
                prejuizo.Data = valoresExcel.Data;
                prejuizo.ValorPrejuizo = valoresExcel.ValorPrejuizo;
                if (valoresExcel.DataLimiteCompensacao.HasValue)
                {
                    prejuizo.DataLimiteCompensacao = valoresExcel.DataLimiteCompensacao.Value;
                }
                #endregion

                #region PrejuizoFundoHistorico
                DateTime dataAux = Calendario.SubtraiDiaUtil(cliente.DataDia.Value, 1);

                while (dataAux >= prejuizo.Data)
                {
                    PrejuizoFundoHistorico prejuizoHistorico = prejuizoFundoHistoricoCollection.AddNew();
                    prejuizoHistorico.IdCliente = prejuizo.IdCliente;
                    prejuizoHistorico.IdCarteira = prejuizo.IdCarteira;
                    prejuizoHistorico.Data = prejuizo.Data;
                    prejuizoHistorico.DataHistorico = dataAux;
                    prejuizoHistorico.ValorPrejuizo = prejuizo.ValorPrejuizo;
                    prejuizoHistorico.DataLimiteCompensacao = prejuizo.DataLimiteCompensacao;

                    dataAux = Calendario.SubtraiDiaUtil(dataAux, 1);
                }                
                #endregion
            }
        }

        using (esTransactionScope scope = new esTransactionScope()) 
        {
            #region Deleta PrejuizoCotista por IdCarteira
            prejuizoCotistaDeletarCollection.Query.Where(prejuizoCotistaDeletarCollection.Query.IdCarteira.In(this.idCarteiraDeletarPrejuizo));
            prejuizoCotistaDeletarCollection.Query.Load();
            prejuizoCotistaDeletarCollection.MarkAllAsDeleted();
            prejuizoCotistaDeletarCollection.Save();
            #endregion

            #region Deleta PrejuizoCotistaHistorico por IdCarteira
            prejuizoCotistaHistoricoDeletarCollection.Query.Where(prejuizoCotistaHistoricoDeletarCollection.Query.IdCarteira.In(this.idCarteiraDeletarPrejuizo));
            prejuizoCotistaHistoricoDeletarCollection.Query.Load();
            prejuizoCotistaHistoricoDeletarCollection.MarkAllAsDeleted();
            prejuizoCotistaHistoricoDeletarCollection.Save();
            #endregion

            #region Deleta PrejuizoFundo por IdCliente
            int i = 0;
            foreach (int idCarteira in idCarteiraDeletarPrejuizo)
            {
                prejuizoFundoDeletarCollection.QueryReset();
                prejuizoFundoDeletarCollection.Query.Where(prejuizoFundoDeletarCollection.Query.IdCarteira.Equal(idCarteira),
                                                           prejuizoFundoDeletarCollection.Query.IdCliente.Equal(this.idCotistaDeletarPrejuizo[i]));
                prejuizoFundoDeletarCollection.Query.Load();
                prejuizoFundoDeletarCollection.MarkAllAsDeleted();
                prejuizoFundoDeletarCollection.Save();

                i++;
            }            
            #endregion

            #region Deleta PrejuizoFundooHistorico por IdCliente
            int j = 0;
            foreach (int idCarteira in idCarteiraDeletarPrejuizo)
            {
                prejuizoFundoHistoricoDeletarCollection.QueryReset();
                prejuizoFundoHistoricoDeletarCollection.Query.Where(prejuizoFundoHistoricoDeletarCollection.Query.IdCarteira.Equal(idCarteira),
                                                                    prejuizoFundoHistoricoDeletarCollection.Query.IdCliente.Equal(this.idCotistaDeletarPrejuizo[j]));
                prejuizoFundoHistoricoDeletarCollection.Query.Load();
                prejuizoFundoHistoricoDeletarCollection.MarkAllAsDeleted();
                prejuizoFundoHistoricoDeletarCollection.Save();

                j++;
            }
            #endregion

            // Salva os Prejuizos Cotistas Presentes no Excel
            prejuizoCotistaCollection.Save();
            //
            prejuizoCotistaHistoricoCollection.Save();

            prejuizoFundoCollection.Save();

            prejuizoFundoHistoricoCollection.Save();

            scope.Complete();
        }
    }

    /// <summary>
    /// Processa a Planilha de Prejuízo Cotista após ter acabado o Upload
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void uplPrejuizoCotista_FileUploadComplete(object sender, FileUploadCompleteEventArgs e) {
        e.CallbackData = "";

        #region Trata Extensão Válida
        if (!ValoresExcelUtil.isExtensaoExcel(e.UploadedFile.FileName.Trim())) {
            e.CallbackData = "Importação Prejuízo Cotista - Formato de Arquivo Inválido. Formato permitido: .xls ou .xlsx\n\n";
            return;
        }
        #endregion

        // Stream de bytes com o Conteudo do Arquivo Excel
        Stream sr = e.UploadedFile.FileContent;

        #region Processamento Arquivo Excel

        //Reseta Vetor de Dados
        this.valoresExcelPrejuizoCotista = new List<ValoresExcelPrejuizoCotista>();
        //
        this.idCarteiraDeletarPrejuizo = new List<int>();

        try {
            // Ler Arquivo
            this.LerArquivoPrejuizoCotista(sr);
            // Carrega Arquivo
            this.CarregaPrejuizoCotista();
        }
        catch (Exception e2) {
            e.CallbackData = "Importação Prejuízo Cotista - " + e2.Message;
            return;
        }

        new HistoricoLog().InsereHistoricoLog(DateTime.Now, DateTime.Now,
            "Importação Planilha Prejuízo Cotista",
            HttpContext.Current.User.Identity.Name, UtilitarioWeb.GetIP(Request), "", HistoricoLogOrigem.Outros);

        #endregion
    }
}