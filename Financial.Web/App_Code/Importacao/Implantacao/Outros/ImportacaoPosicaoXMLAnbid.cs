﻿using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Web.UI.HtmlControls;
using System.Collections.Generic;
using System.Text;
using Financial.Web.Common;
using Financial.Fundo;
using DevExpress.Web;
using System.Globalization;
using Financial.Bolsa;
using Financial.BMF;
using Financial.RendaFixa;
using Financial.ContaCorrente;

public partial class ImportacaoBasePage : BasePage
{    
    /// <summary>
    /// Processa arquivo txt de Posicao SMA Carregando em TabelaCargaPassivo
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void uplPosicaoXMLAnbid_FileUploadComplete(object sender, FileUploadCompleteEventArgs e)
    {
        e.CallbackData = "";

        #region Trata Extensão Válida

        if (!this.isExtensaoXML(e.UploadedFile.FileName.Trim()))
        {
            e.CallbackData = "Arquivo Posição XML Anbid inválido: Extensão do arquivo deve ser .xml. \n\n";
            return;
        }
        #endregion

        // Stream de bytes com o Conteudo do Arquivo Excel
        StreamReader sr = new StreamReader(e.UploadedFile.FileContent);

        Carteira carteira = new Carteira();
        try
        {
            Carteira.CarteiraDiaria carteiraDiaria = new Carteira.CarteiraDiaria();

            carteiraDiaria = carteira.MontaCarteiraXMLAnbid(sr);

            Carteira.ConfigXMLAnbima configXMLAnbima = new Carteira.ConfigXMLAnbima();
            configXMLAnbima.ImportaBolsa = true;
            configXMLAnbima.ImportaBTC = false;
            configXMLAnbima.ImportaCota = true;
            configXMLAnbima.ImportaFundos = true;
            configXMLAnbima.ImportaLiquidacao = true;
            configXMLAnbima.ImportaRendaFixa = true;
            carteira.SalvaCarteiraDiaria(carteiraDiaria, configXMLAnbima);

        }
        catch (Exception e1)
        {
            string msg = "\tPosição XML Anbid com problemas \n";
            msg += " - Dados não Importados: " + e1.Message;

            e.CallbackData = msg;
            return;
        }
    }
}