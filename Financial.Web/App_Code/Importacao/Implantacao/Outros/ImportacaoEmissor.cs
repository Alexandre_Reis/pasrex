﻿using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Financial.Web.Common;
using System.IO;
using Financial.Integracao.Excel.Util;
using Financial.Integracao.Excel;
using EntitySpaces.Interfaces;
using Financial.Common;
using Financial.Bolsa;
using Financial.Bolsa.Enums;
using Financial.Investidor;
using System.Collections.Generic;
using Financial.Investidor.Enums;
using DevExpress.Web;
using Financial.Common.Exceptions;
using Financial.Bolsa.Exceptions;
using Financial.RendaFixa;
using System.Text;
using Financial.RendaFixa.Enums;
using Financial.Common.Enums;
using Financial.Security;
using Financial.Web.Util;
using Financial.Security.Enums;

public partial class ImportacaoBasePage : BasePage {

    /* Estrutura do Excel */
    private List<ValoresExcelEmissor> valoresExcelEmissor = new List<ValoresExcelEmissor>();
    //

    /// <summary>
    /// Leitura de um arquivo Excel
    /// Saida: List<ValoresExcel> com os valores armazenados no arquivo
    /// </summary>
    /// <param name="streamExcel">Stream de bytes de um arquivo Excel</param>
    /// <exception cref="Exception">Se ocorreu problemas</exception>
    private void LerArquivoEmissor(Stream streamExcel) {

        #region Leitura do Arquivo Excel

        // Open Spreadsheet
        Bytescout.Spreadsheet.Spreadsheet document = new Bytescout.Spreadsheet.Spreadsheet();
        document.LoadFromStream(streamExcel);

        Bytescout.Spreadsheet.Worksheet workSheet = document.Workbook.Worksheets[0];

        #region Confere Formato Arquivo
        string[] colunasConferencias = new string[] {"NomeEmissor","CnpjEmissor"};

        bool formato = ValoresExcelUtil.isExcelFormatoValido(colunasConferencias, workSheet);
        if (!formato) {
            string mensagem = "Formato Interno do Arquivo Inválido.\n";
            mensagem += "Linha 1 do Arquivo deve ser: \n";
            for (int i = 0; i < colunasConferencias.Length; i++) {
                int coluna = i + 1;
                mensagem += "\tColuna " + coluna + ": " + colunasConferencias[i] + "\n";
            }

            document.Close();
            document.Dispose();

            throw new Exception(mensagem);
        }
        #endregion
        //
        int index = 0;
        // This row,column index should be changed as per your need.
        // i.e. which cell in the excel you are interesting to read.
        //
        /* Formato: 1)NomeEmissor    - 2)CnpjEmissor
         */
        int linha = 1;
        int coluna1 = 0, coluna2 = 1;
        //
        try {
            // Enquanto tiver registro
            while (workSheet.Cell(linha, coluna1).Value != null) {
                ValoresExcelEmissor item = new ValoresExcelEmissor();
                
                try {
                    item.NomeEmissor = workSheet.Cell(linha, coluna1).ValueAsString;
                }
                catch (Exception ex) {
                    throw new Exception(ex.Message + " - NomeEmissor: linha " + (linha + 1));
                }

                item.CnpjEmissor = null;
                if (workSheet.Cell(linha, coluna2).Value != null)
                {
                    try {
                        item.CnpjEmissor  = workSheet.Cell(linha, coluna2).ValueAsString;    
                    }
                    catch (Exception ex) {
                        throw new Exception(ex.Message + " - CnpjEmissor: linha " + (linha + 1));
                    }
                }
                //
                this.valoresExcelEmissor.Add(item);
                //
                index++;
                linha = 1 + index;
                //                
                Console.WriteLine("{0},{1}", item.NomeEmissor, item.CnpjEmissor);
            }
        }
        catch (Exception ex) {
            document.Close();
            document.Dispose();

            throw new Exception(ex.Message);
        }
        #endregion

        document.Close();
        document.Dispose();
    }

    /// <summary>
    /// Gera e Carrega Emissor com o objeto List<ValoresExcel>
    /// </summary>
    /// <exception cref="Exception">Se ocorreu problemas</exception>
    private void CarregaEmissor() {
        
        // Não considerar esses emissores
        List<string> emissoresNaoConsiderados = new List<string>(new string[] { 
            "Fundo", "fundo",
            "RendaFixa", "rendafixa", "renda fixa", "Renda Fixa",
            "Ação", "ação", 
            "Ações", "ações",
            "Multimercado", "multimercado",
            "Investimento", "investimento"
        });
        
        EmissorCollection emissorCollection = new EmissorCollection();
        //
        for (int i = 0; i < this.valoresExcelEmissor.Count; i++) {
            ValoresExcelEmissor valoresExcel = this.valoresExcelEmissor[i];
            //               
            #region Verifica se Existe Emissor

            EmissorCollection emissorCollectionAux = new EmissorCollection();
            emissorCollectionAux.Query.Select(emissorCollectionAux.Query.IdEmissor);
            emissorCollectionAux.Query.Where(emissorCollectionAux.Query.Nome == valoresExcel.NomeEmissor.Trim() ||
                                             emissorCollectionAux.Query.Cnpj == valoresExcel.CnpjEmissor.Trim());

            if (emissorCollectionAux.Query.Load()) {
                throw new Exception("Emissor já existente: " + valoresExcel.NomeEmissor.Trim());
            }
            #endregion

            if (!String.IsNullOrEmpty(valoresExcel.CnpjEmissor.Trim())) {
                if (valoresExcel.CnpjEmissor.Trim().Length != 14) {
                    throw new Exception("CNPJ deve ser numérico e ter 14 digitos: " + valoresExcel.CnpjEmissor.Trim());
                }
            }

            // Nao considera
            if(emissoresNaoConsiderados.Contains(valoresExcel.NomeEmissor.Trim())) {
                continue; 
            }

            Emissor e = emissorCollection.AddNew();
            //
            e.Nome = valoresExcel.NomeEmissor.Trim();
            e.TipoEmissor = (int)TipoEmissor.PessoaJuridica;
            e.IdSetor = 9998; // Padrão
            //
            e.Cnpj = !String.IsNullOrEmpty(valoresExcel.CnpjEmissor) ? valoresExcel.CnpjEmissor.Trim().Replace("-", "").Replace(".", "").Replace("\\", "") : null;
        }

        emissorCollection.Save();

        if (this.valoresExcelEmissor.Count == 0) {
            throw new Exception("Não há dados para importar.");
        }
    }

    /// <summary>
    /// Processa a Planilha de Emissor após ter acabado o Upload
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void uplEmissor_FileUploadComplete(object sender, FileUploadCompleteEventArgs e) {
        e.CallbackData = "";

        #region Trata Extensão Válida
        if (!ValoresExcelUtil.isExtensaoExcel(e.UploadedFile.FileName.Trim())) {
            e.CallbackData = "Importação Emissor - Formato de Arquivo Inválido. Formato permitido: .xls ou .xlsx\n\n";
            return;
        }
        #endregion

        // Stream de bytes com o Conteudo do Arquivo Excel
        Stream sr = e.UploadedFile.FileContent;

        #region Processamento Arquivo Excel

        //Reseta vetor de dados
        this.valoresExcelEmissor = new List<ValoresExcelEmissor>();

        try {
            // Ler Arquivo
            this.LerArquivoEmissor(sr);
            // Carrega Arquivo
            this.CarregaEmissor();
        }
        catch (Exception e2) {
            e.CallbackData = "Importação Emissor - " + e2.Message;
            return;
        }

        new HistoricoLog().InsereHistoricoLog(DateTime.Now, DateTime.Now,
            "Importação Planilha Emissor",
            HttpContext.Current.User.Identity.Name, UtilitarioWeb.GetIP(Request), "", HistoricoLogOrigem.Outros);

        #endregion
    }
}