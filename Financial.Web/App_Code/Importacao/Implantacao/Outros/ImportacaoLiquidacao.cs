﻿using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Financial.Web.Common;
using System.IO;
using Financial.Integracao.Excel.Util;
using Financial.Integracao.Excel;
using EntitySpaces.Interfaces;
using Financial.Common;
using Financial.Bolsa;
using Financial.Bolsa.Enums;
using Financial.Investidor;
using System.Collections.Generic;
using Financial.Investidor.Enums;
using DevExpress.Web;
using Financial.Common.Exceptions;
using Financial.Bolsa.Exceptions;
using Financial.ContaCorrente;
using Financial.ContaCorrente.Enums;
using Financial.Security;
using Financial.Web.Util;
using Financial.Security.Enums;
using System.Collections;

public partial class ImportacaoBasePage : BasePage {

    /* Estrutura do Excel */
    private List<ValoresExcelLiquidacao> valoresExcelLiquidacao = new List<ValoresExcelLiquidacao>();

    List<int> idClienteDeletarLiquidacao = new List<int>();
    List<DateTime> dataHistoricoDeletarLiquidacao = new List<DateTime>();

    /// <summary>
    /// Leitura de um arquivo Excel
    /// Saida: List<ValoresExcel> com os valores armazenados no arquivo
    /// </summary>
    /// <param name="streamExcel">Stream de bytes de um arquivo Excel</param>
    /// <exception cref="Exception">Se ocorreu problemas</exception>
    private void LerArquivoLiquidacao(Stream streamExcel) {

        #region Leitura do Arquivo Excel

        // Open Spreadsheet
        Bytescout.Spreadsheet.Spreadsheet document = new Bytescout.Spreadsheet.Spreadsheet();
        document.LoadFromStream(streamExcel);

        Bytescout.Spreadsheet.Worksheet workSheet = document.Workbook.Worksheets[0];

        //
        #region Confere Formato Arquivo
        string[] colunasConferencias = new string[] 
                        {"IdCliente","Tipo","DataLancamento",
					     "DataVencimento","Descricao","Valor" };

        bool formato = ValoresExcelUtil.isExcelFormatoValido(colunasConferencias, workSheet);

        if (!formato) {
            string mensagem = "Formato Interno do Arquivo Inválido.\n";
            mensagem += "Linha 1 do Arquivo deve ser: \n";
            for (int i = 0; i < colunasConferencias.Length; i++) {
                int coluna = i + 1;
                mensagem += "\tColuna " + coluna + ": " + colunasConferencias[i] + "\n";
            }

            document.Close();
            document.Dispose();

            throw new Exception(mensagem);
        }
        #endregion

        int index = 0;
        // This row,column index should be changed as per your need.
        // i.e. which cell in the excel you are interesting to read.
        //
        /* Formato: 1)IdCarteira  - 2)Data - 3)Cota - 4)PL - 5)QuantidadeFechamento - 6)IdConta     
         */

        int linha = 1;
        int coluna1 = 0, coluna2 = 1, coluna3 = 2,
            coluna4 = 3, coluna5 = 4, coluna6 = 5,
            coluna7 = 6;

        try {
            // Enquanto IdCliente tiver valor
            while (workSheet.Cell(linha, coluna1).Value != null) {
                ValoresExcelLiquidacao item = new ValoresExcelLiquidacao();
                
                try {
                    item.IdCliente = workSheet.Cell(linha, coluna1).ValueAsInteger;
                }
                catch (Exception ex) {
                    throw new Exception(ex.Message + " - IdCliente: linha " + (linha + 1));
                }

                if (workSheet.Cell(linha, coluna2).Value == null)
                    throw new Exception("TipoSaldo não informado: linha " + (linha + 1));
                try
                {
                    int tipo = workSheet.Cell(linha, coluna2).ValueAsInteger;
                    item.TipoSaldo = (EnumTipoSaldo)tipo;
                }
                catch (Exception ex) {
                    throw new Exception(ex.Message + " - TipoSaldo: linha " + (linha + 1));
                }

                if (workSheet.Cell(linha, coluna3).Value == null)
                    throw new Exception("DataLancamento não informada: linha " + (linha + 1));
                try
                {
                    item.DataLancamento = workSheet.Cell(linha, coluna3).ValueAsDateTime;
                }
                catch (Exception ex) {
                    throw new Exception(ex.Message + " - DataLancamento: linha " + (linha + 1));
                }

                if (workSheet.Cell(linha, coluna4).Value == null)
                    throw new Exception("DataVencimento não informada: linha " + (linha + 1));
                try
                {
                    item.DataVencimento = workSheet.Cell(linha, coluna4).ValueAsDateTime;
                }
                catch (Exception ex) {
                    throw new Exception(ex.Message + " - DataVencimento: linha " + (linha + 1));
                }

                if (workSheet.Cell(linha, coluna5).Value == null)
                    throw new Exception("Descricao não informada: linha " + (linha + 1));
                try
                {
                    item.Descricao = workSheet.Cell(linha, coluna5).Value.ToString();
                }
                catch (Exception ex) {
                    throw new Exception(ex.Message + " - Descricao: linha " + (linha + 1));
                }

                if (workSheet.Cell(linha, coluna6).Value == null)
                    throw new Exception("Valor não informado: linha " + (linha + 1));
                try
                {
                    item.Valor = Convert.ToDecimal(workSheet.Cell(linha, coluna6).Value);
                }
                catch (Exception ex) {
                    throw new Exception(ex.Message + " - Valor: linha " + (linha + 1));
                }

                if (workSheet.Cell(linha, coluna7).Value != null)
                {
                    try
                    {
                        item.IdConta = Convert.ToInt32(workSheet.Cell(linha, coluna7).Value);
                    }
                    catch (Exception ex)
                    {
                        throw new Exception(ex.Message + " - Valor: linha " + (linha + 1));
                    }
                }
                else
                {
                    item.IdConta = null;
                }

                this.valoresExcelLiquidacao.Add(item);
                //
                index++;
                linha = 1 + index;
                //                
                Console.WriteLine("{0},{1},{2},{3},{4},{5},{6}",
                    item.IdCliente, item.TipoSaldo, item.DataLancamento, item.DataVencimento,
                    item.Descricao, item.Valor, item.IdConta);
            }
        }
        catch (Exception ex) {
            document.Close();
            document.Dispose();

            throw new Exception(ex.Message);
        }
        #endregion

        document.Close();
        document.Dispose();
    }


    private void CarregaLiquidacao()
    {
        this.CarregaLiquidacao(true);
    }

    /// <summary>
    /// Gera e carrega as Liquidações e Liquidações Aberturas e saldo Caixas 
    /// de acordo com o objeto List<ValoresExcel>
    /// </summary>
    /// <exception cref="Exception">Se ocorreu problemas</exception>
    private void CarregaLiquidacao(bool apagaLiquidacaoCliente)
    {
        LiquidacaoCollection liquidacaoCollection = new LiquidacaoCollection();
        LiquidacaoCollection liquidacaoDeletarCollection = new LiquidacaoCollection();
        //
        LiquidacaoAberturaCollection liquidacaoAberturaCollection = new LiquidacaoAberturaCollection();
        LiquidacaoAberturaCollection liquidacaoAberturaDeletarCollection = new LiquidacaoAberturaCollection();
        //
        SaldoCaixaCollection saldoCaixaCollection = new SaldoCaixaCollection();
        Financial.Investidor.ContaCorrente contaCorrente = new Financial.Investidor.ContaCorrente();
        Hashtable hsIdContaValido = new Hashtable();

        //    
        for (int i = 0; i < this.valoresExcelLiquidacao.Count; i++) {
            ValoresExcelLiquidacao valoresExcel = this.valoresExcelLiquidacao[i];
            //

            if (apagaLiquidacaoCliente)
            {
                this.idClienteDeletarLiquidacao.Add(valoresExcel.IdCliente);
            }

            if (valoresExcel.IdConta.HasValue && !hsIdContaValido.Contains(valoresExcel.IdConta.Value))
            {
                contaCorrente = new ContaCorrente();

                if (!contaCorrente.LoadByPrimaryKey(valoresExcel.IdConta.Value))
                    throw new Exception("Id.Conta não encontrada/cadastrada!! Id.Conta - " + valoresExcel.IdConta.Value);
                else
                {
                    if (valoresExcel.IdCliente != contaCorrente.IdPessoa.Value)
                        throw new Exception("Id.Conta não pertence a pessoa!! Id.Conta - " + valoresExcel.IdConta.Value + "; Pessoa - " + valoresExcel.IdCliente);

                    hsIdContaValido.Add(valoresExcel.IdConta.Value, valoresExcel.IdConta.Value);
                }
            }

            #region Tipo Inconsistente
            if ((int)valoresExcel.TipoSaldo >= 3) {
                throw new Exception("Tipo Inconsistente. Valores Possíveis: 0, 1, 2");
            }
            #endregion

            #region Verifica se Existe Cliente
            Cliente cliente = new Cliente();
            List<esQueryItem> campos = new List<esQueryItem>();
            campos.Add(cliente.Query.IdCliente);
            campos.Add(cliente.Query.DataDia);
            if (!cliente.LoadByPrimaryKey(campos, valoresExcel.IdCliente)) {
                throw new Exception("Cliente Não existente : " + valoresExcel.IdCliente);
            }
            // Salva a Data Historico para poder Deletar
            this.dataHistoricoDeletarLiquidacao.Add(cliente.DataDia.Value);
            #endregion

            EnumTipoSaldo tipoSaldo = valoresExcel.TipoSaldo;
            //
            switch ((int)tipoSaldo) {
                case (int)EnumTipoSaldo.Normal:
                    #region Liquidacao
                    Liquidacao liquidacao = new Liquidacao();
                    // Dados do Arquivo
                    liquidacao.IdCliente = valoresExcel.IdCliente;
                    liquidacao.DataLancamento = valoresExcel.DataLancamento;
                    liquidacao.DataVencimento = valoresExcel.DataVencimento;
                    liquidacao.Descricao = valoresExcel.Descricao;
                    liquidacao.Valor = valoresExcel.Valor;
                    //
                    //liquidacao.Situacao = valoresExcel.Situacao == null ? (byte)SituacaoLancamentoLiquidacao.Normal : valoresExcel.Situacao;
                    liquidacao.Situacao = (byte)SituacaoLancamentoLiquidacao.Normal;
                    liquidacao.Origem = OrigemLancamentoLiquidacao.Outros;
                    liquidacao.Fonte = (byte)FonteLancamentoLiquidacao.Manual;
                    //liquidacao.IdAgente = null;
                    //liquidacao.IdentificadorOrigem = null;
                    //
                    
                    // Lança exceção se conta não encontrada
                    if (valoresExcel.IdConta.HasValue)
                        liquidacao.IdConta = valoresExcel.IdConta.Value;
                    else
                        liquidacao.IdConta = contaCorrente.RetornaContaDefault(valoresExcel.IdCliente);
                    //
                    // Attach the object
                    liquidacaoCollection.AttachEntity(liquidacao);
                    #endregion

                    #region LiquidacaoAbertura
                    LiquidacaoAbertura liquidacaoAbertura = new LiquidacaoAbertura();
                    liquidacaoAbertura.DataHistorico = cliente.DataDia.Value;
                    liquidacaoAbertura.IdCliente = liquidacao.IdCliente;
                    liquidacaoAbertura.DataLancamento = liquidacao.DataLancamento;
                    liquidacaoAbertura.DataVencimento = liquidacao.DataVencimento;
                    liquidacaoAbertura.Descricao = liquidacao.Descricao;
                    liquidacaoAbertura.Valor = liquidacao.Valor;
                    liquidacaoAbertura.Situacao = liquidacao.Situacao;
                    liquidacaoAbertura.Origem = liquidacao.Origem;
                    liquidacaoAbertura.Fonte = liquidacao.Fonte;
                    liquidacaoAbertura.IdAgente = liquidacao.IdAgente;
                    liquidacaoAbertura.IdentificadorOrigem = liquidacao.IdentificadorOrigem;
                    liquidacaoAbertura.IdConta = liquidacao.IdConta;
                    //
                    // Attach the object
                    liquidacaoAberturaCollection.AttachEntity(liquidacaoAbertura);
                    #endregion
                    //
                    break;
                // Mesmo Tratamento para Saldo Abertura e SaldoFechamento
                case (int)EnumTipoSaldo.SaldoAbertura:
                case (int)EnumTipoSaldo.SaldoFechamento:
                    #region Trata ContaCorrente
                    int? idConta = null;

                    if (valoresExcel.IdConta.HasValue)
                    {
                        idConta = valoresExcel.IdConta.Value;
                    }
                    else
                    {
                        ContaCorrenteCollection c = new ContaCorrenteCollection();
                        bool ret = c.BuscaContaCorrente(valoresExcel.IdCliente);

                        if (ret != true)
                        {
                            throw new Exception("Conta não cadastrada para o cliente: " + valoresExcel.IdCliente);
                        }
                        else
                        {
                            idConta = c[0].IdConta.Value;
                        }
                    }
                    #endregion

                    SaldoCaixa saldoCaixa = new SaldoCaixa();
                    // Se não existe no banco
                    if (!saldoCaixa.LoadByPrimaryKey(valoresExcel.IdCliente, valoresExcel.DataLancamento, idConta.Value)) {
                        // Se não existe na collection
                        SaldoCaixa saldoCaixaAux = saldoCaixaCollection.FindByPrimaryKey(valoresExcel.IdCliente, valoresExcel.DataLancamento, idConta.Value);
                        if (saldoCaixaAux == null) {
                            #region Inserindo uma nova entity
                            // Insert de uma nova entity
                            saldoCaixa = new SaldoCaixa();
                            saldoCaixa.IdCliente = valoresExcel.IdCliente;
                            saldoCaixa.IdConta = idConta;
                            saldoCaixa.Data = valoresExcel.DataLancamento;

                            if (tipoSaldo == EnumTipoSaldo.SaldoAbertura) {
                                saldoCaixa.SaldoAbertura = valoresExcel.Valor;
                                //saldoCaixa.SaldoFechamento = null;
                            }
                            else if (tipoSaldo == EnumTipoSaldo.SaldoFechamento) {
                                saldoCaixa.SaldoAbertura = 0;
                                saldoCaixa.SaldoFechamento = valoresExcel.Valor;
                            }
                            // Attach the object
                            saldoCaixaCollection.AttachEntity(saldoCaixa);
                            #endregion
                        }
                        // Se existe na collection
                        else {
                            #region Atualizando na propria collection
                            // Remove a entidade e insere outra na collection
                            saldoCaixaCollection.DetachEntity(saldoCaixaAux);

                            // Atualizo na propria collection
                            if (tipoSaldo == EnumTipoSaldo.SaldoAbertura) {
                                saldoCaixaAux.SaldoAbertura = valoresExcel.Valor;
                            }
                            else if (tipoSaldo == EnumTipoSaldo.SaldoFechamento) {
                                saldoCaixaAux.SaldoFechamento = valoresExcel.Valor;
                            }

                            // Attach o objeto
                            saldoCaixaCollection.AttachEntity(saldoCaixaAux);
                            #endregion
                        }
                    }
                    // Se ja existe no banco
                    else {
                        // E não existe na collection
                        SaldoCaixa saldoCaixaAux1 = saldoCaixaCollection.FindByPrimaryKey(valoresExcel.IdCliente, valoresExcel.DataLancamento, idConta.Value);
                        if (saldoCaixaAux1 == null) {
                            #region Attach da entity
                            //
                            if (tipoSaldo == EnumTipoSaldo.SaldoAbertura) {
                                saldoCaixa.SaldoAbertura = valoresExcel.Valor;
                            }
                            else if (tipoSaldo == EnumTipoSaldo.SaldoFechamento) {
                                saldoCaixa.SaldoFechamento = valoresExcel.Valor;
                            }
                            // Attach the object
                            saldoCaixaCollection.AttachEntity(saldoCaixa);
                            #endregion
                        }
                        // Se existe na collection
                        else {
                            #region Atualizando na propria collection
                            // Remove a entidade e insere outra na collection
                            saldoCaixaCollection.DetachEntity(saldoCaixaAux1);

                            // Atualizo na propria collection
                            if (tipoSaldo == EnumTipoSaldo.SaldoAbertura) {
                                saldoCaixaAux1.SaldoAbertura = valoresExcel.Valor;
                            }
                            else if (tipoSaldo == EnumTipoSaldo.SaldoFechamento) {
                                saldoCaixaAux1.SaldoFechamento = valoresExcel.Valor;
                            }

                            // Attach o objeto
                            saldoCaixaCollection.AttachEntity(saldoCaixaAux1);
                            #endregion
                        }
                    }
                    break;
            }
        }

        #region Salva as Liquidações e os SaldoCaixas
        using (esTransactionScope scope = new esTransactionScope()) {

            if (apagaLiquidacaoCliente)
            {
                #region Deleta Liquidacao por IdCliente
                liquidacaoDeletarCollection.Query.Where(liquidacaoDeletarCollection.Query.IdCliente.In(this.idClienteDeletarLiquidacao),
                                                        liquidacaoDeletarCollection.Query.Origem.Equal(OrigemLancamentoLiquidacao.Outros),
                                                        liquidacaoDeletarCollection.Query.Fonte.Equal((byte)FonteLancamentoLiquidacao.Manual));
                liquidacaoDeletarCollection.Query.Load();
                liquidacaoDeletarCollection.MarkAllAsDeleted();
                liquidacaoDeletarCollection.Save();
                #endregion

                #region Deleta LiquidacaoAbertura por IdCliente/DataHistorico
                for (int i = 0; i < this.idClienteDeletarLiquidacao.Count; i++)
                {
                    liquidacaoAberturaDeletarCollection = new LiquidacaoAberturaCollection();
                    liquidacaoAberturaDeletarCollection.Query
                    .Where(liquidacaoAberturaDeletarCollection.Query.IdCliente == this.idClienteDeletarLiquidacao[i],
                           liquidacaoAberturaDeletarCollection.Query.DataHistorico == this.dataHistoricoDeletarLiquidacao[i],
                           liquidacaoDeletarCollection.Query.Origem == OrigemLancamentoLiquidacao.Outros,
                           liquidacaoDeletarCollection.Query.Fonte == (byte)FonteLancamentoLiquidacao.Manual);
                    liquidacaoAberturaDeletarCollection.Query.Load();
                    liquidacaoAberturaDeletarCollection.MarkAllAsDeleted();
                    liquidacaoAberturaDeletarCollection.Save();
                }
                #endregion
            }

            // Salva as Liquidações presentes no Excel
            liquidacaoCollection.Save();

            // Seta os novos idLiquidacao na LiquidacaoAbertura
            for (int i = 0; i < liquidacaoCollection.Count; i++) {
                liquidacaoAberturaCollection[i].IdLiquidacao = liquidacaoCollection[i].IdLiquidacao;
            }

            // Salva as Liquidações
            liquidacaoAberturaCollection.Save();

            // Salva os Saldo Caixa
            saldoCaixaCollection.Save();
            //
            scope.Complete();
        }
        #endregion
    }

    /// <summary>
    /// Processa a Planilha de Liquidação após ter acabado o Upload
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void uplLiquidacao_FileUploadComplete(object sender, FileUploadCompleteEventArgs e) {
        e.CallbackData = "";

        #region Trata Extensão Válida
        if (!ValoresExcelUtil.isExtensaoExcel(e.UploadedFile.FileName.Trim())) {
            e.CallbackData = "Importação Liquidação - Formato de Arquivo Inválido. Formato permitido: .xls ou .xlsx\n\n";
            return;
        }
        #endregion

        // Stream de bytes com o Conteudo do Arquivo Excel
        Stream sr = e.UploadedFile.FileContent;

        #region Processamento Arquivo Excel

        //Reseta vetor de dados
        this.valoresExcelLiquidacao = new List<ValoresExcelLiquidacao>();
        //
        this.idClienteDeletarLiquidacao = new List<int>();
        this.dataHistoricoDeletarLiquidacao = new List<DateTime>();

        try {
            // Ler Arquivo
            this.LerArquivoLiquidacao(sr);
            // Carrega Arquivo
            this.CarregaLiquidacao();
        }
        catch (Exception e2) {
            e.CallbackData = "Importação Liquidação - " + e2.Message;
            return;
        }

        new HistoricoLog().InsereHistoricoLog(DateTime.Now, DateTime.Now,
            "Importação Planilha Liquidação",
            HttpContext.Current.User.Identity.Name, UtilitarioWeb.GetIP(Request), "", HistoricoLogOrigem.Outros);

        #endregion
    }
}