﻿using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Financial.Web.Common;
using System.IO;
using Financial.Integracao.Excel.Util;
using Financial.Integracao.Excel;
using EntitySpaces.Interfaces;
using Financial.Common;
using Financial.Bolsa;
using Financial.Bolsa.Enums;
using Financial.Investidor;
using System.Collections.Generic;
using Financial.Investidor.Enums;
using DevExpress.Web;
using Financial.Common.Exceptions;
using Financial.Bolsa.Exceptions;
using Financial.Tributo;
using Financial.Security;
using Financial.Web.Util;
using Financial.Security.Enums;

public partial class ImportacaoBasePage : BasePage {

    /* Estrutura do Excel */
    private List<ValoresExcelApuracaoRendaVariavel> valoresExcelApuracaoRendaVariavel = new List<ValoresExcelApuracaoRendaVariavel>();
    
    /// <summary>
    /// Leitura de um Arquivo Excel
    /// Saida: List<ValoresExcelApuracaoRendaVariavel> com os valores armazenados no arquivo
    /// </summary>
    /// <param name="streamExcel">Stream de bytes de um arquivo Excel</param>
    /// <exception cref="Exception">Se ocorreu problemas</exception>
    private void LerArquivoApuracaoRendaVariavel(Stream streamExcel) {

        #region Leitura do Arquivo Excel
        // Open Spreadsheet
        Bytescout.Spreadsheet.Spreadsheet document = new Bytescout.Spreadsheet.Spreadsheet();
        document.LoadFromStream(streamExcel);

        Bytescout.Spreadsheet.Worksheet workSheet = document.Workbook.Worksheets[0];
        //
        #region Confere Formato Arquivo
        string[] colunasConferencias = new string[] { "IDCliente", "Ano", "Mes", "PrejuizoCompensarNormal", "PrejuizoCompensarDT", "IRFonteCompensar", "PrejuizoCompensarFII" };

        bool formato = ValoresExcelUtil.isExcelFormatoValido(colunasConferencias, workSheet);
        if (!formato) {
            string mensagem = "Formato Interno do Arquivo Inválido.\n";
            mensagem += "Linha 1 do Arquivo deve ser: \n";
            for (int i = 0; i < colunasConferencias.Length; i++) {
                int coluna = i + 1;
                mensagem += "\tColuna " + coluna + ": " + colunasConferencias[i] + "\n";
            }

            document.Close();
            document.Dispose();

            throw new Exception(mensagem);
        }
        #endregion

        int index = 0;
        // This row, column index should be changed as per your need.
        // i.e. which cell in the excel you are interesting to read.
        //
        /* Formato: 1)IdCotista - 2)IdCarteira - 3)Data - 4)ValorPrejuizo - 5)DataLimiteCompensação
         */

        int linha = 1;
        int coluna1 = 0, coluna2 = 1, coluna3 = 2,
            coluna4 = 3, coluna5 = 4, coluna6 = 5, coluna7 = 6;
        //            
        try {
            // Enquanto idCliente tiver valor
            while (workSheet.Cell(linha, coluna1).Value != null) {
                ValoresExcelApuracaoRendaVariavel item = new ValoresExcelApuracaoRendaVariavel();
                
                try {
                    item.IdCliente = workSheet.Cell(linha, coluna1).ValueAsInteger;
                }
                catch (Exception ex) {
                    throw new Exception(ex.Message + " - IdCliente: linha " + (linha + 1));
                }

                if (workSheet.Cell(linha, coluna2).Value == null)
                    throw new Exception("Ano não informado: linha " + (linha + 1));
                try
                {
                    item.Ano = workSheet.Cell(linha, coluna2).ValueAsInteger;
                }
                catch (Exception ex) {
                    throw new Exception(ex.Message + " - Ano: linha " + (linha + 1));
                }

                if (workSheet.Cell(linha, coluna3).Value == null)
                    throw new Exception("Mes não informado: linha " + (linha + 1));
                try
                {
                    item.Mes = workSheet.Cell(linha, coluna3).ValueAsInteger;
                }
                catch (Exception ex) {
                    throw new Exception(ex.Message + " - Mes: linha " + (linha + 1));
                }

                if (workSheet.Cell(linha, coluna4).Value == null)
                    throw new Exception("PrejuizoCompensarNormal não informado: linha " + (linha + 1));
                try
                {
                    item.PrejuizoCompensarNormal = Convert.ToDecimal(workSheet.Cell(linha, coluna4).Value);
                }
                catch (Exception ex) {
                    throw new Exception(ex.Message + " - PrejuizoCompensarNormal: linha " + (linha + 1));
                }

                if (workSheet.Cell(linha, coluna5).Value == null)
                    throw new Exception("PrejuizoCompensarDT não informado: linha " + (linha + 1));
                try
                {
                    item.PrejuizoCompensarDT = Convert.ToDecimal(workSheet.Cell(linha, coluna5).Value);
                }
                catch (Exception ex) {
                    throw new Exception(ex.Message + " - PrejuizoCompensarDT: linha " + (linha + 1));
                }

                if (workSheet.Cell(linha, coluna6).Value == null)
                    throw new Exception("IRFonteCompensar não informado: linha " + (linha + 1));
                try
                {
                    item.IRFonteCompensar = Convert.ToDecimal(workSheet.Cell(linha, coluna6).Value);
                }
                catch (Exception ex) {
                    throw new Exception(ex.Message + " - IRFonteCompensar: linha " + (linha + 1));
                }

                if (workSheet.Cell(linha, coluna7).Value == null)
                    throw new Exception("PrejuizoCompensarFII não informado: linha " + (linha + 1));
                try
                {
                    item.PrejuizoCompensarFII = Convert.ToDecimal(workSheet.Cell(linha, coluna7).Value);
                }
                catch (Exception ex) {
                    throw new Exception(ex.Message + " - PrejuizoCompensarFII: linha " + (linha + 1));
                }

                this.valoresExcelApuracaoRendaVariavel.Add(item);
                //
                index++;
                linha = 1 + index;
                //                
                Console.WriteLine("{0},{1},{2},{3},{4},{5},{6}",
                    item.IdCliente, item.Ano, item.Mes, item.PrejuizoCompensarNormal, item.PrejuizoCompensarDT, item.IRFonteCompensar, item.PrejuizoCompensarFII);
            }
        }
        catch (Exception ex) {
            document.Close();
            document.Dispose();

            throw new Exception(ex.Message);
        }
        #endregion

        document.Close();
        document.Dispose();
    }

    /// <summary>
    /// Gera e Carrega Os Prejuizos de Renda Variavel de Acordo com o objeto List<ValoresExcelApuracaoRendaVariavel>
    /// </summary>
    /// <exception cref="Exception">Se ocorreu problemas</exception>
    private void CarregaApuracaoRendaVariavel() {
        ApuracaoRendaVariavelIRCollection apuracaoRendaVariavelIRCollection = new ApuracaoRendaVariavelIRCollection();
        ApuracaoIRImobiliarioCollection apuracaoIRImobiliarioCollection = new ApuracaoIRImobiliarioCollection();
        //
        int contUpdate = 0;
        for (int i = 0; i < this.valoresExcelApuracaoRendaVariavel.Count; i++) {
            ValoresExcelApuracaoRendaVariavel valoresExcel = this.valoresExcelApuracaoRendaVariavel[i];
            //

            #region Verifica se Existe Cliente
            Cliente cliente = new Cliente();
            if (!cliente.LoadByPrimaryKey(valoresExcel.IdCliente)) {
                throw new Exception("Cliente Não existente : " + valoresExcel.IdCliente);
            }
            #endregion

            #region ApuracaoRendaVariavel
            ApuracaoRendaVariavelIR apuracaoRendaVariavelIR = new ApuracaoRendaVariavelIR();
            if (apuracaoRendaVariavelIR.LoadByPrimaryKey(valoresExcel.IdCliente, valoresExcel.Ano, (byte)valoresExcel.Mes)) {
                apuracaoRendaVariavelIR.PrejuizoCompensar = valoresExcel.PrejuizoCompensarNormal;
                apuracaoRendaVariavelIR.PrejuizoCompensarDT = valoresExcel.PrejuizoCompensarDT;
                apuracaoRendaVariavelIR.IRDayTradeCompensar = valoresExcel.IRFonteCompensar;                
                apuracaoRendaVariavelIR.Save();

                contUpdate += 1;
            }
            else {
                ApuracaoRendaVariavelIR apuracaoRendaVariavelIRNovo = apuracaoRendaVariavelIRCollection.AddNew();
                // Dados do Arquivo
                apuracaoRendaVariavelIRNovo.IdCliente = valoresExcel.IdCliente;
                apuracaoRendaVariavelIRNovo.Ano = valoresExcel.Ano;
                apuracaoRendaVariavelIRNovo.Mes = (byte)valoresExcel.Mes;
                apuracaoRendaVariavelIRNovo.PrejuizoCompensar = valoresExcel.PrejuizoCompensarNormal;
                apuracaoRendaVariavelIRNovo.PrejuizoCompensarDT = valoresExcel.PrejuizoCompensarDT;
                apuracaoRendaVariavelIRNovo.IRDayTradeCompensar = valoresExcel.IRFonteCompensar;
            }

            ApuracaoIRImobiliario apuracaoIRImobiliario = new ApuracaoIRImobiliario();
            if (apuracaoIRImobiliario.LoadByPrimaryKey(valoresExcel.IdCliente, valoresExcel.Ano, (byte)valoresExcel.Mes))
            {
                apuracaoIRImobiliario.PrejuizoCompensar = valoresExcel.PrejuizoCompensarFII;
                apuracaoIRImobiliario.Save();

                contUpdate += 1;
            }
            else
            {
                ApuracaoIRImobiliario apuracaoIRImobiliarioNovo = apuracaoIRImobiliarioCollection.AddNew();
                // Dados do Arquivo
                apuracaoIRImobiliarioNovo.IdCliente = valoresExcel.IdCliente;
                apuracaoIRImobiliarioNovo.Ano = valoresExcel.Ano;
                apuracaoIRImobiliarioNovo.Mes = (byte)valoresExcel.Mes;
                apuracaoIRImobiliarioNovo.PrejuizoCompensar = valoresExcel.PrejuizoCompensarFII;
                apuracaoIRImobiliarioNovo.PrejuizoCompensarDT = 0;
                apuracaoIRImobiliarioNovo.IRDayTradeCompensar = 0;
            }
            #endregion
        }

        // Salva os Prejuizos Cotistas Presentes no Excel
        apuracaoRendaVariavelIRCollection.Save();
        apuracaoIRImobiliarioCollection.Save();
    }

    /// <summary>
    /// Processa a Planilha de Apuração IR após ter acabado o Upload
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void uplApuracaoIR_FileUploadComplete(object sender, FileUploadCompleteEventArgs e) {
        e.CallbackData = "";

        #region Trata Extensão Válida
        if (!ValoresExcelUtil.isExtensaoExcel(e.UploadedFile.FileName.Trim())) {
            e.CallbackData = "Importação Apuração IR - Formato de Arquivo Inválido. Formato permitido: .xls ou .xlsx\n\n";
            return;
        }
        #endregion

        // Stream de bytes com o Conteudo do Arquivo Excel
        Stream sr = e.UploadedFile.FileContent;

        #region Processamento Arquivo Excel

        //Reseta Vetor de Dados
        this.valoresExcelApuracaoRendaVariavel = new List<ValoresExcelApuracaoRendaVariavel>();

        try {
            // Ler Arquivo
            this.LerArquivoApuracaoRendaVariavel(sr);
            // Carrega Arquivo
            this.CarregaApuracaoRendaVariavel();
        }
        catch (Exception e2) {
            e.CallbackData = "Importação Apuração IR - " + e2.Message;
            return;
        }

        new HistoricoLog().InsereHistoricoLog(DateTime.Now, DateTime.Now,
            "Importação Planilha Apuração IR",
            HttpContext.Current.User.Identity.Name, UtilitarioWeb.GetIP(Request), "", HistoricoLogOrigem.Outros);

        #endregion
    }
}