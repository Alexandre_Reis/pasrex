﻿using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Financial.Web.Common;
using System.IO;
using Financial.Integracao.Excel.Util;
using Financial.Integracao.Excel;
using EntitySpaces.Interfaces;
using Financial.Common;
using Financial.Bolsa;
using Financial.Bolsa.Enums;
using Financial.Investidor;
using System.Collections.Generic;
using Financial.Investidor.Enums;
using DevExpress.Web;
using Financial.Common.Exceptions;
using Financial.Bolsa.Exceptions;
using Financial.Fundo;
using Financial.Util;
using Financial.Security;
using Financial.Web.Util;
using Financial.Security.Enums;

public partial class ImportacaoBasePage : BasePage {

    /* Estrutura do Excel */
    private List<ValoresExcelCalculoPerformance> valoresExcelCalculoPerformance = new List<ValoresExcelCalculoPerformance>();
    //
    List<int> idTabelaDeletarCalculoPerformance = new List<int>();
    List<DateTime> dataHistoricoDeletarCalculoPerformance = new List<DateTime>();

    /// <summary>
    /// Leitura de um arquivo Excel
    /// Saida: List<ValoresExcel> com os valores armazenados no arquivo
    /// </summary>
    /// <param name="streamExcel">Stream de bytes de um arquivo Excel</param>
    /// <exception cref="Exception">Se ocorreu problemas</exception>
    private void LerArquivoCalculoPerformance(Stream streamExcel) {

        #region Leitura do Arquivo Excel
        // Open Spreadsheet
        Bytescout.Spreadsheet.Spreadsheet document = new Bytescout.Spreadsheet.Spreadsheet();
        document.LoadFromStream(streamExcel);

        Bytescout.Spreadsheet.Worksheet workSheet = document.Workbook.Worksheets[0];

        #region Confere Formato Arquivo
        string[] colunasConferencias = new string[] {"IdTabela","IdCarteira","ValorAcumulado",
                                            "DataFimApropriacao","DataPagamento","DataUltimoCorte"};

        bool formato = ValoresExcelUtil.isExcelFormatoValido(colunasConferencias, workSheet);
        if (!formato) {
            string mensagem = "Formato Interno do Arquivo Inválido.\n";
            mensagem += "Linha 1 do Arquivo deve ser: \n";
            for (int i = 0; i < colunasConferencias.Length; i++) {
                int coluna = i + 1;
                mensagem += "\tColuna " + coluna + ": " + colunasConferencias[i] + "\n";
            }

            document.Close();
            document.Dispose();

            throw new Exception(mensagem);
        }
        #endregion

        //
        int index = 0;
        // This row,column index should be changed as per your need.
        // i.e. which cell in the excel you are interesting to read.
        //
        /* Formato: 1)IdTabela           - 2)IdCarteira    - 3)ValorAcumulado
         *          4)DataFimApropriacao - 5)DataPagamento - 6)DataUltimoCorte             
         */
        int linha = 1;
        int coluna1 = 0, coluna2 = 1, coluna3 = 2,
            coluna4 = 3, coluna5 = 4, coluna6 = 5;
        //
        try {
            // Enquanto tiver registro
            while (workSheet.Cell(linha, coluna1).Value != null) {
                ValoresExcelCalculoPerformance item = new ValoresExcelCalculoPerformance();
                
                try {
                    item.IdTabela = workSheet.Cell(linha, coluna1).ValueAsInteger;
                }
                catch (Exception ex) {
                    throw new Exception(ex.Message + " - IdTabela: linha " + (linha + 1));
                }

                if (workSheet.Cell(linha, coluna2).Value == null)
                    throw new Exception("IdCarteira não informado: linha " + (linha + 1));
                try
                {
                    item.IdCarteira = workSheet.Cell(linha, coluna2).ValueAsInteger;
                }
                catch (Exception ex) {
                    throw new Exception(ex.Message + " - IdCarteira: linha " + (linha + 1));
                }

                if (workSheet.Cell(linha, coluna3).Value == null)
                    throw new Exception("ValorAcumulado não informado: linha " + (linha + 1));
                try
                {
                    item.ValorAcumulado = Convert.ToDecimal(workSheet.Cell(linha, coluna3).Value);
                }
                catch (Exception ex) {
                    throw new Exception(ex.Message + " - ValorAcumulado: linha " + (linha + 1));
                }

                if (workSheet.Cell(linha, coluna4).Value == null)
                    throw new Exception("DataFimApropriacao não informada: linha " + (linha + 1));
                try
                {
                    item.DataFimApropriacao = workSheet.Cell(linha, coluna4).ValueAsDateTime;
                }
                catch (Exception ex) {
                    throw new Exception(ex.Message + " - DataFimApropriacao: linha " + (linha + 1));
                }

                if (workSheet.Cell(linha, coluna5).Value == null)
                    throw new Exception("DataPagamento não informada: linha " + (linha + 1));
                try
                {
                    item.DataPagamento = workSheet.Cell(linha, coluna5).ValueAsDateTime;
                }
                catch (Exception ex) {
                    throw new Exception(ex.Message + " - DataPagamento: linha " + (linha + 1));
                }

                if (workSheet.Cell(linha, coluna6).Value == null)
                    throw new Exception("DataUltimoCorte não informada: linha " + (linha + 1));
                try
                {
                    item.DataUltimoCorte = workSheet.Cell(linha, coluna6).ValueAsDateTime;
                }
                catch (Exception ex) {
                    throw new Exception(ex.Message + " - DataUltimoCorte: linha " + (linha + 1));
                }

                //
                this.valoresExcelCalculoPerformance.Add(item);
                //
                index++;
                linha = 1 + index;
                //                
                Console.WriteLine("{0},{1},{2},{3},{4},{5}", item.IdTabela, item.IdCarteira, item.ValorAcumulado,
                                    item.DataFimApropriacao.ToString("dd/MM/yyyy"),
                                    item.DataPagamento.ToString("dd/MM/yyyy"),
                                    item.DataUltimoCorte.ToString("dd/MM/yyyy"));
            }
        }
        catch (Exception ex) {
            document.Close();
            document.Dispose();

            throw new Exception(ex.Message);
        }
        #endregion

        document.Close();
        document.Dispose();
    }

    /// <summary>
    /// Gera e Carrega os Calculos Performance e Performance Histórico
    /// com o objeto List<ValoresExcel>
    /// </summary>
    /// <exception cref="Exception">Se ocorreu problemas</exception>
    private void CarregaCalculoPerformance() {
        CalculoPerformanceCollection calculoPerformanceCollection = new CalculoPerformanceCollection();
        CalculoPerformanceCollection calculoPerformanceDeletarCollection = new CalculoPerformanceCollection();
        //
        CalculoPerformanceHistoricoCollection calculoPerformanceHistoricoCollection = new CalculoPerformanceHistoricoCollection();
        CalculoPerformanceHistoricoCollection calculoPerformanceHistoricoDeletarCollection = new CalculoPerformanceHistoricoCollection();
        //    
        for (int i = 0; i < this.valoresExcelCalculoPerformance.Count; i++) {
            ValoresExcelCalculoPerformance valoresExcel = this.valoresExcelCalculoPerformance[i];
            // Salva o IdTabela para poder Deletar
            this.idTabelaDeletarCalculoPerformance.Add(valoresExcel.IdTabela);
            //               
            #region Verifica se Existe Carteira
            Carteira carteira = new Carteira();
            if (!carteira.LoadByPrimaryKey(valoresExcel.IdCarteira)) {
                throw new Exception("Carteira Não existente : " + valoresExcel.IdCarteira);
            }
            #endregion

            #region Verifica se Existe idTabela
            TabelaTaxaPerformance tabelaTaxaPerformance = new TabelaTaxaPerformance();
            tabelaTaxaPerformance.Query
                  .Where(tabelaTaxaPerformance.Query.IdTabela == valoresExcel.IdTabela &
                        tabelaTaxaPerformance.Query.IdCarteira == valoresExcel.IdCarteira);

            if (!tabelaTaxaPerformance.Query.Load()) {
                throw new Exception("Tabela Taxa Performance: " + valoresExcel.IdTabela + " não existente para a carteira " + valoresExcel.IdCarteira);
            }
            #endregion

            #region pega o Cliente
            Cliente cliente = new Cliente();
            List<esQueryItem> campos = new List<esQueryItem>();
            campos.Add(cliente.Query.IdCliente);
            campos.Add(cliente.Query.DataDia);
            cliente.LoadByPrimaryKey(campos, valoresExcel.IdCarteira);
            // Salva a Data Historico para poder Deletar
            DateTime dataAuxiliar = Calendario.SubtraiDiaUtil(cliente.DataDia.Value, 1);
            this.dataHistoricoDeletarCalculoPerformance.Add(dataAuxiliar);
            #endregion

            #region CalculoPerformance
            //
            CalculoPerformance calculoPerformance = new CalculoPerformance();
            calculoPerformance.IdTabela = valoresExcel.IdTabela;
            calculoPerformance.IdCarteira = valoresExcel.IdCarteira;
            calculoPerformance.ValorAcumulado = valoresExcel.ValorAcumulado;
            calculoPerformance.DataFimApropriacao = valoresExcel.DataFimApropriacao;
            calculoPerformance.DataPagamento = valoresExcel.DataPagamento;            
            //
            calculoPerformance.ValorCPMFAcumulado = 0;

            // Attach the entity
            calculoPerformanceCollection.AttachEntity(calculoPerformance);

            #endregion
            //
            #region CalculoPerformanceHistórico
            //
            CalculoPerformanceHistorico calculoPerformanceHistorico = new CalculoPerformanceHistorico();
            calculoPerformanceHistorico.DataHistorico = dataAuxiliar;
            calculoPerformanceHistorico.IdTabela = calculoPerformance.IdTabela;
            calculoPerformanceHistorico.IdCarteira = calculoPerformance.IdCarteira;
            calculoPerformanceHistorico.ValorAcumulado = calculoPerformance.ValorAcumulado;
            calculoPerformanceHistorico.DataFimApropriacao = calculoPerformance.DataFimApropriacao;
            calculoPerformanceHistorico.DataPagamento = calculoPerformance.DataPagamento;
            //
            calculoPerformanceHistorico.ValorCPMFAcumulado = calculoPerformance.ValorCPMFAcumulado;

            // Attach the object
            calculoPerformanceHistoricoCollection.AttachEntity(calculoPerformanceHistorico);
            #endregion
        }

        using (esTransactionScope scope = new esTransactionScope()) {

            #region Deleta CalculoPerformance por Idtabela
            calculoPerformanceDeletarCollection.Query.Where(calculoPerformanceDeletarCollection.Query.IdTabela.In(this.idTabelaDeletarCalculoPerformance));
            calculoPerformanceDeletarCollection.Query.Load();
            calculoPerformanceDeletarCollection.MarkAllAsDeleted();
            calculoPerformanceDeletarCollection.Save();
            #endregion

            #region Deleta CalculoPerformanceHistorico por IdTabela/DataHistorico
            for (int i = 0; i < this.idTabelaDeletarCalculoPerformance.Count; i++) {
                calculoPerformanceHistoricoDeletarCollection = new CalculoPerformanceHistoricoCollection();
                calculoPerformanceHistoricoDeletarCollection.Query
                    .Where(calculoPerformanceHistoricoDeletarCollection.Query.IdTabela == this.idTabelaDeletarCalculoPerformance[i],
                           calculoPerformanceHistoricoDeletarCollection.Query.DataHistorico == this.dataHistoricoDeletarCalculoPerformance[i]);
                calculoPerformanceHistoricoDeletarCollection.Query.Load();
                calculoPerformanceHistoricoDeletarCollection.MarkAllAsDeleted();
                calculoPerformanceHistoricoDeletarCollection.Save();
            }
            #endregion

            // Salva Calculo Provisão presente no Excel
            calculoPerformanceCollection.Save();

            // Salva Calculo Provisão Historico presente no Excel
            calculoPerformanceHistoricoCollection.Save();
            //
            scope.Complete();
        }
    }

    /// <summary>
    /// Processa a Planilha de Cálculo Performance após ter acabado o Upload
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void uplCalculoPerformance_FileUploadComplete(object sender, FileUploadCompleteEventArgs e) {
        e.CallbackData = "";

        #region Trata Extensão Válida
        if (!ValoresExcelUtil.isExtensaoExcel(e.UploadedFile.FileName.Trim())) {
            e.CallbackData = "Importação Cálculo Performance - Formato de Arquivo Inválido. Formato permitido: .xls ou .xlsx\n\n";
            return;
        }
        #endregion

        // Stream de bytes com o Conteudo do Arquivo Excel
        Stream sr = e.UploadedFile.FileContent;

        #region Processamento Arquivo Excel

        //Reseta vetor de dados
        this.valoresExcelCalculoPerformance = new List<ValoresExcelCalculoPerformance>();
        this.idTabelaDeletarCalculoPerformance = new List<int>();
        this.dataHistoricoDeletarCalculoPerformance = new List<DateTime>();

        try {
            // Ler Arquivo
            this.LerArquivoCalculoPerformance(sr);
            // Carrega Arquivo
            this.CarregaCalculoPerformance();
        }
        catch (Exception e2) {
            e.CallbackData = "Importação Cálculo Performance - " + e2.Message;
            return;
        }

        new HistoricoLog().InsereHistoricoLog(DateTime.Now, DateTime.Now,
            "Importação Planilha Cálculo Performance",
            HttpContext.Current.User.Identity.Name, UtilitarioWeb.GetIP(Request), "", HistoricoLogOrigem.Outros);

        #endregion
    }
}