﻿using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Web.UI.HtmlControls;
using System.Collections.Generic;
using System.Text;
using Financial.Web.Common;
using Financial.Interfaces.Import.RendaFixa;
using Financial.InvestidorCotista;
using Financial.Fundo;
using DevExpress.Web;
using Financial.WebConfigConfiguration;
using Dart.PowerTCP.Zip;
using Financial.Interfaces.Import.Tema;
using Financial.Bolsa;

public partial class ImportacaoBasePage : BasePage
{

    class ArquivosIRTema
    {

        /* Lista de arquivos de Saldos */
        public List<FileInfo> fileSaldos = new List<FileInfo>();

        /* Lista de arquivos de Apuração Mensal */
        public List<FileInfo> fileApuracaoMensais = new List<FileInfo>();

        /* Lista de arquivos de Nota Corretagem */
        public List<FileInfo> fileNotasCorretagem = new List<FileInfo>();

        /* Lista de arquivos de Movimentos */
        public List<FileInfo> fileMovimentos = new List<FileInfo>();
    }

    private ArquivosIRTema listaArquivos = new ArquivosIRTema();

    /// <summary>
    /// Processa arquivo Zip de IRTema
    /// O arquivo zip pode conter 4 tipos de arquivos: Saldos.dat, Apuracao.dat, NotasCor.dat, Movimento.dat
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void uplIRTema_FileUploadComplete(object sender, FileUploadCompleteEventArgs e)
    {
        e.CallbackData = "";

        #region Trata Extensão Válida

        if (!this.isExtensaoZIP(e.UploadedFile.FileName.Trim()))
        {
            e.CallbackData = "Arquivo Calculadora IR inválido: Extensão do arquivo deve ser .zip. \n\n";
            return;
        }
        #endregion

        #region Trata path do Arquivo zip
        string path = DiretorioAplicacao.DiretorioBaseAplicacao + WebConfig.AppSettings.DiretorioDownloads;
        if (!path.Trim().EndsWith("\\"))
        {
            path += "\\";
        }
        path = path.Replace("\\", "\\\\");
        #endregion

        // Stream de bytes com o Conteudo do Arquivo Zip
        Stream sr = e.UploadedFile.FileContent;

        #region Processamento Arquivo .zip

        // Descompacta
        Archive arquivo = new Archive();
        arquivo.PreservePath = true;
        arquivo.Clear();
        arquivo.Overwrite = Overwrite.Always;

        string login = HttpContext.Current.User.Identity.Name;
        path += login + @"_CalculadoraIR\";

        // Se Diretorio Existe Apaga o Diretorio
        if (Directory.Exists(path))
        {
            Directory.Delete(path, true);
        }

        // Cria o Diretorio
        if (!Directory.Exists(path))
        {
            Directory.CreateDirectory(path);
        }

        string arqErro = "";

        try
        {
            arquivo.QuickUnzip(sr, path);

            DirectoryInfo dir = new DirectoryInfo(path);
            //Console.WriteLine("Directory: {0}", dir.FullName);

            foreach (FileInfo file in dir.GetFiles("*.dat"))
            {
                string fileName = file.Name.Trim().ToLower();

                #region Saldos
                if (fileName.Contains("saldos"))
                {
                    listaArquivos.fileSaldos.Add(file);
                }
                #endregion

                #region Apuração
                else if (fileName.Contains("apuracao"))
                {
                    listaArquivos.fileApuracaoMensais.Add(file);
                }
                #endregion

                #region Notas Corretagem
                else if (fileName.Contains("notascor"))
                {
                    listaArquivos.fileNotasCorretagem.Add(file);
                }
                #endregion

                #region Movimento
                else if (fileName.Contains("movimento"))
                {
                    listaArquivos.fileMovimentos.Add(file);
                }
                #endregion
            }

            Tema t = new Tema();

            /* Dicionario com tres chaves que contem o conteudo dos arquivos Tema
            ["Movimento"] = lista de Arquivos de Movimento
            ["Mensal"] = lista de Arquivos de ApuracaoMensais
            ["NotaCorretagem"] = lista de Arquivos de NotaCorretagem
            ["HeaderNotaCorretagem"] = lista com os headers dos Arquivos de NotaCorretagem
            */
            Dictionary<string, Object> dicArquivos = new Dictionary<string, Object>();

            /* Processamento dos Arquivos */
            for (int i = 0; i < listaArquivos.fileSaldos.Count; i++)
            {
                //arqErro = listaArquivos.fileSaldos[i].Name;
                //string name = listaArquivos.fileSaldos[i].FullName;

                //using (StreamReader srReader = new StreamReader(name, Encoding.GetEncoding("ISO-8859-1"))) {
                //    Tema.Saldos[] saldos = t.ImportaSaldosTema(srReader);
                //}
            }

            List<Tema.Movimento> movAux = new List<Tema.Movimento>(); // para impressao

            List<Tema.Movimento[]> listaMovimentos = new List<Tema.Movimento[]>();
            for (int i = 0; i < listaArquivos.fileMovimentos.Count; i++)
            {
                arqErro = listaArquivos.fileMovimentos[i].Name;
                string name = listaArquivos.fileMovimentos[i].FullName;

                using (StreamReader srReader = new StreamReader(name, Encoding.GetEncoding("ISO-8859-1")))
                {
                    Tema.Movimento[] movimentos = t.ImportaMovimentoTema(srReader);
                    listaMovimentos.Add(movimentos);

                    //
                    movAux.AddRange(movimentos);
                }
            }
            dicArquivos.Add("Movimento", listaMovimentos);


            List<Tema.Apuracao> apuracaoAux = new List<Tema.Apuracao>(); // para impressao

            List<Tema.Apuracao[]> listaApuracaoMensal = new List<Tema.Apuracao[]>();
            for (int i = 0; i < listaArquivos.fileApuracaoMensais.Count; i++)
            {
                arqErro = listaArquivos.fileApuracaoMensais[i].Name;
                string name = listaArquivos.fileApuracaoMensais[i].FullName;

                using (StreamReader srReader = new StreamReader(name, Encoding.GetEncoding("ISO-8859-1")))
                {
                    Tema.Apuracao[] apuracao = t.ImportaApuracaoTema(srReader);

                    listaApuracaoMensal.Add(apuracao);

                    //
                    apuracaoAux.AddRange(apuracao); // para impressao
                }
            }
            dicArquivos.Add("Mensal", listaApuracaoMensal);

            List<List<Tema.DataHolderNotaCorretagem>> listaNotaCorretagem = new List<List<Tema.DataHolderNotaCorretagem>>();
            List<List<Tema.HeaderNotasCorretagem>> listaHeaderNotaCorretagem = new List<List<Tema.HeaderNotasCorretagem>>();
            //

            List<Object> notasAux = new List<Object>(); // para impressao
            //
            for (int i = 0; i < listaArquivos.fileNotasCorretagem.Count; i++)
            {
                arqErro = listaArquivos.fileNotasCorretagem[i].Name;
                string name = listaArquivos.fileNotasCorretagem[i].FullName;

                using (StreamReader srReader = new StreamReader(name, Encoding.GetEncoding("ISO-8859-1")))
                {
                    object[] notas = t.ImportaNotaCorretagemTema(srReader);


                    notasAux.AddRange(notas); // para impressao

                    //
                    List<Tema.DataHolderNotaCorretagem> lista = t.TransformaNotaCorretagem(notas);
                    listaNotaCorretagem.Add(lista);
                    //

                    List<Tema.HeaderNotasCorretagem> headers = t.GetHeaderNotaCorretagem(notas);
                    listaHeaderNotaCorretagem.Add(headers);
                }
            }

            dicArquivos.Add("NotaCorretagem", listaNotaCorretagem);
            dicArquivos.Add("HeaderNotaCorretagem", listaHeaderNotaCorretagem);



            #region Codigo para Impressão - Debug

            DevExpress.XtraPrinting.XlsxExportOptions a = new DevExpress.XtraPrinting.XlsxExportOptions();
            //a.Suppress256ColumnsWarning = true;
            //           
            ASPxGridView grid = Page.FindControl("gridExportacao1") as ASPxGridView;
            ASPxGridViewExporter gridExport = Page.FindControl("gridExport") as ASPxGridViewExporter;

            //grid.DataSource = movAux;
            //gridExport.WriteXlsToResponse("Grid", false, a);


            /* -----------------------------------------*/

            //grid.DataSource = apuracaoAux;
            //gridExport.WriteXlsToResponse("GridApuracao", false, a);

            /* -----------------------------------------*/

            //            grid.DataSource = notasAux;
            //            gridExport.WriteXlsxToResponse("Grid2", false, a);

            #endregion

            //
            new OperacaoBolsa().IntegraOperacaoTema(dicArquivos);
        }
        catch (Exception e1)
        {
            string msg = "\tArquivo Calculadora IR com problemas: Arquivo " + arqErro + " \n";
            msg += " - Dados não Importados: " + e1.Message;

            e.CallbackData = msg;
            return;
        }
        finally
        {
            // Apaga o Diretorio e todos os arquivos criados no Unzip
            if (Directory.Exists(path))
            {
                Directory.Delete(path, true);
            }

            sr.Close();
        }
        #endregion
    }
}