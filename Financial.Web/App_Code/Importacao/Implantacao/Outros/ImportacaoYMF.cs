﻿using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Web.UI.HtmlControls;
using System.Collections.Generic;
using System.Text;
using Financial.Web.Common;
using Financial.Interfaces.Import.RendaFixa;
using Financial.InvestidorCotista;
using Financial.Fundo;
using DevExpress.Web;
using Financial.Interfaces.Import.YMF;
using Financial.Investidor;
using Financial.CRM;
using Financial.WebConfigConfiguration;
using Dart.PowerTCP.Zip;
using Financial.InvestidorCotista.Enums;
using System.Collections;
using System.Text.RegularExpressions;

public partial class ImportacaoBasePage : BasePage
{
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    /// <param name="isCotista">Indica se é Cotista ou Fundo</param>
    public void uplYMF_FileUploadComplete(object sender, FileUploadCompleteEventArgs e)
    {
        e.CallbackData = "";

        #region Trata Extensão Válida

        if (!this.isExtensaoZIP(e.UploadedFile.FileName.Trim()))
        {
            if (e.UploadedFile.FileName == "Posicao da Nota Historica.txt")
            {
                try
                {
                    ImportaPosicaoNotaHistorica(e.UploadedFile);
                }
                catch (Exception erroPosicaoNotaHistorica)
                {
                    e.CallbackData = "Importação YMF - " + erroPosicaoNotaHistorica.Message;
                    return;
                }
                return;
            }
            else
            {
                e.CallbackData = "Arquivo Cliente YMF inválido: Extensão do arquivo deve ser .zip ou .txt\n\n";
                return;
            }
        }
        #endregion

        #region Trata path do Arquivo zip
        string path = DiretorioAplicacao.DiretorioBaseAplicacao + WebConfig.AppSettings.DiretorioDownloads;
        if (!path.Trim().EndsWith("\\"))
        {
            path += "\\";
        }
        path = path.Replace("\\", "\\\\");
        #endregion

        #region Arquivo Zip
        string nomeArquivo = e.UploadedFile.FileName.Trim();
        string extensao = new FileInfo(nomeArquivo).Extension;
        extensao = extensao.ToLower();

        // Descompacta
        Archive arquivo = new Archive();
        arquivo.PreservePath = true;
        arquivo.Clear();
        arquivo.Overwrite = Overwrite.Always;

        string login = HttpContext.Current.User.Identity.Name;
        path += login + @"\";

        // Se Diretorio Existe Apaga o Diretorio
        if (Directory.Exists(path))
        {
            Directory.Delete(path, true);
        }

        // Cria o Diretorio
        if (!Directory.Exists(path))
        {
            Directory.CreateDirectory(path);
        }

        Stream zipStream = e.UploadedFile.FileContent;
        try
        {
            arquivo.QuickUnzip(zipStream, path);

            PessoaCollection pessoaCollectionImportada;
            using (StreamReader streamReader = new StreamReader(path + "Cliente.txt", Encoding.GetEncoding("ISO-8859-1")))
            {
                pessoaCollectionImportada = this.ProcessaArquivoClienteYMF(streamReader);
            }

            using (StreamReader streamReader = new StreamReader(path + "Endereço.txt", Encoding.GetEncoding("ISO-8859-1")))
            {
                this.ProcessaArquivoEnderecoYMF(streamReader);
            }

            CotistaCollection cotistaCollectionImportada;
            using (StreamReader streamReader = new StreamReader(path + "Cotista.txt", Encoding.GetEncoding("ISO-8859-1")))
            {
                cotistaCollectionImportada = this.ProcessaArquivoCotistaYMF(streamReader);
            }

            //Rotina de importação de arquivos de fundos mapeados para cliente e carteira
            Carteira fundoImportado = this.ImportaFundoYMF(path);

            //Vamos zerar todsa a tabelas de posicoes, operacoes, historicos cota, 
            //resgates relacionadas ao fundo importado para nao duplicar em uma reimportacao
            this.ApagaRegistrosFundo(fundoImportado);

            //Importar historico de cota
            using (StreamReader streamReader = new StreamReader(path + "Posição do Fundo.txt", Encoding.GetEncoding("ISO-8859-1")))
            {
                this.ProcessaArquivoPosicaoFundoYMF(streamReader);
            }

            //Pegar o cliente para termos a data dia em que foi implantado (data mais recente do histórico de cotas)
            Cliente cliente = new Cliente();
            cliente.LoadByPrimaryKey(fundoImportado.IdCarteira.Value);

            MappingOperacaoFinancialYMF mappingOperacaoFinancialYMF;
            using (StreamReader streamReader = new StreamReader(path + "Movimento.txt", Encoding.GetEncoding("ISO-8859-1")))
            {
                mappingOperacaoFinancialYMF = this.ProcessaArquivoMovimentoYMF(streamReader);
            }

            //Os caracteres esquisitos no nome do arquivo sao cortesia da nomenclatura da YMF para arquivos
            //exportados com acentos e cedilhas no nome e a incompetencia do Dart Unzip em lidar com acentuação
            MappingPosicaoFinancialYMF mappingPosicaoFinancialYMF;

            using (StreamReader streamReader = new StreamReader(path + "Posição da Nota.txt", Encoding.GetEncoding("ISO-8859-1")))
            {
                mappingPosicaoFinancialYMF = this.ProcessaArquivoPosicaoNotaYMF(streamReader, cliente.DataImplantacao.Value, mappingOperacaoFinancialYMF);
            }

            if (mappingPosicaoFinancialYMF.Count == 0)
            //Se nao importamos ainda nenhuma posicao, tentar encontrar posicoes do dia da implantacao no arquivo de historico
            {
                using (StreamReader streamReader = new StreamReader(path + "Posição da Nota Histórica.txt", Encoding.GetEncoding("ISO-8859-1")))
                {
                    mappingPosicaoFinancialYMF = this.ProcessaArquivoPosicaoNotaYMF(streamReader, cliente.DataImplantacao.Value, mappingOperacaoFinancialYMF);
                }
            }

            using (StreamReader streamReader = new StreamReader(path + "Resgate.txt", Encoding.GetEncoding("ISO-8859-1")))
            {
                this.ProcessaArquivoResgateYMF(streamReader, mappingOperacaoFinancialYMF, mappingPosicaoFinancialYMF);
            }


            using (StreamReader streamReader = new StreamReader(path + "Rendimento a Compensar.txt", Encoding.GetEncoding("ISO-8859-1")))
            {
                this.ProcessaArquivoRendimentoCompensarYMF(streamReader);
            }

            using (StreamReader streamReader = new StreamReader(path + "Tipo de Cotista.txt", Encoding.GetEncoding("ISO-8859-1")))
            {
                this.ProcessaArquivoTipoCotistaYMF(streamReader, cotistaCollectionImportada);
            }

        }
        catch (Exception e3)
        {
            e.CallbackData = "Importação YMF - " + e3.Message;
        }
        finally
        {
            // Apaga o Diretorio e todos os arquivos criados no Unzip
            if (Directory.Exists(path))
            {
                Directory.Delete(path, true);
            }

            zipStream.Close();
        }

        #endregion

    }

    public void uplYMFFIX_FileUploadComplete(object sender, FileUploadCompleteEventArgs e)
    {
        e.CallbackData = "";

        #region Trata Extensão Válida

        if (!this.isExtensaoZIP(e.UploadedFile.FileName.Trim()))
        {
            e.CallbackData = "Arquivo Cliente YMF inválido: Extensão do arquivo deve ser .zip ou .txt\n\n";
            return;
        }
        #endregion

        #region Trata path do Arquivo zip
        string path = DiretorioAplicacao.DiretorioBaseAplicacao + WebConfig.AppSettings.DiretorioDownloads;
        if (!path.Trim().EndsWith("\\"))
        {
            path += "\\";
        }
        path = path.Replace("\\", "\\\\");
        #endregion

        #region Arquivo Zip
        string nomeArquivo = e.UploadedFile.FileName.Trim();
        string extensao = new FileInfo(nomeArquivo).Extension;
        extensao = extensao.ToLower();

        // Descompacta
        Archive arquivo = new Archive();
        arquivo.PreservePath = true;
        arquivo.Clear();
        arquivo.Overwrite = Overwrite.Always;

        string login = HttpContext.Current.User.Identity.Name;
        path += login + @"\";

        // Se Diretorio Existe Apaga o Diretorio
        if (Directory.Exists(path))
        {
            Directory.Delete(path, true);
        }

        // Cria o Diretorio
        DirectoryInfo rootDirInfo;

        rootDirInfo = Directory.CreateDirectory(path);

        Stream zipStream = e.UploadedFile.FileContent;
        try
        {

            arquivo.QuickUnzip(zipStream, path);

            FileInfo[] zips = rootDirInfo.GetFiles();
            int countZip = 1;
            foreach (FileInfo zip in zips)
            {
                string filenameNoExtension = Regex.Replace(zip.Name, ".zip", "", RegexOptions.IgnoreCase);
                string zipExtractPath = path + "\\" + filenameNoExtension + countZip + "\\";
                Directory.CreateDirectory(zipExtractPath);
                arquivo.QuickUnzip(zip.FullName, zipExtractPath);

                try
                {
                    MappingOperacaoFinancialYMF mappingOperacaoFinancialYMF;
                    using (StreamReader streamReader = new StreamReader(zipExtractPath + "Movimento.txt", Encoding.GetEncoding("ISO-8859-1")))
                    {
                        mappingOperacaoFinancialYMF = this.ProcessaArquivoMovimentoYMFFIX2(zipExtractPath, streamReader);
                    }

                    using (StreamReader streamReader = new StreamReader(zipExtractPath + "Resgate.txt", Encoding.GetEncoding("ISO-8859-1")))
                    {
                        this.ProcessaArquivoResgateYMF(streamReader, mappingOperacaoFinancialYMF, null);
                    }
                }
                catch (Exception e4)
                {
                    throw new Exception("Processando " + filenameNoExtension + ":" + e4.Message);
                }

                countZip++;
            }



        }
        catch (Exception e3)
        {
            e.CallbackData = "Importação YMF - " + e3.Message;
        }
        finally
        {
            // Apaga o Diretorio e todos os arquivos criados no Unzip
            if (Directory.Exists(path))
            {
                Directory.Delete(path, true);
            }

            zipStream.Close();
        }

        #endregion

    }

    private void ApagaRegistrosFundo(Carteira fundoImportado)
    {

        PrejuizoCotistaHistoricoCollection prejuizoCotistaHistoricoCollection = new PrejuizoCotistaHistoricoCollection();
        prejuizoCotistaHistoricoCollection.Query.Where(prejuizoCotistaHistoricoCollection.Query.IdCarteira == fundoImportado.IdCarteira);
        prejuizoCotistaHistoricoCollection.Load(prejuizoCotistaHistoricoCollection.Query);
        prejuizoCotistaHistoricoCollection.MarkAllAsDeleted();
        prejuizoCotistaHistoricoCollection.Save();

        PrejuizoCotistaCollection prejuizoCotistaCollection = new PrejuizoCotistaCollection();
        prejuizoCotistaCollection.Query.Where(prejuizoCotistaCollection.Query.IdCarteira == fundoImportado.IdCarteira);
        prejuizoCotistaCollection.Load(prejuizoCotistaCollection.Query);
        prejuizoCotistaCollection.MarkAllAsDeleted();
        prejuizoCotistaCollection.Save();

        DetalheResgateCotistaCollection detalheResgateCotistaCollection = new DetalheResgateCotistaCollection();
        detalheResgateCotistaCollection.Query.Where(detalheResgateCotistaCollection.Query.IdCarteira == fundoImportado.IdCarteira);
        detalheResgateCotistaCollection.Load(detalheResgateCotistaCollection.Query);
        detalheResgateCotistaCollection.MarkAllAsDeleted();
        detalheResgateCotistaCollection.Save();

        HistoricoCotaCollection historicoCotaCollection = new HistoricoCotaCollection();
        historicoCotaCollection.Query.Where(historicoCotaCollection.Query.IdCarteira == fundoImportado.IdCarteira);
        historicoCotaCollection.Load(historicoCotaCollection.Query);
        historicoCotaCollection.MarkAllAsDeleted();
        historicoCotaCollection.Save();

        PosicaoCotistaAberturaCollection posicaoCotistaAberturaCollection = new PosicaoCotistaAberturaCollection();
        posicaoCotistaAberturaCollection.Query.Where(posicaoCotistaAberturaCollection.Query.IdCarteira == fundoImportado.IdCarteira);
        posicaoCotistaAberturaCollection.Load(posicaoCotistaAberturaCollection.Query);
        posicaoCotistaAberturaCollection.MarkAllAsDeleted();
        posicaoCotistaAberturaCollection.Save();

        PosicaoCotistaHistoricoCollection posicaoCotistaHistoricoCollection = new PosicaoCotistaHistoricoCollection();
        posicaoCotistaHistoricoCollection.Query.Where(posicaoCotistaHistoricoCollection.Query.IdCarteira == fundoImportado.IdCarteira);
        posicaoCotistaHistoricoCollection.Load(posicaoCotistaHistoricoCollection.Query);
        posicaoCotistaHistoricoCollection.MarkAllAsDeleted();
        posicaoCotistaHistoricoCollection.Save();

        PosicaoCotistaCollection posicaoCotistaCollection = new PosicaoCotistaCollection();
        posicaoCotistaCollection.Query.Where(posicaoCotistaCollection.Query.IdCarteira == fundoImportado.IdCarteira);
        posicaoCotistaCollection.Load(posicaoCotistaCollection.Query);
        posicaoCotistaCollection.MarkAllAsDeleted();
        posicaoCotistaCollection.Save();

        OperacaoCotistaCollection operacaoCotistaCollection = new OperacaoCotistaCollection();
        operacaoCotistaCollection.Query.Where(operacaoCotistaCollection.Query.IdCarteira == fundoImportado.IdCarteira);
        operacaoCotistaCollection.Load(operacaoCotistaCollection.Query);
        operacaoCotistaCollection.MarkAllAsDeleted();
        operacaoCotistaCollection.Save();
    }

    private void ImportaPosicaoNotaHistorica(UploadedFile file)
    {
        YMF ymf = new YMF();
        using (StreamReader streamReader = new StreamReader(file.FileContent, Encoding.GetEncoding("ISO-8859-1")))
        {
            Financial.Interfaces.Import.YMF.PosicaoNotaYMF[] posicoes = ymf.ImportaPosicaoNotaYMF(streamReader);


            PosicaoCotistaAberturaCollection posicaoCotistaAberturaCollection = new PosicaoCotistaAberturaCollection();
            posicaoCotistaAberturaCollection.ImportaPosicaoNotaHistoricaYMF(posicoes);

            PosicaoCotistaHistoricoCollection posicaoCotistaHistoricoCollection = new PosicaoCotistaHistoricoCollection();
            posicaoCotistaHistoricoCollection.ImportaPosicaoNotaHistoricaYMF(posicoes);
        }
    }

    private PessoaCollection ProcessaArquivoClienteYMF(StreamReader srReader)
    {
        YMF ymf = new YMF();
        Financial.Interfaces.Import.YMF.ClienteYMF[] clientes = ymf.ImportaClienteYMF(srReader);

        PessoaCollection pessoaCollection = new PessoaCollection();
        pessoaCollection.ImportaClienteYMF(clientes);

        return pessoaCollection;
    }

    private PessoaEnderecoCollection ProcessaArquivoEnderecoYMF(StreamReader srReader)
    {
        YMF ymf = new YMF();
        Financial.Interfaces.Import.YMF.EnderecoYMF[] enderecos = ymf.ImportaEnderecoYMF(srReader);

        PessoaEnderecoCollection pessoaEnderecoCollection = new PessoaEnderecoCollection();
        pessoaEnderecoCollection.ImportaEnderecoYMF(enderecos);

        return pessoaEnderecoCollection;
    }

    private CotistaCollection ProcessaArquivoCotistaYMF(StreamReader srReader)
    {
        YMF ymf = new YMF();
        Financial.Interfaces.Import.YMF.CotistaYMF[] cotistas = ymf.ImportaCotistaYMF(srReader);

        CotistaCollection cotistaCollection = new CotistaCollection();
        cotistaCollection.ImportaCotistaYMF(cotistas);

        return cotistaCollection;
    }

    private MappingOperacaoFinancialYMF ProcessaArquivoMovimentoYMF(StreamReader srReader)
    {
        YMF ymf = new YMF();
        Financial.Interfaces.Import.YMF.MovimentoYMF[] movimentos = ymf.ImportaMovimentoYMF(srReader);

        OperacaoCotistaCollection operacaoCotistaCollection = new OperacaoCotistaCollection();
        MappingOperacaoFinancialYMF mappingOperacaoFinancialYMF = operacaoCotistaCollection.ImportaMovimentoYMF(movimentos);

        return mappingOperacaoFinancialYMF;
    }

    private MappingOperacaoFinancialYMF ProcessaArquivoMovimentoYMFFIX(string path, StreamReader srReader)
    {
        YMF ymf = new YMF();

        MovimentoYMF[] movimentos = ymf.ImportaMovimentoYMF(srReader);
        ResgateYMF[] resgates;

        using (StreamReader streamReader2 = new StreamReader(path + "Resgate.txt", Encoding.GetEncoding("ISO-8859-1")))
        {
            resgates = ymf.ImportaResgateYMF(streamReader2);
        }


        MappingOperacaoFinancialYMF mappingOperacaoFinancialYMF = new MappingOperacaoFinancialYMF();


        //Criar mapping por características
        string cdFundoCorrente = "";
        OperacaoCotistaCollection operacoesCorrente;
        DetalheResgateCotistaCollection detalhesResgates = new DetalheResgateCotistaCollection();

        //Criar dictionary para facilitar busca de operacoes
        IDictionary<string, OperacaoCotista> dictOperacoes = new Dictionary<string, OperacaoCotista>();
        IDictionary<string, DetalheResgateCotista> dictDetalhesResgates = new Dictionary<string, DetalheResgateCotista>();


        foreach (Financial.Interfaces.Import.YMF.MovimentoYMF movimento in movimentos)
        {
            if (movimento.CdFundo != cdFundoCorrente)
            {
                if (cdFundoCorrente != "")
                {
                    //Algo estranho ! Tem movimentos de mais de um fundo no arquivo !
                    throw new Exception("Movimentos de mais de um fundo no arquivo: Corrente:" + cdFundoCorrente + "-Novo:" + movimento.CdFundo);
                }

                Cliente cliente = new ClienteCollection().BuscaClientePorCodigoYMF(movimento.CdFundo);
                if (cliente == null)
                {
                    throw new Exception("Não foi possível encontrar o cliente/fundo com códigoYMF: " + movimento.CdFundo);
                }

                //Carregar in-memory operacoes do fundo pra facilitar pesquisa
                operacoesCorrente = new OperacaoCotistaCollection();
                operacoesCorrente.Query.Where(operacoesCorrente.Query.IdCarteira.Equal(cliente.IdCliente.Value));
                operacoesCorrente.Query.Where(operacoesCorrente.Query.TipoOperacao.In((byte)TipoOperacaoCotista.ComeCotas, (byte)TipoOperacaoCotista.ResgateBruto,
                    (byte)TipoOperacaoCotista.ResgateCotas, (byte)TipoOperacaoCotista.ResgateLiquido, (byte)TipoOperacaoCotista.ResgateTotal));
                operacoesCorrente.Load(operacoesCorrente.Query);

                cdFundoCorrente = movimento.CdFundo;


                foreach (OperacaoCotista operacao in operacoesCorrente)
                {
                    string keyOperacao = String.Format("{0}|{1}|{2}|{3}|{4}|{5}|{6}|{7}|{8}",
                        operacao.TipoOperacao, operacao.IdCotista, operacao.TipoResgate, operacao.DataOperacao,
                        operacao.ValorBruto.Value.ToString("#.####"), operacao.ValorIR.Value.ToString("#.####"), operacao.ValorIOF.Value.ToString("#.####"),
                        operacao.ValorLiquido.Value.ToString("#.####"), operacao.Quantidade.Value.ToString("#.####"));
                    if (!dictOperacoes.ContainsKey(keyOperacao))
                    {
                        dictOperacoes.Add(keyOperacao, operacao);
                    }
                }

                //Carregar in-memory detalhes de resgate deste fundo para facilitar pesquisa
                detalhesResgates.Query.Where(detalhesResgates.Query.IdCarteira.Equal(cliente.IdCliente.Value));
                detalhesResgates.Load(detalhesResgates.Query);
                foreach (DetalheResgateCotista detalheResgate in detalhesResgates)
                {
                    string keyDetalheResgate = String.Format("{0}|{1}|{2}|{3}|{4}|{5}|{6}",
                        detalheResgate.IdOperacao,
                        detalheResgate.IdCotista,
                        detalheResgate.Quantidade.Value.ToString("#.####"),
                        detalheResgate.ValorIOF.Value.ToString("#.####"),
                        detalheResgate.ValorBruto.Value.ToString("#.####"),
                        detalheResgate.ValorIR.Value.ToString("#.####"),
                        detalheResgate.ValorLiquido.Value.ToString("#.####"));
                    if (!dictDetalhesResgates.ContainsKey(keyDetalheResgate))
                    {
                        dictDetalhesResgates.Add(keyDetalheResgate, detalheResgate);
                    }
                }

            }

            //Tentar localizar operacao correspondente no financial
            string tipoMovimentoYMF = movimento.CdTipo;
            if (tipoMovimentoYMF == Financial.Interfaces.Import.YMF.Enums.TipoMovimentoYMF.ResgateBruto ||
                tipoMovimentoYMF == Financial.Interfaces.Import.YMF.Enums.TipoMovimentoYMF.ResgateNotaBruto ||
                tipoMovimentoYMF == Financial.Interfaces.Import.YMF.Enums.TipoMovimentoYMF.ResgateCotas ||
                tipoMovimentoYMF == Financial.Interfaces.Import.YMF.Enums.TipoMovimentoYMF.ResgateNotaCotas ||
                tipoMovimentoYMF == Financial.Interfaces.Import.YMF.Enums.TipoMovimentoYMF.ResgateLiquido ||
                tipoMovimentoYMF == Financial.Interfaces.Import.YMF.Enums.TipoMovimentoYMF.ResgateNotaLiquido ||
                tipoMovimentoYMF == Financial.Interfaces.Import.YMF.Enums.TipoMovimentoYMF.ResgateTotal ||
                tipoMovimentoYMF == Financial.Interfaces.Import.YMF.Enums.TipoMovimentoYMF.ResgateNotaTotal ||
                tipoMovimentoYMF == Financial.Interfaces.Import.YMF.Enums.TipoMovimentoYMF.ResgateIR)
            {

                OperacaoCotista operacaoCotistaStatic = new OperacaoCotista();
                byte tipoOperacao = operacaoCotistaStatic.MapTipoMovimentoYMF(movimento.CdTipo);
                Cotista cotista = new CotistaCollection().BuscaCotistaPorCodigoInterface(movimento.CdCotista.Trim());

                byte? tipoResgate = null;
                if (!string.IsNullOrEmpty(movimento.CdCriterioResgate))
                {
                    tipoResgate = operacaoCotistaStatic.MapCriterioResgateMovimentoYMF(movimento.CdCriterioResgate);
                }

                string keyOperacao = String.Format("{0}|{1}|{2}|{3}|{4}|{5}|{6}|{7}|{8}",
                        tipoOperacao, cotista.IdCotista, tipoResgate, movimento.DtMovimento,
                        movimento.VlBruto.Value.ToString("#.####"), movimento.VlIr.Value.ToString("#.####"),
                        movimento.VlIof.Value.ToString("#.####"), movimento.VlLiquido.Value.ToString("#.####"), movimento.QtCotas.Value.ToString("#.####"));

                //Se encontramos a operação no Financial, remover todos os Detalhes de Resgate que foram importados para esta operação
                if (dictOperacoes.ContainsKey(keyOperacao))
                {
                    OperacaoCotista operacaoEncontrada = dictOperacoes[keyOperacao];
                    mappingOperacaoFinancialYMF.Add(movimento.IdNota, operacaoEncontrada.IdOperacao.Value);

                    //Encontrar no arquivo de resgate todos os resgates desta operacao
                    foreach (ResgateYMF resgate in resgates)
                    {
                        if (resgate.IdNota == movimento.IdNota)
                        {
                            string keyDetalheResgate = String.Format("{0}|{1}|{2}|{3}|{4}|{5}|{6}",
                                operacaoEncontrada.IdOperacao,
                                operacaoEncontrada.IdCotista,
                                resgate.QtCotas.ToString("#.####"),
                                resgate.VlIof.ToString("#.####"),
                                resgate.VlBrutoResgate.ToString("#.####"),
                                resgate.VlIr.ToString("#.####"),
                                resgate.VlLiquidoResgate.ToString("#.####"));

                            if (dictDetalhesResgates.ContainsKey(keyDetalheResgate))
                            {
                                //Remover o detalhe
                                DetalheResgateCotista detalheResgate = dictDetalhesResgates[keyDetalheResgate];
                                detalheResgate.MarkAsDeleted();
                                detalhesResgates.Save();
                            }
                        }
                    }
                }
            }
        }

        return mappingOperacaoFinancialYMF;
    }

    private MappingOperacaoFinancialYMF ProcessaArquivoMovimentoYMFFIX2(string path, StreamReader srReader)
    {
        YMF ymf = new YMF();

        MovimentoYMF[] movimentos = ymf.ImportaMovimentoYMF(srReader);
        ResgateYMF[] resgates;

        using (StreamReader streamReader2 = new StreamReader(path + "Resgate.txt", Encoding.GetEncoding("ISO-8859-1")))
        {
            resgates = ymf.ImportaResgateYMF(streamReader2);
        }

        Cliente cliente = null;

        DateTime dataMaxOperacaoArquivo = DateTime.MinValue;
        string cdFundoCorrente = "";
        foreach (Financial.Interfaces.Import.YMF.MovimentoYMF movimento in movimentos)
        {
            if (movimento.CdFundo != cdFundoCorrente)
            {
                if (cdFundoCorrente != "")
                {
                    //Algo estranho ! Tem movimentos de mais de um fundo no arquivo !
                    throw new Exception("Movimentos de mais de um fundo no arquivo: Corrente:" + cdFundoCorrente + "-Novo:" + movimento.CdFundo);
                }

                cliente = new ClienteCollection().BuscaClientePorCodigoYMF(movimento.CdFundo);
                if (cliente == null)
                {
                    throw new Exception("Não foi possível encontrar o cliente/fundo com códigoYMF: " + movimento.CdFundo);
                }
            }

            if (movimento.DtMovimento >= dataMaxOperacaoArquivo)
            {
                dataMaxOperacaoArquivo = movimento.DtMovimento;
            }
        }

        //Remover movimentos resgates com data <= à data maxima do arquivo de 
        OperacaoCotistaCollection operacoesCorrente = new OperacaoCotistaCollection();
        operacoesCorrente.Query.Where(operacoesCorrente.Query.IdCarteira.Equal(cliente.IdCliente.Value));
        operacoesCorrente.Query.Where(operacoesCorrente.Query.DataConversao.LessThan(cliente.DataImplantacao.Value));
        operacoesCorrente.Query.Where(operacoesCorrente.Query.CotaOperacao.Equal(0));
        //operacoesCorrente.Query.Where(operacoesCorrente.Query.Quantidade.GreaterThan(0));
        operacoesCorrente.Query.Where(operacoesCorrente.Query.TipoOperacao.In((byte)TipoOperacaoCotista.ComeCotas, (byte)TipoOperacaoCotista.ResgateBruto,
            (byte)TipoOperacaoCotista.ResgateCotas, (byte)TipoOperacaoCotista.ResgateLiquido, (byte)TipoOperacaoCotista.ResgateTotal));
        operacoesCorrente.Load(operacoesCorrente.Query);

        foreach (OperacaoCotista operacaoCotista in operacoesCorrente)
        {
            DetalheResgateCotistaCollection delDetalhes = new DetalheResgateCotistaCollection();
            delDetalhes.Query.Where(delDetalhes.Query.IdOperacao.Equal(operacaoCotista.IdOperacao.Value));
            delDetalhes.Load(delDetalhes.Query);
            delDetalhes.MarkAllAsDeleted();
            delDetalhes.Save();
        }

        operacoesCorrente.MarkAllAsDeleted();
        operacoesCorrente.Save();

        MappingOperacaoFinancialYMF mappingOperacaoFinancialYMF = new MappingOperacaoFinancialYMF();

        DateTime dataImplantacao = cliente.DataImplantacao.Value;

        foreach (Financial.Interfaces.Import.YMF.MovimentoYMF movimento in movimentos)
        {

            DateTime dataConversao;
            if(movimento.DtLiquidacaoFisica.HasValue){
                dataConversao =  movimento.DtLiquidacaoFisica.Value;
            }else if(movimento.DtLiquidacaoFinanceira.HasValue){
                dataConversao =  movimento.DtLiquidacaoFinanceira.Value;
            }else{
                dataConversao =  movimento.DtMovimento;
            }

            bool isConversaoAnteriorImplantacao = dataConversao < dataImplantacao;

            string tipoMovimentoYMF = movimento.CdTipo;
            bool isResgate = tipoMovimentoYMF == Financial.Interfaces.Import.YMF.Enums.TipoMovimentoYMF.ResgateBruto ||
                tipoMovimentoYMF == Financial.Interfaces.Import.YMF.Enums.TipoMovimentoYMF.ResgateNotaBruto ||
                tipoMovimentoYMF == Financial.Interfaces.Import.YMF.Enums.TipoMovimentoYMF.ResgateCotas ||
                tipoMovimentoYMF == Financial.Interfaces.Import.YMF.Enums.TipoMovimentoYMF.ResgateNotaCotas ||
                tipoMovimentoYMF == Financial.Interfaces.Import.YMF.Enums.TipoMovimentoYMF.ResgateLiquido ||
                tipoMovimentoYMF == Financial.Interfaces.Import.YMF.Enums.TipoMovimentoYMF.ResgateNotaLiquido ||
                tipoMovimentoYMF == Financial.Interfaces.Import.YMF.Enums.TipoMovimentoYMF.ResgateTotal ||
                tipoMovimentoYMF == Financial.Interfaces.Import.YMF.Enums.TipoMovimentoYMF.ResgateNotaTotal ||
                tipoMovimentoYMF == Financial.Interfaces.Import.YMF.Enums.TipoMovimentoYMF.ResgateIR;

            if (isResgate && isConversaoAnteriorImplantacao)
            {
                OperacaoCotista operacaoCotista = new OperacaoCotista();
                if (operacaoCotista.ImportaMovimentoYMF(movimento, false))
                {
                    //Inserir novo IdOperacao do Financial na tabela de mapping com Id antigo da Operacao na YMF
                    mappingOperacaoFinancialYMF.Add(movimento.IdNota, operacaoCotista.IdOperacao.Value);
                }
            }
        }

        return mappingOperacaoFinancialYMF;
    }

    private MappingPosicaoFinancialYMF ProcessaArquivoPosicaoNotaYMF(StreamReader srReader,
        DateTime dataImplantacao, MappingOperacaoFinancialYMF mappingOperacaoFinancialYMF)
    {
        YMF ymf = new YMF();
        Financial.Interfaces.Import.YMF.PosicaoNotaYMF[] posicoes = ymf.ImportaPosicaoNotaYMF(srReader);

        PosicaoCotistaCollection posicaoCotistaCollection = new PosicaoCotistaCollection();
        MappingPosicaoFinancialYMF mappingPosicaoFinancialYMF = posicaoCotistaCollection.ImportaPosicaoNotaYMF(
            posicoes, dataImplantacao, mappingOperacaoFinancialYMF);

        if (mappingPosicaoFinancialYMF.Count > 0)
        {
            //Se alguma posicao foi importada, importaremos abertura e historico

            PosicaoCotistaAberturaCollection posicaoCotistaAberturaCollection = new PosicaoCotistaAberturaCollection();
            posicaoCotistaAberturaCollection.ImportaPosicaoNotaYMF(posicoes, dataImplantacao, mappingOperacaoFinancialYMF,
                mappingPosicaoFinancialYMF);

            PosicaoCotistaHistoricoCollection posicaoCotistaHistoricoCollection = new PosicaoCotistaHistoricoCollection();
            posicaoCotistaHistoricoCollection.ImportaPosicaoNotaYMF(posicoes, dataImplantacao, mappingOperacaoFinancialYMF,
                mappingPosicaoFinancialYMF);
        }

        return mappingPosicaoFinancialYMF;
    }

    private HistoricoCotaCollection ProcessaArquivoPosicaoFundoYMF(StreamReader srReader)
    {
        YMF ymf = new YMF();
        Financial.Interfaces.Import.YMF.PosicaoFundoYMF[] posicoes = ymf.ImportaPosicaoFundoYMF(srReader);

        HistoricoCotaCollection historicoCotaCollection = new HistoricoCotaCollection();
        historicoCotaCollection.ImportaPosicaoFundoYMF(posicoes);

        return historicoCotaCollection;
    }

    private DetalheResgateCotistaCollection ProcessaArquivoResgateYMF(StreamReader srReader,
        MappingOperacaoFinancialYMF mappingOperacaoFinancialYMF,
        MappingPosicaoFinancialYMF mappingPosicaoFinancialYMF)
    {
        YMF ymf = new YMF();
        ResgateYMF[] resgates = ymf.ImportaResgateYMF(srReader);

        DetalheResgateCotistaCollection detalheResgateCotistaCollection = new DetalheResgateCotistaCollection();
        detalheResgateCotistaCollection.ImportaResgateYMF(resgates,
            mappingOperacaoFinancialYMF, mappingPosicaoFinancialYMF);

        return detalheResgateCotistaCollection;
    }

    private void ProcessaArquivoRendimentoCompensarYMF(StreamReader srReader)
    {
        YMF ymf = new YMF();
        Financial.Interfaces.Import.YMF.RendimentoCompensarYMF[] rendimentos = ymf.ImportaRendimentoCompensarYMF(srReader);

        PrejuizoCotistaCollection prejuizoCotistaCollection = new PrejuizoCotistaCollection();
        prejuizoCotistaCollection.ImportaRendimentoCompensarYMF(rendimentos);

        PrejuizoCotistaHistoricoCollection prejuizoCotistaHistoricoCollection = new PrejuizoCotistaHistoricoCollection();
        prejuizoCotistaHistoricoCollection.ImportaRendimentoCompensarYMF(rendimentos);

    }

    private CotistaCollection ProcessaArquivoTipoCotistaYMF(StreamReader srReader, CotistaCollection cotistaCollectionImportada)
    {
        YMF ymf = new YMF();
        Financial.Interfaces.Import.YMF.TipoCotistaYMF[] tiposCotista = ymf.ImportaTipoCotistaYMF(srReader);

        cotistaCollectionImportada.ImportaTipoCotistaYMF(tiposCotista);

        return cotistaCollectionImportada;
    }

    private Carteira ImportaFundoYMF(string path)
    {
        YMF ymf = new YMF();
        FundoIRYMF[] fundoIRYMFArray;
        FundoLimiteYMF[] fundoLimiteYMFArray;
        FundoParametroYMF[] fundoParametroYMFArray;
        FundoPerformanceYMF[] fundoPerformanceYMFArray;
        FundoYMF[] fundoYMFArray;
        TipoFundoYMF[] tipoFundoYMFArray;

        FundoYMFInMemory fundoYMFInMemory = new FundoYMFInMemory();

        using (StreamReader streamReader = new StreamReader(path + "Fundo IR.txt", Encoding.GetEncoding("ISO-8859-1")))
        {
            fundoIRYMFArray = ymf.ImportaFundoIRYMF(streamReader);

            //Armazenar em um dictionary para facil acesso
            foreach (FundoIRYMF fundoIRYMF in fundoIRYMFArray)
            {
                if (fundoYMFInMemory.FundoIRDictionary.ContainsKey(fundoIRYMF.CdFundo))
                {
                    //Só adicionar se data for mais recente do que a atualmente presente no Dictionary
                    if (fundoIRYMF.DtInicioIr >= fundoYMFInMemory.FundoIRDictionary[fundoIRYMF.CdFundo].DtInicioIr)
                    {
                        fundoYMFInMemory.FundoIRDictionary[fundoIRYMF.CdFundo] = fundoIRYMF;
                    }
                }
                else
                {
                    fundoYMFInMemory.FundoIRDictionary.Add(fundoIRYMF.CdFundo, fundoIRYMF);
                }
            }
        }

        using (StreamReader streamReader = new StreamReader(path + "Fundo Parâmetro.txt", Encoding.GetEncoding("ISO-8859-1")))
        {
            fundoParametroYMFArray = ymf.ImportaFundoParametroYMF(streamReader);

            //Armazenar em um dictionary para facil acesso
            foreach (FundoParametroYMF fundoParametroYMF in fundoParametroYMFArray)
            {
                if (fundoYMFInMemory.FundoParametroDictionary.ContainsKey(fundoParametroYMF.CdFundo))
                {
                    //Só adicionar se data for mais recente do que a atualmente presente no Dictionary
                    if (fundoParametroYMF.DtInicioValidade >= fundoYMFInMemory.FundoParametroDictionary[fundoParametroYMF.CdFundo].DtInicioValidade)
                    {
                        fundoYMFInMemory.FundoParametroDictionary[fundoParametroYMF.CdFundo] = fundoParametroYMF;
                    }
                }
                else
                {
                    fundoYMFInMemory.FundoParametroDictionary.Add(fundoParametroYMF.CdFundo, fundoParametroYMF);
                }
            }
        }

        using (StreamReader streamReader = new StreamReader(path + "Fundo Performance.txt", Encoding.GetEncoding("ISO-8859-1")))
        {
            fundoPerformanceYMFArray = ymf.ImportaFundoPerformanceYMF(streamReader);

            //Armazenar em um dictionary para facil acesso
            foreach (FundoPerformanceYMF fundoPerformanceYMF in fundoPerformanceYMFArray)
            {
                if (fundoYMFInMemory.FundoPerformanceDictionary.ContainsKey(fundoPerformanceYMF.CdFundo))
                {
                    //Só adicionar se data for mais recente do que a atualmente presente no Dictionary
                    if (fundoPerformanceYMF.DtInicioPerformance >= fundoYMFInMemory.FundoPerformanceDictionary[fundoPerformanceYMF.CdFundo].DtInicioPerformance)
                    {
                        fundoYMFInMemory.FundoPerformanceDictionary[fundoPerformanceYMF.CdFundo] = fundoPerformanceYMF;
                    }
                }
                else
                {
                    fundoYMFInMemory.FundoPerformanceDictionary.Add(fundoPerformanceYMF.CdFundo, fundoPerformanceYMF);
                }
            }
        }

        using (StreamReader streamReader = new StreamReader(path + "Fundo.txt", Encoding.GetEncoding("ISO-8859-1")))
        {
            fundoYMFArray = ymf.ImportaFundoYMF(streamReader);
        }

        //Popular estrutura de Tipo do Fundo
        tipoFundoYMFArray = ymf.ImportaTipoFundoYMF();

        //Armazenar em um dictionary para facil acesso
        foreach (TipoFundoYMF tipoFundoYMF in tipoFundoYMFArray)
        {
            fundoYMFInMemory.TipoFundoDictionary.Add(tipoFundoYMF.CdTipoFundo, tipoFundoYMF);
        }

        CarteiraCollection carteiraCollection = new CarteiraCollection();
        carteiraCollection.ImportaFundoYMF(fundoYMFArray, fundoYMFInMemory);

        return carteiraCollection[0];

    }
}