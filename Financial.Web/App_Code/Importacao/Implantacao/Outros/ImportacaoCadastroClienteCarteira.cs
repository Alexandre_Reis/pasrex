﻿using System;
using System.Collections.Generic;
using System.IO;

using Financial.Integracao.Excel;
using Financial.Integracao.Excel.Util;
using Financial.Web.Common;

using EntitySpaces.Interfaces;

using DevExpress.Web;
using Financial.CRM;
using Financial.InvestidorCotista;
using Financial.RendaFixa;
using Financial.Investidor;
using Financial.Bolsa;
using Financial.BMF;
using Financial.Fundo;
using Financial.Common;
using Financial.InvestidorCotista.Enums;
using Financial.CRM.Enums;
using Financial.Investidor.Enums;
using Financial.Common.Enums;
using Financial.Bolsa.Enums;
using Financial.BMF.Enums;
using Financial.Fundo.Enums;
using Financial.Security;
using System.Web;
using Financial.Web.Util;
using Financial.Security.Enums;
using Financial.Util;
using Financial.Tributo.Enums;

public partial class ImportacaoBasePage : BasePage
{

    /* Estrutura do Excel */
    public List<ValoresExcelCadastroClienteCarteira> ValoresExcelCadastroClienteCarteira = new List<ValoresExcelCadastroClienteCarteira>();

    /// <summary>
    /// 
    /// </summary>
    /// <param name="streamExcel"></param>
    private void LerArquivoCadastroClienteCarteira(Stream streamExcel)
    {

        #region Leitura do Arquivo Excel
        // Open Spreadsheet
        Bytescout.Spreadsheet.Spreadsheet document = new Bytescout.Spreadsheet.Spreadsheet();
        document.LoadFromStream(streamExcel);

        Bytescout.Spreadsheet.Worksheet workSheet = document.Workbook.Worksheets[0];
        //
        #region Confere Formato Arquivo
        string[] colunasConferencias = new string[] {"IdPessoa","Nome","Apelido","IsentoIR",
                                                    "IsentoIOF","TipoCliente","TipoCota","BuscaCotaAnterior",
													"StatusAtivo","DataImplantacao","TipoCarteira",
                                                    "CotaInicial","TruncaCota","IdIndiceBenchmark","CasasDecimaisCota",
                                                    "CasasDecimaisQuantidade","IdCategoria","IdSubCategoria",
                                                    "IdAgenteAdministrador","IdAgenteCustodiante","IdAgenteGestor","CobraTaxaFiscalizacaoCVM",
                                                    "CalculaEnquadra","CalculaMTM","DiasCotizacaoAplicacao","DiasLiquidacaoAplicacao",
                                                    "CalculaIOF","TipoCusto","DiasCotizacaoResgate","DiasLiquidacaoResgate",
                                                    "CalculaPrazoMedio","ContagemDiasConversaoResgate","PrioridadeOperacao","TipoTributacao",
                                                    "TipoFundo","ProjecaoIRComeCotas","ProjecaoIRCotista","DiasAposComeCotas",
                                                    "DiasAposResgate","LocalNegociacao"
                };

        bool formato = ValoresExcelUtil.isExcelFormatoValido(colunasConferencias, workSheet);
        if (!formato)
        {
            string mensagem = "Formato Interno do Arquivo Inválido.\n";
            mensagem += "Linha 1 do Arquivo deve ser: \n";
            for (int i = 0; i < colunasConferencias.Length; i++)
            {
                int coluna = i + 1;
                mensagem += "\tColuna " + coluna + ": " + colunasConferencias[i] + "\n";
            }

            document.Close();
            document.Dispose();

            throw new Exception(mensagem);
        }
        #endregion

        //
        int index = 0;
        // This row,column index should be changed as per your need.
        // i.e. which cell in the excel you are interesting to read.
        //
        /* Formato: 1)idPessoa - 2)nome - 3)apelido - 4)isentoIR
					- 5)isentoIOF - 6)TipoCliente - 7)TipoCota - 8)BuscaCotaAnterior
					- 9)statusAtivo - 10)dataImplantacao - 11)tipoCarteira- 12)cotaInicial
					- 13)truncaCota - 14)idIndiceBenchmark - 15)casasDecimaisCota - 16)casasDecimaisQuantidade
					- 17)idCategoria - 18)idSubCategoria - 19)idAgenteAdministrador - 20)idAgenteCustodiante
					- 21)idAgenteGestor - 22)cobraTaxaFiscalizacaoCVM - 23)calculaEnquadra - 24)calculaMTM
					- 25)diasCotizacaoAplicacao - 26)diasLiquidacaoAplicacao - 27)calculaIOF- 28)tipoCusto
					- 29)diasCotizacaoResgate - 30)diasLiquidacaoResgate - 31)calculaPrazoMedio - 32)contagemDiasConversaoResgate
					- 33)prioridadeOperacao - 34)tipoTributacao - 35)tipoFundo - 36)projecaoIRComeCotas
					- 37)projecaoIRCotista - 38)diasAposComeCotas - 39)diasAposResgate - 40)localNegociacao   
         *          - 41)TipoControle   - 42)IdGrupoProcessamento
         */
        int linha = 1;
        int coluna1 = 0,coluna2 = 1,coluna3 = 2,coluna4 = 3,
            coluna5 = 4,coluna6 = 5,coluna7 = 6,coluna8 = 7,
            coluna9 = 8,coluna10 = 9,coluna11 = 10,coluna12 = 11,
            coluna13 = 12,coluna14 = 13,coluna15 = 14,coluna16 = 15,
            coluna17 = 16,coluna18 = 17,coluna19 = 18,coluna20 = 19,
            coluna21 = 20,coluna22 = 21,coluna23 = 22,coluna24 = 23,
            coluna25 = 24,coluna26 = 25,coluna27 = 26,coluna28 = 27,
            coluna29 = 28,coluna30 = 29,coluna31 = 30,coluna32 = 31,
            coluna33 = 32,coluna34 = 33,coluna35 = 34,coluna36 = 35,
            coluna37 = 36,coluna38 = 37,coluna39 = 38,coluna40 = 39,
            coluna41 = 40, coluna42 = 41;
        //            
        try
        {
            AgenteMercadoCollection agenteMercadoCollection = new AgenteMercadoCollection();
            agenteMercadoCollection.Query.Where(agenteMercadoCollection.Query.Nome.Like("%Padrão%"),
                                                agenteMercadoCollection.Query.FuncaoAdministrador.Equal("S"),
                                                agenteMercadoCollection.Query.FuncaoGestor.Equal("S"));
            agenteMercadoCollection.Query.Load();

            if (agenteMercadoCollection.Count == 0)
            {
                agenteMercadoCollection.QueryReset();
                agenteMercadoCollection.Query.Where(agenteMercadoCollection.Query.FuncaoAdministrador.Equal("S"),
                                                    agenteMercadoCollection.Query.FuncaoGestor.Equal("S"));
                agenteMercadoCollection.Query.Load();
            }

            // Enquanto idCliente tiver valor
            while (workSheet.Cell(linha, coluna1).Value != null)
            {
                ValoresExcelCadastroClienteCarteira item = new ValoresExcelCadastroClienteCarteira();

                #region IdPessoa
                try {
                    item.IdPessoa = workSheet.Cell(linha, coluna1).ValueAsInteger;
                }
                catch (Exception ex) {
                    throw new Exception(ex.Message + " - IdPessoa: linha " + (linha + 1));
                }
                #endregion

                #region Nome
                if (workSheet.Cell(linha, coluna2).Value == null)
                    throw new Exception("Nome não informado: linha " + (linha + 1));
                try
                {
                    item.Nome = Convert.ToString(workSheet.Cell(linha, coluna2).Value);
                }
                catch (Exception ex) {
                    throw new Exception(ex.Message + " - Nome: linha " + (linha + 1));
                }
                #endregion

                #region Apelido
                if (workSheet.Cell(linha, coluna3).Value == null)
                    throw new Exception("Apelido não informado: linha " + (linha + 1));
                try
                {
                    item.Apelido = Convert.ToString(workSheet.Cell(linha, coluna3).Value);
                }
                catch (Exception ex) {
                    throw new Exception(ex.Message + " - Apelido: linha " + (linha + 1));
                }   
                #endregion

                #region IsentoIR
                if (workSheet.Cell(linha, coluna4).Value == null)
                    throw new Exception("isentoIR não informado: linha " + (linha + 1));
                try
                {
                    item.IsentoIR = Convert.ToString(workSheet.Cell(linha, coluna4).Value);
                }
                catch (Exception ex) {
                    throw new Exception(ex.Message + " - isentoIR: linha " + (linha + 1));
                }
                #endregion

                #region IsentoIOF
                if (workSheet.Cell(linha, coluna5).Value == null)
                    throw new Exception("IsentoIOF não informado: linha " + (linha + 1));
                try
                {
                    item.IsentoIOF = Convert.ToString(workSheet.Cell(linha, coluna5).Value);
                }
                catch (Exception ex) {
                    throw new Exception(ex.Message + " - IsentoIOF: linha " + (linha + 1));
                }
                #endregion
				
                #region IdTipo
                if (workSheet.Cell(linha, coluna6).Value != null)
                {
                    item.IdTipo = Convert.ToInt32(workSheet.Cell(linha, coluna6).Value);
                }
                #endregion
				
				#region TipoCota
                if (workSheet.Cell(linha, coluna7).Value != null)
                {
                    item.TipoCota = Convert.ToByte(workSheet.Cell(linha, coluna7).Value);
                }
                else
                {
                    item.TipoCota = (byte)TipoCotaFundo.Fechamento;;
                }
                #endregion
				
				#region BuscaCotaAnterior
                if (workSheet.Cell(linha, coluna8).Value != null)
                {
                    item.BuscaCotaAnterior = workSheet.Cell(linha, coluna8).ValueAsString;
                }
                else
                {
                    item.BuscaCotaAnterior = "N";
                }
                #endregion				

                #region StatusAtivo
                if (workSheet.Cell(linha, coluna9).Value == null)
                    throw new Exception("StatusAtivo não informado: linha " + (linha + 1));
                try
                {
                    int status = workSheet.Cell(linha, coluna9).ValueAsInteger;
                    item.StatusAtivo = (StatusAtivoCliente)status;
                }
                catch (Exception ex) {
                    throw new Exception(ex.Message + " - StatusAtivo: linha " + (linha + 1));
                }
                #endregion

                #region dataImplantacao
                if (workSheet.Cell(linha, coluna10).Value != null)
                    item.DataImplantacao = workSheet.Cell(linha, coluna10).ValueAsDateTime;
                else
                    item.DataImplantacao = DateTime.Today;
                #endregion 

                #region tipoCarteira
                if (workSheet.Cell(linha, coluna11).Value != null)
                {
                    try
                    {
                        item.TipoCarteira = Convert.ToByte(workSheet.Cell(linha, coluna11).Value);
                    }
                    catch (Exception ex)
                    {
                        throw new Exception(ex.Message + " - tipoCarteira: linha " + (linha + 1));
                    }
                }
                #endregion 

                #region cotaInicial
                if (workSheet.Cell(linha, coluna12).Value != null)
                {
                    try
                    {
                        item.CotaInicial = Convert.ToDecimal(workSheet.Cell(linha, coluna12).Value);
                    }
                    catch (Exception ex)
                    {
                        throw new Exception(ex.Message + " - cotaInicial: linha " + (linha + 1));
                    }
                }
                else
                {
                    item.CotaInicial = 1;
                }
                #endregion

                #region truncaCota
                if (workSheet.Cell(linha, coluna13).Value != null)
                {
                    item.TruncaCota = workSheet.Cell(linha, coluna13).ValueAsString;
                }
                else
                {
                    item.TruncaCota = "N";
                }
                #endregion

                #region idIndiceBenchmark
                if (workSheet.Cell(linha, coluna14).Value != null)
                {
                    try
                    {
                        int idIndice = workSheet.Cell(linha, coluna14).ValueAsInteger;
                        item.IdIndiceBenchmark = idIndice;
                    }
                    catch (Exception ex)
                    {
                        throw new Exception(ex.Message + " - idIndiceBenchmark: linha " + (linha + 1));
                    }
                }
                else
                {
                    item.IdIndiceBenchmark = (int)ListaIndiceFixo.CDI;
                }
                #endregion

                #region casasDecimaisCota
                if (workSheet.Cell(linha, coluna15).Value != null)
                {
                    try
                    {
                        item.CasasDecimaisCota = Convert.ToByte(workSheet.Cell(linha, coluna15).Value);
                    }
                    catch (Exception ex)
                    {
                        throw new Exception(ex.Message + " - casasDecimaisCota: linha " + (linha + 1));
                    }
                }
                else
                {
                    item.CasasDecimaisCota = 8;
                }
                #endregion

                #region casasDecimaisQuantidade
                if (workSheet.Cell(linha, coluna16).Value != null)
                {
                    try
                    {
                        item.CasasDecimaisQuantidade = Convert.ToByte(workSheet.Cell(linha, coluna16).Value);
                    }
                    catch (Exception ex)
                    {
                        throw new Exception(ex.Message + " - casasDecimaisQuantidade: linha " + (linha + 1));
                    }
                }
                else
                {
                    item.CasasDecimaisQuantidade = 8;
                }
                #endregion

                #region idCategoria
                if (workSheet.Cell(linha, coluna17).Value != null)
                {
                    try
                    {
                        item.IdCategoria = workSheet.Cell(linha, coluna17).ValueAsInteger;
                    }
                    catch (Exception ex)
                    {
                        throw new Exception(ex.Message + " - idCategoria: linha " + (linha + 1));
                    }
                }
                #endregion

                #region IdSubCategoria
                if (workSheet.Cell(linha, coluna18).Value != null)
                {
                    try
                    {
                        item.IdSubCategoria = workSheet.Cell(linha, coluna18).ValueAsInteger;
                    }
                    catch (Exception ex)
                    {
                        throw new Exception(ex.Message + " - IdSubCategoria: linha " + (linha + 1));
                    }
                }
                #endregion

                #region IdAgenteAdministrador
                if (workSheet.Cell(linha, coluna19).Value != null)
                {
                    try
                    {
                        item.IdAgenteAdministrador = workSheet.Cell(linha, coluna19).ValueAsInteger;
                    }
                    catch (Exception ex)
                    {
                        throw new Exception(ex.Message + " - IdAgenteAdministrador: linha " + (linha + 1));
                    }
                }
                else
                {
                    item.IdAgenteAdministrador = agenteMercadoCollection[0].IdAgente.Value;
                }
                #endregion

                #region IdAgenteCustodiante
                if (workSheet.Cell(linha, coluna20).Value != null)
                {
                    try
                    {
                        item.IdAgenteCustodiante = workSheet.Cell(linha, coluna20).ValueAsInteger;
                    }
                    catch (Exception ex)
                    {
                        throw new Exception(ex.Message + " - IdAgenteCustodiante: linha " + (linha + 1));
                    }
                }
                else
                    item.IdAgenteCustodiante = 0;
                #endregion

                #region IdAgenteGestor
                if (workSheet.Cell(linha, coluna21).Value != null)
                {
                    try
                    {
                        item.IdAgenteGestor = workSheet.Cell(linha, coluna21).ValueAsInteger;
                    }
                    catch (Exception ex)
                    {
                        throw new Exception(ex.Message + " - IdAgenteGestor: linha " + (linha + 1));
                    }
                }
                else
                {
                    item.IdAgenteGestor = agenteMercadoCollection[0].IdAgente.Value;
                }
                #endregion

                #region CobraTaxaFiscalizacaoCVM
                if (workSheet.Cell(linha, coluna22).Value != null)
                {
                    item.CobraTaxaFiscalizacaoCVM = workSheet.Cell(linha, coluna22).ValueAsString;
                }
                else
                {
                    item.CobraTaxaFiscalizacaoCVM = "N";
                }
                #endregion

                #region CalculaEnquadra
                if (workSheet.Cell(linha, coluna23).Value != null)
                {
                    item.CalculaEnquadra = workSheet.Cell(linha, coluna23).ValueAsString;
                }
                else
                {
                    item.CalculaEnquadra = "N";
                }
                #endregion

                #region CalculaMTM
                if (workSheet.Cell(linha, coluna24).Value != null)
                {
                    item.CalculaMTM = workSheet.Cell(linha, coluna24).ValueAsString;
                }
                else
                {
                    item.CalculaMTM = "N";
                }
                #endregion

                #region diasCotizacaoAplicacao
                if (workSheet.Cell(linha, coluna25).Value != null)
                {
                    try
                    {
                        item.DiasCotizacaoAplicacao = Convert.ToByte(workSheet.Cell(linha, coluna25).Value);
                    }
                    catch (Exception ex)
                    {
                        throw new Exception(ex.Message + " - diasCotizacaoAplicacao: linha " + (linha + 1));
                    }
                }
                else
                {
                    item.DiasCotizacaoAplicacao = 0;
                }
                #endregion

                #region DiasLiquidacaoAplicacao
                if (workSheet.Cell(linha, coluna26).Value != null)
                {
                    try
                    {
                        item.DiasLiquidacaoAplicacao = Convert.ToByte(workSheet.Cell(linha, coluna26).Value);
                    }
                    catch (Exception ex)
                    {
                        throw new Exception(ex.Message + " - DiasLiquidacaoAplicacao: linha " + (linha + 1));
                    }
                }
                else
                {
                    item.DiasLiquidacaoAplicacao = 0;
                }
                #endregion

                #region CalculaIOF
                if (workSheet.Cell(linha, coluna27).Value != null)
                {
                    item.CalculaIOF = workSheet.Cell(linha, coluna27).ValueAsString;
                }
                else
                {
                    item.CalculaIOF = "N";
                }
                #endregion

                #region TipoCusto
                if (workSheet.Cell(linha, coluna28).Value != null)
                {
                    try
                    {
                        item.TipoCusto = Convert.ToByte(workSheet.Cell(linha, coluna28).Value);
                    }
                    catch (Exception ex)
                    {
                        throw new Exception(ex.Message + " - DiasLiquidacaoAplicacao: linha " + (linha + 1));
                    }
                }
                else
                {
                    item.TipoCusto = (byte)TipoCustoFundo.Aplicacao; 
                }
                #endregion

                #region DiasCotizacaoResgate
                if (workSheet.Cell(linha, coluna29).Value != null)
                {
                    try
                    {
                        item.DiasCotizacaoResgate = Convert.ToByte(workSheet.Cell(linha, coluna29).Value);
                    }
                    catch (Exception ex)
                    {
                        throw new Exception(ex.Message + " - DiasCotizacaoResgate: linha " + (linha + 1));
                    }
                }
                else
                {
                    item.DiasCotizacaoResgate = 0;
                }
                #endregion

                #region DiasLiquidacaoResgate
                if (workSheet.Cell(linha, coluna30).Value != null)
                {
                    try
                    {
                        item.DiasLiquidacaoAplicacao = Convert.ToByte(workSheet.Cell(linha, coluna30).Value);
                    }
                    catch (Exception ex)
                    {
                        throw new Exception(ex.Message + " - DiasLiquidacaoResgate: linha " + (linha + 1));
                    }
                }
                else
                {
                    item.DiasLiquidacaoAplicacao = 0;
                }
                #endregion

                #region CalculaPrazoMedio
                if (workSheet.Cell(linha, coluna31).Value != null)
                {
                    item.CalculaPrazoMedio = workSheet.Cell(linha, coluna31).ValueAsString;
                }
                else
                {
                    item.CalculaPrazoMedio = "N";
                }
                #endregion

                #region ContagemDiasConversaoResgate
                if (workSheet.Cell(linha, coluna32).Value != null)
                {
                    item.ContagemDiasConversaoResgate = Convert.ToByte(workSheet.Cell(linha, coluna32).Value);
                }
                else
                {
                    item.ContagemDiasConversaoResgate = (byte)ContagemDiasLiquidacaoResgate.DiasUteis; 
                }
                #endregion

                #region PrioridadeOperacao
                if (workSheet.Cell(linha, coluna33).Value != null)
                {
                    item.PrioridadeOperacao = Convert.ToByte(workSheet.Cell(linha, coluna33).Value);
                }
                else
                {
                    item.PrioridadeOperacao = (byte)PrioridadeOperacaoFundo.ResgateAntes;
                }
                #endregion

                #region TipoTributacao
                if (workSheet.Cell(linha, coluna34).Value != null)
                {
                    item.TipoTributacao = Convert.ToByte(workSheet.Cell(linha, coluna34).Value);
                }
                else
                {
                    item.TipoTributacao = (byte)TipoTributacaoFundo.Acoes;
                }
                #endregion

                #region TipoFundo
                if (workSheet.Cell(linha, coluna35).Value != null)
                {
                    item.TipoFundo = Convert.ToByte(workSheet.Cell(linha, coluna35).Value);
                }
                else
                {
                    item.TipoFundo = (byte)TipoFundo.Fechado;
                }
                #endregion

                #region ProjecaoIRComeCotas
                if (workSheet.Cell(linha, coluna36).Value != null)
                {
                    item.ProjecaoIRComeCotas = Convert.ToByte(workSheet.Cell(linha, coluna36).Value);
                }
                #endregion    

                #region projecaoIRCotista
                if (workSheet.Cell(linha, coluna37).Value != null)
                {
                    item.ProjecaoIRCotista = Convert.ToByte(workSheet.Cell(linha, coluna37).Value);
                }
                else
                {
                    item.ProjecaoIRCotista = (byte)Financial.Tributo.Custom.CalculoTributo.ProjecaoIRCotista.DiaLiquidacaoResgate;
                }
                #endregion 
                
                #region DiasAposComeCotas
                if (workSheet.Cell(linha, coluna38).Value != null)
                {
                    try
                    {
                        item.DiasAposComeCotas = Convert.ToByte(workSheet.Cell(linha, coluna38).Value);
                    }
                    catch (Exception ex)
                    {
                        throw new Exception(ex.Message + " - DiasAposComeCotas: linha " + (linha + 1));
                    }
                }
                else
                {
                    item.DiasAposComeCotas = 0;
                }
                #endregion

                #region DiasAposResgate
                if (workSheet.Cell(linha, coluna39).Value != null)
                {
                    try
                    {
                        item.DiasAposResgate = Convert.ToByte(workSheet.Cell(linha, coluna39).Value);
                    }
                    catch (Exception ex)
                    {
                        throw new Exception(ex.Message + " - DiasAposResgate: linha " + (linha + 1));
                    }
                }
                else
                {
                    item.DiasAposResgate = 0;
                }
                #endregion

                #region LocalNegociacao
                if (workSheet.Cell(linha, coluna40).Value != null)
                {
                    try
                    {
                        item.LocalNegociacao = workSheet.Cell(linha, coluna40).ValueAsString;
                    }
                    catch (Exception ex)
                    {
                        throw new Exception(ex.Message + " - LocalNegociacao: linha " + (linha + 1));
                    }
                }
                #endregion

                #region TipoControle

                if (workSheet.Cell(linha, coluna41).Value != null)
                {
                    try
                    {
                        item.TipoControle = Convert.ToByte(workSheet.Cell(linha, coluna41).Value);
                    }
                    catch (Exception ex)
                    {
                        throw new Exception(ex.Message + " - TipoControle: linha " + (linha + 1));
                    }
                }
                #endregion


                #region IdGrupoProcessamento
                if (workSheet.Cell(linha, coluna42).Value != null)
                {
                    try
                    {
                        item.IdGrupoProcessamento = workSheet.Cell(linha, coluna42).ValueAsInteger;
                    }
                    catch (Exception ex)
                    {
                        throw new Exception(ex.Message + " - IdGrupoProcessamento: linha " + (linha + 1));
                    }
                }
                #endregion

                //
                this.ValoresExcelCadastroClienteCarteira.Add(item);
                //
                index++;
                linha = 1 + index;
                //                
            }
        }

        catch (Exception ex)
        {
            document.Close();
            document.Dispose();

            throw new Exception(ex.Message);
        }
        #endregion

        document.Close();
        document.Dispose();
    }

    public void CarregaCadastroClienteCarteira(int? idTipo)
    {
        PessoaCollection pessoaCollection = new PessoaCollection();
        PessoaEnderecoCollection pessoaEnderecoCollection = new PessoaEnderecoCollection();
        PessoaTelefoneCollection pessoaTelefoneCollection = new PessoaTelefoneCollection();
        PessoaEmailCollection pessoaEmailCollection = new PessoaEmailCollection();
        //
        ClienteCollection clienteCollection = new ClienteCollection();
        ClienteBolsaCollection clienteBolsaCollection = new ClienteBolsaCollection();
        ClienteBMFCollection clienteBMFCollection = new ClienteBMFCollection();
        ClienteRendaFixaCollection clienteRendaFixaCollection = new ClienteRendaFixaCollection();
        ClienteInterfaceCollection clienteInterfaceCollection = new ClienteInterfaceCollection();
        //
        CarteiraCollection carteiraCollection = new CarteiraCollection();
        CotistaCollection cotistaCollection = new CotistaCollection();
        ContaCorrenteCollection contaCorrenteCollection = new ContaCorrenteCollection();
        PermissaoClienteCollection permissaoClienteCollection = new PermissaoClienteCollection();

        int idClienteNovo = 0;
        ClienteCollection clienteColl = new ClienteCollection();
        clienteColl.Query.Select(clienteColl.Query.IdCliente.Max().Coalesce("0").As("MAX"));

        if (clienteColl.Query.Load())
            idClienteNovo = Convert.ToInt32(clienteColl[0].GetColumn("MAX")) + 1;
        else
            idClienteNovo = 1;
        //
        for (int i = 0; i < this.ValoresExcelCadastroClienteCarteira.Count; i++)
        {
            ValoresExcelCadastroClienteCarteira valoresExcel = this.ValoresExcelCadastroClienteCarteira[i];
            //                    

            #region Valida Pessoa
            Pessoa pessoaAux = new Pessoa();
            List<esQueryItem> campos = new List<esQueryItem>();
            campos.Add(pessoaAux.Query.IdPessoa);
            campos.Add(pessoaAux.Query.Tipo);
            campos.Add(pessoaAux.Query.CodigoInterface);
            if (!pessoaAux.LoadByPrimaryKey(campos, valoresExcel.IdPessoa))
            {
                throw new Exception("Id Pessoa não existente : " + valoresExcel.IdPessoa);
            }
            #endregion

            #region Verifica se Campos Obrigatórios foram Preenchidos
            if (String.IsNullOrEmpty(valoresExcel.Nome.Trim()))
            {
                throw new Exception("Nome Cliente não pode ser em Branco");
            }

            if (String.IsNullOrEmpty(valoresExcel.IsentoIR.Trim()))
            {
                throw new Exception("Campo IsentoIR não Obrigatório");
            }

            if (String.IsNullOrEmpty(valoresExcel.IsentoIOF.Trim()))
            {
                throw new Exception("Campo IsentoIOF não Obrigatório");
            }

            if (valoresExcel.StatusAtivo == 0)
            {   // Conversão do Enum resultou em 0
                throw new Exception("Status Cliente (Ativo/Inativo) deve ser Preenchido");
            }
            #endregion

            Cliente cliente = clienteCollection.AddNew();
            #region Preenche dados de Cliente
            cliente.IdCliente = idClienteNovo;
            cliente.Nome = valoresExcel.Nome;
            cliente.Apelido = valoresExcel.Apelido;
            cliente.IdPessoa = valoresExcel.IdPessoa;
            cliente.IdLocalNegociacao = (int)Financial.Common.Enums.LocalNegociacaoFixo.Brasil;
            if (!string.IsNullOrEmpty(valoresExcel.LocalNegociacao))
            {
                LocalNegociacao localNegociacao = new LocalNegociacao();
                localNegociacao.Query.Where(localNegociacao.Query.Codigo.ToUpper().Equal(valoresExcel.LocalNegociacao.ToUpper()));

                if (localNegociacao.Query.Load())
                    cliente.IdLocalNegociacao = localNegociacao.IdLocalNegociacao.Value;
                else
                    throw new Exception("Local de Negociação não cadastrado: " + valoresExcel.LocalNegociacao);
            }

            cliente.ApuraGanhoRV = "N";
            cliente.CalculaContabil = "N";
            cliente.CalculaGerencial = "N";
            cliente.CalculaRealTime = "N";            
            cliente.DataImplantacao = valoresExcel.DataImplantacao.Value;
            cliente.DataDia = cliente.DataImplantacao.Value;
            cliente.DataInicio = cliente.DataImplantacao.Value;

            if (valoresExcel.TipoControle.HasValue)
                cliente.TipoControle = valoresExcel.TipoControle.Value;
            else
                cliente.TipoControle = (byte)TipoControleCliente.Completo;

            GrupoProcessamentoCollection grupoProcessamentoCollection = new GrupoProcessamentoCollection();
            short idGrupoProcessamento = 0;
            if (valoresExcel.IdGrupoProcessamento.HasValue)
            {
                grupoProcessamentoCollection.Query.Where(grupoProcessamentoCollection.Query.IdGrupoProcessamento.Equal(valoresExcel.IdGrupoProcessamento.Value));

                if (!grupoProcessamentoCollection.Query.Load())
                    throw new Exception("Id Grupo Processamento não existente : " + valoresExcel.IdGrupoProcessamento);
                else
                    idGrupoProcessamento = grupoProcessamentoCollection[0].IdGrupoProcessamento.Value;
            }
            else
            {
                grupoProcessamentoCollection.Query.Where(grupoProcessamentoCollection.Query.Descricao.Like("%Carteira%"));
                grupoProcessamentoCollection.Query.Load();

                if (grupoProcessamentoCollection.Count == 0)
                {
                    grupoProcessamentoCollection.QueryReset();
                    grupoProcessamentoCollection.LoadAll();
                }

                idGrupoProcessamento = grupoProcessamentoCollection[0].IdGrupoProcessamento.Value;
            }
            cliente.IdGrupoProcessamento = idGrupoProcessamento;

            cliente.IdMoeda = (byte)ListaMoedaFixo.Real;

            if (valoresExcel.IdTipo.HasValue)
            {
                TipoCliente tipoCliente = new TipoCliente();
                if (tipoCliente.LoadByPrimaryKey(valoresExcel.IdTipo.Value))
                    cliente.IdTipo = valoresExcel.IdTipo.Value;
                else
                    throw new Exception("Id Tipo de Cliente não existente : " + valoresExcel.IdTipo);
            }
            else if (idTipo.HasValue)
            {
                cliente.IdTipo = idTipo;
            }
            else
            {
                if (pessoaAux.Tipo.Value == (byte)TipoPessoa.Fisica)
                {
                    cliente.IdTipo = (byte)TipoClienteFixo.ClientePessoaFisica;
                }
                else
                {
                    cliente.IdTipo = (byte)TipoClienteFixo.ClientePessoaJuridica;
                }
            }

            cliente.IsentoIOF = valoresExcel.IsentoIR.ToUpper();
            cliente.IsentoIR = valoresExcel.IsentoIOF.ToUpper();

            cliente.IsProcessando = "N";
            cliente.Status = (byte)StatusCliente.Aberto;
            cliente.StatusAtivo = (byte)valoresExcel.StatusAtivo;
            cliente.StatusRealTime = (byte)StatusRealTimeCliente.NaoExecutar;
            cliente.ZeraCaixa = "S";
            cliente.DescontaTributoPL = (byte)DescontoPLCliente.BrutoSemImpacto;
            cliente.GrossUP = (byte)GrossupCliente.NaoFaz;
            #endregion

            #region Cadastra ClienteBolsa
            bool bolsaPreenchido = valoresExcel.Bolsa != null;

            ClienteBolsa clienteBolsa = clienteBolsaCollection.AddNew();
            clienteBolsa.CodigoSinacor = cliente.IdCliente.Value.ToString();
            clienteBolsa.IdCliente = cliente.IdCliente.Value;
            clienteBolsa.IsentoIR = bolsaPreenchido? valoresExcel.Bolsa.IsentoIR: valoresExcel.IsentoIR.ToUpper(); //Se não tem isenção específica de bolsa, acata a isenção geral do cliente
            clienteBolsa.TipoCotacao = bolsaPreenchido? valoresExcel.Bolsa.TipoCotacao.Value: (byte)TipoCotacaoBolsa.Fechamento;
            clienteBolsa.IdAssessor = bolsaPreenchido? valoresExcel.Bolsa.IdAssessor: null;
            clienteBolsa.TipoCusto = 1;
            #endregion

            #region Cadastra ClienteBMF
            bool bmfPreenchido = valoresExcel.BMF != null;

            ClienteBMF clienteBMF = clienteBMFCollection.AddNew();
            clienteBMF.CodigoSinacor = cliente.IdCliente.Value.ToString();
            clienteBMF.IdCliente = cliente.IdCliente.Value;
            clienteBMF.InvestidorInstitucional = "N";
            clienteBMF.TipoCotacao = bmfPreenchido? valoresExcel.BMF.TipoCotacao.Value: (byte)TipoCotacaoBMF.Fechamento;
            clienteBMF.IdAssessor = bmfPreenchido ? valoresExcel.BMF.IdAssessor : null;
            clienteBMF.Socio = "N";
            clienteBMF.TipoPlataforma = (byte)TipoPlataformaOperacional.Normal;
            #endregion

            #region Cadastra ClienteRendaFixa
            bool rendaFixaPreenchido = valoresExcel.RendaFixa != null;

            ClienteRendaFixa clienteRendaFixa = clienteRendaFixaCollection.AddNew();
            clienteRendaFixa.IdCliente = cliente.IdCliente.Value;
            clienteRendaFixa.IsentoIR = rendaFixaPreenchido ? valoresExcel.RendaFixa.IsentoIR : valoresExcel.IsentoIR.ToUpper(); //Se não tem isenção específica de RF, acata a isenção geral do cliente
            clienteRendaFixa.IsentoIOF = rendaFixaPreenchido ? valoresExcel.RendaFixa.IsentoIOF : valoresExcel.IsentoIOF.ToUpper(); //Se não tem isenção específica de RF, acata a isenção geral do cliente
            clienteRendaFixa.IdAssessor = rendaFixaPreenchido ? valoresExcel.RendaFixa.IdAssessor : null;
            clienteRendaFixa.CodigoInterface = cliente.IdCliente.Value.ToString();
            clienteRendaFixa.UsaCustoMedio = "N";
            #endregion

            #region tabPage - ClienteInterface
            ClienteInterface clienteInterface = clienteInterfaceCollection.AddNew();
            clienteInterface.IdCliente = cliente.IdCliente.Value;
            clienteInterface.CodigoYMF = pessoaAux.CodigoInterface;
            #endregion

            #region Cadastra Carteira
            Carteira carteira = carteiraCollection.AddNew();

            carteira.IdCarteira = idClienteNovo;
            carteira.Nome = valoresExcel.Nome;
            carteira.Apelido = valoresExcel.Apelido;
            carteira.CalculaEnquadra = valoresExcel.CalculaEnquadra;
            carteira.CalculaIOF = valoresExcel.CalculaIOF;
            carteira.CasasDecimaisCota = valoresExcel.CasasDecimaisCota;
            carteira.CasasDecimaisQuantidade = valoresExcel.CasasDecimaisQuantidade;
            carteira.CobraTaxaFiscalizacaoCVM = valoresExcel.CobraTaxaFiscalizacaoCVM;
            carteira.ContagemDiasConversaoResgate = valoresExcel.ContagemDiasConversaoResgate;
            carteira.CotaInicial = valoresExcel.CotaInicial;
            carteira.TipoAniversario = 0;
            carteira.DataInicioCota = cliente.DataDia.Value;
            carteira.DiasCotizacaoAplicacao = valoresExcel.DiasCotizacaoAplicacao;
            carteira.DiasCotizacaoResgate = valoresExcel.DiasCotizacaoResgate;
            carteira.DiasLiquidacaoAplicacao = valoresExcel.DiasLiquidacaoAplicacao;
            carteira.DiasLiquidacaoResgate = valoresExcel.DiasLiquidacaoResgate;
            carteira.DistribuicaoDividendo = (byte)DistribuicaoDividendosCarteira.NaoDistribui;
            carteira.BuscaCotaAnterior = valoresExcel.BuscaCotaAnterior;

            AgenteMercado agente = new AgenteMercado();
			if(!agente.LoadByPrimaryKey(valoresExcel.IdAgenteAdministrador))
				throw new Exception("Id.Agente Administrador (Agente de Mercado) não existente : " + valoresExcel.IdAgenteAdministrador);
			
			agente = new AgenteMercado();
            if (!agente.LoadByPrimaryKey(valoresExcel.IdAgenteGestor))
                throw new Exception("Id.Agente Gestor (Agente de Mercado) não existente : " + valoresExcel.IdAgenteGestor);					

            carteira.IdAgenteAdministrador = valoresExcel.IdAgenteAdministrador;
            carteira.IdAgenteGestor = valoresExcel.IdAgenteGestor;

            CategoriaFundoCollection categoriaFundoCollection = new CategoriaFundoCollection();

            int idCategoria = 0;
            if (valoresExcel.IdCategoria.HasValue)
            {
                categoriaFundoCollection.Query.Where(categoriaFundoCollection.Query.IdCategoria.Equal(valoresExcel.IdCategoria.Value));

                if (!categoriaFundoCollection.Query.Load())
                    throw new Exception("Id Categoria não existente : " + valoresExcel.IdCategoria);
                else
                    idCategoria = categoriaFundoCollection[0].IdCategoria.Value;
            }
            else
            {
                categoriaFundoCollection.Query.Where(categoriaFundoCollection.Query.Descricao.Like("%Carteira Administrada%"));
                categoriaFundoCollection.Query.Load();
                if (categoriaFundoCollection.Count == 0)
                {
                    categoriaFundoCollection.QueryReset();
                    categoriaFundoCollection.LoadAll();
                }

                idCategoria = categoriaFundoCollection[0].IdCategoria.Value;
            }

            carteira.IdCategoria = idCategoria;

            SubCategoriaFundoCollection subCategoriaFundoCollection = new SubCategoriaFundoCollection();
            int idSubCategoria = 0;
            if (valoresExcel.IdSubCategoria.HasValue)
            {
                subCategoriaFundoCollection.Query.Where(subCategoriaFundoCollection.Query.IdSubCategoria.Equal(valoresExcel.IdSubCategoria.Value));

                if (!subCategoriaFundoCollection.Query.Load())
                    throw new Exception("Id SubCategoria não existente : " + valoresExcel.IdSubCategoria);
                else
                    idSubCategoria = subCategoriaFundoCollection[0].IdSubCategoria.Value;
            }
            else
            {
                subCategoriaFundoCollection.Query.Where(subCategoriaFundoCollection.Query.Descricao.Like("%Carteira Administrada%"));

                subCategoriaFundoCollection.Query.Load();

                if (subCategoriaFundoCollection.Count == 0)
                {
                    subCategoriaFundoCollection.QueryReset();
                    subCategoriaFundoCollection.LoadAll();
                }

                idSubCategoria = subCategoriaFundoCollection[0].IdSubCategoria.Value;
            }

            carteira.IdSubCategoria = idSubCategoria;

            IndiceCollection indiceCollection = new IndiceCollection();
            short idIndiceBenchmark = 0;
            if (valoresExcel.IdIndiceBenchmark.HasValue)
            {
                indiceCollection.Query.Where(indiceCollection.Query.IdIndice.Equal(valoresExcel.IdIndiceBenchmark.Value));

                if (!indiceCollection.Query.Load())
                    throw new Exception("Id IndiceBenchmark não existente : " + valoresExcel.IdIndiceBenchmark);
                else
                    idIndiceBenchmark = indiceCollection[0].IdIndice.Value;
            }
            else
            {
                indiceCollection.Query.Where(indiceCollection.Query.Descricao.Like("%CDI%"));

                indiceCollection.Query.Load();

                if (indiceCollection.Count == 0)
                {
                    indiceCollection.QueryReset();
                    indiceCollection.LoadAll();
                }

                idIndiceBenchmark = indiceCollection[0].IdIndice.Value;
            }
            carteira.IdIndiceBenchmark = idIndiceBenchmark;

            carteira.ProjecaoIRResgate = valoresExcel.ProjecaoIRCotista;
            carteira.StatusAtivo = (byte)valoresExcel.StatusAtivo;
            carteira.TipoCarteira = valoresExcel.TipoCarteira;
            carteira.TipoCota = valoresExcel.TipoCota;
            carteira.TipoCusto = valoresExcel.TipoCusto;
            carteira.TipoRentabilidade = (byte)TipoRentabilidadeFundo.FinalFinal;
            carteira.TipoTributacao = valoresExcel.TipoTributacao;
            carteira.TipoVisaoFundo = (byte)Financial.Captacao.Enums.TipoVisaoFundoRebate.NaoTrata;
            carteira.TruncaCota = valoresExcel.TruncaCota;
            carteira.TruncaQuantidade = "N";
            carteira.TruncaFinanceiro = "N";
            carteira.ContagemPrazoIOF = (byte)ContagemDiasPrazoIOF.Direta;
            carteira.PrioridadeOperacao = valoresExcel.PrioridadeOperacao;
            carteira.CompensacaoPrejuizo = (byte)TipoCompensacaoPrejuizo.Padrao;
            carteira.TipoCalculoRetorno = (byte)CalculoRetornoCarteira.Cota;
            carteira.ContaPrzIOFVirtual = (short)ContagemIOFVirtual.SomaDiasConvResg;

            if (valoresExcel.IdAgenteCustodiante != 0)    
			{		
				agente = new AgenteMercado();
                if (!agente.LoadByPrimaryKey(valoresExcel.IdAgenteCustodiante))
                    throw new Exception("Id.Agente Custodiante (Agente de Mercado) não existente : " + valoresExcel.IdAgenteCustodiante);		
				
                carteira.IdAgenteCustodiante = valoresExcel.IdAgenteCustodiante;     
			}				
            #endregion

            bool permissaoInternoAuto = ParametrosConfiguracaoSistema.Outras.PermissaoInternoAuto;

            #region Se tem permissão automatico para usuários internos, cria permissão para todos os usuários (internos) do sistema
            if (permissaoInternoAuto)
            {
                GrupoUsuarioQuery grupoUsuarioQuery = new GrupoUsuarioQuery("G");
                UsuarioQuery usuarioQuery = new UsuarioQuery("U");

                usuarioQuery.InnerJoin(grupoUsuarioQuery).On(grupoUsuarioQuery.IdGrupo == usuarioQuery.IdGrupo);
                usuarioQuery.Where(grupoUsuarioQuery.TipoPerfil.In((byte)TipoPerfilGrupo.Administrador, (byte)TipoPerfilGrupo.BackOffice));

                UsuarioCollection usuarioCollection = new UsuarioCollection();
                usuarioCollection.Load(usuarioQuery);

                foreach (Usuario usuarioPermissao in usuarioCollection)
                {
                    int idUsuario = usuarioPermissao.IdUsuario.Value;

                    PermissaoCliente permissaoCliente = new PermissaoCliente();
                    permissaoCliente.IdCliente = cliente.IdCliente;
                    permissaoCliente.IdUsuario = idUsuario;
                    permissaoClienteCollection.AttachEntity(permissaoCliente);
                }
            }
            #endregion

            idClienteNovo++;
        }

        using (esTransactionScope scope = new esTransactionScope())
        {
            // Salva PosicÃµes presentes no Excel
            pessoaTelefoneCollection.Save();
            pessoaEmailCollection.Save();
            //
            clienteCollection.Save();
            clienteBolsaCollection.Save();
            clienteBMFCollection.Save();
            clienteRendaFixaCollection.Save();
            clienteInterfaceCollection.Save();
            carteiraCollection.Save();
            permissaoClienteCollection.Save();
            //
            scope.Complete();
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name=""></param>
    public void CarregaCadastroClienteCarteira()
    {
        this.CarregaCadastroClienteCarteira(null);
    }

    /// <summary>
    /// Processa a Planilha de Cadastro Cliente após ter acabado o Upload
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void uplCadastroClienteCarteira_FileUploadComplete(object sender, FileUploadCompleteEventArgs e)
    {
        e.CallbackData = "";

        #region Trata Extensão Válida
        if (!ValoresExcelUtil.isExtensaoExcel(e.UploadedFile.FileName.Trim()))
        {
            e.CallbackData = "Importação Cadastro Cliente - Formato de Arquivo Inválido. Formato permitido: .xls ou .xlsx\n\n";
            return;
        }
        #endregion

        // Stream de bytes com o Conteudo do Arquivo Excel
        Stream sr = e.UploadedFile.FileContent;

        #region Processamento Arquivo Excel

        try
        {
            // Ler Arquivo
            this.LerArquivoCadastroClienteCarteira(sr);
            // Carrega Arquivo
            this.CarregaCadastroClienteCarteira();
        }
        catch (Exception e2)
        {
            e.CallbackData = "Importação Cadastro Cliente/Carteira - " + e2.Message;
            return;
        }

        new HistoricoLog().InsereHistoricoLog(DateTime.Now, DateTime.Now,
            "Importação Planilha Cadastro Cliente/Carteira",
            HttpContext.Current.User.Identity.Name, UtilitarioWeb.GetIP(Request), "", HistoricoLogOrigem.Outros);

        #endregion
    }
}
