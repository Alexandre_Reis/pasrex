﻿using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Financial.Web.Common;
using System.IO;
using Financial.Integracao.Excel.Util;
using Financial.Integracao.Excel;
using EntitySpaces.Interfaces;
using Financial.Common;
using Financial.Bolsa;
using Financial.Bolsa.Enums;
using Financial.Investidor;
using System.Collections.Generic;
using Financial.Investidor.Enums;
using DevExpress.Web;
using Financial.Common.Exceptions;
using Financial.Bolsa.Exceptions;
using Financial.Fundo;
using Financial.Util;
using Financial.Security;
using Financial.Web.Util;
using Financial.Security.Enums;

public partial class ImportacaoBasePage : BasePage {

    /* Estrutura do Excel */
    private List<ValoresExcelCalculoAdministracao> valoresExcelCalculoAdministracao = new List<ValoresExcelCalculoAdministracao>();
    //
    List<int> idTabelaDeletarCalculoAdministracao = new List<int>();
    List<DateTime> dataHistoricoDeletarCalculoAdministracao = new List<DateTime>();


    /// <summary>
    /// Leitura de um arquivo Excel
    /// Saida: List<ValoresExcel> com os valores armazenados no arquivo
    /// </summary>
    /// <param name="streamExcel">Stream de bytes de um arquivo Excel</param>
    /// <exception cref="Exception">Se ocorreu problemas</exception>
    private void LerArquivoCalculoAdministracao(Stream streamExcel) {

        #region Leitura do Arquivo Excel

        // Open Spreadsheet
        Bytescout.Spreadsheet.Spreadsheet document = new Bytescout.Spreadsheet.Spreadsheet();
        document.LoadFromStream(streamExcel);

        Bytescout.Spreadsheet.Worksheet workSheet = document.Workbook.Worksheets[0];
        //
        #region Confere Formato Arquivo
        string[] colunasConferencias = new string[] {"IdTabela","IdCarteira","ValorDia",
                                                        "ValorAcumulado","DataFimApropriacao","DataPagamento"};

        bool formato = ValoresExcelUtil.isExcelFormatoValido(colunasConferencias, workSheet);
        if (!formato) {
            string mensagem = "Formato Interno do Arquivo Inválido.\n";
            mensagem += "Linha 1 do Arquivo deve ser: \n";
            for (int i = 0; i < colunasConferencias.Length; i++) {
                int coluna = i + 1;
                mensagem += "\tColuna " + coluna + ": " + colunasConferencias[i] + "\n";
            }

            document.Close();
            document.Dispose();

            throw new Exception(mensagem);
        }
        #endregion
        //
        int index = 0;
        // This row,column index should be changed as per your need.
        // i.e. which cell in the excel you are interesting to read.
        //
        /* Formato: 1)IdTabela       - 2)IdCarteira         - 3)ValorDia
         *          4)ValorAcumulado - 5)DataFimApropriacao - 6)DataPagamento             
         */
        int linha = 1;
        int coluna1 = 0, coluna2 = 1, coluna3 = 2,
            coluna4 = 3, coluna5 = 4, coluna6 = 5;
        //
        try {
            // Enquanto tiver registro
            while (workSheet.Cell(linha, coluna1).Value != null) {
                ValoresExcelCalculoAdministracao item = new ValoresExcelCalculoAdministracao();
                
                try {
                    item.IdTabela = workSheet.Cell(linha, coluna1).ValueAsInteger;
                }
                catch (Exception ex) {
                    throw new Exception(ex.Message + " - IdTabela: linha " + (linha + 1));
                }

                if (workSheet.Cell(linha, coluna2).Value == null)
                    throw new Exception("IdCarteira não informado: linha " + (linha + 1));
                try
                {
                    item.IdCarteira = workSheet.Cell(linha, coluna2).ValueAsInteger;
                }
                catch (Exception ex) {
                    throw new Exception(ex.Message + " - IdCarteira: linha " + (linha + 1));
                }

                if (workSheet.Cell(linha, coluna3).Value == null)
                    throw new Exception("ValorDia não informado: linha " + (linha + 1));
                try
                {
                    item.ValorDia = Convert.ToDecimal(workSheet.Cell(linha, coluna3).Value);
                }
                catch (Exception ex) {
                    throw new Exception(ex.Message + " - ValorDia: linha " + (linha + 1));
                }

                if (workSheet.Cell(linha, coluna4).Value == null)
                    throw new Exception("ValorAcumulado não informado: linha " + (linha + 1));
                try
                {
                    item.ValorAcumulado = Convert.ToDecimal(workSheet.Cell(linha, coluna4).Value);
                }
                catch (Exception ex) {
                    throw new Exception(ex.Message + " - ValorAcumulado: linha " + (linha + 1));
                }

                if (workSheet.Cell(linha, coluna5).Value == null)
                    throw new Exception("DataFimApropriacao não informada: linha " + (linha + 1));
                try
                {
                    item.DataFimApropriacao = workSheet.Cell(linha, coluna5).ValueAsDateTime;
                }
                catch (Exception ex) {
                    throw new Exception(ex.Message + " - DataFimApropriacao: linha " + (linha + 1));
                }

                if (workSheet.Cell(linha, coluna6).Value == null)
                    throw new Exception("DataPagamento não informada: linha " + (linha + 1));
                try
                {
                    item.DataPagamento = workSheet.Cell(linha, coluna6).ValueAsDateTime;
                }
                catch (Exception ex) {
                    throw new Exception(ex.Message + " - DataPagamento: linha " + (linha + 1));
                }

                //
                this.valoresExcelCalculoAdministracao.Add(item);
                //
                index++;
                linha = 1 + index;
                //                
                Console.WriteLine("{0},{1},{2},{3},{4},{5}", item.IdTabela, item.IdCarteira, item.ValorDia,
                                    item.ValorAcumulado, item.DataFimApropriacao.ToString("dd/MM/yyyy"),
                                    item.DataPagamento.ToString("dd/MM/yyyy"));
            }
        }
        catch (Exception ex) {
            document.Close();
            document.Dispose();

            throw new Exception(ex.Message);
        }
        #endregion

        document.Close();
        document.Dispose();
    }

    /// <summary>
    /// Gera e carrega os Calculos Administração e Administração Histórico
    /// com o objeto List<ValoresExcel>
    /// </summary>
    /// <exception cref="Exception">Se ocorreu problemas</exception>
    private void CarregaCalculoAdministracao() {
        CalculoAdministracaoCollection calculoAdministracaoCollection = new CalculoAdministracaoCollection();
        CalculoAdministracaoCollection calculoAdministracaoDeletarCollection = new CalculoAdministracaoCollection();
        //
        CalculoAdministracaoHistoricoCollection calculoAdministracaoHistoricoCollection = new CalculoAdministracaoHistoricoCollection();
        CalculoAdministracaoHistoricoCollection calculoAdministracaoHistoricoDeletarCollection = new CalculoAdministracaoHistoricoCollection();
        //    
        for (int i = 0; i < this.valoresExcelCalculoAdministracao.Count; i++) {
            ValoresExcelCalculoAdministracao valoresExcel = this.valoresExcelCalculoAdministracao[i];
            // Salva o IdTabela para poder Deletar
            this.idTabelaDeletarCalculoAdministracao.Add(valoresExcel.IdTabela);
            //               
            #region Verifica se Existe Carteira
            Carteira carteira = new Carteira();
            if (!carteira.LoadByPrimaryKey(valoresExcel.IdCarteira)) {
                throw new Exception("Carteira Não existente : " + valoresExcel.IdCarteira);
            }
            #endregion

            #region Verifica se Existe idTabela
            TabelaTaxaAdministracao tabelaTaxaAdministracao = new TabelaTaxaAdministracao();
            if (!tabelaTaxaAdministracao.LoadByPrimaryKey(valoresExcel.IdTabela)) {
                throw new Exception("Tabela Taxa Administração não existente : " + valoresExcel.IdTabela);
            }
            #endregion

            #region pega o Cliente
            Cliente cliente = new Cliente();
            List<esQueryItem> campos = new List<esQueryItem>();
            campos.Add(cliente.Query.IdCliente);
            campos.Add(cliente.Query.DataDia);
            cliente.LoadByPrimaryKey(campos, valoresExcel.IdCarteira);
            // Salva a Data Historico para poder Deletar
            DateTime dataAuxiliar = Calendario.SubtraiDiaUtil(cliente.DataDia.Value, 1);
            this.dataHistoricoDeletarCalculoAdministracao.Add(dataAuxiliar);
            #endregion

            #region CalculoAdministracao
            //
            CalculoAdministracao calculoAdministracao = new CalculoAdministracao();
            calculoAdministracao.IdTabela = valoresExcel.IdTabela;
            calculoAdministracao.IdCarteira = valoresExcel.IdCarteira;
            calculoAdministracao.ValorDia = valoresExcel.ValorDia;
            calculoAdministracao.ValorAcumulado = valoresExcel.ValorAcumulado;
            calculoAdministracao.DataFimApropriacao = valoresExcel.DataFimApropriacao;
            calculoAdministracao.DataPagamento = valoresExcel.DataPagamento;
            //
            calculoAdministracao.ValorCPMFDia = 0;
            calculoAdministracao.ValorCPMFAcumulado = 0;

            // Attach the entity
            calculoAdministracaoCollection.AttachEntity(calculoAdministracao);

            #endregion
            //
            #region CalculoAdministracaoHistórico
            //
            CalculoAdministracaoHistorico calculoAdministracaoHistorico = new CalculoAdministracaoHistorico();
            calculoAdministracaoHistorico.DataHistorico = dataAuxiliar;
            calculoAdministracaoHistorico.IdTabela = calculoAdministracao.IdTabela;
            calculoAdministracaoHistorico.IdCarteira = calculoAdministracao.IdCarteira;
            calculoAdministracaoHistorico.ValorDia = calculoAdministracao.ValorDia;
            calculoAdministracaoHistorico.ValorAcumulado = calculoAdministracao.ValorAcumulado;
            calculoAdministracaoHistorico.DataFimApropriacao = calculoAdministracao.DataFimApropriacao;
            calculoAdministracaoHistorico.DataPagamento = calculoAdministracao.DataPagamento;
            calculoAdministracaoHistorico.ValorCPMFDia = calculoAdministracao.ValorCPMFDia;
            calculoAdministracaoHistorico.ValorCPMFAcumulado = calculoAdministracao.ValorCPMFAcumulado;

            // Attach the object
            calculoAdministracaoHistoricoCollection.AttachEntity(calculoAdministracaoHistorico);
            #endregion
        }

        using (esTransactionScope scope = new esTransactionScope()) {

            #region Deleta CalculoAdministracao por Idtabela
            calculoAdministracaoDeletarCollection.Query.Where(calculoAdministracaoDeletarCollection.Query.IdTabela.In(this.idTabelaDeletarCalculoAdministracao));
            calculoAdministracaoDeletarCollection.Query.Load();
            calculoAdministracaoDeletarCollection.MarkAllAsDeleted();
            calculoAdministracaoDeletarCollection.Save();
            #endregion

            #region Deleta CalculoAdministracaoHistorico por IdTabela/DataHistorico
            for (int i = 0; i < this.idTabelaDeletarCalculoAdministracao.Count; i++) {
                calculoAdministracaoHistoricoDeletarCollection = new CalculoAdministracaoHistoricoCollection();
                calculoAdministracaoHistoricoDeletarCollection.Query
                    .Where(calculoAdministracaoHistoricoDeletarCollection.Query.IdTabela == this.idTabelaDeletarCalculoAdministracao[i],
                           calculoAdministracaoHistoricoDeletarCollection.Query.DataHistorico == this.dataHistoricoDeletarCalculoAdministracao[i]);
                calculoAdministracaoHistoricoDeletarCollection.Query.Load();
                calculoAdministracaoHistoricoDeletarCollection.MarkAllAsDeleted();
                calculoAdministracaoHistoricoDeletarCollection.Save();
            }
            #endregion

            // Salva Calculo Administracao presente no Excel
            calculoAdministracaoCollection.Save();

            // Salva Calculo Administracao Historico presente no Excel
            calculoAdministracaoHistoricoCollection.Save();
            //
            scope.Complete();
        }
    }

    /// <summary>
    /// Processa a Planilha de Calculo Administração após ter acabado o Upload
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void uplCalculoAdministracao_FileUploadComplete(object sender, FileUploadCompleteEventArgs e) {
        e.CallbackData = "";

        #region Trata Extensão Válida
        if (!ValoresExcelUtil.isExtensaoExcel(e.UploadedFile.FileName.Trim())) {
            e.CallbackData = "Importação Cálculo Administração - Formato de Arquivo Inválido. Formato permitido: .xls ou .xlsx\n\n";
            return;
        }
        #endregion

        // Stream de bytes com o Conteudo do Arquivo Excel
        Stream sr = e.UploadedFile.FileContent;

        #region Processamento Arquivo Excel

        //Reseta vetor de dados
        this.valoresExcelCalculoAdministracao = new List<ValoresExcelCalculoAdministracao>();
        this.idTabelaDeletarCalculoAdministracao = new List<int>();
        this.dataHistoricoDeletarCalculoAdministracao = new List<DateTime>();

        try {
            // Ler Arquivo
            this.LerArquivoCalculoAdministracao(sr);
            // Carrega Arquivo
            this.CarregaCalculoAdministracao();
        }
        catch (Exception e2) {
            e.CallbackData = "Importação Cálculo Administração - " + e2.Message;
            return;
        }

        new HistoricoLog().InsereHistoricoLog(DateTime.Now, DateTime.Now,
            "Importação Planilha Cálculo Administração",
            HttpContext.Current.User.Identity.Name, UtilitarioWeb.GetIP(Request), "", HistoricoLogOrigem.Outros);

        #endregion
    }
}