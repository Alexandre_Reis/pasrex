﻿using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Web.UI.HtmlControls;
using System.Collections.Generic;
using System.Text;
using Financial.Web.Common;
using Financial.Interfaces.Import.RendaFixa;
using Financial.InvestidorCotista;
using Financial.Fundo;
using DevExpress.Web;
using Financial.Interfaces.Import.Fundo;

public partial class ImportacaoBasePage : BasePage {
    
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void uplMovimentoSMAFundo_FileUploadComplete(object sender, FileUploadCompleteEventArgs e) {
        this.uplMovimentoSMA_FileUploadComplete(sender, e, false);
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    /// <param name="isCotista">Indica se é Cotista ou Fundo</param>
    private void uplMovimentoSMA_FileUploadComplete(object sender, FileUploadCompleteEventArgs e, bool isCotista) {
        e.CallbackData = "";

        #region Trata Extensão Válida

        if (!this.isExtensaoValida(e.UploadedFile.FileName.Trim())) {
            e.CallbackData = "Arquivo Movimento SMA inválido: Extensão do arquivo deve ser .txt, .dat ou nula\n\n";
            return;
        }
        #endregion

        // Stream de bytes com o Conteudo do Arquivo Excel
        Stream sr = e.UploadedFile.FileContent;

        #region Processamento Arquivo Excel
        try {
            SMA sma = new SMA();
            SMA.MovimentoPassivo[] movimentos = sma.ImportaMovimentoPassivo(sr);

            OperacaoFundoCollection operacaoFundoCollection = new OperacaoFundoCollection();
            operacaoFundoCollection.ImportaMovimentoSMA(movimentos);
        }
        catch (Exception e2) {
            e.CallbackData = "Importação Movimento SMA - " + e2.Message;
            return;
        }
        #endregion
    }         
}