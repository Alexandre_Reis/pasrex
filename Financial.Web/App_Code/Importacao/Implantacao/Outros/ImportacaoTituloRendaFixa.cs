﻿using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Financial.Web.Common;
using System.IO;
using Financial.Integracao.Excel.Util;
using Financial.Integracao.Excel;
using EntitySpaces.Interfaces;
using Financial.Common;
using Financial.Bolsa;
using Financial.Bolsa.Enums;
using Financial.Investidor;
using System.Collections.Generic;
using Financial.Investidor.Enums;
using DevExpress.Web;
using Financial.Common.Exceptions;
using Financial.Bolsa.Exceptions;
using Financial.RendaFixa;
using System.Text;
using Financial.RendaFixa.Enums;
using Financial.Common.Enums;
using Financial.Security;
using Financial.Web.Util;
using Financial.Security.Enums;
using Financial.Fundo;
using System.Text.RegularExpressions;

public partial class ImportacaoBasePage : BasePage {

    /* Estrutura do Excel */
    public List<ValoresExcelTituloRendaFixa> valoresExcelTituloRendaFixa = new List<ValoresExcelTituloRendaFixa>();
    //
    List<int> idTituloDeletarAgendaRendaFixa = new List<int>();

    /// <summary>
    /// Leitura de um arquivo Excel
    /// Saida: List<ValoresExcel> com os valores armazenados no arquivo
    /// </summary>
    /// <param name="streamExcel">Stream de bytes de um arquivo Excel</param>
    /// <exception cref="Exception">Se ocorreu problemas</exception>
    private void LerArquivoTituloRendaFixa(Stream streamExcel) {

        #region Leitura do Arquivo Excel

        // Open Spreadsheet
        Bytescout.Spreadsheet.Spreadsheet document = new Bytescout.Spreadsheet.Spreadsheet();
        document.LoadFromStream(streamExcel);

        Bytescout.Spreadsheet.Worksheet workSheet = document.Workbook.Worksheets[0];

        #region Confere Formato Arquivo
        string[] colunasConferencias = new string[] {"IdTitulo","idPapel","idIndice",
                                                     "Descricao","Taxa","Percentual","DataEmissao",
                                                     "DataVencimento","PuNominal","IsentoIR","IsentoIOF",
                                                     "Emissor",	"CodigoCustodia",	"IdMoeda",	"IdEstrategia",	
                                                     "CodigoCDA","CodigoIsin","CodigoCetip","CodigoCBLC",
                                                     "DebentureConversivel","CodigoInterface","DebentureInfra",
                                                     "PremioRebate","Periodicidade","CriterioAmortizacao",
                                                     "PermiteRepactuacao","ExecutaProRataEmissao","TipoProRata",
                                                     "DefasagemLiquidacao","ProRataLiquidacao","ProRataEmissaoDia",
                                                     "AtivoRegra","DataInicioCorrecao","DefasagemMeses","EJuros","CodigoCusip",
                                                     "EjurosTipo","EmpSecuritizada","CodigoBDS",
                                                     "OpcaoEmbutida","TipoOpcao","TipoExercicio","TipoPremio","DataInicioExercicio",
                                                     "DataVencimentoOpcao","ValorPremio"
        };

        bool formato = ValoresExcelUtil.isExcelFormatoValido(colunasConferencias, workSheet);
        if (!formato) {
            string mensagem = "Formato Interno do Arquivo Inválido.\n";
            mensagem += "Linha 1 do Arquivo deve ser: \n";
            for (int i = 0; i < colunasConferencias.Length; i++) {
                int coluna = i + 1;
                mensagem += "\tColuna " + coluna + ": " + colunasConferencias[i] + "\n";
            }

            document.Close();
            document.Dispose();

            throw new Exception(mensagem);
        }
        #endregion
        //
        int index = 0;
        // This row,column index should be changed as per your need.
        // i.e. which cell in the excel you are interesting to read.
        //
        /* Formato: 1)IdTitulo       - 2)IdClasse         - 3)IdIndice
         *          4)Descricao      - 5)Taxa             - 6)Percentual
         *          7)DataEmissao    - 8)DataVencimento   - 9)PuNominal
         *          10)IsentoIR      - 11)IsentoIOF       - 12)IdEmissor
         *          13)CodigoCustodia
         *          14)IdMoeda       - 15)IdEstrategia    - 16)CodigoCDA
         *          17)CodigoIsin    - 18)CodigoCetip     - 19)CodigoCBLC
         *          20)DebentureConversivel - 21)CodigoInterface        - 22)DebentureInfra
         *          23)PremioRebate         - 24)Periodicidade          - 25)CriterioAmortizacao
         *          26)PermiteRepactuacao   - 27)ExecutaProRataEmissao  - 28)TipoProRata
         *          29)DefasagemLiquidacao  - 30)ProRataLiquidacao      - 31)ProRataEmissaoDia
         *          32)AtivoRegra           - 33)DataInicioCorrecao     - 34)DefasagemMeses
         *          35)EJuros               - 36)CodigoCusip            - 37)eJurosTipo
         *          38)EmpSecuritizada      - 39)CodigoBDS              - 40)OpcaoEmbutida	
         *          41)TipoOpcao	        - 42)TipoExercicio	        - 43)TipoPremio	
         *          44)DataInicioExercicio	- 45)DataVencimentoOpcao	- 46)ValorPremio
         */
        int linha = 1;
        int coluna1 = 0, coluna2 = 1, coluna3 = 2,
            coluna4 = 3, coluna5 = 4, coluna6 = 5, coluna7 = 6,
            coluna8 = 7, coluna9 = 8, coluna10 = 9, coluna11 = 10,
            coluna12 = 11, coluna13 = 12, coluna14 = 13, coluna15 = 14,
            coluna16 = 15, coluna17 = 16, coluna18 = 17, coluna19 = 18,
            coluna20 = 19, coluna21 = 20, coluna22 = 21, coluna23 = 22,
            coluna24 = 23, coluna25 = 24, coluna26 = 25, coluna27 = 26,
            coluna28 = 27, coluna29 = 28, coluna30 = 29, coluna31 = 30,
            coluna32 = 31, coluna33 = 32, coluna34 = 33, coluna35 = 34,
            coluna36 = 35, coluna37 = 36, coluna38 = 37, coluna39 = 38,
            coluna40 = 39, coluna41 = 40, coluna42 = 41, coluna43 = 42,
            coluna44 = 43, coluna45 = 44, coluna46 = 45;

        //
        try {
            // Enquanto tiver registro
            while (workSheet.Cell(linha, coluna2).Value != null) {

                ValoresExcelTituloRendaFixa item = new ValoresExcelTituloRendaFixa();
                
                try {
                    item.IdTitulo = workSheet.Cell(linha, coluna1).ValueAsInteger;
                }
                catch (Exception ex) {
                    throw new Exception(ex.Message + " - IdTitulo: linha " + (linha + 1));
                }

                if (workSheet.Cell(linha, coluna2).Value == null)
                    throw new Exception("IdClasse não informado: linha " + (linha + 1));
                try
                {
                    item.IdClasse = (ClasseRendaFixa)workSheet.Cell(linha, coluna2).ValueAsInteger;
                }
                catch (Exception ex) {
                    throw new Exception(ex.Message + " - IdClasse: linha " + (linha + 1));
                }

                if (workSheet.Cell(linha, coluna3).Value != null)
                {
                    try {
                        item.IdIndice = (EnumIndice)workSheet.Cell(linha, coluna3).ValueAsInteger;
                    }
                    catch (Exception ex) {
                        throw new Exception(ex.Message + " - IdIndice: linha " + (linha + 1));
                    }
                }
                else { // Indice Nulo
                    item.IdIndice = EnumIndice.NONE;
                }

                if (workSheet.Cell(linha, coluna4).Value == null)
                    throw new Exception("Descricao não informado: linha " + (linha + 1));
                try
                {
                    item.Descricao = workSheet.Cell(linha, coluna4).ValueAsString;
                }
                catch (Exception ex) {
                    throw new Exception(ex.Message + " - Descricao: linha " + (linha + 1));
                }

                if (workSheet.Cell(linha, coluna5).Value != null)
                {
                    try {
                        item.Taxa = Convert.ToDecimal(workSheet.Cell(linha, coluna5).Value);
                    }
                    catch (Exception ex) {
                        throw new Exception(ex.Message + " - Taxa: linha " + (linha + 1));
                    }
                }

                if (workSheet.Cell(linha, coluna6).Value != null)
                {
                    try {
                        item.Percentual = Convert.ToDecimal(workSheet.Cell(linha, coluna6).Value);
                    }
                    catch (Exception ex) {
                        throw new Exception(ex.Message + " - Percentual: linha " + (linha + 1));
                    }
                }

                if (workSheet.Cell(linha, coluna7).Value == null)
                    throw new Exception("DataEmissao não informado: linha " + (linha + 1));
                try
                {
                    item.DataEmissao = workSheet.Cell(linha, coluna7).ValueAsDateTime;
                }
                catch (Exception ex) {
                    throw new Exception(ex.Message + " - DataEmissao: linha " + (linha + 1));
                }

                if (workSheet.Cell(linha, coluna8).Value == null)
                    throw new Exception("DataVencimento não informado: linha " + (linha + 1));
                try
                {
                    item.DataVencimento = workSheet.Cell(linha, coluna8).ValueAsDateTime;
                }
                catch (Exception ex) {
                    throw new Exception(ex.Message + " - DataVencimento: linha " + (linha + 1));
                }

                if (workSheet.Cell(linha, coluna9).Value == null)
                    throw new Exception("PuNominal não informado: linha " + (linha + 1));
                try
                {
                    item.PuNominal = Convert.ToDecimal(workSheet.Cell(linha, coluna9).Value);
                }
                catch (Exception ex) {
                    throw new Exception(ex.Message + " - PuNominal: linha " + (linha + 1));
                }

                if (workSheet.Cell(linha, coluna10).Value == null)
                    throw new Exception("IsentoIR não informado: linha " + (linha + 1));
                try
                {
                    item.IsentoIR = workSheet.Cell(linha, coluna10).ValueAsString;
                }
                catch (Exception ex) {
                    throw new Exception(ex.Message + " - IsentoIR: linha " + (linha + 1));
                }

                if (workSheet.Cell(linha, coluna11).Value == null)
                    throw new Exception("IsentoIOF não informado: linha " + (linha + 1));
                try
                {
                    item.IsentoIOF = workSheet.Cell(linha, coluna11).ValueAsString;
                }
                catch (Exception ex) {
                    throw new Exception(ex.Message + " - IsentoIOF: linha " + (linha + 1));
                }

                #region IdEmissor
                if (workSheet.Cell(linha, coluna12).Value != null)
                {
                    try
                    {
                        item.IdEmissor = workSheet.Cell(linha, coluna12).ValueAsInteger;
                    }
                    catch (Exception ex)
                    {
                        throw new Exception(ex.Message + " - IdEmissor: linha " + (linha + 1));
                    }
                }
                else
                { // IdEmissor Nulo
                    item.IdEmissor = (int)ListaEmissorFixo.Padrao;
                }
                #endregion

                #region CodigoCustodia
                if (workSheet.Cell(linha, coluna13).Value != null)
                {
                    item.CodigoCustodia = workSheet.Cell(linha, coluna13).ValueAsString;
                }
                else
                {
                    item.CodigoCustodia = null;
                }
                #endregion

                #region idMoeda
                if (workSheet.Cell(linha, coluna14).Value != null)
                {
                    try
                    {
                        item.IdMoeda = workSheet.Cell(linha, coluna14).ValueAsInteger;
                    }
                    catch (Exception ex)
                    {
                        throw new Exception(ex.Message + " - idMoeda: linha " + (linha + 1));
                    }
                }
                else
                {
                    throw new Exception("idMoeda não informado: linha " + (linha + 1));
                }
                #endregion


                #region idEstrategia
                if (workSheet.Cell(linha, coluna15).Value != null)
                {
                    try
                    {
                        item.IdEstrategia = workSheet.Cell(linha, coluna15).ValueAsInteger;
                    }
                    catch (Exception ex)
                    {
                        throw new Exception(ex.Message + " - idEstrategia: linha " + (linha + 1));
                    }
                }
                else
                {
                    item.IdEstrategia = null;
                }
                #endregion

                #region codigoCDA
                if (workSheet.Cell(linha, coluna16).Value != null)
                {
                    try
                    {
                        item.CodigoCDA = workSheet.Cell(linha, coluna16).ValueAsInteger;
                    }
                    catch (Exception ex)
                    {
                        throw new Exception(ex.Message + " - codigoCDA: linha " + (linha + 1));
                    }
                }
                else
                {
                    item.CodigoCDA = null;
                }
                #endregion

                #region codigoIsin
                if (workSheet.Cell(linha, coluna17).Value != null)
                {
                    item.CodigoIsin = workSheet.Cell(linha, coluna17).ValueAsString;
                }
                else
                {
                    item.CodigoIsin = null;
                }
                #endregion

                #region codigoCetip
                if (workSheet.Cell(linha, coluna18).Value != null)
                {
                    item.CodigoCetip = workSheet.Cell(linha, coluna18).ValueAsString;
                }
                else
                {
                    item.CodigoCetip = null;
                }
                #endregion

                #region codigoCBLC
                if (workSheet.Cell(linha, coluna19).Value != null)
                {
                    item.CodigoCBLC = workSheet.Cell(linha, coluna19).ValueAsString;
                }
                else
                {
                    item.CodigoCBLC = null;
                }
                #endregion

                #region debentureConversivel
                if (workSheet.Cell(linha, coluna20).Value != null)
                {
                    item.DebentureConversivel = char.ToUpper(Convert.ToChar(workSheet.Cell(linha, coluna20).ValueAsString));
                }
                else
                {
                    item.DebentureConversivel = 'N';
                }
                #endregion

                #region codigoInterface
                if (workSheet.Cell(linha, coluna21).Value != null)
                {
                    item.CodigoInterface = workSheet.Cell(linha, coluna21).ValueAsString;
                }
                else
                {
                    item.CodigoInterface = null;
                }
                #endregion

                #region debentureInfra
                if (workSheet.Cell(linha, coluna22).Value != null)
                {
                    item.DebentureInfra = char.ToUpper(Convert.ToChar(workSheet.Cell(linha, coluna22).ValueAsString));
                }
                else
                {
                    item.DebentureInfra = 'N';
                }
                #endregion

                #region premioRebate
                if (workSheet.Cell(linha, coluna23).Value != null)
                {
                    try
                    {
                        item.PremioRebate = Convert.ToDecimal(workSheet.Cell(linha, coluna23).ValueAsDouble);
                    }
                    catch (Exception ex)
                    {
                        throw new Exception(ex.Message + " - premioRebate: linha " + (linha + 1));
                    }
                }
                else
                {
                    item.PremioRebate = null;
                }
                #endregion

                #region periodicidade
                if (workSheet.Cell(linha, coluna24).Value != null)
                {
                    try
                    {
                        item.Periodicidade = workSheet.Cell(linha, coluna24).ValueAsInteger;
                    }
                    catch (Exception ex)
                    {
                        throw new Exception(ex.Message + " - periodicidade: linha " + (linha + 1));
                    }
                }
                else
                {
                    item.Periodicidade = null;
                }
                #endregion

                #region criterioAmortizacao
                if (workSheet.Cell(linha, coluna25).Value != null)
                {
                    try
                    {
                        item.CriterioAmortizacao = workSheet.Cell(linha, coluna25).ValueAsInteger;
                    }
                    catch (Exception ex)
                    {
                        throw new Exception(ex.Message + " - criterioAmortizacao: linha " + (linha + 1));
                    }
                }
                else
                {
                    throw new Exception("criterioAmortizacao não informado: linha " + (linha + 1));
                }
                #endregion

                #region permiteRepactuacao
                if (workSheet.Cell(linha, coluna26).Value != null)
                {
                    item.PermiteRepactuacao = char.ToUpper(Convert.ToChar(workSheet.Cell(linha, coluna26).ValueAsString));
                }
                else
                {
                    item.PermiteRepactuacao = 'N';
                }
                #endregion

                #region executaProRataEmissao
                if (workSheet.Cell(linha, coluna27).Value != null)
                {
                    item.ExecutaProRataEmissao = char.ToUpper(Convert.ToChar(workSheet.Cell(linha, coluna27).ValueAsString));
                }
                else
                {
                    item.ExecutaProRataEmissao = 'N';
                }
                #endregion

                #region tipoProRata
                if (workSheet.Cell(linha, coluna28).Value != null)
                {
                    try
                    {
                        item.TipoProRata = workSheet.Cell(linha, coluna28).ValueAsInteger;
                    }
                    catch (Exception ex)
                    {
                        throw new Exception(ex.Message + " - tipoProRata: linha " + (linha + 1));
                    }
                }
                else
                {
                    //item.TipoProRata = null;
                }
                #endregion

                #region defasagemLiquidacao
                if (workSheet.Cell(linha, coluna29).Value != null)
                {
                    try
                    {
                        item.DefasagemLiquidacao = workSheet.Cell(linha, coluna29).ValueAsInteger;
                    }
                    catch (Exception ex)
                    {
                        throw new Exception(ex.Message + " - defasagemLiquidacao: linha " + (linha + 1));
                    }
                }
                else
                {
                    item.DefasagemLiquidacao = 0;
                }
                #endregion

                #region proRataLiquidacao
                if (workSheet.Cell(linha, coluna30).Value != null)
                {
                    item.ProRataLiquidacao = char.ToUpper(Convert.ToChar(workSheet.Cell(linha, coluna30).ValueAsString));

                }
                else
                {
                    item.ProRataLiquidacao = 'N';
                }
                #endregion

                #region proRataEmissaoDia
                if (workSheet.Cell(linha, coluna31).Value != null)
                {
                    try
                    {
                        item.ProRataEmissaoDia = workSheet.Cell(linha, coluna31).ValueAsInteger;
                    }
                    catch (Exception ex)
                    {
                        throw new Exception(ex.Message + " - proRataEmissaoDia: linha " + (linha + 1));
                    }
                }
                else
                {
                    item.ProRataEmissaoDia = null;
                }
                #endregion

                #region ativoRegra
                if (workSheet.Cell(linha, coluna32).Value != null)
                {
                    item.AtivoRegra = workSheet.Cell(linha, coluna32).ValueAsString;
                }
                else
                {
                    item.AtivoRegra = "1"; //Sem regra especifica
                }
                #endregion

                #region dataInicioCorrecao
                if (workSheet.Cell(linha, coluna33).Value != null)
                {
                    try
                    {
                        item.DataInicioCorrecao = workSheet.Cell(linha, coluna33).ValueAsDateTime;
                    }
                    catch (Exception ex)
                    {
                        throw new Exception(ex.Message + " - dataInicioCorrecao: linha " + (linha + 1));
                    }
                }
                else
                {
                    item.DataInicioCorrecao = null;
                }
                #endregion

                #region defasagemMeses
                if (workSheet.Cell(linha, coluna34).Value != null)
                {
                    try
                    {
                        item.DefasagemMeses = workSheet.Cell(linha, coluna34).ValueAsInteger;
                    }
                    catch (Exception ex)
                    {
                        throw new Exception(ex.Message + " - defasagemMeses: linha " + (linha + 1));
                    }
                }
                else
                {
                    item.DefasagemMeses = null;
                }
                #endregion

                #region eJuros
                if (workSheet.Cell(linha, coluna35).Value != null)
                {
                    try
                    {
                        item.EJuros = workSheet.Cell(linha, coluna35).ValueAsInteger;
                    }
                    catch (Exception ex)
                    {
                        throw new Exception(ex.Message + " - eJuros: linha " + (linha + 1));
                    }
                }
                else
                {
                    item.EJuros = null;
                }
                #endregion

                #region codigoCusip
                if (workSheet.Cell(linha, coluna36).Value != null)
                {
                    item.CodigoCusip = workSheet.Cell(linha, coluna36).ValueAsString;
                }
                else
                {
                    item.CodigoCusip = null;
                }
                #endregion

                #region eJurosTipo
                if (workSheet.Cell(linha, coluna37).Value != null)
                {
                    try
                    {
                        item.EJurosTipo = workSheet.Cell(linha, coluna37).ValueAsInteger;
                    }
                    catch (Exception ex)
                    {
                        throw new Exception(ex.Message + " - eJurosTipo: linha " + (linha + 1));
                    }
                }
                else
                {
                    item.EJurosTipo = null;
                }
                #endregion

                #region EmpSecuritizada
                if (workSheet.Cell(linha, coluna38).Value != null)
                {
                    try
                    {
                        item.EmpSecuritizada = workSheet.Cell(linha, coluna38).ValueAsString;

                        Regex rx = new Regex(@"([0-9;])");
                        if (!rx.IsMatch(item.EmpSecuritizada))
                            throw new Exception("O campo empSecuritizada aceita apenas números separados por ';' ou ',': linha " + (linha + 1));
                    }
                    catch (Exception ex)
                    {
                        throw new Exception(ex.Message + " - EmpSecuritizada: linha " + (linha + 1));
                    }
                }
                else
                {
                    item.EmpSecuritizada = string.Empty;
                }
                #endregion

                #region codigoBDS
                if (workSheet.Cell(linha, coluna39).Value != null)
                {
                    try
                    {
                        item.CodigoBDS = workSheet.Cell(linha, coluna39).ValueAsInteger;
                    }
                    catch (Exception ex)
                    {
                        throw new Exception(ex.Message + " - codigoCDA: linha " + (linha + 1));
                    }
                }
                else
                {
                    item.CodigoBDS = null;
                }
                #endregion

                #region OpcaoEmbutida
                item.OpcaoEmbutida = "N";
                if (workSheet.Cell(linha, coluna40).Value != null)
                {
                    item.OpcaoEmbutida = workSheet.Cell(linha, coluna40).ValueAsString;
                }
                #endregion

                #region TipoOpcao
                if (workSheet.Cell(linha, coluna41).Value != null)
                {
                    try
                    {
                        item.TipoOpcao = workSheet.Cell(linha, coluna41).ValueAsInteger;
                    }
                    catch (Exception ex)
                    {
                        throw new Exception(ex.Message + " - TipoOpcao: linha " + (linha + 1));
                    }
                }
                else
                {
                    item.TipoOpcao = null;
                }
                #endregion

                #region TipoExercicio
                if (workSheet.Cell(linha, coluna42).Value != null)
                {
                    try
                    {
                        item.TipoExercicio = workSheet.Cell(linha, coluna42).ValueAsInteger;
                    }
                    catch (Exception ex)
                    {
                        throw new Exception(ex.Message + " - TipoExercicio: linha " + (linha + 1));
                    }
                }
                else
                {
                    item.TipoExercicio = null;
                }
                #endregion

                #region TipoPremio
                if (workSheet.Cell(linha, coluna43).Value != null)
                {
                    try
                    {
                        item.TipoPremio = workSheet.Cell(linha, coluna43).ValueAsInteger;
                    }
                    catch (Exception ex)
                    {
                        throw new Exception(ex.Message + " - TipoPremio: linha " + (linha + 1));
                    }
                }
                else
                {
                    item.TipoPremio = null;
                }
                #endregion

                #region DataInicioExercicio
                if (workSheet.Cell(linha, coluna44).Value != null)
                {
                    try
                    {
                        item.DataInicioExercicio = workSheet.Cell(linha, coluna44).ValueAsDateTime;
                    }
                    catch (Exception ex)
                    {
                        throw new Exception(ex.Message + " - DataInicioExercicio: linha " + (linha + 1));
                    }
                }
                else
                {
                    item.DataInicioExercicio = null;
                }
                #endregion

                #region DataVencimentoOpcao
                if (workSheet.Cell(linha, coluna45).Value != null)
                {
                    try
                    {
                        item.DataVencimentoOpcao = workSheet.Cell(linha, coluna45).ValueAsDateTime;
                    }
                    catch (Exception ex)
                    {
                        throw new Exception(ex.Message + " - DataVencimentoOpcao: linha " + (linha + 1));
                    }
                }
                else
                {
                    item.DataVencimentoOpcao = null;
                }
                #endregion

                #region ValorPremio
                if (workSheet.Cell(linha, coluna46).Value != null)
                {
                    try
                    {
                        item.ValorPremio = Convert.ToDecimal(workSheet.Cell(linha, coluna46).Value);
                    }
                    catch (Exception ex)
                    {
                        throw new Exception(ex.Message + " - ValorPremio: linha " + (linha + 1));
                    }
                }
                else
                {
                    item.ValorPremio = null;
                }
                #endregion

                this.valoresExcelTituloRendaFixa.Add(item);
                
                index++;
                linha = 1 + index;
                //                
                /*Console.WriteLine("{0},{1},{2},{3},{4},{5},{6},{7},{8},{9},{10},{11},{12},{13},{14},{15},{16},{17},{18},{19},{20},{21},{22},{23},{24},{25},{26},{27},{28},{29},{30},{31},{32},{33},{34},{35}",
                                    item.IdTitulo,
                                    item.IdClasse.ToString(),
                                    item.IdIndice.ToString(),
                                    item.Descricao,
                                    item.Taxa,
                                    item.Percentual,
                                    item.DataEmissao.ToString("dd/MM/yyyy"),
                                    item.DataVencimento.ToString("dd/MM/yyyy"),
                                    item.PuNominal,
                                    item.IsentoIR,
                                    item.IsentoIOF,
                                    item.IdEmissor,
                                    item.CodigoCustodia,
                                    item.IdMoeda,
                                    item.IdEstrategia,
                                    item.CodigoCDA,
                                    item.CodigoIsin,
                                    item.CodigoCetip,
                                    item.CodigoCBLC,
                                    item.DebentureConversivel,
                                    item.CodigoInterface,
                                    item.DebentureInfra,
                                    item.PremioRebate,
                                    item.Periodicidade,
                                    item.CriterioAmortizacao,
                                    item.PermiteRepactuacao,
                                    item.ExecutaProRataEmissao,
                                    item.TipoProRata,
                                    item.DefasagemLiquidacao,
                                    item.ProRataLiquidacao,
                                    item.ProRataEmissaoDia,
                                    item.AtivoRegra,
                                    item.DataInicioCorrecao.Value.ToString("dd/MM/yyyy"),
                                    item.DefasagemMeses,
                                    item.EJuros,
                                    item.CodigoCusip);*/
            }
        }
        catch (Exception ex) {
            document.Close();
            document.Dispose();

            throw new Exception(ex.Message);
        }
        #endregion

        document.Close();
        document.Dispose();
    }

    /// <summary>
    /// Gera e Carrega TituloRendaFixa
    /// com o objeto List<ValoresExcel>
    /// </summary>
    /// <exception cref="Exception">Se ocorreu problemas</exception>
    public void CarregaTituloRendaFixa() {
        TituloRendaFixaCollection tituloRendaFixaCollection = new TituloRendaFixaCollection();
        TituloRendaFixaCollection tituloRendaFixaSemIdCollection = new TituloRendaFixaCollection();

        EmpresaSecuritizadaCollection empresaSecColl = new EmpresaSecuritizadaCollection();
        Dictionary<int, int> dicEmpSecValidads = new Dictionary<int, int>();
        Dictionary<int, EmpresaSecuritizadaCollection> dicEmpSecSemIdTitulo = new Dictionary<int, EmpresaSecuritizadaCollection>();

        //

        for (int i = 0; i < this.valoresExcelTituloRendaFixa.Count; i++) {
            ValoresExcelTituloRendaFixa valoresExcel = this.valoresExcelTituloRendaFixa[i];
            //        
            TituloRendaFixa tituloRendaFixa = new TituloRendaFixa();

            if (valoresExcel.IdTitulo.GetValueOrDefault(0) != 0)
            {
                if (tituloRendaFixa.LoadByPrimaryKey(valoresExcel.IdTitulo.Value))
                {
                    throw new Exception("Id Titulo Renda Fixa já existente : " + valoresExcel.IdTitulo);
                }
            }

            #region Verifica se Existe idIndice
            Indice indice = new Indice();
            string descricaoIndice = "";
            if (valoresExcel.IdIndice != EnumIndice.NONE) {
                if (!indice.LoadByPrimaryKey((short)valoresExcel.IdIndice)) {
                    throw new Exception("Id Indice não existente : " + valoresExcel.IdIndice);
                }
                else {
                    descricaoIndice = indice.Descricao;
                }
            }
            #endregion

            #region TituloRendaFixa

            int? idPapel = null;
            PapelRendaFixaCollection p = new PapelRendaFixaCollection();
            p.Query.Select(p.Query.IdPapel).Where(p.Query.IdPapel.Equal((int)valoresExcel.IdClasse));
            if (!p.Query.Load())
            {
                throw new Exception("Não existe papel com a classe informada: " + (int)valoresExcel.IdClasse);
            }
            else
            {
                idPapel = p[0].IdPapel.Value;
            }
            //
            tituloRendaFixa = new TituloRendaFixa();

            if (valoresExcel.IdIndice != EnumIndice.NONE) {
                tituloRendaFixa.IdIndice = (short)valoresExcel.IdIndice;
            }

            if (valoresExcel.IdEmissor != 0)
            {
                Emissor emissor = new Emissor();
                emissor.Query.Where(emissor.Query.IdEmissor.Equal((int)valoresExcel.IdEmissor));
                if (!emissor.Query.Load())
                    throw new Exception("Não existe Emissor com o ID informado: " + (int)valoresExcel.IdEmissor);
                tituloRendaFixa.IdEmissor = valoresExcel.IdEmissor;
            }
            else
            {
                tituloRendaFixa.IdEmissor = 1; // Padrão
            }

            tituloRendaFixa.IdPapel = idPapel;
            tituloRendaFixa.Descricao = valoresExcel.Descricao.Trim();
            tituloRendaFixa.Taxa = valoresExcel.Taxa;
            tituloRendaFixa.Percentual = valoresExcel.Percentual;
            tituloRendaFixa.DataEmissao = valoresExcel.DataEmissao;
            tituloRendaFixa.DataVencimento = valoresExcel.DataVencimento;
            tituloRendaFixa.PUNominal = valoresExcel.PuNominal;
            tituloRendaFixa.ValorNominal = tituloRendaFixa.PUNominal;
            //
            tituloRendaFixa.DescricaoCompleta = this.MontaDescricaoCompleta(tituloRendaFixa.Descricao, tituloRendaFixa.DataVencimento.Value, tituloRendaFixa.Percentual, tituloRendaFixa.Taxa, descricaoIndice);
            //
            tituloRendaFixa.IsentoIR = valoresExcel.IsentoIR.ToUpper() == "S" ? (byte)TituloIsentoIR.Isento : (byte)TituloIsentoIR.NaoIsento;
            tituloRendaFixa.IsentoIOF = valoresExcel.IsentoIOF.ToUpper() == "S" ? (byte)TituloIsentoIOF.Isento : (byte)TituloIsentoIOF.NaoIsento;
            tituloRendaFixa.IdMoeda = (int)ListaMoedaFixo.Real;
            //
            tituloRendaFixa.CodigoIsin = valoresExcel.CodigoIsin;
            tituloRendaFixa.CodigoCetip = valoresExcel.CodigoCetip;

            tituloRendaFixa.IdEmissor = valoresExcel.IdEmissor;
            tituloRendaFixa.CodigoCustodia = valoresExcel.CodigoCustodia;
            tituloRendaFixa.IdMoeda = valoresExcel.IdMoeda;

            if (valoresExcel.IdEstrategia.GetValueOrDefault(0) != 0)
            {
                Estrategia estrategia = new Estrategia();
                if (!estrategia.LoadByPrimaryKey((int)valoresExcel.IdEstrategia))
                    throw new Exception("Não existe Estrategia com a ID informado: " + (int)valoresExcel.IdEstrategia);
                else
                    tituloRendaFixa.IdEstrategia = valoresExcel.IdEstrategia;
            }

            tituloRendaFixa.CodigoBDS = valoresExcel.CodigoBDS;
            tituloRendaFixa.CodigoCDA = valoresExcel.CodigoCDA;
            tituloRendaFixa.CodigoCBLC = valoresExcel.CodigoCBLC;
            tituloRendaFixa.DebentureConversivel = valoresExcel.DebentureConversivel.ToString();
            tituloRendaFixa.CodigoInterface = valoresExcel.CodigoInterface;
            tituloRendaFixa.DebentureInfra = valoresExcel.DebentureInfra.ToString();
            tituloRendaFixa.PremioRebate = valoresExcel.PremioRebate;
            tituloRendaFixa.Periodicidade = valoresExcel.Periodicidade;
            tituloRendaFixa.CriterioAmortizacao = valoresExcel.CriterioAmortizacao;
            tituloRendaFixa.PermiteRepactuacao = valoresExcel.PermiteRepactuacao.ToString();
            tituloRendaFixa.ExecutaProRataEmissao = valoresExcel.ExecutaProRataEmissao.ToString();
            tituloRendaFixa.TipoProRata = valoresExcel.TipoProRata;
            tituloRendaFixa.DefasagemLiquidacao = valoresExcel.DefasagemLiquidacao;
            tituloRendaFixa.ProRataLiquidacao = valoresExcel.ProRataLiquidacao.ToString();
            tituloRendaFixa.ProRataEmissaoDia = valoresExcel.ProRataEmissaoDia;
            tituloRendaFixa.AtivoRegra = valoresExcel.AtivoRegra;
            tituloRendaFixa.DataInicioCorrecao = valoresExcel.DataInicioCorrecao;
            tituloRendaFixa.DefasagemMeses = valoresExcel.DefasagemMeses;
            tituloRendaFixa.EJuros = valoresExcel.EJuros;
            tituloRendaFixa.EJurosTipo = valoresExcel.EJurosTipo;
            tituloRendaFixa.CodigoCusip = valoresExcel.CodigoCusip;

            tituloRendaFixa.OpcaoEmbutida = valoresExcel.OpcaoEmbutida;
            tituloRendaFixa.TipoOpcao = null;
            tituloRendaFixa.TipoExercicio = null;
            tituloRendaFixa.DataInicioExercicio = null;
            tituloRendaFixa.DataVencimentoOpcao = null;
            tituloRendaFixa.TipoPremio = null;
            tituloRendaFixa.ValorPremio = null;
            if (tituloRendaFixa.OpcaoEmbutida.Equals("S"))
            {
                if(!valoresExcel.TipoOpcao.HasValue)
                    throw new Exception("Tipo Opção não preenchido!!!");
                if (!valoresExcel.TipoExercicio.HasValue)
                    throw new Exception("Tipo Exercicio não preenchido!!!");
                if (!valoresExcel.DataInicioExercicio.HasValue)
                    throw new Exception("Data Inicio Exercicio não preenchido!!!");
                if (!valoresExcel.DataVencimentoOpcao.HasValue)
                    throw new Exception("Data Vencimento Opcao não preenchido!!!");
                if (!valoresExcel.TipoPremio.HasValue)
                    throw new Exception("Tipo Premio não preenchido!!!");
                if (!valoresExcel.ValorPremio.HasValue)
                    throw new Exception("Valor Premio não preenchido!!!");
                
                tituloRendaFixa.TipoOpcao = (short)valoresExcel.TipoOpcao.Value;
                tituloRendaFixa.TipoExercicio = (short)valoresExcel.TipoExercicio.Value;
                tituloRendaFixa.DataInicioExercicio = valoresExcel.DataInicioExercicio.Value;
                tituloRendaFixa.DataVencimentoOpcao = valoresExcel.DataVencimentoOpcao.Value;
                tituloRendaFixa.TipoPremio = (short)valoresExcel.TipoPremio.Value;
                tituloRendaFixa.ValorPremio = valoresExcel.ValorPremio.Value;
            }    

            bool registroSemId = valoresExcel.IdTitulo.GetValueOrDefault(0) == 0;

            if (!registroSemId)
            {
                tituloRendaFixa.IdTitulo = valoresExcel.IdTitulo;
                tituloRendaFixaCollection.AttachEntity(tituloRendaFixa);                                
            }
            else
            {
                tituloRendaFixaSemIdCollection.AttachEntity(tituloRendaFixa);                    
            }            
            #endregion

            #region Empresa Securitizada
            if (!string.IsNullOrEmpty(valoresExcel.EmpSecuritizada))
            {
                string[] codigos = valoresExcel.EmpSecuritizada.Split(';', ',', '.');

                EmpresaSecuritizadaCollection auxCollection = new EmpresaSecuritizadaCollection();

                foreach (string codigo in codigos)
                {
                    int codigoBovespa = Convert.ToInt32(codigo);
                    int? idAgenteMercado = null;

                    if (dicEmpSecValidads.ContainsKey(codigoBovespa))
                    {
                        idAgenteMercado = dicEmpSecValidads[codigoBovespa];
                    }
                    else
                    {
                        #region Testa AgenteMercado
                        AgenteMercado agenteMercado = new AgenteMercado();
                        try
                        {
                            idAgenteMercado = (int)agenteMercado.BuscaIdAgenteMercadoBovespa(codigoBovespa);
                        }
                        catch (IdAgenteNaoCadastradoException e)
                        {
                            throw new IdAgenteNaoCadastradoException(e.Message);
                        }
                        #endregion

                        dicEmpSecValidads.Add(codigoBovespa, idAgenteMercado.Value);
                    }

                    EmpresaSecuritizada empresa = auxCollection.AddNew();
                    empresa.IdAgenteMercado = idAgenteMercado.Value;
                    empresa.IdTitulo = valoresExcel.IdTitulo;
                }

                if (registroSemId)
                {                    
                    dicEmpSecSemIdTitulo.Add(tituloRendaFixaSemIdCollection.Count, auxCollection);
                }
                else
                {
                    empresaSecColl.Combine(auxCollection);
                }
            }
            else
            {
                if (registroSemId)
                    dicEmpSecSemIdTitulo.Add(tituloRendaFixaSemIdCollection.Count, new EmpresaSecuritizadaCollection());
            }
            #endregion
        }

        #region Verifica se Existe idTitulo Repetido na planilha
        List<int> ids = new List<int>();
        for (int i = 0; i < tituloRendaFixaCollection.Count; i++) {
            if (!ids.Contains(tituloRendaFixaCollection[i].IdTitulo.Value)) {
                ids.Add(tituloRendaFixaCollection[i].IdTitulo.Value);
            }
            else {
                throw new Exception("Id Titulo Duplicado na Planilha Excel : " + tituloRendaFixaCollection[i].IdTitulo);
            }
        }
        #endregion

        using (esTransactionScope scope = new esTransactionScope()) {
            TituloRendaFixaCollection titulo = new TituloRendaFixaCollection();
            // Insere um Titulo Desconsiderando o campo Identity idTitulo
            titulo.InsereTituloRendaFixa(tituloRendaFixaCollection, true);
            tituloRendaFixaSemIdCollection.Save();
            //

            if (tituloRendaFixaSemIdCollection.Count > 0)
            {
                for (int x = 0; tituloRendaFixaSemIdCollection.Count > x; x++)
                {
                    TituloRendaFixa tituloAux = tituloRendaFixaSemIdCollection[x];
                    EmpresaSecuritizadaCollection auxCollection = dicEmpSecSemIdTitulo[x + 1];

                    if (auxCollection.Count > 0)
                    {
                        foreach (EmpresaSecuritizada empresa in auxCollection)
                        {
                            empresa.IdTitulo = tituloAux.IdTitulo.Value;
                        }
                        auxCollection.Save();
                    }
                }
            }
            
            empresaSecColl.Save();
            scope.Complete();
        }
    }

    /// <summary>
    /// Monta a Descrição completa de um TituloRendaFixa
    /// </summary>
    /// <param name="descricao">Descricao Simplificada do Titulo</param>
    /// <param name="dataVencimento"></param>
    /// <param name="percentual"></param>
    /// <param name="taxa"></param>
    /// <param name="descricaoIndice">se Indice for nulo descricao é uma string vazia</param>
    /// <returns>Descricao Completa do Titulo</returns>    
    private string MontaDescricaoCompleta(string descricao, DateTime dataVencimento, decimal? percentual, decimal? taxa, string descricaoIndice) {
        StringBuilder descricaoCompleta = new StringBuilder();
        //
        descricaoCompleta.Append(descricao + " - ");
        descricaoCompleta.Append(" Vcto: ");
        descricaoCompleta.Append(dataVencimento.ToString("dd/MM/yyyy") + " -> ");
        if (percentual.HasValue) {
            descricaoCompleta.Append(percentual.ToString() + "% ");
        }
        descricaoCompleta.Append(descricaoIndice);

        if (taxa.HasValue && taxa != 0.00M) {
            if (!String.IsNullOrEmpty(descricaoIndice)) {
                descricaoCompleta.Append(" + " + taxa + "%");
            }
            else {
                descricaoCompleta.Append(taxa + "%");
            }
        }

        return descricaoCompleta.ToString().Replace("/", "-");
    }

    /// <summary>
    /// Processa a Planilha de Titulo RendaFixa após ter acabado o Upload
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void uplTituloRendaFixa_FileUploadComplete(object sender, FileUploadCompleteEventArgs e) {
        e.CallbackData = "";

        #region Trata Extensão Válida
        if (!ValoresExcelUtil.isExtensaoExcel(e.UploadedFile.FileName.Trim())) {
            e.CallbackData = "Importação Titulo Renda Fixa - Formato de Arquivo Inválido. Formato permitido: .xls ou .xlsx\n\n";
            return;
        }
        #endregion

        // Stream de bytes com o Conteudo do Arquivo Excel
        Stream sr = e.UploadedFile.FileContent;

        #region Processamento Arquivo Excel

        //Reseta vetor de dados
        this.valoresExcelTituloRendaFixa = new List<ValoresExcelTituloRendaFixa>();

        try {
            // Ler Arquivo
            this.LerArquivoTituloRendaFixa(sr);
            // Carrega Arquivo
            this.CarregaTituloRendaFixa();
        }
        catch (Exception e2) {
            e.CallbackData = "Importação Titulo Renda Fixa - " + e2.Message;
            return;
        }

        new HistoricoLog().InsereHistoricoLog(DateTime.Now, DateTime.Now,
            "Importação Planilha Título Renda Fixa",
            HttpContext.Current.User.Identity.Name, UtilitarioWeb.GetIP(Request), "", HistoricoLogOrigem.Outros);

        #endregion
    }
}