﻿using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Web.UI.HtmlControls;
using System.Collections.Generic;
using System.Text;
using Financial.Web.Common;
using DevExpress.Web;
using Financial.Interfaces;
using Financial.Interfaces.Import.Mellon;

public partial class ImportacaoBasePage : BasePage
{    
    /// <summary>
    /// Processa arquivo txt de Posicao SMA Carregando em TabelaCargaPassivo
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void uplXMLClienteMellon_FileUploadComplete(object sender, FileUploadCompleteEventArgs e)
    {
        e.CallbackData = "";

        #region Trata Extensão Válida

        if (!this.isExtensaoXML(e.UploadedFile.FileName.Trim()))
        {
            e.CallbackData = "Arquivo Posição XML Cliente Mellon inválido: Extensão do arquivo deve ser .xml. \n\n";
            return;
        }
        #endregion

        // Stream de bytes com o Conteudo do Arquivo Excel
        StreamReader sr = new StreamReader(e.UploadedFile.FileContent);

        XMLCliente xmlCliente = new XMLCliente();
        try
        {
            xmlCliente.ImportaArquivoXML(sr);
        }
        catch (Exception e1)
        {
            if (e1.Message.StartsWith("Clientes Atualizados:"))
            {
                e.CallbackData = e1.Message;
            }
            else
            {
                string msg = "\tImportação XML Cliente Mellon com problemas \n";
                msg += " - Dados não Importados: " + e1.Message;

                e.CallbackData = msg;
            }
            return;
        }
    }
}