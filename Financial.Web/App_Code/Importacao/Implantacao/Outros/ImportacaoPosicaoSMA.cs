﻿using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Web.UI.HtmlControls;
using System.Collections.Generic;
using System.Text;
using Financial.Web.Common;
using Financial.Interfaces.Import.RendaFixa;
using Financial.InvestidorCotista;
using Financial.Fundo;
using DevExpress.Web;
using System.Globalization;
using Financial.InvestidorCotista.Exceptions;
using Financial.Bolsa;
using Financial.Investidor.Exceptions;
using Financial.Investidor;

public partial class ImportacaoBasePage : BasePage {

    /// <summary>
    /// Processa arquivo txt de Posicao SMA Carregando em TabelaCargaPassivo
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void uplPosicaoSMAFundo_FileUploadComplete(object sender, FileUploadCompleteEventArgs e) {
        e.CallbackData = "";

        #region Trata Extensão Válida

        if (!this.isExtensaoTXT(e.UploadedFile.FileName.Trim())) {
            e.CallbackData = "Arquivo Posição SMA inválido: Extensão do arquivo deve ser .txt. \n\n";
            return;
        }
        #endregion

        // Stream de bytes com o Conteudo do Arquivo Excel
        Stream sr = e.UploadedFile.FileContent;

        TabelaCargaPassivo tabelaCargaPassivo = new TabelaCargaPassivo();
        try {            
            tabelaCargaPassivo.CarregaTabelaCargaPassivoSMA(sr);
        }
        catch (Exception e1) {
            string msg = "\tPosição SMA com problemas \n";
            msg += " - Dados não Importados: " + e1.Message;

            e.CallbackData = msg;
            return;
        }
    }
}