﻿using System;
using System.Collections.Generic;
using System.IO;

using Financial.Integracao.Excel;
using Financial.Integracao.Excel.Util;
using Financial.Web.Common;

using EntitySpaces.Interfaces;

using DevExpress.Web;
using Financial.CRM;
using Financial.InvestidorCotista;
using Financial.RendaFixa;
using Financial.Investidor;
using Financial.Bolsa;
using Financial.BMF;
using Financial.Fundo;
using Financial.Common;
using Financial.InvestidorCotista.Enums;
using Financial.CRM.Enums;
using Financial.Investidor.Enums;
using Financial.Common.Enums;
using Financial.Bolsa.Enums;
using Financial.BMF.Enums;
using Financial.Fundo.Enums;
using Financial.Security;
using System.Web;
using Financial.Web.Util;
using Financial.Security.Enums;
using Financial.Util;

public partial class ImportacaoBasePage : BasePage
{

    /* Estrutura do Excel */
    public List<ValoresExcelCadastroCliente> ValoresExcelCadastroCliente = new List<ValoresExcelCadastroCliente>();

    /// <summary>
    /// 
    /// </summary>
    /// <param name="streamExcel"></param>
    private void LerArquivoCadastroCliente(Stream streamExcel)
    {

        #region Leitura do Arquivo Excel
        // Open Spreadsheet
        Bytescout.Spreadsheet.Spreadsheet document = new Bytescout.Spreadsheet.Spreadsheet();
        document.LoadFromStream(streamExcel);

        Bytescout.Spreadsheet.Worksheet workSheet = document.Workbook.Worksheets[0];
        //
        #region Confere Formato Arquivo
        string[] colunasConferencias = new string[] {"IdCliente","Nome","Apelido", "Tipo","Cpfcnpj",
                                                     "IsentoIR", "IsentoIOF", "StatusAtivo",
                      	"DataImplantacao", "CodigoInterface", "Endereco","Numero","Complemento","Bairro","Cidade","CEP","UF","Pais",
                        "EnderecoCom","NumeroCom","ComplementoCom","BairroCom","CidadeCom","CEPCom","UFCom",
                        "Fone", "Email", "FoneCom", "EmailCom"
                };

        bool formato = ValoresExcelUtil.isExcelFormatoValido(colunasConferencias, workSheet);
        if (!formato)
        {
            string mensagem = "Formato Interno do Arquivo Inválido.\n";
            mensagem += "Linha 1 do Arquivo deve ser: \n";
            for (int i = 0; i < colunasConferencias.Length; i++)
            {
                int coluna = i + 1;
                mensagem += "\tColuna " + coluna + ": " + colunasConferencias[i] + "\n";
            }

            document.Close();
            document.Dispose();

            throw new Exception(mensagem);
        }
        #endregion

        //
        int index = 0;
        // This row,column index should be changed as per your need.
        // i.e. which cell in the excel you are interesting to read.
        //
        /* Formato: 1)IdCliente  - 2)Nome - 3)Apelido - 4)Tipo - 5)Cpfcnpj 
         *          6)IsentoIR   - 7)IsentoIOF  - 8)StatusAtivo
         *          9)DataImplantacao   - 10)CodigoInterface     
         *          11)Endereco  - 12)Numero  - 13) Complemento - 14)Bairro  -15)Cidade - 16)CEP - 17)UF  -18)Pais
         *          19)EnderecoCom - 20)NumeroCom - 21)ComplementoCom  - 22)BairroCom  -23)CidadeCom - 24)CEPCom - 25)UFCom
         *          26)Fone - 27)Email - 28)FoneCom - 29)EmailCom - 30)LocalNegociacao
         */
        int linha = 1;
        int coluna1 = 0, coluna2 = 1, coluna3 = 2,
            coluna4 = 3, coluna5 = 4, coluna6 = 5,
            coluna7 = 6, coluna8 = 7, coluna9 = 8,
            coluna10 = 9, coluna11 = 10, coluna12 = 11,
            coluna13 = 12, coluna14 = 13, coluna15 = 14,
            coluna16 = 15, coluna17 = 16, coluna18 = 17,
            coluna19 = 18, coluna20 = 19, coluna21 = 20,
            coluna22 = 21, coluna23 = 22, coluna24 = 23,
            coluna25 = 24, coluna26 = 25, coluna27 = 26,
            coluna28 = 27, coluna29 = 28, coluna30 = 29;
        //            
        try
        {
            // Enquanto idCliente tiver valor
            while (workSheet.Cell(linha, coluna1).Value != null)
            {
                ValoresExcelCadastroCliente item = new ValoresExcelCadastroCliente();
                
                try {
                    item.IdCliente = workSheet.Cell(linha, coluna1).ValueAsInteger;
                }
                catch (Exception ex) {
                    throw new Exception(ex.Message + " - IdCliente: linha " + (linha + 1));
                }

                if (workSheet.Cell(linha, coluna2).Value == null)
                    throw new Exception("Nome não informado: linha " + (linha + 1));
                try
                {
                    item.Nome = Convert.ToString(workSheet.Cell(linha, coluna2).Value);
                }
                catch (Exception ex) {
                    throw new Exception(ex.Message + " - Nome: linha " + (linha + 1));
                }

                if (workSheet.Cell(linha, coluna3).Value == null)
                    throw new Exception("Apelido não informado: linha " + (linha + 1));
                try
                {
                    item.Apelido = Convert.ToString(workSheet.Cell(linha, coluna3).Value);
                }
                catch (Exception ex) {
                    throw new Exception(ex.Message + " - Apelido: linha " + (linha + 1));
                }

                if (workSheet.Cell(linha, coluna4).Value == null)
                    throw new Exception("TipoPessoa não informado: linha " + (linha + 1));
                try
                {
                    int tipo = workSheet.Cell(linha, coluna4).ValueAsInteger;
                    item.TipoPessoa = (TipoPessoa)tipo;
                }
                catch (Exception ex) {
                    throw new Exception(ex.Message + " - TipoPessoa: linha " + (linha + 1));
                }

                if (workSheet.Cell(linha, coluna5).Value == null)
                    throw new Exception("Cnpj não informado: linha " + (linha + 1));
                try
                {
                    item.Cnpj = Convert.ToString(workSheet.Cell(linha, coluna5).Value);
                }
                catch (Exception ex) {
                    throw new Exception(ex.Message + " - Cnpj: linha " + (linha + 1));
                }

                if (workSheet.Cell(linha, coluna6).Value == null)
                    throw new Exception("IsentoIR não informado: linha " + (linha + 1));
                try
                {
                    item.IsentoIR = Convert.ToString(workSheet.Cell(linha, coluna6).Value);
                }
                catch (Exception ex) {
                    throw new Exception(ex.Message + " - IsentoIR: linha " + (linha + 1));
                }

                if (workSheet.Cell(linha, coluna7).Value == null)
                    throw new Exception("IsentoIOF não informado: linha " + (linha + 1));
                try
                {
                    item.IsentoIOF = Convert.ToString(workSheet.Cell(linha, coluna7).Value);
                }
                catch (Exception ex) {
                    throw new Exception(ex.Message + " - IsentoIOF: linha " + (linha + 1));
                }

                if (workSheet.Cell(linha, coluna8).Value == null)
                    throw new Exception("StatusCliente não informado: linha " + (linha + 1));
                try
                {
                    int status = workSheet.Cell(linha, coluna8).ValueAsInteger;
                    item.StatusCliente = (StatusAtivoCliente)status;
                }
                catch (Exception ex) {
                    throw new Exception(ex.Message + " - StatusCliente: linha " + (linha + 1));
                }

                if (workSheet.Cell(linha, coluna9).Value != null)
                {
                    try {
                        item.DataImplantacao = workSheet.Cell(linha, coluna9).ValueAsDateTime;
                    }
                    catch (Exception ex) {
                        throw new Exception(ex.Message + " - DataImplantacao: linha " + (linha + 1));
                    }
                }
                if (workSheet.Cell(linha, coluna10).Value != null)
                {
                    item.CodigoInterface = Convert.ToString(workSheet.Cell(linha, coluna10).Value);
                }
                //

                if (workSheet.Cell(linha, coluna30).Value == null)
                {
                    item.LocalNegociacao = null;
                }
                else
                {
                    try
                    {
                        item.LocalNegociacao = workSheet.Cell(linha, coluna30).ValueAsString;
                    }
                    catch (Exception ex)
                    {
                        throw new Exception(ex.Message + " - LocalNegociacao: linha " + (linha + 1));
                    }
                }

                ValoresExcelCadastroCliente.Endereco[] endereco = {
                            new ValoresExcelCadastroCliente.Endereco(
                                Convert.ToString(workSheet.Cell(linha, coluna11).Value),  //Endereco
                                Convert.ToString(workSheet.Cell(linha, coluna12).Value),  //Numero
                                Convert.ToString(workSheet.Cell(linha, coluna13).Value), //Complemento 
                                Convert.ToString(workSheet.Cell(linha, coluna14).Value), //Bairro
                                Convert.ToString(workSheet.Cell(linha, coluna15).Value), //Cidade
                                Convert.ToString(workSheet.Cell(linha, coluna16).Value), //CEP
                                Convert.ToString(workSheet.Cell(linha, coluna17).Value), //UF
                                Convert.ToString(workSheet.Cell(linha, coluna18).Value), //Pais
                                Convert.ToString(workSheet.Cell(linha, coluna26).Value), //Fone
                                Convert.ToString(workSheet.Cell(linha, coluna27).Value)  //Email
                                ),
                            new ValoresExcelCadastroCliente.Endereco(
                                Convert.ToString(workSheet.Cell(linha, coluna19).Value), //EnderecoCom
                                Convert.ToString(workSheet.Cell(linha, coluna20).Value), //NumeroCom
                                Convert.ToString(workSheet.Cell(linha, coluna21).Value), //ComplementoCom
                                Convert.ToString(workSheet.Cell(linha, coluna22).Value), //BairroCom
                                Convert.ToString(workSheet.Cell(linha, coluna23).Value), //CidadeCom
                                Convert.ToString(workSheet.Cell(linha, coluna24).Value), //CEPCom
                                Convert.ToString(workSheet.Cell(linha, coluna25).Value), //UFCom
                                Convert.ToString(workSheet.Cell(linha, coluna18).Value), //Pais
                                Convert.ToString(workSheet.Cell(linha, coluna28).Value), //FoneCom
                                Convert.ToString(workSheet.Cell(linha, coluna29).Value)  //EmailCom
                                )
                        };

                item.EnderecoPessoa = endereco[0];
                item.EnderecoComercialPessoa = endereco[1];

                //
                this.ValoresExcelCadastroCliente.Add(item);
                //
                index++;
                linha = 1 + index;
                //                
                Console.WriteLine("{0},{1},{2},{3},{4},{5},{6},{7},{8}",
                    item.IdCliente, item.Nome, item.TipoPessoa, item.Cnpj,
                    item.IsentoIR, item.IsentoIOF, item.StatusCliente,
                    item.EnderecoPessoa, item.EnderecoComercialPessoa);
            }
        }

        catch (Exception ex)
        {
            document.Close();
            document.Dispose();

            throw new Exception(ex.Message);
        }
        #endregion

        document.Close();
        document.Dispose();
    }

    public void CarregaCadastroCliente(int? idTipo)
    {
        PessoaCollection pessoaCollection = new PessoaCollection();
        PessoaEnderecoCollection pessoaEnderecoCollection = new PessoaEnderecoCollection();
        PessoaTelefoneCollection pessoaTelefoneCollection = new PessoaTelefoneCollection();
        PessoaEmailCollection pessoaEmailCollection = new PessoaEmailCollection();
        //
        ClienteCollection clienteCollection = new ClienteCollection();
        ClienteBolsaCollection clienteBolsaCollection = new ClienteBolsaCollection();
        ClienteBMFCollection clienteBMFCollection = new ClienteBMFCollection();
        ClienteRendaFixaCollection clienteRendaFixaCollection = new ClienteRendaFixaCollection();
        ClienteInterfaceCollection clienteInterfaceCollection = new ClienteInterfaceCollection();
        //
        CarteiraCollection carteiraCollection = new CarteiraCollection();
        CotistaCollection cotistaCollection = new CotistaCollection();
        ContaCorrenteCollection contaCorrenteCollection = new ContaCorrenteCollection();
        PermissaoClienteCollection permissaoClienteCollection = new PermissaoClienteCollection();

        //
        for (int i = 0; i < this.ValoresExcelCadastroCliente.Count; i++)
        {
            ValoresExcelCadastroCliente valoresExcel = this.ValoresExcelCadastroCliente[i];
            //                    
            bool pessoaCadastrado = false;
            #region Verifica se já Existe Pessoa e Cliente cadastrado ou se já está na collection (continue no caso de Cliente cadastrado ou se já existir na collection)
            Pessoa pessoaExiste = new Pessoa();
            pessoaExiste = pessoaCollection.FindByPrimaryKey(valoresExcel.IdCliente);

            if (pessoaExiste != null)
            {
                continue;
            }

            Cliente clienteAux = new Cliente();
            List<esQueryItem> campos = new List<esQueryItem>();
            campos.Add(clienteAux.Query.IdCliente);

            if (clienteAux.LoadByPrimaryKey(campos, valoresExcel.IdCliente))
            {
                continue;
            }

            Pessoa pessoaAux = new Pessoa();
            campos = new List<esQueryItem>();
            campos.Add(pessoaAux.Query.IdPessoa);
            campos.Add(pessoaAux.Query.Apelido);
                        
            if (pessoaAux.LoadByPrimaryKey(campos, valoresExcel.IdCliente))
            {
                pessoaCadastrado = true;
            }
            #endregion

            #region Verifica se Campos Obrigatórios foram Preenchidos
            if (String.IsNullOrEmpty(valoresExcel.Nome.Trim()))
            {
                throw new Exception("Nome Cliente não pode ser em Branco");
            }
            if (valoresExcel.TipoPessoa == 0)
            { // Conversão do Enum resultou em 0
                throw new Exception("Tipo Pessoa(Física/Jurídica) deve ser Preenchido");
            }
            if (String.IsNullOrEmpty(valoresExcel.IsentoIR.Trim()))
            {
                throw new Exception("Campo IsentoIR é Obrigatório");
            }
            if (String.IsNullOrEmpty(valoresExcel.IsentoIOF.Trim()))
            {
                throw new Exception("Campo IsentoIOF é Obrigatório");
            }
            if (valoresExcel.StatusCliente == 0)
            { // Conversão do Enum resultou em 0
                throw new Exception("Status Cliente (Ativo/Inativo) deve ser Preenchido");
            }
            #endregion

            string apelidoNovaPessoa = "";
            if (!pessoaCadastrado)
            {
                Pessoa pessoa = pessoaCollection.AddNew();
                #region Preenche campos de Pessoa
                pessoa.IdPessoa = valoresExcel.IdCliente;
                pessoa.Nome = valoresExcel.Nome.Trim();

                if (string.IsNullOrEmpty(valoresExcel.Apelido.Trim()))
                {
                    int maxlength = (int)PessoaMetadata.Meta().Columns.FindByColumnName(PessoaMetadata.ColumnNames.Apelido).CharacterMaxLength;
                    pessoa.Apelido = (pessoa.Nome.Length <= maxlength) ? pessoa.Nome : pessoa.Nome.Substring(0, maxlength - 1);
                }
                else
                {
                    pessoa.Apelido = valoresExcel.Apelido;
                }
                apelidoNovaPessoa = pessoa.Apelido;

                pessoa.Tipo = (byte)valoresExcel.TipoPessoa;
                pessoa.DataCadatro = DateTime.Now;
                pessoa.DataUltimaAlteracao = DateTime.Now;

                if (!string.IsNullOrEmpty(valoresExcel.Cnpj) && valoresExcel.Cnpj != "0")
                {
                    int lengthCnpj = pessoa.Tipo == (byte)TipoPessoa.Fisica ? 11 : 14;

                    string cnpjNumOnly = string.Join(null, System.Text.RegularExpressions.Regex.Split(valoresExcel.Cnpj, "[^\\d]")).PadLeft(lengthCnpj, '0');

                    if (pessoa.Tipo == (byte)TipoPessoa.Fisica)
                    {
                        if (!Utilitario.ValidaCPF(cnpjNumOnly))
                        {
                            throw new Exception("CPF inválido! " + valoresExcel.Cnpj);
                        }
                    }
                    else
                    {
                        if (!Utilitario.ValidaCNPJ(cnpjNumOnly))
                        {
                            throw new Exception("CNPJ inválido! " + valoresExcel.Cnpj);
                        }
                    }

                    pessoa.Cpfcnpj = cnpjNumOnly;
                }
                else
                {
                    pessoa.Cpfcnpj = null;
                }
                pessoa.CodigoInterface = valoresExcel.CodigoInterface;
                #endregion

                #region PessoaEndereco
                if (valoresExcel.EnderecoPessoa != null)
                {
                    PessoaEndereco pessoaEndereco = pessoaEnderecoCollection.AddNew();
                    pessoaEndereco.IdPessoa = valoresExcel.IdCliente;
                    pessoaEndereco.RecebeCorrespondencia = "S";
                    pessoaEndereco.Endereco = valoresExcel.EnderecoPessoa.Rua.Trim();
                    pessoaEndereco.Numero = valoresExcel.EnderecoPessoa.Numero.Trim();
                    pessoaEndereco.Complemento = valoresExcel.EnderecoPessoa.Complemento.Trim();
                    pessoaEndereco.Bairro = valoresExcel.EnderecoPessoa.Bairro.Trim();
                    pessoaEndereco.Cidade = valoresExcel.EnderecoPessoa.Cidade.Trim();
                    pessoaEndereco.Cep = valoresExcel.EnderecoPessoa.Cep.Trim();
                    pessoaEndereco.Uf = valoresExcel.EnderecoPessoa.Uf.Trim().ToUpper();
                    pessoaEndereco.Pais = valoresExcel.EnderecoPessoa.Pais.Trim();
                    if (!String.IsNullOrEmpty(valoresExcel.EnderecoPessoa.Fone))
                    {
                        PessoaTelefone pessoaTelefone = pessoaTelefoneCollection.AddNew();
                        //
                        pessoaTelefone.IdPessoa = pessoa.IdPessoa.Value;
                        pessoaTelefone.Ddi = "";
                        pessoaTelefone.Ddd = "";
                        pessoaTelefone.Numero = valoresExcel.EnderecoPessoa.Fone;
                        pessoaTelefone.TipoTelefone = (byte)TipoTelefonePessoa.Residencial;
                    }

                    if (!String.IsNullOrEmpty(valoresExcel.EnderecoPessoa.Email))
                    {
                        PessoaEmail pessoaEmail = pessoaEmailCollection.AddNew();
                        //
                        pessoaEmail.IdPessoa = pessoa.IdPessoa.Value;
                        pessoaEmail.Email = valoresExcel.EnderecoPessoa.Email;
                    }
                }

                if (valoresExcel.EnderecoComercialPessoa != null)
                {
                    PessoaEndereco pessoaEnderecoComercial = pessoaEnderecoCollection.AddNew();
                    pessoaEnderecoComercial.IdPessoa = valoresExcel.IdCliente;
                    pessoaEnderecoComercial.RecebeCorrespondencia = "S";
                    pessoaEnderecoComercial.Endereco = valoresExcel.EnderecoComercialPessoa.Rua.Trim();
                    pessoaEnderecoComercial.Numero = valoresExcel.EnderecoComercialPessoa.Numero.Trim();
                    pessoaEnderecoComercial.Complemento = valoresExcel.EnderecoComercialPessoa.Complemento.Trim();
                    pessoaEnderecoComercial.Bairro = valoresExcel.EnderecoComercialPessoa.Bairro.Trim();
                    pessoaEnderecoComercial.Cidade = valoresExcel.EnderecoComercialPessoa.Cidade.Trim();
                    pessoaEnderecoComercial.Cep = valoresExcel.EnderecoComercialPessoa.Cep.Trim();
                    pessoaEnderecoComercial.Uf = valoresExcel.EnderecoComercialPessoa.Uf.Trim().ToUpper();
                    pessoaEnderecoComercial.Pais = valoresExcel.EnderecoComercialPessoa.Pais.Trim();

                    if (!String.IsNullOrEmpty(valoresExcel.EnderecoComercialPessoa.Fone))
                    {
                        PessoaTelefone pessoaTelefone = pessoaTelefoneCollection.AddNew();
                        //
                        pessoaTelefone.IdPessoa = pessoa.IdPessoa.Value;
                        pessoaTelefone.Ddi = "";
                        pessoaTelefone.Ddd = "";
                        pessoaTelefone.Numero = valoresExcel.EnderecoComercialPessoa.Fone;
                        pessoaTelefone.TipoTelefone = (byte)TipoTelefonePessoa.Comercial;
                    }

                    if (!String.IsNullOrEmpty(valoresExcel.EnderecoComercialPessoa.Email))
                    {
                        PessoaEmail pessoaEmail = pessoaEmailCollection.AddNew();
                        //
                        pessoaEmail.IdPessoa = pessoa.IdPessoa.Value;
                        pessoaEmail.Email = valoresExcel.EnderecoComercialPessoa.Email;
                    }

                    //
                    // Marcar RecebeCorrespondencia do endereço Normal se existir para N
                    if (valoresExcel.EnderecoPessoa.HasValue())
                    {
                        pessoaEnderecoCollection[pessoaEnderecoCollection.Count - 1].RecebeCorrespondencia = "N";
                    }
                }
                #endregion
            }

            Cliente cliente = clienteCollection.AddNew();
            #region Preenche dados de Cliente
            cliente.IdCliente = valoresExcel.IdCliente;
            cliente.Nome = valoresExcel.Nome;
            cliente.Apelido = valoresExcel.Apelido;
            cliente.IdPessoa = valoresExcel.IdCliente;
            cliente.IdLocalNegociacao = (int)Financial.Common.Enums.LocalNegociacaoFixo.Brasil;
            if (!string.IsNullOrEmpty(valoresExcel.LocalNegociacao))
            {
                LocalNegociacao localNegociacao = new LocalNegociacao();
                localNegociacao.Query.Where(localNegociacao.Query.Codigo.ToUpper().Equal(valoresExcel.LocalNegociacao.ToUpper()));

                if (localNegociacao.Query.Load())
                    cliente.IdLocalNegociacao = localNegociacao.IdLocalNegociacao.Value;
                else
                    throw new Exception("Local de Negociação não cadastrado: " + valoresExcel.LocalNegociacao);
            }

            cliente.ApuraGanhoRV = "N";
            cliente.CalculaContabil = "N";
            cliente.CalculaGerencial = "N";
            cliente.CalculaRealTime = "N";
            if (valoresExcel.DataImplantacao.HasValue)
            {
                cliente.DataImplantacao = valoresExcel.DataImplantacao.Value;
            }
            else
            {
                cliente.DataImplantacao = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day);
            }

            cliente.DataDia = cliente.DataImplantacao.Value;
            cliente.DataInicio = cliente.DataImplantacao.Value;

            GrupoProcessamentoCollection grupoProcessamentoCollection = new GrupoProcessamentoCollection();
            grupoProcessamentoCollection.Query.Where(grupoProcessamentoCollection.Query.Descricao.Like("%Carteira%"));
            grupoProcessamentoCollection.Query.Load();

            if (grupoProcessamentoCollection.Count == 0)
            {
                grupoProcessamentoCollection.QueryReset();
                grupoProcessamentoCollection.LoadAll();
            }

            cliente.IdGrupoProcessamento = grupoProcessamentoCollection[0].IdGrupoProcessamento.Value;
            cliente.IdMoeda = (byte)ListaMoedaFixo.Real;

            if (idTipo.HasValue)
            {
                cliente.IdTipo = idTipo;
            }
            else
            {
                if ((byte)valoresExcel.TipoPessoa == (byte)TipoPessoa.Fisica)
                {
                    cliente.IdTipo = (byte)TipoClienteFixo.ClientePessoaFisica;
                }
                else
                {
                    cliente.IdTipo = (byte)TipoClienteFixo.ClientePessoaJuridica;
                }
            }

            cliente.IsentoIOF = valoresExcel.IsentoIR.ToUpper();
            cliente.IsentoIR = valoresExcel.IsentoIOF.ToUpper();

            cliente.IsProcessando = "N";
            cliente.Status = (byte)StatusCliente.Aberto;
            cliente.StatusAtivo = (byte)valoresExcel.StatusCliente;
            cliente.StatusRealTime = (byte)StatusRealTimeCliente.NaoExecutar;
            cliente.TipoControle = (byte)TipoControleCliente.Completo;
            cliente.ZeraCaixa = "S";
            cliente.DescontaTributoPL = (byte)DescontoPLCliente.BrutoSemImpacto;
            cliente.GrossUP = (byte)GrossupCliente.NaoFaz;
            #endregion

            #region Cadastra ClienteBolsa
            bool bolsaPreenchido = valoresExcel.Bolsa != null;

            ClienteBolsa clienteBolsa = clienteBolsaCollection.AddNew();
            clienteBolsa.CodigoSinacor = cliente.IdCliente.Value.ToString();
            clienteBolsa.IdCliente = cliente.IdCliente.Value;
            clienteBolsa.IsentoIR = bolsaPreenchido? valoresExcel.Bolsa.IsentoIR: valoresExcel.IsentoIR.ToUpper(); //Se não tem isenção específica de bolsa, acata a isenção geral do cliente
            clienteBolsa.TipoCotacao = bolsaPreenchido? valoresExcel.Bolsa.TipoCotacao.Value: (byte)TipoCotacaoBolsa.Fechamento;
            clienteBolsa.IdAssessor = bolsaPreenchido? valoresExcel.Bolsa.IdAssessor: null;
            clienteBolsa.TipoCusto = 1;
            #endregion

            #region Cadastra ClienteBMF
            bool bmfPreenchido = valoresExcel.BMF != null;

            ClienteBMF clienteBMF = clienteBMFCollection.AddNew();
            clienteBMF.CodigoSinacor = cliente.IdCliente.Value.ToString();
            clienteBMF.IdCliente = cliente.IdCliente.Value;
            clienteBMF.InvestidorInstitucional = "N";
            clienteBMF.TipoCotacao = bmfPreenchido? valoresExcel.BMF.TipoCotacao.Value: (byte)TipoCotacaoBMF.Fechamento;
            clienteBMF.IdAssessor = bmfPreenchido ? valoresExcel.BMF.IdAssessor : null;
            clienteBMF.Socio = "N";
            clienteBMF.TipoPlataforma = (byte)TipoPlataformaOperacional.Normal;
            #endregion

            #region Cadastra ClienteRendaFixa
            bool rendaFixaPreenchido = valoresExcel.RendaFixa != null;

            ClienteRendaFixa clienteRendaFixa = clienteRendaFixaCollection.AddNew();
            clienteRendaFixa.IdCliente = cliente.IdCliente.Value;
            clienteRendaFixa.IsentoIR = rendaFixaPreenchido ? valoresExcel.RendaFixa.IsentoIR : valoresExcel.IsentoIR.ToUpper(); //Se não tem isenção específica de RF, acata a isenção geral do cliente
            clienteRendaFixa.IsentoIOF = rendaFixaPreenchido ? valoresExcel.RendaFixa.IsentoIOF : valoresExcel.IsentoIOF.ToUpper(); //Se não tem isenção específica de RF, acata a isenção geral do cliente
            clienteRendaFixa.IdAssessor = rendaFixaPreenchido ? valoresExcel.RendaFixa.IdAssessor : null;
            clienteRendaFixa.CodigoInterface = cliente.IdCliente.Value.ToString();
            clienteRendaFixa.UsaCustoMedio = "N";
            #endregion

            #region tabPage - ClienteInterface
            ClienteInterface clienteInterface = clienteInterfaceCollection.AddNew();
            clienteInterface.IdCliente = valoresExcel.IdCliente;
            clienteInterface.CodigoYMF = valoresExcel.CodigoInterface;
            #endregion

            #region Salva Contacorrente default associada ao cliente
            ContaCorrente contaCorrente = contaCorrenteCollection.AddNew();
            contaCorrente.IdPessoa = valoresExcel.IdCliente;
            contaCorrente.Numero = valoresExcel.IdCliente.ToString();

            TipoConta tipoConta = new TipoConta();
            tipoConta.Query.Select(tipoConta.Query.IdTipoConta.Min());
            tipoConta.Query.Load();
            if (!tipoConta.IdTipoConta.HasValue)
            {
                tipoConta = new TipoConta();
                tipoConta.Descricao = "Conta Depósito";
                tipoConta.Save();
            }
            contaCorrente.IdTipoConta = tipoConta.IdTipoConta.Value;
                        
            contaCorrente.IdMoeda = (int)ListaMoedaFixo.Real;
            contaCorrente.ContaDefault = "S";
            #endregion

            #region Cadastra Carteira
            Carteira carteira = carteiraCollection.AddNew();

            carteira.IdCarteira = valoresExcel.IdCliente;
            carteira.Nome = valoresExcel.Nome;
            carteira.Apelido = pessoaCadastrado ? pessoaAux.Apelido : apelidoNovaPessoa;
            carteira.CalculaEnquadra = "N";
            carteira.CalculaIOF = "N";
            carteira.CasasDecimaisCota = 8;
            carteira.CasasDecimaisQuantidade = 8;
            carteira.CobraTaxaFiscalizacaoCVM = "N";
            carteira.ContagemDiasConversaoResgate = (byte)ContagemDiasLiquidacaoResgate.DiasUteis;
            carteira.CotaInicial = 1;
            carteira.TipoAniversario = 0;
            carteira.DataInicioCota = cliente.DataDia.Value;
            carteira.DiasCotizacaoAplicacao = 0;
            carteira.DiasCotizacaoResgate = 0;
            carteira.DiasLiquidacaoAplicacao = 0;
            carteira.DiasLiquidacaoResgate = 0;
            carteira.DistribuicaoDividendo = (byte)DistribuicaoDividendosCarteira.NaoDistribui;

            AgenteMercadoCollection agenteMercadoCollection = new AgenteMercadoCollection();
            agenteMercadoCollection.Query.Where(agenteMercadoCollection.Query.Nome.Like("%Padrão%"),
                                                agenteMercadoCollection.Query.FuncaoAdministrador.Equal("S"),
                                                agenteMercadoCollection.Query.FuncaoGestor.Equal("S"));
            agenteMercadoCollection.Query.Load();

            if (agenteMercadoCollection.Count == 0)
            {
                agenteMercadoCollection.QueryReset();
                agenteMercadoCollection.Query.Where(agenteMercadoCollection.Query.FuncaoAdministrador.Equal("S"),
                                                    agenteMercadoCollection.Query.FuncaoGestor.Equal("S"));
                agenteMercadoCollection.Query.Load();
            }

            carteira.IdAgenteAdministrador = agenteMercadoCollection[0].IdAgente.Value;
            carteira.IdAgenteGestor = agenteMercadoCollection[0].IdAgente.Value;

            CategoriaFundoCollection categoriaFundoCollection = new CategoriaFundoCollection();
            categoriaFundoCollection.Query.Where(categoriaFundoCollection.Query.Descricao.Like("%Carteira Administrada%"));
            categoriaFundoCollection.Query.Load();

            if (categoriaFundoCollection.Count == 0)
            {
                categoriaFundoCollection.QueryReset();
                categoriaFundoCollection.LoadAll();
            }

            carteira.IdCategoria = categoriaFundoCollection[0].IdCategoria.Value;

            SubCategoriaFundoCollection subCategoriaFundoCollection = new SubCategoriaFundoCollection();
            subCategoriaFundoCollection.Query.Where(subCategoriaFundoCollection.Query.Descricao.Like("%Carteira Administrada%"));
            subCategoriaFundoCollection.Query.Load();

            if (subCategoriaFundoCollection.Count == 0)
            {
                subCategoriaFundoCollection.QueryReset();
                subCategoriaFundoCollection.LoadAll();
            }
            carteira.IdSubCategoria = subCategoriaFundoCollection[0].IdSubCategoria.Value;

            IndiceCollection indiceCollection = new IndiceCollection();
            indiceCollection.Query.Where(indiceCollection.Query.Descricao.Like("%CDI%"));
            indiceCollection.Query.Load();

            if (indiceCollection.Count == 0)
            {
                indiceCollection.QueryReset();
                indiceCollection.LoadAll();
            }
            carteira.IdIndiceBenchmark = indiceCollection[0].IdIndice.Value;
            carteira.ProjecaoIRResgate = (byte)Financial.Tributo.Custom.CalculoTributo.ProjecaoIRCotista.DiaLiquidacaoResgate;
            carteira.StatusAtivo = (byte)valoresExcel.StatusCliente;
            carteira.TipoCarteira = (byte)TipoCarteiraFundo.RendaVariavel;
            carteira.TipoCota = (byte)TipoCotaFundo.Fechamento;
            carteira.TipoCusto = (byte)TipoCustoFundo.Aplicacao;
            carteira.TipoRentabilidade = (byte)TipoRentabilidadeFundo.FinalFinal;
            carteira.TipoTributacao = (byte)TipoTributacaoFundo.Acoes;
            carteira.TipoVisaoFundo = (byte)Financial.Captacao.Enums.TipoVisaoFundoRebate.NaoTrata;
            carteira.TruncaCota = "N";
            carteira.TruncaQuantidade = "N";
            carteira.TruncaFinanceiro = "N";
            carteira.ContagemPrazoIOF = (byte)ContagemDiasPrazoIOF.Direta;
            carteira.PrioridadeOperacao = (byte)PrioridadeOperacaoFundo.ResgateAntes;
            carteira.CompensacaoPrejuizo = (byte)TipoCompensacaoPrejuizo.Padrao;
            carteira.TipoCalculoRetorno = (byte)CalculoRetornoCarteira.Cota;
            #endregion

            bool permissaoInternoAuto = ParametrosConfiguracaoSistema.Outras.PermissaoInternoAuto;

            #region Se tem permisão automática para usuários internos, cria permissão para todos os usuários (internos) do sistema
            if (permissaoInternoAuto)
            {
                GrupoUsuarioQuery grupoUsuarioQuery = new GrupoUsuarioQuery("G");
                UsuarioQuery usuarioQuery = new UsuarioQuery("U");

                usuarioQuery.InnerJoin(grupoUsuarioQuery).On(grupoUsuarioQuery.IdGrupo == usuarioQuery.IdGrupo);
                usuarioQuery.Where(grupoUsuarioQuery.TipoPerfil.In((byte)TipoPerfilGrupo.Administrador, (byte)TipoPerfilGrupo.BackOffice));

                UsuarioCollection usuarioCollection = new UsuarioCollection();
                usuarioCollection.Load(usuarioQuery);

                foreach (Usuario usuarioPermissao in usuarioCollection)
                {
                    int idUsuario = usuarioPermissao.IdUsuario.Value;

                    PermissaoCliente permissaoCliente = new PermissaoCliente();
                    permissaoCliente.IdCliente = cliente.IdCliente;
                    permissaoCliente.IdUsuario = idUsuario;
                    permissaoClienteCollection.AttachEntity(permissaoCliente);
                }
            }
            #endregion
        }

        using (esTransactionScope scope = new esTransactionScope())
        {
            // Salva Posicões presentes no Excel
            pessoaCollection.Save();
            pessoaEnderecoCollection.Save();
            //
            pessoaTelefoneCollection.Save();
            pessoaEmailCollection.Save();
            //
            clienteCollection.Save();
            clienteBolsaCollection.Save();
            clienteBMFCollection.Save();
            clienteRendaFixaCollection.Save();
            clienteInterfaceCollection.Save();
            carteiraCollection.Save();
            contaCorrenteCollection.Save();
            permissaoClienteCollection.Save();
            //
            scope.Complete();
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name=""></param>
    public void CarregaCadastroCliente()
    {
        this.CarregaCadastroCliente(null);
    }

    /// <summary>
    /// Processa a Planilha de Cadastro Cliente após ter acabado o Upload
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void uplCadastroCliente_FileUploadComplete(object sender, FileUploadCompleteEventArgs e)
    {
        e.CallbackData = "";

        #region Trata Extensão Válida
        if (!ValoresExcelUtil.isExtensaoExcel(e.UploadedFile.FileName.Trim()))
        {
            e.CallbackData = "Importação Cadastro Cliente - Formato de Arquivo Inválido. Formato permitido: .xls ou .xlsx\n\n";
            return;
        }
        #endregion

        // Stream de bytes com o Conteudo do Arquivo Excel
        Stream sr = e.UploadedFile.FileContent;

        #region Processamento Arquivo Excel

        try
        {
            // Ler Arquivo
            this.LerArquivoCadastroCliente(sr);
            // Carrega Arquivo
            this.CarregaCadastroCliente();
        }
        catch (Exception e2)
        {
            e.CallbackData = "Importação Cadastro Cliente - " + e2.Message;
            return;
        }

        new HistoricoLog().InsereHistoricoLog(DateTime.Now, DateTime.Now,
            "Importação Planilha Cadastro Cliente",
            HttpContext.Current.User.Identity.Name, UtilitarioWeb.GetIP(Request), "", HistoricoLogOrigem.Outros);

        #endregion
    }
}
