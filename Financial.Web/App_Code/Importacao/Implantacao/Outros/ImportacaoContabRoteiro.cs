﻿using System;
using System.Collections.Generic;
using System.IO;

using Financial.Integracao.Excel;
using Financial.Integracao.Excel.Util;
using Financial.Web.Common;

using EntitySpaces.Interfaces;

using DevExpress.Web;
using Financial.CRM;
using Financial.InvestidorCotista;
using Financial.RendaFixa;
using Financial.Investidor;
using Financial.Bolsa;
using Financial.BMF;
using Financial.Fundo;
using Financial.Common;
using Financial.InvestidorCotista.Enums;
using Financial.CRM.Enums;
using Financial.Investidor.Enums;
using Financial.Common.Enums;
using Financial.Bolsa.Enums;
using Financial.BMF.Enums;
using Financial.Fundo.Enums;
using Financial.Security;
using System.Web;
using Financial.Web.Util;
using Financial.Security.Enums;
using Financial.Util;
using Financial.Contabil;
using Financial.Contabil.Enums;
using EntitySpaces.Core;
using System.Text;


public partial class ImportacaoBasePage : BasePage
{

    /* Estrutura do Excel */
    private List<ValoresExcelContabRoteiro> valoresExcelContabRoteiro = new List<ValoresExcelContabRoteiro>();
    private ContabRoteiroCollection contabRoteiroCollection = new ContabRoteiroCollection();    

    /// <summary>
    /// 
    /// </summary>
    /// <param name="streamExcel"></param>
    private void LerArquivoContabRoteiro(Stream streamExcel)
    {
        #region Leitura do Arquivo Excel
        // Open Spreadsheet
        Bytescout.Spreadsheet.Spreadsheet document = new Bytescout.Spreadsheet.Spreadsheet();
        document.LoadFromStream(streamExcel);

        Bytescout.Spreadsheet.Worksheet workSheet = document.Workbook.Worksheets[0];
        //
        #region Confere Formato Arquivo
        string[] colunasConferencias = new string[] { "ContaDebito", "ContaCredito", "Origem", "Descricao", "IdPlano", "Identificador" };

        bool formato = ValoresExcelUtil.isExcelFormatoValido(colunasConferencias, workSheet);
        if (!formato)
        {
            string mensagem = "Formato Interno do Arquivo Inválido.\n";
            mensagem += "Linha 1 do Arquivo deve ser: \n";
            for (int i = 0; i < colunasConferencias.Length; i++)
            {
                int coluna = i + 1;
                mensagem += "\tColuna " + coluna + ": " + colunasConferencias[i] + "\n";
            }

            document.Close();
            document.Dispose();

            throw new Exception(mensagem);
        }
        #endregion

        //
        int index = 0;
        // This row,column index should be changed as per your need.
        // i.e. which cell in the excel you are interesting to read.
        //
        /* Formato: 1)ContaDebito  - 2)ContaCredito - 3)Origem - 4)Descricao - 5)IdPlano - 6)Identificador
         */
        int linha = 1;
        int coluna1 = 0, coluna2 = 1, coluna3 = 2, coluna4 = 3, coluna5 = 4, coluna6 = 5;
        //            
        try
        {
            // Enquanto idCliente tiver valor
            while (workSheet.Cell(linha, coluna1).Value != null)
            {
                ValoresExcelContabRoteiro item = new ValoresExcelContabRoteiro();

                if (workSheet.Cell(linha, coluna1).Value == null)
                    throw new Exception("ContaDebito não informada: linha " + (linha + 1));
                try
                {
                    item.ContaDebito = Convert.ToString(workSheet.Cell(linha, coluna1).Value);
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message + " - ContaDebito: linha " + (linha + 1));
                }
                
                if (workSheet.Cell(linha, coluna2).Value == null)
                    throw new Exception("ContaCredito não informada: linha " + (linha + 1));
                try
                {
                    item.ContaCredito = Convert.ToString(workSheet.Cell(linha, coluna2).Value);
                }
                catch (Exception ex) {
                    throw new Exception(ex.Message + " - ContaCredito: linha " + (linha + 1));
                }

                if (workSheet.Cell(linha, coluna3).Value == null)
                    throw new Exception("Origem não informada: linha " + (linha + 1));
                try
                {
                    item.Origem = workSheet.Cell(linha, coluna3).ValueAsInteger;
                }
                catch (Exception ex) {
                    throw new Exception(ex.Message + " - Origem: linha " + (linha + 1));
                }

                if (workSheet.Cell(linha, coluna4).Value == null)
                    throw new Exception("Descricao não informada: linha " + (linha + 1));
                try
                {
                    item.Descricao = Convert.ToString(workSheet.Cell(linha, coluna4).Value);
                }
                catch (Exception ex) {
                    throw new Exception(ex.Message + " - Descricao: linha " + (linha + 1));
                }

                if (workSheet.Cell(linha, coluna5).Value == null)
                    throw new Exception("IdPlano não informado: linha " + (linha + 1));
                try
                {
                    item.IdPlano = workSheet.Cell(linha, coluna5).ValueAsInteger;
                }
                catch (Exception ex) {
                    throw new Exception(ex.Message + " - IdPlano: linha " + (linha + 1));
                }

                //
                this.valoresExcelContabRoteiro.Add(item);
                //
                index++;
                linha = 1 + index;
                //                
            }
        }

        catch (Exception ex)
        {
            document.Close();
            document.Dispose();

            throw new Exception(ex.Message);
        }
        #endregion

        document.Close();
        document.Dispose();
    }

    private void InsertRegistro(ValoresExcelContabRoteiro valoresExcel)
    {
        int idPlano = valoresExcel.IdPlano;
        string contaDebito = valoresExcel.ContaDebito.Trim();
        string contaCredito = valoresExcel.ContaCredito.Trim();
        int origem = valoresExcel.Origem;
        
        ContabConta contabContaDebito = new ContabConta();
        contabContaDebito.Query.Where(contabContaDebito.Query.Codigo.Equal(contaDebito),
                                      contabContaDebito.Query.IdPlano.Equal(idPlano));
        contabContaDebito.Query.es.Top = 1;
        contabContaDebito.Query.Load();

        if (!contabContaDebito.es.HasData)
        {
            throw new Exception("Não existe conta definida para o código " + contaDebito + " associada ao Plano Contábil " + idPlano.ToString() + ". Importação cancelada.");
        }

        ContabConta contabContaCredito = new ContabConta();
        contabContaCredito.Query.Where(contabContaCredito.Query.Codigo.Equal(contaCredito),
                                       contabContaCredito.Query.IdPlano.Equal(idPlano));
        contabContaCredito.Query.es.Top = 1;
        contabContaCredito.Query.Load();

        if (!contabContaCredito.es.HasData)
        {
            throw new Exception("Não existe conta definida para o código " + contaCredito + " associada ao Plano Contábil " + idPlano.ToString() + ". Importação cancelada.");
        }

        ContabRoteiroCollection contabRoteiroCollection = new ContabRoteiroCollection();
        contabRoteiroCollection.Query.Where(contabRoteiroCollection.Query.IdPlano.Equal(idPlano),
                                            contabRoteiroCollection.Query.IdContaDebito.Equal(contabContaDebito.IdConta.Value),
                                            contabRoteiroCollection.Query.IdContaCredito.Equal(contabContaCredito.IdConta.Value),
                                            contabRoteiroCollection.Query.Origem.Equal(origem));
        contabRoteiroCollection.Query.Load();

        if (contabRoteiroCollection.Count > 0)
        {
            string mensagem = "Já existem contas associadas no Roteiro ao Plano Contábil: " + idPlano + "." +
                                "- Conta Débito " + contabContaDebito.Codigo + ", Conta Crédito " + contabContaCredito.Codigo + ", Origem " + origem;

            mensagem = mensagem + " Importação cancelada.";
            throw new Exception(mensagem);
        }

        ContabRoteiro contabRoteiro = new ContabRoteiro();
        contabRoteiro.ContaDebito = contabContaDebito.Codigo;
        contabRoteiro.ContaDebitoReduzida = contabContaDebito.CodigoReduzida;
        contabRoteiro.ContaCredito = contabContaCredito.Codigo;
        contabRoteiro.ContaCreditoReduzida = contabContaCredito.CodigoReduzida;        
        contabRoteiro.Descricao = valoresExcel.Descricao;        
        contabRoteiro.IdContaDebito = contabContaDebito.IdConta.Value;
        contabRoteiro.IdContaCredito = contabContaCredito.IdConta.Value;        
        contabRoteiro.Identificador = valoresExcel.Identificador;
        contabRoteiro.IdPlano = idPlano;
        contabRoteiro.Origem = origem;

        contabRoteiro.Save();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name=""></param>
    private void CarregaContabRoteiro()
    {
        ContabRoteiroCollection contabRoteiroCollection = new ContabRoteiroCollection();

        using (esTransactionScope scope = new esTransactionScope())
        {
            for (int i = 0; i < this.valoresExcelContabRoteiro.Count; i++)
            {
                ValoresExcelContabRoteiro valoresExcel = this.valoresExcelContabRoteiro[i];
                this.InsertRegistro(valoresExcel);
            }
            
            scope.Complete();
        }
        
    }

    /// <summary>
    /// Processa a Planilha de Cadastro Cliente após ter acabado o Upload
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void uplContabRoteiro_FileUploadComplete(object sender, FileUploadCompleteEventArgs e)
    {
        e.CallbackData = "";

        #region Trata Extensão Válida
        if (!ValoresExcelUtil.isExtensaoExcel(e.UploadedFile.FileName.ToLower().Trim()))
        {
            e.CallbackData = "Importação Roteiro Contábil - Formato de Arquivo Inválido. Formato permitido: .xls ou .xlsx\n\n";
            return;
        }
        #endregion

        // Stream de bytes com o Conteudo do Arquivo Excel
        Stream sr = e.UploadedFile.FileContent;

        #region Processamento Arquivo Excel

        try
        {
            // Ler Arquivo
            this.LerArquivoContabRoteiro(sr);
            // Carrega Arquivo
            this.CarregaContabRoteiro();
        }
        catch (Exception e2)
        {
            e.CallbackData = "Importação Roteiro Contábil - " + e2.Message;
            return;
        }

        new HistoricoLog().InsereHistoricoLog(DateTime.Now, DateTime.Now,
            "Importação Planilha Roteiro Contábil",
            HttpContext.Current.User.Identity.Name, UtilitarioWeb.GetIP(Request), "", HistoricoLogOrigem.Outros);

        #endregion
    }


}
