﻿using System;
using System.Collections.Generic;
using System.IO;

using Financial.Integracao.Excel;
using Financial.Integracao.Excel.Util;
using Financial.Web.Common;

using EntitySpaces.Interfaces;

using DevExpress.Web;
using Financial.CRM;
using Financial.InvestidorCotista;
using Financial.RendaFixa;
using Financial.Investidor;
using Financial.Bolsa;
using Financial.BMF;
using Financial.Fundo;
using Financial.Common;
using Financial.InvestidorCotista.Enums;
using Financial.CRM.Enums;
using Financial.Investidor.Enums;
using Financial.Common.Enums;
using Financial.Bolsa.Enums;
using Financial.BMF.Enums;
using Financial.Fundo.Enums;
using Financial.Security;
using System.Web;
using Financial.Web.Util;
using Financial.Security.Enums;
using Financial.Util;

public partial class ImportacaoBasePage : BasePage
{

    /* Estrutura do Excel */
    public List<ValoresExcelCadastroPessoa> valoresExcelCadastroPessoa = new List<ValoresExcelCadastroPessoa>();

    /// <summary>
    /// 
    /// </summary>
    /// <param name="streamExcel"></param>
    private void LerArquivoCadastroPessoa(Stream streamExcel)
    {

        #region Leitura do Arquivo Excel
        // Open Spreadsheet
        Bytescout.Spreadsheet.Spreadsheet document = new Bytescout.Spreadsheet.Spreadsheet();
        document.LoadFromStream(streamExcel);

        Bytescout.Spreadsheet.Worksheet workSheet = document.Workbook.Worksheets[0];
        //
        #region Confere Formato Arquivo
        string[] colunasConferencias = new string[] {"IdPessoa","Nome","Apelido", "Tipo","Documento", "CodigoInterface", 
                                                     "Endereco","Numero","Complemento","Bairro","Cidade",
                                                     "CEP","UF","Pais","Fone", "Email",
                                                     "EnderecoCom","NumeroCom","ComplementoCom","BairroCom","CidadeCom",
                                                     "CEPCom","UFCom","PaisCom","FoneCom", "EmailCom", 
                };

        bool formato = ValoresExcelUtil.isExcelFormatoValido(colunasConferencias, workSheet);
        if (!formato)
        {
            string mensagem = "Formato Interno do Arquivo Inválido.\n";
            mensagem += "Linha 1 do Arquivo deve ser: \n";
            for (int i = 0; i < colunasConferencias.Length; i++)
            {
                int coluna = i + 1;
                mensagem += "\tColuna " + coluna + ": " + colunasConferencias[i] + "\n";
            }

            document.Close();
            document.Dispose();

            throw new Exception(mensagem);
        }
        #endregion

        //
        int index = 0;
        // This row,column index should be changed as per your need.
        // i.e. which cell in the excel you are interesting to read.
        //
        /* Formato: 1)IdPessoa  - 2)Nome        - 3)Apelido         - 4)Tipo        - 5)Documento   - 6)CodigoInterface
         *          7)Endereco  - 8)Numero      - 9) Complemento    - 10)Bairro      - 11)Cidade   
         *          12)CEP      - 13)UF         - 14)Pais           - 15)Fone       - 16)Email
         *          17)EnderecoCom      - 18)NumeroCom      - 19)ComplementoCom         - 20)BairroCom 
         *          21)CidadeCom        - 22)CEPCom         - 23)UFCom                  - 24)PaisCom                 
         *          25)FoneCom          - 26)EmailCom       
         */
        int linha = 1;
        int coluna1 = 0, coluna2 = 1, coluna3 = 2, coluna4 = 3,
            coluna5 = 4, coluna6 = 5, coluna7 = 6, coluna8 = 7,
            coluna9 = 8, coluna10 = 9, coluna11 = 10, coluna12 = 11,
            coluna13 = 12, coluna14 = 13, coluna15 = 14, coluna16 = 15,
            coluna17 = 16, coluna18 = 17, coluna19 = 18, coluna20 = 19,
            coluna21 = 20, coluna22 = 21, coluna23 = 22, coluna24 = 23,
            coluna25 = 24, coluna26 = 25;							
        //            
        try
        {
            // Enquanto IdPessoa tiver valor
            while (workSheet.Cell(linha, coluna1).Value != null)
            {
                ValoresExcelCadastroPessoa item = new ValoresExcelCadastroPessoa();

                #region IdPessoa
                try {
                    item.IdPessoa = workSheet.Cell(linha, coluna1).ValueAsInteger;
                }
                catch (Exception ex) {
                    throw new Exception(ex.Message + " - IdPessoa: linha " + (linha + 1));
                }
                #endregion

                #region Nome
                if (workSheet.Cell(linha, coluna2).Value == null)
                    throw new Exception("Nome não informado: linha " + (linha + 1));
                try
                {
                    item.Nome = Convert.ToString(workSheet.Cell(linha, coluna2).Value);
                }
                catch (Exception ex) {
                    throw new Exception(ex.Message + " - Nome: linha " + (linha + 1));
                }
                #endregion

                #region Apelido
                if (workSheet.Cell(linha, coluna3).Value == null)
                    throw new Exception("Apelido não informado: linha " + (linha + 1));
                try
                {
                    item.Apelido = Convert.ToString(workSheet.Cell(linha, coluna3).Value);
                }
                catch (Exception ex) {
                    throw new Exception(ex.Message + " - Apelido: linha " + (linha + 1));
                }
                #endregion

                #region TipoPessoa
                if (workSheet.Cell(linha, coluna4).Value == null)
                    throw new Exception("TipoPessoa não informado: linha " + (linha + 1));
                try
                {
                    int tipo = workSheet.Cell(linha, coluna4).ValueAsInteger;
                    item.TipoPessoa = (TipoPessoa)tipo;
                }
                catch (Exception ex) {
                    throw new Exception(ex.Message + " - TipoPessoa: linha " + (linha + 1));
                }
                #endregion

                #region Documento
                if (workSheet.Cell(linha, coluna5).Value != null)
                {
                    item.Documento = Convert.ToString(workSheet.Cell(linha, coluna5).Value);
                }
                #endregion 

                #region CodigoInterface
                if (workSheet.Cell(linha, coluna26).Value != null)
                {
                    item.CodigoInterface = Convert.ToString(workSheet.Cell(linha, coluna6).Value);
                }
                #endregion 

                #region Endereços
                ValoresExcelCadastroPessoa.Endereco[] endereco = {
                            new ValoresExcelCadastroPessoa.Endereco(
                                Convert.ToString(workSheet.Cell(linha, coluna7).Value),  //Endereco
                                Convert.ToString(workSheet.Cell(linha, coluna8).Value),  //Numero
                                Convert.ToString(workSheet.Cell(linha, coluna9).Value), //Complemento 
                                Convert.ToString(workSheet.Cell(linha, coluna10).Value), //Bairro
                                Convert.ToString(workSheet.Cell(linha, coluna11).Value), //Cidade
                                Convert.ToString(workSheet.Cell(linha, coluna12).Value), //CEP
                                Convert.ToString(workSheet.Cell(linha, coluna13).Value), //UF
                                Convert.ToString(workSheet.Cell(linha, coluna14).Value), //Pais
                                Convert.ToString(workSheet.Cell(linha, coluna15).Value), //Fone
                                Convert.ToString(workSheet.Cell(linha, coluna16).Value)  //Email
                                ),
                            new ValoresExcelCadastroPessoa.Endereco(
                                Convert.ToString(workSheet.Cell(linha, coluna17).Value), //EnderecoCom
                                Convert.ToString(workSheet.Cell(linha, coluna18).Value), //NumeroCom
                                Convert.ToString(workSheet.Cell(linha, coluna19).Value), //ComplementoCom
                                Convert.ToString(workSheet.Cell(linha, coluna20).Value), //BairroCom
                                Convert.ToString(workSheet.Cell(linha, coluna21).Value), //CidadeCom
                                Convert.ToString(workSheet.Cell(linha, coluna22).Value), //CEPCom
                                Convert.ToString(workSheet.Cell(linha, coluna23).Value), //UFCom
                                Convert.ToString(workSheet.Cell(linha, coluna24).Value), //PaisCom
                                Convert.ToString(workSheet.Cell(linha, coluna25).Value), //FoneCom
                                Convert.ToString(workSheet.Cell(linha, coluna26).Value)  //EmailCom
                                )
                        };

                item.EnderecoPessoa = endereco[0];
                item.EnderecoComercialPessoa = endereco[1];
                #endregion 




                //
                this.valoresExcelCadastroPessoa.Add(item);
                //
                index++;
                linha = 1 + index;
                //                
                Console.WriteLine("{0},{1},{2},{3},{4},{5},{6},{7}",
                    item.IdPessoa, item.Nome, item.Apelido, item.TipoPessoa, item.Documento, item.CodigoInterface,                    
                    item.EnderecoPessoa, item.EnderecoComercialPessoa);
            }
        }

        catch (Exception ex)
        {
            document.Close();
            document.Dispose();

            throw new Exception(ex.Message);
        }
        #endregion

        document.Close();
        document.Dispose();
    }

    public void CarregaCadastroPessoa(int? idTipo)
    {
        PessoaCollection pessoaCollection = new PessoaCollection();
        PessoaEnderecoCollection pessoaEnderecoCollection = new PessoaEnderecoCollection();
        PessoaTelefoneCollection pessoaTelefoneCollection = new PessoaTelefoneCollection();
        PessoaEmailCollection pessoaEmailCollection = new PessoaEmailCollection();
        //
        ContaCorrenteCollection contaCorrenteCollection = new ContaCorrenteCollection();

        //
        for (int i = 0; i < this.valoresExcelCadastroPessoa.Count; i++)
        {
            ValoresExcelCadastroPessoa valoresExcel = this.valoresExcelCadastroPessoa[i];
            //                    
            bool pessoaCadastrado = false;
            #region Verifica se já Existe Pessoa e Cliente cadastrado ou se já está na collection (continue no caso de Cliente cadastrado ou se já existir na collection)

            Pessoa pessoaAux = new Pessoa();
            List<esQueryItem> campos = new List<esQueryItem>();
            campos.Add(pessoaAux.Query.IdPessoa);

            if (pessoaAux.LoadByPrimaryKey(campos, valoresExcel.IdPessoa))
            {
                throw new Exception("Id Pessoa já existente : " + valoresExcel.IdPessoa);
            }
            #endregion

            #region Verifica se Campos Obrigatórios foram Preenchidos
            if (String.IsNullOrEmpty(valoresExcel.Nome.Trim()))
            {
                throw new Exception("Nome Cliente não pode ser em Branco");
            }
            if (valoresExcel.TipoPessoa == 0)
            { // Conversão do Enum resultou em 0
                throw new Exception("Tipo Pessoa(Física/Jurídica) deve ser Preenchido");
            }
            #endregion

            string apelidoNovaPessoa = "";
            Pessoa pessoa = pessoaCollection.AddNew();
            #region Preenche campos de Pessoa
            pessoa.IdPessoa = valoresExcel.IdPessoa;
            pessoa.Nome = valoresExcel.Nome.Trim();

            if (string.IsNullOrEmpty(valoresExcel.Apelido.Trim()))
            {
                int maxlength = (int)PessoaMetadata.Meta().Columns.FindByColumnName(PessoaMetadata.ColumnNames.Apelido).CharacterMaxLength;
                pessoa.Apelido = (pessoa.Nome.Length <= maxlength) ? pessoa.Nome : pessoa.Nome.Substring(0, maxlength - 1);
            }
            else
            {
                pessoa.Apelido = valoresExcel.Apelido;
            }
            apelidoNovaPessoa = pessoa.Apelido;

            pessoa.Tipo = (byte)valoresExcel.TipoPessoa;
            pessoa.DataCadatro = DateTime.Now;
            pessoa.DataUltimaAlteracao = DateTime.Now;

            if (!string.IsNullOrEmpty(valoresExcel.Documento))
            {
                int lengthDocumento = pessoa.Tipo == (byte)TipoPessoa.Fisica ? 11 : 14;
                string documentoNumOnly = string.Join(null, System.Text.RegularExpressions.Regex.Split(valoresExcel.Documento, "[^\\d]"))
                    .PadLeft(lengthDocumento, '0');

                pessoa.Cpfcnpj = documentoNumOnly;
            }

            pessoa.CodigoInterface = valoresExcel.CodigoInterface;
            #endregion

            #region PessoaEndereco
            if (valoresExcel.EnderecoPessoa != null)
            {
                PessoaEndereco pessoaEndereco = pessoaEnderecoCollection.AddNew();
                pessoaEndereco.IdPessoa = valoresExcel.IdPessoa;
                pessoaEndereco.RecebeCorrespondencia = "S";
                pessoaEndereco.Endereco = valoresExcel.EnderecoPessoa.Rua.Trim();
                pessoaEndereco.Numero = valoresExcel.EnderecoPessoa.Numero.Trim();
                pessoaEndereco.Complemento = valoresExcel.EnderecoPessoa.Complemento.Trim();
                pessoaEndereco.Bairro = valoresExcel.EnderecoPessoa.Bairro.Trim();
                pessoaEndereco.Cidade = valoresExcel.EnderecoPessoa.Cidade.Trim();
                pessoaEndereco.Cep = valoresExcel.EnderecoPessoa.Cep.Trim();
                pessoaEndereco.Uf = valoresExcel.EnderecoPessoa.Uf.Trim().ToUpper();
                pessoaEndereco.Pais = valoresExcel.EnderecoPessoa.Pais.Trim();
                if (!String.IsNullOrEmpty(valoresExcel.EnderecoPessoa.Fone))
                {
                    PessoaTelefone pessoaTelefone = pessoaTelefoneCollection.AddNew();
                    //
                    pessoaTelefone.IdPessoa = pessoa.IdPessoa.Value;
                    pessoaTelefone.Ddi = "";
                    pessoaTelefone.Ddd = "";
                    pessoaTelefone.Numero = valoresExcel.EnderecoPessoa.Fone;
                    pessoaTelefone.TipoTelefone = (byte)TipoTelefonePessoa.Residencial;
                }

                if (!String.IsNullOrEmpty(valoresExcel.EnderecoPessoa.Email))
                {
                    PessoaEmail pessoaEmail = pessoaEmailCollection.AddNew();
                    //
                    pessoaEmail.IdPessoa = pessoa.IdPessoa.Value;
                    pessoaEmail.Email = valoresExcel.EnderecoPessoa.Email;
                }
            }

            if (valoresExcel.EnderecoComercialPessoa != null)
            {
                PessoaEndereco pessoaEnderecoComercial = pessoaEnderecoCollection.AddNew();
                pessoaEnderecoComercial.IdPessoa = valoresExcel.IdPessoa;
                pessoaEnderecoComercial.RecebeCorrespondencia = "S";
                pessoaEnderecoComercial.Endereco = valoresExcel.EnderecoComercialPessoa.Rua.Trim();
                pessoaEnderecoComercial.Numero = valoresExcel.EnderecoComercialPessoa.Numero.Trim();
                pessoaEnderecoComercial.Complemento = valoresExcel.EnderecoComercialPessoa.Complemento.Trim();
                pessoaEnderecoComercial.Bairro = valoresExcel.EnderecoComercialPessoa.Bairro.Trim();
                pessoaEnderecoComercial.Cidade = valoresExcel.EnderecoComercialPessoa.Cidade.Trim();
                pessoaEnderecoComercial.Cep = valoresExcel.EnderecoComercialPessoa.Cep.Trim();
                pessoaEnderecoComercial.Uf = valoresExcel.EnderecoComercialPessoa.Uf.Trim().ToUpper();
                pessoaEnderecoComercial.Pais = valoresExcel.EnderecoComercialPessoa.Pais.Trim();

                if (!String.IsNullOrEmpty(valoresExcel.EnderecoComercialPessoa.Fone))
                {
                    PessoaTelefone pessoaTelefone = pessoaTelefoneCollection.AddNew();
                    //
                    pessoaTelefone.IdPessoa = pessoa.IdPessoa.Value;
                    pessoaTelefone.Ddi = "";
                    pessoaTelefone.Ddd = "";
                    pessoaTelefone.Numero = valoresExcel.EnderecoComercialPessoa.Fone;
                    pessoaTelefone.TipoTelefone = (byte)TipoTelefonePessoa.Comercial;
                }

                if (!String.IsNullOrEmpty(valoresExcel.EnderecoComercialPessoa.Email))
                {
                    PessoaEmail pessoaEmail = pessoaEmailCollection.AddNew();
                    //
                    pessoaEmail.IdPessoa = pessoa.IdPessoa.Value;
                    pessoaEmail.Email = valoresExcel.EnderecoComercialPessoa.Email;
                }

                //
                // Marcar RecebeCorrespondencia do endereço Normal se existir para N
                if (valoresExcel.EnderecoPessoa.HasValue())
                {
                    pessoaEnderecoCollection[pessoaEnderecoCollection.Count - 1].RecebeCorrespondencia = "N";
                }
            }
            #endregion

            #region Salva Contacorrente default associada ao cliente
            ContaCorrente contaCorrente = contaCorrenteCollection.AddNew();
            contaCorrente.IdPessoa = valoresExcel.IdPessoa;
            contaCorrente.Numero = valoresExcel.IdPessoa.ToString();

            TipoConta tipoConta = new TipoConta();
            tipoConta.Query.Select(tipoConta.Query.IdTipoConta.Min());
            tipoConta.Query.Load();
            if (!tipoConta.IdTipoConta.HasValue)
            {
                tipoConta = new TipoConta();
                tipoConta.Descricao = "Conta Depósito";
                tipoConta.Save();
            }
            contaCorrente.IdTipoConta = tipoConta.IdTipoConta.Value;

            contaCorrente.IdMoeda = (int)ListaMoedaFixo.Real;
            contaCorrente.ContaDefault = "S";
            #endregion
        }

        using (esTransactionScope scope = new esTransactionScope())
        {
            // Salva Posicões presentes no Excel
            pessoaCollection.Save();
            pessoaEnderecoCollection.Save();
            //           
            contaCorrenteCollection.Save();
            //
            scope.Complete();
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name=""></param>
    public void CarregaCadastroPessoa()
    {
        this.CarregaCadastroPessoa(null);
    }

    /// <summary>
    /// Processa a Planilha de Cadastro Cliente após ter acabado o Upload
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void uplCadastroPessoa_FileUploadComplete(object sender, FileUploadCompleteEventArgs e)
    {
        e.CallbackData = "";

        #region Trata Extensão Válida
        if (!ValoresExcelUtil.isExtensaoExcel(e.UploadedFile.FileName.Trim()))
        {
            e.CallbackData = "Importação Cadastro Pessoa - Formato de Arquivo Inválido. Formato permitido: .xls ou .xlsx\n\n";
            return;
        }
        #endregion

        // Stream de bytes com o Conteudo do Arquivo Excel
        Stream sr = e.UploadedFile.FileContent;

        #region Processamento Arquivo Excel

        try
        {
            // Ler Arquivo
            this.LerArquivoCadastroPessoa(sr);
            // Carrega Arquivo
            this.CarregaCadastroPessoa();
        }
        catch (Exception e2)
        {
            e.CallbackData = "Importação Cadastro Pessoa - " + e2.Message;
            return;
        }

        new HistoricoLog().InsereHistoricoLog(DateTime.Now, DateTime.Now,
            "Importação Planilha Cadastro Pessoa",
            HttpContext.Current.User.Identity.Name, UtilitarioWeb.GetIP(Request), "", HistoricoLogOrigem.Outros);

        #endregion
    }
}
