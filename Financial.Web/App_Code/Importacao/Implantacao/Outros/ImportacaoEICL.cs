using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Web.UI.HtmlControls;
using System.Collections.Generic;
using System.Text;
using Financial.Web.Common;
using Financial.Interfaces.Import.RendaFixa;
using Financial.InvestidorCotista;
using Financial.Fundo;
using DevExpress.Web;
using System.Globalization;
using Financial.InvestidorCotista.Exceptions;
using Financial.Bolsa;
using Financial.Investidor.Exceptions;
using Financial.Investidor;
using Financial.Export;

public partial class ImportacaoBasePage : BasePage {

    /// <summary>
    /// Processa arquivo txt EICL
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void uplEICL_FileUploadComplete(object sender, FileUploadCompleteEventArgs e)
    {
        e.CallbackData = "";

        // Stream de bytes com o Conteudo do Arquivo Excel
        Stream sr = e.UploadedFile.FileContent;

        FileParser parser = new FileParser();
        List<ErrorMessage> messages = parser.Parse(sr);
        string msg = "";
        foreach (ErrorMessage message in messages)
        {
            msg = msg + message.Description + ": " + "| " + message.CNPJ;
            if (message.Error1 != "")
            {
                msg = msg + "| " + message.Error1;
            }
            if (message.Error2 != "")
            {
                msg = msg + "| " + message.Error2;
            }
            if (message.Error3 != "")
            {
                msg = msg + "| " + message.Error3;
            }

            msg = msg + "\n";
        }

        if (msg == "")
        {
            msg = "EICL sem erros.";
        }

        e.CallbackData = msg;
        return;
    }
}