﻿using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Financial.Web.Common;
using System.IO;
using Financial.Integracao.Excel.Util;
using Financial.Integracao.Excel;
using EntitySpaces.Interfaces;
using Financial.Common;
using Financial.Bolsa;
using Financial.Bolsa.Enums;
using Financial.Investidor;
using System.Collections.Generic;
using Financial.Investidor.Enums;
using DevExpress.Web;
using Financial.Common.Exceptions;
using Financial.Bolsa.Exceptions;
using Financial.RendaFixa;
using Financial.RendaFixa.Enums;
using Financial.Security;
using Financial.Web.Util;
using Financial.Security.Enums;
using Financial.Util;
using Financial.Common.Enums;
using System.Collections;

public partial class ImportacaoBasePage : BasePage {

    /* Estrutura do Excel */
    private List<ValoresExcelAgendaEventosRendaFixa> valoresExcelAgendaEventosRendaFixa = new List<ValoresExcelAgendaEventosRendaFixa>();
    
    /// <summary>
    /// Leitura de um arquivo Excel
    /// Saida: List<ValoresExcel> com os valores armazenados no arquivo
    /// </summary>
    /// <param name="streamExcel">Stream de bytes de um arquivo Excel</param>
    /// <exception cref="Exception">Se ocorreu problemas</exception>
    private void LerArquivoAgendaEventos(Stream streamExcel) {

        #region Leitura do Arquivo Excel

        // Open Spreadsheet
        Bytescout.Spreadsheet.Spreadsheet document = new Bytescout.Spreadsheet.Spreadsheet();
        document.LoadFromStream(streamExcel);

        Bytescout.Spreadsheet.Worksheet workSheet = document.Workbook.Worksheets[0];

        #region Confere Formato Arquivo
        string[] colunasConferencias = new string[] {"IdTitulo","TipoEvento","DataAgenda",
                                                     "DataEvento","DataPagamento","Taxa","Valor"};

        bool formato = ValoresExcelUtil.isExcelFormatoValido(colunasConferencias, workSheet);
        if (!formato) {
            string mensagem = "Formato Interno do Arquivo Inválido.\n";
            mensagem += "Linha 1 do Arquivo deve ser: \n";
            for (int i = 0; i < colunasConferencias.Length; i++) {
                int coluna = i + 1;
                mensagem += "\tColuna " + coluna + ": " + colunasConferencias[i] + "\n";
            }

            document.Close();
            document.Dispose();

            throw new Exception(mensagem);
        }
        #endregion
        //
        int index = 0;
        // This row,column index should be changed as per your need.
        // i.e. which cell in the excel you are interesting to read.
        //
        /* Formato: 1)IdTitulo       - 2)TipoEvento         - 3)DataAgenda
         *          4)DataEvento     - 5)DataPagamento      - 6)Taxa
         *          7)Valor 
         */
        int linha = 1;
        int coluna1 = 0, coluna2 = 1, coluna3 = 2,
            coluna4 = 3, coluna5 = 4, coluna6 = 5, coluna7 = 6;
        //
        try {
            // Enquanto tiver registro
            while (workSheet.Cell(linha, coluna1).Value != null) {
                ValoresExcelAgendaEventosRendaFixa item = new ValoresExcelAgendaEventosRendaFixa();
                
                try {
                    item.IdTitulo = workSheet.Cell(linha, coluna1).ValueAsInteger;
                }
                catch (Exception ex) {
                    throw new Exception(ex.Message + " - IdTitulo: linha " + (linha + 1));
                }

                if (workSheet.Cell(linha, coluna2).Value == null)
                    throw new Exception("Tipo não informado: linha " + (linha + 1));
                try
                {
                    item.Tipo = (TipoEventoTitulo)workSheet.Cell(linha, coluna2).ValueAsInteger;
                }
                catch (Exception ex) {
                    throw new Exception(ex.Message + " - Tipo: linha " + (linha + 1));
                }

                if (workSheet.Cell(linha, coluna3).Value == null)
                    throw new Exception("DataAgenda não informada: linha " + (linha + 1));
                try
                {
                    item.DataAgenda = workSheet.Cell(linha, coluna3).ValueAsDateTime;
                }
                catch (Exception ex) {
                    throw new Exception(ex.Message + " - DataAgenda: linha " + (linha + 1));
                }

                if (workSheet.Cell(linha, coluna4).Value == null)
                    throw new Exception("DataEvento não informada: linha " + (linha + 1));
                try
                {
                    item.DataEvento = workSheet.Cell(linha, coluna4).ValueAsDateTime;
                }
                catch (Exception ex) {
                    throw new Exception(ex.Message + " - DataEvento: linha " + (linha + 1));
                }

                if (workSheet.Cell(linha, coluna5).Value == null)
                    throw new Exception("DataPagamento não informada: linha " + (linha + 1));
                try
                {
                    item.DataPagamento = workSheet.Cell(linha, coluna5).ValueAsDateTime;
                }
                catch (Exception ex) {
                    throw new Exception(ex.Message + " - DataPagamento: linha " + (linha + 1));
                }

                if (workSheet.Cell(linha, coluna6).Value == null)
                    throw new Exception("Taxa não informada: linha " + (linha + 1));
                try
                {
                    item.Taxa = Convert.ToDecimal(workSheet.Cell(linha, coluna6).Value);
                }
                catch (Exception ex) {
                    throw new Exception(ex.Message + " - Taxa: linha " + (linha + 1));
                }

                if (workSheet.Cell(linha, coluna7).Value == null)
                    throw new Exception("Valor não informado: linha " + (linha + 1));
                try
                {
                    item.Valor = Convert.ToDecimal(workSheet.Cell(linha, coluna7).Value);
                }
                catch (Exception ex) {
                    throw new Exception(ex.Message + " - Valor: linha " + (linha + 1));
                }

                this.valoresExcelAgendaEventosRendaFixa.Add(item);
                
                index++;
                linha = 1 + index;
                
                Console.WriteLine("{0},{1},{2},{3},{4},{5},{6}", item.IdTitulo, item.Tipo, item.DataAgenda.ToString("dd/MM/yyyy"),
                                    item.DataEvento.ToString("dd/MM/yyyy"), item.DataPagamento.ToString("dd/MM/yyyy"),
                                    item.Taxa, item.Valor);
            }
        }
        catch (Exception ex) {
            document.Close();
            document.Dispose();

            throw new Exception(ex.Message);
        }
        #endregion

        document.Close();
        document.Dispose();
    }

    /// <summary>
    /// Gera e carrega a Agenda de Eventos
    /// com o objeto List<ValoresExcel>
    /// </summary>
    /// <exception cref="Exception">Se ocorreu problemas</exception>
    private void CarregaAgendaEventos() {
        AgendaRendaFixaCollection agendaRendaFixaCollection = new AgendaRendaFixaCollection();
        AgendaRendaFixaCollection agendaRendaFixaCollectionDeletar = new AgendaRendaFixaCollection();

        Hashtable hsTituloRendaFixa = new Hashtable();
        //
        for (int i = 0; i < this.valoresExcelAgendaEventosRendaFixa.Count; i++) {
            ValoresExcelAgendaEventosRendaFixa valoresExcel = this.valoresExcelAgendaEventosRendaFixa[i];
            // Salva o IdTitulo para poder Deletar
            this.idTituloDeletarAgendaRendaFixa.Add(valoresExcel.IdTitulo);
            //               
            #region Verifica se Existe idTitulo
            TituloRendaFixa tituloRendaFixa = new TituloRendaFixa();
            if (!hsTituloRendaFixa.Contains(valoresExcel.IdTitulo))
            {
                if (!tituloRendaFixa.LoadByPrimaryKey(valoresExcel.IdTitulo))
                {
                    throw new Exception("Titulo Renda Fixa não existente : " + valoresExcel.IdTitulo);
                }

                hsTituloRendaFixa.Add(valoresExcel.IdTitulo, tituloRendaFixa);
            }
            else
            {
                tituloRendaFixa = (TituloRendaFixa)hsTituloRendaFixa[valoresExcel.IdTitulo];
            }
            #endregion

            #region TipoEvento Inconsistente
            if ((int)valoresExcel.Tipo < 1 || (int)valoresExcel.Tipo == 6 || (int)valoresExcel.Tipo > 8) {
                throw new Exception("Tipo Evento Inconsistente. Valores Possíveis: 1, 2, 3, 4, 5, 7, 8");
            }                       
            #endregion

            //Consiste data de agenda
            if(tituloRendaFixa.DataVencimento.Value < valoresExcel.DataAgenda)
                throw new Exception("Data da agenda não pode ser maior que a data de vencimento do título: Id.Titulo - " + valoresExcel.IdTitulo + " Dt.Vencimento - " + tituloRendaFixa.DataVencimento.Value.ToString("dd/MM/yyyy"));

            //Consiste data de agenda
            if (tituloRendaFixa.DataVencimento.Value < valoresExcel.DataEvento)
                throw new Exception("Data do evento não pode ser maior que a data de vencimento do título: Id.Titulo - " + valoresExcel.IdTitulo + " Dt.Vencimento - " + tituloRendaFixa.DataVencimento.Value.ToString("dd/MM/yyyy"));

            #region AgendaRendaFixa
            //
            AgendaRendaFixa agendaRendaFixa = new AgendaRendaFixa();
            agendaRendaFixa.IdTitulo = valoresExcel.IdTitulo;
            agendaRendaFixa.TipoEvento = Convert.ToByte(valoresExcel.Tipo);

            #region DataAgenda            
            agendaRendaFixa.DataAgenda = valoresExcel.DataAgenda;
            #endregion

            #region DataEvento           
            agendaRendaFixa.DataEvento = valoresExcel.DataEvento;
            #endregion

            #region DataPagamento
            if (!Calendario.IsDiaUtil(valoresExcel.DataAgenda, (byte)LocalFeriadoFixo.Brasil, TipoFeriado.Brasil))
                agendaRendaFixa.DataPagamento = Calendario.AdicionaDiaUtil(valoresExcel.DataPagamento, 1, (byte)LocalFeriadoFixo.Brasil, TipoFeriado.Brasil);
            else
                agendaRendaFixa.DataPagamento = valoresExcel.DataPagamento;
            #endregion

            agendaRendaFixa.Valor = valoresExcel.Valor;
            agendaRendaFixa.Taxa = valoresExcel.Taxa;
            //
            agendaRendaFixaCollection.AttachEntity(agendaRendaFixa);

            #endregion
        }

        using (esTransactionScope scope = new esTransactionScope()) {

            #region Deleta AgendaRendaFixa por Idtitulo
            agendaRendaFixaCollectionDeletar.Query.Where(agendaRendaFixaCollectionDeletar.Query.IdTitulo.In(this.idTituloDeletarAgendaRendaFixa));
            agendaRendaFixaCollectionDeletar.Query.Load();
            agendaRendaFixaCollectionDeletar.MarkAllAsDeleted();
            agendaRendaFixaCollectionDeletar.Save();
            #endregion

            // Salva Agenda RendaFixa presente no Excel
            agendaRendaFixaCollection.Save();
            //
            scope.Complete();
        }
    }

    /// <summary>
    /// Processa a Planilha de Agenda RendaFixa após ter acabado o Upload
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void uplAgendaEventos_FileUploadComplete(object sender, FileUploadCompleteEventArgs e) {
        e.CallbackData = "";

        #region Trata Extensão Válida
        if (!ValoresExcelUtil.isExtensaoExcel(e.UploadedFile.FileName.Trim())) {
            e.CallbackData = "Importação AgendaRendaFixa - Formato de Arquivo Inválido. Formato permitido: .xls ou .xlsx\n\n";
            return;
        }
        #endregion

        // Stream de bytes com o Conteudo do Arquivo Excel
        Stream sr = e.UploadedFile.FileContent;

        #region Processamento Arquivo Excel

        //Reseta vetor de dados
        //this.valoresExcelCadastroCotista = new List<ValoresExcelCadastroCotista>();

        try {
            // Ler Arquivo
            this.LerArquivoAgendaEventos(sr);
            // Carrega Arquivo
            this.CarregaAgendaEventos();
        }
        catch (Exception e2) {
            e.CallbackData = "Importação Agenda Eventos - " + e2.Message;
            return;
        }

        new HistoricoLog().InsereHistoricoLog(DateTime.Now, DateTime.Now,
            "Importação Planilha Agenda Eventos Renda Fixa",
            HttpContext.Current.User.Identity.Name, UtilitarioWeb.GetIP(Request), "", HistoricoLogOrigem.Outros);

        #endregion
    }
}