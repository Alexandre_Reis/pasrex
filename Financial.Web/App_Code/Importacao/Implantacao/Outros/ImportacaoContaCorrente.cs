﻿using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Financial.Web.Common;
using System.IO;
using Financial.Integracao.Excel.Util;
using Financial.Integracao.Excel;
using EntitySpaces.Interfaces;
using Financial.Common;
using Financial.Bolsa;
using Financial.Bolsa.Enums;
using Financial.Investidor;
using System.Collections.Generic;
using Financial.Investidor.Enums;
using DevExpress.Web;
using Financial.Common.Exceptions;
using Financial.Bolsa.Exceptions;
using Financial.RendaFixa;
using System.Text;
using Financial.RendaFixa.Enums;
using Financial.Common.Enums;
using Financial.Security;
using Financial.Web.Util;
using Financial.Security.Enums;
using Financial.CRM;

public partial class ImportacaoBasePage : BasePage {

    /* Estrutura do Excel */
    private List<ValoresExcelContaCorrente> valoresExcelContaCorrente = new List<ValoresExcelContaCorrente>();
    //

    /// <summary>
    /// Leitura de um arquivo Excel
    /// Saida: List<ValoresExcel> com os valores armazenados no arquivo
    /// </summary>
    /// <param name="streamExcel">Stream de bytes de um arquivo Excel</param>
    /// <exception cref="Exception">Se ocorreu problemas</exception>
    private void LerArquivoContaCorrente(Stream streamExcel) {

        #region Leitura do Arquivo Excel

        // Open Spreadsheet
        Bytescout.Spreadsheet.Spreadsheet document = new Bytescout.Spreadsheet.Spreadsheet();
        document.LoadFromStream(streamExcel);

        Bytescout.Spreadsheet.Worksheet workSheet = document.Workbook.Worksheets[0];

        #region Confere Formato Arquivo
        string[] colunasConferencias = new string[] { "IdPessoa", "CodigoBanco", "NumeroAgencia", "DVAgencia", "NumeroConta", "DVConta","IdLocal"};

        bool formato = ValoresExcelUtil.isExcelFormatoValido(colunasConferencias, workSheet);
        if (!formato) {
            string mensagem = "Formato Interno do Arquivo Inválido.\n";
            mensagem += "Linha 1 do Arquivo deve ser: \n";
            for (int i = 0; i < colunasConferencias.Length; i++) {
                int coluna = i + 1;
                mensagem += "\tColuna " + coluna + ": " + colunasConferencias[i] + "\n";
            }

            document.Close();
            document.Dispose();

            throw new Exception(mensagem);
        }
        #endregion
        //
        int index = 0;
        // This row,column index should be changed as per your need.
        // i.e. which cell in the excel you are interesting to read.
        //
        /* Formato: 1)NomeEmissor    - 2)CnpjEmissor
         */
        int linha = 1;
        int coluna1 = 0, coluna2 = 1, coluna3 = 2, coluna4 = 3, coluna5 = 4, coluna6 = 5 , coluna7 = 6;
        //
        try {
            // Enquanto tiver registro
            while (workSheet.Cell(linha, coluna1).Value != null)
            {
                ValoresExcelContaCorrente item = new ValoresExcelContaCorrente();
                
                try {
                    item.IdPessoa = Convert.ToInt32(workSheet.Cell(linha, coluna1).Value);
                }
                catch (Exception ex) {
                    throw new Exception(ex.Message + " - IdPessoa: linha " + (linha + 1));
                }

                item.CodigoBanco = null;
                if (workSheet.Cell(linha, coluna2).Value != null)
                {
                    try {
                        item.CodigoBanco = workSheet.Cell(linha, coluna2).ValueAsString;
                    }
                    catch (Exception ex) {
                        throw new Exception(ex.Message + " - CodigoBanco: linha " + (linha + 1));
                    }
                }

                item.NumeroAgencia = null;
                if (workSheet.Cell(linha, coluna3).Value != null)
                {
                    try {
                        item.NumeroAgencia = workSheet.Cell(linha, coluna3).ValueAsString;
                    }
                    catch (Exception ex) {
                        throw new Exception(ex.Message + " - NumeroAgencia: linha " + (linha + 1));
                    }
                }

                item.DvAgencia = null;
                if (workSheet.Cell(linha, coluna4).Value != null)
                {
                    try {
                        item.DvAgencia = workSheet.Cell(linha, coluna4).ValueAsString;
                    }
                    catch (Exception ex) {
                        throw new Exception(ex.Message + " - DvAgencia: linha " + (linha + 1));
                    }
                }

                item.NumeroConta = null;
                if (workSheet.Cell(linha, coluna5).Value != null)
                {
                    try {
                        item.NumeroConta = workSheet.Cell(linha, coluna5).ValueAsString;
                    }
                    catch (Exception ex) {
                        throw new Exception(ex.Message + " - NumeroConta: linha " + (linha + 1));
                    }
                }

                item.DvConta = null;
                if (workSheet.Cell(linha, coluna6).Value != null)
                {
                    try {
                        item.DvConta = workSheet.Cell(linha, coluna6).ValueAsString;
                    }
                    catch (Exception ex) {
                        throw new Exception(ex.Message + " - DvConta: linha " + (linha + 1));
                    }
                }

                item.IdLocal = null;
                if (workSheet.Cell(linha, coluna7).Value != null)
                {
                    try
                    {
                        item.IdLocal = workSheet.Cell(linha, coluna7).ValueAsString;
                    }
                    catch (Exception ex)
                    {
                        throw new Exception(ex.Message + " - IdLocal: linha " + (linha + 1));
                    }
                }

                this.valoresExcelContaCorrente.Add(item);
                //
                index++;
                linha = 1 + index;
                //                
                Console.WriteLine("{0},{1},{2},{3},{4},{5},{6} ", item.IdPessoa, item.CodigoBanco, item.NumeroAgencia, item.DvAgencia, item.NumeroConta, item.DvConta, item.IdLocal);
            }
        }
        catch (Exception ex) {
            document.Close();
            document.Dispose();

            throw new Exception(ex.Message);
        }
        #endregion

        document.Close();
        document.Dispose();
    }

    /// <summary>
    /// Gera e Carrega Emissor com o objeto List<ValoresExcel>
    /// </summary>
    /// <exception cref="Exception">Se ocorreu problemas</exception>
    private void CarregaContaCorrente() {

        ContaCorrenteCollection contaCorrenteCollection = new ContaCorrenteCollection();        
        //
        for (int i = 0; i < this.valoresExcelContaCorrente.Count; i++)
        {
            bool contaJaCadastrada = false;
            ValoresExcelContaCorrente valoresExcel = this.valoresExcelContaCorrente[i];
            //               
            #region Verifica se Existe ContaCorrente
            ContaCorrenteCollection contaCorrenteCollectionAux = new ContaCorrenteCollection();

            ContaCorrenteQuery contaCorrenteQuery = new ContaCorrenteQuery("C");
            BancoQuery bancoQuery = new BancoQuery("B");
            AgenciaQuery agenciaQuery = new AgenciaQuery("A");
            contaCorrenteQuery.Select(contaCorrenteQuery.IdConta);
            contaCorrenteQuery.InnerJoin(bancoQuery).On(contaCorrenteQuery.IdBanco == bancoQuery.IdBanco);
            contaCorrenteQuery.InnerJoin(agenciaQuery).On(contaCorrenteQuery.IdAgencia == agenciaQuery.IdAgencia);
            contaCorrenteQuery.Where(bancoQuery.CodigoCompensacao.Equal(valoresExcel.CodigoBanco),
                                    agenciaQuery.Codigo.Equal(valoresExcel.NumeroAgencia),
                                    contaCorrenteQuery.Numero.Equal(valoresExcel.NumeroConta));
            if (valoresExcel.DvAgencia != null)
            {
                contaCorrenteQuery.Where(agenciaQuery.DigitoAgencia.Equal(valoresExcel.DvAgencia));
            }
            if (valoresExcel.DvConta != null)
            {
                contaCorrenteQuery.Where(contaCorrenteQuery.DigitoConta.Equal(valoresExcel.DvConta));
            }

            if (contaCorrenteCollectionAux.Load(contaCorrenteQuery))
            {
                contaJaCadastrada = true;
                continue;
            }
            #endregion

            if (!contaJaCadastrada)
            {
                #region Verifica se existe idPessoa
                Pessoa pessoa = new Pessoa();

                if (!pessoa.LoadByPrimaryKey(Convert.ToInt32(valoresExcel.IdPessoa)))
                {
                    throw new Exception("Pessoa não cadastrada: " + valoresExcel.IdPessoa);
                }
                #endregion

                #region verifica IdLocal                
                if( ! LocalFeriadoFixo.Values().Contains(Convert.ToInt32(valoresExcel.IdLocal)))
                {
                    throw new Exception("Local não Cadastrado: " + valoresExcel.IdLocal);
                }                
                #endregion

                #region verifica Banco e se não tiver cadastra
                int idBanco = 0;
                BancoCollection bancoCollection = new BancoCollection();
                bancoCollection.Query.Select(bancoCollection.Query.IdBanco);
                bancoCollection.Query.Where(bancoCollection.Query.CodigoCompensacao.Equal(Convert.ToInt32(valoresExcel.CodigoBanco)));
                bancoCollection.Query.es.Distinct = true;
                bancoCollection.Query.Load();
                if (bancoCollection.Count > 0)
                {
                    idBanco = (int)bancoCollection[0].IdBanco;
                }
                else
                {
                    Banco banco = new Banco();
                    banco.Nome = valoresExcel.CodigoBanco;
                    banco.CodigoCompensacao = valoresExcel.CodigoBanco;
                    banco.Save();
                    idBanco = (int)banco.IdBanco;
                }
                #endregion

                #region agencia
                int idAgencia = 0;
                AgenciaCollection agenciaCollection = new AgenciaCollection();
                agenciaCollection.Query.Select(agenciaCollection.Query.IdAgencia);
                agenciaCollection.Query.Where(agenciaCollection.Query.Codigo.Equal(Convert.ToInt32(valoresExcel.NumeroAgencia)));
                agenciaCollection.Query.Where(agenciaCollection.Query.IdBanco.Equal(idBanco));
                if (valoresExcel.DvAgencia != null)
                {
                    agenciaCollection.Query.Where(agenciaCollection.Query.DigitoAgencia.Equal(valoresExcel.DvAgencia));
                }
                agenciaCollection.Query.es.Distinct = true;
                agenciaCollection.Query.Load();
                if (agenciaCollection.Count > 0)
                {
                    idAgencia = (int)agenciaCollection[0].IdAgencia;
                }
                else
                {
                    Agencia agencia = new Agencia();
                    agencia.IdBanco = idBanco;
                    agencia.Codigo = valoresExcel.NumeroAgencia;
                    agencia.Nome = valoresExcel.NumeroAgencia;
                    agencia.DigitoAgencia = valoresExcel.DvAgencia;
                    agencia.IdLocal = Convert.ToInt16(valoresExcel.IdLocal);
                    agencia.Save();
                    idAgencia = (int)agencia.IdAgencia;
                }
                #endregion

                ContaCorrente contaCorrente = contaCorrenteCollection.AddNew();
                //
                contaCorrente.IdBanco = idBanco;
                contaCorrente.IdAgencia = idAgencia;
                contaCorrente.IdPessoa = (int)valoresExcel.IdPessoa;
                contaCorrente.Numero = valoresExcel.NumeroConta.ToString();
                contaCorrente.IdLocal = Convert.ToInt16(valoresExcel.IdLocal);

                TipoConta tipoConta = new TipoConta();
                tipoConta.Query.Select(tipoConta.Query.IdTipoConta.Min());
                tipoConta.Query.Load();
                if (!tipoConta.IdTipoConta.HasValue)
                {
                    tipoConta = new TipoConta();
                    tipoConta.Descricao = "Conta Depósito";
                    tipoConta.Save();
                }
                contaCorrente.IdTipoConta = tipoConta.IdTipoConta.Value;

                contaCorrente.ContaDefault = "N";
                contaCorrente.IdMoeda = (int)ListaMoedaFixo.Real;
                contaCorrente.DigitoConta = valoresExcel.DvConta;
                contaCorrenteCollection.Save();
            }
        }

        if (this.valoresExcelContaCorrente.Count == 0)
        {
            throw new Exception("Não há dados para importar.");
        }
    }

    /// <summary>
    /// Processa a Planilha de Emissor após ter acabado o Upload
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void uplContaCorrente_FileUploadComplete(object sender, FileUploadCompleteEventArgs e) {
        e.CallbackData = "";

        #region Trata Extensão Válida
        if (!ValoresExcelUtil.isExtensaoExcel(e.UploadedFile.FileName.Trim())) {
            e.CallbackData = "Importação Emissor - Formato de Arquivo Inválido. Formato permitido: .xls ou .xlsx\n\n";
            return;
        }
        #endregion

        // Stream de bytes com o Conteudo do Arquivo Excel
        Stream sr = e.UploadedFile.FileContent;

        #region Processamento Arquivo Excel

        //Reseta vetor de dados
        this.valoresExcelContaCorrente = new List<ValoresExcelContaCorrente>();

        try {
            // Ler Arquivo
            this.LerArquivoContaCorrente(sr);
            // Carrega Arquivo
            this.CarregaContaCorrente();
        }
        catch (Exception e2) {
            e.CallbackData = "Importação Conta Corrente - " + e2.Message;
            return;
        }

        new HistoricoLog().InsereHistoricoLog(DateTime.Now, DateTime.Now,
            "Importação Planilha Conta Corrente",
            HttpContext.Current.User.Identity.Name, UtilitarioWeb.GetIP(Request), "", HistoricoLogOrigem.Outros);

        #endregion
    }
}