﻿using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Web.UI.HtmlControls;
using System.Collections.Generic;
using System.Text;
using Financial.Web.Common;
using Financial.Interfaces.Import.RendaFixa;
using Financial.InvestidorCotista;
using Financial.Fundo;
using DevExpress.Web;
using Financial.Interfaces.Import.YMF;

public partial class ImportacaoBasePage : BasePage
{

    public void uplMovimentoYMF_FileUploadComplete(object sender, FileUploadCompleteEventArgs e)
    {
        e.CallbackData = "";

        #region Trata Extensão Válida

        if (!this.isExtensaoValida(e.UploadedFile.FileName.Trim()))
        {
            e.CallbackData = "Arquivo Movimento YMF inválido: Extensão do arquivo deve ser .txt, .dat ou nula\n\n";
            return;
        }
        #endregion

        // Stream de bytes com o Conteudo do Arquivo Excel
        Stream sr = e.UploadedFile.FileContent;

        #region Processamento Arquivo Excel
        try
        {
            YMF ymf = new YMF();
            //Financial.Interfaces.Import.YMF.MovimentoYMF[] movimentos = ymf.ImportaMovimentoYMF(sr);

            /*OperacaoCotistaCollection operacaoCotistaCollection = new OperacaoCotistaCollection();
            operacaoCotistaCollection.ImportaMovimentoYMF(movimentos);*/

        }
        catch (Exception e2)
        {
            e.CallbackData = "Importação Movimento YMF - " + e2.Message;
            return;
        }
        #endregion
    }
}