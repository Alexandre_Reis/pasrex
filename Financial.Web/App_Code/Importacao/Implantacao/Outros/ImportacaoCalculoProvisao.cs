﻿using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Financial.Web.Common;
using System.IO;
using Financial.Integracao.Excel.Util;
using Financial.Integracao.Excel;
using EntitySpaces.Interfaces;
using Financial.Common;
using Financial.Bolsa;
using Financial.Bolsa.Enums;
using Financial.Investidor;
using System.Collections.Generic;
using Financial.Investidor.Enums;
using DevExpress.Web;
using Financial.Common.Exceptions;
using Financial.Bolsa.Exceptions;
using Financial.Fundo;
using Financial.Util;
using Financial.Web.Util;
using Financial.Security.Enums;
using Financial.Security;

public partial class ImportacaoBasePage : BasePage {

    /* Estrutura do Excel */
    private List<ValoresExcelCalculoProvisao> valoresExcelCalculoProvisao = new List<ValoresExcelCalculoProvisao>();
    //
    List<int> idTabelaDeletarCalculoProvisao = new List<int>();
    List<DateTime> dataHistoricoDeletarCalculoProvisao = new List<DateTime>();

    /// <summary>
    /// Leitura de um arquivo Excel
    /// Saida: List<ValoresExcel> com os valores armazenados no arquivo
    /// </summary>
    /// <param name="streamExcel">Stream de bytes de um arquivo Excel</param>
    /// <exception cref="Exception">Se ocorreu problemas</exception>
    private void LerArquivoCalculoProvisao(Stream streamExcel) {

        #region Leitura do Arquivo Excel
        // Open Spreadsheet
        Bytescout.Spreadsheet.Spreadsheet document = new Bytescout.Spreadsheet.Spreadsheet();
        document.LoadFromStream(streamExcel);

        Bytescout.Spreadsheet.Worksheet workSheet = document.Workbook.Worksheets[0];
        //
        #region Confere Formato Arquivo
        string[] colunasConferencias = new string[] {"IdTabela","IdCarteira","ValorDia",
                                                        "ValorAcumulado","DataFimApropriacao","DataPagamento"};

        bool formato = ValoresExcelUtil.isExcelFormatoValido(colunasConferencias, workSheet);
        if (!formato) {
            string mensagem = "Formato Interno do Arquivo Inválido.\n";
            mensagem += "Linha 1 do Arquivo deve ser: \n";
            for (int i = 0; i < colunasConferencias.Length; i++) {
                int coluna = i + 1;
                mensagem += "\tColuna " + coluna + ": " + colunasConferencias[i] + "\n";
            }

            document.Close();
            document.Dispose();

            throw new Exception(mensagem);
        }
        #endregion
        //
        int index = 0;
        // This row,column index should be changed as per your need.
        // i.e. which cell in the excel you are interesting to read.
        //
        /* Formato: 1)IdTabela       - 2)IdCarteira         - 3)ValorDia
         *          4)ValorAcumulado - 5)DataFimApropriacao - 6)DataPagamento             
         */
        int linha = 1;
        int coluna1 = 0, coluna2 = 1, coluna3 = 2,
            coluna4 = 3, coluna5 = 4, coluna6 = 5;
        //
        try {
            // Enquanto tiver registro
            while (workSheet.Cell(linha, coluna1).Value != null) {
                ValoresExcelCalculoProvisao item = new ValoresExcelCalculoProvisao();
                
                try {
                    item.IdTabela = workSheet.Cell(linha, coluna1).ValueAsInteger;
                }
                catch (Exception ex) {
                    throw new Exception(ex.Message + " - IdTabela: linha " + (linha + 1));
                }

                if (workSheet.Cell(linha, coluna2).Value == null)
                    throw new Exception("IdCarteira não informado: linha " + (linha + 1));
                try
                {
                    item.IdCarteira = workSheet.Cell(linha, coluna2).ValueAsInteger;
                }
                catch (Exception ex) {
                    throw new Exception(ex.Message + " - IdCarteira: linha " + (linha + 1));
                }

                if (workSheet.Cell(linha, coluna3).Value == null)
                    throw new Exception("ValorDia não informado: linha " + (linha + 1));
                try
                {
                    item.ValorDia = Convert.ToDecimal(workSheet.Cell(linha, coluna3).Value);
                }
                catch (Exception ex) {
                    throw new Exception(ex.Message + " - ValorDia: linha " + (linha + 1));
                }

                if (workSheet.Cell(linha, coluna4).Value == null)
                    throw new Exception("ValorAcumulado não informado: linha " + (linha + 1));
                try
                {
                    item.ValorAcumulado = Convert.ToDecimal(workSheet.Cell(linha, coluna4).Value);
                }
                catch (Exception ex) {
                    throw new Exception(ex.Message + " - ValorAcumulado: linha " + (linha + 1));
                }

                if (workSheet.Cell(linha, coluna5).Value == null)
                    throw new Exception("DataFimApropriacao não informado: linha " + (linha + 1));
                try
                {
                    item.DataFimApropriacao = workSheet.Cell(linha, coluna5).ValueAsDateTime;
                }
                catch (Exception ex) {
                    throw new Exception(ex.Message + " - DataFimApropriacao: linha " + (linha + 1));
                }

                if (workSheet.Cell(linha, coluna6).Value == null)
                    throw new Exception("DataPagamento não informado: linha " + (linha + 1));
                try
                {
                    item.DataPagamento = workSheet.Cell(linha, coluna6).ValueAsDateTime;
                }
                catch (Exception ex) {
                    throw new Exception(ex.Message + " - DataPagamento: linha " + (linha + 1));
                }

                //
                this.valoresExcelCalculoProvisao.Add(item);
                //
                index++;
                linha = 1 + index;
                //                
                Console.WriteLine("{0},{1},{2},{3},{4},{5}", item.IdTabela, item.IdCarteira, item.ValorDia,
                                   item.ValorAcumulado, item.DataFimApropriacao.ToString("dd/MM/yyyy"),
                                   item.DataPagamento.ToString("dd/MM/yyyy"));
            }
        }
        catch (Exception ex) {
            document.Close();
            document.Dispose();

            throw new Exception(ex.Message);
        }
        #endregion

        document.Close();
        document.Dispose();
    }

    /// <summary>
    /// Gera e Carrega os Calculos Provisão e Provisão Histórico
    /// com o objeto List<ValoresExcel>
    /// </summary>
    /// <exception cref="Exception">Se ocorreu problemas</exception>
    private void CarregaCalculoProvisao() {
        CalculoProvisaoCollection calculoProvisaoCollection = new CalculoProvisaoCollection();
        CalculoProvisaoCollection calculoProvisaoDeletarCollection = new CalculoProvisaoCollection();
        //
        CalculoProvisaoHistoricoCollection calculoProvisaoHistoricoCollection = new CalculoProvisaoHistoricoCollection();
        CalculoProvisaoHistoricoCollection calculoProvisaoHistoricoDeletarCollection = new CalculoProvisaoHistoricoCollection();
        //    
        for (int i = 0; i < this.valoresExcelCalculoProvisao.Count; i++) {
            ValoresExcelCalculoProvisao valoresExcel = this.valoresExcelCalculoProvisao[i];
            // Salva o IdTabela para poder Deletar
            this.idTabelaDeletarCalculoProvisao.Add(valoresExcel.IdTabela);
            //               
            #region Verifica se Existe Carteira
            Carteira carteira = new Carteira();
            if (!carteira.LoadByPrimaryKey(valoresExcel.IdCarteira)) {
                throw new Exception("Carteira Não existente : " + valoresExcel.IdCarteira);
            }
            #endregion

            #region Verifica se Existe idTabela
            TabelaProvisao tabelaProvisao = new TabelaProvisao();
            tabelaProvisao.Query
                  .Where(tabelaProvisao.Query.IdTabela == valoresExcel.IdTabela &
                        tabelaProvisao.Query.IdCarteira == valoresExcel.IdCarteira);

            if (!tabelaProvisao.Query.Load()) {
                throw new Exception("Tabela Provisão não existente : " + valoresExcel.IdTabela + " para a carteira " + valoresExcel.IdCarteira);
            }
            #endregion

            #region pega o Cliente
            Cliente cliente = new Cliente();
            List<esQueryItem> campos = new List<esQueryItem>();
            campos.Add(cliente.Query.IdCliente);
            campos.Add(cliente.Query.DataDia);
            cliente.LoadByPrimaryKey(campos, valoresExcel.IdCarteira);
            // Salva a Data Historico para poder Deletar
            DateTime dataAuxiliar = Calendario.SubtraiDiaUtil(cliente.DataDia.Value, 1);
            this.dataHistoricoDeletarCalculoProvisao.Add(dataAuxiliar);
            #endregion

            #region CalculoProvisao
            //
            CalculoProvisao calculoProvisao = new CalculoProvisao();
            calculoProvisao.IdTabela = valoresExcel.IdTabela;
            calculoProvisao.IdCarteira = valoresExcel.IdCarteira;
            calculoProvisao.ValorDia = valoresExcel.ValorDia;
            calculoProvisao.ValorAcumulado = valoresExcel.ValorAcumulado;
            calculoProvisao.DataFimApropriacao = valoresExcel.DataFimApropriacao;
            calculoProvisao.DataPagamento = valoresExcel.DataPagamento;
            //
            calculoProvisao.ValorCPMFDia = 0;
            calculoProvisao.ValorCPMFAcumulado = 0;

            // Attach the entity
            calculoProvisaoCollection.AttachEntity(calculoProvisao);

            #endregion
            //
            #region CalculoProvisaoHistórico
            //
            CalculoProvisaoHistorico calculoProvisaoHistorico = new CalculoProvisaoHistorico();
            calculoProvisaoHistorico.DataHistorico = dataAuxiliar;
            calculoProvisaoHistorico.IdTabela = calculoProvisao.IdTabela;
            calculoProvisaoHistorico.IdCarteira = calculoProvisao.IdCarteira;
            calculoProvisaoHistorico.ValorDia = calculoProvisao.ValorDia;
            calculoProvisaoHistorico.ValorAcumulado = calculoProvisao.ValorAcumulado;
            calculoProvisaoHistorico.DataFimApropriacao = calculoProvisao.DataFimApropriacao;
            calculoProvisaoHistorico.DataPagamento = calculoProvisao.DataPagamento;
            calculoProvisaoHistorico.ValorCPMFDia = calculoProvisao.ValorCPMFDia;
            calculoProvisaoHistorico.ValorCPMFAcumulado = calculoProvisao.ValorCPMFAcumulado;

            // Attach the object
            calculoProvisaoHistoricoCollection.AttachEntity(calculoProvisaoHistorico);
            #endregion
        }

        using (esTransactionScope scope = new esTransactionScope()) {

            #region Deleta CalculoProvisao por Idtabela
            calculoProvisaoDeletarCollection.Query.Where(calculoProvisaoDeletarCollection.Query.IdTabela.In(this.idTabelaDeletarCalculoProvisao));
            calculoProvisaoDeletarCollection.Query.Load();
            calculoProvisaoDeletarCollection.MarkAllAsDeleted();
            calculoProvisaoDeletarCollection.Save();
            #endregion

            #region Deleta CalculoProvisaoHistorico por IdTabela/DataHistorico
            for (int i = 0; i < this.idTabelaDeletarCalculoProvisao.Count; i++) {
                calculoProvisaoHistoricoDeletarCollection = new CalculoProvisaoHistoricoCollection();
                calculoProvisaoHistoricoDeletarCollection.Query
                    .Where(calculoProvisaoHistoricoDeletarCollection.Query.IdTabela == this.idTabelaDeletarCalculoProvisao[i],
                           calculoProvisaoHistoricoDeletarCollection.Query.DataHistorico == this.dataHistoricoDeletarCalculoProvisao[i]);
                calculoProvisaoHistoricoDeletarCollection.Query.Load();
                calculoProvisaoHistoricoDeletarCollection.MarkAllAsDeleted();
                calculoProvisaoHistoricoDeletarCollection.Save();
            }
            #endregion

            // Salva Calculo Provisão presente no Excel
            calculoProvisaoCollection.Save();

            // Salva Calculo Provisão Historico presente no Excel
            calculoProvisaoHistoricoCollection.Save();
            //
            scope.Complete();
        }
    }

    /// <summary>
    /// Processa a Planilha de Cálculo Provisão após ter acabado o Upload
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void uplCalculoProvisao_FileUploadComplete(object sender, FileUploadCompleteEventArgs e) {
        e.CallbackData = "";

        #region Trata Extensão Válida
        if (!ValoresExcelUtil.isExtensaoExcel(e.UploadedFile.FileName.Trim())) {
            e.CallbackData = "Importação Cálculo Provisão - Formato de Arquivo Inválido. Formato permitido: .xls ou .xlsx\n\n";
            return;
        }
        #endregion

        // Stream de bytes com o Conteudo do Arquivo Excel
        Stream sr = e.UploadedFile.FileContent;

        #region Processamento Arquivo Excel

        //Reseta vetor de dados
        this.valoresExcelCalculoProvisao = new List<ValoresExcelCalculoProvisao>();
        this.idTabelaDeletarCalculoProvisao = new List<int>();
        this.dataHistoricoDeletarCalculoProvisao = new List<DateTime>();

        try {
            // Ler Arquivo
            this.LerArquivoCalculoProvisao(sr);
            // Carrega Arquivo
            this.CarregaCalculoProvisao();
        }
        catch (Exception e2) {
            e.CallbackData = "Importação Cálculo Provisão - " + e2.Message;
            return;
        }

        new HistoricoLog().InsereHistoricoLog(DateTime.Now, DateTime.Now,
            "Importação Planilha Cálculo Provisão",
            HttpContext.Current.User.Identity.Name, UtilitarioWeb.GetIP(Request), "", HistoricoLogOrigem.Outros);

        #endregion
    }
}