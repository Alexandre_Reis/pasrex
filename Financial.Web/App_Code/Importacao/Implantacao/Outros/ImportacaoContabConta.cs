﻿using System;
using System.Collections.Generic;
using System.IO;

using Financial.Integracao.Excel;
using Financial.Integracao.Excel.Util;
using Financial.Web.Common;

using EntitySpaces.Interfaces;

using DevExpress.Web;
using Financial.CRM;
using Financial.InvestidorCotista;
using Financial.RendaFixa;
using Financial.Investidor;
using Financial.Bolsa;
using Financial.BMF;
using Financial.Fundo;
using Financial.Common;
using Financial.InvestidorCotista.Enums;
using Financial.CRM.Enums;
using Financial.Investidor.Enums;
using Financial.Common.Enums;
using Financial.Bolsa.Enums;
using Financial.BMF.Enums;
using Financial.Fundo.Enums;
using Financial.Security;
using System.Web;
using Financial.Web.Util;
using Financial.Security.Enums;
using Financial.Util;
using Financial.Contabil;
using Financial.Contabil.Enums;
using EntitySpaces.Core;
using System.Text;


public partial class ImportacaoBasePage : BasePage
{

    /* Estrutura do Excel */
    private List<ValoresExcelContabConta> valoresExcelContabConta = new List<ValoresExcelContabConta>();
    private ContabContaCollection contabContaCollection = new ContabContaCollection();
    private Dictionary<string, int> dicCodigoIdCodigo = new Dictionary<string, int>();

    /// <summary>
    /// 
    /// </summary>
    /// <param name="streamExcel"></param>
    private void LerArquivoContabConta(Stream streamExcel)
    {
        #region Leitura do Arquivo Excel
        // Open Spreadsheet
        Bytescout.Spreadsheet.Spreadsheet document = new Bytescout.Spreadsheet.Spreadsheet();
        document.LoadFromStream(streamExcel);

        Bytescout.Spreadsheet.Worksheet workSheet = document.Workbook.Worksheets[0];
        //
        #region Confere Formato Arquivo
        string[] colunasConferencias = new string[] { "IdPlano", "IdConta", "Codigo", "Descricao", "CodigoReduzida" };

        bool formato = ValoresExcelUtil.isExcelFormatoValido(colunasConferencias, workSheet);
        if (!formato)
        {
            string mensagem = "Formato Interno do Arquivo Inválido.\n";
            mensagem += "Linha 1 do Arquivo deve ser: \n";
            for (int i = 0; i < colunasConferencias.Length; i++)
            {
                int coluna = i + 1;
                mensagem += "\tColuna " + coluna + ": " + colunasConferencias[i] + "\n";
            }

            document.Close();
            document.Dispose();

            throw new Exception(mensagem);
        }
        #endregion

        //
        int index = 0;
        // This row,column index should be changed as per your need.
        // i.e. which cell in the excel you are interesting to read.
        //
        /* Formato: 1)IdPlano  - 2)IdConta - 3)Codigo - 4)Descricao - 5)CodigoReduzida
         */
        int linha = 1;
        int coluna1 = 0, coluna2 = 1, coluna3 = 2, coluna4 = 3, coluna5 = 4;
        //            
        try
        {
            // Enquanto idCliente tiver valor
            while (workSheet.Cell(linha, coluna1).Value != null)
            {
                ValoresExcelContabConta item = new ValoresExcelContabConta();
                //  

                try
                {
                    item.IdPlano = workSheet.Cell(linha, coluna1).ValueAsInteger;
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message + " - IdPlano: linha " + (linha + 1));
                }

                if (workSheet.Cell(linha, coluna2).Value == null)
                    throw new Exception("IdConta não informado: linha " + (linha + 1));
                try
                {
                    item.IdConta = workSheet.Cell(linha, coluna2).ValueAsInteger;
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message + " - IdConta: linha " + (linha + 1));
                }

                if (workSheet.Cell(linha, coluna3).Value == null)
                    throw new Exception("Codigo não informado: linha " + (linha + 1));
                try
                {
                    item.Codigo = Convert.ToString(workSheet.Cell(linha, coluna3).Value);
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message + " - Codigo: linha " + (linha + 1));
                }

                if (workSheet.Cell(linha, coluna4).Value == null)
                    throw new Exception("Descricao não informada: linha " + (linha + 1));
                try
                {
                    item.Descricao = Convert.ToString(workSheet.Cell(linha, coluna4).Value);
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message + " - Descricao: linha " + (linha + 1));
                }

                if (workSheet.Cell(linha, coluna5).Value == null)
                    throw new Exception("CodigoReduzida não informado: linha " + (linha + 1));
                try
                {
                    item.CodigoReduzida = Convert.ToString(workSheet.Cell(linha, coluna5).Value);
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message + " - CodigoReduzida: linha " + (linha + 1));
                }

                //
                this.valoresExcelContabConta.Add(item);
                //
                index++;
                linha = 1 + index;
                //                
            }
        }

        catch (Exception ex)
        {
            document.Close();
            document.Dispose();

            throw new Exception(ex.Message);
        }
        #endregion

        document.Close();
        document.Dispose();
    }

    private string InsertRegistro(ValoresExcelContabConta valoresExcel)
    {
        ContabConta contabConta = new ContabConta();
        contabConta.IdConta = valoresExcel.IdConta;
        contabConta.IdPlano = valoresExcel.IdPlano;
        contabConta.Codigo = valoresExcel.Codigo;
        contabConta.CodigoReduzida = valoresExcel.CodigoReduzida;

        ContabPlano contabPlano = new ContabPlano();
        if (!contabPlano.LoadByPrimaryKey(contabConta.IdPlano.Value))
        {
            throw new Exception("Não foi possível encontrar o Plano com o identificador: \"" + contabConta.IdPlano + "\". Importação cancelada.");
        }

        if (contabConta.LoadByPrimaryKey(contabConta.IdConta.Value))
        {
            if (contabConta.IdPlano.Value == valoresExcel.IdPlano)
            {
                throw new Exception("Já existe uma conta cadastrada com o identificador: \"" + contabConta.IdConta + "\". Importação cancelada.");
            }
        }

        // validacao digito
        if (contabPlano.ValidaDigitoConta == "S")
        {
            string conta = valoresExcel.Codigo.Replace(" ", "").Replace(".", "").PadRight(10, '0');
            string digito = contabConta.CalculaDigitoContaCOFI(conta).ToString();
            contabConta.Digito = digito;
            conta = String.Format(@"{0:0\.0\.0\.00\.00\.000}", Convert.ToInt64(conta));
            valoresExcel.Codigo = conta;
            contabConta.Codigo = conta;
        }

        string[] splittedCodigo = valoresExcel.Codigo.Split('.');
        if (splittedCodigo[0] == "1" || splittedCodigo[0] == "2" || splittedCodigo[0] == "3")
        {
            contabConta.TipoConta = TipoContaContabil.Ativo;
        }
        else
        {
            contabConta.TipoConta = TipoContaContabil.Passivo;
        }

        if (splittedCodigo[0] == "1" || splittedCodigo[0] == "2" || splittedCodigo[0] == "4")
        {
            contabConta.FuncaoConta = (int)FuncaoContaContabil.Operacional;
        }
        else if (splittedCodigo[0] == "3" || splittedCodigo[0] == "9")
        {
            contabConta.FuncaoConta = (int)FuncaoContaContabil.Compensado;
        }
        else if (splittedCodigo[0] == "7")
        {
            contabConta.FuncaoConta = (int)FuncaoContaContabil.ResultadoCredito;
        }
        else if (splittedCodigo[0] == "8")
        {
            contabConta.FuncaoConta = (int)FuncaoContaContabil.ResultadoDebito;
        }
        else if (valoresExcel.Codigo.Length >= 5 && valoresExcel.Codigo.Substring(0, 5) == "6.1.8")
        {
            contabConta.FuncaoConta = (int)FuncaoContaContabil.ResultadoAcumulado;
        }
        else
        {
            contabConta.FuncaoConta = (int)FuncaoContaContabil.Patrimonial;
        }

        contabConta.Descricao = valoresExcel.Descricao;

        //Codigo Mae
        bool temMae = valoresExcel.Codigo.Contains(".");
        if (temMae)
        {
            int posicaoUltimoSeparador = valoresExcel.Codigo.LastIndexOf('.');
            string codigoMae = valoresExcel.Codigo.Substring(0, posicaoUltimoSeparador);

            try
            {
                int outCodigo;
                if (dicCodigoIdCodigo.TryGetValue(codigoMae, out outCodigo))
                    contabConta.IdContaMae = outCodigo;
            }
            catch (Exception e)
            {
                throw new Exception(string.Format("Conta duplicada. Codigo: {0}, Id: {1}", valoresExcel.Codigo, contabConta.IdConta.Value));
            }
        }

        try
        {
            int outCodigo;
            if (!dicCodigoIdCodigo.TryGetValue(valoresExcel.Codigo, out outCodigo))
                dicCodigoIdCodigo.Add(valoresExcel.Codigo, contabConta.IdConta.Value);
        }
        catch (Exception e)
        {
            throw e;
        }


        string idContaMae = contabConta.IdContaMae.HasValue ? contabConta.IdContaMae.ToString() : "NULL";
        string sql = String.Format("INSERT INTO ContabConta (IdConta, IdPlano, TipoConta, Descricao, IdContaMae, Codigo, CodigoReduzida, FuncaoConta, Digito) VALUES ({0},{1},'{2}','{3}',{4},'{5}','{6}','{7}','{8}')",
            contabConta.IdConta.Value, contabConta.IdPlano.Value, contabConta.TipoConta, contabConta.Descricao.Replace("'", ""), idContaMae, contabConta.Codigo, contabConta.CodigoReduzida, contabConta.FuncaoConta, contabConta.Digito);
        return sql;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name=""></param>
    private void CarregaContabConta()
    {
        ContabContaCollection contabContaCollection = new ContabContaCollection();

        const string TABLE_NAME = "ContabConta";

        int idContaTotalAtivo = 0;
        int idContaTotalPassivo = 0;
        int idConta3 = 0;
        int idConta9 = 0;

        if (esConfigSettings.DefaultConnection.Provider == "EntitySpaces.SqlClientProvider")
        {
            using (esTransactionScope scope = new esTransactionScope())
            {
                esUtility u = new esUtility();
                StringBuilder stringBuilder = new StringBuilder();
                string sql = String.Format(" set identity_insert [{0}] On ", TABLE_NAME);
                stringBuilder.AppendLine(sql);

                for (int i = 0; i < this.valoresExcelContabConta.Count; i++)
                {
                    ValoresExcelContabConta valoresExcel = this.valoresExcelContabConta[i];
                    stringBuilder.AppendLine(this.InsertRegistro(valoresExcel));

                    if (valoresExcel.Codigo == "3")
                    {
                        idConta3 = valoresExcel.IdConta;
                    }
                    if (valoresExcel.Codigo == "9")
                    {
                        idConta9 = valoresExcel.IdConta;
                    }
                    if (valoresExcel.Codigo == "3.9.9.99.99")
                    {
                        idContaTotalAtivo = valoresExcel.IdConta;
                    }
                    if (valoresExcel.Codigo == "9.9.9.99.99")
                    {
                        idContaTotalPassivo = valoresExcel.IdConta;
                    }
                }

                u = new esUtility();
                sql = String.Format(" set identity_insert [{0}] Off ", TABLE_NAME);
                stringBuilder.AppendLine(sql);
                u.ExecuteNonQuery(esQueryType.Text, stringBuilder.ToString(), "");

                scope.Complete();
            }
            /*
            Jira-2200
            ao encontrar uma conta de um digito a importação de plano contabil deve definir a mae da seguinte forma
            3.9.9.99.99 para as contas 1,2,3
            9.9.9.99.99 para as restantes
            Deve tambem definir mae para as contas 3.0.3, 3.0.4, 3.0.6, 3.0.9 como a conta 3            
            e para as contas 9.0.3, 9.0.4, 9.0.6, 9.0.9 como conta 9 (hoje está em branco).
            */

            int idPlano = this.valoresExcelContabConta[0].IdPlano;

            ContabContaCollection contabContaMae = new ContabContaCollection();
            contabContaMae.Query.Select(contabContaMae.Query.IdConta,
                                        contabContaMae.Query.IdPlano,
                                        contabContaMae.Query.TipoConta,
                                        contabContaMae.Query.Descricao,
                                        contabContaMae.Query.IdContaMae,
                                        contabContaMae.Query.Codigo,
                                        contabContaMae.Query.CodigoReduzida,
                                        contabContaMae.Query.FuncaoConta);
            contabContaMae.Query.Where(contabContaMae.Query.IdPlano.Equal(idPlano));

            contabContaMae.Query.Load();

            foreach (ContabConta contabIdConta in contabContaMae)
            {
                if (contabIdConta.Codigo == "1" || contabIdConta.Codigo == "2" || contabIdConta.Codigo == "3")
                {
                    contabIdConta.IdContaMae = idContaTotalAtivo;
                    contabContaCollection.AttachEntity(contabIdConta);
                }
                if (contabIdConta.Codigo == "4" || contabIdConta.Codigo == "5" || contabIdConta.Codigo == "6" || contabIdConta.Codigo == "7" || contabIdConta.Codigo == "8" || contabIdConta.Codigo == "9")
                {
                    contabIdConta.IdContaMae = idContaTotalPassivo;
                    contabContaCollection.AttachEntity(contabIdConta);
                }

                if (contabIdConta.Codigo == "3.0.3" || contabIdConta.Codigo == "3.0.4" || contabIdConta.Codigo == "3.0.6" || contabIdConta.Codigo == "3.0.9")
                {
                    contabIdConta.IdContaMae = idConta3;
                    contabContaCollection.AttachEntity(contabIdConta);
                }
                if (contabIdConta.Codigo == "9.0.3" || contabIdConta.Codigo == "9.0.4" || contabIdConta.Codigo == "9.0.6" || contabIdConta.Codigo == "9.0.9")
                {
                    contabIdConta.IdContaMae = idConta9;
                    contabContaCollection.AttachEntity(contabIdConta);
                }
            }
            contabContaCollection.Save();
        }


    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void uplContabConta_FileUploadComplete(object sender, FileUploadCompleteEventArgs e)
    {
        e.CallbackData = "";

        #region Trata Extensão Válida
        if (!ValoresExcelUtil.isExtensaoExcel(e.UploadedFile.FileName.ToLower().Trim()))
        {
            e.CallbackData = "Importação Plano Contábil - Formato de Arquivo Inválido. Formato permitido: .xls ou .xlsx\n\n";
            return;
        }
        #endregion

        // Stream de bytes com o Conteudo do Arquivo Excel
        Stream sr = e.UploadedFile.FileContent;

        #region Processamento Arquivo Excel

        try
        {
            // Ler Arquivo
            this.LerArquivoContabConta(sr);
            // Carrega Arquivo
            this.CarregaContabConta();
        }
        catch (Exception e2)
        {
            e.CallbackData = "Importação Plano Contábil - " + e2.Message;
            return;
        }

        new HistoricoLog().InsereHistoricoLog(DateTime.Now, DateTime.Now,
            "Importação Planilha Plano Contábil",
            HttpContext.Current.User.Identity.Name, UtilitarioWeb.GetIP(Request), "", HistoricoLogOrigem.Outros);

        #endregion
    }
}
