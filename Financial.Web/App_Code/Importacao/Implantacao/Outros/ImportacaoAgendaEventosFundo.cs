﻿using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Financial.Web.Common;
using System.IO;
using Financial.Integracao.Excel.Util;
using Financial.Integracao.Excel;
using EntitySpaces.Interfaces;
using Financial.Common;
using Financial.Bolsa;
using Financial.Bolsa.Enums;
using Financial.Investidor;
using System.Collections.Generic;
using Financial.Investidor.Enums;
using DevExpress.Web;
using Financial.Common.Exceptions;
using Financial.Bolsa.Exceptions;
using Financial.Fundo;
using Financial.Fundo.Enums;
using Financial.Security;
using Financial.Web.Util;
using Financial.Security.Enums;
using Financial.Util;
using Financial.Common.Enums;

public partial class ImportacaoBasePage : BasePage {

    /* Estrutura do Excel */
    private List<ValoresExcelAgendaEventosFundo> valoresExcelAgendaEventosFundo = new List<ValoresExcelAgendaEventosFundo>();
    
    /// <summary>
    /// Leitura de um arquivo Excel
    /// Saida: List<ValoresExcel> com os valores armazenados no arquivo
    /// </summary>
    /// <param name="streamExcel">Stream de bytes de um arquivo Excel</param>
    /// <exception cref="Exception">Se ocorreu problemas</exception>
    private void LerArquivoAgendaEventosFundos(Stream streamExcel) {

        #region Leitura do Arquivo Excel

        // Open Spreadsheet
        Bytescout.Spreadsheet.Spreadsheet document = new Bytescout.Spreadsheet.Spreadsheet();
        document.LoadFromStream(streamExcel);

        Bytescout.Spreadsheet.Worksheet workSheet = document.Workbook.Worksheets[0];

        #region Confere Formato Arquivo
        string[] colunasConferencias = new string[] {"IdCarteira","TipoEvento","DataEvento","Taxa","Valor"};

        bool formato = ValoresExcelUtil.isExcelFormatoValido(colunasConferencias, workSheet);
        if (!formato) {
            string mensagem = "Formato Interno do Arquivo Inválido.\n";
            mensagem += "Linha 1 do Arquivo deve ser: \n";
            for (int i = 0; i < colunasConferencias.Length; i++) {
                int coluna = i + 1;
                mensagem += "\tColuna " + coluna + ": " + colunasConferencias[i] + "\n";
            }

            document.Close();
            document.Dispose();

            throw new Exception(mensagem);
        }
        #endregion
        //
        int index = 0;
        // This row,column index should be changed as per your need.
        // i.e. which cell in the excel you are interesting to read.
        //
        /* Formato: 1)IdCarteira     - 2)TipoEvento         - 3)DataEvento
         *          4)Taxa           - 5)Valor 
         */
        int linha = 1;
        int coluna1 = 0, coluna2 = 1, coluna3 = 2,
            coluna4 = 3, coluna5 = 4;
        //
        try {
            // Enquanto tiver registro
            while (workSheet.Cell(linha, coluna1).Value != null) {
                ValoresExcelAgendaEventosFundo item = new ValoresExcelAgendaEventosFundo();

                try {
                    item.IdCarteira = workSheet.Cell(linha, coluna1).ValueAsInteger;
                }
                catch (Exception ex) {
                    throw new Exception(ex.Message + " - IdCarteira: linha " + (linha + 1));
                }

                if (workSheet.Cell(linha, coluna2).Value == null)
                    throw new Exception("Tipo não informado: linha " + (linha + 1));
                try
                {
                    item.Tipo = (TipoEventoFundo)workSheet.Cell(linha, coluna2).ValueAsInteger;
                }
                catch (Exception ex) {
                    throw new Exception(ex.Message + " - Tipo: linha " + (linha + 1));
                }

                if (workSheet.Cell(linha, coluna3).Value == null)
                    throw new Exception("DataEvento não informada: linha " + (linha + 1));
                try
                {
                    item.DataEvento = workSheet.Cell(linha, coluna3).ValueAsDateTime;
                }
                catch (Exception ex) {
                    throw new Exception(ex.Message + " - DataEvento: linha " + (linha + 1));
                }

                if (workSheet.Cell(linha, coluna4).Value == null)
                    throw new Exception("Taxa não informada: linha " + (linha + 1));
                try
                {
                    item.Taxa = Convert.ToDecimal(workSheet.Cell(linha, coluna4).Value);
                }
                catch (Exception ex) {
                    throw new Exception(ex.Message + " - Taxa: linha " + (linha + 1));
                }

                if (workSheet.Cell(linha, coluna5).Value == null)
                    throw new Exception("Valor não informado: linha " + (linha + 1));
                try
                {
                    item.Valor = Convert.ToDecimal(workSheet.Cell(linha, coluna5).Value);
                }
                catch (Exception ex) {
                    throw new Exception(ex.Message + " - Valor: linha " + (linha + 1));
                }

                this.valoresExcelAgendaEventosFundo.Add(item);
                
                index++;
                linha = 1 + index;
                
                Console.WriteLine("{0},{1},{2},{3},{4}", item.IdCarteira, item.Tipo, item.DataEvento.ToString("dd/MM/yyyy"), item.Taxa, item.Valor);
            }
        }
        catch (Exception ex) {
            document.Close();
            document.Dispose();

            throw new Exception(ex.Message);
        }
        #endregion

        document.Close();
        document.Dispose();
    }

    /// <summary>
    /// Gera e carrega a Agenda de Eventos
    /// com o objeto List<ValoresExcel>
    /// </summary>
    /// <exception cref="Exception">Se ocorreu problemas</exception>
    private void CarregaAgendaEventosFundo() {
        AgendaFundoCollection agendaFundoCollection = new AgendaFundoCollection();
        AgendaFundoCollection agendaFundoCollectionDeletar = new AgendaFundoCollection();
        //
        for (int i = 0; i < this.valoresExcelAgendaEventosFundo.Count; i++) {
            ValoresExcelAgendaEventosFundo valoresExcel = this.valoresExcelAgendaEventosFundo[i];

            #region Verifica se Existe idCarteira
            Carteira carteira = new Carteira();
            if (!carteira.LoadByPrimaryKey(valoresExcel.IdCarteira)) {
                throw new Exception("Carteira não existente : " + valoresExcel.IdCarteira);
            }
            #endregion

            #region TipoEvento Inconsistente
            if ((int)valoresExcel.Tipo != (int)TipoEventoFundo.Juros && 
                (int)valoresExcel.Tipo != (int)TipoEventoFundo.Amortizacao && 
                (int)valoresExcel.Tipo != (int)TipoEventoFundo.AmortizacaoJuros && 
                (int)valoresExcel.Tipo != (int)TipoEventoFundo.ComeCotas)
            {
                throw new Exception("Tipo Evento Inconsistente. Valores Possíveis: 1, 2, 3, 20");
            }                       
            #endregion

            if ((int)valoresExcel.Tipo != (int)TipoEventoFundo.AmortizacaoJuros && valoresExcel.Valor != 0)
                throw new Exception("Só é permitido agendar 'Amortização + Juros' por taxa!");

            if ((valoresExcel.Valor != 0 && valoresExcel.Taxa != 0) || (valoresExcel.Valor == 0 && valoresExcel.Taxa == 0))
                throw new Exception("Favor preencher Taxa ou Juros (Um ou outro)!");

            #region Agenda Fundo
            //
            AgendaFundo agendaFundo = agendaFundoCollection.AddNew();
            agendaFundo.IdCarteira = valoresExcel.IdCarteira;
            agendaFundo.TipoEvento = Convert.ToByte(valoresExcel.Tipo);

            #region DataEvento
            if (!Calendario.IsDiaUtil(valoresExcel.DataEvento, (byte)LocalFeriadoFixo.Brasil, TipoFeriado.Brasil))
                agendaFundo.DataEvento = Calendario.AdicionaDiaUtil(valoresExcel.DataEvento, 1, (byte)LocalFeriadoFixo.Brasil, TipoFeriado.Brasil);
            else
                agendaFundo.DataEvento = valoresExcel.DataEvento;
            #endregion

            agendaFundo.Valor = 0;
            agendaFundo.Taxa = 0;
            if (valoresExcel.Valor != 0)
            {
                agendaFundo.Valor = valoresExcel.Valor;
                agendaFundo.TipoValorInput = (int)TipoValorProvento.ValorporCota;
            }
            else
            {
                agendaFundo.Taxa = valoresExcel.Taxa;
                agendaFundo.TipoValorInput = (int)TipoValorProvento.TaxaFixa;
            }
            //

            #endregion
        }

        using (esTransactionScope scope = new esTransactionScope()) {

            #region Deleta AgendaFundo por IdCarteira
            for (int i = 0; i < this.valoresExcelAgendaEventosFundo.Count; i++) {
                agendaFundoCollectionDeletar.Query.Where(agendaFundoCollectionDeletar.Query.IdCarteira.Equal(this.valoresExcelAgendaEventosFundo[i].IdCarteira));
                agendaFundoCollectionDeletar.Query.Where(agendaFundoCollectionDeletar.Query.TipoEvento.Equal(this.valoresExcelAgendaEventosFundo[i].Tipo));
                agendaFundoCollectionDeletar.Query.Where(agendaFundoCollectionDeletar.Query.DataEvento.Equal(this.valoresExcelAgendaEventosFundo[i].DataEvento));
                agendaFundoCollectionDeletar.Query.Load();
                agendaFundoCollectionDeletar.MarkAllAsDeleted();
                agendaFundoCollectionDeletar.Save();
            }
            #endregion

            // Salva Agenda RendaFixa presente no Excel
            agendaFundoCollection.Save();
            //
            scope.Complete();
        }
    }

    /// <summary>
    /// Processa a Planilha de Agenda RendaFixa após ter acabado o Upload
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void uplAgendaEventosFundo_FileUploadComplete(object sender, FileUploadCompleteEventArgs e) {
        e.CallbackData = "";

        #region Trata Extensão Válida
        if (!ValoresExcelUtil.isExtensaoExcel(e.UploadedFile.FileName.Trim())) {
            e.CallbackData = "Importação AgendaFundo - Formato de Arquivo Inválido. Formato permitido: .xls ou .xlsx\n\n";
            return;
        }
        #endregion

        // Stream de bytes com o Conteudo do Arquivo Excel
        Stream sr = e.UploadedFile.FileContent;

        #region Processamento Arquivo Excel

        try {
            // Ler Arquivo
            this.LerArquivoAgendaEventosFundos(sr);
            // Carrega Arquivo
            this.CarregaAgendaEventosFundo();
        }
        catch (Exception e2) {
            e.CallbackData = "Importação Agenda Eventos Fundo - " + e2.Message;
            return;
        }

        new HistoricoLog().InsereHistoricoLog(DateTime.Now, DateTime.Now,
            "Importação Planilha Agenda Eventos Renda Fixa",
            HttpContext.Current.User.Identity.Name, UtilitarioWeb.GetIP(Request), "", HistoricoLogOrigem.Outros);

        #endregion
    }
}