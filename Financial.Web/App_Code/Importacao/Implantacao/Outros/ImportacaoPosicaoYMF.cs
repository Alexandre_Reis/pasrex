﻿using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Web.UI.HtmlControls;
using System.Collections.Generic;
using System.Text;
using Financial.Web.Common;
using Financial.Interfaces.Import.RendaFixa;
using Financial.InvestidorCotista;
using Financial.Fundo;
using DevExpress.Web;
using System.Globalization;
using Financial.InvestidorCotista.Exceptions;
using Financial.Bolsa;
using Financial.Investidor.Exceptions;
using Financial.Investidor;

public partial class ImportacaoBasePage : BasePage {

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void uplPosicaoCotistaYMF_FileUploadComplete(object sender, FileUploadCompleteEventArgs e) {
        this.uplPosicaoYMF_FileUploadComplete(sender, e, true);
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void uplPosicaoFundoYMF_FileUploadComplete(object sender, FileUploadCompleteEventArgs e) {
        this.uplPosicaoYMF_FileUploadComplete(sender, e, false);
    }

    /// <summary>
    /// Processa arquivo txt de Posicao YMF Carregando em PosicaoCotista ou PosicaoFundo
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    /// <param name="isCotista">Indica se é Cotista ou Fundo</param>
    protected void uplPosicaoYMF_FileUploadComplete(object sender, FileUploadCompleteEventArgs e, bool isCotista) {
        e.CallbackData = "";

        #region Trata Extensão Válida

        if (!this.isExtensaoTXT(e.UploadedFile.FileName.Trim())) {
            e.CallbackData = "Arquivo inválido: Extensão do arquivo deve ser .txt. \n\n";
            return;
        }
        #endregion

        // Stream de bytes com o Conteudo do Arquivo Excel
        Stream sr = e.UploadedFile.FileContent;

        #region Processamento Arquivo .txt

        if (isCotista) {

            PosicaoCotista posicaoCotista = new PosicaoCotista();
            //
            try {
                //posicaoCotista.CarregaPosicaoCotistaYMF(sr);
            }
            catch (Exception e1) {
                string msg = "\tPosição Cotista YMF com problemas \n";
                msg += " - Dados não Importados: " + e1.Message;

                e.CallbackData = msg;
                return;
            }
        }
        else { // Fundo
            PosicaoFundo posicaoFundo = new PosicaoFundo();
            //
            try {
               // posicaoFundo.CarregaPosicaoFundoYMF(sr);
            }
            catch (Exception e1) {
                string msg = "\tPosição Fundo YMF com problemas \n";
                msg += " - Dados não Importados: " + e1.Message;

                e.CallbackData = msg;
                return;
            }
        }
        #endregion
    }    
}