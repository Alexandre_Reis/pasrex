﻿using System;
using System.Collections.Generic;
using System.IO;

using Financial.Integracao.Excel;
using Financial.Integracao.Excel.Util;
using Financial.Web.Common;
using Financial.Bolsa;

using EntitySpaces.Interfaces;

using DevExpress.Web;
using Financial.Common.Enums;
using Financial.Fundo;
using Financial.Bolsa.Enums;
using Financial.Security;
using System.Web;
using Financial.Web.Util;
using Financial.Security.Enums;

public partial class ImportacaoBasePage : BasePage {

    #region Estrutura TabelaPerfil

    /// <summary>
    /// Armazena os Valores de TabelaPerfil presentes num arquivo Excel
    /// </summary>
    internal class ValoresExcelTabelaPerfil {
        private int? idCarteira;
        public int? IdCarteira {
            get { return idCarteira; }
            set { idCarteira = value; }
        }

        private DateTime? dataInicio;
        public DateTime? DataInicio {
            get { return dataInicio; }
            set { dataInicio = value; }
        }

        private string resumoteorvotos = ""; // no null
        public string Resumoteorvotos {
            get { return resumoteorvotos; }
            set { resumoteorvotos = value; }
        }
        private string justificativavotos = ""; // no null
        public string Justificativavotos {
            get { return justificativavotos; }
            set { justificativavotos = value; }
        }
        private string deliberacoesassembleia = ""; // no null
        public string Deliberacoesassembleia {
            get { return deliberacoesassembleia; }
            set { deliberacoesassembleia = value; }
        }
        private string fatorrisco1 = "";
        public string Fatorrisco1 {
            get { return fatorrisco1; }
            set { fatorrisco1 = value; }
        }
        private string fatorrisco2 = "";
        public string Fatorrisco2 {
            get { return fatorrisco2; }
            set { fatorrisco2 = value; }
        }
        private string fatorrisco3 = "";
        public string Fatorrisco3 {
            get { return fatorrisco3; }
            set { fatorrisco3 = value; }
        }
        private string fatorrisco4 = "";
        public string Fatorrisco4 {
            get { return fatorrisco4; }
            set { fatorrisco4 = value; }
        }
        private string fatorrisco5 = "";
        public string Fatorrisco5 {
            get { return fatorrisco5; }
            set { fatorrisco5 = value; }
        }
        private string cenario1 = "";
        public string Cenario1 {
            get { return cenario1; }
            set { cenario1 = value; }
        }
        private string cenario2 = "";
        public string Cenario2 {
            get { return cenario2; }
            set { cenario2 = value; }
        }
        private string cenario3 = "";
        public string Cenario3 {
            get { return cenario3; }
            set { cenario3 = value; }
        }
        private string cenario4 = "";
        public string Cenario4 {
            get { return cenario4; }
            set { cenario4 = value; }
        }
        private string cenario5 = "";
        public string Cenario5 {
            get { return cenario5; }
            set { cenario5 = value; }
        }
        private string principalfatorrisco = "";
        public string Principalfatorrisco {
            get { return principalfatorrisco; }
            set { principalfatorrisco = value; }
        }
        private string cnpjcomitente1 = null;
        public string Cnpjcomitente1 {
            get { return cnpjcomitente1; }
            set { cnpjcomitente1 = value; }
        }
        private string parterelacionada1 = null;
        public string Parterelacionada1 {
            get { return parterelacionada1; }
            set { parterelacionada1 = value; }
        }
        private string cnpjcomitente2 = null;
        public string Cnpjcomitente2 {
            get { return cnpjcomitente2; }
            set { cnpjcomitente2 = value; }
        }
        private string parterelacionada2 = null;
        public string Parterelacionada2 {
            get { return parterelacionada2; }
            set { parterelacionada2 = value; }
        }
        private string cnpjcomitente3 = null;
        public string Cnpjcomitente3 {
            get { return cnpjcomitente3; }
            set { cnpjcomitente3 = value; }
        }
        private string parterelacionada3 = null;
        public string Parterelacionada3 {
            get { return parterelacionada3; }
            set { parterelacionada3 = value; }
        }
        private string vedadataxaperformance = null;
        public string Vedadataxaperformance {
            get { return vedadataxaperformance; }
            set { vedadataxaperformance = value; }
        }

        private decimal? var = null;
        public decimal? Var {
            get { return var; }
            set { var = value; }
        }

        private decimal? variacaocota = null;
        public decimal? Variacaocota {
            get { return variacaocota; }
            set { variacaocota = value; }
        }

        private decimal? variacaodiariopiorcenario = null;
        public decimal? Variacaodiariopiorcenario {
            get { return variacaodiariopiorcenario; }
            set { variacaodiariopiorcenario = value; }
        }

        private decimal? variacaofatorpre = null;
        public decimal? Variacaofatorpre {
            get { return variacaofatorpre; }
            set { variacaofatorpre = value; }
        }

        private decimal? variacaofatorcambial = null;
        public decimal? Variacaofatorcambial {
            get { return variacaofatorcambial; }
            set { variacaofatorcambial = value; }
        }

        private decimal? variacaofatoribovespa = null;
        public decimal? Variacaofatoribovespa {
            get { return variacaofatoribovespa; }
            set { variacaofatoribovespa = value; }
        }

        private decimal? variacaoprincipalfatorrisco = null;
        public decimal? Variacaoprincipalfatorrisco {
            get { return variacaoprincipalfatorrisco; }
            set { variacaoprincipalfatorrisco = value; }
        }

        private decimal? valortotal1 = null;
        public decimal? Valortotal1 {
            get { return valortotal1; }
            set { valortotal1 = value; }
        }

        private decimal? valortotal2 = null;
        public decimal? Valortotal2 {
            get { return valortotal2; }
            set { valortotal2 = value; }
        }

        private decimal? valortotal3 = null;
        public decimal? Valortotal3 {
            get { return valortotal3; }
            set { valortotal3 = value; }
        }

        private decimal? valorcotaperformance = null;
        public decimal? Valorcotaperformance {
            get { return valorcotaperformance; }
            set { valorcotaperformance = value; }
        }

        private int? tipoVar = null;
        public int? TipoVar {
            get { return tipoVar; }
            set { tipoVar = value; }
        }

        private DateTime? dataUltimaCobranca = null;
        public DateTime? DataUltimaCobranca {
            get { return dataUltimaCobranca; }
            set { dataUltimaCobranca = value; }
        }
    }
    #endregion

    /* Estrutura do Excel */
    private List<ValoresExcelTabelaPerfil> valoresExcelTabelaPerfil = new List<ValoresExcelTabelaPerfil>();

    /// <summary>
    /// Leitura de um arquivo Excel
    /// Saida: List<ValoresExcel> com os valores armazenados no arquivo
    /// </summary>
    /// <param name="streamExcel">Stream de bytes de um arquivo Excel</param>
    /// <exception cref="Exception">Se ocorreu problemas</exception>
    private void LerArquivoTabelaPerfil(Stream streamExcel) {

        #region Leitura do Arquivo Excel

        // Open Spreadsheet
        Bytescout.Spreadsheet.Spreadsheet document = new Bytescout.Spreadsheet.Spreadsheet();
        document.LoadFromStream(streamExcel);

        Bytescout.Spreadsheet.Worksheet workSheet = document.Workbook.Worksheets[0];

        //
        #region Confere Formato Arquivo

        string[] colunasConferencias = new string[] { 
            "IdCarteira",
            "DataReferencia",	
            "ResumoTeorVotos",	
            "JustificativaVotos",	
            "VAR",	
            "TipoVar",
            "DeliberacoesAssembleia",	
            "VariacaoCota",	
            "FatorRisco1",	
            "FatorRisco2",	
            "FatorRisco3",	
            "FatorRisco4",	
            "FatorRisco5",	
            "Cenario1",	
            "Cenario2",	
            "Cenario3",	
            "Cenario4",	
            "Cenario5",	
            "VariacaoDiarioPiorCenario",	
            "VariacaoFatorPre",	
            "VariacaoFatorCambial",	
            "VariacaoFatorIbovespa",	
            "VariacaoPrincipalFatorRisco",	
            "PrincipalFatorRisco",	
            "CNPJComitente1",	
            "ParteRelacionada1",	
            "ValorTotal1",	
            "CNPJComitente2",	
            "ParteRelacionada2",	
            "ValorTotal2",	
            "CNPJComitente3",	
            "ParteRelacionada3",	
            "ValorTotal3",	
            "VedadaTaxaPerformance",	
            "DataUltimaCobranca",	
            "ValorCotaPerformance"
        };

        bool formato = ValoresExcelUtil.isExcelFormatoValido(colunasConferencias, workSheet);

        if (!formato) {
            string mensagem = "Formato Interno do Arquivo Inválido.\n";
            mensagem += "Linha 1 do Arquivo deve ser: \n";
            for (int i = 0; i < colunasConferencias.Length; i++) {
                int coluna = i + 1;
                mensagem += "\tColuna " + coluna + ": " + colunasConferencias[i] + "\n";
            }

            document.Close();
            document.Dispose();

            throw new Exception(mensagem);
        }
        #endregion

        //
        int index = 0;
        // This row,column index should be changed as per your need.
        // i.e. which cell in the excel you are interesting to read.
        //
        /* Formato: 1)  idCarteira                  - 2)  DataInicio             
         *          
         *          
         *          
         *          
         */

        int linha = 1;
        int coluna1 = 0, coluna2 = 1, coluna3 = 2;
        int coluna4 = 3, coluna5 = 4, coluna6 = 5;
        int coluna7 = 6, coluna8 = 7, coluna9 = 8;
        int coluna10 = 9, coluna11 = 10, coluna12 = 11;
        int coluna13 = 12, coluna14 = 13, coluna15 = 14;
        int coluna16 = 15, coluna17 = 16, coluna18 = 17;
        int coluna19 = 18, coluna20 = 19, coluna21 = 20;
        int coluna22 = 21, coluna23 = 22, coluna24 = 23;
        int coluna25 = 24, coluna26 = 25, coluna27 = 26;
        int coluna28 = 27, coluna29 = 28, coluna30 = 29;
        int coluna31 = 30, coluna32 = 31, coluna33 = 32;
        int coluna34 = 33, coluna35 = 34, coluna36 = 35;

        //            
        try {
            // Enquanto Data tiver valor
            while (workSheet.Cell(linha, coluna1).Value != null) {
                ValoresExcelTabelaPerfil item = new ValoresExcelTabelaPerfil();

                if (workSheet.Cell(linha, coluna1).Value != null) {
                    item.IdCarteira = workSheet.Cell(linha, coluna1).ValueAsInteger;
                }
                if (workSheet.Cell(linha, coluna2).Value != null) {
                    item.DataInicio = workSheet.Cell(linha, coluna2).ValueAsDateTime;
                }
                if (workSheet.Cell(linha, coluna3).Value != null) {
                    item.Resumoteorvotos = workSheet.Cell(linha, coluna3).ValueAsString.Trim();
                }
                if (workSheet.Cell(linha, coluna4).Value != null) {
                    item.Justificativavotos = workSheet.Cell(linha, coluna4).ValueAsString.Trim();
                }
                if (workSheet.Cell(linha, coluna5).Value != null) {
                    item.Var = Convert.ToDecimal(workSheet.Cell(linha, coluna5).Value);
                }
                if (workSheet.Cell(linha, coluna6).Value != null) {
                    item.TipoVar = workSheet.Cell(linha, coluna6).ValueAsInteger;
                }
                if (workSheet.Cell(linha, coluna7).Value != null) {
                    item.Deliberacoesassembleia = workSheet.Cell(linha, coluna7).ValueAsString.Trim();
                }
                if (workSheet.Cell(linha, coluna8).Value != null) {
                    item.Variacaocota = Convert.ToDecimal(workSheet.Cell(linha, coluna8).Value);
                }
                if (workSheet.Cell(linha, coluna9).Value != null) {
                    item.Fatorrisco1 = workSheet.Cell(linha, coluna9).ValueAsString.Trim();
                }
                if (workSheet.Cell(linha, coluna10).Value != null) {
                    item.Fatorrisco2 = workSheet.Cell(linha, coluna10).ValueAsString.Trim();
                }
                if (workSheet.Cell(linha, coluna11).Value != null) {
                    item.Fatorrisco3 = workSheet.Cell(linha, coluna11).ValueAsString.Trim();
                }
                if (workSheet.Cell(linha, coluna12).Value != null) {
                    item.Fatorrisco4 = workSheet.Cell(linha, coluna12).ValueAsString.Trim();
                }
                if (workSheet.Cell(linha, coluna13).Value != null) {
                    item.Fatorrisco5 = workSheet.Cell(linha, coluna13).ValueAsString.Trim();
                }
                if (workSheet.Cell(linha, coluna14).Value != null) {
                    item.Cenario1 = workSheet.Cell(linha, coluna14).ValueAsString.Trim();
                }
                if (workSheet.Cell(linha, coluna15).Value != null) {
                    item.Cenario2 = workSheet.Cell(linha, coluna15).ValueAsString.Trim();
                }
                if (workSheet.Cell(linha, coluna16).Value != null) {
                    item.Cenario3 = workSheet.Cell(linha, coluna16).ValueAsString.Trim();
                }
                if (workSheet.Cell(linha, coluna17).Value != null) {
                    item.Cenario4 = workSheet.Cell(linha, coluna17).ValueAsString.Trim();
                }
                if (workSheet.Cell(linha, coluna18).Value != null) {
                    item.Cenario5 = workSheet.Cell(linha, coluna18).ValueAsString.Trim();
                }
                if (workSheet.Cell(linha, coluna19).Value != null) {
                    item.Variacaodiariopiorcenario = Convert.ToDecimal(workSheet.Cell(linha, coluna19).Value);
                }
                if (workSheet.Cell(linha, coluna20).Value != null) {
                    item.Variacaofatorpre = Convert.ToDecimal(workSheet.Cell(linha, coluna20).Value);
                }
                if (workSheet.Cell(linha, coluna21).Value != null) {
                    item.Variacaofatorcambial = Convert.ToDecimal(workSheet.Cell(linha, coluna21).Value);
                }
                if (workSheet.Cell(linha, coluna22).Value != null) {
                    item.Variacaofatoribovespa = Convert.ToDecimal(workSheet.Cell(linha, coluna22).Value);
                }
                if (workSheet.Cell(linha, coluna23).Value != null) {
                    item.Variacaoprincipalfatorrisco = Convert.ToDecimal(workSheet.Cell(linha, coluna23).Value);
                }
                if (workSheet.Cell(linha, coluna24).Value != null) {
                    item.Principalfatorrisco = workSheet.Cell(linha, coluna24).ValueAsString.Trim();
                }
                if (workSheet.Cell(linha, coluna25).Value != null) {
                    item.Cnpjcomitente1 = workSheet.Cell(linha, coluna25).ValueAsString.Trim();
                }
                if (workSheet.Cell(linha, coluna26).Value != null) {
                    item.Parterelacionada1 = workSheet.Cell(linha, coluna26).ValueAsString.Trim();
                }
                if (workSheet.Cell(linha, coluna27).Value != null) {
                    item.Valortotal1 = Convert.ToDecimal(workSheet.Cell(linha, coluna27).Value);
                }
                if (workSheet.Cell(linha, coluna28).Value != null) {
                    item.Cnpjcomitente2 = workSheet.Cell(linha, coluna28).ValueAsString.Trim();
                }
                if (workSheet.Cell(linha, coluna29).Value != null) {
                    item.Parterelacionada2 = workSheet.Cell(linha, coluna29).ValueAsString.Trim();
                }
                if (workSheet.Cell(linha, coluna30).Value != null) {
                    item.Valortotal2 = Convert.ToDecimal(workSheet.Cell(linha, coluna30).Value);
                }
                if (workSheet.Cell(linha, coluna31).Value != null) {
                    item.Cnpjcomitente3 = workSheet.Cell(linha, coluna31).ValueAsString.Trim();
                }
                if (workSheet.Cell(linha, coluna32).Value != null) {
                    item.Parterelacionada3 = workSheet.Cell(linha, coluna32).ValueAsString.Trim();
                }
                if (workSheet.Cell(linha, coluna33).Value != null) {
                    item.Valortotal3 = Convert.ToDecimal(workSheet.Cell(linha, coluna33).Value);
                }
                if (workSheet.Cell(linha, coluna34).Value != null) {
                    item.Vedadataxaperformance = workSheet.Cell(linha, coluna34).ValueAsString.Trim();
                }
                if (workSheet.Cell(linha, coluna35).Value != null) {
                    item.DataUltimaCobranca = workSheet.Cell(linha, coluna35).ValueAsDateTime;
                }
                if (workSheet.Cell(linha, coluna36).Value != null) {
                    item.Valorcotaperformance = Convert.ToDecimal(workSheet.Cell(linha, coluna36).Value);
                }

                this.valoresExcelTabelaPerfil.Add(item);
                //
                index++;
                linha = 1 + index;
                //                
                //Console.WriteLine("{0},{1},{2},{3},{4},{5},{6},{7}", 
                //            item.CdAtivoBolsa, item.Especificao,
                //            item.Descricao, item.TipoMercado,
                //            item.PuExercicio, item.DataVencimento,
                //            item.CodigoAtivoObjeto, item.CodigoIsin);
            }
        }
        catch (Exception ex) {
            document.Close();
            document.Dispose();

            throw new Exception(ex.Message);
        }
        #endregion

        document.Close();
        document.Dispose();
    }

    /// <summary>
    /// Gera e Carrega a TabelaPerfil de acordo com o objeto List<ValoresExcel>
    /// </summary>
    private void CarregaTabelaPerfil() {
        TabelaPerfilMensalCVMCollection infoCollection = new TabelaPerfilMensalCVMCollection();        
        //    
        for (int i = 0; i < this.valoresExcelTabelaPerfil.Count; i++) {
            ValoresExcelTabelaPerfil valoresExcel = this.valoresExcelTabelaPerfil[i];
            //

            #region Confere Null
            if (!valoresExcel.IdCarteira.HasValue) {
                throw new Exception("IdCarteira não pode ser nulo.");
            }
            else if (!valoresExcel.DataInicio.HasValue) {
                throw new Exception("Data Início não pode ser nulo.");
            }
            #endregion

            #region Verifica se já existe TabelaPerfil com o mesmo IdCarteira/Data
            TabelaPerfilMensalCVM info = new TabelaPerfilMensalCVM();

            // Se não existe é feito um insert, caso contrario é feito um update
            if (!info.LoadByPrimaryKey(valoresExcel.DataInicio.Value, valoresExcel.IdCarteira.Value)) {
                info = new TabelaPerfilMensalCVM();
            }

            #region Update/Insert

            #region Verifica se Existe Carteira
            Carteira carteira = new Carteira();
            if (!carteira.LoadByPrimaryKey(valoresExcel.IdCarteira.Value)) {
                throw new Exception("Carteira Não existente : " + valoresExcel.IdCarteira.Value);
            }
            #endregion

            // Dados do Arquivo
            info.IdCarteira = valoresExcel.IdCarteira.Value;
            info.Data = valoresExcel.DataInicio.Value;
            info.Secao3 = valoresExcel.Resumoteorvotos;
            info.Secao4	= valoresExcel.Justificativavotos;
            info.Secao5 = valoresExcel.Var;
            info.Secao6 = null;
            if(valoresExcel.TipoVar.HasValue) {
                info.Secao6 = Convert.ToByte(valoresExcel.TipoVar.Value);
            }
            info.Secao7 = 0;
            info.Secao8	= valoresExcel.Deliberacoesassembleia;
            info.Secao11PercentCota = valoresExcel.Variacaocota;
            info.Secao11Fator1 = valoresExcel.Fatorrisco1;
            info.Secao11Fator2 = valoresExcel.Fatorrisco2;
            info.Secao11Fator3 = valoresExcel.Fatorrisco3;
            info.Secao11Fator4 = valoresExcel.Fatorrisco4;
            info.Secao11Fator5 = valoresExcel.Fatorrisco5;
            info.Secao11Cenario1 = valoresExcel.Cenario1;
            info.Secao11Cenario2 = valoresExcel.Cenario2;
            info.Secao11Cenario3 = valoresExcel.Cenario3;
            info.Secao11Cenario4 = valoresExcel.Cenario4;
            info.Secao11Cenario5 = valoresExcel.Cenario5;
            info.Secao12 = valoresExcel.Variacaodiariopiorcenario;
            info.Secao13 = valoresExcel.Variacaofatorpre;
            info.Secao14 = valoresExcel.Variacaofatorcambial;
            info.Secao15 = valoresExcel.Variacaofatoribovespa;
            info.Secao16Variacao = valoresExcel.Variacaoprincipalfatorrisco;
            info.Secao16Fator = valoresExcel.Principalfatorrisco;
            info.Secao18CnpjComitente1 = valoresExcel.Cnpjcomitente1;
            info.Secao18ParteRelacionada1 = valoresExcel.Parterelacionada1;
            info.Secao18ValorTotal1 = valoresExcel.Valortotal1;
            info.Secao18CnpjComitente2 = valoresExcel.Cnpjcomitente2;
            info.Secao18ParteRelacionada2 = valoresExcel.Parterelacionada2;
            info.Secao18ValorTotal2 = valoresExcel.Valortotal2;
            info.Secao18CnpjComitente3 = valoresExcel.Cnpjcomitente3;
            info.Secao18ParteRelacionada3 = valoresExcel.Parterelacionada3;
            info.Secao18ValorTotal3 = valoresExcel.Valortotal3;
            info.Secao18VedadaCobrancaTaxa = valoresExcel.Vedadataxaperformance;
            info.Secao18DataUltimaCobranca = valoresExcel.DataUltimaCobranca;
            info.Secao18ValorUltimaCota = valoresExcel.Valorcotaperformance;

            // Attach the object
            infoCollection.AttachEntity(info);

            #endregion
            #endregion           
        }

        // Salva Info Complementar presentes no Excel                
        infoCollection.Save();
    }

    /// <summary>
    /// Processa a Planilha de Info Complementar após ter acabado o Upload
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void uplTabelaPerfilMensal_FileUploadComplete(object sender, FileUploadCompleteEventArgs e) {
        e.CallbackData = "";

        #region Trata Extensão Válida
        if (!ValoresExcelUtil.isExtensaoExcel(e.UploadedFile.FileName.Trim())) {
            e.CallbackData = "Tabela Perfil - Formato de Arquivo Inválido. Formato permitido: .xls ou .xlsx\n\n";
            return;
        }
        #endregion

        // Stream de bytes com o Conteudo do Arquivo Excel
        Stream sr = e.UploadedFile.FileContent;

        #region Processamento Arquivo Excel

        // Reseta Vetor de Dados
        this.valoresExcelTabelaPerfil = new List<ValoresExcelTabelaPerfil>();

        //
        try {
            // Ler Arquivo
            this.LerArquivoTabelaPerfil(sr);
            // Carrega Arquivo
            this.CarregaTabelaPerfil();
        }
        catch (Exception e2) {
            e.CallbackData = "Importação Tabela Perfil - " + e2.Message;
            return;
        }

        new HistoricoLog().InsereHistoricoLog(DateTime.Now, DateTime.Now,
                                "Importação Planilha Tabela Perfil",
                                HttpContext.Current.User.Identity.Name, UtilitarioWeb.GetIP(Request), "", HistoricoLogOrigem.Outros);

        #endregion

    }
}