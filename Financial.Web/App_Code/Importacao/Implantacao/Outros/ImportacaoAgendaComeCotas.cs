﻿using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Financial.Web.Common;
using System.IO;
using Financial.Integracao.Excel.Util;
using Financial.Integracao.Excel;
using EntitySpaces.Interfaces;
using Financial.Common;
using Financial.Bolsa;
using Financial.Bolsa.Enums;
using Financial.Investidor;
using System.Collections.Generic;
using Financial.Investidor.Enums;
using DevExpress.Web;
using Financial.Fundo;
using Financial.Util;
using Financial.Security;
using Financial.Web.Util;
using Financial.Security.Enums;
using Financial.Util.Enums;

public partial class ImportacaoBasePage : BasePage {

    /* Estrutura do Excel */
    private List<ValoresExcelComeCotas> valoresExcelComeCotas = new List<ValoresExcelComeCotas>();

    /// <summary>
    /// Leitura de um arquivo Excel
    /// Saida: List<ValoresExcel> com os valores armazenados no arquivo
    /// </summary>
    /// <param name="streamExcel">Stream de bytes de um arquivo Excel</param>
    /// <exception cref="Exception">Se ocorreu problemas</exception>
    private void LerArquivoComeCotas(Stream streamExcel) {

        #region Leitura do Arquivo Excel

        // Open Spreadsheet
        Bytescout.Spreadsheet.Spreadsheet document = new Bytescout.Spreadsheet.Spreadsheet();
        document.LoadFromStream(streamExcel);

        Bytescout.Spreadsheet.Worksheet workSheet = document.Workbook.Worksheets[0];
        //
        #region Confere Formato Arquivo
        string[] colunasConferencias = new string[] 
                            {"IdPosicaoComeCotas","IdCarteira","TipoEvento","DataAgendamentoEvento","DataPagamentoIR_Evento","DataUltimaCobrancaIR","QuantidadeCotas",
                             "QuantidadeAntesCortes","ValorCotaAplicacao","ValorCotaUltimoPagamentoIR","ValorCotaDataAgendamento","AliquotaIR_ComeCotas",
                             "RendimentoBrutoDesdeAplicacao","RendimentoDesdeUltimoPagamentoIR","PrazoCautela","PrazoIOF","AliquotaIOF","ValorIOF",
                             "ValorIOFVirtual","PrejuizoUtilizado","RendimentoCompensado","ValorIR_Agendado","ValorIR_Pago","Residuo_15","Residuo_175","Residuo_20","Residuo_225",
                             "QuantidadeCotasComeCotas","QuantidadeCotasFechamento","CodExecucaoRecolhimentoIR","CodFormaUsoAliquotaIR","TipoPosicao"};

        bool formato = ValoresExcelUtil.isExcelFormatoValido(colunasConferencias, workSheet);

        if (!formato) {
            string mensagem = "Formato Interno do Arquivo Inválido.\n";
            mensagem += "Linha 1 do Arquivo deve ser: \n";
            for (int i = 0; i < colunasConferencias.Length; i++) {
                int coluna = i + 1;
                mensagem += "\tColuna " + coluna + ": " + colunasConferencias[i] + "\n";
            }

            document.Close();
            document.Dispose();

            throw new Exception(mensagem);
        }
        #endregion

        //
        int index = 0;
        // This row,column index should be changed as per your need.
        // i.e. which cell in the excel you are interesting to read.
        //
        /* Formato: 1)idPosicaoComeCotas - 2)idCarteira - 3)tipoEvento - 4)dataAgendamentoEvento
                    5)dataPagamentoIR_Evento - 6)dataUltimaCobrancaIR - 7)quantidadeCotas - 8)quantidadeAntesCortes
                    9)valorCotaAplicacao - 10)valorCotaUltimoPagamentoIR - 11)valorCotaDataAgendamento - 12)aliquotaIR_ComeCotas
                    13)rendimentoBrutoDesdeAplicacao - 14)rendimentoDesdeUltimoPagamentoIR - 15)prazoCautela - 16)prazoIOF - 17)aliquotaIOF
                    18)valorIOF - 19)valorIOFVirtual - 20)prejuizoUtilizado - 21)rendimentoCompensado - 22)valorIR_Agendado - 23)valorIR_Pago
                    24)residuo_15 - 25)residuo_175 - 26)residuo_20 - 27)residuo_225 - 28)quantidadeCotasComeCotas - 29)quantidadeCotasFechamento
                    30)codExecucaoRecolhimentoIR - 31)codFormaUsoAliquotaIR - 32)tipoPosicao
        */
        int linha = 1;

        int coluna1 = 0, coluna2 = 1, coluna3 = 2, coluna4 = 3, coluna5 = 4, coluna6 = 5, coluna7 = 6, coluna8 = 7, coluna9 = 8, coluna10 = 9, coluna11 = 10,
            coluna12 = 11, coluna13 = 12, coluna14 = 13, coluna15 = 14, coluna16 = 15, coluna17 = 16, coluna18 = 17, coluna19 = 18, coluna20 = 19, coluna21 = 20,
            coluna22 = 21, coluna23 = 22, coluna24 = 23, coluna25 = 24, coluna26 = 25, coluna27 = 26, coluna28 = 27, coluna29 = 28, coluna30 = 29, coluna31 = 30,
            coluna32 = 31;



        //            
        try {
            // Enquanto idPosicaoComeCotas tiver valor
            while (workSheet.Cell(linha, coluna1).Value != null) {
                ValoresExcelComeCotas item = new ValoresExcelComeCotas();

                try
                {
                    item.IdPosicaoComeCotas = workSheet.Cell(linha, coluna1).ValueAsString;
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message + " - IdPosicaoComeCotas: linha " + (linha + 1));
                }

                if (workSheet.Cell(linha, coluna2).Value == null)
                    throw new Exception("IdCarteira não informado: linha " + (linha + 1));
                try
                {
                    item.IdCarteira = workSheet.Cell(linha, coluna2).ValueAsInteger;
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message + " - IdCarteira: linha " + (linha + 1));
                }

                if (workSheet.Cell(linha, coluna3).Value == null)
                    throw new Exception("TipoEvento não informado: linha " + (linha + 1));
                try
                {
                    item.TipoEvento = Convert.ToInt16(workSheet.Cell(linha, coluna3).Value);
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message + " - TipoEvento: linha " + (linha + 1));
                }

                if (workSheet.Cell(linha, coluna4).Value == null)
                    throw new Exception("DataAgendamentoEvento não informada: linha " + (linha + 1));
                try
                {
                    item.DataAgendamentoEvento = workSheet.Cell(linha, coluna4).ValueAsDateTime;
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message + " - DataAgendamentoEvento: linha " + (linha + 1));
                }

                if (workSheet.Cell(linha, coluna5).Value == null)
                    throw new Exception("DataPagamentoIR_Evento não informada: linha " + (linha + 1));
                try
                {
                    item.DataPagamentoIR_Evento = workSheet.Cell(linha, coluna5).ValueAsDateTime;
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message + " - DataPagamentoIR_Evento: linha " + (linha + 1));
                }

                if (workSheet.Cell(linha, coluna6).Value == null)
                    throw new Exception("DataUltimaCobrancaIR não informada: linha " + (linha + 1));
                try
                {
                    item.DataUltimaCobrancaIR = workSheet.Cell(linha, coluna6).ValueAsDateTime;
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message + " - DataUltimaCobrancaIR: linha " + (linha + 1));
                }

                if (workSheet.Cell(linha, coluna7).Value == null)
                    throw new Exception("QuantidadeCotas não informada: linha " + (linha + 1));
                try
                {
                    item.QuantidadeCotas = Convert.ToDecimal(workSheet.Cell(linha, coluna7).Value);
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message + " - QuantidadeCotas: linha " + (linha + 1));
                }

                if (workSheet.Cell(linha, coluna8).Value == null)
                    throw new Exception("QuantidadeAntesCortes não informada: linha " + (linha + 1));
                try
                {
                    item.QuantidadeAntesCortes = Convert.ToDecimal(workSheet.Cell(linha, coluna8).Value);
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message + " - QuantidadeAntesCortes: linha " + (linha + 1));
                }
            

                if (workSheet.Cell(linha, coluna9).Value == null)
                    throw new Exception("ValorCotaAplicacao não informada: linha " + (linha + 1));
                try
                {
                    item.ValorCotaAplicacao = Convert.ToDecimal(workSheet.Cell(linha, coluna9).Value);
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message + " - ValorCotaAplicacao: linha " + (linha + 1));
                }


                if (workSheet.Cell(linha, coluna10).Value == null)
                    throw new Exception("ValorCotaUltimoPagamentoIR não informada: linha " + (linha + 1));
                try
                {
                    item.ValorCotaUltimoPagamentoIR = Convert.ToDecimal(workSheet.Cell(linha, coluna10).Value);
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message + " - ValorCotaUltimoPagamentoIR: linha " + (linha + 1));
                }


                if (workSheet.Cell(linha, coluna11).Value == null)
                    throw new Exception("ValorCotaDataAgendamento não informada: linha " + (linha + 1));
                try
                {
                    item.ValorCotaDataAgendamento = Convert.ToDecimal(workSheet.Cell(linha, coluna11).Value);
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message + " - ValorCotaDataAgendamento: linha " + (linha + 1));
                }

                if (workSheet.Cell(linha, coluna12).Value == null)
                    throw new Exception("AliquotaIR_ComeCotas não informada: linha " + (linha + 1));
                try
                {
                    item.AliquotaIR_ComeCotas = Convert.ToDecimal(workSheet.Cell(linha, coluna12).Value);
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message + " - AliquotaIR_ComeCotas: linha " + (linha + 1));
                }

                if (workSheet.Cell(linha, coluna13).Value == null)
                    throw new Exception("RendimentoBrutoDesdeAplicacao não informada: linha " + (linha + 1));
                try
                {
                    item.RendimentoBrutoDesdeAplicacao = Convert.ToDecimal(workSheet.Cell(linha, coluna13).Value);
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message + " - RendimentoBrutoDesdeAplicacao: linha " + (linha + 1));
                }

                if (workSheet.Cell(linha, coluna14).Value == null)
                    throw new Exception("RendimentoDesdeUltimoPagamentoIR não informada: linha " + (linha + 1));
                try
                {
                    item.RendimentoDesdeUltimoPagamentoIR = Convert.ToDecimal(workSheet.Cell(linha, coluna14).Value);
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message + " - RendimentoDesdeUltimoPagamentoIR: linha " + (linha + 1));
                }


                if (workSheet.Cell(linha, coluna15).Value == null)
                    throw new Exception("PrazoCautela não informada: linha " + (linha + 1));
                try
                {
                    item.PrazoCautela = Convert.ToInt16(workSheet.Cell(linha, coluna15).Value);
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message + " - PrazoCautela: linha " + (linha + 1));
                }


                if (workSheet.Cell(linha, coluna16).Value == null)
                    throw new Exception("PrazoIOF não informada: linha " + (linha + 1));
                try
                {
                    item.PrazoIOF = Convert.ToInt16(workSheet.Cell(linha, coluna16).Value);
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message + " - PrazoIOF: linha " + (linha + 1));
                }


                if (workSheet.Cell(linha, coluna17).Value == null)
                    throw new Exception("AliquotaIOF não informada: linha " + (linha + 1));
                try
                {
                    item.AliquotaIOF = Convert.ToDecimal(workSheet.Cell(linha, coluna17).Value);
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message + " - AliquotaIOF: linha " + (linha + 1));
                }


                if (workSheet.Cell(linha, coluna18).Value == null)
                    throw new Exception("ValorIOF não informada: linha " + (linha + 1));
                try
                {
                    item.ValorIOF = Convert.ToDecimal(workSheet.Cell(linha, coluna18).Value);
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message + " - ValorIOF: linha " + (linha + 1));
                }

                if (workSheet.Cell(linha, coluna19).Value == null)
                    throw new Exception("ValorIOFVirtual não informada: linha " + (linha + 1));
                try
                {
                    item.ValorIOFVirtual = Convert.ToDecimal(workSheet.Cell(linha, coluna19).Value);
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message + " - ValorIOFVirtual: linha " + (linha + 1));
                }
                

                if (workSheet.Cell(linha, coluna20).Value == null)
                    throw new Exception("PrejuizoUtilizado não informada: linha " + (linha + 1));
                try
                {
                    item.PrejuizoUtilizado = Convert.ToDecimal(workSheet.Cell(linha, coluna20).Value);
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message + " - PrejuizoUtilizado: linha " + (linha + 1));
                }

                if (workSheet.Cell(linha, coluna21).Value == null)
                    throw new Exception("RendimentoCompensado não informada: linha " + (linha + 1));
                try
                {
                    item.RendimentoCompensado = Convert.ToDecimal(workSheet.Cell(linha, coluna21).Value);
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message + " - RendimentoCompensado: linha " + (linha + 1));
                }

                if (workSheet.Cell(linha, coluna22).Value == null)
                    throw new Exception("ValorIR_Agendado não informada: linha " + (linha + 1));
                try
                {
                    item.ValorIR_Agendado = Convert.ToDecimal(workSheet.Cell(linha, coluna22).Value);
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message + " - ValorIR_Agendado: linha " + (linha + 1));
                }

                if (workSheet.Cell(linha, coluna23).Value == null)
                    throw new Exception("ValorIR_Pago não informada: linha " + (linha + 1));
                try
                {
                    item.ValorIR_Pago = Convert.ToDecimal(workSheet.Cell(linha, coluna23).Value);
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message + " - ValorIR_Pago: linha " + (linha + 1));
                }

                if (workSheet.Cell(linha, coluna24).Value == null)
                    throw new Exception("Residuo_15 não informada: linha " + (linha + 1));
                try
                {
                    item.Residuo_15 = Convert.ToDecimal(workSheet.Cell(linha, coluna24).Value);
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message + " - Residuo_15: linha " + (linha + 1));
                }

                if (workSheet.Cell(linha, coluna25).Value == null)
                    throw new Exception("Residuo_175 não informada: linha " + (linha + 1));
                try
                {
                    item.Residuo_175 = Convert.ToDecimal(workSheet.Cell(linha, coluna25).Value);
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message + " - Residuo_175: linha " + (linha + 1));
                }

                if (workSheet.Cell(linha, coluna26).Value == null)
                    throw new Exception("Residuo_20 não informada: linha " + (linha + 1));
                try
                {
                    item.Residuo_20 = Convert.ToDecimal(workSheet.Cell(linha, coluna26).Value);
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message + " - Residuo_20: linha " + (linha + 1));
                }

                if (workSheet.Cell(linha, coluna27).Value == null)
                    throw new Exception("Residuo_225 não informada: linha " + (linha + 1));
                try
                {
                    item.Residuo_225 = Convert.ToDecimal(workSheet.Cell(linha, coluna27).Value);
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message + " - Residuo_225: linha " + (linha + 1));
                }
                
                if (workSheet.Cell(linha, coluna28).Value == null)
                    throw new Exception("QuantidadeCotasComeCotas não informada: linha " + (linha + 1));
                try
                {
                    item.QuantidadeCotasComeCotas = Convert.ToDecimal(workSheet.Cell(linha, coluna28).Value);
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message + " - QuantidadeCotasComeCotas: linha " + (linha + 1));
                }

                if (workSheet.Cell(linha, coluna29).Value == null)
                    throw new Exception("QuantidadeCotasFechamento não informada: linha " + (linha + 1));
                try
                {
                    item.QuantidadeCotasFechamento = Convert.ToDecimal(workSheet.Cell(linha, coluna29).Value);
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message + " - QuantidadeCotasFechamento: linha " + (linha + 1));
                }

                if (workSheet.Cell(linha, coluna30).Value == null)
                    throw new Exception("CodExecucaoRecolhimentoIR não informada: linha " + (linha + 1));
                try
                {
                    item.CodExecucaoRecolhimentoIR = Convert.ToInt16(workSheet.Cell(linha, coluna30).Value);
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message + " - CodExecucaoRecolhimentoIR: linha " + (linha + 1));
                }

                if (workSheet.Cell(linha, coluna31).Value == null)
                    throw new Exception("CodFormaUsoAliquotaIR não informada: linha " + (linha + 1));
                try
                {
                    item.CodFormaUsoAliquotaIR = Convert.ToInt16(workSheet.Cell(linha, coluna31).Value);
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message + " - CodFormaUsoAliquotaIR: linha " + (linha + 1));
                }

                if (workSheet.Cell(linha, coluna32).Value == null)
                    throw new Exception("TipoPosicao não informada: linha " + (linha + 1));
                try
                {
                    item.TipoPosicao = Convert.ToInt16(workSheet.Cell(linha, coluna32).Value);
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message + " - TipoPosicao: linha " + (linha + 1));
                }
                

                this.valoresExcelComeCotas.Add(item);
                //
                index++;
                linha = 1 + index;

                Console.WriteLine("{0},{1},{2},{3},{4},{5},{6},{7},{8},{9},{10},{11},{12},{13},{14},{15},{16}," +
                                  "{17},{18},{19},{20},{21},{22},{23},{24},{25},{26},{27},{28},{29},{30},{31}",
                      item.IdPosicaoComeCotas, item.IdCarteira, item.TipoEvento, item.DataAgendamentoEvento
                    , item.DataPagamentoIR_Evento, item.DataUltimaCobrancaIR, item.QuantidadeCotas, item.QuantidadeAntesCortes
                    , item.ValorCotaAplicacao, item.ValorCotaUltimoPagamentoIR, item.ValorCotaDataAgendamento, item.AliquotaIR_ComeCotas
                    , item.RendimentoBrutoDesdeAplicacao, item.RendimentoDesdeUltimoPagamentoIR, item.PrazoCautela, item.PrazoIOF, item.AliquotaIOF
                    , item.ValorIOF, item.ValorIOFVirtual, item.PrejuizoUtilizado, item.RendimentoCompensado, item.ValorIR_Agendado, item.ValorIR_Pago
                    , item.Residuo_15, item.Residuo_175, item.Residuo_20, item.Residuo_225, item.QuantidadeCotasComeCotas, item.QuantidadeCotasFechamento
                    , item.CodExecucaoRecolhimentoIR, item.CodFormaUsoAliquotaIR, item.TipoPosicao);

                    
            }
        }
        catch (Exception ex) {
            document.Close();
            document.Dispose();

            throw new Exception(ex.Message);
        }
        #endregion

        document.Close();
        document.Dispose();
    }

    /// <summary>
    /// Gera e carrega as posições Fundo e Posições Fundo Abertura de acordo 
    /// com o objeto List<ValoresExcel>
    /// </summary>
    /// <exception cref="Exception">Se ocorreu problemas</exception>
    private void CarregaComeCotas() {
        AgendaComeCotasCollection agendaComeCotasCollection = new AgendaComeCotasCollection();  
        List<int> listIdPosicao = new List<int>();


        for (int i = 0; i < this.valoresExcelComeCotas.Count; i++) {
            ValoresExcelComeCotas valoresExcel = this.valoresExcelComeCotas[i];
            //

            #region Verifica se Existe Carteira
            Carteira carteira = new Carteira();
            if (!carteira.LoadByPrimaryKey(valoresExcel.IdCarteira))
            {
                throw new Exception("Carteira Não existente : " + valoresExcel.IdCarteira);
            }
            #endregion

            int? idPosicao = this.EncontrarIDPosicao(valoresExcel.IdPosicaoComeCotas, valoresExcel.TipoPosicao);

            if(!idPosicao.HasValue)
            {
                string tipo = "fundos";
                if(valoresExcel.TipoPosicao.Equals(TipoComeCotas.Cotista.GetHashCode()))
                    tipo = "cotistas";

                throw new Exception("Posição não encontrada para o ID: " + valoresExcel.IdPosicaoComeCotas + ". Favor verificar se importação de posições de " + tipo + "com a informação de come cotas foi efetuada.");
            }

            listIdPosicao.Add(idPosicao.Value);
        }

        for (int i = 0; i < listIdPosicao.Count; i++)
        {
            AgendaComeCotas agendaComeCotas = new AgendaComeCotas();

            agendaComeCotas.Query.Where(agendaComeCotas.Query.IdPosicao.Equal(listIdPosicao[i]),
                                        agendaComeCotas.Query.TipoPosicao.Equal(valoresExcelComeCotas[i].TipoPosicao));
            agendaComeCotas.Query.Load();

            agendaComeCotas.IdCarteira = valoresExcelComeCotas[i].IdCarteira;
            agendaComeCotas.DataLancamento = valoresExcelComeCotas[i].DataAgendamentoEvento;
            agendaComeCotas.DataVencimento = valoresExcelComeCotas[i].DataPagamentoIR_Evento;
            agendaComeCotas.TipoEvento = valoresExcelComeCotas[i].TipoEvento;
            agendaComeCotas.IdPosicao = listIdPosicao[i];
            agendaComeCotas.DataUltimoIR = valoresExcelComeCotas[i].DataUltimaCobrancaIR;
            agendaComeCotas.Quantidade = valoresExcelComeCotas[i].QuantidadeCotas;
            agendaComeCotas.QuantidadeAntesCortes = valoresExcelComeCotas[i].QuantidadeAntesCortes;
            agendaComeCotas.ValorCotaAplic = valoresExcelComeCotas[i].ValorCotaAplicacao;
            agendaComeCotas.ValorCotaUltimoPagamentoIR = valoresExcelComeCotas[i].ValorCotaUltimoPagamentoIR;
            agendaComeCotas.ValorCota = valoresExcelComeCotas[i].ValorCotaDataAgendamento;
            agendaComeCotas.AliquotaCC = valoresExcelComeCotas[i].AliquotaIR_ComeCotas;
            agendaComeCotas.RendimentoBrutoDesdeAplicacao = valoresExcelComeCotas[i].RendimentoBrutoDesdeAplicacao;
            agendaComeCotas.RendimentoDesdeUltimoPagamentoIR = valoresExcelComeCotas[i].RendimentoDesdeUltimoPagamentoIR;
            agendaComeCotas.NumDiasCorridosDesdeAquisicao = valoresExcelComeCotas[i].PrazoCautela;
            agendaComeCotas.PrazoIOF = valoresExcelComeCotas[i].PrazoIOF;
            agendaComeCotas.AliquotaIOF = valoresExcelComeCotas[i].AliquotaIOF;
            agendaComeCotas.ValorIOF = valoresExcelComeCotas[i].ValorIOF;
            agendaComeCotas.ValorIOFVirtual = valoresExcelComeCotas[i].ValorIOFVirtual;
            agendaComeCotas.PrejuizoUsado = valoresExcelComeCotas[i].PrejuizoUtilizado;
            agendaComeCotas.RendimentoCompensado = valoresExcelComeCotas[i].RendimentoCompensado;
            agendaComeCotas.ValorIRAgendado = valoresExcelComeCotas[i].ValorIR_Agendado;
            agendaComeCotas.ValorIRPago = valoresExcelComeCotas[i].ValorIR_Pago;
            agendaComeCotas.Residuo15 = valoresExcelComeCotas[i].Residuo_15;
            agendaComeCotas.Residuo175 = valoresExcelComeCotas[i].Residuo_175;
            agendaComeCotas.Residuo20 = valoresExcelComeCotas[i].Residuo_20;
            agendaComeCotas.Residuo225 = valoresExcelComeCotas[i].Residuo_225;
            agendaComeCotas.QuantidadeComida = valoresExcelComeCotas[i].QuantidadeCotasComeCotas;
            agendaComeCotas.QuantidadeFinal = valoresExcelComeCotas[i].QuantidadeCotasFechamento;
            agendaComeCotas.ExecucaoRecolhimento = valoresExcelComeCotas[i].CodExecucaoRecolhimentoIR;
            agendaComeCotas.TipoAliquotaIR = valoresExcelComeCotas[i].CodFormaUsoAliquotaIR;
            agendaComeCotas.TipoPosicao = valoresExcelComeCotas[i].TipoPosicao;
            
            agendaComeCotasCollection.AttachEntity(agendaComeCotas);
        }

        agendaComeCotasCollection.Save();

    }

    /// <summary>
    /// Processa a Planilha de PosicaoFundo após ter acabado o Upload
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void uplAgendaComeCotas_FileUploadComplete(object sender, FileUploadCompleteEventArgs e) {
        e.CallbackData = "";

        #region Trata Extensão Válida
        if (!ValoresExcelUtil.isExtensaoExcel(e.UploadedFile.FileName.Trim())) {
            e.CallbackData = "Importação Agenda Come Cotas - Formato de Arquivo Inválido. Formato permitido: .xls ou .xlsx\n\n";
            return;
        }
        #endregion

        // Stream de bytes com o Conteudo do Arquivo Excel
        Stream sr = e.UploadedFile.FileContent;

        #region Processamento Arquivo Excel

        //Reseta Vetor de Dados
        this.valoresExcelComeCotas = new List<ValoresExcelComeCotas>();
        //

        try {
            // Ler Arquivo
            this.LerArquivoComeCotas(sr);
            // Carrega Arquivo
            this.CarregaComeCotas();
        }
        catch (Exception e2) {
            e.CallbackData = "Importação Agenda Come Cotas - " + e2.Message;
            return;
        }

        new HistoricoLog().InsereHistoricoLog(DateTime.Now, DateTime.Now,
                "Importação Planilha Agenda Come Cotas",
                HttpContext.Current.User.Identity.Name, UtilitarioWeb.GetIP(Request), "", HistoricoLogOrigem.Outros);

        #endregion
    }

    protected int? EncontrarIDPosicao(string IdPosicaoComeCotas, int TipoPosicao)
    {
        ImportacaoComeCotas importacaoComeCotas = new ImportacaoComeCotas();
        int? retorno;

        importacaoComeCotas.Query.Where(importacaoComeCotas.Query.TipoPosicao.Equal(TipoPosicao),
                                        importacaoComeCotas.Query.IdPosicaoComeCotas.Equal(IdPosicaoComeCotas));
        if (importacaoComeCotas.Query.Load())
            retorno =  importacaoComeCotas.IdPosicao;
        else
            retorno =  null;

        return retorno;

    }


}