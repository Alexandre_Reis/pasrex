﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Web;
using System.Data;
using System.Text;
using EntitySpaces.Interfaces;
using DevExpress.Web;
using System.ComponentModel;
using System.Reflection;
using Financial.Interfaces.Import;
using Financial.Web.Common;
using Financial.Security;
using Financial.Util;
using Financial.Security.Enums;
using Financial.Bolsa;
using Financial.Common;
using Financial.Bolsa.Enums;
using Financial.BMF;
using Financial.RendaFixa;
using Financial.RendaFixa.Enums;
using Financial.Common.Enums;
using Financial.Fundo;

public partial class ImportacaoBasePage : BasePage 
{
    /// <summary>
    /// 
    /// </summary>
    /// <param name="textDescricao"></param>
    /// <param name="textDataVencimento"></param>
    /// <param name="textPercentual"></param>
    /// <param name="dropIndice"></param>
    /// <param name="textTaxa"></param>
    /// <returns></returns>
    private string MontaDescricaoCompleta(string descricao, DateTime dataVencimento, decimal percentual,
                                          short? idIndice, decimal taxa)
    {
        StringBuilder descricaoCompleta = new StringBuilder();
        descricaoCompleta.Append(descricao + " - ");
        descricaoCompleta.Append(" Vcto: ");

        descricaoCompleta.Append(dataVencimento.ToShortDateString());
        if (idIndice.HasValue || taxa != 0)
        {
            descricaoCompleta.Append(" -> ");
        }

        if (idIndice.HasValue)
        {
            Indice indice = new Indice();
            if (indice.LoadByPrimaryKey(idIndice.Value))
            {
                if (percentual != 0)
                {
                    descricaoCompleta.Append(String.Format("{0:n}", percentual + "% "));
                }
                else
                {
                    descricaoCompleta.Append("100% ");
                }

                descricaoCompleta.Append(indice.Descricao);
            }
        }

        if (taxa != 0)
        {
            if (idIndice.HasValue)
            {
                descricaoCompleta.Append(" + " + String.Format("{0:n}", taxa + "%"));
            }
            else
            {
                descricaoCompleta.Append("Tx = " + String.Format("{0:n}", taxa + "%"));
            }
        }

        return descricaoCompleta.ToString().Replace("/", "-");
    }

    private bool IsTituloPublico(string tipoAtivo)
    {
        return (tipoAtivo == "LF" || tipoAtivo == "NTB" || tipoAtivo == "NTC" || tipoAtivo == "LTN");
    }
               
    /// <summary>
    /// Importa Lista Emissores Isin
    /// </summary>
    /// <param name="path"></param>
    protected void ImportaEmissoresIsin(string path) 
    {
        this.Download(path);

        DateTime dataInicio = DateTime.Now;
        bool indicaErro = false;
        //
        
        string mensagemErroEmissoresIsin = "";
        try
        {
            // Estrtutura com os dados do arquivo Numerca
            Numerca.NumercaStructure[] numerca = this.ProcessaNumerca(path);
            //

            #region Códigos ISIN BMF
            AtivoBMFCollection ativoBMFCollection = new AtivoBMFCollection();
            ativoBMFCollection.Query.Select(ativoBMFCollection.Query.CdAtivoBMF,
                                            ativoBMFCollection.Query.Serie,
                                            ativoBMFCollection.Query.CodigoIsin);
            ativoBMFCollection.Query.Load();
            foreach (AtivoBMF ativoBMF in ativoBMFCollection)
            {
                foreach (Numerca.NumercaStructure numercaStructure in numerca)
                {
                    if (ativoBMF.CdAtivoBMF == numercaStructure.tipoAtivo && ativoBMF.Serie == numercaStructure.numSerieOpcao)
                    {
                        ativoBMF.CodigoIsin = numercaStructure.codigoIsin;
                    }
                }
            }
            ativoBMFCollection.Save();
            #endregion

            #region Títulos Públicos (se existe atualiza ISIN, caso contrário cria o título)
            EstrategiaCollection estrategiaCollection = new EstrategiaCollection();
            estrategiaCollection.Query.Select(estrategiaCollection.Query.IdEstrategia);
            estrategiaCollection.Query.Where(estrategiaCollection.Query.Descricao.Like("%Renda Fixa%"));
            estrategiaCollection.Query.Load();

            foreach (Numerca.NumercaStructure numercaStructure in numerca)
            {
                try
                {
                    if (numercaStructure.codigoCFI.Length > 0 && numercaStructure.codigoCFI.Substring(0, 1) == "D" && this.IsTituloPublico(numercaStructure.tipoAtivo)) //Renda Fixa Pública
                    {
                        TituloRendaFixaQuery tituloRendaFixaQuery = new TituloRendaFixaQuery("T");
                        PapelRendaFixaQuery papelRendaFixaQuery = new PapelRendaFixaQuery("P");
                        tituloRendaFixaQuery.Select(tituloRendaFixaQuery);
                        tituloRendaFixaQuery.InnerJoin(papelRendaFixaQuery).On(papelRendaFixaQuery.IdPapel == tituloRendaFixaQuery.IdPapel);
                        tituloRendaFixaQuery.Where(papelRendaFixaQuery.TipoPapel.Equal((byte)TipoPapelTitulo.Publico));

                        TituloRendaFixaCollection tituloRendaFixaCollection = new TituloRendaFixaCollection();
                        tituloRendaFixaCollection.Load(tituloRendaFixaQuery);

                        bool achou = false;
                        foreach (TituloRendaFixa tituloRendaFixa in tituloRendaFixaCollection)
                        {
                            if (!String.IsNullOrEmpty(tituloRendaFixa.CodigoCustodia) &&
                            tituloRendaFixa.DataEmissao == numercaStructure.dataEmissao &&
                            tituloRendaFixa.DataVencimento == numercaStructure.dataExpiracao &&
                            tituloRendaFixa.CodigoCustodia.Trim() == numercaStructure.codigoSelic.Trim())
                            {
                                tituloRendaFixa.CodigoIsin = numercaStructure.codigoIsin;
                                achou = true;
                            }
                        }
                        tituloRendaFixaCollection.Save();

                        
                        if (!achou) //Cria o título se não existir
                        {
                            PapelRendaFixaCollection papelRendaFixaCollection = new PapelRendaFixaCollection();
                            papelRendaFixaCollection.Query.Select(papelRendaFixaCollection.Query.IdPapel);

                            string descricao = "";
                            string descricaoCompleta = "";
                            short? idIndice = null;
                            decimal percentual = 0;
                            decimal taxa = 0;
                            if (numercaStructure.tipoAtivo == "LF")
                            {
                                descricao = "LFT";
                                idIndice = (byte)ListaIndiceFixo.SELIC;
                                percentual = 100;
                                papelRendaFixaCollection.Query.Where(papelRendaFixaCollection.Query.Classe.Equal((int)ClasseRendaFixa.LFT));
                            }
                            else if (numercaStructure.tipoAtivo == "NTB")
                            {
                                descricao = "NTN-B";
                                idIndice = (byte)ListaIndiceFixo.IPCA;
                                percentual = 100;
                                taxa = 6M;
                                papelRendaFixaCollection.Query.Where(papelRendaFixaCollection.Query.Classe.Equal((int)ClasseRendaFixa.NTN),
                                                                     papelRendaFixaCollection.Query.Descricao.Like("%B%"));
                            }
                            else if (numercaStructure.tipoAtivo == "NTC")
                            {
                                descricao = "NTN-C";
                                idIndice = (byte)ListaIndiceFixo.IGPM;
                                percentual = 100;
                                taxa = 6M;
                                papelRendaFixaCollection.Query.Where(papelRendaFixaCollection.Query.Classe.Equal((int)ClasseRendaFixa.NTN),
                                                                     papelRendaFixaCollection.Query.Descricao.Like("%B%"));
                            }
                            else if (numercaStructure.tipoAtivo == "LTN")
                            {
                                descricao = "LTN";
                                papelRendaFixaCollection.Query.Where(papelRendaFixaCollection.Query.Classe.Equal((int)ClasseRendaFixa.LTN));
                            }
                            papelRendaFixaCollection.Query.Load();

                            int idPapel = papelRendaFixaCollection[0].IdPapel.Value;

                            TituloRendaFixa tituloRendaFixa = new TituloRendaFixa();
                            tituloRendaFixa.CodigoCustodia = numercaStructure.codigoSelic;
                            tituloRendaFixa.CodigoIsin = numercaStructure.codigoIsin;
                            tituloRendaFixa.DataEmissao = numercaStructure.dataEmissao;
                            tituloRendaFixa.DataVencimento = numercaStructure.dataExpiracao;
                            tituloRendaFixa.Descricao = descricao;
                            tituloRendaFixa.DescricaoCompleta = descricaoCompleta;
                            tituloRendaFixa.IdEmissor = (int)ListaEmissorFixo.TesouroNacional;

                            if (estrategiaCollection.Count > 0)
                            {
                            tituloRendaFixa.IdEstrategia = estrategiaCollection[0].IdEstrategia.Value;
                            }

                            tituloRendaFixa.IdIndice = idIndice;
                            tituloRendaFixa.IdMoeda = (byte)ListaMoedaFixo.Real;
                            tituloRendaFixa.IdPapel = idPapel;
                            tituloRendaFixa.IsentoIOF = (byte)TituloIsentoIOF.NaoIsento;
                            tituloRendaFixa.IsentoIR = (byte)TituloIsentoIR.NaoIsento;
                            tituloRendaFixa.Percentual = percentual;
                            tituloRendaFixa.PUNominal = 1000;
                            tituloRendaFixa.Taxa = taxa;
                            tituloRendaFixa.ValorNominal = 1000;

                            tituloRendaFixa.Save();
                        }
                        
                    }
                }
                catch (Exception)
                {
                    continue;
                }
            }
            
            #endregion

            #region Emissores
            Financial.Interfaces.Import.Emissor.EmissorStructure[] emissorArray = this.ProcessaEmissor(path);

            EmissorCollection emissorCollection = new EmissorCollection();
            emissorCollection.LoadAll();

            foreach (Financial.Common.Emissor emissor in emissorCollection)
            {
                AtivoBolsaCollection ativoBolsaCollection = new AtivoBolsaCollection();
                ativoBolsaCollection.Query.Select(ativoBolsaCollection.Query.CdAtivoBolsa);
                ativoBolsaCollection.Query.Where(ativoBolsaCollection.Query.TipoMercado.Equal(TipoMercadoBolsa.MercadoVista),
                                                 ativoBolsaCollection.Query.IdEmissor.Equal(emissor.IdEmissor.Value));
                ativoBolsaCollection.Query.Load();

                if (ativoBolsaCollection.Count > 0)
                {
                    foreach (Financial.Interfaces.Import.Emissor.EmissorStructure emissorStructure in emissorArray)
                    {
                        string cdAtivoBolsa = ativoBolsaCollection[0].CdAtivoBolsa.Trim();

                        if (cdAtivoBolsa.Length >= 4 && cdAtivoBolsa.Substring(0, 4) == emissorStructure.codigo)
                        {
                            emissor.Cnpj = emissorStructure.cnpj.PadLeft(14, '0');
                        }
                        
                    }                  
                }
            }

            emissorCollection.Save();
            #endregion
        }
        catch (Exception e1) {
            indicaErro = true;
            mensagemErroEmissoresIsin = "Emissores Isin com problemas.\n";
            mensagemErroEmissoresIsin += " - Erro: " + e1.Message;
        }

        if (!String.IsNullOrEmpty(mensagemErroEmissoresIsin)) {
            this.mensagem.AppendLine("\t" + mensagemErroEmissoresIsin);
        }

        #region Log do Processo
        DateTime dataFim = DateTime.Now;
        //
        if (!indicaErro) {
            HistoricoLog historicoLog = new HistoricoLog();
            historicoLog.Descricao = "Importação Emissores Isin ";
            historicoLog.Login = HttpContext.Current.User.Identity.Name;
            historicoLog.Maquina = Utilitario.GetLocalIp();
            historicoLog.Cultura = "";
            historicoLog.DataInicio = dataInicio;
            historicoLog.DataFim = dataFim;
            historicoLog.Origem = (int)HistoricoLogOrigem.Outros;
            historicoLog.Save();
        }
        #endregion
    }

    /// <summary>
    /// Faz o Download do Arquivo Numerca
    /// </summary>
    /// <param name="path">Path Completo do Diretorio onde o Arquivo será Procurado</param>
    /// <exception cref="Exception">Se download falhou ou Processamento deu errado</exception>                
    private Numerca.NumercaStructure[] ProcessaNumerca(string path) 
    {
        string arq = path + "NUMERACA.txt";
        
        try 
        {
            Numerca.NumercaStructure[] numerca = new Numerca().ImportaNumerca(arq);
            //Numerca.NumercaStructure[] numerca = new Numerca.NumercaStructure[0];            
            //DataTable d = ToDataTable(numerca);
            
            #region DataTable - descomentar para facilitar testes - Produz um DataTable
            /*
            DataTable dataTable = new DataTable();            
            this.CreateColumns(dataTable);

            int j = 0;
            foreach (string line in File.ReadAllLines(arq, Encoding.GetEncoding("ISO-8859-1"))) {
                if (!String.IsNullOrEmpty(line)) {

                    string[] colunas = line.Split(new Char[] { ',' }); // delimitado por ,
                    //
                    //string c1 = colunas[0];
                    //string c2 = colunas[1];

                    dataTable.Rows.Add( j+1,
                        colunas[0],
                        colunas[1],
                        colunas[2],
                        colunas[3],
                        colunas[4],
                        colunas[5],
                        colunas[6],
                        colunas[7],
                        colunas[8],
                        colunas[9],
                        colunas[10],
                        colunas[11],
                        colunas[12],
                        colunas[13],
                        colunas[14],
                        colunas[15],
                        colunas[16],
                        colunas[17],
                        colunas[18],
                        colunas[19],
                        colunas[20],
                        colunas[21],
                        colunas[22],
                        colunas[23],
                        colunas[24],
                        colunas[25],
                        colunas[26],
                        colunas[27],
                        colunas[28],
                        colunas[29],
                        colunas[30],
                        colunas[31],
                        colunas[32],
                        colunas[33],
                        colunas[34],
                        colunas[35],
                        colunas[36],
                        colunas[37],
                        colunas[38],
                        colunas[39],
                        colunas[40],
                        colunas[41],
                        colunas[42],
                        colunas[43]
                        );
                    j++;
                }
            } */
            #endregion
          
            return numerca;
        }
        catch (Exception e) {
            throw new Exception(e.Message);
        }
    }

    /// <summary>
    /// Faz o Download do Arquivo Numerca
    /// </summary>
    /// <param name="path">Path Completo do Diretorio onde o Arquivo será Procurado</param>
    /// <exception cref="Exception">Se download falhou ou Processamento deu errado</exception>                
    private Financial.Interfaces.Import.Emissor.EmissorStructure[] ProcessaEmissor(string path)
    {
        string arq = path + "EMISSOR.txt";

        try
        {
            Financial.Interfaces.Import.Emissor.EmissorStructure[] emissorArray = new Financial.Interfaces.Import.Emissor().ImportaEmissor(arq);

            return emissorArray;
        }
        catch (Exception e)
        {
            throw new Exception(e.Message);
        }
    }

    #region Para Testar resultados
    /// <summary>
    /// Cria as colunas do DataTable - Para poder testar
    /// </summary>
    /// <param name="d"></param>
    private void CreateColumns(DataTable d) {
        d.Columns.Add("linha", typeof(int));
        d.Columns.Add("dataGeracaoArquivo", typeof(String));
        d.Columns.Add("acaoSofrida", typeof(String));
        d.Columns.Add("codigoIsin", typeof(String));
        d.Columns.Add("codigoEmissor", typeof(String));
        d.Columns.Add("codigoCFI", typeof(String));
        d.Columns.Add("descricao", typeof(String));
        d.Columns.Add("anoEmissao", typeof(String));
        d.Columns.Add("dataEmissao", typeof(String));
        d.Columns.Add("anoExpiracao", typeof(String));
        d.Columns.Add("dataExpiracao", typeof(String));
        d.Columns.Add("taxaJuros", typeof(String));                
        d.Columns.Add("moedas", typeof(String));
        d.Columns.Add("valorNominal", typeof(String));
        d.Columns.Add("precoExercicio", typeof(String));
        d.Columns.Add("indexador", typeof(String));
        d.Columns.Add("percentualIndexador", typeof(String));
        d.Columns.Add("dataAcao", typeof(String));
        d.Columns.Add("codigoCetip", typeof(String));
        d.Columns.Add("codigoSelic", typeof(String));
        d.Columns.Add("codigoPais", typeof(String));
        d.Columns.Add("tipoAtivo", typeof(String));
        d.Columns.Add("codigoCategoria", typeof(String));
        d.Columns.Add("codigoEspecie", typeof(String));
        d.Columns.Add("dataBase", typeof(String));
        d.Columns.Add("numeroEmissao", typeof(String));
        d.Columns.Add("numeroSerie", typeof(String));
        d.Columns.Add("tipoEmissao", typeof(String));
        d.Columns.Add("tipoAtivoObjeto", typeof(String));
        d.Columns.Add("tipoEntrega", typeof(String));
        d.Columns.Add("tipoFundo", typeof(String));
        d.Columns.Add("tipoGarantia", typeof(String));
        d.Columns.Add("tipoJuros", typeof(String));
        d.Columns.Add("tipoMercado", typeof(String));
        d.Columns.Add("tipoStatusIsin", typeof(String));
        d.Columns.Add("tipoVencimento", typeof(String));
        d.Columns.Add("tipoProtecao", typeof(String));
        d.Columns.Add("tipoPoliticaDistribuicaoJuros", typeof(String));
        d.Columns.Add("tipoAtivoInvestidoFundo", typeof(String));
        d.Columns.Add("tipoForma", typeof(String));
        d.Columns.Add("tipoEstiloOpcao", typeof(String));
        d.Columns.Add("numSerieOpcao", typeof(String));
        d.Columns.Add("codigoFrequenciaJuros", typeof(String));
        d.Columns.Add("situacaoIsin", typeof(String));
        d.Columns.Add("dataPrimeiroPagamentoJuros", typeof(String));
    }

    public DataTable ToDataTable<T>(IList<T> data)
    {
        PropertyDescriptorCollection properties = TypeDescriptor.GetProperties(typeof(T));
        DataTable table = new DataTable();

        foreach (PropertyDescriptor prop in properties)
            table.Columns.Add(prop.Name, Nullable.GetUnderlyingType(prop.PropertyType) ?? prop.PropertyType);
        
        foreach (T item in data) {
            DataRow row = table.NewRow();
            foreach (PropertyDescriptor prop in properties)
                 row[prop.Name] = prop.GetValue(item) ?? DBNull.Value;
            table.Rows.Add(row);
        }
        return table;
    }
    #endregion

    /// <summary>
    /// Baixa o Arquivo Numerca
    /// Descompacta o Arquivo Zip
    /// </summary>
    /// <param name="path">Caminho Destino</param>
    /// <returns></returns>
    private bool Download(string path) 
    {
        #region Download
        string endereco = "http://www.bmfbovespa.com.br/isin/DownloadArquivo.asp?TipoArquivo=P";

        string pathArquivoDestino = path + "emissores.zip";
        //
        bool download = Utilitario.DownloadFile(endereco, pathArquivoDestino);
        #endregion

        #region Descompacta
        // Se foi feito o Download, descompacta o arquivo

        if (download) {
            bool descompacta = Utilitario.UnzipFile("emissores.zip", path, "+NUMERACA.txt");
            descompacta = Utilitario.UnzipFile("emissores.zip", path, "+EMISSOR.txt");
            if (descompacta) {

                if (File.Exists(pathArquivoDestino)) {
                    File.Delete(pathArquivoDestino);
                }

                // Se arquivo existe retorna true            
                return File.Exists(path + "NUMERACA.txt") && File.Exists(path + "EMISSOR.txt");
            }
            else {

                if (File.Exists(pathArquivoDestino)) {
                    File.Delete(pathArquivoDestino);
                }

                return false;
            }
        }
        else {
            return false;
        }
        #endregion
    }
}