﻿using System;
using System.Collections.Generic;
using System.IO;

using EntitySpaces.Interfaces;

using DevExpress.Web;
using Financial.Web.Common;
using Financial.Bolsa;
using Financial.Security;
using Financial.Security.Enums;
using System.Web;
using Financial.Util;
using Financial.Common;
using Financial.Common.Enums;

public partial class ImportacaoBasePage : BasePage {

    /// <summary>
    /// Importa Indic no periodo Indicado: entre DataInicio e DataFim
    /// </summary>
    /// <param name="dataInicio"></param>
    /// <param name="dataFim"></param>
    /// <param name="path"></param>
    protected void ImportaIndic(DateTime dataInicio, DateTime dataFim, string path) {

        DateTime data = dataInicio;

        for (; data <= dataFim; ) {
            this.ImportaIndic(data, path);
            //
            data = Calendario.AdicionaDiaUtil(data, 1, LocalFeriadoFixo.Brasil, TipoFeriado.Brasil);
        }
        AtualizaFatorIndic();
    }

    private void AtualizaFatorIndic()
    {
        if (ParametrosConfiguracaoSistema.Outras.CalculaFatorIndice)
        {
            CotacaoIndice cotacaoIndice = new CotacaoIndice();
            List<short> indiceAtualizaFator = new List<short>();
            indiceAtualizaFator.Add(ListaIndiceFixo.CDI);
            indiceAtualizaFator.Add(ListaIndiceFixo.PTAX_800VENDA);
            indiceAtualizaFator.Add(ListaIndiceFixo.PTAX_800COMPRA);
            indiceAtualizaFator.Add(ListaIndiceFixo.PTAX_800MEDIO);
            indiceAtualizaFator.Add(ListaIndiceFixo.PTAX_REF);
            indiceAtualizaFator.Add(ListaIndiceFixo.PTAX_REF_2);
            indiceAtualizaFator.Add(ListaIndiceFixo.SELIC);
            indiceAtualizaFator.Add(ListaIndiceFixo.TR);
            foreach (short indice in indiceAtualizaFator)
            {
                cotacaoIndice.AtualizaFatorIndice(indice);
            }
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="data"></param>
    /// <param name="path"></param>
    private void ImportaIndic(DateTime data, string path) {
        #region Indic

        DateTime dataInicio = DateTime.Now;
        bool indicaErro = false;
        //
        CotacaoIndice cotacaoIndice = new CotacaoIndice();
        //
        try {
            cotacaoIndice.CarregaCotacaoIndiceIndic(data, path);
        }
        catch (Exception e1) {
            indicaErro = true;
            //
            string msg = "Indic com problemas - Data: " + data.ToShortDateString() + "\n";
            msg += " - Cotações Índices não Importadas: " + e1.Message;

            this.MensagemErroInternet.AppendLine("\t" + msg);
            //mensagem.AppendLine();
        }

        #region Log do Processo
        DateTime dataFim = DateTime.Now;
        //
        if (!indicaErro) {
            HistoricoLog historicoLog = new HistoricoLog();
            historicoLog.Descricao = "Importação Indic - " + data.ToShortDateString();
            historicoLog.Login = HttpContext.Current.User.Identity.Name;
            historicoLog.Maquina = Utilitario.GetLocalIp();
            historicoLog.Cultura = "";
            historicoLog.DataInicio = dataInicio;
            historicoLog.DataFim = dataFim;
            historicoLog.Origem = (int)HistoricoLogOrigem.Outros;
            historicoLog.Save();
            //
        }
        #endregion

        #endregion
    }
}