﻿using System;
using System.Collections.Generic;
using System.IO;

using EntitySpaces.Interfaces;

using DevExpress.Web;
using Financial.Web.Common;
using Financial.Bolsa;
using Financial.Security;
using Financial.Security.Enums;
using System.Web;
using Financial.Util;
using Financial.Common;
using Financial.BMF;
using Financial.Common.Enums;

public partial class ImportacaoBasePage : BasePage {

    /// <summary>
    /// Importa BdPregao no periodo Indicado: entre DataInicio e DataFim
    /// </summary>
    /// <param name="dataInicio"></param>
    /// <param name="dataFim"></param>
    /// <param name="path"></param>
    protected void ImportaBdPregao(DateTime dataInicio, DateTime dataFim, string path) {

        DateTime data = dataInicio;

        for (; data <= dataFim; ) {
            this.ImportaBdPregao(data, path);
            //
            data = Calendario.AdicionaDiaUtil(data, 1, LocalFeriadoFixo.Bovespa, TipoFeriado.Brasil);
        }
        AtualizaFatorBdPregao();
    }

    private void AtualizaFatorBdPregao()
    {
        if (ParametrosConfiguracaoSistema.Outras.CalculaFatorIndice)
        {
            CotacaoIndice cotacaoIndice = new CotacaoIndice();
            List<short> indiceAtualizaFator = new List<short>();
            indiceAtualizaFator.Add(ListaIndiceFixo.PTAX_800VENDA);
            indiceAtualizaFator.Add(ListaIndiceFixo.PTAX_REF);
            indiceAtualizaFator.Add(ListaIndiceFixo.OUROBMF_FECHA);
            indiceAtualizaFator.Add(ListaIndiceFixo.OUROBMF_MEDIO);
            foreach (short indice in indiceAtualizaFator)
            {
                cotacaoIndice.AtualizaFatorIndice(indice);
            }
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="data"></param>
    /// <param name="path"></param>
    private void ImportaBdPregao(DateTime data, string path) {
        #region BdPregao

        DateTime dataInicio = DateTime.Now;
        bool indicaErro = false;
        //
        AtivoBMF ativoBMF = new AtivoBMF();
        CotacaoIndice cotacaoIndice = new CotacaoIndice();
        CotacaoBMF cotacaoBMF = new CotacaoBMF();
        //
        string mensagemErroBDPregao = "";
        try {
            ativoBMF.CarregaAtivoBdPregao(data, path);
        }
        catch (Exception e1) {
            indicaErro = true;
            mensagemErroBDPregao = "BDPregão com problemas - Data: " + data.ToShortDateString() + "\n";
            mensagemErroBDPregao += " - Cotações não Importadas: " + e1.Message;
        }
        //
        try {
            cotacaoIndice.CarregaCotacaoIndiceBdPregao(data, path);
        }
        catch (Exception e2) {
            indicaErro = true;
            if (String.IsNullOrEmpty(mensagemErroBDPregao)) {
                mensagemErroBDPregao = "BDPregão com problemas - Data: " + data.ToShortDateString() + "\n";
                mensagemErroBDPregao += " - Cotações Índices não Importadas: " + e2.Message;
            }
            else {
                mensagemErroBDPregao += " - Cotações Índices não Importadas: " + e2.Message;
            }
        }
        //
        try {
            cotacaoBMF.CarregaCotacaoBdPregao(data, path);
        }
        catch (Exception e3) {
            //indicaErro = true;
            if (String.IsNullOrEmpty(mensagemErroBDPregao)) {
                mensagemErroBDPregao = "BDPregão com problemas - Data: " + data.ToShortDateString() + "\n";
                mensagemErroBDPregao += " - Cotações BMF não Importadas: " + e3.Message;
            }
            else {
                mensagemErroBDPregao += " - Cotações BMF não Importadas: " + e3.Message;
            }
        }
        //
        if (!String.IsNullOrEmpty(mensagemErroBDPregao)) {
            this.MensagemErroInternet.AppendLine("\t" + mensagemErroBDPregao);
            //mensagem.AppendLine();
        }

        #region Log do Processo
        DateTime dataFim = DateTime.Now;
        //
        if (!indicaErro) {
            HistoricoLog historicoLog = new HistoricoLog();
            historicoLog.Descricao = "Importação BDPregão - " + data.ToShortDateString();
            historicoLog.Login = HttpContext.Current.User.Identity.Name;
            historicoLog.Maquina = Utilitario.GetLocalIp();
            historicoLog.Cultura = "";
            historicoLog.DataInicio = dataInicio;
            historicoLog.DataFim = dataFim;
            historicoLog.Origem = (int)HistoricoLogOrigem.Outros;
            historicoLog.Save();
            //
        }
        #endregion
    }
        #endregion
}