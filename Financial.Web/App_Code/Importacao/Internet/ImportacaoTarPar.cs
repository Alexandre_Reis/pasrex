﻿using System;
using System.Collections.Generic;
using System.IO;

using EntitySpaces.Interfaces;

using DevExpress.Web;
using Financial.Web.Common;
using Financial.Bolsa;
using Financial.Security;
using Financial.Security.Enums;
using System.Web;
using Financial.Util;
using Financial.Common;
using Financial.BMF;
using Financial.Common.Enums;

public partial class ImportacaoBasePage : BasePage {

    /// <summary>
    /// Importa TarPar no periodo Indicado: entre DataInicio e DataFim
    /// </summary>
    /// <param name="dataInicio"></param>
    /// <param name="dataFim"></param>
    /// <param name="path"></param>
    protected void ImportaTarPar(DateTime dataInicio, DateTime dataFim, string path) {

        DateTime data = dataInicio;

        for (; data <= dataFim; ) {
            this.ImportaTarPar(data, path);
            //
            data = Calendario.AdicionaDiaUtil(data, 1, LocalFeriadoFixo.Bovespa, TipoFeriado.Brasil);
        }
        AtualizaFatorTarPar();
    }

    private void AtualizaFatorTarPar()
    {
        if (ParametrosConfiguracaoSistema.Outras.CalculaFatorIndice)
        {
            CotacaoIndice cotacaoIndice = new CotacaoIndice();
            List<short> indiceAtualizaFator = new List<short>();
            indiceAtualizaFator.Add(ListaIndiceFixo.IGPM);
            indiceAtualizaFator.Add(ListaIndiceFixo.IDI);
            foreach (short indice in indiceAtualizaFator)
            {
                cotacaoIndice.AtualizaFatorIndice(indice);
            }
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="data"></param>
    /// <param name="path"></param>
    private void ImportaTarPar(DateTime data, string path) {
        #region TarPar

        DateTime dataInicio = DateTime.Now;
        bool indicaErro = false;
        //
        TabelaCalculoBMF tabelaCalculoBMF = new TabelaCalculoBMF();
        CotacaoIndice cotacaoIndice = new CotacaoIndice();
        //
        string mensagemErroTarpar = "";
        try {
            tabelaCalculoBMF.CarregaTabelaCalculoBMFTarPar(data, path);
        }
        catch (Exception e1) {
            indicaErro = true;
            mensagemErroTarpar = "TarPar com problemas - Data: " + data.ToShortDateString() + "\n";
            mensagemErroTarpar += " - Dados não Importados: " + e1.Message;
        }
        //
        try {
            cotacaoIndice.CarregaCotacaoIndiceTarPar(data, path);
        }
        catch (Exception e2) {
            indicaErro = true;
            if (String.IsNullOrEmpty(mensagemErroTarpar)) {
                mensagemErroTarpar = "TarPar com problemas - Data: " + data.ToShortDateString() + "\n";
                mensagemErroTarpar += " - Cotações Índices não Importadas: " + e2.Message;
            }
        }
        if (!String.IsNullOrEmpty(mensagemErroTarpar)) {
            this.MensagemErroInternet.AppendLine("\t" + mensagemErroTarpar);
            //mensagem.AppendLine();
        }

        #region Log do Processo
        DateTime dataFim = DateTime.Now;

        if (!indicaErro) {
            HistoricoLog historicoLog = new HistoricoLog();
            historicoLog.Descricao = "Importação TarPar - " + data.ToShortDateString();
            historicoLog.Login = HttpContext.Current.User.Identity.Name;
            historicoLog.Maquina = Utilitario.GetLocalIp();
            historicoLog.Cultura = "";
            historicoLog.DataInicio = dataInicio;
            historicoLog.DataFim = dataFim;
            historicoLog.Origem = (int)HistoricoLogOrigem.Outros;
            historicoLog.Save();
        }
        //
        #endregion

        #endregion
    }
}