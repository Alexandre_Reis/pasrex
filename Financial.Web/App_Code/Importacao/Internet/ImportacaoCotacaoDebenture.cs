﻿using System;
using System.Collections.Generic;
using System.IO;

using EntitySpaces.Interfaces;

using DevExpress.Web;
using Financial.Web.Common;
using Financial.Bolsa;
using Financial.Security;
using Financial.Security.Enums;
using System.Web;
using Financial.Util;
using Financial.Common;
using Financial.RendaFixa;
using Financial.Common.Enums;

public partial class ImportacaoBasePage : BasePage {

    #region Chamadores
    protected void ImportaCotacaoDebenture(DateTime dataInicio, DateTime dataFim, string path)
    {
        this.ImportaCotacaoMercadoDebenture(dataInicio, dataFim, path);
        this.ImportaCotacaoParDebenture(dataInicio, dataFim, path);
    }

    /// <summary>
    /// Importa CotacaoDebenture no periodo Indicado: entre DataInicio e DataFim
    /// </summary>
    /// <param name="dataInicio"></param>
    /// <param name="dataFim"></param>
    /// <param name="path"></param>
    protected void ImportaCotacaoMercadoDebenture(DateTime dataInicio, DateTime dataFim, string path)
    {

        DateTime data = dataInicio;

        for (; data <= dataFim; ) {
            this.ImportaCotacaoMercadoDebenture(data, path);
            //
            data = Calendario.AdicionaDiaUtil(data, 1, LocalFeriadoFixo.Brasil, TipoFeriado.Brasil);
        }
    }

    /// <summary>
    /// Importa CotacaoDebentureSiteDebentures no periodo Indicado: entre DataInicio e DataFim
    /// </summary>
    /// <param name="dataInicio"></param>
    /// <param name="dataFim"></param>
    /// <param name="path"></param>
    protected void ImportaCotacaoParDebenture(DateTime dataInicio, DateTime dataFim, string path)
    {

        DateTime data = dataInicio;

        for (; data <= dataFim; ) {
            this.ImportaCotacaoParDebenture(data, path);
            //
            data = Calendario.AdicionaDiaUtil(data, 1, LocalFeriadoFixo.Brasil, TipoFeriado.Brasil);
        }
    }
    #endregion

    /// <summary>
    /// 
    /// </summary>
    /// <param name="data"></param>
    /// <param name="path"></param>
    private void ImportaCotacaoMercadoDebenture(DateTime data, string path) {
        #region CotacaoDebenture

        DateTime dataInicio = DateTime.Now;
        bool indicaErro = false;
        //
        CotacaoMercadoDebenture cotacaoDebenture = new CotacaoMercadoDebenture();
        //
        try {
            cotacaoDebenture.CarregaDebenture(data, path);
        }
        catch (Exception e1) {
            indicaErro = true;
            //
            string msg = "Cotações Debentures com problemas - Data: " + data.ToShortDateString() + "\n";
            msg += " - Dados não Importados: " + e1.Message;

            this.MensagemErroInternet.AppendLine("\t" + msg);
        }

        #region Log do Processo
        if (!indicaErro) {
            DateTime dataFim = DateTime.Now;
            //
            HistoricoLog historicoLog = new HistoricoLog();
            historicoLog.Descricao = "Importação CotacaoDebenture - " + data.ToShortDateString();
            historicoLog.Login = HttpContext.Current.User.Identity.Name;
            historicoLog.Maquina = Utilitario.GetLocalIp();
            historicoLog.Cultura = "";
            historicoLog.DataInicio = dataInicio;
            historicoLog.DataFim = dataFim;
            historicoLog.Origem = (int)HistoricoLogOrigem.Outros;
            historicoLog.Save();
            //
        }
        #endregion

        #endregion
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="data"></param>
    /// <param name="path"></param>
    private void ImportaCotacaoParDebenture(DateTime data, string path) {
        #region CotacaoDebenture

        DateTime dataInicio = DateTime.Now;
        bool indicaErro = false;
        //
        CotacaoDebenture cotacaoDebenture = new CotacaoDebenture();
        //
        try {
            cotacaoDebenture.CarregaDebenture(data, path);
        }
        catch (Exception e1) {
            indicaErro = true;
            //
            string msg = "Cotações Debentures do site www.debentures.com com problemas - Data: " + data.ToShortDateString() + "\n";
            msg += " - Dados não Importados: " + e1.Message;

            this.MensagemErroInternet.AppendLine("\t" + msg);
        }

        #region Log do Processo
        if (!indicaErro) {
            DateTime dataFim = DateTime.Now;
            //
            HistoricoLog historicoLog = new HistoricoLog();
            historicoLog.Descricao = "Importação CotacaoDebenture - www.debentures.com.br - " + data.ToShortDateString();
            historicoLog.Login = HttpContext.Current.User.Identity.Name;
            historicoLog.Maquina = Utilitario.GetLocalIp();
            historicoLog.Cultura = "";
            historicoLog.DataInicio = dataInicio;
            historicoLog.DataFim = dataFim;
            historicoLog.Origem = (int)HistoricoLogOrigem.Outros;
            historicoLog.Save();
        }
        #endregion

        #endregion
    }
}