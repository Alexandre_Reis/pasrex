﻿using System;
using System.Collections.Generic;
using System.IO;

using EntitySpaces.Interfaces;

using DevExpress.Web;
using Financial.Web.Common;
using Financial.Bolsa;
using Financial.Security;
using Financial.Security.Enums;
using System.Web;
using Financial.Util;
using Financial.Common;
using Financial.RendaFixa;
using Financial.Common.Enums;
using System.Net;
using System.Text;
using System.Globalization;

/// <summary>
/// Classe para Importacao do Ima
/// </summary>
public partial class ImportacaoBasePage : BasePage {

    /// <summary>
    /// Importa IMA no periodo Indicado: entre DataInicio e DataFim
    /// </summary>
    /// <param name="dataInicio"></param>
    /// <param name="dataFim"></param>
    protected void ImportaIma(DateTime dataInicio, DateTime dataFim) {

        DateTime data = dataInicio;

        for (; data <= dataFim; ) {
            this.ImportaIma(data);
            //
            data = Calendario.AdicionaDiaUtil(data, 1, LocalFeriadoFixo.Brasil, TipoFeriado.Brasil);
        }
    }

    /// <summary>
    /// Insere a Cotação do Índice Ima-B / Ima-C/ Ima-S e IRF-M
    /// </summary>
    /// <param name="data"></param>
    private void ImportaIma(DateTime data) {
        #region IMA

        DateTime dataInicio = DateTime.Now;
        bool indicaErro = false;
        //
        CotacaoIndice cotacaoIndice = new CotacaoIndice();
        //
        try {
            cotacaoIndice.CarregaCotacaoIndiceIMA(data);
        }
        catch (Exception e1) {
            indicaErro = true;
            //
            string msg = "IMA com problemas - Data: " + data.ToShortDateString() + "\n";
            msg += " - Dados não Importados: " + e1.Message;
            //
            this.MensagemErroInternet.AppendLine("\t" + msg);
            //mensagem.AppendLine();
        }

        #region Log do Processo
        DateTime dataFim = DateTime.Now;
        //
        if (!indicaErro) {
            HistoricoLog historicoLog = new HistoricoLog();
            historicoLog.Descricao = "Importação IMA - " + data.ToShortDateString();
            historicoLog.Login = HttpContext.Current.User.Identity.Name;
            historicoLog.Maquina = Utilitario.GetLocalIp();
            historicoLog.Cultura = "";
            historicoLog.DataInicio = dataInicio;
            historicoLog.DataFim = dataFim;
            historicoLog.Origem = (int)HistoricoLogOrigem.Outros;
            historicoLog.Save();
        }
        //
        #endregion

        #endregion    
    }    
}