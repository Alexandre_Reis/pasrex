﻿using System;
using System.Collections.Generic;
using System.IO;

using EntitySpaces.Interfaces;

using DevExpress.Web;
using Financial.Web.Common;
using Financial.Bolsa;
using Financial.Security;
using Financial.Security.Enums;
using System.Web;
using Financial.Util;
using Financial.Common;
using Financial.RendaFixa;
using Financial.Common.Enums;

/// <summary>
/// Classe para Importacao do Andima Mercado, Andima Indice, Andima238 e Andima550
/// </summary>
public partial class ImportacaoBasePage : BasePage {

    #region Chamadores

    /// <summary>
    /// Importa AndimaMercado no periodo Indicado: entre DataInicio e DataFim
    /// </summary>
    /// <param name="dataInicio"></param>
    /// <param name="dataFim"></param>
    /// <param name="path"></param>
    protected void ImportaAndimaMercado(DateTime dataInicio, DateTime dataFim, string path) {
        DateTime data = dataInicio;

        for (; data <= dataFim; ) {
            this.ImportaAndimaMercado(data, path);
            //
            data = Calendario.AdicionaDiaUtil(data, 1, LocalFeriadoFixo.Brasil, TipoFeriado.Brasil);
        }
    }

    /// <summary>
    /// Importa AndimaIndice no periodo Indicado: entre DataInicio e DataFim
    /// </summary>
    /// <param name="dataInicio"></param>
    /// <param name="dataFim"></param>
    /// <param name="path"></param>
    protected void ImportaCotacaoAndimaIndice(DateTime dataInicio, DateTime dataFim, string path)
    {

        DateTime data = dataInicio;

        for (; data <= dataFim; ) {
            this.ImportaCotacaoAndimaIndice(data, path);
            //
            data = Calendario.AdicionaDiaUtil(data, 1, LocalFeriadoFixo.Brasil, TipoFeriado.Brasil);
        }
    }

    /// <summary>
    /// Importa Andima238 no periodo Indicado: entre DataInicio e DataFim
    /// </summary>
    /// <param name="dataInicio"></param>
    /// <param name="dataFim"></param>
    /// <param name="path"></param>
    protected void ImportaAndima238(DateTime dataInicio, DateTime dataFim, string path) {

        DateTime data = dataInicio;

        for (; data <= dataFim; ) {
            this.ImportaAndima238(data, path);
            //
            data = Calendario.AdicionaDiaUtil(data, 1, LocalFeriadoFixo.Brasil, TipoFeriado.Brasil);
        }
    }

    /// <summary>
    /// Importa Andima550 no periodo Indicado: entre DataInicio e DataFim
    /// </summary>
    /// <param name="dataInicio"></param>
    /// <param name="dataFim"></param>
    /// <param name="path"></param>
    protected void ImportaAndima550(DateTime dataInicio, DateTime dataFim, string path) {

        DateTime data = dataInicio;

        for (; data <= dataFim; ) {
            this.ImportaAndima550(data, path);
            //
            data = Calendario.AdicionaDiaUtil(data, 1, LocalFeriadoFixo.Brasil, TipoFeriado.Brasil);
        }
    }

    #endregion

    /// <summary>
    /// 
    /// </summary>
    /// <param name="data"></param>
    /// <param name="path"></param>
    private void ImportaAndimaMercado(DateTime data, string path) {
        #region AndimaMercado

        DateTime dataInicio = DateTime.Now;
        bool indicaErro = false;
        //
        CotacaoMercadoAndima cotacaoMercadoAndima = new CotacaoMercadoAndima();
        //
        try {
            cotacaoMercadoAndima.CarregaAndima(data, path);
        }
        catch (Exception e1) {
            indicaErro = true;
            //
            string msg = "AndimaMercado com problemas - Data: " + data.ToShortDateString() + "\n";
            msg += " - Dados não Importados: " + e1.Message;
            //
            this.MensagemErroInternet.AppendLine("\t" + msg);
            //mensagem.AppendLine();
        }

        #region Log do Processo
        DateTime dataFim = DateTime.Now;
        //
        if (!indicaErro) {
            HistoricoLog historicoLog = new HistoricoLog();
            historicoLog.Descricao = "Importação Andima Mercado - " + data.ToShortDateString();
            historicoLog.Login = HttpContext.Current.User.Identity.Name;
            historicoLog.Maquina = Utilitario.GetLocalIp();
            historicoLog.Cultura = "";
            historicoLog.DataInicio = dataInicio;
            historicoLog.DataFim = dataFim;
            historicoLog.Origem = (int)HistoricoLogOrigem.Outros;
            historicoLog.Save();
        }
        //
        #endregion

        #endregion
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="data"></param>
    private void ImportaCotacaoAndimaIndice(DateTime data, string path)
    {

        DateTime dataInicio = DateTime.Now;
        bool indicaErro = false;
        //
        CotacaoIndice cotacaoIndice = new CotacaoIndice();
        //
        try {
            cotacaoIndice.CarregaCotacaoIndiceAndima(data, path);
        }
        catch (Exception e1) {
            indicaErro = true;
            //
            string msg = "Cotações Andima com problemas - Data: " + data.ToShortDateString() + "\n";
            msg += " - Dados não Importados: " + e1.Message;
            //
            this.MensagemErroInternet.AppendLine("\t" + msg);
        }

        #region Log do Processo
        DateTime dataFim = DateTime.Now;
        //
        if (!indicaErro) {
            HistoricoLog historicoLog = new HistoricoLog();
            historicoLog.Descricao = "Importação CotacaoAndimaIndice - " + data.ToShortDateString();
            historicoLog.Login = HttpContext.Current.User.Identity.Name;
            historicoLog.Maquina = Utilitario.GetLocalIp();
            historicoLog.Cultura = "";
            historicoLog.DataInicio = dataInicio;
            historicoLog.DataFim = dataFim;
            historicoLog.Origem = (int)HistoricoLogOrigem.Outros;
            historicoLog.Save();
        }
        //
        #endregion
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="data"></param>
    /// <param name="path"></param>
    private void ImportaAndima238(DateTime data, string path) {
        #region Andima238

        DateTime dataInicio = DateTime.Now;
        bool indicaErro = false;
        //
        CotacaoResolucao238 cotacaoResolucao238 = new CotacaoResolucao238();
        //
        try {
            cotacaoResolucao238.CarregaRes238(data, path);
        }
        catch (Exception e1) {
            indicaErro = true;
            //
            string msg = "Andima238 com problemas - Data: " + data.ToShortDateString() + "\n";
            msg += " - Dados não Importados: " + e1.Message;
            //                   
            this.MensagemErroInternet.AppendLine("\t" + msg);
            //mensagem.AppendLine();
        }

        #region Log do Processo
        DateTime dataFim = DateTime.Now;
        //
        if (!indicaErro) {
            HistoricoLog historicoLog = new HistoricoLog();
            historicoLog.Descricao = "Importação Andima 238 - " + data.ToShortDateString();
            historicoLog.Login = HttpContext.Current.User.Identity.Name;
            historicoLog.Maquina = Utilitario.GetLocalIp();
            historicoLog.Cultura = "";
            historicoLog.DataInicio = dataInicio;
            historicoLog.DataFim = dataFim;
            historicoLog.Origem = (int)HistoricoLogOrigem.Outros;
            historicoLog.Save();
        }
        //
        #endregion

        #endregion
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="data"></param>
    /// <param name="path"></param>
    private void ImportaAndima550(DateTime data, string path) {
        #region Andima550
        //
        DateTime dataInicio = DateTime.Now;
        bool indicaErro = false;
        //
        CotacaoResolucao550 cotacaoResolucao550 = new CotacaoResolucao550();
        try {
            cotacaoResolucao550.CarregaRes550(data, path);
        }
        catch (Exception e1) {
            indicaErro = true;
            //
            string msg = "Andima550 com problemas - Data: " + data.ToShortDateString() + "\n";
            msg += " - Dados não Importados: " + e1.Message;
            //
            this.MensagemErroInternet.AppendLine("\t" + msg);
            //mensagem.AppendLine();
        }

        #region Log do Processo
        DateTime dataFim = DateTime.Now;
        //
        if (!indicaErro) {
            HistoricoLog historicoLog = new HistoricoLog();
            historicoLog.Descricao = "Importação Andima 550 - " + data.ToShortDateString();
            historicoLog.Login = HttpContext.Current.User.Identity.Name;
            historicoLog.Maquina = Utilitario.GetLocalIp();
            historicoLog.Cultura = "";
            historicoLog.DataInicio = dataInicio;
            historicoLog.DataFim = dataFim;
            historicoLog.Origem = (int)HistoricoLogOrigem.Outros;
            historicoLog.Save();
        }
        //
        #endregion

        #endregion
    }
}