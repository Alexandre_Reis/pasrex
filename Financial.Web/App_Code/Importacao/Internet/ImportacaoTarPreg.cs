﻿using System;
using System.Collections.Generic;
using System.IO;

using EntitySpaces.Interfaces;

using DevExpress.Web;
using Financial.Web.Common;
using Financial.Bolsa;
using Financial.Security;
using Financial.Security.Enums;
using System.Web;
using Financial.Util;
using Financial.Common;
using Financial.BMF;
using Financial.Common.Enums;

public partial class ImportacaoBasePage : BasePage {

    /// <summary>
    /// Importa TarPreg no periodo Indicado: entre DataInicio e DataFim
    /// </summary>
    /// <param name="dataInicio"></param>
    /// <param name="dataFim"></param>
    /// <param name="path"></param>
    protected void ImportaTarPreg(DateTime dataInicio, DateTime dataFim, string path) {

        DateTime data = dataInicio;

        for (; data <= dataFim; ) {
            this.ImportaTarPreg(data, path);
            //
            data = Calendario.AdicionaDiaUtil(data, 1, LocalFeriadoFixo.Bovespa, TipoFeriado.Brasil);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="data"></param>
    /// <param name="path"></param>
    private void ImportaTarPreg(DateTime data, string path) {
        #region TarPreg
        bool indicaErro = false;

        DateTime dataInicio = DateTime.Now;
        //
        TabelaCustosBMF tabelaCustosBMF = new TabelaCustosBMF();
        //
        try {
            tabelaCustosBMF.CarregaTarPreg(data, path);
        }
        catch (Exception e1) {
            indicaErro = true;
            string msg = "TarPreg com problemas - Data: " + data.ToShortDateString() + "\n";
            msg += " - Dados não Importados: " + e1.Message;
            
            this.MensagemErroInternet.AppendLine("\t" + msg);
            //mensagem.AppendLine();
        }

        #region Log do Processo
        if (!indicaErro) {
            DateTime dataFim = DateTime.Now;
            //
            HistoricoLog historicoLog = new HistoricoLog();
            historicoLog.Descricao = "Importação TarPreg - " + data.ToShortDateString();
            historicoLog.Login = HttpContext.Current.User.Identity.Name;
            historicoLog.Maquina = Utilitario.GetLocalIp();
            historicoLog.Cultura = "";
            historicoLog.DataInicio = dataInicio;
            historicoLog.DataFim = dataFim;
            historicoLog.Origem = (int)HistoricoLogOrigem.Outros;
            historicoLog.Save();
            //
        }
        #endregion
        
        #endregion
    }
}