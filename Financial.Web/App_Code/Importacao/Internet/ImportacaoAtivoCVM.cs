﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Web;

using EntitySpaces.Interfaces;

using DevExpress.Web;
using Financial.Web.Common;
using Financial.Security;
using Financial.Util;
using Financial.Security.Enums;
using System.Data;
using System.Text;
using Financial.Export;
using Financial.Bolsa;
using Financial.BMF;
using Financial.RendaFixa;
using Financial.Bolsa.Enums;

public partial class ImportacaoBasePage : BasePage {

    #region Classe DataHolder dos Dados de um Arquivo de Lista de Ativos
    /// <summary>
    /// Armazena os Valores para Update de AtivoBolsa - AtivoBMF:
    /// DATA INÍCIO VIGÊNCIA,
    /// DATA FIM VIGÊNCIA,
    /// CodigoCDA
    /// </summary>
    [Serializable]
    public class DataHolderAtivoBolsaBMF {
        public List<int> codAProcessar = null; 
        public List<int> tipoBolsa = null;
        public List<int> tipoBMF = null;
        public List<int> tipoRendaFixa = null;

        /// <summary>
        /// Instancia as lista de codigos de Ativos Bolsa / Ativos BMF e Renda Fixa
        /// </summary>
        public DataHolderAtivoBolsaBMF() {
            #region Codigos A Processar
            this.codAProcessar = new List<int>(new int[] {
                            /* Bolsa */                            
                            (int)CDA.TipoAtivoCDA.AcaoOrdinaria, (int)CDA.TipoAtivoCDA.AcaoPreferencial,
                            (int)CDA.TipoAtivoCDA.OpcaoCompra, (int)CDA.TipoAtivoCDA.OpcaoVenda,
                            //
                            (int)CDA.TipoAtivoCDA.CertificadoDepositoAcoes, (int)CDA.TipoAtivoCDA.ReciboDepositoAcoes, 
                            (int)CDA.TipoAtivoCDA.AcaoReciboAcaoMercosul, (int)CDA.TipoAtivoCDA.BonusSubscricao, 
                            (int)CDA.TipoAtivoCDA.DepositoryReceiptExterior, (int)CDA.TipoAtivoCDA.ReciboSubscricao, 
                            (int)CDA.TipoAtivoCDA.BonusPrivado, (int)CDA.TipoAtivoCDA.FIImobiliario, 
                            //
                            (int)CDA.TipoAtivoCDA.BDRNivelI, (int)CDA.TipoAtivoCDA.BDRNivelII, (int)CDA.TipoAtivoCDA.BDRNivelIII, 

                            /* BMF */
                            (int)CDA.TipoAtivoCDA.ContratoFuturo, (int)CDA.TipoAtivoCDA.OutrosInstrumentosDerivativos,
                            //
                            (int)CDA.TipoAtivoCDA.Futuro_ALA, (int)CDA.TipoAtivoCDA.Futuro_B19, (int)CDA.TipoAtivoCDA.Futuro_B34, 
                            (int)CDA.TipoAtivoCDA.Futuro_B40, (int)CDA.TipoAtivoCDA.Futuro_BGI, (int)CDA.TipoAtivoCDA.Futuro_BRI, 
                            (int)CDA.TipoAtivoCDA.Futuro_BZE, (int)CDA.TipoAtivoCDA.Futuro_CNI, (int)CDA.TipoAtivoCDA.Futuro_COT, 
                            (int)CDA.TipoAtivoCDA.Futuro_DAP, (int)CDA.TipoAtivoCDA.Futuro_DDI, (int)CDA.TipoAtivoCDA.Futuro_DI1, 
                            (int)CDA.TipoAtivoCDA.Futuro_DOL, (int)CDA.TipoAtivoCDA.Futuro_EUR, (int)CDA.TipoAtivoCDA.Futuro_IAP, 
                            (int)CDA.TipoAtivoCDA.Futuro_ICF, (int)CDA.TipoAtivoCDA.Futuro_IND, (int)CDA.TipoAtivoCDA.Futuro_ISU, 
                            (int)CDA.TipoAtivoCDA.Futuro_OZ1, (int)CDA.TipoAtivoCDA.Futuro_SOJ, (int)CDA.TipoAtivoCDA.Futuro_T10, 
                            (int)CDA.TipoAtivoCDA.Futuro_WDL, (int)CDA.TipoAtivoCDA.Futuro_WIN,

                            /* RendaFixa */
                            (int)CDA.TipoAtivoCDA.DebentureSimples, (int)CDA.TipoAtivoCDA.DebentureConversivel
                        });
            #endregion

            #region Lista de Bolsa
            this.tipoBolsa = new List<int>(new int[] {
                            /* Bolsa */                            
                            (int)CDA.TipoAtivoCDA.AcaoOrdinaria, (int)CDA.TipoAtivoCDA.AcaoPreferencial,
                            (int)CDA.TipoAtivoCDA.OpcaoCompra, (int)CDA.TipoAtivoCDA.OpcaoVenda,
                            //
                            (int)CDA.TipoAtivoCDA.CertificadoDepositoAcoes, (int)CDA.TipoAtivoCDA.ReciboDepositoAcoes, 
                            (int)CDA.TipoAtivoCDA.AcaoReciboAcaoMercosul, (int)CDA.TipoAtivoCDA.BonusSubscricao, 
                            (int)CDA.TipoAtivoCDA.DepositoryReceiptExterior, (int)CDA.TipoAtivoCDA.ReciboSubscricao, 
                            (int)CDA.TipoAtivoCDA.BonusPrivado, (int)CDA.TipoAtivoCDA.FIImobiliario, 
                            //
                            (int)CDA.TipoAtivoCDA.BDRNivelI, (int)CDA.TipoAtivoCDA.BDRNivelII, (int)CDA.TipoAtivoCDA.BDRNivelIII
                        });

            #endregion

            #region Lista de BMF
            this.tipoBMF = new List<int>(new int[] {
                            /* BMF */
                            (int)CDA.TipoAtivoCDA.ContratoFuturo, (int)CDA.TipoAtivoCDA.OutrosInstrumentosDerivativos,
                            //
                            (int)CDA.TipoAtivoCDA.Futuro_ALA, (int)CDA.TipoAtivoCDA.Futuro_B19, (int)CDA.TipoAtivoCDA.Futuro_B34, 
                            (int)CDA.TipoAtivoCDA.Futuro_B40, (int)CDA.TipoAtivoCDA.Futuro_BGI, (int)CDA.TipoAtivoCDA.Futuro_BRI, 
                            (int)CDA.TipoAtivoCDA.Futuro_BZE, (int)CDA.TipoAtivoCDA.Futuro_CNI, (int)CDA.TipoAtivoCDA.Futuro_COT, 
                            (int)CDA.TipoAtivoCDA.Futuro_DAP, (int)CDA.TipoAtivoCDA.Futuro_DDI, (int)CDA.TipoAtivoCDA.Futuro_DI1, 
                            (int)CDA.TipoAtivoCDA.Futuro_DOL, (int)CDA.TipoAtivoCDA.Futuro_EUR, (int)CDA.TipoAtivoCDA.Futuro_IAP, 
                            (int)CDA.TipoAtivoCDA.Futuro_ICF, (int)CDA.TipoAtivoCDA.Futuro_IND, (int)CDA.TipoAtivoCDA.Futuro_ISU, 
                            (int)CDA.TipoAtivoCDA.Futuro_OZ1, (int)CDA.TipoAtivoCDA.Futuro_SOJ, (int)CDA.TipoAtivoCDA.Futuro_T10, 
                            (int)CDA.TipoAtivoCDA.Futuro_WDL, (int)CDA.TipoAtivoCDA.Futuro_WIN
                        });
            #endregion
           
            #region Lista de RendaFixa
            this.tipoRendaFixa = new List<int>(new int[] {
                            (int)CDA.TipoAtivoCDA.DebentureSimples, (int)CDA.TipoAtivoCDA.DebentureConversivel
                        });

            #endregion
        }

        /// <summary>
        /// Faz o Download dos Ativos e Carrega na Lista de Ativos
        /// </summary>
        /// <param name="path">Path Completo do Diretorio onde o Arquivo será Procurado</param>
        /// <returns>Datatable Lista com os ativos</returns>
        /// <exception cref="Exception">Se download falhou ou Processamento deu errado</exception>                
        public DataTable ProcessaAtivo(string path) {
            bool estado = this.DownloadListaAtivos(path);

            if (!estado) {
                throw new Exception("Download falhou. Arquivo não encontrado");
            }

            DataTable dataTable = new DataTable();

            try {
                #region Lê arquivo e Monta um DataTable
                string arq = path + "ListaAtivos.txt";
                //

                /* CodigoAtivo/ DataInicioVigencia / DataFimVigencia / CodigoTipoAtivo */
                dataTable.Columns.Add("CodigoAtivo", typeof(System.String)); // coluna 2 do arquivo
                dataTable.Columns.Add("DataInicioVigencia", typeof(System.DateTime)); // coluna 4 do arquivo
                dataTable.Columns.Add("DataFimVigencia", typeof(System.DateTime)); // coluna 5 do arquivo
                dataTable.Columns.Add("CodigoTipoAtivo", typeof(System.Int32)); // coluna 8 do arquivo
                dataTable.Columns.Add("DescricaoAtivo", typeof(System.String)); // coluna 3 do arquivo
                                              
                int j = 0;
                foreach (string line in File.ReadAllLines(arq, Encoding.GetEncoding("ISO-8859-1") ) ) {
                    if (j != 0) {

                        string[] colunas = line.Split(new Char[] { ';' }); // delimitado por ponto e virgula
                        //
                        int codigoTipoAtivo = Convert.ToInt32(colunas[8].Trim());
                        
                        DateTime? dataInicio = null;
                        if ( !Convert.IsDBNull(colunas[4].Trim()) && !String.IsNullOrEmpty(colunas[4].Trim()) ) {                            
                            dataInicio = Convert.ToDateTime(colunas[4].Trim());
                        }

                        DateTime? dataFim = null;
                        if ( !Convert.IsDBNull(colunas[5].Trim()) && !String.IsNullOrEmpty(colunas[5].Trim()) ) {
                            dataFim = Convert.ToDateTime(colunas[5].Trim());
                        }

                        if (this.codAProcessar.Contains(codigoTipoAtivo)) {

                            dataTable.Rows.Add(colunas[2].Trim(),
                                               dataInicio,
                                               dataFim,
                                               Convert.ToInt32(colunas[8].Trim()),
                                               colunas[3].Trim()                                                
                                              );
                        }
                    }
                    //
                    j++;
                }

                #endregion
            }
            catch (Exception e) {
                Console.WriteLine(e.Message);
                throw new Exception(e.Message);
            }

            return dataTable;
        }
        
        /// <summary>
        /// Baixa o Arquivo de Ativos da Internet do Site da CVM
        /// Descompacta o Arquivo Zip
        /// </summary>
        /// <param name="path">Caminho Destino</param>
        /// <returns></returns>
        private bool DownloadListaAtivos(string path) {

            #region Download
            string endereco = "http://sistemas.cvm.gov.br/cvmweb/ASP/ListaAtivos.zip";
            
            string pathArquivoDestino = path + "ListaAtivos.zip";
            //
            bool download = Utilitario.DownloadFile(endereco, pathArquivoDestino);
            #endregion

            #region Descompacta
            // Se foi feito o Download, descompacta o arquivo

            if (download) {
                bool descompacta = Utilitario.UnzipFile("ListaAtivos.zip", path);
                if (descompacta) {
                    // Se arquivo existe retorna true            
                    return File.Exists(path + "ListaAtivos.txt");
                }
                else {
                    return false;
                }
            }
            else {
                return false;
            }
            #endregion
        }
    }
    #endregion


    public class UpdateCodigoCDA {
        public void CarregaCodigoCDA(string pathArquivo) {
            DataHolderAtivoBolsaBMF d = new DataHolderAtivoBolsaBMF();

            DataTable dt = d.ProcessaAtivo(pathArquivo);

            AtivoBolsaCollection ativoBolsaCollection = new AtivoBolsaCollection();
            AtivoBMFCollection ativoBMFCollection = new AtivoBMFCollection();
            TituloRendaFixaCollection tituloRendaFixaCollection = new TituloRendaFixaCollection();
            //            

            //StringBuilder s = RetornaCodigoAtivos(dt);
            ////
            //AtivoBolsaCollection ativosBolsa = new AtivoBolsaCollection();
            //ativosBolsa.Query.Select(ativosBolsa.Query.CdAtivoBolsa, ativosBolsa.Query.TipoMercado, ativosBolsa.Query.CodigoCDA, 
            //                         ativosBolsa.Query.TipoPapel, ativosBolsa.Query.DataInicioVigencia, ativosBolsa.Query.DataFimVigencia);
            //ativosBolsa.Query.Where(ativosBolsa.Query.CdAtivoBolsa.In(s.ToString()));
            ////

            //try {
            //    ativosBolsa.Query.Load();
            //}
            //catch (Exception e2) {

            //    string sql = ativosBolsa.Query.es.LastQuery;
            //}
            
            for (int i = 0; i < dt.Rows.Count; i++) {
                
                string cod = dt.Rows[i]["CodigoAtivo"].ToString();

                DateTime? dataInicio = null;
                if (!Convert.IsDBNull(dt.Rows[i]["DataInicioVigencia"])) {
                    dataInicio = Convert.ToDateTime(dt.Rows[i]["DataInicioVigencia"]);
                }
                DateTime? dataFim = null;
                if (!Convert.IsDBNull(dt.Rows[i]["DataFimVigencia"])) {
                    dataFim = Convert.ToDateTime(dt.Rows[i]["DataFimVigencia"]);
                }
                int codigoAtivo = Convert.ToInt32(dt.Rows[i]["CodigoTipoAtivo"]); 
                //

                if(d.tipoBolsa.Contains(codigoAtivo)) {
                    #region Ativo Bolsa
                    //                    
                    //StringBuilder filtro = new StringBuilder();
                    //filtro.Append("" + AtivoBolsaMetadata.ColumnNames.CdAtivoBolsa + " = '" + cod + "'");
                    //ativosBolsa.Filter = filtro.ToString();

                    //if (ativosBolsa.Count == 1) {
                    //    AtivoBolsa a = ativosBolsa[0];

                    AtivoBolsa a = new AtivoBolsa();
                    //
                    a.Query.Select(a.Query.CdAtivoBolsa, a.Query.TipoMercado, a.Query.CodigoCDA,
                                   a.Query.TipoPapel, a.Query.DataInicioVigencia, a.Query.DataFimVigencia)
                            .Where(a.Query.CdAtivoBolsa == cod);

                    if (a.Query.Load()) {

                        bool atualizar = false;

                        if(dataInicio.HasValue) {
                            atualizar = dataInicio >= a.DataInicioVigencia ? true : false; 
                        }
                        if (atualizar)
                        {

                            a.DataInicioVigencia = dataInicio;

                            if (dataFim.HasValue)
                            {
                                a.DataFimVigencia = dataFim;
                            }
                            a.CodigoCDA = codigoAtivo;
                            //

                            #region TipoMercado/TipoPapel
                            if (codigoAtivo == (int)CDA.TipoAtivoCDA.FIImobiliario)
                            {
                                a.TipoMercado = TipoMercadoBolsa.Imobiliario;
                                a.TipoPapel = (byte)TipoPapelAtivo.Normal;
                            }

                            if (codigoAtivo == (int)CDA.TipoAtivoCDA.AcaoOrdinaria ||
                                codigoAtivo == (int)CDA.TipoAtivoCDA.AcaoPreferencial)
                            {
                                a.TipoMercado = TipoMercadoBolsa.MercadoVista;
                            }

                            if (codigoAtivo == (int)CDA.TipoAtivoCDA.OpcaoCompra)
                            {
                                a.TipoMercado = TipoMercadoBolsa.OpcaoCompra;
                            }

                            if (codigoAtivo == (int)CDA.TipoAtivoCDA.OpcaoVenda)
                            {
                                a.TipoMercado = TipoMercadoBolsa.OpcaoVenda;
                            }

                            if (codigoAtivo == (int)CDA.TipoAtivoCDA.CertificadoDepositoAcoes)
                            {
                                a.TipoMercado = TipoMercadoBolsa.MercadoVista;
                                a.TipoPapel = (byte)TipoPapelAtivo.CertificadoDepositoAcoes;
                            }

                            if (codigoAtivo == (int)CDA.TipoAtivoCDA.ReciboDepositoAcoes)
                            {
                                a.TipoMercado = TipoMercadoBolsa.MercadoVista;
                                a.TipoPapel = (byte)TipoPapelAtivo.ReciboDepositoAcoes;
                            }

                            if (codigoAtivo == (int)CDA.TipoAtivoCDA.AcaoReciboAcaoMercosul)
                            {
                                a.TipoMercado = TipoMercadoBolsa.MercadoVista;
                            }

                            if (codigoAtivo == (int)CDA.TipoAtivoCDA.BonusSubscricao)
                            {
                                a.TipoMercado = TipoMercadoBolsa.MercadoVista;
                                a.TipoPapel = (byte)TipoPapelAtivo.BonusSubscricao;
                            }

                            if (codigoAtivo == (int)CDA.TipoAtivoCDA.DepositoryReceiptExterior)
                            {
                                a.TipoMercado = TipoMercadoBolsa.MercadoVista;
                            }

                            if (codigoAtivo == (int)CDA.TipoAtivoCDA.ReciboSubscricao)
                            {
                                a.TipoMercado = TipoMercadoBolsa.MercadoVista;
                                a.TipoPapel = (byte)TipoPapelAtivo.ReciboSubscricao;
                            }

                            if (codigoAtivo == (int)CDA.TipoAtivoCDA.BonusPrivado)
                            {
                                a.TipoMercado = TipoMercadoBolsa.MercadoVista;
                            }

                            if (codigoAtivo == (int)CDA.TipoAtivoCDA.BDRNivelI)
                            {
                                a.TipoMercado = TipoMercadoBolsa.MercadoVista;
                                a.TipoPapel = (byte)TipoPapelAtivo.BDRNivelI;
                            }

                            if (codigoAtivo == (int)CDA.TipoAtivoCDA.BDRNivelII)
                            {
                                a.TipoMercado = TipoMercadoBolsa.MercadoVista;
                                a.TipoPapel = (byte)TipoPapelAtivo.BDRNivelII;
                            }

                            if (codigoAtivo == (int)CDA.TipoAtivoCDA.BDRNivelIII)
                            {
                                a.TipoMercado = TipoMercadoBolsa.MercadoVista;
                                a.TipoPapel = (byte)TipoPapelAtivo.BDRNivelIII;
                            }

                            #endregion

                            // attach entity
                            ativoBolsaCollection.AttachEntity(a);
                        }
                    }

                    // Zera o Filtro
                    //ativosBolsa.Filter = "";
                    #endregion
                }
                else if(d.tipoBMF.Contains(codigoAtivo)) {
                    #region Ativo BMF
                    // exemplo: BGIFUTSET6

                    string codFuturo = cod.Substring(0,3) + cod.Substring(6);
                    string codigo = cod.Substring(0,3);
                    string serie = cod.Substring(6);
                   
                    AtivoBMF b = new AtivoBMF();

                    b.Query.Select(b.Query.CdAtivoBMF, b.Query.Serie, b.Query.CodigoCDA,
                                  b.Query.TipoMercado, b.Query.DataInicioVigencia, b.Query.DataFimVigencia)
                           .Where(b.Query.CdAtivoBMF == codigo &&
                                  b.Query.CdAtivoBMF == serie);

                    if (b.Query.Load()) {
                        if(dataInicio.HasValue) {
                            b.DataInicioVigencia = dataInicio;
                        }
                        if(dataFim.HasValue) {
                            b.DataFimVigencia = dataFim;
                        }
                        b.CodigoCDA = codigoAtivo;
                        //                        
                        // attach entity
                        ativoBMFCollection.AttachEntity(b);        
                    }

                    #endregion
                }
                else if(d.tipoRendaFixa.Contains(codigoAtivo)) {
                    #region Renda Fixa
                    string descricaoAtivo = dt.Rows[i]["DescricaoAtivo"].ToString();

                    TituloRendaFixaCollection coll = new TituloRendaFixaCollection();

                    coll.Query.Select(coll.Query.IdTitulo, coll.Query.IdPapel, coll.Query.CodigoCDA)
                              .Where(coll.Query.DescricaoCompleta == descricaoAtivo);

                    if (coll.Query.Load() ) {
                        coll[0].CodigoCDA = codigoAtivo;                        
                        
                        // attach entity
                        tituloRendaFixaCollection.AttachEntity(coll[0]);
                    }
                    #endregion
                }
            }

            using (esTransactionScope scope = new esTransactionScope()) {
                ativoBolsaCollection.Save();
                ativoBMFCollection.Save();
                tituloRendaFixaCollection.Save();
                //
                scope.Complete();
            }
        }

        /// <summary>
        /// Retorna a Concatenação da Coluna "CodigoAtivo" do Datatable - Codigo dos Ativos
        /// </summary>
        /// <param name="dt"></param>
        /// <returns></returns>
        [Obsolete("Não Usado")]
        public StringBuilder RetornaCodigoAtivos(DataTable dt) {
            
            #region String de Ativos
            StringBuilder cdAtivos = new StringBuilder(10000000);
            //
            for (int i = 0; i < dt.Rows.Count; i++) {
                string cdAtivo = dt.Rows[i]["CodigoAtivo"].ToString();

                if (i == 0) {
                    cdAtivos.Append(cdAtivo + "'");
                }
                else {
                    int ultimo = dt.Rows.Count - 1;
                    if (i == ultimo) {
                        cdAtivos.Append(", " + "'" + cdAtivo);
                    }
                    else {
                        cdAtivos.Append(", " + "'" + cdAtivo + "'");
                    }
                }
            }
            #endregion

            return cdAtivos;
        }
    }

    /// <summary>
    /// Importa Lista Ativo CVM
    /// </summary>
    /// <param name="path"></param>
    protected void ImportaListaAtivoCVM(string path) {
        #region ListaAtivoCVM
        DateTime dataInicio = DateTime.Now;
        bool indicaErro = false;
        //
        UpdateCodigoCDA d = new UpdateCodigoCDA();

        string mensagemErroListaAtivoCVM = "";
        try {
            d.CarregaCodigoCDA(path);
        }
        catch (Exception e1) {
            indicaErro = true;
            mensagemErroListaAtivoCVM = "Lista Ativo CVM com problemas.\n";
            mensagemErroListaAtivoCVM += " - Erro: " + e1.Message;
        }

        if (!String.IsNullOrEmpty(mensagemErroListaAtivoCVM)) {
            this.mensagem.AppendLine("\t" + mensagemErroListaAtivoCVM);
        }

        #region Log do Processo
        DateTime dataFim = DateTime.Now;
        //
        if (!indicaErro) {
            HistoricoLog historicoLog = new HistoricoLog();
            historicoLog.Descricao = "Importação Lista Ativo CVM - ";
            historicoLog.Login = HttpContext.Current.User.Identity.Name;
            historicoLog.Maquina = Utilitario.GetLocalIp();
            historicoLog.Cultura = "";
            historicoLog.DataInicio = dataInicio;
            historicoLog.DataFim = dataFim;
            historicoLog.Origem = (int)HistoricoLogOrigem.Outros;
            historicoLog.Save();
        }
        #endregion

        #endregion
    }
}