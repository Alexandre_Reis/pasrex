﻿using System;
using System.Collections.Generic;
using System.IO;

using EntitySpaces.Interfaces;

using DevExpress.Web;
using Financial.Web.Common;
using Financial.Bolsa;
using Financial.Security;
using Financial.Security.Enums;
using System.Web;
using Financial.Util;
using Financial.Common;
using Financial.BMF;
using Financial.Common.Enums;

public partial class ImportacaoBasePage : BasePage {

    /// <summary>
    /// Carrega Cotações Offshore - Busca do Yahoo, só traz última data disponível
    /// </summary>
    /// <param name="data"></param>
    /// <param name="path"></param>
    protected void ImportaOffshore(DateTime dataInicio, DateTime dataFim, string path) 
    {
        bool indicaErro = false;
        //
        //Para offshore, busca do Yahoo, só traz última data disponível
        DateTime dataHoje = DateTime.Now;
        DateTime dataAnterior = Calendario.SubtraiDiaUtil(dataHoje, 1, (int)LocalFeriadoFixo.NovaYork, TipoFeriado.Outros);
        //
        CotacaoBolsa cotacaoBolsa = new CotacaoBolsa();
        CotacaoBMF cotacaoBMF = new CotacaoBMF();
        CotacaoIndice cotacaoIndice = new CotacaoIndice();

        DateTime dataInicioLog = DateTime.Now;

        string mensagemErroOffShore = "";
        try {
            cotacaoBolsa.CarregaCotacaoBloomberg(dataInicio, dataFim);
            cotacaoBMF.CarregaCotacaoOffShoreBloomberg(dataInicio, dataFim);
            cotacaoIndice.CarregaCotacaoIndiceBloomberg(dataInicio, dataFim);
        }
        catch (Exception e1) {
            indicaErro = true;
            mensagemErroOffShore = "Cotações Offshore com problemas - Data: " + dataAnterior.ToShortDateString() + "\n";
            mensagemErroOffShore += " - Cotações não Importadas: " + e1.Message;
        }

        if (!String.IsNullOrEmpty(mensagemErroOffShore)) {
            this.MensagemErroInternet.AppendLine("\t" + mensagemErroOffShore);
        }

        #region Log do Processo
        if (!indicaErro) {
            DateTime dataFimLog = DateTime.Now;
            //
            HistoricoLog historicoLog = new HistoricoLog();
            historicoLog.Descricao = "Importação Cotações Offshore - " + dataAnterior.ToShortDateString();
            historicoLog.Login = HttpContext.Current.User.Identity.Name;
            historicoLog.Maquina = Utilitario.GetLocalIp();
            historicoLog.Cultura = "";
            historicoLog.DataInicio = dataInicioLog;
            historicoLog.DataFim = dataFimLog;
            historicoLog.Origem = (int)HistoricoLogOrigem.Outros;
            historicoLog.Save();
        }
        #endregion        
    
    }
}