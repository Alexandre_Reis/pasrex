﻿using System;
using System.Collections.Generic;
using System.IO;

using EntitySpaces.Interfaces;

using DevExpress.Web;
using Financial.Web.Common;
using Financial.Bolsa;
using Financial.Security;
using Financial.Security.Enums;
using System.Web;
using Financial.Util;
using Financial.Common;
using Financial.RendaFixa;
using Financial.Common.Enums;

public partial class ImportacaoBasePage : BasePage {

    /// <summary>
    /// Importa CotacaoTesouro no periodo Indicado: entre DataInicio e DataFim
    /// </summary>
    /// <param name="dataInicio"></param>
    /// <param name="dataFim"></param>
    /// <param name="path"></param>
    protected void ImportaCotacaoTesouro(DateTime dataInicio, DateTime dataFim, string path) {

        DateTime data = dataInicio;

        for (; data <= dataFim; ) {
            this.ImportaCotacaoTesouro(data, path);
            //
            data = Calendario.AdicionaDiaUtil(data, 1, LocalFeriadoFixo.Brasil, TipoFeriado.Brasil);
        }
    }
    
    /// <summary>
    /// 
    /// </summary>
    /// <param name="data"></param>
    /// <param name="path"></param>
    private void ImportaCotacaoTesouro(DateTime data, string path) {
        #region CotacaoTesouro
        
        DateTime dataInicio = DateTime.Now;
        bool indicaErro = false;
        //
        CotacaoMercadoTesouro cotacaoMercadoTesouro = new CotacaoMercadoTesouro();
        //
        try {
            cotacaoMercadoTesouro.CarregaCotacaoTesouro(data, path);
        }
        catch (Exception e1) {
            indicaErro = true;
            //
            string msg = "Cotações do Tesouro com problemas - Data: " + data.ToShortDateString() + "\n";
            msg += " - Dados não Importados: " + e1.Message;
            
            this.MensagemErroInternet.AppendLine("\t" + msg);
        }

        #region Log do Processo
        if (!indicaErro) {
            DateTime dataFim = DateTime.Now;
            //
            HistoricoLog historicoLog = new HistoricoLog();
            historicoLog.Descricao = "Importação CotacaoTesouro - " + data.ToShortDateString();
            historicoLog.Login = HttpContext.Current.User.Identity.Name;
            historicoLog.Maquina = Utilitario.GetLocalIp();
            historicoLog.Cultura = "";
            historicoLog.DataInicio = dataInicio;
            historicoLog.DataFim = dataFim;
            historicoLog.Origem = (int)HistoricoLogOrigem.Outros;
            historicoLog.Save();
            //
        }
        #endregion
    
        #endregion
    }
}