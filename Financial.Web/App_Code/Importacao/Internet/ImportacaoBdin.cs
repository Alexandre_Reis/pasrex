﻿using System;
using System.Collections.Generic;
using System.IO;

using EntitySpaces.Interfaces;

using DevExpress.Web;
using Financial.Web.Common;
using Financial.Bolsa;
using Financial.Security;
using Financial.Security.Enums;
using System.Web;
using Financial.Util;
using Financial.Common;
using Financial.Common.Enums;

public partial class ImportacaoBasePage : BasePage {

    /// <summary>
    /// Importa Bdin no periodo Indicado: entre DataInicio e DataFim
    /// </summary>
    /// <param name="dataInicio"></param>
    /// <param name="dataFim"></param>
    /// <param name="path"></param>
    protected void ImportaBdin(DateTime dataInicio, DateTime dataFim, string path) {

        DateTime data = dataInicio;

        for (; data <= dataFim; ) {
            this.ImportaBdin(data, path);
            //
            data = Calendario.AdicionaDiaUtil(data, 1, LocalFeriadoFixo.Bovespa, TipoFeriado.Brasil);
        }
        AtualizaFatorBdin();
    }

    private void AtualizaFatorBdin()
    {
        if (ParametrosConfiguracaoSistema.Outras.CalculaFatorIndice)
        {
            CotacaoIndice cotacaoIndice = new CotacaoIndice();
            List<short> indiceAtualizaFator = new List<short>();
            indiceAtualizaFator.Add(ListaIndiceFixo.IBOVESPA_MEDIO);
            indiceAtualizaFator.Add(ListaIndiceFixo.IBOVESPA_FECHA);
            indiceAtualizaFator.Add(ListaIndiceFixo.IBRX_MEDIO);
            indiceAtualizaFator.Add(ListaIndiceFixo.IBRX_FECHA);
            indiceAtualizaFator.Add(ListaIndiceFixo.IBRX50_MEDIO);
            indiceAtualizaFator.Add(ListaIndiceFixo.IBRX50_FECHA);
            indiceAtualizaFator.Add(ListaIndiceFixo.IBRA);
            indiceAtualizaFator.Add(ListaIndiceFixo.IFIX);
            indiceAtualizaFator.Add(ListaIndiceFixo.IDIVIDENDOS);
            foreach (short indice in indiceAtualizaFator)
            {
                cotacaoIndice.AtualizaFatorIndice(indice);
            }
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="data"></param>
    /// <param name="path"></param>
    private void ImportaBdin(DateTime data, string path) {
        #region Bdin
        DateTime dataInicio = DateTime.Now;
        bool indicaErro = false;
        //
        CotacaoBolsa cotacaoBolsa = new CotacaoBolsa();
        CotacaoIndice cotacaoIndice = new CotacaoIndice();

        string mensagemErroBdin = "";
        try {
            cotacaoBolsa.CarregaCotacaoBdin(data, path);
        }
        catch (Exception e1) {
            indicaErro = true;
            mensagemErroBdin = "Bdin com problemas - Data: " + data.ToShortDateString() + "\n";
            mensagemErroBdin += " - Cotações não Importadas: " + e1.Message;
        }

        try {
            cotacaoIndice.CarregaCotacaoIndiceBdin(data, path);
        }
        catch (Exception e2) {
            indicaErro = true;
            if (String.IsNullOrEmpty(mensagemErroBdin)) {
                mensagemErroBdin = "Bdin com problemas - Data: " + data.ToShortDateString() + "\n";
                mensagemErroBdin += " - Cotações Índices não Importadas: " + e2.Message;
            }
            else {
                mensagemErroBdin += " - Cotações Índices não Importadas: " + e2.Message;
            }
        }

        if (!String.IsNullOrEmpty(mensagemErroBdin)) {
            this.mensagem.AppendLine("\t" + mensagemErroBdin);
        }

        #region Log do Processo
        DateTime dataFim = DateTime.Now;
        //
        if (!indicaErro) {
            HistoricoLog historicoLog = new HistoricoLog();
            historicoLog.Descricao = "Importação BDIN - " + data.ToShortDateString();
            historicoLog.Login = HttpContext.Current.User.Identity.Name;
            historicoLog.Maquina = Utilitario.GetLocalIp();
            historicoLog.Cultura = "";
            historicoLog.DataInicio = dataInicio;
            historicoLog.DataFim = dataFim;
            historicoLog.Origem = (int)HistoricoLogOrigem.Outros;
            historicoLog.Save();
        }
        #endregion

        #endregion
    }
}