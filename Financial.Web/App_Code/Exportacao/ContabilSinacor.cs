﻿using FileHelpers;
using System;
using Financial.Contabil;
using System.Collections.Generic;
using System.IO;
using Financial.WebConfigConfiguration;
using System.Text;
using Financial.Investidor;
using Financial.Util;

namespace Financial.Export
{

    /// <summary>
    /// Summary description for ContabLancamento
    /// </summary>
    public class ContabilSinacor
    {
        const string TIPO_REGISTRO = "1";               /* Campo 1 */
        //string COD_USUARIO;                           /* Campo 2 */
        //const string DATA_REFERENCIA;                 /* Campo 3 */
        const string NUMERO_FILIAL = "00";              /* Campo 4 */
        //const string CONTA_CREDITO_RESUMIDA;          /* Campo 5 */
        const string CONTA_HISTORICO_CREDITO = "999";   /* Campo 6 */
        //const string CONTA_DEBITO_RESUMIDA;           /* Campo 7 */
        const string CONTA_HISTORICO_DEBITO = "999";    /* Campo 8 */
        //const string VALOR_LANCAMENTO;                /* Campo 9 */
        //const string TEXTO_HISTORICO_DEBITO;          /* Campo 10 */
        //const string TEXTO_HISTORICO_CREDITO;         /* Campo 11 */
        const string NUMERO_LOTE = "981";               /* Campo 12 */
        //const string CONTA_CREDITO_BACEN;             /* Campo 13 */
        //const string CONTA_DEBITO_BACEN;              /* Campo 14 */
        
        [FixedLengthRecord()]
        public class ContabLancamentoDS
        {
            //[FieldFixedLength(1)]
            //public int tipoRegistro;

            [FieldFixedLength(5)]
            [FieldAlign(AlignMode.Right, '0')]
            public int codigoUsuario; //Codigo da Corretora

            [FieldFixedLength(8)]
            [FieldConverter(ConverterKind.Date, "ddMMyyyy")]
            public DateTime dataReferencia;

            [FieldFixedLength(2)]
            [FieldAlign(AlignMode.Right, '0')]
            public int numeroFilial;
                        
            [FieldFixedLength(4)]
            public string contaCreditoResumida;

            [FieldFixedLength(3)]
            public string codigoHistoricoCredito;

            [FieldFixedLength(4)]
            public string contaDebitoResumida;

            [FieldFixedLength(3)]
            public string codigoHistoricoDebito;
                        
            [FieldFixedLength(17)]
            [FieldAlign(AlignMode.Right, '0')]
            public Int64 valorLancamento;
            
            [FieldFixedLength(50)]
            public string textoHistoricoDebito;

            [FieldFixedLength(50)]
            public string textoHistoricoCredito;

            [FieldFixedLength(3)]
            public string numeroLote;

            [FieldFixedLength(20)]
            public string contaCreditoBacen;

            [FieldFixedLength(20)]
            public string contaDebitoBacen;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="data"></param>
        /// <param name="idClientes"></param>
        /// <param name="ms">Saida: Memory Stream do arquivo ContabLançamento</param>
        /// <param name="nomeArquivo">Saida: Nome do Arquivo ContabLancamento</param>        
        public void ExportaContabLancamento(DateTime data, List<int> idClientes, out MemoryStream ms, out string nomeArquivo) {
            // out
            ms = new MemoryStream();
            nomeArquivo = "AT" + data.ToString("yyMMdd") + ".txt";

            int codigoBovespaDefault = Convert.ToInt32(ParametrosConfiguracaoSistema.Bolsa.CodigoAgenteDefault);

            #region Consulta Sql
            ContabLancamentoQuery contabLancamentoQuery = new ContabLancamentoQuery("C");
            //
            contabLancamentoQuery.Where(contabLancamentoQuery.DataLancamento.Equal(data) &&
                                        contabLancamentoQuery.IdCliente.In(idClientes));
            //
            contabLancamentoQuery.OrderBy(contabLancamentoQuery.IdCliente.Ascending);
            //
            ContabLancamentoCollection contabLancamentoCollection = new ContabLancamentoCollection();
            contabLancamentoCollection.Load(contabLancamentoQuery);

            if (contabLancamentoCollection.Count == 0)
            {
                throw new Exception("Não há dados para exportar.");
            }
            #endregion


            List<ContabLancamentoDS> contabLancamentoDS = new List<ContabLancamentoDS>();

            int j = 1;
            int k = 0;
            for (int i = 0; i < contabLancamentoCollection.Count; i++)
            {                
                #region Preenchimento do objeto do arquivo
                ContabLancamentoDS cl = new ContabLancamentoDS();

                int? idContaCredito = contabLancamentoCollection[i].IdContaCredito;
                int? idContaDebito = contabLancamentoCollection[i].IdContaDebito;

                ContabConta contabCredito = new ContabConta();
                if (idContaCredito.HasValue)
                {
                    contabCredito.LoadByPrimaryKey(idContaCredito.Value);
                }
                ContabConta contabDebito = new ContabConta();
                if (idContaDebito.HasValue)
                {
                    contabDebito.LoadByPrimaryKey(idContaDebito.Value);
                }
                
                //
                //cl.tipoRegistro = Convert.ToInt32(TIPO_REGISTRO);
                cl.codigoUsuario = codigoBovespaDefault;
                cl.dataReferencia = data;
                cl.numeroFilial = Convert.ToInt32(NUMERO_FILIAL);
                cl.contaCreditoResumida = (contabCredito.es.HasData && !String.IsNullOrEmpty(contabCredito.CodigoReduzida)) ? Utilitario.RemoveCaracteresEspeciais(contabCredito.CodigoReduzida) : "";
                cl.codigoHistoricoCredito = CONTA_HISTORICO_CREDITO;
                cl.contaDebitoResumida = (contabDebito.es.HasData && !String.IsNullOrEmpty(contabDebito.CodigoReduzida)) ? Utilitario.RemoveCaracteresEspeciais(contabDebito.CodigoReduzida) : "";
                cl.codigoHistoricoDebito = CONTA_HISTORICO_DEBITO;
                cl.valorLancamento = Convert.ToInt64(contabLancamentoCollection[i].Valor.Value * 100);
                cl.textoHistoricoDebito = contabLancamentoCollection[i].Descricao;
                cl.textoHistoricoCredito = contabLancamentoCollection[i].Descricao;
                cl.numeroLote = NUMERO_LOTE;
                cl.contaCreditoBacen = contabCredito.es.HasData ? Utilitario.RemoveCaracteresEspeciais(contabCredito.Codigo) : "";
                cl.contaDebitoBacen = contabDebito.es.HasData ? Utilitario.RemoveCaracteresEspeciais(contabDebito.Codigo) : "";
                //
                contabLancamentoDS.Add(cl);
                #endregion
                
                j++;
            }

            if (contabLancamentoDS.Count == 0) {
                throw new Exception("Não há dados para exportar.");
            }

            FileHelperEngine engine = new FileHelperEngine(typeof(ContabLancamentoDS));

            StreamWriter arquivo = new StreamWriter(ms, Encoding.GetEncoding("ISO-8859-1"));
            engine.Encoding = Encoding.GetEncoding("ISO-8859-1");
            //
            engine.WriteStream(arquivo, contabLancamentoDS);

            //Fecha o arquivo
            arquivo.Flush();
        }
    }
}