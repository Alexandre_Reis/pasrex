﻿using System.Xml.Serialization;
using System.Xml;
using System;
using System.IO;
using EntitySpaces.Interfaces;
using System.Collections.Generic;
using Financial.CRM;
using Financial.Investidor;
using Financial.Investidor.Enums;
using Financial.Relatorio;
using Financial.Fundo;
using Financial.Util;
using Financial.Fundo.Enums;
using Financial.ContaCorrente;
using Financial.ContaCorrente.Enums;
using Financial.RendaFixa;
using Financial.RendaFixa.Enums;
using Financial.Common.Enums;
using Financial.Bolsa.Enums;
using Financial.Bolsa;
using Financial.BMF;
using Financial.Fundo.Exceptions;
using Financial.Common;
using Financial.Swap;
using System.Text.RegularExpressions;

namespace Financial.Export {

    public class DesempenhoFundo {
        /// <summary>
        /// Retorna uma Stream de Mémoria de um arquivo PDF. A Stream permanece aberta
        /// </summary>
        /// <param name="dataPosicao"></param>
        /// <param name="idClientes"></param>
        /// <param name="dicMStream">Saida: Dicionario contendo Nome do arquivo e Memory Stream do arquivo </param>
        /// <returns></returns>
        public void ExportaDesempenhoFundo(DateTime data, List<int> idClientes, out Dictionary<string, MemoryStream> dicMStream) {
            dicMStream = new Dictionary<string, MemoryStream>();

            for (int i = 0; i < idClientes.Count; i++) {
                //
                MemoryStream ms = new MemoryStream();
                string mes = data.Month.ToString();
                if (mes.Length == 1) {
                    mes = "0" + mes;
                }

                string nomeArquivo = idClientes[i] + "_" + mes + data.Year.ToString() + ".pdf";
                //

                ReportDesempenhoFundo report = new ReportDesempenhoFundo(idClientes[i], data);
                report.ExportToPdf(ms);

                /* Necessário voltar o ponteiro do arquivo para o Inicio */
                ms.Seek(0, SeekOrigin.Begin);

                // Adiciona no Dicionário
                dicMStream.Add(nomeArquivo, ms);
            }
        }
    }
}