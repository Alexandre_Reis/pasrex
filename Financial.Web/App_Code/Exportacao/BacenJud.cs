﻿using System.Collections.Generic;
using System;
using Financial.Util;
using System.IO;
using Financial.Fundo;
using Financial.Common;
using Financial.InvestidorCotista;
using System.Text;
using Financial.Investidor;
using Financial.Investidor.Enums;
using Financial.Bolsa;
using Financial.Bolsa.Enums;
using Financial.CRM;

namespace Financial.Export {

    /// <summary>
    ///  Classe para fazer as Exportações Pertencentes a Fundo
    /// </summary>
    public class BacenJud {
        /// <summary>
        /// Determina o numero total de casas que um numero deve ter com zeros a esquerda
        /// </summary>
        private enum AlinhamentoEsquerda {
            [StringValue("00")]
            _2Casas = 1,

            [StringValue("0000")]
            _4Casas = 2,

            [StringValue("00000")]
            _5Casas = 3,

            [StringValue("000000")]
            _6Casas = 4,

            [StringValue("00000000")]
            _8Casas = 5,

            [StringValue("0000000000")]
            _10Casas = 6,

            [StringValue("00000000000000")]
            _14Casas = 7,
        }

        /// <summary>
        /// Valores de Detalhe
        /// </summary>
        #region Valores do Detalhe
        class ValoresDetail {
            public DateTime dataBase;            // Campo 01
            public Int64 cpfCNPJ;                // Campo 02
            public string nomeCliente;           // Campo 03            
            public string sistema;               // Campo 04                          
            public string produto;               // Campo 05                          
            public Int64 empresa;                // Campo 06                          
            public int agencia;                  // Campo 07                          
            public string nomeClube;             // Campo 08                          
            public decimal? saldo;               // Campo 09
            public decimal? saldoBloqueadoDepositoCheque;  // Campo 10
            public decimal? saldoBloqueadoOrdemJudicial;   // Campo 11
            public decimal? saldoBloqueadoGarantiaBolsa;   // Campo 12
            public decimal? saldoBloqueadoOutrosMotivos;   // Campo 13
            public string gerente;               // Campo 14
        }
        private List<ValoresDetail> valoresDetail = new List<ValoresDetail>();
        #endregion

        /// <summary>
        /// Retorna uma Stream de Mémoria de um arquivo txt. A Stream permanece aberta
        /// </summary>
        /// <param name="dataPosicao"></param>
        /// <param name="idClientes">Lista de idClientes a executar</param>
        /// <param name="ms">Saida: Memory Stream do arquivo BacenJud</param>
        /// <param name="nomeArquivo">Saida: Nome do Arquivo BanceJud</param>        
        /// <returns></returns>
        /// <exception cref="ProcessaMensalClubesException">
        /// throws ProcessaMensalClubesException se occoreu algum erro no processamento
        /// </exception>
        public void ExportaBacenJud(DateTime dataPosicao, List<int> idClientes, out MemoryStream ms, out string nomeArquivo) {
            ms = new MemoryStream();
            nomeArquivo = "";

            #region Busca as Carteiras
            CarteiraQuery carteiraQuery = new CarteiraQuery("C");
            ClienteQuery clienteQuery = new ClienteQuery("B");
            AgenteMercadoQuery agenteMercadoQuery = new AgenteMercadoQuery("A");

            carteiraQuery.Select(carteiraQuery.IdCarteira, carteiraQuery.Nome, agenteMercadoQuery.Cnpj);
            //
            carteiraQuery.InnerJoin(clienteQuery).On(carteiraQuery.IdCarteira == clienteQuery.IdCliente);
            carteiraQuery.InnerJoin(agenteMercadoQuery).On(carteiraQuery.IdAgenteAdministrador == agenteMercadoQuery.IdAgente);
            //
            carteiraQuery.Where(clienteQuery.IdTipo == TipoClienteFixo.Clube);
            carteiraQuery.Where(clienteQuery.StatusAtivo.Equal((byte)StatusAtivoCliente.Ativo));
            // Acrescentado filtro de icliente
            carteiraQuery.Where(clienteQuery.IdCliente.In(idClientes));
            //
            CarteiraCollection carteiraCollection = new CarteiraCollection();
            carteiraCollection.Load(carteiraQuery);
            #endregion

            if (carteiraCollection.HasData) {
                StringBuilder nomeArquivoAux = new StringBuilder();

                nomeArquivoAux.Append("BACENJUD_" + this.FormataValor(Convert.ToDecimal(dataPosicao.Year), AlinhamentoEsquerda._4Casas, false) +
                                                this.FormataValor(Convert.ToDecimal(dataPosicao.Month), AlinhamentoEsquerda._2Casas, false) +
                                                this.FormataValor(Convert.ToDecimal(dataPosicao.Day), AlinhamentoEsquerda._2Casas, false));
                nomeArquivoAux.Append(".txt");

                // out
                nomeArquivo = nomeArquivoAux.ToString();
                              
                for (int i = 0; i < carteiraCollection.Count; i++) {
                    int idCarteira = carteiraCollection[i].IdCarteira.Value;
                    string nomeCarteira = carteiraCollection[i].Nome;

                    Cliente cliente = new Cliente();
                    cliente.LoadByPrimaryKey(idCarteira);

                    if (cliente.DataDia.Value.CompareTo(dataPosicao) != 0) {
                        throw new ProcessaBacenJudException("Carteira " + nomeCarteira + " na data: " + cliente.DataDia.Value.ToString("d") + " está em data diferente da data passada.");
                    }
                    else if (cliente.Status != (byte)StatusCliente.Divulgado && cliente.Status != (byte)StatusCliente.Fechado) {
                        throw new ProcessaBacenJudException("Carteira " + nomeCarteira + " está com status diferente de fechado e calculado.");
                    }

                    Int64 cnpjCarteira = 0;
                    if ( !String.IsNullOrEmpty( (string)carteiraCollection[i].GetColumn(AgenteMercadoMetadata.ColumnNames.Cnpj) ) ) 
                    {
                        string stringCNPJ = Convert.ToString(carteiraCollection[i].GetColumn(AgenteMercadoMetadata.ColumnNames.Cnpj));

                        if (stringCNPJ.Length >= 8)
                        {
                            cnpjCarteira = Convert.ToInt64(stringCNPJ.Replace("-", "").Replace(" ", "").Replace(".", "").Replace("/", "").Replace("\n", "").Substring(0, 8));
                        }
                    }
                  
                    #region Pega Valores Table
                    PosicaoCotistaCollection posicaoCotistaCollection = new PosicaoCotistaCollection();
                    posicaoCotistaCollection.Query.Select(posicaoCotistaCollection.Query.IdCotista,
                                                          posicaoCotistaCollection.Query.ValorBruto.Sum(),
                                                          posicaoCotistaCollection.Query.QuantidadeBloqueada.Sum(),
                                                          posicaoCotistaCollection.Query.CotaDia.Avg());
                    posicaoCotistaCollection.Query.Where(posicaoCotistaCollection.Query.IdCarteira.Equal(idCarteira),
                                                         posicaoCotistaCollection.Query.Quantidade.NotEqual(0));
                    posicaoCotistaCollection.Query.GroupBy(posicaoCotistaCollection.Query.IdCotista);

                    posicaoCotistaCollection.Query.Load();

                    // Cada Posicao Representa uma linha no Arquivo
                    for (int j = 0; j < posicaoCotistaCollection.Count; j++) {
                        int idCotista = posicaoCotistaCollection[j].IdCotista.Value;
                        decimal saldoDisponivel = posicaoCotistaCollection[j].ValorBruto.Value;
                        decimal quantidadeBloqueada = posicaoCotistaCollection[j].QuantidadeBloqueada.Value;
                        decimal cotaDia = posicaoCotistaCollection[j].CotaDia.Value;
                        decimal saldoBloqueado = Utilitario.Truncate(quantidadeBloqueada * cotaDia, 2);

                        #region Preenche os campos
                        Pessoa pessoa = new Pessoa();
                        Cotista cotista = new Cotista();
                        cotista.LoadByPrimaryKey(idCotista);
                        pessoa.LoadByPrimaryKey(cotista.IdPessoa.Value);
                        //
                        ValoresDetail valorDetailAux = new ValoresDetail();
                        valorDetailAux.dataBase = dataPosicao;
                        //
                        valorDetailAux.cpfCNPJ = String.IsNullOrEmpty(pessoa.str.Cpfcnpj)
                                                 ? 0
                                                 : Convert.ToInt64(pessoa.Cpfcnpj.Replace("-", "").Replace(" ", "").Replace(".", "").Replace("/", "").Replace("\n", ""));

                        valorDetailAux.nomeCliente = pessoa.Nome.Length > 35 ? pessoa.Nome.Substring(0, 35) : pessoa.Nome;
                        //
                        valorDetailAux.sistema = "FN"; //FIXO para Financial
                        valorDetailAux.produto = "Cotas de Fundo"; //Fixo para envio de cotas livres e bloqueadas de cotistas
                        valorDetailAux.empresa = cnpjCarteira;
                        //
                        //Engessado para Fator!!!!
                        valorDetailAux.agencia = 27;  //Engessado para Fator!!!!
                        valorDetailAux.gerente = "FATOR_CLUBES";  //Engessado para Fator!!!!
                        //
                        valorDetailAux.nomeClube = nomeCarteira.Length > 50
                                                   ? nomeCarteira.Substring(0, 50) : nomeCarteira;                                                
                        //
                        valorDetailAux.saldo = saldoDisponivel;
                        valorDetailAux.saldoBloqueadoDepositoCheque = 0;
                        valorDetailAux.saldoBloqueadoOrdemJudicial = saldoBloqueado;
                        valorDetailAux.saldoBloqueadoGarantiaBolsa = 0;
                        valorDetailAux.saldoBloqueadoOutrosMotivos = 0;
                        
                        #endregion

                        // Adiciona os dados referentes a uma carteira
                        this.valoresDetail.Add(valorDetailAux);
                    }
                    #endregion
                }

                // 
                #region EscreveArquivo
                try {
                    //Abrir o arquivo
                    StreamWriter arquivo = new StreamWriter(ms, Encoding.ASCII);
                   
                    #region Detalhes
                    if (this.valoresDetail.Count == 0) { // 0 registros
                        throw new ProcessaBacenJudException("Não existem dados para geração do arquivo BacenJud.");
                    }

                    for (int i = 0; i < this.valoresDetail.Count; i++) {
                        this.EscreveDetalhe(arquivo, i);
                    }
                    #endregion

                    arquivo.Flush();

                    // Retorna a MemoryStream para o inicio
                    ms.Seek(0, SeekOrigin.Begin);
                }
                catch (Exception e) {
                    Console.WriteLine(e.Message);
                    string mensagem = "";
                    mensagem = "Arquivo BacenJud com Data " + dataPosicao.ToString("dd/MM/yyyy") + " não Exportado ";
                    mensagem += "\n - " + e.Message;
               
                    throw new ProcessaBacenJudException(mensagem);
                }
                #endregion
            }
            // 0 registros
            else {
                throw new ProcessaBacenJudException("Não existem dados para geração do arquivo BacenJud.");
            }
        }

        /// <summary>
        /// Escreve no Arquivo o objeto ValoresDetail
        /// </summary>
        /// <param name="arquivo"></param>
        /// <param name="linha">Numero da Linha de detalhe a escrever começando de 0</param>
        private void EscreveDetalhe(StreamWriter arquivo, int linha) {
            //
            arquivo.Write(this.valoresDetail[linha].dataBase.Year.ToString() +
                          this.FormataValor(this.valoresDetail[linha].dataBase.Month, AlinhamentoEsquerda._2Casas, false) +
                          this.FormataValor(this.valoresDetail[linha].dataBase.Day, AlinhamentoEsquerda._2Casas, false));
            //
            arquivo.Write(this.valoresDetail[linha].cpfCNPJ.ToString().PadRight(14, ' '));
            arquivo.Write(this.valoresDetail[linha].nomeCliente.Trim().PadRight(35, ' ')); // Completa com ' ' a direita
            arquivo.Write(this.valoresDetail[linha].sistema);
            arquivo.Write(this.valoresDetail[linha].produto.PadRight(30, ' '));
            //                      
            //arquivo.Write(this.valoresDetail[linha].empresa.ToString().Substring(0, 8));
            arquivo.Write(this.FormataValor(this.valoresDetail[linha].empresa, AlinhamentoEsquerda._8Casas, false));
            //
            arquivo.Write(this.FormataValor(this.valoresDetail[linha].agencia, AlinhamentoEsquerda._5Casas, false));
            arquivo.Write(this.valoresDetail[linha].nomeClube.Trim().PadRight(50, ' ')); // Completa com ' ' a direita

            decimal saldoDisponivel = this.valoresDetail[linha].saldo.Value * 100;
            decimal saldoBloqueadoOrdemJudicial = this.valoresDetail[linha].saldoBloqueadoOrdemJudicial.Value * 100;
            decimal saldoZerado = 0;

            arquivo.Write(saldoDisponivel.ToString().Replace(",00", "").PadLeft(17, '0'));
            arquivo.Write(saldoZerado.ToString().PadLeft(17, '0')); //Bloqueado por deposito em cheque
            arquivo.Write(saldoBloqueadoOrdemJudicial.ToString().Replace(",00", "").PadLeft(17, '0'));
            arquivo.Write(saldoZerado.ToString().PadLeft(17, '0')); //Bloqueado por garantia em bolsa
            arquivo.Write(saldoZerado.ToString().PadLeft(17, '0')); //Bloqueado por outros motivos
            arquivo.Write(this.valoresDetail[linha].gerente);

            // Utima Linha não imprime
            if (linha != this.valoresDetail.Count - 1) {
                arquivo.WriteLine();
            }
        }

        /// <summary>
        /// Formata um valor decimal com N casas Completando com zeros a esquerda
        /// Se numero é Positivo Coloca um Sinal de + na frente
        /// </summary>
        /// <param name="valor"></param>
        /// <param name="numeroCasasAlinhamentoEsquerda"></param>
        /// <returns></returns>
        private string FormataValor(decimal valor, AlinhamentoEsquerda numeroCasasAlinhamentoEsquerda) {
            return this.FormataValor(valor, numeroCasasAlinhamentoEsquerda, true);
        }

        /// <summary>
        /// Formata um valor decimal com N casas Completando com zeros a esquerda
        /// Se colocaPositivo=true e Se numero é Positivo Coloca um Sinal de + na frente
        /// Sinal Negativo Sempre Aparece
        /// </summary>
        /// <param name="valor"></param>
        /// <param name="numeroCasasAlinhamentoEsquerda"></param>
        /// <param name="colocaPositivo"></param>
        /// <returns></returns>
        private string FormataValor(decimal valor, AlinhamentoEsquerda numeroCasasAlinhamentoEsquerda, bool colocaPositivo) {
            if (colocaPositivo) {
                return valor.ToString(
                    valor >= 0
                    ? "+" + StringEnum.GetStringValue(numeroCasasAlinhamentoEsquerda)
                    : StringEnum.GetStringValue(numeroCasasAlinhamentoEsquerda));
            }
            else {
                return valor.ToString(StringEnum.GetStringValue(numeroCasasAlinhamentoEsquerda));
            }
        }
    }
}