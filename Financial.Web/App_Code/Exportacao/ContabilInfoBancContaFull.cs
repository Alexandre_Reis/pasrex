﻿using FileHelpers;
using System;
using Financial.Contabil;
using System.Collections.Generic;
using System.IO;
using Financial.WebConfigConfiguration;
using System.Text;
using Financial.Investidor;
using Financial.Util;
using EntitySpaces.Core;
using System.Text.RegularExpressions;

namespace Financial.Export {

    /// <summary>
    /// Summary description for ContabilInfoBancContaFull
    /// </summary>
    public class ContabilInfoBancContaFull {
        
        [FixedLengthRecord()]
        public class ContabInfobancDSContaFull
        {
            [FieldFixedLength(8)]
            [FieldConverter(ConverterKind.Date, "yyyyMMdd")]
            public DateTime dataLancamento;

            [FieldFixedLength(20)]
            [FieldAlign(AlignMode.Right, '0')]
            public int contaDebito = 0;           

            [FieldFixedLength(20)]
            [FieldAlign(AlignMode.Right, '0')]
            public int contaCredito = 0;


            [FieldFixedLength(18)]
            [FieldAlign(AlignMode.Right, '0')]
            public Int64 valorLancamento;

            [FieldFixedLength(6)]
            [FieldAlign(AlignMode.Right, '0')]
            public int numeroDocumento;

            [FieldFixedLength(40)]
            //[FieldAlign(AlignMode.Left, '0')]
            public string historico;

            [FieldFixedLength(40)]
            //[FieldAlign(AlignMode.Left, '0')]
            public string complementoHistorico1;

            [FieldFixedLength(40)]
            //[FieldAlign(AlignMode.Left, '0')]
            public string complementoHistorico2;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dataInicio"></param>
        /// <param name="dataFim"></param>
        /// <param name="idClientes"></param>
        /// <param name="dicMStream">Dicionario de memory Streams cada uma representando 1 arquivo e seus respectivos nomes de arquivos</param>
        public void ExportaContabLancamento(DateTime dataInicio, DateTime dataFim, List<int> idClientes, out Dictionary<string, MemoryStream> dicMStream) {

            dicMStream = new Dictionary<string, MemoryStream>();

            #region Consulta Sql - Trata Erros
                     
            ContabLancamentoQuery contabLancamentoQuery = new ContabLancamentoQuery("C");
            //
            contabLancamentoQuery.Where(contabLancamentoQuery.DataLancamento >= dataInicio &&
                                        contabLancamentoQuery.DataLancamento <= dataFim &&
                                        contabLancamentoQuery.IdCliente.In(idClientes)
                                        );
            //
            contabLancamentoQuery.OrderBy(contabLancamentoQuery.IdCliente.Ascending);
            //
            ContabLancamentoCollection contabLancamentoCollection = new ContabLancamentoCollection();
            contabLancamentoCollection.Load(contabLancamentoQuery);

            if (contabLancamentoCollection.Count == 0) {
                throw new Exception("Não há dados para exportar.");
            }
            #endregion

            idClientes.Sort(); // ordem crescente
            for (int i = 0; i < idClientes.Count; i++) { // Para cada Cliente

                #region Para cada Cliente
                esEntityCollectionView<ContabLancamento> view = new esEntityCollectionView<ContabLancamento>(contabLancamentoCollection);

                // Procura por IdCliente
                view.Filter = ContabLancamentoMetadata.ColumnNames.IdCliente + " = " + idClientes[i];

                List<ContabInfobancDSContaFull> contabInfobancDS = new List<ContabInfobancDSContaFull>();

                for (int j = 0; j < view.Count; j++) {
                    int idcliente = view[j].IdCliente.Value;
                    
                    #region Lancamento
                    ContabInfobancDSContaFull cl = new ContabInfobancDSContaFull();
                    //                
                    cl.dataLancamento = view[j].DataLancamento.Value;
                    //
                    #region ContaDebito
                    ContabConta contabConta = new ContabConta();
                    contabConta.LoadByPrimaryKey(view[j].IdContaDebito.Value);

                    string contaDebitoCodigo = !String.IsNullOrEmpty(contabConta.Codigo)? contabConta.Codigo.Trim(): "";

                    if (contaDebitoCodigo.Length >= 20)
                    {
                        contaDebitoCodigo = contaDebitoCodigo.Substring(0, 20);
                    }

                    if (!string.IsNullOrEmpty(contaDebitoCodigo))
                        contaDebitoCodigo = Regex.Replace(contaDebitoCodigo, @"\D", "");

                    try
                    {
                        cl.contaDebito = Convert.ToInt32(contaDebitoCodigo);
                    }
                    catch
                    {
                    }
                    #endregion
                    //
                    #region ContaCredito
                    contabConta = new ContabConta();
                    contabConta.LoadByPrimaryKey(view[j].IdContaCredito.Value);

                    string contaCreditoCodigo = !String.IsNullOrEmpty(contabConta.Codigo) ? contabConta.Codigo.Trim() : "";

                    if (contaCreditoCodigo.Length >= 20)
                    {
                        contaCreditoCodigo = contaCreditoCodigo.Substring(0, 20);
                    }

                    if (!string.IsNullOrEmpty(contaCreditoCodigo))
                        contaCreditoCodigo = Regex.Replace(contaCreditoCodigo, @"\D", "");

                    try
                    {
                        cl.contaCredito = Convert.ToInt32(contaCreditoCodigo);
                    }
                    catch
                    {
                    }
                    #endregion

                    //cl.digitoContaCredito = 0; //COMENTADO POIS VAI NO CORPO DA CONTA REDUZIDA
                    //
                    cl.valorLancamento = Convert.ToInt64(view[j].Valor.Value * 100);
                    //
                    cl.numeroDocumento = 0; // fica com 000000 pelo align
                    //
                    cl.historico = view[j].Descricao.Trim();
                    //
                    cl.complementoHistorico1 = "";
                    cl.complementoHistorico2 = "";
                    //
                    contabInfobancDS.Add(cl);
                    #endregion
                    //                    
                }

                if (contabInfobancDS.Count != 0) {
                    string nomeArquivo = "Contabil_" + idClientes[i] + ".txt";
                    MemoryStream ms = new MemoryStream();

                    #region GeraArquivo
                    FileHelperEngine engine = new FileHelperEngine(typeof(ContabInfobancDSContaFull));

                    StreamWriter arquivo = new StreamWriter(ms, Encoding.GetEncoding("ISO-8859-1"));
                    engine.Encoding = Encoding.GetEncoding("ISO-8859-1");
                    //
                    engine.WriteStream(arquivo, contabInfobancDS);

                    //Fecha o arquivo
                    arquivo.Flush();
                    #endregion

                    // Retorna a MemoryStream para o inicio
                    ms.Seek(0, SeekOrigin.Begin);

                    dicMStream.Add(nomeArquivo, ms);
                }
                #endregion
            }            
        }
    }
}