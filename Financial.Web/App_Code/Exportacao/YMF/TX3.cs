﻿using System;
using System.Web;
using Financial.ContaCorrente;
using Financial.Investidor;
using Bytescout.Spreadsheet;
using System.IO;
using Financial.Util;
using Financial.Fundo;
using Financial.InvestidorCotista;
using Financial.CRM;
using Financial.CRM.Enums;
using Financial.InvestidorCotista.Enums;
using System.Collections.Generic;
using FileHelpers;
using System.Text;
using Dart.PowerTCP.Zip;
using Financial.WebConfigConfiguration;
using EntitySpaces.Core;
using EntitySpaces.Interfaces;
using System.Threading;
using System.Globalization;

namespace Financial.Export.YMF
{
    /// <summary>
    /// Summary description for TX3
    /// </summary>
    public class TX3
    {
        public void ExportaArquivo(DateTime dataExportacao, List<int> idCarteiras, out MemoryStream ms, out string nomeArquivoZip)
        {
            Financial.Fundo.Exportacao.YMF.TX3 tx3 = new Financial.Fundo.Exportacao.YMF.TX3(dataExportacao);

            ms = new MemoryStream();
            nomeArquivoZip = "TX3_" + dataExportacao.ToString("yyyyMMdd") + ".zip";

            List<MemoryStream> streamsArquivosExportados = new List<MemoryStream>();
            List<string> nomeArquivosExportados = new List<string>();

            string path = DiretorioAplicacao.DiretorioBaseAplicacao + WebConfig.AppSettings.DiretorioDownloads;
            if (!path.Trim().EndsWith("\\"))
            {
                path += "\\";
            }
            path = path.Replace("\\", "\\\\");

            string login = HttpContext.Current.User.Identity.Name;
            path += login + @"\";

            // Se Diretorio Existe Apaga o Diretorio
            if (Directory.Exists(path))
            {
                Directory.Delete(path, true);
            }

            // Cria o Diretorio
            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }


            foreach (int idCarteira in idCarteiras)
            {
                List<Financial.Interfaces.Export.YMF.TX3> tx3List = tx3.MontaListaExportacao(idCarteira);

                ClienteInterface clienteInterface = new ClienteInterface();
                clienteInterface.LoadByPrimaryKey(idCarteira);

                //Salvar aqui no diretório a ser compactado
                FileHelperEngine engine = new FileHelperEngine(typeof(Financial.Interfaces.Export.YMF.TX3));

                string nomeArquivoExportado = clienteInterface.CodigoYMF + ".TX3";

                MemoryStream arquivoExportadoMemoryStream = new MemoryStream();
                StreamWriter arquivoExportadoStreamWriter = new StreamWriter(arquivoExportadoMemoryStream,
                    Encoding.GetEncoding("ISO-8859-1"));
                engine.Encoding = Encoding.GetEncoding("ISO-8859-1");
                engine.WriteStream(arquivoExportadoStreamWriter, tx3List);
                string debug = engine.WriteString(tx3List);

                arquivoExportadoStreamWriter.Flush();

                nomeArquivosExportados.Add(nomeArquivoExportado);
                streamsArquivosExportados.Add(arquivoExportadoMemoryStream);

                engine.WriteFile(path + nomeArquivoExportado, tx3List);

            }

            //Zipar os diretórios
            Archive arquivoZipado = new Archive();
            try
            {
                // For dos Arquivos
                for (int i = 0; i < nomeArquivosExportados.Count; i++)
                {
                    //arquivoZipado.Add(streamsArquivosExportados[i]);
                    arquivoZipado.Add(path + nomeArquivosExportados[i]);
                    arquivoZipado[i].Name = nomeArquivosExportados[i];
                }

                arquivoZipado.Zip(ms);
            }
            catch (Exception e1)
            {
                throw new Exception(e1.Message + "Erro ao Zipar o Arquivo");
            }

        }

    }
}