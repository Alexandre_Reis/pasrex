﻿using System;
using System.Web;
using Financial.ContaCorrente;
using Financial.Investidor;
using Bytescout.Spreadsheet;
using System.IO;
using Financial.Util;
using Financial.Fundo;
using Financial.InvestidorCotista;
using Financial.CRM;
using Financial.CRM.Enums;
using Financial.InvestidorCotista.Enums;
using System.Collections.Generic;
using FileHelpers;
using System.Text;
using Dart.PowerTCP.Zip;
using Financial.WebConfigConfiguration;
using Financial.Investidor.Enums;

namespace Financial.Export.YMF
{
    /// <summary>
    /// Summary description for TX2
    /// </summary>
    public class TX2
    {
        public void ExportaArquivo(DateTime dataExportacao, List<int> idCarteiras, out MemoryStream ms, out string nomeArquivoZip)
        {
            Financial.Fundo.Exportacao.YMF.TX2 tx2 = new Financial.Fundo.Exportacao.YMF.TX2(dataExportacao);

            ms = new MemoryStream();
            nomeArquivoZip = "TX2_" + dataExportacao.ToString("yyyyMMdd") + ".zip";

            List<MemoryStream> streamsArquivosExportados = new List<MemoryStream>();
            List<string> nomeArquivosExportados = new List<string>();

            string path = DiretorioAplicacao.DiretorioBaseAplicacao + WebConfig.AppSettings.DiretorioDownloads;
            if (!path.Trim().EndsWith("\\"))
            {
                path += "\\";
            }
            path = path.Replace("\\", "\\\\");

            string login = HttpContext.Current.User.Identity.Name;
            path += login + @"\";

            // Se Diretorio Existe Apaga o Diretorio
            if (Directory.Exists(path))
            {
                Directory.Delete(path, true);
            }

            // Cria o Diretorio
            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }


            foreach (int idCarteira in idCarteiras)
            {
                List<Financial.Interfaces.Export.YMF.TX2> tx2List = tx2.MontaListaExportacao(idCarteira);

                ClienteInterface clienteInterface = new ClienteInterface();
                clienteInterface.LoadByPrimaryKey(idCarteira);

                //Salvar aqui no diretório a ser compactado
                FileHelperEngine engine = new FileHelperEngine(typeof(Financial.Interfaces.Export.YMF.TX2));

                string nomeArquivoExportado = clienteInterface.CodigoYMF + ".TX2";

                MemoryStream arquivoExportadoMemoryStream = new MemoryStream();
                StreamWriter arquivoExportadoStreamWriter = new StreamWriter(arquivoExportadoMemoryStream,
                    Encoding.GetEncoding("ISO-8859-1"));
                engine.Encoding = Encoding.GetEncoding("ISO-8859-1");
                engine.WriteStream(arquivoExportadoStreamWriter, tx2List);
                string debug = engine.WriteString(tx2List);

                arquivoExportadoStreamWriter.Flush();

                nomeArquivosExportados.Add(nomeArquivoExportado);
                streamsArquivosExportados.Add(arquivoExportadoMemoryStream);

                engine.WriteFile(path + nomeArquivoExportado, tx2List);

            }

            //Zipar os diretórios
            Archive arquivoZipado = new Archive();
            try
            {
                // For dos Arquivos
                for (int i = 0; i < nomeArquivosExportados.Count; i++)
                {
                    arquivoZipado.Add(path + nomeArquivosExportados[i]);
                    arquivoZipado[i].Name = nomeArquivosExportados[i];
                }

                arquivoZipado.Zip(ms);
            }
            catch (Exception e1)
            {
                throw new Exception(e1.Message + "Erro ao Zipar o Arquivo");
            }

        }
    }
}
