﻿using System;

namespace Financial.Export {
    /// <summary>
    /// Classe base de Exceção das Exportações
    /// </summary>
    public class ExportacoesException : Exception {
        /// <summary>
        ///  Construtor
        /// </summary>
        public ExportacoesException() { }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="mensagem"></param>
        public ExportacoesException(string mensagem) : base(mensagem) { }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="mensagem"></param>
        /// <param name="inner"></param>
        public ExportacoesException(string mensagem, Exception inner) : base(mensagem, inner) { }
    }

    /// <summary>
    /// Exceção ProcessaMensalClubes
    /// </summary>
    public class ProcessaMensalClubesException : ExportacoesException {
        /// <summary>
        ///  Construtor
        /// </summary>
        public ProcessaMensalClubesException() { }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="mensagem"></param>
        public ProcessaMensalClubesException(string mensagem) : base(mensagem) { }
    }

    /// <summary>
    /// Exceção ProcessaAtivosClubes
    /// </summary>
    public class ProcessaAtivosClubesException : ExportacoesException {
        /// <summary>
        ///  Construtor
        /// </summary>
        public ProcessaAtivosClubesException() { }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="mensagem"></param>
        public ProcessaAtivosClubesException(string mensagem) : base(mensagem) { }
    }

    /// <summary>
    /// Exceção ProcessaBacenJUD
    /// </summary>
    public class ProcessaBacenJudException : ExportacoesException
    {
        /// <summary>
        ///  Construtor
        /// </summary>
        public ProcessaBacenJudException() { }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="mensagem"></param>
        public ProcessaBacenJudException(string mensagem) : base(mensagem) { }
    }

    /// <summary>
    /// Exceção ProcessaBacenCCS
    /// </summary>
    public class ProcessaBacenCCSException : ExportacoesException
    {
        /// <summary>
        ///  Construtor
        /// </summary>
        public ProcessaBacenCCSException() { }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="mensagem"></param>
        public ProcessaBacenCCSException(string mensagem) : base(mensagem) { }
    }

    /// <summary>
    /// Exceção ProcessaDirf
    /// </summary>
    public class ProcessaDirfException : ExportacoesException
    {
        /// <summary>
        ///  Construtor
        /// </summary>
        public ProcessaDirfException() { }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="mensagem"></param>
        public ProcessaDirfException(string mensagem) : base(mensagem) { }
    }

    /// <summary>
    /// Exceção ProcessaContabil
    /// </summary>
    public class ProcessaContabilException : ExportacoesException
    {
        /// <summary>
        ///  Construtor
        /// </summary>
        public ProcessaContabilException() { }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="mensagem"></param>
        public ProcessaContabilException(string mensagem) : base(mensagem) { }
    }

    /// <summary>
    /// Exceção ProcessaPicl
    /// </summary>
    public class ProcessaPiclException : ExportacoesException {
        /// <summary>
        ///  Construtor
        /// </summary>
        public ProcessaPiclException() { }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="mensagem"></param>
        public ProcessaPiclException(string mensagem) : base(mensagem) { }
    }

    /// <summary>
    /// Exceção ProcessaTabelaPerfilMensal
    /// </summary>
    public class ProcessaTabelaPerfilMensal : ExportacoesException {
        /// <summary>
        ///  Construtor
        /// </summary>
        public ProcessaTabelaPerfilMensal() { }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="mensagem"></param>
        public ProcessaTabelaPerfilMensal(string mensagem) : base(mensagem) { }
    }
}


