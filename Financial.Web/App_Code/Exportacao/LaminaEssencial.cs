﻿using System.Xml.Serialization;
using System.Xml;
using System;
using System.IO;
using EntitySpaces.Interfaces;
using System.Collections.Generic;
using Financial.CRM;
using Financial.Investidor;
using Financial.Investidor.Enums;
using Financial.Relatorio;
using Financial.Fundo;
using Financial.Util;
using Financial.Fundo.Enums;
using Financial.ContaCorrente;
using Financial.ContaCorrente.Enums;
using Financial.RendaFixa;
using Financial.RendaFixa.Enums;
using Financial.Common.Enums;
using Financial.Bolsa.Enums;
using Financial.Bolsa;
using Financial.BMF;
using Financial.Fundo.Exceptions;
using Financial.Common;
using Financial.Swap;
using System.Text.RegularExpressions;

namespace Financial.Export {

    #region Versao 2.0
    #region PDF
    public class LaminaEssencial {
        /// <summary>
        /// Retorna uma Stream de Mémoria de um arquivo xml. A Stream permanece aberta
        /// </summary>
        /// <param name="dataPosicao"></param>
        /// <param name="idClientes"></param>
        /// <param name="dicMStream">Saida: Dicionario contendo Nome do arquivo e Memory Stream do arquivo CDA</param>
        /// <returns></returns>
        public void ExportaLamina(DateTime data, List<int> idClientes, out Dictionary<string, MemoryStream> dicMStream) {
            dicMStream = new Dictionary<string, MemoryStream>();

            for (int i = 0; i < idClientes.Count; i++) {
                //
                MemoryStream ms = new MemoryStream();
                string mes = data.Month.ToString();
                if (mes.Length == 1) {
                    mes = "0" + mes;
                }

                string nomeArquivo = idClientes[i] + "_" + mes + data.Year.ToString() + ".pdf";
                //

                //#region Gera o PDF
                ReportLaminaExportacao report = new ReportLaminaExportacao(idClientes[i], data);

                //report.CreateDocument();
                //int pagina = report.PrintingSystem.Pages.Last.Index;
                //report.PrintingSystem.Pages.RemoveAt(pagina);

                report.ExportToPdf(ms);

                /* Necessário voltar o ponteiro do arquivo para o Inicio */
                ms.Seek(0, SeekOrigin.Begin);
                //
                //#endregion

                //
                // Adiciona no Dicionário
                dicMStream.Add(nomeArquivo, ms);
            }
        }
    }
    #endregion

    #region Exportacao XML
    [Serializable()]
    [XmlRootAttribute(ElementName = "DOC_ARQ", IsNullable = false, Namespace = "urn:lamina")]
    public class XML_Lamina {

        [XmlElement(ElementName = "CAB_INFORM")]
        public Cabecalho cabecalho = new Cabecalho();

        [XmlArray("LISTA_INFORM")]
        [XmlArrayItem("INFORM")]
        public List<Corpo> corpo = new List<Corpo>();

        #region Cabecalho
        public class Cabecalho {

            public Cabecalho() { }

            [XmlElement("COD_DOC", Order = 1)]
            public int codigoDocumento;

            [XmlElement("VERSAO", Order = 2)]
            public string versao;

            #region DT_COMPT
            [XmlIgnore]
            public DateTime dataCompetencia;

            [XmlElement("DT_COMPT", Order = 3)]
            public string formatDataCompetencia {
                get { return this.dataCompetencia.ToString("MM/yyyy"); }
                set { dataCompetencia = DateTime.Parse(value); }
            }
            #endregion

            #region DT_GERAC_ARQ
            [XmlIgnore]
            public DateTime dataGeracaoArquivo;

            [XmlElement("DT_GERAC_ARQ", Order = 4)]
            public string formatDataGeracaoArquivo {
                get { return this.dataGeracaoArquivo.ToString("dd/MM/yyyy"); }
                set { dataGeracaoArquivo = DateTime.Parse(value); }
            }
            #endregion
        }
        #endregion

        #region Corpo
        public class Corpo {

            public Corpo() { }

            /**********************************************/
            [XmlElement("CNPJ_FUNDO", Order = 1)]
            public string cnpj;

            [XmlElement("NOME_FANTASIA", Order = 2)]
            public string nomeFantasia;

            [XmlElement("END_ELETR", Order = 3)]
            public string enderecoEletronico;

            [XmlElement("DESC_PUB_ALVO", Order = 4)]
            public string descricaoPublicoAlvo;

            [XmlElement("RESTR_INV", Order = 5)]
            public string RESTR_INV;

            [XmlElement("DESC_OBJET", Order = 6)]
            public string DESC_OBJET;

            [XmlElement("DESC_POL_INV", Order = 7)]
            public string descricaoPoliticaInvestimento;

            /* ----------------------------------------------------------- */

            [XmlIgnore]
            public decimal ativo_ext;

            #region ATIVO_EXT
            [XmlElement("ATIVO_EXT", Order = 8)]
            public string format_ATIVO_EXT {

                get {
                    return ativo_ext.ToString("N2").Replace(".", "");
                }
                set {
                    decimal ativo_ext = 0;

                    if (Decimal.TryParse(value, out ativo_ext))
                        ativo_ext = ativo_ext;
                }
            }
            #endregion

            [XmlIgnore]
            public decimal CRED_PRIV;

            #region CRED_PRIV
            [XmlElement("CRED_PRIV", Order = 9)]
            public string format_CRED_PRIV {

                get {
                    return CRED_PRIV.ToString("N2").Replace(".", "");
                }
                set {
                    decimal CRED_PRIV = 0;

                    if (Decimal.TryParse(value, out CRED_PRIV))
                        CRED_PRIV = CRED_PRIV;
                }
            }
            #endregion

            [XmlIgnore]
            public decimal ALAV;

            #region ALAV
            [XmlElement("ALAV", Order = 10)]
            public string format_ALAV {

                get {
                    return ALAV.ToString("N2").Replace(".", "");
                }
                set {
                    decimal ALAV = 0;

                    if (Decimal.TryParse(value, out ALAV))
                        ALAV = ALAV;
                }
            }
            #endregion

            [XmlElement("UTILIZ_DERIV_APENAS_PROTECAO_CART", Order = 11)]
            public string utilizaDerivativosCredito;

            [XmlElement("PERDAS_SIGN", Order = 12)]
            public string perdasSignificativas;

            [XmlElement("PL_NEGAT", Order = 13)]
            public string PL_NEGAT;

            [XmlElementAttribute("PERC_MAX", Order = 14)] // TODO
            public decimal PERC_MAX;

            [XmlIgnore]
            public decimal INV_INICIAL;

            #region INV_INICIAL
            [XmlElement("INV_INICIAL", Order = 15)]
            public string format_INV_INICIAL {

                get {
                    return INV_INICIAL.ToString("N2").Replace(".", "");
                }
                set {
                    decimal INV_INICIAL = 0;

                    if (Decimal.TryParse(value, out INV_INICIAL))
                        INV_INICIAL = INV_INICIAL;
                }
            }
            #endregion

            [XmlIgnore]
            public decimal INV_ADIC;

            #region INV_ADIC
            [XmlElement("INV_ADIC", Order = 16)]
            public string format_INV_ADIC {

                get {
                    return INV_ADIC.ToString("N2").Replace(".", "");
                }
                set {
                    decimal INV_ADIC = 0;

                    if (Decimal.TryParse(value, out INV_ADIC))
                        INV_ADIC = INV_ADIC;
                }
            }
            #endregion

            [XmlIgnore]
            public decimal RESG_MIN;

            #region RESG_MIN
            [XmlElement("RESG_MIN", Order = 17)]
            public string format_RESG_MIN {

                get {
                    return RESG_MIN.ToString("N2").Replace(".", "");
                    //return RESG_MIN.ToString("N2");
                }
                set {
                    decimal RESG_MIN = 0;

                    if (Decimal.TryParse(value, out RESG_MIN))
                        RESG_MIN = RESG_MIN;
                }
            }
            #endregion

            //#region HOR_APL_RESG
            //public DateTime HOR_APL_RESG;

            //[XmlElement("HOR_APL_RESG", Order = 18)]
            //public string formatHOR_APL_RESG {
            //    get { return this.HOR_APL_RESG.ToString("hh:mm"); }
            //    set { HOR_APL_RESG = DateTime.Parse(value); }
            //}
            //#endregion

            #region HOR_APL_RESG
            [XmlIgnore]
            public DateTime? HOR_APL_RESG;

            [XmlElement("HOR_APL_RESG", Order = 18)]
            public string formatHOR_APL_RESG {
                get {
                    return this.HOR_APL_RESG.HasValue ? this.HOR_APL_RESG.Value.ToString("HH:mm") : String.Empty;
                }
                set {
                    if (!this.HOR_APL_RESG.HasValue) {
                        formatHOR_APL_RESG = String.Empty;
                    }
                    else {
                        HOR_APL_RESG = DateTime.Parse(value);
                    }
                }
            }
            #endregion

            [XmlIgnore]
            public decimal VALOR_PERM;

            #region VALOR_PERM
            [XmlElement("VALOR_PERM", Order = 19)]
            public string format_VALOR_PERM {

                get {
                    return VALOR_PERM.ToString("N2").Replace(".", "");
                }
                set {
                    decimal VALOR_PERM = 0;

                    if (Decimal.TryParse(value, out VALOR_PERM))
                        VALOR_PERM = VALOR_PERM;
                }
            }
            #endregion
            /* ----------------------------------------------------------- */

            #region Carencia
            [XmlElementAttribute("CARENCIA", Order = 20)]
            public CARENCIA carencia = new CARENCIA();

            [XmlIgnore]
            public CARENCIA getCARENCIA {
                get { return carencia; }
                set { carencia = value; }
            }
            #endregion

            #region Conversao
            [XmlElementAttribute("CONVERSAO", Order = 21)]
            public CONVERSAO conversao = new CONVERSAO();

            [XmlIgnore]
            public CONVERSAO getCONVERSAO {
                get { return conversao; }
                set { conversao = value; }
            }
            #endregion

            [XmlElement("TP_PRAZO_DIAS", Order = 22)]
            public int TP_PRAZO_DIAS;

            [XmlElement("PRAZO_PAGAM", Order = 23)]
            public string PRAZO_PAGAM;

            #region INFOR_TX_ADM
            [XmlElementAttribute("INFOR_TX_ADM", Order = 24)]
            public INFOR_TX_ADM infor_tx_adm = new INFOR_TX_ADM();

            [XmlIgnore]
            public INFOR_TX_ADM getINFOR_TX_ADM {
                get { return infor_tx_adm; }
                set { infor_tx_adm = value; }
            }
            #endregion

            #region INFOR_TX_ENTR
            [XmlElementAttribute("INFOR_TX_ENTR", Order = 25)]
            public INFOR_TX_ENTR infortxentr = new INFOR_TX_ENTR();

            [XmlIgnore]
            public INFOR_TX_ENTR getINFOR_TX_ENTR {
                get { return infortxentr; }
                set { infortxentr = value; }
            }
            #endregion

            #region INFOR_TX_SAIDA
            [XmlElementAttribute("INFOR_TX_SAIDA", Order = 26)]
            public INFOR_TX_SAIDA infortxsaida = new INFOR_TX_SAIDA();

            [XmlIgnore]
            public INFOR_TX_SAIDA getINFOR_TX_SAIDA {
                get { return infortxsaida; }
                set { infortxsaida = value; }
            }
            #endregion

            [XmlIgnore]
            public string tx_desempenho;

            #region TX_DESEMPENHO
            [XmlElement("TX_DESEMPENHO", Order = 27)]
            public string format_TX_DESEMPENHO {

                get {
                    return tx_desempenho; //.ToString("N2").Replace(".", "");
                }
                set {
                    string tx_desempenho = "0";

                    //if (Decimal.TryParse(value, out tx_desempenho))
                    //    tx_desempenho = tx_desempenho;
                }
            }
            #endregion

            #region INFOR_DESPESAS
            [XmlElementAttribute("INFOR_DESPESAS", Order = 28)]
            public INFOR_DESPESAS infor_despesas = new INFOR_DESPESAS();

            [XmlIgnore]
            public INFOR_DESPESAS getINFOR_DESPESAS {
                get { return infor_despesas; }
                set { infor_despesas = value; }
            }
            #endregion

            #region LISTA_COMP_CARTEIRA
            [XmlElementAttribute("LISTA_COMP_CARTEIRA", Order = 29)]
            public LISTA_COMP_CARTEIRA lista_comp_carteira = new LISTA_COMP_CARTEIRA();

            [XmlIgnore]
            public LISTA_COMP_CARTEIRA getLISTA_COMP_CARTEIRA {
                get { return lista_comp_carteira; }
                set { lista_comp_carteira = value; }
            }
            #endregion

            [XmlElement("CLASSIF_RISCO", Order = 30)]
            public int classificacaoRisco;

            #region INFOR_TAB_RENT_ANUAL
            [XmlElementAttribute("INFOR_TAB_RENT_ANUAL", Order = 31)]
            public INFOR_TAB_RENT_ANUAL infor_tab_rent_anual = new INFOR_TAB_RENT_ANUAL();

            [XmlIgnore]
            public INFOR_TAB_RENT_ANUAL getINFOR_TAB_RENT_ANUAL {
                get { return infor_tab_rent_anual; }
                set { infor_tab_rent_anual = value; }
            }
            #endregion

            #region LISTA_TAB_RENT_ANUAL
            [XmlElementAttribute("LISTA_TAB_RENT_ANUAL", Order = 32)]
            public LISTA_TAB_RENT_ANUAL lista_tab_rent_anual = new LISTA_TAB_RENT_ANUAL();

            [XmlIgnore]
            public LISTA_TAB_RENT_ANUAL getLISTA_TAB_RENT_ANUAL {
                get { return lista_tab_rent_anual; }
                set { lista_tab_rent_anual = value; }
            }
            #endregion

            #region LISTA_TAB_RENT_MENSAL
            [XmlElementAttribute("LISTA_TAB_RENT_MENSAL", Order = 33)]
            public LISTA_TAB_RENT_MENSAL lista_tab_rent_mensal = new LISTA_TAB_RENT_MENSAL();

            [XmlIgnore]
            public LISTA_TAB_RENT_MENSAL getLISTA_TAB_RENT_MENSAL {
                get { return lista_tab_rent_mensal; }
                set { lista_tab_rent_mensal = value; }
            }
            #endregion

            [XmlElementAttribute("FORM_CALC_RENT", Order = 34)]
            public string FORM_CALC_RENT;

            #region TAB_VAR_DESEMP
            [XmlElementAttribute("TAB_VAR_DESEMP", Order = 35)]
            public TAB_VAR_DESEMP tab_var_desemp = new TAB_VAR_DESEMP();

            [XmlIgnore]
            public TAB_VAR_DESEMP getTAB_VAR_DESEMP {
                get { return tab_var_desemp; }
                set { tab_var_desemp = value; }
            }
            #endregion

            #region INFORM_EXEMPLO_COMP
            [XmlElementAttribute("INFORM_EXEMPLO_COMP", Order = 36)]
            public INFORM_EXEMPLO_COMP inform_exemplo_comp = new INFORM_EXEMPLO_COMP();

            [XmlIgnore]
            public INFORM_EXEMPLO_COMP getINFORM_EXEMPLO_COMP {
                get { return inform_exemplo_comp; }
                set { inform_exemplo_comp = value; }
            }
            #endregion

            #region TAB_SIMUL_DESPESAS
            [XmlElementAttribute("TAB_SIMUL_DESPESAS", Order = 37)]
            public TAB_SIMUL_DESPESAS tab_simul_despesas = new TAB_SIMUL_DESPESAS();

            [XmlIgnore]
            public TAB_SIMUL_DESPESAS getTAB_SIMUL_DESPESAS {
                get { return tab_simul_despesas; }
                set { tab_simul_despesas = value; }
            }
            #endregion

            [XmlElement("DS_FORMA_REMUN_DISTR", Order = 38)]
            public string DS_FORMA_REMUN_DISTR;

            [XmlElement("PRINC_DISTR_OFERTA", Order = 39)]
            public string PRINC_DISTR_OFERTA;

            [XmlElement("PRINC_CONFLITO_INTERES", Order = 40)]
            public string PRINC_CONFLITO_INTERES;

            [XmlElement("TEL_SAC", Order = 41)]
            public long telefone;

            [XmlElement("RECLAMACOES", Order = 42)]
            public string reclamacoes;

            [XmlElement("INFO_ADIC", Order = 43)]
            public string INFO_ADIC;
        }
        #endregion
        /* ----------------------------------------------------------- */

        #region CARENCIA
        public class CARENCIA {
            // Construtor
            public CARENCIA() { }

            [XmlElement("DIAS_CAR", Order = 1)]
            public int DIAS_CAR;

            [XmlElement("COND_CAR", Order = 2)]
            public string COND_CAR;
        }
        #endregion

        #region CONVERSAO
        public class CONVERSAO {
            // Construtor
            public CONVERSAO() { }

            [XmlElement("ABERT_FECH", Order = 1)]
            public string ABERT_FECH;

            [XmlElement("DIA_APLIC", Order = 2)]
            public int DIA_APLIC;

            [XmlElement("ABERT_FECH_C", Order = 3)]
            public string ABERT_FECH_C;

            [XmlElement("DIA_RESG", Order = 4)]
            public int DIA_RESG;
        }
        #endregion

        #region INFOR_TX_ADM
        public class INFOR_TX_ADM {
            // Construtor
            public INFOR_TX_ADM() { }

            [XmlElement("TIPO_TX_ADM", Order = 1)]
            public string TIPO_TX_ADM;

            [XmlIgnore]
            public decimal tx_adm;

            #region TX_ADM
            [XmlElement("TX_ADM", Order = 2)]
            public string format_TX_ADM {

                get {
                    return tx_adm.ToString("N2").Replace(".", "");
                }
                set {
                    decimal tx_adm = 0;

                    if (Decimal.TryParse(value, out tx_adm))
                        tx_adm = tx_adm;
                }
            }
            #endregion

            [XmlIgnore]
            public decimal tx_min;

            #region TX_MIN
            [XmlElement("TX_MIN", Order = 3)]
            public string format_TX_MIN {

                get {
                    return tx_min.ToString("N2").Replace(".", "");
                }
                set {
                    decimal tx_min = 0;

                    if (Decimal.TryParse(value, out tx_min))
                        tx_min = tx_min;
                }
            }
            #endregion

            [XmlIgnore]
            public decimal tx_max;

            #region TX_MAX
            [XmlElement("TX_MAX", Order = 4)]
            public string format_TX_MAX {

                get {
                    return tx_max.ToString("N2").Replace(".", "");
                }
                set {
                    decimal tx_max = 0;

                    if (Decimal.TryParse(value, out tx_max))
                        tx_max = tx_max;
                }
            }
            #endregion

            [XmlElement("ESCLAREC", Order = 5)]
            public string ESCLAREC;
        }
        #endregion

        #region INFOR_TX_ENTR
        public class INFOR_TX_ENTR {
            // Construtor
            public INFOR_TX_ENTR() { }

            [XmlElement("TX_ENTR", Order = 1)]
            public int TX_ENTR;

            [XmlElement("COND_ENTR", Order = 2)]
            public string COND_ENTR;
        }
        #endregion

        #region INFOR_TX_SAIDA
        public class INFOR_TX_SAIDA {
            // Construtor
            public INFOR_TX_SAIDA() { }

            [XmlElement("DIAS_SAIDA", Order = 1)]
            public int DIAS_SAIDA;

            [XmlElement("TX_SAIDA", Order = 2)]
            public int TX_SAIDA;

            [XmlElement("COND_SAIDA", Order = 3)]
            public string COND_SAIDA;
        }
        #endregion

        #region INFOR_DESPESAS
        public class INFOR_DESPESAS {
            // Construtor
            public INFOR_DESPESAS() { }

            [XmlIgnore]
            public decimal despesas;

            #region DESPESAS
            [XmlElement("DESPESAS", Order = 1)]
            public string format_DESPESAS {

                get {
                    return despesas.ToString("N2").Replace(".", "");
                }
                set {
                    decimal despesas = 0;

                    if (Decimal.TryParse(value, out despesas))
                        despesas = despesas;
                }
            }
            #endregion

            #region INIC_PERIODO
            [XmlIgnore]
            public DateTime inic_periodo;

            [XmlElement("INIC_PERIODO", Order = 2)]
            public string formatINIC_PERIODO {
                get { return this.inic_periodo.ToString("dd/MM/yyyy"); }
                set { inic_periodo = DateTime.Parse(value); }
            }
            #endregion

            #region FIM_PERIODO
            [XmlIgnore]
            public DateTime fim_periodo;

            [XmlElement("FIM_PERIODO", Order = 3)]
            public string formatFIM_PERIODO {
                get { return this.fim_periodo.ToString("dd/MM/yyyy"); }
                set { fim_periodo = DateTime.Parse(value); }
            }
            #endregion

            [XmlElement("END_ELET_ADM", Order = 4)]
            public string END_ELET_ADM;
        }
        #endregion

        /* ----------------------------------------------------------- */

        public class LISTA_COMP_CARTEIRA {
            // Construtor
            public LISTA_COMP_CARTEIRA() { }

            [XmlIgnore]
            public decimal pl_fundo;

            #region PL_FUNDO
            [XmlElement("PL_FUNDO", Order = 1)]
            public string format_PL_FUNDO {

                get {
                    return pl_fundo.ToString("N2").Replace(".", "");
                }
                set {
                    decimal pl_fundo = 0;

                    if (Decimal.TryParse(value, out pl_fundo))
                        pl_fundo = pl_fundo;
                }
            }
            #endregion

            [XmlElementAttribute("COMP_CARTEIRA", Order = 2)]
            public List<COMP_CARTEIRA> comp_carteira = new List<COMP_CARTEIRA>();

            [XmlIgnore]
            public List<COMP_CARTEIRA> getCOMP_CARTEIRA {
                get { return comp_carteira; }
                set { comp_carteira = value; }
            }
        }

        #region COMP_CARTEIRA
        public class COMP_CARTEIRA {
            // Construtor
            public COMP_CARTEIRA() { }

            [XmlElement("ESPECIE_ATIVO", Order = 1)]
            public string ESPECIE_ATIVO;

            [XmlIgnore]
            public decimal perc_pl;

            #region PERC_PL
            [XmlElement("PERC_PL", Order = 2)]
            public string format_PERC_PL {

                get {
                    return perc_pl.ToString("N2").Replace(".", "");
                }
                set {
                    decimal perc_pl = 0;

                    if (Decimal.TryParse(value, out perc_pl))
                        perc_pl = perc_pl;
                }
            }
            #endregion
        }
        #endregion

        /* ----------------------------------------------------------- */

        #region INFOR_TAB_RENT_ANUAL
        public class INFOR_TAB_RENT_ANUAL {
            // Construtor
            public INFOR_TAB_RENT_ANUAL() { }

            [XmlIgnore]
            public decimal rent_5anos; // * Campo não obrigatório para fundos com menos de um ano de funcionamento

            #region RENT_5ANOS
            [XmlElement("RENT_5ANOS", Order = 1)]
            public string format_RENT_5ANOS {

                get {
                    return rent_5anos.ToString("N2").Replace(".", "");
                }
                set {
                    decimal rent_5anos = 0;

                    if (Decimal.TryParse(value, out rent_5anos))
                        rent_5anos = rent_5anos;
                }
            }
            #endregion

            [XmlElement("IND_REFER", Order = 2)]
            public string IND_REFER; // * CAMPO OPCIONAL

            [XmlIgnore]
            public decimal rent_ind_ref; // * CAMPO OPCIONAL

            #region RENT_IND_REF
            [XmlElement("RENT_IND_REF", Order = 3)]
            public string format_RENT_IND_REF {

                get {
                    return rent_ind_ref.ToString("N2").Replace(".", "");
                }
                set {
                    decimal rent_ind_ref = 0;

                    if (Decimal.TryParse(value, out rent_ind_ref))
                        rent_ind_ref = rent_ind_ref;
                }
            }
            #endregion

            [XmlElement("ANOS_ABAIXO", Order = 4)]
            public int ANOS_ABAIXO; // * CAMPO OPCIONAL

            #region INIC_FUNCIO
            [XmlIgnore]
            public DateTime inic_funcio;  // * Campo não obrigatório para fundos com menos de um ano de funcionamento

            [XmlElement("INIC_FUNCIO", Order = 5)]
            public string formatINIC_FUNCIO {
                get { return this.inic_funcio.ToString("dd/MM/yyyy"); }
                set { inic_funcio = DateTime.Parse(value); }
            }
            #endregion

            [XmlElementAttribute("ANTES_INIC2", Order = 6)]
            public List<ANTES_INIC2> antes_inic2 = new List<ANTES_INIC2>();

            [XmlIgnore]
            public List<ANTES_INIC2> getANTES_INIC2 {
                get { return antes_inic2; }
                set { antes_inic2 = value; }
            }
        }
        #endregion

        #region ANTES_INIC2
        public class ANTES_INIC2 {
            // Construtor
            public ANTES_INIC2() { }

            [XmlElement("ANO", Order = 1)]
            public int? ANO1;

            [XmlElement("ANO", Order = 2)]
            public int? ANO2;

            [XmlElement("ANO", Order = 3)]
            public int? ANO3;

            [XmlElement("ANO", Order = 4)]
            public int? ANO4;

            [XmlElement("ANO", Order = 5)]
            public int? ANO5;

            // Controla Se vai aparecer se for null
            public bool ShouldSerializeANO1() {
                return !(ANO1 == null);
            }
            public bool ShouldSerializeANO2() {
                return !(ANO2 == null);
            }

            public bool ShouldSerializeANO3() {
                return !(ANO3 == null);
            }
            public bool ShouldSerializeANO4() {
                return !(ANO4 == null);
            }
            public bool ShouldSerializeANO5() {
                return !(ANO5 == null);
            }
        }
        #endregion

        /* ----------------------------------------------------------- */

        public class LISTA_TAB_RENT_ANUAL {
            // Construtor
            public LISTA_TAB_RENT_ANUAL() { }

            [XmlElementAttribute("TAB_RENT_ANUAL")]
            public List<TAB_RENT_ANUAL> tab_rent_anual = new List<TAB_RENT_ANUAL>();

            [XmlIgnore]
            public List<TAB_RENT_ANUAL> getTAB_RENT_ANUAL {
                get { return tab_rent_anual; }
                set { tab_rent_anual = value; }
            }
        }

        #region TAB_RENT_ANUAL
        public class TAB_RENT_ANUAL {
            // Construtor
            public TAB_RENT_ANUAL() { }

            [XmlElement("ANO_RENT", Order = 1)]
            public int ANO_RENT;

            [XmlIgnore]
            public decimal rent_liq;

            #region RENT_LIQ
            [XmlElement("RENT_LIQ", Order = 2)]
            public string format_RENT_LIQ {

                get {
                    return rent_liq.ToString("N2").Replace(".", "");
                }
                set {
                    decimal rent_liq = 0;

                    if (Decimal.TryParse(value, out rent_liq))
                        rent_liq = rent_liq;
                }
            }
            #endregion

            [XmlIgnore]
            public decimal var_ind_ref;

            #region VAR_IND_REF
            [XmlElement("VAR_IND_REF", Order = 3)]
            public string format_VAR_IND_REF {

                get {
                    return var_ind_ref.ToString("N2").Replace(".", "");
                }
                set {
                    decimal var_ind_ref = 0;

                    if (Decimal.TryParse(value, out var_ind_ref))
                        var_ind_ref = var_ind_ref;
                }
            }
            #endregion

            [XmlIgnore]
            public decimal rent_perc_ind_ref;

            #region RENT_PERC_IND_REF
            [XmlElement("RENT_PERC_IND_REF", Order = 4)]
            public string format_RENT_PERC_IND_REF {

                get {
                    return rent_perc_ind_ref.ToString("N2").Replace(".", "");
                }
                set {
                    decimal rent_perc_ind_ref = 0;

                    if (Decimal.TryParse(value, out rent_perc_ind_ref))
                        rent_perc_ind_ref = rent_perc_ind_ref;
                }
            }
            #endregion

            [XmlElement("COMENTARIOS", Order = 5)]
            public string COMENTARIOS;
        }
        #endregion

        /* ----------------------------------------------------------- */

        public class LISTA_TAB_RENT_MENSAL {
            // Construtor
            public LISTA_TAB_RENT_MENSAL() { }

            [XmlElementAttribute("TAB_RENT_MENSAL")]
            public List<TAB_RENT_MENSAL> tab_rent_mensal = new List<TAB_RENT_MENSAL>();

            [XmlIgnore]
            public List<TAB_RENT_MENSAL> getTAB_RENT_MENSAL {
                get { return tab_rent_mensal; }
                set { tab_rent_mensal = value; }
            }
        }

        #region TAB_RENT_MENSAL
        public class TAB_RENT_MENSAL {
            // Construtor
            public TAB_RENT_MENSAL() { }

            [XmlElement("MES", Order = 1)]
            public int MES;

            [XmlIgnore]
            public decimal rent_mens;

            #region RENT_MENS
            [XmlElement("RENT_MENS", Order = 2)]
            public string format_RENT_MENS {

                get {
                    return rent_mens.ToString("N2").Replace(".", "");
                }
                set {
                    decimal rent_mensal = 0;

                    if (Decimal.TryParse(value, out rent_mensal))
                        rent_mensal = rent_mensal;
                }
            }
            #endregion

            [XmlIgnore]
            public decimal var_mens_ind_ref;

            #region VAR_MENS_IND_REF
            [XmlElement("VAR_MENS_IND_REF", Order = 3)]
            public string format_VAR_MENS_IND_REF {

                get {
                    return var_mens_ind_ref.ToString("N2").Replace(".", "");
                }
                set {
                    decimal var_mens_ind_ref = 0;

                    if (Decimal.TryParse(value, out var_mens_ind_ref))
                        var_mens_ind_ref = var_mens_ind_ref;
                }
            }
            #endregion

            [XmlIgnore]
            public decimal rent_mens_perc_ind_ref;

            #region VAR_IND_REF
            [XmlElement("RENT_MENS_PERC_IND_REF", Order = 4)]
            public string format_RENT_MENS_PERC_IND_REF {

                get {
                    return rent_mens_perc_ind_ref.ToString("N2").Replace(".", "");
                }
                set {
                    decimal rent_mens_perc_ind_ref = 0;

                    if (Decimal.TryParse(value, out rent_mens_perc_ind_ref))
                        rent_mens_perc_ind_ref = rent_mens_perc_ind_ref;
                }
            }
            #endregion

            [XmlElement("COMENTARIOS", Order = 5)]
            public string COMENTARIOS;
        }
        #endregion

        /* ----------------------------------------------------------- */

        #region TAB_VAR_DESEMP
        public class TAB_VAR_DESEMP {
            // Construtor
            public TAB_VAR_DESEMP() { }

            [XmlIgnore]
            public decimal var_desemp;  // * Campo não obrigatório para fundos com menos de um ano de funcionamento

            #region VAR_DESEMP
            [XmlElement("VAR_DESEMP", Order = 1)]
            public string format_VAR_DESEMP {

                get {
                    return var_desemp.ToString("N2").Replace(".", "");
                }
                set {
                    decimal var_desemp = 0;

                    if (Decimal.TryParse(value, out var_desemp))
                        var_desemp = var_desemp;
                }
            }
            #endregion

            //[XmlElement("VAR_DESEMP", Order = 1)] // * Campo não obrigatório para fundos com menos de um ano de funcionamento
            //public string VAR_DESEMP;

            [XmlElement("FORMULA_RENTAB", Order = 2)]
            public string FORMULA_RENTAB;

            [XmlElement("CENARIO_GATILHO", Order = 3)]
            public string CENARIO_GATILHO; //  *CAMPO OPCIONAL

            [XmlElement("ESCLAREC", Order = 4)]
            public string COMENTARIOS; //  *CAMPO OPCIONAL

        }
        #endregion

        #region INFORM_EXEMPLO_COMP
        public class INFORM_EXEMPLO_COMP {
            // Construtor
            public INFORM_EXEMPLO_COMP() { }

            [XmlElement("ANO_ANT", Order = 1)] //* Campo não obrigatório para fundos com menos de um ano de funcionamento
            public int ANO_ANT;

            [XmlElement("ANO_EMISS", Order = 2)] //* Campo não obrigatório para fundos com menos de um ano de funcionamento
            public int ANO_EMISS;

            [XmlIgnore]
            public decimal valor_resg; // * Campo não obrigatório para fundos com menos de um ano de funcionamento

            #region VALOR_RESG
            [XmlElement("VALOR_RESG", Order = 3)]
            public string format_VALOR_RESG {

                get {
                    return valor_resg.ToString("N2").Replace(".", "");
                }
                set {
                    decimal valor_resg = 0;

                    if (Decimal.TryParse(value, out valor_resg))
                        valor_resg = valor_resg;
                }
            }
            #endregion

            [XmlIgnore]
            public decimal impostos; // * Campo não obrigatório para fundos com menos de um ano de funcionamento

            #region IMPOSTOS
            [XmlElement("IMPOSTOS", Order = 4)]
            public string format_IMPOSTOS {

                get {
                    return impostos.ToString("N2").Replace(".", "");
                }
                set {
                    decimal impostos = 0;

                    if (Decimal.TryParse(value, out valor_resg))
                        impostos = impostos;
                }
            }
            #endregion

            [XmlIgnore]
            public decimal valor_ing;  // *CAMPO OPCIONAL

            #region VALOR_ING
            [XmlElement("VALOR_ING", Order = 5)]
            public string format_VALOR_ING {

                get {
                    return valor_ing.ToString("N2").Replace(".", "");
                }
                set {
                    decimal valor_ing = 0;

                    if (Decimal.TryParse(value, out valor_ing))
                        valor_ing = valor_ing;
                }
            }
            #endregion

            [XmlIgnore]
            public decimal valor_saida;  // *CAMPO OPCIONAL

            #region VALOR_SAIDA
            [XmlElement("VALOR_SAIDA", Order = 6)]
            public string format_VALOR_SAIDA {

                get {
                    return valor_saida.ToString("N2").Replace(".", "");
                }
                set {
                    decimal valor_saida = 0;

                    if (Decimal.TryParse(value, out valor_saida))
                        valor_saida = valor_saida;
                }
            }
            #endregion

            [XmlIgnore]
            public decimal ajuste_perfor;  // *CAMPO OPCIONAL

            #region AJUSTE_PERFOR
            [XmlElement("AJUSTE_PERFOR", Order = 7)]
            public string format_AJUSTE_PERFOR {

                get {
                    return ajuste_perfor.ToString("N2").Replace(".", "");
                }
                set {
                    decimal ajuste_perfor = 0;

                    if (Decimal.TryParse(value, out ajuste_perfor))
                        ajuste_perfor = ajuste_perfor;
                }
            }
            #endregion

            [XmlIgnore]
            public decimal valor_despesas;  // *CAMPO OPCIONAL

            #region VALOR_DESPESAS
            [XmlElement("VALOR_DESPESAS", Order = 8)]
            public string format_VALOR_DESPESAS {

                get {
                    return valor_despesas.ToString("N2").Replace(".", "");
                }
                set {
                    decimal ajustevalor_despesas_perfor = 0;

                    if (Decimal.TryParse(value, out valor_despesas))
                        valor_despesas = valor_despesas;
                }
            }
            #endregion
        }
        #endregion

        #region TAB_SIMUL_DESPESAS
        public class TAB_SIMUL_DESPESAS {
            // Construtor
            public TAB_SIMUL_DESPESAS() { }

            [XmlIgnore]
            public decimal desp_prev3;

            #region DESP_PREV3
            [XmlElement("DESP_PREV3", Order = 1)]
            public string format_DESP_PREV3 {

                get {
                    return desp_prev3.ToString("N2").Replace(".", "");
                }
                set {
                    decimal desp_prev3 = 0;

                    if (Decimal.TryParse(value, out desp_prev3))
                        desp_prev3 = desp_prev3;
                }
            }
            #endregion

            [XmlIgnore]
            public decimal desp_prev5;

            #region DESP_PREV5
            [XmlElement("DESP_PREV5", Order = 2)]
            public string format_DESP_PREV5 {

                get {
                    return desp_prev5.ToString("N2").Replace(".", "");
                }
                set {
                    decimal desp_prev5 = 0;

                    if (Decimal.TryParse(value, out desp_prev5))
                        desp_prev5 = desp_prev5;
                }
            }
            #endregion

            [XmlIgnore]
            public decimal retor_brut3;

            #region RETOR_BRUT3
            [XmlElement("RETOR_BRUT3", Order = 3)]
            public string format_RETOR_BRUT3 {

                get {
                    return retor_brut3.ToString("N2").Replace(".", "");
                }
                set {
                    decimal retor_brut3 = 0;

                    if (Decimal.TryParse(value, out retor_brut3))
                        retor_brut3 = retor_brut3;
                }
            }
            #endregion

            [XmlIgnore]
            public decimal retor_brut5;

            #region RETOR_BRUT5
            [XmlElement("RETOR_BRUT5", Order = 4)]
            public string format_RETOR_BRUT5 {

                get {
                    return retor_brut5.ToString("N2").Replace(".", "");
                }
                set {
                    decimal retor_brut5 = 0;

                    if (Decimal.TryParse(value, out retor_brut5))
                        retor_brut5 = retor_brut5;
                }
            }
            #endregion
        }
        #endregion
    }

    public class LaminaEssencialXML {

        #region Atributos Privates
        private int ano;

        public int Ano {
            get { return ano; }
            set { ano = value; }
        }

        // Armazena os últimos 5 anos em relação ao ano passado em ordem crescente
        private List<int> ultimos_5_Anos;

        /* Valores da Table1 */
        private List<CalculoMedida.EstatisticaRetornoMensal> listaRetornosMensais;

        /* Valores da Table2 */
        private List<CalculoMedida.EstatisticaRetornoAnual> listaRetornosAnuais;

        #endregion

        #region Funções Personalizadas

        // Ordena um dicionario por valor
        private Dictionary<string, decimal> SortDictionaryByValue(Dictionary<string, decimal> myDictionary) {
            List<KeyValuePair<string, decimal>> tempList = new List<KeyValuePair<string, decimal>>(myDictionary);

            tempList.Sort(delegate(KeyValuePair<string, decimal> firstPair, KeyValuePair<string, decimal> secondPair) {
                return firstPair.Value.CompareTo(secondPair.Value);
            }
                         );

            Dictionary<string, decimal> mySortedDictionary = new Dictionary<string, decimal>();
            foreach (KeyValuePair<string, decimal> pair in tempList) {
                mySortedDictionary.Add(pair.Key, pair.Value);
            }

            return mySortedDictionary;
        }

        // Inicializa as Rentabilidades
        public void InitializeRentabilidades(int idCliente, DateTime data) {

            // Carrega a Cliente
            Cliente cliente = new Cliente();
            List<esQueryItem> campos = new List<esQueryItem>();
            campos.Add(cliente.Query.Nome);
            campos.Add(cliente.Query.Apelido);
            campos.Add(cliente.Query.IdCliente);
            campos.Add(cliente.Query.Apelido);
            campos.Add(cliente.Query.DataDia);
            //
            cliente.LoadByPrimaryKey(campos, idCliente);

            #region Rentabilidade Anual/Mensal
            this.ano = data.Year;

            this.ultimos_5_Anos = new List<int>(new int[5] { 
                this.ano - 4, this.ano - 3,
                this.ano - 2, this.ano - 1,
                this.ano
            });

            Carteira carteira = new Carteira();
            carteira.LoadByPrimaryKey(idCliente);

            //int mesInicio = 1;
            //DateTime dataInicio = new DateTime(this.ano, mesInicio, 01);
            DateTime dataInicioAux = data.AddMonths(-11);
            DateTime dataInicio = new DateTime(dataInicioAux.Year, dataInicioAux.Month, 01);

            int mesFinal = data.Month;
            DateTime dataFim = new DateTime(this.ano, mesFinal, 01);
            //
            dataFim = Calendario.RetornaUltimoDiaCorridoMes(dataFim, 0);
            //            
            DateTime dataDia = cliente.DataDia.Value;

            //Ajusta datas pela data de inicio da cota e dataDia do cliente
            if (dataFim > dataDia) {
                dataFim = dataDia;
            }

            if (dataInicio < carteira.DataInicioCota.Value) {
                dataInicio = carteira.DataInicioCota.Value;
            }

            CalculoMedida calculoMedida = new CalculoMedida(idCliente);
            calculoMedida.SetIdIndice(carteira.IdIndiceBenchmark.Value);
            calculoMedida.SetBenchmarkPercentual(true);
            calculoMedida.SetBuscaCotaMaisProxima(true);
            this.listaRetornosMensais = calculoMedida.RetornaListaRetornosMensais(dataInicio, dataFim);

            #region Busca retornos mensais e anuais
            DateTime dataAnteriorAno = new DateTime(dataInicio.Year - 4, 01, 01); //Data 5 anos anteriores

            if (dataAnteriorAno < carteira.DataInicioCota.Value) {
                dataAnteriorAno = carteira.DataInicioCota.Value;
            }
            #endregion

            this.listaRetornosAnuais = calculoMedida.RetornaListaRetornosAnuais(dataAnteriorAno, dataFim);
            #endregion
        }

        /// <summary>
        /// Calculo de Despesas
        /// </summary>
        /// <returns></returns>
        private decimal CalculaDespesas(DateTime dataPosicao, int idCliente) {
            DateTime dataInicio = Calendario.RetornaPrimeiroDiaUtilAno(new DateTime(dataPosicao.Year - 1, 1, 1));
            DateTime dataFim = Calendario.RetornaPrimeiroDiaUtilAno(new DateTime(dataPosicao.Year, 1, 1));

            #region
            LiquidacaoQuery liquidacaoQuery = new LiquidacaoQuery("L");
            ClienteQuery clienteQuery = new ClienteQuery("C");

            liquidacaoQuery.InnerJoin(clienteQuery).On(clienteQuery.IdCliente == liquidacaoQuery.IdCliente);
            //
            liquidacaoQuery.Select(liquidacaoQuery.Valor.Sum());
            liquidacaoQuery.Where(liquidacaoQuery.IdCliente == idCliente,
                                  liquidacaoQuery.DataVencimento.Between(dataInicio, dataFim),
                                  liquidacaoQuery.Origem.In((int)OrigemLancamentoLiquidacao.Provisao.PagtoTaxaAdministracao,
                                                             (int)OrigemLancamentoLiquidacao.Provisao.PagtoTaxaGestao,
                                                             (int)OrigemLancamentoLiquidacao.Provisao.PagtoTaxaPerformance,
                                                             (int)OrigemLancamentoLiquidacao.Provisao.PagtoTaxaFiscalizacaoCVM,
                                                             (int)OrigemLancamentoLiquidacao.Provisao.PagtoProvisaoOutros
                                                            ));
            Liquidacao liquidacao = new Liquidacao();
            liquidacao.Load(liquidacaoQuery);
            #endregion

            decimal despesas = liquidacao.Valor.HasValue ? Math.Abs(liquidacao.Valor.Value) : 0;
            return despesas;
        }

        #region Rentabilidade Mensal/Anual

        #region Preenche os valores das tables

        private decimal? CalculaRentabilidade12Meses(List<CalculoMedida.EstatisticaRetornoMensal> rentabilidades) {

            // Lista com Retornos Mensais não Nulos
            List<decimal> rentabilidadesM = new List<decimal>();

            for (int i = 0; i < rentabilidades.Count; i++) {
                CalculoMedida.EstatisticaRetornoMensal c = rentabilidades[i];
                if (c.Retorno.HasValue) {
                    rentabilidadesM.Add(c.Retorno.Value);
                }
            }

            if (rentabilidadesM.Count == 0) {
                return null;
            }

            // Rentabilidade em fator
            List<decimal> rentabilidadesFator = new List<decimal>(rentabilidadesM.Count);
            for (int i = 0; i < rentabilidadesM.Count; i++) {
                decimal valor = (rentabilidadesM[i] / 100) + 1;
                rentabilidadesFator.Add(valor);
            }

            decimal? rentabilidadeAnual = null;
            for (int j = 0; j < rentabilidadesFator.Count; j++) {
                if (j == 0) {
                    rentabilidadeAnual = rentabilidadesFator[j];
                }
                else {
                    rentabilidadeAnual *= rentabilidadesFator[j];
                }
            }
            return (rentabilidadeAnual - 1) * 100;
        }

        /// <summary>
        /// Calcula Rentabilidade 12 meses do Indice baseado nas rentabilidades Mensais
        /// </summary>
        /// <param name="rentabilidades"></param>
        /// <returns>Null se não conseguiu calcular valor</returns>
        private decimal? CalculaRentabilidade12MesesIndice(List<CalculoMedida.EstatisticaRetornoMensal> rentabilidades) {

            // Lista com Retornos Mensais do Benchamark não Nulos
            List<decimal> rentabilidadesM = new List<decimal>();

            for (int i = 0; i < rentabilidades.Count; i++) {
                CalculoMedida.EstatisticaRetornoMensal c = rentabilidades[i];
                if (c.RetornoBenchmark.HasValue) {
                    rentabilidadesM.Add(c.RetornoBenchmark.Value);
                }
            }

            if (rentabilidadesM.Count == 0) {
                return null;
            }

            // Rentabilidade em fator
            List<decimal> rentabilidadesFator = new List<decimal>(rentabilidadesM.Count);
            for (int i = 0; i < rentabilidadesM.Count; i++) {
                decimal valor = (rentabilidadesM[i] / 100) + 1;
                rentabilidadesFator.Add(valor);
            }

            decimal? rentabilidadeAnualIndice = null;
            for (int j = 0; j < rentabilidadesFator.Count; j++) {
                if (j == 0) {
                    rentabilidadeAnualIndice = rentabilidadesFator[j];
                }
                else {
                    rentabilidadeAnualIndice *= rentabilidadesFator[j];
                }
            }
            return (rentabilidadeAnualIndice - 1) * 100;
        }

        #endregion
        #endregion

        #endregion

        /// <summary>
        /// Retorna uma Stream de Mémoria de um arquivo xml. A Stream permanece aberta
        /// </summary>
        /// <param name="dataPosicao"></param>
        /// <param name="idClientes">lista de idClientes</param>
        /// <param name="ms">Saida: Memory Stream do arquivo Lamina</param>
        /// <param name="nomeArquivo">Saida: Nome do Arquivo Lamina</param>        
        /// <returns></returns>
        public void ExportaLaminaXML(DateTime dataPosicao, List<int> idClientes, out MemoryStream ms, out string nomeArquivo) {
            ms = new MemoryStream();
            nomeArquivo = "Lamina.xml";
            //            
            XML_Lamina.Cabecalho c = this.MontaCabecalho(dataPosicao);
            List<XML_Lamina.Corpo> conteudo = this.MontaConteudo(dataPosicao, idClientes);
            //
            XML_Lamina arq = new XML_Lamina();
            arq.cabecalho = c;
            arq.corpo.AddRange(conteudo);
            //                     
            XmlSerializer x = new XmlSerializer(arq.GetType());

            XmlWriterSettings settings = new XmlWriterSettings();
            settings.Encoding = System.Text.Encoding.GetEncoding("WINDOWS-1252");
            settings.Indent = true;
            settings.IndentChars = "\t";
            settings.NewLineChars = Environment.NewLine;
            settings.ConformanceLevel = ConformanceLevel.Document;

            using (XmlWriter writer = XmlTextWriter.Create(ms, settings)) {
                x.Serialize(writer, arq);
            }
        }

        /// <summary>
        /// Monta o Cabeçalho do arquivo XML
        /// </summary>
        /// <param name="dataPosicao"></param>
        /// <returns></returns>
        private XML_Lamina.Cabecalho MontaCabecalho(DateTime dataPosicao) {
            XML_Lamina.Cabecalho c = new XML_Lamina.Cabecalho();
            //
            c.codigoDocumento = 24;
            c.versao = "2.0";
            c.dataCompetencia = dataPosicao;
            c.dataGeracaoArquivo = DateTime.Now;

            //
            return c;
        }

        /// <summary>
        /// Monta o conteudo do arquivo XML
        /// </summary>
        /// <param name="dataPosicao"></param>
        /// <param name="idClientes">Lista de IdClientes</param>
        /// <returns></returns>
        private List<XML_Lamina.Corpo> MontaConteudo(DateTime dataPosicao, List<int> idClientes) {
            List<XML_Lamina.Corpo> lista = new List<XML_Lamina.Corpo>();

            //
            #region Consulta Inicial
            PessoaQuery pessoaQuery = new PessoaQuery("P");
            ClienteQuery clienteQuery = new ClienteQuery("C");
            //CarteiraQuery carteiraQuery = new CarteiraQuery("A");

            //
            ClienteCollection clienteCollection = new ClienteCollection();
            //
            clienteQuery.Select(clienteQuery, pessoaQuery.Cpfcnpj);
            //
            clienteQuery.InnerJoin(pessoaQuery).On(pessoaQuery.IdPessoa == clienteQuery.IdCliente);
            //clienteQuery.InnerJoin(carteiraQuery).On(carteiraQuery.IdCarteira == clienteQuery.IdCliente);
            //
            //clienteQuery.Where(clienteQuery.TipoControle == (byte)TipoControleCliente.Completo &&
            //                   clienteQuery.IdTipo.In((int)TipoClienteFixo.Fundo) &&
            //                   clienteQuery.IdCliente.In(idClientes));
            ////
            clienteQuery.Where(clienteQuery.IdCliente.In(idClientes));
            //
            clienteQuery.OrderBy(pessoaQuery.Cpfcnpj.Descending);
            //
            clienteCollection.Load(clienteQuery);
            //
            #endregion

            // Carrega Lamina
            DateTime dataUltimoDiaMes = Calendario.RetornaUltimoDiaUtilMes(dataPosicao, 0);

            //
            for (int i = 0; i < clienteCollection.Count; i++) {
                int idCliente = clienteCollection[i].IdCliente.Value;
                //DateTime dataDia = clienteCollection[i].DataDia.Value;

                this.InitializeRentabilidades(idCliente, dataUltimoDiaMes);

                #region Lamina
                LaminaCollection infoCollection = new LaminaCollection();

                // Busca todas as informações menores que a data de referencia ordenado decrescente pela data
                infoCollection.Query
                     .Where(infoCollection.Query.InicioVigencia.LessThanOrEqual(dataUltimoDiaMes),
                            infoCollection.Query.IdCarteira == idCliente)
                     .OrderBy(infoCollection.Query.InicioVigencia.Descending);

                infoCollection.Query.Load();

                Lamina info = new Lamina();
                bool existeInfo = infoCollection.HasData;
                if (existeInfo) {
                    info = infoCollection[0];
                }
                #endregion

                Pessoa pessoa = new Pessoa();
                pessoa.LoadByPrimaryKey(clienteCollection[i].IdPessoa.Value);

                #region Conteudo arquivo XML
                //
                XML_Lamina.Corpo corpo = new XML_Lamina.Corpo();

                corpo.cnpj = pessoa.Cpfcnpj.Replace(".", "").Replace("-", "").Replace("/", "").Trim().PadLeft(14, '0');
                corpo.nomeFantasia = !String.IsNullOrEmpty(clienteCollection[i].Apelido) ? clienteCollection[i].Apelido.Trim() : "";
                corpo.enderecoEletronico = !String.IsNullOrEmpty(info.EnderecoEletronico) ? info.EnderecoEletronico.Trim() : "";
                corpo.descricaoPublicoAlvo = !String.IsNullOrEmpty(info.DescricaoPublicoAlvo) ? info.DescricaoPublicoAlvo.Trim() : "";
                corpo.RESTR_INV = ""; // TODO
                corpo.DESC_OBJET = !String.IsNullOrEmpty(info.ObjetivoFundo) ? info.ObjetivoFundo.Trim() : "";
                corpo.descricaoPoliticaInvestimento = !String.IsNullOrEmpty(info.DescricaoPoliticaInvestimento) ? info.DescricaoPoliticaInvestimento.Trim() : "";
                //
                // decimais
                corpo.ativo_ext = 0.00M;
                if (info.LimiteAplicacaoExterior.HasValue) {
                    corpo.ativo_ext = info.LimiteAplicacaoExterior.Value;
                }

                corpo.CRED_PRIV = 0.00M;
                if (info.LimiteCreditoPrivado.HasValue) {
                    corpo.CRED_PRIV = info.LimiteCreditoPrivado.Value;
                }

                corpo.ALAV = 0.00M;
                if (info.LimiteAlavancagem.HasValue) {
                    corpo.ALAV = info.LimiteAlavancagem.Value;
                }

                corpo.utilizaDerivativosCredito = !String.IsNullOrEmpty(info.DerivativosProtecaoCarteira)
                                                 ? info.DerivativosProtecaoCarteira.ToUpper() == "S" ? "S" : "N" : "";
                //
                corpo.perdasSignificativas = !String.IsNullOrEmpty(info.RegulamentoPerdasPatrimoniais) ? "S" : "N";
                //
                corpo.PL_NEGAT = !String.IsNullOrEmpty(info.RegulamentoPatrimonioNegativo) ? "S" : "N";
                //
                corpo.PERC_MAX = 0; // TODO
                //
                Carteira carteira = new Carteira();
                carteira.LoadByPrimaryKey(idCliente);
                //
                corpo.INV_INICIAL = carteira.ValorMinimoInicial.Value;
                corpo.INV_ADIC = carteira.ValorMinimoAplicacao.Value;
                corpo.RESG_MIN = carteira.ValorMinimoResgate.Value;
                //
                DateTime horaResgate = new DateTime(1900, 1, 1, 0, 0, 0);
                corpo.HOR_APL_RESG = horaResgate;

                if (carteira.HorarioFimResgate.HasValue) {
                    corpo.HOR_APL_RESG = carteira.HorarioFimResgate.Value;
                }
                //
                corpo.VALOR_PERM = carteira.ValorMinimoInicial.Value;
                //

                // campo 20 - campo Opcional
                this.ControeCarencia(dataPosicao, idCliente, corpo, info);

                // campo 21 
                this.ControeConversao(dataPosicao, idCliente, corpo, info);

                // campo 22 -
                int contagem = carteira.ContagemDiasConversaoResgate == (int)ContagemDiasLiquidacaoResgate.DiasUteis ? 1 : 2;
                corpo.TP_PRAZO_DIAS = contagem;

                // campo 23 -
                int diasLiquidacaoResgate = carteira.DiasLiquidacaoResgate.Value;
                corpo.PRAZO_PAGAM = carteira.DiasLiquidacaoResgate.Value.ToString();

                // campo 24
                this.ControeINFOR_TX_ADM(dataPosicao, idCliente, corpo, info);

                // campo 25 - Opcional
                this.ControeINFOR_TX_ENTR(dataPosicao, idCliente, corpo, info);

                // campo 26
                this.ControeINFOR_TX_SAIDA(dataPosicao, idCliente, corpo, info);

                #region TX_DESEMPENHO
                IndiceQuery indiceQuery = new IndiceQuery("I");
                TabelaTaxaPerformanceQuery tabelaTaxas = new TabelaTaxaPerformanceQuery("T");
                CarteiraQuery carteiraQuery = new CarteiraQuery("C");
                tabelaTaxas.Select(tabelaTaxas.IdTabela,
                                      tabelaTaxas.DataReferencia,
                                      tabelaTaxas.TaxaPerformance,
                                      tabelaTaxas.PercentualIndice,
                                      carteiraQuery.IdCarteira,
                                      carteiraQuery.Apelido,
                                      indiceQuery.Descricao);
                tabelaTaxas.InnerJoin(carteiraQuery).On(tabelaTaxas.IdCarteira == carteiraQuery.IdCarteira);
                tabelaTaxas.InnerJoin(indiceQuery).On(tabelaTaxas.IdIndice == indiceQuery.IdIndice);
                tabelaTaxas.Where(tabelaTaxas.IdCarteira == idCliente);

                TabelaTaxaPerformanceCollection tabelaTaxaPerformanceCollection = new TabelaTaxaPerformanceCollection();
                tabelaTaxaPerformanceCollection.Load(tabelaTaxas);

                corpo.tx_desempenho = "0";
                if (tabelaTaxaPerformanceCollection.HasData) {
                    decimal? taxaPerformance = tabelaTaxaPerformanceCollection[0].TaxaPerformance;
                    decimal? percentualIndicePerformance = tabelaTaxaPerformanceCollection[0].PercentualIndice;
                    string descricaoIndicie = tabelaTaxaPerformanceCollection[0].GetColumn(IndiceMetadata.ColumnNames.Descricao).ToString();
                    if (taxaPerformance.HasValue) {
                        corpo.tx_desempenho = taxaPerformance.Value.ToString("N2") + "%" + " do que exceder a " + percentualIndicePerformance.Value.ToString("N2") + "% do " + descricaoIndicie;
                    }
                }
                #endregion

                // campo 28 - * Campo não obrigatório para fundos com menos de um ano de funcionamento
                this.ControeINFOR_DESPESAS(dataPosicao, idCliente, corpo, info);

                // campo 29 - * Campo não obrigatório para fundos com menos de um ano de funcionamento
                this.ControeLISTA_COMP_CARTEIRA(dataPosicao, idCliente, corpo, info);

                // campo 30
                corpo.classificacaoRisco = info.Risco.HasValue ? info.Risco.Value : 0;

                // campo 31
                this.ControeINFOR_TAB_RENT_ANUAL(dataPosicao, idCliente, corpo, info);

                // campo 32
                this.ControeLISTA_TAB_RENT_ANUAL(dataPosicao, idCliente, corpo, info);

                // campo 33
                this.ControeLISTA_TAB_RENT_MENSAL(dataPosicao, idCliente, corpo, info);

                // campo 34 -  * Campo não obrigatório para fundos com menos de um ano de funcionamento
                corpo.FORM_CALC_RENT = !String.IsNullOrEmpty(info.CenariosApuracaoRentabilidade) ? info.CenariosApuracaoRentabilidade : "";

                // campo 35
                this.ControeTAB_VAR_DESEMP(dataPosicao, idCliente, corpo, info);

                // campo 36 - * Campo não obrigatório para fundos com menos de um ano de funcionamento
                this.ControeINFORM_EXEMPLO_COMP(dataPosicao, idCliente, corpo, info);

                // campo 37 - * Campo não obrigatório para fundos com menos de um ano de funcionamento
                this.ControeTAB_SIMUL_DESPESAS(dataPosicao, idCliente, corpo, info);

                // campo 38
                corpo.DS_FORMA_REMUN_DISTR = "";

                // campo 39 
                corpo.PRINC_DISTR_OFERTA = "";

                // campo 40
                corpo.PRINC_CONFLITO_INTERES = "";

                // campo 41
                string telefone = !String.IsNullOrEmpty(info.Telefone) ? info.Telefone.Trim() : "";

                string resultString = null;
                try {
                    Regex regexObj = new Regex(@"[^\d]");
                    resultString = regexObj.Replace(telefone, "");
                }
                catch (ArgumentException ex) {
                }

                corpo.telefone = !String.IsNullOrEmpty(resultString) ? Convert.ToInt64(resultString) : 0;

                // campo 42
                corpo.reclamacoes = !String.IsNullOrEmpty(info.Reclamacoes) ? info.Reclamacoes.Trim() : "";

                // campo 43
                corpo.INFO_ADIC = "";

                lista.Add(corpo);
                //
                #endregion
            }
            //
            return lista;
        }

        /// <summary>
        /// Constroe o objeto Carencia
        /// </summary>
        /// <param name="dataPosicao"></param>
        /// <param name="idCliente"></param>
        /// <param name="corpo">Objeto representativo do Corpo do arquivo XML</param>
        private void ControeCarencia(DateTime dataPosicao, int idCliente, XML_Lamina.Corpo corpo, Lamina info) {

            #region Controe Carencia

            //XML_Lamina.CONVERSAO elem = new XML_Lamina.CONVERSAO();
            //
            //elem.ABERT_FECH = !String.IsNullOrEmpty(info.LocalFormaDivulgacao) ? info.LocalFormaDivulgacao.Trim() : "";
            //
            corpo.getCARENCIA.DIAS_CAR = 0;
            if (!String.IsNullOrEmpty(info.PrazoCarencia)) {
                corpo.getCARENCIA.DIAS_CAR = Convert.ToInt32(info.PrazoCarencia);
            }

            corpo.getCARENCIA.COND_CAR = "";

            #endregion
        }

        /// <summary>
        /// Constroe o objeto Conversao
        /// </summary>
        /// <param name="dataPosicao"></param>
        /// <param name="idCliente"></param>
        /// <param name="corpo">Objeto representativo do Corpo do arquivo XML</param>
        private void ControeConversao(DateTime dataPosicao, int idCliente, XML_Lamina.Corpo corpo, Lamina info) {

            #region Controe Conversao

            //XML_Lamina.CONVERSAO elem = new XML_Lamina.CONVERSAO();
            //
            //elem.ABERT_FECH = !String.IsNullOrEmpty(info.LocalFormaDivulgacao) ? info.LocalFormaDivulgacao.Trim() : "";
            //

            Carteira carteira = new Carteira();
            carteira.LoadByPrimaryKey(idCliente);

            int tipoCota = (int)carteira.TipoCota.Value;
            int diasConversaoAplic = carteira.DiasCotizacaoAplicacao.Value;
            int diasConversaoResgate = carteira.DiasCotizacaoResgate.Value;
            string contagem = carteira.ContagemDiasConversaoResgate == (int)ContagemDiasLiquidacaoResgate.DiasUteis ? "úteis" : "corridos";
            string cota = tipoCota == (int)TipoCotaFundo.Abertura ? "A" : "F";

            //
            corpo.getCONVERSAO.ABERT_FECH = cota;
            corpo.getCONVERSAO.DIA_APLIC = diasConversaoAplic;
            corpo.getCONVERSAO.ABERT_FECH_C = cota;
            corpo.getCONVERSAO.DIA_RESG = diasConversaoResgate;

            #endregion
        }

        /// <summary>
        /// Constroe o objeto INFOR_TX_ADM
        /// </summary>
        /// <param name="dataPosicao"></param>
        /// <param name="idCliente"></param>
        /// <param name="corpo">Objeto representativo do Corpo do arquivo XML</param>
        private void ControeINFOR_TX_ADM(DateTime dataPosicao, int idCliente, XML_Lamina.Corpo corpo, Lamina info) {

            CadastroTaxaAdministracaoQuery cadastroTaxaAdm = new CadastroTaxaAdministracaoQuery("D");
            TabelaTaxaAdministracaoQuery tabelaTaxaAdministracaoQuery = new TabelaTaxaAdministracaoQuery("T");
            CarteiraQuery carteiraQuery = new CarteiraQuery("C");

            tabelaTaxaAdministracaoQuery.Select(tabelaTaxaAdministracaoQuery.IdTabela, tabelaTaxaAdministracaoQuery.IdCarteira,
                                                tabelaTaxaAdministracaoQuery.DataReferencia, tabelaTaxaAdministracaoQuery.Taxa,
                                                tabelaTaxaAdministracaoQuery.ValorMaximo, tabelaTaxaAdministracaoQuery.ValorMinimo,
                                                tabelaTaxaAdministracaoQuery.TipoCalculo, carteiraQuery.Apelido);
            tabelaTaxaAdministracaoQuery.InnerJoin(carteiraQuery).On(tabelaTaxaAdministracaoQuery.IdCarteira == carteiraQuery.IdCarteira);
            tabelaTaxaAdministracaoQuery.InnerJoin(cadastroTaxaAdm).On(tabelaTaxaAdministracaoQuery.IdCadastro == cadastroTaxaAdm.IdCadastro);
            tabelaTaxaAdministracaoQuery.Where((tabelaTaxaAdministracaoQuery.IdCarteira == idCliente),
                                               (cadastroTaxaAdm.Descricao.ToLower().Equal("TAXA ADMINISTRAÇÃO")));

            TabelaTaxaAdministracao tab = new TabelaTaxaAdministracao();

            if (tab.Load(tabelaTaxaAdministracaoQuery)) {
                decimal? taxa = tab.Taxa;

                int tipoCalculo = tab.TipoCalculo.Value;
                //
                corpo.getINFOR_TX_ADM.TIPO_TX_ADM = tipoCalculo == (int)TipoCalculoAdministracao.ValorFixo ? "V" : "F";

                if (tipoCalculo == (int)TipoCalculoAdministracao.PercentualPL) {
                    corpo.getINFOR_TX_ADM.tx_adm = taxa.HasValue ? taxa.Value : 0;
                }
                else {
                    corpo.getINFOR_TX_ADM.tx_min = tab.ValorMinimo.Value;
                    corpo.getINFOR_TX_ADM.tx_max = tab.ValorMaximo.Value;
                }

                corpo.getINFOR_TX_ADM.ESCLAREC = "";
            }
        }

        /// <summary>
        /// Constroe o objeto INFOR_TX_ENTR
        /// </summary>
        /// <param name="dataPosicao"></param>
        /// <param name="idCliente"></param>
        /// <param name="corpo">Objeto representativo do Corpo do arquivo XML</param>
        private void ControeINFOR_TX_ENTR(DateTime dataPosicao, int idCliente, XML_Lamina.Corpo corpo, Lamina info) {

            #region Controe INFOR_TX_ENTR

            corpo.getINFOR_TX_ENTR.TX_ENTR = !String.IsNullOrEmpty(info.TaxaEntrada) ? Convert.ToInt32(info.TaxaEntrada.Trim()) : 0;
            corpo.getINFOR_TX_ENTR.COND_ENTR = "";
            #endregion
        }

        /// <summary>
        /// Constroe o objeto INFOR_TX_SAIDA
        /// </summary>
        /// <param name="dataPosicao"></param>
        /// <param name="idCliente"></param>
        /// <param name="corpo">Objeto representativo do Corpo do arquivo XML</param>
        private void ControeINFOR_TX_SAIDA(DateTime dataPosicao, int idCliente, XML_Lamina.Corpo corpo, Lamina info) {

            Carteira carteira = new Carteira();
            carteira.LoadByPrimaryKey(idCliente);
            //
            int diasConversaoAplic = carteira.DiasCotizacaoAplicacao.Value;

            #region Controe INFOR_TX_SAIDA

            corpo.getINFOR_TX_SAIDA.DIAS_SAIDA = diasConversaoAplic;
            corpo.getINFOR_TX_SAIDA.TX_SAIDA = !String.IsNullOrEmpty(info.TaxaSaida) ? Convert.ToInt32(info.TaxaSaida.Trim()) : 0;
            corpo.getINFOR_TX_SAIDA.COND_SAIDA = "";
            #endregion
        }

        /// <summary>
        /// Constroe o objeto INFOR_DESPESAS
        /// </summary>
        /// <param name="dataPosicao"></param>
        /// <param name="idCliente"></param>
        /// <param name="corpo">Objeto representativo do Corpo do arquivo XML</param>
        private void ControeINFOR_DESPESAS(DateTime dataPosicao, int idCliente, XML_Lamina.Corpo corpo, Lamina info) {

            #region Liquidacao

            DateTime data2 = Calendario.RetornaUltimoDiaUtilMes(dataPosicao, 0);
            DateTime dataAux = data2.AddMonths(-12);
            DateTime data1 = Calendario.RetornaUltimoDiaUtilMes(dataAux, 0);
            //
            LiquidacaoQuery liquidacaoQuery = new LiquidacaoQuery("L");
            ClienteQuery clienteQuery = new ClienteQuery("C");

            liquidacaoQuery.InnerJoin(clienteQuery).On(clienteQuery.IdCliente == liquidacaoQuery.IdCliente);
            //
            liquidacaoQuery.Select(liquidacaoQuery.Valor.Sum());
            liquidacaoQuery.Where(liquidacaoQuery.IdCliente == idCliente,
                                  liquidacaoQuery.DataVencimento.Between(data1, data2),
                                  liquidacaoQuery.Situacao == (Int16)SituacaoLancamentoLiquidacao.Normal,
                                  liquidacaoQuery.Valor < 0,
                                  clienteQuery.IdTipo.In((int)TipoClienteFixo.Fundo));

            Liquidacao liquidacao = new Liquidacao();
            liquidacao.Load(liquidacaoQuery);
            #endregion

            decimal despesas = liquidacao.Valor.HasValue ? Math.Abs(liquidacao.Valor.Value) : 0;

            #region PL
            HistoricoCota historicoCota = new HistoricoCota();
            historicoCota.Query
                 .Select(historicoCota.Query.PLFechamento.Sum())
                 .Where(historicoCota.Query.IdCarteira == idCliente,
                        historicoCota.Query.Data.Between(data1, data2));

            historicoCota.Query.Load();
            decimal plFechamento = historicoCota.PLFechamento.HasValue ? historicoCota.PLFechamento.Value : 0;
            #endregion

            // Média das despesas por patrimonio do fundo. 
            decimal? media = null;
            if (plFechamento > 0) {
                media = despesas / plFechamento;
            }

            #region Controe INFOR_DESPESAS
            corpo.getINFOR_DESPESAS.despesas = media.HasValue ? media.Value : 0;
            corpo.getINFOR_DESPESAS.inic_periodo = data1;
            corpo.getINFOR_DESPESAS.fim_periodo = data2;
            corpo.getINFOR_DESPESAS.END_ELET_ADM = !String.IsNullOrEmpty(info.EnderecoEletronico) ? info.EnderecoEletronico.Trim() : "";
            #endregion
        }

        /// <summary>
        /// Constroe o objeto LISTA_COMP_CARTEIRA
        /// </summary>
        /// <param name="dataPosicao"></param>
        /// <param name="idCliente"></param>
        /// <param name="corpo">Objeto representativo do Corpo do arquivo XML</param>
        private void ControeLISTA_COMP_CARTEIRA(DateTime dataPosicao, int idCliente, XML_Lamina.Corpo corpo, Lamina info) {

            #region Calculos
            DateTime dataAux = Calendario.RetornaUltimoDiaUtilMes(dataPosicao);

            HistoricoCota historicoCota = new HistoricoCota();
            historicoCota.LoadByPrimaryKey(dataAux, idCliente);
            decimal plFechamento = historicoCota.PLFechamento.HasValue ? historicoCota.PLFechamento.Value : 0;

            decimal porcentagem1 = 0.0M;
            decimal porcentagem2 = 0.0M;
            decimal porcentagem3 = 0.0M;
            decimal porcentagem4 = 0.0M;
            decimal porcentagem5 = 0.0M;
            decimal porcentagem6 = 0.0M;
            decimal porcentagem7 = 0.0M;
            decimal porcentagem8 = 0.0M;
            decimal porcentagem9 = 0.0M;
            decimal porcentagem10 = 0.0M;
            decimal porcentagem11 = 0.0M;

            #region 1
            PosicaoRendaFixaHistoricoQuery posicaoRendaFixaQuery = new PosicaoRendaFixaHistoricoQuery("P");
            TituloRendaFixaQuery tituloRendaFixaQuery = new TituloRendaFixaQuery("T");
            PapelRendaFixaQuery papelRendaFixaQuery = new PapelRendaFixaQuery("A");

            posicaoRendaFixaQuery.Select(posicaoRendaFixaQuery.ValorMercado.Sum());
            //
            posicaoRendaFixaQuery.InnerJoin(tituloRendaFixaQuery).On(tituloRendaFixaQuery.IdTitulo == posicaoRendaFixaQuery.IdTitulo);
            posicaoRendaFixaQuery.InnerJoin(papelRendaFixaQuery).On(papelRendaFixaQuery.IdPapel == tituloRendaFixaQuery.IdPapel);
            //
            posicaoRendaFixaQuery.Where(posicaoRendaFixaQuery.IdCliente == idCliente,
                                        posicaoRendaFixaQuery.DataHistorico == dataAux,
                                        posicaoRendaFixaQuery.TipoOperacao.Equal((byte)TipoOperacaoTitulo.CompraFinal),
                //tituloRendaFixaQuery.CodigoCustodia.IsNotNull(),
                //tituloRendaFixaQuery.CodigoCustodia.NotEqual(""),
                                        papelRendaFixaQuery.TipoPapel.Equal((byte)TipoPapelTitulo.Publico));

            PosicaoRendaFixaHistoricoCollection coll = new PosicaoRendaFixaHistoricoCollection();
            coll.Load(posicaoRendaFixaQuery);

            decimal valormercado = 0.0M;
            if (coll.HasData) {
                if (coll[0].ValorMercado.HasValue) {
                    valormercado = coll[0].ValorMercado.Value;
                }
            }

            if (plFechamento != 0) {
                porcentagem1 = valormercado / plFechamento;
            }

            #endregion

            #region 2
            posicaoRendaFixaQuery = new PosicaoRendaFixaHistoricoQuery("O");
            tituloRendaFixaQuery = new TituloRendaFixaQuery("T");
            PapelRendaFixaQuery papelRendaFixa = new PapelRendaFixaQuery("P");

            posicaoRendaFixaQuery.Select(posicaoRendaFixaQuery.ValorMercado.Sum());
            posicaoRendaFixaQuery.InnerJoin(tituloRendaFixaQuery).On(posicaoRendaFixaQuery.IdTitulo == tituloRendaFixaQuery.IdTitulo);
            posicaoRendaFixaQuery.InnerJoin(papelRendaFixa).On(tituloRendaFixaQuery.IdPapel == papelRendaFixa.IdPapel);
            posicaoRendaFixaQuery.Where(posicaoRendaFixaQuery.IdCliente == idCliente,
                                         posicaoRendaFixaQuery.DataHistorico == dataAux,
                                         posicaoRendaFixaQuery.TipoOperacao.Equal((byte)TipoOperacaoTitulo.CompraRevenda),
                                         papelRendaFixa.TipoPapel.Equal((byte)TipoPapelTitulo.Publico)
                                        );

            PosicaoRendaFixaHistoricoCollection oper = new PosicaoRendaFixaHistoricoCollection();

            oper.Load(posicaoRendaFixaQuery);

            decimal valor2 = 0.0M;
            if (oper.HasData) {
                if (oper[0].ValorMercado.HasValue) {
                    valor2 = oper[0].ValorMercado.Value;
                }
            }

            if (plFechamento != 0) {
                porcentagem2 = valor2 / plFechamento;
            }

            #endregion

            #region 3
            posicaoRendaFixaQuery = new PosicaoRendaFixaHistoricoQuery("O");
            tituloRendaFixaQuery = new TituloRendaFixaQuery("T");
            papelRendaFixa = new PapelRendaFixaQuery("P");

            posicaoRendaFixaQuery.Select(posicaoRendaFixaQuery.ValorMercado.Sum());
            posicaoRendaFixaQuery.InnerJoin(tituloRendaFixaQuery).On(posicaoRendaFixaQuery.IdTitulo == tituloRendaFixaQuery.IdTitulo);
            posicaoRendaFixaQuery.InnerJoin(papelRendaFixa).On(tituloRendaFixaQuery.IdPapel == papelRendaFixa.IdPapel);
            posicaoRendaFixaQuery.Where(posicaoRendaFixaQuery.IdCliente == idCliente,
                                         posicaoRendaFixaQuery.DataHistorico == dataAux,
                                         posicaoRendaFixaQuery.TipoOperacao.Equal((byte)TipoOperacaoTitulo.CompraRevenda),
                                         papelRendaFixa.TipoPapel.Equal((byte)TipoPapelTitulo.Privado)
                                        );

            oper = new PosicaoRendaFixaHistoricoCollection();
            oper.Load(posicaoRendaFixaQuery);

            decimal valor3 = 0.0M;
            if (oper.HasData) {
                if (oper[0].ValorMercado.HasValue) {
                    valor3 = oper[0].ValorMercado.Value;
                }
            }

            if (plFechamento != 0) {
                porcentagem3 = valor3 / plFechamento;
            }
            #endregion

            #region 4
            PosicaoBolsaHistoricoQuery posicaoBolsaQuery = new PosicaoBolsaHistoricoQuery("P");
            ClienteQuery clienteQuery = new ClienteQuery("L");
            //
            posicaoBolsaQuery.Select(posicaoBolsaQuery.ValorMercado.Sum());
            posicaoBolsaQuery.InnerJoin(clienteQuery).On(clienteQuery.IdCliente == posicaoBolsaQuery.IdCliente);

            posicaoBolsaQuery.Where(posicaoBolsaQuery.IdCliente == idCliente,
                                    posicaoBolsaQuery.DataHistorico == dataAux,
                                    clienteQuery.IdMoeda == (int)ListaMoedaFixo.Real);

            PosicaoBolsaHistorico p = new PosicaoBolsaHistorico();
            p.Load(posicaoBolsaQuery);

            decimal valor4 = 0.0M;
            if (p.ValorMercado.HasValue) {
                valor4 = p.ValorMercado.Value;
            }
            if (plFechamento != 0) {
                porcentagem4 = valor4 / plFechamento;
            }
            #endregion

            #region 5
            posicaoRendaFixaQuery = new PosicaoRendaFixaHistoricoQuery("P");
            tituloRendaFixaQuery = new TituloRendaFixaQuery("T");
            papelRendaFixaQuery = new PapelRendaFixaQuery("A");

            posicaoRendaFixaQuery.Select(posicaoRendaFixaQuery.ValorMercado.Sum());
            //
            posicaoRendaFixaQuery.InnerJoin(tituloRendaFixaQuery).On(tituloRendaFixaQuery.IdTitulo == posicaoRendaFixaQuery.IdTitulo);
            posicaoRendaFixaQuery.InnerJoin(papelRendaFixaQuery).On(papelRendaFixaQuery.IdPapel == tituloRendaFixaQuery.IdPapel);
            //
            posicaoRendaFixaQuery.Where(posicaoRendaFixaQuery.IdCliente == idCliente,
                                        posicaoRendaFixaQuery.DataHistorico == dataAux,
                                        posicaoRendaFixaQuery.TipoOperacao.Equal((byte)TipoOperacaoTitulo.CompraFinal),
                //tituloRendaFixaQuery.CodigoCustodia.IsNotNull(),
                //tituloRendaFixaQuery.CodigoCustodia.NotEqual(""),
                                        papelRendaFixaQuery.Classe.In((int)ClasseRendaFixa.CDB,
                //(int)ClasseRendaFixa.rdb,
                                                                        (int)ClasseRendaFixa.LF,
                                                                        (int)ClasseRendaFixa.DPGE,
                                                                        (int)ClasseRendaFixa.CCB,
                                                                        (int)ClasseRendaFixa.LCA,
                                                                        (int)ClasseRendaFixa.LCI)
                                        );

            coll = new PosicaoRendaFixaHistoricoCollection();
            coll.Load(posicaoRendaFixaQuery);

            valormercado = 0.0M;
            if (coll.HasData) {
                if (coll[0].ValorMercado.HasValue) {
                    valormercado = coll[0].ValorMercado.Value;
                }
            }

            #region SaldoCaixa
            SaldoCaixaCollection saldoCaixaCollection = new SaldoCaixaCollection();
            //
            SaldoCaixaQuery saldoCaixaQuery = new SaldoCaixaQuery("S");
            ContaCorrenteQuery contaCorrenteQuery = new ContaCorrenteQuery("C");
            MoedaQuery moedaQuery = new MoedaQuery("M");

            saldoCaixaQuery.Select(saldoCaixaQuery.SaldoFechamento.Sum());
            saldoCaixaQuery.InnerJoin(contaCorrenteQuery).On(saldoCaixaQuery.IdConta == contaCorrenteQuery.IdConta);
            saldoCaixaQuery.InnerJoin(moedaQuery).On(contaCorrenteQuery.IdMoeda == moedaQuery.IdMoeda);

            saldoCaixaQuery.Where(saldoCaixaQuery.IdCliente == idCliente,
                                  saldoCaixaQuery.Data == dataAux);

            saldoCaixaCollection.Load(saldoCaixaQuery);
            //

            decimal porcentagemSaldoCaixa = 0;
            if (saldoCaixaCollection.HasData) {
                if (saldoCaixaCollection[0].SaldoFechamento.HasValue) {
                    if (plFechamento != 0) {
                        porcentagemSaldoCaixa = saldoCaixaCollection[0].SaldoFechamento.Value / plFechamento;
                    }
                }
            }
            #endregion

            #region ValoresLiquidar
            LiquidacaoHistoricoCollection l = new LiquidacaoHistoricoCollection();

            l.Query.Select(l.Query.Descricao,
                           l.Query.IdConta,
                           l.Query.DataLancamento,
                           l.Query.DataVencimento,
                           l.Query.Valor.Sum())
                     .Where(l.Query.IdCliente == idCliente &&
                            l.Query.DataLancamento.LessThanOrEqual(dataAux) &
                            (
                                (l.Query.Situacao.In((byte)SituacaoLancamentoLiquidacao.Normal, (byte)SituacaoLancamentoLiquidacao.Compensacao) &
                                    l.Query.DataVencimento.GreaterThan(dataAux)) |
                                (l.Query.Situacao.In((byte)SituacaoLancamentoLiquidacao.Compensacao) &
                                    l.Query.DataVencimento.GreaterThanOrEqual(dataAux) &
                                    l.Query.Origem.Equal(OrigemLancamentoLiquidacao.Fundo.AplicacaoConverter))
                            ) &
                            l.Query.DataHistorico == dataAux &
                            l.Query.Valor != 0)

                     .GroupBy(l.Query.Descricao,
                              l.Query.IdConta,
                              l.Query.DataLancamento,
                              l.Query.DataVencimento);

            l.Query.Load();
            //
            l.Filter = "" + LiquidacaoMetadata.ColumnNames.Valor + " <> 0";

            decimal totalLiquidar = 0;
            decimal porcentagemLiquidacao = 0;
            //
            foreach (LiquidacaoHistorico liquidacao in l) {
                totalLiquidar += liquidacao.Valor.Value;
            }

            if (plFechamento != 0) {
                porcentagemLiquidacao = totalLiquidar / plFechamento;
            }

            #endregion

            if (plFechamento != 0) {
                porcentagem5 = valormercado / plFechamento;
                porcentagem5 += porcentagemSaldoCaixa;
                porcentagem5 += porcentagemLiquidacao;
            }
            #endregion

            #region 6

            //clienteQuery = new ClienteQuery("L");
            PosicaoFundoHistoricoQuery posicaoFundoQuery = new PosicaoFundoHistoricoQuery("P");
            //
            PosicaoFundoHistoricoCollection pf = new PosicaoFundoHistoricoCollection();
            posicaoFundoQuery.Select(posicaoFundoQuery.ValorBruto.Sum());
            posicaoFundoQuery.Where(posicaoFundoQuery.IdCliente == idCliente,
                                    posicaoFundoQuery.DataHistorico == dataAux);

            pf.Load(posicaoFundoQuery);

            decimal valor6 = 0.0M;
            if (pf.HasData) {
                if (pf[0].ValorBruto.HasValue) {
                    valor6 = pf[0].ValorBruto.Value;
                }
            }
            if (plFechamento != 0) {
                porcentagem6 = valor6 / plFechamento;
            }
            #endregion

            #region 7
            posicaoBolsaQuery = new PosicaoBolsaHistoricoQuery("P");
            //
            posicaoBolsaQuery.Select(posicaoBolsaQuery.ValorMercado.Sum());
            posicaoBolsaQuery.Where(posicaoBolsaQuery.IdCliente == idCliente,
                                    posicaoBolsaQuery.DataHistorico == dataAux,
                                    posicaoBolsaQuery.TipoMercado.In(TipoMercadoBolsa.Imobiliario));

            p = new PosicaoBolsaHistorico();
            p.Load(posicaoBolsaQuery);

            decimal valor7 = 0.0M;
            if (p.ValorMercado.HasValue) {
                valor7 = p.ValorMercado.Value;
            }

            if (plFechamento != 0) {
                porcentagem7 = valor7 / plFechamento;
            }
            #endregion

            #region 8
            posicaoRendaFixaQuery = new PosicaoRendaFixaHistoricoQuery("P");
            tituloRendaFixaQuery = new TituloRendaFixaQuery("T");
            papelRendaFixaQuery = new PapelRendaFixaQuery("A");
            clienteQuery = new ClienteQuery("L");

            posicaoRendaFixaQuery.Select(posicaoRendaFixaQuery.ValorMercado.Sum());
            //            
            posicaoRendaFixaQuery.InnerJoin(clienteQuery).On(clienteQuery.IdCliente == posicaoRendaFixaQuery.IdCliente);
            posicaoRendaFixaQuery.InnerJoin(tituloRendaFixaQuery).On(tituloRendaFixaQuery.IdTitulo == posicaoRendaFixaQuery.IdTitulo);
            posicaoRendaFixaQuery.InnerJoin(papelRendaFixaQuery).On(papelRendaFixaQuery.IdPapel == tituloRendaFixaQuery.IdPapel);
            //
            posicaoRendaFixaQuery.Where(posicaoRendaFixaQuery.IdCliente == idCliente,
                                        posicaoRendaFixaQuery.DataHistorico == dataAux,
                                        posicaoRendaFixaQuery.TipoOperacao.Equal((byte)TipoOperacaoTitulo.CompraFinal),
                //tituloRendaFixaQuery.CodigoCustodia.IsNotNull(),
                //tituloRendaFixaQuery.CodigoCustodia.NotEqual(""),
                                        papelRendaFixaQuery.TipoPapel.Equal((byte)TipoPapelTitulo.Privado),
                                        clienteQuery.IdMoeda == (int)ListaMoedaFixo.Real,
                                        papelRendaFixaQuery.Classe.NotIn((int)ClasseRendaFixa.CDB,
                //(int)ClasseRendaFixa.rdb,
                                                                           (int)ClasseRendaFixa.LF,
                                                                           (int)ClasseRendaFixa.DPGE,
                                                                           (int)ClasseRendaFixa.CCB,
                                                                           (int)ClasseRendaFixa.LCA,
                                                                           (int)ClasseRendaFixa.LCI)
                                        );

            coll = new PosicaoRendaFixaHistoricoCollection();
            coll.Load(posicaoRendaFixaQuery);

            valormercado = 0.0M;
            if (coll.HasData) {
                if (coll[0].ValorMercado.HasValue) {
                    valormercado = coll[0].ValorMercado.Value;
                }
            }

            if (plFechamento != 0) {
                porcentagem8 = valormercado / plFechamento;
            }
            #endregion

            #region 9
            #region Bolsa
            PosicaoBolsaHistorico posicaoBolsa = new PosicaoBolsaHistorico();
            posicaoBolsa.Query.Select(posicaoBolsa.Query.ValorMercado.Sum());
            posicaoBolsa.Query.Where(posicaoBolsa.Query.IdCliente == idCliente,
                                     posicaoBolsa.Query.DataHistorico == dataAux,
                                     posicaoBolsa.Query.TipoMercado.In(TipoMercadoBolsa.OpcaoCompra, TipoMercadoBolsa.OpcaoVenda));
            posicaoBolsa.Query.Load();

            decimal valor = 0.0M; ;
            if (posicaoBolsa.ValorMercado.HasValue) {
                valor = posicaoBolsa.ValorMercado.Value;
            }
            #endregion

            #region BMF
            PosicaoBMFHistorico posicaoBMF = new PosicaoBMFHistorico();
            posicaoBMF.Query.Select(posicaoBMF.Query.ValorMercado.Sum());
            posicaoBMF.Query.Where(posicaoBMF.Query.IdCliente == idCliente,
                                   posicaoBMF.Query.DataHistorico == dataAux);
            posicaoBMF.Query.Load();

            if (posicaoBMF.ValorMercado.HasValue) {
                valor += posicaoBMF.ValorMercado.Value;
            }
            #endregion

            #region Termo
            PosicaoTermoBolsaHistorico posicaoTermoBolsa = new PosicaoTermoBolsaHistorico();

            posicaoTermoBolsa.Query.Select(posicaoTermoBolsa.Query.ValorMercado.Sum());
            posicaoTermoBolsa.Query.Where(posicaoTermoBolsa.Query.IdCliente == idCliente,
                                          posicaoTermoBolsa.Query.DataHistorico == dataAux);
            posicaoTermoBolsa.Query.Load();

            if (posicaoTermoBolsa.ValorMercado.HasValue) {
                valor += posicaoTermoBolsa.ValorMercado.Value;
            }
            #endregion

            #region Swap
            PosicaoSwapHistorico posicaoSwap = new PosicaoSwapHistorico();
            posicaoSwap.Query.Select(posicaoSwap.Query.Saldo.Sum());
            posicaoSwap.Query.Where(posicaoSwap.Query.IdCliente == idCliente,
                                    posicaoSwap.Query.DataHistorico == dataAux);
            posicaoSwap.Query.Load();

            if (posicaoSwap.Saldo.HasValue) {
                valor += posicaoSwap.Saldo.Value;
            }
            #endregion

            if (plFechamento != 0) {
                porcentagem9 = valor / plFechamento;
            }
            #endregion

            #region 10

            #region Bolsa
            posicaoBolsaQuery = new PosicaoBolsaHistoricoQuery("P");
            clienteQuery = new ClienteQuery("L");
            //
            posicaoBolsaQuery.Select(posicaoBolsaQuery.ValorMercado.Sum());
            posicaoBolsaQuery.InnerJoin(clienteQuery).On(clienteQuery.IdCliente == posicaoBolsaQuery.IdCliente);

            posicaoBolsaQuery.Where(posicaoBolsaQuery.IdCliente == idCliente,
                                    posicaoBolsaQuery.DataHistorico == dataAux,
                                    clienteQuery.IdMoeda.NotIn((int)ListaMoedaFixo.Real));

            p = new PosicaoBolsaHistorico();
            p.Load(posicaoBolsaQuery);

            decimal valor10 = 0.0M;
            if (p.ValorMercado.HasValue) {
                valor10 = p.ValorMercado.Value;
            }
            #endregion

            #region RF
            posicaoRendaFixaQuery = new PosicaoRendaFixaHistoricoQuery("P");
            clienteQuery = new ClienteQuery("L");
            //
            posicaoRendaFixaQuery.Select(posicaoRendaFixaQuery.ValorMercado.Sum());
            posicaoRendaFixaQuery.InnerJoin(clienteQuery).On(clienteQuery.IdCliente == posicaoRendaFixaQuery.IdCliente);

            posicaoRendaFixaQuery.Where(posicaoRendaFixaQuery.IdCliente == idCliente,
                                        posicaoRendaFixaQuery.DataHistorico == dataAux,
                                        clienteQuery.IdMoeda.NotIn((int)ListaMoedaFixo.Real));

            PosicaoRendaFixaHistorico prf = new PosicaoRendaFixaHistorico();
            prf.Load(posicaoRendaFixaQuery);

            if (prf.ValorMercado.HasValue) {
                valor10 += prf.ValorMercado.Value;
            }
            #endregion

            if (plFechamento != 0) {
                porcentagem10 = valor10 / plFechamento;
            }

            #endregion

            #region 11
            porcentagem11 = 0;
            #endregion

            // Dicionario com as descrições e porcentagens de patrimônio
            Dictionary<string, decimal> porcentagens = new Dictionary<string, decimal>();
            porcentagens.Add("Títulos Públicos federais", porcentagem1);
            porcentagens.Add("Operacões compromissadas lastreadas em títulos públicos federais", porcentagem2);
            porcentagens.Add("Operacões compromissadas lastreadas em títulos privados", porcentagem3);
            porcentagens.Add("Acões", porcentagem4);
            porcentagens.Add("Depósito a prazo e outros títulos de instituições financeiras", porcentagem5);
            porcentagens.Add("Cotas de Fundos de Investimento 555", porcentagem6);
            porcentagens.Add("Outras Cotas de Fundos de Investimento", porcentagem7);
            porcentagens.Add("Títulos de crédito privado", porcentagem8);
            porcentagens.Add("Derivativos", porcentagem9);
            porcentagens.Add("Investimento no exterior", porcentagem10);
            porcentagens.Add("Outras Aplicações", porcentagem11);

            // pegar as posições [10-9-8-7-6] do dicionario ordenado
            Dictionary<string, decimal> porcentagensOrdenadas = this.SortDictionaryByValue(porcentagens);

            int j = -1;
            List<string> descricao = new List<string>();
            List<decimal> valores = new List<decimal>();
            foreach (KeyValuePair<string, decimal> pair in porcentagensOrdenadas) {
                j++;
                if (j <= 5) {
                    continue;
                }
                else {
                    descricao.Add(pair.Key);
                    valores.Add(pair.Value);
                }
            }
            //
            decimal v4 = valores[4] * 100;
            decimal v3 = valores[3] * 100;
            decimal v2 = valores[2] * 100;
            decimal v1 = valores[1] * 100;
            decimal v0 = valores[0] * 100;
            //

            #endregion

            #region Controe LISTA_COMP_CARTEIRA

            HistoricoCota h1 = new HistoricoCota();
            DateTime dataAux1 = Calendario.RetornaUltimoDiaUtilMes(dataPosicao);
            //
            decimal pl = 0.00M;
            try {
                h1.BuscaValorPatrimonioDia(idCliente, dataAux1);
                pl = h1.PLFechamento.Value;
            }
            catch (HistoricoCotaNaoCadastradoException e1) {
                pl = 0.00M;
            }

            corpo.getLISTA_COMP_CARTEIRA.pl_fundo = pl;

            #region 4
            XML_Lamina.COMP_CARTEIRA elem = new XML_Lamina.COMP_CARTEIRA();

            elem.ESPECIE_ATIVO = descricao[4];
            elem.perc_pl = v4;

            corpo.getLISTA_COMP_CARTEIRA.getCOMP_CARTEIRA.Add(elem);
            #endregion

            #region 3
            elem = new XML_Lamina.COMP_CARTEIRA();

            elem.ESPECIE_ATIVO = descricao[3];
            elem.perc_pl = v3;

            corpo.getLISTA_COMP_CARTEIRA.getCOMP_CARTEIRA.Add(elem);
            #endregion

            #region 2
            elem = new XML_Lamina.COMP_CARTEIRA();

            elem.ESPECIE_ATIVO = descricao[2];
            elem.perc_pl = v2;

            corpo.getLISTA_COMP_CARTEIRA.getCOMP_CARTEIRA.Add(elem);
            #endregion

            #region 1
            elem = new XML_Lamina.COMP_CARTEIRA();

            elem.ESPECIE_ATIVO = descricao[1];
            elem.perc_pl = v1;

            corpo.getLISTA_COMP_CARTEIRA.getCOMP_CARTEIRA.Add(elem);
            #endregion

            #region 0
            elem = new XML_Lamina.COMP_CARTEIRA();

            elem.ESPECIE_ATIVO = descricao[0];
            elem.perc_pl = v0;

            corpo.getLISTA_COMP_CARTEIRA.getCOMP_CARTEIRA.Add(elem);
            #endregion
            #endregion
        }

        /// <summary>
        /// Constroe o objeto INFOR_TAB_RENT_ANUAL
        /// </summary>
        /// <param name="dataPosicao"></param>
        /// <param name="idCliente"></param>
        /// <param name="corpo">Objeto representativo do Corpo do arquivo XML</param>
        private void ControeINFOR_TAB_RENT_ANUAL(DateTime dataPosicao, int idCliente, XML_Lamina.Corpo corpo, Lamina info) {
            //            
            Carteira carteira = new Carteira();
            carteira.LoadByPrimaryKey(idCliente);

            string descricaoIndice = carteira.UpToIndiceByIdIndiceBenchmark.Descricao.ToString();

            CalculoMedida.EstatisticaRetornoAnual ano0 = this.listaRetornosAnuais.Find(delegate(CalculoMedida.EstatisticaRetornoAnual a) { return a.Data.Year == this.ultimos_5_Anos[4]; });
            CalculoMedida.EstatisticaRetornoAnual ano1 = this.listaRetornosAnuais.Find(delegate(CalculoMedida.EstatisticaRetornoAnual a) { return a.Data.Year == this.ultimos_5_Anos[3]; });
            CalculoMedida.EstatisticaRetornoAnual ano2 = this.listaRetornosAnuais.Find(delegate(CalculoMedida.EstatisticaRetornoAnual a) { return a.Data.Year == this.ultimos_5_Anos[2]; });
            CalculoMedida.EstatisticaRetornoAnual ano3 = this.listaRetornosAnuais.Find(delegate(CalculoMedida.EstatisticaRetornoAnual a) { return a.Data.Year == this.ultimos_5_Anos[1]; });
            CalculoMedida.EstatisticaRetornoAnual ano4 = this.listaRetornosAnuais.Find(delegate(CalculoMedida.EstatisticaRetornoAnual a) { return a.Data.Year == this.ultimos_5_Anos[0]; });

            decimal p1 = 1;
            decimal p2 = 1;
            decimal p3 = 1;
            decimal p4 = 1;
            decimal p5 = 1;
            //
            decimal benchmark1 = 1;
            decimal benchmark2 = 1;
            decimal benchmark3 = 1;
            decimal benchmark4 = 1;
            decimal benchmark5 = 1;
            //
            #region ano0
            if (ano0 != null && ano0.Retorno.HasValue) {
                p1 = (ano0.Retorno.Value / 100M) + 1M;
            }
            #endregion

            #region ano1
            if (ano1 != null && ano1.Retorno.HasValue) {
                p2 = (ano1.Retorno.Value / 100M) + 1M;
            }
            #endregion

            #region ano2
            if (ano2 != null && ano2.Retorno.HasValue) {
                p3 = (ano2.Retorno.Value / 100M) + 1M;
            }
            #endregion

            #region ano3
            if (ano3 != null && ano3.Retorno.HasValue) {
                p4 = (ano3.Retorno.Value / 100M) + 1M;
            }
            #endregion

            #region ano4
            if (ano4 != null && ano4.Retorno.HasValue) {
                p5 = (ano4.Retorno.Value / 100M) + 1M;
            }
            #endregion

            decimal? fatorAcumulado = ((p1 * p2 * p3 * p4 * p5) - 1M) * 100M;

            #region benchmark0
            if (ano0 != null && ano0.RetornoBenchmark.HasValue) {
                benchmark1 = (ano0.RetornoBenchmark.Value / 100M) + 1M;
            }
            #endregion

            #region benchmark1
            if (ano1 != null && ano1.RetornoBenchmark.HasValue) {
                benchmark2 = (ano1.RetornoBenchmark.Value / 100M) + 1M;
            }
            #endregion

            #region benchmark2
            if (ano2 != null && ano2.RetornoBenchmark.HasValue) {
                benchmark3 = (ano2.RetornoBenchmark.Value / 100M) + 1M;
            }
            #endregion

            #region benchmark3
            if (ano3 != null && ano3.RetornoBenchmark.HasValue) {
                benchmark4 = (ano3.RetornoBenchmark.Value / 100M) + 1M;
            }
            #endregion

            #region benchmark4
            if (ano4 != null && ano4.RetornoBenchmark.HasValue) {
                benchmark5 = (ano4.RetornoBenchmark.Value / 100M) + 1M;
            }
            #endregion

            decimal? indiceAcumulado = ((benchmark1 * benchmark2 * benchmark3 * benchmark4 * benchmark5) - 1M) * 100M;

            // Campo não obrigatório para fundos com menos de um ano de funcionamento
            corpo.getINFOR_TAB_RENT_ANUAL.rent_5anos = 0;
            if (fatorAcumulado.HasValue) {
                corpo.getINFOR_TAB_RENT_ANUAL.rent_5anos = fatorAcumulado.Value;
            }

            corpo.getINFOR_TAB_RENT_ANUAL.IND_REFER = descricaoIndice;

            corpo.getINFOR_TAB_RENT_ANUAL.rent_ind_ref = 0;
            if (indiceAcumulado.HasValue) {
                corpo.getINFOR_TAB_RENT_ANUAL.rent_ind_ref = indiceAcumulado.Value;
            }

            corpo.getINFOR_TAB_RENT_ANUAL.ANOS_ABAIXO = 0; // TODO                                 
            corpo.getINFOR_TAB_RENT_ANUAL.inic_funcio = carteira.DataInicioCota.Value;

            int anoInicio = carteira.DataInicioCota.Value.Year;

            //for (int i = 0; i < ultimos_5_Anos.Count; i++) {
            //    if (ultimos_5_Anos[i] < anoInicio) {
            //        XML_Lamina.ANTES_INIC2 elem = new XML_Lamina.ANTES_INIC2();
            //        elem.ANO = ultimos_5_Anos[i];
            //        //
            //        corpo.getINFOR_TAB_RENT_ANUAL.getANTES_INIC2.Add(elem);
            //    }
            //}
            List<int?> anos = new List<int?>(5);
            anos.AddRange(new int?[] { null, null, null, null, null });
            for (int i = 0; i < ultimos_5_Anos.Count; i++) {
                if (ultimos_5_Anos[i] < anoInicio) {
                    anos[i] = ultimos_5_Anos[i];
                }
            }
            XML_Lamina.ANTES_INIC2 elem = new XML_Lamina.ANTES_INIC2();
            elem.ANO1 = anos[0];
            elem.ANO2 = anos[1];
            elem.ANO3 = anos[2];
            elem.ANO4 = anos[3];
            elem.ANO5 = anos[4];

            if (anos[0] != null || anos[1] != null || anos[2] != null || anos[3] != null || anos[4] != null) {
                corpo.getINFOR_TAB_RENT_ANUAL.getANTES_INIC2.Add(elem);
            }
        }

        /// <summary>
        /// Constroe o objeto LISTA_TAB_RENT_ANUAL
        /// </summary>
        /// <param name="dataPosicao"></param>
        /// <param name="idCliente"></param>
        /// <param name="corpo">Objeto representativo do Corpo do arquivo XML</param>
        private void ControeLISTA_TAB_RENT_ANUAL(DateTime dataPosicao, int idCliente, XML_Lamina.Corpo corpo, Lamina info) {

            Carteira carteira = new Carteira();
            carteira.LoadByPrimaryKey(idCliente);

            #region Preenche Calculos
            //
            string descricaoIndice = carteira.UpToIndiceByIdIndiceBenchmark.Descricao.ToString();
            //        
            CalculoMedida.EstatisticaRetornoAnual ano0 = this.listaRetornosAnuais.Find(delegate(CalculoMedida.EstatisticaRetornoAnual a) { return a.Data.Year == this.ultimos_5_Anos[4]; });
            CalculoMedida.EstatisticaRetornoAnual ano1 = this.listaRetornosAnuais.Find(delegate(CalculoMedida.EstatisticaRetornoAnual a) { return a.Data.Year == this.ultimos_5_Anos[3]; });
            CalculoMedida.EstatisticaRetornoAnual ano2 = this.listaRetornosAnuais.Find(delegate(CalculoMedida.EstatisticaRetornoAnual a) { return a.Data.Year == this.ultimos_5_Anos[2]; });
            CalculoMedida.EstatisticaRetornoAnual ano3 = this.listaRetornosAnuais.Find(delegate(CalculoMedida.EstatisticaRetornoAnual a) { return a.Data.Year == this.ultimos_5_Anos[1]; });
            CalculoMedida.EstatisticaRetornoAnual ano4 = this.listaRetornosAnuais.Find(delegate(CalculoMedida.EstatisticaRetornoAnual a) { return a.Data.Year == this.ultimos_5_Anos[0]; });

            #region Ano0
            if (ano0 != null) {
                XML_Lamina.TAB_RENT_ANUAL elem = new XML_Lamina.TAB_RENT_ANUAL();

                elem.ANO_RENT = this.ultimos_5_Anos[4];
                if (ano0.Retorno.HasValue) {
                    elem.rent_liq = ano0.Retorno.Value;
                }
                if (ano0.RetornoBenchmark.HasValue) {
                    elem.var_ind_ref = ano0.RetornoBenchmark.Value;
                }
                if (ano0.RetornoDiferencial.HasValue) {
                    elem.rent_perc_ind_ref = ano0.RetornoDiferencial.Value;
                }
                elem.COMENTARIOS = "";

                corpo.getLISTA_TAB_RENT_ANUAL.getTAB_RENT_ANUAL.Add(elem);
            }
            #endregion

            #region Ano1
            if (ano1 != null) {
                XML_Lamina.TAB_RENT_ANUAL elem = new XML_Lamina.TAB_RENT_ANUAL();

                elem.ANO_RENT = this.ultimos_5_Anos[3];
                if (ano1.Retorno.HasValue)
                {
                    elem.rent_liq = ano1.Retorno.Value;
                }
                if (ano1.RetornoBenchmark.HasValue)
                {
                    elem.var_ind_ref = ano1.RetornoBenchmark.Value;
                }
                if (ano1.RetornoDiferencial.HasValue)
                {
                    elem.rent_perc_ind_ref = ano1.RetornoDiferencial.Value;
                }
                elem.COMENTARIOS = "";

                corpo.getLISTA_TAB_RENT_ANUAL.getTAB_RENT_ANUAL.Add(elem);
            }
            #endregion

            #region Ano2
            if (ano2 != null) {
                XML_Lamina.TAB_RENT_ANUAL elem = new XML_Lamina.TAB_RENT_ANUAL();

                elem.ANO_RENT = this.ultimos_5_Anos[2];
                if (ano2.Retorno.HasValue)
                {
                    elem.rent_liq = ano2.Retorno.Value;
                }
                if (ano2.RetornoBenchmark.HasValue)
                {
                    elem.var_ind_ref = ano2.RetornoBenchmark.Value;
                }
                if (ano0.RetornoDiferencial.HasValue) {
                    elem.rent_perc_ind_ref = ano2.RetornoDiferencial.Value;
                }
                elem.COMENTARIOS = "";

                corpo.getLISTA_TAB_RENT_ANUAL.getTAB_RENT_ANUAL.Add(elem);
            }
            #endregion

            #region Ano3
            if (ano3 != null) {
                XML_Lamina.TAB_RENT_ANUAL elem = new XML_Lamina.TAB_RENT_ANUAL();

                elem.ANO_RENT = this.ultimos_5_Anos[1];
                if (ano3.Retorno.HasValue)
                {
                    elem.rent_liq = ano3.Retorno.Value;
                }
                if (ano3.RetornoBenchmark.HasValue)
                {
                    elem.var_ind_ref = ano3.RetornoBenchmark.Value;
                }
                if (ano3.RetornoDiferencial.HasValue)
                {
                    elem.rent_perc_ind_ref = ano3.RetornoDiferencial.Value;
                }
                elem.COMENTARIOS = "";

                corpo.getLISTA_TAB_RENT_ANUAL.getTAB_RENT_ANUAL.Add(elem);
            }
            #endregion

            #region Ano4
            if (ano4 != null) {
                XML_Lamina.TAB_RENT_ANUAL elem = new XML_Lamina.TAB_RENT_ANUAL();

                elem.ANO_RENT = this.ultimos_5_Anos[0];
                if (ano4.Retorno.HasValue)
                {
                    elem.rent_liq = ano4.Retorno.Value;
                }
                if (ano4.RetornoBenchmark.HasValue)
                {
                    elem.var_ind_ref = ano4.RetornoBenchmark.Value;
                }
                if (ano4.RetornoDiferencial.HasValue)
                {
                    elem.rent_perc_ind_ref = ano4.RetornoDiferencial.Value;
                }
                elem.COMENTARIOS = "";

                corpo.getLISTA_TAB_RENT_ANUAL.getTAB_RENT_ANUAL.Add(elem);
            }
            #endregion
            #endregion
        }

        /// <summary>
        /// Constroe o objeto LISTA_TAB_RENT_MENSAL
        /// </summary>
        /// <param name="dataPosicao"></param>
        /// <param name="idCliente"></param>
        /// <param name="corpo">Objeto representativo do Corpo do arquivo XML</param>
        private void ControeLISTA_TAB_RENT_MENSAL(DateTime dataPosicao, int idCliente, XML_Lamina.Corpo corpo, Lamina info) {

            Carteira carteira = new Carteira();
            carteira.LoadByPrimaryKey(idCliente);

            #region Calculos ( antigo comentado )
            /*
            string descricaoIndice = carteira.UpToIndiceByIdIndiceBenchmark.Descricao.ToString();
            //
            CalculoMedida.EstatisticaRetornoMensal cJan = this.listaRetornosMensais.Find(delegate(CalculoMedida.EstatisticaRetornoMensal a) { return a.Data.Month == 1; });
            CalculoMedida.EstatisticaRetornoMensal cFev = this.listaRetornosMensais.Find(delegate(CalculoMedida.EstatisticaRetornoMensal a) { return a.Data.Month == 2; });
            CalculoMedida.EstatisticaRetornoMensal cMar = this.listaRetornosMensais.Find(delegate(CalculoMedida.EstatisticaRetornoMensal a) { return a.Data.Month == 3; });
            CalculoMedida.EstatisticaRetornoMensal cAbr = this.listaRetornosMensais.Find(delegate(CalculoMedida.EstatisticaRetornoMensal a) { return a.Data.Month == 4; });
            CalculoMedida.EstatisticaRetornoMensal cMai = this.listaRetornosMensais.Find(delegate(CalculoMedida.EstatisticaRetornoMensal a) { return a.Data.Month == 5; });
            CalculoMedida.EstatisticaRetornoMensal cJun = this.listaRetornosMensais.Find(delegate(CalculoMedida.EstatisticaRetornoMensal a) { return a.Data.Month == 6; });
            CalculoMedida.EstatisticaRetornoMensal cJul = this.listaRetornosMensais.Find(delegate(CalculoMedida.EstatisticaRetornoMensal a) { return a.Data.Month == 7; });
            CalculoMedida.EstatisticaRetornoMensal cAgo = this.listaRetornosMensais.Find(delegate(CalculoMedida.EstatisticaRetornoMensal a) { return a.Data.Month == 8; });
            CalculoMedida.EstatisticaRetornoMensal cSet = this.listaRetornosMensais.Find(delegate(CalculoMedida.EstatisticaRetornoMensal a) { return a.Data.Month == 9; });
            CalculoMedida.EstatisticaRetornoMensal cOut = this.listaRetornosMensais.Find(delegate(CalculoMedida.EstatisticaRetornoMensal a) { return a.Data.Month == 10; });
            CalculoMedida.EstatisticaRetornoMensal cNov = this.listaRetornosMensais.Find(delegate(CalculoMedida.EstatisticaRetornoMensal a) { return a.Data.Month == 11; });
            CalculoMedida.EstatisticaRetornoMensal cDez = this.listaRetornosMensais.Find(delegate(CalculoMedida.EstatisticaRetornoMensal a) { return a.Data.Month == 12; });
            //
            decimal? rentabilidade12Meses = this.CalculaRentabilidade12Meses(this.listaRetornosMensais);
            decimal? rentabilidade12MesesIndice = this.CalculaRentabilidade12MesesIndice(this.listaRetornosMensais);

            #region Janeiro
            if (cJan != null) {
                #region Controe LISTA_TAB_RENT_ANUAL

                XML_Lamina.TAB_RENT_MENSAL elem = new XML_Lamina.TAB_RENT_MENSAL();

                elem.MES = 1;
                if (cJan.Retorno.HasValue) {
                    elem.rent_mens = cJan.Retorno.Value;
                }
                if (cJan.RetornoBenchmark.HasValue) {
                    elem.var_mens_ind_ref = cJan.RetornoBenchmark.Value;
                }
                if (cJan.RetornoDiferencial.HasValue) {
                    elem.rent_mens_perc_ind_ref = cJan.RetornoDiferencial.Value;
                }

                corpo.getLISTA_TAB_RENT_MENSAL.getTAB_RENT_MENSAL.Add(elem);

                #endregion
            }
            #endregion
            #region Fevereiro
            if (cFev != null) {
                #region Controe LISTA_TAB_RENT_ANUAL

                XML_Lamina.TAB_RENT_MENSAL elem = new XML_Lamina.TAB_RENT_MENSAL();

                elem.MES = 2;
                if (cFev.Retorno.HasValue) {
                    elem.rent_mens = cFev.Retorno.Value;
                }
                if (cFev.RetornoBenchmark.HasValue) {
                    elem.var_mens_ind_ref = cFev.RetornoBenchmark.Value;
                }
                if (cFev.RetornoDiferencial.HasValue) {
                    elem.rent_mens_perc_ind_ref = cFev.RetornoDiferencial.Value;
                }

                corpo.getLISTA_TAB_RENT_MENSAL.getTAB_RENT_MENSAL.Add(elem);

                #endregion
            }
            #endregion
            #region Março
            if (cMar != null) {
                #region Controe LISTA_TAB_RENT_ANUAL

                XML_Lamina.TAB_RENT_MENSAL elem = new XML_Lamina.TAB_RENT_MENSAL();

                elem.MES = 3;
                if (cMar.Retorno.HasValue) {
                    elem.rent_mens = cMar.Retorno.Value;
                }
                if (cMar.RetornoBenchmark.HasValue) {
                    elem.var_mens_ind_ref = cMar.RetornoBenchmark.Value;
                }
                if (cMar.RetornoDiferencial.HasValue) {
                    elem.rent_mens_perc_ind_ref = cMar.RetornoDiferencial.Value;
                }

                corpo.getLISTA_TAB_RENT_MENSAL.getTAB_RENT_MENSAL.Add(elem);

                #endregion
            }
            #endregion
            #region Abril
            if (cAbr != null) {
                #region Controe LISTA_TAB_RENT_ANUAL

                XML_Lamina.TAB_RENT_MENSAL elem = new XML_Lamina.TAB_RENT_MENSAL();

                elem.MES = 4;
                if (cAbr.Retorno.HasValue) {
                    elem.rent_mens = cAbr.Retorno.Value;
                }
                if (cAbr.RetornoBenchmark.HasValue) {
                    elem.var_mens_ind_ref = cAbr.RetornoBenchmark.Value;
                }
                if (cAbr.RetornoDiferencial.HasValue) {
                    elem.rent_mens_perc_ind_ref = cAbr.RetornoDiferencial.Value;
                }

                corpo.getLISTA_TAB_RENT_MENSAL.getTAB_RENT_MENSAL.Add(elem);

                #endregion
            }
            #endregion
            #region Maio
            if (cMai != null) {
                #region Controe LISTA_TAB_RENT_ANUAL

                XML_Lamina.TAB_RENT_MENSAL elem = new XML_Lamina.TAB_RENT_MENSAL();

                elem.MES = 5;
                if (cMai.Retorno.HasValue) {
                    elem.rent_mens = cMai.Retorno.Value;
                }
                if (cMai.RetornoBenchmark.HasValue) {
                    elem.var_mens_ind_ref = cMai.RetornoBenchmark.Value;
                }
                if (cMai.RetornoDiferencial.HasValue) {
                    elem.rent_mens_perc_ind_ref = cMai.RetornoDiferencial.Value;
                }

                corpo.getLISTA_TAB_RENT_MENSAL.getTAB_RENT_MENSAL.Add(elem);

                #endregion
            }
            #endregion
            #region Junho
            if (cJun != null) {
                #region Controe LISTA_TAB_RENT_ANUAL

                XML_Lamina.TAB_RENT_MENSAL elem = new XML_Lamina.TAB_RENT_MENSAL();

                elem.MES = 6;
                if (cJun.Retorno.HasValue) {
                    elem.rent_mens = cJun.Retorno.Value;
                }
                if (cJun.RetornoBenchmark.HasValue) {
                    elem.var_mens_ind_ref = cJun.RetornoBenchmark.Value;
                }
                if (cJun.RetornoDiferencial.HasValue) {
                    elem.rent_mens_perc_ind_ref = cJun.RetornoDiferencial.Value;
                }

                corpo.getLISTA_TAB_RENT_MENSAL.getTAB_RENT_MENSAL.Add(elem);

                #endregion
            }
            #endregion
            #region Julho
            if (cJul != null) {
                #region Controe LISTA_TAB_RENT_ANUAL

                XML_Lamina.TAB_RENT_MENSAL elem = new XML_Lamina.TAB_RENT_MENSAL();

                elem.MES = 7;
                if (cJul.Retorno.HasValue) {
                    elem.rent_mens = cJul.Retorno.Value;
                }
                if (cJul.RetornoBenchmark.HasValue) {
                    elem.var_mens_ind_ref = cJul.RetornoBenchmark.Value;
                }
                if (cJul.RetornoDiferencial.HasValue) {
                    elem.rent_mens_perc_ind_ref = cJul.RetornoDiferencial.Value;
                }

                corpo.getLISTA_TAB_RENT_MENSAL.getTAB_RENT_MENSAL.Add(elem);

                #endregion
            }
            #endregion
            #region Agosto
            if (cAgo != null) {
                #region Controe LISTA_TAB_RENT_ANUAL

                XML_Lamina.TAB_RENT_MENSAL elem = new XML_Lamina.TAB_RENT_MENSAL();

                elem.MES = 8;
                if (cAgo.Retorno.HasValue) {
                    elem.rent_mens = cAgo.Retorno.Value;
                }
                if (cAgo.RetornoBenchmark.HasValue) {
                    elem.var_mens_ind_ref = cAgo.RetornoBenchmark.Value;
                }
                if (cAgo.RetornoDiferencial.HasValue) {
                    elem.rent_mens_perc_ind_ref = cAgo.RetornoDiferencial.Value;
                }

                corpo.getLISTA_TAB_RENT_MENSAL.getTAB_RENT_MENSAL.Add(elem);

                #endregion
            }
            #endregion
            #region Setembro
            if (cSet != null) {
                #region Controe LISTA_TAB_RENT_ANUAL

                XML_Lamina.TAB_RENT_MENSAL elem = new XML_Lamina.TAB_RENT_MENSAL();

                elem.MES = 9;
                if (cSet.Retorno.HasValue) {
                    elem.rent_mens = cSet.Retorno.Value;
                }
                if (cSet.RetornoBenchmark.HasValue) {
                    elem.var_mens_ind_ref = cSet.RetornoBenchmark.Value;
                }
                if (cSet.RetornoDiferencial.HasValue) {
                    elem.rent_mens_perc_ind_ref = cSet.RetornoDiferencial.Value;
                }

                corpo.getLISTA_TAB_RENT_MENSAL.getTAB_RENT_MENSAL.Add(elem);

                #endregion
            }
            #endregion
            #region Outubro
            if (cOut != null) {
                #region Controe LISTA_TAB_RENT_ANUAL

                XML_Lamina.TAB_RENT_MENSAL elem = new XML_Lamina.TAB_RENT_MENSAL();

                elem.MES = 10;
                if (cOut.Retorno.HasValue) {
                    elem.rent_mens = cOut.Retorno.Value;
                }
                if (cOut.RetornoBenchmark.HasValue) {
                    elem.var_mens_ind_ref = cOut.RetornoBenchmark.Value;
                }
                if (cOut.RetornoDiferencial.HasValue) {
                    elem.rent_mens_perc_ind_ref = cOut.RetornoDiferencial.Value;
                }

                corpo.getLISTA_TAB_RENT_MENSAL.getTAB_RENT_MENSAL.Add(elem);

                #endregion
            }
            #endregion
            #region Novembro
            if (cNov != null) {
                #region Controe LISTA_TAB_RENT_ANUAL

                XML_Lamina.TAB_RENT_MENSAL elem = new XML_Lamina.TAB_RENT_MENSAL();

                elem.MES = 11;
                if (cNov.Retorno.HasValue) {
                    elem.rent_mens = cNov.Retorno.Value;
                }
                if (cNov.RetornoBenchmark.HasValue) {
                    elem.var_mens_ind_ref = cNov.RetornoBenchmark.Value;
                }
                if (cNov.RetornoDiferencial.HasValue) {
                    elem.rent_mens_perc_ind_ref = cNov.RetornoDiferencial.Value;
                }

                corpo.getLISTA_TAB_RENT_MENSAL.getTAB_RENT_MENSAL.Add(elem);

                #endregion
            }
            #endregion
            #region Dezembro
            if (cDez != null) {
                #region Controe LISTA_TAB_RENT_ANUAL

                XML_Lamina.TAB_RENT_MENSAL elem = new XML_Lamina.TAB_RENT_MENSAL();

                elem.MES = 12;
                if (cDez.Retorno.HasValue) {
                    elem.rent_mens = cDez.Retorno.Value;
                }
                if (cDez.RetornoBenchmark.HasValue) {
                    elem.var_mens_ind_ref = cDez.RetornoBenchmark.Value;
                }
                if (cDez.RetornoDiferencial.HasValue) {
                    elem.rent_mens_perc_ind_ref = cDez.RetornoDiferencial.Value;
                }

                corpo.getLISTA_TAB_RENT_MENSAL.getTAB_RENT_MENSAL.Add(elem);

                #endregion
            }
            #endregion

            decimal? rentabilidade12MesesDiferencial = null;
            if (rentabilidade12Meses.HasValue && rentabilidade12MesesIndice.HasValue) {
                if (rentabilidade12MesesIndice != 0) {
                    rentabilidade12MesesDiferencial = (rentabilidade12Meses / rentabilidade12MesesIndice) * 100;
                }
            }

            // Rentabilidade 12 Meses / Rentabilidade 12 Meses Indice / Diferencial
            //ReportBase.ConfiguraSinalNegativo(((XRTableCell)((XRTableRow)table.Rows[13]).Cells[1]), rentabilidade12Meses, true, ReportBase.NumeroCasasDecimais.DuasCasasDecimais);
            //ReportBase.ConfiguraSinalNegativo(((XRTableCell)((XRTableRow)table.Rows[13]).Cells[2]), rentabilidade12MesesIndice, true, ReportBase.NumeroCasasDecimais.DuasCasasDecimais);
            //ReportBase.ConfiguraSinalNegativo(((XRTableCell)((XRTableRow)table.Rows[13]).Cells[3]), rentabilidade12MesesDiferencial, true, ReportBase.NumeroCasasDecimais.DuasCasasDecimais);
            */
            #endregion

            #region Calculos
            string descricaoIndice = carteira.UpToIndiceByIdIndiceBenchmark.Descricao.ToString();
            //
            CalculoMedida.EstatisticaRetornoMensal _1 = this.listaRetornosMensais.Count >= 1 ? this.listaRetornosMensais[0] : null;
            CalculoMedida.EstatisticaRetornoMensal _2 = this.listaRetornosMensais.Count >= 2 ? this.listaRetornosMensais[1] : null;
            CalculoMedida.EstatisticaRetornoMensal _3 = this.listaRetornosMensais.Count >= 3 ? this.listaRetornosMensais[2] : null;
            CalculoMedida.EstatisticaRetornoMensal _4 = this.listaRetornosMensais.Count >= 4 ? this.listaRetornosMensais[3] : null;
            CalculoMedida.EstatisticaRetornoMensal _5 = this.listaRetornosMensais.Count >= 5 ? this.listaRetornosMensais[4] : null;
            CalculoMedida.EstatisticaRetornoMensal _6 = this.listaRetornosMensais.Count >= 6 ? this.listaRetornosMensais[5] : null;
            CalculoMedida.EstatisticaRetornoMensal _7 = this.listaRetornosMensais.Count >= 7 ? this.listaRetornosMensais[6] : null;
            CalculoMedida.EstatisticaRetornoMensal _8 = this.listaRetornosMensais.Count >= 8 ? this.listaRetornosMensais[7] : null;
            CalculoMedida.EstatisticaRetornoMensal _9 = this.listaRetornosMensais.Count >= 9 ? this.listaRetornosMensais[8] : null;
            CalculoMedida.EstatisticaRetornoMensal _10 = this.listaRetornosMensais.Count >= 10 ? this.listaRetornosMensais[9] : null;
            CalculoMedida.EstatisticaRetornoMensal _11 = this.listaRetornosMensais.Count >= 11 ? this.listaRetornosMensais[10] : null;
            CalculoMedida.EstatisticaRetornoMensal _12 = this.listaRetornosMensais.Count >= 12 ? this.listaRetornosMensais[11] : null;
            //
            decimal? rentabilidade12Meses = this.CalculaRentabilidade12Meses(this.listaRetornosMensais);
            decimal? rentabilidade12MesesIndice = this.CalculaRentabilidade12MesesIndice(this.listaRetornosMensais);

            #region _1
            if (_1 != null)
            {
                #region Controe LISTA_TAB_RENT_ANUAL

                XML_Lamina.TAB_RENT_MENSAL elem = new XML_Lamina.TAB_RENT_MENSAL();

                elem.MES = _1.Data.Month;
                if (_1.Retorno.HasValue)
                {
                    elem.rent_mens = _1.Retorno.Value;
                }
                if (_1.RetornoBenchmark.HasValue)
                {
                    elem.var_mens_ind_ref = _1.RetornoBenchmark.Value;
                }
                if (_1.RetornoDiferencial.HasValue)
                {
                    elem.rent_mens_perc_ind_ref = _1.RetornoDiferencial.Value;
                }

                corpo.getLISTA_TAB_RENT_MENSAL.getTAB_RENT_MENSAL.Add(elem);

                #endregion
            }
            #endregion
            #region _2
            if (_2 != null)
            {
                #region Controe LISTA_TAB_RENT_ANUAL

                XML_Lamina.TAB_RENT_MENSAL elem = new XML_Lamina.TAB_RENT_MENSAL();

                elem.MES = _2.Data.Month;
                if (_2.Retorno.HasValue)
                {
                    elem.rent_mens = _2.Retorno.Value;
                }
                if (_2.RetornoBenchmark.HasValue)
                {
                    elem.var_mens_ind_ref = _2.RetornoBenchmark.Value;
                }
                if (_2.RetornoDiferencial.HasValue)
                {
                    elem.rent_mens_perc_ind_ref = _2.RetornoDiferencial.Value;
                }

                corpo.getLISTA_TAB_RENT_MENSAL.getTAB_RENT_MENSAL.Add(elem);

                #endregion
            }
            #endregion
            #region _3
            if (_3 != null)
            {
                #region Controe LISTA_TAB_RENT_ANUAL

                XML_Lamina.TAB_RENT_MENSAL elem = new XML_Lamina.TAB_RENT_MENSAL();

                elem.MES = _3.Data.Month;
                if (_3.Retorno.HasValue)
                {
                    elem.rent_mens = _3.Retorno.Value;
                }
                if (_3.RetornoBenchmark.HasValue)
                {
                    elem.var_mens_ind_ref = _3.RetornoBenchmark.Value;
                }
                if (_3.RetornoDiferencial.HasValue)
                {
                    elem.rent_mens_perc_ind_ref = _3.RetornoDiferencial.Value;
                }

                corpo.getLISTA_TAB_RENT_MENSAL.getTAB_RENT_MENSAL.Add(elem);

                #endregion
            }
            #endregion
            #region _4
            if (_4 != null)
            {
                #region Controe LISTA_TAB_RENT_ANUAL

                XML_Lamina.TAB_RENT_MENSAL elem = new XML_Lamina.TAB_RENT_MENSAL();

                elem.MES = _4.Data.Month;
                if (_4.Retorno.HasValue)
                {
                    elem.rent_mens = _4.Retorno.Value;
                }
                if (_4.RetornoBenchmark.HasValue)
                {
                    elem.var_mens_ind_ref = _4.RetornoBenchmark.Value;
                }
                if (_4.RetornoDiferencial.HasValue)
                {
                    elem.rent_mens_perc_ind_ref = _4.RetornoDiferencial.Value;
                }

                corpo.getLISTA_TAB_RENT_MENSAL.getTAB_RENT_MENSAL.Add(elem);

                #endregion
            }
            #endregion
            #region _5
            if (_5 != null)
            {
                #region Controe LISTA_TAB_RENT_ANUAL

                XML_Lamina.TAB_RENT_MENSAL elem = new XML_Lamina.TAB_RENT_MENSAL();

                elem.MES = _5.Data.Month;
                if (_5.Retorno.HasValue)
                {
                    elem.rent_mens = _5.Retorno.Value;
                }
                if (_5.RetornoBenchmark.HasValue)
                {
                    elem.var_mens_ind_ref = _5.RetornoBenchmark.Value;
                }
                if (_5.RetornoDiferencial.HasValue)
                {
                    elem.rent_mens_perc_ind_ref = _5.RetornoDiferencial.Value;
                }

                corpo.getLISTA_TAB_RENT_MENSAL.getTAB_RENT_MENSAL.Add(elem);

                #endregion
            }
            #endregion
            #region _6
            if (_6 != null)
            {
                #region Controe LISTA_TAB_RENT_ANUAL

                XML_Lamina.TAB_RENT_MENSAL elem = new XML_Lamina.TAB_RENT_MENSAL();

                elem.MES = _6.Data.Month;
                if (_6.Retorno.HasValue)
                {
                    elem.rent_mens = _6.Retorno.Value;
                }
                if (_6.RetornoBenchmark.HasValue)
                {
                    elem.var_mens_ind_ref = _6.RetornoBenchmark.Value;
                }
                if (_6.RetornoDiferencial.HasValue)
                {
                    elem.rent_mens_perc_ind_ref = _6.RetornoDiferencial.Value;
                }

                corpo.getLISTA_TAB_RENT_MENSAL.getTAB_RENT_MENSAL.Add(elem);

                #endregion
            }
            #endregion
            #region _7
            if (_7 != null)
            {
                #region Controe LISTA_TAB_RENT_ANUAL

                XML_Lamina.TAB_RENT_MENSAL elem = new XML_Lamina.TAB_RENT_MENSAL();

                elem.MES = _7.Data.Month;
                if (_7.Retorno.HasValue)
                {
                    elem.rent_mens = _7.Retorno.Value;
                }
                if (_7.RetornoBenchmark.HasValue)
                {
                    elem.var_mens_ind_ref = _7.RetornoBenchmark.Value;
                }
                if (_7.RetornoDiferencial.HasValue)
                {
                    elem.rent_mens_perc_ind_ref = _7.RetornoDiferencial.Value;
                }

                corpo.getLISTA_TAB_RENT_MENSAL.getTAB_RENT_MENSAL.Add(elem);

                #endregion
            }
            #endregion
            #region _8
            if (_8 != null)
            {
                #region Controe LISTA_TAB_RENT_ANUAL

                XML_Lamina.TAB_RENT_MENSAL elem = new XML_Lamina.TAB_RENT_MENSAL();

                elem.MES = _8.Data.Month;
                if (_8.Retorno.HasValue)
                {
                    elem.rent_mens = _8.Retorno.Value;
                }
                if (_8.RetornoBenchmark.HasValue)
                {
                    elem.var_mens_ind_ref = _8.RetornoBenchmark.Value;
                }
                if (_8.RetornoDiferencial.HasValue)
                {
                    elem.rent_mens_perc_ind_ref = _8.RetornoDiferencial.Value;
                }

                corpo.getLISTA_TAB_RENT_MENSAL.getTAB_RENT_MENSAL.Add(elem);

                #endregion
            }
            #endregion
            #region _9
            if (_9 != null)
            {
                #region Controe LISTA_TAB_RENT_ANUAL

                XML_Lamina.TAB_RENT_MENSAL elem = new XML_Lamina.TAB_RENT_MENSAL();

                elem.MES = _9.Data.Month;
                if (_9.Retorno.HasValue)
                {
                    elem.rent_mens = _9.Retorno.Value;
                }
                if (_9.RetornoBenchmark.HasValue)
                {
                    elem.var_mens_ind_ref = _9.RetornoBenchmark.Value;
                }
                if (_9.RetornoDiferencial.HasValue)
                {
                    elem.rent_mens_perc_ind_ref = _9.RetornoDiferencial.Value;
                }

                corpo.getLISTA_TAB_RENT_MENSAL.getTAB_RENT_MENSAL.Add(elem);

                #endregion
            }
            #endregion
            #region _10
            if (_10 != null)
            {
                #region Controe LISTA_TAB_RENT_ANUAL

                XML_Lamina.TAB_RENT_MENSAL elem = new XML_Lamina.TAB_RENT_MENSAL();

                elem.MES = _10.Data.Month;
                if (_10.Retorno.HasValue)
                {
                    elem.rent_mens = _10.Retorno.Value;
                }
                if (_10.RetornoBenchmark.HasValue)
                {
                    elem.var_mens_ind_ref = _10.RetornoBenchmark.Value;
                }
                if (_10.RetornoDiferencial.HasValue)
                {
                    elem.rent_mens_perc_ind_ref = _10.RetornoDiferencial.Value;
                }

                corpo.getLISTA_TAB_RENT_MENSAL.getTAB_RENT_MENSAL.Add(elem);

                #endregion
            }
            #endregion
            #region _11
            if (_11 != null)
            {
                #region Controe LISTA_TAB_RENT_ANUAL

                XML_Lamina.TAB_RENT_MENSAL elem = new XML_Lamina.TAB_RENT_MENSAL();

                elem.MES = _11.Data.Month;
                if (_11.Retorno.HasValue)
                {
                    elem.rent_mens = _11.Retorno.Value;
                }
                if (_11.RetornoBenchmark.HasValue)
                {
                    elem.var_mens_ind_ref = _11.RetornoBenchmark.Value;
                }
                if (_11.RetornoDiferencial.HasValue)
                {
                    elem.rent_mens_perc_ind_ref = _11.RetornoDiferencial.Value;
                }

                corpo.getLISTA_TAB_RENT_MENSAL.getTAB_RENT_MENSAL.Add(elem);

                #endregion
            }
            #endregion
            #region _12
            if (_12 != null)
            {
                #region Controe LISTA_TAB_RENT_ANUAL

                XML_Lamina.TAB_RENT_MENSAL elem = new XML_Lamina.TAB_RENT_MENSAL();

                elem.MES = _12.Data.Month;
                if (_12.Retorno.HasValue)
                {
                    elem.rent_mens = _12.Retorno.Value;
                }
                if (_12.RetornoBenchmark.HasValue)
                {
                    elem.var_mens_ind_ref = _12.RetornoBenchmark.Value;
                }
                if (_12.RetornoDiferencial.HasValue)
                {
                    elem.rent_mens_perc_ind_ref = _12.RetornoDiferencial.Value;
                }

                corpo.getLISTA_TAB_RENT_MENSAL.getTAB_RENT_MENSAL.Add(elem);

                #endregion
            }
            #endregion

            decimal? rentabilidade12MesesDiferencial = null;
            if (rentabilidade12Meses.HasValue && rentabilidade12MesesIndice.HasValue)
            {
                if (rentabilidade12MesesIndice != 0)
                {
                    rentabilidade12MesesDiferencial = (rentabilidade12Meses / rentabilidade12MesesIndice) * 100;
                }
            }

            /* Rentabilidade 12 Meses / Rentabilidade 12 Meses Indice / Diferencial */
            //ReportBase.ConfiguraSinalNegativo(((XRTableCell)((XRTableRow)table.Rows[13]).Cells[1]), rentabilidade12Meses, true, ReportBase.NumeroCasasDecimais.DuasCasasDecimais);
            //ReportBase.ConfiguraSinalNegativo(((XRTableCell)((XRTableRow)table.Rows[13]).Cells[2]), rentabilidade12MesesIndice, true, ReportBase.NumeroCasasDecimais.DuasCasasDecimais);
            //ReportBase.ConfiguraSinalNegativo(((XRTableCell)((XRTableRow)table.Rows[13]).Cells[3]), rentabilidade12MesesDiferencial, true, ReportBase.NumeroCasasDecimais.DuasCasasDecimais);

            #endregion
        }

        /// <summary>
        /// Constroe o objeto TAB_VAR_DESEMP
        /// </summary>
        /// <param name="dataPosicao"></param>
        /// <param name="idCliente"></param>
        /// <param name="corpo">Objeto representativo do Corpo do arquivo XML</param>
        private void ControeTAB_VAR_DESEMP(DateTime dataPosicao, int idCliente, XML_Lamina.Corpo corpo, Lamina info) {
            //
            
            decimal desempenho = 0;
            try {
                if (!String.IsNullOrEmpty(info.DesempenhoFundo)) {
                    desempenho = Convert.ToDecimal(info.DesempenhoFundo);
                }
            }
            catch (Exception e4) { // nao faz nada
            }

            corpo.getTAB_VAR_DESEMP.var_desemp = desempenho;

            corpo.getTAB_VAR_DESEMP.FORMULA_RENTAB = "";
            corpo.getTAB_VAR_DESEMP.CENARIO_GATILHO = "";
            corpo.getTAB_VAR_DESEMP.COMENTARIOS = "";
        }

        /// <summary>
        /// Constroe o objeto INFORM_EXEMPLO_COM
        /// </summary>
        /// <param name="dataPosicao"></param>
        /// <param name="idCliente"></param>
        /// <param name="corpo">Objeto representativo do Corpo do arquivo XML</param>
        private void ControeINFORM_EXEMPLO_COMP(DateTime dataPosicao, int idCliente, XML_Lamina.Corpo corpo, Lamina info) {

            DateTime dataInicio = Calendario.RetornaUltimoDiaUtilAno(new DateTime(dataPosicao.Year - 1, 1, 1));
            DateTime dataFim = Calendario.RetornaUltimoDiaUtilAno(new DateTime(dataPosicao.Year, 1, 1));
            //
            HistoricoCota historicoCota1 = new HistoricoCota();
            historicoCota1.LoadByPrimaryKey(dataInicio, idCliente);
            decimal cotaFechamentoInicio = historicoCota1.CotaFechamento.HasValue ? historicoCota1.CotaFechamento.Value : 0;
            //
            HistoricoCota historicoCota2 = new HistoricoCota();
            historicoCota2.LoadByPrimaryKey(dataFim, idCliente);
            decimal cotaFechamentoFim = historicoCota2.CotaFechamento.HasValue ? historicoCota2.CotaFechamento.Value : 0;

            decimal valorBruto = 0;
            if (historicoCota1.CotaFechamento.HasValue) {
                valorBruto = 1000 * (cotaFechamentoFim / cotaFechamentoInicio);
            }
            else {
                valorBruto = 1000 * 1;
            }

            decimal valorIR = 0;

            Carteira carteira = new Carteira();
            carteira.LoadByPrimaryKey(idCliente);

            if (carteira.TipoTributacao.Value == (byte)TipoTributacaoFundo.Acoes) {
                valorIR = 1000 * (cotaFechamentoFim - cotaFechamentoInicio) * 0.15M;
            }
            else if (carteira.TipoTributacao.Value == (byte)TipoTributacaoFundo.Isento) {
                valorIR = 1000 * (cotaFechamentoFim - cotaFechamentoInicio) * 0M;
            }
            else {
                valorIR = 1000 * (cotaFechamentoFim - cotaFechamentoInicio) * 0.20M;
            }

            decimal despesas = this.CalculaDespesas(dataPosicao, idCliente);
            //
            corpo.getINFORM_EXEMPLO_COMP.ANO_ANT = dataPosicao.Year - 1;
            corpo.getINFORM_EXEMPLO_COMP.ANO_EMISS = dataPosicao.Year;
            corpo.getINFORM_EXEMPLO_COMP.valor_resg = valorBruto - valorIR;
            corpo.getINFORM_EXEMPLO_COMP.impostos = valorIR;
            //corpo.getINFORM_EXEMPLO_COMP.VALOR_ING = 0;
            //corpo.getINFORM_EXEMPLO_COMP.VALOR_SAIDA = 0;
            //corpo.getINFORM_EXEMPLO_COMP.AJUSTE_PERFOR = 0;
            corpo.getINFORM_EXEMPLO_COMP.valor_despesas = despesas;
        }

        /// <summary>
        /// Constroe o objeto TAB_SIMUL_DESPESAS
        /// </summary>
        /// <param name="dataPosicao"></param>
        /// <param name="idCliente"></param>
        /// <param name="corpo">Objeto representativo do Corpo do arquivo XML</param>
        private void ControeTAB_SIMUL_DESPESAS(DateTime dataPosicao, int idCliente, XML_Lamina.Corpo corpo, Lamina info) {
            DateTime dataInicio = Calendario.RetornaUltimoDiaUtilAno(new DateTime(dataPosicao.Year, 1, 1));
            //
            HistoricoCota historicoCota = new HistoricoCota();
            historicoCota.LoadByPrimaryKey(dataInicio, idCliente);
            decimal plFechamento = historicoCota.PLFechamento.HasValue ? historicoCota.PLFechamento.Value : 0;

            /* regra de tres
             *   1000 - PL
             *   x  - Despesas
             *      X = (Despesas X 1000) / PL
            */
            decimal despesas = this.CalculaDespesas(dataPosicao, idCliente);

            decimal x = 0M;
            if (plFechamento != 0) {
                x = (despesas * 1000) / plFechamento;
            }

            decimal tresAnos = x * 3;
            decimal cincoAnos = x * 5;
            //
            decimal retornoBruto1 = 331M - tresAnos;
            decimal retornoBruto2 = 610.51M - cincoAnos;
            //
            corpo.getTAB_SIMUL_DESPESAS.desp_prev3 = tresAnos;
            corpo.getTAB_SIMUL_DESPESAS.desp_prev5 = cincoAnos;
            corpo.getTAB_SIMUL_DESPESAS.retor_brut3 = retornoBruto1;
            corpo.getTAB_SIMUL_DESPESAS.retor_brut5 = retornoBruto2;
        }
    }

    #endregion
    #endregion

    #region Versao 1.0
    #region PDF
    public class LaminaEssencialV1 {
        /// <summary>
        /// Retorna uma Stream de Mémoria de um arquivo xml. A Stream permanece aberta
        /// </summary>
        /// <param name="dataPosicao"></param>
        /// <param name="idClientes"></param>
        /// <param name="dicMStream">Saida: Dicionario contendo Nome do arquivo e Memory Stream do arquivo CDA</param>
        /// <returns></returns>
        public void ExportaLaminaV1(DateTime data, List<int> idClientes, out Dictionary<string, MemoryStream> dicMStream) {
            dicMStream = new Dictionary<string, MemoryStream>();

            for (int i = 0; i < idClientes.Count; i++) {
                //
                MemoryStream ms = new MemoryStream();
                string mes = data.Month.ToString();
                if (mes.Length == 1) {
                    mes = "0" + mes;
                }

                string nomeArquivo = idClientes[i] + "_" + mes + data.Year.ToString() + ".pdf";
                //
                ReportLaminaExportacaoV1 report = new ReportLaminaExportacaoV1(idClientes[i], data);
                report.ExportToPdf(ms);

                /* Necessário voltar o ponteiro do arquivo para o Inicio */
                ms.Seek(0, SeekOrigin.Begin);
                //
                // Adiciona no Dicionário
                dicMStream.Add(nomeArquivo, ms);
            }
        }
    }
    #endregion

    #region Exportacao XML
    [Serializable()]
    [XmlRootAttribute(ElementName = "DOC_ARQ", IsNullable = false, Namespace = "urn:lamina")]
    public class XML_LaminaV1 {

        [XmlElement(ElementName = "CAB_INFORM")]
        public Cabecalho cabecalho = new Cabecalho();

        [XmlArray("LISTA_INFORM")]
        [XmlArrayItem("INFORM")]
        public List<Corpo> corpo = new List<Corpo>();

        #region Cabecalho
        public class Cabecalho {

            public Cabecalho() { }

            [XmlElement("COD_DOC", Order = 1)]
            public int codigoDocumento;

            [XmlElement("VERSAO", Order = 2)]
            public string versao;

            #region DT_COMPT
            [XmlIgnore]
            public DateTime dataCompetencia;

            [XmlElement("DT_COMPT", Order = 3)]
            public string formatDataCompetencia {
                get { return this.dataCompetencia.ToString("MM/yyyy"); }
                set { dataCompetencia = DateTime.Parse(value); }
            }
            #endregion

            #region DT_GERAC_ARQ
            [XmlIgnore]
            public DateTime dataGeracaoArquivo;

            [XmlElement("DT_GERAC_ARQ", Order = 4)]
            public string formatDataGeracaoArquivo {
                get { return this.dataGeracaoArquivo.ToString("dd/MM/yyyy"); }
                set { dataGeracaoArquivo = DateTime.Parse(value); }
            }
            #endregion
        }
        #endregion

        #region Corpo
        public class Corpo {

            public Corpo() { }

            /**********************************************/
            [XmlElement("CNPJ_FUNDO", Order = 1)]
            public string cnpj;

            [XmlElement("NOME_FANTASIA", Order = 2)]
            public string nomeFantasia;

            [XmlElement("END_ELETR", Order = 3)]
            public string enderecoEletronico;

            [XmlElement("DESC_PUB_ALVO", Order = 4)]
            public string descricaoPublicoAlvo;

            [XmlElement("RESTR_INV", Order = 5)]
            public string RESTR_INV;

            [XmlElement("DESC_OBJET", Order = 6)]
            public string DESC_OBJET;

            [XmlElement("DESC_POL_INV", Order = 7)]
            public string descricaoPoliticaInvestimento;

            /* ----------------------------------------------------------- */

            [XmlIgnore]
            public decimal ativo_ext;

            #region ATIVO_EXT
            [XmlElement("ATIVO_EXT", Order = 8)]
            public string format_ATIVO_EXT {

                get {
                    return ativo_ext.ToString("N2").Replace(".", "");
                }
                set {
                    decimal ativo_ext = 0;

                    if (Decimal.TryParse(value, out ativo_ext))
                        ativo_ext = ativo_ext;
                }
            }
            #endregion

            [XmlIgnore]
            public decimal CRED_PRIV;

            #region CRED_PRIV
            [XmlElement("CRED_PRIV", Order = 9)]
            public string format_CRED_PRIV {

                get {
                    return CRED_PRIV.ToString("N2").Replace(".", "");
                }
                set {
                    decimal CRED_PRIV = 0;

                    if (Decimal.TryParse(value, out CRED_PRIV))
                        CRED_PRIV = CRED_PRIV;
                }
            }
            #endregion

            [XmlIgnore]
            public decimal ALAV;

            #region ALAV
            [XmlElement("ALAV", Order = 10)]
            public string format_ALAV {

                get {
                    return ALAV.ToString("N2").Replace(".", "");
                }
                set {
                    decimal ALAV = 0;

                    if (Decimal.TryParse(value, out ALAV))
                        ALAV = ALAV;
                }
            }
            #endregion

            [XmlIgnore]
            public decimal CONC_EMISSOR;

            #region CONC_EMISSOR
            [XmlElement("CONC_EMISSOR", Order = 11)]
            public string format_CONC_EMISSOR {

                get {
                    return CONC_EMISSOR.ToString("N2").Replace(".", "");
                }
                set {
                    decimal CONC_EMISSOR = 0;

                    if (Decimal.TryParse(value, out CONC_EMISSOR))
                        CONC_EMISSOR = CONC_EMISSOR;
                }
            }
            #endregion

            [XmlElement("PERDAS_SIGN", Order = 12)]
            public string perdasSignificativas;

            [XmlElement("PL_NEGAT", Order = 13)]
            public string PL_NEGAT;

            [XmlIgnore]
            public decimal INV_INICIAL;

            #region INV_INICIAL
            [XmlElement("INV_INICIAL", Order = 14)]
            public string format_INV_INICIAL {

                get {
                    return INV_INICIAL.ToString("N2").Replace(".", "");
                }
                set {
                    decimal INV_INICIAL = 0;

                    if (Decimal.TryParse(value, out INV_INICIAL))
                        INV_INICIAL = INV_INICIAL;
                }
            }
            #endregion

            [XmlIgnore]
            public decimal INV_ADIC;

            #region INV_ADIC
            [XmlElement("INV_ADIC", Order = 15)]
            public string format_INV_ADIC {

                get {
                    return INV_ADIC.ToString("N2").Replace(".", "");
                }
                set {
                    decimal INV_ADIC = 0;

                    if (Decimal.TryParse(value, out INV_ADIC))
                        INV_ADIC = INV_ADIC;
                }
            }
            #endregion

            [XmlIgnore]
            public decimal RESG_MIN;

            #region RESG_MIN
            [XmlElement("RESG_MIN", Order = 16)]
            public string format_RESG_MIN {

                get {
                    return RESG_MIN.ToString("N2").Replace(".", "");
                    //return RESG_MIN.ToString("N2");
                }
                set {
                    decimal RESG_MIN = 0;

                    if (Decimal.TryParse(value, out RESG_MIN))
                        RESG_MIN = RESG_MIN;
                }
            }
            #endregion

            //#region HOR_APL_RESG
            //public DateTime HOR_APL_RESG;

            //[XmlElement("HOR_APL_RESG", Order = 18)]
            //public string formatHOR_APL_RESG {
            //    get { return this.HOR_APL_RESG.ToString("hh:mm"); }
            //    set { HOR_APL_RESG = DateTime.Parse(value); }
            //}
            //#endregion

            #region HOR_APL_RESG
            [XmlIgnore]
            public DateTime? HOR_APL_RESG;

            [XmlElement("HOR_APL_RESG", Order = 17)]
            public string formatHOR_APL_RESG {
                get {
                    return this.HOR_APL_RESG.HasValue ? this.HOR_APL_RESG.Value.ToString("HH:mm") : String.Empty;
                }
                set {
                    if (!this.HOR_APL_RESG.HasValue) {
                        formatHOR_APL_RESG = String.Empty;
                    }
                    else {
                        HOR_APL_RESG = DateTime.Parse(value);
                    }
                }
            }
            #endregion

            [XmlIgnore]
            public decimal VALOR_PERM;

            #region VALOR_PERM
            [XmlElement("VALOR_PERM", Order = 18)]
            public string format_VALOR_PERM {

                get {
                    return VALOR_PERM.ToString("N2").Replace(".", "");
                }
                set {
                    decimal VALOR_PERM = 0;

                    if (Decimal.TryParse(value, out VALOR_PERM))
                        VALOR_PERM = VALOR_PERM;
                }
            }
            #endregion
            /* ----------------------------------------------------------- */

            #region Carencia
            [XmlElementAttribute("CARENCIA", Order = 19)]
            public CARENCIA carencia = new CARENCIA();

            [XmlIgnore]
            public CARENCIA getCARENCIA {
                get { return carencia; }
                set { carencia = value; }
            }
            #endregion

            #region Conversao
            [XmlElementAttribute("CONVERSAO", Order = 20)]
            public CONVERSAO conversao = new CONVERSAO();

            [XmlIgnore]
            public CONVERSAO getCONVERSAO {
                get { return conversao; }
                set { conversao = value; }
            }
            #endregion

            [XmlElement("PRAZO_PAGAM", Order = 21)]
            public string PRAZO_PAGAM;

            #region INFOR_TX_ADM
            [XmlElementAttribute("INFOR_TX_ADM", Order = 22)]
            public INFOR_TX_ADM infor_tx_adm = new INFOR_TX_ADM();

            [XmlIgnore]
            public INFOR_TX_ADM getINFOR_TX_ADM {
                get { return infor_tx_adm; }
                set { infor_tx_adm = value; }
            }
            #endregion

            #region INFOR_TX_ENTR
            [XmlElementAttribute("INFOR_TX_ENTR", Order = 23)]
            public INFOR_TX_ENTR infortxentr = new INFOR_TX_ENTR();

            [XmlIgnore]
            public INFOR_TX_ENTR getINFOR_TX_ENTR {
                get { return infortxentr; }
                set { infortxentr = value; }
            }
            #endregion

            #region INFOR_TX_SAIDA
            [XmlElementAttribute("INFOR_TX_SAIDA", Order = 24)]
            public INFOR_TX_SAIDA infortxsaida = new INFOR_TX_SAIDA();

            [XmlIgnore]
            public INFOR_TX_SAIDA getINFOR_TX_SAIDA {
                get { return infortxsaida; }
                set { infortxsaida = value; }
            }
            #endregion

            [XmlIgnore]
            public string tx_desempenho;

            #region TX_DESEMPENHO
            [XmlElement("TX_DESEMPENHO", Order = 25)]
            public string format_TX_DESEMPENHO {

                get {
                    return tx_desempenho; //.ToString("N2").Replace(".", "");
                }
                set {
                    string tx_desempenho = "0";

                    //if (Decimal.TryParse(value, out tx_desempenho))
                    //    tx_desempenho = tx_desempenho;
                }
            }
            #endregion

            #region INFOR_DESPESAS
            [XmlElementAttribute("INFOR_DESPESAS", Order = 26)]
            public INFOR_DESPESAS infor_despesas = new INFOR_DESPESAS();

            [XmlIgnore]
            public INFOR_DESPESAS getINFOR_DESPESAS {
                get { return infor_despesas; }
                set { infor_despesas = value; }
            }
            #endregion

            #region LISTA_COMP_CARTEIRA
            [XmlElementAttribute("LISTA_COMP_CARTEIRA", Order = 27)]
            public LISTA_COMP_CARTEIRA lista_comp_carteira = new LISTA_COMP_CARTEIRA();

            [XmlIgnore]
            public LISTA_COMP_CARTEIRA getLISTA_COMP_CARTEIRA {
                get { return lista_comp_carteira; }
                set { lista_comp_carteira = value; }
            }
            #endregion

            [XmlElement("CLASSIF_RISCO", Order = 28)]
            public int classificacaoRisco;

            #region INFOR_TAB_RENT_ANUAL
            [XmlElementAttribute("INFOR_TAB_RENT_ANUAL", Order = 29)]
            public INFOR_TAB_RENT_ANUAL infor_tab_rent_anual = new INFOR_TAB_RENT_ANUAL();

            [XmlIgnore]
            public INFOR_TAB_RENT_ANUAL getINFOR_TAB_RENT_ANUAL {
                get { return infor_tab_rent_anual; }
                set { infor_tab_rent_anual = value; }
            }
            #endregion

            #region LISTA_TAB_RENT_ANUAL
            [XmlElementAttribute("LISTA_TAB_RENT_ANUAL", Order = 30)]
            public LISTA_TAB_RENT_ANUAL lista_tab_rent_anual = new LISTA_TAB_RENT_ANUAL();

            [XmlIgnore]
            public LISTA_TAB_RENT_ANUAL getLISTA_TAB_RENT_ANUAL {
                get { return lista_tab_rent_anual; }
                set { lista_tab_rent_anual = value; }
            }
            #endregion

            #region LISTA_TAB_RENT_MENSAL
            [XmlElementAttribute("LISTA_TAB_RENT_MENSAL", Order = 31)]
            public LISTA_TAB_RENT_MENSAL lista_tab_rent_mensal = new LISTA_TAB_RENT_MENSAL();

            [XmlIgnore]
            public LISTA_TAB_RENT_MENSAL getLISTA_TAB_RENT_MENSAL {
                get { return lista_tab_rent_mensal; }
                set { lista_tab_rent_mensal = value; }
            }
            #endregion

            [XmlElementAttribute("FORM_CALC_RENT", Order = 32)]
            public string FORM_CALC_RENT;

            #region TAB_VAR_DESEMP
            [XmlElementAttribute("TAB_VAR_DESEMP", Order = 33)]
            public TAB_VAR_DESEMP tab_var_desemp = new TAB_VAR_DESEMP();

            [XmlIgnore]
            public TAB_VAR_DESEMP getTAB_VAR_DESEMP {
                get { return tab_var_desemp; }
                set { tab_var_desemp = value; }
            }
            #endregion

            #region INFORM_EXEMPLO_COMP
            [XmlElementAttribute("INFORM_EXEMPLO_COMP", Order = 34)]
            public INFORM_EXEMPLO_COMP inform_exemplo_comp = new INFORM_EXEMPLO_COMP();

            [XmlIgnore]
            public INFORM_EXEMPLO_COMP getINFORM_EXEMPLO_COMP {
                get { return inform_exemplo_comp; }
                set { inform_exemplo_comp = value; }
            }
            #endregion

            #region TAB_SIMUL_DESPESAS
            [XmlElementAttribute("TAB_SIMUL_DESPESAS", Order = 35)]
            public TAB_SIMUL_DESPESAS tab_simul_despesas = new TAB_SIMUL_DESPESAS();

            [XmlIgnore]
            public TAB_SIMUL_DESPESAS getTAB_SIMUL_DESPESAS {
                get { return tab_simul_despesas; }
                set { tab_simul_despesas = value; }
            }
            #endregion

            [XmlElement("TEL_SAC", Order = 36)]
            public long telefone;

            [XmlElement("RECLAMACOES", Order = 37)]
            public string reclamacoes;
        }
        #endregion
        /* ----------------------------------------------------------- */

        #region CARENCIA
        public class CARENCIA {
            // Construtor
            public CARENCIA() { }

            [XmlElement("DIAS_CAR", Order = 1)]
            public int DIAS_CAR;

            [XmlElement("COND_CAR", Order = 2)]
            public string COND_CAR;
        }
        #endregion

        #region CONVERSAO
        public class CONVERSAO {
            // Construtor
            public CONVERSAO() { }

            [XmlElement("ABERT_FECH", Order = 1)]
            public string ABERT_FECH;

            [XmlElement("DIA_APLIC", Order = 2)]
            public int DIA_APLIC;

            [XmlElement("ABERT_FECH_C", Order = 3)]
            public string ABERT_FECH_C;

            [XmlElement("DIA_RESG", Order = 4)]
            public int DIA_RESG;
        }
        #endregion

        #region INFOR_TX_ADM
        public class INFOR_TX_ADM {
            // Construtor
            public INFOR_TX_ADM() { }

            [XmlElement("TIPO_TX_ADM", Order = 1)]
            public string TIPO_TX_ADM;

            [XmlIgnore]
            public decimal tx_adm;

            #region TX_ADM
            [XmlElement("TX_ADM", Order = 2)]
            public string format_TX_ADM {

                get {
                    return tx_adm.ToString("N2").Replace(".", "");
                }
                set {
                    decimal tx_adm = 0;

                    if (Decimal.TryParse(value, out tx_adm))
                        tx_adm = tx_adm;
                }
            }
            #endregion

            [XmlIgnore]
            public decimal tx_min;

            #region TX_MIN
            [XmlElement("TX_MIN", Order = 3)]
            public string format_TX_MIN {

                get {
                    return tx_min.ToString("N2").Replace(".", "");
                }
                set {
                    decimal tx_min = 0;

                    if (Decimal.TryParse(value, out tx_min))
                        tx_min = tx_min;
                }
            }
            #endregion

            [XmlIgnore]
            public decimal tx_max;

            #region TX_MAX
            [XmlElement("TX_MAX", Order = 4)]
            public string format_TX_MAX {

                get {
                    return tx_max.ToString("N2").Replace(".", "");
                }
                set {
                    decimal tx_max = 0;

                    if (Decimal.TryParse(value, out tx_max))
                        tx_max = tx_max;
                }
            }
            #endregion

            [XmlElement("ESCLAREC", Order = 5)]
            public string ESCLAREC;
        }
        #endregion

        #region INFOR_TX_ENTR
        public class INFOR_TX_ENTR {
            // Construtor
            public INFOR_TX_ENTR() { }

            [XmlElement("TX_ENTR", Order = 1)]
            public int TX_ENTR;

            [XmlElement("COND_ENTR", Order = 2)]
            public string COND_ENTR;
        }
        #endregion

        #region INFOR_TX_SAIDA
        public class INFOR_TX_SAIDA {
            // Construtor
            public INFOR_TX_SAIDA() { }

            [XmlElement("DIAS_SAIDA", Order = 1)]
            public int DIAS_SAIDA;

            [XmlElement("TX_SAIDA", Order = 2)]
            public int TX_SAIDA;

            [XmlElement("COND_SAIDA", Order = 3)]
            public string COND_SAIDA;
        }
        #endregion

        #region INFOR_DESPESAS
        public class INFOR_DESPESAS {
            // Construtor
            public INFOR_DESPESAS() { }

            [XmlIgnore]
            public decimal despesas;

            #region DESPESAS
            [XmlElement("DESPESAS", Order = 1)]
            public string format_DESPESAS {

                get {
                    return despesas.ToString("N2").Replace(".", "");
                }
                set {
                    decimal despesas = 0;

                    if (Decimal.TryParse(value, out despesas))
                        despesas = despesas;
                }
            }
            #endregion

            #region INIC_PERIODO
            [XmlIgnore]
            public DateTime inic_periodo;

            [XmlElement("INIC_PERIODO", Order = 2)]
            public string formatINIC_PERIODO {
                get { return this.inic_periodo.ToString("dd/MM/yyyy"); }
                set { inic_periodo = DateTime.Parse(value); }
            }
            #endregion

            #region FIM_PERIODO
            [XmlIgnore]
            public DateTime fim_periodo;

            [XmlElement("FIM_PERIODO", Order = 3)]
            public string formatFIM_PERIODO {
                get { return this.fim_periodo.ToString("dd/MM/yyyy"); }
                set { fim_periodo = DateTime.Parse(value); }
            }
            #endregion

            [XmlElement("END_ELET_ADM", Order = 4)]
            public string END_ELET_ADM;
        }
        #endregion

        /* ----------------------------------------------------------- */

        public class LISTA_COMP_CARTEIRA {
            // Construtor
            public LISTA_COMP_CARTEIRA() { }

            [XmlIgnore]
            public decimal pl_fundo;

            #region PL_FUNDO
            [XmlElement("PL_FUNDO", Order = 1)]
            public string format_PL_FUNDO {

                get {
                    return pl_fundo.ToString("N2").Replace(".", "");
                }
                set {
                    decimal pl_fundo = 0;

                    if (Decimal.TryParse(value, out pl_fundo))
                        pl_fundo = pl_fundo;
                }
            }
            #endregion

            [XmlElementAttribute("COMP_CARTEIRA", Order = 2)]
            public List<COMP_CARTEIRA> comp_carteira = new List<COMP_CARTEIRA>();

            [XmlIgnore]
            public List<COMP_CARTEIRA> getCOMP_CARTEIRA {
                get { return comp_carteira; }
                set { comp_carteira = value; }
            }
        }

        #region COMP_CARTEIRA
        public class COMP_CARTEIRA {
            // Construtor
            public COMP_CARTEIRA() { }

            [XmlElement("ESPECIE_ATIVO", Order = 1)]
            public string ESPECIE_ATIVO;

            [XmlIgnore]
            public decimal perc_pl;

            #region PERC_PL
            [XmlElement("PERC_PL", Order = 2)]
            public string format_PERC_PL {

                get {
                    return perc_pl.ToString("N2").Replace(".", "");
                }
                set {
                    decimal perc_pl = 0;

                    if (Decimal.TryParse(value, out perc_pl))
                        perc_pl = perc_pl;
                }
            }
            #endregion
        }
        #endregion

        /* ----------------------------------------------------------- */

        #region INFOR_TAB_RENT_ANUAL
        public class INFOR_TAB_RENT_ANUAL {
            // Construtor
            public INFOR_TAB_RENT_ANUAL() { }

            [XmlIgnore]
            public decimal rent_5anos; // * Campo não obrigatório para fundos com menos de um ano de funcionamento

            #region RENT_5ANOS
            [XmlElement("RENT_5ANOS", Order = 1)]
            public string format_RENT_5ANOS {

                get {
                    return rent_5anos.ToString("N2").Replace(".", "");
                }
                set {
                    decimal rent_5anos = 0;

                    if (Decimal.TryParse(value, out rent_5anos))
                        rent_5anos = rent_5anos;
                }
            }
            #endregion

            [XmlElement("IND_REFER", Order = 2)]
            public string IND_REFER; // * CAMPO OPCIONAL

            [XmlIgnore]
            public decimal rent_ind_ref; // * CAMPO OPCIONAL

            #region RENT_IND_REF
            [XmlElement("RENT_IND_REF", Order = 3)]
            public string format_RENT_IND_REF {

                get {
                    return rent_ind_ref.ToString("N2").Replace(".", "");
                }
                set {
                    decimal rent_ind_ref = 0;

                    if (Decimal.TryParse(value, out rent_ind_ref))
                        rent_ind_ref = rent_ind_ref;
                }
            }
            #endregion

            [XmlElement("ANOS_ABAIXO", Order = 4)]
            public int ANOS_ABAIXO; // * CAMPO OPCIONAL

            #region INIC_FUNCIO
            [XmlIgnore]
            public DateTime inic_funcio;  // * Campo não obrigatório para fundos com menos de um ano de funcionamento

            [XmlElement("INIC_FUNCIO", Order = 5)]
            public string formatINIC_FUNCIO {
                get { return this.inic_funcio.ToString("dd/MM/yyyy"); }
                set { inic_funcio = DateTime.Parse(value); }
            }
            #endregion

            [XmlElementAttribute("ANTES_INIC2", Order = 6)]
            public List<ANTES_INIC2> antes_inic2 = new List<ANTES_INIC2>();

            [XmlIgnore]
            public List<ANTES_INIC2> getANTES_INIC2 {
                get { return antes_inic2; }
                set { antes_inic2 = value; }
            }
        }
        #endregion

        #region ANTES_INIC2
        public class ANTES_INIC2 {
            // Construtor
            public ANTES_INIC2() { }

            [XmlElement("ANO", Order = 1)]
            public int? ANO1;

            [XmlElement("ANO", Order = 2)]
            public int? ANO2;

            [XmlElement("ANO", Order = 3)]
            public int? ANO3;

            [XmlElement("ANO", Order = 4)]
            public int? ANO4;

            [XmlElement("ANO", Order = 5)]
            public int? ANO5;

            // Controla Se vai aparecer se for null
            public bool ShouldSerializeANO1() {
                return !(ANO1 == null);
            }
            public bool ShouldSerializeANO2() {
                return !(ANO2 == null);
            }

            public bool ShouldSerializeANO3() {
                return !(ANO3 == null);
            }
            public bool ShouldSerializeANO4() {
                return !(ANO4 == null);
            }
            public bool ShouldSerializeANO5() {
                return !(ANO5 == null);
            }
        }
        #endregion

        /* ----------------------------------------------------------- */

        public class LISTA_TAB_RENT_ANUAL {
            // Construtor
            public LISTA_TAB_RENT_ANUAL() { }

            [XmlElementAttribute("TAB_RENT_ANUAL")]
            public List<TAB_RENT_ANUAL> tab_rent_anual = new List<TAB_RENT_ANUAL>();

            [XmlIgnore]
            public List<TAB_RENT_ANUAL> getTAB_RENT_ANUAL {
                get { return tab_rent_anual; }
                set { tab_rent_anual = value; }
            }
        }

        #region TAB_RENT_ANUAL
        public class TAB_RENT_ANUAL {
            // Construtor
            public TAB_RENT_ANUAL() { }

            [XmlElement("ANO_RENT", Order = 1)]
            public int ANO_RENT;

            [XmlIgnore]
            public decimal rent_liq;

            #region RENT_LIQ
            [XmlElement("RENT_LIQ", Order = 2)]
            public string format_RENT_LIQ {

                get {
                    return rent_liq.ToString("N2").Replace(".", "");
                }
                set {
                    decimal rent_liq = 0;

                    if (Decimal.TryParse(value, out rent_liq))
                        rent_liq = rent_liq;
                }
            }
            #endregion

            [XmlIgnore]
            public decimal var_ind_ref;

            #region VAR_IND_REF
            [XmlElement("VAR_IND_REF", Order = 3)]
            public string format_VAR_IND_REF {

                get {
                    return var_ind_ref.ToString("N2").Replace(".", "");
                }
                set {
                    decimal var_ind_ref = 0;

                    if (Decimal.TryParse(value, out var_ind_ref))
                        var_ind_ref = var_ind_ref;
                }
            }
            #endregion

            [XmlIgnore]
            public decimal rent_perc_ind_ref;

            #region RENT_PERC_IND_REF
            [XmlElement("RENT_PERC_IND_REF", Order = 4)]
            public string format_RENT_PERC_IND_REF {

                get {
                    return rent_perc_ind_ref.ToString("N2").Replace(".", "");
                }
                set {
                    decimal rent_perc_ind_ref = 0;

                    if (Decimal.TryParse(value, out rent_perc_ind_ref))
                        rent_perc_ind_ref = rent_perc_ind_ref;
                }
            }
            #endregion

            [XmlElement("COMENTARIOS", Order = 5)]
            public string COMENTARIOS;
        }
        #endregion

        /* ----------------------------------------------------------- */

        public class LISTA_TAB_RENT_MENSAL {
            // Construtor
            public LISTA_TAB_RENT_MENSAL() { }

            [XmlElementAttribute("TAB_RENT_MENSAL")]
            public List<TAB_RENT_MENSAL> tab_rent_mensal = new List<TAB_RENT_MENSAL>();

            [XmlIgnore]
            public List<TAB_RENT_MENSAL> getTAB_RENT_MENSAL {
                get { return tab_rent_mensal; }
                set { tab_rent_mensal = value; }
            }
        }

        #region TAB_RENT_MENSAL
        public class TAB_RENT_MENSAL {
            // Construtor
            public TAB_RENT_MENSAL() { }

            [XmlElement("MES", Order = 1)]
            public int MES;

            [XmlIgnore]
            public decimal rent_mens;

            #region RENT_MENS
            [XmlElement("RENT_MENS", Order = 2)]
            public string format_RENT_MENS {

                get {
                    return rent_mens.ToString("N2").Replace(".", "");
                }
                set {
                    decimal rent_mensal = 0;

                    if (Decimal.TryParse(value, out rent_mensal))
                        rent_mensal = rent_mensal;
                }
            }
            #endregion

            [XmlIgnore]
            public decimal var_mens_ind_ref;

            #region VAR_MENS_IND_REF
            [XmlElement("VAR_MENS_IND_REF", Order = 3)]
            public string format_VAR_MENS_IND_REF {

                get {
                    return var_mens_ind_ref.ToString("N2").Replace(".", "");
                }
                set {
                    decimal var_mens_ind_ref = 0;

                    if (Decimal.TryParse(value, out var_mens_ind_ref))
                        var_mens_ind_ref = var_mens_ind_ref;
                }
            }
            #endregion

            [XmlIgnore]
            public decimal rent_mens_perc_ind_ref;

            #region VAR_IND_REF
            [XmlElement("RENT_MENS_PERC_IND_REF", Order = 4)]
            public string format_RENT_MENS_PERC_IND_REF {

                get {
                    return rent_mens_perc_ind_ref.ToString("N2").Replace(".", "");
                }
                set {
                    decimal rent_mens_perc_ind_ref = 0;

                    if (Decimal.TryParse(value, out rent_mens_perc_ind_ref))
                        rent_mens_perc_ind_ref = rent_mens_perc_ind_ref;
                }
            }
            #endregion

            [XmlElement("COMENTARIOS", Order = 5)]
            public string COMENTARIOS;
        }
        #endregion

        /* ----------------------------------------------------------- */

        #region TAB_VAR_DESEMP
        public class TAB_VAR_DESEMP {
            // Construtor
            public TAB_VAR_DESEMP() { }

            [XmlIgnore]
            public decimal var_desemp;  // * Campo não obrigatório para fundos com menos de um ano de funcionamento

            #region VAR_DESEMP
            [XmlElement("VAR_DESEMP", Order = 1)]
            public string format_VAR_DESEMP {

                get {
                    return var_desemp.ToString("N2").Replace(".", "");
                }
                set {
                    decimal var_desemp = 0;

                    if (Decimal.TryParse(value, out var_desemp))
                        var_desemp = var_desemp;
                }
            }
            #endregion

            //[XmlElement("VAR_DESEMP", Order = 1)] // * Campo não obrigatório para fundos com menos de um ano de funcionamento
            //public string VAR_DESEMP;

            [XmlElement("FORMULA_RENTAB", Order = 2)]
            public string FORMULA_RENTAB;

            [XmlElement("CENARIO_GATILHO", Order = 3)]
            public string CENARIO_GATILHO; //  *CAMPO OPCIONAL

            [XmlElement("ESCLAREC", Order = 4)]
            public string COMENTARIOS; //  *CAMPO OPCIONAL

        }
        #endregion

        #region INFORM_EXEMPLO_COMP
        public class INFORM_EXEMPLO_COMP {
            // Construtor
            public INFORM_EXEMPLO_COMP() { }

            [XmlElement("ANO_ANT", Order = 1)] //* Campo não obrigatório para fundos com menos de um ano de funcionamento
            public int ANO_ANT;

            [XmlElement("ANO_EMISS", Order = 2)] //* Campo não obrigatório para fundos com menos de um ano de funcionamento
            public int ANO_EMISS;

            [XmlIgnore]
            public decimal valor_resg; // * Campo não obrigatório para fundos com menos de um ano de funcionamento

            #region VALOR_RESG
            [XmlElement("VALOR_RESG", Order = 3)]
            public string format_VALOR_RESG {

                get {
                    return valor_resg.ToString("N2").Replace(".", "");
                }
                set {
                    decimal valor_resg = 0;

                    if (Decimal.TryParse(value, out valor_resg))
                        valor_resg = valor_resg;
                }
            }
            #endregion

            [XmlIgnore]
            public decimal impostos; // * Campo não obrigatório para fundos com menos de um ano de funcionamento

            #region IMPOSTOS
            [XmlElement("IMPOSTOS", Order = 4)]
            public string format_IMPOSTOS {

                get {
                    return impostos.ToString("N2").Replace(".", "");
                }
                set {
                    decimal impostos = 0;

                    if (Decimal.TryParse(value, out valor_resg))
                        impostos = impostos;
                }
            }
            #endregion

            [XmlIgnore]
            public decimal valor_ing;  // *CAMPO OPCIONAL

            #region VALOR_ING
            [XmlElement("VALOR_ING", Order = 5)]
            public string format_VALOR_ING {

                get {
                    return valor_ing.ToString("N2").Replace(".", "");
                }
                set {
                    decimal valor_ing = 0;

                    if (Decimal.TryParse(value, out valor_ing))
                        valor_ing = valor_ing;
                }
            }
            #endregion

            [XmlIgnore]
            public decimal valor_saida;  // *CAMPO OPCIONAL

            #region VALOR_SAIDA
            [XmlElement("VALOR_SAIDA", Order = 6)]
            public string format_VALOR_SAIDA {

                get {
                    return valor_saida.ToString("N2").Replace(".", "");
                }
                set {
                    decimal valor_saida = 0;

                    if (Decimal.TryParse(value, out valor_saida))
                        valor_saida = valor_saida;
                }
            }
            #endregion

            [XmlIgnore]
            public decimal ajuste_perfor;  // *CAMPO OPCIONAL

            #region AJUSTE_PERFOR
            [XmlElement("AJUSTE_PERFOR", Order = 7)]
            public string format_AJUSTE_PERFOR {

                get {
                    return ajuste_perfor.ToString("N2").Replace(".", "");
                }
                set {
                    decimal ajuste_perfor = 0;

                    if (Decimal.TryParse(value, out ajuste_perfor))
                        ajuste_perfor = ajuste_perfor;
                }
            }
            #endregion

            [XmlIgnore]
            public decimal valor_despesas;  // *CAMPO OPCIONAL

            #region VALOR_DESPESAS
            [XmlElement("VALOR_DESPESAS", Order = 8)]
            public string format_VALOR_DESPESAS {

                get {
                    return valor_despesas.ToString("N2").Replace(".", "");
                }
                set {
                    decimal ajustevalor_despesas_perfor = 0;

                    if (Decimal.TryParse(value, out valor_despesas))
                        valor_despesas = valor_despesas;
                }
            }
            #endregion
        }
        #endregion

        #region TAB_SIMUL_DESPESAS
        public class TAB_SIMUL_DESPESAS {
            // Construtor
            public TAB_SIMUL_DESPESAS() { }

            [XmlIgnore]
            public decimal desp_prev3;

            #region DESP_PREV3
            [XmlElement("DESP_PREV3", Order = 1)]
            public string format_DESP_PREV3 {

                get {
                    return desp_prev3.ToString("N2").Replace(".", "");
                }
                set {
                    decimal desp_prev3 = 0;

                    if (Decimal.TryParse(value, out desp_prev3))
                        desp_prev3 = desp_prev3;
                }
            }
            #endregion

            [XmlIgnore]
            public decimal desp_prev5;

            #region DESP_PREV5
            [XmlElement("DESP_PREV5", Order = 2)]
            public string format_DESP_PREV5 {

                get {
                    return desp_prev5.ToString("N2").Replace(".", "");
                }
                set {
                    decimal desp_prev5 = 0;

                    if (Decimal.TryParse(value, out desp_prev5))
                        desp_prev5 = desp_prev5;
                }
            }
            #endregion

            [XmlIgnore]
            public decimal retor_brut3;

            #region RETOR_BRUT3
            [XmlElement("RETOR_BRUT3", Order = 3)]
            public string format_RETOR_BRUT3 {

                get {
                    return retor_brut3.ToString("N2").Replace(".", "");
                }
                set {
                    decimal retor_brut3 = 0;

                    if (Decimal.TryParse(value, out retor_brut3))
                        retor_brut3 = retor_brut3;
                }
            }
            #endregion

            [XmlIgnore]
            public decimal retor_brut5;

            #region RETOR_BRUT5
            [XmlElement("RETOR_BRUT5", Order = 4)]
            public string format_RETOR_BRUT5 {

                get {
                    return retor_brut5.ToString("N2").Replace(".", "");
                }
                set {
                    decimal retor_brut5 = 0;

                    if (Decimal.TryParse(value, out retor_brut5))
                        retor_brut5 = retor_brut5;
                }
            }
            #endregion
        }
        #endregion
    }

    public class LaminaEssencialXMLV1 {

        #region Atributos Privates
        private int ano;

        public int Ano {
            get { return ano; }
            set { ano = value; }
        }

        // Armazena os últimos 5 anos em relação ao ano passado em ordem crescente
        private List<int> ultimos_5_Anos;

        /* Valores da Table1 */
        private List<CalculoMedida.EstatisticaRetornoMensal> listaRetornosMensais;

        /* Valores da Table2 */
        private List<CalculoMedida.EstatisticaRetornoAnual> listaRetornosAnuais;

        #endregion

        #region Funções Personalizadas

        // Ordena um dicionario por valor
        private Dictionary<string, decimal> SortDictionaryByValue(Dictionary<string, decimal> myDictionary) {
            List<KeyValuePair<string, decimal>> tempList = new List<KeyValuePair<string, decimal>>(myDictionary);

            tempList.Sort(delegate(KeyValuePair<string, decimal> firstPair, KeyValuePair<string, decimal> secondPair) {
                return firstPair.Value.CompareTo(secondPair.Value);
            }
                         );

            Dictionary<string, decimal> mySortedDictionary = new Dictionary<string, decimal>();
            foreach (KeyValuePair<string, decimal> pair in tempList) {
                mySortedDictionary.Add(pair.Key, pair.Value);
            }

            return mySortedDictionary;
        }

        // Inicializa as Rentabilidades
        public void InitializeRentabilidades(int idCliente, DateTime data) {

            // Carrega a Cliente
            Cliente cliente = new Cliente();
            List<esQueryItem> campos = new List<esQueryItem>();
            campos.Add(cliente.Query.Nome);
            campos.Add(cliente.Query.Apelido);
            campos.Add(cliente.Query.IdCliente);
            campos.Add(cliente.Query.Apelido);
            campos.Add(cliente.Query.DataDia);
            //
            cliente.LoadByPrimaryKey(campos, idCliente);

            #region Rentabilidade Anual/Mensal
            this.ano = data.Year;

            this.ultimos_5_Anos = new List<int>(new int[5] { 
                this.ano - 4, this.ano - 3,
                this.ano - 2, this.ano - 1,
                this.ano
            });

            Carteira carteira = new Carteira();
            carteira.LoadByPrimaryKey(idCliente);

            //int mesInicio = 1;
            //DateTime dataInicio = new DateTime(this.ano, mesInicio, 01);
            DateTime dataInicioAux = data.AddMonths(-11);
            DateTime dataInicio = new DateTime(dataInicioAux.Year, dataInicioAux.Month, 01);

            int mesFinal = data.Month;            
            DateTime dataFim = new DateTime(this.ano, mesFinal, 01);
            //
            dataFim = Calendario.RetornaUltimoDiaCorridoMes(dataFim, 0);
            //            
            DateTime dataDia = cliente.DataDia.Value;

            //Ajusta datas pela data de inicio da cota e dataDia do cliente
            if (dataFim > dataDia) {
                dataFim = dataDia;
            }

            if (dataInicio < carteira.DataInicioCota.Value) {
                dataInicio = carteira.DataInicioCota.Value;
            }

            CalculoMedida calculoMedida = new CalculoMedida(idCliente);
            calculoMedida.SetIdIndice(carteira.IdIndiceBenchmark.Value);
            calculoMedida.SetBenchmarkPercentual(true);
            calculoMedida.SetBuscaCotaMaisProxima(true);
            this.listaRetornosMensais = calculoMedida.RetornaListaRetornosMensais(dataInicio, dataFim);

            #region Busca retornos mensais e anuais
            DateTime dataAnteriorAno = new DateTime(dataInicio.Year - 4, 01, 01); //Data 5 anos anteriores

            if (dataAnteriorAno < carteira.DataInicioCota.Value) {
                dataAnteriorAno = carteira.DataInicioCota.Value;
            }
            #endregion

            this.listaRetornosAnuais = calculoMedida.RetornaListaRetornosAnuais(dataAnteriorAno, dataFim);
            #endregion
        }

        /// <summary>
        /// Calculo de Despesas
        /// </summary>
        /// <returns></returns>
        private decimal CalculaDespesas(DateTime dataPosicao, int idCliente) {
            DateTime dataInicio = Calendario.RetornaUltimoDiaUtilAno(new DateTime(dataPosicao.Year - 1, 1, 1));
            DateTime dataFim = Calendario.RetornaUltimoDiaUtilAno(new DateTime(dataPosicao.Year, 1, 1));

            #region
            LiquidacaoQuery liquidacaoQuery = new LiquidacaoQuery("L");
            ClienteQuery clienteQuery = new ClienteQuery("C");

            liquidacaoQuery.InnerJoin(clienteQuery).On(clienteQuery.IdCliente == liquidacaoQuery.IdCliente);
            //
            liquidacaoQuery.Select(liquidacaoQuery.Valor.Sum());
            liquidacaoQuery.Where(liquidacaoQuery.IdCliente == idCliente,
                                  liquidacaoQuery.DataVencimento.Between(dataInicio, dataFim),
                                  liquidacaoQuery.Origem.In((int)OrigemLancamentoLiquidacao.Provisao.PagtoTaxaAdministracao,
                                                             (int)OrigemLancamentoLiquidacao.Provisao.PagtoTaxaGestao,
                                                             (int)OrigemLancamentoLiquidacao.Provisao.PagtoTaxaPerformance,
                                                             (int)OrigemLancamentoLiquidacao.Provisao.PagtoTaxaFiscalizacaoCVM,
                                                             (int)OrigemLancamentoLiquidacao.Provisao.PagtoProvisaoOutros
                                                            ));
            Liquidacao liquidacao = new Liquidacao();
            liquidacao.Load(liquidacaoQuery);
            #endregion

            decimal despesas = liquidacao.Valor.HasValue ? Math.Abs(liquidacao.Valor.Value) : 0;
            return despesas;
        }

        #region Rentabilidade Mensal/Anual

        #region Preenche os valores das tables

        private decimal? CalculaRentabilidade12Meses(List<CalculoMedida.EstatisticaRetornoMensal> rentabilidades) {

            // Lista com Retornos Mensais não Nulos
            List<decimal> rentabilidadesM = new List<decimal>();

            for (int i = 0; i < rentabilidades.Count; i++) {
                CalculoMedida.EstatisticaRetornoMensal c = rentabilidades[i];
                if (c.Retorno.HasValue) {
                    rentabilidadesM.Add(c.Retorno.Value);
                }
            }

            if (rentabilidadesM.Count == 0) {
                return null;
            }

            // Rentabilidade em fator
            List<decimal> rentabilidadesFator = new List<decimal>(rentabilidadesM.Count);
            for (int i = 0; i < rentabilidadesM.Count; i++) {
                decimal valor = (rentabilidadesM[i] / 100) + 1;
                rentabilidadesFator.Add(valor);
            }

            decimal? rentabilidadeAnual = null;
            for (int j = 0; j < rentabilidadesFator.Count; j++) {
                if (j == 0) {
                    rentabilidadeAnual = rentabilidadesFator[j];
                }
                else {
                    rentabilidadeAnual *= rentabilidadesFator[j];
                }
            }
            return (rentabilidadeAnual - 1) * 100;
        }

        /// <summary>
        /// Calcula Rentabilidade 12 meses do Indice baseado nas rentabilidades Mensais
        /// </summary>
        /// <param name="rentabilidades"></param>
        /// <returns>Null se não conseguiu calcular valor</returns>
        private decimal? CalculaRentabilidade12MesesIndice(List<CalculoMedida.EstatisticaRetornoMensal> rentabilidades) {

            // Lista com Retornos Mensais do Benchamark não Nulos
            List<decimal> rentabilidadesM = new List<decimal>();

            for (int i = 0; i < rentabilidades.Count; i++) {
                CalculoMedida.EstatisticaRetornoMensal c = rentabilidades[i];
                if (c.RetornoBenchmark.HasValue) {
                    rentabilidadesM.Add(c.RetornoBenchmark.Value);
                }
            }

            if (rentabilidadesM.Count == 0) {
                return null;
            }

            // Rentabilidade em fator
            List<decimal> rentabilidadesFator = new List<decimal>(rentabilidadesM.Count);
            for (int i = 0; i < rentabilidadesM.Count; i++) {
                decimal valor = (rentabilidadesM[i] / 100) + 1;
                rentabilidadesFator.Add(valor);
            }

            decimal? rentabilidadeAnualIndice = null;
            for (int j = 0; j < rentabilidadesFator.Count; j++) {
                if (j == 0) {
                    rentabilidadeAnualIndice = rentabilidadesFator[j];
                }
                else {
                    rentabilidadeAnualIndice *= rentabilidadesFator[j];
                }
            }
            return (rentabilidadeAnualIndice - 1) * 100;
        }

        #endregion
        #endregion

        #endregion

        /// <summary>
        /// Retorna uma Stream de Mémoria de um arquivo xml. A Stream permanece aberta
        /// </summary>
        /// <param name="dataPosicao"></param>
        /// <param name="idClientes">lista de idClientes</param>
        /// <param name="ms">Saida: Memory Stream do arquivo Lamina</param>
        /// <param name="nomeArquivo">Saida: Nome do Arquivo Lamina</param>        
        /// <returns></returns>
        public void ExportaLaminaXMLV1(DateTime dataPosicao, List<int> idClientes, out MemoryStream ms, out string nomeArquivo) {
            ms = new MemoryStream();
            nomeArquivo = "LaminaV1.xml";
            //            
            XML_LaminaV1.Cabecalho c = this.MontaCabecalho(dataPosicao);
            List<XML_LaminaV1.Corpo> conteudo = this.MontaConteudo(dataPosicao, idClientes);
            //
            XML_LaminaV1 arq = new XML_LaminaV1();
            arq.cabecalho = c;
            arq.corpo.AddRange(conteudo);
            //                     
            XmlSerializer x = new XmlSerializer(arq.GetType());

            XmlWriterSettings settings = new XmlWriterSettings();
            settings.Encoding = System.Text.Encoding.GetEncoding("WINDOWS-1252");
            settings.Indent = true;
            settings.IndentChars = "\t";
            settings.NewLineChars = Environment.NewLine;
            settings.ConformanceLevel = ConformanceLevel.Document;

            using (XmlWriter writer = XmlTextWriter.Create(ms, settings)) {
                x.Serialize(writer, arq);
            }
        }

        /// <summary>
        /// Monta o Cabeçalho do arquivo XML
        /// </summary>
        /// <param name="dataPosicao"></param>
        /// <returns></returns>
        private XML_LaminaV1.Cabecalho MontaCabecalho(DateTime dataPosicao) {
            XML_LaminaV1.Cabecalho c = new XML_LaminaV1.Cabecalho();
            //
            c.codigoDocumento = 24;
            c.versao = "1.0";
            c.dataCompetencia = dataPosicao;
            c.dataGeracaoArquivo = DateTime.Now;

            //
            return c;
        }

        /// <summary>
        /// Monta o conteudo do arquivo XML
        /// </summary>
        /// <param name="dataPosicao"></param>
        /// <param name="idClientes">Lista de IdClientes</param>
        /// <returns></returns>
        private List<XML_LaminaV1.Corpo> MontaConteudo(DateTime dataPosicao, List<int> idClientes) {
            List<XML_LaminaV1.Corpo> lista = new List<XML_LaminaV1.Corpo>();

            //
            #region Consulta Inicial
            PessoaQuery pessoaQuery = new PessoaQuery("P");
            ClienteQuery clienteQuery = new ClienteQuery("C");
            //CarteiraQuery carteiraQuery = new CarteiraQuery("A");

            //
            ClienteCollection clienteCollection = new ClienteCollection();
            //
            clienteQuery.Select(clienteQuery, pessoaQuery.Cpfcnpj);
            //
            clienteQuery.InnerJoin(pessoaQuery).On(pessoaQuery.IdPessoa == clienteQuery.IdCliente);
            //clienteQuery.InnerJoin(carteiraQuery).On(carteiraQuery.IdCarteira == clienteQuery.IdCliente);
            //
            //clienteQuery.Where(clienteQuery.TipoControle == (byte)TipoControleCliente.Completo &&
            //                   clienteQuery.IdTipo.In((int)TipoClienteFixo.Fundo) &&
            //                   clienteQuery.IdCliente.In(idClientes));
            ////
            clienteQuery.Where(clienteQuery.IdCliente.In(idClientes));
            //
            clienteQuery.OrderBy(pessoaQuery.Cpfcnpj.Descending);
            //
            clienteCollection.Load(clienteQuery);
            //
            #endregion

            // Carrega Lamina
            DateTime dataUltimoDiaMes = Calendario.RetornaUltimoDiaUtilMes(dataPosicao, 0);

            //
            for (int i = 0; i < clienteCollection.Count; i++) {
                int idCliente = clienteCollection[i].IdCliente.Value;
                //DateTime dataDia = clienteCollection[i].DataDia.Value;

                this.InitializeRentabilidades(idCliente, dataUltimoDiaMes);

                #region Lamina
                LaminaCollection infoCollection = new LaminaCollection();

                // Busca todas as informações menores que a data de referencia ordenado decrescente pela data
                infoCollection.Query
                     .Where(infoCollection.Query.InicioVigencia.LessThanOrEqual(dataUltimoDiaMes),
                            infoCollection.Query.IdCarteira == idCliente)
                     .OrderBy(infoCollection.Query.InicioVigencia.Descending);

                infoCollection.Query.Load();

                Lamina info = new Lamina();
                bool existeInfo = infoCollection.HasData;
                if (existeInfo) {
                    info = infoCollection[0];
                }
                #endregion

                Pessoa pessoa = new Pessoa();
                pessoa.LoadByPrimaryKey(clienteCollection[i].IdPessoa.Value);

                #region Conteudo arquivo XML
                //
                XML_LaminaV1.Corpo corpo = new XML_LaminaV1.Corpo();

                corpo.cnpj = pessoa.Cpfcnpj.Replace(".", "").Replace("-", "").Replace("/", "").Trim().PadLeft(14, '0');
                corpo.nomeFantasia = !String.IsNullOrEmpty(clienteCollection[i].Apelido) ? clienteCollection[i].Apelido.Trim() : "";
                corpo.enderecoEletronico = !String.IsNullOrEmpty(info.EnderecoEletronico) ? info.EnderecoEletronico.Trim() : "";
                corpo.descricaoPublicoAlvo = !String.IsNullOrEmpty(info.DescricaoPublicoAlvo) ? info.DescricaoPublicoAlvo.Trim() : "";
                corpo.RESTR_INV = ""; // TODO
                corpo.DESC_OBJET = !String.IsNullOrEmpty(info.ObjetivoFundo) ? info.ObjetivoFundo.Trim() : "";
                corpo.descricaoPoliticaInvestimento = !String.IsNullOrEmpty(info.DescricaoPoliticaInvestimento) ? info.DescricaoPoliticaInvestimento.Trim() : "";
                //
                // decimais
                corpo.ativo_ext = 0.00M;
                if (info.LimiteAplicacaoExterior.HasValue) {
                    corpo.ativo_ext = info.LimiteAplicacaoExterior.Value;
                }

                corpo.CRED_PRIV = 0.00M;
                if (info.LimiteCreditoPrivado.HasValue) {
                    corpo.CRED_PRIV = info.LimiteCreditoPrivado.Value;
                }

                corpo.ALAV = 0.00M;
                if (info.LimiteAlavancagem.HasValue) {
                    corpo.ALAV = info.LimiteAlavancagem.Value;
                }

                corpo.CONC_EMISSOR = 0.00M;
                if (info.LimiteConcentracaoEmissor.HasValue) {
                    corpo.CONC_EMISSOR = info.LimiteConcentracaoEmissor.Value;
                }
                //
                corpo.perdasSignificativas = !String.IsNullOrEmpty(info.RegulamentoPerdasPatrimoniais) ? "S" : "N";
                //
                corpo.PL_NEGAT = !String.IsNullOrEmpty(info.RegulamentoPatrimonioNegativo) ? "S" : "N";
                //
                Carteira carteira = new Carteira();
                carteira.LoadByPrimaryKey(idCliente);
                //
                corpo.INV_INICIAL = carteira.ValorMinimoInicial.Value;
                corpo.INV_ADIC = carteira.ValorMinimoAplicacao.Value;
                corpo.RESG_MIN = carteira.ValorMinimoResgate.Value;
                //
                DateTime horaResgate = new DateTime(1900, 1, 1, 0, 0, 0);
                corpo.HOR_APL_RESG = horaResgate;

                if (carteira.HorarioFimResgate.HasValue) {
                    corpo.HOR_APL_RESG = carteira.HorarioFimResgate.Value;
                }
                //
                corpo.VALOR_PERM = carteira.ValorMinimoInicial.Value;
                //

                // campo 19 - campo Opcional
                this.ControeCarencia(dataPosicao, idCliente, corpo, info);

                // campo 20 
                this.ControeConversao(dataPosicao, idCliente, corpo, info);

                // campo 21 -
                int diasLiquidacaoResgate = carteira.DiasLiquidacaoResgate.Value;
                corpo.PRAZO_PAGAM = carteira.DiasLiquidacaoResgate.Value.ToString();

                // campo 22
                this.ControeINFOR_TX_ADM(dataPosicao, idCliente, corpo, info);

                // campo 23 - Opcional
                this.ControeINFOR_TX_ENTR(dataPosicao, idCliente, corpo, info);

                // campo 24
                this.ControeINFOR_TX_SAIDA(dataPosicao, idCliente, corpo, info);

                #region TX_DESEMPENHO
                IndiceQuery indiceQuery = new IndiceQuery("I");
                TabelaTaxaPerformanceQuery tabelaTaxas = new TabelaTaxaPerformanceQuery("T");
                CarteiraQuery carteiraQuery = new CarteiraQuery("C");
                tabelaTaxas.Select(tabelaTaxas.IdTabela,
                                      tabelaTaxas.DataReferencia,
                                      tabelaTaxas.TaxaPerformance,
                                      tabelaTaxas.PercentualIndice,
                                      carteiraQuery.IdCarteira,
                                      carteiraQuery.Apelido,
                                      indiceQuery.Descricao);
                tabelaTaxas.InnerJoin(carteiraQuery).On(tabelaTaxas.IdCarteira == carteiraQuery.IdCarteira);
                tabelaTaxas.InnerJoin(indiceQuery).On(tabelaTaxas.IdIndice == indiceQuery.IdIndice);
                tabelaTaxas.Where(tabelaTaxas.IdCarteira == idCliente);

                TabelaTaxaPerformanceCollection tabelaTaxaPerformanceCollection = new TabelaTaxaPerformanceCollection();
                tabelaTaxaPerformanceCollection.Load(tabelaTaxas);

                corpo.tx_desempenho = "0";
                if (tabelaTaxaPerformanceCollection.HasData) {
                    decimal? taxaPerformance = tabelaTaxaPerformanceCollection[0].TaxaPerformance;
                    decimal? percentualIndicePerformance = tabelaTaxaPerformanceCollection[0].PercentualIndice;
                    string descricaoIndicie = tabelaTaxaPerformanceCollection[0].GetColumn(IndiceMetadata.ColumnNames.Descricao).ToString();
                    if (taxaPerformance.HasValue) {
                        corpo.tx_desempenho = taxaPerformance.Value.ToString("N2") + "%" + " do que exceder a " + percentualIndicePerformance.Value.ToString("N2") + "% do " + descricaoIndicie;
                    }
                }
                #endregion

                // campo 26 - * Campo não obrigatório para fundos com menos de um ano de funcionamento
                this.ControeINFOR_DESPESAS(dataPosicao, idCliente, corpo, info);

                // campo 27 - * Campo não obrigatório para fundos com menos de um ano de funcionamento
                this.ControeLISTA_COMP_CARTEIRA(dataPosicao, idCliente, corpo, info);

                // campo 28
                corpo.classificacaoRisco = info.Risco.HasValue ? info.Risco.Value : 0;

                // campo 29
                this.ControeINFOR_TAB_RENT_ANUAL(dataPosicao, idCliente, corpo, info);

                // campo 30
                this.ControeLISTA_TAB_RENT_ANUAL(dataPosicao, idCliente, corpo, info);

                // campo 31
                this.ControeLISTA_TAB_RENT_MENSAL(dataPosicao, idCliente, corpo, info);

                // campo 32 -  * Campo não obrigatório para fundos com menos de um ano de funcionamento
                corpo.FORM_CALC_RENT = !String.IsNullOrEmpty(info.CenariosApuracaoRentabilidade) ? info.CenariosApuracaoRentabilidade : "";

                // campo 33
                this.ControeTAB_VAR_DESEMP(dataPosicao, idCliente, corpo, info);

                // campo 34 - * Campo não obrigatório para fundos com menos de um ano de funcionamento
                this.ControeINFORM_EXEMPLO_COMP(dataPosicao, idCliente, corpo, info);

                // campo 35 - * Campo não obrigatório para fundos com menos de um ano de funcionamento
                this.ControeTAB_SIMUL_DESPESAS(dataPosicao, idCliente, corpo, info);

                // campo 36
                string telefone = !String.IsNullOrEmpty(info.Telefone) ? info.Telefone.Trim() : "";

                string resultString = null;
                try {
                    Regex regexObj = new Regex(@"[^\d]");
                    resultString = regexObj.Replace(telefone, "");
                }
                catch (ArgumentException ex) {
                }

                corpo.telefone = !String.IsNullOrEmpty(resultString) ? Convert.ToInt64(resultString) : 0;

                // campo 37
                corpo.reclamacoes = !String.IsNullOrEmpty(info.Reclamacoes) ? info.Reclamacoes.Trim() : "";

                lista.Add(corpo);
                //
                #endregion
            }
            //
            return lista;
        }

        /// <summary>
        /// Constroe o objeto Carencia
        /// </summary>
        /// <param name="dataPosicao"></param>
        /// <param name="idCliente"></param>
        /// <param name="corpo">Objeto representativo do Corpo do arquivo XML</param>
        private void ControeCarencia(DateTime dataPosicao, int idCliente, XML_LaminaV1.Corpo corpo, Lamina info) {

            #region Controe Carencia

            //XML_Lamina.CONVERSAO elem = new XML_Lamina.CONVERSAO();
            //
            //elem.ABERT_FECH = !String.IsNullOrEmpty(info.LocalFormaDivulgacao) ? info.LocalFormaDivulgacao.Trim() : "";
            //
            corpo.getCARENCIA.DIAS_CAR = 0;
            if (!String.IsNullOrEmpty(info.PrazoCarencia)) {
                corpo.getCARENCIA.DIAS_CAR = Convert.ToInt32(info.PrazoCarencia);
            }

            corpo.getCARENCIA.COND_CAR = "";

            #endregion
        }

        /// <summary>
        /// Constroe o objeto Conversao
        /// </summary>
        /// <param name="dataPosicao"></param>
        /// <param name="idCliente"></param>
        /// <param name="corpo">Objeto representativo do Corpo do arquivo XML</param>
        private void ControeConversao(DateTime dataPosicao, int idCliente, XML_LaminaV1.Corpo corpo, Lamina info) {

            #region Controe Conversao

            //XML_Lamina.CONVERSAO elem = new XML_Lamina.CONVERSAO();
            //
            //elem.ABERT_FECH = !String.IsNullOrEmpty(info.LocalFormaDivulgacao) ? info.LocalFormaDivulgacao.Trim() : "";
            //

            Carteira carteira = new Carteira();
            carteira.LoadByPrimaryKey(idCliente);

            int tipoCota = (int)carteira.TipoCota.Value;
            int diasConversaoAplic = carteira.DiasCotizacaoAplicacao.Value;
            int diasConversaoResgate = carteira.DiasCotizacaoResgate.Value;
            string contagem = carteira.ContagemDiasConversaoResgate == (int)ContagemDiasLiquidacaoResgate.DiasUteis ? "úteis" : "corridos";
            string cota = tipoCota == (int)TipoCotaFundo.Abertura ? "A" : "F";

            //
            corpo.getCONVERSAO.ABERT_FECH = cota;
            corpo.getCONVERSAO.DIA_APLIC = diasConversaoAplic;
            corpo.getCONVERSAO.ABERT_FECH_C = cota;
            corpo.getCONVERSAO.DIA_RESG = diasConversaoResgate;

            #endregion
        }

        /// <summary>
        /// Constroe o objeto INFOR_TX_ADM
        /// </summary>
        /// <param name="dataPosicao"></param>
        /// <param name="idCliente"></param>
        /// <param name="corpo">Objeto representativo do Corpo do arquivo XML</param>
        private void ControeINFOR_TX_ADM(DateTime dataPosicao, int idCliente, XML_LaminaV1.Corpo corpo, Lamina info) {

            CadastroTaxaAdministracaoQuery cadastroTaxaAdm = new CadastroTaxaAdministracaoQuery("D");
            TabelaTaxaAdministracaoQuery tabelaTaxaAdministracaoQuery = new TabelaTaxaAdministracaoQuery("T");
            CarteiraQuery carteiraQuery = new CarteiraQuery("C");

            tabelaTaxaAdministracaoQuery.Select(tabelaTaxaAdministracaoQuery.IdTabela, tabelaTaxaAdministracaoQuery.IdCarteira,
                                                tabelaTaxaAdministracaoQuery.DataReferencia, tabelaTaxaAdministracaoQuery.Taxa,
                                                tabelaTaxaAdministracaoQuery.ValorMaximo, tabelaTaxaAdministracaoQuery.ValorMinimo,
                                                tabelaTaxaAdministracaoQuery.TipoCalculo, carteiraQuery.Apelido);
            tabelaTaxaAdministracaoQuery.InnerJoin(carteiraQuery).On(tabelaTaxaAdministracaoQuery.IdCarteira == carteiraQuery.IdCarteira);
            tabelaTaxaAdministracaoQuery.InnerJoin(cadastroTaxaAdm).On(tabelaTaxaAdministracaoQuery.IdCadastro == cadastroTaxaAdm.IdCadastro);
            tabelaTaxaAdministracaoQuery.Where((tabelaTaxaAdministracaoQuery.IdCarteira == idCliente),
                                               (cadastroTaxaAdm.Descricao.ToLower().Equal("TAXA ADMINISTRAÇÃO")));;

            TabelaTaxaAdministracao tab = new TabelaTaxaAdministracao();

            if (tab.Load(tabelaTaxaAdministracaoQuery)) {
                decimal? taxa = tab.Taxa;

                int tipoCalculo = tab.TipoCalculo.Value;
                //
                corpo.getINFOR_TX_ADM.TIPO_TX_ADM = tipoCalculo == (int)TipoCalculoAdministracao.ValorFixo ? "V" : "F";

                if (tipoCalculo == (int)TipoCalculoAdministracao.PercentualPL) {
                    corpo.getINFOR_TX_ADM.tx_adm = taxa.HasValue ? taxa.Value : 0;
                }
                else {
                    corpo.getINFOR_TX_ADM.tx_min = tab.ValorMinimo.Value;
                    corpo.getINFOR_TX_ADM.tx_max = tab.ValorMaximo.Value;
                }

                corpo.getINFOR_TX_ADM.ESCLAREC = "";
            }
        }

        /// <summary>
        /// Constroe o objeto INFOR_TX_ENTR
        /// </summary>
        /// <param name="dataPosicao"></param>
        /// <param name="idCliente"></param>
        /// <param name="corpo">Objeto representativo do Corpo do arquivo XML</param>
        private void ControeINFOR_TX_ENTR(DateTime dataPosicao, int idCliente, XML_LaminaV1.Corpo corpo, Lamina info) {

            #region Controe INFOR_TX_ENTR

            corpo.getINFOR_TX_ENTR.TX_ENTR = !String.IsNullOrEmpty(info.TaxaEntrada) ? Convert.ToInt32(info.TaxaEntrada.Trim()) : 0;
            corpo.getINFOR_TX_ENTR.COND_ENTR = "";
            #endregion
        }

        /// <summary>
        /// Constroe o objeto INFOR_TX_SAIDA
        /// </summary>
        /// <param name="dataPosicao"></param>
        /// <param name="idCliente"></param>
        /// <param name="corpo">Objeto representativo do Corpo do arquivo XML</param>
        private void ControeINFOR_TX_SAIDA(DateTime dataPosicao, int idCliente, XML_LaminaV1.Corpo corpo, Lamina info) {

            Carteira carteira = new Carteira();
            carteira.LoadByPrimaryKey(idCliente);
            //
            int diasConversaoAplic = carteira.DiasCotizacaoAplicacao.Value;

            #region Controe INFOR_TX_SAIDA

            corpo.getINFOR_TX_SAIDA.DIAS_SAIDA = diasConversaoAplic;
            corpo.getINFOR_TX_SAIDA.TX_SAIDA = !String.IsNullOrEmpty(info.TaxaSaida) ? Convert.ToInt32(info.TaxaSaida.Trim()) : 0;
            corpo.getINFOR_TX_SAIDA.COND_SAIDA = "";
            #endregion
        }

        /// <summary>
        /// Constroe o objeto INFOR_DESPESAS
        /// </summary>
        /// <param name="dataPosicao"></param>
        /// <param name="idCliente"></param>
        /// <param name="corpo">Objeto representativo do Corpo do arquivo XML</param>
        private void ControeINFOR_DESPESAS(DateTime dataPosicao, int idCliente, XML_LaminaV1.Corpo corpo, Lamina info) {

            #region Liquidacao

            DateTime data2 = Calendario.RetornaUltimoDiaUtilMes(dataPosicao, 0);
            DateTime dataAux = data2.AddMonths(-12);
            DateTime data1 = Calendario.RetornaUltimoDiaUtilMes(dataAux, 0);
            //
            LiquidacaoQuery liquidacaoQuery = new LiquidacaoQuery("L");
            ClienteQuery clienteQuery = new ClienteQuery("C");

            liquidacaoQuery.InnerJoin(clienteQuery).On(clienteQuery.IdCliente == liquidacaoQuery.IdCliente);
            //
            liquidacaoQuery.Select(liquidacaoQuery.Valor.Sum());
            liquidacaoQuery.Where(liquidacaoQuery.IdCliente == idCliente,
                                  liquidacaoQuery.DataVencimento.Between(data1, data2),
                                  liquidacaoQuery.Situacao == (Int16)SituacaoLancamentoLiquidacao.Normal,
                                  liquidacaoQuery.Valor < 0,
                                  clienteQuery.IdTipo.In((int)TipoClienteFixo.Fundo));

            Liquidacao liquidacao = new Liquidacao();
            liquidacao.Load(liquidacaoQuery);
            #endregion

            decimal despesas = liquidacao.Valor.HasValue ? Math.Abs(liquidacao.Valor.Value) : 0;

            #region PL
            HistoricoCota historicoCota = new HistoricoCota();
            historicoCota.Query
                 .Select(historicoCota.Query.PLFechamento.Sum())
                 .Where(historicoCota.Query.IdCarteira == idCliente,
                        historicoCota.Query.Data.Between(data1, data2));

            historicoCota.Query.Load();
            decimal plFechamento = historicoCota.PLFechamento.HasValue ? historicoCota.PLFechamento.Value : 0;
            #endregion

            // Média das despesas por patrimonio do fundo. 
            decimal? media = null;
            if (plFechamento > 0) {
                media = despesas / plFechamento;
            }

            #region Controe INFOR_DESPESAS
            corpo.getINFOR_DESPESAS.despesas = media.HasValue ? media.Value : 0;
            corpo.getINFOR_DESPESAS.inic_periodo = data1;
            corpo.getINFOR_DESPESAS.fim_periodo = data2;
            corpo.getINFOR_DESPESAS.END_ELET_ADM = !String.IsNullOrEmpty(info.EnderecoEletronico) ? info.EnderecoEletronico.Trim() : "";
            #endregion
        }

        /// <summary>
        /// Constroe o objeto LISTA_COMP_CARTEIRA
        /// </summary>
        /// <param name="dataPosicao"></param>
        /// <param name="idCliente"></param>
        /// <param name="corpo">Objeto representativo do Corpo do arquivo XML</param>
        private void ControeLISTA_COMP_CARTEIRA(DateTime dataPosicao, int idCliente, XML_LaminaV1.Corpo corpo, Lamina info) {

            #region Calculos
            DateTime dataAux = Calendario.RetornaUltimoDiaUtilMes(dataPosicao);

            HistoricoCota historicoCota = new HistoricoCota();
            historicoCota.LoadByPrimaryKey(dataAux, idCliente);
            decimal plFechamento = historicoCota.PLFechamento.HasValue ? historicoCota.PLFechamento.Value : 0;

            decimal porcentagem1 = 0.0M;
            decimal porcentagem2 = 0.0M;
            decimal porcentagem3 = 0.0M;
            decimal porcentagem4 = 0.0M;
            decimal porcentagem5 = 0.0M;
            decimal porcentagem6 = 0.0M;
            decimal porcentagem7 = 0.0M;
            decimal porcentagem8 = 0.0M;
            decimal porcentagem9 = 0.0M;
            decimal porcentagem10 = 0.0M;
            decimal porcentagem11 = 0.0M;

            #region 1
            PosicaoRendaFixaHistoricoQuery posicaoRendaFixaQuery = new PosicaoRendaFixaHistoricoQuery("P");
            TituloRendaFixaQuery tituloRendaFixaQuery = new TituloRendaFixaQuery("T");
            PapelRendaFixaQuery papelRendaFixaQuery = new PapelRendaFixaQuery("A");

            posicaoRendaFixaQuery.Select(posicaoRendaFixaQuery.ValorMercado.Sum());
            //
            posicaoRendaFixaQuery.InnerJoin(tituloRendaFixaQuery).On(tituloRendaFixaQuery.IdTitulo == posicaoRendaFixaQuery.IdTitulo);
            posicaoRendaFixaQuery.InnerJoin(papelRendaFixaQuery).On(papelRendaFixaQuery.IdPapel == tituloRendaFixaQuery.IdPapel);
            //
            posicaoRendaFixaQuery.Where(posicaoRendaFixaQuery.IdCliente == idCliente,
                                        posicaoRendaFixaQuery.DataHistorico == dataAux,
                                        posicaoRendaFixaQuery.TipoOperacao.Equal((byte)TipoOperacaoTitulo.CompraFinal),
                //tituloRendaFixaQuery.CodigoCustodia.IsNotNull(),
                //tituloRendaFixaQuery.CodigoCustodia.NotEqual(""),
                                        papelRendaFixaQuery.TipoPapel.Equal((byte)TipoPapelTitulo.Publico));

            PosicaoRendaFixaHistoricoCollection coll = new PosicaoRendaFixaHistoricoCollection();
            coll.Load(posicaoRendaFixaQuery);

            decimal valormercado = 0.0M;
            if (coll.HasData) {
                if (coll[0].ValorMercado.HasValue) {
                    valormercado = coll[0].ValorMercado.Value;
                }
            }

            if (plFechamento != 0) {
                porcentagem1 = valormercado / plFechamento;
            }

            #endregion

            #region 2
            posicaoRendaFixaQuery = new PosicaoRendaFixaHistoricoQuery("O");
            tituloRendaFixaQuery = new TituloRendaFixaQuery("T");
            PapelRendaFixaQuery papelRendaFixa = new PapelRendaFixaQuery("P");

            posicaoRendaFixaQuery.Select(posicaoRendaFixaQuery.ValorMercado.Sum());
            posicaoRendaFixaQuery.InnerJoin(tituloRendaFixaQuery).On(posicaoRendaFixaQuery.IdTitulo == tituloRendaFixaQuery.IdTitulo);
            posicaoRendaFixaQuery.InnerJoin(papelRendaFixa).On(tituloRendaFixaQuery.IdPapel == papelRendaFixa.IdPapel);
            posicaoRendaFixaQuery.Where(posicaoRendaFixaQuery.IdCliente == idCliente,
                                         posicaoRendaFixaQuery.DataHistorico == dataAux,
                                         posicaoRendaFixaQuery.TipoOperacao.Equal((byte)TipoOperacaoTitulo.CompraRevenda),
                                         papelRendaFixa.TipoPapel.Equal((byte)TipoPapelTitulo.Publico)
                                        );

            PosicaoRendaFixaHistoricoCollection oper = new PosicaoRendaFixaHistoricoCollection();

            oper.Load(posicaoRendaFixaQuery);

            decimal valor2 = 0.0M;
            if (oper.HasData) {
                if (oper[0].ValorMercado.HasValue) {
                    valor2 = oper[0].ValorMercado.Value;
                }
            }

            if (plFechamento != 0) {
                porcentagem2 = valor2 / plFechamento;
            }

            #endregion

            #region 3
            posicaoRendaFixaQuery = new PosicaoRendaFixaHistoricoQuery("O");
            tituloRendaFixaQuery = new TituloRendaFixaQuery("T");
            papelRendaFixa = new PapelRendaFixaQuery("P");

            posicaoRendaFixaQuery.Select(posicaoRendaFixaQuery.ValorMercado.Sum());
            posicaoRendaFixaQuery.InnerJoin(tituloRendaFixaQuery).On(posicaoRendaFixaQuery.IdTitulo == tituloRendaFixaQuery.IdTitulo);
            posicaoRendaFixaQuery.InnerJoin(papelRendaFixa).On(tituloRendaFixaQuery.IdPapel == papelRendaFixa.IdPapel);
            posicaoRendaFixaQuery.Where(posicaoRendaFixaQuery.IdCliente == idCliente,
                                         posicaoRendaFixaQuery.DataHistorico == dataAux,
                                         posicaoRendaFixaQuery.TipoOperacao.Equal((byte)TipoOperacaoTitulo.CompraRevenda),
                                         papelRendaFixa.TipoPapel.Equal((byte)TipoPapelTitulo.Privado)
                                        );

            oper = new PosicaoRendaFixaHistoricoCollection();
            oper.Load(posicaoRendaFixaQuery);

            decimal valor3 = 0.0M;
            if (oper.HasData) {
                if (oper[0].ValorMercado.HasValue) {
                    valor3 = oper[0].ValorMercado.Value;
                }
            }

            if (plFechamento != 0) {
                porcentagem3 = valor3 / plFechamento;
            }
            #endregion

            #region 4
            PosicaoBolsaHistoricoQuery posicaoBolsaQuery = new PosicaoBolsaHistoricoQuery("P");
            ClienteQuery clienteQuery = new ClienteQuery("L");
            //
            posicaoBolsaQuery.Select(posicaoBolsaQuery.ValorMercado.Sum());
            posicaoBolsaQuery.InnerJoin(clienteQuery).On(clienteQuery.IdCliente == posicaoBolsaQuery.IdCliente);

            posicaoBolsaQuery.Where(posicaoBolsaQuery.IdCliente == idCliente,
                                    posicaoBolsaQuery.DataHistorico == dataAux,
                                    clienteQuery.IdMoeda == (int)ListaMoedaFixo.Real);

            PosicaoBolsaHistorico p = new PosicaoBolsaHistorico();
            p.Load(posicaoBolsaQuery);

            decimal valor4 = 0.0M;
            if (p.ValorMercado.HasValue) {
                valor4 = p.ValorMercado.Value;
            }
            if (plFechamento != 0) {
                porcentagem4 = valor4 / plFechamento;
            }
            #endregion

            #region 5
            posicaoRendaFixaQuery = new PosicaoRendaFixaHistoricoQuery("P");
            tituloRendaFixaQuery = new TituloRendaFixaQuery("T");
            papelRendaFixaQuery = new PapelRendaFixaQuery("A");

            posicaoRendaFixaQuery.Select(posicaoRendaFixaQuery.ValorMercado.Sum());
            //
            posicaoRendaFixaQuery.InnerJoin(tituloRendaFixaQuery).On(tituloRendaFixaQuery.IdTitulo == posicaoRendaFixaQuery.IdTitulo);
            posicaoRendaFixaQuery.InnerJoin(papelRendaFixaQuery).On(papelRendaFixaQuery.IdPapel == tituloRendaFixaQuery.IdPapel);
            //
            posicaoRendaFixaQuery.Where(posicaoRendaFixaQuery.IdCliente == idCliente,
                                        posicaoRendaFixaQuery.DataHistorico == dataAux,
                                        posicaoRendaFixaQuery.TipoOperacao.Equal((byte)TipoOperacaoTitulo.CompraFinal),
                //tituloRendaFixaQuery.CodigoCustodia.IsNotNull(),
                //tituloRendaFixaQuery.CodigoCustodia.NotEqual(""),
                                        papelRendaFixaQuery.Classe.In((int)ClasseRendaFixa.CDB,
                //(int)ClasseRendaFixa.rdb,
                                                                        (int)ClasseRendaFixa.LF,
                                                                        (int)ClasseRendaFixa.DPGE,
                                                                        (int)ClasseRendaFixa.CCB,
                                                                        (int)ClasseRendaFixa.LCA,
                                                                        (int)ClasseRendaFixa.LCI)
                                        );

            coll = new PosicaoRendaFixaHistoricoCollection();
            coll.Load(posicaoRendaFixaQuery);

            valormercado = 0.0M;
            if (coll.HasData) {
                if (coll[0].ValorMercado.HasValue) {
                    valormercado = coll[0].ValorMercado.Value;
                }
            }

            #region SaldoCaixa
            SaldoCaixaCollection saldoCaixaCollection = new SaldoCaixaCollection();
            //
            SaldoCaixaQuery saldoCaixaQuery = new SaldoCaixaQuery("S");
            ContaCorrenteQuery contaCorrenteQuery = new ContaCorrenteQuery("C");
            MoedaQuery moedaQuery = new MoedaQuery("M");

            saldoCaixaQuery.Select(saldoCaixaQuery.SaldoFechamento.Sum());
            saldoCaixaQuery.InnerJoin(contaCorrenteQuery).On(saldoCaixaQuery.IdConta == contaCorrenteQuery.IdConta);
            saldoCaixaQuery.InnerJoin(moedaQuery).On(contaCorrenteQuery.IdMoeda == moedaQuery.IdMoeda);

            saldoCaixaQuery.Where(saldoCaixaQuery.IdCliente == idCliente,
                                  saldoCaixaQuery.Data == dataAux);

            saldoCaixaCollection.Load(saldoCaixaQuery);
            //

            decimal porcentagemSaldoCaixa = 0;
            if (saldoCaixaCollection.HasData) {
                if (saldoCaixaCollection[0].SaldoFechamento.HasValue) {
                    if (plFechamento != 0) {
                        porcentagemSaldoCaixa = saldoCaixaCollection[0].SaldoFechamento.Value / plFechamento;
                    }
                }
            }
            #endregion

            #region ValoresLiquidar
            LiquidacaoHistoricoCollection l = new LiquidacaoHistoricoCollection();

            l.Query.Select(l.Query.Descricao,
                           l.Query.IdConta,
                           l.Query.DataLancamento,
                           l.Query.DataVencimento,
                           l.Query.Valor.Sum())
                     .Where(l.Query.IdCliente == idCliente &&
                            l.Query.DataLancamento.LessThanOrEqual(dataAux) &
                            (
                                (l.Query.Situacao.In((byte)SituacaoLancamentoLiquidacao.Normal, (byte)SituacaoLancamentoLiquidacao.Compensacao) &
                                    l.Query.DataVencimento.GreaterThan(dataAux)) |
                                (l.Query.Situacao.In((byte)SituacaoLancamentoLiquidacao.Compensacao) &
                                    l.Query.DataVencimento.GreaterThanOrEqual(dataAux) &
                                    l.Query.Origem.Equal(OrigemLancamentoLiquidacao.Fundo.AplicacaoConverter))
                            ) &
                            l.Query.DataHistorico == dataAux &
                            l.Query.Valor != 0)

                     .GroupBy(l.Query.Descricao,
                              l.Query.IdConta,
                              l.Query.DataLancamento,
                              l.Query.DataVencimento);

            l.Query.Load();
            //
            l.Filter = "" + LiquidacaoMetadata.ColumnNames.Valor + " <> 0";

            decimal totalLiquidar = 0;
            decimal porcentagemLiquidacao = 0;
            //
            foreach (LiquidacaoHistorico liquidacao in l) {
                totalLiquidar += liquidacao.Valor.Value;
            }

            if (plFechamento != 0) {
                porcentagemLiquidacao = totalLiquidar / plFechamento;
            }

            #endregion

            if (plFechamento != 0) {
                porcentagem5 = valormercado / plFechamento;
                porcentagem5 += porcentagemSaldoCaixa;
                porcentagem5 += porcentagemLiquidacao;
            }
            #endregion

            #region 6

            //clienteQuery = new ClienteQuery("L");
            PosicaoFundoHistoricoQuery posicaoFundoQuery = new PosicaoFundoHistoricoQuery("P");
            //
            PosicaoFundoHistoricoCollection pf = new PosicaoFundoHistoricoCollection();
            posicaoFundoQuery.Select(posicaoFundoQuery.ValorBruto.Sum());
            posicaoFundoQuery.Where(posicaoFundoQuery.IdCliente == idCliente,
                                    posicaoFundoQuery.DataHistorico == dataAux);

            pf.Load(posicaoFundoQuery);

            decimal valor6 = 0.0M;
            if (pf.HasData) {
                if (pf[0].ValorBruto.HasValue) {
                    valor6 = pf[0].ValorBruto.Value;
                }
            }
            if (plFechamento != 0) {
                porcentagem6 = valor6 / plFechamento;
            }
            #endregion

            #region 7
            posicaoBolsaQuery = new PosicaoBolsaHistoricoQuery("P");
            //
            posicaoBolsaQuery.Select(posicaoBolsaQuery.ValorMercado.Sum());
            posicaoBolsaQuery.Where(posicaoBolsaQuery.IdCliente == idCliente,
                                    posicaoBolsaQuery.DataHistorico == dataAux,
                                    posicaoBolsaQuery.TipoMercado.In(TipoMercadoBolsa.Imobiliario));

            p = new PosicaoBolsaHistorico();
            p.Load(posicaoBolsaQuery);

            decimal valor7 = 0.0M;
            if (p.ValorMercado.HasValue) {
                valor7 = p.ValorMercado.Value;
            }

            if (plFechamento != 0) {
                porcentagem7 = valor7 / plFechamento;
            }
            #endregion

            #region 8
            posicaoRendaFixaQuery = new PosicaoRendaFixaHistoricoQuery("P");
            tituloRendaFixaQuery = new TituloRendaFixaQuery("T");
            papelRendaFixaQuery = new PapelRendaFixaQuery("A");
            clienteQuery = new ClienteQuery("L");

            posicaoRendaFixaQuery.Select(posicaoRendaFixaQuery.ValorMercado.Sum());
            //            
            posicaoRendaFixaQuery.InnerJoin(clienteQuery).On(clienteQuery.IdCliente == posicaoRendaFixaQuery.IdCliente);
            posicaoRendaFixaQuery.InnerJoin(tituloRendaFixaQuery).On(tituloRendaFixaQuery.IdTitulo == posicaoRendaFixaQuery.IdTitulo);
            posicaoRendaFixaQuery.InnerJoin(papelRendaFixaQuery).On(papelRendaFixaQuery.IdPapel == tituloRendaFixaQuery.IdPapel);
            //
            posicaoRendaFixaQuery.Where(posicaoRendaFixaQuery.IdCliente == idCliente,
                                        posicaoRendaFixaQuery.DataHistorico == dataAux,
                                        posicaoRendaFixaQuery.TipoOperacao.Equal((byte)TipoOperacaoTitulo.CompraFinal),
                //tituloRendaFixaQuery.CodigoCustodia.IsNotNull(),
                //tituloRendaFixaQuery.CodigoCustodia.NotEqual(""),
                                        papelRendaFixaQuery.TipoPapel.Equal((byte)TipoPapelTitulo.Privado),
                                        clienteQuery.IdMoeda == (int)ListaMoedaFixo.Real,
                                        papelRendaFixaQuery.Classe.NotIn((int)ClasseRendaFixa.CDB,
                //(int)ClasseRendaFixa.rdb,
                                                                           (int)ClasseRendaFixa.LF,
                                                                           (int)ClasseRendaFixa.DPGE,
                                                                           (int)ClasseRendaFixa.CCB,
                                                                           (int)ClasseRendaFixa.LCA,
                                                                           (int)ClasseRendaFixa.LCI)
                                        );

            coll = new PosicaoRendaFixaHistoricoCollection();
            coll.Load(posicaoRendaFixaQuery);

            valormercado = 0.0M;
            if (coll.HasData) {
                if (coll[0].ValorMercado.HasValue) {
                    valormercado = coll[0].ValorMercado.Value;
                }
            }

            if (plFechamento != 0) {
                porcentagem8 = valormercado / plFechamento;
            }
            #endregion

            #region 9
            #region Bolsa
            PosicaoBolsaHistorico posicaoBolsa = new PosicaoBolsaHistorico();
            posicaoBolsa.Query.Select(posicaoBolsa.Query.ValorMercado.Sum());
            posicaoBolsa.Query.Where(posicaoBolsa.Query.IdCliente == idCliente,
                                     posicaoBolsa.Query.DataHistorico == dataAux,
                                     posicaoBolsa.Query.TipoMercado.In(TipoMercadoBolsa.OpcaoCompra, TipoMercadoBolsa.OpcaoVenda));
            posicaoBolsa.Query.Load();

            decimal valor = 0.0M; ;
            if (posicaoBolsa.ValorMercado.HasValue) {
                valor = posicaoBolsa.ValorMercado.Value;
            }
            #endregion

            #region BMF
            PosicaoBMFHistorico posicaoBMF = new PosicaoBMFHistorico();
            posicaoBMF.Query.Select(posicaoBMF.Query.ValorMercado.Sum());
            posicaoBMF.Query.Where(posicaoBMF.Query.IdCliente == idCliente,
                                   posicaoBMF.Query.DataHistorico == dataAux);
            posicaoBMF.Query.Load();

            if (posicaoBMF.ValorMercado.HasValue) {
                valor += posicaoBMF.ValorMercado.Value;
            }
            #endregion

            #region Termo
            PosicaoTermoBolsaHistorico posicaoTermoBolsa = new PosicaoTermoBolsaHistorico();

            posicaoTermoBolsa.Query.Select(posicaoTermoBolsa.Query.ValorMercado.Sum());
            posicaoTermoBolsa.Query.Where(posicaoTermoBolsa.Query.IdCliente == idCliente,
                                          posicaoTermoBolsa.Query.DataHistorico == dataAux);
            posicaoTermoBolsa.Query.Load();

            if (posicaoTermoBolsa.ValorMercado.HasValue) {
                valor += posicaoTermoBolsa.ValorMercado.Value;
            }
            #endregion

            #region Swap
            PosicaoSwapHistorico posicaoSwap = new PosicaoSwapHistorico();
            posicaoSwap.Query.Select(posicaoSwap.Query.Saldo.Sum());
            posicaoSwap.Query.Where(posicaoSwap.Query.IdCliente == idCliente,
                                    posicaoSwap.Query.DataHistorico == dataAux);
            posicaoSwap.Query.Load();

            if (posicaoSwap.Saldo.HasValue) {
                valor += posicaoSwap.Saldo.Value;
            }
            #endregion

            if (plFechamento != 0) {
                porcentagem9 = valor / plFechamento;
            }
            #endregion

            #region 10

            #region Bolsa
            posicaoBolsaQuery = new PosicaoBolsaHistoricoQuery("P");
            clienteQuery = new ClienteQuery("L");
            //
            posicaoBolsaQuery.Select(posicaoBolsaQuery.ValorMercado.Sum());
            posicaoBolsaQuery.InnerJoin(clienteQuery).On(clienteQuery.IdCliente == posicaoBolsaQuery.IdCliente);

            posicaoBolsaQuery.Where(posicaoBolsaQuery.IdCliente == idCliente,
                                    posicaoBolsaQuery.DataHistorico == dataAux,
                                    clienteQuery.IdMoeda.NotIn((int)ListaMoedaFixo.Real));

            p = new PosicaoBolsaHistorico();
            p.Load(posicaoBolsaQuery);

            decimal valor10 = 0.0M;
            if (p.ValorMercado.HasValue) {
                valor10 = p.ValorMercado.Value;
            }
            #endregion

            #region RF
            posicaoRendaFixaQuery = new PosicaoRendaFixaHistoricoQuery("P");
            clienteQuery = new ClienteQuery("L");
            //
            posicaoRendaFixaQuery.Select(posicaoRendaFixaQuery.ValorMercado.Sum());
            posicaoRendaFixaQuery.InnerJoin(clienteQuery).On(clienteQuery.IdCliente == posicaoRendaFixaQuery.IdCliente);

            posicaoRendaFixaQuery.Where(posicaoRendaFixaQuery.IdCliente == idCliente,
                                        posicaoRendaFixaQuery.DataHistorico == dataAux,
                                        clienteQuery.IdMoeda.NotIn((int)ListaMoedaFixo.Real));

            PosicaoRendaFixaHistorico prf = new PosicaoRendaFixaHistorico();
            prf.Load(posicaoRendaFixaQuery);

            if (prf.ValorMercado.HasValue) {
                valor10 += prf.ValorMercado.Value;
            }
            #endregion

            if (plFechamento != 0) {
                porcentagem10 = valor10 / plFechamento;
            }

            #endregion

            #region 11
            porcentagem11 = 0;
            #endregion

            // Dicionario com as descrições e porcentagens de patrimônio
            Dictionary<string, decimal> porcentagens = new Dictionary<string, decimal>();
            porcentagens.Add("Títulos Públicos federais", porcentagem1);
            porcentagens.Add("Operacões compromissadas lastreadas em títulos públicos federais", porcentagem2);
            porcentagens.Add("Operacões compromissadas lastreadas em títulos privados", porcentagem3);
            porcentagens.Add("Acões", porcentagem4);
            porcentagens.Add("Depósito a prazo e outros títulos de instituições financeiras", porcentagem5);
            porcentagens.Add("Cotas de Fundos de Investimento 555", porcentagem6);
            porcentagens.Add("Outras Cotas de Fundos de Investimento", porcentagem7);
            porcentagens.Add("Títulos de crédito privado", porcentagem8);
            porcentagens.Add("Derivativos", porcentagem9);
            porcentagens.Add("Investimento no exterior", porcentagem10);
            porcentagens.Add("Outras Aplicações", porcentagem11);

            // pegar as posições [10-9-8-7-6] do dicionario ordenado
            Dictionary<string, decimal> porcentagensOrdenadas = this.SortDictionaryByValue(porcentagens);

            int j = -1;
            List<string> descricao = new List<string>();
            List<decimal> valores = new List<decimal>();
            foreach (KeyValuePair<string, decimal> pair in porcentagensOrdenadas) {
                j++;
                if (j <= 5) {
                    continue;
                }
                else {
                    descricao.Add(pair.Key);
                    valores.Add(pair.Value);
                }
            }
            //
            decimal v4 = valores[4] * 100;
            decimal v3 = valores[3] * 100;
            decimal v2 = valores[2] * 100;
            decimal v1 = valores[1] * 100;
            decimal v0 = valores[0] * 100;
            //

            #endregion

            #region Controe LISTA_COMP_CARTEIRA

            HistoricoCota h1 = new HistoricoCota();
            DateTime dataAux1 = Calendario.RetornaUltimoDiaUtilMes(dataPosicao);
            //
            decimal pl = 0.00M;
            try {
                h1.BuscaValorPatrimonioDia(idCliente, dataAux1);
                pl = h1.PLFechamento.Value;
            }
            catch (HistoricoCotaNaoCadastradoException e1) {
                pl = 0.00M;
            }

            corpo.getLISTA_COMP_CARTEIRA.pl_fundo = pl;

            #region 4
            XML_LaminaV1.COMP_CARTEIRA elem = new XML_LaminaV1.COMP_CARTEIRA();

            elem.ESPECIE_ATIVO = descricao[4];
            elem.perc_pl = v4;

            corpo.getLISTA_COMP_CARTEIRA.getCOMP_CARTEIRA.Add(elem);
            #endregion

            #region 3
            elem = new XML_LaminaV1.COMP_CARTEIRA();

            elem.ESPECIE_ATIVO = descricao[3];
            elem.perc_pl = v3;

            corpo.getLISTA_COMP_CARTEIRA.getCOMP_CARTEIRA.Add(elem);
            #endregion

            #region 2
            elem = new XML_LaminaV1.COMP_CARTEIRA();

            elem.ESPECIE_ATIVO = descricao[2];
            elem.perc_pl = v2;

            corpo.getLISTA_COMP_CARTEIRA.getCOMP_CARTEIRA.Add(elem);
            #endregion

            #region 1
            elem = new XML_LaminaV1.COMP_CARTEIRA();

            elem.ESPECIE_ATIVO = descricao[1];
            elem.perc_pl = v1;

            corpo.getLISTA_COMP_CARTEIRA.getCOMP_CARTEIRA.Add(elem);
            #endregion

            #region 0
            elem = new XML_LaminaV1.COMP_CARTEIRA();

            elem.ESPECIE_ATIVO = descricao[0];
            elem.perc_pl = v0;

            corpo.getLISTA_COMP_CARTEIRA.getCOMP_CARTEIRA.Add(elem);
            #endregion
            #endregion
        }

        /// <summary>
        /// Constroe o objeto INFOR_TAB_RENT_ANUAL
        /// </summary>
        /// <param name="dataPosicao"></param>
        /// <param name="idCliente"></param>
        /// <param name="corpo">Objeto representativo do Corpo do arquivo XML</param>
        private void ControeINFOR_TAB_RENT_ANUAL(DateTime dataPosicao, int idCliente, XML_LaminaV1.Corpo corpo, Lamina info) {
            //            
            Carteira carteira = new Carteira();
            carteira.LoadByPrimaryKey(idCliente);

            string descricaoIndice = carteira.UpToIndiceByIdIndiceBenchmark.Descricao.ToString();

            CalculoMedida.EstatisticaRetornoAnual ano0 = this.listaRetornosAnuais.Find(delegate(CalculoMedida.EstatisticaRetornoAnual a) { return a.Data.Year == this.ultimos_5_Anos[4]; });
            CalculoMedida.EstatisticaRetornoAnual ano1 = this.listaRetornosAnuais.Find(delegate(CalculoMedida.EstatisticaRetornoAnual a) { return a.Data.Year == this.ultimos_5_Anos[3]; });
            CalculoMedida.EstatisticaRetornoAnual ano2 = this.listaRetornosAnuais.Find(delegate(CalculoMedida.EstatisticaRetornoAnual a) { return a.Data.Year == this.ultimos_5_Anos[2]; });
            CalculoMedida.EstatisticaRetornoAnual ano3 = this.listaRetornosAnuais.Find(delegate(CalculoMedida.EstatisticaRetornoAnual a) { return a.Data.Year == this.ultimos_5_Anos[1]; });
            CalculoMedida.EstatisticaRetornoAnual ano4 = this.listaRetornosAnuais.Find(delegate(CalculoMedida.EstatisticaRetornoAnual a) { return a.Data.Year == this.ultimos_5_Anos[0]; });

            decimal p1 = 1;
            decimal p2 = 1;
            decimal p3 = 1;
            decimal p4 = 1;
            decimal p5 = 1;
            //
            decimal benchmark1 = 1;
            decimal benchmark2 = 1;
            decimal benchmark3 = 1;
            decimal benchmark4 = 1;
            decimal benchmark5 = 1;
            //
            #region ano0
            if (ano0 != null && ano0.Retorno.HasValue) {
                p1 = (ano0.Retorno.Value / 100M) + 1M;
            }
            #endregion

            #region ano1
            if (ano1 != null && ano1.Retorno.HasValue) {
                p2 = (ano1.Retorno.Value / 100M) + 1M;
            }
            #endregion

            #region ano2
            if (ano2 != null && ano2.Retorno.HasValue) {
                p3 = (ano2.Retorno.Value / 100M) + 1M;
            }
            #endregion

            #region ano3
            if (ano3 != null && ano3.Retorno.HasValue) {
                p4 = (ano3.Retorno.Value / 100M) + 1M;
            }
            #endregion

            #region ano4
            if (ano4 != null && ano4.Retorno.HasValue) {
                p5 = (ano4.Retorno.Value / 100M) + 1M;
            }
            #endregion

            decimal? fatorAcumulado = ((p1 * p2 * p3 * p4 * p5) - 1M) * 100M;

            #region benchmark0
            if (ano0 != null && ano0.RetornoBenchmark.HasValue) {
                benchmark1 = (ano0.RetornoBenchmark.Value / 100M) + 1M;
            }
            #endregion

            #region benchmark1
            if (ano1 != null && ano1.RetornoBenchmark.HasValue) {
                benchmark2 = (ano1.RetornoBenchmark.Value / 100M) + 1M;
            }
            #endregion

            #region benchmark2
            if (ano2 != null && ano2.RetornoBenchmark.HasValue) {
                benchmark3 = (ano2.RetornoBenchmark.Value / 100M) + 1M;
            }
            #endregion

            #region benchmark3
            if (ano3 != null && ano3.RetornoBenchmark.HasValue) {
                benchmark4 = (ano3.RetornoBenchmark.Value / 100M) + 1M;
            }
            #endregion

            #region benchmark4
            if (ano4 != null && ano4.RetornoBenchmark.HasValue) {
                benchmark5 = (ano4.RetornoBenchmark.Value / 100M) + 1M;
            }
            #endregion

            decimal? indiceAcumulado = ((benchmark1 * benchmark2 * benchmark3 * benchmark4 * benchmark5) - 1M) * 100M;

            // Campo não obrigatório para fundos com menos de um ano de funcionamento
            corpo.getINFOR_TAB_RENT_ANUAL.rent_5anos = 0;
            if (fatorAcumulado.HasValue) {
                corpo.getINFOR_TAB_RENT_ANUAL.rent_5anos = fatorAcumulado.Value;
            }

            corpo.getINFOR_TAB_RENT_ANUAL.IND_REFER = descricaoIndice;

            corpo.getINFOR_TAB_RENT_ANUAL.rent_ind_ref = 0;
            if (indiceAcumulado.HasValue) {
                corpo.getINFOR_TAB_RENT_ANUAL.rent_ind_ref = indiceAcumulado.Value;
            }

            corpo.getINFOR_TAB_RENT_ANUAL.ANOS_ABAIXO = 0; // TODO                                 
            corpo.getINFOR_TAB_RENT_ANUAL.inic_funcio = carteira.DataInicioCota.Value;

            int anoInicio = carteira.DataInicioCota.Value.Year;

            //for (int i = 0; i < ultimos_5_Anos.Count; i++) {
            //    if (ultimos_5_Anos[i] < anoInicio) {
            //        XML_Lamina.ANTES_INIC2 elem = new XML_Lamina.ANTES_INIC2();
            //        elem.ANO = ultimos_5_Anos[i];
            //        //
            //        corpo.getINFOR_TAB_RENT_ANUAL.getANTES_INIC2.Add(elem);
            //    }
            //}
            List<int?> anos = new List<int?>(5);
            anos.AddRange(new int?[] { null, null, null, null, null });
            for (int i = 0; i < ultimos_5_Anos.Count; i++) {
                if (ultimos_5_Anos[i] < anoInicio) {
                    anos[i] = ultimos_5_Anos[i];
                }
            }
            XML_LaminaV1.ANTES_INIC2 elem = new XML_LaminaV1.ANTES_INIC2();
            elem.ANO1 = anos[0];
            elem.ANO2 = anos[1];
            elem.ANO3 = anos[2];
            elem.ANO4 = anos[3];
            elem.ANO5 = anos[4];

            if (anos[0] != null || anos[1] != null || anos[2] != null || anos[3] != null || anos[4] != null) {
                corpo.getINFOR_TAB_RENT_ANUAL.getANTES_INIC2.Add(elem);
            }
        }

        /// <summary>
        /// Constroe o objeto LISTA_TAB_RENT_ANUAL
        /// </summary>
        /// <param name="dataPosicao"></param>
        /// <param name="idCliente"></param>
        /// <param name="corpo">Objeto representativo do Corpo do arquivo XML</param>
        private void ControeLISTA_TAB_RENT_ANUAL(DateTime dataPosicao, int idCliente, XML_LaminaV1.Corpo corpo, Lamina info) {

            Carteira carteira = new Carteira();
            carteira.LoadByPrimaryKey(idCliente);

            #region Preenche Calculos
            //
            string descricaoIndice = carteira.UpToIndiceByIdIndiceBenchmark.Descricao.ToString();
            //        
            CalculoMedida.EstatisticaRetornoAnual ano0 = this.listaRetornosAnuais.Find(delegate(CalculoMedida.EstatisticaRetornoAnual a) { return a.Data.Year == this.ultimos_5_Anos[4]; });
            CalculoMedida.EstatisticaRetornoAnual ano1 = this.listaRetornosAnuais.Find(delegate(CalculoMedida.EstatisticaRetornoAnual a) { return a.Data.Year == this.ultimos_5_Anos[3]; });
            CalculoMedida.EstatisticaRetornoAnual ano2 = this.listaRetornosAnuais.Find(delegate(CalculoMedida.EstatisticaRetornoAnual a) { return a.Data.Year == this.ultimos_5_Anos[2]; });
            CalculoMedida.EstatisticaRetornoAnual ano3 = this.listaRetornosAnuais.Find(delegate(CalculoMedida.EstatisticaRetornoAnual a) { return a.Data.Year == this.ultimos_5_Anos[1]; });
            CalculoMedida.EstatisticaRetornoAnual ano4 = this.listaRetornosAnuais.Find(delegate(CalculoMedida.EstatisticaRetornoAnual a) { return a.Data.Year == this.ultimos_5_Anos[0]; });

            #region Ano0
            if (ano0 != null) {
                XML_LaminaV1.TAB_RENT_ANUAL elem = new XML_LaminaV1.TAB_RENT_ANUAL();

                elem.ANO_RENT = this.ultimos_5_Anos[4];
                if (ano0.Retorno.HasValue) {
                    elem.rent_liq = ano0.Retorno.Value;
                }
                if (ano0.RetornoBenchmark.HasValue) {
                    elem.var_ind_ref = ano0.RetornoBenchmark.Value;
                }
                if (ano0.RetornoDiferencial.HasValue) {
                    elem.rent_perc_ind_ref = ano0.RetornoDiferencial.Value;
                }
                elem.COMENTARIOS = "";

                corpo.getLISTA_TAB_RENT_ANUAL.getTAB_RENT_ANUAL.Add(elem);
            }
            #endregion

            #region Ano1
            if (ano1 != null) {
                XML_LaminaV1.TAB_RENT_ANUAL elem = new XML_LaminaV1.TAB_RENT_ANUAL();

                elem.ANO_RENT = this.ultimos_5_Anos[3];
                if (ano1.Retorno.HasValue) {
                    elem.rent_liq = ano1.Retorno.Value;
                }
                if (ano1.RetornoBenchmark.HasValue)
                {
                    elem.var_ind_ref = ano1.RetornoBenchmark.Value;
                }
                if (ano1.RetornoDiferencial.HasValue)
                {
                    elem.rent_perc_ind_ref = ano1.RetornoDiferencial.Value;
                }
                elem.COMENTARIOS = "";

                corpo.getLISTA_TAB_RENT_ANUAL.getTAB_RENT_ANUAL.Add(elem);
            }
            #endregion

            #region Ano2
            if (ano2 != null) {
                XML_LaminaV1.TAB_RENT_ANUAL elem = new XML_LaminaV1.TAB_RENT_ANUAL();

                elem.ANO_RENT = this.ultimos_5_Anos[2];
                if (ano2.Retorno.HasValue)
                {
                    elem.rent_liq = ano2.Retorno.Value;
                }
                if (ano2.RetornoBenchmark.HasValue)
                {
                    elem.var_ind_ref = ano2.RetornoBenchmark.Value;
                }
                if (ano2.RetornoDiferencial.HasValue)
                {
                    elem.rent_perc_ind_ref = ano2.RetornoDiferencial.Value;
                }
                elem.COMENTARIOS = "";

                corpo.getLISTA_TAB_RENT_ANUAL.getTAB_RENT_ANUAL.Add(elem);
            }
            #endregion

            #region Ano3
            if (ano3 != null) {
                XML_LaminaV1.TAB_RENT_ANUAL elem = new XML_LaminaV1.TAB_RENT_ANUAL();

                elem.ANO_RENT = this.ultimos_5_Anos[1];
                if (ano3.Retorno.HasValue)
                {
                    elem.rent_liq = ano3.Retorno.Value;
                }
                if (ano3.RetornoBenchmark.HasValue)
                {
                    elem.var_ind_ref = ano3.RetornoBenchmark.Value;
                }
                if (ano3.RetornoDiferencial.HasValue)
                {
                    elem.rent_perc_ind_ref = ano3.RetornoDiferencial.Value;
                }
                elem.COMENTARIOS = "";

                corpo.getLISTA_TAB_RENT_ANUAL.getTAB_RENT_ANUAL.Add(elem);
            }
            #endregion

            #region Ano4
            if (ano4 != null) {
                XML_LaminaV1.TAB_RENT_ANUAL elem = new XML_LaminaV1.TAB_RENT_ANUAL();

                elem.ANO_RENT = this.ultimos_5_Anos[0];
                if (ano4.Retorno.HasValue)
                {
                    elem.rent_liq = ano4.Retorno.Value;
                }
                if (ano4.RetornoBenchmark.HasValue)
                {
                    elem.var_ind_ref = ano4.RetornoBenchmark.Value;
                }
                if (ano4.RetornoDiferencial.HasValue)
                {
                    elem.rent_perc_ind_ref = ano4.RetornoDiferencial.Value;
                }
                elem.COMENTARIOS = "";

                corpo.getLISTA_TAB_RENT_ANUAL.getTAB_RENT_ANUAL.Add(elem);
            }
            #endregion
            #endregion
        }

        /// <summary>
        /// Constroe o objeto LISTA_TAB_RENT_MENSAL
        /// </summary>
        /// <param name="dataPosicao"></param>
        /// <param name="idCliente"></param>
        /// <param name="corpo">Objeto representativo do Corpo do arquivo XML</param>
        private void ControeLISTA_TAB_RENT_MENSAL(DateTime dataPosicao, int idCliente, XML_LaminaV1.Corpo corpo, Lamina info) {

            Carteira carteira = new Carteira();
            carteira.LoadByPrimaryKey(idCliente);

            #region Calculos ( antigo comentado )
            /*
            //string descricaoIndice = carteira.UpToIndiceByIdIndiceBenchmark.Descricao.ToString();
            ////
            //CalculoMedida.EstatisticaRetornoMensal cJan = this.listaRetornosMensais.Find(delegate(CalculoMedida.EstatisticaRetornoMensal a) { return a.Data.Month == 1; });
            //CalculoMedida.EstatisticaRetornoMensal cFev = this.listaRetornosMensais.Find(delegate(CalculoMedida.EstatisticaRetornoMensal a) { return a.Data.Month == 2; });
            //CalculoMedida.EstatisticaRetornoMensal cMar = this.listaRetornosMensais.Find(delegate(CalculoMedida.EstatisticaRetornoMensal a) { return a.Data.Month == 3; });
            //CalculoMedida.EstatisticaRetornoMensal cAbr = this.listaRetornosMensais.Find(delegate(CalculoMedida.EstatisticaRetornoMensal a) { return a.Data.Month == 4; });
            //CalculoMedida.EstatisticaRetornoMensal cMai = this.listaRetornosMensais.Find(delegate(CalculoMedida.EstatisticaRetornoMensal a) { return a.Data.Month == 5; });
            //CalculoMedida.EstatisticaRetornoMensal cJun = this.listaRetornosMensais.Find(delegate(CalculoMedida.EstatisticaRetornoMensal a) { return a.Data.Month == 6; });
            //CalculoMedida.EstatisticaRetornoMensal cJul = this.listaRetornosMensais.Find(delegate(CalculoMedida.EstatisticaRetornoMensal a) { return a.Data.Month == 7; });
            //CalculoMedida.EstatisticaRetornoMensal cAgo = this.listaRetornosMensais.Find(delegate(CalculoMedida.EstatisticaRetornoMensal a) { return a.Data.Month == 8; });
            //CalculoMedida.EstatisticaRetornoMensal cSet = this.listaRetornosMensais.Find(delegate(CalculoMedida.EstatisticaRetornoMensal a) { return a.Data.Month == 9; });
            //CalculoMedida.EstatisticaRetornoMensal cOut = this.listaRetornosMensais.Find(delegate(CalculoMedida.EstatisticaRetornoMensal a) { return a.Data.Month == 10; });
            //CalculoMedida.EstatisticaRetornoMensal cNov = this.listaRetornosMensais.Find(delegate(CalculoMedida.EstatisticaRetornoMensal a) { return a.Data.Month == 11; });
            //CalculoMedida.EstatisticaRetornoMensal cDez = this.listaRetornosMensais.Find(delegate(CalculoMedida.EstatisticaRetornoMensal a) { return a.Data.Month == 12; });
            ////
            //decimal? rentabilidade12Meses = this.CalculaRentabilidade12Meses(this.listaRetornosMensais);
            //decimal? rentabilidade12MesesIndice = this.CalculaRentabilidade12MesesIndice(this.listaRetornosMensais);

            //#region Janeiro
            //if (cJan != null) {
            //    #region Controe LISTA_TAB_RENT_ANUAL

            //    XML_LaminaV1.TAB_RENT_MENSAL elem = new XML_LaminaV1.TAB_RENT_MENSAL();

            //    elem.MES = 1;
            //    if (cJan.Retorno.HasValue) {
            //        elem.rent_mens = cJan.Retorno.Value;
            //    }
            //    if (cJan.RetornoBenchmark.HasValue) {
            //        elem.var_mens_ind_ref = cJan.RetornoBenchmark.Value;
            //    }
            //    if (cJan.RetornoDiferencial.HasValue) {
            //        elem.rent_mens_perc_ind_ref = cJan.RetornoDiferencial.Value;
            //    }

            //    corpo.getLISTA_TAB_RENT_MENSAL.getTAB_RENT_MENSAL.Add(elem);

            //    #endregion
            //}
            //#endregion
            //#region Fevereiro
            //if (cFev != null) {
            //    #region Controe LISTA_TAB_RENT_ANUAL

            //    XML_LaminaV1.TAB_RENT_MENSAL elem = new XML_LaminaV1.TAB_RENT_MENSAL();

            //    elem.MES = 2;
            //    if (cFev.Retorno.HasValue) {
            //        elem.rent_mens = cFev.Retorno.Value;
            //    }
            //    if (cFev.RetornoBenchmark.HasValue) {
            //        elem.var_mens_ind_ref = cFev.RetornoBenchmark.Value;
            //    }
            //    if (cFev.RetornoDiferencial.HasValue) {
            //        elem.rent_mens_perc_ind_ref = cFev.RetornoDiferencial.Value;
            //    }

            //    corpo.getLISTA_TAB_RENT_MENSAL.getTAB_RENT_MENSAL.Add(elem);

            //    #endregion
            //}
            //#endregion
            //#region Março
            //if (cMar != null) {
            //    #region Controe LISTA_TAB_RENT_ANUAL

            //    XML_LaminaV1.TAB_RENT_MENSAL elem = new XML_LaminaV1.TAB_RENT_MENSAL();

            //    elem.MES = 3;
            //    if (cMar.Retorno.HasValue) {
            //        elem.rent_mens = cMar.Retorno.Value;
            //    }
            //    if (cMar.RetornoBenchmark.HasValue) {
            //        elem.var_mens_ind_ref = cMar.RetornoBenchmark.Value;
            //    }
            //    if (cMar.RetornoDiferencial.HasValue) {
            //        elem.rent_mens_perc_ind_ref = cMar.RetornoDiferencial.Value;
            //    }

            //    corpo.getLISTA_TAB_RENT_MENSAL.getTAB_RENT_MENSAL.Add(elem);

            //    #endregion
            //}
            //#endregion
            //#region Abril
            //if (cAbr != null) {
            //    #region Controe LISTA_TAB_RENT_ANUAL

            //    XML_LaminaV1.TAB_RENT_MENSAL elem = new XML_LaminaV1.TAB_RENT_MENSAL();

            //    elem.MES = 4;
            //    if (cAbr.Retorno.HasValue) {
            //        elem.rent_mens = cAbr.Retorno.Value;
            //    }
            //    if (cAbr.RetornoBenchmark.HasValue) {
            //        elem.var_mens_ind_ref = cAbr.RetornoBenchmark.Value;
            //    }
            //    if (cAbr.RetornoDiferencial.HasValue) {
            //        elem.rent_mens_perc_ind_ref = cAbr.RetornoDiferencial.Value;
            //    }

            //    corpo.getLISTA_TAB_RENT_MENSAL.getTAB_RENT_MENSAL.Add(elem);

            //    #endregion
            //}
            //#endregion
            //#region Maio
            //if (cMai != null) {
            //    #region Controe LISTA_TAB_RENT_ANUAL

            //    XML_LaminaV1.TAB_RENT_MENSAL elem = new XML_LaminaV1.TAB_RENT_MENSAL();

            //    elem.MES = 5;
            //    if (cMai.Retorno.HasValue) {
            //        elem.rent_mens = cMai.Retorno.Value;
            //    }
            //    if (cMai.RetornoBenchmark.HasValue) {
            //        elem.var_mens_ind_ref = cMai.RetornoBenchmark.Value;
            //    }
            //    if (cMai.RetornoDiferencial.HasValue) {
            //        elem.rent_mens_perc_ind_ref = cMai.RetornoDiferencial.Value;
            //    }

            //    corpo.getLISTA_TAB_RENT_MENSAL.getTAB_RENT_MENSAL.Add(elem);

            //    #endregion
            //}
            //#endregion
            //#region Junho
            //if (cJun != null) {
            //    #region Controe LISTA_TAB_RENT_ANUAL

            //    XML_LaminaV1.TAB_RENT_MENSAL elem = new XML_LaminaV1.TAB_RENT_MENSAL();

            //    elem.MES = 6;
            //    if (cJun.Retorno.HasValue) {
            //        elem.rent_mens = cJun.Retorno.Value;
            //    }
            //    if (cJun.RetornoBenchmark.HasValue) {
            //        elem.var_mens_ind_ref = cJun.RetornoBenchmark.Value;
            //    }
            //    if (cJun.RetornoDiferencial.HasValue) {
            //        elem.rent_mens_perc_ind_ref = cJun.RetornoDiferencial.Value;
            //    }

            //    corpo.getLISTA_TAB_RENT_MENSAL.getTAB_RENT_MENSAL.Add(elem);

            //    #endregion
            //}
            //#endregion
            //#region Julho
            //if (cJul != null) {
            //    #region Controe LISTA_TAB_RENT_ANUAL

            //    XML_LaminaV1.TAB_RENT_MENSAL elem = new XML_LaminaV1.TAB_RENT_MENSAL();

            //    elem.MES = 7;
            //    if (cJul.Retorno.HasValue) {
            //        elem.rent_mens = cJul.Retorno.Value;
            //    }
            //    if (cJul.RetornoBenchmark.HasValue) {
            //        elem.var_mens_ind_ref = cJul.RetornoBenchmark.Value;
            //    }
            //    if (cJul.RetornoDiferencial.HasValue) {
            //        elem.rent_mens_perc_ind_ref = cJul.RetornoDiferencial.Value;
            //    }

            //    corpo.getLISTA_TAB_RENT_MENSAL.getTAB_RENT_MENSAL.Add(elem);

            //    #endregion
            //}
            //#endregion
            //#region Agosto
            //if (cAgo != null) {
            //    #region Controe LISTA_TAB_RENT_ANUAL

            //    XML_LaminaV1.TAB_RENT_MENSAL elem = new XML_LaminaV1.TAB_RENT_MENSAL();

            //    elem.MES = 8;
            //    if (cAgo.Retorno.HasValue) {
            //        elem.rent_mens = cAgo.Retorno.Value;
            //    }
            //    if (cAgo.RetornoBenchmark.HasValue) {
            //        elem.var_mens_ind_ref = cAgo.RetornoBenchmark.Value;
            //    }
            //    if (cAgo.RetornoDiferencial.HasValue) {
            //        elem.rent_mens_perc_ind_ref = cAgo.RetornoDiferencial.Value;
            //    }

            //    corpo.getLISTA_TAB_RENT_MENSAL.getTAB_RENT_MENSAL.Add(elem);

            //    #endregion
            //}
            //#endregion
            //#region Setembro
            //if (cSet != null) {
            //    #region Controe LISTA_TAB_RENT_ANUAL

            //    XML_LaminaV1.TAB_RENT_MENSAL elem = new XML_LaminaV1.TAB_RENT_MENSAL();

            //    elem.MES = 9;
            //    if (cSet.Retorno.HasValue) {
            //        elem.rent_mens = cSet.Retorno.Value;
            //    }
            //    if (cSet.RetornoBenchmark.HasValue) {
            //        elem.var_mens_ind_ref = cSet.RetornoBenchmark.Value;
            //    }
            //    if (cSet.RetornoDiferencial.HasValue) {
            //        elem.rent_mens_perc_ind_ref = cSet.RetornoDiferencial.Value;
            //    }

            //    corpo.getLISTA_TAB_RENT_MENSAL.getTAB_RENT_MENSAL.Add(elem);

            //    #endregion
            //}
            //#endregion
            //#region Outubro
            //if (cOut != null) {
            //    #region Controe LISTA_TAB_RENT_ANUAL

            //    XML_LaminaV1.TAB_RENT_MENSAL elem = new XML_LaminaV1.TAB_RENT_MENSAL();

            //    elem.MES = 10;
            //    if (cOut.Retorno.HasValue) {
            //        elem.rent_mens = cOut.Retorno.Value;
            //    }
            //    if (cOut.RetornoBenchmark.HasValue) {
            //        elem.var_mens_ind_ref = cOut.RetornoBenchmark.Value;
            //    }
            //    if (cOut.RetornoDiferencial.HasValue) {
            //        elem.rent_mens_perc_ind_ref = cOut.RetornoDiferencial.Value;
            //    }

            //    corpo.getLISTA_TAB_RENT_MENSAL.getTAB_RENT_MENSAL.Add(elem);

            //    #endregion
            //}
            //#endregion
            //#region Novembro
            //if (cNov != null) {
            //    #region Controe LISTA_TAB_RENT_ANUAL

            //    XML_LaminaV1.TAB_RENT_MENSAL elem = new XML_LaminaV1.TAB_RENT_MENSAL();

            //    elem.MES = 11;
            //    if (cNov.Retorno.HasValue) {
            //        elem.rent_mens = cNov.Retorno.Value;
            //    }
            //    if (cNov.RetornoBenchmark.HasValue) {
            //        elem.var_mens_ind_ref = cNov.RetornoBenchmark.Value;
            //    }
            //    if (cNov.RetornoDiferencial.HasValue) {
            //        elem.rent_mens_perc_ind_ref = cNov.RetornoDiferencial.Value;
            //    }

            //    corpo.getLISTA_TAB_RENT_MENSAL.getTAB_RENT_MENSAL.Add(elem);

            //    #endregion
            //}
            //#endregion
            //#region Dezembro
            //if (cDez != null) {
            //    #region Controe LISTA_TAB_RENT_ANUAL

            //    XML_LaminaV1.TAB_RENT_MENSAL elem = new XML_LaminaV1.TAB_RENT_MENSAL();

            //    elem.MES = 12;
            //    if (cDez.Retorno.HasValue) {
            //        elem.rent_mens = cDez.Retorno.Value;
            //    }
            //    if (cDez.RetornoBenchmark.HasValue) {
            //        elem.var_mens_ind_ref = cDez.RetornoBenchmark.Value;
            //    }
            //    if (cDez.RetornoDiferencial.HasValue) {
            //        elem.rent_mens_perc_ind_ref = cDez.RetornoDiferencial.Value;
            //    }

            //    corpo.getLISTA_TAB_RENT_MENSAL.getTAB_RENT_MENSAL.Add(elem);

            //    #endregion
            //}
            //#endregion

            //decimal? rentabilidade12MesesDiferencial = null;
            //if (rentabilidade12Meses.HasValue && rentabilidade12MesesIndice.HasValue) {
            //    if (rentabilidade12MesesIndice != 0) {
            //        rentabilidade12MesesDiferencial = (rentabilidade12Meses / rentabilidade12MesesIndice) * 100;
            //    }
            //}

            // Rentabilidade 12 Meses / Rentabilidade 12 Meses Indice / Diferencial 
            ////ReportBase.ConfiguraSinalNegativo(((XRTableCell)((XRTableRow)table.Rows[13]).Cells[1]), rentabilidade12Meses, true, ReportBase.NumeroCasasDecimais.DuasCasasDecimais);
            ////ReportBase.ConfiguraSinalNegativo(((XRTableCell)((XRTableRow)table.Rows[13]).Cells[2]), rentabilidade12MesesIndice, true, ReportBase.NumeroCasasDecimais.DuasCasasDecimais);
            ////ReportBase.ConfiguraSinalNegativo(((XRTableCell)((XRTableRow)table.Rows[13]).Cells[3]), rentabilidade12MesesDiferencial, true, ReportBase.NumeroCasasDecimais.DuasCasasDecimais);
            */
            #endregion


            #region Calculos
            string descricaoIndice = carteira.UpToIndiceByIdIndiceBenchmark.Descricao.ToString();
            //
            CalculoMedida.EstatisticaRetornoMensal _1 = this.listaRetornosMensais.Count >= 1 ? this.listaRetornosMensais[0] : null;
            CalculoMedida.EstatisticaRetornoMensal _2 = this.listaRetornosMensais.Count >= 2 ? this.listaRetornosMensais[1] : null;
            CalculoMedida.EstatisticaRetornoMensal _3 = this.listaRetornosMensais.Count >= 3 ? this.listaRetornosMensais[2] : null;
            CalculoMedida.EstatisticaRetornoMensal _4 = this.listaRetornosMensais.Count >= 4 ? this.listaRetornosMensais[3] : null;
            CalculoMedida.EstatisticaRetornoMensal _5 = this.listaRetornosMensais.Count >= 5 ? this.listaRetornosMensais[4] : null;
            CalculoMedida.EstatisticaRetornoMensal _6 = this.listaRetornosMensais.Count >= 6 ? this.listaRetornosMensais[5] : null;
            CalculoMedida.EstatisticaRetornoMensal _7 = this.listaRetornosMensais.Count >= 7 ? this.listaRetornosMensais[6] : null;
            CalculoMedida.EstatisticaRetornoMensal _8 = this.listaRetornosMensais.Count >= 8 ? this.listaRetornosMensais[7] : null;
            CalculoMedida.EstatisticaRetornoMensal _9 = this.listaRetornosMensais.Count >= 9 ? this.listaRetornosMensais[8] : null;
            CalculoMedida.EstatisticaRetornoMensal _10 = this.listaRetornosMensais.Count >= 10 ? this.listaRetornosMensais[9] : null;
            CalculoMedida.EstatisticaRetornoMensal _11 = this.listaRetornosMensais.Count >= 11 ? this.listaRetornosMensais[10] : null;
            CalculoMedida.EstatisticaRetornoMensal _12 = this.listaRetornosMensais.Count >= 12 ? this.listaRetornosMensais[11] : null;
            //
            decimal? rentabilidade12Meses = this.CalculaRentabilidade12Meses(this.listaRetornosMensais);
            decimal? rentabilidade12MesesIndice = this.CalculaRentabilidade12MesesIndice(this.listaRetornosMensais);

            #region _1
            if (_1 != null) {
                #region Controe LISTA_TAB_RENT_ANUAL

                XML_LaminaV1.TAB_RENT_MENSAL elem = new XML_LaminaV1.TAB_RENT_MENSAL();

                elem.MES = _1.Data.Month;
                if (_1.Retorno.HasValue) {
                    elem.rent_mens = _1.Retorno.Value;
                }
                if (_1.RetornoBenchmark.HasValue) {
                    elem.var_mens_ind_ref = _1.RetornoBenchmark.Value;
                }
                if (_1.RetornoDiferencial.HasValue) {
                    elem.rent_mens_perc_ind_ref = _1.RetornoDiferencial.Value;
                }

                corpo.getLISTA_TAB_RENT_MENSAL.getTAB_RENT_MENSAL.Add(elem);

                #endregion
            }
            #endregion
            #region _2
            if (_2 != null) {
                #region Controe LISTA_TAB_RENT_ANUAL

                XML_LaminaV1.TAB_RENT_MENSAL elem = new XML_LaminaV1.TAB_RENT_MENSAL();

                elem.MES = _2.Data.Month;
                if (_2.Retorno.HasValue) {
                    elem.rent_mens = _2.Retorno.Value;
                }
                if (_2.RetornoBenchmark.HasValue) {
                    elem.var_mens_ind_ref = _2.RetornoBenchmark.Value;
                }
                if (_2.RetornoDiferencial.HasValue) {
                    elem.rent_mens_perc_ind_ref = _2.RetornoDiferencial.Value;
                }

                corpo.getLISTA_TAB_RENT_MENSAL.getTAB_RENT_MENSAL.Add(elem);

                #endregion
            }
            #endregion
            #region _3
            if (_3 != null) {
                #region Controe LISTA_TAB_RENT_ANUAL

                XML_LaminaV1.TAB_RENT_MENSAL elem = new XML_LaminaV1.TAB_RENT_MENSAL();

                elem.MES = _3.Data.Month;
                if (_3.Retorno.HasValue) {
                    elem.rent_mens = _3.Retorno.Value;
                }
                if (_3.RetornoBenchmark.HasValue) {
                    elem.var_mens_ind_ref = _3.RetornoBenchmark.Value;
                }
                if (_3.RetornoDiferencial.HasValue) {
                    elem.rent_mens_perc_ind_ref = _3.RetornoDiferencial.Value;
                }

                corpo.getLISTA_TAB_RENT_MENSAL.getTAB_RENT_MENSAL.Add(elem);

                #endregion
            }
            #endregion
            #region _4
            if (_4 != null) {
                #region Controe LISTA_TAB_RENT_ANUAL

                XML_LaminaV1.TAB_RENT_MENSAL elem = new XML_LaminaV1.TAB_RENT_MENSAL();

                elem.MES = _4.Data.Month;
                if (_4.Retorno.HasValue) {
                    elem.rent_mens = _4.Retorno.Value;
                }
                if (_4.RetornoBenchmark.HasValue) {
                    elem.var_mens_ind_ref = _4.RetornoBenchmark.Value;
                }
                if (_4.RetornoDiferencial.HasValue) {
                    elem.rent_mens_perc_ind_ref = _4.RetornoDiferencial.Value;
                }

                corpo.getLISTA_TAB_RENT_MENSAL.getTAB_RENT_MENSAL.Add(elem);

                #endregion
            }
            #endregion
            #region _5
            if (_5 != null) {
                #region Controe LISTA_TAB_RENT_ANUAL

                XML_LaminaV1.TAB_RENT_MENSAL elem = new XML_LaminaV1.TAB_RENT_MENSAL();

                elem.MES = _5.Data.Month;
                if (_5.Retorno.HasValue) {
                    elem.rent_mens = _5.Retorno.Value;
                }
                if (_5.RetornoBenchmark.HasValue) {
                    elem.var_mens_ind_ref = _5.RetornoBenchmark.Value;
                }
                if (_5.RetornoDiferencial.HasValue) {
                    elem.rent_mens_perc_ind_ref = _5.RetornoDiferencial.Value;
                }

                corpo.getLISTA_TAB_RENT_MENSAL.getTAB_RENT_MENSAL.Add(elem);

                #endregion
            }
            #endregion
            #region _6
            if (_6 != null) {
                #region Controe LISTA_TAB_RENT_ANUAL

                XML_LaminaV1.TAB_RENT_MENSAL elem = new XML_LaminaV1.TAB_RENT_MENSAL();

                elem.MES = _6.Data.Month;
                if (_6.Retorno.HasValue) {
                    elem.rent_mens = _6.Retorno.Value;
                }
                if (_6.RetornoBenchmark.HasValue) {
                    elem.var_mens_ind_ref = _6.RetornoBenchmark.Value;
                }
                if (_6.RetornoDiferencial.HasValue) {
                    elem.rent_mens_perc_ind_ref = _6.RetornoDiferencial.Value;
                }

                corpo.getLISTA_TAB_RENT_MENSAL.getTAB_RENT_MENSAL.Add(elem);

                #endregion
            }
            #endregion
            #region _7
            if (_7 != null) {
                #region Controe LISTA_TAB_RENT_ANUAL

                XML_LaminaV1.TAB_RENT_MENSAL elem = new XML_LaminaV1.TAB_RENT_MENSAL();

                elem.MES = _7.Data.Month;
                if (_7.Retorno.HasValue) {
                    elem.rent_mens = _7.Retorno.Value;
                }
                if (_7.RetornoBenchmark.HasValue) {
                    elem.var_mens_ind_ref = _7.RetornoBenchmark.Value;
                }
                if (_7.RetornoDiferencial.HasValue) {
                    elem.rent_mens_perc_ind_ref = _7.RetornoDiferencial.Value;
                }

                corpo.getLISTA_TAB_RENT_MENSAL.getTAB_RENT_MENSAL.Add(elem);

                #endregion
            }
            #endregion
            #region _8
            if (_8 != null) {
                #region Controe LISTA_TAB_RENT_ANUAL

                XML_LaminaV1.TAB_RENT_MENSAL elem = new XML_LaminaV1.TAB_RENT_MENSAL();

                elem.MES = _8.Data.Month;
                if (_8.Retorno.HasValue) {
                    elem.rent_mens = _8.Retorno.Value;
                }
                if (_8.RetornoBenchmark.HasValue) {
                    elem.var_mens_ind_ref = _8.RetornoBenchmark.Value;
                }
                if (_8.RetornoDiferencial.HasValue) {
                    elem.rent_mens_perc_ind_ref = _8.RetornoDiferencial.Value;
                }

                corpo.getLISTA_TAB_RENT_MENSAL.getTAB_RENT_MENSAL.Add(elem);

                #endregion
            }
            #endregion
            #region _9
            if (_9 != null) {
                #region Controe LISTA_TAB_RENT_ANUAL

                XML_LaminaV1.TAB_RENT_MENSAL elem = new XML_LaminaV1.TAB_RENT_MENSAL();

                elem.MES = _9.Data.Month;
                if (_9.Retorno.HasValue) {
                    elem.rent_mens = _9.Retorno.Value;
                }
                if (_9.RetornoBenchmark.HasValue) {
                    elem.var_mens_ind_ref = _9.RetornoBenchmark.Value;
                }
                if (_9.RetornoDiferencial.HasValue) {
                    elem.rent_mens_perc_ind_ref = _9.RetornoDiferencial.Value;
                }

                corpo.getLISTA_TAB_RENT_MENSAL.getTAB_RENT_MENSAL.Add(elem);

                #endregion
            }
            #endregion
            #region _10
            if (_10 != null) {
                #region Controe LISTA_TAB_RENT_ANUAL

                XML_LaminaV1.TAB_RENT_MENSAL elem = new XML_LaminaV1.TAB_RENT_MENSAL();

                elem.MES = _10.Data.Month;
                if (_10.Retorno.HasValue) {
                    elem.rent_mens = _10.Retorno.Value;
                }
                if (_10.RetornoBenchmark.HasValue) {
                    elem.var_mens_ind_ref = _10.RetornoBenchmark.Value;
                }
                if (_10.RetornoDiferencial.HasValue) {
                    elem.rent_mens_perc_ind_ref = _10.RetornoDiferencial.Value;
                }

                corpo.getLISTA_TAB_RENT_MENSAL.getTAB_RENT_MENSAL.Add(elem);

                #endregion
            }
            #endregion
            #region _11
            if (_11 != null) {
                #region Controe LISTA_TAB_RENT_ANUAL

                XML_LaminaV1.TAB_RENT_MENSAL elem = new XML_LaminaV1.TAB_RENT_MENSAL();

                elem.MES = _11.Data.Month;
                if (_11.Retorno.HasValue) {
                    elem.rent_mens = _11.Retorno.Value;
                }
                if (_11.RetornoBenchmark.HasValue) {
                    elem.var_mens_ind_ref = _11.RetornoBenchmark.Value;
                }
                if (_11.RetornoDiferencial.HasValue) {
                    elem.rent_mens_perc_ind_ref = _11.RetornoDiferencial.Value;
                }

                corpo.getLISTA_TAB_RENT_MENSAL.getTAB_RENT_MENSAL.Add(elem);

                #endregion
            }
            #endregion
            #region _12
            if (_12 != null) {
                #region Controe LISTA_TAB_RENT_ANUAL

                XML_LaminaV1.TAB_RENT_MENSAL elem = new XML_LaminaV1.TAB_RENT_MENSAL();

                elem.MES = _12.Data.Month;
                if (_12.Retorno.HasValue) {
                    elem.rent_mens = _12.Retorno.Value;
                }
                if (_12.RetornoBenchmark.HasValue) {
                    elem.var_mens_ind_ref = _12.RetornoBenchmark.Value;
                }
                if (_12.RetornoDiferencial.HasValue) {
                    elem.rent_mens_perc_ind_ref = _12.RetornoDiferencial.Value;
                }

                corpo.getLISTA_TAB_RENT_MENSAL.getTAB_RENT_MENSAL.Add(elem);

                #endregion
            }
            #endregion

            decimal? rentabilidade12MesesDiferencial = null;
            if (rentabilidade12Meses.HasValue && rentabilidade12MesesIndice.HasValue) {
                if (rentabilidade12MesesIndice != 0) {
                    rentabilidade12MesesDiferencial = (rentabilidade12Meses / rentabilidade12MesesIndice) * 100;
                }
            }

            /* Rentabilidade 12 Meses / Rentabilidade 12 Meses Indice / Diferencial */
            //ReportBase.ConfiguraSinalNegativo(((XRTableCell)((XRTableRow)table.Rows[13]).Cells[1]), rentabilidade12Meses, true, ReportBase.NumeroCasasDecimais.DuasCasasDecimais);
            //ReportBase.ConfiguraSinalNegativo(((XRTableCell)((XRTableRow)table.Rows[13]).Cells[2]), rentabilidade12MesesIndice, true, ReportBase.NumeroCasasDecimais.DuasCasasDecimais);
            //ReportBase.ConfiguraSinalNegativo(((XRTableCell)((XRTableRow)table.Rows[13]).Cells[3]), rentabilidade12MesesDiferencial, true, ReportBase.NumeroCasasDecimais.DuasCasasDecimais);

            #endregion
        }

        /// <summary>
        /// Constroe o objeto TAB_VAR_DESEMP
        /// </summary>
        /// <param name="dataPosicao"></param>
        /// <param name="idCliente"></param>
        /// <param name="corpo">Objeto representativo do Corpo do arquivo XML</param>
        private void ControeTAB_VAR_DESEMP(DateTime dataPosicao, int idCliente, XML_LaminaV1.Corpo corpo, Lamina info) {
            //
            //corpo.getTAB_VAR_DESEMP.VAR_DESEMP = !String.IsNullOrEmpty(info.DesempenhoFundo) ? info.DesempenhoFundo : ""; ;

            decimal desempenho = 0;
            try {
                if (!String.IsNullOrEmpty(info.DesempenhoFundo)) {
                    desempenho = Convert.ToDecimal(info.DesempenhoFundo);
                }
            }
            catch (Exception e4) { // nao faz nada
            }

            corpo.getTAB_VAR_DESEMP.var_desemp = desempenho;
                        
            corpo.getTAB_VAR_DESEMP.FORMULA_RENTAB = "";
            corpo.getTAB_VAR_DESEMP.CENARIO_GATILHO = "";
            corpo.getTAB_VAR_DESEMP.COMENTARIOS = "";
        }

        /// <summary>
        /// Constroe o objeto INFORM_EXEMPLO_COM
        /// </summary>
        /// <param name="dataPosicao"></param>
        /// <param name="idCliente"></param>
        /// <param name="corpo">Objeto representativo do Corpo do arquivo XML</param>
        private void ControeINFORM_EXEMPLO_COMP(DateTime dataPosicao, int idCliente, XML_LaminaV1.Corpo corpo, Lamina info) {

            DateTime dataInicio = Calendario.RetornaUltimoDiaUtilAno(new DateTime(dataPosicao.Year - 1, 1, 1));
            DateTime dataFim = Calendario.RetornaUltimoDiaUtilAno(new DateTime(dataPosicao.Year, 1, 1));
            //
            HistoricoCota historicoCota1 = new HistoricoCota();
            historicoCota1.LoadByPrimaryKey(dataInicio, idCliente);
            decimal cotaFechamentoInicio = historicoCota1.CotaFechamento.HasValue ? historicoCota1.CotaFechamento.Value : 0;
            //
            HistoricoCota historicoCota2 = new HistoricoCota();
            historicoCota2.LoadByPrimaryKey(dataFim, idCliente);
            decimal cotaFechamentoFim = historicoCota2.CotaFechamento.HasValue ? historicoCota2.CotaFechamento.Value : 0;

            decimal valorBruto = 0;
            if (historicoCota1.CotaFechamento.HasValue) {
                valorBruto = 1000 * (cotaFechamentoFim / cotaFechamentoInicio);
            }
            else {
                valorBruto = 1000 * 1;
            }

            decimal valorIR = 0;

            Carteira carteira = new Carteira();
            carteira.LoadByPrimaryKey(idCliente);

            if (carteira.TipoTributacao.Value == (byte)TipoTributacaoFundo.Acoes) {
                valorIR = 1000 * (cotaFechamentoFim - cotaFechamentoInicio) * 0.15M;
            }
            else if (carteira.TipoTributacao.Value == (byte)TipoTributacaoFundo.Isento) {
                valorIR = 1000 * (cotaFechamentoFim - cotaFechamentoInicio) * 0M;
            }
            else {
                valorIR = 1000 * (cotaFechamentoFim - cotaFechamentoInicio) * 0.20M;
            }

            decimal despesas = this.CalculaDespesas(dataPosicao, idCliente);
            //
            corpo.getINFORM_EXEMPLO_COMP.ANO_ANT = dataPosicao.Year - 1;
            corpo.getINFORM_EXEMPLO_COMP.ANO_EMISS = dataPosicao.Year;
            corpo.getINFORM_EXEMPLO_COMP.valor_resg = valorBruto - valorIR;
            corpo.getINFORM_EXEMPLO_COMP.impostos = valorIR;
            //corpo.getINFORM_EXEMPLO_COMP.VALOR_ING = 0;
            //corpo.getINFORM_EXEMPLO_COMP.VALOR_SAIDA = 0;
            //corpo.getINFORM_EXEMPLO_COMP.AJUSTE_PERFOR = 0;
            corpo.getINFORM_EXEMPLO_COMP.valor_despesas = despesas;
        }

        /// <summary>
        /// Constroe o objeto TAB_SIMUL_DESPESAS
        /// </summary>
        /// <param name="dataPosicao"></param>
        /// <param name="idCliente"></param>
        /// <param name="corpo">Objeto representativo do Corpo do arquivo XML</param>
        private void ControeTAB_SIMUL_DESPESAS(DateTime dataPosicao, int idCliente, XML_LaminaV1.Corpo corpo, Lamina info) {
            DateTime dataInicio = Calendario.RetornaUltimoDiaUtilAno(new DateTime(dataPosicao.Year, 1, 1));
            //
            HistoricoCota historicoCota = new HistoricoCota();
            historicoCota.LoadByPrimaryKey(dataInicio, idCliente);
            decimal plFechamento = historicoCota.PLFechamento.HasValue ? historicoCota.PLFechamento.Value : 0;

            /* regra de tres
             *   1000 - PL
             *   x  - Despesas
             *      X = (Despesas X 1000) / PL
            */
            decimal despesas = this.CalculaDespesas(dataPosicao, idCliente);

            decimal x = 0M;
            if (plFechamento != 0) {
                x = (despesas * 1000) / plFechamento;
            }

            decimal tresAnos = x * 3;
            decimal cincoAnos = x * 5;
            //
            decimal retornoBruto1 = 331M - tresAnos;
            decimal retornoBruto2 = 610.51M - cincoAnos;
            //
            corpo.getTAB_SIMUL_DESPESAS.desp_prev3 = tresAnos;
            corpo.getTAB_SIMUL_DESPESAS.desp_prev5 = cincoAnos;
            corpo.getTAB_SIMUL_DESPESAS.retor_brut3 = retornoBruto1;
            corpo.getTAB_SIMUL_DESPESAS.retor_brut5 = retornoBruto2;
        }
    }

    #endregion
    #endregion
}