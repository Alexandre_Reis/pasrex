﻿using System;
using System.IO;
using System.Xml.Serialization;
using System.Collections.Generic;
using Financial.Investidor;
using Financial.Investidor.Enums;
using Financial.CRM;
using Financial.InvestidorCotista;
using Financial.Fundo;
using Financial.Common.Enums;
using System.Xml;
using Financial.Fundo.Exceptions;
using Financial.Fundo.Enums;
using Financial.Util;
using Financial.ContaCorrente;
using Financial.ContaCorrente.Enums;

namespace Financial.Export {

    [Serializable()]
    [XmlRootAttribute(ElementName = "DOC_ARQ", IsNullable = false, Namespace = "urn:infdiario")]    
    public class XML_Informe_2_0 {

        [XmlElement(ElementName = "CAB_INFORM")]
        public Cabecalho cabecalho = new Cabecalho();

        [XmlArray("LISTA_INFORM")]
        [XmlArrayItem("INFORM")]
        public List<Corpo> corpo = new List<Corpo>();

        #region Cabecalho
        public class Cabecalho {

            public Cabecalho() { }

            [XmlElement("COD_DOC", Order = 1)]
            public int codigoDocumento;

            #region DT_COMPT
            [XmlIgnore]
            public DateTime dataCompetencia;

            [XmlElement("DT_COMPT", Order = 2)]
            public string formatDataCompetencia {
                get { return this.dataCompetencia.ToString("dd/MM/yyyy"); }
                set { dataCompetencia = DateTime.Parse(value); }
            }
            #endregion

            #region DT_GERAC_ARQ
            [XmlIgnore]
            public DateTime dataGeracaoArquivo;

            [XmlElement("DT_GERAC_ARQ", Order = 3)]
            public string formatDataGeracaoArquivo {
                get { return this.dataGeracaoArquivo.ToString("dd/MM/yyyy"); }
                set { dataGeracaoArquivo = DateTime.Parse(value); }
            }
            #endregion

            [XmlElement("VERSAO", Order = 4)]
            public string versao;
        }
        #endregion

        #region Corpo
        public class Corpo {

            public Corpo() { }

            /**********************************************/
            [XmlElement("CNPJ_FDO", Order = 1)]
            public string cnpj;

            [XmlIgnore]
            public decimal valorTotal;

            [XmlIgnore]
            public decimal valorQuota;

            [XmlIgnore]
            public decimal patrimonioLiquido;

            [XmlIgnore]
            public decimal aplicacao;

            [XmlIgnore]
            public decimal resgate;

            [XmlIgnore]
            public decimal valorTotalSai;

            [XmlIgnore]
            public decimal valorTotalAtv;

            #region VL_TOTAL
            [XmlElement("VL_TOTAL", Order = 2)]
            public string formatValorTotal {

                get { return this.valorTotal.ToString("N2").Replace(".", ""); }

                set {
                    decimal valorTotal = 0;
                    
                    if (Decimal.TryParse(value, out valorTotal))
                        valorTotal = valorTotal;
                }
            }
            #endregion

            #region VL_QUOTA
            [XmlElement("VL_QUOTA", Order = 3)]
            public string formatValorQuota {

                get { return valorQuota.ToString("N10").Replace(".", ""); }

                set {
                    decimal valorQuota = 0;

                    if (Decimal.TryParse(value, out valorQuota))
                        valorQuota = valorQuota;
                }
            }
            #endregion

            #region PATRIM_LIQ

            [XmlElement("PATRIM_LIQ", Order = 4)]
            public string formatPatrimonioLiquido {

                get { return patrimonioLiquido.ToString("N2").Replace(".", ""); }

                set {
                    decimal patrimonioLiquido = 0;

                    if (Decimal.TryParse(value, out patrimonioLiquido))
                        patrimonioLiquido = patrimonioLiquido;
                }
            }
            #endregion

            #region CAPTC_DIA

            [XmlElement("CAPTC_DIA", Order = 5)]
            public string formatAplicacao {

                get { return aplicacao.ToString("N2").Replace(".", ""); }

                set {
                    decimal aplicacao = 0;

                    if (Decimal.TryParse(value, out aplicacao))
                        aplicacao = aplicacao;
                }
            }


            #endregion

            #region RESG_DIA

            [XmlElement("RESG_DIA", Order = 6)]
            public string formatResgate {

                get { return resgate.ToString("N2").Replace(".", ""); }

                set {
                    decimal resgate = 0;

                    if (Decimal.TryParse(value, out resgate))
                        resgate = resgate;
                }
            }

            #endregion

            #region VL_TOTAL_SAI
            [XmlElement("VL_TOTAL_SAI", Order = 7)]
            public string formatValorTotalSai {

                get { return this.valorTotalSai.ToString("N2").Replace(".", ""); }

                set {
                    decimal valorTotalSai = 0;

                    if (Decimal.TryParse(value, out valorTotalSai))
                        valorTotalSai = valorTotalSai;
                }
            }
            #endregion

            #region VL_TOTAL_ATV
            [XmlElement("VL_TOTAL_ATV", Order = 8)]
            public string formatValorTotalAtv {

                get { return this.valorTotalAtv.ToString("N2").Replace(".", ""); }

                set {
                    decimal valorTotalAtv = 0;

                    if (Decimal.TryParse(value, out valorTotalAtv))
                        valorTotalAtv = valorTotalAtv;
                }
            }
            #endregion

            [XmlElement("NR_COTST", Order = 9)]
            public int numCotistas;
            
            [XmlElementAttribute("LISTA_COTST_SIGNIF", Order = 10)]
            public Lista_COTST_SIGNIF listaCOTST = new Lista_COTST_SIGNIF();

            [XmlIgnore]
            public Lista_COTST_SIGNIF getLista_COTST_SIGNIF {
                get { return listaCOTST; }
                set { listaCOTST = value; }
            }
        }
        #endregion

        #region Lista_COTST_SIGNIF em Corpo
        public class Lista_COTST_SIGNIF {
            // Construtor
            public Lista_COTST_SIGNIF() { }

            [XmlElementAttribute("COTST_SIGNIF")]
            public List<COTST_SIGNIF> cotst_SIGNIF = new List<COTST_SIGNIF>();

            [XmlIgnore]
            public List<COTST_SIGNIF> getCOTST_SIGNIF {
                get { return cotst_SIGNIF; }
                set { cotst_SIGNIF = value; }
            }
        }
        #endregion

        #region COTST_SIGNIF em Lista_COTST_SIGNIF
        public class COTST_SIGNIF {
            // Construtor
            public COTST_SIGNIF() { }

            [XmlElement("TP_PESSOA", Order = 1)]
            public string tipoPessoa;

            [XmlElement("CPF_CNPJ_COTST", Order = 2)]
            public string cpf_cnpj_cotista;

            [XmlIgnore]
            public decimal participacaoCotista;

            #region PR_COTST
            [XmlElement("PR_COTST", Order = 3)]
            public string formatParticipacao {

                get { return this.participacaoCotista.ToString("N4"); }

                set {
                    decimal participacaoCotista = 0;

                    if (Decimal.TryParse(value, out participacaoCotista))
                        participacaoCotista = participacaoCotista;
                }
            }
            #endregion
        }
        #endregion
    }

    public class InformeDiario_2_0 {

        enum TipoPesquisa {
            PosicaoCotista = 0,
            PosicaoCotistaHistorico = 1
        }
        
        public struct InfoCotista {
            public int idCotista;
            public decimal participacao;
        } 
        
        /// <summary>
        /// Retorna uma Stream de Mémoria de um arquivo xml. A Stream permanece aberta
        /// </summary>
        /// <param name="dataPosicao"></param>
        /// <param name="idClientes">lista de idClientes</param>
        /// <param name="ms">Saida: Memory Stream do arquivo InformeDiario</param>
        /// <param name="nomeArquivo">Saida: Nome do Arquivo InformeDiario</param>        
        /// <returns></returns>
        public void ExportaInformeDiario(DateTime dataPosicao, List<int> idClientes, out MemoryStream ms, out string nomeArquivo) {
            ms = new MemoryStream();
            nomeArquivo = "InformeDiario.xml";
            //            
            XML_Informe_2_0.Cabecalho c = this.MontaCabecalho(dataPosicao);
            List<XML_Informe_2_0.Corpo> conteudo = this.MontaConteudo(dataPosicao, idClientes);
            //
            XML_Informe_2_0 arq = new XML_Informe_2_0();
            arq.cabecalho = c;
            arq.corpo.AddRange(conteudo);
            //                     
            XmlSerializer x = new XmlSerializer(arq.GetType());
            //x.Serialize(ms, arq);

            XmlWriterSettings settings = new XmlWriterSettings();
            settings.Encoding = System.Text.Encoding.GetEncoding("WINDOWS-1252");
            settings.Indent = true;
            settings.IndentChars = "\t";
            settings.NewLineChars = Environment.NewLine;
            settings.ConformanceLevel = ConformanceLevel.Document;

            using (XmlWriter writer = XmlTextWriter.Create(ms, settings)) {
                x.Serialize(writer, arq);
            }
        }

        /// <summary>
        /// Monta o Cabeçalho do arquivo XML
        /// </summary>
        /// <param name="dataPosicao"></param>
        /// <returns></returns>
        private XML_Informe_2_0.Cabecalho MontaCabecalho(DateTime dataPosicao) 
        {
            XML_Informe_2_0.Cabecalho c = new XML_Informe_2_0.Cabecalho();
            //
            c.codigoDocumento = 1;
            c.dataCompetencia = dataPosicao;                        
            c.dataGeracaoArquivo = DateTime.Now;
            c.versao = "2.0";
            //
            return c;
        }

        /// <summary>
        /// Monta o conteudo do arquivo XML
        /// </summary>
        /// <param name="dataPosicao"></param>
        /// <param name="idClientes">Lista de IdClientes</param>
        /// <returns></returns>
        private List<XML_Informe_2_0.Corpo> MontaConteudo(DateTime dataPosicao, List<int> idClientes)
        {
            List<XML_Informe_2_0.Corpo> lista = new List<XML_Informe_2_0.Corpo>();

            //
            #region Consulta Inicial
            PessoaQuery pessoaQuery = new PessoaQuery("P");
            ClienteQuery clienteQuery = new ClienteQuery("C");
            CarteiraQuery carteiraQuery = new CarteiraQuery("A");
            ClienteCollection clienteCollection = new ClienteCollection();
            //
            clienteQuery.Select(clienteQuery, pessoaQuery.Cpfcnpj,
                                carteiraQuery.DiasCotizacaoResgate,carteiraQuery.DiasLiquidacaoResgate, carteiraQuery.DiasLiquidacaoAplicacao, carteiraQuery.ContagemDiasConversaoResgate);
            //
            clienteQuery.InnerJoin(pessoaQuery).On(pessoaQuery.IdPessoa == clienteQuery.IdPessoa);
            clienteQuery.InnerJoin(carteiraQuery).On(carteiraQuery.IdCarteira == clienteQuery.IdCliente);
            //
            clienteQuery.Where(clienteQuery.TipoControle == (byte)TipoControleCliente.Completo &&
                               clienteQuery.IdTipo.In((int)TipoClienteFixo.Fundo) &&
                               clienteQuery.IdCliente.In(idClientes));
            //
            clienteQuery.OrderBy(pessoaQuery.Cpfcnpj.Descending);
            //
            clienteCollection.Load(clienteQuery);
            //
            HistoricoCota h = new HistoricoCota();
            HistoricoCota h1 = new HistoricoCota();
            #endregion

            //
            for (int i = 0; i < clienteCollection.Count; i++) 
            {
                int idCliente = clienteCollection[i].IdCliente.Value;
                DateTime dataDia = clienteCollection[i].DataDia.Value;
                int diasCotizacao = Convert.ToInt32(clienteCollection[i].GetColumn(CarteiraMetadata.ColumnNames.DiasCotizacaoResgate));
                int diasLiquidacao = Convert.ToInt32(clienteCollection[i].GetColumn(CarteiraMetadata.ColumnNames.DiasLiquidacaoResgate));
                byte contagemDias = Convert.ToByte(clienteCollection[i].GetColumn(CarteiraMetadata.ColumnNames.ContagemDiasConversaoResgate));

                if (contagemDias == (byte)ContagemDiasLiquidacaoResgate.DiasCorridos)
                {
                    diasLiquidacao += diasCotizacao;
                }
                //
                h.BuscaValorCota(idCliente, dataPosicao);
                //h1.BuscaValorPatrimonioDia(idCliente, dataPosicao);

                decimal pl = 0.00M;
                try {
                    h1.BuscaValorPatrimonioDia(idCliente, dataPosicao);
                    pl = h1.PLFechamento.Value;
                }
                catch (HistoricoCotaNaoCadastradoException e) {
                    pl = 0;
                }

                #region Conteudo arquivo XML
                //
                XML_Informe_2_0.Corpo corpo = new XML_Informe_2_0.Corpo();
                //
                corpo.cnpj = clienteCollection[i].UpToPessoaByIdPessoa.Cpfcnpj.Replace(".", "").Replace("-", "").Replace("/", "").Trim().PadLeft(14, '0');
                //
                if (dataDia > dataPosicao)
                {
                    corpo.valorTotal = new HistoricoCota().CalculaCarteiraTotalHistorico(idCliente, dataPosicao, (int)ListaMoedaFixo.Real);  
                }
                else
                {
                    corpo.valorTotal = new HistoricoCota().CalculaCarteiraTotal(idCliente, dataPosicao, (int)ListaMoedaFixo.Real);
                }

                //
                corpo.valorQuota = h.CotaFechamento.HasValue ? h.CotaFechamento.Value : 0;
                corpo.patrimonioLiquido = pl;
                //
                corpo.aplicacao = new OperacaoCotista().RetornaSumValorBrutoAplicacao(idCliente, dataPosicao);
                corpo.resgate = new OperacaoCotista().RetornaSumValorBrutoResgate(idCliente, dataPosicao); ;
                //
                DateTime dataLiquidacao = Calendario.AdicionaDiaUtil(dataPosicao, diasLiquidacao);

                decimal valorPagar = 0;
                decimal valorReceber = 0;

                if (dataDia > dataPosicao)
                {
                    valorPagar = this.RetornaValorVencerHistorico(idCliente, dataPosicao, dataLiquidacao, SinalValorLiquidacao.Retirada);
                    valorReceber = this.RetornaValorVencerHistorico(idCliente, dataPosicao, dataLiquidacao, SinalValorLiquidacao.Entrada);
                }
                else
                {
                    valorPagar = this.RetornaValorVencer(idCliente, dataPosicao, dataLiquidacao, SinalValorLiquidacao.Retirada);
                    valorReceber = this.RetornaValorVencer(idCliente, dataPosicao, dataLiquidacao, SinalValorLiquidacao.Entrada);
                }               
                
                SaldoCaixa saldoCaixa = new SaldoCaixa();
                decimal caixa = 0;
                saldoCaixa.BuscaSaldoCaixa(idCliente, dataPosicao);
                if (saldoCaixa.SaldoFechamento.HasValue)
                {
                    caixa = saldoCaixa.SaldoFechamento.Value;
                }

                Carteira carteira = new Carteira();
                decimal carteiraLiquida = 0;
                if (dataDia > dataPosicao)
                {
                    carteiraLiquida = carteira.RetornaCarteiraLiquida(idCliente, diasLiquidacao, dataPosicao);
                }
                else
                {
                    carteiraLiquida = carteira.RetornaCarteiraLiquida(idCliente, diasLiquidacao, null);
                }

                corpo.valorTotalSai = Math.Abs(valorPagar);
                corpo.valorTotalAtv = Math.Abs(caixa + carteiraLiquida + valorReceber);

                corpo.numCotistas = dataPosicao >= clienteCollection[i].DataDia.Value
                                    ? new PosicaoCotista().RetornaCountNumeroCotistas(idCliente)
                                    : new PosicaoCotistaHistorico().RetornaCountNumeroCotistas(idCliente, dataPosicao);


                this.ControeCOTST_SIGNIF(dataPosicao, clienteCollection[i], idCliente, corpo);
               
                lista.Add(corpo);
                //
                #endregion
			}
            //
            return lista;
        }

        /// <summary>
        /// Constroe os objetos COTST_SIGNIF
        /// </summary>
        /// <param name="dataPosicao"></param>
        /// <param name="cliente"></param>
        /// <param name="idCliente"></param>
        /// <param name="corpo">Objeto representativo do Corpo do arquivo XML</param>
        private void ControeCOTST_SIGNIF(DateTime dataPosicao, Cliente cliente, int idCliente, XML_Informe_2_0.Corpo corpo)
        {
            #region Controe COTST_SIGNIF em LISTA_COTST_SIGNIF

            // Lista do Cotistas e suas participações - somente cotistas com mais de 20% de participação
            List<InfoCotista> infoCotista = new List<InfoCotista>();

            TipoPesquisa tipoPesquisa = dataPosicao >= cliente.DataDia.Value
                                        ? TipoPesquisa.PosicaoCotista : TipoPesquisa.PosicaoCotistaHistorico;

            #region PosicaoCotista
            if (tipoPesquisa == TipoPesquisa.PosicaoCotista) {

                #region Calcuta Total Quantidade
                PosicaoCotista pCotista = new PosicaoCotista();
                //
                pCotista.Query.Select(pCotista.Query.Quantidade.Sum()).Where(pCotista.Query.IdCarteira == idCliente);
                pCotista.Query.Load();

                decimal valorTotal = 0M;
                if (pCotista.es.HasData) {
                    if (pCotista.Quantidade.HasValue) {
                        valorTotal = pCotista.Quantidade.Value;
                    }
                }
                #endregion

                PosicaoCotistaCollection pCollection = new PosicaoCotistaCollection();

                pCollection.Query
                        .Select(pCollection.Query.IdCotista,
                                pCollection.Query.Quantidade.Sum())
                        .Where(pCollection.Query.IdCarteira == idCliente)
                        .GroupBy(pCollection.Query.IdCotista);
                //
                decimal participacao = 0M;
                if (pCollection.Query.Load()) {
                    for (int j = 0; j < pCollection.Count; j++) {
                        if (pCollection[j].Quantidade.HasValue) {
                            if(valorTotal != 0) {
                                participacao = pCollection[j].Quantidade.Value / valorTotal;

                                if (participacao >= 0.20M) { // Adiciona na Lista
                                    InfoCotista info;
                                    info.idCotista = pCollection[j].IdCotista.Value;
                                    info.participacao = Math.Round(participacao * 100, 4);
                                    //
                                    infoCotista.Add(info);
                                }
                            }
                        }
                    }
                }
            }
            #endregion

            #region PosicaoCotistaHistorico
            else if (tipoPesquisa == TipoPesquisa.PosicaoCotistaHistorico) {

                #region Calcuta Total Quantidade
                PosicaoCotistaHistorico pCotista = new PosicaoCotistaHistorico();
                //
                pCotista.Query.Select(pCotista.Query.Quantidade.Sum())
                              .Where(pCotista.Query.IdCarteira == idCliente &&
                                     pCotista.Query.DataHistorico == dataPosicao);
                pCotista.Query.Load();

                decimal valorTotal = 0M;
                if (pCotista.es.HasData) {
                    if (pCotista.Quantidade.HasValue) {
                        valorTotal = pCotista.Quantidade.Value;
                    }
                }
                #endregion

                PosicaoCotistaHistoricoCollection pCollection = new PosicaoCotistaHistoricoCollection();

                pCollection.Query
                        .Select(pCollection.Query.IdCotista,
                                pCollection.Query.Quantidade.Sum())
                        .Where(pCollection.Query.IdCarteira == idCliente &&
                               pCollection.Query.DataHistorico == dataPosicao)
                        .GroupBy(pCollection.Query.IdCotista);
                //
                decimal participacao = 0M;
                if (pCollection.Query.Load()) {
                    for (int j = 0; j < pCollection.Count; j++) {
                        if (pCollection[j].Quantidade.HasValue) {
                            if (valorTotal != 0) {
                                participacao = pCollection[j].Quantidade.Value / valorTotal;

                                if (participacao >= 0.20M) { // Adiciona na Lista
                                    InfoCotista info;
                                    info.idCotista = pCollection[j].IdCotista.Value;
                                    info.participacao = Math.Round(participacao * 100, 4);
                                    //
                                    infoCotista.Add(info);
                                }
                            }
                        }
                    }
                }
            }
            #endregion
            
            for (int i = 0; i < infoCotista.Count; i++) {
                XML_Informe_2_0.COTST_SIGNIF elem = new XML_Informe_2_0.COTST_SIGNIF();
                //
                Cotista c = new Cotista();
                c.LoadByPrimaryKey(infoCotista[i].idCotista);
                //                               
                elem.tipoPessoa = c.UpToPessoaByIdPessoa.IsPessoaFisica() ? "PF" : "PJ";
                elem.cpf_cnpj_cotista = c.UpToPessoaByIdPessoa.IsPessoaFisica()
                                        ? c.UpToPessoaByIdPessoa.Cpfcnpj.Replace(".", "").Replace("-", "").Replace("/", "").Trim().PadLeft(11, '0')
                                        : c.UpToPessoaByIdPessoa.Cpfcnpj.Replace(".", "").Replace("-", "").Replace("/", "").Trim().PadLeft(14, '0');

                elem.participacaoCotista = infoCotista[i].participacao;
                //
                corpo.getLista_COTST_SIGNIF.getCOTST_SIGNIF.Add(elem);                
            }

            #endregion
        }

        private decimal RetornaValorVencer(int idCliente, DateTime data, DateTime dataVencimento, SinalValorLiquidacao sinalValorLiquidacao)
        {
            Liquidacao liquidacao = new Liquidacao();
            liquidacao.QueryReset();
            liquidacao.Query
                 .Select(liquidacao.Query.Valor.Sum())
                 .Where(liquidacao.Query.IdCliente == idCliente,
                        liquidacao.Query.DataVencimento.GreaterThan(data),
                        liquidacao.Query.DataVencimento.LessThanOrEqual(dataVencimento),
                        liquidacao.Query.DataLancamento.LessThanOrEqual(data),
                        liquidacao.Query.Situacao == (Int16)SituacaoLancamentoLiquidacao.Normal);

            if (sinalValorLiquidacao == SinalValorLiquidacao.Entrada)
            {
                liquidacao.Query.Where(liquidacao.Query.Valor.GreaterThan(0));
            }
            else if (sinalValorLiquidacao == SinalValorLiquidacao.Retirada)
            {
                liquidacao.Query.Where(liquidacao.Query.Valor.LessThan(0));
            }

            liquidacao.Query.Load();

            return liquidacao.Valor.HasValue ? liquidacao.Valor.Value : 0;
        }

        private decimal RetornaValorVencerHistorico(int idCliente, DateTime data, DateTime dataVencimento, SinalValorLiquidacao sinalValorLiquidacao)
        {
            LiquidacaoHistorico liquidacao = new LiquidacaoHistorico();
            liquidacao.QueryReset();
            liquidacao.Query
                 .Select(liquidacao.Query.Valor.Sum())
                 .Where(liquidacao.Query.IdCliente == idCliente,
                        liquidacao.Query.DataHistorico == data, 
                        liquidacao.Query.DataVencimento.GreaterThan(data),
                        liquidacao.Query.DataVencimento.LessThanOrEqual(dataVencimento),
                        liquidacao.Query.DataLancamento.LessThanOrEqual(data),
                        liquidacao.Query.Situacao == (Int16)SituacaoLancamentoLiquidacao.Normal);

            if (sinalValorLiquidacao == SinalValorLiquidacao.Entrada)
            {
                liquidacao.Query.Where(liquidacao.Query.Valor.GreaterThan(0));
            }
            else if (sinalValorLiquidacao == SinalValorLiquidacao.Retirada)
            {
                liquidacao.Query.Where(liquidacao.Query.Valor.LessThan(0));
            }

            liquidacao.Query.Load();

            return liquidacao.Valor.HasValue ? liquidacao.Valor.Value : 0;
        }
    }
}