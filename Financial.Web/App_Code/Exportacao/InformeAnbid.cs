﻿using FileHelpers;
using System.IO;
using Financial.Investidor;
using Financial.Investidor.Enums;
using System.Collections.Generic;
using Financial.Fundo;
using Financial.InvestidorCotista;
using System.Text;
using System;
using Financial.Fundo.Exceptions;

namespace Financial.Export {

    /// <summary>
    /// Summary description for InformeAnbid
    /// </summary>
    public class InformeAnbid {

        [DelimitedRecord("\t")]
        public class InformeAnbidDS {

            public int codigoCarteira;

            [FieldConverter(ConverterKind.Date, "dd/MM/yyyy")]
            public DateTime dataAnbid;

            [FieldConverter(typeof(TwoDecimalConverter))]
            public decimal patrimonioLiquido;

            [FieldConverter(typeof(SixDecimalConverter))]
            public decimal valorQuota;

            [FieldConverter(typeof(TwoDecimalConverter))]
            public decimal aplicacao;

            [FieldConverter(typeof(TwoDecimalConverter))]
            public decimal resgate;

            [FieldConverter(typeof(TwoDecimalConverter))]
            public decimal comeCotas;

            public int numCotistas;

            #region Converter 2 e 6 Digitos
            internal class TwoDecimalConverter : ConverterBase {
                // Converte de string para decimal
                public override object StringToField(string from) {
                    return Convert.ToDecimal(from);                    
                }

                // Converte de decimal para string
                public override string FieldToString(object from) {
                    decimal res = Convert.ToDecimal(from);
                    return res.ToString("N2").Replace(".", "");
                }
            }

            internal class SixDecimalConverter : ConverterBase {
                // Converte de string para decimal
                public override object StringToField(string from) {
                    return Convert.ToDecimal(from);
                }

                // Converte de decimal para string
                public override string FieldToString(object from) {
                    decimal res = Convert.ToDecimal(from);
                    return res.ToString("N6").Replace(".", "");
                }
            }
            #endregion
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="data"></param>
        /// <param name="idClientes">Lista de Clientes</param>
        /// <param name="ms">Saida: Memory Stream do arquivo InformeAnbid</param>
        /// <param name="nomeArquivo">Saida: Nome do Arquivo InformeAnbid</param>        
        public void ExportaInformeAnbid(DateTime data, List<int> idClientes, out MemoryStream ms, out string nomeArquivo) {
            ms = new MemoryStream();
            nomeArquivo = "InformeAnbima_" + data.ToString("ddMMyyyy") + ".txt";

            #region Consulta Cliente
            ClienteQuery clienteQuery = new ClienteQuery("C");
            ClienteCollection clienteCollection = new ClienteCollection();
            //
            clienteQuery.Select(clienteQuery);
            clienteQuery.Where(clienteQuery.TipoControle.In((byte)TipoControleCliente.Completo,
                                    (byte)TipoControleCliente.Cotista) &&
                               clienteQuery.IdTipo.In((int)TipoClienteFixo.Fundo, (int)TipoClienteFixo.FDIC) &&
                               clienteQuery.IdCliente.In(idClientes));
            //
            clienteQuery.OrderBy(clienteQuery.IdCliente.Descending);
            //
            clienteCollection.Load(clienteQuery);
            //
            if (clienteCollection.Count == 0) {
                throw new Exception("Não há dados para exportar.");
            }
            #endregion

            List<InformeAnbidDS> informeAnbidDS = new List<InformeAnbidDS>();
            //
            HistoricoCota h = new HistoricoCota();
            HistoricoCota h1 = new HistoricoCota();
            //
            for (int i = 0; i < clienteCollection.Count; i++) {
                InformeAnbidDS inf = new InformeAnbidDS();
                //
                int idCliente = clienteCollection[i].IdCliente.Value;
                //

                #region Cota/Patrimonio
                h.BuscaValorCota(idCliente, data);

                decimal pl = 0.00M;
                try {
                    h1.BuscaValorPatrimonioDia(idCliente, data);
                    pl = h1.PLFechamento.Value;
                }
                catch (HistoricoCotaNaoCadastradoException e) {
                    pl = 0.00M;
                }
                #endregion

                Carteira carteira = new Carteira();
                carteira.LoadByPrimaryKey(idCliente);

                int codigoAnbid = 0;
                if (!String.IsNullOrEmpty(carteira.CodigoAnbid))
                {
                    codigoAnbid = Convert.ToInt32(carteira.CodigoAnbid);
                }

                #region Conteudo arquivo TXT
                inf.codigoCarteira = codigoAnbid;
                inf.dataAnbid = data;
                //
                inf.patrimonioLiquido = pl;
                inf.valorQuota = h.CotaFechamento.HasValue ? h.CotaFechamento.Value : 0.00M;
                //
                inf.aplicacao = new OperacaoCotista().RetornaSumValorBrutoAplicacao(idCliente, data);
                inf.resgate = new OperacaoCotista().RetornaSumValorBrutoResgate(idCliente, data);
                inf.comeCotas = new OperacaoCotista().RetornaSumValorComeCotas(idCliente, data);
                //
                inf.numCotistas = data >= clienteCollection[i].DataDia.Value
                                    ? new PosicaoCotista().RetornaCountNumeroCotistas(idCliente)
                                    : new PosicaoCotistaHistorico().RetornaCountNumeroCotistas(idCliente, data);
                //
                informeAnbidDS.Add(inf);
                #endregion
            }

            FileHelperEngine engine = new FileHelperEngine(typeof(InformeAnbidDS));
            StreamWriter arquivo = new StreamWriter(ms, Encoding.GetEncoding("ISO-8859-1"));
            //
            engine.Encoding = Encoding.GetEncoding("ISO-8859-1");
            engine.WriteStream(arquivo, informeAnbidDS);

            //Fecha o arquivo
            arquivo.Flush();
        }
    }
}