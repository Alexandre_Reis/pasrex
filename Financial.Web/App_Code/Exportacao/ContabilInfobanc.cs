﻿using FileHelpers;
using System;
using Financial.Contabil;
using System.Collections.Generic;
using System.IO;
using Financial.WebConfigConfiguration;
using System.Text;
using Financial.Investidor;
using Financial.Util;
using EntitySpaces.Core;

namespace Financial.Export {

    /// <summary>
    /// Summary description for ContabilInfobanc
    /// </summary>
    public class ContabilInfobanc {
        
        [FixedLengthRecord()]
        public class ContabInfobancDS
        {
            [FieldFixedLength(8)]
            [FieldConverter(ConverterKind.Date, "yyyyMMdd")]
            public DateTime dataLancamento;

            [FieldFixedLength(5)]
            [FieldAlign(AlignMode.Right, '0')]
            public int contaDebitoReduzida = 0;
            
            //COMENTADO POIS VAI NO CORPO DA CONTA REDUZIDA
            //[FieldFixedLength(1)]
            //[FieldAlign(AlignMode.Right, '0')]
            //public int digitoContaDebito;

            [FieldFixedLength(5)]
            [FieldAlign(AlignMode.Right, '0')]
            public int contaCreditoReduzida = 0;

            //COMENTADO POIS VAI NO CORPO DA CONTA REDUZIDA
            //[FieldFixedLength(1)]
            //[FieldAlign(AlignMode.Right, '0')]
            //public int digitoContaCredito;

            [FieldFixedLength(18)]
            [FieldAlign(AlignMode.Right, '0')]
            public Int64 valorLancamento;

            [FieldFixedLength(6)]
            [FieldAlign(AlignMode.Right, '0')]
            public int numeroDocumento;

            [FieldFixedLength(40)]
            //[FieldAlign(AlignMode.Left, '0')]
            public string historico;

            [FieldFixedLength(40)]
            //[FieldAlign(AlignMode.Left, '0')]
            public string complementoHistorico1;

            [FieldFixedLength(40)]
            //[FieldAlign(AlignMode.Left, '0')]
            public string complementoHistorico2;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dataInicio"></param>
        /// <param name="dataFim"></param>
        /// <param name="idClientes"></param>
        /// <param name="dicMStream">Dicionario de memory Streams cada uma representando 1 arquivo e seus respectivos nomes de arquivos</param>
        public void ExportaContabLancamento(DateTime dataInicio, DateTime dataFim, List<int> idClientes, out Dictionary<string, MemoryStream> dicMStream) {

            dicMStream = new Dictionary<string, MemoryStream>();

            #region Consulta Sql - Trata Erros
                     
            ContabLancamentoQuery contabLancamentoQuery = new ContabLancamentoQuery("C");
            //
            contabLancamentoQuery.Where(contabLancamentoQuery.DataLancamento >= dataInicio &&
                                        contabLancamentoQuery.DataLancamento <= dataFim &&
                                        contabLancamentoQuery.IdCliente.In(idClientes)
                                        );
            //
            contabLancamentoQuery.OrderBy(contabLancamentoQuery.IdCliente.Ascending);
            //
            ContabLancamentoCollection contabLancamentoCollection = new ContabLancamentoCollection();
            contabLancamentoCollection.Load(contabLancamentoQuery);

            if (contabLancamentoCollection.Count == 0) {
                throw new Exception("Não há dados para exportar.");
            }
            #endregion

            idClientes.Sort(); // ordem crescente
            for (int i = 0; i < idClientes.Count; i++) { // Para cada Cliente

                #region Para cada Cliente
                esEntityCollectionView<ContabLancamento> view = new esEntityCollectionView<ContabLancamento>(contabLancamentoCollection);

                // Procura por IdCliente
                view.Filter = ContabLancamentoMetadata.ColumnNames.IdCliente + " = " + idClientes[i];

                List<ContabInfobancDS> contabInfobancDS = new List<ContabInfobancDS>();

                for (int j = 0; j < view.Count; j++) {
                    int idcliente = view[j].IdCliente.Value;
                    
                    #region Lancamento
                    ContabInfobancDS cl = new ContabInfobancDS();
                    //                
                    cl.dataLancamento = view[j].DataLancamento.Value;
                    //
                    #region ContaDebitoReduzida
                    ContabConta contabConta = new ContabConta();
                    contabConta.LoadByPrimaryKey(view[j].IdContaDebito.Value);

                    string contaDebitoCodigoReduzida = !String.IsNullOrEmpty(contabConta.CodigoReduzida)? contabConta.CodigoReduzida.Trim(): "";

                    if (contaDebitoCodigoReduzida.Length >= 5) {
                        contaDebitoCodigoReduzida = contaDebitoCodigoReduzida.Substring(0, 5);
                    }
                    try {
                        cl.contaDebitoReduzida = Convert.ToInt32(contaDebitoCodigoReduzida);
                    }
                    catch {
                    }
                    #endregion

                    //cl.digitoContaDebito = 0; //COMENTADO POIS VAI NO CORPO DA CONTA REDUZIDA

                    //
                    #region ContaCreditoReduzida
                    contabConta = new ContabConta();
                    contabConta.LoadByPrimaryKey(view[j].IdContaCredito.Value);

                    string contaCreditoCodigoReduzida = !String.IsNullOrEmpty(contabConta.CodigoReduzida) ? contabConta.CodigoReduzida.Trim() : "";

                    if (contaCreditoCodigoReduzida.Length >= 5) {
                        contaCreditoCodigoReduzida = contaCreditoCodigoReduzida.Substring(0, 5);
                    }
                    try {
                        cl.contaCreditoReduzida = Convert.ToInt32(contaCreditoCodigoReduzida);
                    }
                    catch {
                    }
                    #endregion

                    //cl.digitoContaCredito = 0; //COMENTADO POIS VAI NO CORPO DA CONTA REDUZIDA
                    //
                    cl.valorLancamento = Convert.ToInt64(view[j].Valor.Value * 100);
                    //
                    cl.numeroDocumento = 0; // fica com 000000 pelo align
                    //
                    cl.historico = view[j].Descricao.Trim();
                    //
                    cl.complementoHistorico1 = "";
                    cl.complementoHistorico2 = "";
                    //
                    contabInfobancDS.Add(cl);
                    #endregion
                    //                    
                }

                if (contabInfobancDS.Count != 0) {
                    string nomeArquivo = "Contabil_" + idClientes[i] + ".txt";
                    MemoryStream ms = new MemoryStream();

                    #region GeraArquivo
                    FileHelperEngine engine = new FileHelperEngine(typeof(ContabInfobancDS));

                    StreamWriter arquivo = new StreamWriter(ms, Encoding.GetEncoding("ISO-8859-1"));
                    engine.Encoding = Encoding.GetEncoding("ISO-8859-1");
                    //
                    engine.WriteStream(arquivo, contabInfobancDS);

                    //Fecha o arquivo
                    arquivo.Flush();
                    #endregion

                    // Retorna a MemoryStream para o inicio
                    ms.Seek(0, SeekOrigin.Begin);

                    dicMStream.Add(nomeArquivo, ms);
                }
                #endregion
            }            
        }
    }
}