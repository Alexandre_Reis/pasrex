﻿using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.IO;
using System.Collections.Generic;

using EntitySpaces.Interfaces;
using Financial.Web.Common;
using Bytescout.Spreadsheet;
using Financial.InvestidorCotista;
using Financial.Fundo;
using Financial.BMF;
using Financial.BMF.Enums;
using System.Drawing;
using Financial.Bolsa;
using Financial.RendaFixa;
using Financial.ContaCorrente;
using Financial.Investidor;
using Financial.Bolsa.Enums;
using Financial.RendaFixa.Enums;
using Financial.Common;
using Financial.CRM;

public partial class ExportacaoBasePage : ConsultaBasePage 
{        
    #region Enums
    enum TipoExportacaoPosicao {
        PosicaoBolsa = 1,
        PosicaoBMF = 2,
        PosicaoEmprestimoBolsa = 3,
        PosicaoTermo = 4,
        PosicaoFundo = 5,
        PosicaoRendaFixa = 6,
        PosicaoCotista = 7,
        Liquidacao = 8,
        Cotista = 9
    }
    #endregion    

    /// <summary>
    /// Gera um Conjunto de Planilhas Excel de acordo com as opções escolhidas num combo
    /// </summary>
    /// <param name="ids">ids das opções escolhidas no combo - sempre tem algum valor</param>
    /// <param name="idsCarteiras">ids Carteiras escolhidos - sempre tem algum valor</param>
    /// <param name="dicMStream">Dicionario de memory Streams cada uma representando 1 arquivo e seus respectivos nomes de arquivos</param>
    protected void ProcessaPlanilhasPosicao(List<int> ids, List<int> idsCarteiras, out Dictionary<string, MemoryStream> dicMStream) {

        dicMStream = new Dictionary<string, MemoryStream>();

        #region Bolsa
        if (ids.Contains((int)TipoExportacaoPosicao.PosicaoBolsa)) {
            MemoryStream m1 = this.GeraPosicaoBolsa(idsCarteiras);
            dicMStream.Add(Path.GetFileName(this.pathExcelPosicoes["Path_ImportacaoPosicaoBolsa"]), m1);
        } 
        #endregion
        #region BMF
        if (ids.Contains((int)TipoExportacaoPosicao.PosicaoBMF)) {
            MemoryStream m2 = this.GeraPosicaoBMF(idsCarteiras);
            dicMStream.Add(Path.GetFileName(this.pathExcelPosicoes["Path_ImportacaoPosicaoBMF"]), m2);
        } 
        #endregion
        #region EmprestimoBolsa
        if (ids.Contains((int)TipoExportacaoPosicao.PosicaoEmprestimoBolsa)) {
            MemoryStream m3 = this.GeraPosicaoEmprestimoBolsa(idsCarteiras);
            dicMStream.Add(Path.GetFileName(this.pathExcelPosicoes["Path_ImportacaoPosicaoEmprestimoBolsa"]), m3);
        } 
        #endregion
        #region PosicaoTermo
        if (ids.Contains((int)TipoExportacaoPosicao.PosicaoTermo)) {
            MemoryStream m4 = this.GeraPosicaoTermo(idsCarteiras);
            dicMStream.Add(Path.GetFileName(this.pathExcelPosicoes["Path_ImportacaoPosicaoTermoBolsa"]), m4);            
        } 
        #endregion
        #region PosicaoFundo
        if (ids.Contains((int)TipoExportacaoPosicao.PosicaoFundo)) {
            MemoryStream m5 = this.GeraPosicaoFundo(idsCarteiras);
            dicMStream.Add(Path.GetFileName(this.pathExcelPosicoes["Path_ImportacaoPosicaoFundo"]), m5);
        } 
        #endregion
        #region RendaFixa
        if (ids.Contains((int)TipoExportacaoPosicao.PosicaoRendaFixa)) {
            MemoryStream m6 = this.GeraPosicaoRendaFixa(idsCarteiras);
            dicMStream.Add(Path.GetFileName(this.pathExcelPosicoes["Path_ImportacaoPosicaoRendaFixa"]), m6);
        } 
        #endregion
        #region PosicaoCotista
        if (ids.Contains((int)TipoExportacaoPosicao.PosicaoCotista)) {
            MemoryStream m7 = this.GeraPosicaoCotista(idsCarteiras);
            dicMStream.Add(Path.GetFileName(this.pathExcelPosicoes["Path_ImportacaoPosicaoCotista"]), m7);
        } 
        #endregion
        #region Liquidacao
        if (ids.Contains((int)TipoExportacaoPosicao.Liquidacao)) {
            MemoryStream m8 = this.GeraLiquidacao(idsCarteiras);
            dicMStream.Add(Path.GetFileName(this.pathExcelPosicoes["Path_ImportacaoLiquidacao"]), m8);
        } 
        #endregion
        #region CadastroCotista
        if (ids.Contains((int)TipoExportacaoPosicao.Cotista)) {
            MemoryStream m9 = this.GeraCadastroCotista(idsCarteiras);
            dicMStream.Add(Path.GetFileName(this.pathExcelPosicoes["Path_ImportacaoCadastroCotistas"]), m9);
        }
        #endregion
    }
    
    /// <summary>
    /// Retorna uma Planilha de Cadastro Cotista
    /// </summary>
    /// <param name="idsCarteiras"></param>
    private MemoryStream GeraCadastroCotista(List<int> idsCarteiras) {
        MemoryStream msExcel = new MemoryStream();

        Spreadsheet document = CarregaModelo(this.pathExcelPosicoes["Path_ImportacaoCadastroCotistas"]);

        Worksheet worksheet = document.Workbook.Worksheets[0];

        #region Preenche dados Planilha

        PosicaoCotistaQuery pQuery = new PosicaoCotistaQuery("P");

        pQuery.Select(pQuery.IdCotista);
        pQuery.Where(pQuery.IdCarteira.In(idsCarteiras));
        pQuery.es.Distinct = true;

        PosicaoCotistaCollection p1 = new PosicaoCotistaCollection();
        p1.Load(pQuery);

        for (int i = 0; i < p1.Count; i++) {
            int idPessoa = p1[i].IdCotista.Value;

            Cotista c = new Cotista();
            c.LoadByPrimaryKey(idPessoa);

            //
            int k = i + 2;

            /* Info Cotista */
            worksheet.Cell("A" + k).ValueAsInteger = c.IdCotista.Value; // IdCliente
            worksheet.Cell("B" + k).ValueAsString = c.Nome.Trim(); // Nome
            worksheet.Cell("C" + k).ValueAsInteger = (int)c.UpToPessoaByIdPessoa.Tipo.Value; // Tipo
            worksheet.Cell("D" + k).ValueAsString = c.UpToPessoaByIdPessoa.Cpfcnpj.Replace(".", "").Replace("-", "").Replace("/", "").Trim(); ; // Cpfcnpj
            worksheet.Cell("E" + k).ValueAsString = c.IsentoIR.Trim().ToUpper(); // IsentoIR
            worksheet.Cell("F" + k).ValueAsString = c.IsentoIOF.Trim().ToUpper(); // IsentoIOF
            worksheet.Cell("G" + k).ValueAsInteger = (int)c.StatusAtivo.Value; // StatusAtivo
            worksheet.Cell("H" + k).ValueAsInteger = c.TipoTributacao.Value; // TipoTributacao

            if (!String.IsNullOrEmpty(c.UpToPessoaByIdPessoa.PessoaVinculada)) {
                worksheet.Cell("I" + k).ValueAsString = c.UpToPessoaByIdPessoa.PessoaVinculada.ToUpper(); // PessoaVinculada
            }

            if (c.UpToPessoaByIdPessoa.SituacaoLegal.HasValue)
            {
                worksheet.Cell("J" + k).ValueAsInteger = (int)c.UpToPessoaByIdPessoa.SituacaoLegal.Value; // SituacaoLegal
            }

            if (c.TipoCotistaCVM.HasValue) {
                worksheet.Cell("K" + k).ValueAsInteger = c.TipoCotistaCVM.Value; // TipoCotistaCVM
            }

            if (!String.IsNullOrEmpty(c.CodigoInterface)) {
                worksheet.Cell("L" + k).ValueAsString = c.CodigoInterface; // CodigoInterface
            }

            if (c.UpToPessoaByIdPessoa.EstadoCivil.HasValue)
            {
                worksheet.Cell("M" + k).ValueAsInteger = c.UpToPessoaByIdPessoa.EstadoCivil.Value; // EstadoCivil
            }

            /* ----------------------------------------------------------------*/

            if (!String.IsNullOrEmpty(c.UpToPessoaByIdPessoa.Sexo))
            {
                worksheet.Cell("R" + k).ValueAsString = c.UpToPessoaByIdPessoa.Sexo.ToUpper(); // Sexo
            }

            if (c.UpToPessoaByIdPessoa.DataNascimento.HasValue)
            {
                worksheet.Cell("S" + k).ValueAsDateTime = c.UpToPessoaByIdPessoa.DataNascimento.Value; // Data Nascimento
            }

            if (!String.IsNullOrEmpty(c.UpToPessoaByIdPessoa.Profissao))
            {
                worksheet.Cell("T" + k).ValueAsString = c.UpToPessoaByIdPessoa.Profissao; // Profissao
            }

            if (!String.IsNullOrEmpty(c.UpToPessoaByIdPessoa.UFNaturalidade))
            {
                worksheet.Cell("U" + k).ValueAsString = c.UpToPessoaByIdPessoa.UFNaturalidade.ToUpper(); // UFNaturalidade
            }

            PessoaEnderecoCollection p = new PessoaEnderecoCollection();
            p.Query.Where(p.Query.IdPessoa == idPessoa)
                   .OrderBy(p.Query.RecebeCorrespondencia.Descending);

            if (p.Query.Load()) {
                if (p.Count >= 1) { // se tem pelo menos 1 Endereço cadastrado
                    #region Info Endereço1
                    worksheet.Cell("V" + k).ValueAsString = !String.IsNullOrEmpty(p[0].str.Endereco) ? p[0].str.Endereco : ""; // Endereco
                    worksheet.Cell("W" + k).ValueAsString = !String.IsNullOrEmpty(p[0].str.Numero) ? p[0].str.Numero : ""; // Numero
                    worksheet.Cell("X" + k).ValueAsString = !String.IsNullOrEmpty(p[0].str.Complemento) ? p[0].str.Complemento : ""; // Complemento
                    worksheet.Cell("Y" + k).ValueAsString = !String.IsNullOrEmpty(p[0].str.Bairro) ? p[0].str.Bairro : ""; // Bairro
                    worksheet.Cell("Z" + k).ValueAsString = !String.IsNullOrEmpty(p[0].str.Cidade) ? p[0].str.Cidade : ""; // Cidade
                    worksheet.Cell("AA" + k).ValueAsString = !String.IsNullOrEmpty(p[0].str.Cep) ? p[0].str.Cep : ""; // CEP
                    worksheet.Cell("AB" + k).ValueAsString = !String.IsNullOrEmpty(p[0].str.Uf) ? p[0].str.Uf : ""; // UF
                    worksheet.Cell("AC" + k).ValueAsString = !String.IsNullOrEmpty(p[0].str.Pais) ? p[0].str.Pais : ""; // Pais
                    //
                    Pessoa pessoa = new Pessoa();
                    pessoa.LoadByPrimaryKey(p[0].IdPessoa.Value);

                    string fone = pessoa.FoneResidencial;
                    string email = pessoa.Email;

                    worksheet.Cell("AK" + k).ValueAsString = !String.IsNullOrEmpty(fone) ? fone : ""; // Fone
                    worksheet.Cell("AL" + k).ValueAsString = !String.IsNullOrEmpty(email) ? email : "";// Email
                    #endregion
                }
                if (p.Count >= 2) { // se tem pelo menos 2 Endereços cadastrados
                    #region Info Endereço2
                    worksheet.Cell("AD" + k).ValueAsString = !String.IsNullOrEmpty(p[1].str.Endereco) ? p[1].str.Endereco : ""; // EnderecoCom
                    worksheet.Cell("AE" + k).ValueAsString = !String.IsNullOrEmpty(p[1].str.Numero) ? p[1].str.Numero : ""; // NumeroCom
                    worksheet.Cell("AF" + k).ValueAsString = !String.IsNullOrEmpty(p[1].str.Complemento) ? p[1].str.Complemento : ""; // ComplementoCom
                    worksheet.Cell("AG" + k).ValueAsString = !String.IsNullOrEmpty(p[1].str.Bairro) ? p[1].str.Bairro : ""; // BairroCom
                    worksheet.Cell("AH" + k).ValueAsString = !String.IsNullOrEmpty(p[1].str.Cidade) ? p[1].str.Cidade : ""; // CidadeCom
                    worksheet.Cell("AI" + k).ValueAsString = !String.IsNullOrEmpty(p[1].str.Cep) ? p[1].str.Cep : ""; // CEPCom
                    worksheet.Cell("AJ" + k).ValueAsString = !String.IsNullOrEmpty(p[1].str.Uf) ? p[1].str.Uf : ""; // UFCom
                    //                    
                    Pessoa pessoa = new Pessoa();
                    pessoa.LoadByPrimaryKey(p[1].IdPessoa.Value);

                    string fone = pessoa.FoneComercial;

                    worksheet.Cell("AM" + k).ValueAsString = !String.IsNullOrEmpty(fone) ? fone : ""; // FoneCom
                    //worksheet.Cell("Z" + k).ValueAsString = !String.IsNullOrEmpty(p[1].str.Email) ? p[1].str.Email : ""; // EmailCom
                    #endregion
                }
            }
            #region Alinhamento e Fonte
            worksheet.Cell("A" + k).AlignmentHorizontal = Bytescout.Spreadsheet.Constants.AlignmentHorizontal.Centered;
            worksheet.Cell("B" + k).AlignmentHorizontal = Bytescout.Spreadsheet.Constants.AlignmentHorizontal.Left;
            worksheet.Cell("C" + k).AlignmentHorizontal = Bytescout.Spreadsheet.Constants.AlignmentHorizontal.Centered;
            worksheet.Cell("D" + k).AlignmentHorizontal = Bytescout.Spreadsheet.Constants.AlignmentHorizontal.Centered;
            worksheet.Cell("E" + k).AlignmentHorizontal = Bytescout.Spreadsheet.Constants.AlignmentHorizontal.Centered;
            worksheet.Cell("F" + k).AlignmentHorizontal = Bytescout.Spreadsheet.Constants.AlignmentHorizontal.Centered;
            worksheet.Cell("G" + k).AlignmentHorizontal = Bytescout.Spreadsheet.Constants.AlignmentHorizontal.Centered;
            worksheet.Cell("H" + k).AlignmentHorizontal = Bytescout.Spreadsheet.Constants.AlignmentHorizontal.Centered;
            worksheet.Cell("I" + k).AlignmentHorizontal = Bytescout.Spreadsheet.Constants.AlignmentHorizontal.Centered;
            worksheet.Cell("J" + k).AlignmentHorizontal = Bytescout.Spreadsheet.Constants.AlignmentHorizontal.Centered;
            worksheet.Cell("K" + k).AlignmentHorizontal = Bytescout.Spreadsheet.Constants.AlignmentHorizontal.Centered;
            worksheet.Cell("L" + k).AlignmentHorizontal = Bytescout.Spreadsheet.Constants.AlignmentHorizontal.Centered;
            worksheet.Cell("M" + k).AlignmentHorizontal = Bytescout.Spreadsheet.Constants.AlignmentHorizontal.Centered;
            worksheet.Cell("N" + k).AlignmentHorizontal = Bytescout.Spreadsheet.Constants.AlignmentHorizontal.Centered;
            worksheet.Cell("O" + k).AlignmentHorizontal = Bytescout.Spreadsheet.Constants.AlignmentHorizontal.Centered;
            worksheet.Cell("P" + k).AlignmentHorizontal = Bytescout.Spreadsheet.Constants.AlignmentHorizontal.Centered;
            worksheet.Cell("Q" + k).AlignmentHorizontal = Bytescout.Spreadsheet.Constants.AlignmentHorizontal.Centered;
            worksheet.Cell("R" + k).AlignmentHorizontal = Bytescout.Spreadsheet.Constants.AlignmentHorizontal.Centered;
            worksheet.Cell("S" + k).AlignmentHorizontal = Bytescout.Spreadsheet.Constants.AlignmentHorizontal.Centered;
            worksheet.Cell("T" + k).AlignmentHorizontal = Bytescout.Spreadsheet.Constants.AlignmentHorizontal.Centered;
            worksheet.Cell("U" + k).AlignmentHorizontal = Bytescout.Spreadsheet.Constants.AlignmentHorizontal.Centered;
            worksheet.Cell("V" + k).AlignmentHorizontal = Bytescout.Spreadsheet.Constants.AlignmentHorizontal.Centered;
            worksheet.Cell("W" + k).AlignmentHorizontal = Bytescout.Spreadsheet.Constants.AlignmentHorizontal.Centered;
            worksheet.Cell("X" + k).AlignmentHorizontal = Bytescout.Spreadsheet.Constants.AlignmentHorizontal.Centered;
            worksheet.Cell("Y" + k).AlignmentHorizontal = Bytescout.Spreadsheet.Constants.AlignmentHorizontal.Centered;
            worksheet.Cell("Z" + k).AlignmentHorizontal = Bytescout.Spreadsheet.Constants.AlignmentHorizontal.Centered;
            //
            worksheet.Cell("AA" + k).AlignmentHorizontal = Bytescout.Spreadsheet.Constants.AlignmentHorizontal.Centered;
            worksheet.Cell("AB" + k).AlignmentHorizontal = Bytescout.Spreadsheet.Constants.AlignmentHorizontal.Centered;
            worksheet.Cell("AC" + k).AlignmentHorizontal = Bytescout.Spreadsheet.Constants.AlignmentHorizontal.Centered;
            worksheet.Cell("AD" + k).AlignmentHorizontal = Bytescout.Spreadsheet.Constants.AlignmentHorizontal.Centered;
            worksheet.Cell("AE" + k).AlignmentHorizontal = Bytescout.Spreadsheet.Constants.AlignmentHorizontal.Centered;
            worksheet.Cell("AF" + k).AlignmentHorizontal = Bytescout.Spreadsheet.Constants.AlignmentHorizontal.Centered;
            worksheet.Cell("AG" + k).AlignmentHorizontal = Bytescout.Spreadsheet.Constants.AlignmentHorizontal.Centered;
            worksheet.Cell("AH" + k).AlignmentHorizontal = Bytescout.Spreadsheet.Constants.AlignmentHorizontal.Centered;
            worksheet.Cell("AI" + k).AlignmentHorizontal = Bytescout.Spreadsheet.Constants.AlignmentHorizontal.Centered;
            worksheet.Cell("AJ" + k).AlignmentHorizontal = Bytescout.Spreadsheet.Constants.AlignmentHorizontal.Centered;
            worksheet.Cell("AK" + k).AlignmentHorizontal = Bytescout.Spreadsheet.Constants.AlignmentHorizontal.Centered;
            worksheet.Cell("AL" + k).AlignmentHorizontal = Bytescout.Spreadsheet.Constants.AlignmentHorizontal.Centered;
            worksheet.Cell("AM" + k).AlignmentHorizontal = Bytescout.Spreadsheet.Constants.AlignmentHorizontal.Centered;
            worksheet.Cell("AN" + k).AlignmentHorizontal = Bytescout.Spreadsheet.Constants.AlignmentHorizontal.Centered;
            #endregion
            //
            worksheet.Rows[k].Font = new Font("Arial", 8, FontStyle.Regular);
        }
        #endregion

        //
        document.SaveToStreamXLS(msExcel);
        //
        document.Close();

        /* Necessário voltar o ponteiro do arquivo para o Inicio */
        msExcel.Seek(0, SeekOrigin.Begin);
        //
        return msExcel;
    }
       
    /// <summary>
    /// Retorna uma Planilha de Posição Fundo
    /// </summary>
    /// <param name="idsCarteiras"></param>     
    private MemoryStream GeraPosicaoFundo(List<int> idsCarteiras) {
        MemoryStream msExcel = new MemoryStream();

        Spreadsheet document = CarregaModelo(this.pathExcelPosicoes["Path_ImportacaoPosicaoFundo"]);

        Worksheet worksheet = document.Workbook.Worksheets[0];

        #region Preenche dados Planilha
        PosicaoFundoCollection p = new PosicaoFundoCollection();
        p.Query.Select(p.Query.IdCliente, p.Query.IdCarteira,
                       p.Query.ValorAplicacao, p.Query.DataAplicacao, p.Query.DataConversao,
                       p.Query.Quantidade, p.Query.CotaAplicacao, p.Query.DataUltimaCobrancaIR)
               .Where(p.Query.IdCliente.In(idsCarteiras) &&
                      p.Query.Quantidade != 0)
               .OrderBy(p.Query.IdCliente.Ascending,
                        p.Query.IdCarteira.Ascending,
                        p.Query.DataAplicacao.Ascending,
                        p.Query.DataConversao.Ascending);

        p.Query.Load();

        for (int i = 0; i < p.Count; i++) {
            int k = i + 2;
            worksheet.Cell("A" + k).ValueAsInteger = p[i].IdCliente.Value; // idCliente
            worksheet.Cell("B" + k).ValueAsInteger = p[i].IdCarteira.Value;  // idCarteira
            worksheet.Cell("C" + k).Value = Convert.ToDecimal(p[i].ValorAplicacao.Value.ToString("N2")); // valorAplicação
            worksheet.Cell("D" + k).ValueAsDateTime = p[i].DataAplicacao.Value; // dataAplicação
            worksheet.Cell("E" + k).ValueAsDateTime = p[i].DataConversao.Value; // dataConversão
            worksheet.Cell("F" + k).Value = Convert.ToDecimal(p[i].Quantidade.Value.ToString("N8")); // quantidade
            worksheet.Cell("G" + k).Value = Convert.ToDecimal(p[i].CotaAplicacao.Value.ToString("N8")); // cotaAplicação
            worksheet.Cell("H" + k).ValueAsDateTime = p[i].DataUltimaCobrancaIR.Value; // dataUltimaCobrança
            //
            // Alinhamento e Fonte
            worksheet.Cell("A" + k).AlignmentHorizontal = Bytescout.Spreadsheet.Constants.AlignmentHorizontal.Centered;
            worksheet.Cell("B" + k).AlignmentHorizontal = Bytescout.Spreadsheet.Constants.AlignmentHorizontal.Centered;
            worksheet.Cell("C" + k).AlignmentHorizontal = Bytescout.Spreadsheet.Constants.AlignmentHorizontal.Right;
            worksheet.Cell("D" + k).AlignmentHorizontal = Bytescout.Spreadsheet.Constants.AlignmentHorizontal.Right;
            worksheet.Cell("E" + k).AlignmentHorizontal = Bytescout.Spreadsheet.Constants.AlignmentHorizontal.Right;
            worksheet.Cell("F" + k).AlignmentHorizontal = Bytescout.Spreadsheet.Constants.AlignmentHorizontal.Right;
            worksheet.Cell("G" + k).AlignmentHorizontal = Bytescout.Spreadsheet.Constants.AlignmentHorizontal.Right;
            worksheet.Cell("H" + k).AlignmentHorizontal = Bytescout.Spreadsheet.Constants.AlignmentHorizontal.Right;
            //
            worksheet.Rows[k].Font = new Font("Arial", 8, FontStyle.Regular);
        }
        #endregion

        //
        document.SaveToStreamXLS(msExcel);
        //

        /* Necessário voltar o ponteiro do arquivo para o Inicio */
        msExcel.Seek(0, SeekOrigin.Begin);

        document.Close();
        //
        return msExcel;
    }

    /// <summary>
    /// Retorna uma Planilha de Posição BMF
    /// </summary>
    /// <param name="idsCarteiras"></param>
    private MemoryStream GeraPosicaoBMF(List<int> idsCarteiras) {
        MemoryStream msExcel = new MemoryStream();

        Spreadsheet document = CarregaModelo(this.pathExcelPosicoes["Path_ImportacaoPosicaoBMF"]);

        Worksheet worksheet = document.Workbook.Worksheets[0];

        #region Preenche dados Planilha
        PosicaoBMFCollection p = new PosicaoBMFCollection();
        p.Query.Select(p.Query.IdCliente, p.Query.IdAgente,
                       p.Query.CdAtivoBMF, p.Query.Serie, p.Query.Quantidade, p.Query.PUCustoLiquido, p.Query.TipoMercado)
               .Where(p.Query.IdCliente.In(idsCarteiras) &&
                      p.Query.Quantidade != 0)
               .OrderBy(p.Query.IdCliente.Ascending,
                        p.Query.IdAgente.Ascending,
                        p.Query.CdAtivoBMF.Ascending,
                        p.Query.Serie.Ascending);

        p.Query.Load();

        for (int i = 0; i < p.Count; i++) {
            int k = i + 2;
            worksheet.Cell("A" + k).ValueAsInteger = p[i].IdCliente.Value; // idCliente

            AgenteMercado agenteMercado = new AgenteMercado();
            List<esQueryItem> campos = new List<esQueryItem>();
            campos.Add(agenteMercado.Query.CodigoBovespa);
            campos.Add(agenteMercado.Query.Nome);
            agenteMercado.LoadByPrimaryKey(campos, p[i].IdAgente.Value);

            if (!agenteMercado.CodigoBMF.HasValue)
            {
                throw new Exception("Código BMF inexistente para o agente de mercado " + agenteMercado.Nome);
            }

            worksheet.Cell("B" + k).ValueAsInteger = agenteMercado.CodigoBMF.Value;  // codigoBMF

            worksheet.Cell("C" + k).ValueAsString = p[i].CdAtivoBMF.Trim(); // cdativoBMF
            worksheet.Cell("D" + k).ValueAsString = p[i].Serie.Trim(); // serie
            worksheet.Cell("E" + k).ValueAsInteger = Convert.ToInt32(p[i].Quantidade.Value); // quantidade            
            if (p[i].PUCustoLiquido.HasValue) {
                if (p[i].TipoMercado.Value == (byte)TipoMercadoBMF.Futuro) { // Se for Mercado Futuro = 0
                    worksheet.Cell("F" + k).ValueAsDouble = 0.00f; // puCusto
                }
                else {
                    worksheet.Cell("F" + k).Value = Convert.ToDecimal(p[i].PUCustoLiquido.Value.ToString("N4")); // puCusto
                }
            }
            // Alinhamento e Fonte
            worksheet.Cell("A" + k).AlignmentHorizontal = Bytescout.Spreadsheet.Constants.AlignmentHorizontal.Centered;
            worksheet.Cell("B" + k).AlignmentHorizontal = Bytescout.Spreadsheet.Constants.AlignmentHorizontal.Centered;
            worksheet.Cell("C" + k).AlignmentHorizontal = Bytescout.Spreadsheet.Constants.AlignmentHorizontal.Left;
            worksheet.Cell("D" + k).AlignmentHorizontal = Bytescout.Spreadsheet.Constants.AlignmentHorizontal.Left;
            worksheet.Cell("E" + k).AlignmentHorizontal = Bytescout.Spreadsheet.Constants.AlignmentHorizontal.Right;
            worksheet.Cell("F" + k).AlignmentHorizontal = Bytescout.Spreadsheet.Constants.AlignmentHorizontal.Right;
            //
            worksheet.Rows[k].Font = new Font("Arial", 8, FontStyle.Regular);
        }
        #endregion

        //
        document.SaveToStreamXLS(msExcel);
        //
        /* Necessário voltar o ponteiro do arquivo para o Inicio */
        msExcel.Seek(0, SeekOrigin.Begin);

        document.Close();
        //
        return msExcel;
    }

    /// <summary>
    /// Retorna uma Planilha de Posição Bolsa
    /// </summary>
    /// <param name="idsCarteiras"></param>
    private MemoryStream GeraPosicaoBolsa(List<int> idsCarteiras) {
        MemoryStream msExcel = new MemoryStream();

        Spreadsheet document = CarregaModelo(this.pathExcelPosicoes["Path_ImportacaoPosicaoBolsa"]);

        Worksheet worksheet = document.Workbook.Worksheets[0];

        #region Preenche dados Planilha
        PosicaoBolsaCollection p = new PosicaoBolsaCollection();
        p.Query.Select(p.Query.IdCliente, p.Query.IdAgente,
                       p.Query.CdAtivoBolsa, p.Query.Quantidade, p.Query.PUCustoLiquido)
               .Where(p.Query.IdCliente.In(idsCarteiras) &&
                      p.Query.TipoMercado.NotIn(TipoMercadoBolsa.Termo) &&
                      p.Query.Quantidade != 0)
               .OrderBy(p.Query.IdCliente.Ascending,
                        p.Query.IdAgente.Ascending,
                        p.Query.CdAtivoBolsa.Ascending);

        p.Query.Load();

        for (int i = 0; i < p.Count; i++) {
            int k = i + 2;
            worksheet.Cell("A" + k).ValueAsInteger = p[i].IdCliente.Value; // idCliente

            AgenteMercado agenteMercado = new AgenteMercado();
            List<esQueryItem> campos = new List<esQueryItem>();
            campos.Add(agenteMercado.Query.CodigoBovespa);
            campos.Add(agenteMercado.Query.Nome);
            agenteMercado.LoadByPrimaryKey(campos, p[i].IdAgente.Value);

            if (!agenteMercado.CodigoBovespa.HasValue)
            {
                throw new Exception("Código Bovespa inexistente para o agente de mercado " + agenteMercado.Nome);
            }

            worksheet.Cell("B" + k).ValueAsInteger = agenteMercado.CodigoBovespa.Value;  // codigoBovespa

            worksheet.Cell("C" + k).ValueAsString = p[i].CdAtivoBolsa.Trim(); // cdativo
            worksheet.Cell("D" + k).ValueAsInteger = Convert.ToInt32(p[i].Quantidade.Value); // quantidade            
            if (p[i].PUCustoLiquido.HasValue) {
                worksheet.Cell("E" + k).Value = Convert.ToDecimal(p[i].PUCustoLiquido.Value.ToString("N8")); // puCusto
            }
            // Alinhamento e Fonte
            worksheet.Cell("A" + k).AlignmentHorizontal = Bytescout.Spreadsheet.Constants.AlignmentHorizontal.Centered;
            worksheet.Cell("B" + k).AlignmentHorizontal = Bytescout.Spreadsheet.Constants.AlignmentHorizontal.Centered;
            worksheet.Cell("C" + k).AlignmentHorizontal = Bytescout.Spreadsheet.Constants.AlignmentHorizontal.Left;
            worksheet.Cell("D" + k).AlignmentHorizontal = Bytescout.Spreadsheet.Constants.AlignmentHorizontal.Right;
            worksheet.Cell("E" + k).AlignmentHorizontal = Bytescout.Spreadsheet.Constants.AlignmentHorizontal.Left;
            //
            worksheet.Rows[k].Font = new Font("Arial", 8, FontStyle.Regular);
        }
        #endregion

        //
        document.SaveToStreamXLS(msExcel);

        /* Necessário voltar o ponteiro do arquivo para o Inicio */
        msExcel.Seek(0, SeekOrigin.Begin);
        //
        document.Close();
        //
        return msExcel;
    }

    /// <summary>
    /// Retorna uma Planilha de Posição Termo
    /// </summary>
    /// <param name="idsCarteiras"></param>
    private MemoryStream GeraPosicaoTermo(List<int> idsCarteiras)
    {
        MemoryStream msExcel = new MemoryStream();

        Spreadsheet document = CarregaModelo(this.pathExcelPosicoes["Path_ImportacaoPosicaoTermoBolsa"]);

        Worksheet worksheet = document.Workbook.Worksheets[0];

        #region Preenche dados Planilha
        PosicaoTermoBolsaCollection p = new PosicaoTermoBolsaCollection();
        p.Query.Select(p.Query.IdIndice,
                       p.Query.IdCliente,
                       p.Query.IdAgente,
                       p.Query.CdAtivoBolsa,
                       p.Query.Quantidade,
                       p.Query.PUTermo, p.Query.PUTermoLiquido,
                       p.Query.ValorTermo, p.Query.ValorTermoLiquido,
                       p.Query.DataOperacao, p.Query.DataVencimento,
                       p.Query.PUCustoLiquidoAcao, p.Query.NumeroContrato)
               .Where(p.Query.IdCliente.In(idsCarteiras) &&
                      p.Query.Quantidade != 0)
               .OrderBy(p.Query.IdCliente.Ascending,
                        p.Query.IdAgente.Ascending,
                        p.Query.CdAtivoBolsa.Ascending);

        p.Query.Load();

        for (int i = 0; i < p.Count; i++)
        {
            int idCliente = p[i].IdCliente.Value;

            Cliente cliente = new Cliente();
            List<esQueryItem> campos = new List<esQueryItem>();
            campos.Add(cliente.Query.DataDia);
            cliente.LoadByPrimaryKey(campos, idCliente);

            int k = i + 2;
            worksheet.Cell("A" + k).ValueAsDateTime = cliente.DataDia.Value; // DataPosicao

            if (p[i].IdIndice.HasValue)
            {
                worksheet.Cell("B" + k).ValueAsInteger = p[i].IdIndice.Value;  // IdIndice
            }

            worksheet.Cell("C" + k).ValueAsInteger = idCliente; // IdCliente

            AgenteMercado agenteMercado = new AgenteMercado();
            campos = new List<esQueryItem>();
            campos.Add(agenteMercado.Query.CodigoBovespa);
            campos.Add(agenteMercado.Query.Nome);
            agenteMercado.LoadByPrimaryKey(campos, p[i].IdAgente.Value);

            if (!agenteMercado.CodigoBovespa.HasValue)
            {
                throw new Exception("Código Bovespa inexistente para o agente de mercado " + agenteMercado.Nome);
            }

            worksheet.Cell("D" + k).ValueAsInteger = agenteMercado.CodigoBovespa.Value;  // codigoBovespa

            worksheet.Cell("E" + k).ValueAsString = AtivoBolsa.RetornaCdAtivoBolsaAcao(Convert.ToString(p[i].CdAtivoBolsa.Trim())); // CdAtivoBolsa
            worksheet.Cell("F" + k).ValueAsInteger = Convert.ToInt32(p[i].Quantidade.Value); // Quantidade
            worksheet.Cell("G" + k).Value = Convert.ToDecimal(p[i].PUTermo.Value); // PuTermo
            worksheet.Cell("H" + k).Value = Convert.ToDecimal(p[i].PUTermoLiquido.Value); // PuTermoLiquido
            worksheet.Cell("I" + k).Value = Convert.ToDecimal(p[i].ValorTermo.Value); // ValorTermo
            worksheet.Cell("J" + k).Value = Convert.ToInt32(p[i].ValorTermoLiquido.Value); // ValorTermoLiquido
            worksheet.Cell("K" + k).ValueAsDateTime = Convert.ToDateTime(p[i].DataOperacao.Value); // DataOperacao
            worksheet.Cell("L" + k).ValueAsDateTime = Convert.ToDateTime(p[i].DataVencimento.Value); // DataVencimento
            worksheet.Cell("M" + k).Value = Convert.ToDecimal(p[i].PUCustoLiquidoAcao.Value); // PuCustoLiquidoAcao

            if (!String.IsNullOrEmpty(p[i].NumeroContrato))
            {
                worksheet.Cell("N" + k).ValueAsInteger = Convert.ToInt32(p[i].NumeroContrato.Trim()); // NumeroContrato
            }

            // Alinhamento e Fonte
            worksheet.Cell("A" + k).AlignmentHorizontal = Bytescout.Spreadsheet.Constants.AlignmentHorizontal.Centered;
            worksheet.Cell("B" + k).AlignmentHorizontal = Bytescout.Spreadsheet.Constants.AlignmentHorizontal.Right;
            worksheet.Cell("C" + k).AlignmentHorizontal = Bytescout.Spreadsheet.Constants.AlignmentHorizontal.Left;
            worksheet.Cell("D" + k).AlignmentHorizontal = Bytescout.Spreadsheet.Constants.AlignmentHorizontal.Centered;
            worksheet.Cell("E" + k).AlignmentHorizontal = Bytescout.Spreadsheet.Constants.AlignmentHorizontal.Left;
            worksheet.Cell("F" + k).AlignmentHorizontal = Bytescout.Spreadsheet.Constants.AlignmentHorizontal.Right;
            worksheet.Cell("G" + k).AlignmentHorizontal = Bytescout.Spreadsheet.Constants.AlignmentHorizontal.Right;
            worksheet.Cell("H" + k).AlignmentHorizontal = Bytescout.Spreadsheet.Constants.AlignmentHorizontal.Right;
            worksheet.Cell("I" + k).AlignmentHorizontal = Bytescout.Spreadsheet.Constants.AlignmentHorizontal.Right;
            worksheet.Cell("J" + k).AlignmentHorizontal = Bytescout.Spreadsheet.Constants.AlignmentHorizontal.Right;
            worksheet.Cell("K" + k).AlignmentHorizontal = Bytescout.Spreadsheet.Constants.AlignmentHorizontal.Centered;
            worksheet.Cell("L" + k).AlignmentHorizontal = Bytescout.Spreadsheet.Constants.AlignmentHorizontal.Centered;
            worksheet.Cell("M" + k).AlignmentHorizontal = Bytescout.Spreadsheet.Constants.AlignmentHorizontal.Right;
            worksheet.Cell("N" + k).AlignmentHorizontal = Bytescout.Spreadsheet.Constants.AlignmentHorizontal.Centered;
            //
            worksheet.Rows[k].Font = new Font("Arial", 8, FontStyle.Regular);
        }
        #endregion

        //
        document.SaveToStreamXLS(msExcel);
        //
        document.Close();

        /* Necessário voltar o ponteiro do arquivo para o Inicio */
        msExcel.Seek(0, SeekOrigin.Begin);
        //
        return msExcel;
    }

    /// <summary>
    /// Retorna uma Planilha de Posição Emprestimo Bolsa
    /// </summary>
    /// <param name="idsCarteiras"></param>
    private MemoryStream GeraPosicaoEmprestimoBolsa(List<int> idsCarteiras)
    {
        MemoryStream msExcel = new MemoryStream();

        Spreadsheet document = CarregaModelo(this.pathExcelPosicoes["Path_ImportacaoPosicaoEmprestimoBolsa"]);

        Worksheet worksheet = document.Workbook.Worksheets[0];

        #region Preenche dados Planilha
        PosicaoEmprestimoBolsaCollection p = new PosicaoEmprestimoBolsaCollection();
        p.Query.Where(p.Query.IdCliente.In(idsCarteiras) &&
                      p.Query.Quantidade != 0);
        p.Query.OrderBy(p.Query.IdCliente.Ascending,
                        p.Query.PontaEmprestimo.Ascending,
                        p.Query.IdAgente.Ascending,
                        p.Query.CdAtivoBolsa.Ascending);

        p.Query.Load();

        for (int i = 0; i < p.Count; i++)
        {
            int k = i + 2;

            int idCliente = p[i].IdCliente.Value;

            worksheet.Cell("A" + k).ValueAsInteger = idCliente; // IdCliente

            AgenteMercado agenteMercado = new AgenteMercado();
            List<esQueryItem> campos = new List<esQueryItem>();
            campos.Add(agenteMercado.Query.CodigoBovespa);
            campos.Add(agenteMercado.Query.Nome);
            agenteMercado.LoadByPrimaryKey(campos, p[i].IdAgente.Value);

            if (!agenteMercado.CodigoBovespa.HasValue)
            {
                throw new Exception("Código Bovespa inexistente para o agente de mercado " + agenteMercado.Nome);
            }

            worksheet.Cell("B" + k).ValueAsInteger = agenteMercado.CodigoBovespa.Value;  // codigoBovespa

            worksheet.Cell("C" + k).ValueAsString = Convert.ToString(p[i].CdAtivoBolsa.Trim()); // cdAtivoBolsa
            worksheet.Cell("D" + k).ValueAsInteger = Convert.ToInt32(p[i].Quantidade.Value); // quantidade
            worksheet.Cell("E" + k).Value = Convert.ToDecimal(p[i].PULiquidoOriginal.Value); // puLiquidoOriginal            
            worksheet.Cell("F" + k).Value = Convert.ToDecimal(p[i].ValorBase.Value); // valorBase
            worksheet.Cell("G" + k).ValueAsInteger = p[i].PontaEmprestimo.Value; // pontaEmprestimo
            worksheet.Cell("H" + k).ValueAsDateTime = Convert.ToDateTime(p[i].DataRegistro.Value); // DataOperacao
            worksheet.Cell("I" + k).ValueAsDateTime = Convert.ToDateTime(p[i].DataVencimento.Value); // DataVencimento
            worksheet.Cell("J" + k).Value = Convert.ToDecimal(p[i].TaxaOperacao.Value); // TaxaOperacao
            worksheet.Cell("K" + k).Value = Convert.ToDecimal(p[i].TaxaComissao.Value); // TaxaOperacao

            if (p[i].NumeroContrato.HasValue)
            {
                worksheet.Cell("L" + k).ValueAsInteger = p[i].NumeroContrato.Value; // NumeroContrato
            }

            // Alinhamento e Fonte
            worksheet.Cell("A" + k).AlignmentHorizontal = Bytescout.Spreadsheet.Constants.AlignmentHorizontal.Left;
            worksheet.Cell("B" + k).AlignmentHorizontal = Bytescout.Spreadsheet.Constants.AlignmentHorizontal.Centered;
            worksheet.Cell("C" + k).AlignmentHorizontal = Bytescout.Spreadsheet.Constants.AlignmentHorizontal.Left;
            worksheet.Cell("D" + k).AlignmentHorizontal = Bytescout.Spreadsheet.Constants.AlignmentHorizontal.Right;
            worksheet.Cell("E" + k).AlignmentHorizontal = Bytescout.Spreadsheet.Constants.AlignmentHorizontal.Right;
            worksheet.Cell("F" + k).AlignmentHorizontal = Bytescout.Spreadsheet.Constants.AlignmentHorizontal.Right;

            worksheet.Cell("G" + k).AlignmentHorizontal = Bytescout.Spreadsheet.Constants.AlignmentHorizontal.Centered;
            worksheet.Cell("H" + k).AlignmentHorizontal = Bytescout.Spreadsheet.Constants.AlignmentHorizontal.Centered;
            worksheet.Cell("I" + k).AlignmentHorizontal = Bytescout.Spreadsheet.Constants.AlignmentHorizontal.Centered;

            worksheet.Cell("J" + k).AlignmentHorizontal = Bytescout.Spreadsheet.Constants.AlignmentHorizontal.Right;
            worksheet.Cell("K" + k).AlignmentHorizontal = Bytescout.Spreadsheet.Constants.AlignmentHorizontal.Right;
            worksheet.Cell("L" + k).AlignmentHorizontal = Bytescout.Spreadsheet.Constants.AlignmentHorizontal.Centered;
            //
            worksheet.Rows[k].Font = new Font("Arial", 8, FontStyle.Regular);
        }
        #endregion

        //
        document.SaveToStreamXLS(msExcel);
        //
        /* Necessário voltar o ponteiro do arquivo para o Inicio */
        msExcel.Seek(0, SeekOrigin.Begin);

        document.Close();
        //
        return msExcel;
    }

    /// <summary>
    /// Retorna uma Planilha de Posição Cotista
    /// </summary>
    /// <param name="idsCarteiras"></param>
    private MemoryStream GeraPosicaoCotista(List<int> idsCarteiras) {
        MemoryStream msExcel = new MemoryStream();

        Spreadsheet document = CarregaModelo(this.pathExcelPosicoes["Path_ImportacaoPosicaoCotista"]);

        Worksheet worksheet = document.Workbook.Worksheets[0];

        #region Preenche dados Planilha
        PosicaoCotistaCollection p = new PosicaoCotistaCollection();

        p.Query.Select(p.Query.IdCotista, p.Query.IdCarteira,
                       p.Query.ValorAplicacao, p.Query.DataAplicacao, p.Query.DataConversao,
                       p.Query.Quantidade, p.Query.CotaAplicacao, p.Query.DataUltimaCobrancaIR)
               .Where(p.Query.IdCarteira.In(idsCarteiras) &&
                      p.Query.Quantidade != 0)
               .OrderBy(p.Query.IdCotista.Ascending,
                        p.Query.IdCarteira.Ascending,
                        p.Query.DataAplicacao.Ascending,
                        p.Query.DataConversao.Ascending);

        p.Query.Load();

        for (int i = 0; i < p.Count; i++) {
            int k = i + 2;
            worksheet.Cell("A" + k).ValueAsInteger = p[i].IdCotista.Value; // idCotista
            worksheet.Cell("B" + k).ValueAsInteger = p[i].IdCarteira.Value;  // idCarteira
            worksheet.Cell("C" + k).Value = Convert.ToDecimal(p[i].ValorAplicacao.Value.ToString("N2")); // valorAplicação
            worksheet.Cell("D" + k).ValueAsDateTime = p[i].DataAplicacao.Value; // dataAplicação
            worksheet.Cell("E" + k).ValueAsDateTime = p[i].DataConversao.Value; // dataConversão
            worksheet.Cell("F" + k).Value = Convert.ToDecimal(p[i].Quantidade.Value.ToString("N8")); // quantidade
            worksheet.Cell("G" + k).Value = Convert.ToDecimal(p[i].CotaAplicacao.Value.ToString("N8")); // cotaAplicação
            worksheet.Cell("H" + k).ValueAsDateTime = p[i].DataUltimaCobrancaIR.Value; // dataUltimaCobrança
            //
            // Alinhamento e Fonte
            worksheet.Cell("A" + k).AlignmentHorizontal = Bytescout.Spreadsheet.Constants.AlignmentHorizontal.Right;
            worksheet.Cell("B" + k).AlignmentHorizontal = Bytescout.Spreadsheet.Constants.AlignmentHorizontal.Right;
            worksheet.Cell("C" + k).AlignmentHorizontal = Bytescout.Spreadsheet.Constants.AlignmentHorizontal.Right;
            worksheet.Cell("D" + k).AlignmentHorizontal = Bytescout.Spreadsheet.Constants.AlignmentHorizontal.Right;
            worksheet.Cell("E" + k).AlignmentHorizontal = Bytescout.Spreadsheet.Constants.AlignmentHorizontal.Right;
            worksheet.Cell("F" + k).AlignmentHorizontal = Bytescout.Spreadsheet.Constants.AlignmentHorizontal.Right;
            worksheet.Cell("G" + k).AlignmentHorizontal = Bytescout.Spreadsheet.Constants.AlignmentHorizontal.Right;
            worksheet.Cell("H" + k).AlignmentHorizontal = Bytescout.Spreadsheet.Constants.AlignmentHorizontal.Right;
            //
            worksheet.Rows[k].Font = new Font("Arial", 8, FontStyle.Regular);
        }
        #endregion

        //
        document.SaveToStreamXLS(msExcel);
        //
        /* Necessário voltar o ponteiro do arquivo para o Inicio */
        msExcel.Seek(0, SeekOrigin.Begin);

        document.Close();
        //
        return msExcel;
    }

    /// <summary>
    /// Retorna uma Planilha de Posição Renda Fixa
    /// </summary>
    /// <param name="idsCarteiras"></param>
    private MemoryStream GeraPosicaoRendaFixa(List<int> idsCarteiras) {
        MemoryStream msExcel = new MemoryStream();

        Spreadsheet document = CarregaModelo(this.pathExcelPosicoes["Path_ImportacaoPosicaoRendaFixa"]);

        Worksheet worksheet = document.Workbook.Worksheets[0];

        #region Preenche dados Planilha
        PosicaoRendaFixaCollection p = new PosicaoRendaFixaCollection();
        p.Query.Select(p.Query.IdCliente, p.Query.IdTitulo,
                       p.Query.TipoOperacao, p.Query.Quantidade, p.Query.QuantidadeBloqueada,
                       p.Query.DataOperacao, p.Query.PUOperacao, p.Query.TaxaOperacao,
                       p.Query.PUMercado, p.Query.DataVolta,
                       p.Query.TaxaVolta, p.Query.PUVolta)
               .Where(p.Query.IdCliente.In(idsCarteiras) &&
                      p.Query.Quantidade != 0)
               .OrderBy(p.Query.IdCliente.Ascending,
                        p.Query.IdTitulo.Ascending,
                        p.Query.DataOperacao.Ascending);

        p.Query.Load();

        for (int i = 0; i < p.Count; i++) {
            int k = i + 2;
            worksheet.Cell("A" + k).ValueAsInteger = p[i].IdCliente.Value; // idCliente
            worksheet.Cell("B" + k).ValueAsInteger = p[i].IdTitulo.Value;  // idTitulo
            worksheet.Cell("C" + k).ValueAsInteger = p[i].TipoOperacao.Value; // tipoOperacao            
            worksheet.Cell("D" + k).Value = Convert.ToDecimal(p[i].Quantidade.Value.ToString("N2")); // quantidade
            worksheet.Cell("E" + k).Value = Convert.ToDecimal(p[i].QuantidadeBloqueada.Value.ToString("N2")); // quantidadeBloqueada
            worksheet.Cell("F" + k).ValueAsDateTime = p[i].DataOperacao.Value; // dataOperacao
            worksheet.Cell("G" + k).Value = Convert.ToDecimal(p[i].PUOperacao.Value.ToString("N8")); // puOperacao

            if (p[i].TaxaOperacao.HasValue) {
                worksheet.Cell("H" + k).Value = Convert.ToDecimal(p[i].TaxaOperacao.Value.ToString("N4")); // taxaOperacao
            }
            else {
                worksheet.Cell("H" + k).ValueAsInteger = 0; // taxaOperacao
            }

            worksheet.Cell("I" + k).Value = Convert.ToDecimal(p[i].PUMercado.Value.ToString("N8")); // puMercado

            if ((p[i].TipoOperacao.Value == (byte)TipoOperacaoTitulo.CompraRevenda ||
                p[i].TipoOperacao.Value == (byte)TipoOperacaoTitulo.VendaRecompra) &&
                p[i].DataVolta.HasValue) {
                worksheet.Cell("J" + k).ValueAsDateTime = p[i].DataVolta.Value; // dataVolta
                worksheet.Cell("K" + k).Value = p[i].TaxaVolta.Value; // taxaVolta
                worksheet.Cell("L" + k).Value = p[i].PUVolta.Value; // puVolta
            }
            // Alinhamento e Fonte
            worksheet.Cell("A" + k).AlignmentHorizontal = Bytescout.Spreadsheet.Constants.AlignmentHorizontal.Centered;
            worksheet.Cell("B" + k).AlignmentHorizontal = Bytescout.Spreadsheet.Constants.AlignmentHorizontal.Centered;
            worksheet.Cell("C" + k).AlignmentHorizontal = Bytescout.Spreadsheet.Constants.AlignmentHorizontal.Left;
            worksheet.Cell("D" + k).AlignmentHorizontal = Bytescout.Spreadsheet.Constants.AlignmentHorizontal.Right;
            worksheet.Cell("E" + k).AlignmentHorizontal = Bytescout.Spreadsheet.Constants.AlignmentHorizontal.Right;
            worksheet.Cell("F" + k).AlignmentHorizontal = Bytescout.Spreadsheet.Constants.AlignmentHorizontal.Left;
            worksheet.Cell("G" + k).AlignmentHorizontal = Bytescout.Spreadsheet.Constants.AlignmentHorizontal.Right;
            worksheet.Cell("H" + k).AlignmentHorizontal = Bytescout.Spreadsheet.Constants.AlignmentHorizontal.Right;
            worksheet.Cell("I" + k).AlignmentHorizontal = Bytescout.Spreadsheet.Constants.AlignmentHorizontal.Right;
            worksheet.Cell("J" + k).AlignmentHorizontal = Bytescout.Spreadsheet.Constants.AlignmentHorizontal.Left;
            worksheet.Cell("K" + k).AlignmentHorizontal = Bytescout.Spreadsheet.Constants.AlignmentHorizontal.Right;
            worksheet.Cell("L" + k).AlignmentHorizontal = Bytescout.Spreadsheet.Constants.AlignmentHorizontal.Right;
            //
            worksheet.Rows[k].Font = new Font("Arial", 8, FontStyle.Regular);
        }
        #endregion

        //
        document.SaveToStreamXLS(msExcel);

        /* Necessário voltar o ponteiro do arquivo para o Inicio */
        msExcel.Seek(0, SeekOrigin.Begin);
        //
        document.Close();
        //
        return msExcel;
    }

    /// <summary>
    /// Retorna uma Planilha de Liquidacao
    /// </summary>
    /// <param name="idsCarteiras"></param>
    private MemoryStream GeraLiquidacao(List<int> idsCarteiras) {
        MemoryStream msExcel = new MemoryStream();

        Spreadsheet document = CarregaModelo(this.pathExcelPosicoes["Path_ImportacaoLiquidacao"]);

        Worksheet worksheet = document.Workbook.Worksheets[0];

        #region Preenche dados Planilha (Liquidacao)
        LiquidacaoQuery liquidacaoQuery = new LiquidacaoQuery("L");
        ClienteQuery clienteQuery = new ClienteQuery("C");
        LiquidacaoCollection p = new LiquidacaoCollection();
        liquidacaoQuery.InnerJoin(clienteQuery).On(clienteQuery.IdCliente == liquidacaoQuery.IdCliente);
        liquidacaoQuery.Where(liquidacaoQuery.IdCliente.In(idsCarteiras) &&
                              liquidacaoQuery.DataVencimento.GreaterThan(clienteQuery.DataDia),
                              liquidacaoQuery.DataLancamento.LessThanOrEqual(clienteQuery.DataDia));
        liquidacaoQuery.OrderBy(liquidacaoQuery.IdCliente.Ascending,
                                liquidacaoQuery.DataLancamento.Ascending,
                                liquidacaoQuery.DataVencimento.Ascending);

        p.Load(liquidacaoQuery);

        int cont = 0;
        for (int i = 0; i < p.Count; i++) {
            int k = i + 2;
            worksheet.Cell("A" + k).ValueAsInteger = p[i].IdCliente.Value; // idCliente
            worksheet.Cell("B" + k).ValueAsInteger = 2;  // Tipo            
            worksheet.Cell("C" + k).ValueAsDateTime = p[i].DataLancamento.Value; // dataLancamento
            worksheet.Cell("D" + k).ValueAsDateTime = p[i].DataVencimento.Value; // dataVencimento
            worksheet.Cell("E" + k).ValueAsString = p[i].Descricao; // descricao
            worksheet.Cell("F" + k).Value = Convert.ToDecimal(p[i].Valor.Value.ToString("N2")); // valor

            // Alinhamento e Fonte
            worksheet.Cell("A" + k).AlignmentHorizontal = Bytescout.Spreadsheet.Constants.AlignmentHorizontal.Centered;
            worksheet.Cell("B" + k).AlignmentHorizontal = Bytescout.Spreadsheet.Constants.AlignmentHorizontal.Centered;
            worksheet.Cell("C" + k).AlignmentHorizontal = Bytescout.Spreadsheet.Constants.AlignmentHorizontal.Centered;
            worksheet.Cell("D" + k).AlignmentHorizontal = Bytescout.Spreadsheet.Constants.AlignmentHorizontal.Centered;
            worksheet.Cell("E" + k).AlignmentHorizontal = Bytescout.Spreadsheet.Constants.AlignmentHorizontal.Left;
            worksheet.Cell("F" + k).AlignmentHorizontal = Bytescout.Spreadsheet.Constants.AlignmentHorizontal.Right;
            //
            worksheet.Rows[k].Font = new Font("Arial", 8, FontStyle.Regular);

            cont = k;
        }
        #endregion

        #region Preenche dados Planilha (SaldoCaixa)
        SaldoCaixaCollection saldoCaixaCollection = new SaldoCaixaCollection();

        SaldoCaixaQuery saldoCaixaQuery = new SaldoCaixaQuery("S");
        clienteQuery = new ClienteQuery("C");
        saldoCaixaQuery.Select(saldoCaixaQuery.IdCliente,
                               clienteQuery.DataDia,
                               saldoCaixaQuery.SaldoFechamento.Sum());
        saldoCaixaQuery.InnerJoin(clienteQuery).On(clienteQuery.IdCliente == saldoCaixaQuery.IdCliente);
        saldoCaixaQuery.Where(saldoCaixaQuery.IdCliente.In(idsCarteiras),
                              saldoCaixaQuery.Data.Equal(clienteQuery.DataDia));
        saldoCaixaQuery.GroupBy(saldoCaixaQuery.IdCliente,
                                clienteQuery.DataDia);
        saldoCaixaQuery.OrderBy(saldoCaixaQuery.IdCliente.Ascending);

        saldoCaixaCollection.Load(saldoCaixaQuery);

        cont++;
        foreach (SaldoCaixa saldoCaixa in saldoCaixaCollection) {
            decimal saldo = 0;
            if (saldoCaixa.SaldoFechamento.HasValue) {
                saldo = saldoCaixa.SaldoFechamento.Value;
            }

            worksheet.Cell("A" + cont).ValueAsInteger = saldoCaixa.IdCliente.Value; // idCliente
            worksheet.Cell("B" + cont).ValueAsInteger = 1;  // Tipo            
            worksheet.Cell("C" + cont).ValueAsDateTime = Convert.ToDateTime(saldoCaixa.GetColumn(ClienteMetadata.ColumnNames.DataDia)); // dataLancamento
            worksheet.Cell("D" + cont).ValueAsDateTime = Convert.ToDateTime(saldoCaixa.GetColumn(ClienteMetadata.ColumnNames.DataDia)); // dataVencimento
            worksheet.Cell("E" + cont).ValueAsString = "SALDO CAIXA ABERTURA"; // descricao
            worksheet.Cell("F" + cont).Value = saldo; // valor

            // Alinhamento e Fonte
            worksheet.Cell("A" + cont).AlignmentHorizontal = Bytescout.Spreadsheet.Constants.AlignmentHorizontal.Centered;
            worksheet.Cell("B" + cont).AlignmentHorizontal = Bytescout.Spreadsheet.Constants.AlignmentHorizontal.Centered;
            worksheet.Cell("C" + cont).AlignmentHorizontal = Bytescout.Spreadsheet.Constants.AlignmentHorizontal.Centered;
            worksheet.Cell("D" + cont).AlignmentHorizontal = Bytescout.Spreadsheet.Constants.AlignmentHorizontal.Centered;
            worksheet.Cell("E" + cont).AlignmentHorizontal = Bytescout.Spreadsheet.Constants.AlignmentHorizontal.Left;
            worksheet.Cell("F" + cont).AlignmentHorizontal = Bytescout.Spreadsheet.Constants.AlignmentHorizontal.Right;
            //
            worksheet.Rows[cont].Font = new Font("Arial", 8, FontStyle.Regular);

            cont++;

            worksheet.Cell("A" + cont).ValueAsInteger = saldoCaixa.IdCliente.Value; // idCliente
            worksheet.Cell("B" + cont).ValueAsInteger = 1;  // Tipo            
            worksheet.Cell("C" + cont).ValueAsDateTime = Convert.ToDateTime(saldoCaixa.GetColumn(ClienteMetadata.ColumnNames.DataDia)); // dataLancamento
            worksheet.Cell("D" + cont).ValueAsDateTime = Convert.ToDateTime(saldoCaixa.GetColumn(ClienteMetadata.ColumnNames.DataDia)); // dataVencimento
            worksheet.Cell("E" + cont).ValueAsString = "SALDO CAIXA FECHAMENTO"; // descricao
            worksheet.Cell("F" + cont).Value = saldo; // valor

            // Alinhamento e Fonte
            worksheet.Cell("A" + cont).AlignmentHorizontal = Bytescout.Spreadsheet.Constants.AlignmentHorizontal.Centered;
            worksheet.Cell("B" + cont).AlignmentHorizontal = Bytescout.Spreadsheet.Constants.AlignmentHorizontal.Centered;
            worksheet.Cell("C" + cont).AlignmentHorizontal = Bytescout.Spreadsheet.Constants.AlignmentHorizontal.Centered;
            worksheet.Cell("D" + cont).AlignmentHorizontal = Bytescout.Spreadsheet.Constants.AlignmentHorizontal.Centered;
            worksheet.Cell("E" + cont).AlignmentHorizontal = Bytescout.Spreadsheet.Constants.AlignmentHorizontal.Left;
            worksheet.Cell("F" + cont).AlignmentHorizontal = Bytescout.Spreadsheet.Constants.AlignmentHorizontal.Right;
            //
            worksheet.Rows[cont].Font = new Font("Arial", 8, FontStyle.Regular);

            cont++;
        }
        #endregion
        //
        document.SaveToStreamXLS(msExcel);
        //
        /* Necessário voltar o ponteiro do arquivo para o Inicio */
        msExcel.Seek(0, SeekOrigin.Begin);

        document.Close();
        //
        return msExcel;
    }
}