﻿using System;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using Financial.Web.Common;
using System.Collections.Generic;
using Bytescout.Spreadsheet;

/// <summary>
/// Classe Base da Exportação - Hierarquia mais elevada -  classe Partial
/// </summary>
public partial class ExportacaoBasePage : ConsultaBasePage {

    /* Path de Planilhas Excel de Posicões */
    private Dictionary<string, string> pathExcelPosicoes = new Dictionary<string, string>();
    
    /* Path de Planilhas Excel de Operações */
    private Dictionary<string, string> pathExcelOperacoes = new Dictionary<string, string>();
    
    /* Path de Planilhas Excel de Historico */
    private Dictionary<string, string> pathExcelHistorico = new Dictionary<string, string>();

    private Dictionary<string, string> pathRiskGridHistorico = new Dictionary<string, string>();

    public string PathRiskGrid
    {
        get
        {
            return this.pathRiskGridHistorico["Path_RiskGrid"];
        }
    }
    // Construtor - Carrega path Planilhas
    public ExportacaoBasePage() {

        string pathModelosImplantacao = AppDomain.CurrentDomain.BaseDirectory + "\\Data\\Implantacao";
        string pathModelosDiario = AppDomain.CurrentDomain.BaseDirectory + "\\Data\\Diario";
        string pathRiskGrid = AppDomain.CurrentDomain.BaseDirectory + "\\Data\\RiskGrid";

        /* Posição */
        this.pathExcelPosicoes.Add("Path_ImportacaoPosicaoBolsa", String.Format("{0}\\ImportacaoPosicaoBolsa.xls", pathModelosImplantacao));
        this.pathExcelPosicoes.Add("Path_ImportacaoPosicaoBMF", String.Format("{0}\\ImportacaoPosicaoBMF.xls", pathModelosImplantacao));
        this.pathExcelPosicoes.Add("Path_ImportacaoPosicaoEmprestimoBolsa", String.Format("{0}\\ImportacaoPosicaoEmprestimoBolsa.xls", pathModelosImplantacao));
        this.pathExcelPosicoes.Add("Path_ImportacaoPosicaoTermoBolsa", String.Format("{0}\\ImportacaoTermoBolsa.xls", pathModelosImplantacao));
        this.pathExcelPosicoes.Add("Path_ImportacaoPosicaoFundo", String.Format("{0}\\ImportacaoPosicaoFundo.xls", pathModelosImplantacao));
        this.pathExcelPosicoes.Add("Path_ImportacaoPosicaoRendaFixa", String.Format("{0}\\ImportacaoPosicaoRendaFixa.xls", pathModelosImplantacao));
        this.pathExcelPosicoes.Add("Path_ImportacaoPosicaoCotista", String.Format("{0}\\ImportacaoPosicaoCotista.xls", pathModelosImplantacao));
        this.pathExcelPosicoes.Add("Path_ImportacaoLiquidacao", String.Format("{0}\\ImportacaoLiquidacao.xls", pathModelosImplantacao));
        //
        this.pathExcelPosicoes.Add("Path_ImportacaoCadastroCotistas", String.Format("{0}\\ImportacaoCadastroCotistas.xls", pathModelosDiario));
        //
        /* Histórico */
        this.pathExcelHistorico.Add("Path_ImportacaoPosicaoCotistaHistorico", String.Format("{0}\\ImportacaoPosicaoCotistaHistorico.xls", pathModelosImplantacao));        
        //
        /* Operação */
        this.pathExcelOperacoes.Add("Path_ImportacaoOperacaoCotista", String.Format("{0}\\ImportacaoOperacaoCotista.xls", pathModelosDiario));
        this.pathExcelOperacoes.Add("Path_ImportacaoHistoricoCota", String.Format("{0}\\ImportacaoHistoricoCota.xls", pathModelosDiario));
        
        this.pathRiskGridHistorico.Add("Path_RiskGrid", String.Format("{0}\\RiskGrid.zip", pathRiskGrid));
    }

    /// <summary>
    /// Retorna Uma referencia para um Modelo de Planilha Excel carregado a partir do Path
    /// </summary>
    /// <param name="path"></param>
    /// <returns></returns>
    private Spreadsheet CarregaModelo(string path) {
        Spreadsheet document = new Bytescout.Spreadsheet.Spreadsheet();

        document.LoadFromFile(path);

        Worksheet worksheet = document.Workbook.Worksheets[0];
        //
        worksheet.ViewOptions.ShowZeroValues = true;

        this.LimpaPlanilha(worksheet);

        #region Esconde Planilha Demo do componente
        const string BYTESCOUT_WORKSHEET_NAME = "Bytescout Spreadsheet SDK";
        worksheet = document.Workbook.Worksheets.ByName(BYTESCOUT_WORKSHEET_NAME);
        if (worksheet == null) {
            worksheet = document.Workbook.Worksheets.Add(BYTESCOUT_WORKSHEET_NAME);
        }
        worksheet.Visible = Bytescout.Spreadsheet.Constants.SHEETVISIBILITY.Hidden;
        #endregion
       
        return document;
    }

    /// <summary>
    /// Limpa um quadrante de 50 linhas X 30 colunas começando pela linha 1
    /// </summary>
    /// <param name="worksheet"></param>
    private void LimpaPlanilha(Worksheet worksheet) {
        const int LINHAS = 50;
        const int COLUNAS = 30;

        for (int i = 1; i <= LINHAS; i++) {
            for (int j = 0; j < COLUNAS; j++) {
                worksheet.Rows[i][j].ValueAsString = "";
            }
        }
    }
}