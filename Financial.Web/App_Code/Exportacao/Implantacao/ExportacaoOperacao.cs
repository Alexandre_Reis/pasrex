﻿using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.IO;
using System.Collections.Generic;

using EntitySpaces.Interfaces;
using Financial.Web.Common;
using Bytescout.Spreadsheet;
using Financial.Fundo;
using System.Drawing;
using Financial.InvestidorCotista;

public partial class ExportacaoBasePage : ConsultaBasePage {
    
    #region Enums
    enum TipoExportacaoOperacao {
        OperacaoCotista = 1,
        HistoricoCota = 2
    }
    #endregion

    /// <summary>
    /// Gera um Conjunto de Planilhas Excel de acordo com as opções escolhidas num combo 
    /// </summary>
    /// <param name="ids">ids das opções escolhidas no combo - sempre tem algum valor</param>
    /// <param name="idsCarteiras">ids Carteiras escolhidos - sempre tem algum valor</param>
    /// <param name="dataInicio">Data Inicio</param>
    /// <param name="dataFim">Data Fim</param>
    /// <param name="dicMStream">Dicionario de memory Streams cada uma representando 1 arquivo e seus respectivos nomes de arquivos</param>
    protected void ProcessaPlanilhasOperacao(List<int> ids, List<int> idsCarteiras, DateTime dataInicio, DateTime dataFim,
                                            out Dictionary<string, MemoryStream> dicMStream) {

        dicMStream = new Dictionary<string, MemoryStream>();

        #region OperacaoCotista
        if (ids.Contains((int)TipoExportacaoOperacao.OperacaoCotista)) {
            MemoryStream m1 = this.GeraOperacaoCotista(idsCarteiras, dataInicio, dataFim);
            dicMStream.Add(Path.GetFileName(this.pathExcelOperacoes["Path_ImportacaoOperacaoCotista"]), m1);
        }
        #endregion
        #region HistoricoCota
        if (ids.Contains((int)TipoExportacaoOperacao.HistoricoCota)) {
            MemoryStream m2 = this.GeraHistoricoCota(idsCarteiras, dataInicio, dataFim);
            dicMStream.Add(Path.GetFileName(this.pathExcelOperacoes["Path_ImportacaoHistoricoCota"]), m2);
        }
        #endregion        
    }

    /// <summary>
    /// Retorna uma Planilha de Operação Cotista
    /// </summary>
    /// <param name="idsCarteiras"></param>
    /// <param name="dataInicio"></param>
    /// <param name="dataFim"></param>
    private MemoryStream GeraOperacaoCotista(List<int> idsCarteiras, DateTime dataInicio, DateTime dataFim) {
        MemoryStream msExcel = new MemoryStream();

        Spreadsheet document = CarregaModelo(this.pathExcelOperacoes["Path_ImportacaoOperacaoCotista"]);

        Worksheet worksheet = document.Workbook.Worksheets[0];

        #region Preenche dados Planilha
        OperacaoCotistaCollection p = new OperacaoCotistaCollection();
        p.Query.Select()
               .Where(p.Query.IdCarteira.In(idsCarteiras) &&
                      p.Query.DataOperacao.GreaterThanOrEqual(dataInicio),
                      p.Query.DataOperacao.LessThanOrEqual(dataFim))
               .OrderBy(p.Query.DataOperacao.Ascending,
                        p.Query.IdCarteira.Ascending,
                        p.Query.IdCotista.Ascending);

        p.Query.Load();

        for (int i = 0; i < p.Count; i++)
        {
            int k = i + 2;
            worksheet.Cell("A" + k).ValueAsInteger = p[i].IdCotista.Value; // IdCotista
            worksheet.Cell("B" + k).ValueAsInteger = p[i].IdCarteira.Value; // IdCarteira
            worksheet.Cell("C" + k).ValueAsDateTime = p[i].DataOperacao.Value; // DataOperacao
            worksheet.Cell("D" + k).ValueAsDateTime = p[i].DataConversao.Value; // DataConversao
            worksheet.Cell("E" + k).ValueAsDateTime = p[i].DataLiquidacao.Value; // DataLiquidacao
            worksheet.Cell("F" + k).ValueAsInteger = p[i].TipoOperacao.Value; // TipoOperacao

            if (p[i].TipoResgate.HasValue)
            {
                worksheet.Cell("G" + k).ValueAsInteger = p[i].TipoResgate.Value; // TipoResgate
            }

            worksheet.Cell("H" + k).Value = Convert.ToDecimal(p[i].Quantidade.Value.ToString("N8")); // Quantidade
            worksheet.Cell("I" + k).Value = Convert.ToDecimal(p[i].CotaOperacao.Value.ToString("N8")); // CotaOperacao
            worksheet.Cell("J" + k).Value = Convert.ToDecimal(p[i].ValorBruto.Value.ToString("N2")); // ValorBruto
            worksheet.Cell("K" + k).Value = Convert.ToDecimal(p[i].ValorLiquido.Value.ToString("N2")); // ValorLiquido
            worksheet.Cell("L" + k).Value = Convert.ToDecimal(p[i].ValorIR.Value.ToString("N2")); // ValorIR
            worksheet.Cell("M" + k).Value = Convert.ToDecimal(p[i].ValorIOF.Value.ToString("N2")); // ValorIOF
            worksheet.Cell("N" + k).Value = Convert.ToDecimal(p[i].ValorPerformance.Value.ToString("N2")); // ValorPerformance
            worksheet.Cell("O" + k).Value = Convert.ToDecimal(p[i].RendimentoResgate.Value.ToString("N2")); // RendimentoResgate
            //
            // Alinhamento e Fonte
            worksheet.Cell("A" + k).AlignmentHorizontal = Bytescout.Spreadsheet.Constants.AlignmentHorizontal.Centered;
            worksheet.Cell("B" + k).AlignmentHorizontal = Bytescout.Spreadsheet.Constants.AlignmentHorizontal.Centered;
            worksheet.Cell("C" + k).AlignmentHorizontal = Bytescout.Spreadsheet.Constants.AlignmentHorizontal.Centered;
            worksheet.Cell("D" + k).AlignmentHorizontal = Bytescout.Spreadsheet.Constants.AlignmentHorizontal.Centered;
            worksheet.Cell("E" + k).AlignmentHorizontal = Bytescout.Spreadsheet.Constants.AlignmentHorizontal.Centered;
            worksheet.Cell("F" + k).AlignmentHorizontal = Bytescout.Spreadsheet.Constants.AlignmentHorizontal.Centered;
            worksheet.Cell("G" + k).AlignmentHorizontal = Bytescout.Spreadsheet.Constants.AlignmentHorizontal.Centered;            
            worksheet.Cell("H" + k).AlignmentHorizontal = Bytescout.Spreadsheet.Constants.AlignmentHorizontal.Right;
            worksheet.Cell("I" + k).AlignmentHorizontal = Bytescout.Spreadsheet.Constants.AlignmentHorizontal.Right;
            worksheet.Cell("J" + k).AlignmentHorizontal = Bytescout.Spreadsheet.Constants.AlignmentHorizontal.Right;
            worksheet.Cell("K" + k).AlignmentHorizontal = Bytescout.Spreadsheet.Constants.AlignmentHorizontal.Right;
            worksheet.Cell("L" + k).AlignmentHorizontal = Bytescout.Spreadsheet.Constants.AlignmentHorizontal.Right;
            worksheet.Cell("M" + k).AlignmentHorizontal = Bytescout.Spreadsheet.Constants.AlignmentHorizontal.Right;
            worksheet.Cell("N" + k).AlignmentHorizontal = Bytescout.Spreadsheet.Constants.AlignmentHorizontal.Right;
            worksheet.Cell("O" + k).AlignmentHorizontal = Bytescout.Spreadsheet.Constants.AlignmentHorizontal.Right;
            //
            worksheet.Rows[k].Font = new Font("Arial", 8, FontStyle.Regular);
        }
        #endregion

        //
        document.SaveToStreamXLS(msExcel);
        //

        /* Necessário voltar o ponteiro do arquivo para o Inicio */
        msExcel.Seek(0, SeekOrigin.Begin);
        //
        document.Close();
        //
        return msExcel;

    }

    /// <summary>
    /// Retorna uma Planilha de Histórico Cota
    /// </summary>
    /// <param name="idsCarteiras"></param>
    /// <param name="dataInicio"></param>
    /// <param name="dataFim"></param>    
    private MemoryStream GeraHistoricoCota(List<int> idsCarteiras, DateTime dataInicio, DateTime dataFim) 
    {
        MemoryStream msExcel = new MemoryStream();

        Spreadsheet document = CarregaModelo(this.pathExcelOperacoes["Path_ImportacaoHistoricoCota"]);

        Worksheet worksheet = document.Workbook.Worksheets[0];

        #region Preenche dados Planilha
        HistoricoCotaCollection p = new HistoricoCotaCollection();
        p.Query.Select()
               .Where(p.Query.IdCarteira.In(idsCarteiras) &&
                      p.Query.Data.GreaterThanOrEqual(dataInicio),
                      p.Query.Data.LessThanOrEqual(dataFim))
               .OrderBy(p.Query.IdCarteira.Ascending,
                        p.Query.Data.Ascending);

        p.Query.Load();

        for (int i = 0; i < p.Count; i++)
        {
            int k = i + 2;
            worksheet.Cell("A" + k).ValueAsInteger = p[i].IdCarteira.Value; // IdCarteira
            worksheet.Cell("B" + k).ValueAsDateTime = p[i].Data.Value; // Data
            worksheet.Cell("C" + k).Value = Convert.ToDecimal(p[i].CotaFechamento.Value.ToString("N8")); // CotaFechamento
            worksheet.Cell("D" + k).Value = Convert.ToDecimal(p[i].PLFechamento.Value.ToString("N2")); // PLFechamento
            worksheet.Cell("E" + k).Value = Convert.ToDecimal(p[i].QuantidadeFechamento.Value.ToString("N8")); // QuantidadeFechamento
            //
            // Alinhamento e Fonte
            worksheet.Cell("A" + k).AlignmentHorizontal = Bytescout.Spreadsheet.Constants.AlignmentHorizontal.Centered;
            worksheet.Cell("B" + k).AlignmentHorizontal = Bytescout.Spreadsheet.Constants.AlignmentHorizontal.Centered;
            worksheet.Cell("C" + k).AlignmentHorizontal = Bytescout.Spreadsheet.Constants.AlignmentHorizontal.Right;
            worksheet.Cell("D" + k).AlignmentHorizontal = Bytescout.Spreadsheet.Constants.AlignmentHorizontal.Right;
            worksheet.Cell("E" + k).AlignmentHorizontal = Bytescout.Spreadsheet.Constants.AlignmentHorizontal.Right;            
            //
            worksheet.Rows[k].Font = new Font("Arial", 8, FontStyle.Regular);
        }
        #endregion

        //
        document.SaveToStreamXLS(msExcel);
        //
        
        /* Necessário voltar o ponteiro do arquivo para o Inicio */
        msExcel.Seek(0, SeekOrigin.Begin);
        //
        document.Close();
        //
        return msExcel;
    }
}