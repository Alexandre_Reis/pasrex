﻿using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.IO;
using System.Collections.Generic;
using Financial.RiskGridFileHandler;
using EntitySpaces.Interfaces;
using Financial.Web.Common;
using Bytescout.Spreadsheet;
using Financial.InvestidorCotista;
using System.Drawing;

public partial class ExportacaoBasePage : ConsultaBasePage
{

    #region Enums
    enum TipoExportacaoHistorico
    {
        PosicaoCotistaHistorico = 1,
        PosicaoRiskGrid = 2
    }
    #endregion

    /// <summary>
    /// Gera um Conjunto de Planilhas Excel de acordo com as opções escolhidas num combo
    /// </summary>
    /// <param name="ids">ids das opções escolhidas no combo - sempre tem algum valor</param>
    /// <param name="idsCarteiras">ids Carteiras escolhidos - sempre tem algum valor</param>
    /// <param name="data">Data Escolhida</param>
    /// <param name="dicMStream">Dicionario de memory Streams cada uma representando 1 arquivo e seus respectivos nomes de arquivos</param>
    protected void ProcessaPlanilhasHistorico(List<int> ids, List<int> idsCarteiras, DateTime data,
                                              out Dictionary<string, MemoryStream> dicMStream)
    {

        dicMStream = new Dictionary<string, MemoryStream>();

        #region PosicaoCotistaHistorico
        if (ids.Contains((int)TipoExportacaoHistorico.PosicaoCotistaHistorico))
        {
            MemoryStream m1 = this.GeraPosicaoCotistaHistorico(idsCarteiras, data);
            dicMStream.Add(Path.GetFileName(this.pathExcelHistorico["Path_ImportacaoPosicaoCotistaHistorico"]), m1);
        }
        if (ids.Contains((int)TipoExportacaoHistorico.PosicaoRiskGrid))
        {
            MemoryStream m1 = this.GeraPosicaoRiskGrid(idsCarteiras, data);

            dicMStream.Add(Path.GetFileName(this.pathRiskGridHistorico["Path_RiskGrid"]), m1);
        }
        #endregion
    }

    /// <summary>
    /// Retorna uma Planilha de Posição Cotista Historico
    /// </summary>
    /// <param name="idsCarteiras"></param>
    private MemoryStream GeraPosicaoCotistaHistorico(List<int> idsCarteiras, DateTime data)
    {
        MemoryStream msExcel = new MemoryStream();

        Spreadsheet document = CarregaModelo(this.pathExcelHistorico["Path_ImportacaoPosicaoCotistaHistorico"]);

        Worksheet worksheet = document.Workbook.Worksheets[0];

        #region Preenche dados Planilha
        PosicaoCotistaHistoricoCollection p = new PosicaoCotistaHistoricoCollection();

        p.Query.Select(p.Query.DataHistorico, p.Query.IdCotista, p.Query.IdCarteira,
                       p.Query.ValorAplicacao, p.Query.DataAplicacao, p.Query.DataConversao,
                       p.Query.Quantidade, p.Query.CotaAplicacao, p.Query.DataUltimaCobrancaIR)
               .Where(p.Query.IdCarteira.In(idsCarteiras) &&
                      p.Query.Quantidade != 0 &&
                      p.Query.DataHistorico == data)
               .OrderBy(p.Query.IdCarteira.Ascending,
                        p.Query.IdCotista.Ascending,
                        p.Query.DataAplicacao.Ascending,
                        p.Query.DataConversao.Ascending);

        p.Query.Load();

        for (int i = 0; i < p.Count; i++)
        {
            int k = i + 2;
            worksheet.Cell("A" + k).ValueAsDateTime = p[i].DataHistorico.Value; // DataHistorico
            worksheet.Cell("B" + k).ValueAsInteger = p[i].IdCotista.Value; // IdCotista
            worksheet.Cell("C" + k).ValueAsInteger = p[i].IdCarteira.Value;  // IdCarteira
            worksheet.Cell("D" + k).Value = Convert.ToDecimal(p[i].ValorAplicacao.Value.ToString("N2")); // ValorAplicacao
            worksheet.Cell("E" + k).ValueAsDateTime = p[i].DataAplicacao.Value; // DataAplicacao
            worksheet.Cell("F" + k).ValueAsDateTime = p[i].DataConversao.Value; // DataConversao            
            worksheet.Cell("G" + k).Value = Convert.ToDecimal(p[i].Quantidade.Value); //Quantidade
            worksheet.Cell("H" + k).Value = Convert.ToDecimal(p[i].CotaAplicacao.Value); //CotaAplicacao
            worksheet.Cell("I" + k).ValueAsDateTime = p[i].DataUltimaCobrancaIR.Value; // DataUltimaCobrancaIR
            //
            // Alinhamento e Fonte
            worksheet.Cell("A" + k).AlignmentHorizontal = Bytescout.Spreadsheet.Constants.AlignmentHorizontal.Centered;
            worksheet.Cell("B" + k).AlignmentHorizontal = Bytescout.Spreadsheet.Constants.AlignmentHorizontal.Centered;
            worksheet.Cell("C" + k).AlignmentHorizontal = Bytescout.Spreadsheet.Constants.AlignmentHorizontal.Centered;
            worksheet.Cell("D" + k).AlignmentHorizontal = Bytescout.Spreadsheet.Constants.AlignmentHorizontal.Right;
            worksheet.Cell("E" + k).AlignmentHorizontal = Bytescout.Spreadsheet.Constants.AlignmentHorizontal.Centered;
            worksheet.Cell("F" + k).AlignmentHorizontal = Bytescout.Spreadsheet.Constants.AlignmentHorizontal.Centered;
            worksheet.Cell("G" + k).AlignmentHorizontal = Bytescout.Spreadsheet.Constants.AlignmentHorizontal.Right;
            worksheet.Cell("H" + k).AlignmentHorizontal = Bytescout.Spreadsheet.Constants.AlignmentHorizontal.Right;
            worksheet.Cell("I" + k).AlignmentHorizontal = Bytescout.Spreadsheet.Constants.AlignmentHorizontal.Centered;
            //
            worksheet.Rows[k].Font = new Font("Arial", 8, FontStyle.Regular);
        }
        #endregion

        //
        document.SaveToStreamXLS(msExcel);
        //
        /* Necessário voltar o ponteiro do arquivo para o Inicio */
        msExcel.Seek(0, SeekOrigin.Begin);

        document.Close();
        //
        return msExcel;
    }

    /// <summary>
    /// Retorna uma Planilha de Posição para o sistema RiskGrid
    /// </summary>
    /// <param name="idsCarteiras"></param>
    private MemoryStream GeraPosicaoRiskGrid(List<int> idsCarteiras, DateTime data)
    {
        List<string> clients = new List<string>();
        foreach (int i in idsCarteiras)
            clients.Add(i.ToString());

        string clientsToFile = string.Join("','", clients.ToArray());
        ArquivoRiskGrid riskGrid = new ArquivoRiskGrid();

        ExportacaoBasePage export = new ExportacaoBasePage();

        string cliente = ConfigurationManager.AppSettings["Cliente"];
        return riskGrid.GeraArquivos(cliente, export.PathRiskGrid, data, clientsToFile);
    }
}