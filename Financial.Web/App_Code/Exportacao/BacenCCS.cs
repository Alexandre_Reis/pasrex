﻿using System.Collections.Generic;
using System;
using Financial.Util;
using System.IO;
using Financial.Fundo;
using Financial.Common;
using Financial.InvestidorCotista;
using System.Text;
using Financial.Investidor;
using Financial.Investidor.Enums;
using Financial.Bolsa;
using Financial.Bolsa.Enums;
using Financial.CRM;

namespace Financial.Export {

    /// <summary>
    ///  Classe para fazer as Exportações Pertencentes a Fundo
    /// </summary>
    public class BacenCCS {
        /// <summary>
        /// Determina o numero total de casas que um numero deve ter com zeros a esquerda
        /// </summary>
        private enum AlinhamentoEsquerda {
            [StringValue("00")]
            _2Casas = 1,

            [StringValue("0000")]
            _4Casas = 2,

            [StringValue("00000")]
            _5Casas = 3,

            [StringValue("000000")]
            _6Casas = 4,

            [StringValue("00000000")]
            _8Casas = 5,

            [StringValue("0000000000")]
            _10Casas = 6,

            [StringValue("00000000000000")]
            _14Casas = 7,
        }

        private List<Cotista.CotistaCSS> valoresDetail = new List<Cotista.CotistaCSS>();
        
        /// <summary>
        /// Retorna uma Stream de Mémoria de um arquivo txt. A Stream permanece aberta
        /// </summary>
        /// <param name="dataPosicao"></param>
        /// <param name="idClientes">Lista de idClientes a executar</param>
        /// <param name="ms">Saida: Memory Stream do arquivo BacenCCS</param>
        /// <param name="nomeArquivo">Saida: Nome do Arquivo BacenCCS</param>        
        /// <returns></returns>
        /// <exception cref="ProcessaMensalClubesException">
        /// throws ProcessaBacenCCSException se occoreu algum erro no processamento
        /// </exception>
        public void ExportaBacenCCS(DateTime dataPosicao, List<int> idClientes, out MemoryStream ms, out string nomeArquivo) {
            ms = new MemoryStream();
            nomeArquivo = "";

            #region Busca as Carteiras
            CarteiraQuery carteiraQuery = new CarteiraQuery("C");
            ClienteQuery clienteQuery = new ClienteQuery("B");
            AgenteMercadoQuery agenteMercadoQuery = new AgenteMercadoQuery("A");

            carteiraQuery.Select(carteiraQuery.IdCarteira, carteiraQuery.Nome, agenteMercadoQuery.Cnpj);
            //
            carteiraQuery.InnerJoin(clienteQuery).On(carteiraQuery.IdCarteira == clienteQuery.IdCliente);
            carteiraQuery.InnerJoin(agenteMercadoQuery).On(carteiraQuery.IdAgenteAdministrador == agenteMercadoQuery.IdAgente);
            //
            carteiraQuery.Where(clienteQuery.IdTipo.In((int)TipoClienteFixo.Fundo, (int)TipoClienteFixo.Clube));
            carteiraQuery.Where(clienteQuery.StatusAtivo.Equal((byte)StatusAtivoCliente.Ativo));
            
            // Acrescentado filtro de icliente
            carteiraQuery.Where(clienteQuery.IdCliente.In(idClientes));
            //
            CarteiraCollection carteiraCollection = new CarteiraCollection();
            carteiraCollection.Load(carteiraQuery);
            #endregion

            if (carteiraCollection.HasData) {
                StringBuilder nomeArquivoAux = new StringBuilder();

                nomeArquivoAux.Append("CCS_" + this.FormataValor(Convert.ToDecimal(dataPosicao.Year), AlinhamentoEsquerda._4Casas) +
                                                this.FormataValor(Convert.ToDecimal(dataPosicao.Month), AlinhamentoEsquerda._2Casas) +
                                                this.FormataValor(Convert.ToDecimal(dataPosicao.Day), AlinhamentoEsquerda._2Casas));
                nomeArquivoAux.Append(".txt");

                // out
                nomeArquivo = nomeArquivoAux.ToString();
                              
                // 
                #region EscreveArquivo
                try {
                    //Abrir o arquivo
                    StreamWriter arquivo = new StreamWriter(ms, Encoding.ASCII);

                    #region Header
                    this.EscreveHeader(arquivo, dataPosicao);
                    #endregion

                    #region Detalhes
                    //if (this.valoresDetail.Count == 0) { // 0 registros
                    //    throw new ProcessaBacenCCSException("Não existem dados para geração do arquivo BacenCCS.");
                    //}

                    this.valoresDetail = new Cotista().RetornaListaCCS(dataPosicao, idClientes);

                    for (int i = 0; i < this.valoresDetail.Count; i++) {
                        this.EscreveDetalhe(arquivo, i);
                    }
                    #endregion

                    #region Rodape
                    this.EscreveRodape(arquivo);
                    #endregion
                   
                    arquivo.Flush();

                    // Retorna a MemoryStream para o inicio
                    ms.Seek(0, SeekOrigin.Begin);
                }
                catch (Exception e) {
                    Console.WriteLine(e.Message);
                    string mensagem = "";
                    mensagem = "Arquivo BacenCCS com Data " + dataPosicao.ToString("dd/MM/yyyy") + " não Exportado ";
                    mensagem += "\n - " + e.Message;
               
                    throw new ProcessaBacenCCSException(mensagem);
                }
                #endregion
            }
            // 0 registros
            else {
                throw new ProcessaBacenCCSException("Não existem dados para geração do arquivo BacenCCS.");
            }
        }

        /// <summary>
        /// Escreve no Arquivo o objeto ValoresDetail
        /// </summary>
        /// <param name="arquivo"></param>
        /// <param name="linha">Numero da Linha de detalhe a escrever começando de 0</param>
        private void EscreveDetalhe(StreamWriter arquivo, int linha) {
            string brancos = "";

            arquivo.Write("C");
            arquivo.Write(this.valoresDetail[linha].tipoPessoa);
            arquivo.Write(this.valoresDetail[linha].cpfCNPJ.Replace("-", "").Replace(".", "").PadRight(14, ' '));            
            arquivo.Write(this.valoresDetail[linha].cnpjAdministrador.PadRight(8, ' '));
            arquivo.Write("000027");
            arquivo.Write(this.valoresDetail[linha].idCotista.ToString().PadRight(50, ' '));
            //
            //
            string nome = this.valoresDetail[linha].nome;
            if (nome.Length >= 40) {
                nome = nome.Substring(0, 40);
            }
            //
            arquivo.Write(nome.PadRight(40, ' '));
                        
            arquivo.Write("04");
            arquivo.Write("CLUBE|FUNDO DE INVES");
            arquivo.Write("T");
            arquivo.Write(this.valoresDetail[linha].dataInicioOperacao.ToString("yyyyMMdd"));

            if (this.valoresDetail[linha].dataFimOperacao.HasValue) {
                arquivo.Write(this.valoresDetail[linha].dataFimOperacao.Value.ToString("yyyyMMdd"));
                arquivo.Write("S");
            }
            else {
                arquivo.Write("        ");
                arquivo.Write("N");
            }
            arquivo.Write(brancos.ToString().PadRight(10, ' '));
            int cont = linha + 2;
            arquivo.Write(cont.ToString().PadLeft(10, '0'));
            //
            // Utima Linha não imprime
            if (linha != this.valoresDetail.Count - 1) {
                arquivo.WriteLine();
            }
        }

        /// <summary>
        /// Escreve header do arquivo
        /// </summary>
        /// <param name="arquivo"></param>
        private void EscreveHeader(StreamWriter arquivo, DateTime dataPosicao) {

            string filler = "";
            
            arquivo.Write("H");
            arquivo.Write("99");
            arquivo.Write(dataPosicao.ToString("yyyyMMdd"));
            //
            int num = 1;
            arquivo.Write(num.ToString().PadLeft(10, '0')); // Numero do Arquivo - duvida
            //
            arquivo.Write("A");
            arquivo.Write(filler.ToString().PadRight(148, ' '));
            //
            int num1 = 1;
            arquivo.Write(num1.ToString().PadLeft(10, '0'));
            //
            arquivo.WriteLine();
        }

        /// <summary>
        /// Escreve Rodape do arquivo
        /// </summary>
        /// <param name="arquivo"></param>
        private void EscreveRodape(StreamWriter arquivo) {            
            string linha = "T";
            string filler = "";
            int num = this.valoresDetail.Count + 2;            
            //
            if (this.valoresDetail.Count > 0)
            {
                arquivo.WriteLine();
            }
            arquivo.Write(linha);
            arquivo.Write(filler.ToString().PadRight(169, ' '));
            arquivo.Write(num.ToString().PadLeft(10, '0'));
        }

        /// <summary>
        /// Formata um valor decimal com N casas Completando com zeros a esquerda
        /// Se numero é Positivo Coloca um Sinal de + na frente
        /// </summary>
        /// <param name="valor"></param>
        /// <param name="numeroCasasAlinhamentoEsquerda"></param>
        /// <returns></returns>
        private string FormataValor(decimal valor, AlinhamentoEsquerda numeroCasasAlinhamentoEsquerda) {
            return valor.ToString(StringEnum.GetStringValue(numeroCasasAlinhamentoEsquerda));
        }        
    }
}