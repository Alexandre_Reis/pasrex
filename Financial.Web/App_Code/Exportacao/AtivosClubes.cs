﻿using System.Collections.Generic;
using System;
using Financial.Util;
using System.IO;
using Financial.Fundo;
using Financial.Common;
using Financial.Investidor;
using Financial.Investidor.Enums;
using System.Text;
using Financial.Bolsa;
using Financial.Bolsa.Enums;

namespace Financial.Export {

    /// <summary>
    ///  Classe para fazer as Exportações Pertencentes a Fundo
    /// </summary>
    public class AtivosClubes {

        /// <summary>
        /// Determina o numero total de casas que um numero deve ter com zeros a esquerda
        /// </summary>
        private enum AlinhamentoEsquerda {
            [StringValue("00")]
            _2Casas = 1,

            [StringValue("00000")]
            _5Casas = 2,

            [StringValue("000000")]
            _6Casas = 3,

            [StringValue("0000000000")]
            _10Casas = 4,

            [StringValue("000000000000000000")]
            _18Casas = 5,
        }

        /// <summary>
        /// Valores da Primeira linha do arquivo
        /// </summary>
        #region Valores do Header
        class ValoresHeader {
            public string campoFixo;
            public string anoMes;
            public int idAgenteMercado;
            public string dataReferencia;
        }
        private ValoresHeader valoresHeader = new ValoresHeader();
        #endregion

        /// <summary>
        /// Valores de Detalhe
        /// </summary>
        #region Valores do Detalhe
        class ValoresDetail {
            public string campoFixo;                     // Campo 01
            public string registroBovespa;               // Campo 02
            public string UF;                            // Campo 03            
            public string cdAtivo;                       // Campo 04                          
            public decimal? quantidade;                  // Campo 05
            public decimal? valorMercado;                // Campo 06
            public string campoFixo1;                    // Campo 07
        }
        private List<ValoresDetail> valoresDetail = new List<ValoresDetail>();
        #endregion

        /// <summary>
        /// Retorna uma Stream de Mémoria de um arquivo txt. A Stream permanece aberta
        /// </summary>
        /// <param name="dataPosicao"></param>
        /// <param name="nomeArquivo">Nome do Arquivo AtivosClubes formado de acordo com o IdAgente</param>
        /// <returns></returns>
        /// <exception cref="ProcessaAtivosClubesException">
        /// throws ProcessaAtivosClubesException se ocorreu algum erro no Processamento
        /// </exception>
        public MemoryStream ExportaAtivosClubes(DateTime dataPosicao, out string nomeArquivo) {
            MemoryStream ms = new MemoryStream();
            nomeArquivo = "";

            #region Busca as Carteiras
            CarteiraQuery carteiraQuery = new CarteiraQuery("C");
            ClienteQuery clienteQuery = new ClienteQuery("B");
            AgenteMercadoQuery agenteMercadoQuery = new AgenteMercadoQuery("A");

            carteiraQuery.Select(carteiraQuery.IdCarteira, clienteQuery.IdCliente, 
                                 agenteMercadoQuery.IdAgente, agenteMercadoQuery.Uf);
            //
            carteiraQuery.InnerJoin(clienteQuery).On(carteiraQuery.IdCarteira == clienteQuery.IdCliente);
            carteiraQuery.InnerJoin(agenteMercadoQuery).On(carteiraQuery.IdAgenteAdministrador == agenteMercadoQuery.IdAgente);
            //
            carteiraQuery.Where(clienteQuery.IdTipo == TipoClienteFixo.Clube);
            //
            CarteiraCollection carteiraCollection = new CarteiraCollection();
            carteiraCollection.Load(carteiraQuery);
            #endregion

            if (carteiraCollection.HasData) {
                int idAgenteAux = (int)carteiraCollection[0].GetColumn(AgenteMercadoMetadata.ColumnNames.IdAgente);

                StringBuilder nomeArquivoAux = new StringBuilder();
                nomeArquivoAux.Append("Papeis_");
                nomeArquivoAux.Append(this.FormataValor(Convert.ToDecimal(dataPosicao.Month), AlinhamentoEsquerda._2Casas, false))
                               .Append(".txt");

                // out
                nomeArquivo = nomeArquivoAux.ToString();

                for (int i = 0; i < carteiraCollection.Count; i++) {
                    int idCliente = (int)carteiraCollection[i].GetColumn(ClienteMetadata.ColumnNames.IdCliente);
                    int idCarteira = carteiraCollection[i].IdCarteira.Value;

                    #region Busca o RegistroBovespa
                    ClienteInterface clienteInterface = new ClienteInterface();
                    clienteInterface.LoadByPrimaryKey(idCliente);
                    //
                    string registroBovespa = "";
                    //
                    if (clienteInterface.es.HasData) {
                        registroBovespa = clienteInterface.RegistroBovespa != null
                                ? clienteInterface.RegistroBovespa.Trim()
                                : "";
                    }
                    #endregion

                    #region Busca o Estado
                    string uf = (string)carteiraCollection[i].GetColumn(AgenteMercadoMetadata.ColumnNames.Uf);
                    //uf = String.IsNullOrEmpty(uf) ? "  " : uf.Trim().ToUpper();
                    uf = uf.Trim().ToUpper();
                    //
                    #endregion
                    
                    #region Pega Valores Table
                    PosicaoBolsaHistoricoCollection posicaoBolsaHistoricoCollection = new PosicaoBolsaHistoricoCollection();
                    posicaoBolsaHistoricoCollection.Query
                               .Select(posicaoBolsaHistoricoCollection.Query.CdAtivoBolsa,
                                       posicaoBolsaHistoricoCollection.Query.Quantidade.Sum(),
                                       posicaoBolsaHistoricoCollection.Query.ValorMercado.Sum())
                               .Where(posicaoBolsaHistoricoCollection.Query.IdCliente == idCliente &
                                      posicaoBolsaHistoricoCollection.Query.DataHistorico == dataPosicao,
                                      posicaoBolsaHistoricoCollection.Query.TipoMercado.Equal(TipoMercadoBolsa.MercadoVista))
                               .GroupBy(posicaoBolsaHistoricoCollection.Query.CdAtivoBolsa);

                    posicaoBolsaHistoricoCollection.Query.Load();

                    // Cada Posicao Representa uma linha no Arquivo
                    for (int j = 0; j < posicaoBolsaHistoricoCollection.Count; j++) {
                        string cdAtivoBolsa = posicaoBolsaHistoricoCollection[j].CdAtivoBolsa.Trim();
                        decimal quantidade = posicaoBolsaHistoricoCollection[j].Quantidade.Value;
                        decimal valorMercado = posicaoBolsaHistoricoCollection[j].ValorMercado.Value;

                        #region Preenche os campos
                        //
                        ValoresDetail valorDetailAux = new ValoresDetail();
                        valorDetailAux.campoFixo = "01";
                        //
                        valorDetailAux.registroBovespa = registroBovespa;
                        valorDetailAux.UF = uf;
                        //
                        valorDetailAux.cdAtivo = cdAtivoBolsa;
                        valorDetailAux.quantidade = quantidade;
                        valorDetailAux.valorMercado = valorMercado * 100;
                        //
                        valorDetailAux.campoFixo1 = "S";
                        //
                        #endregion

                        // Adiciona os dados referentes a uma carteira
                        this.valoresDetail.Add(valorDetailAux);
                    }
                    #endregion                    
                }

                // 
                #region EscreveArquivo
                try {

                    //Abrir o arquivo
                    StreamWriter arquivo = new StreamWriter(ms, Encoding.ASCII);

                    #region Header
                    this.EscreveHeader(arquivo, idAgenteAux, dataPosicao);
                    #endregion

                    #region Detalhes
                    for (int i = 0; i < this.valoresDetail.Count; i++) {
                        this.EscreveDetalhe(arquivo, i);
                    }
                    #endregion

                    arquivo.Flush();

                    // Retorna a MemoryStream para o inicio
                    ms.Seek(0, SeekOrigin.Begin);

                }
                catch (Exception e) {
                    Console.WriteLine(e.Message);
                    string mensagem = "";
                    mensagem = "Arquivo Papéis com Data " + dataPosicao.ToString("dd/MM/yyyy") + " não Exportado ";
                    mensagem += "\n\t - " + e.Message;

                    throw new ProcessaAtivosClubesException(mensagem);
                }
                #endregion
            }
            // 0 registros
            else {
                throw new ProcessaAtivosClubesException("Não existem Dados de Exportação de Papéis");
            }

            return ms;
        }

        /// <summary>
        /// Escreve no arquivo o objeto ValoresHeader
        /// </summary>
        /// <param name="arquivo"></param>
        /// <param name="idAgente"></param>
        /// <param name="dataPosicao"></param>
        private void EscreveHeader(StreamWriter arquivo, int idAgente, DateTime dataPosicao) {
            this.valoresHeader.campoFixo = "00";
            //
            string dataStringFormatoPortugues = dataPosicao.ToString("ddMMyyyy");
            string ano = dataStringFormatoPortugues.Substring(4, 4);
            string mes = dataStringFormatoPortugues.Substring(2, 2);
            string dia = dataStringFormatoPortugues.Substring(0, 2);

            this.valoresHeader.anoMes = ano + mes;
            this.valoresHeader.idAgenteMercado = idAgente;
            this.valoresHeader.dataReferencia = ano + mes + dia;
            //
            arquivo.Write(this.valoresHeader.campoFixo);
            arquivo.Write(this.valoresHeader.anoMes);
            arquivo.Write(this.valoresHeader.idAgenteMercado.ToString(StringEnum.GetStringValue(AlinhamentoEsquerda._5Casas)));
            arquivo.Write(this.valoresHeader.dataReferencia);            
        }

        /// <summary>
        /// Escreve no Arquivo o objeto ValoresDetail
        /// </summary>
        /// <param name="arquivo"></param>
        /// <param name="linha">Numero da Linha de detalhe a escrever começando de 0</param>
        private void EscreveDetalhe(StreamWriter arquivo, int linha) {
            //
            if (linha == 0) {
                arquivo.WriteLine();
            }

            arquivo.Write(this.valoresDetail[linha].campoFixo);
            arquivo.Write(this.valoresDetail[linha].registroBovespa.PadLeft(10, '0')); // Completa com '0' a esquerda
            arquivo.Write(this.valoresDetail[linha].UF.PadRight(2)); // Completa com brancos a direita
            //                      
            arquivo.Write( this.valoresDetail[linha].cdAtivo.PadRight(12) ); // Completa com brancos a direita
            arquivo.Write(this.FormataValor(this.valoresDetail[linha].quantidade.Value, AlinhamentoEsquerda._18Casas, false)); // Completa com zeros a esquerda
            arquivo.Write(this.FormataValor(this.valoresDetail[linha].valorMercado.Value, AlinhamentoEsquerda._18Casas, false)); // Completa com zeros a esquerda
            //
            arquivo.Write(this.valoresDetail[linha].campoFixo1);
            //
            // Utima Linha não imprime
            if (linha != this.valoresDetail.Count - 1) {
                arquivo.WriteLine();
            }
        }

        /// <summary>
        /// Formata um valor decimal com N casas Completando com zeros a esquerda
        /// Se numero é Positivo Coloca um Sinal de + na frente
        /// </summary>
        /// <param name="valor"></param>
        /// <param name="numeroCasasAlinhamentoEsquerda"></param>
        /// <returns></returns>
        private string FormataValor(decimal valor, AlinhamentoEsquerda numeroCasasAlinhamentoEsquerda) {
            return this.FormataValor(valor, numeroCasasAlinhamentoEsquerda, true);
        }

        /// <summary>
        /// Formata um valor decimal com N casas Completando com zeros a esquerda
        /// Se colocaPositivo=true e Se numero é Positivo Coloca um Sinal de + na frente
        /// Sinal Negativo Sempre Aparece
        /// </summary>
        /// <param name="valor"></param>
        /// <param name="numeroCasasAlinhamentoEsquerda"></param>
        /// <param name="colocaPositivo"></param>
        /// <returns></returns>
        private string FormataValor(decimal valor, AlinhamentoEsquerda numeroCasasAlinhamentoEsquerda, bool colocaPositivo) {
            if (colocaPositivo) {
                return valor.ToString(
                    valor >= 0
                    ? "+" + StringEnum.GetStringValue(numeroCasasAlinhamentoEsquerda)
                    : StringEnum.GetStringValue(numeroCasasAlinhamentoEsquerda));
            }
            else {
                return valor.ToString(StringEnum.GetStringValue(numeroCasasAlinhamentoEsquerda));
            }
        }
    }
}