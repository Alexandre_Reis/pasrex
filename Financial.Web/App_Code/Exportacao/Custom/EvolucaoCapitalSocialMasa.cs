﻿using System;
using System.IO;
using Bytescout.Spreadsheet;
using Bytescout.Spreadsheet.Constants;
using Financial.Bolsa;
using Financial.Investidor;
using Financial.Investidor.Enums;
using System.Collections.Generic;
using System.Drawing;
using Financial.Common;
using Financial.Bolsa.Enums;

namespace Financial.Export {

    /// <summary>
    /// Summary description for EvolucaoCapitalSocialMasa
    /// </summary>
    public class EvolucaoCapitalSocialMasa {

        private const string MODELO_PLANILHA = "EvolucaoCapitalSocialMasa.xlsx";
        
        private DateTime? dataInicio;
        private DateTime? dataFim;
        private int? idEmissor;

        private EvolucaoCapitalSocialCollection evolucaoCapitalSocialCollection = new EvolucaoCapitalSocialCollection();

        /* Construtor */
        public EvolucaoCapitalSocialMasa(int? idEmissor, DateTime? dataInicio, DateTime? dataFim) {
            this.idEmissor = idEmissor;
            this.dataInicio = dataInicio;
            this.dataFim = dataFim;

            #region Capital Social

            EvolucaoCapitalSocialQuery e = new EvolucaoCapitalSocialQuery("E");
            AtivoBolsaQuery a = new AtivoBolsaQuery("A");

            e.Select(a.IdEmissor, e);
            e.InnerJoin(a).On(e.CdAtivoBolsa == a.CdAtivoBolsa);
            e.Where(a.IdEmissor == this.idEmissor.Value);

            if (this.dataInicio.HasValue)
            {
                e.Where(e.Data.GreaterThanOrEqual(this.dataInicio.Value));
            }

            if (this.dataFim.HasValue)
            {
                e.Where(e.Data.LessThanOrEqual(this.dataFim.Value));
            }
           
            this.evolucaoCapitalSocialCollection.Load(e);
            
            #endregion
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="ms">Saida: Memory Stream do arquivo excel</param>
        /// <param name="nomeArquivo">Saida: Nome do Arquivo Excel</param>
        /// <exception cref="Exception">Se não dados para gerar</exception>
        public void ExportaExcel(out MemoryStream ms, out string nomeArquivo) {

            if (!this.evolucaoCapitalSocialCollection.HasData) {
                throw new Exception("Não há Dados para Gerar.");
            }

            // out
            ms = new MemoryStream();
            nomeArquivo = "EvolucaoCapitalSocial.xls";
            //           
            Spreadsheet document = new Bytescout.Spreadsheet.Spreadsheet();

            string pathModelos = AppDomain.CurrentDomain.BaseDirectory + "\\Relatorios\\Modelos";
            string pathPlanilhaModelo = String.Format("{0}\\{1}", pathModelos, MODELO_PLANILHA);

            document.LoadFromFile(pathPlanilhaModelo);

            Worksheet worksheet = document.Workbook.Worksheets[0];

            #region Ref Planilha - worksheet
            worksheet.ViewOptions.ShowZeroValues = true;
            this.setPlan(worksheet);
            #endregion

            /*-----------------------------------------------------*/

            #region Esconde Planilha Demo do componente
            const string BYTESCOUT_WORKSHEET_NAME = "Bytescout Spreadsheet SDK";
            worksheet = document.Workbook.Worksheets.ByName(BYTESCOUT_WORKSHEET_NAME);
            if (worksheet == null) {
                worksheet = document.Workbook.Worksheets.Add(BYTESCOUT_WORKSHEET_NAME);
            }
            worksheet.Visible = Bytescout.Spreadsheet.Constants.SHEETVISIBILITY.Hidden;
            #endregion

            //
            document.SaveToStreamXLS(ms);
            //
            document.Close();            
        }
       
        /// <summary>
        /// 
        /// </summary>
        /// <param name="worksheet">Planilha 1</param>
        private void setPlan(Worksheet worksheet) {
            
            #region Emissor
            Emissor emissor = new Emissor();
            emissor.LoadByPrimaryKey(this.idEmissor.Value);

            worksheet.Cell("C3").ValueAsString = emissor.Nome.Trim();
            worksheet.Cell("C3").AlignmentHorizontal = AlignmentHorizontal.Left;
            //
            #endregion

            #region for da Planilha

            int linha = 5; // Adiciona Linhas
            for (int i = 0; i < this.evolucaoCapitalSocialCollection.Count; i++) {
                // Add new row
                worksheet.Rows.Insert(linha, 1);
                //
                worksheet.Rows[linha].Height = 22;
                //
                worksheet.Rows[linha].Font = new Font("Calibri", 11, FontStyle.Regular);
                //

                #region Bordas Esquerda
                //coluna B com borda esquerda
                worksheet.Rows[linha][1].LeftBorderStyle = LineStyle.Thin;
                //coluna C com borda esquerda
                worksheet.Rows[linha][2].LeftBorderStyle = LineStyle.Thin;
                //coluna D com borda esquerda
                worksheet.Rows[linha][3].LeftBorderStyle = LineStyle.Thin;
                //coluna E com borda esquerda
                worksheet.Rows[linha][4].LeftBorderStyle = LineStyle.Thin;
                //coluna F com borda esquerda
                worksheet.Rows[linha][5].LeftBorderStyle = LineStyle.Thin;
                //coluna G com borda esquerda
                worksheet.Rows[linha][6].LeftBorderStyle = LineStyle.Thin;
                //coluna H com borda esquerda
                worksheet.Rows[linha][7].LeftBorderStyle = LineStyle.Thin;

                // coluna H com borda direita
                worksheet.Rows[linha][7].RightBorderStyle = LineStyle.Thin;
                //                
                #endregion

                #region Bordas Cima
                //coluna B com borda Cima
                worksheet.Rows[linha][1].TopBorderStyle = LineStyle.Thin;
                //coluna C com borda Cima
                worksheet.Rows[linha][2].TopBorderStyle = LineStyle.Thin;
                //coluna D com borda Cima
                worksheet.Rows[linha][3].TopBorderStyle = LineStyle.Thin;
                //coluna E com borda Cima
                worksheet.Rows[linha][4].TopBorderStyle = LineStyle.Thin;
                //coluna F com borda Cima
                worksheet.Rows[linha][5].TopBorderStyle = LineStyle.Thin;
                //coluna G com borda Cima
                worksheet.Rows[linha][6].TopBorderStyle = LineStyle.Thin;
                //coluna H com borda Cima
                worksheet.Rows[linha][7].TopBorderStyle = LineStyle.Thin;

                #endregion

                linha++;
            }

            linha = 5; // linha inicial dos dados           
            for (int j = 0; j < this.evolucaoCapitalSocialCollection.Count; j++) {
                worksheet.Rows[linha][1].ValueAsDateTime = evolucaoCapitalSocialCollection[j].Data.Value;   // coluna B                
                //
                worksheet.Rows[linha][2].ValueAsString = evolucaoCapitalSocialCollection[j].Descricao; // coluna C
                //
                worksheet.Rows[linha][3].NumberFormatString = "#,##,#0.00";
                worksheet.Rows[linha][3].ValueAsDouble = Convert.ToDouble(evolucaoCapitalSocialCollection[j].Valor.Value); // coluna D
                //                                
                worksheet.Rows[linha][4].NumberFormatString = "#,##,#0.00";
                decimal valor = new EvolucaoCapitalSocial().RetornaValorCapitalSocial(this.idEmissor.Value, evolucaoCapitalSocialCollection[j].Data.Value);
                worksheet.Rows[linha][4].Value = Convert.ToDouble(valor); // coluna E
                //
                worksheet.Rows[linha][5].NumberFormatString = "#,##,#0"; // coluna F
                decimal quantidadeOrdinaria = new EvolucaoCapitalSocial().RetornaQuantidade(this.idEmissor.Value, evolucaoCapitalSocialCollection[j].Data.Value, TipoEspecificacaoBolsa.Ordinaria);
                worksheet.Rows[linha][5].Value = Convert.ToDouble(quantidadeOrdinaria);
                //
                worksheet.Rows[linha][6].NumberFormatString = "#,##,#0"; // coluna G
                decimal quantidadePreferencial = new EvolucaoCapitalSocial().RetornaQuantidade(this.idEmissor.Value, evolucaoCapitalSocialCollection[j].Data.Value, TipoEspecificacaoBolsa.Preferencial);
                worksheet.Rows[linha][6].Value = Convert.ToDouble(quantidadePreferencial);
                //
                worksheet.Rows[linha][7].NumberFormatString = "#,##,#0"; // coluna H
                int l = linha + 1;
                string sum = "=F" + l + "+G" + l;
                worksheet.Rows[linha][7].Value = "" + sum + "";
                //

                #region Alinhamentos B/C/D/E/F/G/H
                worksheet.Rows[linha][1].AlignmentHorizontal = Bytescout.Spreadsheet.Constants.AlignmentHorizontal.Centered; // B
                worksheet.Rows[linha][2].AlignmentHorizontal = Bytescout.Spreadsheet.Constants.AlignmentHorizontal.Left; // C
                worksheet.Rows[linha][3].AlignmentHorizontal = Bytescout.Spreadsheet.Constants.AlignmentHorizontal.Right; // D
                worksheet.Rows[linha][4].AlignmentHorizontal = Bytescout.Spreadsheet.Constants.AlignmentHorizontal.Right; // E
                worksheet.Rows[linha][5].AlignmentHorizontal = Bytescout.Spreadsheet.Constants.AlignmentHorizontal.Right; // F
                worksheet.Rows[linha][6].AlignmentHorizontal = Bytescout.Spreadsheet.Constants.AlignmentHorizontal.Right; // G
                worksheet.Rows[linha][7].AlignmentHorizontal = Bytescout.Spreadsheet.Constants.AlignmentHorizontal.Right; // H
                #endregion
                //
                linha++;
            }
            #endregion
        }
    }
}