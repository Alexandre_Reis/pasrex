﻿using System;
using System.IO;
using Bytescout.Spreadsheet;
using Bytescout.Spreadsheet.Constants;
using Financial.Bolsa;
using Financial.Investidor;
using Financial.Investidor.Enums;
using System.Collections.Generic;
using System.Drawing;
using Financial.Common;
using Financial.Bolsa.Enums;

namespace Financial.Export {

    /// <summary>
    /// Summary description for ParticipacaoSocietariaMasa
    /// </summary>
    public class ParticipacaoSocietariaMasa {

        private const string MODELO_PLANILHA = "ParticipacaoSocietariaMasa.xlsx";

        private enum TipoPesquisa {
            PosicaoBolsa = 0,
            PosicaoBolsaHistorico = 1
        }
        
        private class ValoresParticipacao {
            public string nomeCliente = "";
            public decimal quantidade = 0.00M;
            public string tipoAcoes = "";
            public decimal participacaoVotante = 0.00M;
            public decimal participacaoSocial = 0.00M;
        }
        
        private DateTime data;
        private int emissor;

        /* Construtor */
        public ParticipacaoSocietariaMasa(DateTime data, int emissor) {
            this.data = data;
            this.emissor = emissor;           
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="ms">Saida: Memory Stream do arquivo excel</param>
        /// <param name="nomeArquivo">Saida: Nome do Arquivo Excel</param>        
        public void ExportaExcel(out MemoryStream ms, out string nomeArquivo) {

            // out
            ms = new MemoryStream();
            nomeArquivo = "Masa.xls";
            //           
            Spreadsheet document = new Bytescout.Spreadsheet.Spreadsheet();

            string pathModelos = AppDomain.CurrentDomain.BaseDirectory + "\\Relatorios\\Modelos";
            string pathPlanilhaModelo = String.Format("{0}\\{1}", pathModelos, MODELO_PLANILHA);

            document.LoadFromFile(pathPlanilhaModelo);

            Worksheet worksheet = document.Workbook.Worksheets[0];
            Worksheet worksheet1 = document.Workbook.Worksheets[1];            
            //
            
            #region Ref Planilha1 - worksheet
            worksheet.ViewOptions.ShowZeroValues = true;
            this.setPlan1(worksheet);            
            #endregion

            #region Ref Planilha2 - worksheet1
            worksheet1.ViewOptions.ShowZeroValues = true;
            this.setPlan2(worksheet1);
            #endregion

            /*-----------------------------------------------------*/

            #region Esconde Planilha Demo do componente
            const string BYTESCOUT_WORKSHEET_NAME = "Bytescout Spreadsheet SDK";
            worksheet = document.Workbook.Worksheets.ByName(BYTESCOUT_WORKSHEET_NAME);
            if (worksheet == null) {
                worksheet = document.Workbook.Worksheets.Add(BYTESCOUT_WORKSHEET_NAME);
            }
            worksheet.Visible = Bytescout.Spreadsheet.Constants.SHEETVISIBILITY.Hidden;
            #endregion

            //
            document.SaveToStreamXLS(ms);
            //
            document.Close();            
        }
       
        /// <summary>
        /// 
        /// </summary>
        /// <param name="worksheet">Planilha 1</param>
        private void setPlan1(Worksheet worksheet) {
                       
            #region Descricao Ativo
            Emissor e = new Emissor();
            e.LoadByPrimaryKey(this.emissor);
            //            
            worksheet.Cell("C8").ValueAsString = e.Nome.ToUpper().Trim();
            worksheet.Cell("C8").AlignmentHorizontal = AlignmentHorizontal.Left;
            //            
            #endregion

            #region Emissor
            worksheet.Cell("C9").ValueAsString = e.UpToSetorByIdSetor.Nome.ToUpper().Trim();
            worksheet.Cell("C9").AlignmentHorizontal = AlignmentHorizontal.Left;
            //
            #endregion

            #region Capital Social

            decimal valor = new EvolucaoCapitalSocial().RetornaValorCapitalSocial(this.emissor, this.data);
            decimal ordinarias = new EvolucaoCapitalSocial().RetornaQuantidade(this.emissor, this.data, TipoEspecificacaoBolsa.Ordinaria);
            decimal preferencias = new EvolucaoCapitalSocial().RetornaQuantidade(this.emissor, this.data, TipoEspecificacaoBolsa.Preferencial);

            string capitalSocial = String.Format("R$ {0} representado por {1} ações ordinárias e {2} ações preferênciais", valor.ToString("n2"), ordinarias.ToString("n0"), preferencias.ToString("n0"));
            if (preferencias <= 1) {
                capitalSocial = capitalSocial.Replace("ações preferênciais", "ação preferêncial");
            }

            if (ordinarias <= 1) {
                capitalSocial = capitalSocial.Replace("ações ordinárias", "ação ordinária");
            }

            if(valor == 0 && ordinarias == 0 && preferencias == 0) {
                capitalSocial = "Evolução Capital Social não cadastrado na data " + this.data.ToShortDateString();
            }

            worksheet.Cell("B13").ValueAsString = capitalSocial;
            worksheet.Cell("B13").AlignmentHorizontal = AlignmentHorizontal.Left;
            //
            #endregion
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="worksheet1">Planilha 2</param>
        private void setPlan2(Worksheet worksheet1) {
            #region Descricao Emissor
            Emissor e = new Emissor();
            e.LoadByPrimaryKey(this.emissor);
            //            
            worksheet1.Cell("B7").ValueAsString = e.Nome.ToUpper().Trim();
            worksheet1.Cell("B7").AlignmentHorizontal = AlignmentHorizontal.Centered;
            //
            #endregion

            #region Corpo
            List<ValoresParticipacao> listaVP = new List<ValoresParticipacao>();

            ClienteCollection clienteCollection = new ClienteCollection();
            clienteCollection.Query.Where(clienteCollection.Query.StatusAtivo == (byte)StatusAtivoCliente.Ativo &&
                                          clienteCollection.Query.TipoControle.In((byte)TipoControleCliente.Completo)
                                         );
            //
            clienteCollection.Query.OrderBy(clienteCollection.Query.Nome.Ascending);
            //
            clienteCollection.Query.Load();

            #region for Clientes
            foreach (Cliente cliente in clienteCollection) {
                int idCliente = cliente.IdCliente.Value;

                TipoPesquisa tipoPesquisa = cliente.IsClienteNaData(idCliente, this.data)
                                            ? TipoPesquisa.PosicaoBolsa
                                            : TipoPesquisa.PosicaoBolsaHistorico;

                decimal votante = new EvolucaoCapitalSocial().RetornaQuantidade(this.emissor, this.data, TipoEspecificacaoBolsa.Ordinaria);
                decimal social = new EvolucaoCapitalSocial().RetornaQuantidade(this.emissor, this.data, null);

                #region PosicaoBolsa
                if (tipoPesquisa == TipoPesquisa.PosicaoBolsa) {

                    PosicaoBolsaCollection p = new PosicaoBolsaCollection();
                    //
                    PosicaoBolsaQuery pb = new PosicaoBolsaQuery("P");
                    AtivoBolsaQuery a = new AtivoBolsaQuery("A");
                    EmissorQuery em = new EmissorQuery("E");
                    EspecificacaoBolsaQuery esp = new EspecificacaoBolsaQuery("ESP");

                    pb.es.Distinct = true;

                    pb.Select(pb.IdCliente, esp.Tipo, pb.Quantidade.Sum());
                    pb.InnerJoin(a).On(pb.CdAtivoBolsa == a.CdAtivoBolsa);
                    pb.InnerJoin(em).On(a.IdEmissor == em.IdEmissor);                    
                    pb.InnerJoin(esp).On(a.Especificacao == esp.Especificacao);
                                        
                    //
                    pb.Where(pb.IdCliente == idCliente &&
                             a.IdEmissor == this.emissor &&
                             pb.Quantidade != 0);
                    //
                    pb.GroupBy(pb.IdCliente, esp.Tipo);
                    pb.OrderBy(esp.Tipo.Ascending);
                    //
                    p.Load(pb);

                    for (int i = 0; i < p.Count; i++) {
                        ValoresParticipacao vp = new ValoresParticipacao();

                        Cliente c = new Cliente();
                        c.LoadByPrimaryKey(idCliente);
                        //
                        vp.nomeCliente = c.Nome.Trim();
                        vp.quantidade = p[i].Quantidade.Value;
                        //
                        int tipo = Convert.ToInt32(p[i].GetColumn(EspecificacaoBolsaMetadata.ColumnNames.Tipo));
                        //
                        vp.tipoAcoes = tipo == (int)TipoEspecificacaoBolsa.Ordinaria ? "ON" : "PN";

                        if (votante != 0) {
                            vp.participacaoVotante = vp.quantidade / votante;
                        }

                        if (social != 0) {
                            vp.participacaoSocial = vp.quantidade / social;
                        }
                        //
                        listaVP.Add(vp);
			        }
                }

                #endregion

                #region PosicaoBolsaHistorico
                else if (tipoPesquisa == TipoPesquisa.PosicaoBolsaHistorico) {
                    PosicaoBolsaHistoricoCollection ph = new PosicaoBolsaHistoricoCollection();
                    //
                    PosicaoBolsaHistoricoQuery pbh = new PosicaoBolsaHistoricoQuery("P");
                    AtivoBolsaQuery ah = new AtivoBolsaQuery("A");
                    EmissorQuery emh = new EmissorQuery("E");
                    EspecificacaoBolsaQuery esph = new EspecificacaoBolsaQuery("ESP");

                    pbh.es.Distinct = true;

                    pbh.Select(pbh.IdCliente, esph.Tipo, pbh.Quantidade.Sum());
                    pbh.InnerJoin(ah).On(pbh.CdAtivoBolsa == ah.CdAtivoBolsa);
                    pbh.InnerJoin(emh).On(ah.IdEmissor == emh.IdEmissor);
                    pbh.InnerJoin(esph).On(ah.Especificacao == esph.Especificacao);
                    //
                    pbh.Where(pbh.IdCliente == idCliente &&
                              pbh.DataHistorico == this.data &&
                              ah.IdEmissor == this.emissor &&
                              pbh.Quantidade != 0);
                    //
                    pbh.GroupBy(pbh.IdCliente, esph.Tipo);
                    pbh.OrderBy(esph.Tipo.Ascending);
                    //
                    ph.Load(pbh);

                    for (int i = 0; i < ph.Count; i++) {
                        ValoresParticipacao vp = new ValoresParticipacao();

                        Cliente c = new Cliente();
                        c.LoadByPrimaryKey(idCliente);
                        //
                        vp.nomeCliente = c.Nome.Trim();
                        //
                        vp.quantidade = ph[i].Quantidade.Value;
                        //
                        int tipo = Convert.ToInt32(ph[i].GetColumn(EspecificacaoBolsaMetadata.ColumnNames.Tipo));
                        //
                        vp.tipoAcoes = tipo == (int)TipoEspecificacaoBolsa.Ordinaria ? "ON" : "PN";

                        if (votante != 0) {
                            vp.participacaoVotante = vp.quantidade / votante;
                        }

                        if (social != 0) {
                            vp.participacaoSocial = vp.quantidade / social;
                        }
                        //
                        listaVP.Add(vp);
                    }
                }
                #endregion
            }
            #endregion

            //#region Ordenação da Lista por Quantidade
            // Ordena por Quantidade
            //listaVP.Sort(delegate(ValoresParticipacao vp1, ValoresParticipacao vp2) { return vp2.quantidade.CompareTo(vp1.quantidade); });
            //#endregion

            #region for da Planilha

            int linha = 10; // Adiciona Linhas
            for (int i = 0; i < listaVP.Count; i++) {
                // Add new row
                worksheet1.Rows.Insert(linha, 1);
                //
                worksheet1.Rows[linha].Height = 22;
                //
                worksheet1.Rows[linha].Font = new Font("Calibri", 11, FontStyle.Regular);
                worksheet1.Rows[linha].FontColor = worksheet1.Rows[linha - 1][1].FontColor; // coluna B9
                // coluna B com borda esquerda
                worksheet1.Rows[linha][1].LeftBorderStyle = LineStyle.Double;
                // coluna G com borda direita
                worksheet1.Rows[linha][6].RightBorderStyle = LineStyle.Double;
                //
                linha++;
            }

            linha = 9; // linha inicial dos dados
            for (int j = 0; j < listaVP.Count; j++) {
                worksheet1.Rows[linha][1].Value = listaVP[j].nomeCliente;   // coluna B
                //
                worksheet1.Rows[linha][2].NumberFormatString = "#,##,#0"; // coluna C
                worksheet1.Rows[linha][2].ValueAsDouble = Convert.ToDouble(listaVP[j].quantidade);
                //
                worksheet1.Rows[linha][3].Value = listaVP[j].tipoAcoes; // coluna D                
                //
                worksheet1.Rows[linha][4].NumberFormatString = "#,##,#0.0000%"; // coluna E
                worksheet1.Rows[linha][4].Value = Convert.ToDouble(listaVP[j].participacaoVotante);
                //
                worksheet1.Rows[linha][6].NumberFormatString = "#,##,#0.0000%"; // coluna G
                worksheet1.Rows[linha][6].Value = Convert.ToDouble(listaVP[j].participacaoSocial);
                //
                worksheet1.Rows[linha][1].AlignmentHorizontal = Bytescout.Spreadsheet.Constants.AlignmentHorizontal.Left;
                worksheet1.Rows[linha][2].AlignmentHorizontal = Bytescout.Spreadsheet.Constants.AlignmentHorizontal.Right;
                worksheet1.Rows[linha][3].AlignmentHorizontal = Bytescout.Spreadsheet.Constants.AlignmentHorizontal.Centered;
                worksheet1.Rows[linha][4].AlignmentHorizontal = Bytescout.Spreadsheet.Constants.AlignmentHorizontal.Right;
                worksheet1.Rows[linha][6].AlignmentHorizontal = Bytescout.Spreadsheet.Constants.AlignmentHorizontal.Right;
                //
                linha++;
            }
            #endregion

            #region Coloca Total - colunas E e G
            if (listaVP.Count > 0) {
                int inicio = 10;
                int final = 9 + listaVP.Count;

                string sum1 = "E" + inicio + ":E" + final;
                worksheet1.Rows[linha + 1][4].NumberFormatString = "#,##,#0.0000%";
                worksheet1.Rows[linha + 1][4].Value = "=SUM(" + sum1 + ")";
                //
                string sum2 = "G" + inicio + ":G" + final;
                worksheet1.Rows[linha + 1][6].NumberFormatString = "#,##,#0.0000%";
                worksheet1.Rows[linha + 1][6].Value = "=SUM(" + sum2 + ")";
            }
            #endregion

           #endregion
        }
    }
}