﻿using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Financial.Fundo;
using System.Collections.Generic;
using Financial.Common;
using Financial.Investidor;
using Financial.Investidor.Enums;
using Financial.Util;
using Financial.Common.Enums;

namespace Financial.Export
{

    /// <summary>
    /// Summary description for RentabilPorto
    /// </summary>
    public class RentabilPorto
    {
        public class Retorno
        {
            private string grupo;
            public string Grupo
            {
                get { return grupo; }
                set { grupo = value; }
            }

            private int idCarteira;
            public int IdCarteira
            {
                get { return idCarteira; }
                set { idCarteira = value; }
            }

            private string nomeCarteira;
            public string NomeCarteira
            {
                get { return nomeCarteira; }
                set { nomeCarteira = value; }
            }

            private decimal? retornoDia;
            public decimal? RetornoDia
            {
                get { return retornoDia==0?null:retornoDia; }
                set { retornoDia = value; }
            }

            private decimal? retornoMes;
            public decimal? RetornoMes
            {
                get { return retornoMes == 0 ? null : retornoMes; }
                set { retornoMes = value; }
            }

            private decimal? retornoAno;
            public decimal? RetornoAno
            {
                get { return retornoAno == 0 ? null : retornoAno; }
                set { retornoAno = value; }
            }

            private decimal? retorno12Meses;
            public decimal? Retorno12Meses
            {
                get { return retorno12Meses == 0 ? null : retorno12Meses; }
                set { retorno12Meses = value; }
            }

            private decimal? retornoAnoAnterior;
            public decimal? RetornoAnoAnterior
            {
                get { return retornoAnoAnterior == 0 ? null : retornoAnoAnterior; }
                set { retornoAnoAnterior = value; }
            }

            private decimal? retornoTrimestre1;
            public decimal? RetornoTrimestre1
            {
                get { return retornoTrimestre1 == 0 ? null : retornoTrimestre1; }
                set { retornoTrimestre1 = value; }
            }

            private decimal? retornoTrimestre2;
            public decimal? RetornoTrimestre2
            {
                get { return retornoTrimestre2 == 0 ? null : retornoTrimestre2; }
                set { retornoTrimestre2 = value; }
            }

            private decimal? retornoTrimestre3;
            public decimal? RetornoTrimestre3
            {
                get { return retornoTrimestre3 == 0 ? null : retornoTrimestre3; }
                set { retornoTrimestre3 = value; }
            }

            private decimal? retornoTrimestre4;
            public decimal? RetornoTrimestre4
            {
                get { return retornoTrimestre4 == 0 ? null : retornoTrimestre4; }
                set { retornoTrimestre4 = value; }
            }

            private decimal? retornoDiaBenckmark;
            public decimal? RetornoDiaBenckmark
            {
                get { return retornoDiaBenckmark == 0 ? null : retornoDiaBenckmark; }
                set { retornoDiaBenckmark = value; }
            }

            private decimal? retornoMesBenckmark;
            public decimal? RetornoMesBenckmark
            {
                get { return retornoMesBenckmark == 0 ? null : retornoMesBenckmark; }
                set { retornoMesBenckmark = value; }
            }

            private decimal? retornoAnoBenckmark;
            public decimal? RetornoAnoBenckmark
            {
                get { return retornoAnoBenckmark == 0 ? null : retornoAnoBenckmark; }
                set { retornoAnoBenckmark = value; }
            }

            private decimal? retorno12MesesBenckmark;
            public decimal? Retorno12MesesBenckmark
            {
                get { return retorno12MesesBenckmark == 0 ? null : retorno12MesesBenckmark; }
                set { retorno12MesesBenckmark = value; }
            }

            private decimal? retornoAnoAnteriorBenckmark;
            public decimal? RetornoAnoAnteriorBenckmark
            {
                get { return retornoAnoAnteriorBenckmark == 0 ? null : retornoAnoAnteriorBenckmark; }
                set { retornoAnoAnteriorBenckmark = value; }
            }

            private decimal? retornoTrimestre1Benckmark;
            public decimal? RetornoTrimestre1Benckmark
            {
                get { return retornoTrimestre1Benckmark == 0 ? null : retornoTrimestre1Benckmark; }
                set { retornoTrimestre1Benckmark = value; }
            }

            private decimal? retornoTrimestre2Benckmark;
            public decimal? RetornoTrimestre2Benckmark
            {
                get { return retornoTrimestre2Benckmark == 0 ? null : retornoTrimestre2Benckmark; }
                set { retornoTrimestre2Benckmark = value; }
            }

            private decimal? retornoTrimestre3Benckmark;
            public decimal? RetornoTrimestre3Benckmark
            {
                get { return retornoTrimestre3Benckmark == 0 ? null : retornoTrimestre3Benckmark; }
                set { retornoTrimestre3Benckmark = value; }
            }

            private decimal? retornoTrimestre4Benckmark;
            public decimal? RetornoTrimestre4Benckmark
            {
                get { return retornoTrimestre4Benckmark == 0 ? null : retornoTrimestre4Benckmark; }
                set { retornoTrimestre4Benckmark = value; }
            }

            private decimal? patrimonio;
            public decimal? Patrimonio
            {
                get { return patrimonio == 0 ? null : patrimonio; }
                set { patrimonio = value; }
            }

            private int? orderGrupo;
            public int? OrderGrupo
            {
                get { return orderGrupo; }
                set { orderGrupo = value; }
            }

            private int? orderCarteira;
            public int? OrderCarteira
            {
                get { return orderCarteira; }
                set { orderCarteira = value; }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="idCarteira"></param>
        /// <param name="dia"></param>
        /// <param name="idIndice"></param>
        /// <returns></returns>
        public List<Retorno> RetornaListRetorno(DateTime data)
        {
            DateTime dataInicioAnoAnterior = new DateTime(data.Year - 1, 01, 01);
            dataInicioAnoAnterior = Calendario.SubtraiDiaUtil(dataInicioAnoAnterior, 1);

            DateTime dataFimAnoAnterior = new DateTime(data.Year - 1, 12, 31);
            if (!Calendario.IsDiaUtil(dataFimAnoAnterior))
            {
                dataFimAnoAnterior = Calendario.SubtraiDiaUtil(dataFimAnoAnterior, 1);
            }

            DateTime dataFimTrimestre1 = new DateTime(data.Year, 03, 31);
            if (!Calendario.IsDiaUtil(dataFimTrimestre1))
            {
                dataFimTrimestre1 = Calendario.SubtraiDiaUtil(dataFimTrimestre1, 1);
            }

            DateTime dataFimTrimestre2 = new DateTime(data.Year, 06, 30);
            if (!Calendario.IsDiaUtil(dataFimTrimestre2))
            {
                dataFimTrimestre2 = Calendario.SubtraiDiaUtil(dataFimTrimestre2, 1);
            }

            DateTime dataFimTrimestre3 = new DateTime(data.Year, 09, 30);
            if (!Calendario.IsDiaUtil(dataFimTrimestre3))
            {
                dataFimTrimestre3 = Calendario.SubtraiDiaUtil(dataFimTrimestre3, 1);
            }

            DateTime dataFimTrimestre4 = new DateTime(data.Year, 12, 31);
            if (!Calendario.IsDiaUtil(dataFimTrimestre4))
            {
                dataFimTrimestre4 = Calendario.SubtraiDiaUtil(dataFimTrimestre4, 1);
            }

            ClienteQuery clienteQuery = new ClienteQuery("C");
            CarteiraQuery carteiraQuery = new CarteiraQuery("A");
            GrupoEconomicoQuery grupoEconomicoQuery = new GrupoEconomicoQuery("G");

            clienteQuery.Select(clienteQuery.IdCliente,
                                clienteQuery.DataDia,
                                clienteQuery.Status,
                                carteiraQuery.IdGrupoEconomico,
                                grupoEconomicoQuery.OrdemRelatorio.Coalesce("99999999").Round(0).As("OrderGrupo"),
                                carteiraQuery.OrdemRelatorio.Coalesce("99999999").Round(0).As("OrderCarteira"));
            clienteQuery.InnerJoin(carteiraQuery).On(clienteQuery.IdCliente == carteiraQuery.IdCarteira);
            clienteQuery.LeftJoin(grupoEconomicoQuery).On(carteiraQuery.IdGrupoEconomico == grupoEconomicoQuery.IdGrupo);
            clienteQuery.Where(clienteQuery.TipoControle.Equal((byte)TipoControleCliente.Completo),
                               clienteQuery.StatusAtivo.Equal((byte)StatusAtivoCliente.Ativo));
            clienteQuery.OrderBy(grupoEconomicoQuery.OrdemRelatorio.Coalesce("99999999").Ascending,
                carteiraQuery.OrdemRelatorio.Coalesce("99999999").Ascending);
            ClienteCollection clienteCollection = new ClienteCollection();
            clienteCollection.Load(clienteQuery);

            clienteCollection.Sort = "OrderGrupo asc, OrderCarteira asc";

            Retorno retorno;
            CalculoMedida calculoMedida;
            List<Retorno> listRetorno = new List<Retorno>();

            foreach (Cliente cliente in clienteCollection)
            {
                int idCarteira = cliente.IdCliente.Value;
                DateTime dataDia = cliente.DataDia.Value;

                bool calculado = cliente.Status.Value == (byte)StatusCliente.Fechado || cliente.Status.Value == (byte)StatusCliente.Divulgado;
                
                int? idGrupo = null;
                if (cliente.GetColumn(CarteiraMetadata.ColumnNames.IdGrupoEconomico) != DBNull.Value)
                {
                    idGrupo = (int)cliente.GetColumn(CarteiraMetadata.ColumnNames.IdGrupoEconomico);
                }

                if (idGrupo.HasValue)
                {
                    GrupoEconomico grupoEconomico = new GrupoEconomico();
                    grupoEconomico.LoadByPrimaryKey(idGrupo.Value);

                    calculoMedida = new CalculoMedida(idCarteira, (int)ListaIndiceFixo.CDI);

                    retorno = new Retorno();
                    retorno.IdCarteira = idCarteira;
                    retorno.NomeCarteira = calculoMedida.carteira.Nome;
                    retorno.Grupo = grupoEconomico.Nome;

                    try
                    {
                        retorno.RetornoDia = calculoMedida.CalculaRetornoDia(data);
                    }
                    catch (Exception)
                    {
                    }

                    try
                    {
                        retorno.RetornoMes = calculoMedida.CalculaRetornoMes(data);
                    }
                    catch (Exception)
                    {
                    }

                    try
                    {
                        retorno.RetornoAno = calculoMedida.CalculaRetornoAno(data);
                    }
                    catch (Exception)
                    {
                    }

                    try
                    {
                        retorno.Retorno12Meses = calculoMedida.CalculaRetornoPeriodoMes(data, 12);
                    }
                    catch (Exception)
                    {                        
                    }
                                        
                    try
                    {
                        retorno.RetornoAnoAnterior = calculoMedida.CalculaRetorno(dataInicioAnoAnterior, dataFimAnoAnterior);
                    }
                    catch (Exception)
                    {
                    }



                    //AJUSTE DE DATA PARA TRATAMENTOS DOS TRIMESTRES
                    DateTime dataAuxiliar = data;
                    if (dataDia < dataAuxiliar)
                    {
                        dataAuxiliar = dataDia;
                    }
                    else if (dataDia == dataAuxiliar && !calculado)
                    {
                        dataAuxiliar = Calendario.SubtraiDiaUtil(data, 1);
                    }


                    if (dataAuxiliar < dataFimTrimestre1)
                    {
                        dataFimTrimestre1 = dataAuxiliar;
                    }

                    if (dataAuxiliar < dataFimTrimestre2)
                    {
                        dataFimTrimestre2 = dataAuxiliar;
                    }

                    if (dataAuxiliar < dataFimTrimestre3)
                    {
                        dataFimTrimestre3 = dataAuxiliar;
                    }

                    if (dataAuxiliar < dataFimTrimestre4)
                    {
                        dataFimTrimestre4 = dataAuxiliar;
                    }


                    if (dataFimAnoAnterior <= dataFimTrimestre1)
                    {
                        try
                        {
                            retorno.RetornoTrimestre1 = calculoMedida.CalculaRetorno(dataFimAnoAnterior, dataFimTrimestre1);
                        }
                        catch (Exception)
                        {
                        }
                    }

                    if (dataFimTrimestre1 <= dataFimTrimestre2)
                    {
                        try
                        {
                            retorno.RetornoTrimestre2 = calculoMedida.CalculaRetorno(dataFimTrimestre1, dataFimTrimestre2);
                        }
                        catch (Exception)
                        {
                        }
                    }
                    
                    if (dataFimTrimestre2 <= dataFimTrimestre3)
                    {
                        try
                        {
                            retorno.RetornoTrimestre3 = calculoMedida.CalculaRetorno(dataFimTrimestre2, dataFimTrimestre3);
                        }
                        catch (Exception)
                        {
                        }
                    }
                    
                    if (dataFimTrimestre3 <= dataFimTrimestre4)
                    {
                        try
                        {
                            retorno.RetornoTrimestre4 = calculoMedida.CalculaRetorno(dataFimTrimestre3, dataFimTrimestre4);
                        }
                        catch (Exception)
                        {
                        }
                    }

                    try
                    {
                        retorno.RetornoDiaBenckmark = calculoMedida.CalculaRetornoDiaIndice(data);
                        retorno.RetornoDiaBenckmark = retorno.RetornoDia / retorno.RetornoDiaBenckmark * 100M;
                    }
                    catch (Exception)
                    {
                    }

                    try
                    {
                        retorno.RetornoMesBenckmark = calculoMedida.CalculaRetornoMesIndice(data);
                        retorno.RetornoMesBenckmark = retorno.RetornoMes / retorno.RetornoMesBenckmark * 100M;
                    }
                    catch (Exception)
                    {
                    }

                    try
                    {
                        retorno.RetornoAnoBenckmark = calculoMedida.CalculaRetornoAnoIndice(data);
                        retorno.RetornoAnoBenckmark = retorno.RetornoAno / retorno.RetornoAnoBenckmark * 100M;
                    }
                    catch (Exception)
                    {
                    }

                    try
                    {
                        retorno.Retorno12MesesBenckmark = calculoMedida.CalculaRetornoPeriodoMesIndice(data, 12);
                        retorno.Retorno12MesesBenckmark = retorno.Retorno12Meses / retorno.Retorno12MesesBenckmark * 100M;
                    }
                    catch (Exception)
                    {
                    }

                    try
                    {
                        retorno.RetornoAnoAnteriorBenckmark = calculoMedida.CalculaRetornoIndice(dataInicioAnoAnterior, dataFimAnoAnterior);
                        retorno.RetornoAnoAnteriorBenckmark = retorno.RetornoAnoAnterior / retorno.RetornoAnoAnteriorBenckmark * 100M;
                    }
                    catch (Exception)
                    {
                    }

                    try
                    {
                        retorno.RetornoTrimestre1Benckmark = calculoMedida.CalculaRetornoIndice(dataFimAnoAnterior, dataFimTrimestre1);
                        retorno.RetornoTrimestre1Benckmark = retorno.RetornoTrimestre1 / retorno.RetornoTrimestre1Benckmark * 100M;
                    }
                    catch (Exception)
                    {
                    }

                    try
                    {
                        retorno.RetornoTrimestre2Benckmark = calculoMedida.CalculaRetornoIndice(dataFimTrimestre1, dataFimTrimestre2);
                        retorno.RetornoTrimestre2Benckmark = retorno.RetornoTrimestre2 / retorno.RetornoTrimestre2Benckmark * 100M;
                    }
                    catch (Exception)
                    {
                    }

                    try
                    {
                        retorno.RetornoTrimestre3Benckmark = calculoMedida.CalculaRetornoIndice(dataFimTrimestre2, dataFimTrimestre3);
                        retorno.RetornoTrimestre3Benckmark = retorno.RetornoTrimestre3 / retorno.RetornoTrimestre3Benckmark * 100M;
                    }
                    catch (Exception)
                    {
                    }

                    try
                    {
                        retorno.RetornoTrimestre4Benckmark = calculoMedida.CalculaRetornoIndice(dataFimTrimestre3, dataFimTrimestre4);
                        retorno.RetornoTrimestre4Benckmark = retorno.RetornoTrimestre4 / retorno.RetornoTrimestre4Benckmark * 100M;
                    }
                    catch (Exception)
                    {
                    }
                    
                    HistoricoCota h = new HistoricoCota();
                    try
                    {
                        h.BuscaValorPatrimonioDia(idCarteira, data);
                    }
                    catch { }
                    decimal valorPL = h.PLFechamento.HasValue ? h.PLFechamento.Value : 0;

                    retorno.Patrimonio = Math.Round(valorPL / 1000M, 2);

                    retorno.OrderGrupo = (int?)cliente.GetColumn("OrderGrupo");

                    retorno.OrderCarteira = (int?)cliente.GetColumn("OrderCarteira");

                    listRetorno.Add(retorno);
                }
            }

            calculoMedida = new CalculoMedida((short)ListaIndiceFixo.CDI);
            #region CDI
            retorno = new Retorno();
            retorno.IdCarteira = 0;
            retorno.NomeCarteira = "CDI";
            retorno.Grupo = "Indicadores";

            try
            {
                retorno.RetornoDia = calculoMedida.CalculaRetornoDiaIndice(data);
            }
            catch (Exception)
            {
            }

            try
            {
                retorno.RetornoMes = calculoMedida.CalculaRetornoMesIndice(data);
            }
            catch (Exception)
            {
            }

            try
            {
                retorno.RetornoAno = calculoMedida.CalculaRetornoAnoIndice(data);
            }
            catch (Exception)
            {
            }

            try
            {
                retorno.Retorno12Meses = calculoMedida.CalculaRetornoPeriodoMesIndice(data, 12);
            }
            catch (Exception)
            {
            }

            try
            {
                retorno.RetornoAnoAnterior = calculoMedida.CalculaRetornoIndice(dataInicioAnoAnterior, dataFimAnoAnterior);
            }
            catch (Exception)
            {
            }

            try
            {
                retorno.RetornoTrimestre1 = calculoMedida.CalculaRetornoIndice(dataFimAnoAnterior, dataFimTrimestre1);
            }
            catch (Exception)
            {
            }

            try
            {
                retorno.RetornoTrimestre2 = calculoMedida.CalculaRetornoIndice(dataFimTrimestre1, dataFimTrimestre2);
            }
            catch (Exception)
            {
            }

            retorno.OrderGrupo = 999999999;

            retorno.OrderCarteira = 1;

            listRetorno.Add(retorno);
            #endregion

            calculoMedida = new CalculoMedida((short)ListaIndiceFixo.IGPM);
            #region IGPM
            retorno = new Retorno();
            retorno.IdCarteira = 0;
            retorno.NomeCarteira = "IGPM";
            retorno.Grupo = "Indicadores";

            try
            {
                retorno.RetornoDia = calculoMedida.CalculaRetornoDiaIndice(data);
            }
            catch (Exception)
            {
            }

            try
            {
                retorno.RetornoMes = calculoMedida.CalculaRetornoMesIndice(data);
            }
            catch (Exception)
            {
            }

            try
            {
                retorno.RetornoAno = calculoMedida.CalculaRetornoAnoIndice(data);
            }
            catch (Exception)
            {
            }

            try
            {
                retorno.Retorno12Meses = calculoMedida.CalculaRetornoPeriodoMesIndice(data, 12);
            }
            catch (Exception)
            {
            }

            try
            {
                retorno.RetornoAnoAnterior = calculoMedida.CalculaRetornoIndice(dataInicioAnoAnterior, dataFimAnoAnterior);
            }
            catch (Exception)
            {
            }

            try
            {
                retorno.RetornoTrimestre1 = calculoMedida.CalculaRetornoIndice(dataFimAnoAnterior, dataFimTrimestre1);
            }
            catch (Exception)
            {
            }

            try
            {
                retorno.RetornoTrimestre2 = calculoMedida.CalculaRetornoIndice(dataFimTrimestre1, dataFimTrimestre2);
            }
            catch (Exception)
            {
            }

            retorno.OrderGrupo = 999999999;

            retorno.OrderCarteira = 2;

            listRetorno.Add(retorno);
            #endregion

            calculoMedida = new CalculoMedida((short)ListaIndiceFixo.PTAX_800VENDA);
            #region PTAX_800VENDA
            retorno = new Retorno();
            retorno.IdCarteira = 0;
            retorno.NomeCarteira = "DÓLAR";
            retorno.Grupo = "Indicadores";

            try
            {
                retorno.RetornoDia = calculoMedida.CalculaRetornoDiaIndice(data);
            }
            catch (Exception)
            {
            }

            try
            {
                retorno.RetornoMes = calculoMedida.CalculaRetornoMesIndice(data);
            }
            catch (Exception)
            {
            }

            try
            {
                retorno.RetornoAno = calculoMedida.CalculaRetornoAnoIndice(data);
            }
            catch (Exception)
            {
            }

            try
            {
                retorno.Retorno12Meses = calculoMedida.CalculaRetornoPeriodoMesIndice(data, 12);
            }
            catch (Exception)
            {
            }

            try
            {
                retorno.RetornoAnoAnterior = calculoMedida.CalculaRetornoIndice(dataInicioAnoAnterior, dataFimAnoAnterior);
            }
            catch (Exception)
            {
            }

            try
            {
                retorno.RetornoTrimestre1 = calculoMedida.CalculaRetornoIndice(dataFimAnoAnterior, dataFimTrimestre1);
            }
            catch (Exception)
            {
            }

            try
            {
                retorno.RetornoTrimestre2 = calculoMedida.CalculaRetornoIndice(dataFimTrimestre1, dataFimTrimestre2);
            }
            catch (Exception)
            {
            }

            retorno.OrderGrupo = 999999999;

            retorno.OrderCarteira = 3;

            listRetorno.Add(retorno);
            #endregion

            calculoMedida = new CalculoMedida((short)ListaIndiceFixo.IBOVESPA_FECHA);
            #region IBOVESPA_FECHA
            retorno = new Retorno();
            retorno.IdCarteira = 0;
            retorno.NomeCarteira = "IBOVESPA";
            retorno.Grupo = "Indicadores";

            try
            {
                retorno.RetornoDia = calculoMedida.CalculaRetornoDiaIndice(data);
            }
            catch (Exception)
            {
            }

            try
            {
                retorno.RetornoMes = calculoMedida.CalculaRetornoMesIndice(data);
            }
            catch (Exception)
            {
            }

            try
            {
                retorno.RetornoAno = calculoMedida.CalculaRetornoAnoIndice(data);
            }
            catch (Exception)
            {
            }

            try
            {
                retorno.Retorno12Meses = calculoMedida.CalculaRetornoPeriodoMesIndice(data, 12);
            }
            catch (Exception)
            {
            }

            try
            {
                retorno.RetornoAnoAnterior = calculoMedida.CalculaRetornoIndice(dataInicioAnoAnterior, dataFimAnoAnterior);
            }
            catch (Exception)
            {
            }

            try
            {
                retorno.RetornoTrimestre1 = calculoMedida.CalculaRetornoIndice(dataFimAnoAnterior, dataFimTrimestre1);
            }
            catch (Exception)
            {
            }

            try
            {
                retorno.RetornoTrimestre2 = calculoMedida.CalculaRetornoIndice(dataFimTrimestre1, dataFimTrimestre2);
            }
            catch (Exception)
            {
            }

            retorno.OrderGrupo = 999999999;

            retorno.OrderCarteira = 4;

            listRetorno.Add(retorno);
            #endregion

            return listRetorno;
        }
    }
}