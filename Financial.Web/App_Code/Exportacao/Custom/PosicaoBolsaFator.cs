﻿using System;
using System.IO;
using Bytescout.Spreadsheet;
using Bytescout.Spreadsheet.Constants;
using Financial.Investidor;
using Financial.Investidor.Enums;
using System.Collections.Generic;
using System.Drawing;
using Financial.Common;
using Financial.ContaCorrente;
using Financial.Util;
using Financial.Bolsa;
using System.Globalization;
using Financial.Common.Enums;
using Financial.RendaFixa;
using Financial.RendaFixa.Enums;
using Financial.ContaCorrente.Enums;
using Financial.Fundo;
using Financial.BMF;
using Financial.Bolsa.Enums;
using System.Xml;
using System.Collections;
using System.Web;
using Financial.Security;

namespace Financial.Export {

    /// <summary>
    /// Summary description for IntegralTrust
    /// </summary>
    public class PosicaoBolsaFator {
        public class Posicao
        {
            public string custodia;
            public string Custodia
            {
                get { return custodia; }
            }

            public int idAgente;
            public int IdAgente
            {
                get { return idAgente; }
            }

            public int idCliente;
            public int IdCliente
            {
                get { return idCliente; }
            }

            public string nomeCliente;
            public string NomeCliente
            {
                get { return idCliente.ToString() + " - " + nomeCliente; }
            }

            public string cdAtivoBolsa;
            public string CdAtivoBolsa
            {
                get { return cdAtivoBolsa; }
            }

            public string especificacao;
            public string Especificacao
            {
                get { return especificacao; }
            }

            public decimal quantidadeVista;
            public decimal QuantidadeVista
            {
                get { return quantidadeVista; }
            }

            public decimal quantidadeTomada;
            public decimal QuantidadeTomada
            {
                get { return quantidadeTomada; }
            }

            public decimal quantidadeDoada;
            public decimal QuantidadeDoada
            {
                get { return quantidadeDoada; }
            }

            public decimal netting;
            public decimal Netting
            {
                get { return netting; }
            }

            public decimal valorCusto;
            public decimal ValorCusto
            {
                get { return valorCusto; }
            }

            public decimal valorMercado;
            public decimal ValorMercado
            {
                get { return valorMercado; }
            }

            public decimal resultado;
            public decimal Resultado
            {
                get { return resultado; }
            }

            public decimal? percResultado;
            public decimal? PercResultado
            {
                get { return percResultado; }
            }

            public decimal? percAcoes;
            public decimal? PercAcoes
            {
                get { return percAcoes; }
            }

            public decimal? percPL;
            public decimal? PercPL
            {
                get { return percPL; }
            }

        }

        public List<Posicao> listaPosicao = new List<Posicao>();
        
        private DateTime dataReferencia;

        /* Construtor */
        public PosicaoBolsaFator(DateTime dataReferencia) 
        {
            PermissaoClienteQuery permissaoClienteQuery = new PermissaoClienteQuery("P");
            UsuarioQuery usuarioQuery = new UsuarioQuery("U");
            ClienteQuery clienteQuery = new ClienteQuery("C");
            clienteQuery.Select(clienteQuery.IdCliente,
                                clienteQuery.Status,
                                clienteQuery.Nome,
                                clienteQuery.DataDia);
            clienteQuery.InnerJoin(permissaoClienteQuery).On(permissaoClienteQuery.IdCliente == clienteQuery.IdCliente);
            clienteQuery.InnerJoin(usuarioQuery).On(usuarioQuery.IdUsuario == permissaoClienteQuery.IdUsuario);
            clienteQuery.Where(clienteQuery.TipoControle.Equal((byte)TipoControleCliente.Completo),
                               clienteQuery.StatusAtivo.Equal((byte)StatusAtivoCliente.Ativo),
                               usuarioQuery.Login == HttpContext.Current.User.Identity.Name);
            ClienteCollection clienteCollection = new ClienteCollection();
            clienteCollection.Load(clienteQuery);

            foreach (Cliente cliente in clienteCollection)
            {
                int idCliente = cliente.IdCliente.Value;
                string nome = cliente.Nome;

                try
                {
                    decimal? valorPL = null;
                    HistoricoCota historicoCota = new HistoricoCota();
                    try
                    {
                        historicoCota.BuscaValorPatrimonioDia(idCliente, dataReferencia);
                        valorPL = historicoCota.PLFechamento.Value;
                    }
                    catch { }
 
                    #region Posicao A vista e FII
                    PosicaoBolsaHistoricoCollection posicaoBolsaHistoricoCollection = new PosicaoBolsaHistoricoCollection();
                    posicaoBolsaHistoricoCollection.Query.Where(posicaoBolsaHistoricoCollection.Query.IdCliente.Equal(idCliente),
                                                                posicaoBolsaHistoricoCollection.Query.DataHistorico.Equal(dataReferencia),
                                                                posicaoBolsaHistoricoCollection.Query.TipoMercado.In(TipoMercadoBolsa.MercadoVista, TipoMercadoBolsa.Imobiliario));
                    posicaoBolsaHistoricoCollection.Query.OrderBy(posicaoBolsaHistoricoCollection.Query.IdCliente.Ascending,
                                                                  posicaoBolsaHistoricoCollection.Query.IdAgente.Ascending,
                                                                  posicaoBolsaHistoricoCollection.Query.CdAtivoBolsa.Ascending);
                    posicaoBolsaHistoricoCollection.Query.Load();

                    foreach (PosicaoBolsaHistorico posicaoBolsaHistorico in posicaoBolsaHistoricoCollection)
                    {
                        int idAgente = posicaoBolsaHistorico.IdAgente.Value;

                        AtivoBolsa ativoBolsa = new AtivoBolsa();
                        ativoBolsa.Query.Select(ativoBolsa.Query.Especificacao);
                        ativoBolsa.Query.Where(ativoBolsa.Query.CdAtivoBolsa.Equal(posicaoBolsaHistorico.CdAtivoBolsa));
                        ativoBolsa.Query.Load();

                        AgenteMercado agenteMercado = new AgenteMercado();
                        agenteMercado.LoadByPrimaryKey(idAgente);

                        decimal quantidade = posicaoBolsaHistorico.Quantidade.Value;
                        decimal valorMercado = posicaoBolsaHistorico.ValorMercado.Value;
                        decimal valorCusto = posicaoBolsaHistorico.ValorCustoLiquido.Value;
                        decimal resultado = posicaoBolsaHistorico.ResultadoRealizar.Value;                        
                        
                        Posicao posicao = new Posicao();
                        posicao.idCliente = idCliente;
                        posicao.nomeCliente = nome;
                        posicao.cdAtivoBolsa = posicaoBolsaHistorico.CdAtivoBolsa;
                        posicao.especificacao = ativoBolsa.Especificacao;
                        posicao.netting = quantidade;
                        posicao.percAcoes = 0;
                        posicao.percPL = 0;
                        posicao.percResultado = 0;
                        posicao.quantidadeDoada = 0;
                        posicao.quantidadeTomada = 0;
                        posicao.quantidadeVista = quantidade;
                        posicao.resultado = resultado;
                        posicao.valorCusto = valorCusto;
                        posicao.valorMercado = valorMercado;
                        posicao.idAgente = idAgente;
                        posicao.custodia = agenteMercado.Nome;

                        this.listaPosicao.Add(posicao);
                    }
                    #endregion

                    #region BTC
                    PosicaoEmprestimoBolsaHistoricoCollection posicaoEmprestimoBolsaHistoricoCollection = new PosicaoEmprestimoBolsaHistoricoCollection();
                    posicaoEmprestimoBolsaHistoricoCollection.Query.Where(posicaoEmprestimoBolsaHistoricoCollection.Query.IdCliente.Equal(idCliente),
                                                                          posicaoEmprestimoBolsaHistoricoCollection.Query.DataHistorico.Equal(dataReferencia));
                    posicaoEmprestimoBolsaHistoricoCollection.Query.OrderBy(posicaoEmprestimoBolsaHistoricoCollection.Query.IdCliente.Ascending,
                                                                            posicaoEmprestimoBolsaHistoricoCollection.Query.IdAgente.Ascending,
                                                                            posicaoEmprestimoBolsaHistoricoCollection.Query.CdAtivoBolsa.Ascending);
                    posicaoEmprestimoBolsaHistoricoCollection.Query.Load();

                    foreach (PosicaoEmprestimoBolsaHistorico posicaoEmprestimoBolsaHistorico in posicaoEmprestimoBolsaHistoricoCollection)
                    {
                        int idAgente = posicaoEmprestimoBolsaHistorico.IdAgente.Value;
                        string cdAtivoBolsa = posicaoEmprestimoBolsaHistorico.CdAtivoBolsa;
                        byte ponta = posicaoEmprestimoBolsaHistorico.PontaEmprestimo.Value;

                        AtivoBolsa ativoBolsa = new AtivoBolsa();
                        ativoBolsa.Query.Select(ativoBolsa.Query.Especificacao);
                        ativoBolsa.Query.Where(ativoBolsa.Query.CdAtivoBolsa.Equal(cdAtivoBolsa));
                        ativoBolsa.Query.Load();

                        FatorCotacaoBolsa f = new FatorCotacaoBolsa();
                        f.BuscaFatorCotacaoBolsa(cdAtivoBolsa, dataReferencia);

                        AgenteMercado agenteMercado = new AgenteMercado();
                        agenteMercado.LoadByPrimaryKey(idAgente);

                        decimal quantidade = posicaoEmprestimoBolsaHistorico.Quantidade.Value;
                        decimal valorMercado = posicaoEmprestimoBolsaHistorico.ValorMercado.Value;
                        decimal custoBTC = (quantidade * posicaoEmprestimoBolsaHistorico.PULiquidoOriginal.Value) / f.Fator.Value;
                        decimal resultado = valorMercado - custoBTC;

                        bool achouPosicao = false;
                        for (int c = 0; c<this.listaPosicao.Count; c++)
                        {
                            Posicao posicao = this.listaPosicao[c];
                            if (posicao.cdAtivoBolsa == cdAtivoBolsa && posicao.idAgente == idAgente)
                            {
                                achouPosicao = true;
                                if (ponta == (byte)PontaEmprestimoBolsa.Doador)
                                {
                                    posicao.quantidadeDoada += quantidade;
                                    posicao.netting += quantidade;
                                    posicao.valorMercado += valorMercado;
                                    posicao.valorCusto += custoBTC;
                                    posicao.resultado += resultado;
                                }
                                else
                                {
                                    posicao.quantidadeTomada += quantidade;
                                    posicao.netting -= quantidade;
                                    posicao.valorMercado -= valorMercado;
                                    posicao.valorCusto += custoBTC;
                                    posicao.resultado += resultado;
                                }
                            }
                        }
                        if (achouPosicao = false)
                        {
                            Posicao posicaoNew = new Posicao();
                            posicaoNew.idCliente = idCliente;
                            posicaoNew.nomeCliente = nome;
                            posicaoNew.idAgente = idAgente;
                            posicaoNew.custodia = agenteMercado.Nome;
                            posicaoNew.cdAtivoBolsa = cdAtivoBolsa;
                            posicaoNew.especificacao = ativoBolsa.Especificacao;
                            posicaoNew.percAcoes = 0;
                            posicaoNew.percPL = 0;
                            posicaoNew.percResultado = 0;

                            if (ponta == (byte)PontaEmprestimoBolsa.Doador)
                            {
                                posicaoNew.quantidadeVista = 0;
                                posicaoNew.netting = quantidade;
                                posicaoNew.quantidadeDoada = quantidade;
                                posicaoNew.quantidadeTomada = 0;
                                posicaoNew.resultado = resultado;
                                posicaoNew.valorCusto = custoBTC;
                                posicaoNew.valorMercado = valorMercado;                                    
                            }
                            else
                            {
                                posicaoNew.quantidadeVista = 0;
                                posicaoNew.netting = quantidade * -1;
                                posicaoNew.quantidadeDoada = quantidade;
                                posicaoNew.quantidadeTomada = 0;
                                posicaoNew.resultado = resultado * -1;
                                posicaoNew.valorCusto = custoBTC * -1;
                                posicaoNew.valorMercado = valorMercado * -1;
                            }

                            this.listaPosicao.Add(posicaoNew);
                        }
                        
                    }
                    #endregion

                    decimal valorAcoes = 0;                    
                    foreach (Posicao pTot in this.listaPosicao)
                    {
                        valorAcoes += pTot.valorMercado;
                    }

                    foreach (Posicao pLista in this.listaPosicao)
                    {
                        if (valorAcoes != 0)
                        {
                            pLista.percAcoes = Math.Round((pLista.valorMercado / valorAcoes) * 100, 4);
                        }
                        if (valorPL != null && valorPL.Value != 0)
                        {
                            pLista.percPL = Math.Round((pLista.valorMercado / (decimal)valorPL) * 100, 4);
                        }
                        if (pLista.valorCusto != 0)
                        {
                            pLista.percResultado = Math.Round((pLista.resultado / pLista.valorCusto) * 100, 4);
                        }
                    }
                }
                catch (Exception)
                {                    
                    throw;
                }
                
            }
        }
    }
}