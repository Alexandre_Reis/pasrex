﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using System.Data;
using System.Configuration;
using System.Web.Configuration;
using System.Web;
using System.Text;
using EntitySpaces.Core;
using EntitySpaces.Interfaces;
using System.IO;
using System.Collections.Generic;
using Financial.Fundo;
using Financial.Investidor;
using Financial.Fundo.Enums;
using Financial.Fundo.Exceptions;
using Financial.Common;
using Financial.Common.Enums;
using Financial.CRM.Enums;
using Financial.CRM;

namespace Financial.Relatorio {

    /// <summary>
    /// Summary description for SubReportCotaInvestimentoMasa
    /// </summary>
    public class SubReportCotaInvestimentoMasa : XtraReport {

        private DateTime dataReferencia;

        public DateTime DataReferencia {
            get { return dataReferencia; }
            set { dataReferencia = value; }
        }

        private int idCarteira;

        public int IdCarteira {
            get { return idCarteira; }
            set { idCarteira = value; }
        }
        //
        private Carteira carteira;

        private int idMoeda;

        public int IdMoeda {
            get { return idMoeda; }
            set { idMoeda = value; }
        }

        private int numeroLinhasDataTable;

        /// <summary>
        /// Retorna true se relatorio tem dados
        /// </summary>
        public bool HasData {
            get { return this.numeroLinhasDataTable != 0; }
        }

        private enum TipoPesquisa {
            PosicaoFundo = 0,
            PosicaoFundoHistorico = 1
        }
        TipoPesquisa tipoPesquisa = TipoPesquisa.PosicaoFundo;
        //

        TipoPessoa tipoPessoa; 

        //
        private DevExpress.XtraReports.UI.DetailBand Detail;
        private DevExpress.XtraReports.UI.PageFooterBand PageFooter;
        private XRTable xrTable7;
        private XRTableRow xrTableRow7;
        private XRTableCell xrTableCell26;
        private XRTableCell xrTableCell25;
        private XRTable xrTable10;
        private XRTableRow xrTableRow10;
        private XRTableCell xrTableCell55;
        private XRTable xrTable6;
        private XRTableRow xrTableRow8;
        private XRTableCell xrTableCell3;
        private XRTableCell xrTableCell5;
        private XRTableCell xrTableCell13;
        private XRTableCell xrTableCell19;
        private XRTableCell xrTableCell21;
        private XRTableCell xrTableCell9;
        private XRTableCell xrTableCell10;
        private XRTableCell xrTableCell11;
        private XRTableCell xrTableCell17;
        private XRTableCell xrTableCell20;
        private ReportFooterBand ReportFooter;
        private XRTable xrTable5;
        private XRTableRow xrTableRow5;
        private XRTableCell xrTableCell41;
        private XRTableCell xrTableCell43;
        private XRTableCell xrTableCell45;
        private XRTableCell xrTableCell49;
        private XRTableCell xrTableCell51;
        private XRTableCell xrTableCell52;
        private XRTableCell xrTableCell4;
        private XRTableCell xrTableCell6;
        private XRTableCell xrTableCell12;
        private XRTableCell xrTableCell38;
        private XRTableCell xrTableCell53;
        private XRTableCell xrTableCell56;
        private XRTableCell xrTableCell1;
        private XRTableCell xrTableCell2;
        private XRTableCell xrTableCell7;
        private XRTable xrTable3;
        private XRTableRow xrTableRow4;
        private XRTableCell xrTableCell8;
        private GroupHeaderBand GroupHeader1;
        private PageHeaderBand PageHeader;
        private XRTable xrTable1;
        private XRTableRow xrTableRow1;
        private XRTableCell xrTableCell14;
        private GroupFooterBand GroupFooter1;
        private XRTable xrTable2;
        private XRTableRow xrTableRow2;
        private XRTableCell xrTableCell15;
        private XRTableCell xrTableCell16;
        private XRTableCell xrTableCell18;
        private XRTableCell xrTableCell22;
        private XRTableCell xrTableCell23;
        private XRTableCell xrTableCell24;
        private XRTableCell xrTableCell27;
        private XRTableCell xrTableCell28;
        private XRTableCell xrTableCell29;
        private DevExpress.XtraReports.Parameters.Parameter ParameterTotalValorMercado;
        private DevExpress.XtraReports.Parameters.Parameter ParameterValorPL;
        private CalculatedField calculatedFieldCotasPorcentagem;
        private CalculatedField calculatedFieldValorPLPorcentagem;
        private XRTableCell xrTableCell30;
        private XRTableCell xrTableCell31;
        private XRTableCell xrTableCell32;
        private XRTableCell xrTableCell33;
        private TopMarginBand topMarginBand1;
        private BottomMarginBand bottomMarginBand1;
        private XRTableCell xrTableCell34;
        private XRTableCell xrTableCell35;
        private XRTableCell xrTableCell36;
        private XRTableCell xrTableCell37;

        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        #region Chamada como SubReport
        public SubReportCotaInvestimentoMasa() {
            this.InitializeComponent();
        }

        public void PersonalInitialize(int idCarteira, DateTime dataReferencia, int idMoeda) {
            this.idCarteira = idCarteira;

            // Carrega a Carteira
            this.carteira = new Carteira();
            List<esQueryItem> campos = new List<esQueryItem>();
            campos.Add(this.carteira.Query.TipoCota);
            this.carteira.LoadByPrimaryKey(campos, this.idCarteira);
            //
            this.dataReferencia = dataReferencia;
            //
            //Carrega a Moeda do Cliente Associado                        
            this.idMoeda = idMoeda;

            // Armazena tipoPessoa
            Pessoa p = new Pessoa();
            Cliente c = new Cliente();
            c.LoadByPrimaryKey(this.idCarteira);

            p.LoadByPrimaryKey(c.IdPessoa.Value);
            this.tipoPessoa = p.Tipo.Value == (int)TipoPessoa.Fisica ? TipoPessoa.Fisica : TipoPessoa.Juridica;

            // Consulta do SubRelatorio
            DataTable dt = this.FillDados();
            this.DataSource = dt;
            this.numeroLinhasDataTable = dt.Rows.Count;

            #region Pega Campos do resource
            //this.xrTableCell55.Text = Resources.SubReportCotaInvestimentoMasa._TituloRelatorio;
            this.xrTableCell9.Text = Resources.SubReportCotaInvestimento._Descricao;
            this.xrTableCell25.Text = Resources.SubReportCotaInvestimento._Instituicao;
            this.xrTableCell26.Text = Resources.SubReportCotaInvestimento._QtdCotas;
            this.xrTableCell5.Text = Resources.SubReportCotaInvestimento._ValorCota;
            this.xrTableCell3.Text = Resources.SubReportCotaInvestimento._ValorMercado;
            this.xrTableCell4.Text = Resources.SubReportCotaInvestimento.__Cotas;
            this.xrTableCell6.Text = Resources.SubReportCotaInvestimento.__PL;
            this.xrTableCell11.Text = Resources.SubReportCotaInvestimento._Tributos;
            this.xrTableCell30.Text = Resources.SubReportCotaInvestimento._ValorLiquido;
            this.xrTableCell34.Text = Resources.SubReportCotaInvestimento._AplicacaoDescontada;

            #endregion

            //
            ReportBase relatorioBase = new ReportBase(this);
            //
            this.SetRelatorioSemDados();
        }

        #endregion
       
        /// <summary>
        /// Se relatorio não tem dados após o select mostra o SubReport Sem Dados
        /// </summary>
        private void SetRelatorioSemDados() {
            if (this.numeroLinhasDataTable == 0) { 
                // Desaparece com as todas as bandas menos o subreport
                this.xrTable10.Visible = false;
                this.xrTable7.Visible = false;
                this.xrTable5.Visible = false;
                this.xrTable3.Visible = false;
            }
        }
       
        private DataTable FillDados() 
        {
            esUtility u = new esUtility();
            //
            esParameters esParams = new esParameters();
            esParams.Add("Data", this.dataReferencia);  

            // idCarteira Obrigatorio
            // Define se faz Consulta PosicaoFundo ou PosicaoFundoHistorico
            Cliente cliente = new Cliente();
            this.tipoPesquisa = cliente.IsClienteNaData(this.IdCarteira, this.dataReferencia)
                                ? TipoPesquisa.PosicaoFundo
                                : TipoPesquisa.PosicaoFundoHistorico;

            #region SQL
            StringBuilder sqlText = new StringBuilder();
            sqlText.Append("SELECT A.IdAgente, A.Nome as Corretora, \n");
            sqlText.Append("       F.Nome, \n");
            sqlText.Append("       CF.Descricao, \n");
            sqlText.Append("       P.CotaDia, \n");
            sqlText.Append("       SUM((P.CotaDia - P.CotaAplicacao) * P.Quantidade) AS Rendimento, \n");
            sqlText.Append("       SUM(P.ValorBruto) AS ValorBruto, \n");
            sqlText.Append("       SUM(P.Quantidade) AS Quantidade, \n");            
            sqlText.Append("       F.IdCarteira, \n");
            sqlText.Append("       SUM(P.ValorLiquido) AS ValorLiquido, \n");
            sqlText.Append("       SUM(P.ValorIR + P.ValorIOF) AS Tributos, \n");
            sqlText.Append("       SUM(P.Quantidade * P.CotaAplicacao) AS ValorAplicacaoDescontada \n");
            
            if (this.tipoPesquisa == TipoPesquisa.PosicaoFundo) {
                sqlText.Append(" FROM  [PosicaoFundo] P, \n");
            }
            else if (this.tipoPesquisa == TipoPesquisa.PosicaoFundoHistorico) {
                sqlText.AppendLine(" From PosicaoFundoHistorico P, ");
            }

            sqlText.Append("       [AgenteMercado] A, \n");
            sqlText.Append("       [CategoriaFundo] CF, \n");
            sqlText.Append("       [Carteira] F \n");
            sqlText.Append(" WHERE P.IdCarteira = F.IdCarteira \n");
            sqlText.Append("       AND F.IdAgenteGestor = A.IdAgente \n");
            sqlText.Append("       AND F.IdCategoria = CF.IdCategoria \n");
            sqlText.Append("       AND P.Quantidade > 0 \n");
            sqlText.Append("       AND P.IdCliente = " + this.IdCarteira + "\n");

            if (this.tipoPesquisa == TipoPesquisa.PosicaoFundoHistorico) {
                sqlText.AppendLine("    AND P.DataHistorico = @Data ");
            }

            sqlText.Append("       GROUP BY F.IdCarteira, F.Nome, A.IdAgente, A.Nome, P.CotaDia, CF.Descricao \n");

            #endregion

            DataTable dt = u.FillDataTable(esQueryType.Text, sqlText.ToString(), esParams);

            #region Trata multi-moeda
            //Pega a ptax para agilizar, se o cliente estiver em dólar
            decimal ptax = 0;
            if (this.idMoeda == (int)ListaMoedaFixo.Dolar)
            {
                CotacaoIndice cotacaoIndice = new CotacaoIndice();
                ptax = cotacaoIndice.BuscaCotacaoIndice((int)ListaIndiceFixo.PTAX_800VENDA, this.dataReferencia);
            }

            foreach (DataRow dr in dt.Rows)
            {
                decimal cotaDia = Convert.ToDecimal(dr[4]);
                decimal rendimento = Convert.ToDecimal(dr[5]);
                decimal valorBruto = Convert.ToDecimal(dr[6]);
                int idFundoAplicado = Convert.ToInt32(dr[8]);

                Cliente clienteFundo = new Cliente();
                List<esQueryItem> camposFundo = new List<esQueryItem>();
                camposFundo.Add(clienteFundo.Query.IdMoeda);
                clienteFundo.LoadByPrimaryKey(camposFundo, idFundoAplicado);
                int idMoedaFundo = clienteFundo.IdMoeda.Value;

                if (this.idMoeda != idMoedaFundo)
                {
                    decimal fatorConversao = 0;
                    if (this.idMoeda == (int)ListaMoedaFixo.Dolar && idMoedaFundo == (int)ListaMoedaFixo.Real)
                    {
                        fatorConversao = 1 / ptax;
                    }
                    else
                    {
                        ConversaoMoeda conversaoMoeda = new ConversaoMoeda();
                        conversaoMoeda.Query.Where(conversaoMoeda.Query.IdMoedaDe.Equal(this.idMoeda),
                                                   conversaoMoeda.Query.IdMoedaPara.Equal(idMoedaFundo));
                        if (conversaoMoeda.Query.Load())
                        {
                            int idIndiceConversao = conversaoMoeda.IdIndice.Value;
                            CotacaoIndice cotacaoIndice = new CotacaoIndice();
                            if (conversaoMoeda.Tipo.Value == (byte)TipoConversaoMoeda.Divide)
                            {
                                fatorConversao = 1 / cotacaoIndice.BuscaCotacaoIndice(idIndiceConversao, this.dataReferencia);
                            }
                            else
                            {
                                fatorConversao = cotacaoIndice.BuscaCotacaoIndice(idIndiceConversao, this.dataReferencia);
                            }
                        }
                    }

                    decimal cotaDiaConvertido = cotaDia * fatorConversao;
                    decimal rendimentoConvertido = rendimento * fatorConversao;
                    decimal valorBrutoConvertido = valorBruto * fatorConversao;

                    dr[4] = cotaDiaConvertido;
                    dr[5] = rendimentoConvertido;
                    dr[6] = valorBrutoConvertido;
                }
            }            
            #endregion

            //string serverPath = HttpContext.Current.Server.MapPath("/Financial.Web/");
            //dt.WriteXmlSchema(serverPath + "App_Code/Relatorios/Fundo/Xml/SubReportCotaInvestimentoMasa.xml");

            return dt;
        }

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        /* Necessário Mudar: string resourceFileName = "Relatorios/Fundo/ComposicaoCarteira/SubReportCotaInvestimentoMasa.resx";  */
        private void InitializeComponent() {
            string resourceFileName = "SubReportCotaInvestimentoMasa.resx";
            DevExpress.XtraReports.UI.XRSummary xrSummary1 = new DevExpress.XtraReports.UI.XRSummary();
            DevExpress.XtraReports.UI.XRSummary xrSummary2 = new DevExpress.XtraReports.UI.XRSummary();
            DevExpress.XtraReports.UI.XRSummary xrSummary3 = new DevExpress.XtraReports.UI.XRSummary();
            DevExpress.XtraReports.UI.XRSummary xrSummary4 = new DevExpress.XtraReports.UI.XRSummary();
            DevExpress.XtraReports.UI.XRSummary xrSummary5 = new DevExpress.XtraReports.UI.XRSummary();
            DevExpress.XtraReports.UI.XRSummary xrSummary6 = new DevExpress.XtraReports.UI.XRSummary();
            DevExpress.XtraReports.UI.XRSummary xrSummary7 = new DevExpress.XtraReports.UI.XRSummary();
            DevExpress.XtraReports.UI.XRSummary xrSummary8 = new DevExpress.XtraReports.UI.XRSummary();
            DevExpress.XtraReports.UI.XRSummary xrSummary9 = new DevExpress.XtraReports.UI.XRSummary();
            DevExpress.XtraReports.UI.XRSummary xrSummary10 = new DevExpress.XtraReports.UI.XRSummary();
            DevExpress.XtraReports.UI.XRSummary xrSummary11 = new DevExpress.XtraReports.UI.XRSummary();
            DevExpress.XtraReports.UI.XRSummary xrSummary12 = new DevExpress.XtraReports.UI.XRSummary();
            this.Detail = new DevExpress.XtraReports.UI.DetailBand();
            this.xrTable6 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow8 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell10 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell21 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell19 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell20 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell35 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell13 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell17 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell31 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell12 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell38 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable7 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow7 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell9 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell25 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell26 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell5 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell34 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell11 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell30 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell6 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable10 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow10 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell55 = new DevExpress.XtraReports.UI.XRTableCell();
            this.PageFooter = new DevExpress.XtraReports.UI.PageFooterBand();
            this.xrTable3 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow4 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell8 = new DevExpress.XtraReports.UI.XRTableCell();
            this.ReportFooter = new DevExpress.XtraReports.UI.ReportFooterBand();
            this.xrTable5 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow5 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell41 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell7 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell43 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell45 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell56 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell37 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell53 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell49 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell33 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell51 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell52 = new DevExpress.XtraReports.UI.XRTableCell();
            this.GroupHeader1 = new DevExpress.XtraReports.UI.GroupHeaderBand();
            this.xrTable1 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow1 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell14 = new DevExpress.XtraReports.UI.XRTableCell();
            this.PageHeader = new DevExpress.XtraReports.UI.PageHeaderBand();
            this.GroupFooter1 = new DevExpress.XtraReports.UI.GroupFooterBand();
            this.xrTable2 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow2 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell15 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell16 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell18 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell22 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell23 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell36 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell24 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell27 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell32 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell28 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell29 = new DevExpress.XtraReports.UI.XRTableCell();
            this.ParameterTotalValorMercado = new DevExpress.XtraReports.Parameters.Parameter();
            this.ParameterValorPL = new DevExpress.XtraReports.Parameters.Parameter();
            this.calculatedFieldCotasPorcentagem = new DevExpress.XtraReports.UI.CalculatedField();
            this.calculatedFieldValorPLPorcentagem = new DevExpress.XtraReports.UI.CalculatedField();
            this.topMarginBand1 = new DevExpress.XtraReports.UI.TopMarginBand();
            this.bottomMarginBand1 = new DevExpress.XtraReports.UI.BottomMarginBand();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable6});
            this.Detail.Dpi = 254F;
            this.Detail.HeightF = 50F;
            this.Detail.KeepTogether = true;
            this.Detail.Name = "Detail";
            this.Detail.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.Detail.SortFields.AddRange(new DevExpress.XtraReports.UI.GroupField[] {
            new DevExpress.XtraReports.UI.GroupField("Nome", DevExpress.XtraReports.UI.XRColumnSortOrder.Ascending)});
            this.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // xrTable6
            // 
            this.xrTable6.Dpi = 254F;
            this.xrTable6.LocationFloat = new DevExpress.Utils.PointFloat(25.00009F, 0F);
            this.xrTable6.Name = "xrTable6";
            this.xrTable6.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTable6.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow8});
            this.xrTable6.SizeF = new System.Drawing.SizeF(2744F, 46.00002F);
            this.xrTable6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTable6.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.TableDetailBeforePrint);
            // 
            // xrTableRow8
            // 
            this.xrTableRow8.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell10,
            this.xrTableCell2,
            this.xrTableCell21,
            this.xrTableCell19,
            this.xrTableCell20,
            this.xrTableCell35,
            this.xrTableCell13,
            this.xrTableCell17,
            this.xrTableCell31,
            this.xrTableCell12,
            this.xrTableCell38});
            this.xrTableRow8.Dpi = 254F;
            this.xrTableRow8.Name = "xrTableRow8";
            this.xrTableRow8.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableRow8.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow8.Weight = 1;
            // 
            // xrTableCell10
            // 
            this.xrTableCell10.CanGrow = false;
            this.xrTableCell10.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "esUtility.Nome")});
            this.xrTableCell10.Dpi = 254F;
            this.xrTableCell10.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell10.Name = "xrTableCell10";
            this.xrTableCell10.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell10.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell10.Weight = 0.20106948885545;
            // 
            // xrTableCell2
            // 
            this.xrTableCell2.Dpi = 254F;
            this.xrTableCell2.Name = "xrTableCell2";
            this.xrTableCell2.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCell2.Weight = 0.007748973110618367;
            // 
            // xrTableCell21
            // 
            this.xrTableCell21.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "esUtility.Corretora")});
            this.xrTableCell21.Dpi = 254F;
            this.xrTableCell21.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell21.Name = "xrTableCell21";
            this.xrTableCell21.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell21.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell21.Weight = 0.11200905804081154;
            // 
            // xrTableCell19
            // 
            this.xrTableCell19.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "esUtility.Quantidade", "{0:n8}")});
            this.xrTableCell19.Dpi = 254F;
            this.xrTableCell19.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell19.Name = "xrTableCell19";
            this.xrTableCell19.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell19.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell19.Weight = 0.10508440703470326;
            // 
            // xrTableCell20
            // 
            this.xrTableCell20.Dpi = 254F;
            this.xrTableCell20.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell20.Name = "xrTableCell20";
            this.xrTableCell20.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell20.Text = "ValorCota";
            this.xrTableCell20.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell20.Weight = 0.0940039367876687;
            this.xrTableCell20.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.ValorCotaBeforePrint);
            // 
            // xrTableCell35
            // 
            this.xrTableCell35.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "esUtility.ValorAplicacaoDescontada")});
            this.xrTableCell35.Dpi = 254F;
            this.xrTableCell35.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell35.Name = "xrTableCell35";
            this.xrTableCell35.StylePriority.UseFont = false;
            this.xrTableCell35.StylePriority.UseTextAlignment = false;
            this.xrTableCell35.Text = "xrTableCell35";
            this.xrTableCell35.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell35.Weight = 0.1056851292580302;
            this.xrTableCell35.PrintOnPage += new DevExpress.XtraReports.UI.PrintOnPageEventHandler(this.CustomFormat);
            // 
            // xrTableCell13
            // 
            this.xrTableCell13.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "esUtility.ValorBruto")});
            this.xrTableCell13.Dpi = 254F;
            this.xrTableCell13.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell13.Name = "xrTableCell13";
            this.xrTableCell13.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell13.Text = "ValorMercado";
            this.xrTableCell13.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell13.Weight = 0.10499107065221437;
            this.xrTableCell13.WordWrap = false;
            this.xrTableCell13.PrintOnPage += new DevExpress.XtraReports.UI.PrintOnPageEventHandler(this.CustomFormat);
            // 
            // xrTableCell17
            // 
            this.xrTableCell17.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "esUtility.Tributos", "{0:n2}")});
            this.xrTableCell17.Dpi = 254F;
            this.xrTableCell17.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell17.Name = "xrTableCell17";
            this.xrTableCell17.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell17.Text = "xrTableCell17";
            this.xrTableCell17.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell17.Weight = 0.074421740650043172;
            this.xrTableCell17.WordWrap = false;
            // 
            // xrTableCell31
            // 
            this.xrTableCell31.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "esUtility.ValorLiquido")});
            this.xrTableCell31.Dpi = 254F;
            this.xrTableCell31.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell31.Name = "xrTableCell31";
            this.xrTableCell31.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96F);
            this.xrTableCell31.StylePriority.UseFont = false;
            this.xrTableCell31.StylePriority.UseTextAlignment = false;
            this.xrTableCell31.Text = "xrTableCell31";
            this.xrTableCell31.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell31.Weight = 0.093776518191105551;
            this.xrTableCell31.PrintOnPage += new DevExpress.XtraReports.UI.PrintOnPageEventHandler(this.CustomFormat);
            // 
            // xrTableCell12
            // 
            this.xrTableCell12.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "esUtility.calculatedFieldCotasPorcentagem")});
            this.xrTableCell12.Dpi = 254F;
            this.xrTableCell12.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell12.Name = "xrTableCell12";
            this.xrTableCell12.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell12.Text = "Cotas";
            this.xrTableCell12.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell12.Weight = 0.051209677419354838;
            this.xrTableCell12.WordWrap = false;
            this.xrTableCell12.PrintOnPage += new DevExpress.XtraReports.UI.PrintOnPageEventHandler(this.CustomFormatPorcentagem);
            // 
            // xrTableCell38
            // 
            this.xrTableCell38.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "esUtility.calculatedFieldValorPLPorcentagem")});
            this.xrTableCell38.Dpi = 254F;
            this.xrTableCell38.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell38.Name = "xrTableCell38";
            this.xrTableCell38.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell38.Text = "PL";
            this.xrTableCell38.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell38.Weight = 0.05;
            this.xrTableCell38.WordWrap = false;
            this.xrTableCell38.PrintOnPage += new DevExpress.XtraReports.UI.PrintOnPageEventHandler(this.CustomFormatPorcentagem);
            // 
            // xrTable7
            // 
            this.xrTable7.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTable7.Dpi = 254F;
            this.xrTable7.LocationFloat = new DevExpress.Utils.PointFloat(25.00009F, 55.00003F);
            this.xrTable7.Name = "xrTable7";
            this.xrTable7.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTable7.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow7});
            this.xrTable7.SizeF = new System.Drawing.SizeF(2744F, 48.00002F);
            this.xrTable7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTable7.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.TableGroupHeaderBeforePrint);
            // 
            // xrTableRow7
            // 
            this.xrTableRow7.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell9,
            this.xrTableCell1,
            this.xrTableCell25,
            this.xrTableCell26,
            this.xrTableCell5,
            this.xrTableCell34,
            this.xrTableCell3,
            this.xrTableCell11,
            this.xrTableCell30,
            this.xrTableCell4,
            this.xrTableCell6});
            this.xrTableRow7.Dpi = 254F;
            this.xrTableRow7.Name = "xrTableRow7";
            this.xrTableRow7.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableRow7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow7.Weight = 1;
            // 
            // xrTableCell9
            // 
            this.xrTableCell9.Dpi = 254F;
            this.xrTableCell9.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell9.Name = "xrTableCell9";
            this.xrTableCell9.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell9.Text = "#Descricao";
            this.xrTableCell9.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
            this.xrTableCell9.Weight = 0.20106948885545;
            // 
            // xrTableCell1
            // 
            this.xrTableCell1.Dpi = 254F;
            this.xrTableCell1.Name = "xrTableCell1";
            this.xrTableCell1.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCell1.Weight = 0.007748973110618367;
            // 
            // xrTableCell25
            // 
            this.xrTableCell25.Dpi = 254F;
            this.xrTableCell25.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell25.Name = "xrTableCell25";
            this.xrTableCell25.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell25.Text = "#Instituicao";
            this.xrTableCell25.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
            this.xrTableCell25.Weight = 0.11200905804081154;
            // 
            // xrTableCell26
            // 
            this.xrTableCell26.Dpi = 254F;
            this.xrTableCell26.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell26.Name = "xrTableCell26";
            this.xrTableCell26.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell26.Text = "#QtdCotas";
            this.xrTableCell26.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.xrTableCell26.Weight = 0.10508441815626962;
            // 
            // xrTableCell5
            // 
            this.xrTableCell5.Dpi = 254F;
            this.xrTableCell5.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell5.Name = "xrTableCell5";
            this.xrTableCell5.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell5.StylePriority.UsePadding = false;
            this.xrTableCell5.Text = "#ValorCota";
            this.xrTableCell5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.xrTableCell5.Weight = 0.094003903422969617;
            // 
            // xrTableCell34
            // 
            this.xrTableCell34.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell34.Dpi = 254F;
            this.xrTableCell34.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell34.Name = "xrTableCell34";
            this.xrTableCell34.StylePriority.UseBorders = false;
            this.xrTableCell34.StylePriority.UseFont = false;
            this.xrTableCell34.StylePriority.UseTextAlignment = false;
            this.xrTableCell34.Text = "#AplicDescontada";
            this.xrTableCell34.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.xrTableCell34.Weight = 0.10568512925803018;
            // 
            // xrTableCell3
            // 
            this.xrTableCell3.Dpi = 254F;
            this.xrTableCell3.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell3.Name = "xrTableCell3";
            this.xrTableCell3.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell3.StylePriority.UsePadding = false;
            this.xrTableCell3.Text = "#ValorMercado";
            this.xrTableCell3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.xrTableCell3.Weight = 0.10499109289534712;
            // 
            // xrTableCell11
            // 
            this.xrTableCell11.Dpi = 254F;
            this.xrTableCell11.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell11.Name = "xrTableCell11";
            this.xrTableCell11.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell11.Text = "#Tributos";
            this.xrTableCell11.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.xrTableCell11.Weight = 0.074421740650043172;
            // 
            // xrTableCell30
            // 
            this.xrTableCell30.Dpi = 254F;
            this.xrTableCell30.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell30.Name = "xrTableCell30";
            this.xrTableCell30.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96F);
            this.xrTableCell30.StylePriority.UseFont = false;
            this.xrTableCell30.StylePriority.UseTextAlignment = false;
            this.xrTableCell30.Text = "#ValorLiquido";
            this.xrTableCell30.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.xrTableCell30.Weight = 0.093776518191105551;
            // 
            // xrTableCell4
            // 
            this.xrTableCell4.Dpi = 254F;
            this.xrTableCell4.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell4.Name = "xrTableCell4";
            this.xrTableCell4.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell4.Text = "#%Cotas";
            this.xrTableCell4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.xrTableCell4.Weight = 0.051209677419354838;
            // 
            // xrTableCell6
            // 
            this.xrTableCell6.Dpi = 254F;
            this.xrTableCell6.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell6.Name = "xrTableCell6";
            this.xrTableCell6.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell6.Text = "#%PL";
            this.xrTableCell6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.xrTableCell6.Weight = 0.05;
            // 
            // xrTable10
            // 
            this.xrTable10.BackColor = System.Drawing.Color.Gainsboro;
            this.xrTable10.Dpi = 254F;
            this.xrTable10.Font = new System.Drawing.Font("Times New Roman", 10F);
            this.xrTable10.LocationFloat = new DevExpress.Utils.PointFloat(25.00009F, 14.99997F);
            this.xrTable10.Name = "xrTable10";
            this.xrTable10.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTable10.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow10});
            this.xrTable10.SizeF = new System.Drawing.SizeF(2744F, 48F);
            this.xrTable10.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrTableRow10
            // 
            this.xrTableRow10.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell55});
            this.xrTableRow10.Dpi = 254F;
            this.xrTableRow10.Font = new System.Drawing.Font("Times New Roman", 10F);
            this.xrTableRow10.Name = "xrTableRow10";
            this.xrTableRow10.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableRow10.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow10.Weight = 1;
            // 
            // xrTableCell55
            // 
            this.xrTableCell55.Dpi = 254F;
            this.xrTableCell55.Font = new System.Drawing.Font("Times New Roman", 10F, System.Drawing.FontStyle.Bold);
            this.xrTableCell55.Name = "xrTableCell55";
            this.xrTableCell55.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell55.Text = "#TituloRelatorio";
            this.xrTableCell55.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell55.Weight = 1;
            this.xrTableCell55.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.TituloBeforePrint);
            // 
            // PageFooter
            // 
            this.PageFooter.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable3});
            this.PageFooter.Dpi = 254F;
            this.PageFooter.HeightF = 24F;
            this.PageFooter.Name = "PageFooter";
            this.PageFooter.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.PageFooter.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrTable3
            // 
            this.xrTable3.Dpi = 254F;
            this.xrTable3.LocationFloat = new DevExpress.Utils.PointFloat(25.00009F, 0F);
            this.xrTable3.Name = "xrTable3";
            this.xrTable3.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTable3.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow4});
            this.xrTable3.SizeF = new System.Drawing.SizeF(764.0001F, 19.99999F);
            this.xrTable3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrTableRow4
            // 
            this.xrTableRow4.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell8});
            this.xrTableRow4.Dpi = 254F;
            this.xrTableRow4.Name = "xrTableRow4";
            this.xrTableRow4.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableRow4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow4.Weight = 1;
            // 
            // xrTableCell8
            // 
            this.xrTableCell8.Dpi = 254F;
            this.xrTableCell8.Name = "xrTableCell8";
            this.xrTableCell8.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell8.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCell8.Weight = 1;
            // 
            // ReportFooter
            // 
            this.ReportFooter.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable5});
            this.ReportFooter.Dpi = 254F;
            this.ReportFooter.HeightF = 53F;
            this.ReportFooter.KeepTogether = true;
            this.ReportFooter.Name = "ReportFooter";
            this.ReportFooter.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.ReportFooter.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrTable5
            // 
            this.xrTable5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(238)))), ((int)(((byte)(238)))), ((int)(((byte)(238)))));
            this.xrTable5.Borders = DevExpress.XtraPrinting.BorderSide.Top;
            this.xrTable5.Dpi = 254F;
            this.xrTable5.LocationFloat = new DevExpress.Utils.PointFloat(25.00009F, 0F);
            this.xrTable5.Name = "xrTable5";
            this.xrTable5.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTable5.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow5});
            this.xrTable5.SizeF = new System.Drawing.SizeF(2744F, 46.00002F);
            this.xrTable5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTable5.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.TableTotalBeforePrint);
            // 
            // xrTableRow5
            // 
            this.xrTableRow5.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell41,
            this.xrTableCell7,
            this.xrTableCell43,
            this.xrTableCell45,
            this.xrTableCell56,
            this.xrTableCell37,
            this.xrTableCell53,
            this.xrTableCell49,
            this.xrTableCell33,
            this.xrTableCell51,
            this.xrTableCell52});
            this.xrTableRow5.Dpi = 254F;
            this.xrTableRow5.Name = "xrTableRow5";
            this.xrTableRow5.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableRow5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow5.Weight = 1;
            // 
            // xrTableCell41
            // 
            this.xrTableCell41.Dpi = 254F;
            this.xrTableCell41.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Bold);
            this.xrTableCell41.Name = "xrTableCell41";
            this.xrTableCell41.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell41.Text = "#Total";
            this.xrTableCell41.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell41.Weight = 0.20106948885545;
            // 
            // xrTableCell7
            // 
            this.xrTableCell7.Dpi = 254F;
            this.xrTableCell7.Name = "xrTableCell7";
            this.xrTableCell7.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCell7.Weight = 0.007748973110618367;
            // 
            // xrTableCell43
            // 
            this.xrTableCell43.Dpi = 254F;
            this.xrTableCell43.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell43.Name = "xrTableCell43";
            this.xrTableCell43.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell43.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell43.Weight = 0.11200905804081154;
            // 
            // xrTableCell45
            // 
            this.xrTableCell45.Dpi = 254F;
            this.xrTableCell45.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Bold);
            this.xrTableCell45.Name = "xrTableCell45";
            this.xrTableCell45.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell45.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell45.Weight = 0.10508441815626962;
            // 
            // xrTableCell56
            // 
            this.xrTableCell56.Dpi = 254F;
            this.xrTableCell56.Name = "xrTableCell56";
            this.xrTableCell56.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell56.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell56.Weight = 0.094003903422969617;
            // 
            // xrTableCell37
            // 
            this.xrTableCell37.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "esUtility.ValorAplicacaoDescontada")});
            this.xrTableCell37.Dpi = 254F;
            this.xrTableCell37.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Bold);
            this.xrTableCell37.Name = "xrTableCell37";
            this.xrTableCell37.StylePriority.UseFont = false;
            this.xrTableCell37.StylePriority.UseTextAlignment = false;
            xrSummary1.IgnoreNullValues = true;
            xrSummary1.Running = DevExpress.XtraReports.UI.SummaryRunning.Report;
            this.xrTableCell37.Summary = xrSummary1;
            this.xrTableCell37.Text = "xrTableCell37";
            this.xrTableCell37.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell37.Weight = 0.10568512925803018;
            this.xrTableCell37.PrintOnPage += new DevExpress.XtraReports.UI.PrintOnPageEventHandler(this.CustomFormat);
            // 
            // xrTableCell53
            // 
            this.xrTableCell53.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "esUtility.ValorBruto")});
            this.xrTableCell53.Dpi = 254F;
            this.xrTableCell53.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Bold);
            this.xrTableCell53.Name = "xrTableCell53";
            this.xrTableCell53.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            xrSummary2.IgnoreNullValues = true;
            xrSummary2.Running = DevExpress.XtraReports.UI.SummaryRunning.Report;
            this.xrTableCell53.Summary = xrSummary2;
            this.xrTableCell53.Text = "ValorMercadoTotal";
            this.xrTableCell53.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell53.Weight = 0.10499109289534712;
            this.xrTableCell53.PrintOnPage += new DevExpress.XtraReports.UI.PrintOnPageEventHandler(this.CustomFormat);
            // 
            // xrTableCell49
            // 
            this.xrTableCell49.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "esUtility.Tributos")});
            this.xrTableCell49.Dpi = 254F;
            this.xrTableCell49.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Bold);
            this.xrTableCell49.Name = "xrTableCell49";
            this.xrTableCell49.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            xrSummary3.FormatString = "{0:n2}";
            xrSummary3.IgnoreNullValues = true;
            xrSummary3.Running = DevExpress.XtraReports.UI.SummaryRunning.Report;
            this.xrTableCell49.Summary = xrSummary3;
            this.xrTableCell49.Text = "xrTableCell49";
            this.xrTableCell49.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell49.Weight = 0.074421740650043172;
            // 
            // xrTableCell33
            // 
            this.xrTableCell33.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "esUtility.ValorLiquido", "{0:n2}")});
            this.xrTableCell33.Dpi = 254F;
            this.xrTableCell33.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Bold);
            this.xrTableCell33.Name = "xrTableCell33";
            this.xrTableCell33.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96F);
            this.xrTableCell33.StylePriority.UseFont = false;
            this.xrTableCell33.StylePriority.UseTextAlignment = false;
            xrSummary4.IgnoreNullValues = true;
            xrSummary4.Running = DevExpress.XtraReports.UI.SummaryRunning.Report;
            this.xrTableCell33.Summary = xrSummary4;
            this.xrTableCell33.Text = "xrTableCell33";
            this.xrTableCell33.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell33.Weight = 0.093776518191105551;
            this.xrTableCell33.PrintOnPage += new DevExpress.XtraReports.UI.PrintOnPageEventHandler(this.CustomFormat);
            // 
            // xrTableCell51
            // 
            this.xrTableCell51.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "esUtility.calculatedFieldCotasPorcentagem")});
            this.xrTableCell51.Dpi = 254F;
            this.xrTableCell51.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Bold);
            this.xrTableCell51.Name = "xrTableCell51";
            this.xrTableCell51.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            xrSummary5.IgnoreNullValues = true;
            xrSummary5.Running = DevExpress.XtraReports.UI.SummaryRunning.Report;
            this.xrTableCell51.Summary = xrSummary5;
            this.xrTableCell51.Text = "xrTableCell51";
            this.xrTableCell51.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell51.Weight = 0.051209677419354838;
            this.xrTableCell51.PrintOnPage += new DevExpress.XtraReports.UI.PrintOnPageEventHandler(this.CustomFormatPorcentagem);
            // 
            // xrTableCell52
            // 
            this.xrTableCell52.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "esUtility.calculatedFieldValorPLPorcentagem")});
            this.xrTableCell52.Dpi = 254F;
            this.xrTableCell52.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Bold);
            this.xrTableCell52.Name = "xrTableCell52";
            this.xrTableCell52.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            xrSummary6.IgnoreNullValues = true;
            xrSummary6.Running = DevExpress.XtraReports.UI.SummaryRunning.Report;
            this.xrTableCell52.Summary = xrSummary6;
            this.xrTableCell52.Text = "xrTableCell52";
            this.xrTableCell52.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell52.Weight = 0.05;
            this.xrTableCell52.PrintOnPage += new DevExpress.XtraReports.UI.PrintOnPageEventHandler(this.CustomFormatPorcentagem);
            // 
            // GroupHeader1
            // 
            this.GroupHeader1.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable1,
            this.xrTable7});
            this.GroupHeader1.Dpi = 254F;
            this.GroupHeader1.GroupFields.AddRange(new DevExpress.XtraReports.UI.GroupField[] {
            new DevExpress.XtraReports.UI.GroupField("Descricao", DevExpress.XtraReports.UI.XRColumnSortOrder.Ascending)});
            this.GroupHeader1.GroupUnion = DevExpress.XtraReports.UI.GroupUnion.WholePage;
            this.GroupHeader1.HeightF = 103.0001F;
            this.GroupHeader1.KeepTogether = true;
            this.GroupHeader1.Name = "GroupHeader1";
            this.GroupHeader1.RepeatEveryPage = true;
            // 
            // xrTable1
            // 
            this.xrTable1.BackColor = System.Drawing.Color.WhiteSmoke;
            this.xrTable1.Dpi = 254F;
            this.xrTable1.Font = new System.Drawing.Font("Times New Roman", 10F);
            this.xrTable1.LocationFloat = new DevExpress.Utils.PointFloat(25.00009F, 0F);
            this.xrTable1.Name = "xrTable1";
            this.xrTable1.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTable1.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow1});
            this.xrTable1.SizeF = new System.Drawing.SizeF(2744F, 48.00002F);
            this.xrTable1.StylePriority.UseBackColor = false;
            this.xrTable1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrTableRow1
            // 
            this.xrTableRow1.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell14});
            this.xrTableRow1.Dpi = 254F;
            this.xrTableRow1.Font = new System.Drawing.Font("Times New Roman", 10F);
            this.xrTableRow1.Name = "xrTableRow1";
            this.xrTableRow1.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableRow1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow1.Weight = 1;
            // 
            // xrTableCell14
            // 
            this.xrTableCell14.Dpi = 254F;
            this.xrTableCell14.Font = new System.Drawing.Font("Times New Roman", 10F, System.Drawing.FontStyle.Bold);
            this.xrTableCell14.Name = "xrTableCell14";
            this.xrTableCell14.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell14.Text = "Descricao";
            this.xrTableCell14.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell14.Weight = 1;
            this.xrTableCell14.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.DescricaoBeforePrint);
            // 
            // PageHeader
            // 
            this.PageHeader.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable10});
            this.PageHeader.Dpi = 254F;
            this.PageHeader.HeightF = 66F;
            this.PageHeader.Name = "PageHeader";
            // 
            // GroupFooter1
            // 
            this.GroupFooter1.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable2});
            this.GroupFooter1.Dpi = 254F;
            this.GroupFooter1.GroupUnion = DevExpress.XtraReports.UI.GroupFooterUnion.WithLastDetail;
            this.GroupFooter1.HeightF = 56F;
            this.GroupFooter1.KeepTogether = true;
            this.GroupFooter1.Name = "GroupFooter1";
            // 
            // xrTable2
            // 
            this.xrTable2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(238)))), ((int)(((byte)(238)))), ((int)(((byte)(238)))));
            this.xrTable2.Borders = DevExpress.XtraPrinting.BorderSide.Top;
            this.xrTable2.Dpi = 254F;
            this.xrTable2.LocationFloat = new DevExpress.Utils.PointFloat(25.00009F, 0F);
            this.xrTable2.Name = "xrTable2";
            this.xrTable2.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTable2.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow2});
            this.xrTable2.SizeF = new System.Drawing.SizeF(2744F, 46.00002F);
            this.xrTable2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTable2.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.TableTotalPorGrupoBeforePrint);
            // 
            // xrTableRow2
            // 
            this.xrTableRow2.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell15,
            this.xrTableCell16,
            this.xrTableCell18,
            this.xrTableCell22,
            this.xrTableCell23,
            this.xrTableCell36,
            this.xrTableCell24,
            this.xrTableCell27,
            this.xrTableCell32,
            this.xrTableCell28,
            this.xrTableCell29});
            this.xrTableRow2.Dpi = 254F;
            this.xrTableRow2.Name = "xrTableRow2";
            this.xrTableRow2.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableRow2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow2.Weight = 1;
            // 
            // xrTableCell15
            // 
            this.xrTableCell15.Dpi = 254F;
            this.xrTableCell15.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Bold);
            this.xrTableCell15.Name = "xrTableCell15";
            this.xrTableCell15.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell15.Text = "#Total";
            this.xrTableCell15.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell15.Weight = 0.20106948885545;
            // 
            // xrTableCell16
            // 
            this.xrTableCell16.Dpi = 254F;
            this.xrTableCell16.Name = "xrTableCell16";
            this.xrTableCell16.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell16.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCell16.Weight = 0.007748973110618367;
            // 
            // xrTableCell18
            // 
            this.xrTableCell18.Dpi = 254F;
            this.xrTableCell18.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell18.Name = "xrTableCell18";
            this.xrTableCell18.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell18.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell18.Weight = 0.11200905804081154;
            // 
            // xrTableCell22
            // 
            this.xrTableCell22.Dpi = 254F;
            this.xrTableCell22.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Bold);
            this.xrTableCell22.Name = "xrTableCell22";
            this.xrTableCell22.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell22.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell22.Weight = 0.10508441815626962;
            // 
            // xrTableCell23
            // 
            this.xrTableCell23.Dpi = 254F;
            this.xrTableCell23.Name = "xrTableCell23";
            this.xrTableCell23.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell23.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell23.Weight = 0.094003903422969617;
            // 
            // xrTableCell36
            // 
            this.xrTableCell36.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "esUtility.ValorAplicacaoDescontada")});
            this.xrTableCell36.Dpi = 254F;
            this.xrTableCell36.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Bold);
            this.xrTableCell36.Name = "xrTableCell36";
            this.xrTableCell36.StylePriority.UseFont = false;
            this.xrTableCell36.StylePriority.UseTextAlignment = false;
            xrSummary7.IgnoreNullValues = true;
            xrSummary7.Running = DevExpress.XtraReports.UI.SummaryRunning.Group;
            this.xrTableCell36.Summary = xrSummary7;
            this.xrTableCell36.Text = "xrTableCell36";
            this.xrTableCell36.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell36.Weight = 0.10568512925803018;
            this.xrTableCell36.PrintOnPage += new DevExpress.XtraReports.UI.PrintOnPageEventHandler(this.CustomFormat);
            // 
            // xrTableCell24
            // 
            this.xrTableCell24.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "esUtility.ValorBruto")});
            this.xrTableCell24.Dpi = 254F;
            this.xrTableCell24.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Bold);
            this.xrTableCell24.Name = "xrTableCell24";
            this.xrTableCell24.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            xrSummary8.IgnoreNullValues = true;
            xrSummary8.Running = DevExpress.XtraReports.UI.SummaryRunning.Group;
            this.xrTableCell24.Summary = xrSummary8;
            this.xrTableCell24.Text = "ValorMercadoPorTipo";
            this.xrTableCell24.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell24.Weight = 0.10499109289534712;
            this.xrTableCell24.PrintOnPage += new DevExpress.XtraReports.UI.PrintOnPageEventHandler(this.CustomFormat);
            // 
            // xrTableCell27
            // 
            this.xrTableCell27.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "esUtility.Tributos")});
            this.xrTableCell27.Dpi = 254F;
            this.xrTableCell27.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Bold);
            this.xrTableCell27.Name = "xrTableCell27";
            this.xrTableCell27.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            xrSummary9.FormatString = "{0:n2}";
            xrSummary9.IgnoreNullValues = true;
            xrSummary9.Running = DevExpress.XtraReports.UI.SummaryRunning.Group;
            this.xrTableCell27.Summary = xrSummary9;
            this.xrTableCell27.Text = "xrTableCell27";
            this.xrTableCell27.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell27.Weight = 0.074421740650043172;
            // 
            // xrTableCell32
            // 
            this.xrTableCell32.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "esUtility.ValorLiquido")});
            this.xrTableCell32.Dpi = 254F;
            this.xrTableCell32.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Bold);
            this.xrTableCell32.Name = "xrTableCell32";
            this.xrTableCell32.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96F);
            this.xrTableCell32.StylePriority.UseFont = false;
            this.xrTableCell32.StylePriority.UseTextAlignment = false;
            xrSummary10.IgnoreNullValues = true;
            xrSummary10.Running = DevExpress.XtraReports.UI.SummaryRunning.Group;
            this.xrTableCell32.Summary = xrSummary10;
            this.xrTableCell32.Text = "xrTableCell32";
            this.xrTableCell32.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell32.Weight = 0.093776518191105551;
            this.xrTableCell32.PrintOnPage += new DevExpress.XtraReports.UI.PrintOnPageEventHandler(this.CustomFormat);
            // 
            // xrTableCell28
            // 
            this.xrTableCell28.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "esUtility.calculatedFieldCotasPorcentagem")});
            this.xrTableCell28.Dpi = 254F;
            this.xrTableCell28.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Bold);
            this.xrTableCell28.Name = "xrTableCell28";
            this.xrTableCell28.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            xrSummary11.IgnoreNullValues = true;
            xrSummary11.Running = DevExpress.XtraReports.UI.SummaryRunning.Group;
            this.xrTableCell28.Summary = xrSummary11;
            this.xrTableCell28.Text = "xrTableCell28";
            this.xrTableCell28.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell28.Weight = 0.051209677419354838;
            this.xrTableCell28.PrintOnPage += new DevExpress.XtraReports.UI.PrintOnPageEventHandler(this.CustomFormatPorcentagem);
            // 
            // xrTableCell29
            // 
            this.xrTableCell29.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "esUtility.calculatedFieldValorPLPorcentagem")});
            this.xrTableCell29.Dpi = 254F;
            this.xrTableCell29.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Bold);
            this.xrTableCell29.Name = "xrTableCell29";
            this.xrTableCell29.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            xrSummary12.IgnoreNullValues = true;
            xrSummary12.Running = DevExpress.XtraReports.UI.SummaryRunning.Group;
            this.xrTableCell29.Summary = xrSummary12;
            this.xrTableCell29.Text = "xrTableCell29";
            this.xrTableCell29.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell29.Weight = 0.05;
            this.xrTableCell29.PrintOnPage += new DevExpress.XtraReports.UI.PrintOnPageEventHandler(this.CustomFormatPorcentagem);
            // 
            // ParameterTotalValorMercado
            // 
            this.ParameterTotalValorMercado.Name = "ParameterTotalValorMercado";
            this.ParameterTotalValorMercado.Type = typeof(decimal);
            this.ParameterTotalValorMercado.Value = 0;
            // 
            // ParameterValorPL
            // 
            this.ParameterValorPL.Name = "ParameterValorPL";
            this.ParameterValorPL.Type = typeof(decimal);
            this.ParameterValorPL.Value = 0;
            // 
            // calculatedFieldCotasPorcentagem
            // 
            this.calculatedFieldCotasPorcentagem.DataMember = "esUtility";
            this.calculatedFieldCotasPorcentagem.Expression = "[ValorBruto] / [Parameters.ParameterTotalValorMercado]";
            this.calculatedFieldCotasPorcentagem.Name = "calculatedFieldCotasPorcentagem";
            // 
            // calculatedFieldValorPLPorcentagem
            // 
            this.calculatedFieldValorPLPorcentagem.DataMember = "esUtility";
            this.calculatedFieldValorPLPorcentagem.Expression = "[ValorBruto] / [Parameters.ParameterValorPL]";
            this.calculatedFieldValorPLPorcentagem.Name = "calculatedFieldValorPLPorcentagem";
            // 
            // topMarginBand1
            // 
            this.topMarginBand1.Dpi = 254F;
            this.topMarginBand1.HeightF = 150F;
            this.topMarginBand1.Name = "topMarginBand1";
            // 
            // bottomMarginBand1
            // 
            this.bottomMarginBand1.Dpi = 254F;
            this.bottomMarginBand1.HeightF = 150F;
            this.bottomMarginBand1.Name = "bottomMarginBand1";
            // 
            // SubReportCotaInvestimentoMasa
            // 
            this.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail,
            this.PageFooter,
            this.ReportFooter,
            this.GroupHeader1,
            this.PageHeader,
            this.GroupFooter1,
            this.topMarginBand1,
            this.bottomMarginBand1});
            this.CalculatedFields.AddRange(new DevExpress.XtraReports.UI.CalculatedField[] {
            this.calculatedFieldCotasPorcentagem,
            this.calculatedFieldValorPLPorcentagem});
            this.Dpi = 254F;
            this.ExportOptions.Html.RemoveSecondarySymbols = true;
            this.ExportOptions.Mht.RemoveSecondarySymbols = true;
            this.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.Landscape = true;
            this.Margins = new System.Drawing.Printing.Margins(0, 0, 150, 150);
            this.PageHeight = 2159;
            this.PageWidth = 2794;
            this.Parameters.AddRange(new DevExpress.XtraReports.Parameters.Parameter[] {
            this.ParameterTotalValorMercado,
            this.ParameterValorPL});
            this.PrintOnEmptyDataSource = false;
            this.ReportUnit = DevExpress.XtraReports.UI.ReportUnit.TenthsOfAMillimeter;
            this.Version = "11.1";
            this.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.ReportBeforePrint);
            ((System.ComponentModel.ISupportInitialize)(this.xrTable6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }

        #endregion

        private System.Resources.ResourceManager GetResourceManager() {
            return Resources.SubReportCotaInvestimento.ResourceManager;
        }

        // Armazena o Total Mercado para ser usado no calculo do campo Cotas
        private decimal totalValorMercado = 0;

        #region Funções Internas do Relatorio
        //
        private void ValorCotaBeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e) {
            XRTableCell valorCotaXRTableCell = sender as XRTableCell;
            valorCotaXRTableCell.Text = "";

            if (this.numeroLinhasDataTable != 0) {
                decimal valorCota = (decimal)this.GetCurrentColumnValue(PosicaoFundoMetadata.ColumnNames.CotaDia);
                //
                ReportBase.ConfiguraSinalNegativo(valorCotaXRTableCell, valorCota, ReportBase.NumeroCasasDecimais.OitoCasasDecimais);
            }
        }

        private void DescricaoBeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e) {
            XRTableCell descricaoXRTableCell = sender as XRTableCell;
            descricaoXRTableCell.Text = "";

            if (this.numeroLinhasDataTable != 0) {
                string descricao = Convert.ToString(this.GetCurrentColumnValue(CategoriaFundoMetadata.ColumnNames.Descricao));
                descricaoXRTableCell.Text = descricao.Trim();
            }
        }

        /*
         *  Executado no start do relatorio
         */
        private void ReportBeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e) {
            // Calcula o totalMercado

            decimal totalValor = 0;
            if (this.tipoPesquisa == TipoPesquisa.PosicaoFundo) 
            {
                PosicaoFundoCollection posicaoFundoCollection = new PosicaoFundoCollection();
                posicaoFundoCollection.QueryReset();
                posicaoFundoCollection.Query
                     .Select(posicaoFundoCollection.Query.IdCarteira, posicaoFundoCollection.Query.ValorBruto.Sum())
                     .Where(posicaoFundoCollection.Query.IdCliente == this.idCarteira)
                     .GroupBy(posicaoFundoCollection.Query.IdCarteira);

                posicaoFundoCollection.Query.Load();
                
                #region Converte multi-moeda
                //Pega a ptax para agilizar, se o cliente estiver em dólar
                decimal ptax = 0;
                if (this.idMoeda == (int)ListaMoedaFixo.Dolar)
                {
                    CotacaoIndice cotacaoIndice = new CotacaoIndice();
                    ptax = cotacaoIndice.BuscaCotacaoIndice((int)ListaIndiceFixo.PTAX_800VENDA, this.dataReferencia);
                }

                foreach (PosicaoFundo posicaoFundo in posicaoFundoCollection)
                {
                    int idCarteira = posicaoFundo.IdCarteira.Value;
                    decimal valor = posicaoFundo.ValorBruto.Value;                    

                    #region Trata multi-moeda
                    decimal fatorConversao = 0;

                    Cliente cliente = new Cliente();
                    List<esQueryItem> campos = new List<esQueryItem>();
                    campos.Add(cliente.Query.IdMoeda);
                    cliente.LoadByPrimaryKey(campos, idCarteira);
                    int idMoedaCarteira = cliente.IdMoeda.Value;

                    if (this.idMoeda != idMoedaCarteira)
                    {
                        if (idMoeda == (int)ListaMoedaFixo.Dolar && idMoedaCarteira == (int)ListaMoedaFixo.Real)
                        {
                            fatorConversao = 1 / ptax;
                        }
                        else
                        {
                            ConversaoMoeda conversaoMoeda = new ConversaoMoeda();
                            conversaoMoeda.Query.Where(conversaoMoeda.Query.IdMoedaDe.Equal(this.idMoeda),
                                                       conversaoMoeda.Query.IdMoedaPara.Equal(idMoedaCarteira));
                            if (conversaoMoeda.Query.Load())
                            {
                                int idIndiceConversao = conversaoMoeda.IdIndice.Value;
                                CotacaoIndice cotacaoIndice = new CotacaoIndice();
                                if (conversaoMoeda.Tipo.Value == (byte)TipoConversaoMoeda.Divide)
                                {
                                    fatorConversao = 1 / cotacaoIndice.BuscaCotacaoIndice(idIndiceConversao, this.dataReferencia);
                                }
                                else
                                {
                                    fatorConversao = cotacaoIndice.BuscaCotacaoIndice(idIndiceConversao, this.dataReferencia);
                                }
                            }
                        }

                        valor = Math.Round(valor * fatorConversao, 2);
                    }
                    #endregion

                    totalValor += valor;
                }
                #endregion                
            }
            else if (this.tipoPesquisa == TipoPesquisa.PosicaoFundoHistorico) 
            {
                PosicaoFundoHistoricoCollection posicaoFundoCollection = new PosicaoFundoHistoricoCollection();
                posicaoFundoCollection.QueryReset();
                posicaoFundoCollection.Query
                     .Select(posicaoFundoCollection.Query.IdCarteira, posicaoFundoCollection.Query.ValorBruto.Sum())
                     .Where(posicaoFundoCollection.Query.IdCliente == this.idCarteira,
                            posicaoFundoCollection.Query.DataHistorico.Equal(this.dataReferencia))
                     .GroupBy(posicaoFundoCollection.Query.IdCarteira);

                posicaoFundoCollection.Query.Load();

                #region Converte multi-moeda
                //Pega a ptax para agilizar, se o cliente estiver em dólar
                decimal ptax = 0;
                if (this.idMoeda == (int)ListaMoedaFixo.Dolar)
                {
                    CotacaoIndice cotacaoIndice = new CotacaoIndice();
                    ptax = cotacaoIndice.BuscaCotacaoIndice((int)ListaIndiceFixo.PTAX_800VENDA, this.dataReferencia);
                }

                foreach (PosicaoFundoHistorico posicaoFundo in posicaoFundoCollection)
                {
                    int idCarteira = posicaoFundo.IdCarteira.Value;
                    decimal valor = posicaoFundo.ValorBruto.Value;

                    #region Trata multi-moeda
                    decimal fatorConversao = 1;

                    Cliente cliente = new Cliente();
                    List<esQueryItem> campos = new List<esQueryItem>();
                    campos.Add(cliente.Query.IdMoeda);
                    cliente.LoadByPrimaryKey(campos, idCarteira);
                    int idMoedaCarteira = cliente.IdMoeda.Value;

                    if (this.idMoeda != idMoedaCarteira)
                    {
                        if (idMoeda == (int)ListaMoedaFixo.Dolar && idMoedaCarteira == (int)ListaMoedaFixo.Real)
                        {
                            fatorConversao = 1 / ptax;
                        }
                        else
                        {
                            ConversaoMoeda conversaoMoeda = new ConversaoMoeda();
                            conversaoMoeda.Query.Where(conversaoMoeda.Query.IdMoedaDe.Equal(this.idMoeda),
                                                       conversaoMoeda.Query.IdMoedaPara.Equal(idMoedaCarteira));
                            if (conversaoMoeda.Query.Load())
                            {
                                int idIndiceConversao = conversaoMoeda.IdIndice.Value;
                                CotacaoIndice cotacaoIndice = new CotacaoIndice();
                                if (conversaoMoeda.Tipo.Value == (byte)TipoConversaoMoeda.Divide)
                                {
                                    fatorConversao = 1 / cotacaoIndice.BuscaCotacaoIndice(idIndiceConversao, this.dataReferencia);
                                }
                                else
                                {
                                    fatorConversao = cotacaoIndice.BuscaCotacaoIndice(idIndiceConversao, this.dataReferencia);
                                }
                            }
                        }

                        valor = Math.Round(valor * fatorConversao, 2);
                    }
                    #endregion

                    totalValor += valor;
                }
                #endregion                
            }

            this.totalValorMercado = totalValor;

            #region Preenche Parametros para Poder Fazer SUM de %Cotas e %PL

            // Preenche o TotalValorMercado para ser Usado no Calculo da Porcentagem de Cotas
            this.Parameters["ParameterTotalValorMercado"].Value = this.totalValorMercado;
            //
            HistoricoCota historicoCota = new HistoricoCota();

            bool excecaoCota = false;
            try {
                historicoCota.BuscaValorPatrimonioDia(this.idCarteira, this.dataReferencia);
            }
            catch (HistoricoCotaNaoCadastradoException e1) {
                excecaoCota = true;
            }

            decimal valorPL = 0;

            if (!excecaoCota) {
                valorPL = this.carteira.IsTipoCotaAbertura()
                        ? historicoCota.PLAbertura.Value
                        : historicoCota.PLFechamento.Value;
            }

            this.Parameters["ParameterValorPL"].Value = valorPL;
            #endregion
        }

        #region Exibe Totais
        private void TableTotalPorGrupoBeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e) {
            XRTable summaryFinal = sender as XRTable;

            XRTableRow summaryFinalRow0 = summaryFinal.Rows[0];
            ((XRTableCell)summaryFinalRow0.Cells[0]).Text = "";

            if (this.numeroLinhasDataTable != 0) {
                string total = Resources.SubReportCotaInvestimento._TotalPorGrupo;
                ((XRTableCell)summaryFinalRow0.Cells[0]).Text = total;

                if (this.tipoPessoa == TipoPessoa.Juridica) {
                    ((XRTableCell)summaryFinalRow0.Cells[5]).Visible = false;

                    ((XRTableCell)summaryFinalRow0.Cells[5]).WidthF /= 2f; // divide largura da coluna aplicação por 2

                    ((XRTableCell)summaryFinalRow0.Cells[4]).WidthF += 170f; // aumenta coluna valor em 170


// Removendo coluna - causa erro de alinhamento
                    //XRTableCell r = (XRTableCell)summaryFinalRow0.Cells[5];
                    //summaryFinalRow0.Cells.Remove(r);

                }
            }
        }

        private void TableTotalBeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e) {
            XRTable summaryFinal = sender as XRTable;

            XRTableRow summaryFinalRow0 = summaryFinal.Rows[0];
            ((XRTableCell)summaryFinalRow0.Cells[0]).Text = "";
            
            if (this.numeroLinhasDataTable != 0) {
                string total = Resources.SubReportCotaInvestimento._Total;
                ((XRTableCell)summaryFinalRow0.Cells[0]).Text = total;

                if (this.tipoPessoa == TipoPessoa.Juridica) {
                    ((XRTableCell)summaryFinalRow0.Cells[5]).Visible = false;

                    ((XRTableCell)summaryFinalRow0.Cells[5]).WidthF /= 2f; // divide largura da coluna aplicação por 2

                    ((XRTableCell)summaryFinalRow0.Cells[4]).WidthF += 170f; // aumenta coluna valor em 170


                    // Removendo coluna - causa erro de alinhamento
                    //XRTableCell r = (XRTableCell)summaryFinalRow0.Cells[5];
                    //summaryFinalRow0.Cells.Remove(r);
                }
            }
        }
       
        #endregion

        #region Formatos
        /// <summary>
        /// Aplica o formato na Célula com 2 duas Casas Decimais
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CustomFormat(object sender, PrintOnPageEventArgs e) {
            XRTableCell valorXRTableCell = sender as XRTableCell;
            decimal valor = 0.00M;
            try {
                valor = Convert.ToDecimal(valorXRTableCell.Text);
            }
            catch (Exception e1) {
                // Não faz nada
            }

            ReportBase.ConfiguraSinalNegativo(valorXRTableCell, valor);
        }

        /// <summary>
        /// Aplica o formato na Célula com 2 duas Casas Decimais e Porcentagem
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CustomFormatPorcentagem(object sender, PrintOnPageEventArgs e) {
            XRTableCell valorXRTableCell = sender as XRTableCell;
            decimal valor = 0.00M;
            try {
                valor = Convert.ToDecimal(valorXRTableCell.Text);
            }
            catch (Exception e1) {
                // Não faz nada
            }
            ReportBase.ConfiguraSinalNegativo(valorXRTableCell, valor, ReportBase.NumeroCasasDecimais.DuasCasasDecimaisPorcentagem);

        }
        #endregion

        private void TituloBeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e) {
            XRTableCell tituloXRTableCell = sender as XRTableCell;
            tituloXRTableCell.Text = "";

            tituloXRTableCell.Text = Resources.SubReportCotaInvestimento._TituloRelatorio;
        }

        #endregion     


        private void TableGroupHeaderBeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e) {
            XRTable t = sender as XRTable;

            XRTableRow t0 = t.Rows[0];
            
            if (this.numeroLinhasDataTable != 0) {
                // Tipo Pessoa = Juridica oculta a coluna Apic. Descontada
                if (this.tipoPessoa == TipoPessoa.Juridica) {
                    ((XRTableCell)t0.Cells[5]).Visible = false;

                    //((XRTableCell)t0.Cells[5]).Borders = DevExpress.XtraPrinting.BorderSide.Bottom;

                    //((XRTableCell)t0.Cells[5]).WidthF = 0f;                    
                    //((XRTableCell)t0.Cells[4]).Padding.Right = 0;

                    ((XRTableCell)t0.Cells[5]).WidthF /= 2f; // divide largura da coluna aplicação por 2
                    ((XRTableCell)t0.Cells[4]).WidthF += 170f;

                    //((XRTableCell)t0.Cells[2]).WidthF += 60f;
                    //((XRTableCell)t0.Cells[3]).WidthF += 60f;

                    //((XRTableCell)t0.Cells[4]).WidthF += 10;

                    // Removendo coluna - causa erro de alinhamento
                    //XRTableCell r =(XRTableCell)t0.Cells[5];
                    //t0.Cells.Remove(r);


                }
            }
        }

        private void TableDetailBeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e) {
            XRTable t = sender as XRTable;

            XRTableRow t0 = t.Rows[0];

            if (this.numeroLinhasDataTable != 0) {

                // Tipo Pessoa = Juridica oculta a coluna Apic. Descontada
                if (this.tipoPessoa == TipoPessoa.Juridica) {
                    ((XRTableCell)t0.Cells[5]).Visible = false;

                    //((XRTableCell)t0.Cells[5]).WidthF -= 170f; // divide largura da coluna aplicação por 2
                     
                    //((XRTableCell)t0.Cells[4]).WidthF += 172f; // aumenta coluna valor em 148
                    //((XRTableCell)t0.Cells[2]).WidthF += 60f;
                    //((XRTableCell)t0.Cells[3]).WidthF += 60f;

                    ((XRTableCell)t0.Cells[5]).WidthF /= 2f; // divide largura da coluna aplicação por 2
                    ((XRTableCell)t0.Cells[4]).WidthF += 170f;


                    // Removendo coluna - causa erro de alinhamento
                   // XRTableCell r = (XRTableCell)t0.Cells[5];
                   // t0.Cells.Remove(r);

                }
            }
        }

        
    }
}
