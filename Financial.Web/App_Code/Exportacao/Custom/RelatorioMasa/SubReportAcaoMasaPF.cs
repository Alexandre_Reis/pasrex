﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using System.Data;
using System.Configuration;
using System.Web.Configuration;
using System.Web;
using System.Text;
using EntitySpaces.Core;
using EntitySpaces.Interfaces;
using System.IO;
using System.Collections.Generic;
using Financial.Bolsa;
using Financial.Bolsa.Enums;
using Financial.Fundo;
using Financial.Investidor;
using Financial.Investidor.Enums;
using Financial.Fundo.Exceptions;
using Financial.Common.Enums;
using Financial.Common;
using Financial.WebConfigConfiguration;
using Financial.CRM.Enums;
using Financial.CRM;

namespace Financial.Relatorio {

    /// <summary>
    /// Summary description for SubReportAcaoMasaPF
    /// </summary>
    public class SubReportAcaoMasaPF : XtraReport {

        private DateTime dataReferencia;

        public DateTime DataReferencia {
            get { return dataReferencia; }
            set { dataReferencia = value; }
        }

        private int idCarteira;

        public int IdCarteira {
            get { return idCarteira; }
            set { idCarteira = value; }
        }
        //
        private Carteira carteira;

        private int idMoeda;

        public int IdMoeda
        {
            get { return idMoeda; }
            set { idMoeda = value; }
        }

        private int numeroLinhasDataTable;

        private enum TipoPesquisa {
            PosicaoBolsa = 0,
            PosicaoBolsaAbertura = 1,
            PosicaoBolsaHistorico = 2            
        }
        TipoPesquisa tipoPesquisa = TipoPesquisa.PosicaoBolsa;
        //

        TipoPessoa tipoPessoa; 
        
        /* Indica se existe Posição Emprestimo Para Impressão */
        private bool existePosicaoEmprestimo = false;

        /// <summary>
        /// Armazena o Tipo do Relatorio
        /// </summary>
        private ReportComposicaoCarteira.TipoRelatorio tipoRelatorio {        
            get {
                // Se não tem relatorio Pai então lança Exceção
                if (this.MasterReport == null) { 
                    throw new Exception("Relatorio não tem Pai");
                }
                else {

                    if (this.MasterReport is ReportComposicaoCarteiraMasa) {
                        return ReportComposicaoCarteira.TipoRelatorio.Fechamento;
                    }

                    //
                    Cliente cliente = new Cliente();            
                    List<esQueryItem> campos = new List<esQueryItem>();
                    campos.Add(cliente.Query.TipoControle);
                    cliente.LoadByPrimaryKey(campos, this.idCarteira);

                    if (cliente.TipoControle == (byte)TipoControleCliente.IRRendaVariavel) {
                        // Sem Resumo
                        if (((ReportComposicaoCarteiraSemResumo)this.MasterReport).TipoRelatorioComposicaoCarteiraSemResumo == ReportComposicaoCarteiraSemResumo.TipoRelatorio.Abertura) {
                            return ReportComposicaoCarteira.TipoRelatorio.Abertura;
                        }
                        else {
                            return ReportComposicaoCarteira.TipoRelatorio.Fechamento;
                        }                                                                
                    }
                    else {
                        return ((ReportComposicaoCarteira)this.MasterReport).TipoRelatorioComposicaoCarteira;                    
                    }
                }            
            }
        }
                    
        /// <summary>
        /// Retorna true se relatorio tem dados
        /// </summary>
        public bool HasData {
            get { return this.numeroLinhasDataTable != 0; }
        }

        //
        private DevExpress.XtraReports.UI.DetailBand Detail;
        private DevExpress.XtraReports.UI.PageFooterBand PageFooter;
        private XRTable xrTable7;
        private XRTableRow xrTableRow7;
        private XRTableCell xrTableCell28;
        private XRTableCell xrTableCell26;
        private XRTable xrTable10;
        private XRTableRow xrTableRow10;
        private XRTableCell xrTableCell55;
        private XRTable xrTable6;
        private XRTableRow xrTableRow8;
        private XRTableCell xrTableCell15;
        private PageHeaderBand PageHeader;
        private XRTableCell xrTableCell3;
        private XRTableCell xrTableCell5;
        private XRTableCell xrTableCell13;
        private XRTableCell xrTableCell14;
        private XRTableCell xrTableCell19;
        private GroupHeaderBand GroupHeader1;
        private XRTable xrTable4;
        private XRTableRow xrTableRow4;
        private XRTableCell xrTableCell2;
        private XRTableCell xrTableCell9;
        private XRTableCell xrTableCell10;
        private XRTableCell xrTableCell7;
        private XRTableCell xrTableCell11;
        private XRTableCell xrTableCell17;
        private XRTableCell xrTableCell18;
        private GroupFooterBand GroupFooter1;
        private XRTable xrTable3;
        private XRTableRow xrTableRow3;
        private XRTableCell xrTableCell24;
        private XRTableCell xrTableCell27;
        private XRTableCell xrTableCell29;
        private XRTableCell xrTableCell31;
        private XRTableCell xrTableCell33;
        private XRTableCell xrTableCell20;
        private XRTableCell xrTableCell34;
        private XRTableCell xrTableCell36;
        private XRTableCell xrTableCell35;
        private XRTableCell xrTableCell37;
        private XRTableCell xrTableCell32;
        private XRTableCell xrTableCell40;
        private ReportFooterBand ReportFooter;
        private XRTable xrTable5;
        private XRTableRow xrTableRow5;
        private XRTableCell xrTableCell41;
        private XRTableCell xrTableCell44;
        private XRTableCell xrTableCell45;
        private XRTableCell xrTableCell46;
        private XRTableCell xrTableCell47;
        private XRTableCell xrTableCell49;
        private XRTableCell xrTableCell50;
        private XRTableCell xrTableCell51;
        private XRTableCell xrTableCell52;
        private XRTableCell xrTableCell1;
        private XRTableCell xrTableCell4;
        private XRTableCell xrTableCell6;
        private XRTableCell xrTableCell12;
        private XRTableCell xrTableCell38;
        private XRTableCell xrTableCell39;
        private XRTableCell xrTableCell53;
        private XRTableCell xrTableCell54;
        private XRTableCell xrTableCell56;
        private XRLabel xrMensagem;
        private DevExpress.XtraReports.Parameters.Parameter ParameterTotalValorMercado;
        private DevExpress.XtraReports.Parameters.Parameter ParameterValorPL;
        private CalculatedField calculatedFieldAcoes;
        private CalculatedField calculatedPLPorcentagem;
        private XRTable xrTableFooter;
        private XRTableRow xrTableRow1;
        private XRTableCell xrTableCell21;
        private XRTable xrTable1;
        private XRTableRow xrTableRow2;
        private XRTableCell xrTableCell22;
        private TopMarginBand topMarginBand1;
        private BottomMarginBand bottomMarginBand1;
        private XRTableCell xrTableCell23;
        private XRTableCell xrTableCell25;
        private XRTableCell xrTableCell42;
        private XRTableCell xrTableCell43;
        private XRTableCell xrTableCell57;
        private XRTableCell xrTableCell58;
        private XRTableCell xrTableCell59;
        private XRTableCell xrTableCell60;
        private GroupHeaderBand GroupHeader2;
        private GroupFooterBand GroupFooter2;
        private XRTable xrTable2;
        private XRTableRow xrTableRow6;
        private XRTableCell xrTableCell8;
        private XRTableCell xrTableCell16;
        private XRTableCell xrTableCell30;
        private XRTableCell xrTableCell48;
        private XRTableCell xrTableCell61;
        private XRTableCell xrTableCell62;
        private XRTableCell xrTableCell63;
        private XRTableCell xrTableCell64;
        private XRTableCell xrTableCell65;
        private XRTableCell xrTableCell66;
        private XRTableCell xrTableCell67;
        private XRTableCell xrTableCell68;
        private XRTableCell xrTableCell69;

        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;
        
        #region Chamada como SubReport
        public SubReportAcaoMasaPF() {
            this.InitializeComponent();
        }

        public void PersonalInitialize(int idCarteira, DateTime dataReferencia, int idMoeda) {                       
            this.idCarteira = idCarteira;

            // Carrega a Carteira
            this.carteira = new Carteira();
            List<esQueryItem> campos = new List<esQueryItem>();
            campos.Add(this.carteira.Query.TipoCota);
            this.carteira.LoadByPrimaryKey(campos, this.idCarteira);
            //
            this.dataReferencia = dataReferencia;
            //
            this.idMoeda = idMoeda;
            //

            // Armazena tipoPessoa
            Pessoa p = new Pessoa();
            Cliente c = new Cliente();
            c.LoadByPrimaryKey(this.idCarteira);
            p.LoadByPrimaryKey(c.IdPessoa.Value);
            this.tipoPessoa = p.Tipo.Value == (int)TipoPessoa.Fisica ? TipoPessoa.Fisica : TipoPessoa.Juridica;
                       
            // Consulta do SubRelatorio
            DataTable dt = this.FillDados();
            this.DataSource = dt;
            this.numeroLinhasDataTable = dt.Rows.Count;

            #region Pega Campos do resource
            this.xrTableCell55.Text = Resources.SubReportAcao._TituloRelatorio;

            //this.xrTableCell55.Text = "Pessoa Física";

            this.xrTableCell9.Text = Resources.SubReportAcao._Codigo;
            this.xrTableCell28.Text = Resources.SubReportAcao._Descricao;
            this.xrTableCell26.Text = Resources.SubReportAcao._QuantidadeDisponivel;
            this.xrTableCell5.Text = Resources.SubReportAcao._QuantidadeBloqueada;
            this.xrTableCell3.Text = Resources.SubReportAcao._Quantidade;
            this.xrTableCell35.Text = Resources.SubReportAcao._PuCustoLiquido;
            this.xrTableCell7.Text = Resources.SubReportAcao._ValorCustoLiquido;
            //this.xrTableCell8.Text = Resources.SubReportAcao._PuMercado;
            this.xrTableCell23.Text = Resources.SubReportAcao.__CapitalVotante;
            this.xrTableCell25.Text = Resources.SubReportAcao.__CapitalSocial;
            this.xrTableCell11.Text = Resources.SubReportAcao._ValorMercado;
            this.xrTableCell1.Text = Resources.SubReportAcao._Resultado;
            this.xrTableCell4.Text = Resources.SubReportAcao.__Acoes;
            this.xrTableCell6.Text = this.MasterReport is ReportComposicaoCarteira || this.MasterReport is ReportComposicaoCarteiraMasa
                                     ? Resources.SubReportAcao.__PL : "";
            #endregion

            //
            ReportBase relatorioBase = new ReportBase(this);
            //
            this.SetRelatorioSemDados();
        }
        #endregion
       
        /// <summary>
        /// Se relatorio não tem dados deixa invisible 
        /// </summary>
        private void SetRelatorioSemDados() {
            if (this.numeroLinhasDataTable == 0) {
                this.xrTable10.Visible = false;
                this.xrTable7.Visible = false;
                this.xrTable4.Visible = false;
                this.xrTable3.Visible = false;
                this.xrTable5.Visible = false;
            }
        }

        private DataTable FillDados() {
            // idCarteira Obrigatorio
            // Define se faz Consulta PosicaoBolsa ou PosicaoBolsaHistorico ou PosicaoBolsaAbertura
            Cliente cliente = new Cliente();
            this.tipoPesquisa = this.tipoRelatorio == ReportComposicaoCarteira.TipoRelatorio.Abertura
                                        ? TipoPesquisa.PosicaoBolsaAbertura
                                        : cliente.IsClienteNaData(this.IdCarteira, this.dataReferencia)
                                                ? TipoPesquisa.PosicaoBolsa
                                                : TipoPesquisa.PosicaoBolsaHistorico;
                                           
            esUtility u = new esUtility();
            string tipoMercado = "'" + TipoMercadoBolsa.MercadoVista + "', '" + TipoMercadoBolsa.Imobiliario + "'";

            esParameters esParams = new esParameters();
            esParams.Add("Data", this.dataReferencia);  

            #region SQL
            StringBuilder sqlText = new StringBuilder();
            sqlText.AppendLine(" SELECT G.IdAgente, G.Nome as Corretora, ");
            sqlText.AppendLine("       P.CdAtivoBolsa, ");
            sqlText.AppendLine("       A.Descricao as DescricaoAtivo, ");            
            sqlText.AppendLine("       P.PUMercado, ");
            sqlText.AppendLine("       P.PUCustoLiquido, ");
            sqlText.AppendLine("       P.ValorMercado, ");
            sqlText.AppendLine("       P.ValorCustoLiquido, ");
            sqlText.AppendLine("       P.Quantidade, ");
            sqlText.AppendLine("       P.QuantidadeBloqueada, ");
            sqlText.AppendLine("       P.ResultadoRealizar, ");
            sqlText.AppendLine("       (P.Quantidade - P.QuantidadeBloqueada) as QuantidadeDisponivel ");

            if (this.tipoPesquisa == TipoPesquisa.PosicaoBolsa) {
                sqlText.AppendLine(" From PosicaoBolsa P, ");
            }
            else if (this.tipoPesquisa == TipoPesquisa.PosicaoBolsaHistorico) {
                sqlText.AppendLine(" From PosicaoBolsaHistorico P, ");
            }
            else if (this.tipoPesquisa == TipoPesquisa.PosicaoBolsaAbertura) {
                sqlText.AppendLine(" From PosicaoBolsaAbertura P, ");
            }

            sqlText.AppendLine("       [AgenteMercado] G, ");
            sqlText.AppendLine("       [AtivoBolsa] A ");
            sqlText.AppendLine(" WHERE P.IdAgente = G.IdAgente ");
            sqlText.AppendLine("       AND P.CdAtivoBolsa = A.CdAtivoBolsa ");
            sqlText.AppendLine("       AND P.Quantidade <> 0 ");
            sqlText.AppendLine("       AND A.IdMoeda = 1 "); //Somente ativos em real!
            sqlText.AppendLine("       AND P.TipoMercado In (" + tipoMercado + ") ");
            sqlText.AppendLine("       AND P.IdCliente = " + this.IdCarteira + "");

            if (this.tipoPesquisa == TipoPesquisa.PosicaoBolsaHistorico) {
                sqlText.AppendLine("    AND P.DataHistorico = @Data ");
            }

            if (this.tipoPesquisa == TipoPesquisa.PosicaoBolsaAbertura) {
                sqlText.AppendLine("    AND P.DataHistorico = @Data ");
            }

            #endregion

            DataTable dt = u.FillDataTable(esQueryType.Text, sqlText.ToString(), esParams);
            //
            dt.Columns.Add("CapitalSocial", typeof(Decimal)); // coluna 12
            dt.Columns.Add("CapitalVotante", typeof(Decimal)); // coluna 13
            dt.Columns.Add("PuCustoDivisao", typeof(Decimal)); // coluna 14

            foreach (DataRow dr in dt.Rows) 
            {   
                string cdAtivoBolsa = Convert.ToString(dr[2]);
                decimal puMercado = Convert.ToDecimal(dr[4]);
                decimal puCustoLiquido = Convert.ToDecimal(dr[5]);
                decimal valorMercado = Convert.ToDecimal(dr[6]);
                decimal valorCustoLiquido = Convert.ToDecimal(dr[7]);
                decimal quantidade = Convert.ToDecimal(dr[8]);
                decimal resultadoRealizar = Convert.ToDecimal(dr[10]);

                AtivoBolsa ativoBolsa = new AtivoBolsa();
                ativoBolsa.LoadByPrimaryKey(cdAtivoBolsa);
                int idMoedaAtivo = ativoBolsa.IdMoeda.Value;

                ConversaoMoeda conversaoMoeda = new ConversaoMoeda();
                decimal fatorConversao = conversaoMoeda.RetornaFatorConversao(this.idMoeda, idMoedaAtivo, this.dataReferencia);

                decimal puMercadoConvertido = puMercado * fatorConversao;
                decimal puCustoLiquidoConvertido = puCustoLiquido * fatorConversao;
                decimal valorMercadoConvertido = Math.Round(valorMercado * fatorConversao, 2);
                decimal valorCustoLiquidoConvertido = valorCustoLiquido * fatorConversao;
                decimal resultadoRealizarConvertido = resultadoRealizar * fatorConversao;

                dr[4] = puMercadoConvertido;
                dr[5] = puCustoLiquidoConvertido;
                dr[6] = valorMercadoConvertido;
                dr[7] = valorCustoLiquidoConvertido;
                dr[10] = resultadoRealizarConvertido;

                #region Cálculo Capital Social/Capital Votante
                AtivoBolsa a = new AtivoBolsa();

                AtivoBolsaQuery at = new AtivoBolsaQuery("A");
                EspecificacaoBolsaQuery esp = new EspecificacaoBolsaQuery("Esp");
                //
                at.Select(at, esp.Especificacao.As("Especificacao1"));
                at.InnerJoin(esp).On(at.Especificacao == esp.Especificacao);
                at.Where(at.CdAtivoBolsa == cdAtivoBolsa);

                a.Load(at);

                string esp1 = Convert.ToString(a.GetColumn("Especificacao1"));
                EspecificacaoBolsa especificacaoBolsa = new EspecificacaoBolsa();
                especificacaoBolsa.LoadByPrimaryKey(esp1);
                              
                //
                decimal? percentualCapitalSocial = null;
                try {
                    decimal quantidadeCapitalSocial = new EvolucaoCapitalSocial().RetornaQuantidade(a.IdEmissor.Value, this.dataReferencia, null);

                    if (especificacaoBolsa.Tipo.Value == (byte)TipoEspecificacaoBolsa.Unit)
                    {
                        percentualCapitalSocial = (quantidade * 5M) / quantidadeCapitalSocial * 100M;
                    }
                    else
                    {
                        percentualCapitalSocial = quantidade / quantidadeCapitalSocial * 100M;
                    }
                }
                catch (Exception) { }

                decimal? percentualCapitalVotante = null;
                try {
                    if (especificacaoBolsa.Tipo.HasValue && 
                            (especificacaoBolsa.Tipo.Value == (byte)TipoEspecificacaoBolsa.Ordinaria || especificacaoBolsa.Tipo.Value == (byte)TipoEspecificacaoBolsa.Unit))
                    {
                        decimal quantidadeCapitalVotante = new EvolucaoCapitalSocial().RetornaQuantidade(a.IdEmissor.Value, this.dataReferencia, TipoEspecificacaoBolsa.Ordinaria);
                        percentualCapitalVotante = quantidade / quantidadeCapitalVotante * 100M;
                    }
                }
                catch (Exception) { }

                dr[12] = 0.00M;
                if (percentualCapitalSocial.HasValue) {
                    dr[12] = percentualCapitalSocial;
                }

                dr[13] = 0.00M;
                if (percentualCapitalVotante.HasValue) {
                    dr[13] = percentualCapitalVotante;
                }

                dr[14] = 0.00M;
                decimal valorCustoLiquidoFinal = Convert.ToDecimal(dr[7]);
                decimal quantidadeFinal = Convert.ToDecimal(dr[8]);

                if (quantidadeFinal != 0) {
                    dr[14] = valorCustoLiquidoFinal / quantidadeFinal;
                }
                #endregion
            }            

            //string serverPath = HttpContext.Current.Server.MapPath("/Financial.Web/");
            //dt.WriteXmlSchema(serverPath + "App_Code/Relatorios/Fundo/Xml/SubReportAcaoMasa.xml");

            return dt;
        }

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        /* Necessário Mudar: string resourceFileName = "Relatorios/Fundo/ComposicaoCarteira/SubReportAcaoMasa.resx";  */
        private void InitializeComponent() {
            string resourceFileName = "SubReportAcaoMasaPF.resx";
            DevExpress.XtraReports.UI.XRSummary xrSummary1 = new DevExpress.XtraReports.UI.XRSummary();
            DevExpress.XtraReports.UI.XRSummary xrSummary2 = new DevExpress.XtraReports.UI.XRSummary();
            DevExpress.XtraReports.UI.XRSummary xrSummary3 = new DevExpress.XtraReports.UI.XRSummary();
            DevExpress.XtraReports.UI.XRSummary xrSummary4 = new DevExpress.XtraReports.UI.XRSummary();
            DevExpress.XtraReports.UI.XRSummary xrSummary5 = new DevExpress.XtraReports.UI.XRSummary();
            DevExpress.XtraReports.UI.XRSummary xrSummary6 = new DevExpress.XtraReports.UI.XRSummary();
            DevExpress.XtraReports.UI.XRSummary xrSummary7 = new DevExpress.XtraReports.UI.XRSummary();
            DevExpress.XtraReports.UI.XRSummary xrSummary8 = new DevExpress.XtraReports.UI.XRSummary();
            DevExpress.XtraReports.UI.XRSummary xrSummary9 = new DevExpress.XtraReports.UI.XRSummary();
            DevExpress.XtraReports.UI.XRSummary xrSummary10 = new DevExpress.XtraReports.UI.XRSummary();
            DevExpress.XtraReports.UI.XRSummary xrSummary11 = new DevExpress.XtraReports.UI.XRSummary();
            DevExpress.XtraReports.UI.XRSummary xrSummary12 = new DevExpress.XtraReports.UI.XRSummary();
            DevExpress.XtraReports.UI.XRSummary xrSummary13 = new DevExpress.XtraReports.UI.XRSummary();
            DevExpress.XtraReports.UI.XRSummary xrSummary14 = new DevExpress.XtraReports.UI.XRSummary();
            DevExpress.XtraReports.UI.XRSummary xrSummary15 = new DevExpress.XtraReports.UI.XRSummary();
            DevExpress.XtraReports.UI.XRSummary xrSummary16 = new DevExpress.XtraReports.UI.XRSummary();
            DevExpress.XtraReports.UI.XRSummary xrSummary17 = new DevExpress.XtraReports.UI.XRSummary();
            DevExpress.XtraReports.UI.XRSummary xrSummary18 = new DevExpress.XtraReports.UI.XRSummary();
            DevExpress.XtraReports.UI.XRSummary xrSummary19 = new DevExpress.XtraReports.UI.XRSummary();
            DevExpress.XtraReports.UI.XRSummary xrSummary20 = new DevExpress.XtraReports.UI.XRSummary();
            DevExpress.XtraReports.UI.XRSummary xrSummary21 = new DevExpress.XtraReports.UI.XRSummary();
            DevExpress.XtraReports.UI.XRSummary xrSummary22 = new DevExpress.XtraReports.UI.XRSummary();
            DevExpress.XtraReports.UI.XRSummary xrSummary23 = new DevExpress.XtraReports.UI.XRSummary();
            DevExpress.XtraReports.UI.XRSummary xrSummary24 = new DevExpress.XtraReports.UI.XRSummary();
            DevExpress.XtraReports.UI.XRSummary xrSummary25 = new DevExpress.XtraReports.UI.XRSummary();
            DevExpress.XtraReports.UI.XRSummary xrSummary26 = new DevExpress.XtraReports.UI.XRSummary();
            DevExpress.XtraReports.UI.XRSummary xrSummary27 = new DevExpress.XtraReports.UI.XRSummary();
            DevExpress.XtraReports.UI.XRSummary xrSummary28 = new DevExpress.XtraReports.UI.XRSummary();
            DevExpress.XtraReports.UI.XRSummary xrSummary29 = new DevExpress.XtraReports.UI.XRSummary();
            this.Detail = new DevExpress.XtraReports.UI.DetailBand();
            this.xrTable6 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow8 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell10 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell15 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell19 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell20 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell13 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell42 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell43 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell14 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell36 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell17 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell18 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell12 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell38 = new DevExpress.XtraReports.UI.XRTableCell();
            this.PageHeader = new DevExpress.XtraReports.UI.PageHeaderBand();
            this.xrTable7 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow7 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell9 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell28 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell26 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell5 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell23 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell25 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell35 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell7 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell11 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell6 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable10 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow10 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell55 = new DevExpress.XtraReports.UI.XRTableCell();
            this.PageFooter = new DevExpress.XtraReports.UI.PageFooterBand();
            this.xrTable1 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow2 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell22 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableFooter = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow1 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell21 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrMensagem = new DevExpress.XtraReports.UI.XRLabel();
            this.GroupHeader1 = new DevExpress.XtraReports.UI.GroupHeaderBand();
            this.xrTable4 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow4 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.GroupFooter1 = new DevExpress.XtraReports.UI.GroupFooterBand();
            this.xrTable3 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow3 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell40 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell24 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell27 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell54 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell39 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell57 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell58 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell34 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell29 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell37 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell31 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell32 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell33 = new DevExpress.XtraReports.UI.XRTableCell();
            this.ParameterValorPL = new DevExpress.XtraReports.Parameters.Parameter();
            this.ReportFooter = new DevExpress.XtraReports.UI.ReportFooterBand();
            this.xrTable5 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow5 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell41 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell44 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell45 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell56 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell53 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell59 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell60 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell46 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell47 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell49 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell50 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell51 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell52 = new DevExpress.XtraReports.UI.XRTableCell();
            this.ParameterTotalValorMercado = new DevExpress.XtraReports.Parameters.Parameter();
            this.calculatedFieldAcoes = new DevExpress.XtraReports.UI.CalculatedField();
            this.calculatedPLPorcentagem = new DevExpress.XtraReports.UI.CalculatedField();
            this.topMarginBand1 = new DevExpress.XtraReports.UI.TopMarginBand();
            this.bottomMarginBand1 = new DevExpress.XtraReports.UI.BottomMarginBand();
            this.GroupHeader2 = new DevExpress.XtraReports.UI.GroupHeaderBand();
            this.GroupFooter2 = new DevExpress.XtraReports.UI.GroupFooterBand();
            this.xrTable2 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow6 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell8 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell16 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell30 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell48 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell61 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell62 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell63 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell64 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell65 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell66 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell67 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell68 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell69 = new DevExpress.XtraReports.UI.XRTableCell();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTableFooter)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable6});
            this.Detail.Dpi = 254F;
            this.Detail.HeightF = 46.0001F;
            this.Detail.KeepTogether = true;
            this.Detail.Name = "Detail";
            this.Detail.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.Detail.SortFields.AddRange(new DevExpress.XtraReports.UI.GroupField[] {
            new DevExpress.XtraReports.UI.GroupField("CdAtivoBolsa", DevExpress.XtraReports.UI.XRColumnSortOrder.Ascending),
            new DevExpress.XtraReports.UI.GroupField("CdAtivoBolsa", DevExpress.XtraReports.UI.XRColumnSortOrder.Ascending)});
            this.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // xrTable6
            // 
            this.xrTable6.Dpi = 254F;
            this.xrTable6.LocationFloat = new DevExpress.Utils.PointFloat(25F, 0F);
            this.xrTable6.Name = "xrTable6";
            this.xrTable6.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTable6.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow8});
            this.xrTable6.SizeF = new System.Drawing.SizeF(2744F, 46.00002F);
            this.xrTable6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrTableRow8
            // 
            this.xrTableRow8.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell10,
            this.xrTableCell15,
            this.xrTableCell19,
            this.xrTableCell20,
            this.xrTableCell13,
            this.xrTableCell42,
            this.xrTableCell43,
            this.xrTableCell14,
            this.xrTableCell36,
            this.xrTableCell17,
            this.xrTableCell18,
            this.xrTableCell12,
            this.xrTableCell38});
            this.xrTableRow8.Dpi = 254F;
            this.xrTableRow8.Name = "xrTableRow8";
            this.xrTableRow8.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableRow8.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow8.Weight = 1;
            // 
            // xrTableCell10
            // 
            this.xrTableCell10.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "esUtility.CdAtivoBolsa")});
            this.xrTableCell10.Dpi = 254F;
            this.xrTableCell10.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell10.Name = "xrTableCell10";
            this.xrTableCell10.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell10.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell10.Weight = 0.068548387096774188;
            this.xrTableCell10.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.CdAtivoBolsaBeforePrint);
            // 
            // xrTableCell15
            // 
            this.xrTableCell15.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "esUtility.Corretora")});
            this.xrTableCell15.Dpi = 254F;
            this.xrTableCell15.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell15.Name = "xrTableCell15";
            this.xrTableCell15.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell15.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell15.Weight = 0.11290322580645161;
            // 
            // xrTableCell19
            // 
            this.xrTableCell19.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "esUtility.QuantidadeDisponivel")});
            this.xrTableCell19.Dpi = 254F;
            this.xrTableCell19.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell19.Name = "xrTableCell19";
            this.xrTableCell19.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell19.Text = "QuantidadeDisponivel";
            this.xrTableCell19.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell19.Weight = 0.083064516129032262;
            this.xrTableCell19.PrintOnPage += new DevExpress.XtraReports.UI.PrintOnPageEventHandler(this.CustomFormatZeroCasas);
            // 
            // xrTableCell20
            // 
            this.xrTableCell20.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "esUtility.QuantidadeBloqueada")});
            this.xrTableCell20.Dpi = 254F;
            this.xrTableCell20.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell20.Name = "xrTableCell20";
            this.xrTableCell20.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell20.Text = "QuantidadeBloqueada";
            this.xrTableCell20.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell20.Weight = 0.074193835933401522;
            this.xrTableCell20.PrintOnPage += new DevExpress.XtraReports.UI.PrintOnPageEventHandler(this.CustomFormatZeroCasas);
            // 
            // xrTableCell13
            // 
            this.xrTableCell13.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "esUtility.Quantidade")});
            this.xrTableCell13.Dpi = 254F;
            this.xrTableCell13.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell13.Name = "xrTableCell13";
            this.xrTableCell13.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell13.Text = "Quantidade";
            this.xrTableCell13.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell13.Weight = 0.0874293695509204;
            this.xrTableCell13.PrintOnPage += new DevExpress.XtraReports.UI.PrintOnPageEventHandler(this.CustomFormatZeroCasas);
            this.xrTableCell13.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.QuantidadeBeforePrint);
            // 
            // xrTableCell42
            // 
            this.xrTableCell42.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "esUtility.CapitalVotante", "{0:n6}")});
            this.xrTableCell42.Dpi = 254F;
            this.xrTableCell42.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell42.Name = "xrTableCell42";
            this.xrTableCell42.StylePriority.UseFont = false;
            this.xrTableCell42.StylePriority.UseTextAlignment = false;
            this.xrTableCell42.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell42.Weight = 0.070230954092286385;
            // 
            // xrTableCell43
            // 
            this.xrTableCell43.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "esUtility.CapitalSocial", "{0:n6}")});
            this.xrTableCell43.Dpi = 254F;
            this.xrTableCell43.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell43.Name = "xrTableCell43";
            this.xrTableCell43.StylePriority.UseFont = false;
            this.xrTableCell43.StylePriority.UseTextAlignment = false;
            this.xrTableCell43.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell43.Weight = 0.064445470779262534;
            // 
            // xrTableCell14
            // 
            this.xrTableCell14.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "esUtility.PUCustoLiquido", "{0:n2}")});
            this.xrTableCell14.Dpi = 254F;
            this.xrTableCell14.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell14.Name = "xrTableCell14";
            this.xrTableCell14.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell14.Text = "xrTableCell14";
            this.xrTableCell14.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell14.Weight = 0.070424427659224784;
            // 
            // xrTableCell36
            // 
            this.xrTableCell36.CanGrow = false;
            this.xrTableCell36.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "esUtility.ValorCustoLiquido")});
            this.xrTableCell36.Dpi = 254F;
            this.xrTableCell36.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell36.Name = "xrTableCell36";
            this.xrTableCell36.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell36.Text = "ValorCustoLiquido";
            this.xrTableCell36.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell36.Weight = 0.087201878664175822;
            this.xrTableCell36.WordWrap = false;
            this.xrTableCell36.PrintOnPage += new DevExpress.XtraReports.UI.PrintOnPageEventHandler(this.CustomFormat);
            this.xrTableCell36.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.ValorCustoBeforePrint);
            // 
            // xrTableCell17
            // 
            this.xrTableCell17.CanGrow = false;
            this.xrTableCell17.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "esUtility.ValorMercado")});
            this.xrTableCell17.Dpi = 254F;
            this.xrTableCell17.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell17.Name = "xrTableCell17";
            this.xrTableCell17.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell17.Text = "ValorMercado";
            this.xrTableCell17.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell17.Weight = 0.085993418159438229;
            this.xrTableCell17.WordWrap = false;
            this.xrTableCell17.PrintOnPage += new DevExpress.XtraReports.UI.PrintOnPageEventHandler(this.CustomFormat);
            // 
            // xrTableCell18
            // 
            this.xrTableCell18.CanGrow = false;
            this.xrTableCell18.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "esUtility.ResultadoRealizar")});
            this.xrTableCell18.Dpi = 254F;
            this.xrTableCell18.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell18.Name = "xrTableCell18";
            this.xrTableCell18.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell18.Text = "ResultadoRealizar";
            this.xrTableCell18.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell18.Weight = 0.0939516129032258;
            this.xrTableCell18.WordWrap = false;
            this.xrTableCell18.PrintOnPage += new DevExpress.XtraReports.UI.PrintOnPageEventHandler(this.CustomFormat);
            // 
            // xrTableCell12
            // 
            this.xrTableCell12.CanGrow = false;
            this.xrTableCell12.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "esUtility.calculatedFieldAcoes")});
            this.xrTableCell12.Dpi = 254F;
            this.xrTableCell12.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell12.Name = "xrTableCell12";
            this.xrTableCell12.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell12.Text = "Acoes";
            this.xrTableCell12.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell12.Weight = 0.051209677419354838;
            this.xrTableCell12.WordWrap = false;
            this.xrTableCell12.PrintOnPage += new DevExpress.XtraReports.UI.PrintOnPageEventHandler(this.CustomFormatPorcentagem);
            // 
            // xrTableCell38
            // 
            this.xrTableCell38.CanGrow = false;
            this.xrTableCell38.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "esUtility.calculatedPLPorcentagem")});
            this.xrTableCell38.Dpi = 254F;
            this.xrTableCell38.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell38.Name = "xrTableCell38";
            this.xrTableCell38.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell38.Text = "Pl";
            this.xrTableCell38.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell38.Weight = 0.05040322580645161;
            this.xrTableCell38.WordWrap = false;
            this.xrTableCell38.PrintOnPage += new DevExpress.XtraReports.UI.PrintOnPageEventHandler(this.CustomFormatPLPorcentagem);
            // 
            // PageHeader
            // 
            this.PageHeader.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable7,
            this.xrTable10});
            this.PageHeader.Dpi = 254F;
            this.PageHeader.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.PageHeader.HeightF = 113F;
            this.PageHeader.Name = "PageHeader";
            this.PageHeader.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.PageHeader.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
            // 
            // xrTable7
            // 
            this.xrTable7.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTable7.Dpi = 254F;
            this.xrTable7.LocationFloat = new DevExpress.Utils.PointFloat(25F, 64.99999F);
            this.xrTable7.Name = "xrTable7";
            this.xrTable7.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTable7.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow7});
            this.xrTable7.SizeF = new System.Drawing.SizeF(2744F, 47.99998F);
            this.xrTable7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrTableRow7
            // 
            this.xrTableRow7.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell9,
            this.xrTableCell28,
            this.xrTableCell26,
            this.xrTableCell5,
            this.xrTableCell3,
            this.xrTableCell23,
            this.xrTableCell25,
            this.xrTableCell35,
            this.xrTableCell7,
            this.xrTableCell11,
            this.xrTableCell1,
            this.xrTableCell4,
            this.xrTableCell6});
            this.xrTableRow7.Dpi = 254F;
            this.xrTableRow7.Name = "xrTableRow7";
            this.xrTableRow7.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableRow7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow7.Weight = 1;
            // 
            // xrTableCell9
            // 
            this.xrTableCell9.Dpi = 254F;
            this.xrTableCell9.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell9.Name = "xrTableCell9";
            this.xrTableCell9.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell9.Text = "#Codigo";
            this.xrTableCell9.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
            this.xrTableCell9.Weight = 0.068548387096774188;
            // 
            // xrTableCell28
            // 
            this.xrTableCell28.Dpi = 254F;
            this.xrTableCell28.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell28.Name = "xrTableCell28";
            this.xrTableCell28.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell28.Text = "#Descricao";
            this.xrTableCell28.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
            this.xrTableCell28.Weight = 0.11290322580645161;
            // 
            // xrTableCell26
            // 
            this.xrTableCell26.Dpi = 254F;
            this.xrTableCell26.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell26.Name = "xrTableCell26";
            this.xrTableCell26.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell26.Text = "#QuantidadeDisponivel";
            this.xrTableCell26.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.xrTableCell26.Weight = 0.083064516129032262;
            // 
            // xrTableCell5
            // 
            this.xrTableCell5.Dpi = 254F;
            this.xrTableCell5.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell5.Name = "xrTableCell5";
            this.xrTableCell5.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell5.Text = "#QuantidadeBloqueada";
            this.xrTableCell5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.xrTableCell5.Weight = 0.074193835933401522;
            // 
            // xrTableCell3
            // 
            this.xrTableCell3.Dpi = 254F;
            this.xrTableCell3.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell3.Name = "xrTableCell3";
            this.xrTableCell3.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell3.Text = "#Quantidade";
            this.xrTableCell3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.xrTableCell3.Weight = 0.0874293695509204;
            // 
            // xrTableCell23
            // 
            this.xrTableCell23.Dpi = 254F;
            this.xrTableCell23.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell23.Name = "xrTableCell23";
            this.xrTableCell23.StylePriority.UseFont = false;
            this.xrTableCell23.StylePriority.UseTextAlignment = false;
            this.xrTableCell23.Text = "%Cap. Votante";
            this.xrTableCell23.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.xrTableCell23.Weight = 0.070230954092286385;
            // 
            // xrTableCell25
            // 
            this.xrTableCell25.Dpi = 254F;
            this.xrTableCell25.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell25.Name = "xrTableCell25";
            this.xrTableCell25.StylePriority.UseFont = false;
            this.xrTableCell25.StylePriority.UseTextAlignment = false;
            this.xrTableCell25.Text = "%Cap. Social";
            this.xrTableCell25.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.xrTableCell25.Weight = 0.064445470779262534;
            // 
            // xrTableCell35
            // 
            this.xrTableCell35.Dpi = 254F;
            this.xrTableCell35.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell35.Name = "xrTableCell35";
            this.xrTableCell35.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell35.Text = "#PuCustoLiquido";
            this.xrTableCell35.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.xrTableCell35.Weight = 0.070424427659224784;
            // 
            // xrTableCell7
            // 
            this.xrTableCell7.Dpi = 254F;
            this.xrTableCell7.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell7.Name = "xrTableCell7";
            this.xrTableCell7.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell7.Text = "#ValorCustoLiquido";
            this.xrTableCell7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.xrTableCell7.Weight = 0.087201878664175822;
            // 
            // xrTableCell11
            // 
            this.xrTableCell11.Dpi = 254F;
            this.xrTableCell11.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell11.Name = "xrTableCell11";
            this.xrTableCell11.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell11.Text = "#ValorMercado";
            this.xrTableCell11.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.xrTableCell11.Weight = 0.085993418159438229;
            // 
            // xrTableCell1
            // 
            this.xrTableCell1.Dpi = 254F;
            this.xrTableCell1.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell1.Name = "xrTableCell1";
            this.xrTableCell1.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell1.Text = "#Resultado";
            this.xrTableCell1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.xrTableCell1.Weight = 0.0939516129032258;
            // 
            // xrTableCell4
            // 
            this.xrTableCell4.Dpi = 254F;
            this.xrTableCell4.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell4.Name = "xrTableCell4";
            this.xrTableCell4.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell4.Text = "#%Acoes";
            this.xrTableCell4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.xrTableCell4.Weight = 0.051209677419354838;
            // 
            // xrTableCell6
            // 
            this.xrTableCell6.Dpi = 254F;
            this.xrTableCell6.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell6.Name = "xrTableCell6";
            this.xrTableCell6.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell6.Text = "#%PL";
            this.xrTableCell6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.xrTableCell6.Weight = 0.05040322580645161;
            // 
            // xrTable10
            // 
            this.xrTable10.BackColor = System.Drawing.Color.Gainsboro;
            this.xrTable10.Dpi = 254F;
            this.xrTable10.LocationFloat = new DevExpress.Utils.PointFloat(25F, 10.00004F);
            this.xrTable10.Name = "xrTable10";
            this.xrTable10.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTable10.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow10});
            this.xrTable10.SizeF = new System.Drawing.SizeF(2744F, 48F);
            this.xrTable10.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrTableRow10
            // 
            this.xrTableRow10.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell55});
            this.xrTableRow10.Dpi = 254F;
            this.xrTableRow10.Name = "xrTableRow10";
            this.xrTableRow10.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableRow10.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow10.Weight = 1;
            // 
            // xrTableCell55
            // 
            this.xrTableCell55.Dpi = 254F;
            this.xrTableCell55.Font = new System.Drawing.Font("Times New Roman", 10F, System.Drawing.FontStyle.Bold);
            this.xrTableCell55.Name = "xrTableCell55";
            this.xrTableCell55.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell55.Text = "#TituloRelatorio";
            this.xrTableCell55.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell55.Weight = 1;
            // 
            // PageFooter
            // 
            this.PageFooter.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable1,
            this.xrTableFooter,
            this.xrMensagem});
            this.PageFooter.Dpi = 254F;
            this.PageFooter.HeightF = 79F;
            this.PageFooter.Name = "PageFooter";
            this.PageFooter.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.PageFooter.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.PageFooter.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.PageFooterBeforePrint);
            // 
            // xrTable1
            // 
            this.xrTable1.Dpi = 254F;
            this.xrTable1.LocationFloat = new DevExpress.Utils.PointFloat(25F, 0F);
            this.xrTable1.Name = "xrTable1";
            this.xrTable1.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow2});
            this.xrTable1.SizeF = new System.Drawing.SizeF(518F, 14.99997F);
            this.xrTable1.Visible = false;
            // 
            // xrTableRow2
            // 
            this.xrTableRow2.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell22});
            this.xrTableRow2.Dpi = 254F;
            this.xrTableRow2.Name = "xrTableRow2";
            this.xrTableRow2.Weight = 1;
            // 
            // xrTableCell22
            // 
            this.xrTableCell22.CanGrow = false;
            this.xrTableCell22.Dpi = 254F;
            this.xrTableCell22.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell22.Name = "xrTableCell22";
            this.xrTableCell22.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell22.StylePriority.UseFont = false;
            this.xrTableCell22.Weight = 1;
            // 
            // xrTableFooter
            // 
            this.xrTableFooter.Dpi = 254F;
            this.xrTableFooter.LocationFloat = new DevExpress.Utils.PointFloat(25F, 55.00003F);
            this.xrTableFooter.Name = "xrTableFooter";
            this.xrTableFooter.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow1});
            this.xrTableFooter.SizeF = new System.Drawing.SizeF(518F, 14.99997F);
            this.xrTableFooter.Visible = false;
            // 
            // xrTableRow1
            // 
            this.xrTableRow1.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell21});
            this.xrTableRow1.Dpi = 254F;
            this.xrTableRow1.Name = "xrTableRow1";
            this.xrTableRow1.Weight = 1;
            // 
            // xrTableCell21
            // 
            this.xrTableCell21.CanGrow = false;
            this.xrTableCell21.Dpi = 254F;
            this.xrTableCell21.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell21.Name = "xrTableCell21";
            this.xrTableCell21.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell21.StylePriority.UseFont = false;
            this.xrTableCell21.Weight = 1;
            this.xrTableCell21.WordWrap = false;
            // 
            // xrMensagem
            // 
            this.xrMensagem.Dpi = 254F;
            this.xrMensagem.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Bold);
            this.xrMensagem.LocationFloat = new DevExpress.Utils.PointFloat(25F, 14.99997F);
            this.xrMensagem.Name = "xrMensagem";
            this.xrMensagem.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrMensagem.SizeF = new System.Drawing.SizeF(1682F, 34.99996F);
            this.xrMensagem.StylePriority.UseFont = false;
            this.xrMensagem.Text = "#* Saldo do ativo  = Saldo total - posições doadas + posições tomadas de emprésti" +
                "mos em ações";
            this.xrMensagem.Visible = false;
            this.xrMensagem.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.MsgFooterBeforePrint);
            // 
            // GroupHeader1
            // 
            this.GroupHeader1.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable4});
            this.GroupHeader1.Dpi = 254F;
            this.GroupHeader1.GroupFields.AddRange(new DevExpress.XtraReports.UI.GroupField[] {
            new DevExpress.XtraReports.UI.GroupField("DescricaoAtivo", DevExpress.XtraReports.UI.XRColumnSortOrder.Ascending)});
            this.GroupHeader1.GroupUnion = DevExpress.XtraReports.UI.GroupUnion.WithFirstDetail;
            this.GroupHeader1.HeightF = 48.00002F;
            this.GroupHeader1.KeepTogether = true;
            this.GroupHeader1.Level = 1;
            this.GroupHeader1.Name = "GroupHeader1";
            this.GroupHeader1.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.GroupHeader1.RepeatEveryPage = true;
            this.GroupHeader1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrTable4
            // 
            this.xrTable4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(238)))), ((int)(((byte)(238)))), ((int)(((byte)(238)))));
            this.xrTable4.Dpi = 254F;
            this.xrTable4.LocationFloat = new DevExpress.Utils.PointFloat(25F, 0F);
            this.xrTable4.Name = "xrTable4";
            this.xrTable4.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTable4.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow4});
            this.xrTable4.SizeF = new System.Drawing.SizeF(2744F, 48.00002F);
            this.xrTable4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrTableRow4
            // 
            this.xrTableRow4.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell2});
            this.xrTableRow4.Dpi = 254F;
            this.xrTableRow4.Name = "xrTableRow4";
            this.xrTableRow4.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableRow4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow4.Weight = 1;
            // 
            // xrTableCell2
            // 
            this.xrTableCell2.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "esUtility.DescricaoAtivo")});
            this.xrTableCell2.Dpi = 254F;
            this.xrTableCell2.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell2.Name = "xrTableCell2";
            this.xrTableCell2.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell2.Weight = 1;
            // 
            // GroupFooter1
            // 
            this.GroupFooter1.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable3});
            this.GroupFooter1.Dpi = 254F;
            this.GroupFooter1.GroupUnion = DevExpress.XtraReports.UI.GroupFooterUnion.WithLastDetail;
            this.GroupFooter1.HeightF = 50F;
            this.GroupFooter1.KeepTogether = true;
            this.GroupFooter1.Level = 1;
            this.GroupFooter1.Name = "GroupFooter1";
            this.GroupFooter1.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.GroupFooter1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // xrTable3
            // 
            this.xrTable3.BackColor = System.Drawing.Color.Transparent;
            this.xrTable3.Borders = DevExpress.XtraPrinting.BorderSide.Top;
            this.xrTable3.Dpi = 254F;
            this.xrTable3.LocationFloat = new DevExpress.Utils.PointFloat(25F, 0F);
            this.xrTable3.Name = "xrTable3";
            this.xrTable3.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTable3.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow3});
            this.xrTable3.SizeF = new System.Drawing.SizeF(2744F, 45F);
            this.xrTable3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTable3.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.TableTotalGroupDescricaoAtivoPrint);
            // 
            // xrTableRow3
            // 
            this.xrTableRow3.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell40,
            this.xrTableCell24,
            this.xrTableCell27,
            this.xrTableCell54,
            this.xrTableCell39,
            this.xrTableCell57,
            this.xrTableCell58,
            this.xrTableCell34,
            this.xrTableCell29,
            this.xrTableCell37,
            this.xrTableCell31,
            this.xrTableCell32,
            this.xrTableCell33});
            this.xrTableRow3.Dpi = 254F;
            this.xrTableRow3.Name = "xrTableRow3";
            this.xrTableRow3.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableRow3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow3.Weight = 1;
            // 
            // xrTableCell40
            // 
            this.xrTableCell40.Dpi = 254F;
            this.xrTableCell40.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell40.Name = "xrTableCell40";
            this.xrTableCell40.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell40.StylePriority.UseFont = false;
            this.xrTableCell40.StylePriority.UseTextAlignment = false;
            this.xrTableCell40.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell40.Weight = 0.15340018866596494;
            // 
            // xrTableCell24
            // 
            this.xrTableCell24.Dpi = 254F;
            this.xrTableCell24.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell24.Name = "xrTableCell24";
            this.xrTableCell24.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell24.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell24.Weight = 0.028051424237260879;
            // 
            // xrTableCell27
            // 
            this.xrTableCell27.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "esUtility.QuantidadeDisponivel")});
            this.xrTableCell27.Dpi = 254F;
            this.xrTableCell27.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell27.Name = "xrTableCell27";
            this.xrTableCell27.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell27.StylePriority.UseFont = false;
            xrSummary1.IgnoreNullValues = true;
            xrSummary1.Running = DevExpress.XtraReports.UI.SummaryRunning.Group;
            this.xrTableCell27.Summary = xrSummary1;
            this.xrTableCell27.Text = "xrTableCell27";
            this.xrTableCell27.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell27.Weight = 0.083064516129032262;
            this.xrTableCell27.PrintOnPage += new DevExpress.XtraReports.UI.PrintOnPageEventHandler(this.CustomFormatZeroCasas);
            // 
            // xrTableCell54
            // 
            this.xrTableCell54.Dpi = 254F;
            this.xrTableCell54.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell54.Name = "xrTableCell54";
            this.xrTableCell54.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell54.StylePriority.UseFont = false;
            this.xrTableCell54.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell54.Weight = 0.074193835933401522;
            // 
            // xrTableCell39
            // 
            this.xrTableCell39.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "esUtility.Quantidade")});
            this.xrTableCell39.Dpi = 254F;
            this.xrTableCell39.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell39.Name = "xrTableCell39";
            this.xrTableCell39.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell39.StylePriority.UseFont = false;
            xrSummary2.IgnoreNullValues = true;
            xrSummary2.Running = DevExpress.XtraReports.UI.SummaryRunning.Group;
            this.xrTableCell39.Summary = xrSummary2;
            this.xrTableCell39.Text = "xrTableCell39";
            this.xrTableCell39.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell39.Weight = 0.0874293695509204;
            this.xrTableCell39.PrintOnPage += new DevExpress.XtraReports.UI.PrintOnPageEventHandler(this.CustomFormatZeroCasas);
            // 
            // xrTableCell57
            // 
            this.xrTableCell57.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "esUtility.CapitalVotante")});
            this.xrTableCell57.Dpi = 254F;
            this.xrTableCell57.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell57.Name = "xrTableCell57";
            this.xrTableCell57.StylePriority.UseFont = false;
            this.xrTableCell57.StylePriority.UseTextAlignment = false;
            xrSummary3.FormatString = "{0:n6}";
            xrSummary3.IgnoreNullValues = true;
            xrSummary3.Running = DevExpress.XtraReports.UI.SummaryRunning.Group;
            this.xrTableCell57.Summary = xrSummary3;
            this.xrTableCell57.Text = "xrTableCell57";
            this.xrTableCell57.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell57.Weight = 0.070230954092286385;
            // 
            // xrTableCell58
            // 
            this.xrTableCell58.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "esUtility.CapitalSocial")});
            this.xrTableCell58.Dpi = 254F;
            this.xrTableCell58.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell58.Name = "xrTableCell58";
            this.xrTableCell58.StylePriority.UseFont = false;
            this.xrTableCell58.StylePriority.UseTextAlignment = false;
            xrSummary4.FormatString = "{0:n6}";
            xrSummary4.IgnoreNullValues = true;
            xrSummary4.Running = DevExpress.XtraReports.UI.SummaryRunning.Group;
            this.xrTableCell58.Summary = xrSummary4;
            this.xrTableCell58.Text = "xrTableCell58";
            this.xrTableCell58.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell58.Weight = 0.064445470779262534;
            // 
            // xrTableCell34
            // 
            this.xrTableCell34.Dpi = 254F;
            this.xrTableCell34.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell34.Name = "xrTableCell34";
            this.xrTableCell34.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell34.StylePriority.UseFont = false;
            xrSummary5.FormatString = "{0:n2}";
            xrSummary5.IgnoreNullValues = true;
            this.xrTableCell34.Summary = xrSummary5;
            this.xrTableCell34.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell34.Weight = 0.070424427659224784;
            // 
            // xrTableCell29
            // 
            this.xrTableCell29.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "esUtility.ValorCustoLiquido")});
            this.xrTableCell29.Dpi = 254F;
            this.xrTableCell29.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell29.Name = "xrTableCell29";
            this.xrTableCell29.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            xrSummary6.IgnoreNullValues = true;
            xrSummary6.Running = DevExpress.XtraReports.UI.SummaryRunning.Group;
            this.xrTableCell29.Summary = xrSummary6;
            this.xrTableCell29.Text = "xrTableCell29";
            this.xrTableCell29.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell29.Weight = 0.087201878664175822;
            this.xrTableCell29.PrintOnPage += new DevExpress.XtraReports.UI.PrintOnPageEventHandler(this.CustomFormat);
            // 
            // xrTableCell37
            // 
            this.xrTableCell37.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "esUtility.ValorMercado")});
            this.xrTableCell37.Dpi = 254F;
            this.xrTableCell37.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell37.Name = "xrTableCell37";
            this.xrTableCell37.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            xrSummary7.IgnoreNullValues = true;
            xrSummary7.Running = DevExpress.XtraReports.UI.SummaryRunning.Group;
            this.xrTableCell37.Summary = xrSummary7;
            this.xrTableCell37.Text = "xrTableCell37";
            this.xrTableCell37.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell37.Weight = 0.085993418159438229;
            this.xrTableCell37.PrintOnPage += new DevExpress.XtraReports.UI.PrintOnPageEventHandler(this.CustomFormat);
            // 
            // xrTableCell31
            // 
            this.xrTableCell31.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "esUtility.ResultadoRealizar")});
            this.xrTableCell31.Dpi = 254F;
            this.xrTableCell31.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell31.Name = "xrTableCell31";
            this.xrTableCell31.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            xrSummary8.IgnoreNullValues = true;
            xrSummary8.Running = DevExpress.XtraReports.UI.SummaryRunning.Group;
            this.xrTableCell31.Summary = xrSummary8;
            this.xrTableCell31.Text = "xrTableCell31";
            this.xrTableCell31.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell31.Weight = 0.0939516129032258;
            this.xrTableCell31.PrintOnPage += new DevExpress.XtraReports.UI.PrintOnPageEventHandler(this.CustomFormat);
            // 
            // xrTableCell32
            // 
            this.xrTableCell32.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "esUtility.calculatedFieldAcoes")});
            this.xrTableCell32.Dpi = 254F;
            this.xrTableCell32.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell32.Name = "xrTableCell32";
            this.xrTableCell32.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            xrSummary9.IgnoreNullValues = true;
            xrSummary9.Running = DevExpress.XtraReports.UI.SummaryRunning.Group;
            this.xrTableCell32.Summary = xrSummary9;
            this.xrTableCell32.Text = "xrTableCell32";
            this.xrTableCell32.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell32.Weight = 0.051209677419354838;
            this.xrTableCell32.PrintOnPage += new DevExpress.XtraReports.UI.PrintOnPageEventHandler(this.CustomFormatPorcentagem);
            // 
            // xrTableCell33
            // 
            this.xrTableCell33.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "esUtility.calculatedPLPorcentagem")});
            this.xrTableCell33.Dpi = 254F;
            this.xrTableCell33.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell33.Name = "xrTableCell33";
            this.xrTableCell33.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            xrSummary10.IgnoreNullValues = true;
            xrSummary10.Running = DevExpress.XtraReports.UI.SummaryRunning.Group;
            this.xrTableCell33.Summary = xrSummary10;
            this.xrTableCell33.Text = "xrTableCell33";
            this.xrTableCell33.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell33.Weight = 0.05040322580645161;
            this.xrTableCell33.PrintOnPage += new DevExpress.XtraReports.UI.PrintOnPageEventHandler(this.CustomFormatPLPorcentagem);
            // 
            // ParameterValorPL
            // 
            this.ParameterValorPL.Name = "ParameterValorPL";
            this.ParameterValorPL.Type = typeof(decimal);
            this.ParameterValorPL.Value = 0;
            // 
            // ReportFooter
            // 
            this.ReportFooter.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable5});
            this.ReportFooter.Dpi = 254F;
            this.ReportFooter.HeightF = 59.00003F;
            this.ReportFooter.KeepTogether = true;
            this.ReportFooter.Name = "ReportFooter";
            this.ReportFooter.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.ReportFooter.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrTable5
            // 
            this.xrTable5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(238)))), ((int)(((byte)(238)))), ((int)(((byte)(238)))));
            this.xrTable5.Borders = DevExpress.XtraPrinting.BorderSide.Top;
            this.xrTable5.Dpi = 254F;
            this.xrTable5.LocationFloat = new DevExpress.Utils.PointFloat(26F, 13.00001F);
            this.xrTable5.Name = "xrTable5";
            this.xrTable5.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTable5.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow5});
            this.xrTable5.SizeF = new System.Drawing.SizeF(2744F, 46.00002F);
            this.xrTable5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTable5.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.TableTotalBeforePrint);
            // 
            // xrTableRow5
            // 
            this.xrTableRow5.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell41,
            this.xrTableCell44,
            this.xrTableCell45,
            this.xrTableCell56,
            this.xrTableCell53,
            this.xrTableCell59,
            this.xrTableCell60,
            this.xrTableCell46,
            this.xrTableCell47,
            this.xrTableCell49,
            this.xrTableCell50,
            this.xrTableCell51,
            this.xrTableCell52});
            this.xrTableRow5.Dpi = 254F;
            this.xrTableRow5.Name = "xrTableRow5";
            this.xrTableRow5.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableRow5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow5.Weight = 1;
            // 
            // xrTableCell41
            // 
            this.xrTableCell41.Dpi = 254F;
            this.xrTableCell41.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Bold);
            this.xrTableCell41.Name = "xrTableCell41";
            this.xrTableCell41.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell41.Text = "#Total";
            this.xrTableCell41.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell41.Weight = 0.068145161290322581;
            // 
            // xrTableCell44
            // 
            this.xrTableCell44.Dpi = 254F;
            this.xrTableCell44.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell44.Name = "xrTableCell44";
            this.xrTableCell44.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell44.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell44.Weight = 0.11290322580645161;
            // 
            // xrTableCell45
            // 
            this.xrTableCell45.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "esUtility.QuantidadeDisponivel")});
            this.xrTableCell45.Dpi = 254F;
            this.xrTableCell45.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Bold);
            this.xrTableCell45.Name = "xrTableCell45";
            this.xrTableCell45.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            xrSummary11.IgnoreNullValues = true;
            xrSummary11.Running = DevExpress.XtraReports.UI.SummaryRunning.Report;
            this.xrTableCell45.Summary = xrSummary11;
            this.xrTableCell45.Text = "xrTableCell45";
            this.xrTableCell45.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell45.Weight = 0.083467741935483869;
            this.xrTableCell45.PrintOnPage += new DevExpress.XtraReports.UI.PrintOnPageEventHandler(this.CustomFormatZeroCasas);
            // 
            // xrTableCell56
            // 
            this.xrTableCell56.Dpi = 254F;
            this.xrTableCell56.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Bold);
            this.xrTableCell56.Name = "xrTableCell56";
            this.xrTableCell56.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell56.StylePriority.UseFont = false;
            this.xrTableCell56.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell56.Weight = 0.073790548958334853;
            // 
            // xrTableCell53
            // 
            this.xrTableCell53.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "esUtility.Quantidade")});
            this.xrTableCell53.Dpi = 254F;
            this.xrTableCell53.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Bold);
            this.xrTableCell53.Name = "xrTableCell53";
            this.xrTableCell53.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell53.StylePriority.UseFont = false;
            xrSummary12.IgnoreNullValues = true;
            xrSummary12.Running = DevExpress.XtraReports.UI.SummaryRunning.Report;
            this.xrTableCell53.Summary = xrSummary12;
            this.xrTableCell53.Text = "xrTableCell53";
            this.xrTableCell53.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell53.Weight = 0.087429430719535434;
            this.xrTableCell53.PrintOnPage += new DevExpress.XtraReports.UI.PrintOnPageEventHandler(this.CustomFormatZeroCasas);
            // 
            // xrTableCell59
            // 
            this.xrTableCell59.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "esUtility.CapitalVotante")});
            this.xrTableCell59.Dpi = 254F;
            this.xrTableCell59.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Bold);
            this.xrTableCell59.Name = "xrTableCell59";
            this.xrTableCell59.StylePriority.UseFont = false;
            this.xrTableCell59.StylePriority.UseTextAlignment = false;
            xrSummary13.FormatString = "{0:n6}";
            xrSummary13.IgnoreNullValues = true;
            xrSummary13.Running = DevExpress.XtraReports.UI.SummaryRunning.Report;
            this.xrTableCell59.Summary = xrSummary13;
            this.xrTableCell59.Text = "xrTableCell59";
            this.xrTableCell59.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell59.Weight = 0.0702309096060209;
            // 
            // xrTableCell60
            // 
            this.xrTableCell60.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "esUtility.CapitalSocial")});
            this.xrTableCell60.Dpi = 254F;
            this.xrTableCell60.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Bold);
            this.xrTableCell60.Name = "xrTableCell60";
            this.xrTableCell60.StylePriority.UseFont = false;
            this.xrTableCell60.StylePriority.UseTextAlignment = false;
            xrSummary14.FormatString = "{0:n6}";
            xrSummary14.IgnoreNullValues = true;
            xrSummary14.Running = DevExpress.XtraReports.UI.SummaryRunning.Report;
            this.xrTableCell60.Summary = xrSummary14;
            this.xrTableCell60.Text = "xrTableCell60";
            this.xrTableCell60.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell60.Weight = 0.0644455597517935;
            // 
            // xrTableCell46
            // 
            this.xrTableCell46.Dpi = 254F;
            this.xrTableCell46.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Bold);
            this.xrTableCell46.Name = "xrTableCell46";
            this.xrTableCell46.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell46.StylePriority.UseFont = false;
            this.xrTableCell46.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell46.Weight = 0.070827475520614441;
            // 
            // xrTableCell47
            // 
            this.xrTableCell47.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "esUtility.ValorCustoLiquido")});
            this.xrTableCell47.Dpi = 254F;
            this.xrTableCell47.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Bold);
            this.xrTableCell47.Name = "xrTableCell47";
            this.xrTableCell47.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            xrSummary15.IgnoreNullValues = true;
            xrSummary15.Running = DevExpress.XtraReports.UI.SummaryRunning.Report;
            this.xrTableCell47.Summary = xrSummary15;
            this.xrTableCell47.Text = "xrTableCell47";
            this.xrTableCell47.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell47.Weight = 0.0872019676367068;
            this.xrTableCell47.PrintOnPage += new DevExpress.XtraReports.UI.PrintOnPageEventHandler(this.CustomFormat);
            // 
            // xrTableCell49
            // 
            this.xrTableCell49.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "esUtility.ValorMercado")});
            this.xrTableCell49.Dpi = 254F;
            this.xrTableCell49.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Bold);
            this.xrTableCell49.Name = "xrTableCell49";
            this.xrTableCell49.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            xrSummary16.IgnoreNullValues = true;
            xrSummary16.Running = DevExpress.XtraReports.UI.SummaryRunning.Report;
            this.xrTableCell49.Summary = xrSummary16;
            this.xrTableCell49.Text = "xrTableCell49";
            this.xrTableCell49.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell49.Weight = 0.085993462645703719;
            this.xrTableCell49.PrintOnPage += new DevExpress.XtraReports.UI.PrintOnPageEventHandler(this.CustomFormat);
            // 
            // xrTableCell50
            // 
            this.xrTableCell50.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "esUtility.ResultadoRealizar")});
            this.xrTableCell50.Dpi = 254F;
            this.xrTableCell50.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Bold);
            this.xrTableCell50.Name = "xrTableCell50";
            this.xrTableCell50.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            xrSummary17.IgnoreNullValues = true;
            xrSummary17.Running = DevExpress.XtraReports.UI.SummaryRunning.Report;
            this.xrTableCell50.Summary = xrSummary17;
            this.xrTableCell50.Text = "xrTableCell50";
            this.xrTableCell50.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell50.Weight = 0.0939516129032258;
            this.xrTableCell50.PrintOnPage += new DevExpress.XtraReports.UI.PrintOnPageEventHandler(this.CustomFormat);
            // 
            // xrTableCell51
            // 
            this.xrTableCell51.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "esUtility.calculatedFieldAcoes")});
            this.xrTableCell51.Dpi = 254F;
            this.xrTableCell51.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Bold);
            this.xrTableCell51.Name = "xrTableCell51";
            this.xrTableCell51.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            xrSummary18.IgnoreNullValues = true;
            xrSummary18.Running = DevExpress.XtraReports.UI.SummaryRunning.Report;
            this.xrTableCell51.Summary = xrSummary18;
            this.xrTableCell51.Text = "xrTableCell51";
            this.xrTableCell51.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell51.Weight = 0.051209677419354838;
            this.xrTableCell51.PrintOnPage += new DevExpress.XtraReports.UI.PrintOnPageEventHandler(this.CustomFormatPorcentagem);
            // 
            // xrTableCell52
            // 
            this.xrTableCell52.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "esUtility.calculatedPLPorcentagem")});
            this.xrTableCell52.Dpi = 254F;
            this.xrTableCell52.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Bold);
            this.xrTableCell52.Name = "xrTableCell52";
            this.xrTableCell52.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            xrSummary19.IgnoreNullValues = true;
            xrSummary19.Running = DevExpress.XtraReports.UI.SummaryRunning.Report;
            this.xrTableCell52.Summary = xrSummary19;
            this.xrTableCell52.Text = "xrTableCell52";
            this.xrTableCell52.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell52.Weight = 0.05040322580645161;
            this.xrTableCell52.PrintOnPage += new DevExpress.XtraReports.UI.PrintOnPageEventHandler(this.CustomFormatPLPorcentagem);
            // 
            // ParameterTotalValorMercado
            // 
            this.ParameterTotalValorMercado.Name = "ParameterTotalValorMercado";
            this.ParameterTotalValorMercado.Type = typeof(decimal);
            this.ParameterTotalValorMercado.Value = 0;
            // 
            // calculatedFieldAcoes
            // 
            this.calculatedFieldAcoes.DataMember = "esUtility";
            this.calculatedFieldAcoes.Expression = "[ValorMercado] /[Parameters.ParameterTotalValorMercado]";
            this.calculatedFieldAcoes.Name = "calculatedFieldAcoes";
            // 
            // calculatedPLPorcentagem
            // 
            this.calculatedPLPorcentagem.DataMember = "esUtility";
            this.calculatedPLPorcentagem.Expression = "[ValorMercado] / [Parameters.ParameterValorPL]";
            this.calculatedPLPorcentagem.Name = "calculatedPLPorcentagem";
            // 
            // topMarginBand1
            // 
            this.topMarginBand1.Dpi = 254F;
            this.topMarginBand1.HeightF = 150F;
            this.topMarginBand1.Name = "topMarginBand1";
            // 
            // bottomMarginBand1
            // 
            this.bottomMarginBand1.Dpi = 254F;
            this.bottomMarginBand1.HeightF = 150F;
            this.bottomMarginBand1.Name = "bottomMarginBand1";
            // 
            // GroupHeader2
            // 
            this.GroupHeader2.Dpi = 254F;
            this.GroupHeader2.GroupFields.AddRange(new DevExpress.XtraReports.UI.GroupField[] {
            new DevExpress.XtraReports.UI.GroupField("CdAtivoBolsa", DevExpress.XtraReports.UI.XRColumnSortOrder.Ascending)});
            this.GroupHeader2.GroupUnion = DevExpress.XtraReports.UI.GroupUnion.WithFirstDetail;
            this.GroupHeader2.HeightF = 0F;
            this.GroupHeader2.KeepTogether = true;
            this.GroupHeader2.Name = "GroupHeader2";
            this.GroupHeader2.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.GroupHeaderCdAtivoBolsaBeforePrint);
            // 
            // GroupFooter2
            // 
            this.GroupFooter2.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable2});
            this.GroupFooter2.Dpi = 254F;
            this.GroupFooter2.GroupUnion = DevExpress.XtraReports.UI.GroupFooterUnion.WithLastDetail;
            this.GroupFooter2.HeightF = 45F;
            this.GroupFooter2.KeepTogether = true;
            this.GroupFooter2.Name = "GroupFooter2";
            // 
            // xrTable2
            // 
            this.xrTable2.BackColor = System.Drawing.Color.Transparent;
            this.xrTable2.Borders = DevExpress.XtraPrinting.BorderSide.Top;
            this.xrTable2.Dpi = 254F;
            this.xrTable2.LocationFloat = new DevExpress.Utils.PointFloat(25F, 0F);
            this.xrTable2.Name = "xrTable2";
            this.xrTable2.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTable2.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow6});
            this.xrTable2.SizeF = new System.Drawing.SizeF(2744F, 45F);
            this.xrTable2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTable2.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.TableTotalGroupCdAtivoBeforePrint);
            // 
            // xrTableRow6
            // 
            this.xrTableRow6.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell8,
            this.xrTableCell16,
            this.xrTableCell30,
            this.xrTableCell48,
            this.xrTableCell61,
            this.xrTableCell62,
            this.xrTableCell63,
            this.xrTableCell64,
            this.xrTableCell65,
            this.xrTableCell66,
            this.xrTableCell67,
            this.xrTableCell68,
            this.xrTableCell69});
            this.xrTableRow6.Dpi = 254F;
            this.xrTableRow6.Name = "xrTableRow6";
            this.xrTableRow6.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.xrTableRow6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow6.Weight = 1;
            // 
            // xrTableCell8
            // 
            this.xrTableCell8.Dpi = 254F;
            this.xrTableCell8.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell8.Name = "xrTableCell8";
            this.xrTableCell8.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell8.StylePriority.UseFont = false;
            this.xrTableCell8.StylePriority.UseTextAlignment = false;
            this.xrTableCell8.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell8.Weight = 0.15340018866596492;
            // 
            // xrTableCell16
            // 
            this.xrTableCell16.Dpi = 254F;
            this.xrTableCell16.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell16.Name = "xrTableCell16";
            this.xrTableCell16.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell16.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell16.Weight = 0.028051424237260862;
            // 
            // xrTableCell30
            // 
            this.xrTableCell30.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "esUtility.QuantidadeDisponivel")});
            this.xrTableCell30.Dpi = 254F;
            this.xrTableCell30.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell30.Name = "xrTableCell30";
            this.xrTableCell30.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell30.StylePriority.UseFont = false;
            xrSummary20.IgnoreNullValues = true;
            xrSummary20.Running = DevExpress.XtraReports.UI.SummaryRunning.Group;
            this.xrTableCell30.Summary = xrSummary20;
            this.xrTableCell30.Text = "xrTableCell27";
            this.xrTableCell30.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell30.Weight = 0.083064516129032262;
            this.xrTableCell30.PrintOnPage += new DevExpress.XtraReports.UI.PrintOnPageEventHandler(this.CustomFormatZeroCasas);
            // 
            // xrTableCell48
            // 
            this.xrTableCell48.Dpi = 254F;
            this.xrTableCell48.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell48.Name = "xrTableCell48";
            this.xrTableCell48.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell48.StylePriority.UseFont = false;
            this.xrTableCell48.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell48.Weight = 0.074193835933401522;
            // 
            // xrTableCell61
            // 
            this.xrTableCell61.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "esUtility.Quantidade")});
            this.xrTableCell61.Dpi = 254F;
            this.xrTableCell61.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell61.Name = "xrTableCell61";
            this.xrTableCell61.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell61.StylePriority.UseFont = false;
            xrSummary21.IgnoreNullValues = true;
            xrSummary21.Running = DevExpress.XtraReports.UI.SummaryRunning.Group;
            this.xrTableCell61.Summary = xrSummary21;
            this.xrTableCell61.Text = "xrTableCell39";
            this.xrTableCell61.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell61.Weight = 0.0874293695509204;
            this.xrTableCell61.PrintOnPage += new DevExpress.XtraReports.UI.PrintOnPageEventHandler(this.CustomFormatZeroCasas);
            // 
            // xrTableCell62
            // 
            this.xrTableCell62.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "esUtility.CapitalVotante")});
            this.xrTableCell62.Dpi = 254F;
            this.xrTableCell62.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell62.Name = "xrTableCell62";
            this.xrTableCell62.StylePriority.UseFont = false;
            this.xrTableCell62.StylePriority.UseTextAlignment = false;
            xrSummary22.FormatString = "{0:n6}";
            xrSummary22.IgnoreNullValues = true;
            xrSummary22.Running = DevExpress.XtraReports.UI.SummaryRunning.Group;
            this.xrTableCell62.Summary = xrSummary22;
            this.xrTableCell62.Text = "xrTableCell57";
            this.xrTableCell62.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell62.Weight = 0.070230954092286385;
            // 
            // xrTableCell63
            // 
            this.xrTableCell63.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "esUtility.CapitalSocial")});
            this.xrTableCell63.Dpi = 254F;
            this.xrTableCell63.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell63.Name = "xrTableCell63";
            this.xrTableCell63.StylePriority.UseFont = false;
            this.xrTableCell63.StylePriority.UseTextAlignment = false;
            xrSummary23.FormatString = "{0:n6}";
            xrSummary23.IgnoreNullValues = true;
            xrSummary23.Running = DevExpress.XtraReports.UI.SummaryRunning.Group;
            this.xrTableCell63.Summary = xrSummary23;
            this.xrTableCell63.Text = "xrTableCell58";
            this.xrTableCell63.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell63.Weight = 0.064445470779262534;
            // 
            // xrTableCell64
            // 
            this.xrTableCell64.Dpi = 254F;
            this.xrTableCell64.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell64.Name = "xrTableCell64";
            this.xrTableCell64.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell64.StylePriority.UseFont = false;
            xrSummary24.FormatString = "{0:n2}";
            xrSummary24.IgnoreNullValues = true;
            this.xrTableCell64.Summary = xrSummary24;
            this.xrTableCell64.Text = "PuCustoMedio";
            this.xrTableCell64.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell64.Weight = 0.070424427659224784;
            this.xrTableCell64.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.PuBeforePrint);
            // 
            // xrTableCell65
            // 
            this.xrTableCell65.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "esUtility.ValorCustoLiquido")});
            this.xrTableCell65.Dpi = 254F;
            this.xrTableCell65.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell65.Name = "xrTableCell65";
            this.xrTableCell65.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            xrSummary25.IgnoreNullValues = true;
            xrSummary25.Running = DevExpress.XtraReports.UI.SummaryRunning.Group;
            this.xrTableCell65.Summary = xrSummary25;
            this.xrTableCell65.Text = "xrTableCell29";
            this.xrTableCell65.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell65.Weight = 0.087201878664175822;
            this.xrTableCell65.PrintOnPage += new DevExpress.XtraReports.UI.PrintOnPageEventHandler(this.CustomFormat);
            // 
            // xrTableCell66
            // 
            this.xrTableCell66.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "esUtility.ValorMercado")});
            this.xrTableCell66.Dpi = 254F;
            this.xrTableCell66.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell66.Name = "xrTableCell66";
            this.xrTableCell66.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            xrSummary26.IgnoreNullValues = true;
            xrSummary26.Running = DevExpress.XtraReports.UI.SummaryRunning.Group;
            this.xrTableCell66.Summary = xrSummary26;
            this.xrTableCell66.Text = "xrTableCell37";
            this.xrTableCell66.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell66.Weight = 0.085993418159438229;
            this.xrTableCell66.PrintOnPage += new DevExpress.XtraReports.UI.PrintOnPageEventHandler(this.CustomFormat);
            // 
            // xrTableCell67
            // 
            this.xrTableCell67.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "esUtility.ResultadoRealizar")});
            this.xrTableCell67.Dpi = 254F;
            this.xrTableCell67.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell67.Name = "xrTableCell67";
            this.xrTableCell67.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            xrSummary27.IgnoreNullValues = true;
            xrSummary27.Running = DevExpress.XtraReports.UI.SummaryRunning.Group;
            this.xrTableCell67.Summary = xrSummary27;
            this.xrTableCell67.Text = "xrTableCell31";
            this.xrTableCell67.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell67.Weight = 0.0939516129032258;
            this.xrTableCell67.PrintOnPage += new DevExpress.XtraReports.UI.PrintOnPageEventHandler(this.CustomFormat);
            // 
            // xrTableCell68
            // 
            this.xrTableCell68.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "esUtility.calculatedFieldAcoes")});
            this.xrTableCell68.Dpi = 254F;
            this.xrTableCell68.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell68.Name = "xrTableCell68";
            this.xrTableCell68.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            xrSummary28.IgnoreNullValues = true;
            xrSummary28.Running = DevExpress.XtraReports.UI.SummaryRunning.Group;
            this.xrTableCell68.Summary = xrSummary28;
            this.xrTableCell68.Text = "xrTableCell32";
            this.xrTableCell68.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell68.Weight = 0.051209677419354838;
            this.xrTableCell68.PrintOnPage += new DevExpress.XtraReports.UI.PrintOnPageEventHandler(this.CustomFormatPorcentagem);
            // 
            // xrTableCell69
            // 
            this.xrTableCell69.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "esUtility.calculatedPLPorcentagem")});
            this.xrTableCell69.Dpi = 254F;
            this.xrTableCell69.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCell69.Name = "xrTableCell69";
            this.xrTableCell69.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            xrSummary29.IgnoreNullValues = true;
            xrSummary29.Running = DevExpress.XtraReports.UI.SummaryRunning.Group;
            this.xrTableCell69.Summary = xrSummary29;
            this.xrTableCell69.Text = "xrTableCell33";
            this.xrTableCell69.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell69.Weight = 0.05040322580645161;
            this.xrTableCell69.PrintOnPage += new DevExpress.XtraReports.UI.PrintOnPageEventHandler(this.CustomFormatPLPorcentagem);
            // 
            // SubReportAcaoMasaPF
            // 
            this.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail,
            this.PageFooter,
            this.PageHeader,
            this.GroupHeader1,
            this.GroupFooter1,
            this.ReportFooter,
            this.topMarginBand1,
            this.bottomMarginBand1,
            this.GroupHeader2,
            this.GroupFooter2});
            this.CalculatedFields.AddRange(new DevExpress.XtraReports.UI.CalculatedField[] {
            this.calculatedFieldAcoes,
            this.calculatedPLPorcentagem});
            this.Dpi = 254F;
            this.ExportOptions.Html.RemoveSecondarySymbols = true;
            this.ExportOptions.Mht.RemoveSecondarySymbols = true;
            this.Landscape = true;
            this.Margins = new System.Drawing.Printing.Margins(0, 0, 150, 150);
            this.PageHeight = 2159;
            this.PageWidth = 2794;
            this.Parameters.AddRange(new DevExpress.XtraReports.Parameters.Parameter[] {
            this.ParameterTotalValorMercado,
            this.ParameterValorPL});
            this.PrintOnEmptyDataSource = false;
            this.ReportUnit = DevExpress.XtraReports.UI.ReportUnit.TenthsOfAMillimeter;
            this.Version = "11.1";
            this.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.ReportBeforePrint);
            ((System.ComponentModel.ISupportInitialize)(this.xrTable6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTableFooter)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }

        #endregion

        private System.Resources.ResourceManager GetResourceManager() {
            return Resources.SubReportAcao.ResourceManager;
        }

        // Armazena o Total Mercado para ser usado no calculo do campo Opcões
        private decimal totalValorMercado = 0;

        #region Funções Internas do Relatorio
        //
        private void CdAtivoBolsaBeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {            
            XRTableCell cdAtivoBolsaXRTableCell = sender as XRTableCell;

            string cdAtivoBolsa = cdAtivoBolsaXRTableCell.Text;
            int idCarteira = this.idCarteira;
            DateTime data = this.dataReferencia;

            if (this.tipoPesquisa == TipoPesquisa.PosicaoBolsa)
            {
                PosicaoEmprestimoBolsaCollection posicaoEmprestimoBolsaCollection = new PosicaoEmprestimoBolsaCollection();                
                if (posicaoEmprestimoBolsaCollection.ExistePosicaoEmprestimoBolsa(idCarteira, cdAtivoBolsa))
                {
                    cdAtivoBolsaXRTableCell.Text = cdAtivoBolsa + " *";
                    this.existePosicaoEmprestimo = true;
                    //xrMensagem.Visible = true;
                    //xrTableFooter.Visible = true;
                }
            }
            else if (this.tipoPesquisa == TipoPesquisa.PosicaoBolsaAbertura)
            {
                PosicaoEmprestimoBolsaAberturaCollection posicaoEmprestimoBolsaAberturaCollection = new PosicaoEmprestimoBolsaAberturaCollection();
                if (posicaoEmprestimoBolsaAberturaCollection.ExistePosicaoEmprestimoBolsa(idCarteira, cdAtivoBolsa, data))
                {
                    cdAtivoBolsaXRTableCell.Text = cdAtivoBolsa + " *";
                    this.existePosicaoEmprestimo = true;
                    //xrMensagem.Visible = true;
                    //xrTableFooter.Visible = true;
                }                
            }
            else if (this.tipoPesquisa == TipoPesquisa.PosicaoBolsaHistorico)
            {
                PosicaoEmprestimoBolsaHistoricoCollection posicaoEmprestimoBolsaHistoricoCollection = new PosicaoEmprestimoBolsaHistoricoCollection();
                if (posicaoEmprestimoBolsaHistoricoCollection.ExistePosicaoEmprestimoBolsa(idCarteira, cdAtivoBolsa, data))
                {
                    cdAtivoBolsaXRTableCell.Text = cdAtivoBolsa + " *";
                    this.existePosicaoEmprestimo = true;
                    //xrMensagem.Visible = true;
                    //xrTableFooter.Visible = true;
                }
            }
        }
        
        /*
         *  Executado no start do relatorio
         */
        private void ReportBeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e) {
            // Calcula o totalMercado
                        
            if (this.tipoPesquisa == TipoPesquisa.PosicaoBolsa)
            {
                PosicaoBolsaCollection posicaoBolsaCollection = new PosicaoBolsaCollection();
                posicaoBolsaCollection.Query.Select(posicaoBolsaCollection.Query.CdAtivoBolsa,
                                                    posicaoBolsaCollection.Query.ValorMercado.Sum());
                posicaoBolsaCollection.Query.Where(posicaoBolsaCollection.Query.IdCliente == this.idCarteira,
                                                   posicaoBolsaCollection.Query.TipoMercado.In(TipoMercadoBolsa.MercadoVista));
                posicaoBolsaCollection.Query.GroupBy(posicaoBolsaCollection.Query.CdAtivoBolsa);
                posicaoBolsaCollection.Query.Load();

                decimal totalValor = 0;
                foreach (PosicaoBolsa posicaoBolsa in posicaoBolsaCollection)
                {
                    string cdAtivoBolsa = posicaoBolsa.CdAtivoBolsa;
                    decimal valorMercado = posicaoBolsa.ValorMercado.Value;

                    AtivoBolsa ativoBolsa = new AtivoBolsa();
                    ativoBolsa.LoadByPrimaryKey(cdAtivoBolsa);
                    int idMoedaAtivo = ativoBolsa.IdMoeda.Value;

                    ConversaoMoeda conversaoMoeda = new ConversaoMoeda();
                    decimal fatorConversao = conversaoMoeda.RetornaFatorConversao(this.idMoeda, idMoedaAtivo, this.dataReferencia);

                    valorMercado = Math.Round(valorMercado * fatorConversao, 2);

                    totalValor += valorMercado;
                }

                this.totalValorMercado = totalValor;
            }
            else if (this.tipoPesquisa == TipoPesquisa.PosicaoBolsaHistorico)
            {
                PosicaoBolsaHistoricoCollection posicaoBolsaHistoricoCollection = new PosicaoBolsaHistoricoCollection();
                posicaoBolsaHistoricoCollection.Query.Select(posicaoBolsaHistoricoCollection.Query.CdAtivoBolsa,
                                                             posicaoBolsaHistoricoCollection.Query.ValorMercado.Sum());
                posicaoBolsaHistoricoCollection.Query.Where(posicaoBolsaHistoricoCollection.Query.IdCliente == this.idCarteira,
                                                            posicaoBolsaHistoricoCollection.Query.TipoMercado.In(TipoMercadoBolsa.MercadoVista),
                                                            posicaoBolsaHistoricoCollection.Query.DataHistorico.Equal(this.dataReferencia));
                posicaoBolsaHistoricoCollection.Query.GroupBy(posicaoBolsaHistoricoCollection.Query.CdAtivoBolsa);
                posicaoBolsaHistoricoCollection.Query.Load();

                decimal totalValor = 0;
                foreach (PosicaoBolsaHistorico posicaoBolsaHistorico in posicaoBolsaHistoricoCollection)
                {
                    string cdAtivoBolsa = posicaoBolsaHistorico.CdAtivoBolsa;
                    decimal valorMercado = posicaoBolsaHistorico.ValorMercado.Value;

                    AtivoBolsa ativoBolsa = new AtivoBolsa();
                    ativoBolsa.LoadByPrimaryKey(cdAtivoBolsa);
                    int idMoedaAtivo = ativoBolsa.IdMoeda.Value;

                    ConversaoMoeda conversaoMoeda = new ConversaoMoeda();
                    decimal fatorConversao = conversaoMoeda.RetornaFatorConversao(this.idMoeda, idMoedaAtivo, this.dataReferencia);

                    valorMercado = Math.Round(valorMercado * fatorConversao, 2);

                    totalValor += valorMercado;
                }

                this.totalValorMercado = totalValor;
            }            

            #region Preenche Parametros para Poder Fazer SUM de %Acoes e %PL

            // Preenche o TotalValorMercado para ser Usado no Calculo de Ações
            this.Parameters["ParameterTotalValorMercado"].Value = this.totalValorMercado;
            //
            HistoricoCota historicoCota = new HistoricoCota();

            bool excecaoCota = false;
            try {
                historicoCota.BuscaValorPatrimonioDia(this.idCarteira, this.dataReferencia);
            }
            catch (HistoricoCotaNaoCadastradoException e1) {
                excecaoCota = true;
            }

            decimal valorPL = 0;

            if (excecaoCota) {
                valorPL = 0.0M;
            }
            else {
                valorPL = this.carteira.IsTipoCotaAbertura()
                        ? historicoCota.PLAbertura.Value
                        : historicoCota.PLFechamento.Value;
            }

            this.Parameters["ParameterValorPL"].Value = valorPL;

            #endregion
        }

        #region Exibe Totais
        private void TableTotalBeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e) {
            XRTable summaryFinal = sender as XRTable;
            //            
            XRTableRow summaryFinalRow0 = summaryFinal.Rows[0];
            ((XRTableCell)summaryFinalRow0.Cells[0]).Text = "";

            if (this.numeroLinhasDataTable != 0) {
                ((XRTableCell)summaryFinalRow0.Cells[0]).Text = Resources.SubReportAcao._Total;
            }
        }

        #endregion

        #region Formatos
        /// <summary>
        /// Aplica o formato na Célula com 2 duas Casas Decimais
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CustomFormat(object sender, PrintOnPageEventArgs e) {
            XRTableCell valorXRTableCell = sender as XRTableCell;
            decimal valor = 0.00M;
            try {
                valor = Convert.ToDecimal(valorXRTableCell.Text);
            }
            catch (Exception e1) {
                // Não faz nada
            }

            ReportBase.ConfiguraSinalNegativo(valorXRTableCell, valor);
        }

        /// <summary>
        /// Aplica o formato na Célula com 2 duas Casas Decimais e Porcentagem
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CustomFormatPorcentagem(object sender, PrintOnPageEventArgs e) {
            XRTableCell valorXRTableCell = sender as XRTableCell;
            decimal valor = 0.00M;
            try {
                valor = Convert.ToDecimal(valorXRTableCell.Text);
            }
            catch (Exception e1) {
                // Não faz nada
            }
            ReportBase.ConfiguraSinalNegativo(valorXRTableCell, valor, ReportBase.NumeroCasasDecimais.DuasCasasDecimaisPorcentagem);

        }

        /// <summary>
        /// Aplica o formato na Célula com zero Casas Decimais 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CustomFormatZeroCasas(object sender, PrintOnPageEventArgs e) {
            XRTableCell valorXRTableCell = sender as XRTableCell;
            decimal valor = 0M;
            try {
                valor = Convert.ToDecimal(valorXRTableCell.Text);
            }
            catch (Exception e1) {
                // Não faz nada
            }

            ReportBase.ConfiguraSinalNegativo(valorXRTableCell, valor, ReportBase.NumeroCasasDecimais.ZeroCasasDecimais);
        }

        #endregion

        private void MsgFooterBeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e) {
            XRLabel messageFooterXRLable = sender as XRLabel;
            messageFooterXRLable.Text = "";

            if (this.numeroLinhasDataTable != 0) {
                messageFooterXRLable.Text = Resources.SubReportAcao._MensagemInformativa;           
            }
        }
        #endregion

        /// <summary>
        /// Controla a Visibilidade dos Labels do PageFooter
        /// - Se existe PosicaoEmprestimo, todos labels são Visiveis
        /// - Se não existe PosicaoEmprestimo, só o primeiro é visivel
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void PageFooterBeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e) {
            if (this.existePosicaoEmprestimo) {
                this.xrTable1.Visible = true;
                this.xrMensagem.Visible = true;
                this.xrTableFooter.Visible = true;
            }
            else {
                this.xrTable1.Visible = true;
                this.xrMensagem.Visible = false;
                this.xrTableFooter.Visible = false;                
            }
        }

        /// <summary>
        /// Coluna PL é retirada se TipoReport = ReportComposicaoCarteiraSemResumo
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CustomFormatPLPorcentagem(object sender, PrintOnPageEventArgs e) {
            if (this.MasterReport is ReportComposicaoCarteiraSemResumo) {
                 XRTableCell valorXRTableCell = sender as XRTableCell;
                 valorXRTableCell.Text = "";
            }
            else {
                XRTableCell valorXRTableCell = sender as XRTableCell;
                decimal valor = 0.00M;
                try {
                    valor = Convert.ToDecimal(valorXRTableCell.Text);
                }
                catch (Exception e1) {
                    // Não faz nada
                }
                ReportBase.ConfiguraSinalNegativo(valorXRTableCell, valor, ReportBase.NumeroCasasDecimais.DuasCasasDecimaisPorcentagem);
            }
        }


        private decimal valorsum = 0;
        private decimal quantidadesum = 0;

        private string cdAtivoCorrente = "";

        // Controla a Quebra de Pagina do Grupo codigoAtivo
        private string cdAtivoGrupo = "";
        
        private void QuantidadeBeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e) {
            if (this.numeroLinhasDataTable != 0) {
                decimal quantidade = Convert.ToDecimal(this.GetCurrentColumnValue("Quantidade"));

                this.quantidadesum += quantidade;
            }
        }

        private void ValorCustoBeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e) {
            if (this.numeroLinhasDataTable != 0) {
                decimal vlCusto = Convert.ToDecimal(this.GetCurrentColumnValue("ValorCustoLiquido"));

                this.valorsum += vlCusto;
            }
        }

        private void GroupHeaderCdAtivoBolsaBeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e) {
            if (this.numeroLinhasDataTable != 0) {
                // Salva o codigo do Ativo Corrente
                this.cdAtivoCorrente = (string)this.GetCurrentColumnValue("CdAtivoBolsa");

                // Só Zerar quando não for quebra de Pagina
                string controleCdAtivoGrupo = (string)this.GetCurrentColumnValue("CdAtivoBolsa");

                if (controleCdAtivoGrupo.Trim() != this.cdAtivoGrupo.Trim()) {
                    // Salva o codigo Ativo do Grupo Atual
                    this.cdAtivoGrupo = controleCdAtivoGrupo;
                    //
                    this.valorsum = 0;
                    this.quantidadesum = 0;
                }
            }
        }

        private void PuBeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e) {
            XRTableCell puXRTableCell = sender as XRTableCell;

            decimal? valor = null;
            try {
                valor = this.valorsum / this.quantidadesum;
            }
            catch (Exception e1) {
                // Não faz nada
            }

            ReportBase.ConfiguraSinalNegativo(puXRTableCell, valor, false, ReportBase.NumeroCasasDecimais.DuasCasasDecimais);
        }

        private void TableTotalGroupCdAtivoBeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e) {
            XRTable summaryFinal = sender as XRTable;
            //            
            XRTableRow summaryFinalRow0 = summaryFinal.Rows[0];
            ((XRTableCell)summaryFinalRow0.Cells[0]).Text = "";

            if (this.numeroLinhasDataTable != 0) {
                string ativo = Convert.ToString(this.GetCurrentColumnValue("CdAtivoBolsa"));

                ((XRTableCell)summaryFinalRow0.Cells[0]).Text = Resources.SubReportAcao._Total1 + " " + ativo;
            }
        }

        private void TableTotalGroupDescricaoAtivoPrint(object sender, System.Drawing.Printing.PrintEventArgs e) {
            XRTable summaryFinal = sender as XRTable;
            //            
            XRTableRow summaryFinalRow0 = summaryFinal.Rows[0];
            ((XRTableCell)summaryFinalRow0.Cells[0]).Text = "";

            if (this.numeroLinhasDataTable != 0) {
                string ativo = Convert.ToString(this.GetCurrentColumnValue("DescricaoAtivo"));

                ((XRTableCell)summaryFinalRow0.Cells[0]).Text = Resources.SubReportAcao._Total1 + " " + ativo;
            }
        }
    }
}