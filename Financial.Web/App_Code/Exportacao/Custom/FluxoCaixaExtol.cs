﻿using System;
using System.IO;
using Bytescout.Spreadsheet;
using Bytescout.Spreadsheet.Constants;
using Financial.Investidor;
using Financial.Investidor.Enums;
using System.Collections.Generic;
using System.Drawing;
using Financial.Common;
using Financial.ContaCorrente;
using Financial.Util;
using Financial.Bolsa;
using System.Globalization;
using Financial.Common.Enums;
using Financial.RendaFixa;
using Financial.RendaFixa.Enums;
using Financial.ContaCorrente.Enums;

namespace Financial.Export
{

    /// <summary>
    /// Summary description for FluxoCaixaExtol
    /// </summary>
    public class FluxoCaixaExtol
    {
        private const string MODELO_PLANILHA = "FluxoCaixaExtol.csv";

        private DateTime dataReferencia;

        private LiquidacaoCollection liquidacaoCollection = new LiquidacaoCollection();

        /* Construtor */
        public FluxoCaixaExtol(DateTime dataReferencia, int idCliente)
        {
            this.dataReferencia = Calendario.AdicionaDiaUtil(dataReferencia, 1);

            LiquidacaoQuery liquidacaoQuery = new LiquidacaoQuery("L");
            ClienteQuery clienteQuery = new ClienteQuery("C");

            liquidacaoQuery.Select(liquidacaoQuery.Valor,
                                   liquidacaoQuery.DataVencimento,
                                   liquidacaoQuery.Descricao,
                                   liquidacaoQuery.IdCliente,
                                   clienteQuery.DataDia,
                                   clienteQuery.Apelido);
            liquidacaoQuery.InnerJoin(clienteQuery).On(clienteQuery.IdCliente == liquidacaoQuery.IdCliente);
            liquidacaoQuery.Where(liquidacaoQuery.DataVencimento.GreaterThanOrEqual(this.dataReferencia),
                                  liquidacaoQuery.DataLancamento.LessThanOrEqual(this.dataReferencia),
                                  liquidacaoQuery.Valor.NotEqual(0),
                                  liquidacaoQuery.IdCliente.Equal(idCliente),
                                  liquidacaoQuery.Situacao.NotEqual((byte)SituacaoLancamentoLiquidacao.Compensacao));
            liquidacaoQuery.OrderBy(liquidacaoQuery.IdCliente.Ascending,
                                    liquidacaoQuery.Descricao.Ascending,
                                    liquidacaoQuery.Valor.Ascending,
                                    liquidacaoQuery.DataVencimento.Ascending);

            this.liquidacaoCollection.Load(liquidacaoQuery);

            Cliente cliente = new Cliente();
            cliente.LoadByPrimaryKey(idCliente);

            PosicaoRendaFixa posicaoRendaFixa = new PosicaoRendaFixa();
            posicaoRendaFixa.Query.Select(posicaoRendaFixa.Query.ValorVolta.Sum(),
                                          posicaoRendaFixa.Query.DataVolta.Min());
            posicaoRendaFixa.Query.Where(posicaoRendaFixa.Query.IdCliente.Equal(idCliente),
                                         posicaoRendaFixa.Query.DataVolta.IsNotNull(),
                                         posicaoRendaFixa.Query.TipoOperacao.Equal((byte)TipoOperacaoTitulo.CompraRevenda),
                                         posicaoRendaFixa.Query.DataVolta.GreaterThanOrEqual(this.dataReferencia));
            posicaoRendaFixa.Query.Load();

            if (posicaoRendaFixa.ValorVolta.HasValue)
            {
                Liquidacao liquidacaoNovo = new Liquidacao();

                liquidacaoNovo.Valor = posicaoRendaFixa.ValorVolta.Value;
                liquidacaoNovo.DataVencimento = posicaoRendaFixa.DataVolta.Value;
                liquidacaoNovo.Descricao = "Operação Compromissada";
                liquidacaoNovo.IdCliente = idCliente;

                this.liquidacaoCollection.AttachEntity(liquidacaoNovo);

                this.liquidacaoCollection[liquidacaoCollection.Count - 1].SetColumn("DataDia", cliente.DataDia.Value);
                this.liquidacaoCollection[liquidacaoCollection.Count - 1].SetColumn("Apelido", cliente.str.Apelido);
            }


        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="ms">Saida: Memory Stream do arquivo excel</param>
        /// <param name="nomeArquivo">Saida: Nome do Arquivo Excel</param>
        /// <exception cref="Exception">Se não dados para gerar</exception>
        public bool ExportaExcel(string path, out string nomeArquivo)
        {
            nomeArquivo = "";

            if (!this.liquidacaoCollection.HasData)
            {
                return false;
            }

            int idCliente = this.liquidacaoCollection[0].IdCliente.Value;

            // out            
            string codigoClienteArquivo = idCliente.ToString();
            ClienteBolsa clienteBolsa = new ClienteBolsa();
            if (clienteBolsa.LoadByPrimaryKey(idCliente))
            {
                if (!String.IsNullOrEmpty(clienteBolsa.CodigoSinacor))
                {
                    codigoClienteArquivo = clienteBolsa.CodigoSinacor;
                }
            }

            nomeArquivo = codigoClienteArquivo + "_" + this.dataReferencia.ToString("yyyyMMdd") + ".csv";
            //           
            Spreadsheet document = new Bytescout.Spreadsheet.Spreadsheet();

            string pathModelos = AppDomain.CurrentDomain.BaseDirectory + "\\Relatorios\\Modelos";
            string pathPlanilhaModelo = String.Format("{0}\\{1}", pathModelos, MODELO_PLANILHA);

            document.LoadFromFile(pathPlanilhaModelo);

            Worksheet worksheet = document.Workbook.Worksheets[0];

            #region Ref Planilha - worksheet
            worksheet.ViewOptions.ShowZeroValues = true;
            this.setPlan(worksheet, codigoClienteArquivo);
            #endregion

            /*-----------------------------------------------------*/

            #region Esconde Planilha Demo do componente
            const string BYTESCOUT_WORKSHEET_NAME = "Bytescout Spreadsheet SDK";
            worksheet = document.Workbook.Worksheets.ByName(BYTESCOUT_WORKSHEET_NAME);
            if (worksheet == null)
            {
                worksheet = document.Workbook.Worksheets.Add(BYTESCOUT_WORKSHEET_NAME);
            }
            worksheet.Visible = Bytescout.Spreadsheet.Constants.SHEETVISIBILITY.Hidden;
            #endregion

            //Bug no exporta pra stream csv - precisamos salvar pra disco e depois jogar no memory stream
            #region Salva CSV no disco e carrega no memorystream
            document.SaveAsCSV(path + nomeArquivo, ",");

            #endregion

            document.Close();

            return true;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="worksheet">Planilha 1</param>
        private void setPlan(Worksheet worksheet, string codigoClienteArquivo)
        {
            DateTime dataD0 = this.dataReferencia;
            DateTime dataD1 = Calendario.AdicionaDiaUtil(dataD0, 1);
            DateTime dataD2 = Calendario.AdicionaDiaUtil(dataD1, 1);
            DateTime dataD3 = Calendario.AdicionaDiaUtil(dataD2, 1);
            DateTime dataD4 = Calendario.AdicionaDiaUtil(dataD3, 1);
            DateTime dataD5 = Calendario.AdicionaDiaUtil(dataD4, 1);
            DateTime dataD6 = Calendario.AdicionaDiaUtil(dataD5, 1);
            DateTime dataD7 = Calendario.AdicionaDiaUtil(dataD6, 1);
            DateTime dataD8 = Calendario.AdicionaDiaUtil(dataD7, 1);
            DateTime dataD9 = Calendario.AdicionaDiaUtil(dataD8, 1);

            NumberFormatInfo provider = new NumberFormatInfo();
            provider.NumberDecimalSeparator = ".";
            provider.NumberDecimalDigits = 2;
            provider.NumberGroupSeparator = "";

            int linha = 1;
            int j = 0;
            while (j < this.liquidacaoCollection.Count)
            {
                int idCliente = liquidacaoCollection[j].IdCliente.Value;
                string apelido = Convert.ToString(liquidacaoCollection[j].GetColumn(ClienteMetadata.ColumnNames.Apelido));
                DateTime dataDia = Convert.ToDateTime(liquidacaoCollection[j].GetColumn(ClienteMetadata.ColumnNames.DataDia));
                DateTime dataVencimento = liquidacaoCollection[j].DataVencimento.Value;
                decimal valor = liquidacaoCollection[j].Valor.Value;
                string descricao = liquidacaoCollection[j].Descricao.Replace(".", "").Replace(",", ".");
                string ativoPassivo = valor > 0 ? "A" : "P";

                string chave = idCliente.ToString() + descricao + ativoPassivo;
                string chaveAnterior = chave;

                decimal valorD0 = 0;
                decimal valorD1 = 0;
                decimal valorD2 = 0;
                decimal valorD3 = 0;
                decimal valorD4 = 0;
                decimal valorD5 = 0;
                decimal valorD6 = 0;
                decimal valorD7 = 0;
                decimal valorD8 = 0;
                decimal valorD9 = 0;
                decimal valorDN = 0;

                int idClienteAnterior = idCliente;
                string descricaoAnterior = descricao;
                DateTime dataVencimentoAnterior = dataVencimento;
                string ativoPassivoAnterior = ativoPassivo;

                while (chave == chaveAnterior && j < this.liquidacaoCollection.Count)
                {
                    int numeroDias = Calendario.NumeroDias(dataD0, dataVencimento, LocalFeriadoFixo.Brasil, TipoFeriado.Brasil);

                    #region Verifica se joga em D0, D1...DN
                    if (numeroDias == 0)
                    {
                        valorD0 += valor;
                    }
                    else if (numeroDias == 1)
                    {
                        valorD1 += valor;
                    }
                    else if (numeroDias == 2)
                    {
                        valorD2 += valor;
                    }
                    else if (numeroDias == 3)
                    {
                        valorD3 += valor;
                    }
                    else if (numeroDias == 4)
                    {
                        valorD4 += valor;
                    }
                    else if (numeroDias == 5)
                    {
                        valorD5 += valor;
                    }
                    else if (numeroDias == 6)
                    {
                        valorD6 += valor;
                    }
                    else if (numeroDias == 7)
                    {
                        valorD7 += valor;
                    }
                    else if (numeroDias == 81)
                    {
                        valorD8 += valor;
                    }
                    else if (numeroDias == 9)
                    {
                        valorD9 += valor;
                    }
                    else
                    {
                        valorDN += valor;
                    }
                    #endregion

                    j++;

                    if (j < this.liquidacaoCollection.Count)
                    {
                        dataVencimento = liquidacaoCollection[j].DataVencimento.Value;
                        valor = liquidacaoCollection[j].Valor.Value;

                        idCliente = liquidacaoCollection[j].IdCliente.Value;
                        descricao = liquidacaoCollection[j].Descricao.Replace(".", "").Replace(",", ".");
                        ativoPassivo = valor > 0 ? "A" : "P";

                        chave = idCliente.ToString() + descricao + ativoPassivo;
                    }

                }

                //
                worksheet.Rows[linha][0].ValueAsString = ativoPassivoAnterior; // coluna A
                worksheet.Rows[linha][1].ValueAsString = apelido; // coluna B
                worksheet.Rows[linha][2].ValueAsString = codigoClienteArquivo; // coluna C

                #region D0/Valor
                worksheet.Rows[linha][3].ValueAsDateTime = dataD0; // coluna D (data D0)                

                // coluna E (valor D0)
                worksheet.Rows[linha][4].ValueAsString = valorD0.ToString("N2", provider);
                #endregion

                #region D1/Valor
                worksheet.Rows[linha][5].ValueAsDateTime = dataD1; // coluna F (data D1)                

                // coluna G (valor D1) 
                worksheet.Rows[linha][6].ValueAsString = valorD1.ToString("N2", provider);
                #endregion

                #region D2/Valor
                worksheet.Rows[linha][7].ValueAsDateTime = dataD2; // coluna H (data D2)                

                // coluna I (valor D2)
                worksheet.Rows[linha][8].ValueAsString = valorD2.ToString("N2", provider);
                #endregion

                #region D3/Valor
                worksheet.Rows[linha][9].ValueAsDateTime = dataD3; // coluna J (data D3)                

                // coluna K (valor D3) 
                worksheet.Rows[linha][10].ValueAsString = valorD3.ToString("N2", provider);

                #endregion

                #region D4/Valor
                worksheet.Rows[linha][11].ValueAsDateTime = dataD4; // coluna L (data D4)                

                // coluna M (valor D4)               
                worksheet.Rows[linha][12].ValueAsString = valorD4.ToString("N2", provider);
                #endregion

                #region D5/Valor
                worksheet.Rows[linha][13].ValueAsDateTime = dataD5; // coluna N (data D5)                

                // coluna O (valor D5)   
                worksheet.Rows[linha][14].ValueAsString = valorD5.ToString("N2", provider);
                #endregion

                #region D6/Valor
                worksheet.Rows[linha][15].ValueAsDateTime = dataD6; // coluna P (data D6)                

                // coluna Q (valor D6) 
                worksheet.Rows[linha][16].ValueAsString = valorD6.ToString("N2", provider);

                #endregion

                #region D7/Valor
                worksheet.Rows[linha][17].ValueAsDateTime = dataD7; // coluna R (data D7)                

                // coluna S (valor D7) 
                worksheet.Rows[linha][18].ValueAsString = valorD7.ToString("N2", provider);

                #endregion

                #region D8/Valor
                worksheet.Rows[linha][19].ValueAsDateTime = dataD8; // coluna T (data D8)                

                // coluna U (valor D8)                    
                worksheet.Rows[linha][20].ValueAsString = valorD8.ToString("N2", provider);
                #endregion

                #region D9/Valor
                worksheet.Rows[linha][21].ValueAsDateTime = dataD9; // coluna V (data D9)                

                // coluna W (valor D9) 
                worksheet.Rows[linha][22].ValueAsString = valorD9.ToString("N2", provider);

                #endregion

                #region D10/Valor
                // coluna W (valor DOutros)                                
                worksheet.Rows[linha][23].ValueAsString = valorDN.ToString("N2", provider);
                worksheet.Rows[linha][24].ValueAsDateTime = dataReferencia; // coluna X (data DOutros)                
                #endregion

                worksheet.Rows[linha][25].ValueAsString = descricaoAnterior.Replace("\"", ""); // coluna Z
                worksheet.Rows[linha][26].ValueAsString = "REAL"; // coluna AA
                worksheet.Rows[linha][27].ValueAsString = "0"; // coluna AB

                //
                linha++;
            }
        }
    }
}