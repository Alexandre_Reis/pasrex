﻿using System;
using System.IO;
using Bytescout.Spreadsheet;
using Bytescout.Spreadsheet.Constants;
using Financial.Bolsa;
using Financial.Investidor;
using Financial.Investidor.Enums;
using System.Collections.Generic;
using System.Drawing;
using Financial.Common;
using Financial.Bolsa.Enums;
using Financial.Bolsa.Exceptions;
using Financial.CRM;
using Financial.CRM.Enums;

namespace Financial.Export {

    /// <summary>
    /// Summary description for ControleParticipacaoSocietariaMasa
    /// </summary>
    public class ControleParticipacaoSocietariaMasa {

        private enum TipoPesquisa {
            PosicaoBolsa = 0,
            PosicaoBolsaHistorico = 2
        }
        TipoPesquisa tipoPesquisa = TipoPesquisa.PosicaoBolsa;

        private const string MODELO_PLANILHA = "ControleParticipacaoSocietariaMasa.xls";
        
        private DateTime dataInicio;
        private DateTime dataFim;
        private int idCliente;
        private string codigoAtivo;
        //
        private Cliente cliente = new Cliente();

        private OperacaoBolsaCollection operacaoBolsaCollection = new OperacaoBolsaCollection();
        private PosicaoBolsa posicaoBolsa = new PosicaoBolsa();
        private PosicaoBolsaHistorico posicaoBolsaH = new PosicaoBolsaHistorico();        

        private decimal quantidadeSaldo;
        private decimal valorCusto;
        private decimal puCustoLiquido;
        
        /* Construtor */
        public ControleParticipacaoSocietariaMasa(int idCliente, string cdAtivo, DateTime dataInicio, DateTime dataFim) {
            bool temDados = false;

            this.idCliente = idCliente;
            this.codigoAtivo = cdAtivo;
            this.dataInicio = dataInicio;
            this.dataFim = dataFim;
            //
            this.cliente.LoadByPrimaryKey(this.idCliente);
            //
            this.tipoPesquisa = this.cliente.IsClienteNaData(this.idCliente, this.dataInicio)
                                               ? TipoPesquisa.PosicaoBolsa
                                               : TipoPesquisa.PosicaoBolsaHistorico;

            this.posicaoBolsaH.Query.Select(this.posicaoBolsaH.Query.Quantidade.Sum(),
                                            this.posicaoBolsaH.Query.ValorCustoLiquido.Sum(),
                                            this.posicaoBolsaH.Query.PUCustoLiquido.Avg());
            this.posicaoBolsaH.Query.Where(this.posicaoBolsaH.Query.IdCliente == this.idCliente,
                                           this.posicaoBolsaH.Query.CdAtivoBolsa == this.codigoAtivo,
                                           this.posicaoBolsaH.Query.DataHistorico == this.dataInicio);

            if (this.posicaoBolsaH.Query.Load())
            {
                this.quantidadeSaldo = this.posicaoBolsaH.Quantidade.HasValue ? this.posicaoBolsaH.Quantidade.Value : 0;
                this.valorCusto = this.posicaoBolsaH.ValorCustoLiquido.HasValue ? this.posicaoBolsaH.ValorCustoLiquido.Value : 0;
                this.puCustoLiquido = this.posicaoBolsaH.PUCustoLiquido.HasValue ? this.posicaoBolsaH.PUCustoLiquido.Value : 0;

                temDados = true;
            }
            
            #region OperacaoBolsa
            OperacaoBolsaQuery operacaoBolsaQuery = new OperacaoBolsaQuery("O");
            AtivoBolsaQuery ativoBolsaQuery = new AtivoBolsaQuery("A");

            operacaoBolsaQuery.Select(operacaoBolsaQuery.Data, 
                                      operacaoBolsaQuery.IdOperacao,
                                      operacaoBolsaQuery.TipoOperacao,
                                      operacaoBolsaQuery.Origem,
                                      operacaoBolsaQuery.Observacao,
                                      operacaoBolsaQuery.ValorLiquido,
                                      operacaoBolsaQuery.Valor,
                                      operacaoBolsaQuery.Quantidade,
                                      operacaoBolsaQuery.ResultadoRealizado,
                                      operacaoBolsaQuery.IdAgenteCorretora,
                                      operacaoBolsaQuery.DataLiquidacao,
                                      ativoBolsaQuery.CdAtivoBolsa, 
                                      ativoBolsaQuery.IdEmissor,
                                      ativoBolsaQuery.Descricao,
                                      ativoBolsaQuery.Especificacao);
            //
            operacaoBolsaQuery.InnerJoin(ativoBolsaQuery).On(operacaoBolsaQuery.CdAtivoBolsa == ativoBolsaQuery.CdAtivoBolsa);
            //
            operacaoBolsaQuery.Where(operacaoBolsaQuery.CdAtivoBolsa == this.codigoAtivo,
                                     operacaoBolsaQuery.IdCliente == this.idCliente,
                                     operacaoBolsaQuery.Data.GreaterThan(this.dataInicio),
                                     operacaoBolsaQuery.Data.LessThanOrEqual(this.dataFim),
                                     operacaoBolsaQuery.TipoOperacao.In( TipoOperacaoBolsa.Compra,
                                                                         TipoOperacaoBolsa.Venda,
                                                                         TipoOperacaoBolsa.Deposito,
                                                                         TipoOperacaoBolsa.Retirada)
                                    );
            //
            operacaoBolsaQuery.OrderBy(operacaoBolsaQuery.Data.Ascending, operacaoBolsaQuery.IdOperacao.Ascending);
            //            
            this.operacaoBolsaCollection.Load(operacaoBolsaQuery);            
           
            //#region Evolucao Capital Social
            EvolucaoCapitalSocialQuery evolucaoCapitalSocialQuery = new EvolucaoCapitalSocialQuery("E");
            ativoBolsaQuery = new AtivoBolsaQuery("A");

            evolucaoCapitalSocialQuery.Select(evolucaoCapitalSocialQuery, ativoBolsaQuery);
            evolucaoCapitalSocialQuery.InnerJoin(ativoBolsaQuery).On(ativoBolsaQuery.CdAtivoBolsa == evolucaoCapitalSocialQuery.CdAtivoBolsa);
            evolucaoCapitalSocialQuery.Where(evolucaoCapitalSocialQuery.CdAtivoBolsa.Equal(this.codigoAtivo),
                                             evolucaoCapitalSocialQuery.Data.GreaterThan(this.dataInicio),
                                             evolucaoCapitalSocialQuery.Data.LessThanOrEqual(this.dataFim));
            evolucaoCapitalSocialQuery.OrderBy(evolucaoCapitalSocialQuery.Data.Ascending);

            EvolucaoCapitalSocialCollection evolucaoCapitalSocialCollection = new EvolucaoCapitalSocialCollection();
            evolucaoCapitalSocialCollection.Load(evolucaoCapitalSocialQuery);
            #endregion

            #region União EvolucaoCapitalSocial com OperacaoBolsa
            int max = 0;
            if (evolucaoCapitalSocialCollection.Count > 0)
            {
                OperacaoBolsa operacaoBolsaMax = new OperacaoBolsa();
                operacaoBolsaMax.Query.Select(operacaoBolsaMax.Query.IdOperacao.Max());
                operacaoBolsaMax.Query.Load();
                if (operacaoBolsaMax.IdOperacao.HasValue)
                {
                    max = operacaoBolsaMax.IdOperacao.Value;
                }
            }

            for (int i = 0; i < evolucaoCapitalSocialCollection.Count; i++) 
            {
                OperacaoBolsaCollection operacaoBolsaCollectionCheck = new OperacaoBolsaCollection();
                operacaoBolsaCollectionCheck.Query.Where(operacaoBolsaCollectionCheck.Query.Data.Equal(evolucaoCapitalSocialCollection[i].Data.Value),
                                                         operacaoBolsaCollectionCheck.Query.CdAtivoBolsa.Equal(evolucaoCapitalSocialCollection[i].CdAtivoBolsa),
                                                         operacaoBolsaCollectionCheck.Query.IdCliente.Equal(this.idCliente));
                operacaoBolsaCollectionCheck.Query.Load();

                if (operacaoBolsaCollectionCheck.Count == 0 &&
                    (evolucaoCapitalSocialCollection[i].Tipo.Value == 1 ||
                     evolucaoCapitalSocialCollection[i].Tipo.Value == 2))
                {
                    string observacao = evolucaoCapitalSocialCollection[i].Tipo.Value == 1 ? "Subscrição não aderida" : "Cancelamento Ações em Tesouraria";                    

                    OperacaoBolsa oper = this.operacaoBolsaCollection.AddNew();

                    oper.AddColumn("IdEmissor", typeof(System.Int32));
                    oper.AddColumn("Descricao", typeof(System.String));
                    oper.AddColumn("Especificacao", typeof(System.String));
                    oper.AddColumn("IdOperacao", typeof(System.Int32));
                    //
                    oper.Data = evolucaoCapitalSocialCollection[i].Data;
                    oper.TipoOperacao = "";
                    oper.Origem = (byte)255;
                    oper.Observacao = observacao;
                    oper.ValorLiquido = 0;
                    oper.Valor = 0;
                    oper.Quantidade = 0;
                    oper.ResultadoRealizado = 0;
                    //              
                    oper.CdAtivoBolsa = evolucaoCapitalSocialCollection[i].CdAtivoBolsa;
                    oper.SetColumn("IdEmissor", evolucaoCapitalSocialCollection[i].GetColumn("IdEmissor"));
                    oper.SetColumn("Descricao", evolucaoCapitalSocialCollection[i].GetColumn("Descricao"));
                    oper.SetColumn("Especificacao", evolucaoCapitalSocialCollection[i].GetColumn("Especificacao"));
                    oper.SetColumn("IdOperacao", max);
                }

                max++;
            }
            #endregion

            this.operacaoBolsaCollection.Sort = OperacaoBolsaMetadata.ColumnNames.Data + " ASC, " + OperacaoBolsaMetadata.ColumnNames.IdOperacao + " ASC";

            if (this.operacaoBolsaCollection.Count == 0 && !temDados) 
            {
                throw new Exception("Não há Dados para Gerar.");
            }            
            
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="ms">Saida: Memory Stream do arquivo excel</param>
        /// <param name="nomeArquivo">Saida: Nome do Arquivo Excel</param>
        /// <exception cref="Exception">Se não dados para gerar</exception>
        public void ExportaExcel(out MemoryStream ms, out string nomeArquivo) 
        {
            // out
            ms = new MemoryStream();
            nomeArquivo = "ParticipacaoSocietaria.xls";
            //           
            Spreadsheet document = new Bytescout.Spreadsheet.Spreadsheet();

            string pathModelos = AppDomain.CurrentDomain.BaseDirectory + "\\Relatorios\\Modelos";
            string pathPlanilhaModelo = String.Format("{0}\\{1}", pathModelos, MODELO_PLANILHA);
            
            document.LoadFromFile(pathPlanilhaModelo);

            Worksheet worksheet = document.Workbook.Worksheets[0];

            #region Ref Planilha - worksheet
            worksheet.ViewOptions.ShowZeroValues = true;
            this.setPlan(worksheet);
            #endregion

            /*-----------------------------------------------------*/

            #region Esconde Planilha Demo do componente
            const string BYTESCOUT_WORKSHEET_NAME = "Bytescout Spreadsheet SDK";
            worksheet = document.Workbook.Worksheets.ByName(BYTESCOUT_WORKSHEET_NAME);
            if (worksheet == null) {
                worksheet = document.Workbook.Worksheets.Add(BYTESCOUT_WORKSHEET_NAME);
            }
            worksheet.Visible = Bytescout.Spreadsheet.Constants.SHEETVISIBILITY.Hidden;
            #endregion

            //
            document.SaveToStreamXLS(ms);
            //
            document.Close();            
        }
       
        /// <summary>
        /// 
        /// </summary>
        /// <param name="worksheet">Planilha 1</param>
        private void setPlan(Worksheet worksheet) {
           

            Pessoa pessoa = new Pessoa();
            Cliente cliente = new Cliente();
            cliente.LoadByPrimaryKey(this.idCliente);
            pessoa.LoadByPrimaryKey(cliente.IdPessoa.Value);

            #region Header
            #region Data Emissão
            worksheet.Cell("C10").ValueAsDateTime = DateTime.Now;
            worksheet.Cell("C10").AlignmentHorizontal = AlignmentHorizontal.Left;
            #endregion

            #region Nome Cliente
            
            worksheet.Cell("C3").ValueAsString = this.cliente.Nome.Trim();
            worksheet.Cell("C3").AlignmentHorizontal = AlignmentHorizontal.Left;
            #endregion

            #region Periodo
            worksheet.Cell("H4").ValueAsString = this.dataInicio.ToShortDateString() + " até " + this.dataFim.ToShortDateString();
            worksheet.Cell("H4").AlignmentHorizontal = AlignmentHorizontal.Left;
            #endregion

            #region Ativo
            AtivoBolsa a = new AtivoBolsa();
            
            AtivoBolsaQuery at = new AtivoBolsaQuery("A");
            EspecificacaoBolsaQuery esp = new EspecificacaoBolsaQuery("Esp");
            //
            at.Select(at, esp.Especificacao.As("Especificacao1"));
            at.InnerJoin(esp).On(at.Especificacao == esp.Especificacao);
            at.Where(at.CdAtivoBolsa == this.codigoAtivo);

            a.Load(at);

            //
            worksheet.Cell("C4").ValueAsString = a.Descricao.Trim();
            worksheet.Cell("C4").AlignmentHorizontal = AlignmentHorizontal.Left;
            //
            worksheet.Cell("D4").ValueAsString = a.CdAtivoBolsa.Trim();
            worksheet.Cell("D4").AlignmentHorizontal = AlignmentHorizontal.Left;
            //
            worksheet.Cell("F4").ValueAsString = a.Especificacao.Trim().ToUpper();
            worksheet.Cell("F4").AlignmentHorizontal = AlignmentHorizontal.Left;

            #endregion
            #endregion

            #region Primeira Linha
            string esp1 = Convert.ToString(a.GetColumn("Especificacao1"));
            EspecificacaoBolsa especificacaoBolsa = new EspecificacaoBolsa();
            especificacaoBolsa.LoadByPrimaryKey(esp1);

            decimal quantidadeCapitalSocial = new EvolucaoCapitalSocial().RetornaQuantidade(a.IdEmissor.Value, this.dataInicio, null);
            decimal quantidadeCapitalVotante = new EvolucaoCapitalSocial().RetornaQuantidade(a.IdEmissor.Value, this.dataInicio, TipoEspecificacaoBolsa.Ordinaria);
            //
            decimal? percentualCapitalSocial = null;
            try 
            {
                percentualCapitalSocial = this.quantidadeSaldo / quantidadeCapitalSocial * 100M;            
            }
            catch (Exception) {}

            decimal? percentualCapitalVotante = null;
            try 
            {
                if (especificacaoBolsa.Tipo.HasValue && especificacaoBolsa.Tipo.Value == (byte)TipoEspecificacaoBolsa.Ordinaria)
                {
                    percentualCapitalVotante = this.quantidadeSaldo / quantidadeCapitalVotante * 100M;
                }
            }
            catch (Exception) {}

            #region B
            worksheet.Cell("B7").ValueAsDateTime = this.dataInicio;
            #endregion

            #region C
            worksheet.Cell("C7").ValueAsString = "Saldo Inicial";
            #endregion

            #region D
            worksheet.Cell("D7").ValueAsDouble = Convert.ToDouble(this.quantidadeSaldo);
            worksheet.Cell("D7").NumberFormatString = "#,##,#0";
            #endregion

            #region E
            worksheet.Cell("E7").ValueAsDouble = Convert.ToDouble(this.quantidadeSaldo);
            worksheet.Cell("E7").NumberFormatString = "#,##,#0";
            #endregion

            #region F
            if (percentualCapitalVotante.HasValue) {
                worksheet.Cell("F7").ValueAsDouble = Convert.ToDouble(percentualCapitalVotante);
                worksheet.Cell("F7").NumberFormatString = "#,##,#0.000000";
            }
            else {
                worksheet.Cell("F7").AlignmentHorizontal = AlignmentHorizontal.Centered;
                worksheet.Cell("F7").ValueAsString = " - ";
            }
            #endregion

            #region G
            if (percentualCapitalSocial.HasValue) {
                worksheet.Cell("G7").ValueAsDouble = Convert.ToDouble(percentualCapitalSocial);
                worksheet.Cell("G7").NumberFormatString = "#,##,#0.000000";                
            }
            else {
                worksheet.Cell("G7").AlignmentHorizontal = AlignmentHorizontal.Centered;
                worksheet.Cell("G7").ValueAsString = " - ";
            }
            #endregion

            #region H
            worksheet.Cell("H7").ValueAsDouble = 0;
            worksheet.Cell("H7").NumberFormatString = "#,##,#0.00";            
            #endregion

            #region I
            if (pessoa.Tipo.Value == (byte)TipoPessoa.Fisica)
            {
                worksheet.Cell("I7").ValueAsDouble = Convert.ToDouble(this.valorCusto);
            }
            else
            {
                worksheet.Cell("I7").ValueAsDouble = 0;
            }
            worksheet.Cell("I7").NumberFormatString = "#,##,#0.00";      
            #endregion

            #region J
            if (pessoa.Tipo.Value == (byte)TipoPessoa.Fisica)
            {
                worksheet.Cell("J7").ValueAsDouble = Convert.ToDouble(this.puCustoLiquido);
            }
            else
            {
                worksheet.Cell("J7").ValueAsDouble = 0;
            }
            worksheet.Cell("J7").NumberFormatString = "#,##,#0.00";            
            #endregion

            #region K
            if (pessoa.Tipo.Value == (byte)TipoPessoa.Fisica)
            {
                worksheet.Cell("K7").ValueAsDouble = Convert.ToDouble(this.valorCusto);
            }
            else
            {
                worksheet.Cell("K7").ValueAsDouble = 0;
            }
            worksheet.Cell("K7").NumberFormatString = "#,##,#0.00";            
            #endregion

            #region L
            if (pessoa.Tipo.Value == (byte)TipoPessoa.Fisica)
            {
                worksheet.Cell("L7").ValueAsDouble = Convert.ToDouble(this.puCustoLiquido);
            }
            else
            {
                worksheet.Cell("L7").ValueAsDouble = 0;
            }
            worksheet.Cell("L7").NumberFormatString = "#,##,#0.00";
            #endregion
                    
            #endregion

            #region for da Planilha

            int linha = 7; // Adiciona Linhas
            for (int i = 0; i < this.operacaoBolsaCollection.Count; i++) {
                //Add new row
                worksheet.Rows.Insert(linha, 1);
                
                worksheet.Rows[linha].Height = 22;
                
                worksheet.Rows[linha].Font = new Font("Calibri", 11, FontStyle.Regular);
                
                #region Bordas Esquerda
                //coluna B com borda esquerda
                worksheet.Rows[linha][1].LeftBorderStyle = LineStyle.Thin;
                //coluna C com borda esquerda
                worksheet.Rows[linha][2].LeftBorderStyle = LineStyle.Thin;
                //coluna D com borda esquerda
                worksheet.Rows[linha][3].LeftBorderStyle = LineStyle.Thin;
                //coluna E com borda esquerda
                worksheet.Rows[linha][4].LeftBorderStyle = LineStyle.Thin;
                //coluna F com borda esquerda
                worksheet.Rows[linha][5].LeftBorderStyle = LineStyle.Thin;
                //coluna G com borda esquerda
                worksheet.Rows[linha][6].LeftBorderStyle = LineStyle.Thin;
                //coluna H com borda esquerda
                worksheet.Rows[linha][7].LeftBorderStyle = LineStyle.Thin;
                //coluna I com borda esquerda
                worksheet.Rows[linha][8].LeftBorderStyle = LineStyle.Thin;
                //coluna J com borda esquerda
                worksheet.Rows[linha][9].LeftBorderStyle = LineStyle.Thin;
                //coluna K com borda esquerda
                worksheet.Rows[linha][10].LeftBorderStyle = LineStyle.Thin;
                //coluna L com borda esquerda
                worksheet.Rows[linha][11].LeftBorderStyle = LineStyle.Thin;

                //coluna L com borda direita
                worksheet.Rows[linha][11].RightBorderStyle = LineStyle.Thin;
                                
                #endregion

                #region Bordas Cima
                //coluna B com borda Cima
                worksheet.Rows[linha][1].TopBorderStyle = LineStyle.Thin;
                //coluna C com borda Cima
                worksheet.Rows[linha][2].TopBorderStyle = LineStyle.Thin;
                //coluna D com borda Cima
                worksheet.Rows[linha][3].TopBorderStyle = LineStyle.Thin;
                //coluna E com borda Cima
                worksheet.Rows[linha][4].TopBorderStyle = LineStyle.Thin;
                //coluna F com borda Cima
                worksheet.Rows[linha][5].TopBorderStyle = LineStyle.Thin;
                //coluna G com borda Cima
                worksheet.Rows[linha][6].TopBorderStyle = LineStyle.Thin;
                //coluna H com borda Cima
                worksheet.Rows[linha][7].TopBorderStyle = LineStyle.Thin;
                //coluna I com borda Cima
                worksheet.Rows[linha][8].TopBorderStyle = LineStyle.Thin;
                //coluna J com borda Cima
                worksheet.Rows[linha][9].TopBorderStyle = LineStyle.Thin;
                //coluna K com borda Cima
                worksheet.Rows[linha][10].TopBorderStyle = LineStyle.Thin;
                //coluna L com borda Cima
                worksheet.Rows[linha][11].TopBorderStyle = LineStyle.Thin;
                //
                //coluna B com borda Baixo
                worksheet.Rows[linha][1].BottomBorderStyle = LineStyle.Thin;                
                //coluna C com borda Baixo
                worksheet.Rows[linha][2].BottomBorderStyle = LineStyle.Thin;
                //coluna D com borda Baixo
                worksheet.Rows[linha][3].BottomBorderStyle = LineStyle.Thin;
                //coluna E com borda Baixo
                worksheet.Rows[linha][4].BottomBorderStyle = LineStyle.Thin;
                //coluna F com borda Baixo
                worksheet.Rows[linha][5].BottomBorderStyle = LineStyle.Thin;
                //coluna G com borda Baixo
                worksheet.Rows[linha][6].BottomBorderStyle = LineStyle.Thin;
                //coluna H com borda Baixo
                worksheet.Rows[linha][7].BottomBorderStyle = LineStyle.Thin;
                //coluna I com borda Baixo
                worksheet.Rows[linha][8].BottomBorderStyle = LineStyle.Thin;
                //coluna J com borda Baixo
                worksheet.Rows[linha][9].BottomBorderStyle = LineStyle.Thin;
                //coluna K com borda Baixo
                worksheet.Rows[linha][10].BottomBorderStyle = LineStyle.Thin;
                //coluna L com borda Baixo
                worksheet.Rows[linha][11].BottomBorderStyle = LineStyle.Thin;
                #endregion

                linha++;
            }

            linha = 7; // linha inicial dos dados
           
            FatorCotacaoBolsa fatorCotacaoBolsa = new FatorCotacaoBolsa();
            fatorCotacaoBolsa.BuscaFatorCotacaoBolsa(this.codigoAtivo, this.dataFim);                
            decimal fator = fatorCotacaoBolsa.Fator.Value;

            #region Dados

            decimal puCustoAnterior = puCustoLiquido;
            decimal valorCustoAnterior = valorCusto;
            string tipoOperacaoAnterior = "";
            decimal quantidadeSaldoAnterior = quantidadeSaldo;
            List<decimal> idOperacoesGrupadas = new List<decimal>();
            idOperacoesGrupadas.Add(0);
            DateTime dataOperacaoAnterior = this.dataInicio;
            for (int j = 0; j < this.operacaoBolsaCollection.Count; j++) 
            {
                int idAgente = operacaoBolsaCollection[j].IdAgenteCorretora.HasValue ? operacaoBolsaCollection[j].IdAgenteCorretora.Value : 0 ;
                string tipoOperacao = operacaoBolsaCollection[j].TipoOperacao;
                byte origem = operacaoBolsaCollection[j].Origem.Value;
                decimal quantidadeOperacao = operacaoBolsaCollection[j].Quantidade.Value;
                decimal valorOperacao = (tipoOperacao == TipoOperacaoBolsa.Deposito || tipoOperacao == TipoOperacaoBolsa.Retirada)?
                                operacaoBolsaCollection[j].Valor.Value : operacaoBolsaCollection[j].ValorLiquido.Value;
                DateTime dataOperacao = operacaoBolsaCollection[j].Data.Value;
                decimal resultadoOperacao = operacaoBolsaCollection[j].ResultadoRealizado.Value;
                string observacao = String.IsNullOrEmpty(operacaoBolsaCollection[j].Observacao) ? "" : " - " + operacaoBolsaCollection[j].Observacao;
                string cdAtivoBolsa = operacaoBolsaCollection[j].CdAtivoBolsa;
                
                
                decimal puCustoOperacao = 0;
                if (quantidadeOperacao != 0)
                {
                    puCustoOperacao = (valorOperacao * fator) / quantidadeOperacao;
                }

                quantidadeSaldo = 0; valorCusto = 0; puCustoLiquido = 0;
                #region Posição BolsaHistorico (quantidadeSaldo, valorCusto, puCustoLiquido)
                if (this.operacaoBolsaCollection.Count == 0 ||
                    j == (this.operacaoBolsaCollection.Count - 1) ||  
                    origem == OrigemOperacaoBolsa.ResultadoGrupamento ||
                    dataOperacao != operacaoBolsaCollection[j + 1].Data.Value)
                {
                    PosicaoBolsaHistorico posicaoBolsaH = new PosicaoBolsaHistorico();
                    posicaoBolsaH.Query.Select(posicaoBolsaH.Query.Quantidade.Sum(),
                                               posicaoBolsaH.Query.ValorCustoLiquido.Sum(),
                                               posicaoBolsaH.Query.PUCustoLiquido.Avg()
                                             );
                    posicaoBolsaH.Query.Where(posicaoBolsaH.Query.IdCliente == this.idCliente,
                                             posicaoBolsaH.Query.CdAtivoBolsa == this.codigoAtivo,
                                             posicaoBolsaH.Query.DataHistorico == dataOperacao
                                             );

                    if (posicaoBolsaH.Query.Load())
                    {
                        quantidadeSaldo = posicaoBolsaH.Quantidade.HasValue ? posicaoBolsaH.Quantidade.Value : 0;
                        valorCusto = posicaoBolsaH.ValorCustoLiquido.HasValue ? posicaoBolsaH.ValorCustoLiquido.Value : 0;
                        puCustoLiquido = posicaoBolsaH.PUCustoLiquido.HasValue ? posicaoBolsaH.PUCustoLiquido.Value : 0;
                    }
                }
                //else

                #region recompor quantidade DEPOSITADA para não perder fração.
                //inplit - split
                if (origem == OrigemOperacaoBolsa.ResultadoGrupamento && tipoOperacao == TipoOperacaoBolsa.Deposito)
                { 
                    OperacaoBolsaCollection operacaoBolsaCollectionRet = new OperacaoBolsaCollection();
                    OperacaoBolsaQuery operacaoBolsaQuery = new OperacaoBolsaQuery("O");
                    AtivoBolsaQuery ativoBolsaQuery = new AtivoBolsaQuery("A");

                    operacaoBolsaQuery.Select(operacaoBolsaQuery.Quantidade,
                                              operacaoBolsaQuery.IdOperacao);
                    //
                    operacaoBolsaQuery.InnerJoin(ativoBolsaQuery).On(operacaoBolsaQuery.CdAtivoBolsa == ativoBolsaQuery.CdAtivoBolsa);
                    //
                    operacaoBolsaQuery.Where(operacaoBolsaQuery.CdAtivoBolsa == cdAtivoBolsa,
                                             operacaoBolsaQuery.IdCliente == this.idCliente,
                                             operacaoBolsaQuery.Data.Equal(dataOperacao),
                                             operacaoBolsaQuery.TipoOperacao.Equal(TipoOperacaoBolsa.Retirada),
                                             operacaoBolsaQuery.IdOperacao.NotIn(idOperacoesGrupadas),
                                             operacaoBolsaQuery.Origem.Equal((int)OrigemOperacaoBolsa.ResultadoGrupamento));
                    //
                    operacaoBolsaQuery.OrderBy(operacaoBolsaQuery.Data.Ascending, operacaoBolsaQuery.IdOperacao.Ascending);
                    //            
                    operacaoBolsaCollectionRet.Load(operacaoBolsaQuery);

                    decimal quantidadeRetirada = 0;
                    if (operacaoBolsaCollectionRet.Count > 0)
                    {
                        quantidadeRetirada = operacaoBolsaCollectionRet[0].Quantidade.Value;
                    }

                    GrupamentoBolsaCollection  grupamentoBolsaCollection = new GrupamentoBolsaCollection();

                    GrupamentoBolsaQuery grupamentoBolsaQuery = new GrupamentoBolsaQuery("G");                   
                    grupamentoBolsaQuery.Select(grupamentoBolsaQuery.FatorPU,
                                                grupamentoBolsaQuery.TipoGrupamento);
                    grupamentoBolsaQuery.Where(grupamentoBolsaQuery.DataEx.Equal(dataOperacao),
                                               grupamentoBolsaQuery.CdAtivoBolsa.Equal(cdAtivoBolsa));
                    
                    grupamentoBolsaCollection.Load(grupamentoBolsaQuery);
                    if (grupamentoBolsaCollection.Count > 0)
                    {
                        decimal fatorPu = grupamentoBolsaCollection[0].FatorPU.Value;
                        if (grupamentoBolsaCollection[0].TipoGrupamento ==  (Byte)TipoGrupamentoBolsa.Grupamento)
                        {
                            quantidadeRetirada = quantidadeRetirada * fatorPu;
                        }
                        else
                        {
                            quantidadeRetirada = quantidadeRetirada / fatorPu;
                        }

                    }
                    quantidadeOperacao = (int)quantidadeRetirada;
                    if (operacaoBolsaCollectionRet.Count > 0)
                    {
                        idOperacoesGrupadas.Add(operacaoBolsaCollectionRet[0].IdOperacao.Value);
                        puCustoOperacao = 0;
                    }
                }
                // conversão
                if (origem == OrigemOperacaoBolsa.ResultadoConversao )
                {

                    if (tipoOperacao == TipoOperacaoBolsa.Deposito)
                    {
                        string cdAtivoBolsaOrigem = "";
                        decimal fatorPu = 0;
                        decimal fatorQuantidade = 0;
                        ConversaoBolsaCollection conversaoBolsaCollection = new ConversaoBolsaCollection();
                        ConversaoBolsaQuery conversaoBolsaQuery = new ConversaoBolsaQuery("C");
                        conversaoBolsaQuery.Select(conversaoBolsaQuery.CdAtivoBolsa,
                                                   conversaoBolsaQuery.FatorPU,
                                                   conversaoBolsaQuery.FatorQuantidade);
                        conversaoBolsaQuery.Where(conversaoBolsaQuery.DataEx.Equal(dataOperacao),
                                                   conversaoBolsaQuery.CdAtivoBolsaDestino.Equal(cdAtivoBolsa));

                        conversaoBolsaCollection.Load(conversaoBolsaQuery);
                        for (int k = 0; k < conversaoBolsaCollection.Count; k++)
                        {
                            cdAtivoBolsaOrigem = conversaoBolsaCollection[k].CdAtivoBolsa;
                            fatorPu = conversaoBolsaCollection[k].FatorPU.Value;
                            fatorQuantidade = conversaoBolsaCollection[k].FatorQuantidade.Value;

                            if (cdAtivoBolsa != cdAtivoBolsaOrigem)
                            {

                                //busca custo do papel retirado
                                PosicaoBolsaAbertura posicaoBolsaH = new PosicaoBolsaAbertura();
                                posicaoBolsaH.Query.Select(posicaoBolsaH.Query.Quantidade.Sum(),
                                                           posicaoBolsaH.Query.ValorCustoLiquido.Sum(),
                                                           posicaoBolsaH.Query.PUCustoLiquido.Avg()
                                                         );
                                posicaoBolsaH.Query.Where(posicaoBolsaH.Query.IdCliente == this.idCliente,
                                                         posicaoBolsaH.Query.CdAtivoBolsa == cdAtivoBolsaOrigem,
                                                         posicaoBolsaH.Query.DataHistorico == dataOperacao
                                                         );

                                if (posicaoBolsaH.Query.Load())
                                {
                                    quantidadeSaldo = posicaoBolsaH.Quantidade.HasValue ? posicaoBolsaH.Quantidade.Value * fatorQuantidade : 0;
                                    valorCusto = posicaoBolsaH.ValorCustoLiquido.HasValue ? posicaoBolsaH.ValorCustoLiquido.Value * fatorPu : 0;
                                    puCustoLiquido = posicaoBolsaH.PUCustoLiquido.HasValue ? posicaoBolsaH.PUCustoLiquido.Value * fatorPu : 0;
                                    puCustoAnterior = puCustoLiquido;

                                    if (quantidadeSaldo != 0 || valorCusto != 0 || puCustoLiquido != 0)
                                        setPlanDetalhe(ref worksheet, a.IdEmissor.Value, pessoa.Tipo.Value, cdAtivoBolsaOrigem, quantidadeOperacao, valorOperacao, puCustoOperacao,
                                                       ref quantidadeSaldoAnterior, ref valorCustoAnterior, ref puCustoAnterior, ref dataOperacaoAnterior, fator, origem,
                                                       especificacaoBolsa, ref linha, j);

                                }
                            }
                            else
                            {
                                //busca custo do papel depositado (fatorPu remanescente)
                                PosicaoBolsaHistorico posicaoBolsaHistorico = new PosicaoBolsaHistorico();
                                posicaoBolsaH.Query.Select(posicaoBolsaHistorico.Query.Quantidade.Sum(),
                                                           posicaoBolsaHistorico.Query.ValorCustoLiquido.Sum(),
                                                           posicaoBolsaHistorico.Query.PUCustoLiquido.Avg()
                                                         );
                                posicaoBolsaHistorico.Query.Where(posicaoBolsaHistorico.Query.IdCliente == this.idCliente,
                                                         posicaoBolsaHistorico.Query.CdAtivoBolsa == cdAtivoBolsaOrigem,
                                                         posicaoBolsaHistorico.Query.DataHistorico == dataOperacao
                                                         );

                                if (posicaoBolsaHistorico.Query.Load())
                                {
                                    quantidadeSaldo = posicaoBolsaHistorico.Quantidade.HasValue ? posicaoBolsaHistorico.Quantidade.Value : 0;
                                    valorCusto = posicaoBolsaHistorico.ValorCustoLiquido.HasValue ? posicaoBolsaHistorico.ValorCustoLiquido.Value : 0;
                                    puCustoLiquido = posicaoBolsaHistorico.PUCustoLiquido.HasValue ? posicaoBolsaHistorico.PUCustoLiquido.Value : 0;
                                    puCustoAnterior = puCustoLiquido;

                                    if (quantidadeSaldo != 0 || valorCusto != 0 || puCustoLiquido != 0)
                                        setPlanDetalhe(ref worksheet, a.IdEmissor.Value, pessoa.Tipo.Value, cdAtivoBolsaOrigem, quantidadeOperacao, valorOperacao, puCustoOperacao,
                                                       ref quantidadeSaldoAnterior, ref valorCustoAnterior, ref puCustoAnterior, ref dataOperacaoAnterior, fator, origem,
                                                       especificacaoBolsa, ref linha, j);

                                }

                            }

                        }

                    }
                    else
                    {
                        setPlanDetalhe(ref worksheet, a.IdEmissor.Value, pessoa.Tipo.Value, "", quantidadeOperacao, valorOperacao, puCustoOperacao,
                                       ref quantidadeSaldoAnterior, ref valorCustoAnterior, ref puCustoAnterior, ref dataOperacaoAnterior, fator, origem,
                                       especificacaoBolsa, ref linha, j);
                    }
                }
                #endregion

                #region antecipação de termo
                if (origem == OrigemOperacaoBolsa.LiquidacaoTermo && tipoOperacao == TipoOperacaoBolsa.Deposito )
                {
                    
                    DateTime dataVencimentoTermo = operacaoBolsaCollection[j].DataLiquidacao.Value;
                    string cdAtivoTermo = cdAtivoBolsa.Trim().ToUpper() + 'T' + dataVencimentoTermo.ToString("ddMMyyyy");

                    LiquidacaoTermoBolsa liquidacaoTermoBolsa = new LiquidacaoTermoBolsa();
                    liquidacaoTermoBolsa.Query.Select(liquidacaoTermoBolsa.Query.IdPosicao);
                    liquidacaoTermoBolsa.Query.Where(liquidacaoTermoBolsa.Query.IdCliente == idCliente,
                                                   liquidacaoTermoBolsa.Query.CdAtivoBolsa == cdAtivoTermo,
                                                   liquidacaoTermoBolsa.Query.DataMovimento == dataOperacao,
                                                   liquidacaoTermoBolsa.Query.DataLiquidacao == dataVencimentoTermo,
                                                   liquidacaoTermoBolsa.Query.Quantidade == quantidadeOperacao);

                    if (liquidacaoTermoBolsa.Query.Load())
                    {
                        int idPosicaoBolsaTermo = liquidacaoTermoBolsa.IdPosicao.Value;
                        PosicaoTermoBolsaAbertura pbTermoHistoricoAb = new PosicaoTermoBolsaAbertura();
                        pbTermoHistoricoAb.Query.Select(pbTermoHistoricoAb.Query.ValorTermoLiquido,
                                                        pbTermoHistoricoAb.Query.Quantidade);
                        pbTermoHistoricoAb.Query.Where(pbTermoHistoricoAb.Query.IdPosicao == idPosicaoBolsaTermo,
                                                       pbTermoHistoricoAb.Query.DataHistorico == dataOperacao);

                        if (pbTermoHistoricoAb.Query.Load())
                        {
                            valorOperacao = pbTermoHistoricoAb.ValorTermoLiquido.Value / pbTermoHistoricoAb.Quantidade.Value * quantidadeOperacao;
                            valorCusto = quantidadeOperacao * puCustoAnterior;
                        }         
                    }      
 
                }
                #endregion
                
                #endregion

                // Chama o metodo para inserir a linha de detalhe menos para Conversao que pode chamar mais de uma vez a rotina
                if (origem != OrigemOperacaoBolsa.ResultadoConversao)
                {
                    setPlanDetalhe(ref worksheet, a.IdEmissor.Value, pessoa.Tipo.Value, "", quantidadeOperacao, valorOperacao, puCustoOperacao,
                                   ref quantidadeSaldoAnterior, ref valorCustoAnterior, ref puCustoAnterior, ref dataOperacaoAnterior, fator, origem,
                                   especificacaoBolsa, ref linha, j);
                }

            }
            #endregion

            #endregion
                   
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="worksheet">Planilha 1</param>
        private void setPlanDetalhe(ref Worksheet worksheet, int idEmissor, byte tipoPessoa, string ativoConvertido, decimal quantidadeOperacao, decimal valorOperacao, decimal puCustoOperacao,
                                    ref decimal quantidadeSaldoAnterior, ref decimal valorCustoAnterior, ref decimal puCustoAnterior, ref DateTime dataOperacaoAnterior, decimal fator, byte origem,
                                    EspecificacaoBolsa especificacaoBolsa, ref int linha, int j)
        {

            DateTime dataOperacao = operacaoBolsaCollection[j].Data.Value;
            string tipoOperacao = operacaoBolsaCollection[j].TipoOperacao;
            string observacao = String.IsNullOrEmpty(operacaoBolsaCollection[j].Observacao) ? "" : " - " + operacaoBolsaCollection[j].Observacao;

            bool calculaPUCusto = false;
            bool isReversao = false;

            if (quantidadeSaldoAnterior == 0 && dataOperacaoAnterior < dataOperacao)
            {
                valorCustoAnterior = 0;
            }

            if (tipoOperacao == TipoOperacaoBolsa.Compra || tipoOperacao == TipoOperacaoBolsa.Deposito)
            {
                quantidadeSaldo = quantidadeSaldoAnterior + quantidadeOperacao;
                valorCusto = valorCustoAnterior + valorOperacao;

                if ((Math.Abs(quantidadeSaldoAnterior) - quantidadeSaldo) < 0)
                {
                    calculaPUCusto = true;
                }

                isReversao = Math.Sign(quantidadeSaldoAnterior) != Math.Sign(quantidadeSaldo) && (quantidadeSaldoAnterior != 0 && quantidadeSaldo != 0);
            }
            else
            {
                quantidadeSaldo = quantidadeSaldoAnterior - quantidadeOperacao;
                if (quantidadeSaldo == 0)
                {
                    valorCusto = valorCustoAnterior;
                }
                else
                {
                    if (quantidadeOperacao != 0)
                    {
                        valorCusto = valorCustoAnterior - (valorCustoAnterior / quantidadeSaldoAnterior) * quantidadeOperacao;
                    }
                    else
                    {
                        valorCusto = valorCustoAnterior - valorOperacao;
                    }
                }
            }

            decimal custoBaixado = 0;
            if (((tipoOperacao == TipoOperacaoBolsa.Compra || tipoOperacao == TipoOperacaoBolsa.Deposito) && quantidadeSaldoAnterior >= 0) ||
                ((tipoOperacao == TipoOperacaoBolsa.Venda || tipoOperacao == TipoOperacaoBolsa.Retirada) && quantidadeSaldoAnterior < 0))
            {
                custoBaixado = Math.Round(puCustoOperacao * quantidadeOperacao * fator, 2);
                if (puCustoOperacao == 0)
                {
                    custoBaixado = Math.Round(puCustoAnterior * quantidadeOperacao * fator, 2);
                }
            }
            else
            {
                custoBaixado = Math.Round(puCustoAnterior * quantidadeOperacao * fator, 2);
                if (tipoOperacao != TipoOperacaoBolsa.Venda && tipoOperacao != TipoOperacaoBolsa.Retirada)
                {
                    if (puCustoAnterior == 0)
                    {
                        custoBaixado = Math.Round(puCustoOperacao * quantidadeOperacao * fator, 2);
                    }
                }
            }

            if (isReversao)
            {
                custoBaixado = Math.Round(puCustoAnterior * Math.Abs(quantidadeSaldoAnterior) * fator, 2);
                custoBaixado += Math.Round(puCustoOperacao * quantidadeSaldo * fator, 2);

            }

            if (origem == OrigemOperacaoBolsa.TransferenciaCorretora)
            {
                puCustoOperacao = 0;
                if (quantidadeSaldoAnterior != 0 && tipoOperacao == TipoOperacaoBolsa.Deposito)
                {
                    valorCusto = (valorCustoAnterior / quantidadeSaldoAnterior) * quantidadeSaldo;
                }
                valorOperacao = valorCusto;
                calculaPUCusto = false;
            }

            if (calculaPUCusto)
            {
                if (tipoOperacao == TipoOperacaoBolsa.Compra || tipoOperacao == TipoOperacaoBolsa.Deposito)
                {
                    valorCusto = valorCustoAnterior + custoBaixado;
                }
                else
                {
                    valorCusto = valorCustoAnterior - custoBaixado;
                }

                if (quantidadeSaldo != 0)
                {
                    puCustoLiquido = (valorCusto * fator) / quantidadeSaldo;
                }
            }
            else
            {
                if (puCustoOperacao != 0 && quantidadeOperacao != 0)
                {
                    //Se teve resultado é porque é operação de reversão (venda baixando compra por ex) e mantem o último pu de custo
                    valorCusto = Math.Round((quantidadeSaldo * puCustoAnterior) / fator, 2);

                    if (quantidadeSaldo != 0)
                    {
                        puCustoLiquido = puCustoAnterior;
                    }
                }

            }

            decimal quantidadeCapitalSocial = new EvolucaoCapitalSocial().RetornaQuantidade(idEmissor, dataOperacao, null);
            decimal quantidadeCapitalVotante = new EvolucaoCapitalSocial().RetornaQuantidade(idEmissor, dataOperacao, TipoEspecificacaoBolsa.Ordinaria);
            //
            decimal? percentualCapitalSocial = null;
            try
            {
                if (especificacaoBolsa.Tipo.Value == (byte)TipoEspecificacaoBolsa.Unit)
                {
                    percentualCapitalSocial = (quantidadeSaldo * 5) / quantidadeCapitalSocial * 100M; //1 ON + 4 PN
                }
                else
                {
                    percentualCapitalSocial = quantidadeSaldo / quantidadeCapitalSocial * 100M;
                }
            }
            catch (Exception) { }

            decimal? percentualCapitalVotante = null;
            try
            {
                if (especificacaoBolsa.Tipo.HasValue &&
                        (especificacaoBolsa.Tipo.Value == (byte)TipoEspecificacaoBolsa.Ordinaria || especificacaoBolsa.Tipo.Value == (byte)TipoEspecificacaoBolsa.Unit))
                {
                    percentualCapitalVotante = quantidadeSaldo / quantidadeCapitalVotante * 100M;
                }
            }
            catch (Exception) { }

            worksheet.Rows[linha][1].ValueAsDateTime = dataOperacao;
            //                


            string textoTipo = tipoOperacao != "" ? TipoOperacaoBolsa.str.RetornaTexto(tipoOperacao) : "";
            if (origem == (byte)OrigemOperacaoBolsa.ResultadoBonificacao)
            {
                textoTipo = textoTipo + " por Bonificação";
                valorOperacao = 0;
                custoBaixado = operacaoBolsaCollection[j].ValorLiquido.Value;
                valorCusto = valorCustoAnterior + custoBaixado;
            }
            else if (origem == (byte)OrigemOperacaoBolsa.ResultadoGrupamento)
            {
                GrupamentoBolsaCollection grupamentoBolsaCollection = new GrupamentoBolsaCollection();
                grupamentoBolsaCollection.Query.Select(grupamentoBolsaCollection.Query.TipoGrupamento);
                grupamentoBolsaCollection.Query.Where(grupamentoBolsaCollection.Query.CdAtivoBolsa.Equal(this.codigoAtivo),
                                                      grupamentoBolsaCollection.Query.DataEx.Equal(dataOperacao));
                grupamentoBolsaCollection.Query.Load();

                string grupamentoString = " por Grupamento/Desdobro";
                if (grupamentoBolsaCollection.Count > 0)
                {
                    if (grupamentoBolsaCollection[0].TipoGrupamento.Value == (byte)TipoGrupamentoBolsa.Desdobramento)
                    {
                        grupamentoString = " por Desdobro";
                    }
                    else
                    {
                        grupamentoString = " por Grupamento";
                    }
                }

                textoTipo = textoTipo + grupamentoString;
                valorOperacao = 0;
                custoBaixado = 0;
                valorCusto = valorCustoAnterior;
            }
            else if (origem == (byte)OrigemOperacaoBolsa.ResultadoConversao)
            {
                if (tipoOperacao == TipoOperacaoBolsa.Retirada)
                {
                    textoTipo = textoTipo + " por Conversão";
                }
                else
                {
                    textoTipo = textoTipo + " por Conversão de " + ativoConvertido;
                    if (quantidadeSaldoAnterior == 0)
                    {
                        valorCusto = custoBaixado;
                    }
                }
                valorOperacao = 0;
            }
            else if ((operacaoBolsaCollection[j].TipoOperacao == TipoOperacaoBolsa.Deposito || operacaoBolsaCollection[j].TipoOperacao == TipoOperacaoBolsa.Retirada) && operacaoBolsaCollection[j].Origem != OrigemOperacaoBolsa.LiquidacaoTermo)
            {
                custoBaixado = valorOperacao;
                valorOperacao = 0;
            }

            quantidadeSaldoAnterior = quantidadeSaldo;
            if (quantidadeSaldo == 0)
            {
                if (quantidadeOperacao == 0)
                {
                    custoBaixado = 0;
                }
                else
                {
                    custoBaixado = valorCustoAnterior;
                }
                valorCusto = 0;
                puCustoLiquido = 0;
            }
            else
            {
                puCustoLiquido = valorCusto / quantidadeSaldo;
                puCustoAnterior = puCustoLiquido;
                valorCustoAnterior = valorCusto;
            }
            dataOperacaoAnterior = dataOperacao;

            textoTipo = textoTipo + observacao;

            worksheet.Rows[linha][2].ValueAsString = textoTipo;
            worksheet.Rows[linha][3].NumberFormatString = "#,##,#0";
            worksheet.Rows[linha][3].ValueAsDouble = Convert.ToDouble(quantidadeOperacao);
            worksheet.Rows[linha][4].NumberFormatString = "#,##,#0";
            worksheet.Rows[linha][4].ValueAsDouble = Convert.ToDouble(quantidadeSaldo);

            if (percentualCapitalVotante.HasValue)
            {
                worksheet.Rows[linha][5].NumberFormatString = "#,##,#0.000000";
                worksheet.Rows[linha][5].ValueAsDouble = Convert.ToDouble(percentualCapitalVotante);
            }
            else
            {
                worksheet.Rows[linha][5].ValueAsString = " - ";
            }

            if (percentualCapitalSocial.HasValue)
            {
                worksheet.Rows[linha][6].NumberFormatString = "#,##,#0.000000";
                worksheet.Rows[linha][6].ValueAsDouble = Convert.ToDouble(percentualCapitalSocial);
            }
            else
            {
                worksheet.Rows[linha][6].ValueAsString = " - ";
            }

            //
            worksheet.Rows[linha][7].NumberFormatString = "#,##,#0.00";
            worksheet.Rows[linha][7].ValueAsDouble = Convert.ToDouble(valorOperacao);
            //

            worksheet.Rows[linha][8].NumberFormatString = "#,##,#0.00";
            if (tipoPessoa == (byte)TipoPessoa.Fisica)
            {
                worksheet.Rows[linha][8].ValueAsDouble = Convert.ToDouble(custoBaixado);
            }
            else
            {
                worksheet.Rows[linha][8].ValueAsDouble = 0;
            }

            //
            worksheet.Rows[linha][9].NumberFormatString = "#,##,#0.00";
            if (tipoPessoa == (byte)TipoPessoa.Fisica)
            {
                worksheet.Rows[linha][9].ValueAsDouble = Convert.ToDouble(puCustoOperacao);
            }
            else
            {
                worksheet.Rows[linha][9].ValueAsDouble = 0;
            }
            //
            worksheet.Rows[linha][10].NumberFormatString = "#,##,#0.00";
            if (tipoPessoa == (byte)TipoPessoa.Fisica)
            {
                worksheet.Rows[linha][10].ValueAsDouble = Convert.ToDouble(valorCusto);
            }
            else
            {
                worksheet.Rows[linha][10].ValueAsDouble = 0;
            }
            //
            worksheet.Rows[linha][11].NumberFormatString = "#,##,#0.00";
            if (tipoPessoa == (byte)TipoPessoa.Fisica)
            {
                worksheet.Rows[linha][11].ValueAsDouble = Convert.ToDouble(puCustoLiquido);
            }
            else
            {
                worksheet.Rows[linha][11].ValueAsDouble = 0;
            }
            //     

            #region Alinhamentos B/C/D/E/F/G/H/I/J/K
            worksheet.Rows[linha][1].AlignmentHorizontal = Bytescout.Spreadsheet.Constants.AlignmentHorizontal.Left;   // B
            worksheet.Rows[linha][2].AlignmentHorizontal = Bytescout.Spreadsheet.Constants.AlignmentHorizontal.Left;   // C
            worksheet.Rows[linha][3].AlignmentHorizontal = Bytescout.Spreadsheet.Constants.AlignmentHorizontal.Right;  // D
            worksheet.Rows[linha][4].AlignmentHorizontal = Bytescout.Spreadsheet.Constants.AlignmentHorizontal.Right;  // E                
            worksheet.Rows[linha][5].AlignmentHorizontal = Bytescout.Spreadsheet.Constants.AlignmentHorizontal.Right;  // F
            worksheet.Rows[linha][6].AlignmentHorizontal = Bytescout.Spreadsheet.Constants.AlignmentHorizontal.Right;  // G                
            worksheet.Rows[linha][7].AlignmentHorizontal = Bytescout.Spreadsheet.Constants.AlignmentHorizontal.Right;  // H
            worksheet.Rows[linha][8].AlignmentHorizontal = Bytescout.Spreadsheet.Constants.AlignmentHorizontal.Right;  // I
            worksheet.Rows[linha][9].AlignmentHorizontal = Bytescout.Spreadsheet.Constants.AlignmentHorizontal.Right;  // J
            worksheet.Rows[linha][10].AlignmentHorizontal = Bytescout.Spreadsheet.Constants.AlignmentHorizontal.Right; // K

            if (!percentualCapitalVotante.HasValue)
            {
                worksheet.Rows[linha][5].AlignmentHorizontal = AlignmentHorizontal.Centered;  // F
            }

            if (!percentualCapitalSocial.HasValue)
            {
                worksheet.Rows[linha][6].AlignmentHorizontal = AlignmentHorizontal.Centered;  // G
            }
            #endregion

            linha++;
        }

    }
}