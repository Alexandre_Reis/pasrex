using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Financial.Fundo;
using System.Collections.Generic;
using Financial.Common;
using Financial.Util;
using Financial.Common.Enums;
using Financial.RendaFixa;
using Financial.CRM;
using Financial.Investidor;
using Financial.RendaFixa.Enums;
using Bytescout.Spreadsheet;
using Bytescout.Spreadsheet.Constants;
using System.IO;

namespace Financial.Export
{

    /// <summary>
    /// Summary description for RentabilPorto
    /// </summary>
    public class RendaFixaItau
    {
        private const string MODELO_PLANILHA_OPERACAO = "OperacaoRendaFixa.csv";
        private const string MODELO_PLANILHA_POSICAO = "PosicaoRendaFixa.csv";

        public class OperacaoRF
        {
            private int? cci;
            public int? Cci
            {
                get { return cci; }
                set { cci = value; }
            }

            private string cpf;
            public string Cpf
            {
                get { return cpf; }
                set { cpf = value; }
            }

            private string agencia;
            public string Agencia
            {
                get { return agencia; }
                set { agencia = value; }
            }

            private string conta;
            public string Conta
            {
                get { return conta; }
                set { conta = value; }
            }

            private DateTime dataOperacao;
            public DateTime DataOperacao
            {
                get { return dataOperacao; }
                set { dataOperacao = value; }
            }

            private string mercadoria;
            public string Mercadoria
            {
                get { return mercadoria; }
                set { mercadoria = value; }
            }

            private decimal? taxa;
            public decimal? Taxa
            {
                get { return taxa; }
                set { taxa = value; }
            }

            private string indice;
            public string Indice
            {
                get { return indice; }
                set { indice = value; }
            }

            private DateTime emissao;
            public DateTime Emissao
            {
                get { return emissao; }
                set { emissao = value; }
            }

            private DateTime vencimento;
            public DateTime Vencimento
            {
                get { return vencimento; }
                set { vencimento = value; }
            }

            private string emissor;
            public string Emissor
            {
                get { return emissor; }
                set { emissor = value; }
            }

            private string tipoOperacao;
            public string Tipooperacao
            {
                get { return tipoOperacao; }
                set { tipoOperacao = value; }
            }

            private decimal? qtde;
            public decimal? Qtde
            {
                get { return qtde; }
                set { qtde = value; }
            }

            private decimal? pu;
            public decimal? Pu
            {
                get { return pu; }
                set { pu = value; }
            }

            private decimal? valor;
            public decimal? Valor
            {
                get { return valor; }
                set { valor = value; }
            }

            private decimal? rendimento;
            public decimal? Rendimento
            {
                get { return rendimento; }
                set { rendimento = value; }
            }

            private decimal? ir;
            public decimal? Ir
            {
                get { return ir; }
                set { ir = value; }
            }

            private decimal? iof;
            public decimal? Iof
            {
                get { return iof; }
                set { iof = value; }
            }

            private decimal? corretagem;
            public decimal? Corretagem
            {
                get { return corretagem; }
                set { corretagem = value; }
            }

            private decimal? valorTotalOperacao;
            public decimal? ValorTotalOperacao
            {
                get { return valorTotalOperacao; }
                set { valorTotalOperacao = value; }
            }

            private string depositaria;
            public string Depositaria
            {
                get { return depositaria; }
                set { depositaria = value; }
            }
        }
                
        public List<OperacaoRF> RetornaListOperacaoRF(DateTime data)
        {
            DateTime dataInicioMes = new DateTime(data.Year, data.Month, 1);

            DateTime dataFimMes = Calendario.RetornaUltimoDiaUtilMes(data);

            OperacaoRendaFixaQuery operacaoRendaFixaQuery = new OperacaoRendaFixaQuery("O");
            TituloRendaFixaQuery tituloRendaFixaQuery = new TituloRendaFixaQuery("T");
            PapelRendaFixaQuery papelRendaFixa = new PapelRendaFixaQuery("P");
            PessoaQuery pessoaQuery = new PessoaQuery("E");
            IndiceQuery indiceQuery = new IndiceQuery("I");
            EmissorQuery emissorQuery = new EmissorQuery("M");

            operacaoRendaFixaQuery.Select(operacaoRendaFixaQuery,
                                        pessoaQuery.Cpfcnpj,
                                        papelRendaFixa.Descricao,
                                        indiceQuery.Descricao.As("Indice"),
                                        tituloRendaFixaQuery.DataEmissao,
                                        tituloRendaFixaQuery.DataVencimento,
                                        emissorQuery.Nome.As("Emissor"));
            operacaoRendaFixaQuery.InnerJoin(pessoaQuery).On
                (pessoaQuery.IdPessoa == operacaoRendaFixaQuery.IdCliente);
            operacaoRendaFixaQuery.InnerJoin(tituloRendaFixaQuery).On
                (operacaoRendaFixaQuery.IdTitulo == tituloRendaFixaQuery.IdTitulo);
            operacaoRendaFixaQuery.InnerJoin(papelRendaFixa).On
                (tituloRendaFixaQuery.IdPapel == papelRendaFixa.IdPapel);
            operacaoRendaFixaQuery.InnerJoin(indiceQuery).On
                (tituloRendaFixaQuery.IdIndice == indiceQuery.IdIndice);
            operacaoRendaFixaQuery.InnerJoin(emissorQuery).On
                (tituloRendaFixaQuery.IdEmissor == emissorQuery.IdEmissor);
            operacaoRendaFixaQuery.Where(
                operacaoRendaFixaQuery.DataOperacao.Between(dataInicioMes, dataFimMes));
            
            OperacaoRendaFixaCollection operacaoRendaFixaCollection = new OperacaoRendaFixaCollection();
            operacaoRendaFixaCollection.Load(operacaoRendaFixaQuery);
            operacaoRendaFixaCollection.Sort = "DataOperacao asc, IdOperacao asc";

            List<OperacaoRF> listOperacaoRF = new List<OperacaoRF>();

            foreach (OperacaoRendaFixa operacaoRendaFixa in operacaoRendaFixaCollection)
            {

                OperacaoRF operacaoRF = new OperacaoRF();
                
                #region contacorrente

                Financial.Investidor.ContaCorrente contaCorrente = new Financial.Investidor.ContaCorrente();
                ContaCorrenteQuery contaCorrenteQuery = new ContaCorrenteQuery("C");
                contaCorrenteQuery.Where (contaCorrenteQuery.ContaDefault.Equal("S"),
                                        contaCorrenteQuery.IdPessoa.Equal(operacaoRendaFixa.IdCliente));

                int idagencia = 0;

                if (contaCorrente.Load(contaCorrenteQuery))
                {
                    operacaoRF.Conta = contaCorrente.Numero + contaCorrente.DigitoConta;
                    if (contaCorrente.IdAgencia.HasValue)
                    {
                        idagencia = (int)contaCorrente.IdAgencia;
                    }
                }
                else
                {
                    contaCorrente = new Financial.Investidor.ContaCorrente();
                    contaCorrenteQuery = new ContaCorrenteQuery("N");
                    contaCorrenteQuery.Where(contaCorrenteQuery.IdPessoa.Equal(operacaoRendaFixa.IdCliente));
                    if (contaCorrente.Load(contaCorrenteQuery))
                    {
                        operacaoRF.Conta = contaCorrente.Numero.ToString() + contaCorrente.DigitoConta.ToString();
                        if (contaCorrente.IdAgencia.HasValue)
                        {
                            idagencia = (int)contaCorrente.IdAgencia;
                        }
                    }
                }

                if (idagencia != 0)
                {
                    Agencia agencia = new Agencia();
                    AgenciaQuery agenciaQuery = new AgenciaQuery("A");
                    agenciaQuery.Where(agenciaQuery.IdAgencia.Equal(idagencia));
                    if (agencia.Load(agenciaQuery))
                    {
                        operacaoRF.Agencia = agencia.Codigo;
                    }
                }
                #endregion

                #region pessoa
                operacaoRF.Cpf = operacaoRendaFixa.GetColumn(PessoaMetadata.ColumnNames.Cpfcnpj).ToString();
                #endregion

                #region indice
                operacaoRF.Indice = operacaoRendaFixa.GetColumn("Indice").ToString();
                #endregion

                #region emissor
                operacaoRF.Emissor = operacaoRendaFixa.GetColumn("Emissor").ToString();
                #endregion

                #region titulo RF
                operacaoRF.Emissao = Convert.ToDateTime(operacaoRendaFixa.GetColumn(TituloRendaFixaMetadata.ColumnNames.DataEmissao));
                operacaoRF.Vencimento = Convert.ToDateTime(operacaoRendaFixa.GetColumn(TituloRendaFixaMetadata.ColumnNames.DataVencimento));
                #endregion

                #region papel RF
                operacaoRF.Mercadoria = operacaoRendaFixa.GetColumn(PapelRendaFixaMetadata.ColumnNames.Descricao).ToString();
                #endregion

                #region operacao RF
                operacaoRF.Cci = operacaoRendaFixa.IdCliente;
                operacaoRF.DataOperacao = Convert.ToDateTime(operacaoRendaFixa.DataOperacao);
                operacaoRF.Taxa = operacaoRendaFixa.TaxaOperacao;
                operacaoRF.Tipooperacao = Financial.Util.StringEnum.GetStringValue((TipoOperacaoTitulo)operacaoRendaFixa.TipoOperacao);
                operacaoRF.Qtde = operacaoRendaFixa.Quantidade;
                operacaoRF.Pu = operacaoRendaFixa.PUOperacao;
                operacaoRF.Valor = operacaoRendaFixa.Valor;
                operacaoRF.Rendimento = operacaoRendaFixa.Rendimento;
                operacaoRF.Ir = operacaoRendaFixa.ValorIR;
                operacaoRF.Iof = operacaoRendaFixa.ValorIOF;
                operacaoRF.Corretagem = operacaoRendaFixa.ValorCorretagem;
                operacaoRF.ValorTotalOperacao = operacaoRendaFixa.Valor - operacaoRendaFixa.ValorIR - operacaoRendaFixa.ValorIOF - operacaoRendaFixa.ValorCorretagem;
                operacaoRF.Depositaria = Financial.Util.StringEnum.GetStringValue((LocalCustodiaFixo)operacaoRendaFixa.IdCustodia);
                #endregion
                
                listOperacaoRF.Add(operacaoRF);

            }

            return listOperacaoRF;
        }

        public class PosicaoRF
        {
            private int? cci;
            public int? Cci
            {
                get { return cci; }
                set { cci = value; }
            }

            private string cpf;
            public string Cpf
            {
                get { return cpf; }
                set { cpf = value; }
            }

            private string agencia;
            public string Agencia
            {
                get { return agencia; }
                set { agencia = value; }
            }

            private string conta;
            public string Conta
            {
                get { return conta; }
                set { conta = value; }
            }

            private string mercadoria;
            public string Mercadoria
            {
                get { return mercadoria; }
                set { mercadoria = value; }
            }

            private decimal? taxa;
            public decimal? Taxa
            {
                get { return taxa; }
                set { taxa = value; }
            }

            private string indice;
            public string Indice
            {
                get { return indice; }
                set { indice = value; }
            }

            private DateTime emissao;
            public DateTime Emissao
            {
                get { return emissao; }
                set { emissao = value; }
            }

            private DateTime vencimento;
            public DateTime Vencimento
            {
                get { return vencimento; }
                set { vencimento = value; }
            }
            
            private string emissorOperacao;
            public string EmissorOperacao
            {
                get { return emissorOperacao; }
                set { emissorOperacao = value; }
            }

            private decimal? qtde;
            public decimal? Qtde
            {
                get { return qtde; }
                set { qtde = value; }
            }

            private decimal? pu;
            public decimal? Pu
            {
                get { return pu; }
                set { pu = value; }
            }

            private decimal? valor;
            public decimal? Valor
            {
                get { return valor; }
                set { valor = value; }
            }

            private decimal? puMercado;
            public decimal? PuMercado
            {
                get { return puMercado; }
                set { puMercado = value; }
            }

            private decimal? rendimento;
            public decimal? Rendimento
            {
                get { return rendimento; }
                set { rendimento = value; }
            }

            private decimal? saldo;
            public decimal? Saldo
            {
                get { return saldo; }
                set { saldo = value; }
            }

            private string depositaria;
            public string Depositaria
            {
                get { return depositaria; }
                set { depositaria = value; }
            }

        }

        public List<PosicaoRF> RetornaListPosicaoRF(DateTime data)
        {            
            PosicaoRendaFixaHistoricoQuery posicaoRendaFixaHistoricoQuery = new PosicaoRendaFixaHistoricoQuery("P");
            PessoaQuery pessoaQuery = new PessoaQuery("E");
            TituloRendaFixaQuery tituloRendaFixaQuery = new TituloRendaFixaQuery("T");
            PapelRendaFixaQuery papelRendaFixaQuery = new PapelRendaFixaQuery("A");
            IndiceQuery indiceQuery = new IndiceQuery("I");
            EmissorQuery emissorQuery = new EmissorQuery("M");

            posicaoRendaFixaHistoricoQuery.Select(posicaoRendaFixaHistoricoQuery,
                                        pessoaQuery.Cpfcnpj,
                                        papelRendaFixaQuery.Descricao,
                                        indiceQuery.Descricao.As("Indice"),
                                        tituloRendaFixaQuery.DataEmissao,
                                        tituloRendaFixaQuery.DataVencimento,
                                        emissorQuery.Nome.As("Emissor"));
            posicaoRendaFixaHistoricoQuery.InnerJoin(tituloRendaFixaQuery).On
                (posicaoRendaFixaHistoricoQuery.IdTitulo == tituloRendaFixaQuery.IdTitulo);
            posicaoRendaFixaHistoricoQuery.InnerJoin(papelRendaFixaQuery).On
                (tituloRendaFixaQuery.IdPapel == papelRendaFixaQuery.IdPapel);
            posicaoRendaFixaHistoricoQuery.InnerJoin(emissorQuery).On
                (tituloRendaFixaQuery.IdEmissor == emissorQuery.IdEmissor);
            posicaoRendaFixaHistoricoQuery.InnerJoin(indiceQuery).On
                (tituloRendaFixaQuery.IdIndice == indiceQuery.IdIndice);
            posicaoRendaFixaHistoricoQuery.InnerJoin(pessoaQuery).On
                (posicaoRendaFixaHistoricoQuery.IdCliente == pessoaQuery.IdPessoa);
            posicaoRendaFixaHistoricoQuery.Where(posicaoRendaFixaHistoricoQuery.DataHistorico.Equal(data));

            PosicaoRendaFixaHistoricoCollection posicaoRendaFixaHistoricoCollection = new PosicaoRendaFixaHistoricoCollection();
            posicaoRendaFixaHistoricoCollection.Load(posicaoRendaFixaHistoricoQuery);
            posicaoRendaFixaHistoricoCollection.Sort = "Descricao asc";

            List<PosicaoRF> listPosicaoRF = new List<PosicaoRF>();

            foreach (PosicaoRendaFixaHistorico posicaoRendaFixaHistorico in posicaoRendaFixaHistoricoCollection)
            {
                PosicaoRF posicaoRF = new PosicaoRF();
                
                #region contacorrente
                Financial.Investidor.ContaCorrente contaCorrente = new Financial.Investidor.ContaCorrente();
                ContaCorrenteQuery contaCorrenteQuery = new ContaCorrenteQuery("C");
                contaCorrenteQuery.Where(contaCorrenteQuery.ContaDefault.Equal("S"),
                                        contaCorrenteQuery.IdPessoa.Equal(posicaoRendaFixaHistorico.IdCliente));

                int idagencia = 0;

                if (contaCorrente.Load(contaCorrenteQuery))
                {
                    posicaoRF.Conta = contaCorrente.Numero + contaCorrente.DigitoConta;
                    if (contaCorrente.IdAgencia.HasValue)
                    {
                        idagencia = (int)contaCorrente.IdAgencia;
                    }
                }
                else
                {
                    contaCorrente = new Financial.Investidor.ContaCorrente();
                    contaCorrenteQuery = new ContaCorrenteQuery("N");
                    contaCorrenteQuery.Where(contaCorrenteQuery.IdPessoa.Equal(posicaoRendaFixaHistorico.IdCliente));
                    if (contaCorrente.Load(contaCorrenteQuery))
                    {
                        posicaoRF.Conta = contaCorrente.Numero.ToString() + contaCorrente.DigitoConta.ToString();
                        if (contaCorrente.IdAgencia.HasValue)
                        {
                            idagencia = (int)contaCorrente.IdAgencia;
                        }
                    }
                }

                if (idagencia != 0)
                {
                    Agencia agencia = new Agencia();
                    AgenciaQuery agenciaQuery = new AgenciaQuery("A");
                    agenciaQuery.Where(agenciaQuery.IdAgencia.Equal(idagencia));
                    if (agencia.Load(agenciaQuery))
                    {
                        posicaoRF.Agencia = agencia.Codigo;
                    }
                }
                #endregion

                #region pessoa
                posicaoRF.Cpf = posicaoRendaFixaHistorico.GetColumn(PessoaMetadata.ColumnNames.Cpfcnpj).ToString();
                #endregion

                #region papel RF
                posicaoRF.Mercadoria = posicaoRendaFixaHistorico.GetColumn(PapelRendaFixaMetadata.ColumnNames.Descricao).ToString();
                #endregion

                #region indice
                posicaoRF.Indice = posicaoRendaFixaHistorico.GetColumn(IndiceMetadata.ColumnNames.Descricao).ToString();
                #endregion

                #region titulo RF
                posicaoRF.Emissao = Convert.ToDateTime(posicaoRendaFixaHistorico.GetColumn(TituloRendaFixaMetadata.ColumnNames.DataEmissao));
                posicaoRF.Vencimento = Convert.ToDateTime(posicaoRendaFixaHistorico.GetColumn(TituloRendaFixaMetadata.ColumnNames.DataVencimento));
                #endregion

                #region emissor
                posicaoRF.EmissorOperacao = posicaoRendaFixaHistorico.GetColumn("Emissor").ToString();
                #endregion

                #region posicao RF
                posicaoRF.Cci = posicaoRendaFixaHistorico.IdCliente;
                posicaoRF.Taxa = posicaoRendaFixaHistorico.TaxaOperacao;
                posicaoRF.Qtde = posicaoRendaFixaHistorico.Quantidade;
                posicaoRF.Pu = posicaoRendaFixaHistorico.PUOperacao;
                posicaoRF.PuMercado = posicaoRendaFixaHistorico.PUMercado;
                posicaoRF.Rendimento = (posicaoRendaFixaHistorico.PUMercado - posicaoRendaFixaHistorico.PUOperacao) * posicaoRendaFixaHistorico.Quantidade;
                posicaoRF.Valor = posicaoRendaFixaHistorico.ValorMercado;
                posicaoRF.Saldo = posicaoRendaFixaHistorico.ValorMercado;
                posicaoRF.Depositaria = Financial.Util.StringEnum.GetStringValue((LocalCustodiaFixo)posicaoRendaFixaHistorico.IdCustodia);
                #endregion

                listPosicaoRF.Add(posicaoRF);
            }

            return listPosicaoRF;
        }

        public bool ExportaOperacaoRF(DateTime data, out MemoryStream msPlanilhaOperacaoRFExcel)
        {
            msPlanilhaOperacaoRFExcel = new MemoryStream();

            List<OperacaoRF> listOperacaoRF = RetornaListOperacaoRF(data);
            
            if (listOperacaoRF.Count == 0)
            {
                return false;
            }

            #region EscreveArquivo
            Bytescout.Spreadsheet.Spreadsheet document = new Bytescout.Spreadsheet.Spreadsheet();
            string pathModelos = AppDomain.CurrentDomain.BaseDirectory + "\\Relatorios\\Modelos";
            string pathPlanilhaModelo = String.Format("{0}\\{1}", pathModelos, MODELO_PLANILHA_OPERACAO);

            document.LoadFromFile(pathPlanilhaModelo);

            Worksheet worksheet = document.Workbook.Worksheets[0];
            worksheet.ViewOptions.ShowZeroValues = true;

            int linha = 1;
            foreach (OperacaoRF operacaoRF in listOperacaoRF)
            {
                if (operacaoRF.Cci.HasValue)
                {
                    worksheet.Rows[linha][0].ValueAsInteger = (int)operacaoRF.Cci;
                }
                worksheet.Rows[linha][1].ValueAsString = operacaoRF.Cpf;
                worksheet.Rows[linha][2].ValueAsString = operacaoRF.Agencia;
                worksheet.Rows[linha][3].ValueAsString = operacaoRF.Conta;
                worksheet.Rows[linha][4].ValueAsDateTime = operacaoRF.DataOperacao;
                worksheet.Rows[linha][5].ValueAsString = operacaoRF.Mercadoria;
                worksheet.Rows[linha][6].ValueAsDouble = operacaoRF.Taxa.HasValue ? Convert.ToDouble(operacaoRF.Taxa.Value) : 0;
                worksheet.Rows[linha][7].ValueAsString = operacaoRF.Indice;
                worksheet.Rows[linha][8].ValueAsDateTime = operacaoRF.Emissao;
                worksheet.Rows[linha][9].ValueAsDateTime = operacaoRF.Vencimento;
                worksheet.Rows[linha][10].ValueAsString = operacaoRF.Emissor;
                worksheet.Rows[linha][11].ValueAsString = operacaoRF.Tipooperacao;
                worksheet.Rows[linha][12].ValueAsDouble = operacaoRF.Qtde.HasValue ? Convert.ToDouble(operacaoRF.Qtde.Value) : 0;
                worksheet.Rows[linha][13].ValueAsDouble = operacaoRF.Pu.HasValue ? Convert.ToDouble(operacaoRF.Pu.Value) : 0;
                worksheet.Rows[linha][14].ValueAsDouble = operacaoRF.Valor.HasValue ? Convert.ToDouble(operacaoRF.Valor.Value) : 0;
                worksheet.Rows[linha][15].ValueAsDouble = operacaoRF.Rendimento.HasValue ? Convert.ToDouble(operacaoRF.Rendimento.Value) : 0;
                worksheet.Rows[linha][16].ValueAsDouble = operacaoRF.Ir.HasValue ? Convert.ToDouble(operacaoRF.Ir.Value) : 0;
                worksheet.Rows[linha][17].ValueAsDouble = operacaoRF.Iof.HasValue ? Convert.ToDouble(operacaoRF.Iof.Value) : 0;
                worksheet.Rows[linha][18].ValueAsDouble = operacaoRF.Corretagem.HasValue ? Convert.ToDouble(operacaoRF.Corretagem.Value) : 0;
                worksheet.Rows[linha][19].ValueAsDouble = operacaoRF.ValorTotalOperacao.HasValue ? Convert.ToDouble(operacaoRF.ValorTotalOperacao.Value) : 0;
                worksheet.Rows[linha][20].ValueAsString = operacaoRF.Depositaria;

                linha++;
            }

            #region Esconde Planilha Demo do componente
            const string BYTESCOUT_WORKSHEET_NAME = "Bytescout Spreadsheet SDK";
            worksheet = document.Workbook.Worksheets.ByName(BYTESCOUT_WORKSHEET_NAME);
            if (worksheet == null)
            {
                worksheet = document.Workbook.Worksheets.Add(BYTESCOUT_WORKSHEET_NAME);
            }
            worksheet.Visible = Bytescout.Spreadsheet.Constants.SHEETVISIBILITY.Hidden;
            #endregion

            document.SaveToStreamXLS(msPlanilhaOperacaoRFExcel);
            
            document.Close();
            #endregion

            return true;
        }

        public bool ExportaPosicaoRF(DateTime data, out MemoryStream msPlanilhaPosicaoRFExcel)
        {
            msPlanilhaPosicaoRFExcel = new MemoryStream();

            List<PosicaoRF> listPosicaoRF = RetornaListPosicaoRF(data);

            if (listPosicaoRF.Count == 0)
            {
                return false;
            }

            #region EscreveArquivo
            Bytescout.Spreadsheet.Spreadsheet document = new Bytescout.Spreadsheet.Spreadsheet();
            string pathModelos = AppDomain.CurrentDomain.BaseDirectory + "\\Relatorios\\Modelos";
            string pathPlanilhaModelo = String.Format("{0}\\{1}", pathModelos, MODELO_PLANILHA_POSICAO);

            document.LoadFromFile(pathPlanilhaModelo);

            Worksheet worksheet = document.Workbook.Worksheets[0];
            worksheet.ViewOptions.ShowZeroValues = true;

            int linha = 1;
            foreach (PosicaoRF posicaoRF in listPosicaoRF)
            {
                if (posicaoRF.Cci.HasValue)
                {
                    worksheet.Rows[linha][0].ValueAsInteger = (int)posicaoRF.Cci;
                }
                worksheet.Rows[linha][1].ValueAsString = posicaoRF.Cpf;
                worksheet.Rows[linha][2].ValueAsString = posicaoRF.Agencia != null ? posicaoRF.Agencia : "";
                worksheet.Rows[linha][3].ValueAsString = posicaoRF.Conta;
                worksheet.Rows[linha][4].ValueAsString = posicaoRF.Mercadoria;
                worksheet.Rows[linha][5].ValueAsDouble = posicaoRF.Taxa.HasValue ? Convert.ToDouble(posicaoRF.Taxa.Value) : 0;
                worksheet.Rows[linha][6].ValueAsString = posicaoRF.Indice;
                worksheet.Rows[linha][7].ValueAsDateTime = posicaoRF.Emissao;
                worksheet.Rows[linha][8].ValueAsDateTime = posicaoRF.Vencimento;
                worksheet.Rows[linha][9].ValueAsString = posicaoRF.EmissorOperacao;
                worksheet.Rows[linha][10].ValueAsDouble = posicaoRF.Qtde.HasValue ? Convert.ToDouble(posicaoRF.Qtde.Value) : 0;
                worksheet.Rows[linha][11].ValueAsDouble = posicaoRF.Pu.HasValue ? Convert.ToDouble(posicaoRF.Pu.Value) : 0;
                worksheet.Rows[linha][12].ValueAsDouble = posicaoRF.Valor.HasValue ? Convert.ToDouble(posicaoRF.Valor.Value) : 0;
                worksheet.Rows[linha][13].ValueAsDouble = posicaoRF.PuMercado.HasValue ? Convert.ToDouble(posicaoRF.PuMercado.Value) : 0;
                worksheet.Rows[linha][14].ValueAsDouble = posicaoRF.Rendimento.HasValue ? Convert.ToDouble(posicaoRF.Rendimento.Value) : 0;
                worksheet.Rows[linha][15].ValueAsDouble = posicaoRF.Saldo.HasValue ? Convert.ToDouble(posicaoRF.Saldo.Value) : 0;
                worksheet.Rows[linha][16].ValueAsString = posicaoRF.Depositaria;

                linha++;
            }

            #region Esconde Planilha Demo do componente
            const string BYTESCOUT_WORKSHEET_NAME = "Bytescout Spreadsheet SDK";
            worksheet = document.Workbook.Worksheets.ByName(BYTESCOUT_WORKSHEET_NAME);
            if (worksheet == null)
            {
                worksheet = document.Workbook.Worksheets.Add(BYTESCOUT_WORKSHEET_NAME);
            }
            worksheet.Visible = Bytescout.Spreadsheet.Constants.SHEETVISIBILITY.Hidden;
            #endregion

            document.SaveToStreamXLS(msPlanilhaPosicaoRFExcel);

            document.Close();
            #endregion

            return true;
        }
    }
} 