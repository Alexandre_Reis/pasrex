﻿using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Collections.Generic;
using Financial.Investidor;
using Financial.Investidor.Enums;
using Financial.Fundo;
using Financial.BMF;
using Financial.Util;
using Financial.Common.Enums;
using Financial.Bolsa;
using Financial.Fundo.Enums;
using Financial.RendaFixa;
using Financial.Common;
using Financial.RendaFixa.Enums;
using System.Xml;
using Financial.WebConfigConfiguration;
using EntitySpaces.Interfaces;
using Financial.ContaCorrente;
using Financial.Swap;

/// <summary>
/// Summary description for RiscoPortopar
/// </summary>
public class RiscoPortopar
{
    private string[] tiposCarteira = new string[] { "Fundos Exclusivos", "Carteira Própria", "Fundos Abertos", "Outros Fundos/Carteiras" };

    public class DetalheRelatorio
    {
        private int idGrupoEconomico;
        public int IdGrupoEconomico
        {
            get { return idGrupoEconomico; }
            set { idGrupoEconomico = value; }
        }

        private string nomeGrupoEconomico;
        public string NomeGrupoEconomico
        {
            get { return nomeGrupoEconomico; }
            set { nomeGrupoEconomico = value; }
        }

        private string tipoCarteira;
        public string TipoCarteira
        {
            get { return tipoCarteira; }
            set { tipoCarteira = value; }
        }

        private string grupoTitulo;
        public string GrupoTitulo
        {
            get { return grupoTitulo; }
            set { grupoTitulo = value; }
        }

        private string tipoTitulo;
        public string TipoTitulo
        {
            get { return tipoTitulo; }
            set { tipoTitulo = value; }
        }

        private decimal ate30Dias = 0;
        public decimal Ate30Dias
        {
            get { return ate30Dias; }
            set { ate30Dias = value; }
        }

        private decimal ate90Dias = 0;
        public decimal Ate90Dias
        {
            get { return ate90Dias; }
            set { ate90Dias = value; }
        }

        private decimal ate180Dias = 0;
        public decimal Ate180Dias
        {
            get { return ate180Dias; }
            set { ate180Dias = value; }
        }

        private decimal ate360Dias = 0;
        public decimal Ate360Dias
        {
            get { return ate360Dias; }
            set { ate360Dias = value; }
        }

        private decimal acima360Dias = 0;
        public decimal Acima360Dias
        {
            get { return acima360Dias; }
            set { acima360Dias = value; }
        }

        private decimal total = 0;
        public decimal Total
        {
            get { return ate30Dias + ate90Dias + ate180Dias + ate360Dias + acima360Dias; }
        }
    }

    private List<DetalheRelatorio> listaDetalhe = new List<DetalheRelatorio>();

    public List<DetalheRelatorio> ListaDetalhe
    {
        get { return listaDetalhe; }
    }

    private Dictionary<string, decimal> InitDictionaryTiposCarteira()
    {
        Dictionary<string, decimal> dic = new Dictionary<string, decimal>();
        foreach (string tipoCarteira in tiposCarteira)
        {
            dic.Add(tipoCarteira, 0);
        }
        return dic;
    }

    private string RetornaTipoCarteira(int idCliente)
    {
        string tipoCarteira = "";
        Cliente cliente = new Cliente();
        cliente.LoadByPrimaryKey(idCliente);

        Carteira carteira = new Carteira();
        carteira.LoadByPrimaryKey(idCliente);

        int idTipoCliente = cliente.IdTipo.Value;
        string fundoExclusivo = carteira.FundoExclusivo;

        if (fundoExclusivo == "S")
        {
            tipoCarteira = "Fundos Exclusivos";
        }
        else if (idTipoCliente == (int)TipoClienteFixo.CarteiraPropria)
        {
            tipoCarteira = "Carteira Própria";
        }
        else if (idTipoCliente == (int)TipoClienteFixo.Fundo)
        {
            tipoCarteira = "Fundos Abertos";
        }
        else
        {
            tipoCarteira = "Outros Fundos/Carteiras";
        }

        return tipoCarteira;
    }

    public RiscoPortopar(DateTime data, int? idGrupoEconomicoEspecifico, int? idCarteiraEspecifica)
    {
        List<DetalheRelatorio> listaDet = new List<DetalheRelatorio>();
        //

        string diretorio = DiretorioAplicacao.DiretorioBaseAplicacao + "/XMLCustom/PortoPar/CarteiraGrupoEconomico.xml";
        XmlDocument xml = new XmlDocument();
        xml.Load(@diretorio);

        #region Seleciona todos os nós
        XmlNodeList xnList = null; ;
        if (!idGrupoEconomicoEspecifico.HasValue && !idCarteiraEspecifica.HasValue)
        {

            xnList = xml.SelectNodes("/Lista/Carteira");
            //foreach (XmlNode xn in xnList) {
            //    string firstName = xn["FirstName"].InnerText;
            //    string lastName = xn["LastName"].InnerText;                
            //}
        }
        #endregion

        #region Seleciona somente a carteira especifica
        if (idCarteiraEspecifica.HasValue && !idGrupoEconomicoEspecifico.HasValue)
        {
            string search = string.Format("Lista/Carteira[@IdCarteira='{0}']", idCarteiraEspecifica.Value);
            xnList = xml.SelectNodes(search);
        }
        #endregion

        #region Seleciona somente o grupo especifico
        if (idGrupoEconomicoEspecifico.HasValue && !idCarteiraEspecifica.HasValue)
        {
            string search = string.Format("Lista/Carteira[@IdGrupoEconomico='{0}']", idGrupoEconomicoEspecifico.Value);
            xnList = xml.SelectNodes(search);
        }
        #endregion

        #region Seleciona carteira e grupo especifico
        if (idCarteiraEspecifica.HasValue && idGrupoEconomicoEspecifico.HasValue)
        {
            string search = string.Format("Lista/Carteira[@IdCarteira='{0}' and @IdGrupoEconomico='{1}']", idCarteiraEspecifica.Value, idGrupoEconomicoEspecifico.Value);
            xnList = xml.SelectNodes(search);
        }
        #endregion

        ClienteCollection clienteCollection = new ClienteCollection();

        /* Precisa ter esses campoas
            clienteQuery.IdCliente,
            clienteQuery.IdTipo,
            carteiraQuery.IdGrupoEconomico,                            
            carteiraQuery.FundoExclusivo,
            grupoEconomicoQuery.Nome,
            clienteQuery.Apelido
        */

        // Adiciona Colunas Extras
        clienteCollection.CreateColumnsForBinding();
        clienteCollection.AddColumn("IdGrupoEconomico", typeof(System.Int32));
        clienteCollection.AddColumn("FundoExclusivo", typeof(System.String));

        foreach (XmlNode xn in xnList)
        {
            int idCarteiraXML = Convert.ToInt32(xn.Attributes["IdCarteira"].InnerText.Trim());
            int idGrupoEconomicoXML = Convert.ToInt32(xn.Attributes["IdGrupoEconomico"].InnerText.Trim());

            Cliente cliente = clienteCollection.AddNew();
            //

            #region Cliente
            Cliente cliente1 = new Cliente();
            List<esQueryItem> campos = new List<esQueryItem>();
            campos.Add(cliente1.Query.IdTipo);
            campos.Add(cliente1.Query.Apelido);
            //
            cliente1.LoadByPrimaryKey(campos, idCarteiraXML);
            #endregion

            #region Carteira
            Carteira carteira = new Carteira();
            List<esQueryItem> campos1 = new List<esQueryItem>();
            campos1.Add(carteira.Query.FundoExclusivo);
            //
            carteira.LoadByPrimaryKey(campos1, idCarteiraXML);
            #endregion

            #region GrupoEconomico
            GrupoEconomico grupoEconomico = new GrupoEconomico();
            List<esQueryItem> campos2 = new List<esQueryItem>();
            campos2.Add(grupoEconomico.Query.Nome);

            grupoEconomico.LoadByPrimaryKey(campos2, idGrupoEconomicoXML);
            #endregion

            //
            cliente.IdCliente = idCarteiraXML;
            cliente.IdTipo = cliente1.IdTipo.Value;

            // Nome do grupo economico
            cliente.Nome = grupoEconomico.Nome.Trim();
            cliente.Apelido = cliente1.Apelido.Trim();
            cliente.SetColumn("IdGrupoEconomico", idGrupoEconomicoXML);
            cliente.SetColumn("FundoExclusivo", carteira.FundoExclusivo);
        }

        /* -------------------------------------------------------------------------------------- */
        DateTime dataProxima = Calendario.AdicionaDiaUtil(data, 1, (int)LocalFeriadoFixo.Bovespa, TipoFeriado.Brasil);
        DateTime dataAcoes = Calendario.AdicionaDiaUtil(dataProxima, 2, (int)LocalFeriadoFixo.Bovespa, TipoFeriado.Brasil);
        //
        foreach (Cliente cliente in clienteCollection)
        {
            int idCliente = cliente.IdCliente.Value;
            int idTipoCliente = cliente.IdTipo.Value;
            int idGrupoEconomico = Convert.ToInt32(cliente.GetColumn(CarteiraMetadata.ColumnNames.IdGrupoEconomico));
            string fundoExclusivo = Convert.ToString(cliente.GetColumn(CarteiraMetadata.ColumnNames.FundoExclusivo));
            string nomeGrupoEconomico = Convert.ToString(cliente.GetColumn(GrupoEconomicoMetadata.ColumnNames.Nome));

            string tipoCarteiraPrincipal = "";
            if (fundoExclusivo == "S")
            {
                tipoCarteiraPrincipal = "Fundos Exclusivos";
            }
            else if (idTipoCliente == (int)TipoClienteFixo.CarteiraPropria)
            {
                tipoCarteiraPrincipal = "Carteira Própria";
            }
            else if (idTipoCliente == (int)TipoClienteFixo.Fundo)
            {
                tipoCarteiraPrincipal = "Fundos Abertos";
            }
            else
            {
                tipoCarteiraPrincipal = "Outros Fundos/Carteiras";
            }

            Carteira carteira = new Carteira();
            Carteira.CarteiraDiaria carteiraDiaria = carteira.MontaCarteira(idCliente, data, true);

            Dictionary<string, decimal> valorAcoes = InitDictionaryTiposCarteira();
            Dictionary<string, decimal> valorOpcoes = InitDictionaryTiposCarteira();


            #region Titulos de Renda Variável (Ações, Opções)
            foreach (PosicaoBMF posicaoBMF in carteiraDiaria.PosicaoBMFOpcoesCollection)
            {
                string tipoCarteiraAtivo = RetornaTipoCarteira(posicaoBMF.IdCliente.Value);
                valorOpcoes[tipoCarteiraAtivo] += posicaoBMF.ValorMercado.Value;
            }
            foreach (PosicaoBolsa posicaoBolsa in carteiraDiaria.PosicaoBolsaOpcoesCollection)
            {
                string tipoCarteiraAtivo = RetornaTipoCarteira(posicaoBolsa.IdCliente.Value);
                valorOpcoes[tipoCarteiraAtivo] += posicaoBolsa.ValorMercado.Value;
            }

            foreach (PosicaoBolsa posicaoBolsa in carteiraDiaria.PosicaoBolsaCollection)
            {
                string tipoCarteiraAtivo = RetornaTipoCarteira(posicaoBolsa.IdCliente.Value);
                valorAcoes[tipoCarteiraAtivo] += posicaoBolsa.ValorMercado.Value;
            }

            foreach (string tipoCarteira in tiposCarteira)
            {
                DetalheRelatorio detalheRelatorio = new DetalheRelatorio();
                detalheRelatorio.IdGrupoEconomico = idGrupoEconomico;
                detalheRelatorio.NomeGrupoEconomico = nomeGrupoEconomico;
                detalheRelatorio.TipoCarteira = tipoCarteira;
                detalheRelatorio.GrupoTitulo = "Títulos de Renda Variável";
                detalheRelatorio.TipoTitulo = "Ações de companhias abertas";
                detalheRelatorio.Ate30Dias = Convert.ToInt64(valorAcoes[tipoCarteira]);
                if (detalheRelatorio.Ate30Dias > 0)
                {
                    listaDetalhe.Add(detalheRelatorio);
                }
            }

            foreach (string tipoCarteira in tiposCarteira)
            {
                DetalheRelatorio detalheRelatorio = new DetalheRelatorio();
                detalheRelatorio.IdGrupoEconomico = idGrupoEconomico;
                detalheRelatorio.NomeGrupoEconomico = nomeGrupoEconomico;
                detalheRelatorio.TipoCarteira = tipoCarteira;
                detalheRelatorio.GrupoTitulo = "Títulos de Renda Variável";
                detalheRelatorio.TipoTitulo = "Opções (Derivativos)";
                detalheRelatorio.Ate30Dias = Convert.ToInt64(valorOpcoes[tipoCarteira]);
                if (detalheRelatorio.Ate30Dias > 0)
                {
                    listaDetalhe.Add(detalheRelatorio);
                }
            }
            #endregion

            #region Emprestimo de Acoes
            Dictionary<string, decimal> valorEmprestimoAcoes_30Dias = InitDictionaryTiposCarteira();
            Dictionary<string, decimal> valorEmprestimoAcoes_90Dias = InitDictionaryTiposCarteira();
            Dictionary<string, decimal> valorEmprestimoAcoes_180Dias = InitDictionaryTiposCarteira();
            Dictionary<string, decimal> valorEmprestimoAcoes_360Dias = InitDictionaryTiposCarteira();
            Dictionary<string, decimal> valorEmprestimoAcoes_Acima = InitDictionaryTiposCarteira();

            foreach (PosicaoEmprestimoBolsa posicaoEmprestimoBolsa in carteiraDiaria.PosicaoEmprestimoBolsaCollection)
            {
                string tipoCarteiraAtivo = RetornaTipoCarteira(posicaoEmprestimoBolsa.IdCliente.Value);

                DateTime dataVencimento = posicaoEmprestimoBolsa.DataVencimento.Value;

                try
                {
                        int numeroDias = Calendario.NumeroDias(data, dataVencimento);

                        if (numeroDias <= 30)
                        {
                            valorEmprestimoAcoes_30Dias[tipoCarteiraAtivo] += posicaoEmprestimoBolsa.ValorMercado.Value;
                        }
                        else if (numeroDias <= 90)
                        {
                            valorEmprestimoAcoes_90Dias[tipoCarteiraAtivo] += posicaoEmprestimoBolsa.ValorMercado.Value;
                        }
                        else if (numeroDias <= 180)
                        {
                            valorEmprestimoAcoes_180Dias[tipoCarteiraAtivo] += posicaoEmprestimoBolsa.ValorMercado.Value;
                        }
                        else if (numeroDias <= 360)
                        {
                            valorEmprestimoAcoes_360Dias[tipoCarteiraAtivo] += posicaoEmprestimoBolsa.ValorMercado.Value;
                        }
                        else
                        {
                            valorEmprestimoAcoes_Acima[tipoCarteiraAtivo] += posicaoEmprestimoBolsa.ValorMercado.Value;
                        }
                    
                }
                catch (Exception)
                {
                    throw new Exception("Erro ao gerar exportação para para o cliente " + idCliente.ToString() + " - Emprestimo Bolsa com IdPosicao = " + posicaoEmprestimoBolsa.IdPosicao.ToString());
                }

            }

            foreach (string tipoCarteira in tiposCarteira)
            {
                DetalheRelatorio detalheRelatorio = new DetalheRelatorio();
                detalheRelatorio.IdGrupoEconomico = idGrupoEconomico;
                detalheRelatorio.NomeGrupoEconomico = nomeGrupoEconomico;
                detalheRelatorio.TipoCarteira = tipoCarteira;
                detalheRelatorio.GrupoTitulo = "Títulos de Renda Variável";
                detalheRelatorio.TipoTitulo = "Empréstimos de Ações";
                detalheRelatorio.Ate30Dias = Convert.ToInt64(valorEmprestimoAcoes_30Dias[tipoCarteira]);
                detalheRelatorio.Ate90Dias = Convert.ToInt64(valorEmprestimoAcoes_90Dias[tipoCarteira]);
                detalheRelatorio.Ate180Dias = Convert.ToInt64(valorEmprestimoAcoes_180Dias[tipoCarteira]);
                detalheRelatorio.Ate360Dias = Convert.ToInt64(valorEmprestimoAcoes_360Dias[tipoCarteira]);
                detalheRelatorio.Acima360Dias = Convert.ToInt64(valorEmprestimoAcoes_Acima[tipoCarteira]);
                if ((detalheRelatorio.Ate30Dias + detalheRelatorio.Ate90Dias + detalheRelatorio.Ate180Dias +
                    detalheRelatorio.Ate360Dias + detalheRelatorio.Acima360Dias) > 0)
                {
                    listaDetalhe.Add(detalheRelatorio);
                }
            }
            #endregion

            #region Titulos de Renda Fixa Privados em todos os vencimentos
            Dictionary<string, decimal> valorTitulosPrivados_30Dias = InitDictionaryTiposCarteira();
            Dictionary<string, decimal> valorTitulosPrivados_90Dias = InitDictionaryTiposCarteira();
            Dictionary<string, decimal> valorTitulosPrivados_180Dias = InitDictionaryTiposCarteira();
            Dictionary<string, decimal> valorTitulosPrivados_360Dias = InitDictionaryTiposCarteira();
            Dictionary<string, decimal> valorTitulosPrivados_Acima = InitDictionaryTiposCarteira();

            foreach (PosicaoRendaFixa posicaoRendaFixa in carteiraDiaria.PosicaoRendaFixaPrivadoCollection)
            {
                string tipoCarteiraAtivo = RetornaTipoCarteira(posicaoRendaFixa.IdCliente.Value);

                DateTime dataVencimento = posicaoRendaFixa.DataVencimento.Value;

                try
                {
                    if (posicaoRendaFixa.TipoOperacao != (byte)TipoOperacaoTitulo.CompraRevenda)
                    {
                        int numeroDias = Calendario.NumeroDias(data, dataVencimento);

                        if (numeroDias <= 30)
                        {
                            valorTitulosPrivados_30Dias[tipoCarteiraAtivo] += posicaoRendaFixa.ValorMercado.Value;
                        }
                        else if (numeroDias <= 90)
                        {
                            valorTitulosPrivados_90Dias[tipoCarteiraAtivo] += posicaoRendaFixa.ValorMercado.Value;
                        }
                        else if (numeroDias <= 180)
                        {
                            valorTitulosPrivados_180Dias[tipoCarteiraAtivo] += posicaoRendaFixa.ValorMercado.Value;
                        }
                        else if (numeroDias <= 360)
                        {
                            valorTitulosPrivados_360Dias[tipoCarteiraAtivo] += posicaoRendaFixa.ValorMercado.Value;
                        }
                        else
                        {
                            valorTitulosPrivados_Acima[tipoCarteiraAtivo] += posicaoRendaFixa.ValorMercado.Value;
                        }
                    }
                }
                catch (Exception)
                {
                    throw new Exception("Erro ao gerar exportação para para o cliente " + idCliente.ToString() + " - Renda Fixa privada com IdPosicao = " + posicaoRendaFixa.IdPosicao.ToString());
                }

            }

            foreach (string tipoCarteira in tiposCarteira)
            {
                DetalheRelatorio detalheRelatorio = new DetalheRelatorio();
                detalheRelatorio.IdGrupoEconomico = idGrupoEconomico;
                detalheRelatorio.NomeGrupoEconomico = nomeGrupoEconomico;
                detalheRelatorio.TipoCarteira = tipoCarteira;
                detalheRelatorio.GrupoTitulo = "Títulos de Renda Fixa";
                detalheRelatorio.TipoTitulo = "Títulos Privados";
                detalheRelatorio.Ate30Dias = Convert.ToInt64(valorTitulosPrivados_30Dias[tipoCarteira]);
                detalheRelatorio.Ate90Dias = Convert.ToInt64(valorTitulosPrivados_90Dias[tipoCarteira]);
                detalheRelatorio.Ate180Dias = Convert.ToInt64(valorTitulosPrivados_180Dias[tipoCarteira]);
                detalheRelatorio.Ate360Dias = Convert.ToInt64(valorTitulosPrivados_360Dias[tipoCarteira]);
                detalheRelatorio.Acima360Dias = Convert.ToInt64(valorTitulosPrivados_Acima[tipoCarteira]);
                if ((detalheRelatorio.Ate30Dias + detalheRelatorio.Ate90Dias + detalheRelatorio.Ate180Dias +
                    detalheRelatorio.Ate360Dias + detalheRelatorio.Acima360Dias) > 0)
                {
                    listaDetalhe.Add(detalheRelatorio);
                }
            }
            #endregion

            #region Titulos de Renda Fixa Públicos em todos os vencimentos

            Dictionary<string, decimal> valorLFT_30Dias = InitDictionaryTiposCarteira();
            Dictionary<string, decimal> valorLFT_90Dias = InitDictionaryTiposCarteira();
            Dictionary<string, decimal> valorLFT_180Dias = InitDictionaryTiposCarteira();
            Dictionary<string, decimal> valorLFT_360Dias = InitDictionaryTiposCarteira();
            Dictionary<string, decimal> valorLFT_Acima = InitDictionaryTiposCarteira();

            Dictionary<string, decimal> valorLTN_30Dias = InitDictionaryTiposCarteira();
            Dictionary<string, decimal> valorLTN_90Dias = InitDictionaryTiposCarteira();
            Dictionary<string, decimal> valorLTN_180Dias = InitDictionaryTiposCarteira();
            Dictionary<string, decimal> valorLTN_360Dias = InitDictionaryTiposCarteira();
            Dictionary<string, decimal> valorLTN_Acima = InitDictionaryTiposCarteira();

            Dictionary<string, decimal> valorNTNB_30Dias = InitDictionaryTiposCarteira();
            Dictionary<string, decimal> valorNTNB_90Dias = InitDictionaryTiposCarteira();
            Dictionary<string, decimal> valorNTNB_180Dias = InitDictionaryTiposCarteira();
            Dictionary<string, decimal> valorNTNB_360Dias = InitDictionaryTiposCarteira();
            Dictionary<string, decimal> valorNTNB_Acima = InitDictionaryTiposCarteira();

            Dictionary<string, decimal> valorNTNC_30Dias = InitDictionaryTiposCarteira();
            Dictionary<string, decimal> valorNTNC_90Dias = InitDictionaryTiposCarteira();
            Dictionary<string, decimal> valorNTNC_180Dias = InitDictionaryTiposCarteira();
            Dictionary<string, decimal> valorNTNC_360Dias = InitDictionaryTiposCarteira();
            Dictionary<string, decimal> valorNTNC_Acima = InitDictionaryTiposCarteira();

            Dictionary<string, decimal> valorNTNF_30Dias = InitDictionaryTiposCarteira();
            Dictionary<string, decimal> valorNTNF_90Dias = InitDictionaryTiposCarteira();
            Dictionary<string, decimal> valorNTNF_180Dias = InitDictionaryTiposCarteira();
            Dictionary<string, decimal> valorNTNF_360Dias = InitDictionaryTiposCarteira();
            Dictionary<string, decimal> valorNTNF_Acima = InitDictionaryTiposCarteira();

            foreach (PosicaoRendaFixa posicaoRendaFixa in carteiraDiaria.PosicaoRendaFixaPublicoCollection)
            {
                DateTime dataVencimento = posicaoRendaFixa.DataVencimento.Value;
                string tipoCarteiraAtivo = RetornaTipoCarteira(posicaoRendaFixa.IdCliente.Value);

                try
                {
                    if (posicaoRendaFixa.TipoOperacao != (byte)TipoOperacaoTitulo.CompraRevenda)
                    {
                        int numeroDias = Calendario.NumeroDias(data, dataVencimento);

                        TituloRendaFixa tituloRendaFixa = new TituloRendaFixa();
                        tituloRendaFixa.Query.Select(tituloRendaFixa.Query.IdPapel,
                                                     tituloRendaFixa.Query.Descricao)
                                       .Where(tituloRendaFixa.Query.IdTitulo == posicaoRendaFixa.IdTitulo.Value);
                        tituloRendaFixa.Query.Load();

                        PapelRendaFixa papelRendaFixa = new PapelRendaFixa();
                        papelRendaFixa.LoadByPrimaryKey(tituloRendaFixa.IdPapel.Value);

                        if (papelRendaFixa.Classe.Value == (int)ClasseRendaFixa.LFT)
                        {
                            #region LFT
                            if (numeroDias <= 30)
                            {
                                valorLFT_30Dias[tipoCarteiraAtivo] += posicaoRendaFixa.ValorMercado.Value;
                            }
                            else if (numeroDias <= 90)
                            {
                                valorLFT_90Dias[tipoCarteiraAtivo] += posicaoRendaFixa.ValorMercado.Value;
                            }
                            else if (numeroDias <= 180)
                            {
                                valorLFT_180Dias[tipoCarteiraAtivo] += posicaoRendaFixa.ValorMercado.Value;
                            }
                            else if (numeroDias <= 360)
                            {
                                valorLFT_360Dias[tipoCarteiraAtivo] += posicaoRendaFixa.ValorMercado.Value;
                            }
                            else
                            {
                                valorLFT_Acima[tipoCarteiraAtivo] += posicaoRendaFixa.ValorMercado.Value;
                            }
                            #endregion
                        }
                        else if (papelRendaFixa.Classe.Value == (int)ClasseRendaFixa.LTN)
                        {
                            #region LTN
                            if (numeroDias <= 30)
                            {
                                valorLTN_30Dias[tipoCarteiraAtivo] += posicaoRendaFixa.ValorMercado.Value;
                            }
                            else if (numeroDias <= 90)
                            {
                                valorLTN_90Dias[tipoCarteiraAtivo] += posicaoRendaFixa.ValorMercado.Value;
                            }
                            else if (numeroDias <= 180)
                            {
                                valorLTN_180Dias[tipoCarteiraAtivo] += posicaoRendaFixa.ValorMercado.Value;
                            }
                            else if (numeroDias <= 360)
                            {
                                valorLTN_360Dias[tipoCarteiraAtivo] += posicaoRendaFixa.ValorMercado.Value;
                            }
                            else
                            {
                                valorLTN_Acima[tipoCarteiraAtivo] += posicaoRendaFixa.ValorMercado.Value;
                            }
                            #endregion
                        }
                        else if (papelRendaFixa.Classe.Value == (int)ClasseRendaFixa.NTN)
                        {
                            if (tituloRendaFixa.Descricao.Contains("B"))
                            {
                                #region NTN-B
                                if (numeroDias <= 30)
                                {
                                    valorNTNB_30Dias[tipoCarteiraAtivo] += posicaoRendaFixa.ValorMercado.Value;
                                }
                                else if (numeroDias <= 90)
                                {
                                    valorNTNB_90Dias[tipoCarteiraAtivo] += posicaoRendaFixa.ValorMercado.Value;
                                }
                                else if (numeroDias <= 180)
                                {
                                    valorNTNB_180Dias[tipoCarteiraAtivo] += posicaoRendaFixa.ValorMercado.Value;
                                }
                                else if (numeroDias <= 360)
                                {
                                    valorNTNB_360Dias[tipoCarteiraAtivo] += posicaoRendaFixa.ValorMercado.Value;
                                }
                                else
                                {
                                    valorNTNB_Acima[tipoCarteiraAtivo] += posicaoRendaFixa.ValorMercado.Value;
                                }
                                #endregion
                            }
                            else if (tituloRendaFixa.Descricao.Contains("NTN-C"))
                            {
                                #region NTN-C
                                if (numeroDias <= 30)
                                {
                                    valorNTNC_30Dias[tipoCarteiraAtivo] += posicaoRendaFixa.ValorMercado.Value;
                                }
                                else if (numeroDias <= 90)
                                {
                                    valorNTNC_90Dias[tipoCarteiraAtivo] += posicaoRendaFixa.ValorMercado.Value;
                                }
                                else if (numeroDias <= 180)
                                {
                                    valorNTNC_180Dias[tipoCarteiraAtivo] += posicaoRendaFixa.ValorMercado.Value;
                                }
                                else if (numeroDias <= 360)
                                {
                                    valorNTNC_360Dias[tipoCarteiraAtivo] += posicaoRendaFixa.ValorMercado.Value;
                                }
                                else
                                {
                                    valorNTNC_Acima[tipoCarteiraAtivo] += posicaoRendaFixa.ValorMercado.Value;
                                }
                                #endregion
                            }
                            else if (tituloRendaFixa.Descricao.Contains("F"))
                            {
                                #region NTN-F
                                if (numeroDias <= 30)
                                {
                                    valorNTNF_30Dias[tipoCarteiraAtivo] += posicaoRendaFixa.ValorMercado.Value;
                                }
                                else if (numeroDias <= 90)
                                {
                                    valorNTNF_90Dias[tipoCarteiraAtivo] += posicaoRendaFixa.ValorMercado.Value;
                                }
                                else if (numeroDias <= 180)
                                {
                                    valorNTNF_180Dias[tipoCarteiraAtivo] += posicaoRendaFixa.ValorMercado.Value;
                                }
                                else if (numeroDias <= 360)
                                {
                                    valorNTNF_360Dias[tipoCarteiraAtivo] += posicaoRendaFixa.ValorMercado.Value;
                                }
                                else
                                {
                                    valorNTNF_Acima[tipoCarteiraAtivo] += posicaoRendaFixa.ValorMercado.Value;
                                }
                                #endregion
                            }
                        }
                    }
                }
                catch (Exception e)
                {
                    throw new Exception("Erro ao gerar exportação para para o cliente " + idCliente.ToString() + " - Renda Fixa pública IdPosicao = " + posicaoRendaFixa.IdPosicao.Value.ToString());
                }

            }

            foreach (string tipoCarteira in tiposCarteira)
            {
                DetalheRelatorio detalheRelatorio = new DetalheRelatorio();
                detalheRelatorio.IdGrupoEconomico = idGrupoEconomico;
                detalheRelatorio.NomeGrupoEconomico = nomeGrupoEconomico;
                detalheRelatorio.TipoCarteira = tipoCarteira;
                detalheRelatorio.GrupoTitulo = "Títulos de Renda Fixa";
                detalheRelatorio.TipoTitulo = "Letras Financeiras do Tesouro";
                detalheRelatorio.Ate30Dias = Convert.ToInt64(valorLFT_30Dias[tipoCarteira]);
                detalheRelatorio.Ate90Dias = Convert.ToInt64(valorLFT_90Dias[tipoCarteira]);
                detalheRelatorio.Ate180Dias = Convert.ToInt64(valorLFT_180Dias[tipoCarteira]);
                detalheRelatorio.Ate360Dias = Convert.ToInt64(valorLFT_360Dias[tipoCarteira]);
                detalheRelatorio.Acima360Dias = Convert.ToInt64(valorLFT_Acima[tipoCarteira]);
                if ((detalheRelatorio.Ate30Dias + detalheRelatorio.Ate90Dias + detalheRelatorio.Ate180Dias +
                    detalheRelatorio.Ate360Dias + detalheRelatorio.Acima360Dias) > 0)
                {
                    listaDetalhe.Add(detalheRelatorio);
                }
            }

            foreach (string tipoCarteira in tiposCarteira)
            {
                DetalheRelatorio detalheRelatorio = new DetalheRelatorio();
                detalheRelatorio.IdGrupoEconomico = idGrupoEconomico;
                detalheRelatorio.NomeGrupoEconomico = nomeGrupoEconomico;
                detalheRelatorio.TipoCarteira = tipoCarteira;
                detalheRelatorio.GrupoTitulo = "Títulos de Renda Fixa";
                detalheRelatorio.TipoTitulo = "Letras do Tesouro Nacional";
                detalheRelatorio.Ate30Dias = Convert.ToInt64(valorLTN_30Dias[tipoCarteira]);
                detalheRelatorio.Ate90Dias = Convert.ToInt64(valorLTN_90Dias[tipoCarteira]);
                detalheRelatorio.Ate180Dias = Convert.ToInt64(valorLTN_180Dias[tipoCarteira]);
                detalheRelatorio.Ate360Dias = Convert.ToInt64(valorLTN_360Dias[tipoCarteira]);
                detalheRelatorio.Acima360Dias = Convert.ToInt64(valorLTN_Acima[tipoCarteira]);
                if ((detalheRelatorio.Ate30Dias + detalheRelatorio.Ate90Dias + detalheRelatorio.Ate180Dias +
                    detalheRelatorio.Ate360Dias + detalheRelatorio.Acima360Dias) > 0)
                {
                    listaDetalhe.Add(detalheRelatorio);
                }
            }

            foreach (string tipoCarteira in tiposCarteira)
            {
                DetalheRelatorio detalheRelatorio = new DetalheRelatorio();
                detalheRelatorio.IdGrupoEconomico = idGrupoEconomico;
                detalheRelatorio.NomeGrupoEconomico = nomeGrupoEconomico;
                detalheRelatorio.TipoCarteira = tipoCarteira;
                detalheRelatorio.GrupoTitulo = "Títulos de Renda Fixa";
                detalheRelatorio.TipoTitulo = "Notas do Tesouro Nacional - Série B";
                detalheRelatorio.Ate30Dias = Convert.ToInt64(valorNTNB_30Dias[tipoCarteira]);
                detalheRelatorio.Ate90Dias = Convert.ToInt64(valorNTNB_90Dias[tipoCarteira]);
                detalheRelatorio.Ate180Dias = Convert.ToInt64(valorNTNB_180Dias[tipoCarteira]);
                detalheRelatorio.Ate360Dias = Convert.ToInt64(valorNTNB_360Dias[tipoCarteira]);
                detalheRelatorio.Acima360Dias = Convert.ToInt64(valorNTNB_Acima[tipoCarteira]);
                if ((detalheRelatorio.Ate30Dias + detalheRelatorio.Ate90Dias + detalheRelatorio.Ate180Dias +
                    detalheRelatorio.Ate360Dias + detalheRelatorio.Acima360Dias) > 0)
                {
                    listaDetalhe.Add(detalheRelatorio);
                }
            }

            foreach (string tipoCarteira in tiposCarteira)
            {
                DetalheRelatorio detalheRelatorio = new DetalheRelatorio();
                detalheRelatorio.IdGrupoEconomico = idGrupoEconomico;
                detalheRelatorio.NomeGrupoEconomico = nomeGrupoEconomico;
                detalheRelatorio.TipoCarteira = tipoCarteira;
                detalheRelatorio.GrupoTitulo = "Títulos de Renda Fixa";
                detalheRelatorio.TipoTitulo = "Notas do Tesouro Nacional - Série C";
                detalheRelatorio.Ate30Dias = Convert.ToInt64(valorNTNC_30Dias[tipoCarteira]);
                detalheRelatorio.Ate90Dias = Convert.ToInt64(valorNTNC_90Dias[tipoCarteira]);
                detalheRelatorio.Ate180Dias = Convert.ToInt64(valorNTNC_180Dias[tipoCarteira]);
                detalheRelatorio.Ate360Dias = Convert.ToInt64(valorNTNC_360Dias[tipoCarteira]);
                detalheRelatorio.Acima360Dias = Convert.ToInt64(valorNTNC_Acima[tipoCarteira]);
                if ((detalheRelatorio.Ate30Dias + detalheRelatorio.Ate90Dias + detalheRelatorio.Ate180Dias +
                    detalheRelatorio.Ate360Dias + detalheRelatorio.Acima360Dias) > 0)
                {
                    listaDetalhe.Add(detalheRelatorio);
                }
            }

            foreach (string tipoCarteira in tiposCarteira)
            {
                DetalheRelatorio detalheRelatorio = new DetalheRelatorio();
                detalheRelatorio.IdGrupoEconomico = idGrupoEconomico;
                detalheRelatorio.NomeGrupoEconomico = nomeGrupoEconomico;
                detalheRelatorio.TipoCarteira = tipoCarteira;
                detalheRelatorio.GrupoTitulo = "Títulos de Renda Fixa";
                detalheRelatorio.TipoTitulo = "Notas do Tesouro Nacional - Série F";
                detalheRelatorio.Ate30Dias = Convert.ToInt64(valorNTNF_30Dias[tipoCarteira]);
                detalheRelatorio.Ate90Dias = Convert.ToInt64(valorNTNF_90Dias[tipoCarteira]);
                detalheRelatorio.Ate180Dias = Convert.ToInt64(valorNTNF_180Dias[tipoCarteira]);
                detalheRelatorio.Ate360Dias = Convert.ToInt64(valorNTNF_360Dias[tipoCarteira]);
                detalheRelatorio.Acima360Dias = Convert.ToInt64(valorNTNF_Acima[tipoCarteira]);
                if ((detalheRelatorio.Ate30Dias + detalheRelatorio.Ate90Dias + detalheRelatorio.Ate180Dias +
                    detalheRelatorio.Ate360Dias + detalheRelatorio.Acima360Dias) > 0)
                {
                    listaDetalhe.Add(detalheRelatorio);
                }
            }
            #endregion

            #region Titulos de Renda Fixa Compromissados em todos os vencimentos
            Dictionary<string, decimal> valorTitulosCompromisso_30Dias = InitDictionaryTiposCarteira();
            Dictionary<string, decimal> valorTitulosCompromisso_90Dias = InitDictionaryTiposCarteira();
            Dictionary<string, decimal> valorTitulosCompromisso_180Dias = InitDictionaryTiposCarteira();
            Dictionary<string, decimal> valorTitulosCompromisso_360Dias = InitDictionaryTiposCarteira();
            Dictionary<string, decimal> valorTitulosCompromisso_Acima = InitDictionaryTiposCarteira();

            foreach (PosicaoRendaFixa posicaoRendaFixa in carteiraDiaria.PosicaoRendaFixaPrivadoCollection)
            {
                string tipoCarteiraAtivo = RetornaTipoCarteira(posicaoRendaFixa.IdCliente.Value);

                if (posicaoRendaFixa.TipoOperacao == (byte)TipoOperacaoTitulo.CompraRevenda)
                {
                    DateTime dataVencimento = posicaoRendaFixa.DataVolta.Value;
                    int numeroDias;
                    try
                    {
                        numeroDias = Calendario.NumeroDias(data, dataVencimento);
                    }
                    catch (Exception e)
                    {
                        throw new Exception("Erro ao processar PosicaoRendaFixa: " + posicaoRendaFixa.IdPosicao.Value);
                    }

                    if (numeroDias <= 30)
                    {
                        valorTitulosCompromisso_30Dias[tipoCarteiraAtivo] += posicaoRendaFixa.ValorMercado.Value;
                    }
                    else if (numeroDias <= 90)
                    {
                        valorTitulosCompromisso_90Dias[tipoCarteiraAtivo] += posicaoRendaFixa.ValorMercado.Value;
                    }
                    else if (numeroDias <= 180)
                    {
                        valorTitulosCompromisso_180Dias[tipoCarteiraAtivo] += posicaoRendaFixa.ValorMercado.Value;
                    }
                    else if (numeroDias <= 360)
                    {
                        valorTitulosCompromisso_360Dias[tipoCarteiraAtivo] += posicaoRendaFixa.ValorMercado.Value;
                    }
                    else
                    {
                        valorTitulosCompromisso_Acima[tipoCarteiraAtivo] += posicaoRendaFixa.ValorMercado.Value;
                    }
                }
            }

            foreach (PosicaoRendaFixa posicaoRendaFixa in carteiraDiaria.PosicaoRendaFixaPublicoCollection)
            {
                string tipoCarteiraAtivo = RetornaTipoCarteira(posicaoRendaFixa.IdCliente.Value);

                if (posicaoRendaFixa.TipoOperacao == (byte)TipoOperacaoTitulo.CompraRevenda)
                {
                    DateTime dataVencimento = posicaoRendaFixa.DataVolta.Value;
                    int numeroDias;
                    try
                    {
                        numeroDias = Calendario.NumeroDias(data, dataVencimento, (int)LocalFeriadoFixo.Brasil, TipoFeriado.Brasil);
                    }
                    catch (Exception e)
                    {
                        throw new Exception("Erro ao processar PosicaoRendaFixa: " + posicaoRendaFixa.IdPosicao.Value);
                    }


                    if (numeroDias <= 30)
                    {
                        valorTitulosCompromisso_30Dias[tipoCarteiraAtivo] += posicaoRendaFixa.ValorMercado.Value;
                    }
                    else if (numeroDias <= 90)
                    {
                        valorTitulosCompromisso_90Dias[tipoCarteiraAtivo] += posicaoRendaFixa.ValorMercado.Value;
                    }
                    else if (numeroDias <= 180)
                    {
                        valorTitulosCompromisso_180Dias[tipoCarteiraAtivo] += posicaoRendaFixa.ValorMercado.Value;
                    }
                    else if (numeroDias <= 360)
                    {
                        valorTitulosCompromisso_360Dias[tipoCarteiraAtivo] += posicaoRendaFixa.ValorMercado.Value;
                    }
                    else
                    {
                        valorTitulosCompromisso_Acima[tipoCarteiraAtivo] += posicaoRendaFixa.ValorMercado.Value;
                    }
                }
            }

            foreach (string tipoCarteira in tiposCarteira)
            {
                DetalheRelatorio detalheRelatorio = new DetalheRelatorio();
                detalheRelatorio.IdGrupoEconomico = idGrupoEconomico;
                detalheRelatorio.NomeGrupoEconomico = nomeGrupoEconomico;
                detalheRelatorio.TipoCarteira = tipoCarteira;
                detalheRelatorio.GrupoTitulo = "Títulos de Renda Fixa";
                detalheRelatorio.TipoTitulo = "Over (Compromissadas)";
                detalheRelatorio.Ate30Dias = Convert.ToInt64(valorTitulosCompromisso_30Dias[tipoCarteira]);
                detalheRelatorio.Ate90Dias = Convert.ToInt64(valorTitulosCompromisso_90Dias[tipoCarteira]);
                detalheRelatorio.Ate180Dias = Convert.ToInt64(valorTitulosCompromisso_180Dias[tipoCarteira]);
                detalheRelatorio.Ate360Dias = Convert.ToInt64(valorTitulosCompromisso_360Dias[tipoCarteira]);
                detalheRelatorio.Acima360Dias = Convert.ToInt64(valorTitulosCompromisso_Acima[tipoCarteira]);
                if ((detalheRelatorio.Ate30Dias + detalheRelatorio.Ate90Dias + detalheRelatorio.Ate180Dias +
                    detalheRelatorio.Ate360Dias + detalheRelatorio.Acima360Dias) > 0)
                {
                    listaDetalhe.Add(detalheRelatorio);
                }
            }
            #endregion

            #region Cotas de fundos em todos os periodos de liquidação
            Dictionary<string, decimal> valorFundosRF_30Dias = InitDictionaryTiposCarteira();
            Dictionary<string, decimal> valorFundosRF_90Dias = InitDictionaryTiposCarteira();
            Dictionary<string, decimal> valorFundosRF_180Dias = InitDictionaryTiposCarteira();
            Dictionary<string, decimal> valorFundosRF_360Dias = InitDictionaryTiposCarteira();
            Dictionary<string, decimal> valorFundosRF_Acima = InitDictionaryTiposCarteira();

            Dictionary<string, decimal> valorFundosRV_30Dias = InitDictionaryTiposCarteira();
            Dictionary<string, decimal> valorFundosRV_90Dias = InitDictionaryTiposCarteira();
            Dictionary<string, decimal> valorFundosRV_180Dias = InitDictionaryTiposCarteira();
            Dictionary<string, decimal> valorFundosRV_360Dias = InitDictionaryTiposCarteira();
            Dictionary<string, decimal> valorFundosRV_Acima = InitDictionaryTiposCarteira();

            /*decimal valorFundosRF_30Dias = 0;
            decimal valorFundosRF_90Dias = 0;
            decimal valorFundosRF_180Dias = 0;
            decimal valorFundosRF_360Dias = 0;
            decimal valorFundosRF_Acima = 0;

            decimal valorFundosRV_30Dias = 0;
            decimal valorFundosRV_90Dias = 0;
            decimal valorFundosRV_180Dias = 0;
            decimal valorFundosRV_360Dias = 0;
            decimal valorFundosRV_Acima = 0;*/
            foreach (PosicaoFundo posicaoFundo in carteiraDiaria.PosicaoFundoCollection)
            {
                int idFundo = posicaoFundo.IdCarteira.Value;
                string tipoCarteiraAtivo = RetornaTipoCarteira(posicaoFundo.IdCarteira.Value);

                try
                {
                    Carteira carteiraFundo = new Carteira();
                    carteiraFundo.Query.Select(carteiraFundo.Query.TipoCarteira,
                                               carteiraFundo.Query.ContagemDiasConversaoResgate,
                                               carteiraFundo.Query.DiasCotizacaoResgate,
                                               carteiraFundo.Query.DiasLiquidacaoResgate);
                    carteiraFundo.Query.Where(carteiraFundo.Query.IdCarteira.Equal(idFundo));
                    carteiraFundo.Query.Load();

                    int numeroDias = 0;
                    if (carteiraFundo.ContagemDiasConversaoResgate.Value == (byte)ContagemDiasLiquidacaoResgate.DiasUteis)
                    {
                        numeroDias = carteiraFundo.DiasLiquidacaoResgate.Value;
                    }
                    else
                    {
                        DateTime dataCotizacao = data.AddDays((double)carteiraFundo.DiasCotizacaoResgate.Value);
                        if (!Calendario.IsDiaUtil(dataCotizacao))
                        {
                            dataCotizacao = Calendario.AdicionaDiaUtil(dataCotizacao, 1);
                        }
                        DateTime dataLiquidacao = Calendario.AdicionaDiaUtil(dataCotizacao, (int)carteiraFundo.DiasLiquidacaoResgate.Value);

                        try
                        {
                            numeroDias = Calendario.NumeroDias(data, dataLiquidacao);
                        }
                        catch (Exception e)
                        {
                            throw new Exception("Erro ao processar PosicaoFundo: " + posicaoFundo.IdPosicao.Value);
                        }


                    }

                    if (carteiraFundo.TipoCarteira.Value == (byte)TipoCarteiraFundo.RendaFixa)
                    {
                        #region Fundos de Renda Fixa
                        if (numeroDias <= 30)
                        {
                            valorFundosRF_30Dias[tipoCarteiraAtivo] += posicaoFundo.ValorBruto.Value;
                        }
                        else if (numeroDias <= 90)
                        {
                            valorFundosRF_90Dias[tipoCarteiraAtivo] += posicaoFundo.ValorBruto.Value;
                        }
                        else if (numeroDias <= 180)
                        {
                            valorFundosRF_180Dias[tipoCarteiraAtivo] += posicaoFundo.ValorBruto.Value;
                        }
                        else if (numeroDias <= 360)
                        {
                            valorFundosRF_360Dias[tipoCarteiraAtivo] += posicaoFundo.ValorBruto.Value;
                        }
                        else
                        {
                            valorFundosRF_Acima[tipoCarteiraAtivo] += posicaoFundo.ValorBruto.Value;
                        }
                        #endregion
                    }
                    else
                    {
                        #region Fundos de Renda Variavel
                        if (numeroDias <= 30)
                        {
                            valorFundosRV_30Dias[tipoCarteiraAtivo] += posicaoFundo.ValorBruto.Value;
                        }
                        else if (numeroDias <= 90)
                        {
                            valorFundosRV_90Dias[tipoCarteiraAtivo] += posicaoFundo.ValorBruto.Value;
                        }
                        else if (numeroDias <= 180)
                        {
                            valorFundosRV_180Dias[tipoCarteiraAtivo] += posicaoFundo.ValorBruto.Value;
                        }
                        else if (numeroDias <= 360)
                        {
                            valorFundosRV_360Dias[tipoCarteiraAtivo] += posicaoFundo.ValorBruto.Value;
                        }
                        else
                        {
                            valorFundosRV_Acima[tipoCarteiraAtivo] += posicaoFundo.ValorBruto.Value;
                        }
                        #endregion
                    }
                }
                catch (Exception e)
                {
                    throw new Exception("Erro ao gerar exportação para o fundo " + idFundo.ToString() + " para o cliente " + idCliente.ToString());
                }

            }

            foreach (string tipoCarteira in tiposCarteira)
            {
                DetalheRelatorio detalheRelatorio = new DetalheRelatorio();
                detalheRelatorio.IdGrupoEconomico = idGrupoEconomico;
                detalheRelatorio.NomeGrupoEconomico = nomeGrupoEconomico;
                detalheRelatorio.TipoCarteira = tipoCarteira;
                detalheRelatorio.GrupoTitulo = "Títulos de Renda Fixa";
                detalheRelatorio.TipoTitulo = "Cotas de Fundos";
                detalheRelatorio.Ate30Dias = Convert.ToInt64(valorFundosRF_30Dias[tipoCarteira]);
                detalheRelatorio.Ate90Dias = Convert.ToInt64(valorFundosRF_90Dias[tipoCarteira]);
                detalheRelatorio.Ate180Dias = Convert.ToInt64(valorFundosRF_180Dias[tipoCarteira]);
                detalheRelatorio.Ate360Dias = Convert.ToInt64(valorFundosRF_360Dias[tipoCarteira]);
                detalheRelatorio.Acima360Dias = Convert.ToInt64(valorFundosRF_Acima[tipoCarteira]);
                if ((detalheRelatorio.Ate30Dias + detalheRelatorio.Ate90Dias + detalheRelatorio.Ate180Dias +
                    detalheRelatorio.Ate360Dias + detalheRelatorio.Acima360Dias) > 0)
                {
                    listaDetalhe.Add(detalheRelatorio);
                }
            }

            foreach (string tipoCarteira in tiposCarteira)
            {
                DetalheRelatorio detalheRelatorio = new DetalheRelatorio();
                detalheRelatorio.IdGrupoEconomico = idGrupoEconomico;
                detalheRelatorio.NomeGrupoEconomico = nomeGrupoEconomico;
                detalheRelatorio.TipoCarteira = tipoCarteira;
                detalheRelatorio.GrupoTitulo = "Títulos de Renda Variável";
                detalheRelatorio.TipoTitulo = "Cotas de Fundos";
                detalheRelatorio.Ate30Dias = Convert.ToInt64(valorFundosRV_30Dias[tipoCarteira]);
                detalheRelatorio.Ate90Dias = Convert.ToInt64(valorFundosRV_90Dias[tipoCarteira]);
                detalheRelatorio.Ate180Dias = Convert.ToInt64(valorFundosRV_180Dias[tipoCarteira]);
                detalheRelatorio.Ate360Dias = Convert.ToInt64(valorFundosRV_360Dias[tipoCarteira]);
                detalheRelatorio.Acima360Dias = Convert.ToInt64(valorFundosRV_Acima[tipoCarteira]);
                if ((detalheRelatorio.Ate30Dias + detalheRelatorio.Ate90Dias + detalheRelatorio.Ate180Dias +
                    detalheRelatorio.Ate360Dias + detalheRelatorio.Acima360Dias) > 0)
                {
                    listaDetalhe.Add(detalheRelatorio);
                }
            }


            #endregion

            #region Outros Ativos (Swap, Liquidacao e SaldCaixa)

            Dictionary<string, decimal> valorOutros_30Dias = InitDictionaryTiposCarteira();
            Dictionary<string, decimal> valorOutros_90Dias = InitDictionaryTiposCarteira();
            Dictionary<string, decimal> valorOutros_180Dias = InitDictionaryTiposCarteira();
            Dictionary<string, decimal> valorOutros_360Dias = InitDictionaryTiposCarteira();
            Dictionary<string, decimal> valorOutros_Acima = InitDictionaryTiposCarteira();

            foreach (PosicaoSwap posicaoSwap in carteiraDiaria.PosicaoSwapCollection)
            {
                string tipoCarteiraAtivo = RetornaTipoCarteira(posicaoSwap.IdCliente.Value);

                DateTime dataVencimento = posicaoSwap.DataVencimento.Value;
                int numeroDias;
                try
                {
                    numeroDias = Calendario.NumeroDias(data, dataVencimento, (int)LocalFeriadoFixo.Brasil, TipoFeriado.Brasil);
                }
                catch (Exception e)
                {
                    throw new Exception("Erro ao processar Liquidacao: " + posicaoSwap.IdPosicao.Value);
                }


                if (numeroDias <= 30)
                {
                    valorOutros_30Dias[tipoCarteiraAtivo] += posicaoSwap.Saldo.Value;
                }
                else if (numeroDias <= 90)
                {
                    valorOutros_90Dias[tipoCarteiraAtivo] += posicaoSwap.Saldo.Value;
                }
                else if (numeroDias <= 180)
                {
                    valorOutros_180Dias[tipoCarteiraAtivo] += posicaoSwap.Saldo.Value;
                }
                else if (numeroDias <= 360)
                {
                    valorOutros_360Dias[tipoCarteiraAtivo] += posicaoSwap.Saldo.Value;
                }
                else
                {
                    valorOutros_Acima[tipoCarteiraAtivo] += posicaoSwap.Saldo.Value;
                }

            }

            foreach (Liquidacao liquidacao in carteiraDiaria.LiquidacaoCollection)
            {
                string tipoCarteiraAtivo = RetornaTipoCarteira(liquidacao.IdCliente.Value);

                DateTime dataVencimento = liquidacao.DataVencimento.Value;
                int numeroDias;
                try
                {
                    numeroDias = Calendario.NumeroDias(data, dataVencimento, (int)LocalFeriadoFixo.Brasil, TipoFeriado.Brasil);
                }
                catch (Exception e)
                {
                    throw new Exception("Erro ao processar Liquidacao: " + liquidacao.IdLiquidacao.Value);
                }


                if (numeroDias <= 30)
                {
                    valorOutros_30Dias[tipoCarteiraAtivo] += liquidacao.Valor.Value;
                }
                else if (numeroDias <= 90)
                {
                    valorOutros_90Dias[tipoCarteiraAtivo] += liquidacao.Valor.Value;
                }
                else if (numeroDias <= 180)
                {
                    valorOutros_180Dias[tipoCarteiraAtivo] += liquidacao.Valor.Value;
                }
                else if (numeroDias <= 360)
                {
                    valorOutros_360Dias[tipoCarteiraAtivo] += liquidacao.Valor.Value;
                }
                else
                {
                    valorOutros_Acima[tipoCarteiraAtivo] += liquidacao.Valor.Value;
                }

            }

            foreach (SaldoCaixa saldoCaixa in carteiraDiaria.SaldoCaixaCollection)
            {
                string tipoCarteiraAtivo = RetornaTipoCarteira(saldoCaixa.IdCliente.Value);
                valorOutros_30Dias[tipoCarteiraAtivo] += saldoCaixa.SaldoFechamento.Value;
            }

            foreach (string tipoCarteira in tiposCarteira)
            {
                DetalheRelatorio detalheRelatorio = new DetalheRelatorio();
                detalheRelatorio.IdGrupoEconomico = idGrupoEconomico;
                detalheRelatorio.NomeGrupoEconomico = nomeGrupoEconomico;
                detalheRelatorio.TipoCarteira = tipoCarteira;
                detalheRelatorio.GrupoTitulo = "Outros";
                detalheRelatorio.TipoTitulo = "Outros";
                detalheRelatorio.Ate30Dias = Convert.ToInt64(valorOutros_30Dias[tipoCarteira]);
                detalheRelatorio.Ate90Dias = Convert.ToInt64(valorOutros_90Dias[tipoCarteira]);
                detalheRelatorio.Ate180Dias = Convert.ToInt64(valorOutros_180Dias[tipoCarteira]);
                detalheRelatorio.Ate360Dias = Convert.ToInt64(valorOutros_360Dias[tipoCarteira]);
                detalheRelatorio.Acima360Dias = Convert.ToInt64(valorOutros_Acima[tipoCarteira]);
                if ((detalheRelatorio.Ate30Dias + detalheRelatorio.Ate90Dias + detalheRelatorio.Ate180Dias +
                    detalheRelatorio.Ate360Dias + detalheRelatorio.Acima360Dias) != 0)
                {
                    listaDetalhe.Add(detalheRelatorio);
                }
            }
            #endregion

        }

        foreach (DetalheRelatorio detalheRelatorio in listaDetalhe)
        {
            detalheRelatorio.Ate30Dias = Math.Round(detalheRelatorio.Ate30Dias / 1000M, 2);
            detalheRelatorio.Ate90Dias = Math.Round(detalheRelatorio.Ate90Dias / 1000M, 2);
            detalheRelatorio.Ate180Dias = Math.Round(detalheRelatorio.Ate180Dias / 1000M, 2);
            detalheRelatorio.Ate360Dias = Math.Round(detalheRelatorio.Ate360Dias / 1000M, 2);
            detalheRelatorio.Acima360Dias = Math.Round(detalheRelatorio.Acima360Dias / 1000M, 2);
        }
    }
}
