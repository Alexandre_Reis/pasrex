﻿using System;
using System.IO;
using Bytescout.Spreadsheet;
using Bytescout.Spreadsheet.Constants;
using Financial.Investidor;
using Financial.Investidor.Enums;
using System.Collections.Generic;
using System.Drawing;
using Financial.Common;
using Financial.ContaCorrente;
using Financial.Util;
using Financial.Bolsa;
using System.Globalization;
using Financial.Common.Enums;
using Financial.RendaFixa;
using Financial.Swap;
using Financial.RendaFixa.Enums;
using Financial.ContaCorrente.Enums;
using Financial.Fundo;
using Financial.BMF;
using Financial.Bolsa.Enums;
using System.Xml;
using System.Collections;
using Financial.Fundo.Enums;

namespace Financial.Export
{

    /// <summary>
    /// Summary description for IntegralTrust
    /// </summary>
    public class IntegralTrust
    {
        public class Vista
        {
            public DateTime data;
            public string empresa;
            public string AP;
            public string produto;
            public string grupo;
            public string cod_Bovespa;
            public string venc;
            public decimal qtde;
            public string index;
            public decimal abs_MTM;
            public decimal valor_MTM;
        }

        public class Opcao
        {
            public DateTime data;
            public string empresa;
            public string cod_Opcao;
            public string ativo_Obj;
            public decimal spot;
            public decimal lote;
            public string tipo_Opcao;
            public string tipo_ATPA;
            public string vencimento;
            public decimal qtde;
            public decimal premio;
            public decimal preco_Exerc;
            public decimal vl_Notacional;
            public string produto;
            public decimal valor_MTM;
        }

        public List<Vista> listaVista = new List<Vista>();
        public List<Opcao> listaOpcao = new List<Opcao>();

        private DateTime dataReferencia;

        /* Construtor */
        public IntegralTrust(DateTime dataReferencia, List<int> lstIdCarteira)
        {
            XmlDocument xmldoc = new System.Xml.XmlDocument();
            xmldoc.Load(AppDomain.CurrentDomain.BaseDirectory + "/XMLCustom/PortoPar/DePara_IntegralTrust.xml");

            this.dataReferencia = Calendario.AdicionaDiaUtil(dataReferencia, 1);

            ClienteCollection clienteCollection = new ClienteCollection();
            clienteCollection.Query.Select(clienteCollection.Query.IdCliente,
                                           clienteCollection.Query.Status,
                                           clienteCollection.Query.Nome,
                                           clienteCollection.Query.DataDia);
            clienteCollection.Query.Where(clienteCollection.Query.TipoControle.Equal((byte)TipoControleCliente.Completo),
                                          clienteCollection.Query.StatusAtivo.Equal((byte)StatusAtivoCliente.Ativo));

            if (lstIdCarteira.Count > 0)
                clienteCollection.Query.Where(clienteCollection.Query.IdCliente.In(lstIdCarteira.ToArray()));

            clienteCollection.Query.Load();

            foreach (Cliente cliente in clienteCollection)
            {
                int idCliente = cliente.IdCliente.Value;
                string nome = cliente.Nome;

                try
                {
                    Carteira carteira = new Carteira();
                    Financial.Fundo.Carteira.CarteiraDiaria carteiraDiaria = carteira.MontaCarteira(idCliente, dataReferencia, true);

                    #region Titulos privados
                    foreach (PosicaoRendaFixa posicaoRendaFixa in carteiraDiaria.PosicaoRendaFixaPrivadoCollection)
                    {
                        TituloRendaFixa tituloRendaFixa = new TituloRendaFixa();
                        tituloRendaFixa.LoadByPrimaryKey(posicaoRendaFixa.IdTitulo.Value);

                        PapelRendaFixa papelRendaFixa = new PapelRendaFixa();
                        papelRendaFixa.LoadByPrimaryKey(tituloRendaFixa.IdPapel.Value);

                        Vista vista = new Vista();
                        vista.data = dataReferencia;
                        vista.empresa = nome;

                        if (posicaoRendaFixa.Quantidade.Value > 0)
                        {
                            vista.AP = "A";
                        }
                        else
                        {
                            vista.AP = "P";
                        }

                        vista.produto = "";
                        XmlNode node = xmldoc.SelectSingleNode(string.Format("Lista/DePara[@Ativo='{0}']", papelRendaFixa.IdPapel.ToString()));
                        if (node != null)
                        {
                            vista.produto = node.Attributes["Codigo"].InnerText;
                        }
                        else
                        {
                            if (posicaoRendaFixa.TipoOperacao.Value == (byte)TipoOperacaoTitulo.CompraRevenda)
                            {
                                vista.produto = "012"; //COMPROMISSADAS CETIP
                            }
                            else if (papelRendaFixa.Classe.Value == (int)ClasseRendaFixa.CDB)
                            {
                                vista.produto = "003"; //CDB
                            }
                            else if (papelRendaFixa.Classe.Value == (int)ClasseRendaFixa.Debenture)
                            {
                                vista.produto = "091"; //Debenture
                            }
                        }

                        vista.grupo = "RF";
                        vista.cod_Bovespa = "";
                        vista.venc = posicaoRendaFixa.DataVencimento.Value.ToShortDateString();
                        vista.qtde = posicaoRendaFixa.Quantidade.Value;

                        vista.index = "";
                        if (tituloRendaFixa.IdIndice.HasValue)
                        {
                            Indice indice = new Indice();
                            indice.LoadByPrimaryKey(tituloRendaFixa.IdIndice.Value);
                            vista.index = indice.Descricao;
                        }

                        vista.abs_MTM = Math.Abs(posicaoRendaFixa.ValorMercado.Value);
                        vista.valor_MTM = posicaoRendaFixa.ValorMercado.Value;

                        this.listaVista.Add(vista);
                    }
                    #endregion

                    #region Titulos publicos
                    foreach (PosicaoRendaFixa posicaoRendaFixa in carteiraDiaria.PosicaoRendaFixaPublicoCollection)
                    {
                        TituloRendaFixa tituloRendaFixa = new TituloRendaFixa();
                        tituloRendaFixa.LoadByPrimaryKey(posicaoRendaFixa.IdTitulo.Value);

                        PapelRendaFixa papelRendaFixa = new PapelRendaFixa();
                        papelRendaFixa.LoadByPrimaryKey(tituloRendaFixa.IdPapel.Value);

                        Vista vista = new Vista();
                        vista.data = dataReferencia;
                        vista.empresa = nome;
                        vista.AP = "A";

                        vista.produto = "";
                        XmlNode node = xmldoc.SelectSingleNode(string.Format("Lista/DePara[@Ativo='{0}']", papelRendaFixa.IdPapel.ToString()));
                        if (node != null)
                        {
                            vista.produto = node.Attributes["Codigo"].InnerText;
                        }
                        else
                        {
                            if (posicaoRendaFixa.TipoOperacao.Value == (byte)TipoOperacaoTitulo.CompraRevenda)
                            {
                                vista.produto = "016"; //COMPROMISSADAS SELIC
                            }
                            else if (papelRendaFixa.Classe.Value == (byte)ClasseRendaFixa.NTN)
                            {
                                vista.produto = "015"; //NTN
                            }
                            else if (papelRendaFixa.Classe.Value == (byte)ClasseRendaFixa.LFT)
                            {
                                vista.produto = "032"; //LFT
                            }
                            else if (papelRendaFixa.Classe.Value == (byte)ClasseRendaFixa.LTN)
                            {
                                vista.produto = "033"; //LTN
                            }
                        }

                        vista.grupo = "RF";
                        vista.cod_Bovespa = "";
                        vista.venc = posicaoRendaFixa.DataVencimento.Value.ToShortDateString();
                        vista.qtde = posicaoRendaFixa.Quantidade.Value;

                        vista.index = "";
                        if (tituloRendaFixa.IdIndice.HasValue)
                        {
                            Indice indice = new Indice();
                            indice.LoadByPrimaryKey(tituloRendaFixa.IdIndice.Value);
                            vista.index = indice.Descricao;
                        }

                        vista.abs_MTM = Math.Abs(posicaoRendaFixa.ValorMercado.Value);
                        vista.valor_MTM = posicaoRendaFixa.ValorMercado.Value;

                        this.listaVista.Add(vista);
                    }
                    #endregion

                    #region Swap
                    foreach (PosicaoSwap posicaoSwap in carteiraDiaria.PosicaoSwapCollection)
                    {
                        
                        //Ponta Ativa
                        Vista vistaA = new Vista();
                        vistaA.data = dataReferencia;
                        vistaA.empresa = nome;
                        vistaA.AP = "A";
                        vistaA.produto = "023";
                        vistaA.grupo = "RF";
                        vistaA.cod_Bovespa = "";
                        vistaA.venc = posicaoSwap.DataVencimento.Value.ToShortDateString();
                        vistaA.qtde = posicaoSwap.ValorBase.Value;

                        vistaA.index = "";
                        if (posicaoSwap.IdIndice.HasValue)
                        {
                            Indice indice = new Indice();
                            indice.LoadByPrimaryKey(posicaoSwap.IdIndice.Value);
                            vistaA.index = indice.Descricao;
                        }

                        vistaA.abs_MTM = Math.Abs(posicaoSwap.Saldo.Value);
                        vistaA.valor_MTM = posicaoSwap.Saldo.Value;

                        this.listaVista.Add(vistaA);

                        //Ponta Passiva
                        Vista vistaP = new Vista();
                        vistaP.data = dataReferencia;
                        vistaP.empresa = nome;
                        vistaP.AP = "P";
                        vistaP.produto = "023";
                        vistaP.grupo = "RF";
                        vistaP.cod_Bovespa = "";
                        vistaP.venc = posicaoSwap.DataVencimento.Value.ToShortDateString();
                        vistaP.qtde = posicaoSwap.ValorBase.Value;

                        vistaP.index = "";
                        if (posicaoSwap.IdIndiceContraParte.HasValue)
                        {
                            Indice indice = new Indice();
                            indice.LoadByPrimaryKey(posicaoSwap.IdIndiceContraParte.Value);
                            vistaP.index = indice.Descricao;
                        }

                        vistaP.abs_MTM = 0;
                        vistaP.valor_MTM = 0;

                        this.listaVista.Add(vistaP);
                    }
                    #endregion


                    #region Futuros BMF
                    foreach (PosicaoBMF posicaoBMF in carteiraDiaria.PosicaoBMFCollection)
                    {
                        Vista vista = new Vista();
                        vista.data = dataReferencia;
                        vista.empresa = nome;
                        vista.AP = posicaoBMF.Quantidade.Value > 0 ? "A" : "P";

                        vista.produto = "";

                        XmlNode node = xmldoc.SelectSingleNode(string.Format("Lista/DePara[@Ativo='{0}']", posicaoBMF.CdAtivoBMF));
                        if (node != null)
                        {
                            vista.produto = node.Attributes["Codigo"].InnerText;
                        }
                        else
                        {
                            if (posicaoBMF.CdAtivoBMF == "DI1")
                            {
                                vista.produto = "006";
                            }
                            else if (posicaoBMF.CdAtivoBMF == "DOL")
                            {
                                vista.produto = "030";
                            }
                            else if (posicaoBMF.CdAtivoBMF == "IND")
                            {
                                vista.produto = "031";
                            }
                        }

                        vista.grupo = "DE";
                        vista.cod_Bovespa = "";
                        vista.venc = posicaoBMF.Serie;
                        vista.qtde = Math.Abs(posicaoBMF.Quantidade.Value);
                        vista.index = posicaoBMF.CdAtivoBMF;
                        vista.abs_MTM = Math.Abs(posicaoBMF.ValorMercado.Value);
                        vista.valor_MTM = posicaoBMF.ValorMercado.Value;

                        this.listaVista.Add(vista);
                    }
                    #endregion

                    #region Opcoes BMF
                    foreach (PosicaoBMF posicaoBMF in carteiraDiaria.PosicaoBMFOpcoesCollection)
                    {
                        AtivoBMF ativoBMF = new AtivoBMF();
                        ativoBMF.LoadByPrimaryKey(posicaoBMF.CdAtivoBMF, posicaoBMF.Serie);

                        Opcao opcao = new Opcao();
                        opcao.data = dataReferencia;
                        opcao.empresa = nome;
                        opcao.cod_Opcao = posicaoBMF.CdAtivoBMF;
                        opcao.ativo_Obj = posicaoBMF.CdAtivoBMF;
                        opcao.spot = 0;
                        opcao.lote = ativoBMF.Peso.Value;
                        opcao.tipo_Opcao = ativoBMF.TipoSerie == "C" ? "C" : "P"; //C=Call, P=Put                    
                        opcao.tipo_ATPA = posicaoBMF.Quantidade.Value > 0 ? "P" : "A"; //Layout errado, devemos seguir o layout mesmo assim
                        opcao.vencimento = posicaoBMF.Serie;
                        opcao.qtde = Math.Abs(posicaoBMF.Quantidade.Value);
                        opcao.premio = posicaoBMF.PUMercado.Value;
                        opcao.preco_Exerc = ativoBMF.PrecoExercicio.HasValue ? ativoBMF.PrecoExercicio.Value : 0;
                        opcao.vl_Notacional = Math.Abs(posicaoBMF.ValorMercado.Value);
                        opcao.valor_MTM = posicaoBMF.ValorMercado.Value;
                        opcao.produto = "109";
                        this.listaOpcao.Add(opcao);
                    }
                    #endregion

                    #region Acoes
                    DateTime dataProx = Calendario.AdicionaDiaUtil(dataReferencia, 1);
                    foreach (PosicaoBolsa posicaoBolsa in carteiraDiaria.PosicaoBolsaCollection)
                    {
                        Vista vista = new Vista();
                        vista.data = dataReferencia;
                        vista.empresa = nome;
                        vista.AP = posicaoBolsa.Quantidade.Value > 0 ? "A" : "P";

                        vista.produto = "107";
                        vista.grupo = "RV";
                        vista.cod_Bovespa = posicaoBolsa.CdAtivoBolsa;
                        vista.venc = dataProx.ToShortDateString();
                        vista.qtde = Math.Abs(posicaoBolsa.Quantidade.Value);
                        vista.index = "";
                        vista.abs_MTM = Math.Abs(posicaoBolsa.ValorMercado.Value);
                        vista.valor_MTM = posicaoBolsa.ValorMercado.Value;

                        this.listaVista.Add(vista);
                    }
                    #endregion

                    #region Opcoes Bolsa
                    foreach (PosicaoBolsa posicaoBolsa in carteiraDiaria.PosicaoBolsaOpcoesCollection)
                    {
                        AtivoBolsa ativoBolsa = new AtivoBolsa();
                        ativoBolsa.LoadByPrimaryKey(posicaoBolsa.CdAtivoBolsa);

                        Opcao opcao = new Opcao();
                        opcao.data = dataReferencia;
                        opcao.empresa = nome;
                        opcao.cod_Opcao = posicaoBolsa.CdAtivoBolsa;
                        opcao.ativo_Obj = String.IsNullOrEmpty(ativoBolsa.CdAtivoBolsaObjeto) ? "" : ativoBolsa.CdAtivoBolsaObjeto;

                        decimal cotacaoObjeto = 0;
                        if (!String.IsNullOrEmpty(ativoBolsa.CdAtivoBolsaObjeto))
                        {
                            CotacaoBolsa cotacaoBolsa = new CotacaoBolsa();
                            if (cotacaoBolsa.BuscaCotacaoBolsa(ativoBolsa.CdAtivoBolsaObjeto, dataReferencia))
                            {
                                cotacaoObjeto = cotacaoBolsa.PUFechamento.HasValue ? cotacaoBolsa.PUFechamento.Value : 0;
                            }
                        }
                        opcao.spot = cotacaoObjeto;
                        opcao.lote = 1;
                        opcao.tipo_Opcao = posicaoBolsa.TipoMercado == "OPC" ? "C" : "P"; //C=Call, P=Put                    
                        opcao.tipo_ATPA = posicaoBolsa.Quantidade.Value > 0 ? "A" : "P";
                        opcao.vencimento = posicaoBolsa.DataVencimento.Value.ToShortDateString();
                        opcao.qtde = Math.Abs(posicaoBolsa.Quantidade.Value);
                        opcao.premio = posicaoBolsa.PUMercado.Value;
                        opcao.preco_Exerc = ativoBolsa.PUExercicio.HasValue ? ativoBolsa.PUExercicio.Value : 0;
                        opcao.vl_Notacional = Math.Abs(posicaoBolsa.ValorMercado.Value);
                        opcao.valor_MTM = posicaoBolsa.ValorMercado.Value;
                        opcao.produto = "111";
                        this.listaOpcao.Add(opcao);
                    }
                    #endregion

                    #region BTC
                    foreach (PosicaoEmprestimoBolsa posicaoEmprestimoBolsa in carteiraDiaria.PosicaoEmprestimoBolsaCollection)
                    {
                        Vista vista = new Vista();
                        vista.data = dataReferencia;
                        vista.empresa = nome;
                        vista.AP = posicaoEmprestimoBolsa.PontaEmprestimo.Value == (byte)PontaEmprestimoBolsa.Doador ? "A" : "P";
                        vista.produto = "107";
                        vista.grupo = "RV";
                        vista.cod_Bovespa = posicaoEmprestimoBolsa.CdAtivoBolsa;
                        vista.venc = posicaoEmprestimoBolsa.DataVencimento.Value.ToShortTimeString();
                        vista.qtde = Math.Abs(posicaoEmprestimoBolsa.Quantidade.Value);
                        vista.index = "";
                        vista.abs_MTM = Math.Abs(posicaoEmprestimoBolsa.ValorMercado.Value);
                        vista.valor_MTM = posicaoEmprestimoBolsa.ValorMercado.Value;

                        this.listaVista.Add(vista);
                    }
                    #endregion

                    #region Fundos
                    foreach (PosicaoFundo posicaoFundo in carteiraDiaria.PosicaoFundoCollection)
                    {
                        Carteira carteiraFundo = new Carteira();
                        carteiraFundo.Query.Select(carteiraFundo.Query.TipoTributacao, carteiraFundo.Query.IdCarteira);
                        carteiraFundo.Query.Where(carteiraFundo.Query.IdCarteira.Equal(posicaoFundo.IdCarteira.Value));
                        carteiraFundo.Load(carteiraFundo.Query);

                        Vista vista = new Vista();
                        vista.data = dataReferencia;
                        vista.empresa = nome;
                        if (posicaoFundo.Quantidade.Value > 0)
                        {
                            vista.AP = "A";
                        }
                        else
                        {
                            vista.AP = "P";
                        }

                        XmlNode node = xmldoc.SelectSingleNode(string.Format("Lista/DePara[@Ativo='fundo-{0}']", Convert.ToString(carteiraFundo.IdCarteira.Value)));
                        if (node != null)
                        {
                            vista.produto = node.Attributes["Codigo"].InnerText;
                        }
                        else
                        {
                            vista.produto = Convert.ToString(carteiraFundo.IdCarteira.Value);
                        }
                        
                        vista.grupo = "";
                        vista.cod_Bovespa = "";
                        vista.venc = "";
                        vista.qtde = 0;
                        vista.index = "";
                        vista.abs_MTM = Math.Abs(posicaoFundo.ValorBruto.Value);
                        vista.valor_MTM = posicaoFundo.ValorBruto.Value;

                        this.listaVista.Add(vista);
                    }
                    #endregion

                }
                catch (Exception e)
                {
                    throw new Exception("Erro no cliente " + idCliente.ToString() + " - " + nome + e.Message);
                }
            }
        }
    }
}