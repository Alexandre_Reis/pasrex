﻿using FileHelpers;
using System;
using Financial.Contabil;
using System.Collections.Generic;
using System.IO;
using Financial.WebConfigConfiguration;
using System.Text;
using Financial.Fundo;
using System.Globalization;
using Financial.InvestidorCotista;
using Financial.Investidor;
using EntitySpaces.Interfaces;
using Financial.Captacao;

namespace Financial.Export {

    /// <summary>
    /// Summary description for PosicaoCotistaGradual
    /// </summary>
    public class PosicaoCotistaGradual {

        private enum TipoPesquisa {
            PosicaoCotista = 0,
            PosicaoCotistaHistorico = 1
        }

        [DelimitedRecord(";")]
        public class PosicaoCotistaDS {

            public int idCarteira;
            public int idCarteira1;
            public string nomeCarteira;

            [FieldConverter(ConverterKind.Date, "dd/MM/yyyy")] 
            public DateTime dataPosicao;
            
            [FieldIgnored()]
            public decimal valorCotaAtual;
            
            /* valorCotaAtual formatado com numero de Casas de acordo com numCasasCota */
            public string valorCotaAtualString;

            public int idCotista;
            public string nomeCotista;

            public int numCasasCota;
            public int numCasasQtdCotas;

            [FieldConverter(ConverterKind.Date, "dd/MM/yyyy")]             
            public DateTime dataCertificado;

            [FieldIgnored()]
            public decimal quantidadeCotasCertificado;

            /* quantidadeCotasCertificado formatado com numero de Casas de acordo com numCasasQtdCotas */
            public string quantidadeCotasString;

            [FieldConverter(typeof(PosicaoCotistaDS.TwoDecimalConverter))]
            public decimal valorCertificado;

            [FieldConverter(typeof(PosicaoCotistaDS.TwoDecimalConverter))]
            public decimal pfeeCertificado;

            [FieldConverter(typeof(PosicaoCotistaDS.TwoDecimalConverter))]
            public decimal iofCertificado;

            [FieldConverter(typeof(PosicaoCotistaDS.TwoDecimalConverter))]
            public decimal irCertificado;

            [FieldConverter(typeof(PosicaoCotistaDS.TwoDecimalConverter))]
            public decimal outrasDespesas;

            [FieldConverter(typeof(PosicaoCotistaDS.TwoDecimalConverter))]
            public decimal valorLiquidoCertificado;

            [FieldConverter(typeof(PosicaoCotistaDS.TwoDecimalConverter))]
            public decimal valorAplicacaoCertificado;

            //[FieldNullValue("codigoAssessor")]
            public string codigoAssessor;

            //[FieldNullValue("nomeAssessor")]
            public string nomeAssessor;

            #region
            internal class TwoDecimalConverter : ConverterBase {
                public override object StringToField(string from) {
                    decimal res = Convert.ToDecimal(from);
                    //return res / 100;
                    return res;
                }

                public override string FieldToString(object from) {
                    //decimal d = (decimal)from;
                    //return Math.Round(d * 100).ToString().Replace(".", ",");

                    decimal res = Convert.ToDecimal(from);
                    return res.ToString("N2").Replace(".", "");
                }
            }

            //internal class EightDecimalConverter : ConverterBase {

            //    EightDecimalConverter() {}

            //    EightDecimalConverter(int num1, int num2) {
            //        int u = 0;
            //    }

                
            //    public override object StringToField(string from) {
            //        decimal res = Convert.ToDecimal(from);
            //        //return res / 100000000;
            //        return res;
            //    }

            //    public override string FieldToString(object from) {
            //        //decimal d = (decimal)from;

            //        //return Math.Round(d * 100000000).ToString().Replace(".", ",");

            //        //decimal res = Convert.ToDecimal(from);
            //        //return res.ToString("N8").Replace(".", ",");

            //        /* --------------------------------------------*/
            //        NumberFormatInfo provider = new NumberFormatInfo();
            //        provider.NumberDecimalSeparator = ",";
            //        provider.NumberDecimalDigits = 8;
                    
            //        decimal res = Convert.ToDecimal(from, provider);
            //        return res.ToString("N8").Replace(".", "");
            //    }
            //}
            #endregion

        }

        /// <summary>
        /// Faz Formatação condicional do Numero de Casas dos campos valorCotaAtual e quantidadeCotasCertificado
        /// </summary>
        /// <param name="engine"></param>
        /// <param name="e"></param>
        private void BeforeWriteEvent(EngineBase engine, BeforeWriteRecordEventArgs e) {
            PosicaoCotistaDS r = e.Record as PosicaoCotistaDS;

            #region valorCotaAtual
            int numCasasCotas = r.numCasasCota;
            decimal valorCota = r.valorCotaAtual;
            string format = "N"+numCasasCotas;

            string valor = valorCota.ToString(format).Replace(".", "");

            r.valorCotaAtualString = valor;
            #endregion

            #region quantidadeCotasCertificado
            int numCasasQuantidade = r.numCasasQtdCotas;
            decimal quantidadeCota = r.quantidadeCotasCertificado;
            string format1 = "N" + numCasasQuantidade;

            string valor1 = quantidadeCota.ToString(format1).Replace(".", "");

            r.quantidadeCotasString = valor1;
            #endregion
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dataLancamento"></param>
        /// <param name="ms">Saida: Memory Stream do arquivo de PosicaoCotista</param>
        /// <param name="nomeArquivo">Saida: Nome do Arquivo De PosicaoCotista</param>        
        public void ExportaPosicaoCotista(DateTime dataLancamento, out MemoryStream ms, out string nomeArquivo) {
            // out
            ms = new MemoryStream();
            nomeArquivo = "posfundos.txt";

            #region Consulta

            #region Carteiras
            //CarteiraCollection carteiras = new CarteiraCollection();

            //carteiras.Query.Select(carteiras.Query.IdCarteira);
            //carteiras.Query.OrderBy(carteiras.Query.IdCarteira.Ascending);
            //carteiras.Query.es.Distinct = true;

            //carteiras.Query.Load();

            CarteiraQuery carteiras = new CarteiraQuery("C");
            //
            carteiras.Select(carteiras.IdCarteira);            
            carteiras.es.Distinct = true;
            //
            ClienteQuery c = new ClienteQuery("Cliente");
            //
            c.Select(c.IdCliente.As("IdCarteira"), c.DataDia)
             .Where(c.IdCliente.In(carteiras));

            ClienteCollection c1 = new ClienteCollection();
            c1.Load(c);
            
            #endregion

            // Collection usada para o Bindind
            PosicaoCotistaCollection pBinding = new PosicaoCotistaCollection();

            /* Para cada carteira Consulta em PosicaoCotista ou PosicaoCotistaHistorico de acordo com a dataDia do Cliente */
            for (int i = 0; i < c1.Count; i++) {
                int idCarteira = Convert.ToInt32(c1[i].GetColumn("IdCarteira"));
                DateTime dataDia = c1[i].DataDia.Value;

                TipoPesquisa tipoPesquisa = dataLancamento >= dataDia
                                                        ? TipoPesquisa.PosicaoCotista 
                                                        : TipoPesquisa.PosicaoCotistaHistorico;
                 
                if (tipoPesquisa == TipoPesquisa.PosicaoCotista) {
                    #region PosicaoCotista
                    PosicaoCotistaCollection p = new PosicaoCotistaCollection();
                    
                    p.Query.Select(p.Query.IdCarteira, p.Query.IdCotista, p.Query.CotaDia,
                                   p.Query.DataAplicacao, p.Query.Quantidade,
                                   p.Query.ValorBruto,
                                   p.Query.ValorIOF,
                                   p.Query.ValorIR,
                                   p.Query.ValorPerformance,
                                   p.Query.ValorLiquido,
                                   p.Query.CotaAplicacao);
                    //
                    p.Query.Where(p.Query.IdCarteira == idCarteira &&
                                  p.Query.Quantidade != 0);
                    //
                    p.Query.Load();
                    //
                    pBinding.Combine(p);

                    #endregion
                }
                else {
                    #region PosicaoCotistaHistorico
                    PosicaoCotistaHistoricoCollection pHistorico = new PosicaoCotistaHistoricoCollection();
                    pHistorico.Query.Select(pHistorico.Query.IdCarteira, pHistorico.Query.IdCotista, pHistorico.Query.CotaDia,
                                   pHistorico.Query.DataAplicacao, pHistorico.Query.Quantidade,
                                   pHistorico.Query.ValorBruto,
                                   pHistorico.Query.ValorIOF,
                                   pHistorico.Query.ValorIR,
                                   pHistorico.Query.ValorPerformance,
                                   pHistorico.Query.ValorLiquido,
                                   pHistorico.Query.CotaAplicacao);
                    //
                    pHistorico.Query.Where(pHistorico.Query.IdCarteira == idCarteira &&
                                           pHistorico.Query.DataHistorico == dataLancamento && 
                                           pHistorico.Query.Quantidade != 0);
                    //
                    pHistorico.Query.Load();
                    
                    //
                    for (int j = 0; j < pHistorico.Count; j++) {
                        PosicaoCotista p = pBinding.AddNew();
                        p.IdCarteira = pHistorico[j].IdCarteira;
                        p.IdCotista = pHistorico[j].IdCotista;
                        p.CotaDia = pHistorico[j].CotaDia;
                        p.DataAplicacao = pHistorico[j].DataAplicacao;
                        p.Quantidade = pHistorico[j].Quantidade;
                        p.ValorBruto = pHistorico[j].ValorBruto;
                        p.ValorIOF = pHistorico[j].ValorIOF;
                        p.ValorIR = pHistorico[j].ValorIR;
                        p.ValorPerformance = pHistorico[j].ValorPerformance;
                        p.ValorLiquido = pHistorico[j].ValorLiquido;
                        p.CotaAplicacao = pHistorico[j].CotaAplicacao;
                    }
                    #endregion
                }
            }

            pBinding.Sort = PosicaoCotistaMetadata.ColumnNames.IdCarteira + " ASC, " +
                            PosicaoCotistaMetadata.ColumnNames.IdCotista + " ASC, " +
                            PosicaoCotistaMetadata.ColumnNames.DataAplicacao + " ASC ";            
            #endregion

            if (pBinding.Count == 0) {
                throw new Exception("Não há dados para exportar.");
            }
            
            List<PosicaoCotistaDS> posicaoCotistaDS = new List<PosicaoCotistaDS>();

            for (int i = 0; i < pBinding.Count; i++) {
                #region DataSource
                PosicaoCotistaDS posicao = new PosicaoCotistaDS();
                posicao.idCarteira = pBinding[i].IdCarteira.Value;
                posicao.idCarteira1 = posicao.idCarteira;
                posicao.nomeCarteira = pBinding[i].UpToCarteiraByIdCarteira.str.Nome;
                posicao.dataPosicao = dataLancamento;
                posicao.valorCotaAtual = pBinding[i].CotaDia.Value;
                //
                posicao.idCotista = pBinding[i].IdCotista.Value;
                posicao.nomeCotista = pBinding[i].UpToCotistaByIdCotista.str.Nome;
                
                #region Carteira Info
                //
                Carteira carteira = new Carteira();
                List<esQueryItem> campos = new List<esQueryItem>();
                campos.Add(carteira.Query.IdCarteira);
                campos.Add(carteira.Query.CasasDecimaisCota);
                campos.Add(carteira.Query.CasasDecimaisQuantidade);
                //
                carteira.LoadByPrimaryKey(campos, posicao.idCarteira);
                //
                #endregion

                posicao.numCasasCota = carteira.CasasDecimaisCota.Value;
                posicao.numCasasQtdCotas = carteira.CasasDecimaisQuantidade.Value;
                //
                posicao.dataCertificado = pBinding[i].DataAplicacao.Value;
                posicao.quantidadeCotasCertificado = pBinding[i].Quantidade.Value;
                posicao.valorCertificado = pBinding[i].ValorBruto.Value;
                posicao.pfeeCertificado = pBinding[i].ValorPerformance.Value;
                posicao.iofCertificado = pBinding[i].ValorIOF.Value;
                posicao.irCertificado = pBinding[i].ValorIR.Value;
                posicao.outrasDespesas = 0;
                posicao.valorLiquidoCertificado = pBinding[i].ValorLiquido.Value;
                posicao.valorAplicacaoCertificado = pBinding[i].Quantidade.Value * pBinding[i].CotaAplicacao.Value;
                
                #region Cliente
                ClienteQuery cliente = new ClienteQuery("C1");
                OfficerQuery o = new OfficerQuery("O");
                //
                cliente.Select(cliente.IdCliente, cliente.IdOfficer, o.Nome);
                cliente.LeftJoin(o).On(cliente.IdOfficer == o.IdOfficer);
                //
                cliente.Where(cliente.IdCliente == posicao.idCarteira);                
                //
                ClienteCollection clienteCollection = new ClienteCollection();
                clienteCollection.Load(cliente);

                #endregion

                if (clienteCollection[0].IdOfficer.HasValue) {
                    posicao.codigoAssessor = clienteCollection[0].IdOfficer.Value.ToString();
                }

                if (clienteCollection[0].Nome != null) {
                    posicao.nomeAssessor = clienteCollection[0].str.Nome.Trim();
                }
                #endregion
                //
                posicaoCotistaDS.Add(posicao);
            }

            FileHelperEngine engine = new FileHelperEngine(typeof(PosicaoCotistaDS));
            engine.BeforeWriteRecord += new BeforeWriteRecordHandler(BeforeWriteEvent);
            
            StreamWriter arquivo = new StreamWriter(ms, Encoding.GetEncoding("ISO-8859-1"));
            engine.Encoding = Encoding.GetEncoding("ISO-8859-1");
            //
            engine.WriteStream(arquivo, posicaoCotistaDS);

            //Fecha o arquivo
            arquivo.Flush();
        }
    }
}