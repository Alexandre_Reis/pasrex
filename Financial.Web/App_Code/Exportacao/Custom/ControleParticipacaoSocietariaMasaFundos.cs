﻿using System;
using System.IO;
using Bytescout.Spreadsheet;
using Bytescout.Spreadsheet.Constants;
using Financial.Investidor;
using Financial.Investidor.Enums;
using System.Collections.Generic;
using System.Drawing;
using Financial.Common;
using Financial.Fundo;
using Financial.CRM;
using Financial.Fundo.Enums;

namespace Financial.Export {

    /// <summary>
    /// Summary description for ControleParticipacaoSocietariaMasaFundos
    /// </summary>
    public class ControleParticipacaoSocietariaMasaFundos {

        private const string MODELO_PLANILHA = "ControleParticipacaoSocietariaMasaFundos.xls";
        
        private DateTime dataInicio;
        private DateTime dataFim;
        private int idCliente;
        private int idFundo;
        //
        private Cliente cliente = new Cliente();

        private OperacaoFundoCollection operacaoFundoCollection = new OperacaoFundoCollection();
        
        private decimal quantidadeSaldo;
        private decimal valorCusto;
        private decimal puCustoLiquido;
        
        /* Construtor */
        public ControleParticipacaoSocietariaMasaFundos(int idCliente, int idFundo, DateTime dataInicio, DateTime dataFim) 
        {
            bool temDados = false;

            this.idCliente = idCliente;
            this.idFundo = idFundo;
            this.dataInicio = dataInicio;
            this.dataFim = dataFim;
            //
            this.cliente.LoadByPrimaryKey(this.idCliente);
            //
            bool historico = !this.cliente.IsClienteNaData(this.idCliente, this.dataInicio);

            #region PosicaoFundoHistorico (Saldo Inicial)
            PosicaoFundoHistoricoCollection posicaoFundoHistoricoCollection = new PosicaoFundoHistoricoCollection();
            posicaoFundoHistoricoCollection.Query.Select(posicaoFundoHistoricoCollection.Query.Quantidade,
                                                         posicaoFundoHistoricoCollection.Query.CotaAplicacao);
            posicaoFundoHistoricoCollection.Query.Where(posicaoFundoHistoricoCollection.Query.IdCliente.Equal(this.idCliente),
                                                        posicaoFundoHistoricoCollection.Query.IdCarteira.Equal(this.idFundo),
                                                        posicaoFundoHistoricoCollection.Query.DataHistorico.Equal(this.dataInicio));
            posicaoFundoHistoricoCollection.Query.Load();

            decimal totalQuantidade = 0;
            decimal totalCusto = 0;
            foreach (PosicaoFundoHistorico posicaoFundoHistorico in posicaoFundoHistoricoCollection)
            {
                decimal quantidade = posicaoFundoHistorico.Quantidade.Value;
                decimal cotaAplicacao = posicaoFundoHistorico.CotaAplicacao.Value;

                decimal valorCusto = Math.Round(quantidade * cotaAplicacao, 2);

                totalQuantidade += quantidade;
                totalCusto += valorCusto;

                temDados = true;
            }

            if (totalQuantidade == 0 && temDados)
            {
                throw new Exception("Quantidade em posição zerada ou sem posição na data " + this.dataInicio.ToShortDateString());
            }

            if (temDados)
            {
                this.quantidadeSaldo = totalQuantidade;
                this.valorCusto = totalCusto;
                this.puCustoLiquido = totalCusto / totalQuantidade;
            }
            #endregion
        
            #region OperacaoFundo
            this.operacaoFundoCollection.Query.Select(this.operacaoFundoCollection.Query.Quantidade,
                                                      this.operacaoFundoCollection.Query.ValorLiquido,
                                                      this.operacaoFundoCollection.Query.DataConversao,
                                                      this.operacaoFundoCollection.Query.TipoOperacao,
                                                      this.operacaoFundoCollection.Query.Observacao);
            this.operacaoFundoCollection.Query.Where(this.operacaoFundoCollection.Query.IdCarteira == this.idFundo,
                                                     this.operacaoFundoCollection.Query.IdCliente == this.idCliente,
                                                     this.operacaoFundoCollection.Query.DataConversao.GreaterThan(this.dataInicio),
                                                     this.operacaoFundoCollection.Query.DataConversao.LessThanOrEqual(this.dataFim),
                                                     this.operacaoFundoCollection.Query.Quantidade.NotEqual(0));
            this.operacaoFundoCollection.Query.OrderBy(this.operacaoFundoCollection.Query.DataConversao.Ascending,
                                                       this.operacaoFundoCollection.Query.IdOperacao.Ascending);
            //            
            this.operacaoFundoCollection.Query.Load();

            if (this.operacaoFundoCollection.Count > 0)
            {
                temDados = true;
            }
            #endregion

            if (!temDados) 
            {
                throw new Exception("Não há Dados para Gerar.");
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="ms">Saida: Memory Stream do arquivo excel</param>
        /// <param name="nomeArquivo">Saida: Nome do Arquivo Excel</param>
        /// <exception cref="Exception">Se não dados para gerar</exception>
        public void ExportaExcel(out MemoryStream ms, out string nomeArquivo) 
        {
            // out
            ms = new MemoryStream();
            nomeArquivo = "ParticipacaoSocietariaFundos.xls";
            //           
            Spreadsheet document = new Bytescout.Spreadsheet.Spreadsheet();

            string pathModelos = AppDomain.CurrentDomain.BaseDirectory + "\\Relatorios\\Modelos";
            string pathPlanilhaModelo = String.Format("{0}\\{1}", pathModelos, MODELO_PLANILHA);
            
            document.LoadFromFile(pathPlanilhaModelo);

            Worksheet worksheet = document.Workbook.Worksheets[0];

            #region Ref Planilha - worksheet
            worksheet.ViewOptions.ShowZeroValues = true;
            this.setPlan(worksheet);
            #endregion

            /*-----------------------------------------------------*/

            #region Esconde Planilha Demo do componente
            const string BYTESCOUT_WORKSHEET_NAME = "Bytescout Spreadsheet SDK";
            worksheet = document.Workbook.Worksheets.ByName(BYTESCOUT_WORKSHEET_NAME);
            if (worksheet == null) {
                worksheet = document.Workbook.Worksheets.Add(BYTESCOUT_WORKSHEET_NAME);
            }
            worksheet.Visible = Bytescout.Spreadsheet.Constants.SHEETVISIBILITY.Hidden;
            #endregion

            //
            document.SaveToStreamXLS(ms);
            //
            document.Close();            
        }
       
        /// <summary>
        /// 
        /// </summary>
        /// <param name="worksheet">Planilha 1</param>
        private void setPlan(Worksheet worksheet) {

            #region Header
            #region Nome Cliente

            Pessoa pessoaCliente = new Pessoa();
            Cliente cliente = new Cliente();
            cliente.LoadByPrimaryKey(this.idCliente);
            pessoaCliente.LoadByPrimaryKey(cliente.IdPessoa.Value);

            worksheet.Cell("C3").ValueAsString = pessoaCliente.Nome.Trim();
            worksheet.Cell("C3").AlignmentHorizontal = AlignmentHorizontal.Left;
            #endregion

            #region Periodo
            worksheet.Cell("H4").ValueAsString = this.dataInicio.ToShortDateString() + " até " + this.dataFim.ToShortDateString();
            worksheet.Cell("H4").AlignmentHorizontal = AlignmentHorizontal.Left;
            #endregion

            #region Fundo
            Pessoa pessoaFundo = new Pessoa();
            cliente = new Cliente();
            cliente.LoadByPrimaryKey(this.idFundo);
            pessoaFundo.LoadByPrimaryKey(cliente.IdPessoa.Value);

            //
            worksheet.Cell("C4").ValueAsString = pessoaFundo.Apelido.Trim();
            worksheet.Cell("C4").AlignmentHorizontal = AlignmentHorizontal.Left;            
            #endregion
            #endregion

            #region Primeira Linha
            #region B
            worksheet.Cell("B7").ValueAsDateTime = this.dataInicio;
            #endregion

            #region C
            worksheet.Cell("C7").ValueAsString = "Saldo Inicial";
            #endregion

            #region D
            worksheet.Cell("D7").ValueAsString = "";           
            #endregion

            #region E
            worksheet.Cell("E7").ValueAsDouble = Convert.ToDouble(this.quantidadeSaldo);
            worksheet.Cell("E7").NumberFormatString = "#,##,#0.00000000";
            #endregion

            #region F
            worksheet.Cell("F7").ValueAsString = "";
            #endregion

            #region G
            worksheet.Cell("G7").ValueAsDouble = Convert.ToDouble(this.valorCusto);
            worksheet.Cell("G7").NumberFormatString = "#,##,#0.00";
            #endregion

            #region H
            worksheet.Cell("H7").ValueAsDouble = Convert.ToDouble(this.puCustoLiquido);
            worksheet.Cell("H7").NumberFormatString = "#,##,#0.00";            
            #endregion

            #region I
            worksheet.Cell("I7").ValueAsDouble = Convert.ToDouble(this.valorCusto);
            worksheet.Cell("I7").NumberFormatString = "#,##,#0.00";      
            #endregion

            #region J
            worksheet.Cell("J7").ValueAsDouble = Convert.ToDouble(this.puCustoLiquido);
            worksheet.Cell("J7").NumberFormatString = "#,##,#0.00";            
            #endregion
            #endregion

            #region for da Planilha

            #region Define layout (Bordas)
            int linha = 7; // Adiciona Linhas
            for (int i = 0; i < this.operacaoFundoCollection.Count; i++) 
            {
                //Add new row
                worksheet.Rows.Insert(linha, 1);
                
                worksheet.Rows[linha].Height = 22;
                
                worksheet.Rows[linha].Font = new Font("Calibri", 11, FontStyle.Regular);
                
                #region Bordas Esquerda
                //coluna B com borda esquerda
                worksheet.Rows[linha][1].LeftBorderStyle = LineStyle.Thin;
                //coluna C com borda esquerda
                worksheet.Rows[linha][2].LeftBorderStyle = LineStyle.Thin;
                //coluna D com borda esquerda
                worksheet.Rows[linha][3].LeftBorderStyle = LineStyle.Thin;
                //coluna E com borda esquerda
                worksheet.Rows[linha][4].LeftBorderStyle = LineStyle.Thin;
                //coluna F com borda esquerda
                worksheet.Rows[linha][5].LeftBorderStyle = LineStyle.Thin;
                //coluna G com borda esquerda
                worksheet.Rows[linha][6].LeftBorderStyle = LineStyle.Thin;
                //coluna H com borda esquerda
                worksheet.Rows[linha][7].LeftBorderStyle = LineStyle.Thin;
                //coluna I com borda esquerda
                worksheet.Rows[linha][8].LeftBorderStyle = LineStyle.Thin;
                //coluna J com borda esquerda
                worksheet.Rows[linha][9].LeftBorderStyle = LineStyle.Thin;
                
                //coluna L com borda direita
                worksheet.Rows[linha][9].RightBorderStyle = LineStyle.Thin;
                                
                #endregion

                #region Bordas Cima
                //coluna B com borda Cima
                worksheet.Rows[linha][1].TopBorderStyle = LineStyle.Thin;
                //coluna C com borda Cima
                worksheet.Rows[linha][2].TopBorderStyle = LineStyle.Thin;
                //coluna D com borda Cima
                worksheet.Rows[linha][3].TopBorderStyle = LineStyle.Thin;
                //coluna E com borda Cima
                worksheet.Rows[linha][4].TopBorderStyle = LineStyle.Thin;
                //coluna F com borda Cima
                worksheet.Rows[linha][5].TopBorderStyle = LineStyle.Thin;
                //coluna G com borda Cima
                worksheet.Rows[linha][6].TopBorderStyle = LineStyle.Thin;
                //coluna H com borda Cima
                worksheet.Rows[linha][7].TopBorderStyle = LineStyle.Thin;
                //coluna I com borda Cima
                worksheet.Rows[linha][8].TopBorderStyle = LineStyle.Thin;
                //coluna J com borda Cima
                worksheet.Rows[linha][9].TopBorderStyle = LineStyle.Thin;
                
                //coluna B com borda Baixo
                worksheet.Rows[linha][1].BottomBorderStyle = LineStyle.Thin;                
                //coluna C com borda Baixo
                worksheet.Rows[linha][2].BottomBorderStyle = LineStyle.Thin;
                //coluna D com borda Baixo
                worksheet.Rows[linha][3].BottomBorderStyle = LineStyle.Thin;
                //coluna E com borda Baixo
                worksheet.Rows[linha][4].BottomBorderStyle = LineStyle.Thin;
                //coluna F com borda Baixo
                worksheet.Rows[linha][5].BottomBorderStyle = LineStyle.Thin;
                //coluna G com borda Baixo
                worksheet.Rows[linha][6].BottomBorderStyle = LineStyle.Thin;
                //coluna H com borda Baixo
                worksheet.Rows[linha][7].BottomBorderStyle = LineStyle.Thin;
                //coluna I com borda Baixo
                worksheet.Rows[linha][8].BottomBorderStyle = LineStyle.Thin;
                //coluna J com borda Baixo
                worksheet.Rows[linha][9].BottomBorderStyle = LineStyle.Thin;                
                #endregion

                linha++;
            }
            #endregion

            linha = 7; // linha inicial dos dados
                       
            #region Dados
            decimal saldoAnterior = this.quantidadeSaldo;
            decimal valorCustoAnterior = this.valorCusto;
            for (int j = 0; j < this.operacaoFundoCollection.Count; j++) 
            {
                DateTime dataConversao = operacaoFundoCollection[j].DataConversao.Value;
                byte tipoOperacao = operacaoFundoCollection[j].TipoOperacao.Value;
                decimal quantidadeOperacao = operacaoFundoCollection[j].Quantidade.Value;
                decimal valorOperacao = operacaoFundoCollection[j].ValorLiquido.Value;
                string observacao = String.IsNullOrEmpty(operacaoFundoCollection[j].Observacao) ? "" : " - " + operacaoFundoCollection[j].Observacao;
                
                if (tipoOperacao == (byte)TipoOperacaoFundo.ComeCotas ||
                    tipoOperacao == (byte)TipoOperacaoFundo.ResgateCotas ||
                    tipoOperacao == (byte)TipoOperacaoFundo.ResgateCotasEspecial ||
                    tipoOperacao == (byte)TipoOperacaoFundo.ResgateLiquido ||
                    tipoOperacao == (byte)TipoOperacaoFundo.ResgateTotal ||
                    tipoOperacao == (byte)TipoOperacaoFundo.ResgateBruto)
                {
                    quantidadeOperacao = quantidadeOperacao * -1;
                    valorOperacao = valorOperacao * -1;
                }

                decimal saldo = saldoAnterior + quantidadeOperacao;
                saldoAnterior = saldo;

                
                worksheet.Rows[linha][1].ValueAsDateTime = dataConversao;  //COLUNA B
                //                
                string textoTipo = "";
                #region Texto do tipo de operação
                if (tipoOperacao == (byte)TipoOperacaoFundo.Amortizacao || tipoOperacao == (byte)TipoOperacaoFundo.AmortizacaoJuros || tipoOperacao == (byte)TipoOperacaoFundo.Juros)
                {
                    textoTipo = "Amortização/Juros";
                }
                else if (tipoOperacao == (byte)TipoOperacaoFundo.Aplicacao)
                {
                    textoTipo = "Aplicação";
                }
                else if (tipoOperacao == (byte)TipoOperacaoFundo.AplicacaoCotasEspecial)
                {
                    textoTipo = "Aplicação Especial";
                }
                else if (tipoOperacao == (byte)TipoOperacaoFundo.ComeCotas)
                {
                    textoTipo = "Come Cotas";
                }
                else if (tipoOperacao == (byte)TipoOperacaoFundo.ResgateBruto ||
                         tipoOperacao == (byte)TipoOperacaoFundo.ResgateCotas ||
                         tipoOperacao == (byte)TipoOperacaoFundo.ResgateLiquido ||
                         tipoOperacao == (byte)TipoOperacaoFundo.ResgateTotal)
                {
                    textoTipo = "Resgate";
                }
                else if (tipoOperacao == (byte)TipoOperacaoFundo.ResgateCotasEspecial)
                {
                    textoTipo = "Resgate Especial";
                }

                textoTipo = textoTipo + observacao;
                #endregion

                worksheet.Rows[linha][2].ValueAsString = textoTipo; //COLUNA C

                worksheet.Rows[linha][3].NumberFormatString = "#,##,#0.00000000";  //COLUNA D 
                worksheet.Rows[linha][3].ValueAsDouble = Convert.ToDouble(quantidadeOperacao);

                worksheet.Rows[linha][4].NumberFormatString = "#,##,#0.00000000";  //COLUNA E
                worksheet.Rows[linha][4].ValueAsDouble = Convert.ToDouble(saldo);

                worksheet.Rows[linha][5].NumberFormatString = "#,##,#0.00";  //COLUNA F
                worksheet.Rows[linha][5].ValueAsDouble = Convert.ToDouble(valorOperacao);

                decimal valorCustoOperacao = 0;
                if (tipoOperacao == (byte)TipoOperacaoFundo.Aplicacao)
                {
                    valorCustoOperacao = valorOperacao;
                }
                else
                {
                    valorCustoOperacao = saldoAnterior == 0 ? 0 : Math.Round((valorCustoAnterior / saldoAnterior) * quantidadeOperacao, 2);
                }

                decimal puOperacao = valorCustoOperacao / quantidadeOperacao;
                
                worksheet.Rows[linha][6].NumberFormatString = "#,##,#0.00";  //COLUNA G
                worksheet.Rows[linha][6].ValueAsDouble = Convert.ToDouble(valorCustoOperacao);

                worksheet.Rows[linha][7].NumberFormatString = "#,##,#0.00";  //COLUNA H
                worksheet.Rows[linha][7].ValueAsDouble = Convert.ToDouble(puOperacao);

                decimal custoFinal = 0;
                if (this.operacaoFundoCollection.Count == 0 ||
                    j == (this.operacaoFundoCollection.Count - 1) ||
                    dataConversao != operacaoFundoCollection[j+1].DataConversao.Value)
                {
                    PosicaoFundoHistoricoCollection coll = new PosicaoFundoHistoricoCollection();
                    coll.Query.Select(coll.Query.Quantidade,
                                      coll.Query.CotaAplicacao);
                    coll.Query.Where(coll.Query.IdCliente.Equal(this.idCliente),
                                     coll.Query.IdCarteira.Equal(this.idFundo),
                                     coll.Query.DataHistorico.Equal(dataConversao));
                    coll.Query.Load();

                    foreach (PosicaoFundoHistorico posicaoFundoHistorico in coll)
                    {
                        custoFinal += Math.Round(posicaoFundoHistorico.Quantidade.Value * posicaoFundoHistorico.CotaAplicacao.Value, 2);
                    }                    
                }
                else
                {
                    custoFinal = valorCustoAnterior + valorCustoOperacao;
                }
                
                valorCustoAnterior = custoFinal;

                decimal puCustoFinal = saldo == 0 ? 0 : custoFinal / saldo;

                worksheet.Rows[linha][8].NumberFormatString = "#,##,#0.00";  //COLUNA I
                worksheet.Rows[linha][8].ValueAsDouble = Convert.ToDouble(custoFinal);

                worksheet.Rows[linha][9].NumberFormatString = "#,##,#0.00";  //COLUNA J
                worksheet.Rows[linha][9].ValueAsDouble = Convert.ToDouble(puCustoFinal);

                #region Alinhamentos B/C/D/E/F/G/H/I/J
                worksheet.Rows[linha][1].AlignmentHorizontal = Bytescout.Spreadsheet.Constants.AlignmentHorizontal.Left;   // B
                worksheet.Rows[linha][2].AlignmentHorizontal = Bytescout.Spreadsheet.Constants.AlignmentHorizontal.Left;   // C
                worksheet.Rows[linha][3].AlignmentHorizontal = Bytescout.Spreadsheet.Constants.AlignmentHorizontal.Right;  // D
                worksheet.Rows[linha][4].AlignmentHorizontal = Bytescout.Spreadsheet.Constants.AlignmentHorizontal.Right;  // E                
                worksheet.Rows[linha][5].AlignmentHorizontal = Bytescout.Spreadsheet.Constants.AlignmentHorizontal.Right;  // F
                worksheet.Rows[linha][6].AlignmentHorizontal = Bytescout.Spreadsheet.Constants.AlignmentHorizontal.Right;  // G                
                worksheet.Rows[linha][7].AlignmentHorizontal = Bytescout.Spreadsheet.Constants.AlignmentHorizontal.Right;  // H
                worksheet.Rows[linha][8].AlignmentHorizontal = Bytescout.Spreadsheet.Constants.AlignmentHorizontal.Right;  // I
                worksheet.Rows[linha][9].AlignmentHorizontal = Bytescout.Spreadsheet.Constants.AlignmentHorizontal.Right;  // J                
                #endregion
            
                linha++;
            }
            #endregion

            #endregion
        }
    }
}