﻿using FileHelpers;
using System;
using Financial.Contabil;
using System.Collections.Generic;
using System.IO;
using Financial.WebConfigConfiguration;
using System.Text;
using Financial.Fundo;
using System.Globalization;
using Financial.InvestidorCotista;
using Financial.Investidor;
using EntitySpaces.Interfaces;

namespace Financial.Export {

    /// <summary>
    /// Summary description for CotaGradual
    /// </summary>
    public class CotaGradual {

        [DelimitedRecord(";")]
        public class CotaGradualDS {

            public int idCarteira;

            [FieldConverter(ConverterKind.Date, "dd/MM/yyyy")] 
            public DateTime dataCota;

            [FieldConverter(typeof(CotaGradualDS.EightDecimalConverter))]
            public decimal valorCota;

            [FieldConverter(ConverterKind.Decimal, ",")] 
            //[FieldConverter(typeof(CotaGradualDS.TwoDecimalConverter))]
            public decimal plCota;

            public int numCotistas;

            #region
            internal class TwoDecimalConverter : ConverterBase {
                public override object StringToField(string from) {
                    decimal res = Convert.ToDecimal(from);
                    //return res / 100;
                    return res;
                }

                public override string FieldToString(object from) {
                    //decimal d = (decimal)from;
                    //return Math.Round(d * 100).ToString();

                    decimal res = Convert.ToDecimal(from);
                    return res.ToString("N2").Replace(".", ",");
                }
            }

            internal class EightDecimalConverter : ConverterBase {
                public override object StringToField(string from) {
                    decimal res = Convert.ToDecimal(from);
                    //return res / 100000000;
                    return res;
                }

                public override string FieldToString(object from) {
                    //decimal d = (decimal)from;
                    //return Math.Round(d * 100000000).ToString();

                    decimal res = Convert.ToDecimal(from);
                    return res.ToString("N8").Replace(".", ",");
                }
            }
            #endregion
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dataLancamento"></param>
        /// <param name="ms">Saida: Memory Stream do arquivo de Cotas</param>
        /// <param name="nomeArquivo">Saida: Nome do Arquivo De Cotas</param>        
        public void ExportaCota(DateTime dataLancamento, out MemoryStream ms, out string nomeArquivo) {
            //NumberFormatInfo provider_two = new NumberFormatInfo();
            //provider_two.NumberDecimalDigits = 2;

            //NumberFormatInfo provider_eight = new NumberFormatInfo();
            //provider_eight.NumberDecimalDigits = 8;

            // out
            ms = new MemoryStream();
            nomeArquivo = "";

            #region HistoricoCota
            HistoricoCotaCollection h = new HistoricoCotaCollection();
            h.Query.Select(h.Query.Data, h.Query.IdCarteira, h.Query.PLFechamento, h.Query.CotaFechamento)
                   .Where(h.Query.Data == dataLancamento)
                   .OrderBy(h.Query.IdCarteira.Ascending);

            h.Query.Load();

            if (h.Count == 0) {
                throw new Exception("Não há dados para exportar.");
            }
            #endregion

            // out
            nomeArquivo = "Cota" + dataLancamento.ToString("ddMMyyyy") + ".txt";

            List<CotaGradualDS> cotaDS = new List<CotaGradualDS>();

            for (int i = 0; i < h.Count; i++) {
                CotaGradualDS cota = new CotaGradualDS();
                cota.idCarteira = h[i].IdCarteira.Value;
                cota.dataCota = h[i].Data.Value;
                
                //cota.valorCota = Convert.ToDecimal(h[i].CotaFechamento.Value, provider_two);
                //cota.plCota = Convert.ToDecimal(h[i].PLFechamento.Value, provider_eight);

                cota.valorCota = h[i].CotaFechamento.Value;
                cota.plCota = h[i].PLFechamento.Value;


                Cliente cliente = new Cliente();
                List<esQueryItem> campos = new List<esQueryItem>();
                campos.Add(cliente.Query.DataDia);
                cliente.LoadByPrimaryKey(campos, cota.idCarteira);

                cota.numCotistas = dataLancamento >= cliente.DataDia.Value
                    ? new PosicaoCotista().RetornaCountNumeroCotistas(cota.idCarteira)
                    : new PosicaoCotistaHistorico().RetornaCountNumeroCotistas(cota.idCarteira, dataLancamento);

                //
                cotaDS.Add(cota);
            }

            FileHelperEngine engine = new FileHelperEngine(typeof(CotaGradualDS));

            StreamWriter arquivo = new StreamWriter(ms, Encoding.GetEncoding("ISO-8859-1"));
            engine.Encoding = Encoding.GetEncoding("ISO-8859-1");
            //
            engine.WriteStream(arquivo, cotaDS);

            //Fecha o arquivo
            arquivo.Flush();
        }
    }
}