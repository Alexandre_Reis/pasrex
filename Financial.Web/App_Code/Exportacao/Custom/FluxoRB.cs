﻿using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Financial.Fundo;
using System.Collections.Generic;
using Financial.Common;
using Financial.Investidor;
using Financial.Investidor.Enums;
using Financial.Util;
using Financial.Common.Enums;
using Financial.RendaFixa;
using Financial.RendaFixa.Enums;

namespace Financial.Export
{
    
    public class FluxoRB
    {
        private string titulo1;
        public string Titulo1
        {
            get { return titulo1; }
            set { titulo1 = value; }
        }

        private string titulo2;
        public string Titulo2
        {
            get { return titulo2; }
            set { titulo2 = value; }
        }

        private string titulo3;
        public string Titulo3
        {
            get { return titulo3; }
            set { titulo3 = value; }
        }

        private string titulo4;
        public string Titulo4
        {
            get { return titulo4; }
            set { titulo4 = value; }
        }

        private string titulo5;
        public string Titulo5
        {
            get { return titulo5; }
            set { titulo5 = value; }
        }

        private string titulo6;
        public string Titulo6
        {
            get { return titulo6; }
            set { titulo6 = value; }
        }

        private string titulo7;
        public string Titulo7
        {
            get { return titulo7; }
            set { titulo7 = value; }
        }

        private string titulo8;
        public string Titulo8
        {
            get { return titulo8; }
            set { titulo8 = value; }
        }

        private decimal correcao1;
        public decimal Correcao1
        {
            get { return correcao1; }
            set { correcao1 = value; }
        }

        private decimal correcao2;
        public decimal Correcao2
        {
            get { return correcao2; }
            set { correcao2 = value; }
        }

        private decimal correcao3;
        public decimal Correcao3
        {
            get { return correcao3; }
            set { correcao3 = value; }
        }

        private decimal correcao4;
        public decimal Correcao4
        {
            get { return correcao4; }
            set { correcao4 = value; }
        }

        private decimal correcao5;
        public decimal Correcao5
        {
            get { return correcao5; }
            set { correcao5 = value; }
        }

        private decimal correcao6;
        public decimal Correcao6
        {
            get { return correcao6; }
            set { correcao6 = value; }
        }

        private decimal correcao7;
        public decimal Correcao7
        {
            get { return correcao7; }
            set { correcao7 = value; }
        }

        private decimal correcao8;
        public decimal Correcao8
        {
            get { return correcao8; }
            set { correcao8 = value; }
        }

        private decimal juroCarregado1;
        public decimal JuroCarregado1
        {
            get { return juroCarregado1; }
            set { juroCarregado1 = value; }
        }

        private decimal juroCarregado2;
        public decimal JuroCarregado2
        {
            get { return juroCarregado2; }
            set { juroCarregado2 = value; }
        }

        private decimal juroCarregado3;
        public decimal JuroCarregado3
        {
            get { return juroCarregado3; }
            set { juroCarregado3 = value; }
        }

        private decimal juroCarregado4;
        public decimal JuroCarregado4
        {
            get { return juroCarregado4; }
            set { juroCarregado4 = value; }
        }

        private decimal juroCarregado5;
        public decimal JuroCarregado5
        {
            get { return juroCarregado5; }
            set { juroCarregado5 = value; }
        }

        private decimal juroCarregado6;
        public decimal JuroCarregado6
        {
            get { return juroCarregado6; }
            set { juroCarregado6 = value; }
        }

        private decimal juroCarregado7;
        public decimal JuroCarregado7
        {
            get { return juroCarregado7; }
            set { juroCarregado7 = value; }
        }

        private decimal juroCarregado8;
        public decimal JuroCarregado8
        {
            get { return juroCarregado8; }
            set { juroCarregado8 = value; }
        }

        private DateTime dataAmort1;
        public DateTime DataAmort1
        {
            get { return dataAmort1; }
            set { dataAmort1 = value; }
        }

        private DateTime dataAmort2;
        public DateTime DataAmort2
        {
            get { return dataAmort2; }
            set { dataAmort2 = value; }
        }

        private DateTime dataAmort3;
        public DateTime DataAmort3
        {
            get { return dataAmort3; }
            set { dataAmort3 = value; }
        }

        private DateTime dataAmort4;
        public DateTime DataAmort4
        {
            get { return dataAmort4; }
            set { dataAmort4 = value; }
        }

        private DateTime dataAmort5;
        public DateTime DataAmort5
        {
            get { return dataAmort5; }
            set { dataAmort5 = value; }
        }

        private DateTime dataAmort6;
        public DateTime DataAmort6
        {
            get { return dataAmort6; }
            set { dataAmort6 = value; }
        }

        private DateTime dataAmort7;
        public DateTime DataAmort7
        {
            get { return dataAmort7; }
            set { dataAmort7 = value; }
        }

        private DateTime dataAmort8;
        public DateTime DataAmort8
        {
            get { return dataAmort8; }
            set { dataAmort8 = value; }
        }

        private DateTime dataJuro1;
        public DateTime DataJuro1
        {
            get { return dataJuro1; }
            set { dataJuro1 = value; }
        }

        private DateTime dataJuro2;
        public DateTime DataJuro2
        {
            get { return dataJuro2; }
            set { dataJuro2 = value; }
        }

        private DateTime dataJuro3;
        public DateTime DataJuro3
        {
            get { return dataJuro3; }
            set { dataJuro3 = value; }
        }

        private DateTime dataJuro4;
        public DateTime DataJuro4
        {
            get { return dataJuro4; }
            set { dataJuro4 = value; }
        }

        private DateTime dataJuro5;
        public DateTime DataJuro5
        {
            get { return dataJuro5; }
            set { dataJuro5 = value; }
        }

        private DateTime dataJuro6;
        public DateTime DataJuro6
        {
            get { return dataJuro6; }
            set { dataJuro6 = value; }
        }

        private DateTime dataJuro7;
        public DateTime DataJuro7
        {
            get { return dataJuro7; }
            set { dataJuro7 = value; }
        }

        private DateTime dataJuro8;
        public DateTime DataJuro8
        {
            get { return dataJuro8; }
            set { dataJuro8 = value; }
        }

        
        public class Fluxo
        {
            private DateTime data;
            public DateTime Data
            {
                get { return data; }
                set { data = value; }
            }

            decimal fluxo1;
            public decimal Fluxo1
            {
                get { return fluxo1; }
                set { fluxo1 = value; }
            }

            decimal fluxo2;
            public decimal Fluxo2
            {
                get { return fluxo2; }
                set { fluxo2 = value; }
            }

            decimal fluxo3;
            public decimal Fluxo3
            {
                get { return fluxo3; }
                set { fluxo3 = value; }
            }

            decimal fluxo4;
            public decimal Fluxo4
            {
                get { return fluxo4; }
                set { fluxo4 = value; }
            }

            decimal fluxo5;
            public decimal Fluxo5
            {
                get { return fluxo5; }
                set { fluxo5 = value; }
            }

            decimal fluxo6;
            public decimal Fluxo6
            {
                get { return fluxo6; }
                set { fluxo6 = value; }
            }

            decimal fluxo7;
            public decimal Fluxo7
            {
                get { return fluxo7; }
                set { fluxo7 = value; }
            }

            decimal fluxo8;
            public decimal Fluxo8
            {
                get { return fluxo8; }
                set { fluxo8 = value; }
            }

            decimal total;
            public decimal Total
            {
                get { return total; }
                set { total = value; }
            }

            decimal acumulado;
            public decimal Acumulado
            {
                get { return acumulado; }
                set { acumulado = value; }
            }

            decimal plInicial;
            public decimal PlInicial
            {
                get { return plInicial; }
                set { plInicial = value; }
            }

            decimal valorizacao;
            public decimal Valorizacao
            {
                get { return valorizacao; }
                set { valorizacao = value; }
            }

            decimal amortizacao;
            public decimal Amortizacao
            {
                get { return amortizacao; }
                set { amortizacao = value; }
            }

            decimal plFinal;
            public decimal PlFinal
            {
                get { return plFinal; }
                set { plFinal = value; }
            }
        }
                
        public List<Fluxo> RetornaListaFluxo(DateTime data, int idCliente)
        {
            Cliente cliente = new Cliente();
            cliente.LoadByPrimaryKey(idCliente);

            PosicaoRendaFixaHistoricoCollection posicaoRendaFixaHistoricoCollection = new PosicaoRendaFixaHistoricoCollection();
            PosicaoRendaFixaHistoricoQuery posicaoRendaFixaHistoricoQuery = new PosicaoRendaFixaHistoricoQuery("P");
            AgendaRendaFixaQuery agendaRendaFixaQuery = new AgendaRendaFixaQuery("A");
            posicaoRendaFixaHistoricoQuery.Select(posicaoRendaFixaHistoricoQuery.IdTitulo);
            posicaoRendaFixaHistoricoQuery.InnerJoin(agendaRendaFixaQuery).On(agendaRendaFixaQuery.IdTitulo == posicaoRendaFixaHistoricoQuery.IdTitulo);
            posicaoRendaFixaHistoricoQuery.Where(posicaoRendaFixaHistoricoQuery.DataHistorico.Equal(data),
                                                 posicaoRendaFixaHistoricoQuery.IdCliente.Equal(idCliente),
                                                 agendaRendaFixaQuery.DataEvento.GreaterThan(data));
            posicaoRendaFixaHistoricoQuery.es.Distinct = true;
            posicaoRendaFixaHistoricoCollection.Load(posicaoRendaFixaHistoricoQuery);

            List<int> listaTitulos = new List<int>();
            foreach (PosicaoRendaFixaHistorico posicaoRendaFixaHistoricoLista in posicaoRendaFixaHistoricoCollection)
            {
                if (listaTitulos.Count <= 8)
                {
                    listaTitulos.Add(posicaoRendaFixaHistoricoLista.IdTitulo.Value);
                }
            }
            
            if (listaTitulos.Count == 0)
            {
                throw new Exception("Não há títulos de renda fixa para este cliente nesta data.");
            }

            AgendaRendaFixa agendaRendaFixa = new AgendaRendaFixa();
            agendaRendaFixa.Query.Select(agendaRendaFixa.Query.DataEvento.Max());
            agendaRendaFixa.Query.Where(agendaRendaFixa.Query.IdTitulo.In(listaTitulos),
                                        agendaRendaFixa.Query.DataEvento.GreaterThan(data));
            agendaRendaFixa.Query.Load();

            if (!agendaRendaFixa.DataEvento.HasValue)
            {
                throw new Exception("Não há títulos com fluxo futuro nesta data.");
            }

            #region Preenche titulos do header, tb aproveita para inserir a correção do indice e o juro carregado associado ao titulo
            TituloRendaFixa tituloRendaFixa = new TituloRendaFixa();
            if (listaTitulos.Count >= 1)
            {
                tituloRendaFixa = new TituloRendaFixa();
                tituloRendaFixa.LoadByPrimaryKey(listaTitulos[0]);
                this.titulo1 = tituloRendaFixa.Descricao;
                this.correcao1 = this.CalculaFatorCorrecao(tituloRendaFixa, data);
                this.juroCarregado1 = this.RetornaValorJurosCarregados(data, tituloRendaFixa.IdTitulo.Value);
                this.dataJuro1 = this.RetornaDataProxEvento(data, tituloRendaFixa.IdTitulo.Value, true);
                this.dataAmort1 = this.RetornaDataProxEvento(data, tituloRendaFixa.IdTitulo.Value, false);
            }
            if (listaTitulos.Count >= 2)
            {
                tituloRendaFixa = new TituloRendaFixa();
                tituloRendaFixa.LoadByPrimaryKey(listaTitulos[1]);
                this.titulo2 = tituloRendaFixa.Descricao;
                this.correcao2 = this.CalculaFatorCorrecao(tituloRendaFixa, data);
                this.juroCarregado2 = this.RetornaValorJurosCarregados(data, tituloRendaFixa.IdTitulo.Value);
                this.dataJuro2 = this.RetornaDataProxEvento(data, tituloRendaFixa.IdTitulo.Value, true);
                this.dataAmort2 = this.RetornaDataProxEvento(data, tituloRendaFixa.IdTitulo.Value, false);
            }
            if (listaTitulos.Count >= 3)
            {
                tituloRendaFixa = new TituloRendaFixa();
                tituloRendaFixa.LoadByPrimaryKey(listaTitulos[2]);
                this.titulo3 = tituloRendaFixa.Descricao;
                this.correcao3 = this.CalculaFatorCorrecao(tituloRendaFixa, data);
                this.juroCarregado3 = this.RetornaValorJurosCarregados(data, tituloRendaFixa.IdTitulo.Value);
                this.dataJuro3 = this.RetornaDataProxEvento(data, tituloRendaFixa.IdTitulo.Value, true);
                this.dataAmort3 = this.RetornaDataProxEvento(data, tituloRendaFixa.IdTitulo.Value, false);
            }
            if (listaTitulos.Count >= 4)
            {
                tituloRendaFixa = new TituloRendaFixa();
                tituloRendaFixa.LoadByPrimaryKey(listaTitulos[3]);
                this.titulo4 = tituloRendaFixa.Descricao;
                this.correcao4 = this.CalculaFatorCorrecao(tituloRendaFixa, data);
                this.juroCarregado4 = this.RetornaValorJurosCarregados(data, tituloRendaFixa.IdTitulo.Value);
                this.dataJuro4 = this.RetornaDataProxEvento(data, tituloRendaFixa.IdTitulo.Value, true);
                this.dataAmort4 = this.RetornaDataProxEvento(data, tituloRendaFixa.IdTitulo.Value, false);
            }
            if (listaTitulos.Count >= 5)
            {
                tituloRendaFixa = new TituloRendaFixa();
                tituloRendaFixa.LoadByPrimaryKey(listaTitulos[4]);
                this.titulo5 = tituloRendaFixa.Descricao;
                this.correcao5 = this.CalculaFatorCorrecao(tituloRendaFixa, data);
                this.juroCarregado5 = this.RetornaValorJurosCarregados(data, tituloRendaFixa.IdTitulo.Value);
                this.dataJuro5 = this.RetornaDataProxEvento(data, tituloRendaFixa.IdTitulo.Value, true);
                this.dataAmort5 = this.RetornaDataProxEvento(data, tituloRendaFixa.IdTitulo.Value, false);
            }
            if (listaTitulos.Count >= 6)
            {
                tituloRendaFixa = new TituloRendaFixa();
                tituloRendaFixa.LoadByPrimaryKey(listaTitulos[5]);
                this.titulo6 = tituloRendaFixa.Descricao;
                this.correcao1 = this.CalculaFatorCorrecao(tituloRendaFixa, data);
                this.juroCarregado6 = this.RetornaValorJurosCarregados(data, tituloRendaFixa.IdTitulo.Value);
                this.dataJuro6 = this.RetornaDataProxEvento(data, tituloRendaFixa.IdTitulo.Value, true);
                this.dataAmort6 = this.RetornaDataProxEvento(data, tituloRendaFixa.IdTitulo.Value, false);
            }
            if (listaTitulos.Count >= 7)
            {
                tituloRendaFixa = new TituloRendaFixa();
                tituloRendaFixa.LoadByPrimaryKey(listaTitulos[6]);
                this.titulo7 = tituloRendaFixa.Descricao;
                this.correcao7 = this.CalculaFatorCorrecao(tituloRendaFixa, data);
                this.juroCarregado7 = this.RetornaValorJurosCarregados(data, tituloRendaFixa.IdTitulo.Value);
                this.dataJuro7 = this.RetornaDataProxEvento(data, tituloRendaFixa.IdTitulo.Value, true);
                this.dataAmort7 = this.RetornaDataProxEvento(data, tituloRendaFixa.IdTitulo.Value, false);
            }
            if (listaTitulos.Count >= 8)
            {
                tituloRendaFixa = new TituloRendaFixa();
                tituloRendaFixa.LoadByPrimaryKey(listaTitulos[7]);
                this.titulo8 = tituloRendaFixa.Descricao;
                this.correcao8 = this.CalculaFatorCorrecao(tituloRendaFixa, data);
                this.juroCarregado8 = this.RetornaValorJurosCarregados(data, tituloRendaFixa.IdTitulo.Value);
                this.dataJuro8 = this.RetornaDataProxEvento(data, tituloRendaFixa.IdTitulo.Value, true);
                this.dataAmort8 = this.RetornaDataProxEvento(data, tituloRendaFixa.IdTitulo.Value, false);
            }
            #endregion

            DateTime dataUltima = agendaRendaFixa.DataEvento.Value;
            DateTime dataAux = data;

            List<Fluxo> listaFluxos = new List<Fluxo>();
            
            decimal totalAcumulado = 0;
            decimal plInicial = 0;
            decimal plFinal = 0;

            decimal valorJuroCarregado = 0;
            while (dataAux.Year <= dataUltima.Year)
            {
                Fluxo fluxo = new Fluxo();
                fluxo.Data = dataAux;

                DateTime dataInicioMes = Calendario.RetornaPrimeiroDiaCorridoMes(dataAux, 0);

                if (dataInicioMes < data)
                {
                    dataInicioMes = data;
                }

                DateTime dataFimMes = Calendario.RetornaUltimoDiaCorridoMes(dataAux, 0);

                decimal totalAmortizacao = 0;
                decimal totalJuros = 0;
                
                int i = 0;
                decimal valorJurosCarregados = 0;
                bool amortizacaoAjustada = false;
                bool juroAjustado = false;
                while (i < 8 && i < listaTitulos.Count) 
                {
                    int idTitulo = listaTitulos[i];
                    
                    decimal valorAmortizacao = this.RetornaValorAmortizacao(dataInicioMes, dataFimMes, idTitulo);
                    decimal valorJuros = this.RetornaValorJuros(dataInicioMes, dataFimMes, idTitulo);

                    #region Ajuste Juros Carregados
                    if (i == 0)
                    {
                        if (this.dataAmort1.Month == dataInicioMes.Month && this.dataAmort1.Year == dataInicioMes.Year)
                        {
                            valorAmortizacao += this.juroCarregado1;
                        }

                        if (this.dataJuro1.Month == dataInicioMes.Month && this.dataJuro1.Year == dataInicioMes.Year)
                        {
                            valorJuros -= this.juroCarregado1;
                        }
                    }
                    else if (i == 1)
                    {
                        if (this.dataAmort2.Month == dataInicioMes.Month && this.dataAmort2.Year == dataInicioMes.Year)
                        {
                            valorAmortizacao += this.juroCarregado2;
                        }

                        if (this.dataJuro2.Month == dataInicioMes.Month && this.dataJuro2.Year == dataInicioMes.Year)
                        {
                            valorJuros -= this.juroCarregado2;
                        }
                    }
                    else if (i == 2)
                    {
                        if (this.dataAmort3.Month == dataInicioMes.Month && this.dataAmort3.Year == dataInicioMes.Year)
                        {
                            valorAmortizacao += this.juroCarregado3;
                        }

                        if (this.dataJuro3.Month == dataInicioMes.Month && this.dataJuro3.Year == dataInicioMes.Year)
                        {
                            valorJuros -= this.juroCarregado3;
                        }
                    }
                    else if (i == 3)
                    {
                        if (this.dataAmort4.Month == dataInicioMes.Month && this.dataAmort4.Year == dataInicioMes.Year)
                        {
                            valorAmortizacao += this.juroCarregado4;
                        }

                        if (this.dataJuro4.Month == dataInicioMes.Month && this.dataJuro4.Year == dataInicioMes.Year)
                        {
                            valorJuros -= this.juroCarregado4;
                        }
                    }
                    else if (i == 4)
                    {
                        if (this.dataAmort5.Month == dataInicioMes.Month && this.dataAmort5.Year == dataInicioMes.Year)
                        {
                            valorAmortizacao += this.juroCarregado5;
                        }

                        if (this.dataJuro5.Month == dataInicioMes.Month && this.dataJuro5.Year == dataInicioMes.Year)
                        {
                            valorJuros -= this.juroCarregado5;
                        }
                    }
                    else if (i == 5)
                    {
                        if (this.dataAmort6.Month == dataInicioMes.Month && this.dataAmort6.Year == dataInicioMes.Year)
                        {
                            valorAmortizacao += this.juroCarregado6;
                        }

                        if (this.dataJuro6.Month == dataInicioMes.Month && this.dataJuro6.Year == dataInicioMes.Year)
                        {
                            valorJuros -= this.juroCarregado6;
                        }
                    }
                    else if (i == 6)
                    {
                        if (this.dataAmort7.Month == dataInicioMes.Month && this.dataAmort7.Year == dataInicioMes.Year)
                        {
                            valorAmortizacao += this.juroCarregado7;
                        }

                        if (this.dataJuro7.Month == dataInicioMes.Month && this.dataJuro7.Year == dataInicioMes.Year)
                        {
                            valorJuros -= this.juroCarregado7;
                        }
                    }
                    else if (i == 7)
                    {
                        if (this.dataAmort8.Month == dataInicioMes.Month && this.dataAmort8.Year == dataInicioMes.Year)
                        {
                            valorAmortizacao += this.juroCarregado8;
                        }

                        if (this.dataJuro8.Month == dataInicioMes.Month && this.dataJuro8.Year == dataInicioMes.Year)
                        {
                            valorJuros -= this.juroCarregado8;
                        }
                    }
                    #endregion

                    decimal fatorCorrecao = 1;
                    #region Correção
                    if (i == 0)
                    {
                        fatorCorrecao = this.correcao1;
                    }
                    else if (i == 1)
                    {
                        fatorCorrecao = this.correcao2;
                    }
                    else if (i == 2)
                    {
                        fatorCorrecao = this.correcao3;
                    }
                    else if (i == 3)
                    {
                        fatorCorrecao = this.correcao4;
                    }
                    else if (i == 4)
                    {
                        fatorCorrecao = this.correcao5;
                    }
                    else if (i == 5)
                    {
                        fatorCorrecao = this.correcao6;
                    }
                    else if (i == 6)
                    {
                        fatorCorrecao = this.correcao7;
                    }
                    else if (i == 7)
                    {
                        fatorCorrecao = this.correcao8;
                    }
                    #endregion

                    valorAmortizacao = valorAmortizacao * fatorCorrecao;
                    valorJuros = valorJuros * fatorCorrecao;
                    
                    decimal quantidade = this.RetornaQuantidadePosicao(data, idTitulo, idCliente); //Pouco otimizado mas é para facilitar o código, evitar torna-lo complexo
                    decimal valorEvento = Math.Round(quantidade * valorAmortizacao, 2) + Math.Round(quantidade * valorJuros, 2);

                    if (data == dataAux) //Apenas para 1a data busca o valor de mercado, nas seguintes ele passa a ser calculado deduzindo das amortizações
                    {
                        decimal valorMercado = 0;
                        CotacaoMercadoFiduciario cotacaoMercadoFiduciario = new CotacaoMercadoFiduciario();
                        if (cotacaoMercadoFiduciario.LoadByPrimaryKey(idTitulo, dataAux))
                        {
                            valorMercado = Math.Round(quantidade * cotacaoMercadoFiduciario.Cotacao.Value, 2);
                            plInicial += valorMercado;
                        }
                    }

                    #region Fluxo
                    if (i == 0)
                    {
                        fluxo.Fluxo1 = valorEvento;
                    }
                    else if (i == 1)
                    {
                        fluxo.Fluxo2 = valorEvento;
                    }
                    else if (i == 2)
                    {
                        fluxo.Fluxo3 = valorEvento;
                    }
                    else if (i == 3)
                    {
                        fluxo.Fluxo4 = valorEvento;
                    }
                    else if (i == 4)
                    {
                        fluxo.Fluxo5 = valorEvento;
                    }
                    else if (i == 5)
                    {
                        fluxo.Fluxo6 = valorEvento;
                    }
                    else if (i == 6)
                    {
                        fluxo.Fluxo7 = valorEvento;
                    }
                    else if (i == 7)
                    {
                        fluxo.Fluxo8 = valorEvento;
                    }
                    #endregion

                    totalAmortizacao += Math.Round(quantidade * valorAmortizacao, 2);
                    totalJuros += Math.Round(quantidade * valorJuros, 2);

                    i++;
                }

                decimal totalEventos = totalAmortizacao + totalJuros;
                totalAcumulado += totalEventos;

                fluxo.Total = totalEventos;
                fluxo.Acumulado = totalAcumulado;
                fluxo.Valorizacao = totalJuros;
                fluxo.Amortizacao = totalAmortizacao;
                
                if (data == dataAux) {
                    fluxo.PlInicial = plInicial;
                }
                else {
                    fluxo.PlInicial = plFinal; //Busca o PLFinal da data anterior
                }

                plFinal = fluxo.PlInicial - totalAmortizacao;

                if (dataAux >= dataUltima && plFinal < 100M) //residuo de 100 reais de tolerancia
                {
                    fluxo.Amortizacao += plFinal;
                    plFinal = 0;
                }

                fluxo.PlFinal = plFinal;
                
                listaFluxos.Add(fluxo);

                dataAux = dataAux.AddMonths(1);
            }

            return listaFluxos;
        }

        private decimal RetornaValorAmortizacao(DateTime dataInicioMes, DateTime dataFimMes, int idTitulo)
        {
            AgendaRendaFixa agendaRendaFixaAmort = new AgendaRendaFixa();
            agendaRendaFixaAmort.Query.Select(agendaRendaFixaAmort.Query.Valor.Sum());
            agendaRendaFixaAmort.Query.Where(agendaRendaFixaAmort.Query.IdTitulo.Equal(idTitulo),
                                        agendaRendaFixaAmort.Query.DataEvento.GreaterThanOrEqual(dataInicioMes),
                                        agendaRendaFixaAmort.Query.DataEvento.LessThanOrEqual(dataFimMes),
                                        agendaRendaFixaAmort.Query.TipoEvento.In((byte)TipoEventoTitulo.Amortizacao,
                                                                            (byte)TipoEventoTitulo.AmortizacaoCorrigida,
                                                                            (byte)TipoEventoTitulo.PagamentoPrincipal));
            agendaRendaFixaAmort.Query.Load();

            decimal valorAmortizacao = 0;
            if (agendaRendaFixaAmort.Valor.HasValue)
            {
                valorAmortizacao = agendaRendaFixaAmort.Valor.Value;
            }

            return valorAmortizacao;
        }

        private decimal RetornaValorJuros(DateTime dataInicioMes, DateTime dataFimMes, int idTitulo)
        {
            AgendaRendaFixa agendaRendaFixaJuros = new AgendaRendaFixa();
            agendaRendaFixaJuros.Query.Select(agendaRendaFixaJuros.Query.Valor.Sum());
            agendaRendaFixaJuros.Query.Where(agendaRendaFixaJuros.Query.IdTitulo.Equal(idTitulo),
                                        agendaRendaFixaJuros.Query.DataEvento.GreaterThanOrEqual(dataInicioMes),
                                        agendaRendaFixaJuros.Query.DataEvento.LessThanOrEqual(dataFimMes),
                                        agendaRendaFixaJuros.Query.TipoEvento.NotIn((byte)TipoEventoTitulo.Amortizacao,
                                                                            (byte)TipoEventoTitulo.AmortizacaoCorrigida,
                                                                            (byte)TipoEventoTitulo.PagamentoPrincipal));
            agendaRendaFixaJuros.Query.Load();

            decimal valorJuros = 0;
            if (agendaRendaFixaJuros.Valor.HasValue)
            {
                valorJuros = agendaRendaFixaJuros.Valor.Value;
            }

            return valorJuros;
        }

        private decimal RetornaValorJurosCarregados(DateTime data, int idTitulo)
        {
            TituloRendaFixa tituloRendaFixa = new TituloRendaFixa();
            tituloRendaFixa.LoadByPrimaryKey(idTitulo);

            AgendaRendaFixa agendaRendaFixaJuros = new AgendaRendaFixa();
            agendaRendaFixaJuros.Query.Select(agendaRendaFixaJuros.Query.DataEvento.Max());
            agendaRendaFixaJuros.Query.Where(agendaRendaFixaJuros.Query.IdTitulo.Equal(idTitulo),
                                             agendaRendaFixaJuros.Query.DataEvento.LessThanOrEqual(data),
                                             agendaRendaFixaJuros.Query.TipoEvento.NotIn((byte)TipoEventoTitulo.Amortizacao,
                                                                                        (byte)TipoEventoTitulo.AmortizacaoCorrigida,
                                                                                        (byte)TipoEventoTitulo.PagamentoPrincipal,
                                                                                        (byte)TipoEventoTitulo.PagamentoCorrecao));            
            agendaRendaFixaJuros.Query.Load();
            
            DateTime dataBase = new DateTime();
            if (agendaRendaFixaJuros.DataEvento.HasValue)
            {
                dataBase = agendaRendaFixaJuros.DataEvento.Value;
            }
            else
            {
                dataBase = tituloRendaFixa.DataEmissao.Value;
            }

            if (dataBase == data)
            {
                return 0;
            }
                        

            AgendaRendaFixaCollection agendaRendaFixaCollection = new AgendaRendaFixaCollection();
            agendaRendaFixaCollection.Query.Select(agendaRendaFixaCollection.Query.Valor,
                                                   agendaRendaFixaCollection.Query.DataEvento);
            agendaRendaFixaCollection.Query.Where(agendaRendaFixaCollection.Query.IdTitulo.Equal(idTitulo),
                                             agendaRendaFixaCollection.Query.DataEvento.GreaterThan(data),
                                             agendaRendaFixaCollection.Query.TipoEvento.NotIn((byte)TipoEventoTitulo.Amortizacao,
                                                                                              (byte)TipoEventoTitulo.AmortizacaoCorrigida,
                                                                                              (byte)TipoEventoTitulo.PagamentoPrincipal,
                                                                                              (byte)TipoEventoTitulo.PagamentoCorrecao));
            agendaRendaFixaCollection.Query.OrderBy(agendaRendaFixaCollection.Query.DataEvento.Ascending);
            agendaRendaFixaCollection.Query.Load();

            decimal valor = 0;
            DateTime dataProximoEvento = new DateTime();
            if (agendaRendaFixaCollection.Count > 0)
            {
                valor = agendaRendaFixaCollection[0].Valor.Value;
                dataProximoEvento = agendaRendaFixaCollection[0].DataEvento.Value;
            }

            PapelRendaFixa papel = new PapelRendaFixa();
            papel.LoadByPrimaryKey(tituloRendaFixa.IdPapel.Value);

            int numeroDias = 0;
            int numeroDiasTotal = 0;

            if (papel.ContagemDias.Value == (byte)ContagemDiasTitulo.Corridos || papel.ContagemDias.Value == (byte)ContagemDiasTitulo.Dias360)
            {
                numeroDias = Calendario.NumeroDias(dataBase, data);
                numeroDiasTotal = Calendario.NumeroDias(dataBase, dataProximoEvento);
            }
            else
            {
                numeroDias = Calendario.NumeroDias(dataBase, data, (int)LocalFeriadoFixo.Brasil, TipoFeriado.Brasil);
                numeroDiasTotal = Calendario.NumeroDias(dataBase, dataProximoEvento, (int)LocalFeriadoFixo.Brasil, TipoFeriado.Brasil);
            }

            decimal valorJuros = Math.Round(valor * numeroDias / numeroDiasTotal, 2);

            return valorJuros;
        }

        private DateTime RetornaDataProxEvento(DateTime data, int idTitulo, bool eventoJuros)
        {
            AgendaRendaFixaCollection agendaRendaFixaCollection = new AgendaRendaFixaCollection();
            agendaRendaFixaCollection.Query.Select(agendaRendaFixaCollection.Query.DataEvento);
            agendaRendaFixaCollection.Query.Where(agendaRendaFixaCollection.Query.IdTitulo.Equal(idTitulo));

            if (eventoJuros)
            {
                agendaRendaFixaCollection.Query.Where(agendaRendaFixaCollection.Query.DataEvento.GreaterThan(data),
                                                      agendaRendaFixaCollection.Query.TipoEvento.NotIn((byte)TipoEventoTitulo.Amortizacao,
                                                                                                       (byte)TipoEventoTitulo.AmortizacaoCorrigida,
                                                                                                       (byte)TipoEventoTitulo.PagamentoPrincipal,
                                                                                                       (byte)TipoEventoTitulo.PagamentoCorrecao));
            }
            else
            {
                agendaRendaFixaCollection.Query.Where(agendaRendaFixaCollection.Query.DataEvento.GreaterThan(data),
                                                      agendaRendaFixaCollection.Query.TipoEvento.In((byte)TipoEventoTitulo.Amortizacao,
                                                                                                    (byte)TipoEventoTitulo.AmortizacaoCorrigida,
                                                                                                    (byte)TipoEventoTitulo.PagamentoPrincipal));
            }

            agendaRendaFixaCollection.Query.OrderBy(agendaRendaFixaCollection.Query.DataEvento.Ascending);
            agendaRendaFixaCollection.Query.Load();

            DateTime dataProximoEvento = new DateTime();
            if (agendaRendaFixaCollection.Count > 0)
            {
                dataProximoEvento = agendaRendaFixaCollection[0].DataEvento.Value;
            }

            return dataProximoEvento;
        }

        private decimal RetornaQuantidadePosicao(DateTime data, int idTitulo, int idCliente)
        {
            PosicaoRendaFixaHistorico posicaoRendaFixaHistorico = new PosicaoRendaFixaHistorico();
            posicaoRendaFixaHistorico.Query.Select(posicaoRendaFixaHistorico.Query.Quantidade.Sum());
            posicaoRendaFixaHistorico.Query.Where(posicaoRendaFixaHistorico.Query.IdCliente.Equal(idCliente),
                                                  posicaoRendaFixaHistorico.Query.DataHistorico.Equal(data),
                                                  posicaoRendaFixaHistorico.Query.IdTitulo.Equal(idTitulo));
            posicaoRendaFixaHistorico.Query.Load();

            decimal quantidade = 0;
            if (posicaoRendaFixaHistorico.Quantidade.HasValue)
            {
                quantidade = posicaoRendaFixaHistorico.Quantidade.Value;
            }

            return quantidade;
        }

        private decimal CalculaFatorCorrecao(TituloRendaFixa tituloRendaFixa, DateTime data)
        {
            decimal fatorCorrecao = 1;
            Indice indice = new Indice();
            if (tituloRendaFixa.IdIndice.HasValue)
            {
                int idTitulo = tituloRendaFixa.IdTitulo.Value;

                DateTime dataBaseCorrecao = tituloRendaFixa.DataEmissao.Value;

                AgendaRendaFixaCollection agendaRendaFixaCollectionCorrecao = new AgendaRendaFixaCollection();
                agendaRendaFixaCollectionCorrecao.Query.Select(agendaRendaFixaCollectionCorrecao.Query.DataEvento);
                agendaRendaFixaCollectionCorrecao.Query.Where(agendaRendaFixaCollectionCorrecao.Query.IdTitulo.Equal(idTitulo) &
                                                              agendaRendaFixaCollectionCorrecao.Query.DataEvento.LessThan(data) &
                                                                 (
                                                                   agendaRendaFixaCollectionCorrecao.Query.TipoEvento.Equal((byte)TipoEventoTitulo.JurosCorrecao) |
                                                                   (agendaRendaFixaCollectionCorrecao.Query.TipoEvento.Equal((byte)TipoEventoTitulo.PagamentoCorrecao) &
                                                                    agendaRendaFixaCollectionCorrecao.Query.Taxa.In(0, 100)
                                                                   )
                                                                  )
                                                              );
                agendaRendaFixaCollectionCorrecao.Query.OrderBy(agendaRendaFixaCollectionCorrecao.Query.DataEvento.Descending);
                agendaRendaFixaCollectionCorrecao.Query.Load();

                if (agendaRendaFixaCollectionCorrecao.Count > 0)
                {
                    dataBaseCorrecao = agendaRendaFixaCollectionCorrecao[0].DataEvento.Value;
                }

                indice.LoadByPrimaryKey(tituloRendaFixa.IdIndice.Value);

                PapelRendaFixa papelRendaFixa = new PapelRendaFixa();
                papelRendaFixa.LoadByPrimaryKey(tituloRendaFixa.IdPapel.Value);

                CalculoRendaFixa calculoRendaFixa = new CalculoRendaFixa();
                fatorCorrecao = calculoRendaFixa.RetornaFatorCorrecao(data, dataBaseCorrecao, papelRendaFixa, tituloRendaFixa, indice);
            }

            return fatorCorrecao;
        }
    }
}