﻿using System;
using System.IO;
using Financial.Fundo;
using Financial.Investidor;
using Financial.Investidor.Enums;
using Financial.CRM;
using EntitySpaces.Interfaces;
using System.Collections.Generic;
using Financial.Util;
using Financial.InvestidorCotista;
using Financial.RendaFixa;
using Financial.RendaFixa.Enums;
using Financial.Common;
using Financial.Common.Enums;
using System.Xml.Serialization;
using System.Xml;
using Financial.InvestidorCotista.Enums;
using Financial.ContaCorrente;
using Financial.ContaCorrente.Enums;

namespace Financial.Export {

    #region Perfil Mensal XML V1
    public class TabelaPerfilCVM {

        /// <summary>
        /// Retorna uma Stream de Mémoria de um arquivo xml.
        /// </summary>
        /// <param name="dataPosicao"></param>
        /// <param name="idClientes"></param>
        /// <param name="ms">Saida: Memory Stream do arquivo TabelaPerfilCVM</param>
        /// <param name="nomeArquivo">Saida: Nome do Arquivo TabelaPerfilCVM</param>     
        /// <exception cref="ProcessaTabelaPerfilException">
        /// throws ProcessaTabelaPerfilException se ocorreu algum erro no Processamento
        /// </exception>
        public void ExportaTabelaPerfilCVM(DateTime dataPosicao, List<int> idClientes, out MemoryStream ms, out string nomeArquivo) 
        {
            ms = new MemoryStream();
            nomeArquivo = "PerfilMensalCVM.xml";
            //            
            Perfil_1.TabelaPerfilCVM_Xml arq = new Perfil_1.TabelaPerfilCVM_Xml();
            //
            /* Monta Cabeçalho */
            this.MontaCabecalho(dataPosicao, arq);

            /* Monta Corpo */
            this.MontaCorpo(dataPosicao, idClientes, arq);

            XmlSerializer x = new XmlSerializer(arq.GetType());

            XmlWriterSettings settings = new XmlWriterSettings();
            settings.Encoding = System.Text.Encoding.GetEncoding("WINDOWS-1252");
            settings.Indent = true;
            settings.IndentChars = "\t";
            settings.NewLineChars = Environment.NewLine;
            settings.ConformanceLevel = ConformanceLevel.Document;

            XmlSerializerNamespaces ns = new XmlSerializerNamespaces();
            ns.Add("", "");

            using (XmlWriter writer = XmlTextWriter.Create(ms, settings)) {
                //x.Serialize(writer, arq, ns);
                x.Serialize(writer, arq);
            }
        }

        /// <summary>
        /// Monta o Cabeçalho do Arquivo XML
        /// </summary>
        /// <param name="dataPosicao"></param>
        /// <param name="arq">Estrutura do arquivo XML</param>
        /// <returns></returns>
        private void MontaCabecalho(DateTime dataPosicao, Perfil_1.TabelaPerfilCVM_Xml arq) {
            arq.getCabecalho.codigoDocumento = 40;            
            arq.getCabecalho.dataCompetencia = dataPosicao;
            arq.getCabecalho.dataGeracaoArquivo = DateTime.Now;
            arq.getCabecalho.versao = "1.0";
        }

        /// <summary>
        /// Monta o Corpo do Arquivo XML 
        /// </summary>
        /// <param name="dataPosicao"></param>
        /// <param name="idClientes"></param>
        /// <param name="arq">Estrutura do arquivo XML</param>
        private void MontaCorpo(DateTime dataPosicao, List<int> idClientes, Perfil_1.TabelaPerfilCVM_Xml arq) {
            // -------------------------------------------------
            List<Perfil_1.ListPerfil> pLista = new List<Perfil_1.ListPerfil>();
            //
            
            ClienteCollection clienteCollection = new ClienteCollection();
            clienteCollection.Query.Where(clienteCollection.Query.IdTipo == (int)TipoClienteFixo.Fundo &&
                                          clienteCollection.Query.StatusAtivo == (byte)StatusAtivoCliente.Ativo &&
                                          clienteCollection.Query.TipoControle.In( (byte)TipoControleCliente.CarteiraImportada, 
                                                                                   (byte)TipoControleCliente.Completo ) &&
                                          clienteCollection.Query.IdCliente.In( idClientes )
                                         );

            clienteCollection.Query.Load();
            
            foreach (Cliente cliente in clienteCollection) {
                int idCliente = cliente.IdCliente.Value;

                Pessoa pessoa = new Pessoa();
                List<esQueryItem> campos = new List<esQueryItem>();
                campos.Add(pessoa.Query.Cpfcnpj);
                pessoa.LoadByPrimaryKey(campos, idCliente);

                string cnpj = Utilitario.RemoveCaracteresEspeciais(pessoa.Cpfcnpj.ToString()).Trim();

                HistoricoCota historicoCota = new HistoricoCota();
                historicoCota.BuscaValorPatrimonioDia(idCliente, dataPosicao);

                decimal valorPL = historicoCota.PLFechamento.HasValue ? historicoCota.PLFechamento.Value : 0;

                if (valorPL == 0) {
                    throw new Exception("PL zerado ou inexistente para o fundo " + idCliente.ToString());
                }

                #region Cria Objeto de perfil
                Perfil_1.ListPerfil p1 = new Perfil_1.ListPerfil();
                //
                p1.cnpj = cnpj;

                #region Trata Tabela de tipos de cotistas

                //CotistaQuery cotistaQuery = new CotistaQuery("C");
                //PosicaoCotistaHistoricoQuery posicaoCotistaHistoricoQuery = new PosicaoCotistaHistoricoQuery("P");
                //cotistaQuery.Select(cotistaQuery.TipoCotistaCVM, cotistaQuery.TipoCotistaCVM.Count().As("ContTipo"));
                //cotistaQuery.InnerJoin(posicaoCotistaHistoricoQuery).On(posicaoCotistaHistoricoQuery.IdCotista == cotistaQuery.IdCotista);
                //cotistaQuery.Where(posicaoCotistaHistoricoQuery.IdCarteira.Equal(idCliente),
                //                   posicaoCotistaHistoricoQuery.Quantidade.NotEqual(0),
                //                   posicaoCotistaHistoricoQuery.DataHistorico.Equal(dataPosicao),
                //                   cotistaQuery.TipoCotistaCVM.IsNotNull());
                //cotistaQuery.GroupBy(cotistaQuery.TipoCotistaCVM);
                //CotistaCollection cotistaCollection = new CotistaCollection();
                //cotistaCollection.Load(cotistaQuery);

                CotistaQuery cotistaQuery = new CotistaQuery("C");
                PosicaoCotistaHistoricoQuery posicaoCotistaHistoricoQuery = new PosicaoCotistaHistoricoQuery("P");
                //
                cotistaQuery.es.Distinct = true;
                
                cotistaQuery.Select(cotistaQuery.IdCotista,
                                    cotistaQuery.TipoCotistaCVM.As("TipoCotistaCVM"));
                cotistaQuery.InnerJoin(posicaoCotistaHistoricoQuery).On(posicaoCotistaHistoricoQuery.IdCotista == cotistaQuery.IdCotista);
                cotistaQuery.Where(posicaoCotistaHistoricoQuery.IdCarteira.Equal(idCliente),
                                   posicaoCotistaHistoricoQuery.Quantidade.NotEqual(0),
                                   posicaoCotistaHistoricoQuery.DataHistorico.Equal(dataPosicao),
                                   cotistaQuery.TipoCotistaCVM.IsNotNull());


                // Query Principal
                CotistaQuery cotistaQueryPrincipal = new CotistaQuery("C");
                //
                cotistaQueryPrincipal.Select("<C.TipoCotistaCVM>", "<COUNT(C.TipoCotistaCVM) AS ContTipo>");
                //
                cotistaQueryPrincipal.From(cotistaQuery).As("C");
                cotistaQueryPrincipal.GroupBy("TipoCotistaCVM");
                //    
                CotistaCollection cotistaCollection = new CotistaCollection();
                cotistaCollection.Load(cotistaQueryPrincipal);
                
                #endregion

                #region NR_CLIENT
                Perfil_1.NR_CLIENT nr = new Perfil_1.NR_CLIENT();
                nr.NR_PF_PRIV_BANK = 0;
                nr.NR_PF_VARJ = 0;
                nr.NR_PJ_N_FINANC_PRIV_BANK = 0;
                nr.NR_PJ_N_FINANC_VARJ = 0;
                nr.NR_BNC_COMERC = 0;
                nr.NR_PJ_CORR_DIST = 0;
                nr.NR_PJ_OUTR_FINANC = 0;
                nr.NR_INV_N_RES = 0;
                nr.NR_ENT_AB_PREV_COMPL = 0;
                nr.NR_ENT_FC_PREV_COMPL = 0;
                nr.NR_REG_PREV_SERV_PUB = 0;
                nr.NR_SOC_SEG_RESEG = 0;
                nr.NR_SOC_CAPTLZ_ARRENDM_MERC = 0;
                nr.NR_FDOS_CLUB_INV = 0;
                nr.NR_COTST_DISTR_FDO = 0;
                nr.NR_OUTROS_N_RELAC = 0;
                //

                for (int i = 0; i < cotistaCollection.Count; i++) {
                    byte tipoCotistaCVM = Convert.ToByte(cotistaCollection[i].TipoCotistaCVM.Value);
                    int cont = Convert.ToInt32(cotistaCollection[i].GetColumn("ContTipo"));

                    if (tipoCotistaCVM == (byte)TipoCotista.PESSOA_FISICA_PRIVATE_BANKING) {
                        nr.NR_PF_PRIV_BANK = cont;
                    }
                    else if (tipoCotistaCVM == (byte)TipoCotista.PESSOA_FISICA_VAREJO) {
                        nr.NR_PF_VARJ = cont;
                    }
                    else if (tipoCotistaCVM == (byte)TipoCotista.PESSOA_JURIDICA_NAO_FINANCEIRA_PRIVATE_BANKING) {
                        nr.NR_PJ_N_FINANC_PRIV_BANK = cont;
                    }
                    else if (tipoCotistaCVM == (byte)TipoCotista.PESSOA_JURIDICA_NAO_FINANCEIRA_VAREJO) {
                        nr.NR_PJ_N_FINANC_VARJ = cont;
                    }
                    else if (tipoCotistaCVM == (byte)TipoCotista.BANCO_COMERCIAL) {
                        nr.NR_BNC_COMERC = cont;
                    }
                    else if (tipoCotistaCVM == (byte)TipoCotista.CORRETORA_DISTRIBUIDORA) {
                        nr.NR_COTST_DISTR_FDO = cont;
                    }
                    else if (tipoCotistaCVM == (byte)TipoCotista.OUTRAS_PESSOAS_JURIDICAS_FINANCEIRAS) {
                        nr.NR_PJ_OUTR_FINANC = cont;
                    }
                    else if (tipoCotistaCVM == (byte)TipoCotista.INVESTIDORES_NAO_RESIDENTES) {
                        nr.NR_INV_N_RES = cont;
                    }
                    else if (tipoCotistaCVM == (byte)TipoCotista.ENTIDADE_ABERTA_PREVIDENCIA_COMPLEMENTAR) {
                        nr.NR_ENT_AB_PREV_COMPL = cont;
                    }
                    else if (tipoCotistaCVM == (byte)TipoCotista.ENTIDADE_FECHADA_PREVIDENCIA_COMPLEMENTAR) {
                        nr.NR_ENT_FC_PREV_COMPL = cont;
                    }
                    else if (tipoCotistaCVM == (byte)TipoCotista.REGIME_PROPRIO_PREVIDENCIA_SERVIDORES_PUBLICOS) {
                        nr.NR_REG_PREV_SERV_PUB = cont;
                    }
                    else if (tipoCotistaCVM == (byte)TipoCotista.SOCIEDADE_SEGURADORA_RESSEGURADORA) {
                        nr.NR_SOC_SEG_RESEG = cont;
                    }
                    else if (tipoCotistaCVM == (byte)TipoCotista.SOCIEDADE_CAPITALIZACAO_ARRENDAMENTO_MERCANTIL) {
                        nr.NR_SOC_CAPTLZ_ARRENDM_MERC = cont;
                    }
                    else if (tipoCotistaCVM == (byte)TipoCotista.FUNDOS_CLUBES_INVESTIMENTOS) {
                        nr.NR_FDOS_CLUB_INV = cont;
                    }
                    else if (tipoCotistaCVM == (byte)TipoCotista.COTISTAS_DISTRIBUIDORES_FUNDO_CONTA_ORDEM) {
                        nr.NR_COTST_DISTR_FDO = cont;
                    }
                    else if (tipoCotistaCVM == (byte)TipoCotista.OUTROS_TIPOS_COTISTAS) {
                        nr.NR_OUTROS_N_RELAC = cont;
                    }
                }                

                p1.get_NR_CLIENT = nr;
                #endregion

                #region Trata tabela de tipos de cotistas por patrimônio detido
                cotistaQuery = new CotistaQuery("C");
                posicaoCotistaHistoricoQuery = new PosicaoCotistaHistoricoQuery("P");
                cotistaQuery.Select(cotistaQuery.TipoCotistaCVM, posicaoCotistaHistoricoQuery.ValorBruto.Sum().As("SaldoTipo"));
                cotistaQuery.InnerJoin(posicaoCotistaHistoricoQuery).On(posicaoCotistaHistoricoQuery.IdCotista == cotistaQuery.IdCotista);
                cotistaQuery.Where(posicaoCotistaHistoricoQuery.IdCarteira.Equal(idCliente),
                                   posicaoCotistaHistoricoQuery.Quantidade.NotEqual(0),
                                   posicaoCotistaHistoricoQuery.DataHistorico.Equal(dataPosicao),
                                   cotistaQuery.TipoCotistaCVM.IsNotNull());
                cotistaQuery.GroupBy(cotistaQuery.TipoCotistaCVM);
                cotistaCollection = new CotistaCollection();
                cotistaCollection.Load(cotistaQuery);
                #endregion

                #region DISTR_PATRIM

                Perfil_1.DISTR_PATRIM dp = new Perfil_1.DISTR_PATRIM();
                //
                dp.PR_PF_PRIV_BANK = 0;
                dp.PR_PF_VARJ = 0;
                dp.PR_PJ_N_FINANC_PRIV_BANK = 0;
                dp.PR_PJ_N_FINANC_VARJ = 0;
                dp.PR_BNC_COMERC = 0;
                dp.PR_PJ_CORR_DIST = 0;
                dp.PR_PJ_OUTR_FINANC = 0;
                dp.PR_INV_N_RES = 0;
                dp.PR_ENT_AB_PREV_COMPL = 0;
                dp.PR_ENT_FC_PREV_COMPL = 0;
                dp.PR_REG_PREV_SERV_PUB = 0;
                dp.PR_SOC_SEG_RESEG = 0;
                dp.PR_SOC_CAPTLZ_ARRENDM_MERC = 0;
                dp.PR_FDOS_CLUB_INV = 0;
                dp.PR_COTST_DISTR_FDO = 0;
                dp.PR_OUTROS_N_RELAC = 0;

                for (int i = 0; i < cotistaCollection.Count; i++) {
                    byte tipoCotistaCVM = Convert.ToByte(cotistaCollection[i].TipoCotistaCVM.Value);
                    decimal percentSaldo = Math.Round(Convert.ToDecimal(cotistaCollection[i].GetColumn("SaldoTipo")) / valorPL * 100M, 1);

                    if (tipoCotistaCVM == (byte)TipoCotista.PESSOA_FISICA_PRIVATE_BANKING) {
                        dp.PR_PF_PRIV_BANK = percentSaldo;
                    }
                    else if (tipoCotistaCVM == (byte)TipoCotista.PESSOA_FISICA_VAREJO) {
                        dp.PR_PF_VARJ = percentSaldo;
                    }
                    else if (tipoCotistaCVM == (byte)TipoCotista.PESSOA_JURIDICA_NAO_FINANCEIRA_PRIVATE_BANKING) {
                        dp.PR_PJ_N_FINANC_PRIV_BANK = percentSaldo;
                    }
                    else if (tipoCotistaCVM == (byte)TipoCotista.PESSOA_JURIDICA_NAO_FINANCEIRA_VAREJO) {
                        dp.PR_PJ_N_FINANC_VARJ = percentSaldo;
                    }
                    else if (tipoCotistaCVM == (byte)TipoCotista.BANCO_COMERCIAL) {
                        dp.PR_BNC_COMERC = percentSaldo;
                    }
                    else if (tipoCotistaCVM == (byte)TipoCotista.CORRETORA_DISTRIBUIDORA) {
                        dp.PR_COTST_DISTR_FDO = percentSaldo;
                    }
                    else if (tipoCotistaCVM == (byte)TipoCotista.OUTRAS_PESSOAS_JURIDICAS_FINANCEIRAS) {
                        dp.PR_PJ_OUTR_FINANC = percentSaldo;
                    }
                    else if (tipoCotistaCVM == (byte)TipoCotista.INVESTIDORES_NAO_RESIDENTES) {
                        dp.PR_INV_N_RES = percentSaldo;
                    }
                    else if (tipoCotistaCVM == (byte)TipoCotista.ENTIDADE_ABERTA_PREVIDENCIA_COMPLEMENTAR) {
                        dp.PR_ENT_AB_PREV_COMPL = percentSaldo;
                    }
                    else if (tipoCotistaCVM == (byte)TipoCotista.ENTIDADE_FECHADA_PREVIDENCIA_COMPLEMENTAR) {
                        dp.PR_ENT_FC_PREV_COMPL = percentSaldo;
                    }
                    else if (tipoCotistaCVM == (byte)TipoCotista.REGIME_PROPRIO_PREVIDENCIA_SERVIDORES_PUBLICOS) {
                        dp.PR_REG_PREV_SERV_PUB = percentSaldo;
                    }
                    else if (tipoCotistaCVM == (byte)TipoCotista.SOCIEDADE_SEGURADORA_RESSEGURADORA) {
                        dp.PR_SOC_SEG_RESEG = percentSaldo;
                    }
                    else if (tipoCotistaCVM == (byte)TipoCotista.SOCIEDADE_CAPITALIZACAO_ARRENDAMENTO_MERCANTIL) {
                        dp.PR_SOC_CAPTLZ_ARRENDM_MERC = percentSaldo;
                    }
                    else if (tipoCotistaCVM == (byte)TipoCotista.FUNDOS_CLUBES_INVESTIMENTOS) {
                        dp.PR_FDOS_CLUB_INV = percentSaldo;
                    }
                    else if (tipoCotistaCVM == (byte)TipoCotista.COTISTAS_DISTRIBUIDORES_FUNDO_CONTA_ORDEM) {
                        dp.PR_COTST_DISTR_FDO = percentSaldo;
                    }
                    else if (tipoCotistaCVM == (byte)TipoCotista.OUTROS_TIPOS_COTISTAS) {
                        dp.PR_OUTROS_N_RELAC = percentSaldo;
                    }
                }
                //
                p1.get_DISTR_PATRIM = dp;
                #endregion

                TabelaPerfilMensalCVMCollection tabelaPerfilMensalCVMCollection = new TabelaPerfilMensalCVMCollection();                
                #region Trata tabela informada pelo usuário para a CVM
                DateTime primeiroDiaMes = Calendario.RetornaPrimeiroDiaCorridoMes(dataPosicao, 0);
                DateTime ultimoDiaMes = Calendario.RetornaUltimoDiaCorridoMes(dataPosicao, 0);

                tabelaPerfilMensalCVMCollection.Query.Where(tabelaPerfilMensalCVMCollection.Query.IdCarteira.Equal(idCliente),
                                                            tabelaPerfilMensalCVMCollection.Query.Data.GreaterThanOrEqual(primeiroDiaMes),
                                                            tabelaPerfilMensalCVMCollection.Query.Data.LessThanOrEqual(ultimoDiaMes));
                tabelaPerfilMensalCVMCollection.Query.Load();

                if (tabelaPerfilMensalCVMCollection.Count == 0)
                {
                    throw new Exception("Tabela Perfil Mensal não definida para o fundo " + idCliente.ToString());                    
                }                
                #endregion

                TabelaPerfilMensalCVM tabelaPerfilMensalCVM = tabelaPerfilMensalCVMCollection[0];

                if (!String.IsNullOrEmpty(tabelaPerfilMensalCVM.Secao3)) {
                    p1.resm_teor_vt_profrd = tabelaPerfilMensalCVM.Secao3;
                }
                if (!String.IsNullOrEmpty(tabelaPerfilMensalCVM.Secao4)) {
                    p1.just_sum_vt_profrd = tabelaPerfilMensalCVM.Secao4;
                }
                if (tabelaPerfilMensalCVM.Secao5.HasValue && tabelaPerfilMensalCVM.Secao5.Value != 0) {
                    p1.var_perc_pl = tabelaPerfilMensalCVM.Secao5.Value;
                }
                if (tabelaPerfilMensalCVM.Secao6.HasValue && tabelaPerfilMensalCVM.Secao6.Value != 0) {
                    p1.mod_var_utiliz = tabelaPerfilMensalCVM.Secao6.Value.ToString();
                }
                
                if (tabelaPerfilMensalCVM.Secao7.HasValue && tabelaPerfilMensalCVM.Secao7.Value != 0) {
                    p1.praz_med_cart_tit = Math.Abs(tabelaPerfilMensalCVM.Secao7.Value);
                }
                else {
                    Carteira carteira = new Carteira();
                    decimal prazoMedio = carteira.RetornaPrazoMedio(idCliente, dataPosicao) / 30M; //em meses (30 dias)

                    p1.praz_med_cart_tit = Math.Abs(Math.Round(prazoMedio, 4));
                }

                if (!String.IsNullOrEmpty(tabelaPerfilMensalCVM.Secao8)) {
                    p1.res_delib = tabelaPerfilMensalCVM.Secao8;
                }
                
                p1.total_recurs_exter = 0; //POR ORA NÃO SERÁ CONTROLADO FIEX OU ALGO PARECIDO
                p1.total_recurs_br = 0; //POR ORA NÃO SERÁ CONTROLADO FIEX OU ALGO PARECIDO

                #region VARIACAO_PERC_VAL_COTA
                Perfil_1.VARIACAO_PERC_VAL_COTA v = new Perfil_1.VARIACAO_PERC_VAL_COTA();
                if (tabelaPerfilMensalCVM.Secao11PercentCota.HasValue){
                    v.VAL_PERCENT = tabelaPerfilMensalCVM.Secao11PercentCota.Value;
                }
                //                        
                #region Add lista de FATOR_PRIMIT_RISCO em VARIACAO_PERC_VAL_COTA
                List<Perfil_1.FATOR_PRIMIT_RISCO> lp = new List<Perfil_1.FATOR_PRIMIT_RISCO>();
                //                            
                if (!String.IsNullOrEmpty(tabelaPerfilMensalCVM.Secao11Cenario1)){
                    Perfil_1.FATOR_PRIMIT_RISCO fp = new Perfil_1.FATOR_PRIMIT_RISCO();
                    //
                    fp.NOME_FATOR_PRIMIT_RISCO = tabelaPerfilMensalCVM.Secao11Cenario1;
                    fp.CEN_UTIL = !String.IsNullOrEmpty(tabelaPerfilMensalCVM.Secao11Fator1) ? tabelaPerfilMensalCVM.Secao11Fator1 : " "; // codigo Alt 255
                    //
                    lp.Add(fp);
                }
                if (!String.IsNullOrEmpty(tabelaPerfilMensalCVM.Secao11Cenario2)){
                    Perfil_1.FATOR_PRIMIT_RISCO fp1 = new Perfil_1.FATOR_PRIMIT_RISCO();
                    //                    
                    fp1.NOME_FATOR_PRIMIT_RISCO = tabelaPerfilMensalCVM.Secao11Cenario2;
                    fp1.CEN_UTIL = !String.IsNullOrEmpty(tabelaPerfilMensalCVM.Secao11Fator2) ? tabelaPerfilMensalCVM.Secao11Fator2 : " "; // codigo Alt 255;
                    //
                    lp.Add(fp1);
                }
                if (!String.IsNullOrEmpty(tabelaPerfilMensalCVM.Secao11Cenario3)){
                    Perfil_1.FATOR_PRIMIT_RISCO fp2 = new Perfil_1.FATOR_PRIMIT_RISCO();
                    //
                    fp2.NOME_FATOR_PRIMIT_RISCO = tabelaPerfilMensalCVM.Secao11Cenario3;
                    fp2.CEN_UTIL = !String.IsNullOrEmpty(tabelaPerfilMensalCVM.Secao11Fator3) ? tabelaPerfilMensalCVM.Secao11Fator3 : " "; // codigo Alt 255;
                    //
                    lp.Add(fp2);
                }
                if (!String.IsNullOrEmpty(tabelaPerfilMensalCVM.Secao11Cenario4)){
                    Perfil_1.FATOR_PRIMIT_RISCO fp3 = new Perfil_1.FATOR_PRIMIT_RISCO();
                    //
                    fp3.NOME_FATOR_PRIMIT_RISCO = tabelaPerfilMensalCVM.Secao11Cenario4;
                    fp3.CEN_UTIL = !String.IsNullOrEmpty(tabelaPerfilMensalCVM.Secao11Fator4) ? tabelaPerfilMensalCVM.Secao11Fator4 : " "; // codigo Alt 255;
                    //
                    lp.Add(fp3);
                }
                if (!String.IsNullOrEmpty(tabelaPerfilMensalCVM.Secao11Cenario5)){
                    Perfil_1.FATOR_PRIMIT_RISCO fp4 = new Perfil_1.FATOR_PRIMIT_RISCO();
                    //
                    fp4.NOME_FATOR_PRIMIT_RISCO = tabelaPerfilMensalCVM.Secao11Cenario5;
                    fp4.CEN_UTIL = !String.IsNullOrEmpty(tabelaPerfilMensalCVM.Secao11Fator5) ? tabelaPerfilMensalCVM.Secao11Fator5 : " "; // codigo Alt 255;
                    //
                    lp.Add(fp4);
                }
                //
                
                //
                if (lp.Count > 0) {
                    v.lista_fator_primit_risco.get_FATOR_PRIMIT_RISCO = lp;
                }
                #endregion
                //
                p1.get_VARIACAO_PERC_VAL_COTA = v;
                #endregion

                if (tabelaPerfilMensalCVM.Secao12.HasValue && tabelaPerfilMensalCVM.Secao12.Value != 0) {
                    p1.var_diar_perc_cota_fdo_pior_cen_estress = tabelaPerfilMensalCVM.Secao12.Value;
                }
                if (tabelaPerfilMensalCVM.Secao13.HasValue && tabelaPerfilMensalCVM.Secao13.Value != 0) {
                    p1.var_diar_perc_patrim_fdo_var_n_taxa_anual = tabelaPerfilMensalCVM.Secao13.Value;
                }
                if (tabelaPerfilMensalCVM.Secao14.HasValue && tabelaPerfilMensalCVM.Secao14.Value != 0) {
                    p1.var_diar_perc_patrim_fdo_var_n_taxa_cambio = tabelaPerfilMensalCVM.Secao14.Value;
                }
                if (tabelaPerfilMensalCVM.Secao15.HasValue && tabelaPerfilMensalCVM.Secao15.Value != 0) {
                    p1.var_patrim_fdo_n_preco_acoes = tabelaPerfilMensalCVM.Secao15.Value;
                }

                #region VARIACAO_DIAR_PERC_PATRIM_FDO_VAR_N_OUTROS
                Perfil_1.VARIACAO_DIAR_PERC_PATRIM_FDO_VAR_N_OUTROS vd = new Perfil_1.VARIACAO_DIAR_PERC_PATRIM_FDO_VAR_N_OUTROS();
                //
                string fatorRisco = !String.IsNullOrEmpty(tabelaPerfilMensalCVM.Secao16Fator) 
                                    ? tabelaPerfilMensalCVM.Secao16Fator.Trim() : "-";

                if (!String.IsNullOrEmpty(fatorRisco)) { // Sempre entra
                    vd.FATOR_RISCO_OUTROS = fatorRisco;
                    //
                    decimal variacao = 0;
                    if (tabelaPerfilMensalCVM.Secao16Variacao.HasValue) {
                        variacao = tabelaPerfilMensalCVM.Secao16Variacao.Value;
                    }
                    vd.VAL_PERCENT_OUTROS = variacao;
                    //
                    p1.get_VARIACAO_DIAR_PERC_PATRIM_FDO_VAR_N_OUTROS = vd;
                }
                #endregion

                #region VALOR_NOC_TOT_CONTRAT_DERIV_MANT_FDO
                //POR ORA SEM CONTROLE DE FUNDOS COM POSIÇÃO EM FUTUROS DE BALCÃO

                //VALOR_NOC_TOT_CONTRAT_DERIV_MANT_FDO vc = new VALOR_NOC_TOT_CONTRAT_DERIV_MANT_FDO();
                //vc.VAL_COLATERAL = "1";
                ////                        
                //#region Add lista de FATOR_RISCO_NOC em VALOR_NOC_TOT_CONTRAT_DERIV_MANT_FDO
                //List<FATOR_RISCO_NOC> lr = new List<FATOR_RISCO_NOC>();
                ////            
                //FATOR_RISCO_NOC fr = new FATOR_RISCO_NOC();
                //fr.NOME_FATOR_NOC = "1";
                //fr.VAL_FATOR_RISCO_NOC_LONG = "1";
                //fr.VAL_FATOR_RISCO_NOC_SHORT = "1";
                ////
                //lr.Add(fr);
                ////
                //if (lr.Count > 0) {
                //    vc.lista_fator_risco_noc = new LISTA_FATOR_RISCO_NOC();
                //    vc.lista_fator_risco_noc.get_FATOR_RISCO_NOC = lr;
                //}
                //#endregion
                ////
                //p1.get_VALOR_NOC_TOT_CONTRAT_DERIV_MANT_FDO = vc;
                #endregion

                #region LISTA_OPER_CURS_MERC_BALCAO
                //POR ORA SEM CONTROLE DE FUNDOS COM POSIÇÃO EM MERCADO DE BALCÃO SEM GARANTIA DE CONTRAPARTE

                //LISTA_OPER_CURS_MERC_BALCAO lm = new LISTA_OPER_CURS_MERC_BALCAO();
                ////
                //#region Add lista de OPER_CURS_MERC_BALCAO em LISTA_OPER_CURS_MERC_BALCAO
                //List<OPER_CURS_MERC_BALCAO> lb = new List<OPER_CURS_MERC_BALCAO>();
                ////            
                //OPER_CURS_MERC_BALCAO ob = new OPER_CURS_MERC_BALCAO();
                //ob.TP_PESSOA = "1";
                //ob.NR_PF_PJ_COMITENTE = "1";
                //ob.PARTE_RELACIONADA = "1";
                //ob.VALOR_PARTE = "1";
                ////
                //lb.Add(ob);
                ////
                //lm.get_OPER_CURS_MERC_BALCAO = lb;
                //#endregion
                ////
                //p1.get_LISTA_OPER_CURS_MERC_BALCAO = lm;
                #endregion

                string tipoPessoa1 = "";
                string tipoPessoa2 = "";
                string tipoPessoa3 = "";
                string cpfcnpj1 = "";
                string cpfcnpj2 = "";
                string cpfcnpj3 = "";
                string indicaParte1 = "";
                string indicaParte2 = "";
                string indicaParte3 = "";
                decimal percentEmissor1 = 0;
                decimal percentEmissor2 = 0;
                decimal percentEmissor3 = 0;
                decimal valorPartesRelacionadas = 0;
                #region Calcula lista com até 3 emissores e total em partes relacionadas
                Carteira carteiraInvestidor = new Carteira();
                campos = new List<esQueryItem>();
                campos.Add(carteiraInvestidor.Query.IdAgenteAdministrador);
                campos.Add(carteiraInvestidor.Query.IdAgenteGestor);
                carteiraInvestidor.LoadByPrimaryKey(campos, idCliente);


                PosicaoFundoHistoricoCollection posicaoFundoHistoricoCollection = new PosicaoFundoHistoricoCollection();
                posicaoFundoHistoricoCollection.Query.Select(posicaoFundoHistoricoCollection.Query.IdCarteira,
                                                             posicaoFundoHistoricoCollection.Query.ValorBruto.Sum());
                posicaoFundoHistoricoCollection.Query.Where(posicaoFundoHistoricoCollection.Query.IdCliente.Equal(idCliente),
                                                            posicaoFundoHistoricoCollection.Query.DataHistorico.Equal(dataPosicao),
                                                            posicaoFundoHistoricoCollection.Query.Quantidade.NotEqual(0));
                posicaoFundoHistoricoCollection.Query.GroupBy(posicaoFundoHistoricoCollection.Query.IdCarteira);
                posicaoFundoHistoricoCollection.Query.Load();

                foreach (PosicaoFundoHistorico posicaoFundoHistorico in posicaoFundoHistoricoCollection)
                {
                    int idCarteira = posicaoFundoHistorico.IdCarteira.Value;
                    decimal valor = posicaoFundoHistorico.ValorBruto.Value;

                    Carteira carteiraAplicada = new Carteira();
                    campos = new List<esQueryItem>();
                    campos.Add(carteiraAplicada.Query.IdAgenteAdministrador);
                    campos.Add(carteiraAplicada.Query.IdAgenteGestor);
                    carteiraAplicada.LoadByPrimaryKey(campos, idCarteira);

                    if (carteiraInvestidor.IdAgenteAdministrador.Value == carteiraAplicada.IdAgenteAdministrador.Value ||
                        carteiraInvestidor.IdAgenteGestor.Value == carteiraAplicada.IdAgenteGestor.Value)
                    {
                        valorPartesRelacionadas += valor;
                    }
                }

                decimal valorEmissor1 = 0;
                decimal valorEmissor2 = 0;
                decimal valorEmissor3 = 0;
                PosicaoRendaFixaHistoricoCollection posicaoRendaFixaHistoricoCollection = new PosicaoRendaFixaHistoricoCollection();
                posicaoRendaFixaHistoricoCollection.Query.Select(posicaoRendaFixaHistoricoCollection.Query.IdTitulo,
                                                                 posicaoRendaFixaHistoricoCollection.Query.ValorMercado.Sum());
                posicaoRendaFixaHistoricoCollection.Query.Where(posicaoRendaFixaHistoricoCollection.Query.IdCliente.Equal(idCliente),
                                                            posicaoRendaFixaHistoricoCollection.Query.DataHistorico.Equal(dataPosicao),
                                                            posicaoRendaFixaHistoricoCollection.Query.Quantidade.NotEqual(0));
                posicaoRendaFixaHistoricoCollection.Query.GroupBy(posicaoRendaFixaHistoricoCollection.Query.IdTitulo);
                posicaoRendaFixaHistoricoCollection.Query.Load();

                foreach (PosicaoRendaFixaHistorico posicaoRendaFixaHistorico in posicaoRendaFixaHistoricoCollection)
                {
                    int idTitulo = posicaoRendaFixaHistorico.IdTitulo.Value;
                    decimal valor = posicaoRendaFixaHistorico.ValorMercado.Value;

                    TituloRendaFixa tituloRendaFixa = new TituloRendaFixa();
                    campos = new List<esQueryItem>();
                    campos.Add(tituloRendaFixa.Query.IdPapel);
                    campos.Add(tituloRendaFixa.Query.IdEmissor);
                    tituloRendaFixa.LoadByPrimaryKey(campos, idTitulo);

                    PapelRendaFixa papelRendaFixa = new PapelRendaFixa();
                    campos = new List<esQueryItem>();
                    campos.Add(papelRendaFixa.Query.TipoPapel);
                    papelRendaFixa.LoadByPrimaryKey(campos, tituloRendaFixa.IdPapel.Value);

                    Emissor emissor = new Emissor();
                    campos = new List<esQueryItem>();
                    campos.Add(emissor.Query.IdAgente);
                    campos.Add(emissor.Query.TipoEmissor);
                    campos.Add(emissor.Query.Cnpj);
                    emissor.LoadByPrimaryKey(campos, tituloRendaFixa.IdEmissor.Value);

                    string parteRelacionada = "N";
                    if (emissor.IdAgente.HasValue && (carteiraInvestidor.IdAgenteAdministrador.Value == emissor.IdAgente.Value ||
                        carteiraInvestidor.IdAgenteGestor.Value == emissor.IdAgente.Value))
                    {
                        parteRelacionada = "S";
                        valorPartesRelacionadas += valor;
                    }

                    if (papelRendaFixa.TipoPapel == (byte)TipoPapelTitulo.Privado)
                    {
                        if (valor >= valorEmissor1)
                        {
                            valorEmissor3 = valorEmissor2;
                            valorEmissor2 = valorEmissor1;
                            valorEmissor1 = valor;

                            tipoPessoa3 = tipoPessoa2;
                            tipoPessoa2 = tipoPessoa1;
                            tipoPessoa1 = emissor.TipoEmissor.Value == (byte)TipoEmissor.PessoaFisica ? "PF" : "PJ";

                            cpfcnpj3 = cpfcnpj2;
                            cpfcnpj2 = cpfcnpj1;
                            cpfcnpj1 = emissor.Cnpj;

                            indicaParte3 = indicaParte2;
                            indicaParte2 = indicaParte1;
                            indicaParte1 = parteRelacionada;

                            percentEmissor3 = percentEmissor2;
                            percentEmissor2 = percentEmissor1;
                            percentEmissor1 = historicoCota.PLFechamento.HasValue ? valor / historicoCota.PLFechamento.Value * 100M : 0;
                        }
                        else if (valor >= valorEmissor2)
                        {
                            valorEmissor3 = valorEmissor2;
                            valorEmissor2 = valor;

                            tipoPessoa3 = tipoPessoa2;
                            tipoPessoa2 = emissor.TipoEmissor.Value == (byte)TipoEmissor.PessoaFisica ? "PF" : "PJ";

                            cpfcnpj3 = cpfcnpj2;
                            cpfcnpj2 = emissor.Cnpj;

                            indicaParte3 = indicaParte2;
                            indicaParte2 = parteRelacionada;

                            percentEmissor3 = percentEmissor2;
                            percentEmissor2 = historicoCota.PLFechamento.HasValue ? valor / historicoCota.PLFechamento.Value * 100M : 0;                            
                        }
                        else if (valor >= valorEmissor3)
                        {
                            valorEmissor3 = valor;

                            tipoPessoa3 = emissor.TipoEmissor.Value == (byte)TipoEmissor.PessoaFisica ? "PF" : "PJ";

                            cpfcnpj3 = emissor.Cnpj;

                            indicaParte3 = parteRelacionada;

                            percentEmissor3 = historicoCota.PLFechamento.HasValue ? valor / historicoCota.PLFechamento.Value * 100M : 0;
                        }
                    }


                }
                #endregion
                decimal percentPartesRelacionadas = valorPartesRelacionadas / valorPL * 100M;

                p1.tot_ativos_p_relac = Math.Round(percentPartesRelacionadas, 1);

                #region LISTA_EMISSORES_TIT_CRED_PRIV
                Perfil_1.LISTA_EMISSORES_TIT_CRED_PRIV le = new Perfil_1.LISTA_EMISSORES_TIT_CRED_PRIV();
                //
                #region Add lista de EMISSORES_TIT_CRED_PRIV em LISTA_EMISSORES_TIT_CRED_PRIV
                List<Perfil_1.EMISSORES_TIT_CRED_PRIV> letcp = new List<Perfil_1.EMISSORES_TIT_CRED_PRIV>();
                //            
                Perfil_1.EMISSORES_TIT_CRED_PRIV lep = new Perfil_1.EMISSORES_TIT_CRED_PRIV();
                if (tipoPessoa1 != "") {

                    string cpfcnpj1Aux = String.IsNullOrEmpty(cpfcnpj1) ? " " : cpfcnpj1;

                    lep.TP_PESSOA_EMISSOR = tipoPessoa1;
                    lep.NR_PF_PJ_EMISSOR = cpfcnpj1Aux;
                    lep.PARTE_RELACIONADA = indicaParte1;
                    lep.VALOR_PARTE = Math.Round(percentEmissor1, 1);

                    letcp.Add(lep);
                }
                if (tipoPessoa2 != "") {

                    string cpfcnpj2Aux = String.IsNullOrEmpty(cpfcnpj2) ? " " : cpfcnpj2;

                    lep.TP_PESSOA_EMISSOR = tipoPessoa2;
                    lep.NR_PF_PJ_EMISSOR = cpfcnpj2Aux;
                    lep.PARTE_RELACIONADA = indicaParte2;
                    lep.VALOR_PARTE = Math.Round(percentEmissor2, 1);

                    letcp.Add(lep);
                }
                if (tipoPessoa3 != "") {

                    string cpfcnpj3Aux = String.IsNullOrEmpty(cpfcnpj3) ? " " : cpfcnpj3;

                    lep.TP_PESSOA_EMISSOR = tipoPessoa3;
                    lep.NR_PF_PJ_EMISSOR = cpfcnpj3Aux;
                    lep.PARTE_RELACIONADA = indicaParte3;
                    lep.VALOR_PARTE = Math.Round(percentEmissor3, 1);

                    letcp.Add(lep);
                }
                //                
                le.get_EMISSORES_TIT_CRED_PRIV = letcp;
                #endregion
                //

                if (letcp.Count > 0) {
                    p1.get_LISTA_EMISSORES_TIT_CRED_PRIV = le;
                }
                #endregion

                #region Calcula total de títulos privados
                PosicaoRendaFixaHistoricoQuery posicaoRendaFixaHistoricoQuery = new PosicaoRendaFixaHistoricoQuery("P");
                TituloRendaFixaQuery tituloRendaFixaQuery = new TituloRendaFixaQuery("T");
                PapelRendaFixaQuery papelRendaFixaQuery = new PapelRendaFixaQuery("A");
                posicaoRendaFixaHistoricoQuery.Select(posicaoRendaFixaHistoricoQuery.ValorMercado.Sum());
                posicaoRendaFixaHistoricoQuery.InnerJoin(tituloRendaFixaQuery).On(tituloRendaFixaQuery.IdTitulo == posicaoRendaFixaHistoricoQuery.IdTitulo);
                posicaoRendaFixaHistoricoQuery.InnerJoin(papelRendaFixaQuery).On(papelRendaFixaQuery.IdPapel == tituloRendaFixaQuery.IdPapel);
                posicaoRendaFixaHistoricoQuery.Where(posicaoRendaFixaHistoricoQuery.IdCliente.Equal(idCliente),
                                                     posicaoRendaFixaHistoricoQuery.DataHistorico.Equal(dataPosicao),
                                                     papelRendaFixaQuery.TipoPapel.Equal((byte)TipoPapelTitulo.Privado));

                PosicaoRendaFixaHistorico posicaoRendaFixaHistoricoPrivado = new PosicaoRendaFixaHistorico();
                posicaoRendaFixaHistoricoPrivado.Load(posicaoRendaFixaHistoricoQuery);

                decimal valorRendaFixaPrivado = posicaoRendaFixaHistoricoPrivado.ValorMercado.HasValue ? posicaoRendaFixaHistoricoPrivado.ValorMercado.Value : 0;
                #endregion
                decimal percentRFPrivado = valorRendaFixaPrivado / valorPL * 100M;

                p1.tot_ativos_cred_priv = Math.Round(percentRFPrivado, 1);

                DateTime? dataUltimaCobranca = null;
                decimal? valorCotaUltimaCobranca = null;
                #region Taxa de Performance (ultima data e cota)
                TabelaTaxaPerformanceCollection tabelaTaxaPerformanceCollection = new TabelaTaxaPerformanceCollection();
                tabelaTaxaPerformanceCollection.Query.Select(tabelaTaxaPerformanceCollection.Query.ZeraPerformanceNegativa);
                tabelaTaxaPerformanceCollection.Query.Where(tabelaTaxaPerformanceCollection.Query.IdCarteira.Equal(idCliente),
                                                            tabelaTaxaPerformanceCollection.Query.DataReferencia.LessThanOrEqual(dataPosicao));
                tabelaTaxaPerformanceCollection.Query.Load();

                bool cobraNegativo = false;
                if (tabelaTaxaPerformanceCollection.Count > 0) {
                    cobraNegativo = tabelaTaxaPerformanceCollection[0].ZeraPerformanceNegativa == "S" ? true : false;
                }

                if (!cobraNegativo) 
                {
                    PosicaoCotistaHistoricoCollection posicaoCotistaHistoricoCollection = new PosicaoCotistaHistoricoCollection();
                    posicaoCotistaHistoricoCollection.Query.Select(posicaoCotistaHistoricoCollection.Query.DataUltimoCortePfee);
                    posicaoCotistaHistoricoCollection.Query.Where(posicaoCotistaHistoricoCollection.Query.IdCarteira.Equal(idCliente),
                                                                  posicaoCotistaHistoricoCollection.Query.Quantidade.NotEqual(0));
                    posicaoCotistaHistoricoCollection.Query.OrderBy(posicaoCotistaHistoricoCollection.Query.DataUltimoCortePfee.Descending);
                    posicaoCotistaHistoricoCollection.Query.Load();

                    if (posicaoCotistaHistoricoCollection.Count > 0 && posicaoCotistaHistoricoCollection[0].DataUltimoCortePfee.HasValue)
                    {
                        dataUltimaCobranca = posicaoCotistaHistoricoCollection[0].DataUltimoCortePfee.Value;
                    }

                    HistoricoCota historicoCotaPerformance = new HistoricoCota();
                    if (historicoCotaPerformance.BuscaValorCota(idCliente, dataPosicao) && historicoCotaPerformance.CotaFechamento.HasValue) {
                        valorCotaUltimaCobranca = historicoCotaPerformance.CotaFechamento.Value;
                    }
                }
                #endregion                                

                if (tabelaTaxaPerformanceCollection.Count > 0) {
                    p1.ved_regul_cobr_taxa_perform = cobraNegativo ? "N" : "S";
                    
                    #region RESP_VED_REGUL_COBR_TAXA_PERFORM
                    if (dataUltimaCobranca.HasValue && valorCotaUltimaCobranca.HasValue) {
                        Perfil_1.RESP_VED_REGUL_COBR_TAXA_PERFORM r = new Perfil_1.RESP_VED_REGUL_COBR_TAXA_PERFORM();
                        r.DATA_COTA_FUNDO = dataUltimaCobranca.Value;
                        r.VAL_COTA_FUNDO = Math.Round(valorCotaUltimaCobranca.Value, 5);
                        //
                        p1.get_RESP_VED_REGUL_COBR_TAXA_PERFORM = r;
                    }
                    #endregion                    

                    // Adiciona na lista de Perfil
                    //pLista.Add(p1);
                }
                #endregion

                // Adiciona na lista de Perfil
                pLista.Add(p1);
            }

            if (pLista.Count > 0) {
                // Adiciona Perfil na estrutura do arquivo
                // Cria o Objeto Corpo
                arq.CriaCorpo(); // Para não aparecer o nó <PERFIL_MENSAL> se não existir dados
                //
                arq.getCorpo.getListPerfil = pLista;
            }
        }
    }
    #endregion

    #region Perfil Mensal XML V3
    public class TabelaPerfilCVMV3 {

        /// <summary>
        /// Retorna uma Stream de Mémoria de um arquivo xml.
        /// </summary>
        /// <param name="dataPosicao"></param>
        /// <param name="idClientes"></param>
        /// <param name="ms">Saida: Memory Stream do arquivo TabelaPerfilCVM</param>
        /// <param name="nomeArquivo">Saida: Nome do Arquivo TabelaPerfilCVM</param>     
        /// <exception cref="ProcessaTabelaPerfilException">
        /// throws ProcessaTabelaPerfilException se ocorreu algum erro no Processamento
        /// </exception>
        public void ExportaTabelaPerfilCVM_XMl_3(DateTime dataPosicao, List<int> idClientes, out MemoryStream ms, out string nomeArquivo) {
            ms = new MemoryStream();
            nomeArquivo = "PerfilMensalCVM_V3.xml";
            //            
            Perfil_3.TabelaPerfilCVM_Xml arq = new Perfil_3.TabelaPerfilCVM_Xml();
            //
            /* Monta Cabeçalho */
            this.MontaCabecalho(dataPosicao, arq);

            /* Monta Corpo */
            this.MontaCorpo(dataPosicao, idClientes, arq);

            XmlSerializer x = new XmlSerializer(arq.GetType());

            XmlWriterSettings settings = new XmlWriterSettings();
            settings.Encoding = System.Text.Encoding.GetEncoding("WINDOWS-1252");
            settings.Indent = true;
            settings.IndentChars = "\t";
            settings.NewLineChars = Environment.NewLine;
            settings.ConformanceLevel = ConformanceLevel.Document;

            XmlSerializerNamespaces ns = new XmlSerializerNamespaces();
            ns.Add("", "");

            using (XmlWriter writer = XmlTextWriter.Create(ms, settings)) {
                //x.Serialize(writer, arq, ns);
                x.Serialize(writer, arq);
            }
        }

        /// <summary>
        /// Monta o Cabeçalho do Arquivo XML
        /// </summary>
        /// <param name="dataPosicao"></param>
        /// <param name="arq">Estrutura do arquivo XML</param>
        /// <returns></returns>
        private void MontaCabecalho(DateTime dataPosicao, Perfil_3.TabelaPerfilCVM_Xml arq) {
            arq.getCabecalho.codigoDocumento = 40;
            arq.getCabecalho.dataCompetencia = dataPosicao;
            arq.getCabecalho.dataGeracaoArquivo = DateTime.Now;
            arq.getCabecalho.versao = "3.0";
        }

        /// <summary>
        /// Monta o Corpo do Arquivo XML 
        /// </summary>
        /// <param name="dataPosicao"></param>
        /// <param name="idClientes"></param>
        /// <param name="arq">Estrutura do arquivo XML</param>
        private void MontaCorpo(DateTime dataPosicao, List<int> idClientes, Perfil_3.TabelaPerfilCVM_Xml arq) {
            // -------------------------------------------------
            List<Perfil_3.ListPerfil> pLista = new List<Perfil_3.ListPerfil>();
            //

            ClienteCollection clienteCollection = new ClienteCollection();
            clienteCollection.Query.Where(clienteCollection.Query.IdTipo == (int)TipoClienteFixo.Fundo &&
                                          clienteCollection.Query.StatusAtivo == (byte)StatusAtivoCliente.Ativo &&
                                          clienteCollection.Query.TipoControle.In((byte)TipoControleCliente.CarteiraImportada,
                                                                                   (byte)TipoControleCliente.Completo) &&
                                          clienteCollection.Query.IdCliente.In(idClientes)
                                         );

            clienteCollection.Query.Load();

            foreach (Cliente cliente in clienteCollection) {
                int idCliente = cliente.IdCliente.Value;

                Pessoa pessoa = new Pessoa();
                List<esQueryItem> campos = new List<esQueryItem>();
                campos.Add(pessoa.Query.Cpfcnpj);
                pessoa.LoadByPrimaryKey(campos, idCliente);

                string cnpj = Utilitario.RemoveCaracteresEspeciais(pessoa.Cpfcnpj.ToString()).Trim();

                HistoricoCota historicoCota = new HistoricoCota();
                historicoCota.BuscaValorPatrimonioDia(idCliente, dataPosicao);

                decimal valorPL = historicoCota.PLFechamento.HasValue ? historicoCota.PLFechamento.Value : 0;

                if (valorPL == 0) {
                    throw new Exception("PL zerado ou inexistente para o fundo " + idCliente.ToString());
                }

                #region Cria Objeto de perfil
                Perfil_3.ListPerfil p1 = new Perfil_3.ListPerfil();
                //
                p1.cnpj = cnpj;

                #region Trata Tabela de tipos de cotistas

                //CotistaQuery cotistaQuery = new CotistaQuery("C");
                //PosicaoCotistaHistoricoQuery posicaoCotistaHistoricoQuery = new PosicaoCotistaHistoricoQuery("P");
                //cotistaQuery.Select(cotistaQuery.TipoCotistaCVM, cotistaQuery.TipoCotistaCVM.Count().As("ContTipo"));
                //cotistaQuery.InnerJoin(posicaoCotistaHistoricoQuery).On(posicaoCotistaHistoricoQuery.IdCotista == cotistaQuery.IdCotista);
                //cotistaQuery.Where(posicaoCotistaHistoricoQuery.IdCarteira.Equal(idCliente),
                //                   posicaoCotistaHistoricoQuery.Quantidade.NotEqual(0),
                //                   posicaoCotistaHistoricoQuery.DataHistorico.Equal(dataPosicao),
                //                   cotistaQuery.TipoCotistaCVM.IsNotNull());
                //cotistaQuery.GroupBy(cotistaQuery.TipoCotistaCVM);
                //CotistaCollection cotistaCollection = new CotistaCollection();
                //cotistaCollection.Load(cotistaQuery);

                CotistaQuery cotistaQuery = new CotistaQuery("C");
                PosicaoCotistaHistoricoQuery posicaoCotistaHistoricoQuery = new PosicaoCotistaHistoricoQuery("P");
                //
                cotistaQuery.es.Distinct = true;

                cotistaQuery.Select(cotistaQuery.IdCotista,
                                    cotistaQuery.TipoCotistaCVM.As("TipoCotistaCVM"));
                cotistaQuery.InnerJoin(posicaoCotistaHistoricoQuery).On(posicaoCotistaHistoricoQuery.IdCotista == cotistaQuery.IdCotista);
                cotistaQuery.Where(posicaoCotistaHistoricoQuery.IdCarteira.Equal(idCliente),
                                   posicaoCotistaHistoricoQuery.Quantidade.NotEqual(0),
                                   posicaoCotistaHistoricoQuery.DataHistorico.Equal(dataPosicao),
                                   cotistaQuery.TipoCotistaCVM.IsNotNull());


                // Query Principal
                CotistaQuery cotistaQueryPrincipal = new CotistaQuery("C");
                //
                cotistaQueryPrincipal.Select("<C.TipoCotistaCVM>", "<COUNT(C.TipoCotistaCVM) AS ContTipo>");
                //
                cotistaQueryPrincipal.From(cotistaQuery).As("C");
                cotistaQueryPrincipal.GroupBy("TipoCotistaCVM");
                //    
                CotistaCollection cotistaCollection = new CotistaCollection();
                cotistaCollection.Load(cotistaQueryPrincipal);

                #endregion

                #region NR_CLIENT
                Perfil_3.NR_CLIENT nr = new Perfil_3.NR_CLIENT();
                nr.NR_PF_PRIV_BANK = 0;
                nr.NR_PF_VARJ = 0;
                nr.NR_PJ_N_FINANC_PRIV_BANK = 0;
                nr.NR_PJ_N_FINANC_VARJ = 0;
                nr.NR_BNC_COMERC = 0;
                nr.NR_PJ_CORR_DIST = 0;
                nr.NR_PJ_OUTR_FINANC = 0;
                nr.NR_INV_N_RES = 0;
                nr.NR_ENT_AB_PREV_COMPL = 0;
                nr.NR_ENT_FC_PREV_COMPL = 0;
                nr.NR_REG_PREV_SERV_PUB = 0;
                nr.NR_SOC_SEG_RESEG = 0;
                nr.NR_SOC_CAPTLZ_ARRENDM_MERC = 0;
                nr.NR_FDOS_CLUB_INV = 0;
                nr.NR_COTST_DISTR_FDO = 0;
                nr.NR_OUTROS_N_RELAC = 0;
                //

                for (int i = 0; i < cotistaCollection.Count; i++) {
                    byte tipoCotistaCVM = Convert.ToByte(cotistaCollection[i].TipoCotistaCVM.Value);
                    int cont = Convert.ToInt32(cotistaCollection[i].GetColumn("ContTipo"));

                    if (tipoCotistaCVM == (byte)TipoCotista.PESSOA_FISICA_PRIVATE_BANKING) {
                        nr.NR_PF_PRIV_BANK = cont;
                    }
                    else if (tipoCotistaCVM == (byte)TipoCotista.PESSOA_FISICA_VAREJO) {
                        nr.NR_PF_VARJ = cont;
                    }
                    else if (tipoCotistaCVM == (byte)TipoCotista.PESSOA_JURIDICA_NAO_FINANCEIRA_PRIVATE_BANKING) {
                        nr.NR_PJ_N_FINANC_PRIV_BANK = cont;
                    }
                    else if (tipoCotistaCVM == (byte)TipoCotista.PESSOA_JURIDICA_NAO_FINANCEIRA_VAREJO) {
                        nr.NR_PJ_N_FINANC_VARJ = cont;
                    }
                    else if (tipoCotistaCVM == (byte)TipoCotista.BANCO_COMERCIAL) {
                        nr.NR_BNC_COMERC = cont;
                    }
                    else if (tipoCotistaCVM == (byte)TipoCotista.CORRETORA_DISTRIBUIDORA) {
                        nr.NR_PJ_CORR_DIST = cont;
                    }
                    else if (tipoCotistaCVM == (byte)TipoCotista.OUTRAS_PESSOAS_JURIDICAS_FINANCEIRAS) {
                        nr.NR_PJ_OUTR_FINANC = cont;
                    }
                    else if (tipoCotistaCVM == (byte)TipoCotista.INVESTIDORES_NAO_RESIDENTES) {
                        nr.NR_INV_N_RES = cont;
                    }
                    else if (tipoCotistaCVM == (byte)TipoCotista.ENTIDADE_ABERTA_PREVIDENCIA_COMPLEMENTAR) {
                        nr.NR_ENT_AB_PREV_COMPL = cont;
                    }
                    else if (tipoCotistaCVM == (byte)TipoCotista.ENTIDADE_FECHADA_PREVIDENCIA_COMPLEMENTAR) {
                        nr.NR_ENT_FC_PREV_COMPL = cont;
                    }
                    else if (tipoCotistaCVM == (byte)TipoCotista.REGIME_PROPRIO_PREVIDENCIA_SERVIDORES_PUBLICOS) {
                        nr.NR_REG_PREV_SERV_PUB = cont;
                    }
                    else if (tipoCotistaCVM == (byte)TipoCotista.SOCIEDADE_SEGURADORA_RESSEGURADORA) {
                        nr.NR_SOC_SEG_RESEG = cont;
                    }
                    else if (tipoCotistaCVM == (byte)TipoCotista.SOCIEDADE_CAPITALIZACAO_ARRENDAMENTO_MERCANTIL) {
                        nr.NR_SOC_CAPTLZ_ARRENDM_MERC = cont;
                    }
                    else if (tipoCotistaCVM == (byte)TipoCotista.FUNDOS_CLUBES_INVESTIMENTOS) {
                        nr.NR_FDOS_CLUB_INV = cont;
                    }
                    else if (tipoCotistaCVM == (byte)TipoCotista.COTISTAS_DISTRIBUIDORES_FUNDO_CONTA_ORDEM) {
                        nr.NR_COTST_DISTR_FDO = cont;
                    }
                    else if (tipoCotistaCVM == (byte)TipoCotista.OUTROS_TIPOS_COTISTAS) {
                        nr.NR_OUTROS_N_RELAC = cont;
                    }
                }

                p1.get_NR_CLIENT = nr;
                #endregion

                #region Trata tabela de tipos de cotistas por patrimônio detido
                cotistaQuery = new CotistaQuery("C");
                posicaoCotistaHistoricoQuery = new PosicaoCotistaHistoricoQuery("P");
                cotistaQuery.Select(cotistaQuery.TipoCotistaCVM, posicaoCotistaHistoricoQuery.Quantidade.Sum().As("SaldoTipo"));
                cotistaQuery.InnerJoin(posicaoCotistaHistoricoQuery).On(posicaoCotistaHistoricoQuery.IdCotista == cotistaQuery.IdCotista);
                cotistaQuery.Where(posicaoCotistaHistoricoQuery.IdCarteira.Equal(idCliente),
                                   posicaoCotistaHistoricoQuery.Quantidade.NotEqual(0),
                                   posicaoCotistaHistoricoQuery.DataHistorico.Equal(dataPosicao),
                                   cotistaQuery.TipoCotistaCVM.IsNotNull());
                cotistaQuery.GroupBy(cotistaQuery.TipoCotistaCVM);
                cotistaCollection = new CotistaCollection();
                cotistaCollection.Load(cotistaQuery);
                #endregion

                HistoricoCota historicoCota1 = new HistoricoCota();
                historicoCota1.BuscaQuantidadeCotas(idCliente, dataPosicao);
                //
                decimal valorPL1 = historicoCota1.QuantidadeFechamento.HasValue ? historicoCota1.QuantidadeFechamento.Value : 0;

                if (valorPL1 == 0) {
                    throw new Exception("Quantidade Fechamento Zerada  para o fundo " + idCliente.ToString());
                }

                #region DISTR_PATRIM

                Perfil_3.DISTR_PATRIM dp = new Perfil_3.DISTR_PATRIM();
                //
                dp.PR_PF_PRIV_BANK = 0;
                dp.PR_PF_VARJ = 0;
                dp.PR_PJ_N_FINANC_PRIV_BANK = 0;
                dp.PR_PJ_N_FINANC_VARJ = 0;
                dp.PR_BNC_COMERC = 0;
                dp.PR_PJ_CORR_DIST = 0;
                dp.PR_PJ_OUTR_FINANC = 0;
                dp.PR_INV_N_RES = 0;
                dp.PR_ENT_AB_PREV_COMPL = 0;
                dp.PR_ENT_FC_PREV_COMPL = 0;
                dp.PR_REG_PREV_SERV_PUB = 0;
                dp.PR_SOC_SEG_RESEG = 0;
                dp.PR_SOC_CAPTLZ_ARRENDM_MERC = 0;
                dp.PR_FDOS_CLUB_INV = 0;
                dp.PR_COTST_DISTR_FDO = 0;
                dp.PR_OUTROS_N_RELAC = 0;

                for (int i = 0; i < cotistaCollection.Count; i++) {
                    byte tipoCotistaCVM = Convert.ToByte(cotistaCollection[i].TipoCotistaCVM.Value);
                    decimal percentSaldo = Math.Round(Convert.ToDecimal(cotistaCollection[i].GetColumn("SaldoTipo")) / valorPL1 * 100M, 1);

                    if (tipoCotistaCVM == (byte)TipoCotista.PESSOA_FISICA_PRIVATE_BANKING) {
                        dp.PR_PF_PRIV_BANK = percentSaldo;
                    }
                    else if (tipoCotistaCVM == (byte)TipoCotista.PESSOA_FISICA_VAREJO) {
                        dp.PR_PF_VARJ = percentSaldo;
                    }
                    else if (tipoCotistaCVM == (byte)TipoCotista.PESSOA_JURIDICA_NAO_FINANCEIRA_PRIVATE_BANKING) {
                        dp.PR_PJ_N_FINANC_PRIV_BANK = percentSaldo;
                    }
                    else if (tipoCotistaCVM == (byte)TipoCotista.PESSOA_JURIDICA_NAO_FINANCEIRA_VAREJO) {
                        dp.PR_PJ_N_FINANC_VARJ = percentSaldo;
                    }
                    else if (tipoCotistaCVM == (byte)TipoCotista.BANCO_COMERCIAL) {
                        dp.PR_BNC_COMERC = percentSaldo;
                    }
                    else if (tipoCotistaCVM == (byte)TipoCotista.CORRETORA_DISTRIBUIDORA) {
                        dp.PR_PJ_CORR_DIST = percentSaldo;
                    }
                    else if (tipoCotistaCVM == (byte)TipoCotista.OUTRAS_PESSOAS_JURIDICAS_FINANCEIRAS) {
                        dp.PR_PJ_OUTR_FINANC = percentSaldo;
                    }
                    else if (tipoCotistaCVM == (byte)TipoCotista.INVESTIDORES_NAO_RESIDENTES) {
                        dp.PR_INV_N_RES = percentSaldo;
                    }
                    else if (tipoCotistaCVM == (byte)TipoCotista.ENTIDADE_ABERTA_PREVIDENCIA_COMPLEMENTAR) {
                        dp.PR_ENT_AB_PREV_COMPL = percentSaldo;
                    }
                    else if (tipoCotistaCVM == (byte)TipoCotista.ENTIDADE_FECHADA_PREVIDENCIA_COMPLEMENTAR) {
                        dp.PR_ENT_FC_PREV_COMPL = percentSaldo;
                    }
                    else if (tipoCotistaCVM == (byte)TipoCotista.REGIME_PROPRIO_PREVIDENCIA_SERVIDORES_PUBLICOS) {
                        dp.PR_REG_PREV_SERV_PUB = percentSaldo;
                    }
                    else if (tipoCotistaCVM == (byte)TipoCotista.SOCIEDADE_SEGURADORA_RESSEGURADORA) {
                        dp.PR_SOC_SEG_RESEG = percentSaldo;
                    }
                    else if (tipoCotistaCVM == (byte)TipoCotista.SOCIEDADE_CAPITALIZACAO_ARRENDAMENTO_MERCANTIL) {
                        dp.PR_SOC_CAPTLZ_ARRENDM_MERC = percentSaldo;
                    }
                    else if (tipoCotistaCVM == (byte)TipoCotista.FUNDOS_CLUBES_INVESTIMENTOS) {
                        dp.PR_FDOS_CLUB_INV = percentSaldo;
                    }
                    else if (tipoCotistaCVM == (byte)TipoCotista.COTISTAS_DISTRIBUIDORES_FUNDO_CONTA_ORDEM) {
                        dp.PR_COTST_DISTR_FDO = percentSaldo;
                    }
                    else if (tipoCotistaCVM == (byte)TipoCotista.OUTROS_TIPOS_COTISTAS) {
                        dp.PR_OUTROS_N_RELAC = percentSaldo;
                    }
                }
                //
                p1.get_DISTR_PATRIM = dp;
                #endregion

                TabelaPerfilMensalCVMCollection tabelaPerfilMensalCVMCollection = new TabelaPerfilMensalCVMCollection();
                #region Trata tabela informada pelo usuário para a CVM
                DateTime primeiroDiaMes = Calendario.RetornaPrimeiroDiaCorridoMes(dataPosicao, 0);
                DateTime ultimoDiaMes = Calendario.RetornaUltimoDiaCorridoMes(dataPosicao, 0);

                tabelaPerfilMensalCVMCollection.Query.Where(tabelaPerfilMensalCVMCollection.Query.IdCarteira.Equal(idCliente),
                                                            tabelaPerfilMensalCVMCollection.Query.Data.GreaterThanOrEqual(primeiroDiaMes),
                                                            tabelaPerfilMensalCVMCollection.Query.Data.LessThanOrEqual(ultimoDiaMes));
                tabelaPerfilMensalCVMCollection.Query.Load();

                if (tabelaPerfilMensalCVMCollection.Count == 0) {
                    throw new Exception("Tabela Perfil Mensal não definida para o fundo " + idCliente.ToString());
                }
                #endregion

                TabelaPerfilMensalCVM tabelaPerfilMensalCVM = tabelaPerfilMensalCVMCollection[0];

                if (!String.IsNullOrEmpty(tabelaPerfilMensalCVM.Secao3)) {
                    p1.resm_teor_vt_profrd = tabelaPerfilMensalCVM.Secao3;
                }
                if (!String.IsNullOrEmpty(tabelaPerfilMensalCVM.Secao4)) {
                    p1.just_sum_vt_profrd = tabelaPerfilMensalCVM.Secao4;
                }
                if (tabelaPerfilMensalCVM.Secao5.HasValue && tabelaPerfilMensalCVM.Secao5.Value != 0) {
                    p1.var_perc_pl = tabelaPerfilMensalCVM.Secao5.Value;
                }
                if (tabelaPerfilMensalCVM.Secao6.HasValue && tabelaPerfilMensalCVM.Secao6.Value != 0) {
                    p1.mod_var_utiliz = tabelaPerfilMensalCVM.Secao6.Value.ToString();
                }

                if (tabelaPerfilMensalCVM.Secao7.HasValue && tabelaPerfilMensalCVM.Secao7.Value != 0) {
                    p1.praz_med_cart_tit = Math.Abs(tabelaPerfilMensalCVM.Secao7.Value);
                }
                else {
                    Carteira carteira = new Carteira();
                    decimal prazoMedio = carteira.RetornaPrazoMedio(idCliente, dataPosicao) / 30M; //em meses (30 dias)

                    p1.praz_med_cart_tit = Math.Abs(Math.Round(prazoMedio, 4));
                }

                if (!String.IsNullOrEmpty(tabelaPerfilMensalCVM.Secao8)) {
                    p1.res_delib = tabelaPerfilMensalCVM.Secao8;
                }

                p1.total_recurs_exter = 0; //POR ORA NÃO SERÁ CONTROLADO FIEX OU ALGO PARECIDO
                p1.total_recurs_br = 0; //POR ORA NÃO SERÁ CONTROLADO FIEX OU ALGO PARECIDO

                #region VARIACAO_PERC_VAL_COTA
                Perfil_3.VARIACAO_PERC_VAL_COTA v = new Perfil_3.VARIACAO_PERC_VAL_COTA();
                if (tabelaPerfilMensalCVM.Secao11PercentCota.HasValue) {
                    v.VAL_PERCENT = tabelaPerfilMensalCVM.Secao11PercentCota.Value;
                }
                //                        
                #region Add lista de FATOR_PRIMIT_RISCO em VARIACAO_PERC_VAL_COTA
                List<Perfil_3.FATOR_PRIMIT_RISCO> lp = new List<Perfil_3.FATOR_PRIMIT_RISCO>();
                //                            
                if (!String.IsNullOrEmpty(tabelaPerfilMensalCVM.Secao11Cenario1)) {
                    Perfil_3.FATOR_PRIMIT_RISCO fp = new Perfil_3.FATOR_PRIMIT_RISCO();
                    //
                    fp.NOME_FATOR_PRIMIT_RISCO = tabelaPerfilMensalCVM.Secao11Cenario1;
                    fp.CEN_UTIL = !String.IsNullOrEmpty(tabelaPerfilMensalCVM.Secao11Fator1) ? tabelaPerfilMensalCVM.Secao11Fator1 : " "; // codigo Alt 255
                    //
                    lp.Add(fp);
                }
                if (!String.IsNullOrEmpty(tabelaPerfilMensalCVM.Secao11Cenario2)) {
                    Perfil_3.FATOR_PRIMIT_RISCO fp1 = new Perfil_3.FATOR_PRIMIT_RISCO();
                    //
                    fp1.NOME_FATOR_PRIMIT_RISCO = tabelaPerfilMensalCVM.Secao11Cenario2;
                    fp1.CEN_UTIL = !String.IsNullOrEmpty(tabelaPerfilMensalCVM.Secao11Fator2) ? tabelaPerfilMensalCVM.Secao11Fator2 : " "; // codigo Alt 255;
                    lp.Add(fp1);
                }
                if (!String.IsNullOrEmpty(tabelaPerfilMensalCVM.Secao11Cenario3)) {
                    Perfil_3.FATOR_PRIMIT_RISCO fp2 = new Perfil_3.FATOR_PRIMIT_RISCO();
                    //
                    fp2.NOME_FATOR_PRIMIT_RISCO = tabelaPerfilMensalCVM.Secao11Cenario3;
                    fp2.CEN_UTIL = !String.IsNullOrEmpty(tabelaPerfilMensalCVM.Secao11Fator3) ? tabelaPerfilMensalCVM.Secao11Fator3 : " "; // codigo Alt 255;
                    lp.Add(fp2);
                }
                if (!String.IsNullOrEmpty(tabelaPerfilMensalCVM.Secao11Cenario4)) {
                    Perfil_3.FATOR_PRIMIT_RISCO fp3 = new Perfil_3.FATOR_PRIMIT_RISCO();
                    //
                    fp3.NOME_FATOR_PRIMIT_RISCO = tabelaPerfilMensalCVM.Secao11Cenario4;
                    fp3.CEN_UTIL = !String.IsNullOrEmpty(tabelaPerfilMensalCVM.Secao11Fator4) ? tabelaPerfilMensalCVM.Secao11Fator4 : " "; // codigo Alt 255;
                    lp.Add(fp3);
                }
                if (!String.IsNullOrEmpty(tabelaPerfilMensalCVM.Secao11Cenario5)) {
                    Perfil_3.FATOR_PRIMIT_RISCO fp4 = new Perfil_3.FATOR_PRIMIT_RISCO();
                    //
                    fp4.NOME_FATOR_PRIMIT_RISCO = tabelaPerfilMensalCVM.Secao11Cenario5;
                    fp4.CEN_UTIL = !String.IsNullOrEmpty(tabelaPerfilMensalCVM.Secao11Fator5) ? tabelaPerfilMensalCVM.Secao11Fator5 : " "; // codigo Alt 255;
                    lp.Add(fp4);
                }
                
                //
                if (lp.Count > 0) {
                    v.lista_fator_primit_risco.get_FATOR_PRIMIT_RISCO = lp;
                }
                #endregion
                //
                p1.get_VARIACAO_PERC_VAL_COTA = v;
                #endregion

                if (tabelaPerfilMensalCVM.Secao12.HasValue && tabelaPerfilMensalCVM.Secao12.Value != 0) {
                    p1.var_diar_perc_cota_fdo_pior_cen_estress = tabelaPerfilMensalCVM.Secao12.Value;
                }
                if (tabelaPerfilMensalCVM.Secao13.HasValue && tabelaPerfilMensalCVM.Secao13.Value != 0) {
                    p1.var_diar_perc_patrim_fdo_var_n_taxa_anual = tabelaPerfilMensalCVM.Secao13.Value;
                }
                if (tabelaPerfilMensalCVM.Secao14.HasValue && tabelaPerfilMensalCVM.Secao14.Value != 0) {
                    p1.var_diar_perc_patrim_fdo_var_n_taxa_cambio = tabelaPerfilMensalCVM.Secao14.Value;
                }
                if (tabelaPerfilMensalCVM.Secao15.HasValue && tabelaPerfilMensalCVM.Secao15.Value != 0) {
                    p1.var_patrim_fdo_n_preco_acoes = tabelaPerfilMensalCVM.Secao15.Value;
                }

                #region VARIACAO_DIAR_PERC_PATRIM_FDO_VAR_N_OUTROS
                Perfil_3.VARIACAO_DIAR_PERC_PATRIM_FDO_VAR_N_OUTROS vd = new Perfil_3.VARIACAO_DIAR_PERC_PATRIM_FDO_VAR_N_OUTROS();

                string fatorRisco = !String.IsNullOrEmpty(tabelaPerfilMensalCVM.Secao16Fator) ? tabelaPerfilMensalCVM.Secao16Fator.Trim() : "-";

                if (!String.IsNullOrEmpty(fatorRisco)) { // Sempre entra
                    vd.FATOR_RISCO_OUTROS = fatorRisco;

                    decimal variacao = 0;
                    if (tabelaPerfilMensalCVM.Secao16Variacao.HasValue) {
                        variacao = tabelaPerfilMensalCVM.Secao16Variacao.Value;
                    }
                    vd.VAL_PERCENT_OUTROS = variacao;
                    //
                    p1.get_VARIACAO_DIAR_PERC_PATRIM_FDO_VAR_N_OUTROS = vd;
                }
                #endregion

                #region VALOR_NOC_TOT_CONTRAT_DERIV_MANT_FDO
                //POR ORA SEM CONTROLE DE FUNDOS COM POSIÇÃO EM FUTUROS DE BALCÃO

                //VALOR_NOC_TOT_CONTRAT_DERIV_MANT_FDO vc = new VALOR_NOC_TOT_CONTRAT_DERIV_MANT_FDO();
                //vc.VAL_COLATERAL = "1";
                ////                        
                //#region Add lista de FATOR_RISCO_NOC em VALOR_NOC_TOT_CONTRAT_DERIV_MANT_FDO
                //List<FATOR_RISCO_NOC> lr = new List<FATOR_RISCO_NOC>();
                ////            
                //FATOR_RISCO_NOC fr = new FATOR_RISCO_NOC();
                //fr.NOME_FATOR_NOC = "1";
                //fr.VAL_FATOR_RISCO_NOC_LONG = "1";
                //fr.VAL_FATOR_RISCO_NOC_SHORT = "1";
                ////
                //lr.Add(fr);
                ////
                //if (lr.Count > 0) {
                //    vc.lista_fator_risco_noc = new LISTA_FATOR_RISCO_NOC();
                //    vc.lista_fator_risco_noc.get_FATOR_RISCO_NOC = lr;
                //}
                //#endregion
                ////
                //p1.get_VALOR_NOC_TOT_CONTRAT_DERIV_MANT_FDO = vc;
                #endregion

                #region LISTA_OPER_CURS_MERC_BALCAO
                //POR ORA SEM CONTROLE DE FUNDOS COM POSIÇÃO EM MERCADO DE BALCÃO SEM GARANTIA DE CONTRAPARTE

                //LISTA_OPER_CURS_MERC_BALCAO lm = new LISTA_OPER_CURS_MERC_BALCAO();
                ////
                //#region Add lista de OPER_CURS_MERC_BALCAO em LISTA_OPER_CURS_MERC_BALCAO
                //List<OPER_CURS_MERC_BALCAO> lb = new List<OPER_CURS_MERC_BALCAO>();
                ////            
                //OPER_CURS_MERC_BALCAO ob = new OPER_CURS_MERC_BALCAO();
                //ob.TP_PESSOA = "1";
                //ob.NR_PF_PJ_COMITENTE = "1";
                //ob.PARTE_RELACIONADA = "1";
                //ob.VALOR_PARTE = "1";
                ////
                //lb.Add(ob);
                ////
                //lm.get_OPER_CURS_MERC_BALCAO = lb;
                //#endregion
                ////
                //p1.get_LISTA_OPER_CURS_MERC_BALCAO = lm;
                #endregion

                string tipoPessoa1 = "";
                string tipoPessoa2 = "";
                string tipoPessoa3 = "";
                string cpfcnpj1 = "";
                string cpfcnpj2 = "";
                string cpfcnpj3 = "";
                string indicaParte1 = "";
                string indicaParte2 = "";
                string indicaParte3 = "";
                decimal percentEmissor1 = 0;
                decimal percentEmissor2 = 0;
                decimal percentEmissor3 = 0;
                decimal valorPartesRelacionadas = 0;
                #region Calcula lista com até 3 emissores e total em partes relacionadas
                Carteira carteiraInvestidor = new Carteira();
                campos = new List<esQueryItem>();
                campos.Add(carteiraInvestidor.Query.IdAgenteAdministrador);
                campos.Add(carteiraInvestidor.Query.IdAgenteGestor);
                carteiraInvestidor.LoadByPrimaryKey(campos, idCliente);


                PosicaoFundoHistoricoCollection posicaoFundoHistoricoCollection = new PosicaoFundoHistoricoCollection();
                posicaoFundoHistoricoCollection.Query.Select(posicaoFundoHistoricoCollection.Query.IdCarteira,
                                                             posicaoFundoHistoricoCollection.Query.ValorBruto.Sum());
                posicaoFundoHistoricoCollection.Query.Where(posicaoFundoHistoricoCollection.Query.IdCliente.Equal(idCliente),
                                                            posicaoFundoHistoricoCollection.Query.DataHistorico.Equal(dataPosicao),
                                                            posicaoFundoHistoricoCollection.Query.Quantidade.NotEqual(0));
                posicaoFundoHistoricoCollection.Query.GroupBy(posicaoFundoHistoricoCollection.Query.IdCarteira);
                posicaoFundoHistoricoCollection.Query.Load();

                foreach (PosicaoFundoHistorico posicaoFundoHistorico in posicaoFundoHistoricoCollection) {
                    int idCarteira = posicaoFundoHistorico.IdCarteira.Value;
                    decimal valor = posicaoFundoHistorico.ValorBruto.Value;

                    Carteira carteiraAplicada = new Carteira();
                    campos = new List<esQueryItem>();
                    campos.Add(carteiraAplicada.Query.IdAgenteAdministrador);
                    campos.Add(carteiraAplicada.Query.IdAgenteGestor);
                    carteiraAplicada.LoadByPrimaryKey(campos, idCarteira);

                    if (carteiraInvestidor.IdAgenteAdministrador.Value == carteiraAplicada.IdAgenteAdministrador.Value ||
                        carteiraInvestidor.IdAgenteGestor.Value == carteiraAplicada.IdAgenteGestor.Value) {
                        valorPartesRelacionadas += valor;
                    }
                }

                decimal valorEmissor1 = 0;
                decimal valorEmissor2 = 0;
                decimal valorEmissor3 = 0;
                PosicaoRendaFixaHistoricoCollection posicaoRendaFixaHistoricoCollection = new PosicaoRendaFixaHistoricoCollection();
                posicaoRendaFixaHistoricoCollection.Query.Select(posicaoRendaFixaHistoricoCollection.Query.IdTitulo,
                                                                 posicaoRendaFixaHistoricoCollection.Query.ValorMercado.Sum());
                posicaoRendaFixaHistoricoCollection.Query.Where(posicaoRendaFixaHistoricoCollection.Query.IdCliente.Equal(idCliente),
                                                            posicaoRendaFixaHistoricoCollection.Query.DataHistorico.Equal(dataPosicao),
                                                            posicaoRendaFixaHistoricoCollection.Query.Quantidade.NotEqual(0));
                posicaoRendaFixaHistoricoCollection.Query.GroupBy(posicaoRendaFixaHistoricoCollection.Query.IdTitulo);
                posicaoRendaFixaHistoricoCollection.Query.Load();

                foreach (PosicaoRendaFixaHistorico posicaoRendaFixaHistorico in posicaoRendaFixaHistoricoCollection) {
                    int idTitulo = posicaoRendaFixaHistorico.IdTitulo.Value;
                    decimal valor = posicaoRendaFixaHistorico.ValorMercado.Value;

                    TituloRendaFixa tituloRendaFixa = new TituloRendaFixa();
                    campos = new List<esQueryItem>();
                    campos.Add(tituloRendaFixa.Query.IdPapel);
                    campos.Add(tituloRendaFixa.Query.IdEmissor);
                    tituloRendaFixa.LoadByPrimaryKey(campos, idTitulo);

                    PapelRendaFixa papelRendaFixa = new PapelRendaFixa();
                    campos = new List<esQueryItem>();
                    campos.Add(papelRendaFixa.Query.TipoPapel);
                    papelRendaFixa.LoadByPrimaryKey(campos, tituloRendaFixa.IdPapel.Value);

                    Emissor emissor = new Emissor();
                    campos = new List<esQueryItem>();
                    campos.Add(emissor.Query.IdAgente);
                    campos.Add(emissor.Query.TipoEmissor);
                    campos.Add(emissor.Query.Cnpj);
                    emissor.LoadByPrimaryKey(campos, tituloRendaFixa.IdEmissor.Value);

                    string parteRelacionada = "N";
                    if (emissor.IdAgente.HasValue && (carteiraInvestidor.IdAgenteAdministrador.Value == emissor.IdAgente.Value ||
                        carteiraInvestidor.IdAgenteGestor.Value == emissor.IdAgente.Value)) {
                        parteRelacionada = "S";
                        valorPartesRelacionadas += valor;
                    }

                    if (papelRendaFixa.TipoPapel == (byte)TipoPapelTitulo.Privado) {
                        if (valor >= valorEmissor1) {
                            valorEmissor3 = valorEmissor2;
                            valorEmissor2 = valorEmissor1;
                            valorEmissor1 = valor;

                            tipoPessoa3 = tipoPessoa2;
                            tipoPessoa2 = tipoPessoa1;
                            tipoPessoa1 = emissor.TipoEmissor.Value == (byte)TipoEmissor.PessoaFisica ? "PF" : "PJ";

                            cpfcnpj3 = cpfcnpj2;
                            cpfcnpj2 = cpfcnpj1;
                            cpfcnpj1 = emissor.Cnpj;

                            indicaParte3 = indicaParte2;
                            indicaParte2 = indicaParte1;
                            indicaParte1 = parteRelacionada;

                            percentEmissor3 = percentEmissor2;
                            percentEmissor2 = percentEmissor1;
                            percentEmissor1 = historicoCota.PLFechamento.HasValue ? valor / historicoCota.PLFechamento.Value * 100M : 0;
                        }
                        else if (valor >= valorEmissor2) {
                            valorEmissor3 = valorEmissor2;
                            valorEmissor2 = valor;

                            tipoPessoa3 = tipoPessoa2;
                            tipoPessoa2 = emissor.TipoEmissor.Value == (byte)TipoEmissor.PessoaFisica ? "PF" : "PJ";

                            cpfcnpj3 = cpfcnpj2;
                            cpfcnpj2 = emissor.Cnpj;

                            indicaParte3 = indicaParte2;
                            indicaParte2 = parteRelacionada;

                            percentEmissor3 = percentEmissor2;
                            percentEmissor2 = historicoCota.PLFechamento.HasValue ? valor / historicoCota.PLFechamento.Value * 100M : 0;
                        }
                        else if (valor >= valorEmissor3) {
                            valorEmissor3 = valor;

                            tipoPessoa3 = emissor.TipoEmissor.Value == (byte)TipoEmissor.PessoaFisica ? "PF" : "PJ";

                            cpfcnpj3 = emissor.Cnpj;

                            indicaParte3 = parteRelacionada;

                            percentEmissor3 = historicoCota.PLFechamento.HasValue ? valor / historicoCota.PLFechamento.Value * 100M : 0;
                        }
                    }


                }
                #endregion
                decimal percentPartesRelacionadas = valorPartesRelacionadas / valorPL * 100M;

                p1.tot_ativos_p_relac = Math.Round(percentPartesRelacionadas, 1);

                #region LISTA_EMISSORES_TIT_CRED_PRIV
                Perfil_3.LISTA_EMISSORES_TIT_CRED_PRIV le = new Perfil_3.LISTA_EMISSORES_TIT_CRED_PRIV();
                //
                #region Add lista de EMISSORES_TIT_CRED_PRIV em LISTA_EMISSORES_TIT_CRED_PRIV
                List<Perfil_3.EMISSORES_TIT_CRED_PRIV> letcp = new List<Perfil_3.EMISSORES_TIT_CRED_PRIV>();
                //            
                Perfil_3.EMISSORES_TIT_CRED_PRIV lep = new Perfil_3.EMISSORES_TIT_CRED_PRIV();
                if (tipoPessoa1 != "") {

                    string cpfcnpj1Aux = String.IsNullOrEmpty(cpfcnpj1) ? " " : cpfcnpj1;

                    lep.TP_PESSOA_EMISSOR = tipoPessoa1;
                    lep.NR_PF_PJ_EMISSOR = cpfcnpj1Aux;
                    lep.PARTE_RELACIONADA = indicaParte1;
                    lep.VALOR_PARTE = Math.Round(percentEmissor1, 1);

                    letcp.Add(lep);
                }
                if (tipoPessoa2 != "") {

                    string cpfcnpj2Aux = String.IsNullOrEmpty(cpfcnpj2) ? " " : cpfcnpj2;

                    lep.TP_PESSOA_EMISSOR = tipoPessoa2;
                    lep.NR_PF_PJ_EMISSOR = cpfcnpj2Aux;
                    lep.PARTE_RELACIONADA = indicaParte2;
                    lep.VALOR_PARTE = Math.Round(percentEmissor2, 1);

                    letcp.Add(lep);
                }
                if (tipoPessoa3 != "") {

                    string cpfcnpj3Aux = String.IsNullOrEmpty(cpfcnpj3) ? " " : cpfcnpj3;

                    lep.TP_PESSOA_EMISSOR = tipoPessoa3;
                    lep.NR_PF_PJ_EMISSOR = cpfcnpj3Aux;
                    lep.PARTE_RELACIONADA = indicaParte3;
                    lep.VALOR_PARTE = Math.Round(percentEmissor3, 1);

                    letcp.Add(lep);
                }
                //                
                le.get_EMISSORES_TIT_CRED_PRIV = letcp;
                #endregion
                //

                if (letcp.Count > 0) {
                    p1.get_LISTA_EMISSORES_TIT_CRED_PRIV = le;
                }
                #endregion

                #region Calcula total de títulos privados
                PosicaoRendaFixaHistoricoQuery posicaoRendaFixaHistoricoQuery = new PosicaoRendaFixaHistoricoQuery("P");
                TituloRendaFixaQuery tituloRendaFixaQuery = new TituloRendaFixaQuery("T");
                PapelRendaFixaQuery papelRendaFixaQuery = new PapelRendaFixaQuery("A");
                posicaoRendaFixaHistoricoQuery.Select(posicaoRendaFixaHistoricoQuery.ValorMercado.Sum());
                posicaoRendaFixaHistoricoQuery.InnerJoin(tituloRendaFixaQuery).On(tituloRendaFixaQuery.IdTitulo == posicaoRendaFixaHistoricoQuery.IdTitulo);
                posicaoRendaFixaHistoricoQuery.InnerJoin(papelRendaFixaQuery).On(papelRendaFixaQuery.IdPapel == tituloRendaFixaQuery.IdPapel);
                posicaoRendaFixaHistoricoQuery.Where(posicaoRendaFixaHistoricoQuery.IdCliente.Equal(idCliente),
                                                     posicaoRendaFixaHistoricoQuery.DataHistorico.Equal(dataPosicao),
                                                     papelRendaFixaQuery.TipoPapel.Equal((byte)TipoPapelTitulo.Privado));

                PosicaoRendaFixaHistorico posicaoRendaFixaHistoricoPrivado = new PosicaoRendaFixaHistorico();
                posicaoRendaFixaHistoricoPrivado.Load(posicaoRendaFixaHistoricoQuery);

                decimal valorRendaFixaPrivado = posicaoRendaFixaHistoricoPrivado.ValorMercado.HasValue ? posicaoRendaFixaHistoricoPrivado.ValorMercado.Value : 0;
                #endregion
                decimal percentRFPrivado = valorRendaFixaPrivado / valorPL * 100M;

                p1.tot_ativos_cred_priv = Math.Round(percentRFPrivado, 1);

                DateTime? dataUltimaCobranca = null;
                decimal? valorCotaUltimaCobranca = null;
                #region Taxa de Performance (ultima data e cota)
                TabelaTaxaPerformanceCollection tabelaTaxaPerformanceCollection = new TabelaTaxaPerformanceCollection();
                tabelaTaxaPerformanceCollection.Query.Select(tabelaTaxaPerformanceCollection.Query.ZeraPerformanceNegativa);
                tabelaTaxaPerformanceCollection.Query.Where(tabelaTaxaPerformanceCollection.Query.IdCarteira.Equal(idCliente),
                                                            tabelaTaxaPerformanceCollection.Query.DataReferencia.LessThanOrEqual(dataPosicao));
                tabelaTaxaPerformanceCollection.Query.Load();

                bool cobraNegativo = false;
                if (tabelaTaxaPerformanceCollection.Count > 0) {
                    cobraNegativo = tabelaTaxaPerformanceCollection[0].ZeraPerformanceNegativa == "S" ? true : false;
                }

                if (!cobraNegativo) {
                    PosicaoCotistaHistoricoCollection posicaoCotistaHistoricoCollection = new PosicaoCotistaHistoricoCollection();
                    posicaoCotistaHistoricoCollection.Query.Select(posicaoCotistaHistoricoCollection.Query.DataUltimoCortePfee);
                    posicaoCotistaHistoricoCollection.Query.Where(posicaoCotistaHistoricoCollection.Query.IdCarteira.Equal(idCliente),
                                                                  posicaoCotistaHistoricoCollection.Query.Quantidade.NotEqual(0));
                    posicaoCotistaHistoricoCollection.Query.OrderBy(posicaoCotistaHistoricoCollection.Query.DataUltimoCortePfee.Descending);
                    posicaoCotistaHistoricoCollection.Query.Load();

                    if (posicaoCotistaHistoricoCollection.Count > 0 && posicaoCotistaHistoricoCollection[0].DataUltimoCortePfee.HasValue) {
                        dataUltimaCobranca = posicaoCotistaHistoricoCollection[0].DataUltimoCortePfee.Value;
                    }

                    HistoricoCota historicoCotaPerformance = new HistoricoCota();
                    if (historicoCotaPerformance.BuscaValorCota(idCliente, dataPosicao) && historicoCotaPerformance.CotaFechamento.HasValue) {
                        valorCotaUltimaCobranca = historicoCotaPerformance.CotaFechamento.Value;
                    }
                }
                #endregion

                if (tabelaTaxaPerformanceCollection.Count > 0) {
                    p1.ved_regul_cobr_taxa_perform = cobraNegativo ? "N" : "S";

                    #region RESP_VED_REGUL_COBR_TAXA_PERFORM
                    if (dataUltimaCobranca.HasValue && valorCotaUltimaCobranca.HasValue) {
                        Perfil_3.RESP_VED_REGUL_COBR_TAXA_PERFORM r = new Perfil_3.RESP_VED_REGUL_COBR_TAXA_PERFORM();
                        r.DATA_COTA_FUNDO = dataUltimaCobranca.Value;
                        r.VAL_COTA_FUNDO = Math.Round(valorCotaUltimaCobranca.Value, 5);
                        //
                        p1.get_RESP_VED_REGUL_COBR_TAXA_PERFORM = r;
                    }
                    #endregion

                    // Adiciona na lista de Perfil
                    //pLista.Add(p1);
                }
                #endregion

                DateTime dataInicio = Calendario.RetornaPrimeiroDiaUtilMes(dataPosicao);

                Liquidacao l = new Liquidacao();
                l.Query.Select(l.Query.Valor.Sum())
                     .Where(l.Query.IdCliente == idCliente,
                            l.Query.Origem.In((int)OrigemLancamentoLiquidacao.Bolsa.DividendoDistribuicao),
                            l.Query.DataLancamento.Between(dataInicio, dataPosicao));

                l.Query.Load();

                decimal proventos = 0.0M;
                if (l.es.HasData && l.Valor.HasValue) {
                    proventos = Math.Abs(l.Valor.Value);
                }

                p1.montante_distrib = proventos;

                // Adiciona na lista de Perfil
                pLista.Add(p1);
            }

            if (pLista.Count > 0) {
                // Adiciona Perfil na estrutura do arquivo
                // Cria o Objeto Corpo
                arq.CriaCorpo(); // Para não aparecer o nó <PERFIL_MENSAL> se não existir dados
                //
                arq.getCorpo.getListPerfil = pLista;
            }
        }
    }
    #endregion
}