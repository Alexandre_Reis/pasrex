﻿using System;
using System.IO;
using Financial.Fundo;
using Financial.Investidor;
using Financial.Investidor.Enums;
using Financial.CRM;
using EntitySpaces.Interfaces;
using System.Collections.Generic;
using Financial.Util;
using Financial.InvestidorCotista;
using Financial.RendaFixa;
using Financial.RendaFixa.Enums;
using Financial.Common;
using Financial.Common.Enums;
using System.Xml.Serialization;
using System.Xml;
using Financial.InvestidorCotista.Enums;
using Financial.Relatorio;

namespace Financial.Export {

    /// <summary>
    ///  
    /// </summary>
    public class TabelaPerfilCVM_PDF {

        /// <summary>
        /// Retorna uma Stream de Mémoria de um arquivo PDF. A Stream permanece aberta
        /// </summary>
        /// <param name="dataPosicao"></param>
        /// <param name="idClientes"></param>
        /// <param name="dicMStream">Saida: Dicionario contendo Nome do arquivo e Memory Stream do arquivo PDF</param>
        /// <returns></returns>
        public void ExportaTabelaPerfilCVM_PDF(DateTime data, List<int> idClientes, out Dictionary<string, MemoryStream> dicMStream) {

            dicMStream = new Dictionary<string, MemoryStream>();

            for (int i = 0; i < idClientes.Count; i++) {
                //
                MemoryStream ms = new MemoryStream();
                string mes = data.Month.ToString();
                if (mes.Length == 1) {
                    mes = "0" + mes;
                }

                string nomeArquivo = idClientes[i] + "_" + mes + data.Year.ToString() + ".pdf";
                //
                #region Gera o PDF
                ReportPerfilMensalExportacao report = new ReportPerfilMensalExportacao(idClientes[i], data);
                report.ExportToPdf(ms);

                /* Necessário voltar o ponteiro do arquivo para o Inicio */
                ms.Seek(0, SeekOrigin.Begin);
                //
                #endregion
                //
                // Adiciona no Dicionário
                dicMStream.Add(nomeArquivo, ms);
            }           

        }

        /// <summary>
        /// Realiza a exportação PDF Versão 3
        /// Retorna uma Stream de Mémoria de um arquivo PDF. A Stream permanece aberta
        /// </summary>
        /// <param name="dataPosicao"></param>
        /// <param name="idClientes"></param>
        /// <param name="dicMStream">Saida: Dicionario contendo Nome do arquivo e Memory Stream do arquivo PDF</param>
        /// <returns></returns>
        public void ExportaTabelaPerfilCVM_PDF_V3(DateTime data, List<int> idClientes, out Dictionary<string, MemoryStream> dicMStream) {

            dicMStream = new Dictionary<string, MemoryStream>();

            for (int i = 0; i < idClientes.Count; i++) {
                //
                MemoryStream ms = new MemoryStream();
                string mes = data.Month.ToString();
                if (mes.Length == 1) {
                    mes = "0" + mes;
                }

                string nomeArquivo = idClientes[i] + "_" + mes + data.Year.ToString() + "V3.pdf";
                //
                #region Gera o PDF
                ReportPerfilMensalExportacaoV3 report = new ReportPerfilMensalExportacaoV3(idClientes[i], data);
                report.ExportToPdf(ms);

                /* Necessário voltar o ponteiro do arquivo para o Inicio */
                ms.Seek(0, SeekOrigin.Begin);
                //
                #endregion
                //
                // Adiciona no Dicionário
                dicMStream.Add(nomeArquivo, ms);
            }
        }     
    }
}