﻿using System.Xml.Serialization;
using System.Xml;
using System;
using System.IO;
using Financial.Util;
using Financial.Investidor;
using Financial.Investidor.Enums;
using Financial.CRM;
using Financial.Fundo;
using Financial.ContaCorrente;
using Financial.Fundo.Enums;
using System.Collections.Generic;
using Financial.RendaFixa;
using Financial.RendaFixa.Enums;
using Financial.Common.Enums;
using Financial.BMF;
using Financial.BMF.Enums;
using Financial.Swap;
using Financial.Swap.Enums;
using Financial.Bolsa;
using EntitySpaces.Interfaces;
using Financial.Bolsa.Enums;
using Financial.Common;
using System.Collections;

namespace Financial.Export 
{
    public class CDA  //CDA 3.0 
    {
        int diasConfidencialidade = 0;

        public enum TipoAtivoCDA
        {
            AcaoOrdinaria = 1, //TipoMercado = VIS, Acao ON
            AcaoPreferencial = 2, //TipoMercado = VIS, Acao PN, PNA, PNB...
            OpcaoCompra = 3, //TipoMercado = OPC
            OpcaoVenda = 4, //TipoMercado = OPV
            DebentureSimples = 6, //Papel = Debenture (hoje será tudo listado como debentures simples)
            DebentureConversivel = 7, //Papel = Debenture (hoje será tudo listado como debentures simples)
            ContratoFuturo = 9, //Futuros BMF
            CertificadoDepositoAcoes = 11, //TipoPapelAtivo = CertificadoDepositoAcoes
            ReciboDepositoAcoes = 12, //TipoPapelAtivo = ReciboDepositoAcoes
            AcaoReciboAcaoMercosul = 13, //TRATADO COMO AÇÃO
            BonusSubscricao = 14, //TipoPapelAtivo = BonusSubscricao
            NotaPromissoria_CommercialPaper_Exportnote = 15, //****************RENDA FIXA PRIVADA, VERIFICAR!!!!
            CertificadoInvestimentoAudiovisual = 17, //****************RENDA FIXA PRIVADA, VERIFICAR!!!!
            CertificadoRecebivelImobiliario = 18, //****************RENDA FIXA PRIVADA, VERIFICAR!!!!
            Titulo_ContratoInvestimentoColetivo = 19, //****************RENDA FIXA PRIVADA, VERIFICAR!!!!
            DepositoryReceiptExterior = 20, //TRATADO COMO AÇÃO
            ReciboSubscricao = 21, //TipoPapelAtivo = ReciboSubscricao
            BonusPrivado = 23, //TRATADO COMO AÇÃO
            CertificadoTermoEnergiaEletrica = 24, //****************RENDA FIXA PRIVADA, VERIFICAR!!!!
            CertificadoPrivatizacao = 25, //****************RENDA FIXA PRIVADA, VERIFICAR!!!!
            SWAP = 26, //PosicaoSwap
            TituloDividaExterna = 27, //****************RENDA FIXA PRIVADA, VERIFICAR!!!!
            TituloPublicoFederal = 28, //TipoPapel=Publico e TipoEmissor do papel=federal 
            TituloPublicoEstadual = 29, //TipoPapel=Publico e TipoEmissor do papel=estadual
            TituloPublicoMunicipal = 30, //TipoPapel=Publico e TipoEmissor do papel=municipal
            CPR = 31,//****************RENDA FIXA PRIVADA, VERIFICAR!!!!
            NCA = 32,//****************RENDA FIXA PRIVADA, VERIFICAR!!!!
            CDA = 33,//****************RENDA FIXA PRIVADA, VERIFICAR!!!!
            CDCA = 34,//****************RENDA FIXA PRIVADA, VERIFICAR!!!!
            LCA = 35,//****************RENDA FIXA PRIVADA, VERIFICAR!!!!
            CRA = 36,//****************RENDA FIXA PRIVADA, VERIFICAR!!!!
            WA = 37,//****************RENDA FIXA PRIVADA, VERIFICAR!!!!
            CCB = 38,//****************RENDA FIXA PRIVADA, VERIFICAR!!!!
            CCCB = 39,//****************RENDA FIXA PRIVADA, VERIFICAR!!!!
            FundoInvestimento = 40,
            FMIEmpresasEmergentes = 41,
            FDIC = 42,
            FIParticipações = 43,
            FIImobiliario = 44, //TipoMercado = IMO
            OutrosTiposFundos = 45,
            OutrosInstrumentosDerivativos = 46, //Futuros BMF
            BDRNivelI = 47, //TipoPapelAtivo = BDRNivelI
            BDRNivelII = 48, //TipoPapelAtivo = BDRNivelII
            BDRNivelIII = 49, //TipoPapelAtivo = BDRNivelII
            LetraCambio_LetraHipotecaria_LetraImobiliaria = 50,//****************RENDA FIXA PRIVADA, VERIFICAR!!!!
            CDB_RDB = 51,//****************RENDA FIXA PRIVADA, VERIFICAR!!!!
            Futuro_ALA = 52, //Futuros BMF
            Futuro_B19 = 53, //Futuros BMF
            Futuro_B34 = 54, //Futuros BMF
            Futuro_B40 = 55, //Futuros BMF
            Futuro_BGI = 56, //Futuros BMF
            Futuro_BRI = 57, //Futuros BMF
            Futuro_BZE = 58, //Futuros BMF
            Futuro_CNI = 59, //Futuros BMF
            Futuro_COT = 60, //Futuros BMF
            Futuro_DAP = 61, //Futuros BMF
            Futuro_DDI = 62, //Futuros BMF
            Futuro_DI1 = 63, //Futuros BMF
            Futuro_DOL = 64, //Futuros BMF
            Futuro_EUR = 65, //Futuros BMF
            Futuro_IAP = 66, //Futuros BMF
            Futuro_ICF = 67, //Futuros BMF
            Futuro_IND = 68, //Futuros BMF
            Futuro_ISU = 69, //Futuros BMF
            Futuro_OZ1 = 70, //Futuros BMF
            Futuro_SOJ = 71, //Futuros BMF
            Futuro_T10 = 72, //Futuros BMF
            Futuro_WDL = 73, //Futuros BMF
            Futuro_WIN = 74, //Futuros BMF
            CCI = 75,//****************RENDA FIXA PRIVADA, VERIFICAR!!!!
            LetraFinanceira = 76,//****************RENDA FIXA PRIVADA, VERIFICAR!!!!
            CDBClausula = 77,//****************RENDA FIXA PRIVADA, VERIFICAR!!!!
            DPGE = 78,//****************RENDA FIXA PRIVADA, VERIFICAR!!!!
            Outros = 999
        }

        public enum TipoAplicacaoCDA
        {
            Acoes = 37,
            OpcoesPosicaoTitular = 39,
            OpcoesPosicaoLancada = 40,
            ComprasTermo = 42,
            VendasTermo = 43,
            OutrasAplicacoes = 48,
            DepositoPrazo_OutrosTitulosIF = 74,
            TitulosPublicos = 75,
            Disponibilidades = 94,
            ValoresPagar = 96,
            ValoresReceber = 97,
            MercadoFuturoComprado = 102,
            MercadoFuturoVendido = 103,
            TVMCedidoEmprestimo = 143,
            TVMRecebidoEmprestimo = 146,
            OperacoesCompromissadas = 147,
            Certificado_ReciboDeposito = 151,
            OutrosValoresMobiliarios_OfertaPublica = 152,
            DiferencialSwapReceber = 183,
            DiferencialSwapPagar = 184,
            DisponivelOuro = 187,
            InvestimentoExterior = 189,
            ObrigacoesCompraTermoPagar = 191,
            ObrigacoesVendaTermoEntregar = 192,
            Debentures = 193,
            TitulosAgronegocio = 195,
            TitulosCreditoPrivado = 196,
            CotasFundos = 197,
            BDR = 198,
            OutrosValoresOfertadosPrivado = 199,
            OutrasOperacoesPassivas_Exibilidades = 200,
        }

        public enum TipoNegociacaoCDA
        {
            Negociacao = 1,
            MantidoVencimento = 2
        }

        public static class TipoRentabilidadeCDA
        {
            public const string TAXA_ANBID = "ANB";
            public const string CDI = "DI1";
            public const string DOLAR = "DOL";
            public const string IBRX50 = "I50";
            public const string IPCA = "IAP";
            public const string IBRX = "IBX";
            public const string IEE = "IEE";
            public const string IGPDI = "IGD";
            public const string IGPM = "IGM";
            public const string IGP = "IGP";
            public const string IMA = "IMA";
            public const string IMAB = "IMA-B";
            public const string IMAB5 = "IMA-B5";
            public const string IMAB5Mais = "IMA-B5+";
            public const string IMAC = "IMA-C";
            public const string IMAC5 = "IMA-C5";
            public const string IMAC5Mais = "IMA-C5+";
            public const string IMAS = "IMA-S";
            public const string IBOVESPA = "IND";
            public const string INPC = "INP";
            public const string IPC = "IPC";
            public const string IRFM = "IRF";
            public const string ITEL = "ITE";
            public const string IENE = "JPY";
            public const string OUTROS = "OUT";
            public const string OURO = "OZ1";
            public const string COTAS_PIBB = "PIB";
            public const string PRE_FIXADO = "PRE";
            public const string EURO = "REU";
            public const string CARTEIRA_aCOES1 = "SB1";
            public const string CARTEIRA_aCOES2 = "SB2";
            public const string SELIC = "SEL";
            public const string TBF = "TBF";
            public const string TJLP = "TJL";
            public const string TR = "TR";
        }

        private enum PaisCDA
        {
            AFRICA_DO_SUL = 1,            
            ALBANIA = 109,
            ALEMANHA = 2,
            ANDORRA = 131,
            ANGOLA = 92,
	        ANGUILLA = 84,
            ANTIGUA_E_BARBUDA=3,	
            ANTILHAS_HOLANDESAS=4,
            ARABIA_SAUDITA=82,
            ARGELIA=95,
            ARGENTINA=5,
            AUSTRALIA=6,
            AUSTRIA=7,
            BAHAMAS=8,
            BAHRAIN=89,
            BANGLADESH=110,
            BARBADOS=9,
            BELGICA=10,
            BELIZE=104,
            BERMUDAS=11,
            BOLIVIA=12,
            BOSNIA=119,
            BOTSWANA=101,
            BRASIL=102,
            BRUNEI=129,
            BULGARIA=90,
            CABO_VERDE=103,
            CANADA=13,
            CASAQUISTAO=112,
            CATAR=140,
            CHILE=14,
            CHINA=15,
            CHIPRE=16,
            CINGAPURA=68,
            COLOMBIA=18,
            COREIA_DO_NORTE=19,
            COREIA_DO_SUL=20,
            COSTA_DO_MARFIM=21,
            COSTA_RICA=22,
            CROACIA=97,
            CUBA=93,
            DINAMARCA=23,
            EGITO=24,
            EL_SALVADOR=111,
            EMIRADOS_ARABES_UNIDOS=25,
            EQUADOR=26,
            ESLOVAQUIA=137,
            ESLOVENIA=106,
            ESPANHA=27,
            ESTADOS_UNIDOS=28,
            ESTONIA=134,
            FILIPINAS=30,
            FINLANDIA=29,
            FRANCA=31,
            GANA=121,
            GIBRALTAR=88,
            GRECIA=94,
            GUATEMALA=32,
            GUERNSEY=33,
            HONDURAS=34,
            HONG_KONG=35,
            HUNGRIA=36,
            ILHA_DE_MAN=37,
            ILHAS_CAYMAN=38,
            ILHAS_COOK=83,
            ILHAS_MARIANAS_DO_NORTE=127,
            ILHAS_MARSHALL=133,
            ILHAS_MAURICIO=51,
            ILHAS_VIRGENS_AMERICANAS=126,
            ILHAS_VIRGENS_BRITANICAS=39,
            INDIA=40,
            INDONESIA=41,
            IRLANDA=42,
            ISLANDIA=125,
            ISRAEL=43,
            ITALIA=44,
            JAMAICA=45,
            JAPAO=46,
            JERSEY=86,
            JORDANIA=47,
            KUWAIT=81,
            LETONIA=114,
            LIBANO=128,
            LIBERIA=138,
            LIBIA=98,
            LIECHTENSTEIN=80,
            LITUANIA=108,
            LUXEMBURGO=48,
            MALASIA=49,
            MALTA=50,
            MARROCOS=107,
            MEXICO=52,
            MONACO=17,
            NAMIBIA=132,
            NICARAGUA=99,
            NIGÉRIA=53,
            NORUEGA=54,
            NOVA_ZELANDIA=55,
            OMAN=56,
            PAISES_BAIXOS=57,
            PANAMA=58,
            PAPUA_NOVA_GUINE=115,
            PAQUISTAO=59,
            PARAGUAI=60,
            PERU=61,
            POLONIA=62,
            PORTO_RICO=87,
            PORTUGAL=63,
            QUENIA=64,
            QUIRGUISTAO=113,
            REINO_UNIDO=65,
            REPUBLICA_DA_MACEDONIA=66,
            REPUBLICA_DOMINICANA=67,
            REPUBLICA_TCHECA=96,
            ROMENIA=100,
            RUSSIA=91,
            SAINT_KITTS_AND_NEVIS=130,
            SAINT_VINCENT_AND_THE_GRENADINES=118,
            SEYCHELLES=141,
            SRI_LANKA=69,
            SUDAO=139,
            SUECIA=70,
            SUICA=71,
            TAILANDIA=72,
            TAIWAN=73,
            TANZANIA=116,
            TRINIDAD_E_TOGABO=74,
            TUNISIA=75,
            TURKS_CAICOS=85,
            TURQUIA=76,
            UCRANIA=105,
            UGANDA=123,
            URUGUA=77,
            UZBEQUISTAO=117,
            VENEZUELA=78,
            ZAMBIA=79
        }

        public static class CodigoBolsaCDA
        {
            public const string Outras_bolsas_ou_mercados_de_balcaoorganizados = "99";
            public const string AMERICAN_STOCK_EXCHANGE	= "A1";
            public const string INTERNATIONAL_SECURITIES_EXCHANGE_ISE = "A10";	
            public const string NASDAQ_STOCK_MARKET = "A11";
            public const string NYSE_EURONEXT = "A12";
            public const string TSX_GROUP = "A13";
            public const string BERMUDA_STOCK_EXCHANGE = "A2";
            public const string BOLSA_DE_COMERCIO_DE_BUENOS_AIRES = "A3";
            public const string BOLSA_DE_COMERCIO_DE_SANTIAGO = "A4";
            public const string BOLSA_DE_VALORES_DE_COLOMBIA = "A5";
            public const string BOLSA_DE_VALORES_DE_LIMA = "A6";
            public const string BOLSA_MEXICANA_DE_VALORES = "A7";
            public const string BOURSE_DE_MONTREAL = "A8";
            public const string CHICAGO_BOARD_OPTIONS_EXCHANGE = "A9";
            public const string AUSTRALIAN_SECURITIES_EXCHANGE = "C1";
            public const string OSAKA_SECURITIES_EXCHANGE = "C10";
            public const string PHILIPPINE_STOCK_EXCHANGE = "C11";
            public const string SHANGHAI_STOCK_EXCHANGE = "C12";
            public const string SHENZHEN_STOCK_EXCHANGE = "C13";
            public const string SINGAPORE_EXCHANGE = "C14";
            public const string STOCK_EXCHANGE_OF_TEHRAN = "C15";
            public const string STOCK_EXCHANGE_OF_THAILAND = "C16";
            public const string TAIWAN_STOCK_EXCHANGE_CORP = "C17";
            public const string TOKYO_STOCK_EXCHANGE = "C18";
            public const string BOMBAY_STOCK_EXCHANGE_LTD = "C2";
            public const string BURSA_MALAYSIA = "C3";
            public const string COLOMBO_STOCK_EXCHANGE = "C4";
            public const string HONG_KONG_EXCHANGES_AND_CLEARING = "C5";
            public const string JAKARTA_STOCK_EXCHANGE = "C6";
            public const string KOREA_EXCHANGE = "C7";
            public const string NATIONAL_STOCK_EXCHANGE_OF_INDIA_LIMITED = "C8";
            public const string NEW_ZEALAND_EXCHANGE_LTD = "C9";
            public const string ATHENS_EXCHANGE = "E1";
            public const string IRISH_STOCK_EXCHANGE = "E10";
            public const string ISTANBUL_STOCK_EXCHANGE = "E11";
            public const string JSE_LIMITED = "E12";
            public const string LJUBLJANA_STOCK_EXCHANGE = "E13";
            public const string LONDON_STOCK_EXCHANGE = "E14";
            public const string MALTA_STOCK_EXCHANGE = "E15";
            public const string OMX = "E16";
            public const string OSLO_BORS = "E17";
            public const string STOCK_EXCHANGE_OF_MAURITIUS = "E18";
            public const string SWX_SWISS_EXCHANGE = "E19";
            public const string BME_SPANISH_EXCHANGES = "E2";
            public const string TEL_AVIV_STOCK_EXCHANGE = "E20";
            public const string WARSAW_STOCK_EXCHANGE = "E21";
            public const string WIENER_BORSE_AG = "E22";
            public const string BORSA_ITALIANA_SPA = "E3";
            public const string BOURSE_DE_LUXEMBOURG = "E4";
            public const string BUDAPEST_STOCK_EXCHANGE_LTD = "E5";
            public const string CAIRO_ALEXANDRIA_STOCK_EXCHANGES = "E6";
            public const string CYPRUS_STOCK_EXCHANGE = "E7";
            public const string DEUTSCHE_BORSE_AG = "E8";
            public const string EURONEXT = "E9";
        }
                
        private string RetornaTipoRentabilidadeCDA(short idIndice)
        {            
            string tipo = TipoRentabilidadeCDA.OUTROS;

            switch (idIndice)
            {
                case (short)ListaIndiceFixo.ANBID:
                    tipo = TipoRentabilidadeCDA.TAXA_ANBID;
                    break;
                case (short)ListaIndiceFixo.CDI:
                    tipo = TipoRentabilidadeCDA.CDI;
                    break;
                case (short)ListaIndiceFixo.EURO:
                    tipo = TipoRentabilidadeCDA.EURO;
                    break;
                case (short)ListaIndiceFixo.IBOVESPA_FECHA:
                case (short)ListaIndiceFixo.IBOVESPA_MEDIO:
                    tipo = TipoRentabilidadeCDA.IBOVESPA;
                    break;
                case (short)ListaIndiceFixo.IBRX_FECHA:
                case (short)ListaIndiceFixo.IBRX_MEDIO:
                    tipo = TipoRentabilidadeCDA.IBRX;
                    break;
                case (short)ListaIndiceFixo.IBRX50_FECHA:
                case (short)ListaIndiceFixo.IBRX50_MEDIO:
                    tipo = TipoRentabilidadeCDA.IBRX50;
                    break;
                case (short)ListaIndiceFixo.IGPDI:
                    tipo = TipoRentabilidadeCDA.IGPDI;
                    break;
                case (short)ListaIndiceFixo.IGPM:
                    tipo = TipoRentabilidadeCDA.IGPM;
                    break;
                case (short)ListaIndiceFixo.IMA_B:
                    tipo = TipoRentabilidadeCDA.IMAB;
                    break;
                case (short)ListaIndiceFixo.IMA_C:
                    tipo = TipoRentabilidadeCDA.IMAC;
                    break;
                case (short)ListaIndiceFixo.IMA_S:
                    tipo = TipoRentabilidadeCDA.IMAS;
                    break;
                case (short)ListaIndiceFixo.INPC:
                    tipo = TipoRentabilidadeCDA.INPC;
                    break;
                case (short)ListaIndiceFixo.IPCA:
                    tipo = TipoRentabilidadeCDA.IPCA;
                    break;
                case (short)ListaIndiceFixo.IRFM:
                    tipo = TipoRentabilidadeCDA.IRFM;
                    break;
                case (short)ListaIndiceFixo.OUROBMF_FECHA:
                case (short)ListaIndiceFixo.OUROBMF_MEDIO:
                    tipo = TipoRentabilidadeCDA.OURO;
                    break;
                case (short)ListaIndiceFixo.PTAX_800VENDA:
                    tipo = TipoRentabilidadeCDA.DOLAR;
                    break;
                case (short)ListaIndiceFixo.SELIC:
                    tipo = TipoRentabilidadeCDA.SELIC;
                    break;
                case (short)ListaIndiceFixo.TJLP:
                    tipo = TipoRentabilidadeCDA.TJLP;
                    break;
                case (short)ListaIndiceFixo.TR:
                    tipo = TipoRentabilidadeCDA.TR;
                    break;
                case (short)ListaIndiceFixo.YENE:
                    tipo = TipoRentabilidadeCDA.IENE;
                    break;
                default:
                    break;
            }

            return tipo;
        }

        private string RetornaAtivoFuturoCDA(AtivoBMF ativoBMF)
        {
            string ativo = "";

            if (ativoBMF.TipoMercado == (byte)TipoMercadoBMF.Futuro)
            {
                ativo = ativoBMF.CdAtivoBMF + "FUT" + ativoBMF.Serie;
            }
            else if (ativoBMF.TipoMercado == (byte)TipoMercadoBMF.OpcaoDisponivel)
            {
                ativo = ativoBMF.CdAtivoBMF + "OPD" + ativoBMF.Serie;
            }
            else if (ativoBMF.TipoMercado == (byte)TipoMercadoBMF.OpcaoFuturo)
            {
                ativo = ativoBMF.CdAtivoBMF + "OPF" + ativoBMF.Serie;
            }
            else if (ativoBMF.TipoMercado == (byte)TipoMercadoBMF.Disponivel)
            {
                ativo = ativoBMF.CdAtivoBMF + "DIS" + ativoBMF.Serie;
            }
            else if (ativoBMF.TipoMercado == (byte)TipoMercadoBMF.Termo)
            {
                ativo = ativoBMF.CdAtivoBMF + "TER" + ativoBMF.Serie;
            }

            return ativo;
        }

        private string RetornaCodigoFuturoCDA(AtivoBMF ativoBMF)
        {
            if (ativoBMF.TipoMercado.Value != (byte)TipoMercadoBMF.Futuro)
            {
                return Convert.ToString((int)TipoAtivoCDA.OutrosInstrumentosDerivativos);
            }
            else if (ativoBMF.CdAtivoBMF == "ALA")
            {
                return Convert.ToString((int)TipoAtivoCDA.Futuro_ALA);
            }
            else if (ativoBMF.CdAtivoBMF == "B19")
            {
                return Convert.ToString((int)TipoAtivoCDA.Futuro_B19);
            }
            else if (ativoBMF.CdAtivoBMF == "B34")
            {
                return Convert.ToString((int)TipoAtivoCDA.Futuro_B34);
            }
            else if (ativoBMF.CdAtivoBMF == "B40")
            {
                return Convert.ToString((int)TipoAtivoCDA.Futuro_B40);
            }
            else if (ativoBMF.CdAtivoBMF == "BGI")
            {
                return Convert.ToString((int)TipoAtivoCDA.Futuro_BGI);
            }
            else if (ativoBMF.CdAtivoBMF == "BRI")
            {
                return Convert.ToString((int)TipoAtivoCDA.Futuro_BRI);
            }
            else if (ativoBMF.CdAtivoBMF == "BZE")
            {
                return Convert.ToString((int)TipoAtivoCDA.Futuro_BZE);
            }
            else if (ativoBMF.CdAtivoBMF == "CNI")
            {
                return Convert.ToString((int)TipoAtivoCDA.Futuro_CNI);
            }
            else if (ativoBMF.CdAtivoBMF == "COT")
            {
                return Convert.ToString((int)TipoAtivoCDA.Futuro_COT);
            }
            else if (ativoBMF.CdAtivoBMF == "DAP")
            {
                return Convert.ToString((int)TipoAtivoCDA.Futuro_DAP);
            }
            else if (ativoBMF.CdAtivoBMF == "DDI")
            {
                return Convert.ToString((int)TipoAtivoCDA.Futuro_DDI);
            }
            else if (ativoBMF.CdAtivoBMF == "DOL")
            {
                return Convert.ToString((int)TipoAtivoCDA.Futuro_DOL);
            }
            else if (ativoBMF.CdAtivoBMF == "EUR")
            {
                return Convert.ToString((int)TipoAtivoCDA.Futuro_EUR);
            }
            else if (ativoBMF.CdAtivoBMF == "IAP")
            {
                return Convert.ToString((int)TipoAtivoCDA.Futuro_IAP);
            }
            else if (ativoBMF.CdAtivoBMF == "ICF")
            {
                return Convert.ToString((int)TipoAtivoCDA.Futuro_ICF);
            }
            else if (ativoBMF.CdAtivoBMF == "IND")
            {
                return Convert.ToString((int)TipoAtivoCDA.Futuro_IND);
            }
            else if (ativoBMF.CdAtivoBMF == "ISU")
            {
                return Convert.ToString((int)TipoAtivoCDA.Futuro_ISU);
            }
            else if (ativoBMF.CdAtivoBMF == "OZ1")
            {
                return Convert.ToString((int)TipoAtivoCDA.Futuro_OZ1);
            }
            else if (ativoBMF.CdAtivoBMF == "SOJ")
            {
                return Convert.ToString((int)TipoAtivoCDA.Futuro_SOJ);
            }
            else if (ativoBMF.CdAtivoBMF == "T10")
            {
                return Convert.ToString((int)TipoAtivoCDA.Futuro_T10);
            }
            else if (ativoBMF.CdAtivoBMF == "WDL")
            {
                return Convert.ToString((int)TipoAtivoCDA.Futuro_WDL);
            }
            else if (ativoBMF.CdAtivoBMF == "WIN")
            {
                return Convert.ToString((int)TipoAtivoCDA.Futuro_WIN);
            }
            else if (ativoBMF.CdAtivoBMF == "DI1")
            {
                return Convert.ToString((int)TipoAtivoCDA.Futuro_DI1);
            }
            else
            {
                return "";
            }
        }

        private string RetornaCodigoSwap(PosicaoSwapHistorico posicaoSwapHistorico)
        {
            #region Ponta Pre
            if (posicaoSwapHistorico.TipoPonta.Value == (byte)TipoPontaSwap.PreFixado)
            {
                if (posicaoSwapHistorico.IdIndiceContraParte.Value == (short)ListaIndiceFixo.CDI)
                {
                    return "SDP";
                }
                else if (posicaoSwapHistorico.IdIndiceContraParte.Value == (short)ListaIndiceFixo.EURO)
                {
                    return "SEP";
                }
                else if (posicaoSwapHistorico.IdIndiceContraParte.Value == (short)ListaIndiceFixo.IBOVESPA_FECHA)
                {
                    return "SNP";
                }
                else if (posicaoSwapHistorico.IdIndiceContraParte.Value == (short)ListaIndiceFixo.IGPDI)
                {
                    return "SIP";
                }
                else if (posicaoSwapHistorico.IdIndiceContraParte.Value == (short)ListaIndiceFixo.IGPM)
                {
                    return "SMP";
                }
                else if (posicaoSwapHistorico.IdIndiceContraParte.Value == (short)ListaIndiceFixo.INPC)
                {
                    return "SXP";
                }
                else if (posicaoSwapHistorico.IdIndiceContraParte.Value == (short)ListaIndiceFixo.IPCA)
                {
                    return "SLP";
                }
                else if (posicaoSwapHistorico.IdIndiceContraParte.Value == (short)ListaIndiceFixo.PTAX_800VENDA)
                {
                    return "SCP";
                }
                else if (posicaoSwapHistorico.IdIndiceContraParte.Value == (short)ListaIndiceFixo.SELIC)
                {
                    return "SSP";
                }
                else if (posicaoSwapHistorico.IdIndiceContraParte.Value == (short)ListaIndiceFixo.TR)
                {
                    return "STP";
                }
            }
            #endregion
            #region Ponta CDI
            else if (posicaoSwapHistorico.TipoPonta.Value != (byte)TipoPontaSwap.PreFixado &&
                     posicaoSwapHistorico.IdIndice.Value == (short)ListaIndiceFixo.CDI)
            {
                if (posicaoSwapHistorico.TipoPonta.Value == (byte)TipoPontaSwap.PreFixado)
                {
                    return "SDP";
                }
                else if (posicaoSwapHistorico.IdIndiceContraParte.Value == (short)ListaIndiceFixo.EURO)
                {
                    return "SDE";
                }
                else if (posicaoSwapHistorico.IdIndiceContraParte.Value == (short)ListaIndiceFixo.IBOVESPA_FECHA)
                {
                    return "SDN";
                }
                else if (posicaoSwapHistorico.IdIndiceContraParte.Value == (short)ListaIndiceFixo.IGPDI)
                {
                    return "SDI";
                }
                else if (posicaoSwapHistorico.IdIndiceContraParte.Value == (short)ListaIndiceFixo.IGPM)
                {
                    return "SDM";
                }
                else if (posicaoSwapHistorico.IdIndiceContraParte.Value == (short)ListaIndiceFixo.INPC)
                {
                    return "SDX";
                }
                else if (posicaoSwapHistorico.IdIndiceContraParte.Value == (short)ListaIndiceFixo.IPCA)
                {
                    return "SDL";
                }
                else if (posicaoSwapHistorico.IdIndiceContraParte.Value == (short)ListaIndiceFixo.PTAX_800VENDA)
                {
                    return "SDC";
                }
                else if (posicaoSwapHistorico.IdIndiceContraParte.Value == (short)ListaIndiceFixo.SELIC)
                {
                    return "SDS";
                }
                else if (posicaoSwapHistorico.IdIndiceContraParte.Value == (short)ListaIndiceFixo.TR)
                {
                    return "SDT";
                }
            }
            #endregion
            #region Ponta Euro
            else if (posicaoSwapHistorico.TipoPonta.Value != (byte)TipoPontaSwap.PreFixado &&
                posicaoSwapHistorico.IdIndice.Value == (short)ListaIndiceFixo.EURO)
            {
                if (posicaoSwapHistorico.TipoPonta.Value == (byte)TipoPontaSwap.PreFixado)
                {
                    return "SEP";
                }
                else if (posicaoSwapHistorico.IdIndiceContraParte.Value == (short)ListaIndiceFixo.CDI)
                {
                    return "SDE";
                }
                else if (posicaoSwapHistorico.IdIndiceContraParte.Value == (short)ListaIndiceFixo.IBOVESPA_FECHA)
                {
                    return "SNE";
                }
                else if (posicaoSwapHistorico.IdIndiceContraParte.Value == (short)ListaIndiceFixo.IGPDI)
                {
                    return "SEI";
                }
                else if (posicaoSwapHistorico.IdIndiceContraParte.Value == (short)ListaIndiceFixo.IGPM)
                {
                    return "SME";
                }
                else if (posicaoSwapHistorico.IdIndiceContraParte.Value == (short)ListaIndiceFixo.INPC)
                {
                    return "SXE";
                }
                else if (posicaoSwapHistorico.IdIndiceContraParte.Value == (short)ListaIndiceFixo.IPCA)
                {
                    return "SLE";
                }
                else if (posicaoSwapHistorico.IdIndiceContraParte.Value == (short)ListaIndiceFixo.PTAX_800VENDA)
                {
                    return "SCE";
                }
                else if (posicaoSwapHistorico.IdIndiceContraParte.Value == (short)ListaIndiceFixo.SELIC)
                {
                    return "SSE";
                }
                else if (posicaoSwapHistorico.IdIndiceContraParte.Value == (short)ListaIndiceFixo.TR)
                {
                    return "STE";
                }
            }
            #endregion
            #region Ponta Ibovespa
            else if (posicaoSwapHistorico.TipoPonta.Value != (byte)TipoPontaSwap.PreFixado &&
                posicaoSwapHistorico.IdIndice.Value == (short)ListaIndiceFixo.IBOVESPA_FECHA)
            {
                if (posicaoSwapHistorico.TipoPonta.Value == (byte)TipoPontaSwap.PreFixado)
                {
                    return "SNP";
                }
                else if (posicaoSwapHistorico.IdIndiceContraParte.Value == (short)ListaIndiceFixo.CDI)
                {
                    return "SDN";
                }
                else if (posicaoSwapHistorico.IdIndiceContraParte.Value == (short)ListaIndiceFixo.EURO)
                {
                    return "SNE";
                }
                else if (posicaoSwapHistorico.IdIndiceContraParte.Value == (short)ListaIndiceFixo.IGPDI)
                {
                    return "SNI";
                }
                else if (posicaoSwapHistorico.IdIndiceContraParte.Value == (short)ListaIndiceFixo.IGPM)
                {
                    return "SNM";
                }
                else if (posicaoSwapHistorico.IdIndiceContraParte.Value == (short)ListaIndiceFixo.INPC)
                {
                    return "SNX";
                }
                else if (posicaoSwapHistorico.IdIndiceContraParte.Value == (short)ListaIndiceFixo.IPCA)
                {
                    return "SNL";
                }
                else if (posicaoSwapHistorico.IdIndiceContraParte.Value == (short)ListaIndiceFixo.PTAX_800VENDA)
                {
                    return "SCN";
                }
                else if (posicaoSwapHistorico.IdIndiceContraParte.Value == (short)ListaIndiceFixo.SELIC)
                {
                    return "SNS";
                }
                else if (posicaoSwapHistorico.IdIndiceContraParte.Value == (short)ListaIndiceFixo.TR)
                {
                    return "STN";
                }
            }
            #endregion
            #region Ponta IGPDI
            else if (posicaoSwapHistorico.TipoPonta.Value != (byte)TipoPontaSwap.PreFixado &&
                posicaoSwapHistorico.IdIndice.Value == (short)ListaIndiceFixo.IGPDI)
            {
                if (posicaoSwapHistorico.TipoPonta.Value == (byte)TipoPontaSwap.PreFixado)
                {
                    return "SIP";
                }
                else if (posicaoSwapHistorico.IdIndiceContraParte.Value == (short)ListaIndiceFixo.CDI)
                {
                    return "SDI";
                }
                else if (posicaoSwapHistorico.IdIndiceContraParte.Value == (short)ListaIndiceFixo.EURO)
                {
                    return "SEI";
                }
                else if (posicaoSwapHistorico.IdIndiceContraParte.Value == (short)ListaIndiceFixo.IBOVESPA_FECHA)
                {
                    return "SNI";
                }
                else if (posicaoSwapHistorico.IdIndiceContraParte.Value == (short)ListaIndiceFixo.IGPM)
                {
                    return "SMI";
                }
                else if (posicaoSwapHistorico.IdIndiceContraParte.Value == (short)ListaIndiceFixo.INPC)
                {
                    return "SIX";
                }
                else if (posicaoSwapHistorico.IdIndiceContraParte.Value == (short)ListaIndiceFixo.IPCA)
                {
                    return "SIL";
                }
                else if (posicaoSwapHistorico.IdIndiceContraParte.Value == (short)ListaIndiceFixo.PTAX_800VENDA)
                {
                    return "SCI";
                }
                else if (posicaoSwapHistorico.IdIndiceContraParte.Value == (short)ListaIndiceFixo.SELIC)
                {
                    return "SSI";
                }
                else if (posicaoSwapHistorico.IdIndiceContraParte.Value == (short)ListaIndiceFixo.TR)
                {
                    return "STI";
                }
            }
            #endregion
            #region Ponta IGPM
            else if (posicaoSwapHistorico.TipoPonta.Value != (byte)TipoPontaSwap.PreFixado &&
                posicaoSwapHistorico.IdIndice.Value == (short)ListaIndiceFixo.IGPM)
            {
                if (posicaoSwapHistorico.TipoPonta.Value == (byte)TipoPontaSwap.PreFixado)
                {
                    return "SMP";
                }
                else if (posicaoSwapHistorico.IdIndiceContraParte.Value == (short)ListaIndiceFixo.CDI)
                {
                    return "SDM";
                }
                else if (posicaoSwapHistorico.IdIndiceContraParte.Value == (short)ListaIndiceFixo.EURO)
                {
                    return "SME";
                }
                else if (posicaoSwapHistorico.IdIndiceContraParte.Value == (short)ListaIndiceFixo.IBOVESPA_FECHA)
                {
                    return "SNM";
                }
                else if (posicaoSwapHistorico.IdIndiceContraParte.Value == (short)ListaIndiceFixo.IGPDI)
                {
                    return "SMI";
                }
                else if (posicaoSwapHistorico.IdIndiceContraParte.Value == (short)ListaIndiceFixo.INPC)
                {
                    return "SMX";
                }
                else if (posicaoSwapHistorico.IdIndiceContraParte.Value == (short)ListaIndiceFixo.IPCA)
                {
                    return "SML";
                }
                else if (posicaoSwapHistorico.IdIndiceContraParte.Value == (short)ListaIndiceFixo.PTAX_800VENDA)
                {
                    return "SCM";
                }
                else if (posicaoSwapHistorico.IdIndiceContraParte.Value == (short)ListaIndiceFixo.SELIC)
                {
                    return "SSM";
                }
                else if (posicaoSwapHistorico.IdIndiceContraParte.Value == (short)ListaIndiceFixo.TR)
                {
                    return "STM";
                }
            }
            #endregion
            #region Ponta INPC
            else if (posicaoSwapHistorico.TipoPonta.Value != (byte)TipoPontaSwap.PreFixado &&
                posicaoSwapHistorico.IdIndice.Value == (short)ListaIndiceFixo.INPC)
            {
                if (posicaoSwapHistorico.TipoPonta.Value == (byte)TipoPontaSwap.PreFixado)
                {
                    return "SXP";
                }
                else if (posicaoSwapHistorico.IdIndiceContraParte.Value == (short)ListaIndiceFixo.CDI)
                {
                    return "SDX";
                }
                else if (posicaoSwapHistorico.IdIndiceContraParte.Value == (short)ListaIndiceFixo.EURO)
                {
                    return "SXE";
                }
                else if (posicaoSwapHistorico.IdIndiceContraParte.Value == (short)ListaIndiceFixo.IBOVESPA_FECHA)
                {
                    return "SNX";
                }
                else if (posicaoSwapHistorico.IdIndiceContraParte.Value == (short)ListaIndiceFixo.IGPDI)
                {
                    return "SIX";
                }
                else if (posicaoSwapHistorico.IdIndiceContraParte.Value == (short)ListaIndiceFixo.IGPM)
                {
                    return "SMX";
                }
                else if (posicaoSwapHistorico.IdIndiceContraParte.Value == (short)ListaIndiceFixo.IPCA)
                {
                    return "SXL";
                }
                else if (posicaoSwapHistorico.IdIndiceContraParte.Value == (short)ListaIndiceFixo.PTAX_800VENDA)
                {
                    return "SCX";
                }
                else if (posicaoSwapHistorico.IdIndiceContraParte.Value == (short)ListaIndiceFixo.SELIC)
                {
                    return "SSX";
                }
                else if (posicaoSwapHistorico.IdIndiceContraParte.Value == (short)ListaIndiceFixo.TR)
                {
                    return "STX";
                }
            }
            #endregion
            #region Ponta IPCA
            else if (posicaoSwapHistorico.TipoPonta.Value != (byte)TipoPontaSwap.PreFixado &&
                posicaoSwapHistorico.IdIndice.Value == (short)ListaIndiceFixo.IPCA)
            {
                if (posicaoSwapHistorico.TipoPonta.Value == (byte)TipoPontaSwap.PreFixado)
                {
                    return "SLP";
                }
                else if (posicaoSwapHistorico.IdIndiceContraParte.Value == (short)ListaIndiceFixo.CDI)
                {
                    return "SDL";
                }
                else if (posicaoSwapHistorico.IdIndiceContraParte.Value == (short)ListaIndiceFixo.EURO)
                {
                    return "SLE";
                }
                else if (posicaoSwapHistorico.IdIndiceContraParte.Value == (short)ListaIndiceFixo.IBOVESPA_FECHA)
                {
                    return "SNL";
                }
                else if (posicaoSwapHistorico.IdIndiceContraParte.Value == (short)ListaIndiceFixo.IGPDI)
                {
                    return "SIL";
                }
                else if (posicaoSwapHistorico.IdIndiceContraParte.Value == (short)ListaIndiceFixo.IGPM)
                {
                    return "SML";
                }
                else if (posicaoSwapHistorico.IdIndiceContraParte.Value == (short)ListaIndiceFixo.INPC)
                {
                    return "SXL";
                }
                else if (posicaoSwapHistorico.IdIndiceContraParte.Value == (short)ListaIndiceFixo.PTAX_800VENDA)
                {
                    return "SCL";
                }
                else if (posicaoSwapHistorico.IdIndiceContraParte.Value == (short)ListaIndiceFixo.SELIC)
                {
                    return "SSL";
                }
                else if (posicaoSwapHistorico.IdIndiceContraParte.Value == (short)ListaIndiceFixo.TR)
                {
                    return "STL";
                }
            }
            #endregion
            #region Ponta Dolar (PTAX 800 Venda)
            else if (posicaoSwapHistorico.TipoPonta.Value != (byte)TipoPontaSwap.PreFixado &&
                posicaoSwapHistorico.IdIndice.Value == (short)ListaIndiceFixo.PTAX_800VENDA)
            {
                if (posicaoSwapHistorico.TipoPonta.Value == (byte)TipoPontaSwap.PreFixado)
                {
                    return "SCP";
                }
                else if (posicaoSwapHistorico.IdIndiceContraParte.Value == (short)ListaIndiceFixo.CDI)
                {
                    return "SDC";
                }
                else if (posicaoSwapHistorico.IdIndiceContraParte.Value == (short)ListaIndiceFixo.EURO)
                {
                    return "SCE";
                }
                else if (posicaoSwapHistorico.IdIndiceContraParte.Value == (short)ListaIndiceFixo.IBOVESPA_FECHA)
                {
                    return "SCN";
                }
                else if (posicaoSwapHistorico.IdIndiceContraParte.Value == (short)ListaIndiceFixo.IGPDI)
                {
                    return "SCI";
                }
                else if (posicaoSwapHistorico.IdIndiceContraParte.Value == (short)ListaIndiceFixo.IGPM)
                {
                    return "SCM";
                }
                else if (posicaoSwapHistorico.IdIndiceContraParte.Value == (short)ListaIndiceFixo.INPC)
                {
                    return "SCX";
                }
                else if (posicaoSwapHistorico.IdIndiceContraParte.Value == (short)ListaIndiceFixo.IPCA)
                {
                    return "SCL";
                }
                else if (posicaoSwapHistorico.IdIndiceContraParte.Value == (short)ListaIndiceFixo.SELIC)
                {
                    return "SSC";
                }
                else if (posicaoSwapHistorico.IdIndiceContraParte.Value == (short)ListaIndiceFixo.TR)
                {
                    return "SCT";
                }
            }
            #endregion
            #region Ponta Selic
            else if (posicaoSwapHistorico.TipoPonta.Value != (byte)TipoPontaSwap.PreFixado &&
                posicaoSwapHistorico.IdIndice.Value == (short)ListaIndiceFixo.SELIC)
            {
                if (posicaoSwapHistorico.TipoPonta.Value == (byte)TipoPontaSwap.PreFixado)
                {
                    return "SSP";
                }
                else if (posicaoSwapHistorico.IdIndiceContraParte.Value == (short)ListaIndiceFixo.CDI)
                {
                    return "SDS";
                }
                else if (posicaoSwapHistorico.IdIndiceContraParte.Value == (short)ListaIndiceFixo.EURO)
                {
                    return "SSE";
                }
                else if (posicaoSwapHistorico.IdIndiceContraParte.Value == (short)ListaIndiceFixo.IBOVESPA_FECHA)
                {
                    return "SNS";
                }
                else if (posicaoSwapHistorico.IdIndiceContraParte.Value == (short)ListaIndiceFixo.IGPDI)
                {
                    return "SSI";
                }
                else if (posicaoSwapHistorico.IdIndiceContraParte.Value == (short)ListaIndiceFixo.IGPM)
                {
                    return "SSM";
                }
                else if (posicaoSwapHistorico.IdIndiceContraParte.Value == (short)ListaIndiceFixo.INPC)
                {
                    return "SSX";
                }
                else if (posicaoSwapHistorico.IdIndiceContraParte.Value == (short)ListaIndiceFixo.IPCA)
                {
                    return "SSL";
                }
                else if (posicaoSwapHistorico.IdIndiceContraParte.Value == (short)ListaIndiceFixo.PTAX_800VENDA)
                {
                    return "SSC";
                }
                else if (posicaoSwapHistorico.IdIndiceContraParte.Value == (short)ListaIndiceFixo.TR)
                {
                    return "SST";
                }
            }
            #endregion
            #region Ponta TR
            else if (posicaoSwapHistorico.TipoPonta.Value != (byte)TipoPontaSwap.PreFixado &&
                posicaoSwapHistorico.IdIndice.Value == (short)ListaIndiceFixo.TR)
            {
                if (posicaoSwapHistorico.TipoPonta.Value == (byte)TipoPontaSwap.PreFixado)
                {
                    return "STP";
                }
                else if (posicaoSwapHistorico.IdIndiceContraParte.Value == (short)ListaIndiceFixo.CDI)
                {
                    return "SDT";
                }
                else if (posicaoSwapHistorico.IdIndiceContraParte.Value == (short)ListaIndiceFixo.EURO)
                {
                    return "STE";
                }
                else if (posicaoSwapHistorico.IdIndiceContraParte.Value == (short)ListaIndiceFixo.IBOVESPA_FECHA)
                {
                    return "STN";
                }
                else if (posicaoSwapHistorico.IdIndiceContraParte.Value == (short)ListaIndiceFixo.IGPDI)
                {
                    return "STI";
                }
                else if (posicaoSwapHistorico.IdIndiceContraParte.Value == (short)ListaIndiceFixo.IGPM)
                {
                    return "STM";
                }
                else if (posicaoSwapHistorico.IdIndiceContraParte.Value == (short)ListaIndiceFixo.INPC)
                {
                    return "STX";
                }
                else if (posicaoSwapHistorico.IdIndiceContraParte.Value == (short)ListaIndiceFixo.IPCA)
                {
                    return "STL";
                }
                else if (posicaoSwapHistorico.IdIndiceContraParte.Value == (short)ListaIndiceFixo.PTAX_800VENDA)
                {
                    return "SCT";
                }
                else if (posicaoSwapHistorico.IdIndiceContraParte.Value == (short)ListaIndiceFixo.SELIC)
                {
                    return "SST";
                }
            }
            #endregion

            return "";
        }

        /// <summary>
        /// Indica se o emissor da ação é empresa ligada (é o adm ou gestor do fundo - idCarteira passado).
        /// </summary>
        /// <param name="idTitulo"></param>
        /// <param name="idCarteira"></param>
        /// <returns></returns>
        private bool IndicaEmpresaLigadaAcao(string cdAtivoBolsa, int idAgente1, int idAgente2)
        {
            AtivoBolsa ativoBolsa = new AtivoBolsa();
            List<esQueryItem> campos = new List<esQueryItem>();
            campos.Add(ativoBolsa.Query.IdEmissor);
            ativoBolsa.LoadByPrimaryKey(campos, cdAtivoBolsa);

            Emissor emissor = new Emissor();
            campos = new List<esQueryItem>();
            campos.Add(emissor.Query.IdAgente);
            emissor.LoadByPrimaryKey(campos, ativoBolsa.IdEmissor.Value);

            bool empresaLigada = emissor.IdAgente.HasValue && (emissor.IdAgente.Value == idAgente1 ||
                                                               emissor.IdAgente.Value == idAgente2);

            return empresaLigada;
        }

        /// <summary>
        /// Indica se o emissor do título é empresa ligada (é o adm ou gestor do fundo - idCarteira passado).
        /// </summary>
        /// <param name="idTitulo"></param>
        /// <param name="idCarteira"></param>
        /// <returns></returns>
        private bool IndicaEmpresaLigadaTitulo(int idEmissor, int idAgente1, int idAgente2)
        {
            Emissor emissor = new Emissor();
            List<esQueryItem> campos = new List<esQueryItem>();
            campos.Add(emissor.Query.IdAgente);
            emissor.LoadByPrimaryKey(campos, idEmissor);

            bool empresaLigada = emissor.IdAgente.HasValue && (emissor.IdAgente.Value == idAgente1 ||
                                                               emissor.IdAgente.Value == idAgente2);

            return empresaLigada;
        }

        /// <summary>
        /// Indica se o emissor do título é empresa ligada (é o adm ou gestor do fundo - idCarteira passado).
        /// </summary>
        /// <param name="idTitulo"></param>
        /// <param name="idCarteira"></param>
        /// <returns></returns>
        private bool IndicaEmpresaLigadaFundo(int idFundo, int idAgente1, int idAgente2)
        {
            Carteira carteira = new Carteira();
            List<esQueryItem> campos = new List<esQueryItem>();
            campos.Add(carteira.Query.IdAgenteAdministrador);
            campos.Add(carteira.Query.IdAgenteGestor);
            carteira.LoadByPrimaryKey(campos, idFundo);
            
            bool empresaLigada = (carteira.IdAgenteAdministrador.Value == idAgente1 ||
                                  carteira.IdAgenteGestor == idAgente1 ||
                                  carteira.IdAgenteAdministrador.Value == idAgente2 ||
                                  carteira.IdAgenteGestor.Value == idAgente2);

            return empresaLigada;
        }

        /// <summary>
        /// Retorna uma Stream de Mémoria de um arquivo xml. A Stream permanece aberta
        /// </summary>
        /// <param name="dataPosicao"></param>
        /// <param name="idClientes"></param>
        /// <param name="dicMStream">Saida: Dicionario contendo Nome do arquivo e Memory Stream do arquivo CDA</param>
        /// <returns></returns>
        public void ExportaCDA(DateTime dataPosicao, List<int> idClientes, int diasConf, out Dictionary<string, MemoryStream> dicMStream) 
        {
            this.diasConfidencialidade = diasConf;
            Hashtable hsCarteira = new Hashtable();
            PessoaCollection pessoaCollection = new PessoaCollection();
            
            ClienteQuery clienteQuery = new ClienteQuery("C");
            PessoaQuery pessoaQuery = new PessoaQuery("P");
            //
            pessoaQuery.InnerJoin(clienteQuery).On(clienteQuery.IdPessoa == pessoaQuery.IdPessoa);
            //
            pessoaQuery.Where(clienteQuery.TipoControle == (byte)TipoControleCliente.Completo &&
                              clienteQuery.IdTipo == (int)TipoClienteFixo.Fundo && 
                              clienteQuery.StatusAtivo == (byte)StatusAtivoCliente.Ativo &&
                              clienteQuery.IdCliente.In(idClientes)
                             );
            //
            pessoaCollection.Load(pessoaQuery);

            dicMStream = new Dictionary<string, MemoryStream>();

            foreach (Pessoa pessoa in pessoaCollection) 
            {
                MemoryStream ms = new MemoryStream();
                string nomeArquivo = pessoa.Apelido + "_" + dataPosicao.Month.ToString() + dataPosicao.Year.ToString() + ".xml";
                //            
                CDA_Xml arq = new CDA_Xml();
                //
                /* Monta Cabeçalho */
                dataPosicao = Calendario.RetornaUltimoDiaUtilMes(dataPosicao);
                this.MontaCabecalho(dataPosicao, arq);

                /* Busca Id.Carteira */
                int idCarteira = Convert.ToInt32(pessoa.GetColumn(ClienteMetadata.ColumnNames.IdCliente));

                Carteira carteira = new Carteira();
                if (hsCarteira.Contains(idCarteira))
                {
                    carteira = (Carteira)hsCarteira[idCarteira];
                }
                else
                {
                    carteira.LoadByPrimaryKey(idCarteira);
                    hsCarteira.Add(idCarteira, carteira);
                }

                /* Monta Corpo */
                this.MontaCorpo(dataPosicao, pessoa, arq, carteira);

                XmlSerializer x = new XmlSerializer(arq.GetType());

                XmlWriterSettings settings = new XmlWriterSettings();
                settings.Encoding = System.Text.Encoding.GetEncoding("WINDOWS-1252");
                settings.Indent = true;
                settings.IndentChars = "\t";
                settings.NewLineChars = Environment.NewLine;
                settings.ConformanceLevel = ConformanceLevel.Document;

                using (XmlWriter writer = XmlTextWriter.Create(ms, settings)) {
                    x.Serialize(writer, arq);
                }

                /* Necessário voltar o ponteiro do arquivo para o Inicio */
                ms.Seek(0, SeekOrigin.Begin);
                //
                // Adiciona no Dicionário
                dicMStream.Add(nomeArquivo, ms);
            }
        }

        /// <summary>
        /// Monta o Cabeçalho do Arquivo XML
        /// </summary>
        /// <param name="dataPosicao"></param>
        /// <param name="arq">Estrutura do arquivo XML</param>
        /// <returns></returns>
        private void MontaCabecalho(DateTime dataPosicao, CDA_Xml arq) {
            arq.getCabecalho.codigoDocumento = 3;
            arq.getCabecalho.dataCompetencia = dataPosicao;
            arq.getCabecalho.dataGeracaoArquivo = DateTime.Now;
            arq.getCabecalho.versao = "3.0";
        }

        /// <summary>
        /// Monta o Corpo do Arquivo XML 
        /// </summary>
        /// <param name="dataPosicao"></param>
        /// <param name="arq">Estrutura do arquivo XML</param>
        private void MontaCorpo(DateTime dataPosicao, Pessoa pessoa, CDA_Xml arq, Carteira carteira) 
        {
            int idCarteira = carteira.IdCarteira.Value;

            #region Corpo
            arq.getCorpo.getInform.cnpj = pessoa.Cpfcnpj.Trim().Replace("-", "").Replace("/", "").Replace("\\", "").Replace(".", "");

            decimal valorPL = 0;
            HistoricoCota historicoCota = new HistoricoCota();
            historicoCota.BuscaValorPatrimonioDia(idCarteira, dataPosicao);
            if (historicoCota.PLFechamento.HasValue) {
                valorPL = historicoCota.PLFechamento.Value;
            }

            arq.getCorpo.getInform.valorPL = valorPL;
            #endregion

            /* Monta o Objeto Titulo Publico */
            this.MontaTituloPublico(dataPosicao, arq, carteira);

            /* Monta o Objeto Cotas */
            this.MontaCotas(dataPosicao, arq, carteira);

            /* Monta o Objeto Swap */
            this.MontaSwap(dataPosicao, arq, carteira);

            /* Monta o Objeto Demais_Codif */
            this.MontaDemais_Codif(dataPosicao, arq, carteira);

            /* Monta o Objeto Dep_Prazo_Tit_If */
            this.MontaDep_Prazo_Tit(dataPosicao, arq, carteira);

            /* Monta o Objeto Tit_Agro_Cred_Priv */
            this.MontaTituloAgronegocio_CreditoPrivado(dataPosicao, arq, carteira);

            /* Monta o Objeto InvestimentoExterior */
            this.MontaInvestimentoExterior(dataPosicao, arq, carteira);

            /* Monta o Objeto Demais_N_Codif */
            this.MontaDemais_N_Codif(dataPosicao, arq, carteira);
        }

        #region Monta o Node Titulo Publico
        /// <summary>
        /// Monta o Node Titulo Publico 
        /// </summary>
        /// <param name="dataPosicao"></param>
        /// <param name="arq"></param>
        /// <param name="idCarteira"></param>
        private void MontaTituloPublico(DateTime dataPosicao, CDA_Xml arq, Carteira carteira) 
        {
            int idCarteira = carteira.IdCarteira.Value;
            DateTime dataInicioMes = Calendario.RetornaPrimeiroDiaCorridoMes(dataPosicao, 0);

            #region For Monta Titulos publicos (Compras Finais)
            PosicaoRendaFixaHistoricoQuery posicaoRendaFixaHistoricoQuery = new PosicaoRendaFixaHistoricoQuery("P");
            TituloRendaFixaQuery tituloRendaFixaQuery = new TituloRendaFixaQuery("T");
            PapelRendaFixaQuery papelRendaFixaQuery = new PapelRendaFixaQuery("A");
            posicaoRendaFixaHistoricoQuery.Select(posicaoRendaFixaHistoricoQuery.IdTitulo,
                                                  posicaoRendaFixaHistoricoQuery.DataVencimento,
                                                  posicaoRendaFixaHistoricoQuery.TipoNegociacao,
                                                  tituloRendaFixaQuery.CodigoCustodia,
                                                  posicaoRendaFixaHistoricoQuery.Quantidade.Sum(),
                                                  posicaoRendaFixaHistoricoQuery.ValorMercado.Sum());
            posicaoRendaFixaHistoricoQuery.InnerJoin(tituloRendaFixaQuery).On(tituloRendaFixaQuery.IdTitulo == posicaoRendaFixaHistoricoQuery.IdTitulo);
            posicaoRendaFixaHistoricoQuery.InnerJoin(papelRendaFixaQuery).On(papelRendaFixaQuery.IdPapel == tituloRendaFixaQuery.IdPapel);
            posicaoRendaFixaHistoricoQuery.Where(posicaoRendaFixaHistoricoQuery.IdCliente.Equal(idCarteira),
                                        posicaoRendaFixaHistoricoQuery.DataHistorico.Equal(dataPosicao),
                                        posicaoRendaFixaHistoricoQuery.TipoOperacao.Equal((byte)TipoOperacaoTitulo.CompraFinal),
                                        tituloRendaFixaQuery.CodigoCustodia.IsNotNull(),
                                        tituloRendaFixaQuery.CodigoCustodia.NotEqual(""),
                                        papelRendaFixaQuery.TipoPapel.Equal((byte)TipoPapelTitulo.Publico));
            posicaoRendaFixaHistoricoQuery.GroupBy(posicaoRendaFixaHistoricoQuery.IdTitulo,
                                                   posicaoRendaFixaHistoricoQuery.DataVencimento,
                                                   posicaoRendaFixaHistoricoQuery.TipoNegociacao,
                                                   tituloRendaFixaQuery.CodigoCustodia);

            PosicaoRendaFixaHistoricoCollection coll = new PosicaoRendaFixaHistoricoCollection();
            coll.Load(posicaoRendaFixaHistoricoQuery);

            foreach (PosicaoRendaFixaHistorico p in coll)
            {
                string codigoCustodia = "";
                if (p.GetColumn(TituloRendaFixaMetadata.ColumnNames.CodigoCustodia) != null)
                {
                    codigoCustodia = Convert.ToString(p.GetColumn(TituloRendaFixaMetadata.ColumnNames.CodigoCustodia));
                }

                DateTime dataVencimento = Convert.ToDateTime(p.GetColumn(TituloRendaFixaMetadata.ColumnNames.DataVencimento));
                byte tipoNegociacao = p.TipoNegociacao.Value;

                decimal quantidade = p.Quantidade.Value;
                decimal valor = p.ValorMercado.Value;

                decimal quantidadeAplicacao = 0;
                decimal valorAplicacao = 0;

                decimal quantidadeResgate = 0;
                decimal valorResgate = 0;

                if (tipoNegociacao != (byte)TipoNegociacaoTitulo.Vencimento) //Vencimento desconsidera de forma sumária qq movimentação, já que por definição não pode operar
                {
                    OperacaoRendaFixa operacaoRendaFixa = new OperacaoRendaFixa();
                    operacaoRendaFixa.Query.Select(operacaoRendaFixa.Query.Quantidade.Sum(),
                                                   operacaoRendaFixa.Query.Valor.Sum());
                    operacaoRendaFixa.Query.Where(operacaoRendaFixa.Query.IdCliente.Equal(idCarteira),
                                                  operacaoRendaFixa.Query.IdTitulo.Equal(p.IdTitulo.Value),
                                                  operacaoRendaFixa.Query.DataOperacao.GreaterThanOrEqual(dataInicioMes),
                                                  operacaoRendaFixa.Query.DataOperacao.LessThanOrEqual(dataPosicao),
                                                  operacaoRendaFixa.Query.TipoOperacao.In((byte)TipoOperacaoTitulo.CompraRevenda));
                    operacaoRendaFixa.Query.Load();

                    if (operacaoRendaFixa.Quantidade.HasValue)
                    {
                        quantidadeAplicacao = operacaoRendaFixa.Quantidade.Value;
                        valorAplicacao = operacaoRendaFixa.Valor.Value;
                    }


                    operacaoRendaFixa = new OperacaoRendaFixa();
                    operacaoRendaFixa.Query.Select(operacaoRendaFixa.Query.Quantidade.Sum(),
                                                   operacaoRendaFixa.Query.Valor.Sum());
                    operacaoRendaFixa.Query.Where(operacaoRendaFixa.Query.IdCliente.Equal(idCarteira),
                                                  operacaoRendaFixa.Query.IdTitulo.Equal(p.IdTitulo.Value),
                                                  operacaoRendaFixa.Query.DataOperacao.GreaterThanOrEqual(dataInicioMes),
                                                  operacaoRendaFixa.Query.DataOperacao.LessThanOrEqual(dataPosicao),
                                                  operacaoRendaFixa.Query.TipoOperacao.In((byte)TipoOperacaoTitulo.VendaFinal,
                                                                                          (byte)TipoOperacaoTitulo.VendaTotal));
                    operacaoRendaFixa.Query.Load();

                    if (operacaoRendaFixa.Quantidade.HasValue)
                    {
                        quantidadeResgate = operacaoRendaFixa.Quantidade.Value;
                        valorResgate = operacaoRendaFixa.Valor.Value;
                    }
                }

                TituloPublico t1 = new TituloPublico();

                t1.codigoSelic = codigoCustodia;
                t1.dataVencimento = dataVencimento;

                // TituloPublico - Aplic
                t1.getAplic.cod_Tp_Aplic = Convert.ToString((int)TipoAplicacaoCDA.TitulosPublicos);
                t1.getAplic.qtde_Dias_Confid = this.diasConfidencialidade.ToString();

                // Aplic - Negoc
                t1.getAplic.getAplicNegoc.tipoNegocio = p.TipoNegociacao.Value == (byte)TipoNegociacaoTitulo.Vencimento ?
                                                Convert.ToString((int)TipoNegociacaoCDA.MantidoVencimento) : Convert.ToString((int)TipoNegociacaoCDA.Negociacao);
                t1.getAplic.getAplicNegoc.quantidadeVendasNegocio = quantidadeResgate;
                t1.getAplic.getAplicNegoc.valorVendasNegocio = valorResgate;
                t1.getAplic.getAplicNegoc.quantidadeAquisicaoNegocio = quantidadeAplicacao;
                t1.getAplic.getAplicNegoc.valorAquisicaoNegocio = valorAplicacao;

                // Aplic - Pos_Fim
                t1.getAplic.aplic_Pos_Fim.VersaoCDA("3");
                t1.getAplic.getAplicPosFim.qtde_Pos_Fim = quantidade;
                t1.getAplic.getAplicPosFim.merc_Pos_Fim = valor;

                // Adiciona TituloPublico
                arq.getCorpo.getInform.getListAtiv.getTituloPublico.Add(t1);                
            }
            #endregion

            #region For Monta Titulos publicos (Compras Compromissadas)
            posicaoRendaFixaHistoricoQuery = new PosicaoRendaFixaHistoricoQuery("P");
            tituloRendaFixaQuery = new TituloRendaFixaQuery("T");
            papelRendaFixaQuery = new PapelRendaFixaQuery("A");
            posicaoRendaFixaHistoricoQuery.Select(posicaoRendaFixaHistoricoQuery.IdTitulo,
                                                  posicaoRendaFixaHistoricoQuery.TipoNegociacao,
                                                  tituloRendaFixaQuery.DataVencimento,
                                                  tituloRendaFixaQuery.CodigoCustodia,
                                                  posicaoRendaFixaHistoricoQuery.Quantidade.Sum(),
                                                  posicaoRendaFixaHistoricoQuery.ValorMercado.Sum());
            posicaoRendaFixaHistoricoQuery.InnerJoin(tituloRendaFixaQuery).On(tituloRendaFixaQuery.IdTitulo == posicaoRendaFixaHistoricoQuery.IdTitulo);
            posicaoRendaFixaHistoricoQuery.InnerJoin(papelRendaFixaQuery).On(papelRendaFixaQuery.IdPapel == tituloRendaFixaQuery.IdPapel);
            posicaoRendaFixaHistoricoQuery.Where(posicaoRendaFixaHistoricoQuery.IdCliente.Equal(idCarteira),
                                        posicaoRendaFixaHistoricoQuery.DataHistorico.Equal(dataPosicao),
                                        posicaoRendaFixaHistoricoQuery.TipoOperacao.Equal((byte)TipoOperacaoTitulo.CompraRevenda),
                                        tituloRendaFixaQuery.CodigoCustodia.IsNotNull(),
                                        tituloRendaFixaQuery.CodigoCustodia.NotEqual(""),
                                        papelRendaFixaQuery.TipoPapel.Equal((byte)TipoPapelTitulo.Publico));
            posicaoRendaFixaHistoricoQuery.GroupBy(posicaoRendaFixaHistoricoQuery.IdTitulo,                                                   
                                                   posicaoRendaFixaHistoricoQuery.TipoNegociacao,
                                                   tituloRendaFixaQuery.DataVencimento,
                                                   tituloRendaFixaQuery.CodigoCustodia);

            coll = new PosicaoRendaFixaHistoricoCollection();
            coll.Load(posicaoRendaFixaHistoricoQuery);

            foreach (PosicaoRendaFixaHistorico p in coll)
            {
                string codigoCustodia = "";
                if (p.GetColumn(TituloRendaFixaMetadata.ColumnNames.CodigoCustodia) != null)
                {
                    codigoCustodia = Convert.ToString(p.GetColumn(TituloRendaFixaMetadata.ColumnNames.CodigoCustodia));
                }

                DateTime dataVencimento = Convert.ToDateTime(p.GetColumn(TituloRendaFixaMetadata.ColumnNames.DataVencimento));
                
                decimal quantidade = p.Quantidade.Value;
                decimal valor = p.ValorMercado.Value;

                TituloPublico t1 = new TituloPublico();

                t1.codigoSelic = codigoCustodia;
                t1.dataVencimento = dataVencimento;

                // TituloPublico - Aplic
                t1.getAplic.cod_Tp_Aplic = Convert.ToString((int)TipoAplicacaoCDA.OperacoesCompromissadas);                
                t1.getAplic.empr_Ligada = "N";
                t1.getAplic.qtde_Dias_Confid = this.diasConfidencialidade.ToString();

                // Aplic - Negoc
                t1.getAplic.getAplicNegoc.tipoNegocio = Convert.ToString((int)TipoNegociacaoCDA.Negociacao);                

                // Aplic - Pos_Fim
                t1.getAplic.aplic_Pos_Fim.VersaoCDA("3");
                t1.getAplic.getAplicPosFim.qtde_Pos_Fim = quantidade;
                t1.getAplic.getAplicPosFim.merc_Pos_Fim = valor;

                // Adiciona TituloPublico
                arq.getCorpo.getInform.getListAtiv.getTituloPublico.Add(t1);
            }
            #endregion

        } 
        #endregion

        #region Monta o Node Cotas
        /// <summary>
        /// Monta o Node Cotas
        /// </summary>
        /// <param name="dataPosicao"></param>
        /// <param name="arq"></param>
        /// <param name="idCarteira"></param>
        private void MontaCotas(DateTime dataPosicao, CDA_Xml arq, Carteira carteira) 
        {
            int idCarteira = carteira.IdCarteira.Value;
            DateTime dataInicioMes = Calendario.RetornaPrimeiroDiaCorridoMes(dataPosicao, 0);

            #region For Monta Objeto Cotas
            CarteiraQuery carteiraQuery = new CarteiraQuery("C");
            ClienteQuery clienteQuery = new ClienteQuery("L");
            PosicaoFundoHistoricoQuery posicaoFundoHistoricoQuery = new PosicaoFundoHistoricoQuery("P");
            
            PosicaoFundoHistoricoCollection coll = new PosicaoFundoHistoricoCollection();
            posicaoFundoHistoricoQuery.Select(posicaoFundoHistoricoQuery.IdCarteira,
                                              posicaoFundoHistoricoQuery.Quantidade.Sum(),
                                              posicaoFundoHistoricoQuery.ValorAplicacao.Sum(),
                                              posicaoFundoHistoricoQuery.ValorBruto.Sum());
            posicaoFundoHistoricoQuery.InnerJoin(carteiraQuery).On(carteiraQuery.IdCarteira == posicaoFundoHistoricoQuery.IdCarteira);
            posicaoFundoHistoricoQuery.InnerJoin(clienteQuery).On(clienteQuery.IdCliente == carteiraQuery.IdCarteira);
            posicaoFundoHistoricoQuery.Where(posicaoFundoHistoricoQuery.IdCliente.Equal(idCarteira),
                                             posicaoFundoHistoricoQuery.DataHistorico.Equal(dataPosicao),
                                             posicaoFundoHistoricoQuery.Quantidade.NotEqual(0),
                                             clienteQuery.IdMoeda.Equal((int)ListaMoedaFixo.Real));
            posicaoFundoHistoricoQuery.GroupBy(posicaoFundoHistoricoQuery.IdCarteira);
            coll.Load(posicaoFundoHistoricoQuery);

            foreach (PosicaoFundoHistorico p in coll)
            {
                decimal quantidade = p.Quantidade.Value;
                decimal valor = p.ValorBruto.Value;
                decimal custoCorrecPosFim = p.ValorAplicacao.Value;

                Pessoa pessoaFundo = new Pessoa();
                Cliente cliente = new Cliente();
                cliente.LoadByPrimaryKey(p.IdCarteira.Value);
                pessoaFundo.LoadByPrimaryKey(cliente.IdPessoa.Value);

                string cnpj = "";
                if (!String.IsNullOrEmpty(pessoaFundo.Cpfcnpj)) 
                {
                    cnpj = Utilitario.RemoveCaracteresEspeciais(pessoaFundo.Cpfcnpj);
                }

                OperacaoFundo operacaoFundo = new OperacaoFundo();
                operacaoFundo.Query.Select(operacaoFundo.Query.Quantidade.Sum(),
                                           operacaoFundo.Query.ValorBruto.Sum());
                operacaoFundo.Query.Where(operacaoFundo.Query.IdCliente.Equal(idCarteira),
                                          operacaoFundo.Query.IdCarteira.Equal(p.IdCarteira.Value),
                                          operacaoFundo.Query.DataOperacao.GreaterThanOrEqual(dataInicioMes),
                                          operacaoFundo.Query.DataOperacao.LessThanOrEqual(dataPosicao),
                                          operacaoFundo.Query.TipoOperacao.Equal((byte)TipoOperacaoFundo.Aplicacao));
                operacaoFundo.Query.Load();

                decimal quantidadeAplicacao = 0;
                decimal valorAplicacao = 0;
                if (operacaoFundo.Quantidade.HasValue) {
                    quantidadeAplicacao = operacaoFundo.Quantidade.Value;
                    valorAplicacao = operacaoFundo.ValorBruto.Value;
                }

                operacaoFundo = new OperacaoFundo();
                operacaoFundo.Query.Select(operacaoFundo.Query.Quantidade.Sum(),
                                           operacaoFundo.Query.ValorBruto.Sum());
                operacaoFundo.Query.Where(operacaoFundo.Query.IdCliente.Equal(idCarteira),
                                          operacaoFundo.Query.IdCarteira.Equal(p.IdCarteira.Value),
                                          operacaoFundo.Query.DataOperacao.GreaterThanOrEqual(dataInicioMes),
                                          operacaoFundo.Query.DataOperacao.LessThanOrEqual(dataPosicao),
                                          operacaoFundo.Query.TipoOperacao.NotEqual((byte)TipoOperacaoFundo.Aplicacao));
                operacaoFundo.Query.Load();

                decimal quantidadeResgate = 0;
                decimal valorResgate = 0;
                if (operacaoFundo.Quantidade.HasValue) {
                    quantidadeResgate = operacaoFundo.Quantidade.Value;
                    valorResgate = operacaoFundo.ValorBruto.Value;
                }

                bool empresaLigada = this.IndicaEmpresaLigadaFundo(p.IdCarteira.Value, carteira.IdAgenteAdministrador.Value, carteira.IdAgenteGestor.Value);

                Cotas c1 = new Cotas();
                c1.cnpj = cnpj;
                // Cotas - Aplic                
                c1.getAplic.empr_Ligada = empresaLigada ? "S" : "N";
                c1.getAplic.qtde_Dias_Confid = this.diasConfidencialidade.ToString();
                // Aplic - Pos_Fim
                c1.getAplic.aplic_Pos_Fim.VersaoCDA("3");
                c1.getAplic.getAplicPosFim.qtde_Pos_Fim = quantidade;
                c1.getAplic.getAplicPosFim.custo_Correc_Pos_Fim = custoCorrecPosFim;
                c1.getAplic.getAplicPosFim.merc_Pos_Fim = valor;

                c1.getAplic.getAplicNegoc.SerializaTipoNegocio(false);
                c1.getAplic.getAplicNegoc.tipoNegocio = Convert.ToString((int)TipoNegociacaoCDA.Negociacao); 
                c1.getAplic.getAplicNegoc.quantidadeAquisicaoNegocio = quantidadeAplicacao;
                c1.getAplic.getAplicNegoc.valorAquisicaoNegocio = valorAplicacao;
                c1.getAplic.getAplicNegoc.quantidadeVendasNegocio = quantidadeResgate;
                c1.getAplic.getAplicNegoc.valorVendasNegocio = valorResgate;                

                // Adiciona Cota
                arq.getCorpo.getInform.getListAtiv.getCotas.Add(c1);
            }
            #endregion
        } 
        #endregion

        #region Monta o Node Swap
        /// <summary>
        /// Monta o Node Swap
        /// </summary>
        /// <param name="dataPosicao"></param>
        /// <param name="arq"></param>
        /// <param name="idCarteira"></param>
        private void MontaSwap(DateTime dataPosicao, CDA_Xml arq, Carteira carteira)
        {
            int idCarteira = carteira.IdCarteira.Value;

            Swap s1 = new Swap();

            PosicaoSwapHistoricoCollection posicaoSwapHistoricoCollection = new PosicaoSwapHistoricoCollection();
            posicaoSwapHistoricoCollection.Query.Where(posicaoSwapHistoricoCollection.Query.IdCliente.Equal(idCarteira),
                                                       posicaoSwapHistoricoCollection.Query.DataHistorico.Equal(dataPosicao),
                                                       posicaoSwapHistoricoCollection.Query.Saldo.NotEqual(0));
            posicaoSwapHistoricoCollection.Query.Load();

            foreach (PosicaoSwapHistorico posicaoSwapHistorico in posicaoSwapHistoricoCollection)
            {
                string codigoSwap = this.RetornaCodigoSwap(posicaoSwapHistorico);
                decimal saldo = posicaoSwapHistorico.Saldo.Value;

                s1.codigoSwap = codigoSwap;
                string codigoTipoAplicacao = saldo > 0 ? Convert.ToString((int)TipoAplicacaoCDA.DiferencialSwapReceber) : Convert.ToString((int)TipoAplicacaoCDA.DiferencialSwapPagar);

                // Swap - Aplic
                s1.getAplic.cod_Tp_Aplic = codigoTipoAplicacao;
                s1.getAplic.qtde_Dias_Confid = this.diasConfidencialidade.ToString();

                // Aplic - Pos_Fim            
                s1.getAplic.aplic_Pos_Fim.VersaoCDA("3");
                s1.getAplic.getAplicPosFim.merc_Pos_Fim = saldo;

                // Adiciona Swap
                arq.getCorpo.getInform.getListAtiv.getSwap.Add(s1);
            }
            
        } 
        #endregion

        #region Monta o Node Demais_Codif
        /// <summary>
        /// Monta o Node Demais_Codif
        /// </summary>
        /// <param name="dataPosicao"></param>
        /// <param name="arq"></param>
        /// <param name="idCarteira"></param>
        private void MontaDemais_Codif(DateTime dataPosicao, CDA_Xml arq, Carteira carteira)
        {
            int idCarteira = carteira.IdCarteira.Value;
            DateTime dataInicioMes = Calendario.RetornaPrimeiroDiaCorridoMes(dataPosicao, 0);

            DateTime dataIndefinida = new DateTime(4000, 01, 01);

            #region Acoes
            AtivoBolsaQuery ativoBolsaQuery = new AtivoBolsaQuery("A");
            PosicaoBolsaHistoricoQuery posicaoBolsaHistoricoQuery = new PosicaoBolsaHistoricoQuery("P");

            PosicaoBolsaHistoricoCollection posicaoBolsaHistoricoCollection = new PosicaoBolsaHistoricoCollection();
            posicaoBolsaHistoricoQuery.Select(posicaoBolsaHistoricoQuery.CdAtivoBolsa,
                                             posicaoBolsaHistoricoQuery.Quantidade.Sum(),
                                             posicaoBolsaHistoricoQuery.ValorMercado.Sum());
            posicaoBolsaHistoricoQuery.InnerJoin(ativoBolsaQuery).On(ativoBolsaQuery.CdAtivoBolsa == posicaoBolsaHistoricoQuery.CdAtivoBolsa);
            posicaoBolsaHistoricoQuery.Where(posicaoBolsaHistoricoQuery.IdCliente.Equal(idCarteira),
                                            posicaoBolsaHistoricoQuery.DataHistorico.Equal(dataPosicao),
                                            posicaoBolsaHistoricoQuery.TipoMercado.Equal(TipoMercadoBolsa.MercadoVista),
                                            posicaoBolsaHistoricoQuery.Quantidade.GreaterThan(0),
                                            ativoBolsaQuery.DataInicioVigencia.IsNotNull(),
                                            ativoBolsaQuery.DataFimVigencia.IsNull() || ativoBolsaQuery.DataFimVigencia.GreaterThanOrEqual(dataPosicao),
                                            ativoBolsaQuery.DataInicioVigencia.NotEqual(dataIndefinida));
            posicaoBolsaHistoricoQuery.GroupBy(posicaoBolsaHistoricoQuery.CdAtivoBolsa);
            posicaoBolsaHistoricoQuery.OrderBy(posicaoBolsaHistoricoQuery.CdAtivoBolsa.Ascending);
            posicaoBolsaHistoricoCollection.Load(posicaoBolsaHistoricoQuery);
            
            foreach (PosicaoBolsaHistorico posicaoBolsaHistorico in posicaoBolsaHistoricoCollection)
            {
                string cdAtivoBolsa = posicaoBolsaHistorico.CdAtivoBolsa;

                decimal quantidadeFinal = posicaoBolsaHistorico.Quantidade.HasValue ? posicaoBolsaHistorico.Quantidade.Value : 0;
                decimal valorFinal = posicaoBolsaHistorico.ValorMercado.HasValue ? posicaoBolsaHistorico.ValorMercado.Value : 0;

                AtivoBolsa ativoBolsa = new AtivoBolsa();
                List<esQueryItem> campos = new List<esQueryItem>();
                campos.Add(ativoBolsa.Query.TipoPapel);
                campos.Add(ativoBolsa.Query.CodigoCDA);
                campos.Add(ativoBolsa.Query.DataInicioVigencia);
                ativoBolsa.LoadByPrimaryKey(campos, cdAtivoBolsa);

                string codigoAtivo = "";
                string tipoAplicacao = "";
                #region Checa codigo de ativo
                if (ativoBolsa.TipoPapel.Value == (byte)TipoPapelAtivo.Normal)
                {
                    codigoAtivo = Utilitario.Right(cdAtivoBolsa, 1) == "3" ? Convert.ToString((int)TipoAtivoCDA.AcaoOrdinaria) : Convert.ToString((int)TipoAtivoCDA.AcaoPreferencial);
                    tipoAplicacao = Convert.ToString((int)TipoAplicacaoCDA.Acoes);
                }
                else if (ativoBolsa.TipoPapel.Value == (byte)TipoPapelAtivo.BDRNivelI)
                {
                    codigoAtivo = Convert.ToString((int)TipoAtivoCDA.BDRNivelI);
                    tipoAplicacao = Convert.ToString((int)TipoAplicacaoCDA.BDR);
                }
                else if (ativoBolsa.TipoPapel.Value == (byte)TipoPapelAtivo.BDRNivelII)
                {
                    codigoAtivo = Convert.ToString((int)TipoAtivoCDA.BDRNivelII);
                    tipoAplicacao = Convert.ToString((int)TipoAplicacaoCDA.BDR);
                }
                else if (ativoBolsa.TipoPapel.Value == (byte)TipoPapelAtivo.BDRNivelIII)
                {
                    codigoAtivo = Convert.ToString((int)TipoAtivoCDA.BDRNivelIII);
                    tipoAplicacao = Convert.ToString((int)TipoAplicacaoCDA.BDR);
                }
                else if (ativoBolsa.TipoPapel.Value == (byte)TipoPapelAtivo.BonusSubscricao)
                {
                    codigoAtivo = Convert.ToString((int)TipoAtivoCDA.BonusSubscricao);
                    tipoAplicacao = Convert.ToString((int)TipoAplicacaoCDA.OutrosValoresMobiliarios_OfertaPublica);
                }
                else if (ativoBolsa.TipoPapel.Value == (byte)TipoPapelAtivo.CertificadoDepositoAcoes)
                {
                    codigoAtivo = Convert.ToString((int)TipoAtivoCDA.CertificadoDepositoAcoes);
                    tipoAplicacao = Convert.ToString((int)TipoAplicacaoCDA.Certificado_ReciboDeposito);
                }
                else if (ativoBolsa.TipoPapel.Value == (byte)TipoPapelAtivo.ETF)
                {
                    codigoAtivo = Convert.ToString((int)TipoAtivoCDA.AcaoPreferencial); //Fundos de indice são jogados, por ora, em Ação
                    tipoAplicacao = Convert.ToString((int)TipoAplicacaoCDA.Acoes);
                }
                else if (ativoBolsa.TipoPapel.Value == (byte)TipoPapelAtivo.ReciboDepositoAcoes)
                {
                    codigoAtivo = Convert.ToString((int)TipoAtivoCDA.ReciboDepositoAcoes);
                    tipoAplicacao = Convert.ToString((int)TipoAplicacaoCDA.Certificado_ReciboDeposito);
                }
                else if (ativoBolsa.TipoPapel.Value == (byte)TipoPapelAtivo.ReciboSubscricao)
                {
                    codigoAtivo = Convert.ToString((int)TipoAtivoCDA.ReciboSubscricao);
                    tipoAplicacao = Convert.ToString((int)TipoAplicacaoCDA.OutrosValoresMobiliarios_OfertaPublica);
                }
                #endregion

                if (ativoBolsa.CodigoCDA.HasValue) //Se tiver o codigo CDA força pelo codigo informado
                {
                    codigoAtivo = Convert.ToString(ativoBolsa.CodigoCDA);
                }

                OperacaoBolsa operacaoBolsaCompra = new OperacaoBolsa();
                operacaoBolsaCompra.Query.Select(operacaoBolsaCompra.Query.Quantidade.Sum(),
                                                 operacaoBolsaCompra.Query.Valor.Sum());
                operacaoBolsaCompra.Query.Where(operacaoBolsaCompra.Query.IdCliente.Equal(idCarteira),
                                                operacaoBolsaCompra.Query.CdAtivoBolsa.Equal(cdAtivoBolsa),    
                                                operacaoBolsaCompra.Query.Data.GreaterThanOrEqual(dataInicioMes),
                                                operacaoBolsaCompra.Query.Data.LessThanOrEqual(dataPosicao),
                                                operacaoBolsaCompra.Query.TipoOperacao.In(TipoOperacaoBolsa.Compra, TipoOperacaoBolsa.CompraDaytrade));                                                                    
                operacaoBolsaCompra.Query.Load();

                decimal quantidadeCompra = operacaoBolsaCompra.Quantidade.HasValue ? operacaoBolsaCompra.Quantidade.Value : 0;
                decimal valorCompra = operacaoBolsaCompra.Valor.HasValue ? operacaoBolsaCompra.Valor.Value : 0;

                OperacaoBolsa operacaoBolsaVenda = new OperacaoBolsa();
                operacaoBolsaVenda.Query.Select(operacaoBolsaVenda.Query.Quantidade.Sum(),
                                                operacaoBolsaVenda.Query.Valor.Sum());
                operacaoBolsaVenda.Query.Where(operacaoBolsaVenda.Query.IdCliente.Equal(idCarteira),
                                               operacaoBolsaVenda.Query.CdAtivoBolsa.Equal(cdAtivoBolsa),
                                               operacaoBolsaVenda.Query.Data.GreaterThanOrEqual(dataInicioMes),
                                               operacaoBolsaVenda.Query.Data.LessThanOrEqual(dataPosicao),
                                               operacaoBolsaVenda.Query.TipoOperacao.In(TipoOperacaoBolsa.Venda, TipoOperacaoBolsa.VendaDaytrade));
                operacaoBolsaVenda.Query.Load();

                decimal quantidadeVenda = operacaoBolsaVenda.Quantidade.HasValue ? operacaoBolsaVenda.Quantidade.Value : 0;
                decimal valorVenda = operacaoBolsaVenda.Valor.HasValue ? operacaoBolsaVenda.Valor.Value : 0;
                
                Demais_Codif d1 = new Demais_Codif();

                d1.codigoAtivo = cdAtivoBolsa;
                d1.cod_Tp_Ativ = codigoAtivo;
                d1.dataInicioVigencia = ativoBolsa.DataInicioVigencia.HasValue? ativoBolsa.DataInicioVigencia.Value: new DateTime(4000, 01, 01);

                bool empresaLigada = this.IndicaEmpresaLigadaAcao(cdAtivoBolsa, carteira.IdAgenteAdministrador.Value, carteira.IdAgenteGestor.Value);
                // Demais_Codif - Aplic
                d1.getAplic.cod_Tp_Aplic = tipoAplicacao;
                d1.getAplic.empr_Ligada = empresaLigada? "S": "N";
                d1.getAplic.qtde_Dias_Confid = this.diasConfidencialidade.ToString();

                // Aplic - Negoc
                d1.getAplic.getAplicNegoc.tipoNegocio = Convert.ToString((int)TipoNegociacaoCDA.Negociacao);
                d1.getAplic.getAplicNegoc.quantidadeVendasNegocio = quantidadeVenda;
                d1.getAplic.getAplicNegoc.valorVendasNegocio = valorVenda;
                d1.getAplic.getAplicNegoc.quantidadeAquisicaoNegocio = quantidadeCompra;
                d1.getAplic.getAplicNegoc.valorAquisicaoNegocio = valorCompra;

                // Aplic - Pos_Fim
                d1.getAplic.aplic_Pos_Fim.VersaoCDA("3");
                d1.getAplic.getAplicPosFim.qtde_Pos_Fim = quantidadeFinal;
                d1.getAplic.getAplicPosFim.merc_Pos_Fim = valorFinal;

                // Adiciona Demais_Codif
                arq.getCorpo.getInform.getListAtiv.getDemais_Codif.Add(d1);
            }
            #endregion

            #region Opções Compradas
            ativoBolsaQuery = new AtivoBolsaQuery("A");
            posicaoBolsaHistoricoQuery = new PosicaoBolsaHistoricoQuery("P");

            posicaoBolsaHistoricoCollection = new PosicaoBolsaHistoricoCollection();
            posicaoBolsaHistoricoQuery.Select(posicaoBolsaHistoricoQuery.TipoMercado,
                                             posicaoBolsaHistoricoQuery.CdAtivoBolsa,
                                             posicaoBolsaHistoricoQuery.Quantidade.Sum(),
                                             posicaoBolsaHistoricoQuery.ValorCustoLiquido.Sum(),
                                             posicaoBolsaHistoricoQuery.ValorMercado.Sum());
            posicaoBolsaHistoricoQuery.InnerJoin(ativoBolsaQuery).On(ativoBolsaQuery.CdAtivoBolsa == posicaoBolsaHistoricoQuery.CdAtivoBolsa);
            posicaoBolsaHistoricoQuery.Where(posicaoBolsaHistoricoQuery.IdCliente.Equal(idCarteira),
                                            posicaoBolsaHistoricoQuery.DataHistorico.Equal(dataPosicao),
                                            posicaoBolsaHistoricoQuery.TipoMercado.In(TipoMercadoBolsa.OpcaoCompra, TipoMercadoBolsa.OpcaoVenda),
                                            posicaoBolsaHistoricoQuery.Quantidade.GreaterThan(0),
                                            ativoBolsaQuery.DataInicioVigencia.IsNotNull(),
                                            ativoBolsaQuery.DataFimVigencia.IsNull() || ativoBolsaQuery.DataFimVigencia.GreaterThanOrEqual(dataPosicao),
                                            ativoBolsaQuery.DataInicioVigencia.NotEqual(dataIndefinida));
            posicaoBolsaHistoricoQuery.GroupBy(posicaoBolsaHistoricoQuery.TipoMercado,
                                               posicaoBolsaHistoricoQuery.CdAtivoBolsa);
            posicaoBolsaHistoricoQuery.OrderBy(posicaoBolsaHistoricoQuery.TipoMercado.Ascending,
                                               posicaoBolsaHistoricoQuery.CdAtivoBolsa.Ascending);
            posicaoBolsaHistoricoCollection.Load(posicaoBolsaHistoricoQuery);

            foreach (PosicaoBolsaHistorico posicaoBolsaHistorico in posicaoBolsaHistoricoCollection)
            {
                string cdAtivoBolsa = posicaoBolsaHistorico.CdAtivoBolsa;
                string tipoMercado = posicaoBolsaHistorico.TipoMercado;

                decimal quantidadeFinal = posicaoBolsaHistorico.Quantidade.HasValue ? posicaoBolsaHistorico.Quantidade.Value : 0;
                decimal valorFinal = posicaoBolsaHistorico.ValorMercado.HasValue ? posicaoBolsaHistorico.ValorMercado.Value : 0;
                decimal custoCorrecPosFim = posicaoBolsaHistorico.ValorCustoLiquido.HasValue ? posicaoBolsaHistorico.ValorCustoLiquido.Value : 0;

                AtivoBolsa ativoBolsa = new AtivoBolsa();
                List<esQueryItem> campos = new List<esQueryItem>();
                campos.Add(ativoBolsa.Query.DataInicioVigencia);
                campos.Add(ativoBolsa.Query.DataFimVigencia);
                ativoBolsa.LoadByPrimaryKey(campos, cdAtivoBolsa);

                if (ativoBolsa.DataFimVigencia.HasValue && ativoBolsa.DataFimVigencia.Value < dataPosicao)
                    continue;

                string tipoAplicacao = Convert.ToString((int)TipoAplicacaoCDA.OpcoesPosicaoTitular);

                string codigoAtivo = "";
                if (tipoMercado == TipoMercadoBolsa.OpcaoCompra)
                {
                    codigoAtivo = Convert.ToString((int)TipoAtivoCDA.OpcaoCompra);
                }
                else
                {
                    codigoAtivo = Convert.ToString((int)TipoAtivoCDA.OpcaoVenda);
                }

                OperacaoBolsa operacaoBolsaCompra = new OperacaoBolsa();
                operacaoBolsaCompra.Query.Select(operacaoBolsaCompra.Query.Quantidade.Sum(),
                                                 operacaoBolsaCompra.Query.Valor.Sum());
                operacaoBolsaCompra.Query.Where(operacaoBolsaCompra.Query.IdCliente.Equal(idCarteira),
                                                operacaoBolsaCompra.Query.CdAtivoBolsa.Equal(cdAtivoBolsa),
                                                operacaoBolsaCompra.Query.Data.GreaterThanOrEqual(dataInicioMes),
                                                operacaoBolsaCompra.Query.Data.LessThanOrEqual(dataPosicao),
                                                operacaoBolsaCompra.Query.TipoOperacao.In(TipoOperacaoBolsa.Compra, TipoOperacaoBolsa.CompraDaytrade));
                operacaoBolsaCompra.Query.Load();

                decimal quantidadeCompra = operacaoBolsaCompra.Quantidade.HasValue ? operacaoBolsaCompra.Quantidade.Value : 0;
                decimal valorCompra = operacaoBolsaCompra.Valor.HasValue ? operacaoBolsaCompra.Valor.Value : 0;

                OperacaoBolsa operacaoBolsaVenda = new OperacaoBolsa();
                operacaoBolsaVenda.Query.Select(operacaoBolsaVenda.Query.Quantidade.Sum(),
                                                operacaoBolsaVenda.Query.Valor.Sum());
                operacaoBolsaVenda.Query.Where(operacaoBolsaVenda.Query.IdCliente.Equal(idCarteira),
                                               operacaoBolsaVenda.Query.CdAtivoBolsa.Equal(cdAtivoBolsa), 
                                               operacaoBolsaVenda.Query.Data.GreaterThanOrEqual(dataInicioMes),
                                               operacaoBolsaVenda.Query.Data.LessThanOrEqual(dataPosicao),
                                               operacaoBolsaVenda.Query.TipoOperacao.In(TipoOperacaoBolsa.Venda, TipoOperacaoBolsa.VendaDaytrade));
                operacaoBolsaVenda.Query.Load();

                decimal quantidadeVenda = operacaoBolsaVenda.Quantidade.HasValue ? operacaoBolsaVenda.Quantidade.Value : 0;
                decimal valorVenda = operacaoBolsaVenda.Valor.HasValue ? operacaoBolsaVenda.Valor.Value : 0;
                
                Demais_Codif d1 = new Demais_Codif();

                d1.codigoAtivo = cdAtivoBolsa;
                d1.cod_Tp_Ativ = codigoAtivo;
                d1.dataInicioVigencia = ativoBolsa.DataInicioVigencia.HasValue ? ativoBolsa.DataInicioVigencia.Value : new DateTime(4000, 01, 01);

                // Demais_Codif - Aplic
                d1.getAplic.cod_Tp_Aplic = tipoAplicacao;
                d1.getAplic.empr_Ligada = "N";
                d1.getAplic.qtde_Dias_Confid = this.diasConfidencialidade.ToString();

                // Aplic - Negoc
                d1.getAplic.getAplicNegoc.tipoNegocio = Convert.ToString((int)TipoNegociacaoCDA.Negociacao);
                d1.getAplic.getAplicNegoc.quantidadeVendasNegocio = quantidadeVenda;
                d1.getAplic.getAplicNegoc.valorVendasNegocio = valorVenda;
                d1.getAplic.getAplicNegoc.quantidadeAquisicaoNegocio = quantidadeCompra;
                d1.getAplic.getAplicNegoc.valorAquisicaoNegocio = valorCompra;

                // Aplic - Pos_Fim
                d1.getAplic.aplic_Pos_Fim.VersaoCDA("3");
                d1.getAplic.getAplicPosFim.qtde_Pos_Fim = quantidadeFinal;
                d1.getAplic.getAplicPosFim.custo_Correc_Pos_Fim = custoCorrecPosFim;
                d1.getAplic.getAplicPosFim.merc_Pos_Fim = valorFinal;

                // Adiciona Demais_Codif
                arq.getCorpo.getInform.getListAtiv.getDemais_Codif.Add(d1);
            }
            #endregion

            #region Opções Vendidas
            ativoBolsaQuery = new AtivoBolsaQuery("A");
            posicaoBolsaHistoricoQuery = new PosicaoBolsaHistoricoQuery("P");

            posicaoBolsaHistoricoCollection = new PosicaoBolsaHistoricoCollection();
            posicaoBolsaHistoricoQuery.Select(posicaoBolsaHistoricoQuery.TipoMercado,
                                             posicaoBolsaHistoricoQuery.CdAtivoBolsa,
                                             posicaoBolsaHistoricoQuery.Quantidade.Sum(),
                                             posicaoBolsaHistoricoQuery.ValorCustoLiquido.Sum(),
                                             posicaoBolsaHistoricoQuery.ValorMercado.Sum());
            posicaoBolsaHistoricoQuery.InnerJoin(ativoBolsaQuery).On(ativoBolsaQuery.CdAtivoBolsa == posicaoBolsaHistoricoQuery.CdAtivoBolsa);
            posicaoBolsaHistoricoQuery.Where(posicaoBolsaHistoricoQuery.IdCliente.Equal(idCarteira),
                                            posicaoBolsaHistoricoQuery.DataHistorico.Equal(dataPosicao),
                                            posicaoBolsaHistoricoQuery.TipoMercado.In(TipoMercadoBolsa.OpcaoCompra, TipoMercadoBolsa.OpcaoVenda),
                                            posicaoBolsaHistoricoQuery.Quantidade.LessThan(0),
                                            ativoBolsaQuery.DataInicioVigencia.IsNotNull(),
                                            ativoBolsaQuery.DataFimVigencia.IsNull() || ativoBolsaQuery.DataFimVigencia.GreaterThanOrEqual(dataPosicao),
                                            ativoBolsaQuery.DataInicioVigencia.NotEqual(dataIndefinida));
            posicaoBolsaHistoricoQuery.GroupBy(posicaoBolsaHistoricoQuery.TipoMercado,
                                               posicaoBolsaHistoricoQuery.CdAtivoBolsa);
            posicaoBolsaHistoricoQuery.OrderBy(posicaoBolsaHistoricoQuery.TipoMercado.Ascending,
                                               posicaoBolsaHistoricoQuery.CdAtivoBolsa.Ascending);
            posicaoBolsaHistoricoCollection.Load(posicaoBolsaHistoricoQuery);

            foreach (PosicaoBolsaHistorico posicaoBolsaHistorico in posicaoBolsaHistoricoCollection)
            {
                string cdAtivoBolsa = posicaoBolsaHistorico.CdAtivoBolsa;
                string tipoMercado = posicaoBolsaHistorico.TipoMercado;

                decimal quantidadeFinal = posicaoBolsaHistorico.Quantidade.HasValue ? posicaoBolsaHistorico.Quantidade.Value : 0;
                decimal valorFinal = posicaoBolsaHistorico.ValorMercado.HasValue ? posicaoBolsaHistorico.ValorMercado.Value : 0;
                decimal custoCorrecPosFim = posicaoBolsaHistorico.ValorCustoLiquido.HasValue ? posicaoBolsaHistorico.ValorCustoLiquido.Value : 0;

                AtivoBolsa ativoBolsa = new AtivoBolsa();
                List<esQueryItem> campos = new List<esQueryItem>();
                campos.Add(ativoBolsa.Query.DataInicioVigencia);
                campos.Add(ativoBolsa.Query.DataFimVigencia);
                ativoBolsa.LoadByPrimaryKey(campos, cdAtivoBolsa);

                if (ativoBolsa.DataFimVigencia.HasValue && ativoBolsa.DataFimVigencia.Value < dataPosicao)
                    continue;

                string tipoAplicacao = Convert.ToString((int)TipoAplicacaoCDA.OpcoesPosicaoLancada);

                string codigoAtivo = "";
                if (tipoMercado == TipoMercadoBolsa.OpcaoCompra)
                {
                    codigoAtivo = Convert.ToString((int)TipoAtivoCDA.OpcaoCompra);
                }
                else
                {
                    codigoAtivo = Convert.ToString((int)TipoAtivoCDA.OpcaoVenda);
                }

                OperacaoBolsa operacaoBolsaCompra = new OperacaoBolsa();
                operacaoBolsaCompra.Query.Select(operacaoBolsaCompra.Query.Quantidade.Sum(),
                                                 operacaoBolsaCompra.Query.Valor.Sum());
                operacaoBolsaCompra.Query.Where(operacaoBolsaCompra.Query.IdCliente.Equal(idCarteira),
                                                operacaoBolsaCompra.Query.CdAtivoBolsa.Equal(cdAtivoBolsa),
                                                operacaoBolsaCompra.Query.Data.GreaterThanOrEqual(dataInicioMes),
                                                operacaoBolsaCompra.Query.Data.LessThanOrEqual(dataPosicao),
                                                operacaoBolsaCompra.Query.TipoOperacao.In(TipoOperacaoBolsa.Compra, TipoOperacaoBolsa.CompraDaytrade));
                operacaoBolsaCompra.Query.Load();

                decimal quantidadeCompra = operacaoBolsaCompra.Quantidade.HasValue ? operacaoBolsaCompra.Quantidade.Value : 0;
                decimal valorCompra = operacaoBolsaCompra.Valor.HasValue ? operacaoBolsaCompra.Valor.Value : 0;

                OperacaoBolsa operacaoBolsaVenda = new OperacaoBolsa();
                operacaoBolsaVenda.Query.Select(operacaoBolsaVenda.Query.Quantidade.Sum(),
                                                operacaoBolsaVenda.Query.Valor.Sum());
                operacaoBolsaVenda.Query.Where(operacaoBolsaVenda.Query.IdCliente.Equal(idCarteira),
                                               operacaoBolsaVenda.Query.CdAtivoBolsa.Equal(cdAtivoBolsa),
                                               operacaoBolsaVenda.Query.Data.GreaterThanOrEqual(dataInicioMes),
                                               operacaoBolsaVenda.Query.Data.LessThanOrEqual(dataPosicao),
                                               operacaoBolsaVenda.Query.TipoOperacao.In(TipoOperacaoBolsa.Venda, TipoOperacaoBolsa.VendaDaytrade));
                operacaoBolsaVenda.Query.Load();

                decimal quantidadeVenda = operacaoBolsaVenda.Quantidade.HasValue ? operacaoBolsaVenda.Quantidade.Value : 0;
                decimal valorVenda = operacaoBolsaVenda.Valor.HasValue ? operacaoBolsaVenda.Valor.Value : 0;
                
                Demais_Codif d1 = new Demais_Codif();

                d1.codigoAtivo = cdAtivoBolsa;
                d1.cod_Tp_Ativ = codigoAtivo;
                d1.dataInicioVigencia = ativoBolsa.DataInicioVigencia.HasValue ? ativoBolsa.DataInicioVigencia.Value : new DateTime(4000, 01, 01);

                // Demais_Codif - Aplic
                d1.getAplic.cod_Tp_Aplic = tipoAplicacao;
                d1.getAplic.empr_Ligada = "N";
                d1.getAplic.qtde_Dias_Confid = this.diasConfidencialidade.ToString();

                // Aplic - Negoc
                d1.getAplic.getAplicNegoc.tipoNegocio = Convert.ToString((int)TipoNegociacaoCDA.Negociacao);
                d1.getAplic.getAplicNegoc.quantidadeVendasNegocio = quantidadeVenda;
                d1.getAplic.getAplicNegoc.valorVendasNegocio = valorVenda;
                d1.getAplic.getAplicNegoc.quantidadeAquisicaoNegocio = quantidadeCompra;
                d1.getAplic.getAplicNegoc.valorAquisicaoNegocio = valorCompra;

                // Aplic - Pos_Fim
                d1.getAplic.aplic_Pos_Fim.VersaoCDA("3");
                d1.getAplic.getAplicPosFim.qtde_Pos_Fim = quantidadeFinal;
                d1.getAplic.getAplicPosFim.custo_Correc_Pos_Fim = custoCorrecPosFim;
                d1.getAplic.getAplicPosFim.merc_Pos_Fim = valorFinal;

                // Adiciona Demais_Codif
                arq.getCorpo.getInform.getListAtiv.getDemais_Codif.Add(d1);
            }
            #endregion

            #region BTC doado
            ativoBolsaQuery = new AtivoBolsaQuery("A");
            PosicaoEmprestimoBolsaHistoricoQuery posicaoEmprestimoBolsaHistoricoQuery = new PosicaoEmprestimoBolsaHistoricoQuery("P");
            PosicaoEmprestimoBolsaHistoricoCollection posicaoEmprestimoBolsaHistoricoCollection = new PosicaoEmprestimoBolsaHistoricoCollection();

            posicaoEmprestimoBolsaHistoricoQuery.Select(posicaoEmprestimoBolsaHistoricoQuery.CdAtivoBolsa,
                                                       posicaoEmprestimoBolsaHistoricoQuery.Quantidade.Sum(),
                                                       posicaoEmprestimoBolsaHistoricoQuery.ValorMercado.Sum());
            posicaoEmprestimoBolsaHistoricoQuery.InnerJoin(ativoBolsaQuery).On(ativoBolsaQuery.CdAtivoBolsa == posicaoBolsaHistoricoQuery.CdAtivoBolsa);
            posicaoEmprestimoBolsaHistoricoQuery.Where(posicaoEmprestimoBolsaHistoricoQuery.IdCliente.Equal(idCarteira),
                                                    posicaoEmprestimoBolsaHistoricoQuery.DataHistorico.Equal(dataPosicao),
                                                    posicaoEmprestimoBolsaHistoricoQuery.PontaEmprestimo.Equal((byte)PontaEmprestimoBolsa.Doador),
                                                    ativoBolsaQuery.DataInicioVigencia.IsNotNull(),
                                                    ativoBolsaQuery.DataFimVigencia.IsNull() || ativoBolsaQuery.DataFimVigencia.GreaterThanOrEqual(dataPosicao),
                                                    ativoBolsaQuery.DataInicioVigencia.NotEqual(dataIndefinida));
            posicaoEmprestimoBolsaHistoricoQuery.GroupBy(posicaoEmprestimoBolsaHistoricoQuery.CdAtivoBolsa);
            posicaoEmprestimoBolsaHistoricoQuery.OrderBy(posicaoEmprestimoBolsaHistoricoQuery.CdAtivoBolsa.Ascending);
            posicaoEmprestimoBolsaHistoricoCollection.Load(posicaoEmprestimoBolsaHistoricoQuery);

            foreach (PosicaoEmprestimoBolsaHistorico posicaoEmprestimoBolsaHistorico in posicaoEmprestimoBolsaHistoricoCollection)
            {
                string cdAtivoBolsa = posicaoEmprestimoBolsaHistorico.CdAtivoBolsa;

                decimal quantidadeFinal = posicaoEmprestimoBolsaHistorico.Quantidade.HasValue ? posicaoEmprestimoBolsaHistorico.Quantidade.Value : 0;
                decimal valorFinal = posicaoEmprestimoBolsaHistorico.ValorMercado.HasValue ? posicaoEmprestimoBolsaHistorico.ValorMercado.Value : 0;

                AtivoBolsa ativoBolsa = new AtivoBolsa();
                List<esQueryItem> campos = new List<esQueryItem>();
                campos.Add(ativoBolsa.Query.TipoPapel);
                campos.Add(ativoBolsa.Query.DataInicioVigencia);
                campos.Add(ativoBolsa.Query.CodigoCDA);
                ativoBolsa.LoadByPrimaryKey(campos, cdAtivoBolsa);

                string tipoAplicacao = Convert.ToString((int)TipoAplicacaoCDA.TVMCedidoEmprestimo);

                string codigoAtivo = "";                
                #region Checa codigo de ativo
                if (ativoBolsa.TipoPapel.Value == (byte)TipoPapelAtivo.Normal)
                {
                    codigoAtivo = Utilitario.Right(cdAtivoBolsa, 1) == "3" ? Convert.ToString((int)TipoAtivoCDA.AcaoOrdinaria) : Convert.ToString((int)TipoAtivoCDA.AcaoPreferencial);                    
                }
                else if (ativoBolsa.TipoPapel.Value == (byte)TipoPapelAtivo.BDRNivelI)
                {
                    codigoAtivo = Convert.ToString((int)TipoAtivoCDA.BDRNivelI);                    
                }
                else if (ativoBolsa.TipoPapel.Value == (byte)TipoPapelAtivo.BDRNivelII)
                {
                    codigoAtivo = Convert.ToString((int)TipoAtivoCDA.BDRNivelII);                    
                }
                else if (ativoBolsa.TipoPapel.Value == (byte)TipoPapelAtivo.BDRNivelIII)
                {
                    codigoAtivo = Convert.ToString((int)TipoAtivoCDA.BDRNivelIII);                    
                }
                else if (ativoBolsa.TipoPapel.Value == (byte)TipoPapelAtivo.BonusSubscricao)
                {
                    codigoAtivo = Convert.ToString((int)TipoAtivoCDA.BonusSubscricao);                    
                }
                else if (ativoBolsa.TipoPapel.Value == (byte)TipoPapelAtivo.CertificadoDepositoAcoes)
                {
                    codigoAtivo = Convert.ToString((int)TipoAtivoCDA.CertificadoDepositoAcoes);                    
                }
                else if (ativoBolsa.TipoPapel.Value == (byte)TipoPapelAtivo.ETF)
                {
                    codigoAtivo = Convert.ToString((int)TipoAtivoCDA.AcaoPreferencial); //Fundos de indice são jogados, por ora, em Ação                    
                }
                else if (ativoBolsa.TipoPapel.Value == (byte)TipoPapelAtivo.ReciboDepositoAcoes)
                {
                    codigoAtivo = Convert.ToString((int)TipoAtivoCDA.ReciboDepositoAcoes);                    
                }
                else if (ativoBolsa.TipoPapel.Value == (byte)TipoPapelAtivo.ReciboSubscricao)
                {
                    codigoAtivo = Convert.ToString((int)TipoAtivoCDA.ReciboSubscricao);                    
                }
                #endregion

                if (ativoBolsa.CodigoCDA.HasValue) //Se tiver o codigo CDA força pelo codigo informado
                {
                    codigoAtivo = Convert.ToString(ativoBolsa.CodigoCDA);
                }

                Demais_Codif d1 = new Demais_Codif();

                d1.codigoAtivo = cdAtivoBolsa;
                d1.cod_Tp_Ativ = codigoAtivo;
                d1.dataInicioVigencia = ativoBolsa.DataInicioVigencia.HasValue ? ativoBolsa.DataInicioVigencia.Value : new DateTime(4000, 01, 01);

                bool empresaLigada = this.IndicaEmpresaLigadaAcao(cdAtivoBolsa, carteira.IdAgenteAdministrador.Value, carteira.IdAgenteGestor.Value);
                // Demais_Codif - Aplic
                d1.getAplic.cod_Tp_Aplic = tipoAplicacao;
                d1.getAplic.empr_Ligada = empresaLigada ? "S" : "N";
                d1.getAplic.qtde_Dias_Confid = this.diasConfidencialidade.ToString();

                // Aplic - Negoc
                d1.getAplic.getAplicNegoc.tipoNegocio = Convert.ToString((int)TipoNegociacaoCDA.Negociacao);
                
                // Aplic - Pos_Fim
                d1.getAplic.aplic_Pos_Fim.VersaoCDA("3");
                d1.getAplic.getAplicPosFim.qtde_Pos_Fim = quantidadeFinal;
                d1.getAplic.getAplicPosFim.merc_Pos_Fim = valorFinal;

                // Adiciona Demais_Codif
                arq.getCorpo.getInform.getListAtiv.getDemais_Codif.Add(d1);
            }
            #endregion

            #region BTC tomado
            ativoBolsaQuery = new AtivoBolsaQuery("A");
            posicaoEmprestimoBolsaHistoricoQuery = new PosicaoEmprestimoBolsaHistoricoQuery("P");
            posicaoEmprestimoBolsaHistoricoCollection = new PosicaoEmprestimoBolsaHistoricoCollection();

            posicaoEmprestimoBolsaHistoricoQuery.Select(posicaoEmprestimoBolsaHistoricoQuery.CdAtivoBolsa,
                                                       posicaoEmprestimoBolsaHistoricoQuery.Quantidade.Sum(),
                                                       posicaoEmprestimoBolsaHistoricoQuery.ValorMercado.Sum());
            posicaoEmprestimoBolsaHistoricoQuery.InnerJoin(ativoBolsaQuery).On(ativoBolsaQuery.CdAtivoBolsa == posicaoBolsaHistoricoQuery.CdAtivoBolsa);
            posicaoEmprestimoBolsaHistoricoQuery.Where(posicaoEmprestimoBolsaHistoricoQuery.IdCliente.Equal(idCarteira),
                                                    posicaoEmprestimoBolsaHistoricoQuery.DataHistorico.Equal(dataPosicao),
                                                    posicaoEmprestimoBolsaHistoricoQuery.PontaEmprestimo.Equal((byte)PontaEmprestimoBolsa.Tomador),
                                                    ativoBolsaQuery.DataInicioVigencia.IsNotNull(),
                                                    ativoBolsaQuery.DataFimVigencia.IsNull() || ativoBolsaQuery.DataFimVigencia.GreaterThanOrEqual(dataPosicao),
                                                    ativoBolsaQuery.DataInicioVigencia.NotEqual(dataIndefinida));
            posicaoEmprestimoBolsaHistoricoQuery.GroupBy(posicaoEmprestimoBolsaHistoricoQuery.CdAtivoBolsa);
            posicaoEmprestimoBolsaHistoricoQuery.OrderBy(posicaoEmprestimoBolsaHistoricoQuery.CdAtivoBolsa.Ascending);
            posicaoEmprestimoBolsaHistoricoCollection.Load(posicaoEmprestimoBolsaHistoricoQuery);


            foreach (PosicaoEmprestimoBolsaHistorico posicaoEmprestimoBolsaHistorico in posicaoEmprestimoBolsaHistoricoCollection)
            {
                string cdAtivoBolsa = posicaoEmprestimoBolsaHistorico.CdAtivoBolsa;

                decimal quantidadeFinal = posicaoEmprestimoBolsaHistorico.Quantidade.HasValue ? posicaoEmprestimoBolsaHistorico.Quantidade.Value : 0;
                decimal valorFinal = posicaoEmprestimoBolsaHistorico.ValorMercado.HasValue ? posicaoEmprestimoBolsaHistorico.ValorMercado.Value : 0;

                AtivoBolsa ativoBolsa = new AtivoBolsa();
                List<esQueryItem> campos = new List<esQueryItem>();
                campos.Add(ativoBolsa.Query.TipoPapel);
                campos.Add(ativoBolsa.Query.DataInicioVigencia);
                campos.Add(ativoBolsa.Query.CodigoCDA);
                ativoBolsa.LoadByPrimaryKey(campos, cdAtivoBolsa);

                string tipoAplicacao = Convert.ToString((int)TipoAplicacaoCDA.TVMRecebidoEmprestimo);

                string codigoAtivo = "";
                #region Checa codigo de ativo
                if (ativoBolsa.TipoPapel.Value == (byte)TipoPapelAtivo.Normal)
                {
                    codigoAtivo = Utilitario.Right(cdAtivoBolsa, 1) == "3" ? Convert.ToString((int)TipoAtivoCDA.AcaoOrdinaria) : Convert.ToString((int)TipoAtivoCDA.AcaoPreferencial);
                }
                else if (ativoBolsa.TipoPapel.Value == (byte)TipoPapelAtivo.BDRNivelI)
                {
                    codigoAtivo = Convert.ToString((int)TipoAtivoCDA.BDRNivelI);
                }
                else if (ativoBolsa.TipoPapel.Value == (byte)TipoPapelAtivo.BDRNivelII)
                {
                    codigoAtivo = Convert.ToString((int)TipoAtivoCDA.BDRNivelII);
                }
                else if (ativoBolsa.TipoPapel.Value == (byte)TipoPapelAtivo.BDRNivelIII)
                {
                    codigoAtivo = Convert.ToString((int)TipoAtivoCDA.BDRNivelIII);
                }
                else if (ativoBolsa.TipoPapel.Value == (byte)TipoPapelAtivo.BonusSubscricao)
                {
                    codigoAtivo = Convert.ToString((int)TipoAtivoCDA.BonusSubscricao);
                }
                else if (ativoBolsa.TipoPapel.Value == (byte)TipoPapelAtivo.CertificadoDepositoAcoes)
                {
                    codigoAtivo = Convert.ToString((int)TipoAtivoCDA.CertificadoDepositoAcoes);
                }
                else if (ativoBolsa.TipoPapel.Value == (byte)TipoPapelAtivo.ETF)
                {
                    codigoAtivo = Convert.ToString((int)TipoAtivoCDA.AcaoPreferencial); //Fundos de indice são jogados, por ora, em Ação                    
                }
                else if (ativoBolsa.TipoPapel.Value == (byte)TipoPapelAtivo.ReciboDepositoAcoes)
                {
                    codigoAtivo = Convert.ToString((int)TipoAtivoCDA.ReciboDepositoAcoes);
                }
                else if (ativoBolsa.TipoPapel.Value == (byte)TipoPapelAtivo.ReciboSubscricao)
                {
                    codigoAtivo = Convert.ToString((int)TipoAtivoCDA.ReciboSubscricao);
                }
                #endregion

                if (ativoBolsa.CodigoCDA.HasValue) //Se tiver o codigo CDA força pelo codigo informado
                {
                    codigoAtivo = Convert.ToString(ativoBolsa.CodigoCDA);
                }

                Demais_Codif d1 = new Demais_Codif();

                d1.codigoAtivo = cdAtivoBolsa;
                d1.cod_Tp_Ativ = codigoAtivo;
                d1.dataInicioVigencia = ativoBolsa.DataInicioVigencia.HasValue ? ativoBolsa.DataInicioVigencia.Value : new DateTime(4000, 01, 01);

                bool empresaLigada = this.IndicaEmpresaLigadaAcao(cdAtivoBolsa, carteira.IdAgenteAdministrador.Value, carteira.IdAgenteGestor.Value);
                // Demais_Codif - Aplic
                d1.getAplic.cod_Tp_Aplic = tipoAplicacao;
                d1.getAplic.empr_Ligada = empresaLigada ? "S" : "N";
                d1.getAplic.qtde_Dias_Confid = this.diasConfidencialidade.ToString();

                // Aplic - Negoc
                d1.getAplic.getAplicNegoc.tipoNegocio = Convert.ToString((int)TipoNegociacaoCDA.Negociacao);
                
                // Aplic - Pos_Fim
                d1.getAplic.aplic_Pos_Fim.VersaoCDA("3");
                d1.getAplic.getAplicPosFim.qtde_Pos_Fim = quantidadeFinal;
                d1.getAplic.getAplicPosFim.merc_Pos_Fim = valorFinal;

                // Adiciona Demais_Codif
                arq.getCorpo.getInform.getListAtiv.getDemais_Codif.Add(d1);
            }
            #endregion

            #region Futuros/Opções Compradas
            PosicaoBMFHistoricoQuery posicaoBMFHistoricoQuery = new PosicaoBMFHistoricoQuery("P");
            AtivoBMFQuery ativoBMFQuery = new AtivoBMFQuery("A");

            PosicaoBMFHistoricoCollection posicaoBMFHistoricoCollection = new PosicaoBMFHistoricoCollection();
            posicaoBMFHistoricoQuery.Select(posicaoBMFHistoricoQuery.TipoMercado,
                                           posicaoBMFHistoricoQuery.CdAtivoBMF,
                                           posicaoBMFHistoricoQuery.Serie,
                                           posicaoBMFHistoricoQuery.Quantidade.Sum(),
                                           posicaoBMFHistoricoQuery.ValorCustoLiquido.Sum(),
                                           posicaoBMFHistoricoQuery.ValorMercado.Sum());
            posicaoBMFHistoricoQuery.InnerJoin(ativoBMFQuery).On(ativoBMFQuery.CdAtivoBMF == posicaoBMFHistoricoQuery.CdAtivoBMF &
                                                             ativoBMFQuery.Serie == posicaoBMFHistoricoQuery.Serie);
            posicaoBMFHistoricoQuery.Where(posicaoBMFHistoricoQuery.IdCliente.Equal(idCarteira) &
                                          posicaoBMFHistoricoQuery.DataHistorico.Equal(dataPosicao) &
                                          posicaoBMFHistoricoQuery.Quantidade.GreaterThan(0) &
                                          ativoBMFQuery.DataFimVigencia.IsNotNull() &
                                          ativoBMFQuery.DataFimVigencia.NotEqual(dataIndefinida));
            posicaoBMFHistoricoQuery.GroupBy(posicaoBMFHistoricoQuery.TipoMercado,
                                            posicaoBMFHistoricoQuery.CdAtivoBMF,
                                            posicaoBMFHistoricoQuery.Serie);
            posicaoBMFHistoricoQuery.OrderBy(posicaoBMFHistoricoQuery.TipoMercado.Ascending,
                                            posicaoBMFHistoricoQuery.CdAtivoBMF.Ascending);
            posicaoBMFHistoricoCollection.Load(posicaoBMFHistoricoQuery);

            foreach (PosicaoBMFHistorico posicaoBMFHistorico in posicaoBMFHistoricoCollection)
            {
                string cdAtivoBMF = posicaoBMFHistorico.CdAtivoBMF;
                string serie = posicaoBMFHistorico.Serie;
                byte tipoMercado = posicaoBMFHistorico.TipoMercado.Value;

                decimal quantidadeFinal = posicaoBMFHistorico.Quantidade.HasValue ? posicaoBMFHistorico.Quantidade.Value : 0;
                decimal valorFinal = (posicaoBMFHistorico.TipoMercado.Value != (byte)TipoMercadoBMF.Futuro && posicaoBMFHistorico.ValorMercado.HasValue)? posicaoBMFHistorico.ValorMercado.Value : 0;
                
                AtivoBMF ativoBMF = new AtivoBMF();
                ativoBMF.LoadByPrimaryKey(cdAtivoBMF, serie);

                string tipoAplicacao = Convert.ToString((int)TipoAplicacaoCDA.MercadoFuturoComprado);
                string codigoAtivo = ativoBMF.CodigoCDA.HasValue? Convert.ToString(ativoBMF.CodigoCDA): this.RetornaCodigoFuturoCDA(ativoBMF);

                OperacaoBMF operacaoBMFCompra = new OperacaoBMF();
                operacaoBMFCompra.Query.Select(operacaoBMFCompra.Query.Quantidade.Sum(),
                                               operacaoBMFCompra.Query.Valor.Sum());
                operacaoBMFCompra.Query.Where(operacaoBMFCompra.Query.IdCliente.Equal(idCarteira),
                                              operacaoBMFCompra.Query.CdAtivoBMF.Equal(cdAtivoBMF),
                                              operacaoBMFCompra.Query.Serie.Equal(serie),
                                              operacaoBMFCompra.Query.Data.GreaterThanOrEqual(dataInicioMes),
                                              operacaoBMFCompra.Query.Data.LessThanOrEqual(dataPosicao),
                                              operacaoBMFCompra.Query.TipoOperacao.In(TipoOperacaoBolsa.Compra, TipoOperacaoBolsa.CompraDaytrade));
                operacaoBMFCompra.Query.Load();

                decimal quantidadeCompra = operacaoBMFCompra.Quantidade.HasValue ? operacaoBMFCompra.Quantidade.Value : 0;
                decimal valorCompra = operacaoBMFCompra.Valor.HasValue ? operacaoBMFCompra.Valor.Value : 0;

                OperacaoBMF operacaoBMFVenda = new OperacaoBMF();
                operacaoBMFVenda.Query.Select(operacaoBMFVenda.Query.Quantidade.Sum(),
                                               operacaoBMFVenda.Query.Valor.Sum());
                operacaoBMFVenda.Query.Where(operacaoBMFVenda.Query.IdCliente.Equal(idCarteira),
                                              operacaoBMFVenda.Query.CdAtivoBMF.Equal(cdAtivoBMF),
                                              operacaoBMFVenda.Query.Serie.Equal(serie),
                                              operacaoBMFVenda.Query.Data.GreaterThanOrEqual(dataInicioMes),
                                              operacaoBMFVenda.Query.Data.LessThanOrEqual(dataPosicao),
                                              operacaoBMFVenda.Query.TipoOperacao.In(TipoOperacaoBolsa.Venda, TipoOperacaoBolsa.VendaDaytrade));
                operacaoBMFVenda.Query.Load();

                decimal quantidadeVenda = operacaoBMFVenda.Quantidade.HasValue ? operacaoBMFVenda.Quantidade.Value : 0;
                decimal valorVenda = operacaoBMFVenda.Valor.HasValue ? operacaoBMFVenda.Valor.Value : 0;

                Demais_Codif d1 = new Demais_Codif();

                d1.codigoAtivo = this.RetornaAtivoFuturoCDA(ativoBMF);
                d1.cod_Tp_Ativ = codigoAtivo;
                d1.dataInicioVigencia = ativoBMF.DataInicioVigencia.HasValue ? ativoBMF.DataInicioVigencia.Value : new DateTime(4000, 01, 01);

                // Demais_Codif - Aplic
                d1.getAplic.cod_Tp_Aplic = tipoAplicacao;
                d1.getAplic.empr_Ligada = "N";
                d1.getAplic.qtde_Dias_Confid = this.diasConfidencialidade.ToString();

                // Aplic - Negoc
                d1.getAplic.getAplicNegoc.tipoNegocio = Convert.ToString((int)TipoNegociacaoCDA.Negociacao);
                d1.getAplic.getAplicNegoc.quantidadeVendasNegocio = quantidadeVenda;
                d1.getAplic.getAplicNegoc.valorVendasNegocio = valorVenda;
                d1.getAplic.getAplicNegoc.quantidadeAquisicaoNegocio = quantidadeCompra;
                d1.getAplic.getAplicNegoc.valorAquisicaoNegocio = valorCompra;

                // Aplic - Pos_Fim
                d1.getAplic.aplic_Pos_Fim.VersaoCDA("3");
                d1.getAplic.getAplicPosFim.qtde_Pos_Fim = quantidadeFinal;
                d1.getAplic.getAplicPosFim.merc_Pos_Fim = valorFinal;

                // Adiciona Demais_Codif
                arq.getCorpo.getInform.getListAtiv.getDemais_Codif.Add(d1);
            }
            #endregion

            #region Futuros/Opções Vendidas
            posicaoBMFHistoricoQuery = new PosicaoBMFHistoricoQuery("P");
            ativoBMFQuery = new AtivoBMFQuery("A");

            posicaoBMFHistoricoCollection = new PosicaoBMFHistoricoCollection();
            posicaoBMFHistoricoQuery.Select(posicaoBMFHistoricoQuery.TipoMercado,
                                           posicaoBMFHistoricoQuery.CdAtivoBMF,
                                           posicaoBMFHistoricoQuery.Serie,
                                           posicaoBMFHistoricoQuery.Quantidade.Sum(),
                                           posicaoBMFHistoricoQuery.ValorMercado.Sum());
            posicaoBMFHistoricoQuery.InnerJoin(ativoBMFQuery).On(ativoBMFQuery.CdAtivoBMF == posicaoBMFHistoricoQuery.CdAtivoBMF &
                                                             ativoBMFQuery.Serie == posicaoBMFHistoricoQuery.Serie);
            posicaoBMFHistoricoQuery.Where(posicaoBMFHistoricoQuery.IdCliente.Equal(idCarteira) &
                                          posicaoBMFHistoricoQuery.DataHistorico.Equal(dataPosicao) &
                                          posicaoBMFHistoricoQuery.Quantidade.LessThan(0) &
                                          ativoBMFQuery.DataFimVigencia.IsNotNull() &
                                          ativoBMFQuery.DataFimVigencia.NotEqual(dataIndefinida));
            posicaoBMFHistoricoQuery.GroupBy(posicaoBMFHistoricoQuery.TipoMercado,
                                            posicaoBMFHistoricoQuery.CdAtivoBMF,
                                            posicaoBMFHistoricoQuery.Serie);
            posicaoBMFHistoricoQuery.OrderBy(posicaoBMFHistoricoQuery.TipoMercado.Ascending,
                                            posicaoBMFHistoricoQuery.CdAtivoBMF.Ascending);
            posicaoBMFHistoricoCollection.Load(posicaoBMFHistoricoQuery);

            foreach (PosicaoBMFHistorico posicaoBMFHistorico in posicaoBMFHistoricoCollection)
            {
                string cdAtivoBMF = posicaoBMFHistorico.CdAtivoBMF;
                string serie = posicaoBMFHistorico.Serie;
                byte tipoMercado = posicaoBMFHistorico.TipoMercado.Value;

                decimal quantidadeFinal = posicaoBMFHistorico.Quantidade.HasValue ? posicaoBMFHistorico.Quantidade.Value : 0;
                decimal valorFinal = (posicaoBMFHistorico.TipoMercado.Value != (byte)TipoMercadoBMF.Futuro && posicaoBMFHistorico.ValorMercado.HasValue) ? posicaoBMFHistorico.ValorMercado.Value : 0;
                
                AtivoBMF ativoBMF = new AtivoBMF();
                ativoBMF.LoadByPrimaryKey(cdAtivoBMF, serie);

                string tipoAplicacao = Convert.ToString((int)TipoAplicacaoCDA.MercadoFuturoVendido);
                string codigoAtivo = ativoBMF.CodigoCDA.HasValue? Convert.ToString(ativoBMF.CodigoCDA): this.RetornaCodigoFuturoCDA(ativoBMF);

                OperacaoBMF operacaoBMFCompra = new OperacaoBMF();
                operacaoBMFCompra.Query.Select(operacaoBMFCompra.Query.Quantidade.Sum(),
                                               operacaoBMFCompra.Query.Valor.Sum());
                operacaoBMFCompra.Query.Where(operacaoBMFCompra.Query.IdCliente.Equal(idCarteira),
                                              operacaoBMFCompra.Query.CdAtivoBMF.Equal(cdAtivoBMF),
                                              operacaoBMFCompra.Query.Serie.Equal(serie),
                                              operacaoBMFCompra.Query.Data.GreaterThanOrEqual(dataInicioMes),
                                              operacaoBMFCompra.Query.Data.LessThanOrEqual(dataPosicao),
                                              operacaoBMFCompra.Query.TipoOperacao.In(TipoOperacaoBolsa.Compra, TipoOperacaoBolsa.CompraDaytrade));
                operacaoBMFCompra.Query.Load();

                decimal quantidadeCompra = operacaoBMFCompra.Quantidade.HasValue ? operacaoBMFCompra.Quantidade.Value : 0;
                decimal valorCompra = operacaoBMFCompra.Valor.HasValue ? operacaoBMFCompra.Valor.Value : 0;

                OperacaoBMF operacaoBMFVenda = new OperacaoBMF();
                operacaoBMFVenda.Query.Select(operacaoBMFVenda.Query.Quantidade.Sum(),
                                               operacaoBMFVenda.Query.Valor.Sum());
                operacaoBMFVenda.Query.Where(operacaoBMFVenda.Query.IdCliente.Equal(idCarteira),
                                              operacaoBMFVenda.Query.CdAtivoBMF.Equal(cdAtivoBMF),
                                              operacaoBMFVenda.Query.Serie.Equal(serie),
                                              operacaoBMFVenda.Query.Data.GreaterThanOrEqual(dataInicioMes),
                                              operacaoBMFVenda.Query.Data.LessThanOrEqual(dataPosicao),
                                              operacaoBMFVenda.Query.TipoOperacao.In(TipoOperacaoBolsa.Venda, TipoOperacaoBolsa.VendaDaytrade));
                operacaoBMFVenda.Query.Load();

                decimal quantidadeVenda = operacaoBMFVenda.Quantidade.HasValue ? operacaoBMFVenda.Quantidade.Value : 0;
                decimal valorVenda = operacaoBMFVenda.Valor.HasValue ? operacaoBMFVenda.Valor.Value : 0;

                Demais_Codif d1 = new Demais_Codif();

                d1.codigoAtivo = this.RetornaAtivoFuturoCDA(ativoBMF);
                d1.cod_Tp_Ativ = codigoAtivo;
                d1.dataInicioVigencia = ativoBMF.DataInicioVigencia.HasValue ? ativoBMF.DataInicioVigencia.Value : new DateTime(4000, 01, 01);

                // Demais_Codif - Aplic
                d1.getAplic.cod_Tp_Aplic = tipoAplicacao;
                d1.getAplic.empr_Ligada = "N";
                d1.getAplic.qtde_Dias_Confid = this.diasConfidencialidade.ToString();

                // Aplic - Negoc
                d1.getAplic.getAplicNegoc.tipoNegocio = Convert.ToString((int)TipoNegociacaoCDA.Negociacao);
                d1.getAplic.getAplicNegoc.quantidadeVendasNegocio = quantidadeVenda;
                d1.getAplic.getAplicNegoc.valorVendasNegocio = valorVenda;
                d1.getAplic.getAplicNegoc.quantidadeAquisicaoNegocio = quantidadeCompra;
                d1.getAplic.getAplicNegoc.valorAquisicaoNegocio = valorCompra;

                // Aplic - Pos_Fim
                d1.getAplic.aplic_Pos_Fim.VersaoCDA("3");
                d1.getAplic.getAplicPosFim.qtde_Pos_Fim = quantidadeFinal;
                d1.getAplic.getAplicPosFim.merc_Pos_Fim = valorFinal;

                // Adiciona Demais_Codif
                arq.getCorpo.getInform.getListAtiv.getDemais_Codif.Add(d1);
            }
            #endregion

            #region Termos comprados ("a receber")
            PosicaoTermoBolsaHistoricoCollection posicaoTermoBolsaHistoricoCollection = new PosicaoTermoBolsaHistoricoCollection();
            posicaoTermoBolsaHistoricoCollection.Query.Select(posicaoTermoBolsaHistoricoCollection.Query.CdAtivoBolsa,
                                                             posicaoTermoBolsaHistoricoCollection.Query.Quantidade.Sum(),
                                                             posicaoTermoBolsaHistoricoCollection.Query.ValorMercado.Sum());
            posicaoTermoBolsaHistoricoCollection.Query.Where(posicaoTermoBolsaHistoricoCollection.Query.IdCliente.Equal(idCarteira),
                                                            posicaoTermoBolsaHistoricoCollection.Query.DataHistorico.Equal(dataPosicao),
                                                            posicaoTermoBolsaHistoricoCollection.Query.Quantidade.GreaterThan(0));
            posicaoTermoBolsaHistoricoCollection.Query.GroupBy(posicaoTermoBolsaHistoricoCollection.Query.CdAtivoBolsa);
            posicaoTermoBolsaHistoricoCollection.Query.OrderBy(posicaoTermoBolsaHistoricoCollection.Query.CdAtivoBolsa.Ascending);
            posicaoTermoBolsaHistoricoCollection.Query.Load();

            foreach (PosicaoTermoBolsaHistorico posicaoTermoBolsaHistorico in posicaoTermoBolsaHistoricoCollection)
            {
                string cdAtivoBolsaTermo = posicaoTermoBolsaHistorico.CdAtivoBolsa;

                decimal quantidadeFinal = posicaoTermoBolsaHistorico.Quantidade.HasValue ? posicaoTermoBolsaHistorico.Quantidade.Value : 0;
                decimal valorFinal = posicaoTermoBolsaHistorico.ValorMercado.HasValue ? posicaoTermoBolsaHistorico.ValorMercado.Value : 0;

                AtivoBolsa ativoBolsaTermo = new AtivoBolsa();
                List<esQueryItem> campos = new List<esQueryItem>();
                campos.Add(ativoBolsaTermo.Query.CdAtivoBolsaObjeto);
                ativoBolsaTermo.LoadByPrimaryKey(campos, cdAtivoBolsaTermo);

                if (String.IsNullOrEmpty(ativoBolsaTermo.CdAtivoBolsaObjeto))
                {
                    throw new Exception("Ativo objeto não definido para o ativo Termo " + cdAtivoBolsaTermo);
                }

                string cdAtivoBolsa = ativoBolsaTermo.CdAtivoBolsaObjeto;

                AtivoBolsa ativoBolsa = new AtivoBolsa();
                campos = new List<esQueryItem>();
                campos.Add(ativoBolsa.Query.DataInicioVigencia);
                campos.Add(ativoBolsa.Query.CodigoCDA);
                campos.Add(ativoBolsa.Query.TipoPapel);
                ativoBolsa.LoadByPrimaryKey(campos, cdAtivoBolsa);

                string tipoAplicacao = Convert.ToString((int)TipoAplicacaoCDA.ComprasTermo);

                string codigoAtivo = "";
                #region Checa codigo de ativo
                if (ativoBolsa.TipoPapel.Value == (byte)TipoPapelAtivo.Normal)
                {
                    codigoAtivo = Utilitario.Right(cdAtivoBolsa, 1) == "3" ? Convert.ToString((int)TipoAtivoCDA.AcaoOrdinaria) : Convert.ToString((int)TipoAtivoCDA.AcaoPreferencial);                    
                }
                else if (ativoBolsa.TipoPapel.Value == (byte)TipoPapelAtivo.BDRNivelI)
                {
                    codigoAtivo = Convert.ToString((int)TipoAtivoCDA.BDRNivelI);                    
                }
                else if (ativoBolsa.TipoPapel.Value == (byte)TipoPapelAtivo.BDRNivelII)
                {
                    codigoAtivo = Convert.ToString((int)TipoAtivoCDA.BDRNivelII);                    
                }
                else if (ativoBolsa.TipoPapel.Value == (byte)TipoPapelAtivo.BDRNivelIII)
                {
                    codigoAtivo = Convert.ToString((int)TipoAtivoCDA.BDRNivelIII);                    
                }
                else if (ativoBolsa.TipoPapel.Value == (byte)TipoPapelAtivo.BonusSubscricao)
                {
                    codigoAtivo = Convert.ToString((int)TipoAtivoCDA.BonusSubscricao);                    
                }
                else if (ativoBolsa.TipoPapel.Value == (byte)TipoPapelAtivo.CertificadoDepositoAcoes)
                {
                    codigoAtivo = Convert.ToString((int)TipoAtivoCDA.CertificadoDepositoAcoes);                    
                }
                else if (ativoBolsa.TipoPapel.Value == (byte)TipoPapelAtivo.ETF)
                {
                    codigoAtivo = Convert.ToString((int)TipoAtivoCDA.AcaoPreferencial); //Fundos de indice são jogados, por ora, em Ação                    
                }
                else if (ativoBolsa.TipoPapel.Value == (byte)TipoPapelAtivo.ReciboDepositoAcoes)
                {
                    codigoAtivo = Convert.ToString((int)TipoAtivoCDA.ReciboDepositoAcoes);                    
                }
                else if (ativoBolsa.TipoPapel.Value == (byte)TipoPapelAtivo.ReciboSubscricao)
                {
                    codigoAtivo = Convert.ToString((int)TipoAtivoCDA.ReciboSubscricao);                    
                }
                #endregion

                if (ativoBolsa.CodigoCDA.HasValue) //Se tiver o codigo CDA força pelo codigo informado
                {
                    codigoAtivo = Convert.ToString(ativoBolsa.CodigoCDA);
                }

                OperacaoBolsa operacaoBolsaCompra = new OperacaoBolsa();
                operacaoBolsaCompra.Query.Select(operacaoBolsaCompra.Query.Quantidade.Sum(),
                                                 operacaoBolsaCompra.Query.Valor.Sum());
                operacaoBolsaCompra.Query.Where(operacaoBolsaCompra.Query.IdCliente.Equal(idCarteira),
                                                operacaoBolsaCompra.Query.CdAtivoBolsa.Equal(cdAtivoBolsaTermo),
                                                operacaoBolsaCompra.Query.Data.GreaterThanOrEqual(dataInicioMes),
                                                operacaoBolsaCompra.Query.Data.LessThanOrEqual(dataPosicao),
                                                operacaoBolsaCompra.Query.TipoOperacao.Equal(TipoOperacaoBolsa.Compra));
                operacaoBolsaCompra.Query.Load();

                decimal quantidadeCompra = operacaoBolsaCompra.Quantidade.HasValue ? operacaoBolsaCompra.Quantidade.Value : 0;
                decimal valorCompra = operacaoBolsaCompra.Valor.HasValue ? operacaoBolsaCompra.Valor.Value : 0;

                Demais_Codif d1 = new Demais_Codif();

                d1.codigoAtivo = cdAtivoBolsaTermo;
                d1.cod_Tp_Ativ = codigoAtivo;
                d1.dataInicioVigencia = ativoBolsa.DataInicioVigencia.HasValue ? ativoBolsa.DataInicioVigencia.Value : new DateTime(4000, 01, 01);

                // Demais_Codif - Aplic
                d1.getAplic.cod_Tp_Aplic = tipoAplicacao;
                d1.getAplic.empr_Ligada = "N";
                d1.getAplic.qtde_Dias_Confid = this.diasConfidencialidade.ToString();

                // Aplic - Negoc
                d1.getAplic.getAplicNegoc.tipoNegocio = Convert.ToString((int)TipoNegociacaoCDA.Negociacao);
                d1.getAplic.getAplicNegoc.quantidadeVendasNegocio = 0;
                d1.getAplic.getAplicNegoc.valorVendasNegocio = 0;
                d1.getAplic.getAplicNegoc.quantidadeAquisicaoNegocio = quantidadeCompra;
                d1.getAplic.getAplicNegoc.valorAquisicaoNegocio = valorCompra;

                // Aplic - Pos_Fim
                d1.getAplic.aplic_Pos_Fim.VersaoCDA("3");
                d1.getAplic.getAplicPosFim.qtde_Pos_Fim = quantidadeFinal;
                d1.getAplic.getAplicPosFim.merc_Pos_Fim = valorFinal;

                // Adiciona Demais_Codif
                arq.getCorpo.getInform.getListAtiv.getDemais_Codif.Add(d1);
            }
            #endregion

            #region Termos vendidos ("a entregar")
            posicaoTermoBolsaHistoricoCollection = new PosicaoTermoBolsaHistoricoCollection();
            posicaoTermoBolsaHistoricoCollection.Query.Select(posicaoTermoBolsaHistoricoCollection.Query.CdAtivoBolsa,
                                                             posicaoTermoBolsaHistoricoCollection.Query.Quantidade.Sum(),
                                                             posicaoTermoBolsaHistoricoCollection.Query.ValorMercado.Sum());
            posicaoTermoBolsaHistoricoCollection.Query.Where(posicaoTermoBolsaHistoricoCollection.Query.IdCliente.Equal(idCarteira),
                                                            posicaoTermoBolsaHistoricoCollection.Query.DataHistorico.Equal(dataPosicao),
                                                            posicaoTermoBolsaHistoricoCollection.Query.Quantidade.LessThan(0));
            posicaoTermoBolsaHistoricoCollection.Query.GroupBy(posicaoTermoBolsaHistoricoCollection.Query.CdAtivoBolsa);
            posicaoTermoBolsaHistoricoCollection.Query.OrderBy(posicaoTermoBolsaHistoricoCollection.Query.CdAtivoBolsa.Ascending);
            posicaoTermoBolsaHistoricoCollection.Query.Load();

            foreach (PosicaoTermoBolsaHistorico posicaoTermoBolsaHistorico in posicaoTermoBolsaHistoricoCollection)
            {
                string cdAtivoBolsaTermo = posicaoTermoBolsaHistorico.CdAtivoBolsa;

                decimal quantidadeFinal = posicaoTermoBolsaHistorico.Quantidade.HasValue ? posicaoTermoBolsaHistorico.Quantidade.Value : 0;
                decimal valorFinal = posicaoTermoBolsaHistorico.ValorMercado.HasValue ? posicaoTermoBolsaHistorico.ValorMercado.Value : 0;

                AtivoBolsa ativoBolsaTermo = new AtivoBolsa();
                List<esQueryItem> campos = new List<esQueryItem>();
                campos.Add(ativoBolsaTermo.Query.CdAtivoBolsaObjeto);
                ativoBolsaTermo.LoadByPrimaryKey(campos, cdAtivoBolsaTermo);

                if (String.IsNullOrEmpty(ativoBolsaTermo.CdAtivoBolsaObjeto))
                {
                    throw new Exception("Ativo objeto não definido para o ativo Termo " + cdAtivoBolsaTermo);
                }

                string cdAtivoBolsa = ativoBolsaTermo.CdAtivoBolsaObjeto;

                AtivoBolsa ativoBolsa = new AtivoBolsa();
                campos = new List<esQueryItem>();
                campos.Add(ativoBolsa.Query.DataInicioVigencia);
                campos.Add(ativoBolsa.Query.CodigoCDA);
                campos.Add(ativoBolsa.Query.TipoPapel);
                ativoBolsa.LoadByPrimaryKey(campos, cdAtivoBolsa);

                string tipoAplicacao = Convert.ToString((int)TipoAplicacaoCDA.VendasTermo);

                string codigoAtivo = "";
                #region Checa codigo de ativo
                if (ativoBolsa.TipoPapel.Value == (byte)TipoPapelAtivo.Normal)
                {
                    codigoAtivo = Utilitario.Right(cdAtivoBolsa, 1) == "3" ? Convert.ToString((int)TipoAtivoCDA.AcaoOrdinaria) : Convert.ToString((int)TipoAtivoCDA.AcaoPreferencial);
                }
                else if (ativoBolsa.TipoPapel.Value == (byte)TipoPapelAtivo.BDRNivelI)
                {
                    codigoAtivo = Convert.ToString((int)TipoAtivoCDA.BDRNivelI);
                }
                else if (ativoBolsa.TipoPapel.Value == (byte)TipoPapelAtivo.BDRNivelII)
                {
                    codigoAtivo = Convert.ToString((int)TipoAtivoCDA.BDRNivelII);
                }
                else if (ativoBolsa.TipoPapel.Value == (byte)TipoPapelAtivo.BDRNivelIII)
                {
                    codigoAtivo = Convert.ToString((int)TipoAtivoCDA.BDRNivelIII);
                }
                else if (ativoBolsa.TipoPapel.Value == (byte)TipoPapelAtivo.BonusSubscricao)
                {
                    codigoAtivo = Convert.ToString((int)TipoAtivoCDA.BonusSubscricao);
                }
                else if (ativoBolsa.TipoPapel.Value == (byte)TipoPapelAtivo.CertificadoDepositoAcoes)
                {
                    codigoAtivo = Convert.ToString((int)TipoAtivoCDA.CertificadoDepositoAcoes);
                }
                else if (ativoBolsa.TipoPapel.Value == (byte)TipoPapelAtivo.ETF)
                {
                    codigoAtivo = Convert.ToString((int)TipoAtivoCDA.AcaoPreferencial); //Fundos de indice são jogados, por ora, em Ação                    
                }
                else if (ativoBolsa.TipoPapel.Value == (byte)TipoPapelAtivo.ReciboDepositoAcoes)
                {
                    codigoAtivo = Convert.ToString((int)TipoAtivoCDA.ReciboDepositoAcoes);
                }
                else if (ativoBolsa.TipoPapel.Value == (byte)TipoPapelAtivo.ReciboSubscricao)
                {
                    codigoAtivo = Convert.ToString((int)TipoAtivoCDA.ReciboSubscricao);
                }
                #endregion

                if (ativoBolsa.CodigoCDA.HasValue) //Se tiver o codigo CDA força pelo codigo informado
                {
                    codigoAtivo = Convert.ToString(ativoBolsa.CodigoCDA);
                }

                OperacaoBolsa operacaoBolsaVenda = new OperacaoBolsa();
                operacaoBolsaVenda.Query.Select(operacaoBolsaVenda.Query.Quantidade.Sum(),
                                                 operacaoBolsaVenda.Query.Valor.Sum());
                operacaoBolsaVenda.Query.Where(operacaoBolsaVenda.Query.IdCliente.Equal(idCarteira),
                                                operacaoBolsaVenda.Query.CdAtivoBolsa.Equal(cdAtivoBolsaTermo),
                                                operacaoBolsaVenda.Query.Data.GreaterThanOrEqual(dataInicioMes),
                                                operacaoBolsaVenda.Query.Data.LessThanOrEqual(dataPosicao),
                                                operacaoBolsaVenda.Query.TipoOperacao.Equal(TipoOperacaoBolsa.Venda));
                operacaoBolsaVenda.Query.Load();

                decimal quantidadeVenda = operacaoBolsaVenda.Quantidade.HasValue ? operacaoBolsaVenda.Quantidade.Value : 0;
                decimal valorVenda = operacaoBolsaVenda.Valor.HasValue ? operacaoBolsaVenda.Valor.Value : 0;

                Demais_Codif d1 = new Demais_Codif();

                d1.codigoAtivo = cdAtivoBolsa;
                d1.cod_Tp_Ativ = codigoAtivo;
                d1.dataInicioVigencia = ativoBolsa.DataInicioVigencia.HasValue ? ativoBolsa.DataInicioVigencia.Value : new DateTime(4000, 01, 01);

                // Demais_Codif - Aplic
                d1.getAplic.cod_Tp_Aplic = tipoAplicacao;
                d1.getAplic.empr_Ligada = "N";
                d1.getAplic.qtde_Dias_Confid = this.diasConfidencialidade.ToString();

                // Aplic - Negoc
                d1.getAplic.getAplicNegoc.tipoNegocio = Convert.ToString((int)TipoNegociacaoCDA.Negociacao);
                d1.getAplic.getAplicNegoc.quantidadeVendasNegocio = quantidadeVenda;
                d1.getAplic.getAplicNegoc.valorVendasNegocio = valorVenda;
                d1.getAplic.getAplicNegoc.quantidadeAquisicaoNegocio = 0;
                d1.getAplic.getAplicNegoc.valorAquisicaoNegocio = 0;

                // Aplic - Pos_Fim
                d1.getAplic.aplic_Pos_Fim.VersaoCDA("3");
                d1.getAplic.getAplicPosFim.qtde_Pos_Fim = quantidadeFinal;
                d1.getAplic.getAplicPosFim.merc_Pos_Fim = valorFinal;

                // Adiciona Demais_Codif
                arq.getCorpo.getInform.getListAtiv.getDemais_Codif.Add(d1);
            }
            #endregion
            
        }
        #endregion

        #region Monta o Node Dep_Prazo_Tit
        /// <summary>
        /// Monta o Node Dep_Prazo_Tit
        /// </summary>
        /// <param name="dataPosicao"></param>
        /// <param name="arq"></param>
        /// <param name="idCarteira"></param>
        private void MontaDep_Prazo_Tit(DateTime dataPosicao, CDA_Xml arq, Carteira carteira) 
        {
            int idCarteira = carteira.IdCarteira.Value;
            DateTime dataInicioMes = Calendario.RetornaPrimeiroDiaCorridoMes(dataPosicao, 0);

            #region For Monta Titulos privados de instituição financeira (Compras Finais)
            #region Consulta
            PosicaoRendaFixaHistoricoQuery posicaoRendaFixaHistoricoQuery = new PosicaoRendaFixaHistoricoQuery("P");
            TituloRendaFixaQuery tituloRendaFixaQuery = new TituloRendaFixaQuery("T");
            PapelRendaFixaQuery papelRendaFixaQuery = new PapelRendaFixaQuery("A");
            EmissorQuery emissorQuery = new EmissorQuery("E");
            posicaoRendaFixaHistoricoQuery.Select(posicaoRendaFixaHistoricoQuery.IdTitulo,
                                                  posicaoRendaFixaHistoricoQuery.DataVencimento,
                                                  posicaoRendaFixaHistoricoQuery.TipoNegociacao,
                                                  papelRendaFixaQuery.TipoRentabilidade,
                                                  emissorQuery.IdEmissor,
                                                  tituloRendaFixaQuery.CodigoCDA,
                                                  tituloRendaFixaQuery.IdIndice,
                                                  tituloRendaFixaQuery.Percentual,
                                                  tituloRendaFixaQuery.Taxa,
                                                  posicaoRendaFixaHistoricoQuery.Quantidade.Sum(),
                                                  posicaoRendaFixaHistoricoQuery.ValorMercado.Sum());
            posicaoRendaFixaHistoricoQuery.InnerJoin(tituloRendaFixaQuery).On(tituloRendaFixaQuery.IdTitulo == posicaoRendaFixaHistoricoQuery.IdTitulo);
            posicaoRendaFixaHistoricoQuery.InnerJoin(papelRendaFixaQuery).On(papelRendaFixaQuery.IdPapel == tituloRendaFixaQuery.IdPapel);
            posicaoRendaFixaHistoricoQuery.InnerJoin(emissorQuery).On(emissorQuery.IdEmissor == tituloRendaFixaQuery.IdEmissor);
            posicaoRendaFixaHistoricoQuery.Where(posicaoRendaFixaHistoricoQuery.IdCliente.Equal(idCarteira),
                                        posicaoRendaFixaHistoricoQuery.DataHistorico.Equal(dataPosicao),
                                        posicaoRendaFixaHistoricoQuery.TipoOperacao.Equal((byte)TipoOperacaoTitulo.CompraFinal),
                                        tituloRendaFixaQuery.CodigoCDA.IsNotNull(),
                                        tituloRendaFixaQuery.CodigoCDA.NotEqual(0),
                                        papelRendaFixaQuery.TipoPapel.Equal((byte)TipoPapelTitulo.Privado),
                                        emissorQuery.TipoEmissor.Equal((byte)TipoEmissor.InstituicaoFinanceira));
            posicaoRendaFixaHistoricoQuery.GroupBy(posicaoRendaFixaHistoricoQuery.IdTitulo,
                                                  posicaoRendaFixaHistoricoQuery.DataVencimento,
                                                  posicaoRendaFixaHistoricoQuery.TipoNegociacao,
                                                  papelRendaFixaQuery.TipoRentabilidade,
                                                  emissorQuery.IdEmissor,
                                                  tituloRendaFixaQuery.CodigoCDA,
                                                  tituloRendaFixaQuery.IdIndice,
                                                  tituloRendaFixaQuery.Percentual,
                                                  tituloRendaFixaQuery.Taxa);

            PosicaoRendaFixaHistoricoCollection coll = new PosicaoRendaFixaHistoricoCollection();
            coll.Load(posicaoRendaFixaHistoricoQuery);            
            #endregion

            if (coll.Count > 0)
            {
                int i = 0;
                PosicaoRendaFixaHistorico p = coll[i];

                #region idTitulo/tipoRentabilidade/dataVencimento/tipoNegociacao/codigoCDA/idEmissor/idIndice/percentual/taxa
                int idTitulo = p.IdTitulo.Value;

                byte tipoRentabilidade = Convert.ToByte(p.GetColumn(PapelRendaFixaMetadata.ColumnNames.TipoRentabilidade));
                DateTime dataVencimento = p.DataVencimento.Value;
                byte tipoNegociacao = p.TipoNegociacao.Value;
                string codigoCDA = Convert.ToString(p.GetColumn(TituloRendaFixaMetadata.ColumnNames.CodigoCDA));
                int idEmissor = Convert.ToInt32(p.GetColumn(TituloRendaFixaMetadata.ColumnNames.IdEmissor));

                short? idIndice = null;
                decimal percentual = 0;
                decimal taxa = 0;
                if (tipoRentabilidade != (byte)TipoRentabilidadeTitulo.PreFixado)
                {
                    try
                    {
                        idIndice = Convert.ToInt16(p.GetColumn(TituloRendaFixaMetadata.ColumnNames.IdIndice));
                        percentual = Convert.ToDecimal(p.GetColumn(TituloRendaFixaMetadata.ColumnNames.Percentual));
                    }
                    catch (Exception)
                    {

                    }
                }
                else
                {
                    try
                    {
                        taxa = Convert.ToInt16(p.GetColumn(TituloRendaFixaMetadata.ColumnNames.Taxa));
                    }
                    catch (Exception)
                    {

                    }
                }
                #endregion

                string chave = idEmissor.ToString() + "|" + tipoRentabilidade.ToString() + "|" + dataVencimento.ToShortDateString() + "|" + tipoNegociacao.ToString() + "|" + codigoCDA + "|" +
                               percentual.ToString() + "|" + taxa.ToString();
                if (idIndice.HasValue)
                {
                    chave = chave + "|" + idIndice.Value.ToString();
                }

                int idEmissorAnterior = idEmissor;
                byte tipoRentabilidadeAnterior = tipoRentabilidade;
                DateTime dataVencimentoAnterior = dataVencimento;
                byte tipoNegociacaoAnterior = tipoNegociacao;
                string codigoCDAAnterior = codigoCDA;
                decimal percentualAnterior = percentual;
                decimal taxaAnterior = taxa;
                short? idIndiceAnterior = idIndice;

                while (i < coll.Count)
                {
                    string chaveAnterior = chave;

                    idEmissorAnterior = idEmissor;
                    tipoRentabilidadeAnterior = tipoRentabilidade;
                    dataVencimentoAnterior = dataVencimento;
                    tipoNegociacaoAnterior = tipoNegociacao;
                    codigoCDAAnterior = codigoCDA;
                    percentualAnterior = percentual;
                    taxaAnterior = taxa;
                    idIndiceAnterior = idIndice;

                    decimal totalQuantidade = 0, totalValor = 0, totalQuantidadeAplicacao = 0, totalValorAplicacao = 0, totalQuantidadeResgate = 0, totalValorResgate = 0;
                    while (chave == chaveAnterior)
                    {
                        totalQuantidade += p.Quantidade.Value;
                        totalValor += p.ValorMercado.Value;

                        #region Acumula qtdes e valores de operacao (somente tipoNegociacao = Vencimento)
                        if (tipoNegociacao != (byte)TipoNegociacaoTitulo.Vencimento) //Vencimento desconsidera de forma sumária qq movimentação, já que por definição não pode operar
                        {
                            OperacaoRendaFixa operacaoRendaFixa = new OperacaoRendaFixa();
                            operacaoRendaFixa.Query.Select(operacaoRendaFixa.Query.Quantidade.Sum(),
                                                           operacaoRendaFixa.Query.Valor.Sum());
                            operacaoRendaFixa.Query.Where(operacaoRendaFixa.Query.IdCliente.Equal(idCarteira),
                                                          operacaoRendaFixa.Query.IdTitulo.Equal(p.IdTitulo.Value),
                                                          operacaoRendaFixa.Query.DataOperacao.GreaterThanOrEqual(dataInicioMes),
                                                          operacaoRendaFixa.Query.DataOperacao.LessThanOrEqual(dataPosicao),
                                                          operacaoRendaFixa.Query.TipoOperacao.Equal((byte)TipoOperacaoTitulo.CompraFinal));
                            operacaoRendaFixa.Query.Load();

                            if (operacaoRendaFixa.Quantidade.HasValue)
                            {
                                decimal quantidadeAplicacao = operacaoRendaFixa.Quantidade.Value;
                                decimal valorAplicacao = operacaoRendaFixa.Valor.Value;

                                totalQuantidadeAplicacao += quantidadeAplicacao;
                                totalValorAplicacao += valorAplicacao;
                            }


                            operacaoRendaFixa = new OperacaoRendaFixa();
                            operacaoRendaFixa.Query.Select(operacaoRendaFixa.Query.Quantidade.Sum(),
                                                           operacaoRendaFixa.Query.Valor.Sum());
                            operacaoRendaFixa.Query.Where(operacaoRendaFixa.Query.IdCliente.Equal(idCarteira),
                                                          operacaoRendaFixa.Query.IdTitulo.Equal(p.IdTitulo.Value),
                                                          operacaoRendaFixa.Query.DataOperacao.GreaterThanOrEqual(dataInicioMes),
                                                          operacaoRendaFixa.Query.DataOperacao.LessThanOrEqual(dataPosicao),
                                                          operacaoRendaFixa.Query.TipoOperacao.In((byte)TipoOperacaoTitulo.VendaFinal,
                                                                                                  (byte)TipoOperacaoTitulo.VendaTotal));
                            operacaoRendaFixa.Query.Load();

                            if (operacaoRendaFixa.Quantidade.HasValue)
                            {
                                decimal quantidadeResgate = operacaoRendaFixa.Quantidade.Value;
                                decimal valorResgate = operacaoRendaFixa.Valor.Value;

                                totalQuantidadeResgate += quantidadeResgate;
                                totalValorResgate += valorResgate;
                            }
                        }
                        #endregion

                        i += 1;

                        if (i < coll.Count)
                        {
                            p = coll[i];

                            #region Valores para nova chave
                            idTitulo = p.IdTitulo.Value;

                            tipoRentabilidade = Convert.ToByte(p.GetColumn(PapelRendaFixaMetadata.ColumnNames.TipoRentabilidade));
                            dataVencimento = p.DataVencimento.Value;
                            tipoNegociacao = p.TipoNegociacao.Value;
                            codigoCDA = Convert.ToString(p.GetColumn(TituloRendaFixaMetadata.ColumnNames.CodigoCDA));
                            idEmissor = Convert.ToInt32(p.GetColumn(TituloRendaFixaMetadata.ColumnNames.IdEmissor));

                            idIndice = null;
                            percentual = 0;
                            taxa = 0;
                            if (tipoRentabilidade != (byte)TipoRentabilidadeTitulo.PreFixado)
                            {
                                try
                                {
                                    idIndice = Convert.ToInt16(p.GetColumn(TituloRendaFixaMetadata.ColumnNames.IdIndice));
                                    percentual = Convert.ToDecimal(p.GetColumn(TituloRendaFixaMetadata.ColumnNames.Percentual));
                                }
                                catch (Exception) { }
                            }
                            else
                            {
                                try
                                {
                                    taxa = Convert.ToInt16(p.GetColumn(TituloRendaFixaMetadata.ColumnNames.Taxa));
                                }
                                catch (Exception) { }
                            }
                            #endregion

                            chave = idEmissor.ToString() + "|" + tipoRentabilidade.ToString() + "|" + dataVencimento.ToShortDateString() + "|" + tipoNegociacao.ToString() + "|" + codigoCDA + "|" +
                                    percentual.ToString() + "|" + taxa.ToString();
                            if (idIndice.HasValue)
                            {
                                chave = chave + "|" + idIndice.Value.ToString();
                            }
                        }
                        else
                        {
                            break;
                        }
                    }

                    #region Preenche Dep_Prazo_Tit
                    Dep_Prazo_Tit d1 = new Dep_Prazo_Tit();

                    d1.getAplic.cod_Tp_Aplic = Convert.ToString((int)TipoAplicacaoCDA.DepositoPrazo_OutrosTitulosIF);
                    d1.cod_Tp_Ativ = codigoCDAAnterior;

                    // Dep_Prazo_Tit - Emis
                    Emissor emissor = new Emissor();
                    emissor.LoadByPrimaryKey(idEmissorAnterior);

                    d1.getEmis.cnpj = !String.IsNullOrEmpty(emissor.Cnpj) ? Utilitario.RemoveCaracteresEspeciais(emissor.Cnpj) : "";
                    d1.getEmis.nm = emissor.Nome.Trim();

                    bool tituloPosFixado = tipoRentabilidadeAnterior != (byte)TipoRentabilidadeTitulo.PreFixado;
                    d1.dataVencimento = dataVencimentoAnterior;
                    d1.tipoTitulo = tituloPosFixado ? "S" : "N";

                    // Dep_Prazo_Tit - TIT_POS_FIX_INDEX
                    if (tituloPosFixado)
                    {
                        d1.getTit_Pos_Fix_Index.codigoIndexador = this.RetornaTipoRentabilidadeCDA(idIndiceAnterior.Value);
                        d1.getTit_Pos_Fix_Index.porcentagemIndexador = percentualAnterior.ToString();
                        d1.getTit_Pos_Fix_Index.cupom = taxaAnterior.ToString();
                    }
                    else
                    {
                        d1.taxaTitulo = taxaAnterior;
                        d1.classificacaoRisco = "N";
                    }
                    //
                    //d1.getClassif_Risco.agenciaClassificadoraRisco = String.Empty;
                    //d1.getClassif_Risco.dataClassificacaoRisco = null;
                    //d1.getClassif_Risco.grauRisco = 0;
                    //
                    // Dep_Prazo_Tit - Aplic
                    bool empresaLigada = this.IndicaEmpresaLigadaTitulo(idEmissorAnterior, carteira.IdAgenteAdministrador.Value, carteira.IdAgenteGestor.Value);

                    d1.getAplic.empr_Ligada = empresaLigada ? "S" : "N";
                    d1.getAplic.qtde_Dias_Confid = this.diasConfidencialidade.ToString();

                    // Aplic - Negoc
                    d1.getAplic.getAplicNegoc.tipoNegocio = p.TipoNegociacao.Value == (byte)TipoNegociacaoTitulo.Vencimento ? "2" : "1";
                    d1.getAplic.getAplicNegoc.quantidadeVendasNegocio = totalQuantidadeResgate;
                    d1.getAplic.getAplicNegoc.valorVendasNegocio = totalValorResgate;
                    d1.getAplic.getAplicNegoc.quantidadeAquisicaoNegocio = totalQuantidadeAplicacao;
                    d1.getAplic.getAplicNegoc.valorAquisicaoNegocio = totalValorAplicacao;

                    // Aplic - Pos_Fim
                    d1.getAplic.aplic_Pos_Fim.VersaoCDA("3");
                    d1.getAplic.getAplicPosFim.qtde_Pos_Fim = totalQuantidade;
                    d1.getAplic.getAplicPosFim.merc_Pos_Fim = totalValor;

                    // Adiciona Dep_Prazo_Tit
                    arq.getCorpo.getInform.getListAtiv.getDep_Prazo_Tit.Add(d1);
                    // -------------------------------------------------
                    #endregion
                }
            }
            #endregion            
        }
        #endregion

        #region Monta o Node Tit_Agro_Cred_Priv
        /// <summary>
        /// Monta o Node Tit_Agro_Cred_Priv
        /// </summary>
        /// <param name="dataPosicao"></param>
        /// <param name="arq"></param>
        /// <param name="idCarteira"></param>
        private void MontaTituloAgronegocio_CreditoPrivado(DateTime dataPosicao, CDA_Xml arq, Carteira carteira) 
        {
            int idCarteira = carteira.IdCarteira.Value;
            DateTime dataInicioMes = Calendario.RetornaPrimeiroDiaCorridoMes(dataPosicao, 0);

            #region For Monta Titulos privados do agronegócio e outros de crédito privado
            #region Consulta
            PosicaoRendaFixaHistoricoQuery posicaoRendaFixaHistoricoQuery = new PosicaoRendaFixaHistoricoQuery("P");
            TituloRendaFixaQuery tituloRendaFixaQuery = new TituloRendaFixaQuery("T");
            PapelRendaFixaQuery papelRendaFixaQuery = new PapelRendaFixaQuery("A");
            EmissorQuery emissorQuery = new EmissorQuery("E");
            posicaoRendaFixaHistoricoQuery.Select(posicaoRendaFixaHistoricoQuery.IdTitulo,
                                                  posicaoRendaFixaHistoricoQuery.DataVencimento,
                                                  posicaoRendaFixaHistoricoQuery.TipoNegociacao,
                                                  papelRendaFixaQuery.TipoRentabilidade,
                                                  papelRendaFixaQuery.Classe,
                                                  emissorQuery.IdEmissor,
                                                  tituloRendaFixaQuery.CodigoCDA,
                                                  tituloRendaFixaQuery.IdIndice,
                                                  tituloRendaFixaQuery.Percentual,
                                                  tituloRendaFixaQuery.Taxa,
                                                  posicaoRendaFixaHistoricoQuery.Quantidade.Sum(),
                                                  posicaoRendaFixaHistoricoQuery.ValorMercado.Sum());
            posicaoRendaFixaHistoricoQuery.InnerJoin(tituloRendaFixaQuery).On(tituloRendaFixaQuery.IdTitulo == posicaoRendaFixaHistoricoQuery.IdTitulo);
            posicaoRendaFixaHistoricoQuery.InnerJoin(papelRendaFixaQuery).On(papelRendaFixaQuery.IdPapel == tituloRendaFixaQuery.IdPapel);
            posicaoRendaFixaHistoricoQuery.InnerJoin(emissorQuery).On(emissorQuery.IdEmissor == tituloRendaFixaQuery.IdEmissor);
            posicaoRendaFixaHistoricoQuery.Where(posicaoRendaFixaHistoricoQuery.IdCliente.Equal(idCarteira),
                                        posicaoRendaFixaHistoricoQuery.DataHistorico.Equal(dataPosicao),
                                        posicaoRendaFixaHistoricoQuery.TipoOperacao.Equal((byte)TipoOperacaoTitulo.CompraFinal),
                                        tituloRendaFixaQuery.CodigoCDA.IsNotNull(),
                                        tituloRendaFixaQuery.CodigoCDA.NotEqual(0),
                                        papelRendaFixaQuery.TipoPapel.Equal((byte)TipoPapelTitulo.Privado),
                                        emissorQuery.TipoEmissor.NotEqual((byte)TipoEmissor.InstituicaoFinanceira));
            posicaoRendaFixaHistoricoQuery.GroupBy(posicaoRendaFixaHistoricoQuery.IdTitulo,
                                                  posicaoRendaFixaHistoricoQuery.DataVencimento,
                                                  posicaoRendaFixaHistoricoQuery.TipoNegociacao,
                                                  papelRendaFixaQuery.TipoRentabilidade,
                                                  papelRendaFixaQuery.Classe,
                                                  emissorQuery.IdEmissor,
                                                  tituloRendaFixaQuery.CodigoCDA,
                                                  tituloRendaFixaQuery.IdIndice,
                                                  tituloRendaFixaQuery.Percentual,
                                                  tituloRendaFixaQuery.Taxa);

            PosicaoRendaFixaHistoricoCollection coll = new PosicaoRendaFixaHistoricoCollection();
            coll.Load(posicaoRendaFixaHistoricoQuery);
            #endregion

            if (coll.Count > 0)
            {
                int i = 0;
                PosicaoRendaFixaHistorico p = coll[i];

                #region idTitulo/classe/tipoRentabilidade/dataVencimento/tipoNegociacao/codigoCDA/idEmissor/idIndice/percentual/taxa
                int idTitulo = p.IdTitulo.Value;

                int classe = Convert.ToInt32(p.GetColumn(PapelRendaFixaMetadata.ColumnNames.Classe));
                byte tipoRentabilidade = Convert.ToByte(p.GetColumn(PapelRendaFixaMetadata.ColumnNames.TipoRentabilidade));
                DateTime dataVencimento = p.DataVencimento.Value;
                byte tipoNegociacao = p.TipoNegociacao.Value;
                string codigoCDA = Convert.ToString(p.GetColumn(TituloRendaFixaMetadata.ColumnNames.CodigoCDA));
                int idEmissor = Convert.ToInt32(p.GetColumn(TituloRendaFixaMetadata.ColumnNames.IdEmissor));

                short? idIndice = null;
                decimal percentual = 0;
                decimal taxa = 0;
                if (tipoRentabilidade != (byte)TipoRentabilidadeTitulo.PreFixado)
                {
                    try
                    {
                        idIndice = Convert.ToInt16(p.GetColumn(TituloRendaFixaMetadata.ColumnNames.IdIndice));
                        percentual = Convert.ToDecimal(p.GetColumn(TituloRendaFixaMetadata.ColumnNames.Percentual));
                    }
                    catch (Exception)
                    {

                    }
                }
                else
                {
                    try
                    {
                        taxa = Convert.ToInt16(p.GetColumn(TituloRendaFixaMetadata.ColumnNames.Taxa));
                    }
                    catch (Exception)
                    {

                    }
                }
                #endregion

                string chave = classe.ToString() + "|" + idEmissor.ToString() + "|" + tipoRentabilidade.ToString() + "|" + dataVencimento.ToShortDateString() + "|" + 
                               tipoNegociacao.ToString() + "|" + codigoCDA + "|" + percentual.ToString() + "|" + taxa.ToString();
                if (idIndice.HasValue)
                {
                    chave = chave + "|" + idIndice.Value.ToString();
                }

                int classeAnterior = classe;
                int idEmissorAnterior = idEmissor;
                byte tipoRentabilidadeAnterior = tipoRentabilidade;
                DateTime dataVencimentoAnterior = dataVencimento;
                byte tipoNegociacaoAnterior = tipoNegociacao;
                string codigoCDAAnterior = codigoCDA;
                decimal percentualAnterior = percentual;
                decimal taxaAnterior = taxa;
                short? idIndiceAnterior = idIndice;

                while (i < coll.Count)
                {
                    string chaveAnterior = chave;

                    classeAnterior = classe;
                    idEmissorAnterior = idEmissor;
                    tipoRentabilidadeAnterior = tipoRentabilidade;
                    dataVencimentoAnterior = dataVencimento;
                    tipoNegociacaoAnterior = tipoNegociacao;
                    codigoCDAAnterior = codigoCDA;
                    percentualAnterior = percentual;
                    taxaAnterior = taxa;
                    idIndiceAnterior = idIndice;

                    decimal totalQuantidade = 0, totalValor = 0, totalQuantidadeAplicacao = 0, totalValorAplicacao = 0, totalQuantidadeResgate = 0, totalValorResgate = 0;
                    while (chave == chaveAnterior)
                    {
                        totalQuantidade += p.Quantidade.Value;
                        totalValor += p.ValorMercado.Value;

                        #region Acumula qtdes e valores de operacao (somente tipoNegociacao = Vencimento)
                        if (tipoNegociacao != (byte)TipoNegociacaoTitulo.Vencimento) //Vencimento desconsidera de forma sumária qq movimentação, já que por definição não pode operar
                        {
                            OperacaoRendaFixa operacaoRendaFixa = new OperacaoRendaFixa();
                            operacaoRendaFixa.Query.Select(operacaoRendaFixa.Query.Quantidade.Sum(),
                                                           operacaoRendaFixa.Query.Valor.Sum());
                            operacaoRendaFixa.Query.Where(operacaoRendaFixa.Query.IdCliente.Equal(idCarteira),
                                                          operacaoRendaFixa.Query.IdTitulo.Equal(p.IdTitulo.Value),
                                                          operacaoRendaFixa.Query.DataOperacao.GreaterThanOrEqual(dataInicioMes),
                                                          operacaoRendaFixa.Query.DataOperacao.LessThanOrEqual(dataPosicao),
                                                          operacaoRendaFixa.Query.TipoOperacao.Equal((byte)TipoOperacaoTitulo.CompraFinal));
                            operacaoRendaFixa.Query.Load();

                            if (operacaoRendaFixa.Quantidade.HasValue)
                            {
                                decimal quantidadeAplicacao = operacaoRendaFixa.Quantidade.Value;
                                decimal valorAplicacao = operacaoRendaFixa.Valor.Value;

                                totalQuantidadeAplicacao += quantidadeAplicacao;
                                totalValorAplicacao += valorAplicacao;
                            }


                            operacaoRendaFixa = new OperacaoRendaFixa();
                            operacaoRendaFixa.Query.Select(operacaoRendaFixa.Query.Quantidade.Sum(),
                                                           operacaoRendaFixa.Query.Valor.Sum());
                            operacaoRendaFixa.Query.Where(operacaoRendaFixa.Query.IdCliente.Equal(idCarteira),
                                                          operacaoRendaFixa.Query.IdTitulo.Equal(p.IdTitulo.Value),
                                                          operacaoRendaFixa.Query.DataOperacao.GreaterThanOrEqual(dataInicioMes),
                                                          operacaoRendaFixa.Query.DataOperacao.LessThanOrEqual(dataPosicao),
                                                          operacaoRendaFixa.Query.TipoOperacao.In((byte)TipoOperacaoTitulo.VendaFinal,
                                                                                                  (byte)TipoOperacaoTitulo.VendaTotal));
                            operacaoRendaFixa.Query.Load();

                            if (operacaoRendaFixa.Quantidade.HasValue)
                            {
                                decimal quantidadeResgate = operacaoRendaFixa.Quantidade.Value;
                                decimal valorResgate = operacaoRendaFixa.Valor.Value;

                                totalQuantidadeResgate += quantidadeResgate;
                                totalValorResgate += valorResgate;
                            }
                        }
                        #endregion

                        i += 1;

                        if (i < coll.Count)
                        {
                            p = coll[i];

                            #region Valores para nova chave
                            idTitulo = p.IdTitulo.Value;

                            tipoRentabilidade = Convert.ToByte(p.GetColumn(PapelRendaFixaMetadata.ColumnNames.TipoRentabilidade));
                            dataVencimento = p.DataVencimento.Value;
                            tipoNegociacao = p.TipoNegociacao.Value;
                            codigoCDA = Convert.ToString(p.GetColumn(TituloRendaFixaMetadata.ColumnNames.CodigoCDA));
                            idEmissor = Convert.ToInt32(p.GetColumn(TituloRendaFixaMetadata.ColumnNames.IdEmissor));

                            idIndice = null;
                            percentual = 0;
                            taxa = 0;
                            if (tipoRentabilidade != (byte)TipoRentabilidadeTitulo.PreFixado)
                            {
                                try
                                {
                                    idIndice = Convert.ToInt16(p.GetColumn(TituloRendaFixaMetadata.ColumnNames.IdIndice));
                                    percentual = Convert.ToDecimal(p.GetColumn(TituloRendaFixaMetadata.ColumnNames.Percentual));
                                }
                                catch (Exception) { }
                            }
                            else
                            {
                                try
                                {
                                    taxa = Convert.ToInt16(p.GetColumn(TituloRendaFixaMetadata.ColumnNames.Taxa));
                                }
                                catch (Exception) { }
                            }
                            #endregion

                            chave = classe.ToString() + "|" + idEmissor.ToString() + "|" + tipoRentabilidade.ToString() + "|" + dataVencimento.ToShortDateString() + "|" +
                                    tipoNegociacao.ToString() + "|" + codigoCDA + "|" + percentual.ToString() + "|" + taxa.ToString();
                            if (idIndice.HasValue)
                            {
                                chave = chave + "|" + idIndice.Value.ToString();
                            }
                        }
                        else
                        {
                            break;
                        }
                    }

                    #region Preenche TituloAgronegocioCreditoPrivado
                    TituloAgronegocioCreditoPrivado t1 = new TituloAgronegocioCreditoPrivado();

                    if (codigoCDAAnterior == Convert.ToString((int)TipoAtivoCDA.DebentureConversivel) || 
                        codigoCDAAnterior == Convert.ToString((int)TipoAtivoCDA.DebentureSimples) ||
                        classeAnterior == (int)ClasseRendaFixa.Debenture)
                    {
                        t1.getAplic.cod_Tp_Aplic = Convert.ToString((int)TipoAplicacaoCDA.Debentures);
                    }
                    else if (codigoCDAAnterior == Convert.ToString((int)TipoAtivoCDA.CPR) ||
                             codigoCDAAnterior == Convert.ToString((int)TipoAtivoCDA.LCA) ||
                             codigoCDAAnterior == Convert.ToString((int)TipoAtivoCDA.CRA) ||
                             codigoCDAAnterior == Convert.ToString((int)TipoAtivoCDA.CDCA) ||
                             codigoCDAAnterior == Convert.ToString((int)TipoAtivoCDA.WA) ||
                             classeAnterior == (int)ClasseRendaFixa.LCA ||
                             classeAnterior == (int)ClasseRendaFixa.CDA ||
                             classeAnterior == (int)ClasseRendaFixa.CDCA ||
                             classeAnterior == (int)ClasseRendaFixa.CRA ||
                             classeAnterior == (int)ClasseRendaFixa.CPR)
                    {
                        t1.getAplic.cod_Tp_Aplic = Convert.ToString((int)TipoAplicacaoCDA.TitulosAgronegocio);
                    }
                    else
                    {
                        t1.getAplic.cod_Tp_Aplic = Convert.ToString((int)TipoAplicacaoCDA.TitulosCreditoPrivado);
                    }

                    t1.cod_Tp_Ativ = codigoCDAAnterior;

                    // Dep_Prazo_Tit - Emis
                    Emissor emissor = new Emissor();
                    emissor.LoadByPrimaryKey(idEmissorAnterior);

                    // Tit_Agro_Cred_Priv - Emis
                    t1.getEmis.tp_PF_PJ = "PJ";
                    t1.getEmis.nr_PF_PJ = !String.IsNullOrEmpty(emissor.Cnpj) ? Utilitario.RemoveCaracteresEspeciais(emissor.Cnpj) : "";
                    t1.getEmis.nm = emissor.Nome.Trim();

                    bool tituloPosFixado = tipoRentabilidadeAnterior != (byte)TipoRentabilidadeTitulo.PreFixado;
                    t1.dataVencimento = dataVencimentoAnterior;
                    t1.tipoTitulo = tituloPosFixado ? "S" : "N";

                    // Dep_Prazo_Tit - TIT_POS_FIX_INDEX
                    if (tituloPosFixado && idIndiceAnterior.HasValue)
                    {
                        t1.getTit_Pos_Fix_Index.codigoIndexador = this.RetornaTipoRentabilidadeCDA(idIndiceAnterior.Value);
                        t1.getTit_Pos_Fix_Index.porcentagemIndexador = percentualAnterior.ToString();
                        t1.getTit_Pos_Fix_Index.cupom = taxaAnterior.ToString();
                    }
                    else
                    {
                        t1.taxaTitulo = taxaAnterior;
                    }
                    //
                    //
                    t1.IstituloRegistradoCetip = "S"; //FORÇADO
                    t1.IstituloGarantiaSeguro = "N"; //FORÇADO
                    //t1.cnpjInstituicao = "CNPJ"; //HOJE NÃO TEM COMO SABER SE TEM COOBRIGAÇÃO
                    //
                    // Dep_Prazo_Tit - Aplic
                    bool empresaLigada = this.IndicaEmpresaLigadaTitulo(idEmissorAnterior, carteira.IdAgenteAdministrador.Value, carteira.IdAgenteGestor.Value);

                    t1.getAplic.empr_Ligada = empresaLigada ? "S" : "N";
                    t1.getAplic.qtde_Dias_Confid = this.diasConfidencialidade.ToString();

                    // Aplic - Negoc
                    t1.getAplic.getAplicNegoc.tipoNegocio = p.TipoNegociacao.Value == (byte)TipoNegociacaoTitulo.Vencimento ? "2" : "1";
                    t1.getAplic.getAplicNegoc.quantidadeVendasNegocio = totalQuantidadeResgate;
                    t1.getAplic.getAplicNegoc.valorVendasNegocio = totalValorResgate;
                    t1.getAplic.getAplicNegoc.quantidadeAquisicaoNegocio = totalQuantidadeAplicacao;
                    t1.getAplic.getAplicNegoc.valorAquisicaoNegocio = totalValorAplicacao;

                    // Aplic - Pos_Fim
                    t1.getAplic.aplic_Pos_Fim.VersaoCDA("3");
                    t1.getAplic.getAplicPosFim.qtde_Pos_Fim = totalQuantidade;
                    t1.getAplic.getAplicPosFim.merc_Pos_Fim = totalValor;

                    // Adiciona Dep_Prazo_Tit
                    arq.getCorpo.getInform.getListAtiv.getTituloAgronegocioCreditoPrivado.Add(t1);

                    // -------------------------------------------------
                    #endregion
                }
            }
            #endregion            
            
            // -------------------------------------------------
        }
        #endregion

        #region Monta o Node Investimento Exterior
        /// <summary>
        /// Monta o Node InvestimentoExterior
        /// </summary>
        /// <param name="dataPosicao"></param>
        /// <param name="arq"></param>
        /// <param name="idCarteira"></param>
        private void MontaInvestimentoExterior(DateTime dataPosicao, CDA_Xml arq, Carteira carteira) 
        {
            int idCarteira = carteira.IdCarteira.Value;
            DateTime dataInicioMes = Calendario.RetornaPrimeiroDiaCorridoMes(dataPosicao, 0);

            CarteiraQuery carteiraQuery = new CarteiraQuery("C");
            ClienteQuery clienteQuery = new ClienteQuery("L");
            PosicaoFundoHistoricoQuery posicaoFundoHistoricoQuery = new PosicaoFundoHistoricoQuery("P");

            PosicaoFundoHistoricoCollection coll = new PosicaoFundoHistoricoCollection();
            posicaoFundoHistoricoQuery.Select(posicaoFundoHistoricoQuery.IdCarteira,
                                              clienteQuery.IdMoeda,
                                              clienteQuery.Apelido,
                                              clienteQuery.Nome,
                                              posicaoFundoHistoricoQuery.Quantidade.Sum(),
                                              posicaoFundoHistoricoQuery.ValorBruto.Sum());
            posicaoFundoHistoricoQuery.InnerJoin(carteiraQuery).On(carteiraQuery.IdCarteira == posicaoFundoHistoricoQuery.IdCarteira);
            posicaoFundoHistoricoQuery.InnerJoin(clienteQuery).On(clienteQuery.IdCliente == carteiraQuery.IdCarteira);
            posicaoFundoHistoricoQuery.Where(posicaoFundoHistoricoQuery.IdCliente.Equal(idCarteira),
                                             posicaoFundoHistoricoQuery.DataHistorico.Equal(dataPosicao),
                                             posicaoFundoHistoricoQuery.Quantidade.NotEqual(0),
                                             clienteQuery.IdMoeda.NotEqual((int)ListaMoedaFixo.Real));
            posicaoFundoHistoricoQuery.GroupBy(posicaoFundoHistoricoQuery.IdCarteira,
                                               clienteQuery.IdMoeda,
                                               clienteQuery.Apelido,
                                               clienteQuery.Nome);
            coll.Load(posicaoFundoHistoricoQuery);

            foreach (PosicaoFundoHistorico posicaoFundoHistorico in coll)
            {
                int idFundo = posicaoFundoHistorico.IdCarteira.Value;
                int idMoeda = Convert.ToInt32(posicaoFundoHistorico.GetColumn(ClienteMetadata.ColumnNames.IdMoeda));

                ConversaoMoeda conversaoMoeda = new ConversaoMoeda();
                decimal fatorConversao = conversaoMoeda.RetornaFatorConversao((int)ListaMoedaFixo.Real, idMoeda, dataPosicao);

                string apelido = Convert.ToString(posicaoFundoHistorico.GetColumn(ClienteMetadata.ColumnNames.Apelido));
                string nome = Convert.ToString(posicaoFundoHistorico.GetColumn(ClienteMetadata.ColumnNames.Nome));
                decimal quantidadeFinal = posicaoFundoHistorico.Quantidade.Value;
                decimal valorFinal = Math.Round(posicaoFundoHistorico.ValorBruto.Value * fatorConversao, 2);

                OperacaoFundo operacaoFundo = new OperacaoFundo();
                operacaoFundo.Query.Select(operacaoFundo.Query.Quantidade.Sum(),
                                           operacaoFundo.Query.ValorBruto.Sum());
                operacaoFundo.Query.Where(operacaoFundo.Query.IdCliente.Equal(idCarteira),
                                          operacaoFundo.Query.IdCarteira.Equal(idFundo),
                                          operacaoFundo.Query.DataOperacao.GreaterThanOrEqual(dataInicioMes),
                                          operacaoFundo.Query.DataOperacao.LessThanOrEqual(dataPosicao),
                                          operacaoFundo.Query.TipoOperacao.Equal((byte)TipoOperacaoFundo.Aplicacao));
                operacaoFundo.Query.Load();

                decimal quantidadeAplicacao = 0;
                decimal valorAplicacao = 0;
                if (operacaoFundo.Quantidade.HasValue)
                {
                    quantidadeAplicacao = operacaoFundo.Quantidade.Value;
                    valorAplicacao = Math.Round(operacaoFundo.ValorBruto.Value * fatorConversao, 2);
                }

                operacaoFundo = new OperacaoFundo();
                operacaoFundo.Query.Select(operacaoFundo.Query.Quantidade.Sum(),
                                           operacaoFundo.Query.ValorBruto.Sum());
                operacaoFundo.Query.Where(operacaoFundo.Query.IdCliente.Equal(idCarteira),
                                          operacaoFundo.Query.IdCarteira.Equal(idFundo),
                                          operacaoFundo.Query.DataOperacao.GreaterThanOrEqual(dataInicioMes),
                                          operacaoFundo.Query.DataOperacao.LessThanOrEqual(dataPosicao),
                                          operacaoFundo.Query.TipoOperacao.NotEqual((byte)TipoOperacaoFundo.Aplicacao));
                operacaoFundo.Query.Load();

                decimal quantidadeResgate = 0;
                decimal valorResgate = 0;
                if (operacaoFundo.Quantidade.HasValue)
                {
                    quantidadeResgate = operacaoFundo.Quantidade.Value;
                    valorResgate = Math.Round(operacaoFundo.ValorBruto.Value * fatorConversao, 2);
                }

                InvestimentoExterior i1 = new InvestimentoExterior();

                i1.cod_Tp_Ativ = Convert.ToString((int)TipoAtivoCDA.Outros);
                i1.nomeEmissor = nome;

                i1.dataVencimento = null;
                //
                i1.codigoPais = Convert.ToString(PaisCDA.ESTADOS_UNIDOS);
                i1.codigoBolsa = CodigoBolsaCDA.Outras_bolsas_ou_mercados_de_balcaoorganizados;
                i1.codigoAtivo = apelido;
                i1.classificacaoRisco = "N";

                //
                i1.getClassif_Risco.agenciaClassificadoraRisco = String.Empty;

                i1.getClassif_Risco.dataClassificacaoRisco = null;
                i1.getClassif_Risco.grauRisco = null;
                //
                // InvestimentoExterior - Aplic
                i1.getAplic.empr_Ligada = "N";
                i1.getAplic.qtde_Dias_Confid = this.diasConfidencialidade.ToString();

                // Aplic - Negoc
                i1.getAplic.getAplicNegoc.tipoNegocio = Convert.ToString(TipoNegociacaoCDA.Negociacao);
                i1.getAplic.getAplicNegoc.quantidadeVendasNegocio = quantidadeResgate;
                i1.getAplic.getAplicNegoc.valorVendasNegocio = valorResgate;
                i1.getAplic.getAplicNegoc.quantidadeAquisicaoNegocio = quantidadeAplicacao;
                i1.getAplic.getAplicNegoc.valorAquisicaoNegocio = valorAplicacao;

                // Aplic - Pos_Fim
                i1.getAplic.aplic_Pos_Fim.VersaoCDA("3");
                i1.getAplic.getAplicPosFim.qtde_Pos_Fim = quantidadeFinal;
                i1.getAplic.getAplicPosFim.merc_Pos_Fim = valorFinal;

                // Adiciona Dep_Prazo_Tit
                arq.getCorpo.getInform.getListAtiv.getInvestimentoExterior.Add(i1);
                // -------------------------------------------------                
            }            
        }
        #endregion

        #region Monta o Node Demais_N_Codif
        /// <summary>
        /// Monta o Node Demais_N_Codif
        /// </summary>
        /// <param name="dataPosicao"></param>
        /// <param name="arq"></param>
        /// <param name="idCarteira"></param>
        private void MontaDemais_N_Codif(DateTime dataPosicao, CDA_Xml arq, Carteira carteira) 
        {
            int idCarteira = carteira.IdCarteira.Value;
            DateTime dataInicioMes = Calendario.RetornaPrimeiroDiaCorridoMes(dataPosicao, 0);

            DateTime dataIndefinida = new DateTime(4000, 01, 01);

            #region For Monta Objeto (Disponibilidade) em "Demais_N_Codif"
            decimal caixa = 0;
            SaldoCaixa saldoCaixa = new SaldoCaixa();
            saldoCaixa.Query.Select(saldoCaixa.Query.SaldoFechamento.Sum());
            saldoCaixa.Query.Where(saldoCaixa.Query.IdCliente.Equal(idCarteira),
                                   saldoCaixa.Query.Data.Equal(dataPosicao));
            saldoCaixa.Query.Load();

            if (saldoCaixa.SaldoFechamento.HasValue) 
            {
                caixa = saldoCaixa.SaldoFechamento.Value;
            }

            Demais_N_Codif d = new Demais_N_Codif();
            d.cod_Tp_Ativ = Convert.ToString((int)TipoAtivoCDA.Outros);
            d.descricao = "Disponibilidade";

            // Demais_N_Codif - Emis
            d.getEmis.tp_PF_PJ = string.Empty; // Colocar empty para aparecer nó vazio
            d.getEmis.nr_PF_PJ = string.Empty;
            d.getEmis.nm = string.Empty;

            // Demais_N_Codif - Aplic
            d.getAplic.cod_Tp_Aplic = Convert.ToString((int)TipoAplicacaoCDA.Disponibilidades);
            d.getAplic.empr_Ligada = String.Empty;
            d.getAplic.qtde_Dias_Confid = this.diasConfidencialidade.ToString();

            // Demais_N_Codif - Aplic - Negoc
            d.getAplic.getAplicNegoc.tipoNegocio = Convert.ToString((int)TipoNegociacaoCDA.Negociacao);

            // Demais_N_Codif - Aplic - Pos_FIM
            d.getAplic.aplic_Pos_Fim.VersaoCDA("3");
            d.getAplic.getAplicPosFim.qtde_Pos_Fim = null;
            d.getAplic.getAplicPosFim.merc_Pos_Fim = caixa;

            // Adiciona Demais_N_Codif
            arq.getCorpo.getInform.getListAtiv.getDemais_N_Codif.Add(d);
            #endregion

            #region For Monta Objeto (Valores a Pagar) em "Demais_N_Codif"
            decimal valorPagar = 0;
            LiquidacaoHistorico liquidacaoHistorico = new LiquidacaoHistorico();
            liquidacaoHistorico.Query.Select(liquidacaoHistorico.Query.Valor.Sum());
            liquidacaoHistorico.Query.Where(liquidacaoHistorico.Query.IdCliente.Equal(idCarteira),
                                   liquidacaoHistorico.Query.DataVencimento.GreaterThan(dataPosicao),
                                   liquidacaoHistorico.Query.DataLancamento.LessThanOrEqual(dataPosicao),
                                   liquidacaoHistorico.Query.Valor.LessThan(0),
                                   liquidacaoHistorico.Query.DataHistorico.Equal(dataPosicao));

            liquidacaoHistorico.Query.Load();

            if (liquidacaoHistorico.Valor.HasValue) {
                valorPagar = liquidacaoHistorico.Valor.Value;
            }

            d = new Demais_N_Codif();
            d.cod_Tp_Ativ = Convert.ToString((int)TipoAtivoCDA.Outros);
            d.descricao = "Valores a Pagar";

            // Demais_N_Codif - Emis
            d.getEmis.tp_PF_PJ = string.Empty; // Colocar empty para aparecer nó vazio
            d.getEmis.nr_PF_PJ = string.Empty;
            d.getEmis.nm = string.Empty;

            // Demais_N_Codif - Aplic
            d.getAplic.cod_Tp_Aplic = Convert.ToString((int)TipoAplicacaoCDA.ValoresPagar);
            d.getAplic.empr_Ligada = String.Empty;
            d.getAplic.qtde_Dias_Confid = this.diasConfidencialidade.ToString();

            // Demais_N_Codif - Aplic - Negoc
            d.getAplic.getAplicNegoc.tipoNegocio = Convert.ToString((int)TipoNegociacaoCDA.Negociacao);

            // Demais_N_Codif - Aplic - Pos_FIM
            d.getAplic.aplic_Pos_Fim.VersaoCDA("3");
            d.getAplic.getAplicPosFim.qtde_Pos_Fim = null;
            d.getAplic.getAplicPosFim.custo_Correc_Pos_Fim = null;
            d.getAplic.getAplicPosFim.merc_Pos_Fim = valorPagar;

            // Adiciona Demais_N_Codif
            arq.getCorpo.getInform.getListAtiv.getDemais_N_Codif.Add(d);
            #endregion

            #region For Monta Objeto (Valores a Receber) em "Demais_N_Codif"
            decimal valorReceber = 0;
            liquidacaoHistorico = new LiquidacaoHistorico();
            liquidacaoHistorico.Query.Select(liquidacaoHistorico.Query.Valor.Sum());
            liquidacaoHistorico.Query.Where(liquidacaoHistorico.Query.IdCliente.Equal(idCarteira),
                                   liquidacaoHistorico.Query.DataVencimento.GreaterThan(dataPosicao),
                                   liquidacaoHistorico.Query.DataLancamento.LessThanOrEqual(dataPosicao),
                                   liquidacaoHistorico.Query.Valor.GreaterThan(0),
                                   liquidacaoHistorico.Query.DataHistorico.Equal(dataPosicao));

            liquidacaoHistorico.Query.Load();

            if (liquidacaoHistorico.Valor.HasValue) {
                valorReceber = liquidacaoHistorico.Valor.Value;
            }

            d = new Demais_N_Codif();
            d.cod_Tp_Ativ = Convert.ToString((int)TipoAtivoCDA.Outros);
            d.descricao = "Valores a Receber";

            // Demais_N_Codif - Emis
            d.getEmis.tp_PF_PJ = string.Empty; // Colocar empty para aparecer nó vazio
            d.getEmis.nr_PF_PJ = string.Empty;
            d.getEmis.nm = string.Empty;

            // Demais_N_Codif - Aplic
            d.getAplic.cod_Tp_Aplic = Convert.ToString((int)TipoAplicacaoCDA.ValoresReceber);
            d.getAplic.empr_Ligada = String.Empty;
            d.getAplic.qtde_Dias_Confid = this.diasConfidencialidade.ToString();

            // Demais_N_Codif - Aplic - Negoc
            d.getAplic.getAplicNegoc.tipoNegocio = Convert.ToString((int)TipoNegociacaoCDA.Negociacao);

            // Demais_N_Codif - Aplic - Pos_FIM
            d.getAplic.aplic_Pos_Fim.VersaoCDA("3");
            d.getAplic.getAplicPosFim.qtde_Pos_Fim = null;
            d.getAplic.getAplicPosFim.custo_Correc_Pos_Fim = null;
            d.getAplic.getAplicPosFim.merc_Pos_Fim = valorReceber;

            // Adiciona Demais_N_Codif
            arq.getCorpo.getInform.getListAtiv.getDemais_N_Codif.Add(d);
            #endregion

            #region Acoes com posição negativa
            PosicaoBolsaHistoricoCollection posicaoBolsaHistoricoCollection = new PosicaoBolsaHistoricoCollection();
            posicaoBolsaHistoricoCollection.Query.Select(posicaoBolsaHistoricoCollection.Query.CdAtivoBolsa,
                                                         posicaoBolsaHistoricoCollection.Query.Quantidade.Sum(),
                                                         posicaoBolsaHistoricoCollection.Query.ValorCustoLiquido.Sum(),
                                                         posicaoBolsaHistoricoCollection.Query.ValorMercado.Sum());
            posicaoBolsaHistoricoCollection.Query.Where(posicaoBolsaHistoricoCollection.Query.IdCliente.Equal(idCarteira),
                                                        posicaoBolsaHistoricoCollection.Query.DataHistorico.Equal(dataPosicao),
                                                        posicaoBolsaHistoricoCollection.Query.TipoMercado.Equal(TipoMercadoBolsa.MercadoVista),
                                                        posicaoBolsaHistoricoCollection.Query.Quantidade.LessThan(0));
            posicaoBolsaHistoricoCollection.Query.GroupBy(posicaoBolsaHistoricoCollection.Query.CdAtivoBolsa);
            posicaoBolsaHistoricoCollection.Query.OrderBy(posicaoBolsaHistoricoCollection.Query.CdAtivoBolsa.Ascending);
            posicaoBolsaHistoricoCollection.Query.Load();

            foreach (PosicaoBolsaHistorico posicaoBolsaHistorico in posicaoBolsaHistoricoCollection)
            {
                string cdAtivoBolsa = posicaoBolsaHistorico.CdAtivoBolsa;

                AtivoBolsa ativoBolsa = new AtivoBolsa();
                List<esQueryItem> campos = new List<esQueryItem>();
                campos.Add(ativoBolsa.Query.IdEmissor);
                ativoBolsa.LoadByPrimaryKey(campos, cdAtivoBolsa);

                decimal quantidadeFinal = posicaoBolsaHistorico.Quantidade.HasValue ? posicaoBolsaHistorico.Quantidade.Value : 0;
                decimal valorFinal = posicaoBolsaHistorico.ValorMercado.HasValue ? posicaoBolsaHistorico.ValorMercado.Value : 0;
                decimal custoCorrecPosFim = posicaoBolsaHistorico.ValorCustoLiquido.HasValue ? posicaoBolsaHistorico.ValorCustoLiquido.Value : 0;


                OperacaoBolsa operacaoBolsaCompra = new OperacaoBolsa();
                operacaoBolsaCompra.Query.Select(operacaoBolsaCompra.Query.Quantidade.Sum(),
                                                 operacaoBolsaCompra.Query.Valor.Sum());
                operacaoBolsaCompra.Query.Where(operacaoBolsaCompra.Query.IdCliente.Equal(idCarteira),
                                                operacaoBolsaCompra.Query.CdAtivoBolsa.Equal(cdAtivoBolsa),
                                                operacaoBolsaCompra.Query.Data.GreaterThanOrEqual(dataInicioMes),
                                                operacaoBolsaCompra.Query.Data.LessThanOrEqual(dataPosicao),
                                                operacaoBolsaCompra.Query.TipoOperacao.In(TipoOperacaoBolsa.Compra, TipoOperacaoBolsa.CompraDaytrade));
                operacaoBolsaCompra.Query.Load();

                decimal quantidadeCompra = operacaoBolsaCompra.Quantidade.HasValue ? operacaoBolsaCompra.Quantidade.Value : 0;
                decimal valorCompra = operacaoBolsaCompra.Valor.HasValue ? operacaoBolsaCompra.Valor.Value : 0;

                OperacaoBolsa operacaoBolsaVenda = new OperacaoBolsa();
                operacaoBolsaVenda.Query.Select(operacaoBolsaVenda.Query.Quantidade.Sum(),
                                                operacaoBolsaVenda.Query.Valor.Sum());
                operacaoBolsaVenda.Query.Where(operacaoBolsaVenda.Query.IdCliente.Equal(idCarteira),
                                               operacaoBolsaVenda.Query.CdAtivoBolsa.Equal(cdAtivoBolsa),
                                               operacaoBolsaVenda.Query.Data.GreaterThanOrEqual(dataInicioMes),
                                               operacaoBolsaVenda.Query.Data.LessThanOrEqual(dataPosicao),
                                               operacaoBolsaVenda.Query.TipoOperacao.In(TipoOperacaoBolsa.Venda, TipoOperacaoBolsa.VendaDaytrade));
                operacaoBolsaVenda.Query.Load();

                decimal quantidadeVenda = operacaoBolsaVenda.Quantidade.HasValue ? operacaoBolsaVenda.Quantidade.Value : 0;
                decimal valorVenda = operacaoBolsaVenda.Valor.HasValue ? operacaoBolsaVenda.Valor.Value : 0;

                Emissor emissor = new Emissor();
                emissor.LoadByPrimaryKey(ativoBolsa.IdEmissor.Value);
                string cnpjEmissor = !String.IsNullOrEmpty(emissor.Cnpj) ? Utilitario.RemoveCaracteresEspeciais(emissor.Cnpj) : "";
                string nomeEmissor = emissor.Nome.Trim();

                d = new Demais_N_Codif();
                d.cod_Tp_Ativ = Convert.ToString((int)TipoAtivoCDA.Outros);
                d.descricao = "Valores a Pagar - Posição Vendida de " + cdAtivoBolsa;
                // Demais_N_Codif - Emis
                if (!string.IsNullOrEmpty(cnpjEmissor) && !string.IsNullOrEmpty(nomeEmissor))
                {
                    d.getEmis.tp_PF_PJ = "PJ";
                    d.getEmis.nr_PF_PJ = cnpjEmissor;
                    d.getEmis.nm = nomeEmissor;
                }
                else
                {
                    d.getEmis.tp_PF_PJ = string.Empty;
                    d.getEmis.nr_PF_PJ = string.Empty;
                    d.getEmis.nm = string.Empty;
                }

                // Demais_N_Codif - Aplic
                d.getAplic.cod_Tp_Aplic = Convert.ToString((int)TipoAplicacaoCDA.OutrasAplicacoes);

                bool empresaLigada = this.IndicaEmpresaLigadaAcao(cdAtivoBolsa, carteira.IdAgenteAdministrador.Value, carteira.IdAgenteGestor.Value);
                // Demais_Codif - Aplic
                d.getAplic.empr_Ligada = empresaLigada ? "S" : "N";
                d.getAplic.qtde_Dias_Confid = this.diasConfidencialidade.ToString();

                // Demais_N_Codif - Aplic - Negoc
                d.getAplic.getAplicNegoc.tipoNegocio = Convert.ToString((int)TipoNegociacaoCDA.Negociacao);
                
                d.getAplic.getAplicNegoc.quantidadeVendasNegocio = quantidadeVenda;
                d.getAplic.getAplicNegoc.valorVendasNegocio = valorVenda;
                d.getAplic.getAplicNegoc.quantidadeAquisicaoNegocio = quantidadeCompra;
                d.getAplic.getAplicNegoc.valorAquisicaoNegocio = valorCompra;

                // Demais_N_Codif - Aplic - Pos_FIM
                d.getAplic.aplic_Pos_Fim.VersaoCDA("3");
                d.getAplic.getAplicPosFim.qtde_Pos_Fim = quantidadeFinal;
                d.getAplic.getAplicPosFim.custo_Correc_Pos_Fim = custoCorrecPosFim;
                d.getAplic.getAplicPosFim.merc_Pos_Fim = valorFinal;

                // Adiciona Demais_N_Codif
                arq.getCorpo.getInform.getListAtiv.getDemais_N_Codif.Add(d);
            }
            #endregion

            #region Acoes sem data de vigência definida
            AtivoBolsaQuery ativoBolsaQuery = new AtivoBolsaQuery("A");
            PosicaoBolsaHistoricoQuery posicaoBolsaHistoricoQuery = new PosicaoBolsaHistoricoQuery("P");

            posicaoBolsaHistoricoCollection = new PosicaoBolsaHistoricoCollection();
            posicaoBolsaHistoricoQuery.Select(posicaoBolsaHistoricoQuery.CdAtivoBolsa,
                                             posicaoBolsaHistoricoQuery.Quantidade.Sum(),
                                             posicaoBolsaHistoricoQuery.ValorCustoLiquido.Sum(),
                                             posicaoBolsaHistoricoQuery.ValorMercado.Sum());
            posicaoBolsaHistoricoQuery.InnerJoin(ativoBolsaQuery).On(ativoBolsaQuery.CdAtivoBolsa == posicaoBolsaHistoricoQuery.CdAtivoBolsa);
            posicaoBolsaHistoricoQuery.Where(posicaoBolsaHistoricoQuery.IdCliente.Equal(idCarteira) &
                                            posicaoBolsaHistoricoQuery.DataHistorico.Equal(dataPosicao) &
                                            posicaoBolsaHistoricoQuery.TipoMercado.Equal(TipoMercadoBolsa.MercadoVista) &
                                            (posicaoBolsaHistoricoQuery.Quantidade.GreaterThan(0) &
                                            ativoBolsaQuery.DataInicioVigencia.IsNull() | ativoBolsaQuery.DataInicioVigencia.Equal(dataIndefinida)));
            posicaoBolsaHistoricoQuery.GroupBy(posicaoBolsaHistoricoQuery.CdAtivoBolsa);
            posicaoBolsaHistoricoQuery.OrderBy(posicaoBolsaHistoricoQuery.CdAtivoBolsa.Ascending);
            posicaoBolsaHistoricoCollection.Load(posicaoBolsaHistoricoQuery);

            foreach (PosicaoBolsaHistorico posicaoBolsaHistorico in posicaoBolsaHistoricoCollection)
            {
                string cdAtivoBolsa = posicaoBolsaHistorico.CdAtivoBolsa;

                AtivoBolsa ativoBolsa = new AtivoBolsa();
                ativoBolsa.LoadByPrimaryKey(cdAtivoBolsa);

                decimal quantidadeFinal = posicaoBolsaHistorico.Quantidade.HasValue ? posicaoBolsaHistorico.Quantidade.Value : 0;
                decimal valorFinal = posicaoBolsaHistorico.ValorMercado.HasValue ? posicaoBolsaHistorico.ValorMercado.Value : 0;
                decimal custoCorrecPosFim = posicaoBolsaHistorico.ValorCustoLiquido.HasValue ? posicaoBolsaHistorico.ValorCustoLiquido.Value : 0;

                OperacaoBolsa operacaoBolsaCompra = new OperacaoBolsa();
                operacaoBolsaCompra.Query.Select(operacaoBolsaCompra.Query.Quantidade.Sum(),
                                                 operacaoBolsaCompra.Query.Valor.Sum());
                operacaoBolsaCompra.Query.Where(operacaoBolsaCompra.Query.IdCliente.Equal(idCarteira),
                                                operacaoBolsaCompra.Query.CdAtivoBolsa.Equal(cdAtivoBolsa),
                                                operacaoBolsaCompra.Query.Data.GreaterThanOrEqual(dataInicioMes),
                                                operacaoBolsaCompra.Query.Data.LessThanOrEqual(dataPosicao),
                                                operacaoBolsaCompra.Query.TipoOperacao.In(TipoOperacaoBolsa.Compra, TipoOperacaoBolsa.CompraDaytrade));
                operacaoBolsaCompra.Query.Load();

                decimal quantidadeCompra = operacaoBolsaCompra.Quantidade.HasValue ? operacaoBolsaCompra.Quantidade.Value : 0;
                decimal valorCompra = operacaoBolsaCompra.Valor.HasValue ? operacaoBolsaCompra.Valor.Value : 0;

                OperacaoBolsa operacaoBolsaVenda = new OperacaoBolsa();
                operacaoBolsaVenda.Query.Select(operacaoBolsaVenda.Query.Quantidade.Sum(),
                                                operacaoBolsaVenda.Query.Valor.Sum());
                operacaoBolsaVenda.Query.Where(operacaoBolsaVenda.Query.IdCliente.Equal(idCarteira),
                                               operacaoBolsaVenda.Query.CdAtivoBolsa.Equal(cdAtivoBolsa),
                                               operacaoBolsaVenda.Query.Data.GreaterThanOrEqual(dataInicioMes),
                                               operacaoBolsaVenda.Query.Data.LessThanOrEqual(dataPosicao),
                                               operacaoBolsaVenda.Query.TipoOperacao.In(TipoOperacaoBolsa.Venda, TipoOperacaoBolsa.VendaDaytrade));
                operacaoBolsaVenda.Query.Load();

                decimal quantidadeVenda = operacaoBolsaVenda.Quantidade.HasValue ? operacaoBolsaVenda.Quantidade.Value : 0;
                decimal valorVenda = operacaoBolsaVenda.Valor.HasValue ? operacaoBolsaVenda.Valor.Value : 0;

                Emissor emissor = new Emissor();
                emissor.LoadByPrimaryKey(ativoBolsa.IdEmissor.Value);
                string cnpjEmissor = !String.IsNullOrEmpty(emissor.Cnpj) ? Utilitario.RemoveCaracteresEspeciais(emissor.Cnpj) : "";
                string nomeEmissor = emissor.Nome.Trim();

                string codigoAtivo = "";
                string tipoAplicacao = "";
                #region Checa codigo de ativo
                if (ativoBolsa.TipoPapel.Value == (byte)TipoPapelAtivo.Normal)
                {
                    codigoAtivo = Utilitario.Right(cdAtivoBolsa, 1) == "3" ? Convert.ToString((int)TipoAtivoCDA.AcaoOrdinaria) : Convert.ToString((int)TipoAtivoCDA.AcaoPreferencial);
                    tipoAplicacao = Convert.ToString((int)TipoAplicacaoCDA.Acoes);
                }
                else if (ativoBolsa.TipoPapel.Value == (byte)TipoPapelAtivo.BDRNivelI)
                {
                    codigoAtivo = Convert.ToString((int)TipoAtivoCDA.BDRNivelI);
                    tipoAplicacao = Convert.ToString((int)TipoAplicacaoCDA.BDR);
                }
                else if (ativoBolsa.TipoPapel.Value == (byte)TipoPapelAtivo.BDRNivelII)
                {
                    codigoAtivo = Convert.ToString((int)TipoAtivoCDA.BDRNivelII);
                    tipoAplicacao = Convert.ToString((int)TipoAplicacaoCDA.BDR);
                }
                else if (ativoBolsa.TipoPapel.Value == (byte)TipoPapelAtivo.BDRNivelIII)
                {
                    codigoAtivo = Convert.ToString((int)TipoAtivoCDA.BDRNivelIII);
                    tipoAplicacao = Convert.ToString((int)TipoAplicacaoCDA.BDR);
                }
                else if (ativoBolsa.TipoPapel.Value == (byte)TipoPapelAtivo.BonusSubscricao)
                {
                    codigoAtivo = Convert.ToString((int)TipoAtivoCDA.BonusSubscricao);
                    tipoAplicacao = Convert.ToString((int)TipoAplicacaoCDA.OutrosValoresMobiliarios_OfertaPublica);
                }
                else if (ativoBolsa.TipoPapel.Value == (byte)TipoPapelAtivo.CertificadoDepositoAcoes)
                {
                    codigoAtivo = Convert.ToString((int)TipoAtivoCDA.CertificadoDepositoAcoes);
                    tipoAplicacao = Convert.ToString((int)TipoAplicacaoCDA.Certificado_ReciboDeposito);
                }
                else if (ativoBolsa.TipoPapel.Value == (byte)TipoPapelAtivo.ETF)
                {
                    codigoAtivo = Convert.ToString((int)TipoAtivoCDA.AcaoPreferencial); //Fundos de indice são jogados, por ora, em Ação
                    tipoAplicacao = Convert.ToString((int)TipoAplicacaoCDA.Acoes);
                }
                else if (ativoBolsa.TipoPapel.Value == (byte)TipoPapelAtivo.ReciboDepositoAcoes)
                {
                    codigoAtivo = Convert.ToString((int)TipoAtivoCDA.ReciboDepositoAcoes);
                    tipoAplicacao = Convert.ToString((int)TipoAplicacaoCDA.Certificado_ReciboDeposito);
                }
                else if (ativoBolsa.TipoPapel.Value == (byte)TipoPapelAtivo.ReciboSubscricao)
                {
                    codigoAtivo = Convert.ToString((int)TipoAtivoCDA.ReciboSubscricao);
                    tipoAplicacao = Convert.ToString((int)TipoAplicacaoCDA.OutrosValoresMobiliarios_OfertaPublica);
                }
                #endregion

                d = new Demais_N_Codif();
                d.cod_Tp_Ativ = codigoAtivo;
                d.descricao = ativoBolsa.Descricao;
                // Demais_N_Codif - Emis
                if (!string.IsNullOrEmpty(cnpjEmissor) && !string.IsNullOrEmpty(nomeEmissor))
                {
                    d.getEmis.tp_PF_PJ = "PJ";
                    d.getEmis.nr_PF_PJ = cnpjEmissor;
                    d.getEmis.nm = nomeEmissor;
                }
                else
                {
                    d.getEmis.tp_PF_PJ = string.Empty;
                    d.getEmis.nr_PF_PJ = string.Empty;
                    d.getEmis.nm = string.Empty;
                }

                // Demais_N_Codif - Aplic
                d.getAplic.cod_Tp_Aplic = tipoAplicacao;

                bool empresaLigada = this.IndicaEmpresaLigadaAcao(cdAtivoBolsa, carteira.IdAgenteAdministrador.Value, carteira.IdAgenteGestor.Value);
                // Demais_Codif - Aplic
                d.getAplic.empr_Ligada = empresaLigada ? "S" : "N";

                d.getAplic.qtde_Dias_Confid = this.diasConfidencialidade.ToString();

                // Demais_N_Codif - Aplic - Negoc
                d.getAplic.getAplicNegoc.tipoNegocio = Convert.ToString((int)TipoNegociacaoCDA.Negociacao);

                d.getAplic.getAplicNegoc.quantidadeVendasNegocio = quantidadeVenda;
                d.getAplic.getAplicNegoc.valorVendasNegocio = valorVenda;
                d.getAplic.getAplicNegoc.quantidadeAquisicaoNegocio = quantidadeCompra;
                d.getAplic.getAplicNegoc.valorAquisicaoNegocio = valorCompra;

                // Demais_N_Codif - Aplic - Pos_FIM
                d.getAplic.aplic_Pos_Fim.VersaoCDA("3");
                d.getAplic.getAplicPosFim.qtde_Pos_Fim = quantidadeFinal;
                d.getAplic.getAplicPosFim.custo_Correc_Pos_Fim = custoCorrecPosFim;
                d.getAplic.getAplicPosFim.merc_Pos_Fim = valorFinal;

                // Adiciona Demais_N_Codif
                arq.getCorpo.getInform.getListAtiv.getDemais_N_Codif.Add(d);
            }
            #endregion

            #region Opcoes compradas sem data de vigência definida
            ativoBolsaQuery = new AtivoBolsaQuery("A");
            posicaoBolsaHistoricoQuery = new PosicaoBolsaHistoricoQuery("P");

            posicaoBolsaHistoricoCollection = new PosicaoBolsaHistoricoCollection();
            posicaoBolsaHistoricoQuery.Select(posicaoBolsaHistoricoQuery.TipoMercado,
                                             posicaoBolsaHistoricoQuery.CdAtivoBolsa,
                                             posicaoBolsaHistoricoQuery.Quantidade.Sum(),
                                             posicaoBolsaHistoricoQuery.ValorCustoLiquido.Sum(),
                                             posicaoBolsaHistoricoQuery.ValorMercado.Sum());
            posicaoBolsaHistoricoQuery.InnerJoin(ativoBolsaQuery).On(ativoBolsaQuery.CdAtivoBolsa == posicaoBolsaHistoricoQuery.CdAtivoBolsa);
            posicaoBolsaHistoricoQuery.Where(posicaoBolsaHistoricoQuery.IdCliente.Equal(idCarteira) &
                                            posicaoBolsaHistoricoQuery.DataHistorico.Equal(dataPosicao) &
                                            posicaoBolsaHistoricoQuery.TipoMercado.In(TipoMercadoBolsa.OpcaoCompra, TipoMercadoBolsa.OpcaoVenda) &
                                            (posicaoBolsaHistoricoQuery.Quantidade.GreaterThan(0) &
                                            ativoBolsaQuery.DataInicioVigencia.IsNull() | ativoBolsaQuery.DataInicioVigencia.Equal(dataIndefinida)));
            posicaoBolsaHistoricoQuery.GroupBy(posicaoBolsaHistoricoQuery.TipoMercado,
                                               posicaoBolsaHistoricoQuery.CdAtivoBolsa);
            posicaoBolsaHistoricoQuery.OrderBy(posicaoBolsaHistoricoQuery.TipoMercado.Ascending,
                                               posicaoBolsaHistoricoQuery.CdAtivoBolsa.Ascending);
            posicaoBolsaHistoricoCollection.Load(posicaoBolsaHistoricoQuery);

            foreach (PosicaoBolsaHistorico posicaoBolsaHistorico in posicaoBolsaHistoricoCollection)
            {
                string cdAtivoBolsa = posicaoBolsaHistorico.CdAtivoBolsa;
                string tipoMercado = posicaoBolsaHistorico.TipoMercado;

                AtivoBolsa ativoBolsa = new AtivoBolsa();
                ativoBolsa.LoadByPrimaryKey(cdAtivoBolsa);

                decimal quantidadeFinal = posicaoBolsaHistorico.Quantidade.HasValue ? posicaoBolsaHistorico.Quantidade.Value : 0;
                decimal valorFinal = posicaoBolsaHistorico.ValorMercado.HasValue ? posicaoBolsaHistorico.ValorMercado.Value : 0;
                decimal custoCorrecPosFim = posicaoBolsaHistorico.ValorCustoLiquido.HasValue ? posicaoBolsaHistorico.ValorCustoLiquido.Value : 0;


                OperacaoBolsa operacaoBolsaCompra = new OperacaoBolsa();
                operacaoBolsaCompra.Query.Select(operacaoBolsaCompra.Query.Quantidade.Sum(),
                                                 operacaoBolsaCompra.Query.Valor.Sum());
                operacaoBolsaCompra.Query.Where(operacaoBolsaCompra.Query.IdCliente.Equal(idCarteira),
                                                operacaoBolsaCompra.Query.CdAtivoBolsa.Equal(cdAtivoBolsa),
                                                operacaoBolsaCompra.Query.Data.GreaterThanOrEqual(dataInicioMes),
                                                operacaoBolsaCompra.Query.Data.LessThanOrEqual(dataPosicao),
                                                operacaoBolsaCompra.Query.TipoOperacao.In(TipoOperacaoBolsa.Compra, TipoOperacaoBolsa.CompraDaytrade));
                operacaoBolsaCompra.Query.Load();

                decimal quantidadeCompra = operacaoBolsaCompra.Quantidade.HasValue ? operacaoBolsaCompra.Quantidade.Value : 0;
                decimal valorCompra = operacaoBolsaCompra.Valor.HasValue ? operacaoBolsaCompra.Valor.Value : 0;

                OperacaoBolsa operacaoBolsaVenda = new OperacaoBolsa();
                operacaoBolsaVenda.Query.Select(operacaoBolsaVenda.Query.Quantidade.Sum(),
                                                operacaoBolsaVenda.Query.Valor.Sum());
                operacaoBolsaVenda.Query.Where(operacaoBolsaVenda.Query.IdCliente.Equal(idCarteira),
                                               operacaoBolsaVenda.Query.CdAtivoBolsa.Equal(cdAtivoBolsa),
                                               operacaoBolsaVenda.Query.Data.GreaterThanOrEqual(dataInicioMes),
                                               operacaoBolsaVenda.Query.Data.LessThanOrEqual(dataPosicao),
                                               operacaoBolsaVenda.Query.TipoOperacao.In(TipoOperacaoBolsa.Venda, TipoOperacaoBolsa.VendaDaytrade));
                operacaoBolsaVenda.Query.Load();

                decimal quantidadeVenda = operacaoBolsaVenda.Quantidade.HasValue ? operacaoBolsaVenda.Quantidade.Value : 0;
                decimal valorVenda = operacaoBolsaVenda.Valor.HasValue ? operacaoBolsaVenda.Valor.Value : 0;

                Emissor emissor = new Emissor();
                emissor.LoadByPrimaryKey(ativoBolsa.IdEmissor.Value);
                string cnpjEmissor = !String.IsNullOrEmpty(emissor.Cnpj) ? Utilitario.RemoveCaracteresEspeciais(emissor.Cnpj) : "";
                string nomeEmissor = emissor.Nome.Trim();

                d = new Demais_N_Codif();

                if (tipoMercado == TipoMercadoBolsa.OpcaoCompra)
                {
                    d.cod_Tp_Ativ = Convert.ToString((int)TipoAtivoCDA.OpcaoCompra);
                }
                else
                {
                    d.cod_Tp_Ativ = Convert.ToString((int)TipoAtivoCDA.OpcaoVenda);
                }

                d.descricao = cdAtivoBolsa;
                // Demais_N_Codif - Emis
                if (!string.IsNullOrEmpty(cnpjEmissor) && !string.IsNullOrEmpty(nomeEmissor))
                {
                    d.getEmis.tp_PF_PJ = "PJ";
                    d.getEmis.nr_PF_PJ = cnpjEmissor;
                    d.getEmis.nm = nomeEmissor;
                }
                else
                {
                    d.getEmis.tp_PF_PJ = string.Empty;
                    d.getEmis.nr_PF_PJ = string.Empty;
                    d.getEmis.nm = string.Empty;
                }

                // Demais_N_Codif - Aplic
                d.getAplic.cod_Tp_Aplic = Convert.ToString((int)TipoAplicacaoCDA.OpcoesPosicaoTitular);
                d.getAplic.empr_Ligada = "N";
                d.getAplic.qtde_Dias_Confid = this.diasConfidencialidade.ToString();

                // Demais_N_Codif - Aplic - Negoc
                d.getAplic.getAplicNegoc.tipoNegocio = Convert.ToString((int)TipoNegociacaoCDA.Negociacao);

                d.getAplic.getAplicNegoc.quantidadeVendasNegocio = quantidadeVenda;
                d.getAplic.getAplicNegoc.valorVendasNegocio = valorVenda;
                d.getAplic.getAplicNegoc.quantidadeAquisicaoNegocio = quantidadeCompra;
                d.getAplic.getAplicNegoc.valorAquisicaoNegocio = valorCompra;

                // Demais_N_Codif - Aplic - Pos_FIM
                d.getAplic.aplic_Pos_Fim.VersaoCDA("3");
                d.getAplic.getAplicPosFim.qtde_Pos_Fim = quantidadeFinal;
                d.getAplic.getAplicPosFim.custo_Correc_Pos_Fim = custoCorrecPosFim;
                d.getAplic.getAplicPosFim.merc_Pos_Fim = valorFinal;

                // Adiciona Demais_N_Codif
                arq.getCorpo.getInform.getListAtiv.getDemais_N_Codif.Add(d);
            }
            #endregion

            #region Opcoes vendidas sem data de vigência definida
            ativoBolsaQuery = new AtivoBolsaQuery("A");
            posicaoBolsaHistoricoQuery = new PosicaoBolsaHistoricoQuery("P");

            posicaoBolsaHistoricoCollection = new PosicaoBolsaHistoricoCollection();
            posicaoBolsaHistoricoQuery.Select(posicaoBolsaHistoricoQuery.TipoMercado,
                                             posicaoBolsaHistoricoQuery.CdAtivoBolsa,
                                             posicaoBolsaHistoricoQuery.Quantidade.Sum(),
                                             posicaoBolsaHistoricoQuery.ValorCustoLiquido.Sum(),
                                             posicaoBolsaHistoricoQuery.ValorMercado.Sum());
            posicaoBolsaHistoricoQuery.InnerJoin(ativoBolsaQuery).On(ativoBolsaQuery.CdAtivoBolsa == posicaoBolsaHistoricoQuery.CdAtivoBolsa);
            posicaoBolsaHistoricoQuery.Where(posicaoBolsaHistoricoQuery.IdCliente.Equal(idCarteira) &
                                            posicaoBolsaHistoricoQuery.DataHistorico.Equal(dataPosicao) &
                                            posicaoBolsaHistoricoQuery.TipoMercado.In(TipoMercadoBolsa.OpcaoCompra, TipoMercadoBolsa.OpcaoVenda) &
                                            (posicaoBolsaHistoricoQuery.Quantidade.LessThan(0) &
                                            ativoBolsaQuery.DataInicioVigencia.IsNull() | ativoBolsaQuery.DataInicioVigencia.Equal(dataIndefinida)));
            posicaoBolsaHistoricoQuery.GroupBy(posicaoBolsaHistoricoQuery.TipoMercado,
                                               posicaoBolsaHistoricoQuery.CdAtivoBolsa);
            posicaoBolsaHistoricoQuery.OrderBy(posicaoBolsaHistoricoQuery.TipoMercado.Ascending,
                                               posicaoBolsaHistoricoQuery.CdAtivoBolsa.Ascending);
            posicaoBolsaHistoricoCollection.Load(posicaoBolsaHistoricoQuery);

            foreach (PosicaoBolsaHistorico posicaoBolsaHistorico in posicaoBolsaHistoricoCollection)
            {
                string cdAtivoBolsa = posicaoBolsaHistorico.CdAtivoBolsa;
                string tipoMercado = posicaoBolsaHistorico.TipoMercado;

                AtivoBolsa ativoBolsa = new AtivoBolsa();
                ativoBolsa.LoadByPrimaryKey(cdAtivoBolsa);

                decimal quantidadeFinal = posicaoBolsaHistorico.Quantidade.HasValue ? posicaoBolsaHistorico.Quantidade.Value : 0;
                decimal valorFinal = posicaoBolsaHistorico.ValorMercado.HasValue ? posicaoBolsaHistorico.ValorMercado.Value : 0;
                decimal custoCorrecPosFim = posicaoBolsaHistorico.ValorCustoLiquido.HasValue ? posicaoBolsaHistorico.ValorCustoLiquido.Value : 0;


                OperacaoBolsa operacaoBolsaCompra = new OperacaoBolsa();
                operacaoBolsaCompra.Query.Select(operacaoBolsaCompra.Query.Quantidade.Sum(),
                                                 operacaoBolsaCompra.Query.Valor.Sum());
                operacaoBolsaCompra.Query.Where(operacaoBolsaCompra.Query.IdCliente.Equal(idCarteira),
                                                operacaoBolsaCompra.Query.CdAtivoBolsa.Equal(cdAtivoBolsa),
                                                operacaoBolsaCompra.Query.Data.GreaterThanOrEqual(dataInicioMes),
                                                operacaoBolsaCompra.Query.Data.LessThanOrEqual(dataPosicao),
                                                operacaoBolsaCompra.Query.TipoOperacao.In(TipoOperacaoBolsa.Compra, TipoOperacaoBolsa.CompraDaytrade));
                operacaoBolsaCompra.Query.Load();

                decimal quantidadeCompra = operacaoBolsaCompra.Quantidade.HasValue ? operacaoBolsaCompra.Quantidade.Value : 0;
                decimal valorCompra = operacaoBolsaCompra.Valor.HasValue ? operacaoBolsaCompra.Valor.Value : 0;

                OperacaoBolsa operacaoBolsaVenda = new OperacaoBolsa();
                operacaoBolsaVenda.Query.Select(operacaoBolsaVenda.Query.Quantidade.Sum(),
                                                operacaoBolsaVenda.Query.Valor.Sum());
                operacaoBolsaVenda.Query.Where(operacaoBolsaVenda.Query.IdCliente.Equal(idCarteira),
                                               operacaoBolsaVenda.Query.CdAtivoBolsa.Equal(cdAtivoBolsa),
                                               operacaoBolsaVenda.Query.Data.GreaterThanOrEqual(dataInicioMes),
                                               operacaoBolsaVenda.Query.Data.LessThanOrEqual(dataPosicao),
                                               operacaoBolsaVenda.Query.TipoOperacao.In(TipoOperacaoBolsa.Venda, TipoOperacaoBolsa.VendaDaytrade));
                operacaoBolsaVenda.Query.Load();

                decimal quantidadeVenda = operacaoBolsaVenda.Quantidade.HasValue ? operacaoBolsaVenda.Quantidade.Value : 0;
                decimal valorVenda = operacaoBolsaVenda.Valor.HasValue ? operacaoBolsaVenda.Valor.Value : 0;

                Emissor emissor = new Emissor();
                emissor.LoadByPrimaryKey(ativoBolsa.IdEmissor.Value);
                string cnpjEmissor = !String.IsNullOrEmpty(emissor.Cnpj) ? Utilitario.RemoveCaracteresEspeciais(emissor.Cnpj) : "";
                string nomeEmissor = emissor.Nome.Trim();

                d = new Demais_N_Codif();

                if (tipoMercado == TipoMercadoBolsa.OpcaoCompra)
                {
                    d.cod_Tp_Ativ = Convert.ToString((int)TipoAtivoCDA.OpcaoCompra);
                }
                else
                {
                    d.cod_Tp_Ativ = Convert.ToString((int)TipoAtivoCDA.OpcaoVenda);
                }

                d.descricao = cdAtivoBolsa;
                // Demais_N_Codif - Emis
                if (!string.IsNullOrEmpty(cnpjEmissor) && !string.IsNullOrEmpty(nomeEmissor))
                {
                    d.getEmis.tp_PF_PJ = "PJ";
                    d.getEmis.nr_PF_PJ = cnpjEmissor;
                    d.getEmis.nm = nomeEmissor;
                }
                else
                {
                    d.getEmis.tp_PF_PJ = string.Empty;
                    d.getEmis.nr_PF_PJ = string.Empty;
                    d.getEmis.nm = string.Empty;
                }

                // Demais_N_Codif - Aplic
                d.getAplic.cod_Tp_Aplic = Convert.ToString((int)TipoAplicacaoCDA.OpcoesPosicaoLancada);
                d.getAplic.empr_Ligada = "N";
                d.getAplic.qtde_Dias_Confid = this.diasConfidencialidade.ToString();

                // Demais_N_Codif - Aplic - Negoc
                d.getAplic.getAplicNegoc.tipoNegocio = Convert.ToString((int)TipoNegociacaoCDA.Negociacao);

                d.getAplic.getAplicNegoc.quantidadeVendasNegocio = quantidadeVenda;
                d.getAplic.getAplicNegoc.valorVendasNegocio = valorVenda;
                d.getAplic.getAplicNegoc.quantidadeAquisicaoNegocio = quantidadeCompra;
                d.getAplic.getAplicNegoc.valorAquisicaoNegocio = valorCompra;

                // Demais_N_Codif - Aplic - Pos_FIM
                d.getAplic.aplic_Pos_Fim.VersaoCDA("3");
                d.getAplic.getAplicPosFim.qtde_Pos_Fim = quantidadeFinal;
                d.getAplic.getAplicPosFim.custo_Correc_Pos_Fim = custoCorrecPosFim;
                d.getAplic.getAplicPosFim.merc_Pos_Fim = valorFinal;

                // Adiciona Demais_N_Codif
                arq.getCorpo.getInform.getListAtiv.getDemais_N_Codif.Add(d);
            }
            #endregion

            #region Futuros/Opções Compradas sem data de vigência definida
            PosicaoBMFHistoricoQuery posicaoBMFHistoricoQuery = new PosicaoBMFHistoricoQuery("P");
            AtivoBMFQuery ativoBMFQuery = new AtivoBMFQuery("A");

            PosicaoBMFHistoricoCollection posicaoBMFHistoricoCollection = new PosicaoBMFHistoricoCollection();
            posicaoBMFHistoricoQuery.Select(posicaoBMFHistoricoQuery.TipoMercado,
                                           posicaoBMFHistoricoQuery.CdAtivoBMF,
                                           posicaoBMFHistoricoQuery.Serie,
                                           posicaoBMFHistoricoQuery.Quantidade.Sum(),
                                           posicaoBMFHistoricoQuery.ValorMercado.Sum());
            posicaoBMFHistoricoQuery.InnerJoin(ativoBMFQuery).On(ativoBMFQuery.CdAtivoBMF == posicaoBMFHistoricoQuery.CdAtivoBMF &
                                                                 ativoBMFQuery.Serie == posicaoBMFHistoricoQuery.Serie);
            posicaoBMFHistoricoQuery.Where(posicaoBMFHistoricoQuery.IdCliente.Equal(idCarteira) &
                                          posicaoBMFHistoricoQuery.DataHistorico.Equal(dataPosicao) &
                                          posicaoBMFHistoricoQuery.Quantidade.GreaterThan(0) &
                                          (ativoBMFQuery.DataFimVigencia.IsNull() | ativoBMFQuery.DataFimVigencia.Equal(dataIndefinida)));
            posicaoBMFHistoricoQuery.GroupBy(posicaoBMFHistoricoQuery.TipoMercado,
                                            posicaoBMFHistoricoQuery.CdAtivoBMF,
                                            posicaoBMFHistoricoQuery.Serie);
            posicaoBMFHistoricoQuery.OrderBy(posicaoBMFHistoricoQuery.TipoMercado.Ascending,
                                            posicaoBMFHistoricoQuery.CdAtivoBMF.Ascending);
            posicaoBMFHistoricoCollection.Load(posicaoBMFHistoricoQuery);

            foreach (PosicaoBMFHistorico posicaoBMFHistorico in posicaoBMFHistoricoCollection)
            {
                string cdAtivoBMF = posicaoBMFHistorico.CdAtivoBMF;
                string serie = posicaoBMFHistorico.Serie;
                byte tipoMercado = posicaoBMFHistorico.TipoMercado.Value;

                decimal quantidadeFinal = posicaoBMFHistorico.Quantidade.HasValue ? posicaoBMFHistorico.Quantidade.Value : 0;
                decimal valorFinal = (posicaoBMFHistorico.TipoMercado.Value != (byte)TipoMercadoBMF.Futuro && posicaoBMFHistorico.ValorMercado.HasValue) ? posicaoBMFHistorico.ValorMercado.Value : 0;

                AtivoBMF ativoBMF = new AtivoBMF();
                ativoBMF.LoadByPrimaryKey(cdAtivoBMF, serie);

                string codigoAtivo = ativoBMF.CodigoCDA.HasValue ? Convert.ToString(ativoBMF.CodigoCDA) : Convert.ToString((int)TipoAtivoCDA.ContratoFuturo);

                d = new Demais_N_Codif();
                d.cod_Tp_Ativ = codigoAtivo;

                d.descricao = this.RetornaAtivoFuturoCDA(ativoBMF);

                // Demais_N_Codif - Emis
                d.getEmis.tp_PF_PJ = string.Empty;
                d.getEmis.nr_PF_PJ = string.Empty;
                d.getEmis.nm = string.Empty;                

                // Demais_N_Codif - Aplic
                d.getAplic.cod_Tp_Aplic = Convert.ToString((int)TipoAplicacaoCDA.MercadoFuturoComprado);
                d.getAplic.qtde_Dias_Confid = this.diasConfidencialidade.ToString();

                // Demais_N_Codif - Aplic - Negoc
                d.getAplic.getAplicNegoc.tipoNegocio = Convert.ToString((int)TipoNegociacaoCDA.Negociacao);

                // Demais_N_Codif - Aplic - Pos_FIM
                d.getAplic.aplic_Pos_Fim.VersaoCDA("3");
                d.getAplic.getAplicPosFim.qtde_Pos_Fim = quantidadeFinal;
                d.getAplic.getAplicPosFim.merc_Pos_Fim = valorFinal;

                // Adiciona Demais_N_Codif
                arq.getCorpo.getInform.getListAtiv.getDemais_N_Codif.Add(d);  
            }
            #endregion

            #region Futuros/Opções Vendidos sem data de vigência definida
            posicaoBMFHistoricoQuery = new PosicaoBMFHistoricoQuery("P");
            ativoBMFQuery = new AtivoBMFQuery("A");

            posicaoBMFHistoricoCollection = new PosicaoBMFHistoricoCollection();
            posicaoBMFHistoricoQuery.Select(posicaoBMFHistoricoQuery.TipoMercado,
                                           posicaoBMFHistoricoQuery.CdAtivoBMF,
                                           posicaoBMFHistoricoQuery.Serie,
                                           posicaoBMFHistoricoQuery.Quantidade.Sum(),
                                           posicaoBMFHistoricoQuery.ValorMercado.Sum());
            posicaoBMFHistoricoQuery.InnerJoin(ativoBMFQuery).On(ativoBMFQuery.CdAtivoBMF == posicaoBMFHistoricoQuery.CdAtivoBMF &
                                                                 ativoBMFQuery.Serie == posicaoBMFHistoricoQuery.Serie);
            posicaoBMFHistoricoQuery.Where(posicaoBMFHistoricoQuery.IdCliente.Equal(idCarteira) &
                                          posicaoBMFHistoricoQuery.DataHistorico.Equal(dataPosicao) &
                                          posicaoBMFHistoricoQuery.Quantidade.LessThan(0) &
                                          (ativoBMFQuery.DataFimVigencia.IsNull() | ativoBMFQuery.DataFimVigencia.Equal(dataIndefinida)));
            posicaoBMFHistoricoQuery.GroupBy(posicaoBMFHistoricoQuery.TipoMercado,
                                            posicaoBMFHistoricoQuery.CdAtivoBMF,
                                            posicaoBMFHistoricoQuery.Serie);
            posicaoBMFHistoricoQuery.OrderBy(posicaoBMFHistoricoQuery.TipoMercado.Ascending,
                                            posicaoBMFHistoricoQuery.CdAtivoBMF.Ascending);
            posicaoBMFHistoricoCollection.Load(posicaoBMFHistoricoQuery);

            foreach (PosicaoBMFHistorico posicaoBMFHistorico in posicaoBMFHistoricoCollection)
            {
                string cdAtivoBMF = posicaoBMFHistorico.CdAtivoBMF;
                string serie = posicaoBMFHistorico.Serie;
                byte tipoMercado = posicaoBMFHistorico.TipoMercado.Value;

                decimal quantidadeFinal = posicaoBMFHistorico.Quantidade.HasValue ? posicaoBMFHistorico.Quantidade.Value : 0;
                decimal valorFinal = (posicaoBMFHistorico.TipoMercado.Value != (byte)TipoMercadoBMF.Futuro && posicaoBMFHistorico.ValorMercado.HasValue) ? posicaoBMFHistorico.ValorMercado.Value : 0;

                AtivoBMF ativoBMF = new AtivoBMF();
                ativoBMF.LoadByPrimaryKey(cdAtivoBMF, serie);

                string codigoAtivo = ativoBMF.CodigoCDA.HasValue ? Convert.ToString(ativoBMF.CodigoCDA) : Convert.ToString((int)TipoAtivoCDA.ContratoFuturo);
                d = new Demais_N_Codif();
                d.cod_Tp_Ativ = codigoAtivo;

                d.descricao = this.RetornaAtivoFuturoCDA(ativoBMF);

                // Demais_N_Codif - Emis
                d.getEmis.tp_PF_PJ = string.Empty;
                d.getEmis.nr_PF_PJ = string.Empty;
                d.getEmis.nm = string.Empty;  

                // Demais_N_Codif - Aplic
                d.getAplic.cod_Tp_Aplic = Convert.ToString((int)TipoAplicacaoCDA.MercadoFuturoVendido);
                d.getAplic.qtde_Dias_Confid = this.diasConfidencialidade.ToString();

                // Demais_N_Codif - Aplic - Negoc
                d.getAplic.getAplicNegoc.tipoNegocio = Convert.ToString((int)TipoNegociacaoCDA.Negociacao);

                // Demais_N_Codif - Aplic - Pos_FIM
                d.getAplic.aplic_Pos_Fim.VersaoCDA("3");
                d.getAplic.getAplicPosFim.qtde_Pos_Fim = quantidadeFinal;
                d.getAplic.getAplicPosFim.merc_Pos_Fim = valorFinal;

                // Adiciona Demais_N_Codif
                arq.getCorpo.getInform.getListAtiv.getDemais_N_Codif.Add(d);  
            }
            #endregion

            #region For Monta Titulos publicos (Compras Finais)
            PosicaoRendaFixaHistoricoQuery posicaoRendaFixaHistoricoQuery = new PosicaoRendaFixaHistoricoQuery("P");
            TituloRendaFixaQuery tituloRendaFixaQuery = new TituloRendaFixaQuery("T");
            PapelRendaFixaQuery papelRendaFixaQuery = new PapelRendaFixaQuery("A");
            posicaoRendaFixaHistoricoQuery.Select(tituloRendaFixaQuery.Descricao,
                                                  tituloRendaFixaQuery.DataEmissao,
                                                  tituloRendaFixaQuery.DataVencimento,
                                                  posicaoRendaFixaHistoricoQuery.IdTitulo,
                                                  posicaoRendaFixaHistoricoQuery.TipoNegociacao,
                                                  posicaoRendaFixaHistoricoQuery.Quantidade.Sum(),
                                                  posicaoRendaFixaHistoricoQuery.ValorMercado.Sum());
            posicaoRendaFixaHistoricoQuery.InnerJoin(tituloRendaFixaQuery).On(tituloRendaFixaQuery.IdTitulo == posicaoRendaFixaHistoricoQuery.IdTitulo);
            posicaoRendaFixaHistoricoQuery.InnerJoin(papelRendaFixaQuery).On(papelRendaFixaQuery.IdPapel == tituloRendaFixaQuery.IdPapel);
            posicaoRendaFixaHistoricoQuery.Where(posicaoRendaFixaHistoricoQuery.IdCliente.Equal(idCarteira) &
                                        posicaoRendaFixaHistoricoQuery.DataHistorico.Equal(dataPosicao) &
                                        posicaoRendaFixaHistoricoQuery.TipoOperacao.Equal((byte)TipoOperacaoTitulo.CompraFinal) &
                                        papelRendaFixaQuery.TipoPapel.Equal((byte)TipoPapelTitulo.Publico) &
                                        (tituloRendaFixaQuery.CodigoCustodia.IsNull() | tituloRendaFixaQuery.CodigoCustodia.Equal("")));
            posicaoRendaFixaHistoricoQuery.GroupBy(tituloRendaFixaQuery.Descricao,
                                                   tituloRendaFixaQuery.DataEmissao,
                                                   tituloRendaFixaQuery.DataVencimento, 
                                                   posicaoRendaFixaHistoricoQuery.IdTitulo,
                                                   posicaoRendaFixaHistoricoQuery.TipoNegociacao);

            PosicaoRendaFixaHistoricoCollection coll = new PosicaoRendaFixaHistoricoCollection();
            coll.Load(posicaoRendaFixaHistoricoQuery);

            foreach (PosicaoRendaFixaHistorico p in coll)
            {
                string descricaoTitulo = Convert.ToString(p.GetColumn(TituloRendaFixaMetadata.ColumnNames.Descricao));
                DateTime dataEmissao = Convert.ToDateTime(p.GetColumn(TituloRendaFixaMetadata.ColumnNames.DataEmissao));
                DateTime dataVencimento = Convert.ToDateTime(p.GetColumn(TituloRendaFixaMetadata.ColumnNames.DataVencimento));
                
                byte tipoNegociacao = p.TipoNegociacao.Value;

                decimal quantidade = p.Quantidade.Value;
                decimal valor = p.ValorMercado.Value;

                decimal quantidadeAplicacao = 0;
                decimal valorAplicacao = 0;

                decimal quantidadeResgate = 0;
                decimal valorResgate = 0;

                if (tipoNegociacao != (byte)TipoNegociacaoTitulo.Vencimento) //Vencimento desconsidera de forma sumária qq movimentação, já que por definição não pode operar
                {
                    OperacaoRendaFixa operacaoRendaFixa = new OperacaoRendaFixa();
                    operacaoRendaFixa.Query.Select(operacaoRendaFixa.Query.Quantidade.Sum(),
                                                   operacaoRendaFixa.Query.Valor.Sum());
                    operacaoRendaFixa.Query.Where(operacaoRendaFixa.Query.IdCliente.Equal(idCarteira),
                                                  operacaoRendaFixa.Query.IdTitulo.Equal(p.IdTitulo.Value),
                                                  operacaoRendaFixa.Query.DataOperacao.GreaterThanOrEqual(dataInicioMes),
                                                  operacaoRendaFixa.Query.DataOperacao.LessThanOrEqual(dataPosicao),
                                                  operacaoRendaFixa.Query.TipoOperacao.In((byte)TipoOperacaoTitulo.CompraRevenda));
                    operacaoRendaFixa.Query.Load();

                    if (operacaoRendaFixa.Quantidade.HasValue)
                    {
                        quantidadeAplicacao = operacaoRendaFixa.Quantidade.Value;
                        valorAplicacao = operacaoRendaFixa.Valor.Value;
                    }


                    operacaoRendaFixa = new OperacaoRendaFixa();
                    operacaoRendaFixa.Query.Select(operacaoRendaFixa.Query.Quantidade.Sum(),
                                                   operacaoRendaFixa.Query.Valor.Sum());
                    operacaoRendaFixa.Query.Where(operacaoRendaFixa.Query.IdCliente.Equal(idCarteira),
                                                  operacaoRendaFixa.Query.IdTitulo.Equal(p.IdTitulo.Value),
                                                  operacaoRendaFixa.Query.DataOperacao.GreaterThanOrEqual(dataInicioMes),
                                                  operacaoRendaFixa.Query.DataOperacao.LessThanOrEqual(dataPosicao),
                                                  operacaoRendaFixa.Query.TipoOperacao.In((byte)TipoOperacaoTitulo.VendaFinal,
                                                                                          (byte)TipoOperacaoTitulo.VendaTotal));
                    operacaoRendaFixa.Query.Load();

                    if (operacaoRendaFixa.Quantidade.HasValue)
                    {
                        quantidadeResgate = operacaoRendaFixa.Quantidade.Value;
                        valorResgate = operacaoRendaFixa.Valor.Value;
                    }
                }
                
                d = new Demais_N_Codif();
                string cnpjTesouro = "55730881000198"; //CNPJ forçado do Tesouro Nacional
                d.cod_Tp_Ativ = Convert.ToString((int)TipoAtivoCDA.TituloPublicoFederal);
                string dataEmissaoString = dataEmissao.Day.ToString().PadLeft(2, '0') + dataEmissao.Month.ToString().PadLeft(2, '0') + Utilitario.Right(dataEmissao.Year.ToString(), 2);
                string dataVencimentoString = dataVencimento.Day.ToString().PadLeft(2, '0') + dataVencimento.Month.ToString().PadLeft(2, '0') + Utilitario.Right(dataVencimento.Year.ToString(), 2);
                d.descricao = descricaoTitulo + "/STNC/" + dataEmissaoString + "/" + dataVencimentoString + "/" + cnpjTesouro;
                // Demais_N_Codif - Emis
                if (!string.IsNullOrEmpty(cnpjTesouro))
                {
                    d.getEmis.tp_PF_PJ = "PJ";
                    d.getEmis.nr_PF_PJ = cnpjTesouro;
                    d.getEmis.nm = "Tesouro Nacional";
                }
                else
                {
                    d.getEmis.tp_PF_PJ = string.Empty;
                    d.getEmis.nr_PF_PJ = string.Empty;
                    d.getEmis.nm = string.Empty;  
                }
                // Demais_N_Codif - Aplic
                d.getAplic.cod_Tp_Aplic = Convert.ToString((int)TipoAplicacaoCDA.TitulosPublicos);
                d.getAplic.empr_Ligada = String.Empty;
                d.getAplic.qtde_Dias_Confid = this.diasConfidencialidade.ToString();

                // Aplic - Negoc
                d.getAplic.getAplicNegoc.tipoNegocio = p.TipoNegociacao.Value == (byte)TipoNegociacaoTitulo.Vencimento ?
                                        Convert.ToString((int)TipoNegociacaoCDA.MantidoVencimento) : Convert.ToString((int)TipoNegociacaoCDA.Negociacao);
                d.getAplic.getAplicNegoc.quantidadeVendasNegocio = quantidadeResgate;
                d.getAplic.getAplicNegoc.valorVendasNegocio = valorResgate;
                d.getAplic.getAplicNegoc.quantidadeAquisicaoNegocio = quantidadeAplicacao;
                d.getAplic.getAplicNegoc.valorAquisicaoNegocio = valorAplicacao;

                // Aplic - Pos_Fim
                d.getAplic.aplic_Pos_Fim.VersaoCDA("3");
                d.getAplic.getAplicPosFim.qtde_Pos_Fim = quantidade;
                d.getAplic.getAplicPosFim.merc_Pos_Fim = valor;
                // Adiciona Demais_N_Codif
                arq.getCorpo.getInform.getListAtiv.getDemais_N_Codif.Add(d);
            }
            #endregion

            #region For Monta Titulos publicos (Compras Revenda)
            posicaoRendaFixaHistoricoQuery = new PosicaoRendaFixaHistoricoQuery("P");
            tituloRendaFixaQuery = new TituloRendaFixaQuery("T");
            papelRendaFixaQuery = new PapelRendaFixaQuery("A");
            posicaoRendaFixaHistoricoQuery.Select(papelRendaFixaQuery.Descricao,
                                                  posicaoRendaFixaHistoricoQuery.DataOperacao,
                                                  posicaoRendaFixaHistoricoQuery.DataVolta,
                                                  posicaoRendaFixaHistoricoQuery.Quantidade.Sum(),
                                                  posicaoRendaFixaHistoricoQuery.ValorMercado.Sum());
            posicaoRendaFixaHistoricoQuery.InnerJoin(tituloRendaFixaQuery).On(tituloRendaFixaQuery.IdTitulo == posicaoRendaFixaHistoricoQuery.IdTitulo);
            posicaoRendaFixaHistoricoQuery.InnerJoin(papelRendaFixaQuery).On(papelRendaFixaQuery.IdPapel == tituloRendaFixaQuery.IdPapel);
            posicaoRendaFixaHistoricoQuery.Where(posicaoRendaFixaHistoricoQuery.IdCliente.Equal(idCarteira) &
                                        posicaoRendaFixaHistoricoQuery.DataHistorico.Equal(dataPosicao) &
                                        posicaoRendaFixaHistoricoQuery.TipoOperacao.Equal((byte)TipoOperacaoTitulo.CompraRevenda) &
                                        papelRendaFixaQuery.TipoPapel.Equal((byte)TipoPapelTitulo.Publico) &
                                        (tituloRendaFixaQuery.CodigoCustodia.IsNull() | tituloRendaFixaQuery.CodigoCustodia.Equal("")));
            posicaoRendaFixaHistoricoQuery.GroupBy(papelRendaFixaQuery.Descricao,
                                                   posicaoRendaFixaHistoricoQuery.DataOperacao,
                                                   posicaoRendaFixaHistoricoQuery.DataVolta);

            coll = new PosicaoRendaFixaHistoricoCollection();
            coll.Load(posicaoRendaFixaHistoricoQuery);

            foreach (PosicaoRendaFixaHistorico p in coll)
            {
                string descricao = Convert.ToString(p.GetColumn(PapelRendaFixaMetadata.ColumnNames.Descricao));
                DateTime dataOperacao = p.DataOperacao.Value;
                DateTime dataVolta = p.DataVolta.Value;

                decimal quantidade = p.Quantidade.Value;
                decimal valor = p.ValorMercado.Value;
                
                d = new Demais_N_Codif();
                string cnpjTesouro = "55730881000198"; //CNPJ forçado do Tesouro Nacional
                d.cod_Tp_Ativ = Convert.ToString((int)TipoAtivoCDA.TituloPublicoFederal);
                string dataOperacaoString = dataOperacao.Day.ToString().PadLeft(2, '0') + dataOperacao.Month.ToString().PadLeft(2, '0') + Utilitario.Right(dataOperacao.Year.ToString(), 2);
                string dataVoltaString = dataVolta.Day.ToString().PadLeft(2, '0') + dataVolta.Month.ToString().PadLeft(2, '0') + Utilitario.Right(dataVolta.Year.ToString(), 2);
                d.descricao = descricao + "/STNC/" + dataOperacaoString + "/" + dataVoltaString + "/" + cnpjTesouro;
                // Demais_N_Codif - Emis
                if (!string.IsNullOrEmpty(cnpjTesouro))
                {
                    d.getEmis.tp_PF_PJ = "PJ";
                    d.getEmis.nr_PF_PJ = cnpjTesouro;
                    d.getEmis.nm = "Tesouro Nacional";
                }
                else
                {
                    // Demais_N_Codif - Emis
                    d.getEmis.tp_PF_PJ = string.Empty;
                    d.getEmis.nr_PF_PJ = string.Empty;
                    d.getEmis.nm = string.Empty;  
                }

                // Demais_N_Codif - Aplic
                d.getAplic.cod_Tp_Aplic = Convert.ToString((int)TipoAplicacaoCDA.OperacoesCompromissadas);
                d.getAplic.empr_Ligada = "N";
                d.getAplic.qtde_Dias_Confid = this.diasConfidencialidade.ToString();

                // Aplic - Negoc
                d.getAplic.getAplicNegoc.tipoNegocio = Convert.ToString((int)TipoNegociacaoCDA.Negociacao);
                
                // Aplic - Pos_Fim
                d.getAplic.aplic_Pos_Fim.VersaoCDA("3");
                d.getAplic.getAplicPosFim.qtde_Pos_Fim = quantidade;
                d.getAplic.getAplicPosFim.merc_Pos_Fim = valor;
                // Adiciona Demais_N_Codif
                arq.getCorpo.getInform.getListAtiv.getDemais_N_Codif.Add(d);
            }
            #endregion

            #region For Monta Titulos privados
            #region Consulta
            posicaoRendaFixaHistoricoQuery = new PosicaoRendaFixaHistoricoQuery("P");
            tituloRendaFixaQuery = new TituloRendaFixaQuery("T");
            papelRendaFixaQuery = new PapelRendaFixaQuery("A");
            posicaoRendaFixaHistoricoQuery.Select(tituloRendaFixaQuery.IdEmissor,
                                                  tituloRendaFixaQuery.Descricao,
                                                  tituloRendaFixaQuery.DataEmissao,
                                                  tituloRendaFixaQuery.DataVencimento,
                                                  posicaoRendaFixaHistoricoQuery.IdTitulo,
                                                  posicaoRendaFixaHistoricoQuery.TipoNegociacao,
                                                  papelRendaFixaQuery.Classe,
                                                  posicaoRendaFixaHistoricoQuery.Quantidade.Sum(),
                                                  posicaoRendaFixaHistoricoQuery.ValorMercado.Sum());
            posicaoRendaFixaHistoricoQuery.InnerJoin(tituloRendaFixaQuery).On(tituloRendaFixaQuery.IdTitulo == posicaoRendaFixaHistoricoQuery.IdTitulo);
            posicaoRendaFixaHistoricoQuery.InnerJoin(papelRendaFixaQuery).On(papelRendaFixaQuery.IdPapel == tituloRendaFixaQuery.IdPapel);
            posicaoRendaFixaHistoricoQuery.Where(posicaoRendaFixaHistoricoQuery.IdCliente.Equal(idCarteira) &
                                        posicaoRendaFixaHistoricoQuery.DataHistorico.Equal(dataPosicao) &
                                        posicaoRendaFixaHistoricoQuery.TipoOperacao.Equal((byte)TipoOperacaoTitulo.CompraFinal) &
                                        papelRendaFixaQuery.TipoPapel.Equal((byte)TipoPapelTitulo.Privado) &
                                        (tituloRendaFixaQuery.CodigoCDA.IsNull() | tituloRendaFixaQuery.CodigoCDA.Equal(0)));
            posicaoRendaFixaHistoricoQuery.GroupBy(tituloRendaFixaQuery.IdEmissor,
                                                   tituloRendaFixaQuery.Descricao,
                                                   tituloRendaFixaQuery.DataEmissao,
                                                   tituloRendaFixaQuery.DataVencimento,
                                                   posicaoRendaFixaHistoricoQuery.IdTitulo,
                                                   posicaoRendaFixaHistoricoQuery.TipoNegociacao,
                                                   papelRendaFixaQuery.Classe);

            coll = new PosicaoRendaFixaHistoricoCollection();
            coll.Load(posicaoRendaFixaHistoricoQuery);
            #endregion

            if (coll.Count > 0)
            {
                int i = 0;
                PosicaoRendaFixaHistorico p = coll[i];

                #region idTitulo/classe/dataEmissao/dataVencimento/tipoNegociacao/idEmissor
                int idTitulo = p.IdTitulo.Value;

                string descricaoTitulo = Convert.ToString(p.GetColumn(TituloRendaFixaMetadata.ColumnNames.Descricao));
                int classe = Convert.ToInt32(p.GetColumn(PapelRendaFixaMetadata.ColumnNames.Classe));
                DateTime dataEmissao = Convert.ToDateTime(p.GetColumn(TituloRendaFixaMetadata.ColumnNames.DataEmissao));
                DateTime dataVencimento = p.DataVencimento.Value;
                byte tipoNegociacao = p.TipoNegociacao.Value;
                int idEmissor = Convert.ToInt32(p.GetColumn(TituloRendaFixaMetadata.ColumnNames.IdEmissor));
                #endregion

                string chave = classe.ToString() + "|" + idEmissor.ToString() + "|" + dataEmissao.ToShortDateString() + "|" + dataVencimento.ToShortDateString() + "|" +
                               tipoNegociacao.ToString();

                int classeAnterior = classe;
                int idEmissorAnterior = idEmissor;
                DateTime dataEmissaoAnterior = dataEmissao;
                DateTime dataVencimentoAnterior = dataVencimento;
                byte tipoNegociacaoAnterior = tipoNegociacao;
                
                while (i < coll.Count)
                {
                    string chaveAnterior = chave;

                    classeAnterior = classe;
                    idEmissorAnterior = idEmissor;
                    dataEmissaoAnterior = dataEmissao;
                    dataVencimentoAnterior = dataVencimento;
                    tipoNegociacaoAnterior = tipoNegociacao;

                    decimal totalQuantidade = 0, totalValor = 0, totalQuantidadeAplicacao = 0, totalValorAplicacao = 0, totalQuantidadeResgate = 0, totalValorResgate = 0;
                    while (chave == chaveAnterior)
                    {
                        totalQuantidade += p.Quantidade.Value;
                        totalValor += p.ValorMercado.Value;

                        #region Acumula qtdes e valores de operacao (somente tipoNegociacao = Vencimento)
                        if (tipoNegociacao != (byte)TipoNegociacaoTitulo.Vencimento) //Vencimento desconsidera de forma sumária qq movimentação, já que por definição não pode operar
                        {
                            OperacaoRendaFixa operacaoRendaFixa = new OperacaoRendaFixa();
                            operacaoRendaFixa.Query.Select(operacaoRendaFixa.Query.Quantidade.Sum(),
                                                           operacaoRendaFixa.Query.Valor.Sum());
                            operacaoRendaFixa.Query.Where(operacaoRendaFixa.Query.IdCliente.Equal(idCarteira),
                                                          operacaoRendaFixa.Query.IdTitulo.Equal(p.IdTitulo.Value),
                                                          operacaoRendaFixa.Query.DataOperacao.GreaterThanOrEqual(dataInicioMes),
                                                          operacaoRendaFixa.Query.DataOperacao.LessThanOrEqual(dataPosicao),
                                                          operacaoRendaFixa.Query.TipoOperacao.Equal((byte)TipoOperacaoTitulo.CompraFinal));
                            operacaoRendaFixa.Query.Load();

                            if (operacaoRendaFixa.Quantidade.HasValue)
                            {
                                decimal quantidadeAplicacao = operacaoRendaFixa.Quantidade.Value;
                                decimal valorAplicacao = operacaoRendaFixa.Valor.Value;

                                totalQuantidadeAplicacao += quantidadeAplicacao;
                                totalValorAplicacao += valorAplicacao;
                            }


                            operacaoRendaFixa = new OperacaoRendaFixa();
                            operacaoRendaFixa.Query.Select(operacaoRendaFixa.Query.Quantidade.Sum(),
                                                           operacaoRendaFixa.Query.Valor.Sum());
                            operacaoRendaFixa.Query.Where(operacaoRendaFixa.Query.IdCliente.Equal(idCarteira),
                                                          operacaoRendaFixa.Query.IdTitulo.Equal(p.IdTitulo.Value),
                                                          operacaoRendaFixa.Query.DataOperacao.GreaterThanOrEqual(dataInicioMes),
                                                          operacaoRendaFixa.Query.DataOperacao.LessThanOrEqual(dataPosicao),
                                                          operacaoRendaFixa.Query.TipoOperacao.In((byte)TipoOperacaoTitulo.VendaFinal,
                                                                                                  (byte)TipoOperacaoTitulo.VendaTotal));
                            operacaoRendaFixa.Query.Load();

                            if (operacaoRendaFixa.Quantidade.HasValue)
                            {
                                decimal quantidadeResgate = operacaoRendaFixa.Quantidade.Value;
                                decimal valorResgate = operacaoRendaFixa.Valor.Value;

                                totalQuantidadeResgate += quantidadeResgate;
                                totalValorResgate += valorResgate;
                            }
                        }
                        #endregion

                        i += 1;

                        if (i < coll.Count)
                        {
                            p = coll[i];

                            #region Valores para nova chave
                            idTitulo = p.IdTitulo.Value;
                            classe = Convert.ToInt32(p.GetColumn(PapelRendaFixaMetadata.ColumnNames.Classe));
                            dataEmissao = Convert.ToDateTime(p.GetColumn(TituloRendaFixaMetadata.ColumnNames.DataEmissao));
                            dataVencimento = p.DataVencimento.Value;
                            tipoNegociacao = p.TipoNegociacao.Value;
                            idEmissor = Convert.ToInt32(p.GetColumn(TituloRendaFixaMetadata.ColumnNames.IdEmissor));
                            #endregion

                            chave = classe.ToString() + "|" + idEmissor.ToString() + "|" + dataEmissao.ToShortDateString() + "|" + dataVencimento.ToShortDateString() + "|" +
                                    tipoNegociacao.ToString(); 
                        }
                        else
                        {
                            break;
                        }
                    }

                    #region Preenche Demais_N_Codif (Titulos privados)
                    d = new Demais_N_Codif();

                    Emissor emissor = new Emissor();
                    emissor.LoadByPrimaryKey(idEmissorAnterior);

                    // Demais_N_Codif - Emis
                    string cnpj = string.Empty;
                    if (!String.IsNullOrEmpty(emissor.Cnpj) && !String.IsNullOrEmpty(emissor.Nome.Trim()))
                    {
                        cnpj = d.getEmis.nr_PF_PJ = !String.IsNullOrEmpty(emissor.Cnpj) ? Utilitario.RemoveCaracteresEspeciais(emissor.Cnpj) : "";
                        d.getEmis.tp_PF_PJ = "PJ";
                        d.getEmis.nm = emissor.Nome.Trim();
                    }
                    else
                    {
                        // Demais_N_Codif - Emis
                        d.getEmis.tp_PF_PJ = string.Empty;
                        d.getEmis.nr_PF_PJ = string.Empty;
                        d.getEmis.nm = string.Empty;  
                    }

                    d.cod_Tp_Ativ = Convert.ToString((int)TipoAtivoCDA.Outros);
                    string dataEmissaoString = dataEmissaoAnterior.Day.ToString().PadLeft(2, '0') + dataEmissaoAnterior.Month.ToString().PadLeft(2, '0') + Utilitario.Right(dataEmissaoAnterior.Year.ToString(), 2);
                    string dataVencimentoString = dataVencimentoAnterior.Day.ToString().PadLeft(2, '0') + dataVencimentoAnterior.Month.ToString().PadLeft(2, '0') + Utilitario.Right(dataVencimentoAnterior.Year.ToString(), 2);
                    d.descricao = idTitulo + "/" + descricaoTitulo + "/" + emissor.Nome.Trim() + "/" + dataEmissaoString + "/" + dataVencimentoString + "/" + cnpj;

                    // Demais_N_Codif - Aplic
                    d.getAplic.cod_Tp_Aplic = Convert.ToString((int)TipoAplicacaoCDA.OutrasAplicacoes);

                    bool empresaLigada = this.IndicaEmpresaLigadaTitulo(idEmissorAnterior, carteira.IdAgenteAdministrador.Value, carteira.IdAgenteGestor.Value);
                    d.getAplic.empr_Ligada = empresaLigada ? "S" : "N";

                    d.getAplic.qtde_Dias_Confid = this.diasConfidencialidade.ToString();

                    // Aplic - Negoc
                    d.getAplic.getAplicNegoc.tipoNegocio = p.TipoNegociacao.Value == (byte)TipoNegociacaoTitulo.Vencimento ?
                                            Convert.ToString((int)TipoNegociacaoCDA.MantidoVencimento) : Convert.ToString((int)TipoNegociacaoCDA.Negociacao);
                    d.getAplic.getAplicNegoc.quantidadeVendasNegocio = totalQuantidadeResgate;
                    d.getAplic.getAplicNegoc.valorVendasNegocio = totalValorResgate;
                    d.getAplic.getAplicNegoc.quantidadeAquisicaoNegocio = totalQuantidadeAplicacao;
                    d.getAplic.getAplicNegoc.valorAquisicaoNegocio = totalValorAplicacao;

                    // Aplic - Pos_Fim
                    d.getAplic.aplic_Pos_Fim.VersaoCDA("3");
                    d.getAplic.getAplicPosFim.qtde_Pos_Fim = totalQuantidade;
                    d.getAplic.getAplicPosFim.merc_Pos_Fim = totalValor;
                    // Adiciona Demais_N_Codif
                    arq.getCorpo.getInform.getListAtiv.getDemais_N_Codif.Add(d);
                    #endregion
                }
            }
            #endregion
        }
        #endregion
   }

    public class CDA20   //CDA 2.0 
    {

        int diasConfidencialidade = 0;

        public enum TipoAtivoCDA
        {
            AcaoOrdinaria = 1, //TipoMercado = VIS, Acao ON
            AcaoPreferencial = 2, //TipoMercado = VIS, Acao PN, PNA, PNB...
            OpcaoCompra = 3, //TipoMercado = OPC
            OpcaoVenda = 4, //TipoMercado = OPV
            Termo = 5,
            DebentureSimples = 6, //Papel = Debenture (hoje será tudo listado como debentures simples)
            DebentureConversivel = 7, //Papel = Debenture (hoje será tudo listado como debentures simples)
            TituloPublico = 8,
            ContratoFuturo = 9, //Futuros BMF
            CertificadoDepositoAcoes = 11, //TipoPapelAtivo = CertificadoDepositoAcoes
            ReciboDepositoAcoes = 12, //TipoPapelAtivo = ReciboDepositoAcoes
            AcaoReciboAcaoMercosul = 13, //TRATADO COMO AÇÃO
            BonusSubscricao = 14, //TipoPapelAtivo = BonusSubscricao
            NotaPromissoria_CommercialPaper_Exportnote = 15, //****************RENDA FIXA PRIVADA, VERIFICAR!!!!
            CertificadoInvestimentoAudiovisual = 17, //****************RENDA FIXA PRIVADA, VERIFICAR!!!!
            CertificadoRecebivelImobiliario = 18, //****************RENDA FIXA PRIVADA, VERIFICAR!!!!
            Titulo_ContratoInvestimentoColetivo = 19, //****************RENDA FIXA PRIVADA, VERIFICAR!!!!
            DepositoryReceiptExterior = 20, //TRATADO COMO AÇÃO
            ReciboSubscricao = 21, //TipoPapelAtivo = ReciboSubscricao
            CotasFundos = 22,
            BonusPrivado = 23, //TRATADO COMO AÇÃO
            CertificadoTermoEnergiaEletrica = 24, //****************RENDA FIXA PRIVADA, VERIFICAR!!!!
            CertificadoPrivatizacao = 25, //****************RENDA FIXA PRIVADA, VERIFICAR!!!!
            SWAP = 26, //PosicaoSwap
            TituloDividaExterna = 27, //****************RENDA FIXA PRIVADA, VERIFICAR!!!!
            Outros = 999
        }

        public enum TipoAplicacaoCDA
        {
            Acoes = 37,
            OpcoesPosicaoTitular = 39,
            OpcoesPosicaoLancada = 40,
            ComprasTermo = 42,
            VendasTermo = 43,
            DebentureConversivel = 44,
            DebentureSimples = 45,
            OutrasAplicacoes = 48,
            DepositoPrazo_OutrosTitulosIF = 74,
            TitulosPublicos = 75,
            CotasFundosRV = 92,
            CotasFundosRF = 190,
            Disponibilidades = 94,
            ValoresPagar = 96,
            ValoresReceber = 97,
            MercadoFuturoComprado = 102,
            MercadoFuturoVendido = 103,
            TVMCedidoEmprestimo = 143,
            TVMRecebidoEmprestimo = 146,
            OperacoesCompromissadas = 147,
            Certificado_ReciboDeposito = 151,
            OutrosValoresMobiliarios_OfertaPublica = 152,
            DiferencialSwapReceber = 183,
            DiferencialSwapPagar = 184,
            CPR = 185,
            AcoesRecebidasBonificacao = 186,
            DisponivelOuro = 187,
            InvestimentoExterior = 189,
            CotasFundos409 = 190            
        }

        public enum TipoNegociacaoCDA
        {
            Negociacao = 1,
            MantidoVencimento = 2
        }

        private string RetornaAtivoFuturoCDA(AtivoBMF ativoBMF)
        {
            string ativo = "";

            if (ativoBMF.TipoMercado == (byte)TipoMercadoBMF.Futuro)
            {
                ativo = ativoBMF.CdAtivoBMF + "FUT" + ativoBMF.Serie;
            }
            else if (ativoBMF.TipoMercado == (byte)TipoMercadoBMF.OpcaoDisponivel)
            {
                ativo = ativoBMF.CdAtivoBMF + "OPD" + ativoBMF.Serie;
            }
            else if (ativoBMF.TipoMercado == (byte)TipoMercadoBMF.OpcaoFuturo)
            {
                ativo = ativoBMF.CdAtivoBMF + "OPF" + ativoBMF.Serie;
            }
            else if (ativoBMF.TipoMercado == (byte)TipoMercadoBMF.Disponivel)
            {
                ativo = ativoBMF.CdAtivoBMF + "DIS" + ativoBMF.Serie;
            }
            else if (ativoBMF.TipoMercado == (byte)TipoMercadoBMF.Termo)
            {
                ativo = ativoBMF.CdAtivoBMF + "TER" + ativoBMF.Serie;
            }

            return ativo;
        }

        private string RetornaCodigoSwap(PosicaoSwapHistorico posicaoSwapHistorico)
        {
            #region Ponta Pre
            if (posicaoSwapHistorico.TipoPonta.Value == (byte)TipoPontaSwap.PreFixado)
            {
                if (posicaoSwapHistorico.IdIndiceContraParte.Value == (short)ListaIndiceFixo.CDI)
                {
                    return "SDP";
                }
                else if (posicaoSwapHistorico.IdIndiceContraParte.Value == (short)ListaIndiceFixo.EURO)
                {
                    return "SEP";
                }
                else if (posicaoSwapHistorico.IdIndiceContraParte.Value == (short)ListaIndiceFixo.IBOVESPA_FECHA)
                {
                    return "SNP";
                }
                else if (posicaoSwapHistorico.IdIndiceContraParte.Value == (short)ListaIndiceFixo.IGPDI)
                {
                    return "SIP";
                }
                else if (posicaoSwapHistorico.IdIndiceContraParte.Value == (short)ListaIndiceFixo.IGPM)
                {
                    return "SMP";
                }
                else if (posicaoSwapHistorico.IdIndiceContraParte.Value == (short)ListaIndiceFixo.INPC)
                {
                    return "SXP";
                }
                else if (posicaoSwapHistorico.IdIndiceContraParte.Value == (short)ListaIndiceFixo.IPCA)
                {
                    return "SLP";
                }
                else if (posicaoSwapHistorico.IdIndiceContraParte.Value == (short)ListaIndiceFixo.PTAX_800VENDA)
                {
                    return "SCP";
                }
                else if (posicaoSwapHistorico.IdIndiceContraParte.Value == (short)ListaIndiceFixo.SELIC)
                {
                    return "SSP";
                }
                else if (posicaoSwapHistorico.IdIndiceContraParte.Value == (short)ListaIndiceFixo.TR)
                {
                    return "STP";
                }
            }
            #endregion
            #region Ponta CDI
            else if (posicaoSwapHistorico.TipoPonta.Value != (byte)TipoPontaSwap.PreFixado &&
                     posicaoSwapHistorico.IdIndice.Value == (short)ListaIndiceFixo.CDI)
            {
                if (posicaoSwapHistorico.TipoPonta.Value == (byte)TipoPontaSwap.PreFixado)
                {
                    return "SDP";
                }
                else if (posicaoSwapHistorico.IdIndiceContraParte.Value == (short)ListaIndiceFixo.EURO)
                {
                    return "SDE";
                }
                else if (posicaoSwapHistorico.IdIndiceContraParte.Value == (short)ListaIndiceFixo.IBOVESPA_FECHA)
                {
                    return "SDN";
                }
                else if (posicaoSwapHistorico.IdIndiceContraParte.Value == (short)ListaIndiceFixo.IGPDI)
                {
                    return "SDI";
                }
                else if (posicaoSwapHistorico.IdIndiceContraParte.Value == (short)ListaIndiceFixo.IGPM)
                {
                    return "SDM";
                }
                else if (posicaoSwapHistorico.IdIndiceContraParte.Value == (short)ListaIndiceFixo.INPC)
                {
                    return "SDX";
                }
                else if (posicaoSwapHistorico.IdIndiceContraParte.Value == (short)ListaIndiceFixo.IPCA)
                {
                    return "SDL";
                }
                else if (posicaoSwapHistorico.IdIndiceContraParte.Value == (short)ListaIndiceFixo.PTAX_800VENDA)
                {
                    return "SDC";
                }
                else if (posicaoSwapHistorico.IdIndiceContraParte.Value == (short)ListaIndiceFixo.SELIC)
                {
                    return "SDS";
                }
                else if (posicaoSwapHistorico.IdIndiceContraParte.Value == (short)ListaIndiceFixo.TR)
                {
                    return "SDT";
                }
            }
            #endregion
            #region Ponta Euro
            else if (posicaoSwapHistorico.TipoPonta.Value != (byte)TipoPontaSwap.PreFixado &&
                posicaoSwapHistorico.IdIndice.Value == (short)ListaIndiceFixo.EURO)
            {
                if (posicaoSwapHistorico.TipoPonta.Value == (byte)TipoPontaSwap.PreFixado)
                {
                    return "SEP";
                }
                else if (posicaoSwapHistorico.IdIndiceContraParte.Value == (short)ListaIndiceFixo.CDI)
                {
                    return "SDE";
                }
                else if (posicaoSwapHistorico.IdIndiceContraParte.Value == (short)ListaIndiceFixo.IBOVESPA_FECHA)
                {
                    return "SNE";
                }
                else if (posicaoSwapHistorico.IdIndiceContraParte.Value == (short)ListaIndiceFixo.IGPDI)
                {
                    return "SEI";
                }
                else if (posicaoSwapHistorico.IdIndiceContraParte.Value == (short)ListaIndiceFixo.IGPM)
                {
                    return "SME";
                }
                else if (posicaoSwapHistorico.IdIndiceContraParte.Value == (short)ListaIndiceFixo.INPC)
                {
                    return "SXE";
                }
                else if (posicaoSwapHistorico.IdIndiceContraParte.Value == (short)ListaIndiceFixo.IPCA)
                {
                    return "SLE";
                }
                else if (posicaoSwapHistorico.IdIndiceContraParte.Value == (short)ListaIndiceFixo.PTAX_800VENDA)
                {
                    return "SCE";
                }
                else if (posicaoSwapHistorico.IdIndiceContraParte.Value == (short)ListaIndiceFixo.SELIC)
                {
                    return "SSE";
                }
                else if (posicaoSwapHistorico.IdIndiceContraParte.Value == (short)ListaIndiceFixo.TR)
                {
                    return "STE";
                }
            }
            #endregion
            #region Ponta Ibovespa
            else if (posicaoSwapHistorico.TipoPonta.Value != (byte)TipoPontaSwap.PreFixado &&
                posicaoSwapHistorico.IdIndice.Value == (short)ListaIndiceFixo.IBOVESPA_FECHA)
            {
                if (posicaoSwapHistorico.TipoPonta.Value == (byte)TipoPontaSwap.PreFixado)
                {
                    return "SNP";
                }
                else if (posicaoSwapHistorico.IdIndiceContraParte.Value == (short)ListaIndiceFixo.CDI)
                {
                    return "SDN";
                }
                else if (posicaoSwapHistorico.IdIndiceContraParte.Value == (short)ListaIndiceFixo.EURO)
                {
                    return "SNE";
                }
                else if (posicaoSwapHistorico.IdIndiceContraParte.Value == (short)ListaIndiceFixo.IGPDI)
                {
                    return "SNI";
                }
                else if (posicaoSwapHistorico.IdIndiceContraParte.Value == (short)ListaIndiceFixo.IGPM)
                {
                    return "SNM";
                }
                else if (posicaoSwapHistorico.IdIndiceContraParte.Value == (short)ListaIndiceFixo.INPC)
                {
                    return "SNX";
                }
                else if (posicaoSwapHistorico.IdIndiceContraParte.Value == (short)ListaIndiceFixo.IPCA)
                {
                    return "SNL";
                }
                else if (posicaoSwapHistorico.IdIndiceContraParte.Value == (short)ListaIndiceFixo.PTAX_800VENDA)
                {
                    return "SCN";
                }
                else if (posicaoSwapHistorico.IdIndiceContraParte.Value == (short)ListaIndiceFixo.SELIC)
                {
                    return "SNS";
                }
                else if (posicaoSwapHistorico.IdIndiceContraParte.Value == (short)ListaIndiceFixo.TR)
                {
                    return "STN";
                }
            }
            #endregion
            #region Ponta IGPDI
            else if (posicaoSwapHistorico.TipoPonta.Value != (byte)TipoPontaSwap.PreFixado &&
                posicaoSwapHistorico.IdIndice.Value == (short)ListaIndiceFixo.IGPDI)
            {
                if (posicaoSwapHistorico.TipoPonta.Value == (byte)TipoPontaSwap.PreFixado)
                {
                    return "SIP";
                }
                else if (posicaoSwapHistorico.IdIndiceContraParte.Value == (short)ListaIndiceFixo.CDI)
                {
                    return "SDI";
                }
                else if (posicaoSwapHistorico.IdIndiceContraParte.Value == (short)ListaIndiceFixo.EURO)
                {
                    return "SEI";
                }
                else if (posicaoSwapHistorico.IdIndiceContraParte.Value == (short)ListaIndiceFixo.IBOVESPA_FECHA)
                {
                    return "SNI";
                }
                else if (posicaoSwapHistorico.IdIndiceContraParte.Value == (short)ListaIndiceFixo.IGPM)
                {
                    return "SMI";
                }
                else if (posicaoSwapHistorico.IdIndiceContraParte.Value == (short)ListaIndiceFixo.INPC)
                {
                    return "SIX";
                }
                else if (posicaoSwapHistorico.IdIndiceContraParte.Value == (short)ListaIndiceFixo.IPCA)
                {
                    return "SIL";
                }
                else if (posicaoSwapHistorico.IdIndiceContraParte.Value == (short)ListaIndiceFixo.PTAX_800VENDA)
                {
                    return "SCI";
                }
                else if (posicaoSwapHistorico.IdIndiceContraParte.Value == (short)ListaIndiceFixo.SELIC)
                {
                    return "SSI";
                }
                else if (posicaoSwapHistorico.IdIndiceContraParte.Value == (short)ListaIndiceFixo.TR)
                {
                    return "STI";
                }
            }
            #endregion
            #region Ponta IGPM
            else if (posicaoSwapHistorico.TipoPonta.Value != (byte)TipoPontaSwap.PreFixado &&
                posicaoSwapHistorico.IdIndice.Value == (short)ListaIndiceFixo.IGPM)
            {
                if (posicaoSwapHistorico.TipoPonta.Value == (byte)TipoPontaSwap.PreFixado)
                {
                    return "SMP";
                }
                else if (posicaoSwapHistorico.IdIndiceContraParte.Value == (short)ListaIndiceFixo.CDI)
                {
                    return "SDM";
                }
                else if (posicaoSwapHistorico.IdIndiceContraParte.Value == (short)ListaIndiceFixo.EURO)
                {
                    return "SME";
                }
                else if (posicaoSwapHistorico.IdIndiceContraParte.Value == (short)ListaIndiceFixo.IBOVESPA_FECHA)
                {
                    return "SNM";
                }
                else if (posicaoSwapHistorico.IdIndiceContraParte.Value == (short)ListaIndiceFixo.IGPDI)
                {
                    return "SMI";
                }
                else if (posicaoSwapHistorico.IdIndiceContraParte.Value == (short)ListaIndiceFixo.INPC)
                {
                    return "SMX";
                }
                else if (posicaoSwapHistorico.IdIndiceContraParte.Value == (short)ListaIndiceFixo.IPCA)
                {
                    return "SML";
                }
                else if (posicaoSwapHistorico.IdIndiceContraParte.Value == (short)ListaIndiceFixo.PTAX_800VENDA)
                {
                    return "SCM";
                }
                else if (posicaoSwapHistorico.IdIndiceContraParte.Value == (short)ListaIndiceFixo.SELIC)
                {
                    return "SSM";
                }
                else if (posicaoSwapHistorico.IdIndiceContraParte.Value == (short)ListaIndiceFixo.TR)
                {
                    return "STM";
                }
            }
            #endregion
            #region Ponta INPC
            else if (posicaoSwapHistorico.TipoPonta.Value != (byte)TipoPontaSwap.PreFixado &&
                posicaoSwapHistorico.IdIndice.Value == (short)ListaIndiceFixo.INPC)
            {
                if (posicaoSwapHistorico.TipoPonta.Value == (byte)TipoPontaSwap.PreFixado)
                {
                    return "SXP";
                }
                else if (posicaoSwapHistorico.IdIndiceContraParte.Value == (short)ListaIndiceFixo.CDI)
                {
                    return "SDX";
                }
                else if (posicaoSwapHistorico.IdIndiceContraParte.Value == (short)ListaIndiceFixo.EURO)
                {
                    return "SXE";
                }
                else if (posicaoSwapHistorico.IdIndiceContraParte.Value == (short)ListaIndiceFixo.IBOVESPA_FECHA)
                {
                    return "SNX";
                }
                else if (posicaoSwapHistorico.IdIndiceContraParte.Value == (short)ListaIndiceFixo.IGPDI)
                {
                    return "SIX";
                }
                else if (posicaoSwapHistorico.IdIndiceContraParte.Value == (short)ListaIndiceFixo.IGPM)
                {
                    return "SMX";
                }
                else if (posicaoSwapHistorico.IdIndiceContraParte.Value == (short)ListaIndiceFixo.IPCA)
                {
                    return "SXL";
                }
                else if (posicaoSwapHistorico.IdIndiceContraParte.Value == (short)ListaIndiceFixo.PTAX_800VENDA)
                {
                    return "SCX";
                }
                else if (posicaoSwapHistorico.IdIndiceContraParte.Value == (short)ListaIndiceFixo.SELIC)
                {
                    return "SSX";
                }
                else if (posicaoSwapHistorico.IdIndiceContraParte.Value == (short)ListaIndiceFixo.TR)
                {
                    return "STX";
                }
            }
            #endregion
            #region Ponta IPCA
            else if (posicaoSwapHistorico.TipoPonta.Value != (byte)TipoPontaSwap.PreFixado &&
                posicaoSwapHistorico.IdIndice.Value == (short)ListaIndiceFixo.IPCA)
            {
                if (posicaoSwapHistorico.TipoPonta.Value == (byte)TipoPontaSwap.PreFixado)
                {
                    return "SLP";
                }
                else if (posicaoSwapHistorico.IdIndiceContraParte.Value == (short)ListaIndiceFixo.CDI)
                {
                    return "SDL";
                }
                else if (posicaoSwapHistorico.IdIndiceContraParte.Value == (short)ListaIndiceFixo.EURO)
                {
                    return "SLE";
                }
                else if (posicaoSwapHistorico.IdIndiceContraParte.Value == (short)ListaIndiceFixo.IBOVESPA_FECHA)
                {
                    return "SNL";
                }
                else if (posicaoSwapHistorico.IdIndiceContraParte.Value == (short)ListaIndiceFixo.IGPDI)
                {
                    return "SIL";
                }
                else if (posicaoSwapHistorico.IdIndiceContraParte.Value == (short)ListaIndiceFixo.IGPM)
                {
                    return "SML";
                }
                else if (posicaoSwapHistorico.IdIndiceContraParte.Value == (short)ListaIndiceFixo.INPC)
                {
                    return "SXL";
                }
                else if (posicaoSwapHistorico.IdIndiceContraParte.Value == (short)ListaIndiceFixo.PTAX_800VENDA)
                {
                    return "SCL";
                }
                else if (posicaoSwapHistorico.IdIndiceContraParte.Value == (short)ListaIndiceFixo.SELIC)
                {
                    return "SSL";
                }
                else if (posicaoSwapHistorico.IdIndiceContraParte.Value == (short)ListaIndiceFixo.TR)
                {
                    return "STL";
                }
            }
            #endregion
            #region Ponta Dolar (PTAX 800 Venda)
            else if (posicaoSwapHistorico.TipoPonta.Value != (byte)TipoPontaSwap.PreFixado &&
                posicaoSwapHistorico.IdIndice.Value == (short)ListaIndiceFixo.PTAX_800VENDA)
            {
                if (posicaoSwapHistorico.TipoPonta.Value == (byte)TipoPontaSwap.PreFixado)
                {
                    return "SCP";
                }
                else if (posicaoSwapHistorico.IdIndiceContraParte.Value == (short)ListaIndiceFixo.CDI)
                {
                    return "SDC";
                }
                else if (posicaoSwapHistorico.IdIndiceContraParte.Value == (short)ListaIndiceFixo.EURO)
                {
                    return "SCE";
                }
                else if (posicaoSwapHistorico.IdIndiceContraParte.Value == (short)ListaIndiceFixo.IBOVESPA_FECHA)
                {
                    return "SCN";
                }
                else if (posicaoSwapHistorico.IdIndiceContraParte.Value == (short)ListaIndiceFixo.IGPDI)
                {
                    return "SCI";
                }
                else if (posicaoSwapHistorico.IdIndiceContraParte.Value == (short)ListaIndiceFixo.IGPM)
                {
                    return "SCM";
                }
                else if (posicaoSwapHistorico.IdIndiceContraParte.Value == (short)ListaIndiceFixo.INPC)
                {
                    return "SCX";
                }
                else if (posicaoSwapHistorico.IdIndiceContraParte.Value == (short)ListaIndiceFixo.IPCA)
                {
                    return "SCL";
                }
                else if (posicaoSwapHistorico.IdIndiceContraParte.Value == (short)ListaIndiceFixo.SELIC)
                {
                    return "SSC";
                }
                else if (posicaoSwapHistorico.IdIndiceContraParte.Value == (short)ListaIndiceFixo.TR)
                {
                    return "SCT";
                }
            }
            #endregion
            #region Ponta Selic
            else if (posicaoSwapHistorico.TipoPonta.Value != (byte)TipoPontaSwap.PreFixado &&
                posicaoSwapHistorico.IdIndice.Value == (short)ListaIndiceFixo.SELIC)
            {
                if (posicaoSwapHistorico.TipoPonta.Value == (byte)TipoPontaSwap.PreFixado)
                {
                    return "SSP";
                }
                else if (posicaoSwapHistorico.IdIndiceContraParte.Value == (short)ListaIndiceFixo.CDI)
                {
                    return "SDS";
                }
                else if (posicaoSwapHistorico.IdIndiceContraParte.Value == (short)ListaIndiceFixo.EURO)
                {
                    return "SSE";
                }
                else if (posicaoSwapHistorico.IdIndiceContraParte.Value == (short)ListaIndiceFixo.IBOVESPA_FECHA)
                {
                    return "SNS";
                }
                else if (posicaoSwapHistorico.IdIndiceContraParte.Value == (short)ListaIndiceFixo.IGPDI)
                {
                    return "SSI";
                }
                else if (posicaoSwapHistorico.IdIndiceContraParte.Value == (short)ListaIndiceFixo.IGPM)
                {
                    return "SSM";
                }
                else if (posicaoSwapHistorico.IdIndiceContraParte.Value == (short)ListaIndiceFixo.INPC)
                {
                    return "SSX";
                }
                else if (posicaoSwapHistorico.IdIndiceContraParte.Value == (short)ListaIndiceFixo.IPCA)
                {
                    return "SSL";
                }
                else if (posicaoSwapHistorico.IdIndiceContraParte.Value == (short)ListaIndiceFixo.PTAX_800VENDA)
                {
                    return "SSC";
                }
                else if (posicaoSwapHistorico.IdIndiceContraParte.Value == (short)ListaIndiceFixo.TR)
                {
                    return "SST";
                }
            }
            #endregion
            #region Ponta TR
            else if (posicaoSwapHistorico.TipoPonta.Value != (byte)TipoPontaSwap.PreFixado &&
                posicaoSwapHistorico.IdIndice.Value == (short)ListaIndiceFixo.TR)
            {
                if (posicaoSwapHistorico.TipoPonta.Value == (byte)TipoPontaSwap.PreFixado)
                {
                    return "STP";
                }
                else if (posicaoSwapHistorico.IdIndiceContraParte.Value == (short)ListaIndiceFixo.CDI)
                {
                    return "SDT";
                }
                else if (posicaoSwapHistorico.IdIndiceContraParte.Value == (short)ListaIndiceFixo.EURO)
                {
                    return "STE";
                }
                else if (posicaoSwapHistorico.IdIndiceContraParte.Value == (short)ListaIndiceFixo.IBOVESPA_FECHA)
                {
                    return "STN";
                }
                else if (posicaoSwapHistorico.IdIndiceContraParte.Value == (short)ListaIndiceFixo.IGPDI)
                {
                    return "STI";
                }
                else if (posicaoSwapHistorico.IdIndiceContraParte.Value == (short)ListaIndiceFixo.IGPM)
                {
                    return "STM";
                }
                else if (posicaoSwapHistorico.IdIndiceContraParte.Value == (short)ListaIndiceFixo.INPC)
                {
                    return "STX";
                }
                else if (posicaoSwapHistorico.IdIndiceContraParte.Value == (short)ListaIndiceFixo.IPCA)
                {
                    return "STL";
                }
                else if (posicaoSwapHistorico.IdIndiceContraParte.Value == (short)ListaIndiceFixo.PTAX_800VENDA)
                {
                    return "SCT";
                }
                else if (posicaoSwapHistorico.IdIndiceContraParte.Value == (short)ListaIndiceFixo.SELIC)
                {
                    return "SST";
                }
            }
            #endregion

            return "";
        }

        /// <summary>
        /// Retorna uma Stream de Mémoria de um arquivo xml. A Stream permanece aberta
        /// </summary>
        /// <param name="dataPosicao"></param>
        /// <param name="idClientes"></param>
        /// <param name="dicMStream">Saida: Dicionario contendo Nome do arquivo e Memory Stream do arquivo CDA</param>
        /// <returns></returns>
        public void ExportaCDA(DateTime dataPosicao, List<int> idClientes, int diasConf, out Dictionary<string, MemoryStream> dicMStream)
        {
            this.diasConfidencialidade = diasConf;

            Hashtable hsCarteira = new Hashtable();
            PessoaCollection pessoaCollection = new PessoaCollection();

            ClienteQuery clienteQuery = new ClienteQuery("C");
            PessoaQuery pessoaQuery = new PessoaQuery("P");
            //
            pessoaQuery.InnerJoin(clienteQuery).On(clienteQuery.IdPessoa == pessoaQuery.IdPessoa);
            //
            pessoaQuery.Where(clienteQuery.TipoControle == (byte)TipoControleCliente.Completo &&
                              clienteQuery.IdTipo == (int)TipoClienteFixo.Fundo &&
                              clienteQuery.StatusAtivo == (byte)StatusAtivoCliente.Ativo &&
                              clienteQuery.IdCliente.In(idClientes)
                             );
            //
            pessoaCollection.Load(pessoaQuery);

            dicMStream = new Dictionary<string, MemoryStream>();

            foreach (Pessoa pessoa in pessoaCollection)
            {
                MemoryStream ms = new MemoryStream();
                string nomeArquivo = pessoa.Apelido + "_" + dataPosicao.Month.ToString() + dataPosicao.Year.ToString() + ".xml";
                //            
                CDA_Xml arq = new CDA_Xml();
                //
                /* Monta Cabeçalho */
                dataPosicao = Calendario.RetornaUltimoDiaUtilMes(dataPosicao);
                this.MontaCabecalho(dataPosicao, arq);

                /* Busca Id.Carteira */
                int idCarteira = Convert.ToInt32(pessoa.GetColumn(ClienteMetadata.ColumnNames.IdCliente));

                Carteira carteira = new Carteira();
                if (hsCarteira.Contains(idCarteira))
                {
                    carteira = (Carteira)hsCarteira[idCarteira];
                }
                else
                {
                    carteira.LoadByPrimaryKey(idCarteira);
                    hsCarteira.Add(idCarteira, carteira);
                }

                /* Monta Corpo */
                this.MontaCorpo(dataPosicao, pessoa, arq, carteira);

                XmlSerializer x = new XmlSerializer(arq.GetType());

                XmlWriterSettings settings = new XmlWriterSettings();
                settings.Encoding = System.Text.Encoding.GetEncoding("WINDOWS-1252");
                settings.Indent = true;
                settings.IndentChars = "\t";
                settings.NewLineChars = Environment.NewLine;
                settings.ConformanceLevel = ConformanceLevel.Document;

                using (XmlWriter writer = XmlTextWriter.Create(ms, settings))
                {
                    x.Serialize(writer, arq);
                }

                /* Necessário voltar o ponteiro do arquivo para o Inicio */
                ms.Seek(0, SeekOrigin.Begin);
                //
                // Adiciona no Dicionário
                dicMStream.Add(nomeArquivo, ms);
            }
        }

        /// <summary>
        /// Monta o Cabeçalho do Arquivo XML
        /// </summary>
        /// <param name="dataPosicao"></param>
        /// <param name="arq">Estrutura do arquivo XML</param>
        /// <returns></returns>
        private void MontaCabecalho(DateTime dataPosicao, CDA_Xml arq)
        {
            arq.getCabecalho.codigoDocumento = 3;
            arq.getCabecalho.dataCompetencia = dataPosicao;
            arq.getCabecalho.dataGeracaoArquivo = DateTime.Now;
            arq.getCabecalho.versao = "2.0";
        }

        /// <summary>
        /// Monta o Corpo do Arquivo XML 
        /// </summary>
        /// <param name="dataPosicao"></param>
        /// <param name="arq">Estrutura do arquivo XML</param>
        private void MontaCorpo(DateTime dataPosicao, Pessoa pessoa, CDA_Xml arq, Carteira carteira)
        {
            int idCarteira = carteira.IdCarteira.Value;

            #region Corpo
            arq.getCorpo.getInform.cnpj = pessoa.Cpfcnpj.Trim().Replace("-", "").Replace("/", "").Replace("\\", "").Replace(".", "");

            decimal valorPL = 0;
            HistoricoCota historicoCota = new HistoricoCota();
            historicoCota.BuscaValorPatrimonioDia(idCarteira, dataPosicao);
            if (historicoCota.PLFechamento.HasValue)
            {
                valorPL = historicoCota.PLFechamento.Value;
            }

            arq.getCorpo.getInform.valorPL = valorPL;
            #endregion

            /* Monta o Objeto Titulo Publico */
            this.MontaTituloPublico(dataPosicao, arq, carteira);

            /* Monta o Objeto Cotas */
            this.MontaCotas(dataPosicao, arq, carteira);

            /* Monta o Objeto Swap */
            this.MontaSwap(dataPosicao, arq, carteira);

            /* Monta o Objeto Demais_Codif */
            this.MontaDemais_Codif(dataPosicao, arq, carteira);

            /* Monta o Objeto Demais_N_Codif */
            this.MontaDemais_N_Codif(dataPosicao, arq, carteira);
        }

        #region Monta o Node Titulo Publico
        /// <summary>
        /// Monta o Node Titulo Publico 
        /// </summary>
        /// <param name="dataPosicao"></param>
        /// <param name="arq"></param>
        /// <param name="idCarteira"></param>
        private void MontaTituloPublico(DateTime dataPosicao, CDA_Xml arq, Carteira carteira)
        {
            int idCarteira = carteira.IdCarteira.Value;
            DateTime dataInicioMes = Calendario.RetornaPrimeiroDiaCorridoMes(dataPosicao, 0);

            #region For Monta Titulos publicos (Compras Finais)
            PosicaoRendaFixaHistoricoQuery posicaoRendaFixaHistoricoQuery = new PosicaoRendaFixaHistoricoQuery("P");
            TituloRendaFixaQuery tituloRendaFixaQuery = new TituloRendaFixaQuery("T");
            PapelRendaFixaQuery papelRendaFixaQuery = new PapelRendaFixaQuery("A");
            posicaoRendaFixaHistoricoQuery.Select(posicaoRendaFixaHistoricoQuery.IdTitulo,
                                                  posicaoRendaFixaHistoricoQuery.DataVencimento,
                                                  posicaoRendaFixaHistoricoQuery.TipoNegociacao,
                                                  tituloRendaFixaQuery.CodigoCustodia,
                                                  posicaoRendaFixaHistoricoQuery.Quantidade.Sum(),
                                                  posicaoRendaFixaHistoricoQuery.ValorMercado.Sum());
            posicaoRendaFixaHistoricoQuery.InnerJoin(tituloRendaFixaQuery).On(tituloRendaFixaQuery.IdTitulo == posicaoRendaFixaHistoricoQuery.IdTitulo);
            posicaoRendaFixaHistoricoQuery.InnerJoin(papelRendaFixaQuery).On(papelRendaFixaQuery.IdPapel == tituloRendaFixaQuery.IdPapel);
            posicaoRendaFixaHistoricoQuery.Where(posicaoRendaFixaHistoricoQuery.IdCliente.Equal(idCarteira),
                                        posicaoRendaFixaHistoricoQuery.DataHistorico.Equal(dataPosicao),
                                        posicaoRendaFixaHistoricoQuery.TipoOperacao.Equal((byte)TipoOperacaoTitulo.CompraFinal),
                                        tituloRendaFixaQuery.CodigoCustodia.IsNotNull(),
                                        tituloRendaFixaQuery.CodigoCustodia.NotEqual(""),
                                        papelRendaFixaQuery.TipoPapel.Equal((byte)TipoPapelTitulo.Publico));
            posicaoRendaFixaHistoricoQuery.GroupBy(posicaoRendaFixaHistoricoQuery.IdTitulo,
                                                   posicaoRendaFixaHistoricoQuery.DataVencimento,
                                                   posicaoRendaFixaHistoricoQuery.TipoNegociacao,
                                                   tituloRendaFixaQuery.CodigoCustodia);

            PosicaoRendaFixaHistoricoCollection coll = new PosicaoRendaFixaHistoricoCollection();
            coll.Load(posicaoRendaFixaHistoricoQuery);

            foreach (PosicaoRendaFixaHistorico p in coll)
            {
                string codigoCustodia = "";
                if (p.GetColumn(TituloRendaFixaMetadata.ColumnNames.CodigoCustodia) != null)
                {
                    codigoCustodia = Convert.ToString(p.GetColumn(TituloRendaFixaMetadata.ColumnNames.CodigoCustodia));
                }

                DateTime dataVencimento = Convert.ToDateTime(p.GetColumn(TituloRendaFixaMetadata.ColumnNames.DataVencimento));
                byte tipoNegociacao = p.TipoNegociacao.Value;

                decimal quantidade = p.Quantidade.Value;
                decimal valor = p.ValorMercado.Value;

                decimal quantidadeAplicacao = 0;
                decimal valorAplicacao = 0;

                decimal quantidadeResgate = 0;
                decimal valorResgate = 0;

                if (tipoNegociacao != (byte)TipoNegociacaoTitulo.Vencimento) //Vencimento desconsidera de forma sumária qq movimentação, já que por definição não pode operar
                {
                    OperacaoRendaFixa operacaoRendaFixa = new OperacaoRendaFixa();
                    operacaoRendaFixa.Query.Select(operacaoRendaFixa.Query.Quantidade.Sum(),
                                                   operacaoRendaFixa.Query.Valor.Sum());
                    operacaoRendaFixa.Query.Where(operacaoRendaFixa.Query.IdCliente.Equal(idCarteira),
                                                  operacaoRendaFixa.Query.IdTitulo.Equal(p.IdTitulo.Value),
                                                  operacaoRendaFixa.Query.DataOperacao.GreaterThanOrEqual(dataInicioMes),
                                                  operacaoRendaFixa.Query.DataOperacao.LessThanOrEqual(dataPosicao),
                                                  operacaoRendaFixa.Query.TipoOperacao.In((byte)TipoOperacaoTitulo.CompraRevenda));
                    operacaoRendaFixa.Query.Load();

                    if (operacaoRendaFixa.Quantidade.HasValue)
                    {
                        quantidadeAplicacao = operacaoRendaFixa.Quantidade.Value;
                        valorAplicacao = operacaoRendaFixa.Valor.Value;
                    }


                    operacaoRendaFixa = new OperacaoRendaFixa();
                    operacaoRendaFixa.Query.Select(operacaoRendaFixa.Query.Quantidade.Sum(),
                                                   operacaoRendaFixa.Query.Valor.Sum());
                    operacaoRendaFixa.Query.Where(operacaoRendaFixa.Query.IdCliente.Equal(idCarteira),
                                                  operacaoRendaFixa.Query.IdTitulo.Equal(p.IdTitulo.Value),
                                                  operacaoRendaFixa.Query.DataOperacao.GreaterThanOrEqual(dataInicioMes),
                                                  operacaoRendaFixa.Query.DataOperacao.LessThanOrEqual(dataPosicao),
                                                  operacaoRendaFixa.Query.TipoOperacao.In((byte)TipoOperacaoTitulo.VendaFinal,
                                                                                          (byte)TipoOperacaoTitulo.VendaTotal));
                    operacaoRendaFixa.Query.Load();

                    if (operacaoRendaFixa.Quantidade.HasValue)
                    {
                        quantidadeResgate = operacaoRendaFixa.Quantidade.Value;
                        valorResgate = operacaoRendaFixa.Valor.Value;
                    }
                }

                TituloPublico t1 = new TituloPublico();

                t1.codigoSelic = codigoCustodia;
                t1.dataVencimento = dataVencimento;

                // TituloPublico - Aplic
                t1.getAplic.cod_Tp_Aplic = Convert.ToString((int)TipoAplicacaoCDA.TitulosPublicos);
                t1.getAplic.qtde_Dias_Confid = this.diasConfidencialidade.ToString();

                // Aplic - Negoc
                t1.getAplic.getAplicNegoc.tipoNegocio = p.TipoNegociacao.Value == (byte)TipoNegociacaoTitulo.Vencimento ?
                                                Convert.ToString((int)TipoNegociacaoCDA.MantidoVencimento) : Convert.ToString((int)TipoNegociacaoCDA.Negociacao);
                t1.getAplic.getAplicNegoc.quantidadeVendasNegocio = quantidadeResgate;
                t1.getAplic.getAplicNegoc.valorVendasNegocio = valorResgate;
                t1.getAplic.getAplicNegoc.quantidadeAquisicaoNegocio = quantidadeAplicacao;
                t1.getAplic.getAplicNegoc.valorAquisicaoNegocio = valorAplicacao;

                // Aplic - Pos_Fim
                t1.getAplic.getAplicPosFim.qtde_Pos_Fim = quantidade;
                t1.getAplic.getAplicPosFim.merc_Pos_Fim = valor;

                // Adiciona TituloPublico
                arq.getCorpo.getInform.getListAtiv.getTituloPublico.Add(t1);
            }
            #endregion

            #region For Monta Titulos publicos (Compras Compromissadas)
            posicaoRendaFixaHistoricoQuery = new PosicaoRendaFixaHistoricoQuery("P");
            tituloRendaFixaQuery = new TituloRendaFixaQuery("T");
            papelRendaFixaQuery = new PapelRendaFixaQuery("A");
            posicaoRendaFixaHistoricoQuery.Select(posicaoRendaFixaHistoricoQuery.IdTitulo,
                                                  posicaoRendaFixaHistoricoQuery.DataVolta,
                                                  posicaoRendaFixaHistoricoQuery.TipoNegociacao,
                                                  posicaoRendaFixaHistoricoQuery.Quantidade.Sum(),
                                                  posicaoRendaFixaHistoricoQuery.ValorMercado.Sum());
            posicaoRendaFixaHistoricoQuery.InnerJoin(tituloRendaFixaQuery).On(tituloRendaFixaQuery.IdTitulo == posicaoRendaFixaHistoricoQuery.IdTitulo);
            posicaoRendaFixaHistoricoQuery.InnerJoin(papelRendaFixaQuery).On(papelRendaFixaQuery.IdPapel == tituloRendaFixaQuery.IdPapel);
            posicaoRendaFixaHistoricoQuery.Where(posicaoRendaFixaHistoricoQuery.IdCliente.Equal(idCarteira),
                                        posicaoRendaFixaHistoricoQuery.DataHistorico.Equal(dataPosicao),
                                        posicaoRendaFixaHistoricoQuery.TipoOperacao.Equal((byte)TipoOperacaoTitulo.CompraRevenda),
                                        tituloRendaFixaQuery.CodigoCustodia.IsNotNull(),
                                        tituloRendaFixaQuery.CodigoCustodia.NotEqual(""),
                                        papelRendaFixaQuery.TipoPapel.Equal((byte)TipoPapelTitulo.Publico));
            posicaoRendaFixaHistoricoQuery.GroupBy(posicaoRendaFixaHistoricoQuery.IdTitulo,
                                                   posicaoRendaFixaHistoricoQuery.DataVolta,
                                                   posicaoRendaFixaHistoricoQuery.TipoNegociacao);

            coll = new PosicaoRendaFixaHistoricoCollection();
            coll.Load(posicaoRendaFixaHistoricoQuery);

            foreach (PosicaoRendaFixaHistorico p in coll)
            {
                string codigoCustodia = "";
                if (p.GetColumn(TituloRendaFixaMetadata.ColumnNames.CodigoCustodia) != null)
                {
                    codigoCustodia = Convert.ToString(p.GetColumn(TituloRendaFixaMetadata.ColumnNames.CodigoCustodia));
                }

                DateTime dataVencimento = p.DataVolta.Value;

                decimal quantidade = p.Quantidade.Value;
                decimal valor = p.ValorMercado.Value;

                TituloPublico t1 = new TituloPublico();

                t1.codigoSelic = codigoCustodia;
                t1.dataVencimento = dataVencimento;

                // TituloPublico - Aplic
                t1.getAplic.cod_Tp_Aplic = Convert.ToString((int)TipoAplicacaoCDA.OperacoesCompromissadas);
                t1.getAplic.qtde_Dias_Confid = this.diasConfidencialidade.ToString();

                // Aplic - Negoc
                t1.getAplic.getAplicNegoc.tipoNegocio = Convert.ToString((int)TipoNegociacaoCDA.Negociacao);

                // Aplic - Pos_Fim
                t1.getAplic.getAplicPosFim.qtde_Pos_Fim = quantidade;
                t1.getAplic.getAplicPosFim.merc_Pos_Fim = valor;

                // Adiciona TituloPublico
                arq.getCorpo.getInform.getListAtiv.getTituloPublico.Add(t1);
            }
            #endregion

        }
        #endregion

        #region Monta o Node Cotas
        /// <summary>
        /// Monta o Node Cotas
        /// </summary>
        /// <param name="dataPosicao"></param>
        /// <param name="arq"></param>
        /// <param name="idCarteira"></param>
        private void MontaCotas(DateTime dataPosicao, CDA_Xml arq, Carteira carteira)
        {
            int idCarteira = carteira.IdCarteira.Value;
            DateTime dataInicioMes = Calendario.RetornaPrimeiroDiaCorridoMes(dataPosicao, 0);

            #region For Monta Objeto Cotas
            CarteiraQuery carteiraQuery = new CarteiraQuery("C");
            ClienteQuery clienteQuery = new ClienteQuery("L");
            PosicaoFundoHistoricoQuery posicaoFundoHistoricoQuery = new PosicaoFundoHistoricoQuery("P");

            PosicaoFundoHistoricoCollection coll = new PosicaoFundoHistoricoCollection();
            posicaoFundoHistoricoQuery.Select(posicaoFundoHistoricoQuery.IdCarteira,                                               
                                              posicaoFundoHistoricoQuery.Quantidade.Sum(),
                                              posicaoFundoHistoricoQuery.ValorAplicacao.Sum(),
                                              posicaoFundoHistoricoQuery.ValorBruto.Sum());
            posicaoFundoHistoricoQuery.InnerJoin(carteiraQuery).On(carteiraQuery.IdCarteira == posicaoFundoHistoricoQuery.IdCarteira);
            posicaoFundoHistoricoQuery.InnerJoin(clienteQuery).On(clienteQuery.IdCliente == carteiraQuery.IdCarteira);
            posicaoFundoHistoricoQuery.Where(posicaoFundoHistoricoQuery.IdCliente.Equal(idCarteira),
                                             posicaoFundoHistoricoQuery.DataHistorico.Equal(dataPosicao),
                                             posicaoFundoHistoricoQuery.Quantidade.NotEqual(0),
                                             clienteQuery.IdMoeda.Equal((int)ListaMoedaFixo.Real));
            posicaoFundoHistoricoQuery.GroupBy(posicaoFundoHistoricoQuery.IdCarteira);
            coll.Load(posicaoFundoHistoricoQuery);

            foreach (PosicaoFundoHistorico p in coll)
            {
                Carteira carteiraFundo = new Carteira();
                carteiraFundo.LoadByPrimaryKey(p.IdCarteira.Value);

                decimal quantidade = p.Quantidade.Value;
                decimal valor = p.ValorBruto.Value;
                decimal custoCorrecPosFim = p.ValorAplicacao.Value;

                Pessoa pessoaFundo = new Pessoa();
                Cliente cliente = new Cliente();
                cliente.LoadByPrimaryKey(p.IdCarteira.Value);
                pessoaFundo.LoadByPrimaryKey(cliente.IdPessoa.Value);

                string cnpj = "";
                if (!String.IsNullOrEmpty(pessoaFundo.Cpfcnpj))
                {
                    cnpj = Utilitario.RemoveCaracteresEspeciais(pessoaFundo.Cpfcnpj);
                }

                OperacaoFundo operacaoFundo = new OperacaoFundo();
                operacaoFundo.Query.Select(operacaoFundo.Query.Quantidade.Sum(),
                                           operacaoFundo.Query.ValorBruto.Sum());
                operacaoFundo.Query.Where(operacaoFundo.Query.IdCliente.Equal(idCarteira),
                                          operacaoFundo.Query.IdCarteira.Equal(p.IdCarteira.Value),
                                          operacaoFundo.Query.DataOperacao.GreaterThanOrEqual(dataInicioMes),
                                          operacaoFundo.Query.DataOperacao.LessThanOrEqual(dataPosicao),
                                          operacaoFundo.Query.TipoOperacao.Equal((byte)TipoOperacaoFundo.Aplicacao));
                operacaoFundo.Query.Load();

                decimal quantidadeAplicacao = 0;
                decimal valorAplicacao = 0;
                if (operacaoFundo.Quantidade.HasValue)
                {
                    quantidadeAplicacao = operacaoFundo.Quantidade.Value;
                    valorAplicacao = operacaoFundo.ValorBruto.Value;
                }

                operacaoFundo = new OperacaoFundo();
                operacaoFundo.Query.Select(operacaoFundo.Query.Quantidade.Sum(),
                                           operacaoFundo.Query.ValorBruto.Sum());
                operacaoFundo.Query.Where(operacaoFundo.Query.IdCliente.Equal(idCarteira),
                                          operacaoFundo.Query.IdCarteira.Equal(p.IdCarteira.Value),
                                          operacaoFundo.Query.DataOperacao.GreaterThanOrEqual(dataInicioMes),
                                          operacaoFundo.Query.DataOperacao.LessThanOrEqual(dataPosicao),
                                          operacaoFundo.Query.TipoOperacao.NotEqual((byte)TipoOperacaoFundo.Aplicacao));
                operacaoFundo.Query.Load();

                decimal quantidadeResgate = 0;
                decimal valorResgate = 0;
                if (operacaoFundo.Quantidade.HasValue)
                {
                    quantidadeResgate = operacaoFundo.Quantidade.Value;
                    valorResgate = operacaoFundo.ValorBruto.Value;
                }

                Cotas c1 = new Cotas();
                c1.cnpj = cnpj;
                // Cotas - Aplic
                if (carteiraFundo.TipoCarteira.Value == (byte)TipoCarteiraFundo.RendaFixa)
                {
                    c1.getAplic.cod_Tp_Aplic = Convert.ToString((int)TipoAplicacaoCDA.CotasFundosRF);
                }
                else
                {
                    c1.getAplic.cod_Tp_Aplic = Convert.ToString((int)TipoAplicacaoCDA.CotasFundosRV);
                }

                c1.getAplic.qtde_Dias_Confid = this.diasConfidencialidade.ToString();
                // Aplic - Pos_Fim
                c1.getAplic.getAplicPosFim.qtde_Pos_Fim = quantidade;
                c1.getAplic.getAplicPosFim.custo_Correc_Pos_Fim = custoCorrecPosFim;
                c1.getAplic.getAplicPosFim.merc_Pos_Fim = valor;

                c1.getAplic.getAplicNegoc.tipoNegocio = Convert.ToString((int)TipoNegociacaoCDA.Negociacao);

                c1.getAplic.getAplicNegoc.quantidadeAquisicaoNegocio = quantidadeAplicacao;
                c1.getAplic.getAplicNegoc.valorAquisicaoNegocio = valorAplicacao;
                c1.getAplic.getAplicNegoc.quantidadeVendasNegocio = quantidadeResgate;
                c1.getAplic.getAplicNegoc.valorVendasNegocio = valorResgate;

                // Adiciona Cota
                arq.getCorpo.getInform.getListAtiv.getCotas.Add(c1);
            }
            #endregion
        }
        #endregion

        #region Monta o Node Swap
        /// <summary>
        /// Monta o Node Swap
        /// </summary>
        /// <param name="dataPosicao"></param>
        /// <param name="arq"></param>
        /// <param name="idCarteira"></param>
        private void MontaSwap(DateTime dataPosicao, CDA_Xml arq, Carteira carteira)
        {
            int idCarteira = carteira.IdCarteira.Value;

            Swap s1 = new Swap();

            PosicaoSwapHistoricoCollection posicaoSwapHistoricoCollection = new PosicaoSwapHistoricoCollection();
            posicaoSwapHistoricoCollection.Query.Where(posicaoSwapHistoricoCollection.Query.IdCliente.Equal(idCarteira),
                                                       posicaoSwapHistoricoCollection.Query.DataHistorico.Equal(dataPosicao),
                                                       posicaoSwapHistoricoCollection.Query.Saldo.NotEqual(0));
            posicaoSwapHistoricoCollection.Query.Load();

            foreach (PosicaoSwapHistorico posicaoSwapHistorico in posicaoSwapHistoricoCollection)
            {
                string codigoSwap = this.RetornaCodigoSwap(posicaoSwapHistorico);
                decimal saldo = posicaoSwapHistorico.Saldo.Value;

                s1.codigoSwap = codigoSwap;
                string codigoTipoAplicacao = saldo > 0 ? Convert.ToString((int)TipoAplicacaoCDA.DiferencialSwapReceber) : Convert.ToString((int)TipoAplicacaoCDA.DiferencialSwapPagar);

                // Swap - Aplic
                s1.getAplic.cod_Tp_Aplic = codigoTipoAplicacao;
                s1.getAplic.qtde_Dias_Confid = this.diasConfidencialidade.ToString();

                // Aplic - Pos_Fim            
                s1.getAplic.getAplicPosFim.merc_Pos_Fim = saldo;

                // Adiciona Swap
                arq.getCorpo.getInform.getListAtiv.getSwap.Add(s1);
            }

        }
        #endregion

        #region Monta o Node Demais_Codif
        /// <summary>
        /// Monta o Node Demais_Codif
        /// </summary>
        /// <param name="dataPosicao"></param>
        /// <param name="arq"></param>
        /// <param name="idCarteira"></param>
        private void MontaDemais_Codif(DateTime dataPosicao, CDA_Xml arq, Carteira carteira)
        {
            int idCarteira = carteira.IdCarteira.Value;
            DateTime dataInicioMes = Calendario.RetornaPrimeiroDiaCorridoMes(dataPosicao, 0);

            DateTime dataIndefinida = new DateTime(4000, 01, 01);

            #region Acoes
            AtivoBolsaQuery ativoBolsaQuery = new AtivoBolsaQuery("A");
            PosicaoBolsaHistoricoQuery posicaoBolsaHistoricoQuery = new PosicaoBolsaHistoricoQuery("P");

            PosicaoBolsaHistoricoCollection posicaoBolsaHistoricoCollection = new PosicaoBolsaHistoricoCollection();
            posicaoBolsaHistoricoQuery.Select(posicaoBolsaHistoricoQuery.CdAtivoBolsa,
                                             posicaoBolsaHistoricoQuery.Quantidade.Sum(),
                                             posicaoBolsaHistoricoQuery.ValorCustoLiquido.Sum(),
                                             posicaoBolsaHistoricoQuery.ValorMercado.Sum());
            posicaoBolsaHistoricoQuery.InnerJoin(ativoBolsaQuery).On(ativoBolsaQuery.CdAtivoBolsa == posicaoBolsaHistoricoQuery.CdAtivoBolsa);
            posicaoBolsaHistoricoQuery.Where(posicaoBolsaHistoricoQuery.IdCliente.Equal(idCarteira),
                                            posicaoBolsaHistoricoQuery.DataHistorico.Equal(dataPosicao),
                                            posicaoBolsaHistoricoQuery.TipoMercado.Equal(TipoMercadoBolsa.MercadoVista),
                                            posicaoBolsaHistoricoQuery.Quantidade.GreaterThan(0),
                                            ativoBolsaQuery.DataInicioVigencia.IsNotNull(),
                                            ativoBolsaQuery.DataInicioVigencia.NotEqual(dataIndefinida));
            posicaoBolsaHistoricoQuery.GroupBy(posicaoBolsaHistoricoQuery.CdAtivoBolsa);
            posicaoBolsaHistoricoQuery.OrderBy(posicaoBolsaHistoricoQuery.CdAtivoBolsa.Ascending);
            posicaoBolsaHistoricoCollection.Load(posicaoBolsaHistoricoQuery);

            foreach (PosicaoBolsaHistorico posicaoBolsaHistorico in posicaoBolsaHistoricoCollection)
            {
                string cdAtivoBolsa = posicaoBolsaHistorico.CdAtivoBolsa;

                decimal quantidadeFinal = posicaoBolsaHistorico.Quantidade.HasValue ? posicaoBolsaHistorico.Quantidade.Value : 0;
                decimal custoCorrecPosFim = posicaoBolsaHistorico.ValorCustoLiquido.HasValue ? posicaoBolsaHistorico.ValorCustoLiquido.Value : 0;
                decimal valorFinal = posicaoBolsaHistorico.ValorMercado.HasValue ? posicaoBolsaHistorico.ValorMercado.Value : 0;

                AtivoBolsa ativoBolsa = new AtivoBolsa();
                List<esQueryItem> campos = new List<esQueryItem>();
                campos.Add(ativoBolsa.Query.TipoPapel);
                campos.Add(ativoBolsa.Query.CodigoCDA);
                campos.Add(ativoBolsa.Query.DataInicioVigencia);
                ativoBolsa.LoadByPrimaryKey(campos, cdAtivoBolsa);

                string codigoAtivo = "";
                string tipoAplicacao = "";
                #region Checa codigo de ativo
                if (ativoBolsa.TipoPapel.Value == (byte)TipoPapelAtivo.Normal ||
                    ativoBolsa.TipoPapel.Value == (byte)TipoPapelAtivo.BDRNivelI ||
                    ativoBolsa.TipoPapel.Value == (byte)TipoPapelAtivo.BDRNivelII ||
                    ativoBolsa.TipoPapel.Value == (byte)TipoPapelAtivo.BDRNivelII)
                {
                    codigoAtivo = Utilitario.Right(cdAtivoBolsa, 1) == "3" ? Convert.ToString((int)TipoAtivoCDA.AcaoOrdinaria) : Convert.ToString((int)TipoAtivoCDA.AcaoPreferencial);
                    tipoAplicacao = Convert.ToString((int)TipoAplicacaoCDA.Acoes);
                }
                else if (ativoBolsa.TipoPapel.Value == (byte)TipoPapelAtivo.BonusSubscricao)
                {
                    codigoAtivo = Convert.ToString((int)TipoAtivoCDA.BonusSubscricao);
                    tipoAplicacao = Convert.ToString((int)TipoAplicacaoCDA.OutrosValoresMobiliarios_OfertaPublica);
                }
                else if (ativoBolsa.TipoPapel.Value == (byte)TipoPapelAtivo.CertificadoDepositoAcoes)
                {
                    codigoAtivo = Convert.ToString((int)TipoAtivoCDA.CertificadoDepositoAcoes);
                    tipoAplicacao = Convert.ToString((int)TipoAplicacaoCDA.Certificado_ReciboDeposito);
                }
                else if (ativoBolsa.TipoPapel.Value == (byte)TipoPapelAtivo.ETF)
                {
                    codigoAtivo = Convert.ToString((int)TipoAtivoCDA.AcaoPreferencial); //Fundos de indice são jogados, por ora, em Ação
                    tipoAplicacao = Convert.ToString((int)TipoAplicacaoCDA.Acoes);
                }
                else if (ativoBolsa.TipoPapel.Value == (byte)TipoPapelAtivo.ReciboDepositoAcoes)
                {
                    codigoAtivo = Convert.ToString((int)TipoAtivoCDA.ReciboDepositoAcoes);
                    tipoAplicacao = Convert.ToString((int)TipoAplicacaoCDA.Certificado_ReciboDeposito);
                }
                else if (ativoBolsa.TipoPapel.Value == (byte)TipoPapelAtivo.ReciboSubscricao)
                {
                    codigoAtivo = Convert.ToString((int)TipoAtivoCDA.ReciboSubscricao);
                    tipoAplicacao = Convert.ToString((int)TipoAplicacaoCDA.OutrosValoresMobiliarios_OfertaPublica);
                }
                #endregion

                if (ativoBolsa.CodigoCDA.HasValue) //Se tiver o codigo CDA força pelo codigo informado
                {
                    codigoAtivo = Convert.ToString(ativoBolsa.CodigoCDA);
                }

                OperacaoBolsa operacaoBolsaCompra = new OperacaoBolsa();
                operacaoBolsaCompra.Query.Select(operacaoBolsaCompra.Query.Quantidade.Sum(),
                                                 operacaoBolsaCompra.Query.Valor.Sum());
                operacaoBolsaCompra.Query.Where(operacaoBolsaCompra.Query.IdCliente.Equal(idCarteira),
                                                operacaoBolsaCompra.Query.CdAtivoBolsa.Equal(cdAtivoBolsa),
                                                operacaoBolsaCompra.Query.Data.GreaterThanOrEqual(dataInicioMes),
                                                operacaoBolsaCompra.Query.Data.LessThanOrEqual(dataPosicao),
                                                operacaoBolsaCompra.Query.TipoOperacao.In(TipoOperacaoBolsa.Compra, TipoOperacaoBolsa.CompraDaytrade));
                operacaoBolsaCompra.Query.Load();

                decimal quantidadeCompra = operacaoBolsaCompra.Quantidade.HasValue ? operacaoBolsaCompra.Quantidade.Value : 0;
                decimal valorCompra = operacaoBolsaCompra.Valor.HasValue ? operacaoBolsaCompra.Valor.Value : 0;

                OperacaoBolsa operacaoBolsaVenda = new OperacaoBolsa();
                operacaoBolsaVenda.Query.Select(operacaoBolsaVenda.Query.Quantidade.Sum(),
                                                operacaoBolsaVenda.Query.Valor.Sum());
                operacaoBolsaVenda.Query.Where(operacaoBolsaVenda.Query.IdCliente.Equal(idCarteira),
                                               operacaoBolsaVenda.Query.CdAtivoBolsa.Equal(cdAtivoBolsa),
                                               operacaoBolsaVenda.Query.Data.GreaterThanOrEqual(dataInicioMes),
                                               operacaoBolsaVenda.Query.Data.LessThanOrEqual(dataPosicao),
                                               operacaoBolsaVenda.Query.TipoOperacao.In(TipoOperacaoBolsa.Venda, TipoOperacaoBolsa.VendaDaytrade));
                operacaoBolsaVenda.Query.Load();

                decimal quantidadeVenda = operacaoBolsaVenda.Quantidade.HasValue ? operacaoBolsaVenda.Quantidade.Value : 0;
                decimal valorVenda = operacaoBolsaVenda.Valor.HasValue ? operacaoBolsaVenda.Valor.Value : 0;

                Demais_Codif d1 = new Demais_Codif();

                d1.codigoAtivo = cdAtivoBolsa;
                d1.cod_Tp_Ativ = codigoAtivo;
                d1.dataInicioVigencia = ativoBolsa.DataInicioVigencia.HasValue ? ativoBolsa.DataInicioVigencia.Value : new DateTime(4000, 01, 01);

                // Demais_Codif - Aplic
                d1.getAplic.cod_Tp_Aplic = tipoAplicacao;
                d1.getAplic.qtde_Dias_Confid = this.diasConfidencialidade.ToString();

                // Aplic - Negoc
                d1.getAplic.getAplicNegoc.tipoNegocio = Convert.ToString((int)TipoNegociacaoCDA.Negociacao);
                d1.getAplic.getAplicNegoc.quantidadeVendasNegocio = quantidadeVenda;
                d1.getAplic.getAplicNegoc.valorVendasNegocio = valorVenda;
                d1.getAplic.getAplicNegoc.quantidadeAquisicaoNegocio = quantidadeCompra;
                d1.getAplic.getAplicNegoc.valorAquisicaoNegocio = valorCompra;

                // Aplic - Pos_Fim
                d1.getAplic.getAplicPosFim.qtde_Pos_Fim = quantidadeFinal;
                d1.getAplic.getAplicPosFim.custo_Correc_Pos_Fim = custoCorrecPosFim;
                d1.getAplic.getAplicPosFim.merc_Pos_Fim = valorFinal;

                // Adiciona Demais_Codif
                arq.getCorpo.getInform.getListAtiv.getDemais_Codif.Add(d1);
            }
            #endregion

            #region Opções Compradas
            ativoBolsaQuery = new AtivoBolsaQuery("A");
            posicaoBolsaHistoricoQuery = new PosicaoBolsaHistoricoQuery("P");

            posicaoBolsaHistoricoCollection = new PosicaoBolsaHistoricoCollection();
            posicaoBolsaHistoricoQuery.Select(posicaoBolsaHistoricoQuery.TipoMercado,
                                             posicaoBolsaHistoricoQuery.CdAtivoBolsa,
                                             posicaoBolsaHistoricoQuery.Quantidade.Sum(),
                                             posicaoBolsaHistoricoQuery.ValorCustoLiquido.Sum(),
                                             posicaoBolsaHistoricoQuery.ValorMercado.Sum());
            posicaoBolsaHistoricoQuery.InnerJoin(ativoBolsaQuery).On(ativoBolsaQuery.CdAtivoBolsa == posicaoBolsaHistoricoQuery.CdAtivoBolsa);
            posicaoBolsaHistoricoQuery.Where(posicaoBolsaHistoricoQuery.IdCliente.Equal(idCarteira),
                                            posicaoBolsaHistoricoQuery.DataHistorico.Equal(dataPosicao),
                                            posicaoBolsaHistoricoQuery.TipoMercado.In(TipoMercadoBolsa.OpcaoCompra, TipoMercadoBolsa.OpcaoVenda),
                                            posicaoBolsaHistoricoQuery.Quantidade.GreaterThan(0),
                                            ativoBolsaQuery.DataInicioVigencia.IsNotNull(),
                                            ativoBolsaQuery.DataInicioVigencia.NotEqual(dataIndefinida));
            posicaoBolsaHistoricoQuery.GroupBy(posicaoBolsaHistoricoQuery.TipoMercado,
                                               posicaoBolsaHistoricoQuery.CdAtivoBolsa);
            posicaoBolsaHistoricoQuery.OrderBy(posicaoBolsaHistoricoQuery.TipoMercado.Ascending,
                                               posicaoBolsaHistoricoQuery.CdAtivoBolsa.Ascending);
            posicaoBolsaHistoricoCollection.Load(posicaoBolsaHistoricoQuery);

            foreach (PosicaoBolsaHistorico posicaoBolsaHistorico in posicaoBolsaHistoricoCollection)
            {
                string cdAtivoBolsa = posicaoBolsaHistorico.CdAtivoBolsa;
                string tipoMercado = posicaoBolsaHistorico.TipoMercado;

                decimal quantidadeFinal = posicaoBolsaHistorico.Quantidade.HasValue ? posicaoBolsaHistorico.Quantidade.Value : 0;
                decimal valorFinal = posicaoBolsaHistorico.ValorMercado.HasValue ? posicaoBolsaHistorico.ValorMercado.Value : 0;
                decimal custoCorrecPosFim = posicaoBolsaHistorico.ValorCustoLiquido.HasValue ? posicaoBolsaHistorico.ValorCustoLiquido.Value : 0;

                AtivoBolsa ativoBolsa = new AtivoBolsa();
                List<esQueryItem> campos = new List<esQueryItem>();
                campos.Add(ativoBolsa.Query.DataInicioVigencia);
                ativoBolsa.LoadByPrimaryKey(campos, cdAtivoBolsa);

                string tipoAplicacao = Convert.ToString((int)TipoAplicacaoCDA.OpcoesPosicaoTitular);

                string codigoAtivo = "";
                if (tipoMercado == TipoMercadoBolsa.OpcaoCompra)
                {
                    codigoAtivo = Convert.ToString((int)TipoAtivoCDA.OpcaoCompra);
                }
                else
                {
                    codigoAtivo = Convert.ToString((int)TipoAtivoCDA.OpcaoVenda);
                }

                OperacaoBolsa operacaoBolsaCompra = new OperacaoBolsa();
                operacaoBolsaCompra.Query.Select(operacaoBolsaCompra.Query.Quantidade.Sum(),
                                                 operacaoBolsaCompra.Query.Valor.Sum());
                operacaoBolsaCompra.Query.Where(operacaoBolsaCompra.Query.IdCliente.Equal(idCarteira),
                                                operacaoBolsaCompra.Query.CdAtivoBolsa.Equal(cdAtivoBolsa),
                                                operacaoBolsaCompra.Query.Data.GreaterThanOrEqual(dataInicioMes),
                                                operacaoBolsaCompra.Query.Data.LessThanOrEqual(dataPosicao),
                                                operacaoBolsaCompra.Query.TipoOperacao.In(TipoOperacaoBolsa.Compra, TipoOperacaoBolsa.CompraDaytrade));
                operacaoBolsaCompra.Query.Load();

                decimal quantidadeCompra = operacaoBolsaCompra.Quantidade.HasValue ? operacaoBolsaCompra.Quantidade.Value : 0;
                decimal valorCompra = operacaoBolsaCompra.Valor.HasValue ? operacaoBolsaCompra.Valor.Value : 0;

                OperacaoBolsa operacaoBolsaVenda = new OperacaoBolsa();
                operacaoBolsaVenda.Query.Select(operacaoBolsaVenda.Query.Quantidade.Sum(),
                                                operacaoBolsaVenda.Query.Valor.Sum());
                operacaoBolsaVenda.Query.Where(operacaoBolsaVenda.Query.IdCliente.Equal(idCarteira),
                                               operacaoBolsaVenda.Query.CdAtivoBolsa.Equal(cdAtivoBolsa),
                                               operacaoBolsaVenda.Query.Data.GreaterThanOrEqual(dataInicioMes),
                                               operacaoBolsaVenda.Query.Data.LessThanOrEqual(dataPosicao),
                                               operacaoBolsaVenda.Query.TipoOperacao.In(TipoOperacaoBolsa.Venda, TipoOperacaoBolsa.VendaDaytrade));
                operacaoBolsaVenda.Query.Load();

                decimal quantidadeVenda = operacaoBolsaVenda.Quantidade.HasValue ? operacaoBolsaVenda.Quantidade.Value : 0;
                decimal valorVenda = operacaoBolsaVenda.Valor.HasValue ? operacaoBolsaVenda.Valor.Value : 0;

                Demais_Codif d1 = new Demais_Codif();

                d1.codigoAtivo = cdAtivoBolsa;
                d1.cod_Tp_Ativ = codigoAtivo;
                d1.dataInicioVigencia = ativoBolsa.DataInicioVigencia.HasValue ? ativoBolsa.DataInicioVigencia.Value : new DateTime(4000, 01, 01);

                // Demais_Codif - Aplic
                d1.getAplic.cod_Tp_Aplic = tipoAplicacao;
                d1.getAplic.qtde_Dias_Confid = this.diasConfidencialidade.ToString();

                // Aplic - Negoc
                d1.getAplic.getAplicNegoc.tipoNegocio = Convert.ToString((int)TipoNegociacaoCDA.Negociacao);
                d1.getAplic.getAplicNegoc.quantidadeVendasNegocio = quantidadeVenda;
                d1.getAplic.getAplicNegoc.valorVendasNegocio = valorVenda;
                d1.getAplic.getAplicNegoc.quantidadeAquisicaoNegocio = quantidadeCompra;
                d1.getAplic.getAplicNegoc.valorAquisicaoNegocio = valorCompra;

                // Aplic - Pos_Fim
                d1.getAplic.getAplicPosFim.qtde_Pos_Fim = quantidadeFinal;
                d1.getAplic.getAplicPosFim.custo_Correc_Pos_Fim = custoCorrecPosFim;
                d1.getAplic.getAplicPosFim.merc_Pos_Fim = valorFinal;

                // Adiciona Demais_Codif
                arq.getCorpo.getInform.getListAtiv.getDemais_Codif.Add(d1);
            }
            #endregion

            #region Opções Vendidas
            ativoBolsaQuery = new AtivoBolsaQuery("A");
            posicaoBolsaHistoricoQuery = new PosicaoBolsaHistoricoQuery("P");

            posicaoBolsaHistoricoCollection = new PosicaoBolsaHistoricoCollection();
            posicaoBolsaHistoricoQuery.Select(posicaoBolsaHistoricoQuery.TipoMercado,
                                             posicaoBolsaHistoricoQuery.CdAtivoBolsa,
                                             posicaoBolsaHistoricoQuery.Quantidade.Sum(),
                                             posicaoBolsaHistoricoQuery.ValorCustoLiquido.Sum(),
                                             posicaoBolsaHistoricoQuery.ValorMercado.Sum());
            posicaoBolsaHistoricoQuery.InnerJoin(ativoBolsaQuery).On(ativoBolsaQuery.CdAtivoBolsa == posicaoBolsaHistoricoQuery.CdAtivoBolsa);
            posicaoBolsaHistoricoQuery.Where(posicaoBolsaHistoricoQuery.IdCliente.Equal(idCarteira),
                                            posicaoBolsaHistoricoQuery.DataHistorico.Equal(dataPosicao),
                                            posicaoBolsaHistoricoQuery.TipoMercado.In(TipoMercadoBolsa.OpcaoCompra, TipoMercadoBolsa.OpcaoVenda),
                                            posicaoBolsaHistoricoQuery.Quantidade.LessThan(0),
                                            ativoBolsaQuery.DataInicioVigencia.IsNotNull(),
                                            ativoBolsaQuery.DataInicioVigencia.NotEqual(dataIndefinida));
            posicaoBolsaHistoricoQuery.GroupBy(posicaoBolsaHistoricoQuery.TipoMercado,
                                               posicaoBolsaHistoricoQuery.CdAtivoBolsa);
            posicaoBolsaHistoricoQuery.OrderBy(posicaoBolsaHistoricoQuery.TipoMercado.Ascending,
                                               posicaoBolsaHistoricoQuery.CdAtivoBolsa.Ascending);
            posicaoBolsaHistoricoCollection.Load(posicaoBolsaHistoricoQuery);

            foreach (PosicaoBolsaHistorico posicaoBolsaHistorico in posicaoBolsaHistoricoCollection)
            {
                string cdAtivoBolsa = posicaoBolsaHistorico.CdAtivoBolsa;
                string tipoMercado = posicaoBolsaHistorico.TipoMercado;

                decimal quantidadeFinal = posicaoBolsaHistorico.Quantidade.HasValue ? posicaoBolsaHistorico.Quantidade.Value : 0;
                decimal valorFinal = posicaoBolsaHistorico.ValorMercado.HasValue ? posicaoBolsaHistorico.ValorMercado.Value : 0;
                decimal custoCorrecPosFim  = posicaoBolsaHistorico.ValorCustoLiquido.HasValue ? posicaoBolsaHistorico.ValorCustoLiquido.Value : 0;

                AtivoBolsa ativoBolsa = new AtivoBolsa();
                List<esQueryItem> campos = new List<esQueryItem>();
                campos.Add(ativoBolsa.Query.DataInicioVigencia);
                ativoBolsa.LoadByPrimaryKey(campos, cdAtivoBolsa);

                string tipoAplicacao = Convert.ToString((int)TipoAplicacaoCDA.OpcoesPosicaoLancada);

                string codigoAtivo = "";
                if (tipoMercado == TipoMercadoBolsa.OpcaoCompra)
                {
                    codigoAtivo = Convert.ToString((int)TipoAtivoCDA.OpcaoCompra);
                }
                else
                {
                    codigoAtivo = Convert.ToString((int)TipoAtivoCDA.OpcaoVenda);
                }

                OperacaoBolsa operacaoBolsaCompra = new OperacaoBolsa();
                operacaoBolsaCompra.Query.Select(operacaoBolsaCompra.Query.Quantidade.Sum(),
                                                 operacaoBolsaCompra.Query.Valor.Sum());
                operacaoBolsaCompra.Query.Where(operacaoBolsaCompra.Query.IdCliente.Equal(idCarteira),
                                                operacaoBolsaCompra.Query.CdAtivoBolsa.Equal(cdAtivoBolsa),
                                                operacaoBolsaCompra.Query.Data.GreaterThanOrEqual(dataInicioMes),
                                                operacaoBolsaCompra.Query.Data.LessThanOrEqual(dataPosicao),
                                                operacaoBolsaCompra.Query.TipoOperacao.In(TipoOperacaoBolsa.Compra, TipoOperacaoBolsa.CompraDaytrade));
                operacaoBolsaCompra.Query.Load();

                decimal quantidadeCompra = operacaoBolsaCompra.Quantidade.HasValue ? operacaoBolsaCompra.Quantidade.Value : 0;
                decimal valorCompra = operacaoBolsaCompra.Valor.HasValue ? operacaoBolsaCompra.Valor.Value : 0;

                OperacaoBolsa operacaoBolsaVenda = new OperacaoBolsa();
                operacaoBolsaVenda.Query.Select(operacaoBolsaVenda.Query.Quantidade.Sum(),
                                                operacaoBolsaVenda.Query.Valor.Sum());
                operacaoBolsaVenda.Query.Where(operacaoBolsaVenda.Query.IdCliente.Equal(idCarteira),
                                               operacaoBolsaVenda.Query.CdAtivoBolsa.Equal(cdAtivoBolsa),
                                               operacaoBolsaVenda.Query.Data.GreaterThanOrEqual(dataInicioMes),
                                               operacaoBolsaVenda.Query.Data.LessThanOrEqual(dataPosicao),
                                               operacaoBolsaVenda.Query.TipoOperacao.In(TipoOperacaoBolsa.Venda, TipoOperacaoBolsa.VendaDaytrade));
                operacaoBolsaVenda.Query.Load();

                decimal quantidadeVenda = operacaoBolsaVenda.Quantidade.HasValue ? operacaoBolsaVenda.Quantidade.Value : 0;
                decimal valorVenda = operacaoBolsaVenda.Valor.HasValue ? operacaoBolsaVenda.Valor.Value : 0;

                Demais_Codif d1 = new Demais_Codif();

                d1.codigoAtivo = cdAtivoBolsa;
                d1.cod_Tp_Ativ = codigoAtivo;
                d1.dataInicioVigencia = ativoBolsa.DataInicioVigencia.HasValue ? ativoBolsa.DataInicioVigencia.Value : new DateTime(4000, 01, 01);

                // Demais_Codif - Aplic
                d1.getAplic.cod_Tp_Aplic = tipoAplicacao;
                d1.getAplic.qtde_Dias_Confid = this.diasConfidencialidade.ToString();

                // Aplic - Negoc
                d1.getAplic.getAplicNegoc.tipoNegocio = Convert.ToString((int)TipoNegociacaoCDA.Negociacao);
                d1.getAplic.getAplicNegoc.quantidadeVendasNegocio = quantidadeVenda;
                d1.getAplic.getAplicNegoc.valorVendasNegocio = valorVenda;
                d1.getAplic.getAplicNegoc.quantidadeAquisicaoNegocio = quantidadeCompra;
                d1.getAplic.getAplicNegoc.valorAquisicaoNegocio = valorCompra;

                // Aplic - Pos_Fim
                d1.getAplic.getAplicPosFim.qtde_Pos_Fim = quantidadeFinal;
                d1.getAplic.getAplicPosFim.custo_Correc_Pos_Fim = custoCorrecPosFim;
                d1.getAplic.getAplicPosFim.merc_Pos_Fim = valorFinal;

                // Adiciona Demais_Codif
                arq.getCorpo.getInform.getListAtiv.getDemais_Codif.Add(d1);
            }
            #endregion

            #region BTC doado
            ativoBolsaQuery = new AtivoBolsaQuery("A");
            PosicaoEmprestimoBolsaHistoricoQuery posicaoEmprestimoBolsaHistoricoQuery = new PosicaoEmprestimoBolsaHistoricoQuery("P");
            PosicaoEmprestimoBolsaHistoricoCollection posicaoEmprestimoBolsaHistoricoCollection = new PosicaoEmprestimoBolsaHistoricoCollection();

            posicaoEmprestimoBolsaHistoricoQuery.Select(posicaoEmprestimoBolsaHistoricoQuery.CdAtivoBolsa,
                                                       posicaoEmprestimoBolsaHistoricoQuery.Quantidade.Sum(),
                                                       posicaoEmprestimoBolsaHistoricoQuery.ValorMercado.Sum());
            posicaoEmprestimoBolsaHistoricoQuery.InnerJoin(ativoBolsaQuery).On(ativoBolsaQuery.CdAtivoBolsa == posicaoBolsaHistoricoQuery.CdAtivoBolsa);
            posicaoEmprestimoBolsaHistoricoQuery.Where(posicaoEmprestimoBolsaHistoricoQuery.IdCliente.Equal(idCarteira),
                                                    posicaoEmprestimoBolsaHistoricoQuery.DataHistorico.Equal(dataPosicao),
                                                    posicaoEmprestimoBolsaHistoricoQuery.PontaEmprestimo.Equal((byte)PontaEmprestimoBolsa.Doador),
                                                    ativoBolsaQuery.DataInicioVigencia.IsNotNull(),
                                                    ativoBolsaQuery.DataInicioVigencia.NotEqual(dataIndefinida));
            posicaoEmprestimoBolsaHistoricoQuery.GroupBy(posicaoEmprestimoBolsaHistoricoQuery.CdAtivoBolsa);
            posicaoEmprestimoBolsaHistoricoQuery.OrderBy(posicaoEmprestimoBolsaHistoricoQuery.CdAtivoBolsa.Ascending);
            posicaoEmprestimoBolsaHistoricoCollection.Load(posicaoEmprestimoBolsaHistoricoQuery);

            foreach (PosicaoEmprestimoBolsaHistorico posicaoEmprestimoBolsaHistorico in posicaoEmprestimoBolsaHistoricoCollection)
            {
                string cdAtivoBolsa = posicaoEmprestimoBolsaHistorico.CdAtivoBolsa;

                decimal quantidadeFinal = posicaoEmprestimoBolsaHistorico.Quantidade.HasValue ? posicaoEmprestimoBolsaHistorico.Quantidade.Value : 0;
                decimal valorFinal = posicaoEmprestimoBolsaHistorico.ValorMercado.HasValue ? posicaoEmprestimoBolsaHistorico.ValorMercado.Value : 0;

                AtivoBolsa ativoBolsa = new AtivoBolsa();
                List<esQueryItem> campos = new List<esQueryItem>();
                campos.Add(ativoBolsa.Query.TipoPapel);
                campos.Add(ativoBolsa.Query.DataInicioVigencia);
                campos.Add(ativoBolsa.Query.CodigoCDA);
                ativoBolsa.LoadByPrimaryKey(campos, cdAtivoBolsa);

                string tipoAplicacao = Convert.ToString((int)TipoAplicacaoCDA.TVMCedidoEmprestimo);

                string codigoAtivo = "";
                #region Checa codigo de ativo
                if (ativoBolsa.TipoPapel.Value == (byte)TipoPapelAtivo.Normal ||
                    ativoBolsa.TipoPapel.Value == (byte)TipoPapelAtivo.BDRNivelI ||
                    ativoBolsa.TipoPapel.Value == (byte)TipoPapelAtivo.BDRNivelII ||
                    ativoBolsa.TipoPapel.Value == (byte)TipoPapelAtivo.BDRNivelIII)
                {
                    codigoAtivo = Utilitario.Right(cdAtivoBolsa, 1) == "3" ? Convert.ToString((int)TipoAtivoCDA.AcaoOrdinaria) : Convert.ToString((int)TipoAtivoCDA.AcaoPreferencial);
                }
                else if (ativoBolsa.TipoPapel.Value == (byte)TipoPapelAtivo.BonusSubscricao)
                {
                    codigoAtivo = Convert.ToString((int)TipoAtivoCDA.BonusSubscricao);
                }
                else if (ativoBolsa.TipoPapel.Value == (byte)TipoPapelAtivo.CertificadoDepositoAcoes)
                {
                    codigoAtivo = Convert.ToString((int)TipoAtivoCDA.CertificadoDepositoAcoes);
                }
                else if (ativoBolsa.TipoPapel.Value == (byte)TipoPapelAtivo.ETF)
                {
                    codigoAtivo = Convert.ToString((int)TipoAtivoCDA.AcaoPreferencial); //Fundos de indice são jogados, por ora, em Ação                    
                }
                else if (ativoBolsa.TipoPapel.Value == (byte)TipoPapelAtivo.ReciboDepositoAcoes)
                {
                    codigoAtivo = Convert.ToString((int)TipoAtivoCDA.ReciboDepositoAcoes);
                }
                else if (ativoBolsa.TipoPapel.Value == (byte)TipoPapelAtivo.ReciboSubscricao)
                {
                    codigoAtivo = Convert.ToString((int)TipoAtivoCDA.ReciboSubscricao);
                }
                #endregion

                if (ativoBolsa.CodigoCDA.HasValue) //Se tiver o codigo CDA força pelo codigo informado
                {
                    codigoAtivo = Convert.ToString(ativoBolsa.CodigoCDA);
                }

                Demais_Codif d1 = new Demais_Codif();

                d1.codigoAtivo = cdAtivoBolsa;
                d1.cod_Tp_Ativ = codigoAtivo;
                d1.dataInicioVigencia = ativoBolsa.DataInicioVigencia.HasValue ? ativoBolsa.DataInicioVigencia.Value : new DateTime(4000, 01, 01);

                // Demais_Codif - Aplic
                d1.getAplic.cod_Tp_Aplic = tipoAplicacao;
                d1.getAplic.qtde_Dias_Confid = this.diasConfidencialidade.ToString();

                // Aplic - Negoc
                d1.getAplic.getAplicNegoc.tipoNegocio = Convert.ToString((int)TipoNegociacaoCDA.Negociacao);

                // Aplic - Pos_Fim
                d1.getAplic.getAplicPosFim.qtde_Pos_Fim = quantidadeFinal;
                d1.getAplic.getAplicPosFim.merc_Pos_Fim = valorFinal;

                // Adiciona Demais_Codif
                arq.getCorpo.getInform.getListAtiv.getDemais_Codif.Add(d1);
            }
            #endregion

            #region BTC tomado
            ativoBolsaQuery = new AtivoBolsaQuery("A");
            posicaoEmprestimoBolsaHistoricoQuery = new PosicaoEmprestimoBolsaHistoricoQuery("P");
            posicaoEmprestimoBolsaHistoricoCollection = new PosicaoEmprestimoBolsaHistoricoCollection();

            posicaoEmprestimoBolsaHistoricoQuery.Select(posicaoEmprestimoBolsaHistoricoQuery.CdAtivoBolsa,
                                                       posicaoEmprestimoBolsaHistoricoQuery.Quantidade.Sum(),
                                                       posicaoEmprestimoBolsaHistoricoQuery.ValorMercado.Sum());
            posicaoEmprestimoBolsaHistoricoQuery.InnerJoin(ativoBolsaQuery).On(ativoBolsaQuery.CdAtivoBolsa == posicaoBolsaHistoricoQuery.CdAtivoBolsa);
            posicaoEmprestimoBolsaHistoricoQuery.Where(posicaoEmprestimoBolsaHistoricoQuery.IdCliente.Equal(idCarteira),
                                                    posicaoEmprestimoBolsaHistoricoQuery.DataHistorico.Equal(dataPosicao),
                                                    posicaoEmprestimoBolsaHistoricoQuery.PontaEmprestimo.Equal((byte)PontaEmprestimoBolsa.Tomador),
                                                    ativoBolsaQuery.DataInicioVigencia.IsNotNull(),
                                                    ativoBolsaQuery.DataInicioVigencia.NotEqual(dataIndefinida));
            posicaoEmprestimoBolsaHistoricoQuery.GroupBy(posicaoEmprestimoBolsaHistoricoQuery.CdAtivoBolsa);
            posicaoEmprestimoBolsaHistoricoQuery.OrderBy(posicaoEmprestimoBolsaHistoricoQuery.CdAtivoBolsa.Ascending);
            posicaoEmprestimoBolsaHistoricoCollection.Load(posicaoEmprestimoBolsaHistoricoQuery);


            foreach (PosicaoEmprestimoBolsaHistorico posicaoEmprestimoBolsaHistorico in posicaoEmprestimoBolsaHistoricoCollection)
            {
                string cdAtivoBolsa = posicaoEmprestimoBolsaHistorico.CdAtivoBolsa;

                decimal quantidadeFinal = posicaoEmprestimoBolsaHistorico.Quantidade.HasValue ? posicaoEmprestimoBolsaHistorico.Quantidade.Value : 0;
                decimal valorFinal = posicaoEmprestimoBolsaHistorico.ValorMercado.HasValue ? posicaoEmprestimoBolsaHistorico.ValorMercado.Value : 0;

                AtivoBolsa ativoBolsa = new AtivoBolsa();
                List<esQueryItem> campos = new List<esQueryItem>();
                campos.Add(ativoBolsa.Query.TipoPapel);
                campos.Add(ativoBolsa.Query.DataInicioVigencia);
                campos.Add(ativoBolsa.Query.CodigoCDA);
                ativoBolsa.LoadByPrimaryKey(campos, cdAtivoBolsa);

                string tipoAplicacao = Convert.ToString((int)TipoAplicacaoCDA.TVMRecebidoEmprestimo);

                string codigoAtivo = "";
                #region Checa codigo de ativo
                if (ativoBolsa.TipoPapel.Value == (byte)TipoPapelAtivo.Normal ||
                    ativoBolsa.TipoPapel.Value == (byte)TipoPapelAtivo.BDRNivelI ||
                    ativoBolsa.TipoPapel.Value == (byte)TipoPapelAtivo.BDRNivelII ||
                    ativoBolsa.TipoPapel.Value == (byte)TipoPapelAtivo.BDRNivelIII)
                {
                    codigoAtivo = Utilitario.Right(cdAtivoBolsa, 1) == "3" ? Convert.ToString((int)TipoAtivoCDA.AcaoOrdinaria) : Convert.ToString((int)TipoAtivoCDA.AcaoPreferencial);
                }
                else if (ativoBolsa.TipoPapel.Value == (byte)TipoPapelAtivo.BonusSubscricao)
                {
                    codigoAtivo = Convert.ToString((int)TipoAtivoCDA.BonusSubscricao);
                }
                else if (ativoBolsa.TipoPapel.Value == (byte)TipoPapelAtivo.CertificadoDepositoAcoes)
                {
                    codigoAtivo = Convert.ToString((int)TipoAtivoCDA.CertificadoDepositoAcoes);
                }
                else if (ativoBolsa.TipoPapel.Value == (byte)TipoPapelAtivo.ETF)
                {
                    codigoAtivo = Convert.ToString((int)TipoAtivoCDA.AcaoPreferencial); //Fundos de indice são jogados, por ora, em Ação                    
                }
                else if (ativoBolsa.TipoPapel.Value == (byte)TipoPapelAtivo.ReciboDepositoAcoes)
                {
                    codigoAtivo = Convert.ToString((int)TipoAtivoCDA.ReciboDepositoAcoes);
                }
                else if (ativoBolsa.TipoPapel.Value == (byte)TipoPapelAtivo.ReciboSubscricao)
                {
                    codigoAtivo = Convert.ToString((int)TipoAtivoCDA.ReciboSubscricao);
                }
                #endregion

                if (ativoBolsa.CodigoCDA.HasValue) //Se tiver o codigo CDA força pelo codigo informado
                {
                    codigoAtivo = Convert.ToString(ativoBolsa.CodigoCDA);
                }

                Demais_Codif d1 = new Demais_Codif();

                d1.codigoAtivo = cdAtivoBolsa;
                d1.cod_Tp_Ativ = codigoAtivo;
                d1.dataInicioVigencia = ativoBolsa.DataInicioVigencia.HasValue ? ativoBolsa.DataInicioVigencia.Value : new DateTime(4000, 01, 01);

                // Demais_Codif - Aplic
                d1.getAplic.cod_Tp_Aplic = tipoAplicacao;
                d1.getAplic.qtde_Dias_Confid = this.diasConfidencialidade.ToString();

                // Aplic - Negoc
                d1.getAplic.getAplicNegoc.tipoNegocio = Convert.ToString((int)TipoNegociacaoCDA.Negociacao);

                // Aplic - Pos_Fim
                d1.getAplic.getAplicPosFim.qtde_Pos_Fim = quantidadeFinal;
                d1.getAplic.getAplicPosFim.merc_Pos_Fim = valorFinal;

                // Adiciona Demais_Codif
                arq.getCorpo.getInform.getListAtiv.getDemais_Codif.Add(d1);
            }
            #endregion

            #region Futuros/Opções Compradas
            PosicaoBMFHistoricoQuery posicaoBMFHistoricoQuery = new PosicaoBMFHistoricoQuery("P");
            AtivoBMFQuery ativoBMFQuery = new AtivoBMFQuery("A");

            PosicaoBMFHistoricoCollection posicaoBMFHistoricoCollection = new PosicaoBMFHistoricoCollection();
            posicaoBMFHistoricoQuery.Select(posicaoBMFHistoricoQuery.TipoMercado,
                                           posicaoBMFHistoricoQuery.CdAtivoBMF,
                                           posicaoBMFHistoricoQuery.Serie,
                                           posicaoBMFHistoricoQuery.Quantidade.Sum(),
                                           posicaoBMFHistoricoQuery.ValorMercado.Sum());
            posicaoBMFHistoricoQuery.InnerJoin(ativoBMFQuery).On(ativoBMFQuery.CdAtivoBMF == posicaoBMFHistoricoQuery.CdAtivoBMF &
                                                             ativoBMFQuery.Serie == posicaoBMFHistoricoQuery.Serie);
            posicaoBMFHistoricoQuery.Where(posicaoBMFHistoricoQuery.IdCliente.Equal(idCarteira) &
                                          posicaoBMFHistoricoQuery.DataHistorico.Equal(dataPosicao) &
                                          posicaoBMFHistoricoQuery.Quantidade.GreaterThan(0) &
                                          ativoBMFQuery.DataFimVigencia.IsNotNull() &
                                          ativoBMFQuery.DataFimVigencia.NotEqual(dataIndefinida));
            posicaoBMFHistoricoQuery.GroupBy(posicaoBMFHistoricoQuery.TipoMercado,
                                            posicaoBMFHistoricoQuery.CdAtivoBMF,
                                            posicaoBMFHistoricoQuery.Serie);
            posicaoBMFHistoricoQuery.OrderBy(posicaoBMFHistoricoQuery.TipoMercado.Ascending,
                                            posicaoBMFHistoricoQuery.CdAtivoBMF.Ascending);
            posicaoBMFHistoricoCollection.Load(posicaoBMFHistoricoQuery);

            foreach (PosicaoBMFHistorico posicaoBMFHistorico in posicaoBMFHistoricoCollection)
            {
                string cdAtivoBMF = posicaoBMFHistorico.CdAtivoBMF;
                string serie = posicaoBMFHistorico.Serie;
                byte tipoMercado = posicaoBMFHistorico.TipoMercado.Value;

                decimal quantidadeFinal = posicaoBMFHistorico.Quantidade.HasValue ? posicaoBMFHistorico.Quantidade.Value : 0;
                decimal valorFinal = (posicaoBMFHistorico.TipoMercado.Value != (byte)TipoMercadoBMF.Futuro && posicaoBMFHistorico.ValorMercado.HasValue) ? posicaoBMFHistorico.ValorMercado.Value : 0;

                AtivoBMF ativoBMF = new AtivoBMF();
                ativoBMF.LoadByPrimaryKey(cdAtivoBMF, serie);

                string tipoAplicacao = Convert.ToString((int)TipoAplicacaoCDA.MercadoFuturoComprado);
                
                OperacaoBMF operacaoBMFCompra = new OperacaoBMF();
                operacaoBMFCompra.Query.Select(operacaoBMFCompra.Query.Quantidade.Sum(),
                                               operacaoBMFCompra.Query.Valor.Sum());
                operacaoBMFCompra.Query.Where(operacaoBMFCompra.Query.IdCliente.Equal(idCarteira),
                                              operacaoBMFCompra.Query.CdAtivoBMF.Equal(cdAtivoBMF),
                                              operacaoBMFCompra.Query.Serie.Equal(serie),
                                              operacaoBMFCompra.Query.Data.GreaterThanOrEqual(dataInicioMes),
                                              operacaoBMFCompra.Query.Data.LessThanOrEqual(dataPosicao),
                                              operacaoBMFCompra.Query.TipoOperacao.In(TipoOperacaoBolsa.Compra, TipoOperacaoBolsa.CompraDaytrade));
                operacaoBMFCompra.Query.Load();

                decimal quantidadeCompra = operacaoBMFCompra.Quantidade.HasValue ? operacaoBMFCompra.Quantidade.Value : 0;
                decimal valorCompra = operacaoBMFCompra.Valor.HasValue ? operacaoBMFCompra.Valor.Value : 0;

                OperacaoBMF operacaoBMFVenda = new OperacaoBMF();
                operacaoBMFVenda.Query.Select(operacaoBMFVenda.Query.Quantidade.Sum(),
                                               operacaoBMFVenda.Query.Valor.Sum());
                operacaoBMFVenda.Query.Where(operacaoBMFVenda.Query.IdCliente.Equal(idCarteira),
                                              operacaoBMFVenda.Query.CdAtivoBMF.Equal(cdAtivoBMF),
                                              operacaoBMFVenda.Query.Serie.Equal(serie),
                                              operacaoBMFVenda.Query.Data.GreaterThanOrEqual(dataInicioMes),
                                              operacaoBMFVenda.Query.Data.LessThanOrEqual(dataPosicao),
                                              operacaoBMFVenda.Query.TipoOperacao.In(TipoOperacaoBolsa.Venda, TipoOperacaoBolsa.VendaDaytrade));
                operacaoBMFVenda.Query.Load();

                decimal quantidadeVenda = operacaoBMFVenda.Quantidade.HasValue ? operacaoBMFVenda.Quantidade.Value : 0;
                decimal valorVenda = operacaoBMFVenda.Valor.HasValue ? operacaoBMFVenda.Valor.Value : 0;

                Demais_Codif d1 = new Demais_Codif();

                d1.codigoAtivo = this.RetornaAtivoFuturoCDA(ativoBMF);
                d1.cod_Tp_Ativ = Convert.ToString((int)TipoAtivoCDA.ContratoFuturo);
                d1.dataInicioVigencia = ativoBMF.DataInicioVigencia.HasValue ? ativoBMF.DataInicioVigencia.Value : new DateTime(4000, 01, 01);

                // Demais_Codif - Aplic
                d1.getAplic.cod_Tp_Aplic = tipoAplicacao;
                d1.getAplic.qtde_Dias_Confid = this.diasConfidencialidade.ToString();

                // Aplic - Negoc
                d1.getAplic.getAplicNegoc.tipoNegocio = Convert.ToString((int)TipoNegociacaoCDA.Negociacao);
                d1.getAplic.getAplicNegoc.quantidadeVendasNegocio = quantidadeVenda;
                d1.getAplic.getAplicNegoc.valorVendasNegocio = valorVenda;
                d1.getAplic.getAplicNegoc.quantidadeAquisicaoNegocio = quantidadeCompra;
                d1.getAplic.getAplicNegoc.valorAquisicaoNegocio = valorCompra;

                // Aplic - Pos_Fim
                d1.getAplic.getAplicPosFim.qtde_Pos_Fim = quantidadeFinal;
                d1.getAplic.getAplicPosFim.merc_Pos_Fim = valorFinal;

                // Adiciona Demais_Codif
                arq.getCorpo.getInform.getListAtiv.getDemais_Codif.Add(d1);
            }
            #endregion

            #region Futuros/Opções Vendidas
            posicaoBMFHistoricoQuery = new PosicaoBMFHistoricoQuery("P");
            ativoBMFQuery = new AtivoBMFQuery("A");

            posicaoBMFHistoricoCollection = new PosicaoBMFHistoricoCollection();
            posicaoBMFHistoricoQuery.Select(posicaoBMFHistoricoQuery.TipoMercado,
                                           posicaoBMFHistoricoQuery.CdAtivoBMF,
                                           posicaoBMFHistoricoQuery.Serie,
                                           posicaoBMFHistoricoQuery.Quantidade.Sum(),
                                           posicaoBMFHistoricoQuery.ValorMercado.Sum());
            posicaoBMFHistoricoQuery.InnerJoin(ativoBMFQuery).On(ativoBMFQuery.CdAtivoBMF == posicaoBMFHistoricoQuery.CdAtivoBMF &
                                                             ativoBMFQuery.Serie == posicaoBMFHistoricoQuery.Serie);
            posicaoBMFHistoricoQuery.Where(posicaoBMFHistoricoQuery.IdCliente.Equal(idCarteira) &
                                          posicaoBMFHistoricoQuery.DataHistorico.Equal(dataPosicao) &
                                          posicaoBMFHistoricoQuery.Quantidade.LessThan(0) &
                                          ativoBMFQuery.DataFimVigencia.IsNotNull() &
                                          ativoBMFQuery.DataFimVigencia.NotEqual(dataIndefinida));
            posicaoBMFHistoricoQuery.GroupBy(posicaoBMFHistoricoQuery.TipoMercado,
                                            posicaoBMFHistoricoQuery.CdAtivoBMF,
                                            posicaoBMFHistoricoQuery.Serie);
            posicaoBMFHistoricoQuery.OrderBy(posicaoBMFHistoricoQuery.TipoMercado.Ascending,
                                            posicaoBMFHistoricoQuery.CdAtivoBMF.Ascending);
            posicaoBMFHistoricoCollection.Load(posicaoBMFHistoricoQuery);

            foreach (PosicaoBMFHistorico posicaoBMFHistorico in posicaoBMFHistoricoCollection)
            {
                string cdAtivoBMF = posicaoBMFHistorico.CdAtivoBMF;
                string serie = posicaoBMFHistorico.Serie;
                byte tipoMercado = posicaoBMFHistorico.TipoMercado.Value;

                decimal quantidadeFinal = posicaoBMFHistorico.Quantidade.HasValue ? posicaoBMFHistorico.Quantidade.Value : 0;
                decimal valorFinal = (posicaoBMFHistorico.TipoMercado.Value != (byte)TipoMercadoBMF.Futuro && posicaoBMFHistorico.ValorMercado.HasValue) ? posicaoBMFHistorico.ValorMercado.Value : 0;

                AtivoBMF ativoBMF = new AtivoBMF();
                ativoBMF.LoadByPrimaryKey(cdAtivoBMF, serie);

                string tipoAplicacao = Convert.ToString((int)TipoAplicacaoCDA.MercadoFuturoVendido);
                
                OperacaoBMF operacaoBMFCompra = new OperacaoBMF();
                operacaoBMFCompra.Query.Select(operacaoBMFCompra.Query.Quantidade.Sum(),
                                               operacaoBMFCompra.Query.Valor.Sum());
                operacaoBMFCompra.Query.Where(operacaoBMFCompra.Query.IdCliente.Equal(idCarteira),
                                              operacaoBMFCompra.Query.CdAtivoBMF.Equal(cdAtivoBMF),
                                              operacaoBMFCompra.Query.Serie.Equal(serie),
                                              operacaoBMFCompra.Query.Data.GreaterThanOrEqual(dataInicioMes),
                                              operacaoBMFCompra.Query.Data.LessThanOrEqual(dataPosicao),
                                              operacaoBMFCompra.Query.TipoOperacao.In(TipoOperacaoBolsa.Compra, TipoOperacaoBolsa.CompraDaytrade));
                operacaoBMFCompra.Query.Load();

                decimal quantidadeCompra = operacaoBMFCompra.Quantidade.HasValue ? operacaoBMFCompra.Quantidade.Value : 0;
                decimal valorCompra = operacaoBMFCompra.Valor.HasValue ? operacaoBMFCompra.Valor.Value : 0;

                OperacaoBMF operacaoBMFVenda = new OperacaoBMF();
                operacaoBMFVenda.Query.Select(operacaoBMFVenda.Query.Quantidade.Sum(),
                                               operacaoBMFVenda.Query.Valor.Sum());
                operacaoBMFVenda.Query.Where(operacaoBMFVenda.Query.IdCliente.Equal(idCarteira),
                                              operacaoBMFVenda.Query.CdAtivoBMF.Equal(cdAtivoBMF),
                                              operacaoBMFVenda.Query.Serie.Equal(serie),
                                              operacaoBMFVenda.Query.Data.GreaterThanOrEqual(dataInicioMes),
                                              operacaoBMFVenda.Query.Data.LessThanOrEqual(dataPosicao),
                                              operacaoBMFVenda.Query.TipoOperacao.In(TipoOperacaoBolsa.Venda, TipoOperacaoBolsa.VendaDaytrade));
                operacaoBMFVenda.Query.Load();

                decimal quantidadeVenda = operacaoBMFVenda.Quantidade.HasValue ? operacaoBMFVenda.Quantidade.Value : 0;
                decimal valorVenda = operacaoBMFVenda.Valor.HasValue ? operacaoBMFVenda.Valor.Value : 0;

                Demais_Codif d1 = new Demais_Codif();

                d1.codigoAtivo = this.RetornaAtivoFuturoCDA(ativoBMF);
                d1.cod_Tp_Ativ = Convert.ToString((int)TipoAtivoCDA.ContratoFuturo);

                d1.dataInicioVigencia = ativoBMF.DataInicioVigencia.HasValue ? ativoBMF.DataInicioVigencia.Value : new DateTime(4000, 01, 01);

                // Demais_Codif - Aplic
                d1.getAplic.cod_Tp_Aplic = tipoAplicacao;
                d1.getAplic.qtde_Dias_Confid = this.diasConfidencialidade.ToString();

                // Aplic - Negoc
                d1.getAplic.getAplicNegoc.tipoNegocio = Convert.ToString((int)TipoNegociacaoCDA.Negociacao);
                d1.getAplic.getAplicNegoc.quantidadeVendasNegocio = quantidadeVenda;
                d1.getAplic.getAplicNegoc.valorVendasNegocio = valorVenda;
                d1.getAplic.getAplicNegoc.quantidadeAquisicaoNegocio = quantidadeCompra;
                d1.getAplic.getAplicNegoc.valorAquisicaoNegocio = valorCompra;

                // Aplic - Pos_Fim
                d1.getAplic.getAplicPosFim.qtde_Pos_Fim = quantidadeFinal;
                d1.getAplic.getAplicPosFim.merc_Pos_Fim = valorFinal;

                // Adiciona Demais_Codif
                arq.getCorpo.getInform.getListAtiv.getDemais_Codif.Add(d1);
            }
            #endregion

            #region Termos comprados ("a receber")
            PosicaoTermoBolsaHistoricoCollection posicaoTermoBolsaHistoricoCollection = new PosicaoTermoBolsaHistoricoCollection();
            posicaoTermoBolsaHistoricoCollection.Query.Select(posicaoTermoBolsaHistoricoCollection.Query.CdAtivoBolsa,
                                                             posicaoTermoBolsaHistoricoCollection.Query.Quantidade.Sum(),
                                                             posicaoTermoBolsaHistoricoCollection.Query.ValorMercado.Sum());
            posicaoTermoBolsaHistoricoCollection.Query.Where(posicaoTermoBolsaHistoricoCollection.Query.IdCliente.Equal(idCarteira),
                                                            posicaoTermoBolsaHistoricoCollection.Query.DataHistorico.Equal(dataPosicao),
                                                            posicaoTermoBolsaHistoricoCollection.Query.Quantidade.GreaterThan(0));
            posicaoTermoBolsaHistoricoCollection.Query.GroupBy(posicaoTermoBolsaHistoricoCollection.Query.CdAtivoBolsa);
            posicaoTermoBolsaHistoricoCollection.Query.OrderBy(posicaoTermoBolsaHistoricoCollection.Query.CdAtivoBolsa.Ascending);
            posicaoTermoBolsaHistoricoCollection.Query.Load();

            foreach (PosicaoTermoBolsaHistorico posicaoTermoBolsaHistorico in posicaoTermoBolsaHistoricoCollection)
            {
                string cdAtivoBolsaTermo = posicaoTermoBolsaHistorico.CdAtivoBolsa;

                decimal quantidadeFinal = posicaoTermoBolsaHistorico.Quantidade.HasValue ? posicaoTermoBolsaHistorico.Quantidade.Value : 0;
                decimal valorFinal = posicaoTermoBolsaHistorico.ValorMercado.HasValue ? posicaoTermoBolsaHistorico.ValorMercado.Value : 0;

                AtivoBolsa ativoBolsaTermo = new AtivoBolsa();
                List<esQueryItem> campos = new List<esQueryItem>();
                campos.Add(ativoBolsaTermo.Query.CdAtivoBolsaObjeto);
                ativoBolsaTermo.LoadByPrimaryKey(campos, cdAtivoBolsaTermo);

                if (String.IsNullOrEmpty(ativoBolsaTermo.CdAtivoBolsaObjeto))
                {
                    throw new Exception("Ativo objeto não definido para o ativo Termo " + cdAtivoBolsaTermo);
                }

                string cdAtivoBolsa = ativoBolsaTermo.CdAtivoBolsaObjeto;

                AtivoBolsa ativoBolsa = new AtivoBolsa();
                campos = new List<esQueryItem>();
                campos.Add(ativoBolsa.Query.DataInicioVigencia);
                campos.Add(ativoBolsa.Query.CodigoCDA);
                campos.Add(ativoBolsa.Query.TipoPapel);
                ativoBolsa.LoadByPrimaryKey(campos, cdAtivoBolsa);

                string tipoAplicacao = Convert.ToString((int)TipoAplicacaoCDA.ComprasTermo);

                string codigoAtivo = "";
                #region Checa codigo de ativo
                if (ativoBolsa.TipoPapel.Value == (byte)TipoPapelAtivo.Normal ||
                    ativoBolsa.TipoPapel.Value == (byte)TipoPapelAtivo.BDRNivelI ||
                    ativoBolsa.TipoPapel.Value == (byte)TipoPapelAtivo.BDRNivelII ||
                    ativoBolsa.TipoPapel.Value == (byte)TipoPapelAtivo.BDRNivelIII)
                {
                    codigoAtivo = Utilitario.Right(cdAtivoBolsa, 1) == "3" ? Convert.ToString((int)TipoAtivoCDA.AcaoOrdinaria) : Convert.ToString((int)TipoAtivoCDA.AcaoPreferencial);
                }
                else if (ativoBolsa.TipoPapel.Value == (byte)TipoPapelAtivo.BonusSubscricao)
                {
                    codigoAtivo = Convert.ToString((int)TipoAtivoCDA.BonusSubscricao);
                }
                else if (ativoBolsa.TipoPapel.Value == (byte)TipoPapelAtivo.CertificadoDepositoAcoes)
                {
                    codigoAtivo = Convert.ToString((int)TipoAtivoCDA.CertificadoDepositoAcoes);
                }
                else if (ativoBolsa.TipoPapel.Value == (byte)TipoPapelAtivo.ETF)
                {
                    codigoAtivo = Convert.ToString((int)TipoAtivoCDA.AcaoPreferencial); //Fundos de indice são jogados, por ora, em Ação                    
                }
                else if (ativoBolsa.TipoPapel.Value == (byte)TipoPapelAtivo.ReciboDepositoAcoes)
                {
                    codigoAtivo = Convert.ToString((int)TipoAtivoCDA.ReciboDepositoAcoes);
                }
                else if (ativoBolsa.TipoPapel.Value == (byte)TipoPapelAtivo.ReciboSubscricao)
                {
                    codigoAtivo = Convert.ToString((int)TipoAtivoCDA.ReciboSubscricao);
                }
                #endregion

                if (ativoBolsa.CodigoCDA.HasValue) //Se tiver o codigo CDA força pelo codigo informado
                {
                    codigoAtivo = Convert.ToString(ativoBolsa.CodigoCDA);
                }

                OperacaoBolsa operacaoBolsaCompra = new OperacaoBolsa();
                operacaoBolsaCompra.Query.Select(operacaoBolsaCompra.Query.Quantidade.Sum(),
                                                 operacaoBolsaCompra.Query.Valor.Sum());
                operacaoBolsaCompra.Query.Where(operacaoBolsaCompra.Query.IdCliente.Equal(idCarteira),
                                                operacaoBolsaCompra.Query.CdAtivoBolsa.Equal(cdAtivoBolsaTermo),
                                                operacaoBolsaCompra.Query.Data.GreaterThanOrEqual(dataInicioMes),
                                                operacaoBolsaCompra.Query.Data.LessThanOrEqual(dataPosicao),
                                                operacaoBolsaCompra.Query.TipoOperacao.Equal(TipoOperacaoBolsa.Compra));
                operacaoBolsaCompra.Query.Load();

                decimal quantidadeCompra = operacaoBolsaCompra.Quantidade.HasValue ? operacaoBolsaCompra.Quantidade.Value : 0;
                decimal valorCompra = operacaoBolsaCompra.Valor.HasValue ? operacaoBolsaCompra.Valor.Value : 0;

                Demais_Codif d1 = new Demais_Codif();

                d1.codigoAtivo = cdAtivoBolsaTermo;
                d1.cod_Tp_Ativ = codigoAtivo;
                d1.dataInicioVigencia = ativoBolsa.DataInicioVigencia.HasValue ? ativoBolsa.DataInicioVigencia.Value : new DateTime(4000, 01, 01);

                // Demais_Codif - Aplic
                d1.getAplic.cod_Tp_Aplic = tipoAplicacao;
                d1.getAplic.qtde_Dias_Confid = this.diasConfidencialidade.ToString();

                // Aplic - Negoc
                d1.getAplic.getAplicNegoc.tipoNegocio = Convert.ToString((int)TipoNegociacaoCDA.Negociacao);
                d1.getAplic.getAplicNegoc.quantidadeVendasNegocio = 0;
                d1.getAplic.getAplicNegoc.valorVendasNegocio = 0;
                d1.getAplic.getAplicNegoc.quantidadeAquisicaoNegocio = quantidadeCompra;
                d1.getAplic.getAplicNegoc.valorAquisicaoNegocio = valorCompra;

                // Aplic - Pos_Fim
                d1.getAplic.getAplicPosFim.qtde_Pos_Fim = quantidadeFinal;
                d1.getAplic.getAplicPosFim.merc_Pos_Fim = valorFinal;

                // Adiciona Demais_Codif
                arq.getCorpo.getInform.getListAtiv.getDemais_Codif.Add(d1);
            }
            #endregion

            #region Termos vendidos ("a entregar")
            posicaoTermoBolsaHistoricoCollection = new PosicaoTermoBolsaHistoricoCollection();
            posicaoTermoBolsaHistoricoCollection.Query.Select(posicaoTermoBolsaHistoricoCollection.Query.CdAtivoBolsa,
                                                             posicaoTermoBolsaHistoricoCollection.Query.Quantidade.Sum(),
                                                             posicaoTermoBolsaHistoricoCollection.Query.ValorMercado.Sum());
            posicaoTermoBolsaHistoricoCollection.Query.Where(posicaoTermoBolsaHistoricoCollection.Query.IdCliente.Equal(idCarteira),
                                                            posicaoTermoBolsaHistoricoCollection.Query.DataHistorico.Equal(dataPosicao),
                                                            posicaoTermoBolsaHistoricoCollection.Query.Quantidade.LessThan(0));
            posicaoTermoBolsaHistoricoCollection.Query.GroupBy(posicaoTermoBolsaHistoricoCollection.Query.CdAtivoBolsa);
            posicaoTermoBolsaHistoricoCollection.Query.OrderBy(posicaoTermoBolsaHistoricoCollection.Query.CdAtivoBolsa.Ascending);
            posicaoTermoBolsaHistoricoCollection.Query.Load();

            foreach (PosicaoTermoBolsaHistorico posicaoTermoBolsaHistorico in posicaoTermoBolsaHistoricoCollection)
            {
                string cdAtivoBolsaTermo = posicaoTermoBolsaHistorico.CdAtivoBolsa;

                decimal quantidadeFinal = posicaoTermoBolsaHistorico.Quantidade.HasValue ? posicaoTermoBolsaHistorico.Quantidade.Value : 0;
                decimal valorFinal = posicaoTermoBolsaHistorico.ValorMercado.HasValue ? posicaoTermoBolsaHistorico.ValorMercado.Value : 0;

                AtivoBolsa ativoBolsaTermo = new AtivoBolsa();
                List<esQueryItem> campos = new List<esQueryItem>();
                campos.Add(ativoBolsaTermo.Query.CdAtivoBolsaObjeto);
                ativoBolsaTermo.LoadByPrimaryKey(campos, cdAtivoBolsaTermo);

                if (String.IsNullOrEmpty(ativoBolsaTermo.CdAtivoBolsaObjeto))
                {
                    throw new Exception("Ativo objeto não definido para o ativo Termo " + cdAtivoBolsaTermo);
                }

                string cdAtivoBolsa = ativoBolsaTermo.CdAtivoBolsaObjeto;

                AtivoBolsa ativoBolsa = new AtivoBolsa();
                campos = new List<esQueryItem>();
                campos.Add(ativoBolsa.Query.DataInicioVigencia);
                campos.Add(ativoBolsa.Query.CodigoCDA);
                campos.Add(ativoBolsa.Query.TipoPapel);
                ativoBolsa.LoadByPrimaryKey(campos, cdAtivoBolsa);

                string tipoAplicacao = Convert.ToString((int)TipoAplicacaoCDA.VendasTermo);

                string codigoAtivo = "";
                #region Checa codigo de ativo
                if (ativoBolsa.TipoPapel.Value == (byte)TipoPapelAtivo.Normal ||
                    ativoBolsa.TipoPapel.Value == (byte)TipoPapelAtivo.BDRNivelI ||
                    ativoBolsa.TipoPapel.Value == (byte)TipoPapelAtivo.BDRNivelII ||
                    ativoBolsa.TipoPapel.Value == (byte)TipoPapelAtivo.BDRNivelIII)
                {
                    codigoAtivo = Utilitario.Right(cdAtivoBolsa, 1) == "3" ? Convert.ToString((int)TipoAtivoCDA.AcaoOrdinaria) : Convert.ToString((int)TipoAtivoCDA.AcaoPreferencial);
                }
                else if (ativoBolsa.TipoPapel.Value == (byte)TipoPapelAtivo.BonusSubscricao)
                {
                    codigoAtivo = Convert.ToString((int)TipoAtivoCDA.BonusSubscricao);
                }
                else if (ativoBolsa.TipoPapel.Value == (byte)TipoPapelAtivo.CertificadoDepositoAcoes)
                {
                    codigoAtivo = Convert.ToString((int)TipoAtivoCDA.CertificadoDepositoAcoes);
                }
                else if (ativoBolsa.TipoPapel.Value == (byte)TipoPapelAtivo.ETF)
                {
                    codigoAtivo = Convert.ToString((int)TipoAtivoCDA.AcaoPreferencial); //Fundos de indice são jogados, por ora, em Ação                    
                }
                else if (ativoBolsa.TipoPapel.Value == (byte)TipoPapelAtivo.ReciboDepositoAcoes)
                {
                    codigoAtivo = Convert.ToString((int)TipoAtivoCDA.ReciboDepositoAcoes);
                }
                else if (ativoBolsa.TipoPapel.Value == (byte)TipoPapelAtivo.ReciboSubscricao)
                {
                    codigoAtivo = Convert.ToString((int)TipoAtivoCDA.ReciboSubscricao);
                }
                #endregion

                if (ativoBolsa.CodigoCDA.HasValue) //Se tiver o codigo CDA força pelo codigo informado
                {
                    codigoAtivo = Convert.ToString(ativoBolsa.CodigoCDA);
                }

                OperacaoBolsa operacaoBolsaVenda = new OperacaoBolsa();
                operacaoBolsaVenda.Query.Select(operacaoBolsaVenda.Query.Quantidade.Sum(),
                                                 operacaoBolsaVenda.Query.Valor.Sum());
                operacaoBolsaVenda.Query.Where(operacaoBolsaVenda.Query.IdCliente.Equal(idCarteira),
                                                operacaoBolsaVenda.Query.CdAtivoBolsa.Equal(cdAtivoBolsaTermo),
                                                operacaoBolsaVenda.Query.Data.GreaterThanOrEqual(dataInicioMes),
                                                operacaoBolsaVenda.Query.Data.LessThanOrEqual(dataPosicao),
                                                operacaoBolsaVenda.Query.TipoOperacao.Equal(TipoOperacaoBolsa.Venda));
                operacaoBolsaVenda.Query.Load();

                decimal quantidadeVenda = operacaoBolsaVenda.Quantidade.HasValue ? operacaoBolsaVenda.Quantidade.Value : 0;
                decimal valorVenda = operacaoBolsaVenda.Valor.HasValue ? operacaoBolsaVenda.Valor.Value : 0;

                Demais_Codif d1 = new Demais_Codif();

                d1.codigoAtivo = cdAtivoBolsa;
                d1.cod_Tp_Ativ = codigoAtivo;
                d1.dataInicioVigencia = ativoBolsa.DataInicioVigencia.HasValue ? ativoBolsa.DataInicioVigencia.Value : new DateTime(4000, 01, 01);

                // Demais_Codif - Aplic
                d1.getAplic.cod_Tp_Aplic = tipoAplicacao;
                d1.getAplic.qtde_Dias_Confid = this.diasConfidencialidade.ToString();

                // Aplic - Negoc
                d1.getAplic.getAplicNegoc.tipoNegocio = Convert.ToString((int)TipoNegociacaoCDA.Negociacao);
                d1.getAplic.getAplicNegoc.quantidadeVendasNegocio = quantidadeVenda;
                d1.getAplic.getAplicNegoc.valorVendasNegocio = valorVenda;
                d1.getAplic.getAplicNegoc.quantidadeAquisicaoNegocio = 0;
                d1.getAplic.getAplicNegoc.valorAquisicaoNegocio = 0;

                // Aplic - Pos_Fim
                d1.getAplic.getAplicPosFim.qtde_Pos_Fim = quantidadeFinal;
                d1.getAplic.getAplicPosFim.merc_Pos_Fim = valorFinal;

                // Adiciona Demais_Codif
                arq.getCorpo.getInform.getListAtiv.getDemais_Codif.Add(d1);
            }
            #endregion

            #region For Monta Titulos privados de instituição financeira (Compras Finais)
            PosicaoRendaFixaHistoricoQuery posicaoRendaFixaHistoricoQuery = new PosicaoRendaFixaHistoricoQuery("P");
            TituloRendaFixaQuery tituloRendaFixaQuery = new TituloRendaFixaQuery("T");
            PapelRendaFixaQuery papelRendaFixaQuery = new PapelRendaFixaQuery("A");
            EmissorQuery emissorQuery = new EmissorQuery("E");
            posicaoRendaFixaHistoricoQuery.Select(posicaoRendaFixaHistoricoQuery.IdTitulo,
                                                  posicaoRendaFixaHistoricoQuery.TipoNegociacao,
                                                  tituloRendaFixaQuery.CodigoCDA,
                                                  posicaoRendaFixaHistoricoQuery.Quantidade.Sum(),
                                                  posicaoRendaFixaHistoricoQuery.ValorMercado.Sum());
            posicaoRendaFixaHistoricoQuery.InnerJoin(tituloRendaFixaQuery).On(tituloRendaFixaQuery.IdTitulo == posicaoRendaFixaHistoricoQuery.IdTitulo);
            posicaoRendaFixaHistoricoQuery.InnerJoin(papelRendaFixaQuery).On(papelRendaFixaQuery.IdPapel == tituloRendaFixaQuery.IdPapel);
            posicaoRendaFixaHistoricoQuery.InnerJoin(emissorQuery).On(emissorQuery.IdEmissor == tituloRendaFixaQuery.IdEmissor);
            posicaoRendaFixaHistoricoQuery.Where(posicaoRendaFixaHistoricoQuery.IdCliente.Equal(idCarteira),
                                        posicaoRendaFixaHistoricoQuery.DataHistorico.Equal(dataPosicao),
                                        posicaoRendaFixaHistoricoQuery.TipoOperacao.Equal((byte)TipoOperacaoTitulo.CompraFinal),
                                        tituloRendaFixaQuery.CodigoCDA.IsNotNull(),
                                        tituloRendaFixaQuery.CodigoCDA.NotEqual(0),
                                        papelRendaFixaQuery.TipoPapel.Equal((byte)TipoPapelTitulo.Privado),
                                        emissorQuery.TipoEmissor.Equal((byte)TipoEmissor.InstituicaoFinanceira));
            posicaoRendaFixaHistoricoQuery.GroupBy(posicaoRendaFixaHistoricoQuery.IdTitulo,
                                                  posicaoRendaFixaHistoricoQuery.TipoNegociacao,
                                                  tituloRendaFixaQuery.CodigoCDA);

            PosicaoRendaFixaHistoricoCollection coll = new PosicaoRendaFixaHistoricoCollection();
            coll.Load(posicaoRendaFixaHistoricoQuery);

            foreach (PosicaoRendaFixaHistorico p in coll)
            {
                int idTitulo = p.IdTitulo.Value;
                byte tipoNegociacao = p.TipoNegociacao.Value;
                string codigoCDA = Convert.ToString(p.GetColumn(TituloRendaFixaMetadata.ColumnNames.CodigoCDA));
                
                decimal quantidade = p.Quantidade.Value;
                decimal valor = p.ValorMercado.Value;

                decimal quantidadeAplicacao = 0;
                decimal valorAplicacao = 0;
                decimal quantidadeResgate = 0;
                decimal valorResgate = 0;
                if (tipoNegociacao != (byte)TipoNegociacaoTitulo.Vencimento) //Vencimento desconsidera de forma sumária qq movimentação, já que por definição não pode operar
                {
                    OperacaoRendaFixa operacaoRendaFixa = new OperacaoRendaFixa();
                    operacaoRendaFixa.Query.Select(operacaoRendaFixa.Query.Quantidade.Sum(),
                                                   operacaoRendaFixa.Query.Valor.Sum());
                    operacaoRendaFixa.Query.Where(operacaoRendaFixa.Query.IdCliente.Equal(idCarteira),
                                                  operacaoRendaFixa.Query.IdTitulo.Equal(p.IdTitulo.Value),
                                                  operacaoRendaFixa.Query.DataOperacao.GreaterThanOrEqual(dataInicioMes),
                                                  operacaoRendaFixa.Query.DataOperacao.LessThanOrEqual(dataPosicao),
                                                  operacaoRendaFixa.Query.TipoOperacao.Equal((byte)TipoOperacaoTitulo.CompraFinal));
                    operacaoRendaFixa.Query.Load();

                    if (operacaoRendaFixa.Quantidade.HasValue)
                    {
                        quantidadeAplicacao = operacaoRendaFixa.Quantidade.Value;
                        valorAplicacao = operacaoRendaFixa.Valor.Value;
                    }


                    operacaoRendaFixa = new OperacaoRendaFixa();
                    operacaoRendaFixa.Query.Select(operacaoRendaFixa.Query.Quantidade.Sum(),
                                                   operacaoRendaFixa.Query.Valor.Sum());
                    operacaoRendaFixa.Query.Where(operacaoRendaFixa.Query.IdCliente.Equal(idCarteira),
                                                  operacaoRendaFixa.Query.IdTitulo.Equal(p.IdTitulo.Value),
                                                  operacaoRendaFixa.Query.DataOperacao.GreaterThanOrEqual(dataInicioMes),
                                                  operacaoRendaFixa.Query.DataOperacao.LessThanOrEqual(dataPosicao),
                                                  operacaoRendaFixa.Query.TipoOperacao.In((byte)TipoOperacaoTitulo.VendaFinal,
                                                                                          (byte)TipoOperacaoTitulo.VendaTotal));
                    operacaoRendaFixa.Query.Load();

                    if (operacaoRendaFixa.Quantidade.HasValue)
                    {
                        quantidadeResgate = operacaoRendaFixa.Quantidade.Value;
                        valorResgate = operacaoRendaFixa.Valor.Value;
                    }
                }

                Demais_Codif d1 = new Demais_Codif();

                d1.getAplic.cod_Tp_Aplic = Convert.ToString((int)TipoAplicacaoCDA.DepositoPrazo_OutrosTitulosIF);
                d1.cod_Tp_Ativ = codigoCDA;
                                
                //
                // Dep_Prazo_Tit - Aplic
                d1.getAplic.qtde_Dias_Confid = this.diasConfidencialidade.ToString();

                // Aplic - Negoc
                d1.getAplic.getAplicNegoc.tipoNegocio = p.TipoNegociacao.Value == (byte)TipoNegociacaoTitulo.Vencimento ? "2" : "1";
                d1.getAplic.getAplicNegoc.quantidadeVendasNegocio = quantidadeResgate;
                d1.getAplic.getAplicNegoc.valorVendasNegocio = valorResgate;
                d1.getAplic.getAplicNegoc.quantidadeAquisicaoNegocio = quantidadeAplicacao;
                d1.getAplic.getAplicNegoc.valorAquisicaoNegocio = valorAplicacao;

                // Aplic - Pos_Fim
                d1.getAplic.getAplicPosFim.qtde_Pos_Fim = quantidade;
                d1.getAplic.getAplicPosFim.merc_Pos_Fim = valor;

                // Adiciona Dep_Prazo_Tit
                arq.getCorpo.getInform.getListAtiv.getDemais_Codif.Add(d1);
                // -------------------------------------------------
            }
            #endregion

            #region For Monta Titulos privados do agronegócio e outros de crédito privado
            posicaoRendaFixaHistoricoQuery = new PosicaoRendaFixaHistoricoQuery("P");
            tituloRendaFixaQuery = new TituloRendaFixaQuery("T");
            papelRendaFixaQuery = new PapelRendaFixaQuery("A");
            emissorQuery = new EmissorQuery("E");
            posicaoRendaFixaHistoricoQuery.Select(posicaoRendaFixaHistoricoQuery.IdTitulo,
                                                  posicaoRendaFixaHistoricoQuery.TipoNegociacao,
                                                  papelRendaFixaQuery.Classe,
                                                  tituloRendaFixaQuery.CodigoCDA,
                                                  posicaoRendaFixaHistoricoQuery.Quantidade.Sum(),
                                                  posicaoRendaFixaHistoricoQuery.ValorMercado.Sum());
            posicaoRendaFixaHistoricoQuery.InnerJoin(tituloRendaFixaQuery).On(tituloRendaFixaQuery.IdTitulo == posicaoRendaFixaHistoricoQuery.IdTitulo);
            posicaoRendaFixaHistoricoQuery.InnerJoin(papelRendaFixaQuery).On(papelRendaFixaQuery.IdPapel == tituloRendaFixaQuery.IdPapel);
            posicaoRendaFixaHistoricoQuery.InnerJoin(emissorQuery).On(emissorQuery.IdEmissor == tituloRendaFixaQuery.IdEmissor);
            posicaoRendaFixaHistoricoQuery.Where(posicaoRendaFixaHistoricoQuery.IdCliente.Equal(idCarteira),
                                        posicaoRendaFixaHistoricoQuery.DataHistorico.Equal(dataPosicao),
                                        posicaoRendaFixaHistoricoQuery.TipoOperacao.Equal((byte)TipoOperacaoTitulo.CompraFinal),
                                        tituloRendaFixaQuery.CodigoCDA.IsNotNull(),
                                        tituloRendaFixaQuery.CodigoCDA.NotEqual(0),
                                        papelRendaFixaQuery.TipoPapel.Equal((byte)TipoPapelTitulo.Privado),
                                        emissorQuery.TipoEmissor.NotEqual((byte)TipoEmissor.InstituicaoFinanceira));
            posicaoRendaFixaHistoricoQuery.GroupBy(posicaoRendaFixaHistoricoQuery.IdTitulo,
                                                  posicaoRendaFixaHistoricoQuery.TipoNegociacao,
                                                  papelRendaFixaQuery.Classe,
                                                  tituloRendaFixaQuery.CodigoCDA);

            coll = new PosicaoRendaFixaHistoricoCollection();
            coll.Load(posicaoRendaFixaHistoricoQuery);

            foreach (PosicaoRendaFixaHistorico p in coll)
            {
                int classe = Convert.ToInt32(p.GetColumn(PapelRendaFixaMetadata.ColumnNames.Classe));
                int idTitulo = p.IdTitulo.Value;
                byte tipoNegociacao = p.TipoNegociacao.Value;
                string codigoCDA = Convert.ToString(p.GetColumn(TituloRendaFixaMetadata.ColumnNames.CodigoCDA));
                
                decimal quantidade = p.Quantidade.Value;
                decimal valor = p.ValorMercado.Value;

                decimal quantidadeAplicacao = 0;
                decimal valorAplicacao = 0;
                decimal quantidadeResgate = 0;
                decimal valorResgate = 0;
                if (tipoNegociacao != (byte)TipoNegociacaoTitulo.Vencimento) //Vencimento desconsidera de forma sumária qq movimentação, já que por definição não pode operar
                {
                    OperacaoRendaFixa operacaoRendaFixa = new OperacaoRendaFixa();
                    operacaoRendaFixa.Query.Select(operacaoRendaFixa.Query.Quantidade.Sum(),
                                                   operacaoRendaFixa.Query.Valor.Sum());
                    operacaoRendaFixa.Query.Where(operacaoRendaFixa.Query.IdCliente.Equal(idCarteira),
                                                  operacaoRendaFixa.Query.IdTitulo.Equal(p.IdTitulo.Value),
                                                  operacaoRendaFixa.Query.DataOperacao.GreaterThanOrEqual(dataInicioMes),
                                                  operacaoRendaFixa.Query.DataOperacao.LessThanOrEqual(dataPosicao),
                                                  operacaoRendaFixa.Query.TipoOperacao.Equal((byte)TipoOperacaoTitulo.CompraFinal));
                    operacaoRendaFixa.Query.Load();

                    if (operacaoRendaFixa.Quantidade.HasValue)
                    {
                        quantidadeAplicacao = operacaoRendaFixa.Quantidade.Value;
                        valorAplicacao = operacaoRendaFixa.Valor.Value;
                    }


                    operacaoRendaFixa = new OperacaoRendaFixa();
                    operacaoRendaFixa.Query.Select(operacaoRendaFixa.Query.Quantidade.Sum(),
                                                   operacaoRendaFixa.Query.Valor.Sum());
                    operacaoRendaFixa.Query.Where(operacaoRendaFixa.Query.IdCliente.Equal(idCarteira),
                                                  operacaoRendaFixa.Query.IdTitulo.Equal(p.IdTitulo.Value),
                                                  operacaoRendaFixa.Query.DataOperacao.GreaterThanOrEqual(dataInicioMes),
                                                  operacaoRendaFixa.Query.DataOperacao.LessThanOrEqual(dataPosicao),
                                                  operacaoRendaFixa.Query.TipoOperacao.In((byte)TipoOperacaoTitulo.VendaFinal,
                                                                                          (byte)TipoOperacaoTitulo.VendaTotal));
                    operacaoRendaFixa.Query.Load();

                    if (operacaoRendaFixa.Quantidade.HasValue)
                    {
                        quantidadeResgate = operacaoRendaFixa.Quantidade.Value;
                        valorResgate = operacaoRendaFixa.Valor.Value;
                    }
                }

                Demais_Codif d1 = new Demais_Codif();

                if (classe == (int)ClasseRendaFixa.Debenture)
                {
                    d1.getAplic.cod_Tp_Aplic = Convert.ToString((int)TipoAplicacaoCDA.DebentureSimples);
                }
                else
                {
                    d1.getAplic.cod_Tp_Aplic = Convert.ToString((int)TipoAplicacaoCDA.DepositoPrazo_OutrosTitulosIF);
                }

                d1.cod_Tp_Ativ = codigoCDA;

                //
                // Dep_Prazo_Tit - Aplic
                d1.getAplic.qtde_Dias_Confid = this.diasConfidencialidade.ToString();

                // Aplic - Negoc
                d1.getAplic.getAplicNegoc.tipoNegocio = p.TipoNegociacao.Value == (byte)TipoNegociacaoTitulo.Vencimento ? "2" : "1";
                d1.getAplic.getAplicNegoc.quantidadeVendasNegocio = quantidadeResgate;
                d1.getAplic.getAplicNegoc.valorVendasNegocio = valorResgate;
                d1.getAplic.getAplicNegoc.quantidadeAquisicaoNegocio = quantidadeAplicacao;
                d1.getAplic.getAplicNegoc.valorAquisicaoNegocio = valorAplicacao;

                // Aplic - Pos_Fim
                d1.getAplic.getAplicPosFim.qtde_Pos_Fim = quantidade;
                d1.getAplic.getAplicPosFim.merc_Pos_Fim = valor;

                // Adiciona Dep_Prazo_Tit
                arq.getCorpo.getInform.getListAtiv.getDemais_Codif.Add(d1);

                // -------------------------------------------------
            }
            #endregion            

            

        }
        #endregion

        #region Monta o Node Demais_N_Codif
        /// <summary>
        /// Monta o Node Demais_N_Codif
        /// </summary>
        /// <param name="dataPosicao"></param>
        /// <param name="arq"></param>
        /// <param name="idCarteira"></param>
        private void MontaDemais_N_Codif(DateTime dataPosicao, CDA_Xml arq, Carteira carteira)
        {
            int idCarteira = carteira.IdCarteira.Value;
            DateTime dataInicioMes = Calendario.RetornaPrimeiroDiaCorridoMes(dataPosicao, 0);

            DateTime dataIndefinida = new DateTime(4000, 01, 01);

            #region For Monta Objeto (Disponibilidade) em "Demais_N_Codif"
            decimal caixa = 0;
            SaldoCaixa saldoCaixa = new SaldoCaixa();
            saldoCaixa.Query.Select(saldoCaixa.Query.SaldoFechamento.Sum());
            saldoCaixa.Query.Where(saldoCaixa.Query.IdCliente.Equal(idCarteira),
                                   saldoCaixa.Query.Data.Equal(dataPosicao));
            saldoCaixa.Query.Load();

            if (saldoCaixa.SaldoFechamento.HasValue)
            {
                caixa = saldoCaixa.SaldoFechamento.Value;
            }

            Demais_N_Codif d = new Demais_N_Codif();
            d.cod_Tp_Ativ = Convert.ToString((int)TipoAtivoCDA.Outros);
            d.descricao = "Disponibilidade";
            // Demais_N_Codif - Emis
            d.getEmis.tp_PF_PJ = String.Empty; // Colocar empty para aparecer nó vazio
            d.getEmis.nr_PF_PJ = String.Empty;
            d.getEmis.nm = String.Empty;

            // Demais_N_Codif - Aplic
            d.getAplic.cod_Tp_Aplic = Convert.ToString((int)TipoAplicacaoCDA.Disponibilidades);
            d.getAplic.qtde_Dias_Confid = this.diasConfidencialidade.ToString();

            // Demais_N_Codif - Aplic - Negoc
            d.getAplic.getAplicNegoc.tipoNegocio = Convert.ToString((int)TipoNegociacaoCDA.Negociacao);

            // Demais_N_Codif - Aplic - Pos_FIM
            d.getAplic.getAplicPosFim.qtde_Pos_Fim = null;
            d.getAplic.getAplicPosFim.merc_Pos_Fim = caixa;

            // Adiciona Demais_N_Codif
            arq.getCorpo.getInform.getListAtiv.getDemais_N_Codif.Add(d);
            #endregion

            #region For Monta Objeto (Valores a Pagar) em "Demais_N_Codif"
            decimal valorPagar = 0;
            LiquidacaoHistorico liquidacaoHistorico = new LiquidacaoHistorico();
            liquidacaoHistorico.Query.Select(liquidacaoHistorico.Query.Valor.Sum());
            liquidacaoHistorico.Query.Where(liquidacaoHistorico.Query.IdCliente.Equal(idCarteira),
                                   liquidacaoHistorico.Query.DataVencimento.GreaterThan(dataPosicao),
                                   liquidacaoHistorico.Query.DataLancamento.LessThanOrEqual(dataPosicao),
                                   liquidacaoHistorico.Query.Valor.LessThan(0),
                                   liquidacaoHistorico.Query.DataHistorico.Equal(dataPosicao));

            liquidacaoHistorico.Query.Load();

            if (liquidacaoHistorico.Valor.HasValue)
            {
                valorPagar = liquidacaoHistorico.Valor.Value;
            }

            d = new Demais_N_Codif();
            d.cod_Tp_Ativ = Convert.ToString((int)TipoAtivoCDA.Outros);
            d.descricao = "Valores a Pagar";
            // Demais_N_Codif - Emis
            d.getEmis.tp_PF_PJ = String.Empty; // Colocar empty para aparecer nó vazio
            d.getEmis.nr_PF_PJ = String.Empty;
            d.getEmis.nm = String.Empty;

            // Demais_N_Codif - Aplic
            d.getAplic.cod_Tp_Aplic = Convert.ToString((int)TipoAplicacaoCDA.ValoresPagar);
            d.getAplic.qtde_Dias_Confid = this.diasConfidencialidade.ToString();

            // Demais_N_Codif - Aplic - Negoc
            d.getAplic.getAplicNegoc.tipoNegocio = Convert.ToString((int)TipoNegociacaoCDA.Negociacao);

            // Demais_N_Codif - Aplic - Pos_FIM
            d.getAplic.getAplicPosFim.qtde_Pos_Fim = null;
            d.getAplic.getAplicPosFim.custo_Correc_Pos_Fim = null;
            d.getAplic.getAplicPosFim.merc_Pos_Fim = valorPagar;

            // Adiciona Demais_N_Codif
            arq.getCorpo.getInform.getListAtiv.getDemais_N_Codif.Add(d);
            #endregion

            #region For Monta Objeto (Valores a Receber) em "Demais_N_Codif"
            decimal valorReceber = 0;
            liquidacaoHistorico = new LiquidacaoHistorico();
            liquidacaoHistorico.Query.Select(liquidacaoHistorico.Query.Valor.Sum());
            liquidacaoHistorico.Query.Where(liquidacaoHistorico.Query.IdCliente.Equal(idCarteira),
                                   liquidacaoHistorico.Query.DataVencimento.GreaterThan(dataPosicao),
                                   liquidacaoHistorico.Query.DataLancamento.LessThanOrEqual(dataPosicao),
                                   liquidacaoHistorico.Query.Valor.GreaterThan(0),
                                   liquidacaoHistorico.Query.DataHistorico.Equal(dataPosicao));

            liquidacaoHistorico.Query.Load();

            if (liquidacaoHistorico.Valor.HasValue)
            {
                valorReceber = liquidacaoHistorico.Valor.Value;
            }

            d = new Demais_N_Codif();
            d.cod_Tp_Ativ = Convert.ToString((int)TipoAtivoCDA.Outros);
            d.descricao = "Valores a Receber";
            // Demais_N_Codif - Emis
            d.getEmis.tp_PF_PJ = String.Empty; // Colocar empty para aparecer nó vazio
            d.getEmis.nr_PF_PJ = String.Empty;
            d.getEmis.nm = String.Empty;

            // Demais_N_Codif - Aplic
            d.getAplic.cod_Tp_Aplic = Convert.ToString((int)TipoAplicacaoCDA.ValoresReceber);
            d.getAplic.qtde_Dias_Confid = this.diasConfidencialidade.ToString();

            // Demais_N_Codif - Aplic - Negoc
            d.getAplic.getAplicNegoc.tipoNegocio = Convert.ToString((int)TipoNegociacaoCDA.Negociacao);

            // Demais_N_Codif - Aplic - Pos_FIM
            d.getAplic.getAplicPosFim.qtde_Pos_Fim = null;
            d.getAplic.getAplicPosFim.merc_Pos_Fim = valorReceber;

            // Adiciona Demais_N_Codif
            arq.getCorpo.getInform.getListAtiv.getDemais_N_Codif.Add(d);
            #endregion

            #region Acoes com posição negativa
            PosicaoBolsaHistoricoCollection posicaoBolsaHistoricoCollection = new PosicaoBolsaHistoricoCollection();
            posicaoBolsaHistoricoCollection.Query.Select(posicaoBolsaHistoricoCollection.Query.CdAtivoBolsa,
                                                         posicaoBolsaHistoricoCollection.Query.Quantidade.Sum(),
                                                         posicaoBolsaHistoricoCollection.Query.ValorCustoLiquido.Sum(),
                                                         posicaoBolsaHistoricoCollection.Query.ValorMercado.Sum());
            posicaoBolsaHistoricoCollection.Query.Where(posicaoBolsaHistoricoCollection.Query.IdCliente.Equal(idCarteira),
                                                        posicaoBolsaHistoricoCollection.Query.DataHistorico.Equal(dataPosicao),
                                                        posicaoBolsaHistoricoCollection.Query.TipoMercado.Equal(TipoMercadoBolsa.MercadoVista),
                                                        posicaoBolsaHistoricoCollection.Query.Quantidade.LessThan(0));
            posicaoBolsaHistoricoCollection.Query.GroupBy(posicaoBolsaHistoricoCollection.Query.CdAtivoBolsa);
            posicaoBolsaHistoricoCollection.Query.OrderBy(posicaoBolsaHistoricoCollection.Query.CdAtivoBolsa.Ascending);
            posicaoBolsaHistoricoCollection.Query.Load();

            foreach (PosicaoBolsaHistorico posicaoBolsaHistorico in posicaoBolsaHistoricoCollection)
            {
                string cdAtivoBolsa = posicaoBolsaHistorico.CdAtivoBolsa;

                AtivoBolsa ativoBolsa = new AtivoBolsa();
                List<esQueryItem> campos = new List<esQueryItem>();
                campos.Add(ativoBolsa.Query.IdEmissor);
                ativoBolsa.LoadByPrimaryKey(campos, cdAtivoBolsa);

                decimal quantidadeFinal = posicaoBolsaHistorico.Quantidade.HasValue ? posicaoBolsaHistorico.Quantidade.Value : 0;
                decimal valorFinal = posicaoBolsaHistorico.ValorMercado.HasValue ? posicaoBolsaHistorico.ValorMercado.Value : 0;
                decimal custoCorrecPosFim = posicaoBolsaHistorico.ValorCustoLiquido.HasValue ? posicaoBolsaHistorico.ValorCustoLiquido.Value : 0;

                OperacaoBolsa operacaoBolsaCompra = new OperacaoBolsa();
                operacaoBolsaCompra.Query.Select(operacaoBolsaCompra.Query.Quantidade.Sum(),
                                                 operacaoBolsaCompra.Query.Valor.Sum());
                operacaoBolsaCompra.Query.Where(operacaoBolsaCompra.Query.IdCliente.Equal(idCarteira),
                                                operacaoBolsaCompra.Query.CdAtivoBolsa.Equal(cdAtivoBolsa),
                                                operacaoBolsaCompra.Query.Data.GreaterThanOrEqual(dataInicioMes),
                                                operacaoBolsaCompra.Query.Data.LessThanOrEqual(dataPosicao),
                                                operacaoBolsaCompra.Query.TipoOperacao.In(TipoOperacaoBolsa.Compra, TipoOperacaoBolsa.CompraDaytrade));
                operacaoBolsaCompra.Query.Load();

                decimal quantidadeCompra = operacaoBolsaCompra.Quantidade.HasValue ? operacaoBolsaCompra.Quantidade.Value : 0;
                decimal valorCompra = operacaoBolsaCompra.Valor.HasValue ? operacaoBolsaCompra.Valor.Value : 0;

                OperacaoBolsa operacaoBolsaVenda = new OperacaoBolsa();
                operacaoBolsaVenda.Query.Select(operacaoBolsaVenda.Query.Quantidade.Sum(),
                                                operacaoBolsaVenda.Query.Valor.Sum());
                operacaoBolsaVenda.Query.Where(operacaoBolsaVenda.Query.IdCliente.Equal(idCarteira),
                                               operacaoBolsaVenda.Query.CdAtivoBolsa.Equal(cdAtivoBolsa),
                                               operacaoBolsaVenda.Query.Data.GreaterThanOrEqual(dataInicioMes),
                                               operacaoBolsaVenda.Query.Data.LessThanOrEqual(dataPosicao),
                                               operacaoBolsaVenda.Query.TipoOperacao.In(TipoOperacaoBolsa.Venda, TipoOperacaoBolsa.VendaDaytrade));
                operacaoBolsaVenda.Query.Load();

                decimal quantidadeVenda = operacaoBolsaVenda.Quantidade.HasValue ? operacaoBolsaVenda.Quantidade.Value : 0;
                decimal valorVenda = operacaoBolsaVenda.Valor.HasValue ? operacaoBolsaVenda.Valor.Value : 0;

                Emissor emissor = new Emissor();
                emissor.LoadByPrimaryKey(ativoBolsa.IdEmissor.Value);
                string cnpjEmissor = !String.IsNullOrEmpty(emissor.Cnpj) ? Utilitario.RemoveCaracteresEspeciais(emissor.Cnpj) : "";
                string nomeEmissor = emissor.Nome.Trim();

                d = new Demais_N_Codif();
                d.cod_Tp_Ativ = Convert.ToString((int)TipoAtivoCDA.Outros);
                d.descricao = "Valores a Pagar - Posição Vendida de " + cdAtivoBolsa;
                // Demais_N_Codif - Emis
                d.getEmis.tp_PF_PJ = "PJ";
                d.getEmis.nr_PF_PJ = cnpjEmissor;
                d.getEmis.nm = nomeEmissor;

                // Demais_N_Codif - Aplic
                d.getAplic.cod_Tp_Aplic = Convert.ToString((int)TipoAplicacaoCDA.OutrasAplicacoes);

                // Demais_Codif - Aplic
                d.getAplic.qtde_Dias_Confid = this.diasConfidencialidade.ToString();

                // Demais_N_Codif - Aplic - Negoc
                d.getAplic.getAplicNegoc.tipoNegocio = Convert.ToString((int)TipoNegociacaoCDA.Negociacao);

                d.getAplic.getAplicNegoc.quantidadeVendasNegocio = quantidadeVenda;
                d.getAplic.getAplicNegoc.valorVendasNegocio = valorVenda;
                d.getAplic.getAplicNegoc.quantidadeAquisicaoNegocio = quantidadeCompra;
                d.getAplic.getAplicNegoc.valorAquisicaoNegocio = valorCompra;

                // Demais_N_Codif - Aplic - Pos_FIM
                d.getAplic.getAplicPosFim.qtde_Pos_Fim = quantidadeFinal;
                d.getAplic.getAplicPosFim.custo_Correc_Pos_Fim = custoCorrecPosFim;
                d.getAplic.getAplicPosFim.merc_Pos_Fim = valorFinal;

                // Adiciona Demais_N_Codif
                arq.getCorpo.getInform.getListAtiv.getDemais_N_Codif.Add(d);
            }
            #endregion

            #region Acoes sem data de vigência definida
            AtivoBolsaQuery ativoBolsaQuery = new AtivoBolsaQuery("A");
            PosicaoBolsaHistoricoQuery posicaoBolsaHistoricoQuery = new PosicaoBolsaHistoricoQuery("P");

            posicaoBolsaHistoricoCollection = new PosicaoBolsaHistoricoCollection();
            posicaoBolsaHistoricoQuery.Select(posicaoBolsaHistoricoQuery.CdAtivoBolsa,
                                             posicaoBolsaHistoricoQuery.Quantidade.Sum(),
                                             posicaoBolsaHistoricoQuery.ValorCustoLiquido.Sum(),
                                             posicaoBolsaHistoricoQuery.ValorMercado.Sum());
            posicaoBolsaHistoricoQuery.InnerJoin(ativoBolsaQuery).On(ativoBolsaQuery.CdAtivoBolsa == posicaoBolsaHistoricoQuery.CdAtivoBolsa);
            posicaoBolsaHistoricoQuery.Where(posicaoBolsaHistoricoQuery.IdCliente.Equal(idCarteira) &
                                            posicaoBolsaHistoricoQuery.DataHistorico.Equal(dataPosicao) &
                                            posicaoBolsaHistoricoQuery.TipoMercado.Equal(TipoMercadoBolsa.MercadoVista) &
                                            (posicaoBolsaHistoricoQuery.Quantidade.GreaterThan(0) &
                                            ativoBolsaQuery.DataInicioVigencia.IsNull() | ativoBolsaQuery.DataInicioVigencia.Equal(dataIndefinida)));
            posicaoBolsaHistoricoQuery.GroupBy(posicaoBolsaHistoricoQuery.CdAtivoBolsa);
            posicaoBolsaHistoricoQuery.OrderBy(posicaoBolsaHistoricoQuery.CdAtivoBolsa.Ascending);
            posicaoBolsaHistoricoCollection.Load(posicaoBolsaHistoricoQuery);

            foreach (PosicaoBolsaHistorico posicaoBolsaHistorico in posicaoBolsaHistoricoCollection)
            {
                string cdAtivoBolsa = posicaoBolsaHistorico.CdAtivoBolsa;

                AtivoBolsa ativoBolsa = new AtivoBolsa();
                ativoBolsa.LoadByPrimaryKey(cdAtivoBolsa);

                decimal quantidadeFinal = posicaoBolsaHistorico.Quantidade.HasValue ? posicaoBolsaHistorico.Quantidade.Value : 0;
                decimal valorFinal = posicaoBolsaHistorico.ValorMercado.HasValue ? posicaoBolsaHistorico.ValorMercado.Value : 0;
                decimal custoCorrecPosFim = posicaoBolsaHistorico.ValorCustoLiquido.HasValue ? posicaoBolsaHistorico.ValorCustoLiquido.Value : 0;


                OperacaoBolsa operacaoBolsaCompra = new OperacaoBolsa();
                operacaoBolsaCompra.Query.Select(operacaoBolsaCompra.Query.Quantidade.Sum(),
                                                 operacaoBolsaCompra.Query.Valor.Sum());
                operacaoBolsaCompra.Query.Where(operacaoBolsaCompra.Query.IdCliente.Equal(idCarteira),
                                                operacaoBolsaCompra.Query.CdAtivoBolsa.Equal(cdAtivoBolsa),
                                                operacaoBolsaCompra.Query.Data.GreaterThanOrEqual(dataInicioMes),
                                                operacaoBolsaCompra.Query.Data.LessThanOrEqual(dataPosicao),
                                                operacaoBolsaCompra.Query.TipoOperacao.In(TipoOperacaoBolsa.Compra, TipoOperacaoBolsa.CompraDaytrade));
                operacaoBolsaCompra.Query.Load();

                decimal quantidadeCompra = operacaoBolsaCompra.Quantidade.HasValue ? operacaoBolsaCompra.Quantidade.Value : 0;
                decimal valorCompra = operacaoBolsaCompra.Valor.HasValue ? operacaoBolsaCompra.Valor.Value : 0;

                OperacaoBolsa operacaoBolsaVenda = new OperacaoBolsa();
                operacaoBolsaVenda.Query.Select(operacaoBolsaVenda.Query.Quantidade.Sum(),
                                                operacaoBolsaVenda.Query.Valor.Sum());
                operacaoBolsaVenda.Query.Where(operacaoBolsaVenda.Query.IdCliente.Equal(idCarteira),
                                               operacaoBolsaVenda.Query.CdAtivoBolsa.Equal(cdAtivoBolsa),
                                               operacaoBolsaVenda.Query.Data.GreaterThanOrEqual(dataInicioMes),
                                               operacaoBolsaVenda.Query.Data.LessThanOrEqual(dataPosicao),
                                               operacaoBolsaVenda.Query.TipoOperacao.In(TipoOperacaoBolsa.Venda, TipoOperacaoBolsa.VendaDaytrade));
                operacaoBolsaVenda.Query.Load();

                decimal quantidadeVenda = operacaoBolsaVenda.Quantidade.HasValue ? operacaoBolsaVenda.Quantidade.Value : 0;
                decimal valorVenda = operacaoBolsaVenda.Valor.HasValue ? operacaoBolsaVenda.Valor.Value : 0;

                Emissor emissor = new Emissor();
                emissor.LoadByPrimaryKey(ativoBolsa.IdEmissor.Value);
                string cnpjEmissor = !String.IsNullOrEmpty(emissor.Cnpj) ? Utilitario.RemoveCaracteresEspeciais(emissor.Cnpj) : "";
                string nomeEmissor = emissor.Nome.Trim();

                string codigoAtivo = "";
                string tipoAplicacao = "";
                #region Checa codigo de ativo
                if (ativoBolsa.TipoPapel.Value == (byte)TipoPapelAtivo.Normal ||
                    ativoBolsa.TipoPapel.Value == (byte)TipoPapelAtivo.BDRNivelI ||
                    ativoBolsa.TipoPapel.Value == (byte)TipoPapelAtivo.BDRNivelII ||
                    ativoBolsa.TipoPapel.Value == (byte)TipoPapelAtivo.BDRNivelIII)
                {
                    codigoAtivo = Utilitario.Right(cdAtivoBolsa, 1) == "3" ? Convert.ToString((int)TipoAtivoCDA.AcaoOrdinaria) : Convert.ToString((int)TipoAtivoCDA.AcaoPreferencial);
                    tipoAplicacao = Convert.ToString((int)TipoAplicacaoCDA.Acoes);
                }
                else if (ativoBolsa.TipoPapel.Value == (byte)TipoPapelAtivo.BonusSubscricao)
                {
                    codigoAtivo = Convert.ToString((int)TipoAtivoCDA.BonusSubscricao);
                    tipoAplicacao = Convert.ToString((int)TipoAplicacaoCDA.OutrosValoresMobiliarios_OfertaPublica);
                }
                else if (ativoBolsa.TipoPapel.Value == (byte)TipoPapelAtivo.CertificadoDepositoAcoes)
                {
                    codigoAtivo = Convert.ToString((int)TipoAtivoCDA.CertificadoDepositoAcoes);
                    tipoAplicacao = Convert.ToString((int)TipoAplicacaoCDA.Certificado_ReciboDeposito);
                }
                else if (ativoBolsa.TipoPapel.Value == (byte)TipoPapelAtivo.ETF)
                {
                    codigoAtivo = Convert.ToString((int)TipoAtivoCDA.AcaoPreferencial); //Fundos de indice são jogados, por ora, em Ação
                    tipoAplicacao = Convert.ToString((int)TipoAplicacaoCDA.Acoes);
                }
                else if (ativoBolsa.TipoPapel.Value == (byte)TipoPapelAtivo.ReciboDepositoAcoes)
                {
                    codigoAtivo = Convert.ToString((int)TipoAtivoCDA.ReciboDepositoAcoes);
                    tipoAplicacao = Convert.ToString((int)TipoAplicacaoCDA.Certificado_ReciboDeposito);
                }
                else if (ativoBolsa.TipoPapel.Value == (byte)TipoPapelAtivo.ReciboSubscricao)
                {
                    codigoAtivo = Convert.ToString((int)TipoAtivoCDA.ReciboSubscricao);
                    tipoAplicacao = Convert.ToString((int)TipoAplicacaoCDA.OutrosValoresMobiliarios_OfertaPublica);
                }
                #endregion

                d = new Demais_N_Codif();
                d.cod_Tp_Ativ = codigoAtivo;
                d.descricao = ativoBolsa.Descricao;
                // Demais_N_Codif - Emis
                d.getEmis.tp_PF_PJ = "PJ";
                d.getEmis.nr_PF_PJ = cnpjEmissor;
                d.getEmis.nm = nomeEmissor;

                // Demais_N_Codif - Aplic
                d.getAplic.cod_Tp_Aplic = tipoAplicacao;

                d.getAplic.qtde_Dias_Confid = this.diasConfidencialidade.ToString();

                // Demais_N_Codif - Aplic - Negoc
                d.getAplic.getAplicNegoc.tipoNegocio = Convert.ToString((int)TipoNegociacaoCDA.Negociacao);

                d.getAplic.getAplicNegoc.quantidadeVendasNegocio = quantidadeVenda;
                d.getAplic.getAplicNegoc.valorVendasNegocio = valorVenda;
                d.getAplic.getAplicNegoc.quantidadeAquisicaoNegocio = quantidadeCompra;
                d.getAplic.getAplicNegoc.valorAquisicaoNegocio = valorCompra;

                // Demais_N_Codif - Aplic - Pos_FIM
                d.getAplic.getAplicPosFim.qtde_Pos_Fim = quantidadeFinal;
                d.getAplic.getAplicPosFim.custo_Correc_Pos_Fim = custoCorrecPosFim;
                d.getAplic.getAplicPosFim.merc_Pos_Fim = valorFinal;

                // Adiciona Demais_N_Codif
                arq.getCorpo.getInform.getListAtiv.getDemais_N_Codif.Add(d);
            }
            #endregion

            #region Opcoes compradas sem data de vigência definida
            ativoBolsaQuery = new AtivoBolsaQuery("A");
            posicaoBolsaHistoricoQuery = new PosicaoBolsaHistoricoQuery("P");

            posicaoBolsaHistoricoCollection = new PosicaoBolsaHistoricoCollection();
            posicaoBolsaHistoricoQuery.Select(posicaoBolsaHistoricoQuery.TipoMercado,
                                             posicaoBolsaHistoricoQuery.CdAtivoBolsa,
                                             posicaoBolsaHistoricoQuery.Quantidade.Sum(),
                                             posicaoBolsaHistoricoQuery.ValorCustoLiquido.Sum(),
                                             posicaoBolsaHistoricoQuery.ValorMercado.Sum());
            posicaoBolsaHistoricoQuery.InnerJoin(ativoBolsaQuery).On(ativoBolsaQuery.CdAtivoBolsa == posicaoBolsaHistoricoQuery.CdAtivoBolsa);
            posicaoBolsaHistoricoQuery.Where(posicaoBolsaHistoricoQuery.IdCliente.Equal(idCarteira) &
                                            posicaoBolsaHistoricoQuery.DataHistorico.Equal(dataPosicao) &
                                            posicaoBolsaHistoricoQuery.TipoMercado.In(TipoMercadoBolsa.OpcaoCompra, TipoMercadoBolsa.OpcaoVenda) &
                                            (posicaoBolsaHistoricoQuery.Quantidade.GreaterThan(0) &
                                            ativoBolsaQuery.DataInicioVigencia.IsNull() | ativoBolsaQuery.DataInicioVigencia.Equal(dataIndefinida)));
            posicaoBolsaHistoricoQuery.GroupBy(posicaoBolsaHistoricoQuery.TipoMercado,
                                               posicaoBolsaHistoricoQuery.CdAtivoBolsa);
            posicaoBolsaHistoricoQuery.OrderBy(posicaoBolsaHistoricoQuery.TipoMercado.Ascending,
                                               posicaoBolsaHistoricoQuery.CdAtivoBolsa.Ascending);
            posicaoBolsaHistoricoCollection.Load(posicaoBolsaHistoricoQuery);

            foreach (PosicaoBolsaHistorico posicaoBolsaHistorico in posicaoBolsaHistoricoCollection)
            {
                string cdAtivoBolsa = posicaoBolsaHistorico.CdAtivoBolsa;
                string tipoMercado = posicaoBolsaHistorico.TipoMercado;

                AtivoBolsa ativoBolsa = new AtivoBolsa();
                ativoBolsa.LoadByPrimaryKey(cdAtivoBolsa);

                decimal quantidadeFinal = posicaoBolsaHistorico.Quantidade.HasValue ? posicaoBolsaHistorico.Quantidade.Value : 0;
                decimal valorFinal = posicaoBolsaHistorico.ValorMercado.HasValue ? posicaoBolsaHistorico.ValorMercado.Value : 0;
                decimal custoCorrecPosFim = posicaoBolsaHistorico.ValorCustoLiquido.HasValue ? posicaoBolsaHistorico.ValorCustoLiquido.Value : 0;

                OperacaoBolsa operacaoBolsaCompra = new OperacaoBolsa();
                operacaoBolsaCompra.Query.Select(operacaoBolsaCompra.Query.Quantidade.Sum(),
                                                 operacaoBolsaCompra.Query.Valor.Sum());
                operacaoBolsaCompra.Query.Where(operacaoBolsaCompra.Query.IdCliente.Equal(idCarteira),
                                                operacaoBolsaCompra.Query.CdAtivoBolsa.Equal(cdAtivoBolsa),
                                                operacaoBolsaCompra.Query.Data.GreaterThanOrEqual(dataInicioMes),
                                                operacaoBolsaCompra.Query.Data.LessThanOrEqual(dataPosicao),
                                                operacaoBolsaCompra.Query.TipoOperacao.In(TipoOperacaoBolsa.Compra, TipoOperacaoBolsa.CompraDaytrade));
                operacaoBolsaCompra.Query.Load();

                decimal quantidadeCompra = operacaoBolsaCompra.Quantidade.HasValue ? operacaoBolsaCompra.Quantidade.Value : 0;
                decimal valorCompra = operacaoBolsaCompra.Valor.HasValue ? operacaoBolsaCompra.Valor.Value : 0;

                OperacaoBolsa operacaoBolsaVenda = new OperacaoBolsa();
                operacaoBolsaVenda.Query.Select(operacaoBolsaVenda.Query.Quantidade.Sum(),
                                                operacaoBolsaVenda.Query.Valor.Sum());
                operacaoBolsaVenda.Query.Where(operacaoBolsaVenda.Query.IdCliente.Equal(idCarteira),
                                               operacaoBolsaVenda.Query.CdAtivoBolsa.Equal(cdAtivoBolsa),
                                               operacaoBolsaVenda.Query.Data.GreaterThanOrEqual(dataInicioMes),
                                               operacaoBolsaVenda.Query.Data.LessThanOrEqual(dataPosicao),
                                               operacaoBolsaVenda.Query.TipoOperacao.In(TipoOperacaoBolsa.Venda, TipoOperacaoBolsa.VendaDaytrade));
                operacaoBolsaVenda.Query.Load();

                decimal quantidadeVenda = operacaoBolsaVenda.Quantidade.HasValue ? operacaoBolsaVenda.Quantidade.Value : 0;
                decimal valorVenda = operacaoBolsaVenda.Valor.HasValue ? operacaoBolsaVenda.Valor.Value : 0;

                Emissor emissor = new Emissor();
                emissor.LoadByPrimaryKey(ativoBolsa.IdEmissor.Value);
                string cnpjEmissor = !String.IsNullOrEmpty(emissor.Cnpj) ? Utilitario.RemoveCaracteresEspeciais(emissor.Cnpj) : "";
                string nomeEmissor = emissor.Nome.Trim();

                d = new Demais_N_Codif();

                if (tipoMercado == TipoMercadoBolsa.OpcaoCompra)
                {
                    d.cod_Tp_Ativ = Convert.ToString((int)TipoAtivoCDA.OpcaoCompra);
                }
                else
                {
                    d.cod_Tp_Ativ = Convert.ToString((int)TipoAtivoCDA.OpcaoVenda);
                }

                d.descricao = ativoBolsa.Descricao;
                // Demais_N_Codif - Emis
                d.getEmis.tp_PF_PJ = "PJ";
                d.getEmis.nr_PF_PJ = cnpjEmissor;
                d.getEmis.nm = nomeEmissor;

                // Demais_N_Codif - Aplic
                d.getAplic.cod_Tp_Aplic = Convert.ToString((int)TipoAplicacaoCDA.OpcoesPosicaoTitular);
                d.getAplic.qtde_Dias_Confid = this.diasConfidencialidade.ToString();

                // Demais_N_Codif - Aplic - Negoc
                d.getAplic.getAplicNegoc.tipoNegocio = Convert.ToString((int)TipoNegociacaoCDA.Negociacao);

                d.getAplic.getAplicNegoc.quantidadeVendasNegocio = quantidadeVenda;
                d.getAplic.getAplicNegoc.valorVendasNegocio = valorVenda;
                d.getAplic.getAplicNegoc.quantidadeAquisicaoNegocio = quantidadeCompra;
                d.getAplic.getAplicNegoc.valorAquisicaoNegocio = valorCompra;

                // Demais_N_Codif - Aplic - Pos_FIM
                d.getAplic.getAplicPosFim.qtde_Pos_Fim = quantidadeFinal;
                d.getAplic.getAplicPosFim.custo_Correc_Pos_Fim = custoCorrecPosFim;
                d.getAplic.getAplicPosFim.merc_Pos_Fim = valorFinal;

                // Adiciona Demais_N_Codif
                arq.getCorpo.getInform.getListAtiv.getDemais_N_Codif.Add(d);
            }
            #endregion

            #region Opcoes vendidas sem data de vigência definida
            ativoBolsaQuery = new AtivoBolsaQuery("A");
            posicaoBolsaHistoricoQuery = new PosicaoBolsaHistoricoQuery("P");

            posicaoBolsaHistoricoCollection = new PosicaoBolsaHistoricoCollection();
            posicaoBolsaHistoricoQuery.Select(posicaoBolsaHistoricoQuery.TipoMercado,
                                             posicaoBolsaHistoricoQuery.CdAtivoBolsa,
                                             posicaoBolsaHistoricoQuery.Quantidade.Sum(),
                                             posicaoBolsaHistoricoQuery.ValorCustoLiquido.Sum(),
                                             posicaoBolsaHistoricoQuery.ValorMercado.Sum());
            posicaoBolsaHistoricoQuery.InnerJoin(ativoBolsaQuery).On(ativoBolsaQuery.CdAtivoBolsa == posicaoBolsaHistoricoQuery.CdAtivoBolsa);
            posicaoBolsaHistoricoQuery.Where(posicaoBolsaHistoricoQuery.IdCliente.Equal(idCarteira) &
                                            posicaoBolsaHistoricoQuery.DataHistorico.Equal(dataPosicao) &
                                            posicaoBolsaHistoricoQuery.TipoMercado.In(TipoMercadoBolsa.OpcaoCompra, TipoMercadoBolsa.OpcaoVenda) &
                                            (posicaoBolsaHistoricoQuery.Quantidade.LessThan(0) &
                                            ativoBolsaQuery.DataInicioVigencia.IsNull() | ativoBolsaQuery.DataInicioVigencia.Equal(dataIndefinida)));
            posicaoBolsaHistoricoQuery.GroupBy(posicaoBolsaHistoricoQuery.TipoMercado,
                                               posicaoBolsaHistoricoQuery.CdAtivoBolsa);
            posicaoBolsaHistoricoQuery.OrderBy(posicaoBolsaHistoricoQuery.TipoMercado.Ascending,
                                               posicaoBolsaHistoricoQuery.CdAtivoBolsa.Ascending);
            posicaoBolsaHistoricoCollection.Load(posicaoBolsaHistoricoQuery);

            foreach (PosicaoBolsaHistorico posicaoBolsaHistorico in posicaoBolsaHistoricoCollection)
            {
                string cdAtivoBolsa = posicaoBolsaHistorico.CdAtivoBolsa;
                string tipoMercado = posicaoBolsaHistorico.TipoMercado;

                AtivoBolsa ativoBolsa = new AtivoBolsa();
                ativoBolsa.LoadByPrimaryKey(cdAtivoBolsa);

                decimal quantidadeFinal = posicaoBolsaHistorico.Quantidade.HasValue ? posicaoBolsaHistorico.Quantidade.Value : 0;
                decimal valorFinal = posicaoBolsaHistorico.ValorMercado.HasValue ? posicaoBolsaHistorico.ValorMercado.Value : 0;
                decimal custoCorrecPosFim = posicaoBolsaHistorico.ValorCustoLiquido.HasValue ? posicaoBolsaHistorico.ValorCustoLiquido.Value : 0;

                OperacaoBolsa operacaoBolsaCompra = new OperacaoBolsa();
                operacaoBolsaCompra.Query.Select(operacaoBolsaCompra.Query.Quantidade.Sum(),
                                                 operacaoBolsaCompra.Query.Valor.Sum());
                operacaoBolsaCompra.Query.Where(operacaoBolsaCompra.Query.IdCliente.Equal(idCarteira),
                                                operacaoBolsaCompra.Query.CdAtivoBolsa.Equal(cdAtivoBolsa),
                                                operacaoBolsaCompra.Query.Data.GreaterThanOrEqual(dataInicioMes),
                                                operacaoBolsaCompra.Query.Data.LessThanOrEqual(dataPosicao),
                                                operacaoBolsaCompra.Query.TipoOperacao.In(TipoOperacaoBolsa.Compra, TipoOperacaoBolsa.CompraDaytrade));
                operacaoBolsaCompra.Query.Load();

                decimal quantidadeCompra = operacaoBolsaCompra.Quantidade.HasValue ? operacaoBolsaCompra.Quantidade.Value : 0;
                decimal valorCompra = operacaoBolsaCompra.Valor.HasValue ? operacaoBolsaCompra.Valor.Value : 0;

                OperacaoBolsa operacaoBolsaVenda = new OperacaoBolsa();
                operacaoBolsaVenda.Query.Select(operacaoBolsaVenda.Query.Quantidade.Sum(),
                                                operacaoBolsaVenda.Query.Valor.Sum());
                operacaoBolsaVenda.Query.Where(operacaoBolsaVenda.Query.IdCliente.Equal(idCarteira),
                                               operacaoBolsaVenda.Query.CdAtivoBolsa.Equal(cdAtivoBolsa),
                                               operacaoBolsaVenda.Query.Data.GreaterThanOrEqual(dataInicioMes),
                                               operacaoBolsaVenda.Query.Data.LessThanOrEqual(dataPosicao),
                                               operacaoBolsaVenda.Query.TipoOperacao.In(TipoOperacaoBolsa.Venda, TipoOperacaoBolsa.VendaDaytrade));
                operacaoBolsaVenda.Query.Load();

                decimal quantidadeVenda = operacaoBolsaVenda.Quantidade.HasValue ? operacaoBolsaVenda.Quantidade.Value : 0;
                decimal valorVenda = operacaoBolsaVenda.Valor.HasValue ? operacaoBolsaVenda.Valor.Value : 0;

                Emissor emissor = new Emissor();
                emissor.LoadByPrimaryKey(ativoBolsa.IdEmissor.Value);
                string cnpjEmissor = !String.IsNullOrEmpty(emissor.Cnpj) ? Utilitario.RemoveCaracteresEspeciais(emissor.Cnpj) : "";
                string nomeEmissor = emissor.Nome.Trim();

                d = new Demais_N_Codif();

                if (tipoMercado == TipoMercadoBolsa.OpcaoCompra)
                {
                    d.cod_Tp_Ativ = Convert.ToString((int)TipoAtivoCDA.OpcaoCompra);
                }
                else
                {
                    d.cod_Tp_Ativ = Convert.ToString((int)TipoAtivoCDA.OpcaoVenda);
                }

                d.descricao = ativoBolsa.Descricao;
                // Demais_N_Codif - Emis
                d.getEmis.tp_PF_PJ = "PJ";
                d.getEmis.nr_PF_PJ = cnpjEmissor;
                d.getEmis.nm = nomeEmissor;

                // Demais_N_Codif - Aplic
                d.getAplic.cod_Tp_Aplic = Convert.ToString((int)TipoAplicacaoCDA.OpcoesPosicaoLancada);
                d.getAplic.qtde_Dias_Confid = this.diasConfidencialidade.ToString();

                // Demais_N_Codif - Aplic - Negoc
                d.getAplic.getAplicNegoc.tipoNegocio = Convert.ToString((int)TipoNegociacaoCDA.Negociacao);

                d.getAplic.getAplicNegoc.quantidadeVendasNegocio = quantidadeVenda;
                d.getAplic.getAplicNegoc.valorVendasNegocio = valorVenda;
                d.getAplic.getAplicNegoc.quantidadeAquisicaoNegocio = quantidadeCompra;
                d.getAplic.getAplicNegoc.valorAquisicaoNegocio = valorCompra;

                // Demais_N_Codif - Aplic - Pos_FIM
                d.getAplic.getAplicPosFim.qtde_Pos_Fim = quantidadeFinal;
                d.getAplic.getAplicPosFim.custo_Correc_Pos_Fim = custoCorrecPosFim;
                d.getAplic.getAplicPosFim.merc_Pos_Fim = valorFinal;

                // Adiciona Demais_N_Codif
                arq.getCorpo.getInform.getListAtiv.getDemais_N_Codif.Add(d);
            }
            #endregion

            #region Futuros/Opções Compradas sem data de vigência definida
            PosicaoBMFHistoricoQuery posicaoBMFHistoricoQuery = new PosicaoBMFHistoricoQuery("P");
            AtivoBMFQuery ativoBMFQuery = new AtivoBMFQuery("A");

            PosicaoBMFHistoricoCollection posicaoBMFHistoricoCollection = new PosicaoBMFHistoricoCollection();
            posicaoBMFHistoricoQuery.Select(posicaoBMFHistoricoQuery.TipoMercado,
                                           posicaoBMFHistoricoQuery.CdAtivoBMF,
                                           posicaoBMFHistoricoQuery.Serie,
                                           posicaoBMFHistoricoQuery.Quantidade.Sum(),
                                           posicaoBMFHistoricoQuery.ValorMercado.Sum());
            posicaoBMFHistoricoQuery.InnerJoin(ativoBMFQuery).On(ativoBMFQuery.CdAtivoBMF == posicaoBMFHistoricoQuery.CdAtivoBMF &
                                                                 ativoBMFQuery.Serie == posicaoBMFHistoricoQuery.Serie);
            posicaoBMFHistoricoQuery.Where(posicaoBMFHistoricoQuery.IdCliente.Equal(idCarteira) &
                                          posicaoBMFHistoricoQuery.DataHistorico.Equal(dataPosicao) &
                                          posicaoBMFHistoricoQuery.Quantidade.GreaterThan(0) &
                                          (ativoBMFQuery.DataFimVigencia.IsNull() | ativoBMFQuery.DataFimVigencia.Equal(dataIndefinida)));
            posicaoBMFHistoricoQuery.GroupBy(posicaoBMFHistoricoQuery.TipoMercado,
                                            posicaoBMFHistoricoQuery.CdAtivoBMF,
                                            posicaoBMFHistoricoQuery.Serie);
            posicaoBMFHistoricoQuery.OrderBy(posicaoBMFHistoricoQuery.TipoMercado.Ascending,
                                            posicaoBMFHistoricoQuery.CdAtivoBMF.Ascending);
            posicaoBMFHistoricoCollection.Load(posicaoBMFHistoricoQuery);

            foreach (PosicaoBMFHistorico posicaoBMFHistorico in posicaoBMFHistoricoCollection)
            {
                string cdAtivoBMF = posicaoBMFHistorico.CdAtivoBMF;
                string serie = posicaoBMFHistorico.Serie;
                byte tipoMercado = posicaoBMFHistorico.TipoMercado.Value;

                decimal quantidadeFinal = posicaoBMFHistorico.Quantidade.HasValue ? posicaoBMFHistorico.Quantidade.Value : 0;
                decimal valorFinal = (posicaoBMFHistorico.TipoMercado.Value != (byte)TipoMercadoBMF.Futuro && posicaoBMFHistorico.ValorMercado.HasValue) ? posicaoBMFHistorico.ValorMercado.Value : 0;

                AtivoBMF ativoBMF = new AtivoBMF();
                ativoBMF.LoadByPrimaryKey(cdAtivoBMF, serie);

                d.cod_Tp_Ativ = Convert.ToString(TipoAtivoCDA.ContratoFuturo);

                d.descricao = this.RetornaAtivoFuturoCDA(ativoBMF);
                // Demais_N_Codif - Emis
                d.getEmis.tp_PF_PJ = String.Empty;
                d.getEmis.nr_PF_PJ = String.Empty;
                d.getEmis.nm = String.Empty;

                // Demais_N_Codif - Aplic
                d.getAplic.cod_Tp_Aplic = Convert.ToString((int)TipoAplicacaoCDA.MercadoFuturoComprado);
                d.getAplic.qtde_Dias_Confid = this.diasConfidencialidade.ToString();

                // Demais_N_Codif - Aplic - Negoc
                d.getAplic.getAplicNegoc.tipoNegocio = Convert.ToString((int)TipoNegociacaoCDA.Negociacao);

                // Demais_N_Codif - Aplic - Pos_FIM
                d.getAplic.getAplicPosFim.qtde_Pos_Fim = quantidadeFinal;
                d.getAplic.getAplicPosFim.merc_Pos_Fim = valorFinal;

                // Adiciona Demais_N_Codif
                arq.getCorpo.getInform.getListAtiv.getDemais_N_Codif.Add(d);
            }
            #endregion

            #region Futuros/Opções Vendidos sem data de vigência definida
            posicaoBMFHistoricoQuery = new PosicaoBMFHistoricoQuery("P");
            ativoBMFQuery = new AtivoBMFQuery("A");

            posicaoBMFHistoricoCollection = new PosicaoBMFHistoricoCollection();
            posicaoBMFHistoricoQuery.Select(posicaoBMFHistoricoQuery.TipoMercado,
                                           posicaoBMFHistoricoQuery.CdAtivoBMF,
                                           posicaoBMFHistoricoQuery.Serie,
                                           posicaoBMFHistoricoQuery.Quantidade.Sum(),
                                           posicaoBMFHistoricoQuery.ValorMercado.Sum());
            posicaoBMFHistoricoQuery.InnerJoin(ativoBMFQuery).On(ativoBMFQuery.CdAtivoBMF == posicaoBMFHistoricoQuery.CdAtivoBMF &
                                                                 ativoBMFQuery.Serie == posicaoBMFHistoricoQuery.Serie);
            posicaoBMFHistoricoQuery.Where(posicaoBMFHistoricoQuery.IdCliente.Equal(idCarteira) &
                                          posicaoBMFHistoricoQuery.DataHistorico.Equal(dataPosicao) &
                                          posicaoBMFHistoricoQuery.Quantidade.LessThan(0) &
                                          (ativoBMFQuery.DataFimVigencia.IsNull() | ativoBMFQuery.DataFimVigencia.Equal(dataIndefinida)));
            posicaoBMFHistoricoQuery.GroupBy(posicaoBMFHistoricoQuery.TipoMercado,
                                            posicaoBMFHistoricoQuery.CdAtivoBMF,
                                            posicaoBMFHistoricoQuery.Serie);
            posicaoBMFHistoricoQuery.OrderBy(posicaoBMFHistoricoQuery.TipoMercado.Ascending,
                                            posicaoBMFHistoricoQuery.CdAtivoBMF.Ascending);
            posicaoBMFHistoricoCollection.Load(posicaoBMFHistoricoQuery);

            foreach (PosicaoBMFHistorico posicaoBMFHistorico in posicaoBMFHistoricoCollection)
            {
                string cdAtivoBMF = posicaoBMFHistorico.CdAtivoBMF;
                string serie = posicaoBMFHistorico.Serie;
                byte tipoMercado = posicaoBMFHistorico.TipoMercado.Value;

                decimal quantidadeFinal = posicaoBMFHistorico.Quantidade.HasValue ? posicaoBMFHistorico.Quantidade.Value : 0;
                decimal valorFinal = (posicaoBMFHistorico.TipoMercado.Value != (byte)TipoMercadoBMF.Futuro && posicaoBMFHistorico.ValorMercado.HasValue) ? posicaoBMFHistorico.ValorMercado.Value : 0;

                AtivoBMF ativoBMF = new AtivoBMF();
                ativoBMF.LoadByPrimaryKey(cdAtivoBMF, serie);

                d.cod_Tp_Ativ = Convert.ToString((int)TipoAtivoCDA.ContratoFuturo);

                d.descricao = this.RetornaAtivoFuturoCDA(ativoBMF);
                // Demais_N_Codif - Emis
                d.getEmis.tp_PF_PJ = String.Empty;
                d.getEmis.nr_PF_PJ = String.Empty;
                d.getEmis.nm = String.Empty;

                // Demais_N_Codif - Aplic
                d.getAplic.cod_Tp_Aplic = Convert.ToString((int)TipoAplicacaoCDA.MercadoFuturoVendido);
                d.getAplic.qtde_Dias_Confid = this.diasConfidencialidade.ToString();

                // Demais_N_Codif - Aplic - Negoc
                d.getAplic.getAplicNegoc.tipoNegocio = Convert.ToString((int)TipoNegociacaoCDA.Negociacao);

                // Demais_N_Codif - Aplic - Pos_FIM
                d.getAplic.getAplicPosFim.qtde_Pos_Fim = quantidadeFinal;
                d.getAplic.getAplicPosFim.merc_Pos_Fim = valorFinal;

                // Adiciona Demais_N_Codif
                arq.getCorpo.getInform.getListAtiv.getDemais_N_Codif.Add(d);
            }
            #endregion

            #region For Monta Titulos publicos (Compras Finais)
            PosicaoRendaFixaHistoricoQuery posicaoRendaFixaHistoricoQuery = new PosicaoRendaFixaHistoricoQuery("P");
            TituloRendaFixaQuery tituloRendaFixaQuery = new TituloRendaFixaQuery("T");
            PapelRendaFixaQuery papelRendaFixaQuery = new PapelRendaFixaQuery("A");
            posicaoRendaFixaHistoricoQuery.Select(tituloRendaFixaQuery.Descricao,
                                                  tituloRendaFixaQuery.DataEmissao,
                                                  tituloRendaFixaQuery.DataVencimento,
                                                  posicaoRendaFixaHistoricoQuery.IdTitulo,
                                                  posicaoRendaFixaHistoricoQuery.TipoNegociacao,
                                                  posicaoRendaFixaHistoricoQuery.Quantidade.Sum(),
                                                  posicaoRendaFixaHistoricoQuery.ValorMercado.Sum());
            posicaoRendaFixaHistoricoQuery.InnerJoin(tituloRendaFixaQuery).On(tituloRendaFixaQuery.IdTitulo == posicaoRendaFixaHistoricoQuery.IdTitulo);
            posicaoRendaFixaHistoricoQuery.InnerJoin(papelRendaFixaQuery).On(papelRendaFixaQuery.IdPapel == tituloRendaFixaQuery.IdPapel);
            posicaoRendaFixaHistoricoQuery.Where(posicaoRendaFixaHistoricoQuery.IdCliente.Equal(idCarteira) &
                                        posicaoRendaFixaHistoricoQuery.DataHistorico.Equal(dataPosicao) &
                                        posicaoRendaFixaHistoricoQuery.TipoOperacao.Equal((byte)TipoOperacaoTitulo.CompraFinal) &
                                        papelRendaFixaQuery.TipoPapel.Equal((byte)TipoPapelTitulo.Publico) &
                                        (tituloRendaFixaQuery.CodigoCustodia.IsNull() | tituloRendaFixaQuery.CodigoCustodia.Equal("")));
            posicaoRendaFixaHistoricoQuery.GroupBy(tituloRendaFixaQuery.Descricao,
                                                   tituloRendaFixaQuery.DataEmissao,
                                                   tituloRendaFixaQuery.DataVencimento,
                                                   posicaoRendaFixaHistoricoQuery.IdTitulo,
                                                   posicaoRendaFixaHistoricoQuery.TipoNegociacao);

            PosicaoRendaFixaHistoricoCollection coll = new PosicaoRendaFixaHistoricoCollection();
            coll.Load(posicaoRendaFixaHistoricoQuery);

            foreach (PosicaoRendaFixaHistorico p in coll)
            {
                string descricaoTitulo = Convert.ToString(p.GetColumn(TituloRendaFixaMetadata.ColumnNames.Descricao));
                DateTime dataEmissao = Convert.ToDateTime(p.GetColumn(TituloRendaFixaMetadata.ColumnNames.DataEmissao));
                DateTime dataVencimento = Convert.ToDateTime(p.GetColumn(TituloRendaFixaMetadata.ColumnNames.DataVencimento));

                byte tipoNegociacao = p.TipoNegociacao.Value;

                decimal quantidade = p.Quantidade.Value;
                decimal valor = p.ValorMercado.Value;

                decimal quantidadeAplicacao = 0;
                decimal valorAplicacao = 0;

                decimal quantidadeResgate = 0;
                decimal valorResgate = 0;

                if (tipoNegociacao != (byte)TipoNegociacaoTitulo.Vencimento) //Vencimento desconsidera de forma sumária qq movimentação, já que por definição não pode operar
                {
                    OperacaoRendaFixa operacaoRendaFixa = new OperacaoRendaFixa();
                    operacaoRendaFixa.Query.Select(operacaoRendaFixa.Query.Quantidade.Sum(),
                                                   operacaoRendaFixa.Query.Valor.Sum());
                    operacaoRendaFixa.Query.Where(operacaoRendaFixa.Query.IdCliente.Equal(idCarteira),
                                                  operacaoRendaFixa.Query.IdTitulo.Equal(p.IdTitulo.Value),
                                                  operacaoRendaFixa.Query.DataOperacao.GreaterThanOrEqual(dataInicioMes),
                                                  operacaoRendaFixa.Query.DataOperacao.LessThanOrEqual(dataPosicao),
                                                  operacaoRendaFixa.Query.TipoOperacao.In((byte)TipoOperacaoTitulo.CompraRevenda));
                    operacaoRendaFixa.Query.Load();

                    if (operacaoRendaFixa.Quantidade.HasValue)
                    {
                        quantidadeAplicacao = operacaoRendaFixa.Quantidade.Value;
                        valorAplicacao = operacaoRendaFixa.Valor.Value;
                    }


                    operacaoRendaFixa = new OperacaoRendaFixa();
                    operacaoRendaFixa.Query.Select(operacaoRendaFixa.Query.Quantidade.Sum(),
                                                   operacaoRendaFixa.Query.Valor.Sum());
                    operacaoRendaFixa.Query.Where(operacaoRendaFixa.Query.IdCliente.Equal(idCarteira),
                                                  operacaoRendaFixa.Query.IdTitulo.Equal(p.IdTitulo.Value),
                                                  operacaoRendaFixa.Query.DataOperacao.GreaterThanOrEqual(dataInicioMes),
                                                  operacaoRendaFixa.Query.DataOperacao.LessThanOrEqual(dataPosicao),
                                                  operacaoRendaFixa.Query.TipoOperacao.In((byte)TipoOperacaoTitulo.VendaFinal,
                                                                                          (byte)TipoOperacaoTitulo.VendaTotal));
                    operacaoRendaFixa.Query.Load();

                    if (operacaoRendaFixa.Quantidade.HasValue)
                    {
                        quantidadeResgate = operacaoRendaFixa.Quantidade.Value;
                        valorResgate = operacaoRendaFixa.Valor.Value;
                    }
                }

                d = new Demais_N_Codif();
                string cnpjTesouro = "55730881000198"; //CNPJ forçado do Tesouro Nacional
                d.cod_Tp_Ativ = Convert.ToString((int)TipoAtivoCDA.TituloPublico);
                string dataEmissaoString = dataEmissao.Day.ToString().PadLeft(2, '0') + dataEmissao.Month.ToString().PadLeft(2, '0') + Utilitario.Right(dataEmissao.Year.ToString(), 2);
                string dataVencimentoString = dataVencimento.Day.ToString().PadLeft(2, '0') + dataVencimento.Month.ToString().PadLeft(2, '0') + Utilitario.Right(dataVencimento.Year.ToString(), 2);
                d.descricao = descricaoTitulo + "/STNC/" + dataEmissaoString + "/" + dataVencimentoString + "/" + cnpjTesouro;
                // Demais_N_Codif - Emis
                d.getEmis.tp_PF_PJ = "PJ";
                d.getEmis.nr_PF_PJ = cnpjTesouro;
                d.getEmis.nm = "Tesouro Nacional";

                // Demais_N_Codif - Aplic
                d.getAplic.cod_Tp_Aplic = Convert.ToString((int)TipoAplicacaoCDA.TitulosPublicos);
                d.getAplic.qtde_Dias_Confid = this.diasConfidencialidade.ToString();

                // Aplic - Negoc
                d.getAplic.getAplicNegoc.tipoNegocio = p.TipoNegociacao.Value == (byte)TipoNegociacaoTitulo.Vencimento ?
                                        Convert.ToString((int)TipoNegociacaoCDA.MantidoVencimento) : Convert.ToString((int)TipoNegociacaoCDA.Negociacao);
                d.getAplic.getAplicNegoc.quantidadeVendasNegocio = quantidadeResgate;
                d.getAplic.getAplicNegoc.valorVendasNegocio = valorResgate;
                d.getAplic.getAplicNegoc.quantidadeAquisicaoNegocio = quantidadeAplicacao;
                d.getAplic.getAplicNegoc.valorAquisicaoNegocio = valorAplicacao;

                // Aplic - Pos_Fim
                d.getAplic.getAplicPosFim.qtde_Pos_Fim = quantidade;
                d.getAplic.getAplicPosFim.merc_Pos_Fim = valor;
                // Adiciona Demais_N_Codif
                arq.getCorpo.getInform.getListAtiv.getDemais_N_Codif.Add(d);
            }
            #endregion

            #region For Monta Titulos publicos (Compras Revenda)
            posicaoRendaFixaHistoricoQuery = new PosicaoRendaFixaHistoricoQuery("P");
            tituloRendaFixaQuery = new TituloRendaFixaQuery("T");
            papelRendaFixaQuery = new PapelRendaFixaQuery("A");
            posicaoRendaFixaHistoricoQuery.Select(tituloRendaFixaQuery.Descricao,
                                                  posicaoRendaFixaHistoricoQuery.DataOperacao,
                                                  posicaoRendaFixaHistoricoQuery.DataVolta,
                                                  posicaoRendaFixaHistoricoQuery.IdTitulo,
                                                  posicaoRendaFixaHistoricoQuery.Quantidade.Sum(),
                                                  posicaoRendaFixaHistoricoQuery.ValorMercado.Sum());
            posicaoRendaFixaHistoricoQuery.InnerJoin(tituloRendaFixaQuery).On(tituloRendaFixaQuery.IdTitulo == posicaoRendaFixaHistoricoQuery.IdTitulo);
            posicaoRendaFixaHistoricoQuery.InnerJoin(papelRendaFixaQuery).On(papelRendaFixaQuery.IdPapel == tituloRendaFixaQuery.IdPapel);
            posicaoRendaFixaHistoricoQuery.Where(posicaoRendaFixaHistoricoQuery.IdCliente.Equal(idCarteira) &
                                        posicaoRendaFixaHistoricoQuery.DataHistorico.Equal(dataPosicao) &
                                        posicaoRendaFixaHistoricoQuery.TipoOperacao.Equal((byte)TipoOperacaoTitulo.CompraRevenda) &
                                        papelRendaFixaQuery.TipoPapel.Equal((byte)TipoPapelTitulo.Publico) &
                                        (tituloRendaFixaQuery.CodigoCustodia.IsNull() | tituloRendaFixaQuery.CodigoCustodia.Equal("")));
            posicaoRendaFixaHistoricoQuery.GroupBy(tituloRendaFixaQuery.Descricao,
                                                   posicaoRendaFixaHistoricoQuery.DataOperacao,
                                                   posicaoRendaFixaHistoricoQuery.DataVolta,
                                                   posicaoRendaFixaHistoricoQuery.IdTitulo);

            coll = new PosicaoRendaFixaHistoricoCollection();
            coll.Load(posicaoRendaFixaHistoricoQuery);

            foreach (PosicaoRendaFixaHistorico p in coll)
            {
                string descricaoTitulo = Convert.ToString(p.GetColumn(TituloRendaFixaMetadata.ColumnNames.Descricao));
                DateTime dataOperacao = p.DataOperacao.Value;
                DateTime dataVolta = p.DataVolta.Value;

                decimal quantidade = p.Quantidade.Value;
                decimal valor = p.ValorMercado.Value;

                d = new Demais_N_Codif();
                string cnpjTesouro = "55730881000198"; //CNPJ forçado do Tesouro Nacional
                d.cod_Tp_Ativ = Convert.ToString((int)TipoAtivoCDA.TituloPublico);
                string dataOperacaoString = dataOperacao.Day.ToString().PadLeft(2, '0') + dataOperacao.Month.ToString().PadLeft(2, '0') + Utilitario.Right(dataOperacao.Year.ToString(), 2);
                string dataVoltaString = dataVolta.Day.ToString().PadLeft(2, '0') + dataVolta.Month.ToString().PadLeft(2, '0') + Utilitario.Right(dataVolta.Year.ToString(), 2);
                d.descricao = descricaoTitulo + "/STNC/" + dataOperacaoString + "/" + dataVoltaString + "/" + cnpjTesouro;
                // Demais_N_Codif - Emis
                d.getEmis.tp_PF_PJ = "PJ";
                d.getEmis.nr_PF_PJ = cnpjTesouro;
                d.getEmis.nm = "Tesouro Nacional";

                // Demais_N_Codif - Aplic
                d.getAplic.cod_Tp_Aplic = Convert.ToString((int)TipoAplicacaoCDA.OperacoesCompromissadas);
                d.getAplic.qtde_Dias_Confid = this.diasConfidencialidade.ToString();

                // Aplic - Negoc
                d.getAplic.getAplicNegoc.tipoNegocio = Convert.ToString((int)TipoNegociacaoCDA.Negociacao);

                // Aplic - Pos_Fim
                d.getAplic.getAplicPosFim.qtde_Pos_Fim = quantidade;
                d.getAplic.getAplicPosFim.merc_Pos_Fim = valor;
                // Adiciona Demais_N_Codif
                arq.getCorpo.getInform.getListAtiv.getDemais_N_Codif.Add(d);
            }
            #endregion

            #region For Monta Titulos privados
            posicaoRendaFixaHistoricoQuery = new PosicaoRendaFixaHistoricoQuery("P");
            tituloRendaFixaQuery = new TituloRendaFixaQuery("T");
            papelRendaFixaQuery = new PapelRendaFixaQuery("A");
            posicaoRendaFixaHistoricoQuery.Select(tituloRendaFixaQuery.IdEmissor,
                                                  tituloRendaFixaQuery.Descricao,
                                                  tituloRendaFixaQuery.DataEmissao,
                                                  tituloRendaFixaQuery.DataVencimento,
                                                  posicaoRendaFixaHistoricoQuery.IdTitulo,
                                                  posicaoRendaFixaHistoricoQuery.TipoNegociacao,
                                                  posicaoRendaFixaHistoricoQuery.Quantidade.Sum(),
                                                  posicaoRendaFixaHistoricoQuery.ValorMercado.Sum());
            posicaoRendaFixaHistoricoQuery.InnerJoin(tituloRendaFixaQuery).On(tituloRendaFixaQuery.IdTitulo == posicaoRendaFixaHistoricoQuery.IdTitulo);
            posicaoRendaFixaHistoricoQuery.InnerJoin(papelRendaFixaQuery).On(papelRendaFixaQuery.IdPapel == tituloRendaFixaQuery.IdPapel);
            posicaoRendaFixaHistoricoQuery.Where(posicaoRendaFixaHistoricoQuery.IdCliente.Equal(idCarteira) &
                                        posicaoRendaFixaHistoricoQuery.DataHistorico.Equal(dataPosicao) &
                                        posicaoRendaFixaHistoricoQuery.TipoOperacao.Equal((byte)TipoOperacaoTitulo.CompraFinal) &
                                        papelRendaFixaQuery.TipoPapel.Equal((byte)TipoPapelTitulo.Privado) &
                                        (tituloRendaFixaQuery.CodigoCDA.IsNull() | tituloRendaFixaQuery.CodigoCDA.Equal(0)));
            posicaoRendaFixaHistoricoQuery.GroupBy(tituloRendaFixaQuery.IdEmissor,
                                                   tituloRendaFixaQuery.Descricao,
                                                   tituloRendaFixaQuery.DataEmissao,
                                                   tituloRendaFixaQuery.DataVencimento,
                                                   posicaoRendaFixaHistoricoQuery.IdTitulo,
                                                   posicaoRendaFixaHistoricoQuery.TipoNegociacao);

            coll = new PosicaoRendaFixaHistoricoCollection();
            coll.Load(posicaoRendaFixaHistoricoQuery);

            foreach (PosicaoRendaFixaHistorico p in coll)
            {
                int idTitulo = p.IdTitulo.Value;
                string descricaoTitulo = Convert.ToString(p.GetColumn(TituloRendaFixaMetadata.ColumnNames.Descricao));
                DateTime dataEmissao = Convert.ToDateTime(p.GetColumn(TituloRendaFixaMetadata.ColumnNames.DataEmissao));
                DateTime dataVencimento = Convert.ToDateTime(p.GetColumn(TituloRendaFixaMetadata.ColumnNames.DataVencimento));
                int idEmissor = Convert.ToInt32(p.GetColumn(TituloRendaFixaMetadata.ColumnNames.IdEmissor));

                byte tipoNegociacao = p.TipoNegociacao.Value;

                decimal quantidade = p.Quantidade.Value;
                decimal valor = p.ValorMercado.Value;

                decimal quantidadeAplicacao = 0;
                decimal valorAplicacao = 0;

                decimal quantidadeResgate = 0;
                decimal valorResgate = 0;

                if (tipoNegociacao != (byte)TipoNegociacaoTitulo.Vencimento) //Vencimento desconsidera de forma sumária qq movimentação, já que por definição não pode operar
                {
                    OperacaoRendaFixa operacaoRendaFixa = new OperacaoRendaFixa();
                    operacaoRendaFixa.Query.Select(operacaoRendaFixa.Query.Quantidade.Sum(),
                                                   operacaoRendaFixa.Query.Valor.Sum());
                    operacaoRendaFixa.Query.Where(operacaoRendaFixa.Query.IdCliente.Equal(idCarteira),
                                                  operacaoRendaFixa.Query.IdTitulo.Equal(p.IdTitulo.Value),
                                                  operacaoRendaFixa.Query.DataOperacao.GreaterThanOrEqual(dataInicioMes),
                                                  operacaoRendaFixa.Query.DataOperacao.LessThanOrEqual(dataPosicao),
                                                  operacaoRendaFixa.Query.TipoOperacao.In((byte)TipoOperacaoTitulo.CompraRevenda));
                    operacaoRendaFixa.Query.Load();

                    if (operacaoRendaFixa.Quantidade.HasValue)
                    {
                        quantidadeAplicacao = operacaoRendaFixa.Quantidade.Value;
                        valorAplicacao = operacaoRendaFixa.Valor.Value;
                    }


                    operacaoRendaFixa = new OperacaoRendaFixa();
                    operacaoRendaFixa.Query.Select(operacaoRendaFixa.Query.Quantidade.Sum(),
                                                   operacaoRendaFixa.Query.Valor.Sum());
                    operacaoRendaFixa.Query.Where(operacaoRendaFixa.Query.IdCliente.Equal(idCarteira),
                                                  operacaoRendaFixa.Query.IdTitulo.Equal(p.IdTitulo.Value),
                                                  operacaoRendaFixa.Query.DataOperacao.GreaterThanOrEqual(dataInicioMes),
                                                  operacaoRendaFixa.Query.DataOperacao.LessThanOrEqual(dataPosicao),
                                                  operacaoRendaFixa.Query.TipoOperacao.In((byte)TipoOperacaoTitulo.VendaFinal,
                                                                                          (byte)TipoOperacaoTitulo.VendaTotal));
                    operacaoRendaFixa.Query.Load();

                    if (operacaoRendaFixa.Quantidade.HasValue)
                    {
                        quantidadeResgate = operacaoRendaFixa.Quantidade.Value;
                        valorResgate = operacaoRendaFixa.Valor.Value;
                    }
                }

                d = new Demais_N_Codif();

                Emissor emissor = new Emissor();
                emissor.LoadByPrimaryKey(idEmissor);

                // Demais_N_Codif - Emis
                string cnpj = !String.IsNullOrEmpty(emissor.Cnpj) ? Utilitario.RemoveCaracteresEspeciais(emissor.Cnpj) : "";
                d.getEmis.nr_PF_PJ = cnpj;
                d.getEmis.tp_PF_PJ = "PJ";
                d.getEmis.nm = emissor.Nome.Trim();

                d.cod_Tp_Ativ = Convert.ToString((int)TipoAtivoCDA.Outros);
                string dataEmissaoString = dataEmissao.Day.ToString().PadLeft(2, '0') + dataEmissao.Month.ToString().PadLeft(2, '0') + Utilitario.Right(dataEmissao.Year.ToString(), 2);
                string dataVencimentoString = dataVencimento.Day.ToString().PadLeft(2, '0') + dataVencimento.Month.ToString().PadLeft(2, '0') + Utilitario.Right(dataVencimento.Year.ToString(), 2);
                d.descricao = descricaoTitulo + "/" + emissor.Nome.Trim() + "/" + dataEmissaoString + "/" + dataVencimentoString + "/" + cnpj;

                // Demais_N_Codif - Aplic
                d.getAplic.cod_Tp_Aplic = Convert.ToString((int)TipoAplicacaoCDA.OutrasAplicacoes);

                d.getAplic.qtde_Dias_Confid = this.diasConfidencialidade.ToString();

                // Aplic - Negoc
                d.getAplic.getAplicNegoc.tipoNegocio = p.TipoNegociacao.Value == (byte)TipoNegociacaoTitulo.Vencimento ?
                                        Convert.ToString((int)TipoNegociacaoCDA.MantidoVencimento) : Convert.ToString((int)TipoNegociacaoCDA.Negociacao);
                d.getAplic.getAplicNegoc.quantidadeVendasNegocio = quantidadeResgate;
                d.getAplic.getAplicNegoc.valorVendasNegocio = valorResgate;
                d.getAplic.getAplicNegoc.quantidadeAquisicaoNegocio = quantidadeAplicacao;
                d.getAplic.getAplicNegoc.valorAquisicaoNegocio = valorAplicacao;

                // Aplic - Pos_Fim
                d.getAplic.getAplicPosFim.qtde_Pos_Fim = quantidade;
                d.getAplic.getAplicPosFim.merc_Pos_Fim = valor;
                // Adiciona Demais_N_Codif
                arq.getCorpo.getInform.getListAtiv.getDemais_N_Codif.Add(d);
            }
            #endregion
        }
        #endregion
    }
}