﻿using FileHelpers;
using System;
using Financial.Contabil;
using System.Collections.Generic;
using System.IO;
using Financial.WebConfigConfiguration;
using System.Text;
using Financial.Investidor;
using Financial.Util;

namespace Financial.Export
{

    /// <summary>
    /// Summary description for ContabLancamento
    /// </summary>
    public class ContabilAtt
    {
        const string TIPO_REGISTRO = "25";              /* Campo 1 */
        string COD_PROCESSO = "0001";                   /* Campo 2 */
        const string COD_EMPRESA = "0002";              /* Campo 3 */
        //const string COD_UNIDADE;                     /* Campo 4 */
        //const string DATA_PROCESSAMENTO;              /* Campo 5 */
        //const string SEQ_PROCESSAMENTO;               /* Campo 6 */
        //const string SEQ_MOVIMENTO;                   /* Campo 7 */
        const string COD_PLANO_CONTAS = "02";           /* Campo 8 */
        //const string COD_CONTA_CONTABIL;              /* Campo 9 */
        const string COD_PLANO_AUXILIAR = "00";         /* Campo 10 */
        const string COD_CONTA_AUXILIAR = "0000";       /* Campo 11 */
        const string COD_CONTA_CONTROLE = "0000";       /* Campo 12 */
        //const string DEBITO_CREDITO;                  /* Campo 13 */
        //const string VAL_LANCAMENTO;                  /* Campo 14 */
        const string NUM_DOCUMENTO = "";                /* Campo 15 */
        const string COD_HISTORICO = "000000";          /* Campo 16 */
        //const string COMPLEMENTO_HISTORICO;           /* Campo 17 */
        //const string DATA_CONTABILIZACAO;             /* Campo 18 */
        const string COD_CENTRO_CUSTO = "000000";       /* Campo 19 */
        const string COD_UNIDADE_CORR = "000000";       /* Campo 20 */
        const string DEBITO_CREDITO_CORR = "";          /* Campo 21 */
        const string COD_CENTRO_CUSTO_CORR = "000000";  /* Campo 22 */

        [FixedLengthRecord()]
        public class ContabLancamentoDS
        {
            [FieldFixedLength(2)]
            public int tipoRegistro;

            [FieldFixedLength(4)]
            [FieldAlign(AlignMode.Right, '0')]
            public int codigoProcesso;

            [FieldFixedLength(4)]
            [FieldAlign(AlignMode.Right, '0')]
            public int codigoEmpresa;

            [FieldFixedLength(6)]
            [FieldAlign(AlignMode.Right, '0')]
            public int codigoUnidade;

            [FieldFixedLength(8)]
            [FieldConverter(ConverterKind.Date, "yyyyMMdd")]
            public DateTime dataProcessamento;

            [FieldFixedLength(6)]
            [FieldAlign(AlignMode.Right, '0')]
            public int SeqProcessamento;

            [FieldFixedLength(6)]
            [FieldAlign(AlignMode.Right, '0')]
            public int SeqMovimento;

            [FieldFixedLength(2)]
            [FieldAlign(AlignMode.Right, '0')]
            public int codPlanoContas;

            [FieldFixedLength(25)]
            //[FieldAlign(AlignMode.Left, '0')]
            public string codigoContaContabil;

            [FieldFixedLength(2)]
            public string codigoPlanoAuxiliar;

            [FieldFixedLength(4)]
            public string codigoContaAuxiliar;

            [FieldFixedLength(4)]
            [FieldAlign(AlignMode.Right, '0')]
            public int codigoContaControle;

            [FieldFixedLength(1)]
            public string debitoCredito;

            [FieldFixedLength(17)]
            [FieldAlign(AlignMode.Right, '0')]
            public Int64 valorLancamento;

            [FieldFixedLength(30)]
            public string numeroDocumento;

            [FieldFixedLength(6)]
            [FieldAlign(AlignMode.Right, '0')]
            public int codigoHistorico;

            [FieldFixedLength(130)]
            public string complementoHistorico;

            [FieldFixedLength(8)]
            [FieldConverter(ConverterKind.Date, "yyyyMMdd")]
            public DateTime dataContabilizacao;

            [FieldFixedLength(6)]
            [FieldAlign(AlignMode.Right, '0')]
            public int codigoCentroCusto;

            [FieldFixedLength(6)]
            [FieldAlign(AlignMode.Right, '0')]
            public int codigoUnidadadeCORR;

            [FieldFixedLength(1)]
            public string debitoCreditoCORR;

            [FieldFixedLength(6)]
            [FieldAlign(AlignMode.Right, '0')]
            public int codCentroCustoCORR;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dataInicio"></param>
        /// <param name="dataFim"></param>
        /// <param name="idClientes"></param>
        /// <param name="ms">Saida: Memory Stream do arquivo ContabLançamento</param>
        /// <param name="nomeArquivo">Saida: Nome do Arquivo ContabLancamento</param>        
        public void ExportaContabLancamento(DateTime dataInicio, DateTime dataFim, List<int> idClientes, out MemoryStream ms, out string nomeArquivo)
        {
            string PATH_ARQUIVO_OUTPUT = DiretorioAplicacao.DiretorioBaseAplicacao + WebConfig.AppSettings.DiretorioDownloads;
            if (!PATH_ARQUIVO_OUTPUT.Trim().EndsWith("\\"))
            {
                PATH_ARQUIVO_OUTPUT += "\\";
            }
            PATH_ARQUIVO_OUTPUT = PATH_ARQUIVO_OUTPUT.Replace("\\", "\\\\");
            PATH_ARQUIVO_OUTPUT += @"\ContabLancamento" + dataInicio.ToString("yyyyMMdd") + "_" + dataFim.ToString("yyyyMMdd") + ".txt";

            ms = new MemoryStream();
            nomeArquivo = "";

            #region Consulta Sql
            ContabLancamentoQuery contabLancamentoQuery = new ContabLancamentoQuery("C");
            //
            contabLancamentoQuery.Where(contabLancamentoQuery.DataLancamento >= dataInicio &&
                                        contabLancamentoQuery.DataLancamento <= dataFim &&
                                        contabLancamentoQuery.IdCliente.In(idClientes)
                                        );
            //
            contabLancamentoQuery.OrderBy(contabLancamentoQuery.IdCliente.Ascending);
            //
            ContabLancamentoCollection contabLancamentoCollection = new ContabLancamentoCollection();
            contabLancamentoCollection.Load(contabLancamentoQuery);

            if (contabLancamentoCollection.Count == 0)
            {
                throw new Exception("Não há dados para exportar.");
            }
            #endregion
            
            // out
            nomeArquivo = "ContabLancamento" + dataInicio.ToString("yyyyMMdd") + "_" + dataFim.ToString("yyyyMMdd") + ".txt";

            List<ContabLancamentoDS> contabLancamentoDS = new List<ContabLancamentoDS>();

            DateTime dataProcessamento = dataInicio == dataFim ? dataFim : Calendario.RetornaUltimoDiaUtilMes(dataFim, 0);

            int j = 1;
            int k = 0;
            for (int i = 0; i < contabLancamentoCollection.Count; i++) 
            {
                DateTime dataLancamento = contabLancamentoCollection[i].DataLancamento.Value;
                int codigoContabil = 0;
                ClienteInterface clienteInterface = new ClienteInterface();
                if (!clienteInterface.LoadByPrimaryKey(contabLancamentoCollection[i].IdCliente.Value)) {
                    continue;
                }

                try {
                    if (String.IsNullOrEmpty(clienteInterface.CodigoContabil)) {
                        continue;
                    }
                    else {
                        codigoContabil = Convert.ToInt32(clienteInterface.CodigoContabil);
                    }
                }
                catch (Exception) {
                    continue;
                }
                                
                #region Debito
                ContabLancamentoDS cl = new ContabLancamentoDS();
                //                
                cl.tipoRegistro = Convert.ToInt32(TIPO_REGISTRO);
                cl.codigoProcesso = Convert.ToInt32(COD_PROCESSO);
                cl.codigoEmpresa = Convert.ToInt32(COD_EMPRESA);
                cl.codigoUnidade = codigoContabil;
                cl.dataProcessamento = dataProcessamento;
                cl.SeqProcessamento = j;
                cl.SeqMovimento = ++k;
                cl.codPlanoContas = Convert.ToInt32(COD_PLANO_CONTAS);
                cl.codigoContaContabil = contabLancamentoCollection[i].ContaDebito.Trim();
                cl.codigoPlanoAuxiliar = COD_PLANO_AUXILIAR;
                cl.codigoContaAuxiliar = COD_CONTA_AUXILIAR;
                cl.codigoContaControle = Convert.ToInt32(COD_CONTA_CONTROLE);
                cl.debitoCredito = "D";
                cl.valorLancamento = Convert.ToInt64(contabLancamentoCollection[i].Valor.Value * 100);
                cl.numeroDocumento = NUM_DOCUMENTO;
                cl.codigoHistorico = Convert.ToInt32(COD_HISTORICO);
                cl.complementoHistorico = contabLancamentoCollection[i].Descricao;
                cl.dataContabilizacao = dataLancamento;
                cl.codigoCentroCusto = Convert.ToInt32(COD_CENTRO_CUSTO);
                cl.codigoUnidadadeCORR = Convert.ToInt32(COD_UNIDADE_CORR);
                cl.debitoCreditoCORR = DEBITO_CREDITO_CORR;
                cl.codCentroCustoCORR = Convert.ToInt32(COD_CENTRO_CUSTO_CORR);
                //
                contabLancamentoDS.Add(cl);
                #endregion
                //
                #region Credito
                cl = new ContabLancamentoDS();
                //                
                cl.tipoRegistro = Convert.ToInt32(TIPO_REGISTRO);
                cl.codigoProcesso = Convert.ToInt32(COD_PROCESSO);
                cl.codigoEmpresa = Convert.ToInt32(COD_EMPRESA);
                cl.codigoUnidade = codigoContabil;
                cl.dataProcessamento = dataProcessamento;
                cl.SeqProcessamento = j;
                cl.SeqMovimento = ++k;
                cl.codPlanoContas = Convert.ToInt32(COD_PLANO_CONTAS);
                cl.codigoContaContabil = contabLancamentoCollection[i].ContaCredito;
                cl.codigoPlanoAuxiliar = COD_PLANO_AUXILIAR;
                cl.codigoContaAuxiliar = COD_CONTA_AUXILIAR;
                cl.codigoContaControle = Convert.ToInt32(COD_CONTA_CONTROLE);
                cl.debitoCredito = "C";
                cl.valorLancamento = Convert.ToInt64(contabLancamentoCollection[i].Valor.Value * 100);
                cl.numeroDocumento = NUM_DOCUMENTO;
                cl.codigoHistorico = Convert.ToInt32(COD_HISTORICO);
                cl.complementoHistorico = contabLancamentoCollection[i].Descricao;
                cl.dataContabilizacao = dataLancamento;
                cl.codigoCentroCusto = Convert.ToInt32(COD_CENTRO_CUSTO);
                cl.codigoUnidadadeCORR = Convert.ToInt32(COD_UNIDADE_CORR);
                cl.debitoCreditoCORR = DEBITO_CREDITO_CORR;
                cl.codCentroCustoCORR = Convert.ToInt32(COD_CENTRO_CUSTO_CORR);
                //
                contabLancamentoDS.Add(cl);
                #endregion

                j++;
            }

            FileHelperEngine engine = new FileHelperEngine(typeof(ContabLancamentoDS));
            
            StreamWriter arquivo = new StreamWriter(ms, Encoding.GetEncoding("ISO-8859-1"));
            engine.Encoding = Encoding.GetEncoding("ISO-8859-1");
            //
            engine.WriteStream(arquivo, contabLancamentoDS);

            //Fecha o arquivo
            arquivo.Flush();
        }
    }
}