﻿using FileHelpers;
using System;
using System.Collections.Generic;
using System.IO;
using Financial.WebConfigConfiguration;
using System.Text;
using Financial.Investidor;
using Financial.Util;
using Financial.RendaFixa;
using Financial.RendaFixa.Enums;

namespace Financial.Export
{

    /// <summary>
    
    /// </summary>
    public class CCOUSinacor
    {
        const int TIPO_REGISTRO_HEADER = 0;
        const int TIPO_REGISTRO_FOOTER = 99;
        const int TIPO_REGISTRO_LINHA = 1;
        const string OUTN = "OUTN";
        const string OUTROS = "OUTROS";
        const string OUT = "  OUT";
        const string EMPR = "EMPR";
        const int CODIGO_IR = 1967;
        const int CODIGO_IOF = 1968;

        [FixedLengthRecord()]
        public class CCOUSinacorHeader
        {
            [FieldFixedLength(2)]
            [FieldAlign(AlignMode.Right, '0')]
            public int TipoRegistro;

            [FieldFixedLength(6)]
            public string Outros;

            [FieldFixedLength(5)]
            public string Out;

            [FieldFixedLength(237)]
            public string Filler;
        }

        [FixedLengthRecord()]
        public class CCOUSinacorFooter
        {
            [FieldFixedLength(2)]
            [FieldAlign(AlignMode.Right, '0')]
            public int TipoRegistro;

            [FieldFixedLength(6)]
            public string Outros;

            [FieldFixedLength(5)]
            public string Out;

            [FieldFixedLength(237)]
            public string Filler;
        }


        [FixedLengthRecord()]
        public class CCOUSinacorDS
        {
            [FieldFixedLength(2)]
            [FieldAlign(AlignMode.Right, '0')]
            public int TipoRegistro;

            [FieldFixedLength(10)]
            [FieldConverter(ConverterKind.Date, "dd/MM/yyyy")]
            public DateTime DataReferencia;

            [FieldFixedLength(7)]
            [FieldAlign(AlignMode.Right, '0')]
            public int IdCliente;

            [FieldFixedLength(4)]
            [FieldAlign(AlignMode.Right, '0')]
            public int Codigo;

            [FieldFixedLength(15)]
            [FieldAlign(AlignMode.Right, '0')]
            public Int64 ValorLancamento;

            [FieldFixedLength(97)]
            public string Filler;

            [FieldFixedLength(10)]
            [FieldConverter(ConverterKind.Date, "dd/MM/yyyy")]
            public DateTime DataReferencia2;

            [FieldFixedLength(82)]
            public string Filler2;

            [FieldFixedLength(4)]
            public string Outn;

            [FieldFixedLength(4)]
            public string Empr;

            [FieldFixedLength(15)]
            [FieldAlign(AlignMode.Right, '0')]
            public Int64 ValorBase;
        }

        public void ExportaCCOUSinacor(DateTime dataExportacao, List<int> idClientes, out MemoryStream ms, out string nomeArquivo)
        {
            // out
            ms = new MemoryStream();
            nomeArquivo = "CCOU" + dataExportacao.ToString("MMdd") + ".txt";


            #region Consulta Sql OperacaoRendaFixaCollection
            OperacaoRendaFixaQuery operacaoRendaFixaQuery = new OperacaoRendaFixaQuery("O");
            //

            operacaoRendaFixaQuery.Select(operacaoRendaFixaQuery.IdCliente,
                                            operacaoRendaFixaQuery.Rendimento.Sum(),
                                            operacaoRendaFixaQuery.ValorIR.Sum(),
                                            operacaoRendaFixaQuery.ValorIOF.Sum());
            operacaoRendaFixaQuery.Where(operacaoRendaFixaQuery.DataOperacao.Equal(dataExportacao) &&
                                        operacaoRendaFixaQuery.IdCliente.In(idClientes));
            operacaoRendaFixaQuery.GroupBy(operacaoRendaFixaQuery.IdCliente);
            //
            operacaoRendaFixaQuery.OrderBy(operacaoRendaFixaQuery.IdCliente.Ascending);
            //
            OperacaoRendaFixaCollection operacaoRendaFixaCollection = new OperacaoRendaFixaCollection();
            operacaoRendaFixaCollection.Load(operacaoRendaFixaQuery);
            #endregion

            #region Consulta Sql LiquidacaoRendaFixaCollection
            LiquidacaoRendaFixaQuery liquidacaoRendaFixaQuery = new LiquidacaoRendaFixaQuery("L");
            //
            liquidacaoRendaFixaQuery.Select(liquidacaoRendaFixaQuery.IdCliente,
                                            liquidacaoRendaFixaQuery.Rendimento.Sum(),
                                            liquidacaoRendaFixaQuery.ValorIR.Sum(),
                                            liquidacaoRendaFixaQuery.ValorIOF.Sum());
            liquidacaoRendaFixaQuery.Where( liquidacaoRendaFixaQuery.TipoLancamento.In((byte)TipoLancamentoLiquidacao.Vencimento,
                (byte)TipoLancamentoLiquidacao.Juros, (byte)TipoLancamentoLiquidacao.PagtoPrincipal) && 
                                            liquidacaoRendaFixaQuery.DataLiquidacao.Equal(dataExportacao) &&
                                           liquidacaoRendaFixaQuery.IdCliente.In(idClientes));
            liquidacaoRendaFixaQuery.GroupBy(liquidacaoRendaFixaQuery.IdCliente);
            //
            liquidacaoRendaFixaQuery.OrderBy(liquidacaoRendaFixaQuery.IdCliente.Ascending);
            //
            LiquidacaoRendaFixaCollection liquidacaoRendaFixaCollection = new LiquidacaoRendaFixaCollection();
            liquidacaoRendaFixaCollection.Load(liquidacaoRendaFixaQuery);
            #endregion

            if (operacaoRendaFixaCollection.Count == 0 && liquidacaoRendaFixaCollection.Count == 0)
            {
                throw new Exception("Não há dados para exportar.");
            }

            CCOUSinacorHeader ccouHeader = new CCOUSinacorHeader();
            ccouHeader.TipoRegistro = TIPO_REGISTRO_HEADER;
            ccouHeader.Outros = OUTROS;
            ccouHeader.Out = OUT;

            List<CCOUSinacorHeader> ccouSinacorHeaderList = new List<CCOUSinacorHeader>();
            ccouSinacorHeaderList.Add(ccouHeader);

            List<CCOUSinacorDS> ccouSinacorDS = new List<CCOUSinacorDS>();

            for (int operacaoCount = 0; operacaoCount < operacaoRendaFixaCollection.Count; operacaoCount++)
            {
                OperacaoRendaFixa operacaoRendaFixa = operacaoRendaFixaCollection[operacaoCount];
                #region Preenchimento do Lancamento de IR
                if (operacaoRendaFixa.ValorIR.Value > 0)
                {
                    CCOUSinacorDS ccou = new CCOUSinacorDS();
                    ccou.TipoRegistro = TIPO_REGISTRO_LINHA;
                    ccou.DataReferencia = dataExportacao;
                    ccou.IdCliente = operacaoRendaFixa.IdCliente.Value;
                    ccou.Codigo = CODIGO_IR;
                    ccou.ValorLancamento = Convert.ToInt64(operacaoRendaFixa.ValorIR.Value * 100);
                    ccou.DataReferencia2 = dataExportacao;
                    ccou.ValorBase = Convert.ToInt64(operacaoRendaFixa.Rendimento.Value * 100);
                    ccou.Outn = OUTN;
                    ccou.Empr = EMPR;

                    ccouSinacorDS.Add(ccou);
                }
                #endregion

                #region Preenchimento do Lancamento de IOF
                if (operacaoRendaFixa.ValorIOF.Value > 0)
                {
                    CCOUSinacorDS ccou = new CCOUSinacorDS();
                    ccou.TipoRegistro = TIPO_REGISTRO_LINHA;
                    ccou.DataReferencia = dataExportacao;
                    ccou.IdCliente = operacaoRendaFixa.IdCliente.Value;
                    ccou.Codigo = CODIGO_IOF;
                    ccou.ValorLancamento = Convert.ToInt64(operacaoRendaFixa.ValorIOF.Value * 100);
                    ccou.DataReferencia2 = dataExportacao;
                    ccou.ValorBase = Convert.ToInt64(operacaoRendaFixa.Rendimento.Value * 100);
                    ccou.Outn = OUTN;
                    ccou.Empr = EMPR;

                    ccouSinacorDS.Add(ccou);
                }
                #endregion
            }

            for (int operacaoCount = 0; operacaoCount < liquidacaoRendaFixaCollection.Count; operacaoCount++)
            {
                LiquidacaoRendaFixa liquidacaoRendaFixa = liquidacaoRendaFixaCollection[operacaoCount];
                #region Preenchimento do Lancamento de IR
                if (liquidacaoRendaFixa.ValorIR.Value > 0)
                {
                    CCOUSinacorDS ccou = new CCOUSinacorDS();
                    ccou.TipoRegistro = TIPO_REGISTRO_LINHA;
                    ccou.DataReferencia = dataExportacao;
                    ccou.IdCliente = liquidacaoRendaFixa.IdCliente.Value;
                    ccou.Codigo = CODIGO_IR;
                    ccou.ValorLancamento = Convert.ToInt64(liquidacaoRendaFixa.ValorIR.Value * 100);
                    ccou.DataReferencia2 = dataExportacao;
                    ccou.ValorBase = Convert.ToInt64(liquidacaoRendaFixa.Rendimento.Value * 100);
                    ccou.Outn = OUTN;
                    ccou.Empr = EMPR;

                    ccouSinacorDS.Add(ccou);
                }
                #endregion

                #region Preenchimento do Lancamento de IOF
                if (liquidacaoRendaFixa.ValorIOF.Value > 0)
                {
                    CCOUSinacorDS ccou = new CCOUSinacorDS();
                    ccou.TipoRegistro = TIPO_REGISTRO_LINHA;
                    ccou.DataReferencia = dataExportacao;
                    ccou.IdCliente = liquidacaoRendaFixa.IdCliente.Value;
                    ccou.Codigo = CODIGO_IOF;
                    ccou.ValorLancamento = Convert.ToInt64(liquidacaoRendaFixa.ValorIOF.Value * 100);
                    ccou.DataReferencia2 = dataExportacao;
                    ccou.ValorBase = Convert.ToInt64(liquidacaoRendaFixa.Rendimento.Value * 100);
                    ccou.Outn = OUTN;
                    ccou.Empr = EMPR;

                    ccouSinacorDS.Add(ccou);
                }
                #endregion
            }
                                    
            CCOUSinacorFooter ccouFooter = new CCOUSinacorFooter();
            ccouFooter.TipoRegistro = TIPO_REGISTRO_FOOTER;
            ccouFooter.Outros = OUTROS;
            ccouFooter.Out = OUT;

            List<CCOUSinacorFooter> ccouSinacorFooterList = new List<CCOUSinacorFooter>();
            ccouSinacorFooterList.Add(ccouFooter);


            FileHelperEngine engine = new FileHelperEngine(typeof(CCOUSinacorHeader));
            StreamWriter arquivo = new StreamWriter(ms, Encoding.GetEncoding("ISO-8859-1"));
            //
            engine = new FileHelperEngine(typeof(CCOUSinacorHeader));
            engine.Encoding = Encoding.GetEncoding("ISO-8859-1");
            engine.WriteStream(arquivo, ccouSinacorHeaderList);

            engine = new FileHelperEngine(typeof(CCOUSinacorDS));
            engine.Encoding = Encoding.GetEncoding("ISO-8859-1");
            engine.WriteStream(arquivo, ccouSinacorDS);

            engine = new FileHelperEngine(typeof(CCOUSinacorFooter));
            engine.Encoding = Encoding.GetEncoding("ISO-8859-1");
            engine.WriteStream(arquivo, ccouSinacorFooterList);

            //Fecha o arquivo
            arquivo.Flush();
        }
    }
}