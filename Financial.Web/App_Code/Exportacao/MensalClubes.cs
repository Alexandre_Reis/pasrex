﻿using System.Collections.Generic;
using Financial.Fundo;
using Financial.Investidor;
using Financial.Common;
using System.Text;
using Financial.Util;
using Financial.CRM;
using Financial.Bolsa;
using Financial.RendaFixa;
using Financial.ContaCorrente;
using System;
using System.IO;
using Financial.Investidor.Enums;
using Financial.InvestidorCotista;
using Financial.RendaFixa.Enums;
using Financial.Fundo.Enums;

namespace Financial.Export {

    /// <summary>
    ///  Classe para fazer as Exportações Pertencentes a Fundo 
    /// </summary>
    public class MensalClubes {

        /// <summary>
        /// Determina o numero total de casas que um numero deve ter com zeros a esquerda
        /// </summary>
        private enum AlinhamentoEsquerda {
            [StringValue("00000")]
            _5Casas = 1,

            [StringValue("000000")]
            _6Casas = 2,

            [StringValue("0000000000")]
            _10Casas = 3,

            [StringValue("0000000000000")]
            _13Casas = 4,

            [StringValue("000000000000000000")]
            _18Casas = 5,
        }

        /// <summary>
        /// Valores da Primeira linha do arquivo
        /// </summary>
        #region Valores do Header
        class ValoresHeader {
            public string campoFixo;
            public string anoMes;
            public int idAgenteMercado;
            public string dataReferencia;
        }
        private ValoresHeader valoresHeader = new ValoresHeader();
        #endregion
        
        /// <summary>
        /// Valores da Segunda Linha
        /// </summary>
        #region Valores do Detalhe
        class ValoresDetail {
            public string campoFixo;                     // Campo 01
            public string codigoBovespa;                    // Campo 02
            public string UF;                            // Campo 03            
            public int numeroTotalCotistas;              // Campo 04                          
            public decimal? valorAcoes;                  // Campo 05
            public decimal? valorTotalSubscricao;        // Campo 06
            public decimal? valorDebenturesConversiveis; // Campo 07
            public decimal? valorDebenturesSimples;      // Campo 08   
            public decimal? valorMoveis;                 // Campo 09
            public decimal? valorTituloPrivado;          // Campo 10
            public decimal? valorTituloPublico;          // Campo 11
            public decimal? valorMoedaPrivatizacao;      // Campo 12
            public decimal? valorBMF;                    // Campo 13
            public decimal? valorOutrasAplicacoes;       // Campo 14
            public decimal? valorQuotaFundoRendaFixa;    // Campo 15
            public decimal? valorPremiosPagos;           // Campo 16
            public decimal? valorPremiosRecebidos;       // Campo 17
            public decimal? valorMercadoOpcoes;          // Campo 18
            public decimal? valorComprasTermos;          // Campo 19
            public decimal? valorVendasTermos;           // Campo 20
            public decimal? valorTotalTermos;            // Campo 21
            public decimal? valorCaptacoes;              // Campo 22
            public decimal? valorResgates;               // Campo 23
            public decimal? valorFluxoRecursos;          // Campo 24
            public decimal? valorCotaUltimoDiaMes;       // Campo 25
            public decimal? valorPatrimonioLiquido;      // Campo 26
        }
        private List<ValoresDetail> valoresDetail = new List<ValoresDetail>();
        #endregion

        /// <summary>
        /// Retorna uma Stream de Mémoria de um arquivo txt. A Stream permanece aberta
        /// </summary>
        /// <param name="dataPosicao"></param>
        /// <param name="nomeArquivo">Nome do Arquivo MensalClube formado de acordo com o IdAgente</param>
        /// <returns></returns>
        /// <exception cref="ProcessaMensalClubesException">
        /// throws ProcessaMensalClubesException se ocorreu algum erro no processamento
        /// </exception>
        public MemoryStream ExportaMensalClubes(DateTime dataPosicao, out string nomeArquivo) {
            MemoryStream ms = new MemoryStream();
            nomeArquivo = "";

            #region Busca as Carteiras
            CarteiraQuery carteiraQuery = new CarteiraQuery("C");
            ClienteQuery clienteQuery = new ClienteQuery("B");
            AgenteMercadoQuery agenteMercadoQuery = new AgenteMercadoQuery("A");

            carteiraQuery.Select(carteiraQuery.IdCarteira, clienteQuery.IdCliente, agenteMercadoQuery.IdAgente, agenteMercadoQuery.Uf);
            //
            carteiraQuery.InnerJoin(clienteQuery).On(carteiraQuery.IdCarteira == clienteQuery.IdCliente);
            carteiraQuery.InnerJoin(agenteMercadoQuery).On(carteiraQuery.IdAgenteAdministrador == agenteMercadoQuery.IdAgente);
            //
            carteiraQuery.Where(clienteQuery.IdTipo == TipoClienteFixo.Clube);
            //
            CarteiraCollection carteiraCollection = new CarteiraCollection();
            carteiraCollection.Load(carteiraQuery);
            #endregion

            if (carteiraCollection.HasData) {
                int idAgenteAux = (int)carteiraCollection[0].GetColumn(AgenteMercadoMetadata.ColumnNames.IdAgente);

                StringBuilder nomeArquivoAux = new StringBuilder();
                nomeArquivoAux.Append("MensalClube_");
                nomeArquivoAux.Append(this.FormataValor(Convert.ToDecimal(idAgenteAux), AlinhamentoEsquerda._5Casas, false))
                           .Append("_").Append(dataPosicao.Month)
                           .Append(".txt");
                // out
                nomeArquivo = nomeArquivoAux.ToString();
                
                // Cada Carteira Representa uma linha de Detalhe no Arquivo
                for (int i = 0; i < carteiraCollection.Count; i++) {
                    int idCliente = (int)carteiraCollection[i].GetColumn(ClienteMetadata.ColumnNames.IdCliente);
                    int idCarteira = carteiraCollection[i].IdCarteira.Value;
                    int idAgente = (int)carteiraCollection[i].GetColumn(AgenteMercadoMetadata.ColumnNames.IdAgente);

                    #region Busca o RegistroBovespa
                    ClienteInterface clienteInterface = new ClienteInterface();
                    clienteInterface.LoadByPrimaryKey(idCliente);
                    //
                    string registroBovespa = "";
                    //
                    if (clienteInterface.es.HasData)
                    {
                        registroBovespa = clienteInterface.RegistroBovespa != null
                                ? clienteInterface.RegistroBovespa.Trim()
                                : "";
                    }
                    #endregion

                    #region Processa somente Carteiras que possuem historicoCota na Data Desejada
                    HistoricoCota historicoCotaAux = new HistoricoCota();
                    if (historicoCotaAux.LoadByPrimaryKey(dataPosicao, idCarteira)) 
                    {                        
                        #region Busca Estado Da Pessoa
                        string uf = (string)carteiraCollection[i].GetColumn(AgenteMercadoMetadata.ColumnNames.Uf);                                                                                   
                        
                        uf = String.IsNullOrEmpty(uf) ? "  " : uf.Trim().ToUpper();
                        //
                        #endregion

                        ValoresDetail valorDetailAux = new ValoresDetail();

                        // Salva os Dados de Cada Carteira
                        #region Pega Valores Table
                        PosicaoBolsaHistorico posicaoBolsaHistorico = new PosicaoBolsaHistorico();
                        PosicaoTermoBolsaHistorico posicaoTermoBolsaHistorico = new PosicaoTermoBolsaHistorico();
                        PosicaoRendaFixaHistorico posicaoRendaFixaHistorico = new PosicaoRendaFixaHistorico();
                        PosicaoFundoHistorico posicaoFundoHistorico = new PosicaoFundoHistorico();
                        SaldoCaixa saldoCaixa = new SaldoCaixa();
                        //                                
                        decimal valorMercadoAcoes = posicaoBolsaHistorico.RetornaValorMercadoAcoes(idCarteira, dataPosicao);
                        decimal valorOpcaoPosicaoComprada = posicaoBolsaHistorico.RetornaValorMercadoOpcaoPosicaoComprada(idCarteira, dataPosicao);
                        decimal valorOpcaoPosicaoVendida = Math.Abs(posicaoBolsaHistorico.RetornaValorMercadoOpcaoPosicaoVendida(idCarteira, dataPosicao));
                        //
                        decimal valorOpcaoBolsa = valorOpcaoPosicaoComprada + valorOpcaoPosicaoVendida;
                        //
                        decimal valorTermoComprado = posicaoTermoBolsaHistorico.RetornaValorTermoOriginalComprado(idCarteira, dataPosicao);
                        decimal valorTermoVendido = Math.Abs(posicaoTermoBolsaHistorico.RetornaValorTermoOriginalVendido(idCarteira, dataPosicao));
                        //
                        decimal valorTermoBolsa = valorTermoComprado + valorTermoVendido;
                        //
                        decimal tituloPrivados = posicaoRendaFixaHistorico.RetornaValorMercado(idCarteira, TipoPapelTitulo.Privado, dataPosicao);
                        decimal tituloPublico = posicaoRendaFixaHistorico.RetornaValorMercado(idCarteira, TipoPapelTitulo.Publico, dataPosicao);

                        decimal totalFundosRF = posicaoFundoHistorico.RetornaValorBruto(idCarteira, (byte)TipoCarteiraFundo.RendaFixa, dataPosicao);
                        //
                        saldoCaixa.BuscaSaldoCaixa(idCarteira, dataPosicao);
                        decimal outrasAplicacoes = 0;
                        if (saldoCaixa.es.HasData) {
                            outrasAplicacoes = saldoCaixa.SaldoFechamento.HasValue ? saldoCaixa.SaldoFechamento.Value : 0;
                        }

                        #region Preenche os campos
                        valorDetailAux.campoFixo = "01";
                        valorDetailAux.codigoBovespa = registroBovespa;
                        valorDetailAux.UF = uf;
                        valorDetailAux.valorAcoes = valorMercadoAcoes * 100;
                        valorDetailAux.valorTotalSubscricao = 0;
                        valorDetailAux.valorDebenturesConversiveis = 0;
                        valorDetailAux.valorDebenturesSimples = 0;
                        valorDetailAux.valorMoveis = 0;
                        valorDetailAux.valorTituloPrivado = tituloPrivados * 100;
                        valorDetailAux.valorTituloPublico = tituloPublico * 100;
                        valorDetailAux.valorMoedaPrivatizacao = 0;
                        valorDetailAux.valorBMF = 0;
                        valorDetailAux.valorOutrasAplicacoes = outrasAplicacoes * 100;
                        valorDetailAux.valorQuotaFundoRendaFixa = totalFundosRF * 100;
                        valorDetailAux.valorPremiosPagos = valorOpcaoPosicaoComprada * 100;
                        valorDetailAux.valorPremiosRecebidos = valorOpcaoPosicaoVendida * 100;
                        valorDetailAux.valorMercadoOpcoes = valorOpcaoBolsa * 100;
                        valorDetailAux.valorComprasTermos = valorTermoComprado * 100;
                        valorDetailAux.valorVendasTermos = valorTermoVendido * 100;
                        valorDetailAux.valorTotalTermos = valorTermoBolsa * 100;
                        //
                        OperacaoCotista operacaoCotista = new OperacaoCotista();
                        PosicaoCotistaHistorico posicaoCotistaHistorico = new PosicaoCotistaHistorico();
                        HistoricoCota historicoCota = new HistoricoCota();
                        //
                        DateTime dataInicioMes = Calendario.RetornaPrimeiroDiaCorridoMes(dataPosicao, 0);
                        decimal entradaRecursos = operacaoCotista.RetornaSumValorBrutoAplicacao(idCarteira, dataInicioMes, dataPosicao);
                        decimal saidaRecursos = operacaoCotista.RetornaSumValorBrutoResgate(idCarteira, dataInicioMes, dataPosicao);
                        //
                        decimal fluxoRecursos = entradaRecursos + saidaRecursos;
                        int numeroCotistas = posicaoCotistaHistorico.RetornaCountNumeroCotistas(idCarteira, dataPosicao);
                        historicoCota.BuscaValorCotaDia(idCarteira, dataPosicao);
                        decimal valorCota = historicoCota.CotaFechamento.Value;
                        historicoCota.BuscaValorPatrimonioDia(idCarteira, dataPosicao);
                        decimal patrimonioLiquido = historicoCota.PLFechamento.Value;
                        //
                        valorDetailAux.valorCaptacoes = entradaRecursos * 100;
                        valorDetailAux.valorResgates = saidaRecursos * 100;
                        valorDetailAux.valorFluxoRecursos = fluxoRecursos * 100;
                        valorDetailAux.numeroTotalCotistas = numeroCotistas;

                        valorDetailAux.valorCotaUltimoDiaMes = Math.Round(valorCota, 7) * 10000000;
                        // Numero Positivo
                        valorDetailAux.valorCotaUltimoDiaMes = Math.Abs(valorDetailAux.valorCotaUltimoDiaMes.Value);
                        // Tamanho do Numero
                        // Convert de decimal para bigint
                        Int64 numAux64 = Convert.ToInt64(valorDetailAux.valorCotaUltimoDiaMes);
                        int tam = numAux64.ToString().Trim().Length;
                        // Numero entre 1.000.000,0000000 e 9.999.000,0000000 (tamanho = 14)
                        if (tam == 14) {
                            // Retira o algarismo da esquerda e transforma em decimal
                            string num = numAux64.ToString().Substring(1, 13);
                            decimal numAux = Convert.ToDecimal(num);
                            valorDetailAux.valorCotaUltimoDiaMes = numAux;
                        }
                        //
                        valorDetailAux.valorPatrimonioLiquido = patrimonioLiquido * 100;
                        //
                        #endregion
                        #endregion
                        //
                        // Adiciona os dados referentes a uma carteira
                        this.valoresDetail.Add(valorDetailAux);
                        
                    }
                    #endregion
                }

                //
                #region EscreveArquivo
              
                try {                                                          
                    //Abrir o arquivo
                    StreamWriter arquivo = new StreamWriter(ms, Encoding.ASCII);

                    #region Header
                    this.EscreveHeader(arquivo, idAgenteAux, dataPosicao);
                    #endregion

                    #region Detalhes
                    for (int i = 0; i < this.valoresDetail.Count; i++) {
                        this.EscreveDetalhe(arquivo, i);
                    }
                    #endregion

                    arquivo.Flush();

                    // Retorna a MemoryStream para o inicio
                    ms.Seek(0, SeekOrigin.Begin);
                }
                catch (Exception e) {
                    Console.WriteLine(e.Message);
                    string mensagem = "";
                    mensagem = "Arquivo MensalClubes com Data " + dataPosicao.ToString("dd/MM/yyyy") + " não Exportado \n";
                    mensagem += "\n\t - " +e.Message;
                    
                    throw new ProcessaMensalClubesException(mensagem);

                }
                #endregion
            }
            // 0 registros
            else {
                throw new ProcessaMensalClubesException("Não existem Dados de Exportação");
            }

            return ms;

        }

        /// <summary>
        /// Escreve no arquivo o objeto ValoresHeader
        /// </summary>
        /// <param name="arquivo"></param>
        /// <param name="idAgente"></param>
        /// <param name="dataPosicao"></param>
        private void EscreveHeader(StreamWriter arquivo, int idAgente, DateTime dataPosicao) {                                    
            this.valoresHeader.campoFixo = "00";
            //
            string dataStringFormatoPortugues = dataPosicao.ToString("ddMMyyyy");
            string ano = dataStringFormatoPortugues.Substring(4,4);
            string mes = dataStringFormatoPortugues.Substring(2,2);
            string dia = dataStringFormatoPortugues.Substring(0, 2);

            this.valoresHeader.anoMes = ano + mes;
            this.valoresHeader.idAgenteMercado = idAgente;
            this.valoresHeader.dataReferencia = ano + mes + dia;
            //
            arquivo.Write(this.valoresHeader.campoFixo);
            arquivo.Write(this.valoresHeader.anoMes);
            arquivo.Write(this.valoresHeader.idAgenteMercado.ToString(StringEnum.GetStringValue(AlinhamentoEsquerda._6Casas)));
            arquivo.Write(this.valoresHeader.dataReferencia);
        }

        /// <summary>
        /// Escreve no arquivo o objeto ValoresDetail
        /// </summary>
        /// <param name="arquivo"></param>
        /// <param name="linha">numero da linha de detalhe a escrever começando de 0</param>
        /// 
        private void EscreveDetalhe(StreamWriter arquivo, int linha) {
            arquivo.WriteLine();
            //  
            arquivo.Write(this.valoresDetail[linha].campoFixo);
            arquivo.Write(this.valoresDetail[linha].codigoBovespa.PadLeft(10, '0')); // Completa com '0' a esquerda
            arquivo.Write(this.valoresDetail[linha].UF);
            arquivo.Write(this.valoresDetail[linha].numeroTotalCotistas.ToString(StringEnum.GetStringValue(AlinhamentoEsquerda._5Casas)));
            arquivo.Write(this.FormataValor(this.valoresDetail[linha].valorAcoes.Value, AlinhamentoEsquerda._13Casas));
            arquivo.Write(this.FormataValor(this.valoresDetail[linha].valorTotalSubscricao.Value, AlinhamentoEsquerda._13Casas));
            arquivo.Write(this.FormataValor(this.valoresDetail[linha].valorDebenturesConversiveis.Value, AlinhamentoEsquerda._13Casas));
            arquivo.Write(this.FormataValor(this.valoresDetail[linha].valorDebenturesSimples.Value, AlinhamentoEsquerda._13Casas));
            arquivo.Write(this.FormataValor(this.valoresDetail[linha].valorMoveis.Value, AlinhamentoEsquerda._13Casas));
            arquivo.Write(this.FormataValor(this.valoresDetail[linha].valorTituloPrivado.Value, AlinhamentoEsquerda._13Casas));
            arquivo.Write(this.FormataValor(this.valoresDetail[linha].valorTituloPublico.Value, AlinhamentoEsquerda._13Casas));
            arquivo.Write(this.FormataValor(this.valoresDetail[linha].valorMoedaPrivatizacao.Value, AlinhamentoEsquerda._13Casas));
            arquivo.Write(this.FormataValor(this.valoresDetail[linha].valorBMF.Value, AlinhamentoEsquerda._13Casas));
            arquivo.Write(this.FormataValor(this.valoresDetail[linha].valorOutrasAplicacoes.Value, AlinhamentoEsquerda._13Casas));
            arquivo.Write(this.FormataValor(this.valoresDetail[linha].valorQuotaFundoRendaFixa.Value, AlinhamentoEsquerda._13Casas));
            arquivo.Write(this.FormataValor(this.valoresDetail[linha].valorPremiosPagos.Value, AlinhamentoEsquerda._13Casas));
            arquivo.Write(this.FormataValor(this.valoresDetail[linha].valorPremiosRecebidos.Value, AlinhamentoEsquerda._13Casas));
            arquivo.Write(this.FormataValor(this.valoresDetail[linha].valorMercadoOpcoes.Value, AlinhamentoEsquerda._13Casas));
            arquivo.Write(this.FormataValor(this.valoresDetail[linha].valorComprasTermos.Value, AlinhamentoEsquerda._13Casas));
            arquivo.Write(this.FormataValor(this.valoresDetail[linha].valorVendasTermos.Value, AlinhamentoEsquerda._13Casas));
            arquivo.Write(this.FormataValor(this.valoresDetail[linha].valorTotalTermos.Value, AlinhamentoEsquerda._13Casas));
            arquivo.Write(this.FormataValor(this.valoresDetail[linha].valorCaptacoes.Value, AlinhamentoEsquerda._13Casas));
            arquivo.Write(this.FormataValor(this.valoresDetail[linha].valorResgates.Value, AlinhamentoEsquerda._13Casas));
            arquivo.Write(this.FormataValor(this.valoresDetail[linha].valorFluxoRecursos.Value, AlinhamentoEsquerda._13Casas));                      
            arquivo.Write(this.valoresDetail[linha].valorCotaUltimoDiaMes.Value.ToString(StringEnum.GetStringValue(AlinhamentoEsquerda._13Casas)));
            arquivo.Write(this.FormataValor(this.valoresDetail[linha].valorPatrimonioLiquido.Value, AlinhamentoEsquerda._18Casas));
        }

        /// <summary>
        /// Formata um valor decimal com N casas Completando com zeros a esquerda
        /// Se numero é Positivo Coloca um Sinal de + na frente
        /// </summary>
        /// <param name="valor"></param>
        /// <param name="numeroCasasAlinhamentoEsquerda"></param>
        /// <returns></returns>
        private string FormataValor(decimal valor, AlinhamentoEsquerda numeroCasasAlinhamentoEsquerda) {
            return this.FormataValor(valor, numeroCasasAlinhamentoEsquerda, true);
        }

        /// <summary>
        /// Formata um valor decimal com N casas Completando com zeros a esquerda
        /// Se colocaPositivo=true e Se numero é Positivo Coloca um Sinal de + na frente
        /// Sinal Negativo Sempre Aparece
        /// </summary>
        /// <param name="valor"></param>
        /// <param name="numeroCasasAlinhamentoEsquerda"></param>
        /// <param name="colocaPositivo"></param>
        /// <returns></returns>
        private string FormataValor(decimal valor, AlinhamentoEsquerda numeroCasasAlinhamentoEsquerda, bool colocaPositivo) {
            if (colocaPositivo) {
                return valor.ToString(
                    valor >= 0
                    ? "+" + StringEnum.GetStringValue(numeroCasasAlinhamentoEsquerda)
                    : StringEnum.GetStringValue(numeroCasasAlinhamentoEsquerda));
            }
            else {
                return valor.ToString(StringEnum.GetStringValue(numeroCasasAlinhamentoEsquerda));            
            }
        }
    }
}