﻿using System.Collections.Generic;
using System;
using Financial.Util;
using System.IO;
using Financial.Fundo;
using Financial.Common;
using Financial.InvestidorCotista;
using System.Text;
using Financial.Investidor;
using Financial.Investidor.Enums;
using Financial.Bolsa;
using Financial.Bolsa.Enums;
using Financial.CRM;
using Financial.BMF;
using Financial.RendaFixa;
using Financial.ContaCorrente;
using Financial.Interfaces.Export.Fundo;
using Financial.RendaFixa.Enums;
using System.Data;
using System.Globalization;
using Financial.Common.Enums;
using Financial.BMF.Enums;
using Financial.ContaCorrente.Enums;
using System.Collections;
using System.Xml.Serialization;
using System.Xml;
using EntitySpaces.Interfaces;

namespace Financial.Export
{

    /// <summary>
    ///  Classe para fazer as Exportações Pertencentes a Fundo
    /// </summary>
    public partial class XMLPosicaoAnbid
    {
        private bool multiMoeda;
        private int idMoedaCarteira;

        public bool MultiMoeda
        {
            get { return this.multiMoeda; }
        }

        public int? IdMoedaCarteira
        {
            get { return this.idMoedaCarteira; }
        }

        public XMLPosicaoAnbid()
        {
        }

        #region Estrutura Arquivo XML
        [XmlRootAttribute(ElementName = "arquivoposicao_4_01", IsNullable = false, Namespace = "")]
        public class ArquivoPosicao
        {

            public ArquivoPosicao() { }

            [XmlElement(ElementName = "fundo")]
            public List<Fundo> fundo = new List<Fundo>();

            [XmlElement(ElementName = "carteira")]
            public List<Carteira> carteira = new List<Carteira>();
        }

        public class Carteira
        {

            //public Carteira() { }

            public HeaderCarteira header = new HeaderCarteira();

            [XmlElement(ElementName = "titpublico")]
            public List<TitPublico> titpublico = new List<TitPublico>();

            [XmlElement(ElementName = "titprivado")]
            public List<TitPrivado> titprivado = new List<TitPrivado>();

            [XmlElement(ElementName = "debenture")]
            public List<Debenture> debenture = new List<Debenture>();

            [XmlElement(ElementName = "termorf")]
            public List<TermoRf> termorf = new List<TermoRf>();

            [XmlElement(ElementName = "opcoesderiv")]
            public List<OpcaoDeriv> opcoesderiv = new List<OpcaoDeriv>();

            [XmlElement(ElementName = "acoes")]
            public List<Acoes> acoes = new List<Acoes>();            

            [XmlElement(ElementName = "opcoesacoes")]
            public List<OpcoesAcoes> opcoesacoes = new List<OpcoesAcoes>();

            [XmlElement(ElementName = "termorv")]
            public List<TermoRv> termorv = new List<TermoRv>();

            [XmlElement(ElementName = "futuros")]
            public List<Futuros> futuros = new List<Futuros>();

            [XmlElement(ElementName = "swap")]
            public List<Swap> swap = new List<Swap>();

            public Caixa caixa = new Caixa();

            [XmlElement(ElementName = "cotas")]
            public List<Cotas> cotas = new List<Cotas>();

            public Despesas despesas = new Despesas();

            [XmlElement(ElementName = "outrasdespesas")]
            public List<OutrasDespesas> outrasdespesas = new List<OutrasDespesas>();

            [XmlElement(ElementName = "provisao")]
            public List<Provisao> provisao = new List<Provisao>();           

            //public Corretagem corretagem = new Corretagem();
        }

        public class Fundo
        {
            //public Fundo() { } 

            public HeaderFundo header = new HeaderFundo();

            [XmlElement(ElementName = "titpublico")]
            public List<TitPublico> titpublico = new List<TitPublico>();

            [XmlElement(ElementName = "titprivado")]
            public List<TitPrivado> titprivado = new List<TitPrivado>();

            [XmlElement(ElementName = "termorf")]
            public List<TermoRf> termorf = new List<TermoRf>();

            [XmlElement(ElementName = "debenture")]
            public List<Debenture> debenture = new List<Debenture>();

            [XmlElement(ElementName = "opcoesderiv")]
            public List<OpcaoDeriv> opcoesderiv = new List<OpcaoDeriv>();

            [XmlElement(ElementName = "acoes")]
            public List<Acoes> acoes = new List<Acoes>();
            
            [XmlElement(ElementName = "opcoesacoes")]
            public List<OpcoesAcoes> opcoesacoes = new List<OpcoesAcoes>();

            [XmlElement(ElementName = "termorv")]
            public List<TermoRv> termorv = new List<TermoRv>();

            [XmlElement(ElementName = "futuros")]
            public List<Futuros> futuros = new List<Futuros>();

            [XmlElement(ElementName = "swap")]
            public List<Swap> swap= new List<Swap>();

            public Caixa caixa = new Caixa();

            [XmlElement(ElementName = "cotas")]
            public List<Cotas> cotas = new List<Cotas>();

            public Despesas despesas = new Despesas();

            [XmlElement(ElementName = "outrasdespesas")]
            public List<OutrasDespesas> outrasdespesas = new List<OutrasDespesas>();

            [XmlElement(ElementName = "provisao")]
            public List<Provisao> provisao = new List<Provisao>();

            [XmlElement(ElementName = "participacoes")]
            public List<Participacao> participacoes = new List<Participacao>();


            //public Corretagem corretagem = new Corretagem();
        }

        public class HeaderFundo
        {

            public HeaderFundo() { }

            public HeaderFundo(params object[] list)
            {
            }

            public string isin = "";
            public string cnpj = "";
            public string nome = "";
            public string dtposicao = "";
            public string nomeadm = "";
            public string cnpjadm = "";
            public string nomegestor = "";
            public string cnpjgestor = "";
            public string nomecustodiante = "";
            public string cnpjcustodiante = "";
            public string valorcota = "0.000000000";
            public string quantidade = "0.000000000";
            public string patliq = "0.00";
            public string valorativos = "0.00";
            public string valorreceber = "0.00";
            public string valorpagar = "0.00";
            public string vlcotasemitir = "0.00";
            public string vlcotasresgatar = "0.00";
            public string codanbid = "";
            public string tipofundo = "";
            public string nivelrsc = "";
        }

        public class HeaderCarteira
        {

            public HeaderCarteira() { }

            public string cnpjcpf = "";
            public string codcart = "";
            public string dtposicao = "";
            public string nome = "";
            public string tpcli = "";
            public string codcntcor = "";
            public string nomegestor = "";
            public string cnpjgestor = "";
            public string nomecustodiante = "";
            public string cnpjcustodiante = "";
            public string patliq = "0.00";
            public string tributos = "0.00";
            public string valorativos = "0.00";
            public string valorreceber = "0.00";
            public string valorpagar = "0.00";
        }

        #region TitPublico
        public class TitPublico
        {

            public TitPublico() { }

            public string isin = "";
            public string codativo = "";
            public string cusip = "";
            public string dtemissao = "";
            public string dtoperacao = "";
            public string dtvencimento = "";
            public string qtdisponivel = "";
            public string qtgarantia = "";
            public string depgar = "";
            public string pucompra = "";
            public string puvencimento = "";
            public string puposicao = "";
            public string puemissao = "";
            public string principal = "";
            public string tributos = "";
            public string valorfindisp = "";
            public string valorfinemgar = "";
            public string coupom = "";
            public string indexador = "";
            public string percindex = "";
            public string caracteristica = "";
            public string percprovcred = "";
            public Compromisso compromisso;
            public string classeoperacao = "";
            public string idinternoativo = "";
            public string nivelrsc = "";
        }
        #endregion

        public class Compromisso
        {
            public Compromisso() { }

            public string dtretorno = "";
            public string puretorno = "";
            public string indexadorcomp = "";
            public string perindexcomp = "";
            public string txoperacao = "";
            public string classecomp = "";
        }

        #region TitPrivado
        public class TitPrivado
        {

            public TitPrivado() { }

            public string isin = "";
            public string codativo = "";
            public string cusip = "";
            public string dtemissao = "";
            public string dtoperacao = "";
            public string dtvencimento = "";
            public string cnpjemissor = "";
            public string qtdisponivel = "";
            public string qtgarantia = "";
            public string depgar = "";
            public string pucompra = "";
            public string puvencimento = "";
            public string puposicao = "";
            public string puemissao = "";
            public string principal = "";
            public string tributos = "";
            public string valorfindisp = "";
            public string valorfinemgar = "";
            public string coupom = "";
            public string indexador = "";
            public string percindex = "";
            public string caracteristica = "";
            public string percprovcred = "";
            public Compromisso compromisso;
            public string classeoperacao = "";
            public string idinternoativo = "";
            public string nivelrsc = "";
        }

        #endregion

        #region Debenture
        public class Debenture
        {

            public Debenture() { }

            public string isin = "";
            public string coddeb = "";
            public string debconv = "";
            public string debpartlucro = "";
            public string SPE = "";
            public string cusip = "";
            public string dtemissao = "";
            public string dtoperacao = "";
            public string dtvencimento = "";
            public string cnpjemissor = "";
            public string qtdisponivel = "";
            public string qtgarantia = "";
            public string depgar = "";
            public string pucompra = "";
            public string puvencimento = "";
            public string puposicao = "";
            public string puemissao = "";
            public string principal = "";
            public string tributos = "";
            public string valorfindisp = "";
            public string valorfinemgar = "";
            public string coupom = "";
            public string indexador = "";
            public string percindex = "";
            public string caracteristica = "";
            public string percprovcred = "";
            public Compromisso compromisso;
            public string classeoperacao = "";
            public string idinternoativo = "";
            public string nivelrsc = "";
        }
        #endregion

        #region termorf
        public class TermoRf 
        {

            public TermoRf() {}

            public string isin = "";
            public string dtvencativo = "";
            public string dtoper = "";
            public string dtvencimento = "";
            public string qtd = "";
            public string puoperacao = "";
            public string puvencimento = "";
            public string puposicao = "";
            public string puemissao = "";
            public string principal = "";
            public string valorfin = "";
            public string tributos = "";
            public string coupom = "";
            public string indexador = "";
            public string percindex = "";
            public string caracteristica = "";
            public string classeoperacao = "";
            public string hedge = "";
            public string tphedge = "";
            public string idinternoativo = "";
            public string nivelrsc = "";
        }
        #endregion


        #region OpcoesDeriv

        public class OpcaoDeriv
        {
            public OpcaoDeriv() { }

            public string isin = "";
            public string mercado = "";
            public string ativo = "";
            public string serie = "";
            public string callput = "";
            public string quantidade = "";
            public string valorfinanceiro = "";
            public string tributos = "";
            public string precoexercicio = "";
            public string premio = "";
            public string dtvencimento = "";
            public string classeoperacao = "";
            public string puposicao = "";
            public string hedge = "";
            public string tphedge = "";

        }

        #endregion

        public class Acoes
        {
            public Acoes() { }

            public string isin = "";
            public string cusip = "";
            public string codativo = "";
            public string qtdisponivel = "0.000000000";
            public string lote = "";
            public string qtgarantia = "0.000000000";
            public string valorfindisp = "0.00";
            public string valorfinemgar = "0.00";
            public string tributos = "0.00";
            public string puposicao = "0.00000000";
            public string percprovcred = "0.00";
            public string tpconta = "";
            public string classeoperacao = "";
            public string dtvencalug = "";
            public string txalug = "0.00";
            public string cnpjinter = "";
        }

        public class Participacao
        {
            public Participacao() { }

            public string cnpjpart = "";
            public string valorfinanceiro = "0.00";
        }

        public class OpcoesAcoes
        {

            public OpcoesAcoes() { }

            public string isin = "";
            public string cusip = "";
            public string codativo = "";
            public string ativobase = "";
            public string qtdisponivel = "0.000000000";
            public string valorfinanceiro = "0.00";
            public string precoexercicio = "0.00000000";
            public string dtvencimento = "";
            public string classeoperacao = "";
            public string tributos = "0.00";
            public string puposicao = "0.00000000";
            public string premio = "0.00";
            public string percprovcred = "0.00";
            public string tpconta = "";
            public string hedge = "";
            public string tphedge = "";
        }

        public class TermoRv
        {

            public TermoRv() { }

            public string isin = "";
            public string cusip = "";
            public string ativo = "";
            public string dtoperacao = "";
            public string dtvencimento = "";
            public string garantia = "";
            public string puposicao = "0.00000000";
            public string quantidade = "0.000000000";
            public string valorfinanceiro = "0.00";
            public string tributos = "0.00";
            public string puvenc = "0.00000000";
            public string tpconta = "";
            public string classeoperacao = "";
            public string hedge = "";
            public string tphedge = "";
        }

        public class Futuros
        {

            public Futuros() { }

            public string isin = "";
            public string ativo = "";
            public string cnpjcorretora = "";
            public string serie = "";
            public string quantidade = "0.000000000";
            public string vltotalpos = "0.00";
            public string tributos = "0.00";
            public string dtvencimento = "";
            public string vlajuste = "0.00";
            public string classeoperacao = "";
            public string hedge = "";
            public string tphedge = "";
        }

		public class Swap
        {
            public Swap() { }

            public string cetipbmf = "";
            public string isin = "";
            public string dtoperacao = "";
            public string dtregistro = "";
            public string dtvencimento = "";
            public string cnpjcontraparte = "";
            public string garantia = "";
            public string vlnotional = "0.00";
            public string tributos = "0.00";
            public string vlmercadoativo = "0.00";
            public string taxaativo = "0.0000";
            public string indexadorativo = "";
            public string percindexativo = "0.00";
            public string vlmercadopassivo = "0.00";
            public string taxapassivo = "0.0000";
            public string indexadorpassivo = "";
            public string percindexpassivo = "0.00";
            public string hedge = "";
            public string tphedge = "";
            public string idinternoativo = "";
        }

        public class Caixa
        {

            public Caixa() { }

            public string isininstituicao = "";
            public string tpconta = "";
            public string saldo = "0.00";
            public string nivelrsc = "";
        }

        public class Cotas
        {

            public Cotas() { }

            public string isin = "";
            public string cnpjfundo = "";
            public string qtdisponivel = "0.000000000";
            public string qtgarantia = "0.000000000";
            public string puposicao = "0.00000000";
            public string tributos = "0.00";
            public string nivelrsc = "";
        }

        public class Despesas
        {

            public Despesas() { }

            public string txadm = "0.00";
            public string tributos = "0.00";
            public string perctaxaadm = "0.00000000";
            public string txperf = "";
            public string vltxperf = "0.00";
            public string perctxperf = "0.00000000";
            public string percindex = "0.00000000";
            public string outtax = "0.00";
            public string indexador = "";
        }

        public class OutrasDespesas
        {

            public OutrasDespesas() { }

            public string coddesp = "";
            public string valor = "0.00";
        }

        public class Provisao
        {

            public Provisao() { }

            public string codprov = "";
            public string credeb = "";
            public string dt = "";
            public string valor = "0.00";
        }

        //public class Corretagem {

        //    public Corretagem() { }

        //    public string cnpjcorretora = "";
        //    public string tpcorretora = "";
        //    public string numope = "0";
        //    public string vlbov = "0.00";
        //    public string vlrepassebov = "0.00";
        //    public string vlbmf = "0.00";
        //    public string vlrepassebmf = "0.00";
        //    public string vloutbolsas = "0.00";
        //    public string vlrepasseoutbol = "0.00";
        //}              
        #endregion

        /// <summary>
        /// Retorna uma Stream de Mémoria de um arquivo xml. A Stream permanece aberta
        /// </summary>
        /// <param name="data"></param>
        /// <param name="idClientes">Lista de idClientes a executar</param>
        /// <param name="ms">Saida: Memory Stream do arquivo XML Anbid</param>
        /// <param name="nomeArquivo">Saida: Nome do Arquivo XML Anbid</param>        
        /// <returns></returns>
        public void ExportaCarteiraXMLAnbid(DateTime data, List<int> idClientes, out MemoryStream ms, out string nomeArquivo)
        {
            ms = new MemoryStream();
            nomeArquivo = this.DefineNomeArquivo(idClientes, data);

            ArquivoPosicao arq = new ArquivoPosicao();

            for (int i = 0; i < idClientes.Count; i++)
            {
                int idCliente = idClientes[i];

                #region Cliente
                Cliente cliente = new Cliente();
                List<esQueryItem> campos = new List<esQueryItem>();
                campos.Add(cliente.Query.IdCliente);
                campos.Add(cliente.Query.IdTipo);
                campos.Add(cliente.Query.DataDia);
                cliente.LoadByPrimaryKey(campos, idCliente);
                #endregion

                bool historico = cliente.DataDia.Value > data;

                if (cliente.IsTipoClienteFundo() || cliente.IsTipoClienteClube())
                {
                    /* Monta Fundo */
                    this.MontaFundo(data, idCliente, arq, historico);
                }
                else
                {
                    /* Monta Carteira */
                    this.MontaCarteira(data, idCliente, arq, historico);
                }
            }

            #region Serializer do Arquivo
            XmlSerializer x = new XmlSerializer(arq.GetType());

            XmlWriterSettings settings = new XmlWriterSettings();
            settings.Encoding = System.Text.Encoding.GetEncoding("WINDOWS-1252");
            settings.Indent = true;
            settings.IndentChars = "\t";
            settings.NewLineChars = Environment.NewLine;
            settings.ConformanceLevel = ConformanceLevel.Document;

            XmlSerializerNamespaces ns = new XmlSerializerNamespaces();
            ns.Add("", "");


            using (XmlWriter writer = XmlTextWriter.Create(ms, settings))
            {
                x.Serialize(writer, arq, ns);

            }
            #endregion
        }

        /// <summary>
        /* Define o Nome Do arquivo
         * Nome do Arquivo com apenas um Fundo: Nome do Arquivo com apenas um Fundo: O nome do
         * arquivo dever ser iniciado por "FD" + CNPJ do fundo, para facilitar a identificação do mesmo. + ( _ )
         * + Data da Posição, no formato (AAAAMMDD) + ( _ ) + data e hora de geração do arquivo no formato
         *(AAAAMMDDHHMMSS)+ ( _ ) após este o campo é livre.
         * Exemplo: O Fundo XPTO de CNPJ 12345678901234 de 12 de Agosto de 2003, gerado no dia 15 de
         * Agosto de 2003 às 15:30:10, o nome do arquivo será:
         * FD12345678901234_20030812_20030815153010_XPTO.XML.
         *
         * Nome do Arquivo com apenas uma Carteira: O nome do arquivo deve ser iniciado por "CT" +
         * código interno da carteira no informante, este código interno deve garantir a unicidade da carteira
         * para a Instituição informante e se encontra no campo Código da Carteira na seção Header, + ( _ ) +
         * a Data da Posição no formato (AAAAMMDD) + ( _ ) + data e hora de geração do arquivo no formato
         * (AAAAMMDDHHMMSS)+ ( _ ) após este o campo é livre. Exemplo: A Carteira CART-XPTO de
         * Código Interno 123456789012345 de 12 de Agosto de 2003, gerado no dia 15 de Agosto de 2003
         * às 15:30:10, o nome do arquivo será:
         * CT123456789012345_20030812_20050815153010_CART-XPTO.XML.
         * 
         * Nome do Arquivo com mais de um fundo ou com mais de uma carteira: O nome do arquivo
         * dever ser iniciado por LT (para identificar que é um arquivo de lote) + o CNPJ da Instituição que esta
         * enviando o Lote + ( _ ) + a Data da Posição no formato (AAAAMMDD) + ( _ ) + a quantidade de
         * fundos/carteiras presentes no arquivo + ( _ ) + data e hora de geração do arquivo no formato
         * (AAAAMMDDHHMMSS).
         * Exemplo: A instituição A de CNPJ 12345678901234 em 12 de Agosto de 2003 às 13:50 gera o
         * arquivo com a posição de 30 de julho de 2003 de 10 fundos, o nome do arquivo será:
         * LT12345678901234_20030730_10_20030812135000.XML
        */
        /// </summary>
        /// <param name="idClientes"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        private string DefineNomeArquivo(List<int> idClientes, DateTime data)
        {
            string nomeArquivo = "";
            string quantidade = idClientes.Count.ToString();

            // Apenas 1 Cliente
            if (idClientes.Count == 1)
            {

                #region 1 Node Apenas
                #region Cliente
                Cliente cliente = new Cliente();
                List<esQueryItem> campos = new List<esQueryItem>();
                campos.Add(cliente.Query.IdCliente);
                campos.Add(cliente.Query.IdTipo);
                campos.Add(cliente.Query.IdPessoa);
                cliente.LoadByPrimaryKey(campos, idClientes[0]);
                #endregion

                #region Carteira
                Financial.Fundo.Carteira c = new Financial.Fundo.Carteira();
                List<esQueryItem> campos1 = new List<esQueryItem>();
                campos1.Add(c.Query.IdCarteira);
                campos1.Add(c.Query.Nome);
                c.LoadByPrimaryKey(campos1, idClientes[0]);

                string nome = c.str.Nome.Replace(" ", "");
                string codigoCarteira = !String.IsNullOrEmpty(c.CodigoAnbid) ? c.CodigoAnbid : "";

                //AgenteMercado agenteMercado = new AgenteMercado();
                //agenteMercado.LoadByPrimaryKey(c.IdAgenteAdministrador.Value);

                //string cnpjFundo = !String.IsNullOrEmpty(agenteMercado.Cnpj) ? agenteMercado.Cnpj : "";
                #endregion

                Pessoa p = new Pessoa();
                p.LoadByPrimaryKey(cliente.IdPessoa.Value);
                string cnpj = p.Cpfcnpj.Trim().Replace(".", "").Replace("-", "").Replace("/", "");

                // Fundo
                if (cliente.IsTipoClienteFundo() || cliente.IsTipoClienteClube())
                {

                    nomeArquivo = "FD" + cnpj + "_" + data.ToString("yyyyMMdd") + "_" + DateTime.Now.ToString("yyyyMMddHHmmss") + "_" + nome + ".xml";
                }
                // Carteira
                else
                {
                    nomeArquivo = "CT" + codigoCarteira + "_" + data.ToString("yyyyMMdd") + "_" + DateTime.Now.ToString("yyyyMMddHHmmss") + "_CART-" + nome + ".xml";
                }
                #endregion
            }
            else
            {

                #region Carteira
                Financial.Fundo.Carteira c = new Financial.Fundo.Carteira();
                List<esQueryItem> campos1 = new List<esQueryItem>();
                campos1.Add(c.Query.IdCarteira);
                campos1.Add(c.Query.IdAgenteAdministrador);
                campos1.Add(c.Query.Nome);
                c.LoadByPrimaryKey(campos1, idClientes[0]);

                AgenteMercado agenteMercado = new AgenteMercado();
                agenteMercado.LoadByPrimaryKey(c.IdAgenteAdministrador.Value);

                string cnpjFundo = !String.IsNullOrEmpty(agenteMercado.Cnpj) ? agenteMercado.Cnpj : "";
                #endregion

                nomeArquivo = "LT" + cnpjFundo + "_" + data.ToString("yyyyMMdd") + "_" + quantidade + "_" + DateTime.Now.ToString("yyyyMMddHHmmss") + ".xml";
            }
            return nomeArquivo;
        }

        /// <summary>
        /// Monta Node Fundos
        /// </summary>
        /// <param name="data"></param>
        /// <param name="idCliente"></param>
        /// <param name="historico"></param>
        private void MontaFundo(DateTime data, int idCliente, ArquivoPosicao arq, bool historico)
        {
            Fundo f = new Fundo();
                      
            Cliente cliente = new Cliente();           
            cliente.LoadByPrimaryKey(idCliente);

            this.multiMoeda = cliente.MultiMoeda == "S";
            this.idMoedaCarteira = cliente.IdMoeda.Value;

            #region Header
            try
            {
                f.header = this.RetornaHeaderFundo(idCliente, data, historico);
            }
            catch (Exception e)
            {

                throw new Exception("Erro: node Header");
            }
            #endregion

            #region TitPublico
            try
            {
                f.titpublico.AddRange(this.RetornaTitulosPublicos(idCliente, data, historico));
            }
            catch (Exception e)
            {

                throw new Exception("Erro: node TitPublico");
            }
            #endregion

            #region TitPrivado
            try
            {
                f.titprivado.AddRange(this.RetornaTitulosPrivados(idCliente, data, historico));
            }
            catch (Exception e)
            {

                throw new Exception("Erro: node TitPrivado");
            }
            #endregion

            #region TermoRf
            try
            {
                f.termorf.AddRange(this.RetornaTermoRf(idCliente, data, historico));
            }
            catch (Exception e)
            {

                throw new Exception("Erro: node TermoRf");
            }
            #endregion

            #region Debenture
            try
            {
                f.debenture.AddRange(this.RetornaDebentures(idCliente, data, historico));
            }
            catch (Exception e)
            {

                throw new Exception("Erro: node Debenture");
            }
            #endregion

            #region opcoesDeriv
            try
            {
                f.opcoesderiv.AddRange(this.RetornaOpcoesDeriv(idCliente, data, historico));
            }
            catch (Exception e)
            {
                throw new Exception("Erro: node opcoesDeriv");
            }
            #endregion

            #region Acoes
            try
            {
                f.acoes.AddRange(this.RetornaAcoes(idCliente, data, historico));
            }
            catch (Exception e)
            {

                throw new Exception("Erro: node Acoes");
            }
            #endregion           

            #region OpcoesAcoes
            try
            {
                f.opcoesacoes.AddRange(this.RetornaOpcoes(idCliente, data, historico));
            }
            catch (Exception e)
            {

                throw new Exception("Erro: node Opcoes");
            }
            #endregion

            #region TermoRv
            try
            {
                f.termorv.AddRange(this.RetornaTermoRV(idCliente, data, historico));
            }
            catch (Exception e)
            {

                throw new Exception("Erro: node TermoRv");
            }
            #endregion

            #region Futuros
            try
            {
                f.futuros.AddRange(this.RetornaFuturos(idCliente, data, historico));
            }
            catch (Exception e)
            {

                throw new Exception("Erro: node Futuros");
            }
            #endregion

			#region Swap
            try
            {
                f.swap.AddRange(this.RetornaSwap(idCliente, data, historico));
            }
            catch (Exception e)
            {

                throw new Exception("Erro: node Swap");
            }
            #endregion

            #region Caixa
            try
            {
                f.caixa = this.RetornaCaixa(idCliente, data);
            }
            catch (Exception e)
            {

                throw new Exception("Erro: node Caixa");
            }
            #endregion

            #region Cotas
            try
            {
                f.cotas.AddRange(this.RetornaCotas(idCliente, data, historico));
            }
            catch (Exception e)
            {

                throw new Exception("Erro: node Cotas");
            }
            #endregion

            #region Despesas
            try
            {
                List<Despesas> d = this.RetornaDespesas(idCliente, data);
                if (d.Count > 0)
                {
                    f.despesas = d[0];
                }
            }
            catch (Exception e)
            {
                throw new Exception("Erro: node Despesas");
            }
            #endregion

            #region OutrasDespesas
            try
            {
                f.outrasdespesas.AddRange(this.RetornaOutrasDespesas(idCliente, data));
            }
            catch (Exception e)
            {

                throw new Exception("Erro: node OutrasDespesas");
            }
            #endregion

            #region Provisao
            try
            {
                f.provisao.AddRange(this.RetornaProvisoes(idCliente, data, historico));
            }
            catch (Exception e)
            {
                throw new Exception("Erro: Provisao. Detalhes: " + e.Message);
            }
            #endregion

            #region Corretagem
            #endregion

            #region Partipacoes
            try
            {
                f.participacoes.AddRange(this.RetornaAcoesParticipacoes(idCliente, data, historico));
            }
            catch (Exception e)
            {

                throw new Exception("Erro: node Participacoes");
            }
            try
            {
                f.participacoes.AddRange(this.RetornaDebenturesParticipacoes(idCliente, data, historico));
            }
            catch (Exception e)
            {

                throw new Exception("Erro: node Debentures Participacoes");
            }
            #endregion


            arq.fundo.Add(f);
        }

        /// <summary>
        /// Monta Node Carteira
        /// </summary>
        /// <param name="data"></param>
        /// <param name="idCliente"></param>
        /// <param name="arq"></param>
        /// <param name="historico"></param>
        private void MontaCarteira(DateTime data, int idCliente, ArquivoPosicao arq, bool historico)
        {
            Carteira c = new Carteira();

            Cliente cliente = new Cliente();
            cliente.LoadByPrimaryKey(idCliente);

            this.multiMoeda = cliente.MultiMoeda == "S";
            this.idMoedaCarteira = cliente.IdMoeda.Value;

            #region Header
            c.header = this.RetornaHeaderCarteira(idCliente, data, historico);
            #endregion

            #region TitPublico
            c.titpublico.AddRange(this.RetornaTitulosPublicos(idCliente, data, historico));
            #endregion

            #region TitPrivado
            c.titprivado.AddRange(this.RetornaTitulosPrivados(idCliente, data, historico));
            #endregion

            #region Debenture
            c.debenture.AddRange(this.RetornaDebentures(idCliente, data, historico));
            #endregion

            #region TermoRf
            c.termorf.AddRange(this.RetornaTermoRf(idCliente, data,historico));            
            #endregion


            #region Acoes
            c.acoes.AddRange(this.RetornaAcoes(idCliente, data, historico));
            #endregion

            #region OpcoesAcoes
            c.opcoesacoes.AddRange(this.RetornaOpcoes(idCliente, data, historico));
            #endregion

            #region TermoRv
            c.termorv.AddRange(this.RetornaTermoRV(idCliente, data, historico));
            #endregion

            #region opcoesDeriv
            try
            {
                c.opcoesderiv.AddRange(this.RetornaOpcoesDeriv(idCliente, data, historico));
            }
            catch (Exception e)
            {
                throw new Exception("Erro: node opcoesDeriv");
            }
            #endregion

            #region Futuros
            c.futuros.AddRange(this.RetornaFuturos(idCliente, data, historico));
            #endregion

			#region Swap
            c.swap.AddRange(this.RetornaSwap(idCliente, data, historico));
            #endregion

            #region Caixa
            c.caixa = this.RetornaCaixa(idCliente, data);
            #endregion

            #region Cotas
            c.cotas.AddRange(this.RetornaCotas(idCliente, data, historico));
            #endregion

            #region Despesas
            List<Despesas> d = this.RetornaDespesas(idCliente, data);
            if (d.Count > 0)
            {
                c.despesas = d[0];
            }
            #endregion

            #region OutrasDespesas
            c.outrasdespesas.AddRange(this.RetornaOutrasDespesas(idCliente, data));
            #endregion

            #region Provisao
            c.provisao.AddRange(this.RetornaProvisoes(idCliente, data, historico));
            #endregion

            #region Corretagem
            #endregion

            arq.carteira.Add(c);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="idIndice"></param>
        /// <returns></returns>
        private string RetornaIndice(short idIndice)
        {
            //INDICES NAO MAPEADOS
            //IEE	IEE
            //IGP	Índice de preços
            //IMA	Índice de Mercado Andima Geral
            //IMA-B5	Índice de Mercado Andima NTN-B até 5 anos
            //IMA-B5+	Índice de Mercado Andima NTN-B mais de 5 anos
            //IMA-C5	Índice de Mercado Andima NTN-C até 5 anos
            //IMA-C5+	Índice de Mercado Andima NTN-C mais de 5 anos
            //IPC	Índice de Preços ao Consumidor (IPC/FIPE)
            //ITE	ITEL
            //PIB	Cota de PIBB
            //SB1	Carteira de ações
            //SB2	Carteira de ações
            //TBF	Taxa Básica Financeira
            //

            switch (idIndice)
            {
                case ListaIndiceFixo.ANBID:
                    return "ANB";
                case ListaIndiceFixo.CDI:
                    return "DI1";
                case ListaIndiceFixo.PTAX_800VENDA:
                    return "DOL";
                case ListaIndiceFixo.IBRX50_FECHA:
                    return "I50";
                case ListaIndiceFixo.IPCA:
                    return "IAP";
                case ListaIndiceFixo.IBRX_FECHA:
                    return "IBX";
                case ListaIndiceFixo.IGPDI:
                    return "IGD";
                case ListaIndiceFixo.IGPM:
                    return "IGM";
                case ListaIndiceFixo.IMA_B:
                    return "IMA-B";
                case ListaIndiceFixo.IMA_C:
                    return "IMA-C";
                case ListaIndiceFixo.IMA_S:
                    return "IMA-S";
                case ListaIndiceFixo.IBOVESPA_FECHA:
                    return "IND";
                case ListaIndiceFixo.INPC:
                    return "INP";
                case ListaIndiceFixo.IRFM:
                    return "IRF";
                case ListaIndiceFixo.YENE:
                    return "JPY";
                case ListaIndiceFixo.OUROBMF_FECHA:
                    return "OZ1";
                case ListaIndiceFixo.EURO:
                    return "REU";
                case ListaIndiceFixo.SELIC:
                    return "SEL";
                case ListaIndiceFixo.TJLP:
                    return "TJL";
                case ListaIndiceFixo.TR:
                    return "TR";
                case ListaIndiceFixo.PTAX_800VENDA_1130:
                    return "DOL";
                // case ListaIndiceFixo.INCC:
                // return "I";
                default:
                    //throw new Exception("IdIndice não suportado: " + idIndice);
                    return "";
            }
        }
    }
}