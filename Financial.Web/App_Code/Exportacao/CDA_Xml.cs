﻿using System.Xml.Serialization;
using System;
using System.Collections.Generic;

namespace Financial.Export {    
    
    /// <summary>
    /// Classes que Reproduzem as Estruturas do Arquivo XML CDA
    /// </summary>

    #region CDA_ XML
    [XmlRootAttribute(ElementName = "DOC_ARQ", IsNullable = false, Namespace = "urn:cda")]
    public class CDA_Xml {
        // Construtor
        public CDA_Xml() { }

        [XmlElementAttribute("CAB_INFORM", typeof(Cabecalho))]
        [XmlElementAttribute("LISTA_INFORM", typeof(Corpo))]
        public object[] itemsField = new object[2] { new Cabecalho(), new Corpo() };

        [XmlIgnore]
        public Cabecalho getCabecalho {
            get { return (Cabecalho)itemsField[0]; }
            set { itemsField[0] = (Cabecalho)value; }
        }

        [XmlIgnore]
        public Corpo getCorpo {
            get { return (Corpo)itemsField[1]; }
            set { itemsField[1] = (Corpo)value; }
        }
    }

    public class Cabecalho {
        // Construtor
        public Cabecalho() { }

        [XmlElement("COD_DOC")]
        public int codigoDocumento;

        [XmlElement("VERSAO")]
        public string versao;

        #region DT_COMPT
        [XmlIgnore]
        public DateTime dataCompetencia;

        [XmlElement("DT_COMPT")]
        public string formatDataCompetencia {
            get { return this.dataCompetencia.ToString("MM/yyyy"); }
            set { dataCompetencia = DateTime.Parse(value); }
        }
        #endregion

        #region DT_GERAC_ARQ
        [XmlIgnore]
        public DateTime dataGeracaoArquivo;

        [XmlElement("DT_GERAC_ARQ")]
        public string formatDataGeracaoArquivo {
            get { return this.dataGeracaoArquivo.ToString("dd/MM/yyyy"); }
            set { dataGeracaoArquivo = DateTime.Parse(value); }
        }
        #endregion
    }

    public class Corpo {
        // Construtor
        public Corpo() { }

        [XmlElementAttribute("INFORM")]
        public Inform inform = new Inform();

        [XmlIgnore]
        public Inform getInform {
            get { return inform; }
            set { inform = value; }
        }
    }

    public class Inform {
        // Construtor
        public Inform() { }

        [XmlElement("CNPJ_FDO", Order = 1)]
        public string cnpj;

        [XmlIgnore]
        public decimal valorPL;

        #region VL_PL
        [XmlElement("VL_PL", Order = 2)]
        public string formatValorPL {

            get {
                decimal valor = this.valorPL;
                return valor.ToString("N2").Replace(".", ""); 
            }
            set {
                decimal valorPL = 0;

                if (Decimal.TryParse(value, out valorPL))
                    valorPL = valorPL;
            }
        }
        #endregion

        [XmlElementAttribute("LISTA_ATIV", Order = 3)]
        public ListAtiv listAtiv = new ListAtiv();

        [XmlIgnore]
        public ListAtiv getListAtiv {
            get { return listAtiv; }
            set { listAtiv = value; }
        }
    }

    public class ListAtiv {
        // Construtor
        public ListAtiv() { }

        #region TituloPublico
        [XmlElementAttribute("TIT_PUB", Order = 1)]
        public List<TituloPublico> tituloPublico = new List<TituloPublico>();

        [XmlIgnore]
        public List<TituloPublico> getTituloPublico {
            get { return tituloPublico; }
            set { tituloPublico = value; }
        }
        #endregion

        #region Cotas
        [XmlElementAttribute("COTAS", Order = 2)]
        public List<Cotas> cotas = new List<Cotas>();

        [XmlIgnore]
        public List<Cotas> getCotas {
            get { return cotas; }
            set { cotas = value; }
        }
        #endregion

        #region Swap
        [XmlElementAttribute("SWAP", Order = 3)]
        public List<Swap> swap = new List<Swap>();

        [XmlIgnore]
        public List<Swap> getSwap {
            get { return swap; }
            set { swap = value; }
        }
        #endregion

        #region DEMAIS_CODIF
        [XmlElementAttribute("DEMAIS_CODIF", Order = 4)]
        public List<Demais_Codif> demais_Codif = new List<Demais_Codif>();

        [XmlIgnore]
        public List<Demais_Codif> getDemais_Codif {
            get { return demais_Codif; }
            set { demais_Codif = value; }
        }
        #endregion

        #region DEP_PRAZO_TIT_IF
        [XmlElementAttribute("DEP_PRAZO_TIT_IF", Order = 5)]
        public List<Dep_Prazo_Tit> dep_Prazo_Tit = new List<Dep_Prazo_Tit>();

        [XmlIgnore]
        public List<Dep_Prazo_Tit> getDep_Prazo_Tit {
            get { return dep_Prazo_Tit; }
            set { dep_Prazo_Tit = value; }
        }
        #endregion

        #region TIT_AGRO_CRED_PRIV
        [XmlElementAttribute("TIT_AGRO_CRED_PRIV", Order = 6)]
        public List<TituloAgronegocioCreditoPrivado> tituloAgronegocioCreditoPrivado = new List<TituloAgronegocioCreditoPrivado>();

        [XmlIgnore]
        public List<TituloAgronegocioCreditoPrivado> getTituloAgronegocioCreditoPrivado {
            get { return tituloAgronegocioCreditoPrivado; }
            set { tituloAgronegocioCreditoPrivado = value; }
        }
        #endregion

        #region INV_EXT
        [XmlElementAttribute("INV_EXT", Order = 7)]
        public List<InvestimentoExterior> investimentoExterior = new List<InvestimentoExterior>();

        [XmlIgnore]
        public List<InvestimentoExterior> getInvestimentoExterior {
            get { return investimentoExterior; }
            set { investimentoExterior = value; }
        }
        #endregion

        #region DEMAIS_N_CODIF
        [XmlElementAttribute("DEMAIS_N_CODIF", Order = 8)]
        public List<Demais_N_Codif> demais_N_Codif = new List<Demais_N_Codif>();

        [XmlIgnore]
        public List<Demais_N_Codif> getDemais_N_Codif {
            get { return demais_N_Codif; }
            set { demais_N_Codif = value; }
        }
        #endregion
    }

    /***********************************************************/
    /* Sessão Principal em Lista Atividade --------------------*/
    public class TituloPublico {
        // Construtor
        public TituloPublico() { }

        [XmlElement("COD_SELIC", Order=1)]
        public string codigoSelic;

        #region DT_COMPT
        [XmlIgnore]
        public DateTime dataVencimento;

        [XmlElement("DT_VENC", Order = 2)]
        public string formatDataVencimento {
            get { return this.dataVencimento.ToString("dd/MM/yyyy"); }
            set { dataVencimento = DateTime.Parse(value); }
        }
        #endregion

        [XmlElementAttribute("APLIC", Order = 3)]
        public Aplic aplic = new Aplic();

        [XmlIgnore]
        public Aplic getAplic {
            get { return aplic; }
            set { aplic = value; }
        }
    }

    public class Cotas {
        // Construtor
        public Cotas() { }

        [XmlElement("CNPJ_FDO", Order=1)]
        public string cnpj;

        [XmlElementAttribute("APLIC", Order = 2)]
        public Aplic aplic = new Aplic();

        [XmlIgnore]
        public Aplic getAplic {
            get { return aplic; }
            set { aplic = value; }
        }
    }

    public class Swap {
        // Construtor
        public Swap() {
            this.aplic = new Aplic(this);
        }

        [XmlElement("COD_SWAP", Order = 1)]
        public string codigoSwap;

        [XmlElementAttribute("APLIC", Order = 2)]
        public Aplic aplic;

        [XmlIgnore]
        public Aplic getAplic {
            get { return aplic; }
            set { aplic = value; }
        }
    }

    public class Demais_Codif {
        // Construtor
        public Demais_Codif() { }

        [XmlElement("COD_ATIV", Order = 1)]
        public string codigoAtivo;

        [XmlElement("COD_TP_ATIV", Order = 2)]
        public string cod_Tp_Ativ;

        #region DT_INI_VIGEN
        [XmlIgnore]
        public DateTime dataInicioVigencia;

        [XmlElement("DT_INI_VIGEN", Order = 3)]
        public string formatDataVigencia {
            get { return this.dataInicioVigencia.ToString("dd/MM/yyyy"); }
            set { dataInicioVigencia = DateTime.Parse(value); }
        }
        #endregion

        [XmlElementAttribute("APLIC", Order=4)]
        public Aplic aplic = new Aplic();

        [XmlIgnore]
        public Aplic getAplic {
            get { return aplic; }
            set { aplic = value; }
        }
    }

    public class Dep_Prazo_Tit {
        // Construtor
        public Dep_Prazo_Tit() { }

        [XmlElement("COD_TP_ATIV", Order = 1)]
        public string cod_Tp_Ativ;

        [XmlElementAttribute("EMIS", Order = 2)]
        public Emis_DEP_PRAZO_TIT emis = new Emis_DEP_PRAZO_TIT();

        [XmlIgnore]
        public Emis_DEP_PRAZO_TIT getEmis {
            get { return emis; }
            set { emis = value; }
        }

        #region DT_VENC
        [XmlIgnore]
        public DateTime dataVencimento;

        [XmlElement("DT_VENC", Order = 3)]
        public string formatDataVencimento {
            get { return this.dataVencimento.ToString("dd/MM/yyyy"); }
            set { dataVencimento = DateTime.Parse(value); }
        }
        #endregion

        [XmlElement("ST_TIT_POS_FIX", Order = 4)]
        public string tipoTitulo;

        [XmlElementAttribute("TIT_POS_FIX_INDEX", Order = 5)]
        public Tit_Pos_Fix_Index titPosFixIndex = new Tit_Pos_Fix_Index();

        [XmlIgnore]
        public Tit_Pos_Fix_Index getTit_Pos_Fix_Index {
            get { return titPosFixIndex; }
            set { titPosFixIndex = value; }
        }

        [XmlIgnore]
        public decimal taxaTitulo;
       
        #region TIT_PRE_FIX_TAXA_CONTR
        [XmlElement("TIT_PRE_FIX_TAXA_CONTR", Order = 6)]
        public string formatTaxaTitulo {

            get {
                return taxaTitulo.ToString("N2");
            }
            set {
                decimal taxaTitulo = 0;

                if (Decimal.TryParse(value, out taxaTitulo))
                    taxaTitulo = taxaTitulo;
            }
        }
        #endregion

        [XmlElement("ST_CLASSIF_RISCO", Order = 7)]
        public string classificacaoRisco;

        [XmlElementAttribute("CLASSIF_RISCO", Order = 8)]
        public Classif_Risco classifRisco = new Classif_Risco();

        [XmlIgnore]
        public Classif_Risco getClassif_Risco {
            get { return classifRisco; }
            set { classifRisco = value; }
        }

        [XmlElementAttribute("APLIC", Order = 9)]
        public Aplic aplic = new Aplic();

        [XmlIgnore]
        public Aplic getAplic {
            get { return aplic; }
            set { aplic = value; }
        }
    }

    public class TituloAgronegocioCreditoPrivado {
        // Construtor
        public TituloAgronegocioCreditoPrivado() { }

        [XmlElement("COD_TP_ATIV", Order = 1)]
        public string cod_Tp_Ativ;

        [XmlElementAttribute("EMIS", Order = 2)]
        public Emis emis = new Emis();

        [XmlIgnore]
        public Emis getEmis {
            get { return emis; }
            set { emis = value; }
        }

        #region DT_VENC
        [XmlIgnore]
        public DateTime dataVencimento;

        [XmlElement("DT_VENC", Order = 3)]
        public string formatDataVencimento {
            get { return this.dataVencimento.ToString("dd/MM/yyyy"); }
            set { dataVencimento = DateTime.Parse(value); }
        }
        #endregion

        [XmlElement("ST_TIT_POS_FIX", Order = 4)]
        public string tipoTitulo;

        [XmlElementAttribute("TIT_POS_FIX_INDEX", Order = 5)]
        public Tit_Pos_Fix_Index titPosFixIndex = new Tit_Pos_Fix_Index();

        [XmlIgnore]
        public Tit_Pos_Fix_Index getTit_Pos_Fix_Index {
            get { return titPosFixIndex; }
            set { titPosFixIndex = value; }
        }
       
        #region TaxaTitulo
        [XmlIgnore]
        public decimal? taxaTitulo;

        [XmlElement("TIT_PRE_FIX_TAXA_CONTR", Order = 6)]
        public string formatTaxaTitulo
        {
            get
            {
                return this.taxaTitulo.HasValue ? this.taxaTitulo.Value.ToString("N6").Replace(".", "") : String.Empty;
            }
            set
            {
                if (!this.taxaTitulo.HasValue)
                {
                    formatTaxaTitulo = String.Empty;
                }
                else
                {
                    decimal taxaTitulo;

                    if (Decimal.TryParse(value, out taxaTitulo))
                        taxaTitulo = taxaTitulo;
                }
            }
        }
        #endregion

        [XmlElement("ST_TIT_REG_CETIP", Order = 7)]
        public string IstituloRegistradoCetip;

        [XmlElement("ST_TIT_GARANT_SEGURO", Order = 8)]
        public string IstituloGarantiaSeguro;

        [XmlElement("CNPJ_INSTIT_FINANC", Order = 9)]
        public string cnpjInstituicao;

        [XmlElementAttribute("APLIC", Order = 10)]
        public Aplic aplic = new Aplic();

        [XmlIgnore]
        public Aplic getAplic {
            get { return aplic; }
            set { aplic = value; }
        }
    }

    public class InvestimentoExterior {
        // Construtor
        public InvestimentoExterior() { }

        [XmlElement("COD_TP_ATIV", Order = 1)]
        public string cod_Tp_Ativ;

        [XmlElement("INV_COLETIVO", Order = 2)]
        public string InvColetivo { get; set; }

        [XmlElement("GEST_INV_COLETIVO", Order = 3)]
        public string GestInvColetivo { get; set; }

        [XmlElement("NM_EMIS", Order = 4)]
        public string nomeEmissor;

        #region DT_VENC
        [XmlIgnore]
        public DateTime? dataVencimento;

        [XmlElement("DT_VENC", Order = 5)]
        public string formatDataVencimento {
            get {
                return this.dataVencimento.HasValue ? this.dataVencimento.Value.ToString("dd/MM/yyyy") : String.Empty;                                            
            }
            set {
                if (!this.dataVencimento.HasValue) {
                    formatDataVencimento = String.Empty;
                }
                else {
                    dataVencimento = DateTime.Parse(value);
                }            
            }
        }
        #endregion

        [XmlElement("COD_PAIS", Order = 6)]
        public string codigoPais;

        [XmlElement("COD_BV_MERC", Order = 7)]
        public string codigoBolsa;

        [XmlElement("COD_ATIVO_BV_MERC", Order = 8)]
        public string codigoAtivo;

        [XmlElement("ST_CLASSIF_RISCO", Order = 9)]
        public string classificacaoRisco;

        [XmlElementAttribute("CLASSIF_RISCO", Order = 10)]
        public Classif_Risco classifRisco = new Classif_Risco();

        [XmlIgnore]
        public Classif_Risco getClassif_Risco {
            get { return classifRisco; }
            set { classifRisco = value; }
        }

        [XmlElementAttribute("APLIC", Order = 11)]
        public Aplic aplic = new Aplic();

        [XmlIgnore]
        public Aplic getAplic {
            get { return aplic; }
            set { aplic = value; }
        }
    }

    public class Demais_N_Codif {
        // Construtor
        public Demais_N_Codif() { }

        [XmlElement("COD_TP_ATIV", Order=1)]
        public string cod_Tp_Ativ;

        [XmlElement("DESC", Order=2)]
        public string descricao;

        [XmlElementAttribute("EMIS", Order=3)]
        public Emis emis = new Emis();
        
        [XmlIgnore]
        public Emis getEmis {
            get { return emis; }
            set { emis = value; }
        }

        [XmlElementAttribute("APLIC", Order = 4)]
        public Aplic aplic = new Aplic();

        [XmlIgnore]
        public Aplic getAplic {
            get { return aplic; }
            set { aplic = value; }
        }
    }

    /* -------------------------------------------------------*/

    /***********************************************************/
    /* Sessões Complementares --------------------*/
    public class Emis {
        // Construtor
        public Emis() { }

        [XmlElement("TP_PF_PJ_EMIS")]
        public string tp_PF_PJ;

        [XmlElement("NR_PF_PJ_EMIS")]
        public string nr_PF_PJ;

        [XmlElement("NM_EMIS")]
        public string nm;
    }

    public class Emis_DEP_PRAZO_TIT {
        // Construtor
        public Emis_DEP_PRAZO_TIT() { }

        [XmlElement("CNPJ_EMIS")]
        public string cnpj;

        [XmlElement("NM_EMIS")]
        public string nm;
    }

    public class Aplic {
        // Construtor
        public Aplic() {
            aplic_Pos_Fim = new Aplic_Pos_Fim();
        }

        public Aplic(Swap s) { // Construtor de Aplic para Swap - Alguns campos são nulos
            this.aplicNegoc = null; // Não possui Node negoc
            //this.getAplicPosFim.qtde_Pos_Fim = null; // Não possui QTDE_POS_FIM
            aplic_Pos_Fim = new Aplic_Pos_Fim(s);
        }

        [XmlElement("COD_TP_APLIC")]
        public string cod_Tp_Aplic;

        [XmlElement("EMPR_LIGADA")]
        public string empr_Ligada;

        [XmlElement("QTDE_DIAS_CONFID")]
        public string qtde_Dias_Confid;

        [XmlElementAttribute("NEGOC")]
        public AplicNegoc aplicNegoc = new AplicNegoc();

        [XmlIgnore]
        public AplicNegoc getAplicNegoc {
            get { return aplicNegoc; }
            set { aplicNegoc = value; }
        }

        [XmlElementAttribute("POS_FIM")]
        public Aplic_Pos_Fim aplic_Pos_Fim;

        [XmlElementAttribute("ATIVO_EXT")]
        public AtivoExt AtivoExt { get; set; }

        [XmlIgnore]
        public Aplic_Pos_Fim getAplicPosFim {
            get { return aplic_Pos_Fim; }
            set { aplic_Pos_Fim = value; }
        }
    }

    /// <summary>
    /// AtivoExt
    /// </summary>
    public class AtivoExt
    {
        /// <summary>
        /// Gets or sets the desc at ext.
        /// </summary>
        /// <value>
        /// The desc at ext.
        /// </value>
        [XmlElementAttribute("DESC_AT_EXT")]
        public string DescAtExt { get; set; }

        /// <summary>
        /// Gets or sets the qtde at ext.
        /// </summary>
        /// <value>
        /// The qtde at ext.
        /// </value>
        [XmlElementAttribute("QTDE_AT_EXT")]
        public decimal? QtdeAtExt { get; set; }

        /// <summary>
        /// Gets or sets the valor at ext.
        /// </summary>
        /// <value>
        /// The valor at ext.
        /// </value>
        [XmlElementAttribute("VALOR_AT_EXT")]
        public decimal? ValorAtExt { get; set; }
    }

    public class AplicNegoc {
        private bool serializaTipoNegocio = true;

        // Construtor
        public AplicNegoc() { }

        public void SerializaTipoNegocio(bool serializa)
        {
            serializaTipoNegocio = serializa;
        }

        [XmlElement("TP_NEGOC", Order = 1)]
        public string tipoNegocio;

        /* ShouldSerialize + property  - Chamado automaticamente pelo Framework */
        public virtual bool ShouldSerializetipoNegocio()
        {
            return serializaTipoNegocio;
        }

        [XmlIgnore]
        public decimal? quantidadeVendasNegocio;

        #region QTDE_VENDAS_NEGOC
        [XmlElement("QTDE_VENDAS_NEGOC", Order = 2)]
        public string formatQuantidadeVendasNegocio {
            get {
                decimal valor = 0M;
                if (this.quantidadeVendasNegocio.HasValue) {
                    valor = Math.Abs(this.quantidadeVendasNegocio.Value);
                }

                return this.quantidadeVendasNegocio.HasValue ? valor.ToString("N6").Replace(".", "") : String.Empty;
            }
            set {
                if (!this.quantidadeVendasNegocio.HasValue) {
                    formatQuantidadeVendasNegocio = String.Empty;
                }
                else {
                    decimal quantidadeVendasNegocio;

                    if (Decimal.TryParse(value, out quantidadeVendasNegocio))
                        quantidadeVendasNegocio = quantidadeVendasNegocio;
                }
            }
        }
        #endregion

        [XmlIgnore]
        public decimal? valorVendasNegocio;

        #region VL_VENDAS_NEGOC
        [XmlElement("VL_VENDAS_NEGOC", Order = 3)]
        public string formatValorVendasNegocio {
            get {
                decimal valor = 0M;
                if (this.valorVendasNegocio.HasValue) {
                    valor = Math.Abs(this.valorVendasNegocio.Value);
                }

                return this.valorVendasNegocio.HasValue ? valor.ToString("N2").Replace(".", "") : String.Empty;
            }
            set {
                if (!this.valorVendasNegocio.HasValue) {
                    formatValorVendasNegocio = String.Empty;
                }
                else {
                    decimal valorVendasNegocio;

                    if (Decimal.TryParse(value, out valorVendasNegocio))
                        valorVendasNegocio = valorVendasNegocio;
                }
            }
        }
        #endregion

        [XmlIgnore]
        public decimal? quantidadeAquisicaoNegocio;

        #region QTDE_AQUIS_NEGOC
        [XmlElement("QTDE_AQUIS_NEGOC", Order = 4)]
        public string formatQuantidadeAquisicaoNegocio {
            get {
                decimal valor = 0M;
                if (this.quantidadeAquisicaoNegocio.HasValue) {
                    valor = Math.Abs(this.quantidadeAquisicaoNegocio.Value);
                }

                return this.quantidadeAquisicaoNegocio.HasValue ? valor.ToString("N6").Replace(".", "") : String.Empty;
            }
            set {
                if (!this.quantidadeAquisicaoNegocio.HasValue) {
                    formatQuantidadeAquisicaoNegocio = String.Empty;
                }
                else {
                    decimal quantidadeAquisicaoNegocio;

                    if (Decimal.TryParse(value, out quantidadeAquisicaoNegocio))
                        quantidadeAquisicaoNegocio = quantidadeAquisicaoNegocio;
                }
            }
        }
        #endregion

        [XmlIgnore]
        public decimal? valorAquisicaoNegocio;

        #region VL_AQUIS_NEGOC
        [XmlElement("VL_AQUIS_NEGOC", Order = 5)]
        public string formatValorAquisicaoNegocio {
            get {
                decimal valor = 0M;
                if (this.valorAquisicaoNegocio.HasValue) {
                    valor = Math.Abs(this.valorAquisicaoNegocio.Value);
                }

                return this.valorAquisicaoNegocio.HasValue ? valor.ToString("N2").Replace(".", "") : String.Empty;
            }
            set {
                if (!this.valorAquisicaoNegocio.HasValue) {
                    formatValorAquisicaoNegocio = String.Empty;
                }
                else {
                    decimal valorAquisicaoNegocio;

                    if (Decimal.TryParse(value, out valorAquisicaoNegocio))
                        valorAquisicaoNegocio = valorAquisicaoNegocio;
                }
            }
        }
        #endregion
    }

    public class Aplic_Pos_Fim {
        // Indica se deve serelizar a propertie format_Qtde_Pos_Fim
        private bool serializa = true;
        private bool cda3 = false;

        // Construtor
        public Aplic_Pos_Fim() {
        }

        public Aplic_Pos_Fim(Swap s) { // Chamador é Node Swap
            serializa = false;
        } 

        public void VersaoCDA(string versao) {
            if (versao == "3")
            {
                cda3 = true;
            }
            else
            {
                cda3 = false;
            }
        }
        
        [XmlIgnore]
        public decimal? qtde_Pos_Fim;

        #region QTDE_POS_FIM
        [XmlElement("QTDE_POS_FIM", Order = 1)]
        public string format_Qtde_Pos_Fim {

            get {
                decimal valor = 0M;
                if (this.qtde_Pos_Fim.HasValue) {
                    valor = Math.Abs(this.qtde_Pos_Fim.Value);
                }

                return this.qtde_Pos_Fim.HasValue ? valor.ToString("N6").Replace(".", "") : String.Empty;
            }
            set {
                if (!this.qtde_Pos_Fim.HasValue) {
                    format_Qtde_Pos_Fim = String.Empty;
                }
                else {
                    decimal qtde_Pos_Fim;

                    if (Decimal.TryParse(value, out qtde_Pos_Fim))
                        qtde_Pos_Fim = qtde_Pos_Fim;
                }
            }
        }

        /* ShouldSerialize + property  - Chamado automaticamente pelo Framework */
        public virtual bool ShouldSerializeformat_Qtde_Pos_Fim() { 
            // Se for Swap Não Serealiza - Não existe esse campo quando é Swap
            return serializa; 
        }

        #endregion


        [XmlIgnore]
        public decimal? custo_Correc_Pos_Fim;

        #region CUSTO_CORREC_POS_FIM
        [XmlElement("CUSTO_CORREC_POS_FIM", Order = 2)]
        public string format_Custo_Correc_Pos_Fim
        {

            get
            {
                decimal valor = 0M;
                if (this.custo_Correc_Pos_Fim.HasValue)
                {
                    valor = Math.Abs(this.custo_Correc_Pos_Fim.Value);
                }

                return this.custo_Correc_Pos_Fim.HasValue ? valor.ToString("N2").Replace(".", "") : String.Empty;
            }
            set
            {
                if (!this.custo_Correc_Pos_Fim.HasValue)
                {
                    format_Custo_Correc_Pos_Fim = String.Empty;
                }
                else
                {
                    decimal custo_Correc_Pos_Fim;

                    if (Decimal.TryParse(value, out custo_Correc_Pos_Fim))
                        custo_Correc_Pos_Fim = custo_Correc_Pos_Fim;
                }
            }
        }

        /* ShouldSerialize + property  - Chamado automaticamente pelo Framework */
        public virtual bool ShouldSerializeformat_Custo_Correc_Pos_Fim()
        {
            // Se for CDA3 Não Serealiza - Não existe esse campo quando é CDA3
            return !cda3;
        }

        #endregion

        [XmlIgnore]
        public decimal merc_Pos_Fim;

        #region MERC_POS_FIM
        [XmlElement("MERC_POS_FIM", Order = 3)]
        public string format_Merc_Pos_Fim {

            get {
                decimal valor = Math.Abs(this.merc_Pos_Fim);
                return valor.ToString("N2").Replace(".", "");
            }
            set {
                decimal merc_Pos_Fim = 0;

                if (Decimal.TryParse(value, out merc_Pos_Fim))
                    merc_Pos_Fim = merc_Pos_Fim;
            }
        }
        #endregion
    }

    public class Tit_Pos_Fix_Index {
        // Construtor
        public Tit_Pos_Fix_Index() { }

        [XmlElement("TIT_POS_FIX_COD_INDEX")]
        public string codigoIndexador;

        [XmlElement("TIT_POS_FIX_PR_INDEX")]
        public string porcentagemIndexador;

        [XmlElement("TIT_POS_FIX_CUPOM")]
        public string cupom;
    }

    public class Classif_Risco {
        // Construtor
        public Classif_Risco() { }

        [XmlElement("AG_CLASSIF_RISCO")]
        public string agenciaClassificadoraRisco;

        #region DT_CLASSIF_RISCO
        [XmlIgnore]
        public DateTime? dataClassificacaoRisco;

        [XmlElement("DT_CLASSIF_RISCO")]
        public string formatDataClassificacaoRisco {
            get { 
                return this.dataClassificacaoRisco.HasValue ? this.dataClassificacaoRisco.Value.ToString("dd/MM/yyyy") : String.Empty;
            }
            set {
                if (!this.dataClassificacaoRisco.HasValue) {
                    formatDataClassificacaoRisco = String.Empty;
                }
                else {
                    dataClassificacaoRisco = DateTime.Parse(value);
                }            
            }
        }
        #endregion

        [XmlIgnore]
        public int? grauRisco;

        #region GRAU_CLASSIF_RISCO
        [XmlElement("GRAU_CLASSIF_RISCO")]
        public string format_GRAU_CLASSIF_RISCO {

            get {
                return this.grauRisco.HasValue ? grauRisco.Value.ToString("N1") : String.Empty;
            }
            set {
                if (!this.grauRisco.HasValue) {
                    format_GRAU_CLASSIF_RISCO = String.Empty;
                }
                else {
                    grauRisco = Int32.Parse(value);
                }
            }
        }
        #endregion

    }

    #endregion
}