﻿using System.Collections.Generic;
using System;
using Financial.Util;
using System.IO;
using Financial.Fundo;
using Financial.Common;
using Financial.InvestidorCotista;
using System.Text;
using Financial.Investidor;
using Financial.Investidor.Enums;
using Financial.Bolsa;
using Financial.Bolsa.Enums;
using Financial.CRM;
using FileHelpers;
using System.Collections;
using Financial.Fundo.Enums;
using Financial.BMF;
using Financial.BMF.Enums;
using Financial.RendaFixa;
using Financial.ContaCorrente;

namespace Financial.Export {

    /// <summary>
    ///  Classe para fazer as Exportações Picl
    /// </summary>
    public class Picl 
    {
        private int numeroRegistros;

        #region Enums

        public enum PessoaVinculada {
            NaoVinculada = 0,
            Vinculada = 1
        }

        public enum CustodiaBVMF {
            Nao = 0,
            Sim = 1
        }

        public enum TipoOpcao {
            [StringValue("C")]
            Call = 0,

            [StringValue("P")]
            Put = 1  
        }

        public enum IndicadorPosicaoOpcoes {
            [StringValue("T")]
            Titular = 0,

            [StringValue("L")]
            Lancador = 1
        }

        public enum IndicadorPosicaoTermo {
            [StringValue("C")]
            Comprado = 0,

            [StringValue("V")]
            Vendido = 1
        }

        public enum OpcaoCotacao {
            Nao = 0,
            Sim = 1            
        }

        public enum IndicadorPosicaoEmprestimoAcoes {
            [StringValue("D")]
            Doador = 0,

            [StringValue("T")]
            Tomador = 1
        }

        public enum IndicadorPosicaoFuturo {
            [StringValue("C")]
            Comprado = 0,

            [StringValue("V")]
            Vendido = 1
        }

        public enum TipoFundoInvestimento {
            CurtoPrazo = 01,
            RendaFixa = 02,
            Referenciados = 03,
        }

        #endregion

        #region Header
        /*
            01 – TIPO DE REGISTRO (*)
            02 – CÓDIGO DO ARQUIVO (*)
            03 – CÓDIGO DO USUÁRIO NA BVMF (*)
            04 – CÓDIGO DE ORIGEM (*)
            05 – CÓDIGO DE DESTINO (*)
            06 – NÚMERO DO MOVIMENTO (*)
            07 – MÊS DE REFERÊNCIA (*)
            08 – “01” (*)
            09 – DATA DA GERAÇÃO (*)
            10 – HORA DA GERAÇÃO (*)
            11 – RESERVA (*)
            */
        [FixedLengthRecord()]
        public class Header {            

            // Construtor Completo
            public Header(string tipoRegistro, string codigoArquivo, int codigoUsuarioBvmf, int codigoOrigem, string codigoDestino, int numeroMovimento, DateTime mesReferencia, string valorFixo, DateTime dataGeracao, DateTime horaGeracao) {
                this.tipoRegistro = tipoRegistro;
                this.codigoArquivo = codigoArquivo;
                this.codigoUsuarioBVMF = codigoUsuarioBvmf;
                this.codigoOrigem = codigoOrigem;
                this.codigoDestino = codigoDestino;
                this.numeroMovimento = numeroMovimento;
                this.mesReferencia = mesReferencia;
                this.valorFixo = valorFixo;
                this.dataGeracao = dataGeracao;
                this.horaGeracao = horaGeracao;
            }

            // Construtor - Já Define valores Fixos
            public Header() {
                this.tipoRegistro = "00";
                this.codigoArquivo = "PICL";
                this.codigoDestino = "BVMF";
                this.valorFixo = "01";
            }

            [FieldFixedLength(2)]
            public string tipoRegistro;

            [FieldFixedLength(4)]
            public string codigoArquivo;

            [FieldFixedLength(5)]
            [FieldAlign(AlignMode.Right, '0')]
            public int codigoUsuarioBVMF = 0;

            [FieldFixedLength(5)]
            [FieldAlign(AlignMode.Right, '0')]
            public int codigoOrigem = 0;

            [FieldFixedLength(8)]
            public string codigoDestino;

            [FieldFixedLength(9)]
            [FieldAlign(AlignMode.Right, '0')]
            public int numeroMovimento = 0;

            [FieldFixedLength(6)]
            [FieldConverter(ConverterKind.Date, "yyyyMM")]
            public DateTime mesReferencia;

            [FieldFixedLength(2)]
            public string valorFixo;

            [FieldFixedLength(8)]
            [FieldConverter(ConverterKind.Date, "yyyyMMdd")]
            public DateTime dataGeracao;

            [FieldFixedLength(6)]
            [FieldConverter(ConverterKind.Date, "HHmmss")]
            public DateTime horaGeracao;

            [FieldFixedLength(495)]
            [FieldAlign(AlignMode.Right, ' ')]
            public string reserva;
        }
        #endregion

        #region Detalhe_Informe

        /*
        01 – TIPO DE REGISTRO (*)
        02 – CNPJ DO CLUBE (*)
        03 – NÚMERO DO REGISTRO DO CLUBE (*)
        04 – CÓDIGO DO ADMINISTRADOR (*)
        05 – VALOR TOTAL APLICADO EM AÇÕES
        06 – VALOR DAS AÇÕES EM GARANTIAS
        07 – VALOR DAS AÇÕES EM EMPRÉSTIMO - DOADOR
        08 – EMPRÉSTIMO DE AÇÕES – TOMADOR
        09 – VALOR TOTAL APLICADO EM FUNDOS DE CURTO PRAZO
        10 – VALOR TOTAL APLICADO EM FUNDOS DE ÍNDICE (EX: PIBB E ETF)
        11 – VALOR TOTAL APLICADO EM FUNDOS DE RENDA FIXA
        12 – VALOR TOTAL APLICADO EM FUNDOS REFERENCIADOS
        13 – VALOR TOTAL APLICADO EM OUTROS VALORES MOBILIÁRIOS DE EMISSÃO DE COMPANHIAS ABERTAS
        14 – VALOR TOTAL APLICADO EM DEBÊNTURES CONVERSÍVEIS
        15 – VALOR TOTAL APLICADO EM DEBÊNTURES SIMPLES
        16 – VALOR TOTAL APLICADO EM BÔNUS DE SUBSCRIÇÃO
        17 – VALOR TOTAL APLICADO EM RECIBOS DE SUBSCRIÇÃO
        18 – VALOR TOTAL APLICADO EM CERTIFICADOS DE AÇÕES
        19 – VALOR TOTAL APLICADO EM TÍTULOS PÚBLICOS
        20 – VALOR TOTAL DOS PRÊMIOS DE OPÇÕES
        21 – VALOR TOTAL APLICADO EM TÍTULOS DE RESPONSABILIDADE DE INSTITUIÇÃO FINANCEIRA
        22 – OUTRAS CONTAS A RECEBER (COMPENSAÇÃO FINANCEIRA, REMUNERAÇÃO E PROVENTOS)
        23 – OUTRAS CONTAS A PAGAR (COMPENSAÇÃO FINANCEIRA, TX. ADMINISTRAÇÃO, CUSTÓDIA, ETC.)
        24 – SALDO DISPONÍVEL EM CONTA CORRENTE/BANCOS (CAIXA)
        25 – ENTRADA DE RECURSOS NO MÊS (APORTE DE COTISTAS)
        26 – SAÍDA DE RECURSOS NO MÊS (RESGATE DE COTISTAS)
        27 – NÚMERO TOTAL DE COTISTAS (*)
        28 – PATRIMÔNIO LÍQUIDO (*)
        29 – VALOR DA COTA (*)
        30 – SINAL DA RENTABILIDADE NO PERÍODO
        31 – RENTABILIDADE NO PERÍODO
        32 – RESERVA 
        */
        [FixedLengthRecord()]
        public class Detalhe_Informe {

            // Construtor Completo
            public Detalhe_Informe(string tiporegistro, string cnpjClube, int numeroRegistroClube, int codigoAdministrador, decimal valorAcoesGarantia, decimal valorTotalAplicadoacoes, decimal valorAcoesEmprestimoDoador, decimal valorAcoesEmprestimoTomador, decimal valorTotalFundosCurtoPrazo, decimal valorTotalFundosIndice, decimal valorTotalFundosRendaFixa, decimal valorTotalFundosReferenciados, decimal valorTotalValoresMobilarios, decimal valorTotalDebenturesConversiveis, decimal valorTotalDebenturesSimples, decimal valorTotalBonusSusbcricao, decimal valorTotalTitulosPublicos, decimal valorTotalPremiosOpcoes, decimal valorTotalTitulosInstituicaoFinanceira, decimal contasAReceber, decimal contasApagar, decimal saldoDisponivelContaCorrente, decimal aporteCotistas, decimal resgateCotistas, int numeroCotistas, decimal patrimonioLiquido, decimal valorCota, string sinalRentabilidade, decimal rentabilidade) {
                this.tiporegistro = tiporegistro;
                this.cnpjClube = cnpjClube;
                this.numeroRegistroClube = numeroRegistroClube;
                this.codigoAdministrador = codigoAdministrador;
                this.valorAcoesGarantia = valorAcoesGarantia;
                this.valorTotalAplicadoacoes = valorTotalAplicadoacoes;
                this.valorAcoesEmprestimoDoador = valorAcoesEmprestimoDoador;
                this.valorAcoesEmprestimoTomador = valorAcoesEmprestimoTomador;
                this.valorTotalFundosCurtoPrazo = valorTotalFundosCurtoPrazo;
                this.valorTotalFundosIndice = valorTotalFundosIndice;
                this.valorTotalFundosRendaFixa = valorTotalFundosRendaFixa;
                this.valorTotalFundosReferenciados = valorTotalFundosReferenciados;
                this.valorTotalValoresMobilarios = valorTotalValoresMobilarios;
                this.valorTotalDebenturesConversiveis = valorTotalDebenturesConversiveis;
                this.valorTotalDebenturesSimples = valorTotalDebenturesSimples;
                this.valorTotalBonusSusbcricao = valorTotalBonusSusbcricao;
                this.valorTotalTitulosPublicos = valorTotalTitulosPublicos;
                this.valorTotalPremiosOpcoes = valorTotalPremiosOpcoes;
                this.valorTotalTitulosInstituicaoFinanceira = valorTotalTitulosInstituicaoFinanceira;
                this.contasAReceber = contasAReceber;
                this.contasApagar = contasApagar;
                this.saldoDisponivelContaCorrente = saldoDisponivelContaCorrente;
                this.aporteCotistas = aporteCotistas;
                this.resgateCotistas = resgateCotistas;
                this.numeroCotistas = numeroCotistas;
                this.patrimonioLiquido = patrimonioLiquido;
                this.valorCota = valorCota;
                this.sinalRentabilidade = sinalRentabilidade;
                this.rentabilidade = rentabilidade;
            }

            // Construtor - Já Define valores Fixos
            public Detalhe_Informe() {
                this.tiporegistro = "01";
            }

            [FieldFixedLength(2)]
            public string tiporegistro;

            [FieldFixedLength(14)]
            [FieldAlign(AlignMode.Right, '0')]
            public string cnpjClube;
            
            [FieldFixedLength(8)]
            [FieldAlign(AlignMode.Right, '0')]
            public int numeroRegistroClube = 0;

            [FieldFixedLength(5)]
            [FieldAlign(AlignMode.Right, '0')]
            public int codigoAdministrador = 0;

            [FieldFixedLength(16)]
            [FieldConverter(typeof(Detalhe_Informe.TwoDecimalConverter))]
            [FieldAlign(AlignMode.Right, '0')]
            public decimal? valorTotalAplicadoacoes = 0.00M;

            [FieldFixedLength(16)]
            [FieldConverter(typeof(Detalhe_Informe.TwoDecimalConverter))]
            [FieldAlign(AlignMode.Right, '0')]
            public decimal? valorAcoesGarantia = 0.00M;

            [FieldFixedLength(16)]
            [FieldConverter(typeof(Detalhe_Informe.TwoDecimalConverter))]
            [FieldAlign(AlignMode.Right, '0')]
            public decimal? valorAcoesEmprestimoDoador = 0.00M;

            [FieldFixedLength(16)]
            [FieldConverter(typeof(Detalhe_Informe.TwoDecimalConverter))]
            [FieldAlign(AlignMode.Right, '0')]
            public decimal? valorAcoesEmprestimoTomador = 0.00M;

            [FieldFixedLength(16)]
            [FieldConverter(typeof(Detalhe_Informe.TwoDecimalConverter))]
            [FieldAlign(AlignMode.Right, '0')]
            public decimal? valorTotalFundosCurtoPrazo = 0.00M;

            [FieldFixedLength(16)]
            [FieldConverter(typeof(Detalhe_Informe.TwoDecimalConverter))]
            [FieldAlign(AlignMode.Right, '0')]
            public decimal? valorTotalFundosIndice = 0.00M;

            [FieldFixedLength(16)]
            [FieldConverter(typeof(Detalhe_Informe.TwoDecimalConverter))]
            [FieldAlign(AlignMode.Right, '0')]
            public decimal? valorTotalFundosRendaFixa = 0.00M;

            [FieldFixedLength(16)]
            [FieldConverter(typeof(Detalhe_Informe.TwoDecimalConverter))]
            [FieldAlign(AlignMode.Right, '0')]
            public decimal? valorTotalFundosReferenciados = 0.00M;

            [FieldFixedLength(16)]
            [FieldConverter(typeof(Detalhe_Informe.TwoDecimalConverter))]
            [FieldAlign(AlignMode.Right, '0')]
            public decimal? valorTotalValoresMobilarios = 0.00M;

            [FieldFixedLength(16)]
            [FieldConverter(typeof(Detalhe_Informe.TwoDecimalConverter))]
            [FieldAlign(AlignMode.Right, '0')]
            public decimal? valorTotalDebenturesConversiveis = 0.00M;

            [FieldFixedLength(16)]
            [FieldConverter(typeof(Detalhe_Informe.TwoDecimalConverter))]
            [FieldAlign(AlignMode.Right, '0')]
            public decimal? valorTotalDebenturesSimples = 0.00M;

            [FieldFixedLength(16)] 
            [FieldConverter(typeof(Detalhe_Informe.TwoDecimalConverter))]
            [FieldAlign(AlignMode.Right, '0')]
            public decimal? valorTotalBonusSusbcricao = 0.00M;

            [FieldFixedLength(16)]
            [FieldConverter(typeof(Detalhe_Informe.TwoDecimalConverter))]
            [FieldAlign(AlignMode.Right, '0')]
            public decimal? valorTotalAplicadoRecibosSubscricao = 0.00M;

            [FieldFixedLength(16)]
            [FieldConverter(typeof(Detalhe_Informe.TwoDecimalConverter))]
            [FieldAlign(AlignMode.Right, '0')]
            public decimal? valorTotalAplicadoCertificadosAcoes = 0.00M;

            [FieldFixedLength(16)]
            [FieldConverter(typeof(Detalhe_Informe.TwoDecimalConverter))]
            [FieldAlign(AlignMode.Right, '0')]
            public decimal? valorTotalTitulosPublicos = 0.00M;

            [FieldFixedLength(16)]
            [FieldConverter(typeof(Financial.Export.Picl.Detalhe_Informe.TwoDecimalConverter))]
            [FieldAlign(AlignMode.Right, '0')]
            public decimal? valorTotalPremiosOpcoes = 0.00M;

            [FieldFixedLength(16)]
            [FieldConverter(typeof(Detalhe_Informe.TwoDecimalConverter))]
            [FieldAlign(AlignMode.Right, '0')]
            public decimal? valorTotalTitulosInstituicaoFinanceira = 0.00M;

            [FieldFixedLength(16)]
            [FieldConverter(typeof(Detalhe_Informe.TwoDecimalConverter))]
            [FieldAlign(AlignMode.Right, '0')]
            public decimal? contasAReceber = 0.00M;

            [FieldFixedLength(16)]
            [FieldConverter(typeof(Detalhe_Informe.TwoDecimalConverter))]
            [FieldAlign(AlignMode.Right, '0')]
            public decimal? contasApagar = 0.00M;

            [FieldFixedLength(16)]
            [FieldConverter(typeof(Detalhe_Informe.TwoDecimalConverter))]
            [FieldAlign(AlignMode.Right, '0')]
            public decimal? saldoDisponivelContaCorrente = 0.00M;

            [FieldFixedLength(16)]
            [FieldConverter(typeof(Detalhe_Informe.TwoDecimalConverter))]
            [FieldAlign(AlignMode.Right, '0')]
            public decimal? aporteCotistas = 0.00M;

            [FieldFixedLength(16)]
            [FieldConverter(typeof(Detalhe_Informe.TwoDecimalConverter))]
            [FieldAlign(AlignMode.Right, '0')]
            public decimal? resgateCotistas = 0.00M;

            [FieldFixedLength(9)]
            [FieldAlign(AlignMode.Right, '0')]
            public int numeroCotistas = 0;

            [FieldFixedLength(16)]
            [FieldConverter(typeof(Detalhe_Informe.TwoDecimalConverter))]
            [FieldAlign(AlignMode.Right, '0')]
            public decimal patrimonioLiquido = 0.00M;

            [FieldFixedLength(18)]
            [FieldConverter(typeof(Detalhe_Informe.EightDecimalConverter))]
            [FieldAlign(AlignMode.Right, '0')]
            public decimal valorCota = 0.00000000M;

            [FieldFixedLength(1)]
            public string sinalRentabilidade;

            [FieldFixedLength(7)]
            [FieldConverter(typeof(Detalhe_Informe.TwoDecimalConverter))]
            [FieldAlign(AlignMode.Right, '0')]
            public decimal? rentabilidade = 0.00M;

            [FieldFixedLength(118)]
            [FieldAlign(AlignMode.Right, ' ')]
            public string reserva;

            #region Converter 2 e 8 Digitos
            internal class TwoDecimalConverter : ConverterBase {
                // Converte de string para decimal
                public override object StringToField(string from) {
                    return Convert.ToDecimal(from);
                }

                // Converte de decimal para string
                public override string FieldToString(object from) {
                    //decimal res = Convert.ToDecimal(from);
                    //res = res * 100;
                    //return res.ToString("N2").Replace(".", "");

                    decimal d = (decimal)from;
                    return Math.Round(d * 100).ToString();
                }
            }

            internal class EightDecimalConverter : ConverterBase {
                // Converte de string para decimal
                public override object StringToField(string from) {
                    return Convert.ToDecimal(from);
                }

                // Converte de decimal para string
                public override string FieldToString(object from) {
                    //decimal res = Convert.ToDecimal(from);
                    //res = res * 100000000;
                    //return res.ToString("N8").Replace(".", "");

                    decimal d = (decimal)from;
                    return Math.Round(d * 100000000).ToString();

                }
            }
            #endregion
        } 
        #endregion

        #region Detalhe_Cotista

        /*
        01 – TIPO DE REGISTRO (*)
        02 – CNPJ DO CLUBE (*)
        03 – NOME DO COTISTA (*)
        04 – CPF DO COTISTA (*)
        05 – PESSOA VINCULADA S/N
        06 – QUANTIDADE DE COTAS (*)
        07 – PARTICIPAÇÃO DO COTISTA NO CLUBE (*)
        08 – RESERVA                           
        */
        [FixedLengthRecord()]
        public class Detalhe_Cotista {

            // Construtor Completo
            public Detalhe_Cotista(string tipoRegistro, string cnpjClube, string nomeCotista, string cpfCotista, int isPessoaVinculada, decimal quantidadeCotas, decimal percentualCotistaClube) {
                this.tipoRegistro = tipoRegistro;
                this.cnpjClube = cnpjClube;
                this.nomeCotista = nomeCotista;
                this.cpfCotista = cpfCotista;
                this.isPessoaVinculada = isPessoaVinculada;
                this.quantidadeCotas = quantidadeCotas;
                this.percentualCotistaClube = percentualCotistaClube;
            }
            
            // Construtor
            public Detalhe_Cotista() {
                this.tipoRegistro = "02";
            }

            [FieldFixedLength(2)]
            public string tipoRegistro;

            [FieldFixedLength(14)]
            [FieldAlign(AlignMode.Right, '0')]
            public string cnpjClube;

            [FieldFixedLength(60)]
            public string nomeCotista;

            [FieldFixedLength(11)]
            [FieldAlign(AlignMode.Right, '0')]
            public string cpfCotista;

            [FieldFixedLength(1)]
            public int isPessoaVinculada;

            [FieldFixedLength(18)]
            [FieldConverter(typeof(Detalhe_Cotista.EightDecimalConverter))]
            [FieldAlign(AlignMode.Right, '0')]
            public decimal quantidadeCotas = 0.00000000M;

            [FieldFixedLength(13)]
            [FieldConverter(typeof(Detalhe_Cotista.TenDecimalConverter))]
            [FieldAlign(AlignMode.Right, '0')]
            public decimal percentualCotistaClube = 0.0000000000M;

            [FieldFixedLength(431)]
            [FieldAlign(AlignMode.Right, ' ')]
            public string reserva;

            #region Converter 8 e 10 Digitos

            internal class EightDecimalConverter : ConverterBase {
                // Converte de string para decimal
                public override object StringToField(string from) {
                    return Convert.ToDecimal(from);
                }

                // Converte de decimal para string
                public override string FieldToString(object from) {
                    decimal res = Convert.ToDecimal(from);

                    //res = res * 100000000;
                    //return res.ToString("N8").Replace(".", "");

                    decimal d = (decimal)from;
                    return Math.Round(d * 100000000).ToString();
                }
            }

            internal class TenDecimalConverter : ConverterBase {
                // Converte de string para decimal
                public override object StringToField(string from) {
                    return Convert.ToDecimal(from);
                }

                // Converte de decimal para string
                public override string FieldToString(object from) {
                    //decimal res = Convert.ToDecimal(from);

                    //res = res * 10000000000;
                    //return res.ToString("N10").Replace(".", "");

                    decimal d = (decimal)from;
                    return Math.Round(d * 10000000000).ToString();
                }
            }
            #endregion
        } 
        #endregion

        #region Detalhe_CarteiraAtivo

        /*
        01 – TIPO DE REGISTRO (*)
        02 – CNPJ DO CLUBE (*)
        03 – CÓDIGO DE NEGOCIAÇÃO
        04 – CÓDIGO ISIN (*)
        05 –DISTRIBUIÇÃO (*)
        06 – QUANTIDADE (*)
        07 – VALOR (*)
        08 – CUSTÓDIA (*)
        09 – FATOR DE COTAÇÃO (*)
        10 – CÓDIGO DA CARTEIRA
        11 – RESERVA                 
        */
        [FixedLengthRecord()]
        public class Detalhe_CarteiraAtivo {

            // Construtor Completo
            public Detalhe_CarteiraAtivo(string tipoRegistro, string cnpjClube, string codigoAtivo, string codigoIsin, int distribuicao, decimal quantidade, decimal valor, int isCustodia, int fatorCotacao, int codigoCarteira) {
                this.tipoRegistro = tipoRegistro;
                this.cnpjClube = cnpjClube;
                this.codigoAtivo = codigoAtivo;
                this.codigoIsin = codigoIsin;
                this.distribuicao = distribuicao;
                this.quantidade = quantidade;
                this.valor = valor;
                this.isCustodia = isCustodia;
                this.fatorCotacao = fatorCotacao;
                this.codigoCarteira = codigoCarteira;
            }

            // Construtor
            public Detalhe_CarteiraAtivo() {
                this.tipoRegistro = "03";
            }

            [FieldFixedLength(2)]
            public string tipoRegistro;

            [FieldFixedLength(14)]
            [FieldAlign(AlignMode.Right, '0')]
            public string cnpjClube;

            [FieldFixedLength(12)]
            public string codigoAtivo;

            [FieldFixedLength(12)]
            public string codigoIsin;

            [FieldFixedLength(5)]
            [FieldAlign(AlignMode.Right, '0')]
            public int distribuicao = 0;

            [FieldFixedLength(18)]
            [FieldConverter(typeof(Detalhe_CarteiraAtivo.ThreeDecimalConverter))]
            [FieldAlign(AlignMode.Right, '0')]
            public decimal quantidade = 0.00M;

            [FieldFixedLength(16)]
            [FieldConverter(typeof(Detalhe_CarteiraAtivo.TwoDecimalConverter))]
            [FieldAlign(AlignMode.Right, '0')]
            public decimal valor = 0.00M;

            [FieldFixedLength(1)]
            public int isCustodia;

            [FieldFixedLength(7)]
            [FieldAlign(AlignMode.Right, '0')]
            public int fatorCotacao = 0;

            [FieldFixedLength(5)]
            [FieldAlign(AlignMode.Right, '0')]
            public int? codigoCarteira = 0;

            [FieldFixedLength(458)]
            [FieldAlign(AlignMode.Right, ' ')]
            public string reserva;

            #region Converter 2 e 3 Digitos

            internal class TwoDecimalConverter : ConverterBase {
                // Converte de string para decimal
                public override object StringToField(string from) {
                    return Convert.ToDecimal(from);
                }

                // Converte de decimal para string
                public override string FieldToString(object from) {
                    //decimal res = Convert.ToDecimal(from);

                    //res = res * 100;
                    //return res.ToString("N2").Replace(".", "");

                    decimal d = (decimal)from;
                    return Math.Round(d * 100).ToString();
                }
            }

            internal class ThreeDecimalConverter : ConverterBase {
                // Converte de string para decimal
                public override object StringToField(string from) {
                    return Convert.ToDecimal(from);
                }

                // Converte de decimal para string
                public override string FieldToString(object from) {
                    //decimal res = Convert.ToDecimal(from);

                    //res = res * 1000;
                    //return res.ToString("N3").Replace(".", "");

                    decimal d = (decimal)from;
                    return Math.Round(d * 1000).ToString();
                }
            }
            #endregion
        } 
        #endregion

        #region Detalhe_Opcoes

        /*
        01 – TIPO DE REGISTRO (*)
        02 – CNPJ DO CLUBE (*)
        03 – CÓDIGO ISIN DO ATIVO OBJETO (*)
        04 – DISTRIBUIÇÃO DO ATIVO OBJETO (*)
        05 – TIPO OPÇÃO (*)
        06 – SÉRIE (*)
        07 – CÓDIGO DA OPÇÃO P/ NEGOCIAÇÃO (*)
        08 – PREÇO DE EXERCICIO (*)
        09 – VALOR DO PRÊMIO DE OPÇÃO
        10 – VENCIMENTO (*)
        11 – QUANTIDADE (*)
        12 – POSIÇÃO (*)
        13 – RESERVA                  
        */
        [FixedLengthRecord()]
        public class Detalhe_Opcoes {
            // Construtor Completo
            public Detalhe_Opcoes(string tipoRegistro, string cnpjClube, string codigoIsinAtivoObjeto, int distribuicao, string tipoOpcao, int serie, string codigoOpcao, decimal puExercicio, decimal valorPremio, DateTime dataVencimento, int quantidade, string indicadorPosicao) {
                this.tipoRegistro = tipoRegistro;
                this.cnpjClube = cnpjClube;
                this.codigoIsinAtivoObjeto = codigoIsinAtivoObjeto;
                this.distribuicao = distribuicao;
                this.tipoOpcao = tipoOpcao;
                this.serie = serie;
                this.codigoOpcao = codigoOpcao;
                this.puExercicio = puExercicio;
                this.valorPremio = valorPremio;
                this.dataVencimento = dataVencimento;
                this.quantidade = quantidade;
                this.indicadorPosicao = indicadorPosicao;
            }

            // Construtor
            public Detalhe_Opcoes() {
                this.tipoRegistro = "04";                
            }

            [FieldFixedLength(2)]
            public string tipoRegistro;

            [FieldFixedLength(14)]
            [FieldAlign(AlignMode.Right, '0')]
            public string cnpjClube;

            [FieldFixedLength(12)]
            public string codigoIsinAtivoObjeto;
            
            [FieldFixedLength(5)]
            [FieldAlign(AlignMode.Right, '0')]
            public int distribuicao = 0;

            [FieldFixedLength(1)]
            public string tipoOpcao;

            [FieldFixedLength(7)]
            [FieldAlign(AlignMode.Right, '0')]
            public int serie = 0;

            [FieldFixedLength(12)]
            public string codigoOpcao;

            [FieldFixedLength(13)]
            [FieldConverter(typeof(Detalhe_Opcoes.TwoDecimalConverter))]
            [FieldAlign(AlignMode.Right, '0')]
            public decimal puExercicio = 0.00M;

            [FieldFixedLength(16)]
            [FieldConverter(typeof(Detalhe_Opcoes.TwoDecimalConverter))]
            [FieldAlign(AlignMode.Right, '0')]
            public decimal? valorPremio = 0.00M;

            [FieldFixedLength(8)]
            [FieldConverter(ConverterKind.Date, "yyyyMMdd")]
            public DateTime dataVencimento;

            [FieldFixedLength(15)]
            [FieldAlign(AlignMode.Right, '0')]
            public int quantidade = 0;

            [FieldFixedLength(1)]
            public string indicadorPosicao;

            [FieldFixedLength(444)]
            [FieldAlign(AlignMode.Right, ' ')]
            public string reserva;

            #region Converter 2 Digitos

            internal class TwoDecimalConverter : ConverterBase {
                // Converte de string para decimal
                public override object StringToField(string from) {
                    return Convert.ToDecimal(from);
                }

                // Converte de decimal para string
                public override string FieldToString(object from) {
                    //decimal res = Convert.ToDecimal(from);
                    //res = res * 100;
                    //return res.ToString("N2").Replace(".", "");

                    decimal d = (decimal)from;
                    return Math.Round(d * 100).ToString();
                }
            }            
            #endregion
        } 
        #endregion

        #region Detalhe_Termo

        /*
        01 – TIPO DE REGISTRO (*)
        02 – CNPJ DO CLUBE (*)
        03 – NÚMERO DO CONTRATO (*)
        04 – CÓDIGO ISIN DO ATIVO OBJETO
        05 – DISTRIBUIÇÃO DO ATIVO OBJETO
        06 – QUANTIDADE
        07 – PREÇO ATIVO OBJETO
        08 – VALOR DO CONTRATO (*)
        09 – VENCIMENTO (*)
        10 – POSIÇÃO (*)
        11 – TIPO DO TERMO (*)
        12 – FATOR DE COTAÇÃO DO ATIVO OBJETO
        13 – RESERVA 
        */
        [FixedLengthRecord()]
        public class Detalhe_Termo {
            // Construtor Completo
            public Detalhe_Termo(string tipoRegistro, string cnpjClube, int numeroContrato, string codigoIsinAtivoObjeto, int distribuicao, int quantidade, decimal puAtivoObjeto, decimal valorContrato, DateTime dataVencimento, string indicadorPosicao, int tipoTermo, int fatorCotacaoAtivoObjeto) {
                this.tipoRegistro = tipoRegistro;
                this.cnpjClube = cnpjClube;
                this.numeroContrato = numeroContrato;
                this.codigoIsinAtivoObjeto = codigoIsinAtivoObjeto;
                this.distribuicao = distribuicao;
                this.quantidade = quantidade;
                this.puAtivoObjeto = puAtivoObjeto;
                this.valorContrato = valorContrato;
                this.dataVencimento = dataVencimento;
                this.indicadorPosicao = indicadorPosicao;
                this.tipoTermo = tipoTermo;
                this.fatorCotacaoAtivoObjeto = fatorCotacaoAtivoObjeto;
            }

            // Construtor
            public Detalhe_Termo() {
                this.tipoRegistro = "05"; 
            }

            [FieldFixedLength(2)]
            public string tipoRegistro;

            [FieldFixedLength(14)]
            [FieldAlign(AlignMode.Right, '0')]
            public string cnpjClube;

            [FieldFixedLength(9)]
            [FieldAlign(AlignMode.Right, '0')]
            public int numeroContrato = 0;

            [FieldFixedLength(12)]
            public string codigoIsinAtivoObjeto;

            [FieldFixedLength(5)]
            [FieldAlign(AlignMode.Right, '0')]
            public int? distribuicao = 0;

            [FieldFixedLength(15)]
            [FieldAlign(AlignMode.Right, '0')]            
            public int? quantidade = 0;

            [FieldFixedLength(15)]
            [FieldConverter(typeof(Detalhe_Termo.TwoDecimalConverter))]
            [FieldAlign(AlignMode.Right, '0')]
            public decimal? puAtivoObjeto = 0.00M;

            [FieldFixedLength(15)]
            [FieldConverter(typeof(Detalhe_Termo.TwoDecimalConverter))]
            [FieldAlign(AlignMode.Right, '0')]
            public decimal valorContrato = 0.00M;

            [FieldFixedLength(8)]
            [FieldConverter(ConverterKind.Date, "yyyyMMdd")]
            public DateTime dataVencimento;

            [FieldFixedLength(1)]            
            public string indicadorPosicao;

            [FieldFixedLength(1)]
            public int tipoTermo;

            [FieldFixedLength(7)]
            [FieldAlign(AlignMode.Right, '0')]
            public int? fatorCotacaoAtivoObjeto = 0;

            [FieldFixedLength(446)]
            [FieldAlign(AlignMode.Right, ' ')]
            public string reserva;

            #region Converter 2 Digitos

            internal class TwoDecimalConverter : ConverterBase {
                // Converte de string para decimal
                public override object StringToField(string from) {
                    return Convert.ToDecimal(from);
                }

                // Converte de decimal para string
                public override string FieldToString(object from) {
                    //decimal res = Convert.ToDecimal(from);

                    //res = res * 100;
                    //return res.ToString("N2").Replace(".", "");

                    decimal d = (decimal)from;
                    return Math.Round(d * 100).ToString();
                }
            }
            #endregion
        } 
        #endregion

        #region Detalhe_EmprestimoAcoes

        /*
        01 – TIPO DE REGISTRO (*)
        02 – CNPJ DO CLUBE (*)
        03 – NÚMERO DO CONTRATO (*)
        04 – CÓDIGO ISIN DO ATIVO OBJETO (*)
        05 – DISTRIBUIÇÃO DO ATIVO OBJETO (*)
        06 – QUANTIDADE (*)
        07 – VALOR DA COTAÇÃO (*)
        08 – OPÇÃO DE COTAÇÃO (*)
        09 – VENCIMENTO (*)
        10 – TAXA DE CONTRATO (*)
        11 – POSIÇÃO (*)
        12 – FATOR DE COTAÇÃO DO ATIVO OBJETO (*)
        13 – CÓDIGO DA CARTEIRA (*)
        14 – RESERVA                  
        */
        [FixedLengthRecord()]
        public class Detalhe_EmprestimoAcoes {
            // Construtor Completo
            public Detalhe_EmprestimoAcoes(string tipoRegistro, string cnpjClube, int numeroConrato, string codigoIsinAtivoObjeto, int distribuicao, int quantidade, decimal valorCotacao, int opcaoCotacao, DateTime dataVencimento, decimal taxaContrato, string indicadorPosicao, int fatorCotacaoAtivoObjeto, int codigoCarteira) {
                this.tipoRegistro = tipoRegistro;
                this.cnpjClube = cnpjClube;
                this.numeroContrato = numeroConrato;
                this.codigoIsinAtivoObjeto = codigoIsinAtivoObjeto;
                this.distribuicao = distribuicao;
                this.quantidade = quantidade;
                this.valorCotacao = valorCotacao;
                this.opcaoCotacao = opcaoCotacao;
                this.dataVencimento = dataVencimento;
                this.taxaContrato = taxaContrato;
                this.indicadorPosicao = indicadorPosicao;
                this.fatorCotacaoAtivoObjeto = fatorCotacaoAtivoObjeto;
                this.codigoCarteira = codigoCarteira;
            }

            // Construtor
            public Detalhe_EmprestimoAcoes() {
                this.tipoRegistro = "06";
            }

            [FieldFixedLength(2)]
            public string tipoRegistro;

            [FieldFixedLength(14)]
            [FieldAlign(AlignMode.Right, '0')]
            public string cnpjClube;

            [FieldFixedLength(9)]
            [FieldAlign(AlignMode.Right, '0')]
            public int numeroContrato = 0;

            [FieldFixedLength(12)]
            public string codigoIsinAtivoObjeto;

            [FieldFixedLength(5)]
            [FieldAlign(AlignMode.Right, '0')]
            public int distribuicao = 0;

            [FieldFixedLength(18)]
            [FieldAlign(AlignMode.Right, '0')]
            public int quantidade = 0;

            [FieldFixedLength(18)]
            [FieldConverter(typeof(Detalhe_EmprestimoAcoes.SevenDecimalConverter))]
            [FieldAlign(AlignMode.Right, '0')]
            public decimal valorCotacao = 0.0000000M;

            [FieldFixedLength(1)]
            public int opcaoCotacao;

            [FieldFixedLength(8)]
            [FieldConverter(ConverterKind.Date, "yyyyMMdd")]
            public DateTime dataVencimento;

            [FieldFixedLength(10)]
            [FieldConverter(typeof(Detalhe_EmprestimoAcoes.FiveDecimalConverter))]
            [FieldAlign(AlignMode.Right, '0')]
            public decimal taxaContrato = 0.00000M;

            [FieldFixedLength(1)]
            public string indicadorPosicao;

            [FieldFixedLength(7)]
            [FieldAlign(AlignMode.Right, '0')]
            public int fatorCotacaoAtivoObjeto = 0;

            [FieldFixedLength(5)]
            [FieldAlign(AlignMode.Right, '0')]
            public int codigoCarteira = 0;

            [FieldFixedLength(440)]
            [FieldAlign(AlignMode.Right, ' ')]
            public string reserva;

            #region Converter 5 e 7 Digitos

            internal class FiveDecimalConverter : ConverterBase {
                // Converte de string para decimal
                public override object StringToField(string from) {
                    return Convert.ToDecimal(from);
                }

                // Converte de decimal para string
                public override string FieldToString(object from) {
                    //decimal res = Convert.ToDecimal(from);

                    //res = res * 100000;
                    //return res.ToString("N5").Replace(".", "");

                    decimal d = (decimal)from;
                    return Math.Round(d * 100000).ToString();
                }
            }

            internal class SevenDecimalConverter : ConverterBase {
                // Converte de string para decimal
                public override object StringToField(string from) {
                    return Convert.ToDecimal(from);
                }

                // Converte de decimal para string
                public override string FieldToString(object from) {
                    //decimal res = Convert.ToDecimal(from);

                    //res = res * 10000000;
                    //return res.ToString("N7").Replace(".", "");

                    decimal d = (decimal)from;
                    return Math.Round(d * 10000000).ToString();
                }
            }

            #endregion
        } 
        #endregion

        #region Detalhe_Futuros

        /*
        01 – TIPO DE REGISTRO (*)
        02 – CNPJ DO CLUBE (*)]
        03 – CÓDIGO DE NEGOCIAÇÃO (*)
        04 – SÉRIE (*)
        05 – CÓDIGO ISIN DO ATIVO OBJETO (*)
        06 – DISTRIBUIÇÃO DO ATIVO OBJETO
        07 – QUANTIDADE (*)
        08 – PREÇO DE AJUSTE (*)
        09 – VENCIMENTO (*)
        10 – POSIÇÃO (*)
        11 – FATOR DE COTAÇÃO DO ATIVO OBJETO
        12 – RESERVA
        */
        [FixedLengthRecord()]
        public class Detalhe_Futuros {
            // Construtor Completo
            public Detalhe_Futuros(string tipoRegistro, string cnpjClube, string codigoAtivo, int serie, string codigoIsinAtivoObjeto, int distribuicao, int quantidade, decimal puAjuste, string indicadorPosicao, DateTime dataVencimento, int fatorCotacaoAtivoObjeto) {
                this.tipoRegistro = tipoRegistro;
                this.cnpjClube = cnpjClube;
                this.codigoAtivo = codigoAtivo;
                this.serie = serie;
                this.codigoIsinAtivoObjeto = codigoIsinAtivoObjeto;
                this.distribuicao = distribuicao;
                this.quantidade = quantidade;
                this.puAjuste = puAjuste;
                this.indicadorPosicao = indicadorPosicao;
                this.dataVencimento = dataVencimento;
                this.fatorCotacaoAtivoObjeto = fatorCotacaoAtivoObjeto;
            }

            // Construtor
            public Detalhe_Futuros() {
                this.tipoRegistro = "07";
            }

            [FieldFixedLength(2)]
            public string tipoRegistro;

            [FieldFixedLength(14)]
            [FieldAlign(AlignMode.Right, '0')]
            public string cnpjClube;

            [FieldFixedLength(12)]
            public string codigoAtivo;

            [FieldFixedLength(7)]
            [FieldAlign(AlignMode.Right, '0')]
            public int serie = 0;

            [FieldFixedLength(12)]
            public string codigoIsinAtivoObjeto;

            [FieldFixedLength(5)]
            [FieldAlign(AlignMode.Right, '0')]
            public int? distribuicao = 0;

            [FieldFixedLength(15)]
            [FieldAlign(AlignMode.Right, '0')]
            public int quantidade = 0;

            [FieldFixedLength(13)]
            [FieldConverter(typeof(Detalhe_Futuros.TwoDecimalConverter))]
            [FieldAlign(AlignMode.Right, '0')]
            public decimal puAjuste = 0.0M;

            [FieldFixedLength(8)]
            [FieldConverter(ConverterKind.Date, "yyyyMMdd")]
            public DateTime dataVencimento;

            [FieldFixedLength(1)]
            public string indicadorPosicao;

            [FieldFixedLength(7)]
            [FieldAlign(AlignMode.Right, '0')]
            public int? fatorCotacaoAtivoObjeto = 0;

            [FieldFixedLength(454)]
            [FieldAlign(AlignMode.Right, ' ')]
            public string reserva;

            #region Converter 2 Digitos

            internal class TwoDecimalConverter : ConverterBase {
                // Converte de string para decimal
                public override object StringToField(string from) {
                    return Convert.ToDecimal(from);
                }

                // Converte de decimal para string
                public override string FieldToString(object from) {
                    //decimal res = Convert.ToDecimal(from);

                    //res = res * 100;
                    //return res.ToString("N2").Replace(".", "");

                    decimal d = (decimal)from;
                    return Math.Round(d * 100).ToString();
                }
            }
            #endregion
        }
        
        #endregion

        #region Detalhe_FundoInvestimento

        /*
        01 – TIPO DE REGISTRO (*)
        02 – CNPJ DO CLUBE (*)
        03 – CNPJ DO FUNDO (*)
        04 – NOME DO FUNDO
        05 – VALOR TOTAL APLICADO (*)
        06 – QUANTIDADE DE COTAS (*)
        07 – VALOR DA COTA (*)
        08 – TIPO DE FUNDO DE INVESTIMENTO (*)
        09 – RESERVA 
        */
        [FixedLengthRecord()]
        public class Detalhe_FundoInvestimento {
            // Construtor Completo
            public Detalhe_FundoInvestimento(string tipoRegistro, string cnpjClube, string cnpjFundo, string nomeFundo, decimal valorTotalAplicado, decimal quantidadeCotas, decimal valorCota, int tipoFundoinvestimento) {
                this.tipoRegistro = tipoRegistro;
                this.cnpjClube = cnpjClube;
                this.cnpjFundo = cnpjFundo;
                this.nomeFundo = nomeFundo;
                this.valorTotalAplicado = valorTotalAplicado;
                this.quantidadeCotas = quantidadeCotas;
                this.valorCota = valorCota;
                this.tipoFundoinvestimento = tipoFundoinvestimento;
            }

            // Construtor
            public Detalhe_FundoInvestimento() {
                this.tipoRegistro = "08";
            }

            [FieldFixedLength(2)]
            public string tipoRegistro;

            [FieldFixedLength(14)]
            [FieldAlign(AlignMode.Right, '0')]
            public string cnpjClube;

            [FieldFixedLength(14)]
            [FieldAlign(AlignMode.Right, '0')]
            public string cnpjFundo;

            [FieldFixedLength(60)]
            public string nomeFundo;

            [FieldFixedLength(20)]
            [FieldConverter(typeof(Detalhe_FundoInvestimento.EightDecimalConverter))]
            [FieldAlign(AlignMode.Right, '0')]
            public decimal valorTotalAplicado = 0.00000000M;

            [FieldFixedLength(18)]
            [FieldConverter(typeof(Detalhe_FundoInvestimento.EightDecimalConverter))]
            [FieldAlign(AlignMode.Right, '0')]
            public decimal quantidadeCotas = 0.00000000M;

            [FieldFixedLength(16)]
            [FieldConverter(typeof(Detalhe_FundoInvestimento.EightDecimalConverter))]
            [FieldAlign(AlignMode.Right, '0')]
            public decimal valorCota = 0.00000000M;

            [FieldFixedLength(2)]
            [FieldAlign(AlignMode.Right, '0')]
            public int tipoFundoinvestimento;

            [FieldFixedLength(404)]
            [FieldAlign(AlignMode.Right, ' ')]
            public string reserva;

            #region Converter 8 Digitos

            internal class EightDecimalConverter : ConverterBase {
                // Converte de string para decimal
                public override object StringToField(string from) {
                    return Convert.ToDecimal(from);
                }

                // Converte de decimal para string
                public override string FieldToString(object from) {
                    //decimal res = Convert.ToDecimal(from);

                    //res = res * 100000000;
                    //return res.ToString("N8").Replace(".", "");

                    decimal d = (decimal)from;
                    return Math.Round(d * 100000000).ToString();
                }
            }
            #endregion
        } 
        #endregion

        #region Detalhe_ReclamacoesCotistas

        /*
        01 – TIPO DE REGISTRO (*)
        02 – QUANTIDADE DE RECLAMAÇÕES PROCEDENTES
        03 – QUANTIDADE DE RECLAMAÇÕES IMPROCEDENTES
        04 – QUANTIDADE DE RECLAMAÇÕES RESOLVIDAS
        05 – QUANTIDADE DE RECLAMAÇÕES NÃO RESOLVIDAS
        06 – QUANTIDADE DE RECLAMAÇÕES REFERENTES À ADMINISTRAÇÃO
        07 – QUANTIDADE DE RECLAMAÇÕES REFERENTES À GESTÃO
        08 – QUANTIDADE DE RECLAMAÇÕES REFERENTES À TAXAS
        09 – QUANTIDADE DE RECLAMAÇÕES REFERENTES À POLÍTICA DE INVESTIMENTO
        10 – QUANTIDADE DE RECLAMAÇÕES REFERENTES AOS APORTES
        11 – QUANTIDADE DE RECLAMAÇÕES REFERENTES AOS RESGATES
        12 – QUANTIDADE DE RECLAMAÇÕES REFERENTES AO FORNECIMENTO DE INFORMAÇÕES
        13 – QUANTIDADE DE RECLAMAÇÕES REFERENTES ÀS ASSEMBLÉIAS GERAIS
        14 – QUANTIDADE DE RECLAMAÇÕES REFERENTES ÀS COTAS
        15 – QUANTIDADE DE RECLAMAÇÕES – OUTROS
        16 – RESERVA 
        */
        [FixedLengthRecord()]
        public class Detalhe_ReclamacoesCotistas  {
            // Construtor Completo
            public Detalhe_ReclamacoesCotistas(string tipoRegistro, int quantidadeReclamacoesProcedentes, int quantidadeReclamacoesImProcedentes, int quantidadeReclamacoesNaoResolvidas, int quantidadeReclamacoesResolvidas, int quantidadeReclamacoesRefAdministracao, int quantidadeReclamacoesRefGestao, int quantidadeReclamacoesRefTaxas, int quantidadeReclamacoesRefPoliticaInvestimento, int quantidadeReclamacoesRefAportes, int quantidadeReclamacoesRefResgates, int quantidadeReclamacoesRefFornecimentoInformacoes, int quantidadeReclamacoesRefAssembleiasGerais, int quantidadeReclamacoesRefCotas, int quantidadeReclamacoesOutros) {
                this.tipoRegistro = tipoRegistro;
                quantidadeReclamacoes_Procedentes = quantidadeReclamacoesProcedentes;
                quantidadeReclamacoes_ImProcedentes = quantidadeReclamacoesImProcedentes;
                quantidadeReclamacoes_NaoResolvidas = quantidadeReclamacoesNaoResolvidas;
                quantidadeReclamacoes_Resolvidas = quantidadeReclamacoesResolvidas;
                quantidadeReclamacoes_Ref_Administracao = quantidadeReclamacoesRefAdministracao;
                quantidadeReclamacoes_Ref_Gestao = quantidadeReclamacoesRefGestao;
                quantidadeReclamacoes_Ref_Taxas = quantidadeReclamacoesRefTaxas;
                quantidadeReclamacoes_Ref_Politica_Investimento = quantidadeReclamacoesRefPoliticaInvestimento;
                quantidadeReclamacoes_Ref_Aportes = quantidadeReclamacoesRefAportes;
                quantidadeReclamacoes_Ref_Resgates = quantidadeReclamacoesRefResgates;
                quantidadeReclamacoes_Ref_FornecimentoInformacoes = quantidadeReclamacoesRefFornecimentoInformacoes;
                quantidadeReclamacoes_Ref_AssembleiasGerais = quantidadeReclamacoesRefAssembleiasGerais;
                quantidadeReclamacoes_Ref_Cotas = quantidadeReclamacoesRefCotas;
                quantidadeReclamacoes_Outros = quantidadeReclamacoesOutros;
            }

            // Construtor
            public Detalhe_ReclamacoesCotistas() {
                this.tipoRegistro = "09";
            }

            [FieldFixedLength(2)]
            public string tipoRegistro;
            
            [FieldFixedLength(14)]
            [FieldAlign(AlignMode.Right, '0')]
            public int? quantidadeReclamacoes_Procedentes = 0;
            
            [FieldFixedLength(14)]
            [FieldAlign(AlignMode.Right, '0')]
            public int? quantidadeReclamacoes_ImProcedentes = 0;

            [FieldFixedLength(14)]
            [FieldAlign(AlignMode.Right, '0')]
            public int? quantidadeReclamacoes_Resolvidas = 0;

            [FieldFixedLength(14)]
            [FieldAlign(AlignMode.Right, '0')]
            public int? quantidadeReclamacoes_NaoResolvidas = 0;

            [FieldFixedLength(14)]
            [FieldAlign(AlignMode.Right, '0')]
            public int? quantidadeReclamacoes_Ref_Administracao = 0;

            [FieldFixedLength(14)]
            [FieldAlign(AlignMode.Right, '0')]
            public int? quantidadeReclamacoes_Ref_Gestao = 0;

            [FieldFixedLength(14)]
            [FieldAlign(AlignMode.Right, '0')]
            public int? quantidadeReclamacoes_Ref_Taxas = 0;

            [FieldFixedLength(14)]
            [FieldAlign(AlignMode.Right, '0')]
            public int? quantidadeReclamacoes_Ref_Politica_Investimento = 0;

            [FieldFixedLength(14)]
            [FieldAlign(AlignMode.Right, '0')]
            public int? quantidadeReclamacoes_Ref_Aportes = 0;

            [FieldFixedLength(14)]
            [FieldAlign(AlignMode.Right, '0')]
            public int? quantidadeReclamacoes_Ref_Resgates = 0;

            [FieldFixedLength(14)]
            [FieldAlign(AlignMode.Right, '0')]
            public int quantidadeReclamacoes_Ref_FornecimentoInformacoes = 0;

            [FieldFixedLength(14)]
            [FieldAlign(AlignMode.Right, '0')]
            public int? quantidadeReclamacoes_Ref_AssembleiasGerais = 0;

            [FieldFixedLength(14)]
            [FieldAlign(AlignMode.Right, '0')]
            public int? quantidadeReclamacoes_Ref_Cotas = 0;

            [FieldFixedLength(14)]
            [FieldAlign(AlignMode.Right, '0')]
            public int? quantidadeReclamacoes_Outros = 0;

            [FieldFixedLength(352)]
            [FieldAlign(AlignMode.Right, ' ')]
            public string reserva;
        } 
        #endregion

        #region Detalhe_Trailer

        /*
        01 – TIPO DE REGISTRO (*)
        02 – CÓDIGO DO ARQUIVO (*)
        03 – CÓDIGO DO USUÁRIO NA BVMF (*)
        04 – CÓDIGO DE ORIGEM (*)
        05 – CÓDIGO DE DESTINO (*)
        06 – NÚMERO DO MOVIMENTO (*)
        07 – MÊS DE REFERÊNCIA (*)
        08 – “01” (*)
        09 – TOTAL DE REGISTROS GERADOS
        10 – RESERVA         
        */
        [FixedLengthRecord()]
        public class Detalhe_Trailer  {
            // Construtor Completo
            public Detalhe_Trailer(string tipoRegistro, string codigoArquivo, int codigoUsuarioBvmf, int codigoOrigem, string codigoDestino, int numeroMovimento, DateTime mesReferencia, string valorFixo, int totalRegistrosGerados) {
                this.tipoRegistro = tipoRegistro;
                this.codigoArquivo = codigoArquivo;
                this.codigoUsuarioBVMF = codigoUsuarioBvmf;
                this.codigoOrigem = codigoOrigem;
                this.codigoDestino = codigoDestino;
                this.numeroMovimento = numeroMovimento;
                this.mesReferencia = mesReferencia;
                this.valorFixo = valorFixo;
                this.totalRegistrosGerados = totalRegistrosGerados;
            }

            // Construtor
            public Detalhe_Trailer() {
                this.tipoRegistro = "99";
                this.codigoArquivo = "PICL";
                this.codigoDestino = "BVMF";
                this.valorFixo = "01";                
            }

            [FieldFixedLength(2)]
            public string tipoRegistro;

            [FieldFixedLength(4)]
            public string codigoArquivo;

            [FieldFixedLength(5)]
            [FieldAlign(AlignMode.Right, '0')]
            public int codigoUsuarioBVMF = 0;

            [FieldFixedLength(5)]
            [FieldAlign(AlignMode.Right, '0')]
            public int codigoOrigem = 0;

            [FieldFixedLength(8)]            
            public string codigoDestino;

            [FieldFixedLength(9)]
            [FieldAlign(AlignMode.Right, '0')]
            public int numeroMovimento = 0;

            [FieldFixedLength(6)]
            [FieldConverter(ConverterKind.Date, "yyyyMM")]
            public DateTime mesReferencia;

            [FieldFixedLength(2)]
            [FieldAlign(AlignMode.Right, '0')]
            public string valorFixo;

            [FieldFixedLength(9)]
            [FieldAlign(AlignMode.Right, '0')]
            public int? totalRegistrosGerados = 0;

            [FieldFixedLength(500)]
            [FieldAlign(AlignMode.Right, ' ')]
            public string reserva;
        } 
        #endregion

        /// <summary>
        /// Engine para escrever estrutura na Memory Stream
        /// </summary>
        /// <param name="tipo"></param>
        /// <param name="t"></param>
        /// <returns>Memory Stream com o conteudo da Estrutura t</returns>
        private MemoryStream WriteStream(Type tipo, IList t) {
            MemoryStream ms = new MemoryStream();
            //
            FileHelperEngine engine = new FileHelperEngine(tipo);
            StreamWriter arquivo = new StreamWriter(ms, Encoding.GetEncoding("ISO-8859-1"));
            //
            engine.Encoding = Encoding.GetEncoding("ISO-8859-1");
            engine.WriteStream(arquivo, t);

            arquivo.Flush();

            return ms;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dataPosicao"></param>
        /// <param name="idClientes"></param>
        /// <param name="ms"></param>
        /// <param name="nomeArquivo"></param>
        public void ExportaPicl(DateTime dataPosicao, List<int> idClientes, out MemoryStream ms, out string nomeArquivo) 
        {
            int codigoCorretora = ParametrosConfiguracaoSistema.Bolsa.CodigoAgenteDefault;

            ms = new MemoryStream();
            string codigo = codigoCorretora.ToString().PadLeft(5, '0');
            nomeArquivo = "PICL" + codigo + ".dat";

            #region Busca as Carteiras
            CarteiraQuery carteiraQuery = new CarteiraQuery("C");
            ClienteQuery clienteQuery = new ClienteQuery("B");
            AgenteMercadoQuery agenteMercadoQuery = new AgenteMercadoQuery("A");

            carteiraQuery.Select(carteiraQuery.IdCarteira, clienteQuery.IdCliente, agenteMercadoQuery.IdAgente, agenteMercadoQuery.Uf);
            //
            carteiraQuery.InnerJoin(clienteQuery).On(carteiraQuery.IdCarteira == clienteQuery.IdCliente);
            carteiraQuery.InnerJoin(agenteMercadoQuery).On(carteiraQuery.IdAgenteAdministrador == agenteMercadoQuery.IdAgente);
            //
            carteiraQuery.Where(clienteQuery.IdTipo == TipoClienteFixo.Clube &&
                                clienteQuery.StatusAtivo == StatusAtivoCliente.Ativo &&
                                clienteQuery.DataDia >= dataPosicao &&
                                clienteQuery.IdCliente.In(idClientes)
                                );
            //
            CarteiraCollection carteiraCollection = new CarteiraCollection();
            carteiraCollection.Load(carteiraQuery);
            #endregion

            if (carteiraCollection.Count == 0) {
                // return; // sai e retorna um arquivo vazio - ms = new MemoryStream();
                throw new ProcessaPiclException("Não existe Dados para gerar o Picl.");
            }

            else 
            {
                #region Header
                try {
                    /* Monta Header */
                    MemoryStream ms1 = this.MontaCabecalho(dataPosicao, codigoCorretora);

                    /* Escreve Cabecalho na MemoryStream */
                    ms1.WriteTo(ms);
                }
                catch (Exception e) {
                    Console.WriteLine(e.Message);
                    string mensagem = "";
                    mensagem = "Arquivo Picl com Data " + dataPosicao.ToString("dd/MM/yyyy") + " não Exportado ";
                    mensagem += "\n - " + e.Message;

                    throw new ProcessaPiclException(mensagem);
                }
                #endregion

                #region Corpo
                foreach (Carteira carteira in carteiraCollection) {
                    int idClube = carteira.IdCarteira.Value;
                    Cliente cliente = new Cliente();
                    Pessoa pessoa = new Pessoa();
                    cliente.LoadByPrimaryKey(carteira.IdCarteira.Value);
                    pessoa.LoadByPrimaryKey(cliente.IdPessoa.Value);
                    //
                    string cnpj = pessoa.Cpfcnpj.ToString().Replace(".", "").Replace("-", "").Replace("/", "").Replace("\\", "");

                    cnpj = Utilitario.RemoveAcentos(cnpj);
                    cnpj = Utilitario.RemoveCaracteresEspeciais(cnpj);
                    
                    try {
                        /* Monta Corpo */
                        MemoryStream ms2 = this.MontaCorpo(dataPosicao, idClube, cnpj, codigoCorretora);

                        /* Escreve Corpo na MemoryStream */
                        ms2.WriteTo(ms);
                    }
                    catch (Exception e) {
                        Console.WriteLine(e.Message);
                        string mensagem = "";
                        mensagem = "Arquivo Picl com Data " + dataPosicao.ToString("dd/MM/yyyy") + " não Exportado ";
                        mensagem += "\n - " + e.Message;

                        throw new ProcessaPiclException(mensagem);
                    }
                }
                #endregion

                #region Detalhe_ReclamacoesCotistas
                try
                {
                    /* Monta o Objeto Detalhe_ReclamacoesCotistas */
                    MemoryStream msReclamacoes = this.Monta_Detalhe_ReclamacoesCotistas(dataPosicao);

                    /* Escreve Trailler na MemoryStream */
                    msReclamacoes.WriteTo(ms);
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                    string mensagem = "";
                    mensagem = "Arquivo Picl com Data " + dataPosicao.ToString("dd/MM/yyyy") + " não Exportado ";
                    mensagem += "\n - " + e.Message;

                    throw new ProcessaPiclException(mensagem);
                }
                #endregion

                #region Trailer

                try {
                    /* Monta Trailer */
                    MemoryStream ms3 = this.MontaTrailer(dataPosicao, codigoCorretora);

                    /* Escreve Trailler na MemoryStream */
                    ms3.WriteTo(ms);
                }
                catch (Exception e) {
                    Console.WriteLine(e.Message);
                    string mensagem = "";
                    mensagem = "Arquivo Picl com Data " + dataPosicao.ToString("dd/MM/yyyy") + " não Exportado ";
                    mensagem += "\n - " + e.Message;

                    throw new ProcessaPiclException(mensagem);
                }
                #endregion
            }            
        }

        /// <summary>
        /// Monta Cabeçalho
        /// </summary>
        /// <param name="dataPosicao"></param>
        /// <param name="codigoCorretora"></param>
        /// <returns></returns>
        private MemoryStream MontaCabecalho(DateTime dataPosicao, int codigoCorretora) {
            List<Header> headerDs = new List<Header>();

            #region MontaHeader
            Header h1 = new Header();
            //
            //h1.tipoRegistro = "1";
            //h1.codigoArquivo = 2;
            h1.codigoUsuarioBVMF = codigoCorretora;
            h1.codigoOrigem = codigoCorretora;
            //h1.codigoDestino = "5";
            h1.numeroMovimento = 1;
            h1.mesReferencia = dataPosicao;
            //h1.valorFixo = "8";
            h1.dataGeracao = DateTime.Now;
            h1.horaGeracao = DateTime.Now; 
            #endregion
            //
            headerDs.Add(h1);

            this.numeroRegistros++;
            //
            return this.WriteStream(typeof(Header), headerDs);            
        }

        /// <summary>
        /// Monta Trailer
        /// </summary>
        /// <param name="dataPosicao"></param>
        /// <param name="codigoCorretora"></param>
        /// <returns></returns>
        private MemoryStream MontaTrailer(DateTime dataPosicao, int codigoCorretora) {
            
            /* Monta o Objeto Detalhe_Trailer */
            MemoryStream msTrailer = this.Monta_Detalhe_Trailer(dataPosicao, codigoCorretora);           
            //
            return msTrailer;
        }
        
        /// <summary>
        /// Monta Corpo
        /// </summary>
        /// <param name="dataPosicao"></param>
        /// <returns>Memory Stream Contendo todo conteudo do Documento</returns>
        private MemoryStream MontaCorpo(DateTime dataPosicao, int idClube, string cnpj, int codigoCorretora)
        {
           
            /* Monta o Objeto Detalhe_Informe */
            MemoryStream ms1 = this.Monta_Detalhe_Informe(dataPosicao, idClube, cnpj, codigoCorretora);

            /* Monta o Objeto Detalhe_Cotista */
            MemoryStream ms2 = this.Monta_Detalhe_Cotista(dataPosicao, idClube, cnpj);

            /* Monta o Objeto Detalhe_CarteiraAtivo */
            MemoryStream ms3 = this.Monta_Detalhe_CarteiraAtivo(dataPosicao, idClube, cnpj);

            /* Monta o Objeto Detalhe_Opcoes */
            MemoryStream ms4 = this.Monta_Detalhe_Opcoes(dataPosicao, idClube, cnpj);

            /* Monta o Objeto Detalhe_Termo */
            MemoryStream ms5 = this.Monta_Detalhe_Termo(dataPosicao, idClube, cnpj);

            /* Monta o Objeto Detalhe_EmprestimoAcoes */
            MemoryStream ms6 = this.Monta_Detalhe_EmprestimoAcoes(dataPosicao, idClube, cnpj);

            /* Monta o Objeto Detalhe_Futuros */
            MemoryStream ms7 = this.Monta_Detalhe_Futuros(dataPosicao, idClube, cnpj);

            /* Monta o Objeto Detalhe_FundoInvestimento */
            MemoryStream ms8 = this.Monta_Detalhe_FundoInvestimento(dataPosicao, idClube, cnpj);
                       
            MemoryStream mRetorno = new MemoryStream();
            //
            ms1.WriteTo(mRetorno);
            ms2.WriteTo(mRetorno);
            ms3.WriteTo(mRetorno);
            ms4.WriteTo(mRetorno);
            ms5.WriteTo(mRetorno);
            ms6.WriteTo(mRetorno);
            ms7.WriteTo(mRetorno);
            ms8.WriteTo(mRetorno);
            //
            return mRetorno;
        }

        #region Detalhe_Informe
        private MemoryStream Monta_Detalhe_Informe(DateTime dataPosicao, int idClube, string cnpj, int codigoCorretora)
        {
            List<Detalhe_Informe> informeDs = new List<Detalhe_Informe>();
            
            Detalhe_Informe d1 = new Detalhe_Informe();
            //
            #region Monta Detalhe_Informe
            //d1.tiporegistro = "1";
            d1.cnpjClube = cnpj;

            ClienteInterface clienteInterface = new ClienteInterface();
            if (!clienteInterface.LoadByPrimaryKey(idClube))
            {
                throw new Exception("Não há código Bovespa cadastrado para o clube " + idClube.ToString());
            }

            if (clienteInterface.RegistroBovespa.Trim() == "")
            {
                throw new Exception("Não há código Bovespa cadastrado para o clube " + idClube.ToString());
            }

            int registroBovespa = 0;
            try
            {
                registroBovespa = Convert.ToInt32(clienteInterface.RegistroBovespa.Trim());
            }
            catch (Exception)
            {
                throw new Exception("Código Bovespa inválido cadastrado para o clube " + idClube.ToString());
            }

            PosicaoBolsaHistorico posicaoBolsaHistorico = new PosicaoBolsaHistorico();
            PosicaoEmprestimoBolsaHistorico posicaoEmprestimoBolsaHistorico = new PosicaoEmprestimoBolsaHistorico();
            PosicaoRendaFixaHistorico posicaoRendaFixaHistorico = new PosicaoRendaFixaHistorico();
            SaldoCaixa saldoCaixa = new SaldoCaixa();
            LiquidacaoHistorico liquidacaoHistorico = new LiquidacaoHistorico();
            OperacaoCotista operacaoCotista = new OperacaoCotista();
            PosicaoCotistaHistorico posicaoCotistaHistorico = new PosicaoCotistaHistorico();
            HistoricoCota historicoCota = new HistoricoCota();
            CalculoMedida calculoMedida = new CalculoMedida(idClube);

            decimal totalAcoes = posicaoBolsaHistorico.RetornaValorMercadoAcoes(idClube, dataPosicao);
            if (totalAcoes < 0)
            {
                totalAcoes = 0;
            }

            decimal totalAcoesGarantia = posicaoBolsaHistorico.RetornaValorMercadoBloqueado(idClube, dataPosicao);
            decimal totalDoador = posicaoEmprestimoBolsaHistorico.RetornaValorMercado(idClube, dataPosicao, PontaEmprestimoBolsa.Doador);
            decimal totalTomador = posicaoEmprestimoBolsaHistorico.RetornaValorMercado(idClube, dataPosicao, PontaEmprestimoBolsa.Tomador);

            decimal totalFundoCurtoPrazo = this.RetornaValorFundoCategoria(idClube, dataPosicao, (int)ListaCategoriasFundosCBLC.CurtoPrazo);
            decimal totalFundoRendaFixa = this.RetornaValorFundoCategoria(idClube, dataPosicao, (int)ListaCategoriasFundosCBLC.RendaFixa);
            decimal totalFundoReferenciado = this.RetornaValorFundoCategoria(idClube, dataPosicao, (int)ListaCategoriasFundosCBLC.Referenciado);

            decimal totalFundosIndice = posicaoBolsaHistorico.RetornaValorMercado(idClube, dataPosicao, (byte)TipoPapelAtivo.ETF);
            decimal totalBonusSubscricao = posicaoBolsaHistorico.RetornaValorMercado(idClube, dataPosicao, (byte)TipoPapelAtivo.BonusSubscricao);
            decimal totalReciboSubscricao = posicaoBolsaHistorico.RetornaValorMercado(idClube, dataPosicao, (byte)TipoPapelAtivo.ReciboSubscricao);

            totalAcoes = totalAcoes - totalTomador - totalFundosIndice - totalBonusSubscricao - totalReciboSubscricao;

            totalAcoes = Math.Abs(totalAcoes);

            decimal totalTitulosPublicos = posicaoRendaFixaHistorico.RetornaValorMercado(idClube, Financial.RendaFixa.Enums.TipoPapelTitulo.Publico, dataPosicao);
            decimal totalTitulosPrivados = posicaoRendaFixaHistorico.RetornaValorMercado(idClube, Financial.RendaFixa.Enums.TipoPapelTitulo.Privado, dataPosicao);
            decimal totalDebentures = posicaoRendaFixaHistorico.RetornaValorMercado(idClube, Financial.RendaFixa.Enums.ClasseRendaFixa.Debenture, dataPosicao);

            PosicaoRendaFixaHistoricoQuery posicaoRendaFixaHistoricoQuery = new PosicaoRendaFixaHistoricoQuery("PO");
            TituloRendaFixaQuery tituloRendaFixaQuery = new TituloRendaFixaQuery("TI");
            posicaoRendaFixaHistoricoQuery.Select(posicaoRendaFixaHistoricoQuery.ValorMercado.Sum());
            posicaoRendaFixaHistoricoQuery.InnerJoin(tituloRendaFixaQuery).On(tituloRendaFixaQuery.IdTitulo == posicaoRendaFixaHistoricoQuery.IdTitulo);
            posicaoRendaFixaHistoricoQuery.Where(posicaoRendaFixaHistoricoQuery.IdCliente == idClube,
                                                 posicaoRendaFixaHistoricoQuery.DataHistorico.Equal(dataPosicao),
                                                 tituloRendaFixaQuery.DebentureConversivel.Equal("S"));
            PosicaoRendaFixaHistorico posConv = new PosicaoRendaFixaHistorico();
            posConv.Load(posicaoRendaFixaHistoricoQuery);

            decimal totalDebenturesConv = posConv.ValorMercado.HasValue ? posConv.ValorMercado.Value : 0;
            totalDebentures -= totalDebenturesConv;

            decimal totalCDB = posicaoRendaFixaHistorico.RetornaValorMercado(idClube, Financial.RendaFixa.Enums.ClasseRendaFixa.CDB, dataPosicao);
            decimal totalOutrosTitulosPrivados = totalTitulosPrivados - totalDebentures - totalCDB;

            decimal totalPremiosOpcoes = Math.Abs(posicaoBolsaHistorico.RetornaValorCustoOpcoes(idClube, dataPosicao));

            decimal totalDisponivel = 0;
            saldoCaixa.BuscaSaldoCaixa(idClube, dataPosicao);
            if (saldoCaixa.SaldoFechamento.HasValue && saldoCaixa.SaldoFechamento.Value > 0)
            {
                totalDisponivel = saldoCaixa.SaldoFechamento.Value;
            }

            decimal valorPagar = Math.Abs(liquidacaoHistorico.RetornaLiquidacaoHistoricoPagar(idClube, dataPosicao));
            decimal valorReceber = liquidacaoHistorico.RetornaLiquidacaoHistoricoReceber(idClube, dataPosicao);

            DateTime dataInicioMes = Calendario.RetornaPrimeiroDiaCorridoMes(dataPosicao, 0);
            decimal totalAportes = operacaoCotista.RetornaSumValorBrutoAplicacao(idClube, dataInicioMes, dataPosicao);
            decimal totalResgates = operacaoCotista.RetornaSumValorBrutoResgate(idClube, dataInicioMes, dataPosicao);
            int numeroCotistas = posicaoCotistaHistorico.RetornaCountNumeroCotistas(idClube, dataPosicao);
            
            historicoCota.LoadByPrimaryKey(dataPosicao, idClube);
            decimal valorCota = historicoCota.CotaFechamento.HasValue ? historicoCota.CotaFechamento.Value : 0;
            decimal valorPL = historicoCota.PLFechamento.HasValue ? historicoCota.PLFechamento.Value : 0;

            decimal retornoMes = Math.Round(calculoMedida.CalculaRetornoMes(dataPosicao), 2);
            string sinalRetorno = retornoMes >= 0 ? "+" : "-";
            retornoMes = Math.Abs(retornoMes);

            d1.numeroRegistroClube = registroBovespa;
            d1.codigoAdministrador = codigoCorretora;

            d1.valorTotalAplicadoacoes = totalAcoes; //OK
            d1.valorAcoesGarantia = totalAcoesGarantia; //OK
            d1.valorAcoesEmprestimoDoador = totalDoador; //OK
            d1.valorAcoesEmprestimoTomador = totalTomador; //OK
            d1.valorTotalFundosCurtoPrazo = totalFundoCurtoPrazo; //OK
            d1.valorTotalFundosIndice = totalFundosIndice; //OK
            d1.valorTotalFundosRendaFixa = totalFundoRendaFixa; //OK
            d1.valorTotalFundosReferenciados = totalFundoReferenciado; //OK
            d1.valorTotalValoresMobilarios = totalOutrosTitulosPrivados; //OK
            d1.valorTotalDebenturesConversiveis = totalDebenturesConv; //OK
            d1.valorTotalDebenturesSimples = totalDebentures; //OK
            d1.valorTotalBonusSusbcricao = totalBonusSubscricao; //OK
            d1.valorTotalAplicadoRecibosSubscricao = totalReciboSubscricao; //OK
            d1.valorTotalAplicadoCertificadosAcoes = 0;  //Hoje não tem como definir
            d1.valorTotalTitulosPublicos = totalTitulosPublicos; //OK
            d1.valorTotalPremiosOpcoes = totalPremiosOpcoes; //OK
            d1.valorTotalTitulosInstituicaoFinanceira = totalCDB; //OK
            d1.contasAReceber = valorReceber; //OK
            d1.contasApagar = valorPagar; //OK
            d1.saldoDisponivelContaCorrente = totalDisponivel; //OK
            d1.aporteCotistas = totalAportes; //OK
            d1.resgateCotistas = totalResgates; //OK
            d1.numeroCotistas = numeroCotistas; //OK
            d1.patrimonioLiquido = valorPL; //OK
            d1.valorCota = valorCota; //OK
            d1.sinalRentabilidade = sinalRetorno; //OK
            d1.rentabilidade = retornoMes; //OK
            #endregion
            //
            informeDs.Add(d1);

            this.numeroRegistros++;
            //
            return this.WriteStream(typeof(Detalhe_Informe), informeDs);  
        }     
        #endregion

        #region Detalhe_Cotista
        private MemoryStream Monta_Detalhe_Cotista(DateTime dataPosicao, int idClube, string cnpj)
        {
            List<Detalhe_Cotista> cotistaDs = new List<Detalhe_Cotista>();
            
            PosicaoCotistaHistoricoCollection posicaoCotistaCollection = new PosicaoCotistaHistoricoCollection();
            PosicaoCotistaHistoricoQuery posicaoCotistaQuery = new PosicaoCotistaHistoricoQuery("P");
            CotistaQuery cotistaQuery = new CotistaQuery("C");
            PessoaQuery pessoaQuery = new PessoaQuery("E");
            posicaoCotistaQuery.Select(cotistaQuery.IdCotista, cotistaQuery.Nome, pessoaQuery.Cpfcnpj, posicaoCotistaQuery.Quantidade.Sum());
            posicaoCotistaQuery.InnerJoin(cotistaQuery).On(cotistaQuery.IdCotista == posicaoCotistaQuery.IdCotista);
            posicaoCotistaQuery.InnerJoin(pessoaQuery).On(cotistaQuery.IdPessoa == pessoaQuery.IdPessoa);
            posicaoCotistaQuery.Where(posicaoCotistaQuery.Quantidade.NotEqual(0),
                                      posicaoCotistaQuery.ValorBruto.NotEqual(0),
                                      posicaoCotistaQuery.IdCarteira.Equal(idClube),
                                      posicaoCotistaQuery.DataHistorico.Equal(dataPosicao));
            posicaoCotistaQuery.GroupBy(cotistaQuery.IdCotista,
                                        cotistaQuery.Nome,
                                        pessoaQuery.Cpfcnpj);
            posicaoCotistaCollection.Load(posicaoCotistaQuery);

            decimal quantidadeClube = 0;
            if (posicaoCotistaCollection.Count > 0)
            {
                HistoricoCota h = new HistoricoCota();
                quantidadeClube = h.RetornaQuantidadeCotas(idClube, dataPosicao);
            }

            //
            foreach (PosicaoCotistaHistorico posicaoCotista in posicaoCotistaCollection)
            {
                Detalhe_Cotista d1 = new Detalhe_Cotista();

                string nome = Convert.ToString(posicaoCotista.GetColumn(CotistaMetadata.ColumnNames.Nome));
                string cpfCnpj = Convert.ToString(posicaoCotista.GetColumn(PessoaMetadata.ColumnNames.Cpfcnpj));
                decimal quantidade = posicaoCotista.Quantidade.Value;
                decimal percentual = Math.Round(quantidade / quantidadeClube * 100M, 10);

                nome = Utilitario.RemoveAcentos(nome);
                nome = Utilitario.RemoveCaracteresEspeciais(nome);

                cpfCnpj = Utilitario.RemoveAcentos(cpfCnpj);
                cpfCnpj = Utilitario.RemoveCaracteresEspeciais(cpfCnpj);

                #region Monta Detalhe_Cotista
                d1.cnpjClube = cnpj;
                d1.nomeCotista = nome;
                d1.cpfCotista = cpfCnpj;
                d1.isPessoaVinculada = (int)PessoaVinculada.NaoVinculada;
                d1.quantidadeCotas = quantidade;
                d1.percentualCotistaClube = percentual;
                #endregion
                //
                cotistaDs.Add(d1);

                this.numeroRegistros++;
            }
            
            //
            return this.WriteStream(typeof(Detalhe_Cotista), cotistaDs);  
        } 
        #endregion

        #region Detalhe_CarteiraAtivo
        private MemoryStream Monta_Detalhe_CarteiraAtivo(DateTime dataPosicao, int idClube, string cnpj)
        {
            List<Detalhe_CarteiraAtivo> carteiraAtivoDs = new List<Detalhe_CarteiraAtivo>();

            PosicaoBolsaHistoricoCollection posicaoBolsaCollection = new PosicaoBolsaHistoricoCollection();
            PosicaoBolsaHistoricoQuery posicaoBolsaQuery = new PosicaoBolsaHistoricoQuery("P");
            AtivoBolsaQuery ativoBolsaQuery = new AtivoBolsaQuery("A");
            posicaoBolsaQuery.Select(ativoBolsaQuery.CdAtivoBolsa,
                                     ativoBolsaQuery.CodigoIsin,
                                     posicaoBolsaQuery.ValorMercado.Sum(),
                                     posicaoBolsaQuery.Quantidade.Sum(),
                                     posicaoBolsaQuery.PUMercado.Avg());
            posicaoBolsaQuery.InnerJoin(ativoBolsaQuery).On(ativoBolsaQuery.CdAtivoBolsa == posicaoBolsaQuery.CdAtivoBolsa);
            posicaoBolsaQuery.Where(posicaoBolsaQuery.IdCliente.Equal(idClube),
                                    posicaoBolsaQuery.Quantidade.GreaterThan(0),
                                    posicaoBolsaQuery.ValorMercado.GreaterThan(0),
                                    posicaoBolsaQuery.DataHistorico.Equal(dataPosicao),
                                    posicaoBolsaQuery.TipoMercado.Equal(TipoMercadoBolsa.MercadoVista));
            posicaoBolsaQuery.GroupBy(ativoBolsaQuery.CdAtivoBolsa,
                                      ativoBolsaQuery.CodigoIsin);
            posicaoBolsaQuery.OrderBy(ativoBolsaQuery.CdAtivoBolsa.Ascending);
            posicaoBolsaCollection.Load(posicaoBolsaQuery);

            #region Monta Detalhe_CarteiraAtivo
            foreach (PosicaoBolsaHistorico posicaoBolsa in posicaoBolsaCollection)
            {
                Detalhe_CarteiraAtivo d1 = new Detalhe_CarteiraAtivo();

                string cdAtivoBolsa = posicaoBolsa.CdAtivoBolsa;
                decimal quantidade = posicaoBolsa.Quantidade.Value;
                decimal valor = posicaoBolsa.ValorMercado.Value;
                decimal puMercado = posicaoBolsa.PUMercado.Value;

                PosicaoEmprestimoBolsaHistorico posicaoEmprestimoBolsaHistorico = new PosicaoEmprestimoBolsaHistorico();
                decimal quantidadeTomada = posicaoEmprestimoBolsaHistorico.RetornaQuantidade(idClube, dataPosicao, cdAtivoBolsa, PontaEmprestimoBolsa.Tomador);
                decimal valorTomado = posicaoEmprestimoBolsaHistorico.RetornaValorMercado(idClube, dataPosicao, cdAtivoBolsa, PontaEmprestimoBolsa.Tomador);

                quantidade -= quantidadeTomada;
                valor -= valorTomado;

                quantidade = Math.Abs(quantidade);
                valor = Math.Abs(valor);

                string codigoIsin = Convert.ToString(posicaoBolsa.GetColumn(AtivoBolsaMetadata.ColumnNames.CodigoIsin));

                FatorCotacaoBolsa fatorCotacaoBolsa = new FatorCotacaoBolsa();
                fatorCotacaoBolsa.BuscaFatorCotacaoBolsa(cdAtivoBolsa, dataPosicao);
                
                int distribuicao = 0; 
                DistribuicaoBolsa distribuicaoBolsa = new DistribuicaoBolsa();
                if (distribuicaoBolsa.BuscaDistribuicaoBolsa(cdAtivoBolsa, dataPosicao))
                {
                    distribuicao = distribuicaoBolsa.Distribuicao.Value;
                }
                if (distribuicao == 0)
                {
                    distribuicao = 100; //Se não tiver distribuição assume o default = 100 (1a distribuição)
                }

                
                
                PosicaoBolsaDetalhe posicaoBolsaDetalhe = new PosicaoBolsaDetalhe();
                posicaoBolsaDetalhe.Query.Select(posicaoBolsaDetalhe.Query.Quantidade.Sum());
                posicaoBolsaDetalhe.Query.Where(posicaoBolsaDetalhe.Query.IdCliente.Equal(idClube),
                                                posicaoBolsaDetalhe.Query.CdAtivoBolsa.Equal(cdAtivoBolsa),
                                                posicaoBolsaDetalhe.Query.DataHistorico.Equal(dataPosicao),
                                                posicaoBolsaDetalhe.Query.TipoCarteira.Equal((int)TipoCarteiraBovespa.CoberturaOpcoes));
                posicaoBolsaDetalhe.Query.Load();

                decimal quantidadeCoberturaOpcoes = posicaoBolsaDetalhe.Quantidade.HasValue ? posicaoBolsaDetalhe.Quantidade.Value : 0;
                decimal valorCoberturaOpcoes = Utilitario.Truncate(quantidadeCoberturaOpcoes * puMercado / fatorCotacaoBolsa.Fator.Value, 2);

                posicaoBolsaDetalhe = new PosicaoBolsaDetalhe();
                posicaoBolsaDetalhe.Query.Select(posicaoBolsaDetalhe.Query.Quantidade.Sum());
                posicaoBolsaDetalhe.Query.Where(posicaoBolsaDetalhe.Query.IdCliente.Equal(idClube),
                                                posicaoBolsaDetalhe.Query.CdAtivoBolsa.Equal(cdAtivoBolsa),
                                                posicaoBolsaDetalhe.Query.DataHistorico.Equal(dataPosicao),
                                                posicaoBolsaDetalhe.Query.TipoCarteira.Equal((int)TipoCarteiraBovespa.GarantiasGerais));
                posicaoBolsaDetalhe.Query.Load();

                decimal quantidadeGarantias = posicaoBolsaDetalhe.Quantidade.HasValue ? posicaoBolsaDetalhe.Quantidade.Value : 0;
                decimal valorGarantias = Utilitario.Truncate(quantidadeGarantias * puMercado / fatorCotacaoBolsa.Fator.Value, 2);

                posicaoBolsaDetalhe = new PosicaoBolsaDetalhe();
                posicaoBolsaDetalhe.Query.Select(posicaoBolsaDetalhe.Query.Quantidade.Sum());
                posicaoBolsaDetalhe.Query.Where(posicaoBolsaDetalhe.Query.IdCliente.Equal(idClube),
                                                posicaoBolsaDetalhe.Query.CdAtivoBolsa.Equal(cdAtivoBolsa),
                                                posicaoBolsaDetalhe.Query.DataHistorico.Equal(dataPosicao),
                                                posicaoBolsaDetalhe.Query.TipoCarteira.Equal((int)TipoCarteiraBovespa.GarantiaBMF));
                posicaoBolsaDetalhe.Query.Load();

                decimal quantidadeGarantiasBMF = posicaoBolsaDetalhe.Quantidade.HasValue ? posicaoBolsaDetalhe.Quantidade.Value : 0;
                decimal valorGarantiasBMF = Utilitario.Truncate(quantidadeGarantias * puMercado / fatorCotacaoBolsa.Fator.Value, 2);

                quantidade = quantidade - (quantidadeCoberturaOpcoes + quantidadeGarantias + quantidadeGarantiasBMF);
                valor = valor - (valorCoberturaOpcoes + valorGarantias + valorGarantiasBMF);

                if (quantidade != 0)
                {
                    #region Monta Detalhe_CarteiraAtivo
                    d1.cnpjClube = cnpj;
                    d1.codigoAtivo = cdAtivoBolsa;
                    d1.codigoIsin = codigoIsin;
                    d1.distribuicao = distribuicao;
                    d1.quantidade = quantidade;
                    d1.valor = valor;
                    d1.isCustodia = (int)CustodiaBVMF.Nao;
                    d1.fatorCotacao = (int)fatorCotacaoBolsa.Fator.Value;
                    d1.codigoCarteira = (int)TipoCarteiraBovespa.Livre; //CARTEIRA LIVRE
                    #endregion
                    
                    carteiraAtivoDs.Add(d1);

                    this.numeroRegistros++;
                }

                if (quantidadeCoberturaOpcoes != 0)
                {
                    #region Monta Detalhe_CarteiraAtivo
                    d1.cnpjClube = cnpj;
                    d1.codigoAtivo = cdAtivoBolsa;
                    d1.codigoIsin = codigoIsin;
                    d1.distribuicao = distribuicao;
                    d1.quantidade = quantidadeCoberturaOpcoes;
                    d1.valor = valorCoberturaOpcoes;
                    d1.isCustodia = (int)CustodiaBVMF.Nao;
                    d1.fatorCotacao = (int)fatorCotacaoBolsa.Fator.Value;
                    d1.codigoCarteira = (int)TipoCarteiraBovespa.CoberturaOpcoes;
                    #endregion

                    carteiraAtivoDs.Add(d1);

                    this.numeroRegistros++;
                }

                if (quantidadeGarantias != 0)
                {
                    #region Monta Detalhe_CarteiraAtivo
                    d1.cnpjClube = cnpj;
                    d1.codigoAtivo = cdAtivoBolsa;
                    d1.codigoIsin = codigoIsin;
                    d1.distribuicao = distribuicao;
                    d1.quantidade = quantidadeGarantias;
                    d1.valor = valorGarantias;
                    d1.isCustodia = (int)CustodiaBVMF.Nao;
                    d1.fatorCotacao = (int)fatorCotacaoBolsa.Fator.Value;
                    d1.codigoCarteira = (int)TipoCarteiraBovespa.GarantiasGerais;
                    #endregion

                    carteiraAtivoDs.Add(d1);

                    this.numeroRegistros++;
                }

                if (quantidadeGarantiasBMF != 0)
                {
                    #region Monta Detalhe_CarteiraAtivo
                    d1.cnpjClube = cnpj;
                    d1.codigoAtivo = cdAtivoBolsa;
                    d1.codigoIsin = codigoIsin;
                    d1.distribuicao = distribuicao;
                    d1.quantidade = quantidadeGarantiasBMF;
                    d1.valor = valorGarantiasBMF;
                    d1.isCustodia = (int)CustodiaBVMF.Nao;
                    d1.fatorCotacao = (int)fatorCotacaoBolsa.Fator.Value;
                    d1.codigoCarteira = (int)TipoCarteiraBovespa.GarantiaBMF;
                    #endregion

                    carteiraAtivoDs.Add(d1);

                    this.numeroRegistros++;
                }
            }
            
            #endregion
            
            //
            return this.WriteStream(typeof(Detalhe_CarteiraAtivo), carteiraAtivoDs);  
        } 
        #endregion

        #region Detalhe_Opcoes
        private MemoryStream Monta_Detalhe_Opcoes(DateTime dataPosicao, int idClube, string cnpj)
        {
            List<Detalhe_Opcoes> opcoesDs = new List<Detalhe_Opcoes>();
            //

            #region Opções Compradas
            PosicaoBolsaHistoricoCollection posicaoBolsaCollectionTitular = new PosicaoBolsaHistoricoCollection();
            PosicaoBolsaHistoricoQuery posicaoBolsaQuery = new PosicaoBolsaHistoricoQuery("P");
            AtivoBolsaQuery ativoBolsaQuery = new AtivoBolsaQuery("A");
            posicaoBolsaQuery.Select(ativoBolsaQuery.CdAtivoBolsa,
                                     ativoBolsaQuery.CdAtivoBolsaObjeto,
                                     ativoBolsaQuery.TipoMercado,
                                     ativoBolsaQuery.NumeroSerie,
                                     ativoBolsaQuery.PUExercicio,
                                     ativoBolsaQuery.DataVencimento,
                                     posicaoBolsaQuery.PUCusto.Avg(),
                                     posicaoBolsaQuery.Quantidade.Sum());
            posicaoBolsaQuery.InnerJoin(ativoBolsaQuery).On(ativoBolsaQuery.CdAtivoBolsa == posicaoBolsaQuery.CdAtivoBolsa);
            posicaoBolsaQuery.Where(posicaoBolsaQuery.IdCliente.Equal(idClube),
                                    posicaoBolsaQuery.Quantidade.GreaterThan(0),
                                    posicaoBolsaQuery.ValorMercado.GreaterThan(0),
                                    posicaoBolsaQuery.DataHistorico.Equal(dataPosicao),
                                    posicaoBolsaQuery.TipoMercado.In(TipoMercadoBolsa.OpcaoCompra,TipoMercadoBolsa.OpcaoVenda));
            posicaoBolsaQuery.GroupBy(ativoBolsaQuery.CdAtivoBolsa,
                                     ativoBolsaQuery.CdAtivoBolsaObjeto,
                                     ativoBolsaQuery.TipoMercado,
                                     ativoBolsaQuery.NumeroSerie,
                                     ativoBolsaQuery.PUExercicio,
                                     ativoBolsaQuery.DataVencimento);
            posicaoBolsaCollectionTitular.Load(posicaoBolsaQuery);

            foreach (PosicaoBolsaHistorico posicaoBolsa in posicaoBolsaCollectionTitular)
            {
                Detalhe_Opcoes d1 = new Detalhe_Opcoes();

                string cdAtivoBolsa = posicaoBolsa.CdAtivoBolsa;
                decimal quantidade = posicaoBolsa.Quantidade.Value;
                decimal puCusto = posicaoBolsa.PUCusto.Value;

                string cdAtivoBolsaObjeto = "";
                if (Convert.IsDBNull(posicaoBolsa.GetColumn(AtivoBolsaMetadata.ColumnNames.CdAtivoBolsaObjeto)))
                {
                    throw new Exception("Ativo objeto não cadastrado para a opção " + cdAtivoBolsa);                    
                }

                cdAtivoBolsaObjeto = Convert.ToString(posicaoBolsa.GetColumn(AtivoBolsaMetadata.ColumnNames.CdAtivoBolsaObjeto));
                AtivoBolsa ativoBolsaObjeto = new AtivoBolsa();
                if (!ativoBolsaObjeto.LoadByPrimaryKey(cdAtivoBolsaObjeto))
                {
                    throw new Exception("Ativo não cadastrado - " + cdAtivoBolsaObjeto);
                }

                FatorCotacaoBolsa fatorCotacaoBolsa = new FatorCotacaoBolsa();
                if (!fatorCotacaoBolsa.BuscaFatorCotacaoBolsa(cdAtivoBolsa, dataPosicao))
                {
                    throw new Exception("Fator de cotação bolsa não encontrado para a opção " + cdAtivoBolsa);
                }
                
                int distribuicao = 0;
                DistribuicaoBolsa distribuicaoBolsa = new DistribuicaoBolsa();
                if (distribuicaoBolsa.BuscaDistribuicaoBolsa(cdAtivoBolsaObjeto, dataPosicao))
                {
                    distribuicao = distribuicaoBolsa.Distribuicao.Value;
                }

                decimal premio = Utilitario.Truncate(quantidade * puCusto / fatorCotacaoBolsa.Fator.Value, 2);

                #region Monta Detalhe_Opcoes
                //d1.tipoRegistro = "1";
                d1.cnpjClube = cnpj;
                d1.codigoIsinAtivoObjeto = ativoBolsaObjeto.CodigoIsin;
                d1.distribuicao = distribuicao;
                d1.tipoOpcao = Convert.ToString(posicaoBolsa.GetColumn(AtivoBolsaMetadata.ColumnNames.TipoMercado)) == TipoMercadoBolsa.OpcaoCompra ? "C" : "P";
                d1.serie = Convert.ToInt32(posicaoBolsa.GetColumn(AtivoBolsaMetadata.ColumnNames.NumeroSerie));
                d1.codigoOpcao = Convert.ToString(posicaoBolsa.GetColumn(AtivoBolsaMetadata.ColumnNames.CdAtivoBolsa));
                d1.puExercicio = Convert.ToDecimal(posicaoBolsa.GetColumn(AtivoBolsaMetadata.ColumnNames.PUExercicio));
                d1.valorPremio = premio;
                d1.dataVencimento = Convert.ToDateTime(posicaoBolsa.GetColumn(AtivoBolsaMetadata.ColumnNames.DataVencimento));
                d1.quantidade = (int)posicaoBolsa.Quantidade.Value;
                d1.indicadorPosicao = StringEnum.GetStringValue(IndicadorPosicaoOpcoes.Titular);
                #endregion
                //
                opcoesDs.Add(d1);

                this.numeroRegistros++;
            }
            #endregion

            #region Opções Vendidas
            posicaoBolsaCollectionTitular = new PosicaoBolsaHistoricoCollection();
            posicaoBolsaQuery = new PosicaoBolsaHistoricoQuery("P");
            ativoBolsaQuery = new AtivoBolsaQuery("A");
            posicaoBolsaQuery.Select(ativoBolsaQuery.CdAtivoBolsa,
                                     ativoBolsaQuery.CdAtivoBolsaObjeto,
                                     ativoBolsaQuery.TipoMercado,
                                     ativoBolsaQuery.NumeroSerie,
                                     ativoBolsaQuery.PUExercicio,
                                     ativoBolsaQuery.DataVencimento,
                                     posicaoBolsaQuery.PUCusto.Avg(),
                                     posicaoBolsaQuery.Quantidade.Sum());
            posicaoBolsaQuery.InnerJoin(ativoBolsaQuery).On(ativoBolsaQuery.CdAtivoBolsa == posicaoBolsaQuery.CdAtivoBolsa);
            posicaoBolsaQuery.Where(posicaoBolsaQuery.IdCliente.Equal(idClube),
                                    posicaoBolsaQuery.Quantidade.LessThan(0),
                                    posicaoBolsaQuery.ValorMercado.LessThan(0),
                                    posicaoBolsaQuery.DataHistorico.Equal(dataPosicao),
                                    posicaoBolsaQuery.TipoMercado.In(TipoMercadoBolsa.OpcaoCompra, TipoMercadoBolsa.OpcaoVenda));
            posicaoBolsaQuery.GroupBy(ativoBolsaQuery.CdAtivoBolsa,
                                     ativoBolsaQuery.CdAtivoBolsaObjeto,
                                     ativoBolsaQuery.TipoMercado,
                                     ativoBolsaQuery.NumeroSerie,
                                     ativoBolsaQuery.PUExercicio,
                                     ativoBolsaQuery.DataVencimento);
            posicaoBolsaCollectionTitular.Load(posicaoBolsaQuery);

            foreach (PosicaoBolsaHistorico posicaoBolsa in posicaoBolsaCollectionTitular)
            {
                Detalhe_Opcoes d1 = new Detalhe_Opcoes();

                string cdAtivoBolsa = posicaoBolsa.CdAtivoBolsa;
                decimal quantidade = Math.Abs(posicaoBolsa.Quantidade.Value);
                decimal puCusto = posicaoBolsa.PUCusto.Value;

                string cdAtivoBolsaObjeto = "";
                if (Convert.IsDBNull(posicaoBolsa.GetColumn(AtivoBolsaMetadata.ColumnNames.CdAtivoBolsaObjeto)))
                {
                    throw new Exception("Ativo objeto não cadastrado para a opção " + cdAtivoBolsa);
                }

                cdAtivoBolsaObjeto = Convert.ToString(posicaoBolsa.GetColumn(AtivoBolsaMetadata.ColumnNames.CdAtivoBolsaObjeto));
                AtivoBolsa ativoBolsaObjeto = new AtivoBolsa();
                if (!ativoBolsaObjeto.LoadByPrimaryKey(cdAtivoBolsaObjeto))
                {
                    throw new Exception("Ativo não cadastrado - " + cdAtivoBolsaObjeto);
                }

                FatorCotacaoBolsa fatorCotacaoBolsa = new FatorCotacaoBolsa();
                if (!fatorCotacaoBolsa.BuscaFatorCotacaoBolsa(cdAtivoBolsa, dataPosicao))
                {
                    throw new Exception("Fator de cotação bolsa não encontrado para a opção " + cdAtivoBolsa);
                }

                int distribuicao = 0;
                DistribuicaoBolsa distribuicaoBolsa = new DistribuicaoBolsa();
                if (distribuicaoBolsa.BuscaDistribuicaoBolsa(cdAtivoBolsaObjeto, dataPosicao))
                {
                    distribuicao = distribuicaoBolsa.Distribuicao.Value;
                }

                decimal premio = Utilitario.Truncate(quantidade * puCusto / fatorCotacaoBolsa.Fator.Value, 2);

                #region Monta Detalhe_Opcoes
                //d1.tipoRegistro = "1";
                d1.cnpjClube = cnpj;
                d1.codigoIsinAtivoObjeto = ativoBolsaObjeto.CodigoIsin;
                d1.distribuicao = distribuicao;
                d1.tipoOpcao = Convert.ToString(posicaoBolsa.GetColumn(AtivoBolsaMetadata.ColumnNames.TipoMercado)) == TipoMercadoBolsa.OpcaoCompra ? "C" : "P";
                d1.serie = Convert.ToInt32(posicaoBolsa.GetColumn(AtivoBolsaMetadata.ColumnNames.NumeroSerie));
                d1.codigoOpcao = Convert.ToString(posicaoBolsa.GetColumn(AtivoBolsaMetadata.ColumnNames.CdAtivoBolsa));
                d1.puExercicio = Convert.ToDecimal(posicaoBolsa.GetColumn(AtivoBolsaMetadata.ColumnNames.PUExercicio));
                d1.valorPremio = premio;
                d1.dataVencimento = Convert.ToDateTime(posicaoBolsa.GetColumn(AtivoBolsaMetadata.ColumnNames.DataVencimento));
                d1.quantidade = (int)quantidade;
                d1.indicadorPosicao = StringEnum.GetStringValue(IndicadorPosicaoOpcoes.Titular);
                #endregion
                //
                opcoesDs.Add(d1);

                this.numeroRegistros++;
            }
            #endregion

            //
            return this.WriteStream(typeof(Detalhe_Opcoes), opcoesDs);  
        } 
        #endregion

        #region Detalhe_Termo
        private MemoryStream Monta_Detalhe_Termo(DateTime dataPosicao, int idClube, string cnpj)
        {
            List<Detalhe_Termo> termoDs = new List<Detalhe_Termo>();                                    
            //
            #region Monta Detalhe Termo
            PosicaoTermoBolsaHistoricoCollection posicaoTermoBolsaHistoricoCollection = new PosicaoTermoBolsaHistoricoCollection();
            PosicaoTermoBolsaHistoricoQuery posicaoTermoBolsaQuery = new PosicaoTermoBolsaHistoricoQuery("P");
            AtivoBolsaQuery ativoBolsaQuery = new AtivoBolsaQuery("A");
            posicaoTermoBolsaQuery.Select(ativoBolsaQuery.CdAtivoBolsa,
                                     ativoBolsaQuery.CdAtivoBolsaObjeto,
                                     ativoBolsaQuery.DataVencimento,
                                     posicaoTermoBolsaQuery.PUTermo,
                                     posicaoTermoBolsaQuery.ValorTermo,
                                     posicaoTermoBolsaQuery.Quantidade,
                                     posicaoTermoBolsaQuery.NumeroContrato,
                                     posicaoTermoBolsaQuery.TipoTermo);
            posicaoTermoBolsaQuery.InnerJoin(ativoBolsaQuery).On(ativoBolsaQuery.CdAtivoBolsa == posicaoTermoBolsaQuery.CdAtivoBolsa);
            posicaoTermoBolsaQuery.Where(posicaoTermoBolsaQuery.IdCliente.Equal(idClube),
                                    posicaoTermoBolsaQuery.Quantidade.NotEqual(0),
                                    posicaoTermoBolsaQuery.ValorMercado.NotEqual(0),
                                    posicaoTermoBolsaQuery.DataHistorico.Equal(dataPosicao));
            posicaoTermoBolsaQuery.OrderBy(ativoBolsaQuery.CdAtivoBolsa.Ascending,
                                           ativoBolsaQuery.DataVencimento.Ascending);
            posicaoTermoBolsaHistoricoCollection.Load(posicaoTermoBolsaQuery);

            foreach (PosicaoTermoBolsaHistorico posicaoTermoBolsa in posicaoTermoBolsaHistoricoCollection)
            {
                Detalhe_Termo d1 = new Detalhe_Termo();

                string cdAtivoBolsa = posicaoTermoBolsa.CdAtivoBolsa;
                decimal quantidade = posicaoTermoBolsa.Quantidade.Value;
                decimal puTermo = posicaoTermoBolsa.PUTermo.Value;
                string numeroContrato = posicaoTermoBolsa.NumeroContrato.Replace("-", "").Replace(".", "").Replace(" ", "").Trim();
                decimal valor = posicaoTermoBolsa.ValorTermo.Value;
                byte tipoTermo = posicaoTermoBolsa.TipoTermo.Value;

                string cdAtivoBolsaObjeto = "";
                if (Convert.IsDBNull(posicaoTermoBolsa.GetColumn(AtivoBolsaMetadata.ColumnNames.CdAtivoBolsaObjeto)))
                {
                    throw new Exception("Ativo objeto não cadastrado para o ativo " + cdAtivoBolsa);
                }

                cdAtivoBolsaObjeto = Convert.ToString(posicaoTermoBolsa.GetColumn(AtivoBolsaMetadata.ColumnNames.CdAtivoBolsaObjeto));

                AtivoBolsa ativoBolsaObjeto = new AtivoBolsa();
                if (!ativoBolsaObjeto.LoadByPrimaryKey(cdAtivoBolsaObjeto))
                {
                    throw new Exception("Ativo não cadastrado - " + cdAtivoBolsaObjeto);
                }

                FatorCotacaoBolsa fatorCotacaoBolsa = new FatorCotacaoBolsa();
                if (!fatorCotacaoBolsa.BuscaFatorCotacaoBolsa(cdAtivoBolsa, dataPosicao))
                {
                    throw new Exception("Fator de cotação bolsa não encontrado para o ativo " + cdAtivoBolsa);
                }

                int distribuicao = 0;
                DistribuicaoBolsa distribuicaoBolsa = new DistribuicaoBolsa();
                if (distribuicaoBolsa.BuscaDistribuicaoBolsa(cdAtivoBolsaObjeto, dataPosicao))
                {
                    distribuicao = distribuicaoBolsa.Distribuicao.Value;
                }

                #region Monta Detalhe_Termo
                //d1.tipoRegistro = "1";
                d1.cnpjClube = cnpj;
                d1.numeroContrato = 0;

                if (numeroContrato != "")
                {
                    d1.numeroContrato = Convert.ToInt32(numeroContrato);
                }

                d1.codigoIsinAtivoObjeto = ativoBolsaObjeto.CodigoIsin;
                d1.distribuicao = distribuicao;
                d1.quantidade = Math.Abs((int)posicaoTermoBolsa.Quantidade.Value);
                d1.puAtivoObjeto = puTermo;
                d1.valorContrato = Math.Abs(valor);
                d1.dataVencimento = Convert.ToDateTime(posicaoTermoBolsa.GetColumn(AtivoBolsaMetadata.ColumnNames.DataVencimento));

                if (posicaoTermoBolsa.Quantidade.Value > 0)
                {
                    d1.indicadorPosicao = StringEnum.GetStringValue(IndicadorPosicaoTermo.Comprado);
                }
                else
                {
                    d1.indicadorPosicao = StringEnum.GetStringValue(IndicadorPosicaoTermo.Vendido);
                }

                d1.tipoTermo = (int)tipoTermo;
                d1.fatorCotacaoAtivoObjeto = (int)fatorCotacaoBolsa.Fator.Value;
                #endregion
                //
                termoDs.Add(d1);

                this.numeroRegistros++;
            }
            #endregion
            //
            return this.WriteStream(typeof(Detalhe_Termo), termoDs);  
        }
        #endregion

        #region Detalhe_EmprestimoAcoes
        private MemoryStream Monta_Detalhe_EmprestimoAcoes(DateTime dataPosicao, int idClube, string cnpj)
        {
            List<Detalhe_EmprestimoAcoes> emprestimoAcoesDs = new List<Detalhe_EmprestimoAcoes>();

            #region Monta Detalhe Emprestimo
            PosicaoEmprestimoBolsaHistoricoCollection posicaoEmprestimoBolsaHistoricoCollection = new PosicaoEmprestimoBolsaHistoricoCollection();
            PosicaoEmprestimoBolsaHistoricoQuery posicaoEmprestimoBolsaQuery = new PosicaoEmprestimoBolsaHistoricoQuery("P");
            AtivoBolsaQuery ativoBolsaQuery = new AtivoBolsaQuery("A");
            posicaoEmprestimoBolsaQuery.Select(ativoBolsaQuery.CdAtivoBolsa,
                                             ativoBolsaQuery.CodigoIsin,
                                             posicaoEmprestimoBolsaQuery);
            posicaoEmprestimoBolsaQuery.InnerJoin(ativoBolsaQuery).On(ativoBolsaQuery.CdAtivoBolsa == posicaoEmprestimoBolsaQuery.CdAtivoBolsa);
            posicaoEmprestimoBolsaQuery.Where(posicaoEmprestimoBolsaQuery.IdCliente.Equal(idClube),
                                    posicaoEmprestimoBolsaQuery.Quantidade.NotEqual(0),
                                    posicaoEmprestimoBolsaQuery.DataHistorico.Equal(dataPosicao));
            posicaoEmprestimoBolsaQuery.OrderBy(ativoBolsaQuery.CdAtivoBolsa.Ascending,
                                                posicaoEmprestimoBolsaQuery.DataVencimento.Ascending);
            posicaoEmprestimoBolsaHistoricoCollection.Load(posicaoEmprestimoBolsaQuery);

            foreach (PosicaoEmprestimoBolsaHistorico posicaoEmprestimoBolsa in posicaoEmprestimoBolsaHistoricoCollection)
            {
                Detalhe_EmprestimoAcoes d1 = new Detalhe_EmprestimoAcoes();

                string cdAtivoBolsa = posicaoEmprestimoBolsa.CdAtivoBolsa;
                string codigoIsin = Convert.ToString(posicaoEmprestimoBolsa.GetColumn(AtivoBolsaMetadata.ColumnNames.CodigoIsin));
                int quantidade = (int)posicaoEmprestimoBolsa.Quantidade.Value;                
                int numeroContrato = posicaoEmprestimoBolsa.NumeroContrato.Value;
                DateTime dataRegistro = posicaoEmprestimoBolsa.DataRegistro.Value;
                DateTime dataVencimento = posicaoEmprestimoBolsa.DataVencimento.Value;
                decimal taxa = posicaoEmprestimoBolsa.TaxaOperacao.Value;

                decimal cotacao = 0;
                CotacaoBolsa cotacaoBolsa = new CotacaoBolsa();
                if (cotacaoBolsa.BuscaCotacaoBolsa(cdAtivoBolsa, dataPosicao))
                {
                    cotacao = cotacaoBolsa.PUFechamento.Value;                                        
                }
                else if (cotacaoBolsa.BuscaUltimaCotacaoBolsa(cdAtivoBolsa, dataPosicao)) 
                {
                    cotacao = cotacaoBolsa.PUFechamento.Value;
                }

                FatorCotacaoBolsa fatorCotacaoBolsa = new FatorCotacaoBolsa();
                if (!fatorCotacaoBolsa.BuscaFatorCotacaoBolsa(cdAtivoBolsa, dataPosicao))
                {
                    throw new Exception("Fator de cotação bolsa não encontrado para a opção " + cdAtivoBolsa);
                }

                int distribuicao = 0;
                DistribuicaoBolsa distribuicaoBolsa = new DistribuicaoBolsa();
                if (distribuicaoBolsa.BuscaDistribuicaoBolsa(cdAtivoBolsa, dataPosicao))
                {
                    distribuicao = distribuicaoBolsa.Distribuicao.Value;
                }

                #region Monta Detalhe_EmprestimoAcoes
                //d1.tipoRegistro = "1";
                d1.cnpjClube = cnpj;
                d1.numeroContrato = numeroContrato;
                d1.codigoIsinAtivoObjeto = codigoIsin;
                d1.distribuicao = distribuicao;
                d1.quantidade = Math.Abs(quantidade);
                d1.valorCotacao = cotacao;
                d1.opcaoCotacao = (int)OpcaoCotacao.Nao;
                d1.dataVencimento = dataVencimento;
                d1.taxaContrato = taxa;

                if (posicaoEmprestimoBolsa.PontaEmprestimo.Value == (byte)PontaEmprestimoBolsa.Doador)
                {
                    d1.indicadorPosicao = StringEnum.GetStringValue(IndicadorPosicaoEmprestimoAcoes.Doador);
                }
                else
                {
                    d1.indicadorPosicao = StringEnum.GetStringValue(IndicadorPosicaoEmprestimoAcoes.Tomador);
                }

                d1.fatorCotacaoAtivoObjeto = (int)fatorCotacaoBolsa.Fator.Value;
                d1.codigoCarteira = 21016; //CARTEIRA LIVRE
                #endregion
                //
                emprestimoAcoesDs.Add(d1);

                this.numeroRegistros++;
            }
            #endregion
            //
            return this.WriteStream(typeof(Detalhe_EmprestimoAcoes), emprestimoAcoesDs);  
        }
        #endregion

        #region Detalhe_Futuros
        private MemoryStream Monta_Detalhe_Futuros(DateTime dataPosicao, int idClube, string cnpj)
        {
            List<Detalhe_Futuros> futurosDs = new List<Detalhe_Futuros>();

            PosicaoBMFHistoricoCollection posicaoBMFCollection = new PosicaoBMFHistoricoCollection();
            PosicaoBMFHistoricoQuery posicaoBMFQuery = new PosicaoBMFHistoricoQuery("P");
            AtivoBMFQuery ativoBMFQuery = new AtivoBMFQuery("A");
            posicaoBMFQuery.Select(ativoBMFQuery.CodigoIsin,
                                   posicaoBMFQuery.CdAtivoBMF,
                                   posicaoBMFQuery.Serie,
                                   posicaoBMFQuery.DataVencimento,
                                   posicaoBMFQuery.PUMercado.Avg(),
                                   posicaoBMFQuery.Quantidade.Sum());
            //
            posicaoBMFQuery.InnerJoin(ativoBMFQuery).On(ativoBMFQuery.CdAtivoBMF == posicaoBMFQuery.CdAtivoBMF &&
                                                        ativoBMFQuery.Serie == posicaoBMFQuery.Serie);
            //
            posicaoBMFQuery.Where(posicaoBMFQuery.IdCliente.Equal(idClube),
                                  posicaoBMFQuery.Quantidade.NotEqual(0),
                                  posicaoBMFQuery.DataHistorico.Equal(dataPosicao),
                                  posicaoBMFQuery.TipoMercado.Equal((byte)TipoMercadoBMF.Futuro));
            posicaoBMFQuery.GroupBy(ativoBMFQuery.CodigoIsin,
                                    posicaoBMFQuery.CdAtivoBMF,
                                    posicaoBMFQuery.Serie,
                                    posicaoBMFQuery.DataVencimento);
            posicaoBMFQuery.OrderBy(posicaoBMFQuery.CdAtivoBMF.Ascending,
                                    posicaoBMFQuery.Serie.Ascending);
            posicaoBMFCollection.Load(posicaoBMFQuery);

            #region Monta Monta_Detalhe_Futuros
            foreach (PosicaoBMFHistorico posicaoBMF in posicaoBMFCollection)
            {
                Detalhe_Futuros d1 = new Detalhe_Futuros();
                                
                string cdAtivoBMF = posicaoBMF.CdAtivoBMF;
                string serie = posicaoBMF.Serie;
                DateTime dataVencimento = posicaoBMF.DataVencimento.Value;
                string codigoIsin = Convert.ToString(posicaoBMF.GetColumn(AtivoBMFMetadata.ColumnNames.CodigoIsin));
                decimal quantidade = posicaoBMF.Quantidade.Value;
                decimal pu = posicaoBMF.PUMercado.Value;

                #region Monta Detalhe_Futuros
                //d1.tipoRegistro = "1";
                d1.cnpjClube = cnpj;
                d1.codigoAtivo = cdAtivoBMF+serie;
                d1.serie = 1; //POR ORA JOGA 1
                d1.codigoIsinAtivoObjeto = codigoIsin;
                d1.quantidade = (int)Math.Abs(quantidade);
                d1.puAjuste = pu;
                d1.dataVencimento = dataVencimento;
                
                if (quantidade > 0)
                {
                    d1.indicadorPosicao = StringEnum.GetStringValue(IndicadorPosicaoFuturo.Comprado);
                }
                else
                {
                    d1.indicadorPosicao = StringEnum.GetStringValue(IndicadorPosicaoFuturo.Vendido);
                }
                #endregion
                //
                futurosDs.Add(d1);

                this.numeroRegistros++;
            }

            #endregion
            //
            return this.WriteStream(typeof(Detalhe_Futuros), futurosDs);  
        }
        #endregion

        #region Detalhe_FundoInvestimento
        private MemoryStream Monta_Detalhe_FundoInvestimento(DateTime dataPosicao, int idClube, string cnpj)
        {
            List<Detalhe_FundoInvestimento> fundoInvestimentoDs = new List<Detalhe_FundoInvestimento>();

            PosicaoFundoHistoricoCollection posicaoFundoHistoricoCollection = new PosicaoFundoHistoricoCollection();
            PosicaoFundoHistoricoQuery posicaoFundoHistoricoQuery = new PosicaoFundoHistoricoQuery("P");
            CarteiraQuery carteiraQuery = new CarteiraQuery("C");
            PessoaQuery pessoaQuery = new PessoaQuery("E");
            posicaoFundoHistoricoQuery.Select(posicaoFundoHistoricoQuery.IdCarteira,
                                             carteiraQuery.Nome, 
                                             pessoaQuery.Cpfcnpj,
                                             posicaoFundoHistoricoQuery.Quantidade.Sum(),
                                             posicaoFundoHistoricoQuery.ValorBruto.Sum(),
                                             posicaoFundoHistoricoQuery.CotaDia.Avg());
            posicaoFundoHistoricoQuery.InnerJoin(carteiraQuery).On(carteiraQuery.IdCarteira == posicaoFundoHistoricoQuery.IdCarteira);
            posicaoFundoHistoricoQuery.InnerJoin(pessoaQuery).On(carteiraQuery.IdCarteira == pessoaQuery.IdPessoa);
            posicaoFundoHistoricoQuery.Where(posicaoFundoHistoricoQuery.Quantidade.NotEqual(0),
                                          posicaoFundoHistoricoQuery.ValorBruto.NotEqual(0),  
                                          posicaoFundoHistoricoQuery.IdCliente.Equal(idClube),
                                          posicaoFundoHistoricoQuery.DataHistorico.Equal(dataPosicao));
            posicaoFundoHistoricoQuery.GroupBy(posicaoFundoHistoricoQuery.IdCarteira,
                                      carteiraQuery.Nome,
                                      pessoaQuery.Cpfcnpj);
            posicaoFundoHistoricoQuery.OrderBy(carteiraQuery.Nome.Ascending);
            posicaoFundoHistoricoCollection.Load(posicaoFundoHistoricoQuery);

            foreach (PosicaoFundoHistorico posicaoFundo in posicaoFundoHistoricoCollection)
            {
                Detalhe_FundoInvestimento d1 = new Detalhe_FundoInvestimento();

                int idCarteira = posicaoFundo.IdCarteira.Value;
                string nome = Convert.ToString(posicaoFundo.GetColumn(CarteiraMetadata.ColumnNames.Nome));
                string cnpjFundo = Convert.ToString(posicaoFundo.GetColumn(PessoaMetadata.ColumnNames.Cpfcnpj));
                decimal quantidade = posicaoFundo.Quantidade.Value;
                decimal valor = posicaoFundo.ValorBruto.Value;
                decimal valorCota = posicaoFundo.CotaDia.Value;

                nome = Utilitario.RemoveAcentos(nome);
                nome = Utilitario.RemoveCaracteresEspeciais(nome);

                cnpjFundo = Utilitario.RemoveAcentos(cnpjFundo);
                cnpjFundo = Utilitario.RemoveCaracteresEspeciais(cnpjFundo);
                
                #region Monta Detalhe_Fundo
                d1.cnpjClube = cnpj;
                d1.cnpjFundo = cnpjFundo;
                d1.nomeFundo = nome;
                d1.valorTotalAplicado = valor;
                d1.quantidadeCotas = quantidade;
                d1.valorCota = valorCota;

                int idCategoria = 0;
                TabelaFundoCategoria tabelaFundoCategoria = new TabelaFundoCategoria();
                if (!tabelaFundoCategoria.LoadByPrimaryKey(idCarteira, (int)ListaCategoriaFundoFixo.CBLC))
                {
                    throw new Exception("Não há categoria Bovespa definida na tabela de categorias para o fundo " + idCarteira.ToString());
                }

                bool temFundo = false;
                if (tabelaFundoCategoria.IdCategoriaFundo.Value == (int)ListaCategoriasFundosCBLC.CurtoPrazo)
                {
                    d1.tipoFundoinvestimento = 1;
                    temFundo = true;
                }
                else if (tabelaFundoCategoria.IdCategoriaFundo.Value == (int)ListaCategoriasFundosCBLC.RendaFixa)
                {
                    d1.tipoFundoinvestimento = 2;
                    temFundo = true;
                }
                else if (tabelaFundoCategoria.IdCategoriaFundo.Value == (int)ListaCategoriasFundosCBLC.Referenciado)
                {
                    d1.tipoFundoinvestimento = 3;
                    temFundo = true;
                }
                #endregion
                //
                if (temFundo)
                {
                    fundoInvestimentoDs.Add(d1);

                    this.numeroRegistros++;
                }
            }
            //
            return this.WriteStream(typeof(Detalhe_FundoInvestimento), fundoInvestimentoDs);  
        }
        #endregion

        #region Detalhe_ReclamacoesCotistas
        private MemoryStream Monta_Detalhe_ReclamacoesCotistas(DateTime dataPosicao)
        {
            List<Detalhe_ReclamacoesCotistas> reclamacoesCotistasDs = new List<Detalhe_ReclamacoesCotistas>();

            Detalhe_ReclamacoesCotistas d1 = new Detalhe_ReclamacoesCotistas();
            //
            #region Monta Detalhe_ReclamacoesCotistas
            //d1.tipoRegistro = "1";
            TabelaReclamacoesCotistas tabelaReclamacoesCotistas = new TabelaReclamacoesCotistas();
            if (tabelaReclamacoesCotistas.LoadByPrimaryKey(dataPosicao))
            {
                d1.quantidadeReclamacoes_Procedentes = tabelaReclamacoesCotistas.Procedentes.Value;
                d1.quantidadeReclamacoes_ImProcedentes = tabelaReclamacoesCotistas.Improcedentes.Value;
                d1.quantidadeReclamacoes_Resolvidas = tabelaReclamacoesCotistas.Resolvidas.Value;
                d1.quantidadeReclamacoes_NaoResolvidas = tabelaReclamacoesCotistas.NaoResolvidas.Value;
                d1.quantidadeReclamacoes_Ref_Administracao = tabelaReclamacoesCotistas.Administracao.Value;
                d1.quantidadeReclamacoes_Ref_Gestao = tabelaReclamacoesCotistas.Gestao.Value;
                d1.quantidadeReclamacoes_Ref_Taxas = tabelaReclamacoesCotistas.Taxas.Value;
                d1.quantidadeReclamacoes_Ref_Politica_Investimento = tabelaReclamacoesCotistas.PoliticaInvestimento.Value;
                d1.quantidadeReclamacoes_Ref_Aportes = tabelaReclamacoesCotistas.Aportes.Value;
                d1.quantidadeReclamacoes_Ref_Resgates = tabelaReclamacoesCotistas.Resgates.Value;
                d1.quantidadeReclamacoes_Ref_FornecimentoInformacoes = tabelaReclamacoesCotistas.FornecimentoInformacoes.Value;
                d1.quantidadeReclamacoes_Ref_AssembleiasGerais = tabelaReclamacoesCotistas.AssembleiasGerais.Value;
                d1.quantidadeReclamacoes_Ref_Cotas = tabelaReclamacoesCotistas.Cotas.Value;
                d1.quantidadeReclamacoes_Outros = tabelaReclamacoesCotistas.Outros.Value;
            }
            else
            {                
                d1.quantidadeReclamacoes_Procedentes = 0;
                d1.quantidadeReclamacoes_ImProcedentes = 0;
                d1.quantidadeReclamacoes_Resolvidas = 0;
                d1.quantidadeReclamacoes_NaoResolvidas = 0;
                d1.quantidadeReclamacoes_Ref_Administracao = 0;
                d1.quantidadeReclamacoes_Ref_Gestao = 0;
                d1.quantidadeReclamacoes_Ref_Taxas = 0;
                d1.quantidadeReclamacoes_Ref_Politica_Investimento = 0;
                d1.quantidadeReclamacoes_Ref_Aportes = 0;
                d1.quantidadeReclamacoes_Ref_Resgates = 0;
                d1.quantidadeReclamacoes_Ref_FornecimentoInformacoes = 0;
                d1.quantidadeReclamacoes_Ref_AssembleiasGerais = 0;
                d1.quantidadeReclamacoes_Ref_Cotas = 0;
                d1.quantidadeReclamacoes_Outros = 0;
            }
            #endregion
            //
            reclamacoesCotistasDs.Add(d1);

            this.numeroRegistros++;
            //
            return this.WriteStream(typeof(Detalhe_ReclamacoesCotistas), reclamacoesCotistasDs);  
        }
        #endregion

        #region Detalhe_Trailer
        private MemoryStream Monta_Detalhe_Trailer(DateTime dataPosicao, int codigoCorretora) {
            List<Detalhe_Trailer> trailerDs = new List<Detalhe_Trailer>();

            Detalhe_Trailer d1 = new Detalhe_Trailer();
            this.numeroRegistros++;
            //
            #region Monta Detalhe_Trailer
            //d1.tipoRegistro = "1";
            //d1.codigoArquivo = "2";
            d1.codigoUsuarioBVMF = codigoCorretora;
            d1.codigoOrigem = codigoCorretora;
            //d1.codigoDestino = "5";
            d1.numeroMovimento = 1;
            d1.mesReferencia = dataPosicao;
            //d1.valorFixo = "8";
            d1.totalRegistrosGerados = this.numeroRegistros;

            #endregion
            //
            trailerDs.Add(d1);
            //
            return this.WriteStream(typeof(Detalhe_Trailer), trailerDs);  
        }
        #endregion

        public decimal RetornaValorFundoCategoria(int idClube, DateTime dataHistorico, int idCategoria) {
            PosicaoFundoHistorico posicaoFundoHistorico = new PosicaoFundoHistorico();

            PosicaoFundoHistoricoQuery posicaoFundoQuery = new PosicaoFundoHistoricoQuery("P");
            CarteiraQuery carteiraQuery = new CarteiraQuery("C");
            TabelaFundoCategoriaQuery tabelaFundoCategoriaQuery = new TabelaFundoCategoriaQuery("T");

            posicaoFundoQuery.Select(posicaoFundoQuery.ValorBruto.Sum());
            posicaoFundoQuery.InnerJoin(carteiraQuery).On(carteiraQuery.IdCarteira == posicaoFundoQuery.IdCarteira);
            posicaoFundoQuery.InnerJoin(tabelaFundoCategoriaQuery).On(tabelaFundoCategoriaQuery.IdCarteira == carteiraQuery.IdCarteira);
            posicaoFundoQuery.Where(posicaoFundoQuery.IdCliente.Equal(idClube),
                                    posicaoFundoQuery.DataHistorico.Equal(dataHistorico),
                                    posicaoFundoQuery.Quantidade.GreaterThan(0),
                                    tabelaFundoCategoriaQuery.IdCategoriaFundo.Equal(idCategoria),
                                    tabelaFundoCategoriaQuery.IdLista.Equal((int)ListaCategoriaFundoFixo.CBLC));

            decimal valor = 0;

            if (posicaoFundoHistorico.ValorBruto.HasValue) {
                valor = posicaoFundoHistorico.ValorBruto.Value;
            }

            return valor;
        }
    }

    public class ErrorMessage
    {
        public int Type;
        public string CNPJ;
        public string Description;
        public string Error1;
        public string Error2;
        public string Error3;

        public override string ToString()
        {
            return string.Format("{0}|{1}|{2}|{3}|{4}", Type, CNPJ, Error1, Error2, Error3);
        }

        public static string GetErrorMessage(int error)
        {
            return Resources.ErrorStrings.ResourceManager.GetString(string.Format("E_{0}", error));
        }
    }

    public class FileParser
    {
        private string DetailDescription(int detail)
        {
            string description = "";
            switch (detail)
            {
                case 1:
                    description = "DETALHE DO INFORME";
                    break;
                case 2:
                    description = "DETALHE DOS COTISTAS";
                    break;
                case 3:
                    description = "DETALHE DA CARTEIRA DE ATIVOS";
                    break;
                case 4:
                    description = "DETALHE DE OPÇÕES – SEGMENTO BOVESPA";
                    break;
                case 5:
                    description = "DETALHE DE TERMO – SEGMENTO BOVESPA";
                    break;
                case 6:
                    description = "DETALHE DE EMPRÉSTIMOS DE AÇÕES";
                    break;
                case 7:
                    description = "DETALHE DE FUTUROS";
                    break;
                case 8:
                    description = "DETALHE DE FUNDOS DE INVESTIMENTO";
                    break;
                case 9:
                    description = "INFORMAÇÕES REFERENTES À RECLAMAÇÕES DE COTISTAS";
                    break;
                default:
                    description = "";
                    break;
            }

            return description;
        }

        public List<ErrorMessage> Parse(Stream stream)
        {
            List<ErrorMessage> messages = new List<ErrorMessage>();
            using (StreamReader reader = new StreamReader(stream))
            {
                string line;
                while ((line = reader.ReadLine()) != null)
                {
                    if (line.Length >= 565)
                    {
                        ErrorMessage message = new ErrorMessage();
                        message.Type = int.Parse(line.Substring(0, 2));
                        message.Description = this.DetailDescription(message.Type);
                        message.CNPJ = "CNPJ: " + Utilitario.MascaraCNPJ(line.Substring(2, 16 - 3));

                        int error1 = int.Parse(line.Substring(550, 555 - 550));
                        int error2 = int.Parse(line.Substring(555, 560 - 555));
                        int error3 = int.Parse(line.Substring(560, 565 - 560));
                        message.Error1 = error1 == 0 ? "" : "Erro " + error1.ToString() + " - " + ErrorMessage.GetErrorMessage(error1);
                        message.Error2 = error2 == 0 ? "" : "Erro " + error2.ToString() + " - " + ErrorMessage.GetErrorMessage(error2);
                        message.Error3 = error3 == 0 ? "" : "Erro " + error3.ToString() + " - " + ErrorMessage.GetErrorMessage(error3);
                        messages.Add(message);
                    }
                }
            }

            return messages;
        }
    }
}