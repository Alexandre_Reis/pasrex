﻿using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Financial.InvestidorCotista.Custom;
using System.IO;
using System.Collections.Generic;
using System.Text;

/// <summary>
/// Summary description for InformeRendimentoExport
/// </summary>
public class InformeRendimentoExport
{
    public void ExportaInformeRendimento(int ano, List<int> idClientes, out MemoryStream ms, out string nomeArquivo)
    {
        ms = new MemoryStream();

        nomeArquivo = "InformeRendimento_" + ano.ToString() + ".txt";

        StreamWriter arquivo = new StreamWriter(ms, Encoding.ASCII);
        
        List<InformeRendimento> informeRendimento = new List<InformeRendimento>();

        for (int c = 0; c < idClientes.Count; c++)
        {
            informeRendimento.AddRange(new InformeRendimento().RetornaSaldos(idClientes[c], null, ano));
        }

        if (informeRendimento.Count>0) {

            #region Detalhe
            for (int i = 0; i<informeRendimento.Count; i++){
                arquivo.Write(informeRendimento[i].Ano);
                arquivo.Write("|");
                arquivo.Write(informeRendimento[i].Tipo);
                arquivo.Write("|");
                arquivo.Write(informeRendimento[i].IdFundo);
                arquivo.Write("|");
                arquivo.Write(informeRendimento[i].CodFundo);
                arquivo.Write("|");
                arquivo.Write(informeRendimento[i].NomeFundo);
                arquivo.Write("|");
                arquivo.Write(informeRendimento[i].IdCotista);
                arquivo.Write("|");
                arquivo.Write(informeRendimento[i].CodCotista);
                arquivo.Write("|");
                arquivo.Write(informeRendimento[i].NomeCotista);
                arquivo.Write("|");
                arquivo.Write(informeRendimento[i].DataSaldo);
                arquivo.Write("|");
                arquivo.Write(informeRendimento[i].QuantidadeCotas);
                arquivo.Write("|");
                arquivo.Write(informeRendimento[i].SaldoFinanceiro);
                arquivo.WriteLine();
            }
            #endregion
                    
            arquivo.Flush();

            // Retorna a MemoryStream para o inicio
            ms.Seek(0, SeekOrigin.Begin);
        }
        else {
            throw new Exception("Não existem Dados para Geração do arquivo.");
        }
    }
}
