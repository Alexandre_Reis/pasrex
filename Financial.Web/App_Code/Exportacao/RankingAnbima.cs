﻿using System;
using System.Web;
using Financial.ContaCorrente;
using Financial.Investidor;
using Bytescout.Spreadsheet;
using System.IO;
using Financial.Util;
using Financial.Fundo;
using Financial.InvestidorCotista;
using System.Drawing;
using System.Collections.Generic;


namespace Financial.Export
{
    /// <summary>
    /// Summary description for RankingAnbima
    /// </summary>
    public class RankingAnbima
    {   
        public MemoryStream ExcelMemoryStream;
        public string NomeArquivoExcel;
        public List<int> idsCarteira;
        public DateTime data;

        public void ExportaRankingAnbimaAtivo(DateTime dataRanking, List<int> idsCarteira)
        {
            this.data = dataRanking;
            this.idsCarteira = idsCarteira;

            Spreadsheet planilha = CarregaPlanilha("ranking_anbima_controladoria_ativo.xls");
            Worksheet controladoriaAtivo = planilha.Workbook.Worksheets["Preenchimento"];
            //preenche o cabeçalho
            
            PreencheInformacoesCadastrais(controladoriaAtivo);
            
            //preenche todas as linhas do ranking de acordo com codconta(ex: 1.1.2)

            

            for (int i = 0; i < controladoriaAtivo.UsedRangeRowMax; i++) PreencheLinhaRankingAtivo(controladoriaAtivo.Rows[i]);

            EsconderBytescout(planilha);

            this.ExcelMemoryStream = new MemoryStream();
            planilha.SaveToStreamXLS(this.ExcelMemoryStream);
            this.NomeArquivoExcel = "ranking_anbima_controladoria_ativo.xls";
            
            planilha.Close();

        }

        public void ExportaRankingAnbimaPassivo(DateTime dataRanking, List<int> idsCarteira)
        {
            this.data = dataRanking;
            this.idsCarteira = idsCarteira;

            Spreadsheet planilha = CarregaPlanilha("ranking_anbima_controladoria_passivo.xls");
            Worksheet controladoriaPassivo = planilha.Workbook.Worksheets["Preenchimento"];
            //preenche o cabeçalho

            PreencheInformacoesCadastrais(controladoriaPassivo);

            //preenche todas as linhas do ranking de acordo com codconta(ex: 1.1.2)



            for (int i = 0; i < controladoriaPassivo.UsedRangeRowMax; i++) PreencheLinhaRankingPassivo(controladoriaPassivo.Rows[i]);

            EsconderBytescout(planilha);

            this.ExcelMemoryStream = new MemoryStream();
            planilha.SaveToStreamXLS(this.ExcelMemoryStream);
            this.NomeArquivoExcel = "ranking_anbima_controladoria_passivo.xls";

            planilha.Close();
        }


        public void PreencheLinhaRankingAtivo(Row linha)
        {
            //indice da coluna onde fica o codigo
            const int codContaIndex = 0;

            //indice da coluna do valor
            const int valorIndex = 2;

            if (linha[codContaIndex].Value != null)
            {
                
                

                switch (linha[codContaIndex].Value.ToString().Trim())
                {
                    #region Própria instituição

                    #region Fundo de Investimento

                    //Administrador/Gestor           
                    case "1.1.1.1.":
                        linha[valorIndex].ValueAsDouble = PatrimonioLiquido(Conglomerado.Proprio, CategoriaAnbima.AdministradoresGestores, TipoDeCarteira.Fundo, TipoRankingAnbima.Ativo);
                        break;
                    //EFPC                           
                    case "1.1.1.2.":
                        linha[valorIndex].ValueAsDouble = PatrimonioLiquido(Conglomerado.Proprio, CategoriaAnbima.EFPC, TipoDeCarteira.Fundo, TipoRankingAnbima.Ativo);
                        break;
                    //Seguradoras                    
                    case "1.1.1.3.":
                        linha[valorIndex].ValueAsDouble = PatrimonioLiquido(Conglomerado.Proprio, CategoriaAnbima.Seguradoras, TipoDeCarteira.Fundo, TipoRankingAnbima.Ativo);
                        break;
                    //Empresas                       
                    case "1.1.1.4.":
                        linha[valorIndex].ValueAsDouble = PatrimonioLiquido(Conglomerado.Proprio, CategoriaAnbima.Empresas, TipoDeCarteira.Fundo, TipoRankingAnbima.Ativo);
                        break;

                    #endregion

                    #region Carteira Administrada

                    //Administrador/Gestor           
                    case "1.1.2.1.":
                        linha[valorIndex].ValueAsDouble = PatrimonioLiquido(Conglomerado.Proprio, CategoriaAnbima.AdministradoresGestores, TipoDeCarteira.CarteiraAdministrada, TipoRankingAnbima.Ativo);
                        break;
                    //EFPC                           
                    case "1.1.2.2.":
                        linha[valorIndex].ValueAsDouble = PatrimonioLiquido(Conglomerado.Proprio, CategoriaAnbima.EFPC, TipoDeCarteira.CarteiraAdministrada, TipoRankingAnbima.Ativo);
                        break;
                    //Seguradoras                    
                    case "1.1.2.3.":
                        linha[valorIndex].ValueAsDouble = PatrimonioLiquido(Conglomerado.Proprio, CategoriaAnbima.Seguradoras, TipoDeCarteira.CarteiraAdministrada, TipoRankingAnbima.Ativo);
                        break;
                    //Empresas                       
                    case "1.1.2.4.":
                        linha[valorIndex].ValueAsDouble = PatrimonioLiquido(Conglomerado.Proprio, CategoriaAnbima.Empresas, TipoDeCarteira.CarteiraAdministrada, TipoRankingAnbima.Ativo);
                        break;

                    #endregion

                    #region outros

                    //Outros Investidores Institucionais          
                    case "1.1.3.":
                        linha[valorIndex].ValueAsDouble = PatrimonioLiquido(Conglomerado.Proprio, CategoriaAnbima.OutrosInvestidoresInstitucionais, TipoDeCarteira.Todos, TipoRankingAnbima.Ativo);
                        break;
                    //FIDC                                        
                    case "1.1.5.":
                        linha[valorIndex].ValueAsDouble = PatrimonioLiquido(Conglomerado.Proprio, CategoriaAnbima.FIDC, TipoDeCarteira.Todos, TipoRankingAnbima.Ativo);
                        break;
                    //Clubes de Investimentos                     
                    case "1.1.6.":
                        linha[valorIndex].ValueAsDouble = PatrimonioLiquido(Conglomerado.Proprio, CategoriaAnbima.ClubesDeInvestimento, TipoDeCarteira.Todos, TipoRankingAnbima.Ativo);
                        break;

                    #endregion

                    #endregion

                    #region Outra instituição

                    #region Fundo de Investimento
                    //Administrador/Gestor                        
                    case "1.2.1.1.":
                        linha[valorIndex].ValueAsDouble = PatrimonioLiquido(Conglomerado.Outro, CategoriaAnbima.AdministradoresGestores, TipoDeCarteira.Fundo, TipoRankingAnbima.Ativo);
                        break;
                    //EFPC                                        
                    case "1.2.1.2.":
                        linha[valorIndex].ValueAsDouble = PatrimonioLiquido(Conglomerado.Outro, CategoriaAnbima.EFPC, TipoDeCarteira.Fundo, TipoRankingAnbima.Ativo);
                        break;
                    //Seguradoras                                 
                    case "1.2.1.3.":
                        linha[valorIndex].ValueAsDouble = PatrimonioLiquido(Conglomerado.Outro, CategoriaAnbima.Seguradoras, TipoDeCarteira.Fundo, TipoRankingAnbima.Ativo);
                        break;
                    //Empresas                                    
                    case "1.2.1.4.":
                        linha[valorIndex].ValueAsDouble = PatrimonioLiquido(Conglomerado.Outro, CategoriaAnbima.Empresas, TipoDeCarteira.Fundo, TipoRankingAnbima.Ativo);
                        break;
                    #endregion

                    #region Carteira administrada
                    //Administrador/Gestor                        
                    case "1.2.2.1.":
                        linha[valorIndex].ValueAsDouble = PatrimonioLiquido(Conglomerado.Outro, CategoriaAnbima.AdministradoresGestores, TipoDeCarteira.CarteiraAdministrada, TipoRankingAnbima.Ativo);
                        break;
                    //EFPC                                        
                    case "1.2.2.2.":
                        linha[valorIndex].ValueAsDouble = PatrimonioLiquido(Conglomerado.Outro, CategoriaAnbima.EFPC, TipoDeCarteira.CarteiraAdministrada, TipoRankingAnbima.Ativo);
                        break;
                    //Seguradoras                                 
                    case "1.2.2.3.":
                        linha[valorIndex].ValueAsDouble = PatrimonioLiquido(Conglomerado.Outro, CategoriaAnbima.Seguradoras, TipoDeCarteira.CarteiraAdministrada, TipoRankingAnbima.Ativo);
                        break;
                    //Empresas                                    
                    case "1.2.2.4.":
                        linha[valorIndex].ValueAsDouble = PatrimonioLiquido(Conglomerado.Outro, CategoriaAnbima.Empresas, TipoDeCarteira.CarteiraAdministrada, TipoRankingAnbima.Ativo);
                        break;

                    #endregion

                    #region outros

                    //Outros Investidores Institucionais          
                    case "1.2.3.":
                        linha[valorIndex].ValueAsDouble = PatrimonioLiquido(Conglomerado.Outro, CategoriaAnbima.OutrosInvestidoresInstitucionais, TipoDeCarteira.Todos, TipoRankingAnbima.Ativo);
                        break;
                    //Pessoa Física                               
                    case "1.2.4.":
                        linha[valorIndex].ValueAsDouble = PatrimonioLiquido(Conglomerado.Outro, CategoriaAnbima.PessoasFisicas, TipoDeCarteira.Todos, TipoRankingAnbima.Ativo);
                        break;
                    //FIDC                                        
                    case "1.2.5.":
                        linha[valorIndex].ValueAsDouble = PatrimonioLiquido(Conglomerado.Outro, CategoriaAnbima.FIDC, TipoDeCarteira.Todos, TipoRankingAnbima.Ativo);
                        break;
                    //Clubes de Investimentos                     
                    case "1.2.6.":
                        linha[valorIndex].ValueAsDouble = PatrimonioLiquido(Conglomerado.Outro, CategoriaAnbima.ClubesDeInvestimento, TipoDeCarteira.Todos, TipoRankingAnbima.Ativo);
                        break;

                    #endregion

                    #endregion

                    #region Qtd de Veículos
                    //Origem da Própria Instituição               
                    case "2.1.":
                        linha[valorIndex].ValueAsInteger = QuantidadeDeVeiculos(Conglomerado.Proprio, CategoriaAnbima.Todos, TipoDeCarteira.Todos, TipoRankingAnbima.Ativo);
                        break;
                    //Origem de Outra Instituição                 
                    case "2.2.":
                        linha[valorIndex].ValueAsInteger = QuantidadeDeVeiculos(Conglomerado.Outro, CategoriaAnbima.Todos, TipoDeCarteira.Todos, TipoRankingAnbima.Ativo);
                        break;
                    #endregion

                    #region Qtd de Clientes
                    //Origem da Própria Instituição               
                    case "3.1.":
                        linha[valorIndex].ValueAsInteger = NumeroClientes(Conglomerado.Proprio, CategoriaAnbima.Todos, TipoDeCarteira.Todos, TipoRankingAnbima.Ativo);
                        break;
                    //Origem de Outra Instituição                 
                    case "3.2.":
                        linha[valorIndex].ValueAsInteger = NumeroClientes(Conglomerado.Outro, CategoriaAnbima.Todos, TipoDeCarteira.Todos, TipoRankingAnbima.Ativo);
                        break;
                    #endregion

                    default:
                        break;
                }
            }

        }

        public void PreencheLinhaRankingPassivo(Row linha)
        {
            //indice da coluna onde fica o codigo
            const int codContaIndex = 0;

            //indice da coluna do valor
            const int valorIndex = 2;

            if (linha[codContaIndex].Value != null)
            {
                switch (linha[codContaIndex].Value.ToString().Trim())
                {   
                    #region Qtd de Veículos de Investimentos

                    #region De Origem da Própria Instituição
                    //Fundo de Investimento
                    case "1.1.1.":
                        linha[valorIndex].ValueAsInteger = QuantidadeDeVeiculos(Conglomerado.Proprio, CategoriaAnbima.Fundo, TipoDeCarteira.Todos, TipoRankingAnbima.Passivo);
                        break;
                    //Clubes de Investimento                       
                    case "1.1.2.":
                        linha[valorIndex].ValueAsInteger = QuantidadeDeVeiculos(Conglomerado.Proprio, CategoriaAnbima.ClubesDeInvestimento, TipoDeCarteira.Todos, TipoRankingAnbima.Passivo);
                        break;
                    #endregion

                    #region De Origem de Outra Instituição
                    //Fundo de Investimento
                    case "1.2.1.":
                        linha[valorIndex].ValueAsInteger = QuantidadeDeVeiculos(Conglomerado.Outro, CategoriaAnbima.Fundo, TipoDeCarteira.Todos, TipoRankingAnbima.Passivo);
                        break;
                    //Clubes de Investimento                       
                    case "1.2.2.":
                        linha[valorIndex].ValueAsInteger = QuantidadeDeVeiculos(Conglomerado.Outro, CategoriaAnbima.ClubesDeInvestimento, TipoDeCarteira.Todos, TipoRankingAnbima.Passivo);
                        break;
                    #endregion

                    #endregion

                    #region Qtd de Cotistas
                    //De Origem da Própria Instituição
                    case "2.1.":
                        linha[valorIndex].ValueAsInteger = NumeroCotistas(Conglomerado.Proprio, CategoriaAnbima.Todos, TipoDeCarteira.Todos, TipoRankingAnbima.Passivo);
                        break;
                    //De Origem de Outra Instituição                      
                    case "2.2.":
                        linha[valorIndex].ValueAsInteger = NumeroCotistas(Conglomerado.Outro, CategoriaAnbima.Todos, TipoDeCarteira.Todos, TipoRankingAnbima.Passivo);
                        break;
                    #endregion

                    default:
                        break;
                }
            }

        }


        #region enums
        public enum Conglomerado
        {
            Proprio,
            Outro,
            Todos,
        }

        public enum CategoriaAnbima
        {
            
            AdministradoresGestores = 1,
            EFPC = 2,
            Empresas = 3,
            Seguradoras = 4,
            FIDC = 5,
            ClubesDeInvestimento = 6,
            OutrosInvestidoresInstitucionais = 7,
            PessoasFisicas = 8,
            Todos = 0,
            Fundo = 9
        }

        public enum TipoDeCarteira
        {
            CarteiraAdministrada,
            Fundo,            
            Todos
        }

        public enum TipoRankingAnbima
        {
            Ativo,
            Passivo
        }

        #endregion
        
        #region Calculos
        public CarteiraQuery MontaCarteiraQuery(Conglomerado conglomerado, CategoriaAnbima categoria, TipoDeCarteira tipoDeCarteira, TipoRankingAnbima tipoRanking)
        {
            CarteiraQuery carteiraQuery = new CarteiraQuery("CA");

            carteiraQuery.Where(carteiraQuery.IdCarteira.In(idsCarteira));


            if (conglomerado == Conglomerado.Proprio)
                carteiraQuery.Where(carteiraQuery.MesmoConglomerado == 'S');
            if(conglomerado == Conglomerado.Outro)
                carteiraQuery.Where(carteiraQuery.MesmoConglomerado == 'N');

            if (categoria != CategoriaAnbima.Todos && categoria != CategoriaAnbima.Fundo)
                carteiraQuery.Where(carteiraQuery.CategoriaAnbima == (int)categoria);

            if(tipoDeCarteira == TipoDeCarteira.CarteiraAdministrada)
                carteiraQuery.Where(carteiraQuery.IdCategoria == 1);
            if(tipoDeCarteira == TipoDeCarteira.Fundo)
                carteiraQuery.Where(carteiraQuery.IdCategoria != 1);
            
            if(categoria == CategoriaAnbima.Fundo)
                carteiraQuery.Where(carteiraQuery.CategoriaAnbima != (int)CategoriaAnbima.ClubesDeInvestimento);
            

            return carteiraQuery;
        }
        public Double PatrimonioLiquido(Conglomerado conglomerado, CategoriaAnbima categoria, TipoDeCarteira tipoDeCarteira, TipoRankingAnbima tipoRanking)
        {
            HistoricoCotaQuery historicoCotaQuery = new HistoricoCotaQuery("h");
            CarteiraQuery carteiraQuery = MontaCarteiraQuery(conglomerado, categoria, tipoDeCarteira, tipoRanking);

            carteiraQuery.Select(historicoCotaQuery.PLFechamento.Sum().As("teste"));
            carteiraQuery.InnerJoin(historicoCotaQuery).On(carteiraQuery.IdCarteira == historicoCotaQuery.IdCarteira);

            DateTime dataRanking = Calendario.RetornaUltimoDiaUtilMes(data);

            carteiraQuery.Where(historicoCotaQuery.Data.Date() == dataRanking);

            Carteira carteira = new Carteira();

            if (carteira.Load(carteiraQuery) && carteira.GetColumn("teste").GetType() != typeof(System.DBNull))
                return double.Parse(carteira.GetColumn("teste").ToString())/1000;
            else
                return 0;
        }
        public int QuantidadeDeVeiculos(Conglomerado conglomerado, CategoriaAnbima categoria, TipoDeCarteira tipoDeCarteira, TipoRankingAnbima tipoRanking)
        {
            CarteiraQuery carteiraQuery = MontaCarteiraQuery(conglomerado, categoria, tipoDeCarteira, tipoRanking);
            carteiraQuery.Select(carteiraQuery.IdCarteira.Count().As("carteiras"));
            Carteira carteira = new Carteira();



            if (carteira.Load(carteiraQuery) && carteira.GetColumn("carteiras").GetType() != typeof(System.DBNull))
                return int.Parse(carteira.GetColumn("carteiras").ToString());
            else
                return 0;
        }
        public int NumeroClientes(Conglomerado conglomerado, CategoriaAnbima categoria, TipoDeCarteira tipoDeCarteira, TipoRankingAnbima tipoRanking)
        {
            CarteiraQuery carteiraQuery = MontaCarteiraQuery(conglomerado, categoria, tipoDeCarteira, tipoRanking); 

            carteiraQuery.es.Distinct = true;
            carteiraQuery.Select(carteiraQuery.Contratante);
            CarteiraCollection coll = new CarteiraCollection();



            if (coll.Load(carteiraQuery))
                return coll.Count;
            else return 0;


            //return decimal.Parse(carteira.GetColumn("contratante").ToString());
            //else
            //    return 0;




        }
        //public decimal TotalDeAtivos(Conglomerado conglomerado, CategoriaAnbima categoria, TipoDeCarteira tipoDeCarteira, TipoRankingAnbima tipoRanking)
        //{
        //    Carteira carteira = new Carteira();

        //    #region pl

        //    double pl = PatrimonioLiquido(conglomerado, categoria, tipoDeCarteira, tipoRanking); ;
        //    #endregion

        //    #region liquidacao(CPR)

        //    CarteiraQuery carteiraQuery = MontaCarteiraQuery(conglomerado, categoria, tipoDeCarteira, tipoRanking); 
        //    LiquidacaoHistoricoQuery liquidacaoQuery = new LiquidacaoHistoricoQuery("lh");
        //    carteiraQuery.Select(liquidacaoQuery.Valor.Sum().As("liquidacao"));
        //    carteiraQuery.InnerJoin(liquidacaoQuery).On(carteiraQuery.IdCarteira == liquidacaoQuery.IdCliente);
        //    carteiraQuery.Where(
        //        liquidacaoQuery.DataLancamento.Date() <= data.Date &&
        //        liquidacaoQuery.DataVencimento.Date() > data.Date &&
        //        liquidacaoQuery.DataHistorico.Date() == data.Date
        //    );

        //    decimal liquidacao = 0;
        //    if (carteira.Load(carteiraQuery) && carteira.GetColumn("liquidacao").GetType() != typeof(System.DBNull))
        //        liquidacao = decimal.Parse(carteira.GetColumn("liquidacao").ToString());

        //    #endregion

        //    #region saldo
        //    carteiraQuery = MontaCarteiraQuery(conglomerado, categoria, tipoDeCarteira, tipoRanking); 
        //    SaldoCaixaQuery saldoQuery = new SaldoCaixaQuery("sa");
        //    carteiraQuery.Select(saldoQuery.SaldoFechamento.Sum().As("saldo"));
        //    carteiraQuery.InnerJoin(saldoQuery).On(carteiraQuery.IdCarteira == saldoQuery.IdCliente);
        //    carteiraQuery.Where(
        //        saldoQuery.Data.Date() == data.Date
        //    );

        //    decimal saldo = 0;
        //    if (carteira.Load(carteiraQuery) && carteira.GetColumn("saldo").GetType() != typeof(System.DBNull))
        //        liquidacao = decimal.Parse(carteira.GetColumn("saldo").ToString());

        //    #endregion

        //    return pl - liquidacao - saldo;
        //}
        public int NumeroCotistas(Conglomerado conglomerado, CategoriaAnbima categoria, TipoDeCarteira tipoDeCarteira, TipoRankingAnbima tipoRanking)
        {

            CarteiraQuery carteiraQuery = MontaCarteiraQuery(conglomerado, categoria, tipoDeCarteira, tipoRanking); ;
            PosicaoCotistaHistoricoQuery posicaoQuery = new PosicaoCotistaHistoricoQuery("pq");

            carteiraQuery.InnerJoin(posicaoQuery).On(carteiraQuery.IdCarteira == posicaoQuery.IdCarteira);
            DateTime primeiroDia = Calendario.RetornaPrimeiroDiaUtilMes(data);
            DateTime ultimodia = Calendario.RetornaUltimoDiaUtilMes(data);


            carteiraQuery.Where(posicaoQuery.DataHistorico.Date().Between(primeiroDia, ultimodia));
            carteiraQuery.es.Distinct = true;
            carteiraQuery.Select(posicaoQuery.IdCotista);

            CarteiraCollection coll = new CarteiraCollection();


            if (coll.Load(carteiraQuery))
                return coll.Count;
            else
                return 0;

        }
        #endregion

        public Spreadsheet CarregaPlanilha(string modelo) 
        {
            Spreadsheet spreadsheet = new Bytescout.Spreadsheet.Spreadsheet();

            string pathModelos = System.AppDomain.CurrentDomain.BaseDirectory + "\\Relatorios\\Modelos";
            string pathPlanilhaModelo = String.Format("{0}\\"+modelo, pathModelos);

            spreadsheet.LoadFromFile(pathPlanilhaModelo);            

            return spreadsheet;
        }

        public void PreencheInformacoesCadastrais(Worksheet worksheet)
        {
            //worksheet.Cell(3, 1).ValueAsString = Convert.ToString(ParametrosConfiguracaoSistema.Integracoes.NomeInstituicao);
            worksheet.Cell(4, 1).ValueAsString = Convert.ToString(ParametrosConfiguracaoSistema.Integracoes.ResponsavelInstituicao);
            worksheet.Cell(3, 3).ValueAsString = Convert.ToString(ParametrosConfiguracaoSistema.Integracoes.CodInstituicao);
            worksheet.Cell(4, 3).ValueAsString = Convert.ToString(ParametrosConfiguracaoSistema.Integracoes.TelefoneInstituicao);
            worksheet.Cell(5, 3).ValueAsString = Convert.ToString(ParametrosConfiguracaoSistema.Integracoes.EmailInstituicao);;
            worksheet.Cell(7, 3).ValueAsInteger = data.Month;
            worksheet.Cell(8, 3).ValueAsInteger = data.Year;
        }


        public void EsconderBytescout(Spreadsheet spreadsheet)
        {
            const string BYTESCOUT_WORKSHEET_NAME = "Bytescout Spreadsheet SDK";
            Worksheet worksheet = spreadsheet.Workbook.Worksheets.ByName(BYTESCOUT_WORKSHEET_NAME);
            if (worksheet == null)
            {
                worksheet = spreadsheet.Workbook.Worksheets.Add(BYTESCOUT_WORKSHEET_NAME);
            }
            worksheet.Visible = Bytescout.Spreadsheet.Constants.SHEETVISIBILITY.Hidden;
        }



    }
}
