using System.Xml.Serialization;
using System;
using System.Collections.Generic;


namespace Financial.Export {

    #region  Perfil 1.0

    public class Perfil_1 {
        #region TabelaPerfilCVM_ XML
        [XmlRootAttribute(ElementName = "DOC_ARQ", IsNullable = false, Namespace = "urn:perf")]
        //[XmlRootAttribute(ElementName = "DOC_ARQ")]
        public class TabelaPerfilCVM_Xml {

            // Construtor
            public TabelaPerfilCVM_Xml() { }

            //[XmlElementAttribute("CAB_INFORM", typeof(TabelaPerfilCVM_Cabecalho), Namespace="")]
            //[XmlElementAttribute("PERFIL_MENSAL", typeof(TabelaPerfilCVM_Corpo), Namespace = "")]
            [XmlElementAttribute("CAB_INFORM", typeof(TabelaPerfilCVM_Cabecalho))]
            [XmlElementAttribute("PERFIL_MENSAL", typeof(TabelaPerfilCVM_Corpo))]
            //public object[] itemsField = new object[2] { new TabelaPerfilCVM_Cabecalho(), new TabelaPerfilCVM_Corpo() };
            public object[] itemsField = new object[2] { new TabelaPerfilCVM_Cabecalho(), null };

            [XmlIgnore]
            public TabelaPerfilCVM_Cabecalho getCabecalho {
                get { return (TabelaPerfilCVM_Cabecalho)itemsField[0]; }
                set { itemsField[0] = (TabelaPerfilCVM_Cabecalho)value; }
            }

            [XmlIgnore]
            public TabelaPerfilCVM_Corpo getCorpo {
                get { return (TabelaPerfilCVM_Corpo)itemsField[1]; }
                set { itemsField[1] = (TabelaPerfilCVM_Corpo)value; }
            }

            /// <summary>
            /// Cria o objeto corpo para não aparecer o nó <PERFIL_MENSAL> se não existir dados
            /// </summary>
            public void CriaCorpo() {
                if (this.itemsField[1] == null) {
                    itemsField[1] = new TabelaPerfilCVM_Corpo();
                }
            }
        }

        /* --------------------------------------------------------------------------------*/
        public class TabelaPerfilCVM_Cabecalho {
            // Construtor
            public TabelaPerfilCVM_Cabecalho() { }

            [XmlElement("COD_DOC", Order = 1)]
            public int codigoDocumento;

            #region DT_COMPT
            [XmlIgnore]
            public DateTime dataCompetencia;

            [XmlElement("DT_COMPT", Order = 2)]
            public string formatDataCompetencia {
                get { return this.dataCompetencia.ToString("MM/yyyy"); }
                set { dataCompetencia = DateTime.Parse(value); }
            }
            #endregion

            #region DT_GERAC_ARQ
            [XmlIgnore]
            public DateTime dataGeracaoArquivo;

            [XmlElement("DT_GERAC_ARQ", Order = 3)]
            public string formatDataGeracaoArquivo {
                get { return this.dataGeracaoArquivo.ToString("dd/MM/yyyy"); }
                set { dataGeracaoArquivo = DateTime.Parse(value); }
            }
            #endregion

            [XmlElement("VERSAO", Order = 4)]
            public string versao;
        }

        public class TabelaPerfilCVM_Corpo {
            // Construtor
            public TabelaPerfilCVM_Corpo() {

            }

            #region ROW_PERFIL
            [XmlElementAttribute("ROW_PERFIL", Order = 1)]
            public List<ListPerfil> listPerfil = new List<ListPerfil>();

            [XmlIgnore]
            public List<ListPerfil> getListPerfil {
                get { return listPerfil; }
                set { listPerfil = value; }
            }
            #endregion
        }
        /* --------------------------------------------------------------------------------*/

        public class ListPerfil {
            // Construtor
            public ListPerfil() { }

            [XmlElement("CNPJ_FDO", Order = 1)]
            public string cnpj; // CNPJ DO FUNDO

            #region NR_CLIENT
            [XmlElementAttribute("NR_CLIENT", Order = 2)]
            public NR_CLIENT nr_cliente = new NR_CLIENT();

            [XmlIgnore]
            public NR_CLIENT get_NR_CLIENT {
                get { return nr_cliente; }
                set { nr_cliente = value; }
            }
            #endregion

            #region DISTR_PATRIM
            [XmlElementAttribute("DISTR_PATRIM", Order = 3)]
            public DISTR_PATRIM distr_patrim = new DISTR_PATRIM();

            [XmlIgnore]
            public DISTR_PATRIM get_DISTR_PATRIM {
                get { return distr_patrim; }
                set { distr_patrim = value; }
            }
            #endregion

            [XmlElement("RESM_TEOR_VT_PROFRD", Order = 4)]
            /* CASO O FUNDO POSSUA POLíTICA DE EXERCíCIO DE DIREITO DE VOTO, SUBSTITUA ESTA FRASE PELO RESUMO DO TEOR DOS VOTOS PROFERIDOS 
             * PELO ADMINISTRADOR OU POR SEUS REPRESENTANTES, NAS ASSEMBLEIAS GERAIS E ESPECIAIS DAS COMPANHIAS NAS QUAIS O FUNDO DETENHA PARTICIPAçãO, 
             * QUE TENHAM SIDO REALIZADAS NO PERíODO (Obrigatório, se houver assembleia no período)
            */
            public string resm_teor_vt_profrd;

            [XmlElement("JUST_SUM_VT_PROFRD", Order = 5)]
            /* CASO O FUNDO POSSUA POLíTICA DE EXERCíCIO DE DIREITO DE VOTO, SUBSTITUA ESTA FRASE PELA JUSTIFICATIVA SUMáRIA DO VOTO 
             * PROFERIDO PELO ADMINISTRADOR OU POR SEUS REPRESENTANTES, OU AS RAZõES SUMáRIAS PARA A SUA ABSTENçãO OU NãO COMPARECIMENTO 
             * a ASSEMBLEIA GERAL (Obrigatório, se houver assembleia no periodo)
            */
            public string just_sum_vt_profrd;


            [XmlIgnore]
            /* SUBSTITUA ESTA FRASE PELO VAR (VALOR DE RISCO) COMO % DO PL PARA 21 DIAS úTEIS E 95% DE CONFIANçA (Obrigatório, 
             * opcional para fundo de cotas) */
            public decimal var_perc_pl;

            #region VAR_PERC_PL
            [XmlElement("VAR_PERC_PL", Order = 6)]
            public string format_var_perc_pl {

                get {
                    return this.var_perc_pl.ToString("N4");
                }
                set {
                    decimal var_perc_pl = 0;

                    if (Decimal.TryParse(value, out var_perc_pl))
                        var_perc_pl = var_perc_pl;
                }
            }
            #endregion

            //[XmlElement("VAR_PERC_PL", Order = 6)]
            ///* SUBSTITUA ESTA FRASE PELO VAR (VALOR DE RISCO) COMO % DO PL PARA 21 DIAS TEIS E 95% DE CONFIANA (Obrigatrio, 
            // * opcional para fundo de cotas) */
            //public string var_perc_pl;

            [XmlElement("MOD_VAR_UTILIZ", Order = 7)]
            /* SUBSTITUA ESTA FRASE PELO CDIGO DA CLASSE DO MODELO UTILIZADO NO CALCULO DO VAR NAS QUESTOES ACIMA (Obrigatrio, se houver assembleia no período - 
             * 1 = Parametrico - 2 - No Parametrico - 3- Monte Carlo */
            public string mod_var_utiliz;


            [XmlIgnore]
            /* SUBSTITUA ESTA FRASE PELO PRAZO MDIO EM MESES DA CARTEIRA DE TTULOS DO FUNDO NO LTIMO DIA TIL DO MS (obrigatrio) */
            public decimal praz_med_cart_tit;

            #region PRAZ_MED_CART_TIT
            [XmlElement("PRAZ_MED_CART_TIT", Order = 8)]
            public string format_praz_med_cart_tit {

                get {
                    return this.praz_med_cart_tit.ToString("N4");
                }
                set {
                    decimal praz_med_cart_tit = 0;

                    if (Decimal.TryParse(value, out praz_med_cart_tit))
                        praz_med_cart_tit = praz_med_cart_tit;
                }
            }
            #endregion

            //[XmlElement("PRAZ_MED_CART_TIT", Order = 8)]
            ///* SUBSTITUA ESTA FRASE PELO PRAZO MéDIO EM MESES DA CARTEIRA DE TíTULOS DO FUNDO NO úLTIMO DIA úTIL DO MêS (obrigatório) */
            //public string praz_med_cart_tit;

            [XmlElement("RES_DELIB", Order = 9)]
            /* CASO TENHA SIDO REALIZADA, NO MêS DE REFERêNCIA, ALGUMA ASSEMBLEIA GERAL DE COTISTAS DO FUNDO, SUBSTITUA ESTA FRASE PELO RESUMO 
             * DAS PRINCIPAIS DELIBERAçoES APROVADAS (Obrigatório, se houver assembleia no período) */
            public string res_delib;


            [XmlIgnore]
            /* SUBSTITUA ESTA FRASE PELO VALOR TOTAL DOS CONTRATOS DE COMPRA DE US$ LIQUIDADOS NO MêS (obrigatório). */
            public decimal total_recurs_exter;

            #region TOTAL_RECURS_EXTER
            [XmlElement("TOTAL_RECURS_EXTER", Order = 10)]
            public string format_total_recurs_exter {

                get {
                    return this.total_recurs_exter.ToString("N2");
                }
                set {
                    decimal total_recurs_exter = 0;

                    if (Decimal.TryParse(value, out total_recurs_exter))
                        total_recurs_exter = total_recurs_exter;
                }
            }
            #endregion

            //[XmlElement("TOTAL_RECURS_EXTER", Order = 10)]
            ///* SUBSTITUA ESTA FRASE PELO VALOR TOTAL DOS CONTRATOS DE COMPRA DE US$ LIQUIDADOS NO MêS (obrigatório). */
            //public string total_recurs_exter;

            [XmlIgnore]
            /* SUBSTITUA ESTA FRASE PELO VALOR TOTAL DE CONTRATOS DE VENDA DE US$ LIQUIDADOS NO MêS (obrigatório) */
            public decimal total_recurs_br;

            #region TOTAL_RECURS_BR
            [XmlElement("TOTAL_RECURS_BR", Order = 11)]
            public string format_total_recurs_br {

                get {
                    return this.total_recurs_br.ToString("N2");
                }
                set {
                    decimal total_recurs_br = 0;

                    if (Decimal.TryParse(value, out total_recurs_br))
                        total_recurs_br = total_recurs_br;
                }
            }
            #endregion

            //[XmlElement("TOTAL_RECURS_BR", Order = 11)]
            ///* SUBSTITUA ESTA FRASE PELO VALOR TOTAL DE CONTRATOS DE VENDA DE US$ LIQUIDADOS NO MÊS (obrigatório) */
            //public string total_recurs_br;

            #region VARIACAO_PERC_VAL_COTA
            [XmlElementAttribute("VARIACAO_PERC_VAL_COTA", Order = 12)]
            public VARIACAO_PERC_VAL_COTA variacao_perc_val_cota = new VARIACAO_PERC_VAL_COTA();

            [XmlIgnore]
            public VARIACAO_PERC_VAL_COTA get_VARIACAO_PERC_VAL_COTA {
                get { return variacao_perc_val_cota; }
                set { variacao_perc_val_cota = value; }
            }
            #endregion


            [XmlIgnore]
            /* SUBSTITUA ESSA FRASE PELA VARIACAO DIARIA NO PIOR CASO DE ESTRESSE (Obrigatório, opcional para fundo de cotas */
            public decimal var_diar_perc_cota_fdo_pior_cen_estress;

            #region VAR_DIAR_PERC_COTA_FDO_PIOR_CEN_ESTRESS
            [XmlElement("VAR_DIAR_PERC_COTA_FDO_PIOR_CEN_ESTRESS", Order = 13)]
            public string format_var_diar_perc_cota_fdo_pior_cen_estress {

                get {
                    return this.var_diar_perc_cota_fdo_pior_cen_estress.ToString("N2");
                }
                set {
                    decimal var_diar_perc_cota_fdo_pior_cen_estress = 0;

                    if (Decimal.TryParse(value, out var_diar_perc_cota_fdo_pior_cen_estress))
                        var_diar_perc_cota_fdo_pior_cen_estress = var_diar_perc_cota_fdo_pior_cen_estress;
                }
            }
            #endregion

            //[XmlElement("VAR_DIAR_PERC_COTA_FDO_PIOR_CEN_ESTRESS", Order = 13)]
            ///* SUBSTITUA ESSA FRASE PELA VARIACAO DIARIA NO PIOR CASO DE ESTRESSE (Obrigatório, opcional para fundo de cotas */
            //public string var_diar_perc_cota_fdo_pior_cen_estress;


            [XmlIgnore]
            /* SUBSTITUA ESSA FRASE PELA VARIACAO DIARIA COM TAXA ANUAL DE JUROS (PRé). CONSIDERAR O úLTIMO DIA úTIL DO MêS DE REFERêNCIA 
             * (Obrigatório, opcional para fundo de cotas) */
            public decimal var_diar_perc_patrim_fdo_var_n_taxa_anual;

            #region VAR_DIAR_PERC_PATRIM_FDO_VAR_N_TAXA_ANUAL
            [XmlElement("VAR_DIAR_PERC_PATRIM_FDO_VAR_N_TAXA_ANUAL", Order = 14)]
            public string format_var_diar_perc_patrim_fdo_var_n_taxa_anual {

                get {
                    return this.var_diar_perc_patrim_fdo_var_n_taxa_anual.ToString("N2");
                }
                set {
                    decimal var_diar_perc_patrim_fdo_var_n_taxa_anual = 0;

                    if (Decimal.TryParse(value, out var_diar_perc_patrim_fdo_var_n_taxa_anual))
                        var_diar_perc_patrim_fdo_var_n_taxa_anual = var_diar_perc_patrim_fdo_var_n_taxa_anual;
                }
            }
            #endregion

            //[XmlElement("VAR_DIAR_PERC_PATRIM_FDO_VAR_N_TAXA_ANUAL", Order = 14)]
            ///* SUBSTITUA ESSA FRASE PELA VARIACAO DIARIA COM TAXA ANUAL DE JUROS (PRé). CONSIDERAR O úLTIMO DIA úTIL DO MêS DE REFERêNCIA 
            // * (Obrigatório, opcional para fundo de cotas) */
            //public string var_diar_perc_patrim_fdo_var_n_taxa_anual;


            [XmlIgnore]
            /* SUBSTITUA ESSA FRASE PELA VARIACAO NEGATIVA DE 1% NA TAXA DE CAMBIO (US$/REAL) CONSIDERAR O úLTIMO DIA úTIL DO MêS DE REFERêNCIA 
             * (Obrigatório, opcional para fundo de cotas). */
            public decimal var_diar_perc_patrim_fdo_var_n_taxa_cambio;

            #region VAR_DIAR_PERC_PATRIM_FDO_VAR_N_TAXA_CAMBIO
            [XmlElement("VAR_DIAR_PERC_PATRIM_FDO_VAR_N_TAXA_CAMBIO", Order = 15)]
            public string format_var_diar_perc_patrim_fdo_var_n_taxa_cambio {

                get {
                    return this.var_diar_perc_patrim_fdo_var_n_taxa_cambio.ToString("N2");
                }
                set {
                    decimal var_diar_perc_patrim_fdo_var_n_taxa_cambio = 0;

                    if (Decimal.TryParse(value, out var_diar_perc_patrim_fdo_var_n_taxa_cambio))
                        var_diar_perc_patrim_fdo_var_n_taxa_cambio = var_diar_perc_patrim_fdo_var_n_taxa_cambio;
                }
            }
            #endregion

            //[XmlElement("VAR_DIAR_PERC_PATRIM_FDO_VAR_N_TAXA_CAMBIO", Order = 15)]
            ///* SUBSTITUA ESSA FRASE PELA VARIACAO NEGATIVA DE 1% NA TAXA DE CAMBIO (US$/REAL) CONSIDERAR O úLTIMO DIA úTIL DO MêS DE REFERêNCIA 
            // * (Obrigatório, opcional para fundo de cotas). */
            //public string var_diar_perc_patrim_fdo_var_n_taxa_cambio;


            [XmlIgnore]
            /* SUBSTITUA ESSA FRASE PELA VARIACAO DIARIA NEGATIVA DE 1% NO PREçO DAS AçõES (Obrigatório, opcional para fundo de cotas */
            public decimal var_patrim_fdo_n_preco_acoes;

            #region VAR_PATRIM_FDO_N_PRECO_ACOES
            [XmlElement("VAR_PATRIM_FDO_N_PRECO_ACOES", Order = 16)]
            public string format_var_patrim_fdo_n_preco_acoes {

                get {
                    return this.var_patrim_fdo_n_preco_acoes.ToString("N2");
                }
                set {
                    decimal var_patrim_fdo_n_preco_acoes = 0;

                    if (Decimal.TryParse(value, out var_patrim_fdo_n_preco_acoes))
                        var_patrim_fdo_n_preco_acoes = var_patrim_fdo_n_preco_acoes;
                }
            }
            #endregion

            //[XmlElement("VAR_PATRIM_FDO_N_PRECO_ACOES", Order = 16)]
            ///* SUBSTITUA ESSA FRASE PELA VARIACAO DIARIA NEGATIVA DE 1% NO PREçO DAS AçõES (Obrigatório, opcional para fundo de cotas */ 
            //public string var_patrim_fdo_n_preco_acoes;


            #region VARIACAO_DIAR_PERC_PATRIM_FDO_VAR_N_OUTROS
            [XmlElementAttribute("VARIACAO_DIAR_PERC_PATRIM_FDO_VAR_N_OUTROS", Order = 17)]
            public VARIACAO_DIAR_PERC_PATRIM_FDO_VAR_N_OUTROS variacao_diar_perc_patrim_fdo_var_n_outros = new VARIACAO_DIAR_PERC_PATRIM_FDO_VAR_N_OUTROS();

            [XmlIgnore]
            public VARIACAO_DIAR_PERC_PATRIM_FDO_VAR_N_OUTROS get_VARIACAO_DIAR_PERC_PATRIM_FDO_VAR_N_OUTROS {
                get { return variacao_diar_perc_patrim_fdo_var_n_outros; }
                set { variacao_diar_perc_patrim_fdo_var_n_outros = value; }
            }

            #endregion

            #region VALOR_NOC_TOT_CONTRAT_DERIV_MANT_FDO
            [XmlElementAttribute("VALOR_NOC_TOT_CONTRAT_DERIV_MANT_FDO", Order = 18)]
            //public VALOR_NOC_TOT_CONTRAT_DERIV_MANT_FDO valor_noc_tot_contrat_deriv_mant_fdo = new VALOR_NOC_TOT_CONTRAT_DERIV_MANT_FDO();
            public VALOR_NOC_TOT_CONTRAT_DERIV_MANT_FDO valor_noc_tot_contrat_deriv_mant_fdo = null;

            [XmlIgnore]
            public VALOR_NOC_TOT_CONTRAT_DERIV_MANT_FDO get_VALOR_NOC_TOT_CONTRAT_DERIV_MANT_FDO {
                get {
                    if (this.valor_noc_tot_contrat_deriv_mant_fdo != null) return valor_noc_tot_contrat_deriv_mant_fdo;
                    else return new VALOR_NOC_TOT_CONTRAT_DERIV_MANT_FDO();
                }
                set { valor_noc_tot_contrat_deriv_mant_fdo = value; }
            }
            #endregion

            #region LISTA_OPER_CURS_MERC_BALCAO
            [XmlElementAttribute("LISTA_OPER_CURS_MERC_BALCAO", Order = 19)]
            //public LISTA_OPER_CURS_MERC_BALCAO lista_oper_curs_merc_balcao = new LISTA_OPER_CURS_MERC_BALCAO();
            public LISTA_OPER_CURS_MERC_BALCAO lista_oper_curs_merc_balcao = null;

            [XmlIgnore]
            public LISTA_OPER_CURS_MERC_BALCAO get_LISTA_OPER_CURS_MERC_BALCAO {
                get {
                    if (this.lista_oper_curs_merc_balcao != null) return lista_oper_curs_merc_balcao;
                    else return new LISTA_OPER_CURS_MERC_BALCAO();
                }
                set { lista_oper_curs_merc_balcao = value; }
            }

            #endregion

            [XmlIgnore]
            /* SUBSTITUA ESSA FRASE PELO VALOR TOTAL DOS ATIVOS (EM % DO PL) EM ESTOQUE DE EMISSãO DE PARTES RELACIONADAS (Obrigatório) */
            public decimal tot_ativos_p_relac;

            #region TOT_ATIVOS_P_RELAC
            [XmlElement("TOT_ATIVOS_P_RELAC", Order = 20)]
            public string format_tot_ativos_p_relac {

                get {
                    return this.tot_ativos_p_relac.ToString("N1");
                }
                set {
                    decimal tot_ativos_p_relac = 0;

                    if (Decimal.TryParse(value, out tot_ativos_p_relac))
                        tot_ativos_p_relac = tot_ativos_p_relac;
                }
            }
            #endregion

            //[XmlElement("TOT_ATIVOS_P_RELAC", Order = 20)]
            ///* SUBSTITUA ESSA FRASE PELO VALOR TOTAL DOS ATIVOS (EM % DO PL) EM ESTOQUE DE EMISSãO DE PARTES RELACIONADAS (Obrigatório) */
            //public string tot_ativos_p_relac;

            #region LISTA_EMISSORES_TIT_CRED_PRIV
            [XmlElementAttribute("LISTA_EMISSORES_TIT_CRED_PRIV", Order = 21)]
            //public LISTA_EMISSORES_TIT_CRED_PRIV lista_emissores_tit_cred_priv = new LISTA_EMISSORES_TIT_CRED_PRIV();
            public LISTA_EMISSORES_TIT_CRED_PRIV lista_emissores_tit_cred_priv = null;

            [XmlIgnore]
            public LISTA_EMISSORES_TIT_CRED_PRIV get_LISTA_EMISSORES_TIT_CRED_PRIV {
                get {
                    if (this.lista_emissores_tit_cred_priv != null) return lista_emissores_tit_cred_priv;
                    else return new LISTA_EMISSORES_TIT_CRED_PRIV();
                }
                set { lista_emissores_tit_cred_priv = value; }
            }
            #endregion

            [XmlIgnore]
            /* SUBSTITUA ESSA FRASE PELO VALOR DO TOTAL DE ATIVOS DE CRéDITO PRIVADO (EM % DO PL) EM ESTOQUE.(Obrigatório) */
            public decimal tot_ativos_cred_priv;

            #region TOT_ATIVOS_CRED_PRIV
            [XmlElement("TOT_ATIVOS_CRED_PRIV", Order = 22)]
            public string format_tot_ativos_cred_priv {

                get {
                    return this.tot_ativos_cred_priv.ToString("N1");
                }
                set {
                    decimal tot_ativos_cred_priv = 0;

                    if (Decimal.TryParse(value, out tot_ativos_cred_priv))
                        tot_ativos_cred_priv = tot_ativos_cred_priv;
                }
            }
            #endregion

            //[XmlElement("TOT_ATIVOS_CRED_PRIV", Order = 22)]
            ///* SUBSTITUA ESSA FRASE PELO VALOR DO TOTAL DE ATIVOS DE CRéDITO PRIVADO (EM % DO PL) EM ESTOQUE.(Obrigatório) */
            //public string tot_ativos_cred_priv;


            [XmlElement("VED_REGUL_COBR_TAXA_PERFORM", Order = 23)]
            /* SUBSTITUIR ESSA FRASE PELA INFORMAçãO(S/N) SE é VEDADA NO REGULAMENTO A COBRANçA DE TAXA DE PERFORMANCE QUANDO O VALOR DA COTA DO FUNDO 
             * FOR INFERIOR AO SEU VALOR POR OCASIãO DA úLTIMA COBRANçA EFETUADA, NA FORMA DO DISPOSTO NO  2 DO ART. 62 DA INSTRUçãO CVM N 409, DE 2004 
             * (Obrigatório, se houver taxa de performance) */
            public string ved_regul_cobr_taxa_perform;

            #region RESP_VED_REGUL_COBR_TAXA_PERFORM
            [XmlElementAttribute("RESP_VED_REGUL_COBR_TAXA_PERFORM", Order = 24)]
            //public RESP_VED_REGUL_COBR_TAXA_PERFORM resp_ved_regul_cobr_taxa_perform = new RESP_VED_REGUL_COBR_TAXA_PERFORM();
            public RESP_VED_REGUL_COBR_TAXA_PERFORM resp_ved_regul_cobr_taxa_perform = null;

            [XmlIgnore]
            public RESP_VED_REGUL_COBR_TAXA_PERFORM get_RESP_VED_REGUL_COBR_TAXA_PERFORM {
                get {
                    if (this.resp_ved_regul_cobr_taxa_perform != null) return resp_ved_regul_cobr_taxa_perform;
                    else return new RESP_VED_REGUL_COBR_TAXA_PERFORM();

                }
                set { resp_ved_regul_cobr_taxa_perform = value; }
            }
            #endregion
        }
        /*-----------------------------------------------------------------------------------*/

        public class NR_CLIENT {
            public NR_CLIENT() { }

            [XmlElement("NR_PF_PRIV_BANK", Order = 1)]
            public int NR_PF_PRIV_BANK;

            [XmlElement("NR_PF_VARJ", Order = 2)]
            public int NR_PF_VARJ;

            [XmlElement("NR_PJ_N_FINANC_PRIV_BANK", Order = 3)]
            public int NR_PJ_N_FINANC_PRIV_BANK;

            [XmlElement("NR_PJ_N_FINANC_VARJ", Order = 4)]
            public int NR_PJ_N_FINANC_VARJ;

            [XmlElement("NR_BNC_COMERC", Order = 5)]
            public int NR_BNC_COMERC;

            [XmlElement("NR_PJ_CORR_DIST", Order = 6)]
            public int NR_PJ_CORR_DIST;

            [XmlElement("NR_PJ_OUTR_FINANC", Order = 7)]
            public int NR_PJ_OUTR_FINANC;

            [XmlElement("NR_INV_N_RES", Order = 8)]
            public int NR_INV_N_RES;

            [XmlElement("NR_ENT_AB_PREV_COMPL", Order = 9)]
            public int NR_ENT_AB_PREV_COMPL;

            [XmlElement("NR_ENT_FC_PREV_COMPL", Order = 10)]
            public int NR_ENT_FC_PREV_COMPL;

            [XmlElement("NR_REG_PREV_SERV_PUB", Order = 11)]
            public int NR_REG_PREV_SERV_PUB;

            [XmlElement("NR_SOC_SEG_RESEG", Order = 12)]
            public int NR_SOC_SEG_RESEG;

            [XmlElement("NR_SOC_CAPTLZ_ARRENDM_MERC", Order = 13)]
            public int NR_SOC_CAPTLZ_ARRENDM_MERC;

            [XmlElement("NR_FDOS_CLUB_INV", Order = 14)]
            public int NR_FDOS_CLUB_INV;

            [XmlElement("NR_COTST_DISTR_FDO", Order = 15)]
            public int NR_COTST_DISTR_FDO;

            [XmlElement("NR_OUTROS_N_RELAC", Order = 16)]
            public int NR_OUTROS_N_RELAC;
        }

        public class DISTR_PATRIM {
            public DISTR_PATRIM() { }

            [XmlIgnore]
            public decimal PR_PF_PRIV_BANK;

            #region PR_PF_PRIV_BANK
            [XmlElement("PR_PF_PRIV_BANK", Order = 1)]
            public string format_PR_PF_PRIV_BANK {

                get {
                    return this.PR_PF_PRIV_BANK.ToString("N1");
                }
                set {
                    decimal PR_PF_PRIV_BANK = 0;

                    if (Decimal.TryParse(value, out PR_PF_PRIV_BANK))
                        PR_PF_PRIV_BANK = PR_PF_PRIV_BANK;
                }
            }
            #endregion

            //[XmlElement("PR_PF_PRIV_BANK", Order = 1)]
            //public string PR_PF_PRIV_BANK;

            [XmlIgnore]
            public decimal PR_PF_VARJ;

            #region PR_PF_VARJ
            [XmlElement("PR_PF_VARJ", Order = 2)]
            public string format_PR_PF_VARJ {

                get {
                    return this.PR_PF_VARJ.ToString("N1");
                }
                set {
                    decimal PR_PF_VARJ = 0;

                    if (Decimal.TryParse(value, out PR_PF_VARJ))
                        PR_PF_VARJ = PR_PF_VARJ;
                }
            }
            #endregion

            //[XmlElement("PR_PF_VARJ", Order = 2)]
            //public string PR_PF_VARJ;

            [XmlIgnore]
            public decimal PR_PJ_N_FINANC_PRIV_BANK;

            #region PR_PJ_N_FINANC_PRIV_BANK
            [XmlElement("PR_PJ_N_FINANC_PRIV_BANK", Order = 3)]
            public string format_PR_PJ_N_FINANC_PRIV_BANK {

                get {
                    return this.PR_PJ_N_FINANC_PRIV_BANK.ToString("N1");
                }
                set {
                    decimal PR_PJ_N_FINANC_PRIV_BANK = 0;

                    if (Decimal.TryParse(value, out PR_PJ_N_FINANC_PRIV_BANK))
                        PR_PJ_N_FINANC_PRIV_BANK = PR_PJ_N_FINANC_PRIV_BANK;
                }
            }
            #endregion

            //[XmlElement("PR_PJ_N_FINANC_PRIV_BANK", Order = 3)]
            //public string PR_PJ_N_FINANC_PRIV_BANK;

            [XmlIgnore]
            public decimal PR_PJ_N_FINANC_VARJ;

            #region PR_PJ_N_FINANC_VARJ
            [XmlElement("PR_PJ_N_FINANC_VARJ", Order = 4)]
            public string format_PR_PJ_N_FINANC_VARJ {

                get {
                    return this.PR_PJ_N_FINANC_VARJ.ToString("N1");
                }
                set {
                    decimal PR_PJ_N_FINANC_VARJ = 0;

                    if (Decimal.TryParse(value, out PR_PJ_N_FINANC_VARJ))
                        PR_PJ_N_FINANC_VARJ = PR_PJ_N_FINANC_VARJ;
                }
            }
            #endregion


            //[XmlElement("PR_PJ_N_FINANC_VARJ", Order = 4)]
            //public string PR_PJ_N_FINANC_VARJ;

            [XmlIgnore]
            public decimal PR_BNC_COMERC;

            #region PR_BNC_COMERC
            [XmlElement("PR_BNC_COMERC", Order = 5)]
            public string format_PR_BNC_COMERC {

                get {
                    return this.PR_BNC_COMERC.ToString("N1");
                }
                set {
                    decimal PR_BNC_COMERC = 0;

                    if (Decimal.TryParse(value, out PR_BNC_COMERC))
                        PR_BNC_COMERC = PR_BNC_COMERC;
                }
            }
            #endregion

            //[XmlElement("PR_BNC_COMERC", Order = 5)]
            //public string PR_BNC_COMERC;

            [XmlIgnore]
            public decimal PR_PJ_CORR_DIST;

            #region PR_PJ_CORR_DIST
            [XmlElement("PR_PJ_CORR_DIST", Order = 6)]
            public string format_PR_PJ_CORR_DIST {

                get {
                    return this.PR_PJ_CORR_DIST.ToString("N1");
                }
                set {
                    decimal PR_PJ_CORR_DIST = 0;

                    if (Decimal.TryParse(value, out PR_PJ_CORR_DIST))
                        PR_PJ_CORR_DIST = PR_PJ_CORR_DIST;
                }
            }
            #endregion

            //[XmlElement("PR_PJ_CORR_DIST", Order = 6)]
            //public string PR_PJ_CORR_DIST;

            [XmlIgnore]
            public decimal PR_PJ_OUTR_FINANC;

            #region PR_PJ_OUTR_FINANC
            [XmlElement("PR_PJ_OUTR_FINANC", Order = 7)]
            public string format_PR_PJ_OUTR_FINANC {

                get {
                    return this.PR_PJ_OUTR_FINANC.ToString("N1");
                }
                set {
                    decimal PR_PJ_OUTR_FINANC = 0;

                    if (Decimal.TryParse(value, out PR_PJ_OUTR_FINANC))
                        PR_PJ_OUTR_FINANC = PR_PJ_OUTR_FINANC;
                }
            }
            #endregion

            //[XmlElement("PR_PJ_OUTR_FINANC", Order = 7)]
            //public string PR_PJ_OUTR_FINANC;

            [XmlIgnore]
            public decimal PR_INV_N_RES;

            #region PR_INV_N_RES
            [XmlElement("PR_INV_N_RES", Order = 8)]
            public string format_PR_INV_N_RES {

                get {
                    return this.PR_INV_N_RES.ToString("N1");
                }
                set {
                    decimal PR_INV_N_RES = 0;

                    if (Decimal.TryParse(value, out PR_INV_N_RES))
                        PR_INV_N_RES = PR_INV_N_RES;
                }
            }
            #endregion

            //[XmlElement("PR_INV_N_RES", Order = 8)]
            //public string PR_INV_N_RES;

            [XmlIgnore]
            public decimal PR_ENT_AB_PREV_COMPL;

            #region PR_ENT_AB_PREV_COMPL
            [XmlElement("PR_ENT_AB_PREV_COMPL", Order = 9)]
            public string format_PR_ENT_AB_PREV_COMPL {

                get {
                    return this.PR_ENT_AB_PREV_COMPL.ToString("N1");
                }
                set {
                    decimal PR_ENT_AB_PREV_COMPL = 0;

                    if (Decimal.TryParse(value, out PR_ENT_AB_PREV_COMPL))
                        PR_ENT_AB_PREV_COMPL = PR_ENT_AB_PREV_COMPL;
                }
            }
            #endregion

            //[XmlElement("PR_ENT_AB_PREV_COMPL", Order = 9)]
            //public string PR_ENT_AB_PREV_COMPL;

            [XmlIgnore]
            public decimal PR_ENT_FC_PREV_COMPL;

            #region PR_ENT_FC_PREV_COMPL
            [XmlElement("PR_ENT_FC_PREV_COMPL", Order = 10)]
            public string format_PR_ENT_FC_PREV_COMPL {

                get {
                    return this.PR_ENT_FC_PREV_COMPL.ToString("N1");
                }
                set {
                    decimal PR_ENT_FC_PREV_COMPL = 0;

                    if (Decimal.TryParse(value, out PR_ENT_FC_PREV_COMPL))
                        PR_ENT_FC_PREV_COMPL = PR_ENT_FC_PREV_COMPL;
                }
            }
            #endregion

            //[XmlElement("PR_ENT_FC_PREV_COMPL", Order = 10)]
            //public string PR_ENT_FC_PREV_COMPL;

            [XmlIgnore]
            public decimal PR_REG_PREV_SERV_PUB;

            #region PR_REG_PREV_SERV_PUB
            [XmlElement("PR_REG_PREV_SERV_PUB", Order = 11)]
            public string format_PR_REG_PREV_SERV_PUB {

                get {
                    return this.PR_REG_PREV_SERV_PUB.ToString("N1");
                }
                set {
                    decimal PR_REG_PREV_SERV_PUB = 0;

                    if (Decimal.TryParse(value, out PR_REG_PREV_SERV_PUB))
                        PR_REG_PREV_SERV_PUB = PR_REG_PREV_SERV_PUB;
                }
            }
            #endregion

            //[XmlElement("PR_REG_PREV_SERV_PUB", Order = 11)]
            //public string PR_REG_PREV_SERV_PUB;

            [XmlIgnore]
            public decimal PR_SOC_SEG_RESEG;

            #region PR_SOC_SEG_RESEG
            [XmlElement("PR_SOC_SEG_RESEG", Order = 12)]
            public string format_PR_SOC_SEG_RESEG {

                get {
                    return this.PR_SOC_SEG_RESEG.ToString("N1");
                }
                set {
                    decimal PR_SOC_SEG_RESEG = 0;

                    if (Decimal.TryParse(value, out PR_SOC_SEG_RESEG))
                        PR_SOC_SEG_RESEG = PR_SOC_SEG_RESEG;
                }
            }
            #endregion

            //[XmlElement("PR_SOC_SEG_RESEG", Order = 12)]
            //public string PR_SOC_SEG_RESEG;

            [XmlIgnore]
            public decimal PR_SOC_CAPTLZ_ARRENDM_MERC;

            #region PR_SOC_CAPTLZ_ARRENDM_MERC
            [XmlElement("PR_SOC_CAPTLZ_ARRENDM_MERC", Order = 13)]
            public string format_PR_SOC_CAPTLZ_ARRENDM_MERC {

                get {
                    return this.PR_SOC_CAPTLZ_ARRENDM_MERC.ToString("N1");
                }
                set {
                    decimal PR_SOC_CAPTLZ_ARRENDM_MERC = 0;

                    if (Decimal.TryParse(value, out PR_SOC_CAPTLZ_ARRENDM_MERC))
                        PR_SOC_CAPTLZ_ARRENDM_MERC = PR_SOC_CAPTLZ_ARRENDM_MERC;
                }
            }
            #endregion


            ////[XmlElement("PR_SOC_CAPTLZ_ARRENDM_MERC", Order = 13)]
            ////public string PR_SOC_CAPTLZ_ARRENDM_MERC;

            [XmlIgnore]
            public decimal PR_FDOS_CLUB_INV;


            #region PR_FDOS_CLUB_INV
            [XmlElement("PR_FDOS_CLUB_INV", Order = 14)]
            public string format_PR_FDOS_CLUB_INV {

                get {
                    return this.PR_FDOS_CLUB_INV.ToString("N1");
                }
                set {
                    decimal PR_FDOS_CLUB_INV = 0;

                    if (Decimal.TryParse(value, out PR_FDOS_CLUB_INV))
                        PR_FDOS_CLUB_INV = PR_FDOS_CLUB_INV;
                }
            }
            #endregion

            //[XmlElement("PR_FDOS_CLUB_INV", Order = 14)]
            //public string PR_FDOS_CLUB_INV;

            [XmlIgnore]
            public decimal PR_COTST_DISTR_FDO;


            #region PR_COTST_DISTR_FDO
            [XmlElement("PR_COTST_DISTR_FDO", Order = 15)]
            public string format_PR_COTST_DISTR_FDO {

                get {
                    return this.PR_COTST_DISTR_FDO.ToString("N1");
                }
                set {
                    decimal PR_COTST_DISTR_FDO = 0;

                    if (Decimal.TryParse(value, out PR_COTST_DISTR_FDO))
                        PR_COTST_DISTR_FDO = PR_COTST_DISTR_FDO;
                }
            }
            #endregion


            //[XmlElement("PR_COTST_DISTR_FDO", Order = 15)]
            //public string PR_COTST_DISTR_FDO;


            [XmlIgnore]
            public decimal PR_OUTROS_N_RELAC;

            #region PR_OUTROS_N_RELAC
            [XmlElement("PR_OUTROS_N_RELAC", Order = 16)]
            public string format_PR_OUTROS_N_RELAC {

                get {
                    return this.PR_OUTROS_N_RELAC.ToString("N1");
                }
                set {
                    decimal PR_OUTROS_N_RELAC = 0;

                    if (Decimal.TryParse(value, out PR_OUTROS_N_RELAC))
                        PR_OUTROS_N_RELAC = PR_OUTROS_N_RELAC;
                }
            }
            #endregion

            //[XmlElement("PR_OUTROS_N_RELAC", Order = 16)]
            //public string PR_OUTROS_N_RELAC;                                             
        }

        public class VARIACAO_PERC_VAL_COTA {

            public VARIACAO_PERC_VAL_COTA() { }

            [XmlIgnore]
            /* SUBSTITUA ESTA FRASE PELO VALOR TOTAL DOS CONTRATOS DE COMPRA DE US$ LIQUIDADOS NO MeS (obrigatório). */
            public decimal VAL_PERCENT;

            #region VAL_PERCENT
            [XmlElement("VAL_PERCENT", Order = 1)]
            public string format_VAL_PERCENT {

                get {
                    return this.VAL_PERCENT.ToString("N2");
                }
                set {
                    decimal VAL_PERCENT = 0;

                    if (Decimal.TryParse(value, out VAL_PERCENT))
                        VAL_PERCENT = VAL_PERCENT;
                }
            }
            #endregion

            //[XmlElement("VAL_PERCENT", Order = 1)]
            //public string VAL_PERCENT;

            [XmlElementAttribute("LISTA_FATOR_PRIMIT_RISCO", Order = 2)]
            public LISTA_FATOR_PRIMIT_RISCO lista_fator_primit_risco = new LISTA_FATOR_PRIMIT_RISCO();
        }

        public class LISTA_FATOR_PRIMIT_RISCO {
            public LISTA_FATOR_PRIMIT_RISCO() { }

            #region FATOR_PRIMIT_RISCO
            [XmlElementAttribute("FATOR_PRIMIT_RISCO", Order = 1)]
            public List<FATOR_PRIMIT_RISCO> fator_primit_risco = new List<FATOR_PRIMIT_RISCO>();

            [XmlIgnore]
            public List<FATOR_PRIMIT_RISCO> get_FATOR_PRIMIT_RISCO {
                get { return fator_primit_risco; }
                set { fator_primit_risco = value; }
            }
            #endregion
        }

        public class FATOR_PRIMIT_RISCO {
            public FATOR_PRIMIT_RISCO() { }

            [XmlElement("NOME_FATOR_PRIMIT_RISCO", Order = 1)]
            public string NOME_FATOR_PRIMIT_RISCO;

            [XmlElement("CEN_UTIL", Order = 2)]
            public string CEN_UTIL;
        }

        public class VARIACAO_DIAR_PERC_PATRIM_FDO_VAR_N_OUTROS {
            public VARIACAO_DIAR_PERC_PATRIM_FDO_VAR_N_OUTROS() { }

            [XmlElement("FATOR_RISCO_OUTROS", Order = 1)]
            public string FATOR_RISCO_OUTROS;

            [XmlIgnore]
            public decimal VAL_PERCENT_OUTROS;

            #region VAL_PERCENT_OUTROS
            [XmlElement("VAL_PERCENT_OUTROS", Order = 2)]
            public string format_VAL_PERCENT_OUTROS {

                get {
                    return this.VAL_PERCENT_OUTROS.ToString("N2");
                }
                set {
                    decimal VAL_PERCENT_OUTROS = 0;

                    if (Decimal.TryParse(value, out VAL_PERCENT_OUTROS))
                        VAL_PERCENT_OUTROS = VAL_PERCENT_OUTROS;
                }
            }
            #endregion

            //[XmlElement("VAL_PERCENT_OUTROS", Order = 2)]
            //public string VAL_PERCENT_OUTROS;
        }

        public class VALOR_NOC_TOT_CONTRAT_DERIV_MANT_FDO {
            public VALOR_NOC_TOT_CONTRAT_DERIV_MANT_FDO() { }

            [XmlIgnore]
            public decimal? VAL_COLATERAL;

            #region VAL_COLATERAL
            [XmlElement("VAL_COLATERAL", Order = 1)]
            public string format_VAL_COLATERAL {

                get {
                    return this.VAL_COLATERAL.HasValue ? VAL_COLATERAL.Value.ToString("N2") : String.Empty;
                }
                set {

                    if (!this.VAL_COLATERAL.HasValue) {
                        format_VAL_COLATERAL = String.Empty;
                    }
                    else {
                        decimal VAL_COLATERAL;

                        if (Decimal.TryParse(value, out VAL_COLATERAL))
                            VAL_COLATERAL = VAL_COLATERAL;
                    }
                }
            }
            #endregion

            //[XmlElement("VAL_COLATERAL", Order = 1)]
            //public string VAL_COLATERAL;

            [XmlElementAttribute("LISTA_FATOR_RISCO_NOC", Order = 2)]
            //public LISTA_FATOR_RISCO_NOC lista_fator_risco_noc = new LISTA_FATOR_RISCO_NOC();
            public LISTA_FATOR_RISCO_NOC lista_fator_risco_noc = null;
        }

        public class LISTA_FATOR_RISCO_NOC {
            public LISTA_FATOR_RISCO_NOC() { }

            #region FATOR_RISCO_NOC
            [XmlElementAttribute("FATOR_RISCO_NOC", Order = 1)]
            public List<FATOR_RISCO_NOC> fator_risco_noc = new List<FATOR_RISCO_NOC>();

            [XmlIgnore]
            public List<FATOR_RISCO_NOC> get_FATOR_RISCO_NOC {
                get { return fator_risco_noc; }
                set { fator_risco_noc = value; }
            }
            #endregion
        }

        public class FATOR_RISCO_NOC {
            public FATOR_RISCO_NOC() { }

            [XmlElement("NOME_FATOR_NOC", Order = 1)]
            public string NOME_FATOR_NOC;

            [XmlElement("VAL_FATOR_RISCO_NOC_LONG", Order = 2)]
            public ulong VAL_FATOR_RISCO_NOC_LONG;

            [XmlElement("VAL_FATOR_RISCO_NOC_SHORT", Order = 3)]
            public ushort VAL_FATOR_RISCO_NOC_SHORT;
        }

        public class LISTA_OPER_CURS_MERC_BALCAO {
            public LISTA_OPER_CURS_MERC_BALCAO() { }

            #region OPER_CURS_MERC_BALCAO
            [XmlElementAttribute("OPER_CURS_MERC_BALCAO", Order = 1)]
            public List<OPER_CURS_MERC_BALCAO> oper_curs_merc_balcao = new List<OPER_CURS_MERC_BALCAO>();

            [XmlIgnore]
            public List<OPER_CURS_MERC_BALCAO> get_OPER_CURS_MERC_BALCAO {
                get { return oper_curs_merc_balcao; }
                set { oper_curs_merc_balcao = value; }
            }
            #endregion
        }

        public class OPER_CURS_MERC_BALCAO {
            public OPER_CURS_MERC_BALCAO() { }

            [XmlElement("TP_PESSOA", Order = 1)]
            public string TP_PESSOA;

            [XmlElement("NR_PF_PJ_COMITENTE", Order = 2)]
            public string NR_PF_PJ_COMITENTE;

            [XmlElement("PARTE_RELACIONADA", Order = 3)]
            public string PARTE_RELACIONADA;

            [XmlIgnore]
            public decimal VALOR_PARTE;

            #region VALOR_PARTE
            [XmlElement("VALOR_PARTE", Order = 4)]
            public string format_VALOR_PARTE {

                get {
                    return this.VALOR_PARTE.ToString("N1");
                }
                set {
                    decimal VALOR_PARTE = 0;

                    if (Decimal.TryParse(value, out VALOR_PARTE))
                        VALOR_PARTE = VALOR_PARTE;
                }
            }
            #endregion

            //[XmlElement("VALOR_PARTE", Order = 4)]
            //public string VALOR_PARTE;
        }

        public class LISTA_EMISSORES_TIT_CRED_PRIV {
            public LISTA_EMISSORES_TIT_CRED_PRIV() { }

            #region EMISSORES_TIT_CRED_PRIV
            [XmlElementAttribute("EMISSORES_TIT_CRED_PRIV", Order = 1)]
            public List<EMISSORES_TIT_CRED_PRIV> emissores_tit_cred_priv = new List<EMISSORES_TIT_CRED_PRIV>();

            [XmlIgnore]
            public List<EMISSORES_TIT_CRED_PRIV> get_EMISSORES_TIT_CRED_PRIV {
                get { return emissores_tit_cred_priv; }
                set { emissores_tit_cred_priv = value; }
            }
            #endregion
        }

        public class EMISSORES_TIT_CRED_PRIV {
            public EMISSORES_TIT_CRED_PRIV() { }

            [XmlElement("TP_PESSOA_EMISSOR", Order = 1)]
            public string TP_PESSOA_EMISSOR;

            [XmlElement("NR_PF_PJ_EMISSOR", Order = 2)]
            public string NR_PF_PJ_EMISSOR;

            [XmlElement("PARTE_RELACIONADA", Order = 3)]
            public string PARTE_RELACIONADA;

            [XmlIgnore]
            public decimal VALOR_PARTE;

            #region VALOR_PARTE
            [XmlElement("VALOR_PARTE", Order = 4)]
            public string format_VALOR_PARTE {

                get {
                    return this.VALOR_PARTE.ToString("N1");
                }
                set {
                    decimal VALOR_PARTE = 0;

                    if (Decimal.TryParse(value, out VALOR_PARTE))
                        VALOR_PARTE = VALOR_PARTE;
                }
            }
            #endregion

            //[XmlElement("VALOR_PARTE", Order = 4)]
            //public string VALOR_PARTE;
        }

        public class RESP_VED_REGUL_COBR_TAXA_PERFORM {
            public RESP_VED_REGUL_COBR_TAXA_PERFORM() { }

            #region DATA_COTA_FUNDO
            [XmlIgnore]
            public DateTime DATA_COTA_FUNDO;

            [XmlElement("DATA_COTA_FUNDO", Order = 1)]
            public string formatDATA_COTA_FUNDO {
                get { return this.DATA_COTA_FUNDO.ToString("dd/MM/yyyy"); }
                set { DATA_COTA_FUNDO = DateTime.Parse(value); }
            }
            #endregion

            //[XmlElement("DATA_COTA_FUNDO", Order = 1)]
            //public string DATA_COTA_FUNDO;

            [XmlIgnore]
            public decimal VAL_COTA_FUNDO;

            #region VAL_COTA_FUNDO
            [XmlElement("VAL_COTA_FUNDO", Order = 2)]
            public string format_VAL_COTA_FUNDO {

                get {
                    return this.VAL_COTA_FUNDO.ToString("N5");
                }
                set {
                    decimal VAL_COTA_FUNDO = 0;

                    if (Decimal.TryParse(value, out VAL_COTA_FUNDO))
                        VAL_COTA_FUNDO = VAL_COTA_FUNDO;
                }
            }
            #endregion

            //[XmlElement("VAL_COTA_FUNDO", Order = 2)]
            //public string VAL_COTA_FUNDO;

        }

        #endregion
        
    }

    #endregion

    #region  Perfil 3.0

    public class Perfil_3 {
        #region TabelaPerfilCVM_ XML
        [XmlRootAttribute(ElementName = "DOC_ARQ", IsNullable = false, Namespace = "urn:perf")]
        //[XmlRootAttribute(ElementName = "DOC_ARQ")]
        public class TabelaPerfilCVM_Xml {

            // Construtor
            public TabelaPerfilCVM_Xml() { }

            //[XmlElementAttribute("CAB_INFORM", typeof(TabelaPerfilCVM_Cabecalho), Namespace="")]
            //[XmlElementAttribute("PERFIL_MENSAL", typeof(TabelaPerfilCVM_Corpo), Namespace = "")]
            [XmlElementAttribute("CAB_INFORM", typeof(TabelaPerfilCVM_Cabecalho))]
            [XmlElementAttribute("PERFIL_MENSAL", typeof(TabelaPerfilCVM_Corpo))]
            //public object[] itemsField = new object[2] { new TabelaPerfilCVM_Cabecalho(), new TabelaPerfilCVM_Corpo() };
            public object[] itemsField = new object[2] { new TabelaPerfilCVM_Cabecalho(), null };

            [XmlIgnore]
            public TabelaPerfilCVM_Cabecalho getCabecalho {
                get { return (TabelaPerfilCVM_Cabecalho)itemsField[0]; }
                set { itemsField[0] = (TabelaPerfilCVM_Cabecalho)value; }
            }

            [XmlIgnore]
            public TabelaPerfilCVM_Corpo getCorpo {
                get { return (TabelaPerfilCVM_Corpo)itemsField[1]; }
                set { itemsField[1] = (TabelaPerfilCVM_Corpo)value; }
            }

            /// <summary>
            /// Cria o objeto corpo para não aparecer o nó <PERFIL_MENSAL> se não existir dados
            /// </summary>
            public void CriaCorpo() {
                if (this.itemsField[1] == null) {
                    itemsField[1] = new TabelaPerfilCVM_Corpo();
                }
            }
        }

        /* --------------------------------------------------------------------------------*/
        public class TabelaPerfilCVM_Cabecalho {
            // Construtor
            public TabelaPerfilCVM_Cabecalho() { }

            [XmlElement("COD_DOC", Order = 1)]
            public int codigoDocumento;

            #region DT_COMPT
            [XmlIgnore]
            public DateTime dataCompetencia;

            [XmlElement("DT_COMPT", Order = 2)]
            public string formatDataCompetencia {
                get { return this.dataCompetencia.ToString("MM/yyyy"); }
                set { dataCompetencia = DateTime.Parse(value); }
            }
            #endregion

            #region DT_GERAC_ARQ
            [XmlIgnore]
            public DateTime dataGeracaoArquivo;

            [XmlElement("DT_GERAC_ARQ", Order = 3)]
            public string formatDataGeracaoArquivo {
                get { return this.dataGeracaoArquivo.ToString("dd/MM/yyyy"); }
                set { dataGeracaoArquivo = DateTime.Parse(value); }
            }
            #endregion

            [XmlElement("VERSAO", Order = 4)]
            public string versao;
        }

        public class TabelaPerfilCVM_Corpo {
            // Construtor
            public TabelaPerfilCVM_Corpo() {

            }

            #region ROW_PERFIL
            [XmlElementAttribute("ROW_PERFIL", Order = 1)]
            public List<ListPerfil> listPerfil = new List<ListPerfil>();

            [XmlIgnore]
            public List<ListPerfil> getListPerfil {
                get { return listPerfil; }
                set { listPerfil = value; }
            }
            #endregion
        }
        /* --------------------------------------------------------------------------------*/

        public class ListPerfil {
            // Construtor
            public ListPerfil() { }

            [XmlElement("CNPJ_FDO", Order = 1)]
            public string cnpj; // CNPJ DO FUNDO

            #region NR_CLIENT
            [XmlElementAttribute("NR_CLIENT", Order = 2)]
            public NR_CLIENT nr_cliente = new NR_CLIENT();

            [XmlIgnore]
            public NR_CLIENT get_NR_CLIENT {
                get { return nr_cliente; }
                set { nr_cliente = value; }
            }
            #endregion

            #region DISTR_PATRIM
            [XmlElementAttribute("DISTR_PATRIM", Order = 3)]
            public DISTR_PATRIM distr_patrim = new DISTR_PATRIM();

            [XmlIgnore]
            public DISTR_PATRIM get_DISTR_PATRIM {
                get { return distr_patrim; }
                set { distr_patrim = value; }
            }
            #endregion

            [XmlElement("RESM_TEOR_VT_PROFRD", Order = 4)]
            /* CASO O FUNDO POSSUA POLíTICA DE EXERCíCIO DE DIREITO DE VOTO, SUBSTITUA ESTA FRASE PELO RESUMO DO TEOR DOS VOTOS PROFERIDOS 
             * PELO ADMINISTRADOR OU POR SEUS REPRESENTANTES, NAS ASSEMBLEIAS GERAIS E ESPECIAIS DAS COMPANHIAS NAS QUAIS O FUNDO DETENHA PARTICIPAçãO, 
             * QUE TENHAM SIDO REALIZADAS NO PERíODO (Obrigatório, se houver assembleia no período)
            */
            public string resm_teor_vt_profrd;

            [XmlElement("JUST_SUM_VT_PROFRD", Order = 5)]
            /* CASO O FUNDO POSSUA POLíTICA DE EXERCíCIO DE DIREITO DE VOTO, SUBSTITUA ESTA FRASE PELA JUSTIFICATIVA SUMáRIA DO VOTO 
             * PROFERIDO PELO ADMINISTRADOR OU POR SEUS REPRESENTANTES, OU AS RAZõES SUMáRIAS PARA A SUA ABSTENçãO OU NãO COMPARECIMENTO 
             * a ASSEMBLEIA GERAL (Obrigatório, se houver assembleia no periodo)
            */
            public string just_sum_vt_profrd;


            [XmlIgnore]
            /* SUBSTITUA ESTA FRASE PELO VAR (VALOR DE RISCO) COMO % DO PL PARA 21 DIAS úTEIS E 95% DE CONFIANçA (Obrigatório, 
             * opcional para fundo de cotas) */
            public decimal var_perc_pl;

            #region VAR_PERC_PL
            [XmlElement("VAR_PERC_PL", Order = 6)]
            public string format_var_perc_pl {

                get {
                    return this.var_perc_pl.ToString("N4");
                }
                set {
                    decimal var_perc_pl = 0;

                    if (Decimal.TryParse(value, out var_perc_pl))
                        var_perc_pl = var_perc_pl;
                }
            }
            #endregion

            //[XmlElement("VAR_PERC_PL", Order = 6)]
            ///* SUBSTITUA ESTA FRASE PELO VAR (VALOR DE RISCO) COMO % DO PL PARA 21 DIAS TEIS E 95% DE CONFIANA (Obrigatrio, 
            // * opcional para fundo de cotas) */
            //public string var_perc_pl;

            [XmlElement("MOD_VAR_UTILIZ", Order = 7)]
            /* SUBSTITUA ESTA FRASE PELO CDIGO DA CLASSE DO MODELO UTILIZADO NO CALCULO DO VAR NAS QUESTOES ACIMA (Obrigatrio, se houver assembleia no período - 
             * 1 = Parametrico - 2 - No Parametrico - 3- Monte Carlo */
            public string mod_var_utiliz;


            [XmlIgnore]
            /* SUBSTITUA ESTA FRASE PELO PRAZO MDIO EM MESES DA CARTEIRA DE TTULOS DO FUNDO NO LTIMO DIA TIL DO MS (obrigatrio) */
            public decimal praz_med_cart_tit;

            #region PRAZ_MED_CART_TIT
            [XmlElement("PRAZ_MED_CART_TIT", Order = 8)]
            public string format_praz_med_cart_tit {

                get {
                    return this.praz_med_cart_tit.ToString("N4");
                }
                set {
                    decimal praz_med_cart_tit = 0;

                    if (Decimal.TryParse(value, out praz_med_cart_tit))
                        praz_med_cart_tit = praz_med_cart_tit;
                }
            }
            #endregion

            //[XmlElement("PRAZ_MED_CART_TIT", Order = 8)]
            ///* SUBSTITUA ESTA FRASE PELO PRAZO MéDIO EM MESES DA CARTEIRA DE TíTULOS DO FUNDO NO úLTIMO DIA úTIL DO MêS (obrigatório) */
            //public string praz_med_cart_tit;

            [XmlElement("RES_DELIB", Order = 9)]
            /* CASO TENHA SIDO REALIZADA, NO MêS DE REFERêNCIA, ALGUMA ASSEMBLEIA GERAL DE COTISTAS DO FUNDO, SUBSTITUA ESTA FRASE PELO RESUMO 
             * DAS PRINCIPAIS DELIBERAçoES APROVADAS (Obrigatório, se houver assembleia no período) */
            public string res_delib;


            [XmlIgnore]
            /* SUBSTITUA ESTA FRASE PELO VALOR TOTAL DOS CONTRATOS DE COMPRA DE US$ LIQUIDADOS NO MêS (obrigatório). */
            public decimal total_recurs_exter;

            #region TOTAL_RECURS_EXTER
            [XmlElement("TOTAL_RECURS_EXTER", Order = 10)]
            public string format_total_recurs_exter {

                get {
                    return this.total_recurs_exter.ToString("N2");
                }
                set {
                    decimal total_recurs_exter = 0;

                    if (Decimal.TryParse(value, out total_recurs_exter))
                        total_recurs_exter = total_recurs_exter;
                }
            }
            #endregion

            //[XmlElement("TOTAL_RECURS_EXTER", Order = 10)]
            ///* SUBSTITUA ESTA FRASE PELO VALOR TOTAL DOS CONTRATOS DE COMPRA DE US$ LIQUIDADOS NO MêS (obrigatório). */
            //public string total_recurs_exter;

            [XmlIgnore]
            /* SUBSTITUA ESTA FRASE PELO VALOR TOTAL DE CONTRATOS DE VENDA DE US$ LIQUIDADOS NO MêS (obrigatório) */
            public decimal total_recurs_br;

            #region TOTAL_RECURS_BR
            [XmlElement("TOTAL_RECURS_BR", Order = 11)]
            public string format_total_recurs_br {

                get {
                    return this.total_recurs_br.ToString("N2");
                }
                set {
                    decimal total_recurs_br = 0;

                    if (Decimal.TryParse(value, out total_recurs_br))
                        total_recurs_br = total_recurs_br;
                }
            }
            #endregion

            //[XmlElement("TOTAL_RECURS_BR", Order = 11)]
            ///* SUBSTITUA ESTA FRASE PELO VALOR TOTAL DE CONTRATOS DE VENDA DE US$ LIQUIDADOS NO MÊS (obrigatório) */
            //public string total_recurs_br;

            #region VARIACAO_PERC_VAL_COTA
            [XmlElementAttribute("VARIACAO_PERC_VAL_COTA", Order = 12)]
            public VARIACAO_PERC_VAL_COTA variacao_perc_val_cota = new VARIACAO_PERC_VAL_COTA();

            [XmlIgnore]
            public VARIACAO_PERC_VAL_COTA get_VARIACAO_PERC_VAL_COTA {
                get { return variacao_perc_val_cota; }
                set { variacao_perc_val_cota = value; }
            }
            #endregion


            [XmlIgnore]
            /* SUBSTITUA ESSA FRASE PELA VARIACAO DIARIA NO PIOR CASO DE ESTRESSE (Obrigatório, opcional para fundo de cotas */
            public decimal var_diar_perc_cota_fdo_pior_cen_estress;

            #region VAR_DIAR_PERC_COTA_FDO_PIOR_CEN_ESTRESS
            [XmlElement("VAR_DIAR_PERC_COTA_FDO_PIOR_CEN_ESTRESS", Order = 13)]
            public string format_var_diar_perc_cota_fdo_pior_cen_estress {

                get {
                    return this.var_diar_perc_cota_fdo_pior_cen_estress.ToString("N2");
                }
                set {
                    decimal var_diar_perc_cota_fdo_pior_cen_estress = 0;

                    if (Decimal.TryParse(value, out var_diar_perc_cota_fdo_pior_cen_estress))
                        var_diar_perc_cota_fdo_pior_cen_estress = var_diar_perc_cota_fdo_pior_cen_estress;
                }
            }
            #endregion

            //[XmlElement("VAR_DIAR_PERC_COTA_FDO_PIOR_CEN_ESTRESS", Order = 13)]
            ///* SUBSTITUA ESSA FRASE PELA VARIACAO DIARIA NO PIOR CASO DE ESTRESSE (Obrigatório, opcional para fundo de cotas */
            //public string var_diar_perc_cota_fdo_pior_cen_estress;


            [XmlIgnore]
            /* SUBSTITUA ESSA FRASE PELA VARIACAO DIARIA COM TAXA ANUAL DE JUROS (PRé). CONSIDERAR O úLTIMO DIA úTIL DO MêS DE REFERêNCIA 
             * (Obrigatório, opcional para fundo de cotas) */
            public decimal var_diar_perc_patrim_fdo_var_n_taxa_anual;

            #region VAR_DIAR_PERC_PATRIM_FDO_VAR_N_TAXA_ANUAL
            [XmlElement("VAR_DIAR_PERC_PATRIM_FDO_VAR_N_TAXA_ANUAL", Order = 14)]
            public string format_var_diar_perc_patrim_fdo_var_n_taxa_anual {

                get {
                    return this.var_diar_perc_patrim_fdo_var_n_taxa_anual.ToString("N2");
                }
                set {
                    decimal var_diar_perc_patrim_fdo_var_n_taxa_anual = 0;

                    if (Decimal.TryParse(value, out var_diar_perc_patrim_fdo_var_n_taxa_anual))
                        var_diar_perc_patrim_fdo_var_n_taxa_anual = var_diar_perc_patrim_fdo_var_n_taxa_anual;
                }
            }
            #endregion

            //[XmlElement("VAR_DIAR_PERC_PATRIM_FDO_VAR_N_TAXA_ANUAL", Order = 14)]
            ///* SUBSTITUA ESSA FRASE PELA VARIACAO DIARIA COM TAXA ANUAL DE JUROS (PRé). CONSIDERAR O úLTIMO DIA úTIL DO MêS DE REFERêNCIA 
            // * (Obrigatório, opcional para fundo de cotas) */
            //public string var_diar_perc_patrim_fdo_var_n_taxa_anual;


            [XmlIgnore]
            /* SUBSTITUA ESSA FRASE PELA VARIACAO NEGATIVA DE 1% NA TAXA DE CAMBIO (US$/REAL) CONSIDERAR O úLTIMO DIA úTIL DO MêS DE REFERêNCIA 
             * (Obrigatório, opcional para fundo de cotas). */
            public decimal var_diar_perc_patrim_fdo_var_n_taxa_cambio;

            #region VAR_DIAR_PERC_PATRIM_FDO_VAR_N_TAXA_CAMBIO
            [XmlElement("VAR_DIAR_PERC_PATRIM_FDO_VAR_N_TAXA_CAMBIO", Order = 15)]
            public string format_var_diar_perc_patrim_fdo_var_n_taxa_cambio {

                get {
                    return this.var_diar_perc_patrim_fdo_var_n_taxa_cambio.ToString("N2");
                }
                set {
                    decimal var_diar_perc_patrim_fdo_var_n_taxa_cambio = 0;

                    if (Decimal.TryParse(value, out var_diar_perc_patrim_fdo_var_n_taxa_cambio))
                        var_diar_perc_patrim_fdo_var_n_taxa_cambio = var_diar_perc_patrim_fdo_var_n_taxa_cambio;
                }
            }
            #endregion

            //[XmlElement("VAR_DIAR_PERC_PATRIM_FDO_VAR_N_TAXA_CAMBIO", Order = 15)]
            ///* SUBSTITUA ESSA FRASE PELA VARIACAO NEGATIVA DE 1% NA TAXA DE CAMBIO (US$/REAL) CONSIDERAR O úLTIMO DIA úTIL DO MêS DE REFERêNCIA 
            // * (Obrigatório, opcional para fundo de cotas). */
            //public string var_diar_perc_patrim_fdo_var_n_taxa_cambio;


            [XmlIgnore]
            /* SUBSTITUA ESSA FRASE PELA VARIACAO DIARIA NEGATIVA DE 1% NO PREçO DAS AçõES (Obrigatório, opcional para fundo de cotas */
            public decimal var_patrim_fdo_n_preco_acoes;

            #region VAR_PATRIM_FDO_N_PRECO_ACOES
            [XmlElement("VAR_PATRIM_FDO_N_PRECO_ACOES", Order = 16)]
            public string format_var_patrim_fdo_n_preco_acoes {

                get {
                    return this.var_patrim_fdo_n_preco_acoes.ToString("N2");
                }
                set {
                    decimal var_patrim_fdo_n_preco_acoes = 0;

                    if (Decimal.TryParse(value, out var_patrim_fdo_n_preco_acoes))
                        var_patrim_fdo_n_preco_acoes = var_patrim_fdo_n_preco_acoes;
                }
            }
            #endregion

            //[XmlElement("VAR_PATRIM_FDO_N_PRECO_ACOES", Order = 16)]
            ///* SUBSTITUA ESSA FRASE PELA VARIACAO DIARIA NEGATIVA DE 1% NO PREçO DAS AçõES (Obrigatório, opcional para fundo de cotas */ 
            //public string var_patrim_fdo_n_preco_acoes;


            #region VARIACAO_DIAR_PERC_PATRIM_FDO_VAR_N_OUTROS
            [XmlElementAttribute("VARIACAO_DIAR_PERC_PATRIM_FDO_VAR_N_OUTROS", Order = 17)]
            public VARIACAO_DIAR_PERC_PATRIM_FDO_VAR_N_OUTROS variacao_diar_perc_patrim_fdo_var_n_outros = new VARIACAO_DIAR_PERC_PATRIM_FDO_VAR_N_OUTROS();

            [XmlIgnore]
            public VARIACAO_DIAR_PERC_PATRIM_FDO_VAR_N_OUTROS get_VARIACAO_DIAR_PERC_PATRIM_FDO_VAR_N_OUTROS {
                get { return variacao_diar_perc_patrim_fdo_var_n_outros; }
                set { variacao_diar_perc_patrim_fdo_var_n_outros = value; }
            }

            #endregion

            #region VALOR_NOC_TOT_CONTRAT_DERIV_MANT_FDO
            [XmlElementAttribute("VALOR_NOC_TOT_CONTRAT_DERIV_MANT_FDO", Order = 18)]
            //public VALOR_NOC_TOT_CONTRAT_DERIV_MANT_FDO valor_noc_tot_contrat_deriv_mant_fdo = new VALOR_NOC_TOT_CONTRAT_DERIV_MANT_FDO();
            public VALOR_NOC_TOT_CONTRAT_DERIV_MANT_FDO valor_noc_tot_contrat_deriv_mant_fdo = null;

            [XmlIgnore]
            public VALOR_NOC_TOT_CONTRAT_DERIV_MANT_FDO get_VALOR_NOC_TOT_CONTRAT_DERIV_MANT_FDO {
                get {
                    if (this.valor_noc_tot_contrat_deriv_mant_fdo != null) return valor_noc_tot_contrat_deriv_mant_fdo;
                    else return new VALOR_NOC_TOT_CONTRAT_DERIV_MANT_FDO();
                }
                set { valor_noc_tot_contrat_deriv_mant_fdo = value; }
            }
            #endregion

            #region LISTA_OPER_CURS_MERC_BALCAO
            [XmlElementAttribute("LISTA_OPER_CURS_MERC_BALCAO", Order = 19)]
            //public LISTA_OPER_CURS_MERC_BALCAO lista_oper_curs_merc_balcao = new LISTA_OPER_CURS_MERC_BALCAO();
            public LISTA_OPER_CURS_MERC_BALCAO lista_oper_curs_merc_balcao = null;

            [XmlIgnore]
            public LISTA_OPER_CURS_MERC_BALCAO get_LISTA_OPER_CURS_MERC_BALCAO {
                get {
                    if (this.lista_oper_curs_merc_balcao != null) return lista_oper_curs_merc_balcao;
                    else return new LISTA_OPER_CURS_MERC_BALCAO();
                }
                set { lista_oper_curs_merc_balcao = value; }
            }

            #endregion

            [XmlIgnore]
            /* SUBSTITUA ESSA FRASE PELO VALOR TOTAL DOS ATIVOS (EM % DO PL) EM ESTOQUE DE EMISSãO DE PARTES RELACIONADAS (Obrigatório) */
            public decimal tot_ativos_p_relac;

            #region TOT_ATIVOS_P_RELAC
            [XmlElement("TOT_ATIVOS_P_RELAC", Order = 20)]
            public string format_tot_ativos_p_relac {

                get {
                    return this.tot_ativos_p_relac.ToString("N1");
                }
                set {
                    decimal tot_ativos_p_relac = 0;

                    if (Decimal.TryParse(value, out tot_ativos_p_relac))
                        tot_ativos_p_relac = tot_ativos_p_relac;
                }
            }
            #endregion

            //[XmlElement("TOT_ATIVOS_P_RELAC", Order = 20)]
            ///* SUBSTITUA ESSA FRASE PELO VALOR TOTAL DOS ATIVOS (EM % DO PL) EM ESTOQUE DE EMISSãO DE PARTES RELACIONADAS (Obrigatório) */
            //public string tot_ativos_p_relac;

            #region LISTA_EMISSORES_TIT_CRED_PRIV
            [XmlElementAttribute("LISTA_EMISSORES_TIT_CRED_PRIV", Order = 21)]
            //public LISTA_EMISSORES_TIT_CRED_PRIV lista_emissores_tit_cred_priv = new LISTA_EMISSORES_TIT_CRED_PRIV();
            public LISTA_EMISSORES_TIT_CRED_PRIV lista_emissores_tit_cred_priv = null;

            [XmlIgnore]
            public LISTA_EMISSORES_TIT_CRED_PRIV get_LISTA_EMISSORES_TIT_CRED_PRIV {
                get {
                    if (this.lista_emissores_tit_cred_priv != null) return lista_emissores_tit_cred_priv;
                    else return new LISTA_EMISSORES_TIT_CRED_PRIV();
                }
                set { lista_emissores_tit_cred_priv = value; }
            }
            #endregion

            [XmlIgnore]
            /* SUBSTITUA ESSA FRASE PELO VALOR DO TOTAL DE ATIVOS DE CRéDITO PRIVADO (EM % DO PL) EM ESTOQUE.(Obrigatório) */
            public decimal tot_ativos_cred_priv;

            #region TOT_ATIVOS_CRED_PRIV
            [XmlElement("TOT_ATIVOS_CRED_PRIV", Order = 22)]
            public string format_tot_ativos_cred_priv {

                get {
                    return this.tot_ativos_cred_priv.ToString("N1");
                }
                set {
                    decimal tot_ativos_cred_priv = 0;

                    if (Decimal.TryParse(value, out tot_ativos_cred_priv))
                        tot_ativos_cred_priv = tot_ativos_cred_priv;
                }
            }
            #endregion

            //[XmlElement("TOT_ATIVOS_CRED_PRIV", Order = 22)]
            ///* SUBSTITUA ESSA FRASE PELO VALOR DO TOTAL DE ATIVOS DE CRéDITO PRIVADO (EM % DO PL) EM ESTOQUE.(Obrigatório) */
            //public string tot_ativos_cred_priv;


            [XmlElement("VED_REGUL_COBR_TAXA_PERFORM", Order = 23)]
            /* SUBSTITUIR ESSA FRASE PELA INFORMAçãO(S/N) SE é VEDADA NO REGULAMENTO A COBRANçA DE TAXA DE PERFORMANCE QUANDO O VALOR DA COTA DO FUNDO 
             * FOR INFERIOR AO SEU VALOR POR OCASIãO DA úLTIMA COBRANçA EFETUADA, NA FORMA DO DISPOSTO NO  2 DO ART. 62 DA INSTRUçãO CVM N 409, DE 2004 
             * (Obrigatório, se houver taxa de performance) */
            public string ved_regul_cobr_taxa_perform;

            #region RESP_VED_REGUL_COBR_TAXA_PERFORM
            [XmlElementAttribute("RESP_VED_REGUL_COBR_TAXA_PERFORM", Order = 24)]
            //public RESP_VED_REGUL_COBR_TAXA_PERFORM resp_ved_regul_cobr_taxa_perform = new RESP_VED_REGUL_COBR_TAXA_PERFORM();
            public RESP_VED_REGUL_COBR_TAXA_PERFORM resp_ved_regul_cobr_taxa_perform = null;

            [XmlIgnore]
            public RESP_VED_REGUL_COBR_TAXA_PERFORM get_RESP_VED_REGUL_COBR_TAXA_PERFORM {
                get {
                    if (this.resp_ved_regul_cobr_taxa_perform != null) return resp_ved_regul_cobr_taxa_perform;
                    else return new RESP_VED_REGUL_COBR_TAXA_PERFORM();

                }
                set { resp_ved_regul_cobr_taxa_perform = value; }
            }
            #endregion

            [XmlIgnore]
            /*  MONTANTE_DISTRIB */
            public decimal montante_distrib;

            #region MONTANTE_DISTRIB
            [XmlElement("MONTANTE_DISTRIB", Order = 25)]
            public string format_montante_distrib {

                get {
                    return this.montante_distrib.ToString("N2");
                }
                set {
                    decimal montante_distrib = 0;

                    if (Decimal.TryParse(value, out montante_distrib))
                        montante_distrib = montante_distrib;
                }
            }
            #endregion
        }
        /*-----------------------------------------------------------------------------------*/

        public class NR_CLIENT {
            public NR_CLIENT() { }

            [XmlElement("NR_PF_PRIV_BANK", Order = 1)]
            public int NR_PF_PRIV_BANK;

            [XmlElement("NR_PF_VARJ", Order = 2)]
            public int NR_PF_VARJ;

            [XmlElement("NR_PJ_N_FINANC_PRIV_BANK", Order = 3)]
            public int NR_PJ_N_FINANC_PRIV_BANK;

            [XmlElement("NR_PJ_N_FINANC_VARJ", Order = 4)]
            public int NR_PJ_N_FINANC_VARJ;

            [XmlElement("NR_BNC_COMERC", Order = 5)]
            public int NR_BNC_COMERC;

            [XmlElement("NR_PJ_CORR_DIST", Order = 6)]
            public int NR_PJ_CORR_DIST;

            [XmlElement("NR_PJ_OUTR_FINANC", Order = 7)]
            public int NR_PJ_OUTR_FINANC;

            [XmlElement("NR_INV_N_RES", Order = 8)]
            public int NR_INV_N_RES;

            [XmlElement("NR_ENT_AB_PREV_COMPL", Order = 9)]
            public int NR_ENT_AB_PREV_COMPL;

            [XmlElement("NR_ENT_FC_PREV_COMPL", Order = 10)]
            public int NR_ENT_FC_PREV_COMPL;

            [XmlElement("NR_REG_PREV_SERV_PUB", Order = 11)]
            public int NR_REG_PREV_SERV_PUB;

            [XmlElement("NR_SOC_SEG_RESEG", Order = 12)]
            public int NR_SOC_SEG_RESEG;

            [XmlElement("NR_SOC_CAPTLZ_ARRENDM_MERC", Order = 13)]
            public int NR_SOC_CAPTLZ_ARRENDM_MERC;

            [XmlElement("NR_FDOS_CLUB_INV", Order = 14)]
            public int NR_FDOS_CLUB_INV;

            [XmlElement("NR_COTST_DISTR_FDO", Order = 15)]
            public int NR_COTST_DISTR_FDO;

            [XmlElement("NR_OUTROS_N_RELAC", Order = 16)]
            public int NR_OUTROS_N_RELAC;
        }

        public class DISTR_PATRIM {
            public DISTR_PATRIM() { }

            [XmlIgnore]
            public decimal PR_PF_PRIV_BANK;

            #region PR_PF_PRIV_BANK
            [XmlElement("PR_PF_PRIV_BANK", Order = 1)]
            public string format_PR_PF_PRIV_BANK {

                get {
                    return this.PR_PF_PRIV_BANK.ToString("N1");
                }
                set {
                    decimal PR_PF_PRIV_BANK = 0;

                    if (Decimal.TryParse(value, out PR_PF_PRIV_BANK))
                        PR_PF_PRIV_BANK = PR_PF_PRIV_BANK;
                }
            }
            #endregion

            //[XmlElement("PR_PF_PRIV_BANK", Order = 1)]
            //public string PR_PF_PRIV_BANK;

            [XmlIgnore]
            public decimal PR_PF_VARJ;

            #region PR_PF_VARJ
            [XmlElement("PR_PF_VARJ", Order = 2)]
            public string format_PR_PF_VARJ {

                get {
                    return this.PR_PF_VARJ.ToString("N1");
                }
                set {
                    decimal PR_PF_VARJ = 0;

                    if (Decimal.TryParse(value, out PR_PF_VARJ))
                        PR_PF_VARJ = PR_PF_VARJ;
                }
            }
            #endregion

            //[XmlElement("PR_PF_VARJ", Order = 2)]
            //public string PR_PF_VARJ;

            [XmlIgnore]
            public decimal PR_PJ_N_FINANC_PRIV_BANK;

            #region PR_PJ_N_FINANC_PRIV_BANK
            [XmlElement("PR_PJ_N_FINANC_PRIV_BANK", Order = 3)]
            public string format_PR_PJ_N_FINANC_PRIV_BANK {

                get {
                    return this.PR_PJ_N_FINANC_PRIV_BANK.ToString("N1");
                }
                set {
                    decimal PR_PJ_N_FINANC_PRIV_BANK = 0;

                    if (Decimal.TryParse(value, out PR_PJ_N_FINANC_PRIV_BANK))
                        PR_PJ_N_FINANC_PRIV_BANK = PR_PJ_N_FINANC_PRIV_BANK;
                }
            }
            #endregion

            //[XmlElement("PR_PJ_N_FINANC_PRIV_BANK", Order = 3)]
            //public string PR_PJ_N_FINANC_PRIV_BANK;

            [XmlIgnore]
            public decimal PR_PJ_N_FINANC_VARJ;

            #region PR_PJ_N_FINANC_VARJ
            [XmlElement("PR_PJ_N_FINANC_VARJ", Order = 4)]
            public string format_PR_PJ_N_FINANC_VARJ {

                get {
                    return this.PR_PJ_N_FINANC_VARJ.ToString("N1");
                }
                set {
                    decimal PR_PJ_N_FINANC_VARJ = 0;

                    if (Decimal.TryParse(value, out PR_PJ_N_FINANC_VARJ))
                        PR_PJ_N_FINANC_VARJ = PR_PJ_N_FINANC_VARJ;
                }
            }
            #endregion


            //[XmlElement("PR_PJ_N_FINANC_VARJ", Order = 4)]
            //public string PR_PJ_N_FINANC_VARJ;

            [XmlIgnore]
            public decimal PR_BNC_COMERC;

            #region PR_BNC_COMERC
            [XmlElement("PR_BNC_COMERC", Order = 5)]
            public string format_PR_BNC_COMERC {

                get {
                    return this.PR_BNC_COMERC.ToString("N1");
                }
                set {
                    decimal PR_BNC_COMERC = 0;

                    if (Decimal.TryParse(value, out PR_BNC_COMERC))
                        PR_BNC_COMERC = PR_BNC_COMERC;
                }
            }
            #endregion

            //[XmlElement("PR_BNC_COMERC", Order = 5)]
            //public string PR_BNC_COMERC;

            [XmlIgnore]
            public decimal PR_PJ_CORR_DIST;

            #region PR_PJ_CORR_DIST
            [XmlElement("PR_PJ_CORR_DIST", Order = 6)]
            public string format_PR_PJ_CORR_DIST {

                get {
                    return this.PR_PJ_CORR_DIST.ToString("N1");
                }
                set {
                    decimal PR_PJ_CORR_DIST = 0;

                    if (Decimal.TryParse(value, out PR_PJ_CORR_DIST))
                        PR_PJ_CORR_DIST = PR_PJ_CORR_DIST;
                }
            }
            #endregion

            //[XmlElement("PR_PJ_CORR_DIST", Order = 6)]
            //public string PR_PJ_CORR_DIST;

            [XmlIgnore]
            public decimal PR_PJ_OUTR_FINANC;

            #region PR_PJ_OUTR_FINANC
            [XmlElement("PR_PJ_OUTR_FINANC", Order = 7)]
            public string format_PR_PJ_OUTR_FINANC {

                get {
                    return this.PR_PJ_OUTR_FINANC.ToString("N1");
                }
                set {
                    decimal PR_PJ_OUTR_FINANC = 0;

                    if (Decimal.TryParse(value, out PR_PJ_OUTR_FINANC))
                        PR_PJ_OUTR_FINANC = PR_PJ_OUTR_FINANC;
                }
            }
            #endregion

            //[XmlElement("PR_PJ_OUTR_FINANC", Order = 7)]
            //public string PR_PJ_OUTR_FINANC;

            [XmlIgnore]
            public decimal PR_INV_N_RES;

            #region PR_INV_N_RES
            [XmlElement("PR_INV_N_RES", Order = 8)]
            public string format_PR_INV_N_RES {

                get {
                    return this.PR_INV_N_RES.ToString("N1");
                }
                set {
                    decimal PR_INV_N_RES = 0;

                    if (Decimal.TryParse(value, out PR_INV_N_RES))
                        PR_INV_N_RES = PR_INV_N_RES;
                }
            }
            #endregion

            //[XmlElement("PR_INV_N_RES", Order = 8)]
            //public string PR_INV_N_RES;

            [XmlIgnore]
            public decimal PR_ENT_AB_PREV_COMPL;

            #region PR_ENT_AB_PREV_COMPL
            [XmlElement("PR_ENT_AB_PREV_COMPL", Order = 9)]
            public string format_PR_ENT_AB_PREV_COMPL {

                get {
                    return this.PR_ENT_AB_PREV_COMPL.ToString("N1");
                }
                set {
                    decimal PR_ENT_AB_PREV_COMPL = 0;

                    if (Decimal.TryParse(value, out PR_ENT_AB_PREV_COMPL))
                        PR_ENT_AB_PREV_COMPL = PR_ENT_AB_PREV_COMPL;
                }
            }
            #endregion

            //[XmlElement("PR_ENT_AB_PREV_COMPL", Order = 9)]
            //public string PR_ENT_AB_PREV_COMPL;

            [XmlIgnore]
            public decimal PR_ENT_FC_PREV_COMPL;

            #region PR_ENT_FC_PREV_COMPL
            [XmlElement("PR_ENT_FC_PREV_COMPL", Order = 10)]
            public string format_PR_ENT_FC_PREV_COMPL {

                get {
                    return this.PR_ENT_FC_PREV_COMPL.ToString("N1");
                }
                set {
                    decimal PR_ENT_FC_PREV_COMPL = 0;

                    if (Decimal.TryParse(value, out PR_ENT_FC_PREV_COMPL))
                        PR_ENT_FC_PREV_COMPL = PR_ENT_FC_PREV_COMPL;
                }
            }
            #endregion

            //[XmlElement("PR_ENT_FC_PREV_COMPL", Order = 10)]
            //public string PR_ENT_FC_PREV_COMPL;

            [XmlIgnore]
            public decimal PR_REG_PREV_SERV_PUB;

            #region PR_REG_PREV_SERV_PUB
            [XmlElement("PR_REG_PREV_SERV_PUB", Order = 11)]
            public string format_PR_REG_PREV_SERV_PUB {

                get {
                    return this.PR_REG_PREV_SERV_PUB.ToString("N1");
                }
                set {
                    decimal PR_REG_PREV_SERV_PUB = 0;

                    if (Decimal.TryParse(value, out PR_REG_PREV_SERV_PUB))
                        PR_REG_PREV_SERV_PUB = PR_REG_PREV_SERV_PUB;
                }
            }
            #endregion

            //[XmlElement("PR_REG_PREV_SERV_PUB", Order = 11)]
            //public string PR_REG_PREV_SERV_PUB;

            [XmlIgnore]
            public decimal PR_SOC_SEG_RESEG;

            #region PR_SOC_SEG_RESEG
            [XmlElement("PR_SOC_SEG_RESEG", Order = 12)]
            public string format_PR_SOC_SEG_RESEG {

                get {
                    return this.PR_SOC_SEG_RESEG.ToString("N1");
                }
                set {
                    decimal PR_SOC_SEG_RESEG = 0;

                    if (Decimal.TryParse(value, out PR_SOC_SEG_RESEG))
                        PR_SOC_SEG_RESEG = PR_SOC_SEG_RESEG;
                }
            }
            #endregion

            //[XmlElement("PR_SOC_SEG_RESEG", Order = 12)]
            //public string PR_SOC_SEG_RESEG;

            [XmlIgnore]
            public decimal PR_SOC_CAPTLZ_ARRENDM_MERC;

            #region PR_SOC_CAPTLZ_ARRENDM_MERC
            [XmlElement("PR_SOC_CAPTLZ_ARRENDM_MERC", Order = 13)]
            public string format_PR_SOC_CAPTLZ_ARRENDM_MERC {

                get {
                    return this.PR_SOC_CAPTLZ_ARRENDM_MERC.ToString("N1");
                }
                set {
                    decimal PR_SOC_CAPTLZ_ARRENDM_MERC = 0;

                    if (Decimal.TryParse(value, out PR_SOC_CAPTLZ_ARRENDM_MERC))
                        PR_SOC_CAPTLZ_ARRENDM_MERC = PR_SOC_CAPTLZ_ARRENDM_MERC;
                }
            }
            #endregion


            ////[XmlElement("PR_SOC_CAPTLZ_ARRENDM_MERC", Order = 13)]
            ////public string PR_SOC_CAPTLZ_ARRENDM_MERC;

            [XmlIgnore]
            public decimal PR_FDOS_CLUB_INV;


            #region PR_FDOS_CLUB_INV
            [XmlElement("PR_FDOS_CLUB_INV", Order = 14)]
            public string format_PR_FDOS_CLUB_INV {

                get {
                    return this.PR_FDOS_CLUB_INV.ToString("N1");
                }
                set {
                    decimal PR_FDOS_CLUB_INV = 0;

                    if (Decimal.TryParse(value, out PR_FDOS_CLUB_INV))
                        PR_FDOS_CLUB_INV = PR_FDOS_CLUB_INV;
                }
            }
            #endregion

            //[XmlElement("PR_FDOS_CLUB_INV", Order = 14)]
            //public string PR_FDOS_CLUB_INV;

            [XmlIgnore]
            public decimal PR_COTST_DISTR_FDO;


            #region PR_COTST_DISTR_FDO
            [XmlElement("PR_COTST_DISTR_FDO", Order = 15)]
            public string format_PR_COTST_DISTR_FDO {

                get {
                    return this.PR_COTST_DISTR_FDO.ToString("N1");
                }
                set {
                    decimal PR_COTST_DISTR_FDO = 0;

                    if (Decimal.TryParse(value, out PR_COTST_DISTR_FDO))
                        PR_COTST_DISTR_FDO = PR_COTST_DISTR_FDO;
                }
            }
            #endregion


            //[XmlElement("PR_COTST_DISTR_FDO", Order = 15)]
            //public string PR_COTST_DISTR_FDO;


            [XmlIgnore]
            public decimal PR_OUTROS_N_RELAC;

            #region PR_OUTROS_N_RELAC
            [XmlElement("PR_OUTROS_N_RELAC", Order = 16)]
            public string format_PR_OUTROS_N_RELAC {

                get {
                    return this.PR_OUTROS_N_RELAC.ToString("N1");
                }
                set {
                    decimal PR_OUTROS_N_RELAC = 0;

                    if (Decimal.TryParse(value, out PR_OUTROS_N_RELAC))
                        PR_OUTROS_N_RELAC = PR_OUTROS_N_RELAC;
                }
            }
            #endregion

            //[XmlElement("PR_OUTROS_N_RELAC", Order = 16)]
            //public string PR_OUTROS_N_RELAC;                                             
        }

        public class VARIACAO_PERC_VAL_COTA {

            public VARIACAO_PERC_VAL_COTA() { }

            [XmlIgnore]
            /* SUBSTITUA ESTA FRASE PELO VALOR TOTAL DOS CONTRATOS DE COMPRA DE US$ LIQUIDADOS NO MeS (obrigatório). */
            public decimal VAL_PERCENT;

            #region VAL_PERCENT
            [XmlElement("VAL_PERCENT", Order = 1)]
            public string format_VAL_PERCENT {

                get {
                    return this.VAL_PERCENT.ToString("N2");
                }
                set {
                    decimal VAL_PERCENT = 0;

                    if (Decimal.TryParse(value, out VAL_PERCENT))
                        VAL_PERCENT = VAL_PERCENT;
                }
            }
            #endregion

            //[XmlElement("VAL_PERCENT", Order = 1)]
            //public string VAL_PERCENT;

            [XmlElementAttribute("LISTA_FATOR_PRIMIT_RISCO", Order = 2)]
            public LISTA_FATOR_PRIMIT_RISCO lista_fator_primit_risco = new LISTA_FATOR_PRIMIT_RISCO();
        }

        public class LISTA_FATOR_PRIMIT_RISCO {
            public LISTA_FATOR_PRIMIT_RISCO() { }

            #region FATOR_PRIMIT_RISCO
            [XmlElementAttribute("FATOR_PRIMIT_RISCO", Order = 1)]
            public List<FATOR_PRIMIT_RISCO> fator_primit_risco = new List<FATOR_PRIMIT_RISCO>();

            [XmlIgnore]
            public List<FATOR_PRIMIT_RISCO> get_FATOR_PRIMIT_RISCO {
                get { return fator_primit_risco; }
                set { fator_primit_risco = value; }
            }
            #endregion
        }

        public class FATOR_PRIMIT_RISCO {
            public FATOR_PRIMIT_RISCO() { }

            [XmlElement("NOME_FATOR_PRIMIT_RISCO", Order = 1)]
            public string NOME_FATOR_PRIMIT_RISCO;

            [XmlElement("CEN_UTIL", Order = 2)]
            public string CEN_UTIL;
        }

        public class VARIACAO_DIAR_PERC_PATRIM_FDO_VAR_N_OUTROS {
            public VARIACAO_DIAR_PERC_PATRIM_FDO_VAR_N_OUTROS() { }

            [XmlElement("FATOR_RISCO_OUTROS", Order = 1)]
            public string FATOR_RISCO_OUTROS;

            [XmlIgnore]
            public decimal VAL_PERCENT_OUTROS;

            #region VAL_PERCENT_OUTROS
            [XmlElement("VAL_PERCENT_OUTROS", Order = 2)]
            public string format_VAL_PERCENT_OUTROS {

                get {
                    return this.VAL_PERCENT_OUTROS.ToString("N2");
                }
                set {
                    decimal VAL_PERCENT_OUTROS = 0;

                    if (Decimal.TryParse(value, out VAL_PERCENT_OUTROS))
                        VAL_PERCENT_OUTROS = VAL_PERCENT_OUTROS;
                }
            }
            #endregion

            //[XmlElement("VAL_PERCENT_OUTROS", Order = 2)]
            //public string VAL_PERCENT_OUTROS;
        }

        public class VALOR_NOC_TOT_CONTRAT_DERIV_MANT_FDO {
            public VALOR_NOC_TOT_CONTRAT_DERIV_MANT_FDO() { }

            [XmlIgnore]
            public decimal? VAL_COLATERAL;

            #region VAL_COLATERAL
            [XmlElement("VAL_COLATERAL", Order = 1)]
            public string format_VAL_COLATERAL {

                get {
                    return this.VAL_COLATERAL.HasValue ? VAL_COLATERAL.Value.ToString("N2") : String.Empty;
                }
                set {

                    if (!this.VAL_COLATERAL.HasValue) {
                        format_VAL_COLATERAL = String.Empty;
                    }
                    else {
                        decimal VAL_COLATERAL;

                        if (Decimal.TryParse(value, out VAL_COLATERAL))
                            VAL_COLATERAL = VAL_COLATERAL;
                    }
                }
            }
            #endregion

            //[XmlElement("VAL_COLATERAL", Order = 1)]
            //public string VAL_COLATERAL;

            [XmlElementAttribute("LISTA_FATOR_RISCO_NOC", Order = 2)]
            //public LISTA_FATOR_RISCO_NOC lista_fator_risco_noc = new LISTA_FATOR_RISCO_NOC();
            public LISTA_FATOR_RISCO_NOC lista_fator_risco_noc = null;
        }

        public class LISTA_FATOR_RISCO_NOC {
            public LISTA_FATOR_RISCO_NOC() { }

            #region FATOR_RISCO_NOC
            [XmlElementAttribute("FATOR_RISCO_NOC", Order = 1)]
            public List<FATOR_RISCO_NOC> fator_risco_noc = new List<FATOR_RISCO_NOC>();

            [XmlIgnore]
            public List<FATOR_RISCO_NOC> get_FATOR_RISCO_NOC {
                get { return fator_risco_noc; }
                set { fator_risco_noc = value; }
            }
            #endregion
        }

        public class FATOR_RISCO_NOC {
            public FATOR_RISCO_NOC() { }

            [XmlElement("NOME_FATOR_NOC", Order = 1)]
            public string NOME_FATOR_NOC;

            [XmlElement("VAL_FATOR_RISCO_NOC_LONG", Order = 2)]
            public ulong VAL_FATOR_RISCO_NOC_LONG;

            [XmlElement("VAL_FATOR_RISCO_NOC_SHORT", Order = 3)]
            public ushort VAL_FATOR_RISCO_NOC_SHORT;
        }

        public class LISTA_OPER_CURS_MERC_BALCAO {
            public LISTA_OPER_CURS_MERC_BALCAO() { }

            #region OPER_CURS_MERC_BALCAO
            [XmlElementAttribute("OPER_CURS_MERC_BALCAO", Order = 1)]
            public List<OPER_CURS_MERC_BALCAO> oper_curs_merc_balcao = new List<OPER_CURS_MERC_BALCAO>();

            [XmlIgnore]
            public List<OPER_CURS_MERC_BALCAO> get_OPER_CURS_MERC_BALCAO {
                get { return oper_curs_merc_balcao; }
                set { oper_curs_merc_balcao = value; }
            }
            #endregion
        }

        public class OPER_CURS_MERC_BALCAO {
            public OPER_CURS_MERC_BALCAO() { }

            [XmlElement("TP_PESSOA", Order = 1)]
            public string TP_PESSOA;

            [XmlElement("NR_PF_PJ_COMITENTE", Order = 2)]
            public string NR_PF_PJ_COMITENTE;

            [XmlElement("PARTE_RELACIONADA", Order = 3)]
            public string PARTE_RELACIONADA;

            [XmlIgnore]
            public decimal VALOR_PARTE;

            #region VALOR_PARTE
            [XmlElement("VALOR_PARTE", Order = 4)]
            public string format_VALOR_PARTE {

                get {
                    return this.VALOR_PARTE.ToString("N1");
                }
                set {
                    decimal VALOR_PARTE = 0;

                    if (Decimal.TryParse(value, out VALOR_PARTE))
                        VALOR_PARTE = VALOR_PARTE;
                }
            }
            #endregion

            //[XmlElement("VALOR_PARTE", Order = 4)]
            //public string VALOR_PARTE;
        }

        public class LISTA_EMISSORES_TIT_CRED_PRIV {
            public LISTA_EMISSORES_TIT_CRED_PRIV() { }

            #region EMISSORES_TIT_CRED_PRIV
            [XmlElementAttribute("EMISSORES_TIT_CRED_PRIV", Order = 1)]
            public List<EMISSORES_TIT_CRED_PRIV> emissores_tit_cred_priv = new List<EMISSORES_TIT_CRED_PRIV>();

            [XmlIgnore]
            public List<EMISSORES_TIT_CRED_PRIV> get_EMISSORES_TIT_CRED_PRIV {
                get { return emissores_tit_cred_priv; }
                set { emissores_tit_cred_priv = value; }
            }
            #endregion
        }

        public class EMISSORES_TIT_CRED_PRIV {
            public EMISSORES_TIT_CRED_PRIV() { }

            [XmlElement("TP_PESSOA_EMISSOR", Order = 1)]
            public string TP_PESSOA_EMISSOR;

            [XmlElement("NR_PF_PJ_EMISSOR", Order = 2)]
            public string NR_PF_PJ_EMISSOR;

            [XmlElement("PARTE_RELACIONADA", Order = 3)]
            public string PARTE_RELACIONADA;

            [XmlIgnore]
            public decimal VALOR_PARTE;

            #region TOTAL_RECURS_EXTER
            [XmlElement("VALOR_PARTE", Order = 4)]
            public string format_VALOR_PARTE {

                get {
                    return this.VALOR_PARTE.ToString("N1");
                }
                set {
                    decimal VALOR_PARTE = 0;

                    if (Decimal.TryParse(value, out VALOR_PARTE))
                        VALOR_PARTE = VALOR_PARTE;
                }
            }
            #endregion

            //[XmlElement("VALOR_PARTE", Order = 4)]
            //public string VALOR_PARTE;
        }

        public class RESP_VED_REGUL_COBR_TAXA_PERFORM {
            public RESP_VED_REGUL_COBR_TAXA_PERFORM() { }

            #region DATA_COTA_FUNDO
            [XmlIgnore]
            public DateTime DATA_COTA_FUNDO;

            [XmlElement("DATA_COTA_FUNDO", Order = 1)]
            public string formatDATA_COTA_FUNDO {
                get { return this.DATA_COTA_FUNDO.ToString("dd/MM/yyyy"); }
                set { DATA_COTA_FUNDO = DateTime.Parse(value); }
            }
            #endregion

            //[XmlElement("DATA_COTA_FUNDO", Order = 1)]
            //public string DATA_COTA_FUNDO;

            [XmlIgnore]
            public decimal VAL_COTA_FUNDO;

            #region VAL_COTA_FUNDO
            [XmlElement("VAL_COTA_FUNDO", Order = 2)]
            public string format_VAL_COTA_FUNDO {

                get {
                    return this.VAL_COTA_FUNDO.ToString("N5");
                }
                set {
                    decimal VAL_COTA_FUNDO = 0;

                    if (Decimal.TryParse(value, out VAL_COTA_FUNDO))
                        VAL_COTA_FUNDO = VAL_COTA_FUNDO;
                }
            }
            #endregion

            //[XmlElement("VAL_COTA_FUNDO", Order = 2)]
            //public string VAL_COTA_FUNDO;

        }

        #endregion
    }

    #endregion

}