﻿using System.Collections.Generic;
using System;
using Financial.Bolsa;
using Financial.Bolsa.Enums;
using Financial.Common;
using Financial.Util;
using Financial.BMF;
using Financial.BMF.Enums;
using Financial.Fundo;
using Financial.CRM;
using Financial.ContaCorrente;
using Financial.ContaCorrente.Enums;
using Financial.Fundo.Enums;
using Financial.CRM.Enums;
using Financial.RendaFixa;
using Financial.RendaFixa.Enums;
using Financial.Swap;
using Financial.Investidor;
using EntitySpaces.Interfaces;

namespace Financial.Export
{

    /* Classe parcial*/
    public partial class XMLPosicaoAnbid
    {



        public List<Acoes> RetornaAcoes(int idCliente, DateTime data, bool historico)
        {
            List<Acoes> lista = new List<Acoes>();

            PosicaoBolsaCollection posicaoBolsaCollection = new PosicaoBolsaCollection();
            if (historico)
            {
                PosicaoBolsaHistoricoCollection posicaoBolsaHistoricoCollection = new PosicaoBolsaHistoricoCollection();
                posicaoBolsaHistoricoCollection.Query.Where(posicaoBolsaHistoricoCollection.Query.TipoMercado.In(TipoMercadoBolsa.MercadoVista, TipoMercadoBolsa.Imobiliario),
                                                            posicaoBolsaHistoricoCollection.Query.IdCliente.Equal(idCliente),
                                                            posicaoBolsaHistoricoCollection.Query.DataHistorico.Equal(data));
                posicaoBolsaHistoricoCollection.Query.Load();

                PosicaoBolsaCollection posicaoBolsaCollectionAux = new PosicaoBolsaCollection(posicaoBolsaHistoricoCollection);
                posicaoBolsaCollection.Combine(posicaoBolsaCollectionAux);
            }
            else
            {

                posicaoBolsaCollection.Query.Where(posicaoBolsaCollection.Query.TipoMercado.In(TipoMercadoBolsa.MercadoVista, TipoMercadoBolsa.Imobiliario),
                                                   posicaoBolsaCollection.Query.IdCliente.Equal(idCliente));
                posicaoBolsaCollection.Query.Load();
            }

            foreach (PosicaoBolsa posicaoBolsa in posicaoBolsaCollection)
            {
                string cdAtivoBolsa = posicaoBolsa.CdAtivoBolsa;

                AtivoBolsa ativoBolsa = new AtivoBolsa();
                ativoBolsa.LoadByPrimaryKey(cdAtivoBolsa);

                Emissor emissor = new Emissor();
                emissor.LoadByPrimaryKey(ativoBolsa.IdEmissor.Value);

                FatorCotacaoBolsa fatorCotacaoBolsa = new FatorCotacaoBolsa();
                fatorCotacaoBolsa.BuscaFatorCotacaoBolsa(cdAtivoBolsa, data);

                decimal quantidadeDisponivel = (posicaoBolsa.Quantidade.Value - posicaoBolsa.QuantidadeBloqueada.Value) > 0 ? (posicaoBolsa.Quantidade.Value - posicaoBolsa.QuantidadeBloqueada.Value) : 0;

                decimal quantidadeGarantia = 0;
                if (posicaoBolsa.Quantidade.Value < 0)
                {
                    quantidadeGarantia = Math.Abs(posicaoBolsa.Quantidade.Value);
                }
                else if (posicaoBolsa.QuantidadeBloqueada.Value > 0)
                {
                    quantidadeGarantia = posicaoBolsa.QuantidadeBloqueada.Value;
                }

                decimal valorFinanceiro = Math.Abs(posicaoBolsa.ValorMercado.Value);
                decimal puposicao = posicaoBolsa.PUMercado.Value;

                #region conversão
                if (this.multiMoeda)
                {
                    //***Ajusta para trabalhar com multi-moeda   
                    int idMoedaAtivo = ativoBolsa.IdMoeda.HasValue ? (int)ativoBolsa.IdMoeda.Value : 0;

                    ConversaoMoeda conversaoMoeda = new ConversaoMoeda();
                    decimal fatorMoeda = 1;
                    fatorMoeda = conversaoMoeda.RetornaFatorConversao(this.idMoedaCarteira, idMoedaAtivo, data);

                    valorFinanceiro = Math.Round(valorFinanceiro * fatorMoeda, 2);
                    puposicao = Math.Round(puposicao * fatorMoeda, 8);

                }
                #endregion

                string classeOperacao = posicaoBolsa.Quantidade.Value > 0 ? "C" : "V";
                string cnpjEmissor = emissor.Cnpj;
                if (cnpjEmissor == null)
                {
                    cnpjEmissor = "0";
                }

                Acoes acoes = new Acoes();
                acoes.classeoperacao = classeOperacao;
                acoes.cnpjinter = cnpjEmissor.PadLeft(14, '0');
                acoes.codativo = cdAtivoBolsa.ToUpper();
                acoes.cusip = "";
                acoes.dtvencalug = "";
                acoes.isin = ativoBolsa.CodigoIsin.ToUpper();
                acoes.lote = fatorCotacaoBolsa.Fator.Value.ToString("N0").Replace(".", "").Replace(",", ".");
                acoes.percprovcred = "0.00"; //Forçado % Provisão de Credito = 0
                acoes.puposicao = puposicao.ToString("N8").Replace(".", "").Replace(",", ".");
                acoes.qtdisponivel = quantidadeDisponivel.ToString("N9").Replace(".", "").Replace(",", ".");
                acoes.qtgarantia = quantidadeGarantia.ToString("N9").Replace(".", "").Replace(",", ".");
                acoes.tpconta = "1"; //Sempre Conta movimento
                acoes.tributos = "0.00";
                acoes.txalug = "0.00";
                acoes.valorfindisp = Utilitario.Truncate(quantidadeDisponivel * puposicao / fatorCotacaoBolsa.Fator.Value, 2).ToString("N2").Replace(".", "").Replace(",", ".");
                acoes.valorfinemgar = Utilitario.Truncate(quantidadeGarantia * puposicao / fatorCotacaoBolsa.Fator.Value, 2).ToString("N2").Replace(".", "").Replace(",", ".");

                lista.Add(acoes);
            }

            PosicaoEmprestimoBolsaCollection posicaoEmprestimoBolsaCollection = new PosicaoEmprestimoBolsaCollection();
            if (historico)
            {
                PosicaoEmprestimoBolsaHistoricoCollection posicaoEmprestimoBolsaHistoricoCollection = new PosicaoEmprestimoBolsaHistoricoCollection();
                posicaoEmprestimoBolsaHistoricoCollection.Query.Where(posicaoEmprestimoBolsaHistoricoCollection.Query.IdCliente.Equal(idCliente),
                                                                      posicaoEmprestimoBolsaHistoricoCollection.Query.DataHistorico.Equal(data),
                                                                      posicaoEmprestimoBolsaHistoricoCollection.Query.Quantidade.GreaterThan(0));
                posicaoEmprestimoBolsaHistoricoCollection.Query.Load();

                PosicaoEmprestimoBolsaCollection posicaoEmprestimoBolsaCollectionAux = new PosicaoEmprestimoBolsaCollection(posicaoEmprestimoBolsaHistoricoCollection);
                posicaoEmprestimoBolsaCollection.Combine(posicaoEmprestimoBolsaCollectionAux);
            }
            else
            {

                posicaoEmprestimoBolsaCollection.Query.Where(posicaoEmprestimoBolsaCollection.Query.IdCliente.Equal(idCliente),
                                                             posicaoEmprestimoBolsaCollection.Query.Quantidade.GreaterThan(0));
                posicaoEmprestimoBolsaCollection.Query.Load();
            }

            foreach (PosicaoEmprestimoBolsa posicaoEmprestimoBolsa in posicaoEmprestimoBolsaCollection)
            {
                byte pontaEmprestimo = posicaoEmprestimoBolsa.PontaEmprestimo.Value;
                string cdAtivoBolsa = posicaoEmprestimoBolsa.CdAtivoBolsa;
                DateTime dataVencimento = posicaoEmprestimoBolsa.DataVencimento.Value;
                decimal taxaOperacao = posicaoEmprestimoBolsa.TaxaOperacao.Value;

                AtivoBolsa ativoBolsa = new AtivoBolsa();
                ativoBolsa.LoadByPrimaryKey(cdAtivoBolsa);

                FatorCotacaoBolsa fatorCotacaoBolsa = new FatorCotacaoBolsa();
                fatorCotacaoBolsa.BuscaFatorCotacaoBolsa(cdAtivoBolsa, data);

                decimal valorFinanceiro = Math.Abs(posicaoEmprestimoBolsa.ValorMercado.Value);
                decimal puposicao = posicaoEmprestimoBolsa.PUMercado.Value;

                #region conversão
                if (this.multiMoeda)
                {
                    //***Ajusta para trabalhar com multi-moeda   
                    int idMoedaAtivo = ativoBolsa.IdMoeda.HasValue ? (int)ativoBolsa.IdMoeda.Value : 0;

                    ConversaoMoeda conversaoMoeda = new ConversaoMoeda();
                    decimal fatorMoeda = 1;
                    fatorMoeda = conversaoMoeda.RetornaFatorConversao(this.idMoedaCarteira, idMoedaAtivo, data);

                    valorFinanceiro = Math.Round(valorFinanceiro * fatorMoeda, 2);
                    puposicao = Math.Round(puposicao * fatorMoeda, 8);
                }
                #endregion



                decimal quantidade = posicaoEmprestimoBolsa.Quantidade.Value;

                string classeOperacao = pontaEmprestimo == (byte)PontaEmprestimoBolsa.Doador ? "D" : "T";

                decimal quantidadeGarantia = 0, quantidadeDisponivel = 0;
                if (pontaEmprestimo == (byte)PontaEmprestimoBolsa.Doador)
                {
                    quantidadeDisponivel = 0;
                    quantidadeGarantia = quantidade;
                }
                else
                {
                    quantidadeDisponivel = quantidade;
                    quantidadeGarantia = 0;
                }

                Acoes acoes = new Acoes();
                acoes.classeoperacao = classeOperacao;
                acoes.cnpjinter = "0".PadLeft(14, '0');
                acoes.codativo = cdAtivoBolsa.ToUpper();
                acoes.cusip = "";
                acoes.dtvencalug = dataVencimento.ToString("yyyyMMdd");
                acoes.isin = ativoBolsa.CodigoIsin.ToUpper();
                acoes.lote = fatorCotacaoBolsa.Fator.Value.ToString("N0").Replace(".", "").Replace(",", ".");
                acoes.percprovcred = "0.00"; //Forçado % Provisão de Credito = 0
                acoes.puposicao = puposicao.ToString("N8").Replace(".", "").Replace(",", ".");
                acoes.qtdisponivel = quantidadeDisponivel.ToString("N9").Replace(".", "").Replace(",", ".");
                acoes.qtgarantia = quantidadeGarantia.ToString("N9").Replace(".", "").Replace(",", ".");
                acoes.tpconta = "1"; //Sempre Conta movimento
                acoes.tributos = "0.00";
                acoes.txalug = taxaOperacao.ToString("N2").Replace(".", "").Replace(",", ".");
                acoes.valorfindisp = Utilitario.Truncate(quantidadeDisponivel * puposicao / fatorCotacaoBolsa.Fator.Value, 2).ToString("N2").Replace(".", "").Replace(",", ".");
                acoes.valorfinemgar = Utilitario.Truncate(quantidadeGarantia * puposicao / fatorCotacaoBolsa.Fator.Value, 2).ToString("N2").Replace(".", "").Replace(",", ".");

                lista.Add(acoes);
            }

            return lista;
        }

        public List<Participacao> RetornaAcoesParticipacoes(int idCliente, DateTime data, bool historico)
        {
            List<Participacao> lista = new List<Participacao>();

            AtivoBolsaQuery ativoBolsaQuery = new AtivoBolsaQuery("A");
            PosicaoBolsaCollection posicaoBolsaCollection = new PosicaoBolsaCollection();
            if (historico)
            {
                PosicaoBolsaHistoricoQuery posicaoBolsaHistoricoQuery = new PosicaoBolsaHistoricoQuery("P");

                posicaoBolsaHistoricoQuery.InnerJoin(ativoBolsaQuery).On(ativoBolsaQuery.CdAtivoBolsa == posicaoBolsaHistoricoQuery.CdAtivoBolsa);
                posicaoBolsaHistoricoQuery.Where(posicaoBolsaHistoricoQuery.TipoMercado.In(TipoMercadoBolsa.MercadoVista, TipoMercadoBolsa.Imobiliario) &
                                                 posicaoBolsaHistoricoQuery.IdCliente.Equal(idCliente) &
                                                 posicaoBolsaHistoricoQuery.DataHistorico.Equal(data) &
                                                 ativoBolsaQuery.Participacao.IsNotNull() & ativoBolsaQuery.Participacao.Equal("S"));

                PosicaoBolsaHistoricoCollection posicaoBolsaHistoricoCollection = new PosicaoBolsaHistoricoCollection();
                posicaoBolsaHistoricoCollection.Load(posicaoBolsaHistoricoQuery);

                PosicaoBolsaCollection posicaoBolsaCollectionAux = new PosicaoBolsaCollection(posicaoBolsaHistoricoCollection);
                posicaoBolsaCollection.Combine(posicaoBolsaCollectionAux);
            }
            else
            {

                PosicaoBolsaQuery posicaoBolsaQuery = new PosicaoBolsaQuery("P");
                posicaoBolsaQuery.InnerJoin(ativoBolsaQuery).On(ativoBolsaQuery.CdAtivoBolsa == posicaoBolsaQuery.CdAtivoBolsa);
                posicaoBolsaQuery.Where(posicaoBolsaQuery.TipoMercado.In(TipoMercadoBolsa.MercadoVista, TipoMercadoBolsa.Imobiliario) &
                                        posicaoBolsaQuery.IdCliente.Equal(idCliente) &
                                        ativoBolsaQuery.Participacao.IsNotNull() & ativoBolsaQuery.Participacao.Equal("S"));

                posicaoBolsaCollection.Load(posicaoBolsaQuery);
            }

            foreach (PosicaoBolsa posicaoBolsa in posicaoBolsaCollection)
            {
                string cdAtivoBolsa = posicaoBolsa.CdAtivoBolsa;

                AtivoBolsa ativoBolsa = new AtivoBolsa();
                ativoBolsa.LoadByPrimaryKey(cdAtivoBolsa);

                Emissor emissor = new Emissor();
                emissor.LoadByPrimaryKey(ativoBolsa.IdEmissor.Value);

                FatorCotacaoBolsa fatorCotacaoBolsa = new FatorCotacaoBolsa();
                fatorCotacaoBolsa.BuscaFatorCotacaoBolsa(cdAtivoBolsa, data);

                decimal valorFinanceiro = Math.Abs(posicaoBolsa.ValorMercado.Value);

                string cnpjEmissor = emissor.Cnpj;
                if (cnpjEmissor == null)
                {
                    cnpjEmissor = "0";
                }

                Participacao participacoes = new Participacao();
                participacoes.cnpjpart = cnpjEmissor.PadLeft(14, '0');
                participacoes.valorfinanceiro = valorFinanceiro.ToString("N2").Replace(".", "").Replace(",", ".");
                lista.Add(participacoes);
            }


            PosicaoEmprestimoBolsaCollection posicaoEmprestimoBolsaCollection = new PosicaoEmprestimoBolsaCollection();
            if (historico)
            {

                PosicaoEmprestimoBolsaHistoricoQuery posicaoEmprestimoHistoricoBolsaQuery = new PosicaoEmprestimoBolsaHistoricoQuery("P");

                posicaoEmprestimoHistoricoBolsaQuery.InnerJoin(ativoBolsaQuery).On(ativoBolsaQuery.CdAtivoBolsa == posicaoEmprestimoHistoricoBolsaQuery.CdAtivoBolsa);
                posicaoEmprestimoHistoricoBolsaQuery.Where(posicaoEmprestimoHistoricoBolsaQuery.IdCliente.Equal(idCliente) &
                                                  posicaoEmprestimoHistoricoBolsaQuery.DataHistorico.Equal(data) &
                                                  posicaoEmprestimoHistoricoBolsaQuery.Quantidade.GreaterThan(0) &
                                                  ativoBolsaQuery.Participacao.IsNotNull() & ativoBolsaQuery.Participacao.Equal("S"));


                PosicaoEmprestimoBolsaHistoricoCollection posicaoEmprestimoBolsaHistoricoCollection = new PosicaoEmprestimoBolsaHistoricoCollection();
                posicaoEmprestimoBolsaHistoricoCollection.Load(posicaoEmprestimoHistoricoBolsaQuery);


                PosicaoEmprestimoBolsaCollection posicaoEmprestimoBolsaCollectionAux = new PosicaoEmprestimoBolsaCollection(posicaoEmprestimoBolsaHistoricoCollection);
                posicaoEmprestimoBolsaCollection.Combine(posicaoEmprestimoBolsaCollectionAux);
            }
            else
            {

                PosicaoEmprestimoBolsaQuery posicaoEmprestimoBolsaQuery = new PosicaoEmprestimoBolsaQuery("P");

                posicaoEmprestimoBolsaQuery.InnerJoin(ativoBolsaQuery).On(ativoBolsaQuery.CdAtivoBolsa == posicaoEmprestimoBolsaQuery.CdAtivoBolsa);
                posicaoEmprestimoBolsaQuery.Where(posicaoEmprestimoBolsaQuery.IdCliente.Equal(idCliente) &
                                                  posicaoEmprestimoBolsaQuery.Quantidade.GreaterThan(0) &
                                                  ativoBolsaQuery.Participacao.IsNotNull() & ativoBolsaQuery.Participacao.Equal("S"));

                posicaoEmprestimoBolsaCollection.Load(posicaoEmprestimoBolsaQuery);
            }

            foreach (PosicaoEmprestimoBolsa posicaoEmprestimoBolsa in posicaoEmprestimoBolsaCollection)
            {
                byte pontaEmprestimo = posicaoEmprestimoBolsa.PontaEmprestimo.Value;
                string cdAtivoBolsa = posicaoEmprestimoBolsa.CdAtivoBolsa;
                DateTime dataVencimento = posicaoEmprestimoBolsa.DataVencimento.Value;
                decimal taxaOperacao = posicaoEmprestimoBolsa.TaxaOperacao.Value;

                AtivoBolsa ativoBolsa = new AtivoBolsa();
                ativoBolsa.LoadByPrimaryKey(cdAtivoBolsa);

                Emissor emissor = new Emissor();
                emissor.LoadByPrimaryKey(ativoBolsa.IdEmissor.Value);

                decimal quantidade = posicaoEmprestimoBolsa.Quantidade.Value;

                string classeOperacao = pontaEmprestimo == (byte)PontaEmprestimoBolsa.Doador ? "D" : "T";

                decimal quantidadeGarantia = 0, quantidadeDisponivel = 0;
                if (pontaEmprestimo == (byte)PontaEmprestimoBolsa.Doador)
                {
                    quantidadeDisponivel = 0;
                    quantidadeGarantia = quantidade;
                }
                else
                {
                    quantidadeDisponivel = quantidade;
                    quantidadeGarantia = 0;
                }

                string cnpjEmissor = emissor.Cnpj;
                if (cnpjEmissor == null)
                {
                    cnpjEmissor = "0";
                }

                Participacao participacoes = new Participacao();
                participacoes.cnpjpart = cnpjEmissor.PadLeft(14, '0');
                participacoes.valorfinanceiro = Utilitario.Truncate(quantidadeDisponivel * posicaoEmprestimoBolsa.PUMercado.Value, 2).ToString("N2").Replace(".", "").Replace(",", ".");
                lista.Add(participacoes);
            }

            return lista;
        }	

        public List<OpcoesAcoes> RetornaOpcoes(int idCliente, DateTime data, bool historico)
        {
            List<OpcoesAcoes> lista = new List<OpcoesAcoes>();

            PosicaoBolsaCollection posicaoBolsaCollection = new PosicaoBolsaCollection();
            if (historico)
            {
                PosicaoBolsaHistoricoCollection posicaoBolsaHistoricoCollection = new PosicaoBolsaHistoricoCollection();
                posicaoBolsaHistoricoCollection.Query.Where(posicaoBolsaHistoricoCollection.Query.TipoMercado.In(TipoMercadoBolsa.OpcaoCompra, TipoMercadoBolsa.OpcaoVenda),
                                                            posicaoBolsaHistoricoCollection.Query.IdCliente.Equal(idCliente),
                                                            posicaoBolsaHistoricoCollection.Query.DataHistorico.Equal(data));
                posicaoBolsaHistoricoCollection.Query.Load();

                PosicaoBolsaCollection posicaoBolsaCollectionAux = new PosicaoBolsaCollection(posicaoBolsaHistoricoCollection);
                posicaoBolsaCollection.Combine(posicaoBolsaCollectionAux);
            }
            else
            {

                posicaoBolsaCollection.Query.Where(posicaoBolsaCollection.Query.TipoMercado.In(TipoMercadoBolsa.OpcaoCompra, TipoMercadoBolsa.OpcaoVenda),
                                                   posicaoBolsaCollection.Query.IdCliente.Equal(idCliente));
                posicaoBolsaCollection.Query.Load();
            }

            foreach (PosicaoBolsa posicaoBolsa in posicaoBolsaCollection)
            {
                string cdAtivoBolsa = posicaoBolsa.CdAtivoBolsa;

                AtivoBolsa ativoBolsa = new AtivoBolsa();
                ativoBolsa.LoadByPrimaryKey(cdAtivoBolsa);

                FatorCotacaoBolsa fatorCotacaoBolsa = new FatorCotacaoBolsa();
                fatorCotacaoBolsa.BuscaFatorCotacaoBolsa(cdAtivoBolsa, data);

                decimal quantidade = Math.Abs(posicaoBolsa.Quantidade.Value);
                decimal valorFinanceiro = Math.Abs(posicaoBolsa.ValorMercado.Value);
                decimal premio = Math.Abs(posicaoBolsa.ValorCustoLiquido.Value);
                decimal puposicao = posicaoBolsa.PUMercado.Value;
                decimal precoexercicio = ativoBolsa.PUExercicio.HasValue ? ativoBolsa.PUExercicio.Value : 0;

                string classeOperacao = posicaoBolsa.Quantidade.Value > 0 ? "C" : "V";

                #region conversão
                if (this.multiMoeda)
                {
                    //***Ajusta para trabalhar com multi-moeda   
                    int idMoedaAtivo = ativoBolsa.IdMoeda.HasValue ? (int)ativoBolsa.IdMoeda.Value : 0;

                    ConversaoMoeda conversaoMoeda = new ConversaoMoeda();
                    decimal fatorMoeda = 1;
                    fatorMoeda = conversaoMoeda.RetornaFatorConversao(this.idMoedaCarteira, idMoedaAtivo, data);

                    valorFinanceiro = Math.Round(valorFinanceiro * fatorMoeda, 2);
                    puposicao = Math.Round(puposicao * fatorMoeda, 8);
                    premio = Math.Round(premio * fatorMoeda, 2);
                    precoexercicio = Math.Round(precoexercicio * fatorMoeda, 2);
                }
                #endregion



                OpcoesAcoes opcoesAcoes = new OpcoesAcoes();
                opcoesAcoes.ativobase = ativoBolsa.CdAtivoBolsaObjeto.ToUpper();
                opcoesAcoes.classeoperacao = classeOperacao;
                opcoesAcoes.codativo = cdAtivoBolsa.ToUpper();
                opcoesAcoes.cusip = "";
                opcoesAcoes.dtvencimento = ativoBolsa.DataVencimento.HasValue ? ativoBolsa.DataVencimento.Value.ToString("yyyyMMdd") : "";
                opcoesAcoes.hedge = "N"; //Sempre não faz parte de hedge
                opcoesAcoes.isin = ativoBolsa.CodigoIsin.ToUpper();
                opcoesAcoes.percprovcred = "0.00"; //Forçado % Provisão de Credito = 0
                opcoesAcoes.precoexercicio = precoexercicio.ToString("N8").Replace(".", "").Replace(",", ".");
                opcoesAcoes.premio = premio.ToString("N2").Replace(".", "").Replace(",", ".");
                opcoesAcoes.puposicao = puposicao.ToString("N8").Replace(".", "").Replace(",", ".");
                opcoesAcoes.qtdisponivel = quantidade.ToString("N9").Replace(".", "").Replace(",", ".");
                opcoesAcoes.tpconta = "1"; //Sempre Conta movimento
                opcoesAcoes.tphedge = "0"; //Sempre não faz parte de hedge
                opcoesAcoes.tributos = "0.00";
                opcoesAcoes.valorfinanceiro = valorFinanceiro.ToString("N2").Replace(".", "").Replace(",", ".");

                lista.Add(opcoesAcoes);
            }

            return lista;
        }

        public List<Futuros> RetornaFuturos(int idCliente, DateTime data, bool historico)
        {
            List<Futuros> lista = new List<Futuros>();

            PosicaoBMFCollection posicaoBMFCollection = new PosicaoBMFCollection();
            if (historico)
            {
                PosicaoBMFHistoricoCollection posicaoBMFHistoricoCollection = new PosicaoBMFHistoricoCollection();
                posicaoBMFHistoricoCollection.Query.Where(posicaoBMFHistoricoCollection.Query.TipoMercado.In(TipoMercadoBMF.Futuro),
                                                          posicaoBMFHistoricoCollection.Query.IdCliente.Equal(idCliente),
                                                          posicaoBMFHistoricoCollection.Query.DataHistorico.Equal(data));
                posicaoBMFHistoricoCollection.Query.Load();

                PosicaoBMFCollection posicaoBMFCollectionAux = new PosicaoBMFCollection(posicaoBMFHistoricoCollection);
                posicaoBMFCollection.Combine(posicaoBMFCollectionAux);
            }
            else
            {

                posicaoBMFCollection.Query.Where(posicaoBMFCollection.Query.TipoMercado.In(TipoMercadoBMF.Futuro),
                                                 posicaoBMFCollection.Query.IdCliente.Equal(idCliente));
                posicaoBMFCollection.Query.Load();
            }

            foreach (PosicaoBMF posicaoBMF in posicaoBMFCollection)
            {
                string cdAtivoBMF = posicaoBMF.CdAtivoBMF;
                string serie = posicaoBMF.Serie;
                int idAgente = posicaoBMF.IdAgente.Value;

                AtivoBMF ativoBMF = new AtivoBMF();
                ativoBMF.LoadByPrimaryKey(cdAtivoBMF, serie);

                AgenteMercado agenteMercado = new AgenteMercado();
                agenteMercado.LoadByPrimaryKey(idAgente);

                decimal valorAjuste = posicaoBMF.AjusteDiario.Value;
                decimal quantidade = Math.Abs(posicaoBMF.Quantidade.Value);
                decimal valorFinanceiro = Math.Abs(posicaoBMF.ValorMercado.Value);

                string classeOperacao = posicaoBMF.Quantidade.Value > 0 ? "C" : "V";

                #region conversão
                if (this.multiMoeda)
                {
                    //***Ajusta para trabalhar com multi-moeda   
                    int idMoedaAtivo = ativoBMF.IdMoeda.HasValue ? ativoBMF.IdMoeda.Value : 0;

                    ConversaoMoeda conversaoMoeda = new ConversaoMoeda();
                    decimal fatorMoeda = 1;
                    fatorMoeda = conversaoMoeda.RetornaFatorConversao(this.idMoedaCarteira, idMoedaAtivo, data);

                    valorFinanceiro = Math.Round(valorFinanceiro * fatorMoeda, 2);
                    valorAjuste = Math.Round(valorAjuste * fatorMoeda, 2);
                }
                #endregion


                Futuros futuros = new Futuros();
                futuros.ativo = cdAtivoBMF.ToUpper();
                futuros.classeoperacao = classeOperacao;

                string cnpjCorretora = !String.IsNullOrEmpty(agenteMercado.Cnpj) ? Utilitario.RemoveCaracteresEspeciais(agenteMercado.Cnpj.Trim()) : "";

                futuros.cnpjcorretora = cnpjCorretora.PadLeft(14, '0');
                futuros.dtvencimento = ativoBMF.DataVencimento.HasValue ? ativoBMF.DataVencimento.Value.ToString("yyyyMMdd") : "";
                futuros.hedge = "N"; //Sempre não faz parte de hedge
                futuros.isin = ativoBMF.CodigoIsin.ToUpper();
                futuros.quantidade = quantidade.ToString("N9").Replace(".", "").Replace(",", ".");
                futuros.serie = serie;
                futuros.tphedge = "0"; //Sempre não faz parte de hedge
                futuros.tributos = "0.00";
                futuros.vlajuste = valorAjuste.ToString("N2").Replace(".", "").Replace(",", ".");
                futuros.vltotalpos = valorFinanceiro.ToString("N2").Replace(".", "").Replace(",", ".");

                lista.Add(futuros);
            }

            return lista;
        }

        public List<Swap> RetornaSwap(int idCliente, DateTime data, bool historico)
        {
            List<Swap> lista = new List<Swap>();

            PosicaoSwapCollection posicaoSwapCollection = new PosicaoSwapCollection();
            if (historico)
            {
                PosicaoSwapHistoricoCollection posicaoSwapHistoricoCollection = new PosicaoSwapHistoricoCollection();
                posicaoSwapHistoricoCollection.Query.Where(
                                                          posicaoSwapHistoricoCollection.Query.IdCliente.Equal(idCliente),
                                                          posicaoSwapHistoricoCollection.Query.DataHistorico.Equal(data));
                posicaoSwapHistoricoCollection.Query.Load();

                PosicaoSwapCollection posicaoSwapCollectionAux = new PosicaoSwapCollection(posicaoSwapHistoricoCollection);
                posicaoSwapCollection.Combine(posicaoSwapCollectionAux);
            }
            else
            {

                posicaoSwapCollection.Query.Where(
                                                 posicaoSwapCollection.Query.IdCliente.Equal(idCliente));
                posicaoSwapCollection.Query.Load();
            }

            foreach (PosicaoSwap posicaoSwap in posicaoSwapCollection)
            {
                Swap swap = new Swap();
                swap.cetipbmf = posicaoSwap.TipoRegistro == (byte)Financial.Swap.Enums.TipoRegistroSwap.BMF ? "B" : "C";
                swap.isin = "";
                swap.dtoperacao = posicaoSwap.DataEmissao.Value.ToString("yyyyMMdd");
                DateTime dataRegistro = posicaoSwap.DataEmissao.Value;
                swap.isin = "";
                swap.cnpjcontraparte = "";

                if (posicaoSwap.IdOperacao.HasValue)
                {
                    OperacaoSwap operacaoSwap = new OperacaoSwap();
                    if (operacaoSwap.LoadByPrimaryKey(posicaoSwap.IdOperacao.Value))
                    {
                        dataRegistro = operacaoSwap.DataRegistro.Value;
                        if (operacaoSwap.CodigoIsin != null)
                        {
                            swap.isin = operacaoSwap.CodigoIsin;
                        }
                        if (operacaoSwap.CpfcnpjContraParte != null)
                        {
                            swap.cnpjcontraparte = operacaoSwap.CpfcnpjContraParte;
                        }
                    }
                }

                swap.dtregistro = dataRegistro.ToString("yyyyMMdd");
                swap.dtvencimento = posicaoSwap.DataVencimento.Value.ToString("yyyyMMdd");

                //Garantia - Identificar se há garantias associadas a operação. Preencher com: 
                //“1” – Ambas as partes
                //“2” – Nenhuma
                //“3” – Se houver garantia na posição ativa da carteira ou do fundo
                //“4” – Se houver garantia na posição passiva da carteira ou do fundo 
                swap.garantia = posicaoSwap.ComGarantia == "S" ? "1" : "2";

                swap.vlnotional = posicaoSwap.ValorBase.Value.ToString("N2").Replace(".", "").Replace(",", ".");
                swap.tributos = posicaoSwap.ValorIR.Value.ToString("N2").Replace(".", "").Replace(",", ".");
                swap.vlmercadoativo = posicaoSwap.ValorParte.Value.ToString("N2").Replace(".", "").Replace(",", ".");
                swap.taxaativo = posicaoSwap.TaxaJuros.Value.ToString("N4").Replace(".", "").Replace(",", ".");
                swap.indexadorativo = posicaoSwap.IdIndice.HasValue ? this.RetornaIndice(posicaoSwap.IdIndice.Value).ToString() : "PRE"; //Sem indice associado, assume pre-fixado;
                if (String.IsNullOrEmpty(swap.indexadorativo)) //RetornaIndice pode ter retornado branco
                {
                    swap.indexadorativo = "PRE";
                }
                swap.percindexativo = posicaoSwap.Percentual.Value.ToString("N2").Replace(".", "").Replace(",", ".");
                swap.vlmercadopassivo = posicaoSwap.ValorContraParte.Value.ToString("N2").Replace(".", "").Replace(",", ".");
                swap.taxapassivo = posicaoSwap.TaxaJurosContraParte.Value.ToString("N4").Replace(".", "").Replace(",", ".");
                swap.indexadorpassivo = posicaoSwap.IdIndiceContraParte.HasValue ? this.RetornaIndice(posicaoSwap.IdIndiceContraParte.Value).ToString() : "PRE"; //Sem indice associado, assume pre-fixado;
                if (String.IsNullOrEmpty(swap.indexadorpassivo)) //RetornaIndice pode ter retornado branco
                {
                    swap.indexadorpassivo = "PRE";
                }

                swap.percindexpassivo = posicaoSwap.PercentualContraParte.Value.ToString("N2").Replace(".", "").Replace(",", ".");
                swap.hedge = "N";
                swap.tphedge = "0";
                swap.idinternoativo = "";

                lista.Add(swap);

            }

            return lista;
        }

        public List<TermoRv> RetornaTermoRV(int idCliente, DateTime data, bool historico)
        {
            List<TermoRv> lista = new List<TermoRv>();

            PosicaoTermoBolsaCollection posicaoTermoBolsaCollection = new PosicaoTermoBolsaCollection();
            if (historico)
            {
                PosicaoTermoBolsaHistoricoCollection posicaoTermoBolsaHistoricoCollection = new PosicaoTermoBolsaHistoricoCollection();
                posicaoTermoBolsaHistoricoCollection.Query.Where(posicaoTermoBolsaHistoricoCollection.Query.IdCliente.Equal(idCliente),
                                                                 posicaoTermoBolsaHistoricoCollection.Query.DataHistorico.Equal(data),
                                                                 posicaoTermoBolsaHistoricoCollection.Query.Quantidade.NotEqual(0));
                posicaoTermoBolsaHistoricoCollection.Query.Load();

                PosicaoTermoBolsaCollection posicaoTermoBolsaCollectionAux = new PosicaoTermoBolsaCollection(posicaoTermoBolsaHistoricoCollection);
                posicaoTermoBolsaCollection.Combine(posicaoTermoBolsaCollectionAux);
            }
            else
            {

                posicaoTermoBolsaCollection.Query.Where(posicaoTermoBolsaCollection.Query.IdCliente.Equal(idCliente),
                                                        posicaoTermoBolsaCollection.Query.Quantidade.NotEqual(0));
                posicaoTermoBolsaCollection.Query.Load();
            }

            foreach (PosicaoTermoBolsa posicaoTermoBolsa in posicaoTermoBolsaCollection)
            {
                string cdAtivoBolsa = posicaoTermoBolsa.CdAtivoBolsa;
                DateTime dataVencimento = posicaoTermoBolsa.DataVencimento.Value;
                DateTime dataOperacao = posicaoTermoBolsa.DataOperacao.Value;

                string cdAtivoBolsaTermo = AtivoBolsa.RetornaCdAtivoBolsaAcao(cdAtivoBolsa);

                AtivoBolsa ativoBolsa = new AtivoBolsa();
                ativoBolsa.LoadByPrimaryKey(cdAtivoBolsa);

                decimal quantidade = Math.Abs(posicaoTermoBolsa.Quantidade.Value);
                decimal valorFinanceiro = Math.Abs(posicaoTermoBolsa.ValorMercado.Value);
                decimal puTermo = posicaoTermoBolsa.PUTermo.Value;
                decimal puPosicao = posicaoTermoBolsa.PUMercado.Value;


                string classeOperacao = posicaoTermoBolsa.Quantidade.Value > 0 ? "C" : "V";

                #region conversão
                if (this.multiMoeda)
                {
                    //***Ajusta para trabalhar com multi-moeda   
                    int idMoedaAtivo = ativoBolsa.IdMoeda.HasValue ? (int)ativoBolsa.IdMoeda.Value : 0;

                    ConversaoMoeda conversaoMoeda = new ConversaoMoeda();
                    decimal fatorMoeda = 1;
                    fatorMoeda = conversaoMoeda.RetornaFatorConversao(this.idMoedaCarteira, idMoedaAtivo, data);

                    valorFinanceiro = Math.Round(valorFinanceiro * fatorMoeda, 2);
                    puPosicao = Math.Round(puPosicao * fatorMoeda, 8);
                    puTermo = Math.Round(puTermo * fatorMoeda, 2);
                }
                #endregion


                TermoRv termoRv = new TermoRv();
                termoRv.ativo = cdAtivoBolsaTermo;
                termoRv.classeoperacao = classeOperacao;
                termoRv.cusip = "";
                termoRv.dtoperacao = dataOperacao.ToString("yyyyMMdd");
                termoRv.dtvencimento = dataVencimento.ToString("yyyyMMdd");
                termoRv.garantia = "N";
                termoRv.hedge = "N"; //Sempre não faz parte de hedge
                termoRv.isin = ativoBolsa.CodigoIsin.ToUpper();
                termoRv.puposicao = puPosicao.ToString("N8").Replace(".", "").Replace(",", ".");
                termoRv.puvenc = puTermo.ToString("N8").Replace(".", "").Replace(",", ".");
                termoRv.quantidade = quantidade.ToString("N9").Replace(".", "").Replace(",", ".");
                termoRv.tpconta = "1"; //Sempre Conta movimento
                termoRv.tphedge = "0"; //Sempre não faz parte de hedge
                termoRv.tributos = "0.00";
                termoRv.valorfinanceiro = valorFinanceiro.ToString("N2").Replace(".", "").Replace(",", ".");

                lista.Add(termoRv);
            }

            return lista;
        }

        public List<Cotas> RetornaCotas(int idCliente, DateTime data, bool historico)
        {
            List<Cotas> lista = new List<Cotas>();

            PosicaoFundoCollection posicaoFundoCollection = new PosicaoFundoCollection();
            PosicaoFundoHistoricoCollection posicaoFundoHistoricoCollection = new PosicaoFundoHistoricoCollection();
            posicaoFundoHistoricoCollection.Query.Select(posicaoFundoHistoricoCollection.Query.IdCarteira,
                                                         posicaoFundoHistoricoCollection.Query.Quantidade.Sum(),
                                                         posicaoFundoHistoricoCollection.Query.QuantidadeBloqueada.Sum(),
                                                         posicaoFundoHistoricoCollection.Query.CotaDia.Avg());
            posicaoFundoHistoricoCollection.Query.Where(posicaoFundoHistoricoCollection.Query.IdCliente.Equal(idCliente),
                                                        posicaoFundoHistoricoCollection.Query.DataHistorico.Equal(data),
                                                        posicaoFundoHistoricoCollection.Query.Quantidade.NotEqual(0));
            posicaoFundoHistoricoCollection.Query.GroupBy(posicaoFundoHistoricoCollection.Query.IdCarteira);
            posicaoFundoHistoricoCollection.Query.Load();

            PosicaoFundoCollection posicaoFundoCollectionAux = new PosicaoFundoCollection(posicaoFundoHistoricoCollection);
            posicaoFundoCollection.Combine(posicaoFundoCollectionAux);

            foreach (PosicaoFundo posicaoFundo in posicaoFundoCollection)
            {
                int idFundo = posicaoFundo.IdCarteira.Value;

                Pessoa pessoa = new Pessoa();
                pessoa.Query.Select(pessoa.Query.Cpfcnpj);
                pessoa.Query.Where(pessoa.Query.IdPessoa.Equal(idFundo));
                pessoa.Query.Load();

                string cnpj = !String.IsNullOrEmpty(pessoa.Cpfcnpj) ? Utilitario.RemoveCaracteresEspeciais(pessoa.Cpfcnpj).PadLeft(14, '0') : "0".PadLeft(14, '0');

                Financial.Fundo.Carteira carteira = new Financial.Fundo.Carteira();
                carteira.Query.Select(carteira.Query.CodigoIsin);
                carteira.Query.Where(carteira.Query.IdCarteira.Equal(idFundo));
                carteira.Query.Load();

                string codigoIsin = !String.IsNullOrEmpty(carteira.CodigoIsin) ? carteira.CodigoIsin.ToUpper() : "";

                decimal quantidadeDisponivel = (posicaoFundo.Quantidade.Value - posicaoFundo.QuantidadeBloqueada.Value) > 0 ? (posicaoFundo.Quantidade.Value - posicaoFundo.QuantidadeBloqueada.Value) : 0;

                decimal quantidadeGarantia = posicaoFundo.QuantidadeBloqueada.Value > 0 ? posicaoFundo.QuantidadeBloqueada.Value : 0;

                Cotas cotas = new Cotas();
                cotas.cnpjfundo = cnpj;

                if (!String.IsNullOrEmpty(carteira.CodigoIsin))
                {
                    cotas.isin = carteira.CodigoIsin;
                }

                decimal cotaDia = posicaoFundo.CotaDia.Value;

                if (this.MultiMoeda)
                {
                    Cliente clienteFundo = new Cliente();
                    clienteFundo.LoadByPrimaryKey(idFundo);
                    int idMoedaAtivo = clienteFundo.IdMoeda.HasValue ? (int)clienteFundo.IdMoeda.Value : 0;

                    ConversaoMoeda conversaoMoeda = new ConversaoMoeda();
                    decimal fatorMoeda = 1;
                    fatorMoeda = conversaoMoeda.RetornaFatorConversao(this.idMoedaCarteira, idMoedaAtivo, data);
                    cotaDia = Math.Round(cotaDia * fatorMoeda, 2);
                }

                cotas.nivelrsc = "BB"; //Sempre “BB” – Baixo
                cotas.puposicao = cotaDia.ToString("N8").Replace(".", "").Replace(",", ".");
                cotas.qtdisponivel = quantidadeDisponivel.ToString("N9").Replace(".", "").Replace(",", ".");
                cotas.qtgarantia = quantidadeGarantia.ToString("N9").Replace(".", "").Replace(",", ".");
                cotas.tributos = "0.00";

                lista.Add(cotas);
            }

            return lista;
        }

        public Caixa RetornaCaixa(int idCliente, DateTime data)
        {

            decimal valor = 0;
            ConversaoMoeda conversaoMoeda = new ConversaoMoeda();

            if (this.MultiMoeda)
            {

                SaldoCaixaCollection saldoCaixaCollection = new SaldoCaixaCollection();
                saldoCaixaCollection = new SaldoCaixaCollection();
                saldoCaixaCollection.Query.Select(saldoCaixaCollection.Query.SaldoFechamento.Sum(),
                                                  saldoCaixaCollection.Query.IdConta);
                saldoCaixaCollection.Query.Where(saldoCaixaCollection.Query.IdCliente.Equal(idCliente),
                                                 saldoCaixaCollection.Query.Data.Equal(data),
                                                 saldoCaixaCollection.Query.SaldoFechamento.IsNotNull());
                saldoCaixaCollection.Query.GroupBy(saldoCaixaCollection.Query.IdConta);
                saldoCaixaCollection.Query.Load();

                foreach (SaldoCaixa saldoCaixa in saldoCaixaCollection)
                {
                    Investidor.ContaCorrente conta = new Financial.Investidor.ContaCorrente();
                    conta.LoadByPrimaryKey(saldoCaixa.IdConta.Value);
                    int idMoedaCaixa = conta.IdMoeda.HasValue ? conta.IdMoeda.Value : 0;

                    decimal valorCaixa = saldoCaixa.SaldoFechamento.Value;

                    //***Ajusta para trabalhar com multi-moeda no fluxo
                    decimal fatorMoeda = conversaoMoeda.RetornaFatorConversao(this.idMoedaCarteira, idMoedaCaixa, data);
                    valorCaixa = Math.Round(valorCaixa * fatorMoeda, 2);
                    //**************************************************

                    valor += valorCaixa;
                }
            }
            else
            {
                SaldoCaixa saldoCaixa = new SaldoCaixa();
                saldoCaixa.BuscaSaldoCaixa(idCliente, data);

                if (saldoCaixa.SaldoFechamento.HasValue)
                {
                    valor = saldoCaixa.SaldoFechamento.Value;
                }
            }

            Financial.Investidor.ContaCorrente contaCorrente = new Financial.Investidor.ContaCorrente();
            int idConta = contaCorrente.RetornaContaDefault(idCliente);

            contaCorrente.LoadByPrimaryKey(idConta);

            string isinInstituicao = contaCorrente.DescricaoCodigo != null ? contaCorrente.DescricaoCodigo : "";

            Caixa caixa = new Caixa();
            caixa.isininstituicao = isinInstituicao.ToUpper().Trim();  //"************"; //Não está sendo tratado
            caixa.nivelrsc = "BB"; //Sempre “BB” – Baixo
            caixa.saldo = valor.ToString("N2").Replace(".", "").Replace(",", ".");
            caixa.tpconta = "D"; //D=depósito P=poupança I=investimeo

            return caixa;
        }

        public List<Despesas> RetornaDespesas(int idCliente, DateTime data)
        {
            List<Despesas> lista = new List<Despesas>();

            DateTime dataPrimeiroDiaMes = Calendario.RetornaPrimeiroDiaCorridoMes(data, 0);

            LiquidacaoCollection liquidacao = new LiquidacaoCollection();
            liquidacao.Query.Select(liquidacao.Query.Valor);
            liquidacao.Query.Where(liquidacao.Query.IdCliente.Equal(idCliente),
                                   liquidacao.Query.DataVencimento.GreaterThanOrEqual(dataPrimeiroDiaMes),
                                   liquidacao.Query.DataVencimento.LessThanOrEqual(data),
                                   liquidacao.Query.Origem.Equal((int)OrigemLancamentoLiquidacao.Provisao.PagtoTaxaAdministracao));
            liquidacao.Query.Load();


            decimal valorPagoAdministracao = 0;

            foreach (Liquidacao liq in liquidacao)
            {
                decimal valorLiq = liq.Valor.HasValue ? liq.Valor.Value : 0;

                #region conversão
                if (this.multiMoeda)
                {
                    int idConta = liq.IdConta.HasValue ? liq.IdConta.Value : 0;
                    DateTime dataVencimento = liq.DataVencimento.Value;

                    Investidor.ContaCorrente conta = new Financial.Investidor.ContaCorrente();
                    conta.LoadByPrimaryKey(idConta);
                    int idMoedaCaixa = conta.IdMoeda.HasValue ? conta.IdMoeda.Value : 0;

                    //***Ajusta para trabalhar com multi-moeda no fluxo
                    decimal fatorMoeda = 1;
                    ConversaoMoeda conversaoMoeda = new ConversaoMoeda();
                    fatorMoeda = conversaoMoeda.RetornaFatorConversao(this.idMoedaCarteira, idMoedaCaixa, dataVencimento);
                    valorLiq = Math.Round(valorLiq * fatorMoeda, 2);
                    //**************************************************
                }
                #endregion

                valorPagoAdministracao += valorLiq;

            }

            valorPagoAdministracao = Math.Abs(valorPagoAdministracao);

            liquidacao = new LiquidacaoCollection();
            liquidacao.Query.Select(liquidacao.Query.Valor);
            liquidacao.Query.Where(liquidacao.Query.IdCliente.Equal(idCliente),
                                   liquidacao.Query.DataVencimento.GreaterThanOrEqual(dataPrimeiroDiaMes),
                                   liquidacao.Query.DataVencimento.LessThanOrEqual(data),
                                   liquidacao.Query.Origem.Equal((int)OrigemLancamentoLiquidacao.Provisao.PagtoTaxaPerformance));
            liquidacao.Query.Load();

            decimal valorPagoPfee = 0;

            foreach (Liquidacao liq in liquidacao)
            {
                decimal valorLiq = liq.Valor.HasValue ? liq.Valor.Value : 0;

                #region conversão
                if (this.multiMoeda)
                {
                    int idConta = liq.IdConta.HasValue ? liq.IdConta.Value : 0;
                    DateTime dataVencimento = liq.DataVencimento.Value;

                    Investidor.ContaCorrente conta = new Financial.Investidor.ContaCorrente();
                    conta.LoadByPrimaryKey(idConta);
                    int idMoedaCaixa = conta.IdMoeda.HasValue ? conta.IdMoeda.Value : 0;

                    //***Ajusta para trabalhar com multi-moeda no fluxo
                    decimal fatorMoeda = 1;
                    ConversaoMoeda conversaoMoeda = new ConversaoMoeda();
                    fatorMoeda = conversaoMoeda.RetornaFatorConversao(this.idMoedaCarteira, idMoedaCaixa, dataVencimento);
                    valorLiq = Math.Round(valorLiq * fatorMoeda, 2);
                    //**************************************************
                }
                #endregion

                valorPagoPfee += valorLiq;
            }

            valorPagoPfee = Math.Abs(valorPagoPfee);

            TabelaTaxaAdministracaoCollection tabelaTaxaAdministracaoCollection = new TabelaTaxaAdministracaoCollection();
            tabelaTaxaAdministracaoCollection.Query.Select(tabelaTaxaAdministracaoCollection.Query.Taxa);
            tabelaTaxaAdministracaoCollection.Query.Where(tabelaTaxaAdministracaoCollection.Query.IdCarteira.Equal(idCliente) &
                                                          tabelaTaxaAdministracaoCollection.Query.IdCadastro.Equal((byte)TipoCadastroAdministracao.TaxaAdministracao) &
                                                          tabelaTaxaAdministracaoCollection.Query.DataReferencia.LessThanOrEqual(data) &
                                                          tabelaTaxaAdministracaoCollection.Query.Taxa.IsNotNull() &
                                                          (tabelaTaxaAdministracaoCollection.Query.DataFim.IsNull() | tabelaTaxaAdministracaoCollection.Query.DataFim.LessThan(data)));
            tabelaTaxaAdministracaoCollection.Query.OrderBy(tabelaTaxaAdministracaoCollection.Query.DataReferencia.Descending);
            tabelaTaxaAdministracaoCollection.Query.Load();

            decimal taxaAdministracao = 0;
            if (tabelaTaxaAdministracaoCollection.Count > 0)
            {
                taxaAdministracao = tabelaTaxaAdministracaoCollection[0].Taxa.Value;
            }

            TabelaTaxaPerformanceCollection tabelaTaxaPerformanceCollection = new TabelaTaxaPerformanceCollection();
            tabelaTaxaPerformanceCollection.Query.Select(tabelaTaxaPerformanceCollection.Query.IdIndice,
                                                         tabelaTaxaPerformanceCollection.Query.PercentualIndice,
                                                         tabelaTaxaPerformanceCollection.Query.TaxaPerformance);
            tabelaTaxaPerformanceCollection.Query.Where(tabelaTaxaPerformanceCollection.Query.IdCarteira.Equal(idCliente) &
                                                        tabelaTaxaPerformanceCollection.Query.DataReferencia.LessThanOrEqual(data) &
                                                        (tabelaTaxaPerformanceCollection.Query.DataFim.IsNull() | tabelaTaxaPerformanceCollection.Query.DataFim.LessThan(data)));
            tabelaTaxaPerformanceCollection.Query.OrderBy(tabelaTaxaPerformanceCollection.Query.DataReferencia.Descending);
            tabelaTaxaPerformanceCollection.Query.Load();

            string temPfee = "N";
            decimal taxaPerformance = 0;
            string indexador = "";
            decimal percentualIndice = 0;
            if (tabelaTaxaPerformanceCollection.Count > 0)
            {
                temPfee = "S";
                taxaPerformance = tabelaTaxaPerformanceCollection[0].TaxaPerformance.Value;

                indexador = this.RetornaIndice(tabelaTaxaPerformanceCollection[0].IdIndice.Value);
                if (indexador.Length >= 3)
                {
                    indexador = indexador.Substring(0, 3);
                }
                percentualIndice = tabelaTaxaPerformanceCollection[0].PercentualIndice.Value;
            }

            Despesas despesas = new Despesas();
            despesas.indexador = indexador;
            despesas.outtax = "0.00";
            despesas.percindex = percentualIndice.ToString("N8").Replace(".", "").Replace(",", ".");
            despesas.perctaxaadm = taxaAdministracao.ToString("N8").Replace(".", "").Replace(",", ".");
            despesas.perctxperf = taxaPerformance.ToString("N8").Replace(".", "").Replace(",", ".");
            despesas.tributos = "0.00";
            despesas.txadm = valorPagoAdministracao.ToString("N2").Replace(".", "").Replace(",", ".");
            despesas.txperf = temPfee;
            despesas.vltxperf = valorPagoPfee.ToString("N2").Replace(".", "").Replace(",", ".");
            lista.Add(despesas);

            return lista;
        }

        public List<OutrasDespesas> RetornaOutrasDespesas(int idCliente, DateTime data)
        {
            List<OutrasDespesas> lista = new List<OutrasDespesas>();

            DateTime dataPrimeiroDiaMes = Calendario.RetornaPrimeiroDiaCorridoMes(data, 0);

            List<int> listaOrigem = new List<int>();
            listaOrigem.Add((int)OrigemLancamentoLiquidacao.Provisao.PagtoTaxaGestao);
            listaOrigem.Add((int)OrigemLancamentoLiquidacao.Bolsa.TaxaCustodia);
            LiquidacaoCollection liquidacaoCollection = new LiquidacaoCollection();
            liquidacaoCollection.Query.Where(liquidacaoCollection.Query.IdCliente.Equal(idCliente),
                                           liquidacaoCollection.Query.DataVencimento.GreaterThanOrEqual(dataPrimeiroDiaMes),
                                           liquidacaoCollection.Query.DataVencimento.LessThanOrEqual(data),
                                           liquidacaoCollection.Query.Origem.In(listaOrigem));

            foreach (Liquidacao liquidacao in liquidacaoCollection)
            {
                int origem = liquidacao.Origem.Value;
                decimal valor = liquidacao.Origem.Value;

                #region conversão
                if (this.multiMoeda)
                {
                    int idConta = liquidacao.IdConta.HasValue ? liquidacao.IdConta.Value : 0;
                    DateTime dataVencimento = liquidacao.DataVencimento.Value;

                    Investidor.ContaCorrente conta = new Financial.Investidor.ContaCorrente();
                    conta.LoadByPrimaryKey(idConta);
                    int idMoedaCaixa = conta.IdMoeda.HasValue ? conta.IdMoeda.Value : 0;

                    //***Ajusta para trabalhar com multi-moeda no fluxo
                    decimal fatorMoeda = 1;
                    ConversaoMoeda conversaoMoeda = new ConversaoMoeda();
                    fatorMoeda = conversaoMoeda.RetornaFatorConversao(this.idMoedaCarteira, idMoedaCaixa, dataVencimento);
                    valor = Math.Round(valor * fatorMoeda, 2);
                    //**************************************************
                }
                #endregion

                //Encontrar evento financeiro
                EventoFinanceiro eventoFinanceiro = new EventoFinanceiro();
                if (!liquidacao.IdEventoFinanceiro.HasValue)
                {
                    throw new Exception("Evento financeiro não cadastrado para lançamento: (Id: " +
                        liquidacao.IdLiquidacao +
                        " - Cliente: " + liquidacao.IdCliente.Value +
                        " - Data Lançamento: " + liquidacao.DataLancamento.Value.ToShortDateString() +
                        " - Data Vencimento: " + liquidacao.DataVencimento.Value.ToShortDateString() +
                        " - Descrição: " + liquidacao.Descricao);
                }

                eventoFinanceiro.LoadByPrimaryKey(liquidacao.IdEventoFinanceiro.Value);
                if (!eventoFinanceiro.ClassificacaoAnbima.HasValue)
                {
                    throw new Exception("Evento financeiro não possui Classificação Anbima: (Id: " +
                        eventoFinanceiro.IdEventoFinanceiro +
                        " -  " + eventoFinanceiro.Descricao);
                }

                int codigoProvisao = eventoFinanceiro.ClassificacaoAnbima.Value;

                OutrasDespesas outrasDespesas = new OutrasDespesas();
                outrasDespesas.coddesp = codigoProvisao.ToString().PadLeft(3, '0');
                outrasDespesas.valor = valor.ToString("N2").Replace(".", "").Replace(",", ".");

                lista.Add(outrasDespesas);

            }

            return lista;
        }

        public List<Provisao> RetornaProvisoes(int idCliente, DateTime data, bool historico)
        {
            List<Provisao> lista = new List<Provisao>();

            LiquidacaoCollection liquidacaoCollection = new LiquidacaoCollection();
            string likeFuturos = "%Futuros%";


            if (historico)
            {
                LiquidacaoHistoricoCollection liquidacaoHistoricoCollection = new LiquidacaoHistoricoCollection();
                liquidacaoHistoricoCollection.Query.Where(liquidacaoHistoricoCollection.Query.DataHistorico.Equal(data) & 
                                                          liquidacaoHistoricoCollection.Query.IdCliente.Equal(idCliente) & 
                                                          liquidacaoHistoricoCollection.Query.DataLancamento.LessThanOrEqual(data) &
                                                          liquidacaoHistoricoCollection.Query.DataVencimento.GreaterThan(data) &
                                                          liquidacaoHistoricoCollection.Query.Valor.GreaterThan(0) &
                                                          (liquidacaoHistoricoCollection.Query.Origem.NotIn((int)OrigemLancamentoLiquidacao.BMF.AjusteOperacao,
                                                                                                            (int)OrigemLancamentoLiquidacao.BMF.AjustePosicao ,
                                                                                                            (int)OrigemLancamentoLiquidacao.BMF.Outros) |
                                                         liquidacaoHistoricoCollection.Query.Origem.Equal((int)OrigemLancamentoLiquidacao.BMF.Outros) &
                                                         liquidacaoHistoricoCollection.Query.Descricao.NotLike(likeFuturos)));
                liquidacaoHistoricoCollection.Query.Load();

                LiquidacaoCollection liquidacaoCollectionAux = new LiquidacaoCollection(liquidacaoHistoricoCollection);
                liquidacaoCollection.Combine(liquidacaoCollectionAux);
            }
            else
            {
                liquidacaoCollection.Query.Where(liquidacaoCollection.Query.IdCliente.Equal(idCliente) &
                                                 liquidacaoCollection.Query.DataLancamento.LessThanOrEqual(data) &
                                                 liquidacaoCollection.Query.DataVencimento.GreaterThan(data) &
                                                 liquidacaoCollection.Query.Valor.GreaterThan(0) &
                                                 (liquidacaoCollection.Query.Origem.NotIn((int)OrigemLancamentoLiquidacao.BMF.AjusteOperacao,
                                                                                            (int)OrigemLancamentoLiquidacao.BMF.AjustePosicao,
                                                                                            (int)OrigemLancamentoLiquidacao.BMF.Outros) |
                                                 liquidacaoCollection.Query.Origem.Equal((int)OrigemLancamentoLiquidacao.BMF.Outros) &
                                                 liquidacaoCollection.Query.Descricao.NotLike(likeFuturos)));
                liquidacaoCollection.Query.Load();
            }

            foreach (Liquidacao liquidacao in liquidacaoCollection)
            {

                int origem = liquidacao.Origem.Value;
                if (origem != 1102 && origem != 1101)
                {
                    DateTime dataVencimento = liquidacao.DataVencimento.Value;
                    decimal valor = liquidacao.Valor.Value;

                    #region conversão
                    if (this.multiMoeda)
                    {
                        int idConta = liquidacao.IdConta.HasValue ? liquidacao.IdConta.Value : 0;

                        Investidor.ContaCorrente conta = new Financial.Investidor.ContaCorrente();
                        conta.LoadByPrimaryKey(idConta);
                        int idMoedaCaixa = conta.IdMoeda.HasValue ? conta.IdMoeda.Value : 0;

                        //***Ajusta para trabalhar com multi-moeda no fluxo
                        decimal fatorMoeda = 1;
                        ConversaoMoeda conversaoMoeda = new ConversaoMoeda();
                        fatorMoeda = conversaoMoeda.RetornaFatorConversao(this.idMoedaCarteira, idMoedaCaixa, data);
                        valor = Math.Round(valor * fatorMoeda, 2);
                        //**************************************************
                    }
                    #endregion


                    string creDeb = valor > 0 ? "C" : "D";

                    //Encontrar evento financeiro
                    EventoFinanceiro eventoFinanceiro = new EventoFinanceiro();
                    if (!liquidacao.IdEventoFinanceiro.HasValue)
                    {
                        throw new Exception("Evento financeiro não cadastrado para lançamento: (Id: " +
                            liquidacao.IdLiquidacao +
                            " - Cliente: " + liquidacao.IdCliente.Value +
                            " - Data Lançamento: " + liquidacao.DataLancamento.Value.ToShortDateString() +
                            " - Data Vencimento: " + liquidacao.DataVencimento.Value.ToShortDateString() +
                            " - Descrição: " + liquidacao.Descricao);
                    }

                    eventoFinanceiro.LoadByPrimaryKey(liquidacao.IdEventoFinanceiro.Value);
                    if (!eventoFinanceiro.ClassificacaoAnbima.HasValue)
                    {
                        throw new Exception("Evento financeiro não possui Classificação Anbima: (Id: " +
                            eventoFinanceiro.IdEventoFinanceiro +
                            " -  " + eventoFinanceiro.Descricao);
                    }

                    string codigoProvisao = Convert.ToString(eventoFinanceiro.ClassificacaoAnbima.Value);

                    /*
                    string codigoProvisao = "";
                    if (this.RetornaCodigoLancamento(origem) != 0)
                    {
                        codigoProvisao = this.RetornaCodigoLancamento(origem).ToString();
                    }
                    else
                    {
                        codigoProvisao = "999";
                    }*/
                    
                    Provisao provisao = new Provisao();
                    provisao.dt = dataVencimento.ToString("yyyyMMdd");
                    provisao.valor = Math.Abs(valor).ToString("N2").Replace(".", "").Replace(",", ".");
                    provisao.credeb = creDeb;
                    provisao.codprov = codigoProvisao;

                    lista.Add(provisao);
                }
            }

            liquidacaoCollection = new LiquidacaoCollection();
             if (historico)
             {
                 LiquidacaoHistoricoCollection liquidacaoHistoricoCollection = new LiquidacaoHistoricoCollection();
                 liquidacaoHistoricoCollection.Query.Select(liquidacaoHistoricoCollection.Query.Origem,
                                                            liquidacaoHistoricoCollection.Query.DataVencimento,
                                                            liquidacaoHistoricoCollection.Query.Valor.Sum());
                liquidacaoHistoricoCollection.Query.Where(liquidacaoHistoricoCollection.Query.DataHistorico.Equal(data) &
                                                          liquidacaoHistoricoCollection.Query.IdCliente.Equal(idCliente) &
                                                          liquidacaoHistoricoCollection.Query.DataLancamento.LessThanOrEqual(data) &
                                                          liquidacaoHistoricoCollection.Query.DataVencimento.GreaterThan(data) &
                                                          liquidacaoHistoricoCollection.Query.Valor.LessThan(0) &
                                                          (liquidacaoHistoricoCollection.Query.Origem.NotIn((int)OrigemLancamentoLiquidacao.BMF.AjusteOperacao,
                                                                                                (int)OrigemLancamentoLiquidacao.BMF.AjustePosicao,
                                                                                                (int)OrigemLancamentoLiquidacao.BMF.Outros) |
                                                         liquidacaoHistoricoCollection.Query.Origem.Equal((int)OrigemLancamentoLiquidacao.BMF.Outros) &
                                                         liquidacaoHistoricoCollection.Query.Descricao.NotLike(likeFuturos)));
                 liquidacaoHistoricoCollection.Query.GroupBy(liquidacaoHistoricoCollection.Query.Origem, liquidacaoHistoricoCollection.Query.DataVencimento);
                 liquidacaoHistoricoCollection.Query.Load();
 
                 LiquidacaoCollection liquidacaoCollectionAux = new LiquidacaoCollection(liquidacaoHistoricoCollection);
                 liquidacaoCollection.Combine(liquidacaoCollectionAux);
             }
             else
             {
                 liquidacaoCollection.Query.Select(liquidacaoCollection.Query.Origem,
                                                   liquidacaoCollection.Query.DataVencimento,
                                                   liquidacaoCollection.Query.Valor.Sum());
                liquidacaoCollection.Query.Where(liquidacaoCollection.Query.IdCliente.Equal(idCliente) &
                                                 liquidacaoCollection.Query.DataLancamento.LessThanOrEqual(data) &
                                                 liquidacaoCollection.Query.DataVencimento.GreaterThan(data) &
                                                 liquidacaoCollection.Query.Valor.LessThan(0) &
                                                 (liquidacaoCollection.Query.Origem.NotIn((int)OrigemLancamentoLiquidacao.BMF.AjusteOperacao,
                                                                                        (int)OrigemLancamentoLiquidacao.BMF.AjustePosicao,
                                                                                        (int)OrigemLancamentoLiquidacao.BMF.Outros) |
                                                 liquidacaoCollection.Query.Origem.Equal((int)OrigemLancamentoLiquidacao.BMF.Outros) &
                                                 liquidacaoCollection.Query.Descricao.NotLike(likeFuturos)));
                 liquidacaoCollection.Query.GroupBy(liquidacaoCollection.Query.Origem, liquidacaoCollection.Query.DataVencimento);
                 liquidacaoCollection.Query.Load();
             }
 
             foreach (Liquidacao liquidacao in liquidacaoCollection)
             {
 
                 int origem = liquidacao.Origem.Value;
                 if (origem != 1102 && origem != 1101)
                 {
                     DateTime dataVencimento = liquidacao.DataVencimento.Value;
                     decimal valor = liquidacao.Valor.Value;
 
                     string creDeb = valor > 0 ? "C" : "D";
 
                     string codigoProvisao = "";
                     if (this.RetornaCodigoLancamento(origem) != 0)
                     {
                         codigoProvisao = this.RetornaCodigoLancamento(origem).ToString();
                     }
                     else
                     {
                         codigoProvisao = "999";
                     }
 
                     Provisao provisao = new Provisao();
                     provisao.dt = dataVencimento.ToString("yyyyMMdd");
                     provisao.valor = Math.Abs(valor).ToString("N2").Replace(".", "").Replace(",", ".");
                     provisao.credeb = creDeb;
                     provisao.codprov = codigoProvisao;
 
                     lista.Add(provisao);
                 }
             }
 
             return lista;
       }

           
        

        public List<TermoRf> RetornaTermoRf(int idCliente, DateTime data, bool historico)
        {
            List<TermoRf> lista = new List<TermoRf>();

            PosicaoRendaFixaCollection posicaoRendaFixaCollection = new PosicaoRendaFixaCollection();
            if (historico)
            {
                TituloRendaFixaQuery tituloRendaFixaQuery = new TituloRendaFixaQuery("T");
                PosicaoRendaFixaHistoricoQuery posicaoRendaFixaQuery = new PosicaoRendaFixaHistoricoQuery("P");
                PapelRendaFixaQuery papelRendaFixaQuery = new PapelRendaFixaQuery("A");

                posicaoRendaFixaQuery.InnerJoin(tituloRendaFixaQuery).On(tituloRendaFixaQuery.IdTitulo == posicaoRendaFixaQuery.IdTitulo);
                posicaoRendaFixaQuery.InnerJoin(papelRendaFixaQuery).On(papelRendaFixaQuery.IdPapel == tituloRendaFixaQuery.IdPapel);
                posicaoRendaFixaQuery.Where(posicaoRendaFixaQuery.IdCliente.Equal(idCliente),
                                            posicaoRendaFixaQuery.Quantidade.NotEqual(0),
                                            posicaoRendaFixaQuery.OperacaoTermo.Equal("S"),
                                            posicaoRendaFixaQuery.ValorMercado.GreaterThan(0),
                                            posicaoRendaFixaQuery.DataHistorico.Equal(data));

                PosicaoRendaFixaHistoricoCollection posicaoRendaFixaHistoricoCollection = new PosicaoRendaFixaHistoricoCollection();
                posicaoRendaFixaHistoricoCollection.Load(posicaoRendaFixaQuery);

                PosicaoRendaFixaCollection posicaoRendaFixaCollectionAux = new PosicaoRendaFixaCollection(posicaoRendaFixaHistoricoCollection);
                posicaoRendaFixaCollection.Combine(posicaoRendaFixaCollectionAux);
            }
            else
            {
                TituloRendaFixaQuery tituloRendaFixaQuery = new TituloRendaFixaQuery("T");
                PosicaoRendaFixaQuery posicaoRendaFixaQuery = new PosicaoRendaFixaQuery("P");
                PapelRendaFixaQuery papelRendaFixaQuery = new PapelRendaFixaQuery("A");

                posicaoRendaFixaQuery.InnerJoin(tituloRendaFixaQuery).On(tituloRendaFixaQuery.IdTitulo == posicaoRendaFixaQuery.IdTitulo);
                posicaoRendaFixaQuery.InnerJoin(papelRendaFixaQuery).On(papelRendaFixaQuery.IdPapel == tituloRendaFixaQuery.IdPapel);
                posicaoRendaFixaQuery.Where(papelRendaFixaQuery.TipoPapel.Equal((byte)TipoPapelTitulo.Publico),
                                            posicaoRendaFixaQuery.IdCliente.Equal(idCliente),
                                            posicaoRendaFixaQuery.OperacaoTermo.Equal("S"),
                                            posicaoRendaFixaQuery.ValorMercado.GreaterThan(0),
                                            posicaoRendaFixaQuery.Quantidade.NotEqual(0));
                posicaoRendaFixaCollection.Load(posicaoRendaFixaQuery);
            }

            foreach (PosicaoRendaFixa posicaoRendaFixa in posicaoRendaFixaCollection)
            {
                TituloRendaFixa tituloRendaFixa = new TituloRendaFixa();
                tituloRendaFixa.LoadByPrimaryKey(posicaoRendaFixa.IdTitulo.Value);

                TermoRf termoRf = new TermoRf();

                string dataLiquidacao = posicaoRendaFixa.DataLiquidacao.Value.ToString("yyyyMMdd");
                decimal taxaNominal = tituloRendaFixa.Taxa.HasValue ? tituloRendaFixa.Taxa.Value : 0;
                decimal percentualIndice = tituloRendaFixa.Percentual.HasValue ? tituloRendaFixa.Percentual.Value : 0;
                decimal valorNominal = tituloRendaFixa.PUNominal.HasValue ? tituloRendaFixa.PUNominal.Value : 0;
                decimal principal = Math.Round(posicaoRendaFixa.Quantidade.Value * posicaoRendaFixa.PUOperacao.Value, 2);
                decimal quantidadeDisponivel = (posicaoRendaFixa.Quantidade.Value - posicaoRendaFixa.QuantidadeBloqueada.Value) > 0 ? (posicaoRendaFixa.Quantidade.Value - posicaoRendaFixa.QuantidadeBloqueada.Value) : 0;

                decimal quantidadeGarantia = 0;
                if (posicaoRendaFixa.QuantidadeBloqueada.Value > 0)
                {
                    quantidadeGarantia = posicaoRendaFixa.QuantidadeBloqueada.Value;
                }

                decimal tributos = posicaoRendaFixa.ValorIR.Value + posicaoRendaFixa.ValorIOF.Value;
                decimal valorFinanceiro = Math.Abs(posicaoRendaFixa.ValorMercado.Value);

                decimal decPuOperacao = posicaoRendaFixa.PUOperacao.Value;
                decimal decPuEmissao = tituloRendaFixa.PUNominal.Value;
                decimal decPuPosicao = posicaoRendaFixa.PUMercado.Value;

                #region conversão
                if (this.multiMoeda)
                {
                    //***Ajusta para trabalhar com multi-moeda
                    int idMoedaAtivo = tituloRendaFixa.IdMoeda.HasValue ? tituloRendaFixa.IdMoeda.Value : 0;

                    ConversaoMoeda conversaoMoeda = new ConversaoMoeda();
                    decimal fatorMoeda = 1;
                    fatorMoeda = conversaoMoeda.RetornaFatorConversao(this.idMoedaCarteira, idMoedaAtivo, data);
                    valorFinanceiro = Math.Round(valorFinanceiro * fatorMoeda, 2);
                    tributos = Math.Round(tributos * fatorMoeda, 2);
                    principal = Math.Round(principal * fatorMoeda, 2);
                    valorNominal = Math.Round(valorNominal * fatorMoeda, 2);

                    decPuOperacao = Math.Round(decPuOperacao * fatorMoeda, 8);
                    decPuEmissao = Math.Round(decPuEmissao * fatorMoeda, 8);
                    decPuPosicao = Math.Round(decPuPosicao * fatorMoeda, 8);
                }
                #endregion


                string classeOperacao = (posicaoRendaFixa.Quantidade.Value - posicaoRendaFixa.QuantidadeBloqueada.Value) > 0 ? "C" : "V";

                termoRf.isin = !String.IsNullOrEmpty(tituloRendaFixa.CodigoIsin) ? tituloRendaFixa.CodigoIsin : "";
                termoRf.dtvencativo = tituloRendaFixa.DataVencimento.Value.ToString("yyyyMMdd");
                termoRf.dtoper = posicaoRendaFixa.DataOperacao.Value.ToString("yyyyMMdd");
                termoRf.dtvencimento = dataLiquidacao;
                termoRf.qtd = quantidadeDisponivel.ToString("N9").Replace(".", "").Replace(",", ".");
                termoRf.puoperacao = decPuOperacao.ToString("N8").Replace(".", "").Replace(",", ".");
                termoRf.puvencimento = "0.00000000"; //Conforme cadastro no SELIC ou no ambiente de negociação, caso for emitido no exterior.
                termoRf.puposicao = decPuPosicao.ToString("N8").Replace(".", "").Replace(",", ".");
                termoRf.puemissao = decPuEmissao.ToString("N8").Replace(".", "").Replace(",", ".");
                termoRf.principal = valorNominal.ToString("N2").Replace(".", "").Replace(",", ".");
                termoRf.valorfin = valorFinanceiro.ToString("N2").Replace(".", "").Replace(",", ".");
                termoRf.tributos = tributos.ToString("N2").Replace(".", "").Replace(",", ".");
                termoRf.coupom = taxaNominal.ToString("N2").Replace(".", "").Replace(",", ".");
                termoRf.indexador = tituloRendaFixa.IdIndice.HasValue ? this.RetornaIndice(tituloRendaFixa.IdIndice.Value).ToString() : "PRE"; //Sem indice associado, assume pre-fixado
                termoRf.percindex = percentualIndice.ToString("N2").Replace(".", "").Replace(",", ".");
                termoRf.caracteristica = posicaoRendaFixa.TipoNegociacao.Value == (byte)TipoNegociacaoTitulo.Vencimento ? "V" : "N";
                termoRf.classeoperacao = classeOperacao;
                termoRf.hedge = "";
                termoRf.tphedge = "0";
                termoRf.idinternoativo = tituloRendaFixa.IdTitulo.ToString();
                termoRf.nivelrsc = "";

                lista.Add(termoRf);
            }

            return lista;

        }


        public List<TitPublico> RetornaTitulosPublicos(int idCliente, DateTime data, bool historico)
        {
            List<TitPublico> lista = new List<TitPublico>();

            PosicaoRendaFixaCollection posicaoRendaFixaCollection = new PosicaoRendaFixaCollection();
            if (historico)
            {
                TituloRendaFixaQuery tituloRendaFixaQuery = new TituloRendaFixaQuery("T");
                PosicaoRendaFixaHistoricoQuery posicaoRendaFixaQuery = new PosicaoRendaFixaHistoricoQuery("P");
                PapelRendaFixaQuery papelRendaFixaQuery = new PapelRendaFixaQuery("A");

                posicaoRendaFixaQuery.InnerJoin(tituloRendaFixaQuery).On(tituloRendaFixaQuery.IdTitulo == posicaoRendaFixaQuery.IdTitulo);
                posicaoRendaFixaQuery.InnerJoin(papelRendaFixaQuery).On(papelRendaFixaQuery.IdPapel == tituloRendaFixaQuery.IdPapel);
                posicaoRendaFixaQuery.Where(papelRendaFixaQuery.TipoPapel.Equal((byte)TipoPapelTitulo.Publico),
                                            posicaoRendaFixaQuery.IdCliente.Equal(idCliente),
                                            posicaoRendaFixaQuery.Quantidade.NotEqual(0),
                                            posicaoRendaFixaQuery.OperacaoTermo.NotEqual("S"),
                                            posicaoRendaFixaQuery.DataHistorico.Equal(data));

                PosicaoRendaFixaHistoricoCollection posicaoRendaFixaHistoricoCollection = new PosicaoRendaFixaHistoricoCollection();
                posicaoRendaFixaHistoricoCollection.Load(posicaoRendaFixaQuery);

                PosicaoRendaFixaCollection posicaoRendaFixaCollectionAux = new PosicaoRendaFixaCollection(posicaoRendaFixaHistoricoCollection);
                posicaoRendaFixaCollection.Combine(posicaoRendaFixaCollectionAux);
            }
            else
            {
                TituloRendaFixaQuery tituloRendaFixaQuery = new TituloRendaFixaQuery("T");
                PosicaoRendaFixaQuery posicaoRendaFixaQuery = new PosicaoRendaFixaQuery("P");
                PapelRendaFixaQuery papelRendaFixaQuery = new PapelRendaFixaQuery("A");

                posicaoRendaFixaQuery.InnerJoin(tituloRendaFixaQuery).On(tituloRendaFixaQuery.IdTitulo == posicaoRendaFixaQuery.IdTitulo);
                posicaoRendaFixaQuery.InnerJoin(papelRendaFixaQuery).On(papelRendaFixaQuery.IdPapel == tituloRendaFixaQuery.IdPapel);
                posicaoRendaFixaQuery.Where(papelRendaFixaQuery.TipoPapel.Equal((byte)TipoPapelTitulo.Publico),
                                            posicaoRendaFixaQuery.IdCliente.Equal(idCliente),
                                            posicaoRendaFixaQuery.OperacaoTermo.NotEqual("S"),
                                            posicaoRendaFixaQuery.Quantidade.NotEqual(0));
                posicaoRendaFixaCollection.Load(posicaoRendaFixaQuery);
            }

            foreach (PosicaoRendaFixa posicaoRendaFixa in posicaoRendaFixaCollection)
            {
                TituloRendaFixa tituloRendaFixa = new TituloRendaFixa();
                tituloRendaFixa.LoadByPrimaryKey(posicaoRendaFixa.IdTitulo.Value);

                TitPublico titPublico = new TitPublico();

                decimal taxaNominal = tituloRendaFixa.Taxa.HasValue ? tituloRendaFixa.Taxa.Value : 0;
                decimal percentualIndice = tituloRendaFixa.Percentual.HasValue ? tituloRendaFixa.Percentual.Value : 0;
                decimal valorNominal = tituloRendaFixa.PUNominal.HasValue ? tituloRendaFixa.PUNominal.Value : 0;
                valorNominal = Math.Round(posicaoRendaFixa.Quantidade.Value * valorNominal, 2);
                decimal principal = Math.Round(posicaoRendaFixa.Quantidade.Value * posicaoRendaFixa.PUOperacao.Value, 2);

                decimal quantidadeDisponivel = (posicaoRendaFixa.Quantidade.Value - posicaoRendaFixa.QuantidadeBloqueada.Value) > 0 ? (posicaoRendaFixa.Quantidade.Value - posicaoRendaFixa.QuantidadeBloqueada.Value) : 0;

                decimal quantidadeGarantia = 0;
                if (posicaoRendaFixa.QuantidadeBloqueada.Value > 0)
                {
                    quantidadeGarantia = posicaoRendaFixa.QuantidadeBloqueada.Value;
                }

                decimal tributos = posicaoRendaFixa.ValorIR.Value + posicaoRendaFixa.ValorIOF.Value;
                decimal valorFinanceiro = Math.Abs(posicaoRendaFixa.ValorMercado.Value);

                decimal decPuOperacao = posicaoRendaFixa.PUOperacao.Value;
                decimal decPuEmissao = tituloRendaFixa.PUNominal.Value;
                decimal decPuPosicao = posicaoRendaFixa.PUMercado.Value;

                #region conversão
                if (this.multiMoeda)
                {
                    //***Ajusta para trabalhar com multi-moeda
                    int idMoedaAtivo = tituloRendaFixa.IdMoeda.HasValue ? tituloRendaFixa.IdMoeda.Value : 0;

                    ConversaoMoeda conversaoMoeda = new ConversaoMoeda();
                    decimal fatorMoeda = 1;
                    fatorMoeda = conversaoMoeda.RetornaFatorConversao(this.idMoedaCarteira, idMoedaAtivo, data);
                    valorFinanceiro = Math.Round(valorFinanceiro * fatorMoeda, 2);
                    tributos = Math.Round(tributos * fatorMoeda, 2);
                    principal = Math.Round(principal * fatorMoeda, 2);
                    valorNominal = Math.Round(valorNominal * fatorMoeda, 2);

                    decPuOperacao = Math.Round(decPuOperacao * fatorMoeda, 8);
                    decPuEmissao = Math.Round(decPuEmissao * fatorMoeda, 8);
                    decPuPosicao = Math.Round(decPuPosicao * fatorMoeda, 8);
                }
                #endregion

                string classeOperacao = (posicaoRendaFixa.Quantidade.Value - posicaoRendaFixa.QuantidadeBloqueada.Value) > 0 ? "C" : "V";

                titPublico.caracteristica = posicaoRendaFixa.TipoNegociacao.Value == (byte)TipoNegociacaoTitulo.Vencimento ? "V" : "N";
                titPublico.classeoperacao = classeOperacao;
                titPublico.codativo = !String.IsNullOrEmpty(tituloRendaFixa.CodigoCustodia) ? tituloRendaFixa.CodigoCustodia.Trim() : "";
                titPublico.coupom = taxaNominal.ToString("N2").Replace(".", "").Replace(",", ".");
                titPublico.cusip = "";
                titPublico.depgar = "0"; //Identifica a câmara de liquidação em favor da qual a garantia está depositada, conforme tabela auxiliar “Depositário das Garantias”. 
                //O campo deve ser preenchido se a "Quantidade em Garantia” for diferente de zero.
                titPublico.dtemissao = tituloRendaFixa.DataEmissao.Value.ToString("yyyyMMdd");
                titPublico.dtoperacao = posicaoRendaFixa.DataOperacao.Value.ToString("yyyyMMdd");
                titPublico.dtvencimento = tituloRendaFixa.DataVencimento.Value.ToString("yyyyMMdd");
                titPublico.idinternoativo = tituloRendaFixa.IdTitulo.ToString();
                titPublico.indexador = tituloRendaFixa.IdIndice.HasValue ? this.RetornaIndice(tituloRendaFixa.IdIndice.Value).ToString() : "PRE"; //Sem indice associado, assume pre-fixado
                titPublico.isin = !String.IsNullOrEmpty(tituloRendaFixa.CodigoIsin) ? tituloRendaFixa.CodigoIsin : "";
                titPublico.nivelrsc = "";
                titPublico.percindex = percentualIndice.ToString("N2").Replace(".", "").Replace(",", ".");
                titPublico.percprovcred = "0.00"; //Forçado % Provisão de Credito = 0
                titPublico.principal = valorNominal.ToString("N2").Replace(".", "").Replace(",", ".");
                titPublico.pucompra = decPuOperacao.ToString("N8").Replace(".", "").Replace(",", ".");
                titPublico.puemissao = decPuEmissao.ToString("N8").Replace(".", "").Replace(",", ".");
                titPublico.puposicao = decPuPosicao.ToString("N8").Replace(".", "").Replace(",", ".");
                titPublico.puvencimento = "0.00000000"; //Conforme cadastro no SELIC ou no ambiente de negociação, caso for emitido no exterior.
                titPublico.qtdisponivel = quantidadeDisponivel.ToString("N9").Replace(".", "").Replace(",", ".");
                titPublico.qtgarantia = quantidadeGarantia.ToString("N9").Replace(".", "").Replace(",", ".");
                titPublico.tributos = tributos.ToString("N2").Replace(".", "").Replace(",", ".");
                titPublico.valorfindisp = valorFinanceiro.ToString("N2").Replace(".", "").Replace(",", ".");
                titPublico.valorfinemgar = Utilitario.Truncate(quantidadeGarantia * decPuPosicao, 2).ToString("N2").Replace(".", "").Replace(",", ".");

                if (posicaoRendaFixa.TipoOperacao.Value == (byte)TipoOperacaoTitulo.CompraRevenda)
                {
                    decimal puVolta = posicaoRendaFixa.PUVolta.HasValue ? posicaoRendaFixa.PUVolta.Value : 0;
                    decimal taxaVolta = posicaoRendaFixa.TaxaVolta.HasValue ? posicaoRendaFixa.TaxaVolta.Value : 0;
                    string dataVolta = posicaoRendaFixa.DataVolta.HasValue ? posicaoRendaFixa.DataVolta.Value.ToString("yyyyMMdd") : "";

                    Compromisso compromisso = new Compromisso();
                    compromisso.classecomp = "C"; // Hoje apenas Compra com Revenda;
                    compromisso.dtretorno = dataVolta;
                    compromisso.indexadorcomp = "PRE"; //Hoje apenas compromissadas pre-fixadas
                    compromisso.perindexcomp = "100.00"; //No caso de Títulos pré fixados, o valor preenchido será sempre 100.00. 
                    compromisso.puretorno = puVolta.ToString("N8").Replace(".", "").Replace(",", ".");
                    compromisso.txoperacao = taxaVolta.ToString("N2").Replace(".", "").Replace(",", ".");

                    titPublico.compromisso = compromisso;
                }

                lista.Add(titPublico);
            }

            return lista;
        }

        public List<TitPrivado> RetornaTitulosPrivados(int idCliente, DateTime data, bool historico)
        {
            List<TitPrivado> lista = new List<TitPrivado>();

            PosicaoRendaFixaCollection posicaoRendaFixaCollection = new PosicaoRendaFixaCollection();
            if (historico)
            {
                TituloRendaFixaQuery tituloRendaFixaQuery = new TituloRendaFixaQuery("T");
                PosicaoRendaFixaHistoricoQuery posicaoRendaFixaQuery = new PosicaoRendaFixaHistoricoQuery("P");
                PapelRendaFixaQuery papelRendaFixaQuery = new PapelRendaFixaQuery("A");

                posicaoRendaFixaQuery.InnerJoin(tituloRendaFixaQuery).On(tituloRendaFixaQuery.IdTitulo == posicaoRendaFixaQuery.IdTitulo);
                posicaoRendaFixaQuery.InnerJoin(papelRendaFixaQuery).On(papelRendaFixaQuery.IdPapel == tituloRendaFixaQuery.IdPapel);
                posicaoRendaFixaQuery.Where(papelRendaFixaQuery.TipoPapel.Equal((byte)TipoPapelTitulo.Privado),
                                            papelRendaFixaQuery.Classe.NotEqual((int)ClasseRendaFixa.Debenture),
                                            posicaoRendaFixaQuery.IdCliente.Equal(idCliente),
                                            posicaoRendaFixaQuery.Quantidade.NotEqual(0),
                                            posicaoRendaFixaQuery.OperacaoTermo.NotEqual("S"),
                                            posicaoRendaFixaQuery.DataHistorico.Equal(data));

                PosicaoRendaFixaHistoricoCollection posicaoRendaFixaHistoricoCollection = new PosicaoRendaFixaHistoricoCollection();
                posicaoRendaFixaHistoricoCollection.Load(posicaoRendaFixaQuery);

                PosicaoRendaFixaCollection posicaoRendaFixaCollectionAux = new PosicaoRendaFixaCollection(posicaoRendaFixaHistoricoCollection);
                posicaoRendaFixaCollection.Combine(posicaoRendaFixaCollectionAux);
            }
            else
            {
                TituloRendaFixaQuery tituloRendaFixaQuery = new TituloRendaFixaQuery("T");
                PosicaoRendaFixaQuery posicaoRendaFixaQuery = new PosicaoRendaFixaQuery("P");
                PapelRendaFixaQuery papelRendaFixaQuery = new PapelRendaFixaQuery("A");

                posicaoRendaFixaQuery.InnerJoin(tituloRendaFixaQuery).On(tituloRendaFixaQuery.IdTitulo == posicaoRendaFixaQuery.IdTitulo);
                posicaoRendaFixaQuery.InnerJoin(papelRendaFixaQuery).On(papelRendaFixaQuery.IdPapel == tituloRendaFixaQuery.IdPapel);
                posicaoRendaFixaQuery.Where(papelRendaFixaQuery.TipoPapel.Equal((byte)TipoPapelTitulo.Privado),
                                            papelRendaFixaQuery.Classe.NotEqual((int)ClasseRendaFixa.Debenture),
                                            posicaoRendaFixaQuery.IdCliente.Equal(idCliente),
                                            posicaoRendaFixaQuery.OperacaoTermo.NotEqual("S"),
                                            posicaoRendaFixaQuery.Quantidade.NotEqual(0));
                posicaoRendaFixaCollection.Load(posicaoRendaFixaQuery);
            }

            foreach (PosicaoRendaFixa posicaoRendaFixa in posicaoRendaFixaCollection)
            {
                TituloRendaFixa tituloRendaFixa = new TituloRendaFixa();
                tituloRendaFixa.LoadByPrimaryKey(posicaoRendaFixa.IdTitulo.Value);

                Emissor emissor = new Emissor();
                emissor.LoadByPrimaryKey(tituloRendaFixa.IdEmissor.Value);

                TitPrivado titPrivado = new TitPrivado();

                decimal taxaNominal = tituloRendaFixa.Taxa.HasValue ? tituloRendaFixa.Taxa.Value : 0;
                decimal percentualIndice = tituloRendaFixa.Percentual.HasValue ? tituloRendaFixa.Percentual.Value : 0;
                decimal valorNominal = tituloRendaFixa.PUNominal.HasValue ? tituloRendaFixa.PUNominal.Value : 0;
                valorNominal = Math.Round(posicaoRendaFixa.Quantidade.Value * valorNominal, 2);
                decimal principal = Math.Round(posicaoRendaFixa.Quantidade.Value * posicaoRendaFixa.PUOperacao.Value, 2);

                decimal quantidadeDisponivel = (posicaoRendaFixa.Quantidade.Value - posicaoRendaFixa.QuantidadeBloqueada.Value) > 0 ? (posicaoRendaFixa.Quantidade.Value - posicaoRendaFixa.QuantidadeBloqueada.Value) : 0;

                decimal quantidadeGarantia = 0;
                if (posicaoRendaFixa.QuantidadeBloqueada.Value > 0)
                {
                    quantidadeGarantia = posicaoRendaFixa.QuantidadeBloqueada.Value;
                }

                decimal tributos = posicaoRendaFixa.ValorIR.Value + posicaoRendaFixa.ValorIOF.Value;
                decimal valorFinanceiro = Math.Abs(posicaoRendaFixa.ValorMercado.Value);

                decimal decPuOperacao = posicaoRendaFixa.PUOperacao.Value;
                decimal decPuEmissao = tituloRendaFixa.PUNominal.Value;
                decimal decPuPosicao = posicaoRendaFixa.PUMercado.Value;

                #region conversão
                if (this.multiMoeda)
                {
                    //***Ajusta para trabalhar com multi-moeda
                    int idMoedaAtivo = tituloRendaFixa.IdMoeda.HasValue ? tituloRendaFixa.IdMoeda.Value : 0;

                    ConversaoMoeda conversaoMoeda = new ConversaoMoeda();
                    decimal fatorMoeda = 1;
                    fatorMoeda = conversaoMoeda.RetornaFatorConversao(this.idMoedaCarteira, idMoedaAtivo, data);
                    valorFinanceiro = Math.Round(valorFinanceiro * fatorMoeda, 2);
                    tributos = Math.Round(tributos * fatorMoeda, 2);
                    principal = Math.Round(principal * fatorMoeda, 2);
                    valorNominal = Math.Round(valorNominal * fatorMoeda, 2);

                    decPuOperacao = Math.Round(decPuOperacao * fatorMoeda, 8);
                    decPuEmissao = Math.Round(decPuEmissao * fatorMoeda, 8);
                    decPuPosicao = Math.Round(decPuPosicao * fatorMoeda, 8);
                }
                #endregion


                string classeOperacao = (posicaoRendaFixa.Quantidade.Value - posicaoRendaFixa.QuantidadeBloqueada.Value) > 0 ? "C" : "V";

                titPrivado.caracteristica = posicaoRendaFixa.TipoNegociacao.Value == (byte)TipoNegociacaoTitulo.Vencimento ? "V" : "N";
                titPrivado.classeoperacao = classeOperacao;
                titPrivado.cnpjemissor = !String.IsNullOrEmpty(emissor.Cnpj) ? Utilitario.RemoveCaracteresEspeciais(emissor.Cnpj) : "";
                titPrivado.codativo = !String.IsNullOrEmpty(tituloRendaFixa.CodigoCetip) ? tituloRendaFixa.CodigoCetip.Trim() : string.Empty;
                titPrivado.coupom = taxaNominal.ToString("N2").Replace(".", "").Replace(",", ".");
                titPrivado.cusip = "";
                titPrivado.depgar = "0"; //Identifica a câmara de liquidação em favor da qual a garantia está depositada, conforme tabela auxiliar “Depositário das Garantias”. 
                //O campo deve ser preenchido se a "Quantidade em Garantia” for diferente de zero.
                titPrivado.dtemissao = tituloRendaFixa.DataEmissao.Value.ToString("yyyyMMdd");
                titPrivado.dtoperacao = posicaoRendaFixa.DataOperacao.Value.ToString("yyyyMMdd");
                titPrivado.dtvencimento = tituloRendaFixa.DataVencimento.Value.ToString("yyyyMMdd");
                titPrivado.idinternoativo = tituloRendaFixa.IdTitulo.ToString();
                titPrivado.indexador = tituloRendaFixa.IdIndice.HasValue ? this.RetornaIndice(tituloRendaFixa.IdIndice.Value).ToString() : "PRE"; //Sem indice associado, assume pre-fixado
                titPrivado.isin = !String.IsNullOrEmpty(tituloRendaFixa.CodigoIsin) ? tituloRendaFixa.CodigoIsin : "";
                titPrivado.nivelrsc = "";
                titPrivado.percindex = percentualIndice.ToString("N2").Replace(".", "").Replace(",", ".");
                titPrivado.percprovcred = "0.00"; //Forçado % Provisão de Credito = 0
                titPrivado.principal = valorNominal.ToString("N2").Replace(".", "").Replace(",", ".");
                titPrivado.pucompra = decPuOperacao.ToString("N8").Replace(".", "").Replace(",", ".");
                titPrivado.puemissao = decPuEmissao.ToString("N8").Replace(".", "").Replace(",", ".");
                titPrivado.puposicao = decPuPosicao.ToString("N8").Replace(".", "").Replace(",", ".");
                titPrivado.puvencimento = "0.00000000"; //Conforme cadastro no SELIC ou no ambiente de negociação, caso for emitido no exterior.
                titPrivado.qtdisponivel = quantidadeDisponivel.ToString("N9").Replace(".", "").Replace(",", ".");
                titPrivado.qtgarantia = quantidadeGarantia.ToString("N9").Replace(".", "").Replace(",", ".");
                titPrivado.tributos = tributos.ToString("N2").Replace(".", "").Replace(",", ".");
                titPrivado.valorfindisp = valorFinanceiro.ToString("N2").Replace(".", "").Replace(",", ".");
                titPrivado.valorfinemgar = Utilitario.Truncate(quantidadeGarantia * decPuPosicao, 2).ToString("N2").Replace(".", "").Replace(",", ".");

                if (posicaoRendaFixa.TipoOperacao.Value == (byte)TipoOperacaoTitulo.CompraRevenda)
                {
                    decimal puVolta = posicaoRendaFixa.PUVolta.HasValue ? posicaoRendaFixa.PUVolta.Value : 0;
                    decimal taxaVolta = posicaoRendaFixa.TaxaVolta.HasValue ? posicaoRendaFixa.TaxaVolta.Value : 0;
                    string dataVolta = posicaoRendaFixa.DataVolta.HasValue ? posicaoRendaFixa.DataVolta.Value.ToString("yyyyMMdd") : "";

                    Compromisso compromisso = new Compromisso();
                    compromisso.classecomp = "C"; // Hoje apenas Compra com Revenda;
                    compromisso.dtretorno = dataVolta;
                    compromisso.indexadorcomp = "PRE"; //Hoje apenas compromissadas pre-fixadas
                    compromisso.perindexcomp = "100.00"; //No caso de Títulos pré fixados, o valor preenchido será sempre 100.00. 
                    compromisso.puretorno = puVolta.ToString("N8").Replace(".", "").Replace(",", ".");
                    compromisso.txoperacao = taxaVolta.ToString("N2").Replace(".", "").Replace(",", ".");

                    titPrivado.compromisso = compromisso;
                }

                lista.Add(titPrivado);
            }

            return lista;
        }

        public List<Debenture> RetornaDebentures(int idCliente, DateTime data, bool historico)
        {
            List<Debenture> lista = new List<Debenture>();

            PosicaoRendaFixaCollection posicaoRendaFixaCollection = new PosicaoRendaFixaCollection();
            if (historico)
            {
                TituloRendaFixaQuery tituloRendaFixaQuery = new TituloRendaFixaQuery("T");
                PosicaoRendaFixaHistoricoQuery posicaoRendaFixaQuery = new PosicaoRendaFixaHistoricoQuery("P");
                PapelRendaFixaQuery papelRendaFixaQuery = new PapelRendaFixaQuery("A");

                posicaoRendaFixaQuery.InnerJoin(tituloRendaFixaQuery).On(tituloRendaFixaQuery.IdTitulo == posicaoRendaFixaQuery.IdTitulo);
                posicaoRendaFixaQuery.InnerJoin(papelRendaFixaQuery).On(papelRendaFixaQuery.IdPapel == tituloRendaFixaQuery.IdPapel);
                posicaoRendaFixaQuery.Where(papelRendaFixaQuery.Classe.Equal((int)ClasseRendaFixa.Debenture),
                                            posicaoRendaFixaQuery.IdCliente.Equal(idCliente),
                                            posicaoRendaFixaQuery.Quantidade.NotEqual(0),
                                            posicaoRendaFixaQuery.OperacaoTermo.NotEqual("S"),
                                            posicaoRendaFixaQuery.DataHistorico.Equal(data));

                PosicaoRendaFixaHistoricoCollection posicaoRendaFixaHistoricoCollection = new PosicaoRendaFixaHistoricoCollection();
                posicaoRendaFixaHistoricoCollection.Load(posicaoRendaFixaQuery);

                PosicaoRendaFixaCollection posicaoRendaFixaCollectionAux = new PosicaoRendaFixaCollection(posicaoRendaFixaHistoricoCollection);
                posicaoRendaFixaCollection.Combine(posicaoRendaFixaCollectionAux);
            }
            else
            {
                TituloRendaFixaQuery tituloRendaFixaQuery = new TituloRendaFixaQuery("T");
                PosicaoRendaFixaQuery posicaoRendaFixaQuery = new PosicaoRendaFixaQuery("P");
                PapelRendaFixaQuery papelRendaFixaQuery = new PapelRendaFixaQuery("A");

                posicaoRendaFixaQuery.InnerJoin(tituloRendaFixaQuery).On(tituloRendaFixaQuery.IdTitulo == posicaoRendaFixaQuery.IdTitulo);
                posicaoRendaFixaQuery.InnerJoin(papelRendaFixaQuery).On(papelRendaFixaQuery.IdPapel == tituloRendaFixaQuery.IdPapel);
                posicaoRendaFixaQuery.Where(papelRendaFixaQuery.Classe.Equal((int)ClasseRendaFixa.Debenture),
                                            posicaoRendaFixaQuery.IdCliente.Equal(idCliente),
                                            posicaoRendaFixaQuery.OperacaoTermo.NotEqual("S"),
                                            posicaoRendaFixaQuery.Quantidade.NotEqual(0));
                posicaoRendaFixaCollection.Load(posicaoRendaFixaQuery);
            }

            foreach (PosicaoRendaFixa posicaoRendaFixa in posicaoRendaFixaCollection)
            {
                TituloRendaFixa tituloRendaFixa = new TituloRendaFixa();
                tituloRendaFixa.LoadByPrimaryKey(posicaoRendaFixa.IdTitulo.Value);

                Emissor emissor = new Emissor();
                emissor.LoadByPrimaryKey(tituloRendaFixa.IdEmissor.Value);

                Debenture debenture = new Debenture();

                decimal taxaNominal = tituloRendaFixa.Taxa.HasValue ? tituloRendaFixa.Taxa.Value : 0;
                decimal percentualIndice = tituloRendaFixa.Percentual.HasValue ? tituloRendaFixa.Percentual.Value : 0;
                decimal valorNominal = tituloRendaFixa.PUNominal.HasValue ? tituloRendaFixa.PUNominal.Value : 0;
                decimal principal = Math.Round(posicaoRendaFixa.Quantidade.Value * posicaoRendaFixa.PUOperacao.Value, 2);

                decimal quantidadeDisponivel = (posicaoRendaFixa.Quantidade.Value - posicaoRendaFixa.QuantidadeBloqueada.Value) > 0 ? (posicaoRendaFixa.Quantidade.Value - posicaoRendaFixa.QuantidadeBloqueada.Value) : 0;

                decimal quantidadeGarantia = 0;
                if (posicaoRendaFixa.QuantidadeBloqueada.Value > 0)
                {
                    quantidadeGarantia = posicaoRendaFixa.QuantidadeBloqueada.Value;
                }

                decimal tributos = posicaoRendaFixa.ValorIR.Value + posicaoRendaFixa.ValorIOF.Value;
                decimal valorFinanceiro = Math.Abs(posicaoRendaFixa.ValorMercado.Value);

                decimal decPuOperacao = posicaoRendaFixa.PUOperacao.Value;
                decimal decPuEmissao = tituloRendaFixa.PUNominal.Value;
                decimal decPuPosicao = posicaoRendaFixa.PUMercado.Value;

                #region conversão
                if (this.multiMoeda)
                {
                    //***Ajusta para trabalhar com multi-moeda
                    int idMoedaAtivo = tituloRendaFixa.IdMoeda.HasValue ? tituloRendaFixa.IdMoeda.Value : 0;

                    ConversaoMoeda conversaoMoeda = new ConversaoMoeda();
                    decimal fatorMoeda = 1;
                    fatorMoeda = conversaoMoeda.RetornaFatorConversao(this.idMoedaCarteira, idMoedaAtivo, data);
                    valorFinanceiro = Math.Round(valorFinanceiro * fatorMoeda, 2);
                    tributos = Math.Round(tributos * fatorMoeda, 2);
                    principal = Math.Round(principal * fatorMoeda, 2);
                    valorNominal = Math.Round(valorNominal * fatorMoeda, 2);

                    decPuOperacao = Math.Round(decPuOperacao * fatorMoeda, 8);
                    decPuEmissao = Math.Round(decPuEmissao * fatorMoeda, 8);
                    decPuPosicao = Math.Round(decPuPosicao * fatorMoeda, 8);
                }
                #endregion



                string classeOperacao = (posicaoRendaFixa.Quantidade.Value - posicaoRendaFixa.QuantidadeBloqueada.Value) > 0 ? "C" : "V";

                debenture.caracteristica = posicaoRendaFixa.TipoNegociacao.Value == (byte)TipoNegociacaoTitulo.Vencimento ? "V" : "N";
                debenture.classeoperacao = classeOperacao;
                debenture.cnpjemissor = !String.IsNullOrEmpty(emissor.Cnpj) ? Utilitario.RemoveCaracteresEspeciais(emissor.Cnpj) : "";
                debenture.coddeb = !String.IsNullOrEmpty(tituloRendaFixa.CodigoCustodia) ? tituloRendaFixa.CodigoCustodia.Trim() : tituloRendaFixa.IdTitulo.Value.ToString();
                debenture.coupom = taxaNominal.ToString("N2").Replace(".", "").Replace(",", ".");
                debenture.cusip = "";
                debenture.debconv = tituloRendaFixa.DebentureConversivel;
                debenture.debpartlucro = "N";
                debenture.SPE = "N";
                debenture.depgar = "0"; //Identifica a câmara de liquidação em favor da qual a garantia está depositada, conforme tabela auxiliar “Depositário das Garantias”. 
                //O campo deve ser preenchido se a "Quantidade em Garantia” for diferente de zero.
                debenture.dtemissao = tituloRendaFixa.DataEmissao.Value.ToString("yyyyMMdd");
                debenture.dtoperacao = posicaoRendaFixa.DataOperacao.Value.ToString("yyyyMMdd");
                debenture.dtvencimento = tituloRendaFixa.DataVencimento.Value.ToString("yyyyMMdd");
                debenture.idinternoativo = tituloRendaFixa.IdTitulo.ToString();
                debenture.indexador = tituloRendaFixa.IdIndice.HasValue ? this.RetornaIndice(tituloRendaFixa.IdIndice.Value).ToString() : "PRE"; //Sem indice associado, assume pre-fixado
                debenture.isin = !String.IsNullOrEmpty(tituloRendaFixa.CodigoIsin) ? tituloRendaFixa.CodigoIsin : "";
                debenture.nivelrsc = "";
                debenture.percindex = percentualIndice.ToString("N2").Replace(".", "").Replace(",", ".");
                debenture.percprovcred = "0.00"; //Forçado % Provisão de Credito = 0
                debenture.principal = valorNominal.ToString("N2").Replace(".", "").Replace(",", ".");
                debenture.pucompra = decPuOperacao.ToString("N8").Replace(".", "").Replace(",", ".");
                debenture.puemissao = decPuEmissao.ToString("N8").Replace(".", "").Replace(",", ".");
                debenture.puposicao = decPuPosicao.ToString("N8").Replace(".", "").Replace(",", ".");
                debenture.puvencimento = "0.00000000"; //Conforme cadastro no SELIC ou no ambiente de negociação, caso for emitido no exterior.
                debenture.qtdisponivel = quantidadeDisponivel.ToString("N9").Replace(".", "").Replace(",", ".");
                debenture.qtgarantia = quantidadeGarantia.ToString("N9").Replace(".", "").Replace(",", ".");
                debenture.tributos = tributos.ToString("N2").Replace(".", "").Replace(",", ".");
                debenture.valorfindisp = valorFinanceiro.ToString("N2").Replace(".", "").Replace(",", ".");
                debenture.valorfinemgar = Utilitario.Truncate(quantidadeGarantia * decPuPosicao, 2).ToString("N2").Replace(".", "").Replace(",", ".");

                if (posicaoRendaFixa.TipoOperacao.Value == (byte)TipoOperacaoTitulo.CompraRevenda)
                {
                    decimal puVolta = posicaoRendaFixa.PUVolta.HasValue ? posicaoRendaFixa.PUVolta.Value : 0;
                    decimal taxaVolta = posicaoRendaFixa.TaxaVolta.HasValue ? posicaoRendaFixa.TaxaVolta.Value : 0;
                    string dataVolta = posicaoRendaFixa.DataVolta.HasValue ? posicaoRendaFixa.DataVolta.Value.ToString("yyyyMMdd") : "";

                    Compromisso compromisso = new Compromisso();
                    compromisso.classecomp = "C"; // Hoje apenas Compra com Revenda;
                    compromisso.dtretorno = dataVolta;
                    compromisso.indexadorcomp = "PRE"; //Hoje apenas compromissadas pre-fixadas
                    compromisso.perindexcomp = "100.00"; //No caso de Títulos pré fixados, o valor preenchido será sempre 100.00. 
                    compromisso.puretorno = puVolta.ToString("N8").Replace(".", "").Replace(",", ".");
                    compromisso.txoperacao = taxaVolta.ToString("N2").Replace(".", "").Replace(",", ".");

                    debenture.compromisso = compromisso;
                }

                lista.Add(debenture);
            }

            return lista;
        }

        public List<Participacao> RetornaDebenturesParticipacoes(int idCliente, DateTime data, bool historico)
        {
            List<Participacao> lista = new List<Participacao>();

            PosicaoRendaFixaCollection posicaoRendaFixaCollection = new PosicaoRendaFixaCollection();
            if (historico)
            {
                TituloRendaFixaQuery tituloRendaFixaQuery = new TituloRendaFixaQuery("T");
                PosicaoRendaFixaHistoricoQuery posicaoRendaFixaQuery = new PosicaoRendaFixaHistoricoQuery("P");
                PapelRendaFixaQuery papelRendaFixaQuery = new PapelRendaFixaQuery("A");

                posicaoRendaFixaQuery.InnerJoin(tituloRendaFixaQuery).On(tituloRendaFixaQuery.IdTitulo == posicaoRendaFixaQuery.IdTitulo);
                posicaoRendaFixaQuery.InnerJoin(papelRendaFixaQuery).On(papelRendaFixaQuery.IdPapel == tituloRendaFixaQuery.IdPapel);
                posicaoRendaFixaQuery.Where(papelRendaFixaQuery.Classe.Equal((int)ClasseRendaFixa.Debenture),
                                            posicaoRendaFixaQuery.IdCliente.Equal(idCliente),
                                            posicaoRendaFixaQuery.Quantidade.NotEqual(0),
                                            posicaoRendaFixaQuery.DataHistorico.Equal(data),
                                            tituloRendaFixaQuery.Participacao.Equal("S"));

                PosicaoRendaFixaHistoricoCollection posicaoRendaFixaHistoricoCollection = new PosicaoRendaFixaHistoricoCollection();
                posicaoRendaFixaHistoricoCollection.Load(posicaoRendaFixaQuery);

                PosicaoRendaFixaCollection posicaoRendaFixaCollectionAux = new PosicaoRendaFixaCollection(posicaoRendaFixaHistoricoCollection);
                posicaoRendaFixaCollection.Combine(posicaoRendaFixaCollectionAux);
            }
            else
            {
                TituloRendaFixaQuery tituloRendaFixaQuery = new TituloRendaFixaQuery("T");
                PosicaoRendaFixaQuery posicaoRendaFixaQuery = new PosicaoRendaFixaQuery("P");
                PapelRendaFixaQuery papelRendaFixaQuery = new PapelRendaFixaQuery("A");

                posicaoRendaFixaQuery.InnerJoin(tituloRendaFixaQuery).On(tituloRendaFixaQuery.IdTitulo == posicaoRendaFixaQuery.IdTitulo);
                posicaoRendaFixaQuery.InnerJoin(papelRendaFixaQuery).On(papelRendaFixaQuery.IdPapel == tituloRendaFixaQuery.IdPapel);
                posicaoRendaFixaQuery.Where(papelRendaFixaQuery.Classe.Equal((int)ClasseRendaFixa.Debenture),
                                            posicaoRendaFixaQuery.IdCliente.Equal(idCliente),
                                            posicaoRendaFixaQuery.Quantidade.NotEqual(0),
                                            tituloRendaFixaQuery.Participacao.Equal("S"));
                posicaoRendaFixaCollection.Load(posicaoRendaFixaQuery);
            }

            foreach (PosicaoRendaFixa posicaoRendaFixa in posicaoRendaFixaCollection)
            {
                TituloRendaFixa tituloRendaFixa = new TituloRendaFixa();
                tituloRendaFixa.LoadByPrimaryKey(posicaoRendaFixa.IdTitulo.Value);

                Emissor emissor = new Emissor();
                emissor.LoadByPrimaryKey(tituloRendaFixa.IdEmissor.Value);

                string cnpjEmissor = emissor.Cnpj;
                if (cnpjEmissor == null)
                {
                    cnpjEmissor = "0";
                }

                decimal valorFinanceiro = Math.Abs(posicaoRendaFixa.ValorMercado.Value);

                Participacao participacoes = new Participacao();
                participacoes.cnpjpart = cnpjEmissor.PadLeft(14, '0');
                participacoes.valorfinanceiro = valorFinanceiro.ToString("N2").Replace(".", "").Replace(",", ".");
                lista.Add(participacoes);

            }

            return lista;
        }
		
        public List<OpcaoDeriv> RetornaOpcoesDeriv(int idCliente, DateTime data, bool historico)
        {
            List<OpcaoDeriv> lista = new List<OpcaoDeriv>();
            PosicaoBMFCollection posicaoBmfCollection = new PosicaoBMFCollection();
            AtivoBMFQuery ati = new AtivoBMFQuery("ati");



            if (historico)
            {
                PosicaoBMFHistoricoCollection posicaoBMFHistoricoCollection = new PosicaoBMFHistoricoCollection();

                PosicaoBMFHistoricoQuery pos = new PosicaoBMFHistoricoQuery("pos");


                pos.Select(pos, ati);
                pos.LeftJoin(ati).On(ati.CdAtivoBMF.Equal(pos.CdAtivoBMF) & ati.Serie.Equal(pos.Serie));
                pos.Where(ati.TipoMercado.In(TipoMercadoBMF.OpcaoDisponivel, TipoMercadoBMF.OpcaoFuturo), pos.IdCliente.Equal(idCliente), pos.DataHistorico.Equal(data));


                posicaoBMFHistoricoCollection.Load(pos);

                //PosicaoBMFCollection posicaoBMFCollectionAux = new PosicaoBMFCollection(posicaoBMFHistoricoCollection);                
                //posicaoBmfCollection.Combine(posicaoBMFCollectionAux);

                foreach (PosicaoBMFHistorico posicao in posicaoBMFHistoricoCollection)
                {

                    decimal valorFinanceiro = 0;
                    decimal tributos = 0;
                    decimal precoExercicio = 0;
                    decimal premio = 0;
                    decimal puposicao = 0;

                    valorFinanceiro = Math.Abs(posicao.ValorMercado.Value);
                    if (posicao.GetColumn(ati.PrecoExercicio) != null)
                    {
                        precoExercicio = (decimal)posicao.GetColumn(ati.PrecoExercicio);
                    }
                    premio = Math.Abs(posicao.PUCusto.Value);
                    puposicao = posicao.PUMercado.Value;


                    #region conversão
                    if (this.multiMoeda)
                    {
                        //***Ajusta para trabalhar com multi-moeda
                        string cdAtivoBolsa = posicao.CdAtivoBMF.ToString();

                        AtivoBolsa ativoBolsa = new AtivoBolsa();
                        ativoBolsa.Query.Select(ativoBolsa.Query.IdMoeda);
                        ativoBolsa.Query.Where(ativoBolsa.Query.CdAtivoBolsa.Equal(cdAtivoBolsa));
                        ativoBolsa.Query.Load();
                        int idMoedaAtivo = ativoBolsa.IdMoeda.HasValue ? (int)ativoBolsa.IdMoeda.Value : 0;

                        ConversaoMoeda conversaoMoeda = new ConversaoMoeda();
                        decimal fatorMoeda = 1;
                        fatorMoeda = conversaoMoeda.RetornaFatorConversao(this.idMoedaCarteira, idMoedaAtivo, data);

                        valorFinanceiro = Math.Round(Math.Abs(posicao.ValorMercado.Value) * fatorMoeda, 2);
                        puposicao = Math.Round(puposicao * fatorMoeda, 8);
                        precoExercicio = Math.Round(precoExercicio * fatorMoeda, 2);
                        premio = Math.Round(premio * fatorMoeda, 2);
                    }
                    #endregion


                    OpcaoDeriv opcao = new OpcaoDeriv();
                    if (posicao.GetColumn(ati.CodigoIsin) != null)
                        opcao.isin = posicao.GetColumn(ati.CodigoIsin).ToString();

                    if (posicao.TipoMercado.HasValue)
                        opcao.mercado = posicao.TipoMercado.Value == TipoMercadoBMF.OpcaoDisponivel ? "1" : posicao.TipoMercado.Value == TipoMercadoBMF.OpcaoFuturo ? "2" : "";

                    opcao.ativo = posicao.CdAtivoBMF.ToString();

                    if (posicao.GetColumn(ati.TipoSerie) != null)
                        opcao.callput = posicao.GetColumn(ati.TipoSerie).ToString() == "V" ? "P" : posicao.GetColumn(ati.TipoSerie).ToString();

                    opcao.serie = posicao.Serie.ToString();
                    if (posicao.Quantidade.HasValue)
                        opcao.quantidade = Math.Abs(posicao.Quantidade.Value).ToString("N2").Replace(".", "").Replace(",", ".");

                    if (posicao.ValorMercado.HasValue)
                        opcao.valorfinanceiro = Math.Abs(valorFinanceiro).ToString("N2").Replace(".", "").Replace(",", ".");

                    opcao.tributos = "0.00";

                    if (posicao.GetColumn(ati.PrecoExercicio) != null)
                        opcao.precoexercicio = precoExercicio.ToString().Replace(".", "").Replace(",", ".");

                    if (posicao.PUCusto.HasValue)
                        opcao.premio = premio.ToString("N2").Replace(".", "").Replace(",", ".");

                    if (posicao.GetColumn(ati.DataVencimento) != null)
                        opcao.dtvencimento = posicao.DataVencimento.Value.ToString("yyyyMMdd");

                    if (posicao.Quantidade.HasValue)
                        opcao.classeoperacao = posicao.Quantidade.Value > 0 ? "C" : "V";

                    if (posicao.PUMercado.HasValue)
                        opcao.puposicao = puposicao.ToString("N2").Replace(".", "").Replace(",", ".");

                    opcao.hedge = "N";
                    opcao.tphedge = "0";

                    lista.Add(opcao);

                }

            }
            else
            {
                PosicaoBMFQuery pos = new PosicaoBMFQuery("pos");

                pos.LeftJoin(ati).On(ati.CdAtivoBMF.Equal(pos.CdAtivoBMF) & ati.Serie.Equal(pos.Serie));
                pos.Where(ati.TipoMercado.In(TipoMercadoBMF.OpcaoDisponivel, TipoMercadoBMF.OpcaoFuturo), pos.IdCliente.Equal(idCliente));

                posicaoBmfCollection.Load(pos);

                foreach (PosicaoBMF posicao in posicaoBmfCollection)
                {

                    OpcaoDeriv opcao = new OpcaoDeriv();

                    decimal valorFinanceiro = 0;
                    decimal tributos = 0;
                    decimal precoExercicio = 0;
                    decimal premio = 0;
                    decimal puposicao = 0;

                    valorFinanceiro = Math.Abs(posicao.ValorMercado.Value);
                    if (posicao.GetColumn(ati.PrecoExercicio) != null)
                    {
                        precoExercicio = (decimal)posicao.GetColumn(ati.PrecoExercicio);
                    }

                    premio = Math.Abs(posicao.PUCusto.Value);
                    puposicao = posicao.PUMercado.Value;


                    #region conversão
                    if (this.multiMoeda)
                    {
                        //***Ajusta para trabalhar com multi-moeda
                        string cdAtivoBolsa = posicao.CdAtivoBMF.ToString();

                        AtivoBolsa ativoBolsa = new AtivoBolsa();
                        ativoBolsa.Query.Select(ativoBolsa.Query.IdMoeda);
                        ativoBolsa.Query.Where(ativoBolsa.Query.CdAtivoBolsa.Equal(cdAtivoBolsa));
                        ativoBolsa.Query.Load();
                        int idMoedaAtivo = ativoBolsa.IdMoeda.HasValue ? (int)ativoBolsa.IdMoeda.Value : 0;

                        ConversaoMoeda conversaoMoeda = new ConversaoMoeda();
                        decimal fatorMoeda = 1;
                        fatorMoeda = conversaoMoeda.RetornaFatorConversao(this.idMoedaCarteira, idMoedaAtivo, data);

                        valorFinanceiro = Math.Round(Math.Abs(posicao.ValorMercado.Value) * fatorMoeda, 2);
                        puposicao = Math.Round(puposicao * fatorMoeda, 8);
                        precoExercicio = Math.Round(precoExercicio * fatorMoeda, 2);
                        premio = Math.Round(premio * fatorMoeda, 2);
                    }
                    #endregion


                    if (posicao.GetColumn(ati.CodigoIsin) != null)
                        opcao.isin = posicao.GetColumn(ati.CodigoIsin).ToString();

                    if (posicao.TipoMercado.HasValue)
                        opcao.mercado = posicao.TipoMercado.Value == TipoMercadoBMF.OpcaoDisponivel ? "1" : posicao.TipoMercado.Value == TipoMercadoBMF.OpcaoFuturo ? "2" : "";

                    opcao.ativo = posicao.CdAtivoBMF.ToString();

                    if (posicao.GetColumn(ati.TipoSerie) != null)
                        opcao.callput = posicao.GetColumn(ati.TipoSerie).ToString() == "V" ? "P" : posicao.GetColumn(ati.TipoSerie).ToString();

                    opcao.serie = posicao.Serie.ToString();
                    if (posicao.Quantidade.HasValue)
                        opcao.quantidade = Math.Abs(posicao.Quantidade.Value).ToString("N2").Replace(".", "").Replace(",", ".");

                    if (posicao.ValorMercado.HasValue)
                        opcao.valorfinanceiro = valorFinanceiro.ToString("N2").Replace(".", "").Replace(",", ".");

                    opcao.tributos = "0.00";

                    if (posicao.GetColumn(ati.PrecoExercicio) != null)
                        opcao.precoexercicio = precoExercicio.ToString().Replace(".", "").Replace(",", ".");

                    if (posicao.PUCusto.HasValue)
                        opcao.premio = premio.ToString("N2").Replace(".", "").Replace(",", ".");

                    if (posicao.GetColumn(ati.DataVencimento) != null)
                        opcao.dtvencimento = posicao.DataVencimento.Value.ToString("yyyyMMdd");

                    if (posicao.Quantidade.HasValue)
                        opcao.classeoperacao = posicao.Quantidade.Value > 0 ? "C" : "V";

                    if (posicao.PUMercado.HasValue)
                        opcao.puposicao = puposicao.ToString("N2").Replace(".", "").Replace(",", ".");

                    opcao.hedge = "N";
                    opcao.tphedge = "0";

                    lista.Add(opcao);

                }
            }





            return lista;

        }

        public HeaderFundo RetornaHeaderFundo(int idCliente, DateTime data, bool historico)
        {
            Pessoa pessoa = new Pessoa();
            pessoa.LoadByPrimaryKey(idCliente);

            Financial.Fundo.Carteira carteiraFundo = new Financial.Fundo.Carteira();
            carteiraFundo.LoadByPrimaryKey(idCliente);

            AgenteMercado agenteAdministrador = new AgenteMercado();
            agenteAdministrador.LoadByPrimaryKey(carteiraFundo.IdAgenteAdministrador.Value);

            AgenteMercado agenteGestor = new AgenteMercado();
            agenteGestor.LoadByPrimaryKey(carteiraFundo.IdAgenteGestor.Value);

            AgenteMercado agenteCustodiante = new AgenteMercado();
            int idAgenteCustodiante = carteiraFundo.IdAgenteCustodiante.HasValue ? carteiraFundo.IdAgenteCustodiante.Value :
                carteiraFundo.IdAgenteAdministrador.Value;
            agenteCustodiante.LoadByPrimaryKey(idAgenteCustodiante);

            HeaderFundo header = new HeaderFundo();

            header.cnpj = !String.IsNullOrEmpty(pessoa.Cpfcnpj) ? Utilitario.RemoveCaracteresEspeciais(pessoa.Cpfcnpj.Trim()) : "";
            header.cnpjadm = !String.IsNullOrEmpty(agenteAdministrador.Cnpj) ? Utilitario.RemoveCaracteresEspeciais(agenteAdministrador.Cnpj.Trim()) : "";
            header.cnpjgestor = !String.IsNullOrEmpty(agenteGestor.Cnpj) ? Utilitario.RemoveCaracteresEspeciais(agenteGestor.Cnpj.Trim()) : "";

            //Colocando o CNPJ do Administrador para o Custodiante, mudar depois para pegar o Custodiante da Carteira!
            header.cnpjcustodiante = !String.IsNullOrEmpty(agenteCustodiante.Cnpj) ? Utilitario.RemoveCaracteresEspeciais(agenteCustodiante.Cnpj.Trim()) : "";

            header.codanbid = !String.IsNullOrEmpty(carteiraFundo.CodigoAnbid) ? carteiraFundo.CodigoAnbid : "";
            header.dtposicao = data.ToString("yyyyMMdd");
            header.isin = !String.IsNullOrEmpty(carteiraFundo.CodigoIsin) ? carteiraFundo.CodigoIsin : "";
            header.nivelrsc = "";
            header.nome = pessoa.Nome.ToUpper().Trim();

            header.nomeadm = agenteAdministrador.Nome.ToUpper().Trim();
            header.nomegestor = agenteGestor.Nome.ToUpper().Trim();

            //Colocando o nome do Administrador para o Custodiante, mudar depois para pegar o Custodiante da Carteira!
            header.nomecustodiante = agenteCustodiante.Nome.ToUpper().Trim();

            HistoricoCota historicoCota = new HistoricoCota();
            historicoCota.Query.Select(historicoCota.Query.PLFechamento,
                                       historicoCota.Query.CotaFechamento,
                                       historicoCota.Query.QuantidadeFechamento);
            historicoCota.Query.Where(historicoCota.Query.IdCarteira.Equal(idCliente),
                                      historicoCota.Query.Data.Equal(data));
            historicoCota.Query.Load();

            decimal plFechamento = historicoCota.PLFechamento.HasValue ? historicoCota.PLFechamento.Value : 0;
            decimal valorCota = historicoCota.CotaFechamento.HasValue ? historicoCota.CotaFechamento.Value : 0;
            decimal quantidade = historicoCota.QuantidadeFechamento.HasValue ? historicoCota.QuantidadeFechamento.Value : 0;

            decimal valoresReceber = 0;
            decimal valoresPagar = 0;
            decimal cotasEmitir = 0;
            decimal cotasResgatar = 0;
            string notLikeFuturos = "%Futuros%";
            if (historico)
            {
                LiquidacaoHistorico liquidacaoReceber = new LiquidacaoHistorico();
                liquidacaoReceber.Query.Select(liquidacaoReceber.Query.Valor.Sum());
                liquidacaoReceber.Query.Where(liquidacaoReceber.Query.IdCliente.Equal(idCliente) &
                                            liquidacaoReceber.Query.DataLancamento.LessThanOrEqual(data) &
                                            liquidacaoReceber.Query.DataVencimento.GreaterThan(data) &
                                            liquidacaoReceber.Query.Valor.GreaterThan(0) &
                                            liquidacaoReceber.Query.DataHistorico.Equal(data) &
                                            (liquidacaoReceber.Query.Origem.NotIn((int)OrigemLancamentoLiquidacao.BMF.AjusteOperacao,
                                                                                  (int)OrigemLancamentoLiquidacao.BMF.AjustePosicao,
                                                                                  (int)OrigemLancamentoLiquidacao.Cotista.Resgate,
                                                                                  (int)OrigemLancamentoLiquidacao.Cotista.AplicacaoConverter,
                                                                                  (int)OrigemLancamentoLiquidacao.BMF.Outros) |
                                            liquidacaoReceber.Query.Origem.Equal((int)OrigemLancamentoLiquidacao.BMF.Outros) &
                                            liquidacaoReceber.Query.Descricao.NotLike(notLikeFuturos)));
                liquidacaoReceber.Query.Load();
                valoresReceber = liquidacaoReceber.Valor.HasValue ? liquidacaoReceber.Valor.Value : 0;

                LiquidacaoHistorico liquidacaoPagar = new LiquidacaoHistorico();
                liquidacaoPagar.Query.Select(liquidacaoPagar.Query.Valor.Sum());
                liquidacaoPagar.Query.Where(liquidacaoPagar.Query.IdCliente.Equal(idCliente) &
                                            liquidacaoPagar.Query.DataLancamento.LessThanOrEqual(data) &
                                            liquidacaoPagar.Query.DataVencimento.GreaterThan(data) &
                                            liquidacaoPagar.Query.Valor.LessThan(0) &
                                            liquidacaoPagar.Query.DataHistorico.Equal(data) &
                                            (liquidacaoPagar.Query.Origem.NotIn((int)OrigemLancamentoLiquidacao.BMF.AjusteOperacao,
                                                                                (int)OrigemLancamentoLiquidacao.BMF.AjustePosicao,
                                                                                (int)OrigemLancamentoLiquidacao.Cotista.Resgate,
                                                                                (int)OrigemLancamentoLiquidacao.Cotista.AplicacaoConverter,
                                                                                (int)OrigemLancamentoLiquidacao.BMF.Outros) |
                                            liquidacaoReceber.Query.Origem.Equal((int)OrigemLancamentoLiquidacao.BMF.Outros) &
                                            liquidacaoReceber.Query.Descricao.NotLike(notLikeFuturos)));
                liquidacaoPagar.Query.Load();
                valoresPagar = liquidacaoPagar.Valor.HasValue ? Math.Abs(liquidacaoPagar.Valor.Value) : 0;

                LiquidacaoHistorico liquidacaoResgate = new LiquidacaoHistorico();
                liquidacaoResgate.Query.Select(liquidacaoResgate.Query.Valor.Sum());
                liquidacaoResgate.Query.Where(liquidacaoResgate.Query.IdCliente.Equal(idCliente),
                                           liquidacaoResgate.Query.DataLancamento.LessThanOrEqual(data),
                                           liquidacaoResgate.Query.DataVencimento.GreaterThan(data),
                                           liquidacaoResgate.Query.Valor.LessThan(0),
                                           liquidacaoResgate.Query.DataHistorico.Equal(data),
                                           liquidacaoResgate.Query.Origem.Equal((int)OrigemLancamentoLiquidacao.Cotista.Resgate));

                liquidacaoResgate.Query.Load();
                cotasResgatar = liquidacaoResgate.Valor.HasValue ? Math.Abs(liquidacaoResgate.Valor.Value) : 0;

                LiquidacaoHistorico liquidacaoCotasEmitir = new LiquidacaoHistorico();
                liquidacaoCotasEmitir.Query.Select(liquidacaoCotasEmitir.Query.Valor.Sum());
                liquidacaoCotasEmitir.Query.Where(liquidacaoCotasEmitir.Query.IdCliente.Equal(idCliente),
                                           liquidacaoCotasEmitir.Query.DataLancamento.LessThanOrEqual(data),
                                           liquidacaoCotasEmitir.Query.DataVencimento.GreaterThan(data),
                                           liquidacaoCotasEmitir.Query.Valor.LessThan(0),
                                           liquidacaoCotasEmitir.Query.DataHistorico.Equal(data),
                                           liquidacaoCotasEmitir.Query.Origem.Equal((int)OrigemLancamentoLiquidacao.Cotista.AplicacaoConverter));

                liquidacaoCotasEmitir.Query.Load();
                cotasEmitir = liquidacaoCotasEmitir.Valor.HasValue ? Math.Abs(liquidacaoCotasEmitir.Valor.Value) : 0;
            }
            else
            {
                Liquidacao liquidacaoReceber = new Liquidacao();
                liquidacaoReceber.Query.Select(liquidacaoReceber.Query.Valor.Sum());
                liquidacaoReceber.Query.Where(liquidacaoReceber.Query.IdCliente.Equal(idCliente) &
                                                liquidacaoReceber.Query.DataLancamento.LessThanOrEqual(data) &
                                                liquidacaoReceber.Query.DataVencimento.GreaterThan(data) &
                                                liquidacaoReceber.Query.Valor.GreaterThan(0) &
                                                (liquidacaoReceber.Query.Origem.NotIn((int)OrigemLancamentoLiquidacao.BMF.AjusteOperacao,
                                                                                        (int)OrigemLancamentoLiquidacao.BMF.AjustePosicao,
                                                                                        (int)OrigemLancamentoLiquidacao.Cotista.Resgate,
                                                                                        (int)OrigemLancamentoLiquidacao.Cotista.AplicacaoConverter,
                                                                                        (int)OrigemLancamentoLiquidacao.BMF.Outros) |
                                                liquidacaoReceber.Query.Origem.Equal((int)OrigemLancamentoLiquidacao.BMF.Outros) &
                                                liquidacaoReceber.Query.Descricao.NotLike(notLikeFuturos)));
                liquidacaoReceber.Query.Load();
                valoresReceber = liquidacaoReceber.Valor.HasValue ? liquidacaoReceber.Valor.Value : 0;

                Liquidacao liquidacaoPagar = new Liquidacao();
                liquidacaoPagar.Query.Select(liquidacaoPagar.Query.Valor.Sum());
                liquidacaoPagar.Query.Where(liquidacaoPagar.Query.IdCliente.Equal(idCliente) &
                                            liquidacaoPagar.Query.DataLancamento.LessThanOrEqual(data) &
                                            liquidacaoPagar.Query.DataVencimento.GreaterThan(data) &
                                            liquidacaoPagar.Query.Valor.LessThan(0) &
                                            (liquidacaoPagar.Query.Origem.NotIn((int)OrigemLancamentoLiquidacao.BMF.AjusteOperacao,
                                                                                (int)OrigemLancamentoLiquidacao.BMF.AjustePosicao,
                                                                                (int)OrigemLancamentoLiquidacao.Cotista.Resgate,
                                                                                (int)OrigemLancamentoLiquidacao.Cotista.AplicacaoConverter,
                                                                                (int)OrigemLancamentoLiquidacao.BMF.Outros) |
                                            liquidacaoReceber.Query.Origem.Equal((int)OrigemLancamentoLiquidacao.BMF.Outros) &
                                            liquidacaoReceber.Query.Descricao.NotLike(notLikeFuturos)));
                liquidacaoPagar.Query.Load();
                valoresPagar = liquidacaoPagar.Valor.HasValue ? Math.Abs(liquidacaoPagar.Valor.Value) : 0;


                Liquidacao liquidacaoResgate = new Liquidacao();
                liquidacaoResgate.Query.Select(liquidacaoResgate.Query.Valor.Sum());
                liquidacaoResgate.Query.Where(liquidacaoResgate.Query.IdCliente.Equal(idCliente),
                                           liquidacaoResgate.Query.DataLancamento.LessThanOrEqual(data),
                                           liquidacaoResgate.Query.DataVencimento.GreaterThan(data),
                                           liquidacaoResgate.Query.Valor.LessThan(0),
                                           liquidacaoResgate.Query.Origem.Equal((int)OrigemLancamentoLiquidacao.Cotista.Resgate));

                liquidacaoResgate.Query.Load();
                cotasResgatar = liquidacaoResgate.Valor.HasValue ? Math.Abs(liquidacaoResgate.Valor.Value) : 0;

                Liquidacao liquidacaoCotasEmitir = new Liquidacao();
                liquidacaoCotasEmitir.Query.Select(liquidacaoCotasEmitir.Query.Valor.Sum());
                liquidacaoCotasEmitir.Query.Where(liquidacaoCotasEmitir.Query.IdCliente.Equal(idCliente),
                                           liquidacaoCotasEmitir.Query.DataLancamento.LessThanOrEqual(data),
                                           liquidacaoCotasEmitir.Query.DataVencimento.GreaterThan(data),
                                           liquidacaoCotasEmitir.Query.Valor.LessThan(0),
                                           liquidacaoCotasEmitir.Query.Origem.Equal((int)OrigemLancamentoLiquidacao.Cotista.AplicacaoConverter));

                liquidacaoCotasEmitir.Query.Load();
                cotasEmitir = liquidacaoCotasEmitir.Valor.HasValue ? Math.Abs(liquidacaoCotasEmitir.Valor.Value) : 0;
            }

            carteiraFundo = new Financial.Fundo.Carteira();

            decimal valorAtivos = 0;
            if (historico)
            {
                valorAtivos = carteiraFundo.RetornaValorMercadoHistorico(idCliente, data);
            }
            else
            {
                valorAtivos = carteiraFundo.RetornaValorMercado(idCliente);
            }

            decimal valoresAjustesBMF = 0;
            if (historico)
            {
                LiquidacaoHistorico liquidacaoAjusteBMF = new LiquidacaoHistorico();
                liquidacaoAjusteBMF.Query.Select(liquidacaoAjusteBMF.Query.Valor.Sum());
                liquidacaoAjusteBMF.Query.Where(liquidacaoAjusteBMF.Query.IdCliente.Equal(idCliente) &
                                                liquidacaoAjusteBMF.Query.DataLancamento.LessThanOrEqual(data) &
                                                liquidacaoAjusteBMF.Query.DataVencimento.GreaterThan(data) &
                                                liquidacaoAjusteBMF.Query.DataHistorico.Equal(data) &
                                                (liquidacaoAjusteBMF.Query.Origem.In((int)OrigemLancamentoLiquidacao.BMF.AjusteOperacao &
                                                                                     (int)OrigemLancamentoLiquidacao.BMF.AjustePosicao) |
                                                liquidacaoAjusteBMF.Query.Origem.Equal((int)OrigemLancamentoLiquidacao.BMF.Outros) &
                                                liquidacaoAjusteBMF.Query.Descricao.Like(notLikeFuturos)));
                liquidacaoAjusteBMF.Query.Load();
                valoresAjustesBMF = liquidacaoAjusteBMF.Valor.HasValue ? liquidacaoAjusteBMF.Valor.Value : 0;
            }
            else
            {
                Liquidacao liquidacaoAjusteBMF = new Liquidacao();
                liquidacaoAjusteBMF.Query.Select(liquidacaoAjusteBMF.Query.Valor.Sum());
                liquidacaoAjusteBMF.Query.Where(liquidacaoAjusteBMF.Query.IdCliente.Equal(idCliente) &
                                                liquidacaoAjusteBMF.Query.DataLancamento.LessThanOrEqual(data) &
                                                liquidacaoAjusteBMF.Query.DataVencimento.GreaterThan(data) &
                                                (liquidacaoAjusteBMF.Query.Origem.In((int)OrigemLancamentoLiquidacao.BMF.AjusteOperacao &
                                                                                     (int)OrigemLancamentoLiquidacao.BMF.AjustePosicao) |
                                                liquidacaoAjusteBMF.Query.Origem.Equal((int)OrigemLancamentoLiquidacao.BMF.Outros) &
                                                liquidacaoAjusteBMF.Query.Descricao.Like(notLikeFuturos)));
                liquidacaoAjusteBMF.Query.Load();
                valoresAjustesBMF = liquidacaoAjusteBMF.Valor.HasValue ? liquidacaoAjusteBMF.Valor.Value : 0;
            }

            valorAtivos += valoresAjustesBMF;

            SaldoCaixa saldoCaixa = new SaldoCaixa();
            saldoCaixa.BuscaSaldoCaixa(idCliente, data);

            decimal valor = 0;
            if (saldoCaixa.SaldoFechamento.HasValue)
            {
                valor = saldoCaixa.SaldoFechamento.Value;
            }

            valorAtivos += valor;

            header.patliq = plFechamento.ToString("N2").Replace(".", "").Replace(",", ".");
            header.quantidade = quantidade.ToString("N9").Replace(".", "").Replace(",", ".");
            header.tipofundo = "229"; //Usando por ora o tipo = Renda Fixa, conforme faz a YMF para clubes
            header.valorativos = valorAtivos.ToString("N2").Replace(".", "").Replace(",", ".");
            header.valorcota = valorCota.ToString("N9").Replace(".", "").Replace(",", ".");
            header.valorpagar = Math.Abs(valoresPagar).ToString("N2").Replace(".", "").Replace(",", ".");
            header.valorreceber = Math.Abs(valoresReceber).ToString("N2").Replace(".", "").Replace(",", ".");
            header.vlcotasemitir = cotasEmitir.ToString("N2").Replace(".", "").Replace(",", ".");
            header.vlcotasresgatar = cotasResgatar.ToString("N2").Replace(".", "").Replace(",", ".");

            return header;
        }

        public HeaderCarteira RetornaHeaderCarteira(int idCliente, DateTime data, bool historico)
        {
            Pessoa pessoa = new Pessoa();
            pessoa.LoadByPrimaryKey(idCliente);

            Financial.Fundo.Carteira carteiraFundo = new Financial.Fundo.Carteira();
            carteiraFundo.LoadByPrimaryKey(idCliente);

            AgenteMercado agenteAdministrador = new AgenteMercado();
            agenteAdministrador.LoadByPrimaryKey(carteiraFundo.IdAgenteAdministrador.Value);

            AgenteMercado agenteGestor = new AgenteMercado();
            agenteGestor.LoadByPrimaryKey(carteiraFundo.IdAgenteGestor.Value);

            AgenteMercado agenteCustodiante = new AgenteMercado();
            int idAgenteCustodiante = carteiraFundo.IdAgenteCustodiante.HasValue ? carteiraFundo.IdAgenteCustodiante.Value :
                carteiraFundo.IdAgenteAdministrador.Value;
            agenteCustodiante.LoadByPrimaryKey(idAgenteCustodiante);

            HeaderCarteira header = new HeaderCarteira();

            header.cnpjcpf = !String.IsNullOrEmpty(pessoa.Cpfcnpj) ? Utilitario.RemoveCaracteresEspeciais(pessoa.Cpfcnpj.Trim()) : "";
            //Colocando o CNPJ do Administrador para o Custodiante, mudar depois para pegar o Custodiante da Carteira!
            header.cnpjcustodiante = !String.IsNullOrEmpty(agenteCustodiante.Cnpj) ? Utilitario.RemoveCaracteresEspeciais(agenteCustodiante.Cnpj.Trim()) : "";

            header.cnpjgestor = !String.IsNullOrEmpty(agenteGestor.Cnpj) ? Utilitario.RemoveCaracteresEspeciais(agenteGestor.Cnpj.Trim()) : "";
            header.codcart = idCliente.ToString(); //Conforme documento da Anbima, "Este é o código interno da carteira no sistema do informante." 
            header.codcntcor = "00000000000000000";
            header.dtposicao = data.ToString("yyyyMMdd");
            header.nome = pessoa.Nome.ToUpper().Trim();

            //Colocando o nome do Administrador para o Custodiante, mudar depois para pegar o Custodiante da Carteira!
            header.nomecustodiante = agenteCustodiante.Nome.ToUpper().Trim();

            header.nomegestor = agenteGestor.Nome.ToUpper().Trim();

            header.tpcli = pessoa.Tipo.Value == (byte)TipoPessoa.Fisica ? "F" : "J";

            HistoricoCota historicoCota = new HistoricoCota();
            historicoCota.Query.Select(historicoCota.Query.PLFechamento);
            historicoCota.Query.Where(historicoCota.Query.IdCarteira.Equal(idCliente),
                                      historicoCota.Query.Data.Equal(data));
            historicoCota.Query.Load();

            decimal plFechamento = historicoCota.PLFechamento.HasValue ? historicoCota.PLFechamento.Value : 0;

            decimal valoresReceber = 0;
            decimal valoresPagar = 0;
            if (historico)
            {
                LiquidacaoHistorico liquidacaoReceber = new LiquidacaoHistorico();
                liquidacaoReceber.Query.Select(liquidacaoReceber.Query.Valor.Sum());
                liquidacaoReceber.Query.Where(liquidacaoReceber.Query.IdCliente.Equal(idCliente),
                                           liquidacaoReceber.Query.DataLancamento.LessThanOrEqual(data),
                                           liquidacaoReceber.Query.DataVencimento.GreaterThan(data),
                                           liquidacaoReceber.Query.Valor.GreaterThan(0),
                                           liquidacaoReceber.Query.DataHistorico.Equal(data),
                                           liquidacaoReceber.Query.Origem.NotIn((int)OrigemLancamentoLiquidacao.BMF.AjusteOperacao,
                                                                                (int)OrigemLancamentoLiquidacao.BMF.AjustePosicao));
                liquidacaoReceber.Query.Load();
                valoresReceber = liquidacaoReceber.Valor.HasValue ? liquidacaoReceber.Valor.Value : 0;

                LiquidacaoHistorico liquidacaoPagar = new LiquidacaoHistorico();
                liquidacaoPagar.Query.Select(liquidacaoPagar.Query.Valor.Sum());
                liquidacaoPagar.Query.Where(liquidacaoPagar.Query.IdCliente.Equal(idCliente),
                                           liquidacaoPagar.Query.DataLancamento.LessThanOrEqual(data),
                                           liquidacaoPagar.Query.DataVencimento.GreaterThan(data),
                                           liquidacaoPagar.Query.Valor.LessThan(0),
                                           liquidacaoPagar.Query.DataHistorico.Equal(data),
                                           liquidacaoPagar.Query.Origem.NotIn((int)OrigemLancamentoLiquidacao.BMF.AjusteOperacao,
                                                                              (int)OrigemLancamentoLiquidacao.BMF.AjustePosicao));
                liquidacaoPagar.Query.Load();
                valoresPagar = liquidacaoPagar.Valor.HasValue ? Math.Abs(liquidacaoPagar.Valor.Value) : 0;
            }
            else
            {
                Liquidacao liquidacaoReceber = new Liquidacao();
                liquidacaoReceber.Query.Select(liquidacaoReceber.Query.Valor.Sum());
                liquidacaoReceber.Query.Where(liquidacaoReceber.Query.IdCliente.Equal(idCliente),
                                           liquidacaoReceber.Query.DataLancamento.LessThanOrEqual(data),
                                           liquidacaoReceber.Query.DataVencimento.GreaterThan(data),
                                           liquidacaoReceber.Query.Valor.GreaterThan(0),
                                           liquidacaoReceber.Query.Origem.NotIn((int)OrigemLancamentoLiquidacao.BMF.AjusteOperacao,
                                                                                (int)OrigemLancamentoLiquidacao.BMF.AjustePosicao));
                liquidacaoReceber.Query.Load();
                valoresReceber = liquidacaoReceber.Valor.HasValue ? liquidacaoReceber.Valor.Value : 0;

                Liquidacao liquidacaoPagar = new Liquidacao();
                liquidacaoPagar.Query.Select(liquidacaoPagar.Query.Valor.Sum());
                liquidacaoPagar.Query.Where(liquidacaoPagar.Query.IdCliente.Equal(idCliente),
                                           liquidacaoPagar.Query.DataLancamento.LessThanOrEqual(data),
                                           liquidacaoPagar.Query.DataVencimento.GreaterThan(data),
                                           liquidacaoPagar.Query.Valor.LessThan(0),
                                           liquidacaoPagar.Query.Origem.NotIn((int)OrigemLancamentoLiquidacao.BMF.AjusteOperacao,
                                                                              (int)OrigemLancamentoLiquidacao.BMF.AjustePosicao));
                liquidacaoPagar.Query.Load();
                valoresPagar = liquidacaoPagar.Valor.HasValue ? Math.Abs(liquidacaoPagar.Valor.Value) : 0;
            }


            carteiraFundo = new Financial.Fundo.Carteira();

            decimal valorAtivos = 0;
            if (historico)
            {
                valorAtivos = carteiraFundo.RetornaValorMercadoHistorico(idCliente, data);
            }
            else
            {
                valorAtivos = carteiraFundo.RetornaValorMercado(idCliente);
            }

            decimal valoresAjustesBMF = 0;
            if (historico)
            {
                LiquidacaoHistorico liquidacaoAjusteBMF = new LiquidacaoHistorico();
                liquidacaoAjusteBMF.Query.Select(liquidacaoAjusteBMF.Query.Valor.Sum());
                liquidacaoAjusteBMF.Query.Where(liquidacaoAjusteBMF.Query.IdCliente.Equal(idCliente),
                                           liquidacaoAjusteBMF.Query.DataLancamento.LessThanOrEqual(data),
                                           liquidacaoAjusteBMF.Query.DataVencimento.GreaterThan(data),
                                           liquidacaoAjusteBMF.Query.DataHistorico.Equal(data),
                                           liquidacaoAjusteBMF.Query.Origem.In((int)OrigemLancamentoLiquidacao.BMF.AjusteOperacao,
                                                                               (int)OrigemLancamentoLiquidacao.BMF.AjustePosicao));
                liquidacaoAjusteBMF.Query.Load();
                valoresAjustesBMF = liquidacaoAjusteBMF.Valor.HasValue ? Math.Abs(liquidacaoAjusteBMF.Valor.Value) : 0;
            }
            else
            {
                Liquidacao liquidacaoAjusteBMF = new Liquidacao();
                liquidacaoAjusteBMF.Query.Select(liquidacaoAjusteBMF.Query.Valor.Sum());
                liquidacaoAjusteBMF.Query.Where(liquidacaoAjusteBMF.Query.IdCliente.Equal(idCliente),
                                           liquidacaoAjusteBMF.Query.DataLancamento.LessThanOrEqual(data),
                                           liquidacaoAjusteBMF.Query.DataVencimento.GreaterThan(data),
                                           liquidacaoAjusteBMF.Query.Origem.In((int)OrigemLancamentoLiquidacao.BMF.AjusteOperacao,
                                                                               (int)OrigemLancamentoLiquidacao.BMF.AjustePosicao));
                liquidacaoAjusteBMF.Query.Load();
                valoresAjustesBMF = liquidacaoAjusteBMF.Valor.HasValue ? Math.Abs(liquidacaoAjusteBMF.Valor.Value) : 0;
            }


            valorAtivos += valoresAjustesBMF;

            header.patliq = plFechamento.ToString("N2").Replace(".", "").Replace(",", ".");
            header.valorativos = valorAtivos.ToString("N2").Replace(".", "").Replace(",", ".");
            header.valorpagar = Math.Abs(valoresPagar).ToString("N2").Replace(".", "").Replace(",", ".");
            header.valorreceber = Math.Abs(valoresReceber).ToString("N2").Replace(".", "").Replace(",", ".");

            header.tributos = "0.00";

            return header;
        }

        public int RetornaCodigoLancamento(int origemLiquidacao)
        {
            switch (origemLiquidacao)
            {
                case (int)OrigemLancamentoLiquidacao.Bolsa.CompraAcoes:
                case (int)OrigemLancamentoLiquidacao.Bolsa.CompraOpcoes:
                case (int)OrigemLancamentoLiquidacao.Bolsa.ExercicioCompra:
                case (int)OrigemLancamentoLiquidacao.Bolsa.ExercicioVenda:
                case (int)OrigemLancamentoLiquidacao.Bolsa.Outros:
                case (int)OrigemLancamentoLiquidacao.Bolsa.VendaAcoes:
                case (int)OrigemLancamentoLiquidacao.Bolsa.VendaOpcoes:
                case (int)OrigemLancamentoLiquidacao.Bolsa.Amortizacao: //Jogado aqui por não ter na tabela da Anbima um codigo especifico
                case (int)OrigemLancamentoLiquidacao.Bolsa.CreditoFracoesAcoes: //Jogado aqui por não ter na tabela da Anbima um codigo especifico
                case (int)OrigemLancamentoLiquidacao.Bolsa.OutrosProventos: //Jogado aqui por não ter na tabela da Anbima  um codigo especifico
                case (int)OrigemLancamentoLiquidacao.Bolsa.RestituicaoCapital:  //Jogado aqui por não ter na tabela da Anbima um codigo especifico
                    return 21; //Ações ou Opções Ações
                case (int)OrigemLancamentoLiquidacao.Bolsa.AntecipacaoEmprestimo:
                case (int)OrigemLancamentoLiquidacao.Bolsa.EmprestimoDoado:
                case (int)OrigemLancamentoLiquidacao.Bolsa.EmprestimoTomado:
                    return 31; //Empréstimo Ação
                case (int)OrigemLancamentoLiquidacao.Bolsa.AntecipacaoTermo:
                case (int)OrigemLancamentoLiquidacao.Bolsa.CompraTermo:
                case (int)OrigemLancamentoLiquidacao.Bolsa.VendaTermo:
                    return 23; //Termo Ações
                case (int)OrigemLancamentoLiquidacao.Bolsa.Corretagem:
                    return 36; //Despesa Corretagem Bovespa (somente aplicável se não vier consolidado como Valor Liquido de Bolsa
                case (int)OrigemLancamentoLiquidacao.Bolsa.DespesasTaxas:
                    return 39; //Valor Bovespa
                case (int)OrigemLancamentoLiquidacao.Bolsa.Dividendo:
                    return 27; //Dividendos
                case (int)OrigemLancamentoLiquidacao.Bolsa.JurosCapital:
                    return 28; //Juros s/ Capital Próprio
                case (int)OrigemLancamentoLiquidacao.Bolsa.TaxaCustodia:
                    return 15; //Taxa CUSTÓDIA

                case (int)OrigemLancamentoLiquidacao.BMF.AjusteOperacao:
                case (int)OrigemLancamentoLiquidacao.BMF.AjustePosicao:
                case (int)OrigemLancamentoLiquidacao.BMF.CompraOpcoes:
                case (int)OrigemLancamentoLiquidacao.BMF.CompraVista:
                case (int)OrigemLancamentoLiquidacao.BMF.ExercicioCompra:
                case (int)OrigemLancamentoLiquidacao.BMF.ExercicioVenda:
                case (int)OrigemLancamentoLiquidacao.BMF.VendaOpcoes:
                case (int)OrigemLancamentoLiquidacao.BMF.VendaVista:
                case (int)OrigemLancamentoLiquidacao.BMF.Outros:
                    return 22; //Derivativos (Opções Deriv. ou Flexíveis ou Futuros)
                case (int)OrigemLancamentoLiquidacao.BMF.Corretagem:
                    return 37; //Despesa Corretagem BMF (somente aplicável se não vier consolidado como Valor Liquido de BMF
                case (int)OrigemLancamentoLiquidacao.BMF.DespesasTaxas:
                case (int)OrigemLancamentoLiquidacao.BMF.OutrosCustos:
                case (int)OrigemLancamentoLiquidacao.BMF.Registro:
                case (int)OrigemLancamentoLiquidacao.BMF.TaxaPermanencia:
                    return 41; //Valor BMF
                case (int)OrigemLancamentoLiquidacao.BMF.Emolumento:
                    return 38; //Emolumentos

                case (int)OrigemLancamentoLiquidacao.RendaFixa.Juros:
                    return 30; //Juros (RF)

                case (int)OrigemLancamentoLiquidacao.Provisao.TaxaAdministracao:
                    return 34; //Taxa Administração
                case (int)OrigemLancamentoLiquidacao.Provisao.TaxaPerformance:
                    return 35; //Taxa Administração
                case (int)OrigemLancamentoLiquidacao.Provisao.ProvisaoOutros:
                    return 8; //Outras Despesas Administrativas
                case (int)OrigemLancamentoLiquidacao.Provisao.TaxaFiscalizacaoCVM:
                    return 14; //Taxa CVM

                default:
                    return 0; //Não encontrado!
                    break;
            }
        }
    }
}