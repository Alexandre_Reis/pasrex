﻿using System.Collections.Generic;
using System;
using Financial.Util;
using System.IO;
using Financial.Fundo;
using Financial.Common;
using Financial.Investidor;
using Financial.Investidor.Enums;
using System.Text;
using Financial.Contabil;

namespace Financial.Export {

    /// <summary>
    ///  Classe para fazer as Exportações Pertencentes a Fundo
    /// </summary>
    public class ContabilFolhamatic
    {
        /// <summary>
        /// Determina o numero total de casas que um numero deve ter com zeros a esquerda
        /// </summary>
        private enum AlinhamentoEsquerda 
        {
            [StringValue("000000000000")]
            _12Casas = 1
        }

        /// <summary>
        /// Valores de Detalhe
        /// </summary>
        #region Valores do Detalhe
        class ValoresDetail {
            public string contaDebito;               // Campo 02
            public string contaCredito;              // Campo 03            
            public string codigoHistorico;           // Campo 04                          
            public Int64 valor;                    // Campo 05
            public string dataLancamento;            // Campo 06
            public string complementoHistorico;      // Campo 08
            public string nomeDigitador;             // Campo 11
            public string tipoMovimento;             // Campo 12
            public string contaDebitoCentroCustos;   // Campo 13
            public string valorDebito;              // Campo 14
            public string contaCreditoCentroCustos;  // Campo 15
            public string valorCredito;             // Campo 16
            public string tipoDigitacao;             // Campo 17
            public string codigoParticipante;        // Campo 17
        }
        private List<ValoresDetail> valoresDetail = new List<ValoresDetail>();
        #endregion

        /// <summary>
        /// Retorna uma Collection de Stream de Mémoria de arquivos. A Stream permanece aberta
        /// </summary>
        /// <param name="dataContabil"></param>
        /// <param name="idClientes">Lista de IdClientes</param>
        /// <param name="nomeArquivo">Saida: Lista com os nomes dos arquivos a gerar</param>
        /// <returns>Streams de Mémoria dos Arquivos Gerados</returns>
        /// <exception cref="ProcessaAtivosClubesException">
        /// throws ProcessaContabilException se ocorreu algum erro no Processamento
        /// </exception>
        public MemoryStream[] ExportaContabilFolhamatic(DateTime dataContabil, List<int> idClientes, out List<string> nomeArquivo) {

            #region Busca os Clientes com Contabil = "S"
            ClienteCollection clienteCollection = new ClienteCollection();
            clienteCollection.Query.Where(clienteCollection.Query.CalculaContabil.Equal("S"));
            // Acrescentado filtro de idClientes
            clienteCollection.Query.Where(clienteCollection.Query.IdCliente.In(idClientes));
            clienteCollection.Query.Load();
            #endregion

            // Percorre de tras para frente para remover quem nao possue clienteinterface.codigocontabil definido
            for (int i = clienteCollection.Count-1; i>=0; i--) {
                ClienteInterface c1 = new ClienteInterface();
                if (c1.LoadByPrimaryKey(clienteCollection[i].IdCliente.Value)) {
                    if(String.IsNullOrEmpty(c1.CodigoContabil)) {
                        clienteCollection.DetachEntity(clienteCollection[i]);
                    }
                }
                else {
                    clienteCollection.DetachEntity(clienteCollection[i]);
                }
            }

            // Tamanho = numero de Clientes
            nomeArquivo = new List<string>(clienteCollection.Count);
            MemoryStream[] streamRetorno = new MemoryStream[clienteCollection.Count];

            if (clienteCollection.HasData) {
                int j = 0;
                //
                foreach (Cliente cliente in clienteCollection) {                    
                    int idCliente = cliente.IdCliente.Value;

                    StringBuilder nomeArquivoAux = new StringBuilder();
                    string mes = dataContabil.Month.ToString();
                    if (mes.Length == 1) {
                        mes = "0" + mes;
                    }

                    nomeArquivoAux.Append(idCliente.ToString() + "_FI00220022." + mes);
                    //
                    nomeArquivo.Add(nomeArquivoAux.ToString());
                    //
                    DateTime dataInicio = Calendario.RetornaPrimeiroDiaCorridoMes(dataContabil, 0);
                    DateTime dataFim = Calendario.RetornaUltimoDiaCorridoMes(dataContabil, 0);

                    ContabLancamentoCollection contabLancamentoCollection = new ContabLancamentoCollection();
                    contabLancamentoCollection.Query.Where(contabLancamentoCollection.Query.IdCliente.Equal(idCliente),
                                                           contabLancamentoCollection.Query.DataLancamento.GreaterThanOrEqual(dataInicio),
                                                           contabLancamentoCollection.Query.DataLancamento.LessThanOrEqual(dataFim));
                    contabLancamentoCollection.Query.Load();

                    #region Pega Valores Table
                    this.valoresDetail.Clear();

                    // Cada Posicao Representa uma linha no Arquivo
                    foreach (ContabLancamento contabLancamento in contabLancamentoCollection) {
                        DateTime dataLancamento = contabLancamento.DataLancamento.Value;
                        string contaDebito = contabLancamento.ContaDebito;
                        string contaCredito = contabLancamento.ContaCredito;                        

                        ContabRoteiroCollection contabRoteiroCollection = new ContabRoteiroCollection();
                        contabRoteiroCollection.Query.Select(contabRoteiroCollection.Query.ContaDebitoReduzida, 
                                                             contabRoteiroCollection.Query.ContaCreditoReduzida);
                        contabRoteiroCollection.Query.Where(contabRoteiroCollection.Query.ContaDebito.Equal(contaDebito));
                        contabRoteiroCollection.Query.Load();

                        contaDebito = "";
                        if (contabRoteiroCollection.Count > 0) {
                            contaDebito = contabRoteiroCollection[0].ContaDebitoReduzida;
                            contaDebito = contaDebito.Replace(".", "").Replace("/", "").Replace("-", "");
                        }

                        contaCredito = "";
                        if (contabRoteiroCollection.Count > 0) {
                            contaCredito = contabRoteiroCollection[0].ContaCreditoReduzida;
                            contaCredito = contaCredito.Replace(".", "").Replace("/", "").Replace("-", "");
                        }

                        string descricao = contabLancamento.Descricao;
                        decimal valor = contabLancamento.Valor.Value;
                        
                        ValoresDetail valorDetailAux = new ValoresDetail();
                        valorDetailAux.codigoHistorico = Utilitario.InsertString("0", 4) + " "; //Por ora não tem codigo exportado
                        valorDetailAux.codigoParticipante = Utilitario.InsertString(" ", 5);
                        valorDetailAux.complementoHistorico = descricao.Trim().PadRight(106).ToUpper();
                        valorDetailAux.contaCredito = contaCredito.Trim().PadRight(18);
                        valorDetailAux.contaDebito = contaDebito.Trim().PadRight(18);
                        valorDetailAux.contaCreditoCentroCustos = Utilitario.InsertString(" ", 20);
                        valorDetailAux.contaDebitoCentroCustos = Utilitario.InsertString(" ", 20);
                        valorDetailAux.dataLancamento = dataLancamento.ToShortDateString();
                        valorDetailAux.nomeDigitador = Utilitario.InsertString(" ", 20);
                        valorDetailAux.tipoDigitacao = "N";
                        valorDetailAux.tipoMovimento = "8" + Utilitario.InsertString(" ", 19);
                        valorDetailAux.valor = Convert.ToInt64(valor * 100M);
                        valorDetailAux.valorCredito = Utilitario.InsertString(" ", 15);
                        valorDetailAux.valorDebito = Utilitario.InsertString(" ", 15);
                        
                        // Adiciona os dados referentes a uma carteira
                        this.valoresDetail.Add(valorDetailAux);
                    }
                    #endregion                    

                    #region EscreveArquivo
                    try {
                        // Abrir o arquivo
                        streamRetorno[j] = new MemoryStream();
                        StreamWriter arquivo = new StreamWriter(streamRetorno[j], Encoding.ASCII);

                        for (int i = 0; i < this.valoresDetail.Count; i++) {
                            this.EscreveDetalhe(arquivo, i);
                        }

                        //Fecha o arquivo
                        arquivo.Flush();

                        // Retorna a MemoryStream para o inicio
                        streamRetorno[j].Seek(0, SeekOrigin.Begin);
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine(e.Message);
                        string mensagem = "";
                        mensagem = "Arquivo Contábil " + nomeArquivo[j] + " não Exportado ";
                        mensagem += "\n\t - " + e.Message;

                        throw new ProcessaContabilException(mensagem);
                    }
                    #endregion

                    j++;
                }
            }
            // 0 registros
            else {
                throw new ProcessaContabilException("Não existem dados para exportação.");
            }

            return streamRetorno;
        }
                
        /// <summary>
        /// Escreve no Arquivo o objeto ValoresDetail
        /// </summary>
        /// <param name="arquivo"></param>
        /// <param name="linha">Numero da Linha de detalhe a escrever começando de 0</param>
        private void EscreveDetalhe(StreamWriter arquivo, int linha) {
            //
            
            arquivo.Write(Utilitario.InsertString(" ", 5));
            arquivo.Write(this.valoresDetail[linha].contaDebito);
            arquivo.Write(this.valoresDetail[linha].contaCredito);
            arquivo.Write(this.valoresDetail[linha].codigoHistorico);
            arquivo.Write((this.valoresDetail[linha].valor).ToString().PadLeft(12, '0')); // Completa com zeros a esquerda
            arquivo.Write(this.valoresDetail[linha].dataLancamento);
            arquivo.Write(Utilitario.InsertString(" ", 6));
            arquivo.Write(this.valoresDetail[linha].complementoHistorico);
            arquivo.Write(Utilitario.InsertString(" ", 37));
            arquivo.Write(this.valoresDetail[linha].nomeDigitador);
            arquivo.Write(this.valoresDetail[linha].tipoMovimento);
            arquivo.Write(this.valoresDetail[linha].contaDebitoCentroCustos);
            arquivo.Write(this.valoresDetail[linha].valorDebito);
            arquivo.Write(this.valoresDetail[linha].contaCreditoCentroCustos);
            arquivo.Write(this.valoresDetail[linha].valorCredito);
            arquivo.Write(this.valoresDetail[linha].tipoDigitacao);            
            
            //
            // Utima Linha não imprime
            if (linha != this.valoresDetail.Count - 1) {
                arquivo.WriteLine();
            }
        }
    }
}