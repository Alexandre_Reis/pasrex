﻿using System.Xml.Serialization;
using System.Xml;
using System;
using System.IO;
using EntitySpaces.Interfaces;
using System.Collections.Generic;
using Financial.CRM;
using Financial.Investidor;
using Financial.Investidor.Enums;
using Financial.Relatorio;
using Financial.Fundo;
using Financial.Util;
using Financial.InvestidorCotista;
using Financial.ContaCorrente;

namespace Financial.Export 
{
    public class InfoComplementar  
    {      
        /// <summary>
        /// Retorna uma Stream de Mémoria de um arquivo xml. A Stream permanece aberta
        /// </summary>
        /// <param name="dataPosicao"></param>
        /// <param name="idClientes"></param>
        /// <param name="dicMStream">Saida: Dicionario contendo Nome do arquivo e Memory Stream do arquivo info Complementar</param>
        /// <returns></returns>
        public void ExportaInfoComplementar(DateTime data, List<int> idClientes, out Dictionary<string, MemoryStream> dicMStream) {
            dicMStream = new Dictionary<string, MemoryStream>();

            for (int i = 0; i < idClientes.Count; i++) {
                //
                MemoryStream ms = new MemoryStream();

                string nomeArquivo = idClientes[i] + "_" + data.ToString("ddmmyyyy") + ".pdf";
                //
                #region Gera o PDF
                ReportInfoComplementarExportacao report = new ReportInfoComplementarExportacao(idClientes[i], data);
                report.ExportToPdf(ms);
                
                /* Necessário voltar o ponteiro do arquivo para o Inicio */
                ms.Seek(0, SeekOrigin.Begin);
                //
                #endregion
                //
                // Adiciona no Dicionário
                dicMStream.Add(nomeArquivo, ms);        
            }           
        }     
   }


   [Serializable()]
    [XmlRootAttribute(ElementName = "DOC_ARQ", IsNullable = false, Namespace = "urn:infocomp")]
   public class XML_InfoComplementar {

       [XmlElement(ElementName = "CAB_INFORM")]
       public Cabecalho cabecalho = new Cabecalho();

       [XmlArray("LISTA_INFORM")]
       [XmlArrayItem("INFORM")]
       public List<Corpo> corpo = new List<Corpo>();

       #region Cabecalho
       public class Cabecalho {

           public Cabecalho() { }

           [XmlElement("COD_DOC", Order = 1)]
           public int codigoDocumento;

           [XmlElement("VERSAO", Order = 2)]
           public string versao;

           #region DT_COMPT
           [XmlIgnore]
           public DateTime dataCompetencia;

           [XmlElement("DT_COMPT", Order = 3)]
           public string formatDataCompetencia {
               get { return this.dataCompetencia.ToString("dd/MM/yyyy"); }
               set { dataCompetencia = DateTime.Parse(value); }
           }
           #endregion

           #region DT_GERAC_ARQ
           [XmlIgnore]
           public DateTime dataGeracaoArquivo;

           [XmlElement("DT_GERAC_ARQ", Order = 4)]
           public string formatDataGeracaoArquivo {
               get { return this.dataGeracaoArquivo.ToString("dd/MM/yyyy"); }
               set { dataGeracaoArquivo = DateTime.Parse(value); }
           }
           #endregion


       }
       #endregion

       #region Corpo
       public class Corpo {

           public Corpo() { }

           /**********************************************/
           [XmlElement("CNPJ_FDO", Order = 1)]
           public string cnpj;

           [XmlElement("PERIODIC_MIN_DIVULG_COMPOS_CART", Order = 2)]
           public string periodicidade;

           #region LISTA_LOCAL_MEIO_FORMA_DIVULG_INF
           [XmlElementAttribute("LISTA_LOCAL_MEIO_FORMA_DIVULG_INF", Order = 3)]
           public LISTA_LOCAL_MEIO_FORMA_DIVULG_INF lista_local_meio_forma_divulg_inf = new LISTA_LOCAL_MEIO_FORMA_DIVULG_INF();

           [XmlIgnore]
           public LISTA_LOCAL_MEIO_FORMA_DIVULG_INF getLISTA_LOCAL_MEIO_FORMA_DIVULG_INF {
               get { return lista_local_meio_forma_divulg_inf; }
               set { lista_local_meio_forma_divulg_inf = value; }
           }           
           #endregion

           #region LISTA_LOCAL_MEIO_FORMA_DIVULG_INF_COTISTA
           [XmlElementAttribute("LISTA_LOCAL_MEIO_FORMA_DIVULG_INF_COTISTA", Order = 4)]
           public LISTA_LOCAL_MEIO_FORMA_DIVULG_INF_COTISTA lista_local_meio_forma_divulg_inf_cotista = new LISTA_LOCAL_MEIO_FORMA_DIVULG_INF_COTISTA();

           [XmlIgnore]
           public LISTA_LOCAL_MEIO_FORMA_DIVULG_INF_COTISTA getLISTA_LOCAL_MEIO_FORMA_DIVULG_INF_COTISTA {
               get { return lista_local_meio_forma_divulg_inf_cotista; }
               set { lista_local_meio_forma_divulg_inf_cotista = value; }
           }
           #endregion

           [XmlElement("EXPOSIC_RELEV_FAT_RISC", Order = 5)]
           public string fatoresRisco;

           #region DS_POLIT_EXERC_VOTO_ATIV_FINANC
           [XmlElementAttribute("DS_POLIT_EXERC_VOTO_ATIV_FINANC", Order = 6)]
           public DS_POLIT_EXERC_VOTO_ATIV_FINANC ds_polit_exerc_voto_ativ_financ = new DS_POLIT_EXERC_VOTO_ATIV_FINANC();

           [XmlIgnore]
           public DS_POLIT_EXERC_VOTO_ATIV_FINANC getDS_POLIT_EXERC_VOTO_ATIV_FINANC {
               get { return ds_polit_exerc_voto_ativ_financ; }
               set { ds_polit_exerc_voto_ativ_financ = value; }
           }
           #endregion

           [XmlElement("DS_TRIBUT_POLIT_TRIBUT", Order = 7)]
           public string tributacaoAplicavel;

           [XmlElement("DS_POLIT_ADM_RISC", Order = 8)]
           public string politicaAdministracaoRisco;
           
           #region IDENT_CLASSIF_RISCO
           [XmlElementAttribute("IDENT_CLASSIF_RISCO", Order = 9)]
           public IDENT_CLASSIF_RISCO ident_classif_risco = new IDENT_CLASSIF_RISCO();

           [XmlIgnore]
           public IDENT_CLASSIF_RISCO getIDENT_CLASSIF_RISCO {
               get { return ident_classif_risco; }
               set { ident_classif_risco = value; }
           }
           #endregion

           #region APRES_ADM_GEST
           [XmlElementAttribute("APRES_ADM_GEST", Order = 10)]
           public APRES_ADM_GEST apres_adm_gest = new APRES_ADM_GEST();

           [XmlIgnore]
           public APRES_ADM_GEST getAPRES_ADM_GEST {
               get { return apres_adm_gest; }
               set { apres_adm_gest = value; }
           }
           #endregion
          
           #region LISTA_PREST_SERVIC
           [XmlElementAttribute("LISTA_PREST_SERVIC", Order = 11)]
           public LISTA_PREST_SERVIC lista_prest_servic = new LISTA_PREST_SERVIC();

           [XmlIgnore]
           public LISTA_PREST_SERVIC getLISTA_PREST_SERVIC {
               get { return lista_prest_servic; }
               set { lista_prest_servic = value; }
           }
           #endregion

           #region POLIT_DISTR_COTA
           [XmlElementAttribute("POLIT_DISTR_COTA", Order = 12)]
           public POLIT_DISTR_COTA polit_distr_cota = new POLIT_DISTR_COTA();

           [XmlIgnore]
           public POLIT_DISTR_COTA getPOLIT_DISTR_COTA {
               get { return polit_distr_cota; }
               set { polit_distr_cota = value; }
           }
           #endregion

           #region OUTR_INFORM_RELEV
           [XmlElementAttribute("OUTR_INFORM_RELEV", Order = 13)]
           public OUTR_INFORM_RELEV outr_inform_relev = new OUTR_INFORM_RELEV();

           [XmlIgnore]
           public OUTR_INFORM_RELEV getOUTR_INFORM_RELEV {
               get { return outr_inform_relev; }
               set { outr_inform_relev = value; }
           }
           #endregion
       }
       #endregion

       #region LISTA_LOCAL_MEIO_FORMA_DIVULG_INF em corpo
       public class LISTA_LOCAL_MEIO_FORMA_DIVULG_INF {
           // Construtor
           public LISTA_LOCAL_MEIO_FORMA_DIVULG_INF() { }

           [XmlElementAttribute("LOCAL_MEIO_FORMA_DIVULG_INF")]
           public List<LOCAL_MEIO_FORMA_DIVULG_INF> local_meio_forma_divulg_info = new List<LOCAL_MEIO_FORMA_DIVULG_INF>();

           [XmlIgnore]
           public List<LOCAL_MEIO_FORMA_DIVULG_INF> getLOCAL_MEIO_FORMA_DIVULG_INF {
               get { return local_meio_forma_divulg_info; }
               set { local_meio_forma_divulg_info = value; }
           }
       }
       #endregion

       #region LOCAL_MEIO_FORMA_DIVULG_INF em LISTA_LOCAL_MEIO_FORMA_DIVULG_INF
       public class LOCAL_MEIO_FORMA_DIVULG_INF {
           // Construtor
           public LOCAL_MEIO_FORMA_DIVULG_INF() { }

           [XmlElement("DS_LOCAL_DIVULG", Order = 1)]
           public string DS_LOCAL_DIVULG;

           [XmlElement("COD_MEIO_DIVULG", Order = 2)]
           public string COD_MEIO_DIVULG;

           [XmlElement("DS_FORMA_DIVULG", Order = 3)]
           public string localFormaDivulgacao;           
       }
       #endregion

       /* ----------------------------------------------------------- */

       #region LISTA_LOCAL_MEIO_FORMA_DIVULG_INF_COTISTA em corpo
       public class LISTA_LOCAL_MEIO_FORMA_DIVULG_INF_COTISTA {
           // Construtor
           public LISTA_LOCAL_MEIO_FORMA_DIVULG_INF_COTISTA() { }
           
           [XmlElementAttribute("LOCAL_MEIO_FORMA_DIVULG_INF_COTISTA")]
           public List<LOCAL_MEIO_FORMA_DIVULG_INF_COTISTA> local_meio_forma_divulg_inf_cotista = new List<LOCAL_MEIO_FORMA_DIVULG_INF_COTISTA>();

           [XmlIgnore]
           public List<LOCAL_MEIO_FORMA_DIVULG_INF_COTISTA> getLOCAL_MEIO_FORMA_DIVULG_INF_COTISTA {
               get { return local_meio_forma_divulg_inf_cotista; }
               set { local_meio_forma_divulg_inf_cotista = value; }
           }
       }
       #endregion

       #region LOCAL_MEIO_FORMA_DIVULG_INF_COTISTA em LISTA_LOCAL_MEIO_FORMA_DIVULG_INF_COTISTA
       public class LOCAL_MEIO_FORMA_DIVULG_INF_COTISTA {
           // Construtor
           public LOCAL_MEIO_FORMA_DIVULG_INF_COTISTA() { }

           [XmlElement("DS_RESP", Order = 1)]
           public string DS_RESP;

           [XmlElement("DS_LOCAL", Order = 2)]
           public string DS_LOCAL;

           [XmlElement("COD_MEIO", Order = 3)]
           public string COD_MEIO;

           [XmlElement("DS_FORMA_SOLIC", Order = 4)]
           public string DS_FORMA_SOLIC;
       }
       #endregion

       /* ----------------------------------------------------------- */

       #region DS_POLIT_EXERC_VOTO_ATIV_FINANC em corpo
       public class DS_POLIT_EXERC_VOTO_ATIV_FINANC {
           // Construtor
           public DS_POLIT_EXERC_VOTO_ATIV_FINANC() { }

           //TODO
           [XmlElement("COD_VOTO_GEST_ASSEMB", Order = 1)]
           public string COD_VOTO_GEST_ASSEMB;

           [XmlElement("DS_POLIT_EXERC_VOTO", Order = 2)]
           public string politicaExercicioVoto;
       }
       #endregion       

       #region IDENT_CLASSIF_RISCO em corpo
       public class IDENT_CLASSIF_RISCO {
           // Construtor
           public IDENT_CLASSIF_RISCO() { }

           //TODO
           [XmlElement("AGENC_CLASSIF_RATIN", Order = 1)]
           public string AGENC_CLASSIF_RATIN;

           //TODO
           [XmlElement("NR_CNPJ", Order = 2)]
           public string NR_CNPJ;

           [XmlElement("NM_PREST", Order = 3)]
           public string NM_PREST;

           [XmlElement("CLASSIF_AGENC_RISCO", Order = 4)]
           public string CLASSIF_AGENC_RISCO;

           [XmlElement("DISCL_ADVERT", Order = 5)]
           public string DISCL_ADVERT;
       }
       #endregion       

       #region APRES_ADM_GEST
       public class APRES_ADM_GEST {
           // Construtor
           public APRES_ADM_GEST() { }

           // TODO - não existe
           [XmlElement("APRES_DETALHE_ADM", Order = 1)]          
           public string recursosServicosAdm; 

           [XmlElement("APRES_DETALHE_GEST", Order = 2)]
           public string recursosServicosGestor;

           //[XmlElementAttribute("COTST_SIGNIF")]
           //public List<COTST_SIGNIF> cotst_SIGNIF = new List<COTST_SIGNIF>();

           //[XmlIgnore]
           //public List<COTST_SIGNIF> getCOTST_SIGNIF {
           //    get { return cotst_SIGNIF; }
           //    set { cotst_SIGNIF = value; }
           //}
       }
       #endregion

       /* ----------------------------------------------------------- */

       #region LISTA_PREST_SERVIC em corpo
       public class LISTA_PREST_SERVIC {
           // Construtor
           public LISTA_PREST_SERVIC() { }

           [XmlElementAttribute("PREST_SERVIC")]
           public List<PREST_SERVIC> prest_Servic = new List<PREST_SERVIC>();

           [XmlIgnore]
           public List<PREST_SERVIC> getPREST_SERVICA {
               get { return prest_Servic; }
               set { prest_Servic = value; }
           }
       }
       #endregion

       #region PREST_SERVIC em LISTA_PREST_SERVIC
       public class PREST_SERVIC {
           // Construtor
           public PREST_SERVIC() { }

           [XmlElement("DS_SERVICO_PRESTADO", Order = 1)]
           public string DS_SERVICO_PRESTADO;

           [XmlElement("NM_PREST", Order = 2)]
           public string NM_PREST;
       }
       #endregion

       /* ----------------------------------------------------------- */

       #region POLIT_DISTR_COTA em corpo
       public class POLIT_DISTR_COTA {
           // Construtor
           public POLIT_DISTR_COTA() { }

           // TODO
           [XmlElement("COD_DISTR_OFERTA_PUB", Order = 1)]
           public string COD_DISTR_OFERTA_PUB;

           [XmlElement("DS_DETALHE_POLIT_DISTR_COTA", Order = 2)]
           public string politicaDistribuicaoCotas;

       }
       #endregion

       #region OUTR_INFORM_RELEV em corpo
       public class OUTR_INFORM_RELEV {
           // Construtor
           public OUTR_INFORM_RELEV() { }

           // TODO
           [XmlElement("INFORM_AUTOREGUL_ANBIMA", Order = 1)]
           public string INFORM_AUTOREGUL_ANBIMA;

           [XmlElement("INFORM_RELEV", Order = 2)]
           public string observacoes;
       }
       #endregion       
   }

    public class InfoComplementarXML {

       /// <summary>
       /// Retorna uma Stream de Mémoria de um arquivo xml. A Stream permanece aberta
       /// </summary>
       /// <param name="dataPosicao"></param>
       /// <param name="idClientes">lista de idClientes</param>
       /// <param name="ms">Saida: Memory Stream do arquivo InformeDiario</param>
       /// <param name="nomeArquivo">Saida: Nome do Arquivo InformeDiario</param>        
       /// <returns></returns>
        public void ExportaInfoComplementarXML(DateTime dataPosicao, List<int> idClientes, out MemoryStream ms, out string nomeArquivo) {
           ms = new MemoryStream();
           nomeArquivo = "InformacoesComplementares.xml";
           //            
           XML_InfoComplementar.Cabecalho c = this.MontaCabecalho(dataPosicao);
           List<XML_InfoComplementar.Corpo> conteudo = this.MontaConteudo(dataPosicao, idClientes);
           //
           XML_InfoComplementar arq = new XML_InfoComplementar();
           arq.cabecalho = c;
           arq.corpo.AddRange(conteudo);
           //                     
           XmlSerializer x = new XmlSerializer(arq.GetType());
           //x.Serialize(ms, arq);

           XmlWriterSettings settings = new XmlWriterSettings();
           settings.Encoding = System.Text.Encoding.GetEncoding("WINDOWS-1252");
           settings.Indent = true;
           settings.IndentChars = "\t";
           settings.NewLineChars = Environment.NewLine;
           settings.ConformanceLevel = ConformanceLevel.Document;

           using (XmlWriter writer = XmlTextWriter.Create(ms, settings)) {
               x.Serialize(writer, arq);
           }
       }

       /// <summary>
       /// Monta o Cabeçalho do arquivo XML
       /// </summary>
       /// <param name="dataPosicao"></param>
       /// <returns></returns>
        private XML_InfoComplementar.Cabecalho MontaCabecalho(DateTime dataPosicao) {
            XML_InfoComplementar.Cabecalho c = new XML_InfoComplementar.Cabecalho();
           //
           c.codigoDocumento = 1;
           c.dataCompetencia = dataPosicao;
           c.dataGeracaoArquivo = DateTime.Now;
           c.versao = "1.0";
           //
           return c;
       }

       /// <summary>
       /// Monta o conteudo do arquivo XML
       /// </summary>
       /// <param name="dataPosicao"></param>
       /// <param name="idClientes">Lista de IdClientes</param>
       /// <returns></returns>
        private List<XML_InfoComplementar.Corpo> MontaConteudo(DateTime dataPosicao, List<int> idClientes) {
            List<XML_InfoComplementar.Corpo> lista = new List<XML_InfoComplementar.Corpo>();

            //
            #region Consulta Inicial
            PessoaQuery pessoaQuery = new PessoaQuery("P");
            ClienteQuery clienteQuery = new ClienteQuery("C");
            //CarteiraQuery carteiraQuery = new CarteiraQuery("A");
            
            //
            ClienteCollection clienteCollection = new ClienteCollection();
            //
            clienteQuery.Select(clienteQuery, pessoaQuery.Cpfcnpj);
            //
            clienteQuery.InnerJoin(pessoaQuery).On(pessoaQuery.IdPessoa == clienteQuery.IdCliente);
            //clienteQuery.InnerJoin(carteiraQuery).On(carteiraQuery.IdCarteira == clienteQuery.IdCliente);
            //
            //clienteQuery.Where(clienteQuery.TipoControle == (byte)TipoControleCliente.Completo &&
            //                   clienteQuery.IdTipo.In((int)TipoClienteFixo.Fundo) &&
            //                   clienteQuery.IdCliente.In(idClientes));
            ////
            clienteQuery.Where(clienteQuery.IdCliente.In(idClientes));
            //
            clienteQuery.OrderBy(pessoaQuery.Cpfcnpj.Descending);
            //
            clienteCollection.Load(clienteQuery);
            //
            #endregion

           //
            for (int i = 0; i < clienteCollection.Count; i++) {
                int idCliente = clienteCollection[i].IdCliente.Value;
                //DateTime dataDia = clienteCollection[i].DataDia.Value;

                Pessoa pessoa = new Pessoa();
                pessoa.LoadByPrimaryKey(clienteCollection[i].IdPessoa.Value);

                #region InformacoesComplementaresFundo
                InformacoesComplementaresFundoCollection infoCollection = new InformacoesComplementaresFundoCollection();

                // Busca todas as informações menores que a data de referencia ordenado decrescente pela data
                infoCollection.Query
                     .Where(infoCollection.Query.DataInicioVigencia.LessThanOrEqual(dataPosicao),
                            infoCollection.Query.IdCarteira == idCliente)
                     .OrderBy(infoCollection.Query.DataInicioVigencia.Descending);

                infoCollection.Query.Load();
                
                InformacoesComplementaresFundo info = new InformacoesComplementaresFundo();
                bool existeInfo = infoCollection.HasData;
                if (existeInfo) {
                    info = infoCollection[0];
                }
                #endregion

                #region Conteudo arquivo XML
                //
                XML_InfoComplementar.Corpo corpo = new XML_InfoComplementar.Corpo();
                //
                corpo.cnpj = pessoa.Cpfcnpj.Replace(".", "").Replace("-", "").Replace("/", "").Trim().PadLeft(14, '0');
                //                                               
                corpo.periodicidade = !String.IsNullOrEmpty(info.Periodicidade) ? info.Periodicidade.Trim() : ""; 
                
                // campo 3 - Lista
                this.ControeLISTA_LOCAL_MEIO_FORMA_DIVULG_INF(dataPosicao, idCliente, corpo, info);

                // campo 4
                this.ControeLISTA_LOCAL_MEIO_FORMA_DIVULG_INF_COTISTA(dataPosicao, idCliente, corpo, info);

                // campo 5
                corpo.fatoresRisco = !String.IsNullOrEmpty(info.FatoresRisco) ? info.FatoresRisco.Trim() : ""; 

                // campo 6 - Lista
                this.ControeDS_POLIT_EXERC_VOTO_ATIV_FINANC(dataPosicao, idCliente, corpo, info);

                // campo 7
                corpo.tributacaoAplicavel = !String.IsNullOrEmpty(info.TributacaoAplicavel) ? info.TributacaoAplicavel.Trim() : "";

                // campo 8
                corpo.politicaAdministracaoRisco = !String.IsNullOrEmpty(info.PoliticaAdministracaoRisco) ? info.PoliticaAdministracaoRisco.Trim() : "";

                // campo 9 - Lista
                this.ControeIDENT_CLASSIF_RISCO(dataPosicao, idCliente, corpo, info);

                // campo 10 - Lista
                this.ControeAPRES_ADM_GEST(dataPosicao, idCliente, corpo, info);
                
                // campo 11 - Lista
                this.ControeLISTA_PREST_SERVIC(dataPosicao, idCliente, corpo, info);

                // campo 12 - Lista
                this.ControePOLIT_DISTR_COTA(dataPosicao, idCliente, corpo, info);

                // campo 13 - Lista
                this.ControeOUTR_INFORM_RELEV(dataPosicao, idCliente, corpo, info);

                lista.Add(corpo);
                //
                #endregion
            }
           //
           return lista;
       }

       /// <summary>
       /// Constroe o objeto LISTA_LOCAL_MEIO_FORMA_DIVULG_INF
       /// </summary>
       /// <param name="dataPosicao"></param>
       /// <param name="idCliente"></param>
       /// <param name="corpo">Objeto representativo do Corpo do arquivo XML</param>
        private void ControeLISTA_LOCAL_MEIO_FORMA_DIVULG_INF(DateTime dataPosicao, int idCliente, XML_InfoComplementar.Corpo corpo, InformacoesComplementaresFundo info) {

            #region Controe LOCAL_MEIO_FORMA_DIVULG_INF em LISTA_LOCAL_MEIO_FORMA_DIVULG_INF
            
            XML_InfoComplementar.LOCAL_MEIO_FORMA_DIVULG_INF elem = new XML_InfoComplementar.LOCAL_MEIO_FORMA_DIVULG_INF();
            //
            elem.localFormaDivulgacao = !String.IsNullOrEmpty(info.LocalFormaDivulgacao) ? info.LocalFormaDivulgacao.Trim() : "";
            elem.COD_MEIO_DIVULG = !String.IsNullOrEmpty(info.CodMeioDivulg) ? info.CodMeioDivulg.Trim() : "";
            elem.DS_LOCAL_DIVULG = !String.IsNullOrEmpty(info.DsLocalDivulg) ? info.DsLocalDivulg.Trim() : "";
            //
            corpo.getLISTA_LOCAL_MEIO_FORMA_DIVULG_INF.getLOCAL_MEIO_FORMA_DIVULG_INF.Add(elem);

            #endregion
        }

        /// <summary>
        /// Constroe o objeto LISTA_LOCAL_MEIO_FORMA_DIVULG_INF_COTISTA
        /// </summary>
        /// <param name="dataPosicao"></param>
        /// <param name="idCliente"></param>
        /// <param name="corpo">Objeto representativo do Corpo do arquivo XML</param>
        private void ControeLISTA_LOCAL_MEIO_FORMA_DIVULG_INF_COTISTA(DateTime dataPosicao, int idCliente, XML_InfoComplementar.Corpo corpo, InformacoesComplementaresFundo info) {

            #region Controe LOCAL_MEIO_FORMA_DIVULG_INF_COTISTA em LISTA_LOCAL_MEIO_FORMA_DIVULG_INF_COTISTA

            XML_InfoComplementar.LOCAL_MEIO_FORMA_DIVULG_INF_COTISTA elem = new XML_InfoComplementar.LOCAL_MEIO_FORMA_DIVULG_INF_COTISTA();
            //
            elem.DS_FORMA_SOLIC = !String.IsNullOrEmpty(info.LocalFormaSolicitacaoCotista) ? info.LocalFormaSolicitacaoCotista.Trim() : "";
            elem.DS_RESP = !String.IsNullOrEmpty(info.DsResp) ? info.DsResp.Trim() : "";
            elem.DS_LOCAL = !String.IsNullOrEmpty(info.DsLocal) ? info.DsLocal.Trim() : "";
            elem.COD_MEIO = !String.IsNullOrEmpty(info.CodMeio) ? info.CodMeio.Trim() : "";
            //
            corpo.getLISTA_LOCAL_MEIO_FORMA_DIVULG_INF_COTISTA.getLOCAL_MEIO_FORMA_DIVULG_INF_COTISTA.Add(elem);

            #endregion
        }

        /// <summary>
        /// Constroe o objeto DS_POLIT_EXERC_VOTO_ATIV_FINANC
        /// </summary>
        /// <param name="dataPosicao"></param>
        /// <param name="idCliente"></param>
        /// <param name="corpo">Objeto representativo do Corpo do arquivo XML</param>
        private void ControeDS_POLIT_EXERC_VOTO_ATIV_FINANC(DateTime dataPosicao, int idCliente, XML_InfoComplementar.Corpo corpo, InformacoesComplementaresFundo info) {

            #region Controe DS_POLIT_EXERC_VOTO_ATIV_FINANC

            //XML_InfoComplementar.DS_POLIT_EXERC_VOTO_ATIV_FINANC elem = new XML_InfoComplementar.DS_POLIT_EXERC_VOTO_ATIV_FINANC();
            //
            //elem.DS_FORMA_SOLIC = !String.IsNullOrEmpty(info.LocalFormaSolicitacaoCotista) ? info.LocalFormaSolicitacaoCotista.Trim() : "";
            //
            corpo.getDS_POLIT_EXERC_VOTO_ATIV_FINANC.politicaExercicioVoto = !String.IsNullOrEmpty(info.PoliticaExercicioVoto) ? info.PoliticaExercicioVoto.Trim() : "";
            corpo.getDS_POLIT_EXERC_VOTO_ATIV_FINANC.COD_VOTO_GEST_ASSEMB = !String.IsNullOrEmpty(info.CodVotoGestAssemb) ? info.CodVotoGestAssemb.Trim() : "";

            #endregion
        }

        /// <summary>
        /// Constroe o objeto IDENT_CLASSIF_RISCO
        /// </summary>
        /// <param name="dataPosicao"></param>
        /// <param name="idCliente"></param>
        /// <param name="corpo">Objeto representativo do Corpo do arquivo XML</param>
        private void ControeIDENT_CLASSIF_RISCO(DateTime dataPosicao, int idCliente, XML_InfoComplementar.Corpo corpo, InformacoesComplementaresFundo info) {           
            //
            corpo.getIDENT_CLASSIF_RISCO.CLASSIF_AGENC_RISCO = !String.IsNullOrEmpty(info.AgenciaClassificacaoRisco) ? info.AgenciaClassificacaoRisco.Trim() : "";
            corpo.getIDENT_CLASSIF_RISCO.NM_PREST = !String.IsNullOrEmpty(info.NmPrest) ? info.NmPrest.Trim() : "";
            corpo.getIDENT_CLASSIF_RISCO.NR_CNPJ = !String.IsNullOrEmpty(info.NrCnpj) ? info.NrCnpj.Trim() : "";
            corpo.getIDENT_CLASSIF_RISCO.DISCL_ADVERT = !String.IsNullOrEmpty(info.DisclAdvert) ? info.DisclAdvert.Trim() : "";
            corpo.getIDENT_CLASSIF_RISCO.AGENC_CLASSIF_RATIN = !String.IsNullOrEmpty(info.AgencClassifRatin) ? info.AgencClassifRatin.Trim() : "";
        }

        /// <summary>
        /// Constroe o objeto APRES_ADM_GEST
        /// </summary>
        /// <param name="dataPosicao"></param>
        /// <param name="idCliente"></param>
        /// <param name="corpo">Objeto representativo do Corpo do arquivo XML</param>
        private void ControeAPRES_ADM_GEST(DateTime dataPosicao, int idCliente, XML_InfoComplementar.Corpo corpo, InformacoesComplementaresFundo info) {           
            //
            corpo.getAPRES_ADM_GEST.recursosServicosGestor = !String.IsNullOrEmpty(info.RecursosServicosGestor) ? info.RecursosServicosGestor.Trim() : "";
            corpo.getAPRES_ADM_GEST.recursosServicosAdm = !String.IsNullOrEmpty(info.ApresDetalheAdm) ? info.ApresDetalheAdm.Trim() : "";
        }


        /// <summary>
        /// Constroe o objeto LISTA_PREST_SERVIC
        /// </summary>
        /// <param name="dataPosicao"></param>
        /// <param name="idCliente"></param>
        /// <param name="corpo">Objeto representativo do Corpo do arquivo XML</param>
        private void ControeLISTA_PREST_SERVIC(DateTime dataPosicao, int idCliente, XML_InfoComplementar.Corpo corpo, InformacoesComplementaresFundo info) {

            #region Controe PREST_SERVIC

            XML_InfoComplementar.PREST_SERVIC elem = new XML_InfoComplementar.PREST_SERVIC();
            //
            elem.NM_PREST = !String.IsNullOrEmpty(info.PrestadoresServicos) ? info.PrestadoresServicos.Trim() : "";
            elem.DS_SERVICO_PRESTADO = !String.IsNullOrEmpty(info.DsServicoPrestado) ? info.DsServicoPrestado.Trim() : "";
            //
            corpo.getLISTA_PREST_SERVIC.getPREST_SERVICA.Add(elem);

            #endregion
        }

        /// <summary>
        /// Constroe o objeto POLIT_DISTR_COTA
        /// </summary>
        /// <param name="dataPosicao"></param>
        /// <param name="idCliente"></param>
        /// <param name="corpo">Objeto representativo do Corpo do arquivo XML</param>
        private void ControePOLIT_DISTR_COTA(DateTime dataPosicao, int idCliente, XML_InfoComplementar.Corpo corpo, InformacoesComplementaresFundo info) {           
            //
            corpo.getPOLIT_DISTR_COTA.politicaDistribuicaoCotas = !String.IsNullOrEmpty(info.PoliticaDistribuicaoCotas) ? info.PoliticaDistribuicaoCotas.Trim() : "";
            corpo.getPOLIT_DISTR_COTA.COD_DISTR_OFERTA_PUB = !String.IsNullOrEmpty(info.CodDistrOfertaPub) ? info.CodDistrOfertaPub.Trim() : "";
        }

        /// <summary>
        /// Constroe o objeto OUTR_INFORM_RELEV
        /// </summary>
        /// <param name="dataPosicao"></param>
        /// <param name="idCliente"></param>
        /// <param name="corpo">Objeto representativo do Corpo do arquivo XML</param>
        private void ControeOUTR_INFORM_RELEV(DateTime dataPosicao, int idCliente, XML_InfoComplementar.Corpo corpo, InformacoesComplementaresFundo info) {           
            //
            corpo.getOUTR_INFORM_RELEV.observacoes = !String.IsNullOrEmpty(info.Observacoes) ? info.Observacoes.Trim() : "";
            corpo.getOUTR_INFORM_RELEV.INFORM_AUTOREGUL_ANBIMA = !String.IsNullOrEmpty(info.InformAutoregulAnbima) ? info.InformAutoregulAnbima.Trim() : "";
        }        
   }
}