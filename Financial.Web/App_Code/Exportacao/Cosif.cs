﻿using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Text.RegularExpressions;
using Financial.InvestidorCotista.Custom;
using System.IO;
using System.Collections.Generic;
using System.Text;
using Financial.Contabil;
using Financial.Investidor;
using Financial.Util;
using Financial.CRM;

/// <summary>
/// Summary description for Cosif
/// </summary>
public class Cosif
{

    public void Exporta4016(DateTime data, List<int> idClientes, out MemoryStream msCosif, out string nomeArquivoCosif)
    {
        msCosif = new MemoryStream();
        nomeArquivoCosif = "";

        Cliente cliente = new Cliente();

        DateTime dataPrimeiro = Calendario.RetornaPrimeiroDiaUtilAno(data, 0);
        DateTime dataUltimo = Calendario.RetornaUltimoDiaUtilMes(data, 0);

        for (int c = 0; c < idClientes.Count; c++)
        {
            cliente.LoadByPrimaryKey(idClientes[c]);

            nomeArquivoCosif = "cos4016_" + cliente.IdCliente + "_" + data.ToString("MMyyyy") + ".cvm";
    
            StreamWriter arquivo = new StreamWriter(msCosif, Encoding.ASCII);

            List<Balancete> l = SelecionaDados(data, cliente.IdCliente.Value, dataPrimeiro, dataUltimo);

            if(l.Count > 0)
            {
                Pessoa pessoa = new Pessoa();
                pessoa.LoadByPrimaryKey(cliente.IdPessoa.Value);

                arquivo.Write("#C14016");
                if(pessoa.Cpfcnpj.Length > 8)
                    arquivo.Write(pessoa.Cpfcnpj.Replace(".", "").Replace("/", "").Replace("-", ""));
                else
                    arquivo.Write(new String(' ', 14));

                arquivo.Write(new String(' ', 8) + data.ToString("MMyyyy") + new String(' ', 36));

                arquivo.WriteLine();

                #region Detalhe
                for (int i = 0; i < l.Count; i++)
                {

                    arquivo.Write(CalculoNrContaDigito(l[i].CodigoConta.Replace(".", "").PadRight(7, '0')));
                    arquivo.Write(new String(' ', 4));
                    arquivo.Write(l[i].SaldoFinal.ToString().Replace(",", "").Replace("-", "").PadLeft(18, '0'));
                    if(l[i].SaldoFinal < 0)
                        arquivo.Write('-');
                    else
                        arquivo.Write('+');
                    arquivo.Write(l[i].SaldoFinal.ToString().Replace(",", "").Replace("-", "").PadLeft(18, '0'));
                    if (l[i].SaldoFinal < 0)
                        arquivo.Write('-');
                    else
                        arquivo.Write('+');
                    arquivo.Write("0".PadLeft(18, '0'));
                    arquivo.Write('+');
                    arquivo.WriteLine();
                }
                #endregion

                arquivo.Write("@1");

                arquivo.Write(Convert.ToString(l.Count + 2).PadLeft(6, '0'));

                arquivo.Write(new String(' ', 63));
                arquivo.WriteLine();

                arquivo.Flush();

                // Retorna a MemoryStream para o inicio
                msCosif.Seek(0, SeekOrigin.Begin);
            }
            else
            {
                throw new Exception("Não existem Dados para Geração do arquivo.");
            }
        }

    }

    public void Exporta4010(DateTime data, List<int> idClientes, out MemoryStream msCosif, out string nomeArquivoCosif)
    {
        msCosif = new MemoryStream();
        nomeArquivoCosif = "";

        Cliente cliente = new Cliente();

        DateTime dataPrimeiro = Calendario.RetornaPrimeiroDiaCorridoMes(data, 0);
        DateTime dataUltimo = Calendario.RetornaUltimoDiaUtilMes(data, 0);

        for (int c = 0; c < idClientes.Count; c++)
        {
            cliente.LoadByPrimaryKey(idClientes[c]);

            nomeArquivoCosif = "cos4010_" + cliente.IdCliente + "_" + data.ToString("MMyyyy") + ".cvm";

            StreamWriter arquivo = new StreamWriter(msCosif, Encoding.ASCII);

            List<Balancete> l = SelecionaDados(data, cliente.IdCliente.Value, dataPrimeiro, dataUltimo);

            if (l.Count > 0)
            {
                Pessoa pessoa = new Pessoa();
                pessoa.LoadByPrimaryKey(cliente.IdPessoa.Value);

                arquivo.Write("#C14010");
                if (pessoa.Cpfcnpj.Length > 8)
                    arquivo.Write(pessoa.Cpfcnpj.Replace(".", "").Replace("/", "").Replace("-", ""));
                else
                    arquivo.Write(new String(' ', 14));

                arquivo.Write(new String(' ', 8) + data.ToString("MMyyyy") + new String(' ', 36));

                arquivo.WriteLine();

                #region Detalhe
                for (int i = 0; i < l.Count; i++)
                {
                    arquivo.Write(CalculoNrContaDigito(l[i].CodigoConta.Replace(".", "").PadRight(7, '0')));
                    arquivo.Write(new String(' ', 4));
                    arquivo.Write(l[i].SaldoFinal.ToString().Replace(".", "").Replace(",", "").Replace("-", "").PadLeft(18, '0'));
                    if (l[i].SaldoFinal < 0)
                        arquivo.Write('-');
                    else
                        arquivo.Write('+');
                    arquivo.Write(l[i].SaldoFinal.ToString().Replace(".", "").Replace(",", "").Replace("-", "").PadLeft(18, '0'));
                    if (l[i].SaldoFinal < 0)
                        arquivo.Write('-');
                    else
                        arquivo.Write('+');
                    arquivo.Write("0".PadLeft(18, '0'));
                    arquivo.Write('+');
                    arquivo.WriteLine();
                }
                #endregion

                arquivo.Write("@1");

                arquivo.Write(Convert.ToString(l.Count + 2).PadLeft(6, '0'));

                arquivo.Write(new String(' ', 63));
                arquivo.WriteLine();

                arquivo.Flush();

                // Retorna a MemoryStream para o inicio
                msCosif.Seek(0, SeekOrigin.Begin);
            }
            else
            {
                throw new Exception("Não existem Dados para Geração do arquivo.");
            }
        }

    }

    private List<Balancete> SelecionaDados(DateTime data, int idCliente, DateTime dataPrimeiro, DateTime dataUltimo)
    {
        Cliente cliente = new Cliente();
        cliente.LoadByPrimaryKey(idCliente);

        ContabSaldo contabSaldo = new ContabSaldo();
        contabSaldo.Query.Select(contabSaldo.Query.Data.Max());
        contabSaldo.Query.Where(contabSaldo.Query.IdCliente.Equal(idCliente));
        contabSaldo.Query.Load();

        if (contabSaldo.Data.HasValue)
        {
            if (dataUltimo > contabSaldo.Data.Value)
            {
                dataUltimo = contabSaldo.Data.Value;
            }
        }

        if (dataUltimo > cliente.DataDia.Value)
        {
            dataUltimo = cliente.DataDia.Value;
        }

        //this.dataFimSaldo = dataUltimo;
        #region Consulta
        ContabContaCollection c = new ContabContaCollection();
        c.Query.Load();
        #endregion

        #region Ordenação
        c.CreateColumnsForBinding();
        //
        c.AddColumn("Ordenacao", typeof(System.Int64));
        //
        DateTime dtInicio = Calendario.RetornaPrimeiroDiaCorridoMes(data, 0);
        DateTime dtFinal = Calendario.RetornaUltimoDiaCorridoMes(data, 0);
        //

        for (int i = 0; i < c.Count; i++)
        {
            string codigo = c[i].Codigo;

            codigo = Regex.Replace(codigo, "[^0-9]+", string.Empty);
            codigo = codigo.PadRight(18, '0');
            //
            Int64 codigoInt = Convert.ToInt64(codigo);
            //
            c[i].SetColumn("Ordenacao", codigoInt);
        }

        c.Sort = "Ordenacao ASC";
        //
        #endregion

        #region Monta Objeto Balancete
        List<Balancete> l = new List<Balancete>();
        //
        for (int i = 0; i < c.Count; i++)
        {
            Balancete b = new Balancete();
            //
            b.IdConta = c[i].IdConta.Value;
            b.TipoConta = c[i].TipoConta.Trim().ToUpper();
            b.CodigoConta = String.IsNullOrEmpty(c[i].Codigo) ? "" : c[i].Codigo.Trim();
            b.CodigoReduzida = String.IsNullOrEmpty(c[i].CodigoReduzida) ? "" : c[i].CodigoReduzida.Trim();
            b.Descricao = c[i].Descricao.Trim();
            //
            b.ContaDetalhe = this.IsContaDetalhe(c[i].IdConta.Value);

            List<int> listaContaDetalhe = new List<int>();

            if (!b.ContaDetalhe)
            {
                listaContaDetalhe = RetornaListaContaDetalhe(c[i].IdConta.Value);
            }

            #region Saldos

            b.SaldoAnterior = b.SaldoFinal = 0;

            if (b.ContaDetalhe)
            {
                #region Detalhe
                //
                ContabSaldo cAnterior = new ContabSaldo();
                if (cAnterior.LoadByPrimaryKey(idCliente, c[i].IdConta.Value, dataPrimeiro))
                {
                    b.SaldoAnterior = cAnterior.SaldoAnterior.Value;
                }

                ContabSaldo cFinal = new ContabSaldo();
                if (cFinal.LoadByPrimaryKey(idCliente, c[i].IdConta.Value, dataUltimo))
                {
                    b.SaldoFinal = cFinal.SaldoFinal.Value;
                }
                #endregion
            }
            else
            {
                #region Não Detalhe
                ContabContaCollection cc = new ContabContaCollection();
                cc.Query.Select(cc.Query.IdConta)
                       .Where(cc.Query.IdConta.In(listaContaDetalhe));

                cc.Query.Load();

                // Para cada conta
                for (int j = 0; j < cc.Count; j++)
                {
                    ContabSaldo cAnterior1 = new ContabSaldo();
                    if (cAnterior1.LoadByPrimaryKey(idCliente, cc[j].IdConta.Value, dataPrimeiro))
                    {
                        b.SaldoAnterior += cAnterior1.SaldoAnterior.Value;
                    }

                    ContabSaldo cFinal = new ContabSaldo();
                    if (cFinal.LoadByPrimaryKey(idCliente, cc[j].IdConta.Value, dataUltimo))
                    {
                        b.SaldoFinal += cFinal.SaldoFinal.Value;
                    }
                }

                #endregion
            }
            #endregion

            #region Debito/Credito
            b.CreditoMes = b.DebitoMes = 0;

            if (b.ContaDetalhe)
            {
                #region Detalhe
                //
                ContabSaldo cTotal = new ContabSaldo();
                cTotal.Query.Select(cTotal.Query.TotalCredito.Sum(),
                                    cTotal.Query.TotalDebito.Sum());
                //
                cTotal.Query.Where(cTotal.Query.IdCliente == idCliente,
                                   cTotal.Query.IdConta == c[i].IdConta.Value,
                                   cTotal.Query.Data.Between(dataPrimeiro, dataUltimo));
                //                                
                cTotal.Query.Load();

                if (cTotal.es.HasData)
                {
                    if (cTotal.TotalCredito.HasValue)
                    {
                        b.CreditoMes = cTotal.TotalCredito.Value;
                    }
                    if (cTotal.TotalDebito.HasValue)
                    {
                        b.DebitoMes = cTotal.TotalDebito.Value;
                    }
                }
                #endregion
            }
            else
            {
                #region Não Detalhe
                ContabContaCollection cc1 = new ContabContaCollection();
                cc1.Query.Select(cc1.Query.IdConta)
                       .Where(cc1.Query.IdConta.In(listaContaDetalhe));

                cc1.Query.Load();

                // Para cada conta
                for (int k = 0; k < cc1.Count; k++)
                {
                    ContabSaldo cTotal = new ContabSaldo();
                    cTotal.Query.Select(cTotal.Query.TotalCredito.Sum(),
                                        cTotal.Query.TotalDebito.Sum());
                    //
                    cTotal.Query.Where(cTotal.Query.IdCliente == idCliente,
                                       cTotal.Query.IdConta == cc1[k].IdConta.Value,
                                       cTotal.Query.Data.Between(dataPrimeiro, dataUltimo));
                    //                                
                    cTotal.Query.Load();

                    if (cTotal.es.HasData)
                    {
                        if (cTotal.TotalCredito.HasValue)
                        {
                            b.CreditoMes += cTotal.TotalCredito.Value;
                        }
                        if (cTotal.TotalDebito.HasValue)
                        {
                            b.DebitoMes += cTotal.TotalDebito.Value;
                        }
                    }
                }
                #endregion
            }
            #endregion

            if (this.TemSaldoMovimento(b.SaldoAnterior, b.SaldoFinal, b.CreditoMes, b.DebitoMes))
            {
                l.Add(b);
            }
        }
        #endregion

        return l;
    }

    /// <summary>
    /// Retorna true se algum valor for diferente de 0
    /// </summary>
    /// <param name="saldoInicio"></param>
    /// <param name="SaldoFinal"></param>
    /// <param name="net"></param>
    /// <returns></returns>
    private bool TemSaldoMovimento(decimal saldoInicio, decimal saldoFinal, decimal creditoMes, decimal debitoMes)
    {
        return saldoInicio != 0 ||
               saldoFinal != 0 ||
               creditoMes != 0 ||
               debitoMes != 0;
    }

    private List<Balancete> SelecionaDados4010(DateTime data, int idCliente, DateTime dataPrimeiro, DateTime dataUltimo)
    {

        #region Consulta
        ContabContaCollection c = new ContabContaCollection();
        c.Query.Load();
        #endregion

        #region Ordenação
        c.CreateColumnsForBinding();
        //
        c.AddColumn("Ordenacao", typeof(System.Int64));
        //
        DateTime dtInicio = Calendario.RetornaPrimeiroDiaCorridoMes(data, 0);
        DateTime dtFinal = Calendario.RetornaUltimoDiaCorridoMes(data, 0);
        //

        for (int i = 0; i < c.Count; i++)
        {
            string codigo = c[i].Codigo;

            codigo = Regex.Replace(codigo, "[^0-9]+", string.Empty);
            codigo = codigo.PadRight(18, '0');
            //
            Int64 codigoInt = Convert.ToInt64(codigo);
            //
            c[i].SetColumn("Ordenacao", codigoInt);
        }

        c.Sort = "Ordenacao ASC";
        //
        #endregion

        #region Monta Objeto Balancete
        List<Balancete> l = new List<Balancete>();
        //
        for (int i = 0; i < c.Count; i++)
        {
            Balancete b = new Balancete();
            //
            b.IdConta = c[i].IdConta.Value;
            b.TipoConta = c[i].TipoConta.Trim().ToUpper();
            b.CodigoConta = String.IsNullOrEmpty(c[i].Codigo) ? "" : c[i].Codigo.Trim();
            //b.Descricao = this.RetornaDescricaoAlinhada(c[i].Descricao.Trim(), b.CodigoConta);
            //
            b.ContaDetalhe = this.IsContaDetalhe(c[i].IdConta.Value);
            //

            List<int> listaContaDetalhe = new List<int>();

            if (!b.ContaDetalhe)
            {
                listaContaDetalhe = RetornaListaContaDetalhe(c[i].IdConta.Value);
            }

            #region Saldos
            b.SaldoFinal = 0;

            if (b.ContaDetalhe)
            {
                #region Detalhe
                //           
                ContabSaldoCollection cFinal = new ContabSaldoCollection();
                cFinal.Query.Select(cFinal.Query.SaldoFinal.Sum());
                cFinal.Query.Where(cFinal.Query.IdCliente.Equal(idCliente));
                cFinal.Query.Where(cFinal.Query.Data.GreaterThanOrEqual(dtInicio));
                cFinal.Query.Where(cFinal.Query.Data.LessThanOrEqual(dtFinal));
                cFinal.Query.Where(cFinal.Query.IdConta.Equal(c[i].IdConta.Value));
                cFinal.Query.Load();
                if (cFinal[0].SaldoFinal.HasValue)
                {
                    b.SaldoFinal = cFinal[0].SaldoFinal.Value;
                }
                #endregion
            }
            else
            {
                #region Não Detalhe
                ContabContaCollection cc = new ContabContaCollection();
                cc.Query.Select(cc.Query.IdConta)
                        .Where(cc.Query.IdConta.In(listaContaDetalhe));

                cc.Query.Load();

                // Para cada conta
                for (int j = 0; j < cc.Count; j++)
                {
                    ContabSaldoCollection cFinal = new ContabSaldoCollection();
                    cFinal.Query.Select(cFinal.Query.SaldoFinal.Sum());
                    cFinal.Query.Where(cFinal.Query.IdCliente.Equal(idCliente));
                    cFinal.Query.Where(cFinal.Query.Data.GreaterThanOrEqual(dtInicio));
                    cFinal.Query.Where(cFinal.Query.Data.LessThanOrEqual(dtFinal));
                    cFinal.Query.Where(cFinal.Query.IdConta.Equal(c[i].IdConta.Value));
                    cFinal.Query.Load();
                    if (cFinal[0].SaldoFinal.HasValue)
                    {
                        b.SaldoFinal = cFinal[0].SaldoFinal.Value;
                    }
                }
                #endregion
            }
            #endregion

            // Adiciona se tem Saldo
            if (this.TemSaldo(b.SaldoFinal))
            {
                l.Add(b);
            }

            //l.Add(b);
        }
        #endregion

        return l;
    }

    /// <summary>
    /// Baseado no Nivel do Codigo retorna uma descricao com Pad a Esquerda
    /// </summary>
    /// <param name="descricao"></param>
    /// <param name="codigo"></param>
    /// <returns></returns>
    private string RetornaDescricaoAlinhada(string descricao, string codigo)
    {
        string retorno = "";

        foreach (char c in codigo)
        {
            if (c == '.')
            {
                retorno += "  ";
            }
        }
        return retorno = retorno + descricao;
    }

    /// <summary>
    /// Dado um IdConta determina se é conta detalhe. ultimo nivel
    /// </summary>
    /// <param name="codigoConta"></param>
    /// <returns></returns>
    private bool IsContaDetalhe(int idConta)
    {

        ContabContaCollection c = new ContabContaCollection();
        //
        c.Query.Select(c.Query.IdConta);
        c.Query.Where(c.Query.IdContaMae == idConta);
        c.Query.Load();

        return c.Count == 0;
    }

    private List<int> RetornaListaContaDetalhe(int idContaMae)
    {
        List<int> lista = new List<int>();

        ContabContaCollection coll = new ContabContaCollection();
        coll.Query.Select(coll.Query.IdConta);
        coll.Query.Where(coll.Query.IdContaMae == idContaMae);
        coll.Query.Load();

        foreach (ContabConta contabConta in coll)
        {
            if (this.IsContaDetalhe(contabConta.IdConta.Value))
            {
                lista.Add(contabConta.IdConta.Value);
            }
            else
            {
                List<int> listaAux = this.RetornaListaContaDetalhe(contabConta.IdConta.Value);

                foreach (int id in listaAux)
                {
                    lista.Add(id);
                }
            }
        }

        return lista;
    }

    /// <summary>
    /// Retorna true se saldoFinal for diferente de 0
    /// </summary>
    /// <param name="SaldoFinal"></param>
    /// <returns></returns>
    private bool TemSaldo(decimal saldoFinal)
    {
        return saldoFinal != 0;
    }

    private string CalculoNrContaDigito(string conta)
    {
        int[] ctrl = { 3, 1, 7, 3, 1, 7, 3 };
        int soma = 0;

        for (int i = 0; i <= 6; i++)
        {
            soma += Convert.ToInt32(conta.Substring(i, 1)) * ctrl[i];
        }

        int dv = (soma % 10) == 0 ? 0 : 10 - (soma % 10);

        return string.Format("00{0}{1}", conta, dv);
    }
}
